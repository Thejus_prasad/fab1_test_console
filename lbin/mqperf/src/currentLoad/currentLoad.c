#include <stdio.h>
#include <stdlib.h>
#include <string.h>
/* includes for MQI  */
#include <cmqc.h>
#include <pthread.h>
#include <unistd.h>
#include <time.h>
#include <sys/time.h>
#include <sys/stat.h>
#include <sys/queue.h>
#include <md5.h>
#include <currentLoad.h>
#include <semaphore.h>
#include <errno.h>


MQLONG waitInterval=10000;
short doTerminate=0;
char *queueManager;
char *writeQueue;
int speedupFactor;
int messageInflation;
int iterations;
int sleepInterval;
FILE *fpLogFile;
FILE *fpRandomFile;
time_t startTS;
int cycleDuration;
int fRandomSize;
char *inFile;
sem_t listEmptySemaphore;

static pthread_mutex_t listMutex = PTHREAD_MUTEX_INITIALIZER;

char *getTS(char *buffer){
	struct tm tmStruct;
	time_t t=time(NULL);
	localtime_r(&t, &tmStruct);
	sprintf(buffer,"%04d-%02d-%02d-%02d:%02d:%02d",tmStruct.tm_year+1900,tmStruct.tm_mon+1,tmStruct.tm_mday,tmStruct.tm_hour,tmStruct.tm_min,tmStruct.tm_sec);
	return buffer;
}

int main(int argc, char **argv){
	if(argc != 9 && argc != 12 ){
		printf ("usage %s <messageFile> <logFile> <randomFile> <queueManager> <workingQueue> <Speedup factor> <Message inflation factor> <Iterations> [<start timestamp(seconds since epoche)> <cycleDuration in s> <sleepInterval in s>] \n",argv[0]);
		return 1;
	}
	inFile=argv[1];
	char *logFile=argv[2];
	char *randomFile=argv[3];
	queueManager=argv[4];
	writeQueue=argv[5];
	speedupFactor=atoi(argv[6]);
	messageInflation=atoi(argv[7]);
	iterations=atoi(argv[8]);

	printf("inFile:%s logFile:%s randomFile:%s qm:%s writeQueue:%s speedup:%d inflation:%d iterations:%d\n",inFile,logFile,randomFile,queueManager,writeQueue,speedupFactor,messageInflation,iterations);

	if( speedupFactor<=0 || messageInflation<=0){
		printf ("Speedup factor and Message inflation factor must be greater than 0\n");
		return 1;
	}

	if (! (fpRandomFile=fopen(randomFile,"r"))) {
		printf("unable to open %s\n",randomFile);
		return 1;
	}

	if (! (fpLogFile=fopen(logFile,"a"))) {
		printf("unable to open %s\n",logFile);
		fclose(fpRandomFile);
		return 1;
	}

	struct stat statStruct;
	stat(randomFile,&statStruct);
	fRandomSize=statStruct.st_size;
	//printf ("randomSize: %d byte\n",randomSize);

	if(argc >9){
		startTS=(time_t) atoi(argv[9]);
		cycleDuration=atoi(argv[10]);
		sleepInterval=atoi(argv[11]);
	}else{
		startTS=time(0);
		cycleDuration=0;
		sleepInterval=0;
	}
	printf("startTS: %d, cycleDuration: %d, sleepInterval: %d\n",startTS,cycleDuration,sleepInterval);

	sem_init(&listEmptySemaphore,0,0); //initially list is empty, therefore initialize with 0
	TAILQ_INIT(&head);                       /* Initialize the list. */
	pthread_t thread1,thread2;

	if(pthread_create (&thread1, NULL, readerThread, 0))
		printf("%s\n","Failed to create readerThread");
	else
		printf("%s\n","Created reader thread");


	if(pthread_create (&thread2, NULL, writerThread, 0))
		printf("%s\n","Failed to create writerThread");
	else
		printf("%s\n","Created writer thread");

	pthread_join (thread2, NULL);
	doTerminate=1;
	pthread_join (thread1, NULL);

	sem_destroy(&listEmptySemaphore);
	fclose(fpRandomFile);
	fclose(fpLogFile);
	return 0;

}

int generateRandomString(int iSize, char *pResult)
{
	char dString [50];
	int offset=0;
	//use random offset into the file but max fRandomSize - iSize
	if(iSize < fRandomSize)
		offset=rand()%(fRandomSize - iSize);
	//printf("random File fRandomSize:%d offset:%d size:%d endpos:%d\n",fRandomSize,offset,iSize,(fRandomSize-offset-iSize));
	fseek(fpRandomFile,offset,0);
	if(! fread( pResult,iSize,1,fpRandomFile)){
		fprintf(fpLogFile,"%s error generating random string, but don't know how to behave fRandomSize:%d offset:%d size:%d lengthTillEndOfRandomFile:%d\n",getTS(dString),fRandomSize,offset,iSize,(fRandomSize-offset-iSize));
		return 1;
	};
	return 0;
}


int connectMQ (char *pQManager, MQHCONN  *conHandle){
	MQLONG compCode, reason;
	MQCONN(pQManager,conHandle,&compCode,&reason);
	printf ("Connecting to %s: hConn: %d, compCode:%d, reason:%d\n",pQManager,*conHandle,compCode,reason);
	if(compCode){
		printf ("connection failed\n");
		return 1;
	}
	return 0;
}

int getElement(struct msgElement **msgEle){
	//decrement listEmptySemaphore; don't block longer than 1 minute to be able to end the thread
	struct timespec ts;
	ts.tv_sec=time(NULL)+60;
	ts.tv_nsec=0;

	sem_timedwait(&listEmptySemaphore,&ts);

	pthread_mutex_lock(&listMutex);
	if(head.tqh_first != NULL){
		*msgEle=head.tqh_first;
		TAILQ_REMOVE(&head,head.tqh_first, entries);
		pthread_mutex_unlock(&listMutex);
		return 0;
	}
	pthread_mutex_unlock(&listMutex);
	return 1;
}

int addElement(struct msgElement *msgEle){
	pthread_mutex_lock(&listMutex);
	TAILQ_INSERT_TAIL(&head, msgEle, entries);
	pthread_mutex_unlock(&listMutex);
	//increase semaphore
	sem_post(&listEmptySemaphore);
 }

void *readerThread(void *arg){

	char dString[50];
	MQHCONN hConn;
	if(connectMQ(queueManager,&hConn)){
		doTerminate=1;
		return;
	}

	MQOD od ={MQOD_DEFAULT};
    strncpy(od.ObjectName, writeQueue, (size_t)MQ_Q_NAME_LENGTH);
	MQLONG openOptions = MQOO_INPUT_SHARED | MQOO_FAIL_IF_QUIESCING;
	MQHOBJ qHandle; 
	MQLONG compCode, reason;

	MQOPEN(hConn, &od, openOptions, &qHandle, &compCode,&reason);
	printf ("queueOpen %s: qHandle: %d, compCode:%d, reason:%d\n",writeQueue,qHandle,compCode,reason);
	if(compCode != 0){
		printf ("queueOpen failed\n");
		MQDISC(&hConn,&compCode,&reason);
		printf("Disconnect: hConn: %d, compCode:%d, reason:%d\n",hConn,compCode,reason);;
		doTerminate=1;
		return ;
	}
	
	MQGMO getMsgOpts= {MQGMO_DEFAULT};
	getMsgOpts.Options=MQGMO_FAIL_IF_QUIESCING|MQGMO_WAIT;
	getMsgOpts.WaitInterval=waitInterval;
	getMsgOpts.MatchOptions=MQMO_MATCH_MSG_ID;

	MQMD md = {MQMD_DEFAULT};
	char *msgBuffer=0;
	size_t bufferLength=0;
	MQLONG messLen;
	struct msgElement *msgEle;
	struct timeval tVal;
	while (! doTerminate){
		while(!getElement(&msgEle)){

			memcpy(md.MsgId,msgEle->msgID,sizeof(md.MsgId));
//			printf("Reader:msgID orig:%s copied:%s\n",msgEle->msgID,md.MsgId);

			// take a lazy approach allocating buffer space; grow if needed; don't shrink
			if(bufferLength < msgEle->msgLength){
				if(msgBuffer)
					free(msgBuffer);
				if(! (msgBuffer=(char *)malloc(msgEle->msgLength)))
					{
						fprintf(fpLogFile,"%s OutOfMemory -- malloc failed!\n",getTS(dString));
						doTerminate=1;
						free(msgEle);						
						return;
					}
				bufferLength=msgEle->msgLength;
			}
			MQGET(hConn, qHandle, &md, &getMsgOpts, msgEle->msgLength, msgBuffer, &messLen, &compCode, &reason);
			if(gettimeofday(&tVal,0)){
				fprintf(fpLogFile,"%s gettimeofday returned error\n",getTS(dString));
				doTerminate=1;
				free(msgBuffer);
				free(msgEle);
				return;
			}
			time_t receiveTime=tVal.tv_sec*1000 + tVal.tv_usec/1000;
			unsigned char receivedHash[33];
			if(reason==0){
				getMD5Hash(msgBuffer,messLen,receivedHash);
				//print "receivedHash:\"$receivedHash\"\n";
				time_t roundTripTime=(receiveTime - msgEle->startTime);
				if(strncmp(receivedHash,msgEle->md5Hash,16)){
					fprintf(fpLogFile,"%s message corrupt msgLengthExpected:%d msgLengthGot:%d msgHash expected: %s msgHash got:%s\n",getTS(dString),msgEle->msgLength,messLen,msgEle->md5Hash,receivedHash);
					fflush(fpLogFile);
				}else{
					fprintf(fpLogFile,"%s message received OK roundTripTime:%d ms  messageSize:%d\n",getTS(dString),roundTripTime,messLen);
					fflush(fpLogFile);
				}
				//print "MQGet: qHandle: $qHandle, compCode:$compCode, reason:$reason bufferLength:$bufferLength buffer:$buffer roundTripTime:$roundTripTime\n";
			}else if(reason == 2033){
				fprintf(fpLogFile,"%s MQRC 2033 message not available within timeout; discard it. msgLengthExpected:%d msgLengthGot:%d msgHash expected: %s msgHash got:%s\n",getTS(dString),msgEle->msgLength,messLen,msgEle->md5Hash,receivedHash);
				fflush(fpLogFile);
			}else{
				fprintf(fpLogFile,"%s ERROR: MQGET returned %d\n",getTS(dString),reason);
				fflush(fpLogFile);
			}
			free(msgEle);
		}
		//printf("list empty; lets try again\n");
		//sleep(1);
	}
	free(msgBuffer);
	MQCLOSE(hConn,&qHandle,0,&compCode,&reason);
	printf ("MQClose %s: qHandle: %d, compCode:%d, reason:%d\n",writeQueue,qHandle,compCode,reason);
	MQDISC(&hConn,&compCode,&reason);
	printf ("hConn: %d, compCode:%d, reason:%d\n",hConn,compCode,reason);
	fprintf(fpLogFile,"%s readerThread ended\n",getTS(dString));
	fflush(fpLogFile);
}



void *writerThread(void *arg){
	
	char dString[50];
	MQHCONN hConn;
	if(connectMQ(queueManager,&hConn)){
		doTerminate=1;
		return;
	}

	MQOD od ={MQOD_DEFAULT};
    strncpy(od.ObjectName, writeQueue, (size_t)MQ_Q_NAME_LENGTH);
	MQLONG openOptions = MQOO_OUTPUT | MQOO_FAIL_IF_QUIESCING;
	MQHOBJ qHandle; 
	MQLONG compCode, reason;

	MQOPEN(hConn, &od, openOptions, &qHandle, &compCode,&reason);
	printf ("queueOpen %s: qHandle: %d, compCode:%d, reason:%d\n",writeQueue,qHandle,compCode,reason);
	if(compCode != 0){
		printf ("queueOpen failed\n");
		MQDISC(&hConn,&compCode,&reason);
		printf("Disconnect: hConn: %d, compCode:%d, reason:%d\n",hConn,compCode,reason);;
		return ;
	}
	MQPMO putMsgOpts={MQPMO_DEFAULT};
	putMsgOpts.Options=MQPMO_FAIL_IF_QUIESCING|MQPMO_NEW_MSG_ID;

	MQMD md = {MQMD_DEFAULT};
	md.Report=MQRO_COPY_MSG_ID_TO_CORREL_ID;
	
	char *msgBuffer=0;
	size_t bufferLength=0;
	MQLONG messLen;
	struct timeval tVal;
	time_t sleepTime;
	
	while(iterations --)
	{
		FILE *fpInFile;
		if (! (fpInFile=fopen(inFile,"r")))
		{
			fprintf(fpLogFile,"%s could not open %sfor reading\n",getTS(dString),inFile);
			return;
		}
		char line[1000];
		int lastTimeOffset=0;
		while( fgets(line,sizeof(line),fpInFile)){
			if (doTerminate)
				break;
//			printf("line:%s\n",line);
			time_t timeOffset=(time_t) atoi(strtok(line,"|"));
			md.Persistence=atoi(strtok(NULL,"|"));
			messLen=atoi(strtok(NULL,"|\n"));
//			printf("timeOffset:%d persistence:%d messLen:%d\n",timeOffset,md.Persistence,messLen);
			//timeOffset/=speedupFactor;
			messLen*=messageInflation;
//			printf("timeOffset:%d persistence:%d messLen:%d\n",timeOffset,md.Persistence,messLen);
			// speed up by actually multiplying messages
			int msgCount;
			for(msgCount=1;msgCount<=speedupFactor;msgCount++){
				if (doTerminate)
					break;
				time_t nextTS=startTS+lastTimeOffset+(((timeOffset-lastTimeOffset)/speedupFactor)*msgCount);
				sleepTime=nextTS-time(NULL);
				if(sleepTime>0){
					fprintf(fpLogFile,"%s sleeping:%d\n",getTS(dString),sleepTime);
					fflush(fpLogFile);
					sleep(sleepTime);
				}else if(sleepTime<0){
					fprintf(fpLogFile,"%s Oops. I'm %d seconds behind schedule!\n",getTS(dString),abs(sleepTime));
					fflush(fpLogFile);
				}
				// take a lazy approach allocating buffer space; grow if needed; don't shrink
				if(bufferLength < messLen){
					if(msgBuffer)
						free(msgBuffer);
					if(! (msgBuffer=(char *)malloc(messLen)))
						{
							fprintf(fpLogFile,"%s OutOfMemory -- malloc failed!\n",getTS(dString));
							return;
						}
					bufferLength=messLen;
				}
				if(generateRandomString(messLen,msgBuffer)){
					free(msgBuffer);
					return;
				}
	//			printf("timeOffset:%d persistence:%d messLen:%d\n",timeOffset,md.Persistence,messLen);
				MQPUT(hConn, qHandle, &md, &putMsgOpts, messLen, msgBuffer, &compCode, &reason);
				if(reason ==0)
					fprintf(fpLogFile,"%s Message put OK; messageSize:%d\n",getTS(dString),messLen);
				else
					fprintf(fpLogFile,"%s MQPut failed: qHandle: %d, compCode:%d, reason:%d\n",getTS(dString),qHandle,compCode,reason);
				fflush(fpLogFile);
				struct msgElement *msgEle=(struct msgElement *)malloc(sizeof(struct msgElement));
				if(!msgEle)
				{
					fprintf(fpLogFile,"%s OutOfMemory -- malloc failed!\n",getTS(dString));
					return;
				}
				if(gettimeofday(&tVal,0)){
					fprintf(fpLogFile,"%s gettimeofday returned error\n",getTS(dString));
					free(msgBuffer);
					return;
				}
				msgEle->startTime=tVal.tv_sec*1000 + tVal.tv_usec/1000;
				getMD5Hash(msgBuffer,messLen,msgEle->md5Hash);
				memcpy(msgEle->msgID,md.MsgId,sizeof(md.MsgId));
				msgEle->msgLength=messLen;
	//			printf("Writer: msgID copied:%s orig:%s\n",msgEle->msgID,md.MsgId);
				
				addElement(msgEle);
				//print length($msg)."|".time."|\"".md5_base64($msg)."\"\n";
				lastTimeOffset=timeOffset;
			}
		}
		fclose(fpInFile);
		// sleep until end of cycle and begin next iteration
		time_t sleepTime=(startTS+cycleDuration) - time(NULL);
		int si=sleepInterval;
		while(sleepTime >0){
			if(sleepTime < si || si <= 0)
				si=sleepTime;
			fprintf(fpLogFile,"%s done. sleeping till end of cycle, submitting a message every %d seconds; %d iterations to come\n",getTS(dString),si,iterations);
			fflush(fpLogFile);
			sleep(si);
			//send dummy message ...
			MQPUT(hConn, qHandle, &md, &putMsgOpts, messLen, msgBuffer, &compCode, &reason);
			if(reason ==0)
				fprintf(fpLogFile,"%s Message put OK; messageSize:%d\n",getTS(dString),messLen);
			else
				fprintf(fpLogFile,"%s MQPut failed: qHandle: %d, compCode:%d, reason:%d\n",getTS(dString),qHandle,compCode,reason);
			fflush(fpLogFile);
			struct msgElement *msgEle=(struct msgElement *)malloc(sizeof(struct msgElement));
			if(!msgEle)
			{
				fprintf(fpLogFile,"%s OutOfMemory -- malloc failed!\n",getTS(dString));
				return;
			}
			if(gettimeofday(&tVal,0)){
				fprintf(fpLogFile,"%s gettimeofday returned error\n",getTS(dString));
				free(msgBuffer);
				return;
			}
			msgEle->startTime=tVal.tv_sec*1000 + tVal.tv_usec/1000;
			getMD5Hash(msgBuffer,messLen,msgEle->md5Hash);
			memcpy(msgEle->msgID,md.MsgId,sizeof(md.MsgId));
			msgEle->msgLength=messLen;
//			printf("Writer: msgID copied:%s orig:%s\n",msgEle->msgID,md.MsgId);
			
			addElement(msgEle);
			sleepTime=(startTS+cycleDuration) - time(NULL);
		}
		//set startTS to current time
		startTS+=cycleDuration;
	}
	free(msgBuffer);
	MQCLOSE(hConn,&qHandle,0,&compCode,&reason);
	printf ("MQClose %s: qHandle: %d, compCode:%d, reason:%d\n",writeQueue,qHandle,compCode,reason);
	MQDISC(&hConn,&compCode,&reason);
	printf ("hConn: %d, compCode:%d, reason:%d\n",hConn,compCode,reason);
	fprintf(fpLogFile,"%s writerThread ended\n",getTS(dString));
	return;
}

void getMD5Hash(char *msgBuffer,int messLen,char *hash){
	MD5_CTX mdContext;
	MD5Init (&mdContext);
	MD5Update (&mdContext, msgBuffer, messLen);
	MD5Final (&mdContext);
	int i;
	for (i = 0; i < 16; i++){
		sprintf (hash,"%02x", mdContext.digest[i]);
		hash+=2;
	}
	*hash=0;
}
