#include <cmqc.h>
#include <sys/queue.h>

void *writerThread(void *arg);
void *readerThread(void *arg);

int generateRandomString(int iSize, char *pResult);

TAILQ_HEAD(listhead, msgElement) head;
struct listhead *headp;         /* List head. */

struct msgElement{
	MQBYTE24 msgID;
	size_t msgLength;
	time_t startTime;
	unsigned char md5Hash [33];
	TAILQ_ENTRY(msgElement) entries;      /* List. */
};
int addElement(struct msgElement *msgEleent);
int getElement(struct msgElement **msgElement);

int connectMQ (char *pQManager, MQHCONN  *conHandle);
void getMD5Hash(char *msgBuffer,int messLen,char *hash);


