=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: Steffen Steidten, 2012-06-22
Version: 14.04
=end

require "RubyTestCase"
require "asmviewtest"


# Test the AsmErpShipment job for Fab1
class Test_jCAP_AsmErpShipment_F1 < RubyTestCase

  @@fabcode = 'F1'
  @@sublottype_alt = 'PR'
  @@lot = nil     # will be set in test12 and test21

  # parameters for lot_complete interface tests (Turnkey process in Fab, product configured in AsmView)
  @@tk_j = SiView::MM::LotTkInfo.new('J', '1T', 'CT')
  @@tk_r = SiView::MM::LotTkInfo.new('R', 'CT', 'AT', 'CT')
  @@erpasmlot_f1 = ERPMES::ErpAsmLot.new(nil, @@tk_j, 'SLVOYAGERV21F1-L01', nil, 'TKEY01.A0', 'C02-TKEY.01', 'gf', 'PO')
  @@erpasmlot_f1_r = ERPMES::ErpAsmLot.new(nil, @@tk_r, 'SLVOYAGERV21F1-L01', nil, 'TKEY01.A0', 'C02-TKEY.01', 'gf', 'PO')
  @@bins = [[359,51,222],[234,34,342],[58,53,46],[245,56,67],[124,89,425],[456,45,130],[1056,46,367],[234,34,345],[465,46,56]]
  @@nwafers = @@bins.size

  # parameters for shipment interface tests (ERPItem and ERPOrder configured in ERP)
  @@tk_j_gggg = SiView::MM::LotTkInfo.new('J', 'GG', 'GG')
  @@erpasmlot_f1_gg = ERPMES::ErpAsmLot.new(nil, @@tk_j_gggg, '*SHELBY21AE-M00-JB4', '6010000548:01:01', 'SHELBY21AE.B4', 'C02-1431.01', 'qgt', 'PO')


  def self.startup
    super(nosiview: true)
    $at = AsmView::Test.new($env, f7: false, f8: false, btf: false)
    $av = $at.av
    $avstb = $at.avstb
    $avb2b = $at.avb2b
    $avship = $at.avship
    $avlotcomplete = $at.avlotcomplete
    $f1sv = $at.f1sv
    $f1b2b = $at.f1b2b
    $f7sv = $at.f7sv
    $f7b2b = $at.f7b2b
  end

  def test01_setup
    $setup_ok = false
    assert $at.check_connectivity, "at least one component is not connected"
    $setup_ok = true
  end

  # Shipment interface (msgs normally sent from ERP), using Fab1 regular subcons (not GG)
  #
  # note: the Fab1 Turnkey job must have full turnkey enabled!!

  def test11_std_shipment_trans_wrong_lot
    $setup_ok = false
    assert $at.ship_complete_wrong_lot(:fabcode=>@@fabcode), "error in lot rejection"
    $setup_ok = true
  end

  def test12_std_shipment_trans_new_lot
    # ship a lot not existing in AsmView
    $setup_ok = false
    # create a Fab1 lot, TurnKey not GG
    assert $f1sv.carrier_list(:empty=>true, :status=>"AVAILABLE", :carrier=>"9%").size > 0, "no empty 9% carriers in #{$f1sv.inspect}"
    lot = $f1b2b.new_lot(@@erpasmlot_f1)
    assert lot, "error creating new Fab lot"
    assert $f1b2b.lot_tkey_prepare(lot)
    assert !$av.lot_exists?(lot), "lot #{lot} already exists in AsmView"
    #
    # send ship_complete message and verify SiView status
    assert $avship.ship_complete(lot, :transstatus=>"Trans-Running", :fabcode=>@@fabcode)
    assert $f1b2b.verify_lot_state(lot, :tkbumpshipinit, 'NonProBank')
    #
    # verify AsmView lot parameters
    assert $av.lot_exists?(lot), "lot #{lot} not created in AsmView"
    assert $avstb.lot_tkey_started?(lot, :bumpb)
    assert_equal $f1b2b.lot_characteristics(lot).tkinfo, $avstb.lot_characteristics(lot).tkinfo
    @@lot = lot
    $setup_ok = true
  end

  def test13_std_shipment_trans_existing_lot
    # ship a lot existing in AsmView, using the lot created in test12 again
    lot = @@lot
    assert lot, "no test lot, run test12 to create one"
    assert $av.lot_exists?(lot), "lot #{lot} does not exist in #{$av}"
    assert $f1sv.lot_exists?(lot), "lot #{lot} does not exist in #{$f1sv}"
    assert $f1b2b.lot_tkey_prepare(lot)
    #
    # send ship_complete message
    res = $avship.ship_complete(lot, :transstatus=>"Trans-Running", :fabcode=>@@fabcode)
    assert res, "wrong reply from ERPShip interface: #{res.inspect}"
    #
    # verify SiView and AsmView lot status
    assert $f1b2b.verify_lot_state(lot, :tkbumpshipinit, 'NonProBank')
    assert $avstb.lot_tkey_started?(lot, :bumpb)
  end

  def test14_ship_several_lots
    # ship several lots with one message, using the lot created in test12 again
    lots = ["XXNOTEX1.00", "XXNOTEX2.00", @@lot]
    # prepare the good lot
    assert $f1b2b.lot_tkey_prepare(@@lot)
    #
    # send ship_complete message
    res = $avship.ship_complete(lots, :transstatus=>"Trans-Running", :fabcode=>@@fabcode)
    assert !res, "wrong ship_complete reply"
    # check response
    details = $avship._service_response[:shipCompleteResponse][:return][:details]
    details = [details] if details.kind_of?(Hash)
    assert details.size < lots.size, "wrong number of failure of details\n#{details.inspect}"
  end

  # LotComplete interface

  def test21_lot_complete
    # Fab1 setup
    @@lot = lot = $f1b2b.new_lot(@@erpasmlot_f1_gg, nwafers: @@nwafers)
    assert lot, "error creating lot"
    $log.info "using lot #{lot.inspect}"
    # normally done by FabGUI
    $f1sv.lot_opelocate(lot, :last)
    $f1sv.lot_bankin(lot)
    refute_equal "", $f1sv.lot_info(lot).bank
    # AsmView setup
    assert $avstb.stb(lot)
    assert_equal 0, $av.asm_diecount_req(lot, @@bins)
    assert $avb2b.lot_cleanup(lot, op: :lotshipdesig)
    #
    # set lot onhold, send lot_complete with different lottype (MSR 682962)
    refute_equal @@sublottype_alt, $av.lot_info(lot).sublottype, "wrong sublottype, check setup"
    assert_equal 0, $av.lot_hold(lot, nil)
    assert $avlotcomplete.lot_complete(lot, :lottype=>@@sublottype_alt)
    assert_equal @@sublottype_alt, $av.lot_info(lot).sublottype
    #
    # send lotComplete with currentFabCode and countryOfOrigin (MSR 716638 - only CountryOfOrigin is set as lot script parameter)
    assert $avlotcomplete.lot_complete(lot, lottype: @@erpasmlot_f1_gg.sublottype, country: 'QA', currentfabcode: 'F8')
    assert_equal @@erpasmlot_f1_gg.sublottype, $av.lot_info(lot).sublottype
    assert_equal 'QA', $av.user_parameter('Lot', lot, name: 'CountryOfOrigin').value
    #
    # remove lot hold and send correct lotComplete message to ERP (wait for ERP answer and complete lot in AsmView)
    assert_equal 0, $av.lot_hold_release(lot, nil)
    assert $at.lot_complete_verify(lot, organization: 'FS1')
  end

  def test22_lot_complete_missing_parameters
    # MSR 714302: lots in a batch - one ok, one without partname/erpitem, one without customer, one without sublottype
    #
    # create new lot which is ok and will be shipped
    lot = $f1b2b.new_lot(@@erpasmlot_f1_gg, nwafers: @@nwafers)
    assert lot, "error creating lot"
    assert $f1b2b.lot_cleanup(lot, op: :lotshipdesig)
    # normally done by FabGUI
    $f1sv.lot_opelocate(lot, :last)
    $f1sv.lot_bankin(lot)
    refute_equal "", $f1sv.lot_info(lot).bank
    #
    lots = [lot, "UR30786.000", "UT00A.000", "UT00B.000"]
    assert $f1sv.user_parameter_set_verify('Lot', lots[1], 'ERPItem', '')
    assert $av.user_parameter_set_verify('Lot', lots[1], 'ERPItem', '')
    #
    # clean up lots in asmview
    lots.each {|lot|
      $log.info "using lot #{lot.inspect}"
      $avstb.stb(lot) unless $av.lot_exists?(lot)
      assert $av.lot_exists?(lot), "lot #{lot} does not exist in AsmView"
      assert_equal 0, $av.asm_diecount_req(lot, @@bins)
      assert $avb2b.lot_cleanup(lot, :op=>:lotshipdesig)
    }
    #
    # send lot complete standard message
    params = {:lottype=>['PO', 'PO', 'PO', ''], :customer=>['qgt', 'qgt', '', 'qgt'],
      :partname=>['*SHELBY21AE-M00-JB4', '', '*SHELBY21AE-M00-JB4', '*SHELBY21AE-M00-JB4']} #rganization: 'FS1'}
    assert $avlotcomplete.lot_complete(lots, params), "error sending lot complete message"
    #
    # verify results: first lot passed, others failed
    assert $avlotcomplete.verify_lot_complete(lot)
    # failed does not men ONHOLD, as jCAP may decide not to send the lot to ERP at all
    ##lots[1..-1].each {|lot| assert_equal 'ONHOLD', $av.lot_info(lot).status, "(av) wrong state of lot #{lot}"}
  end

  def test23_lot_complete_r
    # Fab1 setup
    @@lot = lot = $f1b2b.new_lot(@@erpasmlot_f1_r, nwafers: @@nwafers)
    assert lot, "error creating lot"
    # normally done by FabGUI
    assert_equal 0, $f1sv.lot_opelocate(lot, :last)
    $f1sv.lot_bankin(lot)
    refute_equal '', $f1sv.lot_info(lot).bank
    # AsmView setup
    assert $avstb.stb(lot)
    assert_equal 0, $av.asm_diecount_req(lot, @@bins)
    assert $avb2b.lot_cleanup(lot, op: :lotshipdesig)
    #
    # send lot_complete with wrong subcons (reflow missing), don't care if ERP accepts the lot
    assert $avlotcomplete.lot_complete(lot, subconid: "#{@@tk_r.tkbump}#{@@tk_r.tksort}", organization: 'FS1')
    # verify subcons are unchanged in AsmView
    assert_equal @@tk_r, $avb2b.lot_tkinfo(lot)
  end

  def test24_backend_complete
    # Fab1 setup
    lot = $f1b2b.new_lot(@@erpasmlot_f1_gg, nwafers: @@nwafers)
    assert lot, "error creating lot"
    # set SourceFabCode to F8
    assert $f1sv.user_parameter_set_verify('Lot', lot, 'SourceFabCode', 'F8')
    # normally done by FabGUI
    assert_equal 0, $f1sv.lot_opelocate(lot, :last)
    $f1sv.lot_bankin(lot)
    refute_equal '', $f1sv.lot_info(lot).bank
    # AsmView setup FabCode F1!
    assert $avstb.stb(lot, fabcode: 'F1')
    assert_equal 0, $av.asm_diecount_req(lot, @@bins)
    assert $avb2b.lot_cleanup(lot, op: :lotshipdesig)
    #
    # send lot_complete from Fab1
    assert $avlotcomplete.lot_complete(lot, country: 'US', currentfabcode: 'F1')
    assert_equal 'BackendComplete', $avb2b.lot_characteristics(lot).fabqualitycheck
    # send lot_complete from F8
    assert $avlotcomplete.lot_complete(lot)
    assert_equal 'Complete', $avb2b.lot_characteristics(lot).fabqualitycheck
  end

  def XXtest29_lot_complete_ERP_stress # --  BROKEN
    @@erpasmlot.lot = nil  # automatic lot ID
    lots = []
    lotprefix = unique_string("Q")
    @@nlots_stress.times {|i|
      $log.info "\n-- creating lot # #{i}"
      # Fab1 setup
      lot = $f1b2b.new_lot(@@erpasmlot_f1, nwafers: @@nwafers, lot: "#{lotprefix}.000")
      assert lot, "error creating a new test lot"
      # AsmView setup
      assert $avstb.stb(lot), "error STBing lot #{lot} in AsmView"
      ll = $av.lot_list(:lot=>lot)
      assert ll.size > 0, "lot #{lot} does not exist in AsmView"
      assert_equal 0, $av.asm_diecount_req(lot, @@bins)
      # prepare lot, put it ONHOLD and send lotComplete to AsmView
      assert $avb2b.lot_cleanup(lot, op: :lotshipdesig)
      assert_equal 0, $av.lot_hold(lot, @@holdreason)
      assert $avlotcomplete.lot_complete(lot)
      #
      lots << lot
      lotprefix = lotprefix.next
    }
    $log.info "using #{lots.size} test lots:\n#{lots.inspect}"
    $test_lots = lots
    #
    return
    # release lots by removing the hold
    lots.each {|l| assert $av.lot_hold_release(l, nil)}
    $log.info "waiting #{300 * 5}s for the ERP answer"
    sleep 300 * 5
    # verify lot status
    lots_good, lots_bad = [], []
    lots.each {|l|
      li = $av.lot_info(l)
      if li.states["Lot Inventory State"] == "InBank"
        lots_good << l
      else
        $log.warn "lot #{l} is not banked in in AsmView"
        lots_bad << l
      end
    }
    assert_equal 0, lots_bad.size, "#{lots_bad.size} out of #{lots.size} lots are not banked in:\n#{lots_bad.inspect}"
  end

  def test31_picking
    $setup_ok = false
    lot = @@lot
    # complete lot in case of missing ERP response to lotComplete
    ali = $av.lot_info(lot)
    unless ali.states["Lot Finished State"] == "COMPLETED"
      $av.lot_hold_release(lot, nil)
      assert_equal 0, $av.lot_bankin(lot)
    end
    # send pick messages and verify
    assert $at.pick_release_verify(lot), "error in pickRelease"
    assert $at.pick_confirm_verify(lot), "error in pickConfirm"
    $setup_ok = true
  end

  def test32_final_shipment
    assert $at.ship_confirm_verify(@@lot), "error in Ship Confirm message"
  end

  # Chip lots are currently not configured in ERP for Fab1 TK types

  def XXtest40_prepare_chip_lot
    $setup_ok = false
    #
    # Fab1 setup
    lot = $f1b2b.new_lot(@@erpasmlot_f1_gg, nwafers: @@nwafers)
    assert lot, "error creating lot"
    $log.info "using lot #{lot.inspect}"
    assert !$av.lot_exists?(lot), "lot #{lot} already exists in AsmView"
    assert $f1b2b.lot_cleanup(lot, op: :lotshipdesig)
    #
    # AsmView setup
    # send ship_complete trans message for AsmView lot creation
    res = $avship.ship_complete(lot, transstatus: "Trans-Running", fabcode: @@fabcode)
    assert res, "wrong reply from ERPShip interface: #{res.inspect}"
    assert $av.lot_exists?(lot), "lot #{lot} not created in AsmView"
#    $avstb.stb(lot) unless $av.lot_exists?(lot)
#    assert $av.lot_exists?(lot), "lot #{lot} does not exist in AsmView"
#    assert $avb2b.lot_cleanup(lot, op: :lotshipdesig)
    #
    # convert to chip lot
    assert_equal 0, $av.asm_change_lotcontent(lot, @@bins.first.inject(:+) + 10) # chip count must match sum of the first entry in @@bins
    assert_equal 0, $av.asm_diecount_req(lot, [@@bins.first])
    assert $av.user_parameter_set_verify("Lot", lot, "FabBaseLotID", lot)
    #
    # split off a child
    lc = "#{lot.split('.')[0]}.900"
    assert $av.asm_chiplot_split_with_id(lot, lc, 10), "error splitting off child lot #{lc.inspect}"
    assert_equal 0, $av.asm_diecount_req(lc, [[10]])
    @@lot = lot
    @@child = lc
    $setup_ok = true
  end

  def XXtest41_lot_complete_chip
    lot = "UR40249.000" #@@lot
    # move to end PD and bankin in SiView
    assert $f1b2b.lot_cleanup(lot, op: :lotshipdesig)
    # normally done by FabGUI
    $f1sv.lot_opelocate(lot, :last)
    $f1sv.lot_bankin(lot)
    refute_equal "", $f1sv.lot_info(lot).bank
    # move to end PD in AsmView
    assert $avb2b.lot_cleanup(lot, op: :lotshipdesig)
    # send lot_complete msg
    assert $at.lot_complete_verify(lot, organization: 'FS1')
  end

  def XXtest42_pick_confirm_chip
    $setup_ok = false
    lot = @@lot
    # prepare lot in case of missing ERP response
    ali = $av.lot_info(lot)
    unless ali.states["Lot Finished State"] == "COMPLETED"
      $av.lot_hold_release(lot, nil)
      assert_equal 0, $av.lot_bankin(lot)
    end
    assert $at.pick_release_verify(lot), "error in pickRelease"
    assert $at.pick_confirm_verify(lot), "error in pickConfirm"
    $setup_ok = true
  end

  def XXtest43_ship_confirm_chip
    assert $at.ship_confirm_verify(@@lot), "error in Ship Confirm message"
  end

  def XXtest51_lot_complete_chip_child
    $setup_ok = false
    lot = @@child
    # move to LOTSHIPD.1
    assert $avb2b.lot_cleanup(lot, op: :lotshipdesig)
    assert_equal 0, $av.asm_diecount_req(lot, @@bins)
    # send lotComplete msg
    assert $at.lot_complete_verify(lot, organization: 'FS1')
    $setup_ok = true
  end

  def XXtest52_pick_confirm_chip_child
    $setup_ok = false
    assert $at.pick_confirm_verify(@@child), "error in pickConfirm"
    $setup_ok = true
  end

  def XXtest53_ship_confirm_chip_child
    assert $at.ship_confirm_verify(@@child), "error in Ship Confirm message"
  end

  def self.after_all_passed
  end
end
