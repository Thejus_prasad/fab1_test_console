=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: Steffen Steidten, 2012-06-22
=end

require 'RubyTestCase'
require 'asmview'

# Test TxASM_SplitWaferLotWithIDReq
class TestASM001_ScriptParameters < RubyTestCase
  Description = "Test TxASM_SplitWaferLotWithIDReq"

  @@lot = "QA001.00"
  @@product = "ODYSSEYF1.F1"
  @@route = "F1TKY.01"

  @@script_param_i = "TestInheriting"
  @@script_param_m = "TestMerge"
  @@script_param_im = "TestInheritingAndMerge"


  def self.startup
    $av = AsmView::MM.new($env)
  end

  def self.after_all_passed
  end

  def test01_setup
    # get lot and wafer
    unless @@lot
      $av.lot_list(:lot=>"%", :status=>"Waiting").each {|l|
        li = $av.lot_info(l, :wafers=>true)
        $log.info "trying lot #{l}"
        next if li.wafers.size < 4
        @@lot = li.lot
        break
      }
    end
    $log.info "testing with lot #{@@lot}"
    $av.merge_lot_family(@@lot)
  end

  def test11_inherit_merge_script_params
    #
    # set params
    v1 = "QA#{Time.now.utc.iso8601}"
    assert $av.user_parameter_set_verify("Lot", @@lot, @@script_param_i, v1), "error setting script parameter"
    assert $av.user_parameter_set_verify("Lot", @@lot, @@script_param_m, v1), "error setting script parameter"
    assert $av.user_parameter_set_verify("Lot", @@lot, @@script_param_im, v1), "error setting script parameter"
    #
    # split
    lc = $av.asm_lot_split_with_id(@@lot, "", 2)
    assert lc, "error splitting lot #{@@lot}"
    $log.info "verifying inheritance on child lot #{lc.inspect}"
    #
    # check param_i on child
    cp = $av.user_parameter("Lot", lc, :name=>@@script_param_i)
    assert cp.valueflag, "error, valueflag not set: #{cp}"
    assert_equal v1, cp.value, "value mismatch, expected #{v1.inspect}, got #{cp.inspect}"
    # check param_m on child
    cp = $av.user_parameter("Lot", lc, :name=>@@script_param_m)
    assert !cp.valueflag, "error, valueflag is set: #{cp}"
    # check param_im on child
    cp = $av.user_parameter("Lot", lc, :name=>@@script_param_im)
    assert cp.valueflag, "error, valueflag not set: #{cp}"
    assert_equal v1, cp.value, "value mismatch, expected #{v1.inspect}, got #{cp.inspect}"
    #
    # merge child
    assert_equal 0, $av.asm_lot_merge(@@lot, lc), "error merging #{@@lot} with #{lc}"
  end

  def test21_no_merge_script_params
    # split lot
    v1 = $av.user_parameter("Lot", @@lot, :name=>@@script_param_i).value
    v2 = "QA#{Time.now.utc.iso8601}"
    # split
    lc = $av.asm_lot_split_with_id(@@lot, "", 2)
    assert lc, "error splitting lot #{@@lot}"
    $log.info "verifying merge scenarios with child lot #{lc.inspect}"
    #
    $log.info "verifying no merge with MERGE type parameter different"
    refute_equal 0, merge_test(@@lot, lc, v1, v2, v1), "lots must not be merged"
    #
    $log.info "verifying no merge with INHERITING/MERGE parameter different"
    refute_equal 0, merge_test(@@lot, lc, v1, v1, v2), "lots must not be merged"
    #
    $log.info "verifying no merge with INHERITING/MERGE and MERGE type parameters different"
    refute_equal 0, merge_test(@@lot, lc, v1, v2, v2), "lots must not be merged"
    #
    $log.info "verifying no merge with all parameters different"
    refute_equal 0, merge_test(@@lot, lc, v2, v2, v2), "lots must not be merged"
    #
    $log.info "verifying successful merge with INHERITING parameter different and MERGE parameters unset"
    assert_equal 0, merge_test(@@lot, lc, v2, nil, nil), "lots must be merged"
  end


  def merge_test(lot, lc, pi, pm, pim)
    ret = true
    ret &= $av.user_parameter_set_or_delete("Lot", lc, @@script_param_i, pi)
    ret &= $av.user_parameter_set_or_delete("Lot", lc, @@script_param_m, pm)
    ret &= $av.user_parameter_set_or_delete("Lot", lc, @@script_param_im, pim)
    ($log.warn "error setting script parameters on #{lc}"; return nil) unless ret
    return $av.asm_lot_merge(lot, lc)
  end


end
