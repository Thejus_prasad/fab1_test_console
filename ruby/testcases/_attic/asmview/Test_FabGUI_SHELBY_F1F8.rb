=begin 
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.
    
Author: Daniel Steger, 2013-05-28

Version: FabGUI 11.2, BTFTurnkey 12.11
=end

require 'asmviewtest'
require 'fabguictrl'
require "RubyTestCase"


# Test FabGUI Non-Std shipment F1->F8
class Test_FabGUI_SHELBY_F1F8 < RubyTestCase

  @@backup_bank = "O-BACKUPOPER"  # F8
  @@return_bank = "W-BACKUPOPER"  # F1
  @@src_fab_code = "F8"
  
  tk_j_gggg = SiView::MM::LotTkInfo.new("J", "GG", "GG")
  @@erpasmlot = ERPMES::ErpAsmLot.new(nil, tk_j_gggg, "SHELBY3CC-J01", nil, "SHELBY3CC.01", "FF-28LPQ-45M.01", "qgt", "PO", :tkbumpshipinit)
  @@lot =  nil

  def setup
    $at = AsmView::Test.new($env, :f7=>false, :erpshipment=>false, :btf=>true) unless $at and $at.env == $env
    assert $at
    $av = $at.av
    $avb2b = $at.avb2b
    $f1b2b = $at.f1b2b
    $testdbbtf = $at.dbbtf
    $f1sv = $at.f1sv
    $f8sv = $at.f8sv
    $f8b2b = $at.f8b2b
    super(:nosiview=>true)
  end
  
  def test11_prepare_lot_f8_turnkey
    $setup_ok = false
    #
    # create lot 
    lot = @@lot = $f8b2b.new_lot(@@erpasmlot)
    assert @@lot, "error creating lot"
    $log.info "using lot #{lot.inspect}"
    #
    # verify UDATA
    li = $f8sv.lot_info(lot)
    udata = $f8sv.user_data_productgroup(li.productgroup)
    assert_equal "FF", udata["RouteType"], "UDATA RouteType for ProductGroup #{li.productgroup} must be 'FF'"
    #### set source fabcode
    assert $f8sv.user_parameter_set_verify("Lot", lot, "SourceFabCode", @@src_fab_code)
    
    #init lot notes
    assert $f8sv.lot_note_register lot, "FabGUI.Shipping.ProcessedML", "{\"fab\":\"#{@@src_fab_code}\",\"maskLayers\":7}", :user=>"X-FABGUI", :password=>"uJitvQ"
    assert $f8sv.lot_note_register lot, "FabGUI.Shipping.ExternalML", "{\"fab\":\"#{@@src_fab_code}\",\"maskLayers\":20}", :user=>"X-FABGUI", :password=>"uJitvQ"
    assert $f8sv.lot_note_register lot, "FabGUI.Shipping.RawWaferType", "300MM_PRIME_BULK", :user=>"X-FABGUI", :password=>"uJitvQ"
        
    #
    # start backup operation
    assert $f8b2b.lot_locate(lot, @@erpasmlot.op)
    assert $f8sv.backup_prepare_send_receive(lot, $f1sv, :backup_bank=>@@backup_bank)

    #
    # set lot script parameters
    assert $f1b2b.lot_set_params(lot, @@erpasmlot)
    # process lot on the BTF route, including binning data creation
    assert $at.process_btfturnkey(lot, :fabcode=>@@src_fab_code), "error processing lot #{lot} in BTF"
    #
    $setup_ok = true
  end
  
  def test12_return_lot_from_f1
    @@ship_time = Time.now
    puts "\n\nShip lot #{@@lot.inspect} via FabGUI Non-Std Shipment and press return to start the verification."
    gets
    
    # verify lot notes
    notes = $f1sv.lot_notes(@@lot)
    notes = notes.select {|note| note.user == "X-FABGUI" && note.timestamp > $f1sv.siview_timestamp(@@ship_time)}
    assert notes.find {|note| note.title == "FabGUI.Shipping.ProcessedML"}, "should have Fab1 specific mask layers"
  end

  def Xtest13_verify_lot_f1
    
    
    # send to backup operation
    assert $f1sv.backup_prepare_send_receive(@@lot, $f8sv, :backup_bank=>nil, :backup_opNo=>nil)
    #
    # set lot script parameters
    assert $f8b2b.lot_set_params(lot, @@erpasmlot)
    # process lot on the BTF route, including binning data creation
    assert $at.process_btfturnkey(lot, :fabcode=>@@src_fab_code), "error processing lot #{lot} in BTF"
    #
    # return lot to source location
    assert $f8sv.backup_prepare_return_receive(lot, $f1sv, :return_bank=>@@return_bank, :src_carrier=>'9%')

  end
  
  def Xtest12_fabgui_shipping
    $setup_ok = false
    # FabGUI settings
    $shippingctrl = FabGUI::Shipping.new("f8.#{$env}")
    props = {"AsmView.LotComplete.Enabled"=>"true", "BtfTurnkey.SortUpdate.Enabled"=>"true", "SortPostProcessorEI.Enabled"=>"true", "SortPostProcessorEI.Queue"=>"CEI.SHIPDATA8_REQUEST02"}
    #assert $shippingctrl.verify_runtime_values(props), "wrong FabGUI properties"
    assert $shippingctrl.set_runtime_values(props), "wrong FabGUI properties"
    #
    puts "\n\nShip lot #{@@lot.inspect} via FabGUI and press return to start the verification."
    gets
    $setup_ok = true
  end
  
  def Xtest13_verify_lot
    lot = @@lot
    $log.info "waiting for bank move in Fab8"
    sleep 120
    #
    $log.info "verify SiView status"
    fli = $f8sv.lot_info(lot)
    assert_equal "COMPLETED", fli.status
    assert_equal "O-ENDFG", fli.bank
    #
    $log.info "verify AsmView status"
    alc = $avb2b.lot_characteristics(lot) 
    puts alc.pretty_inspect
    assert_equal "Complete", alc.fabqualitycheck
  end

  def Xtest14_verify_lot_ERP
    # on working ERP connection, 5 minuts later:
    $log.info "waiting 5 minutes for ERP to response"
    sleep 310
    $log.info "verify AsmView status"
    ali = $av.lot_info(@@lot)
    assert_equal "COMPLETED", ali.status
    assert_equal "WINV", ali.bank
  end
  
 
end