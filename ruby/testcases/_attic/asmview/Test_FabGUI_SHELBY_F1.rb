=begin 
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.
    
Author: Steffen Steidten, 2012-11-06

Version: FabGUI 10.8, BTFTurnkey 12.11
=end

require 'asmviewtest'
require 'fabguictrl'
require 'RubyTestCase'


# Test FabGUI Shipping for SHELBY Lots in Conjunction with BTF Turnkey
class Test_FabGUI_SHELBY_F1 < RubyTestCase

  tk_j_gggg = SiView::MM::LotTkInfo.new("J", "GG", "GG")
  @@erpasmlot = ERPMES::ErpAsmLot.new(nil, tk_j_gggg, '*SHELBY21AE-M00-JB4', "6010000543*01:01", 'SHELBY21BTF.QA', 'C02-TKEY-ACTIVE.01', 'qgt', 'PO')
  #@@erpasmlot = ERPMES::ErpAsmLot.new(nil, tk_j_gggg, "*SHELBY21AE-M00-JB4", "6010000543*01:01", "SHELBY21AE.B4", "C02-1431.01", "qgt", "PO")
  @@lot =  nil

  def self.startup
    $at = AsmView::Test.new($env, f1: true, btf: true) unless $at && $at.env == $env
    $av = $at.av
    $avb2b = $at.avb2b
    $f1b2b = $at.f1b2b
    $testdbbtf = $at.dbbtf
    $f1sv = $at.f1sv
  end

  def self.after_all_passed
    $f1sv.delete_lot_family(@@lot)
  end


  def test00_setup
    $setup_ok = false
    assert $at.check_connectivity
    $setup_ok = true
  end

  def test10_prepare_lot
    $setup_ok = false
    #
    # create lot 
    assert @@lot = $f1b2b.new_lot(@@erpasmlot), "error creating lot"
    $log.info "using lot #{@@lot.inspect}"
    #
    # verify UDATA
    li = $f1sv.lot_info(@@lot)
    udata = $f1sv.user_data_productgroup(li.productgroup)
    assert_equal 'FF', udata['RouteType'], "wrong UDATA RouteType for ProductGroup #{li.productgroup}"
    #
    # process lot on the BTF route, including binning data cration
    assert $at.process_btfturnkey(@@lot, fabcode: 'F1'), "error processing lot #{@@lot} in BTF"
    #
    $setup_ok = true
  end
  
  def test11_fabgui_shipping
    require 'fabguictrl'
    # FabGUI settings
    $fguictrl = FabGUI::ProcessCtrl.new('itdc')    # FabGUI::Shipping.new("f1.#{$env}")
    props = {'SHIPPING.AsmView.LotComplete.Enabled'=>'true', 
      'SHIPPING.BtfTurnkey.SortUpdate.Enabled'=>'true', 'SHIPPING.SortPostProcessorEI.Enabled'=>'false'}
    assert $fguictrl.set_runtime_values(props), "error setting FabGUI properties"
    puts "\n\nShip lot #{@@lot.inspect} via FabGUI and press RETURN to continue"
    gets
  end
  
  def test12_verify_lot
    $log.info "waiting 2 minutes for bank move in Fab1"
    sleep 120
    #
    $log.info "verify SiView status"
    fli = $f1sv.lot_info(@@lot)
    assert_equal "COMPLETED", fli.status
    assert_equal "OX-ENDFG", fli.bank
    #
    $log.info "verify AsmView status"
    alc = $avb2b.lot_characteristics(@@lot) 
    puts alc.pretty_inspect
    assert_equal "Complete", alc.fabqualitycheck
  end
  
  def test13_verify_lot_ERP
    # on working ERP connection, 5 minuts later:
    $log.info "waiting 5 minutes for ERP to respond"
    sleep 310
    $log.info "verify AsmView status"
    ali = $av.lot_info(@@lot)
    assert_equal "COMPLETED", ali.status
    assert_equal "WINV", ali.bank
  end
  
end