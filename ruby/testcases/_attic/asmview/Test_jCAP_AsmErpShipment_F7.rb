=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: Steffen Steidten, 2012-06-22
Version: 13.10
=end

require 'RubyTestCase'
require 'asmviewtest'

# Test the AsmErpShipment job for Fab7
class Test_jCAP_AsmErpShipment_F7 < RubyTestCase
  
  @@fabcode = "F7"
  @@tkbump = "CT"
  @@tksort = "ST"
  @@tkreflow = @@tkbump
  @@tkassy = "AK"
  @@tk_j = SiView::MM::LotTkInfo.new("J", @@tkbump, @@tksort)
  @@tk_k = SiView::MM::LotTkInfo.new("K", @@tkbump, @@tksort)
  @@tk_m = SiView::MM::LotTkInfo.new("M", @@tkbump, @@tksort, nil, @@tkassy)
  @@tk_aa = SiView::MM::LotTkInfo.new("QE-AA", @@tkbump)
##  @@erpasmlot_f7 = ERPMES::ErpAsmLot.new(nil, @@tk_j, "ODYSSEY65V20-J01", "8007925:01:02", "ODYSSEY65V20.01", "PCMT10LPODY20.1", "7GFS", "TC")  
  @@erpasmlot_f7 = ERPMES::ErpAsmLot.new(nil, @@tk_k, "ODYSSEY65V20-K01", "8007925:01:02", "ODYSSEY65V20.01", "PCMT10LPODY20.1", "7GFS", "TC")  
  @@erpasmlot_f7_chip = ERPMES::ErpAsmLot.new(nil, @@tk_m, "ODYSSEY65V20-M01", "8007925:01:02", "ODYSSEY65V20.01", "PCMT10LPODY20.1", "7GFS", "TC")  
  @@erpasmlot_f7_sltnonship = ERPMES::ErpAsmLot.new(nil, @@tk_m, "ODYSSEY65V20-M01", "8007925:01:02", "ODYSSEY65V20.01", "PCMT10LPODY20.1", "7GFS", "PX")  
  @@lot = nil  # will be set in test12 and test21 

  # parameter for lot_complete interface tests
  @@bins = [[359,51,222],[234,34,342],[58,53,46],[245,56,67],[124,89,425],[456,45,130],[1056,46,367],[234,34,345],[465,46,56]]
  @@nwafers = @@bins.size

  
  def self.startup
    super(nosiview: true)
    $at = AsmView::Test.new($env, f1: false, f8: false, btf: false)
    $av = $at.av
    $avstb = $at.avstb
    $avb2b = $at.avb2b
    $avship = $at.avship
    $avlotcomplete = $at.avlotcomplete
    $f1sv = $at.f1sv
    $f1b2b = $at.f1b2b
    $f7sv = $at.f7sv
    $f7b2b = $at.f7b2b
  end
  
  def test01_setup
    $setup_ok = false
    assert $at.check_connectivity, "at least one component is not connected"
    $setup_ok = true
  end
  
  # Shipment Trans (Turnkey)
  
  def test11_std_shipment_trans_wrong_lot
    $setup_ok = false
    assert $at.ship_complete_wrong_lot(fabcode: @@fabcode), "error in lot rejection"
    $setup_ok = true
  end
  
  def test12_std_shipment_trans_new_lot
    $setup_ok = false
    # ship a lot not existing in AsmView
    # create a new Fab7 lot, move it to the TK start operation
    @@lot = lot = $f7b2b.new_lot(@@erpasmlot_f7, nwafers: @@nwafers)
    assert lot, "error creating a new Fab 7 lot"
    assert !$av.lot_exists?(lot), "the new fab lot #{lot} already exists in AsmView"
    $log.info "using lot #{lot.inspect}"
    assert $f7b2b.lot_locate(lot, :tkbranch)
    #
    # send ship_complete message
    assert $avship.ship_complete(lot, transstatus: "Trans-Running", fabcode: @@fabcode)
    #
    # verify AsmView lot parameters
    assert $av.lot_exists?(lot), "lot #{lot} not created in AsmView"
    assert $avstb.lot_tkey_started?(lot, :bump)
    lca =  $avstb.lot_characteristics(lot)
    assert_equal $f7b2b.lot_characteristics(lot).tkinfo, lca.tkinfo
    assert_equal @@erpasmlot_f7.erpitem, lca.erpitem
    $setup_ok = true
  end
    
  def test13_std_shipment_trans_new_lot_dies
    # ship a lot not existing in AsmView
    # create a new Fab7 lot with die order, move it to the TK start operation
    lot = $f7b2b.new_lot(@@erpasmlot_f7, nwafers: @@nwafers)
    assert lot, "error creating a new Fab 7 lot"
    assert !$av.lot_exists?(lot), "the new fab lot #{lot} already exists in AsmView"
    $log.info "using lot #{lot.inspect}"
    assert $f7b2b.lot_set_order(lot, @@erpasmlot_f7.order.sub(':', '*'))
    assert $f7b2b.lot_locate(lot, :tkbranch)
    #
    # send ship_complete message
    assert $avship.ship_complete(lot, transstatus: "Trans-Running", fabcode: @@fabcode)
    #
    # verify AsmView lot parameters
    assert $av.lot_exists?(lot), "lot #{lot} not created in AsmView"
    assert $avstb.lot_tkey_started?(lot, :bump)
    lca =  $avstb.lot_characteristics(lot)
    assert_equal $f7b2b.lot_characteristics(lot).tkinfo, lca.tkinfo
    assert_equal "*#{@@erpasmlot_f7.erpitem}", lca.erpitem
  end
    
  def test14_std_shipment_trans_existing_lot
    # ship a lot existing in AsmView, using the lot created in test12 again
    lot = @@lot
    assert lot, "no test lot, run test12 to create one"
    assert $av.lot_exists?(lot), "lot #{lot} does not exist in #{$av}"
    assert $f7sv.lot_exists?(lot), "lot #{lot} does not exist in #{$f7sv}"
    #
    # send ship_complete message
    res = $avship.ship_complete(lot, transstatus: "Trans-Running", fabcode: @@fabcode)
    assert res, "wrong reply from ERPShip interface: #{res.inspect}"
    #
    # verify AsmView lot parameters
    assert $avstb.lot_tkey_started?(lot, :bump) 
  end
  

  # LotComplete interface

  def test21_lot_complete
    $setup_ok = false
    unless @@lot
      @@lot = $f7b2b.new_lot(@@erpasmlot_f7, nwafers: @@nwafers)
      assert @@lot, "error creating fab lot"
      assert !$av.lot_exists?(@@lot), "lot #{@@lot} already exists in AsmView"
      assert $avstb.stb(@@lot, fabcode: @@fabcode), "unable to stb lot #{@@lot}"
    end
    lot = @@lot
    assert $avb2b.lot_cleanup(lot, op: :lotshipdesig)
    assert_equal 0, $av.asm_diecount_req(lot, @@bins)
    assert $f7b2b.lot_cleanup(lot, op: :qacheck)
    #
    # Fab7ErpShipment sends the lot complete msg, wait for AsmView jCAP to set the FabQualityCheck param
    #   and send the lotCompletion message to ERP, wait for ERP msg
    assert $avlotcomplete.verify_lot_complete(lot)
    $setup_ok = true
  end
  
  def test29_lot_complete_nonshipable_lottype
    unless @@lot
      @@lot = $f7b2b.new_lot(@@erpasmlot_f7_sltnonship, nwafers: @@nwafers)
      assert @@lot, "error creating fab lot"
      assert !$av.lot_exists?(@@lot), "lot #{@@lot} already exists in AsmView"
      assert $avstb.stb(@@lot, fabcode: @@fabcode), "unable to stb lot #{@@lot}"
    end
    lot = @@lot
    assert $avb2b.lot_cleanup(lot, op: :lotshipdesig)
    assert_equal 0, $av.asm_diecount_req(lot, @@bins)
    assert $f7b2b.lot_cleanup(lot, op: :qacheck)
    #
    # Fab7ErpShipment sends the lot complete msg, wait for AsmView jCAP to set the FabQualityCheck param
    #   but will not send the lotCompletion message to ERP
    assert !$avlotcomplete.verify_lot_complete(lot), "lot #{@@lot} is shipped to ERP"    
  end  
  
  def test31_picking
    $setup_ok = false
    assert $at.pick_confirm_verify(@@lot), "error in pickConfirm"
    $setup_ok = true
  end
  
  def test32_final_shipment
    assert $at.ship_confirm_verify(@@lot), "error in Ship Confirm message"
  end
  
  # Chip Lots
  
  def test40_prepare_chip_lot
    $setup_ok = false
    lot = @@lot = $f7b2b.new_lot(@@erpasmlot_f7_chip)
    assert lot, "error creating fab lot"
    $log.info "using lot #{lot.inspect}"
    assert !$av.lot_exists?(lot), "lot #{lot} already exists in AsmView"
    assert $f7b2b.lot_cleanup(lot, op: :tkbumpshipinit)
    #
    # send ship_complete trans message for AsmView lot creation
    res = $avship.ship_complete(lot, transstatus: "Trans-Running", fabcode: @@fabcode)
    assert res, "wrong reply from ERPShip interface: #{res.inspect}"
    assert $av.lot_exists?(lot), "lot #{lot} not created in AsmView"
    #
    # convert to chip lot
    assert_equal 0, $av.asm_change_lotcontent(lot, @@bins.first.inject(:+) + 10) # chip count must match sum of the first entry in @@bins
    assert_equal 0, $av.asm_diecount_req(lot, [@@bins.first])
    assert $av.user_parameter_set_verify("Lot", lot, "FabBaseLotID", lot)
    #
    # split off a child
    lc = "#{lot.split('.')[0]}.900"
    assert $av.asm_chiplot_split_with_id(lot, lc, 10), "error splitting off child lot #{lc.inspect}"
    assert_equal 0, $av.asm_diecount_req(lc, [[10]])
    @@lot = lot
    @@child = lc
    $setup_ok = true
  end
  
  def test41_lot_complete_chip
    $setup_ok = false
    lot = @@lot
    # move lot to LOTSHIPD
    assert $f7b2b.lot_cleanup(lot, op: :lotshipdesig)
    assert $avb2b.lot_cleanup(lot, op: :lotshipdesig)
    # send lot_complete msg (currently FabQualityCheck is set manually for Fab7)
    assert $at.lot_complete_verify(lot)
    $setup_ok = true
  end
  
  def test42_pick_confirm_chip
    $setup_ok = false
    assert $at.pick_confirm_verify(@@lot), "error in pickConfirm"
    $setup_ok = true
  end
  
  def xtest43_ship_confirm_chip
    assert $at.ship_confirm_verify(@@lot), "error in Ship Confirm message"
  end
  
  def test51_lot_complete_chip_child
    $setup_ok = false
    # move to LOTSHIPD
    assert $avb2b.lot_cleanup(@@child, op: :lotshipdesig)
    # send lotComplete msg
    assert $at.lot_complete_verify(@@child)
    $setup_ok = true
  end
  
  def test52_pick_confirm_chip_child
    $setup_ok = false
    assert $at.pick_confirm_verify(@@child), "error in pickConfirm"
    $setup_ok = true
  end
  
  def test53_ship_confirm_chip_child
    assert $at.ship_confirm_verify(@@child), "error in Ship Confirm message"
  end
  
  # quality tags
  
  def test61_qualitytags
    # shipping refused in all cases with an N code (all except the first)
    qtags = ["GDPI7 GD13D", "NPI7", "NPI7 GD13D", "GDPI7 N13D"]
    lots = []
    qtags.each {|qtag|
      # create lot in SiView and AsmView
      lot = $f7b2b.new_lot(@@erpasmlot_f7, qualitytag: qtag)
      assert lot, "error creating fab lot"
      assert !$av.lot_exists?(lot), "lot #{lot} already exists in AsmView"
      $log.info "using lot #{lot.inspect} with QualityTag #{qtag.inspect}"
      lots << lot
      #
      assert $f7b2b.lot_cleanup(lot, op: :qacheck)
      assert $avstb.stb(lot, fabcode: @@fabcode)
      assert $avstb.lot_locate(lot, :lotshipdesig)
    }
    $log.info "using lots #{lots.inspect}"
    #
    # verify first lot gets banked in
    assert $avlotcomplete.verify_lot_complete(lots.first)
    #
    # verify all other lots have not moved and are ONHOLD in Fab SiView
    sleep 60
    lots[1..-1].each {|lot|
      assert $f7b2b.verify_lot_state(lot, :qacheck, 'ONHOLD')
      assert $avb2b.verify_lot_state(lot, :lotshipdesig, 'Processing')  # might be Waiting in later implementations
    }
  end
  
  def test62_chip_inheritance
    # quality tag for "good"
    qtag = "GD13D"
    # create lot in SiView and AsmView
    lot = $f7b2b.new_lot(@@erpasmlot_f7_chip, qualitytag: qtag)
    assert lot, "error creating fab lot"
    assert !$av.lot_exists?(lot), "lot #{lot} already exists in AsmView"
    $log.info "using lot #{lot.inspect}"
    # split lot
    child1 = $f7sv.lot_split(lot, 2)
    assert child1, "error splitting lot"
    # put lots on TK route
    [lot, child1].each {|l|
      assert $f7b2b.lot_cleanup(l, op: :tkbumpshipinit)
      assert $avstb.stb(l, fabcode: @@fabcode)
      # verify qualitytag
      assert_equal qtag, $av.user_parameter("Lot", l, name: "QualityTag").value, "wrong QualityTag"
    }
    #
    # convert to chip lots
    [lot, child1].each {|l|
      assert_equal 0, $av.asm_change_lotcontent(l, 34567)
      assert $av.user_parameter_set_verify("Lot", l, "FabBaseLotID", l)
      assert_equal qtag, $av.user_parameter("Lot", l, name: "QualityTag").value, "wrong QualityTag"
    }
    # split a child off of the parent lot and verify the QualityTag
    lc = "#{lot.split('.')[0]}.900"
    assert_equal lc, $av.asm_chiplot_split_with_id(lot, lc, 10), "error splitting lot #{lot.inspect}"
    assert_equal qtag, $av.user_parameter("Lot", lc, name: "QualityTag").value, "wrong QualityTag"
    # split a grandchild off of the first child and verify the QualityTag
    gc1 = lc.next
    assert_equal gc1, $av.asm_chiplot_split_with_id(child1, gc1, 10), "error splitting lot #{child1.inspect}"
    assert_equal qtag, $av.user_parameter("Lot", gc1, name: "QualityTag").value, "wrong QualityTag"
    # split a grand-grandchild off of the last chip child and verify the QualityTag
    gc2 = gc1.next
    assert_equal gc2, $av.asm_chiplot_split_with_id(gc1, gc2, 10), "error splitting lot #{gc1.inspect}"
    assert_equal qtag, $av.user_parameter("Lot", gc2, name: "QualityTag").value, "wrong QualityTag"
    #
    # move to Merge PD for lot complete
    [lot, child1].each {|l|
      assert $f7b2b.lot_locate(l, :qacheck)
      assert $avstb.lot_locate(l, :lotshipdesig)
    }
    #
    [lot, child1].each {|l| assert $avlotcomplete.verify_lot_complete(l)}
  end

  def self.after_all_passed
  end
end
