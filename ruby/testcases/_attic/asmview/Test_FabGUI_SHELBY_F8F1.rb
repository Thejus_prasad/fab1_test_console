=begin 
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.
    
Author: Daniel Steger

Version: Fab8 Turnkey Shipment (OS192)
=end

require 'asmviewtest'
require 'fabguictrl'
require "RubyTestCase"



# Test FabGUI Shipping for F8 TURNKEY Lots in Conjunction with Backup Operation and BTF Turnkey
class Test_FabGUI_SHELBY_F8F1 < RubyTestCase

  ERP14 = false # new ERP Interface is disabled
  @@backup_bank = "O-BACKUPOPER"  # F8
  @@return_bank = "W-BACKUPOPER"   # F1
  @@manual_receive = false # Manually receive the lots in FabGUI LotStart GUI
  @@asm_ship = true # Ship using ASMView
  
  class << self
    def startup
      $at = AsmView::Test.new($env, f7: false, erpshipment: true, btf: true) unless $at and $at.env == $env
      $av = $at.av
      $avb2b = $at.avb2b
      $f1b2b = $at.f1b2b
      $testdbbtf = $at.dbbtf
      $f1sv = $at.f1sv
      $f8sv = $at.f8sv
      $f8b2b = $at.f8b2b
      $fguictrl = FabGUI::ProcessCtrl.new("f8stag") unless $fguictrl and $fguictrl.env = $env
      # TODO: add Fab1 Config: "SHIPPING.BtfTurnkey.SortUpdate.SkipForTurnKeyTypes"=>"L",
      ENV['TZ'] = "UTC+5" # Eastern Standard Time is 5h back from UTC
      super(:nosiview=>true)
    end
  end
  
  test "TC01 THORIN lot with Turnkey Type L - Bump only" do
    $erpasmlot = ERPMES::ErpAsmLot.new(nil, SiView::MM::LotTkInfo.new("L", "GG"), "BAGERA11MB-LA1", nil, "BAGERA11MB.A1", "FF-BAGERA1-TN1.01", "gf", "PO", :tkbumpshipdispo)
    lot = prepare_lot_f8($erpasmlot)
    assert $fguictrl.set_runtime_values({"SHIPPING.BtfTurnkey.SortUpdate.SkipForTurnKeyTypes"=>"L"}), "failed to check FabGUI setting"
    
    subcon_shipment_to_btf(lot, $erpasmlot, backup_complete: true, nosort: true)
    fabgui_shipping(lot)
  end
  
  test "TC02 THORIN lot with Turnkey Type S - Sort only" do
    $erpasmlot = ERPMES::ErpAsmLot.new(nil, SiView::MM::LotTkInfo.new("S", nil, "GG"), "THORIN11MA-JA2", nil, "THORIN11MA.A2", "FF-THORIN1.01", "gf", "PO", :tksortshipdispo)
    lot = prepare_lot_f8($erpasmlot)
    subcon_shipment_to_btf(lot, $erpasmlot, backup_complete: true)
    fabgui_shipping(lot)
  end
    
  test "TC03 SHELBY lot with Turnkey Type J - Bump and Sort" do
    # bump and sort lot
    $erpasmlot = ERPMES::ErpAsmLot.new(nil, SiView::MM::LotTkInfo.new("J", "GG", "GG"), "ENGINEERING-U00", nil, "SHELBY3CC.01", "FF-28LPQ-45M.01", "qgt", "PO", :tkbumpshipdispo)
    lot = prepare_lot_f8($erpasmlot)
    # update erpitem for engineering
    $erpasmlot.erpitem = "SHELBY3CC-J01"
    subcon_shipment_to_btf(lot, $erpasmlot, backup_complete: true)
    fabgui_shipping(lot)
  end
    
  test "TC04 GIMLI2 lots with Turnkey Type J - split and merge cousin wafers in BTF" do
    # bump and sort lot
    $erpasmlot = ERPMES::ErpAsmLot.new(nil, SiView::MM::LotTkInfo.new("J", "GG", "GG"), "GIMLI21MA-JA0", nil, "GIMLI21MA.A0", "FF-GIMLI2.01", "qgt", "PO", :tkbumpshipdispo)
    lot = prepare_lot_f8($erpasmlot)
    
    rcv_lots = subcon_shipment_to_btf_with_split(lot, $erpasmlot, backup_complete: true)
    
    fabgui_shipping(rcv_lots)
  end
  
  #TODO ASMView update not included
  test "TC05 Part Conversion" do
    # bump and sort lot
    $erpasmlot = ERPMES::ErpAsmLot.new(nil, SiView::MM::LotTkInfo.new("J", "GG", "GG"), "ENGINEERING-U00", nil, "SHELBY3CC.01", "FF-28LPQ-45M.01", "qgt", "PO", :tkbumpshipdispo)
    lot = prepare_lot_f8($erpasmlot)
    # update erpitem for engineering
    $erpasmlot.erpitem = "SHELBY3CC-J01"
    subcon_shipment_to_btf(lot, $erpasmlot, partconv: "GIMLI21MA.A0", new_erpitem: "GIMLI21MA-JA0", backup_complete: true)
    fabgui_shipping(lot)
  end
  
  test "TC06 GIMLI2 child lot is scrapped in BTF" do
    # bump and sort lot
    $erpasmlot = ERPMES::ErpAsmLot.new(nil, SiView::MM::LotTkInfo.new("J", "GG", "GG"), "GIMLI21MA-JA0", nil, "GIMLI21MA.A0", "FF-GIMLI2.01", "qgt", "PO", :tkbumpshipdispo)
    lot = prepare_lot_f8($erpasmlot)
    
    rcv_lot = subcon_shipment_to_btf(lot, $erpasmlot, scrap: "child", backup_complete: true)
    fabgui_shipping(rcv_lot)
  end
  
  test "TC07 GIMLI2 parent lot is scrapped in BTF" do
    # bump and sort lot
    $erpasmlot = ERPMES::ErpAsmLot.new(nil, SiView::MM::LotTkInfo.new("J", "GG", "GG"), "GIMLI21MA-JA0", nil, "GIMLI21MA.A0", "FF-GIMLI2.01", "qgt", "PO", :tkbumpshipdispo)
    lot = prepare_lot_f8($erpasmlot)
    
    rcv_lot = subcon_shipment_to_btf(lot, $erpasmlot, scrap: "parent", backup_complete: true)
    fabgui_shipping(rcv_lot)
  end
  
  test "TC08 GIMLI2 lot is scrapped in BTF" do
    # bump and sort lot
    $erpasmlot = ERPMES::ErpAsmLot.new(nil, SiView::MM::LotTkInfo.new("J", "GG", "GG"), "GIMLI21MA-JA0", nil, "GIMLI21MA.A0", "FF-GIMLI2.01", "qgt", "PO", :tkbumpshipdispo)
    lot = prepare_lot_f8($erpasmlot)
    
    subcon_shipment_to_btf(lot, $erpasmlot, scrap: "all", backup_complete: true)
  end
  
  test "TC09 GARFIELD lot with Turnkey Type R - Bump and Reflow" do
    # bump and sort lot
    $erpasmlot = ERPMES::ErpAsmLot.new(nil, SiView::MM::LotTkInfo.new("R", "GG", "GG"), "GARFLD11TA-RA0", nil, "GARFLD11TA.01", "FF-BAGERA1-TN1.01", "amd", "PO", :tkbumpshipdispo)
    lot = prepare_lot_f8($erpasmlot)
    subcon_shipment_to_btf(lot, $erpasmlot, backup_complete: true)
    fabgui_shipping(lot)
  end
  
  
  def prepare_lot_f8(erpasmlot)
    #
    # create lot 
    lot = $f8b2b.new_lot(erpasmlot, dies: 777) #todo override die count so we can check that Fab1 synchronizes with Fab8
    assert lot, "error creating lot"
    $log.info "using lot #{lot.inspect}"
    #
    # verify UDATA
    li = $f8sv.lot_info(lot)
    udata = $f8sv.user_data_productgroup(li.productgroup)
    assert_equal "FF", udata["RouteType"], "UDATA RouteType for ProductGroup #{li.productgroup} must be 'FF'"
    #
    # start backup operation
    assert $f8b2b.lot_locate(lot, erpasmlot.op)
    # set source fabcode
    assert $f8sv.user_parameter_set_verify("Lot", lot, "SourceFabCode", "F8")
    
    return lot
  end
  
  def subcon_shipment_to_btf(lot, erpasmlot, params={})
    # FabGUI settings
    assert $fguictrl.set_runtime_values({
      "SHIPPING.YmsDataTransfer.PDList"=>"TKEYBUMPSHIPDISPO.01:TurnkeySubconIDBump;TKEYSORTSHIPDISPO.01:TurnkeySubconIDSort"
    }), "config is not correct"
    
    ship_time = Time.now
    puts "\n\nShip lot #{lot.inspect} via FabGUI SubCon Shipment and press return to start the verification."
    puts "Subcon Ship time: #{ship_time}"
    gets
    
    # lot is in non-pro bank
    assert_equal "NonProBank", $f8sv.lot_info(lot).status, "lot should be in non-pro bank"
    assert _verify_notes(lot, ship_time)

    # send to Fab1
    $f8sv.prepare_for_backup(lot, backup_bank: nil)    
    if @@asm_ship
      assert $at.ship_subcon_verify(lot, :bumpb, :fabcode=>"F8"), "1st leg shipment failed for #{lot.inspect}"
      assert wait_for { $f8sv.lot_info(lot, backup: true).backup.transfering }, "lot was not sent to backup"
    else
      assert_equal 0, $f8sv.backupop_lot_send(lot, $f1sv, backup_bank: nil)
    end
    if @@manual_receive
      puts "\n\nReceive lot #{lot.inspect} via FabGUI in Fab1 and press return to start the verification."    
      gets
    else
      assert_equal 0, $f1sv.backupop_lot_send_receive(lot, $f8sv, bak_carrier:"F%", carrier_category: "FOSB"), "failed receive lot"
      beol_foup = $f1sv.carrier_list(carrier_category: 'BEOL', status: 'AVAILABLE', empty: true).first
      assert_equal 0, $f1sv.wafer_sort_req(lot, beol_foup), "failed to move to BEOL carrier"
    end
        
    # set lot script parameters
    assert $f1b2b.lot_set_params(lot, erpasmlot) 
    # process lot on the BTF route, including binning data creation
    assert $at.process_btfturnkey(lot, params.merge({:fabcode=>"F8", :binning_data=>Hash[25.times.collect {|i| [i,[125,50,25+i]]}]})), 
      "error processing lot #{lot} in BTF"
    
    if params[:scrap]
      if params[:scrap] != "all"
        child = $f1sv.lot_split(lot, 5)
      end
      scrap_lot = (params[:scrap] == "child") ? child : lot      
      if params[:scrap] == "parent" # replace lot that will be shipped
        lot = child
        wafers = $f1sv.lot_info(lot, :wafers=>true).wafers.collect {|w| w.wafer}
        $at.dbbtf.update_lot_for_wafers(wafers, lot)
      end
      #
      wafers = $f1sv.lot_info(scrap_lot, :wafers=>true).wafers.collect {|w| w.wafer}
      $at.dbbtf.update_lot_for_wafers(wafers, scrap_lot)
      assert_equal 0, $f1sv.scrap_wafers(scrap_lot), "failed to scrap lot in BTF"
      assert_equal 0, $f1sv.wafer_sort_req(scrap_lot, ""), "cannot move wafers into FOSB"
      assert wait_for { 
        _avinfo = $av.lot_info(scrap_lot)
        _avinfo ? verify_equal("SCRAPPED", _avinfo.status) : false
      }, "wrong status of scrapped lot #{scrap_lot} in ASMView"
      assert wait_for(timeout: 300) { 
        _f8info = $f8sv.lot_info(scrap_lot)
        _f8info ? verify_equal("SCRAPPED", _f8info.status) : false
      }, "wrong status of scrapped lot #{scrap_lot} in F8 SiView"
      
      return if params[:scrap] == "all"
    end
    
    if params[:partconv] && params[:new_erpitem]
      assert_equal 0, $f1sv.schdl_change(lot, product: params[:partconv], route: $f1sv.lot_info(lot).route)
      assert $f1sv.user_parameter_set_verify("Lot", lot, "ERPItem", params[:new_erpitem], params)
      # TODO need to query ASMView      
      assert_equal 0, $av.asm_schdl_change(lot, product: params[:partconv], route: $av.lot_info(lot).route)
      assert $av.user_parameter_set_verify("Lot", lot, "ERPItem", params[:new_erpitem], params)
      assert wait_for { 
        _avinfo = $av.lot_info(lot)
        res  = verify_equal(params[:partconv], _avinfo.product)
        res &= verify_equal(params[:new_erpitem], $av.user_parameter("Lot", lot, name: "ERPItem").value)
        res
      }, "wrong status after part conversion of lot #{lot} in ASMView"
    end
    
    # add mask layers
    #assert $f1sv.lot_note_register lot, "FabGUI.Shipping.ProcessedML", "{\"fab\":\"F1\",\"maskLayers\":5}", :user=>"X-FABGUI", :password=>"uJitvQ"
    puts "\n\nStandard ship #{lot} via FabGUI in Fab1 and return to start verification."
    gets
    # return lot to source location
    if params[:backup_complete]
      #
      $log.info "verify AsmView status"
      assert wait_for {
        alc = $avb2b.lot_characteristics(lot)
        res =  verify_equal "BackendComplete", alc.fabqualitycheck, "wrong completion state in ASMView"
        res &= verify_equal "LOTSHIPD.1", alc.op, "wrong PD in ASMView"
        res &= verify_equal "Processing", alc.status, "wrong state in ASMView"
        res
      }, "wrong status in ASMView"
    else
      assert $f8sv.backup_prepare_return_receive(lot, $f1sv, :return_bank=>@@return_bank, :src_carrier=>'9%')
    end    
    return lot
  end
  
  # Ship lot and child to subcon
  # Final ship of cousin, child and lot
  def subcon_shipment_to_btf_with_split(lot, erpasmlot, params={})
    # FabGUI settings
    assert $fguictrl.set_runtime_values({
      "SHIPPING.YmsDataTransfer.PDList"=>"TKEYBUMPSHIPDISPO.01:TurnkeySubconIDBump;TKEYSORTSHIPDISPO.01:TurnkeySubconIDSort"
    }), "config is not correct"
    
    child = $f8sv.lot_split(lot, 10)
    sent_lots = [lot,child]
    
    ship_time = Time.now
    puts "\n\nShip lots #{sent_lots.inspect} via FabGUI SubCon Shipment and press return to start the verification."
    puts "Subcon Ship time: #{ship_time}"
    gets
    
    # lots are in non-pro bank
    sent_lots.each do |l|
      assert_equal "NonProBank", $f8sv.lot_info(l).status, "lot should be in non-pro bank"
      assert _verify_notes(l, ship_time)
    
      # send to Fab1
      ###assert $f8sv.backup_prepare_send_receive(lot, $f1sv, :backup_bank=>nil)
      $f8sv.prepare_for_backup(l, backup_bank: nil)
      assert_equal 0, $f8sv.backupop_lot_send(l, $f1sv, backup_bank: nil)
      if @@manual_receive
        puts "\n\nReceive lot #{l.inspect} via FabGUI in Fab1 and press return to start the verification."    
        gets
      else
        $f1sv.backupop_lot_send_receive(l, $f8sv, bak_carrier:"F%", carrier_category: "FOSB")
      end
    end
    
    sent_lots.each do |l|        
      # set lot script parameters
      assert $f1b2b.lot_set_params(l, erpasmlot) 
      # process lot on the BTF route, including binning data creation
      
      li_wafers = $f1sv.lot_info(l).nwafers
      sort_data = Hash[li_wafers.times.collect {|i| [i,[125,50,25+i]]}]
      assert $at.process_btfturnkey(l, params.merge({:fabcode=>"F8", :binning_data=>sort_data})), 
        "error processing lot #{l} in BTF"
    end
    
    child1 = $f1sv.lot_split(lot, 5)
    assert child1, "failed to split #{lot}"
    child2 = $f1sv.lot_split(lot, 5)
    assert_equal 0, $f1sv.wafer_sort_req(child2, $f1sv.lot_info(child1).carrier, slots: 1), "failed to move lot to other carrier"
    assert_equal 0, $f1sv.lot_merge(child1, child2), "failed to merge in backup"
    # verify child1 in ASMView and Fab8 SiView
    assert wait_for { 
      _avinfo = $av.lot_info(child1)
      _avinfo ? verify_equal(10, _avinfo.nwafers) : false
    }, "wrong number of wafers for lot #{child1} in ASMView"
    assert wait_for(timeout: 300) { 
      _f8info = $f8sv.lot_info(child1)
      _f8info ? verify_equal("NonProBank", _f8info.status) : false
    }, "wrong status of child lot #{child1} in F8 SiView"
    
    return [child1, child, lot].each do |l|
      # update sort db with new lotid
      wafers = $f1sv.lot_info(l, :wafers=>true).wafers.collect {|w| w.wafer}
      $at.dbbtf.update_lot_for_wafers(wafers, l)
      #
      puts "\n\nStandard ship #{l.inspect} via FabGUI in Fab1 and return to start verification."
      gets
    
      # return lot to source location
      if params[:backup_complete]
        $log.info "waiting 1 minute for ASMView Update"
        sleep 60
        #
        $log.info "verify AsmView status"
        alc = $avb2b.lot_characteristics(l)
        assert_equal "BackendComplete", alc.fabqualitycheck, "wrong completion state in ASMView"
        assert_equal "LOTSHIPD.1", alc.op, "wrong PD in ASMView"
        assert_equal "Processing", alc.status, "wrong state in ASMView"        
      else
        assert $f8sv.backup_prepare_return_receive(l, $f1sv, return_bank: @@return_bank, src_carrier: '9%', carrier_category: "TKEY")
      end
    end
  end
  
  def fabgui_shipping(lots)    
    # FabGUI settings
    assert $fguictrl.set_runtime_values({
      "SHIPPING.AsmView.LotComplete.Enabled"=>"true",
      "SHIPPING.BtfTurnkey.SortUpdate.Enabled"=>"false", 
      "SHIPPING.BtfTurnkey.SortUpdate.Queue"=>"F1BTF.JCP_TKEY_REQUEST03", 
      "SHIPPING.SortPostProcessorEI.Enabled"=>"false"
    }), "config is not correct"
    #
    lots = [lots] unless lots.kind_of?(Array)
    ship_time = Time.now
    puts "\n\nShip lot #{lots.inspect} in Fab8 via FabGUI standard shipping and press return to start the verification."
    gets
    
    $log.info "waiting for bank move in Fab8"
    sleep 120
    sleep 120
    #
    $log.info "verify SiView status"
    
    lots.each do |lot|
      fli = $f8sv.lot_info(lot)
      assert_equal "COMPLETED", fli.status, "wrong lot status for #{lot}"
      assert_equal "O-ENDFG", fli.bank, "wrong bank for #{lot}"
      assert _verify_notes(lot, ship_time, false)

      #
      $log.info "verify AsmView status"
      alc = $avb2b.lot_characteristics(lot) 
      puts alc.pretty_inspect
      assert_equal "Complete", alc.fabqualitycheck
    end
  
    # on working ERP connection, 5 minuts later:
    $log.info "waiting 5 minutes for ERP to response"
    sleep 310
    lots.each do |lot|
      $log.info "verify AsmView status"
      ali = $av.lot_info(lot)
      assert_equal "COMPLETED", ali.status
      assert_equal "WINV", ali.bank
    end
    # ship the lots   
    lots.each {|l| 
      if @@asm_ship
        assert $at.pick_release_verify(l), "failed pick release in ASMView"        
        assert $at.pick_confirm_verify(l), "failed pick confirm in ASMView"
        assert wait_for { 
          res1 = verify_equal("OY-SHIP", $f1sv.lot_info(l).bank, "wrong bank in Fab1")
          res8 = verify_equal("O-SHIP", $f8sv.lot_info(l).bank, "wrong bank in Fab8")
          res1 && res8
        }, "failed pick confirm in SiView"
        assert $at.ship_confirm_verify(l), "failed to confirm shipment in ASMView"
        assert wait_for { 
          res1 = verify_equal("SHIPPED", $f1sv.lot_info(l).status, "wrong bank in Fab1")
          res8 = verify_equal("SHIPPED", $f8sv.lot_info(l).status, "wrong bank in Fab8")
          res1 && res8
        }, "failed ship confirm in SiView"
      else    
        $f1sv.wafer_sort_req(l, "")
        $f1sv.lot_bank_move(l, "OY-SHIP")
        assert_equal 0, $f1sv.lot_ship(l, "OY-SHIP"), "failed to do final ship in Fab1"
        $f8sv.wafer_sort_req(l, "")
        $f8sv.lot_bank_move(l, "O-SHIP")
        assert_equal 0, $f8sv.lot_ship(l, "O-SHIP"), "failed to do final ship in Fab8"      
      end    
    }
  end

  def _verify_notes(lot, ship_time, is_first_time=true)
    notes = $f8sv.lot_notes(lot)
    notes = notes.select {|note| note.user == "X-FABGUI" && note.timestamp > $f8sv.siview_timestamp(ship_time)}
    res = verify_condition("should have Fab 8 specific mask layers") { notes.find {|note| note.title == "FabGUI.Shipping.ProcessedML"} }
    if is_first_time
      res &= verify_condition("should have external mask layers") { notes.find {|note| note.title == "FabGUI.Shipping.ExternalML"} }
      if ERP14
        res &= verify_condition("should have raw wafer type") { notes.find {|note| note.title == "FabGUI.Shipping.RawWaferType"} }
      end
    end
    return res
  end

end