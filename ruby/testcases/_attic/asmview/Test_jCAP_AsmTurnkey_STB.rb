=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: Steffen Steidten, 2012-06-22

Version: 14.05

History:
  2013-08-01 ssteidte,  one test program for Fab1 and Fab7
  2014-08-13 ssteidte,  using Fab1 route with active versioning, disabled Fab7 test case because of several issues
=end

require 'RubyTestCase'
require 'asmviewtest'

# Test the AsmTurnkey ShipmentToSubcon interface
class Test_jCAP_AsmTurnkey_STB < RubyTestCase
  
  tk_j = SiView::MM::LotTkInfo.new("J", "1T", "CT")
  @@erpasmlot_f1 = ERPMES::ErpAsmLot.new(nil, tk_j, '*SHELBY21AE-M00-JB4', nil, 'SHELBY21BTF.QA', 'C02-TKEY-ACTIVE.01', 'gf', 'PO')
  #tk_m = SiView::MM::LotTkInfo.new("M", "CT", "AT", nil, "CT")
  #tk_aa = SiView::MM::LotTkInfo.new("QE-AA", "CS")
  #@@erpasmlot_f7 = ERPMES::ErpAsmLot.new(nil, tk_m, nil, nil, "ODYSSEY65V20.01", "PCMT10LPODY10.1", "gf", "PO")
  #@@erpasmlot_f7 = ERPMES::ErpAsmLot.new(nil, tk_aa, nil, nil, "IRISV20.01", "PCMT10LPIRIS2.1", "gf", "PO")

  def self.startup
    super(nosiview: true)
  end
  
  def self.afer_all_passed
  end
  
  def test00_setup
    $setup_ok = false
    $at = AsmView::Test.new($env, f1: true)
    $av = $at.av
    $avstb = $at.avstb
    $avb2b = $at.avb2b
    $f1sv = $at.f1sv
    $f1b2b = $at.f1b2b
    #$f7sv = $at.f7sv
    #$f7b2b = $at.f7b2b
    #
    assert $at.check_connectivity, "at least one component is not connected"
    $setup_ok = true
  end
  
  
  def test11_stb_wrong_lot
    $setup_ok = false
    assert $at.stb_wrong_lot(fabcode: 'F1'), "error in lot rejection"
    $setup_ok = true
  end
  
  def test13_stb_new_lot_f1
    # create a Fab1 lot not existing in AsmView
    assert lot = $f1b2b.new_lot(@@erpasmlot_f1, set_dcg: false), "error creating a new Fab1 lot" 
    assert !$av.lot_exists?(lot), "lot #{lot} already exists in AsmView"
    #
    # shipToSubcon (stb) message
    assert $at.stb_verify(lot, :bumpb, fabcode: 'F1'), "lot #{lot.inspect} not created correctly"
    ##return
    #
    # send repeated messages, no changes
    proctime = $avstb.lot_proctime(lot)
    9.times {assert $at.stb_verify(lot, :bumpb, fabcode: 'F1', proctime: proctime)}
    #
    # cleanup
    $f1sv.delete_lot_family(lot)
  end
  
  def XXtest13_stb_new_lot_f7
    # stb with a Fab7 lot not existing in AsmView
    lot = $f7b2b.new_lot(@@erpasmlot_f7, set_dcg: false)
    assert lot, "error creating a new Fab7 lot" 
    assert !$av.lot_exists?(lot), "lot #{lot} already exists in AsmView"
    #
    # shipToSubcon (stb) message
    assert $at.stb_verify(lot, :bumpc, fabcode: "F7"), "lot #{lot.inspect} not created correctly"
    #
    # send repeated messages, no changes
    proctime = $avstb.lot_proctime(lot)
    ok = 0
    9.times {|i| ok += 1 if $at.stb_verify(lot, :bumpa, fabcode: "F7", proctime: proctime)}
    assert_equal 9, ok, "sending repeated shipToSubcon messages failed"
    #
    # cleanup
    $f7sv.delete_lot_family(lot)
  end
  
  
  # working with Fab1 only from here on
  
  # STB order
  
  def test21_stb_parent_child1_child2_grandchild21
    # stb in order of creation
    lot, lc1, lc2, lc21 = new_lot_family(@@erpasmlot_f1, set_dcg: false)
    assert !$av.lot_exists?(lot), "lot #{lot} already exists in AsmView"
    #
    # stb parent, child1, child2, grandchild
    [lot, lc1, lc2, lc21].each {|l| assert $at.stb_verify(l, :bumpb), "lot #{l.inspect} not created correctly"}
    #
    # cleanup
    $f1sv.delete_lot_family(lot)
  end
  
  def test22_stb_parent_child1_grandchild21_child2
    # stb grandchild before child
    lot, lc1, lc2, lc21 = new_lot_family(@@erpasmlot_f1, set_dcg: false)
    assert !$av.lot_exists?(lot), "lot #{lot} already exists in AsmView"
    #
    # stb parent, child1, grandchild, child2
    [lot, lc1, lc21, lc2].each {|l| assert $at.stb_verify(l, :bumpb), "lot #{l.inspect} not created correctly"}
    #
    # cleanup
    $f1sv.delete_lot_family(lot)
  end
  
  def test23_stb_parent_child1_scrapchild2_grandchild21
    # stb grandchild with child scrapped
    lot, lc1, lc2, lc21 = new_lot_family(@@erpasmlot_f1, set_dcg: false)
    assert !$av.lot_exists?(lot), "lot #{lot} already exists in AsmView"
    #
    # scrap child2
    assert_equal 0, $f1sv.scrap_wafers(lc2), "error scrapping #{lc2}"
    #
    # stb parent, child, grandchild
    [lot, lc1, lc21].each {|l| assert $at.stb_verify(l, :bumpb), "lot #{l.inspect} not created correctly"}
    #
    # cleanup
    $f1sv.delete_lot_family(lot)
  end
  
  def test24_stb_parent_child1_mergechild2_grandchild2
    # stb grandchild with child merged
    lot, lc1, lc2, lc21 = new_lot_family(@@erpasmlot_f1, set_dcg: false)
    assert !$av.lot_exists?(lot), "lot #{lot} already exists in AsmView"
    #
    # merge child2 with parent
    assert_equal 0, $f1sv.lot_merge(lot, lc2), "error merging #{lc2} with #{lot}"
    #
    # stb parent, child, grandchild
    [lot, lc1, lc21].each {|l| assert $at.stb_verify(l, :bumpb), "lot #{l.inspect} not created correctly"}
    #
    # cleanup
    $f1sv.delete_lot_family(lot)
  end
  
  def test25_stb_parent_child1_mergegrandchild21_child2
    # stb with grandchild merged to parent
    lot, lc1, lc2, lc21 = new_lot_family(@@erpasmlot_f1, set_dcg: false)
    assert !$av.lot_exists?(lot), "lot #{lot} already exists in AsmView"
    #
    # merge grandchild21 with parent
    assert_equal 0, $f1sv.lot_merge(lot, lc21), "error merging #{lc21} with #{lot}"
    #
    # stb parent, child1, child2
    [lot, lc1, lc2].each {|l| assert $at.stb_verify(l, :bumpb), "lot #{l.inspect} not created correctly"}
    #
    # cleanup
    $f1sv.delete_lot_family(lot)
  end
  
  def test26_stb_parent_child1_mergegrandchild21_scrapchild2
    # stb lots with grandchild merged and child scrapped
    lot, lc1, lc2, lc21 = new_lot_family(@@erpasmlot_f1, set_dcg: false)
    assert !$av.lot_exists?(lot), "lot #{lot} already exists in AsmView"
    #
    # merge grandchild21 with parent and scrap child 2
    assert_equal 0, $f1sv.lot_merge(lot, lc21), "error merging #{lc21} with #{lot}"
    assert_equal 0, $f1sv.scrap_wafers(lc2), "error scrapping #{lc2}"
    #
    # stb parent, child1
    [lot, lc1].each {|l| assert $at.stb_verify(l, :bumpb), "lot #{l.inspect} not created correctly"}
    #
    # cleanup
    $f1sv.delete_lot_family(lot)
  end

  def test27_stb_grandchild
    # stb grandchild only
    lot, lc1, lc2, lc21 = new_lot_family(@@erpasmlot_f1, set_dcg: false)
    assert !$av.lot_exists?(lot), "lot #{lot} already exists in AsmView"
    #
    # stb grandchild only
    assert $at.stb_verify(lc21, :bumpb)
    assert_equal $f1sv.lot_family(lot), $av.lot_family(lot), "lot family mismatch"
    #
    # cleanup
    $f1sv.delete_lot_family(lot)
  end
  
  def test28_split_after_first_STB
    lot = $f1b2b.new_lot(@@erpasmlot_f1, set_dcg: false)
    assert lot, "cannot create a new lot"
    assert !$av.lot_exists?(lot), "lot #{lot} already exists in AsmView"
    #
    # split and stb child1 
    assert (lc1 = $f1sv.lot_split(lot, 5)), "cannot split lot #{lot}"
    assert $at.stb_verify(lc1, :bumpb), "lot #{lot.inspect} not created correctly"
    assert_equal $f1sv.lot_family(lot), $av.lot_family(lot), "lot family mismatch"
    # 
    # split child2, stb parent and child1
    assert (lc2 = $f1sv.lot_split(lot, 5)), "cannot split lot #{lot}"
    [lot, lc1].each {|l| assert $at.stb_verify(l, :bumpb), "lot #{l.inspect} not created correctly"}
    #
    # cleanup
    $f1sv.delete_lot_family(lot)
  end

  def test29_merge_after_first_STB
    # merge after first STB
    lot, lc1, lc2, lc21 = new_lot_family(@@erpasmlot_f1, set_dcg: false)
    assert !$av.lot_exists?(lot), "lot #{lot} already exists in AsmView"
    #
    # stb child1
    assert $at.stb_verify(lc1, :bumpb), "lot #{lot.inspect} not created correctly"
    #
    # merge child2 with parent and stb parent 
    assert_equal 0, $f1sv.lot_merge(lot,lc2), "lot #{lot} merge failed"
    assert $at.stb_verify(lot, :bumpb)
    assert_equal $f1sv.lot_family(lot), $av.lot_family(lot), "lot family mismatch"
    #
    # cleanup
    $f1sv.delete_lot_family(lot)
  end

  def XXtest30_stb_grandchild_mergegrandchild_stbparent
    # stb grandchild, merge with parent, stb parent -- currently fails
    # create lots
    lot, lc1, lc2, lc21 = new_lot_family(@@erpasmlot_f1, set_dcg: false)
    assert !$av.lot_exists?(lot), "lot #{lot} already exists in AsmView"
    #
    # stb grandchild21
    assert $at.stb_verify(lc21, :bumpb), "lot #{lot.inspect} not created correctly"
    #
    # merge grandchild21 with parent
    assert_equal 0, $f1sv.lot_merge(lot, lc21), "error merging #{lc21} with #{lot}"
    #
    # stb parent  (currently fails)
    assert $at.stb_verify(lot, :bumpb), "lot #{lot.inspect} not created correctly"
    #
    # cleanup
    $f1sv.delete_lot_family(lot)
  end
  
  def XXtest31_scrap_after_first_stb
    # currently fails
    lot = $f1b2b.new_lot(@@erpasmlot_f1, set_dcg: false)
    assert lot, "cannot create a new lot"
    assert !$av.lot_exists?(lot), "lot #{lot} already exists in AsmView"
    #
    # split out first child and ship it
    assert (lc1 = $f1sv.lot_split(lot, 5)), "cannot split lot #{lot}"
    assert $at.stb_verify(lc1, :bumpb), "lot #{lot.inspect} not created correctly"
    assert_equal $f1sv.lot_family(lot), $av.lot_family(lot), "lot family mismatch"
    # 
    # scrap in sv
    assert (lc2 = $f1sv.lot_split(lot, 5)), "cannot split lot #{lot}"
    assert_equal 0, $f1sv.scrap_wafers(lc2), "lot #{lot} scrap failed"
    # stb parent
    assert $at.stb_verify(lot, :bumpb)
##    assert $at.stb_verify(lc2, :bumpb), "not implemented yet"
    #
    # cleanup
    $f1sv.delete_lot_family(lot)
  end
  
  # prepare a new lot family for tests with split lots
  #
  # return array [parent, child1, child2, grandchild_of_child2] on success
  def new_lot_family(lparms, params={})
    lot = $f1b2b.new_lot(lparms, params) || return
    $log.info "prepare lot family"
    # split off 2 child and 1 grandchild lots
    lc1 = $f1sv.lot_split(lot, 5) || ($log.warn "  error splitting lot"; return)
    lc2 = $f1sv.lot_split(lot, 5) || ($log.warn "  error splitting lot"; return)
    lc21 = $f1sv.lot_split(lc2, 2) || ($log.warn "  error splitting lot"; return)
    return [lot, lc1, lc2, lc21]
  end
  
end
