=begin 
Test TurnKey

(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.
    
Author: Steffen Steidten, 2011-04-28
        Daniel Steger, 2014-03-27

Version: 13.10.0
=end

require "RubyTestCase"
require "turnkey"
require "derdack"

# Test TurnKey

class Test_Tkey_Flow_F8 < RubyTestCase
  Description = "Test TurnKey"
  
  @@tkbump = "GG"
  @@tksort = "GG"

  @@lott = {:tktype_j   => "8RK0012.000", :tktype_l => "8R40637.000", :tktype_s => "8R40638.000", :tktype_u => "8RK0011.000",}
  @@lots = {:sublot_e   => "8RK0014.000", :sublot_rd  => "8RK0015.000", :sublot_rds => "G00AU.001", :tktype_jn  => "8RK0013.000",}.merge(@@lott)
  
  @@altroute = "FF-28LPQ-45M.01"  # LET: "C02-TKEY.01"
  @@altprod = "SHELBY3CC.01"  # LET: "TKEY01.A0"
  @@partname = "*SHELBY3CC-J01"
  ###@@erpitem_s = "*EVEREST1AA-QE-JA0"
   
  @@sublottype = "PO"
  @@sublottype_e = "ET"
  @@sublottype_rd = "RD"
  @@customer = "qgt"
  @@special_customer = "samsung"

  @@maxsleeptime = "320"
  
  def setup
    super
    $setup_ok = ($setup_ok != false)
    assert $setup_ok, "test setup is not correct"
  end
  
  def test01_setup
    $derdack = Derdack::EventDB.new($env) unless $env=="let"
    $tkbranch = TurnKeyBranchStart.new($env, :sv=>$sv, :tktimeout=>@@maxsleeptime.to_i, :route=>@@altroute) 
    $setup_ok = false
    #
    # tktype U
    tk1 = SiView::MM::LotTkInfo.new("U", nil, nil)
    ep1 = ERPMES::ErpAsmLot.new(nil, tk1, @@partname, nil, @@altprod, @@altroute, @@customer, @@sublottype)
    assert $tkbranch.lot_set_params(@@lots[:tktype_u], ep1)
    # tktype L
    tk2 = SiView::MM::LotTkInfo.new("L", @@tkbump, nil)
    ep2 = ERPMES::ErpAsmLot.new(nil, tk2, @@partname, nil, @@altprod, @@altroute, @@customer, @@sublottype)
    assert $tkbranch.lot_set_params(@@lots[:tktype_l], ep2)
    # tktype S
    tk3 = SiView::MM::LotTkInfo.new("S", nil, @@tksort)
    ep3 = ERPMES::ErpAsmLot.new(nil, tk3, @@partname, nil, @@altprod, @@altroute, @@customer, @@sublottype)
    assert $tkbranch.lot_set_params(@@lots[:tktype_s], ep3)
    # tktype J
    tk4 = SiView::MM::LotTkInfo.new("J", @@tkbump, @@tksort)
    ep4 = ERPMES::ErpAsmLot.new(nil, tk4, @@partname, nil, @@altprod, @@altroute, @@customer, @@sublottype)
    assert $tkbranch.lot_set_params(@@lots[:tktype_j], ep4)
    #  sublottype e
    ep5 = ERPMES::ErpAsmLot.new(nil, tk4, @@partname, nil, @@altprod, @@altroute, @@customer, @@sublottype_e)
    assert $tkbranch.lot_set_params(@@lots[:sublot_e], ep5)
    #  sublottype rd
    ep6 = ERPMES::ErpAsmLot.new(nil, tk4, @@partname, nil, @@altprod, @@altroute, @@customer, @@sublottype_rd)
    assert $tkbranch.lot_set_params(@@lots[:sublot_rd], ep6)
    #  sublottype rd, special customer
    ep7 = ERPMES::ErpAsmLot.new(nil, tk4, @@partname, nil, @@altprod, @@altroute, @@special_customer, @@sublottype_rd)
    assert $tkbranch.lot_set_params(@@lots[:sublot_rds], ep7)
    # no subcons
    tk8 = SiView::MM::LotTkInfo.new("J", nil, nil)  
    ep8 = ERPMES::ErpAsmLot.new(nil, tk8, @@partname, nil, @@altprod, @@altroute, @@customer, @@sublottype)
    assert $tkbranch.lot_set_params(@@lots[:tktype_jn], ep8)
    #
    @@lots.each {|key,lot| 
      $tkbranch.lot_cleanup lot
      li = $sv.lot_info(lot)
      assert_equal @@altroute, li.route, "#{__method__}: #{lot} set up with wrong route #{li.route}"
      assert_equal @@altprod, li.product, "#{__method__}: #{lot} set up with wrong product #{li.product}"
    }
    #
    $setup_ok = true
    $log.info "tested with subcons: #{@@tkbump} #{@@tksort}"
  end
  
  def test10_move_tkbumpdispo
    $setup_ok = ensure_correct_carriers
    @@lots.each {|key,lot| 
      $setup_ok &= $tkbranch.lot_cleanup(lot, :op=>:tkbumpdispo)
    }
    @@start_time = Time.now
    assert $setup_ok
  end
  
  # L, J, S: just go to next operation (FabCode <> GU ==> no skip for GG)
  def test12j_tkbumpdispo
    lot = @@lots[:tktype_j]
    assert $tkbranch.wait_lot_move(lot, :tkbumpdispo, tstart: @@start_time), "unexpected final operation"
    # previous op is :tkbumpdispo
    sleep 10 # additional time to get history refreshed
    lops = $sv.lot_operation_list_fromhistory(lot)
    assert_equal $tkbranch.ops[:tkbumpdispo], lops[1].op, "unexpected previous operation, #{lops.inspect}"
  end
  
  # special treatment for lots with missing subcon
  def test12jn_tkbumpdispo_nosubcons
    lot = @@lots[:tktype_jn]
    assert !$tkbranch.wait_lot_move(lot, :tkbumpdispo, tstart: @@start_time), "unexpected final operation"
    assert $tkbranch.wait_lot_hold(lot, "O-TK", tstart: @@start_time), "lot #{lot} status must be ONHOLD"
    assert verify_derdack_msg(lot, @@start_time), "error in Derdack notification"
  end
  
  # L, J, S: just go to next operation (FabCode <> GU ==> no skip for GG)
  def test12l_tkbumpdispo
    lot = @@lots[:tktype_l]
    assert $tkbranch.wait_lot_move(lot, :tkbumpdispo, tstart: @@start_time), "unexpected final operation"
    # previous op is :tkbumpdispo
    sleep 10 # additional time to get history refreshed
    lops = $sv.lot_operation_list_fromhistory(lot)
    assert_equal $tkbranch.ops[:tkbumpdispo], lops[1].op, "unexpected previous operation, #{lops.inspect}"
  end

  # L, J, S: just go to next operation (FabCode <> GU ==> no skip for GG)
  def test12s_tkbumpdispo
    return # not configured yet
    lot = @@lots[:tktype_s]
    assert $tkbranch.wait_lot_move(lot, :tkbumpdispo, tstart: @@start_time), "unexpected final operation"
    # previous op is :tkbumpdispo
    sleep 10 # additional time to get history refreshed
    lops = $sv.lot_operation_list_fromhistory(lot)
    assert_equal $tkbranch.ops[:tkbumpdispo], lops[1].op, "unexpected previous operation, #{lops.inspect}"
  end
  
  # U: move to next PD, carrier independent
  def test12u_tkbumpdispo
    lot = @@lots[:tktype_u]
    assert $tkbranch.wait_lot_move(lot, :tkbumpdispo, tstart: @@start_time), "unexpected final operation"
    # previous op is :tkbumpdispo
    lop = $sv.lot_operation_list_fromhistory(lot)[1]
    assert_equal $tkbranch.ops[:tkbumpdispo], lop.op, "unexpected previous operation"
  end
  
  # # # # #  sublottypes
  # special treatment for sublottypes RD, E is handled like P*  
  def test15e_tkbumpdispo
    lot = @@lots[:sublot_e]
    assert $tkbranch.wait_lot_move(lot, :tkbumpdispo, tstart: @@start_time), "unexpected final operation"
    # previous op is :tkbumpdispo
    lop = $sv.lot_operation_list_fromhistory(lot)[1]
    assert_equal $tkbranch.ops[:tkbumpdispo], lop.op, "unexpected previous operation"
  end
  
  def test15rd_tkbumpdispo
    lot = @@lots[:sublot_rd]
    assert !$tkbranch.wait_lot_move(lot, :tkbumpdispo, tstart: @@start_time), "unexpected final operation"
    lc = $tkbranch.lot_characteristics(lot)
    assert_not_equal "ONHOLD", lc.status, "lot #{lot} status must not be ONHOLD"
  end
  
  # disabled for now
  def test15rds_tkbumpdispo_special_customer
    lot = @@lots[:sublot_rds]
    assert $tkbranch.wait_lot_move(lot, :tkbumpdispo, tstart: @@start_time), "unexpected final operation"
    lc = $tkbranch.lot_characteristics(lot)
    # previous op is :tkbumpdispo
    lop = $sv.lot_operation_list_fromhistory(lot)[1]
    assert_equal $tkbranch.ops[:tkbumpdispo], lop.op, "unexpected previous operation"
  end
  
  def test20_move_tktypeufilter
    $setup_ok = ensure_correct_carriers
    @@lots.each {|key,lot| 
      $setup_ok &= $tkbranch.lot_cleanup(lot, :op=>:tktypeufilter)
    }
    @@start_time = Time.now
    assert $setup_ok
  end
    
  def test22j_tktypeufilter
    # L, S, J: move to :tkbumpshipdispo
    lot = @@lots[:tktype_j]
    assert $tkbranch.wait_lot_move(lot, :tktypeufilter, tstart: @@start_time), "unexpected final operation"
    lc = $tkbranch.lot_characteristics(lot)
    assert_final_operation :tkbumpshipdispo, lc.op
  end
  
  def test22l_tktypeufilter
    # L, S, J: move to :tkbumpshipdispo
    lot = @@lots[:tktype_l]
    assert $tkbranch.wait_lot_move(lot, :tktypeufilter, tstart: @@start_time), "unexpected final operation"
    lc = $tkbranch.lot_characteristics(lot)
    assert_final_operation :tkbumpshipdispo, lc.op
  end

  def test22s_tktypeufilter
    # L, S, J: move to :tkbumpshipdispo
    lot = @@lots[:tktype_s]
    assert $tkbranch.wait_lot_move(lot, :tktypeufilter, tstart: @@start_time), "unexpected final operation"
    lc = $tkbranch.lot_characteristics(lot)
    assert_final_operation :tkbumpshipdispo, lc.op
  end
  
  def test22u_tktypeufilter
    # U: move to :chkplnhld
    lot = @@lots[:tktype_u]
    assert $tkbranch.wait_lot_move(lot, :tktypeufilter, tstart: @@start_time), "unexpected final operation"
    lc = $tkbranch.lot_characteristics(lot)
    assert_final_operation :lotshipdesig, lc.op
  end
    
  # # # # #  sublottypes
  def test25e_tktypeufilter
    # special treatment for sublottypes E* and RD
    lot = @@lots[:sublot_e]
    assert $tkbranch.wait_lot_move(lot, :tktypeufilter, tstart: @@start_time), "unexpected final operation"
    lc = $tkbranch.lot_characteristics(lot)
    assert_final_operation :tkbumpshipdispo, lc.op
  end
  
  def test25rd_tktypeufilter
    # special treatment for sublottypes E* and RD
    lot = @@lots[:sublot_rd]
    assert !$tkbranch.wait_lot_move(lot, :tktypeufilter, tstart: @@start_time), "unexpected final operation"
    lc = $tkbranch.lot_characteristics(lot)
    assert_not_equal "ONHOLD", lc.status, "lot #{lot} status must not be ONHOLD"
  end
  
  def test25rds_tktypeufilter_special_customer
    # special treatment for sublottypes E* and RD, except special customers (L, S, J move to :tkbumpshipdispo)
    lot = @@lots[:sublot_rds]
    assert $tkbranch.wait_lot_move(lot, :tktypeufilter, tstart: @@start_time), "unexpected final operation"
    lc = $tkbranch.lot_characteristics(lot)
    assert_final_operation :tkbumpshipdispo, lc.op
  end

  # :tkbumpshipdispo handled by FabGUI, removes carriers and moves lot on to :bumpshipinit
  def test30_move_tkbumpshipinit
    $setup_ok = ensure_correct_carriers
    @@lott.each {|key,lot| 
      $setup_ok &= $tkbranch.lot_cleanup(lot, :op=>:tkbumpshipinit)
      # shuffle wafers so both wafer ids and wafer aliases are out of order and not matching each other
      li = $sv.lot_info(lot)
      $sv.wafer_sort_req(lot, li.carrier, :slots=>"shuffle")
      $sv.wafer_alias_set(lot)
      $sv.wafer_sort_req(lot, li.carrier, :slots=>"shuffle")
    }
    @@start_time = Time.now
    assert $setup_ok
  end
  
  # L, J, S subcon GG
  def test32j_tkbumpshipinit    
    # send shipmentToSubCon message
    lot = @@lots[:tktype_j]
    lc = $tkbranch.lot_characteristics(lot)
    ($log.warn "does not happen for subcon GG"; return) if lc.tkinfo.tksort == "GG"
    # remove carrier
    $sv.wafer_sort_req(lot, '')
    # send shipment message (like FabGUI)
    assert $tkbranch.shipment_to_subcon(lot), "not processed"
    assert $tkbranch.verify_lot_state(lot, :tkbumpshipinit, 'NonProBank')
    aliases = $sv.lot_info(lot, :wafers=>true).wafers.collect {|w| w.alias.to_i}
    assert_equal 1.upto(25).collect {|i| i}, aliases, "wafers are not ordered by alias"
  end
  
  # L, J, S subcon GG
  def test32l_tkbumpshipinit
    return # not configured yet
    # send shipmentToSubCon message
    lot = @@lots[:tktype_l]
    lc = $tkbranch.lot_characteristics(lot)
    ($log.warn "does not happen for subcon GG"; return) if lc.tkinfo.tksort == "GG"
    # remove carrier
    $sv.wafer_sort_req(lot, '')
    # send shipment message (like FabGUI)
    assert $tkbranch.shipment_to_subcon(lot), "not processed"
    assert $tkbranch.verify_lot_state(lot, :tkbumpshipinit, 'NonProBank')
    aliases = $sv.lot_info(lot, :wafers=>true).wafers.collect {|w| w.alias.to_i}
    assert_equal 1.upto(25).collect {|i| i}, aliases, "wafers are not ordered by alias"
  end
  
  # L, J, S subcon GG
  def test32s_tkbumpshipinit
    # send shipmentToSubCon message
    lot = @@lots[:tktype_s]
    lc = $tkbranch.lot_characteristics(lot)
    ($log.warn "does not happen for subcon GG"; return) if lc.tkinfo.tksort == "GG"
    # remove carrier
    $sv.wafer_sort_req(lot, '')
    # send shipment message (like FabGUI)
    assert $tkbranch.shipment_to_subcon(lot), "not processed"
    assert $tkbranch.verify_lot_state(lot, :tkbumpshipinit, 'NonProBank')
    aliases = $sv.lot_info(lot, :wafers=>true).wafers.collect {|w| w.alias.to_i}
    assert_equal 1.upto(25).collect {|i| i}, aliases, "wafers are not ordered by alias"
  end
  
  def test32u_tkbumpshipinit
    # U, S: error msg only
    # send shipmentToSubCon message, must fail for types U, S
    lot = @@lots[:tktype_u]
    assert !$tkbranch.shipment_to_subcon(lot), "msg must be rejected"
    lc = $tkbranch.lot_characteristics(lot)
    assert_not_equal "ONHOLD", lc.status, "lot #{lot} status must not be ONHOLD"
    $log.info "behavior as expected"
  end

  # def test93_erpstatechgsort_u
    # lot = @@lots[:tktype_u]
    # assert $tkbranch.lot_cleanup(lot), "error cleaning up lot #{lot}"
    # assert $tkbranch.lot_locate(lot, :erpstatechgsort), "error positioning lot"
    # assert $tkbranch.wait_lot_hold(lot, "O-TK", :tktimeout=>400), "lot #{lot} status must be ONHOLD"
  # end

  def testy_info
    $log.info "tested with subcons: #{@@tkbump} #{@@tksort}"
  end
  
  # :chkplnhld handled by separate jCAP job (MoveToBank?)

  def ensure_correct_carriers
    ok = true
    @@lots.each {|key,lot| puts lot;ok &= ensure_correct_carrier(lot)}
    return ok
  end
  
  def ensure_correct_carrier(lot, params={})
    # return true on success
    ok = true
    $tkbranch.lot_cleanup(lot, :op=>params[:op])
    lc = $tkbranch.lot_characteristics(lot)
    is_gg = [lc.tkinfo.tkbump, lc.tkinfo.tksort].member?("GG")
    carriers = nil
    move_required = false
    if is_gg and lc.carrier.start_with?('9')
      move_required = true
      carriers = $sv.carrier_list(:empty=>true, :status=>"AVAILABLE", :carriers=>'Q%')
      raise "no carriers 3%... available" if carriers.count.zero?
    end
    if !is_gg and !lc.carrier.start_with?('9')
      move_required = true
      carriers = $sv.carrier_list(:empty=>true, :status=>"AVAILABLE", :carriers=>'9%')
      raise "no carriers 9%... available" if carriers.count.zero?
    end
    if move_required
      move_ok = false
      carriers.each {|c|
        $sv.carrier_usage_reset(c)
        move_ok = ($sv.wafer_sort_req(lot, c) == 0)
        break if move_ok
      }
      $log.warn "unable to move lot #{lot} to a carrier like #{carriers[0]}" unless move_ok
      ok &= move_ok
    end
    return ok
  end
  
  def verify_msg_parameter(lce, msg, parameter)
    msge = msg.params[parameter]
    return true if lce == msge
    return true if ["", "null", nil].member?(msge) and ["", "null", nil].member?(lce)
    $log.warn "  wrong #{parameter}: #{msge.inspect} instead of #{lce.inspect}"
    return false
  end
  
  def verify_derdack_msg(lot, tstart)
    # return true on success
    $log.info "verify_derdack_msg for lot #{lot}, starttime #{tstart.inspect}"
    test_ok = true
    msgs = $derdack.get_events(tstart, nil, "Event"=>"TurnkeyDispoMissingSubconIdEvent", "LotID"=>lot)
    ($log.warn "  error accessing Derdack db"; return nil) unless msgs
    msg = msgs[0]
    ($log.warn "  no Derdack message found for lot #{lot}"; return nil) unless msg
    lc = $tkbranch.lot_characteristics(lot)
    test_ok &= verify_msg_parameter(lc.sublottype, msg, "SubLotType")
    test_ok &= verify_msg_parameter(lc.erpitem, msg, "ERPItemID")
    test_ok &= verify_msg_parameter(lc.tkinfo.tktype, msg, "TurnKeyType")
    test_ok &= verify_msg_parameter(lc.tkinfo.tkbump, msg, "SubConBumpID")
    test_ok &= verify_msg_parameter(lc.tkinfo.tksort, msg, "SubConSortID")
    return test_ok
  end
  
  def assert_final_operation(pd_symbol, actual_op)
    if $tkbranch.ops[pd_symbol].kind_of?(Regexp)
      assert_match $tkbranch.ops[pd_symbol], actual_op, "unexpected final operation"
    else
      assert_equal $tkbranch.ops[pd_symbol], actual_op, "unexpected final operation"
    end
  end

  def self.lots
    @@lots
  end
end
