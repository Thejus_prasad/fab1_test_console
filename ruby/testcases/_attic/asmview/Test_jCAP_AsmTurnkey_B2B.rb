=begin 
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.
    
Author: U. Koerner (Saxonia), 2013-01-09

Version: 14.06

History:
  2013-08-05 ssteidte, reordered and unified tests, include GG and Fab7 test cases
  2014-08-13 ssteidte,  using Fab1 route with active versioning, disabled Fab7 test case because of login and data issues
=end

require 'RubyTestCase'
require 'asmviewtest'


# Test AsmView TurnKeyB2B
class Test_jCAP_AsmTurnkey_B2B < RubyTestCase
  @@holdcode = 'PPD'
  @@scrapcode = 'CCC'
  
  @@subcon_bump = 'CT'
  @@subcon_sort = 'ST'
  @@subcon_refl = @@subcon_bump
  @@subcon_assy = 'AK'
  @@tkinfos = {
    l:  SiView::MM::LotTkInfo.new("L", @@subcon_bump),
    s:  SiView::MM::LotTkInfo.new("S", nil, @@subcon_sort),
    j:  SiView::MM::LotTkInfo.new("J", @@subcon_bump, @@subcon_sort),
    r:  SiView::MM::LotTkInfo.new("R", @@subcon_bump, @@subcon_sort, @@subcon_refl),
    m:  SiView::MM::LotTkInfo.new("M", @@subcon_bump, @@subcon_sort, nil, @@subcon_assy),
    aa: SiView::MM::LotTkInfo.new("QE-AA", @@subcon_bump),
    ab: SiView::MM::LotTkInfo.new("AB", nil, nil, nil, @@subcon_assy),
  }
  
  @@erpasmlot = ERPMES::ErpAsmLot.new(nil, @@tkinfos[:j], '*SHELBY21AE-M00-JB4', nil, 'SHELBY21BTF.QA', 'C02-TKEY-ACTIVE.01', 'gf', 'PO')
  ##@@erpasmlot = ERPMES::ErpAsmLot.new(nil, @@tkinfos[:j], "MPW03141AC-M00-UA2", nil, "TKEY01.A0", "C02-TKEY.01", "gf", "PO")
  ##@@erpasmlot_f7 = ERPMES::ErpAsmLot.new(nil, @@tkinfos[:r], "IRISV20-R01", nil, "IRISV20.01", "PCMT10LPIRIS2.1", "gf", "PO")  
  # for tests with lots not existing in AsmView
  @@li_notex = SiView::MM::LotInfo.new
  @@li_notex.nwafers = 25
  @@li_notex.content = 'Wafer'
  @@li_notex.op = 'TKYIQAB.1'
  
  @@lot = 'UXYL33042.000'   #'UXYL31020.000' # nil for new lot
  ##@@lot_f7 = '7R89128.000' # nil for new lot - tktype R for multi purposes
  ### tktypes     L,             S,             J,             R,             M,             AA,            AB
  ##@@f7_lots = ["7R89129.000", "7R8912A.000", "7R8912C.000", "7R8912D.000", "7R8912E.000", "7R8912F.000", "7R8912G.000"]

  
  def self.startup
    super(nosiview: true)
  end
  
  def self.afer_all_passed
    #$sv.delete_lot_family(@@lot)
  end
  
  def test00_setup
    $setup_ok = false
    #
    params = {f1: true, 
      f1params: {product: @@erpasmlot.product, route: @@erpasmlot.route},
      ## f7params: {product: @@erpasmlot_f7.product, route: @@erpasmlot_f7.route},
    }
    $at = AsmView::Test.new($env, params)
    $av = $at.av
    $avb2b = $at.avb2b
    $avstb = $at.avstb
    $f1sv = $at.f1sv
    $f1b2b = $at.f1b2b
    $f7sv = $at.f7sv
    $f7b2b = $at.f7b2b
    assert $at.check_connectivity
    #
    ## ?? assert $f1sv.carrier_list(carrier: '9%', empty: true, status: 'AVAILABLE').size > 0, "not enough empty 9% carriers"
    #
    @@lot ||= $f1b2b.new_lot(@@erpasmlot)
    assert @@lot, "error creating SiView lot"
    assert $f1b2b.lot_set_params(@@lot, @@erpasmlot)
    $avstb.stb(@@lot, fabcode: 'F1') unless $av.lot_exists?(@@lot)
    assert $avb2b.lot_set_params(@@lot, @@erpasmlot)
    #
    ##@@lot_f7 ||= $f7b2b.new_lot(@@erpasmlot_f7)
    ##assert @@lot_f7, "error creating SiView lot"
    ##assert $f7b2b.lot_set_params(@@lot_f7, @@erpasmlot_f7)
    ##$avstb.stb(@@lot_f7, :fabcode=>"F7") unless $av.lot_exists?(@@lot_f7)
    ##assert $avb2b.lot_set_params(@@lot_f7, @@erpasmlot_f7)
    #
    $setup_ok = true
  end
  
  
  # Fab1
  
  def test11_regular
    lot = @@lot
    # 
    [@@tkinfos[:l], @@tkinfos[:s], @@tkinfos[:j], @@tkinfos[:r]].each {|tkinfo|
      $log.info "\n\nTurnkey Type #{tkinfo.tktype}"
      assert $at.lot_set_tkinfo(lot, tkinfo)
      assert $at.lot_tkey_prepare(lot, start: true)
      #
      assert $at.hold_verify(lot, @@holdcode)
      assert $at.release_verify(lot)
      child = $at.split_waferlot_verify(lot, 2)
      assert child
      assert $at.merge_verify(lot, child)
      assert $at.scrap_waferlot_verify_cancel(lot, @@scrapcode)
      #
      assert $at.lot_tkey_prepare(lot, start: true)
      assert $at.b2b_moves_verify(lot)
    }
  end
  
  def test12_implicit_stb_hold
    # prepare new lot in fab sv
    assert lot = $f1b2b.new_lot(@@erpasmlot), "error creating SiView lot"
    assert !$av.lot_exists?(lot)
    assert_equal 0, $f1sv.lot_opelocate(lot, nil, op: $f1b2b.ops[:erpstatechgbump])
    # send a b2b message for the not yet existing lot
    assert $avb2b.hold(lot, @@holdcode, fabcode: 'F1', erpitem: @@erpasmlot.erpitem, subcons: [@@subcon_bump, @@subcon_sort], lotinfo: @@li_notex)
    #
    assert $av.lot_exists?(lot)
    ali = $av.lot_info(lot)
    assert_equal @@li_notex.op, ali.op, "(av) wrong op of #{lot}"
    assert_equal 'ONHOLD', ali.status
    # remark: fab pd does not change
    assert_equal 'NonProBank', $f1sv.lot_info(lot).status
    assert_equal 1, $f1sv.lot_hold_list(lot, reason: 'YPPD').size
    # cleanup
    $f1sv.delete_lot_family(lot)
  end
  
  def test13_implicit_stb_move
    # prepare new lot in fab sv
    lot = $f1b2b.new_lot(@@erpasmlot)
    assert lot, "error creating SiView lot"
    assert !$av.lot_exists?(lot), "(av) #{lot} already built"
    assert_equal 0, $f1sv.lot_opelocate(lot, nil, op: $f1b2b.ops[:erpstatechgbump])
    # send a b2b message for the not yet existing lot
    assert $avb2b.move(lot, op: @@li_notex.op, fabcode: 'F1', erpitem: @@erpasmlot.erpitem, subcons: [@@subcon_bump, @@subcon_sort], lotinfo: @@li_notex)
    assert $av.lot_exists?(lot), "(sv) #{lot} not built"
    assert_equal @@li_notex.op, $av.lot_info(lot).op, "(av) wrong op of #{lot}"
    # remark: fab pd does not change
    assert_equal 'NonProBank', $f1sv.lot_info(lot).status, "(sv) #{lot} not banked in"
    # cleanup
    $f1sv.delete_lot_family(lot)
  end

  def test14_gg
    lot = @@lot
    #
    [SiView::MM::LotTkInfo.new('L', 'GG'), SiView::MM::LotTkInfo.new('S', '', 'GG'),
      SiView::MM::LotTkInfo.new('J', 'GG', 'GG'), SiView::MM::LotTkInfo.new('R', 'GG', 'GG', 'GG')].each {|tkinfo|
      assert $at.lot_set_tkinfo(lot, tkinfo)
      assert $avb2b.lot_tkey_prepare(lot, start: true)
      assert $f1sv.lot_cleanup(lot, op: $f1b2b.ops[:trc4b])
      # start
      assert $avb2b.start(lot)
      # hold
      assert $avb2b.hold_verify(lot, @@holdcode)
      assert_equal 'Waiting', $f1sv.lot_info(lot).status
      # release
      assert $avb2b.release_verify(lot)
      assert_equal 'Waiting', $f1sv.lot_info(lot).status
      # split_with_id
      child = $f1sv.lot_split(lot, 2)
      assert child
      ok = $avb2b.split_waferlot_withid_verify(lot, child, 2)
      assert ok, "error in split_waferlot_withid"
      # merge (not merged in SiView!)
      assert $avb2b.merge_verify(lot, child)
      assert $f1sv.lot_exists?(child)
      assert_equal 0, $f1sv.lot_merge(lot, child)
      # scrap
      $avb2b.scrap_waferlot_verify_cancel(lot, @@scrapcode)
      assert_equal 'Waiting', $f1sv.lot_info(lot).status
      #
      # some moves - makes no sense with bank tracking
#      $log.info "lot must not move in SiView"
#      li = $f1sv.lot_info(lot)
###      assert $at.lot_tkey_prepare(lot, start: true)
###      assert $at.b2b_moves_verify(lot, :fabop=>li.op, :fabstatus=>li.status)
#      ops = $av.lot_operation_list(lot).collect {|o| o.op}
#      ops[2..-1].each {|o|
#        assert $at.move_verify("move", lot, o, "Processing", params.merge(:fabop=>li.op, :fabstatus=>li.status))
#      }
    }
  end
end

class Fab7TestsNotExecutedAnyMore
  # Fab7
  # verify subcon ID check works
  
  def test20_prepare
    $setup_ok = false
    lot = @@lot_f7
    assert $at.lot_set_tkinfo(lot, @@erpasmlot_f7.tkinfo)
    assert $at.lot_tkey_prepare(lot, start: true)
    # since 2014, Mar there is no branch pd
    #assert $at.f7b2b.lot_locate(lot, :xxtkbranch) # tkbranch has pre1 script
    assert $at.f7b2b.lot_locate(lot, :tkbumpshipinit)
    $setup_ok = true
  end
  
  def test21_subconids_d
    lot = @@lot_f7
    assert $at.lot_set_tkinfo(lot, SiView::MM::LotTkInfo.new("D", "CS"))
    assert $avb2b.move(lot, :op=>:current, :subcons=>"CT CS")
    assert $at.lot_set_tkinfo(lot, SiView::MM::LotTkInfo.new("D", ""))
    assert !$avb2b.move(lot, :op=>:current, :subcons=>"CT CS")
  end
  
  def test22_subconids_k
    lot = @@lot_f7
    assert $at.lot_set_tkinfo(lot, SiView::MM::LotTkInfo.new("K", "CT", "CS"))
    assert $avb2b.move(lot, :op=>:current, :subcons=>"CT CS AK")
    assert $at.lot_set_tkinfo(lot, SiView::MM::LotTkInfo.new("K", ""))
    assert !$avb2b.move(lot, :op=>:current, :subcons=>"CT CS AK")
  end

  def test23_subconids_m
    lot = @@lot_f7
    assert $at.lot_set_tkinfo(lot, SiView::MM::LotTkInfo.new("M", "CT", "AK", nil, "CS"))
    assert $avb2b.move(lot, :op=>:current, :subcons=>"AK CS CT")
    assert !$avb2b.move(lot, :op=>:current, :subcons=>"AK CS")
  end

  def test24_subconids_g
    lot = @@lot_f7
    assert $at.lot_set_tkinfo(lot, SiView::MM::LotTkInfo.new("G", "AK", nil, nil, "CS"))
    assert $avb2b.move(lot, :op=>:current, :subcons=>"AK CS")
    assert !$avb2b.move(lot, :op=>:current, :subcons=>"AK CT")
  end

  def test25_subconids_s
    lot = @@lot_f7
    assert $at.lot_set_tkinfo(lot, SiView::MM::LotTkInfo.new("S", "", "CS"))
    assert $avb2b.move(lot, :op=>:current, :subcons=>"CS")
    assert !$avb2b.move(lot, :op=>:current, :subcons=>"CT")
  end

  def test26_dieqty
    lot = @@lot_f7
    assert $at.lot_set_tkinfo(lot, @@erpasmlot_f7.tkinfo)
    li = $av.lot_info(lot, wafers: true)
    [99, 97].each {|dcg|
      assert $avb2b.move(lot, :op=>:current, :dcg=>dcg)
      assert_equal dcg*li.nwafers, $av.user_parameter_wafer_sum(li.wafers, 'DieCountGood'), "wrong DieCountGood"
    }
  end
  
  # since V 13.9 Fab 7 not configured for this
  def XXtest27_split_withid
    lot = @@lot_f7
    li = $f7sv.lot_info(lot)
    $f7sv.eqp_opestart_cancel(li.eqp, li.cj)
    $f7sv.eqp_unload(li.eqp, nil, li.carrier)
    $setup_ok = false
    child = $f7sv.lot_split(lot, 2)
    assert child
    assert $avb2b.split_waferlot_withid_verify(lot, child, 2, :nwafers=>23)
    # clean up and try again (must fail)
    assert_equal 0, $av.asm_lot_merge(lot, child), "error cleaning up lot #{lot}"
    assert !$avb2b.split_waferlot_withid(lot, child, 2)
    assert !$av.lot_family(lot).member?(child), "wrong child split for #{child}"
    # clean up
    assert_equal 0, $f7sv.lot_merge(lot, child)
    assert_equal 0, $f7sv.eqp_load(li.eqp, nil, li.carrier)
    assert $f7sv.eqp_opestart(li.eqp, li.carrier)
    $setup_ok = true
  end
  
  def XXtest28_hold_release_2 # not yet implemented 
    lot = @@lot_f7
    # send 2 holds
    et1 = Time.now
    assert $at.hold_verify(lot, "PPD", :eventtime=>et1), "error in hold action of lot #{lot}"
    et2 = Time.now
    assert $at.hold_verify(lot, "PCD", :eventtime=>et2), "error in hold action of lot #{lot}"
    # release 2 holds, 2nd hold first
    $av.lot_hold_list(lot).pretty_inspect
    $f7sv.lot_hold_list(lot).pretty_inspect
    assert $avb2b.release(lot, :holdtime=>et2), "error releasing hold of lot #{lot}"
    $av.lot_hold_list(lot).pretty_inspect
    $f7sv.lot_hold_list(lot).pretty_inspect
    assert $at.release_verify(lot, :holdtime=>et1), "error releasing hold of lot #{lot}"
    $av.lot_hold_list(lot).pretty_inspect
    $f7sv.lot_hold_list(lot).pretty_inspect
  end
  
  # Fab 7 turnkey with bank tracking
  
  def test29_all_messages_all_tktypes # serial: 80 min, parallel: 50 min
    threads = []
    res = {}
    params = {:fabcode=>"F7"}
    #
    # keep connections for analysing errors or repeated execution
    $atconns_f7only = [] if $atconns_f7only.nil?
    @@f7_lots.count.times{|i| ($atconns_f7only << AsmView::Test.new($env, :f8=>false, :f1=>false, :erpshipment=>false, :btf=>false)) if $atconns_f7only[i].nil?}
    #
    @@f7_lots.count.times {|i|
### comment/uncomment next line to run serial/parallel
      #threads << Thread.new {
        _at = $atconns_f7only[i]
        lot = @@f7_lots[i]
        ok = true
        #
        tktype = _at.avb2b.lot_characteristics(lot).tkinfo.tktype.split('-')[-1].downcase
        Thread.current['name'] = "th_%s" % (tktype.length==1 ? tktype+' ' : tktype)
        #
        # set subcons as planned - but not really the tktype in fab 7
        tkinfo = @@tkinfos[tktype.to_sym]
        ok = (ok and _at.f7b2b.lot_set_tkinfo(lot, tkinfo))
        ok = (ok and _at.avb2b.lot_set_tkinfo(lot, tkinfo))
        #
        # hold, release, split. merge, scrap and scrap cancel in turnkey area
        op = (tkinfo.tktype=="S" ? :tksortshipinit : :tkbumpshipinit)
        # do not use asmviewtest::lot_tkey_prepare instead of next two fkt calls
        ok = (ok and _at.avb2b.lot_tkey_prepare(lot, params.merge(:start=>true)))
        ok = (ok and _at.f7b2b.lot_cleanup(lot, op: op))
        ok = (ok and _at.hold_verify(lot, @@holdcode))
        ok = (ok and _at.release_verify(lot))
        ok = (ok and (child = _at.split_waferlot_verify(lot, 2)))
        ok = (ok and _at.merge_verify(lot, child))
        ok = (ok and _at.scrap_waferlot_verify_cancel(lot, @@scrapcode))
        #
        # move along the turnkey subroutes
        # do not use asmviewtest::ot_tkey_start instead of next two fkt calls
        ok = (ok and _at.avb2b.lot_tkey_prepare(lot, start: true))
        ok = (ok and _at.f7b2b.lot_cleanup(lot, params.merge(:op=>op)))
        ok = (ok and _at.b2b_moves_verify(lot, params))
        #
        Thread.exclusive {
          n = Thread.current['name']
          res[n] = ok
          $log.error("Thread #{n} not successfully completed") unless ok
        }
### comment/uncomment next line (accordingly above) to run serial/parallel
      #}
    }
    #
    # awaiting the end of all threads and show an error message eventually 
    ##ThreadsWait.all_waits(*threads)
    threads.each{|t| t.join }
    #
    Thread.current["name"] = 'main'
    # fails if sth went wrong in at least one thread
    assert (k=res.collect {|k,v| k unless v}.compact).count.zero?, "processing in thread(s) #{k.join(", ")} not successfully"
  end

  def XXtest29a_repeat_test29_with_one_lot
    lot = @@f7_lots[1]
    tktype = $at.avb2b.lot_characteristics(lot).tkinfo.tktype.split('-')[-1].downcase
    tkinfo = @@tkinfos[tktype.to_sym]
    op = (tkinfo.tktype=="S" ? :tksortshipinit : :tkbumpshipinit)
    ok = true
    params = {:fabcode=>"F7"}
    ok = (ok and $at.avb2b.lot_tkey_prepare(lot, start: true))
    ok = (ok and $at.f7b2b.lot_cleanup(lot, params.merge(:op=>op)))
    ok = (ok and $at.b2b_moves_verify(lot, params))
    assert ok, "#{lot} tktype #{tktype} processing failed"
  end
  
  # Fab 7 implicit STB
  
  def test31_erpitem_die
    # create lot, order has an '*' 
    erpasmlot_f7 = ERPMES::ErpAsmLot.new(nil, @@tkinfos[:j], "ODYSSEY65V20-J01", "8007925*01:02", "ODYSSEY65V20.01", "PCMT10LPODY20.1", "7GFS", "TC")
    lot = $f7b2b.new_lot(erpasmlot_f7)
    assert lot
    assert !$av.lot_exists?(lot), "the new fab lot #{lot} already exists in AsmView"
    $log.info "using lot #{lot.inspect}"
    assert $f7b2b.lot_locate(lot, :xxtkbranch) # pre1 script at tkbranch
    # send B2B message, don't care about errors
    li = $f7sv.lot_info(lot, :wafers=>true)
    $avb2b.hold_verify(lot, @@holdcode, :lotinfo=>li, :fabcode=>"F7", :erpitem=>erpasmlot_f7.erpitem, :tkinfo=>erpasmlot_f7.tkinfo)
    # verify AsmView lot
    assert $av.lot_exists?(lot)
    assert_equal "*#{erpasmlot_f7.erpitem}", $avb2b.lot_characteristics(lot).erpitem
  end
  
  def test32_erpitem_wafer
    # create lot, order has no '*' 
    erpasmlot_f7 = ERPMES::ErpAsmLot.new(nil, @@tkinfos[:j], "ODYSSEY65V20-J01", "8007925:01:02", "ODYSSEY65V20.01", "PCMT10LPODY20.1", "7GFS", "TC")
    lot = $f7b2b.new_lot(erpasmlot_f7)
    assert lot
    assert !$av.lot_exists?(lot), "the new fab lot #{lot} already exists in AsmView"
    $log.info "using lot #{lot.inspect}"
    assert $f7b2b.lot_locate(lot, :xxtkbranch)  # pre1 script at tkbranch
    # send B2B message, don't care about errors
    li = $f7sv.lot_info(lot, :wafers=>true)
    $avb2b.hold_verify(lot, @@holdcode, :lotinfo=>li, :fabcode=>"F7", :erpitem=>erpasmlot_f7.erpitem, :tkinfo=>erpasmlot_f7.tkinfo)
    # verify AsmView lot
    assert $av.lot_exists?(lot)
    assert_equal erpasmlot_f7.erpitem, $avb2b.lot_characteristics(lot).erpitem
  end
  
  
  def XXtest50  # ????
    params = {:fabcode=>"F7"}
    lot = @@f7_lots[0] #rand(@@f7_lots.count)]
    ok = true
    #
    tktype = $at.avb2b.lot_tkinfo.tktype.split('-')[-1].downcase
    #
    # set subcons as planned - but not really the tktype in fab 7
    tkinfo = @@tkinfos[tktype.to_sym]
    ok &= $at.f7b2b.lot_set_tkinfo(lot, tkinfo)
    ok &= $at.avb2b.lot_set_tkinfo(lot, tkinfo)
    #
    # hold, release, split. merge, scrap and scrap cancel in turnkey area
    op = (tkinfo.tktype == "S" ? :tksortshipinit : :tkbumpshipinit)
    #
    # move along the turnkey subroutes
    # do not use asmviewtest::lot_tkey_prepare instead of next two fkt calls
    ok &= $at.avb2b.lot_tkey_prepare(lot, params.merge(start: true))
    ok &= $at.f7b2b.lot_cleanup(lot, params.merge(:op=>op))
    ok &= $at.b2b_moves_verify(lot, params)
  end
  
end
