=begin

(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.


Author: Pradeep, 2014-11-23

History:
  
=end

require 'RubyTestCase'


class Test1503_SubLotTypeChange < RubyTestCase
  Description = "Test sublot type change at various states"

  #production lot#
  @@product = "UT-PRODUCT000.01"
  @@route="UTRT001.01"
  @@stoker="UTSTO110"
  #@@routeOperations=[[expected module pd,[various pds of module pd(some first middle, last)]]
  @@routeOperations=[["UTMD000.01", ["1000.100","1000.950", "1000.990", "2000.990"]], ["UT-MD-SIMPLE01.01", ["2500.200"]], ["UT-MD-LP1.00", ["2700.100"]], ["BANKIN.01", ["3000.1000"]]]
  @@lastOpeNo='3000.1000'
  @@holdreason='AEHL'
  @@nonprobank='UT-USE'
=begin
	sub process of UTRT001.01
	1000.200	UTPD001.01	e0001-TEST-A0.01	1000.400
	1000.200	UTPD001.01	UTBR001.01	1000.300
	1000.200	UTPD001.01	UTBR002.01	1000.400
	1000.300	UTPD002.01	e0005-TEST-A0.01	1000.400
	1000.400	UTPD003.02	e0003-TEST-A0.01	1000.500
	1000.400	UTPD003.02	UTBR002.01	1000.500
	2000.300	UTPD002.01	UTBR000.01	2000.990
	2000.500	UTPD004.01	UTBR002.01	2000.500
	2000.600	UTPD005.02	UTBR002.01	1000.200
=end

#@@branch=[["branchRoute", "branc at pd", ["joining opeNo", mpd],[["branch op",respective mod pd],.., ["last branch op",modpd]]]
  @@branch=[["e0001-TEST-A0.01", "1000.200", ["1000.400","UTMD000.01"], [["e1000.1000","e0001-TEST-A0.01"], ["e1000.1300","e0001-TEST-A0.01"]]]] 
			#,["UTPD002.01", "2000.300", [["e1000.1000","UTMD000.01"], ["e1000.1300",""], ["2000.990",""]]]]
			
=begin
		2	1000.150	UTPD000.02	UTRWK01.01	1000.400
		4	1000.300	UTPD002.01	UTRW001.01	1000.400
		4	1000.300	UTPD002.01	UTRWK01.01	2000.990
		5	1000.400	UTPD003.02	UTRW001.01	1000.300
		8	1000.600	UTPD005.02	UTRW003.01	1000.700
		9	1000.700	UTPD006.01	UTRW002.01	1000.800
		9	1000.700	UTPD006.01	UTRW003.01	1000.700
		9	1000.700	UTPD006.01	UTRWK01.01	2000.800
		14	1000.990	UTPD011.01	UTRW001.01	1000.990
		18	2000.300	UTPD002.01	UTRW001.01	2000.400
=end
#@@rework=[["reworkRoute", "rework at pd", ["joining opeNo", mpd],[["rework op",respective mod pd],.., ["last rework op",modpd]]]
  @@rework=[["UTRWK01.01", "1000.150", ["1000.400","UTMD000.01"], [["RW1.e300","UTRMD00.01"], ["RW1.e400","UTRMD00.01"], ["RW1.e600","UTRMD00.01"]]]] 

  @@current='PO'
  @@changeto='PR'
  
  def test00_setup
	assert @@lot=$sv.new_lot_release(product:@@product,sublottype:'PO', stb:true), "Check new lot is created" 
	assert $sv.lot_info(@@lot).sublottype=='PO', "unexpected sublotype for the lot: #{@@lot}. expected PO, actual #{$sv.lot_info(@@lot).sublottype}"
	assert $sv.carrier_xfer_status_change($sv.lot_info(@@lot).carrier, "MO", @@stoker), "issue changing carrier state" #change carrier status
	$log.info "Lot used:: #{@@lot}; product: #{@@product}; route: #{@@route}" 
  end
  
  def test01_changeSublotType_prodlot
	handleSublottypechanges
	lotcomment=siview_timestamp
	$sv.sublottype_change @@lot, @@changeto, lotcomment:lotcomment
	assert $sv.lot_info(@@lot).sublottype==@@changeto, "issue with changing sublotype for the lot: #{@@lot}. expected PR, actual #{$sv.lot_info(@@lot).sublottype}"
	assert $sv.lot_comments(@@lot)[0].desc==lotcomment, "issue with changing lot comment along with sublotype for the lot: #{@@lot}. expected #{lotcomment}, actual #{$sv.lot_comments(@@lot)[0].desc}"
  end
  
  def handleSublottypechanges()
	@@current=$sv.lot_info(@@lot).sublottype
	@@current=='PO' ? @@changeto='PR' : @@changeto='PO'
  end
  
  #first, last, 
  def test02_checksublottypeChangeAtVariousOperations
	@@routeOperations.each{ |modulePD|
		expectedMPD= modulePD[0]
		
		#check able to change sublottype at various stages of the route
		modulePD[1].each{|opeNo|
			assert $sv.lot_opelocate(@@lot, opeNo), "failed to locate lot: #{@@lot} to ope num: #{opeNo}"
			handleSublottypechanges
			lotcomment=siview_timestamp
			$sv.sublottype_change @@lot, @@changeto, lotcomment:lotcomment
			assert $sv.lot_info(@@lot).sublottype==@@changeto, "issue with changing sublotype for the lot: #{@@lot}. expected PR, actual #{$sv.lot_info(@@lot).sublottype}"
			assert $sv.lot_comments(@@lot)[0].desc==lotcomment, "issue with changing lot comment along with sublotype for the lot: #{@@lot}. expected #{lotcomment}, actual #{$sv.lot_comments(@@lot)[0].desc}"
		}
	}
	$sv.lot_bankin_cancel(@@lot) unless $sv.lot_info(@@lot).bank.empty? 
  end  
  
  #first, last, 
  def test03_sublottypeChangewhileLotIsInBranchRoute
  #@@branch=[["branchRoute", "branc at pd", "joining opeNo", [["first op",respective mod pd], ["branch op",modpd],..]]]
	@@branch.each{ |branchDtls|
		branchRt= branchDtls[0]
		branchAtOpeNo=branchDtls[1]
		joiningOpeNo=branchDtls[2][0]
		joiningOpeMPD=branchDtls[2][1]
		assert $sv.lot_opelocate(@@lot, branchAtOpeNo), "failed to locate lot: #{@@lot} to ope num: #{branchAtOpeNo}"
		assert $sv.lot_branch(@@lot,branchRt, memo:"Sublottype change test", retop:joiningOpeNo ), "failed branch: #{@@lot} to branch: #{branchRt} at ope No: #{branchAtOpeNo}"
		branchDtls[3].each{
		|opeNoAndExpectedMPD|
			#check same MPD is retrieved for all the operations in branch
			assert $sv.lot_opelocate(@@lot, opeNoAndExpectedMPD[0]), "failed to locate lot: #{@@lot} to ope num: #{opeNoAndExpectedMPD[0]}"
			handleSublottypechanges
			lotcomment=siview_timestamp
			$sv.sublottype_change @@lot, @@changeto, lotcomment:lotcomment
			assert $sv.lot_info(@@lot).sublottype==@@changeto, "issue with changing sublotype for the lot in branch route: #{@@lot}. expected PR, actual #{$sv.lot_info(@@lot).sublottype}"
			assert $sv.lot_comments(@@lot)[0].desc==lotcomment, "issue with changing lot comment along with sublotype in branch route;  #{@@lot}: expected #{lotcomment}, actual #{$sv.lot_comments(@@lot)[0].desc}"
			
		}
		#expecting lot is at last operation on branch route.
		$sv.claim_process_lot(@@lot) #finds a eqp and does operation complete on it
		handleSublottypechanges
		lotcomment=siview_timestamp
		$sv.sublottype_change @@lot, @@changeto, lotcomment:lotcomment
			assert $sv.lot_info(@@lot).sublottype==@@changeto, "issue with changing sublotype on branch joining operation: #{@@lot}. expected PR, actual #{$sv.lot_info(@@lot).sublottype}"
		assert $sv.lot_comments(@@lot)[0].desc==lotcomment, "issue with changing lot comment along with sublotype on branch joining operation: #{@@lot}: expected #{lotcomment}, actual #{$sv.lot_comments(@@lot)[0].desc}"
			
	}
	
  end

#first, last, 
  def test04_checksublottypewhileLotIsInReworkRoute
  #@@branch=[["branchRoute", "branc at pd", "joining opeNo", [["first op",respective mod pd], ["branch op",modpd],..]]]
	@@rework.each{ |reworkDtls|
		reworkRt= reworkDtls[0]
		reworkAtOpeNo=reworkDtls[1]
		joiningOpeNo=reworkDtls[2][0]
		joiningOpeMPD=reworkDtls[2][1]
		assert $sv.lot_opelocate(@@lot, reworkAtOpeNo), "failed to locate lot: #{@@lot} to ope num: #{reworkAtOpeNo}"
		assert $sv.lot_gatepass(@@lot), "failed to gatepass lot: #{@@lot} to ope num: #{reworkAtOpeNo}" #rework starts after completion of the process, so doing a gatepass
		assert $sv.lot_rework(@@lot,reworkRt, return_opNo:joiningOpeNo), "failed rework: #{@@lot} to branch: #{reworkRt} at ope No: #{reworkAtOpeNo}"
		reworkDtls[3].each{
		|opeNoAndExpectedMPD|
			#check same MPD is retrieved for all the operations in branch
			assert $sv.lot_opelocate(@@lot, opeNoAndExpectedMPD[0]), "failed to locate lot: #{@@lot} to ope num: #{opeNoAndExpectedMPD[0]}"
			handleSublottypechanges
			lotcomment=siview_timestamp
			$sv.sublottype_change @@lot, @@changeto, lotcomment:lotcomment
			assert $sv.lot_info(@@lot).sublottype==@@changeto, "issue with changing sublotype for the lot in rework route: #{@@lot}. expected PR, actual #{$sv.lot_info(@@lot).sublottype}"
			assert $sv.lot_comments(@@lot)[0].desc==lotcomment, "issue with changing lot comment along with sublotype in a rework route: #{@@lot}: expected #{lotcomment}, actual #{$sv.lot_comments(@@lot)[0].desc}"
		
		}
		#expecting lot is at last operation on branch route.
		
		$sv.claim_process_lot(@@lot) #finds a eqp and does operation complete on it
		handleSublottypechanges
		lotcomment=siview_timestamp
		$sv.sublottype_change @@lot, @@changeto, lotcomment:lotcomment
		assert $sv.lot_info(@@lot).sublottype==@@changeto, "issue with changing sublotype rework route joining operation: #{@@lot}. expected PR, actual #{$sv.lot_info(@@lot).sublottype}"
		assert $sv.lot_comments(@@lot)[0].desc==lotcomment, "issue with changing lot comment along with sublotype in a rework route joining operation: #{@@lot}: expected #{lotcomment}, actual #{$sv.lot_comments(@@lot)[0].desc}"
		
	}
	
  end

  
	def test05_checksublottypeChangeWhenLotIsOnHold
		$sv.lot_bankin_cancel(@@lot) unless $sv.lot_info(@@lot).bank.empty? 
		assert $sv.lot_hold(@@lot, @@holdreason), "failed to put lot on hold: #{@@lot} with reason code: #{@@holdreason}"
		handleSublottypechanges
		lotcomment=siview_timestamp
		$sv.sublottype_change @@lot, @@changeto, lotcomment:lotcomment
		assert $sv.lot_info(@@lot).sublottype==@@changeto, "issue with changing sublotype for the lot: #{@@lot} while lot is on hold. expected PR, actual #{$sv.lot_info(@@lot).sublottype}"
		assert $sv.lot_comments(@@lot)[0].desc==lotcomment, "issue with changing lot comment along with sublotype for the lot: #{@@lot} while lot is on hold. expected #{lotcomment}, actual #{$sv.lot_comments(@@lot)[0].desc}"
		assert $sv.lot_hold_release(@@lot, @@holdreason), "failed to release lot hold after subloty type change: #{@@lot} with reason code: #{@@holdreason}"
	end 
	
   def test06_checksublottypeChangeWhenLotIsinBank
		assert $sv.lot_opelocate(@@lot, @@lastOpeNo), "failed to locate lot: #{@@lot} to ope num: #{@@lastOpeNo}"
		assert !$sv.lot_info(@@lot).bank.empty?, "bankin failed for the lot: #{@@lot} to ope num: #{@@lastOpeNo}"
		handleSublottypechanges
		lotcomment=siview_timestamp
		$sv.sublottype_change @@lot, @@changeto, lotcomment:lotcomment
		assert $sv.lot_info(@@lot).sublottype==@@changeto, "issue with changing sublotype for the lot: #{@@lot} while lot is in bank. expected PR, actual #{$sv.lot_info(@@lot).sublottype}"
		assert $sv.lot_comments(@@lot)[0].desc==lotcomment, "issue with changing lot comment along with sublotype for the lot: #{@@lot} while lot is in bank. expected #{lotcomment}, actual #{$sv.lot_comments(@@lot)[0].desc}"
		
	end 
	
   def test07_checksublottypeChangeWhenLotIsinNonProBank
		$sv.lot_bankin_cancel(@@lot) unless $sv.lot_info(@@lot).bank.empty? 
		assert $sv.lot_nonprobankin(@@lot, @@nonprobank), "failed to do non probank in: #{@@lot}, non pro bank; #{@@nonprobank}"
		handleSublottypechanges
		lotcomment=siview_timestamp
		$sv.sublottype_change @@lot, @@changeto, lotcomment:lotcomment
		assert $sv.lot_info(@@lot).sublottype==@@changeto, "issue with changing sublotype for the lot: #{@@lot} while lot is in bank. expected PR, actual #{$sv.lot_info(@@lot).sublottype}"
		assert $sv.lot_comments(@@lot)[0].desc==lotcomment, "issue with changing lot comment along with sublotype for the lot: #{@@lot} while lot is in bank. expected #{lotcomment}, actual #{$sv.lot_comments(@@lot)[0].desc}"
		assert $sv.lot_nonprobankout(@@lot), "failed to do non probank out: #{@@lot}, non pro bank; #{@@nonprobank}"
	end 
	
	
   def test08_checksublottypeChangeforScrapedlot
		$sv.lot_bankin_cancel(@@lot) unless $sv.lot_info(@@lot).bank.empty? 
		$sv.lot_nonprobankout(@@lot)
		
		$sv.scrap_wafers(@@lot)
		handleSublottypechanges
		lotcomment=siview_timestamp
		$sv.sublottype_change @@lot, @@changeto, lotcomment:lotcomment
		assert $sv.lot_info(@@lot).sublottype==@@changeto, "issue with changing sublotype for a scrapped lot: #{@@lot}. expected PR, actual #{$sv.lot_info(@@lot).sublottype}"
		assert $sv.lot_comments(@@lot)[0].desc==lotcomment, "issue with changing lot comment along with sublotype for scrapped lot: #{@@lot}. expected #{lotcomment}, actual #{$sv.lot_comments(@@lot)[0].desc}"
		assert $sv.lot_nonprobankout(@@lot), "failed to do non probank out: #{@@lot}, non pro bank; #{@@nonprobank}"
		$sv.scrap_wafers_cancel(@@lot)
	end 
	
end
