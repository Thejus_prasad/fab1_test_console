=begin

(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.


Author: Pradeep Buddaraju

History:
  
=end

require 'SiViewTestCase'

# New runseq and split seq rules
#
# see http://myteamsgf1/sites/gfmt/domains/FC/MESCON/Shared%20Documents/Development/Design/CodeDrop3_7-LotGen_SplitSeqFormat.mht
# clean data from FSLIGRUNSEQUENCE and setup customers again for this test.

class Test_R15LIG < SiViewTestCase
  @@carrier = 'UR%'
  @@product = 'UT-PRODUCT001.01'
  @@route = "UTRT001.01"
  @@bank = "UT-RAW"
  @@vendor_product = "UT-RAW.00"
  @@cust_NumericThenAlphanumeric2='runSeq3'
  @@cust_NumericThenAlphanumeric3='runSeq4'
  @@cust_NumericThenAlphanumeric='runSeq5'
  @@ignoreChars='IO'
  @@splitSequenceNumberGeneration='Alphanumeric'
  @@carriers=[]
  @@alisformat="" # change env to <##>, R15.<##>, <LOT_FAMILY_ID_PFX>.<##>
  @@lot=nil
  @@current_seq='IO'#just to skip this for first time
  @@counter1=0
  @@counter1Char=65 #Ascii-65 to 90
  @@counter2Char=65
  @@counter3Char=65
  @@fabID='U'
  @@customerCode='PB'
  @@yrCode='M'
  @@workWeek='30'
  @@currentcount=1
  @@maxRunSeq='00D'
  @@lastRunSeq='00C'
  @@maxRunSeqCustomer='maxRunSeq'
  
  def self.shutdown
	#$sv.delete_lot_family @@lot
  end

  def test00_setup
   #$svtest.carriers=@@carrier
   #27936.times{runseq_NumericThenAlphanumeric2; puts @@current_seq }
   #9184.times{runseq_NumericThenAlphanumeric3;  $log.info @@current_seq }
  end
  
  def test01_NumericThenAlphanumeric2
	passed=true
	27936.times{
	lot= $sv.generate_new_lotids(@@cust_NumericThenAlphanumeric2)[0]
	expectedLot= nextlot2
	if (expectedLot!=lot )
		$log.warn ('run seq didnt match expected: expected: ' + expectedLot + ' actual: ' + lot)
		passed=false
	end	
	}
	assert passed, 'some run seq skipped'
  end
  
   def test02_NumericThenAlphanumeric3
	@@customerCode='P4'
	@@currentcount=55
	passed=true
	$log.info 'Customer:: ' + @@cust_NumericThenAlphanumeric3
	9160.times{
	lot= $sv.generate_new_lotids(@@cust_NumericThenAlphanumeric3)[0]
	expectedLot= nextlot3
	if (expectedLot!=lot)
		$log.warn ('run seq didnt match expected: expected: ' + expectedLot + ' actual: ' + lot)
		passed=false
	end	
	}
	assert passed, 'some run seq skipped'
  end
  
  def test03_NumericThenAlphanumeric
	@@customerCode='P5'
	@@currentcount=1
	currSeq=''
	while (currSeq!='00H')
		lot= $sv.generate_new_lotids(@@cust_NumericThenAlphanumeric)[0]
		currSeq= (lot.split('.')[0])[-3..-1]
		puts currSeq
	end
	assert currSeq=='00H', 'unable to reach last run seq expected: "00H" failed after: ' + currSeq
	lot= $sv.generate_new_lotids(@@cust_NumericThenAlphanumeric)[0]
	currSeq= (lot.split('.')[0])[-3..-1]
	assert currSeq=='00J', 'unable to skip run seq expected: "00J" actual: ' + currSeq	
	
	while (currSeq!='00N')
		lot= $sv.generate_new_lotids(@@cust_NumericThenAlphanumeric)[0]
		currSeq= (lot.split('.')[0])[-3..-1]
		puts currSeq
	end
	assert currSeq=='00N', 'unable to reach last run seq expected: "00N" failed after: ' + currSeq
	lot= $sv.generate_new_lotids(@@cust_NumericThenAlphanumeric)[0]
	currSeq= (lot.split('.')[0])[-3..-1]
	assert currSeq=='00P', 'unable to skip run seq expected: "00O" actual: ' + currSeq	
	#above logic is unnecessary, but kept it to check if seq is generated in right order
	while (currSeq!='ZZZ')
		lot= $sv.generate_new_lotids(@@cust_NumericThenAlphanumeric)[0]
		assert false, 'inhibit characets are generated in lot' if (lot.include?('I') || lot.include?('O'))
		currSeq= (lot.split('.')[0])[-3..-1]
		puts currSeq
	end
	assert currSeq=='ZZZ', 'unable to generate till last run seq expected: "ZZZ" actual: ' + currSeq	
  end
  
  def test04_MaxRunSeq
	#generate till it reaches last run seq
	#expected lot id format set for customer $FabId$CustomerPrefix$YearCode$WorkWeek$RunSeq.$SplitSeq
	currSeq=''
	while (currSeq!=@@lastRunSeq)
		lot= $sv.generate_new_lotids(@@maxRunSeqCustomer)[0]
		currSeq= (lot.split('.')[0])[-3..-1]
	end
	assert currSeq==@@lastRunSeq, 'unable to reach last run seq expected: ' + @@lastRunSeq + ' failed after: ' + currSeq
	#check if it is able to generate max run seq
	 lot= $sv.generate_new_lotids(@@maxRunSeqCustomer)[0] #this should fail here.
  end
  
  def nextlot2
	#FabId$CustomerPrefix$YearCode$WorkWeek$RunSeq.$SplitSeq
	runseq_NumericThenAlphanumeric2 # to generate seq
	runseq_NumericThenAlphanumeric2 until !(@@current_seq.include?('I') || @@current_seq.include?('O') ) #to skip if generated seq contains IO
	@@fabID +  @@customerCode +  @@yrCode +  @@workWeek + @@current_seq + '.000'
  end
  
  def nextlot3
	#FabId$CustomerPrefix$YearCode$WorkWeek$RunSeq.$SplitSeq
	runseq_NumericThenAlphanumeric3 # to generate seq
	runseq_NumericThenAlphanumeric3 until !(@@current_seq.include?('I') || @@current_seq.include?('O') ) #to skip if generated seq contains IO
	@@fabID +  @@customerCode +  @@yrCode +  @@workWeek + @@current_seq + '.000'
  end
  
  def runseq_NumericThenAlphanumeric2
    if @@currentcount<1000
      @@current_seq='%.3d' % @@currentcount
	elsif @@currentcount>=1000 and @@currentcount<=3599
      @@current_seq='%.2d' % @@counter1 + @@counter1Char.chr
	  @@counter1Char+=1
	  (@@counter1+=1; @@counter1Char=65) if @@counter1Char==91
	elsif @@currentcount==3600
	  @@counter1=0
	  @@counter1Char=65
	  @@counter2Char=65
	  @@counter3Char=65
	  @@current_seq=@@counter1.to_s + @@counter2Char.chr + @@counter1Char.chr
	  @@counter1Char+=1
	elsif @@currentcount>3600 and @@currentcount<=10359
	  @@current_seq=@@counter1.to_s + @@counter2Char.chr + @@counter1Char.chr
	  @@counter1Char+=1
	  if @@counter2Char==90 and @@counter1Char==91
	    @@counter1+=1
		@@counter1Char=65
		@@counter2Char=65
	  elsif @@counter1Char==91
		@@counter1Char=65
		@@counter2Char+=1
	  end
	elsif @@currentcount==10360
	  @@counter1Char=65
	  @@counter2Char=65
	  @@counter3Char=65
	  @@current_seq=@@counter3Char.chr + @@counter2Char.chr + @@counter1Char.chr
	  @@counter1Char+=1
	  $log.info @@currentcount.to_s + '-->' + @@current_seq
	elsif @@currentcount>10360 and @@currentcount<=27935
	  @@current_seq=@@counter3Char.chr + @@counter2Char.chr + @@counter1Char.chr
	  @@counter1Char+=1
	  if @@counter2Char==90 and @@counter1Char==91
		@@counter1Char=65
		@@counter2Char=65
		@@counter3Char+=1
	  elsif @@counter1Char==91 
	    @@counter1Char=65
		@@counter2Char+=1
	  end
	end
	$log.info(@@currentcount.to_s + '-->' + @@current_seq) if @@currentcount==3599 || @@currentcount==3600 || @@currentcount==3601 || @@currentcount==10358 || @@currentcount==10359 || @@currentcount==10360 || @@currentcount==10361 || @@currentcount==27935
	@@currentcount+=1
  end
  
  def runseq_NumericThenAlphanumeric3
	#skip I,O chars in run seq. runseq_NumericThenAlphanumeric3 has hardcoded logic to remove these characters
	@@counter1Char+=1 if (@@counter1Char.chr==('I') || @@counter1Char.chr==('O') )
	@@counter2Char+=1 if (@@counter2Char.chr==('I') || @@counter2Char.chr==('O') )
	@@counter3Char+=1 if (@@counter3Char.chr==('I') || @@counter3Char.chr==('O') )
	
    if @@currentcount<1000
      @@current_seq='%.3d' % @@currentcount
	elsif @@currentcount>=1000 and @@currentcount<=3399
      @@current_seq= @@counter1Char.chr + '%.2d' % @@counter1 
	  @@counter1+=1
	  (@@counter1=0; @@counter1Char+=1) if @@counter1==100
	elsif @@currentcount==3400
	  @@counter1=0
	  @@counter1Char=65
	  @@counter2Char=65
	  @@counter3Char=65
	  @@current_seq=@@counter2Char.chr + @@counter1Char.chr + @@counter1.to_s 
	  $log.info @@currentcount.to_s + '-->' + @@current_seq
	elsif @@currentcount>3400 and @@currentcount<=9159
	  @@current_seq= @@counter2Char.chr + @@counter1Char.chr + @@counter1.to_s 
	  @@counter1+=1
	  
	  if @@counter1Char==90 and @@counter1==10
	    @@counter1=0
		@@counter1Char=65
		@@counter2Char+=1
	  elsif @@counter1==10
		@@counter1=0
		@@counter1Char+=1
	  end
	elsif @@currentcount==9160
	  @@counter1Char=65
	  @@counter2Char=65
	  @@counter3Char=65
	  @@current_seq=@@counter3Char.chr + @@counter2Char.chr + @@counter1Char.chr
	  @@counter1Char+=1
	  $log.info @@currentcount.to_s + '-->' + @@current_seq
	elsif @@currentcount>9160 and @@currentcount<=9183
	  @@current_seq=@@counter3Char.chr + @@counter2Char.chr + @@counter1Char.chr
	  @@counter1Char+=1
	  if @@counter2Char==90 and @@counter1Char==91
		@@counter1Char=65
		@@counter2Char=65
		@@counter3Char+=1
	  elsif @@counter1Char==91 
	    @@counter1Char=65
		@@counter2Char+=1
	  end
	end
	@@currentcount+=1
  end
  
  
 
  #TODO: Test wafer Lot ID
end
