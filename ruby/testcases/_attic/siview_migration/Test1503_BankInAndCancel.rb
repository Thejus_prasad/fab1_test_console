=begin

(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.


Author: Pradeep, 2014-11-23

History:
  
=end

require 'RubyTestCase'

class Test1502_BankInAndCancel < RubyTestCase
  Description = "Test wcDateIn is set while bank in and removed while bankin cancel"

  
  @@carrier = 'R15%'
  @@product = 'UT-PRODUCT000.01'
  @@route = 'UTRT001.01'
  @@bank = 'UT-RAW'
  
  def test00_setup
	assert @@lot = $sv.new_lot_release(@@params.merge(stb:true)), "Issue with creating lot with all UDATA"
	$log.info "Lot used:: #{@@lot};" 
  end
  
  
  def test01_WC_DATE_IN_on_bankin
		attr=$sv.lot_attributes(@@lot)
		assert attr['wcDateIn'].strip=='',"unexpected wcDateIn in new lot even before bankin #{attr['wcDateIn']}"
		lastOpeNo=$sv.route_operations(@@route).last.opNo #this is expected to be auto bank in operation
		assert $sv.lot_opelocate(@@lot, lastOpeNo), "opearation locate to last opearation(bankin) failed"
		#check auto bank in
		assert $sv.lot_info(@@lot).bank!='', "Auto bankin failed"
		attr=$sv.lot_attributes(@@lot)
		assert attr['wcDateIn'].strip=='',"unexpected wcDateIn in new lot after bankin #{attr['wcDateIn'].strip}"
  end  
  
  def test01_WC_DATE_IN_on_bankinCancel
		attr=$sv.lot_attributes(@@lot)
		assert attr[:wcDateIn].strip!='',"expected lot to be in bank from previous step and with valid wcDateIn value, and its not; actual: #{attr[:wcDateIn]}"
		assert $sv.lot_bankin_cancel(@@lot), 'expecting bank in cancel and lot moved to bank on route'
		attr=$sv.lot_attributes(@@lot)
		assert attr['wcDateIn'].strip=='',"unexpected wcDateIn in new lot after bankin cancel, it should be emptied; actual #{attr['wcDateIn'].strip}"
  end  
end