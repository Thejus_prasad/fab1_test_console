=begin

(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.


Author: Pradeep, 2014-11-12

History:
  
=end

require 'RubyTestCase'


class Test1502_LotUDATA < RubyTestCase
  Description = "Test that All Lot UDATA set from new lot release are showing up in lot inf inq and lot list inq"

  
  @@carrier = "R15%"
  @@product = "UT-PRODUCT000.01"
  @@route = "UTRT001.01"
  @@bank = "UT-RAW"
  @@vendor_product = "UT-RAW.00"
  @@lottypes=[["Vendor",["Vendor"]], ["Dummy",["Dummy","UnittestDummy"]], ["Equipment Monitor",["Equipment Monitor", "UnittestMonitor"]], ["Process Monitor",["Process Monitor"]], ["Recycle",["Recycle"]]]
  @@carriers=""
  @@vendorlot = ""
  @@fab7ALIAS_FORMAT="%2.2d"
  @@fab1_8ALIAS_FORMAT="R15.%2.2d"
  @@ALIAS_FORMAT=""
  @@index=0
  @@time1 = Time.now
  @@time2 = (Time.now + 500000)
  @@time3 = (Time.now + 1000000)
  @@params=nil
  #@@lotAttributes=		["assetteOwner",		"edgePublishFlag",		"expSplits",		"inlineTestCode",		"jobClass",		"lineCode",		"lotLabel",		"lotGrade",		"POI",		"profitAssetteFlag",		"qualityCode",		"lotProductType",		"facilityID",		"famCode",		"supplierLocation",		"year",		"week",		"pendShipFlag",		"disbCompState",		"lotExperimentName",		"demandClass",		"wafFinDueDate",		"dieStkDueDate",		"financeUpdateFlag",		"featureCode",		"dateOfMfg",		"expediteFlag",		"wcDateIn",		"wcModule",		"SPFlag",		"lotOwner",		"lotComment",		"shipDefinition",		"activeRouteFlag",		"activeModuleFlag"]
  def test00_setup
	@@params={ 
		product:@@product,
		route:@@route,
		lottype:"Production",
		sublottype:"PO",
		customer:"gf",
		tktype:"TKTP",
		subcons:"SCON",
		qualitytag:"QTAG",
		assetteOwner: $sv._user.userID.identifier,
		edgePublishFlag: "true",
		expSplits: "10",
		inlineTestCode: "ITC",
		jobClass: "JCLS",
		lineCode: "LCD",
		lotLabel: "1234T12345",
		lotGrade: "A",
		POI: "POI",
		profitAssetteFlag: "true",
		qualityCode: "A",
		lotProductType: "PROTO",
		facilityID: "C",
		famCode: "FDC", #need to check family code dependency
		supplierLocation: "DRS",
		year: "2015",
		week: "01",
		pendShipFlag: "Y",
		disbCompState: "N",
		lotExperimentName: "EXPNAME",
		demandClass: "DCLS",
		wafFinDueDate: @@time3.to_s,
		dieStkDueDate: @@time3.to_s,
		financeUpdateFlag: "true",
		featureCode: "FCD",
		dateOfMfg: @@time1.to_s,
		expediteFlag: "true",
		wcDateIn: @@time1.to_s,
		wcModule: "WCMOD",
		SPFlag: "true",
		lotOwner: $sv._user.userID.identifier,
		lotComment: "Auto generated lot",
		shipDefinition: "SDEF",
		activeRouteFlag: "true",
		activeModuleFlag: "true"
		}
	assert @@lot = $sv.new_lot_release(@@params.merge(stb:true)), "Issue with creating lot with all UDATA"
	$log.info "Lot used:: #{@@lot};" 
	@@FabType = $sv.env=="mtqa4"? "f7":"f1/8"
  end
  
  
  def _test01_lotInfo_siInfo_UDATA
		matched=true
		mismatchList=["lot: #{@@lot}"]
		res = $sv.lot_info(@@lot, raw:true)
		
		
		res.strLotInfo[0].strLotOperationInfo.routeID.identifier==@@route ? "passed" : mismatchList << "Expected route:#{@@route} Actual #{res.strLotInfo[0].strLotOperationInfo.routeID.identifier}"
		res.strLotInfo[0].strLotBasicInfo.lotType=="Production" ? "passed" : mismatchList << "Expected lottype:#{"Production"} Actual #{res.strLotInfo[0].strLotBasicInfo.lotType}"
		res.strLotInfo[0].strLotBasicInfo.subLotType=="PO" ? "passed" : mismatchList << "Expected sublottype:#{"PO"} Actual #{res.strLotInfo[0].strLotBasicInfo.subLotType}"
		res.strLotInfo[0].strLotOrderInfo.customerCode=="gf" ? "passed" : mismatchList << "Expected customer:#{"gf"} Actual #{res.strLotInfo[0].strLotOrderInfo.customerCode}"
		lotAdditonalAttributes=$sv.extract_si($sv.extract_si(res.strLotInfo[0].siInfo).siInfo)
		lotAdditonalAttributes.turnkeyType=="TKTP" ? "passed" : mismatchList << "Expected tktype:#{"TKTP"} Actual #{lotAdditonalAttributes.turnkeyType}"
		lotAdditonalAttributes.subconID=="SCON" ? "passed" : mismatchList << "Expected subcons:#{"SCON"} Actual #{lotAdditonalAttributes.subconID}"
		lotAdditonalAttributes.lot_attrib5=="QTAG" ? "passed" : mismatchList << "Expected qualitytag:#{"QTAG"} Actual #{ lotAdditonalAttributes.lot_attrib5}"
		
		lotAttributes_siInfo=$sv.extract_si(res.strLotInfo[0].siInfo)
		lotAttributes_hash = $sv.result_to_hash(lotAttributes_siInfo)
		
		lotAttributes_siInfo.assetteOwner== $sv._user.userID.identifier ? "passed" : mismatchList << "Expected assetteOwner:#{ $sv._user.userID.identifier} Actual #{ lotAttributes_siInfo.assetteOwner}"
		lotAttributes_siInfo.edgePublishFlag== "true" ? "passed" : mismatchList << "Expected edgePublishFlag:#{ "true"} Actual #{ lotAttributes_siInfo.edgePublishFlag}"
		lotAttributes_siInfo.expSplits== "10" ? "passed" : mismatchList << "Expected expSplits:#{ "10"} Actual #{ lotAttributes_siInfo.expSplits}"
		lotAttributes_siInfo.inlineTestCode== "ITC" ? "passed" : mismatchList << "Expected inlineTestCode:#{ "ITC"} Actual #{ lotAttributes_siInfo.inlineTestCode}"
		lotAttributes_siInfo.jobClass== "JCLS" ? "passed" : mismatchList << "Expected jobClass:#{ "JCLS"} Actual #{ lotAttributes_siInfo.jobClass}"
		lotAttributes_siInfo.lineCode== "LCD" ? "passed" : mismatchList << "Expected lineCode:#{ "LCD"} Actual #{ lotAttributes_siInfo.lineCode}"
		lotAttributes_siInfo.lotLabel== "1234T12345" ? "passed" : mismatchList << "Expected lotLabel:#{ "1234T12345"} Actual #{ lotAttributes_siInfo.lotLabel}"
		lotAttributes_siInfo.lotGrade== "A" ? "passed" : mismatchList << "Expected lotGrade:#{ "A"} Actual #{ lotAttributes_siInfo.lotGrade}"
		lotAttributes_siInfo.POI== "POI" ? "passed" : mismatchList << "Expected POI:#{ "POI"} Actual #{ lotAttributes_siInfo.POI}"
		lotAttributes_siInfo.profitAssetteFlag== "true" ? "passed" : mismatchList << "Expected profitAssetteFlag:#{ "true"} Actual #{ lotAttributes_siInfo.profitAssetteFlag}"
		lotAttributes_siInfo.qualityCode== "A" ? "passed" : mismatchList << "Expected qualityCode:#{ "A"} Actual #{ lotAttributes_siInfo.qualityCode}"
		lotAttributes_siInfo.lotProductType== "PROTO" ? "passed" : mismatchList << "Expected lotProductType:#{ "PROTO"} Actual #{ lotAttributes_siInfo.lotProductType}"
		lotAttributes_siInfo.facilityID== "C" ? "passed" : mismatchList << "Expected facilityID:#{ "C"} Actual #{ lotAttributes_siInfo.facilityID}"
		lotAttributes_siInfo.famCode== "FCD" ? "passed" : mismatchList << "Expected famCode:#{ "FCD"} Actual #{ lotAttributes_siInfo.famCode}"
		lotAttributes_siInfo.supplierLocation== "DRS" ? "passed" : mismatchList << "Expected supplierLocation:#{ "DRS"} Actual #{ lotAttributes_siInfo.supplierLocation}"
		lotAttributes_siInfo.year== "2015" ? "passed" : mismatchList << "Expected year:#{ "2015"} Actual #{ lotAttributes_siInfo.year}"
		lotAttributes_siInfo.week== "01" ? "passed" : mismatchList << "Expected week:#{ "01"} Actual #{ lotAttributes_siInfo.week}"
		lotAttributes_siInfo.pendShipFlag== "Y" ? "passed" : mismatchList << "Expected pendShipFlag:#{ "Y"} Actual #{ lotAttributes_siInfo.pendShipFlag}"
		lotAttributes_siInfo.disbCompState== "N" ? "passed" : mismatchList << "Expected disbCompState:#{ "N"} Actual #{ lotAttributes_siInfo.disbCompState}"
		lotAttributes_siInfo.lotExperimentName== "EXPNAME" ? "passed" : mismatchList << "Expected lotExperimentName:#{ "EXPNAME"} Actual #{ lotAttributes_siInfo.lotExperimentName}"
		lotAttributes_siInfo.demandClass== "DCLS" ? "passed" : mismatchList << "Expected demandClass:#{ "DCLS"} Actual #{ lotAttributes_siInfo.demandClass}"
		lotAttributes_siInfo.wafFinDueDate== siview_timestamp(@@time3.to_s) ? "passed" : mismatchList << "Expected wafFinDueDate:#{ siview_timestamp(@@time3.to_s)} Actual #{ lotAttributes_siInfo.wafFinDueDate}"
		lotAttributes_siInfo.dieStkDueDate== siview_timestamp(@@time3.to_s) ? "passed" : mismatchList << "Expected dieStkDueDate:#{ siview_timestamp(@@time3.to_s)} Actual #{ lotAttributes_siInfo.dieStkDueDate}"
		lotAttributes_siInfo.financeUpdateFlag== "true" ? "passed" : mismatchList << "Expected financeUpdateFlag:#{ "true"} Actual #{ lotAttributes_siInfo.financeUpdateFlag}"
		lotAttributes_siInfo.featureCode== "FCD" ? "passed" : mismatchList << "Expected featureCode:#{ "FCD"} Actual #{ lotAttributes_siInfo.featureCode}"
		lotAttributes_siInfo.dateOfMfg== siview_timestamp(@@time1.to_s) ? "passed" : mismatchList << "Expected dateOfMfg:#{ siview_timestamp(@@time1.to_s)} Actual #{ lotAttributes_siInfo.dateOfMfg}"
		lotAttributes_siInfo.expediteFlag== "true" ? "passed" : mismatchList << "Expected expediteFlag:#{ "true"} Actual #{ lotAttributes_siInfo.expediteFlag}"
		lotAttributes_siInfo.wcDateIn== siview_timestamp(@@time1.to_s) ? "passed" : mismatchList << "Expected wcDateIn:#{ siview_timestamp(@@time1.to_s)} Actual #{ lotAttributes_siInfo.wcDateIn}"
		lotAttributes_siInfo.wcModule== "WCMOD" ? "passed" : mismatchList << "Expected wcModule:#{ "WCMOD"} Actual #{ lotAttributes_siInfo.wcModule}"
		lotAttributes_siInfo.SPFlag== "true" ? "passed" : mismatchList << "Expected SPFlag:#{ "true"} Actual #{ lotAttributes_siInfo.SPFlag}"
		lotAttributes_siInfo.lotOwner== $sv._user.userID.identifier ? "passed" : mismatchList << "Expected lotOwner:#{ $sv._user.userID.identifier} Actual #{ lotAttributes_siInfo.lotOwner}"
		lotAttributes_siInfo.lotComment== "Auto generated lot" ? "passed" : mismatchList << "Expected lotComment:#{ "Auto generated lot"} Actual #{ lotAttributes_siInfo.lotComment}"
		lotAttributes_siInfo.shipDefinition== "SDEF" ? "passed" : mismatchList << "Expected shipDefinition:#{ "SDEF"} Actual #{ lotAttributes_siInfo.shipDefinition}"
		lotAttributes_siInfo.activeRouteFlag== "true" ? "passed" : mismatchList << "Expected activeRouteFlag:#{ "true"} Actual #{ lotAttributes_siInfo.activeRouteFlag}"
		lotAttributes_siInfo.activeModuleFlag== "true" ? "passed" : mismatchList << "Expected activeModuleFlag:#{ "true"} Actual #{ lotAttributes_siInfo.activeModuleFlag}"
		
		assert mismatchList.count==1, mismatchList.join("\n")
  end  
  
  def _test01_lotList_siInfo_UDATA
		matched=true
		mismatchList=["lot: #{@@lot}"]
		res = $sv.lot_list(lot:@@lot, raw:true)
		
				res.strLotListAttributes[0].routeID.identifier==@@route ? "passed" : mismatchList << "Expected route:#{@@route} Actual #{res.strLotListAttributes[0].routeID.identifier}"
		res.strLotListAttributes[0].lotType=="Production" ? "passed" : mismatchList << "Expected lottype:#{""} Actual #{res.strLotListAttributes[0].lotType}"
		res.strLotListAttributes[0].subLotType=="PO" ? "passed" : mismatchList << "Expected sublottype:#{""} Actual #{res.strLotListAttributes[0].subLotType}"
		res.strLotListAttributes[0].customerCode=="gf" ? "passed" : mismatchList << "Expected customer:#{"gf"} Actual #{res.strLotListAttributes[0].customerCode}"
		#---Additional params are not retrieved in lot list tx ------#
		#lotAdditonalAttributes=$sv.extract_si($sv.extract_si(res.strLotInfo[0].siInfo).siInfo)
		#lotAdditonalAttributes.turnkeyType=="TKTP"
 		#matched ? "passed" : mismatchList << "Expected tktype:#{""} Actual #{otAdditonalAttributes.turnkeyType}"
		#lotAdditonalAttributes.subconID=="SCON"
 		#matched ? "passed" : mismatchList << "Expected subcons:#{""} Actual #{lotAdditonalAttributes.subconID}"
		#lotAdditonalAttributes.lot_attrib5=="A"
 		#matched ? "passed" : mismatchList << "Expected qualitytag:#{""} Actual #{ lotAdditonalAttributes.qualitytag}"
		lotAttributes_siInfo=$sv.extract_si(res.strLotListAttributes[0].siInfo).strCS_LotInfo_siInfo
		
		lotAttributes_siInfo.assetteOwner== $sv._user.userID.identifier ? "passed" : mismatchList << "Expected assetteOwner:#{ $sv._user.userID.identifier} Actual #{ lotAttributes_siInfo.assetteOwner}"
		lotAttributes_siInfo.edgePublishFlag== "true" ? "passed" : mismatchList << "Expected edgePublishFlag:#{ "true"} Actual #{ lotAttributes_siInfo.edgePublishFlag}"
		lotAttributes_siInfo.expSplits== "10" ? "passed" : mismatchList << "Expected expSplits:#{ "10"} Actual #{ lotAttributes_siInfo.expSplits}"
		lotAttributes_siInfo.inlineTestCode== "ITC" ? "passed" : mismatchList << "Expected inlineTestCode:#{ "ITC"} Actual #{ lotAttributes_siInfo.inlineTestCode}"
		lotAttributes_siInfo.jobClass== "JCLS" ? "passed" : mismatchList << "Expected jobClass:#{ "JCLS"} Actual #{ lotAttributes_siInfo.jobClass}"
		lotAttributes_siInfo.lineCode== "LCD" ? "passed" : mismatchList << "Expected lineCode:#{ "LCD"} Actual #{ lotAttributes_siInfo.lineCode}"
		lotAttributes_siInfo.lotLabel== "1234T12345" ? "passed" : mismatchList << "Expected lotLabel:#{ "1234T12345"} Actual #{ lotAttributes_siInfo.lotLabel}"
		lotAttributes_siInfo.lotGrade== "A" ? "passed" : mismatchList << "Expected lotGrade:#{ "A"} Actual #{ lotAttributes_siInfo.lotGrade}"
		lotAttributes_siInfo.POI== "POI" ? "passed" : mismatchList << "Expected POI:#{ "POI"} Actual #{ lotAttributes_siInfo.POI}"
		lotAttributes_siInfo.profitAssetteFlag== "true" ? "passed" : mismatchList << "Expected profitAssetteFlag:#{ "true"} Actual #{ lotAttributes_siInfo.profitAssetteFlag}"
		lotAttributes_siInfo.qualityCode== "A" ? "passed" : mismatchList << "Expected qualityCode:#{ "A"} Actual #{ lotAttributes_siInfo.qualityCode}"
		lotAttributes_siInfo.lotProductType== "PROTO" ? "passed" : mismatchList << "Expected lotProductType:#{ "PROTO"} Actual #{ lotAttributes_siInfo.lotProductType}"
		lotAttributes_siInfo.facilityID== "C" ? "passed" : mismatchList << "Expected facilityID:#{ "C"} Actual #{ lotAttributes_siInfo.facilityID}"
		lotAttributes_siInfo.famCode== "FCD" ? "passed" : mismatchList << "Expected famCode:#{ "FCD"} Actual #{ lotAttributes_siInfo.famCode}"
		lotAttributes_siInfo.supplierLocation== "DRS" ? "passed" : mismatchList << "Expected supplierLocation:#{ "DRS"} Actual #{ lotAttributes_siInfo.supplierLocation}"
		lotAttributes_siInfo.year== "2015" ? "passed" : mismatchList << "Expected year:#{ "2015"} Actual #{ lotAttributes_siInfo.year}"
		lotAttributes_siInfo.week== "01" ? "passed" : mismatchList << "Expected week:#{ "01"} Actual #{ lotAttributes_siInfo.week}"
		lotAttributes_siInfo.pendShipFlag== "Y" ? "passed" : mismatchList << "Expected pendShipFlag:#{ "Y"} Actual #{ lotAttributes_siInfo.pendShipFlag}"
		lotAttributes_siInfo.disbCompState== "N" ? "passed" : mismatchList << "Expected disbCompState:#{ "N"} Actual #{ lotAttributes_siInfo.disbCompState}"
		lotAttributes_siInfo.lotExperimentName== "EXPNAME" ? "passed" : mismatchList << "Expected lotExperimentName:#{ "EXPNAME"} Actual #{ lotAttributes_siInfo.lotExperimentName}"
		lotAttributes_siInfo.demandClass== "DCLS" ? "passed" : mismatchList << "Expected demandClass:#{ "DCLS"} Actual #{ lotAttributes_siInfo.demandClass}"
		lotAttributes_siInfo.wafFinDueDate== siview_timestamp(@@time3.to_s) ? "passed" : mismatchList << "Expected wafFinDueDate:#{ siview_timestamp(@@time3.to_s)} Actual #{ lotAttributes_siInfo.wafFinDueDate}"
		lotAttributes_siInfo.dieStkDueDate== siview_timestamp(@@time3.to_s) ? "passed" : mismatchList << "Expected dieStkDueDate:#{ siview_timestamp(@@time3.to_s)} Actual #{ lotAttributes_siInfo.dieStkDueDate}"
		lotAttributes_siInfo.financeUpdateFlag== "true" ? "passed" : mismatchList << "Expected financeUpdateFlag:#{ "true"} Actual #{ lotAttributes_siInfo.financeUpdateFlag}"
		lotAttributes_siInfo.featureCode== "FCD" ? "passed" : mismatchList << "Expected featureCode:#{ "FCD"} Actual #{ lotAttributes_siInfo.featureCode}"
		lotAttributes_siInfo.dateOfMfg== siview_timestamp(@@time1.to_s) ? "passed" : mismatchList << "Expected dateOfMfg:#{ siview_timestamp(@@time1.to_s)} Actual #{ lotAttributes_siInfo.dateOfMfg}"
		lotAttributes_siInfo.expediteFlag== "true" ? "passed" : mismatchList << "Expected expediteFlag:#{ "true"} Actual #{ lotAttributes_siInfo.expediteFlag}"
		lotAttributes_siInfo.wcDateIn== siview_timestamp(@@time1.to_s) ? "passed" : mismatchList << "Expected wcDateIn:#{ siview_timestamp(@@time1.to_s)} Actual #{ lotAttributes_siInfo.wcDateIn}"
		lotAttributes_siInfo.wcModule== "WCMOD" ? "passed" : mismatchList << "Expected wcModule:#{ "WCMOD"} Actual #{ lotAttributes_siInfo.wcModule}"
		lotAttributes_siInfo.SPFlag== "true" ? "passed" : mismatchList << "Expected SPFlag:#{ "true"} Actual #{ lotAttributes_siInfo.SPFlag}"
		lotAttributes_siInfo.lotOwner== $sv._user.userID.identifier ? "passed" : mismatchList << "Expected lotOwner:#{ $sv._user.userID.identifier} Actual #{ lotAttributes_siInfo.lotOwner}"
		lotAttributes_siInfo.lotComment== "Auto generated lot" ? "passed" : mismatchList << "Expected lotComment:#{ "Auto generated lot"} Actual #{ lotAttributes_siInfo.lotComment}"
		lotAttributes_siInfo.shipDefinition== "SDEF" ? "passed" : mismatchList << "Expected shipDefinition:#{ "SDEF"} Actual #{ lotAttributes_siInfo.shipDefinition}"
		lotAttributes_siInfo.activeRouteFlag== "true" ? "passed" : mismatchList << "Expected activeRouteFlag:#{ "true"} Actual #{ lotAttributes_siInfo.activeRouteFlag}"
		lotAttributes_siInfo.activeModuleFlag== "true" ? "passed" : mismatchList << "Expected activeModuleFlag:#{ "true"} Actual #{ lotAttributes_siInfo.activeModuleFlag}"
		
		assert mismatchList.count==1, mismatchList.join("\n")
  end  
  
  def _test02_newlotupdate_lotInfo_siInfo_UDATA
		assert @@lot=$sv.new_lot_release(@@params), "Issue with release new lot"
		
		params={ 
		product:@@product,
		route:@@route,
		lottype:"Production",
		sublottype:"PR",
		customer:"gf",
		tktype:"CKTP",
		subcons:"CCON",
		qualitytag:"CTAG",
		assetteOwner: $sv._user.userID.identifier,
		edgePublishFlag: "false",
		expSplits: "20",
		inlineTestCode: "CTC",
		jobClass: "CCLS",
		lineCode: "CCD",
		lotLabel: "2234T12345",
		lotGrade: "B",
		POI: "COI",
		profitAssetteFlag: "false",
		qualityCode: "B",
		lotProductType: "CROTO",
		facilityID: "F",
		famCode: "", #need to check family code dependency
		supplierLocation: "SGP",
		year: "2016",
		week: "02",
		pendShipFlag: "N",
		disbCompState: "Y",
		lotExperimentName: "CXPNAME",
		demandClass: "CCLS",
		wafFinDueDate: siview_timestamp((@@time3 +1000).to_s),
		dieStkDueDate: siview_timestamp((@@time3 +1000).to_s),
		financeUpdateFlag: "false",
		featureCode: "CCD",
		dateOfMfg: siview_timestamp((@@time1 + 1000).to_s),
		expediteFlag: "false",
		wcDateIn: siview_timestamp((@@time1 + 1000).to_s),
		wcModule: "CCMOD",
		SPFlag: "false",
		lotOwner: $sv._user.userID.identifier,
		lotComment: "CAuto generated lot",
		shipDefinition: "CDEF",
		activeRouteFlag: "false",
		activeModuleFlag: "false"
		}
		assert res=$sv.new_lot_update(@@lot, params.merge(raw:true)), "failure at new lot update "
		puts '----------------------#######################################----------------------'
		puts '----------------------########## NEW LOT UPDATE #############----------------------'
		puts '----------------------#######################################----------------------'
		$sv.show_result
		puts '----------------------###############  END ##################----------------------'
		$log.info "STB new-lot:#{@@lot} after new lot update"
		assert $sv.stb(lot: @@lot), "stb lot after new lot release" #releasedlot info doesnt show all the udata
		matched=true
		mismatchList=["lot: #{@@lot}"]
		res = $sv.lot_info(@@lot, raw:true)
		
		res.strLotInfo[0].strLotOperationInfo.routeID.identifier==@@route? "passed" : mismatchList << "Expected route:#{@@route} Actual #{res.strLotInfo[0].strLotOperationInfo.routeID.identifier}"
		res.strLotInfo[0].strLotBasicInfo.lotType=="Production" ? "passed" : mismatchList << "Expected lottype:#{"Production"} Actual #{res.strLotInfo[0].strLotBasicInfo.lotType}"
		res.strLotInfo[0].strLotBasicInfo.subLotType=="PR" ? "passed" : mismatchList << "Expected sublottype:#{"PR"} Actual #{res.strLotInfo[0].strLotBasicInfo.subLotType}"
		res.strLotInfo[0].strLotOrderInfo.customerCode=="gf" ? "passed" : mismatchList << "Expected customer:#{"gf"} Actual #{res.strLotInfo[0].strLotOrderInfo.customerCode}"
		lotAdditonalAttributes=$sv.extract_si($sv.extract_si(res.strLotInfo[0].siInfo).siInfo)
		lotAdditonalAttributes.turnkeyType=="CKTP" ? "passed" : mismatchList << "Expected tktype:#{"CKTP"} Actual #{lotAdditonalAttributes.turnkeyType}"
		lotAdditonalAttributes.subconID=="CCON" ? "passed" : mismatchList << "Expected subcons:#{"CCON"} Actual #{lotAdditonalAttributes.subconID}"
		lotAdditonalAttributes.lot_attrib5=="B" ? "passed" : mismatchList << "Expected qualitytag:#{"B"} Actual #{ lotAdditonalAttributes.lot_attrib5}"
		lotAttributes_siInfo=$sv.extract_si(res.strLotInfo[0].siInfo)
		lotAttributes_siInfo.assetteOwner== $sv._user.userID.identifier
		
		lotAttributes_siInfo.assetteOwner== $sv._user.userID.identifier ? "passed" : mismatchList << "Expected assetteOwner:#{ $sv._user.userID.identifier} Actual #{ lotAttributes_siInfo.assetteOwner}"
		lotAttributes_siInfo.edgePublishFlag== "false" ? "passed" : mismatchList << "Expected edgePublishFlag:#{ "false"} Actual #{ lotAttributes_siInfo.edgePublishFlag}"
		lotAttributes_siInfo.expSplits== "20" ? "passed" : mismatchList << "Expected expSplits:#{ "20"} Actual #{ lotAttributes_siInfo.expSplits}"
		lotAttributes_siInfo.inlineTestCode== "CTC" ? "passed" : mismatchList << "Expected inlineTestCode:#{ "CTC"} Actual #{ lotAttributes_siInfo.inlineTestCode}"
		lotAttributes_siInfo.jobClass== "CCLS" ? "passed" : mismatchList << "Expected jobClass:#{ "CCLS"} Actual #{ lotAttributes_siInfo.jobClass}"
		lotAttributes_siInfo.lineCode== "CCD" ? "passed" : mismatchList << "Expected lineCode:#{ "CCD"} Actual #{ lotAttributes_siInfo.lineCode}"
		lotAttributes_siInfo.lotLabel== "2234T12345" ? "passed" : mismatchList << "Expected lotLabel:#{ "2234T12345"} Actual #{ lotAttributes_siInfo.lotLabel}"
		lotAttributes_siInfo.lotGrade== "B" ? "passed" : mismatchList << "Expected lotGrade:#{ "B"} Actual #{ lotAttributes_siInfo.lotGrade}"
		lotAttributes_siInfo.POI== "COI" ? "passed" : mismatchList << "Expected POI:#{"COI"} Actual #{ lotAttributes_siInfo.POI}"
		lotAttributes_siInfo.profitAssetteFlag== "false" ? "passed" : mismatchList << "Expected profitAssetteFlag:#{ "false"} Actual #{ lotAttributes_siInfo.profitAssetteFlag}"
		lotAttributes_siInfo.qualityCode== "B" ? "passed" : mismatchList << "Expected qualityCode:#{ "B"} Actual #{ lotAttributes_siInfo.qualityCode}"
		lotAttributes_siInfo.lotProductType== "CROTO" ? "passed" : mismatchList << "Expected lotProductType:#{ "CROTO"} Actual #{ lotAttributes_siInfo.lotProductType}"
		lotAttributes_siInfo.facilityID== "F" ? "passed" : mismatchList << "Expected facilityID:#{ "F"} Actual #{ lotAttributes_siInfo.facilityID}"
		lotAttributes_siInfo.famCode== "CCD" ? "passed" : mismatchList << "Expected famCode:#{ "CCD"} Actual #{ lotAttributes_siInfo.famCode}"
		lotAttributes_siInfo.supplierLocation== "SGP" ? "passed" : mismatchList << "Expected supplierLocation:#{ "SGP"} Actual #{ lotAttributes_siInfo.supplierLocation}"
		lotAttributes_siInfo.year== "2016" ? "passed" : mismatchList << "Expected year:#{ "2016"} Actual #{ lotAttributes_siInfo.year}"
		lotAttributes_siInfo.week== "02" ? "passed" : mismatchList << "Expected week:#{ "02"} Actual #{ lotAttributes_siInfo.week}"
		lotAttributes_siInfo.pendShipFlag== "N" ? "passed" : mismatchList << "Expected pendShipFlag:#{ "N"} Actual #{ lotAttributes_siInfo.pendShipFlag}"
		lotAttributes_siInfo.disbCompState== "Y" ? "passed" : mismatchList << "Expected disbCompState:#{ "Y"} Actual #{ lotAttributes_siInfo.disbCompState}"
		lotAttributes_siInfo.lotExperimentName== "CXPNAME" ? "passed" : mismatchList << "Expected lotExperimentName:#{ "CXPNAME"} Actual #{ lotAttributes_siInfo.lotExperimentName}"
		lotAttributes_siInfo.demandClass== "CCLS" ? "passed" : mismatchList << "Expected demandClass:#{ "CCLS"} Actual #{ lotAttributes_siInfo.demandClass}"
		lotAttributes_siInfo.wafFinDueDate== siview_timestamp((@@time3 +1000).to_s) ? "passed" : mismatchList << "Expected wafFinDueDate:#{ siview_timestamp((@@time3 +1000).to_s)} Actual #{ lotAttributes_siInfo.wafFinDueDate}"
		lotAttributes_siInfo.dieStkDueDate== siview_timestamp((@@time3 +1000).to_s) ? "passed" : mismatchList << "Expected dieStkDueDate:#{ siview_timestamp((@@time3 +1000).to_s)} Actual #{ lotAttributes_siInfo.dieStkDueDate}"
		lotAttributes_siInfo.financeUpdateFlag== "false" ? "passed" : mismatchList << "Expected financeUpdateFlag:#{ "false"} Actual #{ lotAttributes_siInfo.financeUpdateFlag}"
		lotAttributes_siInfo.featureCode== "CCD" ? "passed" : mismatchList << "Expected featureCode:#{ "CCD"} Actual #{ lotAttributes_siInfo.featureCode}"
		lotAttributes_siInfo.dateOfMfg== siview_timestamp((@@time1 +1000).to_s) ? "passed" : mismatchList << "Expected dateOfMfg:#{ siview_timestamp((@@time1 +1000).to_s)} Actual #{ lotAttributes_siInfo.dateOfMfg}"
		lotAttributes_siInfo.expediteFlag== "flase" ? "passed" : mismatchList << "Expected expediteFlag:#{ "false"} Actual #{ lotAttributes_siInfo.expediteFlag}"
		lotAttributes_siInfo.wcDateIn== siview_timestamp((@@time1 +1000).to_s) ? "passed" : mismatchList << "Expected wcDateIn:#{ siview_timestamp((@@time1 +1000).to_s)} Actual #{ lotAttributes_siInfo.wcDateIn}"
		lotAttributes_siInfo.wcModule== "CCMOD" ? "passed" : mismatchList << "Expected wcModule:#{"CCMOD"} Actual #{ lotAttributes_siInfo.wcModule}"
		lotAttributes_siInfo.SPFlag== "false" ? "passed" : mismatchList << "Expected SPFlag:#{"false"} Actual #{ lotAttributes_siInfo.SPFlag}"
		lotAttributes_siInfo.lotOwner== $sv._user.userID.identifier ? "passed" : mismatchList << "Expected lotOwner:#{ $sv._user.userID.identifier} Actual #{ lotAttributes_siInfo.lotOwner}"
		lotAttributes_siInfo.lotComment== "CAuto generated lot" ? "passed" : mismatchList << "Expected lotComment:#{ "CAuto generated lot"} Actual #{ lotAttributes_siInfo.lotComment}"
		lotAttributes_siInfo.shipDefinition== "CDEF" ? "passed" : mismatchList << "Expected shipDefinition:#{ "CDEF"} Actual #{ lotAttributes_siInfo.shipDefinition}"
		lotAttributes_siInfo.activeRouteFlag== "false" ? "passed" : mismatchList << "Expected activeRouteFlag:#{"false"} Actual #{ lotAttributes_siInfo.activeRouteFlag}"
		lotAttributes_siInfo.activeModuleFlag== "false" ? "passed" : mismatchList << "Expected activeModuleFlag:#{"false"} Actual #{ lotAttributes_siInfo.activeModuleFlag}"
		assert mismatchList.count==1, mismatchList.join("\n")
		
  end
  
  def _test02_newlotupdate_lotList_siInfo_UDATA
		
		#Expecting lot attributes are already updated as part of test02_newlotupdate_lotInfo_siInfo_UDATA
		matched=true
		mismatchList=["lot: #{@@lot}"]
		res = $sv.lot_list(lot:@@lot, raw:true)
				res.strLotListAttributes[0].routeID.identifier==@@route ? "passed" : mismatchList << "Expected route:#{@@route} Actual #{res.strLotListAttributes[0].routeID.identifier}"
		res.strLotListAttributes[0].lotType=="Production" ? "passed" : mismatchList << "Expected lottype:#{"Production"} Actual #{res.strLotListAttributes[0].lotType}"
		res.strLotListAttributes[0].subLotType=="PR" ? "passed" : mismatchList << "Expected sublottype:#{"PR"} Actual #{res.strLotListAttributes[0].subLotType}"
		res.strLotListAttributes[0].customerCode=="gf" ? "passed" : mismatchList << "Expected customer:#{"gf"} Actual #{res.strLotListAttributes[0].customerCode}"
		#<------Lot additional attributes are not retrieved in lot list tx -----#
		#lotAdditonalAttributes.turnkeyType=="CKTP"
 		#matched ? "passed" : mismatchList << "Expected tktype:#{"CKTP"} Actual #{otAdditonalAttributes.turnkeyType}"
		#lotAdditonalAttributes.subconID=="CCON"
 		#matched ? "passed" : mismatchList << "Expected subcons:#{"CCON"} Actual #{lotAdditonalAttributes.subconID}"
		#lotAdditonalAttributes.lot_attrib5=="B"
 		#matched ? "passed" : mismatchList << "Expected qualitytag:#{"B"} Actual #{ lotAdditonalAttributes.qualitytag}"
		#------Lot additional attributes are not retrieved in lot list tx ----->#
		lotAttributes_siInfo=$sv.extract_si(res.strLotListAttributes[0].siInfo).strCS_LotInfo_siInfo
	
		lotAttributes_siInfo.assetteOwner== $sv._user.userID.identifier ? "passed" : mismatchList << "Expected assetteOwner:#{ $sv._user.userID.identifier} Actual #{ lotAttributes_siInfo.assetteOwner}"
		lotAttributes_siInfo.edgePublishFlag== "false" ? "passed" : mismatchList << "Expected edgePublishFlag:#{ "false"} Actual #{ lotAttributes_siInfo.edgePublishFlag}"
		lotAttributes_siInfo.expSplits== "20" ? "passed" : mismatchList << "Expected expSplits:#{ "20"} Actual #{ lotAttributes_siInfo.expSplits}"
		lotAttributes_siInfo.inlineTestCode== "CTC" ? "passed" : mismatchList << "Expected inlineTestCode:#{ "CTC"} Actual #{ lotAttributes_siInfo.inlineTestCode}"
		lotAttributes_siInfo.jobClass== "CCLS" ? "passed" : mismatchList << "Expected jobClass:#{ "CCLS"} Actual #{ lotAttributes_siInfo.jobClass}"
		lotAttributes_siInfo.lineCode== "CCD" ? "passed" : mismatchList << "Expected lineCode:#{ "CCD"} Actual #{ lotAttributes_siInfo.lineCode}"
		lotAttributes_siInfo.lotLabel== "2234T12345" ? "passed" : mismatchList << "Expected lotLabel:#{ "2234T12345"} Actual #{ lotAttributes_siInfo.lotLabel}"
		lotAttributes_siInfo.lotGrade== "B" ? "passed" : mismatchList << "Expected lotGrade:#{ "B"} Actual #{ lotAttributes_siInfo.lotGrade}"
		lotAttributes_siInfo.POI== "COI" ? "passed" : mismatchList << "Expected POI:#{"COI"} Actual #{ lotAttributes_siInfo.POI}"
		lotAttributes_siInfo.profitAssetteFlag== "false" ? "passed" : mismatchList << "Expected profitAssetteFlag:#{ "false"} Actual #{ lotAttributes_siInfo.profitAssetteFlag}"
		lotAttributes_siInfo.qualityCode== "B" ? "passed" : mismatchList << "Expected qualityCode:#{ "B"} Actual #{ lotAttributes_siInfo.qualityCode}"
		lotAttributes_siInfo.lotProductType== "CROTO" ? "passed" : mismatchList << "Expected lotProductType:#{ "CROTO"} Actual #{ lotAttributes_siInfo.lotProductType}"
		lotAttributes_siInfo.facilityID== "F" ? "passed" : mismatchList << "Expected facilityID:#{ "F"} Actual #{ lotAttributes_siInfo.facilityID}"
		lotAttributes_siInfo.famCode== "CCD" ? "passed" : mismatchList << "Expected famCode:#{ "CCD"} Actual #{ lotAttributes_siInfo.famCode}"
		lotAttributes_siInfo.supplierLocation== "SGP" ? "passed" : mismatchList << "Expected supplierLocation:#{ "SGP"} Actual #{ lotAttributes_siInfo.supplierLocation}"
		lotAttributes_siInfo.year== "2016" ? "passed" : mismatchList << "Expected year:#{ "2016"} Actual #{ lotAttributes_siInfo.year}"
		lotAttributes_siInfo.week== "02" ? "passed" : mismatchList << "Expected week:#{ "02"} Actual #{ lotAttributes_siInfo.week}"
		lotAttributes_siInfo.pendShipFlag== "N" ? "passed" : mismatchList << "Expected pendShipFlag:#{ "N"} Actual #{ lotAttributes_siInfo.pendShipFlag}"
		lotAttributes_siInfo.disbCompState== "Y" ? "passed" : mismatchList << "Expected disbCompState:#{ "Y"} Actual #{ lotAttributes_siInfo.disbCompState}"
		lotAttributes_siInfo.lotExperimentName== "CXPNAME" ? "passed" : mismatchList << "Expected lotExperimentName:#{ "CXPNAME"} Actual #{ lotAttributes_siInfo.lotExperimentName}"
		lotAttributes_siInfo.demandClass== "CCLS" ? "passed" : mismatchList << "Expected demandClass:#{ "CCLS"} Actual #{ lotAttributes_siInfo.demandClass}"
		lotAttributes_siInfo.wafFinDueDate== siview_timestamp((@@time3 +1000).to_s) ? "passed" : mismatchList << "Expected wafFinDueDate:#{ siview_timestamp((@@time3 +1000).to_s)} Actual #{ lotAttributes_siInfo.wafFinDueDate}"
		lotAttributes_siInfo.dieStkDueDate== siview_timestamp((@@time3 +1000).to_s) ? "passed" : mismatchList << "Expected dieStkDueDate:#{ siview_timestamp((@@time3 +1000).to_s)} Actual #{ lotAttributes_siInfo.dieStkDueDate}"
		lotAttributes_siInfo.financeUpdateFlag== "false" ? "passed" : mismatchList << "Expected financeUpdateFlag:#{ "false"} Actual #{ lotAttributes_siInfo.financeUpdateFlag}"
		lotAttributes_siInfo.featureCode== "CCD" ? "passed" : mismatchList << "Expected featureCode:#{ "CCD"} Actual #{ lotAttributes_siInfo.featureCode}"
		lotAttributes_siInfo.dateOfMfg== siview_timestamp((@@time1 +1000).to_s) ? "passed" : mismatchList << "Expected dateOfMfg:#{ siview_timestamp((@@time1 +1000).to_s)} Actual #{ lotAttributes_siInfo.dateOfMfg}"
		lotAttributes_siInfo.expediteFlag== "flase" ? "passed" : mismatchList << "Expected expediteFlag:#{ "false"} Actual #{ lotAttributes_siInfo.expediteFlag}"
		lotAttributes_siInfo.wcDateIn== siview_timestamp((@@time1 +1000).to_s) ? "passed" : mismatchList << "Expected wcDateIn:#{ siview_timestamp((@@time1 +1000).to_s)} Actual #{ lotAttributes_siInfo.wcDateIn}"
		lotAttributes_siInfo.wcModule== "CCMOD" ? "passed" : mismatchList << "Expected wcModule:#{"CCMOD"} Actual #{ lotAttributes_siInfo.wcModule}"
		lotAttributes_siInfo.SPFlag== "false" ? "passed" : mismatchList << "Expected SPFlag:#{"false"} Actual #{ lotAttributes_siInfo.SPFlag}"
		lotAttributes_siInfo.lotOwner== $sv._user.userID.identifier ? "passed" : mismatchList << "Expected lotOwner:#{ $sv._user.userID.identifier} Actual #{ lotAttributes_siInfo.lotOwner}"
		lotAttributes_siInfo.lotComment== "CAuto generated lot" ? "passed" : mismatchList << "Expected lotComment:#{ "CAuto generated lot"} Actual #{ lotAttributes_siInfo.lotComment}"
		lotAttributes_siInfo.shipDefinition== "CDEF" ? "passed" : mismatchList << "Expected shipDefinition:#{ "CDEF"} Actual #{ lotAttributes_siInfo.shipDefinition}"
		lotAttributes_siInfo.activeRouteFlag== "false" ? "passed" : mismatchList << "Expected activeRouteFlag:#{"false"} Actual #{ lotAttributes_siInfo.activeRouteFlag}"
		lotAttributes_siInfo.activeModuleFlag== "false" ? "passed" : mismatchList << "Expected activeModuleFlag:#{"false"} Actual #{ lotAttributes_siInfo.activeModuleFlag}"
		
		assert mismatchList.count==1, mismatchList.join("\n")
		
  end
end