=begin

(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.


Author: Pradeep, 2014-11-12

History:
  
=end

require 'RubyTestCase'


class Test1502_WaferAlias < RubyTestCase
  Description = "Test that wafer alias name are set and able to get the changed info"

  
  @@product = "UT-PRODUCT000.01"
  @@fab7ALIAS_FORMAT="%2.2d"
  @@fab1_8ALIAS_FORMAT="R15.%2.2d"
  @@ALIAS_FORMAT=""
  @@lot=""
  
  def test00_setup
	assert @@lot=$sv.new_lot_release(product:@@product, stb:true), "Check new lot is created" 
	@@ALIAS_FORMAT = $sv.env=="mtqa4" ? @@fab7ALIAS_FORMAT : @@fab1_8ALIAS_FORMAT
	$log.info "Lot used:: #{@@lot}; Expected Alias format: #{@@ALIAS_FORMAT}" 
  end
  
  
  def test01_checkWaferAliasNameSetFromSTBTx
	matched=true
	mismatchList=[]
	wf = $sv.lot_info(@@lot, wafers:true).wafers
	wf.each_with_index{|w, i|
		matched=matched && w.alias==@@ALIAS_FORMAT % (i+1) ? true : false 
		matched ? "passed" : mismatchList << (w.wafer +" expected: #{w.alias}; actual: #{@@ALIAS_FORMAT % (i+1)}") 
	}
	assert matched, "some wafer alias didnt match: #{mismatchList.join("; ")}"
  end  
  
  def test02_checkWaferAliasInfo
	matched=true
	mismatchList=[]
	wf = $sv.wafer_alias_info(lot:@@lot)
	wf.each_with_index{|w, i|
		matched = matched && w==@@ALIAS_FORMAT % (i+1) ? true : false 
		matched ? "passed" : mismatchList << " expected: #{w}; actual: #{@@ALIAS_FORMAT % (i+1)}" 
	}
	assert matched, "some wafer alias didnt match: #{mismatchList.join("; ")}"
  end
  
  def test02_checkWaferAliasSet
	matched=true
	mismatchList=[]
	wf = $sv.wafer_alias_info(lot:@@lot)
	aliases = wf.collect{|w| w +"_NEW" }
	$sv.wafer_alias_set @@lot, aliases:aliases
	wf = $sv.wafer_alias_info(lot:@@lot)
	wf.each_with_index{|w, i|
		matched=matched && w==aliases[i] ? true : false 
		matched ? "passed" : mismatchList << " expected: #{w}; actual: #{aliases[i]}"
	}
	assert matched, "some wafer alias didnt match: #{mismatchList.join("; ")}"
  end
  
end
