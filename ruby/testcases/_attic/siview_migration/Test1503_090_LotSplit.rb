=begin 
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.
    
Author: Steffen Steidten, 2012-04-16

=end

require "RubyTestCase"

# Test TxLotSplit et al. including MSR57546
# added MSR80115, MSR529507
class Test1503_090_LotSplit < RubyTestCase
  Description = "Test TxLotSplit et al. including MSR57546"

  @@lot_script_param = "UTstring" #just with inheritence
  @@sparam_merge_check = "UTmergecheck" #with inheritance and mere check
#  ERPItem		123	STRING	<INHERITING> ERP Item

  
  @@rework_route = "UTRW001.01"
  @@rework_op = "1000.400"
  @@rework_ret = "1000.400"

  @@psm_route = "UTBR001.01"
  @@psm_split_op = "1000.200"
  @@psm_return_op = "1000.300"
  
  @@hold_reason = "AEHL"
  
  @@vlot = "VL000DD.00"
  
  def self.startup
    super
    @@lot = $sv.new_lot_release(product:"UT-PRODUCT000.01", stb:true) #$sv.lot_list(status: "Waiting", route: "UTRT001.01" )[0]
    $log.info "using lot #{@@lot}"        
  end
    
  def _test01_split_merge_multiple_times
    $sv.lot_cleanup(@@lot)
    3.times {|i|
      $log.info "pass #{i}"
      $sv.user_parameter_change("Lot", @@lot, @@lot_script_param, "QA#{i}")
      lc = $sv.lot_split(@@lot, ["01", "02"])
      assert lc
      assert_equal "QA#{i}", $sv.user_parameter("Lot", lc, :name=>@@lot_script_param).value
      assert_equal 0, $sv.lot_merge(@@lot, nil)
    }
  end
  
  def _test02_split_merge_lot_onhold
    $sv.lot_cleanup(@@lot)
	$sv.lot_merge(@@lot, nil)
    assert_equal 0, $sv.lot_hold(@@lot, "ENG")
    3.times {|i|
      $log.info "pass #{i}"
      $sv.user_parameter_change("Lot", @@lot, @@lot_script_param, "QA#{i}")
      lc = $sv.lot_split(@@lot, ["01", "02"], :onhold=>true)
	  assert lc
	  assert $sv.lot_info(@@lot).status=="ONHOLD", "parent lot hold released!"
	  assert $sv.lot_info(lc).status=="ONHOLD", "failed to inherit lot hold"
      assert_equal "QA#{i}", $sv.user_parameter("Lot", lc, :name=>@@lot_script_param).value
      assert_equal 0, $sv.lot_merge(@@lot, nil)
    }
  end
  
  def test02_split_merge_lot_onhold_with_holdrelease
    $sv.lot_cleanup(@@lot)
	$sv.lot_merge(@@lot, nil)
    
    3.times {|i|
	  assert_equal 0, $sv.lot_hold(@@lot, "ENG") #hold lot
      $log.info "pass #{i}"
      $sv.user_parameter_change("Lot", @@lot, @@lot_script_param, "QA#{i}")
      lc = $sv.lot_split(@@lot, ["01", "02"], :onhold=>true, release:true)
      assert lc
	  assert $sv.lot_info(@@lot).status!="ONHOLD", "parent lot hold is not released!"
	  assert $sv.lot_info(lc).status!="ONHOLD", "child lot is still on hold"
      assert_equal "QA#{i}", $sv.user_parameter("Lot", lc, :name=>@@lot_script_param).value
      #assert_equal 0, $sv.lot_merge(@@lot, nil)
    }
  end
  
  def _test03_split_actions
    $sv.lot_cleanup(@@lot)
	$sv.lot_merge(@@lot, nil)
    $sv.user_parameter_change("Lot", @@lot, @@lot_script_param, "VALUE")
    children = do_split_actions($sv, @@lot, 
      @@rework_route, @@rework_op, @@rework_ret, 
      @@psm_route, @@psm_split_op, @@psm_return_op, @@psm_return_op)
    # check all children for script parameter
    $log.info "#{children.inspect}"
    assert children.inject(true) {|res,clot|
      res &= verify_equal "VALUE", $sv.user_parameter("Lot", clot, :name=>@@lot_script_param).value, " wrong value for #{clot}"
    }, "failed inheritance check"
    # Merge
    assert $sv.merge_lot_family(@@lot, :nosort=>true)
  end
  
  def _test10_split_mergecheck
    $sv.lot_cleanup(@@lot)
	$sv.lot_merge(@@lot, nil)
    assert do_action(@@lot, 
      lambda {|lot| $sv.lot_split(lot, 1)}, 
      lambda {|lot,child| $sv.lot_merge(lot, child, raw: true)[0]}), "failed split-merge"
  end
    
  def _test11_partial_rework
    $sv.lot_cleanup(@@lot)
	$sv.lot_merge(@@lot, nil)
    assert do_action(@@lot, 
      lambda {|lot| 
        assert_equal 0, $sv.lot_opelocate(@@lot, @@rework_op), "Failed to locate to rework"
        $sv.lot_partial_rework(@@lot, 1, @@rework_route, :return_opNo=>@@rework_ret)    
      },
      lambda {|lot,child|
        $sv.lot_partial_rework_cancel(lot, child, raw: true)
      }), "failed partial rework"
  end
  
  def _test12_split_merge_notonroute
	$sv.lot_merge(@@lot, nil)
    assert do_action(@@vlot, 
      lambda {|lot| 
        $sv.lot_split_notonroute(@@vlot, 1, )
      },
      lambda {|lot,child|
        $sv.lot_merge_notonroute(lot, child, raw: true)[0]
      }), "failed split-merge not on route"
  end
  
  # split - merge with changed script parameters
  def do_action(lot, split_action, merge_action)
	assert $sv.user_parameter_set_verify("Lot", lot, @@sparam_merge_check, "VALUE1")
    child = split_action.(lot)
    assert child, "failed to split lot"
    assert_equal "VALUE1", $sv.user_parameter("Lot", child, :name=>@@sparam_merge_check).value
        
    # modify value
    assert $sv.user_parameter_set_verify("Lot", child, @@sparam_merge_check, "VALUE2")    
    res = merge_action.(lot, child)
    ok  = verify_equal 157, $sv.rc(res)
    ok &= verify_contains "[#{@@sparam_merge_check}], Parent: [VALUE1], Child: [VALUE2]", $sv.msg_text(res)
    
    # empty value
    assert $sv.user_parameter_set_verify("Lot", lot, @@sparam_merge_check, "")    
    res = merge_action.(lot, child)
    ok  = verify_equal 157, $sv.rc(res)
    ok &= verify_contains "[#{@@sparam_merge_check}], Parent: [], Child: [VALUE2]", $sv.msg_text(res)
    
    # equal value
    assert $sv.user_parameter_set_verify("Lot", lot, @@sparam_merge_check, "VALUE1")
    assert $sv.user_parameter_set_verify("Lot", child, @@sparam_merge_check, "VALUE1")
    ok &= verify_equal 0, $sv.rc(merge_action.(lot, child))
    return ok
  end
  
  def do_split_actions(svuser, lot, rework_route, rework_op, rework_ret, branch_route=nil, psm_split_op=nil, psm_return_op=nil, psm_merge_op=nil)
    children = []
    assert verify_condition("Failed to merge lots") {
      # Normal split
      children << svuser.lot_split(lot, 1)
      # Onhold split
      assert_equal 0, svuser.lot_hold(lot, @@hold_reason), "Hold failed"
      # MSR717973: SplitWithoutLotHoldRelease
      children << svuser.lot_split(lot, 1, :onhold=>true)
      children << svuser.lot_split(lot, 1, :onhold=>true, :release=>true)
      # Auto split
      children << svuser.AMD_WaferSorterAutoSplitReq(lot, 1)
      # MSR717973: PSM in backup
      if branch_route
        assert_equal 0, svuser.lot_experiment(lot, 1, branch_route, psm_split_op, psm_return_op, :merge=>psm_merge_op), "Failed to setup PSM"
        reset = false
        2.times do
          if reset
            assert_equal 0, svuser.lot_experiment_reset(lot, psm_split_op), "Failed to reset PSM"
          end

          assert_equal 0, svuser.lot_opelocate(lot, psm_split_op), "Failed to do PSM split"
          sublot = svuser.lot_family(lot)[-1]
          children << sublot
          [lot,sublot].each { |lot| assert_equal 0, svuser.lot_opelocate(lot, psm_return_op), "Failed to locate to return" }
          assert_equal 0, svuser.lot_merge(lot, sublot), "Failed to merge lots"
          #
          reset = true
        end          

        assert svuser.lot_experiment_delete(lot), "Failed to clear PSM"
      end
      # MSR717973: Split on bank -> NA

      # FPC --> extra test


      # Partial Rework
      assert_equal 0, svuser.lot_opelocate(lot, rework_op), "Failed to locate to rework"
      child = svuser.lot_partial_rework(lot, 1, rework_route, :return_opNo=>rework_ret)
      children << child
      assert_equal 0, svuser.lot_partial_rework_cancel(lot, child), "Failed to merge after rework"
      # Partial Rework w/o hold release
      assert_equal 0, svuser.lot_hold(lot, @@hold_reason), "Hold failed"
      child = svuser.lot_partial_rework(lot, 1, rework_route, :return_opNo=>rework_ret, :onhold=>true)
      children << child
      assert_equal 0, svuser.lot_partial_rework_cancel(lot, child), "Failed to merge after rework"
      # Partial Rework with hold release
      child = svuser.lot_partial_rework(lot, 1, rework_route, :return_opNo=>rework_ret, :onhold=>true, :release=>true)
      children << child
      assert_equal 0, svuser.lot_partial_rework_cancel(lot, child), "Failed to merge after rework"
      #assert svuser.lot_hold_release(lot, nil), "Failed to release lot"
      true
    }
    return children
  end
  
  def self.after_all_passed
    $sv.lot_cleanup(@@lot)
  end
end
