=begin

(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.


Author: Conny Stenzel, 2010-12-06

History:
  2012-11-30 ssteidte,  moved LIG logic into a separate library
=end

require 'RubyTestCase'


class R15_VendorlotPreparationAndCancel < RubyTestCase
  Description = "Test vendor lot preparation then check prepared lot info and then cancel"

  
  @@carrier = "R15%"
  @@product = "UT-PRODUCT000.01"
  @@route = "UTRT001.01"
  @@bank = "UT-RAW"
  @@vendor_product = "UT-RAW.00"
  @@lottypes=[["Vendor",["Vendor"]], ["Dummy",["Dummy","UnittestDummy"]], ["Equipment Monitor",["Equipment Monitor", "UnittestMonitor"]], ["Process Monitor",["Process Monitor"]], ["Recycle",["Recycle"]]]
  @@carriers=""
  @@vendorlot = ""
  @@index=0
  
  def test00_setup
	assert @@vendorlot=$sv.vendorlot_receive(@@bank, @@vendor_product, 400), "Vendor lot receive" 
	$log.info "vendor lot receive: #{@@vendorlot}"
    @@carriers = $sv.carrier_list(status: 'NOTAVAILABLE', empty: true, carrier: @@carrier)
    $sv.carrier_status_change(@@carriers, 'AVAILABLE')
    @@carriers = $sv.carrier_list(status: 'AVAILABLE', empty: true, carrier: @@carrier)
	$log.info " @@carriers.count: #{@@carriers.count}"
    assert @@carriers.size > 50, "not enough carriers, need 50 empty carriers #{@@carrier.inspect}"
  end
  
  #receive,prepare,cancel info, cancel, validate new vendor lot 
  def test02_vendorlot_receive_prepare_cancel
	
	@@lottypes.each{ |lottype| 
		type=lottype[0]
		lottype[1].each{ |sublottype|
			#check vendor lot prepration for all lot types + sub lot types combination
			 $log.info "vendorlot_prepare vendorlot:#{@@vendorlot}, carrier:#{@@carriers[@@index]}, lottype:#{type}, sublottype:#{sublottype}"
			 assert vlot=$sv.vendorlot_prepare(@@vendorlot, @@carriers[@@index],lottype:type, sublottype:sublottype), "vendor lot preparation"
			#check prepared lot info
			$log.info "Do vendorlot_preparation_cancel_info for preparedlot:#{vlot}"
			assert res=$sv.vendorlot_preparation_cancel_info(vlot), "Failed to inquire vendorlot_preparation_cancel_info"
			if (res.strPreparationCancelledLotInfo.lotID.identifier== vlot &&
				res.strPreparationCancelledLotInfo.lotType==type &&
				res.strPreparationCancelledLotInfo.subLotType==sublottype &&
				res.strPreparationCancelledLotInfo.lotStatus=="FINISHED" &&
				res.strPreparationCancelledLotInfo.productID.identifier==@@vendor_product &&
				res.strPreparationCancelledLotInfo.cassetteID.identifier==@@carriers[@@index] &&
				res.strPreparationCancelledLotInfo.bankID.identifier==@@bank &&
				res.strNewVendorLotInfoSeq[0].originalVendorLotID== @@vendorlot &&
				res.strNewVendorLotInfoSeq[0].subLotType== "Vendor" &&
				res.strNewVendorLotInfoSeq[0].productID.identifier==@@vendor_product &&
				res.strNewVendorLotInfoSeq[0].vendorLotID=="" &&
				res.strNewVendorLotInfoSeq[0].vendorName==""
				)
				assert true, "some data in vendorlot_preparation_cancel_info retrun struct is not as expected"
			else
				assert false, "some data in vendorlot_preparation_cancel_info retrun struct is not as expected"
			end
			#cancel preprared vendor lot
			assert !res=$sv.vendorlot_preparation_cancel(vlot), "vendorlot_preparation_cancel is expected to fail as it still has carrier associated with it, but passed"
			assert $sv.wafer_sort_req(vlot,""), "failed to remove wafers from carrier"
			wafercount = $sv.lot_info(vlot).nwafers
			assert new_vlot=$sv.vendorlot_preparation_cancel(vlot), "Failed vendorlot_preparation_cancel"
			new_vlot=new_vlot[0]#above statement is returing array of lots, but it will always have one lot in prepration cancel
			$log.info "newly created vendor lot : #{new_vlot}"
			assert !$sv.lot_info(new_vlot).wafers, "Wafers are not removed from siview" #check wafers are removed from siview new lot
			assert_equal wafercount,$sv.lot_info(new_vlot).nwafers #check wafercount matches from old lot in new lot
			assert_equal 0,$sv.lot_info(vlot).nwafers #check wafers are removed from prepred lot
			#-- need to check if new vendor lot is generated!!
			@@index+=1
		}
		
	}
  end  
  
  #receive,prepare,cancel info, cancel, validate new vendor lot 
  #receive,prepare,cancel info, cancel, validate new vendor lot 
  def test03_vendorlot_prepare_cancel_for_partially_used_prepared_lot
  
	@@lottypes=[["Vendor",["Vendor"]], ["Dummy",["Dummy","UnittestDummy"]], ["Equipment Monitor",["Equipment Monitor", "UnittestMonitor"]], ["Process Monitor",["Process Monitor"]], ["Recycle",["Recycle"]]]
	
	carrier=
	@@lottypes.each{ |lottype| 
		type=lottype[0]
		lottype[1].each{ |sublottype|
			#check vendor lot prepration for all lot types + sub lot types combination
			 $log.info "vendorlot_prepare vendorlot:#{@@vendorlot}, carrier:#{@@carriers[@@index]}, lottype:#{type}, sublottype:#{sublottype}"
			 assert vlot=$sv.vendorlot_prepare(@@vendorlot, @@carriers[@@index],lottype:"Vendor", sublottype:"Vendor"), "vendor lot preparation"
			#check prepared lot info
			$log.info "Do vendorlot_preparation_cancel_info for preparedlot:#{vlot}"
			assert res=$sv.vendorlot_preparation_cancel_info(vlot), "Failed to inquire vendorlot_preparation_cancel_info"
			if (res.strPreparationCancelledLotInfo.lotID.identifier== vlot &&
				res.strPreparationCancelledLotInfo.lotType=="Vendor" &&
				res.strPreparationCancelledLotInfo.subLotType=="Vendor" &&
				res.strPreparationCancelledLotInfo.lotStatus=="FINISHED" &&
				res.strPreparationCancelledLotInfo.productID.identifier==@@vendor_product &&
				res.strPreparationCancelledLotInfo.cassetteID.identifier==@@carriers[@@index] &&
				res.strPreparationCancelledLotInfo.bankID.identifier==@@bank &&
				res.strNewVendorLotInfoSeq[0].originalVendorLotID== @@vendorlot &&
				res.strNewVendorLotInfoSeq[0].subLotType== "Vendor" &&
				res.strNewVendorLotInfoSeq[0].productID.identifier==@@vendor_product &&
				res.strNewVendorLotInfoSeq[0].vendorLotID=="" &&
				res.strNewVendorLotInfoSeq[0].vendorName==""
				)
				assert true, "some data in vendorlot_preparation_cancel_info retrun struct is not as expected"
			else
				assert false, "some data in vendorlot_preparation_cancel_info retrun struct is not as expected"
			end
			#@@index+=1
			assert vlot1=$sv.vendorlot_prepare(vlot, @@carriers[@@index],lottype:type, sublottype:sublottype, nwafers:5), "vendor lot preparation"
			$log.info "Do vendorlot_preparation_cancel_info fpr preparedlot:#{vlot1} which is prepared from already prepared lot"
			assert res=$sv.vendorlot_preparation_cancel_info(vlot1), "Failed to inquire vendorlot_preparation_cancel_info"
			if (res.strPreparationCancelledLotInfo.lotID.identifier== vlot1 &&
				res.strPreparationCancelledLotInfo.lotType==type &&
				res.strPreparationCancelledLotInfo.subLotType==sublottype &&
				res.strPreparationCancelledLotInfo.lotStatus=="FINISHED" &&
				res.strPreparationCancelledLotInfo.productID.identifier==@@vendor_product &&
				res.strPreparationCancelledLotInfo.cassetteID.identifier==@@carriers[@@index] &&
				res.strPreparationCancelledLotInfo.bankID.identifier==@@bank &&
				res.strNewVendorLotInfoSeq[0].originalVendorLotID==  @@vendorlot&&
				res.strNewVendorLotInfoSeq[0].subLotType== "Vendor" &&
				res.strNewVendorLotInfoSeq[0].productID.identifier==@@vendor_product &&
				res.strNewVendorLotInfoSeq[0].vendorLotID=="" &&
				res.strNewVendorLotInfoSeq[0].vendorName==""
				)
				assert true, "some data in vendorlot_preparation_cancel_info retrun struct is not as expected"
			else
				assert false, "some data in vendorlot_preparation_cancel_info retrun struct is not as expected"
			end
			#cancel preprared vendor lot which is already used
			assert !res=$sv.vendorlot_preparation_cancel(vlot), "vendorlot_preparation_cancel is expected to fail as it still has carrier associated with it, but passed"
			assert $sv.wafer_sort_req(vlot,""), "failed to remove wafers from carrier"
			wafercount = $sv.lot_info(vlot).nwafers #should be 20 as 5 were already used.
			assert new_vlot=$sv.vendorlot_preparation_cancel(vlot), "Failed vendorlot_preparation_cancel"
			new_vlot=new_vlot[0]#above statement is returing array of lots, but it will always have one lot in prepration cancel
			$log.info "newly created vendor lot : #{new_vlot}"
			assert !$sv.lot_info(new_vlot).wafers, "Wafers are not removed from siview" #check wafers are removed from siview new lot
			assert_equal wafercount,$sv.lot_info(new_vlot).nwafers #check wafercount matches from old lot in new lot
			assert_equal 0,$sv.lot_info(vlot).nwafers #check wafers are removed from prepred lot
			#-- need to check if new vendor lot is generated!!
			
			#cancel preprared vendor lot which was prepared from already prepared lot which is canceled now.
			assert !res=$sv.vendorlot_preparation_cancel(vlot1), "vendorlot_preparation_cancel is expected to fail as it still has carrier associated with it, but passed"
			assert $sv.wafer_sort_req(vlot1,""), "failed to remove wafers from carrier"
			wafercount = $sv.lot_info(vlot1).nwafers #should be 5
			assert new_vlot=$sv.vendorlot_preparation_cancel(vlot1), "Failed vendorlot_preparation_cancel"
			new_vlot=new_vlot[0]#above statement is returing array of lots, but it will always have one lot in prepration cancel
			$log.info "newly created vendor lot : #{new_vlot}"
			assert !$sv.lot_info(new_vlot).wafers, "Wafers are not removed from siview" #check wafers are removed from siview new lot
			assert_equal wafercount,$sv.lot_info(new_vlot).nwafers #check wafercount matches from old lot in new lot
			assert_equal 0,$sv.lot_info(vlot).nwafers #check wafers are removed from prepred lot
			
			@@index+=1
		}
		
	}
  end  
  
  
end
