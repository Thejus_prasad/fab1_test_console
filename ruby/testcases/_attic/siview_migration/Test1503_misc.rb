=begin

(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.


Author: Pradeep, 2014-11-23

History:
  
=end

require 'SiViewTestCase'
require 'siviewmmdb'

class Test1503_misc < SiViewTestCase
  Description = "Test sublot type change at various states"

  #production lot#
  @@product = 'UT-PRODUCT000.01'
  @@route = 'UTRT001.01'
  @@stoker = 'UTSTO110'
  #@@routeOperations=[[expected module pd,[various pds of module pd(some first middle, last)]]
  @@routeOperations=[["UTMD000.01", ["1000.100","1000.950", "1000.990", "2000.990"]], ["UT-MD-SIMPLE01.01", ["2500.200"]], ["UT-MD-LP1.00", ["2700.100"]], ["BANKIN.01", ["3000.1000"]]]
  @@lastOpeNo='3000.1000'
  @@holdreason='AEHL'
  @@nonprobank='UT-USE'
=begin
	sub process of UTRT001.01
	1000.200	UTPD001.01	e0001-TEST-A0.01	1000.400
	1000.200	UTPD001.01	UTBR001.01	1000.300
	1000.200	UTPD001.01	UTBR002.01	1000.400
	1000.300	UTPD002.01	e0005-TEST-A0.01	1000.400
	1000.400	UTPD003.02	e0003-TEST-A0.01	1000.500
	1000.400	UTPD003.02	UTBR002.01	1000.500
	2000.300	UTPD002.01	UTBR000.01	2000.990
	2000.500	UTPD004.01	UTBR002.01	2000.500
	2000.600	UTPD005.02	UTBR002.01	1000.200
=end

#@@branch=[["branchRoute", "branc at pd", ["joining opeNo", mpd],[["branch op",respective mod pd],.., ["last branch op",modpd]]]
  @@branch=[["e0001-TEST-A0.01", "1000.200", ["1000.400","UTMD000.01"], [["e1000.1000","e0001-TEST-A0.01"], ["e1000.1300","e0001-TEST-A0.01"]]]] 
			#,["UTPD002.01", "2000.300", [["e1000.1000","UTMD000.01"], ["e1000.1300",""], ["2000.990",""]]]]
			
=begin
		2	1000.150	UTPD000.02	UTRWK01.01	1000.400
		4	1000.300	UTPD002.01	UTRW001.01	1000.400
		4	1000.300	UTPD002.01	UTRWK01.01	2000.990
		5	1000.400	UTPD003.02	UTRW001.01	1000.300
		8	1000.600	UTPD005.02	UTRW003.01	1000.700
		9	1000.700	UTPD006.01	UTRW002.01	1000.800
		9	1000.700	UTPD006.01	UTRW003.01	1000.700
		9	1000.700	UTPD006.01	UTRWK01.01	2000.800
		14	1000.990	UTPD011.01	UTRW001.01	1000.990
		18	2000.300	UTPD002.01	UTRW001.01	2000.400
=end
#@@rework=[["reworkRoute", "rework at pd", ["joining opeNo", mpd],[["rework op",respective mod pd],.., ["last rework op",modpd]]]
  @@rework=[["UTRWK01.01", "1000.150", ["1000.400","UTMD000.01"], [["RW1.e300","UTRMD00.01"], ["RW1.e400","UTRMD00.01"], ["RW1.e600","UTRMD00.01"]]]] 

  @@current='PO'
  @@changeto='PR'
  @@memo='memo'
  @@tx=[
	#[CS_TxCmpCurRouteInq, ''],
	#['CS_TxHoldClaimMemoUpdateReq','memo'],
	#['CS_TxRemeasureLocateReq', ''],
	#['CS_TxLotSubconIDChangeReq', 'SUBCON_ID'],
	['CS_TxLotExpediteFlagChangeReq','EXPEDITE_FLAG'],
	['CS_TxLotDateOfMfgChangeReq', 'DATE_OF_MFG'],
	['CS_TxLotDemandClassChangeReq', 'DEMAND_CLASS'],
	['CS_TxLotDieStkDueDateChangeReq', 'DIE_STK_DUE_DATE'],
	['CS_TxLotFeatureCodeChangeReq', 'FEATURE_CODE'],
	['CS_TxLotFinanceUpdateFlagChangeReq','FINANCE_UPDATE_FLAG'],
	['CS_TxLotGradeChangeReq', 'LOT_GRADE'],
	['CS_TxLotInlineTestCodeChangeReq', 'INLINE_TEST_CODE'],
	['CS_TxLotQualityCodeChangeReq','QUALITY_CODE'],
	['CS_TxLotWafFinDueDateChangeReq', 'WAF_FIN_DUE_DATE'],
	['CS_TxLotAttributeChangeReq','ATTRIB2']
	]
	@@lot = $sv.f7 ?  'UXYL48020.000' : 'UBAL48011.00'
	
	#$sv.manager.CS_TxCmpCurRouteInq($sv._user, $sv.oid(lot))
  def test00_setup
    $setup_ok = false
	@@changeudata={'memo'=>['memo1', 'memo2'], 'SUBCON_ID'=>['SCON1', 'SCON2'],'EXPEDITE_FLAG'=>['yes', 'no'], 
	  'DATE_OF_MFG'=> [getCurrDate, getCurrDate(5000)], 'DEMAND_CLASS'=>['CLS1','CLS2'], 'DIE_STK_DUE_DATE'=>[getCurrDate(20000), getCurrDate(30000)],
	  'FEATURE_CODE'=>['COD1','COD2'], 'FINANCE_UPDATE_FLAG'=>['yes','no'], 'LOT_GRADE'=>['C1', 'C2'], 'INLINE_TEST_CODE'=>['CD1','CD2'], 
	  'QUALITY_CODE'=>['1M', '4S'],'WAF_FIN_DUE_DATE'=>[getCurrDate(60000), getCurrDate(70000)], 'ATTRIB2'=>['attr21', 'attr22']}
	unless $sv.lot_info(@@lot)
      assert @@lot = $sv.new_lot_release(product: @@product, UDATA: 'PO', stb: true), "Check new lot is created" 
	  assert_equal 'PO', $sv.lot_info(@@lot).UDATA, "unexpected sublotype for the lot: #{@@lot}"
	  assert_equal 0, $sv.carrier_xfer_status_change($sv.lot_info(@@lot).carrier, "MO", @@stoker), "issue changing carrier state" #change carrier status
	end
	#//some cleanup
	cleanup
	$log.info "Lot used:: #{@@lot}; product: #{@@product}; route: #{@@route}" 
	$setup_ok = true
  end
  
  def cleanup
	$sv.lot_bankin_cancel(@@lot) unless $sv.lot_info(@@lot).bank.empty? 
	$sv.lot_nonprobankout(@@lot) if $sv.lot_info(@@lot).status=='NonProBank'
	$sv.scrap_wafers_cancel(@@lot)	if $sv.lot_info(@@lot).status=='SCRAPPED'
	$sv.lot_hold_release(@@lot, nil)	if $sv.lot_info(@@lot).status=='ONHOLD'
  end
  
  def lotnotecheck
	notes = $sv.lot_notes(@@lot)
	title= Time.now
	description = title + 5000
	$sv.lot_note_register @@lot, title.to_s, description.to_s
	updatednotes = $sv.lot_notes(@@lot)
	assert_equal ((notes.count) +1) , (updatednotes.count), 'issue with new lotnotes is creation'
	assert_equal title.to_s, updatednotes[0].title, 'new lot notes title didnt match'
	assert_equal description.to_s, updatednotes[0].desc, 'new lot notes title didnt match'
  end
  
  #return YYYY-MM-DD format data
  def getCurrDate(plusSec=0) 
	  time=Time.now + plusSec 
	  return time.year.to_s + '-' + time.mon.to_s + '-' + time.day.to_s 
  end
  
  def changeDataSetup(udataType)
	@@current = $sv.user_data('PosLot', @@lot)[udataType]
	@@changeto = (@@current == @@changeudata[udataType][0] ? @@changeudata[udataType][1] : @@changeudata[udataType][0])
  end
  
  def test_CS_TxProcessDefinitionIDInq()
	
	failed=[]
	['UTPD103.01','UTPD101.01', 'UTPD005.02'].each{ |pd|
		$log.info 'using pd: ' + pd
	  res1=$sv.manager.CS_TxProcessDefinitionIDInq($sv._user, $sv.oid(pd))
	  res2=$sm.extract_si($sm.object_info(:pd , pd, raw:true).first.classInfo)
	  failed<< pd + 'Department' unless res1.department == res2.department.identifier # 'Department check'
	  failed<< pd + 'standardProcessTime' unless res1.standardProcessTime == res2.standardProcessTime # 'standardProcessTime check'
	  failed<< pd + 'standardWaitTime' unless res1.standardWaitTime == res2.standardWaitTime # 'standardWaitTime check'
	  failed<< pd + 'standardWPH ' unless res1.standardWPH == res2.standardWPH # 'standardWPH check'
	  failed<< pd + ': opeName' unless res1.opeName == res2.processDefinitionName # 'opeName check'
	  failed<< pd + 'description' unless res1.description == res2.description # 'description check'
	  failed<< pd + 'pdType' unless res1.pdType == res2.pdType # 'pdType standardWPH check'
	  #failed<< pd + 'pdType' unless res1.pdType == res2.pdType # 'pdType standardWPH check'
	  # no check for standard cycle time, check manaully
	}
	assert failed.count==0, "Some PD info didnt match #{"\n" + failed.join("\n")}"
  end  
  
  def test_CS_TxProcessDefinitionIDInq()
	sm=SiView::SM.new($sv.env)
	resolveBoolean=[true:1, false:0]
	failed=[]
	['UT-MN-UTC001.01',	#eqp monitor
	'UT-PR-UTI002.01',	#Process Monitor
	'UTBR000.01',		#Branch
	'UTRW001.01',			#Rework
	'UTRT001.01',		#Main
	'UT-DN-UTF001.01',		#Dummy
	'UT-PR-UTR001.##'		#Active version
	].each{ |mpd|
		$log.info 'using Main pd: ' + mpd
	  activeversion = (mpd=='UT-PR-UTR001.##' ? 'UT-PR-UTR001.02' : '')
	  res1=$sv.manager.CS_TxMainProcessDefinitionInq($sv._user, $sv.oid(mpd))
	  res2=sm.extract_si(sm.object_info(:mainpd , mpd, raw:true).first.classInfo)
	  failed<< mpd + ': ownerID' unless res1.ownerID == res2.mainProcessDefinitionOwner.identifier # 'mainProcessDefinitionOwner check'
	  failed<< mpd + ': versionID' unless res1.versionID == res2.version # 'versionID check'
	  failed<< mpd + ': activeID' unless  res1.activeID == activeversion # 'activeversion check'
	  failed<< mpd + ': description ' unless res1.description == res2.description # 'description check'
	  failed<< mpd + ': flowType' unless res1.flowType == res2.processFlowType # 'processFlowType check'
	  failed<< mpd + ': mfgLayer' unless res1.mfgLayer == res2.mfgLayer.identifier # 'mfgLayer standardWPH check'
	  failed<< mpd + ': startBankID' unless res1.startBankID == res2.startBank.identifier # 'startBank check'
	  failed<< mpd + ': endBankID' unless res1.endBankID == res2.endBank.identifier # 'endBankID standardWPH check'
	  failed<< mpd + ': pdType' unless res1.pdType == res2.mainProcessDefinitionType # 'mainProcessDefinitionType standardWPH check'
	  failed<< mpd + ': monitorRouteFlag' unless res1.monitorRouteFlag == (res2.monitorRouteFlag ? 1 : 0)# 'monitor route flag check'
	  failed<< mpd + ': dynamicRouteFlag' unless res1.dynamicRouteFlag == (res2.dynamicRouteFlag ? 1 : 0)# 'dynamicRouteFlag check'
	  
	}

	assert failed.count==0, "Some PD info didnt match #{"\n" + failed.join("\n")}"
  end  
  
  def test_CS_TxQualityCodeListInq
	skip "for Fab7 only" unless $sv.f7
	dbconn = SiView::MMDB.new $sv.env
	failed=[]
	mmdb = dbconn.db.select_all('select * from SIVIEW.FSQCCODE ')  
	qcodes = $sv.manager.CS_TxQualityCodeListInq($sv._user, '')
	assert_equal mmdb.count, qcodes.strCS_QualityCodeAttributes.count, "Quality code records from mm db doesn't match with quality codes fetched from tx"
	qcodes.strCS_QualityCodeAttributes.each{|code| 
		qcode=[code.qualityCodeID, code.coverageFlag, code.qualityCodeDescription]
		failed<< qcode unless mmdb.include?(qcode) 
	}
	assert failed.count==0, "som quality code data didnt match! #{"\n" + failed.join("\n")}"
  end  

  
  def checkUdataChange
	skip "for Fab7 only" unless $sv.f7
	openo = $sv.lot_info(@@lot).opNo
	@@tx.each {|tx|
	  changeDataSetup(tx[1])
	  $log.info 'Processing TX: ' + tx[0]
	  if ['CS_TxLotSubconIDChangeReq','CS_TxLotAttributeChangeReq'].include?(tx[0]) 
		$sv.manager.send(tx[0], $sv._user, $sv.oid(@@lot), @@changeto,@@memo)
	  else
		$sv.manager.send(tx[0], $sv._user, $sv.oid(@@lot), $sv.oid(@@route),openo, @@changeto,@@memo)
	  end
	  
	  assert_equal @@changeto, $sv.user_data('PosLot', @@lot)[tx[1]], "issue with changing udate using #{tx[0]} for the lot: #{@@lot}. expected #{@@changeto}, actual #{$sv.user_data('PosLot', @@lot)[tx[1]]}"
	}
  end
  
  def test01_changeUDATA_prodlot
	#at first operation
	checkUdataChange
	lotnotecheck
  end
  
  
  
  #first, last, 
  def test02_checkUDATAChangeAtVariousOperations
	@@routeOperations.each{ |modulePD|
		expectedMPD= modulePD[0]
		
		#check able to change UDATA at various stages of the route
		modulePD[1].each{|opeNo|
			assert $sv.lot_opelocate(@@lot, opeNo), "failed to locate lot: #{@@lot} to ope num: #{opeNo}"
			checkUdataChange
			lotnotecheck
		}
	}
	$sv.lot_bankin_cancel(@@lot) unless $sv.lot_info(@@lot).bank.empty? 
  end  
  
  #first, last, 
  def test03_UDATAChangewhileLotIsInBranchRoute
  #@@branch=[["branchRoute", "branc at pd", "joining opeNo", [["first op",respective mod pd], ["branch op",modpd],..]]]
	@@branch.each{ |branchDtls|
		branchRt= branchDtls[0]
		branchAtOpeNo=branchDtls[1]
		joiningOpeNo=branchDtls[2][0]
		joiningOpeMPD=branchDtls[2][1]
		assert $sv.lot_opelocate(@@lot, branchAtOpeNo), "failed to locate lot: #{@@lot} to ope num: #{branchAtOpeNo}"
		assert $sv.lot_branch(@@lot,branchRt, memo:"UDATA change test", retop:joiningOpeNo ), "failed branch: #{@@lot} to branch: #{branchRt} at ope No: #{branchAtOpeNo}"
		$log.info 'LotOnBranchRoute->CS_TxCmpCurRouteInq->DifferentFlag: ' + $sv.manager.CS_TxCmpCurRouteInq($sv._user, $sv.oid(@@lot)).isDifferentFlag
		branchDtls[3].each{
		|opeNoAndExpectedMPD|
			#check same MPD is retrieved for all the operations in branch
			assert $sv.lot_opelocate(@@lot, opeNoAndExpectedMPD[0]), "failed to locate lot: #{@@lot} to ope num: #{opeNoAndExpectedMPD[0]}"
			checkUdataChange
		}
		#expecting lot is at last operation on branch route.
		$sv.claim_process_lot(@@lot) #finds a eqp and does operation complete on it
		checkUdataChange
		lotnotecheck
	}
	
  end

#first, last, 
  def test04_checkUDATAwhileLotIsInReworkRoute
  #@@branch=[["branchRoute", "branc at pd", "joining opeNo", [["first op",respective mod pd], ["branch op",modpd],..]]]
	@@rework.each{ |reworkDtls|
		reworkRt= reworkDtls[0]
		reworkAtOpeNo=reworkDtls[1]
		joiningOpeNo=reworkDtls[2][0]
		joiningOpeMPD=reworkDtls[2][1]
		assert $sv.lot_opelocate(@@lot, reworkAtOpeNo), "failed to locate lot: #{@@lot} to ope num: #{reworkAtOpeNo}"
		assert $sv.lot_gatepass(@@lot), "failed to gatepass lot: #{@@lot} to ope num: #{reworkAtOpeNo}" #rework starts after completion of the process, so doing a gatepass
		assert $sv.lot_rework(@@lot,reworkRt, return_opNo:joiningOpeNo), "failed rework: #{@@lot} to branch: #{reworkRt} at ope No: #{reworkAtOpeNo}"
		$log.info 'LotOnReworkRoute->CS_TxCmpCurRouteInq->DifferentFlag: ' + $sv.manager.CS_TxCmpCurRouteInq($sv._user, $sv.oid(@@lot)).isDifferentFlag
		reworkDtls[3].each{
		|opeNoAndExpectedMPD|
			#check same MPD is retrieved for all the operations in branch
			assert $sv.lot_opelocate(@@lot, opeNoAndExpectedMPD[0]), "failed to locate lot: #{@@lot} to ope num: #{opeNoAndExpectedMPD[0]}"
			checkUdataChange	
			lotnotecheck
		}
		#expecting lot is at last operation on branch route.
		
		$sv.claim_process_lot(@@lot) #finds a eqp and does operation complete on it
		checkUdataChange
		lotnotecheck
	}
  end

  
	def test05_checkUDATAChangeWhenLotIsOnHold
		cleanup
		assert $sv.lot_hold(@@lot, @@holdreason), "failed to put lot on hold: #{@@lot} with reason code: #{@@holdreason}"
		checkUdataChange
		lotnotecheck
		assert $sv.lot_hold_release(@@lot, @@holdreason), "failed to release lot hold after subloty type change: #{@@lot} with reason code: #{@@holdreason}"
	end 
	
   def test06_checkUDATAChangeWhenLotIsinBank
		assert $sv.lot_opelocate(@@lot, @@lastOpeNo), "failed to locate lot: #{@@lot} to ope num: #{@@lastOpeNo}"
		assert !$sv.lot_info(@@lot).bank.empty?, "bankin failed for the lot: #{@@lot} to ope num: #{@@lastOpeNo}"
		checkUdataChange
		lotnotecheck
   end 
	
   def test07_checkUDATAChangeWhenLotIsinNonProBank
		cleanup
		assert $sv.lot_nonprobankin(@@lot, @@nonprobank), "failed to do non probank in: #{@@lot}, non pro bank; #{@@nonprobank}"
		checkUdataChange
		lotnotecheck
		assert $sv.lot_nonprobankout(@@lot), "failed to do non probank out: #{@@lot}, non pro bank; #{@@nonprobank}"
   end 
		
   def test08_checkUDATAChangeforScrapedlot
		cleanup
		$sv.scrap_wafers(@@lot)
		checkUdataChange
		lotnotecheck
		$sv.scrap_wafers_cancel(@@lot)
	end 
	
end
