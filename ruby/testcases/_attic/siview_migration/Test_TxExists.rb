=begin

(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.


Author: Pradeep, 2014-11-23

History:
  
=end

require 'RubyTestCase'

class Test_TxExists < RubyTestCase
  Description = "Check if tx exists"
@@tx=[:AMD_TxModulePDInq,:CS_TxHoldClaimMemoHistoryListInq,:CS_TxHoldClaimMemoUpdateReq,:CS_TxLotFamilyInq,:CS_TxLotOperationInQTimeZoneInq,:CS_TxProductIDDetailListInq,:TxBankInByPostProcReq,:TxBranchReq,:TxBranchWithHoldReleaseReq,:TxCarrierOutFromInternalBufferReq,:TxEnhancedFutureHoldReq,:TxFutureHoldListByKeyInq,:TxFutureHoldPostByPostProcReq,:TxFutureHoldPreByPostProcReq,:TxHoldLotReleaseReq,:TxHoldLotReq,:TxLotCassettePostProcessForceDeleteReq,:TxLotHoldListInq,:TxLotInfoByWaferInq,:TxLotInfoInq__140,:TxMergeWaferLotNotOnRouteReq,:TxMergeWaferLotReq,:TxPartialReworkReq,:TxPartialReworkWithHoldReleaseReq,:TxPartialReworkWithoutHoldReleaseReq,:TxPostProcessActionListInq__130,:TxPostProcessActionRegistReq__100,:TxPostProcessActionUpdateReq__130,:TxPostProcessConfigInfoInq__130,:TxPostProcessExecReq__100,:TxPostProcessExecWithSeqNoReq,:TxPostProcessParallelExecReq,:TxProductRequestInq,:TxReworkPartialWaferLotCancelReq,:TxReworkReq,:TxReworkWholeLotCancelReq,:TxReworkWholeLotReq,:TxReworkWithHoldReleaseReq,:TxScrapWaferCancelReq,:TxScrapWaferNotOnRouteCancelReq,:TxScrapWaferNotOnRouteReq,:TxScrapWaferReq,:TxSplitWaferLotNotOnRouteReq,:TxSplitWaferLotReq,:TxSplitWaferLotWithHoldReleaseReq,:TxSplitWaferLotWithoutHoldReleaseReq,:TxSubRouteBranchCancelReq,:TxSubRouteBranchReq,:TxWhatNextLotListInq__140,:CS_TxGenerateNewLotIds__R15,:CS_TxGenerateNewLotIds,:TxGatePassReq,:TxLotFamilyInq,:TxLotCommentInfoInq,:TxLotNoteInfoInq,:TxLotOperationListInq,:TxLotOperationListFromHistoryInq,:TxMultipleLotsOperationListInq,:TxConnectedRouteListInq,:TxLotOperationNoteInfoInq,:TxLotOperationNoteInfoRegistReq ,:TxLotOperationNoteListInq,:TxLotNoteInfoRegistReq,:CS_TxLotAttributeChangeReq,:CS_TxLotQualityCodeChangeReq,:CS_TxLotWafFinDueDateChangeReq,:CS_TxLotPriorityClassChangeReq,:TxLotSchdlChangeReq,:TxLotSchdlChangeReq__110,:CS_TxCmpCurRouteInq,:CS_TxLotSubconIDChangeReq,:CS_TxMainProcessDefinitionInq,:CS_TxProcessDefinitionIDInq,:CS_TxLotDateOfMfgChangeReq ,:CS_TxLotDemandClassChangeReq,:CS_TxLotDieStkDueDateChangeReq ,:CS_TxLotExpediteFlagChangeReq,:CS_TxLotFeatureCodeChangeReq,:CS_TxLotFinanceUpdateFlagChangeReq,:CS_TxLotGradeChangeReq,:CS_TxLotInlineTestCodeChangeReq ,:CS_TxPriorityClassListInq,:CS_TxQualityCodeListInq,:CS_TxRemeasureLocateReq,:TxForceOpeLocateReq,:TxOpeLocateReq,:TxGatePassReq,:TxFutureHoldCancelReq,:TxLotFutureActionDetailInfoInq,:TxLotFutureActionListInq,:TxRouteOperationListForLotInq,:TxOpeGuideInfoInq,:CS_TxSubLotTypeChangeReq,:CS_TxCumulativeReworkCountbyWaferInq,:CS_TxEnhancedLotOperationListInq,:TxBankInCancelReq,:TxBankInReq]
@@drop4=[:TxScrapHistoryInq, :TxLotListInq, :TxLotListInq__100, :TxCollectedDataActionInq, :TxCollectedDataHistoryInq__120, :TxCassetteStatusInq, :TxCassetteStatusInq__100, :TxLotInfoInq, :TxLotInfoInq__100, :CS_TxAdjustDieCountWaferReq, :AMD_TxAllEquipmentStatesInq, :TxFixtureIDListInq, :TxFixtureGroupIDListInq, :TxEqpRelatedRecipeIDListInq, :TxEntityListInq, :TxEntityInhibitCancelReq, :AMD_TxChamberPMEventReq, :AMD_TxEqpPMEventReq, :AMD_TxPMEventCancelReq, :TxHistoryInformationInq, :CS_TxEqpPortStatusChangeReq, :TxLotReQueueReq, :TxModuleProcessDefinitionIDListInq, :CS_TxWorkOrderCreateReq, :TxOperationHistoryInq__101, :TxOwnerChangeReq, :TxProcessDefinitionIDListInq, :TxReticleGroupIDListInq, :TxReticleIDListInq, :TxStageIDListInq, :CS_TxWorkOrderListInq, :CS_TxEntityInhibitConfigurationInq, :TxUserDefinedDataAttributeInfoInq, :TxUserDescInq, :TxUserParameterValueInq, :TxEnvironmentVariableInfoInq, :TxEnvironmentVariableUpdateReq, :AMD_TxGetUDATAInq, :CS_TxUserParameterValueForMultipleObjectInq, :CS_TxUserParameterValueForMultipleObjectReq, :TxObjectIDListInq, :TxUserDataInq__101, :TxEntityInhibitListInq, :TxUserDataUpdateReq, :TxUserParameterValueChangeReq, :CS_TxEntityInhibitEndDateUpdateReq, :CS_TxEntityInhibitUpdateReq, :TxEntityInhibitCancelReq__101, :TxEntityInhibitReq, :TxEntityInhibitReq__101, :AMD_TxGetChambersAvailabilityInq, :TxAllAvailableEqpInq, :TxCandidateChamberStatusInq, :TxCandidateEqpModeInq, :TxCandidateEqpStatusInq, :TxEqpAlarmHistoryInq, :TxEqpAlarmRpt, :TxEqpDetailInfoInq, :TxEqpNoteInq, :TxEqpNoteRegistReq, :TxEqpUsageCountResetReq, :TxMachineRecipeListInq, :TxMoveCarrierToInternalBufferRpt, :TxRecipeConfirmationReq, :TxRecipeDeletionReq, :TxRecipeDirectoryInq, :TxRecipeDownloadReq, :TxRecipeFileDeletionReq, :TxRecipeUploadReq, :TxSignalTowerOffReq, :TxWorkAreaListInq, :CS_TxPMEventCheckReq, :CS_TxPMJobCreateReq, :CS_TxPMReqListInq, :CS_TxPMTriggerDataRpt, :GF_TxAllEquipmentStatesListInq, :GF_TxPMEventRpt, :TxChamberStatusChangeReq, :TxChamberStatusChangeRpt, :TxEqpInfoForInternalBufferInq, :TxEqpInfoForInternalBufferInq__120, :TxEqpInfoInq, :TxEqpInfoInq__120, :TxEqpModeChangeReq, :TxEqpStatusAdjustReq, :TxEqpStatusChangeReq, :TxEqpStatusChangeRpt, :TxEqpStatusRecoverReq, :TxLoadingLotForInternalBufferRpt, :TxLoadingLotRpt, :TxLotCassetteXferStatusChangeRpt, :TxLotVerifyForLoadingForInternalBufferReq, :TxMoveCarrierFromInternalBufferRpt, :TxStockerStatusChangeRpt, :CS_TxOperationHistoryInq, :AMD_TxGetRecipeNameInq, :CS_TxRecipeRscListInq, :CS_TxRMSRecipeDownloadReq, :CS_TxRMSRecipeUploadReq, :sivAuthenticateUser, :sivGetToolList, :sivGetToolTypeList, :sivGetUserList, :TxArrivalCarrierCancelReq, :TxArrivalCarrierCancelReq__090, :TxLotsInfoForOpeStartInq, :CS_TxFunctionalCheckReq, :CS_TxReactivateAccountUserReq, :CS_TxReactivateRevokedUserReq, :CS_TxUserInfoInq, :CS_TxUserPrivilegesInq, :TxLogOnCheckReq]
  
  def test00_tx_exisits
	@@tx = @@tx + @@drop4
	matchedlist=[]
	missingtx=[]
	methods=$sv.manager.methods
	@@tx.each{|txSymbol|
		methods.include?(txSymbol) ? matchedlist<<txSymbol : missingtx<<txSymbol
	}
	$log.info "\n\n"
	$log.info "Methods that doesnt exist in @manager  #{"\n\t\t" + missingtx.join("\n\t\t")}"
	#now check if missing methods exist in manager
	methods=$sv.manager.methods
	missingTx_in_manager=[]
	missingtx.each{|txSymbol|
		methods.include?(txSymbol) ? matchedlist<<txSymbol : missingTx_in_manager<<txSymbol
	}
	assert missingTx_in_manager.count==0, "following tx are missing in the drop: #{"\n#################################################\n\t\t" + missingTx_in_manager.join("\n\t\t") +"\n#################################################\n\t\t" }"
  end
end