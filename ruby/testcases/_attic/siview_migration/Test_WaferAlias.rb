=begin

(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.


Author: Pradeep Buddaraju

History:
  
=end

require 'SiViewTestCase'

# Wafer Alias format based controlled by env var CS_SP_WAFER_ALIAS_NAME_FORMAT
#
# see http://myteamsgf1/sites/gfmt/domains/FC/MESCON/Shared%20Documents/Development/Design/CodeDrop2-WaferAlias.mht
#

class Test_WaferAlias < SiViewTestCase
  @@carrier = 'UR%'
  @@product = 'UT-PRODUCT001.01'
  @@route = "UTRT001.01"
  @@bank = "UT-RAW"
  @@vendor_product = "UT-RAW.00"
  @@carriers=[]
  @@alisformat="<LOT_FAMILY_ID_PFX>.<##>" # change env to <##>, R15.<##>, <LOT_FAMILY_ID_PFX>.<##>
  @@lot=nil
  
  def self.shutdown
	$sv.delete_lot_family @@lot
  end

  def test00_setup
   $svtest.carriers=@@carrier
   @@carriers = $sv.carrier_list(status: 'AVAILABLE', empty: true, carrier: @@carrier)
   assert @@carriers.count>2, 'Not enough empty carriers' 
   assert @@alisformat==$sv.environment_variable[1]['CS_SP_WAFER_ALIAS_NAME_FORMAT'], "alias configured is not as expected(#{@@alisformat})"
   
  end
  
  
  def test01_prodlot
    assert @@lot=$sv.new_lot_release(product:@@product, stb:true, carrier:@@carriers.pop), 'lot not generated'
	assert verify_alias(@@lot), 'Alias formats are not as expected'
  end
  
  def test02_controllot
    result=[]
	['Equipment Monitor', 'Process Monitor', 'Dummy'].each{ |lottype| 
	  $sv.delete_lot_family @@lot
	  @@lot=$svtest.new_monitor_lot(lottype, carrier:@@carrier)
	  $log.info 'Alias for ' + lottype + 'lot ' + @@lot
      result<< verify_alias(@@lot)
	}
	test_ok=true
	$log.info 'lottype-->pass/fail' 
	['Equipment Monitor', 'Process Monitor', 'Dummy'].each_with_index{|lottype, i| 
      $log.info lottype + "-->" + result[i].to_s
	  test_ok &=result[i]
	  }
	assert test_ok, 'Alias formats are not as expected'
  end
  
  def verify_alias(lot)
   wafers= $sv.lot_info(lot, wafers:true).wafers    
	hasdelimiter=false
	assert wafers.count==25, 'expected number of wafers not generated'
    (@@alisformat.include? '.')? (pre,post=@@alisformat.split('.');hasdelimiter=true) : (pre,post='',@@alisformat)
    pre_calculate=true if (pre.include? "<##>" || @@alisformat=='') #handles both cases were pre has calculate value of if env var is set empty, it should use same logic as <##>
    post_calculate=true if post.include? "<##>"
    lotfamily=lot.split('.')[0]    
    lotprefix_post=true if post.include? "LOT_FAMILY_ID_PFX"
	
    test_ok = true
    $log.info 'expected alias ' + @@alisformat
	$log.info 'Generated Alias'
    wafers.each_with_index{|wafer, i|
	  
      pre_calculate ? prefix='%.2d' % (i+1)  : prefix=pre
      post_calculate ? postfix='%.2d' % (i+1)  : postfix=post
      prefix=lotfamily	if (pre.include? 'LOT_FAMILY_ID_PFX') 
      postfix=lotfamily	if (post.include? 'LOT_FAMILY_ID_PFX') 
	  expectedAlias= hasdelimiter ? prefix+ '.' +postfix : prefix+postfix
      test_ok &= wafer.alias== expectedAlias ? true: false 
	  $log.info 'expected/actual: ' + expectedAlias + '/' + wafer.alias
    }
	test_ok
  end
  #TODO: Test wafer Lot ID
end
