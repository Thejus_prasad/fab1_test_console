=begin 
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.
    
Author:  pradeep buddaraju

wild characters in entities for following inhibit combinations
[Entity: Product Group]
[Entity: Technology]
[Entity: Logical Recipe]
[Entity: Customer]
[Entity: Fixture Group]  
[Entity: Equipment and Chamber : Check lot chamber availability] 
=end

require 'SiViewTestCase'

# Test MSRs 613451, 613482, 613483 (ProductGroup), 832862 (technology, lrecipe, stage)
#   884685 (non-expiring inhibits), MSR832876 (inhibit owner), 99391
class Test_R15Inhibits < SiViewTestCase
  @@eqp = 'UTR003'               # 'UTI003RTCL'
  @@opNo = '1000.2000'
  @@route = 'UTRT-INHIBIT.01'    # 'UTRT-IB-RTCL.01'
  @@product = 'UT-INHIBIT.01'    # 'UT-IB-RTCL.01'
  @@reticle = 'UTRETICLEFBRTCL'  # 'UTRGIBRTCL002'
  @@fixture = 'UTFIXTURE0001'
  @@fixture_eqp = 'UTF003'
  @@fixture_opNo = '1100.1000'
  @@other_lrecipe = 'UTLR005.01'
  @@other_stage = 'UTNOTUSED'
  @@other_owner = 'dsteger'
  
  # take one inhibit class für enddate checks
  @@inhibit_enddate = :eqp_chamber_productgroup_recipe
  @@expire_timestamp = '2034-12-31-00.00.00.000000'
  @@nonexpire_timestamp = '1901-01-01-00.00.00.000000'
  # reverse order of inhibit entities
  @@reversed = false
  @@test_fixtures = true
  @@wc=true #wild card yes or no
  @@wc_combinations={
	:productgroup=>['Product Group'],
    :productgroup_eqp=>['Product Group', 'Equipment'],
    :productgroup_eqp_recipe=>['Product Group', 'Equipment', 'Machine Recipe'],
    :productgroup_pd=>['Product Group', 'Process Definition'],
    :productgroup_pd_eqp=>['Product Group', 'Process Definition', 'Equipment'],
    :productgroup_pd_recipe=>['Product Group', 'Process Definition', 'Machine Recipe'],
    :technology_eqp=>['Technology', 'Equipment'],
    :technology_eqp_chamber=>['Technology', 'Chamber'],
    :technology_pd=>['Technology', 'Process Definition'],
    :technology_pd_eqp=>['Technology', 'Process Definition', 'Equipment'],
    :technology_pd_eqp_chamber=>['Technology', 'Process Definition', 'Chamber'],
	:lrecipe=>['Logical Recipe'], # C6.20
    :lrecipe_eqp=>['Logical Recipe', 'Equipment'],
    :lrecipe_eqp_chamber=>['Logical Recipe', 'Chamber'],
	:fixture=>['Fixture'],
    :fixture_eqp=>['Fixture', 'Equipment'],
	:customer=>['Customer'],
	:customer_technology_eqp=>['Customer', 'Technology', 'Equipment'],
	:customer_technology_eqp_chamber=>['Customer', 'Technology', 'Chamber'],
	:fg=>['Fixture Group'],
	:eqp_chamber=>['Chamber'],
    :eqp_chamber_productgroup=>['Chamber', 'Product Group'],
    :eqp_chamber_pd=>['Chamber', 'Process Definition'],
    :eqp_chamber_productgroup_pd=>['Chamber', 'Product Group', 'Process Definition'],
    :eqp_chamber_productgroup_recipe=>['Chamber', 'Product Group', 'Machine Recipe'],
    :eqp_chamber_product=>['Chamber', 'Product Specification'],
    :eqp_chamber_recipe=>['Chamber', 'Machine Recipe']
	}

  def self.startup
    super
    if @@reversed
      # change order of inhibit entities and store old settings
      SiView::MM::InhibitClasses.each_pair {|k, v| SiView::MM::InhibitClasses[k] = v.reverse}
      $log.info "inhibit entities are reversed!"      
    end
  end
  

  def self.after_all_passed
    $sv.delete_lot_family(@@lot)
  end

  def self.shutdown
    SiView::MM::InhibitClasses.each_pair {|k, v| SiView::MM::InhibitClasses[k] = v.reverse} if @@reversed
  end
  

  def test00_setup
    $setup_ok = false
    #
    assert_equal '2034-01-01-00.00.00.000000', $sv.environment_variable[1]['CS_SP_INHIBIT_DEFAULT_ENDTIME'], "incorrect MM environment"
    # clean up eqps
    [@@eqp, @@fixture_eqp].each {|eqp|
      assert_equal 0, $sv.eqp_cleanup(eqp)
      assert_equal 0, $sv.eqp_mode_offline1(eqp, notify: false)
    }
    # create test lot
    assert @@lot = $svtest.new_lot(product: @@product, route: @@route), "error creating test lot"
    assert_equal 0, $sv.lot_opelocate(@@lot, @@opNo)
    # get inbit entity values
    eqpi = $sv.eqp_info(@@eqp)
    @@chamber = eqpi.chambers[0].chamber
    li = $sv.lot_info(@@lot)
    @@productgroup = li.productgroup
	@@technology = li.technology
    @@customer = li.customer
    @@stage = li.stage
    @@product = li.product
    @@route = li.route
    @@module = $sv.AMD_modulepd(@@lot)
    @@op = li.op
    assert wn = $sv.what_next(@@eqp).find {|w| w.lot == @@lot}, "lot #{@@lot} is no candicate for #{@@eqp}"
    @@lrecipe = wn.lrecipe
    @@mrecipe = wn.mrecipe
    r = eqpi.reticles.find {|r| r.reticle == @@reticle}
    assert r, "reticle #{@@reticle} not loaded at #{@@eqp}"
    reticle_group = r.rg
    f = $sv.fixture_list(eqp: @@fixture_eqp, fixtureinfo: true).find {|f| f.fid == @@fixture}
    assert f, "fixture #{@@fixture} not loaded at #{@@fixture_eqp}"
    fixture_group = f.group
    #
    @@inhibits = {}
    @@wc_combinations.keys.each {|iclass|
      attrib = []
      oids = @@wc_combinations[iclass].collect {|e|
        case e
        when 'Chamber'; attrib << @@chamber; @@eqp
        when 'Operation'; attrib << @@opNo; @@route
        when 'Equipment'; attrib << ''; iclass.to_s.start_with?('f') ? @@fixture_eqp : @@eqp
        when 'Fixture'; attrib << ''; @@fixture
        when 'Fixture Group'; attrib << ''; fixture_group
        when 'Logical Recipe'; attrib << ''; @@lrecipe
        when 'Machine Recipe'; attrib << ''; @@mrecipe
        when 'Module Process Definition'; attrib << ''; @@module
        when 'Route'; attrib << '*'; @@route
        when 'Process Definition'; attrib << ''; @@op
        when 'Product Specification'; attrib << ''; @@product
        when 'Product Group'; attrib << ''; @@productgroup
        when 'Reticle'; attrib << ''; @@reticle
        when 'Reticle Group'; attrib << ''; reticle_group
        when 'Stage'; attrib << ''; @@stage
        when 'Technology'; attrib << ''; @@technology 
        when 'Customer'; attrib << ''; @@customer
        else
          flunk "unknown class #{e}"
        end
      }
      @@inhibits[iclass] = [oids, attrib]
    }
    $log.info "inhibit class values:\n" + @@inhibits.pretty_inspect
    #
    # other entity values
    @@other_chamber = eqpi.chambers[1].chamber
    assert_not_equal @@chamber, @@other_chamber, "#{@@eqp} must have at least to chambers"
    l_op = $sv.lot_operation_list(@@lot).find {|i| i.opNo != @@opNo}
    @@other_opNo = l_op.opNo
    @@other_op = l_op.op    
    @@other_productgroup = $sv.productgroup_list.find {|pg| pg != @@productgroup}
    @@other_tech = $sv.technology_list.find {|t| t != @@technology}
    @@other_customer = $sv.object_id_list('PosCustomer', '%').find {|c| c != @@customer}
    @@other_product = $sv.product_list.find {|p| p.product != @@product}.product
    @@other_route = $sv.module_list(level: "Main").find {|m| m != @@route}
    @@other_module = $sv.module_list(level: "Module").find {|m| m != @@module}    
    @@other_mrecipe = $sv.machine_recipe_list(mrecipe: "%.UT%").find {|m| m != @@mrecipe}    
    o_reticle = $sv.reticle_list(reticleinfo: true).find {|r| r.reticle != @@reticle}
    @@other_reticle = o_reticle.reticle
    @@other_rg = o_reticle.rg    
    o_fixture = $sv.fixture_list(fixtureinfo: true).find {|f| f.fid != @@fixture}
    @@other_fixture = o_fixture.fid
    @@other_fg = o_fixture.group
    assert_not_equal @@lrecipe, @@other_lrecipe
    #
    $setup_ok = true
  end
  
  def _define_other_entity(iclass)    
    oids, attrib = @@inhibits[iclass]
    _oids = oids.clone
    _attrib = attrib.clone
    # override most important entity
    _iclass = SiView::MM::InhibitClasses[iclass]
    i = 0 # no proper cleanup possible if rand is used: rand(_iclass.count)
    case _iclass[i]
      when 'Chamber'; _attrib[i] = @@other_chamber
      when 'Equipment'; _oids[i] = iclass.to_s.start_with?('f') ? @@eqp : @@fixture_eqp
      when 'Logical Recipe';_oids[i] = @@other_lrecipe
      when 'Machine Recipe'; _oids[i] = @@other_mrecipe
      when 'Process Definition'; _oids[i] = @@other_op
      when 'Module Process Definition'; _oids[i] = @@other_module
      when 'Route'; _oids[i] = @@other_route
      when 'Operation'; _attrib[i] = @@other_opNo
      when 'Product Specification'; _oids[i] = @@other_product      
      when 'Product Group'; _oids[i] = @@other_productgroup
      when 'Reticle'; _oids[i] = @@other_reticle
      when 'Reticle Group'; _oids[i] = @@other_rg
      when 'Fixture'; _oids[i] = @@other_fixture
      when 'Fixture Group'; _oids[i] = @@other_fg
      when 'Stage'; _oids[i] = @@other_stage
      when 'Technology'; _oids[i] = @@other_tech
      when 'Customer'; _oids[i] = @@other_customer
      else
        flunk "unknown class #{_iclass[i]}"
    end    
    return _oids, _attrib
  end

 
    # generate test2xx test for fixture and fixture group
  @@wc_combinations.keys.each_with_index {|iclass, i|
    send(:define_method, "test#{i + 301}_#{iclass}") {
     whatnext = !@@wc_combinations[iclass].member?('Chamber')
	 if @@wc_combinations[iclass].member?('Fixture') || @@wc_combinations[iclass].member?('Fixture Group')
	  assert verify_inhibit_class(iclass, 1407, eqp: @@fixture_eqp, opNo: @@fixture_opNo, whatnext: false)# inhibit for the entity      
	 else
      assert verify_inhibit_class(iclass, 979, whatnext: whatnext, other: false)  # inhibit for the entity   
	 end
    }
  }
  
  
  def verify_inhibit_attrs(iclass, oids, params)
    inhs = $sv.inhibit_list(iclass, oids)
    assert_equal 1, inhs.count, "expected one inhibit, but got #{inhs.count}"
    inh = inhs.first
    res  = verify_equal(params[:inhibit_owner], inh.inhibit_owner, "wrong owner")
    res &= verify_equal(params[:release_time], inh.release_time, "wrong expected release time")
    res &= verify_equal(params[:memo], inh.memo, "wrong memo")
    return res
  end
  
  # verify an inhibit class
  def verify_inhibit_class(iclass, exp_rc, params)
    eqp = params[:eqp] || @@eqp
    opNo = params[:opNo] || @@opNo
    is_avail = params[:other] || params[:expired] || (params[:delayed] && !params[:extended])
    $log.info "verify_inhibit_class #{iclass.inspect}, #{exp_rc} is_available: #{is_avail}"    
    ret = true
    #
    # cancel _all_ inhit combinations for the test entities
    res = @@wc_combinations.keys.inject(0) {|ok, iclass|
      oids, attrib = @@inhibits[iclass]
      ok += $sv.inhibit_cancel(iclass, oids, attrib: attrib, silent: true)
    }
    ($log.warn "  error cleaning up stale inhibits"; return) if res != 0
    # clean up lot and eqp, load carrier
    res = $sv.lot_cleanup(@@lot, opNo: opNo)
    ($log.warn "  error cleaning up lot #{@@lot}"; return) if res != 0
    li = $sv.lot_info(@@lot)
    ($log.warn "  lot #{@@lot} is not at an op for #{eqp}"; return) unless li.opEqps.member?(eqp)
    res = $sv.eqp_cleanup(eqp)
    ($log.warn "  error cleaning up eqp #{eqp}"; return) if res != 0
    res = $sv.eqp_mode_offline1(eqp, notify: false)
    ($log.warn "  error switching eqp #{eqp} mode"; return) if res != 0
    $sv.carrier_xfer_status_change(li.carrier, 'EO', eqp)
    res = $sv.eqp_load(eqp, nil, li.carrier)
    ($log.warn "  error loading carrier #{li.carrier} on #{eqp}"; return) if res != 0
    #
    # register inhibit
    oids, attrib = @@inhibits[iclass]
	
	
	
	if @@wc && @@wc_combinations.has_key?(iclass)
	  oids_wc = oids.clone
	  oids_orig = oids.clone
	  oids_wc[0]=oids_wc[0][0..-2]+'*' #adding wild char to oid
	  oids = oids_wc
	end
    iparams = {attrib: attrib}
    if params[:delayed]
      iparams.merge!(tstart: $sv.siview_timestamp(Time.now + 60))
    elsif (params[:expired] || params[:extended])
      iparams.merge!(tend: $sv.siview_timestamp(Time.now + 60))
    end
    res = $sv.inhibit_entity(iclass, oids, iparams) 
    ($log.warn "  error registering inhibit"; return false) if res != 0
    #
    # verify the inhibit is effective (check inhibit list for entity - MSR99391)
    inhs = $sv.inhibit_list(iclass, oids, attrib: attrib)
    assert_equal 1, inhs.size, "  wrong number of inhibits: #{inhs.inspect}"
    if params[:extended]
      res = $sv.inhibit_end_date_update(inhs.first.iid_oid, @@expire_timestamp)
      ($log.warn "  error updating inhibit end date"; return false) if res != 0
    end
    ($log.info "sleeping 60s..."; sleep 60) if params[:expired] || params[:extended]
    
    # opestart
    cj = $sv.eqp_opestart(eqp, li.carrier)
    ret &= check_availability(iclass, cj, is_avail, $sv.rc, exp_rc)
    $sv.eqp_opestart_cancel(eqp, cj) if cj
    assert ret
    
    # unload carrier
    res = $sv.eqp_unload(eqp, nil, li.carrier)
    ($log.warn "  error unloading carrier from #{eqp}"; return) if res != 0
    
    # what next
    if params[:whatnext]
      res = $sv.what_next(eqp).find {|e| e.lot == @@lot}
      ($log.warn "  setup error, lot #{@@lot} not in WhatNext list of #{eqp}"; return) unless res    
      ret &= verify_equal((is_avail ? 0 : 1), res.inhibits.size, "  wrong inhibits in WhatNext: #{res.inhibits.inspect}")
      assert ret
    end
    
    # slr
    cj = $sv.slr(eqp, lot: @@lot)
    ret &= check_availability(iclass, cj, is_avail, $sv.rc, exp_rc)
    $sv.slr_cancel(eqp, cj) if cj
    
    # cancel inhibit
    res = $sv.inhibit_cancel(iclass, oids, attrib: attrib)
    ($log.warn "  error canceling inhibit"; return false) if res != 0
    res = $sv.inhibit_list(iclass, oids, attrib: attrib)
    ($log.warn "  error canceling inhibit: #{res.inspect}"; return false) if res.size != 0
    #
    return ret
  end

  def check_availability(iclass, cj, available, rc, exp_rc)
    if SiView::MM::InhibitClasses[iclass].member?('Chamber')
      # check chamber availability
      ($log.warn "  cj expected in case of chamber inhibit"; return false) unless cj
      cha = $sv.AMD_chambers_availability(cj) || ($log.warn "  chamber availability check failed"; return)
      cha_lot = cha.find {|lot| lot.lot == @@lot} || return
      cha_avail = cha_lot.availability.find {|a| a.chamber == @@chamber} || return
      verify_equal(available ? '1' : '0', cha_avail.available, "  chamber #{@@chamber} should available: #{available}")
    elsif available
      # check for CJ
      verify_condition("  cj expected despite of the inhibit, last rc: #{rc}") {cj}
    else
      verify_equal(nil, cj, "  no cj expected") && verify_equal(exp_rc, rc, "  wrong rc")
    end
  end

end 
