=begin

(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.


Author: Pradeep Buddaraju

History:
  
=end

require 'SiViewTestCase'

# Wafer Level tracking 
#
# see http://myteamsgf1/sites/gfmt/domains/FC/MESCON/Shared%20Documents/Development/Design/CodeDrop4-WaferLevelTracking.mht
#
####Checks to ignore#####
#1  Privilege Check (TxChamberProcessWaferRpt) --test01
#2  CJ exist Check  --test06
#3  All Action Code is same* --test05
#4  Lot Status is InProcessing
#5  CJ is same as reported one --test02
#6  Lot-Wafer relation --test03
#7  Eqp-Chamber relation --test04
###########################

class Test_WLT< SiViewTestCase
  @@carrier = 'UR%'
  @@product = 'UT-PRODUCT001.01'
  @@route = "UTRT001.01"
  @@bank = "UT-RAW"
  @@vendor_product = "UT-RAW.00"
  @@MESUser='X-MESTEST' #who doesnt have access to TxChamberProcessWaferRpt 
  @@MESUserPwd='UTFUTFN.Y'
  @@MESUser1='X-MESTEST1' #who have access to TxChamberProcessWaferRpt, but not MES user
  @@MESUserPwd1='2UTFUTFN.Y'
  @@eqp_fb_opno='1000.100'
  @@eqp_fb='UTC001'
  @@eqp_port_fb='P1'
  @@eqp_chamber_fb='PM1'
  @@eqp_ib_opno='1000.300'
  @@eqp_ib='UTI001'
  @@eqp_port_ib='P1'
  @@eqp_chamber_ib='MODULE1'
  @@carriers=[]
  @@lot1=nil
  @@lot2=nil
  @@carrier1, @@carrier2 = nil,nil
  @@cj1, @@cj2 = nil,nil
  $_MESUser=nil
  $_MESUser1=nil
  @@actions=['ProcessStart', 'ProcessEnd']
  
  def self._after_all_passed
   $sv.delete_lot_family @@lot1
   $sv.delete_lot_family @@lot2
  end

  def test00_setup
   $svtest.carriers=@@carrier
   $_MESUser=$sv.user_struct(@@MESUser, @@MESUserPwd)
   $_MESUser1=$sv.user_struct(@@MESUser1, @@MESUserPwd1)
   @@carriers = $sv.carrier_list(status: 'AVAILABLE', empty: true, carrier: @@carrier)
   assert @@lot1=$sv.new_lot_release(product:@@product, stb:true, carrier:@@carriers.pop), 'lot not generated'
   assert @@lot2=$sv.new_lot_release(product:@@product, stb:true, carrier:@@carriers.pop), 'lot not generated'
   @@carrier1=$sv.lot_info(@@lot1).carrier
   @@carrier2=$sv.lot_info(@@lot2).carrier
   $sv.eqp_cleanup @@eqp_fb
   $sv.eqp_cleanup @@eqp_ib
   
   #setup data for fb tool
   $sv.claim_process_prepare @@lot1
   $sv.lot_opelocate @@lot2, @@eqp_ib_opno
   $sv.claim_process_prepare @@lot2
   $sv.slr   @@eqp_fb, lot:@@lot1, port:@@eqp_port_fb
   $sv.eqp_load @@eqp_fb, @@eqp_port_fb, @@carrier1
   @@cj1 =$sv.eqp_opestart(@@eqp_fb, @@carrier1)
   
   #setup data for ib tool
   $sv.slr   @@eqp_ib, lot:@@lot2, port:@@eqp_port_ib
   $sv.eqp_load @@eqp_ib, @@eqp_port_ib, @@carrier2
   @@cj2 =$sv.eqp_opestart(@@eqp_ib, @@carrier2)
   
  end
  
  
  def test01_privilegeCheck
   @@actions.each{ |action|
   $_MESUser.clientNode=''
   assert 11405==chamber_process_wafer_rpt(@@eqp_fb, @@eqp_chamber_fb, @@lot1,   action, cj:@@cj1, user:$_MESUser), "Expected to show unauthorized user error but it didn't"
   assert 11405==chamber_process_wafer_rpt(@@eqp_ib, @@eqp_chamber_ib, @@lot2,   action, cj:@@cj2, user:$_MESUser), "Expected to show unauthorized user error but it didn't"
   $_MESUser.clientNode='SystemUser'
   assert 0==chamber_process_wafer_rpt(@@eqp_fb, @@eqp_chamber_fb, @@lot1,  action, cj:@@cj1, user:$_MESUser), "Expected that it ignores privilege check for this user as client node is system user, but it didn't"
   assert 0==chamber_process_wafer_rpt(@@eqp_ib, @@eqp_chamber_ib, @@lot2,  action, cj:@@cj2, user:$_MESUser), "Expected that it ignores privilege check for this user as client node is system user, but it didn't"
   }
  end
  
  def test02_wrong_cj
  @@actions.each{ |action|
   assert 1231==chamber_process_wafer_rpt(@@eqp_fb, @@eqp_chamber_fb, @@lot1,   action, cj:(@@cj1 + '_wrong'), user:$_MESUser1), "Expected to show cj check error but it didn't"
   assert 1231==chamber_process_wafer_rpt(@@eqp_ib, @@eqp_chamber_ib, @@lot2,   action, cj:(@@cj1 + '_wrong'), user:$_MESUser1), "Expected to show cj check error but it didn't"
   $_MESUser.clientNode='SystemUser'
   assert 0==chamber_process_wafer_rpt(@@eqp_fb, @@eqp_chamber_fb, @@lot1,   action, cj:(@@cj1 + '_wrong'), user:$_MESUser), "Expected that it ignores cj check for this user as client node is system user, but it didn't"
   assert 0==chamber_process_wafer_rpt(@@eqp_ib, @@eqp_chamber_ib, @@lot2,   action, cj:(@@cj1 + '_wrong'), user:$_MESUser), "Expected that it ignores cj check for this user as client node is system user, but it didn't"
   }
  end
  
  def test03_lot_wafer_relation
   @@actions.each{ |action|
   #non sys user
   assert 1248==chamber_process_wafer_rpt(@@eqp_fb, @@eqp_chamber_fb, @@lot1,   action, cj:(@@cj1 ), wrong_wafer:'_wrong', user:$_MESUser1), "Expected to show wafer-lot relation error but it didn't"
   assert 1248==chamber_process_wafer_rpt(@@eqp_ib, @@eqp_chamber_ib, @@lot2,   action, cj:(@@cj2 ), wrong_wafer:'_wrong', user:$_MESUser1), "Expected to show wafer-lot relation error but it didn't"
   #sys user
   $_MESUser.clientNode='SystemUser'
   assert 0==chamber_process_wafer_rpt(@@eqp_fb, @@eqp_chamber_fb, @@lot1,   action, cj:(@@cj1 ), wrong_wafer:'_wrong', user:$_MESUser), "Expected that it ignores wafer-lot check for this user as client node is system user, but it didn't"
   assert 0==chamber_process_wafer_rpt(@@eqp_ib, @@eqp_chamber_ib, @@lot2,  action, cj:(@@cj2 ), wrong_wafer:'_wrong', user:$_MESUser), "Expected that it ignores wafer-lot check for this user as client node is system user, but it didn't"
   }
  end
  
  def test04_eqp_chamber_relation
   @@actions.each{ |action|
   #non sys user
   assert 1487==chamber_process_wafer_rpt(@@eqp_fb, @@eqp_chamber_fb + '_wrong', @@lot1,  action, cj:(@@cj1 ),  user:$_MESUser1), "Expected to show eqp-chamber relation error but it didn't"
   assert 1487==chamber_process_wafer_rpt(@@eqp_ib, @@eqp_chamber_ib + '_wrong', @@lot2,   action, cj:(@@cj2 ),  user:$_MESUser1), "Expected to show eqp-chamber relation error but it didn't"
   #sys user
   $_MESUser.clientNode='SystemUser'
   assert 0==chamber_process_wafer_rpt(@@eqp_fb, @@eqp_chamber_fb + '_wrong', @@lot1,  action, cj:(@@cj1 ),  user:$_MESUser), "Expected that it ignores eqp-chamber check for this user as client node is system user, but it didn't"
   assert 0==chamber_process_wafer_rpt(@@eqp_ib, @@eqp_chamber_ib + '_wrong', @@lot2,  action, cj:(@@cj2 ),  user:$_MESUser), "Expected that it ignores eqp-chamber check for this user as client node is system user, but it didn't"
   }
  end
  
  def test05_diff_action_codes
   @@actions.each{ |action|
   #non sys user
   assert 145==chamber_process_wafer_rpt(@@eqp_fb, @@eqp_chamber_fb , @@lot1,   action, cj:(@@cj1 ),  user:$_MESUser1, diff_action:true), "Expected to show diff actions error but it didn't"
   assert 145==chamber_process_wafer_rpt(@@eqp_ib, @@eqp_chamber_ib , @@lot2,   action, cj:(@@cj2 ),  user:$_MESUser1, diff_action:true), "Expected to show show diff actions error but it didn't"
   #sys user
   $_MESUser.clientNode='SystemUser'
   assert 0==chamber_process_wafer_rpt(@@eqp_fb, @@eqp_chamber_fb , @@lot1,   action, cj:(@@cj1 ),  user:$_MESUser, diff_action:true), "Expected that it ignores diff actions check for this user as client node is system user, but it didn't"
   assert 0==chamber_process_wafer_rpt(@@eqp_ib, @@eqp_chamber_ib , @@lot2,  action, cj:(@@cj2 ),  user:$_MESUser, diff_action:true), "Expected that it ignores diff actions check for this user as client node is system user, but it didn't"
   }
  end
  def testxx_diff_actions
  $_MESUser.clientNode='SystemUser'
  assert 0==chamber_process_wafer_rpt(@@eqp_fb, @@eqp_chamber_fb , @@lot1,   'ProcessStart', cj:(@@cj1 ),  user:$_MESUser, diff_action:true), "Expected that it ignores diff actions check for this user as client node is system user, but it didn't"
   assert 0==chamber_process_wafer_rpt(@@eqp_ib, @@eqp_chamber_ib , @@lot2,  'ProcessEnd', cj:(@@cj2 ),  user:$_MESUser, diff_action:true), "Expected that it ignores diff actions check for this user as client node is system user, but it didn't"
  end
  
  def test06_no_cj
   $sv.eqp_opestart_cancel @@eqp_fb, @@cj1
   $sv.eqp_opestart_cancel @@eqp_ib, @@cj2
    @@actions.each{ |action|
   #non sys user
   assert 1231==chamber_process_wafer_rpt(@@eqp_fb, @@eqp_chamber_fb , @@lot1,  action, cj:(@@cj1 ),  user:$_MESUser1), "Expected to show no cj error but it didn't"
   assert 1231==chamber_process_wafer_rpt(@@eqp_ib, @@eqp_chamber_ib , @@lot2, action, cj:(@@cj2 ),  user:$_MESUser1), "Expected to show no cj error but it didn't"
   #sys user
   $_MESUser.clientNode='SystemUser'
   assert 0==chamber_process_wafer_rpt(@@eqp_fb, @@eqp_chamber_fb , @@lot1,  action, cj:(@@cj1 ),  user:$_MESUser), "Expected that it ignores that cj exists when user has client node is system user, but it didn't"
   assert 0==chamber_process_wafer_rpt(@@eqp_ib, @@eqp_chamber_ib , @@lot2, action, cj:(@@cj2 ),  user:$_MESUser), "Expected that it ignores that cj exists when user has client node is system user, but it didn't"
   }
  end
  
  def chamber_process_wafer_rpt(eqp, chamber, lot, action='ProcessStart', params={})
	memo = params[:memo] || ''
    li = $sv.lot_info(lot, wafers: true)
    cj = params[:cj] || li.cj
	wrong_wafer = params[:wrong_wafer] || ''
	diff_action = params[:diff_action] || false
	user = params[:user] || $sv._user
    chi = [$sv.jcscode.pptProcessedChamberInfo_struct.new($sv.oid(chamber), $sv.siview_timestamp, $sv.any)
      ].to_java($sv.jcscode.pptProcessedChamberInfo_struct)
    i = 0
    chpli = li.wafers.collect {|w|
	if diff_action
	  action=='ProcessEnd'? action='ProcessStart' : action='ProcessEnd'  
	  puts action
	end
      wi = [$sv.jcscode.pptChamberProcessWaferInfo_struct.new($sv.oid(w.wafer + wrong_wafer), chi, $sv.any)].to_java($sv.jcscode.pptChamberProcessWaferInfo_struct)
      $sv.jcscode.pptChamberProcessLotInfo_struct.new($sv.oid(lot), action, wi, $sv.any)
    }
    #
	pp chpli
    res = $sv.manager.TxChamberProcessWaferRpt(user, $sv.oid(eqp), $sv.oid(cj), chpli, memo)
    return $sv.rc(res)
  end
  #TODO: Test wafer Lot ID
end
