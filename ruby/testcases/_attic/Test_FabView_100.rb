=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: sfrieske, 2015-09-30

=end

require 'SiViewTestCase'


class Test_FabView_100 < SiViewTestCase
  @@route = 'INC0384882.01'
  @@product = 'INC0384882.01'
  

  def self.startup
    super
    @@testlots = []
  end

  def self.manual_cleanup
    @@testlots.each {|lot| $sv.delete_lot_family(lot)}
  end

  
  def test11
    # create lot
    assert lot = $svtest.new_lot(route: @@route, product: @@product, carrier: '%')
    @@testlots << lot
    # after being processed the lot is gatepassed and banked in, script parameter changed 
    assert $sv.claim_process_lot(lot, happylot: true)
    # create sorter job
    assert ec = $svtest.get_empty_carrier
    assert $svtest.cleanup_eqp($svtest.sorter, mode: 'Auto-1')
    assert sjid = $sv.sj_create($svtest.sorter, 'P1', 'P2', lot, ec)
    assert $sv.claim_sj_execute($svtest.sorter)
  end  
end
