=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: Steffen Steidten, 2013-08-26

Version: 6.9

History:
  2015-01-09 ssteidte, use vaptest to generate test lots
  2016-11-07 sfrieske, added test11 for history cleanup
  2018-05-24 sfrieske, use @@vaptest instead of $vaptest, remove Corba client tests
  2019-02-22 sfrieske, minor cleanup, renamed from Test_VAMOS.. to VAMOS...
=end

require 'vamos/vamos'
require_relative 'VAMOS_AutoProc'


class VAMOS_StockerDisplay < VAMOS_AutoProc
  @@stocker2 = 'UTSTO12'
  @@outport = 'P4'
  # overrides for Dev tests in ITDC
  @@url = nil
  @@ior = nil

  @@sleeptime = 15


  def test00_setup
    $setup_ok = false
    #
    @@sd = Vamos::StockerDisplay.new($env, @@svtest.stocker, @@outport, url: @@url, ior: @@ior)  # pass IOR for dev tests in ITDC
    assert @@sd.comm_test
    @@sd.start_browser
    #
    refute_equal @@svtest.stocker, @@stocker2, 'pick another 2nd stocker'
    assert res = @@vaptest.find_operation_eqp(ib: true, max_batchsize: 4), 'no eqp found'
    @@opNo, @@eqp = res
    eqpi = @@sv.eqp_info(@@eqp)
    assert @@port = eqpi.ports.first.port
    $log.info "using opNo #{@@opNo} with eqp #{@@eqp}"
    #
    $setup_ok = true
  end

  def test10_happy
    $setup_ok = false
    #
    assert @@svtest.cleanup_eqp(@@eqp, mode: 'Auto-1')
    # preparation
    assert lot = @@svtest.new_lot
    @@testlots << lot
    carrier = @@sv.lot_info(lot).carrier
    assert_equal 0, @@sv.lot_cleanup(lot, opNo: @@opNo, stocker: @@stocker2)
    $log.info "waiting 10 s for the MDS to sync"; sleep 10
    puts "\nOpen a StockerDisplay for #{@@svtest.stocker} #{@@outport} and press ENTER!"
    gets
    #
    # SLR
    assert cj = @@sv.slr(@@eqp, port: @@port, lot: lot, carrier: carrier), "error reserving lot"
    # create transfer job to stock out the carrier and send set_user
    assert_equal 0, @@sv.carrier_xfer(carrier, @@stocker2, '', @@svtest.stocker, @@outport)
    assert @@sd.set_user(carrier, 'QAUser1')
    # wait for event "MO" and send carrier_info
    assert wait_for {@@sv.carrier_status(carrier).stocker == @@svtest.stocker}
    assert @@sd.carrier_info(carrier, false)  # 'removed false'
    # misc info
    sleep @@sleeptime
    assert @@sd.set_error(carrier)
    sleep @@sleeptime
    assert @@sd.clear_error(carrier)
    sleep @@sleeptime
    assert @@sd.set_user(carrier, 'QAUser2')
    assert @@sd.carrier_info(carrier, true) # 'removed true'
    $log.info 'carrier removed, must go to history'
    sleep @@sleeptime
    #
    ##assert_equal 0, @@sv.carrier_xfer_status_change(carrier, 'EO', @@eqp)
    assert_equal 0, @@sv.eqp_port_status_change(@@eqp, @@port, 'LoadComp')
    assert_equal 0, @@sv.eqp_load(@@eqp, @@port, carrier)
    $log.info 'carrier loaded, must disappear from history'
    sleep @@sleeptime
    ## unused method
    ## @@sd.remove_carrier_from_history(@@carrier)
    ## sleep @@sleeptime
    #
    $setup_ok = true
  end

  def test11_history
    assert @@svtest.cleanup_eqp(@@eqp, mode: 'Auto-1')
    # preparation
    assert lots = @@svtest.new_lots(4)
    @@testlots += lots
    carriers = lots.collect {|lot| @@sv.lot_info(lot).carrier}
    lots.each {|lot|
      assert_equal 0, @@sv.lot_cleanup(lot, opNo: @@opNo, stocker: @@stocker2)
    }
    $log.info "waiting 10 s for the MDS to sync"; sleep 10
    puts "\nOpen a StockerDisplay for #{@@svtest.stocker} #{@@outport} and press ENTER!"
    gets
    #
    # SLR
    assert cj = @@sv.slr(@@eqp, port: @@port, lot: lots, carriers: carriers), 'error reserving lot'
    # create transfer jobs to stock out the carriers
    carriers.each {|carrier|
      assert_equal 0, @@sv.carrier_xfer(carrier, @@stocker2, '', @@svtest.stocker, @@outport)
      assert @@sd.set_user(carrier, 'QAUser1')
      # wait for event "MO" and send carrier_info
      assert wait_for {@@sv.carrier_status(carrier).stocker == @@svtest.stocker}
      assert @@sd.carrier_info(carrier, true) # 'removed true'
      assert_equal 0, @@sv.carrier_xfer_status_change(carrier, 'EO', @@eqp) # not required
    }
    $log.info "carriers #{carriers} removed, must appear in history"
    sleep @@sleeptime
    # load carriers
    assert_equal 0, @@sv.eqp_port_status_change(@@eqp, @@port, 'LoadComp')
    carriers.each {|carrier|
      assert_equal 0, @@sv.eqp_load(@@eqp, @@port, carrier, ib: true)
    }
    $log.info 'carriers loaded, must disappear from history'
    sleep @@sleeptime
  end

end
