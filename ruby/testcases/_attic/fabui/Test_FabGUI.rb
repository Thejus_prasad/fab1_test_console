=begin
Test FabGUI Data Input

(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.


Author: Steffen Steidten, 2011-01-24
        Daniel Steger, 2013-05-07 added TMRS
=end

require "RubyTestCase"
require 'builder'
require 'fabgui'
require 'fabguictrl'
require 'ammo'

# Test FabGUI SOAP interface tests
class Test_FabGUI < RubyTestCase

  @@eqp = "MES500"
  @@lot = "U8ACK.00"
  @@pl = "ZG"
  @@pt = "PR-PHVTRNDM"#"RTPHTONIT1"

  def self.startup
    super
    if $env == "f8stag"
      @@lot = "8XYJ32055.000"
    end
    $tmrs = FabGUI::TMRSClient.new($env) unless $tmrs and $tmrs.env == $env
    $ammo = AMMO::Client.new($env) unless $ammo and $ammo.env == $env
    $fguictrl = FabGUI::LotCtrl.new($env) unless $fguictrl and $fguictrl.env = $env
  end

  def test00_setup
    $setup_ok = false
    assert $fguictrl.set_runtime_values({"Sampling.UseAmmo"=>"NO"}), "config is not correct"
    $setup_ok = true
  end

  def test10_no_access
    linfo = $sv.lot_info(@@lot, :wafers=>true)
    lot_wafers = {}
    lot_wafers[@@lot] = Hash[linfo.wafers.map {|w| [w.wafer, 1]}]
    # no access
    assert_raises(FabGUI::FabGUIError) do
      $tmrs.get_wafer_sampling(lot_wafers, @@pt, @@pl, :suser=>"gmstest05")
    end
  end

  def test11_set_and_get
    linfo = $sv.lot_info(@@lot, :wafers=>true)
    lot_wafers = {}
    lot_wafers[@@lot] = Hash[linfo.wafers.map {|w| [w.wafer, 1]}]
    assert $ammo.remove_wafer_selection(lot_wafers[@@lot].keys, @@pt, @@pl, :selecting_system=>"SUPERUSER")
    assert $tmrs.set_wafer_sampling(lot_wafers, @@pt, @@pl, :export=>"log/set_sampling.xml"), "failed to set wafer sampling"
    return
    _res = $tmrs.get_wafer_sampling(lot_wafers, @@pt, @@pl, :export=>"log/get_sampling.xml")
    _linfo = _res[:return][:lotInfos][:lotInfo]
    new_lot = {}
    new_lot[_linfo[:lotId]] = Hash[_linfo[:waferInfos][:waferInfo].map {|w| [w[:waferId], w[:selection].to_i]}]
    assert verify_hash(new_lot, lot_wafers), "wrong sampling setting"
  end

  def test12_set_not_allowed
    linfo = $sv.lot_info(@@lot, :wafers=>true)
    lot_wafers = {}
    lot_wafers[@@lot] = Hash[linfo.wafers.map {|w| [w.wafer, 2]}]
    assert $ammo.remove_wafer_selection(lot_wafers[@@lot].keys, @@pt, @@pl, :selecting_system=>"SUPERUSER")
    assert !$tmrs.set_wafer_sampling(lot_wafers, @@pt, @@pl, :export=>"log/set_sampling.xml"), "set wafer sampling not allowed"
  end
end
