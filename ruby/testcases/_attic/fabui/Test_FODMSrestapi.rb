=begin 
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.
    
Author: Thomas Klose

http://confluence/display/FODMS/IntermediateTarget+REST+API

Version: FOD-MS rest api
=end

require 'RubyTestCase'
require 'rqrsp_http'

class Test_FODMSrestapi < RubyTestCase
 
  @@msg_tgt = {"intTargetSelector": {"lotId": "U8ACK.00", "intTargetForLotSelector": {"category": "MANUAL", "mainPdId": "C02-1116.01", "opeNo": "6500.1000", "pdId": "FOUT-GATE.01"}}, "targetDate": "2017-08-09T23:59:00Z"}.to_json
  
  def self.startup
    super
    @@rq_none = RequestResponse::HttpJSON.new('itdc', url: 'http://f36fabguid1:8089/fodms/api/')
    @@rq_user = RequestResponse::HttpJSON.new('itdc', url: 'http://f36fabguid1:8089/fodms/api/', user: 'dnareike', password: 'Zm9kbXM=')
    @@rq_admin = RequestResponse::HttpJSON.new('itdc', url: 'http://f36fabguid1:8089/fodms/api/', user: 'fodau', password: 'Zm9kX0lUREM=')
  end

  def test10_get_no_auth
    lot = "U8ACK.00"
    pd = "FOUT-GATE.01"
    res = @@rq_none.get(resource: ["lots", lot, "intermediatetargets", pd])  
    assert @@rq_none.response.kind_of?(Net::HTTPOK), "http response failed"
    assert_equal lot, res["intTargetSelector"]["lotId"], "missing or wrong Lot ID"
    assert_equal pd, res["intTargetSelector"]["intTargetForLotSelector"]["pdId"], "missing or wrong PD ID"
    assert res["intTargetSelector"]["intTargetForLotSelector"]["opeNo"], "missing opNo"
    assert res["intTargetSelector"]["intTargetForLotSelector"]["category"] == "END", "missing or wrong category"
    assert res["intTargetSelector"]["intTargetForLotSelector"]["mainPdId"], "missing MainPD"
    
    lot = "NONELOT" # not existant lot id
    res = @@rq_none.get(resource: ["lots", lot, "intermediatetargets", pd])
    assert @@rq_none.response.kind_of?(Net::HTTPNotFound), 'bad response'
  end  
  
  def test20_get_basic_auth
    lot = "U8ACK.00"
    pd = "BANKIN.01"
    res = @@rq_user.get(resource: ["lots", lot, "intermediatetargets", pd])  
    assert @@rq_user.response.kind_of?(Net::HTTPOK), "http response failed"
    assert_equal lot, res["intTargetSelector"]["lotId"], "missing or wrong Lot ID"
    assert_equal pd, res["intTargetSelector"]["intTargetForLotSelector"]["pdId"], "missing or wrong PD ID"
    assert res["intTargetSelector"]["intTargetForLotSelector"]["opeNo"], "missing opNo"
    assert res["intTargetSelector"]["intTargetForLotSelector"]["category"] == "END", "missing or wrong category"
    assert res["intTargetSelector"]["intTargetForLotSelector"]["mainPdId"], "missing MainPD"
    
    lot = "NONELOT" # not existant lot id
    res = @@rq_user.get(resource: ["lots", lot, "intermediatetargets", pd])
    assert @@rq_user.response.kind_of?(Net::HTTPNotFound), 'bad response'
  end
  
  def test30_post_basic_auth
    lot = "U8ACK.00"
    pd = "BANKIN.01"
    res = @@rq_user.post(@@msg_tgt, resource: ["lots", lot, "intermediatetargets"])
    assert @@rq_user.response.kind_of?(Net::HTTPForbidden), 'bad response'
  end
  
  def test40_put_admin_auth
    
    lot = "U8ACK.00"
    pd = "FOUT-GATE.01"
    assert @@rq_admin.put(@@msg_tgt, resource: ["lots", lot, "intermediatetargets", pd])
    
    res = $rq.get(resource: ["lots", lot, "intermediatetargets", pd])
  end
end