=begin 
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.
    
Author: Thomas Klose, 2016-02-09

Version: 0.1

History
  2016-02-29 tklose, initial version
=end

require 'SiViewTestCase'
require 'rtdclient'

class Test_MfgCockpitRuleContent < SiViewTestCase
  @@rtd_rule = 'G_TASKLIST'
  @@dd_header = "Column	Type	Context	Mandatory	Introduced"
  @@data_dict = "task_id	string	general	yes	phase 2
task_type	string	general	yes	phase 1
rank_no	integer	general	yes	phase 1
priority_source	string	general	no	phase 1
group_id	string	general	yes	phase 1
area_id	string	general	yes	phase 1
toolgroup_list	string	general	yes	phase 2
eqp_id	string	general	no	phase 1
station_id	string	general	yes	phase 2
department	string	general	no	phase 1
reporting_stnfam	string	general	yes	phase 1
history_context_value	string	general	yes	phase 2
start_time	timestamp	general	yes	phase 1 
inhibit_id	string	inhibit only	yes	phase 1
inhibit_rsn_code	string	inhibit only	yes	phase 1
inhibit_claim_memo	string	inhibit only	no	phase 1
inhibit_start_time	timestamp	inhibit only	no	phase 1
inhibit_context_combination	string	inhibit only	no	phase 1
inhibit_reason_desc	string	inhibit only	no	phase 2
inhibit_context_value	string	inhibit only	no	phase 1
space_parameter	string	space only	no	phase 1
space_type	string	space only	no	phase 1
space_ch_id	integer	space only	no	phase 1
space_ckc_id	integer	space only	no	phase 1
space_lds	string	space only	no	phase 1
space_run_id	integer	space only	no	phase 1
space_sample_id	integer	space only	no	phase 1
last_control_job_id	string	general	no	phase 2
fdc_result_id	string	fdc only	no	phase 2
hold_user_id	string	hold only	no	phase 2
hold_reason_id	string	hold only	no	phase 2
hold_reason_description	string	hold only	no	phase 2
hold_claim_memo	string	hold only	no	phase 2
lot_id	string	general	no	phase 2
lot_wfr_qty	integer	general	no	phase 2
lot_priority_class	integer	general	no	phase 2
lot_priority_desc	string	general	no	phase 2
lot_sublot_type	string	general	no	phase 2
lot_tech_id	string	general	no	phase 2
lot_prodspec_id	string	general	no	phase 2
lot_mainpd_id	string	general	no	phase 2
lot_pd_id	string	general	no	phase 2
lot_ope_no	string	general	no	phase 2
lot_pd_id_desc	string	general	no	phase 2
lot_lrecipe_id	string	general	no	phase 2
lot_eqpid_last_processed	string	general	no	phase 2
lot_cast_id	string	general	no	phase 2
lot_owner_id	string	general	no	phase 2
lot_related_lot_id	string	general	no	phase 2
lot_is_pilot	boolean	general	no	phase 2
lot_timelink_criticality	string	general	no	phase 2
lot_timelink_timeout	string	general	no	phase 2"

    $dd = @@data_dict
    $dh = @@dd_header 
     
  def test00_setup
    #$sv = SiView::Client.new($env)
    $rtdclient = RTD::Client.new($env)
  end
  
  def test10_rule_content
    my_dict = {}
    
    max_err = 60
    cnt_err = 0 

    res = $rtdclient.dispatch_list("NONE", report: @@rtd_rule, category: "DISPATCHER/SCHEDULING")

    h = res['headers']
    rows = res['rows']
    
    @@data_dict.split("\n").each {|line|
      _c = line.split("\t")[0]
      if h.member?(_c)
        my_dict[_c] = {}
        @@dd_header.split("\t").each {|header|
          my_dict[_c][header] = line.split("\t")[@@dd_header.split("\t").index(header)]
        }
      else
        $log.warn "#{_c} not found"
      end
      }
        
    

    
    rows.each {|row|
      my_dict.each {|k, v|
        testobject = row[get_idx(h, k)]
        v.each {|test, expectation|
          case test
          when "Type"
            case expectation
            when "integer"
              $log.warn "#{k} #{test} is not #{expectation}: #{testobject}" unless is_integer(testobject) or is_empty(testobject)
              cnt_err += 1 unless is_integer(testobject) or is_empty(testobject)
            when "timestamp"
              $log.warn "#{k} #{test} is not #{expectation}: #{testobject}" unless is_timestamp(testobject)
              cnt_err += 1 unless is_timestamp(testobject)
            end
          when "Mandatory"
            case expectation
            when "yes"
              $log.warn "#{k} #{test} is #{expectation}: task_id = #{row[0]}" if is_empty(testobject)
              cnt_err += 1 if is_empty(testobject)
            end
          when "Context"
            pass # what to test here?
          end
        }
        # Introduced - skip this!
        #puts get_idx(h, k), k, v.inspect, testobject
      }
      break if cnt_err > max_err
    }
    
    $md = my_dict
  end
  
  def get_idx(ary, item)
    return ary.index(item)
  end
  
  def is_integer(testobject)
    return true if testobject =~ /\A\d+\z/
    return false
  end
  
  def is_empty(testobject)
    return true if testobject == ""
    return false
  end
  
  def is_timestamp(testobject)
    return true if testobject =~ /[0-9]{4}\-[0-9]{2}\-[0-9]{2}\T[0-9\:]{8}[\-+][012]{2}\:[0]{2}/
    #return true if testobject =~ /[0-9\-]{10}\T[0-9\:]{8}\+[012]{2}\:[0]{2}/
    #return true if testobject =~ /[0-9-T:]{19}\+[0-9:]{5}/
    return false
  end
  
  def inh_memo(fix=false)
    ch_id = "1060518 1070518 1060528".split.sample
    run_id, sample_id = "#{rand*10**6.to_i}".split('.')
    reason = "(Mean above control limit)|(Mean below control limit)|(Mean above spec limit)|(Mean below spec limit)".split('|').sample
    channelname = "2S@L-TRK-TPR-TACLDC 1S@L-TRK-TPR-TACLDC 2S@L-TRK-TPR-TACFDC".split.sample
    lds = ""  # for later use
    memo = "#{channelname}[#{reason} LDS->SETUP, RUN_ID->#{run_id}, CH_ID->#{ch_id}, CKC_ID->15, SAMPLE_ID->#{sample_id}]"
    return memo
  end
end

##File.open("G_TASKLIST-out.csv", 'w') {|f| f.write(headers.join(",")+"\n"); rows.each{|r| f.write(r.join(",")+"\n")}}