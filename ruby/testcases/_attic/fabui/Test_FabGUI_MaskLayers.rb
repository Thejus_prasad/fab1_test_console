=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

    
Author: Daniel Steger, 2013-05-30 created

History:
  2018-03-06 sfrieske, replaced user_parameter_set_verify by user_paramer_change
=end

require 'fabguictrl'
require 'RubyTestCase'

class Test_FabGUI_MaskLayers < RubyTestCase
  Description = "Sends a lot through the Fabs and collects the mask layers"

  @@backup_env = "f8stag"
  @@lot = nil
  @@product = "UT-PRODUCT-FAB8.01"
  @@route = "UTRT101.01"
  @@nprobank_return = "O-BACKUPOPER"
  @@backup_opNo = "1000.200"
  @@return_opNo = "1000.9000"
  @@return_bank = "O-BACKUPOPER"
  @@src_fab_code = "F1"
  @@other_fab = "F8"
  @@erpitem = "ENGINEERING-U00"
  
  def setup
    super
    if $env == "f8stag"
      @@product = "UT-PRODUCT101.01"
      @@backup_env = "itdc"
      @@backup_bank = "O-BACKUPOPER"
      @@return_bank = "W-BACKUPOPER"
      @@wrong_bank = "OFFSITE-BANK"
      @@src_fab_code = "F8"
      @@other_fab = "F1"
      @@return_opNo = "2700.100"
    end    
    $svbak = SiView::MM.new(@@backup_env) unless $svbak and $svbak.env == @@backup_env
    $fguictrl = FabGUI::ProcessCtrl.new($env) unless $fguictrl and $fguictrl.env = $env
  end
      
  def test10_setup_external
    $setup_ok = false
    # Config for External ML
    assert $fguictrl.set_runtime_values({
      "SHIPPING.ExternalMaskLayers.ProductGroupUData.Name"=>"DeviceID",
      "SHIPPING.ExternalMaskLayers.ProductGroupUData.Value"=>"QATEST"
    }), "config is not correct"
    # 
    @@lot = nil
    self._prepare_send_receive_lot
    $setup_ok = true
  end
  
  def test11_verify
    puts "check sum of masklayers reported tp ERP system: should be 30"
    gets
  end
  
  def test20_setup
    $setup_ok = false
    # Config for External ML (disabled)
    assert $fguictrl.set_runtime_values({
      "SHIPPING.ExternalMaskLayers.ProductGroupUData.Name"=>"",
      "SHIPPING.ExternalMaskLayers.ProductGroupUData.Value"=>""
    }), "config is not correct"
    #
    @@lot = nil
    self._prepare_send_receive_lot
    $setup_ok = true
  end
  
  def test21_verify
    puts "check sum of masklayers reported tp ERP system: should be 10"
    gets
  end
  
  def test30_setup
    $setup_ok = false
    self._prepare_send_lot
    
    $setup_ok = true
  end
  
  def test31_verify
    #TODO: need to define checks for scriptparameters, etc.
  end
  
  def _prepare_send_receive_lot
    @@lot ||= $sv.new_lot_release :product=>@@product, :route=>@@route, :stb=>true, :waferids=>"t7m12"
    sleep 30 # for STB WIP Attributes
    $sv.lot_hold_release @@lot, nil
    assert_equal 0, $sv.user_parameter_change("Lot", @@lot, "ERPItem", @@erpitem)
    $sv.lot_opelocate @@lot, @@backup_opNo

    # nonstd-ship
    @@ship_time = Time.now
    puts "Use non-standard shipment in FabGUI. Choose Lot will Return and an available non-pro bank"
    gets
    assert_equal "NonProBank", $sv.lot_info(@@lot).status, "lot should be in non-pro bank"
    notes = $sv.lot_notes(@@lot)
    notes = notes.select {|note| note.user == "X-FABGUI" && note.timestamp > $sv.siview_timestamp(@@ship_time)}
    assert notes.find {|note| note.title == "FabGUI.Shipping.ProcessedML"}, "should have Fab specific mask layers"
    assert notes.find {|note| note.title == "FabGUI.Shipping.ExternalML"}, "should have external mask layers"
    assert notes.find {|note| note.title == "FabGUI.Shipping.RawWaferType"}, "should have raw wafer type"
         
    $sv.backup_prepare_send_receive(@@lot, $svbak)
    $svbak.lot_opelocate @@lot, @@return_opNo 
    # nonstd-ship
    $svbak.lot_note_register @@lot, "FabGUI.Shipping.ProcessedML", "{\"fab\":\"#{@@other_fab}\",\"maskLayers\":7}"
    $svbak.backup_prepare_return_receive(@@lot, $sv, :return_bank=>@@return_bank)
    $sv.lot_opelocate @@lot, "last"
    #
    # nonstd-ship
    puts "Use non-standard shipment in FabGUI. Choose Lot will not Return and an available non-pro bank"
    gets
    assert_equal "COMPLETED", $sv.lot_info(@@lot).status, "lot should be banked in"       
  end
  
  # for receiving GUI
  def _prepare_send_lot
    @@lot = $sv.new_lot_release :product=>@@product, :route=>@@route, :stb=>true, :waferids=>"t7m12"
    sleep 30 # for STB WIP Attributes
    $sv.lot_hold_release @@lot, nil
    assert_equal 0, $sv.user_parameter_change("Lot", @@lot, "ERPItem", @@erpitem)

    # nonstd-ship
    @@ship_time = Time.now
    assert $sv.lot_note_register @@lot, "FabGUI.Shipping.ProcessedML", "{\"fab\":\"#{@@src_fab_code}\",\"maskLayers\":7}", :user=>"X-FABGUI", :password=>"uJitvQ"
    assert $sv.lot_note_register @@lot, "FabGUI.Shipping.ExternalML", "{\"fab\":\"#{@@src_fab_code}\",\"maskLayers\":20}", :user=>"X-FABGUI", :password=>"uJitvQ"
    assert $sv.lot_note_register @@lot, "FabGUI.Shipping.RawWaferType", "300MM_PRIME_BULK", :user=>"X-FABGUI", :password=>"uJitvQ"
    
    carrier = $svbak._get_empty_carrier("F%")
    assert $sv.lot_note_register @@lot, "FabGUI.BackupOper.Carrier", carrier, :user=>"X-FABGUI", :password=>"uJitvQ"
    
    assert $sv.prepare_for_backup(@@lot, :backup_opNo=>@@backup_opNo, :backup_bank=>@@backup_bank), "failed to prepare backup"
    assert_equal 0, $sv.backupop_lot_send(@@lot, $svbak), "failed to send lot"
    
    #
    # receive
    puts "Use receiving GUI in FabGUI. Receive lot at backup site. #{@@lot}"
    gets    
  end
end