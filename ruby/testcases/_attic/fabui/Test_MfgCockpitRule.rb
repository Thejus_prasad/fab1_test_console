=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: Thomas Klose, 2016-02-09

Version: 0.1

History
  2016-02-29 tklose, initial version
=end

require 'SiViewTestCase'
require 'rtdclient'

class Test_MfgCockpitRule < SiViewTestCase
  @@phase1 = true
  @@phase2 = true
  @@phase3 = false

  @@rtd_rule = 'G_TASKLIST'
  @@eqps_twqual = "ALC400 POL205 CVD400".split # bottleneck
  @@eqps_stnfam = "LSA4100 IMP100 FVX150 ETX900".split # stnfam
  @@eqps_inhratio = "ETX101 ETX100".split  # inhibit ratio

  def test00_setup
    #$sv = SiView::Client.new($env)
    $rtdclient = RTD::Client.new($env)
    if $env == 'itdc'
      @@inhtypes = "SpaceInhibit FDCInhibit OtherXInhibit FDCLotHold OtherLotHold".split
      @@inhtypes += "SPCLotHold".split if @@phase2 == true
      @@inhtypes += "MergeHold".split if @@phase3 == true
    else
      @@inhtypes = "SpaceInhibit FDCInhibit OtherXInhibit QualInhibit SPCLotHold FDCLotHold MergeHold OtherLotHold".split
    end

    # cleanup
    eqps = @@eqps_twqual + @@eqps_inhratio + @@eqps_stnfam
    eqps.each{|eqp|
      $sv.inhibit_cancel("Equipment", eqp)
      }

    # create a better memo
    ch_id = "1060518 1070518 1060528".split.sample
    run_id, sample_id = "#{rand*10**6.to_i}".split('.')
    reason = "(Mean above control limit)|(Mean below control limit)|(Mean above spec limit)|(Mean below spec limit)".split('|').sample
    channelname = "2S@L-TRK-TPR-TACLDC 1S@L-TRK-TPR-TACLDC 2S@L-TRK-TPR-TACFDC".split.sample
    @@memo = "#{channelname} #{reason} LDS->SETUP, RUN_ID->#{run_id}, CH_ID->#{ch_id}, CKC_ID->15, SAMPLE_ID->#{sample_id}"
  end

  def test10_call_rule
    columns = "rank_no priority_source group_id area_id eqp_id department reporting_stnfam inhibit_id inhibit_rsn_code inhibit_claim_memo
    inhibit_start_time inhibit_context_combination inhibit_context_value space_parameter space_type space_ch_id space_ckc_id space_lds
    space_run_id space_sample_id".split if $env == "f8stag"

    columns = "task_id task_type rank_no priority_source group_id area_id toolgroup_list eqp_id station_id department reporting_stnfam
    history_context_value task_start_time inhibit_id inhibit_rsn_code inhibit_claim_memo inhibit_start_time inhibit_context_combination
    inhibit_reason_desc inhibit_context_value space_parameter space_type space_ch_id space_ckc_id space_lds space_run_id space_sample_id
    last_control_job_id hold_user_id hold_reason_id hold_reason_description hold_claim_memo lot_id lot_wfr_qty lot_priority_class
    lot_priority_desc lot_sublot_type lot_tech_id lot_prodspec_id lot_mainpd_id lot_pd_id lot_ope_no lot_pd_id_desc lot_lrecipe_id
    lot_eqpid_last_processed lot_cast_id lot_owner_id lot_related_lot_id lot_timelink_criticality
    lot_timelink_timeout start_time".split if $env == "itdc" # 12.05.

    columns += "fdc_result_id".split if $env == "itdc" && @@phase3 == true

    res = $rtdclient.dispatch_list("NONE", report: @@rtd_rule, category: "DISPATCHER/SCHEDULING")
    headers = res['headers']
    rows = res['rows']
    #assert headers.size == columns.size, "header size is not correct, maybe G_TASKLIST is not setup"
    assert rows.size > 0, "rows are emty"

    columns.each {|c| $log.warn("column #{c} missing in header") unless headers.member?(c)}
    headers.each {|h| $log.warn("found header #{h} missing in data dict") unless columns.member?(h)}

    inhtypes = rows.collect{|row| row[1]}.uniq
    $log.info("checking all inhibit types...")
    $log.info("found inhibit types #{inhtypes.inspect}")
    @@inhtypes.each {|it|
      $log.warn("No #{it} found!") unless inhtypes.member?(it)}

    # assert false == inhtypes.member?("FDCInhibit"), "No FDCInhibit found!"
    # assert inhtypes.member?("SpaceInhibit"), "No SpaceInhibit found!"
    # assert false == inhtypes.member?("MissingEquipmentRecipe"), "MissingEquipmentRecipe found!" # MissingEquipmentRecipe should not be in result!
  end

  def test20_bottleneck_priority
    # create inhibits for TW_Qual GUI (ALC400=1, POL205=2, OTHERS=3)
    # X_ALC400|1
    # X_POL205|2

    memo = "G_TASKLIST_" + Time.now.to_s
    memo = @@memo
    eqps = @@eqps_twqual

    eqps.each {|eqp|
      $sv.inhibit_entity("Equipment", eqp, reason: "X-S2", memo: inh_memo)
      }
    sleep 60
    dl = $rtdclient.dispatch_list("NONE", report: @@rtd_rule, category: "DISPATCHER/SCHEDULING")

    rank = []
    ieqp = []

    rows = dl['rows']
    rows.each {|row|
      if row[1] == "SpaceInhibit"
        rank << row[3] if eqps.member?(row[9])
        ieqp << row[9] if eqps.member?(row[9])
      end
      }

    assert rank.size == 3, "not all inhibits found #{ieqp.inspect}"
    assert ieqp == eqps, "Equipment not sorted correctly #{ieqp.inspect}"
  end

  def test30_stnfam
    # create inhibits for stnfam tests (EquipmentType Udata ReportFamily)
    # D3_LaserAnneal_LSA10x|1|HK -> LSA4100
    # I3_IMP-HC_AMAT-Quantum_SD_IMP10x|2|MOL -> IMP100
    # D3_TEOSFurn_FVX15x|4|FEOL -> FVX150
    # E3_MetalEtch_ETX90x|5|BEOL -> ETX900

    memo = "G_TASKLIST_" + Time.now.to_s
    memo = @@memo
    eqps = @@eqps_stnfam

    eqps.each {|eqp|
      $sv.inhibit_entity("Equipment", eqp, reason: "X-S1", memo: inh_memo)
      }
    sleep 60
    dl = $rtdclient.dispatch_list("NONE", report: @@rtd_rule, category: "DISPATCHER/SCHEDULING")

    rank = []
    ieqp = []

    rows = dl['rows']
    rows.each {|row|
      if row[1] == "SpaceInhibit"
        rank << row[3] if eqps.member?(row[9])
        ieqp << row[9] if eqps.member?(row[9])
      end
      }

    assert rank.size == 4, "not all inhibits found #{ieqp.inspect}"
    assert ieqp == eqps, "Equipment not sorted correctly #{ieqp.inspect}"
  end

  def test40_inhibit_ratio
    #eqp_id|snap|wip|INHIBIT_ratio|lot_id_inh
    #ETX100|02/24/2016 10:02:00|38|0.0|
    #ETX101|02/24/2016 10:02:00|1000|0.5|   <- PRIO!

    memo = "G_TASKLIST_" + Time.now.to_s
    memo = @@memo
    eqps = @@eqps_inhratio

    eqps.each {|eqp|
      $sv.inhibit_entity("Equipment", eqp, reason: "X-S1", memo: inh_memo)
      }
    sleep 60
    dl = $rtdclient.dispatch_list("NONE", report: @@rtd_rule, category: "DISPATCHER/SCHEDULING")

    rank = []
    ieqp = []

    rows = dl['rows']
    rows.each {|row|
      if row[1] == "SpaceInhibit"
        rank << row[3] if eqps.member?(row[9])
        ieqp << row[9] if eqps.member?(row[9])
      end
      }

    assert rank.size == 2, "not all inhibits found #{ieqp.inspect}"
    assert ieqp == eqps, "Equipment not sorted correctly #{ieqp.inspect}"
  end

  def xtest50_age_of_task

  end

  def xtest60_dtc_rule

  end

  def xtest70_rule_content

    #inhtypes = "SpaceInhibit FDCInhibit OtherXInhibit FDCLotHold OtherLotHold".split
    #inhtypes += "SPCLotHold".split if @@phase2 == true
    #inhtypes += "MergeHold".split if @@phase3 == true


    res = $rtdclient.dispatch_list("NONE", report: @@rtd_rule, category: "DISPATCHER/SCHEDULING")
    h = res['headers']
    rows = res['rows']

    rows.each {|r|
      if r[get_idx(h, "task_type")] == "SpaceInhibit"
        pass
      end
      }
  end

  def get_idx(ary, item)
    return ary.index(item)
  end

  def inh_memo(fix=false)
    ch_id = "1060518 1070518 1060528".split.sample
    run_id, sample_id = "#{rand*10**6.to_i}".split('.')
    reason = "(Mean above control limit)|(Mean below control limit)|(Mean above spec limit)|(Mean below spec limit)".split('|').sample
    channelname = "2S@L-TRK-TPR-TACLDC 1S@L-TRK-TPR-TACLDC 2S@L-TRK-TPR-TACFDC".split.sample
    lds = ""  # for later use
    memo = "#{channelname}[#{reason} LDS->SETUP, RUN_ID->#{run_id}, CH_ID->#{ch_id}, CKC_ID->15, SAMPLE_ID->#{sample_id}]"
    return memo
  end
end

##File.open("G_TASKLIST-out.csv", 'w') {|f| f.write(headers.join(",")+"\n"); rows.each{|r| f.write(r.join(",")+"\n")}}
