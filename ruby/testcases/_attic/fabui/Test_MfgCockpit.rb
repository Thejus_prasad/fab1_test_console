=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: Thomas Klose, 2016-02-09

Version: 0.1

History
  2016-02-09 tklose,   initial version
  2017-01-05 sfrieske, use new  RTD::EmuRemote, minor cleanup
=end

require 'SiViewTestCase'
require 'rtdserver/rtdemuremote'

class Test_MfgCockpit < SiViewTestCase
  @@rtd_rule = 'G_TASKLIST'
  @@coldefs = {}
  @@data_folder = "etc/testcasedata/rtdemulator/cockpit/"

  def teardown
    $rtdremote.delete_emustation(@@rtd_rule)
  end

  def test00_setup
    $setup_ok = false
    #
    $rtdremote = RTD::EmuRemote.new($env)
    $rtdremote.delete_emustation(@@rtd_rule)
    columns = %w(task_type rank_no group_id area_id reporting_stnfam inhibit_id rsn_code claim_memo start_time priority_source
      eqp_id inhibit_context_combination inhibit_context_value chamber_id lcrecipe_id mainpd_id modulepd_id mrecipe_id ope_no
      pd_id prodspec_id product_grp reticle_id rtclgrp_id tech_id sub_lot_types space_parameter space_type space_ch_id space_ckc_id
      space_lds space_run_id space_sample_id)
    types = columns.collect {|c| ['rank_no'].member?(c) ? 'INTEGER' : 'STRING'}
    @@headers = {'headers'=>columns, 'widths'=>Array.new(columns.size, 12), 'types'=>types}
    # @@coldefs = Hash[columns.collect {|c|
    #   t = ['rank_no'].member?(c) ? 'INTEGER' : 'STRING'
    #   [c, RTD::Emulator::Column.new(c, 12, t)]
    # }]

    $setup_ok = true
  end

  def test10_mfgcockpit_etch_inh
    datafile = "etch_inh.data"
    data = get_rtdresponse_from_file(datafile)
    # rtdresponse = RTD::StaticReply.new(coldefs: @@coldefs, colkeys: @@coldefs.keys, data: data)
    # $log.info("Setting up rule for #{__method__}")
    # $rtdremote.add_emustation(@@rtd_rule, method: rtdresponse)
    @@rtdremote.add_emustation(@@rtd_rule) {|*args| @@headers.merge('rows'=>data)}
    sleep 300
    $log.info("Check MfgCockpit for #{__method__} and press Enter to proceed testing..."); gets
  end

  def generate_rtdresponse(width=33)
    data = []
'SpaceInhibits' '1' '3870985150' '1L-MSK' 'L3_ArFim_ALC3xx' '20160209133426606280.00005398.X-INSPACE' 'X-S4' '2S@L-TRK-TPR-TACLDC[(Mean above control limit) LDS->SETUP, RUN_ID->3870985150, CH_ID->1060518, CKC_ID->15, SAMPLE_ID->4519385483]' '2016-02-09T13:34:26+00:00' '' 'ALC1360' 'Equipment,Machine Recipe' 'ALC1360,L-ALCA.L-RTI33Y.01' '' '' '' '' 'L-ALCA.L-RTI33Y.01' '' '' '' '' '' '' '' '' '2S@L-TRK-TPR-TACLDC' 'Mean above control limit' '1060518' '15' 'SETUP' '3870985150' '4519385483'


    return data
  end

  def get_rtdresponse_from_file(filename, width=33)
    data = []

    File.readlines(@@data_folder + filename).each {|line|
      if line.start_with?("#") or line.start_with?("\n")
        next
      else
        # extract data from line, append line to data if data seems to be valid
        linedata = line[1...line.rindex("'")].split("' '")
        if linedata.size == width
          data << linedata
        end
      end
      }
    return data
  end
end


=begin
TW Qual Priorities ITDC (fixed list of tools/ toolsets for first priorization)

X_D3_LaserAnneal_LSA10x|1
X_SNK4380|1
X_ALC400|1
X_F3_TiNALD_MDX72x|1
X_P3_STIOx_POL20x|1
X_ETX105|2
X_D3_Epitax_EPI20x|2
X_SNK330|2
X_ALC1363|2
X_ETX104|2
X_POL205|2
X_ETX1273|2
X_SNK340|2
X_ALC353|2
X_I3_FEoL-SWC_SNK38x|2
X_ACS103|2

=end
