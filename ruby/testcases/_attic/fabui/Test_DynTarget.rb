=begin 
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.
    
Author: Thomas Klose

Version: 
  - initial version 2015.07.23
  
=end

require 'RubyTestCase'
require 'misc'

class Test_DynTarget < RubyTestCase

  @@lot_list = {}
  
  def setup
    super
  end
  
  def self.shutdown
    $log.info "test lots:\n#{@@lot_list.pretty_inspect}"
    #for k, v in @@lot_list
    #  puts "#{k}: #{v}"
    #end
  end
  
  def test01_setup
    @@waittime = 10
  
    # let
    if $sv.env == 'let'
      @@route = "C02-1138.01"
      @@product = "3150CC.D1"
      @@sltype_1 = "WBL1"
      @@sltype_2 = "WBL2"
      @@stop_oper = "RX-MSKA.01"
      @@customer = "wbl"
    end
    
    # itdc
    if $sv.env == 'itdc'
      @@route = "C02-1219.01"
      @@product = "3007AA.A0"
      @@sltype_1 = "WBL1"
      @@sltype_2 = "WBL2"
      @@stop_oper = "RX-MSKA-45BK.01"
      @@customer = "wbl"
    end
  end
  
  def test10_lot1_rxmsk
    fodinit = Time.now + duration_in_seconds('70d')
    
    lot = $sv.new_lot_release(stb: true, route: @@route, product: @@product, sublottype: @@sltype_1, customer: @@customer)
    assert $sv.user_parameter_set_verify('Lot', lot, 'FOD_INIT', fodinit)
    sleep @@waittime
    assert $sv.lot_hold_release(lot, nil)
    while true
      assert $sv.lot_gatepass(lot)
      break if $sv.lot_info(lot).op == @@stop_oper
    end
    
    @@lot_list[__method__] = lot
  end
  
  def test11_lot2_norm
    fodinit = Time.now + duration_in_seconds('70d')
    
    lot = $sv.new_lot_release(stb: true, route: @@route, product: @@product, sublottype: @@sltype_1, customer: @@customer)
    assert $sv.user_parameter_set_verify('Lot', lot, 'FOD_INIT', fodinit)
    sleep @@waittime
    assert $sv.lot_hold_release(lot, nil)
    
    @@lot_list[__method__] = lot
  end  
  
  def test12_lot3_140d
    fodinit = Time.now + duration_in_seconds('140d')
    
    lot = $sv.new_lot_release(stb: true, route: @@route, product: @@product, sublottype: @@sltype_1, customer: @@customer)
    assert $sv.user_parameter_set_verify('Lot', lot, 'FOD_INIT', fodinit)
    sleep @@waittime
    assert $sv.lot_hold_release(lot, nil)
    
    @@lot_list[__method__] = lot
  end  
  
  def test13_lot4_wbl2
    fodinit = Time.now + duration_in_seconds('70d')
    
    lot = $sv.new_lot_release(stb: true, route: @@route, product: @@product, sublottype: @@sltype_2, customer: @@customer)
    assert $sv.user_parameter_set_verify('Lot', lot, 'FOD_INIT', fodinit)
    sleep @@waittime
    assert $sv.lot_hold_release(lot, nil)
    
    @@lot_list[__method__] = lot
  end  

  def test14_lot5_wbl2_20d
    fodinit = Time.now + duration_in_seconds('20d')
    
    lot = $sv.new_lot_release(stb: true, route: @@route, product: @@product, sublottype: @@sltype_2, customer: @@customer)
    assert $sv.user_parameter_set_verify('Lot', lot, 'FOD_INIT', fodinit)
    sleep @@waittime
    assert $sv.lot_hold_release(lot, nil)
    
    @@lot_list[__method__] = lot
  end    
end