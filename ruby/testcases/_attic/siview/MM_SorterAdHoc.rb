=begin 
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.
    

Author: sfrieske, 2018-07-11

History:
=end

require 'SiViewTestCase'


# Test Sorter AdHoc Actions, MSRs 1271995, (not yet: 1350126)
class MM_SorterAdHoc < SiViewTestCase
  @@eqp = 'UTS001'
  @@port1 = 'P1'
  @@port2 = 'P2'

  @@testlots = []

  def self.after_all_passed
    @@sv.eqp_cleanup(@@eqp)
    @@testlots.each {|lot| @@sv.delete_lot_family(lot)}
  end
          

  def test00_setup
    $setup_ok = false
    #
    assert lot = @@svtest.new_lot, 'error creating test lot'
    @@testlots << lot
    #
    $setup_ok = true
  end
  
  def testdev11_inhibit  # MSR 1350126, fails in C7.9
    lot = @@testlots.first
    assert_equal 0, @@sv.eqp_cleanup(@@eqp, mode: 'Auto-1')
    assert_equal 0, @@sv.lot_cleanup(lot)
    assert res = @@sv.claim_process_prepare(lot, eqp: @@eqp, mode: 'Auto-1', purpose: 'Other', slr: false)
    cj, cjcarriers, eqpi, port, pg = res
    assert carrier = @@sv.lot_info(lot).carrier
    assert @@sv.inhibit_entity(:eqp, @@eqp)
    refute_equal 0, @@sv.sorter_action(@@eqp, port, port, 'WaferIDRead', pg: pg, carrier: carrier), 'action must be refused'
  end

  #  wrong understanding, not active anyway; preserve
  def testdev21_carrier_notavailable  # MSR 1271995
    lot = @@testlots.first
    assert_equal 0, @@sv.eqp_cleanup(@@eqp, mode: 'Auto-1')
    assert_equal 0, @@sv.lot_cleanup(lot)
    assert res = @@sv.claim_process_prepare(lot, eqp: @@eqp, port: @@port1,  mode: 'Auto-1', purpose: 'Other', slr: false)
    assert carrier = @@sv.lot_info(lot).carrier
    assert ec = @@svtest.get_empty_carrier, 'no empty carrier'
    assert_equal 0, @@sv.carrier_xfer_status_change(ec, 'EO', @@eqp)
    assert_equal 0, @@sv.eqp_port_status_change(@@eqp, @@port2, 'LoadComp')
    assert_equal 0, @@sv.eqp_load(@@eqp, @@port2, ec, purpose: 'Other')
    # src carrier AVAILABLE, as reference
    # assert_equal 0, @@sv.carrier_status_change(carrier, 'NOTAVAILABLE')
    assert_equal 0, @@sv.sorter_action(@@eqp, @@port1, @@port2, 'AdjustToSorter', lot: lot, tgt_carrier: ec)
    # make src carrier NOTAVAILABLE and call AdjustToSorter again
    # assert_equal 0, @@sv.wafer_sort(lot, carrier)
    assert_equal 0, @@sv.carrier_status_change(carrier, 'NOTAVAILABLE')
    assert_equal 0, @@sv.sorter_action(@@eqp, @@port2, @@port1, 'AdjustToSorter', lot: lot, tgt_carrier: ec)
  end

end
