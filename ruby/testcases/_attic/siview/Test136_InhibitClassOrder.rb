=begin 
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.
    
Author:  Steffen Steidten, 2014-05-21

Version: C6.20

=end

require 'RubyTestCase'

# Test the influence of class order (only) on inhibit management.
#
# The effects on TXns like WhatNext etc. is tested separately.
class Test136_InhibitClassOrder < RubyTestCase
  Description = ""
  
  @@productgroup = 'TEST'
  @@product = 'SHELBY21AE.B4'
  @@eqp = 'UTR003'
  @@chamber = nil
  @@op = 'UT-UTI003-RTCL.01'

  
  def self.after_all_passed
  end
  
  def test00_setup
    $setup_ok = false
    @@chamber = $sv.eqp_info(@@eqp).chambers.first.chamber
    assert @@chamber, "#{@@eqp} must be a chamber tool"
    $setup_ok = true
  end
  
  def test12_2classes
    do_test($_inhibit_classes[:productgroup_pd], [@@productgroup, @@op])
  end
  
  def test13_3classes
    do_test($_inhibit_classes[:productgroup_pd_eqp], [@@productgroup, @@op, @@eqp])
  end
  
  def test22_2classes_attrib
    do_test($_inhibit_classes[:eqp_chamber_product], [@@eqp, @@product], [@@chamber, nil])
  end
  
  def test23_3classes_attrib
    do_test($_inhibit_classes[:eqp_chamber_productgroup_pd], [@@eqp, @@productgroup, @@op], [@@chamber, nil, nil])
  end
  
  
  def do_test(classes, objects, attrib=[])
    # test with all permutations
    classes_p = classes.permutation(classes.size).to_a
    objs_p = objects.permutation(classes.size).to_a
    attrib_p = attrib.permutation(classes.size).to_a
    # clean up inhibits and verify
    classes_p.each_with_index {|classes, i| $sv.inhibit_cancel(classes, objs_p[i])}
    classes_p.each_with_index {|classes, i| assert_equal [], $sv.inhibit_list(classes, objs_p[i])}
    #
    # set - list in all permutations
    classes_p.each_with_index {|classes, i| 
      # set inhibit
      assert_equal 0, $sv.inhibit_entity(classes, objs_p[i], attrib: attrib_p[i])
      # list
      classes_p.each_index {|j|
        assert_equal 1, $sv.inhibit_list(classes_p[j], objs_p[j]).size, "inhibit not shown as #{classes_p[j].inspect}"
      }
      # delete to clean up
      assert_equal 0, $sv.inhibit_cancel(classes, objs_p[i], attrib: attrib_p[i])
    }
    #
    # set - delete in all permutaions
    classes_p.each_with_index {|classes, i| 
      classes_p.each_index {|j|
        # set inhibit
        assert_equal 0, $sv.inhibit_entity(classes, objs_p[i], attrib: attrib_p[i])
        # delete inhibit
        assert_equal 0, $sv.inhibit_cancel(classes_p[j], objs_p[j], attrib: attrib_p[j])
        assert_equal [], $sv.inhibit_list(classes, objs_p[i])
        assert_equal [], $sv.inhibit_list(classes_p[j], objs_p[j])
      }
    }
  end

end  