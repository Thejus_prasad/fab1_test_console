=begin 
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.
    
Author:  sfrieske, 2015-12-09 
Version: 0.1.0

History:
=end

require 'SiViewTestCase'


# Test SiView DC
class Test681_DC < SiViewTestCase
  @@opNo_proc = '1000.100'
  @@eqp_proc = 'UTC001'
  @@opNo_meas = '1000.995'  # LR UTLR007.01 with DCDef UTDCDEF01
  @@eqp_meas = 'UTF003'

  @@loops = 1000


  def self.after_all_passed
    $sv.delete_lot_family(@@lot)
  end


  def test00_setup
  end

  # generate many data sets for MM DB
  def test11_load
    assert lot = @@lot = $svtest.new_lot
    @@loops.times {|n|
      $log.info "\n\n--- loop #{n+1}/#{@@loops}"
      assert_equal 0, $sv.lot_opelocate(lot, @@opNo_proc)
      assert $sv.claim_process_lot(lot)
      while $sv.lot_info(lot).opNo != @@opNo_meas
        assert_equal 0, $sv.lot_gatepass(lot)
      end
      assert $sv.claim_process_lot(lot, eqp: @@eqp_meas)
    }
  end
end
