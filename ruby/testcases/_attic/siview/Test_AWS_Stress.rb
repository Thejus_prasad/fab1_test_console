=begin 
Stress test for physical wafer sorters in Auto-2 Equipment mode. 
Multiple PDs with different recipes are used.

(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.
    
Author: Steffen Steidten, 2011-08-22
=end

require "RubyTestCase"

# Stress test for physical wafer sorters in Auto-2 Equipment mode. 
# 
# Multiple PDs with different recipes are used.

class Test_AWS_Stress < RubyTestCase
  Description = "Test AWS Wafer Sorter"
  
#  @@carrier1 = "A315" #"D304"
#  @@carrier2 = "B316"
  @@lot1 = "U757H.00"
 
  @@sorter = "AWS001"
  @@port1 = "P1"
  @@port2 = "P2"
  @@port3 = "P3"
  @@port4 = "P4"
  
  @@stocker = "STO160"
  
  @@opnos = "1000.7000 1000.8000"
  

  def setup
    super
    $setup_ok = ($setup_ok != false)
    assert $setup_ok, "test setup is not correct"
  end

  def test01_carriers
    # clean up carriers
    li = $sv.lot_info(@@lot1)
    $setup_ok &= Test_AWS_Stress.reset_carrier(li.carrier)
    $setup_ok &= Test_AWS_Stress.remove_carrier(li.carrier, @@stocker)
    assert $setup_ok, "carrier #{li.carrier} needs to be recovered"
  end
  
  def test02_eqp_mode
    # try to set the port mode
    assert_equal 0, $sv.eqp_mode_change(@@sorter, "Auto-2"), "error in EI comm for #{@@sorter}"
    # check
    eqpi = $sv.eqp_info(@@sorter)
    eqpi.ports.each {|p| $setup_ok &= p.mode == "Auto-2"}
  end

  def test10_route1
    lot = @@lot1
    $sv.lot_opelocate lot, "1000.7000" 
    li = $sv.lot_info(@@lot1)
    assert $sv.auto_process_carriers(@@sorter, @@port1, li.carrier)
    cj = $sv.slr @@sorter, :port=>@@port1, :lot=>@@lot1
    assert_not_nil cj
  end
  
  def self.reset_carrier(carriers)
    # set carriers AVAILABLE and remove all lot holds and NPW reservations on the sorter
    # return true on success
    carriers = carriers.split if carriers.kind_of?(String)
    ret = true
    carriers.each {|carrier|
      $log.info "reset carrier #{carrier.inspect}"
      lot = $sv.lot_list_incassette(carrier)[0]
      res = $sv.lot_hold_release(lot, nil)
      ret &= (res == 0)
      if $sv.carrier_status(carrier).status != "AVAILABLE"
        res = $sv.carrier_status_change(carrier, "AVAILABLE")
        ret &= (res == 0)
      end
      sleep 10
      res = $sv.npw_reserve_cancel(@@sorter, :carrier=>carrier)
      ret &= (res == 0)
    }
    return ret
  end
  
  def self.remove_carrier(carrier, stocker=nil)
    # remove a carrier from a tool after processing
    # return true on success
    ret = true
    $log.info "clean up, waiting for carrier #{carrier} to be removed"
    cs = $sv.carrier_status(carrier)
    eqp = cs.eqp
    ($log.warn "carrier is not at a tool"; return true) if eqp == ""
    eqpi = $sv.eqp_info(eqp)
    port = eqpi.ports.collect {|p| p.port if p.loaded_carrier == carrier}.compact[0]
    ($log.warn "carrier is not loaded at #{eqp}"; return nil) unless port
    # wait for carrier to become ready for unload
    res = $sv.wait_eqp_port_state(eqp, port, "UnloadReq")
    ($log.warn "eqp port #{eqp}:#{port} not in UnloadReq"; return nil) unless res
    # create xfer job, in case AutoProc is not active, ignore any errors
    $sv.carrier_xfer(carrier, eqp, port, stocker, "") if stocker
    # wait for carrier unload
    $sv.wait_eqp_port_state(eqp, port, 'UnloadReq', notequal: true)
  end

  def self.waferid_read(eqp, ports, carriers)
    # return true on successful WaferIDRead
    carriers = carriers.split if carriers.kind_of?(String)
    ports = ports.split if ports.kind_of?(String)
    # move carrier to the sorter
    carriers.each_with_index {|carrier, i|
      res = $sv.npw_reserve(eqp, :carrier=>carrier, :port=>ports[i])
      return nil if res != 0
      res = $sv.carrier_xfer(carrier, "", "", eqp, ports[i])
      return nil if res != 0
    }
    res = $sv.wait_carrier_xfer(carriers)
    return nil unless res
    # wait for carrier to be loaded
    carriers.each_with_index {|carrier, i|
      wait_for {
        pi = eqp_info(eqp).ports.find {|p| p.port == ports[i]}
        pi.state == 'LoadComp' && pi.loaded_carrier == carrier
      } || return
    }
    # allow the sorter to recognize the carrier
    sleep 10
    # WaferIDRead on the sorter
    res = $sv.sorter_action_waferidread(eqp, ports, :carrier=>carriers)
    return nil unless res
    # release carrier, will be moved by AutoProc
    ports.each {|port|
      res = $sv.eqp_port_status_change(eqp, port, "UnloadReq")
      return nil if res != 0
    }
    ret = true
    ports.each {|port|
      ret &= $sv.wait_eqp_port_state(eqp, port, 'UnloadReq', notequal: true)
    }
    return ret
  end
  
end
