=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.


Author: ssteidte, 2015-07-22

History:
=end

require 'SiViewTestCase'


# Tests and test preparationfor GlobalNaming
class Test_GlobalNaming < SiViewTestCase
  # FAB route
  ##@@layer = 'FAB'
  ##@@bank_start = 'G-WSTB'
  ##@@bank_end = 'G-WEND'
  ##@@technology = '3N5FSOI-A0'
  ##@@productgroup = 'DRESDEN-89F'
  @@product = 'DRESDEN-89FFA4U.XB01'
  @@route = '3N5FSOI-A0F00-QATEST1.P001'
  @@opNo = '9900.1000'
  # BUMP route
  ##@@bump_layer = 'BUMP'
  @@bump_product = 'DRESDEN-89FFA4B.AA99'
  @@bump_route = '000-BUMP-AA-DRESDEN.P001'
  @@bump_startbank = 'G-BSTB'
  @@bump_endbank = 'G-BEND'


  def self.startup
    super(route: @@route, product: @@product, carriers: '%')
  end


  def test00_setup
  end

  def testdev11_stb_bump
    assert lot = $svtest.new_lot
    assert $sv.claim_process_lot(lot, happylot: true)
    assert_equal 0, $sv.lot_opelocate(lot, :last)
    #
    assert_equal 0, $sv.lot_bank_move(lot, @@bump_startbank)
    assert lot2 = $svtest.new_lot(product: @@bump_product, route: @@bump_route, 
      lotGenerationType: 'By Source Lot', srclot: lot, lot: lot)
    assert_equal lot2, lot
  end

  def testdev12_split_stb_bump
    assert lot = $svtest.new_lot
    assert lc = $sv.lot_split(lot, 5)
    [lot, lc].each {|l|
      assert $sv.claim_process_lot(l, happylot: true)
      assert_equal 0, $sv.lot_opelocate(l, :last)
      assert_equal 0, $sv.lot_bank_move(l, @@bump_startbank)
      assert l2 = $svtest.new_lot(product: @@bump_product, route: @@bump_route, 
        lotGenerationType: 'By Source Lot', srclot: l, lot: l)
      assert_equal l2, l
    }
  end
end
