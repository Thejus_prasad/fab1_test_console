=begin 
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.
    
Author: Daniel Steger, 2014-07-29 added EI notification

=end

require 'SiViewTestCase'

# Test MSR 724414 (MM should inform EI/CMMS in case of Port Status Change Up-Down)
class Test540_NotifyPortStatusChange < SiViewTestCase
  @@opNo = "1000.600"
  @@opNoib = "1000.800"
  @@eqp_fb = "UTR001" # need reticle tool
  @@eqp_ib = "UTI004" # need reticle tool
  @@brs = "UTBRSTO110"
  @@brs_port = "P4"
  @@portstate_cei_notify = "ON" #OFF
  @@portstate_cmms_notify = "OFF" #ON
  @@opemodes = "Semi-Start-1 Auto-1 Auto-3"
  @@interbay = "INTER1"
  @@ports = "P2 RP1"
  #
  @@clientnode = ""    #"FC8XA5DESKQ01-dsteger"#""#"CMMS"

  @@dual_cmms = "OFF"    # nil/OFF - is single CMMS or "ON"
  @@cmms_udata = "SAPPM" # or Xsite
  
  
  def test00_setup
    $setup_ok = false
    assert_equal @@portstate_cei_notify, $sv.environment_variable[1]["CS_SP_NOTIFY_EI_ON_DOWNPORTSTATE"], "incorrect server setting"    
    assert_equal @@portstate_cmms_notify, $sv.environment_variable[1]["CS_SP_NOTIFY_CMMS_ON_DOWNPORTSTATE"], "incorrect server setting"
    assert_equal @@dual_cmms, ($sv.environment_variable[1]["CS_SP_DUALCMMS_ENABLE"] or ""), "incorrect value for DUALCMMS"    
    if !["OFF",""].member?(@@dual_cmms)
      # Update udata according setup
      $sm = SiView::SM.new($env)    
      [@@eqp_fb, @@eqp_ib].each {|eqp|
        assert $sm.update_udata(:eqp, eqp, {"CMMS"=>@@cmms_udata}), "error writing UDATA in SM"
        u = $sv.user_data(:eqp, eqp)["CMMS"] || ""   # nil when empty string
        assert_equal @@cmms_udata, u, "UDATA CMMS not correctly set in SM"
      }
      # TODO: include BRS in dual CMMS
    $setup_ok = true
    end
    
    $eis = Dummy::EIs.new("local") unless $eis #and $eis.env == $env
    [@@eqp_fb, @@eqp_ib, @@brs].each {|eqp| $eis.restart_tool(eqp)}          
  end
    
  def test10_eqp_portstate_change
    [false, true].each do |custom|
      [@@eqp_fb, @@eqp_ib].each do |eqp|
        @@opemodes.split.each do |opmode|
          assert_equal 0, $sv.eqp_mode_change(eqp, opmode), "failed to change #{eqp} to #{opmode}"
          @@ports.split.each do |port|
            $eis.set_portstate_verify(eqp, "-", "-", port)
            _port_state_change(eqp, port, "LoadReq", core: custom)
            _port_state_change(eqp, port, "LoadComp", core: custom)
            _port_state_change(eqp, port, "UnloadReq", core: custom)
            _port_state_change(eqp, port, "UnloadComp", core: custom)
            _port_state_change(eqp, port, "LoadReq", core: custom)
            assert_equal "-", $eis.portstate(eqp, port)
            _port_state_change(eqp, port, "Down", core: custom)
            assert_equal "Down", $eis.portstate(eqp, port)
            _port_state_change(eqp, port, "LoadReq", core: custom)
            assert_equal "LoadReq", $eis.portstate(eqp, port)      
            if port.start_with?("R")
              _port_state_change(eqp, port, "-", core: custom)
            else
              #_port_state_change(eqp, port, "Unknown", custom: custom)
            end
            assert_equal "LoadReq", $eis.portstate(eqp, port)
          end
        end
      end
    end
  end
  
  def test20_brs_portstate_change
    b_info = $sv.brs_info(@@brs)
    rport = b_info.reticleports.find {|rp| rp.port == @@brs_port}
    assert rport, "reticle port #{@@brs_port} not found"
    assert_equal 0, $sv.brs_mode_change(@@brs, "On-Line Remote") if b_info.online_mode == "Off-Line"
    [true, false].each do |custom|
      ["Manual", "Auto"].each do |opmode|
        assert_equal 0, $sv.eqp_rsp_port_access(@@brs, @@brs_port, opmode), "failed to change #{@@brs} to #{opmode}" unless rport.mode == opmode
        $eis.set_portstate_verify(@@brs, "-", "-", @@brs_port)
        assert_equal 0, $sv.eqp_rsp_port_status_change(@@brs, @@brs_port, "LoadReq", core: custom, client_node: @@clientnode)
        assert_equal 0, $sv.eqp_rsp_port_status_change(@@brs, @@brs_port, "LoadComp", core: custom, client_node: @@clientnode)
        assert_equal 0, $sv.eqp_rsp_port_status_change(@@brs, @@brs_port, "UnloadReq", core: custom, client_node: @@clientnode)
        assert_equal 0, $sv.eqp_rsp_port_status_change(@@brs, @@brs_port, "UnloadComp", core: custom, client_node: @@clientnode)
        assert_equal 0, $sv.eqp_rsp_port_status_change(@@brs, @@brs_port, "LoadReq", core: custom, client_node: @@clientnode)
        assert_equal "-", $eis.portstate(@@brs, @@brs_port)
        assert_equal 0, $sv.eqp_rsp_port_status_change(@@brs, @@brs_port, "Down", core: custom, client_node: @@clientnode)
        assert_equal "Down", $eis.portstate(@@brs, @@brs_port)
        assert_equal 0, $sv.eqp_rsp_port_status_change(@@brs, @@brs_port, "LoadReq", core: custom, client_node: @@clientnode)
        assert_equal "LoadReq", $eis.portstate(@@brs, @@brs_port)      
        assert_equal 0, $sv.eqp_rsp_port_status_change(@@brs, @@brs_port, "-", core: custom, client_node: @@clientnode)
        assert_equal "LoadReq", $eis.portstate(@@brs, @@brs_port)
      end
      
    end
  end
  
  def test30_cmms_reporting
    pend "need CMMS implementation"
  end
  
  def _port_state_change(eqp, port, state, custom)
    if port.start_with?("R")
      assert_equal 0, $sv.eqp_rsp_port_status_change(eqp, port, state, core: custom, client_node: @@clientnode)
    else
      assert_equal 0, $sv.eqp_port_status_change(eqp, port, state, core: custom, client_node: @@clientnode)
    end
  end
  

end
