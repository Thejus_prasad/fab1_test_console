=begin 
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.
Author: Beate Kochinka    2010-08-13
=end

require "RubyTestCase"
require 'dbsequel'

# Tests Lot Maintenance. Be patient - test will sleep after/before some operations.

class Test_LotMaintenance < RubyTestCase
  
  Description = "Tests Lot Maintenance. Be patient - test will sleep after/before some operations."

  @@product = "UT-PRODUCT000.01"
  @@route = "UTRT001.01"
  @@port = "P1"
  @@sleep_time_before_delete = 180 # seconds - recommended: 3 minutes
  @@sleep_time_after_stb = 60 # seconds - recommended: 1 minute
  @@eqp = "UTI002"

  @@note1 = "Lot Maintenance Test 1"
  @@note2 = "Lot Maintenance Test 2"
  @@note3 = "Lot Maintenance Test 3"
  
  #@@lot = "U09A1.00" # -----> temporary test only, normally set by test

  def test_00_init
    op = "create lot"
    $log.info ">>> " + op
    @@lot = $sv.new_lot_release :product=>@@product, :route=>@@route, :stb=>true, :memo=>@@note1
    assert_not_nil @@lot, op + " failed"
    # wait for jCAP STBMfgWIPAttributes to finish and remove possible holds
    sleep @@sleep_time_after_stb.to_i
    $sv.lot_hold_release(@@lot, nil)
  end
  
  def test_01_setup
    op = "verify that lot exists"
    $log.info ">>> " + op
    assert count_rows("SIVIEW.FHOPEHS")>0, "no rows in history table"
    verify_num_rows "SIVIEW.FRLOT", 1
    # get carrier of lot
    @@carrier = $sv.lot_info(@@lot).carrier
    $log.info "+++ " + op

    op = "operation locate"
    $log.info ">>> " + op
    # remove lot holds, if any
    $sv.lot_hold_release(@@lot, nil)
    $sv.lot_opelocate @@lot, "1000.300"
    $sv.lot_opelocate @@lot, "1000.100", :memo=>@@note2
    $sv.lot_opelocate @@lot, "1000.200"
    $log.info "+++ " + op

    op = "operation note"
    $log.info ">>> " + op
    $sv.lot_openote_register @@lot, "Lot Maintenance", @@note3
    $log.info "+++ " + op

    op = "start lots reservation"
    $log.info ">>> " + op
    @@cj = $sv.slr @@eqp, @@lot
    $log.info "+++ " + op
    op = "start lots reservation cancel"
    #@@cj = "UTI002-20100811-0001" # -----> manual test only
    $log.info ">>> " + op
    $sv.slr_cancel @@eqp, @@cj
    $log.info "+++ " + op

    op = "start lots reservation"
    $log.info ">>> " + op
    @@cj = $sv.slr @@eqp, @@lot
    $log.info "+++ " + op

    op = "load " + @@carrier + " on " + @@eqp + ":" + @@port
    $log.info ">>> " + op
    rc = $sv.eqp_load(@@eqp, @@port, @@carrier)
    assert_equal(0, rc, "failed: " + op)
    $log.info "+++ " + op
    
    op = "operation start " + @@carrier + " on " + @@eqp + ":" + @@port
    $log.info ">>> " + op
    @@cj = $sv.eqp_opestart(@@eqp, @@carrier)
    assert_not_nil(@@cj, "failed: " + op)
    $log.info "+++ " + op

    op = "operation complete " + @@cj
    $log.info ">>> " + op
    rc = $sv.eqp_opecomp(@@eqp, @@cj)
    assert_not_nil(@@cj, "failed: " + op)
    $log.info "+++ " + op
    
    op = "unload " + @@carrier + " on " + @@eqp + "/" + @@port
    $log.info ">>> " + op
    rc = $sv.eqp_unload(@@eqp, @@port, @@carrier)
    assert_equal(0, rc, "failed: " + op)
    $log.info "+++ " + op
  end
  
  def test_98_lotDelete
    op = "scrap wafers"
    $log.info ">>> " + op
    rc = $sv.scrap_wafers @@lot
    assert_equal 0, rc, op + " failed"
    $log.info "+++ " + op

    op = "remove lot from carrier"
    $log.info ">>> " + op
    rc = $sv.wafer_sort_req @@lot, ""
    assert_equal 0, rc, op + " failed"
    $log.info "+++ " + op

    $log.info "waiting #{@@sleep_time_before_delete} s before deleting lot"
    sleep @@sleep_time_before_delete.to_i
    # get number of rows in history table
    @@num_ops = count_rows "SIVIEW.FHOPEHS"
    # lot tables contain data for lot
    assert count_rows("SIVIEW.FRLOT")>0, "no rows in table " + "SIVIEW.FRLOT"
    assert count_rows("SIVIEW.FRLOTOPENOTE")>0, "no rows in table " + "SIVIEW.FRLOTOPENOTE"
#    assert count_rows("SIVIEW.FRLOTNOTE")>0, "no rows in table " + "SIVIEW.FRLOTNOTE" # not set
    assert count_rows("SIVIEW.FRLOTCOMMENT")>0, "no rows in table " + "SIVIEW.FRLOTCOMMENT"
    # actually delete lot
    op = "delete lot"
    $log.info ">>> " + op
    rc = $sv.lot_delete @@lot
    assert_equal 0, rc, op + " failed"
    # rows in history table remain
    verify_num_rows "SIVIEW.FHOPEHS", @@num_ops
    # rows in lot tables are deleted
    verify_num_rows "SIVIEW.FRLOT", 0
    verify_num_rows "SIVIEW.FRLOTOPENOTE", 0
    verify_num_rows "SIVIEW.FRLOTNOTE", 0
    verify_num_rows "SIVIEW.FRLOTCOMMENT", 0
    $log.info "+++ " + op
  end
  
  def verify_num_rows(table, expected)
    # verify that table has expected number of rows for lot
    assert_equal expected, count_rows(table), "incorrect number of rows for lot " + @@lot + " in " + table
    $log.debug "(ok)"
  end
  
  def count_rows(table)
    # count rows in table for lot
    num = @@db.select_all("select count(*) from " + table + " where LOT_ID = '" + @@lot + "'")[0][0]
    $log.debug table + " has " + num.to_s + " rows for lot " + @@lot
    return num
  end
  
  def setup
    $log.info ">>> setup"
    $env="itdc"
    @@db = DB.new("siviewmm.#{$env}")
    $log.info "<<< setup"
  end
end
