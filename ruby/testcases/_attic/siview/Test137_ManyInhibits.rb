=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: Steffen Steidten, 2015-07-06

History:
  2015-10-07 sfrieske,  added test for inhibit_list performance
=end

require 'SiViewTestCase'
require 'charts'


# Test SLR, OpeStart and OpeComp can be called if many eqp/chamber inhibits are registered
#
# Performance test for TxEntityInhibitListInq (MSR995784)
class Test137_ManyInhibits < SiViewTestCase
  @@eqp = 'UTC001'
  @@opNo = '1000.700'
  @@numbers = [1, 2, 5, 10, 20, 50, 100, 200, 500, 1000, 2000, 3000]


  def self.shutdown
    $sv.eqp_cleanup(@@eqp)
    $sv.delete_lot_family(@@lot) if self.class_variable_defined?(:@@lot)
  end
  

  # def startup
  #   teardown
  # end

  # def teardown
  #   $sv.inhibit_cancel(:eqp_chamber_recipe, @@eqp)
  #   $sv.inhibit_cancel(:eqp_recipe, @@eqp)
  # end


  def test00_setup
    # get a list of machine recipes
    assert @@mrecipes = $sv.machine_recipes
    assert @@mrecipes.size > @@numbers.last, "need #{@@numbers.last} recipes, got #{@@mrecipes.size}"
  end
  

  def test11_slr  # http://gfjira/browse/MES-1961
    assert_equal 0, $sv.eqp_cleanup(@@eqp)
    @@chamber = $sv.eqp_info(@@eqp).chambers.first.chamber
    # create test lot
    assert @@lot = $svtest.new_lot, "error creating test lot"
    # process with no inhibits
    assert $sv.claim_process_lot(@@lot, eqp: @@eqp), "error processing lot #{@@lot} on #{@@eqp}"
    # exclude the current mrecipe
    assert_equal 0, $sv.lot_opelocate(@@lot, @@opNo)
    assert wn = $sv.what_next(@@eqp).find {|e| e.lot == @@lot}
    @@mrecipes.delete(wn.mrecipe)
    # run the test
    nold = 0
    @@numbers.each {|n|
      $log.info "\n\n-- testing with #{n} inhibits"
      @@mrecipes[nold...n].each {|mrecipe| 
        assert_equal 0, $sv.inhibit_entity(:eqp_chamber_recipe, [@@eqp, mrecipe], attrib: [@@chamber])
      }
      assert_equal 0, $sv.lot_opelocate(@@lot, @@opNo)
      assert $sv.claim_process_lot(@@lot, eqp: @@eqp)
      nold = n
    }
  end

  def test12_inhibit_list
    assert_equal 0, $sv.eqp_cleanup(@@eqp)
    nold = 0
    @@result = @@numbers.collect {|n|
      $log.info "\n\n-- testing with #{n} inhibits"
      @@mrecipes[nold...n].each {|mrecipe| 
        assert_equal 0, $sv.inhibit_entity(:eqp_recipe, [@@eqp, mrecipe])
      }
      nold = n
      tstart = Time.now
      assert $sv.inhibit_list(:eqp_recipe, @@eqp, raw: true)
      [n, (Time.now - tstart) * 1000]
    }
    fname = "log/inhibitlist_#{$env}_#{Time.now.utc.iso8601.gsub(':', '-')}.png"
    $log.info "creating chart #{fname}"
    ch = Chart::XY.new(@@result)
    ch.create_chart(title: "Inhibit List ", x: 'n inhibits', y: 't (ms)', dotsize: 6, export: fname)
    # not working in C7.1.0
    #assert @@result[-1][1] < @@result[-2][1] * 1.2, "inhibit_list scales with the number of inhibits"
  end

end