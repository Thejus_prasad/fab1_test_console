=begin 
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.
    
Author: Daniel Steger

Version: 1.0.0

=end

require 'SiViewTestCase'


# Q-time related tests including MSR830330, MSR836284
# http://myteamsgf/sites/Fab8CIM/CIMEng/FAB8CIMQA/Test%20Plans/SiView/SiView%20QTime%20C6.21.0.xlsm
class Test602_InQTimeChecks < SiViewTestCase
  @@product = 'UT-PROD-QTIME.06'
  @@product_active = 'UT-PROD-QTIME.07'
  @@eqp = 'UTC003'
  
  @@c1_opes = ['1000.1200', '1000.1300', '2000.1500', '3000.1000']
  @@c2_opes = ['1000.1300', '2000.1500', '2000.2500']
  @@cc_opes = ['1000.1100', '2000.1500', '2100.1000']
  @@cc_product = 'UT-PROD-QTIME.03'
  @@cc_route = 'UTRT-QTIME.03'
  
  @@pre_post_ope = '2000.2500'
  @@noqtime_post_ope = '2000.1500'
  @@mainpd_active = 'UTRT-QTIME.07'
  
  @@duration_ope_po = { 
    5 => ['3000.1300', 'TC02 - Duration 60min'], 
    120 => ['3000.1200', 'TC05 - Other Sublottype Hold'],
    200 => ['3000.1100', '']}
  
  @@duration_ope_eo = { 
    5 => ['3000.1300', 'TC02 - Duration 60min'], 
    120 => ['3000.1200', 'TC03 - Engineering Sublottype Hold'],
    200 => ['3000.1100', 'TC04 - Engineering Sublottype Hold and Duration']}
    
  @@holdtypes = ['LotHold', 'FutureHold', 'MergeHold', 'FPCHold', 'PlannedSplitAndMergeHold', 'ProcessHold', 'NonProBankHold']
  @@other_holdtype = 'RunningHold'
  
  @@first_trigger = '1000.1100' # first QTime trigger operation
  

  def self.after_all_passed
    [@@lot, @@lot_active].each {|lot| $sv.delete_lot_family(lot)}
  end
  
  
  def test00_setup
    $setup_ok = false
    #
    #assert_match /EO/, $sv.environment_variable[1]["CS_SP_ENGINEERING_SUBLOTTYPES"], "engineering sublottype not set"
    assert @@lot = $svtest.new_lot(product: @@product), "failed to prepare lot"
    assert @@lot_active = $svtest.new_lot(product: @@product_active), "failed to prepare lot"
    #
    $setup_ok = true
  end
  
  def test01_c1_holdtype_check
    [nil, :locate, :expired].each do |action|
      prepare_lot(action)
      @@c1_opes.each do |ope|
        @@holdtypes.each do |holdtype|
          $log.info " checking at #{ope} with #{holdtype}"
          rec = $sv.lot_operation_in_qtime(@@lot, ope, hold_type: holdtype, details: true)
          assert verify_qtime_details(rec, "C1", "PreventHold", "TC01 - C1 criticality - #{holdtype}", 4), "wrong qtime check result for #{holdtype}"
        end
        rec = $sv.lot_operation_in_qtime(@@lot, ope, hold_type: @@other_holdtype, details: true)
        if @@c2_opes.member?(ope)        
          assert verify_qtime_details(rec, "C2", "Warning", "TC01 - C2 criticality", 3), "wrong qtime check result for #{@@other_holdtype}"
        else # default warning
          assert verify_qtime_details(rec, nil, "Warning", "", 4), "wrong qtime check result for #{@@other_holdtype} and default config"
        end
      end
    end
  end
  
  def test02_c2_holdtype_check
    #lot at first operation
    prepare_lot
    @@c2_opes.each do |ope|
      @@holdtypes.each do |holdtype|
        $log.info " checking at #{ope} with #{holdtype}"
        rec = $sv.lot_operation_in_qtime(@@lot, ope, hold_type: holdtype, details: true)
        if @@c1_opes.member?(ope)
          assert verify_qtime_details(rec, "C1", "PreventHold", "TC01 - C1 criticality - #{holdtype}", 4), "wrong qtime check result for #{holdtype}"
        else
          assert verify_qtime_details(rec, "C2", "Warning", "TC01 - C2 criticality", 3), "wrong qtime check result for #{holdtype}"
        end
      end
      rec = $sv.lot_operation_in_qtime(@@lot, ope, hold_type: @@other_holdtype, details: true)
      assert verify_qtime_details(rec, "C2", "Warning", "TC01 - C2 criticality", 3), "wrong qtime check result for #{@@other_holdtype}"      
    end
  end
  
  def test03_duration_check
    #lot at first operation
    prepare_lot
    # PO Lot
    assert_ok $sv.lot_mfgorder_change(@@lot, sublottype: "PO"), "failed to change sublottype"
    @@duration_ope_po.each_pair do |duration, (ope,desc)|
      rec = $sv.lot_operation_in_qtime(@@lot, ope, hold_type: "LotHold", details: true)
      assert verify_qtime_details(rec, nil, "Warning", desc, duration), "wrong qtime check result for #{ope}"
    end
  end
  
  def test04_backward
    prepare_lot
    assert_ok $sv.lot_opelocate(@@lot, "last")
    @@c1_opes.each do |ope|      
      holdtype = "LotHold"
      $log.info " checking at #{ope} with #{holdtype}"
      rec = $sv.lot_operation_in_qtime(@@lot, ope, ope_info: "LocateBackward", hold_type: holdtype, details: true)
      assert verify_qtime_details(rec, "C1", "PreventHold", "TC01 - C1 criticality - #{holdtype}", 4), "wrong qtime check result for #{holdtype}"      
    end
  end
  
  def test05_pre_post_futurehold
    #lot at first operation
    assert_ok $sv.lot_opelocate(@@lot, "first")
    rec = $sv.lot_operation_in_qtime(@@lot, @@pre_post_ope, hold_type: "FutureHold", ope_info: "PRE", details: true)
    assert verify_qtime_details(rec, "C2", "Warning", "TC01 - C2 criticality", 3), "wrong qtime check result at #{@@pre_post_ope} (PRE)"
    rec = $sv.lot_operation_in_qtime(@@lot, @@pre_post_ope, hold_type: "FutureHold", ope_info: "POST", details: true)
    assert verify_qtime_details(rec, "C1", "PreventHold", "TC01 - C1 criticality - FutureHold", 4), "wrong qtime check result at #{@@pre_post_ope} (POST)"
    assert !$sv.lot_operation_in_qtime(@@lot, @@noqtime_post_ope, hold_type: "FutureHold", ope_info: "POST"), "no qtime expected"    
  end

  
  def test06_engineering_check
    #lot at first operation
    prepare_lot
    # EO Lot
    assert_ok $sv.lot_mfgorder_change(@@lot, sublottype: "EO"), "failed to change sublottype"
    @@duration_ope_eo.each_pair do |duration, (ope,desc)|
      rec = $sv.lot_operation_in_qtime(@@lot, ope, hold_type: "LotHold", details: true)
      assert verify_qtime_details(rec, nil, "Warning", desc, duration), "wrong qtime check result for #{ope}"
    end
  end
  
  def test07_product_tech_check
    @@cc_opes.each do |ope|
      holdtype = "ProcessHold"
      $log.info " checking at #{ope} with #{holdtype}"
      rec = $sv.lot_operation_in_qtime("", ope, route: @@cc_route, hold_type: holdtype, details: true)
      assert verify_qtime_details(rec, "C1", "PreventHold", "TC01 - C1 criticality - #{holdtype}", 5), "wrong qtime check result for #{holdtype} - route"
      rec = $sv.lot_operation_in_qtime("", ope, route: @@cc_route, product: @@cc_product, hold_type: holdtype, details: true)
      assert verify_qtime_details(rec, "CORRECT", "Warning", "", 5), "wrong qtime check result for #{holdtype} - route + product"
    end
  end
  
  def test08_active_routes
    assert_equal 0, $sv.lot_cleanup(@@lot_active, opNo: :first)
    assert objinfo = $sm.object_info(:mainpd, @@mainpd_active).first
    assert module_hash = objinfo.specific[:modules].find {|m| m.name.end_with?("##")}, "module with active versioning not found"
    modulepd, version = module_hash.name.split('.')
    # activate version 01 if not active already
    verify_active_module("#{modulepd}.01", "C2", "Warning", "TC01 - C2 criticality")
    verify_active_module("#{modulepd}.02", "C1", "PreventHold", "TC01 - C1 criticality - LotHold")
  end
  

  def prepare_lot(qtime=nil)
    if qtime == :expired
      # move lot to C1 and wait for expired
      assert_equal 0, $sv.lot_cleanup(@@lot, opNo: @@first_trigger)
      assert $sv.claim_process_lot(@@lot, eqp: @@eqp)
      $log.info "waiting up to 4min for the qtime to expire"
      assert wait_for(timeout: 4*65) {$sv.lot_info(@@lot).status == 'ONHOLD'}, "lot should have qtime hold"
    elsif qtime == :locate
      # move lot to C1
      assert_equal 0, $sv.lot_cleanup(@@lot, opNo: @@c1_opes.first)
      assert !$sv.lot_info(@@lot).qtime, "qtime should not be active"
    else
      #lot at first operation
      assert_equal 0, $sv.lot_cleanup(@@lot, opNo: :first)
      if $sv.lot_qtime_list(@@lot).size > 0
        assert $sv.claim_process_lot(@@lot, eqp: @@eqp)
      end
    end    
  end
  
  def verify_active_module(module_active, criticality, action, description)    
    assert objinfo = $sm.object_info(:modulepd, module_active).first
    unless objinfo.active
      assert $sm.reserve_active(:modulepd, module_active), "failed to activate #{module_active}"
      assert $sm.release_objects(:modulepd, module_active), "failed to release #{module_active}"
    end
    holdtype = 'LotHold'
    rec = $sv.lot_operation_in_qtime(@@lot_active, '1000.1200', hold_type: holdtype, details: true)
    assert verify_qtime_details(rec, "C2", "Warning", "TC01 - C2 criticality", 5), "wrong qtime check result for #{holdtype} - route"
    rec = $sv.lot_operation_in_qtime(@@lot_active, '2000.1200', hold_type: holdtype, details: true)
    assert verify_qtime_details(rec, criticality, action, description, 5), "wrong qtime check result for #{holdtype} - route"
  end
  
  def verify_qtime_details(qtime_rec, criticality, action, description, duration)
    res  = true
    (res &= verify_equal criticality, qtime_rec.criticality, " unexpected criticality reported") if criticality
    res &= verify_equal action, qtime_rec.action, "  unexpected qtime action"
    res &= verify_equal description, qtime_rec.description, "  unexpected description"
    res &= verify_equal duration, qtime_rec.exp_duration, "  unexpected duration"
    return res
  end
  
end
