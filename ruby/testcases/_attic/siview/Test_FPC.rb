=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author:  Daniel Steger, 2013-08-06
Version: 1.0.1

=end

require "RubyTestCase"
require "dummytools"
require "siviewmmdb"

# Test flexible process control

# Excel sheet "http://myteamsgf/sites/Fab8CIM/CIMEng/FAB8CIMQA/Test%20Plans/SiView/SiView%20FPC.xlsm"
class Test_FPC < RubyTestCase
  Description = "Test Flexible Process Condition for experiments"

  @@lot          = "UXYL32044.000"#8XYK32006.000" #"U17LU.00"
  @@product      = "UT-PROD-FPC.01"
  @@route        = "UTRT-FPC.01"
  @@fpc_pre      = "1010.1000" # normal process
  @@fpc_op1      = "1010.1100" # equipment
  @@fpc_op2      = "1010.1200" # metrology
  @@fpc_op3      = "1010.1300" # reticle
  @@fpc_op4      = "1010.1400" # no white def
  @@fpc_psm_op   = "e1000.1100" # fpc on PSM
  @@fpc_psm_op2   = "e1100.1100" # fpc on PSM branch
  @@fpc_eqpi     = "UTI004"
  @@fpc_eqpp     = "UTR001"
  @@fpc_eqpm     = "UTF001"
  @@fpc_mrecipe  = "P.UTMR000.01"
  @@hold_reason  = "ENG"
  @@reticles     = "UTFPC%"

  @@branch_route = "UTBR-FPC1.01"
  @@psm_split_op = "1010.1000"
  @@psm_return_op = "1010.1200"
  #
  @@branch_route2 = "UTBR-FPC2.01"
  @@psm_split_op2 = "e1000.1100"
  @@psm_return_op2 = "e1000.1400"

  @@cfm_lot     = "UXYL32056.000"
  @@cfm_product = "UT-PROD-FPC.02"
  @@cfm_fpc_nop = "1010.1100"
  @@cfm_fpc_op  = "1010.1200"


  def test00_setup
    $setup_ok = false
    $eis = Dummy::EIs.new($env) unless $eis and $eis.env == $env
    $mmdb = SiView::MMDB.new($env) unless $mmdb and $mmdb.env == $env
    # ensure lot exists
    @@lot = $sv.new_lot_release(:stb=>true, :route=>@@route, :product=>@@product, :waferids=>"t7m12") if @@lot == ""
    assert @@lot, "error creating test lot"
    assert $sv.fpc_process_list_inroute(@@lot, raw: true).FPCAvailableFlag, "FPC should be available for lot #{@@lot}"
    # ensure lot is in a carrier
    assert_not_equal "", $sv.lot_info(@@lot).carrier, "lot #{@@lot} is not in a carrier"
    assert_equal "1", $sv.environment_variable[1]["SP_FPC_ADAPTATION_FLAG"], "environment not ready"
    [@@fpc_eqpi, @@fpc_eqpp, @@fpc_eqpm].each do |eqp|
      $sv.eqp_mode_change(eqp, 'Off-Line-1')
      assert_equal 0, $sv.eqp_cleanup(eqp), "failed to cleanup #{eqp}" # TODO: also test in different operation mode i.e. Auto-3
      $eis.restart_tool(eqp)
    end
    $setup_ok = true
  end

  def cleanup_fpc(lot=@@lot)
    route_info = $sv.fpc_process_list_inroute(lot)
    route_info.each do |fpc|
      if fpc.fpc_avail
        $sv.fpc_delete(lot: lot, route: fpc.route, opNo: fpc.op_no)
      end
      # cleanup sub routes
      fpc.sub_route.each do |subroute|
        subroute.each do |sfpc|
          if sfpc.fpc_avail
            $sv.fpc_delete(lot: lot, route: sfpc.route, opNo: sfpc.op_no, org_route: fpc.route, org_opNo: fpc.op_no)
          end
          sfpc.sub_route.each do |sub2route|
            sub2route.select {|s2ope| s2ope.fpc_avail }.each do |s2fpc|
              $sv.fpc_delete(lot: lot, route: s2fpc.route, opNo: s2fpc.op_no,
                org_route: fpc.route, org_opNo: fpc.op_no,
                sub_route: sfpc.route, sub_opNo: sfpc.op_no)
            end
          end
        end
      end
    end
  end

  def cleanup_lot(l=@@lot)
    assert_equal 0, $sv.lot_cleanup(l), "failed to cleanup #{l}"
    assert $sv.merge_lot_family(l, nosort: true), "failed to cleanup lot"
  end

  def test01_no_fpc_setup_for_white_def
    cleanup_lot
    cleanup_fpc
    $sv.lot_opelocate(@@lot, @@fpc_op1)
    assert_nil $sv.claim_process_lot(@@lot, slr: true, claim_carrier: true), "process not allowed at FPC white ope"
    assert_equal "640", $sv._res.strResult.returnCode
  end

  # TODO: additional privileges to Add/Update/Delete FPC
  def test02_cfm_on_production_route
    @@cfm_lot = $sv.new_lot_release(:stb=>true, :product=>@@cfm_product) if @@cfm_lot == ""
    assert @@cfm_lot, "error creating test lot for CFM"
    assert !$sv.fpc_process_list_inroute(@@cfm_lot, raw: true).FPCAvailableFlag, "FPC should not be available for lot #{@@cfm_lot}"

    cleanup_lot(@@cfm_lot)
    cleanup_fpc(@@cfm_lot)

    no_cfm_sv = SiView::MM.new("#{$env}_gms") ## none privileged user

    pdid = $sv.lot_route_lookahead(@@cfm_lot).find {|op| op.opNo == @@cfm_fpc_nop}.op
    cfm_op = $sv.user_data_pd(pdid)["CFM_Operation"]
    assert cfm_op != "Yes", "#{cfm_op} should have no CFM privilege set"
    # CFM user cannot setup FPC at non CFM PD
    assert_nil $sv.fpc_update(@@cfm_lot, @@cfm_fpc_nop, wafers: 2, eqp: @@fpc_eqpp, mrecipe: @@fpc_mrecipe), "FPC should not be allowed at #{@@cfm_fpc_nop}"
    # none CFM user cannot setup FPC at non CFM PD
    assert_nil no_cfm_sv.fpc_update(@@cfm_lot, @@cfm_fpc_nop, wafers: 2, eqp: @@fpc_eqpp, mrecipe: @@fpc_mrecipe), "FPC should not be allowed at #{@@cfm_fpc_nop}"

    pdid = $sv.lot_route_lookahead(@@cfm_lot).find {|op| op.opNo == @@cfm_fpc_op}.op
    cfm_op = $sv.user_data_pd(pdid)["CFM_Operation"]
    assert_equal "Yes", cfm_op, "#{cfm_op} should have CFM privilege set"
    # none CFM user is not allowed to add FPC to CFM PD
    assert_nil no_cfm_sv.fpc_update(@@cfm_lot, @@cfm_fpc_op, wafers: 2, eqp: @@fpc_eqpp, mrecipe: @@fpc_mrecipe)
    # CFM user is allowed to add FPC to CFM PD
    fpc_id = $sv.fpc_update(@@cfm_lot, @@cfm_fpc_op, wafers: 2, eqp: @@fpc_eqpp, mrecipe: @@fpc_mrecipe)
    assert fpc_id, "Failed to setup FPC"

    # none CFM user is not allowed to update FPC to CFM PD
    assert_nil no_cfm_sv.fpc_update(@@cfm_lot, @@cfm_fpc_op, wafers: 4, eqp: @@fpc_eqpp, mrecipe: @@fpc_mrecipe, fpc_id: fpc_id.first, action_type: "Update")

    # CFM user is allowed to update FPC to CFM PD
    fpc_id = $sv.fpc_update(@@cfm_lot, @@cfm_fpc_op, wafers: 4, eqp: @@fpc_eqpp, mrecipe: @@fpc_mrecipe, fpc_id: fpc_id.first, action_type: "Update")
    assert fpc_id, "Failed to setup FPC"

    # none CFM user cannot delete FPC
    assert_equal 638, no_cfm_sv.fpc_delete(fpc_ids: fpc_id), "user is not allowed to delete FPC"

    # CFM user can delete FPC
    assert_equal 0, $sv.fpc_delete(fpc_ids: fpc_id), "CFM user should be able to delete FPC"

    fpc_id = fpc_id = $sv.fpc_update(@@cfm_lot, @@cfm_fpc_op, wafers: 2, eqp: @@fpc_eqpp, mrecipe: @@fpc_mrecipe)
    #
    assert_equal 0, $sv.lot_futurehold(@@cfm_lot, @@cfm_fpc_op, @@hold_reason), "failed to register futurehold"
    assert $sv.lot_opelocate(@@cfm_lot, @@cfm_fpc_op), "failed to locate to fpc operation"

    assert_equal({}, $sv.user_privileges(no_cfm_sv.user, "MM", "HOLDS"), "user #{no_cfm_sv.user} should not release privileged holds")
    assert_equal 0, no_cfm_sv.lot_hold_release(@@cfm_lot, @@hold_reason), "failed to release hold"
    #
    check_fpc_process(fpc_id, @@cfm_lot, @@fpc_eqpp, true)
  end




  def test10_locate_fpc_lot
    fpc_id = setup_fpc(@@fpc_op1, @@fpc_op1)
    #
    check_fpc_process(fpc_id, @@lot, @@fpc_eqpp, false)
  end


  def test11_gatepass_fpc_lot
    fpc_id = setup_fpc(@@fpc_op1, @@fpc_pre)
    #
    assert_equal 0, $sv.lot_gatepass(@@lot), "failed to gatepass"
    #
    check_fpc_process(fpc_id, @@lot, @@fpc_eqpp, false)
  end

  def test12_opecomp_fpc_lot
    fpc_id = setup_fpc(@@fpc_op1, @@fpc_pre)
    #
    assert $sv.claim_process_lot(@@lot), "failed to operate lot"
    #
    check_fpc_process(fpc_id, @@lot, @@fpc_eqpp, false)
  end

  def test13_lotrelease_fpc_lot
    fpc_id = setup_fpc(@@fpc_op1, @@fpc_pre)
    #
    op = $sv.lot_route_lookahead(@@lot).find {|op| op.opNo == @@fpc_op1}.op

    ##TODO: not activated assert_equal [@@fpc_eqpp], $sv.eqp_list_by_ope(@@product, op)
    assert_equal 0, $sv.lot_futurehold(@@lot, @@fpc_op1, @@hold_reason), "failed to register futurehold"
    assert $sv.lot_opelocate(@@lot, @@fpc_op1), "failed to locate to fpc operation"
    assert_equal 0, $sv.lot_hold_release(@@lot, @@hold_reason), "failed to release hold"
    #
    check_fpc_process(fpc_id, @@lot, @@fpc_eqpp, false)
  end

  def test15_metro_fpc
    fpc_id = setup_fpc(@@fpc_op2, @@fpc_op2, eqp: @@fpc_eqpm)
    check_fpc_process(fpc_id, @@lot, @@fpc_eqpm, false)
  end


  def test20_locate_fpc_wafer
    fpc_id = setup_fpc(@@fpc_op1, @@fpc_op1, :wafers=>2)
    #
    check_fpc_process(fpc_id, @@lot, @@fpc_eqpp, true)
  end

  def test21_gatepass_fpc_wafer
    fpc_id = setup_fpc(@@fpc_op1, @@fpc_pre, :wafers=>2)
    #
    assert_equal 0, $sv.lot_gatepass(@@lot), "failed to gatepass"
    #
    check_fpc_process(fpc_id, @@lot, @@fpc_eqpp, true)
  end

  def test22_opecomp_fpc_wafer
    fpc_id = setup_fpc(@@fpc_op1, @@fpc_pre, :wafers=>2)
    #
    assert $sv.claim_process_lot(@@lot), "failed to operate lot"
    #
    check_fpc_process(fpc_id, @@lot, @@fpc_eqpp, true)
  end

  # MSR 772659
  def test23_lotrelease_fpc_wafer
    fpc_id = setup_fpc(@@fpc_op1, @@fpc_pre, :wafers=>2)
    #
    assert_equal 0, $sv.lot_futurehold(@@lot, @@fpc_op1, @@hold_reason), "failed to register futurehold"
    assert $sv.lot_opelocate(@@lot, @@fpc_op1), "failed to locate to fpc operation"
    sv = SiView::MM.new("#{$env}_gms") ## none privileged user
    assert_equal({}, $sv.user_privileges(sv.user, "MM", "HOLDS"), "user #{sv.user} should not release privileged holds")
    assert_equal 0, sv.lot_hold_release(@@lot, @@hold_reason), "failed to release hold"
    #
    check_fpc_process(fpc_id, @@lot, @@fpc_eqpp, true)
  end

  # TODO: include MSR830564 / Reticle Group tx
  # TODO: double exposure
  def test30_reticle_fpc
    cleanup_lot
    cleanup_fpc

    reticles = $sv.reticle_list(reticle: @@reticles, reticleinfo: true)
    assert_equal 25, reticles.count, "not enough FPC reticles"
    reticles.each {|r|
      $sv.reticle_status_change(r.reticle, "AVAILABLE") unless r.status == "AVAILABLE"
      $sv.reticle_xfer_status_change(r.reticle, "EO", :eqp=>@@fpc_eqpp) unless r.xfer_status == "EO"
    }
    # helper to identify lots later
    wafer_reticle = {}
    li_info = $sv.lot_info(@@lot, :wafers=>true)
    li_info.wafers.each_with_index do |w,i|
      reticle = reticles[i]
      fpc_id = $sv.fpc_update(@@lot, @@fpc_op3, eqp: @@fpc_eqpp, restrict_eqp: false, mrecipe: @@fpc_mrecipe,
        reticle: reticle.reticle, wafers: [w.alias])
      assert fpc_id, "failed to setup FPC"
      wafer_reticle[w.wafer] = [fpc_id, reticle]
    end
    assert $sv.lot_opelocate(@@lot, @@fpc_op3), "failed to locate to setup operation"
    #
    lots = $sv.lot_family(@@lot)
    assert_equal 25, lots.count, "not all lots were split"
    #
    w_next = $sv.what_next(@@fpc_eqpp)
    lots.each {|l|
      # identify lot by wafer id
      li = $sv.lot_info(l, :wafers=>true)
      rg = wafer_reticle[li.wafers[0].wafer][1].rg
      assert li.opEqps.member?(@@fpc_eqpp), "lot not candidate for equipment"
      w_lot = w_next.find {|w| w.lot == l}
      assert w_lot, "lot #{l} not on what next"
      assert_equal rg, w_lot.rgs.first, "wrong reticle group"
      assert_nil $sv.slr(@@fpc_eqpp, :lot=>l), "reservation should fail"
    }
    #load reticles
    reticles = $sv.reticle_list(reticle: @@reticles, reticleinfo: true)
    reticles.each {|r|
      $sv.reticle_status_change(r.reticle, "AVAILABLE") unless r.status == "AVAILABLE"
      $sv.reticle_xfer_status_change(r.reticle, "EI", :eqp=>@@fpc_eqpp) unless r.xfer_status == "EI"
    }
    assert $sv.claim_process_lot(lots, eqp: @@fpc_eqpp, slr: true, mode: "Off-Line-1"), "failed to process lot"
    lots.each do |l|
      # identify lot by wafer id
      li = $sv.lot_info(l, :wafers=>true)
      fpc_id, reticle = wafer_reticle[li.wafers[0].wafer]
      assert verify_fpc_history(fpc_id, l, @@fpc_op3, @@fpc_eqpp, @@fpc_mrecipe, [reticle.reticle]), "wrong lot history"
    end
  end

  def test40_internalbuffer_fpc
    fpc_id = setup_fpc(@@fpc_op1, @@fpc_op1, eqp: @@fpc_eqpi, :wafers=>2)
    check_fpc_process(fpc_id, @@lot, @@fpc_eqpi, true)
  end

  #MSR830813
  def XXXtest50_psm_fpc_split
    # Setup PSM and FPC at first operation of PSM split route
  end

  def test51_psm_fpc_split
    fpc_id = setup_fpc(@@fpc_psm_op, @@psm_split_op, wafers: 2, subroute: @@branch_route)
    #
    child1 = $sv.lot_family(@@lot)[-1]
    assert child1, "failed to execute experiment"

    assert_equal 0, $sv.lot_opelocate(child1, @@fpc_psm_op), #failed to locate to psm - fpc operation
    #
    check_fpc_process(fpc_id, child1, @@fpc_eqpp, false)

    assert_equal 0, $sv.lot_opelocate(@@lot, @@psm_return_op), "failed to locate #{@@lot} to merge operation"
    assert_equal 0, $sv.lot_opelocate(child1, @@psm_return_op), "failed to locate #{child1} to merge operation"
    assert_equal 0, $sv.lot_merge(@@lot, child1), "failed to merge lots"
  end

  def test52_branch_2_branch_fpc
    fpc_id = setup_fpc(@@fpc_psm_op2, @@psm_split_op, wafers: 2, subroute: @@branch_route, sub2route: @@branch_route2)
    #
    child1 = $sv.lot_family(@@lot)[-1]
    assert child1, "failed to execute experiment"

    assert_equal 0, $sv.lot_opelocate(child1, @@psm_split_op2)
    # lot should go to branch2branch route
    assert_equal @@branch_route2, $sv.lot_info(child1).route, "lot expected on branch2branch route"

    assert_equal 0, $sv.lot_opelocate(child1, @@fpc_psm_op2), #failed to locate to psm - fpc operation
    #
    check_fpc_process(fpc_id, child1, @@fpc_eqpp, false)

    assert_equal 0, $sv.lot_opelocate(child1, @@psm_return_op2), "failed to locate #{child1} to return operation"
    assert_equal 0, $sv.lot_opelocate(@@lot, @@psm_return_op), "failed to locate #{@@lot} to merge operation"
    assert_equal 0, $sv.lot_opelocate(child1, @@psm_return_op), "failed to locate #{child1} to merge operation"
    assert_equal 0, $sv.lot_merge(@@lot, child1), "failed to merge lots"
  end

  #MSR830816
  def test60_hold_at_fpc_operation
    fpc_id = setup_fpc(@@fpc_op1, @@fpc_op1, hold: true)
    #
    assert_equal "ONHOLD", $sv.lot_info(@@lot).status, "expected FPC hold state"
    assert_equal 1, $sv.lot_hold_list(@@lot).select {|h| h.reason == "FPCH"}.count, "expected FPC hold"
    ##TODO: releasing the hold before removing the FPC definition will return 0

    assert_equal 0, $sv.fpc_delete(lot: @@lot, route: @@route, opNo: @@fpc_op1), "failed to delete FPC"
    assert_equal 0, $sv.lot_hold_release(@@lot, nil), "failed to release lot"
    assert_equal "Waiting", $sv.lot_info(@@lot).status, "expected FPC hold state"

    sleep 10
    assert_equal [], $mmdb.fpc_execinfo(fpc_id,  @@lot).map {|e| e.action}, "no fpc exec info expected for #{@@lot}"
  end

  def test70_restrict_equipment
    fpc_id = setup_fpc(@@fpc_op4, @@fpc_op4, restrict_eqp: true)
    #
    check_fpc_process(fpc_id, @@lot, @@fpc_eqpp, false, true)
  end

  #MSR869132
  def XXXtest80_skip_fpc_lot
    fpc_id = setup_fpc(@@fpc_op1, @@fpc_op1, skip: true)
    #
    l_info = $sv.lot_info(@@lot)
    assert_equal @@fpc_op3, l_info.opNo, "operation has not been skipped (including Pre-1)"
    assert wait_for(:timeout=>60) {
      # get operation pass count (latest)
      pass_count = $sv.lot_operation_list_fromhistory(@@lot, opNo: @@fpc_op1)[0].pass
      $sv.lot_operation_history(@@lot, opNo: @@fpc_op1, pass: pass_count, pinpoint: true, category: "GatePass").count > 0
    }, "no gate pass in history"
  end

  def test81_skip_fpc_wafers
    _alias = ["24", "25"]
    fpc_id = setup_fpc(@@fpc_op1, @@fpc_op1, wafers: _alias, skip: true)
    child1 = $sv.lot_family(@@lot)[-1]
    #
    l_info = $sv.lot_info(child1, wafers: true)
    ok  = verify_equal @@fpc_op2, l_info.opNo, "operation has not been skipped"
    ok &= verify_equal _alias, l_info.wafers.map {|w| w.alias}, "wrong wafers are splitted"
    ok &= verify_equal @@fpc_op1, $sv.lot_info(@@lot).opNo, "parent is still at FPC operation"
    ok &= wait_for(:timeout=>60) {
      # get operation pass count (latest)
      hist = $sv.lot_operation_history(child1, opNo: l_info.opNo, pinpoint: true, category: "LocateForward")
      next unless hist
      res = verify_equal(1, hist.count, "no gate pass in history")
      res &= verify_equal(["Exec_Split"], $mmdb.fpc_execinfo(fpc_id, @@lot).map {|e| e.action}, "wrong history for #{@@lot}")
      res &= verify_equal(["Exec_Apply", "Exec_Skip"], $mmdb.fpc_execinfo(fpc_id, child1).map {|e| e.action}.sort, "wrong history for #{child1}")
      res
    }
    assert ok
  end

  def test90_recipe_params_bylot
    fpc_id = setup_fpc(@@fpc_op1, @@fpc_op1, eqp: @@fpc_eqpi, rcp_params: {@@lot=>[["QA-TEST", "FPC"]]})
    #
    check_fpc_process(fpc_id, @@lot, @@fpc_eqpi, false, false)
  end

  def test91_recipe_params_bywafer
    _alias = ["01", "02"]
    _wafers = $sv.lot_info(@@lot, wafers: true).wafers.select {|w| _alias.member?(w.alias)}
    _rcp_par = Hash[_wafers.map {|w| [w.wafer,[["QA-TEST", "FPC-#{w.wafer}"]]] }]
    fpc_id = setup_fpc(@@fpc_op1, @@fpc_op1, eqp: @@fpc_eqpi, wafers: _alias, rcp_params: _rcp_par)
    #
    check_fpc_process(fpc_id, @@lot, @@fpc_eqpi, true, false)
  end

  def setup_fpc(fpc_op_no, proc_op_no, params={})
    cleanup_lot
    cleanup_fpc
    eqp = (params[:eqp] or @@fpc_eqpp)
    restrict = (params[:restrict_eqp] or false)

    #
    fpc_id = $sv.fpc_update(@@lot, fpc_op_no, params.merge(eqp: eqp, restrict_eqp: restrict, mrecipe: @@fpc_mrecipe))
    assert_equal 1, fpc_id.count, "failed to setup FPC"

    if params[:subroute] or params[:sub2route]
      assert_equal 0, $sv.lot_experiment(@@lot, params[:wafers], params[:subroute], @@psm_split_op, @@psm_return_op)
      if params[:sub2route]
        assert $sv.lot_experiment(@@lot, params[:wafers], params[:sub2route], @@psm_split_op2, @@psm_return_op2,
          split_route: @@branch_route, original_opNo: @@psm_split_op)
      end
    end

    if params[:skip]
      assert_equal 0, $sv.user_parameter_change("Lot", @@lot, "UTSkip", "YES")
    end
    assert_equal 0, $sv.lot_opelocate(@@lot, proc_op_no), "failed to locate to setup operation"
    return fpc_id[0]
  end

  def check_fpc_process(fpc_id, lot, eqp, splitted, restricted=false)
    if splitted
      child = $sv.lot_family(lot)[-1]
      fpc_lot = child
      assert_equal 0, $sv.lot_gatepass(lot), "failed to gate pass parent"
    else
      fpc_lot = lot
    end
    li = $sv.lot_info(fpc_lot)
    assert li.opEqps.member?(eqp), "lot not candidate for equipment"
    assert_equal 1, li.opEqps.count, "eqp list should be restricted to #{eqp}" if restricted
    assert $sv.what_next(eqp).find {|w| w.lot == fpc_lot}, "lot not found on what next"
    assert $sv.claim_process_lot(fpc_lot, eqp: eqp, slr: true, mode: "Off-Line-1"), "failed to process lot"
    assert verify_fpc_history(fpc_id, fpc_lot, li.opNo, eqp), "wrong lot history"
    if splitted
      # check parent split history
      assert $mmdb.fpc_execinfo(fpc_id, lot).find {|e| e.action == "Exec_Split"}, "no fpc exec info for #{lot} split"
      # merge parent and child
      assert $sv.lot_merge(lot, child), "failed to merge child"
    end
  end


  def verify_fpc_history(fpc_id, lot, op_no, eqp, mrecipe=@@fpc_mrecipe, reticles=[])
    $log.info "#{__method__} #{fpc_id}, #{lot}, #{op_no}, #{eqp}, #{mrecipe}, #{reticles}"
    return wait_for(:timeout=>60) {
      # get operation pass count (latest)
      e = $sv.lot_operation_list_fromhistory(lot, opNo: op_no)
      if e && e.count > 0
        pass_count = e[0].pass
        hist = $sv.lot_operation_history(lot, opNo: op_no, pass: pass_count, pinpoint: true, category: "OperationStart").first
        if hist
          res  = verify_equal mrecipe, hist.mrecipe, "wrong recipe in history"
          res &= verify_equal eqp, hist.lotinfo.eqp, "wrong equipment in history"
          res &= verify_equal reticles, hist.reticles, "wrong reticles"
          hist = $sv.lot_operation_history(lot, opNo: op_no, pass: pass_count, pinpoint: true, category: "OperationComplete").first
          if hist
            res &= verify_equal mrecipe, hist.mrecipe, "wrong recipe in history"
            res &= verify_equal eqp, hist.lotinfo.eqp, "wrong equipment in history"
            res &= verify_equal reticles, hist.reticles, "wrong reticles"

            res &= verify_equal ["Exec_Apply"], $mmdb.fpc_execinfo(fpc_id, lot).map {|e| e.action}, "wrong exec info for #{lot}"
            res
          else
            #$log.info(" no history found")
            false
          end
        else
          #$log.info(" no history found")
          false
        end
      else
        #$log.info(" no history found")
        false
      end
    }
  end
end
