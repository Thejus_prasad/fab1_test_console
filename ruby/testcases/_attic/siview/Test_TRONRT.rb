=begin 
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.


To create a prod-like lot:
  @@lot = "G02G3.000"
  @@wafers = ["PGVQK046MX", "PGVQK045MX", "PGVQK044MX", "PGVQK043MX", "PGVQK042MX", 
    "PGVQK041MX", "PGVQK040MX", "PGVQK039MX", "PGVQK038MX", "PGVQK037MX", 
    "PGVQK036MX", "PGVQK035MX", "PGVQK034MX", "PGVQK033MX", "PGVQK032MX", 
    "PGVQK031MX", "PGVQK030MX", "PGVQK029MX", "PGVQK028MX", "PGVQK027MX", 
    "PGVQK026MX", "PGVQK172MX", "PGVQK171MX", "PGVQK170MX", "PGVQK169MX"]
  @@sublottype = "PR"
  @@customer = "samsung"
  params = {:product=>@@product, :route=>@@route, :lot=>@@lot, :waferids=>@@wafers, :customer=>@@customer, :sublottype=>@@sublottype, :stbdelay=>1, :carrier=>"UT37"}
  $sv.new_lot_release(params)
  
Tools: either LETCL1000 or ETX502 with LAM2300 emulator
    
Author: Steffen Steidten, 2012-08-03
=end

require "RubyTestCase"
require 'dummytools'
require 'toolevents'

class Test_TRONRT < RubyTestCase
  Description = "Test Transfer-On-Route scenario with virtual/real equipment"
  
  # carriers to be used
  @@carriers = ['A6GF', 'ACRR', 'A6X4', 'A658', 'A5HR', 'A6Y2', 'A7E4', 'A3GD', 'A3LC'] #'A3LC', 'A658', 'A6GF', 'A6X4', 'A5HR', 'A6Y2', 'A7E4', 'ACRR', 'A3GD' #'A3LC', 'A658', 'A6Y2', 'A7E4'
  @@vcarriers = ['CSTR01', 'CSTR02', 'CSTR03', 'CSTR04', 'CSTR05', 'CSTR06', 'CSTR07', 'CSTR07', 'CSTR09', 'CSTR10']
  @@stockers1 = ['STO110', 'STO110', 'STO110'] #'STO160', 'STO110', 'STO160'
  @@stockers2 = ['STO110', 'STO160', 'STO110', 'STO160', 'STO110', 'STO160'] #'STO110', 'STO160', 'STO110', 'STO160'
  @@vstockers = ['UTSTO11', 'UTSTO12']
  @@carrierstat = 'NOTAVAILABLE'
  
  @@sorter = 'AWS001'
  @@vsorter = 'AWS800'
  @@origpgs = ['PG1', 'PG1', 'PG2', 'PG2']
  @@pgs = ['PG1', 'PG2', 'PG3', 'PG4']
  @@origmodes = ['Auto-3', 'Auto-3', 'Auto-3', 'Auto-3']
  
  # real test setup
  @@testcarrierstat = 'AVAILABLE'
  @@route = 'UT-TRANSFER.01'
  @@oplotstart = '1000.1000'
  @@optransfer = '1000.2000'
  @@optrtomol = '1000.4000'
  @@optrtofeol = '1000.6000'
  @@product = 'ITDCBB.01'
  
  @@timeout = 14200
  @@sleep = 180
  
  def teardown
    # lot back on original route, product, operation
    # set lot ONHOLD
    # carrier color back to BEOL
    # carrier back to NOTAVAILABLE and manual stockout (STOxxx/MO)
    # 
    #$sv.schdl_change(@@lot1, :route=>@@route1, :product=>@@product1, :opNo=>@@origopNo1)
    #li = $sv.lot_info(@@lot1)
    #assert_equal(@@route1, li.route, "Expected original route: #{@@route1} was #{li.route} instead! PLEASE execute SCHEDULE CHANGE manually again!")
    #$sv.lot_hold(@@lot1, 'FAT', :user=>'fhaase')
    
    #$sv.schdl_change(@@lot2, :route=>@@route2, :product=>@@product2, :opNo=>@@origopNo2)
    #li = $sv.lot_info(@@lot2)
    #assert_equal(@@route2, li.route, "Expected original route: #{@@route2} was #{li.route} instead! PLEASE execute SCHEDULE CHANGE manually again!")
    #$sv.lot_hold(@@lot2, 'FAT', :user=>'fhaase')
    
    #$sm = SiView::SM.new($env)
    
    @@carriers.each {|c|
      cs = $sv.carrier_status(c)
      lot = ''
      assert(cs.lots.size <= 1, "To many lots #{cs.lots} in carrier: #{c}. Expected was a mx. of 1 lot")
      if cs.lots.size == 1
        lot = cs.lots[0]
        $sv.lot_hold(lot, nil, :user=>'cstenzel')
      #  assert_equal(0, $sv.wafer_sort_req(lot, ''))
      #  sleep 1
      #  $sm.change_carriers [c], category: @@carriercat
      #  assert_equal(0, $sv.wafer_sort_req(lot, c))
      end
      $sv.carrier_status_change(c, @@carrierstat)
    }
    
    assert $sv.user_parameter_set_verify('Eqp', @@sorter, 'Pull', "")
    assert $sv.user_parameter_set_verify('Eqp', @@sorter, 'Push', "")
  end

  def cleanup
    eqpi = $sv.eqp_info(@@sorter)
    eqpi.ports.each_with_index {|p,i| 
      assert(p.pg == @@origpgs[i], "PLEASE SET SORTER PORTGROUPS BACK to their ORIGINAL VALUES: #{@@origpgs}")
      assert(p.mode == @@origmodes[i], "PLEASE SET SORTER LP MODES BACK to their ORIGINAL VALUES: #{@@origmodes}")
    }
  end
  
  def test00_switch_portmodes
    modes = ['Auto-2', 'Auto-2', 'Auto-1', 'Auto-1']
    
    eqpi = $sv.eqp_info(@@sorter)
    eqpi.ports.each_with_index {|p,i| 
      $log.info "PLEASE SET SORTER PORTGROUPS to their TRANSFERONROUTE VALUES: #{@@pgs}"
      $log.info "PLEASE SET SORTER LP MODES to their TRANSFERONROUTE VALUES: #{modes}"
    }
    
    $log.info "PLEASE SET SORTER LP MODES. When ready press any key to continue."
		gets
  end

  def test01_transfer_from_BEOL_to_C4
    modes = ['Auto-2', 'Auto-2', 'Auto-1', 'Auto-1']
    startcarriercat = 'BEOL'
    targetcarriercat = 'C4'
    lots = Array.new
    
    eqpi = $sv.eqp_info(@@sorter)
    eqpi.ports.each_with_index {|p,i| 
      assert(p.pg == @@pgs[i], "PLEASE SET SORTER PORTGROUPS to their TRANSFERONROUTE VALUES: #{@@pgs}")
      assert(p.mode == modes[i], "PLEASE SET SORTER LP MODES to their TRANSFERONROUTE VALUES: #{modes}")
    }
    
    stockers = (@@stockers1 + @@stockers1).uniq
    stockers.each {|s| $sv.eqp_status_change_req(s, "2WPR")}
    $sv.eqp_status_change_req(@@sorter, "2WPR")
    
    assert $sv.user_parameter_set_verify('Eqp', @@sorter, 'Pull', "")
    assert $sv.user_parameter_set_verify('Eqp', @@sorter, 'Push', "")
    
    $sm = SiView::SM.new($env)
    
    lotcountnotempty = 0
    @@carriers.each {|c|
      $sv.carrier_status_change(c, @@testcarrierstat)
      cs = $sv.carrier_status(c)
      lot = ''
      assert(cs.lots.size <= 1, "To many lots #{cs.lots} in carrier: #{c}. Expected was a mx. of 1 lot")
      if cs.lots.size == 1
        lot = cs.lots[0]
        #assert_equal(0, $sv.wafer_sort_req(lot, ''))
        sleep 1
        #$sm.change_carriers([c], contents: "Wafer", category: startcarriercat)
        #assert_equal(0, $sv.wafer_sort_req(lot, c))
        
        # TrToBEOL route: UT-TRANSFER.02
        #$sv.schdl_change(lot, :route=>@@route, :product=>@@product, :opNo=>@@oplotstart)
        #li = $sv.lot_info(lot)
        #assert_equal(@@route, li.route, "Expected route: #{@@route} was #{li.route} instead! PLEASE execute SCHEDULE CHANGE manually again!")
        $sv.lot_hold_release(lot, nil)
        lots << lot
        $sv.carrier_xfer(c, "", "", @@stockers1[lots.size - 1], "")
      elsif cs.lots.size == 0
        #$sm.change_carriers([c], contents: "Wafer", category: targetcarriercat)
        $sv.carrier_xfer(c, "", "", @@stockers2[lotcountnotempty], "")
        lotcountnotempty = lotcountnotempty + 1
      end
    }
    
    $log.info "...sleeping 120s...to wait for carrier to stocker transfer finished"
    sleep 120
    
    #turn on PUSH/PULL for Auto-3 Ports
    #assert $sv.user_parameter_set_verify('Eqp', @@sorter, 'Pull', "P1,P2")
    #assert $sv.user_parameter_set_verify('Eqp', @@sorter, 'Push', "P1,P2,P3,P4")
    #$itdc.user_parameter_set_verify('Eqp', 'AWS001', 'Pull', "P1,P2")
    #$itdc.user_parameter_set_verify('Eqp', 'AWS001', 'Push', "P1,P2,P3,P4")
    
    $sv.lot_opelocate(lots[0], @@oplotstart)
    $sv.lot_requeue(lots[0])
    $sv.lot_opelocate(lots[0], @@optransfer)
    $sv.lot_opelocate(lots[1], @@oplotstart)
    $sv.lot_requeue(lots[1])
    $sv.lot_opelocate(lots[1], @@optransfer)
    $sv.lot_opelocate(lots[2], @@oplotstart)
    $sv.lot_requeue(lots[2])
    $sv.lot_opelocate(lots[2], @@optransfer)
    
    #$sv.eqp_status_change_req(@@sorter, "2WPR")
    #sleep 1
    #$sv.eqp_status_change_req(@@sorter, "1PRD")
    #sleep 1
    #$sv.eqp_status_change_req(@@sorter, "2WPR")
    
    #assert wait_for(:timeout=>@@timeout) {$sv.lot_info(lots[1]).opNo == '1000.3000'}, "TransferOnRoute job failed"
    
    $log.info "When test finished press any key to cleanup."
		gets
    
    sleep @@sleep
  end
  
  def xtest02_switch_portmodes
    modes = ['Auto-2', 'Auto-2', 'Auto-1', 'Auto-1']
    
    eqpi = $sv.eqp_info(@@sorter)
    eqpi.ports.each_with_index {|p,i| 
      $log.info "PLEASE SET SORTER PORTGROUPS to their TRANSFERONROUTE VALUES: #{@@pgs}"
      $log.info "PLEASE SET SORTER LP MODES to their TRANSFERONROUTE VALUES: #{modes}"
    }
    
    $log.info "PLEASE SET SORTER LP MODES. When ready press any key to continue."
		gets
  end

  def xtest03_transfer_from_C4_to_BEOL
    modes = ['Auto-2', 'Auto-2', 'Auto-1', 'Auto-1']
    startcarriercat = 'C4'
    targetcarriercat = 'BEOL'
    lots = Array.new
    
    eqpi = $sv.eqp_info(@@sorter)
    eqpi.ports.each_with_index {|p,i| 
      assert(p.pg == @@pgs[i], "PLEASE SET SORTER PORTGROUPS to their TRANSFERONROUTE VALUES: #{@@pgs}")
      assert(p.mode == modes[i], "PLEASE SET SORTER LP MODES to their TRANSFERONROUTE VALUES: #{modes}")
    }
    
    stockers = (@@stockers1 + @@stockers1).uniq
    stockers.each {|s| $sv.eqp_status_change_req(s, "2WPR")}
    $sv.eqp_status_change_req(@@sorter, "2WPR")
    $sm = SiView::SM.new($env)
    
    lotcountnotempty = 0
    @@carriers.each {|c|
      $sv.carrier_status_change(c, @@testcarrierstat)
      cs = $sv.carrier_status(c)
      lot = ''
      assert(cs.lots.size <= 1, "To many lots #{cs.lots} in carrier: #{c}. Expected was a mx. of 1 lot")
      if cs.lots.size == 1
        lot = cs.lots[0]
        #assert_equal(0, $sv.wafer_sort_req(lot, ''))
        sleep 1
        #$sm.change_carriers([c], contents: "Wafer", category: startcarriercat)
        #assert_equal(0, $sv.wafer_sort_req(lot, c))
        
        #$sv.schdl_change(lot, :route=>@@route, :product=>@@product, :opNo=>@@oplotstart)
        #li = $sv.lot_info(lot)
        #assert_equal(@@route, li.route, "Expected route: #{@@route} was #{li.route} instead! PLEASE execute SCHEDULE CHANGE manually again!")
        $sv.lot_hold_release(lot, nil)
        lots << lot
        $sv.carrier_xfer(c, "", "", @@stockers1[lots.size - 1], "")
      elsif cs.lots.size == 0
        #$sm.change_carriers([c], contents: "Wafer", category: targetcarriercat)
        $sv.carrier_xfer(c, "", "", @@stockers2[lotcountnotempty], "")
        lotcountnotempty = lotcountnotempty + 1
      end
    }
    
    $log.info "...sleeping 120s...to wait for carrier to stocker transfer finished"
    sleep 120
    
    #turn on PUSH/PULL for Auto-3 Ports
    assert $sv.user_parameter_set_verify('Eqp', @@sorter, 'Pull', "P1,P2,P3,P4")
    assert $sv.user_parameter_set_verify('Eqp', @@sorter, 'Push', "P1,P2,P3,P4")
    #$itdc.user_parameter_set_verify('Eqp', 'AWS001', 'Pull', "P1,P2,P3,P4")
    #$itdc.user_parameter_set_verify('Eqp', 'AWS001', 'Push', "P1,P2,P3,P4")

    $sv.lot_opelocate(lots[0], @@oplotstart)
    $sv.lot_requeue(lots[0])
    $sv.lot_opelocate(lots[0], @@optransfer)
    $sv.lot_opelocate(lots[1], @@oplotstart)
    $sv.lot_requeue(lots[1])
    $sv.lot_opelocate(lots[1], @@optransfer)
    
    #$sv.eqp_status_change_req(@@sorter, "2WPR")
    #sleep 1
    #$sv.eqp_status_change_req(@@sorter, "1PRD")
    #sleep 1
    #$sv.eqp_status_change_req(@@sorter, "2WPR")
    
    assert wait_for(:timeout=>@@timeout) {$sv.lot_info(lots[0]).opNo == '1000.3000'}, "TransferOnRoute job failed"
    
    sleep @@sleep
  end
  
  def xtest99
  end
end
