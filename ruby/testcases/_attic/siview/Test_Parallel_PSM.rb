=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: Daniel Steger, 2013-01-10
=end

require "RubyTestCase"

# Test Parallel PSMs (MSR672112)
class Test_Parallel_PSM < RubyTestCase
    Description = "Test for parallel PSM updates"

    @@lot = "U10X2.00" # Sandbox "PSM002.00" #
    @@branch_route1 = "eG103-RX-ET-32-A0.01" #"eeNieto.01" #"UTBR002.01" #
    @@psm_split_op1 = "1100.2800" #"2000.500" #
    @@psm_return_op1 = "1100.2900" #"2000.500" #
    @@psm_merge_op1 = "1100.2900" #"2000.600" #
    @@nthreads = 20

    
    def test00_setup
      $setup_ok = false
            
      # unload loaded carrier
      li = $sv.lot_info @@lot
      if li.eqp != ""
        eqi = $sv.eqp_info(li.eqp)
        eqi.ports.each {|p| assert $sv.eqp_unload(li.eqp, p.port, p.loaded_carrier) if p.loaded_carrier != ""}
        eqi.ib.loaded_carriers.each {|c| assert $sv.eqp_unload(li.eqp, eqi.ports[0].port, c)} if eqi.ib
      end

      assert $sv.lot_hold_release(@@lot, nil), "cannot release hold"
      assert $sv.lot_opelocate @@lot, "first"
      assert $sv.merge_lot_family(@@lot)
      assert $sv.lot_experiment_delete(@@lot)
      # move lots to current point PD
      assert $sv.lot_opelocate(@@lot, "first")

      $setup_ok = true
    end

    def test01_parallel_calls
      threads = []
      @@nthreads.times.each do |i|
        threads << Thread.new(i) {
          sv = SiView::MM.new $env
          sv.lot_experiment(@@lot, ["01", "02"], @@branch_route1, @@psm_split_op1, @@psm_return_op1, :merge=>@@psm_merge_op1)
        }
      end
      threads.each { |t| t.join }
      # TODO: Move lot to operation and check splits
    end
end
