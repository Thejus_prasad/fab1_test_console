=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: ssteidte, 2015-01-13

History:
=end

require 'SiViewTestCase'

class Test_STBSameLotID < SiViewTestCase
  @@product1 = 'BSM-1FA.UAA00'
  @@product2 = 'BSM-1FA.BAA00'
  #@@product3 = 'BSM-1FA.SAA00'


  def self.after_all_passed
    @@lots.each {|lot| $sv.delete_lot_family(lot)}
  end


  def test00_setup
    $setup_ok = false
    #
    @@startbank2 = $sv.product_list(product: @@product2).first.startbank
    @@lots = []
    #
    $setup_ok = true
  end
  
  def test01_happy
    $setup_ok = false
    #
    # create and complete test lot on the 1st route
    assert lot = $svtest.new_lot(product: @@product1, bankin: true)
    @@lots << lot
    # move to start bank for 2nd route, schedule and STB
    assert_equal 0, $sv.lot_bank_move(lot, @@startbank2)
    newlot = $svtest.new_lot(product: @@product2, lotGenerationType: 'By Source Lot', lot: lot, srclot: lot)
    assert_equal lot, newlot, "wrong lot id"
    #
    $setup_ok = true
  end

  def test02_parent_grandchild
    # create and complete test lot on the 1st route
    assert lot = $svtest.new_lot(product: @@product1, bankin: true)
    @@lots << lot
    # split lot into child and grandchild, scrap child
    assert lc = $sv.lot_split(lot, 10)
    assert lgc = $sv.lot_split(lc, 5)
    assert_equal 0, $sv.scrap_wafers(lc)
    assert_equal 0, $sv.wafer_sort(lc, '')
    # move parent and grandchild to start bank for 2nd route and STB
    [lot, lgc].each {|l|
      assert_equal 0, $sv.lot_bank_move(l, @@startbank2)
      newlot = $svtest.new_lot(product: @@product2, lotGenerationType: 'By Source Lot', lot: l, srclot: l)
      assert_equal l, newlot, "wrong lot id"
    }
    # merge lot on 2nd route route
    assert_equal 0, $sv.lot_merge(lot, lgc)
  end

  def test03_grandchild_parent
    # create and complete test lot on the 1st route
    assert lot = $svtest.new_lot(product: @@product1, bankin: true)
    @@lots << lot
    # split lot into child and grandchild, scrap child
    assert lc = $sv.lot_split(lot, 10)
    assert lgc = $sv.lot_split(lc, 5)
    assert_equal 0, $sv.scrap_wafers(lc)
    assert_equal 0, $sv.wafer_sort(lc, '')
    # move parent and grandchild to start bank for 2nd route and STB
    [lgc, lot].each {|l|
      assert_equal 0, $sv.lot_bank_move(l, @@startbank2)
      newlot = $svtest.new_lot(product: @@product2, lotGenerationType: 'By Source Lot', lot: l, srclot: l)
      assert_equal l, newlot, "wrong lot id"
    }
    # merge lot on 2nd route route
    assert_equal 0, $sv.lot_merge(lot, lgc)
  end

end
