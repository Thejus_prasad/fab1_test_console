=begin 
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.
    
Author: Steffen Steidten, 2010-05-03
=end

$:.unshift(File.dirname(__FILE__)) unless $:.member?(File.dirname(__FILE__))
require "RubyTestCase"

# Test Lot IDs, MSR 452861
class Test190_LotIDs < RubyTestCase
  Description = "Test Lot IDs, MSR 452861"
    
  @@product = "UT-PRODUCT001.01"
  @@route = "UTRT001.01"
  @@salt = "1"
  
  # test with and without leading "U"
  
  def test10_8_digits_U2
    check_lot("U#{@@salt}1OI.00")
  end
  
  def test10_8_digits_A2
    check_lot("A#{@@salt}1OI.00")
  end
  
  def test10_8_digits_U3
    check_lot("U#{@@salt}2O.000")
  end
  
  def test10_8_digits_A3
    check_lot("A#{@@salt}2O.000")
  end
  
  def test11_9_digits_UA2
    check_lot("U#{@@salt}3OIO.00")
    check_lot("A#{@@salt}3OIO.00")
  end
  
  def test11_9_digits_UA3
    check_lot("U#{@@salt}4OI.000")
    check_lot("A#{@@salt}4OI.000")
  end
  
  def test12_10_digits_UA2
    check_lot("U#{@@salt}5OIOI.00")
    check_lot("A#{@@salt}5OIOI.00")
  end
  
  def test12_10_digits_UA3
    check_lot("U#{@@salt}6OIO.000")
    check_lot("A#{@@salt}6OIO.000")
  end
  
  def test13_13_digits_UA2
    check_lot("U#{@@salt}7OIOIOIO.00")
    check_lot("A#{@@salt}7OIOIOIO.00")
  end
  
  def test13_13_digits_UA3
    check_lot("U#{@@salt}8OIOIOI.000")
    check_lot("A#{@@salt}8OIOIOI.000")
  end
  
  def XXtest14_63_digits_2
    check_lot("U" + "IO"*29 + "1.00")
    check_lot("A" + "IO"*29 + "1.00")
  end
  
  def XXtest14_63_digits_3
    check_lot("U" + "IO"*29 + ".000")
    check_lot("A" + "IO"*29 + ".000")
  end
  
  def XXtest15_64_digits_2
    check_lot("U" + "IO"*29 + "12.00")
    check_lot("A" + "IO"*29 + "12.00")
  end
  
  def XXtest15_64_digits_3
    check_lot("U" + "IO"*29 + "2.000")
    check_lot("A" + "IO"*29 + "2.000")
  end
  
  def test81_notdot8
    lot = "U#{@@salt}987654"  # 8 chars, but no dot
    $sv.delete_lot_family(lot)
    res = $sv.new_lot_release(:product=>@@product, :route=>@@route, :lot=>lot)
    assert_nil res, "error: lot #{lot} shouldn't have been scheduled"
    res = $sv.stb(:product=>@@product, :route=>@@route, :lot=>lot)
    assert_nil res, "error: lot #{lot} shouldn't have been created"
  end
  
  def test82_notdot13
    lot = "U#{@@salt}98765432101"  # 13 chars, but no dot
    $sv.delete_lot_family(lot)
    res = $sv.new_lot_release(:product=>@@product, :route=>@@route, :lot=>lot)
    assert_nil res, "error: lot #{lot} shouldn't have been scheduled"
    res = $sv.stb(:product=>@@product, :route=>@@route, :lot=>lot)
    assert_nil res, "error: lot #{lot} shouldn't have been created"
  end
  
  
  def check_lot(lot)
    $log.info "checking lot #{lot}, name has #{lot.size} chars"
    $sv.delete_lot_family(lot)
    assert_nil $sv.lot_info(lot), "lot #{lot} already exists, cannot be used for tests."
    $sv.new_lot_release(:product=>@@product, :route=>@@route, :lot=>lot)
    assert_equal lot, $sv.stb(:product=>@@product, :route=>@@route, :lot=>lot)
    clot = $sv.lot_split(lot, 2)
    assert_not_nil clot, "error splitting lot #{lot}"
    assert_equal 0, $sv.lot_merge(lot, nil), "error merging lot #{lot}"
    true
  end

end
