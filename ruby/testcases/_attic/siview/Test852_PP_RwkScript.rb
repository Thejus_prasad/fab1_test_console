=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: sfrieske, 2017-03-20

History:
=end

require 'SiViewTestCase'
require 'siviewmmdb'


# Test script parameter history is written correctly
class Test852_PP_RwkScript < SiViewTestCase
  @@opNo_rwk = '1000.300'
  @@rwk_route = 'UTRW001.01'
  @@rwk_route_dyn = 'UTRW001D.01'
  @@script = 'UT-PRE1-OOC'
  @@script_params = {'OOC'=>'Y', 'FabDPML'=>'3.142340', 'PassCount'=>'1'}  # as configured in SM for the script, beware of Floats!
  @@history_delay = 30  # history watchdogs run every 10 s


  def self.after_all_passed
    @@testlots.each {|lot| $sv.delete_lot_family(lot)}
  end

  def setup
    return unless self.class.class_variable_defined?(:@@lot)
    assert_equal 0, $sv.lot_cleanup(@@lot)
    @@script_params.each_pair {|k, v| assert $sv.user_parameter_set_verify('Lot', @@lot, k, 0)} # anything that gets changed
    assert verify_scriptparameter_history(@@lot, {'OOC'=>'0', 'FabDPML'=>'0', 'PassCount'=>'0'}), "error resetting script params"
  end


  def test00_setup
    $setup_ok = false
    #
    # ensure rwk route has the PRE-1 script at first operation
    [@@rwk_route, @@rwk_route_dyn].each {|route|
      assert oinfo = $sm.object_info(:mainpd, route).first
      assert_equal @@script, oinfo.specific[:operations].first.pre1, "wrong PRE-1 script at rework route #{route}'s 1st operation"
    }
    # create test lot
    @@testlots = []
    assert @@lot = $svtest.new_lot, "error creating test lot"
    @@testlots << @@lot
    #
    @@historydb = SiView::MMDB.new($env)
    #
    $setup_ok = true
  end

  def test11_rwk
    lot = @@lot
    assert_equal 0, $sv.lot_opelocate(lot, @@opNo_rwk, offset: 1)
    assert_equal 0, $sv.lot_rework(lot, @@rwk_route)
    assert verify_scriptparameter_history(lot, @@script_params), "no history update"
  end

  def test12_rwk_force
    lot = @@lot
    assert_equal 0, $sv.lot_opelocate(lot, @@opNo_rwk, offset: 1)
    assert_equal 0, $sv.lot_rework(lot, @@rwk_route, force: true)
    assert verify_scriptparameter_history(lot, @@script_params), "no history update"
  end

  def test13_rwk_dyn
    lot = @@lot
    assert_equal 0, $sv.lot_opelocate(lot, @@opNo_rwk, offset: 1)
    assert_equal 0, $sv.lot_rework(lot, @@rwk_route_dyn, dynamic: true)
    assert verify_scriptparameter_history(lot, @@script_params), "no history update"
  end

  def test14_rwk_dyn_force
    lot = @@lot
    assert_equal 0, $sv.lot_opelocate(lot, @@opNo_rwk, offset: 1)
    assert_equal 0, $sv.lot_rework(lot, @@rwk_route_dyn, dynamic: true, force: true)
    assert verify_scriptparameter_history(lot, @@script_params), "no history update"
  end

  def test21_rwkonhold
    lot = @@lot
    assert_equal 0, $sv.lot_opelocate(lot, @@opNo_rwk, offset: 1)
    assert_equal 0, $sv.lot_hold(lot, nil)
    assert_equal 0, $sv.lot_rework(lot, @@rwk_route, onhold: true)
    assert verify_scriptparameter_history(lot, @@script_params), "no history update"
  end

  def test22_rwkonhold_force
    lot = @@lot
    assert_equal 0, $sv.lot_opelocate(lot, @@opNo_rwk, offset: 1)
    assert_equal 0, $sv.lot_hold(lot, nil)
    assert_equal 0, $sv.lot_rework(lot, @@rwk_route, onhold: true, force: true)
    assert verify_scriptparameter_history(lot, @@script_params), "no history update"
  end

  def test23_rwkonhold_dyn
    lot = @@lot
    assert_equal 0, $sv.lot_opelocate(lot, @@opNo_rwk, offset: 1)
    assert_equal 0, $sv.lot_hold(lot, nil)
    assert_equal 0, $sv.lot_rework(lot, @@rwk_route_dyn, onhold: true, dynamic: true)
    assert verify_scriptparameter_history(lot, @@script_params), "no history update"
  end

  def test24_rwkonhold_dyn_force
    lot = @@lot
    assert_equal 0, $sv.lot_opelocate(lot, @@opNo_rwk, offset: 1)
    assert_equal 0, $sv.lot_hold(lot, nil)
    assert_equal 0, $sv.lot_rework(lot, @@rwk_route_dyn, onhold: true, dynamic: true, force: true)
    assert verify_scriptparameter_history(lot, @@script_params), "no history update"
  end


  # aux methods

  def verify_scriptparameter_history(lot, params)
    $log.info "verify_scriptparameter_history #{lot.inspect}, #{params}"
    tstart = Time.now
    ret = true
    params.each_pair {|k, v|
      res = wait_for(timeout: @@history_delay, tstart: tstart) {
        p = @@historydb.user_parameter_history(id: lot, name: k).first
        p && p.value == v
      }
      ($log.warn "no current history entry for lot #{lot.inspect}, name #{k.inspect}"; ret = false) unless res
    }
    return ret
  end

end
