=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: Steffen Steidten, 2014-12-02

=end

require 'SiViewTestCase'

# Test CS_TxProductIDListInq for active versioning, MSR836284
class Test910_CSProductList < SiViewTestCase
  @@product = 'UT-PRODUCT200.01'

  def test00_setup
    $setup_ok = false
    assert objinfo = $sm.object_info(:product, @@product).first
    @@smroute = objinfo.specific[:route]
    assert  @@smroute.end_with?('##'), "route #{ @@smroute} has no active versioning"
    $setup_ok = true
  end

  def test11_product
    # product filter
    assert prod = $sv.CS_product_list(product: @@product).first
    rte, version = prod.route.split('.')
    # verify it is the correct route
    assert_equal  @@smroute.split('.').first, rte, "wrong route"
    # verify it is a specific route, not .##
    refute_equal '##', version, "wrong version, must be a spefic version"
  end

  def test12_route_hash
    # route filter with ## route
    assert prod = $sv.CS_product_list(route: @@smroute).first
    rte, version = prod.route.split('.')
    refute_equal '##', version, "wrong version, must be a spefic version"
  end

  def test12_route_regular
    # route filter with specific route, test11 has established it is correct
    assert route = $sv.CS_product_list(product: @@product).first.route
    assert prod = $sv.CS_product_list(route: route).first
    rte, version = prod.route.split('.')
    refute_equal '##', version, "wrong version, must be a spefic version"
  end

end
