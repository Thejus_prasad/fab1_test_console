=begin 
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.
    
Author:  Daniel Steger, 2015-05-04
Version: 1.0.0

=end

require 'SiViewTestCase'


# Test MSR 128560 (load and unload sequences on a sorter without NPW and SLR)
# included the Transfer On Route scenario
class Test_SorterOnRoute < SiViewTestCase
  @@eqp = "UTS001"
  @@sorter_opNo = "1000.930"
  @@reason_code = "INT"


  def self.after_all_passed
    $sv.delete_lot_family(@@testlot) if @@testlot
  end

  
  def test00_setup
    $setup_ok = false
    @@ports = $sv.eqp_info(@@eqp).ports.select {|p| p.pg == 'PG1'}.map {|p| p.port}
    assert @@ports.count > 1, "need at least 2 ports in the group"
    $svtest.eis.restart_tool(@@eqp)    
    assert @@testlot = $svtest.new_lot, "failed to create test lot"
    $setup_ok = true
  end
  
  def test11_load_unload_offline1
    $setup_ok = false
    _run_scenario("Off-Line-1")
    $setup_ok = true
  end
    
  def test12_load_unload_auto1
    _run_scenario("Auto-1")
  end
  
  def test21_load_unload_offline2
    _run_scenario("Off-Line-2")
  end
    
  def test22_load_unload_auto3
    _run_scenario("Auto-3")
  end
  

  def _run_scenario(mode)
    assert_equal 0, $sv.eqp_cleanup(@@eqp, mode: mode), "cannot cleanup equipment"
    lot = @@testlot
    assert $sv.lot_cleanup(lot, opNo: @@sorter_opNo, stocker: $svtest.stocker), "failed to cleanup lot"
    #
    empty = $svtest.get_empty_carrier    
    assert $svtest.cleanup_carrier(empty), "failed to set xfer state"
    #    
    assert cj = $sv.slr(@@eqp, lot: lot, port: @@ports[0]), "failed to reserve lot at #{@@ports[0]}"
    if mode =~ /(-2|-3)/
      assert_equal 0, $sv.npw_reserve(@@eqp, carrier: empty, port: @@ports[1]), "failed to do NPW"    
    end
    assert_equal 0, $sv.carrier_xfer_status_change(empty, 'EO', @@eqp), "failed to set xfer state"
    _load_foup(@@eqp, @@ports[1], empty, mode, purpose: 'Other')
    #    
    carrier = $sv.lot_info(lot).carrier
    assert $sv.carrier_xfer_status_change(carrier, 'EO', @@eqp), "failed to set xfer state"
    _load_foup(@@eqp, @@ports[0], carrier, mode)
    
    assert cj = $sv.eqp_opestart(@@eqp, carrier), "failed to start operation"
    assert_equal 0, $sv.lot_futurehold(lot, $sv.lot_info(lot).opNo, @@reason_code, post: true), "failed to set future hold"
    assert_equal 0, $sv.eqp_opecomp(@@eqp, cj), "failed to complete job"
    assert_equal "ONHOLD", $sv.lot_info(lot).status, "wrong lot status"
    assert_equal 0, $sv.wafer_sort_rpt(lot, empty, eqp: @@eqp), "failed to transfer wafers to destination"
    assert_equal empty, $sv.lot_info(lot).carrier, "lot should be in empty carrier"
    assert_equal [], $sv.carrier_status(carrier).lots, "no lot should be in empty carrier"
    assert_equal 0, $sv.lot_hold_release(lot, nil), "releasing lot failed"
    _unload_foup(@@eqp, @@ports[0], carrier, mode)
    _unload_foup(@@eqp, @@ports[1], empty, mode)
  end
  
  
  def _load_foup(eqp, port, carrier, mode, params={})
    assert_equal 0, $sv.eqp_port_status_change(eqp, port, 'LoadComp', params) if mode.start_with?('Auto')
    assert_equal 0, $sv.eqp_load(eqp, port, carrier, params)
  end
  
  def _unload_foup(eqp, port, carrier, mode)    
    assert_equal 0, $sv.eqp_port_status_change(eqp, port, 'UnloadReq') if mode.start_with?('Auto')
    assert_equal 0, $sv.eqp_unload(eqp, port, carrier)
    if mode.start_with?('Auto')
      assert_equal 0, $sv.eqp_port_status_change(eqp, port, 'UnloadComp') 
      assert_equal 0, $sv.eqp_port_status_change(eqp, port, 'LoadReq') 
    end
  end

end
