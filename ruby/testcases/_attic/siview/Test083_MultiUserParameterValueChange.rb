=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: Paul Peschel, 2013-12-17
=end

require "SiViewTestCase"

# Test CS_TxUserParameterValueForMultipleObjectReq
class Test083_MultiUserParameterValueChange < SiViewTestCase
  @@lotcount = 10
  @@datatypes = "STRING INTEGER REAL"
  @@userparams = "UTstring UTinteger UTreal"

  def self.after_all_passed
    @@lots.each {|lot| $sv.delete_lot_family(lot)}
  end
    
  def test00_setup
    @@userparameters = Hash[@@datatypes.split.zip(@@userparams.split)]
    # get lot and wafer
    @@lots = $svtest.new_lots(@@lotcount)
    @@wafers = $sv.lot_info(@@lots[0], wafers: true).wafers.collect {|w| w.wafer}
    $log.info "testing with lots #{@@lots} and wafers #{@@wafers}"
  end
  
  def test11_correct_datatypes_multi_single
    # set with function multi, check with function single
    cleanup_params
    set_correct_datatypes_multi_single("Lot", @@lots, @@datatypes.split, @@userparams.split)
    set_correct_datatypes_multi_single("Wafer", @@wafers, @@datatypes.split, @@userparams.split)
  end
  
  def test12_correct_datatypes_single_multi
    # set with function single, check with function multi
    cleanup_params
    set_correct_datatypes_single_multi("Lot", @@lots, @@datatypes.split, @@userparams.split)
    set_correct_datatypes_single_multi("Wafer", @@wafers, @@datatypes.split, @@userparams.split)
  end
    
  def test21_wrong_type_for_string
    cleanup_params
    check_wrong_datatypes("Lot", @@lots, @@datatypes.split, "INTEGER", @@userparams.split)
    check_wrong_datatypes("Wafer", @@wafers, @@datatypes.split, "INTEGER", @@userparams.split)
  end
  
  def test22_wrong_type_for_integer
    cleanup_params
    check_wrong_datatypes("Lot", @@lots, @@datatypes.split, "REAL", @@userparams.split)
    check_wrong_datatypes("Wafer", @@wafers, @@datatypes.split, "REAL", @@userparams.split)
  end
  
  def test23_wrong_type_for_real
    cleanup_params
    check_wrong_datatypes("Lot", @@lots, @@datatypes.split, "STRING", @@userparams.split)
    check_wrong_datatypes("Wafer", @@wafers, @@datatypes.split, "STRING", @@userparams.split)
  end
  
  def test24_update_wrong_datatypes
    cleanup_params
    check_update_wrong_datatypes("Lot", @@lots, @@datatypes.split, "STRING", @@userparams.split)
    check_update_wrong_datatypes("Wafer", @@wafers, @@datatypes.split, "STRING", @@userparams.split)
  end
  
  def testmanual91_writeperf_multi_single
    # compare performance of single vs. multi writes, special test: on demand only
    lots = $sv.lot_list(lot: 'U%', status: 'Waiting').take(1000)
    assert_equal 1000, lots.size, "not enough lots"
    [1, 2].each {|mode|
        # single
        data_single = lots[0..499].collect {|l| ['Lot', l, 'UTstring', ("TEST%02d" % (10*rand)).to_s, mode, {datatype: 'STRING', silent: true}]}
        $log.info "mode #{mode}, #{data_single.size} individual lots"
        res = 0
        tstart = Time.now
        data_single.each {|e| res += $sv.user_parameter_change(*e)}
        dsingle = Time.now - tstart
        assert_equal 0, res
        # multi
        data_multi = get_data_hash('Lot', lots[500..-1], ['STRING'], ['UTstring'])
        $log.info "mode #{mode}, #{data_multi.keys.size} lots bulk change"
        tstart = Time.now
        assert_equal 0, $sv.user_parameter_change_multi(data_multi, mode)
        dmulti = Time.now - tstart
        $log.info "mode #{mode} duration  multi: #{dmulti.round(1)}s, single: #{dsingle.round(1)}s"
      }
  end
  
  def set_correct_datatypes_multi_single(classname, pid, datatypes, pnames)
    $log.info "set_correct_datatypes_multi_single #{classname.inspect}, #{pid.inspect}"
    [1, 2].each do |mode|
      data_hash = get_data_hash(classname, pid, datatypes, pnames)
      assert_equal 0, $sv.user_parameter_change_multi(data_hash, mode), "error setting multiple parameters"
      # validate values are set
      data_hash.each_pair do |k,v|
        oid, lot = k.split("::")
        p_result = $sv.user_parameter(oid, lot, v)
        res = true
        v[:names].each_with_index do |p,i|
          res &= verify_equal(v[:values][i], p_result.find {|r| r.name == p}.value)
        end
        assert res, "verification failed"
      end
    end
  end
  
  def set_correct_datatypes_single_multi(classname, pid, datatypes, pnames)
    $log.info "set_correct_datatypes_single_multi #{classname.inspect}, #{pid.inspect}"
    data_hash = {}
    data_hash = get_data_hash(classname, pid, datatypes, pnames)
    data_hash.each_pair do |k,v|
      oid, lot = k.split("::")
      $sv.user_parameter_change("#{oid}", lot, v[:names], v[:values], 1, :datatype=>v[:datatype])
    end
    # validate values are set
    validate_data_hash(classname, data_hash)
  end
  
  def check_update_wrong_datatypes(classname, pid, datatypes, wrong_datatype, pnames)
    $log.info "check_update_wrong_datatypes #{classname.inspect}, #{pid.inspect}"
    data_hash = {}
    data_hash_wrong = {}
    data_hash = get_data_hash(classname, pid, datatypes, pnames)
    # set parameter
    assert_equal 0, $sv.user_parameter_change_multi(data_hash, 1), "error setting multiple parameters"
    # manipulate hash
    wrong_pid = pid[0,1]
    data_hash_wrong = get_data_hash(classname, wrong_pid, [wrong_datatype]*pnames.count, pnames)
    # update parameter   
    assert_equal 12008, $sv.user_parameter_change_multi(data_hash_wrong, 2)
    # check if parameter not changed
    validate_data_hash(classname, data_hash)
  end
  
  def check_wrong_datatypes(classname, pid, datatypes, wrong_datatype, pnames)
    $log.info "check_wrong_datatypes #{classname.inspect}, #{pid.inspect}"
    data_hash = {}
    # objects with wrong datatype
    wrong_pid = pid[0,1]
    data_hash = get_data_hash(classname, wrong_pid, [wrong_datatype]*pnames.count, pnames)
    # objects with correct datatype
    correct_pid = pid[1..-1]
    data_hash.merge!(get_data_hash(classname, correct_pid, datatypes, pnames))
    # try to add a formerly deleted parameter
    assert_equal 12008, $sv.user_parameter_change_multi(data_hash, 1)
    wrong_pid.each do |wpid|
      r = $sv._res.strCS_UserParameterValueChangeResultSeq.select {|res| res.objectID.identifier == wpid}
      assert_equal 2, r.count, "expected error for #{wpid}"
    end
    correct_pid.each do |cpid|
      r = $sv._res.strCS_UserParameterValueChangeResultSeq.select {|res| res.objectID.identifier == cpid}
      assert r.count == 0, "expected no error for #{cpid}"
    end    
    # check that no parameter was set
    res = true
    data = $sv.user_parameter_multi(classname, pid)
    data.each_pair do |k,v|
      pnames.each do |pname|
        res &= verify_equal(nil, v.find {|r| r.name == pname}, "User Parameter #{pnames.inspect} set but it should not be set for #{k}" )
      end
    end
    assert res
  end
  
  # Help Funnctions
  def get_data_hash(type, pid, datatypes, pnames)
    param_hash = {}
    pid.each do |p| 
      values = datatypes.collect do |d_type|
        if d_type == "STRING"
          ("TEST%02d" % (10*rand)).to_s
        elsif d_type == "INTEGER"
          (rand*10).to_i.to_s
        elsif d_type == "REAL"
          rand.round(6).to_s
        end
      end
      param_hash["#{type}::#{p}"] = {:names=>pnames, :values=>values, :datatype=>datatypes}
    end
    return param_hash
  end
  
  def validate_data_hash(classname, hash)
    if classname == "Lot"
      data = $sv.user_parameter_multi(classname, @@lots)
    elsif classname == "Wafer"
      data = $sv.user_parameter_multi(classname, @@wafers)
    end
    hash.each_pair do |key,value|
      oid, lot = key.split("::")
      res = true
      value[:names].each_with_index do |p,i|
        res &= verify_equal(value[:values][i], data[key].find {|r| r.name == p}.value)
      end
      assert res, "verification failed"
    end
  end
  
  def cleanup_params
    $log.info "cleanup_params"
    @@userparameters.each_pair do |datatype, pname|
      @@lots.each {|l| $sv.user_parameter_change_multi({"Lot::#{l}" => {:names=>pname, :datatype=>datatype}}, 3)}
      @@wafers.each {|w| $sv.user_parameter_change_multi({"Wafer::#{w}" => {:names=>pname, :datatype=>datatype}}, 3)}
    end
    sleep 2
  end
end