=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: Daniel Steger, 2013-04-04

http://myteamsdrs2/sites/FICSQA/Testplans/SiView/SiView%20C6.14%20MSR643339.xlsm
=end

require "RubyTestCase"
require "misc"


# Test all maintenance for all existing inhibit combinations (MSR643339)
#  should be run whenever a new inhibit class is introduced
class Test134_InhibitMaintenance < RubyTestCase
  @@eqp = "UTC001" # Need to have a chamber tool
  @@chamber = "PM1"
  @@stage = "UTNOTUSED"
  # inhibit maintenance runs every 4 hours
  @@maintenance_time = 4 * 3600 + 600

  def test01_maintenance
    # get setup
    pgroup = $sv.productgroup_list.first
    pd = $sv.pd_list.first
    lrecipe = $sv.object_id_list("PosLogicalRecipe", "A%").first
    mrecipe = $sv.machine_recipes.first
    mods = $sv.module_list.first
    product = $sv.product_list.first.product
    reticle_group = $sv.reticle_group_list.first
    reticle = $sv.reticleid_list.first
    fixture_group = $sv.fixture_groups.first
    fixture = $sv.fixture_list.first
    route = $sv.route_index_list("Production").first
    ope = $sv.route_operations(route).first.opNo
    tech = $sv.object_id_list("PosTechnology", "%").first
    stage = @@stage
    # setup inhibits
    itime = $sv.siview_timestamp(Time.now + 60) # inhibit to exipire in 1min
    $log.info "inhibits (1st run) will expire at #{itime}"
    etime = $sv.siview_timestamp(Time.now + 24*3600) # inhibit to exipire in 24 hours
    # clean up
    assert $sv.inhibit_cancel([], nil, :end=>etime), "failed to cleanup"
    SiView::MM::InhibitClasses.each_pair do |iclass,entities|
      attrib = []
      oids = entities.collect {|e|      
        case e
        when "Chamber"; attrib << @@chamber; @@eqp
        when "Equipment"; attrib << ''; @@eqp
        when "Logical Recipe"; attrib << ''; lrecipe
        when "Machine Recipe"; attrib << ''; mrecipe
        when "Process Definition"; attrib << ''; pd
        when "Product Group"; attrib << ''; pgroup
        when "Reticle"; attrib << ''; reticle
        when "Fixture"; attrib << ''; fixture
        when "Product Specification"; attrib << ''; product
        when "Module Process Definition"; attrib << ''; mods
        when "Reticle Group"; attrib << ''; reticle_group
        when "Fixture Group"; attrib << ''; fixture_group
        when "Route"; attrib << ''; route
        when "Operation"; attrib << ope; route
        when 'Stage'; attrib << ''; stage
        when 'Technology'; attrib << ''; tech
        else $log.error "Entity not found: #{e}"
        end
      }
      $log.info "testing inhibits for class #{iclass.to_s}"
      register_inhibit(iclass, oids, attrib, itime, etime)
    end
    # check all inhibits are registered
    icount = $sv.inhibit_list([], nil, :tend=>itime).count
    assert_equal SiView::MM::InhibitClasses.keys.count, icount, "wrong number of inhibits"
    ecount = $sv.inhibit_list([], nil, :tend=>etime).count
    assert_equal 2*SiView::MM::InhibitClasses.keys.count, ecount, "wrong number of inhibits"    
    # wait for maintenance (check every 5 min)
    inhibits = []
    assert wait_for(:sleeptime=>300, :timeout=>@@maintenance_time) {
      $log.info "checking inhibit list..."
      inhibits = $sv.inhibit_list([], nil, :end=>itime)
      inhibits.count == 0
    }, "inhibits not cleared correctly: #{inhibits.inspect}"
    ecount = $sv.inhibit_list([], nil, :tend=>etime).count
    assert_equal SiView::MM::InhibitClasses.keys.count, ecount, "inhibits should still exist"
	#cleanup
	assert $sv.inhibit_cancel([], nil, :tend=>etime), "failed to cancel existing inhibits"
  end
  
  def register_inhibit(iclass, oids, attrib, itime, etime)
    $log.info "register_inhibit #{iclass.inspect}, #{oids.inspect}, #{attrib.inspect}, #{itime.inspect}"
    assert $sv.inhibit_cancel(iclass, oids, :attrib=>attrib), "failed to cleanup"
    # set new inhibit with end time
    assert $sv.inhibit_entity(iclass, oids, :attrib=>attrib, :end=>itime), "failed to register inhibit"
    assert $sv.inhibit_entity(iclass, oids, :attrib=>attrib, :end=>etime), "failed to register inhibit"
  end
end