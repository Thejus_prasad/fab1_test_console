=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: sfrieske, 2016-07-13

=end

require 'SiViewTestCase'
require 'charts'


# Test TxLotOperationListInq, MSR 1078627 (rolled back in release C7.6)
class Test330_LotOpeList < SiViewTestCase
  @@routelong = 'UTRT001.99'  # 90+ operations
  @@searchcount = 90
  @@loops = 50
  
  def self.after_all_passed
    @@testlots.each {|lot| $sv.delete_lot_family(lot)}
  end


  def test00_setup
    $setup_ok = false
    #
    assert @@lot = $svtest.new_lot, "error creating test lot"
    @@testlots = [@@lot]
    refute_empty user2 = $sv.environment_variable[1]['CS_SP_PERSON_FOR_PASSCOUNT_ON_LOTOPELIST'], "no user configured"
    assert @@sv2 = SiView::MM.new($env, user: user2, password: :default, collect_durations: true)
    #
    $setup_ok = true
  end


  def test11_new_lot
    # use a fresh test lot
    lot = @@lot
    # regular user: verify lot_operation_list passcounts are empty except at first op
    assert opes = $sv.lot_operation_list(lot)
    assert_equal '1', opes[0].pass
    opes[1..-1].each {|ope| assert_equal '', ope.pass}
    # special user: verify lot_operation_list passcounts are empty except at first op
    assert opes2 = @@sv2.lot_operation_list(lot)
    assert_equal '1', opes2[0].pass
    opes2[1..-1].each {|ope| assert_equal '', ope.pass}
  end

  def test12_passcount
    # use the test lot and gapass it at all operations, depends on test11
    lot = @@lot
    assert_equal 0, $sv.lot_opelocate(lot, :first)
    assert opes = $sv.lot_operation_list(lot)
    opes[0..-2].each {|ope| assert_equal 0, $sv.lot_gatepass(lot, mandatory: ope.mandatory)}
    $sv.lot_bankin_cancel(lot)
    assert_equal 0, $sv.lot_opelocate(lot, :first)
    # regular user: verify lot_operation_list passcounts are empty except at first op
    assert opes = $sv.lot_operation_list(lot)
    assert_equal '2', opes[0].pass
    opes[1..-1].each {|ope| assert_equal '', ope.pass}
    # special user: verify lot_operation_list passcounts are not empty
    assert opes2 = @@sv2.lot_operation_list(lot)
    assert_equal '2', opes2[0].pass
    opes2[1..-2].each {|ope| assert_equal '1', ope.pass}
  end

  def test91_passcount_perf
    assert lot = $svtest.new_lot, "error creating test lot"
    @@testlots << lot
    assert_equal 0, $sv.schdl_change(lot, route: @@routelong)
    # get operation list for gate_pass
    assert opes = $sv.lot_operation_list(lot)
    assert opes.length > @@searchcount, "route is not long enough"
    # warm up
    2.times {@@sv2.lot_operation_list(lot)}
    @@sv2.manager.durations = []
    @@loops.times {|n|
      $log.info "\n\n--- loop #{n+1}/#{@@loops}"
      assert_equal 0, $sv.lot_opelocate(lot, :first)
      # call lot_operation_list as special user
      assert @@sv2.lot_operation_list(lot, searchcount: @@searchcount, raw: true)
      $log.info "duration: #{@@sv2.manager.durations.last}"
      # increase passcount of all ops
      opes[0..-2].each {|ope| 
        assert_equal 0, $sv.lot_gatepass(lot, route: ope.route, opNo: ope.opNo, mandatory: ope.mandatory)
      }
      $sv.lot_bankin_cancel(lot)
    }
    durations = @@sv2.manager.durations.collect {|tx, duration| tx == :TxLotOperationListInq ? duration : nil}.compact
    $log.info "durations: #{durations}"
    fname = "log/operationlist_#{$env}_#{Time.now.utc.iso8601.gsub(':', '-')}.png"
    $log.info "creating chart #{fname}"
    ch = Chart::XY.new(durations.each_with_index.collect {|d, i| [i+1, d]})
    ch.create_chart(title: "LotOperationList as #{@@sv2.user}", x: 'passcount', y: 't (ms)', dotsize: 6, export: fname)
  end

end
