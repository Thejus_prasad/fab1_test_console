=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: Steffen Steidten, 2013-12-04

History:
  2015-05-19 ssteidte,  cleanup, verified testcases with R15
=end

require 'SiViewTestCase'

class Test_SchdlChangeRsv < SiViewTestCase
    @@product = "BIC12-PG11.01"
    @@route = "BIC12-MAINROUTE-1.P002"
    @@opNo = "2000.1000"
    @@product_new = "BIC12-PG11.01"
    @@route_new = "BIC12-MAINROUTE-1.P1"
    @@route_new2 = "BIC12-MAINROUTE-1.Q1"
    @@opNo_new = "2000.2000"
    

    def self.after_all_passed
      @@testlots.each {|lot| $sv.delete_lot_family(lot)}
    end
    

    def teardown
      $sv.schdl_change_rsv_cancel('Lot', '')
      $sv.schdl_change_rsv_cancel('MainPD', '')
    end


    def test00_setup
      $setup_ok = false
      assert @@testlots = $svtest.new_lots(5, product: @@product, route: @@route)
      $setup_ok = true
    end
    
    def test11_lot
      lot = @@testlots[0]
      # create a new schdl change reservation for the lot
      assert_equal 0, $sv.schdl_change_rsv_create('Lot', lot, tgt_route: @@route, tgt_opNo: @@opNo, product: @@product_new, route: @@route_new, opNo: @@opNo_new)
      # process lot to get the schdl_change executed as part of PP
      assert_equal 0, $sv.lot_opelocate(lot, @@opNo)
      assert $sv.claim_process_lot(lot, happylot: true)
      # check schdl change has been executed
      li = $sv.lot_info(lot)
      assert_equal @@route_new, li.route
      assert_equal @@opNo_new, li.opNo
    end
    
    def test12_lot_wildcard
      lot = @@testlots[1]
      # create a new schdl change reservation with wildcard
      assert_equal 0, $sv.schdl_change_rsv_create('Lot', "#{lot[0..3]}*", tgt_route: @@route, tgt_opNo: @@opNo, product: @@product_new, route: @@route_new, opNo: @@opNo_new)
      # process lot to get the schdl_change executed as part of PP
      assert_equal 0, $sv.lot_opelocate(lot, @@opNo)
      assert $sv.claim_process_lot(lot, happylot: true)
      # check schdl change has been executed
      li = $sv.lot_info(lot)
      assert_equal @@route_new, li.route
      assert_equal @@opNo_new, li.opNo
    end
    
    def test21_route
      lot = @@testlots[2]
      # create a new schdl change reservation for the route
      assert_equal 0, $sv.schdl_change_rsv_create('MainPD', @@route, tgt_route: @@route, tgt_opNo: @@opNo, product: @@product_new, route: @@route_new2, opNo: @@opNo_new)
      # process lot to get the schdl_change executed as part of PP
      assert_equal 0, $sv.lot_opelocate(lot, @@opNo)
      assert $sv.claim_process_lot(lot, happylot: true)
      # check schdl change has been executed
      li = $sv.lot_info(lot)
      assert_equal @@route_new2, li.route
      assert_equal @@opNo_new, li.opNo
    end
    
    def test22_lot_route
      lot = @@testlots[3]
      # create new schdl change reservations for lot and route
      assert_equal 0, $sv.schdl_change_rsv_create('MainPD', @@route, tgt_route: @@route, tgt_opNo: @@opNo, product: @@product_new, route: @@route_new2, opNo: @@opNo_new)
      assert_equal 0, $sv.schdl_change_rsv_create('Lot', lot, tgt_route: @@route, tgt_opNo: @@opNo, product: @@product_new, route: @@route_new, opNo: @@opNo_new)
      # process lot to get the schdl_change executed as part of PP
      assert_equal 0, $sv.lot_opelocate(lot, @@opNo)
      assert $sv.claim_process_lot(lot, happylot: true)
      # check schdl change has been executed with lot data taking preference
      li = $sv.lot_info(lot)
      assert_equal @@route_new, li.route
      assert_equal @@opNo_new, li.opNo
    end
    
    def test31_futurehold
      lot = @@testlots[4]
      # create new schdl change reservation for lot
      assert_equal 0, $sv.schdl_change_rsv_create('Lot', lot, tgt_route: @@route, tgt_opNo: @@opNo, product: @@product_new, route: @@route_new, opNo: @@opNo_new)
      # create post future hold at current op
      assert_equal 0, $sv.lot_futurehold(lot, @@opNo, nil, post: true)
      # process lot to get the schdl_change executed as part of PP
      assert_equal 0, $sv.lot_opelocate(lot, @@opNo)
      assert $sv.claim_process_lot(lot, happylot: true)
      # verify lot status: on new route, next PD, OnHold
      sleep 5  # some time to execute the schedule change!!
      li = $sv.lot_info(lot)
      assert_equal @@route_new, li.route
      assert_equal @@opNo_new, li.opNo
      assert_equal 'ONHOLD', li.status
      assert !li.pp_lot && !li.pp_carrier, "lot is stil in PP"
    end
end
