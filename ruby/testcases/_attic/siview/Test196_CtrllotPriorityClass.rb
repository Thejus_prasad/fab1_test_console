=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: Steffen Steidten, 2011-01-12
=end

require 'SiViewTestCase'

# Test Priority Class of new Control Lots is actually controlled by SP_CTRLLOT_PRTYCLASS, MSR335442
# This MSR335442 will be removed in R15
class Test196_CtrllotPriorityClass < RubyTestCase
  
  
  def test11
    ['Equipment Monitor', 'Process Monitor', 'Dummy'].each {|lottype|
      assert lot = $svtest.new_monitor_lot(lottype), 'no lot'
      li = $sv.lot_info(lot)
      $log.info "created control lot #{lot} with prio #{li.priority}"
      prio = $sv.environment_variable(var: 'SP_CTRLLOT_PRTYCLASS')
      assert_not_equal '0', prio, "SP_CTRLLOT_PRTYCLASS is set to '0', test is invalid"
      assert_equal prio, li.priority, "wrong priority class"
      assert_equal '499999', li.epriority, " wrong external priority for #{lot}"
      $sv.delete_lot_family(lot)
    }
  end
  
end