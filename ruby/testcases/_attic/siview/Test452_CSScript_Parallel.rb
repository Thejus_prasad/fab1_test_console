=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: Daniel Steger, 2015-03-23
=end

require "SiViewTestCase"

# SiView Script testing MSR250861, TRK20
# #TODO: include parallel postprocessing fix Drop7    ## NOT YET WORKING in R15 ITDC
class Test452_CSScript_Parallel < SiViewTestCase
  @@product = 'UT-PROD-SCRIPT.01'
  @@route = 'UTRT-SCRIPT.01'
  @@eqps = ['UTC003']
  @@skip_chain_pre = '2000.5000'
  @@thread_count = 25
  @@timeout = 90


  def self.after_all_passed
    $sv.delete_lot_family(@@lot)
  end

  def teardown
    @@eqps.each {|eqp| assert_equal 0, $sv.eqp_cleanup(eqp) }
  end


  def test00_setup
    $setup_ok = false
    if $sv.release == 15
      assert_equal '1', $sv.environment_variable[1]['SP_POSTPROC_PARALLEL_SWITCH'], "parallel execution needs to be enabled"
      #assert_equal @@thread_count.to_s, $sv.environment_variable[1]["SP_POSTPROC_PARALLEL_THREADCOUNT"], "need at least as many threads"
      #TODO: SYNC_FLAG should be 3
    end
    # create test lot
    assert @@lot = $svtest.new_lot(product: @@product, route: @@route), "failed to create new lot"
    $setup_ok = true
  end


  def test10_parallel_execution
    eqp = @@eqps.first
    lot = @@lot
    assert_equal 0, $sv.lot_opelocate(lot, @@skip_chain_pre), "failed to locate lot to process operation"
    ##$sv.user_parameter_clear("Lot", lot)
    names = $sv.user_parameter('Lot', lot).map {|p| p.name}
    assert_equal 0, $sv.user_parameter_change('Lot', lot, names, '', 3)
    lots = [lot] + 24.times.collect {
      assert child = $sv.lot_split(lot, 1)
      assert_equal 0, $sv.user_parameter_change('Lot', child, 'UTstring', child)
      child
    }
    assert_equal 25, lots.length, "expected 25 lots after split"
    assert cj = $sv.claim_process_lot(lots, eqp: eqp, noopecomp: true, nounload: true, claim_carrier: true)
    assert_equal 0, $sv.eqp_opecomp(eqp, cj), "opecomp failed"
  end

  #TODO
  # Internal Buffer
  # Force Complete
  # Partial Complete

  def reset_scriptparameters(lot)
    assert $sv.user_parameter_delete_verify("Lot", lot, "UTContextKeys")
    assert $sv.user_parameter_delete_verify("Lot", lot, "UTCurrentPD")
    assert $sv.user_parameter_delete_verify("Lot", lot, "UTPassCount")
    assert $sv.user_parameter_delete_verify("Lot", lot, "UTUData")
  end

  def _get_user_parameter(params, name)
    _context = params.select {|p| p.name == name}
    assert _context.count > 0, "script parameter #{name} not found"
    return _context[0].value
  end

  def verify_scriptparameters(lot, linfo, current=true)
    $log.info "#{__method__} #{lot}, #{linfo.inspect}, #{current}"
    _params = $sv.user_parameter("Lot", lot)
    res = true
    res &= verify_equal("#{linfo.opNo};#{linfo.op};#{linfo.route}", _get_user_parameter(_params, "UTContextKeys"), "UTContextKeys")
    op = linfo.op
    # parameters are set from a post-script; CS_CurrentPD_ID returns the operation AFTER the OpeComplete
    op = $sv.lot_info(linfo.lot).op unless current
    res &= verify_equal("#{op}", _get_user_parameter(_params, "UTCurrentPD"), "UTCurrentPD")

    entry = $sv.lot_operation_list(lot, :forward=>false, :history=>false, :searchcount=>1, :current=>current)
    pass_count = entry[0].pass
    res &= verify_equal(pass_count, _get_user_parameter(_params, "UTPassCount"), "UTPassCount")

    udata1 = $sv.user_data_pd(@@pd)["ProcessLayer"]
    udata2 = $sv.user_data_route_operation(linfo.route, linfo.opNo)["RouteOpSafeOp"]
    res &= verify_equal("#{udata1};#{udata2};SQLERROR=100;SQLERROR=100", _get_user_parameter(_params, "UTUData"), "UTUData")
    return res
  end


  def verify_scriptparameter_history(lot, txtime, count)
    $log.info "verify scriptparameter_history #{lot}, #{txtime.inspect}, #{count}"
    return wait_for(timeout: @@timeout) {
      evs = $mds.user_parameter_history(:id=>lot, :tstart=>txtime)
      $log.info "#{evs.inspect}, size: #{evs.size}, expected: #{count}" if evs
      (evs and evs.size == count)
    }
  end

end
