=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: Steffen Steidten, 2011

History:
  2020-02-07 sfrieske, minor cleanup, retired because of DummyAMHS issues
=end

require 'SiViewTestCase'

# Test TxXmTransportJobInq, MSR437089
class XM_Regression_TransportJobs < SiViewTestCase
  @@carrier1 = nil
  @@stocker1 = 'UTSTO11'
  @@stocker2 = 'UTSTO12'
  @@eqp1 = 'UTF001'
  @@port1 = 'P1'    #"P4"

  # Choose a carrier that is NOTAVAILABLE and a tool in Semi2 or Auto-2 mode
  # Ensure the tools are not valid for the current operation of the lots in the carriers

  def test00_setup
    $setup_ok = false
    #
    assert_equal 0, @@sv.stocker_inventory_upload(@@stocker1)
    assert_equal 0, @@sv.stocker_inventory_upload(@@stocker2)
    @@carrier1 ||= $svtest.get_empty_carrier
    $log.info "using carrier #{@@carrier1}"
    # ensure correct carrier positions
    assert @@sv.wait_carrier_xfer([@@carrier1])
    cstat = @@sv.carrier_status(@@carrier1)
    unless [@@stocker1, @@stocker2].member?(cstat.stocker) && ['SI', 'MI', 'HI'].member?(cstat.xfer_status)
      @@sv.carrier_stockin(@@carrier1, @@stocker1)
    end
    # swap source and target stockers depending on current carrier position
    @@sv.carrier_xfer(@@carrier1, '', '', @@stocker1, '')
    assert @@sv.wait_carrier_xfer(@@carrier1)
    cstat = @@sv.carrier_status(@@carrier1)
    @@stocker1, @@stocker2 = @@stocker2, @@stocker1 if cstat.stocker == @@stocker2
    assert_equal @@stocker1, cstat.stocker, "carrier #{@@carrier1} is not in stocker #{@@stocker1}"
    assert cstat.xfer_status.index('I'), "transfer status of carrier #{@@carrier1} is not SI/MI/HI"
    # make carriers NOTAVAILABLE to prevent processing
    @@sv.carrier_status_change(@@carrier1, 'NOTAVAILABLE', check: true)
    # switch eqp to Semi-2
    assert_equal 0, @@sv.eqp_mode_change(@@eqp1, 'Semi-2', port: @@port1), "error switching #{@@eqp1} #{@@port1} to Semi-2"
    #
    assert @@xm = SiView::XM.new($env)
    #
    $setup_ok = true
  end

  # ### seem to be working, sf 2017-08-04   does not work with DummyAMHS and transport_jobs(function: 'INQUIRY')
  def test11_carrier
    assert_equal 0, @@sv.carrier_xfer(@@carrier1, @@stocker1, '', @@stocker2, ''), "error creating xfer job"
    #
    # tranfer job inquiries
    #
    # all carriers
    assert xjs = @@xm.transport_jobs, 'error in transport_jobs request to XM'
    has_job = false
    xjs.jobInqData.each {|j| j.carrierJobInqInfo.each {|c| has_job = true if c.carrierID.identifier == @@carrier1}}
    assert has_job, "no transport job for carrier '*'"
    #
    # specific carrier
    assert xjs = @@xm.transport_jobs(carrier: @@carrier1), 'error in transport_jobs request to XM'
    has_job = false
    xjs.jobInqData.each {|j| j.carrierJobInqInfo.each {|c| has_job = true if c.carrierID.identifier == @@carrier1}}
    assert has_job, "no transport job for carrier #{@@carrier1}"
    #
    # specific notexistent carrier
    assert $xjs = xjs = @@xm.transport_jobs(carrier: 'NOTEXISTENT'), 'error in transport_jobs request to XM'
    # assert_equal 0, xjs.size, "wrong xfer job: #{xjs.pretty_inspect}"
    assert_empty xjs.jobInqData, 'wrong xfer job found'
  end

  # does not work with DummyAMHS and transport_jobs(function: 'INQUIRY')
  def testman12_frommachine
    # frommachine valid
    assert xjs = @@xm.transport_jobs(frommachine: @@stocker1, carrier: @@carrier1), "error in transport_jobs request to XM"
    refute_equal 0, xjs.size, "no valid transport jobs"
    $log.debug "xjs: #{xjs.inspect}"
    jobs = xjs.collect {|j|
      j if j.carrierjobs.collect {|cj| cj if cj.frommachine != @@stocker1}.compact.size > 0
    }.compact
    assert_equal 0, jobs.size, "wrong filter for frommachine='#{@@stocker1}', jobs: #{jobs.inspect}"
    #
    # frommachine invalid
    assert xjs = @@xm.transport_jobs(frommachine: 'NOTEXISTENT'), "error in transport_jobs request to XM"
    $log.debug "xjs: #{xjs.inspect}"
    assert_equal 0, xjs.size, "wrong filter for frommachine='NOTEXISTENT', xjs: #{xjs.inspect}"
    #
    # frommachine valid again
    assert xjs = @@xm.transport_jobs(frommachine: @@stocker1, carrier: @@carrier1), "error in transport_jobs request to XM"
    refute_equal 0, xjs.size, "no valid transport jobs"
  end

end
