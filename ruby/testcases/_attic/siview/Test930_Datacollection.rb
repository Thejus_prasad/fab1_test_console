=begin
(c) Copyright 2016 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: Daniel Steger <daniel.steger@globalfoundries.com>
Version: 1.0.1

History:
  2016-09-05 dsteger, updated with C7.5 code
  2016-11-10 dsteger, added Pre-1 script test
=end

require 'SiViewTestCase'

class Test930_Datacollection < SiViewTestCase
  @@eqp_meas = 'UTF001'
  @@eqp_measib = 'UTI001'
  @@eqp_proc = 'UTC001'
  @@meas_opNo = '1000.5000'
  @@proc_opNo = '1000.1200'
  @@dcdef_meas = 'UTDCDEF01.00'
  @@dcspec_onreserve = 'ON'
  @@multi_corresope = '1'
  @@product = 'UT-PROD-DC001.01'
  @@product2 = 'UT-PROD-DC005.01'

  @@corresp_op = {
    '1000.5000' =>
      { '1000.1200' => 'DCSpecGroup.Corresponding-1',
        '1000.1500' => 'DCSpecGroup.Corresponding-2',
        '1000.2000' => 'DCSpecGroup.Corresponding-3',
        '1000.2500' => 'DCSpecGroup.Corresponding-1_1'
      },
    '1000.1200' => # process operation
      { '1000.5000' => 'DCSpecGroup.Corresponding-1'},
    '2000.5000' =>
      {  '2000.1200' => 'DCSpecGroup.Corresponding-1' }
    }
  @@premeas_chain = {
    '1000.1000' => [[]],
    '1000.1100' => [['1000.1000','UTPD-DCPREMEAS1.01']],
    '1000.5000' => [['1000.1000','UTPD-DCPREMEAS1.01'], ['1000.1100','UTPD-DCPREMEAS2.01'], ['1000.1200','UTPD-DCPROC1.01']],
  }

  @@meas_dcdefitems = ['UT-PARAM-001', 'UT-PARAM-002', 'UT-PARAM-003', 'CORRESPONDING_1', 'CORRESPONDING_2', 'CORRESPONDING_3', 'Site1-W1']
  @@dcitem_tags = { 'UT-PARAM-002' => 'A:Value;B:Test;' }

  @@ctx_route = 'UTRT-DC01.01'
  @@ctx_opNo = '2000.5000'
  @@ctx_results = {
    'UT-PROD-DC001.01' => ['UTDCSPEC05.00', 'UTDDCSPEC05.00'], # match product
    'UT-PROD-DC002.01' => ['UTDCSPEC01.00', ''],               # default
    'UT-PROD-DC003.01' => ['UTDCSPEC06.00', ''],               # match product group
    'UT-PROD-DC004.01' => ['UTDCSPEC07.00', '']                # match technology
  }

  @@process_pd = 'UTPD-DCPROCESS1.01'


  def self.after_all_passed
    @@testlots.each {|lot| $sv.delete_lot_family(lot)}
  end

  def setup
    [@@eqp_meas, @@eqp_measib].each {|eqp|
      #$eis.set_variable(eqp, 'processlot', 'false')
      assert $svtest.cleanup_eqp(eqp, mode: 'Auto-1')
    }
  end

  def teardown
    [@@eqp_meas, @@eqp_measib].each do |eqp|
      #$eis.set_variable(eqp, 'processlot', 'true')
      $sv.eqp_cleanup(eqp)
    end
    $sv.merge_lot_family(@@lot)
  end


  def test00_setup
    $setup_ok = false
    #
    assert_equal @@dcspec_onreserve, $sv.environment_variable[1]['CS_SP_DCSPEC_SET_ON_START_RESERVE'], "wrong environment setting"
    assert_equal '1', $sv.environment_variable[1]['SP_DCSPEC_FOR_PROCESS'], "wrong environment setting"
    assert_equal @@multi_corresope, $sv.environment_variable[1]['SP_MULTI_CORRESOPE_MERGE_RULE'], "wrong environment setting"
    assert @@lot = $svtest.new_lot(product: @@product, no_cache: true), "failed to get new lot"
    @@testlots = [@@lot]
    #
    $setup_ok = true
  end

  def test10_corresponding_pds
    # process lot at pre-measurement
    assert_equal 0, $sv.lot_opelocate(@@lot, @@premeas_chain.keys.first), "failed to locate to pre-measure"
    2.times { assert $sv.claim_process_lot(@@lot, happylot: true, slr: true, nodcitems: true), "failed to process at pre-measure op" }
    # process step
    assert_equal 0, $sv.lot_opelocate(@@lot, @@proc_opNo), "failed to locate to process step"
    check_limits_corresponding_ops(@@lot, @@proc_opNo, has_dcspec: true, process: true)
    # call at post-measurement
    assert_equal 0, $sv.lot_opelocate(@@lot, @@meas_opNo), "failed to locate to pre-measure"
    check_limits_corresponding_ops(@@lot, @@meas_opNo)
    # call after reservation
    assert_equal 0, $sv.eqp_mode_change(@@eqp_meas, 'Auto-1')
    assert cj = $sv.slr(@@eqp_meas, lot: @@lot), "failed to reserve lot at #{@@eqp_meas}"
    check_limits_corresponding_ops(@@lot, @@meas_opNo, reserved: true)
    # check control job
    check_ei_control_job(@@eqp_meas, cj, @@lot)
  end

  def test11_fb_multiplelots
    lots = [@@lot]
    lots << $sv.lot_split(@@lot, 2)
    _run_multilots(@@eqp_meas, lots)
  end

  def test20_ib
    # process lot at premeasurement
    assert_equal 0, $sv.lot_opelocate(@@lot, @@premeas_chain.keys.first), "failed to locate to pre-measure"
    2.times { assert_ok $sv.claim_process_lot(@@lot, happylot: true), "failed to process at pre-measure op" }
    # call at postmeasurement
    assert_equal 0, $sv.lot_opelocate(@@lot, @@meas_opNo), "failed to locate to pre-measure"
    # call after reservation
    assert_equal 0, $sv.eqp_mode_change(@@eqp_measib, 'Auto-1')
    assert cj = $sv.slr(@@eqp_measib, lot: @@lot), "failed to reserve lot at #{@@eqp_measib}"
    check_limits_corresponding_ops(@@lot, @@meas_opNo, reserved: cj)
    # check control job
    check_ei_control_job(@@eqp_measib, cj, @@lot)
  end

  def test21_ib_multiplelots
    lots = [@@lot]
    lots << $sv.lot_split(@@lot, 2)
    _run_multilots(@@eqp_measib, lots, separate: true)
  end

  def test30_contextcheck
    @@ctx_results.each_pair do |prod, res|
      dcspec, ddcspec = res
      check_limits_corresponding_ops_route_ope(@@ctx_route, @@ctx_opNo, prod, dcspec, ddcspec)
    end
  end

 def test40_slr_takeoutin
    # prepare eqp
    assert_equal 0, $sv.eqp_cleanup(@@eqp_meas, mode: 'Auto-3'), "failed to set #{@@eqp_meas} to Auto-3"
    # get 2 lots in different carriers
    assert @@testlots << $svtest.new_lot(product: @@product)
    lots = @@testlots
    carriers = @@testlots.map {|l| $sv.lot_info(l).carrier }
    assert_equal 2, carriers.count, "need 2 lots in different carriers"
    # prepare first lot and carrier
    assert $sv.wait_carrier_xfer(carriers)
    lots.each {|lot| assert_equal 0, $sv.lot_cleanup(lot, opNo: @@meas_opNo)}
    assert $svtest.cleanup_carrier(carriers[0], $svtest.stocker), "error stocking in #{carriers[0]}"
    port = $sv.eqp_info(@@eqp_meas).ports.first.port
    assert $sv.claim_process_lot(lots[0], eqp: @@eqp_meas, port: port, slr: true, nounload: true, claim_carrier: true,  nodcitems: true), "failed to process lot"
    assert_equal 0, $sv.eqp_port_status_change(@@eqp_meas, port, 'UnloadReq'), "failed to switch portstate to UnloadReq"
    # prepare 2nd carrier
    lot, carrier = lots[1], carriers[1]
    assert_equal 0, $sv.carrier_xfer_status_change(carrier, 'SI', $svtest.stocker), "failed to prepare foup"
    # SLR TakeOutIn
    cj = $sv.slr_takeoutin(@@eqp_meas, lot: lots[1], port: port)
    check_ei_control_job(@@eqp_meas, cj, lots[1])
  end

  def test50_notfound
    li = $sv.lot_info(@@lot)
    $sv.apc_speclimit(li.route, li.opNo, li.technology, li.productgroup, li.product, lot: '')
    assert_equal 0, $sv.rc
    $sv.apc_speclimit(li.route, li.opNo, li.technology, li.productgroup, li.product, lot: 'INVALID.01')
    assert_equal 0, $sv.rc
    $sv.apc_speclimit("", "", "", "", "", lot: 'INVALID.01')
    assert_equal 0, $sv.rc
    $sv.apc_speclimit("INVALID", "INVALID", "INVALID", "INVALID", "INVALID", lot: 'INVALID.01')
    assert_equal 1465, $sv.rc
  end

  #MSR1096451
  def test60_pcl_script
    assert lot = $svtest.new_lot(product: @@product2), "failed to create new lot"
    @@testlots << lot
    ["1100.1100", "1100.1200", "1100.1300", "1100.1400", "1100.1500"].each { |opNo|
      reset_scriptparameters(lot)
      assert_equal 0, $sv.lot_opelocate(lot, opNo), "failed to locate to operation"
      assert verify_scriptparameters(lot), "failed to verify scrip at #{opNo}"
    }
  end

  def reset_scriptparameters(lot)
    assert_equal 0, $sv.user_parameter_delete('Lot', lot, "UTUData")
  end

  def verify_scriptparameters(lot, params={})
    $log.info "#{__method__} #{lot}, #{params.inspect}"
    pre_ddc, pst_ddc = $sv.lot_deltadc_definitions(lot)
    cur_dcspec = nil
    pre_pds = pre_ddc.map { |op|
      # current DC spec
      cur_dcspec = op.postdcspec
      "#{op.preMeas_pd}:#{$sv.user_data_dcdef(op.deltadcdef)['MeasChainCategory']}"
    }
    pst_pds = pst_ddc.collect { |op|
      # current DC spec
      cur_dcspec = op.predcspec
      "#{op.preMeas_pd}:#{$sv.user_data_dcdef(op.deltadcdef)['MeasChainCategory']}"
    }

    # get current dcspec and tags
    smpl_udata = $sv.user_data_dcspec(cur_dcspec)
    # APC Speclimit tx does not work for process pds
    # using workaround
    if $sv.lot_info(lot).op == @@process_pd
      cur_dcspecitems = pst_ddc.first.predcitems
    else
      cur_dcspecitems = $sv.lot_apc_speclimit(lot).first.conditions.first.dcitems
    end
    # extract criticality
    criticality = Set.new
    custom_crit = Set.new
    cur_dcspecitems.each do |specitem|
      if /A:([A-Za-z1-9]+);/ === specitem.tag
        criticality << $1
      end
      if /B:([A-Za-z1-9]+);/ === specitem.tag
        custom_crit << $1
      end
    end
    res_str = "LSC=#{smpl_udata['MinLotSamplingCategory']} WSC=#{smpl_udata['MinWaferSamplingCategory']} SSC=#{smpl_udata['MinSiteSamplingCategory']}"
    res_str += " PreC=#{pre_pds.join(',')}"
    res_str += " PostC=#{pst_pds.join(',')}"
    res_str += " SCrit=#{criticality.to_a.join('')},#{custom_crit.to_a.join('')};"
    user_p = $sv.user_parameter "Lot", lot, name: "UTUData"
    ($log.error "script parameter not set for #{lot}"; return false) unless user_p
    verify_equal(res_str, user_p.value, "UTUData")
  end

  def check_limits_corresponding_ops_route_ope(route, opNo, product, dcspecid, ddcspecid, process: false)
    assert prod_info = $sm.object_info(:product, product).first, "failed to get product #{product}"
    pg = prod_info.specific[:productgroup]
    assert pg_info = $sm.object_info(:productgroup, pg).first, "failed to get product group #{pg}"
    tech = pg_info.specific[:technology]
    assert corresp_op = $sv.lot_corresponding_operations('', route: route, opNo: opNo, technology: tech, productgroup: pg, product: product), "failed to get corresponding operations"
    assert dcspecs = $sv.apc_speclimit(route, opNo, tech, pg, product).first, "failed to get speclimits for #{route}-#{opNo}"
    assert op = $sv.route_operations(route).find { |ope| ope.opNo == opNo }, "operation #{opNo} not found on #{route}"
    assert_equal op.op, dcspecs.op, "operation id should be returned" # changed in C7.5
    # first entry should be most specific one
    assert dcspecitems = dcspecs.conditions.first, "failed to get specitems for #{dcspecs.inspect}"
    # reservation to retrieve
    @@corresp_op[opNo].each_pair do |corres_opNo, dcspecgroup|
      if process
        assert corres_op = corresp_op.last.find { |op| op.opNo == corres_opNo }, "failed to get corresponding measurement operation #{corres_opNo}"
        assert dcspecs = $sv.apc_speclimit(route, corres_op.opNo, tech, pg, product).first, "failed to get speclimits for #{corres_op.opNo}"
        assert dcspecitems = dcspecs.conditions.first, "failed to get specitems for #{dcspecs.inspect}"
      else # measurement, todo: need fix for MES-3186
        assert corres_op = corresp_op.first.select { |op| op.opNo == corres_opNo }.last, "corresponding operation #{corres_opNo} not found"
      end
      assert_equal dcspecid, corres_op.dc_specid, "wrong dcspec (context: #{route}, #{opNo}, #{product}, #{pg}, #{tech})"
      assert_equal dcspecgroup, corres_op.dc_specgroup, "wrong dcspec group for #{corres_opNo} returned"
      # filter by specgroup or empty value
      dcspecitems_filtered = dcspecitems.dcitems.select { |dc| dc.dc_specgroup == dcspecgroup || dc.dc_specgroup == '' }
      dcspecitems_filtered = dcspecitems_filtered.select { |dc| @@meas_dcdefitems.include?(dc.name) }
      assert_equal(dcspecitems_filtered.sort { |x,y| x.name <=> y.name }, corres_op.dcitems.sort { |x,y| x.name <=> y.name }, "APC Speclimits do not match corresponding speclimits") unless process
      # check pre-measure operations
      # MES-3166 most specific one should be returned (first)
      ddcspec_pre, ddcspec_post = $sv.lot_deltadc_definitions('', route: route, opNo: opNo, technology: tech, productgroup: pg, product: product)
      assert ddcspec_pre.last, "failed to get delta dcs (pre)"
      assert_equal ddcspecid, ddcspec_pre.first.deltadcspecid, "wrong delta spec (#{route}, #{opNo}, #{tech}, #{pg}, #{product})"
    end
  end

  def _run_multilots(eqp, lots, separate: false)
    # process lot at pre-measurement
    lots.each do |lot|
      assert_equal 0, $sv.lot_opelocate(lot, @@premeas_chain.keys.first), "failed to locate to pre-measure"
    end
    2.times { assert $sv.claim_process_lot(lots, happylot: true), "failed to process at pre-measure op" }
    # call at post-measurement
    lots.each do |lot|
      assert_equal 0, $sv.lot_opelocate(lot, @@meas_opNo), "failed to locate to pre-measure"
    end
    if separate
      assert carrier = $svtest.get_empty_carrier(), "no empty carrier found"
      assert_equal 0, $sv.wafer_sort_req(lots.last, carrier), "failed to move lot to second carrier #{carrier}"
    end
    # call after reservation
    assert_equal 0, $sv.eqp_mode_change(eqp, 'Auto-1')
    assert cj = $sv.slr(eqp, lot: lots), "failed to reserve lot at #{eqp}"
    lots.each do |lot|
      check_ei_control_job(eqp, cj, lot)
    end
  end

  def check_limits_corresponding_ops(lot, opNo, has_dcspec: true, process: false, reserved: false)
    # no reservation yet
    assert corresp_op = $sv.lot_corresponding_operations(lot), "failed to get corresponding operations"
    assert dcspecs = $sv.lot_apc_speclimit(lot).first, "failed to get speclimits for #{opNo}"
    check_apc_speclimits(lot, dcspecs)
    linfo = $sv.lot_info(lot)
    if process
      assert_equal corresp_op.last.first.pd, dcspecs.op, "operation id should be returned" # changed in C7.5
    else
      assert_equal linfo.op, dcspecs.op, "operation id should be returned" # changed in C7.5
    end
    if has_dcspec
      # first entry should be most specific one
      assert dcspecitems = dcspecs.conditions.first, "failed to get specitems for #{dcspecs.inspect}"
    else
      assert dcspecs.conditions.empty?, "no dcspecs expected"
    end
    # reservation to retrieve
    @@corresp_op[opNo].each_pair do |corres_opNo, dcspecgroup|
      if process
        assert corres_op = corresp_op.last.find { |op| op.opNo == corres_opNo }, "failed to get corresponding measurement operation #{corres_opNo}"
        li = $sv.lot_info(lot)
        assert dcspecs = $sv.apc_speclimit(li.route, corres_op.opNo, li.technology, li.productgroup, li.product).first,
               "failed to get speclimits for #{corres_op.opNo}"
        assert dcspecitems = dcspecs.conditions.first, "failed to get specitems for #{dcspecs.inspect}"
      else # measurement
        assert corres_op = corresp_op.first.find { |op| op.opNo == corres_opNo }, "corresponding operation #{corres_opNo} not found"
      end
      assert_equal dcspecgroup, corres_op.dc_specgroup, "wrong dcspec group for #{corres_opNo} returned"
      # filter by specgroup or empty value
      dcspecitems_filtered = dcspecitems.dcitems.select { |dc| dc.dc_specgroup == dcspecgroup || dc.dc_specgroup == '' }
      dcspecitems_filtered = dcspecitems_filtered.select { |dc| @@meas_dcdefitems.include?(dc.name) } ##if reserved
      assert_equal(dcspecitems_filtered.sort { |x,y| x.name <=> y.name },
                   corres_op.dcitems.sort { |x,y| x.name <=> y.name },
                   "APC spec limits do not match corresponding spec limits") unless process
      # check premeasure operations
      if reserved
        li = $sv.lot_info(lot)
        ddcspecs_pre, ddcspecs_post = $sv.lot_deltadc_definitions(lot, route: li.route, opNo: li.opNo, product: li.product,
          productgroup: li.productgroup, technology: li.technology)
        assert ddcspecs_pre.count > 0, "failed to get delta dcs"
        ddcspecs_pre.each_with_index do |ddcspec, i|
          $log.info "#{@@premeas_chain[opNo]}, #{i}"
          op, pd = @@premeas_chain[opNo][i]
          assert_equal pd, ddcspec.preMeas_pd, "wrong pre-measurement pd"
          assert_equal op, ddcspec.preMeas_opNo, "wrong pre-measurement opNo"
        end
      end
    end
  end

  # apc speclimit tx should return dcspecs for all matching contexts
  def check_apc_speclimits(lot, dcspecs)
    assert dcspecs.count > 1, "not enough match dcspec contexts, at least 2 needed"
    linfo = $sv.lot_info(lot)
    dcspecs.conditions.each do |cond|
      assert [linfo.technology, '*'].include?(cond.technology), "wrong technology context"
      assert [linfo.productgroup, '*'].include?(cond.productgroup), "wrong product group context"
      assert [linfo.product, '*'].include?(cond.product), "wrong product context"
    end
  end

  def check_ei_control_job(eqp, cj, lot)
    carrier = $sv.lot_info(lot).carrier
    assert apc_dcspecs = $sv.lot_apc_speclimit(lot).first, "failed to get speclimits for #{lot}"
    assert dcspecitems = apc_dcspecs.conditions.first, "failed to get specitems for #{apc_dcspecs.inspect}"
    dcspecs = dcspecitems.dcitems.select { |dc| @@meas_dcdefitems.include?(dc.name) }.map { |dc| dc1 = dc.clone; dc1.dc_specgroup = ''; dc1 }
    ddcspecs_pre, ddcspecs_post = $sv.lot_deltadc_definitions(lot)
    assert ddcspecs_pre.last, "failed to get delta dcs (pre)"
    assert cj_info = $svtest.eis_rest.control_job_info(eqp, cj), "failed to get controljob info"
    assert car_cj_info = cj_info.find { |c_info| c_info['cassetteID']['identifier'] == carrier }, "failed to get reservation info for #{carrier}"
    assert lot_cj_info = car_cj_info['strLotInCassette'].find {|l_info| l_info["lotID"]["identifier"] == lot}, "failed to get reservation info for lot #{lot}"
    assert dc_defs = lot_cj_info['strStartRecipe']['strDCDef'], "dc defintions not found"
    dc_def = dc_defs.first
    assert_equal @@dcdef_meas, dc_def['dataCollectionDefinitionID']['identifier']
    dcitems_cj = dc_def['strDCItem'].collect { |dc|
      SiView::MM::DCItem.new(
        dc['dataCollectionItemName'], dc['dataCollectionMode'], dc['dataCollectionUnit'], dc['dataType'], dc['itemType'],
        dc['measurementType'], dc['waferID']['identifier'], dc['waferPosition'], dc['sitePosition'], dc['historyRequiredFlag'],
        dc['calculationType'], dc['calculationExpression'], dc['dataValue'], dc['targetValue'], dc['specCheckResult'], dc['actionCode']
    )}
    # check calculation expression
    @@dcitem_tags.each_pair do |k,v|
      assert dcitem = dcitems_cj.find { |dc| dc.name == k }, "failed to find DC Item #{k}"
      assert_equal v, dcitem.calc_expression, "wrong tag field"
    end
    # check dc specs
    dcspecs_cj = dc_def['strDCSpec'].collect { |dc|
      SiView::MM::DCSpecItem.new(dc['dataItemName'],
        dc['specLimitUpperRequired'] ? dc['specLimitUpper'] : nil, dc['target'],
        dc['specLimitLowerRequired'] ? dc['specLimitLower'] : nil, dc['actionCodes_usl'], dc['actionCodes_lsl'],
        dc['controlLimitUpperRequired'] ? dc['controlLimitUpper'] : nil,
        dc['controlLimitLowerRequired'] ? dc['controlLimitLower'] : nil, dc['actionCodes_ucl'], dc['actionCodes_lcl'],
        dc['screenLimitUpperRequired'] ? dc['screenLimitUpper'] : nil,
        dc['screenLimitLowerRequired'] ? dc['screenLimitLower'] : nil, dc['actionCodes_uscrn'], dc['actionCodes_lscrn'],
        dc['tag'], ''
      )
    }
    assert_equal dcspecs.sort { |x,y| x.name <=> y.name }, dcspecs_cj.sort { |x,y| x.name <=> y.name }, "failed to verify CJ items"
    # check delta dcs --> skipped if no premeasurement data was collected (MES-3095)
    ##ddcspecs.each_with_index do |ddcspec,i|
    ##  assert ddc_def = dc_defs.find { |dcd| dcd['strDCItem'].find { |dci| dci['dataCollectionItemName'] == ddcspec.dcitems.first.name } },
    ##    "failed to find #{ddcspec.dcitems.first.name} in CJ"
    ##  assert_equal @@premeas_op[i].op, ddc_def['previousOperationID']['identifier'], "wrong premeasurement pd"
    ##  assert_equal @@premeas_op[i].opNo, ddc_def['previousOperationNumber'], "wrong premeasurement opNo"
    ##end
  end


end
