=begin 
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.
    
Author: Steffen Steidten, 2010-05-03

History:
  2013-12-18 ssteidte,  use new SiView::XX instead of EasySiViewR
  2014-10-30 ssteidte,  renamed from Test110_Reticles to Test_SmartFacade
=end

require 'RubyTestCase'

# Test Reticle and ReticleGroup operations via SmartFacade
class Test_SmartFacade < RubyTestCase
    Description = "Test Reticle and ReticleGroup operations via SmartFacade"
    
    @@reticle = "TRTCL110"
    @@reticlegroup = "TRTGR110"
    @@test_inhibits = false
    
    def self.after_all_passed
      # delete reticle and reticlegroup
      $smf.delete(@@reticle, @@reticlegroup)
    end
    
    def self.startup
      super
      $smf = SiView::SMFacade.new($env)
    end
    
    
    def test01_create
      # ensure reticle and reticlegroup don't exist
      $smf.delete(@@reticle, @@reticlegroup)
      assert !$smf.reticle_exists?(@@reticle)
      assert !$smf.reticlegroup_exists?(@@reticlegroup)
      # create reticle and reticlegroup
      $smf.create(@@reticle, @@reticlegroup)
      assert $smf.reticle_exists?(@@reticle)
      assert $smf.reticlegroup_exists?(@@reticlegroup)
    end

    def test02_inhibit
      # inhibit reticle
      ($log.warn "skipped"; return) unless @@test_inhibits
      assert_equal 0, $sv.inhibit_entity("Reticle", @@reticle)
      assert_equal 1, $sv.inhibit_list("Reticle", @@reticle).size
      assert_equal 0, $sv.inhibit_cancel("Reticle", @@reticle)
      assert_equal 0, $sv.inhibit_list("Reticle", @@reticle).size
    end
    
    def test03_delete
      # ensure reticle and reticlegroup exist
      $smf.create(@@reticle, @@reticlegroup)
      assert $smf.reticle_exists?(@@reticle)
      assert $smf.reticlegroup_exists?(@@reticlegroup)
      # delete reticle and reticlegroup
      $smf.delete(@@reticle, @@reticlegroup)
      assert !$smf.reticle_exists?(@@reticle)
      assert !$smf.reticlegroup_exists?(@@reticlegroup)
    end
    
end
