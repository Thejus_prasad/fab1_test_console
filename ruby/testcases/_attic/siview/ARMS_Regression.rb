=begin 
Test the XM

(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.
    
Author: Daniel Steger, 2013-04-17
=end

require "RubyTestCase"
require 'siviewmmdb'
require 'dummytools'
require 'misc'

# Test Reticle Transport Functions.
# MSR661853
class ARMS_Regression < RubyTestCase
  Description = "Test ARMS reticle pod transports"
  
  # virtual env
  @@virtual_pods = "EM108H76 EM108H77"
  @@virtual_reticles = "RIPCORD0PPAZUVP1 OBANSK0QIAZUKP1"
  @@virtual_podstockers = "RET14W001 UTRPSTO110"
  @@virtual_brs = "UTBRS110"
  @@virtual_tools = "UTR001 ALC2300"
  @@virtual_ports = "RP1 RP2"
  @@virtual_timeout = 180
  
  # common
  @@use_emulators = "true"
  
  def self.startup    
    super
    $mmdb = SiView::MMDB.new($env, use_mds: true) unless $mmdb and $mmdb.env == $env
    $amhs = Dummy::AMHS.new($env) unless $amhs and $amhs.env == $env
    $eis = Dummy::EIs.new($env) unless $eis and $eis.env == $env
    @@resources = {}
    if @@use_emulators == "true"
      @@pod1, @@pod2 = @@virtual_pods.split
      @@reticle1, @@reticle2 = @@virtual_reticles.split
      @@podstocker1, @@podstocker2 = @@virtual_podstockers.split
      @@brs = @@virtual_brs
      @@eqp1, @@eqp2 = @@virtual_tools.split
      @@port1, @@port2 = @@virtual_ports.split
      @@timeout = @@virtual_timeout
    else
      @@pod1, @@pod2 = @@real_pods.split
      @@podstocker1, @@podstocker2 = @@real_podstockers.split
      @@brs = @@real_brs
      @@eqp1, @@eqp2 = @@real_tools.split
      @@port1, @@port2 = @@real_ports.split
      @@timeout = @@real_timeout
    end
    [@@eqp1, @@eqp2].each { |eqp| $eis.restart_tool(eqp) }
    
    $eis.restart_tool(@@brs)
    $log.info "use_emulators: #{@@use_emulators}"
    
  end
  
  def test00_prepare    
    assert_equal "1", $sv.environment_variable[1]["CS_AUTO_RTCLPOD_LOCATION_SYNC"], "incorrect value to test MSR553382"
    #assert_equal "1", $sv.environment_variable[1]["CS_BARE_RETICLE_STOCKER_FULL_ENABLED"], "incorrect value to test MSR663021"
    assert_equal "1", $sv.environment_variable[1]["CS_IGNORE_RSP_XFER_COMP_JOB"], "incorrect value to test MSR727822"
    
  end
  
  def test01_inventory_upload_brs
    # check BRS is in Online-Remote
    prepare_pod
    # Just a smoke test
    assert_equal 0, $sv.reticle_inventory_upload(stocker: @@brs), "failed to call inventory upload"
  end
  
  def test02_inventory_upload_stocker
    # check BRS is in Online-Remote
    prepare_pod
    # Just a smoke test
    assert_equal 0, $sv.reticle_pod_inventory_upload(@@podstocker2), "failed to call inventory upload"
  end
    
  def test03_inventory_upload_eqp
    # check reticle in eqp
    reticles = $sv.reticle_list(eqp: @@eqp1, reticleinfo: true)
    # remove on reticle from inventory in SiView
    reticle = reticles.find {|r| r.xfer_status == "EI"}.reticle
    assert reticle, "please move at least one reticle into #{@@eqp1}"
    assert $sv.reticle_xfer_status_change(reticle, "EO", :eqp=>@@eqp1), "failed to change xfer state"
    
    # inventory upload
    assert_equal 0, $sv.reticle_inventory_upload(eqp: @@eqp1), "failed to call inventory upload"
    
    assert_equal "EI", $sv.reticle_status(reticle).xfer_status, "reticle should be in tool"
  end
  
  def test10_pod_xfer_invalid_pod
    assert_equal 2823, $sv.reticle_pod_xfer_create("INVALID", @@podstocker1, "", @@eqp1, @@port1)
  end
      
  def test11_pod_xfer_stocker_to_tool    
    prepare_pod
    assert_equal 0, $sv.reticle_pod_xfer_create(@@pod1, @@podstocker1, "", @@eqp1, @@port1)
    assert verify_pod_xfer_to_target(@@pod1, @@eqp1)
    assert $sv.wait_carrier_xfer(@@pod1)
    assert verify_reticle_position(@@reticle1, eqp: @@eqp1, port: @@port1, pod: @@pod1)
    assert verify_rdj_complete(@@reticle1)
    
    # move to other equipment and port
    assert_equal 0, $sv.reticle_pod_unclamp_xfer_create(@@pod1, @@eqp1, @@port1, @@eqp2, @@port2)
    assert verify_pod_xfer_to_target(@@pod1, @@eqp2)
    assert $sv.wait_carrier_xfer(@@pod1)
    assert verify_reticle_position(@@reticle1, eqp: @@eqp2, port: @@port2, pod: @@pod1)
    assert verify_rdj_complete(@@reticle1)
    
    # return reticle to stocker (sitting on load port)
    assert_equal 0, $sv.async_reticle_xfer_create(@@reticle1, @@pod1, @@podstocker1)
    assert verify_pod_xfer_to_target(@@pod1, @@podstocker1)
    assert $sv.wait_carrier_xfer(@@pod1)
    assert verify_reticle_position(@@reticle1, stocker: @@podstocker1)
  end
 
  def test12_pod_xfer_stocker_to_stocker
    prepare_pod
    assert_equal 0, $sv.reticle_pod_xfer_create(@@pod1, @@podstocker1, "", @@podstocker2, "")
    assert verify_pod_xfer_to_target(@@pod1, @@podstocker2)
    assert $sv.wait_carrier_xfer(@@pod1)
    assert verify_reticle_position(@@reticle1, stocker: @@podstocker2, pod: @@pod1)
    assert verify_rdj_complete(@@reticle1)
  end

  def test13_pod_xfer_stocker_to_brs_tool
    prepare_pod
    assert_equal 0, $sv.reticle_pod_xfer_create(@@pod1, @@podstocker1, "", @@brs, @@port1)
    assert verify_pod_xfer_to_target(@@pod1, @@brs)
    assert $sv.wait_carrier_xfer(@@pod1)
    assert verify_reticle_position(@@reticle1, stocker: @@brs, port: @@port1, pod: @@pod1)
    assert verify_rdj_complete(@@reticle1)
    
    # to tool
    assert_equal 0, $sv.reticle_pod_unclamp_xfer_create(@@pod1, @@brs, @@port1, @@eqp1, @@port1)
    assert verify_pod_xfer_to_target(@@pod1, @@eqp1)
    assert $sv.wait_carrier_xfer(@@pod1)
    assert verify_reticle_position(@@reticle1, eqp: @@eqp1, port: @@port1, pod: @@pod1)
    assert verify_rdj_complete(@@reticle1)
        
    # return reticle to stocker (sitting on load port)
    assert_equal 0, $sv.async_reticle_xfer_create(@@reticle1, @@pod1, @@podstocker1)
    assert verify_pod_xfer_to_target(@@pod1, @@podstocker1)
    assert $sv.wait_carrier_xfer(@@pod1)
    assert verify_reticle_position(@@reticle1, stocker: @@podstocker1)
  end
  
  def test15_pod_xfer_stocker_to_tool_error  
    prepare_pod
    assert $amhs.set_variable("errormode", true) # Activate failure mode
    assert_equal 0, $sv.reticle_pod_xfer_create(@@pod1, @@podstocker1, "", @@eqp1, @@port1)
    assert verify_pod_xfer_to_target_in_error(@@pod1, @@eqp1, "2037", "DummyAMHS generated response|DummyAMHS errormodeactive")

    rdj = $sv.reticle_dispatch_job_list(pod: @@pod1).first
    rcj = $sv.reticle_component_job_list(rdj.job_id)[-1]
    assert $sv.reticle_component_job_skip(rcj.job_id, rcj.cjob_id), "failed to skip job"
    assert $amhs.set_variable("errormode", false) # Deactivate failure mode
    #
    assert verify_rdj_complete(@@reticle1)    
    assert verify_reticle_position(@@reticle1, eqp: @@eqp1, port: @@port1, pod: @@pod1)    
  end

  def test16_pod_xfer_stockout    
    prepare_pod
    assert_equal 0, $sv.reticle_pod_xfer_create(@@pod1, @@podstocker1, "", @@podstocker2, "P4")
    assert verify_pod_xfer_to_target(@@pod1, @@podstocker2)
    assert $sv.wait_carrier_xfer(@@pod1)
    assert verify_rdj_complete(@@reticle1)
    assert verify_reticle_position(@@reticle1, stocker: @@podstocker2, stockout: true, pod: @@pod1)    
  end

  def test17_pod_xfer_reroute_target
    prepare_pod
    assert_equal 0, $sv.reticle_pod_xfer_create(@@pod1, @@podstocker1, "", @@eqp1, @@port1)
    assert_equal 675, $sv.reticle_pod_xfer_create(@@pod1, @@podstocker1, "", @@eqp2, @@port2)
    assert verify_pod_xfer_to_target(@@pod1, @@eqp1)
    assert $sv.wait_carrier_xfer(@@pod1)
    assert verify_reticle_position(@@reticle1, eqp: @@eqp1, port: @@port1, pod: @@pod1)
    assert verify_rdj_complete(@@reticle1)
  end  

  def test18_pod_xfer_multi_target
    pend "will make the MM Server crash."
    prepare_pod
    assert_equal 0, $sv.reticle_pod_xfer_create(@@pod1, @@podstocker1, "", @@eqp2, @@port1)
    assert_equal 0, $sv.reticle_pod_xfer_create(@@pod2, @@podstocker1, "", @@eqp2, @@port2)
    assert verify_pod_xfer_to_target(@@pod1, @@eqp2)
    assert verify_pod_xfer_to_target(@@pod2, @@eqp2)
    assert $sv.wait_carrier_xfer(@@pod1)
    assert $sv.wait_carrier_xfer(@@pod2)
    assert verify_reticle_position(@@reticle1, eqp: @@eqp2, port: @@port1, pod: @@pod1)
    assert verify_rdj_complete(@@reticle1)
  end  

  
  def test20_reticle_xfer_invalid_id
    assert_equal 1488, $sv.async_reticle_xfer_create("INVALID", @@pod1, @@podstocker2)
  end
  
  def test21_reticle_xfer_rps_rps
    prepare_pod
    assert $sv.async_reticle_xfer_create(@@reticle1, @@pod1, @@podstocker2)
    assert verify_reticle_xfer_to_target(@@reticle1, @@podstocker2)
    assert $sv.wait_carrier_xfer(@@pod1)
    assert verify_reticle_position(@@reticle1, stocker: @@podstocker2)
  end
  
  def test22_reticle_xfer_src_equal_dst
    #prepare_resources
    prepare_pod
    assert_equal 2962, $sv.async_reticle_xfer_create(@@reticle1, @@pod1, @@podstocker1)
    rdj = $sv.reticle_dispatch_job_list(:reticle=>@@reticle1)
    assert_equal 0, rdj.count, "no reticle pod job expected: #{rdj}"
  end
       
  def test23_reticle_xfer_rps_tool_loaded_pod
    prepare_pod    
    _move_reticle(@@reticle1, @@pod1, from_stocker: @@podstocker1, to_eqp: @@eqp1, to_port: @@port1, retrieve: true)
    _move_reticle(@@reticle1, @@pod1, from_eqp: @@eqp1, from_port: @@port1, to_stocker: @@podstocker1)
  end
  
  def test24_reticle_xfer_rps_tool_unloaded_pod
    prepare_pod        
    _move_reticle(@@reticle1, @@pod1, from_stocker: @@podstocker1, to_eqp: @@eqp1, to_port: @@port1, retrieve: true)
    _move_pod(@@pod1, from_eqp: @@eqp1, from_port: @@port1, to_stocker: @@podstocker1)
    _move_reticle(@@reticle1, @@pod1, from_eqp: @@eqp1, to_stocker: @@podstocker1)
  end
    
  def test25_reticle_xfer_rps_brs_to_tool
    prepare_pod
    _move_reticle(@@reticle1, @@pod1, from_stocker: @@podstocker1, to_brs: @@brs, to_port: @@port1, retrieve: true)
    _move_pod(@@pod1, from_brs: @@brs, from_port: @@port1, to_stocker: @@podstocker1)
    _move_reticle(@@reticle1, @@pod1, from_brs: @@brs, from_port: @@port1, to_eqp: @@eqp2, to_port: @@port1, retrieve: true)
    _move_reticle(@@reticle1, @@pod1, from_eqp: @@eqp2, from_port: @@port1, to_stocker: @@podstocker1)
  end
  
  def test26_reticle_xfer_rps_tool_to_brs
    prepare_pod
    _move_reticle(@@reticle1, @@pod1, from_stocker: @@podstocker1, to_eqp: @@eqp2, to_port: @@port1, retrieve: true)
    _move_reticle(@@reticle1, @@pod1, from_eqp: @@eqp2, to_brs: @@brs, to_port: @@port1, retrieve: true)
    _move_pod(@@pod1, from_brs: @@brs, from_port: @@port1, to_stocker: @@podstocker1)
    _move_reticle(@@reticle1, @@pod1, from_brs: @@brs, from_port: @@port1, to_stocker: @@podstocker1)
  end

  def test27_reticle_xfer_stocker_to_tool_skip
    prepare_pod
    assert $amhs.set_variable("errormode", true) # Activate failure mode
    assert_equal 0, $sv.reticle_dispatch_job_insert(@@reticle1, @@pod1, @@podstocker1, @@eqp2, from_cat: "ReticlePod", to_cat: "Equipment")
    assert verify_pod_xfer_to_target_in_error(@@pod1, @@eqp2, "2037", "DummyAMHS generated response|DummyAMHS errormodeactive", to_port: @@port1)
    #
    rdj = $sv.reticle_dispatch_job_list(pod: @@pod1).first
    rcj = $sv.reticle_component_job_list(rdj.job_id).find {|crj| crj.status == "Error"}
    assert rcj, "expected component job in error, but found none"
    #
    assert $amhs.set_variable("errormode", false) # Deactivate failure mode
    assert $sv.reticle_component_job_skip(rcj.job_id, rcj.cjob_id), "failed to skip job"
    # Verify rest of job is executed
    assert verify_rdj_complete(@@reticle1)
    assert verify_reticle_position(@@reticle1, eqp: @@eqp2, port: @@port1, pod: @@pod1, retrieved: true)    
  end
  
  def test28_reticle_xfer_stocker_to_tool_retry
    prepare_pod
    assert $amhs.set_variable("errormode", true) # Activate failure mode
    assert_equal 0, $sv.reticle_dispatch_job_insert(@@reticle1, @@pod1, @@podstocker1, @@eqp1, from_cat: "ReticlePod", to_cat: "Equipment")
    assert verify_pod_xfer_to_target_in_error(@@pod1, @@eqp1, "2037", "DummyAMHS generated response|DummyAMHS errormodeactive", to_port: @@port1)
    #
    rdj = $sv.reticle_dispatch_job_list(pod: @@pod1).first
    rcj = $sv.reticle_component_job_list(rdj.job_id).find {|crj| crj.status == "Error"}
    assert rcj, "expected component job in error, but found none"
    #
    assert $amhs.set_variable("errormode", false) # Deactivate failure mode
    assert $sv.reticle_component_job_retry(rcj.job_id, rcj.cjob_id), "failed to retry job"    
    # Verify rest of job is executed
    assert verify_rdj_complete(@@reticle1)
    assert verify_reticle_position(@@reticle1, eqp: @@eqp1, port: @@port1, pod: @@pod1, retrieved: true)
  end
    
  def test29_reticle_xfer_dispatch_retry
    prepare_pod
    # provoke RDJ error by setting port state to 'LoadComp'
    assert_equal 0, $sv.eqp_rsp_port_status_change(@@eqp1, @@port1, "LoadComp"), "failed to prepare load port on #{@@eqp1}"
    assert_equal 0, $sv.reticle_dispatch_job_insert(@@reticle1, @@pod1, @@podstocker1, @@eqp1, from_cat: "ReticlePod", to_cat: "Equipment")
    assert verify_pod_dispatch_to_target_in_error(@@pod1, @@eqp1, to_port: @@port1)
    #
    assert_equal 0, $sv.eqp_rsp_port_status_change(@@eqp1, @@port1, "LoadReq"), "failed to prepare load port on #{@@eqp1}"
    job = $sv.reticle_dispatch_job_list(reticle: @@reticle1)
    assert_equal 0, $sv.reticle_dispatch_job_retry(job.first.job_id), "failed to retry dispatch job"
    # Verify rest of job is executed
    assert verify_reticle_xfer_to_target(@@reticle1, @@eqp1), "failed to get new component job"
    assert verify_rdj_complete(@@reticle1)
    assert verify_reticle_position(@@reticle1, eqp: @@eqp1, port: @@port1, pod: @@pod1, retrieved: true)    
  end
  
  def test35_pod_e84_reroute
    prepare_pod
    assert $amhs.set_variable("e84mode", true) # Activate E84 failure mode
    fallback_podstocker = $amhs.variable("fallbackreticlepodstocker")
    $sv.reticle_pod_xfer_create(@@pod1, @@podstocker1, "", @@eqp1, @@port1)
    assert verify_pod_xfer_to_target(@@pod1, @@eqp1), "job request failed to tool"
    assert wait_for(:timeout=>@@timeout, :sleeptime=>5) {
      $sv.reticle_pod_xfer_jobs(:to_stocker=>fallback_podstocker)
    }, "reroute to pod stocker failed"
    assert $sv.wait_carrier_xfer(@@pod1)
    assert verify_reticle_position(@@reticle1, stocker: fallback_podstocker)
    # no destination information anymore
    assert wait_for { verify_pod_eqp_rsv(@@pod1, @@eqp1, @@port1) }, "no data expected"
  end
  
  def test36_pod_xfer_delete
    prepare_pod
    fallback_podstocker = $amhs.variable("fallbackreticlepodstocker")
    assert_equal 0, $sv.reticle_pod_xfer_create(@@pod1, @@podstocker1, "", @@eqp1, @@port1), "failed to create transfer"
    assert verify_pod_xfer_to_target(@@pod1, @@eqp1), "job request failed to tool"
    assert_equal 0, $sv.reticle_pod_xfer_job_delete(@@pod1), "failed to delete pod transfer"
    #
    assert verify_rdj_complete(@@reticle1), "job is not completed"
    assert verify_reticle_position(@@reticle1, stocker: fallback_podstocker)
    # no destination information anymore
    assert wait_for { verify_pod_eqp_rsv(@@pod1, @@eqp1, @@port1) }, "no data expected"
  end
  
  def test37_pod_manual_stockin_while_transfering
    prepare_pod    
    $sv.reticle_pod_xfer_create(@@pod1, @@podstocker1, "", @@eqp1, @@port1)
    assert verify_pod_xfer_to_target(@@pod1, @@eqp1), "job request failed to tool"
    #Manual Stock in
    assert_equal 0, $sv.reticle_pod_xfer_status_change(@@pod1, "MI", stocker: @@podstocker2), "failed to do manual stockin"
    #
    assert_equal 0, $sv.reticle_dispatch_job_list(:pod=>@@pod1).count, "RDJ should be canceled"
    
    assert wait_for { verify_pod_eqp_rsv(@@pod1, @@eqp1, @@port1) }, "no data expected"
  end
  
  def test38_pod_manual_stockin_on_eqp
    prepare_pod    
    $sv.reticle_pod_xfer_create(@@pod1, @@podstocker1, "", @@eqp1, @@port1)
    assert verify_pod_xfer_to_target(@@pod1, @@eqp1), "job request failed to tool"
    assert verify_rdj_complete(@@reticle1), "job is not completed"
    assert verify_reticle_position(@@reticle1, eqp: @@eqp1, port: @@port1, pod: @@pod1), "pod is not on #{@@eqp1}"
    
    #Manual Stock in
    assert_equal 0, $sv.reticle_pod_xfer_status_change(@@pod1, "MI", stocker: @@podstocker2), "failed to do manual stockin"
    assert wait_for { verify_pod_eqp_rsv(@@pod1, @@eqp1, @@port1) }, "no data expected"
  end
  
  # Bare Reticle Stockers are not yet supported
  def XXXtest39_pod_manual_stockin_on_brs
    pend "No recovery supported"
    prepare_pod    
    $sv.reticle_pod_xfer_create(@@pod1, @@podstocker1, "", @@brs, @@port1)
    assert verify_pod_xfer_to_target(@@pod1, @@brs), "job request failed to tool"
    assert verify_rdj_complete(@@reticle1), "job is not completed"
    assert verify_reticle_position(@@reticle1, stocker: @@brs, port: @@port1, pod: @@pod1), "pod is not on #{@@brs}"
    
    #Manual Stock in
    assert_equal 0, $sv.reticle_pod_xfer_status_change(@@pod1, "MI", stocker: @@podstocker2), "failed to do manual stockin"
    assert wait_for { verify_pod_eqp_rsv(@@pod1, @@brs, @@port1) }, "no data expected"
  end
  
  def test40_multi_reticle_xfer
    pend "will make the MM Server crash."
    prepare_pod
    {@@reticle1=>@@pod1, @@reticle2=>@@pod2}.each_pair do |reticle, pod|
      assert_equal 0, $sv.reticle_dispatch_job_insert(reticle, pod, @@podstocker1, @@eqp2, from_cat: "ReticlePod", to_cat: "Equipment")      
    end
    [@@reticle1, @@reticle2].each {|reticle|
      assert verify_reticle_xfer_to_target(reticle, @@eqp2)
    }
    
    assert $sv.wait_carrier_xfer([@@pod1, @@pod2])
    assert verify_rdj_complete(@@reticle1), "reticle dispatch job is not completed"
    assert verify_rdj_complete(@@reticle2), "reticle dispatch job is not completed"
  end
  
  def test50_reticle_xfer_cancel_reroute
    prepare_pod        
    $sv.reticle_pod_xfer_create(@@pod1, @@podstocker1, "", @@eqp1, @@port1)
    assert verify_pod_xfer_to_target(@@pod1, @@eqp1), "job request failed to tool"
    rdj = $sv.reticle_dispatch_job_list(pod: @@pod1).first.job_id
    assert_equal 0, $sv.reticle_dispatch_job_cancel(rdj), "failed to cancel job"
    $sv.reticle_dispatch_job_insert(@@reticle1, @@pod1, "", @@eqp2, to_cat: "Equipment")
    assert verify_pod_xfer_to_target(@@pod1, @@eqp2), "job request failed to tool"
    assert verify_rdj_complete(@@reticle1), "reticle dispatch job is not completed"
  end
  
  # aux methods
  
  # move reticle to pod
  def prepare_pod
    $setup_ok = false
    assert $amhs.register
    assert $amhs.set_variable("e84mode", false) # E84 failure mode should be deactivated
    assert $amhs.set_variable("errormode", false) 
    
    {@@reticle1=>@@pod1, @@reticle2=>@@pod2}.each_pair do |reticle, pod|
      rdjs = $sv.reticle_dispatch_job_list(:reticle=>reticle)
      rdjs.each {|rdj| $sv.reticle_dispatch_job_cancel(rdj.job_id)}
      _rinfo = $sv.reticle_status(reticle)    
      $sv.wait_carrier_xfer([pod])
      if @@use_emulators == "true"       
        if _rinfo.xfer_status == "EI"
          _pod = (_rinfo.pod == "") ? pod : _rinfo.pod
          #
          eqp = _rinfo.eqp
          _einfo = $sv.eqp_info(eqp)        
          rport = _einfo.reticleports.find {|rp| rp.loaded_pod == _pod}
          #switch tool too offline for recovery
          $sv.eqp_mode_offline1(eqp)
          if rport
            rport = rport.port
          else          
            _pinfo = $sv.reticle_pod_status(_pod)
            if _pinfo.xfer_status =~ /[MS]I/
              $sv.reticle_pod_xfer_status_change_new(_pod, "SO", @@podstocker1)
            end
            assert_equal 0, $sv.reticle_pod_offline_load(eqp, @@port1, _pod), "cleanup failed: failed to load pod"
            rport = @@port1
          end        
          if _einfo.reticles.find {|r| r.reticle == reticle}          
            $sv.reticle_offline_retrieve(eqp, rport, _pod, reticle)
          end
          $sv.eqp_rsp_port_manual(@@eqp1, rport)
          assert_equal 0, $sv.reticle_pod_offline_unload(eqp, rport, _pod), "cleanup failed: please recover manually."
        end
        _rinfo = $sv.reticle_status(reticle)
        if _rinfo.pod != pod
          if _rinfo.pod != ""
            $sv.reticle_justinout(reticle, _rinfo.pod, 1, "out")
          end
          assert_equal 0, $sv.reticle_justinout(reticle, pod, 1, "in")
        end

        if _rinfo.xfer_status =~ /[MSHE]O/ or _rinfo.stocker != @@podstocker1
          # automatically clean up strange carrier states
          $sv.reticle_pod_xfer_status_change_new(pod, "MI", @@podstocker1, :manualin=>true)
        end
      end
    end
    # switch eqp to Auto-1 and Reticle Port to Auto
    [@@eqp1, @@eqp2].each do |eqp|
      e_info = $sv.eqp_info(eqp)
      assert_equal 0, $sv.eqp_mode_auto1(eqp)
      e_info.reticleports.each do |rport|
        $sv.eqp_rsp_port_status_change(eqp, rport.port, "LoadReq")
        $sv.eqp_rsp_port_auto(eqp, rport.port) unless rport.mode == "Auto"
      end
    end
    # switch brs to Auto-1 and Reticle Port to Auto
    b_info = $sv.brs_info(@@brs)
    assert_equal 0, $sv.brs_mode_change(@@brs, "On-Line Remote") unless b_info.online_mode == "On-Line Remote"
    b_info.reticleports.each do |rport|
      $sv.eqp_rsp_port_status_change(@@brs, rport.port, "LoadReq")
      $sv.eqp_rsp_port_auto(@@brs, rport.port) unless rport.mode == "Auto"
    end
    $setup_ok = true
  end
  
  def set_podstockers(reticle)
    # swap source and target stockers depending on current carrier position
    rstat = $sv.reticle_status(reticle)
    @@podstocker1, @@podstocker2 = @@podstocker2, @@podstocker1 if rstat.stocker == @@podstocker2
    assert verify_reticle_position(reticle, stocker: @@podstocker1), "wrong position: #{rstat.inspect}"
  end
    
  def _find_category(prefix, params)
    key = params.keys.find {|k| k =~ /^#{prefix}/}    
    case key
    when /stocker$/
      [params[key], "ReticlePod", {stocker: params[key]}]
    when /brs$/
      [params[key], "BareReticle", {stocker: params[key], port: params["#{prefix}_port".to_sym]}]
    when /eqp$/
      [params[key], "Equipment", {eqp: params[key], port: params["#{prefix}_port".to_sym]}]
    else
      [key, "Unknown"]
    end
  end

  def _move_reticle(reticle, pod, params)
    from_eqp, from_cat, from_pars = _find_category("from", params)
    to_eqp, to_cat, to_pars = _find_category("to", params)
    $log.info "> transfer reticle #{reticle} in #{pod} from #{from_eqp} (#{from_cat}) -> #{to_eqp} (#{to_cat})"
    $sv.reticle_dispatch_job_insert(reticle, pod, from_eqp, to_eqp, from_cat: from_cat, to_cat: to_cat)
    assert verify_reticle_xfer_to_target(reticle, to_eqp)
    assert $sv.wait_carrier_xfer(pod)
    assert verify_rdj_complete(reticle), "reticle dispatch job is not completed"
    #
    if params[:retrieved]
      assert $sv.reticle_pod_info(pod).first.isempty, "pod should be empty"
      to_pars.merge!({:retrieved=>true})
    end
    assert verify_reticle_position(reticle, to_pars.merge({pod: pod})), "invalid reticle location"
  end
  
  def _move_pod(pod, params={})    
    from_eqp, from_cat, from_pars = _find_category("from", params)
    to_eqp, to_cat, to_pars = _find_category("to", params)
    $log.info "> transfer pod #{pod} from #{from_eqp} (#{from_cat}) -> #{to_eqp} (#{to_cat})"    
    if from_cat == "ReticlePod"
      assert $sv.reticle_pod_xfer_create(pod, from_eqp, (from_pars[:port] or ""), to_eqp, (to_pars[:port] or ""))
    else
      assert $sv.reticle_pod_unclamp_xfer_create(pod, from_eqp, from_pars[:port], to_eqp, (to_pars[:port] or ""))
    end
    assert verify_pod_xfer_to_target(pod, to_eqp)
    assert $sv.wait_carrier_xfer(pod)
    #
    if params[:reticle]
      assert verify_reticle_position(params[:reticle], to_pars.merge({pod: pod}))
      assert verify_rdj_complete(params[:reticle])
    end
  end
  
  def verify_reticle_position(reticle, params)
    res = true
    rstat = $sv.reticle_status(reticle)
    if params[:stocker]
      res &= verify_equal params[:stocker], rstat.stocker, "reticle #{reticle} is not in stocker #{params[:stocker]}"
      if params[:stockout]
        res &= verify_equal("MO", rstat.xfer_status, "reticle #{reticle} is not stocked out")
      else
        res &= verify_condition("transfer status of reticle #{reticle} is not SI/MI/HI") {
          ["SI", "HI", "MI"].member?(rstat.xfer_status)
        }
      end
      _brsinfo = $sv.brs_info(params[:stocker]) if params[:port] # only Bare Reticle Stocker have managed ports
      res &= verify_condition("pod not loaded") {
        _rpinfo = _brsinfo.reticleports.find {|rp| rp.port == params[:port]}
        (_rpinfo && (params[:pod] == _rpinfo.loaded_pod))
      } if params[:port]
      res &= verify_condition("reticle in equipment") {
        _brsinfo.reticles.find {|r| r.reticle == reticle }
      } if params[:retrieved]      
    elsif params[:eqp]
      res &= verify_equal params[:eqp], rstat.eqp, "reticle #{reticle} is not in eqp #{params[:eqp]}"
      res &= verify_equal "EI", rstat.xfer_status, "transfer status of reticle #{reticle} is not EI"      
      _einfo = $sv.eqp_info(params[:eqp])
      res &= verify_condition("pod not loaded") {
        _rpinfo = _einfo.reticleports.find {|rp| rp.port == params[:port]}
        (_rpinfo && (params[:pod] == _rpinfo.loaded_pod))
      }
      res &= verify_condition("reticle in equipment") {
        _einfo.reticles.find {|r| r.reticle == reticle }
      } if params[:retrieved]
    end
    return res
  end
  

  def verify_no_pod_xfer_job(pod, mcs=false)
    jobs = $sv.carrier_xfer_jobs(pod, mcs: mcs)
    assert jobs.size == 0, "lingering transfer job for pod #{pod}"
  end

  def verify_rdj_complete(reticle)
    wait_for {
      rdjs = $sv.reticle_dispatch_job_list(:reticle=>reticle)
      rdjs.count == 0
    }
  end
  
  # Check reticle dispatch job and component job to target for reticle
  def verify_reticle_xfer_to_target(reticle, target)
    rdjs = $sv.reticle_dispatch_job_list(:reticle=>reticle)
    res = verify_equal(1, rdjs.count, "expected RDJ, got #{rdjs.inspect}", true)
    
    job = rdjs.first
    res &= verify_equal(target, job.to_eqp, "wrong target for RDJ: #{job}")
    res &= wait_for(:timeout=>@@timeout) {
      rdjs = $sv.reticle_dispatch_job_list(:reticle=>reticle)
      if rdjs.count > 0 && rdjs[0].status == "Error"
        $log.error "error in RDJ #{rdjs[0]}"
        return false
      else         
        _params = rdjs[0].to_cat == "Equipment" ? {:to_eqp=>target} : {:to_stocker=>target}
        $sv.reticle_pod_xfer_jobs(_params)
      end
    }
    # check MMDB reticle xfer job information
    sleep 10
    $log.info "  #{job.inspect}"
    data = $mmdb.reticle(reticle)
    if job.to_cat == "Equipment"
      res &= verify_equal(target, data.xfer_dest_eqp_id, "wrong destination eqp")    
    elsif job.to_cat == "BareReticle"
      res &= verify_equal(target, data.xfer_dest_stk_id, "wrong destination stocker")
    end
    verify_equal($sv.user, data.xfer_rsv_user_id, "wrong user")
    return res
  end

  # Check reticle dispatch job and component job to target for reticle pod
  def verify_pod_xfer_to_target(pod, target)
    rdjs = $sv.reticle_dispatch_job_list(pod: pod)
    res = verify_equal(1, rdjs.count, "expected RDJ, got #{rdjs.inspect}", true)    
    job = rdjs.first
    res &= verify_equal(target, job.to_eqp, "wrong target for RDJ: #{job}")
    res &= wait_for(:timeout=>@@timeout) {
      rdjs = $sv.reticle_dispatch_job_list(pod: pod)
      if rdjs.count > 0 && rdjs[0].status == "Error"
        $log.error "error in RDJ #{rdjs[0]}"
        return false
      else         
        _params = rdjs[0].to_cat == "Equipment" ? {:to_eqp=>target} : {:to_stocker=>target}
        jobs = $sv.reticle_pod_xfer_jobs(_params)
        (jobs && jobs.count > 0 && jobs[0].carrierjobs.find {|c| c.pod == pod})
      end
    }
    # check MMDB reticle xfer job information
    sleep 10
    $log.info "  #{job.inspect}"
    data = $mmdb.reticle_pod(pod)
    if job.to_cat == "Equipment"
      res &= verify_equal(target, data.xfer_dest_eqp_id, "wrong destination eqp")    
    else
      res &= verify_equal(target, data.xfer_dest_stk_id, "wrong destination stocker")
    end
    verify_equal($sv.user, data.xfer_rsv_user_id, "wrong user")
    return res
  end
  
  def verify_pod_dispatch_to_target_in_error(pod, target, params={})
    wait_for(timeout: @@timeout) { 
      rdjs = $sv.reticle_dispatch_job_list(pod: pod)
      res = verify_equal(1, rdjs.count, "expected RDJ, got #{rdjs.inspect}", true)    
      job = rdjs.first
      res &= verify_equal(target, job.to_eqp, " wrong target for RDJ: #{job}")
      res &= verify_equal("RDJDispatchError", job.status, " #{job} should be in error")
      if res
        if params[:to_port] # assume target is an equipment
          _einfo = $sv.eqp_info(target)
          rport = _einfo.reticleports.find {|rp| rp.port == params[:to_port]}
          res &= verify_equal("RDJ_ERR", rport.err_info, " error state for #{params[:to_port]} missing")
        end
      end
      res
    }
  end
  
  def verify_pod_xfer_to_target_in_error(pod, target, rc, reason, params={})
    wait_for(timeout: @@timeout) { 
      rdjs = $sv.reticle_dispatch_job_list(pod: pod)
      res = verify_equal(1, rdjs.count, "expected RDJ, got #{rdjs.inspect}", true)    
      job = rdjs.first
      res &= verify_equal(target, job.to_eqp, " wrong target for RDJ: #{job}")
      res &= verify_equal("Error", job.status, " #{job} should be in error")
      if res
        cjob = $sv.reticle_component_job_list(job.job_id).find {|c| c.status == "Error"}
        res &= verify_equal(rc, cjob.rc, " expected error in #{cjob}")
        res &= verify_equal(reason, cjob.reason, " expected reason in #{cjob}")
        if params[:to_port] # assume target is an equipment
          _einfo = $sv.eqp_info(target)
          rport = _einfo.reticleports.find {|rp| rp.port == params[:to_port]}
          res &= verify_equal("RDJ_RCJ_ERR", rport.err_info, " error state for #{params[:to_port]} missing")
        end
      end
      res
    }
  end
  
  def verify_pod_eqp_rsv(pod, eqp, port)
    _einfo = $sv.eqp_info(eqp)
    rport = _einfo.reticleports.find {|rp| rp.port == port}
    res = verify_condition("No pod on #{@@eqp1} expected, but found #{rport.inspect}") {
      rport && rport.loaded_pod == "" && rport.rsv_pod == ""
    }
    data = $mmdb.reticle_pod(pod)
    res &= verify_equal(nil, data.xfer_dest_eqp_id, "wrong destination eqp")    
    res &= verify_equal(nil, data.xfer_rsv_user_id, "wrong user")
    return res
  end
end
