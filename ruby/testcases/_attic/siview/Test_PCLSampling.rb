=begin 
Test PCL Applications (Toolwish, Lotguard, Sampling)

(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.
    
Author: Daniel Steger, 2013-01-23

=end

require "RubyTestCase"

# Test PCL Applications (Toolwish, Lotguard, Sampling)
class Test_PCLSampling < RubyTestCase
  Description = "Test PCL Applications (Toolwish, Lotguard, Sampling)"
  
  @@product = 'UT-PROD-SAMPLING.01'
  @@route = 'UTRT-SAMPLING.01'
  @@list_spec = "M07-00020089"
  @@list_proj = "ITDC_PROTO"
  @@lot = "8XYK04004.000"
  @@list_smpl_rules = {
    "1000.1500" => ["1000.1500",nil],
    "1000.2000" => ["1000.4500",nil],
    "1000.5000" => ["1000.6000",nil],
    "1000.7000" => ["1000.7000",nil],
    "1000.8000" => ["1000.8000",nil]
    }
    
  @@stocker = "UTSTO11"
      
  def self.create_lot
    $sv.new_lot_release :product=>@@product, :route=>@@route, :stb=>true, :waferids=>"t7m12"
  end
  
  def setup
    puts "\n"
    $setup_ok = ($setup_ok != false)
    assert $setup_ok, "test setup is not correct"
  end
  
  def test01_setup
    $setup_ok = false
          
    # unload loaded carrier
    li = $sv.lot_info @@lot

    if not li.xfer_status.match(/[M|S]I/)
      $sv.carrier_xfer_status_change li.carrier, "SI", @@stocker
    end

    if li.eqp != ""
      eqi = $sv.eqp_info(li.eqp)
      eqi.ports.each {|p| assert $sv.eqp_unload(li.eqp, p.port, p.loaded_carrier) if p.loaded_carrier != ""}
      eqi.ib.loaded_carriers.each {|c| assert $sv.eqp_unload(li.eqp, eqi.ports[0].port, c)} if eqi.ib
    end

    assert $sv.user_parameter_delete_verify("Lot", @@lot, "SkipListSampling")    
    assert $sv.lot_opelocate @@lot, "first", :force=>(li.status == "ONHOLD")
    assert $sv.lot_hold_release(@@lot, nil), "cannot release hold"
    assert $sv.merge_lot_family(@@lot)
    assert $sv.lot_experiment_delete(@@lot)
    # move lots to current point PD
    assert $sv.lot_opelocate(@@lot, "first")
      
    $setup_ok = true
  end
  
  def test10_list_sampling
    assert $sv.user_parameter_set_verify("Lot", @@lot, "SkipListSampling", "#{@@list_spec},#{@@list_proj}")
    @@list_smpl_rules.each_pair do |op,data|
      assert $sv.lot_opelocate(@@lot, op), "No error expected"
      lot_info = $sv.lot_info(@@lot)
      nop, rule = data
      assert_equal nop, lot_info.opNo, "Expected operation is #{nop}, but was #{lot_info.opNo}"
    end
  end
  
  def est99_delete_lots
    $rtd.close if $rtd
  end
end