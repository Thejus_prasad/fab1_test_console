=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: Daniel Steger, 2014-02-25 created
=end

require "RubyTestCase"
require 'dummytools'

# MSR816231
class Test151_IBXferStateEI < RubyTestCase
  Description = "Take out/in reservation test"

  @@proc_eqp = "UTI004"
  @@tw_opno = "100.200"
  @@tw_product = "UT-MN-UTI004.01"
  @@tw_startbank = "UT-RAW"
  @@carrier = "UT%"
  @@stocker = "UTSTO11"
  @@hold_reason = "AEHL"

  def self.startup
    super
    $eis = Dummy::EIs.new($env) unless $eis
    $eis.restart_tool(@@proc_eqp)
    @@lots = []
  end

  def self.after_all_passed
    $sv.eqp_cleanup(@@proc_eqp)
    @@lots.each {|l| $sv.delete_lot_family(l)}
  end

  def setup
    $eis.set_variable(@@proc_eqp, "slrdelaytime", 30000)
  end

  def teardown
    $eis.set_variable(@@proc_eqp, "slrdelaytime", 0)
  end

  def test00_setup
    assert_equal "0", $sv.environment_variable[1]["SP_LOT_OPERATION_EI_CHECK"], "Xfer state EI check should be disabled"
    $sv.eqp_cleanup(@@proc_eqp)

    # TODO: Multi Recipe Check
  end

  def test01_control_stb
    lot = $sv.stb_controllot(@@tw_product, "Equipment Monitor")
    @@lots << lot
    assert lot, "error creating test wafer lot"
    prepare(@@proc_eqp, lot, @@tw_opno)
    lc = $sv.lot_split(lot, 10)
    assert lc, "error splitting lot #{lot}"
    # bankin lot for STB
    assert_equal 0, $sv.lot_opelocate(lot, "last"), "failed to move lot to last operation"
    $sv.lot_bankin(lot)
    assert_equal 0, $sv.lot_bank_move(lot, @@tw_startbank), "failed to prepare lot for stb"

    # issue SLR
    t = Thread.new {
      assert $sv.slr(@@proc_eqp, lot: lc, port: "P1"), "failed to complete reservation"
    }
    sleep 10
    stb_lot = $sv.stb_controllot(@@tw_product, "Equipment Monitor", srclot: lot, nwafers: 15)
    assert stb_lot, "failed to STB new lot"
    @@lots << stb_lot
    t.join
    carrier = $sv.lot_info(lc).carrier
    assert_equal 0, $sv.carrier_xfer_status_change(carrier, "EO", @@proc_eqp)
    assert_equal 0, $sv.eqp_port_status_change(@@proc_eqp, "P1", "LoadComp")
    assert_equal 0, $sv.eqp_load(@@proc_eqp, "P1", carrier), "failed to load foup"
    assert 10.times.inject(true) {|res,i| res &= ($sv.eqp_info(@@proc_eqp) != nil)},"failed to do equipment info"
  end

  def test02_split
    lot = $sv.stb_controllot(@@tw_product, "Equipment Monitor")
    @@lots << lot
    assert lot, "error creating test wafer lot"
    prepare(@@proc_eqp, lot, @@tw_opno)
    lc = $sv.lot_split(lot, 10)
    assert lc, "error splitting lot #{lot}"

    # issue SLR
    t = Thread.new {
      assert $sv.slr(@@proc_eqp, lot: lc, port: "P1"), "failed to complete reservation"
    }
    sleep 10
    child = $sv.lot_split(lot, 5)
    assert child, "failed to split new lot"
    @@lots << child
    t.join
    carrier = $sv.lot_info(lc).carrier
    assert_equal 0, $sv.carrier_xfer_status_change(carrier, "EO", @@proc_eqp)
    assert_equal 0, $sv.eqp_port_status_change(@@proc_eqp, "P1", "LoadComp")
    assert_equal 0, $sv.eqp_load(@@proc_eqp, "P1", carrier), "failed to load foup"
    assert 10.times.inject(true) {|res,i| res &= ($sv.eqp_info(@@proc_eqp) != nil)},"failed to do equipment info"
  end

  def test03_splitonhold
    lot = $sv.stb_controllot(@@tw_product, "Equipment Monitor")
    @@lots << lot
    assert lot, "error creating test wafer lot"
    prepare(@@proc_eqp, lot, @@tw_opno)
    lc = $sv.lot_split(lot, 10)
    assert lc, "error splitting lot #{lot}"

    assert_equal 0, $sv.lot_hold(lot, @@hold_reason), "failed to put lot onhold"

    # issue SLR
    t = Thread.new {
      assert $sv.slr(@@proc_eqp, lot: lc, port: "P1"), "failed to complete reservation"
    }
    sleep 10
    child = $sv.lot_split(lot, 5, onhold: true)
    assert child, "failed to split new lot"
    @@lots << child
    t.join
    carrier = $sv.lot_info(lc).carrier
    assert_equal 0, $sv.carrier_xfer_status_change(carrier, "EO", @@proc_eqp)
    assert_equal 0, $sv.eqp_port_status_change(@@proc_eqp, "P1", "LoadComp")
    assert_equal 0, $sv.eqp_load(@@proc_eqp, "P1", carrier), "failed to load foup"
    assert 10.times.inject(true) {|res,i| res &= ($sv.eqp_info(@@proc_eqp) != nil)},"failed to do equipment info"
  end


  def test04_merge
    lot = $sv.stb_controllot(@@tw_product, "Equipment Monitor")
    @@lots << lot
    assert lot, "error creating test wafer lot"
    prepare(@@proc_eqp, lot, @@tw_opno)
    lc = $sv.lot_split(lot, 10)
    assert lc, "error splitting lot #{lot}"

    # split second lot
    lc2 = $sv.lot_split(lot,5)

    # issue SLR
    t = Thread.new {
      assert $sv.slr(@@proc_eqp, lot: lc, port: "P1"), "failed to complete reservation"
    }
    sleep 10
    assert_equal 0, $sv.lot_merge(lot, lc2), "failed to merge lots"
    t.join
    carrier = $sv.lot_info(lc).carrier
    assert_equal 0, $sv.carrier_xfer_status_change(carrier, "EO", @@proc_eqp)
    assert_equal 0, $sv.eqp_port_status_change(@@proc_eqp, "P1", "LoadComp")
    assert_equal 0, $sv.eqp_load(@@proc_eqp, "P1", carrier), "failed to load foup"
    assert 10.times.inject(true) {|res,i| res &= ($sv.eqp_info(@@proc_eqp) != nil)},"failed to do equipment info"
  end

  def prepare(eqp, lot, opno)
    # Cleanup eqp
    assert_equal 0, $sv.eqp_cleanup(eqp, mode: 'Auto-3'), "failed to set tool to Auto-3"
    # Lots
    li = $sv.lot_info lot
    $sv.pp_force_delete("", lot)
    $sv.lot_bankin_cancel lot if li.status == "COMPLETED"
    $sv.lot_nonprobankout lot if li.status == "NonProBank"
    $sv.lot_hold_release(lot, nil)
    $sv.merge_lot_family(lot, :nosort=>true)
    assert_equal 0, $sv.carrier_xfer_status_change(li.carrier, "SI", @@stocker)
    assert_equal 0, $sv.lot_opelocate(lot, opno), "failed to locate #{lot} to operation"
  end
end
