=begin 
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.
    
Author: Daniel Steger

History
  2016-02-03 dsteger,  added branch cancel cases

TODO: add branch qtimes on module PDs
=end

require "SiViewTestCase"

# Q-time tests for branch routes
class Test603_QTimeOnBranch < SiViewTestCase  
  @@eqp = "UTC003"
  @@product = "UT-PROD-QTIME.09"
  @@erfroutes1 = ["UTBR-QTIME.01", "UTBR-QTIME.02", "UTBR-QTIME.03"]
  @@erf_split1 = '1000.1200'
  @@erf_join1 = '1000.1400'
  @@trigger1 = '1000.1100'
  @@target1 = '1000.1300'
  @@erf_targets1 = ['2000.1200', '2000.1000', '2000.1300']
  @@erf_triggers1 = ['2000.1200', '2000.1300', '2000.1000']
  @@trigger2 = '1000.1300'
  @@target2 = '1000.1500'
      
  @@erfroutes2 = ["UTBR-QTIME.01", "UTBR-QTIME.02"]
  @@erf_split2 = '1000.1400'
  @@erf_join2 = '1000.1500'  
  
  @@erfroute3 = 'UTBR-QTIME.01'
  @@erf_split3 = '1000.1100'
  @@erf_join3 = '1000.1600'
  @@erfroute4 = 'UTBR-QTIME.04'
  @@erf_split4 = '2000.1100'
  @@erf_join4 = '2000.1300'
  @@erf_trigger3 = 'e1000.1500'
  @@erf_target3 = '2000.1300'
  
  @@erfroute5 = 'd-QTIME.01'
  
  @@rwroutes1 = ["UTRW-QTIME.01", "UTRW-QTIME.02"]
  @@rw_opNo1 = '1000.1200'
  @@rw_join1 = '1000.1400'
  @@rw_targets1 = ['2100.0900', '2100.1200']
  @@rw_triggers1 = ['2100.1200', '2100.0900']
  
  @@rwroutes2 = ["UTRW-QTIME.01", "UTRW-QTIME.02"]
  @@rw_opNo2 = '1000.1500'
  @@rw_join2 = '1000.1500'
  
  @@rwroute3 = "UTRW-QTIME.03"
  @@rw_opNo3 = '1000.1500'
  @@rw_join3 = '1000.1600'
  
  @@epsilon = 25
  @@qtime_hold = 'QEXR'
  @@qtime_expires = 185
  @@stop_outof_qzone = 'ON'
  
  def self.after_all_passed
    $sv.delete_lot_family(@@lot)
  end
  
  setup_test
  def test00_setup
    assert @@lot = $svtest.new_lot(product: @@product), "failed to prepare lot"  
    assert $sv.lot_cleanup(@@lot, nosort: true)
    assert_equal @@stop_outof_qzone, $sv.environment_variable[1]["CS_SP_STOP_QACTION_OUTOF_QZONE"], "Variable should be set"
  end
  
  def setup
    if self.class.class_variable_defined?('@@lot')      
      assert $sv.merge_lot_family(@@lot)
      assert $sv.lot_cleanup(@@lot, nosort: true)
    end
  end
  
  def test10_qtime_on_branch_override
    # setup experiment
    assert_ok $sv.lot_experiment(@@lot, ['01', '02','03'].zip, @@erfroutes1, @@erf_split1, @@erf_join1), "failed to setup experiment for #{@@lot}"
    # start qtime
    locate_process(@@lot, @@trigger1, @@trigger1, @@target1)
    # split
    assert_ok $sv.lot_opelocate(@@lot, @@erf_split1), "failed to locate to split operation"
    
    children = $sv.lot_family(@@lot) - [@@lot]    
    children.each do |lc|
      assert i = @@erfroutes1.find_index { |b| b == $sv.lot_info(lc, operation_info: true).route}, "#{lc} not found on child branch"            
      erf_trigger = @@erf_triggers1[i]
      erf_target = @@erf_targets1[i]
      check_qtime(lc, @@trigger1, erf_target)      
      if i == 0
        # check the branch cancel works correctly
        assert_ok $sv.lot_branch_cancel(lc), "failed to cancel branch for #{lc}"
        check_qtime(lc, @@trigger1, @@target1, has_qtime: true)
        assert_ok $sv.lot_branch(lc, @@erfroutes1.first, return_opNo: @@erf_join1), "failed to branch #{lc}"
        check_qtime(lc, @@trigger1, erf_target)
        # trigger and branch at same process step
        locate_process(lc, erf_target, @@trigger1, erf_target, overlap_target_opNo: @@target2)
        assert_equal 112, $sv.lot_branch_cancel(lc), "cancel branch for #{lc} should fail"
      elsif i == 1
        # target before trigger
        locate_process(lc, erf_target, @@trigger1, erf_target)
        locate_process(lc, erf_trigger, erf_trigger, @@target2)
      else
        # trigger before target
        locate_process(lc, erf_trigger, @@trigger1, erf_target, overlap_target_opNo: @@target2)
        locate_process(lc, erf_target, erf_trigger, @@target2)
      end
      assert_ok $sv.lot_opelocate(lc, @@erf_join1), "failed to locate #{lc} to split operation"
      check_qtime(lc, erf_trigger, @@target2)
    end
    #
    locate_process(@@lot, @@target1, @@trigger1, @@target1, overlap_target_opNo: @@target2)    
    assert_ok $sv.lot_opelocate(@@lot, @@erf_join1), "failed to locate to split operation"    
    #
    children.each { |lc| check_automerge(@@lot, lc) }
    locate_process(@@lot, @@target2, @@trigger2, @@target2)
  end

  def test11_qtime_branch_delete_reset
    # setup experiment
    assert_ok $sv.lot_experiment(@@lot, ['04', '05'].zip, @@erfroutes2, @@erf_split2, @@erf_join2), "failed to setup experiment for #{@@lot}"
    # start qtime
    locate_process(@@lot, @@trigger2, @@trigger2, @@target2)
    sleep 60
    # split
    assert_ok $sv.lot_opelocate(@@lot, @@erf_split2), "failed to locate to split operation"
    
    children = $sv.lot_family(@@lot) - [@@lot]    
    children.each do |lc|
      assert i = @@erfroutes2.find_index { |b| b == $sv.lot_info(lc, operation_info: true).route}, "#{lc} not found on child branch"
      if i==0
        # delete (triggered after processing at first PD
        assert $sv.claim_process_lot(lc, eqp: @@eqp, slr: true, claim_carrier: true), "failed to process #{lc}"
        check_qtime(lc, @@trigger2, @@target2, active: false, has_qtime: false)
        assert_ok $sv.lot_opelocate(lc, @@erf_join2), "failed to locate #{lc} to join operation"
        check_qtime(lc, @@trigger2, @@target2, active: false, has_qtime: false)
      else
        #reset
        trigger = $sv.lot_info(lc, operation_info: true).opNo
        assert $sv.claim_process_lot(lc, eqp: @@eqp, slr: true, claim_carrier: true), "failed to process #{lc}"
        lc_remain_time = check_qtime(lc, trigger, @@target2).first.remain_time
        lot_remain_time = check_qtime(@@lot, @@trigger2, @@target2).first.remain_time
        assert_in_delta lc_remain_time, lot_remain_time + 60, 35, "#{lc}'s remaining qtime was not reset"
        assert_ok $sv.lot_opelocate(lc, @@erf_join2), "failed to locate #{lc} to join operation"
        check_qtime(lc, trigger, @@target2)
      end      
    end
    #
    assert_ok $sv.lot_opelocate(@@lot, @@erf_join2), "failed to locate to join operation"    
    #
    children.each { |lc| check_automerge(@@lot, lc) }
    locate_process(@@lot, @@target2, @@trigger2, @@target2)
  end
  
  def test12_qtime_on_branch
    # setup experiment
    assert_ok $sv.lot_experiment(@@lot, ['01', '02','03'], @@erfroute3, @@erf_split3, @@erf_join3), "failed to setup experiment for #{@@lot}"
    assert_ok $sv.lot_experiment(@@lot, ['01', '02','03'], @@erfroute4, @@erf_split4, @@erf_join4, split_route: @@erfroute3, original_opNo: @@erf_split3), "failed to setup experiment for #{@@lot}"
    # split
    assert_ok $sv.lot_opelocate(@@lot, @@erf_split3), "failed to locate to split operation"
    assert child = $sv.lot_family(@@lot)[-1], "failed to branch child"
    assert_ok $sv.lot_opelocate(child, @@erf_split4), "failed to locate #{child} to branch to branch operation"
    locate_process(child, @@erf_trigger3, @@erf_trigger3, @@erf_target3)
    # locate child to join
    assert_ok $sv.lot_opelocate(child, @@erf_join4), "failed to locate #{child} to join operation"
    check_qtime(child, @@erf_trigger3, @@erf_target3)
    locate_process(child, @@erf_target3, @@erf_trigger3, @@erf_target3)
    assert_ok $sv.lot_opelocate(child, @@erf_join3), "failed to locate #{child} to join operation"
    assert_ok $sv.lot_opelocate(@@lot, @@erf_join3), "failed to locate #{@@lot} to join operation"
    check_automerge(@@lot, child)
  end
  
  def test13_qtime_branch_to_subroute
    # setup experiment
    assert_ok $sv.lot_experiment(@@lot, 25, @@erfroutes2[1], @@erf_split2, @@erf_join2), "failed to setup experiment for #{@@lot}"
    # start qtime
    locate_process(@@lot, @@trigger2, @@trigger2, @@target2)
    sleep 60    
    lot_remain_time = check_qtime(@@lot, @@trigger2, @@target2).first.remain_time
    # branch
    ##assert_ok $sv.lot_opelocate(@@lot, @@erf_split2), "failed to locate to split operation"    
    #reset
    trigger = $sv.lot_info(@@lot, operation_info: true).opNo
    assert $sv.claim_process_lot(@@lot, eqp: @@eqp, slr: true, claim_carrier: true), "failed to process #{@@lot}"
    lc_remain_time = check_qtime(@@lot, trigger, @@target2).first.remain_time
    assert_in_delta lc_remain_time, lot_remain_time + 60, 35, "#{@@lot}'s remaining qtime was not reset"
    assert_ok $sv.lot_opelocate(@@lot, @@erf_join2), "failed to locate #{@@lot} to join operation"
    check_qtime(@@lot, trigger, @@target2)    
    locate_process(@@lot, @@target2, trigger, @@target2)
  end
  
  def test14_action_expire_on_branch
    # setup experiment
    assert_ok $sv.lot_experiment(@@lot, ['05', '06'].zip, @@erfroutes1[1..2], @@erf_split1, @@erf_join1), "failed to setup experiment for #{@@lot}"
    # start qtime
    locate_process(@@lot, @@trigger1, @@trigger1, @@target1)
    # split
    assert_ok $sv.lot_opelocate(@@lot, @@erf_split1), "failed to locate to split operation"
    
    lots = $sv.lot_family(@@lot)
    lots.all_parallel do |lc|
      i = @@erfroutes1.find_index { |b| b == $sv.lot_info(lc, operation_info: true).route}
      if i
        erf_trigger = @@erf_triggers1[i]
        erf_target = @@erf_targets1[i]
      else
        erf_trigger = @@trigger1
        erf_target = @@target1
      end
      check_qtime(lc, @@trigger1, erf_target)
      if i == 1
        # target before trigger
        ##locate_process(lc, erf_target, @@trigger1, erf_target)
        wait_for_expire(lc, @@trigger1, erf_target)
      elsif i == 2
        # trigger before target
        locate_process(lc, erf_trigger, @@trigger1, erf_target, overlap_target_opNo: @@target2)
        wait_for_expire(lc, @@trigger1, erf_target)
      else
        wait_for_expire(lc, @@trigger1, @@target1)
      end
    end    
    $sv.merge_lot_family(@@lot)
  end
  
  def wait_for_expire(lot, trigger_opNo, target_opNo)
    $log.info "wait until qtime to expire (#{@@qtime_expires}s)..."; sleep @@qtime_expires    
    assert wait_for(timeout: 2*@@epsilon) {
      linfo = $sv.lot_info(lot)
      qinfo = $sv.lot_qtime_list(lot)
      qtime = qinfo.find {|q| q.route == linfo.route && q.opNo == linfo.opNo}
      qdetails = qtime.qt_details
      assert qdetails = qdetails.find { |d| d.trigger_opNo == trigger_opNo && d.target_opNo == target_opNo }, 
        "Qtime for #{lot} [#{trigger_opNo}, #{target_opNo}]"
      (!$sv.lot_info(lot).qtime && qdetails.remain_time < 0)
    }, "qtime not expired for #{lot} after #{@@qtime_expires + 2*@@epsilon}s"
    assert_equal 1, $sv.lot_hold_list(lot, reason: @@qtime_hold).count, "expected hold for #{lot}"
  end
  
  def test20_qtime_on_rework
    # setup rework
    assert_ok $sv.lot_cleanup(@@lot)
    # start qtime
    locate_process(@@lot, @@trigger1, @@trigger1, @@target1)
    # rework
    assert_ok $sv.lot_opelocate(@@lot, @@rw_opNo1), "failed to locate to rework operation"
    
    assert_ok child = $sv.lot_partial_rework(@@lot, 5, @@rwroutes1[0], return_opNo: @@rw_join1), "failed to do partial rework for #{@@lot}"    
    assert_ok $sv.lot_rework(@@lot, @@rwroutes1[1], return_opNo: @@rw_join1), "failed to setup rework for #{@@lot}"
    
    [child, @@lot].each_with_index do |lot, i|
      rw_trigger = @@rw_triggers1[i]
      rw_target = @@rw_targets1[i]
      check_qtime(lot, @@trigger1, rw_target)
      if i == 0
        # target before trigger
        locate_process(lot, rw_target, @@trigger1, rw_target)
        locate_process(lot, rw_trigger, rw_trigger, @@target2)
      else
        # trigger before target
        locate_process(lot, rw_trigger, @@trigger1, rw_target, overlap_target_opNo: @@target2)
        locate_process(lot, rw_target, rw_trigger, @@target2)
      end
    end
    #
    check_automerge(@@lot, child)
    locate_process(@@lot, @@target2, @@trigger2, @@target2)
  end
  
  def test21_qtime_rework_delete_reset
    # setup rework
    assert_ok $sv.lot_cleanup(@@lot)
    # start qtime
    locate_process(@@lot, @@trigger2, @@trigger2, @@target2)
    # rework
    assert_ok $sv.lot_opelocate(@@lot, @@rw_opNo2), "failed to locate to rework operation"
    
    assert_ok child = $sv.lot_partial_rework(@@lot, 5, @@rwroutes2[0], return_opNo: @@rw_join2), "failed to do partial rework for #{@@lot}"    
    assert_ok $sv.lot_rework(@@lot, @@rwroutes2[1], return_opNo: @@rw_join2, release: @@rw_opNo2 == @@rw_join2), "failed to setup rework for #{@@lot}"
    
    [child, @@lot].each_with_index do |lot, i|
      check_qtime(lot, @@trigger2, @@target2, has_qtime: true)
      if i == 0
        # delete (triggered after processing at first PD
        assert $sv.claim_process_lot(lot, eqp: @@eqp, slr: true, claim_carrier: true), "failed to process #{lot}"
        check_qtime(lot, @@trigger2, @@target2, active: false, has_qtime: false)
        assert_ok $sv.lot_opelocate(lot, @@rw_join2), "failed to locate #{lot} to join operation"
        check_qtime(lot, @@trigger2, @@target2, active: false, has_qtime: false)
      else
        #reset
        $log.info "sleep 60s..."; sleep 60
        lot_remain_time = check_qtime(@@lot, @@trigger2, @@target2).first.remain_time
        trigger = $sv.lot_info(lot, operation_info: true).opNo
        assert $sv.claim_process_lot(lot, eqp: @@eqp, slr: true, claim_carrier: true), "failed to process #{lot}"
        reset_remain_time = check_qtime(lot, trigger, @@target2).first.remain_time
        
        assert_in_delta reset_remain_time, lot_remain_time + 60, 30, "#{lot}'s remaining qtime was not reset"
        assert_ok $sv.lot_opelocate(lot, @@rw_join2), "failed to locate #{lot} to join operation"
        check_qtime(lot, trigger, @@target2)
      end
    end
    #
    assert_ok $sv.lot_hold_release(child, nil), "release child's rework hold"
    assert_ok $sv.lot_merge(@@lot, child), "failed to merge lots"
    locate_process(@@lot, @@target2, @@trigger2, @@target2)
  end
  
  def test30_branch_within_qtime
    # setup experiment after trigger before target
    assert_ok $sv.lot_experiment(@@lot, ['05', '06','07'], @@erfroute5, @@erf_split1, @@target1, dynamic: true), "failed to setup experiment for #{@@lot}"
    # start qtime
    locate_process(@@lot, @@trigger1, @@trigger1, @@target1)
    # split
    assert_ok $sv.lot_opelocate(@@lot, @@erf_split1), "failed to locate to split operation"
    
    lc = $sv.lot_family(@@lot).last
    check_qtime(lc, @@trigger1, @@target1, has_qtime: true)
    assert_ok $sv.lot_opelocate(lc, @@target1), "failed to locate #{lc} to split operation"
    check_qtime(lc, @@trigger1, @@target1, has_qtime: true)
    #
    assert_ok $sv.lot_opelocate(@@lot, @@target1), "failed to locate to split operation"    
    #
    check_automerge(@@lot, lc) 
  end
  
  def test31_branch_outof_qtime
    # setup experiment after trigger before target
    assert_ok $sv.lot_experiment(@@lot, ['05', '06','07'], @@erfroute5, @@erf_split1, @@target2, dynamic: true), "failed to setup experiment for #{@@lot}"
    # start qtime
    locate_process(@@lot, @@trigger1, @@trigger1, @@target1)
    # split
    assert_ok $sv.lot_opelocate(@@lot, @@erf_split1), "failed to locate to split operation"
    
    lc = $sv.lot_family(@@lot).last
    assert_equal @@erfroute5, $sv.lot_info(lc).route, "child #{lc} is not on branch route"
    check_qtime(lc, @@trigger1, @@target1, has_qtime: false)
    assert_ok $sv.lot_branch_cancel(lc), "failed to cancel branch for #{lc}"
    check_qtime(lc, @@trigger1, @@target1, has_qtime: true)
    assert_ok $sv.lot_branch(lc, @@erfroute5, return_opNo: @@target2, dynamic: true), "failed to branch #{lc}"
    check_qtime(lc, @@trigger1, @@target1, has_qtime: false)
    assert $sv.claim_process_lot(lc, eqp: @@eqp, mode: 'Auto-1', slr: true, claim_carrier: true), "failed to process #{lc}"
    check_qtime(lc, @@trigger1, @@target1, active: false, has_qtime: false)
    assert_ok $sv.lot_opelocate(lc, @@target2), "failed to locate #{lc} to split operation"
    check_qtime(lc, @@trigger1, @@target1, active: false, has_qtime: false)
    #
    assert_ok $sv.lot_opelocate(@@lot, @@target2), "failed to locate to split operation"    
    #
    check_automerge(@@lot, lc)
    locate_process(@@lot, @@target2, @@trigger2, @@target2)  
  end
  
  def test40_qtime_on_rework_outside
    # setup rework
    assert_ok $sv.lot_cleanup(@@lot)
    # start qtime
    locate_process(@@lot, @@trigger2, @@trigger2, @@target2)
    # rework
    assert_ok $sv.lot_opelocate(@@lot, @@rw_opNo3), "failed to locate to rework operation"
    
    assert_ok $sv.lot_rework(@@lot, @@rwroute3, return_opNo: @@rw_join3), "failed to setup rework for #{@@lot}"
    check_qtime(@@lot, @@trigger2, @@target2, has_qtime: false)
    assert_ok $sv.lot_rework_cancel(@@lot), "failed to cancel rework"
    check_qtime(@@lot, @@trigger2, @@target2, has_qtime: true)
    locate_process(@@lot, @@target2, @@trigger2, @@target2)
  end
    
  
  # locate to operation and process to trigger qtime action (trigger or target)
  def locate_process(lot, opNo, trigger_opNo, target_opNo, overlap_target_opNo: nil, eqp: @@eqp)
    assert_ok $sv.lot_opelocate(lot, opNo), "failed to locate to target operation"
    assert $sv.claim_process_lot(lot, eqp: eqp, mode: 'Auto-1', slr: true, claim_carrier: true), "failed to process #{lot}"
    active = opNo != target_opNo
    check_qtime(lot, trigger_opNo, target_opNo, active: active, has_qtime: active || !overlap_target_opNo.nil?)
    if overlap_target_opNo
      check_qtime(lot, opNo, overlap_target_opNo)
    end
  end
  
  def check_qtime(lot, trigger_opNo, target_opNo, active: true, has_qtime: true)
    linfo = $sv.lot_info(lot)
    assert_equal(has_qtime, linfo.qtime, "wrong qtime state for #{lot}")
    return unless active
    qinfo = $sv.lot_qtime_list(lot)
    assert qtime = qinfo.find {|q| q.route == linfo.route && q.opNo == linfo.opNo}, "failed to get qtime for #{lot} [#{trigger_opNo}, #{target_opNo}]"
    qdetails = qtime.qt_details
    qdetails = qdetails.find_all { |d| d.trigger_opNo == trigger_opNo && d.target_opNo == target_opNo }
    assert_equal(1, qdetails.count, "expected one qtime for #{lot} [#{trigger_opNo}, #{target_opNo}]")    
    return qdetails
  end

  # merge check for different fabs used by multiple tests
  def check_automerge(lot, child)
    if @@sv.f7 && !child.nil? # check for automerge
      assert_equal 'EMPTIED', @@sv.lot_info(child).status, "#{child} should have been merged with parent automatically"
    else
      # merge any remaining child (even if nil given)
      assert_equal 0, @@sv.lot_merge(lot, child), "failed to merge #{child} to #{lot}"
    end
  end

end
