=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: Steffen Steidten, 2011-01-05

History:
  2015-05-20 ssteidte,  separated MSR256958 (xfer status EI) into Test125_ReserveLotEI; verified with R10
  2015-06-16 dsteger,   merged from Fab1QA, extended to cover dispatch flags, NPW for internal buffer
=end

require 'SiViewTestCase'


# SLR and NPW reserve for carriers in SO, MSR135069 and MSR849535
#  SLR for lots on tools, MSR256958
#
# http://myteamsgf/sites/Fab8CIM/CIMEng/FAB8CIMQA/Test%20Plans/SiView/SiView%20Reservations.xlsm
class Test124_ReserveCarrier < SiViewTestCase
  @@eqp = 'UTC001'
  @@eqp_ib = 'UTI001'     # requires dummy buffer
  @@eqp_other = 'UTF001'  # other equipment with e2e disabled
  @@interbay = 'OHT01'
  @@stocker2 = 'UTSTO120'
  @@shelf = "UTSHF01"
  @@opNo = '1000.700'
  @@opNo_ib = '1000.300'
  @@modes = ['Auto-1', 'Semi-1', 'Auto-2', 'Semi-2', 'Auto-3']


  def self.shutdown
    $dummyamhs.set_variable('intrabaytime', 30000)
    $dummyamhs.set_variable('norerouteondelete', false)
  end

  def self.after_all_passed
    $sv.delete_lot_family(@@lot)
    $sv.delete_lot_family(@@dummy_lot)
    $svtest.cleanup_carrier(@@carrier)
  end


  def teardown
    [@@eqp,  @@eqp_ib, @@eqp_other].each {|eqp| assert_equal 0, $sv.eqp_cleanup(eqp)}
  end


  def test00_setup
    $setup_ok = false
    #
    assert_equal '1', $sv.environment_variable[1]['SP_REROUTE_XFER_FLAG'], "setting doesn't support reroute for xfer_state SO"
    $dummyamhs = Dummy::AMHS.new($env)
    $dummyamhs.set_variable('intrabaytime', 45000)
    $dummyamhs.set_variable('norerouteondelete', true)
    # cleanup eqps
    [@@eqp,  @@eqp_ib, @@eqp_other].each {|eqp|
      assert !$sv.eqp_info(eqp).e2e, "equipment2Equipment flag should not be set for #{eqp}"
      assert $svtest.cleanup_eqp(eqp)
    }
    assert_equal 0, $sv.eqp_mode_change(@@eqp_other, 'Off-Line-1'), "failed to set #{@@eqp_other} offline"
    einfo = $sv.eqp_info(@@eqp_ib)
    assert einfo.ib && einfo.ib.buffers['Filler Dummy Lot'] > 0, "internal buffer tool #{@@eqp_ib} should have dummy buffer spaces"
    # create test lots
    assert @@lot = $svtest.new_lot
    @@carrier = $sv.lot_info(@@lot).carrier
    assert @@dummy_lot = $svtest.new_monitor_lot('Dummy', bankin: true), 'failed to prepare dummy lot'
    @@dummy_carrier = $sv.lot_info(@@dummy_lot).carrier
    #
    $setup_ok = true
  end


  # generated test cases
  ['SI', 'MI', 'SO', 'EO'].each_with_index do |xfer_state, i|
    #
    method = "test%02d_fb_slr_#{xfer_state}" % (11+i)
    send(:define_method, method) {scenario_slr_auto(xfer_state, @@eqp, @@opNo)}
    #
    method = "test%02d_fb_npw_#{xfer_state}" % (21+i)
    send(:define_method, method) {scenario_npw_auto(xfer_state, @@eqp)}
    #
    method = "test%02d_ib_slr_#{xfer_state}" % (31+i)
    send(:define_method, method) {scenario_slr_auto(xfer_state, @@eqp_ib, @@opNo_ib)}
    #
    method = "test%02d_ib_npw_#{xfer_state}" % (41+i)
    send(:define_method, method) {scenario_npw_auto(xfer_state, @@eqp_ib)}
  end

  ['HO', 'HI', 'MO', 'SO', 'EO', 'EI'].each_with_index do |xfer_state, i|
    #
    method = "test%02d_fb_slr_#{xfer_state}" % (51+i)
    send(:define_method, method) {scenario_slr_manual(xfer_state, @@eqp, @@opNo)}
    #
    method = "test%02d_fb_npw_#{xfer_state}" % (61+i)
    send(:define_method, method) {scenario_npw_manual(xfer_state, @@eqp)}
    #
    method = "test%02d_ib_slr_#{xfer_state}" % (71+i)
    send(:define_method, method) {scenario_slr_manual(xfer_state, @@eqp_ib, @@opNo_ib)}
    #
    method = "test%02d_ib_npw_#{xfer_state}" % (81+i)
    send(:define_method, method) {scenario_npw_manual(xfer_state, @@eqp_ib)}
  end


  # this is just a support script for manual tests
  def testmanual99_manualtest
    $dummyamhs = Dummy::AMHS.new($env)
    $dummyamhs.set_variable('norerouteondelete', true)
    $dummyamhs.set_variable('intrabaytime', 60000)
    eqp = @@eqp
    port = $sv.eqp_info(eqp).ports.first.port
    ##@@lot = 'UXYM26005.000'
    @@carrier = 'ACEY'
    ##opNo = '1000.100'
    @@modes.each do |mode|
      $log.info "--- mode #{mode}"
      ['HO', 'HI', 'SI', 'MI', 'MO', 'SO', 'EO', 'EI'].each do |xfer_state|
        ##assert_equal 0, $sv.lot_cleanup(@@lot, opNo:opNo)
        assert_equal 0, $sv.eqp_cleanup(eqp, mode: mode)
        clocation = prepare_carrier(@@carrier, xfer_state)
        puts "Please do reservation for #{xfer_state} - #{mode}"
        gets
      end
    end
  end

  def scenario_slr_auto(xfer_state, eqp, opNo)
    assert_equal 0, $sv.lot_cleanup(@@lot, opNo: opNo)
    port = $sv.eqp_info(eqp).ports.first.port
    @@modes.each do |mode|
      $log.info "--- mode #{mode}"
      clocation = prepare_carrier(@@carrier, xfer_state)
      assert_equal 0, $sv.eqp_cleanup(eqp, mode: mode)
      # SLR
      stime = Time.now
      assert cj = $sv.slr(eqp, lot: @@lot)
      assert $sv.eqp_info(eqp).rsv_carriers.include?(@@carrier)
      if mode =~ /-[23]$/ # check dispatch flags
        assert_equal 0, $sv.carrier_xfer([@@carrier], clocation, "", eqp, port, multicarrier: true)
        assert verify_dispatch_data(eqp, port, @@carrier, "Dispatched", stime)
        stime = Time.now
        assert_equal 0, $sv.slr_cancel(eqp, cj), "failed to cancel job"
        assert verify_dispatch_data(eqp, port, "", "Required", stime)
        assert_equal 0, $sv.carrier_xfer_jobs_delete(@@carrier)
      else
        assert_equal 0, $sv.slr_cancel(eqp, cj)
      end
    end
  end

  def scenario_npw_auto(xfer_state, eqp)
    einfo = $sv.eqp_info(eqp)
    port = einfo.ports.first.port
    @@modes.each do |mode|
      $log.info "--- mode #{mode}"
      clocation = prepare_carrier(@@dummy_carrier, xfer_state)
      assert_equal 0, $sv.eqp_cleanup(eqp, mode: mode)
      # NPW
      stime = Time.now
      purpose = einfo.ib ? 'Filler Dummy Lot' : 'Other'
      assert_equal 0, $sv.npw_reserve(eqp, carrier: @@dummy_carrier, purpose: purpose)
      assert verify_dispatch_data(eqp, port, @@dummy_carrier, "Dispatched", stime)
      stime = Time.now
      assert_equal 0, $sv.npw_reserve_cancel(eqp, carrier: @@dummy_carrier)
      assert verify_dispatch_data(eqp, port, "", "Required", stime)
    end
  end


  def scenario_slr_manual(xfer_state, eqp, opNo)
    assert_equal 0, $sv.lot_cleanup(@@lot, opNo: opNo)
    einfo = $sv.eqp_info(eqp)
    port = einfo.ports.first.port
    @@modes.each do |mode|
      $log.info "--- mode #{mode}"
      prepare_carrier(@@carrier, xfer_state, no_reroute: true, other_eqp: @@eqp_ib)
      assert_equal 0, $sv.eqp_cleanup(eqp, mode: mode)
      # SLR
      cj = $sv.slr(eqp, lot: @@lot)
      if mode =~ /-[23]$/ && !(mode == 'Auto-3' && xfer_state == 'SO' && einfo.ib)
        # reservation fails in auto-mode, but not for SO and internal buffer
        assert_equal ($sv.release == 10) ? 0 : 905, $sv.rc, "reservation should have failed"
      else
        assert cj, "reservation should have succeeded"
        assert $sv.eqp_info(eqp).rsv_carriers.include?(@@carrier)
        assert_equal 0, $sv.slr_cancel(eqp, cj)
      end
    end
  end

  def scenario_npw_manual(xfer_state, eqp)
    einfo = $sv.eqp_info(eqp)
    port = einfo.ports.first.port
    @@modes.each do |mode|
      $log.info "--- mode #{mode}"
      clocation = prepare_carrier(@@dummy_carrier, xfer_state)
      assert_equal 0, $sv.eqp_cleanup(eqp, mode: mode)
      # NPW
      stime = Time.now
      purpose = einfo.ib ? 'Filler Dummy Lot' : 'Other'
      rc = $sv.npw_reserve(eqp, carrier: @@dummy_carrier, purpose: purpose)
      if mode =~ /-[23]$/ && !['SO','EO'].member?(xfer_state) # reservation fails in auto-mode, but not for SO,EO
        assert_equal 905, rc, "reservation should have failed"
      else
        assert_equal 0, rc, "reservation should have succeeded"
        assert verify_dispatch_data(eqp, port, @@dummy_carrier, "Dispatched", stime)
        stime = Time.now
        assert_equal 0, $sv.npw_reserve_cancel(eqp, carrier: @@dummy_carrier)
        assert verify_dispatch_data(eqp, port, "", "Required", stime)
      end
    end
  end

  def verify_dispatch_data(eqp, port, carrier, disp_state, stime)
    $log.info "verify_dispatch_data #{eqp.inspect}, #{port.inspect}, #{carrier.inspect}, #{disp_state.inspect}, #{stime.inspect}"
    ret = true
    assert einfo = $sv.eqp_info(eqp), "failed to get equipment info for #{eqp}"
    pinfo = einfo.ports.find {|p| p.port == port}
    ret &= verify_condition("dispatch time of port #{pinfo.disp_time} should be same as #{stime}") { (pinfo.disp_time - stime) < 10 }
    if einfo.ib.nil?
      ret &= verify_equal disp_state, pinfo.disp_state, "dispatch state not correct for #{eqp}:#{port}"
      ret &= verify_equal carrier, pinfo.disp_load_carrier, "dispatch carrier not correct for #{eqp}:#{port}"
    else # ignoring the dispatch state
      ret &= verify_condition("no dispatch carrier expected for #{eqp}:#{port}") {
        carrier.empty? ? einfo.ib.rsv_carriers.empty? : einfo.ib.rsv_carriers.include?(carrier)
      }
    end
    return ret
  end


  def prepare_carrier(carrier, cstatus, params={})
    #assert_equal 0, $sv.carrier_xfer_jobs_delete(carrier), "failed to remove existing transport for #{carrier}"
    #wait_for { $sv.carrier_xfer_jobs(carrier, mcs: true).empty? }
    assert $svtest.cleanup_carrier(carrier), "failed to stock-in #{carrier}"
    case cstatus
    when "MI"
      $sv.carrier_xfer_status_change(carrier, "MI", $svtest.stocker, "", true)
      location = $svtest.stocker
    when "MO"
      $sv.carrier_xfer_status_change(carrier, "MO", $svtest.stocker, "P4", false)
      location = $svtest.stocker
    when "HO"
      $sv.carrier_xfer_status_change(carrier, "HO", @@shelf)
      location = @@shelf
    when "HI"
      $sv.carrier_xfer_status_change(carrier, "HI", @@shelf)
      location = @@shelf
    when "SI"
      $sv.carrier_xfer_status_change(carrier, "SO", @@interbay)
      $sv.carrier_xfer_status_change(carrier, "SI", $svtest.stocker)
      location = $svtest.stocker
    when "EI"
      port = $sv.eqp_info(@@eqp_other).ports.first
      assert_equal 0, $sv.carrier_xfer_status_change(carrier, 'EO', @@eqp_other), "failed to set xfer_state for #{carrier}"
      assert_equal 0, $sv.eqp_load(@@eqp_other, port.port, carrier, purpose: 'Other'), "failed to load carrier #{carrier}"
      location = @@eqp_other
    when /(SO|EO)/
      if params[:no_reroute]
        if cstatus == 'EO'
          $sv.carrier_xfer_status_change(carrier, 'EO', @@eqp)
          location = @@eqp
        else
          $sv.carrier_xfer_status_change(carrier, 'SO', @@interbay)
          location = @@interbay
        end
      else
        assert_equal 0, $sv.carrier_xfer(carrier, $svtest.stocker, '', @@stocker2, '')
        cstat = nil
        assert wait_for(timeout: 40, sleeptime: 3) {
          cstat = $sv.carrier_status(carrier)
          cstat.xfer_status == 'SO'
        }, "no carrier xfer"
        location = cstat.stocker
        if cstatus == 'EO'
          $sv.carrier_xfer_status_change(carrier, 'EO', @@eqp)
        end
      end
    else
      fail "Unknown carrier xfer state #{cstatus}"
    end
    assert_equal cstatus, $sv.carrier_status(carrier).xfer_status, "wrong xfer_state of carrier #{carrier}"
    return location
  end
end
