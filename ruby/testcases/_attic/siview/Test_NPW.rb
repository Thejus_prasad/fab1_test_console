=begin 
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.
    
Author:  Steffen Steidten, 2011-09-26
Version: 1.0.0

=end

require "RubyTestCase"

# Test MSR 103581 (NPW issues), including 125159 (NPW after SLR), 88364
class Test_NPW < RubyTestCase
  Description = "Test MSR 103581 (NPW issues), including 125159 (NPW after SLR), 88364"
  
  @@eqp = "UTS001"
  @@port1 = "P3"
  @@port2 = "P4"
  @@mode = "Auto-2"
  @@carrier1 = "CS09"
  @@carrier2 = "CS10"

  def setup
    super
    $setup_ok = ($setup_ok != false)
    assert $setup_ok, "test setup is not correct"
  end
  
  def test00_setup
    $setup_ok = false
    $sv.eqp_mode_change(@@eqp, @@mode)
    eqi = $sv.eqp_info(@@eqp)
    eqi.ports.each {|p|
      if [@@port1, @@port2].member?(p.port)
        assert_equal @@mode, p.mode, "wrong port mode"
      end
    }
    $sv.npw_reserve_cancel(@@eqp, :port=>@@port2, :loadseq=>2, :carrier=>@@carrier2)
    $setup_ok = true
  end

  
  def test10_npw
    assert_equal 0, $sv.npw_reserve(@@eqp, :port=>@@port2, :loadseq=>2, :carrier=>@@carrier2), "port #{@@port2} not set up correctly for NPW reserve"
    eqi = $sv.eqp_info(@@eqp)
    assert eqi.disp_load_carriers.member?(@@carrier2), "carrier #{@@carrier2} not show as dispach_load_carrier"
  end
  
  def test11_npw_after_slr
    $sv.npw_reserve_cancel(@@eqp, :port=>@@port2, :loadseq=>2, :carrier=>@@carrier2)
    #
    @@cj = $sv.slr(@@eqp, :port=>@@port1, :carrier=>@@carrier1)
    assert @@cj, "error in SLR"
    assert_equal 0, $sv.npw_reserve(@@eqp, :port=>@@port2, :loadseq=>2, :carrier=>@@carrier2), "port #{@@port2} not set up correctly for NPW reserve"
  end
  
  def test99_cleanup
    $sv.slr_cancel(@@eqp, @@cj, :port=>@@port1)
    $sv.npw_reserve_cancel(@@eqp, :port=>@@port2, :loadseq=>2, :carrier=>@@carrier2)
  end

end  