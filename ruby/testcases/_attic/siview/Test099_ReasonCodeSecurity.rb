=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: Daniel Steger, 2016-08-09

History:
  2016-08-09 dsteger,  http://gfjira/browse/MES-3152
=end

require 'SiViewTestCase'


# Test Reason code security, MSR 993831 http://gfjira/browse/MES-3152
# TODO: need to drop checks acccording to MES-2991
class Test099_ReasonCodeSecurity < SiViewTestCase
  @@reasoncode = 'INT'
  @@reasoncode_priv = 'X-S1'
  @@bank_reasoncode = 'INT'
  @@bank_reasoncode_priv = 'QA2'
  @@inhibit_reasoncode = 'PENG'
  @@inhibit_reasoncode_priv = 'X-S1'

  @@eqp = 'UTF001'

  @@rework_route = 'UTRW001.01'
  @@rework_opNo = '1000.400'
  @@rework_return_opNo = '1000.400'
  @@merge_opNo = '1000.900'

  @@branch_route = 'UTBR001.01'
  @@branch_split_opNo = '1000.200'
  @@branch_return_opNo = '1000.300'

  @@remeas_opNo = '1000.600'

  # branch, rework, measurement
  @@groups = {
    'MM.HOLDS.QA' => true,
    'MM.HOLDS.NONE' => false,
    'MM.HOLDS.NO-MERGE' => true,
    'MM.HOLDS.MERGE-ONLY' => false
  }


  def self.after_all_passed
    $sv.delete_lot_family(@@lot)
  end

  def setup
    assert_equal 0, $sv.lot_cleanup(@@lot) if self.class.class_variable_defined?(:@@lot)
  end


  def test00_setup
    $setup_ok = false
    #
    assert @@sv_user = SiView::MM.new("#{$env}_gms")
    assert @@lot = $svtest.new_lot
    $log.info "using lot #{@@lot.inspect}"
    #
    $setup_ok = true
  end

  def test10_hold_security
    @@groups.each_pair do |privilegegroup, access|
      $log.info ">> privilege group #{privilegegroup}, access: #{access}"
      assert $sm.user_update_privilegegroup(@@sv_user.user, privilegegroup), "failed to update group"
      # none-privileged hold
      do_holdactions(@@sv_user, @@lot, @@reasoncode, true, @@rework_opNo)
      # privileged hold
      do_holdactions(@@sv_user, @@lot, @@reasoncode_priv, access, @@rework_opNo)
      ##assert_equal 0, $sv.lot_cleanup(@@lot)
    end
  end

  def test11_split_security
    @@groups.each_pair do |privilegegroup, access|
      $log.info ">> privilege group #{privilegegroup}, access: #{access}"
      assert $sm.user_update_privilegegroup(@@sv_user.user, privilegegroup), "failed to update group"
      # none-privileged hold
      do_split_actions(@@sv_user, @@lot, @@reasoncode, true, @@rework_route, @@rework_opNo, @@rework_return_opNo, @@merge_opNo)
      assert $sv.merge_lot_family(@@lot, nosort: true)
      assert_equal 0, $sv.lot_cleanup(@@lot)
      # privileged hold
      do_split_actions(@@sv_user, @@lot, @@reasoncode_priv, access, @@rework_route, @@rework_opNo, @@rework_return_opNo, @@merge_opNo)
      assert $sv.merge_lot_family(@@lot, nosort: true)
      ##assert_equal 0, $sv.lot_cleanup(@@lot)
    end
  end

  def test12_locate_security
    @@groups.each_pair do |privilegegroup, access|
      $log.info ">> privilege group #{privilegegroup}, access: #{access}"
      assert $sm.user_update_privilegegroup(@@sv_user.user, privilegegroup), "failed to update group"
      # none-privileged hold
      do_locate_actions(@@sv_user, @@lot, @@reasoncode, true, @@branch_route, @@branch_split_opNo, @@branch_return_opNo, @@remeas_opNo)
      assert_equal 0, $sv.lot_cleanup(@@lot)
      # privileged hold
      do_locate_actions(@@sv_user, @@lot, @@reasoncode_priv, access, @@branch_route, @@branch_split_opNo, @@branch_return_opNo, @@remeas_opNo)
      ##assert_equal 0, $sv.lot_cleanup(@@lot)
    end
  end

  def test13_bankhold_security
    @@groups.each_pair do |privilegegroup, access|
      assert_equal 0, $sv.lot_opelocate(@@lot, :last), "failed to locate to last operation"
      assert_equal 0, $sv.lot_bankin(@@lot) unless $sv.lot_info(@@lot).status == 'COMPLETED'
      $log.info ">> privilege group #{privilegegroup}, access: #{access}"
      assert $sm.user_update_privilegegroup(@@sv_user.user, privilegegroup), "failed to update group"
      # none-privileged hold
      do_bank_actions(@@sv_user, @@lot, @@bank_reasoncode, true)
      # privileged hold
      do_bank_actions(@@sv_user, @@lot, @@bank_reasoncode_priv, access)
      ##$sv.lot_cleanup(@@lot)
    end
  end

  def test14_inhibit_security
    assert_equal 0, $sv.eqp_cleanup(@@eqp)
    @@groups.each_pair do |privilegegroup, access|
      $log.info ">> privilege group #{privilegegroup}, access: #{access}"
      assert $sm.user_update_privilegegroup(@@sv_user.user, privilegegroup), "failed to update group"
      do_inhibit_actions(@@sv_user, @@eqp, @@inhibit_reasoncode, true)
      do_inhibit_actions(@@sv_user, @@eqp, @@inhibit_reasoncode_priv, access)
    end
  end

  # aux methods

  def do_holdactions(svuser, lot, reason, has_access, opNo)
    rc = has_access ? 0 : 11406
    # LotHold
    assert_equal rc, svuser.lot_hold(lot, reason), "wrong return code on lothold"
    assert_equal 0, $sv.lot_hold(lot, reason) unless $sv.lot_hold_list(lot, reason: reason).count > 0
    assert_equal rc, svuser.lot_holdmemo_update(lot, "new memo"), "wrong return code on lotholdmemoupdate"
    assert_equal rc, svuser.lot_hold_release(lot, nil), "wrong return code on lotholdrelease"
    assert_equal 0, $sv.lot_hold_release(lot, nil) if $sv.lot_hold_list(lot, reason: reason).count > 0
    # FutureHold
    assert_equal rc, svuser.lot_futurehold(lot, opNo, reason), "wrong return code on futurehold"
    assert_equal 0, $sv.lot_futurehold(lot, opNo, reason) unless svuser.rc == 0
    assert_equal rc, svuser.lot_futurehold_cancel(lot, reason), "wrong return code on futureholdcancel"
    assert_equal 0, $sv.lot_futurehold_cancel(lot, reason) unless svuser.rc == 0
  end

  def do_inhibit_actions(svuser, eqp, reason, has_access)
    rc = has_access ? 0 : 11406
    assert_equal rc, svuser.inhibit_entity(:eqp, eqp, reason: reason), "wrong return code on inhibit"
    assert_equal 0, $sv.inhibit_entity(:eqp, eqp, reason: reason) unless svuser.rc == 0
    assert_equal rc, svuser.inhibit_cancel(:eqp, eqp), "wrong return code on inhibitcancel"
  end

  def do_bank_actions(svuser, lot, reason, has_access)
    assert_equal (has_access ? 0 : 206), svuser.lot_bankhold(lot, reason), "wrong return code on bankhold"
    assert_equal 0, $sv.lot_bankhold(lot, reason) unless $sv.lot_hold_list(lot, reason: reason).count > 0
    assert_equal (has_access ? 0 : 11406), svuser.lot_holdmemo_update(lot, "new memo"), "wrong return code on lotholdmemoupdate"
    assert_equal (has_access ? 0 : 2056), svuser.lot_bankhold_release(lot, nil), "wrong return code on bankholdrelease"
    assert_equal 0, $sv.lot_bankhold_release(lot, nil) if $sv.lot_hold_list(lot, reason: reason).count > 0
  end

  def do_locate_actions(svuser, lot, reason, has_access, branch_route, branch_op, branchreturn_op, meas_opNo)
    rc = has_access ? 0 : 11406
    assert_equal 0, $sv.lot_opelocate(lot, branch_op), "failed to locate to branch"
    assert_equal 0, $sv.lot_hold(lot, reason)
    # BranchWithHoldRelease
    assert_equal rc, svuser.lot_branch(lot, branch_route, return_opNo: branchreturn_op, force: true), "wrong return code on branchwithholdrelease"
    assert_equal 0, $sv.lot_branch_cancel(lot) if svuser.rc == 0
    # ForceLocate
    assert_equal 0, $sv.lot_hold(lot, reason) unless $sv.lot_hold_list(lot, reason: reason).count > 0
    assert_equal rc, svuser.lot_opelocate(lot, :first, force: true), "wrong return code on forceopelocate"
    # GatePassOnhold
    assert_equal 0, $sv.lot_opelocate(lot, meas_opNo, force: true)
    assert_equal rc, svuser.lot_gatepass(lot, onhold: true), "wrong return code on gatepassonhold"
    # Remeasure
    assert_equal 0, $sv.lot_opelocate(lot, 1, force: true) unless svuser.rc == 0
    assert_equal rc, svuser.lot_remeasure(lot), "wrong return code on forceremeasure"
  end

  def do_split_actions(svuser, lot, reason, has_access, rework_route, rework_op, rework_ret, mergeopNo)
    children = []
    rc = has_access ? 0 : 11406
    assert_equal 0, $sv.lot_hold(lot, reason), "Hold failed"
    # SplitWithoutLotHoldRelease
    children << svuser.lot_split(lot, 1, mergeop: mergeopNo, onhold: true)
    res  = verify_equal rc, svuser.rc, "wrong return code on splitwithoutholdrelease"
    children << svuser.lot_split(lot, 1, mergeop: mergeopNo, onhold: true, release: true)
    res &= verify_equal rc, svuser.rc, "wrong return code on splitwithholdrelease"
    # Partial Rework w/o hold release
    assert_equal 0, $sv.lot_hold_release(lot, nil)
    assert_equal 0, $sv.lot_opelocate(lot, rework_op), "Failed to locate to rework"
    assert_equal 0, $sv.lot_hold(lot, reason), "Hold failed"
    child = svuser.lot_partial_rework(lot, 1, rework_route, return_opNo: rework_ret, onhold: true)
    res &= verify_equal rc, svuser.rc, "wrong return code on partialreworkwithoutholdrelease"
    if child
      children << child
      assert_equal 0, svuser.lot_partial_rework_cancel(lot, child), "Failed to merge after rework"
    end
    # Partial Rework with hold release
    child = svuser.lot_partial_rework(lot, 1, rework_route, return_opNo: rework_ret, onhold: true, release: true)
    res &= verify_equal rc, svuser.rc, "wrong return code on partialreworkwithholdrelease"
    if child
      children << child
      assert_equal 0, svuser.lot_partial_rework_cancel(lot, child), "Failed to merge after rework"
    end
    assert res
  end

end
