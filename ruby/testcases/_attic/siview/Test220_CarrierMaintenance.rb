=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: dsteger, 2015-10-24

History:
  2016-12-02 sfrieske,  minor setup improvement
=end

require 'siviewmmdb'
require 'SiViewTestCase'


# Carrier maintenance
class Test220_CarrierMaintenance < SiViewTestCase
  @@carrier_category = 'UTNOJCAPDIRTY'
  @@opNo = '1000.150'
  @@eqp = 'UTC002'
  @@partial_complete = false

  @@timeout = 300


  def self.startup
    super
    self.class.remove_class_variable(:@@carrier) if self.class.class_variable_defined?(:@@carrier)
  end

  def self.after_all_passed
    $sv.delete_lot_family(@@lot)
    $sv.carrier_category_change(@@carrier_category, changetype: 3)  # remove data to prevent NOTAVAILABLE carriers
  end


  def setup
    return unless self.class.class_variable_defined?(:@@carrier)
    assert_equal 0, $sv.carrier_status_change(@@carrier, 'AVAILABLE', check: true)
    unless $sv.carrier_category_list.member?(@@carrier_category)
      # initialze carrier category information, required for carrier_counter_reset to reset the condition
      assert_equal 0, $sv.carrier_category_change(@@carrier_category, changetype: 1), 'error creating category data'
    end
    assert_equal 0, $sv.carrier_counter_reset(@@carrier), "cannot reset counter"
    assert cinfo = $sv.carrier_status(@@carrier), "cannot get state for #{@@carrier}"
    #
    assert_equal 0, cinfo.starts, "wrong operation starts counter for #{@@carrier}"
    assert_equal 'Cleaned', cinfo.condition, "wrong carrier condition"
    assert_equal 0, cinfo.pm_counter.filter_starts, "wrong filter counter"
    assert_equal cinfo.claim_time, cinfo.pm_counter.filter_reset, "wrong filter start time"
    assert_equal 0, cinfo.pm_counter.gasket_starts, "wrong gasket counter"
    assert_equal cinfo.claim_time, cinfo.pm_counter.gasket_reset, "wrong gasket start time"
  end


  def test00_setup
    $setup_ok = false
    #
    assert @@mmdb = SiView::MMDB.new($env), 'no connection to MMDB'
    assert_equal 0, $sv.eqp_cleanup(@@eqp, mode: 'Off-Line-1')
    assert @@carrier = $svtest.get_empty_carrier(carrier: '%', carrier_category: @@carrier_category), 'no empty carrier'
    assert @@lot = $svtest.new_lot(carrier: @@carrier), "failed to create new lot"
    unless $sv.carrier_category_list.member?(@@carrier_category)
      # initialze carrier category information
      assert_equal 0, $sv.carrier_category_change(@@carrier_category, changetype: 1), 'error creating category data'
    end
    #
    $setup_ok = true
  end

  def test01_opestart_counter
    $setup_ok = false
    #
    assert_equal 0, $sv.carrier_category_change(@@carrier_category, changetype: 2, op_warning: 2, op_due: 3, op_inhibit: 4), "failed to set carrier category #{@@carrier_category}"
    run_counter_test
    #
    $setup_ok = true
  end

  def test02_filter_counter
    assert_equal 0, $sv.carrier_category_change(@@carrier_category, changetype: 2, filter_op_warning: 2, filter_op_due: 3, filter_op_inhibit: 4), "failed to set carrier category #{@@carrier_category}"
    run_counter_test
  end

  def test03_gasket_counter
    assert_equal 0, $sv.carrier_category_change(@@carrier_category, changetype: 2, gasket_op_warning: 2, gasket_op_due: 3, gasket_op_inhibit: 4), "failed to set carrier category #{@@carrier_category}"
    run_counter_test
  end

  def test04_filter_timer
    assert_equal 0, $sv.carrier_category_change(@@carrier_category, changetype: 2, filter_time_warn: 1, filter_time_due: 2, filter_time_inhibit: 3), "failed to set carrier category #{@@carrier_category}"
    run_timer_test('FILTER_START_TIME')
  end

  def test05_gasket_timer
    assert_equal 0, $sv.carrier_category_change(@@carrier_category, changetype: 2, gasket_time_warn: 1, gasket_time_due: 2, gasket_time_inhibit: 3), "failed to set carrier category #{@@carrier_category}"
    run_timer_test('GASKET_START_TIME')
  end

  def test06_carrier_ei
    # no change if carrier loaded
  end

  def test07_partial_complete
    skip "testing for partial complete skipped" unless @@partial_complete
    assert_equal 0, $sv.lot_cleanup(@@lot), "failed lot cleanuplot"
    assert_equal 0, $sv.eqp_cleanup(@@eqp)
    [0, 15, 25].each_with_index do |count, i|
      walias1, walias2 = $sv.lot_info(@@lot, wafers: true).wafers.map {|w| w.alias}.partition.with_index {|a,i| i >= count}
      #
      assert_equal 0, $sv.lot_opelocate(@@lot, @@opNo)
      assert $sv.claim_process_lot(@@lot, eqp: @@eqp, claim_carrier: true, partial_comp: true, opecancel: walias1), "failed to create CJ"
      check_opestart_counter(@@carrier, i) # first iteration will not increase counter
      assert $sv.merge_lot_family(@@lot, nosort: true)
    end
  end

  def test08_usage_reset
    assert_equal 0, $sv.lot_cleanup(@@lot, opNo: @@opNo, carrier: @@carrier), "failed lot cleanup and locate #{@@lot}"
    assert $sv.claim_process_lot(@@lot, eqp: @@eqp, claim_carrier: true, slr: true, mode: 'Auto-1'), "failed to create CJ"
    check_opestart_counter(@@carrier, 1)
    $log.info "wait for 1min "; sleep 60
    assert csinfo1 = $sv.carrier_status(@@carrier), "failed to get carrier status before"
    assert_equal 1, csinfo1.pm_age, "wrong pm age"
    ##tstart = Time.now-10
    assert_equal 0, $sv.carrier_usage_reset(@@carrier), "failed to do usage reset"
    assert csinfo = $sv.carrier_status(@@carrier), "failed to get carrier status after"
    csinfo1.pm_counter.starts = 0 # ope starts is reset
    assert_equal csinfo.pm_counter, csinfo1.pm_counter, "failed to compare counters"
    assert_equal 0, csinfo.pm_age, "pm age should be reset"
    assert wait_for {@@mmdb.durable_history(@@carrier, action: 'PMRESET', tstart: $sv.siview_timestamp(csinfo1.claim_time)).first}, "failed to get history"
    ##check_reset_history(:opestart, tstart)
  end

  def test09_counter_reset
    counters = {opestart: 0, filter: 0, gasket: 0}
    counters.each_key {|counter_type|
      assert_equal 0, $sv.lot_opelocate(@@lot, @@opNo), "failed lot locate for #{@@lot}"
      assert $sv.claim_process_lot(@@lot, eqp: @@eqp, claim_carrier: true, slr: true) #, mode: 'Auto-1'), "failed to create CJ"
      counters.each_key {|k| counters[k] += 1}
      check_opestart_counter(@@carrier, counters)
      flags = Hash[(counters.keys - [counter_type]).map {|k| [k, false]}]
      assert_equal 0, $sv.carrier_counter_reset(@@carrier, flags), "failed to do usage reset"
      ##tstart = Time.now-10
      tstart = $sv.siview_timestamp($sv.carrier_status(@@carrier).claim_time)
      counters[counter_type] = 0
      check_opestart_counter(@@carrier, counters)
      check_reset_history(counter_type, tstart)
    }
  end

  def test10_opestart_cancel
    assert_equal 0, $sv.lot_cleanup(@@lot, opNo: @@opNo), "failed lot cleanup and locate #{@@lot}"
    assert $sv.claim_process_lot(@@lot, eqp: @@eqp, claim_carrier: true, slr: true, mode: 'Auto-1', opestart_cancel: true), "failed to create CJ"
    check_opestart_counter(@@carrier, 0)
  end

  def test11_no_counter_reset
    assert_equal 10981, $sv.carrier_counter_reset(@@carrier, gasket: false, filter: false, opestart: false), "No reset expected"
  end

  def test20_carrier_condition_used
    assert ec = $svtest.get_empty_carrier, "Failed to get empty carrier"
    assert_equal 0, $sv.wafer_sort(@@lot, ec), "failed to transfer lot to #{ec.inspect}"
    assert_equal 0, $sv.carrier_counter_reset(@@carrier), "cannot reset counter"
    #setup # for a clean sweep
    ['Used', 'Due', 'Dirty'].each do |condition|
      assert_equal 0, $sv.carrier_condition_change(@@carrier, condition) unless condition == 'Used'
      assert child = $sv.lot_split(@@lot, 5), "failed to split child lot"
      assert_equal 0, $sv.wafer_sort_rpt(child, @@carrier), "failed to transfer lot to #{@@carrier}"
      assert cinfo = $sv.carrier_status(@@carrier), "failed to get #{@@carrier} status"
      assert_equal condition, cinfo.condition, "wrong carrier condition"
      setup # for a clean sweep
      assert_equal 0, $sv.carrier_condition_change(@@carrier, condition) unless condition == 'Used'
      #
      assert_equal 0, $sv.wafer_sort(@@lot, @@carrier), "failed to transfer lot to #{@@carrier}"
      assert cinfo = $sv.carrier_status(@@carrier), "failed to get #{@@carrier} status"
      assert_equal condition, cinfo.condition, "wrong carrier condition"
    end
  end


  def run_counter_test
    assert_equal 0, $sv.eqp_cleanup(@@eqp)
    [:opecomp, :force_comp, :opecomp, :force_comp].each_with_index do |scenario, i|
      assert_equal 0, $sv.lot_hold_release(@@lot, nil), "failed to release hold"
      assert_equal 0, $sv.lot_opelocate(@@lot, @@opNo), "failed lot locate #{@@lot}"
      assert $sv.claim_process_lot(@@lot, eqp: @@eqp, claim_carrier: true, running_hold: (scenario == :force_comp)), "failed to create CJ"
      check_opestart_counter(@@carrier, i+1)
      check_carrier_condition(@@carrier, i, ['Used', 'Used', 'Due', 'Dirty'][i])
    end
  end

  def run_timer_test(timer_name)
    4.times do |i|
      if i > 0
        sleeptime = $sv.siview_timestamp(Time.now-24*3610*i)
        assert $sv.user_data_update(:carrier, @@carrier, {timer_name=>sleeptime})
        $log.info "set filter start time #{i} day(s) back"
      end
      check_carrier_condition(@@carrier, i, ['Cleaned', 'Cleaned', 'Due', 'Dirty'][i])
    end
  end

  def check_opestart_counter(carrier, count)
    assert cinfo = $sv.carrier_status(carrier), "failed to get #{carrier} status"
    if count.kind_of?(Hash)
      assert_equal count[:opestart], cinfo.pm_counter.starts, "wrong operation start count"
      assert_equal count[:filter], cinfo.pm_counter.filter_starts, "wrong filter start count"
      assert_equal count[:gasket], cinfo.pm_counter.gasket_starts, "wrong gasket start count"
    else
      assert_equal count, cinfo.pm_counter.starts, "wrong operation start count"
      assert_equal count, cinfo.pm_counter.filter_starts, "wrong filter start count"
      assert_equal count, cinfo.pm_counter.gasket_starts, "wrong gasket start count"
    end
  end

  def check_carrier_condition(carrier, limit, condition)
    udata = nil
    assert wait_for(timeout: @@timeout) {
      udata = $sv.user_data(:carrier, carrier)
      udata['LIMIT_REACHED'] == limit.to_s
    }, "carrier limit #{limit} not reached (#{udata['LIMIT_REACHED']})"
    assert cinfo = $sv.carrier_status(carrier), "failed to get #{carrier} status"
    assert_equal condition, cinfo.condition, "wrong carrier condition"
    assert_equal 'NOTAVAILABLE', cinfo.status, "wrong carrier state" if condition == 'Dirty'
  end

  def check_reset_history(counter_type, tstart)
    assert action = {opestart: 'PMRESET', filter: 'FILTERRESET', gasket: 'GASKETRESET'}[counter_type], 'wrong counter type'
    assert wait_for {@@mmdb.durable_history(@@carrier, action: action, tstart: tstart).first}, "failed to get history for #{action.inspect}"
  end
end
