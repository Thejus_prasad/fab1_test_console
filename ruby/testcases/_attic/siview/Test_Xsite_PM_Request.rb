=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

This testcase does simple lot processing in Offline-1 Mode, SiView.

Author: Daniel Steger, 2011-05-16
=end

require "RubyTestCase"
require 'easyxsite'

# This testcase does simple lot processing in Offline-1 Mode, SiView.
class Test_Xsite_PM_Request < RubyTestCase
    Description = "Sample Xsite testcases"

    @@eqp="AMI2100"
    @@lot=""
    @@operation=""

   def prepare_lot
     if @@lot == ""
       _list = $sv.what_next(@@eqp)
       assert _list.length > 0, "No lot found on dispatch list!"
       @@lot = _list[0].lot
       l_info = $sv.lot_info @@lot
       @@operation = l_info.opNo
     else
       l_info = $sv.lot_info @@lot
       $sv.lot_bankin_cancel @@lot if l_info.status == "COMPLETED"
       $sv.lot_opelocate(@@lot, @@operation)
     end
   end


    def test01_setup
      $setup_ok = false

      # Tool needs to be online-remote
      $sv.eqp_mode_change(@@eqp, 'Auto-1')

      $xs = EasyXsite.new("xsite." + $env)
      $sv.eqp_status_change_req @@eqp, '2WPR'
      einfo = $sv.eqp_info @@eqp
      # The tool should be in state 2WPR
      assert_equal "SBY.2WPR", einfo.status

      $setup_ok = true
    end

    def test10_processlot
      prepare_lot

      eqpi, port, pg, carrier, mode = $sv.claim_process_prepare @@lot, :eqp=>@@eqp, :claim_carrier=>true
      cj = $sv.eqp_opestart(@@eqp, carrier)
      check_E10state_Xsite "PRD-PRODUCT"

      sleep 10

      rc = $sv.eqp_opecomp(@@eqp, cj)
      check_E10state_Xsite "SBY-WT-PRODUCT"

      rc = $sv.eqp_unload(@@eqp, port, carrier)
    end

    def test10_processlot_cancel
      prepare_lot

      eqpi, port, pg, carrier, mode = $sv.claim_process_prepare @@lot, :eqp=>@@eqp, :claim_carrier=>true
      cj = $sv.eqp_opestart(@@eqp, carrier)
      check_E10state_Xsite "PRD-PRODUCT"

      sleep 10

      rc = $sv.eqp_opestart_cancel(@@eqp, cj)
      check_E10state_Xsite "SBY-WT-PRODUCT"

      rc = $sv.eqp_unload(@@eqp, port, carrier)
    end

    def check_E10state_Xsite(expected_state)
      # Check E10 state in Xsite
      sleep 2
      reply = $xs.eqp_check_status @@eqp
      assert_equal expected_state, reply.state
    end

    def test99_close
      $xs.disconnect
    end


end
