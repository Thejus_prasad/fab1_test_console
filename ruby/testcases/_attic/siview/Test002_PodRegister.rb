=begin 
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.
    
Author: Daniel Steger, 2011-07

# $Rev: 13314 $
# $LastChangedDate: 2011-07-28 16:06:01 +0200 (Do, 28 Jul 2011) $
# $LastChangedBy: dsteger $
=end

require "RubyTestCase"

# Register new reticle pods in SiView
# MSR484446
class Test002_PodRegister < RubyTestCase
    Description = "Register new reticle pods in SiView"

    @@pod_count = 10
    @@prefix = "UTP"
    @@pod_category="MAIN-E"
    
    def generate_podids
      return @@pod_count.times.collect {|i| @@prefix + "%.3d" % i}
    end

    # clean up old data
    def _cleanup(pod)
      $log.info("#{__method__} #{pod}")
      rps = $sv.reticle_pod_status(pod)
      rps.reticles.each {|r|
        res = $sv.reticle_justinout(r.reticle, pod, r.slot, "out")
        return nil if res != 0
      }
      $sv.reticle_pod_multistatus_change(pod, "AVAILABLE") if rps.status != "AVAILABLE"
      eqi = $sv.eqp_info(rps.eqp)
      $sv.reticle_pod_offline_unload(rps.eqp, eqi.reticleports[0].port, pod) if rps.eqp != "" and rps.xfer_status == "EI"
      if rps.stocker != "" and rps.xfer_status == "SI"
        brs = $sv.brs_info(rps.stocker)
        if brs
          _rp = brs.ports.find {|rp| rp.loaded_pod == pod}
          $sv.reticle_pod_unload(pod, :brs=>rps.stocker, :rsc=>_rp.port) if _rp
        end
      end
    end
    
    def test01      
      _ids = generate_podids
      # delete the pods, don't care about any errors
      _ids.each {|p|  _cleanup(p)}
      $sv.reticle_pod_multistatus_change _ids, "NOTAVAILABLE", :substate=>'DIRTY'
      $sv.reticle_pod_delete _ids
      # try to create the pods
      $sv.reticle_pod_register _ids, @@pod_category
      rc = $sv.reticle_pod_multistatus_change _ids, "AVAILABLE"
      assert_equal 0, rc, "something is wrong with pods #{_ids.inspect}"      
    end
end
