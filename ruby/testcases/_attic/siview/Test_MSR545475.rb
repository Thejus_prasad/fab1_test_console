=begin 
Test the jCAP TemporalSpaceIncreaser job.

(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.
    
    to run the tests from the test console:
    load_run_testcase "Test_MSR545475", "let"
    
Author: Thomas Klose, 2012-02-14
=end

=begin
   for AKochinka, prepare tests for MDS History enhancements, details in MSR
=end

require "RubyTestCase"

# Test the jCAP TemporalSpaceIncreaser job.
# Test MSR545475
class Test_MSR545475 < RubyTestCase
  Description = "Test MSR545475"
  
  # route  
  @@route = "C02-TKEY.01"
  @@product = "TKEY01.A0"  
  
  
  def test11_start_tests
    lot = $sv.new_lot_release(:stb=>true, :product=>@@product, :route=>@@route, :nwafers=>25)
    assert lot
    sleep 60
    assert $sv.lot_hold_release(lot, nil)
    sleep 10
    assert $sv.lot_opelocate(lot, "6500.1100")
    sleep 10
    assert $sv.scrap_wafers(lot, "AEHL")
    sleep 10
    assert $sv.scrap_wafers_cancel(lot)
    sleep 10
    assert $sv.lot_split(lot, 5)
    sleep 10
    assert $sv.merge_lot_family(lot)
    sleep 10
    carrier = $sv.lot_info(lot)["carrier"]
    assert carrier
    assert $sv.wafer_sort_req(lot, "")
    sleep 10
    assert $sv.wafer_sort_req(lot, carrier)
    sleep 10
    # randomize
    assert $sv.wafer_sort_req(lot, ($sv.lot_info(lot).carrier), :slots=>"shuffle")
    sleep 10
    # carrier change
    carrier = $sv.carrier_list(:empty=>true, :status=>"AVAILABLE")[0]
    assert $sv.wafer_sort_req(lot, carrier)
    sleep 10
    assert $sv.lot_opelocate(lot, +1)
    assert $sv.lot_bankin(lot)
    sleep 10
    assert $sv.lot_bank_move(lot, "OY-SHIP")
    sleep 10
    assert $sv.lot_ship(lot, "OY-SHIP")
    sleep 10
    assert $sv.lot_ship_cancel(lot, "OY-SHIP")
    sleep 10
    $sv.delete_lot_family(lot, :sleeptime=>2)
  end
  
  def self.result
    return @@result
  end
  
end
