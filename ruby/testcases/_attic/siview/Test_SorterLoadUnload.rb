=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author:  Steffen Steidten, 2011-09-26
Version: 1.0.0

=end

require "RubyTestCase"

# Test MSRs 128560 (load and unload sequences on a sorter without NPW and SLR) to fix MSR 125159
class Test_SorterLoadUnload < RubyTestCase
  @@eqp = "UTS001"
  @@ports = ["P1", "P2", "P3", "P4"]
  @@carrier_empty = "UT80"
  @@carrier_lot = "UT88"


  def test00_setup
    $setup_ok = false
    #
    @@carriers = [@@carrier_empty, @@carrier_lot]
    # check eqp
    assert eqp_cleanup(@@eqp)
    @@ports.each {|p|
      @@carriers.each {|c|
        assert_equal 0, $sv.npw_reserve_cancel(@@eqp, :port=>p, :carrier=>c), "error canceling NPW reservations"
      }
    }
    assert_equal 0, $sv.eqp_mode_offline1(@@eqp)
    eqi = $sv.eqp_info(@@eqp)
    assert_equal [], eqi.loaded_carriers + eqi.disp_load_carriers + eqi.rsv_carriers, "eqp #{@@eqp} has reserved or loaded carriers"
    # check carriers
    assert_equal [], $sv.carrier_status(@@carrier_empty).lots, "carrier #{@@carrier_empty} must be empty"
    assert_not_equal [], $sv.carrier_status(@@carrier_lot).lots, "carrier #{@@carrier_lot} must not be empty"
    #
    $setup_ok = true
  end


  def test11_load_unload_offline
    assert eqp_cleanup(@@eqp), "#{@@eqp} is not clean"
    assert_equal 0, $sv.eqp_mode_offline1(@@eqp)
    test_ok = true
    @@ports.each {|p|
      @@carriers.each {|c|
##        eqp_cleanup(@@eqp)
        test_ok &= ($sv.eqp_load(@@eqp, p, c, purpose: "Other") == 0)
        test_ok &= ($sv.eqp_unload(@@eqp, p, c) == 0)
      }
    }
    assert test_ok
  end

  def test11_load_unload_online
    assert eqp_cleanup(@@eqp), "#{@@eqp} is not clean"
    assert_equal 0, $sv.eqp_mode_change(@@eqp, 'Semi-1')
    test_ok = true
    @@ports.each {|p|
      @@carriers.each {|c|
        eqp_cleanup(@@eqp)
        $sv.eqp_port_status_change(@@eqp, p, "LoadComp")
        test_ok &= ($sv.eqp_load(@@eqp, p, c, purpose: "Other") == 0)
        test_ok &= ($sv.eqp_unload(@@eqp, p, c) == 0)
        $sv.eqp_port_status_change(@@eqp, p, "LoadReq")
      }
    }
    assert test_ok
  end


  def eqp_cleanup(eqp)
    # return true if no carrier is loaded
    eqi = $sv.eqp_info(eqp)
    eqi.ports.each {|p|
      $sv.eqp_unload(eqp, p.port, p.loaded_carrier) if p.loaded_carrier != ""
    }
    eqi = $sv.eqp_info(eqp)
    return (eqi.loaded_carriers + eqi.disp_load_carriers + eqi.rsv_carriers == [])
  end

end
