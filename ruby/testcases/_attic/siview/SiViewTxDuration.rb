=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: Mahendranath Nampally    2015-02-19

History:
  2015-03-28 ssteidte,  minor changes

Notes:
  start with Tx release level: runtest "SiViewTxDuration", "itdc", parameters: {txrelease: 8}
=end

require "SiViewTestCase"
require 'svlog'

# Measuring SiView TX Duration
class SiViewTxDuration < SiViewTestCase
  @@sv_defaults = {user: 'X-UNITPRF', password: :default} # dedicated user for svlog
  @@lot = nil
  @@uparam = 'MaxProcessWaferCnt'
  @@eqp = 'UTC003'
  @@opNo = '1000.100'
  @@eqp_ib = 'UTI002' #Test equipmen for internal buffer
  @@opNo_ib = '1000.200'
  @@eqp_reticle = 'UTR001'
  @@reticleport = 'RP1'  # eqp_info R8 does not have reticleport data
  @@reticle = 'UTRETICLE0003'
  @@rpod = 'R29N'
  @@rpstocker = 'UTRPSTO110'
  @@workarea = 'UT'
  # PSM
  @@subroute = 'UTBR001.01'
  @@opNo_split = '1000.200'
  @@opNo_merge = '1000.300'

  @@svlog_delay = 600  # 10 minutes
  @@svtxperf = 'testcasedata/svtxperf.yaml'  # optional Tx list from svlog to get the coverage
  @@txrelease = 15
  @@tx_blacklist = []
  @@use_svlog = false


  def self.startup
    @@sv_defaults[:collect_durations] = !@@use_svlog
    $sv = $svtest = nil
    super
    $svlog = SvLog.new($env) if @@use_svlog
    @@testlots = []
  end

  def self.shutdown
    @@testlots.each {|lot| $sv.delete_lot_family(lot)}
    $sv = $svtest = nil
  end


  def test00_setup
    $setup_ok = false
    #
    # set SiView TX release level, e.g. use TxLotInfoInq of ...__150
    @@svrelease = $sv.release
    $sv.release = @@txrelease
    $log.info $sv.inspect
    #
    @@tstart = Time.now - 10
    if @@lot
      assert_equal 0, $sv.lot_cleanup(@@lot)
    else
      @@lot = $svtest.new_lot
      @@testlots << @@lot
    end
    @@carrier = $sv.lot_info(@@lot).carrier
    assert_not_equal [], $sv.eqp_info(@@eqp).chambers, "no chamber tool"
    assert $svtest.cleanup_eqp(@@eqp)
    assert $svtest.cleanup_eqp(@@eqp_ib)
    #
    $setup_ok = true
  end

  def test101_lot
    assert_equal 0, $sv.lot_cleanup(@@lot)
    assert_equal 0, $sv.lot_gatepass(@@lot)
    assert $sv.lot_operation_list(@@lot), "Unable to retrieve the lot operation list"
    li = $sv.lot_info(@@lot, wafers: true)
    assert $sv.eqp_list_byop($svtest.product, li.op)
    assert $sv.lot_by_waferid(li.wafers.first.wafer)
    assert $sv.lot_family(@@lot)
    assert $sv.AMD_modulepd(@@lot)
    assert $sv.lot_operation_history(@@lot, first: true)
  end

  def test103_lot_hold
    assert_not_equal [], $sv.code_list('LotHold', reason_list: true)
    assert_equal 0, $sv.lot_hold(@@lot, nil)
    assert $sv.lot_hold_list(@@lot)
    assert_equal 0, $sv.lot_hold_release(@@lot, nil)
  end

  def test104_lot_futurehold
    assert_equal 0, $sv.lot_futurehold(@@lot, $sv.lot_info(@@lot).opNo, nil, post: true)
    assert $sv.lot_futurehold_list(@@lot)
    assert_equal 0, $sv.lot_futurehold_cancel(@@lot, nil)
  end

  def test105_lot_openote
    assert_equal 0, $sv.lot_openote_register(@@lot, 'test', 'test')
    assert $sv.lot_openote_list(@@lot)
  end

  def test106_lot_split
    assert lc = $sv.lot_split(@@lot, 2)
    assert_equal 0, $sv.lot_merge(@@lot, lc)
  end

  def test107_lot_experiment
    assert_equal 0, $sv.lot_experiment(@@lot, 2, @@subroute, @@opNo_split, @@opNo_merge)
    assert $sv.lot_experiment_list(@@lot)
    assert $sv.lot_experiment_delete(@@lot)
  end

  def test109_qtime
    assert_equal 0,$sv.lot_qtime_reset(@@lot)
  end

  def test109_stb_controllot
    assert lot = $svtest.new_controllot('Equipment Monitor')
    $sv.delete_lot_family(lot)
  end

  def test111_pd_list
    assert $sv.pd_list
  end

  def test112_inhibit
    assert_equal 0, $sv.inhibit_cancel(:eqp, @@eqp)
    assert_equal [], $sv.inhibit_list(:eqp, @@eqp)
    assert_equal 0, $sv.inhibit_entity(:eqp, @@eqp)
    assert_equal 0, $sv.inhibit_cancel(:eqp, @@eqp)
  end

  def test113_GetUDATA
    assert $sv.AMDGetUDATA('EQP', @@eqp), "Unable to retrieve the UDATA of the equipment"
  end

  def test114_user_parameter
    assert $sv.user_parameter('Eqp', @@eqp)
    assert_equal 0, $sv.user_parameter_change('Eqp', @@eqp, @@uparam, 1)
    assert_equal 0, $sv.user_parameter_delete('Eqp', @@eqp, @@uparam)
  end

  def test121_carrier
    assert_not_equal [], $sv.carrier_list, "Unable to retrieve the carrier list information"
    assert $sv.carrier_status(@@carrier)
    assert $sv.lot_list_incassette(@@carrier)
  end

  def test122_carrier_reserve
    $sv.carrier_reserve_cancel(@@carrier) unless $sv.carrier_status(@@carrier).xfer_user.empty?
    assert_equal 0, $sv.carrier_reserve(@@carrier)
    assert_equal 0, $sv.carrier_reserve_cancel(@@carrier)
  end

  def test123_carrier_status_change
    $sv.carrier_status_change(@@carrier, 'AVAILABLE') unless $sv.carrier_status(@@carrier).status == 'AVAILABLE'
    assert_equal 0, $sv.carrier_status_change(@@carrier, 'INUSE')
    assert_equal 0, $sv.carrier_status_change(@@carrier, 'AVAILABLE')
  end

  def test124_carrier_xfer
    $sv.eqp_cleanup(@@eqp)
    port = $sv.eqp_info(@@eqp).ports.first.port
    assert_equal 0, $sv.carrier_xfer_status_change(@@carrier, 'EO', @@eqp)
    assert_equal 0, $sv.carrier_xfer_complete(@@carrier, 'EO', @@eqp, port)
    # single
    assert_equal 0, $sv.carrier_xfer(@@carrier, '', '', $svtest.stocker, '')
    assert_not_equal [], $sv.carrier_xfer_jobs(@@carrier)
    wait_for {$sv.carrier_xfer_jobs(@@carrier).empty?}
    # multi
    $sv.eqp_mode_change(@@eqp, 'Auto-2')
    assert_equal 0, $sv.carrier_xfer(@@carrier, $svtest.stocker, '', @@eqp, port, multicarrier: true)
    assert_not_equal [], $sv.carrier_xfer_jobs(@@carrier)
    wait_for {$sv.carrier_xfer_jobs(@@carrier).empty?}
  end

  def test131_sj_list
    assert $sv.sj_list
  end

  def test132_eqp_list
    assert $sv.workarea_eqp_list(@@workarea)
  end

  def test133_eqp
    assert_equal 0, $sv.lot_cleanup(@@lot, opNo: @@opNo)
    assert $svtest.cleanup_eqp(@@eqp, mode: 'Auto-1')
    assert_equal 0, $sv.eqp_status_change_rpt(@@eqp, '2WPR')
    port = $sv.eqp_info(@@eqp).ports.first.port
    assert cj = $sv.slr(@@eqp, lot: @@lot)
    assert_equal 0, $sv.slr_cancel(@@eqp, cj)
    assert $sv.claim_process_lot(@@lot, eqp: @@eqp, slr: true, claim_carrier: true, opestart_cancel: true)
    assert ch = $sv.eqp_info(@@eqp).chambers[0]
    assert $sv.claim_process_lot(@@lot, eqp: @@eqp, slr: true, claim_carrier: true) {|cj|
      $sv.AMD_chambers_availability(cj)
      assert_equal 0, $sv.chamber_status_change_rpt(@@eqp, ch.chamber, '1PRD')
      assert_equal 0, $sv.chamber_process_wafer_rpt(@@eqp, ch.chamber, @@lot, 'ProcessStart')
      assert_equal 0, $sv.chamber_process_wafer_rpt(@@eqp, ch.chamber, @@lot, 'ProcessEnd')
      assert_equal 0, $sv.chamber_status_change_rpt(@@eqp, ch.chamber, '2WPR')
    }
  end

  def test134_eqp_ib
    assert_equal 0, $sv.lot_cleanup(@@lot, opNo: @@opNo_ib)
    assert $svtest.cleanup_eqp(@@eqp_ib, mode: 'Auto-1')
    assert cj = $sv.slr(@@eqp_ib, lot: @@lot)
    assert_equal 0, $sv.slr_cancel(@@eqp_ib, cj)
    assert $sv.claim_process_lot(@@lot, eqp: @@eqp_ib, slr: true, claim_carrier: true, opestart_cancel: true)
    assert $sv.claim_process_lot(@@lot, eqp: @@eqp_ib, slr: true, claim_carrier: true)
  end

  def test139_eqp_alarm
    assert $sv.eqp_alarm(@@eqp)
  end

  def test141_reticle
    assert $sv.reticle_list(reticle: @@reticle)
    assert $sv.reticle_status(@@reticle)
  end

  def test142_reticle_pod
    assert $sv.reticle_pod_list(pod: @@rpod)
    assert $sv.reticle_pod_status(@@rpod)
    assert_equal 0, $sv.reticle_pod_xfer_status_change_new(@@rpod, 'MI', @@rpstocker)
  end

  def test143_lot_reticlegrouplist
    assert $sv.AMD_lot_reticlegrouplist(@@lot, @@eqp)
  end

  def test144_EqpReticlePortStatusChange
    assert_equal 0, $sv.eqp_cleanup(@@eqp_reticle, mode: 'Off-Line-1')
    assert_equal 0, $sv.eqp_mode_change(@@eqp_reticle, 'Auto-1')
    assert_equal 0, $sv.eqp_rsp_port_status_change(@@eqp_reticle, @@reticleport, 'LoadReq')
  end

  def test145_what_reticle
    $sv.what_reticle_actionlist(@@eqp)  # fails in Fab1
  end

  def test151_misc
    assert_equal 0, $sv.logon_check
    assert_equal 0, $sv.privilege_check
    ## no info about Tx duration but on RTD process time:  assert $sv.rtd_dispatch_list(@@eqp)
  end


  def test199_summary
    # write perf statistics
    tend = Time.now
    if @@use_svlog
      $log.info "waiting #{@@svlog_delay}s for svlog"; sleep @@svlog_delay
      res = $svlog.get_statistics(release: $sv.release, svrelease: @@svrelease, export: true, tstart: @@tstart, tend: tend, user: $sv.user, sv: $sv)
    else
      res = $sv.manager.get_statistics(release: $sv.release, svrelease: @@svrelease, export: true)
    end
    #
    # verify TX list
    $log.info "checking TX coverage, Tx list: #{@@svtxperf.inspect}"
    txref = YAML.load_file(@@svtxperf).sort.uniq - @@tx_blacklist
    txcalled = res[:data].collect {|e| e.first}.sort.uniq
    $log.warn "not covered:\n#{(txref - txcalled).pretty_inspect}"
  end

  def testmanual299_comp
    SiView.compare_statistics(env: $env)
  end

end
