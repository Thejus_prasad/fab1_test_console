=begin 
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.
    
Author: Steffen Steidten, 2010-12-03
=end

require "RubyTestCase"
require 'apcei'

# Extended Tests of VRNDM with real equipment in Auto-2 mode
class Test_VRNDM2 < RubyTestCase
  Description = "Extended Tests of VRNDM with real equipment in Auto-2 mode"
  
  @@run_test1 = "false"   # run one lot at VRNDM and NoVRNDM Litho ops
  @@run_test2 = "true"    # run two lots in the same carrier at VRNDM
  
  @@carrier = "A315" #"D304"
  @@stocker = "STO160"
  @@vrndm_eqp = "ALC1001"
  @@vrndm_port = "P1"
  
  @@olotstart_port = "P6"
  
  @@op_lotstart = "1000.1000"
  @@op_gatepass = "2000.2000"
  @@op_VRNDM = "2000.3000"
  @@op_NoVRNDM = "2000.4000"
  
  @@li_old = nil
  @@li_new = nil

  def setup
    super
    $setup_ok = ($setup_ok != false)
    assert $setup_ok, "test setup is not correct"
  end

  # Test preparation
  
  def test01_carrier
    # clean up carrier
    $setup_ok &= Test_VRNDM2.reset_carrier(@@carrier)
    $setup_ok &= Test_VRNDM2.remove_carrier(@@carrier, @@stocker)
    assert $setup_ok, "carrier #{@@carrier} needs to be recovered"
  end
  
  def test02_lot_merge
    lot1 = $sv.lot_list_incassette(@@carrier)[0]
    lf = $sv.lot_family(lot1)
    lf.each {|l| 
      $sv.lot_hold_release(l, nil)
      $sv.lot_opelocate(l, @@op_lotstart)
    }
    $sv.lot_merge(lf[0], nil)
    # check all children are merged back
    lots = $sv.lot_list_incassette(@@carrier)
    $setup_ok &= (lots.size == 1)
    assert $setup_ok, "too many lots in carrier #{@@carrier}: #{lots.inspect}"
  end
  
  def test03_eqp_mode
    # try to set the port mode
    $sv.eqp_mode_change(@@vrndm_eqp, "Auto-2", :port=>@@vrndm_port)
    # check
    eqpi = $sv.eqp_info(@@vrndm_eqp)
    eqpi.ports.each {|p| $setup_ok &= p.mode == "Auto-2" if p.port == @@vrndm_port}
  end
  
  def test03_apc
    # verify APC is working
    $setup_ok = false
    lot = $sv.lot_list_incassette(@@carrier)[0]
    $sv.lot_hold_release(lot, nil)
    $sv.lot_futurehold_cancel(lot, nil)
    tref = Time.now
    $sv.lot_opelocate(lot, @@op_gatepass)
    apc = APC::EI.new($env, :sv=>$sv)
    res = apc.get_VRNDM_data(@@vrndm_eqp, lot, :recipe=>"T175CT20_6VS45", :recipe_context=>"ALC-Process")
    assert_equal 6, res.size
    $log.info "APC timestamp: #{res[2]}"
    assert (Time.parse(res[2]) - tref).abs < 120
    $setup_ok = true
  end
  
  # Test1: process at VRNDM and NoVRNDM operations
  
  def test11_VRNDM_op
    ($log.warn "skipped"; return) unless @@run_test1.downcase == "true"
    # "happy randomize" szenario with one lot 
    lot1 = $sv.lot_list_incassette(@@carrier)[0]
    #
    $log.info "-- locating to VRNDM GatePass operation"
    assert_equal 0, $sv.lot_opelocate(lot1, @@op_gatepass), "error locating lot #{lot1}"
    #
    $log.info "-- processing at the VRNDM operation on a real tool"
    @@li_old = $sv.lot_info(lot1, :wafers=>true)
    assert_equal @@op_VRNDM, @@li_old.opNo, "error in GatePass for lot #{lot1}"
    assert $sv.auto_process_carriers(@@vrndm_eqp, @@vrndm_port, @@carrier)
    @@li_new = $sv.lot_info(lot1, :wafers=>true)
    #
    $log.info "-- comparing slotmaps"
    $log.debug "original slotmap: #{@@li_old.wafers.pretty_inspect}"
    $log.debug "current slotmap: #{@@li_new.wafers.pretty_inspect}"
    assert @@li_old.wafers != @@li_new.wafers, "slot maps must not be identical"
  end
  
  def test12_cleanup
    ($log.warn "skipped"; return) unless @@run_test1.downcase == "true"
    assert Test_VRNDM2.remove_carrier(@@carrier, @@stocker)
  end
  
  def test13_NoVRNDM_op
    ($log.warn "skipped"; return) unless @@run_test1.downcase == "true"
    # process lot at a non-VRNDM Litho op
    lot1 = $sv.lot_list_incassette(@@carrier)[0]
    #
    $log.info "-- processing at non-VRNDM mask operation on a real tool"
    @@li_old = $sv.lot_info(lot1, :wafers=>true)
    assert_equal @@op_NoVRNDM, @@li_old.opNo, "lot #{lot1} is not at opNo #{@@op_NoVRNDM}" 
    res = $sv.lot_hold_release(lot1, nil)
    assert_equal 0, res, "error releasing holds of lot #{lot1}"
    assert $sv.auto_process_carriers(@@vrndm_eqp, @@vrndm_port, @@carrier)
    @@li_new = $sv.lot_info(lot1, :wafers=>true)
    #
    $log.info "-- comparing slotmaps"
    $log.debug "original slotmap: #{@@li_old.wafers.pretty_inspect}"
    $log.debug "current slotmap: #{@@li_new.wafers.pretty_inspect}"
    assert @@li_old.wafers == @@li_new.wafers, "slot maps must be identical"
  end
  
  def test14_cleanup
    ($log.warn "skipped"; return) unless @@run_test1.downcase == "true"
    assert Test_VRNDM2.remove_carrier(@@carrier, @@stocker)
  end
  
  
  # Test2: process 2 lots in one carrier at the VRNDM operation

  def test21_VRNDM_both
    ($log.warn "skipped"; return) unless @@run_test2.downcase == "true"
    lot1 = $sv.lot_list_incassette(@@carrier)[0]
    $sv.lot_hold_release(lot1, nil)
    $sv.lot_futurehold_cancel(lot1, nil)
    lot2 = $sv.lot_split(lot1, 12)
    assert_not_nil lot2
    #
    $log.info "-- executing initial (virtual) processing at lotstart operation"
    assert Test_VRNDM2.claim_process_carrier("O-LOTSTART", @@olotstart_port, @@carrier, @@op_lotstart)
    #
    $log.info "-- locating both lots to VRNDM GatePass operation"
    assert_equal 0, $sv.lot_opelocate(lot1, @@op_gatepass), "error locating lot #{lot1}"
    assert_equal 0, $sv.lot_opelocate(lot2, @@op_gatepass), "error locating lot #{lot2}"
    #
    $log.info "-- processing at the VRNDM operation"
    @@li1_old = $sv.lot_info(lot1, :wafers=>true)
    @@li2_old = $sv.lot_info(lot2, :wafers=>true)
    assert_equal @@op_VRNDM, @@li1_old.opNo, "error in GatePass for lot #{lot1}"
    assert_equal @@op_VRNDM, @@li2_old.opNo, "error in GatePass for lot #{lot2}"
    assert $sv.auto_process_carriers(@@vrndm_eqp, @@vrndm_port, @@carrier)
    @@li1_new = $sv.lot_info(lot1, :wafers=>true)
    @@li2_new = $sv.lot_info(lot2, :wafers=>true)
    #
    $log.info "-- comparing slotmaps per lot"
    $log.debug "original slotmap1: #{@@li1_old.wafers.pretty_inspect}"
    $log.debug "current slotmap1: #{@@li1_new.wafers.pretty_inspect}"
    $log.debug "original slotmap2: #{@@li2_old.wafers.pretty_inspect}"
    $log.debug "current slotmap2: #{@@li2_new.wafers.pretty_inspect}"
    assert_equal @@li1_old.wafers.collect {|w| w.slot}, @@li1_new.wafers.collect {|w| w.slot}, "slots of #{lot1} must be identical"
    assert_equal [], @@li1_old.wafers.collect {|w| w.wafer} - @@li1_new.wafers.collect {|w| w.wafer}, "wafers of #{lot1} must be identical"
    assert_not_equal @@li1_old.wafers.collect {|w| w.wafer}, @@li1_new.wafers.collect {|w| w.wafer}, "slot map of #{lot1} must not be identical"
    assert_equal @@li2_old.wafers.collect {|w| w.slot}, @@li2_new.wafers.collect {|w| w.slot}, "slots of #{lot2} must be identical"
    assert_equal [], @@li2_old.wafers.collect {|w| w.wafer} - @@li2_new.wafers.collect {|w| w.wafer}, "wafers of #{lot2} must be identical"
    assert_not_equal @@li2_old.wafers.collect {|w| w.wafer}, @@li2_new.wafers.collect {|w| w.wafer}, "slot map of #{lot2} must not be identical"
  end
  
  def test22_cleanup
    ($log.warn "skipped"; return) unless @@run_test2.downcase == "true"
    assert Test_VRNDM2.remove_carrier(@@carrier, @@stocker)
  end

  def test23_VRNDM_one_of_two
    ($log.warn "skipped"; return) unless @@run_test2.downcase == "true"
    lots = $sv.lot_list_incassette(@@carrier)
    if lots.size == 1
      # need to split
      lot1 = lots[0]
      $sv.lot_hold_release(lot1, nil)
      $sv.lot_futurehold_cancel(lot1, nil)
      lot2 = $sv.lot_split(lot1, 12)
      assert_not_nil lot2
    else
      lot1, lot2 = lots
    end
    lots = $sv.lot_list_incassette(@@carrier)
    assert_equal 2, lots.size, "there must be 2 lots in carrier #{@@carrier}, found #{lots.inspect}"
    lot1, lot2 = lots
    #
    $log.info "-- executing initial (virtual) processing at lotstart operation"
    # required to reset potential APC information
    assert Test_VRNDM2.claim_process_carrier("O-LOTSTART", @@olotstart_port, @@carrier, @@op_lotstart)
    #
    $log.info "-- locating lot1 to VRNDM GatePass operation"
    assert_equal 0, $sv.lot_opelocate(lot1, @@op_gatepass), "error locating lot #{lot1}"
    assert_equal 0, $sv.lot_opelocate(lot2, @@op_NoVRNDM), "error locating lot #{lot2}"
    #
    $log.info "-- processing at the VRNDM/NoVRNDM operation"
    @@li1_old = $sv.lot_info(lot1, :wafers=>true)
    @@li2_old = $sv.lot_info(lot2, :wafers=>true)
    assert_equal @@op_VRNDM, @@li1_old.opNo, "error in GatePass for lot #{lot1}"
    assert_equal @@op_NoVRNDM, @@li2_old.opNo, "wrong location for lot #{lot2}: #{@@li2_old.opNo}"
    assert $sv.auto_process_carriers(@@vrndm_eqp, @@vrndm_port, @@carrier)
    @@li1_new = $sv.lot_info(lot1, :wafers=>true)
    @@li2_new = $sv.lot_info(lot2, :wafers=>true)
    #
    $log.info "-- comparing slotmaps per lot"
    $log.debug "original slotmap1: #{@@li1_old.wafers.pretty_inspect}"
    $log.debug "current slotmap1: #{@@li1_new.wafers.pretty_inspect}"
    $log.debug "original slotmap2: #{@@li2_old.wafers.pretty_inspect}"
    $log.debug "current slotmap2: #{@@li2_new.wafers.pretty_inspect}"
    assert_equal @@li1_old.wafers.collect {|w| w.slot}, @@li1_new.wafers.collect {|w| w.slot}, "slots of #{lot1} must be identical"
    assert_equal [], @@li1_old.wafers.collect {|w| w.wafer} - @@li1_new.wafers.collect {|w| w.wafer}, "wafers of #{lot1} must be identical"
    assert_not_equal @@li1_old.wafers.collect {|w| w.wafer}, @@li1_new.wafers.collect {|w| w.wafer}, "slot map of #{lot1} must not be identical"
    assert_equal @@li2_old.wafers.collect {|w| w.slot}, @@li2_new.wafers.collect {|w| w.slot}, "slots of #{lot2} must be identical"
    assert_equal [], @@li2_old.wafers.collect {|w| w.wafer} - @@li2_new.wafers.collect {|w| w.wafer}, "wafers of #{lot2} must be identical"
    assert_equal @@li2_old.wafers.collect {|w| w.wafer}, @@li2_new.wafers.collect {|w| w.wafer}, "slot map of #{lot2} must be identical"
  end
  
  def test24_cleanup
    ($log.warn "skipped"; return) unless @@run_test2.downcase == "true"
    assert Test_VRNDM2.remove_carrier(@@carrier, @@stocker)
  end


  def self.reset_carrier(carriers)
    # set carriers AVAILABLE and remove all lot holds and NPW reservations on the sorter
    # return true on success
    carriers = carriers.split if carriers.kind_of?(String)
    ret = true
    carriers.each {|carrier|
      $log.info "reset carrier #{carrier.inspect}"
      lot = $sv.lot_list_incassette(carrier)[0]
      res = $sv.lot_hold_release(lot, nil)
      ret &= (res == 0)
      if $sv.carrier_status(carrier).status != "AVAILABLE"
        res = $sv.carrier_status_change(carrier, "AVAILABLE")
        ret &= (res == 0)
      end
      sleep 10
      ##res = $sv.npw_reserve_cancel(@@sorter, :carrier=>carrier)
      ret &= (res == 0)
    }
    return ret
  end
  
  def self.remove_carrier(carrier, stocker=nil)
    # remove a carrier from a tool after processing
    # return true on success
    ret = true
    $log.info "removing carrier #{carrier} from equipment"
    cs = $sv.carrier_status(carrier)
    eqp = cs.eqp
    ($log.warn "carrier is not at a tool"; return true) if eqp == ""
    eqpi = $sv.eqp_info(eqp)
    port = eqpi.ports.collect {|p| p.port if p.loaded_carrier == carrier}.compact[0]
    ($log.warn "carrier is not loaded at #{eqp}"; return nil) unless port
    # wait for carrier to become ready for unload
    res = $sv.wait_eqp_port_state(eqp, port, "UnloadReq")
    ($log.warn "eqp port #{eqp}:#{port} not in UnloadReq"; return nil) unless res
    # create xfer job, in case AutoProc is not active, ignore any errors
    $sv.carrier_xfer(carrier, eqp, port, stocker, "") if stocker
    # wait for carrier unload
    $sv.wait_eqp_port_state(eqp, port, 'UnloadReq', notequal: true)
  end

  def self.claim_process_carrier(eqp, port, carrier, opNo=nil)
    # ensure lot is properly located, claim OpeStart and OpeComplete
    # return true on success
    #
    # locate lot
    lots = $sv.lot_list_incassette(carrier)
    # release lot holds
    lots.each {|l| 
      $sv.lot_hold_release(l, nil)
      if opNo
        res = $sv.lot_opelocate(l, opNo)
        return nil if res != 0
      end
    }
    # carrier transfer
    cs = $sv.carrier_status(carrier)
    res = $sv.carrier_xfer_status_change(carrier, "MO", cs.stocker)
    return nil if res != 0
    # process lots
    res = $sv.claim_process_lot(lots[0], :carrier=>carrier, :eqp=>eqp, :port=>port)
    # reset carrier xfer_status in any case
    res2 = $sv.carrier_xfer_status_change(carrier, cs.xfer_status, cs.stocker)
    return nil if res2 != 0
    # return process status
    return !!res
  end
  
  def self.waferid_read(eqp, ports, carriers)
    # return true on successful WaferIDRead
    carriers = carriers.split if carriers.kind_of?(String)
    ports = ports.split if ports.kind_of?(String)
    # move carrier to the sorter
    carriers.each_with_index {|carrier, i|
      res = $sv.npw_reserve(eqp, :carrier=>carrier, :port=>ports[i])
      return nil if res != 0
      res = $sv.carrier_xfer(carrier, "", "", eqp, ports[i])
      return nil if res != 0
    }
    res = $sv.wait_carrier_xfer(carriers)
    return nil unless res
    # wait for carrier to be loaded
    carriers.each_with_index {|carrier, i|
      wait_for {
        pi = eqp_info(eqp).ports.find {|p| p.port == ports[i]}
        pi.state == 'LoadComp' && pi.loaded_carrier == carrier
      } || return
    }
    # allow the sorter to recognize the carrier
    sleep 10
    # WaferIDRead on the sorter
    res = $sv.sorter_action_waferidread(eqp, ports, :carrier=>carriers)
    return nil unless res
    # release carrier, will be moved by AutoProc
    ports.each {|port|
      res = $sv.eqp_port_status_change(eqp, port, "UnloadReq")
      return nil if res != 0
    }
    ret = true
    ports.each {|port|
      ret &= $sv.wait_eqp_port_state(eqp, port, 'UnloadReq', notequal: true)
    }
    return ret
  end
  
  def self.get_li_old
    @@li_old
  end
  
  def self.get_li_new
    @@li_new
  end
end
