=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: Steffen Steidten, 2011-01-12

History:
  2013-04-04 ssteidte, renamed from ...810... to 811
  2017-03-20 sfrieske, renamed again to Test851_..
=end

require 'SiViewTestCase'

# Test Merge Holds take precedence over GatePass in a Pre1 script
class Test851_PP_RwkGatePass < SiViewTestCase
  @@route = "UT-MN-UTC001.01"   #"ITDC-MAIN-RWKGATEPASS.01"
  @@product = 'UT-MN-UTC001.01'
  @@op_pre = "100.200"          #"1000.2000"
  @@op_rwk = "100.300"          #"1000.3000"
  @@rwk_route = nil
  @@skript_gatepass_parameter = "UTSkip"
  @@skript_gatepass_value = "YES"

  def after_all_passed
    $sv.delete_lot_family(@@lot)
  end

  def setup
    return unless self.class.class_variable_defined?(:@@lot)
    assert_equal 0, $sv.lot_opelocate(@@lot, :first)
  end


  def test00_setup
    $setup_ok = false
    #
    # create test lot
    assert @@lot = $svtest.new_monitor_lot('Equipment Monitor', route: @@route, product: @@product)
    # find a rework route
    @@rwk_route ||=  $sv.dynamic_routes('Rework').first.route
    $log.info "using rework route #{@@rwk_route}"
    #
    $setup_ok = true
  end

  def test02_verify_gatepass
    $setup_ok = false
    assert_equal 0, $sv.user_parameter_change("Lot", @@lot, @@skript_gatepass_parameter, @@skript_gatepass_value)
    $sv.lot_opelocate(@@lot, @@op_rwk)
    refute_equal @@op_rwk,  $sv.lot_info(@@lot).opNo
    $setup_ok = true
  end

  def test11
    assert_equal 0, $sv.user_parameter_change("Lot", @@lot, @@skript_gatepass_parameter, "")
    assert_equal 0, $sv.lot_opelocate(@@lot, @@op_rwk)
    ### set future hold at current op
    ##assert_equal 0, $sv.lot_futurehold(@@lot, "1000.3000", "RWK", :post=>true, :single=>true)
    # set lot script parameter OOC to "Y" to enable GatePass
    $sv.user_parameter_change("Lot", @@lot, @@skript_gatepass_parameter, @@skript_gatepass_value)
    p = $sv.user_parameter("Lot", @@lot, :name=>@@skript_gatepass_parameter)
    assert_equal @@skript_gatepass_value, p.value, "wrong value of lot scrip parameter #{@@skript_gatepass_parameter}"
    # start partial rework
    assert @@child = $sv.lot_partial_rework(@@lot, 2, @@rwk_route, :dynamic=>true)
    $log.info "child lot #{@@child} is on rework route @@rwk_route"
    # check parent is onhold and future hold is set
    holds = $sv.lot_futurehold_list(@@lot)
    assert holds.inject(false) {|s, h| s |= (h.type == "ReworkHold")}, "Rework future hold is not set for #{@@lot}"
    holds = $sv.lot_hold_list(@@lot)
    assert holds.inject(false) {|s, h| s |= (h.type == "ReworkHold")}, "Rework hold is not set for #{@@lot}"
    # check future hold for child is set
    holds = $sv.lot_futurehold_list(@@child)
    assert holds.inject(false) {|s, h| s |= (h.type == "ReworkHold")}, "Rework future hold is not set for #{@@child}"
    # set script parameter for child lot to activate GatePass
    $sv.user_parameter_change("Lot", @@child, @@skript_gatepass_parameter, @@skript_gatepass_value)
    p = $sv.user_parameter("Lot", @@child, :name=>@@skript_gatepass_parameter)
    assert_equal @@skript_gatepass_value, p.value, "wrong value of lot scrip parameter #{@@skript_gatepass_parameter}"
    # process child at the last operation on the rework route
    ##op = $sv.lot_route_operations(@@child)[-1]
    op = $sv.lot_operation_list(@@child).last
    route, opNo = op.route, op.opNo
    $sv.lot_opelocate(@@child, opNo, :route=>route)
    li = $sv.lot_info(@@child)
    assert_equal route, li.route   # fails (long since)
    assert_equal opNo, li.opNo
    $log.info "child lot #{@@child} is at the last operation on the rework route, processing it"
    eqp = li.opEqps[0]
    $sv.claim_process_lot(@@child, :eqp=>eqp, :mode=>"Off-Line-1", :notify=>false)
    # verify holds and lot position
    [@@lot, @@child].each {|l|
      li = $sv.lot_info(l)
      assert_equal @@route, li.route, "lot #{l} is on the wrong route"
      assert_equal @@op_rwk, li.opNo, "lot #{l} is at the wrong operation"
      holds = $sv.lot_futurehold_list(l)
      assert holds.inject(false) {|s, h| s |= (h.type == "ReworkHold")}, "Rework future hold is not set for #{l}"
      holds = $sv.lot_hold_list(l)
      assert holds.inject(false) {|s, h| s |= (h.type == "ReworkHold")}, "Rework hold is not set for #{l}"
    }
  end

end
