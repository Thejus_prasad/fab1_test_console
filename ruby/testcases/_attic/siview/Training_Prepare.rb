=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: ssteidte, 2015-04-30

History:
=end

require 'SiViewTestCase'
require 'misc'

# Prepare the Fab1 Training lots
class Training_Prepare < SiViewTestCase
  @@eqplist = '^X'
  @@nlots = 30

  def test00_setup
    $setup_ok = false
    #
    assert_equal 'training', $env, "wrong environment"
    #
    $setup_ok = true
  end

  def test11_cleanup
    # make carriers empty and AVAILABLE
    carriers = $sv.carrier_list(carrier: $svtest.carriers).take(@@nlots)
    assert_equal @@nlots, carriers.size, "not enough carriers #{$svtest.carriers}"
    carriers.each {|c| assert $svtest.cleanup_carrier(c, make_empty: true)}
    # cleanup test eqps
    $sv.eqp_list(eqp: @@eqplist).each {|eqp|
      $sv.eqp_cleanup(eqp)
      $sv.eqp_mode_change(eqp, 'Off-Line-1', notify: false)  # XSNK31 no access
    }
  end

  def test21_prepare
    lots = @@nlots.times.collect {unique_string + '.000'}
    assert $svtest.new_lots(lots)
  end
end
