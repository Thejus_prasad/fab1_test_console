=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: Conny Stenzel, 2014-03-05

History:
  2016-11-14 sfrieske,  removed UTDIRTY3 (obsolete carrier category)
=end

require 'RubyTestCase'

class Test_SMObjects < RubyTestCase
  @@SMObjectNames = {
    reticle: ['4048AUGPA1' 'UTRETICLE0003', 'UTRETICLEIBRTCL', 'UTRETICLEFBRTCL', 'UTRETICLE0100', 'UTFPCRETICLE01'],
    rg: ['UTRETICLEGROUP4', 'UTRETICLEGROUP5', 'UTRETICLEGROUP15', 'UTRETICLEGROUP25', 'UTRETICLEGROUP14', 'UTRETICLEGROUP24', 'UTRETICLEGROUP10', 'UTRETICLEGROUP11', 'UTRETICLEGROUP12', 'UTRETICLEGROUP13', 'UTRETICLEGROUP16', 'UTRETICLEGROUP17', 'UTRETICLEGROUP18', 'UTRETICLEGROUP19', 'UTRGIBRTCL'],
    eqp: ['UTA001', 'UTA002', 'UTA003', 'UTA004', 'UTA005', 'UTA006', 'UTC001', 'UTC002', 'UTC003', 'UTF001', 'UTF002', 'UTF003', 'UTI001', 'UTI002', 'UTI003RTCL', 'UTI005', 'UTI006', 'UTR001', 'UTR002', 'UTR003', 'UTS001', 'UTS002', 'DUM2201', 'MES500', 'AWS001', 'AWS2200', 'AWS161', 'PSX150', 'PSX151', 'PSX152', 'PSX153', 'PSX154', 'PSX155', 'CVD400'],
    stocker: ['STO110', 'STO120', 'STO160', 'INTER1', 'UTSTO11', 'UTSTO12', 'ZFG01T001', 'ZFG1010', 'ZFG38A1', 'UTSHF01', 'STK01T001', 'STK02T001', 'OHT01', 'Bay04', 'UTRPSTO110', 'UTRPSTO120'],
    eqptype: [],
    lrcp: ['UTLR-IB-RTCL', 'MSR328785_MEAS2', 'UTLR000', 'UTLR001', 'UTLR011'],
    recipe: ['UTMR000', 'UTMR001', 'UTMR-IB-RTCL'],
    pd: ['UTPD000', 'UTPD001', 'UTPD002', 'UTPD003', 'UTPD004', 'UTPD005', 'UTPD006', 'UTPD007', 'UTPD008', 'UTPD009', 'UTPD010', 'UTRT001', 'BANKIN', 'UT-UTI003-RTCL'],
    modulepd: ['UT-MD-UTC001', 'UT-MD-UTC002', 'UTMD000', 'UTMD-IB-RTCL', '45-BANKIN', 'UT-MD-SIMPLE01', 'UT-MD-LP1', 'UTRW-SCRIPT', 'e2403-UT-SCRIPT-A0', 'e2403-UT-SCRIPT-A1'],
    mainpd: ['UT-AUTOPROC', 'A-AP-MAINPD', 'UTBR000', 'UTRW001', 'UTRT101', 'UTRT001', 'UT-MN-UTR001', 'UT-DN-UTF002', 'e2403-UT-SCRIPT-A1', 'e2403-UT-SCRIPT-A0', 'UTRW-SCRIPT', 'UTRW002', 'UTBR001', 'UTBR002', 'UTRT-IB-RTCL', 'UT-MN-UTC001', 'UT-MN-UTC002', 'UTRT-SCRIPT'],
    technology: ['90NM', '45BK', 'TEST', '45NM', 'RAW', '28BK', '65NM', '32NM', 'SL45NM'],
    productgroup: ['TEST', '1460E'],
    product: ['UTAUTOPROC', 'UT-PRODUCT-FAB1', 'UT-MN-SCRIPT', 'UT-PRODUCT000', 'UT-PRODUCT001', 'UT-PRODUCT002', '30000064.ALL', 'UT-DN-UTF002', 'UT-DN-UTF001', 'UT-MN-UTC002', 'UT-MN-UTC001', 'UT-MN-UTI002', 'UT-RAW', 'UT-IB-RTCL', 'UT-PR-UTI002', 'UT-PROD-SCRIPT'],
    lottype: ['Correlation', 'Engineering', 'Equipment Monitor', 'Process Monitor', 'Production', 'Recycle', 'Vendor', 'Dummy'],
    customer: ['samsung', 'amd', 'broadcom', 'ibm', 'qgt', 'renesas', 'stmicro', 'gf', 'mstar'],
    user: ['X-UNITTEST', 'NOX-UNITTEST', 'OJT-UNITTEST', 'Dummy', '.', ':', 'ID: XXX - ; \t \n \r \r\n , '],
    carrier: ['UF*', 'UK01', 'UT*', 'UX*', 'A3C8', 'A4MN', 'A75S', '38LW', '33B9', 'G*'],
    carriercategory: ['FEOL', 'BEOL', 'UTCLEAN1', 'UTCLEAN2', 'MOL', 'C4', 'C4IN', 'FOI', 'FOSB', 'HK', 'NOSPEC', 'TEST', 'TKEY', 'UTBEOL', 'UTFEOL', 'UTMOL'],
    reticlepod: ['R222', 'R223', 'R224', 'R225', 'R29N', 'UTP009', 'R263', 'R204'],
    bank: ['UT-SHIP', 'W-BACKUPOPER', 'O-BACKUPOPER', 'OY-SHIP', 'TX-SCRAP', 'UT-USE', 'UT-RAW', 'ON-RAW', 'UT-RECYCLE', 'E-NONPROD', 'T-END', 'UT-NONPROD'],
    reasoncode: ['DQE', 'BR', 'ENG', 'LONG', 'INT', 'C-ENG', 'X-WB', 'FCHL', 'ALL', 'X-A0'],
    workarea: ['QA-F1M1', 'QA-F1M2', 'A-TKEY', 'UT', '1-STUTT', 'A-SKIH', 'A-SRTB', 'A-SSJB', 'A-SSJF', 'C-BEOL', 'C-FEOL', 'C-HK', 'C-MOL', 'D-FEOL', 'E-BEOL', 'E1-BOL', 'E1-FOL', 'E1-HK', 'E1-MOL', 'F-FEOL', 'F-METR', 'L-MASK', 'L-METR', 'L-SPRT', 'O-LSTR', 'O-WPCK'],
    equipmentstate: ['2WPR', '1PRD', '2NDP', '4QUL', '4DLY', '4MTN'],
    e10state: ['SBY', 'PRD', 'ENG', 'SDT'],
    fixture: ['UTFIXTURE0010', 'UTFIXTURE0001'],
    fg: ['UTFIXGROUP01'],
    scriptparameter: ['Pull', 'Push', 'CustomerPriority', 'FOD_INIT', 'ShipToEngineer', 'ERPItem', 'CustomerLotGrade', 'OOC', 'FabDPML', 'PassCount', 'MaxProcessWaferCnt', 'IdleTime', 'UTstring', 'UTinteger', 'UTreal', 'UTSkip', 'UTContextKeys', 'UTCurrentPD', 'UTPassCount', 'UTUData'],
  }



  def test000_setup
    $sm = SiView::SM.new($env)
    @@result = {}
  end

  def test001_reticles
    res = object_test(:reticle)
    assert_equal 0, res.size, "error during object test, #{res.size} objects #{res.join(', ')} not successfully edited"
  end

  def test002_reticlegroups
    res = object_test(:rg)
    assert_equal 0, res.size, "error during object test, #{res.size} objects #{res.join(', ')} not successfully edited"
  end

  def test003_equipments
    res = object_test(:eqp)
    assert_equal 0, res.size, "error during object test, #{res.size} objects #{res.join(', ')} not successfully edited"
  end

  def test004_stockers
    res = object_test(:stocker)
    assert_equal 0, res.size, "error during object test, #{res.size} objects #{res.join(', ')} not successfully edited"
  end

  def test005_eqptypes
    res = object_test(:eqptype)
    assert_equal 0, res.size, "error during object test, #{res.size} objects #{res.join(', ')} not successfully edited"
  end

  def test006_lrcps
    res = object_test(:lrcp, split: 0)
    assert_equal 0, res.size, "error during object test, #{res.size} objects #{res.join(', ')} not successfully edited"
  end

  def test007_recipes
    res = object_test(:recipe, split: 1)
    assert_equal 0, res.size, "error during object test, #{res.size} objects #{res.join(', ')} not successfully edited"
  end

  def test008_pds
    res = object_test(:pd, split: 0)
    assert_equal 0, res.size, "error during object test, #{res.size} objects #{res.join(', ')} not successfully edited"
  end

  def test009_modulepds
    res = object_test(:modulepd, split: 0)
    assert_equal 0, res.size, "error during object test, #{res.size} objects #{res.join(', ')} not successfully edited"
  end

  def test010_mainpds
    res = object_test(:mainpd, split: 0)
    assert_equal 0, res.size, "error during object test, #{res.size} objects #{res.join(', ')} not successfully edited"
  end

  def test011_technologies
    res = object_test(:technology)
    assert_equal 0, res.size, "error during object test, #{res.size} objects #{res.join(', ')} not successfully edited"
  end

  def test012_productgroups
    res = object_test(:productgroup)
    assert_equal 0, res.size, "error during object test, #{res.size} objects #{res.join(', ')} not successfully edited"
  end

  def test013_products
    res = object_test(:product, split: 0)
    assert_equal 0, res.size, "error during object test, #{res.size} objects #{res.join(', ')} not successfully edited"
  end

  def test014_lottypes
    res = object_test(:lottype)
    assert_equal 0, res.size, "error during object test, #{res.size} objects #{res.join(', ')} not successfully edited"
  end

  def test015_customers
    res = object_test(:customer)
    assert_equal 0, res.size, "error during object test, #{res.size} objects #{res.join(', ')} not successfully edited"
  end

  def test016_users
    res = object_test(:user)
    assert_equal 0, res.size, "error during object test, #{res.size} objects #{res.join(', ')} not successfully edited"
  end

  def test017_carriers
    # Uncomment the next two lines if new carriers have been added

    #smo = @@SMObjectNames[:carrier].uniq
    #$sm.change_carriers smo, contents: "Wafer"
    res = object_test(:carrier)
    assert_equal 0, res.size, "error during object test, #{res.size} objects #{res.join(', ')} not successfully edited"
  end

  def test018_carriercategorys
    res = object_test(:carriercategory)
    assert_equal 0, res.size, "error during object test, #{res.size} objects #{res.join(', ')} not successfully edited"
  end

  def test019_reticlepods
    res = object_test(:reticlepod)
    assert_equal 0, res.size, "error during object test, #{res.size} objects #{res.join(', ')} not successfully edited"
  end

  def test020_banks
    res = object_test(:bank)
    assert_equal 0, res.size, "error during object test, #{res.size} objects #{res.join(', ')} not successfully edited"
  end

  def test021_reasoncodes
    res = object_test(:reasoncode)
    assert_equal 0, res.size, "error during object test, #{res.size} objects #{res.join(', ')} not successfully edited"
  end

  def test022_workareas
    res = object_test(:workarea)
    assert_equal 0, res.size, "error during object test, #{res.size} objects #{res.join(', ')} not successfully edited"
  end

  def test023_equipmentstates
    res = object_test(:equipmentstate)
    assert_equal 0, res.size, "error during object test, #{res.size} objects #{res.join(', ')} not successfully edited"
  end

  def test024_e10states
    res = object_test(:e10state)
    assert_equal 0, res.size, "error during object test, #{res.size} objects #{res.join(', ')} not successfully edited"
  end

  def test025_fixtures
    res = object_test(:fixture)
    assert_equal 0, res.size, "error during object test, #{res.size} objects #{res.join(', ')} not successfully edited"
  end

  def test026_fgs
    res = object_test(:fg)
    assert_equal 0, res.size, "error during object test, #{res.size} objects #{res.join(', ')} not successfully edited"
  end

  def test027_scriptparameters
    res = object_test(:scriptparameter)
    assert_equal 0, res.size, "error during object test, #{res.size} objects #{res.join(', ')} not successfully edited"
  end

  #MSR 898010
  def test030_save_edit_complete_one_tx
    classname = :pd
    tmpl_obj = "#{@@SMObjectNames[:pd].first}.01"
    oinfos = $sm.object_info(classname, tmpl_obj, raw: true)
    new_obj = "PD-#{Time.now.to_i}.01"
    oinfos[0].objectId = $sm.oid(new_obj)
    assert $sm.save_edit_complete(classname, oinfos)
    assert $sm.delete_production(classname, new_obj), "failed to delete #{new_obj}"
  end

  def test031_save_edit_complete_multi_tx
    classname = :pd
    tmpl_obj = "#{@@SMObjectNames[:pd].first}.01"
    oinfos = $sm.object_info(classname, tmpl_obj, raw: true)
    #
    new_obj = "PD-#{Time.now.to_i}.01"
    oinfos[0].objectId = $sm.oid(new_obj)
    assert $sm.save_objects(classname, oinfos), "failed to save #{new_obj}"
    assert $sm.edit_complete(classname, new_obj), "failed to edit & complete #{new_obj}"
    assert $sm.release_objects(classname, new_obj), "failed to release #{new_obj}"
    assert $sm.delete_production(classname, new_obj), "failed to delete #{new_obj}"
  end

  def test099_summary
    $log.info "Test duration:\n#{@@result.pretty_inspect}"
  end


  def object_test(smobject, params={})
    tstart = Time.now
    failededit, notfound, wildcardobjects, oinames = [], [], [], []
    split = params[:split] || -1
    smo = @@SMObjectNames[smobject].uniq.sort
    smo.each {|oname|
      wildcardobjects << oname.sub('*', '') if oname.include?('*')
      oi = $sm.object_info(smobject, oname, params)
      notfound << oname if oi.size == 0
      oi.each {|o|
        ##if split != "last"
          oinames << o.name.split('.')[split]
        ##else
        ##  oinames << o.name.split('.').last
        ##end
      }
    }
    wildcardobjects.each {|wco| notfound << "#{wco}*" if oinames.any? {|oname| oname.start_with?(wco)} == false}
    oinames.each {|oname|
      object = $sm.edit_objects(smobject, [oname], params) {|oinfos| oinfos.size > 0}
      edited = true if object != nil
      failededit << oname if edited != true
    }
    $log.warn "-!- SM Object(s) #{notfound.join(', ')} could not be found -!-" if notfound.size > 0
    @@result[smobject] = Time.now - tstart
    return failededit
  end

end
