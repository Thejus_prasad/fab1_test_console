=begin 
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.


To create a prod-like lot:
  @@lot = "G02G3.000"
  @@wafers = ["PGVQK046MX", "PGVQK045MX", "PGVQK044MX", "PGVQK043MX", "PGVQK042MX", 
    "PGVQK041MX", "PGVQK040MX", "PGVQK039MX", "PGVQK038MX", "PGVQK037MX", 
    "PGVQK036MX", "PGVQK035MX", "PGVQK034MX", "PGVQK033MX", "PGVQK032MX", 
    "PGVQK031MX", "PGVQK030MX", "PGVQK029MX", "PGVQK028MX", "PGVQK027MX", 
    "PGVQK026MX", "PGVQK172MX", "PGVQK171MX", "PGVQK170MX", "PGVQK169MX"]
  @@sublottype = "PR"
  @@customer = "samsung"
  params = {:product=>@@product, :route=>@@route, :lot=>@@lot, :waferids=>@@wafers, :customer=>@@customer, :sublottype=>@@sublottype, :stbdelay=>1, :carrier=>"UT37"}
  $sv.new_lot_release(params)
  
Tools: either LETCL1000 or ETX502 with LAM2300 emulator
    
Author: Steffen Steidten, 2012-08-03
=end

require "RubyTestCase"
require 'dummytools'
require 'toolevents'

# To create a prod-like lot:
#  @@lot = "G02G3.000"
#  @@wafers = ["PGVQK046MX", "PGVQK045MX", "PGVQK044MX", "PGVQK043MX", "PGVQK042MX", 
#    "PGVQK041MX", "PGVQK040MX", "PGVQK039MX", "PGVQK038MX", "PGVQK037MX", 
#    "PGVQK036MX", "PGVQK035MX", "PGVQK034MX", "PGVQK033MX", "PGVQK032MX",
#    "PGVQK031MX", "PGVQK030MX", "PGVQK029MX", "PGVQK028MX", "PGVQK027MX", 
#    "PGVQK026MX", "PGVQK172MX", "PGVQK171MX", "PGVQK170MX", "PGVQK169MX"]
#
#  @@sublottype = "PR"
#  @@customer = "samsung"
#  params = {:product=>@@product, :route=>@@route, :lot=>@@lot, :waferids=>@@wafers, :customer=>@@customer, :sublottype=>@@sublottype, :stbdelay=>1, :carrier=>"UT37"}
#  $sv.new_lot_release(params)

class Test_LRNDM < RubyTestCase
  Description = "Test Logical Randomize with virtual equipment"
  
  @@lot = "QG02G3.000"  # "G02G3.000" # real lot from Prod
  
  @@op_lotstart = "LOTSTART.01"
  @@eqp_lotstart = "A-DUMMY"
  
  @@op_process = "ITDC-PADNIT.01"
  @@eqp_process = "ALC1001"
  
  @@op_LRNDM = "ITDC-LRNDMOP.01"
  @@eqp_LRNDM = "LETCL1000"   #"ETX502" or "LETCL1000"  # <-- this must be a real EI or LET EI, no JavaEI!!
  @@port_LRNDM = "P1"
  
  @@op_PRNDM = "ITDC-LRNDM-ILINRNDM.01"
  @@eqp_PRNDM = "UTS001"
  
  @@stocker = "UTSTO11"
  
  @@product = "3014A1AA.C1"   #"ITDC-LRNDM.01"
  @@route = "ITDC-LRNDM.01"

  def test00_setup
    $setup_ok = false
    $eis = Dummy::EIs.new($env)
    $einotify = Dummy::EINotification.new($env, @@eqp_LRNDM)
    $wh = ToolEvents::WaferHistory.new($env, :sv=>$sv)
    #
    li = $sv.lot_info(@@lot)
    $sv.lot_bankin_cancel(@@lot) if li.states["Lot Inventory State"] == "InBank"
    $sv.eqp_unload(li.eqp, nil, li.carrier) if li.xfer_status == "EI"
    $sv.lot_hold_release(@@lot, nil)
    assert_equal 0, $sv.lot_opelocate(@@lot, nil, :op=>@@op_lotstart), "error locating lot to #{@@op_lotstart}"
    eqi = $sv.eqp_info(@@eqp_process)
    $sv.eqp_status_change_req(@@eqp_process, "2WPR") unless eqi.status == "SBY.2WPR"
    eqi.chambers.each {|ch| $sv.chamber_status_change_req(eqi.eqp, ch.chamber, "2WPR") unless ch.status == "SBY.2WPR"}
    eqi = $sv.eqp_info(@@eqp_LRNDM)
    $sv.eqp_status_change_req(@@eqp_LRNDM, "2WPR") unless eqi.status == "SBY.2WPR"
    eqi.chambers.each {|ch| $sv.chamber_status_change_req(eqi.eqp, ch.chamber, "2WPR") unless ch.status == "SBY.2WPR"}
    $setup_ok = true
  end
  
  def test01_lotstart_process
    $setup_ok = false
    assert $eis.restart_tool(@@eqp_lotstart), "error restarting eqp #{@@eqp_lotstart}"
    li = $sv.lot_info(@@lot)
    assert_equal 0, $sv.carrier_xfer_status_change(li.carrier, "EO", @@eqp_lotstart)
    assert $sv.claim_process_lot(@@lot, :eqp=>@@eqp_lotstart), "error processing lot at #{@@op_lotstart}"
    assert $eis.restart_tool(@@eqp_process), "error restarting eqp #{@@eqp_process}"
    assert $sv.eqp_status_change_req(@@eqp_process, "2WPR")
    assert $sv.claim_process_lot(@@lot, :eqp=>@@eqp_process, :mode=>"Auto-1", :wafer_history=>$wh), "error processing lot on #{@@eqp_process}"
    $setup_ok = true
  end
  
  def test02_LRNDM_after_process
    $setup_ok = false
    li = $sv.lot_info(@@lot, :wafers=>true)
    assert_equal @@op_LRNDM, li.op, "wrong operation: #{li.op} instead of #{@@op_LRNDM}"
    res = $sv.user_data_route_operation(@@route, li.opNo)
    assert_equal "LRNDM", res["RouteOpAutomation"], "wrong RoutePD UDATA: #{res.inspect} at opNo #{li.opNo}"
    assert_equal 0, $sv.claim_carrier_stockin(li.carrier, @@stocker)
    assert process_LRNDM_auto1(@@eqp_LRNDM, @@port_LRNDM, li.carrier, :javaei=>false)
    li2 = $sv.lot_info(@@lot, :wafers=>true)
    assert_equal li.wafers, li2.wafers, "wafer map has changed"
    $setup_ok = true
  end
  
  def test03_PRNDM
    $setup_ok = false
    assert $eis.restart_tool(@@eqp_PRNDM), "error restarting eqp #{@@eqp_PRNDM}"
    li = $sv.lot_info(@@lot, wafers: true)
    assert_equal @@op_PRNDM, li.op, "wrong operation: #{li.op} instead of #{@@op_PRNDM}"
    callback = Proc.new {$sv.wafer_sort_rpt(@@lot, li.carrier, eqp: @@eqp_PRNDM, slots: 'forcedrandomize')}
    assert $sv.claim_process_lot(@@lot, slr: true, eqp: @@eqp_PRNDM, mode: 'Auto-1', proctime: callback), "error processing lot on #{@@eqp_PRNDM}"
    li2 = $sv.lot_info(@@lot, wafers: true)
    assert_not_equal li.wafers, li2.wafers, "not randomized"
    $setup_ok = true
  end
  
  def test04_LRNDM_LRNDM
    $setup_ok = false
    # first LRNDM
    li = $sv.lot_info(@@lot, :wafers=>true)
    assert_equal @@op_LRNDM, li.op, "wrong operation: #{li.op} instead of #{@@op_LRNDM}"
    res = $sv.user_data_route_operation(@@route, li.opNo)
    assert_equal "LRNDM", res["RouteOpAutomation"], "wrong RoutePD UDATA: #{res.inspect} at opNo #{li.opNo}"
    assert_equal 0, $sv.claim_carrier_stockin(li.carrier, @@stocker)
    assert process_LRNDM_auto1(@@eqp_LRNDM, @@port_LRNDM, li.carrier, :javaei=>false)
    li2 = $sv.lot_info(@@lot, :wafers=>true)
    assert_equal li.wafers, li2.wafers, "wafer map has changed"
    # second LRNDM
    li = $sv.lot_info(@@lot, :wafers=>true)
    assert_equal @@op_LRNDM, li.op, "wrong operation: #{li.op} instead of #{@@op_LRNDM}"
    res = $sv.user_data_route_operation(@@route, li.opNo)
    assert_equal "LRNDM", res["RouteOpAutomation"], "wrong RoutePD UDATA: #{res.inspect} at opNo #{li.opNo}"
    assert_equal 0, $sv.claim_carrier_stockin(li.carrier, @@stocker)
    assert process_LRNDM_auto1(@@eqp_LRNDM, @@port_LRNDM, li.carrier, :javaei=>false)
    li2 = $sv.lot_info(@@lot, :wafers=>true)
    assert_equal li.wafers, li2.wafers, "wafer map has changed"
    $setup_ok = true
  end
  
  def test05_noLRNDM_LRNDM
    $setup_ok = false
    # first (no LRNDM)
    li = $sv.lot_info(@@lot, :wafers=>true)
    assert_equal @@op_LRNDM, li.op, "wrong operation: #{li.op} instead of #{@@op_LRNDM}"
    res = $sv.user_data_route_operation(@@route, li.opNo)
    assert_nil res["RouteOpAutomation"], "wrong RoutePD UDATA: #{res.inspect} at opNo #{li.opNo}"
    assert_equal 0, $sv.claim_carrier_stockin(li.carrier, @@stocker)
    assert process_LRNDM_auto1(@@eqp_LRNDM, @@port_LRNDM, li.carrier, :javaei=>false)
    li2 = $sv.lot_info(@@lot, :wafers=>true)
    assert_equal li.wafers, li2.wafers, "wafer map has changed"
    # second (LRNDM)
    li = $sv.lot_info(@@lot, :wafers=>true)
    assert_equal @@op_LRNDM, li.op, "wrong operation: #{li.op} instead of #{@@op_LRNDM}"
    res = $sv.user_data_route_operation(@@route, li.opNo)
    assert_equal "LRNDM", res["RouteOpAutomation"], "wrong RoutePD UDATA: #{res.inspect} at opNo #{li.opNo}"
    assert_equal 0, $sv.claim_carrier_stockin(li.carrier, @@stocker)
    assert process_LRNDM_auto1(@@eqp_LRNDM, @@port_LRNDM, li.carrier, :javaei=>false)
    li2 = $sv.lot_info(@@lot, :wafers=>true)
    assert_equal li.wafers, li2.wafers, "wafer map has changed"
    $setup_ok = true
  end
  
  def process_LRNDM_auto1(eqp, port, carrier, params={})
    # return true on success
    $log.info "process_LRNDM #{eqp.inspect}, #{port.inspect}, #{params.inspect}"
    res = $sv.eqp_mode_auto1(eqp)
    return nil if res != 0
    sleep 3
    res = $sv.carrier_xfer_status_change(carrier, "SI", @@stocker)
    res = $sv.carrier_xfer_status_change(carrier, "EO", eqp)
    return nil if res != 0
    cj = $sv.slr(eqp, :carrier=>carrier)
    return nil unless cj
    if eqp.start_with?("LETCL")
      res = $einotify.xfer_complete(carrier, port, "EI")
      return nil if res != 0
    else
      puts "\n\n Send Carrier Placed signal and press return!"
      gets
    end
    res = $sv.wait_cj(eqp, cj)
    return nil unless res
    unless eqp.start_with?("LETCL")
      puts "\n\n Send Carrier Removed signal and press return!"
      gets
    end
    # wait for unload
    sleep 120
    return true
  end
    
  def process_LRNDM_auto2(eqp, port, carrier, params={})
    # return true on success
    $log.info "process_LRNDM #{eqp.inspect}, #{port.inspect}, #{params.inspect}"
    res = $sv.eqp_mode_auto2(eqp)
    return nil if res != 0
    res = $sv.auto_process_carriers(eqp, port, carrier)
    return nil unless res
    res = $sv.eqp_unload(eqp, port, carrier)
    return nil if res != 0
    res = $sv.eqp_port_status_change(eqp, port, "LoadReq")
    return nil if res != 0
    return true
  end
end
