=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.
Author: Paul Peschel     2013-08-01

History:
  2014-12-16 ssteidte, renamed from Test_MSR571678 to Test990_LotDelete, code cleanup
=end

require 'SiViewTestCase'

# Test for MSR571678, fixed in R10.0.1-16 core (C6.16, check for sorter jobs before deleting lots)
# this test is obsolete since C7.4 due to MSR815910
class Test990_LotDelete < SiViewTestCase
  @@sorter = 'UTS001'
  
  def test00_setup
    $setup_ok = false      
    assert_equal 0, $sv.eqp_cleanup(@@sorter)
    assert $eis.restart_tool(@@sorter)
    assert_equal 0, $sv.eqp_mode_auto1(@@sorter)
    $setup_ok = true
  end

  def test01_sorter_job
    # lot preparation, eqp monitor product is its own source product
    srclot = $svtest.new_monitor_lot('Equipment Monitor')
    assert_equal 0, $sv.lot_opelocate(srclot, 'last')
    li = $sv.lot_info(srclot)
    $sv.lot_bankin(srclot) unless li.status == 'COMPLETED'
    startbank = $sv.product_list(product: li.product, productcategory: 'Equipment Monitor').first.startbank
    assert_equal 0, $sv.lot_bank_move(srclot, startbank)
    # enable lot deletion in SM
    assert $sm.change_sublottype(li.sublottype, 0)
    # create SJ and change status to 'Error' to prevent execution
    ec = $svtest.get_empty_carrier
    assert sj = $sv.sj_create(@@sorter, 'P1', 'P2', lot, ec)
    assert_equal 0, $sv.sj_status_change(@@sorter, sj, 'Error')
    # STB control lot from srclot to remove the wafers from srclot ## not working anymore since C7.4
    li = $sv.lot_info(srclot)
    assert lot = $sv.stb_controllot(li.product, li.lottype, srclot: srclot), "failed to STB new lot"
    # try to delete with SJ
    assert_equal 2049, $sv.lot_delete(lot)
    # cancel SJ and try again, SJC may already have canceled the failed SJ
    $sv.sj_cancel(sj)
    sleep 30
    assert_equal 0, $sv.lot_delete(srclot)
    # cleanup
    $sv.delete_lot_family(lot)
    $sm.change_sublottype(li.sublottype, 1)
  end

end