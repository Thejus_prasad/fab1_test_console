=begin 
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.
    
Author: Daniel Steger, 2011-07

# $Rev: 16957 $
# $LastChangedDate: 2014-08-25 11:27:06 +0200 (Mo, 25 Aug 2014) $
# $LastChangedBy: dsteger $
=end

require "RubyTestCase"

# "Register new carriers in SiView"
# MSR484446
class Test002_CarrierRegister < RubyTestCase
    Description = "Register new carriers in SiView"

    @@carrier_count = 30
    @@prefix = "UTX"
    @@carrier_category="FEOL"
    
    def generate_carrierids
      ids = @@carrier_count.times.collect do |i|
        @@prefix + "%.3d" % i
      end
    end
    
    def test01
      _ids = generate_carrierids
      # delete the carriers, don't care about any errors
      _ids.each {|c| $sv.carrier_status_change c, "NOTAVAILABLE"}
      $sv.durable_delete _ids
      # try to create the carriers
      $sv.durable_register _ids, @@carrier_category
      _ids.each do |c|
        rc = $sv.carrier_status_change c, "AVAILABLE"
        assert_equal 0, rc, "something is wrong with carrier #{c}"
      end
    end
end
