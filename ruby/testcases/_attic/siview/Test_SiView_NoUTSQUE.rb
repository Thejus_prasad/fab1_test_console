=begin 
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.
    
Author: Steffen Steidten, 2011-07-27

=end

require "RubyTestCase"
require 'svlog'

# Verify no entries are written to the UTS queue
class Test_SiView_NoUTSQUE < RubyTestCase
  Description = "Verify no entries are written to the UTS queue"
  
  # TXs to check
  @@txs = %w(TXBKC010 TXDFC002 TXDSC001 TXDSC002 TXDSC007 TXEWC004 TXEWC005 TXEWC006 TXEWC007 TXEWC008 TXEWC012 TXPCC044 TXOMC004 TXOMC010 TXOMC011 TXPCC018 TXEQC015 TXPDR008 TXPDR011 TcEWC006 TcEWC007 TXTRC014 TXTRC015 TXTRC016 TXTRC017 TXTRC019 TXTRC023 TXTRC026 TXTRC027 TXTRC028 TXTRC029 TXTRC034 TXTRC035 TXTRC046 TXTRC047 ThTRC014 TcTRC019 TcTRC028 TcTRC047 TXTRC083)
  @@txfound = []
  @@txnotfound = []
  
  @@tminsv = '2011-07-14 00:00:00.000'
  
  def setup
    #super
    puts "\n"
    $setup_ok = ($setup_ok != false)
    assert $setup_ok, "test setup is not correct"
  end
    
  def test01_setup
    $setup_ok = false
    $mmdb = DB.new("siviewmm.#{$env}")
    assert $mmdb.connected?, "no connection to MM db"
    $svlog = SvLog.new($env)
    assert $svlog.db.connected?, "no connection to svlog db"
    @@tmin = Time.parse(@@tminsv)
    $setup_ok = true
  end
  
  def test11_svlog_check
    return
    @@txs.each_with_index {|tx, i|
      n = $svlog.get_txcount :tx=>tx, :tmin=>@@tmin, :tmax=>Time.now
      $log.info "found #{n} entries for #{tx} (#{i+1}/#{@@txs.size})"
      if n == 0
        @@txnotfound << tx
      else
        @@txfound << tx
      end
    }
    assert_equal [], @@txnotfound, "some TXs have not been executed in #{$env} since #{@@tminsv}"
  end
  
  def test12_utsque_check
    q = "select count(*) from SIVIEW.FQMSGUTSQUE where EVENT_TIME > ?"
    res = $mmdb.select_one(q, @@tminsv)
    assert_not_nil res, "MM db connection failed"
    assert_equal 0, res[0], "unexpected entries in FQMSGUTSQUE"
  end 
  
  def test98_stats
    puts "\nTX Execution Check#{Time.now}"
    puts "=" * 32
    puts "TXs found in svlog    : #{@@txfound.size}"
    puts "TXs not found in svlog: #{@@txnotfound.size} (#{@@txnotfound.inspect})"
  end
  
  def self.lookup_tx(tx)
    $mmdb.select_one("select NOTE_CONTENTS from SIVIEW.FIMMTRAN where TX_ID=?", tx)[0]
  end
  
  def self.stats
    return {:txfound=>@@txfound, :txnotfound=>@@txnotfound}
  end
end
