=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: Daniel Steger, 2012-11-07
=end

require "RubyTestCase"
require "misc"

# Test Reticle and ReticleGroup operations, including SmartFacade
class Test_Contamination < RubyTestCase
    Description = "Test CSS scripting. Requires SM setup"

    @@lot = "8XYK10002.000"#"8XYK03006.000"
    @@zone1_start = "2000.1100"
    @@zone1_end = "2000.1400"
    @@zone2_start = "2000.1500"
    @@zone2_end = "2000.1700"
    
    @@route = "UTRT-CONTAMINATE.01"
    @@route_2 = "UTRT-CONTAMINATE.02"
    @@product = "UT-PROD-CONTAMINATE.01"
    @@product_2 = "UT-PROD-CONTAMINATE.02"
    @@ope_route_2 = "1000.1200"
    
    @@opnum = "1000.2000"
    @@opnum_skip = "1000.4000"
    @@reasoncode = "EEEE"
    @@pd = "BANKIN.01" # PD for wich UDATA is queried in script
    @@eqp = "UTC003"

    @@branch_route = "b-FEM-CONTAMINATE.01"
    @@branch_op = "2000.1000"

    @@dbranch_route = "d-UT-CONTAMINATE.01"


    @@erf_route = "e12345-CONTAMINATE-A0.01"
    @@erf_endop = "2000.1000"

    @@rework_route = "R-UT-CONTAMINATE.01"

    @@rework_route_2 = "R-UT-CONTAMINATE.02"
    @@rework_endop = "1000.3000"

    @@nonpro_bank = "OFFSITE-BANK"



    @@script_params = {
      "ContaminantMaterialOnWafers" => [nil, "Resist"],
      "ContaminantRiskNextStep" => ["", "Dry resist strip", "Wet resist strip", "OTHER"],
      "ContaminantRiskSegmentID" => ["", "RX-Litho", "RC-Litho"]
    }
    
    def self.create_lot
      $sv.new_lot_release :product=>@@product, :route=>@@route, :stb=>true, :waferids=>"t7m12"
    end

    
    def test10_pre1_combinationcheck
      omit "only on logic change"
      _prepare_lot(@@lot, 'first')
      @@outfile = open("log/#{__method__}_#{Time.now.iso8601.gsub(":",".")}.csv", "w+")
      @@outfile.puts "ContaminantMaterialOnWafers;ContaminantRiskSegmentID;ContaminantRiskNextStep;RequiredStep;NextStepRequiredToNeutralize;RiskSegment;Hold"
      opNums = (1000..2100).step(100).map {|i| "1000.#{i}"}
      opNums.each do |opNo|         
        _force = {:force => $sv.lot_info(@@lot).status == "ONHOLD"}
        $sv.lot_opelocate(@@lot, opNo, _force)
        _set_scriptparameters(@@lot, @@script_params.keys, :_do_pre1test)
      end
      @@outfile.close
    end

    def test11_disable_combinationcheck
      omit "only on logic change"
      _prepare_lot(@@lot, 'first')
      assert $sv.user_parameter_set_verify("Factory", "Fab8", "ContaminationAvoidanceSkipOrRun", "Skip"), "Factory Scriptparameter not set"
      @@outfile = open("log/#{__method__}_#{Time.now.iso8601.gsub(":",".")}.csv", "w+")
      @@outfile.puts "ContaminantMaterialOnWafers;ContaminantRiskSegmentID;ContaminantRiskNextStep;RequiredStep;NextStepRequiredToNeutralize;RiskSegment;Hold"
      noholds = true
      opNums = (1000..2100).step(100).map {|i| "1000.#{i}"}
      opNums.each do |opNo|
        _force = {:force => $sv.lot_info(@@lot).status == "ONHOLD"}
        $sv.lot_opelocate(@@lot, opNo, _force)
        noholds &= _set_scriptparameters(@@lot, @@script_params.keys, :_do_pre1test)
      end
      @@outfile.close
      assert noholds, "No hold expected"
    end


    def test20_post_combinationcheck
      omit "only on logic change"
      _prepare_lot(@@lot, 'first')
      # Disable the PRE1-Check, because we are only interested in Post script
      assert $sv.user_parameter_set_verify("Factory", "Fab8", "ContaminationAvoidanceSkipOrRun", "Skip"), "Factory Scriptparameter not set"
      @@outfile = open("log/#{__method__}_#{Time.now.iso8601.gsub(":",".")}.csv", "w+")
      @@outfile.puts "ContaminantMaterialOnWafers;ContaminantRiskSegmentID;ContaminantRiskNextStep;RequiredStep;NextStepRequiredToNeutralize;RiskSegment;Hold"
      opNums = (1000..2100).step(100).map {|i| "1000.#{i}"}
      opNums.each do |opNo|
        _force = {:force => $sv.lot_info(@@lot).status == "ONHOLD"}
        $sv.lot_opelocate(@@lot, opNo, _force)
        _set_scriptparameters(@@lot, @@script_params.keys, :_do_posttest)
      end
      @@outfile.close
    end

    
    def test30_contaminant_normal_scenario
      _prepare_lot(@@lot, @@zone1_start)
      lot_info = $sv.lot_info @@lot
      
      # Process cont zone #1
      while lot_info.opNo <= @@zone1_end
        $sv.claim_process_lot @@lot, :eqp=>@@eqp
        lot_info = $sv.lot_info @@lot
        assert lot_info.status != "ONHOLD", "Lot should not be onhold"
      end
      
      # Check script parameters are empty
      assert verify_scriptparameters_cleared(@@lot), "Not all script parameters are cleared"
      assert $sv.lot_opelocate(@@lot, @@zone2_start), "Failed to move to next contamination zone" 
      # Process cont zone #2
      while lot_info.opNo <= @@zone2_end
        $sv.claim_process_lot @@lot, :eqp=>@@eqp
        lot_info = $sv.lot_info @@lot
        assert lot_info.status != "ONHOLD", "Lot should not be onhold"        
      end
      
      # Check script parameters are empty
      assert verify_scriptparameters_cleared(@@lot), "Not all script parameters are cleared"      
    end
    
    def test31_contaminant_required_step_missed
      _prepare_lot(@@lot, @@zone1_start)
      lot_info = $sv.lot_info @@lot
      
      # Process 1st step at cont zone #1
      $sv.claim_process_lot @@lot, :eqp=>@@eqp
      lot_info = $sv.lot_info @@lot
      assert lot_info.status != "ONHOLD", "Lot should not be onhold"
      
      # Locate to last step at cont zone #1
      assert $sv.lot_opelocate(@@lot, @@zone1_end), "Lot locate failed"
      assert verify_contamination_hold(@@lot, "Err07", @@zone1_start), "Contamination hold expected"
    end
    
    def test32_contaminant_other_segment
      _prepare_lot(@@lot, @@zone1_start)
      lot_info = $sv.lot_info @@lot
      
      # Process 1st step at cont zone #1
      $sv.claim_process_lot @@lot, :eqp=>@@eqp
      lot_info = $sv.lot_info @@lot
      assert lot_info.status != "ONHOLD", "Lot should not be onhold"
      
      # Locate to last step at cont zone #2
      assert $sv.lot_opelocate(@@lot, @@zone2_end), "Lot locate failed"
      assert verify_contamination_hold(@@lot, "Err05", @@zone1_start), "Contamination hold expected"
    end

    
    
    def test40_contaminant_rework_with_neutralize
      _prepare_lot(@@lot, @@zone1_start)
      lot_info = $sv.lot_info @@lot
      
      while lot_info.opNo < @@zone1_end
        # Process 1st step at cont zone #1
        cur_ope = lot_info.opNo
        $sv.claim_process_lot @@lot, :eqp=>@@eqp
        lot_info = $sv.lot_info @@lot
        log_scriptparameters(@@lot)
        assert lot_info.status != "ONHOLD", "Lot should not be onhold"
        next_ope = lot_info.opNo
        
        # Do rework
        assert $sv.lot_rework(@@lot, @@rework_route, :return_opNo=>@@zone1_start, :dynamic=>true), "failed to start rework"        
        process_until(@@lot, @@zone1_start)
        
        # process until next operation
        lot_info = process_until(@@lot, next_ope)
      end      
    end
    
    def test41_contaminant_rework_in_segment
      _prepare_lot(@@lot, @@zone2_start)
      
      # No rework for first operation in risk segment
      $sv.claim_process_lot @@lot, :eqp=>@@eqp
      lot_info = $sv.lot_info @@lot
      
      while lot_info.opNo < @@zone2_end
        # Process 1st step at cont zone #1
        cur_ope = lot_info.opNo
        $sv.claim_process_lot @@lot, :eqp=>@@eqp
        lot_info = $sv.lot_info @@lot
        log_scriptparameters(@@lot)
        assert lot_info.status != "ONHOLD", "Lot should not be onhold"
        next_ope = lot_info.opNo
        
        # Do rework
        assert $sv.lot_rework(@@lot, @@rework_route_2, :return_opNo=>cur_ope, :dynamic=>true), "failed to start rework"        
        process_until(@@lot, cur_ope)
        
        # process until next operation
        lot_info = process_until(@@lot, next_ope)
      end      
    end

    def test42_contaminant_rework_wrong_step
      _prepare_lot(@@lot, @@zone1_start)
      lot_info = $sv.lot_info @@lot
      
      # Process 1st step at cont zone #1
      $sv.claim_process_lot @@lot, :eqp=>@@eqp
      lot_info = $sv.lot_info @@lot
      log_scriptparameters(@@lot)
      assert lot_info.status != "ONHOLD", "Lot should not be onhold"

      # Do rework
      assert $sv.lot_rework(@@lot, @@rework_route_2, :return_opNo=>@@zone1_start, :dynamic=>true), "failed to start rework"
      assert $sv.lot_opelocate(@@lot, @@rework_endop), "Error in locate"
      assert verify_contamination_hold(@@lot, "Err08", @@zone1_start), "Contamination hold expected"
    end
        
    def test50_contaminant_chg_route
      _prepare_lot(@@lot, @@zone1_start)
      lot_info = $sv.lot_info @@lot
      
      # Process 1st step at cont zone #1
      $sv.claim_process_lot @@lot, :eqp=>@@eqp
      lot_info = $sv.lot_info @@lot
      assert lot_info.status != "ONHOLD", "Lot should not be onhold"
      
      # Change route/product and move to other cont zone #3
      assert $sv.schdl_change(@@lot, :route=>@@route_2, :product=>@@product_2, :opNo=>@@ope_route_2), "Schedule Change failed"
      assert verify_contamination_hold(@@lot, "Err05", @@zone1_start), "Contamination hold expected"
    end

    def test60_contaminant_mig_locate
      _prepare_lot(@@lot, @@zone1_start)
      # Locate lot to operations after OPL (first operation of segment)
      ops = $sv.lot_route_lookahead(@@lot).select {|op| op.opNo <= @@zone1_end}

      ops.each do |op|
        reset_scriptparameters(@@lot)
        $sv.lot_opelocate(@@lot, op.opNo)
        lot_info = $sv.lot_info @@lot
        assert lot_info.status != "ONHOLD", "Lot should not be onhold"

        # Process lot
        process_until(@@lot, @@zone2_start)
        assert lot_info.status != "ONHOLD", "Lot should not be onhold"
      end
    end

    def test61_contaminant_mig_branch_route
      _prepare_lot(@@lot, @@zone1_start)
      # Branch lot to operations after OPL (first operation of segment)
      ops = $sv.lot_route_lookahead(@@lot).select {|op| op.opNo <= @@zone1_end}

      ops.each do |op|
        reset_scriptparameters(@@lot)
        $sv.lot_opelocate(@@lot, @@zone1_start)
        
        assert $sv.lot_branch(@@lot, @@branch_route, :retop=>op.opNo, :dynamic=>true), "Branch failed"
        $sv.lot_opelocate(@@lot, @@branch_op)

        $sv.lot_gatepass(@@lot)
        lot_info = $sv.lot_info @@lot
        assert lot_info.status != "ONHOLD", "Lot should not be onhold"

        # Process lot
        process_until(@@lot, @@zone2_start)
        assert lot_info.status != "ONHOLD", "Lot should not be onhold"
      end
    end

    def test62_contaminant_mig_erf_route
      _prepare_lot(@@lot, @@zone1_start)
      # Split lot to operations after OPL (first operation of segment)
      ops = $sv.lot_route_lookahead(@@lot).select {|op| op.opNo <= @@zone1_end}

      ops.each do |op|
        reset_scriptparameters(@@lot)
        assert $sv.lot_experiment_delete(@@lot), "cannot delete ERFs"

        $sv.lot_opelocate(@@lot, @@zone1_start)
        assert $sv.lot_experiment(@@lot, 2, @@erf_route, @@zone1_start, op.opNo, :dynamic=>true), "cannot assign ERF"
        $sv.lot_hold(@@lot, "ENG")
        $sv.lot_hold_release(@@lot, nil)

        child = $sv.lot_family(@@lot)[-1]
        assert $sv.lot_opelocate(child, @@erf_endop), "cannot locate #{child} to end of ERF"

        $sv.lot_opelocate(@@lot, op.opNo)

        # Merge parent and child
        assert $sv.lot_merge(@@lot, child), "cannot merge lots"

        lot_info = $sv.lot_info @@lot
        assert lot_info.status != "ONHOLD", "Lot should not be onhold"

        # Process lot
        process_until(@@lot, @@zone2_start)
        assert lot_info.status != "ONHOLD", "Lot should not be onhold"
      end
    end

    def test71_dbranch_route
      _prepare_lot(@@lot, @@zone1_start)
      
      reset_scriptparameters(@@lot)
      $sv.lot_opelocate(@@lot, @@zone1_start)

      # Process 1st step at cont zone #1
      $sv.claim_process_lot @@lot, :eqp=>@@eqp
      lot_info = $sv.lot_info @@lot

      assert $sv.lot_branch(@@lot, @@dbranch_route, :dynamic=>true), "Branch failed"

      # Process lot
      process_until(@@lot, @@zone2_start)

      lot_info = $sv.lot_info @@lot
      assert lot_info.status != "ONHOLD", "Lot should not be onhold"
    end

    def test72_dbranch_clean_hold
      _prepare_lot(@@lot, @@zone1_start)

      reset_scriptparameters(@@lot)
      $sv.lot_opelocate(@@lot, @@zone1_start)

      # Process 1st step at cont zone #1
      $sv.claim_process_lot @@lot, :eqp=>@@eqp

      assert $sv.lot_branch(@@lot, @@dbranch_route, :retop=>@@zone1_end, :dynamic=>true), "Branch failed"

      # Process lot
      assert $sv.lot_opelocate(@@lot, @@zone1_end), "failed to locate lot"

      assert verify_contamination_hold(@@lot, "Err07", @@zone1_start), "Contamination hold expected"
    end

    def test73_dbranch_zone_hold
      _prepare_lot(@@lot, @@zone1_start)

      reset_scriptparameters(@@lot)
      $sv.lot_opelocate(@@lot, @@zone1_start)

      # Process 1st step at cont zone #1
      $sv.claim_process_lot @@lot, :eqp=>@@eqp

      assert $sv.lot_branch(@@lot, @@dbranch_route, :retop=>@@zone2_start, :dynamic=>true), "Branch failed"

      # Process lot
      assert $sv.lot_opelocate(@@lot, @@zone2_start), "failed to locate lot"

      assert verify_contamination_hold(@@lot, "Err05", @@zone1_start), "Contamination hold expected"
    end
            
    def _prepare_lot(lot, opno)
      reset_scriptparameters(lot)
      lot_info = $sv.lot_info(lot)
      if lot_info.route != @@route
        assert $sv.schdl_change(lot, :route=>@@route, :product=>@@product), "Schedule Change failed"
      end
      assert $sv.lot_cleanup(lot, opNo: opno)
      xfer_state = lot_info.xfer_status
      if xfer_state =~ /[HMS]I/
        assert_equal 0, $sv.carrier_xfer_status_change(lot_info.carrier, 'EO', @@eqp)        
      elsif xfer_state.end_with?('I')
        flunk "Cannot recover xfer state of #{lot_info.carrier}"
      end
    end
    
    def _set_scriptparameters(lot, script_params, func)
      if script_params == []
        return send(func, lot)
      else
        par_name = script_params[0]
        @@script_params[par_name].each do |value|
          $sv.user_parameter_set_or_delete("Lot", lot, par_name, value)
          _set_scriptparameters(lot, script_params[1..-1], func)
        end
      end
    end

    def _do_posttest(lot)
      $sv.lot_hold_release(lot, nil)
      assert $sv.claim_process_lot(lot, :eqp=>@@eqp)      
      $sv.lot_opelocate(lot, -1) # need to have locate before to get correct udata
      _show_values(lot)
    end
    
    def _do_pre1test(lot)
      assert $sv.lot_hold(lot, "ENG")
      $sv.lot_hold_release(lot, nil)
      _show_values(lot)      
    end

    def _show_values(lot)
      _linfo = $sv.lot_info(lot)
      _udata_vals = $sv.user_data_route_operation(_linfo.route, _linfo.opNo)
      _script_parm = $sv.user_parameter("Lot", lot)
      _mat_wafer = _script_parm.select {|s| s.name == "ContaminantMaterialOnWafers"}.map {|s| s.value}
      _risk_seg = _script_parm.select {|s| s.name == "ContaminantRiskSegmentID"}.map {|s| s.value}
      _risk_next = _script_parm.select {|s| s.name == "ContaminantRiskNextStep"}.map {|s| s.value}
      _strres = "#{_mat_wafer};" +
                "#{_risk_seg};" +
                "#{_risk_next};" +
                "#{_udata_vals["RequiredStep"]};" +
                "#{_udata_vals["NextStepRequiredToNeutralize"]};" +
                "#{_udata_vals["RiskSegment"]}"
      if _linfo.status == "ONHOLD"
        _holds = $sv.lot_hold_list(lot)
        _strres += ";HOLD;" + "#{_holds[0].memo}"
        _strres += ";more than one hold!" if _holds.size > 1
      end
      @@outfile.puts(_strres)
      return _holds.nil?
    end

    def _get_user_parameter(params, name)
      _context = params.select {|p| p.name == name}
      return nil unless _context.count > 0 #"script parameter #{name} not found"
      return _context[0].value
    end

    def _cleanup_experiments
      assert $sv.lot_opelocate @@lot, "first"
      assert $sv.merge_lot_family(@@lot)
      assert $sv.lot_experiment_delete(@@lot)
      # move lots to current point PD
      assert $sv.lot_opelocate(@@lot, "first")
    end
    
    def reset_scriptparameters(lot)
      assert $sv.user_parameter_set_verify("Factory", "Fab8", "ContaminationAvoidanceSkipOrRun", "Run"), "Factory Scriptparameter not set"

      assert $sv.user_parameter_delete_verify("Lot", lot, "ContaminantMaterialOnWafers")
      assert $sv.user_parameter_delete_verify("Lot", lot, "ContaminantRiskSegmentID")
      assert $sv.user_parameter_delete_verify("Lot", lot, "ContaminantRiskNextStep")
    end
    
     def log_scriptparameters(lot)
      _params = $sv.user_parameter("Lot", lot)
      $log.info("MaterialOnWafer=#{_get_user_parameter(_params, "ContaminantMaterialOnWafers")}" +
        " RiskSegment=#{_get_user_parameter(_params, "ContaminantRiskSegmentID")}" +
        " NextStep=#{_get_user_parameter(_params, "ContaminantRiskNextStep")}")
    end
    
    def verify_scriptparameters_cleared(lot)
      _params = $sv.user_parameter("Lot", lot)
      res = true
      res &= verify_equal("", _get_user_parameter(_params, "ContaminantMaterialOnWafers"), "ContaminantMaterialOnWafers")
      res &= verify_equal("", _get_user_parameter(_params, "ContaminantRiskSegmentID"), "ContaminantRiskSegmentID")
      res &= verify_equal("", _get_user_parameter(_params, "ContaminantRiskNextStep"), "ContaminantRiskNextStep")
      return res
    end

    def verify_contamination_hold(lot, error, opNo=nil)
      _holds = $sv.lot_hold_list(lot, :reason=>"RRSK")
      res = verify_equal(1, _holds.size, "Expected contamination hold")
      res &= _holds[0].memo.include?(error)
      if opNo
        res &= _holds[0].memo.include?(opNo)
        ##skip for now: res &= verify_equal(opNo, _holds[0].rsp_op, " wrong responsible operation #{_holds[0].rsp_op} instead of #{opNo}")
      end
      return res
    end
    
    
    def process_until(lot, op_no)
      lot_info = $sv.lot_info lot
      while lot_info.opNo != op_no
        $sv.claim_process_lot lot, :eqp=>@@eqp
        lot_info = $sv.lot_info lot
        log_scriptparameters(lot)
        assert lot_info.status != "ONHOLD", "Lot should not be onhold"        
      end
      return lot_info
    end

    def xxxtest99_contamination_stress
      nthreads = 20
      lots = $sv.lot_list :product=>@@product
      route_op = $sv.route_operations(@@route).select {|op| op.op != "BANKIN.01"}
      $log.info "Lots per operation: #{lots.count / route_op.count}"
      lslice = (1.0 * lots.count / nthreads).ceil

      # Reset and distribute lots
      lots.each_with_index do |lot,i|
        _prepare_lot(lot, route_op[i % route_op.count].opNo)
        assert $sv.user_parameter_set_verify("Lot", lot, "ContaminantMaterialOnWafers", "Resist")
        assert $sv.user_parameter_set_verify("Lot", lot, "ContaminantRiskSegmentID", "RX-Litho")
        assert $sv.user_parameter_set_verify("Lot", lot, "ContaminantRiskNextStep", "Wet resist strip")
      end

      # Precondition

      assert $sv.user_parameter_set_verify("Factory", "Fab8", "ContaminationAvoidanceSkipOrRun", "Run"), "Factory Scriptparameter not set"
      #assert $sv.user_parameter_delete_verify("Factory", "Fab8", "ContaminationAvoidanceSkipOrRun"), "Factory Scriptparameter not deleted"

      threads = []
      nthreads.times do |t|
        #threads << LocateThread.new(t, lots.slice(t*lslice, lslice), @@eqp)
        threads << RunBRScriptThread.new(t, lots.slice(t*lslice, lslice), @@eqp)
        threads << ScriptParameterThread.new(t, lots.slice(t*lslice, lslice), @@eqp)
      end
      threads.each {|t| t.run }
      time = 30*60
      $log.warn "Waiting #{time/60} min to complete"
      sleep time
      threads.each {|t| t.stop }
      threads.each {|t| t.join }
    end

    class LocateThread < LoopThread
      def initialize(t, lots, eqp)
        $log.info "#{t}"
        @lots = (lots or [])
        super() {     
          @lots.each {|lot|
            $sv.lot_opelocate(lot, +1)
            sleep 1
            $sv.user_parameter("Lot", lot)
            sleep 1
          }
        }
      end

      def before_loop()
        $log.warn "Start locate for #{@lots.inspect}"
      end

      def after_loop()
        $log.warn "Stopping thread"
      end
    end

    class RunBRScriptThread < LoopThread
      def initialize(t, lots, eqp)
        $log.info "#{t}"
        @lots = (lots or [])
        super() {
          @lots.each {|lot|
            #$sv.lot_opelocate(lot, +1)
            $sv.run_script(lot, eqp)
            sleep 1
          }
        }
      end

      def before_loop()
        $log.warn "Start script runs for #{@lots.inspect}"
      end

      def after_loop()
        $log.warn "Stopping thread"
      end
    end


    class ScriptParameterThread < LoopThread
      def initialize(t, lots, eqp)
        $log.info "#{t}"
        @lots = (lots or [])
        super() {
          @lots.each {|lot|
            $sv.user_parameter("Lot", lot)
            sleep 1
          }
        }
      end

      def before_loop()
        $log.warn "Start script parameter queries for #{@lots.inspect}"
      end

      def after_loop()
        $log.warn "Stopping thread"
      end
    end
end
