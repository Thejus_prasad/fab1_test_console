=begin 
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.
    
Author: Paul Peschel, 2013-07-31

History:
  2015-07-01 ssteidte,  cleaned up and verified with R15

=end

require 'SiViewTestCase'


# Test Probe Card Support through Fixtures in SiView, MSR658285, MSR612416
class Test140_Fixture < SiViewTestCase
  @@eqp = 'UTF003'
  @@stocker = 'UTFSHF01'
  @@fid = 'UTFIXTURE0001'
  @@fid2 = 'UTFIXTURE0010'
  @@fgr = 'UTFIXGROUP01'
  @@status = ['AVAILABLE', 'INUSE', 'SCRAPPED', 'NOTAVAILABLE']
  @@opNo = '1000.995'
  

  def self.after_all_passed
    @@sv.delete_lot_family(@@lot)
  end


  def test00_setup
    $setup_ok = false
    #
    assert @@lot = @@svtest.new_lot
    assert_equal 0, @@sv.lot_opelocate(@@lot, @@opNo)
    $log.info "using lot #{@@lot.inspect}"
    _clean_inhibits
    assert_equal 0, @@sv.eqp_cleanup(@@eqp)
    #
    $setup_ok = true
  end
  
  def test10_fixture_status_change
    assert_equal 0, @@sv.fixture_status_change(@@fid, @@status[0], check: true)
    @@status[1..3].each {|s|
      assert_equal 0, @@sv.fixture_status_change(@@fid, s)
      assert_equal s, @@sv.fixture_status(@@fid).status
    }
  end
  
  def test11_fixture_eqp_in_out
    # Fixture out EQP
    assert_equal 0, @@sv.fixture_status_change(@@fid, @@status[0], check: true)
    assert_equal 0, @@sv.fixture_status_change(@@fid2, @@status[0], check: true)
    assert_equal 0, @@sv.fixture_status_change(@@fid, @@status[3], check: true)
    assert_equal 0, @@sv.fixture_status_change(@@fid2, @@status[3], check: true)
    assert_equal 0, @@sv.fixture_xfer_status_change(@@fid, 'EO', eqp: @@eqp)
    assert_equal 0, @@sv.fixture_xfer_status_change(@@fid2, 'EO', eqp: @@eqp)
    assert_equal 0, @@sv.fixture_status_change(@@fid, @@status[0], check: true)
    assert_equal 0, @@sv.fixture_status_change(@@fid2, @@status[0], check: true)
    @@sv.slr(@@eqp, lot: @@lot)
    assert_equal 1407, @@sv.rc, "Is not possible, because Fixture is not on Tool"
    # Fixture 1 in EQP
    assert_equal 0, @@sv.fixture_xfer_status_change(@@fid, "EI", eqp: @@eqp)
    @@sv.slr(@@eqp, lot: @@lot)
    assert_equal 1407, @@sv.rc, "Is not possible, because Fixture is not on Tool"
    # Fixture 2 in EQP
    assert_equal 0, @@sv.fixture_xfer_status_change(@@fid2, "EI", eqp: @@eqp)
    assert cj = @@sv.slr(@@eqp, lot: @@lot), "error in SLR"
    assert_equal 0, @@sv.slr_cancel(@@eqp, cj)
  end
  
  def test12_fixture_stocker_in_out
    # Fixture in stocker
    assert_equal 0, @@sv.fixture_xfer_status_change(@@fid, "HI", stocker: @@stocker)
    assert_equal [@@fid], @@sv.fixture_stocker_info(@@stocker)
    # Fixture out stocker
    assert_equal 0, @@sv.fixture_xfer_status_change(@@fid, "HO", stocker: @@stocker)
    @@sv.fixture_stocker_info(@@stocker)
    assert_equal 1421, @@sv.rc, "Is not possible, because Fixture is not on Stocker"
    assert_equal 0, @@sv.fixture_xfer_status_change(@@fid, "EI", eqp: @@eqp)
  end
  
  def test13_fixture_use_reset
    assert_equal 0, @@sv.fixture_usage_reset(@@fid)
  end
  
  def test14_fixture_inhibit
    # Fixture Inhibit
    _clean_inhibits
    _run_test(:fixture, @@fid, "Is not possible, because Fixture is Inhibit")
  end

  def test15_fixturegroup_inhibit
    # Fixturegroup Inhibit
    _clean_inhibits
    _run_test(:fg, @@fgr, "Is not possible, because Fixturegroup is Inhibit")
  end
  
  def test16_fixture_eqp_inhibit
    # Fixture and EQP Inhibit
    _clean_inhibits
    _run_test(:fixture_eqp, [@@fid, @@eqp], "Is not possible, because Fixture and EQP is Inhibit")
  end 
  
  def test17_fixturegroup_eqp_inhibit
    # Fixturegroup and EQP Inhibit
    _clean_inhibits
    _run_test(:fg_eqp, [@@fgr, @@eqp], "Is not possible, because Fixturegroup and EQP is Inhibit")
  end

  
  def _clean_inhibits
    @@sv.inhibit_cancel(:fixture, @@fid, silent: true)
    @@sv.inhibit_cancel(:fg, @@fgr, silent: true)
    @@sv.inhibit_cancel(:fixture_eqp, [@@fid, @@eqp], silent: true)
    @@sv.inhibit_cancel(:fg_eqp, [@@fgr, @@eqp], silent: true)
  end
  
  def _run_test(itype, entity, emsg)
    assert @@sv.inhibit_entity(itype, entity)
    cj = @@sv.slr(@@eqp, lot: @@lot)
    assert_equal 1407, @@sv.rc, emsg
    # Inhibit Exception
    assert_equal 0, @@sv.inhibit_exception_register(@@lot, itype, entity)
    assert @@sv.claim_process_lot(@@lot, eqp: @@eqp, claim_carrier: true)
    assert_equal 0, @@sv.lot_opelocate(@@lot, @@opNo)
    sleep 30
    cj = @@sv.slr(@@eqp, lot: @@lot)
    assert_equal 1407, @@sv.rc, emsg
    assert_equal 0, @@sv.inhibit_cancel(itype, entity)
  end
end
