=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.
Author: Beate Kochinka    2010-08-13
        Daniel Steger     2013-03-25

History:
        2014-03-28 dsteger, dual CMMS support via userid (MSR841753)
=end

require 'misc'
require 'RubyTestCase'
require 'dummytools'

# Dual CMMS Support (MSR841753)
# Run this test in the following scenarios:
#   1) DUALCMMS is OFF and no UDATA defined
#   2) DUALCMMS is OFF and UDATA set to Xsite / SAPPM
#
#   3) DUALCMMS is ON and Tool UDATA set to Xsite
#   4) DUALCMMS is ON and Tool UDATA set to SAPPM
#   5) DUALCMMS is ON and Tool UDATA is invalid / empty
class Test213_DualCMMS < RubyTestCase
  Description = "Tests various E10 status changes, incl MSR704108 (transport eqp)"

  UserMap = {
    "Xsite" => "X-XSITE",
    "SAPPM" => "X-SAP"
  }

  @@cmms_udata = "Xsite" # or SAPPM or empty
  @@users = "X-SAP" # X-XSITE" # environment users

  @@eqps = "UTC001 UTI001"
  @@eqps_chamber = "UTC001:PM3 UTI001:MODULE1"  #same tools, but with the chamber

  @@lot = "U2A6A.00"
  @@opnos = "1000.700 1000.800"

  @@work_area = "UT"

  @@e10_notify = "ON"    # E10 notification to CEI should work
  @@clientnode = "CMMS"  # Xsite and SAPPM will both send this
  @@dual_cmms = "OFF"    # nil/OFF - is single CMMS or "ON"

  def test00_setup
    $setup_ok = false
    # environment var check
    assert_equal @@dual_cmms, ($sv.environment_variable[1]["CS_SP_DUALCMMS_ENABLE"] or ""), "incorrect value for DUALCMMS"
    if @@dual_cmms != ""
      UserMap.each_pair {|type,user|
        assert_equal user, $sv.environment_variable[1]["CS_SP_CMMS_USERID_#{type.upcase}"], "incorrect user #{user} for #{type}"
      }
    $setup_ok = true
    end

    # retrieve AMHS equipments in workarea
    _list = $sv.workarea_eqp_list(@@work_area, :raw=>true)
    @@eqps_amhs = ["Intra Bay", "Auto", "Shelf", "BareReticle", "ReticlePod", "ReticleShelf", "Fixture"].collect {|eqptype|
      entity = _list.strAreaStocker.find {|s| s.stockerType == eqptype}
      assert entity, "failed to find eqp of type #{eqptype}"
      entity.stockerID.identifier
    }
    @@eqps = @@eqps.split

    # Update udata according setup
    $sm = SiView::SM.new($env)
    @@eqps.each do |eqp|
      if ($sm.udata_attrs(:eqp).find {|u| u.name == "CMMS"} && @@cmms_udata != "")
        assert $sm.update_udata(:eqp, eqp, {"CMMS"=>@@cmms_udata}), "error writing UDATA in SM"
        u = $sv.user_data(:eqp, eqp)["CMMS"] || ""   # not reported when empty string
        assert_equal @@cmms_udata, u, "UDATA CMMS not correctly set in SM"
      end
    end
    @@eqps_amhs.each do |stocker|
      if ($sm.udata_attrs(:stocker).find {|u| u.name == "CMMS"} && @@cmms_udata != "")
        assert $sm.update_udata(:stocker, stocker, {"CMMS"=>@@cmms_udata}), "error writing UDATA in SM"
        u = $sv.user_data(:stocker, stocker)["CMMS"] || ""   # not reported when empty string
        assert_equal @@cmms_udata, u, "UDATA CMMS not correctly set in SM"
      end
    end

    $eis = Dummy::EIs.new($env) unless $eis
    @@eqps.each {|e| $eis.restart_tool(e)}
  end


  def _determine_rc(entity, user, is_stocker=false)
    _cmms = $sv.user_data((is_stocker ? :stocker : :eqp), entity)["CMMS"]
    #determine if Udata matches user
    if @@dual_cmms == "ON"
      if user
        if _cmms
          _rc = (UserMap[_cmms] != user) ? 10919 : 0
        else
          _rc = 10918 # invalid UDATA
        end
      else
        _rc = 0
      end
    else
      _rc = 0
    end
    return _rc
  end

  ############# AMHS Equipment ####################

  def test01_cmms_e10_state_changes_transport
    (@@users.split+[nil]).each do |user|
      @@eqps_amhs.each do |eqp|
        ["2NDP", "2WPR"].each do |state|
          assert _e10_state_change_stocker(@@work_area, eqp, state, user)
        end
      end
    end
  end

  # Send E10 state  update and check resulting E10 state (if not wrong user given)
  def _e10_state_change_stocker(work_area, entity, new_state, user=nil)
    _rc = _determine_rc(entity, user, true)
    res = verify_equal _rc, $sv.eqp_status_change_req(entity, new_state, client_node: @clientnode, user: user)

    _state = (_rc == 0) ? new_state : "2WPR"
    _list_new = $sv.workarea_eqp_list(work_area, :raw=>true)
    entity = _list_new.strAreaStocker.find {|s| s.stockerID.identifier == entity}
    res &= verify_equal(_state, entity.stockerStatus.identifier, " wrong E10 state")

    return res
  end

  ############### EQUIPMENT #######################

  def test02_cmms_e10_eqp_state_change_req
    (@@users.split+[nil]).each do |user|
      @@eqps.each do |eqp|
        ["2NDP", "4DLY", "4MTN", "4QUL", "2NDP", "2WPR"].each do |state|
          assert _e10_state_change_eqp(eqp, state, user), " E10 state change failed for #{eqp} and user #{user}"
        end
      end
    end
  end

  def _e10_state_change_eqp(eqp, new_state, user=nil)
    _rc = _determine_rc(eqp, user)
    res = verify_equal _rc, $sv.eqp_status_change_req(eqp, new_state, client_node: @@clientnode, user: user)
    _state = (_rc == 0) ? new_state : "2WPR"
    res &= _verify_e10_state(eqp, _state)
    return res
  end

  def _verify_e10_state(eqp, state)
    e_info = $sv.eqp_info(eqp)
    res = verify_equal state, e_info.status.split(".")[-1], " wrong state in SiView"
    sleep 1 #to allow sync
    res &= verify_equal state, $eis.e10state(eqp), " wrong state reported to EI" if @@e10_notify == "ON"
    return res
  end

  def test03_pm_event_for_slr
    (@@users.split+[nil]).each do |user|
      @@eqps.each_with_index do |eqp,i|
        _prepare(eqp, @@opnos.split()[i])
        $sv.eqp_mode_change(eqp, 'Auto-1')
        cj = $sv.slr(eqp, :lot=>@@lot)
        _rc = _determine_rc(eqp, user)
        assert_equal _rc, $sv.eqp_pm_event(eqp, state: "2NDP", client_node: @@clientnode, user: user), "Can't register PM event for #{eqp} and #{user}"
        assert_equal((_rc == 0) ? 1 : 0, $sv.inhibit_list(:eqp, eqp, :reason=>"RQPM").count, "inhibit for PM expected")
        assert_equal _rc, $sv.eqp_pm_event_cancel(eqp, state: "2NDP", client_node: @@clientnode, user: user), "Can't cancel PM event for #{eqp} and #{user}"
        assert_equal(0, $sv.inhibit_list(:eqp, eqp, :reason=>"RQPM").count, "inhibit for PM expected")
        $sv.slr_cancel(eqp, cj)
        assert _verify_e10_state(eqp, "2WPR"), "wrong E10 state"
      end
    end
  end

  ################## CHAMBER ######################

  def test10_e10_change_state_change_req
    (@@users.split+[nil]).each do |user|
      @@eqps_chamber.split.each do |ec|
        eqp, ch = ec.split(":")
        # Tool needs to be online-remote
        assert_equal 0, $sv.eqp_mode_change(eqp, 'Auto-1'), "failed to switch modeq"
        ["2NDP", "4DLY", "4MTN", "4QUL", "2NDP", "2WPR"].each do |state|
          assert _e10_state_change_chamber(eqp, ch, state, user), " E10 state change failed for #{ec} and user #{user}"
        end
      end
    end
  end

  def _e10_state_change_chamber(eqp, chambers, new_state, user)
    _rc = _determine_rc(eqp, user)
    chambers = [chambers] unless chambers.kind_of?(Array)
    res = verify_equal _rc, $sv.chamber_status_change_req(eqp, chambers, new_state, client_node: @@clientnode, user: user)
    _state = (_rc == 0) ? new_state : "2WPR"
    assert _verify_e10_state_chamber(eqp, chambers, _state), "wrong E10 state"
    return res
  end

  def _verify_e10_state_chamber(eqp, chambers, new_state)
    res = true
    c_info = $sv.eqp_info(eqp).chambers
    chambers.each do |chamber|
      ch_info = c_info.find {|ch| ch.chamber == chamber }
      verify_condition("Chamber not found", true) { ch_info }
      res &= verify_equal new_state, ch_info.status.split(".")[-1], " wrong state in SiView"
      sleep 1 #to allow sync
      res &= verify_equal new_state, $eis.e10state(eqp, chamber), " wrong state reported to EI" if @@e10_notify == "ON"
    end
    return res
  end

  def test11_pm_chamber_event_for_slr
    (@@users.split+[nil]).each do |user|
      @@eqps_chamber.split.each_with_index do |ec,i|
        eqp, ch = ec.split(":")
        _prepare(eqp, @@opnos.split()[i])
        $sv.eqp_mode_change(eqp, 'Auto-1')
        cj = $sv.slr(eqp, :lot=>@@lot)
        assert $eis.set_cj_chambers(eqp, cj, [ch])
        _rc = _determine_rc(eqp, user)
        assert_equal _rc, $sv.chamber_pm_event(eqp, ch, state: "2NDP", client_node: @@clientnode, user: user), "Can't register PM event for #{eqp} and #{user}"
        assert_equal((_rc == 0) ? 1 : 0, $sv.inhibit_list(:eqp_chamber, eqp, :attrib=>[ch], :reason=>"RQPM").count, "inhibit for PM expected")
        assert_equal _rc, $sv.eqp_pm_event_cancel(eqp, chamber: ch, state: "2NDP", client_node: @@clientnode, user: user), "Can't cancel PM event for #{eqp} and #{user}"
        assert_equal(0, $sv.inhibit_list(:eqp_chamber, eqp, :attrib=>[ch], :reason=>"RQPM").count, "inhibit for PM expected")
        $sv.slr_cancel(eqp, cj)
        assert _verify_e10_state_chamber(eqp, [ch], "2WPR"), "wrong E10 state"
      end
    end
  end

  def _prepare(eqp, opno)
    # Cleanup eqp
    assert_equal 0, $sv.eqp_cleanup(eqp), "can' cleanup #{eqp}"

    $eis.set_cj_chambers(eqp)
    # Lots
    l_info = $sv.lot_info @@lot
    $sv.lot_bankin_cancel @@lot if l_info.status == "COMPLETED"
    $sv.lot_hold_release(@@lot, nil) if l_info.status == "ONHOLD"
    $sv.merge_lot_family(@@lot)
    assert $sv.lot_opelocate(@@lot, opno), "failed to locate #{@@lot} to operation"
    $sv.eqp_mode_change(eqp, 'Off-Line-1')
    assert _e10_state_change_eqp(eqp, "2NDP"), " E10 state change failed"
    assert _e10_state_change_eqp(eqp, "2WPR"), " E10 state change failed"
    #
  end
end
