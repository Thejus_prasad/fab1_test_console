=begin 
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.
    
Author: Steffen Steidten, 2010-12-03
=end

require "RubyTestCase"
require 'apcei'

# Test Virtual Randomize with real equipment in Auto-2 mode
class Test_VRNDM1 < RubyTestCase
  Description = "Test Virtual Randomize with real equipment in Auto-2 mode"
  
  @@run_sorter_ops = "false"
  @@run_lot1 = "true"
  @@run_lot2 = "false"
  @@run_combined = "false"
  
  @@carrier1 = "A315" #"D304"
  @@carrier2 = "B316"
  
  @@sorter = "AWS001"
  @@sorter_port1 = "P1"
  @@sorter_port2 = "P3"
  
  @@stocker = "STO160"
  
  @@vrndm_eqp = "ALC1001"
  @@vrndm_port1 = "P1"
  @@vrndm_port2 = "P2"
  
  @@olotstart_port = "P6"
  
  @@op_lotstart = "1000.1000"
  @@op_gatepass = "2000.2000"
  @@op_VRNDM = "2000.3000"
  @@op_NoVRNDM = "2000.4000"
  @@op_sorter_RNDM = "2000.5000"
  
  @@li_old = nil
  @@li_new = nil

  def setup
    super
    $setup_ok = ($setup_ok != false)
    assert $setup_ok, "test setup is not correct"
  end

  def test01_carriers
    # clean up carriers
    $setup_ok &= Test_VRNDM1.reset_carrier(@@carrier1)
    $setup_ok &= Test_VRNDM1.remove_carrier(@@carrier1, @@stocker)
    $setup_ok &= Test_VRNDM1.reset_carrier(@@carrier2)
    $setup_ok &= Test_VRNDM1.remove_carrier(@@carrier2, @@stocker)
    assert $setup_ok, "carriers #{@@carrier1}, #{@@carrier2} needs to be recovered"
  end
  
  def test03_apc
    # verify APC is working, use carrier2!!
    $setup_ok = false
    lot = $sv.lot_list_incassette(@@carrier2)[0]
    $sv.lot_hold_release(lot, nil)
    $sv.lot_futurehold_cancel(lot, nil)
    tref = Time.now
    $sv.lot_opelocate(lot, @@op_gatepass)
    apc = APC::EI.new($env, :sv=>$sv)
    res = apc.get_VRNDM_data(@@vrndm_eqp, lot, :recipe=>"T175CT20_6VS45", :recipe_context=>"ALC-Process")
    assert_equal 6, res.size
    $log.info "APC timestamp: #{res[2]}"
    assert (Time.parse(res[2]) - tref).abs < 120
    $setup_ok = true
  end
  
  def test02_eqp_mode
    # try to set the port mode
    $sv.eqp_mode_change(@@vrndm_eqp, "Auto-2", :port=>[@@vrndm_port1, @@vrndm_port2])
    # check
    eqpi = $sv.eqp_info(@@vrndm_eqp)
    eqpi.ports.each {|p| $setup_ok &= p.mode == "Auto-2" if [@@vrndm_port1, @@vrndm_port2].member?(p.port)}
  end

  #
  # lot 1: VRNDM at the Litho op, no VRNDM at the next Litho op, randomize on a sorter
  #
  
  def test11_VRNDM
    ($log.warn "skipped"; return) unless @@run_lot1.downcase == "true"
    $setup_ok = false
    # "happy randomize" szenario with one lot 
    lot1 = $sv.lot_list_incassette(@@carrier1)[0]
    #
    #
    if @@run_sorter_ops.downcase == "true"
      $log.info "-- executing initial WaferIDRead on a real sorter"
      assert wait_for(timeout: 1800) {sorter_jobs(eqp, port: @@sorter_port1).to_a.first.last.nil?}, "old sorter job on #{@@sorter} #{@@sorter_port1}"
      assert_equal 0, $sv.eqp_mode_auto2(@@sorter), "error setting sorter in mode Auto-1"
      assert $sv.wait_eqp_port_state(@@sorter, @@sorter_port1, 'UnloadReq', notequal: true), "check #{@@sorter} port #{@@sorter_port1}"
      assert Test_VRNDM1.reset_carrier(@@carrier1), "error preparing carrier #{@@carrier1}"
      assert Test_VRNDM1.waferid_read(@@sorter, @@sorter_port1, @@carrier1), "error in WaferIDRead for #{@@carrier1}"
      assert $sv.wait_eqp_port_state(@@sorter, @@sorter_port1, "UnloadReq")
      assert $sv.wait_eqp_port_state(@@sorter, @@sorter_port1, 'UnloadReq', notequal: true), "check #{@@sorter} port #{@@sorter_port1}"
      assert_equal 0, $sv.lot_requeue(lot1), "error re-queueing lot #{lot1}"
    end
    #
    $log.info "-- executing initial (virtual) processing at lotstart operation"
    assert Test_VRNDM1.claim_process_carrier("O-LOTSTART", @@olotstart_port, @@carrier1, @@op_lotstart)
    #
    $log.info "-- locating to VRNDM GatePass operation"
    assert_equal 0, $sv.lot_opelocate(lot1, @@op_gatepass), "error locating lot #{lot1}"
    #
    $log.info "-- processing at the VRNDM operation on a real tool"
    @@li_old = $sv.lot_info(lot1, :wafers=>true)
    assert_equal @@op_VRNDM, @@li_old.opNo, "error in GatePass for lot #{lot1}"
    assert $sv.auto_process_carriers(@@vrndm_eqp, @@vrndm_port1, @@carrier1)
    @@li_new = $sv.lot_info(lot1, :wafers=>true)
    #
    $log.info "-- comparing slotmaps"
    $log.debug "original slotmap: #{@@li_old.wafers.pretty_inspect}"
    $log.debug "current slotmap: #{@@li_new.wafers.pretty_inspect}"
    assert @@li_old.wafers != @@li_new.wafers, "slot maps must not be identical"
    $setup_ok = true
  end
  
  def test12_cleanup
    ($log.warn "skipped"; return) unless @@run_lot1.downcase == "true"
    $log.info "clean up, waiting for carrier to be removed from #{@@vrndm_eqp} #{@@vrndm_port1}"
    # wait for carrier to become ready for unload
    assert $sv.wait_eqp_port_state(@@vrndm_eqp, @@vrndm_port1, "UnloadReq")
    # create xfer job, in case AutoProc is not active
    $sv.carrier_xfer(@@carrier, @@vrndm_eqp, @@vrndm_port1, @@stocker, "")
    # wait for carrier unload
    assert $sv.wait_eqp_port_state(@@vrndm_eqp, @@vrndm_port1, 'UnloadReq', notequal: true)
  end
  
  def test13_NoVRNDM_op
    ($log.warn "skipped"; return) unless @@run_lot1.downcase == "true"
    $setup_ok = false
    # process lot at a non-VRNDM op
    lot1 = $sv.lot_list_incassette(@@carrier1)[0]
    #
    $log.info "-- processing at non-VRNDM mask operation on a real tool"
    @@li_old = $sv.lot_info(lot1, :wafers=>true)
    assert_equal @@op_NoVRNDM, @@li_old.opNo, "lot #{lot1} is not at opNo #{@@op_NoVRNDM}" 
    res = $sv.lot_hold_release(lot1, nil)
    assert_equal 0, res, "error releasing holds of lot #{lot1}"
    assert $sv.auto_process_carriers(@@vrndm_eqp, @@vrndm_port1, @@carrier1)
    @@li_new = $sv.lot_info(lot1, :wafers=>true)
    #
    $log.info "-- comparing slotmaps"
    $log.debug "original slotmap: #{@@li_old.wafers.pretty_inspect}"
    $log.debug "current slotmap: #{@@li_new.wafers.pretty_inspect}"
    assert @@li_old.wafers == @@li_new.wafers, "slot maps must be identical"
    $setup_ok = true
  end
  
  def test14_cleanup
    ($log.warn "skipped"; return) unless @@run_lot1.downcase == "true"
    $log.info "clean up, waiting for carrier to be removed from #{@@vrndm_eqp} #{@@vrndm_port1}"
    # wait for carrier to become ready for unload
    assert $sv.wait_eqp_port_state(@@vrndm_eqp, @@vrndm_port1, "UnloadReq")
    # create xfer job, in case AutoProc is not active
    $sv.carrier_xfer(@@carrier1, @@vrndm_eqp, @@vrndm_port1, @@stocker, "")
    # wait for carrier unload
    assert $sv.wait_eqp_port_state(@@vrndm_eqp, @@vrndm_port1, 'UnloadReq', notequal: true)
  end
  
  def test15_sorter_RNDM
    ($log.warn "skipped"; return) unless @@run_lot1.downcase == "true" and @@run_sorter_ops.downcase == "true"
    $setup_ok = false
    # process lot at a wafer sorter with real randomize recipe
    lot1 = $sv.lot_list_incassette(@@carrier1)[0]
    #
    $log.info "-- processing at an RNDM operation on a real sorter"
    @@li_old = $sv.lot_info(lot1, :wafers=>true)
    assert_equal @@op_sorter_RNDM, @@li_old.opNo, "lot #{lot1} is not at opNo #{@@op_sorter_RNDM}" 
    res = $sv.lot_hold_release(lot1, nil)
    assert_equal 0, res, "error releasing holds of lot #{lot1}"
    assert $sv.auto_process_carriers(@@sorter, @@sorter_port1, @@carrier1)
    @@li_new = $sv.lot_info(lot1, :wafers=>true)
    #
    $log.info "-- comparing slotmaps"
    $log.debug "original slotmap: #{@@li_old.wafers.pretty_inspect}"
    $log.debug "current slotmap: #{@@li_new.wafers.pretty_inspect}"
    assert @@li_old.wafers != @@li_new.wafers, "slot maps must not be identical"
    $setup_ok = true
  end
  
  def test19_cleanup
    ($log.warn "skipped"; return) unless @@run_lot1.downcase == "true" and @@run_sorter_ops.downcase == "true"
    $log.info "clean up, waiting for carrier to be removed from #{@@vrndm_eqp} #{@@vrndm_port1}"
    # wait for carrier to become ready for unload
    assert $sv.wait_eqp_port_state(@@vrndm_eqp, @@vrndm_port1, "UnloadReq")
    # create xfer job, in case AutoProc is not active
    $sv.carrier_xfer(@@carrier1, @@vrndm_eqp, @@vrndm_port1, @@stocker, "")
    # wait for carrier unload
    assert $sv.wait_eqp_port_state(@@vrndm_eqp, @@vrndm_port1, 'UnloadReq', notequal: true)
  end
  
  #
  # lot2: no GatePass, processed without VRNDM at the mask op
  #
  
  def test21_No_GatePass
    ($log.warn "skipped"; return) unless @@run_lot2.downcase == "true"
    # "no randomize" szenario with a lot being processed at the mask PD without previous GatePass
    lot2 = $sv.lot_list_incassette(@@carrier2)[0]
    #
    if @@run_sorter_ops.downcase == "true"
      $log.info "-- ensure sorter resources are available"
      assert wait_for(timeout: 1800) {sorter_jobs(eqp, port: @@sorter_port1).to_a.first.last.nil?}, "old sorter job on #{@@sorter} #{@@sorter_port1}"
      assert $sv.wait_eqp_port_state(@@sorter, @@sorter_port1, "UnloadReq")
      assert $sv.wait_eqp_port_state(@@sorter, @@sorter_port1, 'UnloadReq', notequal: true), "check #{@@sorter} port #{@@sorter_port1}"
      $log.info "-- executing initial WaferIDRead on a real sorter"
      assert Test_VRNDM1.reset_carrier(@@carrier2), "error preparing carrier #{@@carrier2}"
      assert Test_VRNDM1.waferid_read(@@sorter, @@sorter_port1, @@carrier2), "error in WaferIDRead for #{@@carrier2}"
      assert_equal 0, $sv.lot_requeue(lot2), "error re-queueing lot #{lot2}"
    end
    #
    $log.info "-- executing initial (virtual) processing at lotstart operation"
    assert Test_VRNDM1.reset_carrier(@@carrier2), "error preparing carrier #{@@carrier2}"
    assert Test_VRNDM1.claim_process_carrier("O-LOTSTART", @@olotstart_port, @@carrier2, @@op_lotstart)
    #
    $log.info "-- locating to randomize operation without VRNDM GatePass" 
    assert_equal 0, $sv.lot_opelocate(lot2, @@op_VRNDM), "error locating lot #{lot2}"
    #
    $log.info "-- processing at the VRNDM operation on a real tool"
    @@li_old = $sv.lot_info(lot2, :wafers=>true)
    assert $sv.auto_process_carriers(@@vrndm_eqp, @@vrndm_port1, @@carrier2)
    @@li_new = $sv.lot_info(lot2, :wafers=>true)
    #
    $log.info "-- comparing slotmaps"
    $log.debug "original slotmap: #{@@li_old.wafers.pretty_inspect}"
    $log.debug "current slotmap: #{@@li_new.wafers.pretty_inspect}"
    assert @@li_old.wafers == @@li_new.wafers, "slot maps must be identical"
  end
  
  def test29_cleanup
    ($log.warn "skipped"; return) unless @@run_lot2.downcase == "true"
    $log.info "clean up, waiting for carrier to be removed from #{@@vrndm_eqp} #{@@vrndm_port1}"
    # wait for carrier to become ready for unload
    assert $sv.wait_eqp_port_state(@@vrndm_eqp, @@vrndm_port1, "UnloadReq")
    # create xfer job, in case AutoProc is not active
    $sv.carrier_xfer(@@carrier2, @@vrndm_eqp, @@vrndm_port1, @@stocker, "")
    # wait for carrier unload
    assert $sv.wait_eqp_port_state(@@vrndm_eqp, @@vrndm_port1, 'UnloadReq', notequal: true)
  end
  
  #
  # lot 1 with GatePass and VRNDM, lot2 without GatePass and VRNDM at the mask op
  #
  
  def test31_concurrent
    ($log.warn "skipped"; return) unless @@run_combined.downcase == "true"
    # process 2 lots at the same time at the mask op, 1st with GatePass and randomize, 2nd without
    lot1 = $sv.lot_list_incassette(@@carrier1)[0]
    lot2 = $sv.lot_list_incassette(@@carrier2)[0]
    #
    if @@run_sorter_ops.downcase == "true"
      $log.info "-- ensure sorter resources are available"
      assert wait_for(timeout: 1800) {sorter_jobs(eqp, port: @@sorter_port1).to_a.first.last.nil?}, "old sorter job on #{@@sorter} #{@@sorter_port1}"
      assert $sv.wait_eqp_port_state(@@sorter, @@sorter_port1, "UnloadReq")
      assert $sv.wait_eqp_port_state(@@sorter, @@sorter_port1, 'UnloadReq', notequal: true), "check #{@@sorter} port #{@@sorter_port1}"
      assert wait_for(timeout: 1800) {sorter_jobs(eqp, port: @@sorter_port2).to_a.first.last.nil?}, "old sorter job on #{@@sorter} #{@@sorter_port2}"
      assert $sv.wait_eqp_port_state(@@sorter, @@sorter_port2, "UnloadReq")
      assert $sv.wait_eqp_port_state(@@sorter, @@sorter_port2, 'UnloadReq', notequal: true), "check #{@@sorter} port #{@@sorter_port2}"
      #
      $log.info "-- executing initial WaferIDRead on a real sorter"
      assert Test_VRNDM1.reset_carrier(@@carrier1), "error preparing carrier #{@@carrier1}"
      assert Test_VRNDM1.reset_carrier(@@carrier2), "error preparing carrier #{@@carrier2}"
      if @@sorter_port1 != @@sorter_port2
        # parallel WaferIDRead jobs on the sorter
        assert Test_VRNDM1.waferid_read(@@sorter, [@@sorter_port1, @@sorter_port2], [@@carrier1, @@carrier2]), 
          "error in WaferIDRead of carriers #{@@carrier1}, #{@@carrier2}"
      else
        # separate consecutive jobs on one port
        assert Test_VRNDM1.waferid_read(@@sorter, @@sorter_port1, @@carrier1), "error in WaferIDRead for #{@@carrier1}"
        assert Test_VRNDM1.waferid_read(@@sorter, @@sorter_port1, @@carrier2), "error in WaferIDRead for #{@@carrier2}"
      end
      assert_equal 0, $sv.lot_requeue(lot1), "error re-queueing lot #{lot1}"
      assert_equal 0, $sv.lot_requeue(lot2), "error re-queueing lot #{lot2}"
    end
    #
    $log.info "-- executing initial (virtual) processing at lotstart operation"
    assert Test_VRNDM1.reset_carrier(@@carrier1), "error preparing carrier #{@@carrier1}"
    assert Test_VRNDM1.reset_carrier(@@carrier2), "error preparing carrier #{@@carrier2}"
    assert Test_VRNDM1.claim_process_carrier("O-LOTSTART", @@olotstart_port, @@carrier1, @@op_lotstart)
    assert Test_VRNDM1.claim_process_carrier("O-LOTSTART", @@olotstart_port, @@carrier2, @@op_lotstart)
    #
    $log.info "-- locating lot1 to VRNDM GatePass operation"
    assert_equal 0, $sv.lot_opelocate(lot1, @@op_gatepass), "error locating lot #{lot1}"
    $log.info "-- locating lot2 to the VRNDM opeartion (no GatePass)"
    assert_equal 0, $sv.lot_opelocate(lot2, @@op_VRNDM), "error locating lot #{lot2}"
    #
    $log.info "-- processing lots at the VRNDM operation on a real tool"
    @@li_old = $sv.lots_info([lot1, lot2], :wafers=>true)
    assert $sv.auto_process_carriers(@@vrndm_eqp, [@@vrndm_port1, @@vrndm_port2], [@@carrier1, @@carrier2]),
      "error processing carriers #{@@carrier1}, #{@@carrier2} at the VRNDM operation"
    @@li_new = $sv.lots_info([lot1, lot2], :wafers=>true)
    #
    $log.info "-- comparing slotmaps of randomized lot1 in #{@@carrier1}"
    $log.debug "original slotmap: #{@@li_old[0].wafers.pretty_inspect}"
    $log.debug "current slotmap: #{@@li_new[0].wafers.pretty_inspect}"
    assert @@li_old[0].wafers != @@li_new[0].wafers, "slot maps must not be identical"
    #
    $log.info "-- comparing slotmaps of not randomized lot2 in #{@@carrier2}"
    $log.debug "original slotmap: #{@@li_old[1].wafers.pretty_inspect}"
    $log.debug "current slotmap: #{@@li_new[1].wafers.pretty_inspect}"
    assert @@li_old[1].wafers == @@li_new[1].wafers, "slot maps must be identical"
  end
  
  def test39_concurrent_cleanup
    ($log.warn "skipped"; return) unless @@run_combined.downcase == "true"
    test_ok = true
    $log.info "clean up, waiting for carrier to be removed from #{@@vrndm_eqp} #{@@vrndm_port1}"
    test_ok &= $sv.wait_eqp_port_state(@@vrndm_eqp, @@vrndm_port1, "UnloadReq")
    $sv.carrier_xfer(@@carrier1, @@vrndm_eqp, @@vrndm_port1, @@stocker, "")
    test_ok &= $sv.wait_eqp_port_state(@@vrndm_eqp, @@vrndm_port1, 'UnloadReq', notequal: true)
    $log.info "clean up, waiting for carrier to be removed from #{@@vrndm_eqp} #{@@vrndm_port2}"
    test_ok &= $sv.wait_eqp_port_state(@@vrndm_eqp, @@vrndm_port2, "UnloadReq")
    $sv.carrier_xfer(@@carrier2, @@vrndm_eqp, @@vrndm_port2, @@stocker, "")
    test_ok &= $sv.wait_eqp_port_state(@@vrndm_eqp, @@vrndm_port2, 'UnloadReq', notequal: true)
    assert test_ok, "error removing carriers from #{@@vrndm_eqp}"
  end


  # either :pg=>pg or :port=>port can be passed as parameter, else all jobs are reported
  # DEPRECATED! used in attic Test_VRNDM1 only, use sorter_data instead
  #
  # return hash of sorter jobs per port group
  def sorter_jobs(eqp, params={})
    pgs = params[:pg] 
    pgs ||= $sv.eqp_info(eqp).ports.collect {|p| p.pg if params[:port].nil? or (p.port == params[:port])}.compact
    pgs = pgs.split if pgs.kind_of?(String)
    ret = {}
    pgs.each {|pg|
      data = $sv.sorter_data(eqp, :pg=>pg)
      ret[pg] = data.collect {|j|
        [j.action, j.slotmap.tgt_carrier, j.request_time] if j.sorter_status == 'SP_Sorter_Requested'
      }.compact[0]
    }
    return ret
  end


  
  
  def self.reset_carrier(carriers)
    # set carriers AVAILABLE and remove all lot holds and NPW reservations on the sorter
    # return true on success
    carriers = carriers.split if carriers.kind_of?(String)
    ret = true
    carriers.each {|carrier|
      $log.info "reset carrier #{carrier.inspect}"
      lot = $sv.lot_list_incassette(carrier)[0]
      res = $sv.lot_hold_release(lot, nil)
      ret &= (res == 0)
      if $sv.carrier_status(carrier).status != "AVAILABLE"
        res = $sv.carrier_status_change(carrier, "AVAILABLE")
        ret &= (res == 0)
      end
      sleep 10
      res = $sv.npw_reserve_cancel(@@sorter, :carrier=>carrier)
      ret &= (res == 0)
    }
    return ret
  end
  
  def self.claim_process_carrier(eqp, port, carrier, opNo=nil)
    # ensure lot is properly located, claim OpeStart and OpeComplete
    # return true on success
    #
    # locate lot
    lot = $sv.lot_list_incassette(carrier)[0]
    # release lot holds
    $sv.lot_hold_release(lot, nil)
    if opNo
      res = $sv.lot_opelocate(lot, opNo)
      return nil if res != 0
    end
    # carrier transfer
    cs = $sv.carrier_status(carrier)
    res = $sv.carrier_xfer_status_change(carrier, "MO", cs.stocker)
    return nil if res != 0
    # process lot
    res = $sv.claim_process_lot(lot, :carrier=>carrier, :eqp=>eqp, :port=>port)
    # reset carrier xfer_status in any case
    res2 = $sv.carrier_xfer_status_change(carrier, cs.xfer_status, cs.stocker)
    return nil if res2 != 0
    # return process status
    return !!res
  end
  
  def self.remove_carrier(carrier, stocker=nil)
    # remove a carrier from a tool after processing
    # return true on success
    ret = true
    $log.info "clean up, waiting for carrier #{carrier} to be removed"
    cs = $sv.carrier_status(carrier)
    eqp = cs.eqp
    ($log.warn "carrier is not at a tool"; return nil) unless eqp
    eqpi = $sv.eqp_info(eqp)
    port = eqpi.ports.collect {|p| p.port if p.loaded_carrier == carrier}.compact[0]
    ($log.warn "carrier is not loaded at #{eqp}"; return nil) unless port
    # wait for carrier to become ready for unload
    res = $sv.wait_eqp_port_state(eqp, port, "UnloadReq")
    ($log.warn "eqp port #{eqp}:#{port} not in UnloadReq"; return nil) unless res
    # create xfer job, in case AutoProc is not active, ignore any errors
    $sv.carrier_xfer(carrier, eqp, port, stocker, "") if stocker
    # wait for carrier unload
    $sv.wait_eqp_port_state(eqp, port, 'UnloadReq', notequal: true)
  end

  def self.waferid_read(eqp, ports, carriers)
    # return true on successful WaferIDRead
    carriers = carriers.split if carriers.kind_of?(String)
    ports = ports.split if ports.kind_of?(String)
    # move carrier to the sorter
    carriers.each_with_index {|carrier, i|
      res = $sv.npw_reserve(eqp, :carrier=>carrier, :port=>ports[i])
      return nil if res != 0
      res = $sv.carrier_xfer(carrier, "", "", eqp, ports[i])
      return nil if res != 0
    }
    res = $sv.wait_carrier_xfer(carriers)
    return nil unless res
    # wait for carrier to be loaded
    carriers.each_with_index {|carrier, i|
      wait_for {
        pi = eqp_info(eqp).ports.find {|p| p.port == ports[i]}
        pi.state == 'LoadComp' && pi.loaded_carrier == carrier
      } || return
    }
    # allow the sorter to recognize the carrier
    sleep 10
    # WaferIDRead on the sorter
    res = $sv.sorter_action_waferidread(eqp, ports, :carrier=>carriers)
    return nil unless res
    # release carrier, will be moved by AutoProc
    ports.each {|port|
      res = $sv.eqp_port_status_change(eqp, port, "UnloadReq")
      return nil if res != 0
    }
    ret = true
    ports.each {|port|
      ret &= $sv.wait_eqp_port_state(eqp, port, 'UnloadReq', notequal: true)
    }
    return ret
  end
  
  def self.get_li_old
    @@li_old
  end
  
  def self.get_li_new
    @@li_new
  end
end
