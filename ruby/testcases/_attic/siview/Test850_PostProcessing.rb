=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.
    
Author: Daniel Steger     2015-11-23

=end

require 'SiViewTestCase'


# Test for MES-2266, Place lot hold on post-processing
class Test850_PostProcessing < SiViewTestCase
  @@eqp = 'UTF002'
  @@opNo = '1000.500'
  @@reason = 'ENG'
  
    
  def self.shutdown
    $sv.lot_cleanup(@@lot)
    $sv.merge_lot_family(@@lot, nosort: true)
  end

  def self.after_all_passed
    $sv.delete_lot_family(@@lot)
  end
  

  def teardown
    @@thread.join if self.class.class_variable_defined?(:@@thread)
    $sv.user_parameter_delete('Lot', @@lot, ['UTSkip', 'UTSleep'])
  end
  

  def test00_setup
    $setup_ok = false
    #
    assert @@pp_usergrp = $sv.environment_variable[1]['SP_EXTERNAL_POSTPROC_USERGRP'], "failed to get user group for post processing"    
    if @@pp_usergrp != ""
      assert $sm.object_info(:user, $sv.user)[0].specific[:usergroups].include?(@@pp_usergrp), "#{$sv.user} should be in PostProc user group"
    end
    assert $svtest.cleanup_eqp(@@eqp)
    assert @@lot = $svtest.new_lot
    $sv_gms = SiView::MM.new("#{$env}_gms")
    #
    $setup_ok = true
  end
  
  def test01_lothold
    #skip 'PP user group is empty' if @@pp_usergrp.empty?
    _prepare
    assert_equal 0, $sv.lot_hold(@@lot, @@reason), "lot hold during post-processing should succeed"
    assert wait_for {
      li = $sv.lot_info(@@lot)
      !(li.pp_lot || li.pp_carrier)
    }, "failed to complete post processing"
    assert_equal 0, $sv.lot_hold_release(@@lot, @@reason), "failed to release lot after post processing"
  end
  
  def test02_lothold_not_privileged
    _prepare
    assert !$sm.object_info(:user, $sv_gms.user)[0].specific[:usergroups].include?(@@pp_usergrp), "#{$sv_gms.user} should not be in PostProc user group"
    assert_equal 1254, $sv_gms.lot_hold(@@lot, @@reason), "lot hold during post-processing should fail"
  end
  

  def _prepare
    # cleanup lot and eqp
    assert_equal 0, $sv.lot_cleanup(@@lot, opNo: @@opNo)
    assert_equal 0, $sv.eqp_cleanup(@@eqp)
    #
    # process lot, no opecomp
    assert $sv.user_parameter_set_verify("Lot", @@lot, "UTSkip", "YES"), "failed to set script parameters for #{@@lot}"
    assert $sv.user_parameter_set_verify("Lot", @@lot, "UTSleep", "20"), "failed to set script parameters for #{@@lot}"
    assert cj = $sv.claim_process_lot(@@lot, eqp: @@eqp, noopecomp: true, nounload: true, mode: 'Off-Line-1', claim_carrier: true), "processing failed"
    pg = $sv.eqp_info(@@eqp).ports.find {|p| p.cj == cj}.pg
    $sv.eqp_tempdata(@@eqp, pg, cj)    
    @@thread = Thread.new { $sv.eqp_opecomp(@@eqp, cj) }
    # check post processing flags
    sleep 5
    li = $sv.lot_info(@@lot)
    assert_equal true, li.pp_lot, "lot should be in post processing"
    assert_equal true, li.pp_carrier, "carrier should not be in post processing"
  end
  
end
  