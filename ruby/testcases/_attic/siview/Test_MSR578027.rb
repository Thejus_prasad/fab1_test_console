=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: Steffen Steidten, 2012-06-11
=end

require 'SiViewTestCase'


# Test MSR 578027 - RTD Dispatch, Whats Next and Lot Info screens encounter system error
# (Lot on a route with a module or operation with the same name) - fixed in core R10.0.1
class Test_MSR578027 < SiViewTestCase
  @@product = 'TKEY01.A0'
  @@route = 'C02-TKEY.01'
  @@broute = 'TKEY-SORT.01'   # a branch route having a module with the same name
  @@bop = '6350.1000'
  @@retop = '6500.1000'
  
  
  def self.shutdown
    $sv.delete_lot_family(@@lot)
  end


  def test01    
    # prepare lot
    assert @@lot = $svtest.new_lot(route: @@route, product: @@product)
    assert_equal 0, $sv.lot_opelocate(@@lot, @@bop), "error locating lot"
    assert_equal 0, $sv.lot_branch(@@lot, @@broute, retop: @@retop), "error branching lot"
    assert_equal 0, $sv.lot_opelocate(@@lot, 2), "no op with name #{@@broute} found"
    # lot info
    $log.info ">> testing lot info"
    assert li = $sv.lot_info(@@lot)
    assert_equal li.route, $sv.AMD_modulepd(@@lot), "module PD name is not the same as route name"
    # what next
    $log.info ">> testing what next"
    assert $sv.what_next(li.opEqps[0])
    # route lookahead
    $log.info ">> testing route lookahead"
    assert $sv.lot_route_lookahead(@@lot)
  end
  

end