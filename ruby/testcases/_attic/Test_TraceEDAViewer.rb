=begin 
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.
    
Author: Steffen Steidten, 2013-11-28

=end

require 'RubyTestCase'
require 'traceeda'

# Create data for EDAViewer tests
class Test_TraceEDAViewer < RubyTestCase
  
  @@plan = 'QA_LETCL2'
  @@eqp1 = "LETCL1051"   # ETC must be running in WW
  @@ntools = 2
  
  def self.startup
    super(nosiview: true)
    $wwtest = TraceEDA::Test.new($env, plan: @@plan, tool0: @@eqp1, ntools: @@ntools)
  end
  
  def self.after_all_passed
  end
  
  def test11
    res = $wwtest.load_test(duration: "3m", nocharts: true)
    assert res.end_with?("passed\n")
  end
  
end