=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.
    
Author: Steffen Steidten, Thomas Klose, Daniel Steger 2012-05-21
Version: 1.36.0

=end

require_relative 'Test_CP.rb'

# Test Common Platform data feed LOT_FINISHED_GOODS
#
# Requirements: http://myteamsmst/sites/MST/IPMO/prj/Common%20Platform%20Integration/Shared%20Documents/MFG%20TRACK/Requirement%20Details/OracleShipData2010_Requirements.xlsx 
    
class Test_CP20 < Test_CP
  @@loader = 'lot_finished_goods'

  
  def self.after_all_passed
    $sv.delete_lot_family(@@lot)
  end


  def test00_setup
    $setup_ok = false
    #
    # create test lot
    assert @@lot = $cptest.new_lot
    assert $sv.user_parameter_set_verify('Lot', @@lot, 'CustomerNCSLInformation', 'STM')
    assert $sv.user_parameter_set_verify('Lot', @@lot, 'ShipLabelNote', 'TO MALTA, NY')
    assert $sv.user_parameter_set_verify('Lot', @@lot, 'CustomerLotGrade', '20')
    assert $sv.user_parameter_set_verify('Lot', @@lot, 'CustomerNCSLPrintFlag', 'Y')
    assert $sv.user_parameter_set_verify('Lot', @@lot, 'CustomerQualityCode', '00')   
    assert $sv.user_parameter_set_verify('Lot', @@lot, 'ERPSalesOrder', '30308617/30')
    # auto-bankin
    assert_equal 0, $sv.lot_opelocate(@@lot, 'last')
    sleep 1
    assert_equal 'COMPLETED', $sv.lot_info(@@lot).status
    #
    $setup_ok = true
  end

  
  # TurnkeyType U

  def test10_setup_U
    $setup_ok = false
    assert $sv.user_parameter_set_verify('Lot', @@lot, 'TurnkeyType', 'U')
    $sv.lot_info(@@lot, wafers: true).wafers.each {|w|
      $sv.user_parameter_change('Wafer', w.wafer, 'DieCountGood', 0)
      $sv.user_parameter_change('Wafer', w.wafer, 'OQAGoodDie', 51)
    }
    #
    $log.info "waiting #{@@sleeptime}s before running the loader"; sleep @@sleeptime
    $cp.run_loader(@@loader)
    assert $cp.get_reports_MES_Oracle
    #
    $setup_ok = true
  end
  
  def test11_siview1
    assert $cptest.verify_siview1_data(@@lot), "SiView comparison failed"
  end
  
  def test12_picklist1
    assert $cptest.verify_picklist_data(@@lot), "Picklist comparison failed"
  end
    

  # TurnkeyType J

  def test20_setup_J
    $setup_ok = false
    assert $sv.user_parameter_set_verify('Lot', @@lot, 'TurnkeyType', 'J')
    assert $sv.user_parameter_set_verify('Lot', @@lot, 'TurnkeySubconIDBump', 'BU')
    assert $sv.user_parameter_set_verify('Lot', @@lot, 'TurnkeySubconIDSort', 'SO')
    $sv.lot_info(@@lot, wafers: true).wafers.each {|w| 
      $sv.user_parameter_change('Wafer', w.wafer, 'DieCountGood', 52)
      $sv.user_parameter_change('Wafer', w.wafer, 'OQAGoodDie', 53)
    }
    #
    $log.info "waiting #{@@sleeptime}s before running the loader"; sleep @@sleeptime
    $cp.run_loader(@@loader)
    assert $cp.get_reports_MES_Oracle
    #
    $setup_ok = true
  end  
  
  def test21_siview1
    assert $cptest.verify_siview1_data(@@lot), "SiView comparison failed"
  end
  
  def test22_picklist1
    assert $cptest.verify_picklist_data(@@lot), "Picklist comparison failed"
  end  
  
end
