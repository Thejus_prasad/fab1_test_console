=begin
(c) Copyright 2016 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.


Author: Thomas Klose, 2018-02-23

Version: 0.1

History:
=end

require 'csv'
require 'fastrequest'

class CRMonthlyReport

  @fr = GF::FastRequest.new
  $log.info("getting CRs in production")
  prdcr = @fr.monthly_cr_report
  $log.info("getting rolled back CRs")
  rbkcr = @fr.monthly_cr_rollback_report
  reportts = Time.now.strftime('%Y-%m-%d_%H-%M')
  fname = (Time.now-2578400).strftime("%Y-%m-Report_#{reportts}.csv")  # last month

  CSV.open(fname, 'w') do |csv|
    csv << ["to production"]
    prdcr.each{|r| csv << [r.id, r.title, r.apps]}
    csv << ["roll back"]
    rbkcr.each{|r| csv << [r.id, r.title]}
  end
  $log.info('Done.')
end
