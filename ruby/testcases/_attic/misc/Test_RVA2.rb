=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author:  Steffen Steidten, 2013-07-11

Version: 2.7.4

GUI ITDC: http://f36asd25:8380/RVA2GUI/client.html

History:
  2016-08-11 sfrieske,  added test for MSR 1142380 (ssetup in route C02-QE01)
=end

require 'RubyTestCase'
require 'rvatest'


# Test route definitions are located in H:\FactoryAutomation\RVA_Test_ITDC\Flows\QA\C02-QA-Ruletest\ and C02-QA-Effective,
#
# UDATA definitions in H:\FactoryAutomation\RVA_Test_ITDC\Flows\Sampling-Spec\
class Test_RVA2 < RubyTestCase
  # reload test
  @@routespecs = 'testcasedata/rva/C02-*.yaml'
  # before signoff tests
  @@ruletestdata = 'testcasedata/rva/C02-QR*-v1.yaml'
  # before effective tests
  @@udataspec = 'C07-00000428KH'
  @@udatawrong = 'C07-00000428KH1'
  # reload files test
  @@loops = 3  # perf issues from 2.6.0 til 2.7.1


  def self.startup
    super
    @@rvatest = RVA::Test.new($env)
  end


  def setup
    @@rvatest.server.udataspec = @@udataspec
    super
  end


  def test00_setup
    $setup_ok = false
    assert @@rvatest.reload_files, "error reloading XLS files from P drive"
    # try first without reload_files
    # reloaded = false
    # ok = nil
    # loop {
    #   ok = true
    #   Dir[@@routespecs].each {|f|
    #     spec = File.basename(f, '.yaml')
    #     ($log.warn "missing spec #{spec.inspect}"; ok=false) unless @@rvatest.server.specfiles(spec).first
    #   }
    #   break if ok || reloaded
    #   assert @@rvatest.reload_files, "error reloading XLS files from P drive"
    #   reloaded = true
    # }
    # assert ok, "missing specs"
    $setup_ok = true
  end

  # before signoff

  def test11_rules
    Dir[@@ruletestdata].each {|f|
      spec = File.basename(f, '.yaml')
      $log.info "\n\n-- testing spec #{spec}"
      assert @@rvatest.validate_verify(spec)
    }
  end

  def test12_multiple_specs
    specs = ['C02-QR00v2', 'C02-QR00v1']
    assert @@rvatest.validate_verify(specs, tcname: specs.first)
  end

  def test13_cancel
    s = 'C02-QL00'
    specs = 20.times.collect {|i| s = SiView.siview_next(s); s + 'v1'}
    # submit request
    assert @@rvatest.server.check_specs(specs)
    # cancel running request
    assert @@rvatest.server.cancel_validation
    ##tend = @@rvatest.server.validation_results(unfinished: true).first['timeEnd']
    ##assert tend.kind_of?(Time) || tend.start_with?('Job cancel')
    assert @@rvatest.server.validation_results(unfinished: true).first['timeEnd'].start_with?('Job cancel')
  end


  # before effective

  def test21_noroute
    $setup_ok = false
    spec = 'C02-QE00v1'
    assert_empty $sv.module_list(level: 'Main', id: spec.split('v').first), "route for #{spec} must not exist"
    assert @@rvatest.validate_verify(spec, effective: true)
    $setup_ok = true
  end

  def test22_good
    $setup_ok = false
    spec = 'C02-QE01v1'
    assert @@rvatest.validate_verify(spec, effective: true)
    $setup_ok = true
  end

  def test23_udataspec
    spec = 'C02-QE01v1'
    @@rvatest.server.udataspec = @@udatawrong
    assert @@rvatest.validate_verify(spec, effective: true, tcname: 'C07-wrong', udata: true)
  end

  def test24_pds_module
    spec = 'C02-QE01v2'
    assert @@rvatest.validate_verify(spec, effective: true)
  end

  def test25_pd_data
    spec = 'C02-QE02v1'   # manipulated PD CA-TDEP-7973.01
    # test w/o UDATA check
    assert @@rvatest.validate_verify(spec, effective: true)
    # test with UDATA check
    assert @@rvatest.validate_verify(spec, effective: true, tcname: "#{spec}-UDATA", udata: true)
  end

  def test26_route
    # ref spec, route with same name
    spec = 'C02-QE03v1'
    assert @@rvatest.validate_verify(spec, effective: true)
    # identical SiView data but different names
    spec = 'C02-QE-LONGER-NAMEv01'
    assert @@rvatest.validate_verify(spec, effective: true)
  end


  # load tests

  def test91_reload
    assert @@loops > 0, "configured to not run: loops is #{@@loops}"
    @@result = {}
    # first series
    @@result[1] = []
    @@loops.times {|i|
      $log.info "loop #{i+1}/#{@@loops}"
      res = @@rvatest.reload_files
      @@result[1] << (res ? @@rvatest.server.duration : 9999)
    }
    #
    res = "#{@@loops} loops for each series\n"
    @@result.each_pair {|i, tt|
      res += "#{i}: avg: #{tt.avg.round(1)}s +- #{tt.standard_deviation.round(1)}s; first run: #{tt.first.round(1)}s\n"
    }
    $log.info "result:\n\n#{res}\n"
  end

  def test92_perf_verify_signoff
    s = 'C02-QL00'
    specs = 100.times.collect {|i| s = SiView.siview_next(s); s + 'v1'}
    assert @@rvatest.validation_performance(specs, wait_timeout: 3600)
  end

  def test93_perf_verify_effective
    s = 'C02-QL00'
    specs = 100.times.collect {|i| s = SiView.siview_next(s); s + 'v1'}
    assert @@rvatest.validation_performance(specs, wait_timeout: 3600, effective: true)
  end

end
