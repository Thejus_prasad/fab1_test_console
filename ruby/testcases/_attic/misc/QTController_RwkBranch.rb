=begin
(c) Copyright 2016 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.


Author: Steffen Frieske, 2017-11-01

Version: 1.0.2

History:
=end

require 'SiViewTestCase'

# Test MSR1261968 - nested QTs
class QTController_RwkBranch < SiViewTestCase
  @@sv_defaults = {carriers: 'EQA%'}
  @@route = 'C02-1910.QA02'
  @@qt1 = 92 * 3600
  @@qt2 = 240 * 60   # 4 h     6 * 3600
  @@opNo_qt1 = '99500.1100'
  @@opNo_rwk = '99500.1400'
  @@opNo_ret = '99600.1000'
  @@route_rwk = 'R-B-2NB-BACKSIDEGRINDING-QT.01'
  @@opNo_qt2 = '1000.1000'  # on rwk route, next op is branch PD per design
  @@route_branch = 'R-B-2NB-PEEL-BRANCH.01'
  @@spread_qt = 120
  @@duration_sep = 330
  

  def self.after_all_passed
    @@testlots.each {|lot| @@sv.delete_lot_family(lot)}
  end

  def test1
    # create lot on test route
    assert lot = @@svtest.new_lot, 'error creating test lot'
    @@testlots = [lot]
    assert_equal 0, @@sv.schdl_change(lot, route: @@route, opNo: @@opNo_qt1), 'error changing route'
    # locate to QT1 start and GatePass -> QT1 
    assert_equal 0, @@sv.lot_gatepass(lot, mandatory: true), 'error gatepassing lot'
    assert qt = @@sv.lot_qtime_list(lot).first, 'no QT'
    assert qt.qt_details.first.remain_time > @@qt1 - @@spread_qt, 'wrong QT'
    # locate to Rwk start and Rework
    $log.info "sleeping #{@@duration_sep} s for event separation"; sleep @@duration_sep
    assert_equal 0, @@sv.lot_opelocate(lot, @@opNo_rwk), 'error locating lot'
    assert_equal 0, @@sv.lot_rework(lot, @@route_rwk, return_opNo: @@opNo_ret), 'error starting rework'
    # on rwk locate to QT2 start and GatePass -> QT2
    $log.info "sleeping #{@@duration_sep} s for event separation"; sleep @@duration_sep
    assert_equal 0, @@sv.lot_opelocate(lot, @@opNo_qt2), 'error locating lot'
return
    assert_equal 0, @@sv.lot_gatepass(lot, mandatory: true), 'error gatepassing lot'
    assert qt = @@sv.lot_qtime_list(lot).first, 'no QT'
    assert qt.qt_details.first.remain_time > @@qt2 - @@spread_qt, 'wrong QT'  
    # lot still has the main route QT as qt.qt_details[1] !
    # wait for QT2 timeout
return
    $log.info "waiting #{@@qt2} s for QT2 timeout"; sleep @@qt2 + 10
    # lot onhold????
    # assert_equal 0, @@sv.lot_hold_release(lot, nil)
    #
    # branch lot from current op and GatePass -> lot on branch route (with 1 op only), then back
    assert_equal 0, @@sv.lot_branch(lot, @@route_branch), 'error branching lot'
    assert_equal 0, @@sv.lot_gatepass(lot, mandatory: true), 'error gatepassing lot'
    li = @@sv.lot_info(lot)
    assert_equal @@route_rwk, li.route, 'lot is not on the rework route'
    assert_equal @@opNo_qt2, li.opNo, 'lot is not at QT2 opNo'
    #
  end


end
