=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

	load_run_testcase 'Test_F_CVD_LT_HL', "itdc"

Author: Paul Peschel, 2013-02-04
Version: 1.0.0

=end

require "RubyTestCase"
require 'rtdruletest'
require 'rtdclient'

# Testcases for RTD Rule F_CVD
#
# If run testcases alone over "load_run_testcase 'Test_F_CVD_LT_HL', "itdc", :tp => /20/" you must do first:
#
# 1. @@lots = ""
#
# 3. @@lots_all = ""
#
# 4. Call "test01_prepare_test", because without that, you have no Lot

class Test_F_CVD_LT_HL < RubyTestCase

  @@blocktime = 30*60 #time to stop test, test have to wait for start again at least blocktime for restart
  @@waittime = 10
  @@lots = []
  @@lots_all = []
  @@lotsn = []
  @@opEqps = "RTDC01 RTDC02 RTDC03".split
  $result_rtd = []

  def setup
    super
    $rtdt = RTD::RuleTest.new($env, :sv=>$sv) unless $rtdt and $rtdt.env == $env
    $rtdc = RTD::Client.new($env) unless $rtdc and $rtdc.env == $env
    assert $setup_ok, "test setup is not correct"
  end

  # ###################################################### Setup des Tests ##########################################################
  #
  # Prepare all equipment and lots for the Test
  #
  # See also testsetup in excel file Testsetup.xls in sheet "Testsetup_F_cvd32x".
  # Here described all important stuff for the tests in this File.
  #
  # http://myteamsdrs2/sites/FICSQA/_layouts/xlviewer.aspx?id=/sites/FICSQA/Documentation/Technical%20Documentation/RTD/Testcases_F_CVD32.xlsx

  def test01_check_time
    @@opEqps.each {|eq|
      eqptime = $sv.eqp_info(eq).ports.map{|p| p.disp_time.to_i}.sort
      difftime = Time.now.to_i - eqptime[0]
      if difftime < @@blocktime
        $setup_ok = false
        $log.warn("test have to wait for start again at least blocktime #{difftime/60} min for restart")
      end
    }
  end

  def test02_prepare_test
      # Return Value
      ok = true

      # Expected Values for Lot
      user = "X-UNITTEST"
      opNo = "1000.1000"

      # EQP Cleanup
      @@opEqps.each {|eq|
        eqp = $sv.eqp_info(eq)
        ok &= ($sv.eqp_opestart_cancel(eqp.eqp, eqp.cjs[0]) == 0) unless eqp.cjs == []
      }
      ok &= $rtdt.eqp_cleanup(@@opEqps)
      ok &= $rtdt.stocker_cleanup()

      # Create Lot
      lotsold = $sv.lot_list(:route =>"P-RTDC-MAINPD01.01")
      $rtdt.delete_lots(lotsold) if lotsold.size > 0
      @@lots_all = $rtdt.create_lots_rtd("RTDC-SIN450.01")
      @@lots_all += $rtdt.create_lots_rtd("RTDC-UDOX30.01")
      @@lots_all += $rtdt.create_lots_rtd("RTDC-UDOX200.01")
      @@lots_all += $rtdt.create_lots_rtd("RTDC-SIN450.01")
      @@lots_all += $rtdt.create_lots_rtd("RTDC-UDOX30.01")
      @@lots_all += $rtdt.create_lots_rtd("RTDC-UDOX200.01")
      @@lots_all += $rtdt.create_lots_rtd("RTDC-SIN450.01", :count=>3)
      @@lots_all += $rtdt.create_lots_rtd("RTDC-UDOX200.01", :prio=>"2")
      @@lots_all += $rtdt.create_lots_rtd("RTDC-SIN450.01")
      @@lots_all += $rtdt.create_lots_rtd("RTDC-UDOX200.01", :count=>3, :prio=>"2")
      @@lots_all += $rtdt.create_lots_rtd("RTDC-UDOX30.01")
      @@lots_all += $rtdt.create_lots_rtd("RTDC-UDOX200.01")
      # Change Lot to PD and other Stuff
      @@lots_all.each {|l|
        li = $sv.lot_info(l)
        ok &= ($sv.lot_hold_release(l, nil) == 0) if li.status == "ONHOLD"
        ok &= ($sv.lot_opelocate(l, opNo) == 0) unless li.opNo == opNo
        ok &= ($sv.carrier_reserve_cancel(li.carrier) == 0) unless $sv.carrier_status(li.carrier).xfer_user == ""
        unless li.xfer_status == "SI" && li.stocker == "UTSTO11"
          ok &= ($sv.carrier_xfer_status_change(li.carrier, "SO", "INTER1") == 0)
          ok &= ($sv.carrier_xfer_status_change(li.carrier, "MI", "UTSTO11") == 0)
        end
      }

      # Cancel setup if user is wrong
      setup_correct = false unless $sv.user == user and ok == true

      # Check Setup is correct
      if setup_correct == false
        $log.info("Setup has wrong User or wrong Setup")
      end
      @@lots += @@lots_all
  end

  ####################################################### Testcases for RTD #########################################################

  # *Testcase*: builds cascade layer thickness
  #
  # *Number*: 30
  #
  # *Description*: To test compliance with the coating thickness cascade and preventing the "wide driving" of pri-nots.
  #
  # *Logic*:
  #
  #  * RTDC-UDOX30.01     1 lot
  #  * RTDC-UDOX200.01    1 lot
  #  * RTDC-SIN450.01     1 lot
  #
  #  One lot with product "RTDC-UDOX200.01" must be process on tool RTDC01
  #
  #  One lot with product "RTDC-UDOX30.01" must be process on tool RTDC02
  #
  #  One lot with product "RTDC-SIN450.01" must be process on tool RTDC03
  #
  #  1 x Call from F_cvd32x Rule for Tools
  #  Result: Cascades will be continue
  #
  #  RTDC01: 5 lots with RTDC-UDOX200.01 with A3=Y (all other Lots A3=N)
  #
  #  RTDC02: all lots with RTDC-UDOX30.01 with A3=Y (all other Lots A3=N)
  #
  #  RTDC03: all lots with RTDC-SIN450.01 with A3=Y (all other Lots A3=N)
  #
  # *Condition*: RTDC-UDOX200.01
  #
  # OperationReportingType ist DispatchFamily	 -1---1-	 Dispatch Load Balancing
  #
  # *Setup*:
  # * Product: RTDC-SIN450.01, RTDC-UDOX30.01, P-RTDC-UDOX200.01
  # * Tools: RTDC01 RTDC02 RTDC03
  # * PD: 1000.1000
  # * Scriptparameter:
  # * Carriercategory: BEOL
  # * Splitlot: No

  def test30_builds_cascade_layer_thickness
    # Start values
    stocker = "UTSTO11"
    state2 = "MO"
    opNo = "1000.1000"

    # Testcase: builds cascade layer thickness
    # create and prepare lots
    @@lotsn = $rtdt.create_lots_rtd("RTDC-UDOX200.01")
    @@lotsn += $rtdt.create_lots_rtd("RTDC-UDOX30.01")
    @@lotsn += $rtdt.create_lots_rtd("RTDC-SIN450.01")
    @@lotsn.each {|l|
      li = $sv.lot_info(l)
      assert ($sv.lot_hold_release(l, nil) == 0) if li.status == "ONHOLD"
      assert ($sv.lot_opelocate(l, opNo) == 0) unless li.opNo == opNo
      assert ($sv.carrier_reserve_cancel(li.carrier) == 0) unless $sv.carrier_status(li.carrier).xfer_user == ""
      assert ($sv.carrier_xfer_status_change(li.carrier, state2, stocker) == 0)
    }
    # process lots
    @@opEqps.each {|eqp| assert_equal 0, $sv.eqp_mode_offline1(eqp, :notifyTCS=>false)}
    refute_equal "", cj = $sv.claim_process_lot(@@lotsn[0], :eqp => @@opEqps[0])
    refute_equal "", cj = $sv.claim_process_lot(@@lotsn[1], :eqp => @@opEqps[1])
    refute_equal "", cj = $sv.claim_process_lot(@@lotsn[2], :eqp => @@opEqps[2])
    @@opEqps.each {|eqp| assert_equal 0, $sv.eqp_mode_auto3(eqp, :notifyTCS=>false)}
    sleep @@waittime
    @@lots += @@lotsn
  end

  def test32_RTDC01
    eqp = @@opEqps[0]  # RTDC01
    list = $sv.rtd_dispatch_list(eqp)
    header = list.headers
    rows = list.rows
    li = $sv.lot_info(@@lotsn[@@opEqps.index(eqp)])
    rows.each {|r|
      if li.product != r[header.index("Product ID")] and \
                 "" == r[header.index("A3Info")]
        $result_rtd << "#{__method__}: Lot ID #{r[header.index("Lot ID")]} from Product #{r[header.index("Product ID")]} on Equipment #{eqp} should be not A3 Status N"
      end
      if li.product == r[header.index("Product ID")] and \
                eqp == r[header.index("PlanEqp")] and \
                "N" ==  r[header.index("A3")]
        $result_rtd << "#{__method__}: Lot ID #{r[header.index("Lot ID")]} from Product #{r[header.index("Product ID")]} on Equipment #{eqp} should be not A3 Status Y"
      end
    }
  end

  def test34_RTDC02
    eqp = @@opEqps[1]  # RTDC02
    list = $sv.rtd_dispatch_list(eqp)
    rows = list.rows
    headers = list.headers

    r2a3 = "NNNNNYNNYNNNNNYN".split("")
    r2pe = "RTDC01 RTDC01 RTDC01 RTDC01 RTDC03 RTDC02 RTDC02 RTDC03 RTDC02 RTDC01 RTDC03 RTDC03 RTDC03 RTDC03 RTDC02 RTDC02".split
    a3 = rows.map {|r| r[headers.index("A3")]}
    planeqp = rows.map {|r| r[headers.index("PlanEqp")]}

    $result_rtd << "#{__method__}: A3 codes does not match RTD criteria" if a3 != r2a3
    $result_rtd << "#{__method__}: A3 codes does not match RTD criteria" if planeqp != r2pe
  end

  def test36_RTDC03
    eqp = @@opEqps[2]  # RTDC03
    list = $sv.rtd_dispatch_list(eqp)
    header = list.headers
    rows = list.rows
    li = $sv.lot_info(@@lotsn[@@opEqps.index(eqp)])
    rows.each {|r|
      if li.product != r[header.index("Product ID")] and \
                 "" == r[header.index("A3Info")]
        $result_rtd << "#{__method__}: Lot ID #{r[header.index("Lot ID")]} from Product #{r[header.index("Product ID")]} on Equipment #{eqp} should be not A3 Status N"
      end
      if li.product == r[header.index("Product ID")] and \
                eqp == r[header.index("PlanEqp")] and \
                "N" ==  r[header.index("A3")]
        $result_rtd << "#{__method__}: Lot ID #{r[header.index("Lot ID")]} from Product #{r[header.index("Product ID")]} on Equipment #{eqp} should be not A3 Status Y"
      end
    }
  end

  # ###################################################### Result and Delete Lots ###################################################

  def test999_results
    puts "Test OK" if $result_rtd.size == 0
    puts "Test NOK, #{$result_rtd.size} failerures found" if $result_rtd.size > 0

    $result_rtd.each {|i|
      pp i
      if $result_rtd.index(i)%10 == 9
        print "show more Errors, press y?"
        res_input = gets
        break unless res_input.start_with?("y")
      end} if $result_rtd.size > 0

    puts "\ndelete all lots: #{@@lots.inspect} (y/n)?"
    answer = STDIN.gets
    @@lots.each {|l| $sv.delete_lot_family(l, :delete => false)} if "jy".index(answer.chomp.downcase)
  end
end
