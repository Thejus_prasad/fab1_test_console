=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: sfrieske, 2019-04-18
=end

require 'pp'
require 'SiViewTestCase'

# create lots for EPI RTD tests
class RTD_EPI < SiViewTestCase
  @@sv_defaults = {carriers: '%'}
  @@lots = {usecase1: [], usecase2: [], usecase3: [], usecase4: []}


  def self.manual_cleanup
    @@lots.values.flatten.each {|lot| @@sv.delete_lot_family(lot)}
  end


  def test11
    assert lots = 4.times.collect {
      @@svtest.new_controllot('Dummy', {product: 'MD-DN-U-EPI1120-CASE01.01', route:	'MD-DN-U-EPI-DU.01'})
    }
    lots.each {|lot| assert_equal 0, @@sv.lot_opelocate(lot, nil, op: 'ME-DN-USE-EPI-DU.01')}
    @@lots[:usecase1] << lots.clone
    #
    assert lot = @@svtest.new_controllot('Process Monitor', {product: 'MP-DN-U-EPI1120-CASE01.01', route:	'MP-DN-U-FVX-LPCVD-QUAL.01', nwafers: 3})
    assert_equal 0, @@sv.lot_opelocate(lot, nil, op: 'MP-DN-USE-FVX-LPCVD-QUAL.01')
    @@lots[:usecase1] << [lot]
    #
    assert lots = @@svtest.new_lots(4, {product: '3260CN.E2', route:	'C02-1100.01'})
    lots.each {|lot| assert_equal 0, @@sv.lot_opelocate(lot, nil, op: 'CSG-DEP-2200.01')}
    @@lots[:usecase1] << lots.clone
  end

  def test12
    assert lots = 4.times.collect {
      @@svtest.new_controllot('Dummy', {product: 'MD-DN-U-EPI1120-CASE02.01', route:	'MD-DN-U-EPI-DU.01'})
    }
    lots.each {|lot| assert_equal 0, @@sv.lot_opelocate(lot, nil, op: 'ME-DN-USE-EPI-DU.01')}
    @@lots[:usecase2] << lots.clone
    #
    assert lot = @@svtest.new_controllot('Process Monitor', {product: 'MP-DN-U-EPI1120-CASE02.01', route:	'MP-DN-U-FVX-LPCVD-QUAL.01', nwafers: 3})
    assert_equal 0, @@sv.lot_opelocate(lot, nil, op: 'MP-DN-USE-FVX-LPCVD-QUAL.01')
    @@lots[:usecase2] << [lot]
    #
    assert lots = @@svtest.new_lots(4, {product: '4098AA.A1', route:	'C02-1096.01'})
    lots.each {|lot| assert_equal 0, @@sv.lot_opelocate(lot, nil, op: 'CSG-DEP-2200.01')}
    @@lots[:usecase2] << lots.clone
  end

  def test13
    assert lots = 4.times.collect {
      @@svtest.new_controllot('Dummy', {product: 'MD-DN-U-EPI1120-CASE03.01', route:	'MD-DN-U-EPI-DU.01'})
    }
    lots.each {|lot| assert_equal 0, @@sv.lot_opelocate(lot, nil, op: 'ME-DN-USE-EPI-DU.01')}
    @@lots[:usecase3] << lots.clone
    #
    assert lot = @@svtest.new_controllot('Process Monitor', {product: 'MP-DN-U-EPI1120-CASE03.01', route:	'MP-DN-U-FVX-LPCVD-QUAL.01', nwafers: 3})
    assert_equal 0, @@sv.lot_opelocate(lot, nil, op: 'MP-DN-USE-FVX-LPCVD-QUAL.01')
    @@lots[:usecase3] << [lot]
  end

  def test14
    assert lots = 4.times.collect {
      @@svtest.new_controllot('Dummy', {product: 'MD-DN-U-EPI1120-CASE04.01', route:	'MD-DN-U-EPI-DU.01'})
    }
    lots.each {|lot| assert_equal 0, @@sv.lot_opelocate(lot, nil, op: 'ME-DN-USE-EPI-DU.01')}
    @@lots[:usecase4] << lots.clone
    #
    assert lot = @@svtest.new_controllot('Process Monitor', {product: 'MP-DN-U-EPI1120-CASE04-ONE.01', route:	'MP-DN-U-FVX-LPCVD-QUAL.01', nwafers: 3})
    assert_equal 0, @@sv.lot_opelocate(lot, nil, op: 'MP-DN-USE-FVX-LPCVD-QUAL.01')
    @@lots[:usecase4] << [lot]
    #
    assert lot = @@svtest.new_controllot('Process Monitor', {product: 'MP-DN-U-EPI1120-CASE04-TWO.01', route:	'MP-DN-U-FVX-LPCVD-QUAL.01', nwafers: 3})
    assert_equal 0, @@sv.lot_opelocate(lot, nil, op: 'MP-DN-USE-FVX-LPCVD-QUAL.01')
    @@lots[:usecase4] << [lot]
  end


  def testman81_2_process_monitors
    lot = 'UXYU20127.000'
    op = 'CSG-DEP-2200.01'
    pmlots = ['TU002M3.00', 'TU002M4.00']
    op = 'CSG-DEP-2200.01'
    pmop = 'MP-DN-USE-FVX-LPCVD-QUAL.01'
    lots = [lot] + pmlots
    eqp = 'EPI1120'
    assert cj = @@sv.slr(eqp, lot: lots, monitor: pmlots), 'SLR failed'
  end

  def test99_lots
    $log.info "Testlots:\n#{@@lots.pretty_inspect}\n"
  end

end
