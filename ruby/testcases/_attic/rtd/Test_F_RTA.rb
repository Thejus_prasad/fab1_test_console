=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

	load_run_testcase 'Test_F_RTA', "itdc"

Author: Paul Peschel, 2012-09-27
Version: 1.0.0

=end

require "RubyTestCase"
require 'rtdruletest'
require 'rtdclient'

# Testcases for RTD Rule F_RTA
#
# If run testcases alone over "load_run_testcase 'Test_F_RTA', "itdc", :tp => /20/" you must do first:
#
# 1. @@lots = ""
#
# 3. @@lots_all = ""
#
# 4. Call "test01_prepare_test", because without that, you have no Lot

class Test_F_RTA < RubyTestCase

  @@waittime = 10

  def setup
    super
    $rtdt = RTD::RuleTest.new($env, :sv=>$sv) unless $rtdt and $rtdt.env == $env
    $rtdc = RTD::Client.new($env) unless $rtdc and $rtdc.env == $env
    assert $setup_ok, "test setup is not correct"
  end

  # ###################################################### Setup des Tests ##########################################################
  #
  # Prepare all equipment and lots for the Test
  #
  # See also testsetup in excel file Testsetup.xls in sheet "Testsetup_F_RTA".
  # Here described all important stuff for the tests in this File.
  #
  # http://myteamsdrs2/sites/FICSQA/_layouts/xlviewer.aspx?id=/sites/FICSQA/Documentation/Technical%20Documentation/RTD/Testcases_F_RTA.xlsx

  def test01_prepare_test
      # Return Value
      $result_rtd = []
      ok = true

      # Expected Values for Lot
      user = "X-UNITTEST"
      opNo = "1000.1000"

      # All Equipments
      @@opEqps = "RTDF01 RTDF02 RTDF03 RTDF04 RTDF05".split

      # EQP Cleanup
      @@opEqps.each{|eq|
        eqp = $sv.eqp_info(eq)
        ok &= ($sv.eqp_opestart_cancel(eqp.eqp, eqp.cjs[0]) == 0) unless eqp.cjs == []
      }
      ok &= $rtdt.eqp_cleanup(@@opEqps)
      ok &= $rtdt.stocker_cleanup()

      # Create Lot
      @@lots = []
      @@lots_all = []
      cl = $sv.carrier_list(:category=>"BEOL", :status=>"AVAILABLE", :carrier =>"G%", :empty=>true)
      comment = "ERPITEM=3006AA-UA0,DPML=2.0,PLANNED_FSD=#{Time.now.strftime('%Y-%m-%d')}"
      5.times do
        @@lots_all << $sv.new_lot_release(:product=>"RTDF-PRD300.01", :customer=>"gf", :route=>"P-RTDF-MAINPD01.01", :sublottype=>"PO", :carrier => cl.pop, :comment=>comment, :stbdelay=>1)
        @@lots_all << $sv.new_lot_release(:product=>"RTDF-PRD500.01", :customer=>"gf", :route=>"P-RTDF-MAINPD01.01", :sublottype=>"PO", :carrier => cl.pop, :comment=>comment, :stbdelay=>1)
      end
      4.times do
        @@lots_all << $sv.new_lot_release(:product=>"RTDF-PRD320.01", :customer=>"gf", :route=>"P-RTDF-MAINPD01.01", :sublottype=>"PO", :carrier => cl.pop, :comment=>comment, :stbdelay=>1)
      end
      1.times do
        @@lots_all << $sv.new_lot_release(:product=>"RTDF-PRD400.01", :customer=>"gf", :route=>"P-RTDF-MAINPD01.01", :sublottype=>"PO", :carrier => cl.pop, :comment=>comment, :stbdelay=>1)
        @@lots_all << $sv.new_lot_release(:product=>"RTDF-PRD450.01", :customer=>"gf", :route=>"P-RTDF-MAINPD01.01", :sublottype=>"PO", :carrier => cl.pop, :comment=>comment, :stbdelay=>1)
      end

      # Change Lot to PD and other Stuff
      @@lots_all.each{|l|
        li = $sv.lot_info(l)
        ok &= ($sv.lot_hold_release(l, nil) == 0) if li.status == "ONHOLD"
        ok &= ($sv.lot_opelocate(l, opNo) == 0) unless li.opNo == opNo
        ok &= ($sv.carrier_reserve_cancel(li.carrier) == 0) unless $sv.carrier_status(li.carrier).xfer_user == ""
        unless li.xfer_status == "SI" && li.stocker == "UTSTO11"
          ok &= ($sv.carrier_xfer_status_change(li.carrier, "SO", "INTER1") == 0)
          ok &= ($sv.carrier_xfer_status_change(li.carrier, "MI", "UTSTO11") == 0)
        end
      }

      # Cancel setup if user is wrong
      setup_correct = false unless $sv.user == user and ok == true

      # Check Setup is correct
      if setup_correct == false
          $log.info("Setup has wrong User or wrong Setup")
      end
      @@lots = @@lots + @@lots_all
  end

  ####################################################### Testcases for RTD #########################################################

  # *Testcase*: Builds Cascade
  #
  # *Number*: 20
  #
  # *Description*: Lots must be processed on different Tools. After that new Lots with Product must go to tools wish have at last tool
  #                the same Product. Means all Lots withj same Products have to be go to the Tools wish are work mostly with the Product.
  #
  # *Logic*:
  #
  #  RTA300:                320�C     4 Lots
  #  RTA301:                300�C     5 Lots
  #  RTA302:                400�C     1 Lots
  #  RTA303:                450�C     1 Lots
  #  RTA304:                500�C     5 Lots
  #  Lots are processed on Tool (Operation History is filled with the values) and no Tool has a reservation
  #
  #  5 x Call from F_rta Rule for all RTA Tools (Do not process Lots only check RTD Dispatch List)
  #  Result: Cascaden woll be continue
  #  RTA300: all 5 Lots with 320�C  with A3=Y and PlanEqp=RTA300 on first row on Dispatchlist (all other Lots A3=N)
  #  RTA301: all 5 Lots with 320�C  with A3=Y and PlanEqp=RTA301 on first row on Dispatchlist (all other Lots A3=N)
  #  RTA302: all 5 Lots with 400�C  with A3=Y and PlanEqp=RTA302 on first row on Dispatchlist (all other Lots A3=N)
  #  RTA303: all 5 Lots with 500�C  with A3=Y and PlanEqp=RTA303 on first row on Dispatchlist (all other Lots A3=N)
  #  RTA304: all 5 Lots with 500�C  with A3=Y and PlanEqp=RTA304 on first row on Dispatchlist (all other Lots A3=N)
  #
  # *Condition*:
  #
  # OperationReportingType ist DispatchFamily	 -1---1-	 Dispatch Load Balancing
  #
  # Tool must be include in this file "f36asd14:/apf/apf_itdc/models/fab1/class/ie_cascade.apf" with value "RTA300|F3_SilicideRTA_RTA30x||||5"
  #
  # *Setup*:
  # * Product: RTDF-PRD250.01, RTDF-PRD260.01, RTDF-PRD270.01, RTDF-PRD300.01, RTDF-PRD320.01, RTDF-PRD330.01, RTDF-PRD400.01, RTDF-PRD450.01, RTDF-PRD500.01, RTDF-PRD550.01
  # * Tools: RTDF01 RTDF02 RTDF03 RTDF04 RTDF05
  # * PD: 1000.1000
  # * Scriptparameter:
  # * Carriercategory: BEOL
  # * Splitlot: No

  def test20_builds_cascade
    # Start values
    stocker = "UTSTO11"
    state1 = "MI"
    state2 = "MO"
    opNo = "1000.1000"
    lotsn = []

    tt = {"300" => "RTDF01", "320" => "RTDF02", "400" => "RTDF03", "450" => "RTDF04", "500" => "RTDF05"}

    # Testcase: Builds Cascade
    @@lots_all.each{|l|
      li = $sv.lot_info(l)
      eqp = tt[li.product[8..10]]
        assert_equal 0, $sv.eqp_mode_offline1(eqp, :notifyTCS=>false)
        assert_equal 0, $sv.carrier_xfer_status_change(li.carrier, state2, stocker)
        refute_equal "", cj = $sv.claim_process_lot(l, :eqp => eqp)
        assert_equal 0, $sv.eqp_mode_auto2(eqp, :notifyTCS=>false)
        assert_equal 0, $sv.carrier_xfer_status_change(li.carrier, state1, stocker)
    }

    cl = $sv.carrier_list(:category=>"BEOL", :status=>"AVAILABLE", :carrier =>"G%", :empty=>true)
    comment = "ERPITEM=3006AA-UA0,DPML=2.0,PLANNED_FSD=#{Time.now.strftime('%Y-%m-%d')}"
    5.times do
       lotsn << $sv.new_lot_release(:product=>"RTDF-PRD270.01", :customer=>"gf", :route=>"P-RTDF-MAINPD01.01", :sublottype=>"PO", :carrier => cl.pop, :comment=>comment, :stbdelay=>1)
       lotsn << $sv.new_lot_release(:product=>"RTDF-PRD320.01", :customer=>"gf", :route=>"P-RTDF-MAINPD01.01", :sublottype=>"PO", :carrier => cl.pop, :comment=>comment, :stbdelay=>1)
       lotsn << $sv.new_lot_release(:product=>"RTDF-PRD400.01", :customer=>"gf", :route=>"P-RTDF-MAINPD01.01", :sublottype=>"PO", :carrier => cl.pop, :comment=>comment, :stbdelay=>1)
       lotsn << $sv.new_lot_release(:product=>"RTDF-PRD500.01", :customer=>"gf", :route=>"P-RTDF-MAINPD01.01", :sublottype=>"PO", :carrier => cl.pop, :comment=>comment, :stbdelay=>1)
       lotsn << $sv.new_lot_release(:product=>"RTDF-PRD550.01", :customer=>"gf", :route=>"P-RTDF-MAINPD01.01", :sublottype=>"PO", :carrier => cl.pop, :comment=>comment, :stbdelay=>1)
    end

    lotsn.each{|l|
        li = $sv.lot_info(l)
        assert ($sv.lot_hold_release(l, nil) == 0) if li.status == "ONHOLD"
        assert ($sv.lot_opelocate(l, "1000.1000") == 0) unless li.opNo == opNo
        assert ($sv.carrier_reserve_cancel(li.carrier) == 0) unless $sv.carrier_status(li.carrier).xfer_user == ""
        unless li.xfer_status == "SI" && li.stocker == "UTSTO11"
          assert ($sv.carrier_xfer_status_change(li.carrier, "SO", "INTER1") == 0)
          assert ($sv.carrier_xfer_status_change(li.carrier, "MI", "UTSTO11") == 0)
        end
    }

    sleep @@waittime
    @@opEqps.each{|eq|
      list = $sv.rtd_dispatch_list(eq)
      header = list.headers
      rows = list.rows
      a3y = rows.reject {|row| row[header.index("A3")] == "N"}
      ["PlanEqp", "Machine Recipe ID", "Product ID"].each {|v|
        $result_rtd << "#{__method__}: #{v} is not unique at #{eq} " unless a3y.collect{|row| row[header.index(v)]}.uniq.size == 1
      }
    }
    @@lots = @@lots + lotsn
  end

  # *Testcase*: Prio broke Cascade
  #
  # *Number*: 30
  #
  # *Description*: Like Test 20, but additinial where is a Rocket-Lot with 260�C
  #
  # *Logic*:
  # The next tool must catch the Lot (first the tool will be selected, than the Lot, the calling tool will be the next Operation
  #
  # *Condition*:
  #
  # OperationReportingType ist DispatchFamily	 -1---1-	 Dispatch Load Balancing
  #
  # *Setup*:
  # * Product: RTDF-PRD260.01
  # * Tools: RTDF01 RTDF02 RTDF03 RTDF04 RTDF05
  # * PD: 1000.1000
  # * Scriptparameter:
  # * Carriercategory: BEOL
  # * Splitlot: No

  def test30_prio_broke_cascade
    # Start values
    @@lotsr = []
    opNo = "1000.1000"

    # Testcase: Prio broke Cascade
    cl = $sv.carrier_list(:category=>"BEOL", :status=>"AVAILABLE", :carrier =>"G%", :empty=>true)
    comment = "ERPITEM=3006AA-UA0,DPML=2.0,PLANNED_FSD=#{Time.now.strftime('%Y-%m-%d')}"
    @@lotsr = $sv.new_lot_release(:product=>"RTDF-PRD260.01", :customer=>"gf", :route=>"P-RTDF-MAINPD01.01", :sublottype=>"PO", :carrier => cl.pop, :comment=>comment, :stbdelay=>1)
    li = $sv.lot_info(@@lotsr)
    assert_equal 0, $sv.schdl_change(@@lotsr, :priority =>"1")
    assert ($sv.lot_hold_release(@@lotsr, nil) == 0) if li.status == "ONHOLD"
    assert_equal 0, $sv.lot_opelocate(@@lotsr, opNo)

    @@opEqps.each{|eq|
      sleep @@waittime
      list = $sv.rtd_dispatch_list(eq)
      convert_list = $rtdt.change_list_to_hash(list, @@lotsr)
      $result_rtd << "#{__method__}: Lot #{@@lotsr} is not found on RTD List" unless (convert_list["Lot ID"] == @@lotsr)
      $result_rtd << "#{__method__}: SEQ is wrong '#{convert_list["Seq"]}' instead of '01'" unless (convert_list["Seq"] == "01")
      $result_rtd << "#{__method__}: PlanEqp is wrong '#{convert_list["PlanEqp"]}' instead of #{eq}" unless (convert_list["PlanEqp"] == eq)
    }
    assert_equal 0, $sv.lot_hold(@@lotsr, "ENG")
    @@lots += [@@lotsr]
  end


  # *Testcase*: Wip with one cascade
  #
  # *Number*: 40
  #
  # *Description*: Actual Cascade like Test 20, Low-Wip-Szenario with unfavorable distribution
  #
  # *Logic*:  5 Lots 300�C only on the route and call every RTD List from the tools. Result: Every Tool cache 1 Lot.
  #
  # *Condition*:
  #
  # OperationReportingType ist DispatchFamily	 -1---1-	 Dispatch Load Balancing
  #
  # *Setup*:
  # * Product: RTDF-PRD300.01
  # * Tools: RTDF01 RTDF02 RTDF03 RTDF04 RTDF05
  # * PD: 1000.1000
  # * Scriptparameter:
  # * Carriercategory: BEOL
  # * Splitlot: No

  def test40_wip_with_one_cascade
    # Start values
    reason = "ENG"
    opNo = "1000.1000"
    route = "P-RTDF-MAINPD01.01"
    product = "RTDF-PRD320.01"

    # Testcase: Wip with one cascade
    lotsor = $sv.lot_list(:route => route, :opNP => opNo, :status => "Waiting")
    lotsor.each{|l|
      li = $sv.lot_info(l)
      assert_equal 0, $sv.lot_hold(l, reason) unless li.product == product
    }

    @@opEqps.each{|eq|
      sleep @@waittime
      list = $sv.rtd_dispatch_list(eq)
      header = list.headers
      rows = list.rows
      peqp = rows.collect{|row| row if row[header.index("PlanEqp")] == eq}.compact
      $result_rtd << "#{__method__}: #{eq} is not found at #{eq} " if peqp.size == 0
    }

    lotsor.each{|l|
      li = $sv.lot_info(l)
      assert_equal 0, $sv.lot_hold_release(l, nil) unless li.product == product
    }
  end

  # *Testcase*: Wip with unfavorable distribution
  #
  # *Number*: 50
  #
  # *Description*: Like Test 30 but with upcoming WIP, must like the same tempratur like the other Lots from cascade, hier w�re dann interessant ob die Tools auf die passenderen Lose warten
  #                Upcoming-Wip-Scenario with unfavorable distribution
  #
  # *Logic*:  5 Lots 320�C call every Tool. Result: The Lots will be only go to 320�C Tool.
  #
  # *Condition*:
  #
  # OperationReportingType ist DispatchFamily	 -1---1-	 Dispatch Load Balancing
  #
  # *Setup*:
  # * Product: RTDF-PRD320.01
  # * Tools: RTDF02
  # * PD: 1000.1000
  # * Scriptparameter:
  # * Carriercategory: BEOL
  # * Splitlot: No

  def test50_wip_with_unfavorable_cascade
    # Start values
    lots320 = []
    opNo = "1000.1000"
    product = "RTDF-PRD320.01"
    route = "P-RTDF-MAINPD01.01"
    eq = "RTDF02"

    # Testcase: Wip with unfavorable distribution
    cl = $sv.carrier_list(:category=>"BEOL", :status=>"AVAILABLE", :carrier =>"G%", :empty=>true)
    comment = "ERPITEM=3006AA-UA0,DPML=2.0,PLANNED_FSD=#{Time.now.strftime('%Y-%m-%d')}"
    5.times do
       lots320 << $sv.new_lot_release(:product=>"RTDF-PRD320.01", :customer=>"gf", :route=>"P-RTDF-MAINPD01.01", :sublottype=>"PO", :carrier => cl.pop, :comment=>comment, :stbdelay=>1)
    end

    lots320.each{|l|
      li = $sv.lot_info(l)
      assert ($sv.lot_hold_release(l, nil) == 0) if li.status == "ONHOLD"
      assert_equal 0, $sv.lot_opelocate(l, opNo)
    }
    lots320o = $sv.lot_list(:route => route, :opNP => opNo, :status => "Waiting", :product => product)

    sleep @@waittime
    lots320o.each{|l|
      list = $sv.rtd_dispatch_list(eq)
      convert_list = $rtdt.change_list_to_hash(list, l)
      $result_rtd << "#{__method__}: Lot #{l} is not found on RTD List" unless (convert_list["Lot ID"] == l)
      $result_rtd << "#{__method__}: PlanEqp is wrong '#{convert_list["PlanEqp"]}' instead of #{eq}" unless (convert_list["PlanEqp"] == eq)
    }
    @@lots = @@lots + lots320
  end

  # *Testcase*: Prio broke Cascade on Load Tool
  #
  # *Number*: 60
  #
  # *Description*: Like Test 20 but with Processing Tool, on different Times ready with operation. Means all Tools are in Process. Prio Lot must go to first tool, wish will be ready
  #
  # *Logic*:  All Tools with Lots. Prio Lot mus go to next free Tool
  #
  # *Condition*:
  #
  # OperationReportingType ist DispatchFamily	 -1---1-	 Dispatch Load Balancing
  #
  # *Setup*:
  # * Product: RTDF-PRD250.01, RTDF-PRD260.01, RTDF-PRD270.01, RTDF-PRD300.01, RTDF-PRD320.01, RTDF-PRD330.01, RTDF-PRD400.01, RTDF-PRD450.01, RTDF-PRD500.01, RTDF-PRD550.01
  # * Tools: RTDF01
  # * PD: 1000.1000
  # * Scriptparameter:
  # * Carriercategory: BEOL
  # * Splitlot: No

  def test60_prio_broke_cascade_on_load_tool
    # Start values
    route = "P-RTDF-MAINPD01.01"
    stocker = "UTSTO11"
    state1 = "MI"
    state2 = "MO"
    opNo = "1000.1000"
    eq = "RTDF01"
    lots = []

    # Testcase: Prio broke Cascade on Load Tool
    lots270 = $sv.lot_list(:route => route, :opNP => opNo, :status => "Waiting", :product => "RTDF-PRD270.01")[0]
    lots320 = $sv.lot_list(:route => route, :opNP => opNo, :status => "Waiting", :product => "RTDF-PRD320.01")[0]
    lots400 = $sv.lot_list(:route => route, :opNP => opNo, :status => "Waiting", :product => "RTDF-PRD400.01")[0]
    lots500 = $sv.lot_list(:route => route, :opNP => opNo, :status => "Waiting", :product => "RTDF-PRD500.01")[0]
    lots550 = $sv.lot_list(:route => route, :opNP => opNo, :status => "Waiting", :product => "RTDF-PRD550.01")[0]

    li = $sv.lot_info(lots270)
    assert_equal 0, $sv.eqp_mode_offline1(eq, :notifyTCS=>false)
    assert_equal 0, $sv.carrier_xfer_status_change(li.carrier, state2, stocker)
    refute_equal "", cj = $sv.claim_process_lot(lots270, :eqp => eq, :noopecomp => true)

    sleep 60
    lots << lots320 << lots400 << lots500 << lots550
    tt = {"320" => "RTDF02", "400" => "RTDF03", "500" => "RTDF05", "550" => "RTDF04"}
    lots.each{|l|
      li = $sv.lot_info(l)
      eqp = tt[li.product[8..10]]
      assert_equal 0, $sv.eqp_mode_offline1(eqp, :notifyTCS=>false)
      assert_equal 0, $sv.carrier_xfer_status_change(li.carrier, state2, stocker)
      refute_equal "", cj = $sv.claim_process_lot(l, :eqp => eqp, :noopecomp => true)
    }

    li = $sv.lot_info(@@lotsr)
    assert ($sv.lot_hold_release(@@lotsr, nil) == 0) if li.status == "ONHOLD"
    assert_equal 0, $sv.lot_opelocate(@@lotsr, opNo)

    sleep @@waittime
    list = $sv.rtd_dispatch_list(eq)
    convert_list = $rtdt.change_list_to_hash(list, @@lotsr)
    $result_rtd << "#{__method__}: Lot #{@@lotsr} is not found on RTD List" unless (convert_list["Lot ID"] == @@lotsr)
    $result_rtd << "#{__method__}: PlanEqp is wrong '#{convert_list["PlanEqp"]}' instead of #{eq}" unless (convert_list["PlanEqp"] == eq)

    lots << lots270
    lots.each{|l|
      li = $sv.lot_info(l)
      assert_equal 0, $sv.eqp_opestart_cancel(li.eqp, li.cj)
    }
    $rtdt.eqp_cleanup(@@opEqps)
  end

  # ###################################################### Result and Delete Lots ###################################################

  def test999_results
    puts "Test OK" if $result_rtd.size == 0
    puts "Test NOK, #{$result_rtd.size} failerures found" if $result_rtd.size > 0

    $result_rtd.each {|i|
      pp i
      if $result_rtd.index(i)%10 == 9
        print "show more Errors, press y?"
        res_input = gets
        break unless res_input.start_with?("y")
      end} if $result_rtd.size > 0

    puts "\ndelete all lots: #{@@lots.inspect} (y/n)?"
    answer = STDIN.gets
    @@lots.each{|l| $sv.delete_lot_family(l, :delete => false)} if "jy".index(answer.chomp.downcase)
  end
end
