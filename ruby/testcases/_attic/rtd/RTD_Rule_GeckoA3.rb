=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author:  sfrieske, 2016-03-08
Version: 1.1.0

History:
  2017-05-23 sfrieske, moved test341_3qt here (from former RTD_Rule_Gecko06)
  2021-09-15 sfrieske, finally replaced $svtest by @@svtest, $sv by @@sv and $sm by @@sm
  2022-01-10 sfrieske, moved update_lotsummary_lot from RTD::Test here
=end

require 'apfam'
require 'jcap/lmtrans'
require 'rtdtest'
require 'RubyTestCase'


# Testcases for RTD GateController A3 Rule
# Monitor: http://f36wwd2:8000/cgi-bin/dispatching/infoscreen/ie_qt_gate_controller_status_gecko.pl?instance_id=gecko
class RTD_Rule_GeckoA3 < RubyTestCase
  @@rtda3_defaults = {route: 'UTRT-GECKOA3.01', product: 'UT-PRODUCT-GECKOA3.01', carriers: 'EQA%',
    rule: 'QA_gecko_rule', category: 'DISPATCHER/QA'}
  @@rtd_defaults = {rule: 'QA_ie_qt_gate_controller_data', category: 'DISPATCHER/QA', carriers: 'EQA%'}
  @@sublottype = 'PO'         # 'QD', 'EJ'
  @@eqp = 'UTF002'  # at opNo_trigger
  @@opNo_trigger = '1000.500'
  @@eqp2 = 'UTF001' # at opNo_target
  @@opNo_target = '1000.600'    # keep the Q-time short to make runpath happier
  @@ccat_nofoups = 'UTFEOL'     # for special tests
  # PD specific exceptions for Jumper (EJ) lots
  @@jumper_exception = false    # Jumpers behave like regular lots if the PD is an exception PD (CSIG-PRECLN-*)
  @@route_exception = 'UTRT-GECKOA3.21'
  @@product_exception = 'UT-PRODUCT-GECKOA3.21'
  # for capacity test (code29), longer QT
  @@route2 = 'UTRT-GECKOA3.02'
  @@product2 = 'UT-PRODUCT-GECKOA3.02'
  @@route2_exception = 'UTRT-GECKOA3.22'
  @@product2_exception = 'UT-PRODUCT-GECKOA3.22'
  # standard location (consistent with other rule tests)
  @@loc1 = {'AmhsArea'=>'F36', 'Location'=>'M1-L3-MAIN'}
  # for 3 Q-times in a row
  @@product3 = 'UT-PRODUCT-GECKO.04'
  @@route3 = 'UTRT-GECKO.04'
  @@opNo_trigger1 = '1000.450'
  @@opNo_target1 = '1000.900'
  @@opNo_trigger2 = '1000.900'
  @@opNo_target2 = '1000.950'
  @@opNo_trigger3 = '1000.950'
  @@opNo_target3 = '1000.990'

  @@amjob_handle = 'Job-99'    # http://f36apfd1:21241/jobs/details/Job-99
  @@amjob_name = '/QUEUE_TIME/ie_qt_gate_controller_gecko'


  def self.after_all_passed
    @@testlots.each {|lot|
      @@rtdtest.lmtrans.delete_lotsummary_lot(lot)
      @@sv.delete_lot_family(lot)
    }
    @@carriers.each {|c| @@sv.carrier_status_change(c, 'NOTAVAILABLE', check: true)}
  end


  def setup
    return unless self.class.class_variable_defined?(:@@rtda3test)
    assert_equal 0, @@sv.eqp_cleanup(@@eqp, mode: 'Auto-1')
    assert_equal 0, @@sv.eqp_cleanup(@@eqp2, mode: 'Auto-1')
    assert @@sm.change_recipeparameter(@@lrcp_target, 'RecipeToolRunTime', '1')  # lower than QT
    assert @@sm.mainpd_change_carrier_category(@@rtda3test.route, @@opNo_target, @@rtda3test.carrier_category)
  end


  def test00_setup
    $setup_ok = false
    #
    assert @@lmtrans = LotManager::Transformer.new($env)
    # RTD client
    assert @@rtda3test = RTD::Test.new($env, @@rtda3_defaults.merge(sv: @@sv))
    @@sm = @@rtda3test.sm
    @@rtda3test.parameters = ['EqpID', @@eqp, 'cast_regex', @@rtda3test.carriers.sub('%', '')]
    assert @@rtda3test.check_rule, 'wrong RTD rule called, setup issue?'
    assert @@rtda3test.stocker_location(@@rtda3test.stocker, @@loc1), "error setting stocker UDATA"
    assert @@rtda3test.eqp_location(@@eqp, @@loc1), "error setting eqp UDATA"
    assert @@rtda3test.eqp_location(@@eqp2, @@loc1), "error setting eqp UDATA"
    assert @@rtda3test.check_eqp(@@eqp), "wrong eqp setup"
    assert @@rtda3test.check_eqp(@@eqp2), "wrong eqp setup"
    @@sm = @@rtda3test.sm
    # RTD client for Gecko runpath checks
    @@rtdtest = RTD::Test.new($env, @@rtd_defaults.merge(sv: @@rtda3test.sv, sm: @@rtda3test.sm))
    # AM client, ensure AM job can be controlled
    @@apfam = APF::AM.new($env)
    assert details = @@apfam.job_details(@@amjob_handle)
    assert_equal @@amjob_name, File.join(details[:folder], details[:name]), "wrong job handle"
    # remove old lots on the test routes
    [@@rtda3test.route, @@route_exception, @@route2, @@route3].each {|route|
      @@sv.lot_list(route: route).each {|lot|
        @@rtdtest.lmtrans.delete_lotsummary_lot(lot)
        @@sv.delete_lot_family(lot)
      }
    }
    # special routes
    if @@jumper_exception
      @@rtda3test.route = @@route_exception
      @@rtda3test.product = @@product_exception
    end
    # prepare 4 carriers for the test
    assert @@carriers = @@sv.carrier_list(carrier: @@rtda3test.carriers, carrier_category: @@rtda3test.carrier_category).take(4)
    assert_equal 4, @@carriers.size, "not enough carriers"
    @@carriers.each {|c|
      # delete leftover lots from previous tests
      @@sv.carrier_status(c).lots.each {|lot| assert @@sv.delete_lot_family(lot)}
      assert @@rtda3test.cleanup_carrier(c)
      assert_equal 0, @@sv.carrier_usage_reset(c)
    }
    # create test lot
    @@testlots = []
    assert @@lot = @@rtda3test.new_lot(sublottype: @@sublottype)
    @@testlots << @@lot
    assert self.class.update_lotsummary_lot(@@lot, @@opNo_trigger, @@opNo_target, eqp_cleanup: true)
    # check eqps
    assert @@sv.lot_eqplist_in_route(@@lot)[@@opNo_trigger].include?(@@eqp), "wrong eqp"
    assert @@sv.lot_eqplist_in_route(@@lot)[@@opNo_target] == [@@eqp2], "wrong eqp"
    # logical recipes
    ops = @@sv.lot_processlist_in_route(@@lot)
    @@lrcp_trigger = @@sm.object_info(:pd, ops[@@opNo_trigger]).first.specific[:lrcps][nil]
    @@lrcp_target = @@sm.object_info(:pd, ops[@@opNo_target]).first.specific[:lrcps][nil]
    #
    $setup_ok = true
  end

  def test310_happy
    $setup_ok = false
    #
    assert_equal 0, @@sv.lot_opelocate(@@lot, @@opNo_trigger)
    assert @@rtdtest.check_tlrunpath(filter: {'lot_id'=>@@lot}), "no runpath"
    assert @@apfam.wait_run_job(@@amjob_handle), "error running #{@@amjob_handle}"
    assert @@rtda3test.verify_a3codes([], wait: false), "wrong A3 code"
    #
    $setup_ok = true
  end

  def test311_outdated_data  # requires the happy setup from test210, then outdate
    $log.info "waiting 30 min for the GateKeeper result file to expire"; sleep 1810
    rcs = ['PO', 'QD'].include?(@@sublottype) || @@jumper_exception ? [49] : []
    assert @@rtda3test.verify_a3codes(rcs), "wrong A3 code"
  end

  def test312_runpath
    assert_equal 0, @@sv.lot_opelocate(@@lot, @@opNo_trigger)
    assert @@rtdtest.check_tlrunpath(filter: {'lot_id'=>@@lot}), "no runpath"
    assert_equal 0, @@sv.eqp_cleanup(@@eqp2, portstatus: 'Down')
    sleep @@rtda3test.apf_delay
    assert @@apfam.wait_run_job(@@amjob_handle), "error running #{@@amjob_handle}"
    rcs = ['PO', 'QD'].include?(@@sublottype) || @@jumper_exception ? [79] : []
    assert @@rtda3test.verify_a3codes(rcs, wait: false), "wrong A3 code"
  end

  def test313_unscheduled
    assert @@apfam.wait_run_job(@@amjob_handle), "error running #{@@amjob_handle}"
    assert lot = @@rtda3test.new_lot(sublottype: @@sublottype)
    @@testlots << lot
    assert self.class.update_lotsummary_lot(lot, @@opNo_trigger, @@opNo_target)
    assert_equal 0, @@sv.lot_opelocate(lot, @@opNo_trigger)
    assert @@rtdtest.check_tlrunpath(filter: {'lot_id'=>lot}), "no runpath"
    rcs = ['PO', 'QD'].include?(@@sublottype) || @@jumper_exception ? [61] : []
    assert @@rtda3test.verify_a3codes(rcs, filter: {'lot_id'=>lot}, wait: false), "wrong A3 code"
  end

  def test314_unscheduled2
    lots = @@sv.lot_list(route: @@rtda3test.route)
    refute_empty lots, "2nd lot required else resultfile is empty -> code 41"  # check if at least 1 lot is at @@opNo_trigger?
    assert @@apfam.wait_run_job(@@amjob_handle), "error running #{@@amjob_handle}"
    assert lot = @@rtda3test.new_lot(sublottype: @@sublottype)
    @@testlots << lot
    assert self.class.update_lotsummary_lot(lot, @@opNo_trigger, @@opNo_target)
    assert_equal 0, @@sv.lot_opelocate(lot, @@opNo_trigger)
    assert @@rtdtest.check_tlrunpath(filter: {'lot_id'=>lot}), "no runpath"
    $log.info "waiting 25 min for the Gecko internal run check.."; sleep 1500
    rcs = ['PO', 'QD'].include?(@@sublottype) || @@jumper_exception ? [80] : []
    assert @@rtda3test.verify_a3codes(rcs, filter: {'lot_id'=>lot}, wait: false), "wrong A3 code"
  end

  def test315_no_empty_foup
    assert_equal 0, @@sv.lot_opelocate(@@lot, @@opNo_trigger)
    # change target PD's required carrier category (to make it a transfer PD) to a one with not enough empty carriers
    assert @@sm.mainpd_change_carrier_category(@@rtda3test.route, @@opNo_target, @@ccat_nofoups)
    cc = @@sv.carrier_list(carrier_category: @@ccat_nofoups)
    cc.each {|c| @@sv.carrier_status_change(c, 'AVAILABLE', check: true)}
    assert_equal 0, @@sv.carrier_status_change(cc, 'NOTAVAILABLE')
    assert_equal 0, @@sv.lot_requeue(@@lot)
    #
    assert @@rtdtest.check_tlrunpath(filter: {'lot_id'=>@@lot}), "no runpath"
    assert @@apfam.wait_run_job(@@amjob_handle), "error running #{@@amjob_handle}"
    rcs = ['PO', 'QD'].include?(@@sublottype) || @@jumper_exception ? [92] : []
    assert @@rtda3test.verify_a3codes(rcs, wait: false, filter: {'lot_id'=>@@lot}), "wrong A3 code"
  end

  def test321_split
    assert lot = @@rtda3test.new_lot(sublottype: @@sublottype)
    @@testlots << lot
    assert self.class.update_lotsummary_lot(lot, @@opNo_trigger, @@opNo_target)
    assert lc = @@sv.lot_split(lot, 5, mergeop: @@opNo_target)
    assert self.class.update_lotsummary_lot(lc, @@opNo_trigger, @@opNo_target)
    assert_equal 0, @@sv.lot_opelocate(lc, @@opNo_trigger)
    assert @@rtdtest.check_tlrunpath(filter: {'lot_id'=>lc}), "no runpath"
    assert @@apfam.wait_run_job(@@amjob_handle), "error running #{@@amjob_handle}"
    rcs = ['PO', 'QD'].include?(@@sublottype) || @@jumper_exception ? [104] : []
    assert @@rtda3test.verify_a3codes(rcs, filter: {'lot_id'=>lc}, wait: false), "wrong A3 code"
  end

  def test331_capacity_lot
    # remove all other test lots
    (@@testlots - [@@lot]).each {|lot| assert @@sv.lot_opelocate(lot, :first)}
    # 2nd lot on this route, lower priority
    assert lot2 = @@rtda3test.new_lot(priorityclass: 5, sublottype: @@sublottype)
    @@testlots << lot2
    assert self.class.update_lotsummary_lot(lot2, @@opNo_trigger, @@opNo_target)
    assert_equal 0, @@sv.lot_opelocate(lot2, @@opNo_trigger)
    assert_equal 0, @@sv.lot_opelocate(@@lot, @@opNo_trigger)
    assert @@rtdtest.check_tlrunpath(filter: {'lot_id'=>@@lot}), "no runpath"
    # set target RecipeToolRunTime to the QT duration, trigger RecipeToolRunTime very short
    assert oinfo = @@sm.object_info(:mainpd, @@rtda3test.route).first
    assert duration = oinfo.specific[:operations].find {|e| e.opNo == @@opNo_trigger}.qtimes[1].duration
    assert @@sm.change_recipeparameter(@@lrcp_trigger, 'RecipeToolRunTime', '1')
    assert @@sm.change_recipeparameter(@@lrcp_target, 'RecipeToolRunTime', (duration + 1).to_s)
    sleep @@rtda3test.apf_delay
    # verify lot codes
    assert @@apfam.wait_run_job(@@amjob_handle), "error running #{@@amjob_handle}"
    assert @@rtda3test.verify_a3codes([], filter: {'lot_id'=>@@lot}), "wrong A3 code"
    rcs = ['PO'].include?(@@sublottype) || @@jumper_exception ? [11] : []
    assert @@rtda3test.verify_a3codes(rcs, response: :last, filter: {'lot_id'=>lot2}), "wrong A3 code"
    #
    # 3rd lot that has no TL constraint but is blocked by the 2nd one
    # QT duration must be RecipeToolRunTime for 1st lot + 2nd lot (else it gets blocked byitself with code 11)
    rte = @@jumper_exception ? @@route2_exception : @@route2
    prd = @@jumper_exception ? @@product2_exception : @@product2
    ## setup note: changed QT duration from 9/10 to 60 / 70 because the AM job runs longer now
    assert oinfo2 = @@sm.object_info(:mainpd, rte).first
    assert oinfo2.specific[:operations].find {|e| e.opNo == @@opNo_trigger}.qtimes[1].duration > duration * 2, "wrong QT setup at #{@@route2}"
    assert lot3 = @@rtda3test.new_lot(priorityclass: 5, route: rte, product: prd, sublottype: @@sublottype)
    @@testlots << lot3
    assert self.class.update_lotsummary_lot(lot3, @@opNo_trigger, @@opNo_target)
    assert_equal 0, @@sv.lot_opelocate(lot3, @@opNo_trigger)
    assert @@rtdtest.check_tlrunpath(filter: {'lot_id'=>lot3}), "no runpath"
    # verify lot codes
    assert @@apfam.wait_run_job(@@amjob_handle), "error running #{@@amjob_handle}"
    assert @@rtda3test.verify_a3codes([], wait: false, filter: {'lot_id'=>@@lot}), "wrong A3 code"
    rcs = ['PO'].include?(@@sublottype) || @@jumper_exception ? [11] : []
    assert @@rtda3test.verify_a3codes(rcs, response: :last, filter: {'lot_id'=>lot2}), "wrong A3 code"
    rcs = ['PO'].include?(@@sublottype) || @@jumper_exception ? [29] : []
    assert @@rtda3test.verify_a3codes(rcs, response: :last, filter: {'lot_id'=>lot3}), "wrong A3 code"
  end

  def test341_3qt  # scenario 06_15, no SLT dependencies
    # remove all old test lots to avoid code 11
    @@sv.lot_list(route: @@route3).each {|lot| @@sv.delete_lot_family(lot)}
    # create new test lot
    assert lot = @@rtdtest.new_lot(route: @@route3, product: @@product3)
    @@testlots << lot
    # make the lot happy
    assert self.class.update_lotsummary_lot(lot, @@opNo_trigger1, @@opNo_target3, eqp_cleanup: true)
    eqp = @@sv.lot_eqplist_in_route(lot)[@@opNo_trigger1].first
    @@rtda3test.parameters = ['EqpID', eqp, 'cast_regex', @@rtdtest.carriers.sub('%', '')]  # eqp at trigger1
    udata = @@sv.user_data(:stocker, @@rtdtest.stocker)
    assert @@rtda3test.eqp_location(eqp, udata)  # suppress codes 31 and 120
    # locate lot to the 1st trigger PD
    assert_equal 0, @@sv.lot_opelocate(lot, @@opNo_trigger1)
    assert @@rtdtest.check_tlrunpath(filter: {'lot_id'=>lot}), "lot #{lot} is not happy"
    # verify entries are only up to and including @@opNo_target1  note: APF setup is probably wrong if this test fails
    assert rops = @@sv.route_operations(@@route3)
    assert op_trigger1 = rops.find {|e| e.operationNumber == @@opNo_trigger1}.operationID.identifier
    assert op_target1 = rops.find {|e| e.operationNumber == @@opNo_target1}.operationID.identifier
    col_pd = @@rtdtest.response.headers.index('qt_pd_id')
    op_valid = nil
    @@rtdtest.filter_response(:last, filter: {'lot_id'=>lot}).rows.each {|row|
      op_valid = true if op_valid == nil && row[col_pd] == op_trigger1
      assert op_valid, "wrong data reported: #{row}\n wrong APF setup?"
      op_valid = false if op_valid == true && row[col_pd] == op_target1
    }
    # verify A3 codes, somehow code 11 is sometimes reported, e.g. if the QT is too short
    assert @@apfam.wait_run_job(@@amjob_handle), "error running #{@@amjob_handle}"
    assert @@rtda3test.verify_a3codes([], wait: false), "wrong A3 code"
    ## last resort in case code 11 appears: refute @@rtda3test.verify_a3codes([79], wait: false), "wrong A3 code"
    # take eqps at trigger3 (any PD after target1) down and verify again
    @@sv.lot_eqplist_in_route(lot)[@@opNo_trigger3].each {|eqp| assert_equal 0, @@sv.eqp_cleanup(eqp, portstatus: 'Down')}
    $log.info "waiting #{@@rtda3test.apf_delay} s for APF replication"; sleep @@rtda3test.apf_delay
    assert @@apfam.wait_run_job(@@amjob_handle), "error running #{@@amjob_handle}"
    assert @@rtda3test.verify_a3codes([79], wait: false), "wrong A3 code"
  end

  # semi-manual tests

  def testman351_resultfile_empty
    assert_equal 0, @@sv.lot_opelocate(@@lot, @@opNo_trigger)
    assert @@rtdtest.check_tlrunpath(filter: {'lot_id'=>@@lot}), "no runpath"
    assert @@apfam.wait_run_job(@@amjob_handle), "error running #{@@amjob_handle}"
    assert @@rtda3test.verify_a3codes([], wait: false), "wrong A3 code"
    #
    $log.info "make the resultfile empty and press ENTER to continue"
    gets
    rcs = ['PO', 'QD'].include?(@@sublottype) || @@jumper_exception ? [41] : []
    assert @@rtda3test.verify_a3codes(rcs, wait: false), "wrong A3 code"
  end

  def testman361_wip_ratio
    assert_equal 0, @@sv.lot_opelocate(@@lot, @@opNo_trigger)
    assert @@rtdtest.check_tlrunpath(filter: {'lot_id'=>@@lot}), "no runpath"
    assert @@apfam.wait_run_job(@@amjob_handle), "error running #{@@amjob_handle}"
    assert @@rtda3test.verify_a3codes([], wait: false), "wrong A3 code"
    #
    $log.info "change resultfile's column 'is_blocked_wip_ratio' to true and press ENTER to continue"
    gets
    rcs = ['PO', 'QD'].include?(@@sublottype) || @@jumper_exception ? [33] : []  # no exception
    assert @@rtda3test.verify_a3codes(rcs, wait: false), "wrong A3 code"
  end

  def testman371_intermediate1
    assert_equal 0, @@sv.lot_opelocate(@@lot, @@opNo_trigger)
    assert @@rtdtest.check_tlrunpath(filter: {'lot_id'=>@@lot}), "no runpath"
    assert @@apfam.wait_run_job(@@amjob_handle), "error running #{@@amjob_handle}"
    $log.info "change column 'is_block_qt_violation_intermediate' and press ENTER to continue"
    gets
    rcs = ['PO', 'QD'].include?(@@sublottype) || @@jumper_exception ? [74] : []
    assert @@rtda3test.verify_a3codes(rcs, wait: false), "wrong A3 code"
  end

  def testman372_intermediate2
    assert_equal 0, @@sv.lot_opelocate(@@lot, @@opNo_trigger)
    assert @@rtdtest.check_tlrunpath(filter: {'lot_id'=>@@lot}), "no runpath"
    assert @@apfam.wait_run_job(@@amjob_handle), "error running #{@@amjob_handle}"
    $log.info "change column 'is_block_no_runpath_intermediate' and press ENTER to continue"
    gets
    rcs = ['PO', 'QD'].include?(@@sublottype) || @@jumper_exception ? [75] : []
    assert @@rtda3test.verify_a3codes(rcs, wait: false), "wrong A3 code"
  end


  # aux methods

  # send lotManager data for an existing lot and a QT area, incl trigger and target ops, optionally clean up eqps
  def self.update_lotsummary_lot(lot, opNo_trigger, opNo_target, params={})
    lotops = params[:lotops] || @sv.lot_processlist_in_route(lot)
    loteqps = params[:loteqps] || @sv.lot_eqplist_in_route(lot)
    ret = true
    trigger_found = false
    lotops.each_pair {|opNo, op|
      trigger_found ||= (opNo == opNo_trigger)
      if trigger_found
        $log.info "data: #{lot}, #{opNo}, #{op}, #{loteqps[opNo]}"
        if params[:toolwish] == false
          pclbase = nil
          $log.info "  no toolwish"
        else
          toolwish = params[:toolwish] || ''  # NEVER, MUST or ''
          pclbase = LotManager::LmPclBase.new(toolwish, Time.now, 'QA Gecko Toolwish',
            31, Time.now, 'QA Gecko ToolScore', 4, 'MANUAL', Time.now, 'QA Gecko Selection')
          $log.info "  toolwish #{toolwish}"
        end
        loteqps[opNo].each {|eqp|
          ctx = LotManager::LmContext.new(nil, lot, op.split('.').first, eqp)
          data = LotManager::LotSummary.new(ctx, pclbase)
          if params[:delete]
            ret &= @lmtrans.delete_lotsummary(data: data)
          else
            ret &= @lmtrans.update_lotsummary(data: data)
          end
          ret &= @sv.eqp_cleanup(eqp, notify: false, mode: 'Auto-1').zero? if params[:eqp_cleanup]
        }
        break if opNo == opNo_target
      end
    }
    return ret && trigger_found
  end

end
