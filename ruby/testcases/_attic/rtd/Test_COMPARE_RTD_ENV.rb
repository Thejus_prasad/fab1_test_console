=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

  load_run_testcase 'Test_COMPARE_RTD_ENV', "itdc"

Author: Paul Peschel, 2012-10-15
Version: 1.0.0

=end

require "RubyTestCase"
require 'rtdruletest'
require 'rtdclient'

# Testcases for Compare RTD Rules for Migration Version 7 to Version 8
# 
# Both RDT Lists Version 7 and Version 8 from Tools will be get and compare.
#
# Rule in Version 7 must do the same like Rule in Version 8

class Test_COMPARE_RTD_ENV < RubyTestCase
  
  @@inputfile = "log/V7_V8_COMPARE/b2bTestcases.csv"
  @@output_list_v7 = "log/V7_V8_COMPARE/dispatchlistv7.csv"
  @@output_list_v8 = "log/V7_V8_COMPARE/dispatchlistv8.csv"
  @@result_file = "log/V7_V8_COMPARE/b2bTestresults.csv"
  @@result_file_qa = "log/V7_V8_COMPARE/resultqa.csv"
  @@result_header = "#station called,report,category1,category2,additional parameter,max retries,rule v7,duration v7,rule v8,duration v8,rows v7,rows v8,retries,passed"
  @@warmuptime = 600 #in seconds

  #struct for input parameter for rtd call
  InputParams = Struct.new(
    :station,
    :rule,
    :category1,
    :category2,
    :parameter,
    :wait,
    :retries,
    :exclude)

  def setup
    $rtdc7 = RTD::Client.new("#{$env}_v7") unless $rtdc7 and $rtdc7.env == "#{$env}_v7"
    $rtdc8 = RTD::Client.new("#{$env}_v8") unless $rtdc8 and $rtdc8.env == "#{$env}_v8"
    super
    ##assert $setup_ok, "test setup is not correct"
  end

  def test01_prepare_test
    @@tlist = File.readlines(@@inputfile).collect {|s|
      next if s.start_with?("#")
      a = s.chomp.split("|")
      InputParams.new(
        a[0].gsub(/station=/,""),
        a[1].gsub(/rule=/,""),
        a[2].gsub(/category=/,""),
        a[3].gsub(/category=/,""),
        a[4].gsub(/parameter=/,""),
        a[5].to_f,
        a[6] == "" ? 1 : a[6].to_i,
        (a[7].upcase.split(',') + ["AGE", "TIME"]).uniq
      )
    }.compact

    # create result files with headers
    [@@output_list_v7, @@output_list_v8].each {|f|
      open(f, "wb") {|fh| fh.write("Test starttime: " + Time.now.iso8601 + "\n")}
    }
    open(@@result_file, "wb") {|fh| fh.write("Test starttime: " + Time.now.iso8601 + "\n")}
    open(@@result_file_qa, "wb") {|fh| fh.write("Test starttime: " + Time.now.iso8601 + "\n")}
    write_result(@@result_file, @@result_header)
  end

  def test10_warmup
    # Start Values
    wutime = Time.now + @@warmuptime
    queue = []
    # call each Rule to fill dispatcher cache
    @@tlist.each {|k|
      k.retries.times{ queue << k }
    }
    queue = queue.flatten.shuffle
    
    while Time.now < wutime
      queue.push(queue.shift)
      q = queue.first
      list7, list8 = get_rtd_list(q.station, q.rule, q.category1, q.category2)
    end
  end
  
  def test20_compare_rules
    # test all rules from inputfile and compare
    @@tlist.each {|k|
      list7 = list8 = nil
      ntry = 0
      ok = false
      params_ary = k.parameter.gsub(/\$/,'').split(/[=,]/) # gsub(\$) deletes $ in string params. split (/[=,]/) splits at "=" and ","
      
      k.retries.times {
        ntry += 1
        errors = false
        list7, list8 = get_rtd_list(k.station, k.rule, k.category1, k.category2, params_ary)
        sleep k.wait if ntry > 1
        
        # check environment
        if list7.headers.first.start_with?("#error") or list8.headers.first.start_with?("#error")
          write_result(@@result_file_qa, "error calling rule #{k.station} in #{$rtdc7.env}: #{list7.headers.inspect}")
          write_result(@@result_file_qa, "error calling rule #{k.station} in #{$rtdc8.env}: #{list8.headers.inspect}")
          errors = true
          ok = "failed"
        else
          # compare header length
          headers_ok = (list7.headers.length == list8.headers.length)
          if headers_ok
            # compare header content
            list7.headers.each {|k1|
              write_result(@@result_file_qa, "#{k.station}: different field: #{k1}") unless list7.headers.member?(k1) == list8.headers.member?(k1)
              write_result(@@result_file_qa, "#{k.station}: different position: #{k1} - v7: #{list7.headers.index(k1)} || v8: #{list8.headers.index(k1)}") unless list7.headers.index(k1) == list8.headers.index(k1)
            }
          else
            write_result(@@result_file_qa, "difference in #{k.station} counts of fields: #{$rtdc7.env}: #{list7.headers.length}, #{$rtdc8.env}: #{list8.headers.length}")
            errors = true
            next
          end
        
          # generate blacklist
          blacklist = (k.exclude + list7.headers.collect{|h| h.upcase if h.upcase.include?("AGE") or h.upcase.include?("TIME")}.compact).uniq
          
          # compare rows
          row7 = list7.rows ? list7.rows.size: 0
          row8 = list7.rows ? list8.rows.size: 0
          [row7, row8].each{|r| errors = true if r == 0}
          if row7 != row8
            write_result(@@result_file_qa, "#{k.station}: row count mismatch. #{$rtdc7.env}: #{row7}, #{$rtdc8.env}: #{row8}")
            errors = true
          else
            list7.rows.each_with_index {|r, j|
              r.each_with_index {|row, i|
                next if blacklist.include?(list7.headers[i].upcase) || blacklist.include?(list8.headers[i].upcase)
                if list7.rows[j][i] != list8.rows[j][i]
                  write_result(@@result_file_qa, "#{k.station} mismatch in row #{j+1}, field #{i+1}")
                  errors = true
                end
              }
            }
          end
        end
        (ok = true; break) unless errors
      }
      
      unless ok
        # write complete dispatch list on error
        write_result(@@output_list_v7, list7.headers.unshift("Station Called #{k.station}").join(', '))
        list7.rows.each {|r| write_result(@@output_list_v7, r.unshift(k.station).join(', '))} if list7.rows
        write_result(@@output_list_v8, list8.headers.unshift("Station Called #{k.station}").join(', '))
        list8.rows.each {|r| write_result(@@output_list_v8, r.unshift(k.station).join(', '))} if list8.rows
      end
      # write rule test result
      res = []
      rowv7 = list7.rows ? list7.rows.size: 0
      rowv8 = list8.rows ? list8.rows.size: 0
      
      rowv7 = "error" if list7.headers.first.start_with?("#error")
      rowv8 = "error" if list8.headers.first.start_with?("#error")

      res = [k.station, k.rule, k.category1, k.category2, params_ary.join("|"), k.retries, list7.rule, list7.process_duration, list8.rule, list8.process_duration, rowv7, rowv8, ntry, ok]
      write_result(@@result_file, res* ', ')
    }
  end
  
  def get_rtd_list(station=nil, rule=nil, cat1=nil, cat2=nil, parameters=nil)
    # get dispatch lists
    cat1="/DISPATCHER/RULES" if cat1 == ""
    cat2="/DISPATCHER/RULES" if cat2 == ""
    threads = []
    list7 = nil
    list8 = nil
    threads << Thread.new {list7 = $rtdc7.dispatch_list(station, :report=>rule, :category=>cat1, :parameters=>parameters)}
    threads << Thread.new {list8 = $rtdc8.dispatch_list(station, :report=>rule, :category=>cat2, :parameters=>parameters)}
    threads.each {|th| th.join}
    return list7, list8
  end
  
  def write_result(filename, s, mode="ab")
    open(filename, mode) {|fh| fh.write(s + "\n")}
  end
end