=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author:  sfrieske, 2017-03-15
Version: 17.03

History:
  2020-05-07 sfrieske, changed wafer_history_report call
  2021-04-12 sfrieske, moved check_routeop from RTD::Test here, only usage
  2022-01-10 sfrieske, fixed eqpstate
=end

require 'rtdtest'
require 'jcap/toolevents'
require 'RubyTestCase'


class RTD_Rule_ChamberBalancing < RubyTestCase
  @@rtd_defaults = {route: 'UTRT-CHAMBER-BALANCING.01', product: 'UT-PROD-CHAMBER-BALANCE.01', carriers: '%'}
  @@op = 'UTPD-CHAMBER-BALANCING1.01'
  @@op2 = 'UTPD-CHAMBER-BALANCING2.01'

  @@eqp = 'ETX950'      # with special recipe setup in Armor/RMS
  @@ch = 'PM2'          # chamber for MRCP at @@op
  @@ch2 = 'PM3'         # chamber for MRCP at @@op2
  @@eqpstate = 'SDT.4MTN'
  @@eqpstate_N = 'UDT.5DLY'
  ##@@lrcp = 'UTLR-CHAMBER-BALANCING1' # mrcp: 'B-Q-ETX950.Q-PM2-SIO-ER.01'
  ##@@lrcp2 = 'UTLR-CHAMBER-BALANCING2' # mrcp: 'B-Q-ETX950.Q-PM3-SIO-ER.01'
  # note: mrcps for [PM2:PM3] 'B-P-ETX950.P-3260C-PASET-B.01', 'B-P-ETX950.P-2970G-PASET-B.01'
  @@rtd_proctime = 7500  # 25 wafers x 300 s

  @@rule = 'E_etx_cb'  # not called directly, just for verification
  @@apf_sync = 15
  @@testlots = []


  def self.after_all_passed
    @@testlots.each {|lot| @@sv.delete_lot_family(lot)}
  end


  def test00_setup
    $setup_ok = false
    #
    # RTD client
    assert @@rtdtest = RTD::Test.new($env, @@rtd_defaults.merge(sv: @@sv, station: @@eqp)) # rule is NOT set, called via station
    @@sm = @@rtdtest.sm
    assert res = @@rtdtest.client.dispatch_list(@@eqp)   # check_rule would work, but passes @@rule as report parameter
    assert_equal @@rule, res.rule, "wrong RTD rule, setup issue"
    assert @@idx_planeqp = res.headers.find_index('PlanEqp')
    assert @@idx_grank = res.headers.find_index('GRANK')
    assert @@idx_lrank = res.headers.find_index('LRANK')
    assert @@idx_lot = res.headers.find_index('Lot ID')
    #
    @@wh = ToolEvents::WaferHistory.new($env, sv: @@sv)
    # set eqp and stocker Location
    assert @@sm.update_udata(:stocker, @@rtdtest.stocker, {'Location'=>'F36'})
    assert @@sm.update_udata(:eqp, @@eqp, {'Location'=>'F36'})
    assert @@rtdtest.check_eqp(@@eqp)
    #
    [@@op, @@op2].each {|op|
      # set recipe parameters
      lrcp = @@sm.object_info(:pd, op).first.specific[:lrcps][nil]
      assert @@sm.object_info(:lrcp, lrcp).first.specific[:recipe].first[1]['AUTO-3'], "missing setup for PD #{op}, LRCP #{lrcp}"
      assert @@sm.change_recipeparameter(lrcp, 'AUTO-3', '+A')
      # set PD udata, OperationReportingType +A for A3 and +D for E10DownA3 (see below)
      assert @@sm.update_udata(:pd, op, {'E10ConditionalAvailable'=>'Yes', 'OperationReportingType'=>'+A+D'})
      # check route operation sanity from APF perspective, return true on success
      $log.info "check_routeop "#{op.inspect}"
      oinfo = @@sm.object_info(:mainpd, @@rtdtest.route, details: true).first || ($log.warn 'route not found in SM'; return)
      assert entry = oinfo.specific[:operations].find {|e| e.op == op}, 'no such route operation'
      assert_equal @@rtdtest.carrier_category, entry.carrier_category, 'wrong carrier_category'
    }
    # SM Eqp State UDATA
    assert @@sm.update_udata(:equipmentstate, @@eqpstate, {'E10DownA3'=>'+D'})
    #
    $setup_ok = true
  end

  def testman01_toolevents
    # create test lot and move it to the PD for 1 chamber
    assert lot = @@rtdtest.new_lot, "error creating test lot"
    @@testlots << lot
    assert res = @@sv.claim_process_prepare(lot)
    cj, cjinfo, eqpiraw, pstatus, mode = res
    assert cj = @@sv.eqp_opestart(@@eqp, @@sv.lot_info(lot).carrier)
    assert li = @@sv.lot_info(lot, wafers: true, select: [2, 3])
    assert @@wh.wafer_history_report(cj, li, eqpiraw.equipmentID.identifier, pstatus.portID.identifier)
  end

  def test11_cond_available
    # create test lot and move it to the PD for 1 chamber
    assert lot = @@rtdtest.new_lot, "error creating test lot"
    @@testlots << lot
    assert_equal 0, @@sv.lot_opelocate(lot, nil, op: @@op)
    # prepare eqp and call rule, A3 = 'Y'
    assert_equal 0, @@sv.eqp_cleanup(@@eqp)
    assert @@rtdtest.verify_a3codes([], filter: {'Lot ID'=>lot}), "wrong A3"
    assert_equal @@eqp, @@rtdtest.response.rows.first[@@idx_planeqp], "wrong PlanEqp"
    # change a chamber's E10 state to 5DLY and call rule again -> A3 = 'N'
    assert_equal 0, @@sv.chamber_status_change(@@eqp, @@ch, @@eqpstate_N.split('.')[1])   # 'SDT.4MTN' -> '4MTN'
    assert @@rtdtest.verify_a3codes([13], filter: {'Lot ID'=>lot}), "wrong A3"
    assert_equal @@eqp, @@rtdtest.response.rows.first[@@idx_planeqp], "wrong PlanEqp"
    # change a chamber's E10 state to 4MTN and call rule again -> A3 = 'Y'
    assert_equal 0, @@sv.chamber_status_change(@@eqp, @@ch, @@eqpstate.split('.')[1])     # 'SDT.4MTN' -> '4MTN'
    assert @@rtdtest.verify_a3codes([], filter: {'Lot ID'=>lot}), "wrong A3"
    assert_equal @@eqp, @@rtdtest.response.rows.first[@@idx_planeqp], "wrong PlanEqp"
  end

  def test12_balancing
    # create 3 more test lots
    assert lots = @@rtdtest.new_lots(4 - @@testlots.size), "error creating test lots"
    @@testlots += lots
    # prepare lots
    # lots 1,2 at PD1 (for chamber 1), prio 0
    @@testlots[0..1].each {|lot|
      assert_equal 0, @@sv.schdl_change(lot, priority: 0)
      assert_equal 0, @@sv.lot_opelocate(lot, nil, op: @@op)
    }
    # lots 3,4 at PD2 (for chamber 4), prio 4
    @@testlots[2..3].each {|lot|
      assert_equal 0, @@sv.schdl_change(lot, priority: 4)
      assert_equal 0, @@sv.lot_opelocate(lot, nil, op: @@op2)
    }
    # cleanup eqp
    assert_equal 0, @@sv.eqp_cleanup(@@eqp)
    #
    # call the rule and verify A3, PlanEqp
    assert @@rtdtest.call_rule
    @@testlots.each_with_index {|lot, idx|
      assert @@rtdtest.verify_a3codes([], response: :last, filter: {'Lot ID'=>lot}), "wrong A3"
      res = @@rtdtest.filter_response(:last, filter: {'Lot ID'=>lot})
      assert_equal @@eqp, res.rows.first[@@idx_planeqp], "wrong PlanEqp"
    }
    # verify order (LRANK or seq, prio 0/ch1 - prio4/ch2 - prio0/ch1 - prio4/ch2)
    @@rtdtest.response.rows.each_with_index {|row, idx|
      lots = [0, 2].member?(idx) ? @@testlots[0..1] : @@testlots[2..3]
      assert lots.member?(row[@@idx_lot]), "wrong lot order"
    }
    #
    # call the rule again with a long proctime for carrier1 and verify order (prio0 - prio4 - prio4 - prio0)
    c0 = @@sv.lot_info(@@testlots[0]).carrier
    assert @@rtdtest.call_rule(wait: false, parameters: ['ept', "#{c0}:#{@@rtd_proctime * 2.1}"])
    @@rtdtest.response.rows.each_with_index {|row, idx|
      lots = [0, 3].member?(idx) ? @@testlots[0..1] : @@testlots[2..3]
      assert lots.member?(row[@@idx_lot]), "wrong lot order"
    }
  end

end
