=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author:  sfrieske, 2016-03-08
Version: 1.0.0

History:
  2016-12-09 sfrieske, using new rtdtest.rb
  2021-09-15 sfrieske, finally replaced $svtest by @@svtest, $sv by @@sv and $sm by @@sm
=end

require 'rtdtest'
require 'RubyTestCase'


# Testcases for RTD DOWN Rule
class RTD_Rule_DOWN < RubyTestCase
  @@rtd_defaults = {carriers: 'EQA%', rule: 'QA_DOWN_TEST', category: 'DISPATCHER/QA'}
  @@sv_defaults = {carriers: 'EQA%'}
  @@eqp = 'UTC003'
  @@opNo = '1000.700'
  @@eqpstate = 'SDT.4MTN'

  @@rule = 'QA_DOWN_TEST'
  @@apf_sync = 15

  # combinations of [PD UDATA OperationReportingType, LRCP Parameter AUTO-3, EqpState UDATA E10DownA3, A3] to test
  @@data = [
    # E10DownA3 YES
    ['', '+D', '', false],
    ['', '+D', 'YES', true],
    ['+D', '', '', false],
    ['+D', '', 'YES', true],
    ['+M', '', 'YES', false],
    # E10DownA3 +M (only opreptype matters)
    ['+M', '', '', false],    # reference
    ['+M', '', '+M', true],
    ['+D,+M', '', '+M', true],
    ['+M,+D', '', '+M', true],
    ['', '', '+M', false],
    ['+D', '', '+M', false],
    ['+D+M', '', '+M', false],
    ['+M+D', '', '+M', false],
    #   TC24 lrcp parameter (no influence)
    ['', '+D', '+M', false],
    ['', '+D+M', '+M', false],
    ['', '+D,+M', '+M', false],
    ['', '-D', '+M', false],
    # E10DownA3 +D
    ['', '', '+D', false],
    ['-D', '', '+D', false],
    ['+M,+D', '', '+D', true],
    ['', '+A+D', '+D', true],
  ]


  def self.shutdown
    @@sv.user_parameter_change('Eqp', @@eqp, 'Pull', '')
    @@sm.update_udata(:pd, @@op, {'AutomationType'=>'', 'OperationReportingType'=>''})
  end

  def self.after_all_passed
    @@testlots.each {|lot| @@sv.delete_lot_family(lot)}
    @@sv.carrier_status_change(@@carrier, 'NOTAVAILABLE', check: true)
    @@sv.eqp_cleanup(@@eqp)
  end


  def test00_setup
    $setup_ok = false
    #
    assert @@rtdtest = RTD::Test.new($env, @@rtd_defaults.merge(sv: @@sv))
    @@sm = @@rtdtest.sm
    @@rtdtest.parameters = ['EqpID', @@eqp, 'cast_regex', @@rtdtest.carriers.sub('%', '')]
    assert res = @@rtdtest.client.dispatch_list(nil, report: @@rtdtest.rule, category: @@rtdtest.category)
    assert_equal @@rtdtest.rule, res.rule, 'wrong rule, RTD setup?'
    assert @@rtdtest.check_eqp(@@eqp), "wrong eqp setup"
    #
    # prepare 1 carrier for the test
    assert @@carrier = @@sv.carrier_list(carrier: @@rtdtest.carriers, carrier_category: @@rtdtest.carrier_category).first
    @@sv.carrier_status(@@carrier).lots.each {|lot| assert @@sv.delete_lot_family(lot)}  # remove leftover lots from previous tests
    assert @@rtdtest.cleanup_carrier(@@carrier)
    assert_equal 0, @@sv.carrier_usage_reset(@@carrier)
    #
    # create test lot with sublottype 'Equipment Monitor' or 'Dummy'
    assert @@lot = @@rtdtest.new_lot(carrier: @@carrier)
    @@testlots = [@@lot]
    assert_equal 0, @@sv.sublottype_change(@@lot, 'Equipment Monitor')
    @@rtdtest.filter = {'lot_id'=>@@lot, 'eqp_id'=>@@eqp}
    # locate lot to the eqp op
    assert_equal 0, @@sv.lot_opelocate(@@lot, @@opNo)
    #
    # store PD and logical recipe for further reference
    @@op = @@sv.lot_info(@@lot).op
    assert_equal 'Yes', @@sv.user_data_pd(@@op)['E10ConditionalAvailable'], 'missing setup'
    @@lrcp = @@sm.object_info(:pd, @@op).first.specific[:lrcps][nil]
    assert @@sm.object_info(:lrcp, @@lrcp).first.specific[:recipe].first[1]['AUTO-3'], "missing setup"
    #
    $setup_ok = true
  end

  def test11_eqp
    # clean up eqp
    assert_equal 0, @@sv.eqp_cleanup(@@eqp, mode: 'Off-Line-1'), 'error cleaning up sorter'
    assert @@rtdtest.verify_a3codes([]), "wrong setup"
    # set eqp state to 4MTN
    assert_equal 0, @@sv.eqp_status_change(@@eqp, '2NDP')
    assert_equal 0, @@sv.eqp_status_change(@@eqp, @@eqpstate.split('.')[1])   # 'SDT.4MTN' -> '4MTN'
    #
    # tests
    @@fails_eqp = []
    @@data.each_with_index {|parameters, i|
      opreptype, auto3, e10downa3, a3 = parameters
      $log.info "-- ##{i}: #{parameters}"
      assert set_parameters(opreptype, auto3, e10downa3)
      assert response = @@rtdtest.call_rule(parameters: @@rparameters, select: {'lot_id'=>@@lot, 'eqp_id'=>@@eqp})
      res = @@rtdtest.verify_a3codes(a3 ? [] : [10], response: response)
      @@fails_eqp << i unless res
      ##assert res, "failed: ##{i}: #{parameters}"
    }
    assert_empty @@fails_eqp, "failed eqp tests: #{@@fails_eqp}"
  end

  def test12_chamber
    # clean up eqp
    assert_equal 0, @@sv.eqp_cleanup(@@eqp, mode: 'Off-Line-1'), 'error cleaning up sorter'
    assert response = @@rtdtest.call_rule(parameters: @@rparameters, select: {'lot_id'=>@@lot, 'eqp_id'=>@@eqp})
    assert @@rtdtest.verify_a3codes([], response: response), "wrong setup"
    # set eq state to 4MTN
    chs = @@sv.eqp_info(@@eqp).chambers.collect {|e| e.chamber}
    assert_equal 0, @@sv.chamber_status_change(@@eqp, chs, '2NDP')
    assert_equal 0, @@sv.chamber_status_change(@@eqp, chs, @@eqpstate.split('.')[1])   # 'SDT.4MTN' -> '4MTN'
    #
    # tests
    @@fails_ch = []
    @@data.each_with_index {|parameters, i|
      opreptype, auto3, e10downa3, a3 = parameters
      $log.info "-- ##{i}: #{parameters}"
      assert set_parameters(opreptype, auto3, e10downa3)
      assert response = @@rtdtest.call_rule(parameters: @@rparameters, select: {'lot_id'=>@@lot, 'eqp_id'=>@@eqp})
      res = @@rtdtest.verify_a3codes(a3 ? [] : [13], response: response)
      ##res = verify_a3codes(a3 ? [] : [13])
      @@fails_ch << i unless res
      ##assert res, "failed: ##{i}: #{parameters}"
    }
    assert_empty @@fails_ch, "failed chamber tests: #{@@fails_ch}"
  end


  # aux methods

  def set_parameters(opreptype, auto3, e10downa3)
    $log.info "set_parameters #{opreptype.inspect}, #{auto3.inspect}, #{e10downa3.inspect}"
    assert @@sm.update_udata(:pd, @@op, {'OperationReportingType'=>opreptype})
    assert @@sm.change_recipeparameter(@@lrcp, 'AUTO-3', auto3)
    assert @@sm.update_udata(:equipmentstate, @@eqpstate, {'E10DownA3'=>e10downa3})
  end

  def current_parameters
    {'OperationReportingType'=>@@sv.user_data_pd(@@op)['OperationReportingType'],
      'AUTO-3'=>@@sm.object_info(:lrcp, @@lrcp).first.specific[:recipe].first[1]['AUTO-3'],
      'E10DownA3'=>@@sm.object_info(:equipmentstate, @@eqpstate).first.udata['E10DownA3']
    }
  end

end
