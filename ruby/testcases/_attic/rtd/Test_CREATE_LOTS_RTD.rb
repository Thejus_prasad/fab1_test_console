=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

  load_run_testcase 'Test_CREATE_LOTS_RTD', "itdc"

Author: Paul Peschel, 2012-12-07
Version: 1.0.0

=end

require "RubyTestCase"
require 'rtdruletest'
require 'rtdclient'

# Testcases for create lots for RTD rules
# 
# Create lots for RTD tools if no lots are on tool

class Test_CREATE_LOTS_RTD < RubyTestCase
  
  @@inputfile = "log/lots/lots.csv"
  @@result_file = "log/lots/result.csv"

  #struct for input parameter for lot create
  InputParams = Struct.new(
    :count,
    :product,
    :route,
    :equipment,
    :customer,
    :sublottype,
    :opNo)
  
  def setup
    super
    $rtdt = RTD::RuleTest.new($env, :sv=>$sv) unless $rtdt and $rtdt.env == $env
    $rtdc = RTD::Client.new($env) unless $rtdc and $rtdc.env == $env
    assert $setup_ok, "test setup is not correct"
  end

  def test01_prepare_test
    @@tlist = File.readlines(@@inputfile).collect {|s|
      next if s.start_with?("#")
      a = s.split("|")
      InputParams.new(
        a[0].to_i,
        a[1],
        a[2],
        a[3],
        a[4],
        a[5],
        a[6]
      )
    }.compact
    open(@@result_file, "wb") {|fh| fh.write("create starttime: " + Time.now.iso8601 + "\n")}
  end
  
  def test10_create_lots
    # variablen definition
    ok = true
    result = []
    resultqa = []
    comment = "ERPITEM=3006AA-UA0,DPML=2.0,PLANNED_FSD=#{Time.now.strftime('%Y-%m-%d')}" 
    # create lots from list
    @@tlist.each {|k|
      # set fields if null
      ok = true
      lotsn = []
      cc = get_cc(k.equipment)
      k.customer = "gf" unless k.customer
      k.sublottype = "PO" unless k.sublottype
      # create lots
      k.count.times {
        res = $sv.new_lot_release(:product=>k.product, :customer=>k.customer, :route=>k.route, :sublottype=>k.sublottype, :carrier=>"G%", :carrier_category=>cc, :comment=>comment, :stbdelay=>1)
        lotsn += [res] if res
      }
      result << "From the product: #{k.product}, customer: #{k.customer}, route: #{k.route}, sublottype: #{k.sublottype} are #{lotsn.size} from #{k.count} created, Lots are #{lotsn.inspect}\n"
      ok = false unless k.count == lotsn.length
      (lotsn.each{|l| assert_equal 0, $sv.lot_opelocate(l, k.opNo) if k.opNo}) unless lotsn.empty?
    }
    puts result
    write_result(@@result_file, result.to_s)
  end
  
  def get_cc(eqp)
    return "BEOL" unless eqp
    count = 0
    ncat = ""
    eq = $sv.eqp_info(eqp)
    cat = eq.ports.collect{|p| p.category}
    pgt = cat.flatten.uniq
    pgt.each {|pgs| (count = cat.count(pgs); ncat=pgs) if count <= cat.count(pgs)}
    return ncat
  end
  
  def write_result(filename, s, mode="ab")
    open(filename, mode) {|fh| fh.write(s)}
  end
end