=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

	load_run_testcase 'Test_F_CVD', "itdc"

Author: Paul Peschel, 2013-01-28
Version: 1.0.0

=end

require "RubyTestCase"
require 'rtdruletest'
require 'rtdclient'

# Testcases for RTD Rule F_CVD
#
# If run testcases alone over "load_run_testcase 'Test_F_CVD', "itdc", :tp => /20/" you must do first:
#
# 1. @@lots = ""
#
# 3. @@lots_all = ""
#
# 4. Call "test01_prepare_test", because without that, you have no Lot

class Test_F_CVD < RubyTestCase

  @@waittime = 10
  @@blocktime = 30*60 #time to stop test, test have to wait for start again at least blocktime for restart
  @@lots = []
  @@lots_all = []
  @@lotsn = []
  @@opEqps = "RTDC01 RTDC02".split
  $result_rtd = []

  def setup
    super
    $rtdt = RTD::RuleTest.new($env, :sv=>$sv) unless $rtdt and $rtdt.env == $env
    $rtdc = RTD::Client.new($env) unless $rtdc and $rtdc.env == $env
    assert $setup_ok, "test setup is not correct"
  end

  # ###################################################### Setup des Tests ##########################################################
  #
  # Prepare all equipment and lots for the Test
  #
  # See also testsetup in excel file Testsetup.xls in sheet "Testsetup_F_cvd32x".
  # Here described all important stuff for the tests in this File.
  #
  # http://myteamsdrs2/sites/FICSQA/_layouts/xlviewer.aspx?id=/sites/FICSQA/Documentation/Technical%20Documentation/RTD/Testcases_F_CVD32.xlsx

  def test01_check_time
    @@opEqps.each {|eq|
      eqptime = $sv.eqp_info(eq).ports.map{|p| p.disp_time.to_i}.sort
      difftime = Time.now.to_i - eqptime[0]
      if difftime < @@blocktime
        $setup_ok = false
        $log.warn("test have to wait for start again at least blocktime #{difftime/60} min for restart")
      end
    }
  end

  def test02_prepare_test
      # Return Value
      ok = true

      # Expected Values for Lot
      user = "X-UNITTEST"
      opNo = "1000.1000"

      # EQP Cleanup
      @@opEqps.each {|eq|
        eqp = $sv.eqp_info(eq)
        ok &= ($sv.eqp_opestart_cancel(eqp.eqp, eqp.cjs[0]) == 0) unless eqp.cjs == []
      }
      ok &= $rtdt.eqp_cleanup(@@opEqps)
      ok &= $rtdt.stocker_cleanup()
      ok &= $sv.eqp_status_change_req("RTDC03", "2NDP") == 0

      # Create Lot
      lotsold = $sv.lot_list(:route =>"P-RTDC-MAINPD01.01")
      $rtdt.delete_lots(lotsold) if lotsold.size > 0
      @@lots_all = $rtdt.create_lots_rtd("RTDC-VHTSIN240.01", :count=>7)
      @@lots_all += $rtdt.create_lots_rtd("RTDC-HDSIN400.01", :count=>1)
      @@lots_all += $rtdt.create_lots_rtd("RTDC-SIN450.01", :count=>1)
      @@lots_all += $rtdt.create_lots_rtd("RTDC-HDSIN400.01", :count=>1)
      @@lots_all += $rtdt.create_lots_rtd("RTDC-SIN450.01", :count=>1)
      @@lots_all += $rtdt.create_lots_rtd("RTDC-UDOX40.01", :count=>5)
      # Change Lot to PD and other Stuff
      @@lots_all.each {|l|
        li = $sv.lot_info(l)
        ok &= ($sv.lot_hold_release(l, nil) == 0) if li.status == "ONHOLD"
        ok &= ($sv.lot_opelocate(l, opNo) == 0) unless li.opNo == opNo
        ok &= ($sv.carrier_reserve_cancel(li.carrier) == 0) unless $sv.carrier_status(li.carrier).xfer_user == ""
        unless li.xfer_status == "SI" && li.stocker == "UTSTO11"
          ok &= ($sv.carrier_xfer_status_change(li.carrier, "SO", "INTER1") == 0)
          ok &= ($sv.carrier_xfer_status_change(li.carrier, "MI", "UTSTO11") == 0)
        end
      }
      # Cancel setup if user is wrong
      setup_correct = false unless $sv.user == user and ok == true

      # Check Setup is correct
      if setup_correct == false
        $log.info("Setup has wrong User or wrong Setup")
      end
      @@lots += @@lots_all
  end

  ####################################################### Testcases for RTD #########################################################

  # *Testcase*: builds cascade vhtsin limit
  #
  # *Number*: 20
  #
  # *Description*: Test the continuation of current cascade and observance of a max time limit for VHTSIN and following running a SIN-Cascade
  #
  # *Logic*:
  #
  #  * RTDC-VHTSIN240.01 1 lot
  #  * RTDC-HDSIN400.01
  #  * RTDC-SIN450.01
  #  * RTDC-UDOX40.01    1 lot
  #
  #  One lot with product "RTDC-VHTSIN240.01" must be process on tool RTDC01.
  #  One lot with product "RTDC-UDOX40.01" must be process on tool RTDC02.
  #
  #  1 x Call from F_cvd32x Rule for RTDC01
  #  Result: Cascades will be continue
  #
  #  RTDC01: first 3 lots with RTDC-VHTSIN240.01 with A3=Y (all other Lots A3=N)
  #
  #  RTDC02: all lots with RTDC-UDOX40.01 with A3=Y (all other Lots A3=N)
  #
  #  RTDC03: empty
  #
  # *Condition*:
  #
  # OperationReportingType ist DispatchFamily	 -1---1-	 Dispatch Load Balancing
  #
  # *Setup*:
  # * Product: RTDC-VHTSIN240.01, RTDC-HDSIN400.01, RTDC-SIN450.01, RTDC-UDOX40.01
  # * Tools: RTDC01 RTDC02 RTDC03
  # * PD: 1000.1000
  # * Scriptparameter:
  # * Carriercategory: BEOL
  # * Splitlot: No

  def test20_builds_cascade_vhtsin_limit
    # Start values
    stocker = "UTSTO11"
    state2 = "MO"
    opNo = "1000.1000"

    # Testcase: builds cascade vhtsin limit
    # create and prepare lots
    @@lotsn = $rtdt.create_lots_rtd("RTDC-VHTSIN240.01")
    @@lotsn += $rtdt.create_lots_rtd("RTDC-UDOX40.01")
    @@lotsn.each {|l|
      li = $sv.lot_info(l)
      assert ($sv.lot_hold_release(l, nil) == 0) if li.status == "ONHOLD"
      assert ($sv.lot_opelocate(l, opNo) == 0) unless li.opNo == opNo
      assert ($sv.carrier_reserve_cancel(li.carrier) == 0) unless $sv.carrier_status(li.carrier).xfer_user == ""
      assert ($sv.carrier_xfer_status_change(li.carrier, state2, stocker) == 0)
    }
    # process lots
    @@opEqps.each {|eqp| assert_equal 0, $sv.eqp_mode_offline1(eqp, :notifyTCS=>false)}
    refute_equal "", cj = $sv.claim_process_lot(@@lotsn[0], :eqp => @@opEqps[0])
    refute_equal "", cj = $sv.claim_process_lot(@@lotsn[1], :eqp => @@opEqps[1])
    @@opEqps.each {|eqp| assert_equal 0, $sv.eqp_mode_auto3(eqp, :notifyTCS=>false)}
    sleep @@waittime
    @@lots += @@lotsn
  end

  def test22_RTDC01
    eqp = @@opEqps[0]  # RTDC01
    list = $sv.rtd_dispatch_list(eqp)
    rows = list.rows
    headers = list.headers

    #expectedres lot, prod, a3, planeqp
    r2a3 = "YYYNNNNNNNNNNNNN".split("")
    r2pe = "RTDC01 RTDC01 RTDC01 RTDC02 RTDC02 RTDC02 RTDC01 RTDC01 RTDC01 RTDC01 RTDC01 RTDC02 RTDC02 RTDC02 RTDC02 RTDC02".split
    expectedres = @@lots_all.collect{|l| [l, $sv.lot_info(l).product, r2a3[@@lots_all.index(l)], r2pe[@@lots_all.index(l)]]}

    rows.each_with_index {|r, v|
      unless r[headers.index("Lot ID")] == expectedres[v][0] and \
        r[headers.index("Product ID")] == expectedres[v][1] and \
        r[headers.index("A3")] == expectedres[v][2] and \
        r[headers.index("PlanEqp")] == expectedres[v][3]
        $result_rtd << "#{__method__}: Lot #{expectedres[v][0]} does not match RTD criteria"
      end
    }
  end

  def test24_RTDC02
    eqp = @@opEqps[1]  # RTDC02
    list = $sv.rtd_dispatch_list(eqp)
    header = list.headers
    rows = list.rows
    li = $sv.lot_info(@@lotsn[@@opEqps.index(eqp)])
    rows.each {|r|
      if li.product != r[header.index("Product ID")] and \
                 "" == r[header.index("A3Info")] and \
                "Y" == r[header.index("A3")]
        $result_rtd << "#{__method__}: Lot ID #{r[header.index("Lot ID")]} from Product #{r[header.index("Product ID")]} on Equipment #{eqp} should be not A3 Status N"
      end
      if li.product == r[header.index("Product ID")] and \
                eqp == r[header.index("PlanEqp")] and \
                "N" ==  r[header.index("A3")]
        $result_rtd << "#{__method__}: Lot ID #{r[header.index("Lot ID")]} from Product #{r[header.index("Product ID")]} on Equipment #{eqp} should be not A3 Status Y"
      end
    }
    #expectedres lot, prod, a3, planeqp
    r2a3 = "NNNNNNNNNNNYYYYY".split("")
    r2pe = "RTDC01 RTDC01 RTDC01 RTDC02 RTDC02 RTDC02 RTDC01 RTDC01 RTDC01 RTDC01 RTDC01 RTDC02 RTDC02 RTDC02 RTDC02 RTDC02".split
    expectedres = @@lots_all.collect{|l| [l, $sv.lot_info(l).product, r2a3[@@lots_all.index(l)], r2pe[@@lots_all.index(l)]]}

    list = $sv.rtd_dispatch_list(eqp)
    rows = list.rows
    headers = list.headers

    rows.each_with_index {|r, v|
      unless r[headers.index("Lot ID")] == expectedres[v][0] and \
        r[headers.index("Product ID")] == expectedres[v][1] and \
        r[headers.index("A3")] == expectedres[v][2] and \
        r[headers.index("PlanEqp")] == expectedres[v][3]
        $result_rtd << "#{__method__}: Lot #{expectedres[v][0]} does not match RTD criteria"
      end
    }
  end

  def test26_RTDC03
    $result_rtd << "#{__method__}: On RTD list from tool RTDC03 are lots" if $sv.rtd_dispatch_list("RTDC03").rows.size > 0
  end

  # ###################################################### Result and Delete Lots ###################################################

  def test999_results
    puts "Test OK" if $result_rtd.size == 0
    puts "Test NOK, #{$result_rtd.size} failerures found" if $result_rtd.size > 0

    $result_rtd.each {|i|
      pp i
      if $result_rtd.index(i)%10 == 9
        print "show more Errors, press y?"
        res_input = gets
        break unless res_input.start_with?("y")
      end} if $result_rtd.size > 0

    puts "\ndelete all lots: #{@@lots.inspect} (y/n)?"
    answer = STDIN.gets
    @@lots.each {|l| $sv.delete_lot_family(l, :delete => false)} if "jy".index(answer.chomp.downcase)
  end
end
