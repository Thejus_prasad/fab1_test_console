=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author:  sfrieske, 2018-03-27

Version: 1.0.0

History:
  2022-01-10 sfrieske, moved set_lmtoolwish from RTD::Test here
=end

require 'rtdtest'
require 'jcap/lmtrans'
require 'RubyTestCase'


# lot = 'UXYT16004.000'

# Testcases for RTD Rule L_ALC_RDI_SCHED
#   requires ITDC spec Eqp http://vf1sfcd01:10080/setupfc-rest/v1/specs/C07-00003546/content?format=html
#   C14-QARTCL01
#
class RTD_Rule_ALCRDI < RubyTestCase
  @@rtd_defaults = {route: 'C02-1550.01', product: 'SHELBY31AK.B1', apf_delay: 240,
    station: 'ALC', rule: 'L_ALC_RDI_SCHED', category: 'DISPATCHER/SCHEDULING'}
  @@headers = nil             # will be read from parameters file
  @@opNo = '4200.1100'        # '1900.1000'  # '3600.1100'
  @@op = 'BH-MSKA-28BK.01'    # proper RouteProgram etc spec setup
  @@eqp = 'ALC303'
  @@eqptype = 'ALC30X'
  @@reticle = 'SHELBY30BHAZUA1'
  @@reticle_stocker = 'RET1130'     # UDATA Location is read, value is F38
  @@reticle_tgtlocation = 'Module2' # matches stocker's location F38
  @@testlots = []
  

  def self.after_all_passed
    @@testlots.each {|lot| @@sv.delete_lot_family(lot)}
    nil
  end


  def test00_setup
    $setup_ok = false
    #
    assert @@lmtrans = LotManager::Transformer.new($env)
    # RTD rule and headers
    assert @@rtdtest = RTD::Test.new($env, @@rtd_defaults.merge(sv: @@sv))
    assert @@rtdtest.check_rule(headers: @@headers), 'wrong RTD rule called, setup issue?'
    # eqp type: UDATA DispatchFamily 'ALC-1-'
    assert_equal 'ALC-1-', @@rtdtest.sm.object_info(:eqptype, @@eqptype).first.udata['DispatchFamily'], 'wrong eqptype UDATA DispatchFamily'
    #
    # setup reticle, reticle in rg at mask layer of op and AVAILABLE
    assert opi = @@rtdtest.sm.object_info(:mainpd, @@rtdtest.route).first.specific[:operations].find {|e| e.op == @@op}
    assert rs = @@rtdtest.sm.object_info(:product, @@rtdtest.product).first.specific[:reticleset]
    assert pli = @@rtdtest.sm.object_info(:reticleset, rs).first.specific[:rgs].find {|e| e[:photolayer] == opi.photolayer}
    assert rg = pli[:rgs].first, 'no reticlegroup defined'
    assert_equal @@reticle, @@rtdtest.sm.object_ids(:reticle, :all, search: {reticlegroup: rg}).collect {|e| e.identifier}.first, 'wrong reticle'
    assert_equal 'AVAILABLE', @@sv.reticle_status(@@reticle).reticleStatusInfo.reticleStatus, 'wrong reticle status'
    assert_equal 0, @@sv.inhibit_cancel(:reticle, @@reticle)
    assert_equal 0, @@sv.reticle_status_change(@@reticle, 'AVAILABLE', check: true)
    assert_equal 0, @@sv.reticle_xfer_status_change(@@reticle, 'EO', eqp: @@eqp)
    sparams = @@sv.user_parameter('Reticle', @@reticle, all: true)
    sparams.each {|p|
      value = p.datatype == 'STRING' ? 'OK' : 99
      assert_equal 0, @@sv.user_parameter_change('Reticle', @@reticle, p.name, value, 1, datatype: p.datatype)
    }
    parms = {'Location'=>@@reticle_stocker, 'TargetLocation'=>@@reticle_tgtlocation}
    assert_equal 0, @@sv.user_parameter_change('Reticle', @@reticle, parms)
    #
    $setup_ok = true
  end

  def test11
    assert lot = @@rtdtest.new_lot, 'error creating testlot'
    @@testlots << lot
    # locate to opNo? reticle for lot, something is still missing
    assert @@sv.claim_process_lot(lot)
    assert_equal 0, @@sv.lot_cleanup(lot, op: @@op)
    assert self.class.set_lmtoolwish(@@eqp, lot, @@op, 'MUST')
    #
    assert @@rtdtest.call_rule, 'error calling rule'
    assert row = @@rtdtest.filter_response(:last, filter: {'job_lot_id'=>lot}).rows.first, "lot #{lot} missing"
  end

  # aux methods

  def self.set_lmtoolwish(eqp, lot, op, toolwish)
    $log.info "set_lmtoolwish #{eqp.inspect}, #{lot.inspect}, #{op.inspect}, #{toolwish.inspect}"
    pclbase = LotManager::LmPclBase.new(toolwish, Time.now, 'QA RTDTest Toolwish',
      31, Time.now, 'QA RTDTest ToolScore', 4, 'MANUAL', Time.now, 'QA RTDTest Selection')
    ctx = LotManager::LmContext.new(nil, lot, op.split('.').first, eqp)
    data = LotManager::LotSummary.new(ctx, pclbase)
    return @@lmtrans.update_lotsummary(data: data)
  end

end
