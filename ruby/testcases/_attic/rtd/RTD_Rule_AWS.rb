=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author:  sfrieske, 2016-01-25

Version: 1.1.4

History:
  2016-12-21 sschoen3, add require 'rtdtest' and change row 297 from $rtdclient.a3codes to RTD.a3codes
  2017-01-02 sfrieske, use RTD::Test to verify A3 codes, removed local implementation
  2017-08-30 sfrieske, added TC19
  2019-01-15 sfrieske, minor cleanup
  2021-04-21 sfrieske, moved @sorter and @pg form SiView::Test to SJC::Test
  2022-01-10 sfrieske, moved set_lmtoolwish from RTD::Test here

Notes:
  Handles on-route WhatNext and SJC jobs (status UNPROCESSED or PENDING)

  requirements: carrier MI/SI and AVAILABLE, AmhsArea of eqp and carrier's stocker must match
  PD UDATA OperationReportingType must include AllowMultiPass if lot passcount > 1
  PD UDATA AutomationType *XFER* or empty (-> RND)
  route op must have a required cast cat
  port state LoadReq and dispatch state Required and no sj, Eqp ScriptParameter Pull must contain ports in A-1, modes Auto-1, -3

  A3='N' in any of these cases:
  1. nicht genug available empty FOUPs for XFER (20)
  2. nicht genug saubere FOUPs im Modul sind (siehe EMPTY Regel, evtl. mit verminderter Testcaseanzahl)
  3. das Los an PD mit AutomationType NOFOUPOPEN steht
  4. Loadports an AWSen für Protokollzone nicht vorhanden sind
  5. Loadport mcopemode_id ungleich  "Auto-3" oder "Semi-Start-3" oder "Auto-1" oder "Semi-Start-1" ist
  6. ScriptParameter "Pull " am Eqp nicht PortID beinhaltet
  7. einer der Loadports der Portgruppe nicht in port_disp_state=="Required" AND port_state=="LoadReq" steht

  Nur für on-route gibt es außerdem folgende Testfälle, bei denen die Lose A3="N" werden sollten:
  8. Automation Type (UDATA an der PD) = "XFER" und MultilotFOUPS
  9. Combine jobs mit weniger als 2 beteiligten Losen

  FSDR docs: http://f1onewiki:21080/display/F1Disp/DokuAaws
=end

require 'jcap/lmtrans'
require 'jcap/sjctest'
require 'rtdserver/rtdemuremote'
require 'rtdtest'
require 'rtddb'
require 'apfam'
require 'util/waitfor'
require 'RubyTestCase'


# Testcases for RTD AWS Rule
class RTD_Rule_AWS < RubyTestCase
  @@rtd_defaults = {product: 'UT-PRODUCT000.AWS', route: 'UTRT001.AWS', carriers: 'EQA%', rule: 'QA_A_aws'}
  @@rtd_twparams = {rule: 'QA_TWCHECK', category: 'DISPATCHER/RULES'}  # check if test objects pass the toolwish macro
  @@sorter = 'UTS001'
  @@opNo = '1000.930'   # @@sorter as opEqp, Class1
  @@opNo2 = '1000.990'  # @@sorter as opEqp, just another opNo
  ##@@starts2 = 3         # for Class2 carriers
  ##@@starts3 = 6         # for Class3 carriers
  ##@@nempty = 3          # limit is changed to 3 for FEOL in ITDC, normally 20 - configured in ie_dispatch_config
  # for customer FOSB tests, UTS001
  ##@@opNoSBMO = '2000.990'
  # for SJC tests
  @@loc1 = {'AmhsArea'=>'F36', 'Location'=>'M1-L3-MAIN'}
  @@loc2 = {'AmhsArea'=>'F38', 'Location'=>'M2-L3-MAIN'}
  @@stocker2 = 'UTSTO13'
  @@amjob_handle = 'Job-90'  # http://f36apfd1:21241/jobs/details/Job-90
  @@amjob_name = '/DISPATCH/set_target_location_for_aws'
  # for 4xxx carrier tests
  @@carriers4 = '4EQA%'

  @@apf_delay = 30
  @@testlots = []


  def self.startup
    super
    @@lmtrans = LotManager::Transformer.new($env)
    @@sjctest = SJC::Test.new($env, sv: @@sv)
    @@rtdtest = RTD::Test.new($env, @@rtd_defaults.merge(sv: @@sv, apf_delay: @@apf_delay))
    @@rtdtest.parameters = ['EqpID', @@sorter, 'cast_regex', @@rtdtest.carriers.sub('%', '')]
    @@rtdtest.station = @@sorter  # new since 2019
    @@sm = @@rtdtest.sm
    @@mds = RTD::MDS.new($env)
    @@rtdtwck = RTD::Test.new($env, @@rtd_twparams.merge(
      sv: @@sv, sm: @@sm, lmtrans: nil, apf_delay: @@apf_delay, station: @@sorter,
      parameters: ['EqpID', @@sorter, 'cast_regex', @@rtdtest.carriers.sub('%', '')]
    ))
    @@rtdremote = RTD::EmuRemote.new($env)
    @@rtdremote.add_emustation(@@sorter, method: 'empty_reply')  # suppress AutoProc activities
  end

  def self.shutdown
    @@sv.user_parameter_change('Eqp', @@sorter, 'Pull', '')
    @@rtdremote.delete_emustation(@@sorter)
    @@sm.update_udata(:pd, @@op, {'AutomationType'=>''})
    @@sjctest.cleanup_sort_requests(@@rtdtest.carriers)
  end

  def self.after_all_passed
    @@testlots.each {|lot| @@sv.delete_lot_family(lot)}
    @@sv.carrier_list(carrier: @@rtdtest.carriers, carrier_category: @@rtdtest.carrier_category).each {|c|
      @@sv.carrier_status_change(c, 'NOTAVAILABLE', check: true)
    }
    @@sv.carrier_list(carrier: 'EE%').each {|c| @@sv.carrier_status_change(c, 'NOTAVAILABLE', check: true)}
  end

  def setup
    @@sjctest.cleanup_sort_requests(@@rtdtest.carriers)
    # move the lots out of the way to avoid code 56 (batch)
    @@testlots.each {|lot|
      @@sv.lot_family(lot).each {|l| assert_equal 0, @@sv.lot_cleanup(l, opNo: :first)}
    }
  end


  def test00_setup
    $setup_ok = false
    #
    # RTD rules
    # assert @@rtdtest.check_rule, 'wrong RTD rule called, setup issue?'
    assert res = @@rtdtest.client.dispatch_list(nil, report: @@rtdtest.rule, category: @@rtdtest.category)
    assert_equal @@rtdtest.rule, res.rule, 'wrong rule, RTD setup?'
    # AM client, ensure AM job can be controlled
    assert @@apfam = APF::AM.new($env)
    assert details = @@apfam.job_details(@@amjob_handle)
    assert_equal @@amjob_name, File.join(details[:folder], details[:name]), 'wrong job handle'
    # set AmhsArea and Location (for test19)
    assert @@rtdtest.stocker_location(@@rtdtest.stocker, @@loc1), 'error setting UDATA'
    assert @@rtdtest.stocker_location(@@stocker2, @@loc2), 'error setting UDATA'
    assert @@rtdtest.eqp_location(@@sorter, @@loc1), 'error setting UDATA'
    @@sorterports = @@sv.eqp_info(@@sorter).ports.collect {|p| p.port}.join(',')  # for scrip param 'Pull'
    # prepare 2 carriers for the test lots and 1 emptys carrier for SJ
    cc = @@sv.carrier_list(carrier: @@rtdtest.carriers, carrier_category: @@rtdtest.carrier_category).take(3)
    assert_equal 3, cc.size, "not enough empty carriers #{@@rtdtest.carriers}"
    cc.each {|c|
      @@sv.carrier_status(c).lots.each {|lot| assert @@sv.delete_lot_family(lot)}  # remove leftover lots from previous tests
      assert @@rtdtest.cleanup_carrier(c)
      assert_equal 0, @@sv.carrier_usage_reset(c)
    }
    # prepare test lot
    assert @@testlots = @@rtdtest.new_lots(2), 'error creating test lots'
    @@lot, @@lot1 = @@testlots
    # locate lot to the sorter op
    assert_equal 0, @@sv.lot_opelocate(@@lot, @@opNo)
    # store PD for further reference
    @@op = @@sv.lot_info(@@lot).op
    # disable all other equipments
    @@sv.lot_info(@@lot).opEqps.each {|eqp| assert_equal 0, @@sv.eqp_status_change(eqp, '2NDP')}
    assert @@rtdtest.cleanup_eqp(@@sorter), 'error cleaning up sorter'
    #
    $setup_ok = true
  end


  # Automation Type RND

  def test11_a3_yes
    assert @@sm.update_udata(:pd, @@op, {'AutomationType'=>''})  # empty is treated as RND
    assert_equal 0, @@sv.lot_cleanup(@@lot, opNo: @@opNo, stocker: @@rtdtest.stocker), 'error cleaning up lot'
    ['Auto-1', 'Auto-3', 'Semi-Start-1', 'Semi-Start-3'].each {|mode|
      $log.info "-- mode #{mode}"
      assert_equal 0, @@sv.eqp_cleanup(@@sorter, mode: mode), 'error cleaning up sorter'
      assert_equal 0, @@sv.user_parameter_change('Eqp', @@sorter, 'Pull', @@sorterports) if mode.end_with?('-1')
      @@rtdtest.verify_a3codes([])
    }
  end

  def test12_a3_no_pull
    assert @@sm.update_udata(:pd, @@op, {'AutomationType'=>''})  # empty is treated as RND
    assert_equal 0, @@sv.lot_cleanup(@@lot, opNo: @@opNo, stocker: @@rtdtest.stocker), 'error cleaning up lot'
    assert self.class.set_lmtoolwish(@@rtdtest.stocker, @@lot, @@op, 'MUST'), 'error setting toolwish'
    assert resp = @@rtdtwck.call_rule
    assert resp = @@rtdtwck.filter_response(resp, filter: {'lot_id'=>@@lot})
    refute_empty resp.rows, 'lot did not pass base macro (toolwish?)'
    ['Auto-1', 'Auto-3', 'Semi-Start-1', 'Semi-Start-3'].each {|mode|
      $log.info "-- mode #{mode}"
      assert_equal 0, @@sv.eqp_cleanup(@@sorter, mode: mode), 'error cleaning up sorter'
      assert_equal 0, @@sv.user_parameter_change('Eqp', @@sorter, 'Pull', @@sorterports) if mode.end_with?('-1')
      # reference call with A3='Y'
      assert @@rtdtest.verify_a3codes([])
      # no Pull
      assert_equal 0, @@sv.user_parameter_change('Eqp', @@sorter, 'Pull', '')
      assert @@rtdtest.verify_a3codes(mode.end_with?('-1') ? 91 : [])
    }
  end

  def test13_wrong_modes
    assert @@sm.update_udata(:pd, @@op, {'AutomationType'=>''})  # empty is treated as RND
    assert_equal 0, @@sv.lot_cleanup(@@lot, opNo: @@opNo, stocker: @@rtdtest.stocker), 'error cleaning up lot'
    # reference call with A3='Y'
    assert_equal 0, @@sv.eqp_cleanup(@@sorter, mode: 'Auto-1')
    assert_equal 0, @@sv.user_parameter_change('Eqp', @@sorter, 'Pull', @@sorterports)
    assert @@rtdtest.verify_a3codes([])
    # wrong modes
    ['Off-Line-1', 'Semi-Start-2', 'Auto-2'].each {|mode|
      $log.info "-- mode #{mode}"
      assert_equal 0, @@sv.eqp_cleanup(@@sorter, mode: mode)
      assert_equal 0, @@sv.user_parameter_change('Eqp', @@sorter, 'Pull', @@sorterports)
      assert @@rtdtest.verify_a3codes(91)
    }
  end

  def test14_port_status
    assert @@sm.update_udata(:pd, @@op, {'AutomationType'=>''})  # empty is treated as RND
    assert_equal 0, @@sv.lot_cleanup(@@lot, opNo: @@opNo, stocker: @@rtdtest.stocker), 'error cleaning up lot'
    assert_equal 0, @@sv.eqp_cleanup(@@sorter, mode: 'Auto-3')
    # reference call with A3='Y'
    assert @@rtdtest.verify_a3codes([])
    # wrong port status
    ['UnloadReq', 'UnloadComp', 'LoadComp'].each {|portstatus|
      assert_equal 0, @@sv.eqp_cleanup(@@sorter, mode: 'Auto-3', portstatus: portstatus)
      assert @@rtdtest.verify_a3codes(36)
    }
  end

  def test15_dispatch_required
    assert @@sm.update_udata(:pd, @@op, {'AutomationType'=>''})  # empty is treated as RND
    assert_equal 0, @@sv.lot_cleanup(@@lot, opNo: @@opNo, stocker: @@rtdtest.stocker), 'error cleaning up lot'
    assert_equal 0, @@sv.eqp_cleanup(@@sorter, mode: 'Auto-3')
    # reference call with A3='Y'
    assert @@rtdtest.verify_a3codes([])
    # port reserved
    ports = @@sv.eqp_info(@@sorter).ports.collect {|p| p.port}
    ports[1..-1].each {|port| assert_equal 0, @@sv.eqp_port_status_change(@@sorter, port, 'Down')}
    assert_equal 0, @@sv.lot_cleanup(@@lot1, opNo: @@opNo, stocker: @@rtdtest.stocker), 'error cleaning up lot'
    assert @@sv.slr(@@sorter, lot: @@lot1, port: ports[0])
    assert @@rtdtest.verify_a3codes(36, filter: {'Lot ID'=>@@lot})
  end

  def test16_multilot
    assert @@sm.update_udata(:pd, @@op, {'AutomationType'=>''})  # empty is treated as RND
    # move @@lot out of the way and create a new one
    assert_equal 0, @@sv.lot_cleanup(@@lot, opNo: :first), 'error cleaning up lot'
    assert lot = @@rtdtest.new_lot
    @@testlots << lot
    assert_equal 0, @@sv.lot_cleanup(lot, opNo: @@opNo, stocker: @@rtdtest.stocker), 'error cleaning up lot'
    assert_equal 0, @@sv.eqp_cleanup(@@sorter, mode: 'Auto-3')
    assert lc = @@sv.lot_split(lot, 2)
    assert_equal 0, @@sv.lot_cleanup(lc, opNo: @@opNo2, stocker: @@rtdtest.stocker), 'error cleaning up lot'
    assert @@rtdtest.verify_a3codes([], filter: {'Lot ID'=>lot})
  end

  def test17_NOFOUPOPEN
    assert_equal 0, @@sv.lot_cleanup(@@lot, opNo: @@opNo, stocker: @@rtdtest.stocker), 'error cleaning up lot'
    assert_equal 0, @@sv.eqp_cleanup(@@sorter, mode: 'Auto-3')
    pd = @@sv.lot_info(@@lot).op
    assert @@sm.update_udata(:pd, pd, {'AutomationType'=>'NOFOUPOPEN'})
    assert @@rtdtest.verify_a3codes(151, filter: {'Lot ID'=>@@lot})
  end

  def test18_sjc_srid
    assert lot = @@rtdtest.new_lot
    @@testlots << lot
    assert_equal 0, @@sv.eqp_cleanup(@@sorter, mode: 'Auto-3', portstatus: 'LoadReq')
    # create SJC COMBINE request
    assert ec = @@rtdtest.get_empty_carrier, 'no empty carrier'
    assert @@rtdtest.cleanup_carrier(ec, stocker: @@rtdtest.stocker)
    assert @@sjctest.create_combine_verify(@@sv.lot_info(lot).carrier, ec, srccarrier_cat: 'FEOL', tgtcarrier_cat: 'FEOL'), 'error in SJC request'
    assert st = @@sjctest.mds.sjc_tasks(tgtcarrier: ec).first
    # verify RTD response contains the SR ID
    assert @@rtdtest.verify_a3codes([], filter: {'Lot ID'=>st.srid.to_s}), 'wrong A3 code, missing ie_dispatch_config?'
  end

  def test19_sjc_combine  # fails 2019-03-05
    # prepare lot and child in carriers at different locations
    assert lot = @@testlots.last
    assert ec = @@rtdtest.get_empty_carrier(stockin: true, stocker: @@stocker2)
    assert lc = @@sv.lot_split(lot, 5)
    assert_equal 0, @@sv.wafer_sort(lc, ec)
    assert_equal 0, @@sv.eqp_cleanup(@@sorter, mode: 'Auto-3', portstatus: 'LoadReq')
    # create SJC COMBINE request
    tgtcarrier = @@sv.lot_info(lot).carrier
    assert @@sjctest.create_combine_verify(@@sv.lot_info(lc).carrier, tgtcarrier, srccarrier_cat: 'FEOL', tgtcarrier_cat: 'FEOL'), 'error in SJC request'
    assert st = @@sjctest.mds.sjc_tasks(tgtcarrier: tgtcarrier).first
    # verify RTD response contains no line for the child lot or the SR ID
    assert response = @@rtdtest.call_rule, 'error calling RTD'
    assert_empty @@rtdtest.filter_response(response, filter: {'Lot ID'=>st.srid.to_s}).rows, 'wrong entry for SR ID'
    assert_empty @@rtdtest.filter_response(response, filter: {'Lot ID'=>lot}).rows, 'wrong entry for parent lot'
    assert_empty @@rtdtest.filter_response(response, filter: {'Lot ID'=>lc}).rows, 'wrong entry for child lot'
    # call AM Job
    assert @@apfam.wait_run_job(@@amjob_handle), "error running #{@@amjob_handle}"
    # verify result in T_SCRIPT_PARA
    value = nil
    assert wait_for {value = @@mds.script_para('Lot', lc, 'TargetLocation')}, 'missing entry in T_SCRIPT_PARA'
    assert value.include?('SJC'), "missing string 'SJC': #{value}"
    mod = {'F36'=>'Module1', 'F38'=>'Module2'}[@@loc1['AmhsArea']]  # hard coded mapping in RTD
    assert value.include?(mod), "wrong TargetLocation: #{value}"
    assert value.include?(@@rtdtest.stocker), "wrong stocker: #{value}"
    # need to clean up (?)
    assert_equal 0, @@sv.delete_lot_family(lot), 'error cleaning up lot'
  end

  # like test11 with carrier 4xxx, A3 no
  def test21_4xxx_a3_no
    assert @@sm.update_udata(:pd, @@op, {'AutomationType'=>''})  # empty is treated as RND
    @@sv.carrier_list(carrier: @@carriers4).each {|c| assert @@rtdtest.cleanup_carrier(c, make_empty: true)}
    assert lot = @@rtdtest.new_lot(carrier: @@carriers4), 'error creating test lot'
    @@testlots << lot
    assert_equal 0, @@sv.lot_cleanup(lot, opNo: @@opNo, stocker: @@rtdtest.stocker), 'error cleaning up lot'
    ['Auto-1', 'Auto-3', 'Semi-Start-1', 'Semi-Start-3'].each {|mode|
      $log.info "-- mode #{mode}"
      assert_equal 0, @@sv.eqp_cleanup(@@sorter, mode: mode), 'error cleaning up sorter'
      assert_equal 0, @@sv.user_parameter_change('Eqp', @@sorter, 'Pull', @@sorterports) if mode.end_with?('-1')
      assert @@rtdtest.verify_a3codes(69, parameters: ['EqpID', @@sorter, 'cast_regex', '4EQA']), 'wrong result'
    }
  end

  # like test18 with carrier 4xxx, A3 no
  def test22_sjc_srid
    @@sv.carrier_list(carrier: @@carriers4).each {|c| assert @@rtdtest.cleanup_carrier(c, stocker: @@rtdtest.stocker, make_empty: true)}
    assert lot = @@rtdtest.new_lot(carrier: @@carriers4), 'error creating test lot'
    @@testlots << lot
    assert_equal 0, @@sv.eqp_cleanup(@@sorter, mode: 'Auto-3', portstatus: 'LoadReq')
    # create SJC COMBINE request
    assert ec = @@rtdtest.get_empty_carrier, 'no empty carrier'
    assert @@rtdtest.cleanup_carrier(ec, stocker: @@rtdtest.stocker)
    assert @@sjctest.create_combine_verify(@@sv.lot_info(lot).carrier, ec, srccarrier_cat: 'FEOL', tgtcarrier_cat: 'FEOL'), 'error in SJC request'
    assert st = @@sjctest.mds.sjc_tasks(tgtcarrier: ec).first
    # verify RTD response contains the SR ID
    assert @@rtdtest.verify_a3codes(69, parameters: ['EqpID', @@sorter, 'cast_regex', '4EQA'], filter: {'Lot ID'=>st.srid.to_s}), 'wrong A3 code, missing ie_dispatch_config?'
  end


  # AutomationType XFER, enough empty carriers are required - not used anymore since 18.04

  def XXtest31_empty_carriers
    assert @@sm.update_udata(:pd, @@op, {'AutomationType'=>'XFER'})
    assert_equal 0, @@sv.lot_cleanup(@@lot, opNo: @@opNo, stocker: @@rtdtest.stocker), 'error cleaning up lot'
    assert_equal 0, @@sv.eqp_cleanup(@@sorter, mode: 'Auto-3')
    # ensure exactly @@nempty carriers are AVAILABLE
    @@sv.wafer_sort(@@lot1, '')  # so the lot doesn't leave an empty carrier later
    cc = @@sv.carrier_list(carrier: @@rtdtest.carriers, carrier_category: @@rtdtest.carrier_category, empty: true)
    assert cc.size >= @@nempty, 'not enough empty carriers'
    cc.each_with_index {|c, i|
      if i < @@nempty
        assert @@rtdtest.cleanup_carrier(c)
        assert_equal 0, @@sv.carrier_usage_reset(c)
      else
        assert_equal 0, @@sv.carrier_status_change(c, 'NOTAVAILABLE', check: true)
      end
    }
    cc = @@sv.carrier_list(carrier: @@rtdtest.carriers, carrier_category: @@rtdtest.carrier_category, status: 'AVAILABLE', empty: true)
    assert_equal @@nempty, cc.size, 'wrong number of AVAILABLE carriers'
    # reference call with A3='Y'
    assert @@rtdtest.verify_a3codes([]), 'wrong setting in ie_dispatch_config?'
    # make last carrier NOTAVAILABLE
    assert_equal 0, @@sv.carrier_status_change(cc.last, 'NOTAVAILABLE')
    assert @@rtdtest.verify_a3codes(150)
    # make all carriers NOTAVAILABLE
    assert_equal 0, @@sv.carrier_status_change(cc[0..-2], 'NOTAVAILABLE')
    assert @@rtdtest.verify_a3codes([150, 162])
    # AVAILABLE again
    assert_equal 0, @@sv.carrier_status_change(cc, 'AVAILABLE')
    assert @@rtdtest.verify_a3codes([])
    # all empty carriers are dirty
    cc.each {|c| assert age_carrier(c, @@starts3, @@lot1)}
    assert @@rtdtest.verify_a3codes(162)
    # clean up
    cc.each {|c| assert_equal 0, @@sv.carrier_usage_reset(c)}
  end

  def XXtest36_multilot
    assert @@sm.update_udata(:pd, @@op, {'AutomationType'=>'XFER'})
    assert ec = @@rtdtest.get_empty_carrier, 'no empty carrier'
    assert assert @@rtdtest.cleanup_carrier(ec)
    assert_equal 0, @@sv.carrier_usage_reset(ec)
    #
    assert_equal 0, @@sv.lot_cleanup(@@lot, opNo: @@opNo, stocker: @@rtdtest.stocker), 'error cleaning up lot'
    assert_equal 0, @@sv.eqp_cleanup(@@sorter, mode: 'Auto-3')
    assert lc = @@sv.lot_split(@@lot, 2)
    assert_equal 0, @@sv.lot_cleanup(lc, opNo: @@opNo2, stocker: @@rtdtest.stocker), 'error cleaning up lot'
    assert @@sm.update_udata(:pd, @@sv.lot_info(lc).op, {'AutomationType'=>''})
    assert @@rtdtest.verify_a3codes(5, filter: {'Lot ID'=>@@lot})
  end

  # requires special SiView setup
  def XXtest41_customer_fosb
    assert_equal 0, @@sv.eqp_cleanup(@@sorter, mode: 'Auto-3')
    # ensure there are exactly 2 EE2 carriers available and empty
    cc = @@sv.carrier_list(carrier: 'EE2%')
    cc.each {|c| assert_equal 0, @@sv.carrier_status_change(c, 'NOTAVAILABLE', check: true)}
    cc[0..1].each {|c|
      assert @@rtdtest.cleanup_carrier(c, make_empty: true)
      assert_equal 0, @@sv.user_data_delete(:carrier, c, 'MaterialType')  # start without MaterialType!
    }
    # create a test lot, customer gf, carrier EE2xx, carrier MaterialType not set, category SBMO
    assert lot = @@rtdtest.new_lot(customer: 'amd', carrier: cc[0]), "error creating test lot"
    @@testlots << lot
    assert_equal 0, @@sv.lot_opelocate(lot, @@opNoSBMO)
    li = @@sv.lot_info(lot)
    assert_equal 'SBMO', li.cast_cat, "wrong carrier category"
    assert @@sm.update_udata(:pd, li.op, {'AutomationType'=>'XFER'})
    assert @@sm.update_udata(:technology, li.technology, {'Geometry'=>''}) # TECH UDATA Geometry empty
    # call the rule, A3code 162
    assert @@rtdtest.verify_a3codes(162, parameters: ['EqpID', @@sorter, 'cast_regex', 'EE'])
    # set MaterialType and call rule again -> A3 Y (if code 150 is present the config file is wrong, must be set to 1)
    assert_equal 0, @@sv.user_data_update(:carrier, cc[1], {'MaterialType'=>'PRIME-FOSB'})
    assert @@rtdtest.verify_a3codes([], parameters: ['EqpID', @@sorter, 'cast_regex', 'EE'])
  end


  def age_carrier(carrier, starts, lot)
    @@rtdtest.cleanup_carrier(carrier, make_empty: true) || return
    @@sv.carrier_usage_reset(carrier).zero? || return
    if starts > 0
      @@sv.lot_cleanup(lot, carrier: carrier).zero? || return
      starts.times {
        @@sv.lot_opelocate(lot, :first).zero? || return
        @@sv.claim_process_lot(lot, proctime: 0) || return
      }
      @@sv.lot_opelocate(lot, :first).zero? || return
      @@sv.wafer_sort(lot, '').zero? || return
    end
    return @@sv.carrier_xfer_status_change(carrier, '', @@rtdtest.stocker).zero?
  end

  # aux methods

  def self.set_lmtoolwish(eqp, lot, op, toolwish)
    $log.info "set_lmtoolwish #{eqp.inspect}, #{lot.inspect}, #{op.inspect}, #{toolwish.inspect}"
    pclbase = LotManager::LmPclBase.new(toolwish, Time.now, 'QA RTDTest Toolwish',
      31, Time.now, 'QA RTDTest ToolScore', 4, 'MANUAL', Time.now, 'QA RTDTest Selection')
    ctx = LotManager::LmContext.new(nil, lot, op.split('.').first, eqp)
    data = LotManager::LotSummary.new(ctx, pclbase)
    return @@lmtrans.update_lotsummary(data: data)
  end

end
