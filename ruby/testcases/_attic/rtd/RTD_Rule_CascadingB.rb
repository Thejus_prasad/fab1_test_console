=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author:  sfrieske, 2017-03-27
Version: 1.0.1

History:
  2017-12-08 sfrieske, adjusted test cases
  2021-09-15 sfrieske, finally replaced $svtest by @@svtest, $sv by @@sv and $sm by @@sm
=end

require 'RubyTestCase'
require 'rtdtest'


# Batching Testcases for RTD Cascading Rule, spec C07-00003041 (ITDC)
#   eqptype 'SNK31X' -> AMHS spec (ITDC: C07-00003407), default: 7200:10:10800:1:1, Cascade spec (ITDC: C07-00003482)
class RTD_Rule_CascadingB < RubyTestCase
  @@rtd_defaults = {route: 'UTRT-CASCADING.01', product: 'UT-PRODUCT-CASCADING.01', carriers: 'EQA%',
    rule: 'QA_Cascading_test', category: 'DISPATCHER/QA', apf_delay: 15}
  # PDs with different LRs for the same set of eqps
  @@op1 = 'UTPD-CASCADINGB.01'
  @@op2 = 'UTPD-CASCADINGB.02'
  # stockers at different locations
  @@sto1 = 'UTSTO11'
  @@sto2 = 'UTSTO12'
  # eqp that is used for 2 different logical recipes, standard UT route
  @@eqp1 = 'UTICASB1'
  @@eqp2 = 'UTICASB2'
  @@eqp3 = 'UTICASB3'
  @@eqptype = 'SNK31X'  # must have an entry in cascading spec, low TCT in APF and DispatchFamily set
  @@loc1 = {'AmhsArea'=>'F36', 'Location'=>'M1-L3-MAIN'}
  @@loc2 = {'AmhsArea'=>'F38', 'Location'=>'M2-L3-MAIN'}


  def self.after_all_passed
    @@testlots.each {|lot| @@sv.delete_lot_family(lot)}
    @@sv.carrier_list(carrier: @@rtdtest.carriers, carrier_category: @@rtdtest.carrier_category).each {|c|
      @@sv.carrier_status_change(c, 'NOTAVAILABLE', check: true)
    }
  end


  def test00_setup
    $setup_ok = false
    #
    # verify correct rule will be called
    @@rtdtest = RTD::Test.new($env, @@rtd_defaults.merge(sv: @@sv))
    @@rtdtest.parameters = ['EqpID', @@eqp1, 'MinBatchSize', 1, 'MaxBatchSize', 2]
    @@rtdtest.columns = %w(PlanEqp balancing_kind transport_needed batch_id)
    assert @@rtdtest.check_rule, 'wrong RTD rule called'
    #
    # set eqp and stocker locations, remove duplicate MM UDATA
    assert @@rtdtest.stocker_location(@@sto1, @@loc1), 'error setting UDATA'
    assert @@rtdtest.stocker_location(@@sto2, @@loc2), 'error setting UDATA'
    assert @@rtdtest.eqp_location(@@eqp1, @@loc1), 'error setting UDATA'
    assert @@rtdtest.eqp_location(@@eqp2, @@loc1), 'error setting UDATA'
    assert @@rtdtest.eqp_location(@@eqp3, @@loc2), 'error setting UDATA'
    # configure and verify eqps
    assert @@rtdtest.sm.object_info(:eqptype, @@eqptype).first.udata['DispatchFamily'].end_with?('-1-'), 'missing eqptype UDATA'
    [@@eqp1, @@eqp2, @@eqp3].each {|eqp|
      assert_equal @@eqptype, @@rtdtest.sm.object_info(:eqp, eqp).first.specific[:eqptype], 'wrong eqp setup'
      assert @@rtdtest.cleanup_eqp(eqp, mode: 'Auto-3')
      assert @@rtdtest.check_eqp(eqp, nocheck: ['Location']), 'wrong eqp setup'
    }
    #
    col_lot = @@rtdtest.response.headers.find_index('Lot ID')
    @@rtdtest.response.rows.each {|row| @@sv.lot_opelocate(row[col_lot], :first)}
    #
    # remove old lots
    @@sv.carrier_list(carrier: @@rtdtest.carriers, carrier_category: @@rtdtest.carrier_category).each {|c|
      @@sv.carrier_status(c).lots.each {|lot| @@sv.delete_lot_family(lot)}
    }
    # create new test lots
    assert @@testlots = @@rtdtest.new_lots(5), 'error creating test lots'
    # lot0 for special purpose, currently not used
    #
    $setup_ok = true
  end

  def test51_1eqp_same_lr
    $setup_ok = false
    #
    # 1 eqp at loc1 (same as stocker)
    prepare_eqps([@@eqp1])
    # move lots 1 and 2 to op1 and lots 3 and 4 out of the way
    @@testlots[1..2].each {|lot| assert_equal 0, @@sv.lot_cleanup(lot, op: @@op1, stocker: @@sto1)}
    @@testlots[3..4].each {|lot| assert_equal 0, @@sv.lot_opelocate(lot, :first)}
    # call the rule and verify response
    expected = [
      [[], @@eqp1, 'Local', 'TRUE', 1],
      [[], @@eqp1, 'Local', 'TRUE', 1]
    ]
    assert @@rtdtest.verify_rows(expected), 'wrong RTD data'
    #
    $setup_ok = true
  end

  def test52_2eqp_same_lr_same_loc
    # 2 eqps at different locations
    prepare_eqps([@@eqp1, @@eqp3])
    # 4 test lots at op1, all in stocker at loc1
    @@testlots[1..4].each {|lot| assert_equal 0, @@sv.lot_cleanup(lot, op: @@op1, stocker: @@sto1)}
    expected = [
      [[], @@eqp1, 'Local', 'TRUE', 1],
      [[], @@eqp1, 'Local', 'TRUE', 1],
      [21, @@eqp3, 'Global', 'TRUE', 2],
      [21, @@eqp3, 'Global', 'TRUE', 2]
    ]
    assert @@rtdtest.verify_rows(expected), 'wrong RTD data'
  end

  def test53_2eqp_same_lr_diff_loc
    # 2 eqps at different locations
    prepare_eqps([@@eqp1, @@eqp3])
    # 4 test lots at op1, 2 in stocker at loc1, 2 in stocker at loc2
    @@testlots[1..2].each {|lot| assert_equal 0, @@sv.lot_cleanup(lot, op: @@op1, stocker: @@sto1)}
    @@testlots[3..4].each {|lot| assert_equal 0, @@sv.lot_cleanup(lot, op: @@op1, stocker: @@sto2)}
    # call the rule and verify response
    expected = [
      [[], @@eqp1, 'Local', 'TRUE', 1],
      [[], @@eqp1, 'Local', 'TRUE', 1],
      [21, @@eqp3, 'Local', 'TRUE', 2],
      [21, @@eqp3, 'Local', 'TRUE', 2],
    ]
    assert @@rtdtest.verify_rows(expected), 'wrong RTD data'
  end

  def test54_2eqp_mixed_lr_and_loc
    # 2 eqps at different locations
    prepare_eqps([@@eqp1, @@eqp3])
    # 4 test lots at [op1, op2, op2, op1], in stocker at loc1
    assert_equal 0, @@sv.lot_cleanup(@@testlots[1], op: @@op1, stocker: @@sto1)
    assert_equal 0, @@sv.lot_cleanup(@@testlots[2], op: @@op1, stocker: @@sto2)
    assert_equal 0, @@sv.lot_cleanup(@@testlots[3], op: @@op2, stocker: @@sto1)
    assert_equal 0, @@sv.lot_cleanup(@@testlots[4], op: @@op2, stocker: @@sto2)
    # call the rule and verify response
    expected = [
      [[], @@eqp1, 'Local', 'TRUE', 1],
      [21, @@eqp1, 'Local', 'TRUE', 1],
      [21, @@eqp3, 'Global', 'TRUE', 2],
      [[], @@eqp3, 'Global', 'TRUE', 2]
    ]
    assert @@rtdtest.verify_rows(expected), 'wrong RTD data'
  end

  def test55_2eqp_3lots_same
    # 2 eqps at different locations
    prepare_eqps([@@eqp1, @@eqp3])
    # 3 test lots at op1, all in stocker at loc1
    @@testlots[1..3].each {|lot| assert_equal 0, @@sv.lot_cleanup(lot, op: @@op1, stocker: @@sto1)}
    assert_equal 0, @@sv.lot_opelocate(@@testlots[4], :first)
    # call the rule and verify response
    expected = [
      [[], @@eqp1, 'Local', 'TRUE', 1],
      [[], @@eqp1, 'Local', 'TRUE', 1],
      [21, @@eqp1, 'Local', 'TRUE', 2]
    ]
    assert @@rtdtest.verify_rows(expected), 'wrong RTD data'
  end


  # aux methods

  # enable or disable all eqps in the machine recipe as required
  def prepare_eqps(eqps)
    [@@eqp1, @@eqp2, @@eqp3].each {|e|
      assert_equal 0, @@sv.eqp_cleanup(e, mode: eqps.member?(e) ? 'Auto-3' : 'Off-Line-1')
    }
  end

end
