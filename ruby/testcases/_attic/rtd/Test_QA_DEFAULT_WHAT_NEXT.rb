=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

	load_run_testcase 'Test_QA_DEFAULT_WHAT_NEXT', "itdc"

Author: Paul Peschel, 2012-07-12
Version: 1.0.0

=end

require "RubyTestCase"
require 'rtdruletest'
require 'dummytools'
require 'rtdclient'
require 'rqrsp'
require 'sjc'

# Testcases for RTD
#
# If run testcases alone over "load_run_testcase 'Test_QA_DEFAULT_WHAT_NEXT', "itdc", :tp => /20/" you must do first:
#
# 1. @@lot = ""
#
# 3. @@lots = ""
#
# 4. Call "test01_prepare_test", because without that, you have no Lot
#
# 5. Or call for each Testcase also test "test01_prepare_test" means "load_run_testcase 'Test_QA_DEFAULT_WHAT_NEXT', "itdc", :tp => /01|20/"

class Test_QA_DEFAULT_WHAT_NEXT < RubyTestCase

  @@waittime = 10

  def setup
    super
    $rtdt = RTD::RuleTest.new($env, :sv=>$sv) unless $rtdt and $rtdt.env == $env
    $rtdc = RTD::Client.new($env) unless $rtdc and $rtdc.env == $env
    $eis = Dummy::EIs.new($env)
    assert $setup_ok, "test setup is not correct"
  end

  # ###################################################### Setup des Tests ##########################################################
  #
  # Prepare all Equipment and Lots for the Test
  #
  # See also testsetup in Excel File Testsetup.xls in sheet "Testsetup_QA_DEFAULT_WHAT_NEXT".
  # Here described all important stuff for the tests in this File.
  #
  # http://myteamsdrs2/sites/FICSQA/_layouts/xlviewer.aspx?id=/sites/FICSQA/Documentation/Technical%20Documentation/RTD/Testcases_QA_DEFAULT_WHAT_NEXT.xlsx

  def test01_prepare_test
    lot_count = 1
    @@lots = $rtdt.setup_for_test(lot_count)
    @@lot = @@lots[0]
    comment = "ERPITEM=3006AA-UA0,DPML=2.0,PLANNED_FSD=#{Time.now.strftime('%Y-%m-%d')}"
    @@new_lot_params = {:product=>"RTDT-PRODUCT.01", :customer=>"gf", :route=>"P-RTDT-MAINPD01.01", :sublottype=>"PO", :comment=>comment, :stbdelay=>1}
    return @@lot + @@lots.to_s
  end

  ####################################################### Testcases for RTD #########################################################

  # *Testcase*: A3 Checks cast Available
  #
  # *Number*: 25
  #
  # *Description*: Carrier state is not available then A3Info=�FOUP-Status not available�.
  #
  # *Logic*:
  #
  # IF NULL drbl_state OR drbl_state!='AVAILABLE' THEN A3Info=�FOUP-Status not available�
  #
  # *Condition*:
  #
  # drbl_state := FRCAST_LOT.CASTMAP.drbl_state //durable state from carrier
  #
  # *Setup*:
  # * Product: RTDT-PRODUCT.01
  # * Tools: RTDT01
  # * PD: 1000.1000
  # * Scriptparameter:
  # * Carriercategory: BEOL
  # * Splitlot: No

  def test025_A3_checks_cast_available
    # Start values
    tool = $sv.lot_info(@@lot).opEqps
    status = "INUSE"

    # Test Cleanup
    assert $rtdt.eqp_cleanup(tool)
    assert $rtdt.lot_carrier_cleanup(@@lot)
    assert $rtdt.stocker_cleanup()

    # Testcase Carrier set state in Use
    li = $sv.lot_info(@@lot)
    tool = li.opEqps[0]
    assert_equal 0, $sv.carrier_status_change(li.carrier, status)
    sleep @@waittime
    list = $sv.rtd_dispatch_list(tool)
    convert_list = $rtdt.change_list_to_hash(list, @@lot)
    $result_rtd << "#{__method__}: Carrier #{li.carrier} is not found on RTD List" unless (convert_list["Lot ID"] == @@lot)
    $result_rtd << "#{__method__}: Carrier #{li.carrier} is not available and A3 is Yes" unless (convert_list["A3"] == "N")
    $result_rtd << "#{__method__}: Carrier #{li.carrier} is not available and A3 Info is '#{convert_list["A3Info"]}' instead of 'FOUP-Status not available'"  unless (convert_list["A3Info"] == "FOUP-Status not available")
  end

  # *Testcase*: A3 Checks cast unallowed (Code Nr: 6)
  #
  # *Number*: 30
  #
  # *Description*: Carrier type cannot load on equipment.
  #
  # *Logic*:
  #
  # IF FULL cast_id AND (
  # MATCH(cast_id[1,1],"[A-X]") OR cast_id[1,1]=="6" AND FULL mrecipe_auto3 AND MATCH(mrecipe_auto3, '*+A*') AND NOT MATCH(OperationReportingType, '*-A*')) THEN A3code ELSE A3Info= �unerlaubte FOUP ID�
  #
  # *Condition*:
  #
  # MATCH(cast_id[1,1],"[A-X]") // the first character of the cast_id is A to X?
  #
  # cast_id[1,1] =="6" // the first character of cast_id is a 6?
  #
  # mrecipe_auto3 := FRLRCP_DSET_RPARM{d_thesystemkey, STRING(d_seqno), "AUTO-3"}.rparm_default //here is set for example ��A� for not Auto3
  #
  # *Setup*:
  # * Product: RTDT-PRODUCT.01
  # * Tools: RTDT01
  # * PD: 1000.1000
  # * Scriptparameter:
  # * Carriercategory: BEOL
  # * Splitlot: No

  def test030_A3_checks_cast_notallowed
    # Start values
    status = "AVAILABLE"
    stocker = "UTSTO11"
    state = "MI"
    tool = $sv.lot_info(@@lot).opEqps

    # Test Cleanup
    assert $rtdt.eqp_cleanup(tool)
    assert $rtdt.lot_carrier_cleanup(@@lot)
    assert $rtdt.stocker_cleanup()

    # Testcase: Carrier with ID 6 and Auto 3
    li = $sv.lot_info(@@lot)
    tool = li.opEqps[0]
    cold = li.carrier
    cnew = $sv.carrier_list(:category=>"BEOL", :carrier =>"6%", :empty=>true).first
    csold =  $sv.carrier_status(cnew).status
    assert_equal 0, $sv.carrier_status_change(cnew, status) unless $sv.carrier_status(cnew).status == status
    assert_equal 0, $sv.wafer_sort_req(@@lot, cnew)
    assert_equal 0, $sv.carrier_xfer_status_change(cnew, state, stocker)
    sleep @@waittime
    list = $sv.rtd_dispatch_list(tool)
    convert_list = $rtdt.change_list_to_hash(list, @@lot)
    $result_rtd << "#{__method__}: Carrier #{cnew} is not found on RTD List" unless (convert_list["Lot ID"] == @@lot)
    $result_rtd << "#{__method__}: Carrier #{cnew} is cast unallowed and A3 is Yes" unless (convert_list["A3"] == "N")
    $result_rtd << "#{__method__}: Carrier #{cnew} is cast unallowed and A3 Info is '#{convert_list["A3Info"]}' instead of 'unerlaubte FOUP ID'"  unless (convert_list["A3Info"] == "unerlaubte FOUP ID")
  end

  # *Testcase*: A3 Checks single recipe (Code Nr: 5)
  #
  # *Number*: 50
  #
  # *Description*: Lots from a carrier on different PDs with different recipe.
  #
  # *Logic*:
  #
  # IF NULL mltrcp_capa OR mltrcp_capa!='Multiple Recipe' THEN IF MATCH(multi_lot_type, '*-SR') AND is_ml_foup_complete THEN A3code ELSE A3Info = �Keine ML-FOUPs erlaubt� ELSE A3code
  #
  # *Condition*:
  #
  # mltrcp_capa := FREQP.mltrcp_capa
  #
  # multi_lot_type := FRCAST_LOT.CASTMAP.multi_lot_type
  #
  # is_ml_foup_complete := IF NumberLotsOnList == number_lots_carrier AND FULL number_lots_carrier AND FULL NumberLotsOnList
  #
  # NumberLotsOnList := Count(cast_id)
  #
  # cast_id = FRCAST_LOT.CASTMAP.cast_id
  #
  # *Setup*:
  # * Product: RTDT-PRODUCT.01
  # * Tools: RTDT01, RTDT03
  # * PD: 1000.1000, 1000.1200
  # * Scriptparameter:
  # * Carriercategory: BEOL
  # * Splitlot: Yes

  def test050_A3_checks_single_recipe
    # Start values
    opNo = "1000.1200"
    tool = $sv.lot_info(@@lot).opEqps

    # Test Cleanup
    assert $rtdt.eqp_cleanup(tool)
    assert $rtdt.lot_carrier_cleanup(@@lot)
    assert $rtdt.stocker_cleanup()

    # Check A3 State Lot Split and after that locate split lot on Operation 1000.1200. Check Both Tools with Lot and Split Lot
    stool = $sv.lot_info(@@lot).opEqps[0]
    mtool = $sv.lot_info(@@lot).opEqps[2]
    cl = $sv.lot_split(@@lot, 10)
    assert_equal 0, $sv.lot_opelocate(cl, opNo)

    # Testcase 1: With Lot1 and Tool Single Recipe
    sleep @@waittime
    list = $sv.rtd_dispatch_list(stool)
    convert_list = $rtdt.change_list_to_hash(list, @@lot)
    $result_rtd << "#{__method__}_1: Lot #{@@lot} is on Single Recipe Tool #{stool} and not found on RTD List" unless (convert_list["Lot ID"] == @@lot)
    $result_rtd << "#{__method__}_1: Lot #{@@lot} is on Single Recipe Tool #{stool} and A3 is Yes" unless (convert_list["A3"] == "N")
    $result_rtd << "#{__method__}_1: Lot #{@@lot} is on Single Recipe Tool #{stool} and A3 Info is #{convert_list["A3Info"]} instead of 'Keine ML-FOUPs erlaubt'"  unless (convert_list["A3Info"] == "Keine ML-FOUPs erlaubt")

    # Testcase 2: With Lot2 and Tool Single Recipe
    sleep @@waittime
    list = $sv.rtd_dispatch_list(stool)
    convert_list = $rtdt.change_list_to_hash(list, cl)
    $result_rtd << "#{__method__}_2: Split Lot #{cl} is on Single Recipe Tool #{stool} and not found on RTD List" unless (convert_list["Lot ID"] == cl)
    $result_rtd << "#{__method__}_2: Split Lot #{cl} is on Single Recipe Tool #{stool} and A3 is Yes" unless (convert_list["A3"] == "N")
    $result_rtd << "#{__method__}_2: Split Lot #{cl} is on Single Recipe Tool #{stool} and A3 Info is '#{convert_list["A3Info"]}' instead of 'Keine ML-FOUPs erlaubt'"  unless (convert_list["A3Info"] == "Keine ML-FOUPs erlaubt")

    # Testcase 3: With Lot1 and Tool Multi Recipe
    sleep @@waittime
    list = $sv.rtd_dispatch_list(mtool)
    convert_list = $rtdt.change_list_to_hash(list, @@lot)
    $result_rtd << "#{__method__}_3: Lot #{@@lot} is on Multi Recipe Tool #{mtool} and not found on RTD List" unless (convert_list["Lot ID"] == @@lot)
    $result_rtd << "#{__method__}_3: Lot #{@@lot} is on Multi Recipe Tool #{mtool} and A3 is No" unless (convert_list["A3"] == "Y")

    # Testcase 4: With Lot2 and Tool Multi Recipe
    sleep @@waittime
    list = $sv.rtd_dispatch_list(mtool)
    convert_list = $rtdt.change_list_to_hash(list, cl)
    $result_rtd << "#{__method__}_4: Split Lot #{cl} is on Multi Recipe Tool #{mtool} and not found on RTD List" unless (convert_list["Lot ID"] == cl)
    $result_rtd << "#{__method__}_4: Split Lot #{cl} is on Multi Recipe Tool #{mtool} and A3 is No" unless (convert_list["A3"] == "Y")
  end

  # *Testcase*: A3 Checks carrier reachable 1
  #
  # *Number*: 55
  #
  # *Description*: Block Lots with state E0 and not VAMOS or the carrier is locate in BTF (SiView SuperAreaID == "BTF").
  #
  # *Logic*:
  #
  # A3 blocked:
  #
  # IF trans_state == "EO" AND (NOT vamos OR eqp_superarea_id != "BTF")
  #
  # *Condition*:
  #
  # Scriptparemter on Equipment vamos:= PPTYSTRING_altKey_d_objmanager_pptystring_id{property_obj_eqp, �Pull�}.PPTYSTRING_ptr.value
  #
  # vamos: (MATCH(vamos, "*" + port_id + "*")) AND ((mcopemode_id == "Auto-1") OR (mcopemode_id == "Semi-Start-1"))
  #
  # trans_state := FRCAST.trans_state eqp_superarea_id := FREQP.eqp_superarea_id
  #
  # vamos:= PPTYSTRING_altKey_d_objmanager_pptystring_id{property_obj_eqp, �Push�}.PPTYSTRING_ptr.value
  #
  # DISP_PUSH_FG := MATCH(Push, "*"+load_port_id+"*")
  #
  # job_id := FXTRNREQ.job_id for the carrier
  #
  # *Setup*:
  # * Product: RTDT-PRODUCT.01
  # * Tools: RTDT01, RTDT02, RTDT03, RTDT04, RTDT05
  # * PD: 1000.1000
  # * Scriptparameter:
  # * Carriercategory: BEOL
  # * Splitlot: No

  def test055_A3_checks_carrier_reachable_1
    # Start values
    state = ["MO", "MI"]
    stocker = ["UTSTO11", "UTSTOBTF"] # Location UData = [M1, BTF]
    tool = $sv.lot_info(@@lot).opEqps

    # Test Cleanup
    assert $rtdt.eqp_cleanup(tool)
    assert $rtdt.lot_carrier_cleanup(@@lot)
    assert $rtdt.stocker_cleanup()

    # Testcase for reachable Block Lot with sate MO and not VAMOS or Carrier in BTF (SiView SuperAreaID == "BTF")
    li = $sv.lot_info(@@lot)
    tool = li.opEqps
    toolm1 = tool[0..2] # Location UData = [M1]
    toolbtf = tool[3..4] # Location UData = [BTF]

    # Testcase 1: #xfer = MI, Location: UData = M1
    assert_equal 0, $sv.carrier_xfer_status_change(li.carrier, state[1], stocker[0])
    sleep @@waittime
    tool.each {|t|
      list = $sv.rtd_dispatch_list(t)
      convert_list = $rtdt.change_list_to_hash(list, @@lot)
      #$result_rtd << "#{__method__}_1: Lot #{@@lot} is not found in Location (UTSTO11), actual Location is #{t}" unless (convert_list["Lot ID"] == @@lot) == toolm1.member?(t)
      $result_rtd << "#{__method__}_1: Lot #{@@lot} is not found with State A3 = Yes, actual Location is #{t}" unless (convert_list["A3"] == "Y") == toolm1.member?(t)
      $result_rtd << "#{__method__}_1: Lot #{@@lot} is not found with State A3 = NO, actual Location is #{t}" unless (convert_list["A3"] == "N") == toolbtf.member?(t)
      $result_rtd << "#{__method__}_1: Lot #{@@lot} A3 Info is '#{convert_list["A3Info"]}' instead of 'Carrier at other location'" unless (convert_list["A3Info"].split('|').member?("Carrier at other location")) == toolbtf.member?(t)}

    # Testcase 2: #xfer = MO, Location: UData = M1
    assert_equal 0, $sv.carrier_xfer_status_change(li.carrier, state[0], stocker[0])
    sleep @@waittime
    tool.each {|t|
      list = $sv.rtd_dispatch_list(t)
      convert_list = $rtdt.change_list_to_hash(list, @@lot)
      $result_rtd << "#{__method__}_2: Tool #{t} is set to A3 = Yes" if (convert_list["Lot ID"] == @@lot) && (convert_list["A3"] == "Y")}

    # Testcase 3: #xfer = MI, Location: UData = BTF
    assert_equal 0, $sv.carrier_xfer_status_change(li.carrier, state[1], stocker[1])
    sleep @@waittime
    tool.each {|t|
      list = $sv.rtd_dispatch_list(t)
      convert_list = $rtdt.change_list_to_hash(list, @@lot)
      #$result_rtd << "#{__method__}_3: Lot #{@@lot} is not found in Location (UTSTOBTF), actual Location is #{t}" unless (convert_list["Lot ID"] == @@lot) == toolbtf.member?(t)
      $result_rtd << "#{__method__}_3: Lot #{@@lot} is not found with State A3 = Yes, actual Location is #{t}" unless (convert_list["A3"] == "Y") == toolbtf.member?(t)
      $result_rtd << "#{__method__}_3: Lot #{@@lot} is not found with State A3 = NO, actual Location is #{t}" unless (convert_list["A3"] == "N") == toolm1.member?(t)
      $result_rtd << "#{__method__}_3: Lot #{@@lot} A3 Info is '#{convert_list["A3Info"]}' instead of 'Carrier at other location'" unless (convert_list["A3Info"].split('|').member?("Carrier at other location")) == toolm1.member?(t)}

    # Testcase 4: #xfer = MO, Location: UData = BTF
    assert_equal 0, $sv.carrier_xfer_status_change(li.carrier, state[0], stocker[1])
    sleep @@waittime
    tool.each {|t|
      $sv.eqp_mode_auto1(t, :notifyTCS=>false)
      sleep @@waittime
      list = $sv.rtd_dispatch_list(t)
      convert_list = $rtdt.change_list_to_hash(list, @@lot)
      #$result_rtd << "#{__method__}_4: Lot #{@@lot} is not found in Location (UTSTOBTF), actual Location is #{t}" unless (convert_list["Lot ID"] == @@lot) == toolbtf.member?(t)
      $result_rtd << "#{__method__}_4_1: Lot #{@@lot} is not found with State A3 = Yes, actual Location is #{t}" unless (convert_list["A3"] == "Y") == toolbtf.member?(t)
      $result_rtd << "#{__method__}_4_1: Lot #{@@lot} is not found with State A3 = NO, actual Location is #{t}" unless (convert_list["A3"] == "N") == toolm1.member?(t)
      $result_rtd << "#{__method__}_4_1: Lot #{@@lot} A3 Info is '#{convert_list["A3Info"]}' instead of 'Carrier at other location'" unless (convert_list["A3Info"].split('|').member?("Carrier at other location")) == toolm1.member?(t)
      assert_equal 0, $sv.eqp_mode_auto2(t, :notifyTCS=>false)
      cj = $sv.lot_info(@@lot).cj
      assert_equal 0, $sv.slr_cancel(t,cj) unless cj==""
    }
    tool.each {|t|
      assert_equal 0, $sv.eqp_mode_change(t, "Semi-Start-1", :notifyTCS=>false)
      sleep @@waittime
      list = $sv.rtd_dispatch_list(t)
      convert_list = $rtdt.change_list_to_hash(list, @@lot)
      #$result_rtd << "#{__method__}_4: Lot #{@@lot} is not found in Location (UTSTOBTF), actual Location is #{t}" unless (convert_list["Lot ID"] == @@lot) == toolbtf.member?(t)
      $result_rtd << "#{__method__}_4_2: Lot #{@@lot} is not found with State A3 = Yes, actual Location is #{t}" unless (convert_list["A3"] == "Y") == toolbtf.member?(t)
      $result_rtd << "#{__method__}_4_2: Lot #{@@lot} is not found with State A3 = NO, actual Location is #{t}" unless (convert_list["A3"] == "N") == toolm1.member?(t)
      $result_rtd << "#{__method__}_4_2: Lot #{@@lot} A3 Info is '#{convert_list["A3Info"]}' instead of 'Carrier at other location'" unless (convert_list["A3Info"].split('|').member?("Carrier at other location")) == toolm1.member?(t)
      cj = $sv.lot_info(@@lot).cj
      assert_equal 0, $sv.slr_cancel(t,cj) unless cj==""
      assert_equal 0, $sv.eqp_mode_auto2(t, :notifyTCS=>false)
    }
  end

  # *Testcase*: A3 Checks carrier reachable 2
  #
  # *Number*: 60
  #
  # *Description*: Is dispatching in dependency from port mode, from  calling instance (Push, Pull, etc) and the transportstate from the carrier.
  #
  # *Logic*:
  #
  # A3 blocked:
  #
  # IF trans_state == "EI" AND NOT ((vamos AND DISP_PUSH_FG == "Y" AND (mcopemode_id=='Auto-1' OR mcopemode_id=='Semi-Start-1')) OR
  #
  # (eqp_to_eqp_trns == 1  AND $requestingEntity == "AutoPush" AND MATCH(mcopemode_id,"*-[23]")))
  #
  # *Condition*:
  #
  # Scriptparemter on Equipment vamos := PPTYSTRING_altKey_d_objmanager_pptystring_id{property_obj_eqp, �Pull�}.PPTYSTRING_ptr.value
  #
  # vamos := (MATCH(vamos, "*" + port_id + "*")) AND ((mcopemode_id == "Auto-1") OR (mcopemode_id == "Semi-Start-1"))
  #
  # trans_state := FRCAST.trans_state eqp_superarea_id := FREQP.eqp_superarea_id
  #
  # vamos := PPTYSTRING_altKey_d_objmanager_pptystring_id{property_obj_eqp, �Push�}.PPTYSTRING_ptr.value
  #
  # DISP_PUSH_FG := MATCH(Push, "*"+load_port_id+"*")
  #
  # job_id := FXTRNREQ.job_id for the carrier
  #
  # *Setup*:
  # * Product: RTDT-PRODUCT.01
  # * Tools: RTDT01, RTDT02, RTDT03, RTDT04, RTDT05
  # * PD: 1000.1000
  # * Scriptparameter:
  # * Carriercategory: BEOL
  # * Splitlot: No

  def test060_A3_checks_carrier_reachable_2
    # Start values
    port = "P1"
    state = "LoadComp"
    stateold = "LoadReq"
    stateunload = "UnloadReq"
    tool = $sv.lot_info(@@lot).opEqps

    # Test Cleanup
    assert $rtdt.eqp_cleanup(tool)
    assert $rtdt.lot_carrier_cleanup(@@lot)
    assert $rtdt.stocker_cleanup()

    # Testcase for A3 geblocked: IF trans_state == "EI" AND NOT ((vamos AND DISP_PUSH_FG == "Y" AND (mcopemode_id=='Auto-1' OR mcopemode_id=='Semi-Start-1')) OR (eqp_to_eqp_trns == 1  AND $requestingEntity == "AutoPush" AND MATCH(mcopemode_id,"*-[23]")))
    li = $sv.lot_info(@@lot)
    tool = li.opEqps
    # Testcase 1: Carrier on Tool
    assert_equal 0, $sv.carrier_xfer_status_change(li.carrier, "MO", "UTSTO11")
    tool.each {|t|
      assert_equal 0, $sv.eqp_mode_offline1(t, :notifyTCS=>false)
      assert_equal 0, $sv.eqp_load(t, port, li.carrier)
      sleep @@waittime
      list = $sv.rtd_dispatch_list(t)
      convert_list = $rtdt.change_list_to_hash(list, @@lot)
      $result_rtd << "#{__method__}_1: Carrier #{li.carrier} is on Tool #{t} and not found on RTD List" unless (convert_list["Lot ID"] == @@lot)
      $result_rtd << "#{__method__}_1: Carrier #{li.carrier} is on Tool #{t} and blocked and A3 is Yes" unless (convert_list["A3"] == "N")
      $result_rtd << "#{__method__}_1: Carrier #{li.carrier} is not on Tool #{t}" unless (convert_list["Carrier Xfer Status"] == "EI")
      assert_equal 0, $sv.eqp_unload(t, port, li.carrier)
      assert_equal 0, $sv.eqp_mode_auto2(t, :notifyTCS=>false)}
    $sv.carrier_xfer_status_change(li.carrier, "MI", "UTSTO11")

    # Testcase 2: Carrier on Tool and Auto_1
    tool = tool[0]
    assert_equal 0, $sv.carrier_xfer_status_change(li.carrier, "MI", "UTSTO11")
    cj = $sv.slr(tool, :port=>port, :lots=>li.lot)
    assert_equal 0, $sv.eqp_port_status_change(tool, port, state)
    assert_equal 0, $sv.carrier_xfer_status_change(li.carrier, "MO", "UTSTO11")
    assert_equal 0, $sv.eqp_load(tool, port, li.carrier)
    assert_equal 0, $sv.eqp_mode_auto1(tool, :notifyTCS=>false)
    assert_equal 0, $sv.slr_cancel(tool, cj)
    sleep @@waittime
    list = $sv.rtd_dispatch_list(tool)
    convert_list = $rtdt.change_list_to_hash(list, @@lot)
    $result_rtd << "#{__method__}_2: Carrier #{li.carrier} is on Tool #{tool} and not found on RTD List" unless (convert_list["Lot ID"] == @@lot)
    $result_rtd << "#{__method__}_2: Carrier #{li.carrier} is on Tool #{tool} and A3 is No" unless (convert_list["A3"] == "Y")
    $result_rtd << "#{__method__}_2: Carrier #{li.carrier} is not on Tool #{tool}" unless (convert_list["Carrier Xfer Status"] == "EI")

    # Testcase 3: Carrier on Tool and state Auto_3
    assert_equal 0, $sv.eqp_mode_auto3(tool, :notifyTCS=>false)
    assert_equal 0, $sv.eqp_port_status_change(tool, port, stateunload)
    sleep @@waittime
    list = $sv.rtd_dispatch_list(tool, function="WhatNextLotList", :parameters => "requestingEntity|AutoPush")
    convert_list = $rtdt.change_list_to_hash(list, @@lot)
    $result_rtd << "#{__method__}_3: Carrier #{li.carrier} is on Tool #{tool} and not found on RTD List" unless (convert_list["Lot ID"] == @@lot)
    $result_rtd << "#{__method__}_3: Carrier #{li.carrier} is on Tool #{tool} and A3 is No" unless (convert_list["A3"] == "Y")
    $result_rtd << "#{__method__}_3: Carrier #{li.carrier} is not on Tool #{tool}" unless (convert_list["Carrier Xfer Status"] == "EI")

    # Testcase 4: Carrier not on Tool and Stat SI
    assert_equal 0, $sv.eqp_unload(tool, port, li.carrier)
    assert_equal 0, $sv.eqp_mode_auto2(tool, :notifyTCS=>false)
    assert_equal 0, $sv.eqp_port_status_change(tool, port, stateold)
    assert_equal 0, $sv.carrier_xfer_status_change(li.carrier, "SI", "UTSTO11")
    sleep @@waittime
    list = $sv.rtd_dispatch_list(tool)
    convert_list = $rtdt.change_list_to_hash(list, @@lot)
    $result_rtd << "#{__method__}_4: Carrier #{li.carrier} is aviable for Tool #{tool} and not found on RTD List" unless (convert_list["Lot ID"] == @@lot)
    $result_rtd << "#{__method__}_4: Carrier #{li.carrier} is aviable for Tool #{tool} and A3 is No" unless (convert_list["A3"] == "Y")
    $result_rtd << "#{__method__}_4: Carrier #{li.carrier} is not state SI" unless (convert_list["Carrier Xfer Status"] == "SI")
  end

  # *Testcase*: A3 Checks carrier reachable 3
  #
  # *Number*: 65
  #
  # *Description*: VAMOS versus transportstate from carriers.
  #
  # *Logic*:
  #
  # A3 geblocked:
  #
  # IF ((trans_state == "HO" OR trans_state == "MO") AND (NOT vamos OR eqp_superarea_id != "BTF")) OR trans_state == "HI" OR (trans_state == "SO" AND NULL job_id) OR trans_state == "AO"
  #
  # *Condition*:
  #
  # Scriptparemter on Equipment vamos:= PPTYSTRING_altKey_d_objmanager_pptystring_id{property_obj_eqp, �Pull�}.PPTYSTRING_ptr.value
  #
  # vamos := (MATCH(vamos, "*" + port_id + "*")) AND ((mcopemode_id == "Auto-1") OR (mcopemode_id == "Semi-Start-1"))
  #
  # trans_state := FRCAST.trans_state eqp_superarea_id := FREQP.eqp_superarea_id
  #
  # vamos := PPTYSTRING_altKey_d_objmanager_pptystring_id{property_obj_eqp, �Push�}.PPTYSTRING_ptr.value
  #
  # DISP_PUSH_FG := MATCH(Push, "*"+load_port_id+"*")
  #
  # job_id := FXTRNREQ.job_id for the carrier
  #
  # *Setup*:
  # * Product: RTDT-PRODUCT.01
  # * Tools: RTDT02, RTDT04
  # * PD: 1000.1000
  # * Scriptparameter:
  # * Carriercategory: BEOL
  # * Splitlot: No

  def test065_A3_checks_carrier_reachable_3
    # Start values
    state = ["MO", "HO", "HI", "SO", "AO"] # MO und AO on UTST011 and UTSTOBTF, HO and HI on SHF102, SO on INTER1
    stocker = ["UTSTO11", "UTSTOBTF", "SHFM1", "SHFBTF", "UTINTER1"] # Location UData = [M1, BTF]
    tool = $sv.lot_info(@@lot).opEqps

    # Test Cleanup
    assert $rtdt.eqp_cleanup(tool)
    assert $rtdt.lot_carrier_cleanup(@@lot)
    assert $rtdt.stocker_cleanup()

    # Testcase VAMOS versus Transportstate from Carriers MO and HO on location M1
    li = $sv.lot_info(@@lot)
    tool = li.opEqps
    toolm1 = tool[1] # Location UData = [M1], tool RTDT02
    toolbtf = tool[3] # Location UData = [BTF], tool RTDT04, QA_DEFAULT_WHAT_NEXT_cluster

    # Testcase 1: #xfer = MO, Location: UData = M1
    assert_equal 0,  $sv.carrier_xfer_status_change(li.carrier, state[0], stocker[0])
    sleep @@waittime
    list = $sv.rtd_dispatch_list(toolm1)
    convert_list = $rtdt.change_list_to_hash(list, @@lot)
    $result_rtd << "#{__method__}_1: Lot #{@@lot} is not found in Location #{stocker[0]} and state #{state[0]}" unless (convert_list["Lot ID"] == @@lot)
    $result_rtd << "#{__method__}_1: Lot #{@@lot} is found in Location #{stocker[0]} and state #{state[0]} with State A3 = Yes" unless (convert_list["A3"] == "N")

    # Testcase 2: #xfer = HO, Location: UData = M1
    assert_equal 0, $sv.carrier_xfer_status_change(li.carrier, state[1], stocker[2])
    sleep @@waittime
    list = $sv.rtd_dispatch_list(toolm1)
    convert_list = $rtdt.change_list_to_hash(list, @@lot)
    $result_rtd << "#{__method__}_2: Lot #{@@lot} is not found in Location #{stocker[2]} and state #{state[1]}" unless (convert_list["Lot ID"] == @@lot)
    $result_rtd << "#{__method__}_2: Lot #{@@lot} is found in Location #{stocker[2]} and state #{state[1]} with State A3 = Yes" unless (convert_list["A3"] == "N")

    # Testcase VAMOS versus Transportstate from Carriers HI, SO and AO on location M1
    # Testcase 3: #xfer = HI, Location: UData = M1
    assert_equal 0, $sv.carrier_xfer_status_change(li.carrier, state[2], stocker[2])
    sleep @@waittime
    list = $sv.rtd_dispatch_list(toolm1)
    convert_list = $rtdt.change_list_to_hash(list, @@lot)
    $result_rtd << "#{__method__}_3: Lot #{@@lot} is not found in Location #{stocker[2]} and state #{state[2]}" unless (convert_list["Lot ID"] == @@lot)
    $result_rtd << "#{__method__}_3: Lot #{@@lot} is found in Location #{stocker[2]} and state #{state[2]} with State A3 = Yes" unless (convert_list["A3"] == "N")

    # Testcase 4: #xfer = SO, Location: UData = M1
    assert_equal 0, $sv.carrier_xfer_status_change(li.carrier, state[3], stocker[4])
    sleep @@waittime
    list = $sv.rtd_dispatch_list(toolm1)
    convert_list = $rtdt.change_list_to_hash(list, @@lot)
    $result_rtd << "#{__method__}_4: Lot #{@@lot} is not found in Location #{stocker[4]} and state #{state[3]}" unless (convert_list["Lot ID"] == @@lot)
    $result_rtd << "#{__method__}_4: Lot #{@@lot} is found in Location #{stocker[4]} and state #{state[3]} with State A3 = Yes" unless (convert_list["A3"] == "N")

    # Testcase 5: #xfer = AO, Location: UData = M1
    assert_equal 0, $sv.carrier_xfer_status_change(li.carrier, state[4], stocker[0])
    sleep @@waittime
    list = $sv.rtd_dispatch_list(toolm1)
    convert_list = $rtdt.change_list_to_hash(list, @@lot)
    $result_rtd << "#{__method__}_5: Lot #{@@lot} is not found in Location #{stocker[0]} and state #{state[4]}" unless (convert_list["Lot ID"] == @@lot)
    $result_rtd << "#{__method__}_5: Lot #{@@lot} is found in Location #{stocker[0]} and state #{state[4]} with State A3 = Yes" unless (convert_list["A3"] == "N")

    # Testcase VAMOS versus Transportstate from Carriers  MI, SO and AO on location BTF
    # Testcase 6: #xfer = HI, Location: UData = BTF
    assert_equal 0, $sv.carrier_xfer_status_change(li.carrier, state[2], stocker[3])
    sleep @@waittime
    list = $sv.rtd_dispatch_list(toolbtf)
    convert_list = $rtdt.change_list_to_hash(list, @@lot)
    $result_rtd << "#{__method__}_6: Lot #{@@lot} is not found in Location #{stocker[3]} and state #{state[2]}" unless (convert_list["Lot ID"] == @@lot)
    $result_rtd << "#{__method__}_6: Lot #{@@lot} is found in Location #{stocker[3]} and state #{state[2]} with State A3 = Yes" unless (convert_list["A3"] == "N")

    # Testcase 7: #xfer = SO, Location: UData = BTF
    assert_equal 0, $sv.carrier_xfer_status_change(li.carrier, state[3], stocker[4])
    sleep @@waittime
    list = $sv.rtd_dispatch_list(toolbtf)
    convert_list = $rtdt.change_list_to_hash(list, @@lot)
    #$result_rtd << "#{__method__}_7: Lot #{@@lot} is found in Location #{stocker[4]} and state #{state[3]}" if (convert_list["Lot ID"] == @@lot)
    $result_rtd << "#{__method__}_7: Lot #{@@lot} is found in Location #{stocker[4]} and state #{state[3]} with State A3 = Yes" unless (convert_list["A3"] == "N")

    # Testcase 8: #xfer = AO, Location: UData = BTF
    assert_equal 0, $sv.carrier_xfer_status_change(li.carrier, state[4], stocker[1]) #xfer = AO, Location: UData = BTF
    sleep @@waittime
    list = $sv.rtd_dispatch_list(toolbtf)
    convert_list = $rtdt.change_list_to_hash(list, @@lot)
    $result_rtd << "#{__method__}_8: Lot #{@@lot} is not found in Location #{stocker[1]} and state #{state[4]}" unless (convert_list["Lot ID"] == @@lot)
    $result_rtd << "#{__method__}_8: Lot #{@@lot} is found in Location #{stocker[1]} and state #{state[4]} with State A3 = Yes" unless (convert_list["A3"] == "N")
  end

  # *Testcase*: A3 Checks carrier reservation (Code Nr: 3)
  #
  # *Number*: 70
  #
  # *Description*: Carrier has a reservation and is not allowed to dispatch (reservation column).
  #
  # *Logic*:
  #
  # IF FULL Reservation THEN A3code=� FOUP schon reserviert�
  #
  # *Condition*:
  #
  # Reservation: IF FULL resrv_user_id THEN "T:" + resrv_user_id
  #
  # ELSE IF FULL proc_resrv_user_id AND FULL ctrljob_id THEN "P:" + proc_resrv_user_id + "-" + ctrljob_eqp
  #
  # ELSE IF FULL proc_resrv_user_id AND NULL ctrljob_id THEN "F:" + proc_resrv_user_id + "-" + ctrljob_eqp ELSE ""
  #
  # resrv_user_id := FRCAST_LOT.resrv_user_id
  #
  # proc_resrv_user_id := IF is_CastMap THEN CastMap->CtrlJobMap->owner_id ELSE ""
  #
  # is_CastMap := (NOT NULL CastMap->CtrlJobMap->owner_id) AND (CastMap->CtrlJobMap->reserved_flag == 1)
  #
  # FRCAST_LOT.CastMap->CtrlJobMap->owner_id
  #
  # FRCAST_LOT.CastMap->CtrlJobMap->reserved_flag
  #
  # *Setup*:
  # * Product: RTDT-PRODUCT.01
  # * Tools: RTDT01
  # * PD: 1000.1000
  # * Scriptparameter:
  # * Carriercategory: BEOL
  # * Splitlot: No

  def test070_A3_checks_carrier_reservation
    # Start values
    port = "P1"
    tool = $sv.lot_info(@@lot).opEqps

    # Test Cleanup
    assert $rtdt.eqp_cleanup(tool)
    assert $rtdt.lot_carrier_cleanup(@@lot)
    assert $rtdt.stocker_cleanup()

    # Check check empty carrier if carrier has a reservation
    li = $sv.lot_info(@@lot)
    tool = li.opEqps[0]
    sleep @@waittime
    list = $sv.rtd_dispatch_list(tool)
    convert_list = $rtdt.change_list_to_hash(list, @@lot)
    $result_rtd << "#{__method__}: Carrier  #{li.carrier} is reserved" unless (convert_list["Lot ID"] == @@lot) && (convert_list["A3"] == "Y")

    # Testcase 1: Carrier has a reservation and it is not allowed to dispatch (Reservation Column)
    assert_equal 0, $sv.carrier_reserve(li.carrier)
    sleep @@waittime
    list = $sv.rtd_dispatch_list(tool)
    convert_list = $rtdt.change_list_to_hash(list, @@lot)
    $result_rtd << "#{__method__}_1: Carrier #{li.carrier} is reserved and not found on RTD List" unless (convert_list["Lot ID"] == @@lot)
    $result_rtd << "#{__method__}_1: Carrier #{li.carrier} is reserved and A3 is Yes " unless (convert_list["A3"] == "N")
    $result_rtd << "#{__method__}_1: Carrier #{li.carrier} is reserved and A3 Info is '#{convert_list["A3Info"]}' instead of 'FOUP schon reserviert'" unless (convert_list["A3Info"] == "FOUP schon reserviert")
    assert $rtdt.prepare_carrier(@@lot)

    # Testcase 2: SLR
    assert_equal 0, $sv.eqp_mode_offline1(tool, :notifyTCS=>false)
    cj = $sv.slr(tool, :port=>port, :lots=>li.lot)
    sleep @@waittime
    list = $sv.rtd_dispatch_list(tool)
    convert_list = $rtdt.change_list_to_hash(list, @@lot)
    $result_rtd << "#{__method__}_2: Carrier #{li.carrier} is reserved and not found on RTD List" unless (convert_list["Lot ID"] == @@lot)
    $result_rtd << "#{__method__}_2: Carrier #{li.carrier} is reserved and A3 is Yes " unless (convert_list["A3"] == "N")
    $result_rtd << "#{__method__}_2: Carrier #{li.carrier} is reserved and A3 Info is '#{convert_list["A3Info"]}' instead of 'FOUP schon reserviert|Controljob schon vorhanden'" unless (convert_list["A3Info"] == "FOUP schon reserviert|Controljob schon vorhanden")
    assert_equal 0, $sv.slr_cancel(tool, cj)
    assert_equal 0, $sv.eqp_mode_auto2(tool, :notifyTCS=>false)

    # Testcase 3 Check A3 = Y without reservation
    sleep @@waittime
    list = $sv.rtd_dispatch_list(tool)
    convert_list = $rtdt.change_list_to_hash(list, @@lot)
    $result_rtd << "#{__method__}_3: Lot #{@@lot} is not found on RTD List" unless (convert_list["Lot ID"] == @@lot)
    $result_rtd << "#{__method__}_3: A3 is No but should be Yes" unless (convert_list["A3"] == "Y")
  end

  # *Testcase*: A3 Checks carrier controljobs (Code Nr: 4)
  #
  # *Number*: 75
  #
  # *Description*: Carrier has a reservation and it is not allowed to dispatch (count ContrJobs from carrier).
  #
  # *Logic*:
  #
  # IF is_CastMap THEN CastMap->CtrlJobMap->owner_id ELSE ""
  #
  # is_CastMap :=
  # (NOT NULL CastMap->CtrlJobMap->owner_id) AND
  # (CastMap->CtrlJobMap->reserved_flag == 1)
  #
  # *Condition*:
  #
  # resrv_user_id :=FRCAST_LOT.resrv_user_id
  #
  # proc_resrv_user_id:=
  #
  # FRCAST_LOT.CastMap->CtrlJobMap->owner_id
  #
  # FRCAST_LOT.CastMap->CtrlJobMap->reserved_flag
  #
  # IF FULL number_ctrljobs_carrier AND number_ctrljobs_carrier>0 THEN A3Info = �Controljob schon vorhanden�
  #
  # number_ctrljobs_carrier: Sum from num_ctrljobs //sum from the num_ctrljobs
  #
  # num_ctrljobs := IF NULL ctrljob_id THEN 0 ELSE 1 //count the ctrljob_ids
  #
  # ctrljob_id := FRCAST_LOT.LotMap->ctrljob_id
  #
  # *Setup*:
  # * Product: RTDT-PRODUCT.01
  # * Tools: RTDT01
  # * PD: 1000.1000
  # * Scriptparameter:
  # * Carriercategory: BEOL
  # * Splitlot: Yes

  def test075_A3_checks_carrier_controljobs
    # Start values
    port = "P1"
    tool = $sv.lot_info(@@lot).opEqps

    # Test Cleanup
    assert $rtdt.eqp_cleanup(tool)
    assert $rtdt.lot_carrier_cleanup(@@lot)
    assert $rtdt.stocker_cleanup()

    # Testcase 1: Check empty carrier if carrier has a reservation
    li = $sv.lot_info(@@lot)
    tool = li.opEqps[0]
    sleep @@waittime
    list = $sv.rtd_dispatch_list(tool)
    convert_list = $rtdt.change_list_to_hash(list, @@lot)
    $result_rtd << "#{__method__}_1: Carrier #{li.carrier} has no reservation, #{li.carrier}" unless (convert_list["Lot ID"] == @@lot) && (convert_list["A3"] == "Y")

    # Testcase 2: Carrier has a reservation and it is not allowed to Dispatch, also not the split Lot (counts ContrJobs on Carrier)
    cl = $sv.lot_split(@@lot, 10)
    assert_equal 0, $sv.carrier_reserve(li.carrier)
    sleep @@waittime
    list = $sv.rtd_dispatch_list(tool)
    convert_list = $rtdt.change_list_to_hash(list, @@lot)
    $result_rtd << "#{__method__}_2: Carrier #{li.carrier} is reserved and not found on RTD List #{@@lot}" unless (convert_list["Lot ID"] == @@lot)
    $result_rtd << "#{__method__}_2: Carrier #{li.carrier} is reserved and A3 is Yes #{@@lot}" unless (convert_list["A3"] == "N")
    $result_rtd << "#{__method__}_2: Carrier #{li.carrier} is reserved and A3 Info is '#{convert_list["A3Info"]}' instead of 'FOUP schon reserviert' #{@@lot}" unless (convert_list["A3Info"] == "FOUP schon reserviert")
    convert_list = $rtdt.change_list_to_hash(list, cl)
    $result_rtd << "#{__method__}_2: Carrier #{li.carrier} is reserved and not found on RTD List #{cl}" unless (convert_list["Lot ID"] == cl)
    $result_rtd << "#{__method__}_2: Carrier #{li.carrier} is reserved and A3 is Yes #{cl}" unless (convert_list["A3"] == "N")
    $result_rtd << "#{__method__}_2: Carrier #{li.carrier} is reserved and A3 Info is '#{convert_list["A3Info"]}' instead of 'FOUP schon reserviert' #{cl}" unless (convert_list["A3Info"] == "FOUP schon reserviert")
    assert $rtdt.prepare_carrier(@@lot)

    # Testcase 3: SLR
    assert_equal 0, $sv.eqp_mode_offline1(tool, :notifyTCS=>false)
    cj = $sv.slr(tool, :port=>port, :lots=>@@lot)
    sleep @@waittime
    list = $sv.rtd_dispatch_list(tool)
    convert_list = $rtdt.change_list_to_hash(list, @@lot)
    $result_rtd << "#{__method__}_3: Carrier #{li.carrier} is reserved and not found on RTD List" unless (convert_list["Lot ID"] == @@lot)
    $result_rtd << "#{__method__}_3: Carrier #{li.carrier} is reserved and A3 is Yes " unless (convert_list["A3"] == "N")
    $result_rtd << "#{__method__}_3: Carrier #{li.carrier} is reserved and A3 Info is '#{convert_list["A3Info"]}' instead of 'FOUP schon reserviert|Controljob schon vorhanden'" unless (convert_list["A3Info"] == "FOUP schon reserviert|Controljob schon vorhanden")
    convert_list = $rtdt.change_list_to_hash(list, cl)
    $result_rtd << "#{__method__}_3: Carrier #{li.carrier} is reserved and not found on RTD List" unless (convert_list["Lot ID"] == cl)
    $result_rtd << "#{__method__}_3: Carrier #{li.carrier} is reserved and A3 is Yes " unless (convert_list["A3"] == "N")
    $result_rtd << "#{__method__}_3: Carrier #{li.carrier} is reserved and A3 Info is '#{convert_list["A3Info"]}' instead of 'FOUP schon reserviert|Controljob schon vorhanden'" unless (convert_list["A3Info"] == "FOUP schon reserviert|Controljob schon vorhanden")
  end

  # *Testcase*: A3 Checks single recipe equipments
  #
  # *Number*: 80
  #
  # *Description*: Checks the single recipe tools versus single lot / multiple lot carrier.
  #
  # *Logic*:
  #
  # IF NULL mltrcp_capa OR mltrcp_capa!='Multiple Recipe' THEN IF MATCH(multi_lot_type, '*-SR') AND is_ml_foup_complete THEN A3=Y ELSE A3=N ELSE A3=Y
  #
  # *Condition*:
  #
  # multi_lot_type := FRCAST.multi_lot_type
  #
  # mltrcp_capa := FREQP.mltrcp_capa
  #
  # is_ml_foup_complete := IF NumberLotsOnList == number_lots_carrier AND FULL number_lots_carrier AND FULL NumberLotsOnList NumberLotsOnList := Count(cast_id)
  #
  # cast_id = FRCAST_LOT.CASTMAP.cast_id
  #
  # *Setup*:
  # * Product: RTDT-PRODUCT.01
  # * Tools: RTDIB01, RTDIB02
  # * PD: 1000.1000, 1000.1100
  # * Scriptparameter:
  # * Carriercategory: BEOL
  # * Splitlot: Yes

  def test080_A3_checks_single_recipe_equipments
    # Start values
    lopNo = "1000.1100"
    singletool = "RTDIB01"
    multitool = "RTDIB02"
    tool = $sv.lot_info(@@lot).opEqps

    # Test Cleanup
    assert $rtdt.eqp_cleanup(tool + singletool.split + multitool.split)
    assert $rtdt.lot_carrier_cleanup(@@lot)
    assert $rtdt.stocker_cleanup()

    # Check Equipment Single Recipe versus Single Lot /Multiple Lot Carrier Lot split and split Lot locate to Tool
    tool = $sv.lot_info(@@lot).opEqps[0]
    cl = $sv.lot_split(@@lot, 10)
    assert_equal 0, $sv.lot_opelocate(cl, lopNo)

    # Testcase 1: With Internal Buffer Tool and Multi Recipe
    sleep @@waittime
    list = $sv.rtd_dispatch_list(multitool)
    convert_list = $rtdt.change_list_to_hash(list, cl)
    $result_rtd << "#{__method__}_1: Split Lot #{cl} is locate on Tool and not found on RTD List" unless (convert_list["Lot ID"] == cl)
    $result_rtd << "#{__method__}_1: Split Lot #{cl} is on Multi Recipe Tool and A3 is No" unless (convert_list["A3"] == "Y")

    # Testcase 2: With Internal Buffer Tool and Single Recipe
    list = $sv.rtd_dispatch_list(singletool)
    convert_list = $rtdt.change_list_to_hash(list, cl)
    $result_rtd << "#{__method__}_2: Split Lot #{cl} is locate on Tool and not found on RTD List" unless (convert_list["Lot ID"] == cl)
    $result_rtd << "#{__method__}_2: Split Lot #{cl} is on Single Recipe Tool and A3 is Yes " unless (convert_list["A3"] == "N")

    # Testcase 3: With Tool and Single Recipe
    list = $sv.rtd_dispatch_list(tool)
    convert_list = $rtdt.change_list_to_hash(list, @@lot)
    $result_rtd << "#{__method__}_3: Main Lot #{@@lot} is locate on Tool and not found on RTD List" unless (convert_list["Lot ID"] == @@lot)
    $result_rtd << "#{__method__}_3: Main Lot #{@@lot} is on Single Recipe Tool and A3 is No" unless (convert_list["A3"] == "N")
  end

  # *Testcase*: A3 Check free inputports
  #
  # *Number*: 85
  #
  # *Description*: Check free and available inputports in case of automatically reservation.
  #
  # *Logic*:
  #
  # FRCAST_LOT.CastMap->CtrlJobMap->reserved_flag
  #
  # *Condition*:
  #
  # *Setup*:
  # * Product: RTDT-PRODUCT.01
  # * Tools: RTDT01
  # * PD: 1000.1000
  # * Scriptparameter:
  # * Carriercategory: BEOL
  # * Splitlot: No

  def test085_A3_checks_free_ports
    # Start values
    state = "Down"
    state1 = "LoadReq"
    li = $sv.lot_info(@@lot)
    tool = li.opEqps

    # Test Cleanup
    assert $rtdt.eqp_cleanup(tool)
    assert $rtdt.lot_carrier_cleanup(@@lot)
    assert $rtdt.stocker_cleanup()

    # Testcase 1: Check free Ports in case of automatic reservation
    li = $sv.lot_info(@@lot)
    eq = $sv.eqp_info(li.opEqps[0])
    eq.ports.each {|port|
      assert_equal 0, $sv.eqp_port_status_change(eq.eqp, port.port, state)
      sleep @@waittime
      list = $sv.rtd_dispatch_list(eq.eqp)
      convert_list = $rtdt.change_list_to_hash(list, @@lot)
      loadreq = $sv.eqp_info(eq.eqp).ports.collect{|pstatus| true if pstatus.state == state1}
      $result_rtd << "#{__method__}: RTD List show lots with no free Ports on Tool #{eq.eqp} or RTD List show no lots with free Ports on Tool #{eq.eqp}" unless (convert_list["Lot ID"] == @@lot) == (loadreq.member?(true))}
    eq.ports.each {|port|
    assert_equal 0, $sv.eqp_port_status_change(eq.eqp, port.port, state1)}
  end

  # *Testcase*: A3 Checks reservation (Code Nr: 3)
  #
  # *Number*: 90
  #
  # *Description*: The lot has A3 state = "N" and A3Info � FOUP schon reserviert�, because lot has already a reservation.
  #
  # *Logic*:
  #
  # Reservation:=
  #
  # IF FULL resrv_user_id THEN
  # "T:" + resrv_user_id
  #
  # ELSE IF FULL proc_resrv_user_id AND FULL ctrljob_id THEN
  # "P:" + proc_resrv_user_id + "-" + ctrljob_eqp
  #
  # ELSE IF FULL proc_resrv_user_id AND NULL ctrljob_id THEN
  # "F:" + proc_resrv_user_id + "-" + ctrljob_eqp
  #
  # ELSE
  # ""
  #
  # *Condition*:
  #
  # *Setup*:
  # * Product: RTDT-PRODUCT.01
  # * Tools: RTDT01
  # * PD: 1000.1000
  # * Scriptparameter:
  # * Carriercategory: BEOL
  # * Splitlot: Yes

  def test090_A3_checks_reservation
    # Start values
    port = "P1"
    tool = $sv.lot_info(@@lot).opEqps

    # Test Cleanup
    assert $rtdt.eqp_cleanup(tool)
    assert $rtdt.lot_carrier_cleanup(@@lot)
    assert $rtdt.stocker_cleanup()

    # Testcase 1: Check if Reservation is empty
    li = $sv.lot_info(@@lot)
    tool = $sv.lot_info(@@lot).opEqps[0]
    sleep @@waittime
    list = $sv.rtd_dispatch_list(tool)
    convert_list = $rtdt.change_list_to_hash(list, @@lot)
    $result_rtd << "#{__method__}_1: Carrier #{li.carrier} is reserved on Tool #{tool}" unless (convert_list["Reservation"] == "")
    $result_rtd << "#{__method__}_1: Carrier #{li.carrier} is reserved and A3 Info is filled with Reservation Infos" if (convert_list["A3Info"] == "FOUP schon reserviert")

    # Testcase 2: Check reservation Field is T
    assert_equal 0, $sv.npw_reserve(tool, :carrier => li.carrier, :port=>port)
    sleep @@waittime
    list = $sv.rtd_dispatch_list(tool)
    convert_list = $rtdt.change_list_to_hash(list, @@lot)
    a3info = convert_list["A3Info"].split("|")
    $result_rtd << "#{__method__}_2: Carrier #{li.carrier} is reserved and A3 Info is '#{convert_list["A3Info"]}' instead of 'FOUP schon reserviert'" unless a3info.member?"FOUP schon reserviert"
    $result_rtd << "#{__method__}_2: Carrier #{li.carrier} is not reserved on Tool #{tool}, but reserved user is set and state is T" unless (convert_list["Reservation"] == "T:X-UNITTEST")
    $result_rtd << "#{__method__}_2: Carrier #{li.carrier} is reserved and A3 is Yes #{@@lot}" unless (convert_list["A3"] == "N")
    assert_equal 0, $sv.npw_reserve_cancel(tool, :carrier => li.carrier, :port=>port)

    # Testcase 3: Check reservation Field is P
    assert_equal 0, $sv.eqp_mode_offline1(tool, :notifyTCS=>false)
    cj = $sv.slr(tool, :port=>port, :lots=>li.lot)
    sleep @@waittime
    list = $sv.rtd_dispatch_list(tool)
    convert_list = $rtdt.change_list_to_hash(list, @@lot)
    a3info = convert_list["A3Info"].split("|")
    reservation_tool, reservation_text = convert_list["Reservation"].reverse.split("-", 2).collect{|x| x.reverse}
    $result_rtd << "#{__method__}_3: Carrier #{li.carrier} is reserved and A3 Info is '#{convert_list["A3Info"]}' instead of 'FOUP schon reserviert|Controljob schon vorhanden'" unless a3info.member?("FOUP schon reserviert") && a3info.member?("Controljob schon vorhanden")
    $result_rtd << "#{__method__}_3: Carrier #{li.carrier} is not reserved on Tool #{tool}, but reserved user and tool is set and state is P" unless (reservation_text == "P:X-UNITTEST") && (reservation_tool == tool)
    $result_rtd << "#{__method__}_3: Carrier #{li.carrier} is reserved and A3 is Yes #{@@lot}" unless (convert_list["A3"] == "N")
    assert_equal 0, $sv.slr_cancel(tool, cj)
    assert_equal 0, $sv.eqp_mode_auto2(tool, :notifyTCS=>false)

    # Testcase 4: Check reservation Field is F
    assert_equal 0, $sv.eqp_mode_offline1(tool, :notifyTCS=>false)
    cl = $sv.lot_split(@@lot, 1)
    cj = $sv.slr(tool, :port=>port, :lots=>li.lot)
    sleep @@waittime
    list = $sv.rtd_dispatch_list(tool)
    convert_list = $rtdt.change_list_to_hash(list, cl)
    a3info = convert_list["A3Info"].split("|")
    reservation_tool, reservation_text = convert_list["Reservation"].reverse.split("-", 2).collect{|x| x.reverse}
    $result_rtd << "#{__method__}_4: Carrier #{li.carrier} has Info is not reserved on Tool #{tool}, instead of 'FOUP schon reserviert|Controljob schon vorhanden'" unless a3info.member?("FOUP schon reserviert") && a3info.member?("Controljob schon vorhanden")
    $result_rtd << "#{__method__}_4: Carrier #{li.carrier} is not reserved on Tool #{tool}, but reserved user and tool is set and state is F" unless (reservation_text == "F:X-UNITTEST") && (reservation_tool == tool)
    $result_rtd << "#{__method__}_4: Carrier #{li.carrier} is reserved and A3 is Yes #{cl}" unless (convert_list["A3"] == "N")
    assert_equal 0, $sv.slr_cancel(tool, cj)
    assert_equal 0, $sv.lot_merge(@@lot, cl)
    assert_equal 0, $sv.eqp_mode_auto2(tool, :notifyTCS=>false)

    # Testcase 5 Check A3 = Y without reservation
    sleep @@waittime
    list = $sv.rtd_dispatch_list(tool)
    convert_list = $rtdt.change_list_to_hash(list, @@lot)
    $result_rtd << "#{__method__}_5: Lot #{@@lot} is not found on RTD List" unless (convert_list["Lot ID"] == @@lot)
    $result_rtd << "#{__method__}_5: A3 is No but should be Yes" unless (convert_list["A3"] == "Y")
  end

  # *Testcase*: A3 Checks free inputports for carrier category (Code Nr: 99)
  #
  # *Number*:95
  #
  # *Description*: Check free available inputports for carrier with defined categorie, port dispatchable.
  #
  # *Logic*:
  #
  # IF  (
  # $requestingEntity == "Push" OR $requestingEntity == "Pull" OR $requestingEntity == "AutoPush" OR $requestingEntity == "AutoPull")
  # AND
  # NULL inputports THEN A3Info = Kein Input Port verfuegbar ELSE IF FULL ports_input_auto AND NULL ports_input_auto_by_cast_category
  # THEN A3Info = Kein Input Port verfuegbar ELSE A3code
  #
  #
  # *Condition*:
  #
  # ports_input_auto:= comma separate string from port_ids (FRPORT_CASTCAP.PortMap.port_id) from
  # Ports on the equipment which are inputports, and not down and comply with following conditionen:
  # "Required" == port_disp_state AND
  # "LoadReq" == port_state AND
  # (
  # MATCH(mcopemode_id,"*-3")  OR (
  # MATCH(mcopemode_id,"{Semi-Start,Auto}-1") AND FULL PullPorts AND MATCH (port_id,"{"+PullPorts+"}")
  # )
  # )
  #
  # ports_input_auto_by_cast_category:= like ports_input_auto , here also include the cast_category
  #
  # *Setup*:
  # * Product: RTDT-PRODUCT.01
  # * Tools: RTDT01, RTDA01
  # * PD: 1000.1000, 1000.1300
  # * Scriptparameter:
  # * Carriercategory: FEOL
  # * Splitlot: No

  def test095_A3_checks_carrier_free_ports
    # Start values
    opNo = "1000.1300"
    port = "P2"
    state = "LoadComp"
    statelr = "LoadReq"
    stocker = "UTSTO11"
    sstate1 = "MI"
    sstate2 = "MO"
    tool = $sv.lot_info(@@lot).opEqps

    # Test Cleanup
    assert $rtdt.eqp_cleanup(tool)
    assert $rtdt.lot_carrier_cleanup(@@lot)
    assert $rtdt.stocker_cleanup()

    # Create new Lot
    carrier_new = $sv.carrier_list(:category=>"FEOL", :status =>"AVAILABLE", :carrier =>"G%", :empty=>true).first
    @@lot2 = $sv.new_lot_release(@@new_lot_params.merge(:carrier=>carrier_new))
    li2 = $sv.lot_info(@@lot2)

    # Testcase 1: Port dispatchable 1 Lot is load on Port2 and other Lot can only on Port1 but this port allow no FEOL Carrier
    li = $sv.lot_info(@@lot)
    assert_equal 0, $sv.lot_opelocate(@@lot, opNo)
    assert_equal 0, $sv.lot_opelocate(@@lot2, opNo)
    tool = $sv.lot_info(@@lot).opEqps[0]
    assert_equal 0, $sv.carrier_xfer_status_change(li.carrier, sstate1, stocker)
    assert_equal 0, $sv.carrier_xfer_status_change(li2.carrier, sstate1, stocker)
    cj = $sv.slr(tool, :port=>port, :lots=>li.lot)
    assert_equal 0, $sv.eqp_port_status_change(tool, port, state)
    assert_equal 0, $sv.carrier_xfer_status_change(li.carrier, sstate2, stocker)
    assert_equal 0, $sv.eqp_load(tool, port, li.carrier)
    assert_equal 0, $sv.eqp_mode_auto3(tool, :notifyTCS=>false)
    sleep @@waittime
    list = $sv.rtd_dispatch_list(tool)
    convert_list = $rtdt.change_list_to_hash(list, @@lot2)
    $result_rtd << "#{__method__}_1: Lot #{@@lot2} is not found on RTD List" unless (convert_list["Lot ID"] == @@lot2)
    $result_rtd << "#{__method__}_1: Tool #{tool} has no free Port for the Carrier #{carrier_new} category FEOL and A3 is Yes" unless (convert_list["A3"] == "N")
    $result_rtd << "#{__method__}_1: Tool #{tool} has no free Port for the Carrier #{carrier_new} category FEOL A3 Info is '#{convert_list["A3Info"]}' instead of 'Kein Input Port verfuegbar'"  unless (convert_list["A3Info"] == "Kein Input Port verfuegbar")
    assert_equal 0, $sv.eqp_unload(tool, port, li.carrier)
    assert_equal 0, $sv.slr_cancel(tool, cj)
    assert_equal 0, $sv.eqp_port_status_change(tool, port, statelr)
    assert_equal 0, $sv.eqp_mode_auto2(tool, :notifyTCS=>false)
    assert_equal 0, $sv.carrier_xfer_status_change(li.carrier, sstate1, stocker)

    # Testcase 2: A3 Check with free Ports
    sleep @@waittime
    list = $sv.rtd_dispatch_list(tool)
    convert_list = $rtdt.change_list_to_hash(list, @@lot2)
    $result_rtd << "#{__method__}_2: Lot #{@@lot2} is not found on RTD List" unless (convert_list["Lot ID"] == @@lot2)
    $result_rtd << "#{__method__}_2: A3 is No but should be Yes" unless (convert_list["A3"] == "Y")
    @@lots += [@@lot2]
  end

  # *Testcase*: A3 A3 Checks inter fab move allowed (Code Nr: 115)
  #
  # *Number*: 100
  #
  # *Description*: The location is not the same target location.
  #
  # *Logic*:
  #
  # IF Location != PrioLocation AND MATCH(AutomationType,"*NoInterFabXfer*") THEN A3Info="No Interfab Transfer per Setup"
  #
  # *Condition*:
  #
  # PrioLocation := IF has_fabwish THEN PrioLocation ELSE CarrierLocation
  #
  # PrioLocation :=
  # IF FULL TargetLocation THEN (
  # IF TargetLocation == "Module2" THEN "F38" ELSE IF TargetLocation == "Module1" THEN "F36" ELSE TargetLocation)
  # ELSE CarrierLocation
  #
  # has_fabwish := FULL tl_trigger_time AND FULL tl_pd_Id AND tl_pd_Id == pd_id
  #
  # tl_trigger_time := Timestamp on targetLocation
  #
  # tl_pd_id := pd_id from the targetLocation
  #
  # *Setup*:
  # * Product: RTDT-PRODUCT.01
  # * Tools: RTDA03
  # * PD: 1000.1000, 1000.1400
  # * Scriptparameter: TargetLocation
  # * Carriercategory: BEOL
  # * Splitlot: No

  def test100_A3_checks_InterFabMove_allowed
    # Start values
    opNo = "1000.1400"
    lopNo = "1000.1000"
    tool = $sv.lot_info(@@lot).opEqps
    tool2 = "RTDA03"
    tool3 = "RTDT01"

    # Test Cleanup
    assert $rtdt.eqp_cleanup(tool + tool2.split + tool3.split)
    assert $rtdt.lot_carrier_cleanup(@@lot)
    assert $rtdt.stocker_cleanup()

    # Testcase 1: Set Interfab transfer
    assert_equal 0,  $sv.lot_opelocate(@@lot, opNo)
    sleep @@waittime
    list = $sv.rtd_dispatch_list(tool2)
    convert_list = $rtdt.change_list_to_hash(list, @@lot)
    $result_rtd << "#{__method__}_1: Lot #{@@lot} is not found on RTD List" unless (convert_list["Lot ID"] == @@lot)
    $result_rtd << "#{__method__}_1: A3 is Yes but should be no" unless (convert_list["A3"] == "N")
    $result_rtd << "#{__method__}_1: A3 Info is wrong '#{convert_list["A3Info"]}' instead of 'NoIFX'" unless (convert_list["A3Info"] == "No Interfab Transfer per Setup|Carrier at other location")
    assert_equal 0, $sv.lot_opelocate(@@lot, lopNo)

    # Testcase 2: A3 Check without Interfab transfer
    sleep @@waittime
    list = $sv.rtd_dispatch_list(tool3)
    convert_list = $rtdt.change_list_to_hash(list, @@lot)
    $result_rtd << "#{__method__}_2: Lot #{@@lot} is not found on RTD List" unless (convert_list["Lot ID"] == @@lot)
    $result_rtd << "#{__method__}_2: A3 is No but should be Yes" unless (convert_list["A3"] == "Y")
  end

  # *Testcase*: A3 Checks stocker/AMHS Problem (Code Nr: 7)
  #
  # *Number*: 105
  #
  # *Description*: Change Stocker State to another as reachable.
  #
  # *Logic*:
  #
  # IF FULL stocker_e10_state AND stocker_e10_state!="2WPR" AND stocker_e10_state!="2NDP" AND stocker_e10_state!="1PRD" THEN A3Info = �Stocker/AMHS Problem�
  #
  # *Condition*:
  #
  # stocker_e10_state := FRCAST_LOT.CastMap.StkMap.cur_state_id
  #
  # *Setup*:
  # * Product: RTDT-PRODUCT.01
  # * Tools: RTDT01
  # * PD: 1000.1000
  # * Scriptparameter:
  # * Carriercategory: BEOL
  # * Splitlot: No

  def test105_A3_checks_stocker_reachable
    # Start values
    stocker = "UTSTO12"
    sstat = "MI"
    state = "2NDP"
    state1 = "3OTH"
    state2 = "2WPR"
    tool = $sv.lot_info(@@lot).opEqps

    # Test Cleanup
    assert $rtdt.eqp_cleanup(tool)
    assert $rtdt.lot_carrier_cleanup(@@lot)
    assert $rtdt.stocker_cleanup()

    # Testcase 1: Set Stocker to not reachable
    li = $sv.lot_info(@@lot)
    tool = li.opEqps[0]
    assert_equal 0, $sv.carrier_xfer_status_change(li.carrier, sstat, stocker)
    assert_equal 0, $sv.eqp_status_change_req(stocker, state)
    assert_equal 0, $sv.eqp_status_change_req(stocker, state1)
    sleep @@waittime
    list = $sv.rtd_dispatch_list(tool)
    convert_list = $rtdt.change_list_to_hash(list, @@lot)
    $result_rtd << "#{__method__}_1: Lot #{@@lot} is not found on RTD List" unless (convert_list["Lot ID"] == @@lot)
    $result_rtd << "#{__method__}_1: A3 is Yes but should be No" unless (convert_list["A3"] == "N")
    $result_rtd << "#{__method__}_1: A3 Info is wrong '#{convert_list["A3Info"]}' instead of 'Stocker/AMHS Problem'" unless (convert_list["A3Info"] == "Stocker/AMHS Problem")
    assert_equal 0, $sv.eqp_status_change_req(stocker, state2)

    # Testcase 2: A3 Check Stocker reachable
    sleep @@waittime
    list = $sv.rtd_dispatch_list(tool)
    convert_list = $rtdt.change_list_to_hash(list, @@lot)
    $result_rtd << "#{__method__}_2: Lot #{@@lot} is not found on RTD List" unless (convert_list["Lot ID"] == @@lot)
    $result_rtd << "#{__method__}_2: A3 is No but should be Yes" unless (convert_list["A3"] == "Y")
  end

  # *Testcase*: A3 Checks siview setup (Code Nr: 9)
  #
  # *Number*: 110
  #
  # *Description*: Auto 3 - A and Operation -A set on PD, then A3 Info.
  #
  # *Logic*:
  #
  # IF (MATCH(OperationReportingType, '*-A*') OR MATCH(mrecipe_auto3, '*-A*')) THEN A3Info = �Kein A3 per SiViewSetup�
  #
  # *Condition*:
  # OperationReportingType := FRPD_UDATA{pd_d_thesystemkey, "OperationReportingType"}.value
  #
  # pd_d_thesystemkey := FRLOT_EQP.d_thesystemkey
  #
  # mrecipe_auto3:= FRLRCP_DSET_RPARM{d_thesystemkey, STRING(d_seqno), "AUTO-3"}.rparm_default
  #
  # *Setup*:
  # * Product: RTDT-PRODUCT.01
  # * Tools: RTDA04, RTDT02
  # * PD: 1000.1000, 1000.1500
  # * Scriptparameter:
  # * Carriercategory: BEOL
  # * Splitlot: No

  def test110_A3_checks_siview
    # Start values
    li = $sv.lot_info(@@lot)
    tool = $sv.lot_info(@@lot).opEqps
    tool2 = "RTDA04"
    tool3 = "RTDT02"
    opNo = "1000.1500"
    opNo2 = "1000.1200"

    # Test Cleanup
    assert $rtdt.eqp_cleanup(tool + tool2.split + tool3.split)
    assert $rtdt.lot_carrier_cleanup(@@lot)
    assert $rtdt.stocker_cleanup()

    # Testcase 1: Operation Dispatch -A No A3 per Siview
    assert_equal 0, $sv.lot_opelocate(@@lot, opNo)
    sleep @@waittime
    list = $sv.rtd_dispatch_list(tool2)
    convert_list = $rtdt.change_list_to_hash(list, @@lot)
    $result_rtd << "#{__method__}_1: Lot #{@@lot} is not found on RTD List" unless (convert_list["Lot ID"] == @@lot)
    $result_rtd << "#{__method__}_1: A3 is Yes but should be no" unless (convert_list["A3"] == "N")
    $result_rtd << "#{__method__}_1: A3 Info is wrong '#{convert_list["A3Info"]}' instead of 'Kein A3 per SiViewSetup'" unless (convert_list["A3Info"] == "Kein A3 per SiViewSetup")
    assert_equal 0, $sv.lot_opelocate(@@lot, opNo2)

    # Testcase 2: A3 Check Siview Yes
    sleep @@waittime
    list = $sv.rtd_dispatch_list(tool3)
    convert_list = $rtdt.change_list_to_hash(list, @@lot)
    $result_rtd << "#{__method__}_2: Lot #{@@lot} is not found on RTD List" unless (convert_list["Lot ID"] == @@lot)
    $result_rtd << "#{__method__}_2: A3 is No but should be Yes" unless (convert_list["A3"] == "Y")
  end

  # *Testcase*: A3 Checks AHMSMatch (Code Nr: 31)
  #
  # *Number*: 115
  #
  # *Description*: Change AMHS Area from the equipment.
  #
  # *Logic*:
  #
  # IF FULL vamos AND NOT vamos AND AMHS!=CarrierAMHS THEN A3Info=�No AMHS connect�
  #
  # *Condition*:
  #
  # AHMS := AmhsArea from the equipment there lot must be arrive (FREQP_UDATA{eqp_stk_d_thesystemkey,"AmhsArea"}.value oder FRSTK_UDATA{eqp_stk_d_thesystemkey,"AmhsArea"})
  #
  # CarrierAMHS := AmhsArea from the Carriers
  #
  # *Setup*:
  # * Product: RTDT-PRODUCT.01
  # * Tools: RTDA06
  # * PD: 1000.1000, 1000.1600
  # * Scriptparameter:
  # * Carriercategory: BEOL
  # * Splitlot: No

  def test115_A3_checks_AHMS
    # Start values
    tool = $sv.lot_info(@@lot).opEqps
    tool2 = "RTDA06"
    opNo = "1000.1600"

    # Test Cleanup
    assert $rtdt.eqp_cleanup(tool + tool2.split)
    assert $rtdt.lot_carrier_cleanup(@@lot)
    assert $rtdt.stocker_cleanup()

    # Testcase: Change AHMS Area to an undefind Area
    assert_equal 0, $sv.lot_opelocate(@@lot, opNo)
    sleep @@waittime
    list = $sv.rtd_dispatch_list(tool2)
    convert_list = $rtdt.change_list_to_hash(list, @@lot)
    $result_rtd << "#{__method__}: Lot #{@@lot} is not found on RTD List" unless (convert_list["Lot ID"] == @@lot)
    $result_rtd << "#{__method__}: A3 is Yes but should be no" unless (convert_list["A3"] == "N")
    $result_rtd << "#{__method__}: A3 Info is wrong '#{convert_list["A3Info"]}' instead of 'No AMHS connect'" unless (convert_list["A3Info"] == "No AMHS connect")
  end

  # *Testcase*: A3 Checks Eng Recipe (Code Nr: 27)
  #
  # *Number*: 120
  #
  # *Description*: Move the lot to a branchroute.
  #
  # *Logic*:
  #
  # IF (FULL EngRcpSelect AND EngRcpSelect==pd_id) OR mainpd_id[3,5]=="ERS" THEN A3Info=� Eng Recipe Select�
  #
  # *Condition*:
  #
  # EngRcpSelect := PPTYSTRING_altKey_d_objmanager_pptystring_id{property_obj_eqp, "Push"}.PPTYSTRING_ptr.value
  #
  # *Setup*:
  # * Product: RTDT-PRODUCT.01
  # * Tools: RTDT01
  # * PD: 1000.1000
  # * Scriptparameter:
  # * Carriercategory: BEOL
  # * Splitlot: No

  def test120_A3_checks_eng_recipe
    # Start values
    tool = $sv.lot_info(@@lot).opEqps
    broute = "P-ERS-RTDT-MAINPD.01"
    retop = "1000.1000"
    tool2 = "RTDT01"

    # Test Cleanup
    assert $rtdt.eqp_cleanup(tool + tool2.split)
    assert $rtdt.lot_carrier_cleanup(@@lot)
    assert $rtdt.stocker_cleanup()

    # Testcase: Set Lot to Branch Route
    assert_equal 0, $sv.lot_branch(@@lot, broute, :retop=>retop, :dynamic => true)
    sleep @@waittime
    list = $sv.rtd_dispatch_list(tool2)
    convert_list = $rtdt.change_list_to_hash(list, @@lot)
    $result_rtd << "#{__method__}: Lot #{@@lot} is not found on RTD List" unless (convert_list["Lot ID"] == @@lot)
    $result_rtd << "#{__method__}: A3 is Yes but should be no" unless (convert_list["A3"] == "N")
    $result_rtd << "#{__method__}: A3 Info is wrong '#{convert_list["A3Info"]}' should be include 'Eng Recipe Select'" unless (convert_list["A3Info"].split("|").member?("Eng Recipe Select"))
  end

  # *Testcase*: A3 Checks manual area (Code Nr: 34)
  #
  # *Number*: 125
  #
  # *Description*: Move the lot from one to another tool with different manuell Area.
  #
  # *Logic*:
  #
  # IF FULL vamos AND vamos AND NOT ManualAreaMatch AND
  # (NOT AMHSMatch OR (DISP_PUSH_FG == "Y" AND (mcopemode_id=='Auto-1' OR mcopemode_id=='Semi-Start-1') ))THEN A3Info = �Falscher Tragebereich�
  # ELSE A3code
  #
  # *Condition*:
  #
  # ManualAreaMatch // The ManualArea from the target tool there the carrier is located is not the same like the manuall area from the receive tool
  #
  # ManualArea := FREQP_UDATA{eqp_stk_d_thesystemkey,"ManualArea"}.value oder FRSTK_UDATA{eqp_stk_d_thesystemkey,"ManualArea"}.value
  #
  # *Setup*:
  # * Product: RTDT-PRODUCT.01
  # * Tools: RTDA06, RTDA07
  # * PD: 1000.1000, 1000.1600, 1000.1700
  # * Scriptparameter:
  # * Carriercategory: BEOL
  # * Splitlot: No

  def test125_A3_checks_manual_area
    # Start values
    opNo = "1000.1600"
    opNo2 = "1000.1700"
    port = "P1"
    state = "LoadComp"
    statelr = "LoadReq"
    stocker = "UTSTO11"
    stocker_state1 = "MI"
    stocker_state2 = "MO"
    tool = $sv.lot_info(@@lot).opEqps
    tool2 = "RTDA07"

    # Test Cleanup
    assert $rtdt.eqp_cleanup(tool + tool2.split)
    assert $rtdt.lot_carrier_cleanup(@@lot)
    assert $rtdt.stocker_cleanup()

    # Testcase: Manual Area Lot load on tool after that tool locate to tool with other manuall Area
    li = $sv.lot_info(@@lot)
    assert_equal 0, $sv.lot_opelocate(@@lot, opNo)
    tool_load = $sv.lot_info(@@lot).opEqps[0]
    assert_equal 0, $sv.eqp_mode_auto1(tool2, :notifyTCS=>false)
    assert_equal 0, $sv.carrier_xfer_status_change(li.carrier, stocker_state1, stocker)
    cj = $sv.slr(tool_load, :port=>port, :lots=>li.lot)
    assert_equal 0, $sv.eqp_port_status_change(tool_load, port, state)
    assert_equal 0, $sv.carrier_xfer_status_change(li.carrier, stocker_state2, stocker)
    assert_equal 0, $sv.eqp_load(tool_load, port, li.carrier)
    assert_equal 0, $sv.eqp_unload(tool_load, port, li.carrier)
    assert_equal 0, $sv.slr_cancel(tool_load, cj)
    assert_equal 0, $sv.eqp_port_status_change(tool_load, port, statelr)
    assert_equal 0, $sv.lot_opelocate(@@lot, opNo2)
    sleep @@waittime
    list = $sv.rtd_dispatch_list(tool2)
    convert_list = $rtdt.change_list_to_hash(list, @@lot)
    $result_rtd << "#{__method__}: Lot #{@@lot} is not found on RTD List" unless (convert_list["Lot ID"] == @@lot)
    $result_rtd << "#{__method__}: A3 is Yes but should be no" unless (convert_list["A3"] == "N")
    $result_rtd << "#{__method__}: A3 Info is wrong '#{convert_list["A3Info"]}' should be include 'Falscher Tragebereich'" unless (convert_list["A3Info"].split("|").member?("Falscher Tragebereich"))
  end

  # *Testcase*: A3 Checks tool DOWN E10 (Code Nr: 10)
  #
  # *Number*: 130
  #
  # *Description*: Equipment down and PD with parameter E10ConditionalAvailable.
  #
  # *Logic*:
  #
  # IF FULL equip_down AND equip_down THEN
  # IF ((NOT MATCH(OperationReportingType, '*-D*') AND NOT MATCH(mrecipe_auto3, '*-D*')) AND (MATCH(OperationReportingType, '*+D*') OR MATCH(mrecipe_auto3, '*+D*')))
  # THEN A3code
  # ELSE A3Info = �Tool ist DOWN/zeitweise in speziellem Status�
  # ELSE A3code
  #
  # *Condition*:
  #
  # equip_down := IF ((e10_state!='PRD') AND (e10_state!='SBY')) THEN TRUE ELSE FALSE
  #
  # *Setup*:
  # * Product: RTDT-PRODUCT.01
  # * Tools: RTDA01
  # * PD: 1000.1000, 1000.1300
  # * Scriptparameter:
  # * Carriercategory: BEOL
  # * Splitlot: No

  def test130_A3_checks_tool_down
    # Start values
    opNo = "1000.1300"
    estatus1 = "2NDP"
    estatus2 = "3OTH"
    estatus3 = "2WPR"
    tool = $sv.lot_info(@@lot).opEqps
    tool2 = "RTDA01"
    type = ["Equipment Monitor", "Recycle", "Dummy", "Process Monitor", "PO"]
    sublottype = type

    # Test Cleanup
    assert $rtdt.eqp_cleanup(tool + tool2.split)
    assert $rtdt.lot_carrier_cleanup(@@lot)
    assert $rtdt.stocker_cleanup()

    # Testcase: Set Tool Down
    assert_equal 0, $sv.lot_opelocate(@@lot, opNo)
    assert_equal 0, $sv.eqp_status_change_req(tool2, estatus1)
    assert_equal 0, $sv.eqp_status_change_req(tool2, estatus2)
    sublottype.each {|s|
      assert_equal 0, $sv.lot_mfgorder_change(@@lot, sublottype: s)
      sleep @@waittime
      list = $sv.rtd_dispatch_list(tool2)
      convert_list = $rtdt.change_list_to_hash(list, @@lot)
      $result_rtd << "#{__method__}_1: Lot #{@@lot} is not found on RTD List" unless (convert_list["Lot ID"] == @@lot)
      $result_rtd << "#{__method__}_1: A3 is Yes but should be No" unless (convert_list["A3"] == "N")
      $result_rtd << "#{__method__}_1: A3 Info is wrong '#{convert_list["A3Info"]}' instead of 'Tool ist DOWN/zeitweise in speziellem Status'" unless (convert_list["A3Info"] == "Tool ist DOWN/zeitweise in speziellem Status")}
    assert_equal 0,$sv.lot_mfgorder_change(@@lot, sublottype: "PO")
    assert_equal 0, $sv.eqp_status_change_req(tool2, estatus3)

    # Testcase 2: A3 Check Tool Reachable
    sleep @@waittime
    list = $sv.rtd_dispatch_list(tool2)
    convert_list = $rtdt.change_list_to_hash(list, @@lot)
    $result_rtd << "#{__method__}_2: Lot #{@@lot} is not found on RTD List" unless (convert_list["Lot ID"] == @@lot)
    $result_rtd << "#{__method__}_2: A3 is No but should be Yes" unless (convert_list["A3"] == "Y")
  end

  # *Testcase*: A3 Checks carrier reachable 4 (Code Nr: 66)
  #
  # *Number*: 135
  #
  # *Description*: Lot is on an Internal Buffer tool and the carrier is also on a Internal Buffer tool.
  #
  # *Logic*:
  #
  # IF trans_state=="EI" AND carrier_eqp_category=="Internal Buffer" AND FULL carrier_mtrlloc_id AND carrier_mtrlloc_id enth�lt "Lot." THEN A3Info=� Carrier noch im Internal Buffer Tool�
  #
  # *Condition*:
  #
  # *Setup*:
  # * Product: RTDT-PRODUCT.01
  # * Tools: RTDIB01
  # * PD: 1000.1000, 1000.1100
  # * Scriptparameter:
  # * Carriercategory: BEOL
  # * Splitlot: No

  def test135_A3_checks_carrier_reachable_4
    # Start values
    opNo = "1000.1100"
    port = "PG1"
    state = "LoadComp"
    stocker = "UTSTO11"
    state1 = "MI"
    state2 = "MO"
    tool1 = $sv.lot_info(@@lot).opEqps
    tool = "RTDIB01"

    # Test Cleanup
    assert $rtdt.eqp_cleanup(tool1 + tool.split)
    assert $rtdt.lot_carrier_cleanup(@@lot)
    assert $rtdt.stocker_cleanup()

    # Testcase: Lot on Internal Buffer Tool
    li = $sv.lot_info(@@lot)
    assert_equal 0, $sv.lot_opelocate(@@lot, opNo)
    tool = $sv.lot_info(@@lot).opEqps[0]
    assert_equal 0, $sv.carrier_xfer_status_change(li.carrier, state1, stocker)
    cj = $sv.slr(tool, :port=>port, :lots=>li.lot)
    assert_equal 0, $sv.eqp_port_status_change(tool, port, state)
    assert_equal 0, $sv.carrier_xfer_status_change(li.carrier, state2, stocker)
    assert_equal 0, $sv.eqp_load(tool, port, li.carrier)
    sleep @@waittime
    list = $sv.rtd_dispatch_list(tool)
    convert_list = $rtdt.change_list_to_hash(list, @@lot)
    $result_rtd << "#{__method__}: Lot #{@@lot} is not found on RTD List" unless (convert_list["Lot ID"] == @@lot)
    $result_rtd << "#{__method__}: A3 is Yes but should be no" unless (convert_list["A3"] == "N")
    $result_rtd << "#{__method__}: A3 Info is wrong '#{convert_list["A3Info"]}' should be include 'Carrier noch im Internal Buffer Tool'" unless (convert_list["A3Info"].split("|").member?("Carrier noch im Internal Buffer Tool"))
  end

  # *Testcase*: A3 Checks OperPassCount (Code Nr: 30)
  #
  # *Number*: 140
  #
  # *Description*: Lot is on this PD again.
  #
  # *Logic*:
  #
  # IF FULL pass_count_flag AND pass_count_flag==1 THEN A3Info= �Los ist wiederholt an dieser PD�
  #
  # *Condition*:
  #
  # pass_count_flag := IF (pd_type == "Process") AND (lot_category != "TEST") AND
  # (NOT MATCH(OperationReportingType, "*AllowMultiPass*")) AND (POMap->pass_count > 1) THEN 1 ELSE 0
  #
  # *Setup*:
  # * Product: RTDT-PRODUCT.01
  # * Tools: RTDT01
  # * PD: 1000.1000
  # * Scriptparameter:
  # * Carriercategory: BEOL
  # * Splitlot: No

  def test140_A3_checks_operpasscount
    # Start values
    cl = $sv.carrier_list(:category=>"BEOL", :status=>"AVAILABLE", :carrier =>"G%", :empty=>true)
    @@lot3 = $sv.new_lot_release(@@new_lot_params.merge(:nwafers=>5, :carrier=>cl.pop))
    opNo = "1000.1000"
    tool = $sv.lot_info(@@lot).opEqps
    tool2 = "RTDT01"
    stocker = "UTSTO11"
    state1 = "MI"
    state2 = "MO"

    # Test Cleanup
    assert $rtdt.eqp_cleanup(tool + tool2.split)
    assert $rtdt.lot_carrier_cleanup(@@lot.split + @@lot3.split)
    assert $rtdt.stocker_cleanup()

    # Testcase: Lot is on PD again
    li3 = $sv.lot_info(@@lot3)
    assert_equal 0, $sv.eqp_mode_offline1(tool2, :notifyTCS=>false)
    assert_equal 0, $sv.carrier_xfer_status_change(li3.carrier, state2, stocker)
    refute_equal "", cj = $sv.claim_process_lot(@@lot3)
    assert_equal 0, $sv.eqp_mode_auto2(tool2, :notifyTCS=>false)
    assert_equal 0, $sv.lot_opelocate(@@lot3, opNo)
    assert_equal 0, $sv.carrier_xfer_status_change(li3.carrier, state1, stocker)
    sleep @@waittime
    list = $sv.rtd_dispatch_list(tool2)
    convert_list = $rtdt.change_list_to_hash(list, @@lot3)
    $result_rtd << "#{__method__}: Lot #{@@lot3} is not found on RTD List" unless (convert_list["Lot ID"] == @@lot3)
    $result_rtd << "#{__method__}: A3 is Yes but should be no" unless (convert_list["A3"] == "N")
    $result_rtd << "#{__method__}: A3 Info is wrong '#{convert_list["A3Info"]}' instead of 'Los ist wiederholt an dieser PD'" unless (convert_list["A3Info"] == "Los ist wiederholt an dieser PD")
    @@lots += [@@lot3]
  end

  # *Testcase*: A3 Checks Post Process Flag (Code Nr: 95)
  #
  # *Number*: 145
  #
  # *Description*: PostProcessFlag is set.
  #
  # *Logic*:
  #
  # IF post_process_flag THEN A3Info=� PostProcessFlag is set�
  #
  # post_process_flag := FRCAST_LOT -> LotMap -> post_process_flag
  #
  # *Condition*:
  #
  # post_process_flag := FRCAST_LOT -> LotMap -> post_process_flag
  #
  # *Setup*:
  # * Product: RTDT-PRODUCT.01
  # * Tools: RTDIB01
  # * PD: 1000.1000, 1000.1100, 1000.1200
  # * Scriptparameter:
  # * Carriercategory: BEOL
  # * Splitlot: No

  def test145_A3_checks_postprocessflag
    # Start values
    splitop = "1000.1100"
    retop = "1000.1200"
    broute = "P-ERS-RTDT-MAINPD.01"
    tool = $sv.lot_info(@@lot).opEqps
    tool2 = "RTDIB01"

    # Test Cleanup
    assert $rtdt.eqp_cleanup(tool + tool2.split)
    assert $rtdt.lot_carrier_cleanup(@@lot)
    assert $rtdt.stocker_cleanup()

    # Testcase: Lot PostProcessFlag is set
    assert_equal 0, $sv.lot_experiment(@@lot, 10, broute, splitop, retop, :dynamic=>true)
    assert_equal 0, $sv.lot_futurehold(@@lot, splitop, "ENG", :post=>true)
    assert_equal 1738, $sv.lot_opelocate(@@lot, splitop)
    sleep @@waittime
    list = $sv.rtd_dispatch_list(tool2)
    convert_list = $rtdt.change_list_to_hash(list, @@lot)
    $result_rtd << "#{__method__}: Lot #{@@lot} is not found on RTD List" unless (convert_list["Lot ID"] == @@lot)
    $result_rtd << "#{__method__}: A3 is Yes but should be no" unless (convert_list["A3"] == "N")
    $result_rtd << "#{__method__}: A3 Info is wrong '#{convert_list["A3Info"]}'instead of 'PostProcessFlag is set'" unless (convert_list["A3Info"] == "PostProcessFlag is set")
  end

  # *Testcase*: A3 Checks LeadLotWaitForJmp (Code Nr: 96)
  #
  # *Number*: 150
  #
  # *Description*: The lot must be blocked and then A3 Info "Lead Lot wait for Jumper".
  #
  # *Logic*:
  #
  # IF FULL block AND block THEN A3Info=� Lead Lot wait for Jumper�
  #
  # *Condition*:
  #
  # block :=come from ie_block_lead_lot.apf in the class folder
  #
  # *Setup*:
  # * Product: RTDT-PRODUCT.01
  # * Tools: RTDT01
  # * PD: 1000.1000
  # * Scriptparameter:
  # * Carriercategory: BEOL
  # * Splitlot: No

  def test150_A3_checks_leadlotwaitforjmp
    # Start values
    cl = $sv.carrier_list(:category=>"BEOL", :status=>"AVAILABLE", :carrier =>"G%", :empty=>true)
    @@lblock = $sv.new_lot_release(@@new_lot_params.merge(:nwafers=>5, :carrier=>cl.pop, :lot =>"U253Q.00"))
    tool = $sv.lot_info(@@lot).opEqps
    stocker = "UTSTO11"
    state1 = "MI"

    # Test Cleanup
    assert $rtdt.eqp_cleanup(tool)
    assert $rtdt.lot_carrier_cleanup(@@lot.split + @@lblock.split)
    assert $rtdt.stocker_cleanup()

    # Testcase: Lot is blocked
    li4 = $sv.lot_info(@@lblock)
    tool2 = li4.opEqps[0..2]
    assert_equal 0, $sv.carrier_xfer_status_change(li4.carrier, state1, stocker)
    tool2.each {|t|
      sleep @@waittime
      list = $sv.rtd_dispatch_list(t)
      convert_list = $rtdt.change_list_to_hash(list, @@lblock)
      $result_rtd << "#{__method__}: Lot #{@@lblock} is not found on RTD List" unless (convert_list["Lot ID"] == @@lblock)
      $result_rtd << "#{__method__}: A3 is Yes but should be no" unless (convert_list["A3"] == "N")
      $result_rtd << "#{__method__}: A3 Info is wrong '#{convert_list["A3Info"]}' instead of 'Lead Lot wait for Jumper'" unless (convert_list["A3Info"] == "Lead Lot wait for Jumper")}
    @@lots += [@@lblock]
  end

  # *Testcase*: A3 Checks Man Level xfer (Code Nr: 65)
  #
  # *Number*: 155
  #
  # *Description*: Hand Stocker is activate.
  #
  # *Logic*:
  #
  # IF FULL allow_level_transfer AND NOT allow_level_transfer THEN A3Info = �manueller Level XFER� // allow_level_transfer must be available and false
  #
  # IF manuel_transport AND (vet_availible==0  OR priority_class>3) THEN is_BayNameLevel1 == is_cur_bayNameLevel1 AND FULL is_cur_bayNameLevel1 AND is_BayNameLevel1 ELSE TRUE
  #
  # *Condition*:
  #
  # manuel_transport := stk_id=="A-VETHAND" AND (e10_state=="PRD" OR e10_state=="SBY")
  #
  # vet_availible := SUM(IF e10_state=="PRD" OR e10_state=="SBY" THEN 1 ELSE 0) //for all Stocker with stk_id[1,4]=="VET0" from table FRSTK
  #
  # priority_class:= FRLOT_EQP -> LotMap -> priority_class
  #
  # is_cur_bayNameLevel1 := TRUE if CarrierBayName has the String "LEVEL1"
  #
  # is_BayNameLevel1 := TRUE if EqpBayName has the String "LEVEL1"
  #
  # *Setup*:
  # * Product: RTDT-PRODUCT.01
  # * Tools: RTDT01
  # * PD: 1000.1000
  # * Scriptparameter:
  # * Carriercategory: BEOL
  # * Splitlot: No

  def test155_A3_checks_manlevelxfer
    # Start values
    shand = "A-VETHAND"
    state1 = "2NDP"
    state2 = "2WPR"
    tool2 = "RTDT01"
    tool = $sv.lot_info(@@lot).opEqps

    # Test Cleanup
    assert $rtdt.eqp_cleanup(tool + tool2.split)
    assert $rtdt.lot_carrier_cleanup(@@lot)
    assert $rtdt.stocker_cleanup()

    # Testcase 1: Manualler Xfer all Vert Stocker down
    assert_equal 0, $sv.eqp_status_change_req(shand, state2)
    sleep @@waittime
    list = $sv.rtd_dispatch_list(tool2)
    convert_list = $rtdt.change_list_to_hash(list, @@lot)
    $result_rtd << "#{__method__}_1: Lot #{@@lot} is not found on RTD List" unless (convert_list["Lot ID"] == @@lot)
    $result_rtd << "#{__method__}_1: A3 is Yes but should be no" unless (convert_list["A3"] == "N")
    $result_rtd << "#{__method__}_1: A3 Info is wrong '#{convert_list["A3Info"]}' instead of 'manueller Level XFER'" unless (convert_list["A3Info"] == "manueller Level XFER")
    assert_equal 0, $sv.eqp_status_change_req(shand, state1)

    # Testcase 2: A3 Check Stocker Reachable
    sleep @@waittime
    list = $sv.rtd_dispatch_list(tool2)
    convert_list = $rtdt.change_list_to_hash(list, @@lot)
    $result_rtd << "#{__method__}_2: Lot #{@@lot} is not found on RTD List" unless (convert_list["Lot ID"] == @@lot)
    $result_rtd << "#{__method__}_2: A3 is No but should be Yes" unless (convert_list["A3"] == "Y")
  end

  # *Testcase*: A3 Checks wait for other lots in QualKit (Code Nr: 45)
  #
  # *Number*: 160
  #
  # *Description*: Two lots in same carrier on different PDs.
  #
  # *Logic*:
  #
  # IF FULL wait AND wait THEN A3Info=� Waiting for other lots in QualKit� // wait is available and TRUE
  #
  # *Condition*:
  #
  # Wait :=
  # Wait on, that all lots from a carrier locate on the same operation (ope_no) and can be effect in Auto-3.
  # The lots must be have the same SAP Order Number -> and must be in the same QualKit. If the Lots located
  # on the same Route, they must be have the same OperationNumber and the same mainpd_id.
  #
  # mainpd_id := FRLOT_EQP -> LotMap -> mainpd_id
  #
  # SAPOrderNumber := PPTYINT_altKey_d_objmanager_pptyint_id{property_obj,�SAPOrderNumber�}.PPTYINT_ptr.value
  #
  # all_ope_no := FRLOT -> ope_no
  #
  # *Setup*:
  # * Product: RTDT-PRODUCT.01
  # * Tools: RTDA08
  # * PD: 1000.1000; 1000.1800
  # * Scriptparameter:
  # * Carriercategory: BEOL
  # * Splitlot: No

  def test160_A3_checks_qualkit
    # Start values
    opNo = "1000.1800"
    opNo2 = "1000.1000"
    tool = "RTDA08"
    tool2 = "RTDT01"
    tool3 = $sv.lot_info(@@lot).opEqps

    # Create 2 new lots
    cl = $sv.carrier_list(:category=>"BEOL", :status=>"AVAILABLE", :carrier =>"G%", :empty=>true)
    @@lot4 = $sv.new_lot_release(@@new_lot_params.merge(:nwafers=>5, :carrier=>cl.pop))
    assert @@lot4
    @@lot5 = $sv.new_lot_release(@@new_lot_params.merge(:nwafers=>5, :carrier=>cl.pop))
    assert @@lot5
    @@lots += [@@lot4, @@lot5]

    # Test Cleanup
    assert $rtdt.eqp_cleanup(tool.split + tool2.split + tool3)
    assert $rtdt.lot_carrier_cleanup(@@lots)
    assert $rtdt.stocker_cleanup()

    # Testcase 1: Two Lots in same Carrier on different PDs
    li4 = $sv.lot_info(@@lot4, :wafers=>true)
    wafers = li4.wafers
    slots = wafers.collect {|w| w.slot}.sort.last + 1
    assert_equal 0, $sv.wafer_sort_req(@@lot5, li4.carrier, :slot => slots)
    assert_equal 0, $sv.lot_opelocate(@@lot4, opNo2)
    assert_equal 0, $sv.lot_opelocate(@@lot5, opNo)
    sleep @@waittime
    list = $sv.rtd_dispatch_list(tool)
    convert_list = $rtdt.change_list_to_hash(list, @@lot5)
    $result_rtd << "#{__method__}_1: Lot #{@@lot5} is not found on RTD List" unless (convert_list["Lot ID"] == @@lot5)
    $result_rtd << "#{__method__}_1: A3 is Yes but should be no" unless (convert_list["A3"] == "N")
    $result_rtd << "#{__method__}_1: A3 Info is wrong '#{convert_list["A3Info"]}' should be include 'Waiting for other lots in QualKit'" unless (convert_list["A3Info"].split("|").member?("Waiting for other lots in QualKit"))

    # Testcase 2: Two Lots in same Carrier on different PDs and Different SAP Order Number
    assert_equal 0, $sv.user_parameter_change("Lot", @@lot5, "SAPOrderNumber", 12345678, 1, :datatype=>"INTEGER")
    sleep @@waittime
    list = $sv.rtd_dispatch_list(tool)
    convert_list = $rtdt.change_list_to_hash(list, @@lot5)
    $result_rtd << "#{__method__}_2: Lot #{@@lot5} is not found on RTD List" unless (convert_list["Lot ID"] == @@lot5)
    $result_rtd << "#{__method__}_2: A3 is Yes but should be no" unless (convert_list["A3"] == "N")
    $result_rtd << "#{__method__}_2: A3 Info is wrong '#{convert_list["A3Info"]}' instead of 'Keine ML-FOUPs erlaubt'" unless (convert_list["A3Info"] == "Keine ML-FOUPs erlaubt")
    assert_equal 0, $sv.user_parameter_change("Lot", @@lot5, "SAPOrderNumber", "", 3, :datatype=>"INTEGER")
    $sv.lot_opelocate(@@lot5, opNo2)

    # Testcase 3: Two Lots in same Carrier on same PD
    sleep @@waittime
    list = $sv.rtd_dispatch_list(tool2)
    convert_list = $rtdt.change_list_to_hash(list, @@lot4)
    $result_rtd << "#{__method__}_3: Lot #{@@lot4} is not found on RTD List" unless (convert_list["Lot ID"] == @@lot4)
    $result_rtd << "#{__method__}_3: A3 is No but should be Yes" unless (convert_list["A3"] == "Y")
    list = $sv.rtd_dispatch_list(tool2)
    convert_list = $rtdt.change_list_to_hash(list, @@lot5)
    $result_rtd << "#{__method__}_3: Lot #{@@lot5} is not found on RTD List" unless (convert_list["Lot ID"] == @@lot5)
    $result_rtd << "#{__method__}_3: A3 is No but should be Yes" unless (convert_list["A3"] == "Y")
  end

  # *Testcase*: A3 Checks SuperRocket (Code Nr: 47)
  #
  # *Number*: 165
  #
  # *Description*: The Lot has prio 0 and is shown as SuperRocket.
  #
  # *Logic*:
  #
  # IF FULL priority_class AND priority_class==0 AND NOT MATCH(OperationReportingType,"*+ASRKT*") THEN A3Info=�SuperRocket�
  #
  # *Condition*:
  #
  # FRLOT_EQP->LotMap->priority_class
  #
  # *Setup*:
  # * Product: RTDT-PRODUCT.01
  # * Tools: RTDT01
  # * PD: 1000.1000
  # * Scriptparameter:
  # * Carriercategory: BEOL
  # * Splitlot: No

  def test165_A3_checks_superrocket
    # Start values
    tool = $sv.lot_info(@@lot).opEqps
    tool2 = "RTDT01"

    # Test Cleanup
    assert $rtdt.eqp_cleanup(tool + tool2.split)
    assert $rtdt.lot_carrier_cleanup(@@lot)
    assert $rtdt.stocker_cleanup()

    # Testcase: Set Lot to prio 0 is same like super Rocket
    assert_equal 0, $sv.schdl_change(@@lot, :priority =>"0")
    sleep @@waittime
    list = $sv.rtd_dispatch_list(tool2)
    convert_list = $rtdt.change_list_to_hash(list, @@lot)
    $result_rtd << "#{__method__}_1: Lot #{@@lot} is not found on RTD List" unless (convert_list["Lot ID"] == @@lot)
    $result_rtd << "#{__method__}_1: A3 is Yes but should be no" unless (convert_list["A3"] == "N")
    $result_rtd << "#{__method__}_1: A3 Info is wrong '#{convert_list["A3Info"]}' instead of 'SuperRocket'" unless (convert_list["A3Info"] == "SuperRocket")
    assert_equal 0, $sv.schdl_change(@@lot, :priority =>"4")

    # Testcase 2: A3 Check not Super Rocket
    sleep @@waittime
    list = $sv.rtd_dispatch_list(tool2)
    convert_list = $rtdt.change_list_to_hash(list, @@lot)
    $result_rtd << "#{__method__}_2: Lot #{@@lot} is not found on RTD List" unless (convert_list["Lot ID"] == @@lot)
    $result_rtd << "#{__method__}_2: A3 is No but should be Yes" unless (convert_list["A3"] == "Y")
  end

  # *Testcase*: A3 Checks QTLCLineStop (Code Nr: 106)
  #
  # *Number*: 170
  #
  # *Description*: This testcase is a special test for the Fab Shut Down. For lots which are locate on a start from a QueueTime operation.
  #
  # *Logic*:
  #
  # IF FULL is_trigger_oper AND is_trigger_oper AND NULL is_target_oper THEN A3Info=�QT LC Line Stop�
  #
  # *Condition*:
  #
  # eqp_id == "A-QTSTOP" AND e10_state == "PRD"
  #
  # is_target_oper := target_mainpd_id and end_pd_id from the two upper named Files match to the zum Lot
  #
  # is_trigger_oper := trigger_mainpd_id and start_pd_id from the two upper named Files match to the zum Lot
  #
  # Files: ie_qt_erf_areas_C02.apf.def, ie_qt_std_areas_C02.apf.def
  #
  # *Setup*:
  # * Product: RTDT-PRODUCT.01
  # * Tools: RTDIB01, A-QTSTOP
  # * PD: 1000.1000, 1000.1100
  # * Scriptparameter:
  # * Carriercategory: BEOL
  # * Splitlot: No

  def test170_A3_checks_qtlc_line_stop
    # Start values
    opNo = "1000.1100"
    tool2 = "RTDIB01"
    qtstop = "A-QTSTOP"
    tool = $sv.lot_info(@@lot).opEqps

    # Test Cleanup
    assert $rtdt.eqp_cleanup(tool + tool2.split)
    assert $rtdt.lot_carrier_cleanup(@@lot)
    assert $rtdt.stocker_cleanup()

    # Testcase 1: Fab Shut Down
    assert_equal 0, $sv.lot_opelocate(@@lot, opNo)
    assert_equal 0, $sv.eqp_status_change_req(qtstop, "1PRD")
    sleep @@waittime
    list = $sv.rtd_dispatch_list(tool2)
    convert_list = $rtdt.change_list_to_hash(list, @@lot)
    $result_rtd << "#{__method__}_1: Lot #{@@lot} is not found on RTD List" unless (convert_list["Lot ID"] == @@lot)
    $result_rtd << "#{__method__}_1: A3 is Yes but should be no" unless (convert_list["A3"] == "N")
    $result_rtd << "#{__method__}_1: A3 Info is wrong '#{convert_list["A3Info"]}' instead of 'QT LC Line Stop'" unless (convert_list["A3Info"] == "QT LC Line Stop")
    assert_equal 0, $sv.eqp_status_change_req(qtstop, "2WPR")

    # Testcase 2: A3 Check Lot Reachable
    sleep @@waittime
    list = $sv.rtd_dispatch_list(tool2)
    convert_list = $rtdt.change_list_to_hash(list, @@lot)
    $result_rtd << "#{__method__}_2: Lot #{@@lot} is not found on RTD List" unless (convert_list["Lot ID"] == @@lot)
    $result_rtd << "#{__method__}_2: A3 is No but should be Yes" unless (convert_list["A3"] == "Y")
  end

  # *Testcase*: A3 Checks A3 Checks SJM (Code Nr: 42)
  #
  # *Number*: 175
  #
  # *Description*: All src_cast_id�s and dest_cast_id�s (from FSSORTJOB_COMPONENT) they have a Sort_Request_id.
  #
  # *Logic*:
  #
  # IF FULL sj_active AND sj_active THEN �FOUP hat SortJob?�
  #
  # *Condition*:
  #
  # sj_active := Ist TRUE, if dthe cast_id == cast_id_sortjob
  #
  # cast_id_sortjob := all src_cast_id�s and dest_cast_id�s (from FSSORTJOB_COMPONENT) they have a Sort_Request_id (MDS_GG  Datenbank)
  #
  # the cast_id�s from FSSORTJOB_COMPONENT are filter:
  #
  # component_job_status == "Wait To Executing" OR
  # component_job_status == "Xfer" OR
  # component_job_status == "Executing"
  #
  # component_job_status  := FSSORTJOB_COMPONENT.component_job_status
  #
  # *Setup*:
  # * Product: RTDT-PRODUCT.01
  # * Tools: RTDT01
  # * PD: 1000.1000
  # * Scriptparameter:
  # * Carriercategory: BEOL
  # * Splitlot: No

  def test175_A3_checks_sjm
    # Start values
    assert $rtdt.lot_carrier_cleanup(@@lot)
    sorter = "UTS001"
    tool = $sv.lot_info(@@lot).opEqps[0]
    eq = $sv.eqp_info(sorter)

    # Test Cleanup
    assert $rtdt.eqp_cleanup(tool)
    assert $rtdt.stocker_cleanup()
    assert $eis.restart_tool(sorter)

    # Testcase 1: Fab Shut Down
    li = $sv.lot_info(@@lot)
    carrier_empty = $sv.carrier_list(:category=>"BEOL", :carrier =>"G%", :empty=>true).first
    assert sj = $sv.sj_create(sorter, "P1", "P2", @@lot, carrier_empty)
    sleep @@waittime
    list = $sv.rtd_dispatch_list(tool)
    convert_list = $rtdt.change_list_to_hash(list, @@lot)
    $result_rtd << "#{__method__}_1: Lot #{@@lot} is not found on RTD List" unless (convert_list["Lot ID"] == @@lot)
    $result_rtd << "#{__method__}_1: A3 is Yes but should be no" unless (convert_list["A3"] == "N")
    $result_rtd << "#{__method__}_1: A3 Info is wrong '#{convert_list["A3Info"]}' instead of 'FOUP hat SortJob'" unless (convert_list["A3Info"] == "FOUP hat SortJob")
    $sv.sj_status_change(sorter,sj, "Error")
    $sv.sj_cancel(sj)

    # Testcase 2: A3 Check Lot Reachable
    sleep @@waittime
    list = $sv.rtd_dispatch_list(tool)
    convert_list = $rtdt.change_list_to_hash(list, @@lot)
    $result_rtd << "#{__method__}_2: Lot #{@@lot} is not found on RTD List" unless (convert_list["Lot ID"] == @@lot)
    $result_rtd << "#{__method__}_2: A3 is No but should be Yes" unless (convert_list["A3"] == "Y")
    assert $eis.restart_tool(sorter)
  end

  # ###################################################### Result and Delete Lots ###################################################

  def test999_results
    puts "Test OK" if $result_rtd.size == 0
    puts "Test NOK, #{$result_rtd.size} failures found" if $result_rtd.size > 0

    $result_rtd.each {|i|
      pp i
      if $result_rtd.index(i)%10 == 9
        print "show more Errors, press y?"
        res_input = gets
        break unless res_input.start_with?("y")
      end
    } if $result_rtd.size > 0
    $rtdt.delete_lots(@@lots)
  end
end
