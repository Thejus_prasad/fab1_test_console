=begin 
RTD rule test

(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.
    
Author: Thomas Klose, 2013-01-14

load_run_testcase "Test_QT_RTDRules", "itdc"

based on Dummy Test to verify the test framework works. Author: Steffen Steidten, 2012-08-02

test checks the rules QT_LOTS_WITHOUT_QT and QT_LOTS_WITH_QT.
=end

require "RubyTestCase"

class Test_QT_RTDRules < RubyTestCase
  Description = "RTD QT Rule Test"

  @@lotparams = { :product=>"A-QT-PRODUCT.01", :customer=>"gf", :route=>"A-QT-MAINPD.01", :sublottype=>"PROD-PO",
                  :stbdelay=>1, :comment=>"ERPITEM=3006AA-UA0,DPML=2.0,PLANNED_FSD=#{Time.now.strftime('%Y-%m-%d')}"}
  @@flags = "A U D".split    # Flags A, U and D (Add, Update, Delete)
  @@hld_opers =    "1000.1200 1000.1500 1000.1700".split # opNo for hold scenario
  @@fhpost_opers = "2000.1200 2000.1500".split # opNo for future hold post scenario
  @@fhpre_opers =  "3000.1200 3000.1400".split # opNo for future hold pre scenario
  @@rules = ["QT_LOTS_WITHOUT_QT", "QT_LOTS_WITH_QT"] 
  @@sleeptime = 120
  @@carrier = ""
                  
  def setup
    super
    assert $setup_ok, "test setup is not correct"
  end

  def test01_preparation
    #carrier = $sv.carrier_list(:empty=>true, :carrier=>"A%", :status=>"AVAILABLE")
    #@@carrier = carrier.pop
    carrier = $sv._get_empty_carrier("A%")
    assert carrier
    $sv.carrier_xfer_status_change(carrier, "MO", "UTSTO11")
    @@lotparams.merge!(:carrier=>carrier)
  end
  
  def test10_hold
  # rule for siview setup with HLD hold code
    tr = []
    opers = @@hld_opers
    
    lot = $sv.new_lot_release(@@lotparams)
    opers.each {|oper|
      prepare_lot(lot, oper)
      tr += [oper, get_qtflag(lot, @@flags[opers.index(oper)])].flatten
      sleep @@sleeptime unless oper == opers.last
      }
    $sv.delete_lot_family(lot, :delete=>false)
    
    $log.info("Testresult: #{tr.inspect}")
    assert tr.any? {|e| e =~ /A|U|D/}, "no flag found"   # checking result array for A, U or D. 
  end

  def test20_futurehold_post
  # rule for siview setup with FHD POST hold code
    tr = []
    opers = @@fhpost_opers
    
    lot = $sv.new_lot_release(@@lotparams)
    opers.each {|oper|
      prepare_lot(lot, oper)
      tr += [oper, get_qtflag(lot, @@flags[opers.index(oper)])].flatten
      sleep @@sleeptime unless oper == opers.last
      }
    $sv.delete_lot_family(lot, :delete=>false)
    
    $log.info("Testresult: #{tr.inspect}")
    assert tr.any? {|e| e =~ /A|U|D/}, "no flag found"   # checking result array for A, U or D. 
  end

  def test30_futurehold_pre
  # rule for siview setup with FHD PRE hold code
    tr = []
    opers = @@fhpre_opers
    
    lot = $sv.new_lot_release(@@lotparams)
    opers.each {|oper|
      prepare_lot(lot, oper)
      tr += [oper, get_qtflag(lot, @@flags[opers.index(oper)])].flatten
      sleep @@sleeptime unless oper == opers.last
      }
    $sv.delete_lot_family(lot, :delete=>false)
    
    $log.info("Testresult: #{tr.inspect}")
    assert tr.any? {|e| e =~ /A|U|D/}, "no flag found"   # checking result array for A, U or D. 
  end  
  
  def get_qtflag(lot, flag)
    # return the QT FLAG or E in case the lot didnt show up on dispatch list
    rule = flag == "A" ? "QT_LOTS_WITHOUT_QT" : "QT_LOTS_WITH_QT"
    tend = Time.now + @@sleeptime 
    res = nil
    while Time.now < tend
      res = $sv.rtd_dispatch_list(rule)
      resrow = res.rows.collect {|r| r if r[res.headers.index("LOT_ID")] == lot}.compact 
      return resrow.map{|r| r[res.headers.index("FLAG")]} unless resrow.empty?
      #return resrow[res.headers.index("FLAG")] unless resrow.empty?    # old, return first flag only
      sleep 5
    end
    return "E"
  end
  
  def prepare_lot(lot, oper)
    $sv.lot_opelocate(lot, oper)
    #$sv.carrier_xfer_status_change(@@carrier, "MO", "UTSTO11") # uncomment this line in case other tests impact qt rule test
    $sv.claim_process_lot(lot)
  end
end

