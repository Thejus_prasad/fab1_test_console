=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author:  sfrieske, 2017-05-17
Version: 1.0.0

History:
  2021-09-15 sfrieske, finally replaced $svtest by @@svtest, $sv by @@sv and $sm by @@sm
=end

require 'RubyTestCase'
require 'rtdtest'
require 'apfam'


# Testcases for RTD Timelink Gate Controller (Gecko) for Rework on Rework (MSR 1261968)
# Customer setup: C02-1910.002 -> R-B-2NB-BACKSIDEGRINDING-QT.01 -> R-B-2NB-PEEL.01
class RTD_Rule_GeckoRwk < RubyTestCase
  # copy of the default route with QT and required carrier category set
  @@rtd_defaults = {route: 'UTRT-GECKO.05', product: 'UT-PRODUCT-GECKO.05', carriers: 'EQA%',
    rule: 'QA_ie_qt_gate_controller_data', category: 'DISPATCHER/QA'}
  @@rtda3_defaults = {rule: 'QA_gecko_rule', category: 'DISPATCHER/QA'}
  @@amjob_handle = 'Job-121'    # http://f36apfd1:21241/jobs/details/Job-99
  @@amjob_name = '/QUEUE_TIME/ie_qt_gate_controller_gecko'
  # QT on main
  @@opNo_trigger = '1000.500'
  @@opNo_target = '1000.900'
  # 1st level rework
  @@rte_rwk1 = 'UTRT-GECKO-RWK1.01'   # after 'R-B-2NB-BACKSIDEGRINDING-QT.01'
  @@opNo_rwk1 = '1000.700'   # 1 after SM op!
  @@opNo_join1 = '1000.740'  # way after rwk start!
  # 2nd level rework, on @@rte_rwk1
  @@rte_rwk2 = 'UTRT-GECKO-RWK11.01'  # after 'R-B-2NB-PEEL.01.01'
  @@opNo_rwk2 = '1000.1100'  # 1 after SM op!
  @@opNo_join2 = '1000.1000'

  def self.after_all_passed
    @@testlots.each {|lot| @@sv.delete_lot_family(lot)}
  end


  def test00_setup
    $setup_ok = false
    #
    @@testlots = []
    # RTD client
    assert @@rtdtest = RTD::Test.new($env, @@rtd_defaults.merge(sv: @@sv))
    assert @@rtdtest.check_rule(parameters: ['lot_id', 'X']), 'wrong RTD rule called, setup issue?'
    @@sm = @@rtdtest.sm
    # # AM client, ensure AM job can be controlled
    # @@apfam = APF::AM.new($env)
    # assert details = @@apfam.job_details(@@amjob_handle)
    # assert_equal @@amjob_name, File.join(details[:folder], details[:name]), "wrong job handle"
    #
    #
    $setup_ok = true
  end

  def test401  # MSR 1261968
    assert lot = @@rtdtest.new_lot, "error creating test lot"
    @@testlots << lot
    # locate lot to trigger op and gatepass so it getes the Q-Time
    assert_equal 0, @@sv.lot_opelocate(lot, @@opNo_trigger)
    assert_equal 0, @@sv.lot_gatepass(lot)
    assert_equal 1, @@sv.lot_qtime_list(lot).size, "lot #{lot} has no QT"
    # 1st level rework
    assert_equal 0, @@sv.lot_opelocate(lot, @@opNo_rwk1)
    assert_equal 0, @@sv.lot_rework(lot, @@rte_rwk1, return_opNo: @@opNo_join1)
    # 2nd level rework
    ## QT ?
    assert_equal 0, @@sv.lot_opelocate(lot, @@opNo_rwk2)
    assert_equal 0, @@sv.lot_rework(lot, @@rte_rwk2, return_opNo: @@opNo_join2)   # fails, MES-3521 / MSR 1279300
  end
end
