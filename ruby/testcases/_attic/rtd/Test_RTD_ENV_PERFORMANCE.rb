=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

  load_run_testcase 'Test_RTD_ENV_PERFORMANCE', "itdc"

Author: Paul Peschel, 2012-12-11
Version: 1.0.0

=end

require "RubyTestCase"
require 'rtdruletest'
require 'rtdclient'

# Testcases for Check RTD in different Environment
# 
# Check all list from one environment and get Performance Data during call Dispatcher
#
# Rule have to send some return messages
#
# RTD Environment @@rtdenv is to set for the environment what is to test

class Test_RTD_ENV_PERFORMANCE < RubyTestCase
  
  @@rtdenv = "v7"
  @@inputfile = "log/Peformance_Test/b2bTestcases.csv"
  @@output_list = "log/Peformance_Test/dispatchlist_#{@@rtdenv}.csv"
  @@result_file = "log/Peformance_Test/PerformanceTestresults_#{@@rtdenv}.csv"
  @@result_file_qa = "log/Peformance_Test/resultqa_#{@@rtdenv}.csv"
  @@result_header = "#station called,report,category,additional parameter,max retries,rule,duration,rows,retries,passed"
  @@warmuptime = 600 #in seconds
  
  #struct for input parameter for rtd call
  InputParams = Struct.new(
    :station,
    :rule,
    :category1,
    :category2,
    :parameter,
    :wait,
    :retries,
    :exclude)
  
  def setup
    $rtdc = RTD::Client.new("#{$env}_#{@@rtdenv}") unless $rtdc and $rtdc.env == "#{$env}_#{@@rtdenv}"
    super
    ##assert $setup_ok, "test setup is not correct"
  end

  def test01_prepare_test
    @@tlist = File.readlines(@@inputfile).collect {|s|
      next if s.start_with?("#")
      a = s.chomp.split("|")
      InputParams.new(
        a[0].gsub(/station=/,""),
        a[1].gsub(/rule=/,""),
        a[2].gsub(/category=/,""),
        a[3].gsub(/category=/,""),
        a[4].gsub(/parameter=/,""),
        a[5].to_f,
        a[6] == "" ? 1 : a[6].to_i,
        (a[7].upcase.split(',') + ["AGE", "TIME"]).uniq
      )
    }.compact
  
    # create result files with headers
    open(@@output_list, "wb") {|fh| fh.write("Test starttime: " + Time.now.iso8601 + "\n")}
    open(@@result_file, "wb") {|fh| fh.write("Test starttime: " + Time.now.iso8601 + "\n")}
    open(@@result_file_qa, "wb") {|fh| fh.write("Test starttime: " + Time.now.iso8601 + "\n")}
    write_result(@@result_file, @@result_header)
  end
  
  def test10_warmup
    # Start Values
    wutime = Time.now + @@warmuptime
    queue = []
    # call each Rule to fill dispatcher cache
    @@tlist.each {|k|
      k.retries.times{ queue << k }
    }
    queue = queue.flatten.shuffle
    
    while Time.now < wutime
      queue.push(queue.shift)
      q = queue.first
      list = get_rtd_list(q.station, q.rule, q.category1)
    end
  end
      
  def test20_compare_rules
    # test all rules from inputfile and compare
    @@tlist.each {|k|
      list = nil
      ntry = 0
      ok = false
      params_ary = k.parameter.gsub(/\$/,'').split(/[=,]/) # gsub(\$) deletes $ in string params. split (/[=,]/) splits at "=" and ","
      
      k.retries.times {
        ntry += 1
        errors = false
        list = get_rtd_list(k.station, k.rule, k.category1, params_ary)
        sleep k.wait
        
        # check environment
        row = list.rows ? list.rows.size: 0
        errors = true if row == 0
        if list.headers.first.start_with?("#error")
          write_result(@@result_file_qa, "error calling rule #{k.station} in #{$rtdc.env}: #{list.headers.inspect}")
          errors = true
          ok = "failed"
        else
          ok = true unless errors
        end
        # write rule test result, if rows == nil return 0
        # if list values duration, rule, rows are empty(nil), the row duration ist set to '0' and rule is set to 'rule not found'
        row = list.rows ? list.rows.size: 0
        row = "error" if list.headers.first.start_with?("#error")
        duration = list.process_duration ? list.process_duration: 0
        rule = list.rule ? list.rule: "rule not found"
        res = [k.station, k.rule, k.category1, params_ary.join("|"), k.retries, rule, duration, row, ntry, ok]
        write_result(@@result_file, res* ', ')
      }
      unless ok
        # write complete dispatch list on error
        write_result(@@output_list, list.headers.unshift("Station Called #{k.station}").join(', '))
        list.rows.each {|r| write_result(@@output_list, r.unshift(k.station).join(', '))} if list.rows
      end
    }
  end
  
  def get_rtd_list(station=nil, rule=nil, cat=nil, parameters=nil)
    # get dispatch lists
    list = nil
    cat="/DISPATCHER/RULES" if cat == ""
    list = $rtdc.dispatch_list(station, :report=>rule, :category=>cat, :parameters=>parameters)
    return list
  end
  
  def write_result(filename, s, mode="ab")
    open(filename, mode) {|fh| fh.write(s + "\n")}
  end
end