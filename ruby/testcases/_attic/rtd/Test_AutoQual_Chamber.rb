=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

  load_run_testcase 'Test_AutoQual_Chamber', "itdc"

Author: Paul Peschel, 2013-06-18
Version: 1.0.0

=end

require "RubyTestCase"
require 'rtdruletest'
require 'rtdclient'

# Testcases for RTD
#
# If run testcases alone over "load_run_testcase 'Test_AutoQual_Chamber.rb', "itdc", :tp => /20/" you must do first:
#
# 1. @@lot = ""
#
# 2. Call "test01_prepare_test", because without that, you have no Lot
#
# 3. Or call for each Testcase also test "test01_prepare_test" means "load_run_testcase 'Test_AutoQual_Chamber.rb', "itdc", :tp => /01|20/"

class Test_AutoQual_Chamber < RubyTestCase

  @@waittime = 10
  @@lot = ""
  @@tool = "RTDQ01"
  @@route = "P-RTDQ-MAINPD01.01"
  $result_rtd = []
  @@prodg1 = "RTDQ-PRODUCT.01  RTDQ-PRODUCT.02  RTDQ-PRODUCT.03".split
  @@prodg4 = "RTDQ-PRODUCT.04  RTDQ-PRODUCT.05  RTDQ-PRODUCT.06".split
  @@prodg7 = "RTDQ-PRODUCT.07  RTDQ-PRODUCT.08  RTDQ-PRODUCT.09".split
  @@opno = "1000.1000 1000.1100 1000.1200".split
  @@ch1 = ["2WPR 2WPR",
          "5CNS 2WPR",
          "5MTN 2WPR",
          "INH_2WPR 2WPR"]
  @@ch2 = ["2WPR 2WPR",
          "5CNS 5CNS",
          "5MTN 5MTN",
          "2WPR 5CNS",
          "2WPR 5MTN",
          "5MTN 5CNS",
          "INH_2WPR 2WPR"]
  @@ch3 = ["2WPR 2WPR",
          "5CNS 5CNS",
          "5MTN 5MTN",
          "2WPR 5CNS",
          "2WPR 5MTN",
          "5MTN 5CNS",
          "INH_2WPR 2WPR",
          "INH_2WPR 5CNS",
          "INH_2WPR 5MTN"]
  @@a3 = ["YNN0YNNNNN0YNNYYNY0N",
          "YNY0YNYNYN0YNYYYYY0Y",
          "YNN0YNNNNN0YNNYYNY0N",
          "YNN0YNNNNN0YNNYYNY0N",
          "YNN0YNNNNN0YNNYYNY0N",
          "YNN0YNNNNN0YNNYYNY0N",
          "YNN0YNNNNN0YNNYYNY0N",
          "YNY0YNYNYN0YNYYYYY0Y",
          "YNY0YNYNYN0YNYYYYY0Y"]
  @@chall = [@@ch1] + [@@ch2] + [@@ch3]
  @@prdall = [@@prodg1] + [@@prodg4] + [@@prodg7]
  # Testscenario array
  @@sc = []
  (@@opno * 3).each_with_index {|opno, opidx| 
    @@sc << [opno] + @@prdall[opidx/3].collect{|prd| [prd] + [@@chall[@@prdall[opidx/3].index(prd)]]}
  }

  def setup
    super
    $rtdt = RTD::RuleTest.new($env, :sv=>$sv) unless $rtdt and $rtdt.env == $env
    $rtdc = RTD::Client.new($env) unless $rtdc and $rtdc.env == $env
    assert $setup_ok, "test setup is not correct"
  end
  
  # ###################################################### Setup des Tests ##########################################################
  #
  # Prepare all Equipment and Lots for the Test
  #
  # See also testsetup in Excel File Testsetup.xls in sheet "Testsetup_AutoQual_Chamber".
  # Here described all important stuff for the tests in this File.
  #
  # http://myteamsdrs2/sites/FICSQA/_layouts/xlviewer.aspx?id=/sites/FICSQA/Documentation/Technical%20Documentation/RTD/Testcases_QA_DEFAULT_WHAT_NEXT.xlsx
  
def test01_prepare_test
      # Return Value
      ok = true
      
      # Expected Values for Lot
      user = "X-UNITTEST"
      opNo = "1000.1000"

      # tool Cleanup
      ok &= $rtdt.eqp_cleanup(@@tool)
      ok &= $rtdt.stocker_cleanup()
      
      # Create Lot
      @@lot = $rtdt.create_lots_rtd(@@prodg1[0], :route=>@@route, :sublottype=>"Equipment Monitor", :count=>1).join
      
      # Change Lot to PD and other Stuff
      li = $sv.lot_info(@@lot)
      ok &= ($sv.lot_hold_release(@@lot, nil) == 0) if li.status == "ONHOLD"
      ok &= ($sv.lot_opelocate(@@lot, opNo) == 0) unless li.opNo == opNo
      ok &= ($sv.carrier_reserve_cancel(li.carrier) == 0) unless $sv.carrier_status(li.carrier).xfer_user == ""
      unless li.xfer_status == "SI" && li.stocker == "UTSTO11"
        ok &= ($sv.carrier_xfer_status_change(li.carrier, "SO", "INTER1") == 0)
        ok &= ($sv.carrier_xfer_status_change(li.carrier, "MI", "UTSTO11") == 0)
      end
      
      # Cancel setup if user is wrong
      setup_correct = false unless $sv.user == user and ok == true
      
      # Check Setup is correct
      if setup_correct == false
        $log.info("Setup has wrong User or wrong Setup")
      end
  end
  
  ####################################################### Testcases for RTD #########################################################
  
  # *Testcase*: AutoQual auf Kammer Level (RCR711974)
  #
  # *Number*: 20
  #
  # *Description*: Check A3 Code and A3 Info with different Chamber states.
  #
  # *Logic*:
  #
  # See File "Testplan_AutoQual_RCR711974.xlsx"
  #
  # *Condition*:
  #
  # See File "Testplan_AutoQual_RCR711974.xlsx". See also Module Combination
  #
  # *Setup*:
  # * Product: RTDQ-PRODUCT.01, RTDQ-PRODUCT.02, RTDQ-PRODUCT.03
  # * Tools: RTDQ01
  # * PD: 1000.1000, 1000.1100, 1000.1200
  # * Scriptparameter:
  # * Carriercategory: FEOL
  # * Splitlot: No
  
  def test20_AutoQual
    # Test for only 1 scenario
    @@preselect = nil
    if @@preselect != nil
      @@sc = [@@sc[@@preselect]]
      @@a3 = [@@a3[@@preselect]]
    end
    ## test
    # a3test = ""
    # i=0
    # @@sc[sc][1..-1].each_with_index{|prd, prdidx|
      # prd[-1].each_with_index{|ch, chidx|
        # #helper = [0,4,11]
        # puts ("prd #{prdidx}, ch #{chidx}, a3idx #{[helper[prdidx] + chidx]}, a3 #{@@a3[sc].split('')[helper[prdidx] + chidx]}, i #{i}, @@a3 #{@@a3[0].split('')[i]}")
        # i += 1
        # a3test = a3test + @@a3[sc].split('')[[0,4,11][prdidx] + chidx]}}
              
    # @@a3 = ["YNN0YNNNNN0YNNYYNY0N"]
    
    # Testcase AutoQual for Chamber
    @@sc.each_with_index {|sc, scidx|
      prep_lot_op(sc[0])
      sc[1..-1].each_with_index {|prd, prdidx|
        prep_lot_schdl(prd[0])
        prd[-1].each_with_index {|ch, chidx|
          prep_chamber(ch)
          sleep @@waittime
          list = $sv.rtd_dispatch_list(@@tool)
          convert_list = $rtdt.change_list_to_hash(list, @@lot)
          a3code = @@a3[scidx].split('')[[0,4,11][prdidx] + chidx]
          #puts ("sc #{scidx+1}, lotop #{sc[0]}, prd #{prd[0]}, ch #{ch}")#, a3idx #{[helper[prdidx] + chidx]}, a3 #{@@a3[sc].split('')[helper[prdidx] + chidx]}, i #{i}, @@a3 #{@@a3[0].split('')[i]}")
          $log.info("RTD #{convert_list["A3"]} expected result #{a3code}")
          if a3code == "0"
            $result_rtd << "#{__method__}: Lot is on List. Scenario is #{scidx+1} and chamber combination is #{ch} in #{prd[0]}. Operation type is #{convert_list["OperationReportingType"]} and mcrecipe_A3 is #{convert_list["mrecipe_auto3"]}" if (convert_list["Lot ID"] == @@lot)
          else
            $result_rtd << "#{__method__}: Lot is on list A3 state is #{convert_list["A3"]} but must be A3 #{a3code}. Scenario is #{scidx+1} and chamber combination is #{ch} in #{prd[0]}. Operation type is #{convert_list["OperationReportingType"]} and mcrecipe_A3 is #{convert_list["mrecipe_auto3"]}" unless (convert_list["A3"] == a3code)
          end
        }
      }
    }
  end
  
  def prep_lot_op(op)
    assert_equal 0, $sv.lot_opelocate(@@lot, op)
  end
  
  def prep_lot_schdl(prd)
    assert_equal 0, $sv.schdl_change(@@lot, :product=>prd, :route=>@@route)
  end
    
  def prep_chamber(cc)
     pm1, pm2 =cc.split
     # release inhibit
     assert_equal 0, $sv.inhibit_cancel("Chamber", @@tool, :attrib=>"PM1")
     # set inhibit
     if pm1.start_with?("INH")
        assert_equal 0, $sv.inhibit_entity("Chamber", @@tool, :attrib=> "PM1")
        pm1 = pm1.split("_").last
     end
     # change chamber states
     $sv.chamber_status_change_req(@@tool,"PM1","5DLY") if pm1.start_with?("5")
     $sv.chamber_status_change_req(@@tool,"PM2","5DLY")  if pm2.start_with?("5")
     eq = $sv.eqp_info(@@tool)
     $sv.chamber_status_change_req(@@tool,"PM1", pm1) unless eq.chambers[0].status.split(".")[-1] == pm1
     $sv.chamber_status_change_req(@@tool,"PM2", pm2) unless eq.chambers[1].status.split(".")[-1] == pm2
  end
  
  def test999_results
    puts "Test OK" if $result_rtd.size == 0
    puts "Test NOK, #{$result_rtd.size} failures found" if $result_rtd.size > 0
    
    $result_rtd.each {|i|
      pp i
      if $result_rtd.index(i)%10 == 9
        print "show more Errors, press y?"
        res_input = gets
        break unless res_input.start_with?("y")
      end
    } if $result_rtd.size > 0
    $rtdt.delete_lots([@@lot])
  end
end