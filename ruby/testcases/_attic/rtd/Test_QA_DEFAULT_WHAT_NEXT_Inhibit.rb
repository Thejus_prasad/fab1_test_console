=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

	load_run_testcase 'Test_QA_DEFAULT_WHAT_NEXT_Inhibit', "itdc"
	
Author: Paul Peschel, 2012-07-12
Version: 1.0.0

=end

require "RubyTestCase"
require 'rtdruletest'
require 'rtdclient'

# Testcases for RTD
#
# If run testcases alone over "load_run_testcase 'Test_QA_DEFAULT_WHAT_NEXT_Inhibit', "itdc", :tp => /20/" you must do first:
#
# 1. @@lot = ""
#
# 3. @@lots = ""
#
# 4. Call "test01_prepare_test", because without that, you have no Lot
#
# 5. Or call for each Testcase also test "test01_prepare_test" means "load_run_testcase 'Test_QA_DEFAULT_WHAT_NEXT', "itdc", :tp => /01|20/"

class Test_QA_DEFAULT_WHAT_NEXT_Inhibit < RubyTestCase

  @@waittime = 40

  def setup
    super
    $rtdt = RTD::RuleTest.new($env, :sv=>$sv) unless $rtdt and $rtdt.env == $env
    $rtdc = RTD::Client.new($env) unless $rtdc and $rtdc.env == $env
    assert $setup_ok, "test setup is not correct"
  end

  # ###################################################### Setup des Tests ##########################################################
  #
  # Prepare all Equipment and Lots for the Test
  #
  # See also testsetup in excel file Testsetup.xls in sheet "Testsetup_QA_DEFAULT_WHAT_NEXT". 
  # Here described all important stuff for the tests in this File.
  #
  # http://myteamsdrs2/sites/FICSQA/_layouts/xlviewer.aspx?id=/sites/FICSQA/Documentation/Technical%20Documentation/RTD/Testcases_QA_DEFAULT_WHAT_NEXT.xlsx

  def test01_prepare_test
    lot_count = 1
    @@lots = $rtdt.setup_for_test(lot_count)
    @@lot = @@lots[0]
    return @@lot + @@lots.to_s
  end

  ####################################################### Testcases for RTD #########################################################

  # *Testcase*: Inhibits
  # * Testcase 1: for Equipment Inhibit
  # * Testcase 2: for Equipment and Machine Recipe Inhibit
  # * Testcase 3: for Equipment and ProcessDefinition Inhibit
  # * Testcase 4: for Equipment and Chamber Inhibit
  # * Testcase 5: for Equipment and Chamber and Recipe Inhibit
  # * Testcase 6: for Equipment and Chamber and Product Inhibit
  # * Testcase 7: Check A3 = Y without Inhibit (must be visible on List)
  # * Testcase 8: for Product Group and	Process Definition Inhibit
  # * Testcase 9: for Product Group and	Process Definition and Recipe Inhibit
  # * Testcase 10: for Product Group and Process Definition and Equipment Inhibit
  # * Testcase 11: for Product Group and Process Definition and Equipment and Chamber Inhibit
  # * Testcase 12: for Product Group and Equipment Inhibit
  # * Testcase 13: for Product Group and Equipment and Chamber and Recipe Inhibit
  # * Testcase 14: for Product Group and Equipment and Recipe Inhibit
  # * Testcase 15: for Product Group Inhibit
  # * Testcase 16: for Machine Recipe Inhibit
  # * Testcase 17: for Equipment Inhibit and Inhibit Exceptions
  # * Testcase 18: for Product Inhibit
  # * Testcase 19: for Route Inhibit
  # * Testcase 20: for Route and Operation Inhibit
  # * Testcase 21: for Route and Operation and Product Inhibit
  # * Testcase 22: for Route and Operation and Product and Equipment Inhibit
  # * Testcase 23: for Module PD Inhibit  
  # * Testcase 24: for Route and Operation and Equipment and Chamber
  # * Testcase 25: for Equipment and Chamber and Product Group
  # * Testcase 26: for Equipment and Module PD and Chamber
  # * Testcase 27: for Technologie and Equipment
  # * Testcase 28: for Technologie and Equipment and Chamber
  # * Testcase 29: for Technologie and Module PD and Equipment
  # * Testcase 30: for Technologie and Module PD and Equipment and Chamber
  # * Testcase 31: for Technologie and Module PD
  # * Testcase 32: for Reticle
  # * Testcase 33: for Reticle and Equipment
  # * Testcase 34: for Reticle Group
  # * Testcase 35: for Reticle Group and Equipment
  # * Testcase 36: for Logical Recipe
  # * Testcase 37: for Logical Recipe and Equipment
  # * Testcase 38: for Logical Recipe and Equipment and Chamber
  # * Testcase 39: for Route and Operation Product and Equipment and Chamber
  #
  # Testcases with Chamber running only on RTDA08 and RTDA10. All other Testcases run on all tools. Beacause RTDA09 has other rule.
  #
  # *Number*: 45
  #
  # *Description*: Filter from inhibit lots (check diverse inhibits)
  #
  # *Logic*:  
  #
  # Filter from inhibit Lots (check diverse inhibits)
  #
  # *Condition*:
  #
  # For Inhibit Chamber tests (4,5,6,11,13) set the tool and chamber in "f36asd14:/apf/apf_itdc/models/fab1/class/ie_modulecombinations_version.txt"
  #
  # with value "P-RTD|RTDA06|Test|RTDA08|PM1:PM2|1" and "P-RTD|RTDA06|Test|RTDA09|PM1:PM2|1"
  #
  # product group is defined: IF class_name == "Product Group" THEN entity_id ELSE ""
  #
  # entity_id and the class_name come from table FRENTINHBT_ENTTY 
  #
  # rule: RTDA08 (QA_DEFAULT_WHAT_NEXT_CLUSTER) and RTDA09 (QA_DEFAULT_WHAT_NEXT) and RTDA10 (QA_DEFAULT_WHAT_NEXT_alc)
  # 
  # RTDA10 have to be as Chamber ID: STP and TRK
  #
  # lcrecipe_id, name (set to CoatOnly), lot_id, eqp_id from table FRMRCP_EQP the value have to be:
  #
  # coat_flag_recipe = FRLRCP_DSET_RPARM{FRLRCP_d_thesystemkey, STRING(d_seqno),name}.rparm_default
  #
  # IF coat_flag_recipe=="T" THEN "TRK" ELSE "TRK,STP"
  #
  # *Setup*: 
  # * Product: RTDT-PRODUCT.01
  # * Tools: RTDA08, RTDA09, RTDA10
  # * PD: 1000.1800
  # * modulepd: P-RTDT-MOD01.01
  # * Recipe: P-RTD.RTDA06.01
  # * route: P-RTDT-MAINPD01.01
  # * Scriptparameter:
  # * Carriercategory: BEOL
  # * Splitlot: No

  def test45_Inhibits
    # Start values    
    mrecipe = "P-RTD.RTDA06.01"
    route = "P-RTDT-MAINPD01.01"
    modulepd = "P-RTDT-MOD01.01"
    opNo = "1000.1800"
    tool = "RTDA08 RTDA10".split
    
    # Test Cleanup
    assert $rtdt.lot_carrier_cleanup(@@lot)
    $sv.lot_opelocate(@@lot, opNo)
    tools = $sv.lot_info(@@lot).opEqps
    assert $rtdt.eqp_cleanup(tools)
    assert $rtdt.stocker_cleanup()

    # get Infos from PD
    li = $sv.lot_info(@@lot)
    pd = li.op
    product = li.product
    productg = li.productgroup
    
    # Testcase 1: for Equipment Inhibit
    tools.each {|t|
      assert_equal 0, $sv.inhibit_entity(["Equipment"], [t])}
    sleep @@waittime
    tools.each {|t|
      list = $sv.rtd_dispatch_list(t)
      convert_list = $rtdt.change_list_to_hash(list, @@lot)
      $result_rtd << "#{__method__}_1: Tool #{t} is Inhibit and RTD found the Lot" if (convert_list["Lot ID"] == @@lot)
      assert_equal 0, $sv.inhibit_cancel(["Equipment"], [t])}
    sleep @@waittime
    tools.each {|t|
      list = $sv.rtd_dispatch_list(t)
      convert_list = $rtdt.change_list_to_hash(list, @@lot)
      $result_rtd << "#{__method__}_1: Tool #{t} is not Inhibit and RTD found no Lot" unless (convert_list["Lot ID"] == @@lot)}

    # Testcase 2: for Equipment and Machine Recipe Inhibit
    tools.each {|t|
      assert_equal 0, $sv.inhibit_entity(["Equipment", "Machine Recipe"], [t, mrecipe])}
    sleep @@waittime
    tools.each {|t|
      list = $sv.rtd_dispatch_list(t)
      convert_list = $rtdt.change_list_to_hash(list, @@lot)
      $result_rtd << "#{__method__}_2: Tool #{t} is Inhibit with combination Recipe #{mrecipe} and RTD found Lot" if (convert_list["Lot ID"] == @@lot)
      assert_equal 0, $sv.inhibit_cancel(["Equipment", "Machine Recipe"], [t, mrecipe])}
    sleep @@waittime
    tools.each {|t|
      list = $sv.rtd_dispatch_list(t)
      convert_list = $rtdt.change_list_to_hash(list, @@lot)
      $result_rtd << "#{__method__}_2: Tool #{t} is not Inhibit with combination Recipe #{mrecipe} and RTD found no Lot" unless (convert_list["Lot ID"] == @@lot)}

    # Testcase 3: for Equipment and ProcessDefinition Inhibit
    tools.each {|t|
      assert_equal 0, $sv.inhibit_entity(["Equipment", "Process Definition"], [t, pd])}
    sleep @@waittime
    tools.each {|t|
      list = $sv.rtd_dispatch_list(t)
      convert_list = $rtdt.change_list_to_hash(list, @@lot)
      $result_rtd << "#{__method__}_3: Tool #{t} is Inhibit with combination Process Definition #{pd} and RTD found Lot" if (convert_list["Lot ID"] == @@lot)
      assert_equal 0, $sv.inhibit_cancel(["Equipment", "Process Definition"], [t, pd])}
    sleep @@waittime
    tools.each {|t|
      list = $sv.rtd_dispatch_list(t)
      convert_list = $rtdt.change_list_to_hash(list, @@lot)
      $result_rtd << "#{__method__}_3: Tool #{t} is not Inhibit with combination Process Definition #{pd} and RTD found no Lot" unless (convert_list["Lot ID"] == @@lot)}

    # Testcase 4: Chamber Inhibit for Equipment
    tool.each {|t|
      eq = $sv.eqp_info(t)
      eq.chambers.each {|ch|
        assert_equal 0, $sv.inhibit_entity("Chamber", t, :attrib=> ch.chamber)}}
    sleep @@waittime
    tool.each {|t|
      list = $sv.rtd_dispatch_list(t)
      convert_list = $rtdt.change_list_to_hash(list, @@lot)
      $result_rtd << "#{__method__}_4: Tool #{t} is Inhibit with combination EQP and Chamber and RTD found Lot" if (convert_list["Lot ID"] == @@lot)
      eq = $sv.eqp_info(t)
      eq.chambers.each {|ch|
      assert_equal 0, $sv.inhibit_cancel("Chamber", t, :attrib=> ch.chamber)}}
    sleep @@waittime
    tool.each {|t|
      list = $sv.rtd_dispatch_list(t)
      convert_list = $rtdt.change_list_to_hash(list, @@lot)
      $result_rtd << "#{__method__}_4: Tool #{t} is not Inhibit with combination EQP and Chamber and RTD found no Lot" unless (convert_list["Lot ID"] == @@lot)}

    # Testcase 5: Chamber and Recipe Inhibit for Equipment
    tool.each {|t|
      eq = $sv.eqp_info(t)
      eq.chambers.each {|ch|
       assert_equal 0, $sv.inhibit_entity(["Chamber", "Machine Recipe"], [t, mrecipe], :attrib=>[ch.chamber, ""])}}
    sleep @@waittime
    tool.each {|t|
      list = $sv.rtd_dispatch_list(t)
      convert_list = $rtdt.change_list_to_hash(list, @@lot)
      $result_rtd << "#{__method__}_5: Tool #{t} is Inhibit with combination EQP and Chamber and Recipe and RTD found Lot" if (convert_list["Lot ID"] == @@lot)
      eq = $sv.eqp_info(t)
      eq.chambers.each {|ch|
      assert_equal 0, $sv.inhibit_cancel(["Chamber", "Machine Recipe"], [t, mrecipe], :attrib=>[ch.chamber, ""])}}
    sleep @@waittime
    tool.each {|t|
      list = $sv.rtd_dispatch_list(t)
      convert_list = $rtdt.change_list_to_hash(list, @@lot)
      $result_rtd << "#{__method__}_5: Tool #{t} is not Inhibit with combination EQP and Chamber and Recipe and RTD found no Lot" unless (convert_list["Lot ID"] == @@lot)}

    # Testcase 6: Chamber and Product Inhibit for Equipment
    tool.each {|t|
      eq = $sv.eqp_info(t)
      eq.chambers.each {|ch|
        assert_equal 0, $sv.inhibit_entity(["Chamber", "Product Specification"], [t, product], :attrib=>[ch.chamber, ""])}}
    sleep @@waittime
    tool.each {|t|
      list = $sv.rtd_dispatch_list(t)
      convert_list = $rtdt.change_list_to_hash(list, @@lot)
      $result_rtd << "#{__method__}_6: Tool #{t} is Inhibit with combination EQP and Chamber and Product and RTD found Lot" if (convert_list["Lot ID"] == @@lot)
      eq = $sv.eqp_info(t)
      eq.chambers.each {|ch|
        assert_equal 0, $sv.inhibit_cancel(["Chamber", "Product Specification"], [t, product], :attrib=>[ch.chamber, ""])}}
    sleep @@waittime
    tool.each {|t|
      list = $sv.rtd_dispatch_list(t)
      convert_list = $rtdt.change_list_to_hash(list, @@lot)
      $result_rtd << "#{__method__}_6: Tool #{t} is not Inhibit with combination EQP and Chamber and Product and RTD found no Lot" unless (convert_list["Lot ID"] == @@lot)}

    # Testcase 7: Check A3 = Y without Inhibit
    sleep @@waittime
    tools.each {|t|
      list = $sv.rtd_dispatch_list(t)
      convert_list = $rtdt.change_list_to_hash(list, @@lot)
      $result_rtd << "#{__method__}_7: Lot #{@@lot} is not found on RTD List" unless (convert_list["Lot ID"] == @@lot)
      $result_rtd << "#{__method__}_7: A3 is No but should be Yes" unless (convert_list["A3"] == "Y")}

    # Testcase 8: for Product Group and	Process Definition
    assert_equal 0, $sv.inhibit_entity(["Product Group", "Process Definition"], [productg, pd])
    sleep @@waittime
    tools.each {|t|
      list = $sv.rtd_dispatch_list(t)
      convert_list = $rtdt.change_list_to_hash(list, @@lot)
      $result_rtd << "#{__method__}_8: Product Group #{productg} is Inhibit with combination Process Definition #{pd} and RTD found the Lot" if (convert_list["Lot ID"] == @@lot)}
    assert_equal 0, $sv.inhibit_cancel(["Product Group", "Process Definition"], [productg, pd])
    sleep @@waittime
    tools.each {|t|
      list = $sv.rtd_dispatch_list(t)
      convert_list = $rtdt.change_list_to_hash(list, @@lot)
      $result_rtd << "#{__method__}_8: Product Group #{productg} is not Inhibit with combination Process Definition #{pd} and RTD found no Lot" unless (convert_list["Lot ID"] == @@lot)}

    # Testcase 9: for Product Group and	Process Definition and Recipe Inhibit
    assert_equal 0, $sv.inhibit_entity(["Product Group", "Process Definition", "Machine Recipe"], [productg, pd, mrecipe])
    sleep @@waittime
    tools.each {|t|
      list = $sv.rtd_dispatch_list(t)
      convert_list = $rtdt.change_list_to_hash(list, @@lot)
      $result_rtd << "#{__method__}_9: Product Group #{productg} is Inhibit with combination Process Definition #{pd} and Recipe #{mrecipe} and RTD found the Lot" if (convert_list["Lot ID"] == @@lot)}
    assert_equal 0, $sv.inhibit_cancel(["Product Group", "Process Definition", "Machine Recipe"], [productg, pd, mrecipe])
    sleep @@waittime
    tools.each {|t|
      list = $sv.rtd_dispatch_list(t)
      convert_list = $rtdt.change_list_to_hash(list, @@lot)
      $result_rtd << "#{__method__}_9: Product Group #{productg} is not Inhibit with combination Process Definition #{pd} and Recipe #{mrecipe} and RTD found no Lot" unless (convert_list["Lot ID"] == @@lot)}

    # Testcase 10: for Product Group and Process Definition and Equipment Inhibit
    tools.each {|t|
      assert_equal 0, $sv.inhibit_entity(["Product Group", "Process Definition", "Equipment"], [productg, pd, t])}
    sleep @@waittime
    tools.each {|t|
      list = $sv.rtd_dispatch_list(t)
      convert_list = $rtdt.change_list_to_hash(list, @@lot)
      $result_rtd << "#{__method__}_10: Product Group #{productg} is Inhibit with combination Process Definition #{pd} and Tool #{t} and RTD found the Lot" if (convert_list["Lot ID"] == @@lot)
      assert_equal 0, $sv.inhibit_cancel(["Product Group", "Process Definition", "Equipment"], [productg, pd, t])}
    sleep @@waittime
    tools.each {|t|
      list = $sv.rtd_dispatch_list(t)
      convert_list = $rtdt.change_list_to_hash(list, @@lot)
      $result_rtd << "#{__method__}_10: Product Group #{productg} is not Inhibit with combination Process Definition #{pd} and Tool #{t} and RTD found no Lot" unless (convert_list["Lot ID"] == @@lot)}

    # Testcase 11: for Product Group and Process Definition and Equipment and Chamber Inhibit
    tool.each {|t|
      eq = $sv.eqp_info(t)
      eq.chambers.each {|ch|
        assert_equal 0, $sv.inhibit_entity(["Chamber", "Product Group", "Process Definition", ], [t, productg, pd], :attrib=>[ch.chamber, ""])}}
    sleep @@waittime
    tool.each {|t|
      list = $sv.rtd_dispatch_list(t)
      convert_list = $rtdt.change_list_to_hash(list, @@lot)
      $result_rtd << "#{__method__}_11: Product Group #{productg} is Inhibit with combination Process Definition #{pd}, Tool #{t} and Chamber and RTD found the Lot" if (convert_list["Lot ID"] == @@lot)
      eq = $sv.eqp_info(t)
      eq.chambers.each {|ch|
      assert_equal 0, $sv.inhibit_cancel(["Chamber", "Product Group", "Process Definition"], [t, productg, pd], :attrib=>[ch.chamber, ""])}}
    sleep @@waittime
    tool.each {|t|
      list = $sv.rtd_dispatch_list(t)
      convert_list = $rtdt.change_list_to_hash(list, @@lot)
      $result_rtd << "#{__method__}_11: Product Group #{productg} is not Inhibit with combination Process Definition #{pd}, Tool #{t} and Chamber and RTD found no Lot" unless (convert_list["Lot ID"] == @@lot)}

    # Testcase 12: for Product Group and Equipment Inhibit
    tools.each {|t|
      assert_equal 0, $sv.inhibit_entity(["Product Group", "Equipment"], [productg, t])}
    sleep @@waittime
    tools.each {|t|
      list = $sv.rtd_dispatch_list(t)
      convert_list = $rtdt.change_list_to_hash(list, @@lot)
      $result_rtd << "#{__method__}_12: Product Group #{productg} is Inhibit with combination Tool #{t} and RTD found the Lot" if (convert_list["Lot ID"] == @@lot)
      assert_equal 0, $sv.inhibit_cancel(["Product Group", "Equipment"], [productg, t])}
    sleep @@waittime
    tools.each {|t|
      list = $sv.rtd_dispatch_list(t)
      convert_list = $rtdt.change_list_to_hash(list, @@lot)
      $result_rtd << "#{__method__}_12: Product Group #{productg} is not Inhibit with combination Tool #{t} and RTD found no Lot" unless (convert_list["Lot ID"] == @@lot)}

    # Testcase 13: for Product Group and Equipment and Chamber and Recipe Inhibit
    tool.each {|t|
      eq = $sv.eqp_info(t)
      eq.chambers.each {|ch|
        assert_equal 0, $sv.inhibit_entity(["Chamber", "Product Group", "Machine Recipe", ], [t, productg, mrecipe], :attrib=>[ch.chamber, ""])}}
    sleep @@waittime
    tool.each {|t|
      list = $sv.rtd_dispatch_list(t)
      convert_list = $rtdt.change_list_to_hash(list, @@lot)
      $result_rtd << "#{__method__}_13: Product Group #{productg} is Inhibit with combination Tool #{t}, Machine Recipe #{mrecipe} and Chamber and RTD found the Lot" if (convert_list["Lot ID"] == @@lot)
      eq = $sv.eqp_info(t)
      eq.chambers.each {|ch|
      assert_equal 0, $sv.inhibit_cancel(["Chamber", "Product Group", "Machine Recipe", ], [t, productg, mrecipe], :attrib=>[ch.chamber, ""])}}
    sleep @@waittime
    tool.each {|t|
      list = $sv.rtd_dispatch_list(t)
      convert_list = $rtdt.change_list_to_hash(list, @@lot)
      $result_rtd << "#{__method__}_13: Product Group #{productg} is not Inhibit with combination Tool #{t}, Machine Recipe #{mrecipe} and Chamber and RTD found no Lot" unless (convert_list["Lot ID"] == @@lot)}

    # Testcase 14: for Product Group and Equipment and Recipe Inhibit
    tools.each {|t|
      assert_equal 0, $sv.inhibit_entity(["Product Group", "Equipment", "Machine Recipe"], [productg, t, mrecipe])}
    sleep @@waittime
    tools.each {|t|
      list = $sv.rtd_dispatch_list(t)
      convert_list = $rtdt.change_list_to_hash(list, @@lot)
      $result_rtd << "#{__method__}_14: Product Group #{productg} is Inhibit with combination Tool #{t} and Machine Recipe #{mrecipe} and RTD found the Lot" if (convert_list["Lot ID"] == @@lot)
      assert_equal 0, $sv.inhibit_cancel(["Product Group", "Equipment", "Machine Recipe"], [productg, t, mrecipe])}
    sleep @@waittime
    tools.each {|t|
      list = $sv.rtd_dispatch_list(t)
      convert_list = $rtdt.change_list_to_hash(list, @@lot)
      $result_rtd << "#{__method__}_14: Product Group #{productg} is not Inhibit with combination Tool #{t} and Machine Recipe #{mrecipe} and RTD found no Lot" unless (convert_list["Lot ID"] == @@lot)}
      
    # Testcase 15: for Product Group
    assert_equal 0, $sv.inhibit_entity(["Product Group"], [productg])
    sleep @@waittime
    tools.each {|t|
      list = $sv.rtd_dispatch_list(t)
      convert_list = $rtdt.change_list_to_hash(list, @@lot)
      $result_rtd << "#{__method__}_15: Product Group #{productg} is Inhibit and RTD found the Lot" if (convert_list["Lot ID"] == @@lot)}
    assert_equal 0, $sv.inhibit_cancel(["Product Group"], [productg])
    sleep @@waittime
    tools.each {|t|
      list = $sv.rtd_dispatch_list(t)
      convert_list = $rtdt.change_list_to_hash(list, @@lot)
      $result_rtd << "#{__method__}_15: Product Group #{productg} is not Inhibit and RTD found no Lot" unless (convert_list["Lot ID"] == @@lot)}
      
    # Testcase 16: for Machine Recipe Inhibit
    assert_equal 0, $sv.inhibit_entity(["Machine Recipe"], [mrecipe])
    sleep @@waittime
    tools.each {|t|
      list = $sv.rtd_dispatch_list(t)
      convert_list = $rtdt.change_list_to_hash(list, @@lot)
      $result_rtd << "#{__method__}_16: Recipe #{mrecipe} is Inhibit and RTD found Lot" if (convert_list["Lot ID"] == @@lot)}
    assert_equal 0, $sv.inhibit_cancel(["Machine Recipe"], [mrecipe])
    sleep @@waittime
    tools.each {|t|
      list = $sv.rtd_dispatch_list(t)
      convert_list = $rtdt.change_list_to_hash(list, @@lot)
      $result_rtd << "#{__method__}_16: Recipe #{mrecipe} is not Inhibit and RTD found no Lot" unless (convert_list["Lot ID"] == @@lot)}
      
    # Testcase 17: for Equipment Inhibit und Inhibit Exceptions
    tools.each {|t|
      assert_equal 0, $sv.inhibit_entity("Equipment", t)
      assert_equal 0, $sv.inhibit_exception_register(@@lot, "Equipment", t)}
    sleep @@waittime
    tools.each {|t|
      list = $sv.rtd_dispatch_list(t)
      convert_list = $rtdt.change_list_to_hash(list, @@lot)
      $result_rtd << "#{__method__}_17: Tool #{t} is Inhibit but Lot has inhibit exception and RTD found Lot" unless (convert_list["Lot ID"] == @@lot)
      assert_equal 0, $sv.inhibit_exception_cancel(@@lot, "Equipment", t)
      assert_equal 0, $sv.inhibit_cancel("Equipment", t)}
    sleep @@waittime
    tools.each {|t|
      list = $sv.rtd_dispatch_list(t)
      convert_list = $rtdt.change_list_to_hash(list, @@lot)
      $result_rtd << "#{__method__}_1: Tool #{t} is not Inhibit and RTD found no Lot" unless (convert_list["Lot ID"] == @@lot)}

    # Testcase 18: Product Inhibit
    assert_equal 0, $sv.inhibit_entity(["Product Specification"], [product])
    sleep @@waittime
    tools.each {|t|
      list = $sv.rtd_dispatch_list(t)
      convert_list = $rtdt.change_list_to_hash(list, @@lot)
      $result_rtd << "#{__method__}_18: Product #{product} is Inhibit RTD found Lot" if (convert_list["Lot ID"] == @@lot)}
    assert_equal 0, $sv.inhibit_cancel(["Product Specification"], [product])
    sleep @@waittime
    tools.each {|t|
      list = $sv.rtd_dispatch_list(t)
      convert_list = $rtdt.change_list_to_hash(list, @@lot)
      $result_rtd << "#{__method__}_18: Product #{product} is not Inhibit RTD found no Lot" unless (convert_list["Lot ID"] == @@lot)}

    # Testcase 19: Route
    assert_equal 0, $sv.inhibit_entity(["Route"], [route])
    sleep @@waittime
    tools.each {|t|
      list = $sv.rtd_dispatch_list(t)
      convert_list = $rtdt.change_list_to_hash(list, @@lot)
      $result_rtd << "#{__method__}_19: Route #{route} is Inhibit RTD found Lot" if (convert_list["Lot ID"] == @@lot)}
    assert_equal 0, $sv.inhibit_cancel(["Route"], [route])
    sleep @@waittime
    tools.each {|t|
      list = $sv.rtd_dispatch_list(t)
      convert_list = $rtdt.change_list_to_hash(list, @@lot)
      $result_rtd << "#{__method__}_19: Route #{route} is not Inhibit RTD found no Lot" unless (convert_list["Lot ID"] == @@lot)}
      
    # Testcase 20: Route and Operation
    assert_equal 0, $sv.inhibit_entity(["Operation"], [route], :attrib=>[opNo])
    sleep @@waittime
    tools.each {|t|
      list = $sv.rtd_dispatch_list(t)
      convert_list = $rtdt.change_list_to_hash(list, @@lot)
      $result_rtd << "#{__method__}_20: Route #{route} and Operation #{opNo} are Inhibit RTD found Lot" if (convert_list["Lot ID"] == @@lot)}
    assert_equal 0, $sv.inhibit_cancel(["Operation"], [route], :attrib=>[opNo])
    sleep @@waittime
    tools.each {|t|
      list = $sv.rtd_dispatch_list(t)
      convert_list = $rtdt.change_list_to_hash(list, @@lot)
      $result_rtd << "#{__method__}_20: Route #{route} and Operation #{opNo} are not Inhibit RTD found no Lot" unless (convert_list["Lot ID"] == @@lot)}

    # Testcase 21: Route and Operation and Product
    assert_equal 0, $sv.inhibit_entity(["Operation", "Product Specification"], [route, product], :attrib=>[opNo])
    sleep @@waittime
    tools.each {|t|
      list = $sv.rtd_dispatch_list(t)
      convert_list = $rtdt.change_list_to_hash(list, @@lot)
      $result_rtd << "#{__method__}_21: Route #{route} and Operation #{opNo} and Product #{product} are Inhibit RTD found Lot" if (convert_list["Lot ID"] == @@lot)}
    assert_equal 0, $sv.inhibit_cancel(["Operation", "Product Specification"], [route, product], :attrib=>[opNo])
    sleep @@waittime
    tools.each {|t|
      list = $sv.rtd_dispatch_list(t)
      convert_list = $rtdt.change_list_to_hash(list, @@lot)
      $result_rtd << "#{__method__}_21: Route #{route} and Operation #{opNo} and Product #{product} are not Inhibit RTD found no Lot" unless (convert_list["Lot ID"] == @@lot)}

    # Testcase 22: Route and Operation and Product and Equipment
    tools.each {|t|
      assert_equal 0, $sv.inhibit_entity(["Operation", "Product Specification", "Equipment"], [route, product, t], :attrib=>[opNo])}
    sleep @@waittime
    tools.each {|t|
      list = $sv.rtd_dispatch_list(t)
      convert_list = $rtdt.change_list_to_hash(list, @@lot)
      $result_rtd << "#{__method__}_22: Route #{route} and Operation #{opNo} and Product #{product} and Equipment #{t} are Inhibit RTD found Lot" if (convert_list["Lot ID"] == @@lot)
      assert_equal 0, $sv.inhibit_cancel(["Operation", "Product Specification", "Equipment"], [route, product, t], :attrib=>[opNo])}
    sleep @@waittime
    tools.each {|t|
      list = $sv.rtd_dispatch_list(t)
      convert_list = $rtdt.change_list_to_hash(list, @@lot)
      $result_rtd << "#{__method__}_22: Route #{route} and Operation #{opNo} and Product #{product} and Equipment #{t} are not Inhibit RTD found no Lot" unless (convert_list["Lot ID"] == @@lot)}
      
    # Testcase 23: Module PD
    assert_equal 0, $sv.inhibit_entity(["Module Process Definition"], [modulepd])
    sleep @@waittime
    tools.each {|t|
      list = $sv.rtd_dispatch_list(t)
      convert_list = $rtdt.change_list_to_hash(list, @@lot)
      $result_rtd << "#{__method__}_23: Module PD #{modulepd} is Inhibit RTD found Lot" if (convert_list["Lot ID"] == @@lot)}
    assert_equal 0, $sv.inhibit_cancel(["Module Process Definition"], [modulepd])
    sleep @@waittime
    tools.each {|t|
      list = $sv.rtd_dispatch_list(t)
      convert_list = $rtdt.change_list_to_hash(list, @@lot)
      $result_rtd << "#{__method__}_23: Module PD #{modulepd} is not Inhibit RTD found no Lot" unless (convert_list["Lot ID"] == @@lot)}
  end

  # ###################################################### Result and Delete Lots ###################################################

  def test999_results
    puts "Test OK" if $result_rtd.size == 0
    puts "Test NOK, #{$result_rtd.size} failures found" if $result_rtd.size > 0
    
    $result_rtd.each {|i|
      pp i
      if $result_rtd.index(i)%10 == 9
        print "show more Errors, press y?"
        res_input = gets
        break unless res_input.start_with?("y")
      end} if $result_rtd.size > 0
    $rtdt.delete_lots(@@lots)
  end
end