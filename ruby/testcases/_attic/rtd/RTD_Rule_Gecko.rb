=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author:  sfrieske, 2016-10-28
Version: 1.1.1

History:
  2018-03-06 sfrieske, replaced user_parameter_delete_verify by user_paramer_delete
  2022-01-10 sfrieske, moved update_lotsummary_lot from RTD::Test here
=end

require 'jcap/lmtrans'
require 'rtdtest'
require 'RubyTestCase'


# Testcases for RTD Timelink Gate Controller (Gecko)
# reason codes: https://docs.google.com/spreadsheets/d/1clYtLvGgs0j1A4Ph4S2EQkd9g6aZX2g0v7DDXoCP04Y/edit?ts=59228e5d#gid=0
# prerequisites:
# - route_ops must have a required carrier cat
class RTD_Rule_Gecko < RubyTestCase
  # copy of the default route with QT and required carrier category set
  @@rtd_defaults = {route: 'UTRT-GECKO.01', product: 'UT-PRODUCT-GECKO.01', carriers: 'EQA%',
    rule: 'QA_ie_qt_gate_controller_data', category: 'DISPATCHER/QA'}
  @@opNo_trigger = '1000.500'
  @@opNo_trigger2 = '1000.600'      # for wip ratio tests (test191)
  @@route2 = 'UTRT-GECKO.02'        # route with QT at opNO_trigger2
  # for E10 and dedication (mandatory operation within Q-time)
  @@opNo_e10 = '1000.700'
  @@opNo_target = '1000.900'
  @@eqp = 'UTC003'
  @@eqpstate = '4MTN'
  # for reticle (mandatory operation within Q-time)
  @@reticle = 'UTRETICLE0002'
  @@eqp_reticle = 'UTR001'
  @@stocker_reticle = 'RET1130'     # UDATA Location is read, value is F38
  # for CUPOL
  @@eqp_cupol = 'UTFCUPOL1'
  @@op_cupol_prep = 'UTPDCUPOL.01'  # for CUPOL processing before the Q-time
  @@op_cupol = 'UTPDCUPOL.02'       # CUPOL op within the Q-time
  # for ERF
  @@product_erf = 'UT-PRODUCT-GECKO.03'
  @@route_erf = 'UTRT-GECKO.03'
  @@subroute = 'e6795-RECIPE-CHECK-A1.01'  #  must start with 'e'
  @@opNo_split = '1000.500'
  @@opNo_merge = '1000.900'
  @@opNo_erf_trigger1 = '1000.450'
  @@opNo_erf_target1 = '1000.900'
  @@opNo_erf_trigger2 = '1000.900'
  @@opNo_erf_target2 = '1000.950'


  def self.after_all_passed
    @@testlots.each {|lot|
      @@rtdtest.lmtrans.delete_lotsummary_lot(lot)
      @@sv.delete_lot_family(lot)
    }
    @@sv.eqp_cleanup(@@eqp)
  end


  def setup
    return unless self.class.class_variable_defined?(:@@lot)
    # to make PCL checks pass if PD is owned by DIF
    assert_equal 0, @@sv.user_parameter_delete('Lot', @@lot, 'QualEqpID', check: true)
    assert_equal 0, @@sv.user_parameter_delete('Eqp', @@eqp, 'Dedication', check: true)
    # clean up PD UDATA
    [@@op_e10, @@op_cupol].each {|op|
      udata = @@sv.user_data_pd(op)
      if udata['E10ConditionalAvailable'] || udata['OperationReportingType']
        assert @@sm.update_udata(:pd, op, {'E10ConditionalAvailable'=>'', 'OperationReportingType'=>''})
      end
    }
    assert_equal 0, @@sv.eqp_cleanup(@@eqp)
    ##@@rtdtest.parameters = ['lot_id', @@lot]
    @@rtdtest.filter = {'eqp_id'=>@@eqp, 'lot_id'=>@@lot}
  end


  def test00_setup
    $setup_ok = false
    #
    assert @@lmtrans = LotManager::Transformer.new($env)
    # RTD client
    assert @@rtdtest = RTD::Test.new($env, @@rtd_defaults.merge(sv: @@sv))
    @@sm = @@rtdtest.sm
    assert @@rtdtest.check_rule, 'wrong RTD rule called, setup issue?'
    loc = @@sv.user_data(:stocker, @@rtdtest.stocker)['Location']
    assert @@sm.update_udata(:eqp, @@eqp, {'Location'=>loc}), "error setting eqp UDATA"
    assert @@rtdtest.check_eqp(@@eqp), "wrong eqp setup"
    assert @@rtdtest.check_eqp(@@eqp_reticle, nocheck: 'Location'), "wrong eqp setup"
    assert @@sm.update_udata(:eqp, @@eqp_cupol, {'Location'=>loc}), "error setting eqp UDATA"
    assert @@rtdtest.check_eqp(@@eqp_cupol), "wrong eqp setup"
    #
    # remove old test lots to prevent code "W (WIP ratio)" (??)
    @@sv.lot_list(route: @@rtdtest.route).each {|lot| @@sv.delete_lot_family(lot)}
    # create test lot
    @@testlots = []
    assert @@lot = @@rtdtest.new_lot
    @@testlots << @@lot
    assert self.class.update_lotsummary_lot(@@lot, @@opNo_trigger, @@opNo_target, eqp_cleanup: true)
    # check operations
    assert lop = @@sv.lot_operation_list(@@lot).find {|e| e.opNo == @@opNo_e10}
    assert lop.mandatory, "operation #{@@opNo_e10} is not mandatory"
    # store E10 PD and logical recipe for further reference, clean up UDATA
    assert @@op_e10 = @@sv.lot_processlist_in_route(@@lot)[@@opNo_e10]
    assert oinfo = @@sm.object_info(:pd, @@op_e10).first
    assert_equal 'DIF', oinfo.specific[:department], "wrong PD department for Dedication tests"
    @@lrcp = oinfo.specific[:lrcps][nil]
    assert @@sm.object_info(:lrcp, @@lrcp).first.specific[:recipe].first[1]['AUTO-3'], "missing setup"
    assert @@sm.update_udata(:pd, @@op_e10, {'E10ConditionalAvailable'=>'', 'OperationReportingType'=>''})
    @@lrcp_cupol = @@sm.object_info(:pd, @@op_cupol).first.specific[:lrcps][nil]
    @@lrcp_cupol_prep = @@sm.object_info(:pd, @@op_cupol_prep).first.specific[:lrcps][nil]
    #
    $setup_ok = true
  end

  def test110_happy
    $setup_ok = false
    #
    assert_equal 0, @@sv.lot_opelocate(@@lot, @@opNo_trigger)
    assert @@rtdtest.verify_tlgatecodes(''), "wrong setup (PCL spec?)"
    #
    $setup_ok = true
  end

  def test111_E10
    assert_equal 0, @@sv.lot_opelocate(@@lot, @@opNo_trigger)
    assert @@rtdtest.verify_tlgatecodes('')
    # eqp state 2NDP
    assert_equal 0, @@sv.eqp_status_change(@@eqp, '2NDP')
    assert @@rtdtest.verify_tlgatecodes('A')
    # eqp state 4MTN for cond availability, cond parameters '' or 'Yes'
    assert_equal 0, @@sv.eqp_status_change(@@eqp, @@eqpstate)
    assert @@sm.update_udata(:pd, @@op_e10, {'E10ConditionalAvailable'=>''})
    assert @@rtdtest.verify_tlgatecodes('A')
    assert @@sm.update_udata(:pd, @@op_e10, {'E10ConditionalAvailable'=>'Yes'})
    assert @@rtdtest.verify_tlgatecodes('')
  end

  def test112_qual
    assert_equal 0, @@sv.lot_opelocate(@@lot, @@opNo_trigger)
    assert @@rtdtest.verify_tlgatecodes('')
    # set lot script parameter QualEqpID to @@eqp
    assert_equal 0, @@sv.user_parameter_change('Lot', @@lot, 'QualEqpID', @@eqp)
    assert eqp2 = (@@sv.lot_eqplist_in_route(@@lot)[@@opNo_e10] - [@@eqp]).first, "no 2nd eqp at PD #{@@op_e10}"
    # OperationReportingType *QUAL
    assert @@sm.update_udata(:pd, @@op_e10, {'OperationReportingType'=>'QA-QUAL'})
    assert @@rtdtest.verify_tlgatecodes('')
    assert @@rtdtest.verify_tlgatecodes('PQ', filter: {'eqp_id'=>eqp2}, response: :last)
    # OperationReportingType *qual
    assert @@sm.update_udata(:pd, @@op_e10, {'OperationReportingType'=>'QA-qual'})
    assert @@rtdtest.verify_tlgatecodes('')
    # OperationReportingType empty
    assert @@sm.update_udata(:pd, @@op_e10, {'OperationReportingType'=>''})
    assert @@rtdtest.verify_tlgatecodes('')
  end

  def test121_ports_down
    assert_equal 0, @@sv.lot_opelocate(@@lot, @@opNo_trigger)
    assert @@rtdtest.verify_tlgatecodes('')
    # ports down
    assert_equal 0, @@sv.eqp_cleanup(@@eqp, portstatus: 'Down')
    assert @@rtdtest.verify_tlgatecodes('r')
  end

  def test131_fmfg_spec
    assert_equal 0, @@sv.lot_opelocate(@@lot, @@opNo_trigger)
    assert @@rtdtest.verify_tlgatecodes('')
    assert eqp2 = (@@sv.lot_eqplist_in_route(@@lot)[@@opNo_e10] - [@@eqp]).first, "no 2nd eqp at PD #{@@op_e10}"
    assert @@rtdtest.verify_tlgatecodes('P', filter: {'eqp_id'=>eqp2}, response: :last)
  end

  def testdev141_dedication_eqp # DIF .... Not yet working
    assert_equal 0, @@sv.lot_opelocate(@@lot, @@opNo_trigger)
    assert @@rtdtest.verify_tlgatecodes('')
    # eqp script parameter Dedication set (wrong dedication)
    assert_equal 0, @@sv.user_parameter_change('Eqp', @@eqp, 'Dedication', 'QAxxx')
    assert @@rtdtest.verify_tlgatecodes('D')
    # dedication by lot script parameter QualEqpID
    assert_equal 0, @@sv.user_parameter_change('Lot', @@lot, 'QualEqpID', @@eqp)
    assert @@rtdtest.verify_tlgatecodes('')
    # ... dedication by others ..
  end

  def test151_reticle
    # setup lot
    assert lot = @@rtdtest.new_lot
    @@testlots << lot
    assert self.class.update_lotsummary_lot(lot, @@opNo_trigger, @@opNo_target)
    assert_equal 0, @@sv.sublottype_change(lot, 'Equipment Monitor') # to avoid C14 spec checks
    assert_equal 0, @@sv.lot_opelocate(lot, @@opNo_trigger)
    # setup eqp
    assert_equal 0, @@sv.eqp_cleanup(@@eqp_reticle)
    assert_equal 'F38', @@sv.user_data(:stocker, @@stocker_reticle)['Location'], "wrong UDATA"
    # set dispatch_list parameters and response filter
    ##@@rtdtest.parameters = ['lot_id', lot]
    @@rtdtest.filter = {'eqp_id'=>@@eqp_reticle, 'lot_id'=>lot}
    # setup reticle
    assert_equal 0, @@sv.inhibit_cancel(:reticle, @@reticle)
    assert_equal 0, @@sv.reticle_status_change(@@reticle, 'AVAILABLE', check: true)
    assert_equal 0, @@sv.reticle_xfer_status_change(@@reticle, 'EO', eqp: @@eqp_reticle)
    sparams = @@sv.user_parameter('Reticle', @@reticle, all: true)
    sparams.each {|p|
      value = p.datatype == 'STRING' ? 'OK' : 99
      assert @@sv.user_parameter_change('Reticle', @@reticle, p.name, value, 1, datatype: p.datatype)
    }
    assert_equal 0, @@sv.user_parameter_change('Reticle', @@reticle, 'Location', @@stocker_reticle)
    assert_equal 0, @@sv.user_parameter_change('Reticle', @@reticle, 'TargetLocation', 'Module2') # matches stocker's F38
    assert @@rtdtest.verify_tlgatecodes('')
    # change a reticle script parameter to other than OK
    assert_equal 0, @@sv.user_parameter_change('Reticle', @@reticle, 'URSA', 'QA')
    assert @@rtdtest.verify_tlgatecodes('R[u]')
    # change back and inhibit reticle
    assert_equal 0, @@sv.user_parameter_change('Reticle', @@reticle, 'URSA', 'OK')
    assert @@rtdtest.verify_tlgatecodes('')
    assert @@sv.inhibit_entity(:reticle, @@reticle)
    assert @@rtdtest.verify_tlgatecodes('R[iu]')
    # cancel inhibit and change reticle Location/TargetLocation to a wrong combination
    assert_equal 0, @@sv.inhibit_cancel(:reticle, @@reticle)
    assert @@rtdtest.verify_tlgatecodes('')
    assert_equal 0, @@sv.user_parameter_change('Reticle', @@reticle, 'TargetLocation', 'Module1') # mismatch
    sleep 10  # somehow extra delay required
    assert @@rtdtest.verify_tlgatecodes('R[l]')
  end

  def XXtest161_ProcessGroup_CUPOL    ## currently fails, probably configuration issues (APF)
    # prepare for not blocked
    assert @@sm.update_udata(:pd, @@op_cupol_prep, {'OperationReportingType'=>'CUPOL'})
    assert @@sm.change_recipeparameter(@@lrcp_cupol_prep, 'ProcessGroup', 'CUPOL')
    assert @@sm.update_udata(:pd, @@op_cupol, {'OperationReportingType'=>'CUPOL'})
    assert @@sm.change_recipeparameter(@@lrcp_cupol, 'ProcessGroup', 'CUPOL')
    assert_equal 0, @@sv.lot_opelocate(@@lot, nil, op: @@op_cupol_prep)
    assert @@sv.claim_process_lot(@@lot, eqp: @@eqp_cupol)
    assert_equal 0, @@sv.lot_opelocate(@@lot, @@opNo_trigger)
    assert @@rtdtest.verify_tlgatecodes('', filter: {'eqp_id'=>@@eqp_cupol})
    # change recipe parameter ProcessGroup and verify blocked
    assert @@sm.change_recipeparameter(@@lrcp_cupol, 'ProcessGroup', 'CUPOL')
    assert @@rtdtest.verify_tlgatecodes('G', filter: {'eqp_id'=>@@eqp_cupol})
  end

  def test171_toolwish
    assert lot = @@rtdtest.new_lot
    @@testlots << lot
    assert_equal 0, @@sv.lot_opelocate(lot, @@opNo_trigger)
    @@rtdtest.filter = {'eqp_id'=>@@eqp, 'lot_id'=>lot}
    # no LM data
    assert @@rtdtest.verify_tlgatecodes('L')
    # LM w/o ToolWish
    assert self.class.update_lotsummary_lot(lot, @@opNo_trigger, @@opNo_target, delete: true)
    assert self.class.update_lotsummary_lot(lot, @@opNo_trigger, @@opNo_target, toolwish: false)
    assert @@rtdtest.verify_tlgatecodes('')
    # ToolWish empty
    assert self.class.update_lotsummary_lot(lot, @@opNo_trigger, @@opNo_target, toolwish: '')
    assert @@rtdtest.verify_tlgatecodes('')
    # ToolWish MUST
    assert self.class.update_lotsummary_lot(lot, @@opNo_trigger, @@opNo_target, toolwish: 'MUST')
    assert @@rtdtest.verify_tlgatecodes('')
    # ToolWish NEVER
    assert self.class.update_lotsummary_lot(lot, @@opNo_trigger, @@opNo_target, toolwish: 'NEVER')
    assert @@rtdtest.verify_tlgatecodes('T')
  end

  def XXtest181_merge  # deactivated, moved to GeckoA3
    assert lot = @@rtdtest.new_lot
    @@testlots << lot
    assert lc = @@sv.lot_split(lot, 5, mergeop: @@opNo_e10)
    assert self.class.update_lotsummary_lot(lot, @@opNo_trigger, @@opNo_target)
    assert_equal 0, @@sv.lot_opelocate(lot, @@opNo_trigger)
    @@rtdtest.parameters = ['lot_id', lot]
    # child not yet at gate PD
    assert @@rtdtest.verify_tlgatecodes('M')  # trigger_pd is not blocked   ## currently fails
    # child also at gate PD
    assert_equal 0, @@sv.lot_opelocate(lc, @@opNo_trigger)
    assert @@rtdtest.verify_tlgatecodes('')
  end

  def test191_wip_ratio
    # special APF configuration required
    # prepare 3 lots
    @@sv.lot_list(route: @@route2).each {|lot| @@sv.delete_lot_family(lot)}
    assert lots = @@rtdtest.new_lots(3)
    @@testlots += lots
    assert_equal 0, @@sv.schdl_change(lots[2], route: @@route2)
    lots.each {|lot| self.class.update_lotsummary_lot(lot, @@opNo_trigger, @@opNo_target)}
    # lot1 has a QT and is at target PD
    assert_equal 0, @@sv.lot_opelocate(lots[0], @@opNo_trigger)
    assert_equal 0, @@sv.lot_gatepass(lots[0])
    assert_equal 0, @@sv.lot_opelocate(lots[0], @@opNo_target)
    # lot2 is at trigger PD, happy path required: prevent 'G' blocking
    lot = lots[1]
    assert @@sm.update_udata(:pd, @@op_cupol_prep, {'OperationReportingType'=>'CUPOL'})
    assert @@sm.change_recipeparameter(@@lrcp_cupol_prep, 'ProcessGroup', 'CUPOL')
    assert @@sm.update_udata(:pd, @@op_cupol, {'OperationReportingType'=>'CUPOL'})
    assert @@sm.change_recipeparameter(@@lrcp_cupol, 'ProcessGroup', 'CUPOL')
    assert_equal 0, @@sv.lot_opelocate(lot, nil, op: @@op_cupol_prep)
    assert @@sv.claim_process_lot(lot, eqp: @@eqp_cupol)
    assert_equal 0, @@sv.lot_opelocate(lot, @@opNo_trigger)
    assert @@rtdtest.check_tlrunpath(filter: {'eqp_id'=>@@eqp, 'lot_id'=>lot})
    # lot3 is at a differnt trigger PD with same target PD, different route required, happy path required
    lot = lots[2]
    assert_equal 0, @@sv.lot_opelocate(lot, nil, op: @@op_cupol_prep)
    assert @@sv.claim_process_lot(lot, eqp: @@eqp_cupol)
    assert_equal 0, @@sv.lot_opelocate(lots[2], @@opNo_trigger2)
    ##assert @@rtdtest.check_tlrunpath(filter: {'eqp_id'=>@@eqp, 'lot_id'=>lot}) # the one lot passes
    ### call rule with regexp for all 3 lots
    assert @@rtdtest.verify_tlgatecodes('W', filter: {'eqp_id'=>@@eqp, 'lot_id'=>lot})
  end

  def test211_erf  # scenario 06_13
    # create test lot, attach PSM
    assert lot = @@rtdtest.new_lot(product: @@product_erf, route: @@route_erf)
    @@testlots << lot
    assert_equal 0, @@sv.lot_experiment(lot, 2, @@subroute, @@opNo_split, @@opNo_merge)
    # get QT and split
    assert_equal 0, @@sv.lot_opelocate(lot, @@opNo_erf_trigger1)
    gatepd = @@sv.lot_info(lot).op
    assert_equal 0, @@sv.lot_gatepass(lot)
    lc = @@sv.lot_family(lot).last
    refute_equal lot, lc, "PSM not executed"
    assert res = @@rtdtest.call_rule, "no response"
    # verify parent lot entries
    assert oinfo = @@sm.object_info(:mainpd, @@route_erf).first
    assert opinfo1 = oinfo.specific[:operations].find {|e| e.opNo == @@opNo_erf_trigger1}
    assert opinfo2 = oinfo.specific[:operations].find {|e| e.opNo == @@opNo_erf_trigger2}
    col_step = res.headers.index('lot_plan_step_id')
    col_trigger = res.headers.index('is_trigger')
    col_target = res.headers.index('is_target')
    col_pd = res.headers.index('qt_gate_pd_id_generic')
    col_ctrl = res.headers.index('qt_control_limit_by_target')
    col_spec = res.headers.index('qt_spec_limit_by_target')
    steps = res.rows.map {|r| r[col_step]}.uniq.sort
    @@rtdtest.filter_response(res, filter: {'lot_id'=>lot}).rows.each {|row|
      if row[col_step] == '4'
        assert_equal 'TRUE', row[col_trigger]
        assert_equal 'TRUE', row[col_target]
      elsif row[col_step] == steps.last
        assert_equal 'FALSE', row[col_trigger]
        assert_equal 'TRUE', row[col_target]
      else
        assert_equal 'FALSE', row[col_trigger]
        assert_equal 'FALSE', row[col_target]
      end
      assert row[col_pd].start_with?(gatepd), "wrong gatepd: #{row}"
      opinfo = row[col_step] > '4' ? opinfo2 : opinfo1
      assert_equal (opinfo.qtimes[0].duration * 60).to_s, row[col_ctrl], "wrong control limit: #{row}"
      assert_equal (opinfo.qtimes[1].duration * 60).to_s, row[col_spec], "wrong spec limit: #{row}"
    }
    # verify child lot entries, note that spe numbers have a different meaning!
    op = @@sv.lot_info(lc).op
    @@rtdtest.filter_response(res, filter: {'lot_id'=>lc}).rows.each {|row|
      if row[col_step] == steps.first
        assert_equal 'TRUE', row[col_trigger]
        assert_equal 'FALSE', row[col_target]
      else
        assert_equal 'FALSE', row[col_trigger]
        assert_equal 'FALSE', row[col_target]
      end
      assert op.start_with?(row[col_pd]), "wrong gatepd: #{row}"
      assert_equal (opinfo1.qtimes[0].duration * 60).to_s, row[col_ctrl], "wrong control limit: #{row}"
      assert_equal (opinfo1.qtimes[1].duration * 60).to_s, row[col_spec], "wrong spec limit: #{row}"
    }
  end

  # aux methods

  # send lotManager data for an existing lot and a QT area, incl trigger and target ops, optionally clean up eqps
  def self.update_lotsummary_lot(lot, opNo_trigger, opNo_target, params={})
    lotops = params[:lotops] || @sv.lot_processlist_in_route(lot)
    loteqps = params[:loteqps] || @sv.lot_eqplist_in_route(lot)
    ret = true
    trigger_found = false
    lotops.each_pair {|opNo, op|
      trigger_found ||= (opNo == opNo_trigger)
      if trigger_found
        $log.info "data: #{lot}, #{opNo}, #{op}, #{loteqps[opNo]}"
        if params[:toolwish] == false
          pclbase = nil
          $log.info "  no toolwish"
        else
          toolwish = params[:toolwish] || ''  # NEVER, MUST or ''
          pclbase = LotManager::LmPclBase.new(toolwish, Time.now, 'QA Gecko Toolwish',
            31, Time.now, 'QA Gecko ToolScore', 4, 'MANUAL', Time.now, 'QA Gecko Selection')
          $log.info "  toolwish #{toolwish}"
        end
        loteqps[opNo].each {|eqp|
          ctx = LotManager::LmContext.new(nil, lot, op.split('.').first, eqp)
          data = LotManager::LotSummary.new(ctx, pclbase)
          if params[:delete]
            ret &= @lmtrans.delete_lotsummary(data: data)
          else
            ret &= @lmtrans.update_lotsummary(data: data)
          end
          ret &= @sv.eqp_cleanup(eqp, notify: false, mode: 'Auto-1').zero? if params[:eqp_cleanup]
        }
        break if opNo == opNo_target
      end
    }
    return ret && trigger_found
  end

end
