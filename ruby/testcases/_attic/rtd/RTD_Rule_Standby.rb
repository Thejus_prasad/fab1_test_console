=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author:  sfrieske, 2017-04-03

Version: 1.0.0

History:
  2021-09-15 sfrieske, finally replaced $svtest by @@svtest, $sv by @@sv and $sm by @@sm
  2021-12-08 sfrieske, simplified MDS queries
=end

require 'RubyTestCase'
require 'rtdtest'
require 'rtddb'


# Testcases for RTD Cascading Rule, spec C07-00003041 (ITDC)
##eqptype 'POL40X' -> AMHS spec (ITDC: C07-00003407), default: 7200:10:10800:1:1, Cascade spec (ITDC: C07-00003482)
class RTD_Rule_Standby < RubyTestCase
  @@rtd_defaults = {rule: 'QA_sby_logging', category: 'DISPATCHER/QA'}
  # any eqp that has the chambers in the module combination, modulecombination <ch1>:<ch2> must exist in APF (from Armor/RMS) _independently from the tool_
  @@eqp = 'UTC002'
  @@ch1 = 'PM1'
  @@ch2 = 'PM2'
  @@a3code = 11
  @@substates = {11=>'2WLT', 13=>'2INH', 17=>'2WCB', 22=>'2NTL', 24=>'2WMR'}

  def self.shutdown
  end

  def self.after_all_passed
    @@testlots.each {|lot| @@sv.delete_lot_family(lot)}
  end


  def setup
    # change chamber from 2SUP back to 2WPR to clean up the substates
    assert_equal 0, @@sv.eqp_cleanup(@@eqp)
    [@@ch1, @@ch2].each {|ch| assert_equal 0, @@sv.chamber_status_change(@@eqp, ch, '2SUP')}
    assert_equal 0, @@sv.eqp_status_change(@@eqp, '2SUP')
    assert_equal 0, @@sv.eqp_cleanup(@@eqp)
  end

  def test00_setup
    $setup_ok = false
    #
    assert @@rtdtest = RTD::Test.new($env, @@rtd_defaults.merge(sv: @@sv))
    @@sm = @@rtdtest.sm
    @@rtdtest.parameters = ['EqpID', @@eqp, 'cast_regex', @@rtdtest.carriers.sub('%', '')]
    assert res = @@rtdtest.client.dispatch_list(nil, report: @@rtdtest.rule, category: @@rtdtest.category)
    assert_equal @@rtdtest.rule, res.rule, 'wrong rule, RTD setup?'
    assert @@mds = RTD::MDS.new($env)
    #
    # create test lots
    assert lot = @@rtdtest.new_lot, 'error creating test lots'
    @@testlots = [lot]
    assert @@opNo = @@sv.lot_eqplist_in_route(lot, eqp: @@eqp).keys.first
    #
    $setup_ok = true
  end

  def test11_nocj_inh
    $setup_ok = false
    #
    # eqp and ch1 2SUP and rule call
    assert_equal 0, @@sv.eqp_status_change(@@eqp, '2SUP')
    assert_equal 0, @@sv.chamber_status_change(@@eqp, @@ch1, '2SUP')
    assert @@rtdtest.verify_a3codes([@@a3code], parameters: ['data', "#{@@eqp};#{@@ch1}:#{@@ch2};#{@@ch2};TRUE;#{@@a3code}"])
    # verify new substates
    sleep 30
    assert_equal @@substates[@@a3code], @@mds.tool(@@eqp).first[:substate], 'wrong substate'
    assert_equal @@substates[@@a3code], @@mds.subtool(@@eqp, @@ch1).first[:substate], 'wrong substate'
    assert_equal '2INH', @@mds.subtool(@@eqp, @@ch2).first[:substate], 'wrong substate of inhibited ch'
    #
    $setup_ok = true
  end

  def test12_nocj_noinh
    # eqp and ch1 2SUP and rule call
    assert_equal 0, @@sv.eqp_status_change(@@eqp, '2SUP')
    assert_equal 0, @@sv.chamber_status_change(@@eqp, @@ch1, '2SUP')
    assert @@rtdtest.verify_a3codes([@@a3code], parameters: ['data', "#{@@eqp};#{@@ch1}:#{@@ch2};;TRUE;#{@@a3code}"])
    # verify new substates
    sleep 30
    assert_equal @@substates[@@a3code], @@mds.tool(@@eqp).first[:substate], 'wrong substate'
    assert_equal @@substates[@@a3code], @@mds.subtool(@@eqp, @@ch1).first[:substate], 'wrong substate'
    assert_equal @@substates[@@a3code], @@mds.subtool(@@eqp, @@ch2).first[:substate], 'wrong substate'
  end

  def test21_cj_inh
    # SLR
    lot = @@testlots[0]
    assert_equal 0, @@sv.lot_opelocate(lot, @@opNo)
    assert cj = @@sv.slr(@@eqp, lot: lot)
    # eqp and ch1 2SUP and rule call
    assert_equal 0, @@sv.eqp_status_change(@@eqp, '2SUP')
    assert_equal 0, @@sv.chamber_status_change(@@eqp, @@ch1, '2SUP')
    assert @@rtdtest.verify_a3codes([@@a3code], parameters: ['data', "#{@@eqp};#{@@ch1}:#{@@ch2};#{@@ch2};TRUE;#{@@a3code}"])
    # verify new substates
    sleep 30
    assert_equal 'n/a', @@mds.tool(@@eqp).first[:substate], 'wrong substate'
    assert_equal 'n/a', @@mds.subtool(@@eqp, @@ch1).first[:substate], 'wrong substate'
    assert_equal '2INH', @@mds.subtool(@@eqp, @@ch2).first[:substate], 'wrong substate of inhibited ch'
  end

  def test22_cj_noinh
    # SLR
    lot = @@testlots[0]
    assert_equal 0, @@sv.lot_opelocate(lot, @@opNo)
    assert cj = @@sv.slr(@@eqp, lot: lot)
    # eqp and ch1 2SUP and rule call
    assert_equal 0, @@sv.eqp_status_change(@@eqp, '2SUP')
    assert_equal 0, @@sv.chamber_status_change(@@eqp, @@ch1, '2SUP')
    assert @@rtdtest.verify_a3codes([@@a3code], parameters: ['data', "#{@@eqp};#{@@ch1}:#{@@ch2};;TRUE;#{@@a3code}"])
    # verify new substates
    sleep 30
    assert_equal 'n/a', @@mds.tool(@@eqp).first[:substate], 'wrong substate'
    assert_equal 'n/a', @@mds.subtool(@@eqp, @@ch1).first[:substate], 'wrong substate'
    assert_equal @@substates[@@a3code], @@mds.subtool(@@eqp, @@ch2).first[:substate], 'wrong substate'
  end

end
