=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

	load_run_testcase 'Test_P_FVX', "itdc"

Author: Paul Peschel, 2012-10-02
Version: 1.0.0

=end

require "RubyTestCase"
require 'rtdruletest'
require 'rtdclient'

# Testcases for RTD Rule P_FVX
#
# If run testcases alone over "load_run_testcase 'Test_P_FVX', "itdc", :tp => /20/" you must do first:
#
# 1. @@lots = ""
#
# 3. @@lots_all = ""
#
# 4. Call "test01_prepare_test", because without that, you have no Lot

class Test_P_FVX < RubyTestCase

  @@waittime = 10

  def setup
    super
    $rtdt = RTD::RuleTest.new($env, :sv=>$sv) unless $rtdt and $rtdt.env == $env
    $rtdc = RTD::Client.new($env) unless $rtdc and $rtdc.env == $env
    assert $setup_ok, "test setup is not correct"
  end

  # ###################################################### Setup des Tests ##########################################################
  #
  # Prepare all equipment and lots for the Test
  #
  # See also testsetup in excel file Testsetup.xls in sheet "Testsetup_P_FVX".
  # Here described all important stuff for the tests in this File.
  #
  # http://myteamsdrs2/sites/FICSQA/_layouts/xlviewer.aspx?id=/sites/FICSQA/Documentation/Technical%20Documentation/RTD/Testcases_P_FVX.xlsx

  def test01_prepare_test
      # Return Value
      $result_rtd = []
      ok = true

      # Expected Values for Lot
      user = "X-UNITTEST"

      # All Equipments
      opEqps = "RTDPIB01 RTDPIB02".split

      # Create Lot
      @@lots = ""
      cl = $sv.carrier_list(:category=>"BEOL", :status=>"AVAILABLE", :carrier =>"G%", :empty=>true)
      comment = "ERPITEM=3006AA-UA0,DPML=2.0,PLANNED_FSD=#{Time.now.strftime('%Y-%m-%d')}"
      @@lot = $sv.new_lot_release(:product=>"RTDP-PRODUCT.01", :customer=>"gf", :route=>"P-RTDP-MAINPD01.01", :sublottype=>"PO", :carrier => cl.pop, :comment=>comment, :stbdelay=>1)
      @@lot2 = $sv.new_lot_release(:product=>"RTDP-PRODUCT.01", :customer=>"gf", :route=>"P-RTDP-MAINPD01.01", :sublottype=>"PO", :carrier => cl.pop, :comment=>comment, :stbdelay=>1)
      @@lot3 = $sv.new_lot_release(:product=>"RTDP-PRODUCT.01", :customer=>"gf", :route=>"P-RTDP-MAINPD01.01", :sublottype=>"PO", :carrier => cl.pop, :comment=>comment, :stbdelay=>1)
      @@lots = [@@lot]+[@@lot2]+[@@lot3]

      ok &= $rtdt.lot_carrier_cleanup(@@lots)
      ok &= $rtdt.eqp_cleanup(opEqps)
      ok &= $rtdt.stocker_cleanup()

      # Cancel setup if user is wrong
      setup_correct = false unless $sv.user == user and ok == true

      # Check Setup is correct
      if setup_correct == false
          $log.info("Setup has wrong User or wrong Setup")
      end
  end

  ####################################################### Testcases for RTD #########################################################

  # *Testcase*: A3 Checks QT (Code Nr: 11)
  #
  # *Number*: 20
  #
  # *Description*: Lot will be QT exceed
  #
  # *Logic*:
  #
  #  IF FULL qt_blocked AND qt_blocked THEN A3Info = �Lot w�rde QT �berschreiten�
  #
  # *Condition*:
  #
  # qt_blocked := block_lot (File: ie_wet_qt_capacity_check_dispatch.apf)
  #
  # *Setup*:
  # * Product: RTDP-PRODUCT.01
  # * Tools: RTDPIB01, RTDPIB02
  # * PD: 1000.1000
  # * Scriptparameter:
  # * Carriercategory: BEOL
  # * Splitlot: No

  def test20_A3_checks_qt_exceed
    # Start values
    cl = $sv.carrier_list(:category=>"BEOL", :status=>"AVAILABLE", :carrier =>"G%", :empty=>true)
    comment = "ERPITEM=3006AA-UA0,DPML=2.0,PLANNED_FSD=#{Time.now.strftime('%Y-%m-%d')}"
    @@lot4 = $sv.new_lot_release(:product=>"RTDP-PRODUCT.01", :customer=>"gf", :route=>"P-RTDP-MAINPD01.01", :sublottype=>"PO", :carrier => cl.pop, :lot =>"U253Q.00", :comment=>comment, :stbdelay=>1)

    # Test Cleanup
    assert $rtdt.lot_carrier_cleanup(@@lot4)
    li = $sv.lot_info(@@lot4)
    tool = li.opEqps[0]
    assert $rtdt.eqp_cleanup(tool)
    assert $rtdt.stocker_cleanup()

    # Testcase: Lot will be QT exceed
    sleep @@waittime
    list = $sv.rtd_dispatch_list(tool)
    convert_list = $rtdt.change_list_to_hash(list, @@lot4)
    $result_rtd << "#{__method__}: Lot #{@@lot2} is on PD and not found on RTD List" unless (convert_list["Lot ID"] == @@lot4)
    $result_rtd << "#{__method__}: Lot #{@@lot2} can not be on Tool and A3 is Yes " unless (convert_list["A3"] == "N")
    $result_rtd << "#{__method__}: A3 Info is wrong '#{convert_list["A3Info"]}' should be include 'Lot wuerde QT ueberschreiten'" unless (convert_list["A3Info"].split("|").member?("Lot wuerde QT ueberschreiten"))
    @@lots += [@@lot4]
  end

  # *Testcase*: A3 Checks load_distribution (Code Nr: 21)
  #
  # *Number*: 25
  #
  # *Description*: Load-Distribution
  #
  # *Logic*:
  #
  #  IF NOT(FULL eqp_distributed AND eqp_distributed == eqp_id) THEN A3Info=�Load-Verteilung�
  #
  # *Condition*:
  #
  # eqp_distributed:= FRLOT -> CntlJobMap -> eqp_id
  #
  # *Setup*:
  # * Product: RTDP-PRODUCT.01
  # * Tools: RTDPIB01, RTDPIB02
  # * PD: 1000.1000
  # * Scriptparameter:
  # * Carriercategory: BEOL
  # * Splitlot: No

  def test25_A3_checks_load_distribution
    # Start values
    stocker = "UTSTO11"
    state1 = "MI"
    state2 = "MO"
    port = "PG1"
    li = $sv.lot_info(@@lot)
    tool = li.opEqps[0]

    # Test Cleanup
    assert $rtdt.eqp_cleanup(tool)
    assert $rtdt.lot_carrier_cleanup(@@lot)
    assert $rtdt.stocker_cleanup()

    # Testcase: Load Disribution
    assert_equal 0, $sv.carrier_xfer_status_change(li.carrier, state2, stocker)
    assert_equal 0, $sv.eqp_mode_offline1(tool, :notifyTCS=>false)
    assert_equal 0, $sv.eqp_load(tool, port, li.carrier)
    sleep @@waittime
    list = $sv.rtd_dispatch_list(tool)
    convert_list = $rtdt.change_list_to_hash(list, @@lot2)
    $result_rtd << "#{__method__}: Lot #{@@lot2} is on PD and not found on RTD List" unless (convert_list["Lot ID"] == @@lot2)
    $result_rtd << "#{__method__}: Lot #{@@lot2} can not be on Tool and A3 is Yes " unless (convert_list["A3"] == "N")
    $result_rtd << "#{__method__}: Lot #{@@lot2} can not be on Tool and A3 Info is '#{convert_list["A3Info"]}' instead of 'Load-Verteilung'" unless (convert_list["A3Info"] == "Load-Verteilung")
  end

  # *Testcase*: A3 Checks only one Batch is possible with A3 (Code Nr: 56)
  #
  # *Number*: 30
  #
  # *Description*: Only one Batch is possible with A3
  #
  # *Logic*:
  #
  #  IF (NULL batch_crit_a3 OR batch_crit_a3 != batch_crit) AND eqp_distributed == eqp_id THEN A3Info =� Nur ein Batch A3 f�hig�
  #
  # *Condition*:
  #
  # *Setup*:
  # * Product: RTDP-PRODUCT.01
  # * Tools: RTDPIB01, RTDPIB02
  # * PD: 1000.1000
  # * Scriptparameter:
  # * Carriercategory: BEOL
  # * Splitlot: No

  def test30_A3_checks_only_batch_possible
    # Start values


    # Test Cleanup
    assert $rtdt.eqp_cleanup(tool)
    assert $rtdt.lot_carrier_cleanup(@@lot)
    assert $rtdt.stocker_cleanup()

    # Testcase: Only one Batch is possible with A3
    assert_equal 0, $sv.carrier_xfer_status_change(li.carrier, state2, stocker)
    assert_equal 0, $sv.eqp_mode_offline1(tool, :notifyTCS=>false)
    assert_equal 0, $sv.eqp_load(tool, port, li.carrier)


  end

  # *Testcase*: A3 Checks Max Jobcount reached (Code Nr: 78)
  #
  # *Number*: 35
  #
  # *Description*: Max Jobcount reached
  #
  # *Logic*:
  #
  #  IF FULL queued_jobs AND queued_jobs >= MaxJobCount - 1 THEN A3Info =� Max Jobanzahl erreicht�
  #
  # *Condition*:
  #
  # *Setup*:
  # * Product: RTDP-PRODUCT.01
  # * Tools: RTDPIB01, RTDPIB02
  # * PD: 1000.1000
  # * Scriptparameter:
  # * Carriercategory: BEOL
  # * Splitlot: No

  def test35_A3_checks_max_jobcount_reached
    # Start values
    stocker = "UTSTO11"
    state2 = "MO"
    port = "PG1"
    port2 = "PG2"
    li = $sv.lot_info(@@lot)
    tool = li.opEqps[0]

    # Test Cleanup
    assert $rtdt.eqp_cleanup(tool)
    assert $rtdt.lot_carrier_cleanup(@@lot.split + @@lot2.split + @@lot3.split)
    assert $rtdt.stocker_cleanup()

    # Testcase: Max Jobcount reached
    lot = @@lot.split + @@lot2.split
    assert_equal 0, $sv.eqp_mode_offline1(tool, :notifyTCS=>false)
    lot.each {|l|
      li = $sv.lot_info(l)
      assert_equal 0, $sv.carrier_xfer_status_change(li.carrier, state2, stocker)
      assert_equal 0, $sv.eqp_load(tool, port, li.carrier)
      refute_equal "", cj = $sv.eqp_opestart(tool, li.carrier)
    }
    sleep @@waittime
    list = $sv.rtd_dispatch_list(tool)
    convert_list = $rtdt.change_list_to_hash(list, @@lot3)
    $result_rtd << "#{__method__}: Lot #{@@lot3} is on PD and not found on RTD List" unless (convert_list["Lot ID"] == @@lot3)
    $result_rtd << "#{__method__}: Lot #{@@lot3} can not be on Tool and A3 is Yes " unless (convert_list["A3"] == "N")
    $result_rtd << "#{__method__}: Lot #{@@lot3} can not be on Tool and A3 Info is '#{convert_list["A3Info"]}' instead of 'Max Jobanzahl erreicht'" unless (convert_list["A3Info"] == "Max Jobanzahl erreicht")
    $rtdt.eqp_cleanup(tool)
  end

  # *Testcase*: A3 Checks Internal Buffer full (Code Nr: 59)
  #
  # *Number*: 40
  #
  # *Description*: Internal Buffer full
  #
  # *Logic*:
  #
  #  IF FULL batch_castcnt AND FULL free_shelf_ports AND free_shelf_ports < batch_castcnt THEN A3Info=� Internal Buffer full�
  #
  # *Condition*:
  #
  # *Setup*:
  # * Product: RTDP-PRODUCT.01
  # * Tools: RTDPIB01, RTDPIB02
  # * PD: 1000.1000
  # * Scriptparameter:
  # * Carriercategory: BEOL
  # * Splitlot: No

  def test40_A3_checks_internal_buffer_full
    # Start values


    # Test Cleanup
    assert $rtdt.eqp_cleanup(tool)
    assert $rtdt.lot_carrier_cleanup(@@lot)
    assert $rtdt.stocker_cleanup()

    # Testcase: Internal Buffer full



  end

  # *Testcase*: A3 Checks Lot not in Schedule (Code Nr: 80)
  #
  # *Number*: 45
  #
  # *Description*: Lot not in Schedule
  #
  # *Logic*:
  #
  #  FULL QT_ANL AND QT_ANL AND NULL qt_blocked THEN A3Info =� Lot not in Schedule�
  #
  # *Condition*:
  #
  # *Setup*:
  # * Product: RTDP-PRODUCT.01
  # * Tools: RTDPIB01, RTDPIB02
  # * PD: 1000.1000
  # * Scriptparameter:
  # * Carriercategory: BEOL
  # * Splitlot: No

  def test45_A3_checks_lot_in_schedule
    # Start values


    # Test Cleanup
    assert $rtdt.eqp_cleanup(tool)
    assert $rtdt.lot_carrier_cleanup(@@lot)
    assert $rtdt.stocker_cleanup()

    # Testcase: Lot not in Schedule



  end

  # *Testcase*: A3 Checks FOUP has SortJob (Code Nr: 42)
  #
  # *Number*: 50
  #
  # *Description*: FOUP hat SortJob
  #
  # *Logic*:
  #
  # IF FULL target_cast THEN A3Info =�FOUP hat SortJob�
  #
  # *Condition*:
  #
  # *Setup*:
  # * Product: RTDP-PRODUCT.01
  # * Tools: RTDPIB01, RTDPIB02
  # * PD: 1000.1000
  # * Scriptparameter:
  # * Carriercategory: BEOL
  # * Splitlot: No

  def test50_A3_checks_foup_has_SortJob
    # Start values


    # Test Cleanup
    assert $rtdt.eqp_cleanup(tool)
    assert $rtdt.lot_carrier_cleanup(@@lot)
    assert $rtdt.stocker_cleanup()

    # Testcase: FOUP hat SortJob



  end

  # *Testcase*: A3 Checks Not enought wafer for Batch (Code Nr: 17)
  #
  # *Number*: 55
  #
  # *Description*: FOUP hat SortJob
  #
  # *Logic*:
  #
  # IF (FULL cnt_wafers_in_batch  AND cnt_wafers_in_batch > batch_size_max * InstantBatchingMinWfrRel)  OR
  # (FULL prio_lot_in_batch    AND prio_lot_in_batch)         OR
  # (FULL max_age_lot_in_batch AND max_age_lot_in_batch > MINUTES(InstantBatchingMaxAge_min)) OR
  # cnt_carriers_in_batch == batch_size_max OR
  # NOT active OR
  # FULL QualEqpID
  # THEN A3code ELSE A3Info =� Zu wenige Wafer f�r Batch�
  #
  # *Condition*:
  #
  # *Setup*:
  # * Product: RTDP-PRODUCT.01
  # * Tools: RTDPIB01, RTDPIB02
  # * PD: 1000.1000
  # * Scriptparameter:
  # * Carriercategory: BEOL
  # * Splitlot: No

  def test55_A3_checks_not_enought_wafer
    # Start values


    # Test Cleanup
    assert $rtdt.eqp_cleanup(tool)
    assert $rtdt.lot_carrier_cleanup(@@lot)
    assert $rtdt.stocker_cleanup()

    # Testcase: Not enought wafer for Batch



  end


  # ###################################################### Result and Delete Lots ###################################################

  def test999_results
    puts "Test OK" if $result_rtd.size == 0
    puts "Test NOK, #{$result_rtd.size} failerures found" if $result_rtd.size > 0

    $result_rtd.each {|i|
      pp i
      if $result_rtd.index(i)%10 == 9
        print "show more Errors, press y?"
        res_input = gets
        break unless res_input.start_with?("y")
      end} if $result_rtd.size > 0
    $rtdt.delete_lots(@@lots)
  end
end
