=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author:  sfrieske, 2017-03-27
Version: 1.0.3

History:
  2017-12-08 sfrieske, adjusted test cases
  2018-04-24 sfrieske, use RTD::Test.verify_rows
  2021-02-05 sfrieske, use instance_of instead of kind_of
  2021-09-15 sfrieske, finally replaced $svtest by @@svtest, $sv by @@sv and $sm by @@sm
=end

require 'rtdtest'
require 'misc/sfc'
require 'RubyTestCase'


# Testcases for RTD Cascading Rule, spec C07-00003041 (ITDC)
#   eqptype 'POL40X' -> AMHS spec (ITDC: C07-00003407), default: 7200:10:10800:1:1, Cascade spec (ITDC: C07-00003482)
class RTD_Rule_Cascading < RubyTestCase
  @@rtd_defaults = {route: 'UTRT-CASCADING.01', product: 'UT-PRODUCT-CASCADING.01', carriers: 'EQA%',
    rule: 'QA_Cascading_test', category: 'DISPATCHER/QA', rtdinstance: 'itdc', apf_delay: 15}
  # PDs with different LR for the same set of eqps
  @@op1 = 'UTPD-CASCADING.01'
  @@op2 = 'UTPD-CASCADING.02'
  # stockers at different locations
  @@sto1 = 'UTSTO11'
  @@sto2 = 'UTSTO12'
  # eqp that is used for 2 different logical recipes, standard UT route
  @@eqp1 = 'UTFCASC1'
  @@eqp2 = 'UTFCASC2'
  @@eqp3 = 'UTFCASC3'
  @@eqptype = 'MDX20X'  # must have an entry in cascading spec, low TCT in APF
  @@loc1 = {'AmhsArea'=>'F36', 'Location'=>'M1-L3-MAIN'}
  @@loc2 = {'AmhsArea'=>'F38', 'Location'=>'M2-L3-MAIN'}
  @@spec_casc = 'C07-00003482'


  def self.after_all_passed
    @@testlots.each {|lot| @@sv.delete_lot_family(lot)}
    @@sv.carrier_list(carrier: @@rtdtest.carriers, carrier_category: @@rtdtest.carrier_category).each {|c|
      @@sv.carrier_status_change(c, 'NOTAVAILABLE', check: true)
    }
  end


  def test00_setup
    $setup_ok = false
    #
    # verify correct rule will be called
    @@rtdtest = RTD::Test.new($env, @@rtd_defaults.merge(sv: @@sv))
    @@rtdtest.parameters = ['EqpID', @@eqp1]
    @@rtdtest.columns = %w(PlanEqp balancing_kind transport_needed)
    assert @@rtdtest.check_rule, 'wrong RTD rule called'
    # verify specs
    @@sfc = SetupFC::Client.new($env)
    assert specdata = @@sfc.spec_content(@@spec_casc, parsed: true)
    assert rows = specdata[:myFields][:Content][:TableTypes][:TableType][:Tables][:Table][:Rows][:Row]
    assert row = rows.find {|r| r[:Col_2] == "SPEC_#{@@eqptype}" && r[:Col_3] == 'DispatchCascadeContext'}
    assert_equal 'logical_recipe', row[:Col_7], 'wrong spec setup'
    assert row = rows.find {|r| r[:Col_2] == "SPEC_#{@@eqptype}" && r[:Col_3] == 'DispatchCascadeMax'}
    assert_equal '2', row[:Col_7], 'wrong spec setup'
    #
    # set eqp and stocker locations, remove duplicate MM UDATA
    assert @@rtdtest.stocker_location(@@sto1, @@loc1), 'error setting UDATA'
    assert @@rtdtest.stocker_location(@@sto2, @@loc2), 'error setting UDATA'
    assert @@rtdtest.eqp_location(@@eqp1, @@loc1), 'error setting UDATA'
    assert @@rtdtest.eqp_location(@@eqp2, @@loc1), 'error setting UDATA'
    assert @@rtdtest.eqp_location(@@eqp3, @@loc2), 'error setting UDATA'
    # configure and verify eqps
    [@@eqp1, @@eqp2, @@eqp3].each {|eqp|
      assert_equal @@eqptype, @@rtdtest.sm.object_info(:eqp, eqp).first.specific[:eqptype], 'wrong eqp setup'
      assert @@rtdtest.cleanup_eqp(eqp, mode: 'Auto-3')
      assert @@rtdtest.check_eqp(eqp, nocheck: ['Location']), 'wrong eqp setup'
    }
    #
    col_lot = @@rtdtest.response.headers.find_index('Lot ID')
    @@rtdtest.response.rows.each {|row| @@sv.lot_opelocate(row[col_lot], :first)}
    #
    # remove old lots
    @@sv.carrier_list(carrier: @@rtdtest.carriers, carrier_category: @@rtdtest.carrier_category).each {|c|
      @@sv.carrier_status(c).lots.each {|lot| @@sv.delete_lot_family(lot)}
    }
    # create new test lots
    assert @@testlots = @@rtdtest.new_lots(5), 'error creating test lots'
    # open the cascade for eqps and fill it up (DispatchCascadeMax is 2, see spec), for test1x and test2x
    prepare_eqps({@@eqp1=>[@@op2, 2], @@eqp2=>[@@op1, 2], @@eqp3=>[@@op1, 2]})
    #
    $setup_ok = true
  end

  def test11_1eqp_same_lr
    $setup_ok = false
    #
    # 1 eqp at loc1 (same as stocker)
    prepare_eqps([@@eqp1])
    # move lots 1 and 2 to op1 and lots 3 and 4 out of the way
    @@testlots[1..2].each {|lot| assert_equal 0, @@sv.lot_cleanup(lot, op: @@op1, stocker: @@sto1)}
    @@testlots[3..4].each {|lot| assert_equal 0, @@sv.lot_opelocate(lot, :first)}
    # call the rule and verify response
    expected = [
      [[], @@eqp1, 'Local', 'TRUE'],
      [[], @@eqp1, 'Local', 'TRUE']
    ]
    assert @@rtdtest.verify_rows(expected), 'wrong RTD data'
    #
    $setup_ok = true
  end

  def test12_1eqp_different_lrs
    $setup_ok = false
    # 1 eqp at loc1 (same as stocker)
    prepare_eqps([@@eqp1])
    # locate lots 1 and 2 to op1 and op2 resp. and move lots 3 and 4 out of the way
    assert_equal 0, @@sv.lot_cleanup(@@testlots[1], op: @@op1, stocker: @@sto1)
    assert_equal 0, @@sv.lot_cleanup(@@testlots[2], op: @@op2, stocker: @@sto1)
    @@testlots[3..4].each {|lot| assert_equal 0, @@sv.lot_opelocate(lot, :first)}
    # call the rule and verify response
    expected = [
      [[], @@eqp1, 'Local', 'TRUE'],
      [12, @@eqp1, 'Local', 'TRUE']
    ]
    assert @@rtdtest.verify_rows(expected), 'wrong RTD data'
    #
    $setup_ok = true
  end

  def test21_2eqps_same_location
    # 2 eqps at same location
    prepare_eqps([@@eqp1, @@eqp2])
    # 4 test lots in stocker at loc1, at op1
    @@testlots[1..4].each {|lot| assert_equal 0, @@sv.lot_cleanup(lot, op: @@op1, stocker: @@sto1)}
    # call the rule and verify response
    expected = [
      [[], @@eqp1, 'Local', 'TRUE'],
      [21, @@eqp2, 'Local', 'TRUE'],
      [[], @@eqp1, 'Local', 'TRUE'],
      [21, @@eqp2, 'Local', 'TRUE']
    ]
    assert @@rtdtest.verify_rows(expected), 'wrong RTD data'
  end

  def test22_2eqps_different_locations
    # 2 eqps at different locations
    prepare_eqps([@@eqp1, @@eqp3])
    # 4 test lots in stocker at loc1, at op1
    @@testlots[1..4].each {|lot| assert_equal 0, @@sv.lot_cleanup(lot, op: @@op1, stocker: @@sto1)}
    # call the rule and verify response
    expected = [
      [[], @@eqp1, 'Local', 'TRUE'],
      [21, @@eqp3, 'Global', 'TRUE'],
      [[], @@eqp1, 'Local', 'TRUE'],
      [21, @@eqp3, 'NotBalanced', 'FALSE']
    ]
    assert @@rtdtest.verify_rows(expected), 'wrong RTD data'
  end

  def test23_2eqps_diff_lots_diff_locations  # Test5
    # 2 eqps at different locations
    prepare_eqps([@@eqp1, @@eqp3])
    # 4 test lots at op1, 2 in stocker at loc1, 2 in stocker at loc2
    @@testlots[1..2].each {|lot| assert_equal 0, @@sv.lot_cleanup(lot, op: @@op1, stocker: @@sto1)}
    @@testlots[3..4].each {|lot| assert_equal 0, @@sv.lot_cleanup(lot, op: @@op1, stocker: @@sto2)}
    # call the rule and verify response
    expected = [
      [[], @@eqp1, 'Local', 'TRUE'],
      [[], @@eqp1, 'Local', 'TRUE'],
      [21, @@eqp3, 'Local', 'TRUE'],
      [21, @@eqp3, 'Local', 'TRUE']
    ]
    assert @@rtdtest.verify_rows(expected), 'wrong RTD data'
  end

  def test24_2eqps_diff_lots_diff_locations_reversed
    # 2 eqps at different locations
    prepare_eqps([@@eqp1, @@eqp3])
    # 4 test lots at op1, 2 in stocker at loc2, 2 in stocker at loc1
    @@testlots[1..2].each {|lot| assert_equal 0, @@sv.lot_cleanup(lot, op: @@op1, stocker: @@sto2)}
    @@testlots[3..4].each {|lot| assert_equal 0, @@sv.lot_cleanup(lot, op: @@op1, stocker: @@sto1)}
    # call the rule and verify response
    expected = [
      [21, @@eqp3, 'Local', 'TRUE'],
      [21, @@eqp3, 'Local', 'TRUE'],
      [[], @@eqp1, 'Local', 'TRUE'],
      [[], @@eqp1, 'Local', 'TRUE'],
    ]
    assert @@rtdtest.verify_rows(expected), 'wrong RTD data'
  end

  def test25_2eqps_diff_lots_other_location  # Test7
    # 2 eqps at different locations
    prepare_eqps([@@eqp1, @@eqp3])
    # 4 test lots at op1, in stocker at loc2
    @@testlots[1..4].each {|lot| assert_equal 0, @@sv.lot_cleanup(lot, op: @@op1, stocker: @@sto2)}
    # call the rule and verify response
    expected = [
      [21, @@eqp3, 'Local', 'TRUE'],
      [21, @@eqp1, 'Global', 'TRUE'],
      [21, @@eqp3, 'Local', 'TRUE'],
      [21, @@eqp1, 'NotBalanced', 'FALSE'],
    ]
    assert @@rtdtest.verify_rows(expected), 'wrong RTD data'
  end

  def test26_1eqp_lots_other_location
    # 1 eqp, other loc as stocker
    prepare_eqps([@@eqp1])
    # 4 test lots at op1, in stocker at loc2
    @@testlots[1..4].each {|lot| assert_equal 0, @@sv.lot_cleanup(lot, op: @@op1, stocker: @@sto2)}
    # call the rule and verify response
    expected = [
      [21, @@eqp1, 'Local', 'TRUE'],
      [21, @@eqp1, 'NotBalanced', 'FALSE'],
      [21, @@eqp1, 'NotBalanced', 'FALSE'],
      [21, @@eqp1, 'NotBalanced', 'FALSE'],
    ]
    assert @@rtdtest.verify_rows(expected), 'wrong RTD data'
  end

  def test27_2eqps_same_lots_diff
    # 2 eqps at same location
    prepare_eqps([@@eqp1, @@eqp2])
    # 4 test lots at [op1, op2, op2, op1], in stocker at loc1
    assert_equal 0, @@sv.lot_cleanup(@@testlots[1], op: @@op1, stocker: @@sto1)
    assert_equal 0, @@sv.lot_cleanup(@@testlots[2], op: @@op2, stocker: @@sto1)
    assert_equal 0, @@sv.lot_cleanup(@@testlots[3], op: @@op2, stocker: @@sto1)
    assert_equal 0, @@sv.lot_cleanup(@@testlots[4], op: @@op1, stocker: @@sto1)
    # call the rule and verify response
    expected = [
      [[], @@eqp1, 'Local', 'TRUE'],
      [[12, 21], @@eqp2, 'Local', 'TRUE'],
      [[12, 21], @@eqp2, 'Local', 'TRUE'],
      [[], @@eqp1, 'Local', 'TRUE'],
    ]
    assert @@rtdtest.verify_rows(expected), 'wrong RTD data'
  end

  def test28_2eqps_diff_lots_diff
    # 2 eqps at different locations
    prepare_eqps([@@eqp1, @@eqp3])
    # 4 test lots at [op1, op2, op2, op1], in stocker at loc1
    assert_equal 0, @@sv.lot_cleanup(@@testlots[1], op: @@op1, stocker: @@sto1)
    assert_equal 0, @@sv.lot_cleanup(@@testlots[2], op: @@op2, stocker: @@sto1)
    assert_equal 0, @@sv.lot_cleanup(@@testlots[3], op: @@op2, stocker: @@sto1)
    assert_equal 0, @@sv.lot_cleanup(@@testlots[4], op: @@op1, stocker: @@sto1)
    # call the rule and verify response
    expected = [
      [[], @@eqp1, 'Local', 'TRUE'],
      [[12, 21], @@eqp3, 'Global', 'TRUE'],
      [[12, 21], @@eqp3, 'NotBalanced', 'FALSE'],
      [[], @@eqp1, 'Local', 'TRUE'],
    ]
    assert @@rtdtest.verify_rows(expected), 'wrong RTD data'
  end

  # different cascades

  def test31_2eqps_same_lots_diff
    # eqp1 (Loc1, last recipe LR1 1x), eqp3 (Loc2, last recipe LR2 1x)
    prepare_eqps({@@eqp1=>@@op1, @@eqp2=>@@op2})
    # 4 test lots at [op1, op1, op2, op2], in stocker at loc1
    assert_equal 0, @@sv.lot_cleanup(@@testlots[1], op: @@op1, stocker: @@sto1)
    assert_equal 0, @@sv.lot_cleanup(@@testlots[2], op: @@op1, stocker: @@sto1)
    assert_equal 0, @@sv.lot_cleanup(@@testlots[3], op: @@op2, stocker: @@sto1)
    assert_equal 0, @@sv.lot_cleanup(@@testlots[4], op: @@op2, stocker: @@sto1)
    # call the rule and verify response
    expected = [
      [[], @@eqp1, 'Local', 'TRUE'],
      [[], @@eqp1, 'Local', 'TRUE'],
      [[12, 21], @@eqp2, 'Local', 'TRUE'],
      [[12, 21], @@eqp2, 'Local', 'TRUE'],
    ]
    assert @@rtdtest.verify_rows(expected), 'wrong RTD data'
  end

  def test32_2eqps_diff_lots_diff
    # eqp1 (Loc1, last recipe LR1 1x), eqp3 (Loc2, last recipe LR2 1x)
    prepare_eqps({@@eqp1=>@@op1, @@eqp3=>@@op2})
    # 4 test lots at [op1, op1, op2, op2], in stocker at loc1
    assert_equal 0, @@sv.lot_cleanup(@@testlots[1], op: @@op1, stocker: @@sto1)
    assert_equal 0, @@sv.lot_cleanup(@@testlots[2], op: @@op1, stocker: @@sto1)
    assert_equal 0, @@sv.lot_cleanup(@@testlots[3], op: @@op2, stocker: @@sto1)
    assert_equal 0, @@sv.lot_cleanup(@@testlots[4], op: @@op2, stocker: @@sto1)
    # call the rule and verify response
    expected = [
      [[], @@eqp1, 'Local', 'TRUE'],
      [[], @@eqp1, 'Local', 'TRUE'],
      [[12, 21], @@eqp3, 'Global', 'TRUE'],
      [[12, 21], @@eqp3, 'NotBalanced', 'FALSE'],
    ]
    assert @@rtdtest.verify_rows(expected), 'wrong RTD data'
  end

  def test33_2eqps_diff_lots_diff_reversed
    # eqp1 (Loc1, last recipe LR1 1x), eqp3 (Loc2, last recipe LR2 1x)
    prepare_eqps({@@eqp1=>@@op1, @@eqp3=>@@op2})
    # 4 test lots at [op2, op2, op1, op1], in stocker at loc1
    assert_equal 0, @@sv.lot_cleanup(@@testlots[1], op: @@op2, stocker: @@sto1)
    assert_equal 0, @@sv.lot_cleanup(@@testlots[2], op: @@op2, stocker: @@sto1)
    assert_equal 0, @@sv.lot_cleanup(@@testlots[3], op: @@op1, stocker: @@sto1)
    assert_equal 0, @@sv.lot_cleanup(@@testlots[4], op: @@op1, stocker: @@sto1)
    # call the rule and verify response
    expected = [
      [[21], @@eqp3, 'Global', 'TRUE'],
      [[12], @@eqp1, 'Local', 'TRUE'],
      [[], @@eqp1, 'Local', 'TRUE'],
      [[21], @@eqp3, 'NotBalanced', 'FALSE'],
    ]
    assert @@rtdtest.verify_rows(expected), 'wrong RTD data'
  end

  def test34_2eqps_diff_lots_diff_mixed
    # eqp1 (Loc1, last recipe LR2 1x), eqp3 (Loc2, last recipe LR1 1x)
    prepare_eqps({@@eqp1=>@@op2, @@eqp3=>@@op1})
    # 4 test lots at [op1, op1, op2, op2], in stocker at loc1
    assert_equal 0, @@sv.lot_cleanup(@@testlots[1], op: @@op1, stocker: @@sto1)
    assert_equal 0, @@sv.lot_cleanup(@@testlots[2], op: @@op2, stocker: @@sto1)
    assert_equal 0, @@sv.lot_cleanup(@@testlots[3], op: @@op1, stocker: @@sto2)
    assert_equal 0, @@sv.lot_cleanup(@@testlots[4], op: @@op2, stocker: @@sto2)
    # call the rule and verify response
    expected = [
      [[12], @@eqp1, 'Local', 'TRUE'],
      [[], @@eqp1, 'Local', 'TRUE'],
      [[12, 21], @@eqp3, 'Local', 'TRUE'],
      [[21], @@eqp3, 'Local', 'TRUE'],
    ]
    assert @@rtdtest.verify_rows(expected), 'wrong RTD data'
  end

  def test35_2eqps_diff_lots_diff_other
    # eqp1 (Loc1, last recipe LR2 1x), eqp3 (Loc2, last recipe LR1 1x)
    prepare_eqps({@@eqp1=>@@op2, @@eqp3=>@@op1})
    # 4 test lots at [op2, op2, op1, op1], in stocker at loc1
    assert_equal 0, @@sv.lot_cleanup(@@testlots[1], op: @@op1, stocker: @@sto2)
    assert_equal 0, @@sv.lot_cleanup(@@testlots[2], op: @@op2, stocker: @@sto2)
    assert_equal 0, @@sv.lot_cleanup(@@testlots[3], op: @@op1, stocker: @@sto2)
    assert_equal 0, @@sv.lot_cleanup(@@testlots[4], op: @@op2, stocker: @@sto2)
    # call the rule and verify response
    expected = [
      [[12, 21], @@eqp3, 'Local', 'TRUE'],
      [[21], @@eqp1, 'Global', 'TRUE'],
      [[12, 21], @@eqp3, 'Local', 'TRUE'],
      [[21], @@eqp1, 'NotBalanced', 'FALSE'],
    ]
    assert @@rtdtest.verify_rows(expected), 'wrong RTD data'
  end

  def test36_2eqps_same_lots_diff_other  # Test16
    # eqp1 (Loc1, last recipe LR1 1x), eqp2 (Loc1, last recipe LR2 1x)
    prepare_eqps({@@eqp1=>@@op1, @@eqp2=>@@op2})
    # 4 test lots at [op1, op1, op2, op2], in stocker at loc1
    assert_equal 0, @@sv.lot_cleanup(@@testlots[1], op: @@op1, stocker: @@sto2)
    assert_equal 0, @@sv.lot_cleanup(@@testlots[2], op: @@op1, stocker: @@sto2)
    assert_equal 0, @@sv.lot_cleanup(@@testlots[3], op: @@op2, stocker: @@sto2)
    assert_equal 0, @@sv.lot_cleanup(@@testlots[4], op: @@op2, stocker: @@sto2)
    # call the rule and verify response
    expected = [
      [[21], @@eqp1, 'Global', 'TRUE'],
      [[21], @@eqp1, 'NotBalanced', 'FALSE'],
      [[12, 21], @@eqp2, 'Global', 'TRUE'],
      [[12, 21], @@eqp2, 'NotBalanced', 'FALSE'],
    ]
    assert @@rtdtest.verify_rows(expected), 'wrong RTD data'
  end


  # aux methods

  # enable or disable all eqps in the machine recipe as required, open cascades if required
  def prepare_eqps(cascades)
    # cascades is either an array of eqps (no new cascade) or a hash of eqp=>op
    eqps = cascades.instance_of?(Hash) ? cascades.keys : cascades
    [@@eqp1, @@eqp2, @@eqp3].each {|e|
      assert_equal 0, @@sv.eqp_cleanup(e, mode: eqps.member?(e) ? 'Auto-3' : 'Off-Line-1')
    }
    if cascades.instance_of?(Hash)
      $log.info "-- creating cascades"
      cascades.each_pair {|eqp, op_and_count|
        if op_and_count.instance_of?(Array)
          op, count = op_and_count
        else
          op, count = op_and_count, 1
        end
        opclear = (op == @@op1) ? @@op2 : @@op1
        lot = @@testlots[0]
        assert_equal 0, @@sv.lot_cleanup(lot, stocker: @@sto1, op: opclear)
        assert @@sv.claim_process_lot(lot, eqp: eqp, mode: 'Auto-3')
        count.times {
          assert_equal 0, @@sv.lot_cleanup(lot, stocker: @@sto1, op: op)
          assert @@sv.claim_process_lot(lot, eqp: eqp, mode: 'Auto-3')
        }
      }
      $log.info "-- created cascades #{cascades}"
    end
  end

end
