=begin 
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.
    
	load_run_testcase 'Test_QA_DEFAULT_WHAT_NEXT_Lot_Info', "itdc"
	
Author: Paul Peschel, 2012-07-12
Version: 1.0.0

=end

require "RubyTestCase"
require 'rtdruletest'
require 'rtdclient'

# Testcases for RTD
#
# If run testcases alone over "load_run_testcase 'Test_QA_DEFAULT_WHAT_NEXT_Lot_Info', "itdc", :tp => /20/" you must do first:
#
# 1. @@lot = ""
#
# 2. @@lot2 = ""
#
# 3. @@lots = ""
#
# 4. Call "test01_prepare_test", because without that, you have no Lot
#
# 5. Or call for each Testcase also test "test01_prepare_test" means "load_run_testcase 'Test_QA_DEFAULT_WHAT_NEXT', "itdc", :tp => /01|20/"
 
class Test_QA_DEFAULT_WHAT_NEXT_Lot_Info < RubyTestCase

  @@waittime = 10

  def setup
    super
    $rtdt = RTD::RuleTest.new($env, :sv=>$sv) unless $rtdt and $rtdt.env == $env
    $rtdc = RTD::Client.new($env) unless $rtdc and $rtdc.env == $env
    assert $setup_ok, "test setup is not correct"
  end
  
  # ###################################################### Setup des Tests ##########################################################
  #
  # Prepare all equipment and lots for the Test
  #
  # See also testsetup in excel file Testsetup.xls in sheet "Testsetup_QA_DEFAULT_WHAT_NEXT". 
  # Here described all important stuff for the tests in this File.
  #
  # http://myteamsdrs2/sites/FICSQA/_layouts/xlviewer.aspx?id=/sites/FICSQA/Documentation/Technical%20Documentation/RTD/Testcases_QA_DEFAULT_WHAT_NEXT.xlsx
  
  def test01_prepare_test 
    lot_count = 2
    @@lots = $rtdt.setup_for_test(lot_count)
    @@lot, @@lot2 = @@lots
    return @@lot + @@lot2 + @@lots.to_s
  end
  
  ####################################################### Testcases for RTD #########################################################
  
  # *Testcase*: QualEqpID
  #
  # *Number*: 20
  #
  # *Description*: Lots with set SiView lot scriptparameter "QualEqPID" allowed only show on Eqp = QualEqpID.
  #
  # *Logic*:  
  #
  # is_qual_lot = (FULL QualEqpID) AND (MATCH(OperationReportingType,"*-QUAL*"))
  #
  # is_qual_eqp = IF is_qual_lot THEN  QualEqpID == eqp_id ELSE FALSE
  #
  # FILTER := (is_qual_lot AND is_qual_eqp_id) OR (NOT is_qual_lot)
  #
  # *Condition*:
  #
  # OperationReportingType ist pd UDATA, QualEqpID ist LotScriptParameter
  #
  # *Setup*: 
  # * Product: RTDT-PRODUCT.02
  # * Tools: RTDT01, RTDT02, RTDT03
  # * PD: 1000.1000	
  # * Scriptparameter: QualEqpID
  # * Carriercategory: BEOL
  # * Splitlot: No
  
  def test20_QualEqpID
    # Start values
    sp = "QualEqpID"
    tool = $sv.lot_info(@@lot2).opEqps
    
    # Test Cleanup
    assert $rtdt.eqp_cleanup(tool)
    assert $rtdt.lot_carrier_cleanup(@@lot2)
    assert $rtdt.stocker_cleanup()
    
    # Testcase: Lot with Scriptparameter "QualEqPID" can only show in Eqp = QualEqpID
    tool = $sv.lot_info(@@lot2).opEqps[0..2]
    scriptparam = ["ToolNotSet"] + tool
    scriptparam.each {|s|
      assert_equal 0, $sv.user_parameter_change("Lot", @@lot2, sp, s, 1, :datatype=>"STRING") unless s == scriptparam[0]
      sleep @@waittime
      tool.each {|t| 
        list = $sv.rtd_dispatch_list(t)
        convert_list = $rtdt.change_list_to_hash(list, @@lot2)
        $result_rtd << "#{__method__}: Lot with Scriptparameter #{sp} with value #{s} should not be on Tool #{t}" unless (convert_list["Lot ID"] == @@lot2) == (s == "ToolNotSet" || s == t)}}
    assert_equal 0, $sv.user_parameter_change("Lot", @@lot2, sp, "", 3, :datatype=>"STRING")
  end
  
  # *Testcase*: carrier_category
  #
  # *Number*: 35
  #
  # *Description*: Show lots with appropriate SiView required carrier category (operation) / ProtocolZone from the carriers versus on tool available inputports.
  #
  # *Logic*: 
  #
  # Show lots with appropriate SiView required carrier category (operation)
  #
  # *Condition*:    
  #
  # ProtocolZone from the carriers versus on tool available inputports
  #
  # *Setup*: 
  # * Product: RTDT-PRODUCT.01
  # * Tools: RTDT01, RTDT02, RTDT03
  # * PD: 1000.1000
  # * Scriptparameter:
  # * Carriercategory: BEOL, FEOL, MOL
  # * Splitlot: No
  
  def test35_carrier_category
    # Start values
    cbeol = $sv.carrier_list(:category=>"BEOL", :status=>"AVAILABLE", :carrier =>"G%", :empty=>true).first
    cmol = $sv.carrier_list(:category=>"MOL", :status=>"AVAILABLE", :carrier =>"G%", :empty=>true).first
    cfeol = $sv.carrier_list(:category=>"FEOL", :status=>"AVAILABLE", :carrier =>"G%", :empty=>true).first
    tool = $sv.lot_info(@@lot).opEqps
    cl = [cbeol, cfeol, cmol]
    cg = ["BEOL", "FEOL", "MOL"]
    
    # Test Cleanup
    assert $rtdt.eqp_cleanup(tool)
    assert $rtdt.lot_carrier_cleanup(@@lot)
    assert $rtdt.stocker_cleanup()
    
    # Testcase: Show Lots with requiered Carrier Category (Operation) / ProtocolZone from the Carriers versus free Ports on Tool
    tool = $sv.lot_info(@@lot).opEqps[0..2]
    cl.each {|c|
      assert_equal 0,  $sv.carrier_xfer_status_change(c, "SO", "INTER1")
      assert_equal 0,  $sv.carrier_xfer_status_change(c, "MI", "UTSTO11")
      assert_equal 0,  $sv.wafer_sort_req(@@lot, c)
      sleep @@waittime
      tool.each {|t|
        eqp = $sv.eqp_info(t)
        list = $sv.rtd_dispatch_list(t)
        convert_list = $rtdt.change_list_to_hash(list, @@lot)
        $result_rtd << "#{__method__}: Carriercategory #{c} does not match tool capatility #{t}" unless (convert_list["Lot ID"] == @@lot) == (eqp.ports[0].category.member?(cg[cl.index(c)]))}}
    assert_equal 0, $sv.wafer_sort_req(@@lot, cbeol)
  end
  
  # *Testcase*: Target Location
  #
  # *Number*: 40
  #
  # *Description*: Show lots with appropriate target location versus physical location (carrier location) versus eqpuipment location
  #
  # *Logic*:  
  #
  # Show lots with appropriate target location versus physical location (carrier location) versus eqpuipment location
  #
  # *Condition*:    
  #
  # *Setup*: 
  # * Product: RTDT-PRODUCT.01
  # * Tools: RTDT01, RTDT02, RTDT03, RTDT04, RTDT05
  # * PD: 1000.1000
  # * Scriptparameter:
  # * Carriercategory: BEOL
  # * Splitlot: No
  
  def test40_target_location    
    # Test Cleanup
    tool = $sv.lot_info(@@lot).opEqps
    assert $rtdt.eqp_cleanup(tool)
    assert $rtdt.lot_carrier_cleanup(@@lot)
    assert $rtdt.stocker_cleanup()
    
    # Start values
    li = $sv.lot_info(@@lot)    
    
    # Testcase Show Lots with set special Target Location versus physical Location (Carrier Location) versus Eqpuipment Location
    tool = $sv.lot_info(@@lot).opEqps
    toolm1 = tool[0..2] # Location UData = [M1]
    toolbtf = tool[3..4] # Location UData = [BTF]
    # Testcase 1: Check toolm1
    assert_equal 0, $sv.carrier_xfer_status_change(li.carrier, "EO", toolm1[0]) #Location: UData = M1
    sleep @@waittime
    tool.each {|t|
      list = $sv.rtd_dispatch_list(t)
      convert_list = $rtdt.change_list_to_hash(list, @@lot)
      #$result_rtd << "#{__method__}_1: Last Location #{toolm1[0]} is not the same target location, #{t}" unless (convert_list["Lot ID"] == @@lot) == (toolm1.member?(t))}      
      $result_rtd << "#{__method__}_1: Lot #{@@lot} is not found with State A3 = Yes, actual Location is #{t}" unless (convert_list["A3"] == "N")
      $result_rtd << "#{__method__}_1: Lot #{@@lot} A3 Info is '#{convert_list["A3Info"]}' instead of 'XferState gleich EI oder EO'"  unless (convert_list["A3Info"] == "XferState gleich EI oder EO") == toolm1.member?(t)
      $result_rtd << "#{__method__}_1: Lot #{@@lot} A3 Info is '#{convert_list["A3Info"]}' instead of 'Carrier at other location'" unless (convert_list["A3Info"].split('|').member?("Carrier at other location")) == toolbtf.member?(t)}

    # Testcase 2: Check toolbtf
    assert_equal 0, $sv.carrier_xfer_status_change(li.carrier, "EO", toolbtf[0]) #Location: UData = BTF
    sleep @@waittime
    tool.each {|t|
      list = $sv.rtd_dispatch_list(t)
      convert_list = $rtdt.change_list_to_hash(list, @@lot)
      #$result_rtd << "#{__method__}_2: Last Location #{toolm1[0]} is not the same target location, #{t}" unless (convert_list["Lot ID"] == @@lot) == (toolbtf.member?(t))}
      $result_rtd << "#{__method__}_2: Lot #{@@lot} is not found with State A3 = Yes, actual Location is #{t}" unless (convert_list["A3"] == "N")
      $result_rtd << "#{__method__}_2: Lot #{@@lot} A3 Info is '#{convert_list["A3Info"]}' instead of 'Carrier at other location'"  unless (convert_list["A3Info"].split('|').member?("Carrier at other location")) == toolm1.member?(t)
      $result_rtd << "#{__method__}_2: Lot #{@@lot} A3 Info is '#{convert_list["A3Info"]}' instead of 'XferState gleich EI oder EO'" unless (convert_list["A3Info"] == "XferState gleich EI oder EO") == toolbtf.member?(t)}
    $rtdt.prepare_carrier(@@lot)
  end
  
  # ###################################################### Result and Delete Lots ###################################################
  
  def test999_results
    puts "Test OK" if $result_rtd.size == 0
    puts "Test NOK, #{$result_rtd.size} failures found" if $result_rtd.size > 0
    
    $result_rtd.each {|i|
      pp i
      if $result_rtd.index(i)%10 == 9
        print "show more Errors, press y?"
        res_input = gets
        break unless res_input.start_with?("y")
      end} if $result_rtd.size > 0
    $rtdt.delete_lots(@@lots)
  end
end
  