=begin 
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.
    
Author: ssteidte, 2014-05-21

to enable the checks:
1) SP_PRIVILEGECHECK_FOR_CAST = 1
2) SP_PRIVILEGECHECK_FOR_CJ = 1
3) Tx must have FIMMTRAN.PROD_STATE = 1
=end

require 'RubyTestCase'


class Stress_ProductGroupUserGroup < RubyTestCase
  Description = "Test performance of transactions when the product group has a user group assigned, MSR744625"
  
  # secured
  @@pg1 = 'UTGRP1'
  @@p1 = 'UTGRP1.01'
  @@r1 = 'UTGRP1.01'
  @@lots1 = ["UXYL22011.000", "UXYL22012.000", "UXYL22013.000", "UXYL22014.000", "UXYL22015.000", "UXYL22016.000", "UXYL22017.000", "UXYL22018.000", "UXYL22019.000", "UXYL22020.000"]
  # unsecured
  @@pg2 = 'UTGRP2'
  @@p2 = 'UTGRP2.01'
  @@r2 = 'UTGRP2.01'
  @@lots2 = ["UXYL22021.000", "UXYL22022.000", "UXYL22023.000", "UXYL22024.000", "UXYL22025.000", "UXYL22026.000", "UXYL22027.000", "UXYL22028.000", "UXYL22029.000", "UXYL22030.000"]
  
  @@loops = 10
  
  def test00_setup
    $setup_ok = false
    assert_equal '1', $sv.environment_variable[1]['SP_PRIVILEGECHECK_FOR_CAST'], 'SP_PRIVILEGECHECK_FOR_CAST not set'
    assert_equal '1', $sv.environment_variable[1]['SP_PRIVILEGECHECK_FOR_CJ'], 'SP_PRIVILEGECHECK_FOR_CJ not set'
    assert $svnox = SiView::MM.new($env, user: 'NOX-UNIT', password: :default)
    assert $sm = SiView::SM.new($env)
    # verify secured setup
    assert_equal @@pg1, $sm.object_info(:product, @@p1.split('.').first).first.specific[:productgroup], "wrong product"
    pgusers = $sm.object_info(:productgroup, @@pg1).first.specific[:usergroups]
    usergrps = $sm.object_info(:user, $sm._userinfo.userId).first.specific[:usergroups]
    assert_not_equal [], pgusers & usergrps, "test user #{$sm._userinfo.userId} is not in the user groups for pgrp #{@@pg1}"
    @@lots1 ||= 10.times.collect {$sv.new_lot_release(stb: true, route: @@r1, product: @@p1)}.compact
    assert_equal 10, @@lots1.size, "not enough lots of product #{@@p1}"
    $log.info "using secured product #{@@p1}, productgroup #{@@pg1}"
    # verify unsecured setup
    assert_equal @@pg2, $sm.object_info(:product, @@p2.split('.').first).first.specific[:productgroup], "wrong product"
    assert_equal [], $sm.object_info(:productgroup, @@pg2).first.specific[:usergroups], "product5group #{@@pg2} must have no user groups"
    @@lots2 ||= 10.times.collect {$sv.new_lot_release(stb: true, route: @@r2, product: @@p2)}.compact
    assert_equal 10, @@lots2.size, "not enough lots of product #{@@p2}"
    $log.info "using unsecured product #{@@p2}, productgroup #{@@pg2}"
    $setup_ok = true
  end
  
  def test11_lotinfo
    assert_nil $svnox.lots_info(@@lots1), "product group security is not effective"
    tstart = Time.now
    @@loops.times {|i| $sv.lots_info(@@lots1, raw: true)}
    t1 = Time.now
    @@loops.times {|i| $sv.lots_info(@@lots2, raw: true)}
    tend = Time.now
    $log.info "lotinfo, duration secured: #{t1 - tstart}s, unsecured: #{tend - t1}s"
  end
  
  def test12_slr
    (@@lots1 + @@lots2).each {|lot| assert_equal 0, $sv.lot_cleanup(lot)}
    eqp = 'O-LOTSTART'
    assert_equal 0, $sv.eqp_cleanup(eqp)
    #
    tstart = Time.now
    @@lots1.each {|lot| cj = $sv.slr(eqp, lot: lot); $sv.slr_cancel(eqp, cj)}
    t1 = Time.now
    @@lots2.each {|lot| cj = $sv.slr(eqp, lot: lot); $sv.slr_cancel(eqp, cj)}
    tend = Time.now
    $log.info "slr, duration secured: #{t1 - tstart}s, unsecured: #{tend - t1}s"
  end
  
  
  def self.after_all_passed
  end
end
