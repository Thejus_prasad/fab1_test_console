=begin 
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.
    
Author:  Steffen Steidten, 2013-06-27
Version: 1.1.0

History:
  2015-10-06 sfrieske,  use SiView ::InstrumentedServiceManager, change UDATA count
=end

require 'SiViewTestCase'
require 'charts'


# Test Carrier and Eqp Tx performance when there is UDATA attached
class Stress_CarrierUDATA < SiViewTestCase
  @@carriers = 'UT%'
  @@eqp = 'UTF001'
  @@eqpib = 'UTI001'
  @@udataprefix = 'LET_UDATA_STRESSTEST_'
  @@udatanumbers = [1, 2, 5, 10, 30, 70, 100]
  @@loops = 10
  
  def self.startup
    super(collect_durations: true)
  end

  def self.shutdown
    $svtest = nil
    $sv = nil
    $sm = nil
  end


  def test1
    @@cc = $sv.carrier_list(carrier: @@carriers)
    udatas = $sm.udata_attrs(:carrier).collect {|e| e.name if e.name.start_with?(@@udataprefix)}.compact
    assert udatas.size >= 100, "must have at least 100 UDATAs"
    # remove all UDATAs
    uds = Hash[udatas.collect {|e| [e, '']}]
    assert $sm.update_udata(:carrier, @@cc, uds)
    #
    @@result = @@udatanumbers.collect {|n|
      $log.info "-- testing with #{n} UDATAs"
      # set UDATAs
      uds = Hash[udatas.take(n).collect {|e| [e, 'QAStressTest']}]
      assert $sm.update_udata(:carrier, @@cc, uds)
      @@cc.each {|c| 
        assert_equal n, $sv.carrier_basicinfo(@@cc.first).first.udata.size, "wrong UDATA size"
      }
      $sv.manager.durations = []
      # call TXen
      @@loops.times {|i|
        $log.info "loop #{i+1}/#{@@loops}"
        assert $sv.carrier_list(carrier: @@carriers, raw: true)
        assert $sv.carrier_list(carrier: @@carriers, raw: true, customized: true)
        assert $sv.carrier_basicinfo(@@cc, raw: true)
        @@cc.each {|c| assert $sv.carrier_status(c, raw: true)}
        @@cc.each {|c| assert $sv.carrier_xfer_jobs(c, raw: true)}
        @@cc.each {|c| assert $sv.carrier_xfer_jobs(c, raw: true, mcs: true)}
        @@cc.each {|c| assert $sv.lot_list_incassette(c, raw: true)}
        assert $sv.eqp_info(@@eqp, ib: false, raw: true)
        assert $sv.eqp_info(@@eqpib, ib: true, raw: true)
      }
      [n, $sv.manager.get_statistics]
    }
    #
    @@chartdata = {}
    @@result.each {|n, stats|
      res = stats[:time].iso8601
      res += "\n#{@@loops} loops, #{@@cc.size} carriers, #{n} UDATA each\n\n"
      stats[:data].each {|txdata|
        tx = txdata[0]
        tx += '__' + txdata[1] unless txdata[1].empty?
        tt = txdata[2][1]
        @@chartdata[tx] ||= []
        @@chartdata[tx] << [n, tt.avg]
        res += '%-40.40s' % tx + '% 6.1f' % tt.avg + ' +- % 6.1f s' % tt.standard_deviation + " (#{(tt.standard_deviation/tt.avg * 100).round}%)\n"
      }
      $log.info "result:\n\n#{res}\n"
    }
    time = Time.now.utc.iso8601.gsub(':', '-')
    @@chartdata.each_pair {|tx, data|
      fname = "log/carrierudata_#{$env}_#{time}_#{tx}.png"
      $log.info "creating chart #{fname}"
      ch = Chart::XY.new(data)
      ch.create_chart(title: "Carrier UDATA: #{tx}", x: 'n UDATA', y: 't (ms)', dotsize: 6, export: fname)
    }
  end

end
