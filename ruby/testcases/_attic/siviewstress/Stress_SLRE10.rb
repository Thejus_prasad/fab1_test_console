=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: Paul Peschel, 2013-02-21 migrate from StressEqpInfoInternalBuffer.java

History:
  2015-09-03 dsteger,  MSR943986 added
  2015-10-01 sfrieske, added test for LotsInfoForStartLotsReservation (found in R15C7.1.0)
  2021-09-15 sfrieske, finally replaced $svtest by @@svtest
=end

require 'SiViewTestCase'


# Testcase to replicate error when doing equipment info while unloading lot report is ongoing.
# MSR943986 (eqp info during opecomp), MSR193114 (eqp load/unload) http://gfjira/browse/MES-2514
class Stress_SLRE10 < SiViewTestCase
  @@product = 'UT-INHIBIT.01'
  @@route = 'UTRT-INHIBIT.01'
  @@eqp = 'UTI002'
  @@e10states = ['1PRD', '1SUP', '1PRD', '2WPR']
  @@loops = 100


  def self.startup
    super
    @@svtest.product = @@product
    @@svtest.route = @@route
    @@testlots = []
  end

  def self.shutdown
    @@t1.kill
    @@testlots.each {|lot| $sv.delete_lot_family(lot)}
  end


  def setup
    assert_equal 0, $sv.eqp_cleanup(@@eqp)
    @@testlots.each {|lot| assert_equal 0, $sv.lot_cleanup(lot, opNo: @@opNo)}
  end


  def test00_setup
    $setup_ok = false
    #
    # ensure eqp is Multiple Recipe
    ##assert_equal 'Multiple Recipe', $sm.object_info(:eqp, @@eqp).first.specific[:mrtype], "eqp type must be Multiple Recipe"
    # clean up eqp
    assert_equal 0, $sv.eqp_mode_change(@@eqp, 'Off-Line-1', notify: false)
    # create test lots
    assert @@lot = @@svtest.new_lot
    @@testlots = [@@lot]
    assert e = $sv.lot_eqplist_in_route(@@lot).find {|k, vv| vv.include?(@@eqp)}, "no op for eqp #{@@eqp}"
    @@opNo = e[0]
    #
    $setup_ok = true
  end

  def test10_slr  #  MES-3317
    lot = @@lot
    assert_equal 0, $sv.lot_opelocate(lot, @@opNo)
    carrier = $sv.lot_info(lot).carrier
    running = true
    testok = true
    @@t1 = Thread.new {
      while running
        @@e10states.each {|state| $sv.eqp_status_change(@@eqp, state)}
      end
    }
    100.times {
      cj = $sv.slr(@@eqp, slr_retries: 1, ib: true, lot: [lot], carrier: [carrier])
      if cj
        $sv.slr_cancel(@@eqp, cj)
      else
        testok = false
        running = false
        break
      end
    }
    running = false
    @@t1.join
    assert testok
  end


end
