=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: sfrieske, 2016-10-12

History:
  2021-09-15 sfrieske, finally replaced $svtest by @@svtest
=end

require 'SiViewTestCase'
require 'util/charts'


# Performance test calling Txs by processing a lot along a route
class Perf_ClaimProcessLot < SiViewTestCase
  @@routelong = 'UTRT001.99'  # 90+ operations
  @@searchcount = 90
  @@loops = 3

  def self.after_all_passed
    @@testlots.each {|lot| $sv.delete_lot_family(lot)}
  end


  def test00_setup
    $setup_ok = false
    #
    @@testlots = []
    refute_empty user2 = $sv.environment_variable[1]['CS_SP_PERSON_FOR_PASSCOUNT_ON_LOTOPELIST'], "no user configured"
    assert @@sv2 = SiView::MM.new($env, user: user2, password: :default, collect_durations: true)
    #
    $setup_ok = true
  end

  def test91_passcount_perf
    assert lot = @@svtest.new_lot, "error creating test lot"
    @@testlots << lot
    assert_equal 0, $sv.schdl_change(lot, route: @@routelong)
    # get operation list for gate_pass
    assert opes = $sv.lot_operation_list(lot)
    assert opes.length > @@searchcount, "route is not long enough"
    # warm up
    2.times {@@sv2.lot_operation_list(lot)}
    @@sv2.manager.durations = []
    @@loops.times {|n|
      $log.info "\n\n--- loop #{n+1}/#{@@loops}"
      assert_equal 0, $sv.lot_opelocate(lot, :first)
      # call lot_operation_list as special user
      assert @@sv2.lot_operation_list(lot, searchcount: @@searchcount, raw: true)
      $log.info "duration: #{@@sv2.manager.durations.last}"
      # increase passcount of all ops
      opes[0..-2].each {|ope|
        assert_equal 0, $sv.lot_gatepass(lot, route: ope.route, opNo: ope.opNo, mandatory: ope.mandatory)
      }
      $sv.lot_bankin_cancel(lot)
    }
    durations = @@sv2.manager.durations.collect {|tx, duration| tx == :TxLotOperationListInq ? duration : nil}.compact
    $log.info "durations: #{durations}"
    fname = "log/operationlist_#{$env}_#{Time.now.utc.strftime('%F_%TZ').gsub(':', '-')}.png"
    $log.info "creating chart #{fname}"
    ch = Chart::XY.new(durations.each_with_index.collect {|d, i| [i+1, d]})
    ch.create_chart(title: "LotOperationList as #{@@sv2.user}", x: 'passcount', y: 't (ms)', dotsize: 6, export: fname)
  end

  def test92_claim_process
    assert lot = @@svtest.new_lot, "error creating test lot"
    @@testlots << lot
    carrier = $sv.lot_info(lot).carrier
    assert_equal 0, $sv.schdl_change(lot, route: @@routelong)
    # get operation list for gate_pass
    assert opes = $sv.lot_operation_list(lot)
    1.times {|n|
      $log.info "\n\n--- loop #{n+1}/#{@@loops}"
      assert_equal 0, $sv.lot_opelocate(lot, :first)
      # process lot along the route
      opes[0..-2].each {|ope|
        ok = @@sv2.claim_process_lot(lot, proctime: 0)
        if !ok && [1408, 1488].include?(@@sv2.rc)
          # skip reticle not loaded issues
          ok = @@sv2.lot_gatepass(lot, route: ope.route, opNo: ope.opNo, mandatory: ope.mandatory).zero?
        end
        assert ok, "error processing lot at #{ope}"
      }
      $sv.lot_bankin_cancel(lot)
    }
    # $log.info "durations: #{durations}"
    # fname = "log/operationlist_#{$env}_#{Time.now.utc.strftime('%F_%TZ').gsub(':', '-')}.png"
    # $log.info "creating chart #{fname}"
    # ch = Chart::XY.new(durations.each_with_index.collect {|d, i| [i+1, d]})
    # ch.create_chart(title: "LotOperationList as #{@@sv2.user}", x: 'passcount', y: 't (ms)', dotsize: 6, export: fname)
  end

end
