=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: Steffen Steidten, 2011-11-25

History:
  2013-07-30 ssteidte,  adapted as after-downtime test

=end

require "RubyTestCase"
require "spacetest"


# Test Space/SIL
class AD03_Space < RubyTestCase
  Description = "Test Space/SIL"

  @@lds_inline = "Inline_Fab1"
  @@lds_setup = "Setup_Fab1"

  @@templates_folder = "_TEMPLATES"
  @@default_template = "_DEFAULT_TEMPLATE"

  @@channels = ""
  @@loops = 3

  def test0001_LDS
    $setup_ok = false
    #
    $spc.close if $spc
    $spctest = SpaceTest.new($env, :space_only=>true)
    $spc = $spctest.spc
    assert ($spc and $spc.env == $env)
    #
    $lds = $spc.lds(@@lds_inline)
    $lds_setup = $spc.lds(@@lds_setup)
    refute_nil $lds, "LDS #{@@lds_inline} not found"
    refute_nil $lds_setup, "LDS #{@@lds_setup} not found"
    #
    $setup_ok = true
  end

  def test0002_extractor_keys
    [@@lds_inline, @@lds_setup].each {|l|
      $log.info "LDS: #{l}"
      lds = $spc.ldses(l)[0]
      ekeys = lds.extractor_keys
      assert ekeys.size > 2, "wrong number of extractor keys"
    }
  end

  def test0003_templates
    folder = $lds.folder(@@templates_folder)
    assert folder.spc_channel(@@default_template), "default template #{@@default_template} does not exist"
  end

  def test0004_perf_test
    @@channels = @@channels.split
    #
    tt = @@loops.times.collect {|i|
      $log.info "-- loop #{i+1}/#{@@loops}"
      tstart = Time.now
      @@channels.each_with_index {|chid, j|
        $log.info "channel #{j+1}/#{@@channels.size}"
        channel = $spc.spc_channel(chid.to_i)
        assert channel, "failed to load channel #{chid}"
        assert channel.samples, "failed to load samples of channel #{chid}"
      }
      (Time.now - tstart).round(1)
    }
    $log.info "durations per round: #{tt.join('|')}"
  end

end
