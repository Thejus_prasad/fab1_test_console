=begin 
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.
    
Author: Daniel Steger, 2014-03-14 downtime test
=end

require 'RubyTestCase'


class ADStress_ProcessListInRoute < RubyTestCase
  @@nlots = 100
  @@lotprefix = 'U%'

  
  def test1_stress
    @@lotprefix = '8%' if $env.start_with?('f8')
    lots = $sv.lot_list(status: "Waiting", lot: @@lotprefix).take(@@nlots)
    lots.each_with_index {|lot, i|
      tstart = Time.now
      assert $sv.lot_processlist_in_route(lot, raw: true), "error executing TxRouteOperationListInq for #{lot}"
      $log.info "-- #{i+1}/#{@@nlots}  #{lot}  time: #{(Time.now - tstart).round(2)}"
    }
  end
end
