=begin 
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.
    
Author: Daniel Steger, 2014-03-14 downtime test
=end

require "RubyTestCase"

class ADStress_LotFamilyListForDeletion < RubyTestCase
  @@sublottypes = %w(PO EC QD PZ Dummy)
  
  def test1_stress
    @@sublottypes.each_with_index {|slt, i|
      tstart = Time.now
      assert $sv.lot_family_for_deletion(slt), "error executing TxLotFamiliyListForDeletionInq for #{slt}"
      $log.info "-- #{i+1}/#{sublottypes.count}  #{slt}  time: #{(Time.now - tstart).round(2)}"
    }
  end
end
