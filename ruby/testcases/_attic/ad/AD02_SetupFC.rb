=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

  2013/07/31 ukoerne2 PcmsSoap changed to general hash_to_xml
=end

require "RubyTestCase"
require 'rexml/document'
require 'rqrsp'

class AD02_SetupFC < RubyTestCase

  @@location = "C"
  @@class_code = "05"
  @@pcms_spec = "00000002"
  @@fc_spec = "00001318"
  @@user = "qauser01"
  @@password = "qauser01"
  @@scheme = "http"
  @@host = "vf1sfcd01"
  @@port = "10080"
  
  def setup
    super :nosiview=>true
  end
  
  def test00_config
    @@results = {}
    @@conf = {
      # specs
      :location => @@location,
      :class_code => @@class_code, 
      :name1 => @@pcms_spec,    # pcms spec
      :name2 =>  @@fc_spec,     # setup.FC spec
      # rest auth
      :user => @@user,
      :passw => @@password,
      # rest url
      :scheme => @@scheme,
      :host => @@host,
      :port => @@port,
    }
    @@conf[:sp1] = "#{@@conf[:location]}#{@@conf[:class_code]}-#{@@conf[:name1]}"
    @@conf[:sp2] = "#{@@conf[:location]}#{@@conf[:class_code]}-#{@@conf[:name2]}"
    @@conf[:rest_uri] = [
      {:path => '/setupfc-rest/v1/auth/token'},
      {:path => '/setupfc-rest/v1/specs', :query => "specId=#{@@conf[:sp2]}"},
      {:path => "/setupfc-rest/v1/specs/#{@@conf[:sp2]}/effective/content"},
    ]
  end
  
  # # # # #  PCMS via migration adapter
  
  def test11_pcms_find_spec
    t0 = Time.now
    p = PcmsSoap.new($env)
    assert (res = p.find_current_version(@@conf[:name1], @@conf[:class_code], :location=>@@conf[:location]))
    assert !(ver=res[:Version]).nil?, "#{@@conf[:location]}#{@@conf[:class_code]}-#{@@conf[:name1]} not found"
    assert ver =~ /[A-Z]*/ # pcms version is alphabetically
    $log.info("Specification #{@@conf[:location]}#{@@conf[:class_code]}-#{@@conf[:name1]}-#{ver} found in PCMS")
    @@results[__method__] = Time.now.to_f - t0.to_f
  end
  
  def test12_pcms_get_spec
    t0 = Time.now
    p = PcmsSoap.new($env)
    assert (res = p.get_document(@@conf[:name1], @@conf[:class_code], :location=>@@conf[:location]))
    begin
      doc = REXML::Document.new(res)
    rescue
      assert false, "request response is no valid xml\n#{res.inspect}"
    end
    assert 1==doc.elements.each('//my:myFields'){|e|}.count, 'no valid specification'
    $log.info("Specification #{@@conf[:location]}#{@@conf[:class_code]}-#{@@conf[:name1]} fetched from PCMS")
    @@results[__method__] = Time.now.to_f - t0.to_f
  end
  
  # # # # #  setup.FC via migration adapter
  
  def test21_setupFC_find_spec
    t0 = Time.now
    p = PcmsSoap.new($env)
    assert (res = p.find_current_version(@@conf[:name2], @@conf[:class_code], :location=>@@conf[:location]))
    assert !(ver=res[:Version]).nil?, "#{@@conf[:location]}#{@@conf[:class_code]}-#{@@conf[:name2]} not found"
    assert ver =~ /\d/ # setup.FC version is numerically
    $log.info("Specification #{@@conf[:location]}#{@@conf[:class_code]}-#{@@conf[:name2]}-#{ver} found in setup.FC")
    @@results[__method__] = Time.now.to_f - t0.to_f
  end
  
  def test22_setupFC_get_spec
    t0 = Time.now
    p = PcmsSoap.new($env)
    assert (res = p.get_document(@@conf[:name2], @@conf[:class_code], :location=>@@conf[:location]))
    begin
      doc = REXML::Document.new(res)
    rescue
      assert false, "request response is no valid xml\n#{res.inspect}"
    end
    assert 1==doc.elements.each('//my:myFields'){|e|}.count, 'no valid specification'
    $log.info("Specification #{@@conf[:location]}#{@@conf[:class_code]}-#{@@conf[:name2]} fetched from setup.FC")
    @@results[__method__] = Time.now.to_f - t0.to_f
  end
  
  # # # # #  Restful API setup.FC
  
  def test31_rest_get_auth
    t0 = Time.now
    assert !(res = send_rest_req(get_rest_uri, :auth_basic=>true)).nil?, ""
    @@conf[:token] = res 
    $log.info("authetication token received")
    @@results[__method__] = Time.now.to_f - t0.to_f
  end
  
  def test32_rest_find_spec
    t0 = Time.now
    assert !@@conf[:token].nil?, "no valid token for authentication"
    assert !(res = send_rest_req(get_rest_uri(1), :auth_token=>@@conf[:token])).nil?, "failed finding specification #{@@conf[:sp2]}"
    $log.info("request response successfully received")
    #
    begin
      doc = REXML::Document.new(res)
    rescue
      assert false, "request response is no valid xml\n#{res.inspect}"
    end
    assert 1==doc.elements.collect("//specifications") { |e| e }.compact.count, "specification #{@@conf[:sp2]} not found"
    $log.info("specification #{@@conf[:sp2]} found")
    @@results[__method__] = Time.now.to_f - t0.to_f
  end
  
  def test33_rest_get_spec
    t0 = Time.now
    assert !@@conf[:token].nil?, "no valid token for authentication"
    assert !(res = send_rest_req(get_rest_uri(2), :auth_token=>@@conf[:token])).nil?, "failed fetching specification #{@@conf[:sp2]}"
    $log.info("request response successfully received")
    begin
      $doc=doc = REXML::Document.new(res)
    rescue
      assert false, "request response is no valid xml\n#{res.inspect}"
    end
    assert 1==doc.elements.collect("//my:myFields") { |e| e }.compact.count, "specification #{@@conf[:sp2]} not fetched"
    $log.info("specification #{@@conf[:sp2]} fetched")
    @@results[__method__] = Time.now.to_f - t0.to_f
  end
  
  def test99_results
    $log.info("results")
    @@results.each{|k,v| puts "    #{k.to_s}:\t #{(v*100).to_i/100.00} s"}
  end
  
  # # # # #
  
  def get_rest_uri(index=0)
    uri = URI("#{@@conf[:scheme]}://#{@@conf[:host]}:#{@@conf[:port]}")
    uri.path = @@conf[:rest_uri][index][:path]
    uri.query = @@conf[:rest_uri][index][:query]
    uri
  end
  
  def send_rest_req(uri, params={})
    $log.info("#{__method__} using URL #{uri.to_s.inspect}")
    req = Net::HTTP::Get.new(uri.request_uri)
    req.basic_auth @@conf[:user], @@conf[:passw] if params[:auth_basic]
    req['X-SFC-AUTH-TOKEN'] = params[:auth_token] if params[:auth_token]

    res = Net::HTTP.start(uri.host, uri.port) {|http|
      http.request(req)
    }
    if res.code=="200"
      ret = res.body
    else
      $log.warn("#{res.inspect}")
      ret = nil
    end
    ret
  end
end

# # # # #

class PcmsSoap < RequestResponse::HTTP 
  @@namespaces = {
    "soapenv" => "http://schemas.xmlsoap.org/soap/envelope/",
    "pcms" => "urn:PegaRULES:SOAP:PackageForRMIs:SpecSharp-Data-Services" 
  }
  @@wsdl = 'http://vf1sfcq01:10080/prweb/wsdl/PackageForRMIs/SpecSharp-Data-Services.wsdl'

  def initialize(env,params={})
    @env = env
    instance = "pcmsservice.#{env}"
    super(instance)
  end

  # # # # #
  
  def find_current_version(sp_id, class_code, params={})
    loc = (params[:location] or 'C')
    $log.info("find current version for specification #{loc}#{class_code}-#{sp_id}")
    p = {
      :SpecId => {
        :Name => sp_id, 
        :LocationCode => loc, 
        :ClassCode => class_code
      }
    }
    fkt = __method__.to_s.split('_').map{|e| e.capitalize}.join
    xml = build_request(fkt, p)
    params.merge!(:soapaction=>"#{@@namespaces['pcms']}#"+fkt) if params[:soapaction].nil?
    res = _send_receive(xml, params)
    ($log.warn("specification #{loc}#{class_code}-#{sp_id} not found"); return false) if (res=res[:FindCurrentVersionResponse]).nil?
    ($log.warn("specification #{loc}#{class_code}-#{sp_id} not found"); return false) if (res=res[:CurrentVersion]).nil?
    res
  end
  
  def get_document(sp_id, class_code, params={})
    loc = (params[:location] or 'C')
    $log.info("get specification #{loc}#{class_code}-#{sp_id}")
    p = {
      :SpecId => {
        :Name => sp_id, 
        :LocationCode => loc, 
        :ClassCode => class_code,
      }
    }
    p[:SpecId][:Version] = params[:version]
    p[:SpecId][:UserID] = (params[:user] or @user)
    p[:SpecId][:UserPassword] = (params[:pass] or @password.unpack('m'))
    fkt = __method__.to_s.split('_').map{|e| e.capitalize}.join
    #
    xml = build_request(fkt, p)
    params.merge!(:soapaction=>"#{@@namespaces['pcms']}#"+fkt) if params[:soapaction].nil?
    res = _send_receive(xml, params)
    ($log.warn("specification #{loc}#{class_code}-#{sp_id} not fetched"); return false) if (res=res[:GetDocumentResponse]).nil?
    ($log.warn("specification #{loc}#{class_code}-#{sp_id} not fetched"); return false) if (res=res[:SpecContents]).nil?
    ($log.warn("specification #{loc}#{class_code}-#{sp_id} not fetched"); return false) unless res[:Format]=='XML'
    ($log.warn("specification #{loc}#{class_code}-#{sp_id} not fetched"); return false) if (res=res[:IPContents]).nil?
    res
  end
  
  # # # # #
  
  def operations
    url = @@wsdl
    uri = URI(url)
    #
    req = Net::HTTP::Get.new(uri.request_uri)
    res = Net::HTTP.start(uri.host, uri.port) {|http|
      http.request(req)
    }
    wsdl = REXML::Document.new res.body
    wsdl.elements.collect('//operation'){|e| e.attributes['name']}.sort.uniq
  end
  
  def addAttachment; not_implemented end
  def addAttachmentV2; not_implemented end
  def deleteAttachment; not_implemented end
  # def findCurrentVersion; not_implemented end
  def findCurrentVersions; not_implemented end
  def findCurrentVersionsV2; not_implemented end
  def findDocuments; not_implemented end
  def findDocumentsByKeyword; not_implemented end
  def findDocumentsByKeywordV2; not_implemented end
  def findDocumentsBySubClass; not_implemented end
  def findDocumentsBySubclassC4; not_implemented end
  def findDocumentsByUnit; not_implemented end
  def findDocumentsV2; not_implemented end
  def getAttachment; not_implemented end
  def getAttachmentV2; not_implemented end
  def getAttachmentV3; not_implemented end
  # def getDocument; not_implemented end
  def getDocumentV2; not_implemented end
  def getDocumentV3; not_implemented end
  def getEquipments; not_implemented end
  def getListOfAttachments; not_implemented end
  def getListOfAttachmentsV2; not_implemented end
  def getMetaData; not_implemented end
  def getMetaDataV2; not_implemented end
  def getMetaDataV3; not_implemented end
  
  def not_implemented(); $log.warn "#{caller[0][/`.*'/][1..-2]} NOT IMPLEMENTED"; nil end

  def build_request(fname, params={})
    h = {"SOAP-ENV:Envelope"=>{"xmlns:SOAP-ENV"=>@@namespaces["soapenv"], "xmlns:urn"=>@@namespaces["pcms"],
      "SOAP-ENV:Body".to_sym=>{
          ("urn:"+fname).to_sym=>params
      }
    }}
    hash_to_xml(h)
  end

  def _send_receive(xml, params={})
    res = super(xml, params)
    res = res["return"] if ((!res.nil?) && (res.has_key?("return")))
    return res
  end
end
