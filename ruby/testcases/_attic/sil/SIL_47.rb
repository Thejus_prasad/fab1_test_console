=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: Conny Stenzel, 2012-06-27

History:
  2017-08-01 cstenzel/sfrieske, refactored all tests
=end

require_relative 'SILTest'


# AutoCharting Rules  was: Test_Space17
class SIL_47 < SILTest
  @@test_defaults = {parameters: ['QA_Rule_Param1', 'QA_Rule_Param2'], ppd: :default}
  @@autocharting_ignorespeccheck = true
  # check criticality for template when creating a new channel
  @@templatecheck_criticality_inline_enabled = true
  @@templatecheck_criticality_setup_enabled = true
  # check corrective actions for template when creating a new channel
  @@templatecheck_correctiveactions_inline_enabled = false
  @@templatecheck_correctiveactions_setup_enabled = false
  # check alarm rules for template when creating a new channel
  @@templatecheck_alarmrules_inline_enabled = false
  @@templatecheck_alarmrules_setup_enabled = false

  @@template = '_Template_QA_RULE_CHECK'
  @@ekeys = [{keyid: 4, fixed: '*'}, {keyid: 5, fixed: '*'}, {keyid: 6, fixed: 'FIXED'}, {keyid: 10, fixed: 'FIXED'}]


  def self.shutdown
    [@@lds, @@lds_setup].each {|lds| lds.folder(@@templates_folder).spc_channel(name: @@template).keys = @@ekeys}
    super
  end

  def test00_setup
    $setup_ok = false
    #
    assert @@siltest.server.verify_property(:'templatecheck.criticality.inline.enabled', @@templatecheck_criticality_inline_enabled), 'wrong property'
    assert @@siltest.server.verify_property(:'templatecheck.criticality.setup.enabled', @@templatecheck_criticality_setup_enabled), 'wrong property'
    assert @@siltest.server.verify_property(:'templatecheck.correctiveactions.inline.enabled', @@templatecheck_correctiveactions_inline_enabled), 'wrong property'
    assert @@siltest.server.verify_property(:'templatecheck.correctiveactions.setup.enabled', @@templatecheck_correctiveactions_setup_enabled), 'wrong property'
    assert @@siltest.server.verify_property(:'templatecheck.alarmrules.inline.enabled', @@templatecheck_alarmrules_inline_enabled), 'wrong property'
    assert @@siltest.server.verify_property(:'templatecheck.alarmrules.setup.enabled', @@templatecheck_alarmrules_setup_enabled), 'wrong property'
    #
    assert @@siltest.set_defaults(@@test_defaults.merge(mpd: @@autocharting_ignorespeccheck ? 'QA_RULE-CHECK.01' : 'QA_RULE-CHECK.02'))
    #
    [@@lds, @@lds_setup].each {|lds|
      assert t = lds.folder(@@templates_folder).spc_channel(name: @@template), "missing template #{lds.inspect} #{@@template}"
      assert t.keys = @@ekeys, 'wrong key config'
    }
    #
    $setup_ok = true
  end

  #see MSR512888
  def test11_criticality
    run_business_rule_test('32NM', :criticality, @@templatecheck_criticality_inline_enabled, "Criticality")
  end

  def test12_criticality_TEST
    run_business_rule_test('TEST', :criticality, @@templatecheck_criticality_setup_enabled, "Criticality")
  end

  # known to fail (??) (rejects DCR if enabled=false)
  def testdev13_cas
    run_business_rule_test('32NM', :ca, @@templatecheck_correctiveactions_inline_enabled, "No Valid Corrective Action assigned")
  end

  # known to fail (??) (rejects DCR if enabled=false)
  def testdev14_cas_TEST
    run_business_rule_test('TEST', :ca, @@templatecheck_correctiveactions_setup_enabled, "No Valid Corrective Action assigned")
  end

  def test15_alarms
    run_business_rule_test('32NM', :alarms, @@templatecheck_alarmrules_inline_enabled, "Rule is not assigned")
  end

  def test16_alarms_TEST
    run_business_rule_test('TEST', :alarms, @@templatecheck_alarmrules_setup_enabled, "Rule is not assigned")
  end

  def test21_exclude_value
    assert tmpl = @@lds.folder(@@templates_folder).spc_channel(name: @@template)
    setup_template(tmpl, nil)
    # none matching exclude criteria
    assert tmpl.keys = @@ekeys + [{keyid: 7, exclude: ['~e*', 'USIL-0001.01']}]  ## [SIL::SpaceApi::SpcCKey.new(7, nil, ['~e*', 'USIL-0001.01'])])
    #
    # successful DCR
    assert dcr = @@siltest.submit_meas_dcr(route: 'SIL-0001.01'), 'error submitting DCR'
    assert @@siltest.verify_channels(dcr, template: @@template)
    # excluded
    assert @@lds.folder(@@autocreated_qa).delete_channels
    assert dcr = @@siltest.submit_meas_dcr(route: 'eSIL-0001.01', nsamples: 0), 'error submitting DCR'
    assert @@siltest.verify_channels(dcr, template: @@template, readings: {})
    # excluded
    assert @@lds.folder(@@autocreated_qa).delete_channels
    assert dcr = @@siltest.submit_meas_dcr(route: 'USIL-0001.01', nsamples: 0), 'error submitting DCR'
    assert @@siltest.verify_channels(dcr, template: @@template, readings: {})
  end

  def test22_include_value
    assert lots = $svtest.new_lots(1), 'error creating lot'
    assert @@siltest.set_defaults(@@test_defaults.merge(lot: lots[0], mpd: @@autocharting_ignorespeccheck ? 'QA_RULE-CHECK.01' : 'QA_RULE-CHECK.02')), 'wrong default test data'
    #
    assert tmpl = @@lds.folder(@@templates_folder).spc_channel(name: @@template)
    setup_template(tmpl, nil)
    # set channel extractor keys, none matching exclude criteria
    assert tmpl.keys = @@ekeys + [{keyid: 7, exclude: ['~!e*', '~!b*']}]
    #
    # successful DCR
    assert dcr = @@siltest.submit_meas_dcr(route: 'eSIL-0001.01'), 'error submitting DCR'
    assert @@siltest.verify_channels(dcr, template: @@template)
    # excluded
    assert @@lds.folder(@@autocreated_qa).delete_channels
    assert dcr = @@siltest.submit_meas_dcr(route: 'SIL-0001.01', nsamples: 0), 'error submitting DCR'
    assert @@siltest.verify_channels(dcr, template: @@template, readings: {})
  end


  # aux methods

  def run_business_rule_test(technology, rule, config, msg)
    lds = technology == 'TEST' ? @@lds_setup : @@lds
    assert tmpl = lds.folder(@@templates_folder).spc_channel(name: @@template)
    # remove feature
    setup_template(tmpl, rule)
    # send DCR
    if config
      assert @@siltest.submit_meas_dcr(technology: technology, nsamples: 0, spcerror: true)
    else
      assert dcr = @@siltest.submit_meas_dcr(technology: technology)
      assert @@siltest.verify_channels(dcr, template: @@template) {|channel, sample|
        ['LSL', 'TARGET', 'USL'].each {|k| refute_nil sample.specs[k]}
        ['MEAN_VALUE_UCL', 'MEAN_VALUE_CENTER', 'MEAN_VALUE_LCL'].each {|k| refute_nil sample.limits[k]}
      }
    end
    # add the feature back and verify channel creation works again
    setup_template(tmpl, nil)
    if config
      assert dcr = @@siltest.submit_meas_dcr(technology: technology)
      assert @@siltest.verify_channels(dcr, template: @@template) {|channel, sample|
        ['LSL', 'TARGET', 'USL'].each {|k| refute_nil sample.specs[k]}
        ['MEAN_VALUE_UCL', 'MEAN_VALUE_CENTER', 'MEAN_VALUE_LCL'].each {|k| refute_nil sample.limits[k]}
      }
    end
  end

  # note: a template is a channel
  def setup_template(tmpl, rule)
    tmpl.set_customer_fields(rule == :criticality ? {'Module'=>'CFM'} : {'Criticality'=>'Key', 'Module'=>'CFM'})
    vals = SIL::SpaceApi.valuations.select {|v| v.vid <= 6}
    vals = vals.drop(1) if rule == :alarms
    tmpl.set_valuations(vals)
    tmpl.set_cas(rule == :ca ? [] : ['AutoLotHold'])
  end

end
