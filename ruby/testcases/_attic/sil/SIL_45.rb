=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: Conny Stenzel, 2012-06-27

History:
  2017-08-01 cstenzel/sfrieske, refactored all tests
=end

require_relative 'SILTest'


# Rework  was: Test_Space14 part2
# Note: lot ids must be unique because SIL keeps lot/wafer history even if all related channels have been deleted
class SIL_45 < SILTest
  @@test_defaults = {mpd: 'QA-MB-FTDEPM.01', mtool: 'QAMeas'}
  @@erf_mpd = 'e1234-QA-MB-FTDEPM-A0.01'
  @@erf_ppd = 'e1234-QA-MB-FTDEP-A0.01'
  @@erf_route = 'e1234-QASIL-A0.01'
  @@flagging_delay = 5
  @@inline_rework_autoflagging_enabled = true
  @@setup_rework_autoflagging_enabled = true


  def test00_setup
    $setup_ok = false
    #
    assert @@siltest.server.verify_property(:'inline.rework.autoflagging.enabled', @@inline_rework_autoflagging_enabled), 'wrong property'
    assert @@siltest.server.verify_property(:'setup.rework.autoflagging.enabled', @@setup_rework_autoflagging_enabled), 'wrong property'
    #
    assert @@siltest.set_defaults(@@test_defaults)
    #
    $setup_ok = true
  end

  def test11_rework
    skip 'inline.rework.autoflagging.enabled=false' unless @@inline_rework_autoflagging_enabled
    #
    assert lot = $sv.generate_new_lotids('gf').first
    parameters = @@siltest.parameters.collect {|p| p + '_REWORK'}
    #
    3.times {|i| rework_scenario(lot, parameters: parameters, mpd: 'QA-MB-RW-M.01', ppd: 'QA-MB-RW.01', run: i+1)}
  end

  def test12_rework_2wafers
    skip 'inline.rework.autoflagging.enabled=false' unless @@inline_rework_autoflagging_enabled
    #
    assert lot = $sv.generate_new_lotids('gf').first
    readings = Hash[SIL::DCR::DefaultWaferValues.each_with_index.collect {|v, i| ["#{lot[0..10]}#{i}", v]}]
    #
    rework_scenario(lot, readings: readings, run: '1')
    rework_scenario(lot, readings: Hash[readings.to_a[0..1]], ekey: 'Lot', run: '2')
    rework_scenario(lot, readings: Hash[readings.to_a[0..1]], ekey: 'Lot', mtool: 'QAMeas2', run: '3')
    rework_scenario(lot, readings: Hash[readings.to_a[1..2]], ekey: 'Lot', run: '4')
  end

  def test13_rework_splitlots
    skip 'inline.rework.autoflagging.enabled=false' unless @@inline_rework_autoflagging_enabled
    #
    assert lot = $sv.generate_new_lotids('gf').first
    readings = Hash[SIL::DCR::DefaultWaferValues.each_with_index.collect {|v, i| ["#{lot[0..10]}#{i}", v]}]
    lc1 = lot.next
    lc2 = lc1.next
    lc3 = lc2.next
    #
    rework_scenario(lot, readings: readings, run: '1')
    rework_scenario(lc1, readings: Hash[readings.to_a[0..1]], run: '2')
    rework_scenario(lc2, readings: Hash[readings.to_a[0..1]], mtool: 'QAMeas2', run: '3')
    rework_scenario(lc3, readings: Hash[readings.to_a[1..2]], run: '4')
  end

  def test14_rework_pilot
    skip 'inline.rework.autoflagging.enabled=false' unless @@inline_rework_autoflagging_enabled
    #
    assert lot = $sv.generate_new_lotids('gf').first
    readings = Hash[SIL::DCR::DefaultWaferValues.each_with_index.collect {|v, i| ["#{lot[0..10]}#{i}", v]}]
    lc = lot.next
    #
    rework_scenario(lc, readings: Hash[readings.to_a[0..0]], run: '1')
    rework_scenario(lc, readings: Hash[readings.to_a[0..0]], run: '2')
    rework_scenario(lot, readings: Hash[readings.to_a[1..2]], run: '3')
  end

  def test15_rework_multilot
    skip 'inline.rework.autoflagging.enabled=false' unless @@inline_rework_autoflagging_enabled
    #
    assert lots = $sv.generate_new_lotids('gf', n: 2)
    lot1, lot2 = lots
    #
    rework_scenario([lot1, lot2], run: '1')
    rework_scenario([lot1, lot2], run: '2')
    rework_scenario(lot2, run: '3')
    rework_scenario(lot1, run: '4')
  end

  def test16_rework_erf_lots1
    assert lots = $sv.generate_new_lotids('gf', n: 2)
    #
    rework_scenario(lots[0], mpd: @@erf_mpd, route: @@erf_route, run: '1')
    rework_scenario(lots[1], run: '2')
    # note (sf 2017-08-18): This scenario works (verified manually).
    #   However this test program should fail because the comment is 'Experiment' not 'Rework' -> review verify_channel_reworked?
  end

  def test17_rework_erf_lots2
    assert lots = $sv.generate_new_lotids('gf', n: 2)
    #
    rework_scenario(lots[0], ppd: @@erf_ppd, route: @@erf_route, run: '1')
    rework_scenario(lots[1], run: '2')
    # note (sf 2017-08-18): This scenario works (verified manually).
    #   However this test program should fail because the comment is 'Experiment' not 'Rework' -> review verify_channel_reworked?
  end

  def test18_rework_TEST
    skip 'setup.rework.autoflagging.enabled=false' unless @@setup_rework_autoflagging_enabled
    #
    assert lot = $sv.generate_new_lotids('gf').first
    parameters = @@siltest.parameters.collect {|p| p + '_REWORK'}
    #
    3.times {|i| rework_scenario(lot, parameters: parameters, technology: 'TEST', mpd: 'QA-MB-RW-M.01', ppd: 'QA-MB-RW.01', run: i+1)}
  end

  def test19_lotlevel_and_rework
    skip 'inline.rework.autoflagging.enabled=false' unless @@inline_rework_autoflagging_enabled
    #
    assert lot = $sv.generate_new_lotids('gf').first
    parameters = @@siltest.parameters.collect {|p| p + '_REWORK-LOT'}
    #
    3.times {|i| rework_scenario(lot, parameters: parameters, nsamples: 5, mpd: 'QA-MB-RW-M.01', ppd: 'QA-MB-RW.01', run: i+1)}
  end

  def test21_lotlevel_and_rework_differentlots
    skip 'inline.rework.autoflagging.enabled=false' unless @@inline_rework_autoflagging_enabled
    #
    assert lot = $sv.generate_new_lotids('gf').first
    parameters = @@siltest.parameters.collect {|p| p + '_REWORK-LOT'}
    #
    rework_scenario(lot, parameters: parameters, nsamples: 5, mpd: 'QA-MB-RW1.01', ppd: 'QA-MB-RW1.01', run: '1')
    assert lot = $sv.generate_new_lotids('gf').first
    rework_scenario(lot, parameters: parameters, nsamples: 5, mpd: 'QA-MB-RW1.01', ppd: 'QA-MB-RW1.01', run: '2')
  end


  # aux methods

  def rework_scenario(lot, params={})
    lottag = lot
    lottag = lot.join('-') if lot.kind_of?(Array)
    run = params[:run] || '0'
    technology = params[:technology] || '32NM'
    lds = technology == 'TEST' ? @@lds_setup : @@lds
    parameters = params[:parameters] || @@siltest.parameters
    # count old samples
    assert folder = lds.folder(@@autocreated_qa)
    old_nsamples = {}
    parameters.each {|p|
      ch = folder.spc_channel(p)
      old_nsamples[p] = ch ? ch.samples.size : 0
    }
    # submit process DCR
    assert @@siltest.submit_process_dcr(lot: lot, ppd: params[:ppd], route: params[:route],
      technology: technology, parameters: params[:parameters], readings: params[:readings], exporttag: "#{lottag}-#{run}")
    # submit meas DCR
    assert dcr = @@siltest.submit_meas_dcr(lot: lot, mpd: params[:mpd], route: params[:route],
      technology: technology, parameters: params[:parameters], readings: params[:readings], nsamples: params[:nsamples], exporttag: "#{lottag}-#{run}")
    # wait for flagging and check flags
    $log.info "waiting #{@@flagging_delay} s for flagging"; sleep @@flagging_delay
    assert folder = lds.folder(@@autocreated_qa)
    # check for flags
    parameters.each {|p|
      assert ch = folder.spc_channel(p), 'no channel'
      samples = ch.samples
      dcrreadingssize = parameters.first.include?('-LOT') ? 1 : dcr.readings(p).size
      assert_equal old_nsamples[p] + dcrreadingssize, samples.size, "wrong number of samples in #{ch.inspect}"
      if technology != 'TEST' && @@inline_rework_autoflagging_enabled || technology == 'TEST' &&  @@setup_rework_autoflagging_enabled
        assert verify_channel_reworked(ch, params[:ekey]), 'samples not autoflagged for rework'
      else
        if old_nsamples[p] > 0
          samples[0..(old_nsamples[p]-1)].each {|s|
            assert !s.iflag || !s.icomment || !s.icomment.include?('Rework'), 'wrong autoflagging for rework'
          }
        end
      end
    }
  end

  def verify_channel_reworked(ch, ekey, params={})
    rhash = {}
    ch.samples.each {|s|
      ekeys = s.ekeys
      lotwafer = s.ekeys[ekey || 'Wafer']
      rkey = [ekeys['Lot'], ekeys['Wafer'], ekeys['PTool'], ekeys['POperationID'], s.dkeys['MOperationID'], lotwafer].join(':')
      rhash[rkey] ||= []
      rhash[rkey] << s
    }
    ret = true
    rhash.each_pair {|rkey, samples|
      samples.sort! {|a, b| a.dkeys['MTime'] <=> b.dkeys['MTime']}
      reftime = samples.last.time # sample time of newest sample is the reference
      samples.each_with_index {|s, i|
        if s.time < reftime
          ret &= s.iflag && s.icomment && s.icomment.include?('Rework')
        else
          ret &= !s.iflag || !s.icomment || !s.icomment.include?('Rework')
        end
      }
    }
    return ret
  end

end
