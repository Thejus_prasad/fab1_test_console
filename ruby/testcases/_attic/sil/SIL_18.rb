=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: Steffen Steidten, 2011-11-25
=end

require_relative 'SILTest'


# Channel behaviour, was Test_Space00 (Pre-run)
class SIL_18 < SILTest
  @@test_defaults = {
    mpd: 'QA-MB-CC-M-PRERUN.1', ppd: 'QA-MB-CC.01', department: 'QACCALL', technology: 'TEST',
    parameters: ['QA_PARAM_900_PRERUN', 'QA_PARAM_901_PRERUN', 'QA_PARAM_902_PRERUN', 'QA_PARAM_903_PRERUN', 'QA_PARAM_904_PRERUN'],
    cas: ['LotHold', 'AutoLotHold', 'CancelLotHold', 'ReleaseLotHold']
  }


  def test00_setup
    $setup_ok = false
    #
    assert lot = $svtest.new_lot, 'error creating test lot'
    @@testlots << lot
    $log.info "using lot #{lot}"
    #
    assert @@siltest.set_defaults(@@test_defaults.merge(lot: lot)), 'wrong setup'
    #
    $setup_ok = true
  end

  def testman11_study
    check_prerun_behaviour('Study', :default, technology: 'TEST')
  end

  def testman16_inline_study
    check_prerun_behaviour('Study', :default, technology: '32NM')
  end

  def testman21_offline
    check_prerun_behaviour('Offline', :default, technology: 'TEST')
  end

  def testman26_inline_offline
    check_prerun_behaviour('Offline', :default, technology: '32NM')
  end


  # It is important to get samples which are not flagged
  def check_prerun_behaviour(state, readings, params={})
    assert channels = @@siltest.setup_channels(state: state, technology: params[:technology])
    # send pre-run DCRs
    dcrp = nil
    dcrm = nil
    count = params[:prerundcrs] || 25
    # TODO: real wafers needed?
    wafers = $sv.lot_info(@@siltest.lot, wafers: true).wafers.take(SIL::DCR::DefaultWaferValues.size).collect {|w| w.wafer}
    count.times {|i|
      postfix = '%02d' % i
      readings = Hash[wafers.collect {|w| w+postfix}.zip(SIL::DCR::DefaultWaferValues)]
      assert dcrp = @@siltest.submit_process_dcr(technology: params[:technology], ppd: @@siltest.ppd + postfix, readings: readings)
      assert dcrm = @@siltest.submit_meas_dcr(technology: params[:technology], mpd: @@siltest.mpd + postfix, readings: readings)
    }
    $log.info "\n1. enable LDS (#{params[:technology]}) prerun\n2. stop prerun at all channels\n3. disable LDS prerun again\npress Enter to continue!"
    gets
    $log.info 'waiting 10 minutes for SIL/Space to calculate'
    sleep 600
    # send new DCRs
    postfix = '%02d' % count
    readings = Hash[wafers.collect {|w| w+postfix}.zip(SIL::DCR::DefaultWaferValues)]
    assert dcrp = @@siltest.submit_process_dcr(technology: params[:technology], ppd: @@siltest.ppd + postfix, readings: readings)
    assert dcrm = @@siltest.submit_meas_dcr(technology: params[:technology], mpd: @@siltest.mpd + postfix, readings: readings)
    # verify actions
    tstart = Time.now
    refute @@siltest.wait_futurehold('Chart Check issue', tstart: tstart), 'wrong future hold'
    refute @@siltest.wait_notification('Chart Check Failures for Lot', tstart: tstart), 'wrong notification'
  end

end
