=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: Steffen Steidten, 2011-11-25

History:
  2017-08-01 cstenzel/sfrieske, refactored all tests
=end

require_relative 'SILTest'

# SIL basic smoke tests, incl templates
class SIL_00 < SILTest
  @@test_defaults = {mpd: 'QA-MB-FTDEPM-STATE.01', ppd: :default}  # this MPD has no PPD, using DCR builtin default
  @@long_parameters = ['QA_FORTY-----16|-------------32||----40|',
    'QA_ONEHUNDERTTWENTYEIGHT-----32||----------------------------64||----------------------------96||---------------------------128|',
    'QA_TWOHUNDERTFIFTYSIX--------32||----------------------------64||----------------------------96||---------------------------128||---------------------------160||---------------------------192||---------------------------224||---------------------------256|',
  ]


  def test00_setup
    $setup_ok = false
    #
    assert @@siltest.set_defaults(@@test_defaults)
    assert @@siltest.lds_inline, "LDS Inline not found"
    assert @@siltest.lds_setup, "LDS Setup not found"
    assert @@siltest.server.verify_properties, 'wrong SIL server properties'
    #
    $setup_ok = true
  end

  def test02_extractor_keys
    [@@lds, @@lds_setup].each {|lds|
      $log.info lds
      ekeys = lds.ekeys
      assert ekeys.size > 1, 'wrong number of extractor keys'
      assert_equal({keyid: 1, name: 'Lot', desc: 'LotID', refonly: 'Y'}, ekeys[0])
      assert_equal({keyid: 2, name: 'Wafer', desc: 'WaferID', refonly: 'Y'}, ekeys[1])
    }
  end

  def test14_no_department
    # verify templates exist
    assert folder = @@lds.folder(@@templates_folder)
    [@@default_template, @@default_template_qa, @@params_template].each {|t| assert folder.spc_channel(name: t), "missing template #{t}"}
    #
    # ensure parameters to be used are not configured for autocharting
    parameters = @@siltest.parameters.collect {|p| "#{p}DEF"}
    parameters.each {|p| refute @@siltest.db.isAutocharting(p), "parameter #{p} has an autochart configuration"}
    $log.info "using parameters #{parameters.inspect}"
    #
    # submit DCR w/o department and verify channels
    assert dcr = SIL::DCR.new
    dcr.add_lot(:default, op: 'QA_NODEP.01')
    dcr.lot_context.delete('department')
    dcr.add_parameters(parameters, :default)
    assert @@siltest.send_dcr_verify(dcr)
    folder = @@lds.folder('AutoCreated_null')
    assert @@siltest.verify_channels(dcr, folder: @@lds.folder('AutoCreated_null'), template: @@default_template, parameters: parameters)
  end

  def test15_no_cj
    # submit DCR w/o cj
    dcr = SIL::DCR.new
    dcr.context[:JobSetup][:SiViewControlJob] = nil
    dcr.add_parameters(:default, {nil=>99})
    assert @@siltest.send_dcr_verify(dcr), 'error submitting DCR'
    # verify spc channels
    assert folder = @@lds_setup.folder(@@autocreated_nonwip)
    @@siltest.parameters.each {|p|
      assert ch = folder.spc_channel(p)
      samples = ch.samples
      assert_equal 1, samples.size, "wrong number of samples in channel #{ch.inspect}"   # fails in LET, missing setup?
      assert_equal dcr.eqp, samples.first.ekeys['PTool'], "wrong PTool in channel #{ch.inspect}"
    }
  end

  def test21_create_autocharting_folder
    department = 'QADummy'
    parameter = 'QA_DummyPARAM1'
    technology = 'dummytech'
    #
    # ensure autocharting folder does not exist
    @@lds.folders("AutoCreated_#{department}*").each {|folder| assert_equal 0, folder.delete, "error deleting #{folder}"}
    #
    # submit a DCR
    assert dcr = @@siltest.submit_meas_dcr(department: department, technology: technology, parameters: [parameter])
    #
    # verify autocharting folder creation
    assert folder = @@lds.folder("AutoCreated_#{department}"), 'error creating Autocharting folder'
    #
    # verify spc channel data
    channels = folder.spc_channels
    assert_equal 1, channels.size, "wrong number of channels in #{folder}"
    ref = {desc: "Auto-created using template: #{@@default_template}", parameter: parameter, unit: '-'}
    ref[:unit] = 'WAFER' if $env == 'f8stag'
    ch = channels.first
    assert_equal SIL::DCR::DefaultWaferValues.size, ch.samples.size, 'wrong number of samples'
    assert verify_hash(ref, ch.to_hash, nil, refonly: true), 'wrong channel data'
  end

  def test22_create_autocharting_folder_long
    # submit DCRs
    
    # Testcase works - however, since PROD ->>> ITDC copy, SIL answer seems to be 0 samples, but there are 3 as expected.
    # possible timing issue? to be investigated further!
    #@@long_parameters.each {|p| assert dcr = @@siltest.submit_meas_dcr(parameters: [p])}
    
    @@long_parameters.each {|p| @@siltest.submit_meas_dcr(parameters: [p])}
    # verify channel names are not truncated (MSR 743545), works up to including 256
    folder = @@lds.folder(@@autocreated_qa)
    @@long_parameters.each {|p|
      assert ch = folder.spc_channel(parameter: p), 'error creating channel'
      assert ch.name.include?(p), "channel name truncated to #{ch.name.inspect}"  ## .../32NM
		}
  end

  def test31_autocharting_module
    parameters = {'QA_AUTOCHART_900'=>'QA', 'QA_AUTOCHART_901'=>'LIT',
      'QA_AUTOCHART_902'=>'MTR', 'QA_AUTOCHART_903'=>'ETC', 'QA_AUTOCHART_904'=>'CFM'}
    @@lds.spc_channels(name: 'QA_AUTOCHART_90*').each {|ch| assert ch.delete}
    #
    # submit a meas DCR and verify SPC channels
    assert dcr = @@siltest.submit_meas_dcr(department: 'NODEP', mpd: 'QA_AUTOCHARTING.01', parameters: parameters.keys)
    channels = @@lds.spc_channels(name: '*QA_AUTOCHART_90*')
    assert_equal parameters.size, channels.size, 'missing channels'
    parameters.keys.each {|p|
      assert prop = @@siltest.server.advanced_props[:'sampledata.module.lookup.enabled'], 'no such property: sampledata.module.lookup.enabled'
      if prop == 'true'
        dpt = parameters[p]
        templ = "_Template_QA_AUTOCHART_#{dpt}"
      else
        dpt = 'NODEP'
        templ = @@params_template
      end
      assert folder = @@lds.folder("AutoCreated_#{dpt}"), 'error creating folder'
      assert ch = folder.spc_channel(p), "no channel for parameter #{p}"
      assert_equal "Auto-created using template: #{templ}", ch.desc, 'wrong channel description'
    }
  end

  def test32_autocharting_module_default_template
    skip "not working LET" if $env == 'let'   ## TODO: really??
    parameters = {'QA_AUTOCHART_900F'=>'QA', 'QA_AUTOCHART_901F'=>'LIT',
      'QA_AUTOCHART_902F'=>'MTR', 'QA_AUTOCHART_903F'=>'ETC', 'QA_AUTOCHART_904F'=>'CFM'}
    @@lds.spc_channels(name: 'QA_AUTOCHART_90*').each {|ch| assert ch.delete}
    #
    # ensure module template exists
    assert folder = @@lds.folder(@@templates_folder)
    assert folder.spc_channel(name: 'DEFAULT_QA_TEMPLATE'), 'missing DEFAULT_QA_TEMPLATE'
    #
    # submit a meas DCR and verify SPC channels
    assert dcr = @@siltest.submit_meas_dcr(department: 'NODEP', mpd: 'QA_AUTOCHARTING.01', parameters: parameters.keys)
    channels = @@lds.spc_channels(name: '*QA_AUTOCHART_90*')
    assert_equal parameters.size, channels.size, 'missing channels'
    parameters.keys.each {|p|
      assert prop = @@siltest.server.advanced_props[:'sampledata.module.lookup.enabled'], 'no such property: sampledata.module.lookup.enabled'
      if prop == 'true'
        dpt = parameters[p]
        templ = "_Template_QA_AUTOCHART_#{dpt}"
      else
        dpt = 'NODEP'
        templ = @@default_template
      end
      assert folder = @@lds.folder("AutoCreated_#{dpt}"), 'error creating folder'
      assert ch = folder.spc_channel(p), "no channel for parameter #{p}"
      assert_equal "Auto-created using template: #{templ}", ch.desc, 'wrong channel description'
    }
  end

  def test91_create_autocharting_folders_load
    n = ($env == 'let') ? 20 : 5
    #
    department = 'QADummy'
    parameter = 'QA_DummyPARAM'
    pg = 'QAPG'
    product = 'SILPROD.0'
    technology = 'dummytech'
    # send DCRs
    test_ok = true
    n.times.collect {|i|
      Thread.new {
        dcr = SIL::DCR.new
        dcr.add_lot(:default, op: @@siltest.mpd, department: "#{department}#{i}", technology: technology, productgroup: "#{pg}#{i}", product: "#{product}#{i}")
        dcr.add_parameter("#{parameter}#{i}", :default)
        test_ok &= @@siltest.send_dcr_verify(dcr, exporttag: i)
      }
    }.each_with_index {|t, i| $log.info "waiting for thread #{i} to complete"; t.join}
    assert test_ok
    #
    # verify autocharting folder creation
    n.times {|i|
      folder = @@lds.folder("AutoCreated_#{department}#{i}")
      ($log.warn "missing folder AutoCreated_#{department}#{i}"; test_ok = false) unless folder
      chs = folder.spc_channels
      ($log.warn "wrong number of channels in #{folder}"; test_ok = false) unless chs && chs.size == 1
    }
    # cleanup channels and folders
    fnames = ["AutoCreated_#{department}*"]
    fnames.each {|f| @@lds.folders(f).each {|folder| folder.delete_channels(silent: true, exclude: '2S@QA')}}
    #
    assert test_ok
  end

  def testman92_create_autocharting_folders_load_ooc
    n = ($env == 'let') ? 20 : 5
    #
    department = 'QADummy'
    parameter = 'QA_DummyPARAM'
    pg = 'QAPG'
    product = 'SILPROD.0'
    technology = '28SLP1STM'
    op = 'QA-MB-FTDEPM-DUMMY'
    #
    # ensure autocharting folders do not exist
    @@lds.folders("AutoCreated_#{department}*").each {|folder| assert_equal 0, folder.delete, "error deleting #{folder}"}
    #
    # send DCRs
    test_ok = true
    n.times.collect {|i|
      Thread.new {
        dcr = SIL::DCR.new
        dcr.add_lot(:default, op: "#{op}#{i}.01", department: "#{department}#{i}", technology: technology, productgroup: "#{pg}#{i}", product: "#{product}#{i}")
        dcr.add_parameter("#{parameter}#{i}", :ooc)
        test_ok &= @@siltest.send_dcr_verify(dcr, exporttag: i)
      }
    }.each_with_index {|t, i| $log.info "waiting for thread #{i} to complete"; t.join}
    assert test_ok
    #
    # verify autocharting folder creation
    n.times {|i|
      folder = @@lds.folder("AutoCreated_#{department}#{i}")
      ($log.warn "missing folder AutoCreated_#{department}#{i}"; test_ok = false) unless folder
      chs = folder.spc_channels
      ($log.warn "wrong number of channels in #{folder}"; test_ok = false) unless chs && chs.size == 1
      assert @@siltest.verify_ecomment(chs, 'AutoLotHold')
    }
    # clean up
    @@lds.folders("AutoCreated_#{department}*").each {|folder| assert_equal 0, folder.delete, "error deleting #{folder}"}
    #
    assert test_ok
  end

end
