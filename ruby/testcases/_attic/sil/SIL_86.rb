=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: Conny Stenzel, 2012-06-27

History:
  2017-08-01 cstenzel/sfrieske, refactored all tests
=end

require_relative 'SILTest'

# 2S Tests: After first run (when no 2S@ channels exist in Setup LDS/AutoCreated_QA folder) the channels have to be created manually:
# Go to the exclude list, select the parameter channels and do Actions -> Create SPC Channel
# Select one of the original channels in the AutoCreated_QA folder as template for the new channels and check box "create CKC"
# After the channels have been created go to channel properties of each channel -> tab Settings -> uncheck "Inherit"
# at Control Limit Mode and choose External.
# The last step can be skipped when other 2S@ channels already exists which have been modified in the way like this
# When this last step is not performed - no CKC Mean Limits will be in the 2S@ channel


# Monitoring Spec Limits (ECP Limits), Green Zone, 2Sigma   was part of Test_Space25  # needs manual interaction
class SIL_86 < SILTest
  @@test_defaults = {mpd: 'QA-GZV-FTDEPM.01', technology: 'TEST'}
  @@prefix2s = '2S@'
  @@spec = 'C05-00001864'
  @@parametersets = {
    R: ['QA_PARAM_850_ECP', 'QA_PARAM_851_ECP', 'QA_PARAM_852_ECP', 'QA_PARAM_853_ECP', 'QA_PARAM_854_ECP'],
    U: ['QA_PARAM_860_ECP', 'QA_PARAM_861_ECP', 'QA_PARAM_862_ECP', 'QA_PARAM_863_ECP', 'QA_PARAM_864_ECP'],
    D: ['QA_PARAM_870_ECP', 'QA_PARAM_871_ECP', 'QA_PARAM_872_ECP', 'QA_PARAM_873_ECP', 'QA_PARAM_874_ECP'],
    N: ['QA_PARAM_880_ECP', 'QA_PARAM_881_ECP', 'QA_PARAM_882_ECP', 'QA_PARAM_883_ECP', 'QA_PARAM_884_ECP'],
    NoGZV: ['QA_PARAM_890_ECP', 'QA_PARAM_891_ECP', 'QA_PARAM_892_ECP', 'QA_PARAM_893_ECP', 'QA_PARAM_894_ECP'],
  }
  @@assembly_group = '000000000016028714'


  def test00_setup
    $setup_ok = false
    #
    assert lots = $svtest.new_lots(1), 'error creating lots'
    @@testlots += lots
    #
    assert @@siltest.set_defaults(@@test_defaults.merge(lot: lots[0])), 'wrong default test data'
    #
    $setup_ok = true
  end

  def test10_s2channel_create
    $setup_ok = false
    #
    # ensure S2 channels exist or can be created manually
    # set E10 status
    #['2NDP', '4PMQ'].each {|s|
    #  assert_equal 0, $sv.eqp_status_change(@@siltest.ptool, s)
    #  assert_equal 0, $sv.chamber_status_change(@@siltest.ptool, @@siltest.chambers, s)
    #}
    [:R].each {|type|
      parameters = @@parametersets[type]
      assert folder = @@lds_setup.folder(@@autocreated_qa)
      assert channels2s = parameters.collect {|p| folder.spc_channel(@@prefix2s + p)}
      if channels2s.compact.size != parameters.size
        # send data to create S2 channels in the Exclude List
        assert create_2S_channels(parameters)
        $log.warn "\nMissing 2S channels in #{folder.inspect}. Create them from the exclude list as described in TC header and press Enter to continue!"
        # create the channels manually
        gets
      end
      assert folder = @@lds_setup.folder(@@autocreated_qa)
      parameters.each {|p| assert folder.spc_channel(@@prefix2s + p), 'missing S2 channels'}
    }
    #
    $setup_ok = true
  end

  def test11_R
    parameters = @@parametersets[:R]
    newlimits = {'MEAN_VALUE_LCL'=>40.0, 'MEAN_VALUE_UCL'=>50.0}
    # set E10 status
    #['2NDP', '4PMQ'].each {|s|
    #  assert_equal 0, $sv.eqp_status_change(@@siltest.ptool, s)
    #  assert_equal 0, $sv.chamber_status_change(@@siltest.ptool, @@siltest.chambers, s)
    #}
    # create channels
    assert res = create_2S_channels(parameters, newlimits: newlimits)
    # verify 2S spc channels exist in AutoCreated_QA
    assert folder = @@lds_setup.folder(@@autocreated_qa)
    parameters.each {|p| assert folder.spc_channel(@@prefix2s + p), 'missing S2 channels'}
    # verify E10 state in SIL DB  # TODO: move to create_2S_channels ?
    #verify_db_e10(res, expected_eqp_e10_state: 'SDT.4PMQ', expected_chamber_e10_state: 'SDT.4PMQ')
    # verify 2S limits
    verify_2slimits(parameters, newlimits)
  end

  def test12_R_eqp2WPR
    parameters = @@parametersets[:R]
    newlimits = {'MEAN_VALUE_LCL'=>40.0, 'MEAN_VALUE_UCL'=>50.0}
    # set E10 status for chambers, eqp stays 2WPR
    ['2NDP', '4PMQ'].each {|s|
      assert_equal 0, $sv.chamber_status_change(@@siltest.ptool, @@siltest.chambers, s)
    }
    # create channels
    assert res = create_2S_channels(parameters, newlimits: newlimits)
    # verify 2S spc channels exist in AutoCreated_QA
    assert folder = @@lds_setup.folder(@@autocreated_qa)
    parameters.each {|p| assert folder.spc_channel(@@prefix2s + p), 'missing S2 channels'}
    # verify E10 state in SIL DB
    verify_db_e10(res, expected_eqp_e10_state: 'SBY.2WPR', expected_chamber_e10_state: 'SDT.4PMQ')
    # verify 2S limits
    verify_2slimits(parameters, newlimits)
  end

  def test13_R_eqp2WPR_1chamber4PMQ
    parameters = @@parametersets[:R]
    newlimits = {'MEAN_VALUE_LCL'=>40.0, 'MEAN_VALUE_UCL'=>50.0}
    # set E10 status for 1 chamber, eqp stays 2WPR
    ['2NDP', '4PMQ'].each {|s|
      assert_equal 0, $sv.chamber_status_change(@@siltest.ptool, @@siltest.chambers.first, s)
    }
    sleep 10
    # create channels
    assert res = create_2S_channels(parameters, newlimits: newlimits)
    # verify 2S spc channels exist in AutoCreated_QA
    assert folder = @@lds_setup.folder(@@autocreated_qa)
    parameters.each {|p| assert folder.spc_channel(@@prefix2s + p), 'missing S2 channels'}
    # verify E10 state in SIL DB
    verify_db_e10(res, expected_eqp_e10_state: 'SBY.2WPR', expected_chamber_e10_state: 'SDT.4PMQ', multichamberstates: true)
    # verify 2S limits
    verify_2slimits(parameters, newlimits)
  end

  def test14_R_eqp2WPR_singlechamber4PMQ
    parameters = @@parametersets[:R]
    newlimits = {'MEAN_VALUE_LCL'=>40.0, 'MEAN_VALUE_UCL'=>50.0}
    # set E10 status for 1st and only chamber, eqp stays 2WPR
    ['2NDP', '4PMQ'].each {|s|
      assert_equal 0, $sv.chamber_status_change(@@siltest.ptool, @@siltest.chambers.first, s)
    }
    sleep 10
    # create channels
    assert res = create_2S_channels(parameters, newlimits: newlimits, chambers: [@@siltest.chambers.first])
    # verify 2S spc channels exist in AutoCreated_QA
    assert folder = @@lds_setup.folder(@@autocreated_qa)
    parameters.each {|p| assert folder.spc_channel(@@prefix2s + p), 'missing S2 channels'}
    # verify E10 state in SIL DB
    verify_db_e10(res, expected_eqp_e10_state: 'SBY.2WPR', expected_chamber_e10_state: 'SDT.4PMQ')
    # verify 2S limits
    verify_2slimits(parameters, newlimits)
  end

  def test15_R_eqp4PMQ_chambers2WPR
    parameters = @@parametersets[:R]
    newlimits = {'MEAN_VALUE_LCL'=>40.0, 'MEAN_VALUE_UCL'=>50.0}
    # set E10 status for eqp, chambers stay 2WPR
    ['2NDP', '4PMQ'].each {|s|
      assert_equal 0, $sv.eqp_status_change(@@siltest.ptool, s)
    }
    # create channels
    assert res = create_2S_channels(parameters, newlimits: newlimits)
    # verify 2S spc channels exist in AutoCreated_QA
    assert folder = @@lds_setup.folder(@@autocreated_qa)
    parameters.each {|p| assert folder.spc_channel(@@prefix2s + p), 'missing S2 channels'}
    # verify E10 state in SIL DB
    verify_db_e10(res, expected_eqp_e10_state: 'SDT.4PMQ', expected_chamber_e10_state: 'SBY.2WPR')
    # verify 2S limits
    verify_2slimits(parameters, newlimits)
  end

  def test16_R_all2WPR
    parameters = @@parametersets[:R]
    newlimits = {'MEAN_VALUE_LCL'=>40.0, 'MEAN_VALUE_UCL'=>50.0}
    # set no E10 status, eqp and chambers stay 2WPR
    ['2NDP', '2WPR'].each {|s|
      assert_equal 0, $sv.eqp_status_change(@@siltest.ptool, s)
      assert_equal 0, $sv.chamber_status_change(@@siltest.ptool, @@siltest.chambers, s)
    }
    sleep 10
    # create channels
    assert res = create_2S_channels(parameters, newlimits: newlimits)
    # verify 2S spc channels exist in AutoCreated_QA
    assert folder = @@lds_setup.folder(@@autocreated_qa)
    parameters.each {|p| assert folder.spc_channel(@@prefix2s + p), 'missing S2 channels'}
    # verify E10 state in SIL DB
    verify_db_e10(res, expected_eqp_e10_state: 'SBY.2WPR', expected_chamber_e10_state: 'SBY.2WPR')
    # verify 2S limits
    verify_2slimits(parameters, newlimits, no2s: true)
  end

  # interactively create 2S channels for types U and D
  def test30_U_D_create
    $setup_ok = false
    #
    # ensure S2 channels exist or can be created manually
    # set E10 status
    ['2NDP', '4PMQ'].each {|s|
      assert_equal 0, $sv.eqp_status_change(@@siltest.ptool, s)
      assert_equal 0, $sv.chamber_status_change(@@siltest.ptool, @@siltest.chambers, s)
    }
    [:U, :D].each {|type|
      parameters = @@parametersets[type]
      assert folder = @@lds_setup.folder(@@autocreated_qa)
      assert channels2s = parameters.collect {|p| folder.spc_channel(@@prefix2s + p)}
      if channels2s.compact.size != parameters.size
        # send data to create S2 channels in the Exclude List
        assert create_2S_channels(parameters)
        $log.warn "\nMissing 2S channels in #{folder.inspect}. Create them from the exclude list as described in TC header and press Enter to continue!"
        # create the channels manually
        gets
      end
      assert folder = @@lds_setup.folder(@@autocreated_qa)
      parameters.each {|p| assert folder.spc_channel(@@prefix2s + p), 'missing S2 channels'}
    }
    #
    $setup_ok = true
  end

  def test31_U
    parameters = @@parametersets[:U]
    # set E10 status
    ['2NDP', '4PMQ'].each {|s|
      assert_equal 0, $sv.eqp_status_change(@@siltest.ptool, s)
      assert_equal 0, $sv.chamber_status_change(@@siltest.ptool, @@siltest.chambers, s)
    }
    # create channels  TODO: this not create anymore
    assert res = create_2S_channels(parameters)
    # verify 2S spc channels exist in AutoCreated_QA
    assert folder = @@lds_setup.folder(@@autocreated_qa)
    parameters.each {|p| assert folder.spc_channel(@@prefix2s + p), 'missing S2 channels'}
    # verify E10 state in SIL DB
    verify_db_e10(res, expected_e10_state: 'SDT.4PMQ')
    # check 2S@ channel CKC mean limits, calculated by using green zone value  (# ensure spec and sample limits are set)
    parameters.each {|p|
      assert ch2s = folder.spc_channel(@@prefix2s + p)
      assert s2s = ch2s.samples.last
      assert gzv = @@siltest.db.greenzonevalue(p, 'U', @@spec), 'no (unique) DB greenzonevalue'
      assert_equal gzv, s2s.limits['MEAN_VALUE_UCL'], 'wrong 2S MEAN UCL'
    }
  end

  def test32_D
    parameters = @@parametersets[:D]
    # set E10 status
    ['2NDP', '4PMQ'].each {|s|
      assert_equal 0, $sv.eqp_status_change(@@siltest.ptool, s)
      assert_equal 0, $sv.chamber_status_change(@@siltest.ptool, @@siltest.chambers, s)
    }
    # create channels  TODO: this not create anymore
    assert res = create_2S_channels(parameters)
    # verify 2S spc channels exist in AutoCreated_QA
    assert folder = @@lds_setup.folder(@@autocreated_qa)
    parameters.each {|p| assert folder.spc_channel(@@prefix2s + p), 'missing S2 channels'}
    # verify E10 state in SIL DB
    verify_db_e10(res, expected_e10_state: 'SDT.4PMQ')
    # check 2S@ channel CKC mean limits, calculated by using green zone value  (# ensure spec and sample limits are set)
    parameters.each {|p|
      assert ch2s = folder.spc_channel(@@prefix2s + p)
      assert s2s = ch2s.samples.last
      assert gzv = @@siltest.db.greenzonevalue(p, 'D', @@spec), 'no (unique) DB greenzonevalue'
      assert_equal gzv, s2s.limits['MEAN_VALUE_UCL'], 'wrong 2S MEAN UCL'
    }
  end

  def test41_N
    # verify E10 state in SIL DB
    parameters = @@parametersets[:N]
    # set E10 status
    ['2NDP', '4PMQ'].each {|s|
      assert_equal 0, $sv.eqp_status_change(@@siltest.ptool, s)
      assert_equal 0, $sv.chamber_status_change(@@siltest.ptool, @@siltest.chambers, s)
    }
    # create channels  TODO: this not create anymore
    assert res = create_2S_channels(parameters)
    # verify E10 state in SIL DB
    verify_db_e10(res, expected_e10_state: 'SDT.4PMQ')
    # verify 2S spc channels do not exist in AutoCreated_QA
    assert folder = @@lds_setup.folder(@@autocreated_qa)
    parameters.each {|p| assert_nil folder.spc_channel(@@prefix2s + p), 'wrong S2 channels'}
    # check 2S@ channel CKC mean limits, calculated by using green zone value
    parameters.each {|p|
      assert gzv = @@siltest.db.greenzonevalue(p, 'N', @@spec), 'no (unique) DB greenzonevalue'
    }
    $log.info "\nVerify there are no 2S@QA_PARAM_83X_ECP channels in the Exclude List. Press Enter to continue!"
    gets
  end

  def test42_NoGZV
    parameters = @@parametersets[:NoGZV]
    # set E10 status
    ['2NDP', '4PMQ'].each {|s|
      assert_equal 0, $sv.eqp_status_change(@@siltest.ptool, s)
      assert_equal 0, $sv.chamber_status_change(@@siltest.ptool, @@siltest.chambers, s)
    }
    # create channels  TODO: this not create anymore
    assert res = create_2S_channels(parameters)
    # verify E10 state in SIL DB
    verify_db_e10(res, expected_e10_state: 'SDT.4PMQ')
    # verify 2S spc channels do not exist in AutoCreated_QA
    assert folder = @@lds_setup.folder(@@autocreated_qa)
    parameters.each {|p| assert_nil folder.spc_channel(@@prefix2s + p), 'wrong S2 channels'}
    $log.info "\nVerify there are no 2S@QA_PARAM_84X_ECP channels in the Exclude List. Press Enter to continue!"
    gets
  end


  # aux methods

  #
  def create_2S_channels(parameters, params={})
    # submit process DCR
    assert dcrp = @@siltest.submit_process_dcr(parameters: parameters, chambers: params[:chambers])
    resp = @@siltest.client.service_response
    # submit meas DCR, number of samples depends on ??
    ###nsamples = parameters.size * 6  # sometimes * 3    skip check unless reliably known how to calc
    assert dcrm = @@siltest.submit_meas_dcr(parameters: parameters, ooc: true, nsamples: :any), 'missing 2S channels?'
    assert folder = @@lds_setup.folder(@@autocreated_qa)
    # set new limits
    if newlimits = params[:newlimits]
      parameters.each {|p| assert folder.spc_channel(p).set_limits(newlimits)}
    end
    # resend meas DCR
    $log.info "sending meas DCR again"
    assert @@siltest.client.send_receive(dcrm.to_xml)
    return resp
  end

  def verify_2slimits(parameters, newlimits, params={})
    assert folder = @@lds_setup.folder(@@autocreated_qa)
    parameters.each {|p|
      # base channel
      assert ch = folder.spc_channel(p)
      assert s = ch.samples.last
      assert verify_hash(newlimits, s.limits, nil, refonly: true)
      # 2S channel
      assert ch2s = folder.spc_channel(@@prefix2s + p)
      assert s2s = ch2s.samples.last
      if params[:no2s]
        assert s2s.time < s.time, 'wrong new samples in GZV channel'  # depending on ch state ?
      else
        assert s2s.time >= s.time, 'not enough samples in GZV channel'  # depending on ch state ?
      end
      # get GZV
      assert gzv = @@siltest.db.greenzonevalue(p, 'R', @@spec), 'no (unique) DB greenzonevalue'
      # 2S limits
      mucl = s.limits['MEAN_VALUE_CENTER'] + gzv * (s.limits['MEAN_VALUE_UCL'] - s.limits['MEAN_VALUE_CENTER'])
      mlcl = s.limits['MEAN_VALUE_CENTER'] - gzv * (s.limits['MEAN_VALUE_CENTER'] - s.limits['MEAN_VALUE_LCL'])
      assert_equal mucl, s2s.limits['MEAN_VALUE_UCL'], 'wrong 2S MEAN UCL'
      assert_equal mlcl, s2s.limits['MEAN_VALUE_LCL'], 'wrong 2S MEAN LCL'
    }
  end

  def verify_db_e10(res, params={})
    expected_chamber_e10_state = params[:expected_chamber_e10_state]
    expected_eqp_e10_state = params[:expected_eqp_e10_state]
    multichamberstates = !!params[:multichamberstates]
    runids = @@siltest.runids(res)
    runids.each {|rid|
      assert pal = @@siltest.db.processingarealookup(rid), "no processingarealookup entry found for runid #{rid}"
      $pal = pal
      $log.info "processingarealookup: #{pal.size}"
      if multichamberstates
        pal.each_with_index {|p, i|
          if i == 0
            assert_equal(expected_chamber_e10_state, p.e10, "E10 state in processingarealookup table #{p.e10} not as expected: #{expected_chamber_e10_state}")
          else
            assert_equal(expected_eqp_e10_state, p.e10, "E10 state in processingarealookup table #{p.e10} not as expected: #{expected_eqp_e10_state} for #{p.processingarea}")
          end
        }
      else
        pal.each {|p| assert_equal(expected_chamber_e10_state, p.e10, "E10 state in processingarealookup table #{p.e10} not as expected: #{expected_chamber_e10_state}")}
      end
      assert run = @@siltest.db.run(rid), "no run entry found for runid #{rid}"
      $log.info "run #{run}"
      run.each {|r| assert_equal(expected_eqp_e10_state, r.e10, "E10 state in run table #{r.e10} not as expected: #{expected_eqp_e10_state}")}
    }
  end

end
