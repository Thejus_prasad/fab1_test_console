=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: U. Koerner, 2012-01-11

History:
  2014-02-05 ssteidte, separated Test_Space07, code cleanup
=end

require_relative 'SILTest'


# CAs Eqp Inhibits (old Test_Space_071)
class SIL_71 < SILTest
  @@cafailure_lothold_fallback = true    # F1 true, F8 false
  @@mpd = 'QA-MB-FTDEPM-LIS-CAEX.01'

  def test00_setup
    $setup_ok = false
    #
    assert lots = $svtest.new_lots(2), 'error creating lots'
    @@testlots += lots
    @@lot2 = lots[1]
    #
    assert @@siltest.set_defaults(lot: lots[0], mpd: @@mpd), "no PPD for MPD #{@@mpd} ?"
    #
    $setup_ok = true
  end

  def test11_equipment_inhibit
    assert_equal 0, $sv.eqp_cleanup(@@siltest.ptool)
    assert channels = @@siltest.setup_channels(cas: ['EquipmentInhibit', 'AutoEquipmentInhibit', 'ReleaseEquipmentInhibit', 'ReleaseEquipmentInhibit-noDefault'])
    assert dcrp = @@siltest.submit_process_dcr
    # send meas DCR with OOC values
    assert dcr = @@siltest.submit_meas_dcr(ooc: true, nooc: 'all*2'), 'error submitting DCR'
    # verify inhibit and comment
    assert @@siltest.wait_inhibit(:eqp, @@siltest.ptool, '', n: channels.size), 'equipment must still have all inhibits'
    inhs = $sv.inhibit_list(:eqp, @@siltest.ptool, details: true)
    inhs.each {|i| 
      rd = i['reason_details'][0]
      assert_equal rd['lot'], dcr.lot_context['id']
      assert_equal rd['cj'], dcr.context[:JobSetup][:SiViewControlJob]['id']
      assert_equal rd['route'], dcrp.lot_context['route']
      assert_equal rd['pd'], dcrp.lot_context['operationID']
      assert_equal rd['opNo'], dcrp.lot_context['operationNumber']
      assert_equal rd['pass'], dcrp.lot_context['passCount']
      
    #  assert_equal rd['charts'][0]['dc_type'], '???'
    #  assert_equal rd['charts'][0]['chart_group'], '???'
    #  assert_equal rd['charts'][0]['chart'], '???'
    #  assert_equal rd['charts'][0]['chart_type'], '???'
    #  assert_equal rd['charts'][0]['url'], '???'
    }
    
    assert @@siltest.verify_ecomment(channels, "TOOL_HELD: #{@@siltest.ptool}: reason={X-S")
    # try to release inhibit with noDefault attribute
    channels.each {|ch| assert !ch.ckc_samples.last.assign_ca('ReleaseEquipmentInhibit-noDefault'), 'wrongly assigned corrective action'}
    assert @@siltest.wait_inhibit(:eqp, @@siltest.ptool, '', n: channels.size), 'equipment must still have all inhibits'
    # release inhibit with noDefault attribute - with comment
    channels.each {|ch| assert ch.ckc_samples.last.assign_ca('ReleaseEquipmentInhibit-noDefault', comment: '[Release=All]'), 'error assigning CA'}
    assert @@siltest.wait_inhibit(:eqp, @@siltest.ptool, '', n: 0), 'equipment must still have all inhibits'
    # send process DCR again
    assert @@siltest.submit_process_dcr
    # send meas DCR with OOC values
    assert dcr = @@siltest.submit_meas_dcr(ooc: true, nooc: 'all*2'), 'error submitting DCR'
    assert @@siltest.wait_inhibit(:eqp, @@siltest.ptool, '', n: channels.size), 'equipment must still have all inhibits'
    assert @@siltest.verify_ecomment(channels, "TOOL_HELD: #{@@siltest.ptool}: reason={X-S")
    # release inhibit
    channels.each {|ch| assert ch.ckc_samples.last.assign_ca('ReleaseEquipmentInhibit'), 'error assigning CA'}
    assert @@siltest.wait_inhibit(:eqp, @@siltest.ptool, '', n: 0), 'equipment must not have an inhibit'
    # set inhibit (manual)
    assert channels[0].ckc_samples.last.assign_ca('EquipmentInhibit'), 'error assigning CA'
    assert @@siltest.wait_inhibit(:eqp, @@siltest.ptool, '', n: 1), 'equipment must have an inhibit'
    assert @@siltest.verify_ecomment(channels, "TOOL_HELD: #{@@siltest.ptool}: reason={X-S")
    # release inhibit
    channels.each {|ch| assert ch.ckc_samples.last.assign_ca('ReleaseEquipmentInhibit'), 'error assigning CA'}
    assert @@siltest.wait_inhibit(:eqp, @@siltest.ptool, '', n: 0), 'equipment must not have an inhibit'
    # set inhibit again (manual)
    assert channels[0].ckc_samples.last.assign_ca('EquipmentInhibit'), 'error assigning CA'
    assert @@siltest.wait_inhibit(:eqp, @@siltest.ptool, '', n: 1), 'equipment must have an inhibit'
    assert @@siltest.verify_ecomment(channels, "TOOL_HELD: #{@@siltest.ptool}: reason={X-S")
    #
    # release inhibit with noDefault attribute
    channels.each {|ch| assert !ch.ckc_samples.last.assign_ca('ReleaseEquipmentInhibit-noDefault'), 'wrongly assigned corrective action'}
    assert @@siltest.wait_inhibit(:eqp, @@siltest.ptool, '', n: 1), 'equipment must still have all inhibits'
  end

  def test11a_equipment_inhibit
    assert_equal 0, $sv.eqp_cleanup(@@siltest.ptool)
    assert channels = @@siltest.setup_channels(cas: ['EquipmentInhibit', 'AutoEquipmentInhibit', 'ReleaseEquipmentInhibit', 'ReleaseEquipmentInhibit-noDefault'])
    assert dcrp = @@siltest.submit_process_dcr
    # send meas DCR with OOC values
    assert dcr = @@siltest.submit_meas_dcr(ooc: true, nooc: 'all*2'), 'error submitting DCR'
    # verify inhibit and comment
    assert @@siltest.wait_inhibit(:eqp, @@siltest.ptool, '', n: channels.size), 'equipment must still have all inhibits'
    inhs = $sv.inhibit_list(:eqp, @@siltest.ptool, details: true)
    inhs.each {|i| 
      rd = i['reason_details'][0]
      assert_equal rd['lot'], dcr.lot_context['id']
      assert_equal rd['cj'], dcr.context[:JobSetup][:SiViewControlJob]['id']
      assert_equal rd['route'], dcrp.lot_context['route']
      assert_equal rd['pd'], dcrp.lot_context['operationID']
      assert_equal rd['opNo'], dcrp.lot_context['operationNumber']
      assert_equal rd['pass'], dcrp.lot_context['passCount']
    }
    
    assert @@siltest.verify_ecomment(channels, "TOOL_HELD: #{@@siltest.ptool}: reason={X-S")
  end

  def test12_equipment_inhibit_fallback
    assert_equal 0, $sv.eqp_cleanup(@@siltest.ptool)
    mpd = "QA-NO-RESP-PD.01"
    assert channels = @@siltest.setup_channels(cas: ['EquipmentInhibit', 'AutoEquipmentInhibit', 'ReleaseEquipmentInhibit'], mpd: mpd)
    # send meas DCR with OOC values
    assert dcr = @@siltest.submit_meas_dcr(ooc: true, nooc: 'all*2', mpd: mpd), 'error submitting DCR'
    # verify inhibit and comment
    assert @@siltest.wait_inhibit(:eqp, @@siltest.ptool, '', n: 0), 'equipment must not have an inhibit'
    if @@cafailure_lothold_fallback
      assert @@siltest.wait_futurehold('Execution of [AutoEquipmentInhibit] failed'), 'missing future hold'
      assert @@siltest.verify_ecomment(channels, "LOT_HELD: #{@@siltest.lot}: reason={X-S")
    else
      @@siltest.parameters.each {|p| assert @@siltest.wait_notification('Execution of [AutoEquipmentInhibit] failed'), "no notification for #{p}"}
    end
    assert @@siltest.verify_ecomment(channels, 'Execution of [AutoEquipmentInhibit] failed')
  end

  def test13_auto_and_manual_lot_and_equipment_eqp_inhibit_fails
    assert_equal 0, $sv.eqp_cleanup(@@siltest.ptool)
    mpd = "QA-NO-RESP-PD.01"
    assert channels = @@siltest.setup_channels(cas: ['EquipmentInhibit', 'AutoEquipmentInhibit', 'ReleaseEquipmentInhibit', 'LotHold', 'AutoLotHold', 'CancelLotHold'], mpd: mpd)
    # send meas DCR with OOC values
    assert dcr = @@siltest.submit_meas_dcr(ooc: true, nooc: 'all*2', mpd: mpd), 'error submitting DCR'
    # check lot hold
    assert @@siltest.wait_futurehold('Execution of [AutoEquipmentInhibit] failed'), 'missing future hold'
    count = @@cafailure_lothold_fallback ? 6 : 5
    assert count <= $sv.lot_futurehold_list(@@siltest.lot).count, 'wrong number of future holds'
    # check two ext. comments
    assert @@siltest.verify_ecomment(channels, "LOT_HELD: #{@@siltest.lot}: reason={X-S")
    assert @@siltest.verify_ecomment(channels, 'Execution of [AutoEquipmentInhibit] failed: reason={Error in SiView')
    #
    # release lot holds
    assert_equal 0, $sv.lot_gatepass(@@siltest.lot), 'SiView GatePass error'
    refute_empty $sv.lot_hold_list(@@siltest.lot, type: 'FutureHold'), 'missing lot hold'
    channels.each {|ch| assert ch.ckc_samples.last.assign_ca('ReleaseLotHold'), 'error assigning corective action'}
    assert @@siltest.wait_lothold_release('[(Raw above specification)'), 'no further holds expected'
    assert @@siltest.wait_futurehold('[(Raw above specification)', n: 0), 'lot must have no future hold'
  end

  def test14_equipment_inhibit_route_inhibit
    route = $sv.lot_info(@@siltest.lot).route
    assert_equal 0, $sv.inhibit_cancel(:route, route)
    assert_equal 0, $sv.eqp_cleanup(@@siltest.ptool)
    assert channels = @@siltest.setup_channels
    ch1 = channels.select {|e| channels.index(e).odd?}
    ch2 = channels.select {|e| channels.index(e).even?}
    assert @@siltest.assign_verify_cas(ch1, 'AutoEquipmentInhibit')
    assert @@siltest.assign_verify_cas(ch2, 'AutoRouteInhibit')
    # send process DCRs
    assert @@siltest.submit_process_dcr
    # send DCR with OOC values
    assert dcr = @@siltest.submit_meas_dcr(ooc: true, nooc: 'all*2', route: route)
    #
    assert @@siltest.wait_inhibit(:route, route, '', n: ch2.size), 'route must have an inhibit'
    assert @@siltest.wait_inhibit(:eqp, @@siltest.ptool, '', n: ch1.size), 'equipment must have an inhibit'
    assert @@siltest.verify_ecomment(ch1, "TOOL_HELD: #{@@siltest.ptool}: reason={X-S")
    assert @@siltest.verify_ecomment(ch2, "ROUTE_HELD:#{route}: reason={X-S")
  end

  def test15_additional_inhibit
    assert_equal 0, $sv.eqp_cleanup(@@siltest.ptool)
    assert channels = @@siltest.setup_channels(cas: ['EquipmentInhibit', 'AutoEquipmentInhibit', 'ReleaseEquipmentInhibit', 'ReleaseEquipmentInhibit-noDefault'])
    # set "external" inhibit with reason code X-S1 and no end date
    assert $sv.inhibit_entity(:eqp, @@siltest.ptool, tend: '', reason: 'X-S1')
    assert @@siltest.submit_process_dcr
    # send DCR with OOC values
    assert dcr = @@siltest.submit_meas_dcr(ooc: true, nooc: 'all*2'), 'error submitting DCR'
    # verify inhibit count
    assert wait_for {$sv.inhibit_list(:eqp, @@siltest.ptool).size == channels.size + 1}, 'wrong inhibit count'
    # release inhibit with noDefault attribute - with comment
    channels.each {|ch| assert ch.ckc_samples.last.assign_ca('ReleaseEquipmentInhibit-noDefault', comment: '[Release=All]'), 'error assigning CA'}
    assert wait_for {$sv.inhibit_list(:eqp, @@siltest.ptool).size == 1}, 'wrong inhibit count'
    assert_equal 'X-S1', $sv.inhibit_list(:eqp, @@siltest.ptool).first.reason
    # send process DCR again
    assert @@siltest.submit_process_dcr
    # send DCR with OOC values
    assert dcr = @@siltest.submit_meas_dcr(ooc: true, nooc: 'all*2'), 'error submitting DCR'
    # verify inhibit count
    assert wait_for {$sv.inhibit_list(:eqp, @@siltest.ptool).size == channels.size + 1}, 'wrong inhibit count'
    # release inhibit
    channels.each {|ch| assert ch.ckc_samples.last.assign_ca('ReleaseEquipmentInhibit'), 'error assigning CA'}
    assert wait_for {$sv.inhibit_list(:eqp, @@siltest.ptool).size == 1}, 'wrong inhibit count'
    assert_equal 'X-S1', $sv.inhibit_list(:eqp, @@siltest.ptool).first.reason
    # set inhibit (manual)
    assert channels[0].ckc_samples.last.assign_ca('EquipmentInhibit'), 'error assigning CA'
    assert wait_for {$sv.inhibit_list(:eqp, @@siltest.ptool).size == 2}, 'wrong inhibit count'
    # release inhibit
    assert channels[0].ckc_samples.last.assign_ca('ReleaseEquipmentInhibit'), 'error assigning CA'
    assert wait_for {$sv.inhibit_list(:eqp, @@siltest.ptool).size == 1}, 'wrong inhibit count'
    assert_equal 'X-S1', $sv.inhibit_list(:eqp, @@siltest.ptool).first.reason
  end
end
