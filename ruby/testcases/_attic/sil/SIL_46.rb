=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: Conny Stenzel, 2012-06-27

History:
  2017-08-01 cstenzel/sfrieske, refactored all tests
=end

require_relative 'SILTest'


# Non WIP DCRs  was: Test_Space16   based on Spec C05-00001624
class SIL_46 < SILTest
  @@test_defaults = {}
  @@reticle = 'QAReticle0001'  # must really exist, gets inhibited
  @@wafer_parameters = ['QA_PARAM_900W', 'QA_PARAM_901W', 'QA_PARAM_902W', 'QA_PARAM_903W', 'QA_PARAM_904W']
  @@eqp_parameters = ['QA_PARAM_900E', 'QA_PARAM_901E', 'QA_PARAM_902E', 'QA_PARAM_903E', 'QA_PARAM_904E']
  @@lot_parameters = ['QA_PARAM_90046L', 'QA_PARAM_90146L', 'QA_PARAM_90246L', 'QA_PARAM_90346L', 'QA_PARAM_90446L']


  def test00_setup
    $setup_ok = false
    #
    assert @@siltest.set_defaults(@@test_defaults)
    #
    $setup_ok = true
  end

  # see MSR 512888
  def test11_noneqp1
    # submit Equipment DCR
    eqp = 'QATest1'
    dcr = SIL::DCR.new(eqp: eqp, event: 'non_wip_dcr')
    dcr.context.delete(:JobSetup)
    dcr.add_parameters(:default, {eqp=>22.0}, readingtype: 'Equipment', valuetype: 'double')
    assert @@siltest.send_dcr_verify(dcr, nsamples: @@siltest.parameters.size), 'error sending DCR'
    # verify ekeys
    assert folder = @@lds_setup.folder(@@autocreated_nonwip)
    @@siltest.parameters.each {|p|
      assert ch = folder.spc_channel(p), 'missing channel'
      assert_equal 1, ch.samples.size, 'wrong samples'
      assert verify_hash({'PTool'=>eqp, 'Event'=>'non_wip_dcr'}, ch.samples.first.ekeys, nil, refonly: true), 'wrong ekeys'
    }
  end

  # MSR558175
  def test12_nonwip_nocharting
    eqp = 'QATest1'
    dcr = SIL::DCR.new(eqp: eqp)
    dcr.context.delete(:JobSetup)
    dcr.add_parameters(:default, {eqp=>22.0}, readingtype: 'Equipment', valuetype: 'double', nocharting: true)
    assert @@siltest.send_dcr_verify(dcr, nsamples: 0), 'error sending DCR'
  end

  # MSR558175
  def test13_nonwip_invalid
    eqp = 'QATest1'
    readings = {eqp=>[22.0, 30.1, 23.0]}
    dcr = SIL::DCR.new(eqp: eqp)
    dcr.context.delete(:JobSetup)
    # reading invalid
    dcr.add_parameter(@@siltest.parameters[0], readings, readingtype: 'Equipment', valuetype: 'double')
    dcr.add_readings({eqp=>102.21}, readingtype: 'Equipment', valuetype: 'double', reading_valid: false)
    # parameter invalid attribute should be ignored, readings are valid
    dcr.add_parameter(@@siltest.parameters[1], readings, readingtype: 'Equipment', valuetype: 'double', parameter_valid: false)
    # all readings invalid
    dcr.add_parameter(@@siltest.parameters[2], readings, readingtype: 'Equipment', valuetype: 'double', reading_valid: false)
    # submit DCR
    assert @@siltest.send_dcr_verify(dcr, nsamples: 1), 'error sending DCR'
    # verify channel for parameter0
    assert folder = @@lds_setup.folder(@@autocreated_nonwip)
    assert ch = folder.spc_channel(@@siltest.parameters[0]), 'missing channel'
    assert_equal 1, ch.samples.size, 'wrong samples'
  end

  # DCR with job context, but only equipment parameters
  def test15_nonwip_jobcontext
    eqp = 'QATest1'
    dcr = SIL::DCR.new(eqp: eqp)  # don't delete :JobSetup; add_lot desirable ?
    dcr.add_parameters(:default, {eqp=>28.0}, readingtype: 'Equipment', valuetype: 'double')
    assert @@siltest.send_dcr_verify(dcr), 'error sending DCR'
    # verify channels and keys
    assert folder = @@lds_setup.folder(@@autocreated_nonwip)
    @@siltest.parameters.each {|p|
      assert ch = folder.spc_channel(p), 'missing channel'
      assert_equal 1, ch.samples.size, 'wrong samples'
      assert_equal eqp, ch.samples.first.ekeys['PTool'], 'wrong ekeys'
    }
    assert @@siltest.send_dcr_verify(dcr), 'error sending DCR'
  end

  def test16_chamber_subprocessingarea
    eqp = 'QATest1'
    dcr = SIL::DCR.new(eqp: eqp, chambers: @@siltest.chambers, event: 'non_wip_dcr')
    dcr.context.delete(:JobSetup)
    dcr.add_parameters(:default, {eqp=>23.3}, processing_areas: 'cycle_parameter', readingtype: 'Equipment')
    assert @@siltest.send_dcr_verify(dcr), 'error sending DCR'
    # verify channels and keys
    assert folder = @@lds_setup.folder(@@autocreated_nonwip)
    @@siltest.parameters.each_with_index {|p, i|
      assert ch = folder.spc_channel(p), 'missing channel'
      assert_equal 1, ch.samples.size, 'wrong samples'
      s = ch.samples.last
      cha = @@siltest.chambers[i % @@siltest.chambers.size]
      ekeys = {'PTool'=>eqp, 'PChamber'=>cha, 'Event'=>'non_wip_dcr', 'Reserve4'=>"#{cha}_SIDE1", 'Reserve5'=>eqp,
        'Reserve6'=>cha, 'Reserve7'=>dcr.context['reportingTimeStamp']}
      assert verify_hash(ekeys, s.ekeys, nil, refonly: true), 'wrong ekeys'
      dkeys = {'DatReserve4'=>"#{cha}_SIDE1", 'DatReserve5'=>eqp, 'DatReserve6'=>cha,
        'DatReserve7'=>dcr.context['reportingTimeStamp']}
      assert verify_hash(dkeys, s.dkeys, nil, refonly: true), 'wrong dkeys'
    }
  end

  def test17_chamber_subprocessingarea_multi
    eqp = 'QATest1'
    dcr = SIL::DCR.new(eqp: eqp, chambers: @@siltest.chambers, event: 'non_wip_dcr')
    dcr.context.delete(:JobSetup)
    dcr.add_parameter(@@siltest.parameters[0], {eqp=>[22.0, 21.0, 20.0]}, processing_areas: 'cycle_reading', readingtype: 'Equipment')
    assert @@siltest.send_dcr_verify(dcr, nsamples: 1), 'error sending DCR'
    # verify channel and keys
    assert folder = @@lds_setup.folder(@@autocreated_nonwip)
    assert ch = folder.spc_channel(@@siltest.parameters[0]), 'missing channel'
    assert_equal 1, ch.samples.size, 'wrong samples'
    s = ch.samples.last
    chas = @@siltest.chambers.join(':')
    spas = @@siltest.chambers.map {|cha| "#{cha}_SIDE1"}.join(':')
    ekeys = {'PTool'=>eqp, 'PChamber'=>chas, 'Event'=>'non_wip_dcr', 'Reserve4'=>spas, 'Reserve5'=>eqp,
      'Reserve6'=>chas, 'Reserve7'=>dcr.context['reportingTimeStamp']}
    assert verify_hash(ekeys, s.ekeys, nil, refonly: true), 'wrong ekeys'
    dkeys = {'DatReserve4'=>spas, 'DatReserve5'=>eqp, 'DatReserve6'=>chas,
      'DatReserve7'=>dcr.context['reportingTimeStamp']}
    assert verify_hash(dkeys, s.dkeys, nil, refonly: true), 'wrong dkeys'
  end

  # was: original and new tests currently fail!
  def testdev18_chamber_subprocessingarea_sap
    ##assert $spctest.sil_upload_verify_generickey(@@file_generic_keys_fixed_params_nonwip[1])
    sapicid = '0010'
    sapoo = '0095'
    sapdcpid = '60012916'
    sapon = '67853957'
    eqp = 'QATest1'
    sapjob = {'inspectionCharacteristicID'=>'0010', 'orderOperation'=>'0095', 'SAPdataCollectionPlanID'=>'60012916', 'SAPOrderNumber'=>'67853957'}
    # submit a DCR with no SiViewControlJob but an SAPJob
    dcr = SIL::DCR.new(eqp: eqp, chambers: @@siltest.chambers, event: 'non_wip_dcr')
    dcr.context[:JobSetup] = {SAPJob: sapjob}
    dcr.add_parameters(:default, {eqp=>23.3}, processing_areas: 'cycle_parameter', readingtype: 'Equipment')
    assert @@siltest.send_dcr_verify(dcr), 'error sending DCR'
    # verify channels and keys
    assert folder = @@lds_setup.folder(@@autocreated_nonwip)
    @@siltest.parameters.each_with_index {|p, i|
      assert ch = folder.spc_channel(p), 'missing channel'
      assert_equal 1, ch.samples.size, 'wrong samples'
      s = ch.samples.last
      cha = @@siltest.chambers[i % @@siltest.chambers.size]
      ekeys = {'PTool'=>eqp, 'PChamber'=>cha, 'Event'=>'non_wip_dcr', 'Reserve4'=>"#{cha}_SIDE1", 'Reserve5'=>eqp,
        'Reserve6'=>cha, 'Reserve7'=>dcr.context['reportingTimeStamp'], 'Reserve8'=>sapdcpid, 'Reserve1'=>sapoo, 'Reserve2'=>sapon, 'Reserve3'=>sapicid}
      assert verify_hash(ekeys, s.ekeys, nil, refonly: true), 'wrong ekeys'
      dkeys = {'DatReserve4'=>"#{cha}_SIDE1", 'DatReserve5'=>eqp, 'DatReserve6'=>cha,
        'DatReserve7'=>dcr.context['reportingTimeStamp'], 'DatReserve8'=>sapdcpid, 'DatReserve1'=>sapoo, 'DatReserve2'=>sapon, 'DatReserve3'=>sapicid}
      assert verify_hash(dkeys, s.dkeys, nil, refonly: true), 'wrong dkeys'
    }
  end

  # DCR with job context, but only equipment parameters
  # TODO: it does send wafers, broken ?
  def test19_dummydcr
    dcr = SIL::DCR.new
    dcr.add_lot(:default)
    dcr.add_parameter('Dummy', {'Reading1'=>'', 'Reading2'=>'', 'Reading3'=>''})
    assert @@siltest.send_dcr_verify(dcr), 'error sending DCR'
  end

  # MSR669435
  def test21_nonwip_generickey_dc_double
    eqp = 'QATest1'
    parameters = (5..8).collect {|i| "QA-NonWip-Test-0#{i}"}
    gkvals = [26.0, 27.0, 28.0, 29.0]
    dcr = SIL::DCR.new(eqp: eqp)
    dcr.context.delete(:JobSetup)
    parameters.each_index {|i|
      dcr.add_parameter(parameters[i], {eqp=>gkvals[i]}, readingtype: 'Equipment', valuetype: 'double', nocharting: true)
    }
    dcr.add_parameter(@@siltest.parameters[0], {eqp=>[555]}, readingtype: 'Equipment')
    assert @@siltest.send_dcr_verify(dcr, nsamples: 1), 'error sending DCR'
    # verify keys
    assert folder = @@lds_setup.folder(@@autocreated_nonwip)
    assert ch = folder.spc_channel(@@siltest.parameters[0]), 'missing channel'
    assert_equal 1, ch.samples.size, 'wrong samples'
    s = ch.samples.last
    [1,2,3,8].each {|i|
      assert_equal gkvals[(i) % 8].to_s, s.ekeys["Reserve#{i}"], 'wrong ekey'  # mapping Reserve1 => gkvals[5], etc.
      assert_equal gkvals[(i) % 8].to_s, s.dkeys["DatReserve#{i}"], 'wrong ekey'  # mapping Reserve1 => gkvals[5], etc.
    }
  end

  # MSR669435
  def test22_nonwip_generickey_dc_string
    eqp = 'QATest1'
    parameters = (5..8).collect {|i| "QA-NonWip-Test-0#{i}"}
    gkvals = ('A' .. 'D').to_a
    dcr = SIL::DCR.new(eqp: eqp)
    dcr.context.delete(:JobSetup)
    parameters.each_index {|i|
      dcr.add_parameter(parameters[i], {eqp=>gkvals[i]}, readingtype: 'Equipment', valuetype: 'string', nocharting: true)
    }
    dcr.add_parameter(@@siltest.parameters[0], {eqp=>[555]}, readingtype: 'Equipment')
    assert @@siltest.send_dcr_verify(dcr, nsamples: 1), 'error sending DCR'
    # verify keys
    assert folder = @@lds_setup.folder(@@autocreated_nonwip)
    assert ch = folder.spc_channel(@@siltest.parameters[0]), 'missing channel'
    assert_equal 1, ch.samples.size, 'wrong samples'
    s = ch.samples.last
    [1,2,3,8].each {|i|
      assert_equal gkvals[(i) % 8], s.ekeys["Reserve#{i}"], 'wrong ekey'  # mapping Reserve1 => gkvals[5], etc.
      assert_equal gkvals[(i) % 8], s.dkeys["DatReserve#{i}"], 'wrong ekey'  # mapping Reserve1 => gkvals[5], etc.
    }
  end

  def test31_reticle
    ['ReticleInhibit', 'AutoReticleInhibit', 'ReleaseReticleInhibit'].each {|ca| assert SIL::SpaceApi.ca(ca), "CA #{ca} not defined"}
    assert_equal 0, $sv.inhibit_cancel(:reticle, @@reticle)
    eqp = 'QATest1'
    # send DCR
    dcr = SIL::DCR.new(eqp: eqp)
    dcr.context.delete(:JobSetup)
    dcr.add_parameters(:default, {@@reticle=>1}, readingtype: 'Reticle')
    assert @@siltest.send_dcr_verify(dcr), 'error sending DCR'
    # verify channels and keys
    assert folder = @@lds_setup.folder(@@autocreated_nonwip)
    @@siltest.parameters.each {|p|
      assert ch = folder.spc_channel(p), 'missing channel'
      assert_equal 1, ch.samples.size, 'wrong samples'
      s = ch.samples.last
      assert verify_hash({'PTool'=>eqp, 'Event'=>dcr.context['eventReportingName']}, s.ekeys, nil, refonly: true), 'wrong ekeys'
      assert verify_hash({'MTool'=>eqp, 'Reticle'=>@@reticle}, s.dkeys, nil, refonly: true), 'wrong ekeys'
      ch
    }
    # assign limits and CAs
    @@siltest.parameters.each {|p|
      assert ch = folder.spc_channel(p), 'missing channel'
      assert ch = folder.spc_channel(p), 'missing channel'
      assert ch.set_limits({'MEAN_VALUE_UCL'=>2.0, 'MEAN_VALUE_CENTER'=>1.0, 'MEAN_VALUE_LCL'=>0.0})
      assert @@siltest.assign_verify_cas(ch, ['ReticleInhibit', 'AutoReticleInhibit', 'ReleaseReticleInhibit'])
    }
    # send DCR with OOC value
    dcr = SIL::DCR.new(eqp: eqp)
    dcr.context.delete(:JobSetup)
    dcr.add_parameters(:default, {@@reticle=>5}, readingtype: 'Reticle')
    assert @@siltest.send_dcr_verify(dcr), 'error sending DCR'
    # verify CA and ecomment
    refute_empty @@siltest.wait_inhibit(:reticle, @@reticle, '', n:  @@siltest.parameters.size), 'missing reticle inhibit'
    channels = @@siltest.parameters.collect {|p| folder.spc_channel(p)}
    assert @@siltest.verify_ecomment(channels, "RETICLE_HELD:#{@@reticle}: reason={X-S"), 'wrong ecomment'
  end

  def test32_reticle_nocharting
    dcr = SIL::DCR.new
    dcr.context.delete(:JobSetup)
    dcr.add_parameters(:default, {@@reticle=>1}, readingtype: 'Reticle', nocharting: true)
    assert @@siltest.send_dcr_verify(dcr), 'error sending DCR'
  end

  # MSR512888
  def test33_reticle_invalid
    readings = {@@reticle=>1}
    dcr = SIL::DCR.new
    dcr.context.delete(:JobSetup)
    # reading invalid
    dcr.add_parameter(@@siltest.parameters[0], readings, readingtype: 'Reticle')
    dcr.add_readings({@@reticle=>102.21}, readingtype: 'Reticle', valuetype: 'double', reading_valid: false)
    # parameter invalid attribute should be ignored, readings are valid
    dcr.add_parameter(@@siltest.parameters[1], readings, readingtype: 'Reticle', valuetype: 'double', parameter_valid: false)
    # all readings invalid
    dcr.add_parameter(@@siltest.parameters[2], readings, readingtype: 'Reticle', valuetype: 'double', reading_valid: false)
    # submit DCR
    assert @@siltest.send_dcr_verify(dcr, nsamples: 1), 'error sending DCR'
    # verify channel for parameter0
    assert folder = @@lds_setup.folder(@@autocreated_nonwip)
    assert ch = folder.spc_channel(@@siltest.parameters[0]), 'missing channel'
    assert_equal 1, ch.samples.size, 'wrong samples'
  end

  # DCR with job context, equipment parameters, wafer parameters, lot parameters
  def test41_nonwip_jobcontext
    runs = 3
    eqp = 'QATest1'
    lot = 'SILQALOT00.00'
    dcr = SIL::DCR.new(eqp: eqp)
    dcr.add_lot(lot, op: 'QA-SIL46-41W.01')
    dcr.add_parameters(@@wafer_parameters, :default)
    dcr.add_parameters(@@lot_parameters, {lot=>22.2}, readingtype: 'Lot')
    dcr.add_parameters(@@eqp_parameters, {eqp=>32.2}, readingtype: 'Equipment')
    # submit DCR
    runs.times {assert @@siltest.send_dcr_verify(dcr, wip: 'mixed'), 'error sending DCR'}
    # verify channels
    assert folder = @@lds.folder(@@autocreated_qa)
    @@wafer_parameters.each {|p|
      assert ch = folder.spc_channel(p)
      assert_equal SIL::DCR::DefaultWaferValues.size * runs, ch.samples.size
      assert_equal SIL::DCR::DefaultTechnology, ch.samples.last.ekeys['Technology']
      dcrreadingssize = dcr.readings(p).size
      #$log.info "dcrreadingssize: #{dcrreadingssize}"
      samplepos = dcrreadingssize + 1
      #$log.info "samplepos: #{samplepos}"
      s = ch.samples[-samplepos]
      assert s.iflag && s.icomment && s.icomment.include?('Rework'), "wrong autoflagging for rework (#{p}@#{ch}\niflag: #{s.iflag}\nicomment: #{s.icomment})"
    }
    @@lot_parameters.each {|p|
      assert ch = folder.spc_channel(p)
      assert_equal 1 * runs, ch.samples.size
      assert_equal SIL::DCR::DefaultTechnology, ch.samples.last.ekeys['Technology']
      s = ch.samples[-2]
      assert s.iflag && s.icomment && s.icomment.include?('Rework'), 'wrong autoflagging for rework'
    }
    assert folder = @@lds_setup.folder(@@autocreated_nonwip)
    @@eqp_parameters.each {|p|
      assert ch = folder.spc_channel(p)
      assert_equal 1 * runs, ch.samples.size
      assert_equal '-', ch.samples.last.ekeys['Technology']
      s = ch.samples[-2]
      assert !s.iflag || !s.icomment || !s.icomment.include?('Rework'), "wrong autoflagging for nonwip rework (#{p}@#{ch}\niflag: #{s.iflag}\nicomment: #{s.icomment})"
      assert !s.iflag || !s.icomment || !s.icomment.include?('Remeasurement'), 'wrong autoflagging for nonwip remeasurement'
    }
  end

  # DCR with job context, equipment parameters, lot parameters
  def test42_nonwip_jobcontext
    eqp = 'QATest1'
    lot = 'SILQALOT00.00'
    dcr = SIL::DCR.new(eqp: eqp)
    dcr.add_lot(lot, op: 'QA-SIL46-41W.01')
    dcr.add_parameters(@@lot_parameters, {lot=>22.2}, readingtype: 'Lot')
    dcr.add_parameters(@@eqp_parameters, {eqp=>32.2}, readingtype: 'Equipment')
    # submit DCR
    assert @@siltest.send_dcr_verify(dcr, wip: 'mixed'), 'error sending DCR'
    # verify channels
    assert folder = @@lds.folder(@@autocreated_qa)
    @@lot_parameters.each {|p|
      assert ch = folder.spc_channel(p)
      assert_equal 1, ch.samples.size
      assert_equal SIL::DCR::DefaultTechnology, ch.samples.last.ekeys['Technology']
    }
    assert folder = @@lds_setup.folder(@@autocreated_nonwip)
    @@eqp_parameters.each {|p|
      assert ch = folder.spc_channel(p)
      assert_equal 1, ch.samples.size
      assert_equal '-', ch.samples.last.ekeys['Technology']
    }
  end

  # TODO: move to SIL_1x ?
  def test51_noneqp_chartcheck
    eqp = 'QATest1'
    parameters = @@siltest.parameters.collect {|p| "#{p}_NONWIP_ECP"}
    dcr = SIL::DCR.new(eqp: eqp, event: 'non_wip_dcr')
    dcr.context.delete(:JobSetup)
    dcr.add_parameters(parameters, {eqp=>50.0}, readingtype: 'Equipment')
    assert @@siltest.send_dcr_verify(dcr), 'error sending DCR'
    # verify channel creation, set state to Study
    assert folder = @@lds_setup.folder(@@autocreated_nonwip)
    parameters.each {|p|
      assert ch = folder.spc_channel(p), 'missing channel'
      assert ch.set_state('Study')
    }
    # resend DCR
    assert @@siltest.send_dcr_verify(dcr), 'error sending DCR'
    # verify channel ekeys
    parameters.each {|p|
      assert ch = folder.spc_channel(p), 'missing channel'
      samples = ch.samples
      assert_equal 2, samples.size, 'wrong number of samples'
      assert verify_hash({'PTool'=>eqp, 'Event'=>'non_wip_dcr'}, samples.last.ekeys, nil, refonly: true), 'wrong samples'
    }
  end

end
