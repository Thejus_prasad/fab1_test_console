=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: Steffen Steidten, 2011-11-25

History:
  2017-08-01 cstenzel/sfrieske, refactored all tests
=end

require_relative 'SILTest'

# SIL Autocharting no upload
class SIL_01 < SILTest
  @@test_defaults = {parameters: ['QA_PARAM_900NO', 'QA_PARAM_901NO', 'QA_PARAM_902NO', 'QA_PARAM_903NO', 'QA_PARAM_904NO']}


  def test00_setup
    $setup_ok = false
    #
    @@siltest.set_defaults(@@test_defaults)
    @@advanced_props = @@siltest.server.read_jobprops
    #
    qq = {pd: SIL::DCR::DefaultOperation, route: SIL::DCR::DefaultRoute,
      product: SIL::DCR::DefaultProduct, productgroup: SIL::DCR::DefaultProductGroup}
    # verify the parameters have no spec limits
    @@siltest.parameters.each {|p| assert_equal 0, @@siltest.db.nspeclimits(p, qq), "wrong speclimit for #{p}"}
    # verify the default parameters have speclimits
    @@siltest.defaults[:parameters].each {|p| refute_equal 0, @@siltest.db.nspeclimits(p, qq), "missing speclimit for #{p}"}
    #
    $setup_ok = true
  end

  def test11_INLINE
    pname = :'parameters.missingspeclimit.upload.inline.enabled'
    assert prop = @@advanced_props[pname], "no such prop: #{pname.inspect}"
    # submit DCR and verify
    if prop == 'true'
      assert dcr = @@siltest.submit_meas_dcr(product: 'NOProd', productgroup: 'NOPG')
      assert @@siltest.notifications.wait_for(timeout: 120, tstart: @@siltest.client.txtime - 2, filter: {'Application'=>'SIL'}) {|msgs|
        msgs.find {|m| m['Message'].include?('Error code : NO_SPEC_LIMITS')}
      }, 'invalid notification'
    else
      assert dcr = @@siltest.submit_meas_dcr(nsamples: 0)
    end
  end

  def test12_TEST
    pname = :'parameters.missingspeclimit.upload.setup.enabled'
    assert prop = @@advanced_props[pname], "no such prop: #{pname.inspect}"
    # submit DCR and verify
    if prop == 'true'
      assert dcr = @@siltest.submit_meas_dcr(technology: 'TEST', product: 'NOProd', productgroup: 'NOPG')
      assert @@siltest.notifications.wait_for(timeout: 120, tstart: @@siltest.client.txtime - 2, filter: {'Application'=>'SIL'}) {|msgs|
        msgs.find {|m| m['Message'].include?('Error code : NO_SPEC_LIMITS')}
      }, 'invalid notification'
    else
      assert dcr = @@siltest.submit_meas_dcr(technology: 'TEST', product: 'NOProd', productgroup: 'NOPG', nsamples: 0)
    end
  end

  def test21_INLINE_mixed
    pname = :'parameters.missingspeclimit.upload.inline.enabled'
    assert prop = @@advanced_props[pname], "no such prop: #{pname.inspect}"
    # send mixed parameters
    parameters = @@siltest.parameters + @@siltest.defaults[:parameters]
    nsamples = @@siltest.defaults[:parameters].size * SIL::DCR::DefaultWaferValues.size
    # submit DCR and verify
    if prop == 'true'
      assert dcr = @@siltest.submit_meas_dcr(parameters: parameters, product: 'NOProd', productgroup: 'NOPG')
      assert @@siltest.notifications.wait_for(timeout: 120, tstart: @@siltest.client.txtime - 2, filter: {'Application'=>'SIL'}) {|msgs|
        msgs.find {|m| m['Message'].include?('Error code : NO_SPEC_LIMITS')}
      }, 'invalid notification'
    else
      assert dcr = @@siltest.submit_meas_dcr(parameters: parameters, product: 'NOProd', productgroup: 'NOPG', nsamples: nsamples)
    end
  end

  def test22_TEST_mixed
    pname = :'parameters.missingspeclimit.upload.setup.enabled'
    assert prop = @@advanced_props[pname], "no such prop: #{pname.inspect}"
    # send mixed parameters
    parameters = @@siltest.parameters + @@siltest.defaults[:parameters]
    nsamples = @@siltest.defaults[:parameters].size * SIL::DCR::DefaultWaferValues.size
    # submit DCR and verify
    if prop == 'true'
      assert dcr = @@siltest.submit_meas_dcr(parameters: parameters, technology: 'TEST', product: 'NOProd', productgroup: 'NOPG')
      assert @@siltest.notifications.wait_for(timeout: 120, tstart: @@siltest.client.txtime - 2, filter: {'Application'=>'SIL'}) {|msgs|
        msgs.find {|m| m['Message'].include?('Error code : NO_SPEC_LIMITS')}
      }, 'invalid notification'
    else
      assert dcr = @@siltest.submit_meas_dcr(parameters: parameters, technology: 'TEST', product: 'NOProd', productgroup: 'NOPG', nsamples: nsamples)
    end
  end

end
