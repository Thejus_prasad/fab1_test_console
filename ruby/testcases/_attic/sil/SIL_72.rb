=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: U. Koerner, 2012-01-11

History:
  2014-02-05 ssteidte, separated Test_Space07, code cleanup
=end

require_relative 'SILTest'


# CAs Chamber Inhibit (old Test_Space_072)
class SIL_72 < SILTest
  def test00_setup
    $setup_ok = false
    #
    assert lots = $svtest.new_lots(2), 'error creating lots'
    @@testlots += lots
    @@lot2 = lots[1]
    #
    assert @@siltest.set_defaults(lot: lots[0]), "no PPD for MPD #{@@siltest.mpd} ?"
    #
    $setup_ok = true
  end

  def test11_chamber_inhibit
    assert_equal 0, $sv.eqp_cleanup(@@siltest.ptool)
    assert channels = @@siltest.setup_channels(cas: ['AutoLotHold', 'CancelLotHold', 'ChamberInhibit', 'AutoChamberInhibit', 'ReleaseChamberInhibit', 'ReleaseChamberInhibit-noDefault'])
    # send process DCR
    assert @@siltest.submit_process_dcr
    # send meas DCR with OOC values
    assert dcr = @@siltest.submit_meas_dcr(ooc: true, nooc: 'all*2'), 'error submitting DCR'
    assert @@siltest.wait_inhibit(:eqp_chamber, @@siltest.ptool, '', n: @@siltest.chambers.size*channels.size), 'chamber must have an inhibit'
    assert @@siltest.verify_ecomment(channels, "TOOL+CHAMBER_HELD: #{@@siltest.ptool}/#{@@siltest.chambers.first}: reason={X-S")
    # release inhibit with noDefault attribute
    channels.each {|ch| assert !ch.ckc_samples.last.assign_ca('ReleaseChamberInhibit-noDefault'), 'wrongly assigned corrective action'}
    assert @@siltest.wait_inhibit(:eqp_chamber, @@siltest.ptool, '', n: @@siltest.chambers.size*channels.size), 'chamber must still have all inhibits'
    # release inhibit with noDefault attribute - with comment
    channels.each {|ch| assert ch.ckc_samples.last.assign_ca('ReleaseChamberInhibit-noDefault', comment: '[Release=All]'), 'error assigning corrective action'}
    assert @@siltest.wait_inhibit(:eqp_chamber, @@siltest.ptool, '', n: 0), 'chamber must not have an inhibit'
    assert @@siltest.submit_process_dcr
    # send DCR with OOC values
    assert dcr = @@siltest.submit_meas_dcr(ooc: true, nooc: 'all*2'), 'error submitting DCR'
    assert @@siltest.wait_inhibit(:eqp_chamber, @@siltest.ptool, '', n: @@siltest.chambers.size*channels.size), 'chamber must have an inhibit'
    assert @@siltest.verify_ecomment(channels, "TOOL+CHAMBER_HELD: #{@@siltest.ptool}/#{@@siltest.chambers.first}: reason={X-S")
    # release inhibit
    channels.each {|ch| assert ch.ckc_samples.last.assign_ca('ReleaseChamberInhibit'), 'error assigning corrective action'}
    assert @@siltest.wait_inhibit(:eqp_chamber, @@siltest.ptool, '', n: 0), 'chamber must not have an inhibit'
    # set inhibit (manual)
    assert channels[0].ckc_samples.last.assign_ca('ChamberInhibit'), 'error assigning corrective action'
    assert @@siltest.wait_inhibit(:eqp_chamber, @@siltest.ptool, '', n: @@siltest.chambers.size), 'chamber must have an inhibit'
    assert @@siltest.verify_ecomment(channels, "TOOL+CHAMBER_HELD: #{@@siltest.ptool}/#{@@siltest.chambers.first}: reason={X-S")
    # release inhibit
    assert channels[0].ckc_samples.last.assign_ca('ReleaseChamberInhibit'), 'error assigning corrective action'
    assert @@siltest.wait_inhibit(:eqp_chamber, @@siltest.ptool, '', n: 0), 'chamber must not have an inhibit'
    # set inhibit again (manual)
    assert channels[0].ckc_samples.last.assign_ca('ChamberInhibit'), 'error assigning corrective action'
    assert @@siltest.wait_inhibit(:eqp_chamber, @@siltest.ptool, '', n: @@siltest.chambers.size), 'chamber must have an inhibit'
    assert @@siltest.verify_ecomment(channels, "TOOL+CHAMBER_HELD: #{@@siltest.ptool}/#{@@siltest.chambers.first}: reason={X-S")
    # release inhibit with noDefault attribute
    assert !channels[0].ckc_samples.last.assign_ca('ReleaseChamberInhibit-noDefault'), 'wrongly assigned corrective action'
    assert @@siltest.wait_inhibit(:eqp_chamber, @@siltest.ptool, '', n: @@siltest.chambers.size), 'chamber must still have all inhibits'
  end

  def test12_one_chamber_not_existing
    assert_equal 0, $sv.eqp_cleanup(@@siltest.ptool)
    chambers = @@siltest.chambers + ['CHX']
    assert channels = @@siltest.setup_channels(cas: ['AutoChamberInhibit'], chambers: chambers)
    # send process DCR
    assert @@siltest.submit_process_dcr(chambers: chambers)
    # send meas DCR with OOC values
    assert dcr = @@siltest.submit_meas_dcr(ooc: true, nooc: 'all*2'), 'error submitting DCR'
    assert @@siltest.wait_inhibit(:eqp, @@siltest.ptool, '', n: 1), 'equipment must have an inhibit'
    assert @@siltest.verify_ecomment(channels, "TOOL+CHAMBER_HELD: #{@@siltest.ptool}/#{@@siltest.chambers.first}: reason={X-S")
		assert @@siltest.verify_ecomment(channels, 'Execution of [AutoChamberInhibit] failed: reason={Error in SiView call caused by')
    assert @@siltest.verify_ecomment(channels, "TOOL_HELD: #{@@siltest.ptool}: reason={X-S")
  end

  def test13_some_chambers_not_existing
    assert_equal 0, $sv.eqp_cleanup(@@siltest.ptool)
    chambers = @@siltest.chambers + ['CHX', 'CHY', 'CHZ']
    assert channels = @@siltest.setup_channels(cas: ['AutoChamberInhibit'], chambers: chambers)
    # send process DCR
    assert @@siltest.submit_process_dcr(chambers: chambers)
    # send meas DCR with OOC values
    assert dcr = @@siltest.submit_meas_dcr(ooc: true, nooc: 'all*2'), 'error submitting DCR'
    assert @@siltest.wait_inhibit(:eqp, @@siltest.ptool, '', n: 1), 'equipment must have an inhibit'
    assert @@siltest.wait_inhibit(:eqp_chamber, @@siltest.ptool, '', n: (chambers.count-3)*channels.size), 'chamber must have an inhibit'
    assert @@siltest.verify_ecomment(channels, "TOOL+CHAMBER_HELD: #{@@siltest.ptool}/#{@@siltest.chambers.first}: reason={X-S")
		assert @@siltest.verify_ecomment(channels, "TOOL+CHAMBER_HELD: #{@@siltest.ptool}/#{@@siltest.chambers[1]}: reason={X-S")
		assert @@siltest.verify_ecomment(channels, 'Execution of [AutoChamberInhibit] failed: reason={Error in SiView call caused by')
		assert @@siltest.verify_ecomment(channels, "TOOL_HELD: #{@@siltest.ptool}: reason={X-S")
  end

  def test14_one_not_existing_chamber_and_also_lot_hold
    assert_equal 0, $sv.eqp_cleanup(@@siltest.ptool)
    chambers = @@siltest.chambers + ['CHX']
    assert channels = @@siltest.setup_channels(cas: ['AutoLotHold', 'AutoChamberInhibit'], chambers: chambers)
    # send process DCR
    assert @@siltest.submit_process_dcr(chambers: chambers)
    # send meas DCR with OOC values
    assert dcr = @@siltest.submit_meas_dcr(ooc: true, nooc: 'all*2'), 'error submitting DCR'
    assert @@siltest.wait_futurehold('[(Raw above specification)', n: channels.size), 'missing future hold'
    assert @@siltest.wait_inhibit(:eqp, @@siltest.ptool, '', n: 1), 'chamber must have an inhibit'
    assert @@siltest.verify_ecomment(channels, "LOT_HELD: #{@@siltest.lot}: reason={X-")
		assert @@siltest.verify_ecomment(channels, "TOOL+CHAMBER_HELD: #{@@siltest.ptool}/#{@@siltest.chambers.first}: reason={X-S")
		assert @@siltest.verify_ecomment(channels, "TOOL+CHAMBER_HELD: #{@@siltest.ptool}/#{@@siltest.chambers[1]}: reason={X-S")
		assert @@siltest.verify_ecomment(channels, 'Execution of [AutoChamberInhibit] failed: reason={Error in SiView call caused by')
		assert @@siltest.verify_ecomment(channels, "TOOL_HELD: #{@@siltest.ptool}: reason={X-S")
  end

  def test15_some_chambers_not_existing_and_also_lot_hold
    assert_equal 0, $sv.eqp_cleanup(@@siltest.ptool)
    chambers = @@siltest.chambers + ['CHX', 'CHY', 'CHZ']
    assert channels = @@siltest.setup_channels(cas: ['AutoLotHold', 'AutoChamberInhibit'], chambers: chambers)
    # send process DCR
    assert @@siltest.submit_process_dcr(chambers: chambers)
    # send meas DCR with OOC values
    assert dcr = @@siltest.submit_meas_dcr(ooc: true, nooc: 'all*2'), 'error submitting DCR'
    assert @@siltest.wait_futurehold('[(Raw above specification)', n: channels.size), 'missing future hold'
    assert @@siltest.wait_inhibit(:eqp, @@siltest.ptool, '', n: 1), 'equipment must have an inhibit'
    assert @@siltest.verify_ecomment(channels, "LOT_HELD: #{@@siltest.lot}: reason={X-S")
    assert @@siltest.verify_ecomment(channels, "TOOL+CHAMBER_HELD: #{@@siltest.ptool}/#{@@siltest.chambers.first}: reason={X-S")
  end

  # MSR 546359
  def test16_exclude_list
    assert_equal 0, $sv.eqp_cleanup(@@siltest.ptool)
    chambers = @@siltest.chambers
    # exclude list is as follows: ExcludeChamber=PM1, ExcludeChamber=PM2_Side1, ExcludeChamber=PM2_Side2
    assert channels = @@siltest.setup_channels(cas: ['AutoChamberInhibit-Exclude', 'ReleaseChamberInhibit'])
    # # send process DCR
    assert @@siltest.submit_process_dcr(chambers: chambers)
    # send meas DCR with OOC values
    assert dcr = @@siltest.submit_meas_dcr(ooc: true, nooc: 'all*2'), 'error submitting DCR'
    assert @@siltest.wait_inhibit(:eqp_chamber, @@siltest.ptool, '', n: (chambers.count-1)*channels.size), 'chamber must have an inhibit'
    assert @@siltest.verify_ecomment(channels, "TOOL+CHAMBER_HELD: #{@@siltest.ptool}/#{@@siltest.chambers[1]}: reason={X-S")
    # release inhibit
    channels.each {|ch| ch.ckc_samples.last.assign_ca('ReleaseChamberInhibit')}
    assert @@siltest.wait_inhibit(:eqp_chamber, @@siltest.ptool, '', n: 0), 'chamber must not have an inhibit'
  end

  def test17_chamber_inhibit_parameter_extension_lot
    assert_equal 0, $sv.eqp_cleanup(@@siltest.ptool)
    pa_behaviour = 'cycle_wafer'
    chambers = @@siltest.chambers
    parameters = @@siltest.parameters.collect {|p| p + "_LOT"}
    assert channels = @@siltest.setup_channels(cas: ['ChamberInhibit', 'AutoChamberInhibit', 'ReleaseChamberInhibit'], parameters: parameters, nsamples: parameters.size, ppd: 'QA-MB-FTDEPP-LIS-CAEX.01', mpd: 'QA-MB-FTDEPM-LIS-CAEX.01')
    # send process DCR
    assert @@siltest.submit_process_dcr(chambers: chambers, processing_areas: pa_behaviour, parameters: parameters, ppd: 'QA-MB-FTDEPP-LIS-CAEX.01')
    # send meas DCR with OOC values
    expected_samples = parameters.size * chambers.size
    assert dcr = @@siltest.submit_meas_dcr(ooc: true, nooc: 'all*2', parameters: parameters, nsamples: expected_samples, mpd: 'QA-MB-FTDEPM-LIS-CAEX.01'), 'error submitting DCR'
    assert @@siltest.wait_inhibit(:eqp_chamber, @@siltest.ptool, '', n: chambers.count*channels.size), 'chamber must have an inhibit'
    assert @@siltest.verify_ecomment(channels, "TOOL+CHAMBER_HELD: #{@@siltest.ptool}")
    channels.each {|ch| ch.ckc_samples.each {|s| s.assign_ca('ReleaseChamberInhibit')}}
    assert @@siltest.wait_inhibit(:eqp_chamber, @@siltest.ptool, '', n: 0), 'chamber must not have an inhibit'
    #
    # set inhibit (manual)
    channels[0].ckc_samples.each {|s|
      next unless s.violations.size > 0
      assert s.assign_ca('ChamberInhibit'), 'error assigning corective action'
      chambers = s.ekeys['PChamber'].split(':')
      assert @@siltest.wait_inhibit(:eqp_chamber, @@siltest.ptool, '', n: chambers.count), 'chamber must have an inhibit'
      assert @@siltest.verify_ecomment(channels[0], "TOOL+CHAMBER_HELD: #{@@siltest.ptool}")
      # release inhibit
      assert s.assign_ca('ReleaseChamberInhibit'), 'error assigning corrective action'
      assert @@siltest.wait_inhibit(:eqp_chamber, @@siltest.ptool, '', n: 0), 'chamber must not have an inhibit'
    }
  end
end
