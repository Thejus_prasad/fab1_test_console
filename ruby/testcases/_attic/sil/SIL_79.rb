=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: U. Koerner, 2012-01-11

History:
  2014-02-05 ssteidte, separated Test_Space07, code cleanup
  2017-08-01 cstenzel/sfrieske, refactored all tests
=end

require_relative 'SILTest'


# CAs Alarm limits (old Test_Space_75)   TODO: move to SIL_1x?
class SIL_79 < SILTest
  @@test_defaults = {mpd: 'QA-ALARMLMTS.01'}
  # not in advanced props
  @@autoqual_reporting_inline_enabled = false
  @@autoqual_reporting_setup_enabled = true

  def setup
    super
    @@siltest.mqaqv.delete_msgs(nil)
  end


  def test00_setup
    $setup_ok = false
    #
    ['AutoLotHold', 'ReleaseLotHold'].each {|action| assert SIL::SpaceApi.ca(action), "CA #{action.inspect} not defined"}
    assert @@valuations = SIL::SpaceApi.valuations.select {|v| v.vid >= 45 && v.vid <= 48}
    assert @@valuations.size > 0, 'no eligible valuations '
    #
    assert lot = $svtest.new_lot, 'error creating test lot'
    @@testlots << lot
    assert @@siltest.set_defaults(@@test_defaults.merge(lot: lot)), 'wrong default test data'
    #
    $setup_ok = true
  end

  def test11_inline_ooc
    assert dcr = send_alarmlimit_dcr('32NM', {'RAW_UAL'=>55.0, 'MEAN_VALUE_UAL'=>50.0}, oolimit: true)
    if @@autoqual_reporting_inline_enabled
      assert @@siltest.verify_aqv_message(dcr, 'fail', comment: 'Found 5 OOS/OOC violation(s)'), 'wrong AQV message'
    end
  end

  def test12_testlds_ooc
    assert dcr = send_alarmlimit_dcr('TEST', {'RAW_UAL'=>55.0, 'MEAN_VALUE_UAL'=>50.0}, oolimit: true)
    if @@autoqual_reporting_setup_enabled
      assert @@siltest.verify_aqv_message(dcr, 'fail', comment: 'Found 10 OOS/OOC violation(s)'), 'wrong AQV message'
    end
  end


  def test13_testlds_nooc
    assert dcr = send_alarmlimit_dcr('TEST', {'RAW_UAL'=>55.0, 'MEAN_VALUE_UAL'=>54.0}, oolimit: false)
    sleep 120
    assert_empty $sv.lot_futurehold_list(@@siltest.lot), 'wrong future hold'
    if @@autoqual_reporting_setup_enabled
      assert @@siltest.verify_aqv_message(dcr, 'success', timeout: 60), 'wrong AQV message'
    end
  end

  # aux methods

  def send_alarmlimit_dcr(technology, limits, params={})
    assert channels = @@siltest.setup_channels(technology: technology)
    # set limits and valuations, assign CAs after the controls have been set!
    channels.each {|ch|
      assert ch.set_limits(limits)
      ch.set_valuations(@@valuations)  # returns nil
    }
    assert @@siltest.assign_verify_cas(channels, ['AutoLotHold', 'ReleaseLotHold'])
    # submit process and meas DCRs
    assert @@siltest.submit_process_dcr(technology: technology)
    assert dcr = @@siltest.submit_meas_dcr(technology: technology, readings: {'Wafer1'=>47, 'Wafer2'=>52, 'Wafer3'=>54})
    #
    return dcr if params[:oolimit] == false
    # verify futurehold and ecomment from AutoLotHold
    assert @@siltest.wait_futurehold('QA_PARAM_9', n: @@siltest.parameters.size), 'missing future hold'
    assert @@siltest.verify_ecomment(channels, "LOT_HELD: #{@@siltest.lot}: reason={X-S")
    #
    assert_equal 0, $sv.lot_gatepass(@@siltest.lot), 'SiView GatePass error'
    refute_empty $sv.lot_hold_list(@@siltest.lot, type: 'FutureHold'), 'missing lot hold'
    #
    return dcr
  end

end
