=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: Conny Stenzel, 2012-06-27

History:
  2017-08-01 cstenzel/sfrieske, refactored all tests
=end

require_relative 'SILTest'


# Generic Keys - FIXED, DC, GROUP  was: Test_Space15S
class SIL_51 < SILTest
  @@test_defaults = {mpd: 'QA-MB-FTDEPM.01', mtool: 'QAMeas'}
  @@route_udata = 'C02-1238.01'        # route with UDATA

  @@gekeys = (1..8).collect {|i| "Reserve#{i}"}     # generic extraxtor keys
  @@gdkeys = (1..8).collect {|i| "DatReserve#{i}"}  # generic data keys

  @@keys_fixed = {  # spec C05-00001624
    'QAGKFX1'=>['equipment', 'port', 'PToolChamber', 'PMachineRecipe', 'department', 'dcparmorpath', 'dcparmorpathwithoutversion', 'dcpname'],
    'QAGKFX2'=>['sapdcpid', 'sapordernumber', 'saporderoperation', 'sapinspectioncharacteristicid', 'machinerecipe', 'lotid', 'customer', 'SubLotType'],
    'QAGKFX3'=>['process', 'productgroup', 'product', 'controljob', 'operationpasscount', 'logicalrecipe', 'operationnumber', 'operationname'],
    'QAGKFX4'=>['facility', 'Foup', 'subprocessingarea', 'foup', 'waferid', 'slot', 'operator', 'Event'],
    'QAGKFX5'=>['Tool', 'sublottype', 'POperation', 'PChamberSeq', 'SpecID', 'SpecIDandRev', 'Reticle', 'reticle'],
    'QAGKFX6'=>['mainprocess', 'technology', 'processingarea', 'rmsrecipe', 'photolayer', 'maskedlotid', 'PE10State', 'WaferVendorID'],
    'QAGKFX7'=>['spec_LSL_TARGET_USL', 'spec_TARGET', 'spec_LSL_USL', nil, nil, nil, nil, nil], # New SIL 5.7 keys, already setup in GK Spec
    
    # Keys which can be choosen in spec editor, but have not been used till 2017/08
    # MTime, MTool, MOperation, MSlot, AutoFlagLotType
    # processjob - removed from spec, element was removed from SPACE xml schema
    # contextid  not from DCR, unclear what it is
    # runid      from the SIL response, special treatment needed
  }
  @@limits_spec = 'C05-00001520.52'  # for 'SpecID', 'SpecIDandRev'

  @@keys_dc = {
    'QAGKDC1'=>@@keys_fixed['QAGKFX1']
  }
  # not filled by SIL with DC
  @@keys_dc_empty = ['PMachineRecipe', 'sapdcpid', 'sapordernumber', 'saporderoperation', 'sapinspectioncharacteristicid']

  @@keys_group = {  # spec C05-00001624
    'QAGKGP1'=>['SIL_LOT', 'SIL_WAFER', 'F-P-FTEOS8K15.0x', 'UTCSIL0x', 'QA-MB-xTDEP.0x', 'QA-MB-FTDEPM.0x', 'QAProducts', '-'],
    'QAGKGP2'=>['UTC_Chamber', 'PMx', 'S2', 'xxNM', 'SIL_ROUTE', 'SIL_PRODUCT', 'QA-MB-xTDEP.0x', 'DummyLayer']
  }


  def test00_setup
    $setup_ok = false
    #
    assert lots = @@svtest.new_lots(2), 'error creating lots'
    @@testlots += lots
    @@lot2 = lots[1]
    #
    assert @@siltest.set_defaults(@@test_defaults.merge(lot: lots[0])), 'wrong default test data'
    @@ppds = @@siltest.db.pd_reference(mpd: @@siltest.mpd)
    assert_equal 9, @@ppds.size, "wrong PD references found for #{@@siltest.mpd}: #{@@ppds}"
    # set E10 status for chambers & eqp
    ['2NDP', '2WPR'].each {|s|
      assert_equal 0, @@sv.eqp_status_change(@@siltest.ptool, s)
      assert_equal 0, @@sv.chamber_status_change(@@siltest.ptool, @@siltest.chambers, s)
    }
    $setup_ok = true
  end

  # method FIXED

  def test11_fixed
    @@keys_fixed.keys.each {|department|
      setup
      # submit process DCR, sapjob is for QAGKFX2, route with UDATA for QAGKFX5
      sapjob = {'inspectionCharacteristicID'=>'0010', 'orderOperation'=>'0095', 'SAPdataCollectionPlanID'=>'60012916', 'SAPOrderNumber'=>'67853957'}
      assert dcrp = @@siltest.submit_process_dcr(department: department) {|dcr|
        dcr.context[:JobSetup][:SAPJob] = sapjob
      }
      # sumbit meas DCR
      assert dcr = @@siltest.submit_meas_dcr(department: department), 'error submitting DCR'
      # verify sample generic keys
      assert @@siltest.verify_channels(dcr) {|channel, sample|
        w = sample.ekeys['Wafer']
        gekeys_ref = dcrp.gkeys('Wafer'=>w, mapping: @@gekeys.zip(@@keys_fixed[department]))
        gdkeys_ref = dcrp.gkeys('Wafer'=>w, mapping: @@gdkeys.zip(@@keys_fixed[department]))
        if department == 'QAGKFX5'
          gekeys_ref.merge!('Reserve5'=>@@limits_spec.split('.').first, 'Reserve6'=>@@limits_spec)
          gdkeys_ref.merge!('DatReserve5'=>@@limits_spec.split('.').first, 'DatReserve6'=>@@limits_spec)
        elsif department == 'QAGKFX7'
          gekeys_ref.merge!('Reserve1'=>'25.0_45.0_75.0', 'Reserve2'=>'45.0', 'Reserve3'=>'25.0_75.0')
          gdkeys_ref.merge!('DatReserve1'=>'25.0_45.0_75.0', 'DatReserve2'=>'45.0', 'DatReserve3'=>'25.0_75.0')
        end
        assert verify_hash(gekeys_ref, sample.ekeys, nil, refonly: true), 'wrong generic ekey'
        assert verify_hash(gdkeys_ref, sample.dkeys, nil, refonly: true), 'wrong generic dkey'
      }
    }
  end

  # was: test15812_fixed_specid_pcp
  def testdev12_fixed_specid_pcp
    @@keys_fixed.keys.each {|department|
      setup
      # submit process DCR
      assert dcrp = @@siltest.submit_process_dcr(department: department)
      # sumbit meas DCR
      assert dcr = @@siltest.submit_meas_dcr(department: department)
      # verify sample generic keys
      assert @@siltest.verify_channels(dcr) {|channel, sample|
        assert_equal @@keys_fixed[department][0], sample.ekeys['Reserve1'], 'wrong generic ekey'
        assert_equal @@keys_fixed[department][1], sample.ekeys['Reserve2'], 'wrong generic ekey'
        assert_equal @@keys_fixed[department][0], sample.dkeys['DatReserve1'], 'wrong generic dkey'
        assert_equal @@keys_fixed[department][1], sample.dkeys['DatReserve2'], 'wrong generic dkey'
      }
    }
  end

  # was: test15813_fixed_specid_ecp
  # same as test12 with ECP parameters    # fails with empty SpecID and SpecIDandRev ('-')
  def testdev13_fixed_specid_ecp
    parameters = @@siltest.parameters.collect {|p| "#{p}_ECP"}
    @@keys_fixed.keys.each {|department|
      setup
      # submit process DCR
      assert dcrp = @@siltest.submit_process_dcr(department: department, parameters: parameters)
      # sumbit meas DCR
      assert dcr = @@siltest.submit_meas_dcr(department: department, parameters: parameters)
      # verify sample generic keys
      assert @@siltest.verify_channels(dcr, parameters: parameters, template: @@default_template) {|channel, sample|
        assert_equal @@fixed_specs[department][0], sample.ekeys['Reserve1'], 'wrong generic ekey'
        assert_equal @@fixed_specs[department][1], sample.ekeys['Reserve2'], 'wrong generic ekey'
        assert_equal @@fixed_specs[department][0], sample.dkeys['DatReserve1'], 'wrong generic dkey'
        assert_equal @@fixed_specs[department][1], sample.dkeys['DatReserve2'], 'wrong generic dkey'
      }
    }
  end

  # SIL 5.1 MSR1076957 - Different GK Key contents for setup and inline LDS - inline key only was used before
  def test14_fx_multilot_inline_and_test
    department_inline = 'QAGKFX1'
    department_setup = 'QAGKFX3' # use technology 'Test'
    dptmts = [department_inline, department_setup]
    # submit DCRs for all process PDs
    dcrps = []
    assert dcrps << @@siltest.submit_process_dcr(department: department_inline)
    assert dcrps << @@siltest.submit_process_dcr(lot: @@lot2, department: department_setup, technology: 'TEST')
    # send meas DCR for lot1 OOC and lot2 NOOC
    assert dcr = @@siltest.submit_meas_dcr(ooc: true, nooc: 'all', department: department_inline) {|dcr|
      dcr.add_lot(@@lot2, eqp: @@siltest.mtool, op: @@siltest.mpd, department: department_setup, technology: 'TEST')
      dcr.add_parameters(:default, :default)
    }, "error submitting DCR"
    # verify sample generic keys
    dptmts.each_with_index {|dpt, idx|
      lctx = dcr.lot_context(ilot: idx)
      assert_equal dpt, lctx['department'], 'wrong department in DCR?'
      # calculate expected data from DCR
      w = dcr.lot_context(ilot: idx)[:Wafer].first['id']
      gekeys_ref = dcrps[idx].gkeys(ilot: 0, 'Wafer'=>w, mapping: @@gekeys.zip(@@keys_fixed[dpt]))
      gdkeys_ref = dcrps[idx].gkeys(ilot: 0, 'Wafer'=>w, mapping: @@gdkeys.zip(@@keys_fixed[dpt]))
      # get sample for the chosen wafer
      assert folder = @@lds.folder("AutoCreated_#{dpt}") if idx == 0
      assert folder = @@lds_setup.folder("AutoCreated_#{dpt}") if idx == 1
      assert ch = folder.spc_channel(@@siltest.parameters.first), 'missing channel'
      assert sample = ch.sample('Wafer'=>w), 'no sample'
      # compare
      assert verify_hash(gekeys_ref, sample.ekeys, nil, refonly: true), 'wrong generic ekey'
      assert verify_hash(gdkeys_ref, sample.dkeys, nil, refonly: true), 'wrong generic dkey'
    }
  end

  # method DC

  def test21_dc_inline
    @@keys_dc.keys.each {|department|
      setup
      send_scenario_dc(department)
    }
  end

  def test22_dc_test
    @@keys_dc.keys.each {|department|
      setup
      send_scenario_dc(department, technology: 'TEST')
    }
  end

  # TODO: already covered by test21 (!?)
  def test23_dc_chambers
    send_scenario_dc('QAGKDC1', chambers: 6.times.collect {|i| "PM#{i+1}"})
  end

  def test24_dc_boatslot
    department = 'QAGKDCB'
    readings = []  # boatslot readings
    # submit DCRs for all process PDs
    assert dcrs = @@ppds.each_with_index.collect {|ppd, i|
      lot2 = @@siltest.lot.sub('.00', ".0#{i+1}")
      @@siltest.submit_process_dcr(ppd: ppd, ptool:"#{@@siltest.ptool}_#{i}", department: department) {|dcr|
        r = Hash[dcr.lot_context[:Wafer].each_with_index.collect {|e, widx| [e['id'], "#{i}0#{widx+1}"]}]  # {'Wafer1'=>001, ...}
        readings << r
        dcr.add_parameter("D-BoatSlot#{i}", r, nocharting: true)
      }
    }
    # submit DCR for related MPD, multilot to cover old test1221
    assert dcr = @@siltest.submit_meas_dcr(department: department) {|dcr|
      r = Hash[dcr.lot_context[:Wafer].each_with_index.collect {|e, widx| [e['id'], "90#{widx+1}"]}]  # {'Wafer1'=>901, ...}
      readings << r
      dcr.add_parameter('D-BoatSlotM', r, nocharting: true)
    }
    # verify channels incl. slot readings
    assert @@siltest.verify_channels(dcr) {|channel, sample|
      w = sample.ekeys['Wafer']
      bslots = [0, 1, 2, 3, 9].collect {|resppd| readings[resppd][w]}
      eref = Hash[(4..8).collect {|i| ["Reserve#{i}", bslots[i-4]]}]
      assert verify_hash(eref, sample.ekeys, nil, refonly: true), 'wrong generic ekey'
      dref = Hash[(4..8).collect {|i| ["DatReserve#{i}", bslots[i-4]]}]
      assert verify_hash(dref, sample.dkeys, nil, refonly: true), 'wrong generic dkey'
    }
  end

  def test25_dc_params_wafer_history
    department = 'QAGKDCP1'
    2.times {|n|
      $log.info "-- loop #{n}"
      setup  # clean up channels between loops
      # submit DCRs for all process PDs
      readings = []
      assert dcrs = @@ppds.each_with_index.collect {|ppd, i|
        @@siltest.submit_process_dcr(ppd: ppd, ptool:"#{@@siltest.ptool}_#{i}", department: department, readings: i*100) {|dcr|
          r = Hash[dcr.lot_context[:Wafer].each_with_index.collect {|e, widx| [e['id'], "GK#{n}_#{i}0#{widx+1}"]}]  # {'Wafer1'=>'GK0_001, ...}
          readings << r
          dcr.add_parameter("Q-PARAM_GKDC_#{i}", r, nocharting: true)
        }
      }
      # submit meas DCR for related MPD
      assert dcr = @@siltest.submit_meas_dcr(department: department, readings: 900)
      # verify sample generic keys
      assert @@siltest.verify_channels(dcr) {|channel, sample|
        w = sample.ekeys['Wafer']
        eref = Hash[(4..7).collect {|i| ["Reserve#{i}", readings[i-4][w]]} + ['Reserve8', '-']]
        assert verify_hash(eref, sample.ekeys, nil, refonly: true), 'wrong generic ekey'
        dref = Hash[(4..7).collect {|i| ["DatReserve#{i}", readings[i-4][w]]} + ['DatReserve8', '-']]
        assert verify_hash(dref, sample.dkeys, nil, refonly: true), 'wrong generic dkey'
      }
    }
  end

  # TODO: difference to test25?
  def test26_dc_params_no_wafer_history
    department = 'QAGKDCP1'
    # new lot required?
    2.times {|n|
      $log.info "-- loop #{n}"
      setup  # clean up channels between loops
      # submit DCRs for all process PDs
      readings = []
      assert dcrs = @@ppds.each_with_index.collect {|ppd, i|
        @@siltest.submit_process_dcr(ppd: ppd, ptool:"#{@@siltest.ptool}_#{i}", department: department, readings: i*100) {|dcr|
          r = Hash[dcr.lot_context[:Wafer].each_with_index.collect {|e, widx| [e['id'], "GK#{n}_#{i}#{widx+1}#{e['id']}"]}]  # {'Wafer1'=>'GK0_01Wafer1, ...}
          readings << r
          dcr.add_parameter("Q-PARAM_GKDC_#{i}", r, nocharting: true)
        }
      }
      # submit meas DCR for related MPD
      assert dcr = @@siltest.submit_meas_dcr(department: department, readings: 600)
      # verify sample generic keys
      assert @@siltest.verify_channels(dcr) {|channel, sample|
        w = sample.ekeys['Wafer']
        eref = Hash[(4..7).collect {|i| ["Reserve#{i}", readings[i-4][w]]} + ['Reserve8', '-']]
        assert verify_hash(eref, sample.ekeys, nil, refonly: true), 'wrong generic ekey'
        dref = Hash[(4..7).collect {|i| ["DatReserve#{i}", readings[i-4][w]]} + ['DatReserve8', '-']]
        assert verify_hash(dref, sample.dkeys, nil, refonly: true), 'wrong generic dkey'
      }
    }
  end

  def test28_dc_lotsplit_boatslot
    department = 'QAGKDCB'
    readings = []  # boatslot readings
    # submit DCRs for all process PDs
    assert dcrs = @@ppds.each_with_index.collect {|ppd, i|
      lot2 = @@siltest.lot.sub('.00', ".0#{i+1}")
      @@siltest.submit_process_dcr(ppd: ppd, ptool:"#{@@siltest.ptool}_#{i}",
        department: department, lots: [lot2, @@siltest.lot]) {|dcr|
        r = Hash[dcr.lot_context[:Wafer].each_with_index.collect {|e, widx| [e['id'], "#{i}0#{widx+1}"]}]  # {'Wafer1'=>001, ...}
        readings << r
        dcr.add_parameter("D-BoatSlot#{i}", r, nocharting: true)
      }
    }
    # submit DCR for related MPD, multilot to cover old test1221
    assert dcr = @@siltest.submit_meas_dcr(department: department) {|dcr|
      r = Hash[dcr.lot_context[:Wafer].each_with_index.collect {|e, widx| [e['id'], "90#{widx+1}"]}]  # {'Wafer1'=>901, ...}
      readings << r
      dcr.add_parameter('D-BoatSlotM', r, nocharting: true)
    }
    # verify channels incl. slot readings
    assert @@siltest.verify_channels(dcr) {|channel, sample|
      w = sample.ekeys['Wafer']
      bslots = [0, 1, 2, 3, 9].collect {|resppd| readings[resppd][w]}
      eref = Hash[(4..8).collect {|i| ["Reserve#{i}", bslots[i-4]]}]
      assert verify_hash(eref, sample.ekeys, nil, refonly: true), 'wrong generic ekey'
      dref = Hash[(4..8).collect {|i| ["DatReserve#{i}", bslots[i-4]]}]
      assert verify_hash(dref, sample.dkeys, nil, refonly: true), 'wrong generic dkey'
    }
  end

  # method GROUP

  def test31_group
    # note: according to spec C05-00001624 lot must start with UXY, wafers with 000 which is the current default
    @@keys_group.keys.each {|department|
      setup
      # submit DCRs for all process PDs
      assert dcrs = @@ppds.each_with_index.collect {|ppd, i|
        @@siltest.submit_process_dcr(ppd: ppd, ptool:"#{@@siltest.ptool}_#{i}",
          lrecipe: "F-P-FTEOS8K15.0#{i}", mrecipe: "F-CVD60X-P.F-P-FTEOS8K15.0#{i}", department: department)
      }
      # submit DCR for related MPD
      assert dcr = @@siltest.submit_meas_dcr(department: department)
      # verify channels and sample keys incl generic keys
      keys = @@keys_group[department]
      gekeys_ref = Hash[keys.each_with_index.collect {|k, i| ["Reserve#{i+1}", k]}]
      gdkeys_ref = Hash[keys.each_with_index.collect {|k, i| ["DatReserve#{i+1}", k]}]
      assert @@siltest.verify_channels(dcr) {|channel, sample|
        assert verify_hash(gekeys_ref, sample.ekeys, nil, refonly: true), 'wrong generic ekey'
        assert verify_hash(gdkeys_ref, sample.dkeys, nil, refonly: true), 'wrong generic dkey'
      }
    }
  end

  # to be automated completely
  def testman32_group_nonwip
      setup
      # submit DCRs for all process PDs

      parameters = @@siltest.parameters

      # submit DCR for related MPD
      dcr = SIL::DCR.new(eqp: @@siltest.ptool, event: 'non_wip_dcr')
      dcr.context.delete(:JobSetup)
      dcr.add_parameters(:default, {@@siltest.ptool=>22.0}, readingtype: 'Equipment', valuetype: 'double')
      assert @@siltest.send_dcr_verify(dcr, nsamples: @@siltest.parameters.size), 'error sending DCR'

      $log.info "Check nonwip channels for correct value of PToolChamber key and press enter to continue"
      gets
  end

  # Route UDATA FIXED

  def test41_fixed_routeudata_empty
    route = 'SIL-0001.01'
    assert_nil @@sv.user_data_route(route)['RouteProgramID'], 'wrong route UDATA'
    send_scenario_route('QAGKFXR', route, '-')
  end

  def test42_fixed_routeudata_noroute
    route = 'SIL-XXXX.01'
    assert_empty @@sv.module_list(id: route, level: 'Main'), 'route must not exist'
    send_scenario_route('QAGKFXR', route, '-')
  end

  def test43_fixed_routeudata
    assert programid = @@sv.user_data_route(@@route_udata)['RouteProgramID'], "wrong route UDATA"
    send_scenario_route('QAGKFXR', @@route_udata, programid)
  end

  # Route UDATA GROUP

  def test51_group_routeudata_empty
    route = 'SIL-0001.01'
    assert_nil @@sv.user_data_route(route)['RouteProgramID'], "wrong route UDATA"
    send_scenario_route('QAGKGPR', route, '-')
  end

  def test52_group_routeudata_noroute
    route = 'SIL-XXXX.01'
    assert_empty @@sv.module_list(id: route, level: 'Main'), 'route must not exist'
    send_scenario_route('QAGKGPR', route, '-')
  end

  def test53_group_routeudata
    assert programid = @@sv.user_data_route(@@route_udata)['RouteProgramID'], "missing route UDATA"
    send_scenario_route('QAGKGPR', @@route_udata, "#{programid[0..1]}XX")  # 45L1 -> 45XX
  end


  # aux methods

  def send_scenario_dc(department, params={})
    # submit DCRs for all process PDs
    assert dcrs = @@ppds.each_with_index.collect {|ppd, i|
      @@siltest.submit_process_dcr(ppd: ppd, ptool: "#{@@siltest.ptool}_#{i}", lrecipe: "F-P-FTEOS8K15.0#{i}",
        mrecipe: "F-CVD60X-P.F-P-FTEOS8K15.0#{i}", chambers: params[:chambers], technology: params[:technology], department: department)
    }
    # submit DCR for related MPD
    assert dcr = @@siltest.submit_meas_dcr(technology: params[:technology], department: department), 'error submitting DCR'
    # get generic key values from the resp DCR, as configured in the spec
    gekeys_ref = @@siltest.gkeys_dc(dcrs, @@gekeys.zip(@@keys_dc[department]), empty: @@keys_dc_empty)
    gdkeys_ref = @@siltest.gkeys_dc(dcrs, @@gdkeys.zip(@@keys_dc[department]), empty: @@keys_dc_empty)
    # verify channels and sample keys incl generic keys
    assert @@siltest.verify_channels(dcr) {|channel, sample|
      assert verify_hash(gekeys_ref, sample.ekeys, nil, refonly: true), 'wrong generic ekey'
      assert verify_hash(gdkeys_ref, sample.dkeys, nil, refonly: true), 'wrong generic dkey'
    }
  end

  def send_scenario_route(department, route, programid, params={})
    # submit process DCRs
    assert @@ppds.each_with_index.collect {|ppd, i|
       @@siltest.submit_process_dcr(ppd: ppd, eqp: "#{@@siltest.ptool}_#{i}", route: route, department: department)
    }
    # submit DCR for related MPD
    assert dcr = @@siltest.submit_meas_dcr(route: route, department: department)
    # verify channels and sample keys incl generic keys
    assert @@siltest.verify_channels(dcr) {|channel, sample|
      assert_equal programid, sample.ekeys['Reserve4'], 'wrong generic ekey'
      assert_equal programid, sample.dkeys['DatReserve4'], 'wrong generic dkey'
    }
  end

end
