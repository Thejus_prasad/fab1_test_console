=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: Conny Stenzel, 2012-06-27

History:
  2017-08-01 cstenzel/sfrieske, refactored all tests
=end

require_relative 'SILTest'


# <i>Initially wildcards were not requested as requirement. However, the implementation as it currently is,
# works in the way that an empty cell at the columns technology or route is interpreted as a wildcard. The most specified line wins.
# Empty PDs instead are allowed but result in no spec limits.</i>
# 2015-11-16 Fab 1 with SetupFC: No gaps are allowed in hierarchy on the left - means: fields have to be filled from left to right

# Spec Limits Hierarchy (PCP) - Wildcard tests for PD/Technology/Route   # was: Test_Space061
class SIL_63 < SILTest
  @@test_defaults = {mpd: 'QA-SPEC-HIER-METROLOGY-04.01', ppd: :default,
    route: 'SIL-0002.01', product: 'SILPROD2.01', productgroup: '3261C', technology: '32NM',
    parameters: ['QA_PARAM_950', 'QA_PARAM_951']
  }
  
  @@limit_offset = 200 # 100 for ZTABLESPCLIMITS01, 200 for DCSPEC

  def test00_setup
    $setup_ok = false
    #
    assert lot = $svtest.new_lot, 'error creating test lot'
    @@testlots << lot
    $log.info "using lot #{lot}"
    #
    assert @@siltest.set_defaults(@@test_defaults.merge(lot: lot))
    #
    $setup_ok = true
  end


  # INLINE

  # C05-00001583
  def test11_speclimits_INLINE
    lsl = 100 + 1
    send_dcr_verifylimits([lsl, lsl + 1, lsl + 2], technology: 'xxNM', route: 'SIL-xxx2.01', product: 'SILPRODx.01', productgroup: '32xxC')
  end
  
  # No match, no limits
  def test12_no_speclimits_INLINE
    send_dcr_verifylimits([], technology: 'xxNM', route: 'SIL-xxx2.01', product: 'SILPRODx.01', productgroup: '32xxC', mpd: 'QA-SPEC-HIER-METROLOGY-99.01')
  end
  
  # C05-00001583 - MPC-0266: Test O1 / C05-00001869 - MPC-0266: Test O2 - latest spec STORETIME wins
  def test13_speclimits_INLINE
    lsl = 100 + 4 # C05-00001583 - MPC-0266: Test O1
    #lsl = 200 + 13 # C05-00001869 - MPC-0266: Test O2
    send_dcr_verifylimits([lsl, lsl + 1, lsl + 2], route: 'SIL-xxx2.01', product: 'SILPRODx.01', productgroup: '32xxC')
  end

  # SILUseRouteContext = YES -> C05-00001583 - MPC-0266: Test O4 / NO -> C05-00001869 - MPC-0266: Test O2
  def test14_speclimits_INLINE
    lsl = 100 + 7 # C05-00001583 - MPC-0266: Test O4
    #lsl = 200 + 13 # C05-00001869 - MPC-0266: Test O2
    send_dcr_verifylimits([lsl, lsl + 1, lsl + 2], product: 'SILPRODx.01', productgroup: '32xxC')
  end

  # SILUseRouteContext = YES -> C05-00001583 - MPC-0266: Test O3 / NO -> C05-00001869 - MPC-0266: Test O2
  def test15_speclimits_INLINE
    lsl = 100 + 10 # C05-00001583 - MPC-0266: Test O3
    #lsl = 200 + 13 # C05-00001869 - MPC-0266: Test O2
    send_dcr_verifylimits([lsl, lsl + 1, lsl + 2], product: 'SILPRODx.01')
  end
  
  # SILUseRouteContext = YES -> C05-00001583 - MPC-0266: Test O2 / NO -> C05-00001869 - MPC-0266: Test O2
  def test16a_speclimits_INLINE
    lsl = @@limit_offset + 13
    send_dcr_verifylimits([lsl, lsl + 1, lsl + 2])
  end
  
  # SILUseRouteContext = YES -> C05-00001583 - MPC-0266: Test O2 / NO -> C05-00001869 - MPC-0266: Test O2
  def test16b_speclimits_INLINE
    lsl = @@limit_offset + 13
    send_dcr_verifylimits([lsl, lsl + 1, lsl + 2], route: 'SIL-xxx2.01')
  end
  
  # SILUseRouteContext = YES -> C05-00001583 - MPC-0266: Test O2 / NO -> C05-00001869 - MPC-0266: Test O2
  def test16c_speclimits_INLINE
    lsl = @@limit_offset + 13
    send_dcr_verifylimits([lsl, lsl + 1, lsl + 2], route: 'SIL-xxx2.01', product: 'SILPRODx.01')
  end
  
  # SILUseRouteContext = YES -> C05-00001583 - MPC-0266: Test O2 / NO -> C05-00001869 - MPC-0266: Test O2
  def test16d_speclimits_INLINE
    lsl = @@limit_offset + 13
    send_dcr_verifylimits([lsl, lsl + 1, lsl + 2], route: 'SIL-xxx2.01', product: 'SILPRODx.01', productgroup: '32xxC')
  end
  
  # C05-00001869 - MPC-0266: Test N1
  def test17_speclimits_INLINE
    lsl = 200 + 19
    send_dcr_verifylimits([lsl, lsl + 1, lsl + 2], mpd: 'QA-SPEC-HIER-METROLOGY-05.01', route: 'SIL-xxx2.01', product: 'SILPRODx.01', productgroup: '32xxC')
  end
  
  # C05-00001869 - MPC-0266: Test N2
  def test18_speclimits_INLINE
    lsl = 200 + 22
    send_dcr_verifylimits([lsl, lsl + 1, lsl + 2], mpd: 'QA-SPEC-HIER-METROLOGY-05.01', route: 'SIL-xxx2.01')
  end
  
  # C05-00001869 - MPC-0266: Test N3
  def test19_speclimits_INLINE
    lsl = 200 + 25
    send_dcr_verifylimits([lsl, lsl + 1, lsl + 2], mpd: 'QA-SPEC-HIER-METROLOGY-05.01', route: 'SIL-xxx2.01', product: 'SILPRODx.01')
  end
  
  # C05-00001583 - MPC-0266: Test O5
  def test20_speclimits_INLINE
    lsl = 100 + 51
    send_dcr_verifylimits([lsl, lsl + 1, lsl + 2], mpd: 'e567-PD1.01', route: 'SIL-xxx2.01', product: 'SILPRODx.01', productgroup: '32xxC')
  end
  
  # C05-00001583 - MPC-0266: Test O6
  def test21_speclimits_INLINE
    lsl = 100 + 54
    send_dcr_verifylimits([lsl, lsl + 1, lsl + 2], mpd: 'e567-PD1.01', route: 'e567-R1.01', product: 'SILPRODx.01')
  end

  # C05-00001583 - MPC-0266: Test O7
  def test22_speclimits_INLINE
    lsl = 100 + 57
    send_dcr_verifylimits([lsl, lsl + 1, lsl + 2], mpd: 'e567-PD1.01', route: 'e567-R1.01')
  end

  # C05-00001583 - MPC-0266: Test O8
  def test23_speclimits_INLINE
    lsl = 100 + 60
    send_dcr_verifylimits([lsl, lsl + 1, lsl + 2], mpd: 'RP-PD1.01', route: 'RP-CMP-R3.01')
  end

  # SILUseRouteContext = YES -> C05-00001583 - MPC-0266: Test C1 / NO -> C05-00001869 - MPC-0266: Test C1
  def test24_speclimits_INLINE
    lsl = @@limit_offset + 63
    send_dcr_verifylimits([lsl, lsl + 1, lsl + 2], mpd: 'QA-SPEC-HIER-METROLOGY-06.01', product: 'SILPRODx.01')
  end

  # SILUseRouteContext = YES -> C05-00001583 - MPC-0266: Test C2 / NO -> C05-00001869 - MPC-0266: Test C2
  def test25_speclimits_INLINE
    lsl = @@limit_offset + 66
    send_dcr_verifylimits([lsl, lsl + 1, lsl + 2], mpd: 'QA-SPEC-HIER-METROLOGY-07.01', product: 'SILPRODx.01')
  end
  
  # C05-00001583 - MPC-0266: Test C3 / C05-00001869 - MPC-0266: Test C3 - latest spec STORETIME wins
  def test26_speclimits_INLINE
    lsl = 100 + 69 # C05-00001583 - MPC-0266: Test C3
    #lsl = 200 + 69 # C05-00001869 - MPC-0266: Test C3
    send_dcr_verifylimits([lsl, lsl + 1, lsl + 2], mpd: 'QA-SPEC-HIER-METROLOGY-07.01', route: 'SIL-xxx2.01', product: 'SILPRODx.01', productgroup: '32xxC')
  end

  # SILUseRouteContext = YES -> C05-00001583 - MPC-0266: Test C4 / NO -> C05-00001869 - MPC-0266: Test C4
  def test27_speclimits_INLINE
    lsl = @@limit_offset + 72
    send_dcr_verifylimits([lsl, lsl + 1, lsl + 2], mpd: 'QA-SPEC-HIER-METROLOGY-07.01', technology: '45NM', product: 'SILPRODx.01', productgroup: '3260C')
  end

  # C05-00001583 - MPC-0266: Test C5
  def test28_speclimits_INLINE
    lsl = 100 + 83
    send_dcr_verifylimits([lsl, lsl + 1, lsl + 2], mpd: 'QA-SPEC-HIER-METROLOGY-08.01', product: 'SILPRODx.01')
  end

  # C05-00001583 - MPC-0266: Test C5
  def test28a_speclimits_INLINE
    lsl = 100 + 83
    send_dcr_verifylimits([lsl, lsl + 1, lsl + 2], mpd: 'QA-SPEC-HIER-METROLOGY-08.01', route: 'e567-R1.01', product: 'SILPRODx.01')
  end
  
  # C05-00001869 - MPC-0266: Test N5
  def test29_speclimits_INLINE
    lsl = 200 + 31
    send_dcr_verifylimits([lsl, lsl + 1, lsl + 2], mpd: 'e567-PD2.01', route: 'e567-R1.01', product: 'SILPRODx.01', productgroup: '32xxC')
  end
  
  # C05-00001869 - MPC-0266: Test N6
  def test30_speclimits_INLINE
    lsl = 200 + 34
    send_dcr_verifylimits([lsl, lsl + 1, lsl + 2], mpd: 'e567-PD2.01', route: 'e567-R1.01', product: 'SILPRODx.01', productgroup: '3261C')
  end
  
  # C05-00001869 - MPC-0266: Test N7
  def test31_speclimits_INLINE
    lsl = 200 + 37
    send_dcr_verifylimits([lsl, lsl + 1, lsl + 2], mpd: 'e567-PD2.01', route: 'e567-R1.01', product: 'SILPROD2.01', productgroup: '3261C')
  end
  
  # C05-00001869 - MPC-0266: Test N8
  def test32_speclimits_INLINE
    lsl = 200 + 40
    send_dcr_verifylimits([lsl, lsl + 1, lsl + 2], mpd: 'RP-PD2.01', route: 'SIL-xxx2.01', product: 'SILPROD2.01', productgroup: '3261C')
  end
  
  # C05-00001869 - MPC-0266: Test N9
  def test33_speclimits_INLINE
    lsl = 200 + 43
    send_dcr_verifylimits([lsl, lsl + 1, lsl + 2], mpd: 'e567-PD3.01', route: 'e567-R1.01', product: 'SILPRODx.01', productgroup: '3260C', technology: '45NM')
  end
  
  # C05-00001869 - MPC-0266: Test N10
  def test34_speclimits_INLINE
    lsl = 200 + 46
    send_dcr_verifylimits([lsl, lsl + 1, lsl + 2], mpd: 'e567-PD3.01', route: 'e567-R1.01', product: 'SILPROD2.01', productgroup: '32xxC')
  end
  
  # C05-00001869 - MPC-0266: Test N11
  def test35_speclimits_INLINE
    lsl = 200 + 49
    send_dcr_verifylimits([lsl, lsl + 1, lsl + 2], mpd: 'QA-SPEC-HIER-METROLOGY-08.01', route: 'e567-R1.01', product: 'SILPROD2.01', productgroup: '3261C')
  end

  # SETUP

  def test51_speclimits_TEST
    lsl = 100 + 31
    send_dcr_verifylimits([lsl, lsl + 1, lsl + 2], technology: 'TEST', route: 'SIL-xxx2.01', product: 'SILPRODx.01', productgroup: '32xxC')
  end

  def test52_speclimits_TEST
    lsl = 100 + 34
    send_dcr_verifylimits([lsl, lsl + 1, lsl + 2], technology: 'TEST', product: 'SILPRODx.01', productgroup: '32xxC')
  end

  def test53_speclimits_TEST
    lsl = 100 + 37
    send_dcr_verifylimits([lsl, lsl + 1, lsl + 2], technology: 'TEST', product: 'SILPRODx.01')
  end

  def test54_speclimits_TEST
    lsl = 100 + 80
    send_dcr_verifylimits([lsl, lsl + 1, lsl + 2], technology: 'TEST')
  end


  # aux methods

  def send_dcr_verifylimits(specs, params={})
    # submit and verify DCR
    assert dcr = @@siltest.submit_meas_dcr(params.merge(ooc: true, nooc: specs.empty? ? 0 : 'all*2'))
    # verify spec limits
    unless specs.empty?
      specsref = Hash[['LSL', 'TARGET', 'USL'].zip(specs)]
      lds = dcr.lot_context['technology'] == 'TEST' ? @@lds_setup : @@lds
      assert folder = lds.folder(@@autocreated_qa)
      (params[:parameters] || @@siltest.parameters).each {|p|
        assert ch = folder.spc_channel(p)
        assert s = ch.samples.last
        $log.info "Expected specs: #{specs.inspect}"
        $log.info "Sample specs: #{s.specs.inspect}"
        assert verify_hash(specsref, s.specs, nil, refonly: true)
      }
    end
  end

end
