=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: Conny Stenzel, 2012-06-27

History:
  2017-08-01 cstenzel/sfrieske, refactored all tests
=end

require_relative 'SILTest'


# Referenced PDs  was: Test_Space13
class SIL_43 < SILTest
  @@test_defaults = {mpd: 'QA-MB-FTDEPM.01'}


  def test00_setup
    $setup_ok = false
    #
    assert @@siltest.set_defaults(@@test_defaults)
    @@ppds = @@siltest.db.pd_reference(mpd: @@siltest.mpd)
    assert @@ppds.size > 2, 'not enough related PPDs'
    #
    $setup_ok = true
  end

  def test11_referenced_pd
    lrecipe = 'C-P-PROCESS-RECIPE.01'
    #
    # submit process DCR
    assert @@siltest.submit_process_dcr(lrecipe: lrecipe, processing_areas: [@@siltest.chambers.last]), 'error submitting DCR'
    # submit meas DCR
    assert @@siltest.submit_meas_dcr(processing_areas: [@@siltest.chambers.first]), 'error submitting DCR'
    #
    # verify sample extractor keys
    ekeys = {'POperationID'=>@@siltest.ppd, 'PLogRecipe'=>lrecipe, 'PChamber'=>@@siltest.chambers.last, 'PTool'=>@@siltest.ptool}
    assert folder = @@lds.folder(@@autocreated_qa)
    @@siltest.parameters.each {|p|
      assert ch = folder.spc_channel(p), 'no channel'
      assert s = ch.samples.last, 'no sample'
      assert verify_hash(ekeys, s.ekeys, nil, refonly: true), 'wrong ekeys'
    }
  end

  def test12_lotparam
    # submit process DCR
    assert @@siltest.submit_process_dcr(processing_areas: [@@siltest.chambers.last]), 'error submitting DCR'
    # submit meas DCR
    assert @@siltest.submit_meas_dcr(readings: {@@siltest.lot=>7}, readingtype: 'Lot'), 'error submitting DCR'
    #
    # verify sample extractor keys
    ekeys = {'POperationID'=>@@siltest.ppd, 'PChamber'=>'-', 'PTool'=>@@siltest.ptool}
    assert folder = @@lds.folder(@@autocreated_qa)
    @@siltest.parameters.each {|p|
      assert ch = folder.spc_channel(p), 'no channel'
      assert s = ch.samples.last, 'no sample'
      assert verify_hash(ekeys, s.ekeys, nil, refonly: true), 'wrong ekeys'
    }
  end

  def test13_multiple_ppds
    lrecipes = ['C-P-4710B-MGT-NMETDEPDR.01','C-P-4710B-MGT-NMETDEPDR.02','C-P-4710B-MGT-NMETDEPDR.03']
    #
    # submit process DCRs for 3 referenced PPDs
    @@ppds.take(3).each_with_index {|ppd, i|
      assert @@siltest.submit_process_dcr(ptool: "#{@@siltest.ptool}_#{i}", ppd: ppd, lrecipe: lrecipes[i],
        processing_areas: [@@siltest.chambers[i]]), 'error submitting DCR'
    }
    # submit meas DCR
    assert @@siltest.submit_meas_dcr(processing_areas: [@@siltest.chambers.last]), 'error submitting DCR'
    #
    # verify sample extractor keys
    ekeys = {'POperationID'=>@@ppds[0], 'PChamber'=>@@siltest.chambers[0], 'PTool'=>"#{@@siltest.ptool}_0", 'PLogRecipe'=>lrecipes[0]}
    assert folder = @@lds.folder(@@autocreated_qa)
    @@siltest.parameters.each {|p|
      assert ch = folder.spc_channel(p), 'no channel'
      assert s = ch.samples.last, 'no sample'
      assert verify_hash(ekeys, s.ekeys, nil, refonly: true), 'wrong ekeys'
    }
  end

  def test14_not_configured_mpd
    # submit meas DCR
    assert @@siltest.submit_meas_dcr(mpd: 'QAXX-MB-FTDEPX.99'), 'error submitting DCR'
    #
    # verify sample extractor keys
    ekeys = {'POperationID'=>'-', 'PChamber'=>'-', 'PTool'=>'-'}
    assert folder = @@lds.folder(@@autocreated_qa)
    @@siltest.parameters.each {|p|
      assert ch = folder.spc_channel(p), 'no channel'
      assert s = ch.samples.last, 'no sample'
      assert verify_hash(ekeys, s.ekeys, nil, refonly: true), 'wrong ekeys'
    }
  end

  def test15_mpd_is_ppd
    # submit meas DCR at MPD for process eqp
    dcr = SIL::DCR.new(eqptype: :chamber, chambers: %w(CHA CHB CHC))  # why do chamber names matter? empty PChambers with PM1..
    dcr.add_lot(:default, op: 'QA-SPEC-HIER-METROLOGY-01.01')
    dcr.add_parameters(:default, :default)
    assert @@siltest.send_dcr_verify(dcr), 'error submitting DCR'
    #
    # verify sample extractor keys incl responsible PD keys
    mapping = [['PLogRecipe', 'logicalrecipe'], ['PTool', 'equipment'], ['PChamber', 'processingarea'], ['POperationID', 'process']]
    assert @@siltest.verify_channels(dcr) {|channel, sample|
      assert verify_hash(dcr.gkeys(mapping: mapping), sample.ekeys, nil, refonly: true), 'wrong additional ekeys'
    }
  end

  def test21_referenced_pd_multiple_chambers
    # submit process DCR
    assert @@siltest.submit_process_dcr(processing_areas: @@siltest.chambers.take(2)), 'error submitting DCR'
    # submit meas DCR
    assert @@siltest.submit_meas_dcr(processing_areas: [@@siltest.chambers.last]), 'error submitting DCR'
    #
    # verify sample extractor keys
    ekeys = {'POperationID'=>@@siltest.ppd, 'PChamber'=>@@siltest.chambers.take(2).join(':'), 'PTool'=>@@siltest.ptool}
    assert folder = @@lds.folder(@@autocreated_qa)
    @@siltest.parameters.each {|p|
      assert ch = folder.spc_channel(p), 'no channel'
      assert s = ch.samples.last, 'no sample'
      assert verify_hash(ekeys, s.ekeys, nil, refonly: true), 'wrong ekeys'
    }
  end

  def test22_referenced_pd_single_chamber_and_multiple_chambers
    # test with different chamber combinations
    pchambers = [[@@siltest.chambers.first], @@siltest.chambers, [@@siltest.chambers.first]]
    pchambers.each {|chambers|
      # submit process DCR
      assert @@siltest.submit_process_dcr(processing_areas: chambers), 'error submitting DCR'
      # submit meas DCR
      assert @@siltest.submit_meas_dcr(processing_areas: [@@siltest.chambers.last]), 'error submitting DCR'
      #
      # verify sample extractor keys
      ekeys = {'POperationID'=>@@siltest.ppd, 'PChamber'=>chambers.join(':'), 'PTool'=>@@siltest.ptool}
      assert folder = @@lds.folder(@@autocreated_qa)
      @@siltest.parameters.each {|p|
        assert ch = folder.spc_channel(p), 'no channel'
        assert s = ch.samples.last, 'no sample'
        assert verify_hash(ekeys, s.ekeys, nil, refonly: true), 'wrong ekeys'
      }
    }
  end

  def test23_mpd_is_ppd_single_chamber_and_multiple_chambers
    # test with different chamber combinations
    pchambers = [[@@siltest.chambers.first], @@siltest.chambers, [@@siltest.chambers.first]]
    pchambers.each {|chambers|
      # submit meas DCR at MPD for process eqp
      dcr = SIL::DCR.new(eqp: @@siltest.ptool, eqptype: :chamber, chambers: @@siltest.chambers)
      dcr.add_lot(:default, op: @@siltest.ppd)
      dcr.add_parameters(:default, :default, processing_areas: chambers)
      assert @@siltest.send_dcr_verify(dcr), 'error submitting DCR'
      #
      # verify sample extractor keys
      ekeys = {'POperationID'=>@@siltest.ppd, 'PChamber'=>chambers.join(':'), 'PTool'=>@@siltest.ptool}
      assert folder = @@lds.folder(@@autocreated_qa)
      @@siltest.parameters.each {|p|
        assert ch = folder.spc_channel(p), 'no channel'
        assert s = ch.samples.last, 'no sample'
        assert verify_hash(ekeys, s.ekeys, nil, refonly: true), 'wrong ekeys'
      }
    }

  end

  def test31_mpd_is_ppd_multiple_dcrs
    count = 10
    p = @@siltest.parameters.first
    readings = {'2502J493KO'=>41}
    count.times {|i|
      readings.each {|k, v| readings[k] = v + 1}
      # submit meas DCR at MPD for process eqp
      dcr = SIL::DCR.new(eqptype: :chamber)
      dcr.add_lot(:default, op: 'QA-SPEC-HIER-METROLOGY-01.01')
      dcr.add_parameter(p, readings)
      assert @@siltest.send_dcr_verify(dcr), 'error submitting DCR'
    }
    # verify last value
    assert folder = @@lds.folder(@@autocreated_qa)
    assert ch = folder.spc_channel(p)
    samples = ch.samples
    assert_equal count, samples.size, "wrong number of samples in #{ch.inspect}"
    assert_equal samples.last.raws.first.value, readings.values.first, 'value not the last sended'
  end

  def test32_referenced_pd
    count = 10
    p = @@siltest.parameters.first
    eqps = 7.times.collect {|i| "#{@@siltest.ptool}_#{i}"}
    readings = {'2502J493KO'=>41}
    count.times {|i|
      readings.each {|k, v| readings[k] = v + 1}
      ptool = eqps.sample
      chamber = @@siltest.chambers.sample
      # submit process DCR with random eqp and chamber
      assert @@siltest.submit_process_dcr(ptool: ptool, processing_areas: [chamber], parameters: [p], readings: readings), 'error submitting DCR'
      # submit meas DCR
      assert @@siltest.submit_meas_dcr(parameters: [p], readings: readings, processing_areas: [@@siltest.chambers.first]), 'error submitting DCR'
      #
      # verify sample extractor keys
      assert folder = @@lds.folder(@@autocreated_qa)
      assert ch = folder.spc_channel(p), 'no channel'
      assert_equal i + 1, ch.samples.size, "wrong number of samples in #{ch.inspect}"
      s = ch.samples.last
      assert_equal s.raws.first.value, readings.values.first, 'value not the last sended'
      ekeys = {'POperationID'=>@@siltest.ppd, 'PTool'=>ptool, 'PChamber'=>@@siltest.chambers.first}
      assert verify_hash(ekeys, s.ekeys, nil, refonly: true), 'wrong ekeys'
      sleep 3
    }
  end

  # See MSR542002
  # was: test1330_referenced_pd_responsiblepd2to5
  def test33_referenced_pd_responsiblepd2to5
    p = 'QA_PARAM_901'
    mpd = 'QA-999999.01'
    ppds = ['QA-999999.03', 'QA-999999.04', 'QA-999999.05', 'QA-999999.06']
    ekeys = {'PTool'=>'-', 'POperationID'=>'-', 'PChamber'=>'-'}
    folder = @@lds.folder(@@autocreated_qa)
    ppds.each {|ppd|
      $log.info "-- using PPD #{ppd}"
      assert folder.delete_channels
      # submit process DCR
      assert @@siltest.submit_process_dcr(ppd: ppd, processing_areas: [@@siltest.chambers.first], parameters: [p]), 'error submitting DCR'
      # submit meas DCR
      assert @@siltest.submit_meas_dcr(mpd: mpd, processing_areas: [@@siltest.chambers.last], parameters: [p]), 'error submitting DCR'
      #
      # verify sample extractor keys
      assert folder = @@lds.folder(@@autocreated_qa)
      assert ch = folder.spc_channel(p), 'no channel'
      assert s = ch.samples.last, 'no sample'
      assert verify_hash(ekeys, s.ekeys, nil, refonly: true), 'wrong ekeys'
    }
  end

  def test34_referenced_pd_multiple_resppd1
    p = 'QA_PARAM_901'
    mpd = 'QA-MULT-PD-MB-FTDEPM.01'
    # get referenced PPDs
    ppds = @@siltest.db.pd_reference(mpd: mpd)
    refute_empty ppds, "no PD reference found for #{mpd}"
    folder = @@lds.folder(@@autocreated_qa)
    ppds.each_with_index {|ppd, i|
      $log.info "using PPD #{ppd}"
      eqp = "#{@@siltest.ptool}_#{i}"
      cha = @@siltest.chambers[i]
      assert folder.delete_channels
      # submit process DCR
      assert @@siltest.submit_process_dcr(ptool: eqp, ppd: ppd, processing_areas: [cha], parameters: [p]), 'error submitting DCR'
      # submit meas DCR
      assert @@siltest.submit_meas_dcr(mpd: mpd, processing_areas: [], parameters: [p]), 'error submitting DCR'
      #
      # verify sample extractor keys
      assert folder = @@lds.folder(@@autocreated_qa)
      assert ch = folder.spc_channel(p), 'no channel'
      assert s = ch.samples.last, 'no sample'
      assert verify_hash({'PTool'=>eqp, 'POperationID'=>ppd, 'PChamber'=>cha}, s.ekeys, nil, refonly: true), 'wrong ekeys'
    }
  end

  def test35_multiple_referenced_pd_multiple_resppd1
    p = 'QA_PARAM_901'
    mpds = ['QA-MULT-PD-MB-FTDEPM.01','QA-MULT-PD-MB-FTDEPM.02']
    # get PPDs referenced by MPDs
    ppds = []
    mpds.each {|mpd|
      res = @@siltest.db.pd_reference(mpd: mpd, all: true)
      refute_empty res, "no PD reference found for #{mpd}"
      refute_empty res.first.pdrefs, "no PD reference found for #{mpd}"
      ppds += res.first.pdrefs.flatten
    }
    ppds.uniq!
    ppds.sort!
    #
    folder = @@lds.folder(@@autocreated_qa)
    mpds.each {|mpd|
      ppds.each_with_index {|ppd, i|
        $log.info "using PPD #{ppd}, MPD #{mpd}"
        eqp = "#{@@siltest.ptool}_#{i}"
        cha = @@siltest.chambers[i]
        assert folder.delete_channels
        # submit process DCR
        assert @@siltest.submit_process_dcr(ptool: eqp, ppd: ppd, processing_areas: [cha], parameters: [p]), 'error submitting DCR'
        # submit meas DCR
        assert @@siltest.submit_meas_dcr(mpd: mpd, processing_areas: [], parameters: [p]), 'error submitting DCR'
        #
        # verify sample extractor keys
        assert folder = @@lds.folder(@@autocreated_qa)
        assert ch = folder.spc_channel(p), 'no channel'
        assert s = ch.samples.last, 'no sample'
        assert verify_hash({'PTool'=>eqp, 'POperationID'=>ppd, 'PChamber'=>cha}, s.ekeys, nil, refonly: true), 'wrong ekeys'
      }
    }
  end

  #See use cases for MSR517251
  def test36_referenced_pd_parameter
    mpd = 'QA-PARAM-FTDEPM.01'
    # get PPDs referenced by MPD
    assert pd_setup = @@siltest.db.pd_reference(mpd: mpd, all: true)
    refute_empty pd_setup, "no PD reference found for #{mpd}"
    refute_empty pd_setup.first.pdrefs, "no PD reference found for #{mpd}"
    # submit process DCRs
    folder = @@lds.folder(@@autocreated_qa)
    pd_setup.each_with_index {|pdref, i|
      eqp = "#{@@siltest.ptool}_#{i}"
      cha = @@siltest.chambers[i % @@siltest.chambers.size]
      ppd = pdref.pdrefs.flatten.first
      parameter = pdref.parameter || 'QA_PARAM_904'
      assert folder.delete_channels
      # submit process DCR
      assert @@siltest.submit_process_dcr(ptool: eqp, ppd: ppd, processing_areas: [cha], parameters: ['Dummy']), 'error submitting DCR'
    }
    # submit meas DCR
    assert dcr = @@siltest.submit_meas_dcr(mpd: mpd, processing_areas: []), 'error submitting DCR'
    #
    # verify sample extractor and data keys
    assert folder = @@lds.folder(@@autocreated_qa)
    @@siltest.parameters.each {|p|
      assert ch = folder.spc_channel(p), 'no channel'
      assert samples = ch.samples, 'no samples'
      assert_equal dcr.readings(p).size, samples.size, 'wrong number of channels'
      s = samples.last
      s_param = (p == 'QA_PARAM_904') ? nil : p
      ppd_i = pd_setup.find_index {|pdref| pdref.parameter == s_param}
      ppd = pd_setup[ppd_i].pdrefs.flatten.first
      $log.info "verifying PPD #{ppd}"
      cha = @@siltest.chambers[ppd_i % @@siltest.chambers.size]
      assert verify_hash({'PTool'=>"#{@@siltest.ptool}_#{ppd_i}", 'POperationID'=>ppd, 'PChamber'=>cha}, s.ekeys, nil, refonly: true), 'wrong ekeys'
      assert verify_hash({'PSequence'=>"#{cha}:#{cha}_SIDE1:#{cha}_SIDE2"}, s.dkeys, nil, refonly: true), 'wrong dkeys'
    }
  end

end
