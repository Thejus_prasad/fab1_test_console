=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: Conny Stenzel, 2012-06-27

History:
  2017-08-01 cstenzel/sfrieske, refactored all tests
=end

require_relative 'SILTest'


# Remeasurement  was: Test_Space14 part 1
class SIL_44 < SILTest
  @@test_defaults = {mpd: 'QA-MB-FTDEPM.01', mtool: 'QAMeas'}
  @@flagging_delay = 20
  @@inline_remeasurement_autoflagging_enabled = true
  @@setup_remeasurement_autoflagging_enabled = true


  def test00_setup
    $setup_ok = false
    #
    assert @@siltest.server.verify_property(:'inline.remeasurement.autoflagging.enabled', @@inline_remeasurement_autoflagging_enabled), 'wrong property'
    assert @@siltest.server.verify_property(:'setup.remeasurement.autoflagging.enabled', @@setup_remeasurement_autoflagging_enabled), 'wrong property'
    #
    assert @@siltest.set_defaults(@@test_defaults)
    #
    $setup_ok = true
  end

  def test11_remeasure
    assert @@siltest.submit_process_dcr, 'error submitting DCR'
    3.times {|i| remeasure_scenario(parameters: @@siltest.parameters.collect {|p| p + '_REMEASURE'})}
  end

  def test12_2wafers
    assert dcr = @@siltest.submit_process_dcr, 'error submitting DCR'
    readings = dcr.readings(@@siltest.parameters.first, all: true)
    #
    remeasure_scenario
    remeasure_scenario(readings: Hash[readings.to_a[0..1]])
    remeasure_scenario(readings: Hash[readings.to_a[0..1]], mtool: 'QAMeas2')
    remeasure_scenario(readings: Hash[readings.to_a[1..2]])
  end

  def test13_multilot
    lots = ['SILQA01.00', 'SILQA02.00']
    assert dcr = @@siltest.submit_process_dcr(lots: lots), 'error submitting DCR'
    #
    remeasure_scenario(lot: 'SILQA02.00')
    remeasure_scenario(lot: 'SILQA02.00')
    remeasure_scenario(lot: 'SILQA01.00')
  end

  def test14_referenced_pd
    remeasure_scenario_refpd
  end

  def test15_internalflag_and_remeasure
    skip 'inline.remeasurement.autoflagging.enabled=false' unless @@inline_remeasurement_autoflagging_enabled
    assert dcr = @@siltest.submit_process_dcr, 'error submitting DCR'
    #
    3.times {|i| remeasure_scenario(internalflag: true)}
  end

  def test16_lotlevel_and_remeasure
    assert @@siltest.submit_process_dcr, 'error submitting DCR'
    3.times {|i| remeasure_scenario(parameters: @@siltest.parameters.collect {|p| p + '_REMEASURE-LOT'}, nsamples: 5)}
  end

  def test21_remeasure_TEST
    assert @@siltest.submit_process_dcr(technology: 'TEST'), 'error submitting DCR'
    3.times {|i| remeasure_scenario(technology: 'TEST', parameters: @@siltest.parameters.collect {|p| p + '_REMEASURE'})}
  end

  def test22_2wafers_TEST
    assert dcr = @@siltest.submit_process_dcr(technology: 'TEST'), 'error submitting DCR'
    readings = dcr.readings(@@siltest.parameters.first, all: true)
    #
    remeasure_scenario(technology: 'TEST')
    remeasure_scenario(technology: 'TEST', readings: Hash[readings.to_a[0..1]])
    remeasure_scenario(technology: 'TEST', readings: Hash[readings.to_a[0..1]], mtool: 'QAMeas2')
    remeasure_scenario(technology: 'TEST', readings: Hash[readings.to_a[1..2]])
  end

  def test23_multilot_TEST
    lots = ['SILQA01.00', 'SILQA02.00']
    assert dcr = @@siltest.submit_process_dcr(technology: 'TEST', lots: lots), 'error submitting DCR'
    #
    remeasure_scenario(technology: 'TEST', lot: 'SILQA02.00')
    remeasure_scenario(technology: 'TEST', lot: 'SILQA02.00')
    remeasure_scenario(technology: 'TEST', lot: 'SILQA01.00')
  end

  def test24_referenced_pd_TEST
    remeasure_scenario_refpd(technology: 'TEST')
  end

  def test25_internalflag_and_remeasure_TEST
    ## wrong: skip 'inline.remeasurement.autoflagging.enabled=false' unless @@inline_remeasurement_autoflagging_enabled
    skip 'inline.remeasurement.autoflagging.enabled=false' unless @@setup_remeasurement_autoflagging_enabled
    assert dcr = @@siltest.submit_process_dcr(technology: 'TEST'), 'error submitting DCR'
    #
    3.times {|i| remeasure_scenario(technology: 'TEST', internalflag: true)}
  end

  def test26_nonwip_no_remeasure
    assert @@siltest.submit_process_dcr, 'error submitting DCR'
    3.times {|i| remeasure_scenario_nonwip}
  end

  # aux methods

  def remeasure_scenario(params={})
    technology = params[:technology] || '32NM'
    lds = technology == 'TEST' ? @@lds_setup : @@lds
    parameters = params[:parameters] || @@siltest.parameters
    old_nsamples = {}
    assert folder = lds.folder(@@autocreated_qa)
    parameters.each {|p|
      ch = folder.spc_channel(p)
      old_nsamples[p] = ch ? ch.samples.size : 0
      ch.samples.each {|s| s.flag} if params[:internalflag] && ch
    }
    # submit meas DCR
    assert dcr = @@siltest.submit_meas_dcr(mtool: params[:mtool], mpd: params[:mpd], lot: params[:lot], technology: technology,
      parameters: params[:parameters], readings: params[:readings], nsamples: params[:nsamples]), 'error submitting DCR'
    # wait for flagging and check flags
    $log.info "waiting #{@@flagging_delay} s for flagging"; sleep @@flagging_delay
    assert folder = lds.folder(@@autocreated_qa)
    parameters.each {|p|
      assert ch = folder.spc_channel(p), 'no channel'
      samples = ch.samples
      dcrreadingssize = parameters.first.include?('-LOT') ? 1 : dcr.readings(p).size
      assert_equal old_nsamples[p] + dcrreadingssize, samples.size, "wrong number of samples in #{ch.inspect}"
      if technology != 'TEST' && @@inline_remeasurement_autoflagging_enabled || technology == 'TEST' &&  @@setup_remeasurement_autoflagging_enabled
        assert verify_channel_remeasured(ch), "samples not autoflagged for remeasurement - flag delay = #{@@flagging_delay}"
      else
        if old_nsamples[p] > 0
          samples[0..(old_nsamples[p]-1)].each {|s|
            assert !s.iflag || !s.icomment || !s.icomment.include?('Remeasurement'), 'wrong autoflagging for remeasurement'
          }
        end
      end
    }
  end

  def remeasure_scenario_refpd(params={})
    technology = params[:technology] || '32NM'
    lds = technology == 'TEST' ? @@lds_setup : @@lds
    mdcrcount = 5
    mdcrcount.times {|i|
      $log.info "\n-- loop ##{i+1}/#{mdcrcount}"
      # submit process DCR
      assert @@siltest.submit_process_dcr(technology: technology) if [0, 3].include?(i)
      # count old samples
      old_nsamples = {}
      assert folder = lds.folder(@@autocreated_qa)
      @@siltest.parameters.each {|p|
        ch = folder.spc_channel(p)
        old_nsamples[p] = ch ? ch.samples.size : 0
      }
      # submit meas DCR
      assert dcr = @@siltest.submit_meas_dcr(technology: technology), 'error submitting DCR'
      # wait for flagging and check flags
      $log.info "waiting #{@@flagging_delay} s for flagging"; sleep @@flagging_delay
      assert folder = lds.folder(@@autocreated_qa)
      @@siltest.parameters.each {|p|
        assert ch = folder.spc_channel(p), 'no channel'
        samples = ch.samples
        assert_equal (i + 1) * dcr.readings(p).size, samples.size, "wrong number of samples in #{ch.inspect}"
        if @@setup_remeasurement_autoflagging_enabled
          assert verify_channel_remeasured(ch), "samples not autoflagged for remeasurement - flag delay = #{@@flagging_delay}"
        else
          if old_nsamples[p] > 0
            samples[0..(old_nsamples[p]-1)].each {|s|
              assert !s.iflag || !s.icomment || !s.icomment.include?('Remeasurement'), 'wrong autoflagging for remeasurement'
            }
          end
        end
      }
    }
  end

  def remeasure_scenario_nonwip(params={})
    technology = params[:technology] || '32NM'
    lds = @@lds_setup
    parameters = params[:parameters] || @@siltest.parameters
    assert folder = lds.folder(@@autocreated_nonwip)
    # submit nonwip DCR
    eqp = 'QATest1'
    dcr = SIL::DCR.new(eqp: eqp, event: 'non_wip_dcr')
    dcr.context.delete(:JobSetup) if !params[:mixed]
    dcr.add_parameters(:default, {eqp=>22.0}, readingtype: 'Equipment', valuetype: 'double')
    assert @@siltest.send_dcr_verify(dcr, nsamples: @@siltest.parameters.size), 'error sending DCR'
    
    # wait for flagging and check flags
    $log.info "waiting #{@@flagging_delay} s for flagging"; sleep @@flagging_delay
    assert folder = lds.folder(@@autocreated_nonwip)
    parameters.each {|p|
      assert ch = folder.spc_channel(p), 'no channel'
      samples = ch.samples
      samples.each {|s| 
        assert !s.iflag || !s.icomment || !s.icomment.include?('Remeasurement'), 'wrong autoflagging for remeasurement'
        assert !s.iflag || !s.icomment || !s.icomment.include?('Rework'), 'wrong autoflagging for rework'
      }
    }
  end

  def verify_channel_remeasured(ch)
    rhash = {}
    ch.samples.each {|s|
      ekeys = s.ekeys
      rkey = [s.time.to_i, ekeys['PTool'], ekeys['POperationID'], ekeys['Lot'], ekeys['Wafer']].join(':')
      rhash[rkey] ||= []
      rhash[rkey] << s
    }
    ret = true
    rhash.each_pair {|rkey, samples|
      samples.sort! {|a, b| a.dkeys['MTime'] <=> b.dkeys['MTime']}.each_with_index {|s, i|
        if i < samples.size - 1
          ret &= s.iflag && s.icomment && s.icomment.include?('Remeasurement')
        else  # last sample
          ret &= !s.iflag || !s.icomment || !s.icomment.include?('Remeasurement')
        end
      }
    }
    return ret
  end

end
