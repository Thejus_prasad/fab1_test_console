=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: Steffen Steidten, 2011-11-25

History:
  2017-08-01 cstenzel/sfrieske, refactored all tests
=end

require_relative 'SILTest'
require 'statistics'


# SIL readingValid and parameterValid
class SIL_02 < SILTest
  @@autoqual_reporting_setup_enabled = true  # not in advanced props

  def test00_setup
    $setup_ok = false
    #
    ##@@siltest.set_defaults(@@test_defaults)
    #
    $setup_ok = true
  end

  def test10_smoke
    $setup_ok = false
    #
    assert folder = @@lds.folder(@@templates_folder), 'missing templates folder'
    assert templ = folder.spc_channel(name: @@params_template), 'missing template'
    #
    # build and submit DCR
    dcr = SIL::DCR.new(eqp: @@siltest.ptool)
    dcr.add_lot(:default)
    dcr.add_parameters(:default, :default)
    assert @@siltest.send_dcr_verify(dcr), 'error submitting DCR'
    #
    # verify spc channels
    assert folder = @@lds.folder(@@autocreated_qa), 'missing folder'
    lotctx = dcr.lot_context
    @@siltest.parameters.each {|p|
      assert ch = folder.spc_channel(p), 'missing channel'
      assert_equal 1, @@siltest.db.nspeclimits(p, pd: lotctx['operationID'], technology: lotctx['technology'],
        route: lotctx['route'], product: lotctx['productID'], productgroup: lotctx['productGroupID']), 'wrong limit definition'
    }
    assert @@siltest.verify_channels(dcr) {|channel, sample|
      assert_equal 'WAFER', channel.unit, 'wrong channel unit'
      assert verify_hash({'LSL'=>25, 'TARGET'=>45, 'USL'=>75}, sample.specs, nil), 'wrong spec limits'
      ['MEAN_VALUE_LCL', 'MEAN_VALUE_CENTER', 'MEAN_VALUE_UCL'].each {|l| refute_nil sample.limits[l], 'wrong limit'}
    }
    #
    $setup_ok = true
  end

  def test11_invalid_wafer_readings
    parameters = @@siltest.parameters
    # build DCR
    dcr = SIL::DCR.new
    dcr.add_lot(:default)
    # readings invalid
    dcr.add_parameter(parameters[0], :default, reading_valid: false)
    # parameter invalid
    dcr.add_parameter(parameters[1], :default, parameter_valid: false)
    # some readings invalid
    dcr.add_parameter(parameters[2], :default)
    wvalid = dcr.parameters.first[:Reading].first['Wafer']
    dcr.add_readings({'INVALIDWAFER'=>222, wvalid=>443}, reading_valid: false)
    # send DCR
    assert @@siltest.send_dcr_verify(dcr), 'error submitting DCR'
    #
    # verify no channels for parameters 0 and 1
    folder = @@lds.folder(@@autocreated_qa)
    parameters[0..1].each {|p| assert_nil folder.spc_channel(p), "wrong channel for #{p}"}
    # verify data for valid readings of parameter 2, passes as of 4.7.3
    assert ch = folder.spc_channel(parameters[2])
    assert_equal SIL::DCR::DefaultWaferValues.size, ch.samples.size, 'wrong number of samples'
    assert_equal "Auto-created using template: #{@@params_template}", ch.desc, 'wrong channel description'
    assert s = ch.sample('Wafer'=>wvalid)  # samples are not ordered by wafer
    assert verify_hash(dcr.ekeys('Wafer'=>wvalid), s.ekeys, nil, refonly: true), 'wrong ekey'
    assert verify_hash(dcr.dkeys('Wafer'=>wvalid, parameter: s.parameter), s.dkeys, nil, refonly: true, timediff: 1), 'wrong dkey'
  end

  def test12_invalid_lot_readings
    parameters = @@siltest.parameters
    readings = {SIL::DCR::DefaultLot=>22.0}
    # build DCR
    dcr = SIL::DCR.new
    dcr.add_lot(:default)
    # readings invalid
    dcr.add_parameter(parameters[0], readings, readingtype: 'Lot', reading_valid: false)
    # parameter invalid
    dcr.add_parameter(parameters[1], readings, readingtype: 'Lot', parameter_valid: false)
    # some readings invalid
    dcr.add_parameter(parameters[2], readings, readingtype: 'Lot')
    dcr.add_readings({'INVALIDLOT'=>222, SIL::DCR::DefaultLot=>443}, readingtype: 'Lot', reading_valid: false)
    # send DCR
    assert @@siltest.send_dcr_verify(dcr), 'error submitting DCR'
    #
    # verify no channels for parameters 0 and 1, 1 channel for parameter 2
    folder = @@lds.folder(@@autocreated_qa)
    parameters[0..1].each {|p| assert_nil folder.spc_channel(p), "wrong channel for #{p}"}
    assert ch = folder.spc_channel(parameters[2])
    assert_equal 1, ch.samples.size, 'wrong number of samples'
  end

  def test21_csv_routes_speclimits
    # build and submit DCR
    dcr = SIL::DCR.new
    dcr.add_lot(:default, route: 'SIL-0002.01')
    dcr.add_parameters(:default, :default)
    assert @@siltest.send_dcr_verify(dcr), 'error submitting DCR'
    #
    # verify spec and sample limits are set
    assert folder = @@lds.folder(@@autocreated_qa)
    @@siltest.parameters.each {|p|
      assert ch = folder.spc_channel(p)
      ch.samples.each {|s, i|
        ['USL', 'TARGET', 'LSL'].each {|l| refute_nil s.specs[l], "no #{l} for parameter #{p}"}
        ['MEAN_VALUE_UCL', 'MEAN_VALUE_CENTER', 'MEAN_VALUE_LCL'].each {|l| refute_nil s.limits[l], "no #{l} for parameter #{p}"}
      }
    }
  end

  def test22_unconfigured_route
    skip "not applicable in #{$env}" unless ['itdc', 'let'].member?($env)
    # build and submit DCR
    dcr = SIL::DCR.new
    dcr.add_lot(:default, route: 'SIL-0003.01')
    dcr.add_parameters(:default, :default)
    assert @@siltest.send_dcr_verify(dcr), 'error submitting DCR'
    #
    # verify spec and sample limits are not set
    assert folder = @@lds.folder(@@autocreated_qa)
    @@siltest.parameters.each {|p|
      assert ch = folder.spc_channel(p)
      ch.samples.each {|s, i|
        ['USL', 'TARGET', 'LSL'].each {|l| assert_nil s.specs[l], "no #{l} for parameter #{p}"}
        ['MEAN_VALUE_UCL', 'MEAN_VALUE_CENTER', 'MEAN_VALUE_LCL'].each {|l| assert_nil s.limits[l], "no #{l} for parameter #{p}"}
      }
    }
  end

  def test31_parameter_extension_minus
    p = 'QA_PARAM_900'
    ctrl = {'MEAN'=>41, 'STDDEV'=>1, 'MIN'=>31, 'MAX'=>55, 'COUNT'=>5} # for MSR873848/JIRA MTQA-2272: 'STDDEV'=>999999
    # build and submit DCR
    dcr = SIL::DCR.new
    dcr.add_lot(:default)
    ctrl.each_pair {|k, v| dcr.add_parameter("#{p}-#{k}", {'2502J493KO'=>v})}
    assert @@siltest.send_dcr_verify(dcr, nsamples: 1), 'error submitting DCR'
    # verify sample statistics
    folder = @@lds.folder(@@autocreated_qa)
    assert ch = folder.spc_channel(p)
    assert s = ch.samples.last
    assert verify_hash(ctrl, s.statistics, nil, refonly: true), 'wrong sample statistics'
    assert verify_hash({'LSL'=>25.0, 'TARGET'=>45.0, 'USL'=>75.0}, s.specs, nil, refonly: true), 'wrong sample speclimits'
  end

  def test32_parameter_extension_minus_mean_only
    p = 'QA_PARAM_901'
    v = 41
    # build and submit DCR
    dcr = SIL::DCR.new
    dcr.add_lot(:default)
    dcr.add_parameter("#{p}-MEAN", {'2502J493KO'=>v})
    assert @@siltest.send_dcr_verify(dcr, nsamples: 1), 'error submitting DCR'
    #
    folder = @@lds.folder(@@autocreated_qa)
    assert ch = folder.spc_channel(p)
    assert s = ch.samples.last
    assert verify_hash({'MEAN'=>v, 'STDDEV'=>0, 'MIN'=>v, 'MAX'=>v, 'COUNT'=>1}, s.statistics, nil, refonly: true), 'wrong sample statistics'
    assert verify_hash({'LSL'=>25.0, 'TARGET'=>45.0, 'USL'=>75.0}, s.specs, nil, refonly: true), 'wrong sample speclimits'
  end

  def test33_parameter_extension_minus_setup
    p = 'QA_PARAM_0207b_ECP'
    ctrl = {'MEAN'=>41, 'STDDEV'=>1, 'MIN'=>31, 'MAX'=>55, 'COUNT'=>5}
    # build and submit DCR
    dcr = SIL::DCR.new
    dcr.add_lot(:default, route: 'SIL-0207b.01', technology: 'TEST')
    ctrl.each {|k, v| dcr.add_parameter("#{p}-#{k}", {'2502J493KO'=>v})}
    assert @@siltest.send_dcr_verify(dcr, nsamples: 1), 'error submitting DCR'
    #
    folder = @@lds_setup.folder(@@autocreated_qa)
    assert ch = folder.spc_channel(p)
    assert s = ch.samples.last
    assert verify_hash(ctrl, s.statistics, nil, refonly: true), 'wrong sample statistics'
    assert verify_hash({'LSL'=>25.0, 'TARGET'=>45.0, 'USL'=>75.0}, s.specs, nil, refonly: true), 'wrong sample speclimits'
    if @@autoqual_reporting_setup_enabled
      assert @@siltest.verify_aqv_message(dcr, 'success'), 'wrong AQV message'
    end
  end

  def test41_parameter_extension_aggregated_wafer
    p = 'QA_PARAM_900'
    ctrl = {'MEAN'=>41, 'STDDEV'=>1, 'MIN'=>31, 'MAX'=>55}  # no COUNT
    # build and submit DCR
    dcr = SIL::DCR.new
    dcr.add_lot(:default)
    ctrl.each {|k, v| dcr.add_parameter("#{p}-#{k}", {'2502J493KO'=>v})}
    assert @@siltest.send_dcr_verify(dcr, nsamples: 1), 'error submitting DCR'
    #
    folder = @@lds.folder(@@autocreated_qa)
    assert ch = folder.spc_channel(p)
    assert s = ch.samples.last
    assert verify_hash(ctrl.merge('COUNT'=>2), s.statistics, nil, refonly: true), 'wrong sample statistics'
    assert verify_hash({'LSL'=>25.0, 'TARGET'=>45.0, 'USL'=>75.0}, s.specs, nil, refonly: true), 'wrong sample speclimits'
  end

  def test42_parameter_extension_aggregated_lot
    p = 'QA_PARAM_900'
    ctrl = {'MEAN'=>41, 'STDDEV'=>1, 'MIN'=>31, 'MAX'=>55}  # no COUNT
    # build and submit DCR
    dcr = SIL::DCR.new
    dcr.add_lot(:default)
    ctrl.each {|k, v| dcr.add_parameter("#{p}-#{k}", {@@siltest.lot=>v}, readingtype: 'Lot')}
    assert @@siltest.send_dcr_verify(dcr, nsamples: 1), 'error submitting DCR'
    #
    folder = @@lds.folder(@@autocreated_qa)
    assert ch = folder.spc_channel(p)
    assert s = ch.samples.last
    assert verify_hash(ctrl.merge('COUNT'=>2), s.statistics, nil, refonly: true), 'wrong sample statistics'
    assert verify_hash({'LSL'=>25.0, 'TARGET'=>45.0, 'USL'=>75.0}, s.specs, nil, refonly: true), 'wrong sample speclimits'
  end

  def test51_parameter_extension_lot_1chamber_sites
    chambers = @@siltest.chambers.take(1)
    pas = 'cycle_parameter'  # 'cycle_wafer'
    sample_count = (pas == 'cycle_wafer') ? chambers.size : 1
    #
    # build and submit DCR
    parameters = @@siltest.defaults[:parameters].collect {|p| "#{p}-LOT"}
    nsamples = pas == 'cycle_wafer' ? parameters.size * chambers.size : parameters.size
    readings, wvalues = generate_readings(3, sample_count, 3)
    assert dcr = SIL::DCR.new(eqp: @@siltest.ptool, eqptype: :chamber, chambers: chambers)
    dcr.add_lot(:default, op: @@siltest.ppd)
    dcr.add_parameters(parameters, readings, processing_areas: pas, sites: true)
    assert @@siltest.send_dcr_verify(dcr, nsamples: nsamples, exporttag: 'sil02_51'), 'error submitting DCR'
    #
    # verify channels and samples
    folder = @@lds.folder(@@autocreated_qa)
    parameters.each {|p|
      assert ch = folder.spc_channel(p), "no channel for #{p}"
      assert_equal sample_count, ch.samples.count, ' wrong sample count'
      ch.samples.each_with_index {|s, i|   # TODO: only 1 sample here!
        assert verify_hash(Statistics.space(wvalues[i]), s.statistics, nil, numdiff: 0.0002), 'wrong statistics'
        assert verify_hash({'PTool'=>dcr.eqp, 'PChamber'=>chambers[i], 'Wafer'=>'-'}, s.ekeys, nil, refonly: true), 'wrong ekeys'
        assert verify_hash({'PSequence'=>'-', 'MSlot'=>'-'}, s.dkeys, nil, refonly: true), 'wrong dkeys'
        assert verify_sample_raws(s, wvalues[i], true), "wrong raw values for #{s.inspect}"
      }
    }
  end

  # same as test51, except no SITE areas
  def test52_parameter_extension_lot_1chamber_nosites
    chambers = @@siltest.chambers.take(1)
    pas = 'cycle_parameter'  # 'cycle_wafer'
    sample_count = (pas == 'cycle_wafer') ? chambers.size : 1
    #
    # build and submit DCR
    parameters = @@siltest.defaults[:parameters].collect {|p| "#{p}-LOT"}
    nsamples = pas == 'cycle_wafer' ? parameters.size * chambers.size : parameters.size
    readings, wvalues = generate_readings(3, sample_count, 3)
    assert dcr = SIL::DCR.new(eqp: @@siltest.ptool, eqptype: :chamber, chambers: chambers)
    dcr.add_lot(:default, op: @@siltest.ppd)
    dcr.add_parameters(parameters, readings, processing_areas: pas)
    assert @@siltest.send_dcr_verify(dcr, nsamples: nsamples, exporttag: 'sil02_52'), 'error submitting DCR'
    #
    # verify channels and samples
    folder = @@lds.folder(@@autocreated_qa)
    parameters.each {|p|
      assert ch = folder.spc_channel(p), "no channel for #{p}"
      assert_equal sample_count, ch.samples.count, ' wrong sample count'
      ch.samples.each_with_index {|s, i|   # TODO: only 1 sample here!
        assert verify_hash(Statistics.space(wvalues[i]), s.statistics, nil, numdiff: 0.0002), 'wrong statistics'
        assert verify_hash({'PTool'=>dcr.eqp, 'PChamber'=>chambers[i], 'Wafer'=>'-'}, s.ekeys, nil, refonly: true), 'wrong ekeys'
        assert verify_hash({'PSequence'=>'-', 'MSlot'=>'-'}, s.dkeys, nil, refonly: true), 'wrong dkeys'
        assert verify_sample_raws(s, wvalues[i], false), "wrong raw values for #{s.inspect}"
      }
    }
  end

  # like test52 with multiple chambers
  def test53_parameter_extension_lot_chambers_parameters
    chambers = @@siltest.chambers
    pas = 'cycle_parameter'
    sample_count = (pas == 'cycle_wafer') ? chambers.size : 1
    #
    # build and submit DCR
    parameters = @@siltest.defaults[:parameters].collect {|p| "#{p}-LOT"}
    nsamples = pas == 'cycle_wafer' ? parameters.size * chambers.size : parameters.size
    readings, wvalues = generate_readings(25, sample_count)
    assert dcr = SIL::DCR.new(eqp: @@siltest.ptool, eqptype: :chamber, chambers: chambers)
    dcr.add_lot(:default, op: @@siltest.ppd)
    dcr.add_parameters(parameters, readings, processing_areas: pas)
    assert @@siltest.send_dcr_verify(dcr, nsamples: nsamples, exporttag: 'sil02_53'), 'error submitting DCR'
    #
    # verify channels and samples
    folder = @@lds.folder(@@autocreated_qa)
    parameters.each {|p|
      assert ch = folder.spc_channel(p), "no channel for #{p}"
      ##assert @@siltest.verify_ecomment(ch, 'LOT_HELD: reason={X-S')
      assert_equal sample_count, ch.samples.count, ' wrong sample count'
      ch.samples.each_with_index {|s, i|   # TODO: only 1 sample here!
        assert verify_hash(Statistics.space(wvalues[i]), s.statistics, nil, numdiff: 0.0002), 'wrong statistics'
        assert verify_hash({'PTool'=>dcr.eqp, 'PChamber'=>chambers.join(':'), 'Wafer'=>'-'}, s.ekeys, nil, refonly: true), 'wrong ekeys'
        assert verify_hash({'PSequence'=>'-', 'MSlot'=>'-'}, s.dkeys, nil, refonly: true), 'wrong dkeys'
        assert verify_sample_raws(s, wvalues[i], false), "wrong raw values for #{s.inspect}"
      }
    }
  end

  def test56_parameter_extension_lot_chambers
    chambers = @@siltest.chambers
    pas = 'cycle_wafer'  # 'cycle_parameter'
    sample_count = (pas == 'cycle_wafer') ? chambers.size : 1
    #
    # build and submit DCR
    parameters = @@siltest.defaults[:parameters].collect {|p| "#{p}-LOT"}
    nsamples = pas == 'cycle_wafer' ? parameters.size * chambers.size : parameters.size
    readings, wvalues = generate_readings(24, sample_count)
    assert dcr = SIL::DCR.new(eqp: @@siltest.ptool, eqptype: :chamber, chambers: chambers)
    dcr.add_lot(:default, op: @@siltest.ppd)
    dcr.add_parameters(parameters, readings, processing_areas: pas)
    assert @@siltest.send_dcr_verify(dcr, nsamples: nsamples), 'error submitting DCR'
    #
    # verify channels and samples
    folder = @@lds.folder(@@autocreated_qa)
    parameters.each {|p|
      assert ch = folder.spc_channel(p), "no channel for #{p}"
      assert_equal sample_count, ch.samples.count, ' wrong sample count'
      chambers.each_with_index {|chamber, i|
        assert s = ch.sample('PChamber'=>chamber), 'no sample'
        assert verify_hash(Statistics.space(wvalues[i]), s.statistics, nil, numdiff: 0.0002), 'wrong statistics'
        assert verify_hash({'PTool'=>@@siltest.ptool, 'PChamber'=>chambers[i], 'Wafer'=>'-'}, s.ekeys, nil, refonly: true), 'wrong ekeys'
        assert verify_hash({'PSequence'=>'-', 'MSlot'=>'-'}, s.dkeys, nil, refonly: true), 'wrong dkeys'
        assert verify_sample_raws(s, wvalues[i]), "wrong raw values for #{s.inspect}"
      }
    }
  end

  def test61_1chamber_special_parameters
    department = 'QALOTGRP'  # see spec C05-00001624
    parameter = 'QA_PARAM_900-LOT'
    wafer_count = 3
    readings, wvalues = generate_readings(wafer_count, 1)
    #
    # build and submit DCR for process PD
    assert dcr = SIL::DCR.new(eqp: @@siltest.ptool, eqptype: :chamber, chambers: [@@siltest.chambers.first])
    dcr.add_lot(:default, op: @@siltest.ppd, department: department)
    dcr.add_parameter(parameter, readings)
    wafer_count.times {|i|
      dcr.add_parameter("QA_COATER_#{i}", {"2502J49#{i}LE"=>"QA_CoaterUnit_#{i}"}, nocharting: true)
      dcr.add_parameter("QA_WAFER_#{i}", {"2502J49#{i}LE"=>"WAFER:2502J49#{i}LE"}, nocharting: true)
    }
    dcr.add_parameter('QA_LOT_PARAMETER', {@@siltest.lot=>'QA_LotValue'}, readingtype: 'Lot', nocharting: true)
    assert @@siltest.send_dcr_verify(dcr, nsamples: 1), 'error submitting DCR'
    #
    # verify sample limits
    assert folder = @@lds.folder("AutoCreated_#{department}"), 'missing folder'
    assert ch = folder.spc_channel(parameter), 'missing channel'
    assert_equal 1, ch.samples.size, 'wrong number of samples'
    s = ch.samples.last
    assert verify_hash(Statistics.space(readings), s.statistics, nil), 'wrong statistics'
    assert verify_sample_raws(s, readings), "Raw values for #{s.inspect} do not match"
    assert verify_hash({'PTool'=>@@siltest.ptool, 'PChamber'=>@@siltest.chambers.first, 'Wafer'=>'-'}, s.ekeys, nil, refonly: true), 'wrong ekeys'
    assert verify_hash({'PSequence'=>'-', 'MSlot'=>'-'}, s.dkeys, nil, refonly: true), 'wrong dkeys'
  end

  def test62_1chamber_invalid_special_parameters
    department = 'QALOTGRP'  # see spec C05-00001624
    parameter = 'QA_PARAM_900-LOT'
    wafer_count = 3
    readings, wvalues = generate_readings(wafer_count, 1)
    #
    # build and submit DCR for process PD
    assert dcr = SIL::DCR.new(eqp: @@siltest.ptool, eqptype: :chamber, chambers: ['NotSetupCHA'])
    dcr.add_lot(:default, op: @@siltest.ppd, department: department)
    dcr.add_parameter(parameter, readings)
    wafer_count.times {|i|
      dcr.add_parameter("QA_COATER_#{i}", {"2502J49#{i}LE"=>"QA_CoaterUnit_#{i}"}, nocharting: true)
      dcr.add_parameter("QA_WAFER_#{i}", {"2502J49#{i}LE"=>"WAFER:2502J49#{i}LE"}, nocharting: true)
    }
    dcr.add_parameter('QA_LOT_PARAMETER', {@@siltest.lot=>'QA_LotValue'}, readingtype: 'Lot', nocharting: true)
    assert @@siltest.send_dcr_verify(dcr, nsamples: 1), 'error submitting DCR'
    #
    # verify sample limits
    assert folder = @@lds.folder("AutoCreated_#{department}"), 'missing folder'
    assert ch = folder.spc_channel(parameter), 'missing channel'
    assert_equal 1, ch.samples.size, 'wrong number of samples'
    s = ch.samples.last
    assert verify_hash(Statistics.space(readings), s.statistics, nil), 'wrong statistics'
    assert verify_sample_raws(s, readings), "Raw values for #{s.inspect} do not match"
    assert verify_hash({'PTool'=>@@siltest.ptool, 'PChamber'=>'-', 'Wafer'=>'-'}, s.ekeys, nil, refonly: true), 'wrong ekeys'
    assert verify_hash({'PSequence'=>'-', 'MSlot'=>'-'}, s.dkeys, nil, refonly: true), 'wrong dkeys'
  end

  
  # aux methods

  # returns all wafer values and per sample mapping of values and wafers
  def generate_readings(wafer_count, sample_count, nvalues=1)
    t = Time.now.to_i
    readings = Hash[wafer_count.times.collect {|i| ["RW#{t}%02dLE" % i, nvalues.times.collect {|j| 100 + i + j.to_f / nvalues}]}]
    ##$log.info "using wafer values #{readings.inspect}"
    wvalues = sample_count.times.collect { {} }   # for sample_count == 1 just [readings], others [readings_for_wafer0.., readings_for_wafer1..] !!
    readings.each_with_index {|e, i|
      k, v = e
      ri = i % sample_count
      wvalues[ri][k] ||= []
      if v.is_a?(Array)
        wvalues[ri][k] += v
      else
        wvalues[ri][k] << v
      end
    }
    return [readings, wvalues]
  end

  # return true if wafer values match (checks also site data if needed)
  def verify_sample_raws(sample, wafer_values, sites=false)
    ret = true
    raws = sample.raws
    wafer_values.each_pair {|wafer, value|
      wafer_value = value
      wafer_value = [wafer_value] unless wafer_value.is_a?(Array)
      if sites
        wafer_value.each_with_index {|wv, i|
          rval = raws.find {|r| r.dkeys['SiteID'] == "#{wafer}_#{i+1}"}.value
          ($log.warn "  wrong data for #{wafer}/site#{i+1}: #{rval}"; ret = false) if (rval - wv).abs > 0.0002
        }
      else
        rvals = raws.select {|r| r.dkeys['SiteID'] == wafer}.collect {|r| r.value}.sort
        wafer_value.sort.each_with_index {|wv, i|
          ($log.warn "  wrong data for #{wafer}: #{rvals[i]}"; ret = false)  if (rvals[i] - wv).abs > 0.0002
        }
      end
    }
    return ret
  end

end
