=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: U. Koerner, 2012-01-11

History:
  2014-02-05 ssteidte, separated Test_Space07, code cleanup
  2017-08-01 cstenzel/sfrieske, refactored all tests
=end

require_relative 'SILTest'


# CAs Inhibits with different LDS keys (old Test_Space_76) Tests for MSR663212 - usage of different ldskeys
class SIL_74 < SILTest
  @@test_defaults = {mpd: 'QA-MB-MSR663212M.01', department: 'QARESPCKC'}  # see C05-00001624    # department: 'QARESPCKC' seems to be not necessary, why?

  @@ptools = ['UTCSIL01_0', 'UTCSIL01_1', 'UTCSIL01_2', 'UTCSIL01_3', 'UTCSIL01_4']
  @@chambers = ['PM1', 'PM2', 'PM3', 'PM4', 'PM5']
  @@valid_chambers = ['PM4', 'PM5']
  @@recipe = nil    # assigned later
  @@recipe_m = nil  # assigned later


  def setup
    super
    @@ptools.each {|eqp| assert_equal 0, $sv.eqp_cleanup(eqp)}
    if @@recipe
      assert_equal 0, $sv.inhibit_cancel(:recipe, @@recipe, silent: true)
      assert_equal 0, $sv.inhibit_cancel(:recipe, @@recipe_m, silent: true)
    end
  end


  def test00_setup
    $setup_ok = false
    #
    ['AutoEquipmentInhibit-MSR663212-6', 'AutoEquipmentInhibit-MSR663212-7',
     'AutoChamberInhibit-MSR663212-1', 'AutoChamberInhibit-MSR663212-2', 'AutoChamberInhibit-MSR663212-3',
     'AutoMachineRecipeInhibit-MSR663212-8', 'AutoMachineRecipeInhibit-MSR663212-9', 'AutoEquipmentAndRecipeInhibit-M663212-10',
     'AutoEquipmentAndRecipeInhibit-M663212-11', 'AutoEquipmentAndRecipeInhibit-M663212-12', 'AutoEquipmentAndRecipeInhibit-M663212-13',
     'AutoChamberAndRecipeInhibit-MSR663212-14', 'AutoChamberAndRecipeInhibit-MSR663212-15', 'AutoChamberAndRecipeInhibit-MSR663212-16',
     'AutoEquipmentInhibit-M663212-16',
     'ReleaseEquipmentInhibit', 'ReleaseChamberInhibit', 'ReleaseMachineRecipeInhibit',
     'ReleaseEquipmentAndRecipeInhibit', 'ReleaseChamberAndRecipeInhibit'
    ].each {|action| assert SIL::SpaceApi.ca(action), "CA #{action.inspect} not defined"}
    #
    assert lot = $svtest.new_lot, 'error creating test lot'
    @@testlots << lot
    assert @@siltest.set_defaults(@@test_defaults.merge(lot: lot)), 'wrong default test data'
    assert @@pdrefs = @@siltest.db.pd_reference(mpd: @@siltest.mpd)
    assert_equal 5, @@pdrefs.size, 'wrong number of responsible PDs'
    @@pdrefs[2] = @@pdrefs[1].next  # assign a wrong/different PD (?!)
    #
    assert recipes = $sv.machine_recipes
    assert @@recipe = recipes[2]
    assert @@recipe_m = recipes[3]
    #
    $setup_ok = true
  end

  # Equipment inhibit for ExKey04 which is PTool in ITDC
  def test11_eqp_ptool
    assert channels = cleanup_send_dcrs(cas: 'AutoEquipmentInhibit-MSR663212-6')
    # verify inhibits and ecomment
    assert @@siltest.wait_inhibit(:eqp, @@ptools.first, '', n: channels.size), 'missing inhibit'
    assert @@siltest.verify_ecomment(channels, "TOOL_HELD: #{@@ptools.first}: reason={X-S"), 'wrong ecomment'
    # release inhibits
    channels.each {|ch| assert ch.ckc_samples.last.assign_ca('ReleaseEquipmentInhibit'), 'error assigning CA'}
    assert @@siltest.wait_inhibit(:eqp, @@ptools.first, '', n: 0), 'wrong inhibit'
  end

  # No Equipment inhibit for ExKey04 which is PTool in ITDC, because LDSKey attribute is combined with attribute ResponsiblePD in the CA
  def test12_eqp_ptool_attribute
    assert channels = cleanup_send_dcrs(cas: 'AutoEquipmentInhibit-MSR663212-7')
    # verify no inhibit but notification
    n = @@siltest.parameters.size * SIL::DCR::DefaultWaferValues.size * 2
    assert @@siltest.wait_notification('LDSKey attributes combined', n: n), 'missing notifications'
    assert_empty $sv.inhibit_list(:eqp, @@ptools.first), 'wrong inhibit'
  end

  # No Equipment inhibit for ExKey04 which is PTool in Fab1 - PTool is invalid
  def test13_eqp_ptool_invalid
    action = 'AutoEquipmentInhibit-MSR663212-6'
    assert channels = cleanup_send_dcrs(cas: action, ptools: @@ptools.collect {|e| "#{e}XX"})
    # verify no inhibit and ecomment
    assert_empty $sv.inhibit_list(:eqp, @@ptools.first), 'wrong inhibit'
    assert @@siltest.verify_ecomment(channels, "Execution of [#{action}] failed"), 'wrong ecomment'
    assert_empty $sv.lot_futurehold_list(@@siltest.lot), 'wrong future hold'
  end

  # EquipmentChamber inhibit for ExKey04 which is PTool in Fab1 and ExKey05 which is PChamber in Fab1
  def test14_chamber_ptool_pchamber
    assert channels = cleanup_send_dcrs(cas: 'AutoChamberInhibit-MSR663212-1')
    #
    assert sample = channels.last.ckc_samples.last, 'missing CKC sample'
    assert_equal @@siltest.chambers.sort.join(':'), sample.ekeys['PChamber'], 'wrong PChamber'
    assert @@siltest.wait_inhibit(:eqp_chamber, @@ptools.first, '', n: channels.size * @@siltest.chambers.size), 'missing inhibit'
    assert @@siltest.verify_ecomment(channels, "TOOL+CHAMBER_HELD: #{@@ptools.first}/"), 'wrong ecomment'
    # release inhibits
    channels.each {|ch| assert ch.ckc_samples.last.assign_ca('ReleaseChamberInhibit'), 'error assigning CA'}
    assert @@siltest.wait_inhibit(:eqp_chamber, @@ptools.first, '', n: 0), 'wrong inhibit'
  end

  # EquipmentChamber inhibit for ExKey04 which is PTool in Fab1 and ExKey05 which is PChamber in Fab1
  def test15_chamber_ptoolchamber
    assert channels = cleanup_send_dcrs(cas: 'AutoChamberInhibit-MSR663212-2')
    #
    assert sample = channels.last.ckc_samples.last, 'missing CKC sample'
    assert_equal "#{@@ptools.first}-#{@@siltest.chambers.sort.join(':')}", sample.ekeys['Reserve4'], 'wrong PToolChamber'
    #
    assert @@siltest.wait_inhibit(:eqp_chamber, @@ptools.first, '', n: channels.size * @@siltest.chambers.size), 'missing inhibit'
    assert @@siltest.verify_ecomment(channels, "TOOL+CHAMBER_HELD: #{@@ptools.first}/#{@@siltest.chambers.first}: reason={X-S"), 'wrong ecomment'
    # release inhibits
    channels.each {|ch| assert ch.ckc_samples.last.assign_ca('ReleaseChamberInhibit'), 'error assigning CA'}
    assert @@siltest.wait_inhibit(:eqp_chamber, @@ptools.first, '', n: 0), 'wrong inhibit'
  end

  # No EquipmentChamber inhibit for ExKey04 which is PTool in Fab1 and ExKey05 which is PChamber in Fab1, because LDSKey attribute is combined with attribute ResponsiblePD in the CA
  def test16_chamber_attribute
    assert channels = cleanup_send_dcrs(cas: 'AutoChamberInhibit-MSR663212-3')
    #
    assert sample = channels.last.ckc_samples.last, 'missing CKC sample'
    assert_equal "#{@@ptools.first}-#{@@siltest.chambers.sort.join(':')}", sample.ekeys['Reserve4'], 'wrong PToolChamber'
    # verify no inhibit but notification
    n = @@siltest.parameters.size * SIL::DCR::DefaultWaferValues.size * 2
    assert @@siltest.wait_notification('LDSKey attributes combined', n: n), 'missing notifications'
    assert_empty $sv.inhibit_list(:eqp_chamber, @@ptools.first), 'wrong inhibit'
  end

  # No EquipmentChamber inhibit for ExKey04 which is PTool in Fab1 and ExKey05 which is PChamber in Fab1, because of invalid chambers
  def test17_chamber_invalid_some
    action = 'AutoChamberInhibit-MSR663212-2'
    chambers = ['INV1', 'INV2'] + @@siltest.chambers  # invalid and valid chambers mixed
    assert channels = cleanup_send_dcrs(cas: action, chambers: chambers)
    #
    assert sample = channels.last.ckc_samples.last, 'missing CKC sample'
    assert_equal "#{@@ptools.first}-#{chambers.sort.join(':')}", sample.ekeys['Reserve4'], 'wrong PToolChamber'
    # verify inhibits for valid chambers and ecomments
    assert @@siltest.wait_inhibit(:eqp_chamber, @@ptools.first, '', n: channels.size * @@siltest.chambers.size), 'missing inhibit'
    assert @@siltest.verify_ecomment(channels, "TOOL+CHAMBER_HELD: #{@@ptools.first}/#{@@siltest.chambers.first}: reason={X-S"), 'wrong ecomment'
    assert @@siltest.verify_ecomment(channels, "Execution of [#{action}] failed"), 'wrong ecomment'
    # release inhibits
    channels.each {|ch| assert ch.ckc_samples.last.assign_ca('ReleaseChamberInhibit'), 'error assigning CA'}  ## ReleaseEquipmentInhibit
    assert @@siltest.wait_inhibit(:eqp_chamber, @@ptools.first, '', n: 0), 'wrong inhibit'
    ###assert @@siltest.verify_inhibit(ptool, 0, class: :eqp), 'equipment+chamber must not have an inhibit'
  end

  # No EquipmentChamber and Equipment inhibit for ExKey04 which is PTool in Fab1 and ExKey05 which is PChamber in Fab1, because of invalid chambers and invalid ptool
  def test18_chamber_invalid_ptool_invalid
    action = 'AutoChamberInhibit-MSR663212-2'
    chambers = ['INV1', 'INV2', 'INV3']
    assert channels = cleanup_send_dcrs(cas: action, ptools: ['UTCSIL01_A'], chambers: chambers, nooc: false)
    #
    assert sample = channels.last.ckc_samples.last, 'missing CKC sample'
    assert_equal "UTCSIL01_A-#{chambers.sort.join(':')}", sample.ekeys['Reserve4'], 'wrong PToolChamber'
    # verify ecomments and no lot future hold
    assert @@siltest.verify_ecomment(channels, "Equipment UTCSIL01_A Information has not been found"), 'wrong ecomment'
    assert @@siltest.verify_ecomment(channels, "Execution of [#{action}] failed"), 'wrong ecomment'
    assert_empty $sv.lot_futurehold_list(@@siltest.lot), 'wrong future hold'
  end

  # Equipment inhibit for MTool which is MTool in Fab1
  def test20_eqp_mtool
    assert channels = cleanup_send_dcrs(cas: 'AutoEquipmentInhibit-M663212-16')
    #
    #
    assert sample = channels.last.ckc_samples.last, 'missing CKC sample'
    assert_equal @@siltest.mtool, sample.dkeys['MTool'], 'wrong MTool'
    # verify inhibits and ecomment
    assert @@siltest.wait_inhibit(:eqp, @@siltest.mtool, '', n: channels.size), 'missing inhibit'
    assert @@siltest.verify_ecomment(channels, "TOOL_HELD: #{@@siltest.mtool}: reason={X-S"), 'wrong ecomment'
    assert_empty $sv.inhibit_list(:eqp, @@ptools.first), 'wrong inhibit'
    # release inhibits
    channels.each {|ch| assert ch.ckc_samples.last.assign_ca('ReleaseEquipmentInhibit'), 'error assigning CA'}
    assert @@siltest.wait_inhibit(:eqp, @@siltest.mtool, '', n: 0), 'wrong inhibit'
  end


  # aux methods

  # special version for multiple PPDs, returns the channels created
  def cleanup_send_dcrs(params={})
    ptools = params[:ptools] || @@ptools
    #
    # prepare channels
    assert channels = @@siltest.setup_channels(cas: params[:cas])
    # send process DCRs
    ptools.each_with_index {|ptool, i|
      assert @@siltest.submit_process_dcr(ppd: @@pdrefs[i], ptool: ptool, chambers: params[:chambers], mrecipe: @@recipe)
    }
    # send meas DCR with OOC values
    nooc = params.has_key?(:nooc) ? params[:nooc] : 'all*2'   # no OOC check if all chambers are invalid
    assert @@siltest.submit_meas_dcr(mrecipe: params[:recipe_m] || @@recipe_m, ooc: true, nooc: nooc)
    #
    assert s = channels.last.ckc_samples.last, 'missing CKC sample'
    assert_equal ptools[0], s.ekeys['PTool'], 'wrong PTool'
    ##return [channels, ptool, pchamber, @@recipe, params[:recipe_m] || @@recipe_m]
    return channels
  end

end
