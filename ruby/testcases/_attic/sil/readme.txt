SIL Test Cases
==============
new cases, starting July 2017, cstenzel and sfrieske


within each _file_ manual tests are typically testman8x, load tests test9x
manual and load tests should preferrably (if not too much setup is duplicated) moved to SIL_y8 and SIL_y9 _files_


SIL_0x: smoke tests, must pass unconditionally
SIL_1x: Channel Behaviour



SIL_7x: Corrective Actions
