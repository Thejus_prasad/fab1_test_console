=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: U. Koerner, 2012-01-11

History:
  2014-02-05 ssteidte, separated Test_Space07, code cleanup
=end

require_relative 'SILTest'


# CAs Lot Hold (old Test_Space_070)
class SIL_70 < SILTest
  @@test_defaults = {mpd: 'QA-MB-FTDEPM-LIS-CAEX.01'}


  def test00_setup
    $setup_ok = false
    #
    assert lots = $svtest.new_lots(2), 'error creating lots'
    @@testlots += lots
    @@lot2 = lots[1]
    #
    assert @@siltest.set_defaults(@@test_defaults.merge(lot: lots[0])), 'wrong default test data'
    #
    $setup_ok = true
  end

  def test11_hold
    assert channels = @@siltest.setup_channels(cas: ['LotHold', 'AutoLotHold', 'ReleaseLotHold', 'CancelLotHold'])
    assert @@siltest.submit_process_dcr
    # send meas DCR with OOC values
    assert dcr = @@siltest.submit_meas_dcr(ooc: true), 'error submitting DCR'
    assert @@siltest.verify_aqv_message(dcr, 'fail'), 'wrong AQV message'
    #
    # verify futurehold from AutoLotHold, incl ecomment
    assert @@siltest.wait_futurehold('[(Raw above specification)', n: channels.size), 'missing future hold'
    assert @@siltest.verify_ecomment(channels, "LOT_HELD: #{@@siltest.lot}: reason={X-S")
    # cancel LotHold in all channels by CA assignment
    channels.each {|ch| assert ch.ckc_samples.last.assign_ca('CancelLotHold'), 'error assigning CA'}
    assert @@siltest.wait_futurehold('[(Raw above specification)', n: 0), "wrong future hold"
    #
    # verify manual actions with last sample in CKC
    s = channels.first.ckc_samples.last
    #
    # LotHold, CancelLotHold
    assert s.assign_ca('LotHold'), 'error assigning CA'
    assert @@siltest.wait_futurehold(s.parameter), 'missing future hold'
    assert s.assign_ca('CancelLotHold'), 'error assigning CA'
    assert @@siltest.wait_futurehold(s.parameter, n: 0), "wrong future hold"
    #
    # LotHold, ReleaseLotHold
    assert s.assign_ca('LotHold'), 'error assigning CA'
    assert @@siltest.wait_futurehold(s.parameter), 'missing future hold'
    assert_equal 0, $sv.lot_gatepass(@@siltest.lot), "SiView GatePass error"
    refute_empty $sv.lot_hold_list(@@siltest.lot, type: 'FutureHold'), 'missing lot holds'
    assert s.assign_ca('ReleaseLotHold'), 'error assigning CA'
    assert @@siltest.wait_lothold_release(s.parameter), 'wrong lot hold'
  end

  def test12_hold_multilot
    assert_equal 0, $sv.lot_cleanup(@@lot2)
    assert channels = @@siltest.setup_channels(cas: 'AutoLotHold')
    assert @@siltest.submit_process_dcr
    assert @@siltest.submit_process_dcr(lot: @@lot2)
    # send meas DCR for both lots with OOC values
    assert dcr = @@siltest.submit_meas_dcr(ooc: true, lots: [@@siltest.lot, @@lot2]), 'error submitting DCR'
    # verify future holds for both lots
    assert @@siltest.wait_futurehold("[(Raw above specification)", n: channels.size), 'missing future hold'
    assert @@siltest.wait_futurehold("[(Raw above specification)", n: channels.size, lot: @@lot2), 'missing future hold'
  end

  def test13_hold_multilot_1ooc
    assert_equal 0, $sv.lot_cleanup(@@lot2)
    assert channels = @@siltest.setup_channels(cas: 'AutoLotHold')
    assert @@siltest.submit_process_dcr
    assert @@siltest.submit_process_dcr(lot: @@lot2)
    # send meas DCR for lot1 OOC and lot2 NOOC
    assert dcr = @@siltest.submit_meas_dcr(ooc: true, nooc: 'all') {|dcr|
      dcr.add_lot(@@lot2, eqp: @@siltest.mtool, op: @@siltest.mpd)
      dcr.add_parameters(:default, :default)
    }, "error submitting DCR"
    # verify future holds for both lots
    assert @@siltest.wait_futurehold("[(Raw above specification)", n: channels.size), 'missing future hold'
    assert_empty $sv.lot_futurehold_list(@@lot2), "wrong future hold"
  end

  def test14_different_cas
    route = $sv.lot_info(@@siltest.lot).route
    assert_equal 0, $sv.inhibit_cancel(:route, route)
    assert_equal 0, $sv.eqp_cleanup(@@siltest.ptool)
    assert channels = @@siltest.setup_channels(cas: [])
    # set different corrective actions
    ch1 = channels[0 .. 1]
    ch2 = channels[2 .. 3]
    ch3 = [channels[4]]
    assert @@siltest.assign_verify_cas(ch1, 'AutoEquipmentInhibit')
    assert @@siltest.assign_verify_cas(ch2, 'AutoRouteInhibit')
    assert @@siltest.assign_verify_cas(ch3, 'AutoLotHold')
    assert @@siltest.submit_process_dcr
    # send meas DCR with OOC values
    assert dcr = @@siltest.submit_meas_dcr(ooc: true), "error submitting DCR"
    # verify CAs
    assert @@siltest.wait_inhibit(:eqp, @@siltest.ptool, '', n: ch1.size), "missing ptool inhibit"
    assert @@siltest.wait_inhibit(:route, route, '', n: ch2.size), "missing route inhibit"
    assert @@siltest.wait_futurehold('specification'), 'missing future hold'
    assert @@siltest.verify_ecomment(ch1, "TOOL_HELD: #{@@siltest.ptool}: reason={X-S")
    assert @@siltest.verify_ecomment(ch2, "ROUTE_HELD:#{route}: reason={X-S")
    assert @@siltest.verify_ecomment(ch3, "LOT_HELD: #{@@siltest.lot}: reason={X-S")
  end

  def test15_hold_childlots_LOT_parameters
    assert lot = $svtest.new_lot, "error creating lot"
    @@testlots << lot
    assert lcs = 5.times.collect {$sv.lot_split(lot, 1)}
    lots = [lot] + lcs
    #
    parameters = @@siltest.parameters.collect{|p| p + '_LOT'}
    assert channels = @@siltest.setup_channels(parameters: parameters, nsamples: 5, cas: ['AutoLotHold', 'RaiseMRB'])
    # submit process DCR for all lots
    assert @@siltest.submit_process_dcr(parameters: parameters, ppd: 'QA-MB-FTDEPP-SIL70TC15.01', lots: lots)
    # submit meas DCR with all lots OOC
    # because of LOT parameter grouping, each group of 3 LOT parameter values is counted as one sample only. Therefore SIL reports 80 samples instead of 90.
    assert @@siltest.submit_meas_dcr(parameters: parameters, lot: lot, mpd: 'QA-MB-FTDEPM-SIL70TC15.01', ooc: true, nsamples: 80) {|dcr|
      lcs.each_index {|i|
        dcr.add_lot(lcs[i], op: @@siltest.mpd)
        dcr.add_parameters(:default, :ooc)
      }
    }
    # send raisemrb ca (manual)
    channels[0].ckc_samples.each {|s|
      next unless s.violations.size > 0
      assert s.assign_ca('RaiseMRB'), 'error assigning corrective action'
    }
    # verify all lots have future holds
    lots.each {|l| assert @@siltest.wait_futurehold('[(Raw above specification)', n: channels.size, lot: l), 'missing future hold'}
  end

  def test16_hold_childlots_LOT_grouping
    assert lot = $svtest.new_lot, "error creating lot"
    @@testlots << lot
    assert lcs = 5.times.collect {$sv.lot_split(lot, 1)}
    lots = [lot] + lcs
    #
    assert channels = @@siltest.setup_channels(cas: 'AutoLotHold')
    # submit process DCR
    assert @@siltest.submit_process_dcr(lots: lots)
    # submit meas DCR with all lots OOC, readingtype Lot
    assert @@siltest.submit_meas_dcr(lot: lot, readings: {lot=>221.0}, readingtype: 'Lot') {|dcr|
      lcs.each_index {|i|
        dcr.add_lot(lcs[i], op: @@siltest.mpd)
        dcr.add_parameters(:default, {lcs[i]=>222.0 + i}, readingtype: 'Lot')
      }
    }
    # verify all lots have future holds
    lots.each {|l| assert @@siltest.wait_futurehold('[(Raw above specification)', n: channels.size, lot: l), 'missing future hold'}
  end

  def test17_hold_childlots_LOT_parameter_grouping
    assert lot = $svtest.new_lot, "error creating lot"
    @@testlots << lot
    assert lcs = 5.times.collect {$sv.lot_split(lot, 1)}
    lots = [lot] + lcs
    #
    assert channels = @@siltest.setup_channels(cas: 'AutoLotHold')
    # submit process DCR
    assert @@siltest.submit_process_dcr(lots: lots, ppd: 'QA-MB-FTDEPP-SIL70TC17.01')
    # submit meas DCR with all lots OOC, readingtype Lot and LOT parameters
    parameters = @@siltest.parameters.collect{|p| p + '_LOT'}
    assert @@siltest.submit_meas_dcr(lot: lot, mpd: 'QA-MB-FTDEPM-SIL70TC17.01', readings: {lot=>221.0}, readingtype: 'Lot') {|dcr|
      lcs.each_index {|i|
        dcr.add_lot(lcs[i], op: @@siltest.mpd)
        dcr.add_parameters(parameters, {lcs[i]=>222.0 + i}, readingtype: 'Lot')
      }
    }
    # verify all lots have future holds
    lots.each {|l| assert @@siltest.wait_futurehold('[(Raw above specification)', n: channels.size, lot: l), 'missing future hold'}
  end

end
