=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: Steffen Steidten, 2011-11-25

History:
  2017-08-01 cstenzel/sfrieske, refactored all tests
=end

require_relative 'SILTest'

# SIL template renaming tests (slow!)
class SIL_03 < SILTest
  @@test_defaults = {mpd: 'QA-MB-FTDEPM-STATE.01', ppd: :default}  # this MPD has no PPD, using DCR builtin default
  @@module_qa = 'QA'                          # QA module folder (not auto-created)


  def self.shutdown
    # ensure templates are not renamed in case of incomplete tests
    folder = @@lds.folder(@@templates_folder)
    [@@default_template, @@default_template_qa, @@params_template].each {|t|
      next if folder.spc_channel(name: t)
      if ch = folder.spc_channel(name: "#{t}_renamed")
        $log.info "renaming #{t}_renamed back to #{t}"
        ch.name = t
      end
    }
    super
  end

  def teardown
    # ensure templates are not renamed after a test
    folder = @@lds.folder(@@templates_folder)
    [@@default_template, @@default_template_qa, @@params_template].each {|t|
      next if folder.spc_channel(name: t)
      if ch = folder.spc_channel(name: "#{t}_renamed")
        $log.info "renaming #{t}_renamed back to #{t}"
        ch.name = t
      end
    }
    super
  end


  def test00_setup
    assert $setup_ok = @@siltest.set_defaults(@@test_defaults)
  end

  def test03_default_channel_rename
    folder = @@lds.folder(@@templates_folder)
    [@@default_template_qa, @@params_template].each {|t|
      ch = folder.spc_channel(name: t)
      ch.name = "#{t}_renamed" if ch
    }
    [@@default_template_qa, @@params_template].each {|t|
      cname = "#{t}_renamed"
      assert folder.spc_channel(name: cname), "template channel #{t.inspect} does not exist"
    }
    assert folder.spc_channel(name: @@default_template), "default template #{@@default_template} does not exist"
  end

  # TODO: why NODEP?
  def test11_default_template_channel_move
    # clean up original folder and move target folder
    @@lds.folders(@@autocreated_nodep).each {|folder| folder.delete}
    assert_nil @@lds.folder(@@autocreated_nodep), 'autocharting folder was not removed'
    assert tfolder = @@lds.folder(@@module_qa) || @@lds.create_folder(@@module_qa)  # TODO: better AutoCreated_QA ?
    assert tfolder.delete_channels, "error deleting channels in #{tfolder}"
    #
    # send DCRs to create the original folder, verify channels exist
    assert channels = @@siltest.setup_channels(department: 'NODEP', cas: []), 'error submitting DCRs'
    # verify SPC channel description and number of samples
    @@siltest.parameters.each {|p|
      assert ch = channels.find {|ch| ch.parameter == p}
      assert_equal "Auto-created using template: #{@@default_template}", ch.to_hash[:desc], 'wrong channel description'
      assert_equal SIL::DCR::DefaultWaferValues.size, ch.samples.size, 'wrong number of samples'
      # ekeys and dkeys are verified below
    }
    #
    # verify autocharting folder has been created and the channels have been created in this folder
    assert folder = @@lds.folder(@@autocreated_nodep), 'autocharting folder was not created'
    channels = folder.spc_channels
    assert_equal @@siltest.parameters.size, channels.size, 'wrong number of channels'
    #
    # move channels
    channels.each {|ch| assert tfolder.move_spc_channel(ch)}
    assert_equal @@siltest.parameters.size, tfolder.spc_channels.size, "error moving channels to #{tfolder}"
    assert_equal 0, folder.spc_channels.size, "error removing channels from #{folder}"
    #
    # send another meas DCR with the same parameters
    assert dcr = @@siltest.submit_meas_dcr(department: 'NODEP')
    #
    # verify no new channels have been created
    assert_equal @@siltest.parameters.size, tfolder.spc_channels.size, "error moving channels to #{tfolder}"
    assert_equal 0, folder.spc_channels.size, "error removing channels from #{folder}"
    # verify there are 2 samples per channel and wafer, extractor and data keys
    tfolder.spc_channels.each {|ch|
      # one sample after automated move (with sample deletetion; 2 samples per wafer after manual move)
      assert_equal SIL::DCR::DefaultWaferValues.size * 1, ch.samples.size, 'wrong number of samples'
      dcr.parameters.first[:Reading].each {|r|
        assert s = ch.sample('Wafer'=>r['Wafer']), 'missing sample'
        assert verify_hash(dcr.ekeys('Wafer'=>r['Wafer']), s.ekeys, nil, refonly: true), 'wrong ekey'
        assert verify_hash(dcr.dkeys('Wafer'=>r['Wafer'], parameter: s.parameter), s.dkeys, nil, refonly: true), 'wrong dkey'
      }
    }
  end

  def test12_module_default_template
    # ensure module default template exist
    folder = @@lds.folder(@@templates_folder)
    folder.spc_channel(name: "#{@@default_template_qa}_renamed").name = @@default_template_qa unless folder.spc_channel(name: @@default_template_qa)
    assert folder.spc_channel(name: @@default_template_qa), "missing template #{@@default_template_qa}"
    #
    # ensure parameters to be used are not configured for autocharting
    parameters = @@siltest.parameters.collect {|p| "#{p}DEF"}
    parameters.each {|p| refute @@siltest.db.isAutocharting(p), "parameter #{p} has an autochart configuration"}
    $log.info "using parameters #{parameters.inspect}"
    #
    # submit DCR and verify channels
    assert dcr = @@siltest.submit_meas_dcr(parameters: parameters)
    folder = @@lds.folder(@@autocreated_qa)
    folder.spc_channels.each {|ch|
      assert_equal "Auto-created using template: #{@@default_template_qa}", ch.to_hash[:desc]
      assert_equal SIL::DCR::DefaultWaferValues.size, ch.samples.size, 'wrong number of samples'
      dcr.parameters.first[:Reading].each {|r|
        assert s = ch.sample('Wafer'=>r['Wafer']), 'missing sample'
        assert verify_hash(dcr.ekeys('Wafer'=>r['Wafer']), s.ekeys, nil, refonly: true), 'wrong ekey'
        assert verify_hash(dcr.dkeys('Wafer'=>r['Wafer'], parameter: s.parameter), s.dkeys, nil, refonly: true), 'wrong dkey'
      }
    }
  end

  def test13_no_default_templates
    # ensure parameters to be used are not configured for autocharting
    parameters = @@siltest.parameters.collect {|p| "#{p}DEF"}
    parameters.each {|p| refute @@siltest.db.isAutocharting(p), "parameter #{p} has an autochart configuration"}
    $log.info "using parameters #{parameters.inspect}"
    # rename templates
    folder = @@lds.folder(@@templates_folder)
    [@@default_template_qa, @@default_template].each {|t|
      $log.info "renaming template #{t}"
      ch = folder.spc_channel(name: t)
      ch.name = "#{t}_renamed" if ch
      assert folder.spc_channel(name: "#{t}_renamed"), "error renaming template #{t}"
    }
    # submit DCR and verify no channels in AutoCreated_QA
    assert dcr = @@siltest.submit_meas_dcr(parameters: parameters, nsamples: 0, spcerror: true)
    assert folder = @@lds.folder(@@autocreated_qa)
    parameters.each {|p| refute folder.spc_channel(p), "no SPC channel must be created for #{p}"}
    # rename templates back
    folder = @@lds.folder(@@templates_folder)
    [@@default_template_qa, @@default_template].each {|t|
      $log.info "renaming template #{t}_renamed back"
      ch = folder.spc_channel(name: "#{t}_renamed")
      ch.name = t if ch
      assert folder.spc_channel(name: t), "error renaming template #{t}"
    }
  end

end
