=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: Steffen Steidten, 2011-11-25
=end

require_relative 'SILTest'


# Channel behaviour, was Test_Space00 (basic)
class SIL_11 < SILTest
  @@test_defaults = {mpd: 'QA-MB-FTDEPM-STATE.01', ppd: :default, technology: 'TEST',
    cas: ['LotHold', 'AutoLotHold', 'CancelLotHold', 'ReleaseLotHold']}
  @@email_addresses = '<M23434>'   # dl QA Fab1    '<M3016>'  # ITDC Conny Stenzel

  def test00_setup
    $setup_ok = false
    #
    assert lot = $svtest.new_lot, 'error creating test lot'  # seams to be really required e.g. test31
    @@testlots << lot
    $log.info "using lot #{lot.inspect}"
    #
    assert @@siltest.set_defaults(@@test_defaults.merge(lot: lot))
    #
    $setup_ok = true
  end

  # channel state 'Archived'

  def test11_archived
    assert dcr = @@siltest.cleanup_send_dcrs(:default, state: 'Archived')
    assert @@siltest.verify_aqv_message(dcr, 'fail'), "wrong AQV message"
  end

  def test12_archived_ooc
    assert dcr = @@siltest.cleanup_send_dcrs(:ooc, state: 'Archived')
    assert @@siltest.verify_aqv_message(dcr, 'fail'), "wrong AQV message"
  end

  # channel state 'Frozen'

  def test21_frozen
    assert dcr = @@siltest.cleanup_send_dcrs(:default, state: 'Frozen')
    assert @@siltest.verify_aqv_message(dcr, 'success'), "wrong AQV message"
  end

  def test22_frozen_ooc
    assert dcr = @@siltest.cleanup_send_dcrs(:ooc, state: 'Frozen')
    assert @@siltest.verify_aqv_message(dcr, 'fail'), "wrong AQV message"
  end

  # channel state 'Study'

  def test31_study
    assert dcr = @@siltest.cleanup_send_dcrs(:default, state: 'Study')
    assert @@siltest.verify_aqv_message(dcr, 'fail'), "wrong AQV message"
  end

  def test32_study_ooc
    assert dcr = @@siltest.cleanup_send_dcrs(:ooc, state: 'Study')
    assert @@siltest.verify_aqv_message(dcr, 'fail'), "wrong AQV message"
  end

  def test33_4study1offline
    assert dcr = @@siltest.cleanup_send_dcrs(:default, state: 'Study', exceptState: 'Offline')
    assert @@siltest.verify_aqv_message(dcr, 'fail'), "wrong AQV message"
  end

  def test34_4study1offline_ooc
    assert dcr = @@siltest.cleanup_send_dcrs(:ooc, state: 'Study', exceptState: 'Offline')
    assert @@siltest.verify_aqv_message(dcr, 'fail'), "wrong AQV message"
  end

  def test35_4offline1study
    assert dcr = @@siltest.cleanup_send_dcrs(:default, state: 'Offline', exceptState: 'Study')
    assert @@siltest.verify_aqv_message(dcr, 'fail'), "wrong AQV message"
  end

  def test36_4offline1study_ooc
    assert dcr = @@siltest.cleanup_send_dcrs(:ooc, state: 'Offline', exceptState: 'Study')
    assert @@siltest.verify_aqv_message(dcr, 'fail'), "wrong AQV message"
  end

  # channel state 'Offline'

  def test41_offline_nomail
    folder = @@lds_setup.folder(@@templates_folder)
    ch = folder.spc_channel(name: @@params_template)
    ch.set_email_addresses('')
    ch.valuations.each {|v| assert_empty v.email_addresses, 'wrong email address'}
    assert dcr = @@siltest.cleanup_send_dcrs(:default, state: 'Offline')
    assert @@siltest.verify_aqv_message(dcr, 'success'), "wrong AQV message"
  end

  def test42_offline_nomail_ooc
    folder = @@lds_setup.folder(@@templates_folder)
    ch = folder.spc_channel(name: @@params_template)
    ch.set_email_addresses('')
    ch.valuations.each {|v| assert_empty v.email_addresses, 'wrong email address'}
    assert dcr = @@siltest.cleanup_send_dcrs(:ooc, state: 'Offline')
    assert @@siltest.verify_aqv_message(dcr, 'fail'), "wrong AQV message"
  end

  def test43_offline_nomail_noca
    folder = @@lds_setup.folder(@@templates_folder)
    ch = folder.spc_channel(name: @@params_template)
    ch.set_email_addresses('')
    ch.valuations.each {|v| assert_empty v.email_addresses, 'wrong email address'}
    assert dcr = @@siltest.cleanup_send_dcrs(:default, cas: [], state: 'Offline')
    # wrong behavior in 4.9.3, returns 'success'
    assert @@siltest.verify_aqv_message(dcr, 'fail'), "wrong AQV message"
  end

  def test44_offline_nomail_noca_ooc
    folder = @@lds_setup.folder(@@templates_folder)
    ch = folder.spc_channel(name: @@params_template)
    ch.set_email_addresses('')
    ch.valuations.each {|v| assert_empty v.email_addresses, 'wrong email address'}
    assert dcr = @@siltest.cleanup_send_dcrs(:ooc, state: 'Offline', cas: [])
    assert @@siltest.verify_aqv_message(dcr, 'fail'), "wrong AQV message"
  end

  def test45_offline_email
    folder = @@lds_setup.folder(@@templates_folder)
    ch = folder.spc_channel(name: @@params_template)
    ch.set_email_addresses(@@email_addresses)
    ch.valuations.each {|v| refute_empty v.email_addresses, 'wrong email address'}
    assert dcr = @@siltest.cleanup_send_dcrs(:default, state: 'Offline')
    assert @@siltest.verify_aqv_message(dcr, 'success'), "wrong AQV message"
  end

  def test46_offline_email_ooc
    folder = @@lds_setup.folder(@@templates_folder)
    ch = folder.spc_channel(name: @@params_template)
    ch.set_email_addresses(@@email_addresses)
    ch.valuations.each {|v| refute_empty v.email_addresses, 'wrong email address'}
    assert dcr = @@siltest.cleanup_send_dcrs(:ooc, state: 'Offline')
    assert @@siltest.verify_aqv_message(dcr, 'fail'), "wrong AQV message"
  end

  def test47_offline_email_noca
    folder = @@lds_setup.folder(@@templates_folder)
    ch = folder.spc_channel(name: @@params_template)
    ch.set_email_addresses(@@email_addresses)
    ch.valuations.each {|v| refute_empty v.email_addresses, 'wrong email address'}
    assert dcr = @@siltest.cleanup_send_dcrs(:default, state: 'Offline', cas: [])
    # wrong behavior in 4.9.3, returns 'success'
    assert @@siltest.verify_aqv_message(dcr, 'fail'), "wrong AQV message"
  end

  def test48_offline_email_noca_ooc
    folder = @@lds_setup.folder(@@templates_folder)
    ch = folder.spc_channel(name: @@params_template)
    ch.set_email_addresses(@@email_addresses)
    ch.valuations.each {|v| refute_empty v.email_addresses, 'wrong email address'}
    assert dcr = @@siltest.cleanup_send_dcrs(:ooc, state: 'Offline', cas: [])
    assert @@siltest.verify_aqv_message(dcr, 'fail'), "wrong AQV message"
  end

  def test49_offline_email_caX_ooc
    folder = @@lds_setup.folder(@@templates_folder)
    ch = folder.spc_channel(name: @@params_template)
    ch.set_email_addresses(@@email_addresses)
    ch.valuations.each {|v| refute_empty v.email_addresses, 'wrong email address'}
    # setup channels incl. state and CAs
    assert channels = @@siltest.setup_channels(state: 'Offline', cas: [])
    channels.each {|ch| ch.set_cas('AutoLotHold', valuations: [17])}  # set AutoCA to "Sigma below...", which is not activated
    # send process and meas DCRs
    assert @@siltest.submit_process_dcr
    assert dcr = @@siltest.submit_meas_dcr(ooc: true)
    assert @@siltest.verify_aqv_message(dcr, 'success'), "wrong AQV message"
  end


  def test99
    $log.warn "INLINE AQV is turned off in SIL config, results might not be significant!" unless @@siltest.flags[:aqv_inline_active]
    $log.warn "SETUP AQV is turned off in SIL config, results might not be significant!" unless @@siltest.flags[:aqv_setup_active]
  end


end
