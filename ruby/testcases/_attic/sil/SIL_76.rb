=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: U. Koerner, 2012-01-11

History:
  2014-02-05 ssteidte, separated Test_Space07, code cleanup
  2017-08-01 cstenzel/sfrieske, refactored all tests
=end

require_relative 'SILTest'


# CAs with future holds (old Test_Space_076 and Test_Space_074)
class SIL_76 < SILTest
  @@test_defaults = {mpd: 'QA-MB-FTDEPM-LIS-CAEX.01'}
  @@cafailure_lothold_fallback = true    # F1 true, F8 false
  @@resp_opnum_1 = '1000.3500'
  @@resp_opnum_2 = '2700.1000'
  @@resp_start_2 = '2000.1000'
  @@resp_opnum_3 = ['2000.1000', '2700.1000', '2700.2000', '2800.1000']
  @@resp_start_4 = '2500.1000'

  def test00_setup
    $setup_ok = false
    #
    ['AutoHoldAtFuturePD-TC0760', 'ReleaseLotHold', 'AutoHoldAtFuturePD-TC0761', 'HoldAtFuturePD-TC0761',
      'AutoHoldAtFuturePD-TC0762', 'HoldAtFuturePD-TC0762', 'AutoHoldAtFuturePD-TC0763', 'HoldAtFuturePD-TC0763',
      'AutoHoldAtFuturePD-TC0764', 'HoldAtFuturePD-TC0764', 'AutoHoldAtFuturePD-TC0765', 'HoldAtFuturePD-TC0765',
      'AutoHoldAtFuturePD-TC0766-1', 'AutoHoldAtFuturePD-TC0766-2', 'AutoHoldAtFuturePD-TC0766-3',
      'HoldAtFuturePD-TC0766-1', 'HoldAtFuturePD-TC0766-2', 'HoldAtFuturePD-TC0766-3',
      'AutoHoldAtFuturePD-TC0767', 'AutoLotHold', 'HoldAtFuturePD-TC0768',
      'AutoUnknown', 'Unknown'
    ].each {|action| assert SIL::SpaceApi.ca(action), "CA #{action.inspect} not defined"}
    #
    assert lot = $svtest.new_lot, 'error creating test lot'
    @@testlots << lot
    assert @@siltest.set_defaults(@@test_defaults.merge(lot: lot)), 'wrong default test data'
    assert_equal 0, $sv.lot_cleanup(@@siltest.lot)
    #
    $setup_ok = true
  end

  def test11_pre
    assert dcr = @@siltest.cleanup_send_dcrs(:ooc, cas: ['AutoHoldAtFuturePD-TC0760', 'ReleaseLotHold'])
    # verify future holds
    assert folder = @@lds.folder(@@autocreated_qa)
    @@siltest.parameters.each {|p|
      assert hh = @@siltest.wait_futurehold("#{p}[(Raw above specification)"), 'missing future hold'
      assert_equal @@resp_opnum_1, hh.first.rsp_op, 'wrong responsible operation'
      refute hh.first.post, 'wrong POST flag'
      # comment is LOT_HELD, known issue
      ##assert @@siltest.verify_ecomment(folder.spc_channel(p), "FUTURE_HOLD: #{@@siltest.lot} #{@@resp_opnum_1}:  reason={X-S"), 'wrong ecomment'
    }
    # remove missing parameter hold (since SIL 4.12) and locate to responsible PD
    assert_equal 0, $sv.lot_futurehold_cancel(@@siltest.lot, 'XSPC-P')
    assert_equal 0, $sv.lot_opelocate(@@siltest.lot, @@resp_opnum_1)
    refute_empty $sv.lot_hold_list(@@siltest.lot, type: 'FutureHold'), 'missing lot hold'
    # release actions
    @@siltest.parameters.each {|p|
      assert ch = folder.spc_channel(p)
      assert ch.ckc_samples.last.assign_ca('ReleaseLotHold'), 'error assigning CA'
    }
    assert wait_for(timeout: 120) {$sv.lot_hold_list(@@siltest.lot).empty?}, 'wrong lot hold'
  end

  def test12_missing_filter
    assert dcr = @@siltest.cleanup_send_dcrs(:ooc, cas: ['AutoHoldAtFuturePD-TC0761', 'HoldAtFuturePD-TC0761'])
    # verify future holds
    memo = 'Execution of [AutoHoldAtFuturePD-TC0761] failed'
    assert folder = @@lds.folder(@@autocreated_qa)
    if @@cafailure_lothold_fallback
      assert @@siltest.wait_futurehold(memo), 'missing future hold'
      @@siltest.parameters.each {|p|
        assert @@siltest.verify_ecomment(folder.spc_channel(p), "LOT_HELD: #{@@siltest.lot}: reason={X-S"), 'wrong ecomment'
      }
    else
      @@siltest.parameters.each {|p|
        refute_empty msgs = @@siltest.wait_notification(memo, n: @@siltest.parameters.size), 'missing notification'
      }
    end
    # manual CA not assignable
    assert ch = folder.spc_channel(@@siltest.parameters.last), 'missing channel'
    refute ch.ckc_samples.last.assign_ca('HoldAtFuturePD-TC0761'), 'wrong CA assignment'
  end

  def test13_post
    assert dcr = @@siltest.cleanup_send_dcrs(:ooc, cas: ['AutoHoldAtFuturePD-TC0762', 'HoldAtFuturePD-TC0762'])
    # verify future holds
    assert folder = @@lds.folder(@@autocreated_qa)
    @@siltest.parameters.each {|p|
      assert hh = @@siltest.wait_futurehold("#{p}[(Raw above specification)"), 'missing future hold'
      assert_equal @@resp_opnum_1, hh.first.rsp_op, 'wrong responsible operation'
      assert hh.first.post, 'missing POST flag'
      # comment is LOT_HELD, known issue
      ##assert @@siltest.verify_ecomment(folder.spc_channel(p), "FUTURE_HOLD: #{@@siltest.lot} #{@@resp_opnum_1}:  reason={X-S"), 'wrong ecomment'
    }
    # manual CA is assignable
    assert ch = folder.spc_channel(@@siltest.parameters.last), 'missing channel'
    assert ch.ckc_samples.last.assign_ca('HoldAtFuturePD-TC0762'), 'error assigning CA'
  end

  def test14_post_wildcards
    assert_equal 0, $sv.lot_opelocate(@@siltest.lot, @@resp_start_2)
    assert dcr = @@siltest.cleanup_send_dcrs(:ooc, cas: ['AutoHoldAtFuturePD-TC0763', 'HoldAtFuturePD-TC0763'])
    # verify future holds
    assert folder = @@lds.folder(@@autocreated_qa)
    @@siltest.parameters.each {|p|
      assert hh = @@siltest.wait_futurehold("#{p}[(Raw above specification)"), 'missing future hold'
      assert_equal @@resp_opnum_2, hh.first.rsp_op, 'wrong responsible operation'
      assert hh.first.post, 'missing POST flag'
      # comment is LOT_HELD, known issue
      ##assert @@siltest.verify_ecomment(folder.spc_channel(p), "FUTURE_HOLD: #{@@siltest.lot} #{@@resp_opnum_2}:  reason={X-S"), 'wrong ecomment'
    }
    # manual CA incl MSR691919
    verify_manual_ca(@@siltest.parameters.first, 'HoldAtFuturePD-TC0763', @@resp_opnum_2)
  end

  # TODO: review, duplicate of test11?
  def test15_pre_no_wildcards
    assert dcr = @@siltest.cleanup_send_dcrs(:ooc, cas: ['AutoHoldAtFuturePD-TC0764', 'HoldAtFuturePD-TC0764'])
    # verify future holds
    assert folder = @@lds.folder(@@autocreated_qa)
    @@siltest.parameters.each {|p|
      assert hh = @@siltest.wait_futurehold("#{p}[(Raw above specification)"), 'missing future hold'
      assert_equal @@resp_opnum_1, hh.first.rsp_op, 'wrong responsible operation'
      refute hh.first.post, 'wrong POST flag'
      refute hh.first.single, 'wrong SINGLE flag'
      # comment is LOT_HELD, known issue
      ##assert @@siltest.verify_ecomment(folder.spc_channel(p), "FUTURE_HOLD: #{@@siltest.lot} #{@@resp_opnum_1}:  reason={X-S"), 'wrong ecomment'
    }
    # manual CA incl MSR691919
    verify_manual_ca(@@siltest.parameters.last, 'HoldAtFuturePD-TC0764', @@resp_opnum_1, post: false, single: false)
  end

  def test16_pre_wildcards
    assert_equal 0, $sv.lot_opelocate(@@siltest.lot, @@resp_start_2)
    assert dcr = @@siltest.cleanup_send_dcrs(:ooc, cas: ['AutoHoldAtFuturePD-TC0765', 'HoldAtFuturePD-TC0765'])
    # verify future holds
    assert folder = @@lds.folder(@@autocreated_qa)
    @@siltest.parameters.each {|p|
      assert hh = @@siltest.wait_futurehold("#{p}[(Raw above specification)"), 'missing future hold'
      assert_equal @@resp_opnum_2, hh.first.rsp_op, 'wrong responsible operation'
      refute hh.first.post, 'wrong POST flag'
      refute hh.first.single, 'wrong SINGLE flag'
      # comment is LOT_HELD, known issue
      ##assert @@siltest.verify_ecomment(folder.spc_channel(p), "FUTURE_HOLD: #{@@siltest.lot} #{@@resp_opnum_2}:  reason={X-S"), 'wrong ecomment'
    }
    # manual CA incl MSR691919
    verify_manual_ca(@@siltest.parameters.last, 'HoldAtFuturePD-TC0765', @@resp_opnum_2, post: false, single: false)
  end

  def test17_pre_post_wildcards
    assert_equal 0, $sv.lot_opelocate(@@siltest.lot, @@resp_start_2)
    assert dcr = @@siltest.setup_channels(cas: ['AutoHoldAtFuturePD-TC0766-1', 'AutoHoldAtFuturePD-TC0766-2',
      'AutoHoldAtFuturePD-TC0766-3', 'HoldAtFuturePD-TC0766-1', 'HoldAtFuturePD-TC0766-2', 'HoldAtFuturePD-TC0766-3'])
    # submit process DCR
    assert @@siltest.submit_process_dcr
    # submit meas DCR for only 1 parameter
    assert @@siltest.submit_meas_dcr(parameters: @@siltest.parameters.first, ooc: true)
    # remove missing parameter hold (since SIL 4.12) and verify future holds
    assert_equal 0, $sv.lot_futurehold_cancel(@@siltest.lot, 'XSPC-P')  ## actually nothing to cancel
    assert_equal 0, $sv.lot_futurehold_cancel(@@siltest.lot, 'CC')    ## actually nothing to cancel
    hh = $sv.lot_futurehold_list(@@siltest.lot)
    assert hh.size >= 3, 'wrong number of lot holds'
    assert_equal @@resp_opnum_3[1..-1].sort, hh.collect {|h| h.rsp_op}.uniq.sort, 'wrong responsible op in lot hold'
    # manual CA
    assert_equal 0, $sv.lot_cleanup(@@siltest.lot, op: :current)
    assert folder = @@lds.folder(@@autocreated_qa), 'missing folder'
    assert ch = folder.spc_channel(@@siltest.parameters.first), 'missing channel'
    (1..3).each {|i| assert ch.ckc_samples.last.assign_ca("HoldAtFuturePD-TC0766-#{i}"), 'error assigning CA'}
    # verify holds
    assert hh = @@siltest.wait_futurehold('specification', n: 3), 'wrong number of future holds'
    assert_equal @@resp_opnum_3[1..-1].sort, hh.collect {|h| h.rsp_op}.sort, 'wrong responsible op in lot hold'
  end

  def test18_pre_no_wildcards_lothold
    assert dcr = @@siltest.cleanup_send_dcrs(:ooc, cas: ['AutoHoldAtFuturePD-TC0767', 'AutoLotHold'])
    # verify number of future holds, hard limit in SIL
    assert @@siltest.wait_futurehold('specification', n: ['itdc', 'let'].member?($env) ? 10 : 9), 'wrong number of future holds'
  end

  # MSR 550533
  def test19_hold_at_pd_no_wildcards
    assert dcr = @@siltest.cleanup_send_dcrs(:ooc, cas: ['HoldAtFuturePD-TC0768'])
    sample_comments = {}
    assert folder = @@lds.folder(@@autocreated_qa), 'missing folder'
    @@siltest.parameters.each {|p|
      assert ch = folder.spc_channel(p), 'missing channel'
      assert s = ch.ckc_samples.last, 'missing sample'
      sample_comments[s.sid] = "ReleaseHold #{s.sid}"
      # assign CA for future hold
      assert s.assign_ca('HoldAtFuturePD-TC0768', comment: sample_comments[s.sid]), 'error assigning CA'
      assert hh = @@siltest.wait_futurehold("#{s.parameter}[("), 'missing future hold'
      assert_equal @@resp_opnum_1, hh.first.rsp_op, 'wrong responsible operation'
      refute hh.first.post, 'wrong POST flag'
      # known issue, LOT_HELD
      ##sleep 10
      ##assert s = ch.ckc_samples.last, 'missing sample'
      ##assert s.ecomment.include?("FUTURE_HOLD: #{@@siltest.lot} #{@@resp_opnum_1}:  reason={X-S"), 'wrong ecomment'
    }
    # since SIL 4.12, we separated the reason code for missing param and a violation, therefore, this is necessary now: TODO ??
    $sv.lot_futurehold_cancel(@@siltest.lot, 'XSPC-P')
    $sv.lot_futurehold_cancel(@@siltest.lot, 'XSPC-C')
    # locate lot to responsible PD and release
    assert_equal 0, $sv.lot_opelocate(@@siltest.lot, @@resp_opnum_1)
    assert $sv.lot_hold_list(@@siltest.lot, type: 'FutureHold').size > 0, 'missing lot hold'
    @@siltest.parameters.each {|p|
      assert ch = folder.spc_channel(p), 'missing channel'
      assert ch.ckc_samples.last.assign_ca('ReleaseLotHold'), 'error assigning CA'
    }
    assert @@siltest.wait_lothold_release(''), 'wrong lot hold'
  end


  # aux methods

  # MSR691919
  def verify_manual_ca(parameter, ca, resp_op, params={})
    assert_equal 0, $sv.lot_cleanup(@@siltest.lot, op: :current)
    assert folder = @@lds.folder(@@autocreated_qa), 'missing folder'
    assert channel = folder.spc_channel(parameter), 'missing channel'
    assert channel.ckc_samples.last.assign_ca(ca), 'error assigning CA'
    #
    assert hh = @@siltest.wait_futurehold("#{parameter}["), 'missing future hold'
    assert_equal resp_op, hh.first.rsp_op, 'wrong responsible operation'
    post = params[:post] != false
    single = params[:single] != false
    assert_equal post, hh.first.post, 'wrong post flag value'
    assert_equal single, hh.first.single, 'wrong single flag value'
    # comment is LOT_HELD, known issue
    ##assert @@siltest.verify_ecomment_sample(channel.ckc_samples.last, "FUTURE_HOLD: #{@@siltest.lot} #{@@resp_op}:  reason={X-S"), 'wrong ecomment'
  end
end
