=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: Steffen Steidten, 2011-11-25

History:
  2017-08-01 cstenzel/sfrieske, refactored all tests
=end

require_relative 'SILTest'


# Channel behaviour, was Test_Space00 (ECP)
class SIL_12 < SILTest
  @@test_defaults = {
    mpd: 'QA-MB-CC-M-ECP.01', ppd: 'QA-MB-CC.01', department: 'QACCALL', technology: 'TEST',
    parameters: ['QA_PARAM_900_ECP', 'QA_PARAM_901_ECP', 'QA_PARAM_902_ECP', 'QA_PARAM_903_ECP', 'QA_PARAM_904_ECP'],
    cas: ['LotHold', 'AutoLotHold', 'CancelLotHold', 'ReleaseLotHold']
  }


  def test00_setup
    $setup_ok = false
    #
    assert @@lots = $svtest.new_lots(2), 'error creating test lots'
    @@testlots += @@lots
    $log.info "using lots #{@@lots}"
    #
    assert @@siltest.set_defaults(@@test_defaults.merge(lot: @@lots[0]))
    #
    $setup_ok = true
  end

  def test12_study_background
    assert dcr = @@siltest.cleanup_send_dcrs(:default, state: 'Study', mpd: 'QA-MB-CC-M-ECP-BACKGROUND.01')  # Criticality=B (BACKGROUND)
    tstart = Time.now
    assert @@siltest.verify_aqv_message(dcr, 'success'), 'wrong AQV message'
    refute @@siltest.wait_futurehold('Chart Check issue', tstart: tstart), 'wrong future hold'
    refute @@siltest.wait_notification('Chart Check Failures for Lot', tstart: tstart), 'wrong notification'
  end

  # currently fails!
  def testdev16_inline_study
    assert dcr = @@siltest.cleanup_send_dcrs(:default, state: 'Study', technology: '32NM')
    tstart = Time.now
    assert @@siltest.verify_aqv_message(dcr, 'fail'), 'wrong AQV message'
    assert @@siltest.wait_futurehold('Chart Check issue', tstart: tstart), 'missing future hold'
    gets
    assert @@siltest.wait_notification('Chart Check Failures for Lot', tstart: tstart), 'missing notification'
  end

  def test17_inline_study_background
    assert dcr = @@siltest.cleanup_send_dcrs(:default, state: 'Study', technology: '32NM', mpd: 'QA-MB-CC-M-ECP-BACKGROUND.01')  # Criticality=B (BACKGROUND)
    tstart = Time.now
    assert @@siltest.verify_aqv_message(dcr, 'success'), 'wrong AQV message'
    refute @@siltest.wait_futurehold('Chart Check issue', tstart: tstart), 'wrong future hold'
    refute @@siltest.wait_notification('Chart Check Failures for Lot', tstart: tstart), 'wrong notification'
  end

  def test22_offline_background
    assert dcr = @@siltest.cleanup_send_dcrs(:default, state: 'Offline', mpd: 'QA-MB-CC-M-ECP-BACKGROUND.01')  # Criticality=B (BACKGROUND)
    tstart = Time.now
    assert @@siltest.verify_aqv_message(dcr, 'success'), 'wrong AQV message'
    refute @@siltest.wait_futurehold('Chart Check issue', tstart: tstart), 'wrong future hold'
    refute @@siltest.wait_notification('Chart Check Failures for Lot', tstart: tstart), 'wrong notification'
  end

  def test27_inline_offline
    assert dcr = @@siltest.cleanup_send_dcrs(:default, state: 'Offline', technology: '32NM')
    tstart = Time.now
    assert @@siltest.verify_aqv_message(dcr, 'success'), 'wrong AQV message'
    refute @@siltest.wait_futurehold('Chart Check issue', tstart: tstart), 'wrong future hold'
    refute @@siltest.wait_notification('Chart Check Failures for Lot', tstart: tstart), 'wrong notification'
  end

  def test28_inline_offline_background
    assert dcr = @@siltest.cleanup_send_dcrs(:default, state: 'Offline', technology: '32NM', mpd: 'QA-MB-CC-M-ECP-BACKGROUND.01')  # Criticality=B (BACKGROUND)
    tstart = Time.now
    assert @@siltest.verify_aqv_message(dcr, 'success'), 'wrong AQV message'
    refute @@siltest.wait_futurehold('Chart Check issue', tstart: tstart), 'wrong future hold'
    refute @@siltest.wait_notification('Chart Check Failures for Lot', tstart: tstart), 'wrong notification'
  end

  # currently fails at futurehold!!!
  def testdev51_2lots_study_missing_parameters
    assert_equal 0, $sv.lot_cleanup(@@lots[1])
    assert dcr = @@siltest.cleanup_send_dcrs(:default, state: 'Study', lots: @@lots, parameters: [@@siltest.parameters.first])
    tstart = Time.now
    assert @@siltest.verify_aqv_message(dcr, 'fail'), 'wrong AQV message'
    assert @@siltest.wait_futurehold('Chart Check issue', tstart: tstart), 'missing future hold'
    assert @@siltest.wait_notification('Chart Check Failures for Lot', tstart: tstart), 'notification missing'
  end

  def test52_2lots_offline
    assert_equal 0, $sv.lot_cleanup(@@lots[1])
    assert dcr = @@siltest.cleanup_send_dcrs(:default, state: 'Offline', lots: @@lots)
    tstart = Time.now
    assert @@siltest.verify_aqv_message(dcr, 'success'), "wrong AQV message"
    refute @@siltest.wait_futurehold('Chart Check issue', tstart: tstart), "missing future hold"
    refute @@siltest.wait_notification('Chart Check Failures for Lot', tstart: tstart), 'notification missing'
  end


  # manual tests, need maintenance

  def testman81_ooc_nooc_statetest_5offline_1study
    parameters = ['QA_PARAM_900_ECP', 'QA_PARAM_901_ECP', 'QA_PARAM_902_ECP', 'QA_PARAM_903_ECP', 'QA_PARAM_904_ECP']
    dcr = nil
    2.times {|i|
      # send process DCR
      dcrp = SIL::DCR.new(eqp: @@siltest.ptool, eqptype: :chamber)
      dcrp.add_lot(@@lot, technology: 'TEST')
      dcrp.add_parameters(parameters, :default, nocharting: true)
      assert @@siltest.send_dcr_verify(dcrp)
      # send meas DCR
      dcr = Space::DCR.new(eqp: @@siltest.mtool)
      dcr.add_lot(@@lot, op: 'QA-MB-AQV-MultiLot-M.01', technology: 'TEST', route: 'AQV_ECP.01')
      dcr.add_parameters(parameters, :default)
      res = $client.send_dcr(dcr, export: :caller)

      if i == 0
        $log.info 'Please copy one of the existing channels in a way that 1 parameter will be in two channels and set one channel to Offline, the other one to Study then press any key to continue.'
        gets
      end
    }
    assert @@siltest.verify_aqv_message(dcr, 'success'), 'wrong AQV message'
  end

  def testman82_ooc_nooc_statetest_5offline_1study_multilot
    parameters = ['QA_PARAM_900_ECP', 'QA_PARAM_901_ECP', 'QA_PARAM_902_ECP', 'QA_PARAM_903_ECP', 'QA_PARAM_904_ECP']
    assert lot2 = $svtest.new_lot
    @@testlots << lot2
    $log.info "using second lot #{lot2.inspect}"
    dcr = nil
    2.times {|i|
      wv = i == 0 ? :default : :ooc
      dcrp = SIL::DCR.new(eqp: @@siltest.ptool, eqptype: :chamber)
      dcrp.add_lot(@@lot, technology: 'TEST')
      dcrp.add_parameters(parameters, wv, nocharting: true)
      dcrp.add_lot(lot2, technology: 'TEST')
      dcrp.add_parameters(parameters, wv, nocharting: true)
      assert @@siltest.send_dcr_verify(dcrp)
      #
      dcr = Space::DCR.new(eqp: @@siltest.mtool)
      dcr.add_lot(@@lot, op: 'QA-MB-AQV-MultiLot-M.01', route: 'AQV_ECP.01', technology: 'TEST')
      dcr.add_parameters(parameters, wv)
      dcrp.add_lot(lot2, op: 'QA-MB-AQV-MultiLot-M.01', technology: 'TEST', route: 'AQV_ECP.01')
      dcrp.add_parameters(parameters, wv, nocharting: true)
      res = @@siltest.client.send_receive(dcr.to_xml, export: :caller)
      #
      if i == 0
        $log.info 'Please copy one of the existing channels in a way that 1 parameter will be in two channels and set one channel to Offline, the other one to Study then press any key to continue.'
        gets
        assert @@siltest.verify_aqv_message(dcr, 'success', lot: @@lot), 'wrong AQV message'
      elsif
        assert @@siltest.verify_aqv_message(dcr, 'fail', lot: @@lot), 'wrong AQV message'
      end
    }
  end

  def testman83_nooc_statetest_offline_novaluation
    $log.info 'Remove all valuations from the channel template _Template_QA_PARAMS_900 then press any key to continue.'
    gets
    assert dcr = @@siltest.cleanup_send_dcrs(:default, state: 'Offline', cas: [], valuations: nil)
    assert @@siltest.verify_aqv_message(dcr, 'success'), "wrong AQV message"    ## success ok?
  end

  def testman84_ooc_statetest_offline_novaluation
    $log.info 'Please remove all valuations from the channel template (_Template_QA_PARAMS_900) then press any key to continue.'
    gets
    assert dcr = @@siltest.cleanup_send_dcrs(:ooc, state: 'Offline', cas: [], valuations: nil)
    assert @@siltest.verify_aqv_message(dcr, 'fail'), "wrong AQV message"    ## fail ok?
  end



end
