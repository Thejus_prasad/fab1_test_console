=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: Conny Stenzel, 2012-06-27

History:
  2017-08-01 cstenzel/sfrieske, refactored all tests
=end

require_relative 'SILTest'


# DataCheck
class SIL_48 < SILTest
  @@test_defaults = {ppd: 'QA-PARAM-CHECK.01'}
  @@pd_ecp = 'QA-PARAM-CHECK-ECP.01'
  # advanced props
  @@datacheck_parameter_inline_enabled = true
  @@datacheck_parameter_setup_enabled = true
  @@missingparametercheck_inline_active = true
  @@missingparametercheck_inline_trace = true
  @@missingparametercheck_inline_notification = false
  @@missingparametercheck_setup_active = true
  @@missingparametercheck_setup_trace = true
  @@missingparametercheck_setup_notification = false
  @@missingparametercheck_monitoringspeclimits_wildcards_enabled = false
  @@autoqual_missingparametercheck_monitoringspeclimits_wildcards_enabled = false
  # not in advanced props
  @@autoqual_reporting_inline_enabled = false
  @@autoqual_reporting_setup_enabled = true

  @@missingparam_errormsg = 'Error code : MISSING_DCR_PARAMETERS'   # sent to AQV and notificatioon
  @@missingparam_memo = 'Missing Parameter: '                  # future hold claim memo
  @@no_param_uploaded = 'No parameter is uploaded to SPACE - a missing parameter check is not possible'


  def test00_setup
    $setup_ok = false
    #
    assert @@siltest.server.verify_property(:'datacheck.parameter.inline.enabled', @@datacheck_parameter_inline_enabled), 'wrong property'
    assert @@siltest.server.verify_property(:'datacheck.parameter.setup.enabled', @@datacheck_parameter_setup_enabled), 'wrong property'
    assert @@siltest.server.verify_property(:'missingparametercheck.inline.active', @@missingparametercheck_inline_active), 'wrong property'
    assert @@siltest.server.verify_property(:'missingparametercheck.inline.trace', @@missingparametercheck_inline_trace), 'wrong property'
    assert @@siltest.server.verify_property(:'missingparametercheck.inline.notification', @@missingparametercheck_inline_notification), 'wrong property'
    assert @@siltest.server.verify_property(:'missingparametercheck.setup.active', @@missingparametercheck_setup_active), 'wrong property'
    assert @@siltest.server.verify_property(:'missingparametercheck.setup.trace', @@missingparametercheck_setup_trace), 'wrong property'
    assert @@siltest.server.verify_property(:'missingparametercheck.setup.notification', @@missingparametercheck_setup_notification), 'wrong property'
    assert @@siltest.server.verify_property(:'missingparametercheck.monitoringspeclimits.wildcards.enabled', @@missingparametercheck_monitoringspeclimits_wildcards_enabled), 'wrong property'
    assert @@siltest.server.verify_property(:'autoqual.missingparametercheck.monitoringspeclimits.wildcards.enabled', @@autoqual_missingparametercheck_monitoringspeclimits_wildcards_enabled), 'wrong property'
    #
    assert @@lots = $svtest.new_lots(2), 'error creating test lots'
    @@testlots += @@lots
    $log.info "using lots #{@@lots}"
    #
    assert @@siltest.set_defaults(@@test_defaults.merge(lot: @@lots[0]))
    #
    ##[@@lds, @@lds_setup].each {|lds| assert lds.folder(@@templates_folder).spc_channel(name: @@params_template), 'missing template'}
    #
    $setup_ok = true
  end

  # LDS Setup

  def test01_simple
    assert dcr = @@siltest.submit_process_dcr(nocharting: false, technology: 'TEST')
    assert @@siltest.verify_channels(dcr) {|channel, sample|
      ['LSL', 'TARGET', 'USL'].each {|k| refute_nil sample.specs[k]}
      ['MEAN_VALUE_UCL', 'MEAN_VALUE_CENTER', 'MEAN_VALUE_LCL'].each {|k| refute_nil sample.limits[k]}
    }
  end

  def test03_lot
    dcr = send_missingparam([], 'TEST', lotparams: @@siltest.parameters[1..-1])
    # TODO: move lotparams channel verification here?
    check_missingparam_actions(dcr, 'fail', missingparameter: @@siltest.parameters[0])
  end

  def test04_lot_wafer
    dcr = send_missingparam(@@siltest.parameters[1..-2], 'TEST', lotparams: [@@siltest.parameters.last])
    check_missingparam_actions(dcr, 'fail', missingparameter: @@siltest.parameters[0])
  end


  # PCP (Spec Limits) spec: C05-00001650 (1802x)

  def test11_missingparameter_PCP
    [false, true].each {|multilot|
      $log.info "\n-- multilot: #{multilot}"
      parameters = @@siltest.parameters.collect {|p| "#{p}_TC1802a"}
      dcr = send_missingparam(parameters[1..-1], 'TEST', op: 'QA-PARAM-CHECK-TC1802a.01', readings: +18000, multilot: multilot)
      check_missingparam_actions(dcr, 'fail', multilot: multilot, missingparameter: parameters[0])
    }
  end

  def test12_CALCULATION
    #ignorecontrolplan.spacescenariosettings.enabled: 'true'
    #speclimits.ignorecontrolplan.spacescenariosettings: '%MANUAL_SPECLIMIT_SPACE%,%CALCULATION%,%IGNORE_ALL_PARAMETER_CHECK%,%BACKGROUND%,%Background%,%ManSL%'
    #missingparametercheck.ignorecontrolplan.spacescenariosettings: 'NoMP','MANUAL_SPECLIMIT_SPACE','CALCULATION','IGNORE_ALL_PARAMETER_CHECK','BACKGROUND','B','Background'
    #param QA_PARAM_900_TC1802b space scenario set to CALCULATION - no missing parameter expected
    [false, true].each {|multilot|
      $log.info "\n-- multilot: #{multilot}"
      parameters = @@siltest.parameters.collect {|p| "#{p}_TC1802b"}
      dcr = send_missingparam(parameters[1..-1], 'TEST', op: 'QA-PARAM-CHECK-TC1802b.01', readings: +18000, multilot: multilot)
      check_missingparam_actions(dcr, 'success', parameter_missing: false, multilot: multilot)
    }
  end

  def test13_spacescenario_notempty
    #ignorecontrolplan.spacescenariosettings.enabled: 'true'
    #speclimits.ignorecontrolplan.spacescenariosettings: '%MANUAL_SPECLIMIT_SPACE%,%CALCULATION%,%IGNORE_ALL_PARAMETER_CHECK%,%BACKGROUND%,%Background%,%ManSL%'
    #missingparametercheck.ignorecontrolplan.spacescenariosettings: 'NoMP','MANUAL_SPECLIMIT_SPACE','CALCULATION','IGNORE_ALL_PARAMETER_CHECK','BACKGROUND','B','Background'
    #param QA_PARAM_900_TC1802c space scenario set to NoCC - missing parameter expected
    [false, true].each {|multilot|
      $log.info "\n-- multilot: #{multilot}"
      parameters = @@siltest.parameters.collect {|p| "#{p}_TC1802c"}
      dcr = send_missingparam(parameters[1..-1], 'TEST', op: 'QA-PARAM-CHECK-TC1802c.01', readings: +18000, multilot: multilot)
      check_missingparam_actions(dcr, 'fail', multilot: multilot, missingparameter: parameters[0])
    }
  end

  # not testable: B not possible in SetupFC due to catalyst restrictions
  def testdev14_spacescenario_B
    #ignorecontrolplan.spacescenariosettings.enabled: 'true'
    #speclimits.ignorecontrolplan.spacescenariosettings: '%MANUAL_SPECLIMIT_SPACE%,%CALCULATION%,%IGNORE_ALL_PARAMETER_CHECK%,%BACKGROUND%,%Background%,%ManSL%'
    #missingparametercheck.ignorecontrolplan.spacescenariosettings: 'NoMP','MANUAL_SPECLIMIT_SPACE','CALCULATION','IGNORE_ALL_PARAMETER_CHECK','BACKGROUND','B','Background'
    #param QA_PARAM_900_TC1802d space scenario set to B - no missing parameter expected
    [false, true].each {|multilot|
      $log.info "\n-- multilot: #{multilot}"
      parameters = @@siltest.parameters.collect {|p| "#{p}_TC1802d"}
      dcr = send_missingparam(parameters[1..-1], 'TEST', op: 'QA-PARAM-CHECK-TC1802d.01', readings: +18000, multilot: multilot)
      check_missingparam_actions(dcr, 'success', parameter_missing: false, multilot: multilot)
    }
  end

  def test15_spacescenario_BACKGROUND
    #ignorecontrolplan.spacescenariosettings.enabled: 'true'
    #speclimits.ignorecontrolplan.spacescenariosettings: '%MANUAL_SPECLIMIT_SPACE%,%CALCULATION%,%IGNORE_ALL_PARAMETER_CHECK%,%BACKGROUND%,%Background%,%ManSL%'
    #missingparametercheck.ignorecontrolplan.spacescenariosettings: 'NoMP','MANUAL_SPECLIMIT_SPACE','CALCULATION','IGNORE_ALL_PARAMETER_CHECK','BACKGROUND','B','Background'
    #param QA_PARAM_900_TC1802e space scenario set to BACKGROUND - no missing parameter expected
    [false, true].each {|multilot|
      $log.info "\n-- multilot: #{multilot}"
      parameters = @@siltest.parameters.collect {|p| "#{p}_TC1802e"}
      dcr = send_missingparam(parameters[1..-1], 'TEST', op: 'QA-PARAM-CHECK-TC1802e.01', readings: +18000, multilot: multilot)
      check_missingparam_actions(dcr, 'success', parameter_missing: false, multilot: multilot)
    }
  end

  def test16_spacescenario_criticality_B
    #ignorecontrolplan.spacescenariosettings.enabled: 'true'
    #speclimits.ignorecontrolplan.spacescenariosettings: '%MANUAL_SPECLIMIT_SPACE%,%CALCULATION%,%IGNORE_ALL_PARAMETER_CHECK%,%BACKGROUND%,%Background%,%ManSL%'
    #missingparametercheck.ignorecontrolplan.spacescenariosettings: 'NoMP','MANUAL_SPECLIMIT_SPACE','CALCULATION','IGNORE_ALL_PARAMETER_CHECK','BACKGROUND','B','Background'
    #ignore.background.criticality: 'true'
    #ignore.criticality.values: B,BACKGROUND
    #no missing parameters expected
    [false, true].each {|multilot|
      $log.info "\n-- multilot: #{multilot}"
      parameters = @@siltest.parameters.collect {|p| "#{p}_TC1802f"}
      dcr = send_missingparam(parameters[1..-1], 'TEST', op: 'QA-PARAM-CHECK-TC1802f.01', readings: +18000, multilot: multilot)
      check_missingparam_actions(dcr, 'success', parameter_missing: false, multilot: multilot)
    }
  end


  def test21_all_nocharting
    dcr = send_missingparam(@@siltest.parameters[1..-1], 'TEST', nochartingparams: @@siltest.parameters[1..-1])
    check_missingparam_actions(dcr, 'fail', parameter_missing: false, aqvcomment: @@no_param_uploaded)
  end

  def test22_missing_nocharting
    dcr = send_missingparam(@@siltest.parameters[1..-1], 'TEST', nochartingparams: @@siltest.parameters[0])
    check_missingparam_actions(dcr, 'fail', parameter_missing: true, missingparameter: @@siltest.parameters[0])
  end

  def test23_missing_string
    dcr = send_missingparam(@@siltest.parameters[1..-1], 'TEST', stringparams: @@siltest.parameters[0])
    check_missingparam_actions(dcr, 'fail', parameter_missing: true, missingparameter: @@siltest.parameters[0])
  end

  # must behave like test21_all_nocharting
  def test24_nocharting_string
    dcr = send_missingparam([], 'TEST', nochartingparams: @@siltest.parameters[1..-1], stringparams: @@siltest.parameters[0])
    check_missingparam_actions(dcr, 'fail', parameter_missing: false, aqvcomment: @@no_param_uploaded)
  end


  # ECP (Monitoring Spec Limits) spec: C05-00001658 (1810x)

  def test31_missingparameter_ECP
    [false, true].each {|multilot|
      $log.info "\n-- multilot: #{multilot}"
      parameters = @@siltest.parameters.collect {|p| "#{p}_TC1810a"}
      dcr = send_missingparam(parameters[1..-1], 'TEST', op: 'QA-PARAM-CHECK-TC1810a.01', readings: +18000, multilot: multilot)
      check_missingparam_actions(dcr, 'fail', multilot: multilot, missingparameter: parameters[0])
    }
  end

  def test32_CALCULATION
    #ignorecontrolplan.spacescenariosettings.enabled: 'true'
    #speclimits.ignorecontrolplan.spacescenariosettings: '%MANUAL_SPECLIMIT_SPACE%,%CALCULATION%,%IGNORE_ALL_PARAMETER_CHECK%,%BACKGROUND%,%Background%,%ManSL%'
    #missingparametercheck.ignorecontrolplan.spacescenariosettings: 'NoMP','MANUAL_SPECLIMIT_SPACE','CALCULATION','IGNORE_ALL_PARAMETER_CHECK','BACKGROUND','B','Background'
    #param QA_PARAM_900_TC1810b space scenario set to CALCULATION - no missing parameter expected
    [false, true].each {|multilot|
      $log.info "\n-- multilot: #{multilot}"
      parameters = @@siltest.parameters.collect {|p| "#{p}_TC1810b"}
      dcr = send_missingparam(parameters[1..-1], 'TEST', op: 'QA-PARAM-CHECK-TC1810b.01', readings: +18000, multilot: multilot)
      check_missingparam_actions(dcr, 'success', parameter_missing: false, multilot: multilot)
    }
  end

  def test33_spacescenario_notempty
    #ignorecontrolplan.spacescenariosettings.enabled: 'true'
    #speclimits.ignorecontrolplan.spacescenariosettings: '%MANUAL_SPECLIMIT_SPACE%,%CALCULATION%,%IGNORE_ALL_PARAMETER_CHECK%,%BACKGROUND%,%Background%,%ManSL%'
    #missingparametercheck.ignorecontrolplan.spacescenariosettings: 'NoMP','MANUAL_SPECLIMIT_SPACE','CALCULATION','IGNORE_ALL_PARAMETER_CHECK','BACKGROUND','B','Background'
    #param QA_PARAM_900_TC1810c space scenario set to NoCC - missing parameter expected
    [false, true].each {|multilot|
      $log.info "\n-- multilot: #{multilot}"
      parameters = @@siltest.parameters.collect {|p| "#{p}_TC1810c"}
      dcr = send_missingparam(parameters[1..-1], 'TEST', op: 'QA-PARAM-CHECK-TC1810c.01', readings: +18000, multilot: multilot)
      check_missingparam_actions(dcr, 'fail', multilot: multilot, missingparameter: parameters[0])
    }
  end

  def test34_spacescenario_NoMP
    #ignorecontrolplan.spacescenariosettings.enabled: 'true'
    #speclimits.ignorecontrolplan.spacescenariosettings: '%MANUAL_SPECLIMIT_SPACE%,%CALCULATION%,%IGNORE_ALL_PARAMETER_CHECK%,%BACKGROUND%,%Background%,%ManSL%'
    #missingparametercheck.ignorecontrolplan.spacescenariosettings: 'NoMP','MANUAL_SPECLIMIT_SPACE','CALCULATION','IGNORE_ALL_PARAMETER_CHECK','BACKGROUND','B','Background'
    #param QA_PARAM_900_TC1810d space scenario set to NoMP - no missing parameter expected
    [false, true].each {|multilot|
      $log.info "\n-- multilot: #{multilot}"
      parameters = @@siltest.parameters.collect {|p| "#{p}_TC1810d"}
      dcr = send_missingparam(parameters[1..-1], 'TEST', op: 'QA-PARAM-CHECK-TC1810d.01', readings: +18000, multilot: multilot)
      check_missingparam_actions(dcr, 'success', parameter_missing: false, multilot: multilot)
    }
  end

  def test35_spacescenario_BACKGROUND
    #ignorecontrolplan.spacescenariosettings.enabled: 'true'
    #speclimits.ignorecontrolplan.spacescenariosettings: '%MANUAL_SPECLIMIT_SPACE%,%CALCULATION%,%IGNORE_ALL_PARAMETER_CHECK%,%BACKGROUND%,%Background%,%ManSL%'
    #missingparametercheck.ignorecontrolplan.spacescenariosettings: 'NoMP','MANUAL_SPECLIMIT_SPACE','CALCULATION','IGNORE_ALL_PARAMETER_CHECK','BACKGROUND','B','Background'
    #param QA_PARAM_900_TC1810e space scenario set to BACKGROUND - no missing parameter expected
    [false, true].each {|multilot|
      $log.info "\n-- multilot: #{multilot}"
      parameters = @@siltest.parameters.collect {|p| "#{p}_TC1810e"}
      dcr = send_missingparam(parameters[1..-1], 'TEST', op: 'QA-PARAM-CHECK-TC1810e.01', readings: +18000, multilot: multilot)
      check_missingparam_actions(dcr, 'success', parameter_missing: false, multilot: multilot)
    }
  end

  def test36_spacescenario_criticality_B
    #ignorecontrolplan.spacescenariosettings.enabled: 'true'
    #speclimits.ignorecontrolplan.spacescenariosettings: '%MANUAL_SPECLIMIT_SPACE%,%CALCULATION%,%IGNORE_ALL_PARAMETER_CHECK%,%BACKGROUND%,%Background%,%ManSL%'
    #missingparametercheck.ignorecontrolplan.spacescenariosettings: 'NoMP','MANUAL_SPECLIMIT_SPACE','CALCULATION','IGNORE_ALL_PARAMETER_CHECK','BACKGROUND','B','Background'
    #ignore.background.criticality: 'true'
    #ignore.criticality.values: B,BACKGROUND
    #no missing parameters expected
    [false, true].each {|multilot|
      $log.info "\n-- multilot: #{multilot}"
      parameters = @@siltest.parameters.collect {|p| "#{p}_TC1810f"}
      dcr = send_missingparam(parameters[1..-1], 'TEST', op: 'QA-PARAM-CHECK-TC1810f.01', readings: +18000, multilot: multilot)
      check_missingparam_actions(dcr, 'success', parameter_missing: false, multilot: multilot)
    }
  end

  def test41_missingparameter_wildcard
    #ignorecontrolplan.spacescenariosettings.enabled: 'true'
    #speclimits.ignorecontrolplan.spacescenariosettings: '%MANUAL_SPECLIMIT_SPACE%,%CALCULATION%,%IGNORE_ALL_PARAMETER_CHECK%,%BACKGROUND%,%Background%,%ManSL%'
    #missingparametercheck.ignorecontrolplan.spacescenariosettings: 'NoMP','MANUAL_SPECLIMIT_SPACE','CALCULATION','IGNORE_ALL_PARAMETER_CHECK','BACKGROUND','B','Background'
    #missingparametercheck.monitoringspeclimits.wildcards.enabled: 'true'
    #autoqual.missingparametercheck.monitoringspeclimits.wildcards.enabled: 'false'
    #different entries for ptool with and without wildcard
    #only one parameter without ptool entry is sent
    #missing param for all params (incl. *) (log, derdack, hold) expected if missingparametercheck.monitoringspeclimits.wildcards.enabled: 'true'
    #missing param for all params (incl. *) (aqv) expected if autoqual.missingparametercheck.monitoringspeclimits.wildcards.enabled: 'true'
    #if config switches = false -> missing param checks for params without wildcards expected only
    parameters = @@siltest.parameters.collect {|p| "#{p}_TC1810g"}
    dcr = send_missingparam([parameters.last], 'TEST', op: 'QA-PARAM-CHECK-TC1810g.01', readings: +18000)
    check_missingparam_actions(dcr, 'fail', missingparameter: parameters[0])
    $log.info ' ~?~?~ Please test each *wildcard* combination and check details manually! ~?~?~ '
  end

  def test42_missingparameter_lot_wildcard
    #ignorecontrolplan.spacescenariosettings.enabled: 'true'
    #speclimits.ignorecontrolplan.spacescenariosettings: '%MANUAL_SPECLIMIT_SPACE%,%CALCULATION%,%IGNORE_ALL_PARAMETER_CHECK%,%BACKGROUND%,%Background%,%ManSL%'
    #missingparametercheck.ignorecontrolplan.spacescenariosettings: 'NoMP','MANUAL_SPECLIMIT_SPACE','CALCULATION','IGNORE_ALL_PARAMETER_CHECK','BACKGROUND','B','Background'
    #missingparametercheck.monitoringspeclimits.wildcards.enabled: 'true'
    #autoqual.missingparametercheck.monitoringspeclimits.wildcards.enabled: 'false'
    #different entries for ptool with and without wildcard
    #only one parameter without ptool entry is sent
    #missing param for all params (incl. *) (log, derdack, hold) expected if missingparametercheck.monitoringspeclimits.wildcards.enabled: 'true'
    #missing param for all params (incl. *) (aqv) expected if autoqual.missingparametercheck.monitoringspeclimits.wildcards.enabled: 'true'
    #if config switches = false -> missing param checks for params without wildcards expected only
    parameters = @@siltest.parameters.collect {|p| "#{p}_TC1810g"}
    dcr = send_missingparam([], 'TEST', op: 'QA-PARAM-CHECK-TC1810g.01', readings: +18000, lotparams: [parameters.last], lotparameter_value: 18050.0)
    check_missingparam_actions(dcr, 'fail', missingparameter: parameters[0])
    $log.info ' ~?~?~ Please test each *wildcard* combination and check details manually! ~?~?~ '
  end

  def test43_missingparameter_wafer_lot_wildcard
    #ignorecontrolplan.spacescenariosettings.enabled: 'true'
    #speclimits.ignorecontrolplan.spacescenariosettings: '%MANUAL_SPECLIMIT_SPACE%,%CALCULATION%,%IGNORE_ALL_PARAMETER_CHECK%,%BACKGROUND%,%Background%,%ManSL%'
    #missingparametercheck.ignorecontrolplan.spacescenariosettings: 'NoMP','MANUAL_SPECLIMIT_SPACE','CALCULATION','IGNORE_ALL_PARAMETER_CHECK','BACKGROUND','B','Background'
    #missingparametercheck.monitoringspeclimits.wildcards.enabled: 'true'
    #autoqual.missingparametercheck.monitoringspeclimits.wildcards.enabled: 'false'
    #different entries for ptool with and without wildcard
    #only one parameter without ptool entry is sent
    #missing param for all params (incl. *) (log, derdack, hold) expected if missingparametercheck.monitoringspeclimits.wildcards.enabled: 'true'
    #missing param for all params (incl. *) (aqv) expected if autoqual.missingparametercheck.monitoringspeclimits.wildcards.enabled: 'true'
    #if config switches = false -> missing param checks for params without wildcards expected only
    parameters = @@siltest.parameters.collect {|p| "#{p}_TC1810g"}
    dcr = send_missingparam([parameters[1]], 'TEST', op: 'QA-PARAM-CHECK-TC1810g.01', readings: +18000, lotparams: [parameters.last], lotparameter_value: 18050.0)
    # aqv success in case of parameters[0] and fail in case of any other param
    check_missingparam_actions(dcr, 'fail', missingparameter: parameters[0])
    $log.info ' ~?~?~ Please test each *wildcard* combination and check details manually! ~?~?~ '
  end

  def test44_missingparameter_1wafer_wildcard
    #ignorecontrolplan.spacescenariosettings.enabled: 'true'
    #speclimits.ignorecontrolplan.spacescenariosettings: '%MANUAL_SPECLIMIT_SPACE%,%CALCULATION%,%IGNORE_ALL_PARAMETER_CHECK%,%BACKGROUND%,%Background%,%ManSL%'
    #missingparametercheck.ignorecontrolplan.spacescenariosettings: 'NoMP','MANUAL_SPECLIMIT_SPACE','CALCULATION','IGNORE_ALL_PARAMETER_CHECK','BACKGROUND','B','Background'
    #missingparametercheck.monitoringspeclimits.wildcards.enabled: 'true'
    #autoqual.missingparametercheck.monitoringspeclimits.wildcards.enabled: 'false'
    #different entries for ptool with and without wildcard
    #only one parameter without ptool entry is sent
    #missing param for all params (incl. *) (log, derdack, hold) expected if missingparametercheck.monitoringspeclimits.wildcards.enabled: 'true'
    #missing param for all params (incl. *) (aqv) expected if autoqual.missingparametercheck.monitoringspeclimits.wildcards.enabled: 'true'
    #if config switches = false -> missing param checks for params without wildcards expected only
    parameters = @@siltest.parameters.collect {|p| "#{p}_TC1810g"}
    dcr = send_missingparam([parameters.last], 'TEST', op: 'QA-PARAM-CHECK-TC1810g.01', readings: {'WAFER1'=>18050})
    check_missingparam_actions(dcr, 'fail', missingparameter: parameters[0])
    $log.info ' ~?~?~ Please test each *wildcard* combination and check details manually! ~?~?~ '
  end

  # not documented
  def testdev45_multilot
    parameters = @@siltest.parameters.collect {|p| "#{p}_TC1810g"}
    dcr = send_missingparam(parameters[1..-1], 'TEST', op: 'QA-PARAM-CHECK-TC1810g.01', readings: +18000, multilot: true)
    check_missingparam_actions(dcr, 'fail', missingparameter: parameters[0])
  end


  # LDS Inline

  def test51_simple
    assert dcr = @@siltest.submit_process_dcr(nocharting: false)
    assert @@siltest.verify_channels(dcr) {|channel, sample|
      ['LSL', 'TARGET', 'USL'].each {|k| refute_nil sample.specs[k]}
      ['MEAN_VALUE_UCL', 'MEAN_VALUE_CENTER', 'MEAN_VALUE_LCL'].each {|k| refute_nil sample.limits[k]}
    }
  end

  def test52_missingparameter_wafer
    dcr = send_missingparam(@@siltest.parameters[1..-1], '32NM')
    check_missingparam_actions(dcr, 'fail', missingparameter: @@siltest.parameters[0])
  end

  def test53_missingparameter_lot
    dcr = send_missingparam([], '32NM', lotparams: @@siltest.parameters[1..-1])
    check_missingparam_actions(dcr, 'fail', missingparameter: @@siltest.parameters[0])
  end

  def test54_missingparameter_lot_wafer
    dcr = send_missingparam(@@siltest.parameters[1..-2], '32NM', lotparams: [@@siltest.parameters.last])
    check_missingparam_actions(dcr, 'fail', missingparameter: @@siltest.parameters[0])
  end

  # MSR1293015
  def test55_missingparameter_but_exclude_wafer
    parameters = ['MP1QAEXCLUDE1']
    dcr = send_missingparam(parameters, '32NM', op: 'QA-PARAM-CHECKMP1.01')
    check_missingparam_actions(dcr, 'pass', parameter_missing: false)
  end

  # MSR1293015
  def test56_missingparameter_but_exclude_wafer
    parameters = ['MP1QAEXCLUDE2MP1']
    dcr = send_missingparam(parameters, '32NM', op: 'QA-PARAM-CHECKMP2.01')
    check_missingparam_actions(dcr, 'pass', parameter_missing: false)
  end

  # MSR1293015
  def test57_missingparameter_but_exclude_wafer
    parameters = ['QAEXCLUDE3MP1']
    dcr = send_missingparam(parameters, '32NM', op: 'QA-PARAM-CHECKMP3.01')
    check_missingparam_actions(dcr, 'pass', parameter_missing: false)
  end


  # LDS Setup, ECP

  def test61_module_template_ecp
    skip "not applicable in #{$env}" if $env == 'f8stag'
    parameters = @@siltest.parameters.collect {|p| p + '_ECP'}
    assert dcr = @@siltest.submit_process_dcr(nocharting: false, ppd: @@pd_ecp, parameters: parameters, technology: 'TEST')
    assert @@siltest.verify_channels(dcr, parameters: parameters) {|channel, sample|
      ['LSL', 'TARGET', 'USL'].each {|k| refute_nil sample.specs[k]}
      ['MEAN_VALUE_UCL', 'MEAN_VALUE_CENTER', 'MEAN_VALUE_LCL'].each {|k| refute_nil sample.limits[k]}
    }
  end

  def test62_missing_parameter_wafer_active_ecp
    skip "not applicable in #{$env}" if $env == 'f8stag'
    parameters = @@siltest.parameters.collect {|p| p + '_ECP'}
    dcr = send_missingparam(parameters[1..-1], 'TEST', op: @@pd_ecp)
    check_missingparam_actions(dcr, 'fail', missingparameter: parameters[0])
  end

  def test63_missing_parameter_lot_active_ecp
    skip "not applicable in #{$env}" if $env == 'f8stag'
    parameters = @@siltest.parameters.collect {|p| p + '_ECP'}
    dcr = send_missingparam([], 'TEST', op: @@pd_ecp, lotparams: parameters[1..-1])
    check_missingparam_actions(dcr, 'fail', missingparameter: parameters[0])
  end

  def test64_missing_parameter_lot_wafer_active_ecp
    skip "not applicable in #{$env}" if $env == 'f8stag'
    parameters = @@siltest.parameters.collect {|p| p + '_ECP'}
    dcr = send_missingparam(parameters[1..-2], 'TEST', op: @@pd_ecp, lotparams: [parameters.last])
    check_missingparam_actions(dcr, 'fail', missingparameter: parameters[0])
  end

  # LDS Inline, ECP

  def test71_module_template_ecp
    skip "not applicable in #{$env}" if $env == 'f8stag'
    parameters = @@siltest.parameters.collect {|p| p + '_ECP'}
    assert dcr = @@siltest.submit_process_dcr(nocharting: false, ppd: @@pd_ecp, parameters: parameters)
    assert @@siltest.verify_channels(dcr, parameters: parameters) {|channel, sample|
      ['LSL', 'TARGET', 'USL'].each {|k| refute_nil sample.specs[k]}
      ['MEAN_VALUE_UCL', 'MEAN_VALUE_CENTER', 'MEAN_VALUE_LCL'].each {|k| refute_nil sample.limits[k]}
    }
  end

  def test72_missing_parameter_wafer_active_ecp
    skip "not applicable in #{$env}" if $env == 'f8stag'
    parameters = @@siltest.parameters.collect {|p| p + '_ECP'}
    dcr = send_missingparam(parameters[1..-1], '32NM', op: @@pd_ecp)
    check_missingparam_actions(dcr, 'fail', missingparameter: parameters[0])
  end

  def test73_missing_parameter_lot_active_ecp
    skip "not applicable in #{$env}" if $env == 'f8stag'
    parameters = @@siltest.parameters.collect {|p| p + '_ECP'}
    dcr = send_missingparam([], '32NM', op: @@pd_ecp, lotparams: parameters[1..-1])
    check_missingparam_actions(dcr, 'fail', missingparameter: parameters[0])
  end

  def test74_missing_parameter_lot_wafer_active_ecp
    skip "not applicable in #{$env}" if $env == 'f8stag'
    parameters = @@siltest.parameters.collect {|p| p + '_ECP'}
    dcr = send_missingparam(parameters[1..-2], '32NM', op: @@pd_ecp, lotparams: [parameters.last])
    check_missingparam_actions(dcr, 'fail', missingparameter: parameters[0])
  end


  # aux methods

  def send_missingparam(parameters, technology, params={})
    setup  # call TestCase#setup to clean up between multilot runs
    op = params[:op] || @@siltest.ppd
    nochartingparams = params[:nochartingparams]
    stringparams = params[:stringparams]
    lotparams = params[:lotparams]
    lds = technology == 'TEST' ? @@lds_setup : @@lds
    readings = params[:readings] || :default
    lotparameter_value = params[:lotparameter_value] || 35.0
    #
    # build and submit DCR
    dcr = SIL::DCR.new(eqp: @@siltest.ptool, eqptype: :chamber)
    dcr.add_lot(@@siltest.lot, technology: technology, op: op)
    if parameters == nochartingparams
      parameters = []
      dcr.add_parameters(nochartingparams, readings, nocharting: true)
    else
      dcr.add_parameters(parameters, readings)
      dcr.add_parameters(nochartingparams, readings, nocharting: true) if nochartingparams
      dcr.add_parameters(stringparams, readings, valuetype: 'string') if stringparams
    end
    dcr.add_parameters(lotparams, {@@siltest.lot=>lotparameter_value}, readingtype: 'Lot') if lotparams
    if params[:multilot]  ## TODO: @@lots.each ...
      assert_equal 0, $sv.lot_cleanup(@@lots[1])
      dcr.add_lot(@@lots[1], technology: technology, op: op)
      dcr.add_parameters(parameters, readings)  ## readings must be symbolic, i.e. a number, :default or :ooc
      dcr.add_parameters(nochartingparams, readings, nocharting: true) if nochartingparams
      dcr.add_parameters(stringparams, readings, valuetype: 'string') if stringparams
      dcr.add_parameters(lotparams, {@@lots[1]=>lotparameter_value}, readingtype: 'Lot') if lotparams
    end
    assert @@siltest.send_dcr_verify(dcr), 'error submitting DCR'
    # verify spc channels
    assert @@siltest.verify_channels(dcr, parameters: parameters)
    # TODO: remove, doesn't do anything in most cases because of i == 0
    assert folder = lds.folder(@@autocreated_qa)
    lotparams.each_with_index {|p, i|
      assert ch = folder.spc_channel(p), 'missing channel'
      assert_equal (params[:multilot] ? 2 : 1), ch.samples.size, "wrong number of samples for parameter #{p}, channel #{ch.name}" if i != 0
    } if lotparams
    #
    return dcr
  end

  def check_missingparam_actions(dcr, aqvresult, params={})
    $log.info "check_missingparam_actions  #{aqvresult.inspect}, #{params}"
    # get the switches relevant for the LDS
    if dcr.lot_context['technology'] == 'TEST'
      prop_datacheck = @@datacheck_parameter_setup_enabled
      prop_missingparamcheck = @@missingparametercheck_setup_active
      prop_missingparamnotification = @@missingparametercheck_setup_notification
      prop_aqvreporting = @@autoqual_reporting_setup_enabled
    else
      prop_datacheck = @@datacheck_parameter_inline_enabled
      prop_missingparamcheck = @@missingparametercheck_inline_active
      prop_missingparamnotification = @@missingparametercheck_inline_notification
      prop_aqvreporting = @@autoqual_reporting_inline_enabled
    end
    #
    parameters = dcr.parameters.collect {|e| e['name']}
    parameter_missing = params.has_key?(:parameter_missing) ? params[:parameter_missing] : parameters.size < @@siltest.parameters.size
    aqvcomment = params[:aqvcomment] || @@missingparam_errormsg
    tstart = Time.now  # shortens test duration significantly
    # for multilot and prop_aqvreporting: get AQV messages before verifying for each lot
    if params[:multilot] && prop_datacheck && prop_aqvreporting
      sleep 120
      assert aqvmsgs = @@siltest.mqaqv.read_msgs, 'no AQV messages'
    else
      aqvmsgs = nil
    end
    $aqvmsgs = aqvmsgs
    # verify for each lot
    (params[:multilot] ? @@lots : [@@siltest.lot]).each_with_index {|lot, ilot|
      if prop_datacheck
        # AQV message
        if prop_aqvreporting  # note: aqv_comment ignored in case of success
          assert @@siltest.verify_aqv_message(dcr, aqvresult, msgs: aqvmsgs, comment: aqvcomment, ilot: ilot, tstart: tstart), 'wrong AQV message'
        end
        # future hold
        if prop_missingparamcheck && parameter_missing
          $log.info "verify future hold"
          assert holds = @@siltest.wait_futurehold(@@missingparam_memo, lot: lot, tstart: tstart), 'missing future hold'
          holds.each {|h| assert h.memo.include?(params[:missingparameter]), "wrong futurehold claimmemo: #{h.memo} does not contain: #{params[:missingparameter]}"}
        else
          $log.info "verify no future hold because of missing parameters"  # other holds may be set, like Mean below.., e.g. test03
          refute @@siltest.wait_futurehold(@@missingparam_memo, lot: lot, timeout: 60, tstart: tstart), 'wrong future hold'
        end
        # notification
        if prop_missingparamnotification && parameter_missing
          $log.info "verify notification"
          assert msgs = @@siltest.wait_notification(@@missingparam_errormsg, tstart: tstart), 'notification missing'
          msgs.each {|msg| assert msg.include?(params[:missingparameter]), 'wrong notification text'}
        else
          $log.info "verify no notification"
          refute @@siltest.wait_notification(@@missingparam_errormsg, timeout: 60, tstart: tstart), 'wrong notification'
        end
      else
        $log.info '--- DataCheck disabled, check log file for NO missing parameters entries! ---'
        refute @@siltest.wait_notification(@@missingparam_errormsg, timeout: 60, tstart: tstart), 'wrong notification'
        assert_empty $sv.lot_futurehold_list(lot), 'wrong future hold'
      end
    }
  end

end
