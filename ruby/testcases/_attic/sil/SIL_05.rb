=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: Steffen Steidten, 2011-11-25

History:
  2017-08-01 cstenzel/sfrieske, refactored all tests
=end

require_relative 'SILTest'


# Test Mulitilot DCRs with multiple parameter instances are handled correctly, MSR 1017508
class SIL_05 < SILTest
  @@test_defaults = {mpd: 'MGT-NMETDEPDR.QA', ppd: :default}


  def test00_setup
    $setup_ok = false
    #
    @@advanced_props = @@siltest.server.read_jobprops
    # create test lots for TEST DCRs with wrong parameters, where SIL sets the lots ONHOLD
    assert @@lots = $svtest.new_lots(3), 'error creating test lot'
    @@testlots += @@lots
    $log.info "using lots #{@@lots}"
    #
    assert $setup_ok = @@siltest.set_defaults(@@test_defaults), 'wrong setup'
    #
    $setup_ok = true
  end

  # multilot, different technologies

  def test11_multilot
    assert dcr = @@siltest.submit_meas_dcr(lots: [:default, 'ANOTHERLOT.00']), 'error submitting DCR'
    assert @@siltest.verify_channels(dcr), 'wrong channel data'
  end

  def test12_multilot_TEST
    assert dcr = @@siltest.submit_meas_dcr(lots: [:default, 'ANOTHERLOT.00'], technology: 'TEST'), 'error submitting DCR'
    assert @@siltest.verify_channels(dcr), 'wrong channel data'
  end

  def test13_multilot_notech
    pname = :'parameters.missingspeclimit.upload.setup.enabled'
    assert prop = @@advanced_props[pname], "no such prop: #{pname.inspect}"
    if prop == 'true'
      assert dcr = @@siltest.submit_meas_dcr(lots: [:default, 'ANOTHERLOT.00']) {|dcr|
        dcr.lots_context.each {|ctx| assert ctx.delete('technology'), 'DCR setup error'}
      }, 'error submitting DCR'
      assert @@siltest.verify_channels(dcr, folder: @@lds_setup.folder(@@autocreated_qa)), 'wrong channel data'
    else
      assert dcr = @@siltest.submit_meas_dcr(nsamples: 0, lots: [:default, 'ANOTHERLOT.00']) {|dcr|
        dcr.lots_context.each {|ctx| assert ctx.delete('technology'), 'DCR setup error'}
      }, 'error submitting DCR'
    end
  end

  # was: test0503_multi_lot_tech_empty
  def test14_multilot_emptytech
    assert dcr = @@siltest.submit_meas_dcr(lots: [:default, 'ANOTHERLOT.00'], technology: ''), 'error submitting DCR'
    assert @@siltest.verify_channels(dcr, folder: @@lds.folder(@@autocreated_qa)), 'wrong channel data'
  end

  # multilot with varying parameter and reading validity

  def test21_multilot_1parameter_invalid_3instances
    parameter = @@siltest.parameters.first
    # build DCR with 3 lots, 1 parameter, separate instances per lot
    assert dcr = SIL::DCR.new
    dcr.add_lot(:default, op: @@siltest.mpd)
    readings1 = {'WAFER0101'=>41, 'WAFER0102'=>42}
    dcr.add_parameter(parameter, readings1, parameter_valid: false)
    #
    dcr.add_lot('ANOTHERLOT1.00', op: @@siltest.mpd)
    readings2 = {'WAFER0201'=>43, 'WAFER0201'=>44}
    dcr.add_parameter(parameter, readings2, newinstance: true, parameter_valid: true)
    #
    dcr.add_lot('ANOTHERLOT2.00', op: @@siltest.mpd)
    readings3 = {'WAFER0301'=>45, 'WAFER0302'=>46}
    dcr.add_parameter(parameter, readings3, newinstance: true, parameter_valid: false)
    #
    # submit DCR and verify SPC channels: none for readings1 and 3 (invalid), all for readings2
    assert @@siltest.send_dcr_verify(dcr), 'error sending DCR'
    assert folder = @@lds.folder(@@autocreated_qa), "no folder #{@@lds.inspect} #{@@autocreated_qa}"
    (@@siltest.parameters - [parameter]).each {|p| assert_nil folder.spc_channel(p), 'wrong channel'}
    assert ch = folder.spc_channel(parameter), 'missing channel'
    (readings1.keys + readings3.keys).each {|w| assert_empty ch.samples('Lot'=>w), "wrong sample for wafer #{w}"}
    # passes as of 4.7.3
    assert @@siltest.verify_channels(dcr, parameters: [parameter]), 'wrong parameter channel'
  end


  # multilot with varying parameter and reading validity, TEST

  def test31_multilot_1parameter_invalid_3instances_TEST
    parameter = @@siltest.parameters.first
    # build DCR with 3 lots, 1 parameter in separate instances per lot
    assert dcr = SIL::DCR.new
    dcr.add_lot(:default, op: @@siltest.mpd, technology: 'TEST')
    readings1 = {'WAFER0101'=>41, 'WAFER0102'=>42}
    dcr.add_parameter(parameter, readings1, parameter_valid: false)
    #
    dcr.add_lot('ANOTHERLOT1.00', op: @@siltest.mpd, technology: 'TEST')
    readings2 = {'WAFER0201'=>43, 'WAFER0201'=>44}
    dcr.add_parameter(parameter, readings2, newinstance: true)
    #
    dcr.add_lot('ANOTHERLOT2.00', op: @@siltest.mpd, technology: 'TEST')
    readings3 = {'WAFER0301'=>45, 'WAFER0302'=>46}
    dcr.add_parameter(parameter, readings3, newinstance: true, parameter_valid: false)
    #
    # submit DCR and verify SPC channels: none for readings1 and 3 (invalid), all for readings2
    assert @@siltest.send_dcr_verify(dcr), 'error sending DCR'
    assert folder = @@lds_setup.folder(@@autocreated_qa), "no folder #{@@lds.inspect} #{@@autocreated_qa}"
    (@@siltest.parameters - [parameter]).each {|p| assert_nil folder.spc_channel(p), 'wrong channel'}
    assert ch = folder.spc_channel(parameter), 'missing channel'
    (readings1.keys + readings3.keys).each {|w| assert_empty ch.samples('Wafer'=>w), "wrong sample for wafer #{w}"}
    # passes as of 4.7.3
    assert @@siltest.verify_channels(dcr, parameters: [parameter]), 'wrong parameter channel, check filter.parameter=Y and filter.measurement=Y'
    #
    # get AQV messasges and verify for each lot
    sleep 120
    assert msgs = @@siltest.mqaqv.read_msgs, 'no AQV messages'
    [0, 1, 2].each {|ilot|
      assert @@siltest.verify_aqv_message(dcr, 'fail', msgs: msgs, ilot: ilot, comment: 'MISSING_DCR_PARAMETERS'), 'wrong AQV message'
    }
  end

  # known to fail, MSR 1121489 / SI-265
  def testdev42_mixed_validity_TEST
    @@lots.each {|lot| assert_equal 0, $sv.lot_cleanup(lot)}  # need real lots because of holds ??
    # build DCR with 3 lots, parameters in separate instances per lot
    assert dcr = SIL::DCR.new
    dcr.add_lot(@@lots[0], op: @@siltest.mpd, technology: 'TEST')
    dcr.add_parameters(:default, :default, parameter_valid: false)
    #
    dcr.add_lot(@@lots[1], op: @@siltest.mpd, technology: 'TEST')
    dcr.add_parameters(:default, :default, newinstance: true)
    #
    dcr.add_lot(@@lots[2], op: @@siltest.mpd, technology: 'TEST')
    dcr.add_parameters(:default, :default, newinstance: true, reading_valid: false)
    #
    # submit DCR and verify SPC channels: none for readings1 and 3 (invalid), all for readings2
    assert @@siltest.send_dcr_verify(dcr), 'error sending DCR'
    assert @@siltest.verify_channels(dcr), 'wrong parameter channel'
    #
    # get AQV messasges and verify for each lot    wrong 'success' for lot0
    sleep 120
    assert msgs = @@siltest.mqaqv.read_msgs, 'no AQV messages'
    [0, 1, 2].each {|ilot|
      assert @@siltest.verify_aqv_message(dcr, 'fail', msgs: msgs, ilot: ilot, comment: 'MISSING_DCR_PARAMETERS'), 'wrong AQV message'
    }
  end

  # known to fail, MSR 1121489 / SI-265
  # same as test32 with OOC values
  def testdev43_mixed_validity_TEST_ooc
    @@lots.each {|lot| assert_equal 0, $sv.lot_cleanup(lot)}  # need real lots because of holds ??
    # build DCR with 3 lots, parameters in separate instances per lot
    assert dcr = SIL::DCR.new
    dcr.add_lot(@@lots[0], op: @@siltest.mpd, technology: 'TEST')
    dcr.add_parameters(:default, :ooc, parameter_valid: false)
    #
    dcr.add_lot(@@lots[1], op: @@siltest.mpd, technology: 'TEST')
    dcr.add_parameters(:default, :ooc, newinstance: true)
    #
    dcr.add_lot(@@lots[2], op: @@siltest.mpd, technology: 'TEST')
    dcr.add_parameters(:default, :ooc, newinstance: true, reading_valid: false)
    #
    # submit DCR and verify SPC channels: none for readings1 and 3 (invalid), all for readings2
    assert @@siltest.send_dcr_verify(dcr), 'error sending DCR'
    assert @@siltest.verify_channels(dcr), 'wrong parameter channel'
    #
    # get AQV messasges and verify for each lot    wrong 'success' for lot0
    sleep 120
    assert msgs = @@siltest.mqaqv.read_msgs, 'no AQV messages'
    [0, 1, 2].each {|ilot|
      assert @@siltest.verify_aqv_message(dcr, 'fail', msgs: msgs, ilot: ilot, comment: 'MISSING_DCR_PARAMETERS'), 'wrong AQV message'
    }
  end

  # known to fail, MSR 1121489 / SI-265
  # same as test32 with other parameters
  def testdev44_mixed_validity_TEST_parameters
    @@lots.each {|lot| assert_equal 0, $sv.lot_cleanup(lot)}  # need real lots because of holds ??
    parameters = ['QA_PARAM_900_TG05', 'QA_PARAM_901_TG05', 'QA_PARAM_902_TG05', 'QA_PARAM_903_TG05', 'QA_PARAM_904_TG05']
    # build DCR with 3 lots, parameters in separate instances per lot
    assert dcr = SIL::DCR.new
    dcr.add_lot(@@lots[0], op: 'QA-MultiLot-TG05.01', technology: 'TEST')
    dcr.add_parameters(parameters, :default, parameter_valid: false)
    #
    dcr.add_lot(@@lots[1], op: @@siltest.mpd, technology: 'TEST')
    dcr.add_parameters(parameters, :default, newinstance: true)
    #
    dcr.add_lot(@@lots[2], op: @@siltest.mpd, technology: 'TEST')
    dcr.add_parameters(parameters, :default, newinstance: true, reading_valid: false)
    #
    # submit DCR and verify SPC channels: none for readings1 and 3 (invalid), all for readings2
    assert @@siltest.send_dcr_verify(dcr), 'error sending DCR'
    assert @@siltest.verify_channels(dcr, parameters: parameters, template: @@default_template_qa), 'wrong parameter channel'
    #
    # get AQV messasges and verify for each lot    wrong 'success' for lot0
    sleep 120
    assert msgs = @@siltest.mqaqv.read_msgs, 'no AQV messages'
    [0, 1, 2].each {|ilot|
      assert @@siltest.verify_aqv_message(dcr, 'fail', msgs: msgs, ilot: ilot, comment: 'MISSING_DCR_PARAMETERS'), 'wrong AQV message'
    }
  end

  # known to fail, MSR 1121489 / SI-265
  # same as test33 (OOC) with other parameters
  def testdev45_mixed_validity_TEST_parameters_ooc
    @@lots.each {|lot| assert_equal 0, $sv.lot_cleanup(lot)}  # need real lots because of holds ??
    parameters = ['QA_PARAM_900_TG05', 'QA_PARAM_901_TG05', 'QA_PARAM_902_TG05', 'QA_PARAM_903_TG05', 'QA_PARAM_904_TG05']
    # build DCR with 3 lots, parameters in separate instances per lot
    assert dcr = SIL::DCR.new
    dcr.add_lot(@@lots[0], op: 'QA-MultiLot-TG05.01', technology: 'TEST')
    dcr.add_parameters(parameters, :ooc, parameter_valid: false)
    #
    dcr.add_lot(@@lots[1], op: @@siltest.mpd, technology: 'TEST')
    dcr.add_parameters(parameters, :ooc, newinstance: true)
    #
    dcr.add_lot(@@lots[2], op: @@siltest.mpd, technology: 'TEST')
    dcr.add_parameters(parameters, :ooc, newinstance: true, reading_valid: false)
    #
    # submit DCR and verify SPC channels: none for readings1 and 3 (invalid), all for readings2
    assert @@siltest.send_dcr_verify(dcr), 'error sending DCR'
    assert @@siltest.verify_channels(dcr, parameters: parameters, template: @@default_template_qa), 'wrong parameter channel'
    #
    # get AQV messasges and verify for each lot    wrong 'success' for lot0
    sleep 120
    assert msgs = @@siltest.mqaqv.read_msgs, 'no AQV messages'
    [0, 1, 2].each {|ilot|
      assert @@siltest.verify_aqv_message(dcr, 'fail', msgs: msgs, ilot: ilot, comment: 'MISSING_DCR_PARAMETERS'), 'wrong AQV message'
    }
  end

  # known to fail, ticket MTQA-2424
  # all parameters valid but valuetype string)
  def testdev46_mixed_validity_TEST_string
    @@lots.each {|lot| assert_equal 0, $sv.lot_cleanup(lot)}  # need real lots because of holds ??
    parameters = ['QA_PARAM_900_TG05', 'QA_PARAM_901_TG05', 'QA_PARAM_902_TG05', 'QA_PARAM_903_TG05', 'QA_PARAM_904_TG05']
    # build DCR with 3 lots, parameters in separate instances per lot
    assert dcr = SIL::DCR.new
    dcr.add_lot(@@lots[0], op: 'QA-MultiLot-TG05.01', technology: 'TEST')
    dcr.add_parameters(parameters, :default, valuetype: 'string')
    #
    dcr.add_lot(@@lots[1], op: @@siltest.mpd, technology: 'TEST')
    dcr.add_parameters(parameters, :default, newinstance: true)       # not string
    #
    dcr.add_lot(@@lots[2], op: @@siltest.mpd, technology: 'TEST')
    dcr.add_parameters(parameters, :default, newinstance: true, valuetype: 'string')
    #
    # submit DCR and verify SPC channels: none for readings1 and 3 (invalid), all for readings2
    assert @@siltest.send_dcr_verify(dcr), 'error sending DCR'
    assert @@siltest.verify_channels(dcr, parameters: parameters, template: @@default_template_qa), 'wrong parameter channel'
    #
    # get AQV messasges and verify for each lot    wrong 'success' for lot0
    sleep 120
    assert msgs = @@siltest.mqaqv.read_msgs, 'no AQV messages'
    [0, 2].each {|ilot|
      assert @@siltest.verify_aqv_message(dcr, 'fail', msgs: msgs, ilot: ilot, comment: 'MISSING_DCR_PARAMETERS'), 'wrong AQV message'
    }
    assert @@siltest.verify_aqv_message(dcr, 'success', msgs: msgs, ilot: 1)
  end


  # multilot lotparameter with varying parameter and reading validity
  def test51_multilot_1lotparameter_invalid_3instances
    parameter = @@siltest.parameters.first
    # build DCR with 3 lots, 1 parameter in separate instances per lot, readingtype Lot
    assert dcr = SIL::DCR.new
    dcr.add_lot(:default, op: @@siltest.mpd)
    readings1 = {SIL::DCR::DefaultLot=>21.0}
    dcr.add_parameter(parameter, readings1, readingtype: 'Lot', parameter_valid: false)
    #
    dcr.add_lot('ANOTHERLOT1.00', op: @@siltest.mpd)
    readings2 = {'ANOTHERLOT1.00'=>22.1}
    dcr.add_parameter(parameter, readings2, readingtype: 'Lot', newinstance: true, parameter_valid: true)
    #
    dcr.add_lot('ANOTHERLOT2.00', op: @@siltest.mpd)
    readings3 = {'ANOTHERLOT2.00'=>23.7}
    dcr.add_parameter(parameter, readings3, readingtype: 'Lot', newinstance: true, parameter_valid: false)
    #
    # submit DCR and verify SPC channels: none for readings1 and 3 (invalid), all for readings2
    assert @@siltest.send_dcr_verify(dcr), 'error sending DCR'
    assert folder = @@lds.folder(@@autocreated_qa), "no folder #{@@lds.inspect} #{@@autocreated_qa}"
    (@@siltest.parameters - [parameter]).each {|p| assert_nil folder.spc_channel(p), 'wrong channel'}
    assert ch = folder.spc_channel(parameter), 'missing channel'
    (readings1.keys + readings3.keys).each {|w| assert_empty ch.samples('Lot'=>w), "wrong sample for lot #{w}"}
    # passes as of 4.7.3
    assert @@siltest.verify_channels(dcr, readingtype: 'Lot', parameters: [parameter]), 'wrong parameter channel'
  end

  # multilot lotparameter with varying parameter and reading validity, TEST

  # TODO: superseded by test27?
  def test61_multilot_1lotparameter_invalid_3instances_TEST
    @@lots.each {|lot| assert_equal 0, $sv.lot_cleanup(lot)}  # need real lots because of holds
    #
    parameter = @@siltest.parameters.first
    # build DCR with 3 lots, 1 parameter in separate instances per lot, readingtype Lot
    assert dcr = SIL::DCR.new
    dcr.add_lot(@@lots[0], op: @@siltest.mpd, technology: 'TEST')
    readings1 = {@@lots[0]=>21.0}
    dcr.add_parameter(parameter, readings1, readingtype: 'Lot', parameter_valid: false)
    #
    dcr.add_lot(@@lots[1], op: @@siltest.mpd, technology: 'TEST')
    readings2 = {@@lots[1]=>22.1}
    dcr.add_parameter(parameter, readings2, readingtype: 'Lot', newinstance: true, parameter_valid: true)
    #
    dcr.add_lot(@@lots[2], op: @@siltest.mpd, technology: 'TEST')
    readings3 = {@@lots[2]=>23.7}
    dcr.add_parameter(parameter, readings3, readingtype: 'Lot', newinstance: true, parameter_valid: false)
    #
    # submit DCR and verify SPC channels: none for readings1 and 3 (invalid), all for readings2
    assert @@siltest.send_dcr_verify(dcr), 'error sending DCR'
    assert folder = @@lds_setup.folder(@@autocreated_qa), "no folder #{@@lds.inspect} #{@@autocreated_qa}"
    (@@siltest.parameters - [parameter]).each {|p| assert_nil folder.spc_channel(p), 'wrong channel'}
    assert ch = folder.spc_channel(parameter), 'missing channel'
    (readings1.keys + readings3.keys).each {|w| assert_empty ch.samples('Lot'=>w), "wrong sample for lot #{w}"}
    # passes as of 4.7.3
    assert @@siltest.verify_channels(dcr, readingtype: 'Lot', parameters: [parameter]), 'wrong parameter channel'
    #
    # get AQV messasges and verify for each lot
    sleep 120
    assert msgs = @@siltest.mqaqv.read_msgs, 'no AQV messages'
    [0, 1, 2].each {|ilot|
      assert @@siltest.verify_aqv_message(dcr, 'fail', msgs: msgs, ilot: ilot, comment: 'MISSING_DCR_PARAMETERS'), 'wrong AQV message'
    }
  end

end
