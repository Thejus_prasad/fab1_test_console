=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: Conny Stenzel, 2020-06-17

History:
=end

require_relative 'SILTest'


# Remeasurement  was: Test_Space14 part 1
class SIL_21 < SILTest
  @@test_defaults = {mpd: 'QA-MB-FTDEPM.01', mtool: 'QAMeas'}
  @@flagging_delay = 5
  @@metainfo_autoflagging_multiport_enabled = 'true'
  @@metainfo_autoflagging_key = 'PinMaturity, GOF'
  @@metainfo_autoflagging_comparemethod = 'equal, lessthan'
  @@metainfo_autoflagging_value = 'invalid, 0.9'

  def test00_setup
    $setup_ok = false
    #
    assert @@siltest.server.verify_property(:'metainfo.autoflagging.multiport.enabled', @@metainfo_autoflagging_multiport_enabled), 'wrong property'
    assert @@siltest.server.verify_property(:'metainfo.autoflagging.key', @@metainfo_autoflagging_key), 'wrong property'
    assert @@siltest.server.verify_property(:'metainfo.autoflagging.comparemethod', @@metainfo_autoflagging_comparemethod), 'wrong property'
    assert @@siltest.server.verify_property(:'metainfo.autoflagging.value', @@metainfo_autoflagging_value), 'wrong property'
    #
    assert @@siltest.set_defaults(@@test_defaults)
    #
    $setup_ok = true
  end

  # Requirement 1 of MSR1507512
  def test11_readings_and_metaInfo_at_waferlevel_all_wafers_pinmaturity
    assert @@siltest.server.verify_property(:'metainfo.autoflagging', true), 'wrong property'
    technology = '32NM'
    metainfos = [['QA_PARAM_900', [{'key'=>'PinMaturity', 'value'=>'invalid'}]], 
      ['QA_PARAM_901', [{'key'=>'PinMaturity', 'value'=>'invalid'}]],
      ['QA_PARAM_902', [{'key'=>'PinMaturity', 'value'=>'invalid'}]],
      ['QA_PARAM_903', [{'key'=>'PinMaturity', 'value'=>'invalid'}]],
      ['QA_PARAM_904', [{'key'=>'PinMaturity', 'value'=>'invalid'}]]]
    assert @@siltest.submit_process_dcr(technology: technology), 'error submitting DCR'
    # send meas DCR with OOC values
    assert dcr = @@siltest.submit_meas_dcr(technology: technology, metainfos: metainfos, miforallreadings: true), 'error submitting DCR'
    
    # wait for flagging and check flags
    $log.info "waiting #{@@flagging_delay} s for flagging"; sleep @@flagging_delay
    lds = technology == 'TEST' ? @@lds_setup : @@lds
    parameters = ['QA_PARAM_900', 'QA_PARAM_901', 'QA_PARAM_902', 'QA_PARAM_903', 'QA_PARAM_904']
    assert folder = lds.folder(@@autocreated_qa)
    parameters.each {|p|
      assert ch = folder.spc_channel(p), 'no channel'
      samples = ch.samples
      samples.each {|s| assert s.iflag && s.icomment && s.icomment.include?('PinMaturity = invalid'), 'wrong autoflagging for metainfo'}
  }
  end

  # Requirement 2 of MSR1507512
  def test12_readings_and_metaInfo_at_waferlevel_some_wafers_pinmaturity
    assert @@siltest.server.verify_property(:'metainfo.autoflagging', true), 'wrong property'
    technology = '32NM'
    assert @@siltest.submit_process_dcr(technology: technology), 'error submitting DCR'
    metainfos = [['QA_PARAM_900', [{'key'=>'PinMaturity', 'value'=>'invalid'}]], ['QA_PARAM_902', [{'key'=>'GOF', 'value'=>0.5}]]]
    assert dcr = @@siltest.submit_meas_dcr(technology: technology, metainfos: metainfos), 'error submitting DCR'
    
    # wait for flagging and check flags
    $log.info "waiting #{@@flagging_delay} s for flagging"; sleep @@flagging_delay
    lds = technology == 'TEST' ? @@lds_setup : @@lds
    assert folder = lds.folder(@@autocreated_qa)
    assert ch = folder.spc_channel('QA_PARAM_900'), 'no channel'
    count = 0
    ch.samples.each {|s| count += 1 if s.iflag && s.icomment && s.icomment.include?('PinMaturity = invalid')}
    assert count == 1, 'wrong autoflagging for metainfo'
    assert ch = folder.spc_channel('QA_PARAM_902'), 'no channel'
    count = 0
    ch.samples.each {|s| count += 1 if s.iflag && s.icomment && s.icomment.include?('GOF 0.5 < 0.9')}
    assert count == 1, 'wrong autoflagging for metainfo'
    ['QA_PARAM_901', 'QA_PARAM_903', 'QA_PARAM_904'].each {|p|
      assert ch = folder.spc_channel(p), 'no channel'
      samples = ch.samples
      samples.each {|s| assert !s.iflag || !s.icomment || !s.icomment.include?('GOF 0.5 < 0.9') || !s.icomment.include?('PinMaturity = invalid'), 'wrong autoflagging for metainfo'}
    }
  end

  # Requirement 3 of MSR1507512
  def test13_readings_and_metaInfo_at_waferlevel_some_wafers_gof
    assert @@siltest.server.verify_property(:'metainfo.autoflagging', true), 'wrong property'
    technology = '32NM'
    metainfos = [['QA_PARAM_900', [{'key'=>'GOF', 'value'=>0.3}]], 
      ['QA_PARAM_901', [{'key'=>'GOF', 'value'=>0.5}]],
      ['QA_PARAM_902', [{'key'=>'GOF', 'value'=>0.7}]],
      ['QA_PARAM_903', [{'key'=>'GOF', 'value'=>0.95}]]]
    assert @@siltest.submit_process_dcr(technology: technology), 'error submitting DCR'
    # send meas DCR with OOC values
    assert dcr = @@siltest.submit_meas_dcr(technology: technology, metainfos: metainfos), 'error submitting DCR'
    
    # wait for flagging and check flags
    $log.info "waiting #{@@flagging_delay} s for flagging"; sleep @@flagging_delay
    lds = technology == 'TEST' ? @@lds_setup : @@lds
    assert folder = lds.folder(@@autocreated_qa)
    ['QA_PARAM_900', 'QA_PARAM_901', 'QA_PARAM_902'].each {|p|
      assert ch = folder.spc_channel(p), 'no channel'
      count = 0
      ch.samples.each {|s| count += 1 if s.iflag && s.icomment && s.icomment.include?('GOF 0.') && s.icomment.include?('< 0.9')}
      assert count == 1, 'wrong autoflagging for metainfo'
    }
    ['QA_PARAM_903', 'QA_PARAM_904'].each {|p|
      assert ch = folder.spc_channel(p), 'no channel'
      samples = ch.samples
      samples.each {|s| assert !s.iflag || !s.icomment || !s.icomment.include?('GOF 0.5 < 0.9'), 'wrong autoflagging for metainfo'}
    }
  end

  # Requirement 4 of MSR1507512
  def test14_readings_at_waferlevel_and_metaInfo_at_lotlevel_some_wafers_gof
    assert @@siltest.server.verify_property(:'metainfo.autoflagging', true), 'wrong property'
    technology = '32NM'
    metainfos = [['QA_PARAM_900', [{'key'=>'GOF', 'value'=>0.3}]]]
    assert @@siltest.submit_process_dcr(technology: technology), 'error submitting DCR'
    # send meas DCR with OOC values
    assert dcr = @@siltest.submit_meas_dcr(technology: technology, metainfos: metainfos, milotbased: true), 'error submitting DCR'
    
    # wait for flagging and check flags
    $log.info "waiting #{@@flagging_delay} s for flagging"; sleep @@flagging_delay
    lds = technology == 'TEST' ? @@lds_setup : @@lds
    assert folder = lds.folder(@@autocreated_qa)
    ['QA_PARAM_900'].each {|p|
      assert ch = folder.spc_channel(p), 'no channel'
      ch.samples.each {|s| assert s.iflag && s.icomment && s.icomment.include?('GOF 0.3 < 0.9'), 'wrong autoflagging for metainfo'}
    }
    ['QA_PARAM_901', 'QA_PARAM_902', 'QA_PARAM_903', 'QA_PARAM_904'].each {|p|
      assert ch = folder.spc_channel(p), 'no channel'
      samples = ch.samples
      samples.each {|s| assert !s.iflag || !s.icomment || !s.icomment.include?('GOF 0.3 < 0.9'), 'wrong autoflagging for metainfo'}
    }
  end

  # Requirement 6 of MSR1507512
  def test15_readings_and_metaInfo_at_waferlevel_some_wafers_pinmaturity_and_gof
    assert @@siltest.server.verify_property(:'metainfo.autoflagging', true), 'wrong property'
    technology = '32NM'
    assert @@siltest.submit_process_dcr(technology: technology), 'error submitting DCR'
    metainfos = [['QA_PARAM_900', [{'key'=>'PinMaturity', 'value'=>'invalid'}, {'key'=>'GOF', 'value'=>0.5}]], ['QA_PARAM_902', [{'key'=>'PinMaturity', 'value'=>'invalid'}, {'key'=>'GOF', 'value'=>0.5}]]]
    assert dcr = @@siltest.submit_meas_dcr(technology: technology, metainfos: metainfos), 'error submitting DCR'
    
    # wait for flagging and check flags
    $log.info "waiting #{@@flagging_delay} s for flagging"; sleep @@flagging_delay
    lds = technology == 'TEST' ? @@lds_setup : @@lds
    parameters = ['QA_PARAM_900', 'QA_PARAM_902']
    assert folder = lds.folder(@@autocreated_qa)
    parameters.each {|p|
      assert ch = folder.spc_channel(p), 'no channel'
      count = 0
      ch.samples.each {|s| count += 1 if s.iflag && s.icomment && s.icomment.include?('PinMaturity = invalid') && s.icomment.include?('GOF 0.5 < 0.9')}
      assert count == 1, 'wrong autoflagging for metainfo'
    }
    ['QA_PARAM_901', 'QA_PARAM_903', 'QA_PARAM_904'].each {|p|
      assert ch = folder.spc_channel(p), 'no channel'
      samples = ch.samples
      samples.each {|s| assert !s.iflag || !s.icomment || !s.icomment.include?('GOF 0.5 < 0.9') || !s.icomment.include?('PinMaturity = invalid'), 'wrong autoflagging for metainfo'}
    }
  end

  # Requirement 8 of MSR1507512
  def test16_readings_and_metaInfo_nonwip_gof_and_pinmaturity
    assert @@siltest.server.verify_property(:'metainfo.autoflagging', true), 'wrong property'

    lds = @@lds_setup
    parameters = @@siltest.parameters
    assert folder = lds.folder(@@autocreated_nonwip)
    # submit nonwip DCR
    eqp = 'QATest1'
    dcr = SIL::DCR.new(eqp: eqp, event: 'non_wip_dcr')
    dcr.context.delete(:JobSetup)
    dcr.add_parameters(:default, {eqp=>22.0}, readingtype: 'Equipment', valuetype: 'double')
    dcr.add_metainfo(parameters[0], [{'key'=>'GOF', 'value'=>0.3}, {'key'=>'PinMaturity', 'value'=>'invalid'}])
    assert @@siltest.send_dcr_verify(dcr, nsamples: @@siltest.parameters.size), 'error sending DCR'
    
    # wait for flagging and check flags
    $log.info "waiting #{@@flagging_delay} s for flagging"; sleep @@flagging_delay
    assert folder = lds.folder(@@autocreated_nonwip)
    assert ch = folder.spc_channel(parameters[0]), 'no channel'
    count = 0
    ch.samples.each {|s| count += 1 if s.iflag && s.icomment && s.icomment.include?('GOF 0.3 < 0.9') && s.icomment.include?('PinMaturity = invalid')}
    assert count == 1, 'wrong autoflagging for metainfo'
    ['QA_PARAM_901', 'QA_PARAM_903', 'QA_PARAM_904'].each {|p|
      assert ch = folder.spc_channel(p), 'no channel'
      samples = ch.samples
      samples.each {|s| assert !s.iflag || !s.icomment || !s.icomment.include?('GOF 0.3 < 0.9') || !s.icomment.include?('PinMaturity = invalid'), 'wrong autoflagging for metainfo'}
    }
  end

  # Requirement 9 of MSR1507512
  def test17_readings_at_lotlevel_and_metaInfo_at_lotlevel_some_wafers_gof
    assert @@siltest.server.verify_property(:'metainfo.autoflagging', true), 'wrong property'
    technology = '32NM'
    assert @@siltest.submit_process_dcr(technology: technology), 'error submitting DCR'
    # send meas DCR with OOC values
    parameters = @@siltest.parameters
    readings = {SIL::DCR::DefaultLot=>22.0}
    # build DCR
    dcr = SIL::DCR.new
    dcr.add_lot(:default)
    dcr.add_parameters(:default, readings, readingtype: 'Lot', valuetype: 'double')
    dcr.add_metainfo(parameters[0], [{'key'=>'GOF', 'value'=>0.3, 'lotID'=>'SPCDUMMY0.00'}, {'key'=>'PinMaturity', 'value'=>'invalid', 'lotID'=>'SPCDUMMY0.00'}])
    assert @@siltest.send_dcr_verify(dcr, technology: technology, nsamples: @@siltest.parameters.size), 'error sending DCR'
    
    # wait for flagging and check flags
    $log.info "waiting #{@@flagging_delay} s for flagging"; sleep @@flagging_delay
    lds = technology == 'TEST' ? @@lds_setup : @@lds
    assert folder = lds.folder(@@autocreated_qa)
    ['QA_PARAM_900'].each {|p|
      assert ch = folder.spc_channel(p), 'no channel'
      ch.samples.each {|s| assert s.iflag && s.icomment && s.icomment.include?('GOF 0.3 < 0.9') && s.icomment.include?('PinMaturity = invalid'), 'wrong autoflagging for metainfo'}
    }
    ['QA_PARAM_901', 'QA_PARAM_902', 'QA_PARAM_903', 'QA_PARAM_904'].each {|p|
      assert ch = folder.spc_channel(p), 'no channel'
      samples = ch.samples
      samples.each {|s| assert !s.iflag || !s.icomment || !s.icomment.include?('GOF 0.3 < 0.9') || !s.icomment.include?('PinMaturity = invalid'), 'wrong autoflagging for metainfo'}
    }
  end

  # Requirement 12 of MSR1507512
  def test18_readings_at_waferlevel_and_metaInfo_at_lotandwaferlevel_some_wafers_gof
    assert @@siltest.server.verify_property(:'metainfo.autoflagging', true), 'wrong property'
    technology = '32NM'
    metainfos = [['QA_PARAM_900', [{'key'=>'GOF', 'value'=>0.3}, {'key'=>'GOF', 'value'=>0.6}]]]
    assert @@siltest.submit_process_dcr(technology: technology), 'error submitting DCR'
    # send meas DCR with OOC values
    assert dcr = @@siltest.submit_meas_dcr(technology: technology, metainfos: metainfos, milotandwaferbased: true), 'error submitting DCR'
    
    # wait for flagging and check flags
    $log.info "waiting #{@@flagging_delay} s for flagging"; sleep @@flagging_delay
    lds = technology == 'TEST' ? @@lds_setup : @@lds
    assert folder = lds.folder(@@autocreated_qa)
    ['QA_PARAM_900'].each {|p|
      assert ch = folder.spc_channel(p), 'no channel'
      ch.samples.each {|s| assert s.iflag && s.icomment && s.icomment.include?('GOF 0.3 < 0.9'), 'wrong autoflagging for metainfo'}
    }
    ['QA_PARAM_901', 'QA_PARAM_902', 'QA_PARAM_903', 'QA_PARAM_904'].each {|p|
      assert ch = folder.spc_channel(p), 'no channel'
      samples = ch.samples
      samples.each {|s| assert !s.iflag || !s.icomment || !s.icomment.include?('GOF 0.3 < 0.9'), 'wrong autoflagging for metainfo'}
    }
  end

  # Requirement 13 of MSR1507512
  def test19_readings_and_metaInfo_at_waferlevel_unknown_key
    assert @@siltest.server.verify_property(:'metainfo.autoflagging', true), 'wrong property'
    technology = '32NM'
    metainfos = [['QA_PARAM_900', [{'key'=>'abc', 'value'=>'invalid'}]], 
      ['QA_PARAM_901', [{'key'=>'ABC', 'value'=>'invalid'}]],
      ['QA_PARAM_902', [{'key'=>'123', 'value'=>'invalid'}]],
      ['QA_PARAM_903', [{'key'=>'ÜÖÄ', 'value'=>'invalid'}]],
      ['QA_PARAM_904', [{'key'=>'&$%!', 'value'=>'invalid'}]]]
    assert @@siltest.submit_process_dcr(technology: technology), 'error submitting DCR'
    # send meas DCR with OOC values
    assert dcr = @@siltest.submit_meas_dcr(technology: technology, metainfos: metainfos, miforallreadings: true), 'error submitting DCR'
    
    # wait for flagging and check flags
    $log.info "waiting #{@@flagging_delay} s for flagging"; sleep @@flagging_delay
    lds = technology == 'TEST' ? @@lds_setup : @@lds
    parameters = ['QA_PARAM_900', 'QA_PARAM_901', 'QA_PARAM_902', 'QA_PARAM_903', 'QA_PARAM_904']
    assert folder = lds.folder(@@autocreated_qa)
    parameters.each {|p|
      assert ch = folder.spc_channel(p), 'no channel'
      samples = ch.samples
      samples.each {|s| assert !s.iflag || !s.icomment || 
        !s.icomment.include?('abc') || 
        !s.icomment.include?('ABC') || 
        !s.icomment.include?('123') || 
        !s.icomment.include?('ÜÖÄ') || 
        !s.icomment.include?('&$%!'), 'wrong autoflagging for metainfo'}
  }
  end

  # Requirement 15 of MSR1507512
  def test20_readings_and_metaInfo_at_waferlevel_metainfo_autoflagging_false
    assert @@siltest.server.verify_property(:'metainfo.autoflagging', false), 'wrong property'
    technology = '32NM'
    metainfos = [['QA_PARAM_900', [{'key'=>'PinMaturity', 'value'=>'invalid'}]], ['QA_PARAM_902', [{'key'=>'GOF', 'value'=>0.5}]]]
    assert @@siltest.submit_process_dcr(technology: technology, metainfos: metainfos, miforallreadings: true), 'error submitting DCR'
    # send meas DCR with OOC values
    assert dcr = @@siltest.submit_meas_dcr(technology: technology, metainfos: metainfos), 'error submitting DCR'
    
    # wait for flagging and check flags
    $log.info "waiting #{@@flagging_delay} s for flagging"; sleep @@flagging_delay
    lds = technology == 'TEST' ? @@lds_setup : @@lds
    parameters = ['QA_PARAM_900', 'QA_PARAM_902'] || @@siltest.parameters
    assert folder = lds.folder(@@autocreated_qa)
    assert ch = folder.spc_channel('QA_PARAM_900'), 'no channel'
    samples = ch.samples
    samples.each {|s| assert !s.iflag || !s.icomment || !s.icomment.include?('PinMaturity = invalid'), 'wrong autoflagging for metainfo'}
    assert ch = folder.spc_channel('QA_PARAM_902'), 'no channel'
    samples = ch.samples
    samples.each {|s| assert !s.iflag || !s.icomment || !s.icomment.include?('GOF 0.5 < 0.9'), 'wrong autoflagging for metainfo'}
  end

end
