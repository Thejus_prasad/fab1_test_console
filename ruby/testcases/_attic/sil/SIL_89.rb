=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: Conny Stenzel, 2012-06-27

History:
  2017-08-01 cstenzel/sfrieske, refactored all tests
=end

require_relative 'SILTest'

# MPC Chart release test, manual interaction required
class SIL_89 < SILTest
  @@test_defaults = {mpd: 'QA-MPCM.01', department: 'QAMPC',
    parameters: ['QA_PARAM_920_TG19', 'QA_PARAM_921_TG19', 'QA_PARAM_922_TG19', 'QA_PARAM_923_TG19', 'QA_PARAM_924_TG19']}

  def test00_setup
    assert lot = $svtest.new_lot, 'error creating test lot'
    @@testlots << lot
    assert @@siltest.set_defaults(@@test_defaults.merge(lot: lot)), 'wrong default test data'
  end

  def test11_mpc_ooc
    assert channels = @@siltest.setup_channels(cas:
      ['LotHold', 'AutoLotHold', 'ReleaseLotHold', 'CancelLotHold', 'EquipmentInhibit', 'AutoEquipmentInhibit', 'ReleaseEquipmentInhibit'])
    #
    # submit DCR
    assert dcr = @@siltest.submit_meas_dcr(ooc: true)
    refute_empty $sv.lot_futurehold_list(@@siltest.lot), 'no future hold'
    refute_empty $sv.inhibit_list(:eqp, @@siltest.ptool), 'no inhibit'
    $log.info "equipment inhibits: #{$sv.inhibit_list(:eqp, @@siltest.mtool).pretty_inspect}"
    $log.info 'Manual interaction neccessary: Open an MPC chart in AutoCreated_QAMPC (press M in Navigator) from a CKC.'
    $log.info 'Select ReleaseEquipmentInhibit and then CancelLotHold. When ready press any key to continue.'
    gets
    $log.info 'waiting 60s...'
    sleep 60
    assert_empty $sv.lot_futurehold_list(@@siltest.lot), 'wrong future hold'
    assert_empty $sv.inhibit_list(:eqp, @@siltest.ptool), 'wrong inhibit'
  end
end
