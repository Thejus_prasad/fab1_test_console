=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: Conny Stenzel, 2012-06-27

History:
  2017-08-01 cstenzel/sfrieske, refactored all tests
=end

require_relative 'SILTest'


# Spec Limits Hierarchy (ECP) - Wildcard tests for PD/Technology/Route   # was: Test_Space251
#
# An empty cell at the columns technology or route is interpreted as a wildcard. The most specified line wins.
# Empty PDs instead are allowed but result in no spec limits.
class SIL_64 < SILTest
  @@test_defaults = {department: 'QAGKGPECP'}
  @@control_limit_coef_1 = 0.4   # read from config


  def test00_setup
    $setup_ok = false
    #
    assert lot = $svtest.new_lot, 'error creating test lot'
    @@testlots << lot
    #
    assert @@siltest.set_defaults(@@test_defaults.merge(lot: lot))
    #
    $setup_ok = true
  end

  # INLINE

  def test11_speclimits_INLINE
    params = ['QA_PARAM_901_ECP', 'QA_PARAM_902_ECP']
    send_dcr_verifylimits(params, [400, 500, 600], '45NM', 'QA-SPEC-HIER-METROLOGY-04.01')
  end

  def test12_no_speclimits_INLINE
    params = ['QA_PARAM_901_ECP', 'QA_PARAM_902_ECP']
    send_dcr_verifylimits(params, [], 'xxNM', 'QA-SPEC-HIER-METROLOGY-04.01')
  end

  def test13_no_speclimits_INLINE
    params = ['QA_PARAM_901_ECP', 'QA_PARAM_902_ECP']
    send_dcr_verifylimits(params, [], '45NM', 'QA-SPEC-HIER-METROLOGY-04.01', route: 'SIL-xxx2.01')
  end

  # Duplicate - Combined test of test12 and test13
  def testman14_no_speclimits_INLINE
    params = ['QA_PARAM_901_ECP', 'QA_PARAM_902_ECP']
    send_dcr_verifylimits(params, [], 'xxNM', 'QA-SPEC-HIER-METROLOGY-04.01', route: 'SIL-xxx2.01')
  end

  def test21_speclimits_INLINE
    params = ['QA_PARAM_903_ECP', 'QA_PARAM_904_ECP']
    send_dcr_verifylimits(params, [499, 599, 699], '45NM', 'QA-SPEC-HIER-METROLOGY-04.01')
  end

  def test22_speclimits_INLINE
    params = ['QA_PARAM_903_ECP', 'QA_PARAM_904_ECP']
    send_dcr_verifylimits(params, [500, 600, 700], 'xxNM', 'QA-SPEC-HIER-METROLOGY-04.01')
  end

  def test23_speclimits_INLINE
    params = ['QA_PARAM_903_ECP', 'QA_PARAM_904_ECP']
    send_dcr_verifylimits(params, [501, 601, 701], '45NM', 'QA-SPEC-HIER-METROLOGY-04.01', route: 'SIL-xxx2.01')
  end

  def test24_speclimits_INLINE
    params = ['QA_PARAM_903_ECP', 'QA_PARAM_904_ECP']
    send_dcr_verifylimits(params, [502, 602, 702], 'xxNM', 'QA-SPEC-HIER-METROLOGY-04.01', route: 'SIL-xxx2.01')
  end

  def test25_speclimits_INLINE
    params = ['QA_PARAM_905_ECP', 'QA_PARAM_906_ECP']
    send_dcr_verifylimits(params, [600, 700, 800], '45NM', 'QA-SPEC-HIER-METROLOGY-04.01')
  end

  def test26_no_speclimits_INLINE
    params = ['QA_PARAM_905_ECP', 'QA_PARAM_906_ECP']
    send_dcr_verifylimits(params, [], 'xxNM', 'QA-SPEC-HIER-METROLOGY-04.01')
  end

  def test27_speclimits_INLINE
    params = ['QA_PARAM_905_ECP', 'QA_PARAM_906_ECP']
    send_dcr_verifylimits(params, [600, 700, 800], '45NM', 'QA-SPEC-HIER-METROLOGY-04.01', route: 'SIL-xxx2.01')
  end

  # Duplicate - Combined test of test26 and test27
  def testman28_no_speclimits_INLINE
    params = ['QA_PARAM_905_ECP', 'QA_PARAM_906_ECP']
    send_dcr_verifylimits(params, [], 'xxNM', 'QA-SPEC-HIER-METROLOGY-04.01', route: 'SIL-xxx2.01')
  end

  def test31_speclimits_INLINE
    params = ['QA_PARAM_907_ECP', 'QA_PARAM_908_ECP']
    send_dcr_verifylimits(params, [700, 800, 900], '45NM', 'QA-SPEC-HIER-METROLOGY-04.01')
  end

  def test32_speclimits_INLINE
    params = ['QA_PARAM_907_ECP', 'QA_PARAM_908_ECP']
    send_dcr_verifylimits(params, [700, 800, 900], 'xxNM', 'QA-SPEC-HIER-METROLOGY-04.01')
  end

  def test33_speclimits_INLINE
    params = ['QA_PARAM_907_ECP', 'QA_PARAM_908_ECP']
    send_dcr_verifylimits(params, [700, 800, 900], '45NM', 'QA-SPEC-HIER-METROLOGY-04.01', route: 'SIL-xxx2.01')
  end

  def test34_speclimits_INLINE
    params = ['QA_PARAM_907_ECP', 'QA_PARAM_908_ECP']
    send_dcr_verifylimits(params, [700, 800, 900], 'xxNM', 'QA-SPEC-HIER-METROLOGY-04.01', route: 'SIL-xxx2.01')
  end

  def test35_speclimits_INLINE
    params = ['QA_PARAM_909_ECP', 'QA_PARAM_910_ECP']
    send_dcr_verifylimits(params, [499, 599, 699], '45NM', 'QA-SPEC-HIER-METROLOGY-04.01')
  end

  def test36_speclimits_INLINE
    params = ['QA_PARAM_909_ECP', 'QA_PARAM_910_ECP']
    send_dcr_verifylimits(params, [500, 600, 700], 'xxNM', 'QA-SPEC-HIER-METROLOGY-04.01')
  end

  def test37_speclimits_INLINE
    params = ['QA_PARAM_909_ECP', 'QA_PARAM_910_ECP']
    send_dcr_verifylimits(params, [501, 601, 701], '45NM', 'QA-SPEC-HIER-METROLOGY-04.01', route: 'SIL-xxx2.01')
  end

  def test38_speclimits_INLINE
    params = ['QA_PARAM_909_ECP', 'QA_PARAM_910_ECP']
    send_dcr_verifylimits(params, [502, 602, 702], 'xxNM', 'QA-SPEC-HIER-METROLOGY-04.01', route: 'SIL-xxx2.01')
  end

  def test41_speclimits_INLINE
    params = ['QA_PARAM_911_ECP', 'QA_PARAM_912_ECP']
    send_dcr_verifylimits(params, [400, 500, 600], '45NM', 'QA-SPEC-HIER-METROLOGY-05.01')
  end

  def test42_speclimits_INLINE
    params = ['QA_PARAM_911_ECP', 'QA_PARAM_912_ECP']
    send_dcr_verifylimits(params, [400, 500, 600], 'xxNM', 'QA-SPEC-HIER-METROLOGY-05.01')
  end

  def test43_speclimits_INLINE
    params = ['QA_PARAM_911_ECP', 'QA_PARAM_912_ECP']
    send_dcr_verifylimits(params, [400, 500, 600], '45NM', 'QA-SPEC-HIER-METROLOGY-05.01', route: 'SIL-xxx2.01')
  end

  def test44_speclimits_INLINE
    params = ['QA_PARAM_911_ECP', 'QA_PARAM_912_ECP']
    send_dcr_verifylimits(params, [400, 500, 600], 'xxNM', 'QA-SPEC-HIER-METROLOGY-05.01', route: 'SIL-xxx2.01')
  end

  def test45_speclimits_INLINE
    params = ['QA_PARAM_913_ECP', 'QA_PARAM_914_ECP']
    send_dcr_verifylimits(params, [], '45NM', 'QA-SPEC-HIER-METROLOGY-05.01')
  end

  def test46_speclimits_INLINE
    params = ['QA_PARAM_915_ECP', 'QA_PARAM_916_ECP']
    send_dcr_verifylimits(params, [600, 700, 800], '45NM', 'QA-SPEC-HIER-METROLOGY-05.01')
  end

  def test47_speclimits_INLINE
    params = ['QA_PARAM_917_ECP', 'QA_PARAM_918_ECP']
    send_dcr_verifylimits(params, [700, 800, 900], '45NM', 'QA-SPEC-HIER-METROLOGY-05.01')
  end

  def test48_speclimits_INLINE
    params = ['QA_PARAM_919_ECP', 'QA_PARAM_920_ECP']
    send_dcr_verifylimits(params, [750, 850, 950], '45NM', 'QA-SPEC-HIER-METROLOGY-05.xx')
  end


  # SETUP

  def test51_speclimits__TEST
    params = ['QA_PARAM_901_ECP', 'QA_PARAM_902_ECP']
    send_dcr_verifylimits(params, [400, 500, 600], 'TEST', 'QA-SPEC-HIER-METROLOGY-04.01')
  end

  def test52_speclimits__TEST
    params = ['QA_PARAM_903_ECP', 'QA_PARAM_904_ECP']
    send_dcr_verifylimits(params, [500, 600, 700], 'TEST', 'QA-SPEC-HIER-METROLOGY-04.01')
  end

  def test53_speclimits__TEST
    params = ['QA_PARAM_905_ECP', 'QA_PARAM_906_ECP']
    send_dcr_verifylimits(params, [600, 700, 800], 'TEST', 'QA-SPEC-HIER-METROLOGY-04.01')
  end

  def test54_speclimits__TEST
    params = ['QA_PARAM_907_ECP', 'QA_PARAM_908_ECP']
    send_dcr_verifylimits(params, [700, 800, 900], 'TEST', 'QA-SPEC-HIER-METROLOGY-04.01')
  end

  def test55_speclimits__TEST
    params = ['QA_PARAM_911_ECP', 'QA_PARAM_912_ECP']
    specs = [400, 500, 600]
    send_dcr_verifylimits(params, [400, 500, 600], 'TEST', 'QA-SPEC-HIER-METROLOGY-05.01')
  end

  def test56_speclimits_INLINE
    params = ['QA_PARAM_913_ECP', 'QA_PARAM_914_ECP']
    send_dcr_verifylimits(params, [500, 600, 700], 'TEST', 'QA-SPEC-HIER-METROLOGY-05.01')
  end

  def test57_speclimits__TEST
    params = ['QA_PARAM_915_ECP', 'QA_PARAM_916_ECP']
    send_dcr_verifylimits(params, [600, 700, 800], 'TEST', 'QA-SPEC-HIER-METROLOGY-05.01')
  end

  def test58_speclimits__TEST
    params = ['QA_PARAM_917_ECP', 'QA_PARAM_918_ECP']
    send_dcr_verifylimits(params, [700, 800, 900], 'TEST', 'QA-SPEC-HIER-METROLOGY-05.01')
  end

  def test59_speclimits__TEST
    params = ['QA_PARAM_919_ECP', 'QA_PARAM_920_ECP']
    send_dcr_verifylimits(params, [750, 850, 950], 'TEST', 'QA-SPEC-HIER-METROLOGY-05.xx')
  end


  # Generic Key Grouping (ECP)

  def test61_speclimits_INLINE
    params = ['QA_PARAM_996_ECP', 'QA_PARAM_997_ECP']
    send_dcr_verifylimits(params, [750, 850, 950], '45NM', 'QA-MGT-NMETDEPDR.01', pg: 'QAPGECP1')
  end

  def test62_speclimits_INLINE
    params = ['QA_PARAM_996_ECP', 'QA_PARAM_997_ECP']
    send_dcr_verifylimits(params, [751, 851, 951], '32NM', 'QA-MGT-NMETDEPDR.01', pg: 'QAPGECP1')
  end

  def test63_speclimits_INLINE
    params = ['QA_PARAM_996_ECP', 'QA_PARAM_997_ECP']
    send_dcr_verifylimits(params, [752, 852, 952], '22NM', 'QA-MGT-NMETDEPDR.01', pg: 'QAPGECP1')
  end

  def test64_no_speclimits_INLINE
    params = ['QA_PARAM_996_ECP', 'QA_PARAM_997_ECP']
    send_dcr_verifylimits(params, [], '88NM', 'QA-MGT-NMETDEPDR.01', pg: 'QAPGECP1')
  end


  # Generic Keys DC (ECP)

  def test72_no_speclimits_INLINE
    params = ['QA_PARAM_996_ECP', 'QA_PARAM_997_ECP']
    send_dcr_verifylimits(params, [], '200KM', 'QA-MGT-NMETDEPDR.01')
  end

  def test73_speclimits_INLINE
    params = ['QA_PARAM_1000_ECP']
    send_dcr_verifylimits(params, [753, 853, 953], 'NoTech', 'QA-MGT-NMETDEPDR.01', route: 'NoRoute', checklimits: false)
  end

  def test74_speclimits_INLINE
    params = ['QA_PARAM_1001_ECP']
    send_dcr_verifylimits(params, [753, 853, 953], 'INLINE', 'NoRespPD.01', route: 'NoRoute', checklimits: false)
  end


  # aux methods

  def send_dcr_verifylimits(parameters, specs, technology, op, params={})
    readings = Hash[SIL::DCR::DefaultWaferValues.each_with_index.collect {|v, i| ["WAFER#{i}", v + 650]}]
    #
    checklimits = params.has_key?(:checklimits) ? params[:checklimits] : !specs.empty?
    if checklimits
      # calculate spec limits
      specs = [0, 0, 0] if specs.empty?
      specsref = Hash[['LSL', 'TARGET', 'USL'].zip(specs)]
      # .. and nooc
      mUCL = specs[1] + @@control_limit_coef_1 * (specs[2] - specs[1])
      mLCL = specs[1] - @@control_limit_coef_1 * (specs[1] - specs[0])
      oos = readings.values.inject(0) {|s, v| v < specsref['LSL'] || v > specsref['USL'] ? s + 1 : s}
      ooc = readings.values.inject(0) {|s, v| v < mLCL || v > mUCL ? s + 1 : s}
      nooc = (oos + ooc) * parameters.size
    else
      nooc = 0
    end
    # submit and verify DCR
    assert @@siltest.submit_meas_dcr(mpd: op, product: 'SILPROD2.01', productgroup: params[:pg] || '3261C',
      route: params[:route] || 'SIL-0002.01', technology: technology, parameters: parameters, readings: readings, nooc: nooc)
    #
    if checklimits
      lds = technology == 'TEST' ? @@lds_setup : @@lds
      assert folder = lds.folder('AutoCreated_QAGKGPECP')
      parameters.each {|p|
        assert ch = folder.spc_channel(p)
        assert s = ch.samples.last
        assert verify_hash(specsref, s.specs, nil, refonly: true)
      }
    end
  end

end
