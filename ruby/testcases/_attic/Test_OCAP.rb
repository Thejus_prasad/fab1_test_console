=begin 
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

This testcase checks valid state changes from SiView.    

Author: Daniel Steger, 2011-05-16
        Youssef Kaouk, 2013-03-01
=end

require "RubyTestCase"
require "ocap"
require "xsitetest"

class Test_OCAP < RubyTestCase
  Description = "simple test case for OCAP" 

  @@lot = "8XYJ32071.000"   # Need a lot candidate setup in ocap, lot keep on changing, verify with SiView.
  @@lot2 = "8XYJ32072.013"
  @@opeNo =  "2700.1200"   # Operation for processing, check against SiView if still valid, things keep on changing in SiView.
  @@eqp = "THK2101"    
  @@eqp2 = "THK2151"       # Tool used for disabled alarm
  @@eqp3 = "BST2101"       # Multiple Chamber Tool used for chamber down, chmaber need to be setup in OCAP.
  @@siview_timeout = 10
  @@timeout = 5
  # Alarm Codes
  @@code = "258"
  @@code_ignored = "100"   # codes < 128 are ignored

  # Alarm ids (configured actions in OCAP)
  @@alid_default = "456"
  @@alid_downtool = "457"
  @@alid_runninghold = "458"
  @@alid_down_hold = "459"
  @@alid_futurehold = "460"
  @@alid_disabled = "461"
  @@alid_chamberdown2 = "500"
  @@alid_chamberdown3 = "501"
  @@alid_unconf = "1000"

  
  def self.startup    
    $xstest = XsiteTest.new($env) unless $xstest and $xstest.env = $env
    $ocap = OCAP::Client.new($env) unless $ocap and $ocap.env == $env
    $ocaptest = OCAP::Test.new($env, :sv=>$xstest.sv, :derdack=>$xstest.derdack) unless $ocaptest and $ocaptest.env == $env
    $xs = $xstest.xs
    $sv = $xstest.sv
  end

  #cleanup the tool
  def cleanup(lot=@@lot)

    # clean the tool
    assert $sv.eqp_mode_offline1( @@eqp, :notify=>false ), "Cannot switch ope mode"
    $sv.eqp_cleanup(@@eqp)

    $xstest.cleanup_work_requests(@@eqp)
    $xstest.cleanup_work_requests(@@eqp3, :chambers=>true)

    
    # reset the lot operation
    #cleanup the lot
    linfo = $sv.lot_info(lot)
    $sv.lot_futurehold_cancel(lot, nil)
    $sv.lot_hold_release( lot, nil ) if linfo.status == "ONHOLD"
    linfo = $sv.lot_info(lot)
    $sv.eqp_opestart_cancel( @@eqp, linfo.cj ) if  linfo.status == "Processing"
    $sv.lot_bankin_cancel(lot) if linfo.status == "COMPLETED"        
    assert $sv.lot_opelocate( lot, @@opeNo ), "Cannot move lot to proper operation"
  end
  
  # simple scenario: 
  # 1) Send OCAP message
  # 2) Check SiView alarm history
  def test01_simple_scenario
    $setup_ok = false    
    cleanup
    $log.info "Send OCAP alarm to #{@@eqp}"

    txtime = Time.now
    $ocap.alarmrpt @@eqp, @@code, @@alid_default
    
    $log.info "Wait for alarm report..."
    
    #Need to check msg against database, alarm not logged in SIVIEW anymore - MSR573860 / CR49117.
    assert wait_for {
      $ocaptest.verify_alarm(@@eqp, :alarm_code=>@@code, :alarm_id=>@@alid_default, :from=>txtime)
    }, "No alarm for #{@@code} found!"
    assert wait_for {
      $ocaptest.verify_notification(@@eqp, @@alid_default, @@code, :from=>txtime-10)
    }, "No derdack!"
      
    $setup_ok = true
  end
  
  def test02_ignore_scenario
    $log.info "Send OCAP alarm to #{@@eqp}"

    txtime = Time.now
    $ocap.alarmrpt @@eqp, @@code_ignored, "456"
    
    $log.info "Sleep #{@@siview_timeout}"
    sleep @@siview_timeout
    
    #Need to check msg against database, alarm not logged in SIVIEW anymore - MSR573860 / CR49117.
    assert $ocaptest.verify_alarm(@@eqp, :alarm_code=>@@code_ignored, :alarm_id=>"456", :ignored=>true, :from=>txtime), "Alarm should be ignored for #{@@code_ignored}"
 end

  def test03_ocaptest_alarm_disabled
    $log.info "Send OCAP alarm to #{@@eqp}"

    txtime = Time.now
    $ocap.alarmrpt @@eqp, @@code, @@alid_disabled

    $log.info "Sleep #{@@siview_timeout}"
    sleep @@siview_timeout

    #Need to check msg against database, alarm not logged in SIVIEW anymore - MSR573860 / CR49117.
    assert $ocaptest.verify_alarm(@@eqp, :alarm_code=>@@code, :alarm_id=>@@alid_disabled, :ignored=>true, :from=>txtime), "Alarm should be ignored for #{@@code}"
  end

  def test04_ocaptest_alarm_disabled_by_tool
    $log.info "Send OCAP alarm to #{@@eqp2}"

    txtime = Time.now
    $ocap.alarmrpt @@eqp2, @@code, @@alid_futurehold
      
    $log.info "Sleep #{@@siview_timeout}"
    sleep @@siview_timeout
    
    #Need to check msg against database, alarm not logged in SIVIEW anymore - MSR573860 / CR49117.
    assert $ocaptest.verify_alarm(@@eqp2, :alarm_code=>@@code, :alarm_id=>@@alid_futurehold, :ignored=>true, :from=>txtime), "Alarm should be ignored for #{@@code}"
  end
  
  def test05_tooldown_scenario
    $log.info "Send OCAP alarm to #{@@eqp}"
    cleanup

    txtime = Time.now
    $ocap.alarmrpt @@eqp, @@code, @@alid_downtool

    $log.info "Sleep #{@@siview_timeout}"
    sleep @@siview_timeout

    assert $xstest.verify_work_request(@@eqp, "OCAP Tool Alarm"), "work request not found"
    assert $ocaptest.verify_alarm(@@eqp, :alarm_code=>@@code, :alarm_id=>@@alid_downtool, :downtool=>true, :from=>txtime), "Alarm should be ignored for #{@@code}"

    $log.info "Sleep 60s"
    sleep 60
    txtime = Time.now
    $ocap.alarmrpt @@eqp, @@code, @@alid_downtool
    assert $xstest.verify_work_request(@@eqp, "OCAP Tool Alarm"), "work request not found"
    assert $ocaptest.verify_alarm(@@eqp, :alarm_code=>@@code, :alarm_id=>@@alid_downtool, :downtool=>true, :from=>txtime), "Alarm should be ignored for #{@@code}"
  end

 
  def test06_runninghold_scenario
    cleanup
    #first setup the tool, start processing (this method will do the reservation and loads the carrier
    cj = $sv.claim_process_lot(@@lot, :eqp=>@@eqp, :claim_carrier=>true, :nounload=>true, :noopecomp=>true)
    assert_not_nil(cj, "no control job for " + @@eqp)
      
    $log.info "Send OCAP alarm to #{@@eqp}"
    txtime = Time.now
    $ocap.alarmrpt @@eqp, @@code, @@alid_runninghold
             
    $log.info "Sleep #{@@siview_timeout}"
    sleep @@siview_timeout
     
    #Need to check msg against database, alarm not logged in SIVIEW anymore - MSR573860 / CR49117.    
    assert $sv.lot_hold_list(@@lot, :reason=>"RNHL").size > 0, "lot must have running hold"
    assert $ocaptest.verify_alarm(@@eqp, :alarm_code=>@@code, :alarm_id=>@@alid_runninghold,
      :rh=>true, :cj=>cj, :lot=>@@lot, :from=>txtime), "No alarm for #{@@code} found!"
  end

  def test07_runninghold_failed
    cleanup
    cleanup(@@lot2)
    #No controljob should exist

    $log.info "Send OCAP alarm to #{@@eqp}"
    txtime = Time.now
    $ocap.alarmrpt @@eqp, @@code, @@alid_runninghold

    $log.info "Sleep #{@@siview_timeout}"
    sleep @@siview_timeout

    #Need to check msg against database, alarm not logged in SIVIEW anymore - MSR573860 / CR49117.
    assert $sv.lot_hold_list(@@lot, :reason=>"RNHL").size == 0, "lot must have no running hold"
    assert $ocaptest.verify_alarm(@@eqp, :alarm_code=>@@code, :alarm_id=>@@alid_runninghold,
      :rh=>true, :cj=>nil, :lot=>@@lot, :from=>txtime), "No alarm for #{@@code} found!"
  end
  
  def test08_runninghold_multiple_cjs
    cleanup
    cleanup(@@lot2)
    
    #first setup the tool, start processing (this method will do the reservation and loads the carrier
    cj1 = $sv.claim_process_lot(@@lot, :eqp=>@@eqp, :port=>"P1", :claim_carrier=>true, :nounload=>true, :noopecomp=>true)
    assert_not_nil(cj1, "no control job for " + @@eqp)
    
    cj2 = $sv.claim_process_lot(@@lot2, :eqp=>@@eqp, :port=>"P2", :claim_carrier=>true, :nounload=>true, :noopecomp=>true)
    assert_not_nil(cj2, "no 2nd control job for " + @@eqp)
      
    $log.info "Send OCAP alarm to #{@@eqp}"
    txtime = Time.now
    $ocap.alarmrpt @@eqp, @@code, @@alid_runninghold
             
    $log.info "Sleep #{@@siview_timeout}"
    sleep @@siview_timeout
     
    #Need to check msg against database, alarm not logged in SIVIEW anymore - MSR573860 / CR49117.    
    assert $sv.lot_hold_list(@@lot, :reason=>"RNHL").size > 0, "lot must have running hold"
    assert $sv.lot_hold_list(@@lot2, :reason=>"RNHL").size > 0, "lot must have running hold"
    assert $ocaptest.verify_alarm(@@eqp, :alarm_code=>@@code, :alarm_id=>@@alid_runninghold,
      :rh=>true, :cj=>cj1, :lot=>@@lot, :from=>txtime), "No alarm for #{@@code} found!"
    assert $ocaptest.verify_alarm(@@eqp, :alarm_code=>@@code, :alarm_id=>@@alid_runninghold,
      :rh=>true, :cj=>cj2, :lot=>@@lot2, :from=>txtime), "No alarm for #{@@code} found!"
  end

  def test09_futurehold_scenario
    cleanup
    cleanup(@@lot2)
    #first setup the tool, start processing (this method will do the reservation and loads the carrier
    cj = $sv.claim_process_lot(@@lot, :eqp=>@@eqp, :claim_carrier=>true, :nounload=>true, :noopecomp=>true)
    assert_not_nil(cj, "no control job for " + @@eqp)
      
    $log.info "Send OCAP alarm to #{@@eqp}"   
    txtime = Time.now    
    $ocap.alarmrpt @@eqp, @@code, @@alid_futurehold
             
    $log.info "Sleep #{@@siview_timeout}"
    sleep @@siview_timeout
    
    assert_equal 1, $sv.lot_futurehold_list(@@lot, :reason=>"AMHS").count, "lot must have future hold"
    assert $ocaptest.verify_alarm(@@eqp, :alarm_code=>@@code, :alarm_id=>@@alid_futurehold,
      :fh=>true, :cj=>cj, :lot=>@@lot, :from=>txtime), "No alarm for #{@@code} found!"
  end
  
  def test10_futurehold_failed
    cleanup
    cleanup(@@lot2)
    #No controljob should exist
      
    $log.info "Send OCAP alarm to #{@@eqp}"   
    txtime = Time.now    
    $ocap.alarmrpt @@eqp, @@code, @@alid_futurehold
             
    $log.info "Sleep #{@@siview_timeout}"
    sleep @@siview_timeout

    assert_equal 0, $sv.lot_futurehold_list(@@lot, :reason=>"AMHS").count, "lot must have no future hold"
    assert $ocaptest.verify_alarm(@@eqp, :alarm_code=>@@code, :alarm_id=>@@alid_futurehold,
      :fh=>true, :cj=>nil, :lot=>@@lot, :from=>txtime), "No alarm for #{@@code} found!"
  end
  
  def test11_tooldown_runninghold    
    cleanup
    #first setup the tool, start processing (this method will do the reservation and loads the carrier
    cj = $sv.claim_process_lot(@@lot, :eqp=>@@eqp, :claim_carrier=>true, :nounload=>true, :noopecomp=>true)
    assert_not_nil(cj, "no control job for " + @@eqp)

    $log.info "Send OCAP alarm to #{@@eqp}"
    txtime = Time.now
    $ocap.alarmrpt @@eqp, @@code, @@alid_down_hold

    $log.info "Sleep #{@@siview_timeout}"
    sleep @@siview_timeout

    assert $xstest.verify_work_request(@@eqp, "OCAP Tool Alarm"), "work request not found"
    assert $sv.lot_hold_list(@@lot, :reason=>"RNHL").size > 0, "lot must have running hold"
    assert $ocaptest.verify_alarm(@@eqp, :alarm_code=>@@code, :alarm_id=>@@alid_down_hold, :downtool=>true, 
      :rh=>true, :cj=>cj, :lot=>@@lot, :from=>txtime), "No alarm for #{@@code}"
  end


  def test12_single_chamber_down_scenario
    cleanup

    $log.info "Send OCAP alarm to #{@@eqp3}"
    txtime = Time.now
    $ocap.alarmrpt @@eqp3, @@code, @@alid_chamberdown2
    
    $log.info "Sleep #{@@siview_timeout}"
    sleep @@siview_timeout
    
    assert $xstest.verify_work_request(@@eqp3, "OCAP Chamber Alarm", :chamber=>"PM2"), "work request not found"
    assert $ocaptest.verify_alarm(@@eqp3, :chamber_id=>"PM2", :alarm_code=>@@code, :alarm_id=>@@alid_chamberdown2, :downchamber=>true, :from=>txtime), "Alarm should be ignored for #{@@code}"

    $log.info "Sleep 60s"
    sleep 60
    txtime = Time.now
    $ocap.alarmrpt @@eqp3, @@code, @@alid_chamberdown3
    
    $log.info "Sleep #{@@siview_timeout}"
    sleep @@siview_timeout
    
    assert $xstest.verify_work_request(@@eqp3, "OCAP Chamber Alarm", :chamber=>"PM3"), "work request not found"
    assert $ocaptest.verify_alarm(@@eqp3, :chamber=>"PM3", :alarm_code=>@@code, :alarm_id=>@@alid_chamberdown3, :downchamber=>true, :from=>txtime), "Alarm should be ignored for #{@@code}"
  end
  
  def test13_default
    $log.info "Send OCAP alarm to #{@@eqp}"
    # send some unconfigured code
    txtime = Time.now
    $ocap.alarmrpt @@eqp, @@code, @@alid_unconf
    
    $log.info "Sleep #{@@siview_timeout}"
    sleep @@siview_timeout
    
    #Need to check msg against database, alarm not logged in SIVIEW anymore - MSR573860 / CR49117.
    assert $ocaptest.verify_alarm(@@eqp, :alarm_code=>@@code, :alarm_id=>@@alid_unconf, :from=>txtime), "No alarm for #{@@code} found!"
    $log.info "Sleep 10s (extra for message)"
    sleep 10
    assert $ocaptest.verify_notification(@@eqp, @@alid_unconf, @@code, :from=>txtime-10), "No derdack!"
  end
 
  def test20_ignore_alarm_before_for_10s
    alarm = "500"   
    txtime = Time.now
    4.times { $ocap.alarmrpt @@eqp, @@code, alarm }
    sleep 10
    4.times { $ocap.alarmrpt @@eqp, @@code, alarm }
    sleep 10
    4.times { $ocap.alarmrpt @@eqp, @@code, alarm }
  
    $log.info "Sleep #{@@siview_timeout}"
    sleep @@siview_timeout
      
    assert $ocaptest.verify_alarm_count(@@eqp, 2, :alarm_code=>@@code, :alarm_id=>alarm, :from=>txtime), "Wrong alarm count!"
  end

  def test21_ignore_alarm_before_3_times    
    alarm = "501"
    # Number of total alarm calls can be divided y 4
    txtime = Time.now
    4.times { $ocap.alarmrpt @@eqp, @@code, alarm }
    sleep 10
    2.times { $ocap.alarmrpt @@eqp, @@code, alarm }
    sleep 10
    2.times { $ocap.alarmrpt @@eqp, @@code, alarm }
  
    $log.info "Sleep #{@@siview_timeout}"
    sleep @@siview_timeout
      
    assert $ocaptest.verify_alarm_count(@@eqp, 2, :alarm_code=>@@code, :alarm_id=>alarm, :from=>txtime), "Wrong alarm count!"
  end
  
  def test22_ignore_alarm_before_for_10s_and_3_times    
    alarm = "502"
    # Number of total alarm calls can be divided y 4
    txtime = Time.now
    3.times { $ocap.alarmrpt @@eqp, @@code, alarm }
    sleep 10
    2.times { $ocap.alarmrpt @@eqp, @@code, alarm }
    sleep 10
    7.times { $ocap.alarmrpt @@eqp, @@code, alarm }
  
    $log.info "Sleep #{@@siview_timeout}"
    sleep @@siview_timeout
      
    assert $ocaptest.verify_alarm_count(@@eqp, 2, :alarm_code=>@@code, :alarm_id=>alarm, :from=>txtime), "Wrong alarm count!"
  end
  
  def test23_ignore_alarm_after_for_10s
    alarm = "503"
    txtime = Time.now
    4.times { $ocap.alarmrpt @@eqp, @@code, alarm }
    sleep 11
    4.times { $ocap.alarmrpt @@eqp, @@code, alarm }
    sleep 11
    4.times { $ocap.alarmrpt @@eqp, @@code, alarm }
  
    $log.info "Sleep #{@@siview_timeout}"
    sleep @@siview_timeout
      
    assert $ocaptest.verify_alarm_count(@@eqp, 3, :alarm_code=>@@code, :alarm_id=>alarm, :from=>txtime), "Wrong alarm count!"
  end
  
  def test24_ignore_alarm_after_3_times    
    alarm = "504"
    # Number of total alarm calls can be divided y 4
    txtime = Time.now
    4.times { $ocap.alarmrpt @@eqp, @@code, alarm }
    sleep 10
    2.times { $ocap.alarmrpt @@eqp, @@code, alarm }
    sleep 10
    2.times { $ocap.alarmrpt @@eqp, @@code, alarm }
  
    $log.info "Sleep #{@@siview_timeout}"
    sleep @@siview_timeout
      
    assert $ocaptest.verify_alarm_count(@@eqp, 2, :alarm_code=>@@code, :alarm_id=>alarm, :from=>txtime), "Wrong alarm count!"
  end
  
  def test25_ignore_alarm_after_for_10s_and_3_times    
    alarm = "505"
    txtime = Time.now
    3.times { $ocap.alarmrpt @@eqp, @@code, alarm }
    sleep 11
    2.times { $ocap.alarmrpt @@eqp, @@code, alarm }
    sleep 11
    7.times { $ocap.alarmrpt @@eqp, @@code, alarm }
  
    $log.info "Sleep #{@@siview_timeout}"
    sleep @@siview_timeout
      
    assert $ocaptest.verify_alarm_count(@@eqp, 3, :alarm_code=>@@code, :alarm_id=>alarm, :from=>txtime), "Wrong alarm count!"
  end
  
  # disabled by default
  def Xtest99_stress_test    
    txtime = Time.now
    1000.times { $ocap.alarmrpt "ALC2301", @@code, "777" }
    $ocaptest.verify_alarm_count "ALC2301", 1000, {:alarm_code=>"258", :alarm_id=>"777", :from=>txtime, :ca=>"OCAP_DEFAULT"}
    fail("Please check the log file for errors")
  end

end


