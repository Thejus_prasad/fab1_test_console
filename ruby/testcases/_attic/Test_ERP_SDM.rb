=begin
Test SDM Data Loader (ERP to MDS)

(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

    
Author: Steffen Steidten, 2011-01-14
=end

require "RubyTestCase"
require 'erpsdm'

# Test SDM Data Loader (ERP to MDS)
class Test_ERP_SDM < RubyTestCase

  @@testmessagefiles = "etc/testcasedata/erpsdm_e2e8"
  
  @@test_mq_connectivity = "false"
  @@ignore_loader_errors = "false"
  
  def setup
    puts "\n"
    ##super  # no SiView env
    $setup_ok = ($setup_ok != false)
    assert $setup_ok, "test setup is not correct"
  end
  
  def test01_setup
    $erpsdm = ERPSDM.new($env)
  end
  
  def test11_mq_local
    ($log.warn "skipped"; return) unless @@test_mq_connectivity == "true"
    $setup_ok = false
    text = "QA test #{Time.now.iso8601}"
    assert $erpsdm.mq.send_msg(text), "error sending local MQ message"
    sleep 10
    msgs = $erpsdm.mq_read.read_msgs(:first=>true)
    assert_equal text, msgs[-1], "error in local MQ comm"
    $setup_ok = true
  end
  
  def test12_mq_remote_to_local
    ($log.warn "skipped"; return) unless @@test_mq_connectivity == "true"
    $setup_ok = false
    text = "QA test #{Time.now.iso8601}"
    assert $erpsdm.rmq.send_msg(text), "error sending remote MQ message"
    sleep 10
    msgs = $erpsdm.mq_read.read_msgs(:first=>true)
    assert_equal text, msgs[-1], "error in local MQ comm"
    $setup_ok = true
  end
  
  def test19_mq_cleanup
    $setup_ok = false
    n = $erpsdm.mq_read.qdepth
    assert $erpsdm.mq_read.delete_msgs(n), "error cleaning queue"
    $setup_ok = true
  end
  
  def test20_sdm_loader
    $setup_ok = false
    # create a set of SDM data
    assert_equal 0, $erpsdm.mq_read.qdepth, "stale MQ messages"
    msgs = $erpsdm.read_msgs(@@testmessagefiles)
    # put the messages on the queue
    $erpsdm.new_snapshot_id
    $erpsdm.write_msgs(msgs, "MQ")
    sleep 10
    assert_equal 1, $erpsdm.mq_read.qdepth, "no MQ messages sent"
    # run the loader
    @@ts = Time.now
    res = $erpsdm.run_loader
    assert res, "loader did not run successfully" unless @@ignore_loader_errors == "true"
    $setup_ok = true
  end
  
  def test21_sdm_entries
    msgs = $erpsdm.read_msgs(@@testmessagefiles)
    assert $erpsdm.verify_records(msgs, :nil_for_empty=>true, :store_time=>@@ts)
  end

  #
  
  def self.store_time
    @@ts
  end
  
  def self.store_time=(s)
    @@ts = s
  end
  
end
