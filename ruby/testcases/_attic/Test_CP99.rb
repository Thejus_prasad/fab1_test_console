=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: Steffen Steidten, Thomas Klose, 2011-06-10
Version: 1.0.0
=end

require_relative 'Test_CP.rb'

# Test Common Platform data feed for empty reports.
#
# Requirement:
#
# Issue 22: "All feeds: empty reports should contain top-level XML elements"
#
# *	   -Feeds affected: ALL FEEDS
#
# *	   -Developed by Srikanth Gangireddy

class Test_CP99 < Test_CP

  @@product = "NOPROD.A0"

  def notest10_check_empty_feeds
    msg_types = $cp.msg_types
    msg_types.each{ |msg, dnu|
      $log.info "checking feed #{msg}"
      check_feed(msg)
    }
  end
  
  def test10_check_CP1
    feed = "wip_status"
    $log.info "testing feed #{feed}"
    $cp.update_reports(3, feed, :args=>"-PRODSPEC_ID #{@@product}")
    check_report(feed)
  end
  
  def test12_check_CP2
    feed = "reticle_runsheet"
    $log.info "testing feed #{feed}"
    $cp.get_reports(feed)
    check_report(feed)
  end

  def test14_check_CP3
    feed = "part"
    $log.info "testing feed #{feed}"
    assert $cp.update_reports(3, feed, :args=>"-PRODSPEC_ID #{@@product}")
    check_report(feed)
  end
  
  def test16_check_CP4
    feed = "lot_start"
    $log.warn "not testable feed #{feed}"
    #assert $cp.update_reports(3, feed, :args=>"-PRODSPEC_ID #{@@product}")
    #check_record(feed)
  end

  def test18_check_CP5
    feed = "fab_out"
    $log.info "testing feed #{feed}"
    assert $cp.update_reports(3, feed, :args=>"-DAYS_BACK 0")
    check_record(feed)
  end

  def test20_check_CP6
    feed = "wip_attribute"
    $log.info "testing feed #{feed}"
    assert $cp.update_reports(3, feed, :args=>"-PRODSPEC_ID #{@@product}")
    check_report(feed)
  end

  def test22_check_CP7
    feed = "lot_hold_release"
    $log.info "testing feed #{feed}"
    assert $cp.update_reports(3, feed, :args=>"-PRODSPEC_ID #{@@product}")
    check_report(feed)
  end

  def test24_check_CP8
    feed = "etest_event"
    $log.info "testing feed #{feed}"
    assert $cp.update_reports(3, feed, :args=>"-PRODSPEC_ID #{@@product}")
    check_report(feed)
  end

  def test26_check_CP9
    feed = "lot_split_merge"
    $log.info "testing feed #{feed}"
    assert $cp.update_reports(3, feed, :args=>"-LOT_ID XCVAQ% -DAYS_BACK 1", :feed=>"lot_split_wafer")
    check_report("lot_split_wafer")
  end

  def test28_check_CP10
    feed = "lot_stage_move"
    $log.warn "not testable feed #{feed}"
    #assert $cp.update_reports(3, feed, :args=>"-PRODSPEC_ID #{@@product}")
    #check_report(feed)
  end
  
  def test30_check_CP11
    feed = "lot_complete"
    $log.info "testing feed #{feed}"
    assert $cp.update_reports(3, feed, :args=>"-PRODSPEC_ID #{@@product} -DAYS_BACK 1")
    check_report(feed)
  end

  def test32_check_CP12
    feed = "lot_ship"
    $log.info "testing feed #{feed}"
    assert $cp.update_reports(3, feed, :args=>"-PRODSPEC_ID #{@@product}")
    check_report(feed)
  end

  def test34_check_CP13
    feed = "lot_ship_details"
    $log.info "testing feed #{feed}"
    assert $cp.update_reports(3, feed, :args=>"-PRODSPEC_ID #{@@product}")
    check_report(feed)
  end

  def test36_check_CP14
    feed = "lot_attribute_change"
    $log.info "testing feed #{feed}"
    assert $cp.update_reports(3, feed, :args=>"-PRODSPEC_ID #{@@product}")
    check_report(feed)
  end  
 
  def test38_check_CP15
    feed = "runsheet"
    $log.warn "not testable feed #{feed}"
    #$cp.lserver.execute(". #{$cp._loader_env}; export PATH=$PATH:/usr/local/pkg/abinitio/prod/bin; m_rm $AI_MFS_STATIC/seed_in.dat; m_touch $AI_MFS_STATIC/seed_in.dat; rm $AI_SERIAL_STATIC_RUN/runsheet.lock")
    #$cp.update_reports(1, "wip_status")    
    #assert $cp.update_reports(3, feed, "-PRODSPEC_ID #{@@product}")
    #check_report(feed)
  end  

  def test40_check_CP16
    feed = "tk_subcon_alert"
    $log.warn "testing feed #{feed}"
    assert $cp.update_reports(3, feed, :args=>"-PRODSPEC_ID #{@@product}")
    check_report(feed)
  end  

  def test42_check_CP17
    feed = "equipment_current_info"
    $log.warn "not testable feed #{feed}"
    #assert $cp.update_reports(3, feed, :args=>"-PRODSPEC_ID #{@@product}")
    #check_report(feed)
  end  

  def test44_check_CP18
    feed = "scrap"
    $log.info "testing feed #{feed}"
    assert $cp.update_reports(3, feed, :args=>"-PRODSPEC_ID #{@@product}")
    check_report(feed)
  end

  def test46_check_CP19
    feed = "plan_start"
    $log.info "testing feed #{feed}"
    assert $cp.update_reports(3, feed, :args=>"-PRODSPEC_ID #{@@product}")
    check_report(feed)
  end
  
  def test48_check_CP20
    feed = "lot_finished_goods"
    $log.warn "not testing feed #{feed}"
    #assert $cp.update_reports(3, feed, :args=>"-PRODSPEC_ID #{@@product}")
    #$cp.run_loader(@@loader)
    #$cp.get_reports_MES_Oracle()
    #check_report(feed)
  end
  
  def test50_check_CP21
    feed = "lot_rework"
    loader = "lot_rework"
    $log.info "testing feed #{feed}"
    #$cp.update_reports(3, feed, :args=>"-PRODSPEC_ID #{@@product}")
    assert $cp.run_loader(loader, "-DAYS_BACK 0")
    check_report(feed)
  end
  
  def test52_check_CP22
    feed = "source_products"
    $log.info "testing feed #{feed}"
    assert $cp.update_reports(3, feed, :args=>"-PRODSPEC_ID #{@@product}")
    check_report(feed)
  end
  
  def test54_check_CP23
    feed = "lot_history"
    $log.warn "not testable feed #{feed}"
    #$cp ||= CP.new($env, :sv=>$sv, :loadercmd=>"$AI_BIN/run_lot_history.ksh -p")
    #$sv = $cp.sv
    #assert $cp.update_reports(3, nil, :args=>"false -l ACDC.00", :feed=>feed)
    #check_record(feed)
  end

  def test56_check_CP24
    feed = "recipeidlist"
    $log.info "testing feed #{feed}"
    assert $cp.update_reports(3, feed, :args=>"-EQP_TYPE none")
    check_report(feed)
  end
    
  def test58_check_CP25
    feed = "ie"
    $log.warn "not testable feed #{feed}"
    #assert $cp.update_reports(3, feed, :args=>"-EQP_TYPE none")
    #check_report(feed)
  end
    
  def check_report(feed)
    r = $cp.get_reports(feed)
    assert r, true
  end
  
  def check_record(feed)
    #r = $cp.reports[feed].get_records_lot
    #assert r, true
  end
end