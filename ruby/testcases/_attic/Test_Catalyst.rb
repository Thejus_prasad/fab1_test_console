=begin 
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.
    
    
Author: Steffen Steidten, 2014-04-03

Version: 3.7

History:
=end

require 'RubyTestCase'
require 'catalyst'

# Test Catalyst
class Test_Catalyst < RubyTestCase
  @@lot = 'UR40230.000'
  @@eqp = 'AWS001'
  @@spec = 'C07-00000428'
  
  def self.after_all_passed
  end
  
  def test00_setup
    $setup_ok = false
    $cat = Catalyst::Client.new($env)
    assert $cat
    assert $cat.ping, "Catalyst instance #{$cat.inspect} is not working"
    $setup_ok = true
  end
  
  def test11_siview
    $setup_ok = false
    assert $cat.qasimulator(lotinfo: @@lot, futurehold: @@lot, eqpinfo: @@eqp)
    $setup_ok = true
  end
  
  def test12_load_timeout
    assert $cat.qasimulator(baselineaddon: 180)
  end
  
  def test13_MQ
    assert $cat.qasimulator(mqsend: "CEI.#{@@eqp}_REQUEST01")
  end

  def test14_setupfc
    assert $cat.qasimulator(setupfc: @@spec)
  end

end
