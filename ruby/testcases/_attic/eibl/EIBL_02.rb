=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: Conny Stenzel, 2020-04-01

History:
=end

require_relative 'EIBLTest'


# GENERIC TESTPLAN
# Error Scenarios
class EIBL_02 < EIBLTest
  ## nono, set explicitly # @@test_defaults = {}  # placeholder for test specific defaults

  def test00_setup
    $setup_ok = false
    #
    ### check EI Baseline Regression test plan (sheet Test Env Setup) for cei env setup and startup of ei
    #
    @@eibltest = EIBL::Test.new($env, @@eqp, @@eiversion, sv: @@sv)
    # assert @@eibltest.set_defaults(@@test_defaults)
    #
    $setup_ok = true
  end
  
  def test11_slot_map_verification_failed
  end
  
  def test12_cj_create_failure
  end
  
  def test13_manual_pj_abort
  end
  
  def test14_reservation_failed_recipe_retrieval_error
  end
  
  def test15_reservation_failed_wrong_dcp
  end
  
  def test16_reservation_failed_waferselection
  end
  
  def test17_cancel_all_queued_jobs
  end
  
  def test18_tool_lost_communication_during_production
  end
  
  def test19_ei_lost_communication_during_production
  end
  
  def test20_alarm_handling
  end
  
  def test21_alarm_handling_ii
  end
  
  def test22_ocap_alarm_registration_depending_on_e10_states
  end
  
  def test23_send_and_receive_timeout
  end

end
