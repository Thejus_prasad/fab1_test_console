=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: Conny Stenzel, 2020-04-01

History:
=end

require 'SiViewTestCase'
require 'eibl/eibltest'


# Common setup for EIBL tests
class EIBLTest < SiViewTestCase
  @@sv_defaults = {} # to be defined...or removed
  # @@eibltest_params = {
  #   notification_timeout: 120,
  #   lot: 'U70D1.01',
  #   eqp: 'HYB171'
  # }
  @@eqp = 'HYB171'
  @@eiversion = '10.1.0'
  @@lot = 'U70D1.01'  # TODO: remove static lot def

  @@testlots = []


  # def self.startup
  #   super
  #   @@eibltest = EIBL::Test.new($env, @@eqp, @@eiversion, sv: @@sv)
  # end

  def self.shutdown
    @@testlots.each {|lot| @@sv.delete_lot_family(lot)}
  end


  # def setup
  #   # needed or not for EI Baseline tests...? Lets try
    
  #   # cleanup eqp, route, lot
  #   ## call individually where needed: assert_equal 0, $sv.eqp_cleanup(@@siltest.ptool)
  #   #assert_equal 0, $sv.inhibit_cancel(:route, $svtest.route, silent: true)
  #   #if $sv.lot_exists?(@@siltest.lot)
  #   #  assert_equal 0, $sv.lot_cleanup(@@siltest.lot)
  #   #  assert $sv.merge_lot_family(@@siltest.lot, silent: true)  # TODO: try to get rid of it by using a dummy lot or a new lot for special tests
  #   #end
  # end

end
