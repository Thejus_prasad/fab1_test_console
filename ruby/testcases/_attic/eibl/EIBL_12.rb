=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: Conny Stenzel, 2020-04-01

History:
=end

require_relative 'EIBLTest'

# E10 TEST PLAN
# Cluster Tools (Chambers)
class EIBL_12 < EIBLTest
  ## nono, set explicitly # @@test_defaults = {}  # placeholder for test specific defaults

  def test00_setup
    $setup_ok = false
    #
    ### check EI Baseline Regression test plan (sheet Test Env Setup) for cei env setup and startup of ei
    #
    @@eibltest = EIBL::Test.new($env, @@eqp, @@eiversion, sv: @@sv)
    # assert @@eibltest.set_defaults(@@test_defaults)
    #
    $setup_ok = true
  end
  
  def test11_multiple_job_run
  end
  
  def test12_conditioning_happy_lot
  end
  
  def test13_conditioning_consumer_waits_for_provider_and_conditioningjobwaitingforstarttimeout
  end
  
  def test14_conditioning_startlotsreservation_fail_conditioning_type_missmatch
  end
  
  def test15_equipment_e10_state_change_reporting_to_space
  end
  
  def test16_siview_pcp_happy_lot_at_processoperation
  end
  
  def test17_siview_pcp_advanced_ei_service_preview_run
  end
  
  def test18_siview_pcp_happy_lot_at_measurementoperation
  end
  
  def test19_siview_pcp_messoperation_pcpsetuperrorpcpanddcpmismatch
  end

end
