=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: Conny Stenzel, 2020-04-01

History:
=end

require_relative 'EIBLTest'

# SYNCHRONIZATION TEST PLAN
# Errors on Startup
class EIBL_06 < EIBLTest
  ## nono, set explicitly # @@test_defaults = {}  # placeholder for test specific defaults

  def test00_setup
    $setup_ok = false
    #
    ### check EI Baseline Regression test plan (sheet Test Env Setup) for cei env setup and startup of ei
    #
    @@eibltest = EIBL::Test.new($env, @@eqp, @@eiversion, sv: @@sv)
    # assert @@eibltest.set_defaults(@@test_defaults)
    #
    $setup_ok = true
  end
  
  def test11_siview_portdefinition_does_not_match_tdm
  end

  def test12_siview_chamberdefinition_does_not_match_tdm
  end

end
