=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: Conny Stenzel, 2020-04-01

History:
=end

require_relative 'EIBLTest'

# SYNCHRONIZATION TEST PLAN
# Synchronization Triggers
class EIBL_04 < EIBLTest
  ## nono, set explicitly # @@test_defaults = {}  # placeholder for test specific defaults

  def test00_setup
    $setup_ok = false
    #
    ### check EI Baseline Regression test plan (sheet Test Env Setup) for cei env setup and startup of ei
    #
    @@eibltest = EIBL::Test.new($env, @@eqp, @@eiversion, sv: @@sv)
    # assert @@eibltest.set_defaults(@@test_defaults)
    #
    $setup_ok = true
  end
  
  def test11_startup
  end
  
  def test12_user_initiated_in_siview
  end
  
  def test13_incoming_tool_event_during_synchronization
  end
  
  def test14_incoming_siview_event_during_synch
  end
  
  def test15_versions_of_toolsoftware_and_cei_dont_match
  end
  
  def test16_synchronization_during_startlotsreservationscenario
  end
  
  def test17_tool_issues_portoutofserviceevents
  end

end
