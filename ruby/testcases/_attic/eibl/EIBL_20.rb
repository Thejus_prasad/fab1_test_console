=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: Conny Stenzel, 2020-04-01

History:
=end

require_relative 'EIBLTest'

# RMS Tests
# RMS
class EIBL_20 < EIBLTest
  ## nono, set explicitly # @@test_defaults = {}  # placeholder for test specific defaults

  def test00_setup
    $setup_ok = false
    #
    ### check EI Baseline Regression test plan (sheet Test Env Setup) for cei env setup and startup of ei
    #
    @@eibltest = EIBL::Test.new($env, @@eqp, @@eiversion, sv: @@sv)
    # assert @@eibltest.set_defaults(@@test_defaults)
    #
    $setup_ok = true
  end
  
  def test11_enable_rms_disable_armor
  end
  
  def test12_hyb17x_recipe_download_happy_lot_scenario
  end
  
  def test13_hyb17x_upload_and_compare_in_rms_happy_lot_scenario
  end
  
  def test14_hyb17x_reject_rms_upload_and_compare_setup_with_recipe_check_parameter
  end
  
  def test15_hyb17x_upload_and_compare_in_ei_with_rms_check_parameter
  end
  
  def test16_hyb17x_upload_and_compare_check_parameter_verification_fail
  end
  
  def test17_hyb17x_recipe_download_with_check_parameter_verification_ok
  end
  
  def test18_hyb17x_recipe_download_with_check_parameter_verification_fail
  end
  
  def test19_hybcl_conditioning_happy_lot_recipe_download
  end
  
  def test20_hybib_recipe_select
  end
  
  def test21_recipeservices_triggertransfer
  end
  
  def test22_disable_rms_enable_armor
  end

end
