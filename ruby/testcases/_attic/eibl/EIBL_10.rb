=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: Conny Stenzel, 2020-04-01

History:
=end

require_relative 'EIBLTest'

# YMS TEST PLAN
# Standard YMS - DC
class EIBL_10 < EIBLTest
  ## nono, set explicitly # @@test_defaults = {}  # placeholder for test specific defaults

  def test00_setup
    $setup_ok = false
    #
    ### check EI Baseline Regression test plan (sheet Test Env Setup) for cei env setup and startup of ei
    #
    @@eibltest = EIBL::Test.new($env, @@eqp, @@eiversion, sv: @@sv)
    # assert @@eibltest.set_defaults(@@test_defaults)
    #
    $setup_ok = true
  end
  
  def test11_opecomp_with_manual_dc_and_yms_dc
  end
  
  def test12_lot_hold_on_job_completion_when_yms_dc_timeout_occurs
  end
  
  def test13_job_competion_for_reporting_apc_data_and_yms_data
  end
  
  def test14_reporting_on_jobcomplete_with_yms_dc
  end
  
  def test15_yms_multiple_inspection
  end
  
  def test16_two_jobs_run_on_same_port
  end
  
  def test17_jobabort_with_dc_on_datacollectioncompleted_and_yms
  end
  
  def test18_jobabort_with_dc_on_datacollectioncompleted_and_yms_and_apc
  end

end
