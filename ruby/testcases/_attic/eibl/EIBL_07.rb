=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: Conny Stenzel, 2020-04-01

History:
=end

require_relative 'EIBLTest'

# DATACOLLECTION TESTPLAN
# DATACOLLECTION
class EIBL_07 < EIBLTest
  ## nono, set explicitly # @@test_defaults = {}  # placeholder for test specific defaults

  def test00_setup
    $setup_ok = false
    #
    ### check EI Baseline Regression test plan (sheet Test Env Setup) for cei env setup and startup of ei
    #
    @@eibltest = EIBL::Test.new($env, @@eqp, @@eiversion, sv: @@sv)
    # assert @@eibltest.set_defaults(@@test_defaults)
    #
    $setup_ok = true
  end
  
  def test11_apc_on_wafercomplete_spc_and_eda_on_jobcomplete
  end
  
  def test12_apc_on_wafercomplete_spc_and_eda_on_wafercomplete
  end
  
  def test13_apc_on_wafercomplete_no_other_dc
  end
  
  def test14_report_apc_feedback_data_to_other_destination
  end
  
  def test15_apc_feedback_allsites_dc_by_dcp_filter_and_per_default
  end
  
  def test16_waferselection_differs_from_lot
  end
  
  def test17_finalization_when_there_is_no_dc_plan_a
  end
  
  def test18_finalization_when_there_is_no_dc_plan_b
  end
  
  def test19_equipment_only_dcp
  end
  
  def test20_dc_completion_only_no_apc
  end
  
  def test21_jobabort_with_no_variables_on_datacollectioncomleted
  end

end
