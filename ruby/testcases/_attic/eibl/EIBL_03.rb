=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: Conny Stenzel, 2020-04-01

History:
=end

require_relative 'EIBLTest'

# GENERIC TESTPLAN
# EngineeringRecipeSelect
class EIBL_03 < EIBLTest
  ## nono, set explicitly # @@test_defaults = {}  # placeholder for test specific defaults

  def test00_setup
    $setup_ok = false
    #
    ### check EI Baseline Regression test plan (sheet Test Env Setup) for cei env setup and startup of ei
    #
    @@eibltest = EIBL::Test.new($env, @@eqp, @@eiversion, sv: @@sv)
    # assert @@eibltest.set_defaults(@@test_defaults)
    #
    $setup_ok = true
  end
  
  def test11_engineeringrecipeselect_in_auto_1
  end

  def test12_engineeringrecipeselect_in_semi_comp_1
  end

  def test13_engineeringrecipeselect_with_namefilter
  end

  def test14_engineeringrecipeselect_with_lotscriptparameter
  end

  def test15_engineeringrecipeselect_for_ib_tools
  end

  def test16_apc_previewrun_for_hybib_no_ers
  end

  def test17_engineeringrecipeselect_for_cluster_tools
  end

  def test18_apc_previewrun_for_hybcl_no_ers
  end

end
