=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: Conny Stenzel, 2020-04-01

History:
=end

require_relative 'EIBLTest'


class EIBL_00 < EIBLTest
  ## nono, set explicitly # @@test_defaults = {}  # placeholder for test specific defaults

  def test00_setup
    $setup_ok = false
    #
    ### check EI Baseline Regression test plan (sheet Test Env Setup) for cei env setup and startup of ei
    #
    @@eibltest = EIBL::Test.new($env, @@eqp, @@eiversion, sv: @@sv)

    @@eibltest.eiserver.kill_toolsims
    @@eibltest.eiserver.kill_ei
    #
    $setup_ok = true
  end
  
  def test11_ei_startup_without_tool
    # if the test started too late, log file entries maybe already there, therefore we go back 5 minutes
    tstart = Time.now.utc - 300

    @@eibltest.eiserver.kill_toolsims
    @@eibltest.eiserver.start_ei

    # versions are changing sometimes, this is the original sequence of the old EI test plan
    log_ref_x = ['AMD.CEI36.ModelLoader>>logModels =>',
      'Loading following TDMs, DCCs and TCMs:',
      'PATH: /TDM/Baseline/Repository Baseline 10.1/BasicBaselineTDM/3',
      'PATH: /TDM/Baseline/Repository Baseline 10.1/BaselineRecipeParameter TDM/4',
      'PATH: /TDM/ITDC/Repository/BASELINETEST/TDM/17',
      'PATH: /TDM/Baseline/DCC Repository/Baseline ITEM DCC/5',
      'PATH: /TDM/Baseline/DCC Repository/Baseline VPD DCC/2',
      'PATH: /TDM/Baseline/DCC Repository/Baseline YMS DCC/42',
      'PATH: /TDM/Baseline/DCC Repository/Baseline Basic DCC/14',
      'PATH: /TDM/ITDC/Repository/BASELINETEST_DCC/DCC/7',
      'PATH: /TDM/Baseline/TCM Repository/TCM Baseline/3',
      'PATH: /TDM/Baseline/TCM Repository/TCM Baseline RGA/2',
      'PATH: /TDM/Baseline/TCM Repository/TCM Baseline Targoss/1',
      'PATH: /TDM/Baseline/TCM Repository/TCM Baseline BasicGem/1',
      'PATH: /TDM/Baseline/TCM Repository/TCM Baseline 300mm/1',
      'PATH: /TDM/Baseline/TCM Repository/TCM Baseline RGA2/1',
      'PATH: /TDM/Baseline/TCM Repository/TCM Baseline ModBus/2',
      'PATH: /TDM/ITDC/Repository/BASELINETEST-TCM/TCM/1']

    # current lines found in log
    log_ref = ['AMD.CEI36.ModelLoader>>logModels =>',
      'Loading following TDMs, DCCs and TCMs:',
      'PATH: /TDM/Baseline/Repository Baseline 10.1/BasicBaselineTDM/5',
      'PATH: /TDM/Baseline/Repository Baseline 10.1/BaselineRecipeParameter TDM/4',
      'PATH: /TDM/ITDC/Repository/BASELINETEST/TDM/17',
      'PATH: /TDM/Baseline/DCC Repository/Baseline ITEM DCC/6',
      'PATH: /TDM/Baseline/DCC Repository/Baseline VPD DCC/2',
      'PATH: /TDM/Baseline/DCC Repository/Baseline YMS DCC/43',
      'PATH: /TDM/Baseline/DCC Repository/Baseline Basic DCC/15',
      'PATH: /TDM/ITDC/Repository/BASELINETEST_DCC/DCC/7',
      'PATH: /TDM/Baseline/TCM Repository/TCM Baseline/3',
      'PATH: /TDM/Baseline/TCM Repository/TCM Baseline Targoss/1',
      'PATH: /TDM/Baseline/TCM Repository/TCM Baseline RGA/2',
      'PATH: /TDM/Baseline/TCM Repository/TCM Baseline BasicGem/3',
      'PATH: /TDM/Baseline/TCM Repository/TCM Baseline 300mm/1',
      'PATH: /TDM/Baseline/TCM Repository/TCM Baseline RGA2/1',
      'PATH: /TDM/Baseline/TCM Repository/TCM Baseline ModBus/3',
      'PATH: /TDM/ITDC/Repository/BASELINETEST-TCM/TCM/1']

    timeout = 120
    $log.info "waiting for #{timeout} s to establish all EI connections" # TODO: wait_for
    sleep timeout

    log = @@eibltest.eiserver.read_log
    log_res = []
    missing_entries = []
    log_ref.each {|e| missing_entries << e.dup}
    log.each {|line|
      tline = Time.parse('1970-01-01 00:00:00.000Z')
      tline = Time.parse(line[0..22] + 'Z') if line.start_with?(Time.now.utc.year.to_s)
      if tline.between?(tstart, Time.now.utc)
        if line.include?(log_ref.first)
          log_res << line
          missing_entries.delete(missing_entries.first)
        end
      end
      if missing_entries.count < log_ref.count
        log_ref.each {|ref|
          if line.include?(ref)
            log_res << line
            missing_entries.delete(ref)
          end
        }
      end
    }
    assert log_res.count > 0, "No entries found in log. Check if EI is really running on correct server!"
    log_res_flat = log_res.flatten
    assert Time.parse(log_res_flat.first[0..22] + 'Z').between?(tstart, Time.now.utc), "No log entries for #{Time.parse(log_res_flat.first)} found during test period: #{tstart}...#{Time.now.utc}"
    assert missing_entries == [], "Some entries not found in log: #{missing_entries}"

    log = @@eibltest.eiserver.read_log('armor')
    ref = "rendererPath: '/system/plugins/renderers/Element Identifying Verbose XML Renderer DCP_2_2_9 TDM_2_2_9 DCC_2_2_9 OperationMode"
    assert find_last_log_entry(log, ref, tstart), "No armor log entries found during test period, expected was: #{ref}"

    log = @@eibltest.eiserver.read_log
    ref = "Sync with equipment failed - but EI continues running"
    assert find_last_log_entry(log, ref, tstart), "No ei log entries found during test period, expected was: #{ref}"

    log = @@eibltest.eiserver.read_log('fabGui')
    ref = "Equipment Interface is starting up..."
    assert find_last_log_entry(log, ref, tstart), "No fabGui log entries found during test period, expected was: #{ref}"

    $log.info "\n\tCheck FabGUI Status! FabGui -> Control Center -> EI Status for HYB171 is NotInSync and shows a error messages:
    SiView and Tool are not in sync (CEI24020: Sync online mode change failed! Details: CEI41005: Equipment is not communicating. … etc.)\nPress <enter>"
    gets

    log = @@eibltest.eiserver.read_log
    ref = "EI start sequence successful completed"
    assert find_last_log_entry(log, ref, tstart), "No ei log entries found during test period, expected was: #{ref}"

    @@eibltest.eiserver.start_toolsim
    $log.info "\n\tSelect Entity in ToolSim or press button Connect and check FabGui Status again! 
      It changes to InSync and no error messages will be displayed\nPress <enter>"
    gets
  end

  def test12_ei_startup_with_filein
    # if the test started too late, log file entries maybe already there, therefore we go back 5 minutes
    tstart = Time.now.utc - 300

    $log.info "\n\tGoto current EI version folder: $ cd $CEI_ROOT/repository/HYB171/<version>!\nPress <enter>"
    gets
    $log.info "\n\tRename $ mv ceiST.cfg.FileIn-Test ceiST.cfg!\nPress <enter>"
    gets
    $log.info "\n\tSet switch checkEIVersions to true in HYB171.cfg!\nPress <enter>"
    gets
    @@eibltest.eiserver.start_ei

    timeout = 120
    $log.info "waiting for #{timeout} s to establish all EI connections" # TODO: wait_for
    sleep timeout

    log = @@eibltest.eiserver.read_log
    ref = "EI version check passed - EI version is "
    assert find_last_log_entry(log, ref, tstart), "No ei log entries found during test period, expected was: #{ref}"

    log = @@eibltest.eiserver.read_log
    ref = "Load FileIn <F36ELSRT-EIVersionCheck.st>  successful" #two spaces?
    assert find_last_log_entry(log, ref, tstart), "No ei log entries found during test period, expected was: #{ref}"
    
    $log.info "\n\tCheck FabView equipment history for Event EIStart with Details: Memo … STFiles: F36ELSRT-EIVersionCheck.st!\nPress <enter>"
    gets

    # Move to cleanup method? However this is TC03 too - just added it here and removed TC03
    @@eibltest.eiserver.kill_ei
    log = @@eibltest.eiserver.read_log
    ref = "release log writer for /ei/develop/running/log/HYB171/"
    assert find_last_log_entry(log, ref, tstart), "No ei log entries found during test period, expected was: #{ref}"
    assert @@eibltest.eiserver.ei_running?, "EI still running"

    $log.info "\n\tRename $ mv ceiST.cfg ceiST.cfg.FileIn-Test!\nPress <enter>"
    gets
    $log.info "\n\tSet switch checkEIVersions to false in HYB171.cfg!\nPress <enter>"
    gets
  end
  
  def test13_ei_startup_with_tool
    # if the test started too late, log file entries maybe already there, therefore we go back 5 minutes
    tstart = Time.now.utc - 300

    @@eibltest.eiserver.start_toolsim
    $log.info "\n\tSelect Entity in ToolSim or press button Connect and check FabGui Status again! 
      It changes to InSync and no error messages will be displayed\nPress <enter>"
    gets
    @@eibltest.eiserver.start_ei

    timeout = 120
    $log.info "waiting for #{timeout} s to establish all EI connections" # TODO: wait_for
    sleep timeout

    log = @@eibltest.eiserver.read_log
    ref = "EI start sequence successful completed"
    assert res = log.find {|line| line.include?(ref)}
    assert find_last_log_entry(log, ref, tstart), "No ei log entries found during test period, expected was: #{ref}"

    log = @@eibltest.eiserver.read_log('fabGui')
    ref = "Equipment Interface is starting up..."
    assert find_last_log_entry(log, ref, tstart), "No fabGui log entries found during test period, expected was: #{ref}"

    log = @@eibltest.eiserver.read_log
    ref = "EstablishSession to TraceEDA returned an error"
    errors = []
    log.find {|line| 
      if line.include?('error')
        errors << line if !line.include?(ref)
      end 
    }
    assert errors.count == 0, "Errors found in log\n#{errors}"

    $log.info "\n\tCheck FabGui status! It is InSync and no error messages will be displayed\nPress <enter>"
    gets
  end

  def find_last_log_entry(log, ref, tstart)
    tline = Time.parse('1970-01-01 00:00:00.000Z')
    log.each {|line|
      tline = Time.parse(line[0..22] + 'Z') if line.start_with?(Time.now.utc.year.to_s)
      if tline.between?(tstart, Time.now.utc)
        if line.include?(ref)
          return true
        end
      end
    }
  end
end
