=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: Conny Stenzel, 2020-04-01

History:
=end

require_relative 'EIBLTest'

# E10 TEST PLAN
# Internal Buffer Tool
class EIBL_13 < EIBLTest
  ## nono, set explicitly # @@test_defaults = {}  # placeholder for test specific defaults

  def test00_setup
    $setup_ok = false
    #
    ### check EI Baseline Regression test plan (sheet Test Env Setup) for cei env setup and startup of ei
    #
    @@eibltest = EIBL::Test.new($env, @@eqp, @@eiversion, sv: @@sv)
    # assert @@eibltest.set_defaults(@@test_defaults)
    #
    $setup_ok = true
  end
  
  def test11_single_job_run_ib_tool
  end
  
  def test12_multiple_job_run
  end
  
  def test13_opestartcancel_for_batch
  end
  
  def test14_suspended_opestartcancel_for_batch
  end
  
  def test15_batch_cancel_scenario_triggered_by_tool_inhibit
  end
  
  def test16_happy_lot_semi_comp_1
  end

end
