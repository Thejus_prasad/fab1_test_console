=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: Conny Stenzel, 2020-04-01

History:
=end

require_relative 'EIBLTest'

# MANUAL DC TEST PLAN
# Regular DC Runs
class EIBL_08 < EIBLTest
  ## nono, set explicitly # @@test_defaults = {}  # placeholder for test specific defaults

  def test00_setup
    $setup_ok = false
    #
    ### check EI Baseline Regression test plan (sheet Test Env Setup) for cei env setup and startup of ei
    #
    @@eibltest = EIBL::Test.new($env, @@eqp, @@eiversion, sv: @@sv)
    # assert @@eibltest.set_defaults(@@test_defaults)
    #
    $setup_ok = true
  end
  
  def test11_equipment_level_dcp_offline_1
  end
  
  def test12_job_level_dcp_offline_1
  end
  
  def test13_job_level_dcp_semi_start_1
  end
  
  def test14_run_w_2_lots_offline_1
  end
  
  def test15_run_w_2_lots_semi_start_1
  end
  
  def test16_batch_run_w_2_lots_offline_1_hybib
  end
  
  def test17_batch_run_w_2_lots_semi_start_1_hybib
  end
  
  def test18_no_dcp_defined_offline_1
  end
  
  def test19_no_dcp_defined_semi_start_1
  end
  
  def test20_recipe_level_dcp
  end

end
