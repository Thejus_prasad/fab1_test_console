=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: Conny Stenzel, 2020-04-01

History:
=end

require_relative 'EIBLTest'

# E10 TEST PLAN
# Fixed Buffer Tools (Non-chambers)
class EIBL_11 < EIBLTest
  ## nono, set explicitly # @@test_defaults = {}  # placeholder for test specific defaults

  def test00_setup
    $setup_ok = false
    #
    ### check EI Baseline Regression test plan (sheet Test Env Setup) for cei env setup and startup of ei
    #
    @@eibltest = EIBL::Test.new($env, @@eqp, @@eiversion, sv: @@sv)
    # assert @@eibltest.set_defaults(@@test_defaults)
    #
    $setup_ok = true
  end
  
  def test11_single_job_run
  end
  
  def test12_multiple_job_run
  end
  
  def test13_auto_1_and_semi_1_mixed_mode_test_case
  end
  
  def test14_e10_state_is_eng_and_should_not_be_changed_by_ei
  end

end
