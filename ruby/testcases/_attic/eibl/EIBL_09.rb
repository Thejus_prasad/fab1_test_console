=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: Conny Stenzel, 2020-04-01

History:
=end

require_relative 'EIBLTest'

# MANUAL DC TEST PLAN
# Manual DC Error Conditions
class EIBL_09 < EIBLTest
  ## nono, set explicitly # @@test_defaults = {}  # placeholder for test specific defaults

  def test00_setup
    $setup_ok = false
    #
    ### check EI Baseline Regression test plan (sheet Test Env Setup) for cei env setup and startup of ei
    #
    @@eibltest = EIBL::Test.new($env, @@eqp, @@eiversion, sv: @@sv)
    # assert @@eibltest.set_defaults(@@test_defaults)
    #
    $setup_ok = true
  end
  
  def test11_no_data_entered_in_fabgui_but_user_clicks_ok_offline_1
  end
  
  def test12_no_data_entered_in_fabgui_but_user_clicks_ok_semi_start_1
  end
  
  def test13_no_data_entered_in_fabgui_but_user_clicks_cancel_offline_1
  end
  
  def test14_no_data_entered_in_fabgui_but_user_clicks_cancel_semi_start_1
  end
  
  def test15_no_data_entered_in_fabgui_and_request_times_out_offline_1
  end
  
  def test16_no_data_entered_in_fabgui_and_request_times_out_semi_start_1
  end
  
  def test17_data_does_not_validate
  end
  
  def test18_data_does_not_validate_ii
  end
  
  def test19_closing_mm_opi_siview_before_results_are_returned
  end
  
  def test20_user_presses_opecomp_after_previous_opecomp_request_fails_offline_1
  end
  
  def test21_user_presses_opecomp_after_previous_opecomp_request_fails_semi_start_1
  end
  
  def test22_incorrect_dcp_defined
  end

end
