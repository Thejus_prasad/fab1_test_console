=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: Conny Stenzel, 2020-04-01

History:
=end

require_relative 'EIBLTest'


# GENERIC TESTPLAN
# General Baseline Scenarios
class EIBL_01 < EIBLTest
  ## nono, set explicitly # @@test_defaults = {}  # placeholder for test specific defaults

  def test00_setup
    $setup_ok = false
    #
    ### check EI Baseline Regression test plan (sheet Test Env Setup) for cei env setup and startup of ei
    #
    @@eibltest = EIBL::Test.new($env, @@eqp, @@eiversion, sv: @@sv)
    # assert @@eibltest.set_defaults(@@test_defaults)
    #
    $setup_ok = true
  end
  
  def test11_recipe_select_happy_lot_scenario_one_job
  end

  def test12_recipe_select_allowed_by_siview_userdefinedcodes_sublottyperecipeselectenable
  end
  
  def test13_recipe_select_rejected_by_siview_userdefinedcodes_sublottyperecipeselectenable
  end

  def test14_upload_and_compare_happy_lot_scenario_one_job
  end
  
  def test15_upload_and_compare_verification_fail
  end
  
  def test16_compare_and_download_happy_lot_scenario_one_job
  end
  
  def test17_compare_and_download_verification_fail
  end
  
  def test18_happy_lot_scenario_2_with_auto_data_collection
  end
  
  def test19_recipe_download_without_renaming
  end
  
  def test20_cancel_startlotreservation
  end
  
  def test21_cancel_operationstart
  end
  
  def test22_operstartcancel_on_slow_tools_new_config_switch
  end
  
  def test23_recipe_management
  end
  
  def test24_apc_previewrun
  end
  
  def test25_ec_and_sv_check_with_csv_blob
  end
  
  def test26_conditioningncl_conditioning_for_non_cluster_tools_happylot
  end
  
  def test27_conditioningncl_consumer_waits_for_provider_conditioningjobwaitingforstarttimeout
  end
  
  def test28_conditioningncl_startlotsreservation_fail_conditioning_type_missmatch
  end
  
  def test29_late_foup_opening_just_in_time_opening
  end

end
