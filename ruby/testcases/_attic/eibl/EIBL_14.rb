=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: Conny Stenzel, 2020-04-01

History:
=end

require_relative 'EIBLTest'

# TRACE-DATACOLLECTION
# TraceDC
class EIBL_14 < EIBLTest
  ## nono, set explicitly # @@test_defaults = {}  # placeholder for test specific defaults

  def test00_setup
    $setup_ok = false
    #
    ### check EI Baseline Regression test plan (sheet Test Env Setup) for cei env setup and startup of ei
    #
    @@eibltest = EIBL::Test.new($env, @@eqp, @@eiversion, sv: @@sv)
    # assert @@eibltest.set_defaults(@@test_defaults)
    #
    $setup_ok = true
  end
  
  def test11_happy_lot
  end
  
  def test12_stop_on_startlotsreservationcancel
  end
  
  def test13_ei_sends_value_request
  end
  
  def test14_count_of_tracereports
  end
  
  def test15_start_stop_on_event
  end
  
  def test16_start_stop_on_event_2_running_jobs
  end
  
  def test17_tooltraceplan
  end
  
  def test18_cluster_tool_interfaceA_wonderware
  end

end
