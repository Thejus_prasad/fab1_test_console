require "RubyTestCase"

# ELM: prepares lots for automatic assignment to projects by ELM
#
# (requires products ELM-PROD-000.00 - ELM-PROD-019.00)

class Test_ELM_Preparation < RubyTestCase
  Description = "ELM: prepares lots for automatic assignment to projects by ELM
  (requires products ELM-PROD-000.00 - ELM-PROD-019.00)"
  
  # true: carriers will be deleted and recreated; false: carriers will be emptied only
  @@create_carriers = false
  # delete existing carriers?
  @@delete_existing_carriers = false
  # will test lots be created?
  @@create_lots = true
  # create only lots for automatic assignment?
  @@create_lots_auto_only = false
  # create lots for one product and sublot type only?
  #   for details, see init method
  @@create_lots_for_one_project_only = false
  # delete existing lots?
  @@delete_existing_lots = false

  # auto bank in operation
  @@bankin_op = "3000.100" # ITDC
  #@@bankin_op = "6500.1000" # LET
    
  # restrict number of lots by this parameter (if > 0) or by combinations (otherwise)
  @@number_of_lots = "0"
  
  @@products = []
  @@src_product = "UT-RAW.00"
  @@start_bank = "ON-RAW"
  
  # lot name prefix
  @@lot_prefix = "ELM"
  # infix for name of lots that should be assigned by ELM automatically
  @@lot_infix_yes = "-OK-"
  # infix for name of lots that should not be assigned by ELM automatically
  @@lot_infix_no = "-XX-"
  
  # carriers to use
  @@carrier_prefix = "ELM"
  @@carrier_category="FEOL"
  
  # sublot types for ELM: examples, all not excluded should be included
  @@slt_yes = "PO,PX,PC,PB,PR,QF,QD,QE,QX,EQ,RQ,ES,EO,EC,EB,RD".split(",")

  # sublot types not for ELM
  @@slt_no = "MW,EM,ET,SCRAP,PZ,Dummy,Equipment Monitor,Process Monitor,Recycle,VendorLager_29,VendorOPSPPL".split(",")

  @@prios_no = "0,1".split(",")
  @@prios_yes = "2,3,4,5".split(",")

  # initialize carriers (empty and delete, if existing, then create newly)
  def test00_init
    init()
  end
  
  def test01_deleteCarriers
    return if !@@delete_existing_carriers
    @@carriers.each {|c|
      if @@delete_existing_lots
        deleteLots(c)
      end
      $sv.carrier_status_change c, "NOTAVAILABLE"
      $sv.durable_delete(c)
    }
  end
  
  def test02_createCarriers
    return if !@@create_carriers
    @@carriers.each {|c|
      $sv.durable_register(c, @@carrier_category)
      rc = $sv.carrier_status_change(c, "AVAILABLE")
      assert_equal(0, rc, "something is wrong with carrier #{c}")
    }
  end

  def test11_createLots
    return unless @@create_lots
    $run = $run == nil ? 1 : $run+1
    # create list of ELM products to use
    init
    srclot = $sv.new_srclot_for_product(@@products[0])
    assert srclot, "error creating srclot"
#    reqInfo = (@@number_of_lots > 0 ? "#{@@number_of_lots} required" : "")
#    $log.info("#{reqInfo}")
#    $log.info("will create lots using products #{@@products.inspect}")
#    # create source lots, if required
#    get_source_lots(@@products[0], @@src_product, @@number_of_lots.to_i * 25, @@start_bank)
#    # create test lots
    createTestLots
    $log.info "#{$testlots.size} lots created"
    $log.info("#{reqInfo}")
  end
  
  def self.set_number_of_lots(no)
    @@number_of_lots = no
  end
  
  def self.get_number_of_lots
    @@number_of_lots
  end
  
  # create list of ELM products to use
  def init
    @@carriers = @@number_of_lots.to_i.times.collect {|i|
      @@carrier_prefix + "%.#{@@number_of_lots.size}d" % i
    }
    @@carrier_pattern = @@carrier_prefix + "%"
    if @@create_lots_for_one_project_only
      ### GENERATE LOTS FOR ONE PRODUCT ONLY ###
      @@products << "ELM-PROD-012.00"
      @@slt = ["PO"]
      @@prios = ["4"]
    else
      ### GENERATE LOTS FOR ALL COMBINATIONS OF PRODUCT AND SUBLOT TYPE ###
      20.times{|i|
        @@products << "ELM-PROD-%03d.00"%i
      }
      @@slt = @@create_lots_auto_only ? @@slt_yes : @@slt_no + @@slt_yes
      @@prios = @@create_lots_auto_only ? @@prios_yes : @@prios_yes + @@prios_no
    end
  end

  # delete lots in carrier
  def deleteLots(carrier)
    carrierStatus = $sv.carrier_status(carrier)
    if carrierStatus != nil
      # carrier exists
      carrierStatus.lots.each{|lot|
        $sv.delete_lot_family(lot, :delete=>false)
      }
    end
  end
  
  # create lots
  def createTestLots
    $testlots = []
    $i = 0
    $expected_assigned = 0
    numberOfLots = @@number_of_lots.to_i
    if numberOfLots > 0
      $log.info("...trying to create #{numberOfLots} lots")
      # number of lots restricted by parameter
      while $i < numberOfLots
        @@prios.each{|prio|
          @@slt.each{|slt|
            release(slt, prio, false)
            release(slt, prio, true) if !@@create_lots_auto_only
            return if $i >= numberOfLots
          }
        }
      end
    else
      # number of lots restricted by prios and sublottypes
      $log.info("...trying to create lots")
      @@prios.each{|prio|
        @@slt.each{|slt|
          release(slt, prio, false)
          release(slt, prio, true) if !@@create_lots_auto_only
        }
      }
    end
  end

  # utility functions
  
  # create new source lots for STB, if needed
  def get_source_lots(product, source_product, num_wafers, bank)
    if true
      #$sv.srclot_inq(:product=>product, :nwafers=>num_wafers).length < 1
      # not enough source wafers, so create some new
      $sv.vendorlot_receive(bank, source_product, num_wafers+1000)
    end
  end

  # releases ELM lots
  def release(sublottype, prio = 4, bank_in = false)
    $i += 1
    lot = @@lot_prefix + $run.to_s
    if @@slt_no.index(sublottype) != nil  or bank_in or @@prios_no.index(prio) != nil
      lot = lot + @@lot_infix_no
    else
      lot = lot + @@lot_infix_yes
      $expected_assigned += 1
    end
    lot = lot + "%.#{@@number_of_lots.size}d" % $i + ".00"
    # release lot
    lot = $sv.new_lot_release(:lot=>lot, :product=>@@products[$i % @@products.size], :sublottype=>sublottype, :priority=>prio,
      :stb=>true, :carrier=>@@carrier_pattern)
    if lot != nil
      #$log.info(lot)
      $testlots << lot
      if bank_in
        # (auto) bank in, if required
        $sv.lot_opelocate(lot, @@bankin_op)
      end
    end
  end
  
  # move lots to other carriers (restricted by maximum length of carrier list)
  # from - pattern for source carriers
  # to - pattern for destination carriers
  def self.changeCarrier(from, to)
    return if from == to
    while true
      clFrom=$sv.carrier_list(:carrier=>from,:notempty=>true)#,:status=>"AVAILABLE")
      $log.info("changeCarrier from: " + clFrom.inspect)
      break if clFrom.size == 0
      clTo=$sv.carrier_list(:carrier=>to,:empty=>true,:status=>"AVAILABLE")
      $log.info("changeCarrier to: " + clTo.inspect)
      break if clFrom.size == 0
      n = clFrom.size < clTo.size ? clFrom.size : clTo.size
      clFrom[0..n-1].each_with_index{|c, i|
        $sv.carrier_status(c).lots.each{|lot|
          $sv.wafer_sort_rpt(lot, clTo[i])
        }
      }
    end
  end
  
  # change sublot types of lots
  # lots - lot IDs
  # product - new product
  # sublotType - new sublot type
  # limit - maximum number of changes (default 0 = no limit)
  def self.changeLots(lots, params={})
    limit = (params[:limit] or 0)
    product = (params[:product] or "ELM-PROD-011.00")
    route = (params[:route] or "C02-1116.01")
    sublottype = (params[:sublottype] or "QD")
    changed = 0
    lots.each{|lot|
      info = $sv.lot_info(lot)
      if info.sublottype != sublottype or info.product != product
        rc1 = $sv.schdl_change(lot, :product=>product, :route=>route)
        rc2 = $sv.lot_mfgorder_change(lot, :sublottype=>sublottype)
        # Attention! rc1 ==0 has no real meaning, rc2 is array!
        if rc1 == 0 and rc2[0] == 0
          changed += 1
        end
      end
      break if limit > 0 and changed >= limit
    }
    $log.info("changed #{changed} lots")
    return changed
  end
  
  #load_run_testcase "Test_ELM_Preparation", "let"
  #Test_ELM_Preparation.changeCarrier("ZELM%","ELM%")
end
