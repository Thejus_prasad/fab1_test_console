=begin
(c) Copyright 2011 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

 author      - ulrich.koerner@globalfoundries.com
 date        - 2011/08/01
=end

require "RubyTestCase"
require 'setupfc'
require 'rexml/document'
require 'base64'
require 'digest/md5'

#  automated test of setup.FC API
class Test_SetupFC_Setup < RubyTestCase
  Description = "Test SetupFC XML Interface"

  @@version        = '1.11.1' #"1.10.3"
  @@department     = "FAT"
  @@defaultUser    = "sschoene3"

  # will be filled out by the setup test
  @@test_specs = {}

  @@loop_create_specs = "10"
  @@count_find_specs_by_keywords = "100"

  def setup
    if $env == "f8stag"
      @@defaultUser    = "dsteger"
    end
  end

  def teardown
    $sfc.support.user = @@defaultUser
    $sfc.service.user = @@defaultUser
  end

  # config/preparation task
  def test000_setup
    $log.info "Environment: #{$env}"

    $setup_ok = false
    @@sfcDepartments = []
    @@sfcClasses     = Hash.new
    @@sfcKeywords    = Hash.new
    @@sfcUsersSpecs  = Hash.new

    $sfc = EasySetupFC.new($env) unless $sfc and $sfc.env == $env
    @@defaultUser = $sfc.service.user

    @@default_attachment_file = "etc/testcasedata/setupfc_att.png"
    @@defaultAttachmentFileName = "gf.png"

    # setup the specs
    @@default_attachment = Base64.encode64(File.read(@@default_attachment_file)).tr("\n","")

    @@defaultTestSpecTitle = "qa_#{@@defaultUser}_#{@@version}"

    attach = {
     :attachment_name => @@defaultAttachmentFileName,
     :attachment => @@default_attachment
    }
    # set the specs according to environment configs
    if $env == "let"
      @@fabLocation = "C"
      @@test_specs["02_base_1"] = create_testspec_hash("02", "Base", "Base", "C02-FAT-2", { :id=>create_test_id })
      @@test_specs["02_base_2"] = create_testspec_hash("02", "Base", "Base", "C02-FAT-2", { :id=>"#{create_test_id}-NEW" })
      #2 @@test_specs["05_base_1"] = create_testspec_hash("05", "Base", "Base", "C05-ALL-1", attach)
      @@test_specs["05_armor_1"] = create_testspec_hash("05", "ARMOR", "ARMOR", "C05-FAT-41", attach) # C05-18 without approval :-)
      @@test_specs["08_base_1"] = create_testspec_hash("08", "Base", "Base", "C08-1", attach)
      @@test_specs["08_base_3"] = create_testspec_hash("08", "Base", "Base", "C08-FAT-3", attach)
      # #
      #5 @@test_specs["07_base_1"] = create_testspec_hash("07", "Base", "Base", "C07-FAT-15", { :id=>create_test_id }))
      # @@spec_inedit = "C05-00000074"
      @@armor_spec = "C05-00012955"
      @@spec_transfer = "C05-00000074"
      @@second_user = "testuser1" # test550
      @@approver1 = "testuser1"   # for approving content change and terminate
      @@approver2 = "testuser4"   # for approving property change
      @@adm_user = 'qa_admin'
      $setup_ok = true
    elsif $env == "f8itdc"
      @@fabLocation = "M"
      @@test_specs["02_base_1"] = create_testspec_hash("02", "Production", "Base", "M02-1", { :id=>"M02-ITDC-TEST-SPEC" })
      @@test_specs["02_base_2"] = create_testspec_hash("02", "Production", "Base", "M02-1", { :id=>"M02-ITDC-TEST-SPEC-NEW" })
      @@test_specs["05_armor_1"] = create_testspec_hash("05", "ToolRecipe", "ARMOR", "M05-3", attach)
      @@test_specs["05_armor_2"] = create_testspec_hash("05", "ToolRecipe", "ARMOR", "M05-3", attach)
      @@test_specs["08_base_1"] = create_testspec_hash("08", "Procedure", "Base", "M08-1", attach)
      @@test_specs["07_eopmca_1"] = create_testspec_hash("07", "Corrective-Action", "EOPMCorrectiveAction", "M07-3", attach)
      @@test_specs["07_eopmqual_1"] = create_testspec_hash("07", "Operation-Qualification", "EOPMQual", "M07-4", attach)
      @@test_specs["07_eopmpm_1"] = create_testspec_hash("07", "Preventive-Maintenance", "EOPMPM", "M07-ALL-18", attach)
      @@test_specs["07_table_1"] = create_testspec_hash("07", "SpaceTCP", "Table", "M07-ALL-22", attach)
      @@test_specs["07_eopm_1"] = create_testspec_hash("07", "TestWaferKit", "EOPM", "M07-ALL-22", attach)
      $setup_ok = true
    elsif $env == "f8stag"
      @@fabLocation = "M"
      @@test_specs["02_base_1"] = create_testspec_hash("02", "Production", "Base", "M02-2", { :id=>create_test_id })
      @@test_specs["02_base_2"] = create_testspec_hash("02", "Production", "Base", "M02-2", { :id=>"#{create_test_id}-NEW" })
      @@test_specs["05_armor_1"] = create_testspec_hash("05", "ToolRecipe", "ARMOR", "M05-1", attach)
      @@test_specs["05_armor_2"] = create_testspec_hash("05", "ToolRecipe", "ARMOR", "M05-1", attach)
      @@test_specs["08_base_1"] = create_testspec_hash("08", "Procedure", "Base", "M08-1", attach)
      @@test_specs["08_base_2"] = create_testspec_hash("08", "Procedure", "Base", "M08-1", attach)
      @@test_specs["08_base_3"] = create_testspec_hash("08", "Procedure", "Base", "M08-2", attach)
      @@test_specs["07_eopmca_1"] = create_testspec_hash("07", "Corrective-Action", "EOPMCorrectiveAction", "M07-6", attach)
      @@test_specs["07_eopmqual_1"] = create_testspec_hash("07", "Operation-Qualification", "EOPMQual", "M07-7", attach)
      @@test_specs["07_eopmpm_1"] = create_testspec_hash("07", "Preventive-Maintenance", "EOPMPM", "M07-8", attach)
      @@test_specs["07_table_1"] = create_testspec_hash("07", "ToolControlPlan", "Table", "M07-5", { :type=>"SPACE", :filename=>"prod_specs/M07-00001295.xml" })
      @@test_specs["07_eopm_1"] = create_testspec_hash("07", "TestWaferKit", "EOPM", "M07-9", attach)
      ##disabled: @@test_specs["05_arac_1"] = create_testspec_hash("05", "ARAC", "ARAC", "M05-2", attach)
      @@test_specs["05_sp_1"] = create_testspec_hash("05", "SP", "SP", "M05-2", attach)
      # @@spec_inedit = "M05-00001508"
      @@armor_spec = "M05-00000570"
      @@spec_transfer = "M05-00000559"
      @@second_user = "jcoon"
      @@approver1 = "dsteger"
      @@approver2 = "dsteger"
      @@adm_user = "adm_dsteger"
      $setup_ok = true
    elsif $env == "itdc"
      @@department     = "FAS"
      @@fabLocation = "C"
      @@test_specs["02_base_1"] = create_testspec_hash("02", "TXT-ROUTES", "Base", "C02-2", { :id=>create_test_id })
      @@test_specs["02_base_2"] = create_testspec_hash("02", "TXT-ROUTES", "Base", "C02-2", { :id=>"#{create_test_id}-NEW" })
      @@test_specs["05_armor_1"] = create_testspec_hash("05", "Process", "ARMOR", "C05-3", attach) #
      @@test_specs["08_base_1"] = create_testspec_hash("08", "TXT-PROCEDURES", "Base", "C08-2", attach)
      @@test_specs["08_base_3"] = create_testspec_hash("08", "TXT-PROCEDURES", "Base", "C08-1", attach)
      #
      @@armor_spec = "C05-00000018"
      @@spec_transfer = "C05-00000074"
      @@second_user = "qauser01" # test550
      @@approver1 = "qauser01"   # for approving content change and terminate
      @@approver2 = "qauser02"   # for approving property change
      $setup_ok = true
$log.warn("setup.FC testcases not completely configured for #{$env} yet")
    elsif $env == "dev"
      @@department     = "FAT"
      @@fabLocation = "M"
      @@test_specs["02_base_1"] = create_testspec_hash("05", "Base", "Base", "M05-FAT-16", { :id=>create_test_id })
      #
      $setup_ok = true
$log.warn("setup.FC testcases not completely configured for #{$env} yet")
    else
      $setup_ok = false
      $log.warn("setup.FC testcases not configured for #{$env} yet")
    end
    $ts = @@test_specs
  end


  # # # # # creating, getting, deleting specifications

  # Helper method to create the a spec hash
  def create_testspec_hash(spec_class, sub_class, template, workflow_config, params={:id=>"automatically created"})
    spec = {
      :title          => @@defaultTestSpecTitle,
      :author         => @@defaultUser,
      :owner          => @@defaultUser,
      :department     => @@department,
      :fabLocation    => @@fabLocation,
      :classId        => spec_class,
      :subClassId     => sub_class,
      :template       => template,
      :workflowConfig => workflow_config,
      :type           => template
    }
    spec.merge!(params)
    return spec
  end

  # create a new spec from a hash and returns the Id
  def create_new_specification(params={})
    # create new specification
    res0 = $sfc.support.createSpecification({ :arg0 => params })
    $log.info "Reply: #{res0.inspect}"
    assert_not_nil res0[:createSpecificationResponse][:return][:id], " - specification not created:  #{res0.inspect}"
    spId = res0[:createSpecificationResponse][:return][:id]
    $log.info " - #{spId} specification created"
    return spId
  end

  # delete spec
  def delete_specification(spId, workingCopyToo = true)
    # since Version 1.1 there is no need deleting the workingcopy separately
    # use admin user for deletion
    res = false
    res = $sfc.delete_spec(spId, user: @@adm_user)
    assert res, "failed to delete spec"
    $log.info " - #{spId} specification deleted"
    return res
  end

  # get a specification
  def get_specification(spId, version = nil)
    res = $sfc.service.getSpecification(spId, version)[:getSpecificationResponse] [:return]
    assert_equal "true", res[:success]
    assert_equal "SUCCESS", res[:errorCode]
    $log.info " - #{spId} " + (version.nil? ? "" : "(#{version}) ") + "specification fetched"
    return res
  end

  def get_spec_as_rexml(sp_id)
    sp = get_specification(sp_id)
    spec1 = Base64.decode64(sp["result"]["content"])
    xml = REXML::Document.new(spec1)
    return xml
  end

  def get_spec_as_xml(sp_id)
    xml = get_spec_as_rexml(sp_id)
    xml.write(h1="",indent = 2)
    return h1
  end

  # # # # # references and attachments

  # attach, fetch and delete a attachment
  # be sure there is an edit workflow started for the specifications
  def add_delete_attachment(spId, newVersion, params={})
    assert_nothing_raised do
      # add attachment
      $sfc.create_attachment(params[:attachment_name], params[:attachment], spId)
      $log.info " - #{spId} (working copy) Attachment #{params[:attachment_name]} created"

      # get attachment
      res5 = $sfc.get_attachment(params[:attachment_name], spId, newVersion.to_s)
      # assert_equal @@default_attachment, res5[:content] unless params[:attachment_name]!=@@defaultAttachmentFileName
      assert res5
      $log.info " - #{spId} (working copy) Attachment #{params[:attachment_name]} fetched"

      # get attachment with metadata
      res6 = $sfc.get_spec_metadata(spId, newVersion.to_s)
      ref = res6[:attachmentReferences]
      assert_not_nil ref, "Attachment not found!"
      $log.info " - #{spId} (working copy) Attachment #{params[:attachment_name]} fetched"

      # delete attachment
      $sfc.delete_attachment(params[:attachment_name], spId, newVersion.to_s)
      $log.info " - #{spId} (working copy) Attachment #{params[:attachment_name]} deleted"
    end
  end

  # attach, fetch and delete a reference
  # be sure there is an edit workflow started for the specifications
  def add_delete_reference(spId, newVersion)
    # add reference
    targetSpecId = spId
    targetSpec = $sfc.findSpecificationsByMetadata({:author => @@defaultUser})[:result][:specList][0]
    assert targetSpec, "no spec to reference found"
    targetSpecId = targetSpec[:id]

    assert_nothing_raised do
      assert $sfc.create_reference(spId, targetSpecId), "failed to add reference #{targetSpecId} to #{spId}"
      $log.info " - #{spId} (working copy) Reference to #{targetSpecId} created"

      # get reference - have to use get working copy
      res8 = get_specification(spId, newVersion)
      assert_equal true, res8[:result].has_key?(:references)
      ref = res8[:result][:references]
      ref = [ref] if ref.kind_of?(String)
      assert_equal false, ref.index(targetSpecId).nil?, "ERR: spec reference not properly attached"
      $log.info " - #{spId} (working copy) reference to #{targetSpecId} found"

      # delete reference
      attachment = attachment # we use them from add reference
      res9 = $sfc.delete_reference(spId, targetSpecId)
      assert res9
      # assert_equal "true", res9[:success]
      # assert_equal "SUCCESS", res9[:errorCode]
      $log.info " - #{spId} (working copy) Reference to #{targetSpecId} deleted"
    end
  end

  # # # # # keywords

  def get_keyword_types()
    res0 = $sfc.getKeywordTypes
    assert_equal "true", res0[:success]
    assert_equal "SUCCESS", res0[:errorCode]
    ($log.warn("-- NOT TESTABLE -- no keywords found"); return) if res0[:result].nil?
    keys = res0[:result]
    keys = [keys] if keys.kind_of?(String)
    return keys
  end

  def get_values_for_keyword_type(keyword_type)
    res1 = $sfc.getKeywordValues(keyword_type)
    ($log.warn("#{keyword_type}: nothing found #{res1.inspect}");next) if !res1.has_key?(:result)
    return nil unless res1.has_key?(:result)
    return res1[:result]
  end

  # # # # # workflow

  def content_change(spec_id)
    assert_nothing_raised do
      res = $sfc.content_change spec_id
      $log.info " - #{spec_id} locked for content change"
      return res
    end
  end

  def properties_change(spec_id)
    assert_nothing_raised do
      res = $sfc.properties_change spec_id
      $log.info " - #{spec_id} locked for property change"
      return res
    end
  end

  def termination(spec_id)
    assert_nothing_raised do
      res = $sfc.terminate spec_id
      $log.info " - #{spec_id} locked for termination"
      return res
    end
  end

  def submit_properties_change(spec_id)
    assert_nothing_raised do
      res, new_version = $sfc.submit_properties_change spec_id
      return res, new_version
    end
  end

  def submit_termination(spec_id)
    assert_nothing_raised do
      res, new_version = $sfc.submit_termination spec_id
      return res, new_version
    end
  end

  # Submit a new version of the spec
  def submit_specification(spec_id, params={})
 #   assert_nothing_raised do
      res, version = $sfc.submit_specification spec_id, params
      return res
 #   end
  end

  # Approves a spec
  def human_workflow_action(spec_id, action, params={})
    assert_nothing_raised do
      res = $sfc.human_workflow_action spec_id, {:action=>action, :polling=>true}.merge(params)
      return res
    end
  end

   def approve_content(spec_id, params={})
    assert $sfc.human_workflow_action spec_id, {:polling=>true, :user=>@@approver2, :name=>"gfwf:approve"}.merge(params)
  end

  def approve_change(spec_id, params={})
    assert $sfc.human_workflow_action spec_id, {:polling=>true, :user=>@@approver1, :name=>"gfwf:approve"}.merge(params)
  end

  # Acknowledge
  def acknowledge(spec_id)
    assert_nothing_raised do
      res= $sfc.acknowledge spec_id, :polling=>true
      return res
    end
  end

  # # # # # complex workflow tasks

  # Process a spec lifecycle
  def process_spec(test_spec)
    sp_id = create_new_specification(test_spec)

    $log.info "Fill in content and get effective"
    content_change(sp_id)
    assert $sfc.wait_for_edit_content(sp_id), "no edit task"
    submit_specification(sp_id, test_spec)
    assert $sfc.wait_for_approve(sp_id), "no approve task"
    human_workflow_action(sp_id, "APPROVE")
    assert $sfc.wait_for_acknowledge(sp_id), "no acknowledge task"
    acknowledge(sp_id)

    $log.info "Create a new version"
    content_change(sp_id)
    assert $sfc.wait_for_edit_content(sp_id), "no edit task"
    submit_specification(sp_id, test_spec)
    assert $sfc.wait_for_approve(sp_id), "no approve task"
    human_workflow_action(sp_id, "APPROVE")
    assert $sfc.wait_for_acknowledge(sp_id), "no acknowledge task"
    acknowledge(sp_id)

    $log.info "Property change"
    properties_change(sp_id)
    assert $sfc.wait_for_edit_metadata(sp_id), "no edit task"
    submit_properties_change(sp_id)
    assert $sfc.wait_for_approve(sp_id), "no approve task"
    human_workflow_action(sp_id, "APPROVE")
    if $env == "let"
      assert $sfc.wait_for_approve(sp_id), "no approve task"
      human_workflow_action(sp_id, "APPROVE")
      assert $sfc.wait_for_acknowledge(sp_id), "no acknowledge task"
      acknowledge(sp_id)
    end

    assert wait_for(:timeout=>120) {
      wf = $sfc.get_active_workflows_for_spec(sp_id)
      (wf.select {|wf| wf[:workflowDefinition][:name] != "jbpm$gfwf:acknowledgement"} == [])
    }, "task not completed"

    $log.info "Termination"
    termination(sp_id)
    assert $sfc.wait_for_terminate(sp_id), "no terminate task"
    submit_termination(sp_id)
    assert $sfc.wait_for_approve(sp_id), "no approve task"
    human_workflow_action(sp_id, "APPROVE")
    assert $sfc.wait_for_acknowledge(sp_id), "no acknowledge task"
    acknowledge(sp_id)

    $log.info "Delete specification - skipped"
    #delete_specification(sp_id)
  end

  def create_test_id(); "#{@@fabLocation}TEST-#{(Time.now.to_f*1000).to_i}-SPEC"; end

  # Process a spec lifecycle
  def finish_spec(spec_id)
    $log.info "Termination"
    termination(spec_id)
    submit_termination(spec_id)
    human_workflow_action(spec_id, "APPROVE")
    acknowledge(spec_id)
  end


  # # # # # helper functions

  def generate_binary_file(name, size_mb)

    def one_mb_string
      i = 128 #*1024
      i.times.map{rand(1<<64)}.pack(i.times.map{"Q"}.to_s)
    end

    f=File.new(name,"wb")
    size_mb.times{f.write(one_mb_string)}
    f.close
  end

  # Base64 encoded string representing a file of filesize MB
  def generate_base64_string(filesize)
    n = filesize * 1024 * 1024
    z = 4 * (n+2)/3
    return "0" * z
  end

  # not necessary, should be avoided
  def assert_nothing_raised(*args)
    self.assertions += 1
    if Module === args.last
      msg = nil
    else
      msg = args.pop
    end
    begin
      line = __LINE__; yield
    rescue Minitest::Skip
      raise
    rescue Exception => e
      bt = e.backtrace
      as = e.instance_of?(Minitest::Assertion)
      if as
        ans = /\A#{Regexp.quote(__FILE__)}:#{line}:in /o
        bt.reject! {|ln| ans =~ ln}
      end
      if ((args.empty? && !as) ||
          args.any? {|a| a.instance_of?(Module) ? e.is_a?(a) : e.class == a })
        msg = message(msg) { "Exception raised:\n<#{mu_pp(e)}[#{line}]>" }
        raise Minitest::Assertion, msg.call, bt
      else
        raise
      end
    end
    nil
  end

end
