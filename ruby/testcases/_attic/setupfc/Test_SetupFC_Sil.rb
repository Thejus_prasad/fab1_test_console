=begin
(c) Copyright 2011 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.
=end


require_relative 'Test_SetupFC_Setup'

#  automated test of setup.FC API 
class Test_SetupFC_Sil < Test_SetupFC_Setup
  @@MonitoringSpecTable ='C05-00001381'
  @@SpecTable	=	'C05-00001382'
  
  @@approver = 'tklose'

  @@cycles = 1
  
  def test00_cleanup
    sp_id = @@MonitoringSpecTable
    (human_workflow_action(sp_id, "CANCEL", :user=>@@approver); sleep 30) if $sfc.wait_for_approve(sp_id, :user=>@@approver, :timeout=>1, :sleeptime=>1)
    (human_workflow_action(sp_id, "CANCEL"); sleep 30) if $sfc.wait_for_edit_content(sp_id, :timeout=>1, :sleeptime=>1)
    sp_id = @@SpecTable
    (human_workflow_action(sp_id, "CANCEL", :user=>@@approver); sleep 30) if $sfc.wait_for_approve(sp_id, :user=>@@approver, :timeout=>1, :sleeptime=>1)
    (human_workflow_action(sp_id, "CANCEL", :user=>@@approver); sleep 30) if $sfc.wait_for_edit_content(sp_id, :user=>@@approver, :timeout=>1, :sleeptime=>1)
    (human_workflow_action(sp_id, "CANCEL"); sleep 30) if $sfc.wait_for_edit_content(sp_id, :timeout=>1, :sleeptime=>1)
  end
  
  def test10_load_monitoring_table
    sp_id = @@MonitoringSpecTable
    @@cycles.times{edit_submit_approve(sp_id, @@approver)}
  end

  def test20_load_spec_table
    sp_id = @@SpecTable
    @@cycles.times{edit_submit_approve(sp_id, @@approver)}
  end
  
  def xtestxx
    $log.info("Hi")
    s = get_specification(@@SpecTable)
    puts Base64.decode64(s[:result][:content])
  end
  
  def edit_submit_approve(sp_id, approver)
    version = get_specification(sp_id)[:result][:version].to_i
    $log.info("Start edit workflow for version #{version}")
    $sfc.content_change(sp_id)
    submit_specification(sp_id) #, :type=>test_spec[:template])
    human_workflow_action(sp_id, "APPROVE", :user=>approver)
    sleep 30
    assert wait_for{"#{version+1}"==get_specification(sp_id)[:result][:version]}
  end
end
