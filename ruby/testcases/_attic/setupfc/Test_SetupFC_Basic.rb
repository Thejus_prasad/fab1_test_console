=begin
(c) Copyright 2011 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

 author      - ulrich.koerner@globalfoundries.com, daniel.steger
 date        - 2012/12/10
=end

require_relative 'Test_SetupFC_Setup'

#  automated test of setup.FC API 
class Test_SetupFC_Basic < Test_SetupFC_Setup
  Description = "Test SetupFC XML Interface Basic functionality"

       
  # getStackVersion
  def test010_get_stack_version
    $setup_ok = false
    assert_match Regexp.new(Regexp.quote("Gearbox-Version: #{@@version}")), $sfc.getStackVersion()[:result]
    $setup_ok = true
  end

  # hasOpenAcknowledgements
  def test020_has_open_acknowledgements
    res0 = $sfc.hasOpenAcknowledgements(@@defaultUser, :export=>"log/#{__method__}.xml")
    #$log.info "--> #{res0.inspect}"
    assert_equal "true", res0[:success], "hasOpenAcknowledgements fialed"
    assert_equal "SUCCESS", res0[:errorCode], "hasOpenAcknowledgements fialed"
    
    $log.warn "-- at the moment only acknowledgements for class 07 specs --"
    $log.info((res0[:result] ? "" : "no ") + "acknowledgements in list for #{@@defaultUser}") #if "false" != res0[:result]
  end
  
  # getDepartments
  def test030_get_departments
    res0 = $sfc.getDepartments
    assert_equal "true", res0[:success]
    assert_equal "SUCCESS", res0[:errorCode]
    assert !res0[:result].nil?

    # hold the departments
    @@sfcDepartments = res0[:result]
    assert @@sfcDepartments.length > 0, "WARN: No departments configured!"
    $log.info " - #{@@sfcDepartments.length} departments found"
  end
  
  # specificationClasses and -SubClasses
  # fills @@sfcClasses
  def test040_get_specification_classes_and_subclasses
    $setup_ok = false
    res0 = $sfc.getSpecificationClasses
    assert_equal "true", res0[:success]
    assert_equal "SUCCESS", res0[:errorCode]
    classes = res0[:result]
    assert_not_equal nil, classes
    $log.info " - #{classes.count} classes"
    # get sub-classes and hold them all
    count = 0
    test_ok = true
    classes.each do |key|
      res1 = $sfc.getSpecificationSubClasses(key)
      ($log.warn(""); test_ok = false) unless res1.has_key?(:result)
      sub_classes = res1[:result]; sub_classes = [sub_classes] if sub_classes.kind_of?(String)
      @@sfcClasses[key.to_sym] = sub_classes
      count += sub_classes.count
      $log.info " - #{sub_classes.count} subclasses in class #{key}"
    end
    $log.info " - #{count} class - subclass combinations"
    $setup_ok = true
  end
  
  # getSpecificationByIdWithTransform
  def test041_get_specification_with_transformation        
    xslt = "ArmorSetup.xsl"
    
    # collect specs all versions with getSpecificationById and ...WithTransform
    $mys = s = []
    $myt = t = []
    $myx = x = []
    
    s << get_specification(@@armor_spec)
    t << $sfc.getSpecificationWithContentTransformation(@@armor_spec)
    x << $sfc.getSpecificationWithContentTransformation(@@armor_spec,nil,xslt)

    (s[0][:result][:version].to_i+1).times{|i|
      s << get_specification(@@armor_spec,i.to_s)
      t << $sfc.getSpecificationWithContentTransformation(@@armor_spec,i.to_s)
      x << $sfc.getSpecificationWithContentTransformation(@@armor_spec,i.to_s,xslt)
    }
    
    # without version gets newest?
    test_ok = true
    ($log.warn("error bei getSpecificationById"); test_ok = false) if (s[0]!=s[-1])
    ($log.warn("error bei getSpecificationByIdWithTransform" ); test_ok = false) if (t[0]!=t[-1])
    ($log.warn("error bei getSpecificationByIdWithTransform with xslt"); test_ok = false) if (x[0]!=x[-1])
    assert test_ok, "unexpected"
    $log.info("getSpecification... without version gets the newest version - ok")
    
    # without xslt same as getSpecificationById ?
    test_ok = true
    s.count.times{|i| test_ok = (s[i]==t[i]) }
    assert test_ok, "specifications fetched by getSpecificationByIdWithTransformation and getSpecificationById differ"
    $log.info("getSpecificationByIdWithTransformation without xslt gets the same as getSpecificationById - ok")
    
    # check xslt ??? - no
    # agreed with s.steidten und r.boerner - implicitly tested while ARMOR-communication
    
    # verify that transformation provides sth. different from original
    assert (s[0]!=x[0]) , "transformation provides the same as original"
    $log.info("transformation provides sth different from original spec - ok")
  end
 
  # KeywordTypes and -Values
  # fills @@sfcKeywords
  def test050_get_keyword_types_and_values
    $setup_ok = false
    $log.info "getting keyword types"
    types = (get_keyword_types() or [])
    return if types.count.zero?
    $log.info " - #{types.count} keyword types"
    count = 0
    types.each do |type|
      $log.info("getting keywords for type #{type}")
      res1 = get_values_for_keyword_type(type)
      @@sfcKeywords[type.to_sym] = res1
      if res1.is_a?(Array)
        count += res1.count
        $log.info " - #{res1.count} keyword values in category #{type}" #\n#{res1.inspect.slice(1..30)+'...'}"
      else
        count += 1
        $log.info " - 1 keyword value in category #{type}"
      end
    end
    $log.info "summary"
    $log.info " - #{count} keyword category-value pairs"
    $setup_ok = true
  end
  
  # findSpecificationsByKeywords
  # uses @@sfcKeywords filled by test_50...
  def test060_find_specifications_by_keywords
    specsToFind = @@count_find_specs_by_keywords.to_i
    # this uses the keword-type-value-pairs from test050_...    
    ($log.warn("-- NOT TESTABLE -- no keywords found (run test050 also)"); return) if @@sfcKeywords.keys.count.zero?
    
    @@sfcKeywords.keys.each do |key|
      @@sfcKeywords[key].each do |val|
        if (specsToFind > 0)
          res0 = $sfc.findSpecificationsByKeywords({:key=>key.to_s, :value=>val.to_s})
          assert_equal "true", res0[:success], "Invalid reply: #{res0.inspect}"
          assert_equal "SUCCESS", res0[:errorCode], "Invalid reply: #{res0.inspect}"
          cnt = 0
          spec_list = []
          spec_list = res0[:result][:specList] if res0[:result].has_key?(:specList)
          spec_list = [spec_list] if spec_list.kind_of?(Hash)
          cnt = spec_list.count
          $log.warn("no spec found for #{key} = #{val}. This should not occur") if cnt.zero?
          specsToFind -= cnt
          $log.info " - #{cnt} specs found: #{key} - #{val} (#{[specsToFind,0].max} to find)" unless cnt.zero?
        end  
      end
    end
  end
 
  # findSpecificationsByMetadata
  # fills @@sfcUsersSpecs
  def test070_find_specifications_by_metadata
    $setup_ok = false
    # this finds only specs of the author 
    res0 = $sfc.findSpecificationsByMetadata({:author => @@defaultUser})
    assert_equal "true", res0[:success]
    assert_equal "SUCCESS", res0[:errorCode]
    # store the specifications in hash
    if res0[:result].has_key?(:specList)
      res0[:result][:specList].each do |spec|
        # if only spec[:id] is used there are problems if working copies exist
        specid = Digest::MD5.hexdigest(spec.to_s)
        @@sfcUsersSpecs[specid.to_sym] = spec
      end
      $log.info(" - #{@@sfcUsersSpecs.count} specifications from author #{@@defaultUser} found" + (("true" == res0[:result][:limitExceeded]) ? " (limited)" : ""))
    else
      $log.info(" - no specifications from author #{@@defaultUser} found")
    end
    $setup_ok = true
  end

  def test071_jira1021
    res1 = $sfc.findSpecificationsByMetadata({:pcrb=>"xyz123"}) #{'classId' => cl.to_s}})
    found_specs = res1[:result][:specList]
    ($log.warn("No specs found for jira 1021 test of findSpecificationByMetadata()"); return) if found_specs.nil?
    found_specs = [found_specs] if found_specs.kind_of?(Hash)
    
    $log.info("#{found_specs.count} specifications found with pcrb=xyz123")
  end

  # find an arbitrarily specification
  def test072_use_all_keys_for_find    
    res1 = $sfc.findSpecificationsByMetadata({:owner=>@@defaultUser, :templateVersion=>"*"}) 
    found_specs = res1[:result][:specList]
    ($log.warn "No specs found for elaborately test of findSpecificationByMetadata()"; return) if found_specs.nil?
    found_specs = [found_specs] if found_specs.kind_of?(Hash)
    if found_specs.class==Hash
      selected_spec = found_specs
    else
      idx = rand(found_specs.count)
      selected_spec = found_specs[idx]
    end
	
    test_ok = true
    res2 = []
    selected_spec.each {|k,v|
      next if (k=="attachmentReferences")
      next if (k=="author")
      cycle_ok = true
      msg = " - #{k} => #{v}"
      res2 = $sfc.findSpecificationsByMetadata({:owner=>@@defaultUser, k=>v})
      (cycle_ok = false; msg += "\nfatal: no response") if res2.nil? 
      (cycle_ok = false; msg += "\nwarning: limit exceeded") if cycle_ok && res2[:result][:limitExceeded]=="true"
      found_specs = res2[:result][:specList]
      (cycle_ok = false; msg += "\nno specs found") if found_specs.nil?
      (test_ok = false; $log.info msg; next) if !cycle_ok
      found_specs = [found_specs] if found_specs.class == Hash
      msg += "\n   #{found_specs.count} specs found" if cycle_ok
      $log.info msg
      a=found_specs.collect{|s| s.has_key?(:id) && (s[:id]==selected_spec[:id])}
      test_ok &= cycle_ok
    }
    assert test_ok, "findSpecificationsByMetadata not as expected"
  end
  
  def test073_find_specification_by_classes
    test_ok = true
    @@sfcClasses.keys.each {|cl|
      this_cycle = true
      res1 = $sfc.findSpecificationsByMetadata({:classId => cl.to_s}) 
      $log.warn("classId #{cl} - limit exceeded") if this_cycle && ("true"==res1[:result][:limitExceeded])
      $log.info("classId #{cl} - " + ((res1[:result].include?(:specList)) ? res1[:result][:specList].count.to_s : "no") + " specifications found") if this_cycle
      test_ok &= this_cycle
    }
    assert test_ok, "findSpecificationsByMetadata failed"
    $log.info("findSpecificationsByMetadata succeeded")
  end
  
  # getSpecification
  # use @@sfcUsersSpecs filled by test_70...
  def test080_get_specifications
    # uses specifications found by test070...
    
    @@sfcUsersSpecs.keys.each do |specid|
      spec = @@sfcUsersSpecs[specid]
      res0 = get_specification(spec[:id], spec[:version])
    end
  end
 
  # getSpecificationHistory
  # use @@sfcUsersSpecs filled by test_70...
  def test090_get_specification_history
    # uses specifications found by test060...
        
    @@sfcUsersSpecs.keys.each do |specid|
      spec = @@sfcUsersSpecs[specid]
      res0 = $sfc.getSpecificationHistory(spec[:id]) #unless spec['id']=="C05-00000058"
      assert_equal false, res0.nil?
      assert_equal "true", res0[:success]
      assert_equal "SUCCESS", res0[:errorCode]
      $log.info " - #{spec[:id]} specification history fetched"
    end
  end
  
    # clens up test specifications from former tests
  # use @@sfcUsersSpecs filled by test_70...
  def self.after_all_passed

    count = 0
    # clean up test specs 
    @@sfcUsersSpecs.keys.each do |specid|
      spec = @@sfcUsersSpecs[specid]
      if (spec['title'] == @@test_specs["05_armor_1"][:title]) #&& (spec[:status] == "New") && (spec[:version] == "0")
        if "false"==spec['workingCopy']
          delete_specification(spec['id'], false)
          count += 1
          $log.info "#{spec['id']} #{spec['workingCopy']} #{spec['version']} #{spec['status']} #{spec['title']} "
        end
      end
    end

    if count == 0
      $log.info " - no specifications to cleanup"
    else
      $log.info " - #{count} specifications/working copies deleted"
    end
  end
  
end
