=begin
(c) Copyright 2011 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

 author      - ulrich.koerner@globalfoundries.com, daniel.steger
 date        - 2012/12/10

 http://vf1sfcq01:20080/alfresco/ - login -  Company Home -> Data Dictionary -> Workflow Defintions
 
 v 1.11 changes
 all review-author-tasks replace transition "CANCEL" with transitions "REWORK" and "DISCARD" 
 workflow flag "reworkRequired" is omitted
 REWORK is "Reedit" in GUI, DISCARD is "Discard Changes" in GUI
=end

require_relative 'Test_SetupFC_Setup'

#  automated test of setup.FC API 
class Test_SetupFC_Keywords < Test_SetupFC_Setup
  Description = "Test SetupFC XML Interface Keywords"
  
  def test700_keyword_life_cycle
    # test keyword changes over spec life cycle - jira 1375
  
    used_keyword_type = "EquipmentID" #"Equipment - Scope"
    h = Time.now.to_i # every test run uses new equipment
    eqp1 = "eqp#{h-1}"
    eqp2 = "eqp#{h+1}"

    # create a EOPM spec
    sp_new = create_testspec_hash("07", "EOPM", "EOPM", "C07-FAT-18", {:fabLocation=>@@fabLocation})
    sp_id = create_new_specification(sp_new)

    # verify no specs with this equipment are found
    keys0 = keys1 = get_values_for_keyword_type(used_keyword_type)
    assert !keys1.member?(eqp1), "use other equipment than #{eqp1}"
    assert !keys1.member?(eqp2), "use other equipment than #{eqp2}"
    $log.info("keywords[\"#{used_keyword_type}\"] contains neither #{eqp1} nor #{eqp2}\n")

    # content change eqp1, submit
    content_change(sp_id)
    assert $sfc.wait_for_edit_content(sp_id), "no edit task" 
    submit_specification(sp_id, :type=>"EOPM", :additional_equipment => eqp1)
    assert $sfc.wait_for_approve([sp_id, {:user=>@@approver1}]), "no approve task"
    $sfc.human_workflow_action(sp_id, :action=>"APPROVE", :user=>@@approver1)
    sleep 120

    assert wait_for{get_values_for_keyword_type(used_keyword_type).member?(eqp1)}, "keywords[\"#{used_keyword_type}\"] does not contain #{eqp1}"
    assert !get_values_for_keyword_type(used_keyword_type).member?(eqp2), "use other equipment than #{eqp2}"
    $log.info("keywords[\"#{used_keyword_type}\"] contains #{eqp1}\n")
    
    # content change eqp2, reject
    content_change(sp_id)
    assert $sfc.wait_for_edit_content(sp_id), "no edit task" 
    submit_specification(sp_id, :type=>"EOPM", :additional_equipment => eqp2)
    assert $sfc.wait_for_approve([sp_id, {:user=>@@approver1}]), "no approve task"
    $sfc.human_workflow_action(sp_id, :action=>"REJECT", :user=>@@approver1)
    assert (task_list = $sfc.wait_for_review(sp_id)), "no edit task" 
    $sfc.human_workflow_action(sp_id, :action=>"DISCARD", :requireRework=>"false") # v 1.11 cancel is invalid, use discard now, require rework flag omitted
    
    sleep 120
    assert wait_for{get_values_for_keyword_type(used_keyword_type).member?(eqp1)}, "keywords[\"#{used_keyword_type}\"] does not contain #{eqp1}"
    assert !get_values_for_keyword_type(used_keyword_type).member?(eqp2), "use other equipment than #{eqp2}"
    $log.info("keywords[\"#{used_keyword_type}\"] contains #{eqp1}\n")
    
    # content change eqp2, submit
    content_change(sp_id)
    assert $sfc.wait_for_edit_content(sp_id), "no edit task" 
    submit_specification(sp_id, :type=>"EOPM", :additional_equipment => eqp2)
    assert $sfc.wait_for_approve([sp_id, {:user=>@@approver1}]), "no approve task"
    $sfc.human_workflow_action(sp_id, :action=>"APPROVE", :user=>@@approver1)
    
    sleep 120
    assert wait_for{!get_values_for_keyword_type(used_keyword_type).member?(eqp1)}, "keywords[\"#{used_keyword_type}\"] contains #{eqp1}"
    assert wait_for{get_values_for_keyword_type(used_keyword_type).member?(eqp2)}, "keywords[\"#{used_keyword_type}\"] does not contain #{eqp2}"
    $log.info("keywords[\"#{used_keyword_type}\"] contains #{eqp2}\n")

    # delete keywords
    $log.info("delete all keywords for spec #{sp_id}")
    $sfc.support.deleteKeywords(sp_id)
    assert wait_for{!get_values_for_keyword_type(used_keyword_type).member?(eqp1)}, "keywords[\"#{used_keyword_type}\"] contains #{eqp1}"
    assert wait_for{!get_values_for_keyword_type(used_keyword_type).member?(eqp2)}, "keywords[\"#{used_keyword_type}\"] contains #{eqp2}"
    $log.info("keywords[\"#{used_keyword_type}\"] contains neither #{eqp1} nor #{eqp2}\n")

    # property change, cancel
    properties_change(sp_id)
    assert $sfc.wait_for_edit_metadata(sp_id), "no edit task" 
    submit_properties_change(sp_id)
    assert $sfc.wait_for_approve([sp_id, {:user=>@@approver2}]), "no approve task"
    $sfc.human_workflow_action(sp_id, :action=>"REJECT", :user=>@@approver2)
    assert $sfc.wait_for_review(sp_id), "no edit task" 
    $sfc.human_workflow_action(sp_id, :action=>"DISCARD", :requireRework=>"false")

    sleep 120
    assert wait_for{!get_values_for_keyword_type(used_keyword_type).member?(eqp1)}, "keywords[\"#{used_keyword_type}\"] contains #{eqp1}"
    assert wait_for{!get_values_for_keyword_type(used_keyword_type).member?(eqp2)}, "keywords[\"#{used_keyword_type}\"] contains #{eqp2}"
    $log.info("keywords[\"#{used_keyword_type}\"] contains neither #{eqp1} nor #{eqp2}\n")

    # property change, approve
    properties_change(sp_id)
    assert $sfc.wait_for_edit_metadata(sp_id), "no edit task" 
    submit_properties_change(sp_id)
    assert $sfc.wait_for_approve([sp_id, {:user=>@@approver2}]), "no approve task"
    $sfc.human_workflow_action(sp_id, :action=>"APPROVE", :user=>@@approver2)
    assert $sfc.wait_for_approve([sp_id, {:user=>@@approver2}]), "no approve task"
    $sfc.human_workflow_action(sp_id, :action=>"APPROVE", :user=>@@approver2)

    sleep 120
    assert wait_for{!get_values_for_keyword_type(used_keyword_type).member?(eqp1)}, "keywords[\"#{used_keyword_type}\"] contains #{eqp1}"
    assert wait_for{get_values_for_keyword_type(used_keyword_type).member?(eqp2)}, "keywords[\"#{used_keyword_type}\"] does not contain #{eqp2}"
    acknowledge(sp_id)
    $log.info("keywords[\"#{used_keyword_type}\"] contains #{eqp2}\n")

    # terminate spec, cancel
    termination(sp_id)
    assert $sfc.wait_for_terminate(sp_id), "no edit task" 
    submit_termination(sp_id)
    assert $sfc.wait_for_approve([sp_id, {:user=>@@approver1}]), "no approve task"
    $sfc.human_workflow_action(sp_id, :action=>"REJECT", :user=>@@approver1)
    assert (task_list = $sfc.wait_for_review(sp_id)), "no edit task" 
    $sfc.human_workflow_action(sp_id, :action=>"DISCARD", :requireRework=>"false")
    
    sleep 120
    assert wait_for{!get_values_for_keyword_type(used_keyword_type).member?(eqp1)}, "keywords[\"#{used_keyword_type}\"] contains #{eqp1}"
    assert wait_for{get_values_for_keyword_type(used_keyword_type).member?(eqp2)}, "keywords[\"#{used_keyword_type}\"] does not contain #{eqp2}"
    $log.info("keywords[\"#{used_keyword_type}\"] contains #{eqp2}\n")
    
    # terminate spec, approve
    termination(sp_id)
    assert $sfc.wait_for_terminate(sp_id), "no edit task" 
    submit_termination(sp_id)
    assert $sfc.wait_for_approve([sp_id, {:user=>@@approver1}]), "no approve task"
    $sfc.human_workflow_action(sp_id, :action=>"APPROVE", :user=>@@approver1)
    
    sleep 120
    assert wait_for{!get_values_for_keyword_type(used_keyword_type).member?(eqp1)}, "keywords[\"#{used_keyword_type}\"] contains #{eqp1}"
    assert wait_for{!get_values_for_keyword_type(used_keyword_type).member?(eqp2)}, "keywords[\"#{used_keyword_type}\"] contains #{eqp2}"
    $log.info("keywords[\"#{used_keyword_type}\"] contains neither #{eqp1} nor #{eqp2}\n")
    
    # $sfc.support.deleteKeywords(sp_id)
    delete_specification(sp_id)
  end
  
  def xtest710_multible_keywords_same_type
    keys = (get_keyword_types() or [])
    res1 = get_values_for_keyword_type(keys[0])
    pp res1 # ["MPX601", "MPX600", "ETX102", "ETX101", "ETX100"]
    # res1.each{|k| delete_keyword(k)}
    # res2 = get_values_for_keyword_type(keys[0])
    # verify res2.zero?
    # create spec multiple keys "Equipment - Referenced" 
    # submit
    # verify all keawords are stored
    # delete spec
    # extractAndPersistKeywords(arg0, arg1) to restore state before test
    # CAVEAT: that all is not possible, because commitKeywords only via jmx available
    # res3 = get_values_for_keyword_type(keys[0])
    # verify res1==res3
  end
  
end