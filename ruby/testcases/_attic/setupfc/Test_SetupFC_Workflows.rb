=begin
 automated test of setup.FC API 

(c) Copyright 2011 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

 author      - ulrich.koerner@globalfoundries.com, daniel.steger
 date        - 2012/12/10
=end

require "Test_SetupFC_Setup"

#  automated test of setup.FC API 
class Test_SetupFC_Workflows < Test_SetupFC_Setup
  Description = "Test SetupFC XML Interface workflows"
  
  # spec create/delete (@@lot_create_specs specifications)
  def test10_create_get_delete_specification    
    assert !@@test_specs.count.zero?, "have to run test000... too! "
    specs = []

    $log.info("Create specifications")
    t_start = Time.now

    @@loop_create_specs.to_i.times { specs << (sp_id = create_new_specification(@@test_specs["05_armor_1"])) }
    $log.info " - #{specs.count} specifications created in #{(Time.now-t_start).to_f} seconds"

    $log.info("Retrieve specifications")
    t_start = Time.now
    specs_get = specs.collect { |sp_id| get_specification(sp_id) }
    $log.info " - #{specs_get.count} specifications fetched in #{(Time.now-t_start).to_f} seconds"

    $log.info("Delete specifications")
    t_start = Time.now
    specs.each { |sp_id| delete_specification(sp_id, false) }
    $log.info " - #{specs.count} specifications deleted in #{(Time.now-t_start).to_f} seconds"
  end

  
  # create/delete specification and working copy
  def test12_create_workingcopy_delete
    assert !@@test_specs.count.zero?, "have to run test000... too! "
    
    $log.info("Create specification")
    spId = create_new_specification(@@test_specs["05_armor_1"])
    
    $log.info("Start edit workflow")
    $sfc.content_change(spId)
    
    $log.info("Add/delete attachments and references")
    add_delete_attachment(spId, 1, @@test_specs["05_armor_1"]) 
    add_delete_reference(spId, 1)

    $log.info("Delete specification")
    delete_specification(spId)
  end
  
  # create/delete specification and working copy
  def test14_create_add_reference
    assert !@@test_specs.count.zero?, "have to run test000... too! "
    
    $log.info("Create specification")
    spec_id1 = create_new_specification(@@test_specs["02_base_1"])
    spec_id2 = create_new_specification(@@test_specs["02_base_2"])
    
    $log.info("Start edit workflow")
    assert_nothing_raised do
      $sfc.content_change(spec_id1)
      $sfc.content_change(spec_id2)
    end        
    
    $log.info("Add/delete references")
    add_delete_reference(spec_id1, 1)
    add_delete_reference(spec_id2, 1)

    $log.info("Delete specification")
    delete_specification(spec_id1)
    delete_specification(spec_id2)
  end
  
  # get the workflow-id
  def test20_getworkflow_id
    assert !@@test_specs.count.zero?, "have to run test000... too! "
    test_spec = @@test_specs["02_base_1"]
    
    $log.info("Create specification")
    assert (sp_id = create_new_specification(test_spec)), "error creating specification"
    
    $log.info("Start edit workflow")
    content_change(sp_id)    
    assert $sfc.wait_for_edit_content(sp_id), "no edit task" 

    $log.info("Retrieve workflow")
    res0 = $sfc.support.getActiveWorkflowsForSpec(sp_id)
    assert res0.has_key?(:getActiveWorkflowsForSpecResponse) & (res = res0[:getActiveWorkflowsForSpecResponse]).has_key?(:return), "no workflow found"
    wf = res[:return][:workflowDefinition]
    $log.info " - workflow #{wf[:title]} (id: #{wf[:id]}) found for #{sp_id}" 

    $log.info("Delete specification")
    delete_specification(sp_id)
  end
  
  def test22_cancel_workflow_user
    assert !@@test_specs.count.zero?, "have to run test000... too! "
    test_spec = @@test_specs["08_base_3"]
      
    $log.info("Create specification")
    assert (sp_id = create_new_specification(test_spec)), "no specification created" 
    
    $log.info("Start edit workflow")
    content_change(sp_id)
    assert $sfc.wait_for_edit_content(sp_id), "no edit task" 

    $log.info("Submit content change")
    submit_specification(sp_id, :type=>test_spec[:template])
    assert $sfc.wait_for_approve(sp_id), "no approve task"
    
    $log.info("Cancel approve task")
    erg = $sfc.request_cancel_signoff(sp_id)
    assert_equal "CANCEL_REQUESTED", erg, "Can cancel workflow"
    assert $sfc.wait_for_edit_content(sp_id), "no edit task" 
    task_list = $sfc.get_user_tasks({:specid=>sp_id})
    $log.info(" - #{sp_id} task #{task_list[0][:workflowProperties][:businessProcess]} still exists")
    
    # First tier approved in first cycle --> Cancel is still possible
    $log.info("Submit content change once more")
    submit_specification(sp_id, :type=>test_spec[:template])
    assert $sfc.wait_for_approve(sp_id), "no approve task"
    human_workflow_action(sp_id, "APPROVE")
    
    erg = $sfc.request_cancel_signoff(sp_id)
    assert_equal "CANCEL_REQUESTED", erg, "Can cancel workflow"
    assert $sfc.wait_for_edit_content(sp_id), "no edit task" 
    task_list = $sfc.get_user_tasks({:specid=>sp_id})
    $log.info(" - #{sp_id} task #{task_list[0][:workflowProperties][:businessProcess]} still exists")
    
    # Task in Review - cancel is possible
    $log.info("Submit content change once more")
    submit_specification(sp_id, :type=>test_spec[:template])
    assert $sfc.wait_for_approve(sp_id), "no approve task"

    $log.info("Reject approve")
    human_workflow_action(sp_id, "REJECT")
    assert (task_list = $sfc.wait_for_review(sp_id)), "no edit task" 
    
    $log.info("Cancel review task")
    erg = $sfc.request_cancel_signoff(sp_id)
    assert_equal "CANCEL_REQUESTED", erg, "Can cancel workflow"
    assert $sfc.wait_for_edit_content(sp_id), "no edit task" 

    $log.info("Delete specification")
    delete_specification(sp_id)
  end

  def test24_cancel_workflow_useradmin
    $log.info("Find an USERADMIN who is no ADMIN")
    users = $sfc.users_for_group("USERADMIN") - $sfc.users_for_group("ADMIN")
    assert users.count > 0, "No user found!"
    user = users[0]
    $log.info "Use account #{user} to cancel workflow"

    test_spec = @@test_specs["08_base_3"]

    $log.info("Create specification")
    sp_id = create_new_specification(test_spec)

    $log.info("Start workflow")
    content_change(sp_id)
    assert $sfc.wait_for_edit_content(sp_id), "no edit task"
    submit_specification(sp_id, :type=>test_spec[:template])
    assert $sfc.wait_for_approve(sp_id), "no approve task"

    $log.info("Cancel approve task by #{user}")
    erg = $sfc.request_cancel_signoff(sp_id, :user=>user)
    assert_equal "CANCEL_REQUESTED", erg, "Can cancel workflow"

    $log.info("Verify an edit task rises")
    assert $sfc.wait_for_edit_content(sp_id), "no edit task to continue"
    task_list = $sfc.get_user_tasks({:specid=>sp_id})
    $log.info(" - #{sp_id} task #{task_list[0][:workflowProperties][:businessProcess]} still exists")
    
    $log.info("Delete specification")
    delete_specification(sp_id)
  end
    
  # # # # #
  
  # complete workflow
  def test30_complete_workflow_different_user_spelling
    assert !@@test_specs.count.zero?, "have to run test000... too! "

    def capitalize_user(patt='')
      user = @@defaultUser.split("")
      patt.split("").each_with_index{|c,i| user[i]=user[i].upcase if c=="X"}
      return user.join()
    end
    
    delay_time = 15

    # cresate a spec
    $sfc.support.user = capitalize_user 'XxXxxxX'
    spec_id = create_new_specification(@@test_specs["08_base_1"])	# [4]
    
    #start edit workflow (creates a working copy)    
    $sfc.support.user = capitalize_user 'XxxxxxX'
    content_change(spec_id)
    submit_specification(spec_id, :type=>"Base")
    
    $sfc.support.user = capitalize_user 'xxxxxXX'
    human_workflow_action(spec_id, "CANCEL", :sleep=>delay_time)
    
    sleep delay_time
    $sfc.support.user = capitalize_user 'XxxxXxX'

    sleep delay_time
    $sfc.service.user = capitalize_user 'xxxxxxx'    
    submit_specification(spec_id)
    
    $sfc.support.user = capitalize_user 'xxxxxxX'
    human_workflow_action(spec_id, "REJECT", :sleep=>delay_time)
    
    $sfc.support.user = capitalize_user 'xxxxxXX'
    human_workflow_action(spec_id, "DISCARD", :sleep=>delay_time)
    
    sleep delay_time
    $sfc.service.user = capitalize_user 'xxxxXXX'
    content_change(spec_id)
    sleep delay_time
    submit_specification(spec_id)
    
    $sfc.support.user = capitalize_user 'xxxXXXX'
    human_workflow_action(spec_id, "REJECT", :sleep=>delay_time)
    
    $sfc.support.user = capitalize_user 'xxXXXXX'    
    human_workflow_action(spec_id, "RETRY", :sleep=>delay_time)
    
    $sfc.support.user = capitalize_user 'XXXXXXX'
    human_workflow_action(spec_id, "APPROVE", :sleep=>delay_time)
    
    sleep delay_time
    $sfc.support.user = capitalize_user 'XxXxXXX'
    acknowledge(spec_id)
    
    sleep delay_time
    delete_specification(spec_id)
  end
  
  # create, approve, terminate spec
  def test31_complete_base_workflow
    assert !@@test_specs.count.zero?, "have to run test000... too! "
    
    #finish_spec "TEST-1334826046-SPEC"
    process_spec(@@test_specs["02_base_1"])
  end

  def test32_complete_armor_workflow
    assert !@@test_specs.count.zero?, "have to run test000... too! "
    
    #finish_spec "M05-00000165"  
    process_spec(@@test_specs["05_armor_1"]) 
  end
  
  def test33_eopmca_workflow
    assert !@@test_specs.count.zero?, "have to run test000... too! "
    ($log.info("fab 8 only"); return) unless @@fabLocation == "M"

    #finish_spec "M07-00000061"  
    process_spec(@@test_specs["07_eopmca_1"])	# [5]
  end
  
  def test34_eopmqual_workflow
    assert !@@test_specs.count.zero?, "have to run test000... too! "
    ($log.info("fab 8 only"); return) unless @@fabLocation == "M"

    #finish_spec "M07-00000062"   
    process_spec(@@test_specs["07_eopmqual_1"])	# [6]
  end
  
  def test35_eopmpm_workflow
    assert !@@test_specs.count.zero?, "have to run test000... too! "
    ($log.info("fab 8 only"); return) unless @@fabLocation == "M"

    #finish_spec "M07-00000063"    
    process_spec(@@test_specs["07_eopmpm_1"])	# [7]
  end
  
  def test36_eopm_workflow
    assert !@@test_specs.count.zero?, "have to run test000... too! "
    ($log.info("fab 8 only"); return) unless @@fabLocation == "M"

    #finish_spec "M07-00000064"  
    process_spec(@@test_specs["07_eopm_1"])	# [9]
  end
  
  def test37_space_workflow
    assert !@@test_specs.count.zero?, "have to run test000... too! "
    ($log.info("fab 8 only"); return) unless @@fabLocation == "M"

    process_spec(@@test_specs["07_table_1"])	# [8]
  end
  
  def test38_sp_workflow
    assert !@@test_specs.count.zero?, "have to run test000... too! "
    ($log.info("fab 8 only"); return) unless @@fabLocation == "M"

    #finish_spec "M07-00000064"  
    process_spec(@@test_specs["05_sp_1"])	# [11]
  end
    
  def test39_arac_workflow
    assert !@@test_specs.count.zero?, "have to run test000... too! "
    ($log.info("fab 8 only"); return) unless @@test_specs["05_arac_1"]

    #finish_spec "M07-00000064"  
    process_spec(@@test_specs["05_arac_1"])	# [10]
  end
  
  def test50_longer_delay_before_acknowledgments
    version_count = 2
    delay = 300
    # create spec
    test_spec = @@test_specs["02_base_1"]
    test_spec[:id] = create_test_id()
    assert (sp_id = create_new_specification(test_spec)), "no specification created" 
    #
    # create version_count versions
    ack_count = 0
    version_count.times {
      ack_count += 1
      # edit
      $log.info("Start edit workflow")
      content_change(sp_id)
      assert $sfc.wait_for_edit_content(sp_id), "no edit task"
      $log.info("Submit content change")
      submit_specification(sp_id, :type=>test_spec[:template])
      # approve
      assert $sfc.wait_for_approve(sp_id, :user=>@@approver2)
      approve_content(sp_id)
      ### wait second acknowledge task is created
      assert $sfc.wait_for_acknowledge(sp_id, :user=>@@approver2), "missing acknowledge task"
    }
    #
    $log.info("delaying acknowledgement for #{delay} sec")
    sleep delay # essential for this test, does not fail without delay 
    # acknowledge all
    $sfc.acknowledge(sp_id, :all=>true)
    
    # check if audit trail is complete
    assert version_count.times.inject(true) {|res,i|
      res &= verify_ack_entry(sp_id, i+1)
    }, " acknowledgement not shown correctly in audit trail" # expected failure <= v1.10
  end
  
  def test60_workflow_config
    sclass = @@test_specs["02_base_1"][:classId]
    subclass = @@test_specs["02_base_1"][:subClassId]
    # create workflow config
    wf_id = $sfc.create_workflow_config("QA Test #{@@version}", 
      :classId=>sclass, 
      :subClassId=>subclass, 
      :fablocation=>@@fabLocation)
      
    # find workflow config
    wfs = $sfc.find_workflow_configs(sclass, subclass)
    assert_equal 1, wfs.select {|a| a[:metaInformation][:workflowConfigId] == wf_id}.count, " workflow config not found"
    # delete it
    assert $sfc.delete_workflow_config(wf_id), "failed to delete workflow config"
    
    wfs = $sfc.find_workflow_configs(sclass, subclass)
    assert_equal 0, wfs.select {|a| a[:metaInformation][:workflowConfigId] == wf_id}.count, " no workflow config expected"
  end
  
  def test61_delete_tiers
    pend "need approval status API"
    
    test_spec = @@test_specs["02_base_1"]
        
    sclass = test_spec[:classId]
    subclass = test_spec[:subClassId]
    # wf_id = $sfc.create_workflow_config("QA Test #{@@version}", 
      # :classId=>sclass, 
      # :subClassId=>subclass, 
      # :fablocation=>@@fabLocation,
      # :normal_approval=> {"type"=>"normal_approval", :"wor:approvalCycle"=>{"id"=>"approval1", :"wor:approvalTier"=>
        # [{"order"=>"1", :"wor:groupConfig"=>{"group"=>"QA"}},
         # {"order"=>"2", :"wor:groupConfig"=>{"group"=>"QA"}},
         # {"order"=>"3", :"wor:groupConfig"=>{"group"=>"QA"}}]}}
    # )
    wf_id = "M02-ALL-1405069457"
    test_spec[:workflowConfig] = wf_id
    
    specs = (0..2).collect { |i|
      test_spec[:id] = create_test_id()
      spec_id = create_new_specification(test_spec)
      content_change(spec_id)    
      assert $sfc.wait_for_edit_content(spec_id), "no edit task"
      submit_specification(spec_id, test_spec)
      i.times {
        assert $sfc.wait_for_approve(spec_id), "no approve task"
        human_workflow_action(spec_id, "APPROVE")
      }
      spec_id
    }
    $log.info "Workflow config: #{wf_id}"
    $log.info "Created specs: #{specs.inspect}"
    
    
    assert sfc.update_workflow_config( wf_id, sclass, subclass, :normal_approval=> 
      {"type"=>"normal_approval", :"wor:approvalCycle"=>{"id"=>"approval1", :"wor:approvalTier"=>
         {"order"=>"1", :"wor:groupConfig"=>{"group"=>"QA"}}
    }}), "failed to update workflow"
    
    specs.each_with_index {|spec,i|
      (3-i).times {
        assert $sfc.wait_for_approve(spec_id), "no approve task"
        human_workflow_action(spec_id, "APPROVE")
      }
    }
    # TODO: Delete the workflow config
  end
  
  def test62_delete_approval_tier
    pend "need approval status API"
    
    test_spec = @@test_specs["02_base_1"]
        
    sclass = test_spec[:classId]
    subclass = test_spec[:subClassId]
    wf_id = $sfc.create_workflow_config("QA Test #{@@version}", 
      :classId=>sclass, 
      :subClassId=>subclass, 
      :fablocation=>@@fabLocation,
      :normal_approval=> {"type"=>"normal_approval", :"wor:approvalCycle"=>{"id"=>"approval1", :"wor:approvalTier"=>
        [{"order"=>"1", :"wor:groupConfig"=>{"group"=>"QA"}},
         {"order"=>"2", :"wor:groupConfig"=>{"group"=>"F8_CIMQA"}},
         {"order"=>"3", :"wor:groupConfig"=>{"group"=>"QA"}}]}}
    )
    ##wf_id = "M02-ALL-1405069457"
    test_spec[:workflowConfig] = wf_id
    
    spec_id = create_new_specification(test_spec)
    content_change(spec_id)    
    assert $sfc.wait_for_edit_content(spec_id), "no edit task"
    submit_specification(spec_id, test_spec)
    assert $sfc.wait_for_approve(spec_id), "no approve task"
    human_workflow_action(spec_id, "APPROVE")
    
    $log.info "Workflow config: #{wf_id}"
    $log.info "Created spec: #{spec_id.inspect}"
    
    pend "need approval status API"
    
    assert sfc.update_workflow_config( wf_id, sclass, subclass, :normal_approval=> 
      {"type"=>"normal_approval", :"wor:approvalCycle"=>{"id"=>"approval1", :"wor:approvalTier"=>
         [{"order"=>"1", :"wor:groupConfig"=>{"group"=>"QA"}},
          {"order"=>"2", :"wor:groupConfig"=>{"group"=>"QA"}}]
    }}), "failed to update workflow"
    
    2.times {
      assert $sfc.wait_for_approve(spec_id), "no approve task"
      human_workflow_action(spec_id, "APPROVE")
    }
  end
  
  def test63_delete_approval_tier
    pend "need approval status API"
    test_spec = @@test_specs["02_base_1"]
        
    sclass = test_spec[:classId]
    subclass = test_spec[:subClassId]
    wf_id = $sfc.create_workflow_config("QA Test #{@@version}", 
      :classId=>sclass, 
      :subClassId=>subclass, 
      :fablocation=>@@fabLocation,
      :normal_approval=> {"type"=>"normal_approval", :"wor:approvalCycle"=>{"id"=>"approval1", :"wor:approvalTier"=>
        [{"order"=>"1", :"wor:groupConfig"=>{"group"=>"QA"}},
         {"order"=>"2", :"wor:groupConfig"=>{"group"=>"F8_CIMQA"}},
         {"order"=>"3", :"wor:groupConfig"=>{"group"=>"QA"}}]}}
    )
    ##wf_id = "M02-ALL-1405069457"
    test_spec[:workflowConfig] = wf_id
    
    spec_id = create_new_specification(test_spec)
    content_change(spec_id)    
    assert $sfc.wait_for_edit_content(spec_id), "no edit task"
    submit_specification(spec_id, test_spec)
    assert $sfc.wait_for_approve(spec_id), "no approve task"
    human_workflow_action(spec_id, "APPROVE")
    
    $log.info "Workflow config: #{wf_id}"
    $log.info "Created spec: #{spec_id.inspect}"
    
    pend "need approval status API"
    
    assert sfc.update_workflow_config( wf_id, sclass, subclass, :normal_approval=> 
      {"type"=>"normal_approval", :"wor:approvalCycle"=>{"id"=>"approval1", :"wor:approvalTier"=>
         [{"order"=>"1", :"wor:groupConfig"=>{"group"=>"F8_CIMQA"}},
          {"order"=>"2", :"wor:groupConfig"=>{"group"=>"QA"}}]
    }}), "failed to update workflow"
    
    2.times {
      assert $sfc.wait_for_approve(spec_id), "no approve task"
      human_workflow_action(spec_id, "APPROVE")
    }
  end
  
  # transfer task 
  def test70_task_transfer
    assert !@@test_specs.count.zero?, "have to run test000... too! "

    test_spec = @@test_specs["08_base_3"]
    
    assert_nothing_raised do
      #create an new spec
      spec_id = create_new_specification(test_spec)
      
      #start edit workflow (creates a working copy)
      content_change(spec_id)
      
      assert $sfc.create_attachment(@@defaultAttachmentFileName, @@default_attachment_file, spec_id)
      assert $sfc.create_reference(spec_id, @@spec_transfer)
   
      # Transfer the edit task
      $sfc.assign_task( spec_id , @@second_user )
        
      # Task is not on user task list, but on other user's tasks
      assert !$sfc.wait_for_task(spec_id, :name=>:edit_content, :timeout=>60 ), "Task should not be on user task list"
      assert_equal 1, $sfc.get_user_tasks( :specid=>spec_id, :name=>:edit_content, :user=>@@second_user ).size, "Task should be on user task list"
      
      assert $sfc.delete_reference(spec_id, @@spec_transfer, :user=>@@second_user)
      assert $sfc.delete_attachment(@@defaultAttachmentFileName, spec_id, nil, :user=>@@second_user), "Can delete attachment after transfer"
      
      # Tansfer task back to user
      $sfc.assign_task( spec_id, @@defaultUser, :requestor=>@@second_user )   
      
      # Submit the spec
      submit_specification(spec_id, :type=>test_spec[:template])
      
      # Transfer the approval task
      sleep 20
      $sfc.assign_task( spec_id , @@second_user )
        
      # Task is not on user task list, but on other user's tasks
      assert !$sfc.wait_for_task(spec_id, :timeout=>60, :name=>:approve ), "Task should not be on user task list"
      assert_equal 1, $sfc.get_user_tasks( :specid=>spec_id, :name=>:approve, :user=>@@second_user ).size, "Task should be on user task list"
      
      # Tansfer task back to user
      $sfc.assign_task( spec_id, @@defaultUser, :requestor=>@@second_user )
      
      human_workflow_action(spec_id, "APPROVE")
    end
  end
  
  def test80_attachment_size
    $log.warn("this test consummes a lot of memory")
    test_spec = @@test_specs["08_base_1"]	# [4]
    # cresate a spec
    spId = create_new_specification(test_spec)
    # start edit workflow (creates a working copy)

    res1 = $sfc.content_change(spId)
    
    for i in 7..10
      fn="tst#{i}"      
      data = generate_base64_string(i)
      # test attachment
      add_delete_attachment(spId, 1, :attachment_name=>fn, :attachment => data)      
      $log.info("attached #{data.size} bytes")
      data = nil
    end
    # delete spec
    delete_specification(spId)    
  end
  
  # # # # #
  
  # there are acknowledgements where audit trail is incomplete
  def verify_ack_entry(sp_id, version = 0)
    $log.info("#{__method__} for #{sp_id} version #{version}")    
    res = $sfc.getSpecificationHistory(sp_id, :version=>version)[:result]    
    ackwf = res.select {|e| e[:workflowName] == "Acknowledgement"}
    res = verify_condition("  no acknowledgement workflow") { ackwf.count > 0 }
    return ackwf.inject(true) {|res,e|
      res &= verify_condition("  no 'task-end' in audit trail") { e[:auditEntries].select {|a| a[:eventType] == "task-end"}.count > 0}
    }
  end

end