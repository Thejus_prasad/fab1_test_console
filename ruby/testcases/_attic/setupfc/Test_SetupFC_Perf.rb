=begin
 automated test of setup.FC API 

(c) Copyright 2011 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

 author      - daniel.steger@globalfoundries.com
 date        - 2012/04/27
=end

require "RubyTestCase"
require "setupfc"
require "armor"
require "misc"

# automated test of setup.FC API 
class Test_SetupFC_Perf < RubyTestCase
  Description = "Test SetupFC Performance"

  @@department     = "ALL"
  @@fabLocation = "M"
  @@eqp = "THK2101"
  @@loop_count = 100
  @@threads = 3
  @@timeout = 360

  @@subclasses = "TestWafer Production ToolRecipe Operation FactorySystem ProcessControlPlan Procedure EMS ToolControlPlan ProductionToolRecipe"
  #@@subclasses = "TestWafer"
  @@min_versions = 50


  @@read_methods = ["getSpecificationHistory", "getSpecification", "getSpecificationWithContentTransformation", "getSpecificationMetadata"]

      
  $setup_ok = nil

  def setup
    super
    $setup_ok = ($setup_ok != false)
    assert $setup_ok, "test setup is not correct"
  end
  
  # config/preparation task
  def test000_setup
    $sfc = EasySetupFC.new($env) unless $sfc and $sfc.env == $env
    $setup_ok = true    
  end
    
  def test100_spec_query

    # find specifications with at least @@min_version versions
    found_specs = []
    

    $log.info "Find specs for subclasses #{@@subclasses}"
    @@subclasses.split.each do |subclass|
      res = $sfc.service.findSpecificationsByMetadata(:subClassId=>subclass)
      specs = res[:findSpecificationsByMetadataResponse][:return][:result][:specList]
      specs = [specs] unless specs.is_a?(Array)
      found_specs += specs.find_all {|spec| spec[:version].to_i > @@min_versions}
    end
    assert found_specs.count > 0, "No spec found"
    $log.info "#{found_specs.count} specs found."

    threads = @@threads.times.collect do
      Thread.new do
        result_hash = {}
        sfc = EasySetupFC.new($env)
        @@read_methods.each {|r| result_hash[r] = []}
        @@loop_count.times do |i|
          spec = found_specs[rand*found_specs.size]
          spec_id = spec[:id]
          func = @@read_methods[rand*@@read_methods.size]
          $log.info "#{spec_id}: #{func}"
          sfc.service.send(func, spec_id)
          result_hash[func] << sfc.service.responsetime          
          sleep 5
        end
        result_hash
      end
    end
    res = threads.collect {|t| t.value}

    @@read_methods.each do |k|
      v = res.collect{|h| h[k]}.flatten
      $log.info "#{k}: #{v.count} #{v.average} #{v.min} #{v.max}"
    end
  end

  def test200_create_big_specs
    process_spec("Base", 100, "02", "Production", "M02-2", "Base", "Performance Test")
    #process_spec("Base", 100, "02", "Production", "M02-2", "Base", "Performance Test")
    #process_spec("ARMOR", 100, "05", "ToolRecipe", "M05-1", "ARMOR", "Performance Test")
  end

  # Process a list of specs and check the result (reject the given version)
  def process_spec(spectype, count, specclass, subclass, workflow, template, title, reject=nil)

    id = specclass == "02" ? "M02-SFCPERF-#{Time.now.to_i}" : nil

    spec_id = $sfc.create_new_specification(specclass, subclass, workflow, template, :title=>title, :id=>id)
    assert_not_nil spec_id, "Failed to get new specification"
    version = 0

    count.times do |i|

      $log.info "Create a new version"
      $sfc.content_change(spec_id)
      res, version = $sfc.submit_specification(spec_id, :type=>spectype)
      $log.info "#{spec_id}, Version is #{version}"

      if reject == i+1
        $log.info "Rollback version #{reject}"
        $sfc.human_workflow_action(spec_id, :action=>"REJECT", :timeout=>@@timeout, :polling=>true)
        $sfc.human_workflow_action(spec_id, :action=>"CANCEL", :timeout=>@@timeout, :polling=>true)
      else
        $sfc.human_workflow_action(spec_id, :timeout=>@@timeout, :polling=>true)
        $sfc.acknowledge(spec_id, :timeout=>@@timeout, :polling=>true)
      end
    end


    #$sfc.terminate(spec_id)
    #$sfc.human_workflow_action(spec_id, :action=>"SUBMIT", :sleep=>@@timeout)
    #$sfc.human_workflow_action(spec_id, :sleep=>@@timeout)
    #$sfc.acknowledge(spec_id)
  end

  # Does not work yet
  def test300_rest_interface

    # find specifications with at least @@min_version versions
    found_specs = []

    $log.info "Find specs for subclasses #{@@subclasses}"
    @@subclasses.split.each do |subclass|
      specs = $sfc.rest.get_spec(:args=>{"subClass"=>subclass})
      specs = specs[:collection][:specification]
      specs = [specs] unless specs.is_a?(Array)
      found_specs += specs.find_all {|spec| spec[:version].to_i > @@min_versions}
    end
    assert found_specs.count > 0, "No spec found"
    $log.info "#{found_specs.count} specs found."

    results = []
    @@loop_count.times do |i|
      spec = found_specs[rand*found_specs.size]
      spec_id = spec[:id]
      version = (rand*(spec[:version].to_i) + 1).to_i
      $sfc.rest.get_spec(:spec_id=>spec_id, :version=>version, :content=>true, :args=>{"format"=>"html"})
      results << $sfc.service.responsetime
      $log.info "#{i}: #{spec_id} #{version}"
      sleep 5
    end
    $log.info "#{results.count} #{results.average} #{results.min} #{results.max}"
  end

  def test400_spec_history_comparison

    # find specifications with at least @@min_version versions
    found_specs = []

    $log.info "Find specs for subclasses #{@@subclasses}"
    @@subclasses.split.each do |subclass|
      res = $sfc.service.findSpecificationsByMetadata(:subClassId=>subclass)
      specs = res[:findSpecificationsByMetadataResponse][:return][:result][:specList]
      specs = [specs] unless specs.is_a?(Array)
      found_specs += specs.find_all {|spec| spec[:version].to_i > @@min_versions}
    end
    assert found_specs.count > 0, "No spec found"
    $log.info "#{found_specs.count} specs found."

    threads = @@threads.times.collect do
      Thread.new do
        result_hash = []
        sfc = EasySetupFC.new($env)
        @@loop_count.times do |i|
          spec = found_specs[rand*found_specs.size]
          spec_id = spec[:id]
          version = spec[:version]
          $log.info "#{spec_id}, #{version}"
          sfc.service.getSpecificationHistory(spec_id, version)
          result_hash << sfc.service.responsetime
          sleep 5
        end
        result_hash
      end
    end
    res1 = threads.collect {|t| t.value}.flatten

    # inquire all versions
    threads = @@threads.times.collect do
      Thread.new do
        result_hash = []
        sfc = EasySetupFC.new($env)
        @@loop_count.times do |i|
          spec = found_specs[rand*found_specs.size]
          spec_id = spec[:id]
          version = spec[:version]
          $log.info "#{spec_id}, all"
          sfc.service.getSpecificationHistory(spec_id)
          result_hash << sfc.service.responsetime
          sleep 5
        end
        result_hash
      end
    end
    res2 = threads.collect {|t| t.value}.flatten


    $log.info "versioned:   #{res1.count} #{res1.average} #{res1.min} #{res1.max}"
    $log.info "unversioned: #{res2.count} #{res2.average} #{res2.min} #{res2.max}"
  end
end
