=begin
(c) Copyright 2011 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

 author      - ulrich.koerner@globalfoundries.com, daniel.steger
 date        - 2012/12/10
=end

require_relative 'Test_SetupFC_Setup'

#  automated test of setup.FC API
class Test_SetupFC_Rest < Test_SetupFC_Setup
  Description = "Test SetupFC XML Interface Rest"

  @@specs = [] #"M05-00001507", "M02-DF-IMP-VPD"]

  def test10_rest_api_xsd
    # test rest-api xsds
    #
    ret = $sfc.rest.get_xml_schemas
    schemas = ret[:collection][:document].collect{|i| i[:name]}
    schemas.each do |xsd|
      ret = $sfc.rest.get_xml_schemas :xsd=>xsd
      assert_not_nil ret, "Schema file not valid #{xsd} = #{ret}"
    end
    $log.info "#{schemas.join(", ")} are valid xml files"
  end

  def test20_get_specs
    # seems it defaults to class 05
    res = $sfc.rest.get_specs
    assert res.count>0, 'no specs found'
    $log.info("#{res.count} specifications found")
    sp1 = res.first[:id]
    # key - class, val 05
    res = $sfc.rest.get_specs(:args=>{"class"=>"05"})
    assert res.count>0, 'no specs found'
    $log.info("#{res.count} specifications found")
    # key - class, val does not exist
    res = $sfc.rest.get_specs(:args=>{"class"=>"xx"})
    assert res.nil?, 'no specs should be found'
    # key class, val 02
    res = $sfc.rest.get_specs(:args=>{"class"=>"02"})
    assert res.count>0, 'no specs found'
    $log.info("#{res.count} specifications found")
    # key subclass, val Base
    res = $sfc.rest.get_specs(:args=>{"subClass"=>"EC"})
    assert res.count>0, 'no specs found'
    $log.info("#{res.count} specifications found")
    # keys - class + subclass
    res = $sfc.rest.get_specs(:args=>{"class"=>"07", "subClass"=>"EC"})
    assert (cnt=res.count)>0, 'no specs found'
    $log.info("#{res.count} specifications found")
    # not existing key
    res = $sfc.rest.get_specs(:args=>{"class"=>"07", "subClass"=>"EC", "mist"=>"xxx"})
    assert_equal res.count,cnt, 'count unexpected'
    $log.info("#{res.count} specifications found")
    # uniq spec
    res = $sfc.rest.get_specs(:spec_id=>sp1)
    assert_equal res.count, 1, 'spec not found'
    $log.info("#{res.count} specifications found")
  end
  
  def test30_get_tasks
    # not all tasks are shown
    $log.info("#{$sfc.rest.user} has #{$sfc.rest.get_tasks.count} task(s)")
  end

  def test40_get_attachments
    # find specs 
    $log.info('searching specification with attachments')
    res = $sfc.rest.get_specs(:args=>{"class"=>"07", "subClass"=>"EC"})
    assert !res.count.zero?, 'no specifications with attachments found'
    specs = res.collect {|s| s[:id] }
    $log.info("found #{@@specs.count} specifications")
    # get attachments
    $log.info('getting attachments')
    specs.each { |spec|
      res = $sfc.rest.get_specs(:spec_id=>spec, :attachments=>true)
      unless res[:attachmentList].nil?
        atts = res[:attachmentList][:attachments]
        atts = [atts] unless atts.kind_of?(Array)
        att_files = atts.collect{|a| a[:name]}.compact 
        att_files.each { |aname|
          s = $sfc.rest.get_specs(:spec_id=>spec, :filename=>aname, :content=>true, :raw=>true)
          # a_file = File.new(aname, "wb")
          # a_file.write(s)
          # a_file.close
          ext = aname.split(".").last
          assert_equal _get_mime_type(ext), $sfc.rest.http_response.content_type, "invalid content type"
        } unless att_files.nil?
      end
    }
    $log.info('attachments fetched successfully')
  end
  
  def test42_get_master_config
    keys = [:masterConfiguration, :fabLocations, :departments, :specificationClasses, :systemWorkflowSet, :templates, :workflowSets]
    master_cfg = $sfc.rest.get_master_config
    assert master_cfg.has_key?(keys[0]), 'no valid master config fetched'
    master_cfg = master_cfg[keys[0]]
    assert master_cfg.has_key?(keys[1]), 'no valid master config fetched'
    master_cfg = master_cfg[keys[1]]
    assert_equal keys[2..-1], master_cfg.keys.uniq.select{|k| k.kind_of?(Symbol)}.sort, 'no valid master config fetched'
    $log.info('master config fetched successfully')
  end

  # # # # #
  
  def _get_mime_type(extension)
    case extension
    when "txt" then return "text/plain"
    when "html" then return "text/html"
    when "xml" then return "text/xml"
    when 'jpg' then return "image/jpeg"
    when 'zip' then "application/zip"
    when "xlsx" then return "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
    when "xlsm" then return "application/octet-stream"
    when "csv" then return "text/csv"
    end
  end

end