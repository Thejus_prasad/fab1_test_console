=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

=end

require "setupfc.rb"
require "RubyTestCase"


class Test_SetupFC_Perf2 < RubyTestCase
  
  @@specs = []
  @@res = {}
  
  def setup; super :nosiview=>true; end
  
  def test00_setup
    @service = ServiceSoap.new($env, {})
    $log.info " - #{get_version()}" 
  end
  
  def test10_application_ping
    $log.info("collecting all specifications in #{$env}")
    @service = ServiceSoap.new($env, {})
    # 
    @@specs = get_complete_spec_list
    #
    msg = "#{@@specs.count} specifications found"
    $log.info(msg)
    @@res["gets"] = msg
  end

  def test11_performance_single_threaded
    $log.info("fetching #{@@specs.count} specifications in main thread")
    ts = Time.now
    fetch_all_specs(ServiceSoap.new($env, {}))
    #
    msg = "#{__method__} takes #{Time.now - ts} sec"
    $log.info(msg)
    @@res["main"] = msg
  end
  
  def xtest12_performance_multi_threaded
    thread_counts = [1,2,5,10,20] #,50,100] 
    #
    thread_counts.each{|thread_cnt|
      $log.info("fetching #{@@specs.count} specifications in #{thread_cnt} thread(s)")
      @threads = []
      ts = Time.now
      #
      thread_cnt.times{|i|
        @threads << Thread.new {
          Thread.current["name"] = "%04d" % i
          fetch_all_specs(ServiceSoap.new($env, {}), (@@specs.count/thread_cnt).to_i*i)
        }
      }
      #
      @threads.each {|t| t.join}
      msg = "#{__method__}: #{thread_cnt} threads fetching #{@@specs.count} specifications takes #{Time.now - ts} sec"
      $log.info(msg)
      @@res["%04d" % thread_cnt] = msg
    }
  end

  def xtest12a
    thread_cnt = 75
    @threads = []
    ts = Time.now
    #
    thread_cnt.times{|i|
      @threads << Thread.new {
        Thread.current["name"] = "%04d" % i
        fetch_all_specs(ServiceSoap.new($env, {}), (@@specs.count/thread_cnt).to_i*i)
      }
    }
    #
    @threads.each {|t| t.join}
    msg = "#{__method__}: #{thread_cnt} threads fetching #{@@specs.count} specifications takes #{Time.now - ts} sec"
    $log.info(msg)
  end  
  
  def xtest13_performance_single_threaded
    $log.info("fetching #{@@specs.count} specifications in main thread")
    ts = Time.now
    fetch_all_specs(ServiceSoap.new($env, {}))
    #
    msg = "#{__method__} takes #{Time.now - ts} sec"
    $log.info(msg)
    @@res["mai2"] = msg
  end

  def test99_results; pp @@res; end
  
  # # # # #

  def get_version
    begin
      res = @service.getStackVersion()[:getStackVersionResponse][:return][:result].split(",")[0]
    rescue
      $log.warn(" - error while fetching #{sp_id}")
      res = false
    end
    return res
  end
  
  # get classes configured
  def get_classes
    begin
      res = @service.getSpecificationClasses()[:getSpecificationClassesResponse][:return][:result]
    rescue
      $log.warn(" - error getting classes")
      res = []
    end
    return res.kind_of?(String) ? [res] : res
  end
  
  # get subclasses configured
  def get_subclasses(sp_class)
    begin
      res = @service.getSpecificationSubClasses(sp_class)[:getSpecificationSubClassesResponse][:return][:result]
    rescue
      $log.warn(" - error getting subclasses for class #{sp_class}")
      res = []
    end
    return res.kind_of?(String) ? [res] : res
  end
  
  # get specifications for one class-subclasse combination
  def get_spec_list(sp_class, sp_subclass="Base")
    begin
      res = @service.findSpecificationsByMetadata({:classId=>sp_class,:subClassId=>sp_subclass})[:findSpecificationsByMetadataResponse][:return][:result][:specList]
      res = [] if res.nil? # no specs
    rescue
      $log.warn(" - error while searching specifications class #{sp_class}/cubclass #{sp_subclass}")
      res = []
    end
    return res.kind_of?(Hash) ? [res] : res
  end
  
  # get one specification
  def fetch_specification(service, sp_id, version = nil)
    begin
      res = service.getSpecification(sp_id, version)[:getSpecificationResponse][:return][:result]
    rescue
      $log.warn(" - error while fetching #{sp_id}")
      res = false
    end
    return res
  end 

  # get specifications for all class-subclasse combinations
  def get_complete_spec_list
    all_specs = []
    get_classes().each{|sp_class|
      get_subclasses(sp_class).each{|sp_subclass|
        all_specs += (specs = get_spec_list(sp_class, sp_subclass).collect{|s| s[:id]})
        $log.info("#{sp_class}-#{sp_subclass} found #{specs.count} specifications")
      }
    }
    #
    return all_specs
  end
  
  # get all specifications found
  def fetch_all_specs(service, disp=0)
    cnt_all = @@specs.count
    cnt = 0
    err = 0
    cnt_all.times {|i| cnt +=1; err += 1 unless fetch_specification(service, @@specs[(i+disp) % cnt_all])}
    #
    $log.warn("#{__method__}: #{err} errors while fetching #{cnt} specifications (#{'%.2f' % (100.0*err/(cnt.zero? ? 1 : cnt))}%)") unless err.zero?
  end
end
