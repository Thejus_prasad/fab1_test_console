=begin
 Automated test of ARMOR API

(c) Copyright 2011 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

 author      - daniel.steger@globalfoundries.com
 date        - 2012/05/31
=end

require "RubyTestCase"
require "armor"

# automated test of ARMOR API
class Test_Armor_EI_InterfaceA < RubyTestCase
  Description = "Test EI Communication with ARMOR: InterfaceA OIDs"

  @@eqp = "THK2101"

  @@oids = {"94c39b4c0a0b20160160e0394287c6f0" => "/InterfaceA/DCP Tool/BST-LAM/Trace Coronus (3CH)/1",
    "20e23abf0a0b20150077f0b8286676f5" => "/InterfaceA/DCP Tool/CVD-AMAT/Trace Producer GT_CVD70x/2",
    "f55411da0a0b2016011768d8123a991f" => "/InterfaceA/DCP Tool/MPX-NOVELLUS/Trace Sabre Excel/1",
    "cd78cecd0a0b2016016889c3b77bd8a0" => "/InterfaceA/DCP Tool/POL-AMAT/Trace Reflexion ClusterTool/2",
    "a49832940a0b201501b58c34f39f68f9" => "/InterfaceA/DCP Tool/RTA-AMAT/Trace Vantage/1" }

  @@recipe_namespace = "LETCL10XX"
  @@recipe = "QS-MPC"

  def test000_setup
    $armor = Armor::EI.new($env, @@eqp) unless $armor and $armor.env == $env
    $setup_ok = true
  end

  def test100_armor_getoids
    #@@oids = { "55b0353a0a0b20153a9140cc002690d1" => "/InterfaceA/DCC/Repository/LAM/KiyoCX/DCC KiyoCX (5CH Etch) - ETX230X/4"}

    @@oids.each_pair do |oid, dcppath|
      $log.info "Check DCP in ARMOR: #{oid}"
      payload = $armor.xml_to_hash($armor.rendered_payload_oid(oid))
      refute_nil payload[:folder], "No dcp returned: #{payload.inspect}"
      assert_equal dcppath, payload[:folder][:dcpversion]["fullElementPath"], "Could not find correct dpc path information #{dcppath}"
    end
  end

  def test200_armor_getoid_missing
    $log.info "Check DCP in ARMOR does not exist"
    payload = $armor.rendered_payload_oid("not found")
    assert_equal "com.amd.thor.runtime.ResolveException: Failed getRenderedPayloadByOid(installationId = \"Default\", oid = \"not found\", rendererPath = \"/system/plugins/renderers/idxmlrenderer-DCP_2_2_9-TDM_2_2_9-DCC_2_2_9\" exception = \"com.amd.thor.element.ElementRetrievalException: Cannot find OID \"not found\"; is it valid?\"",
      payload[:Fault][:faultstring], "Could not find exception information: #{payload.inspect}"
  end

  def test300_get_payload_long_comment
    $log.info "Check ARMOR payload with long comment. BROKEN: missing recipe and recipe_namespace!!!"
    payload = $armor.rendered_payload_context("", "", :eqp=>"BST2100")
    rcp = $armor.to_recipe(payload)
    refute_nil rcp, "No recipe returned: #{payload.inspect}"
    assert_equal rcp.eqprecipename, "Fill in equipment recipe name", "No recipe returned: #{rcp.inspect}"
  end

end
