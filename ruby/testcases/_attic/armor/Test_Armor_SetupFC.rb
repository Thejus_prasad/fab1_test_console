=begin
 automated test of setup.FC API

(c) Copyright 2011 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

 author      - daniel.steger@globalfoundries.com
 date        - 2012/04/27
=end

require "RubyTestCase"
require "setupfc"
require "armor"

# automated test of setup.FC API
class Test_Armor_SetupFC < RubyTestCase
  Description = "Test ARMOR SetupFC Interface"

  @@department     = "ALL"
  @@fabLocation = "M"
  @@eqp = "THK2101"
  @@loop_count = 20
  @@timeout = 30


  # config/preparation task
  def test000_setup
    $sfc = EasySetupFC.new($env) unless $sfc and $sfc.env == $env
    $armor = Armor::EI.new($env, @@eqp) unless $armor and $armor.env == $env
    @@test_spec = {
          :title          => "ARMOR stress test",
          :department     => @@department,
          :fabLocation    => @@fabLocation,
          :classId        => "05",
          :subClassId     => "ToolRecipe",
          :template       => "ARMOR",
          :workflowConfig => "M05-5",
          # :attachment_name => @@defaultAttachmentFileName,
          # :attachment => Base64.encode64(File.read(@@default_attachment_file)).tr("\n","")
        }
    $setup_ok = true
  end

  def test100_armor_spec

    #create an new spec
    spec_id = $sfc.create_new_specification("05", "ToolRecipe", "M05-1", "ARMOR", @@test_spec)
    assert spec_id, "Error creating spec"


    tooltype = "STS-SETUPFC-TEST"
    recipe = "STRESS-RECIPE-#{Time.now.to_i}"

    @@loop_count.times do |i|
      #each version has a new equipment recipe version
      eqprecipe = "\\Equpment\\Recipe\\#{i+1}"

      #start edit workflow (creates a working copy)
      assert $sfc.content_change(spec_id), "Cannot change spec"

      res, version = $sfc.submit_specification(spec_id, :type=>"ARMOR", :tooltype=>tooltype, :recipename=>recipe, :eqprecipename=>eqprecipe)
      refute_nil $sfc.human_workflow_action(spec_id, :name=>"gfwf:approve", :polling=>true ), "Approval not working!"

      sleep @@timeout
      $log.info "Check recipe in ARMOR: #{recipe}"
      payload = $armor.rendered_payload_context(tooltype, recipe, :startingPath=>"/Job Recipes")
      refute_nil payload.index( eqprecipe ), "Could not find correct equipment recipe for #{version}"
      $log.info " #{eqprecipe} found!"
      sleep @@timeout
    end

  end


end
