=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: Steffen Steidten, 2012-11-16

Armor Version: 5.0
=end

require "RubyTestCase"
require "armor"

# Test ARMOR EI Communication: Recipe Download to EI
class Test_Armor_EI_Recipe < RubyTestCase
  Description = "Test EI Communication with ARMOR: Recipe Download"

  @@eqp = "LETCL1000"
  @@recipe_namespace = "LETCL10XX"
  @@recipe = "QS-MPC"
  @@invalid_recipe = "QS-MPC2"
  ###@@oid = "e274b20aac14be046a9009575338f923"

  def setup
    super
    $armor = Armor::EI.new($env, @@eqp) unless $armor and $armor.env == $env
  end

  def test100_rendered_payload_valid_recipe
    # get rendered payload by context
    filename = "c:/Temp/payload_#{@@eqp}_#{$env}.xml"
    payload = $armor.rendered_payload_context(@@recipe_namespace, @@recipe)
    refute_nil payload, "invalid payload returned for recipe #{@@recipe} on name space #{@@recipe_namespace}"
    $log.info "received payload successfully. Check exported payload file: #{filename}"
    # save payload content in an xml file:
    File.open(filename,"w") do |f|
      f.write(payload)
    end
    assert payload.kind_of?(String), "error in recipe download: #{payload}"
    rcp = $armor.to_recipe(payload)
    assert_equal @@recipe, rcp.eqprecipename, "wrong recipe"
    # get rendered payload by oid
    payload_oid = $armor.rendered_payload_oid(rcp.oid)
    assert payload_oid.kind_of?(String), "error in recipe download: #{payload_oid}"
    rcp_oid = $armor.to_recipe(payload_oid)
    # compare recipes
    $log.info "comparing recipes"
    rcp.members.each {|m|
      assert_equal rcp[m], rcp_oid[m], "different recipes: #{m.inspect}"
    }
  end

  # under construction. do not run. need to prepare an invalid recipe name on ARMOR
  def test101_rendered_payload_invalid_recipe
    # get rendered payload by context
    payload = $armor.rendered_payload_context(@@recipe_namespace, @@invalid_recipe)
    assert_nil payload, "invalid payload returned for recipe #{@@invalid_recipe} on name space #{@@recipe_namespace}"
    $log.info "received payload: #{payload}"
    assert payload.kind_of?(String), "error in recipe download: #{payload}"
    rcp = $armor.to_recipe(payload)
    assert_equal @@recipe, rcp.eqprecipename, "wrong recipe"
    # get rendered payload by oid
    payload_oid = $armor.rendered_payload_oid(rcp.oid)
    assert payload_oid.kind_of?(String), "error in recipe download: #{payload_oid}"
    rcp_oid = $armor.to_recipe(payload_oid)
    # compare recipes
    $log.info "comparing recipes"
    rcp.members.each {|m|
      assert_equal rcp[m], rcp_oid[m], "different recipes: #{m.inspect}"
    }
  end

end
