=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: Steffen Steidten, 2011-07-05

Note: verify log file at f38asq1:/local/logs/armorStress/server.log

History:
  2017-04-25 sfrieske, replaced rqrsp by rqrsp_mq, minor cleanup
=end

require 'yaml'
require 'RubyTestCase'
require 'armor'
require 'misc'

# Test EI Communication with ARMOR: EC-SV Download Performance
class Test_Armor_EI_ECSV < RubyTestCase
  @@loops = 2
  @@mintools = 1
  @@maxtools = 20
  @@timeout = 300

  def test01_setup
    $setup_ok = false
    #
    @@results = {}
    # create tool list
    n = (@@maxtools/10).to_i + 1
    @@tools = ((0..9).collect {|i| "POL70#{i}"} * n)[0..@@maxtools-1]
    @@aclients = @@tools.collect {|t| Armor::EI.new($env, eqp: t)}
    # clean up the queue
    ac = @@aclients[0]
    ac.mq_response.delete_msgs(nil)
    #
    $setup_ok = true
  end

  def test11_ecsv_stress
    test_ok = true
    (@@mintools..@@maxtools).each {|n|
      $log.info "testing with #{n} tools"
      stats = ecsv_stress(@@aclients[0..n-1], loops: @@loops, timeout: @@timeout, show: true)
      @@results[n] = stats
      test_ok &= stats[:ok]
      break unless test_ok
    }
    assert test_ok, "ARMOR message timeout"
  end

  def test19_summary
    File.binwrite("armor_ecsv-#{Time.now.utc.iso8601.gsub(':', '-')}.yaml", @@results.to_yaml)
    show_ecsv_summary(@@results, file: "armor_ecsv-#{Time.now.utc.iso8601.gsub(':', '-')}.txt")
  end

end


def ecsv_stress(aclients, params={})
  # run the stress test and return performance stats
  loops = params[:loops] || 1
  res = []
  threads = []
  tstart = Time.now
  aclients.each {|ac|
    sleep 0.2 if threads.size > 0
    threads << Thread.new {
      # ctx = {"RecipeNameSpaceID"=>"PC-P-POL7XX", "RecipeName"=>"32NM-POL-M2-01", "Equipment"=>ac.eqp}
      # ctx = {"RecipeNameSpaceID"=>"PC-P-POL7XX", "RecipeName"=>"22NM-POL-M1-01", "Equipment"=>ac.eqp}
      loops.times {|r|
        sleep 0.2 if r > 0
        #ac.getRenderedPayloadByContext(:context=>ctx, :raw=>true, :rawxml=>true, :timeout=>params[:timeout].to_i)
        ##ac.rendered_payload_context("PC-P-POL7XX", "22NM-POL-M1-01", {raw:=>true, :rawxml=>true, :timeout=>params[:timeout].to_i})
        ac.get_payload(:bycontext, recipe_namespace: 'PC-P-POL7XX', recipe: '22NM-POL-M1-01', timeout: params[:timeout])
				res << ac.mq_response.responsetime
      }
    }
  }
  threads.each_with_index {|t, i|
    $log.info "joining thread #{i}"
    t.join
  }
  stats = {tstart: tstart.clone, duration: Time.now - tstart, loops: loops,
    ntools: aclients.size, responsetime: res.clone, ok: !res.member?(nil)}
  #
  show_ecsv_stats(stats) if params[:show]
  return stats  #.clone
end

def show_ecsv_stats(stats)
  # display the results of an ECSV stress test
  res = stats[:responsetime]
  r = res.compact
  puts "\nECSV Stress Test from #{stats[:tstart].iso8601}"
  puts "=" * 47
  puts "test duration:         #{stats[:duration]}s"
  puts "number of tools:       #{stats[:ntools]}, #{stats[:loops]} loops"
  puts "number of ARMOR calls: %d  (%.1f per hour)" % [r.size, r.size/stats[:duration]*3600]
  puts "average call duration: %.1fs +- %.1fs (%d%%)" % [
    r.average, r.standard_deviation, r.standard_deviation/r.average*100]
  puts
  puts
end

def show_ecsv_summary(results, params={})
  # display the summary of a series of ECSV stress tests
  # results can be a Hash or a YAML file name
  results = YAML.load_file(results) if results.kind_of?(String)
  #
  r0 = results[results.keys[0]]
  puts "\nECSV Stress Test Summary from #{r0[:tstart]}"
  puts "=" * 60
  puts "#{r0[:loops]} loops"
  puts
  puts "Tools  Time (s) StdDev       Calls per hour"
  puts "-" * 45
  results.each_pair {|n, stats|
    r = stats[:responsetime].compact
    puts "%3d   %.1f  %.1f (%d%%)   %d" % [
      n, r.average, r.standard_deviation, r.standard_deviation/r.average*100, r.size/stats[:duration]*3600]
  }
  puts
  # write CSV file for chart generation
  fname = params[:file]
  return nil unless fname
  open(fname, "wb") {|fh|
    fh.write("Parallel Calls\tCall Duration (s)\tStd Dev (%)\tCalls per Hour\n")
    results.each_pair {|n, stats|
      r = stats[:responsetime].compact
      fh.write("#{n}\t#{r.average}\t#{r.standard_deviation/r.average*100}\t#{r.size/stats[:duration]*3600}\n")
    }
  }
  nil
end
