=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author:  sfrieske, 2016-04-14

Version: 1.1.0

=end

require_relative 'ECMTestCase'


# RouteOperation edit tests.
class ECM_RteOp < ECMTestCase

  def testdev421_new_op
    # create a SiView route and an EC for it
    assert route =  @@ecmtest.new_testobject(:mainpd)
    assert ec = @@ecmtest.new_ecroute(route)
    @@ecs << ec
    #
    # pick the first op in the 2nd module
  end

  def test491_validation_rules_rteop
    # create a SiView route and an EC for it
    assert route =  @@ecmtest.new_testobject(:mainpd)
    assert ec = @@ecmtest.new_ecroute(route)
    @@ecs << ec
    assert rdata = ec.ecroute_rdata(route)
    assert rops = ec.ecroute_operations(nil, rdata: rdata)
    #
    # validate MUST_MAKE_CHANGE
    assert ec.verify_validation('MUST_MAKE_CHANGE', only: true)
    assert_equal 0, ec.impact['total']
    #
    # pick a mandatory op and remove RouteOpSafeOp UDATA, must pass
    assert rop = ec.ecroute_operations(nil, rdata: rdata, mandatory: true, safeop: 'PRE').first
    assert ec.update_operation_udata(route, rop['number'], 'RouteOpSafeOp', '')
    assert_empty ec.validation
    # pick a mandatory op and add RouteOpSafeOp UDATA, must pass
    assert rop = ec.ecroute_operations(nil, rdata: rdata, mandatory: true, safeop: '').first
    assert ec.update_operation_udata(route, rop['number'], 'RouteOpSafeOp', 'PRElong')
    assert_empty ec.validation(accept_warning: true)  # about mandatory measurement ops
    #
    # pick a not mandatory op and add RouteOpSafeOp UDATA, must fail
    assert rop = ec.ecroute_operations(nil, rdata: rdata, mandatory: false, safeop: '').first
    assert ec.update_operation_udata(route, rop['number'], 'RouteOpSafeOp', 'PRElong')
    assert ec.verify_validation('ECM.Rule.Main.Udata.SafeOperationMandatoryValid', only: true, accept_warning: true)
    # remove the RouteOpSafeOp UDATA again and validate
    assert ec.update_operation_udata(route, rop['number'], 'RouteOpSafeOp', '')
    assert_empty ec.validation(accept_warning: true)  # about mandatory measurement ops
    # optional: pick a not mandatory op and remove RouteOpSafeOp UDATA, must pass
    rop = ec.ecroute_operations(nil, rdata: rdata, mandatory: false, type: :measurement, safeop: 'PRE').first
    if rop
      assert ec.update_operation_udata(route, rop['number'], 'RouteOpSafeOp', '')
      assert_empty ec.validation(accept_warning: true)  # about mandatory measurement ops
    end
    #
    # pick an op not owned by TST and remove the PCL-PRE1 script
    assert rop = ec.ecroute_operations(nil, rdata: rdata, department: '!TST').first
    assert ec.update_operation(route, rop['number'], mainPdScripts: {preOne: ''})
    assert ec.verify_validation('ECM.Rule.OP.PreOneNotTSTValid', only: true, accept_warning: true)
    # add the PCL-PRE1 script back
    assert ec.update_operation(route, rop['number'], mainPdScripts: {preOne: 'PCL-PRE1'})
    assert_empty ec.validation
    #
    # repeat the test with an op owned by TST
    assert rop = ec.ecroute_operations(nil, rdata: rdata, department: 'TST').first
    assert ec.update_operation_udata(route, rop['number'], 'RouteOpSafeOp', '') # avoid RouteOpSafeOp issues
    assert ec.update_operation(route, rop['number'], mainPdScripts: {preOne: ''})
    assert ec.verify_validation('ECM.Rule.OP.PreOneTSTValid', only: true)
    # add a script not from the list, must fail
    assert ec.update_operation(route, rop['number'], mainPdScripts: {preOne: 'QA-NOTEX'})
    assert ec.verify_validation('ECM.Rule.OP.PreOneTSTValid', only: true)
    # add a script from the whitelist, must pass
    assert ec.update_operation(route, rop['number'], mainPdScripts: {preOne: 'S-ALL'})
    assert_empty ec.validation
    #
    # pick a PD with UDATA PCLSpecsPOST and remove post script
    assert rop = rops.find {|e|
      e['pdUData'].find {|u| u['key'] == 'PCLSpecsPOST' && !u['value']['value'].empty?}
    }
    assert ec.update_operation(route, rop['number'], mainPdScripts: {post: ''})
    assert ec.verify_validation('ECM.Rule.OP.PCLUdataDefined', only: true)
    # add back the post script
    assert ec.update_operation(route, rop['number'], mainPdScripts: {post: 'PCL-POST'})
    assert_empty ec.validation
    #
    # find a Q-Time and set RouteOpSafeOp UDATA at trigger PD, must pass
    assert qtop = ec.ecroute_timelinks(nil, rdata: rdata, qtime: true).first
    assert ec.update_operation_udata(route, qtop.opNo, 'RouteOpSafeOp', 'PRElong')
    refute ec.verify_validation('ECM.Rule.OP.Udata.SafeOperationValid', only: true)
    # set RouteOpSafeOp UDATA at target PD, must fail
    assert ec.update_operation_udata(route, qtop.target_opNo, 'RouteOpSafeOp', 'PRElong')
    assert ec.verify_validation('ECM.Rule.OP.Udata.SafeOperationValid')
  end

  def test492_otherchange_and_mandatory # was ECM-3223
    # create an EC
    assert route = @@testobjects[:mainpd][2]
    assert ec = @@ecmtest.new_ecroute(route)
    @@ecs << ec
    #
    # validate MUST_MAKE_CHANGE
    assert ec.verify_validation('MUST_MAKE_CHANGE', only: true)
    assert_equal 0, ec.impact['total']
    #
    # pick an operation with rework and remove it
    assert rwremove = ec.ecroute_subroutes(route, rework: true).last
    assert ec.delete_subroute(route, rwremove.opNo, rwremove.id, rework: true)
    refute ec.verify_validation('MUST_MAKE_CHANGE')
    # change the mandatory flag
    assert opdata = ec.ecroute_operations(route, opNo: rwremove.opNo)
    mandatory = opdata['state']['value'] == 1
    assert ec.update_operation(route, rwremove.opNo, mandatory: !mandatory)
    refute ec.verify_validation('MUST_MAKE_CHANGE')
   end


end
