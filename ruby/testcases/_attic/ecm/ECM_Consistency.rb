=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author:  sfrieske, 2019-05-10

Version: 1.0.0

=end

require 'misc'  # verify_equal
require 'ecm/test'
require 'ecm/ecroute'
require 'RubyTestCase'


class ECM_Consistency < RubyTestCase
  # @@route = 'C02-0395.01'
  @@routes = ['F1QA-ECM-A-RTE0001.0000']
  @@opNo_erf = '1100.1000'
  @@opNo_erfjoin = '1100.1200'

  @@ecs = []


  def self.after_all_passed
    @@ecs.each {|ec| ec.cleanup}
  end


  def test00_setup
    $setup_ok = false
    #
    @@ecmtest = ECM::Test.new($env)
    @@session = @@ecmtest.session
    @@sm = @@ecmtest.sm
    #
    assert @@routes = @@ecmtest.new_testobject(:mainpd, n: 2) unless @@routes
    #
    $setup_ok = true
  end

  def test11_lagtime1
    route = @@routes[0]
    assert ec = ECM::ECRoute.new(@@session, routes: route)
    @@ecs << ec
    #
    lts = ec.lagtimes(route)
    assert opNo_lt = lts.keys.last
    ltorig = lts[opNo_lt]['duration']['originalValue']
    # remove a lagtime PD and change SiView lagtime
    assert_equal 3, ec.consistency['results'].first['result']  # no change
    assert ec.opNo_delete(route, opNo_lt)
    assert @@sm.mainpd_change_lagtime(route, opNo_lt, ltorig + 1)
    assert_equal 1, ec.consistency['results'].first['result']  # change detected, TODO: fails!
    assert ec.opNo_reset(route, opNo_lt)
    #
    # remove an existing lagtime and change SiView lagtime
    assert_equal 3, ec.consistency['results'].first['result']  # no change
    assert ec.lagtime_delete(route, opNo_lt, lts[opNo_lt])
    assert @@sm.mainpd_change_lagtime(route, opNo_lt, ltorig + 2)
    assert_equal 1, ec.consistency['results'].first['result']  # change detected
    assert ec.opNo_reset(route, opNo_lt)
  end

  def test21_erf_pds
    route = @@routes[1]
    assert ec = ECM::ECRoute.new(@@session, routes: route)
    @@ecs << ec
    #
    # remove an ERF branch PD
    assert_equal 3, ec.consistency['results'].first['result']  # no change
    assert ec.opNo_delete(route, @@opNo_erf)
    assert_equal 1, ec.consistency['results'].first['result']  # change detected
    assert ec.opNo_reset(route, @@opNo_erf)
    # remove an ERF join PD
    assert_equal 3, ec.consistency['results'].first['result']  # no change
    assert ec.opNo_delete(route, @@opNo_erfjoin)
    assert_equal 1, ec.consistency['results'].first['result']  # change detected
    assert ec.opNo_reset(route, @@opNo_erfjoin)
  end

end
