=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author:  sfrieske, 2016-04-14

Version: 1.2.0

=end

load 'ecmtest.rb'
require 'RubyTestCase'


# Base of all ECM tests, providing SiView testobjects, SM and ECM access
class ECMTestCase < RubyTestCase
  @@object_prefix = nil
  @@sync_duration = 180

  def self.startup
    super(nosiview: true)
    @@ecmtest = ECM::Test.new($env, object_prefix: @@object_prefix, sync_duration: @@sync_duration)
    @@sm = @@ecmtest.sm
    @@ecs = []
  end

  def self.after_all_passed
    @@ecs.each {|ec| ec.cleanup}
  end

  def test000_setup
    $setup_ok = false
    #
    # ensure there are no stale test objects affecting the impact analysis
    assert @@ecmtest.delete_all_testobjects, "error deleting old test objects in SM"
    $log.info "waiting #{@@ecmtest.sync_duration} s for ECM sync"; sleep @@ecmtest.sync_duration
    #
    $setup_ok = true
  end
end
