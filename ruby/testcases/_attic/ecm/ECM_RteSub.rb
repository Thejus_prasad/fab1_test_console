=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author:  sfrieske, 2016-05-30

Version: 1.1.0

=end

require_relative 'ECMTestCase'


# Branch and Rework route edit tests
class ECM_RteSub < ECMTestCase
  @@opNo_rwk = '1100.1200'
  @@rte_rwk = 'F1QA-ECM-A-RWK0001.0000'
  @@rte_rwk2 = 'F1QA-ECM-A-RWK0002.0000'
  @@opNo_rwk_join = '1100.1100'
  @@opNo_branch = '1100.1100'
  @@rte_branch = 'F1QA-ECM-A-BRA0001.0000'
  @@rte_branch2 = 'F1QA-ECM-A-BRA0002.0000'
  @@opNo_branch_join = '1100.1200'


  # single route rework changes, complete
  def test310_happy_rework
    $setup_ok = false
    #
    # create a SiView route and an EC for it
    assert route = @@ecmtest.new_testobject(:mainpd)
    assert ec = @@ecmtest.new_ecroute(route), "connection issues"
    @@ecs << ec
    #
    # verify EC is in MyECs
    assert myecs = ec.session.ecs_user
    assert myecs['data'].find {|e| e['ecId'] == ec.ecname}
    assert_equal 'EDIT', ec.metadata['state']
    #
    # remove an already existing rework
    assert rwremove = ec.ecroute_subroutes(route, rework: true).last, "no rework found"
    assert ec.delete_subroute(route, rwremove.opNo, rwremove.id, rework: true)
    assert changes = ec.changes
    assert_equal 1, changes['subFlows']['total']
    assert_equal 1, changes['subFlows']['totalRemoved']
    assert ec.update_operation(route, rwremove.opNo, mandatory: true) # to fix RouteOpSafeOp issues
    ##assert ec.update_operation_udata(route, rwremove.opNo, 'RouteOpSafeOp', '') # to fix RouteOpSafeOp issues
    assert_empty ec.validation  # wrong validation, ECM-3442
    #
    # add rework
    assert srid = ec.add_subroute(route, @@opNo_rwk, rework: true)
    data = {subFlowMainPd: @@rte_rwk, joiningOpNumber: @@opNo_rwk_join, rework: true}
    assert ec.update_subroute(route, @@opNo_rwk, srid, data)
    assert changes = ec.changes
    assert_equal 2, changes['subFlows']['total']
    assert_equal 1, changes['subFlows']['totalAdded']
    assert ec.update_operation(route, @@opNo_rwk, mandatory: true) # to fix RouteOpSafeOp issues
    ##assert ec.update_operation_udata(route, @@opNo_rwk, 'RouteOpSafeOp', '') # to fix RouteOpSafeOp issues
    assert_empty ec.validation   # timeouts observed (.... v1.7.10)
    #
    # complete the EC
    assert ec.complete_ec(approver_session: @@ecmtest.session2)
    #
    # verify reworks in SiView
    assert oinfo = @@sm.object_info(:mainpd, route).first
    assert_equal 'Released', oinfo.state
    # verify rework removal
    assert opinfo = oinfo.specific[:operations].find {|o| o.opNo == rwremove.opNo}, "no such operation"
    assert_empty opinfo.reworks
    # verify rework added
    assert opinfo = oinfo.specific[:operations].find {|o| o.opNo == @@opNo_rwk}, "no such operation"
    assert rwkinfo = opinfo.reworks.first, "no rework"
    assert_equal @@rte_rwk, rwkinfo.mainpd, "wrong rework route"
    assert_equal @@opNo_rwk_join, rwkinfo.joining_opNo, "wrong joining opNo"
    #
    $setup_ok = true
  end

  def test311_happy_branch
    $setup_ok = false
    #
    # create a SiView route and an EC for it
    assert route =  @@ecmtest.new_testobject(:mainpd)
    assert ec = @@ecmtest.new_ecroute(route), "connection issues"
    @@ecs << ec
    #
    # add branch route
    assert oinfo = @@sm.object_info(:mainpd, route).first
    idx = oinfo.specific[:operations][2..-1].find_index {|e| e.branches.empty?} # [2..-1] ??
    opNo_branch = oinfo.specific[:operations][idx].opNo
    opNo_branch_join = oinfo.specific[:operations][idx + 1].opNo
    assert srid = ec.add_subroute(route, opNo_branch, branch: true)
    data = {subFlowMainPd: @@rte_branch, joiningOpNumber: opNo_branch_join, branch: true}
    assert ec.update_subroute(route, opNo_branch, srid, data)
    assert ec.update_operation_udata(route, opNo_branch, 'RouteOpSafeOp', '') # to fix RouteOpSafeOp issues
    assert ec.update_operation_udata(route, opNo_branch_join, 'RouteOpSafeOp', '') # to fix RouteOpSafeOp issues
    #
    # complete EC and verify route has been added in SiView
    assert ec.complete_ec(approver_session: @@ecmtest.session2)
    assert oinfo = @@sm.object_info(:mainpd, route).first
    assert_equal 'Released', oinfo.state
    assert opinfo = oinfo.specific[:operations].find {|o| o.opNo == opNo_branch}, "no such operation"
    assert brinfo = opinfo.branches.first, "no rework"
    assert_equal @@rte_branch, brinfo.mainpd, "wrong rework route"
    assert_equal opNo_branch_join, brinfo.joining_opNo, "wrong joining opNo"
    #
    $setup_ok = true
  end

  def test321_siview_changes
    # create a SiView route and an EC for it
    assert route =  @@ecmtest.new_testobject(:mainpd)
    assert ec = @@ecmtest.new_ecroute(route), "connection issues"
    @@ecs << ec
    #
    # add rework route
    assert srid = ec.add_subroute(route, @@opNo_rwk, rework: true)
    data = {subFlowMainPd: @@rte_rwk, joiningOpNumber: @@opNo_rwk_join, rework: true}
    assert ec.update_subroute(route, @@opNo_rwk, srid, data)
    assert ec.update_operation(route,  @@opNo_rwk, mandatory: true) # to fix RouteOpSafeOp issues
    assert_empty ec.validation, "wrong changes in #{ec.ecname}"
    #
    # in SiView remove an already existing branch and wait for sync
    oinfo = @@sm.object_info(:mainpd, route).first
    assert brop = oinfo.specific[:operations].reverse.find {|e| !e.branches.empty?}
    assert @@sm.mainpd_delete_branch(route, brop.opNo, brop.branches.first)
    assert @@ecmtest.wait_ecm_sync(route) {|newec|
      !newec.ecroute_subroutes(route, branch: true).find {|e| e[0] == brop.opNo}
    }, "no update from SiView"
    # validate EC again
    assert_empty ec.validation, "EC #{ec.ecname} cannot be submitted"
    #
    # in SiView remove an already existing rework and wait for sync
    assert rwop = oinfo.specific[:operations].reverse.find {|e| !e.reworks.empty?}
    assert @@sm.mainpd_delete_rework(route, rwop.opNo, rwop.reworks.first)
    assert @@ecmtest.wait_ecm_sync(route) {|newec|
      !newec.ecroute_subroutes(route, rework: true).find {|e| e.opNo == rwop.opNo}
    }, "no update from SiView"
    # validate EC
    assert_empty ec.validation, "EC #{ec.ecname} cannot be submitted"
  end

  def test391_validation_rules_sub
    # create a SiView route and an EC for it
    assert route =  @@ecmtest.new_testobject(:mainpd)
    assert ec = @@ecmtest.new_ecroute(route)
    @@ecs << ec
    #
    # validate MUST_MAKE_CHANGE
    vals = ec.validation
    assert_equal 1, vals.length
    assert_equal 'MUST_MAKE_CHANGE', vals[0]['name']
    assert_equal 0, ec.impact['total']
    #
    # add rework w/o data
    assert ec.update_operation_udata(route, @@opNo_rwk, 'RouteOpSafeOp', '') # to fix RouteOpSafeOp issues
    assert srid = ec.add_subroute(route, @@opNo_rwk, rework: true)
    vals = ec.validation
    assert_equal 1, vals.length
    assert_equal 'ECM.Rule.SUB.SubflowFieldsNotBlank', vals[0]['name']
    #
    # add data, must pass
    data = {subFlowMainPd: @@rte_rwk, joiningOpNumber: @@opNo_rwk_join, rework: true}
    assert ec.update_subroute(route, @@opNo_rwk, srid, data)
    assert_empty ec.validation, "wrong validation error"
    #
    # add second rework with the same data
    assert srid = ec.add_subroute(route, @@opNo_rwk, rework: true)
    assert ec.update_subroute(route, @@opNo_rwk, srid, data)
    vals = ec.validation
    assert_equal 1, vals.length
    assert_equal 'ECM.Rule.SUB.SubflowNotDuplicate', vals[0]['name']
    #
    # remove 2nd rework, must pass
    assert ec.delete_subroute(route, @@opNo_rwk, srid)
    assert_empty ec.validation, "wrong validation error"
    #
    # add branch w/o data
    assert ec.update_operation_udata(route, @@opNo_branch, 'RouteOpSafeOp', '') # to fix RouteOpSafeOp issues
    assert srid = ec.add_subroute(route, @@opNo_branch, branch: true)
    vals = ec.validation
    assert_equal 1, vals.length
    assert_equal 'ECM.Rule.SUB.SubflowFieldsNotBlank', vals[0]['name']
    #
    # add data, must pass
    data = {subFlowMainPd: @@rte_branch, joiningOpNumber: @@opNo_branch_join, branch: true}
    assert ec.update_subroute(route, @@opNo_branch, srid, data)
    assert_empty ec.validation, "wrong validation error"
    #
    # add second branch with the same data
    assert srid = ec.add_subroute(route, @@opNo_branch, branch: true)
    assert ec.update_subroute(route, @@opNo_branch, srid, data)
    vals = ec.validation
    assert_equal 1, vals.length
    assert_equal 'ECM.Rule.SUB.SubflowNotDuplicate', vals[0]['name']
  end

end
