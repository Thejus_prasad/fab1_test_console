=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author:  sfrieske, 2016-04-14

Version: 1.0.0

=end

load 'ecmtest.rb'
require 'RubyTestCase'


# Route Module edit tests.
class ECM_RteMod < RubyTestCase
  @@module_new = 'F1QA-ECM-A-START0000.0000'

  def self.startup
    super(nosiview: true)
    @@ecmtest = ECM::Test.new($env)
    @@sm = @@ecmtest.sm
    #
    @@testobjects = {}  # in SiView
    @@ecs = []          # in ECM
  end

  def self.after_all_passed
    #@@testobjects.each_pair {|oclass, objs| @@sm.delete_objects(oclass, objs)}
    @@ecs.each {|ec| ec.cleanup}
  end


  def test500_setup
    $setup_ok = false
    #
    # create new SiView test objects and a new EC and wait for ECM to sync with SiView
    assert @@testobjects[:mainpd] = @@ecmtest.new_testobject(:mainpd, n: 3)
    @@testobjects[:mainpd].each {|route|
      assert @@ecmtest.sv.user_data_route(route)['RouteProgramID'], "missing UDATA RouteProgramID on route #{route}"
    }
    assert @@ecmtest.wait_ecm_sync(@@testobjects[:mainpd]), "ECM not in sync for #{@@testobjects[:mainpd]}"
    #
    $setup_ok = true
  end

  def test510_basic
    $setup_ok = false
    #
    # create an EC
    assert route = @@testobjects[:mainpd][0]
    assert ec = @@ecmtest.new_ecroute(route)
    @@ecs << ec
    assert rdata = ec.ecroute_rdata(route)
    assert rmods = @@session.ecroute_modules(nil, nil, rdata: rdata)
    #
    # pick 2nd module and delete it - not yet implemented
    # assert rmod = rmods[1]
    # assert @@session.ecroute_mod_delete(ec, route, rmod)
    # assert changes = ec.changes  # fails in 1.5.18
    # assert_equal 1, changes['timelinks']['total']
    # assert_equal 1, changes['timelinks']['totalRemoved']
    #
    # pick 2nd module and add a module
    assert rmod = rmods[1]
    assert modid = @@session.ecroute_mod_add(ec, route, rmod, before: true)
    assert @@session.ecroute_moddata(ec, route, modid, modulePd: @@module_new)
    assert_empty ec.validation
    assert changes = ec.changes
    assert_equal 1, changes['mainPds']['total']
    ## fails   assert_equal 1, changes['mainPds']['totalEdited']
    #
    # complete the EC
    assert @@ecmtest.complete_ec
    #
    # verify in SiView
    #
    $setup_ok = true
  end

  def test511_new_first
    $setup_ok = false
    #
    # create an EC
    assert route = @@testobjects[:mainpd][1]
    assert ec = @@ecmtest.new_ecroute(route)
    @@ecs << ec
    assert rdata = ec.ecroute_rdata(route)
    assert rmods = @@session.ecroute_modules(nil, nil, rdata: rdata)
    #
    # insert a new module before the first one
    assert rmod = rmods.first
    assert modid = @@session.ecroute_mod_add(ec, route, rmod, before: true)
    assert @@session.ecroute_moddata(ec, route, modid, modulePd: @@module_new)
    assert_empty ec.validation
    assert changes = ec.changes
    assert_equal 1, changes['mainPds']['total']
    ##fails assert_equal 1, changes['mainPds']['totalEdited']
    #
    # complete the EC
    assert @@ecmtest.complete_ec
    #
    $setup_ok = true
  end

  def test512_new_last  # fails, ECM-3228
    $setup_ok = false
    #
    # create an EC
    assert route = @@testobjects[:mainpd][2]
    assert ec = @@ecmtest.new_ecroute(route)
    @@ecs << ec
    assert rdata = ec.ecroute_rdata(route)
    assert rmods = @@session.ecroute_modules(nil, nil, rdata: rdata)
    #
    # insert a new module after the last one
    assert rmod = rmods.last
    assert modid = @@session.ecroute_mod_add(ec, route, rmod, after: true)
    assert @@session.ecroute_moddata(ec, route, modid, modulePd: @@module_new)
    assert_empty ec.validation
    assert changes = ec.changes
    assert_equal 1, changes['mainPds']['total']
    ##fails assert_equal 1, changes['mainPds']['totalEdited']
    #
    # complete the EC
    assert @@ecmtest.complete_ec
    #
    $setup_ok = true
  end

  def test513_extended
    # create an EC
    assert route = @@testobjects[:mainpd][3]
    assert ec = @@ecmtest.new_ecroute(route)
    @@ecs << ec
    assert rdata = ec.ecroute_rdata(route)
    assert rmods = @@session.ecroute_modules(nil, nil, rdata: rdata)
    #
    # pick modules and insert a new one before and after, resp
    newmods = []
    [rmods[1], rmods[0], rmods[-2], rmods[-1]].each {|rmod|
      assert modid = @@session.ecroute_mod_add(ec, route, rmod, before: true)
      newmods << modid
      assert @@session.ecroute_moddata(ec, route, modid, modulePd: @@module_new)
      assert_empty ec.validation
      assert modid = @@session.ecroute_mod_add(ec, route, rmod, after: true)
      newmods << modid
      assert @@session.ecroute_moddata(ec, route, modid, modulePd: @@module_new)
      assert_empty ec.validation
    }
    assert changes = ec.changes
    assert_equal 8, changes['mainPds']['total']
    assert_equal 4, changes['mainPds']['totalEdited']
    #
    # complete the EC
    assert @@ecmtest.complete_ec
  end

  def test520_remove_mandatory_flag
    # create an EC
    route = @@testobjects[:mainpd][0]
    assert ec = @@ecmtest.new_ecroute(route)
    @@ecs << ec
    #
    # pick a mandatory op and remove the flag
    assert rop = ec.ecroute_operations(route, mandatory: true).first
    assert ec.update_operation(route, rop['number'], mandatory: false)
    assert ec.update_operation_udata(route, rop['number'], 'RouteOpSafeOp', '') # to fix RouteOpSafeOp issues
    assert ec.complete_ec(approver_session: @@ecmtest.session2)    # was ECM-2917
    # verify in SiView
    assert svdata = @@sm.object_info(:modulepd, rop['modulePd']['value']).first
    assert svop = svdata.specific.find {|e| e.opNo == rop['number'].split('.').last}
    assert_equal false, svop.mandatory
    ## as this is a module change further tests are required!!
  end

  def test521_set_mandatory_flag
    # create an EC
    route = @@testobjects[:mainpd][1]
    assert ec = @@ecmtest.new_ecroute(route)
    @@ecs << ec
    #
    # pick a non-mandatory op and set the flag
    assert rop = ec.ecroute_operations(route, mandatory: false).first
    assert ec.update_operation(route, rop['number'], mandatory: true)
    assert ec.complete_ec(approver_session: @@ecmtest.session2)  # was ECM-3171
    # verify in SiView
    assert svdata = @@sm.object_info(:modulepd, rop['modulePd']['value']).first
    assert svop = svdata.specific.find {|e| e.opNo == rop['number'].split('.').last}
    assert_equal true, svop.mandatory
    ## as this is a module change further tests are required!!
  end


  def test591_validation_rules
    # create an EC
    route = @@testobjects[:mainpd][0]
    assert ec = @@ecmtest.new_ecroute(route)
    @@ecs << ec
    assert rdata = ec.ecroute_rdata(route)
    assert rmods = @@session.ecroute_modules(nil, nil, rdata: rdata)
    #
    # validate MUST_MAKE_CHANGE
    assert ec.verify_validation('MUST_MAKE_CHANGE', only: true)
    assert_equal 0, ec.impact['total']
    #
    # pick the 2nd module and insert a new one without ID, must fail with IDNotBlank
    assert rmod = rmods[1]
    assert modid = @@session.ecroute_mod_add(ec, route, rmod, before: true)
    assert ec.verify_validation('ECM.Rule.ModulePD.IDNotBlank', only: true)
    # remove new module again and validate
    assert @@session.ecroute_mod_delete(ec, route, modid)
    assert ec.verify_validation('MUST_MAKE_CHANGE', only: true)
    #
  end

end
