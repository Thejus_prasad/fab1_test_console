=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author:  sfrieske, 2016-04-14

Version: 1.1.0

=end

require_relative 'ECMTestCase'


# Q-Time and Lagtime edit tests
class ECM_RteQtime < ECMTestCase
  @@qt_trigger = '1100.2500'  # '1100.1400'
  @@qt_trigger2 = '1100.2600' # '1100.1500'
  @@qt_target = '1100.2700'   # '1100.1600'
  @@qt_target2 = '1100.2800'  # '1100.1700'
  @@qt_duration = 13
  @@routes = nil


  def test210_happy_lagtime
    $setup_ok = false
    #
    # create a SiView route and an EC for it
    assert route = @@routes ? @@routes[0] : @@ecmtest.new_testobject(:mainpd)
    assert ec = @@ecmtest.new_ecroute(route), "connection issues"
    @@ecs << ec
    #
    # remove an existing lagtime
    assert ltremove = ec.ecroute_timelinks(route, lagtime: true).last
    assert ec.delete_timelink(route, ltremove.id)
    assert changes = ec.changes
    assert_equal 1, changes['timelinks']['total']
    assert_equal 1, changes['timelinks']['totalRemoved']
    #
    # find an op without lagtime and add one
    assert oinfo = @@sm.object_info(:mainpd, route).first
    assert ltadd = oinfo.specific[:operations].find {|e| e.lagtime == 0}
    assert lt = ec.add_lagtime(route, ltadd.opNo)
    assert changes = ec.changes
    assert_equal 2, changes['timelinks']['total']
    assert_equal 1, changes['timelinks']['totalAdded']
    # delete and add again
    assert ec.delete_timelink(route, lt)
    assert changes = ec.changes
    assert_equal 1, changes['timelinks']['total']
    assert_equal 0, changes['timelinks']['totalAdded']
    assert lt = ec.add_lagtime(route, ltadd.opNo)
    assert changes = ec.changes
    assert_equal 2, changes['timelinks']['total']
    assert_equal 1, changes['timelinks']['totalAdded']
    # add an action to the lagtime
    ltduration = 1
    assert action = ec.add_timelink_action(route, lt, action: 'lagtime', duration: ltduration)
    #
    # complete the EC
    assert ec.complete_ec(approver_session: @@ecmtest.session2)
    #
    # verify lagtimes in SiView
    assert oinfo = @@sm.object_info(:mainpd, route).first
    assert_equal 'Released', oinfo.state
    # verify lagtime removal
    assert opinfo = oinfo.specific[:operations].find {|o| o.opNo == ltremove.opNo}
    assert_equal 0, opinfo.lagtime, "lagtime has not been removed"
    # verify lagtime added
    assert opinfo = oinfo.specific[:operations].find {|o| o.opNo == ltadd.opNo}
    assert_equal ltduration, opinfo.lagtime, "lagtime has not been added"
    #
    $setup_ok = true
  end

  def test211_happy_qtime
    $setup_ok = false
    #
    # create a SiView route and an EC for it
    assert route = @@routes ? @@routes[1] : @@ecmtest.new_testobject(:mainpd)
    assert ec = @@ecmtest.new_ecroute(route), "connection issues"
    @@ecs << ec
    # remove an existing Q-time
    assert qtremove = ec.ecroute_timelinks(route, qtime: true).first
    assert ec.delete_timelink(route, qtremove.id)
    # verify changes
    assert changes = ec.changes
    assert_equal qtremove.actions.length, changes['timelinks']['total']
    assert_equal qtremove.actions.length, changes['timelinks']['totalRemoved']
    #
    # find an op without Q-time and add one
    assert oinfo = @@sm.object_info(:mainpd, route).first
    assert qtidx = oinfo.specific[:operations].find_index {|e| e.qtimes.empty? && e.opNo != qtremove.opNo}
    qt_trigger = oinfo.specific[:operations][qtidx].opNo
    qt_target = oinfo.specific[:operations][qtidx + 1].opNo
    assert qt = ec.add_qtime(route, qt_trigger, qt_target)
    action = {action: 'DispatchPrecede', duration: @@qt_duration - 1, criticality: 'C2'}
    assert ec.add_timelink_action(route, qt, action)
    action = {action: 'ImmediateHold', duration: @@qt_duration, criticality: 'C2', reasonCode: 'LotHold.QTC2'}
    assert ec.add_timelink_action(route, qt, action)
    # verify changes
    assert changes = ec.changes
    assert_equal 2 + qtremove.actions.length, changes['timelinks']['total']
    assert_equal qtremove.actions.length, changes['timelinks']['totalRemoved']
    assert_equal 2, changes['timelinks']['totalAdded']
    #
    # complete the EC
    assert ec.complete_ec(approver_session: @@ecmtest.session2)
    #
    # verify Q-times in SiView
    assert oinfo = @@sm.object_info(:mainpd, route).first
    assert_equal 'Released', oinfo.state
    # verify Q-time removal
    assert opinfo = oinfo.specific[:operations].find {|o| o.opNo == qtremove.opNo}
    assert_empty opinfo.qtimes, "QT has not been removed"
    # verify Q-time added
    assert opinfo = oinfo.specific[:operations].find {|o| o.opNo == qt_trigger}
    assert_equal 2, opinfo.qtimes.size
    #
    $setup_ok = true
  end

  def test212_2routes
    # create 2 SiView routes and an EC for them
    assert routes = @@routes ? @@routes[2..3] : @@ecmtest.new_testobject(:mainpd, n: 2)
    assert ec = @@ecmtest.new_ecroute(routes), "connection issues"
    @@ecs << ec
    # add a lagtime to 1st route
    assert oinfo = @@sm.object_info(:mainpd, routes[0]).first
    assert lttriggerop = oinfo.specific[:operations].find {|e| e.lagtime == 0}
    assert lt = ec.add_lagtime(routes[0], lttriggerop.opNo)
    ltduration = 3
    assert ec.add_timelink_action(routes[0], lt, action: 'lagtime', duration: ltduration)
    # add qtime to 2nd route
    assert oinfo = @@sm.object_info(:mainpd, routes[1]).first
    assert qtidx = oinfo.specific[:operations].find_index {|e| e.qtimes.empty?}
    ###assert qtidx = oinfo.specific[:operations].find_index {|e| e.qtimes.empty? && e.opNo != lttriggerop.opNo}
    qt_trigger = oinfo.specific[:operations][qtidx].opNo
    qt_target = oinfo.specific[:operations][qtidx + 1].opNo
    assert qt = ec.add_qtime(routes[1], qt_trigger, qt_target)
    action = {action: 'DispatchPrecede', duration: @@qt_duration - 1, criticality: 'C2'}
    assert ec.add_timelink_action(routes[1], qt, action)
    action = {action: 'ImmediateHold', duration: @@qt_duration, criticality: 'C2', reasonCode: 'LotHold.QTC2'}
    assert ec.add_timelink_action(routes[1], qt, action)
    # release simultaneously
    assert ec.complete_ec(approver_session: @@ecmtest.session2), "EC not completed successfully"
    #
    # verify in SiView
    oinfo = @@sm.object_info(:mainpd, routes[0]).first
    assert_equal 'Released', oinfo.state
    assert opinfo = oinfo.specific[:operations].find {|o| o.opNo == lttriggerop.opNo}
    assert_equal ltduration, opinfo.lagtime
    oinfo = @@sm.object_info(:mainpd, routes[1]).first
    assert_equal 'Released', oinfo.state
    assert opinfo = oinfo.specific[:operations].find {|o| o.opNo == qt_trigger}
    assert_equal 2, opinfo.qtimes.size
  end

  # route changes in ECM and SiView at different operations

  def test221_siview_changes
    # create a SiView route and an EC for it
    assert route = @@routes ? @@routes[4] : @@ecmtest.new_testobject(:mainpd)
    assert ec = @@ecmtest.new_ecroute(route), "connection issues"
    @@ecs << ec
    # add a lagtime
    assert oinfo = @@sm.object_info(:mainpd, route).first
    assert lttriggerop = oinfo.specific[:operations].find {|e| e.lagtime == 0}
    assert lt = ec.add_lagtime(route, lttriggerop.opNo)
    assert ec.add_timelink_action(route, lt, action: 'lagtime', duration: 3)
    #
    # in SiView remove an already existing lagtime and wait for sync
    assert ltexist = oinfo.specific[:operations].reverse.find {|e| e.lagtime > 0}
    assert @@sm.mainpd_change_lagtime(route, ltexist.opNo, 0)
    assert @@ecmtest.wait_ecm_sync(route) {|newec|
      !newec.ecroute_timelinks(route, lagtime: true).find {|e| e.opNo == ltexist.opNo}
    }, "no update from SiView"
    # validate EC
    assert_empty ec.validation, "#{ec.ecname} cannot be submitted"
    #
    # in SiView change a route UDATA and wait for sync
    ukey, uvalue = 'RouteSpecialinfo', unique_string
    assert @@sm.update_udata(:mainpd, route, {ukey=>uvalue})
    assert @@ecmtest.wait_ecm_sync(route) {|newec|
      newec.ecroute_udata(route)[ukey] == uvalue
    }, "no update from SiView"
    # validate EC
    assert_empty ec.validation, "EC #{ec.ecname} cannot be submitted"
    #
    # in SiView remove an already existing Q-time and wait for sync
    assert qtop = oinfo.specific[:operations].find {|e| !e.qtimes.empty?}
    assert @@sm.mainpd_delete_qtime(route, qtop.opNo, qtop.qtimes)
    assert @@ecmtest.wait_ecm_sync(route) {|newec|
      !newec.ecroute_timelinks(route, qtime: true).find {|e| e.opNo == qtop.opNo}
    }, "no update from SiView"  # v1.7.10 ECM-4417
    # validate EC, currently fails with CONSISTENCY_CHECK
    assert_empty ec.validation, "#{ec.ecname} cannot be submitted"
  end

  def test291_validation_rules_qt
    # create a SiView route and an EC for it
    assert route = @@routes ? @@routes[5] : @@ecmtest.new_testobject(:mainpd)
    assert ec = @@ecmtest.new_ecroute(route)
    @@ecs << ec
    #
    # validate MUST_MAKE_CHANGE
    assert ec.verify_validation('MUST_MAKE_CHANGE', only: true)
    assert_equal 0, ec.impact['total']
    #
    # add Q-time w/o action
    assert qt = ec.add_qtime(route, @@qt_trigger, @@qt_target)
    assert ec.verify_validation('MUST_MAKE_CHANGE', only: true)
    #
    # add FutureHold with wrong operation not after QT area and verify ECM.Rule.QT.ActionOperNum
    action = {action: 'FutureHold', duration: @@qt_duration, criticality: 'C2',
      reasonCode: 'FutureHold.QTC2', timing: 'PRE', actionOperationNumber: @@qt_trigger2}
    assert aid = ec.add_timelink_action(route, qt, action)
    assert ec.verify_validation('ECM.Rule.QT.ActionOperNum')
    #
    # remove wrong FutureHold, add LimitOnly and validate ECM.Rule.QT.MultiActionDispatchCount
    assert ec.delete_timelink_action(route, qt, aid)
    action = {action: 'LimitOnly', duration: @@qt_duration, criticality: 'C2'}
    assert aid = ec.add_timelink_action(route, qt, action)
    assert ec.verify_validation('ECM.Rule.QT.MultiActionDispatchCount', accept_warning: true, only: true)
    #
    # add missing DispatchPrecede with different criticality and verify
    action = {action: 'DispatchPrecede', duration: @@qt_duration-1, criticality: 'C1'}
    assert aid = ec.add_timelink_action(route, qt, action)
    assert ec.verify_validation('ECM.Rule.QT.CustomField', only: true) # strange rule name, description is OK
    #
    # correct criticality but set a wrong duration and verify MultiActionDispatch
    assert ec.update_timelink_action(route, qt, aid, duration: @@qt_duration, criticality: 'C2')
    assert ec.verify_validation('ECM.Rule.QT.MultiActionDispatch', only: true)
    #
    # correct duration and verify no error
    assert ec.update_timelink_action(route, qt, aid, duration: @@qt_duration-1)
    assert_empty ec.validation
    #
    # set lagtime at QT target op and verify no error
    assert lt = ec.add_lagtime(route, @@qt_target)
    action = {action: 'LimitOnly', duration: 1, criticality: 'C2'}
    assert aid = ec.add_timelink_action(route, lt, action)
    assert_empty ec.validation # wrong TL.DoesntExcedeDuration error, ECM-3044
    #
    # MOVE To ECM_RteOp: set RouteOpSafeOp UDATA at trigger PD and validate (fails in 1.4.12, passes in 1.5.10)
    assert ec.update_operation_udata(route, @@qt_trigger, 'RouteOpSafeOp', 'PRElong')
    assert ec.update_operation(route,  @@qt_trigger, mandatory: true) # to fix RouteOpSafeOp issues
    assert_empty ec.validation
    #
    # set RouteOpSafeOp UDATA at target PD and validate
    assert ec.update_operation_udata(route, @@qt_target, 'RouteOpSafeOp', 'PRElong')
    assert ec.update_operation(route, @@qt_target, mandatory: true) # to fix RouteOpSafeOp issues
    assert ec.verify_validation('ECM.Rule.OP.Udata.SafeOperationValid', only: true)
    # remove UDATA
    assert ec.update_operation_udata(route, @@qt_trigger, 'RouteOpSafeOp', '')
    assert ec.update_operation_udata(route, @@qt_target, 'RouteOpSafeOp', '')
    #
    # add 2nd Q-time with correct actions and verify the duplicate is detected
    assert qt2 = ec.add_qtime(route, @@qt_trigger, @@qt_target)
    refute_empty ec.validation              # multiple errors shown
    action = {action: 'LimitOnly', duration: @@qt_duration, criticality: 'C2'}
    assert ec.add_timelink_action(route, qt2, action)
    action = {action: 'DispatchPrecede', duration: @@qt_duration-1, criticality: 'C2'}
    assert ec.add_timelink_action(route, qt2, action)
    assert ec.verify_validation('ECM.Rule.QT.DuplicateOnLevel', only: true)
    #
    # remove duplicate qtime and add a nested one
    assert ec.delete_timelink(route, qt2)
    assert qt2 = ec.add_qtime(route, @@qt_trigger2, @@qt_target)
    assert ec.verify_validation('ECM.Rule.QT.Nested')
    #
    # remove nested qtime and add an intersecting one
    assert ec.delete_timelink(route, qt2)
    assert qt2 = ec.add_qtime(route, @@qt_trigger2, @@qt_target2)
    assert ec.verify_validation('ECM.Rule.QT.Intersecting')
    #
    # remove intersecting Q-time and add a lagtime with same duration as Q-time and verify
    assert ec.delete_timelink(route, qt2)
    assert lt1 = ec.add_lagtime(route, @@qt_trigger)
    action = {action: 'LimitOnly', duration: @@qt_duration, criticality: 'C2'}
    assert aid = ec.add_timelink_action(route, lt1, action)
    assert ec.verify_validation('ECM.Rule.TL.DoesntExcedeDuration', only: true)
    #
    # remove Q-time and add a 2nd lagtime, verify duplicate lagtime is detected
    assert ec.delete_timelink(route, qt)
    assert lt2 = ec.add_lagtime(route, @@qt_trigger)
    assert ec.add_timelink_action(route, lt2, action: 'lagtime', duration: @@qt_duration-2)
    assert ec.verify_validation('ECM.Rule.LT.Duplicate', only: true)
    #
    # remove 1st lagtime and verify
    assert ec.delete_timelink(route, lt1)
    assert_empty ec.validation, "wrong validation failure"
    #
    ## ECM.Rule.QT.RemovedQtimeNotReplacement
  end

end
