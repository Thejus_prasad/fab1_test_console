=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author:  sfrieske, 2016-04-14

Version: 1.2.0

=end

require_relative 'ECMTestCase'


# General route edit tests.
class ECM_Rte < ECMTestCase

  def test110_ec_states
    $setup_ok = false
    #
    # create a SiView route and an EC for it
    assert route =  @@ecmtest.new_testobject(:mainpd)
    assert ec = @@ecmtest.new_ecroute(route), "connection issues"
    @@ecs << ec
    #
    # verify EC is in MyECs and in state EDIT
    assert myecs = ec.session.ecs_user
    assert myecs['data'].find {|e| e['ecId'] == ec.ecname}
    assert_equal 'EDIT', ec.metadata['state']
    #
    # validate MUST_MAKE_CHANGE
    vals = ec.validation
    assert_equal 1, vals.length
    assert_equal 'MUST_MAKE_CHANGE', vals[0]['name']
    assert_equal 0, ec.impact['total']
    #
    # remove an existing lagtime (must be done first)
    assert ltremove = ec.ecroute_timelinks(route, lagtime: true).last
    assert ec.delete_timelink(route, ltremove.id)
    assert changes = ec.changes
    assert_equal 1, changes['timelinks']['total']
    assert_equal 1, changes['timelinks']['totalRemoved']
    #
    # add a lagtime at a different op
    assert oinfo = @@sm.object_info(:mainpd, route).first
    assert ltadd = oinfo.specific[:operations].find {|e| e.lagtime == 0 && e.opNo != ltremove.opNo}
    assert lt = ec.add_lagtime(route, ltadd.opNo)
    assert changes = ec.changes
    assert_equal 2, changes['timelinks']['total']
    assert_equal 1, changes['timelinks']['totalAdded']
    #
    # delete and add again
    assert ec.delete_timelink(route, lt)
    assert changes = ec.changes
    assert_equal 1, changes['timelinks']['total']
    assert_equal 0, changes['timelinks']['totalAdded']
    assert lt = ec.add_lagtime(route, ltadd.opNo)
    assert changes = ec.changes
    assert_equal 2, changes['timelinks']['total']
    assert_equal 1, changes['timelinks']['totalAdded']
    # add an action
    ltduration = 1
    assert ec.add_timelink_action(route, lt, action: 'lagtime', duration: ltduration)
    #
    # add a dependency and description, call impact and validation (required in OPI only)
    assert ec.update_dependency(name: 'QA', description: 'Test')
    assert ec.actions(description: 'QA Test')
    assert ec.impact
    assert_empty ec.validation, "unexpected validation error"
    #
    # submit EC, recall and submit again
    assert ec.submit
    assert ec.recall
    assert_equal 'EDIT', ec.metadata['state']
    assert ec.submit
    #
    # complete pre-approvals tasks
    assert ec.complete_tasks(pre_approvals: true), "error completing pre-approval tasks"
    #
    # try to approve as submitting user, must fail (ECM-2346)
    ## refute ec.approve  # http://f1jira:11080/browse/ECM-2346
    # approve EC as different user
    #impact/validation?
    assert ec.approve(session: @@ecmtest.session2)
    #
    # complete tasks and verify the changes in SiView
    assert ec.complete_tasks, "error completing tasks"
    oinfo = @@sm.object_info(:mainpd, route).first
    assert_equal 'Released', oinfo.state
    # change notices in MTDC have an suffix like EC00011sv
    assert oinfo.props.start_with?(ec.ecname), "wrong Change Notice"
    # verify lagtime removed
    assert opinfo = oinfo.specific[:operations].find {|o| o.opNo == ltremove.opNo}
    assert_equal 0, opinfo.lagtime, "lagtime has not been removed"
    # verify lagtime added
    assert opinfo = oinfo.specific[:operations].find {|o| o.opNo == ltadd.opNo}
    assert_equal ltduration, opinfo.lagtime
    #
    $setup_ok = true
  end

  def test111_changes_after_resolution
    assert ec = @@ecs.last
    # try to delete EC
    refute ec.delete_ec, "EC deleteable after completion"
    # try to change description, test fails in v1.4.7 (API only, ECM-2560)
    ##TODO refute ec.actions(description: 'QA Test after'), "EC description changeable after completion"
  end

  def test112_2ecs
    # create a SiView route and an EC for it
    assert route =  @@ecmtest.new_testobject(:mainpd)
    assert ec = @@ecmtest.new_ecroute(route), "connection issues"
    @@ecs << ec
    assert lt = ec.ecroute_timelinks(route, lagtime: true).last
    assert ec.update_timelink_action(route, lt.id, lt.actions.first.id, duration: lt.actions.first.duration + 1)
    assert ec.complete_ec(approver_session: @@ecmtest.session2)
    # verify CN in SiView
    oinfo = @@sm.object_info(:mainpd, route).first
    assert_equal 'Released', oinfo.state
    # change notices in MTDC have an suffix like EC00011sv
    assert oinfo.props.start_with?(ec.ecname), "wrong Change Notice"
    #
    # wait for SiView sync to ECM
    assert @@ecmtest.wait_ecm_sync(route) {|newec|
      tls = newec.ecroute_timelinks(route, lagtime: true)
      tls && tls.last && tls.last.actions.first.duration == lt.actions.first.duration + 1
    }, "no update from SiView"
    #
    # create and process another EC with the same route
    assert ec2 = @@ecmtest.new_ecroute(route), "connection issues"
    @@ecs << ec2
    assert lt2 = ec2.ecroute_timelinks(route, lagtime: true).last
    assert ec2.update_timelink_action(route, lt2.id, lt2.actions.first.id, duration: lt2.actions.first.duration + 2)
    assert ec2.complete_ec(approver_session: @@ecmtest.session2)
    # verify new CN in SiView
    oinfo = @@sm.object_info(:mainpd, route).first
    assert_equal 'Released', oinfo.state
    # change notices in MTDC have an suffix like EC00011sv
    assert oinfo.props.start_with?(ec2.ecname), "wrong Change Notice"
  end

  def test121_change_obsolete_route
    # create an obsolete SiView route and an EC for it
    assert route =  @@ecmtest.new_testobject(:mainpd, obsolete: true)
    assert ec = @@ecmtest.new_ecroute(route), "connection issues"
    @@ecs << ec
    # increment any lagtime, should always work
    assert lt = ec.ecroute_timelinks(route, lagtime: true).last, "incomplete sync?"
    assert ec.update_timelink_action(route, lt.id, lt.actions.first.id, duration: lt.actions.first.duration + 1)
    # verify change has been made but does not pass validation or submit
    assert_equal 1, ec.impact['total']
    assert ec.verify_validation('MUST_MAKE_CHANGE', only: true) ||
      !ec.complete_ec(approver_session: @@ecmtest.session2), "#{ec.ecname} with an obsolete route must fail"
  end

  def test122_change_obsolete_route2 # fails in 1.5.29 API (GUI doesn't show the route), ECM-3410, v1.7
    # create a NOT obsolete SiView route and an EC for it
    assert route =  @@ecmtest.new_testobject(:mainpd)
    assert ec = @@ecmtest.new_ecroute(route), "connection issues"
    @@ecs << ec
    # increment the lagtime, should always work
    assert lt = ec.ecroute_timelinks(route, lagtime: true).last, "incomplete sync?"
    assert ec.update_timelink_action(route, lt.id, lt.actions.first.id, duration: lt.actions.first.duration + 1)
    # complete EC until before SiView submittal
    assert_empty ec.validation, "unexpected validation error"
    assert ec.update_dependency(name: 'QA', description: 'Test')
    assert ec.actions(description: 'QA Test')
    assert ec.impact
    assert ec.submit
    assert ec.complete_tasks(pre_approvals: true)
    assert ec.approve(session: @@ecmtest.session2)
    # in SiView, make the route obsolete
    assert @@sm.obsolete_mainobject(:mainpd, route)
    # try to release the EC
    task = ec.tasks_pending.first
    $log.info "trying to complete task #{task['data']['name']}"
    refute ec.complete_task(task)   # fails in v1.7.6, ECM-3410
  end

  def test131_siview_change_udata
    # create a SiView route and an EC for it
    assert route =  @@ecmtest.new_testobject(:mainpd)
    assert ec = @@ecmtest.new_ecroute(route)
    @@ecs << ec
    # remove a lagtime, just to have a change
    assert ltremove = ec.ecroute_timelinks(route, lagtime: true).first
    assert ec.delete_timelink(route, ltremove)
    assert_empty ec.validation
    #
    # in SiView change a route UDATA and wait for sync
    ukey, uvalue = 'RouteSpecialinfo', unique_string
    assert @@sm.update_udata(:mainpd, route, {ukey=>uvalue})
    assert @@ecmtest.wait_ecm_sync(route) {|newec|
      newec.ecroute_udata(route)[ukey] == uvalue
    }, "no update from SiView"
    # validate EC
    assert_empty ec.validation, "EC #{ec.ecname} cannot be submitted"
  end

  def test192_validation_after_release
    # create a SiView route and an EC for it
    assert route =  @@ecmtest.new_testobject(:mainpd)
    assert ec = @@ecmtest.new_ecroute(route)
    @@ecs << ec
    assert rdata = ec.ecroute_rdata(route)
    assert rlts = ec.ecroute_timelinks(nil, rdata: rdata, lagtime: true)
    #
    # make a change (remove lagtime)
    assert ltremove = rlts.first, "incomplete sync?"
    assert ec.delete_timelink(route, ltremove)
    assert ec.complete_ec(last_state: 'POST_RELEASE')
    # wait for next sync and validate
    $log.info "waiting #{@@ecmtest.sync_duration}s for sync"; sleep @@ecmtest.sync_duration
    # validation must not fail (wrong CONSISTENCY_CHECK, ECM-2116, still in 1.5.x)
    assert_empty ec.validation, "wrong validation failure"
  end

end
