=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.
    
Author: Steffen Steidten, 2014-12-15

Version: 1.41
=end

require_relative 'Test_CP'


# Show the loader run durations
class Test_CP29 < Test_CP

  def test01_durations
    fname = "log/cploaders_#{$env}_#{Time.now.utc.iso8601.gsub(':', '-')}.txt"
    durations = Hash[$cp.loader_run_durations.keys.sort.collect {|k| [k, $cp.loader_run_durations[k]]}]
    File.binwrite(fname, durations.to_yaml)
    $log.info "\n\nloader run durations, see #{fname}:\n#{durations.pretty_inspect}\n\n"
  end
  
end
