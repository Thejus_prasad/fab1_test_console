=begin 
Test TurnKey/ERP communication via FabGUI as prerequisite for lot transfers FAB/BMP or BMP/SRT
see http://fast/request/msr/Main/Frame.asp?appId=1&requestId=483931 (TurnKey)
and http://fast/request/msr/Main/Frame.asp?appId=1&requestId=483934 (FabGUI)

(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.
    
Author: Steffen Steidten, 2011-04-28

Version: 11.5.0
=end

require "RubyTestCase"
require "fabgui"
require "turnkey"

# Test TurnKey/ERP communication via FabGUI as prerequisite for lot transfers FAB/BMP or BMP/SRT
# see http://fast/request/msr/Main/Frame.asp?appId=1&requestId=483931 (TurnKey)
# and http://fast/request/msr/Main/Frame.asp?appId=1&requestId=483934 (FabGUI)

class Test_ERP_LotTransfer < RubyTestCase
  Description = "Test TurnKey/ERP communication via FabGUI"
  
  @@lot_fabgui = "U14BH.00"
  
  @@tk_l = SiView::MM::LotTkInfo.new("L", "CT")
  @@erpasmlot_l = ERPMES::ErpAsmLot.new(nil, @@tk_l, "*EVEREST1AA-QE-JA0", nil, "TKEY01.A0", "C02-TKEY.01", "gf", "PB")
  @@tk_s = SiView::MM::LotTkInfo.new("S", "", "CS")
  @@erpasmlot_s = ERPMES::ErpAsmLot.new(nil, @@tk_s, "*EVEREST1AA-QE-JA0", nil, "TKEY01.A0", "C02-TKEY.01", "gf", "PB")
  @@tk_j = SiView::MM::LotTkInfo.new("J", "CT", "GG")
  @@erpasmlot_j = ERPMES::ErpAsmLot.new(nil, @@tk_j, "EVEREST1AA-QE-JA0", nil, "TKEY01.A0", "C02-TKEY.01", "gf", "PB")
 
  @@run_communication_test = "false"   # test FabGUI - ERP communication
  @@run_lottransfer_test = "true"     # test that FabGUI and ERP handle lotTransfer requests correctly

  
  def setup
    fail "NEED TO FIX XMLHash RESPONSES BELOW!!"
    super
    $setup_ok = ($setup_ok != false)
    assert $setup_ok, "test setup is not correct"
  end
  
  def test01_setup
    $tkey = TurnKey.new($env, :sv=>$sv)
    $fgu = FabGUI::Client.new $env    #($env == "itdc" ? "dev" : $env)  # TODO
    @@lots = []
  end
  
  def test11_communication
    # test FabGUI - ERP communication
    ($log.warn "skipped"; return) unless @@run_communication_test == "true"
    $setup_ok = false
    assert_not_nil $fgu.lot_transfer(@@lot_fabgui), "FabGUI communication error?"
    # true or false does not matter, as long as the service replies
    # TODO!! XMLHash
    resp = $fgu._service_response($fgu.response)["lotTransferReturn"]["details"]["code"]
    assert resp.start_with?("ONEERP_MES_TKLOTTRANSFER_SERVICE_")
    $setup_ok = true
  end
  
  def test12_lottransfer_bump_l
    # happy lotTransfer scenario, type "L"
    ($log.warn "skipped"; return) unless @@run_lottransfer_test == "true"
    # create and locate a new lot
    lot = $tkey.new_lot(@@erpasmlot_l)
    @@lots << lot if lot
    lcdata = $tkey.lot_locate(lot, :erpstatechgbump)
    assert lcdata, "error locating lot #{lot} to :erpstatechgbump"
    # send msg to FabGUI
    assert $fgu.lot_transfer(lot), "1st lot_transfer of lot #{lot} failed"
    # re-send msg
    assert !$fgu.lot_transfer(lot), "2nd lot_transfer of lot #{lot} must fail"
    sresp = $fgu._service_response($fgu.response)
    msg = sresp["lotTransferReturn"]["details"]["msg"]
    assert msg.index("Duplicate LotID"), "ERP processing error?\n#{sresp.inspect}"
  end
  
  def test13_lottransfer_bump_j
    # happy lotTransfer scenario, type "J"
    ($log.warn "skipped"; return) unless @@run_lottransfer_test == "true"
    # create and locate a new lot, tksubcon sort always GG
    lot = $tkey.new_lot(@@erpasmlot_j)
    @@lots << lot if lot
    lcdata = $tkey.lot_locate(lot, :erpstatechgbump)
    assert lcdata, "error locating lot #{lot} to :erpstatechgbump"
    # send msg to FabGUI
    assert $fgu.lot_transfer(lot), "1st lot_transfer of lot #{lot} failed"
    # re-send msg
    assert !$fgu.lot_transfer(lot), "2nd lot_transfer of lot #{lot} must fail"
    sresp = $fgu._service_response($fgu.response)
    msg = sresp["lotTransferReturn"]["details"]["msg"]
    assert msg.index("Duplicate LotID"), "ERP processing error?\n#{sresp.inspect}"
  end

  def test22_lottransfer_sort_s
    return
    # happy lotTransfer scenario, type "S"
    ($log.warn "skipped"; return) unless @@run_lottransfer_test == "true"
    # create and locate a new lot
    lot = $tkey.new_lot(@@erpasmlot_s)
    @@lots << lot if lot
    lcdata = $tkey.lot_locate(lot, :erpstatechgsort)
    assert lcdata, "error locating lot #{lot} to :erpstatechgsort"
    # send msg to FabGUI
    assert $fgu.lot_transfer(lot), "1st lot_transfer of lot #{lot} failed"
    # re-send msg
    assert !$fgu.lot_transfer(lot), "2nd lot_transfer of lot #{lot} must fail"
    sresp = $fgu._service_response($fgu.response)
    msg = sresp["lotTransferReturn"]["details"]["msg"]
    assert msg.index("Duplicate LotID"), "ERP processing error?\n#{sresp.inspect}"
  end
  
  def test23_lottransfer_sort_j
    return  # missing setup
    # happy lotTransfer scenario, type "J"
    ($log.warn "skipped"; return) unless @@run_lottransfer_test == "true"
    # create and locate a new lot, tksubcon sort always GG
    lot = $tkey.new_lot(@@erpasmlot_j)
    @@lots << lot if lot
    lcdata = $tkey.lot_locate(lot, :erpstatechgsort)
    assert lcdata, "error locating lot #{lot} to :erpstatechgsort"
    # send msg to FabGUI
    assert $fgu.lot_transfer(lot), "1st lot_transfer of lot #{lot} failed"
    # re-send msg
    assert !$fgu.lot_transfer(lot), "2nd lot_transfer of lot #{lot} must fail"
    sresp = $fgu._service_response($fgu.response)
    msg = sresp["lotTransferReturn"]["details"]["msg"]
    assert msg.index("Duplicate LotID"), "ERP processing error?\n#{sresp.inspect}"
  end

  def test98_cleanup
    @@lots.each {|lot| $sv.wafer_sort_req(lot, "")}
  end
end
