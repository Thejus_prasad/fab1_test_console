=begin
Create test data for FabView regressiontest

(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

    load_run_testcase "Test_FabView", "itdc"

Author: Conny Stenzel, 2012-10-10
        Christian Lauenroth, 2013-01-03
=end

require "RubyTestCase"
require "toolevents"

class Test_FabView < RubyTestCase
  Description = "Test script for the FabView test"

	@@raw_bank = "UT-RAW"
	@@raw_product = "UT-RAW.00"
  @@product = "UT-PRODUCT001.01"
  @@route = "UTRT001.01"
	@@carrier = "FVW%"

	@@eqp = "FVW001"
	@@chamber = "CHA"
	@@opNo = "1000.600"
	@@eqpIB = "UTI002"
	@@opNoIB = "1000.200"
	
	@@hold_reason = "ENG"
	@@release_reason = "ALL"
	@@inhibit_reason = "RICO"
	@@cancel_reason = "ICWS"
	
	@@reticle_eqp = "UTR004"
	@@reticle_chamber = "PM1"
	@@reticle = "UTRETICLE0129"
	@@reticle_opNo = "1000.950"
	
	@@branch_pd = "UTBR002.01"
	
	def setup
		super
		$whrpt = ToolEvents::WaferHistory.new($env) unless $whrpt and $whrpt.env == $env
	end
	
	
  def test00_setup
		assert_equal 0, $sv.eqp_mode_offline1(@@eqp, :notify=>false)
	end

	# Lot Events
 	
	def test01_data_generation
		# STB
		vlot = $sv.vendorlot_receive(@@raw_bank, @@raw_product, 25, :prepare=>true, :carrier=>@@carrier)
		lot = $sv.new_lot_release(:stb=>true, :srclot=>vlot, :product=>@@product, :route=>@@route)
		assert lot, "error creating lot"
		@@lot = lot
		# release possible lot hold from jCAP
		$log.info "waiting 30s for jCAP holds"
		sleep 30
		$sv.lot_hold_release(lot, nil)
		
		# LocateForward
		assert_equal 0, $sv.lot_opelocate(lot, @@opNo)
		
		# split lot
		childlot = $sv.lot_split(lot, 13)
		assert childlot, "error splitting lot #{lot}"
		$log.info "split off child lot #{childlot}"
		
		# process parent and child lots to create a wafer map for SlotStory
		sleep 1
		assert $sv.claim_process_lot(lot, :eqp=>@@eqp, :chamber=>@@chamber, :wafer_history=>$whrpt), "error processing #{lot}"
		sleep 1
		assert $sv.claim_process_lot(childlot, :eqp=>@@eqp, :chamber=>@@chamber, :wafer_history=>$whrpt), "error processing #{childlot}"
		
		# merge lot
		sleep 1
		assert_equal 0, $sv.lot_merge(lot, childlot), "error merging #{lot}"
		
		# lot hold/release
		sleep 1
		assert_equal 0, $sv.lot_hold(lot, @@hold_reason)
		sleep 1
		assert_equal 0, $sv.lot_hold_release(lot, @@hold_reason, :release_reason=>@@release_reason)
		
		# process lot on internal buffer tool (no wafer history)
		sleep 1
		assert_equal 0, $sv.lot_opelocate(lot, @@opNoIB)
		assert $sv.claim_process_lot(lot, :eqp=>@@eqpIB), "error processing #{lot}"
		
		# process lot on a reticle tool
		assert_equal 0, $sv.lot_opelocate(lot, @@reticle_opNo)
		assert $sv.claim_process_lot(lot, :eqp=>@@reticle_eqp, :chamber=>@@reticle_chamber, :wafer_history=>$whrpt), "error processing #{lot}"
		
		# scrap/scrap cancel
		assert_equal 0, $sv.scrap_wafers(lot)
		sleep 1
		assert_equal 0, $sv.scrap_wafers_cancel(lot)
		
		
		# inhibits
		inh = [
			[:eqp_chamber, @@eqp, 											@@chamber],
			[:reticle_eqp, [@@reticle, @@reticle_eqp],	nil],
		]
		inh.each {|args|
			sleep 5
			assert_equal 0, $sv.inhibit_entity(args[0], args[1], :attrib=>args[2], :reason=>@@inhibit_reason)
			sleep 5
			assert_equal 0, $sv.inhibit_cancel(args[0], args[1], :attrib=>args[2], :inhibit_reason=>@@inhibit_reason, :cancel_reason=>@@cancel_reason)
		}
	end
	
end