=begin 
Stress test the TurnKey/ERP communication via FabGUI

(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.
    
Author: Steffen Steidten, 2011-04-28

Version: 11.5.0
=end

require "RubyTestCase"
require "fabgui"
require "turnkey"
require "yaml"

# Stress test the TurnKey/ERP communication via FabGUI
class Test_ERP_LotTransfer_Stress < RubyTestCase
  Description = "Stress test the TurnKey/ERP communication via FabGUI"
  
  @@testlot = "U14BH.00"

  @@tk_j = SiView::MM::LotTkInfo.new("J", "CT", "GG")
  @@erpasmlot_j = ERPMES::ErpAsmLot.new(nil, @@tk_j, "EVEREST1AA-QE-JA0", nil, "TKEY01.A0", "C02-TKEY.01", "gf", "PB")

  @@run_communication_test = "true"   # test TKey - FabGUI - ERP communication
  @@run_lottransfer_test = "true"    # stress test
  
  # test duration in seconds, minutes ("30 m") or hours ("1 h")
  @@test_duration = "1 m"

  
  def setup
    fail "NEED TO FIX XMLHash RESPONSES BELOW!!"
    super
    $setup_ok = ($setup_ok != false)
    assert $setup_ok, "test setup is not correct"
  end
  
  def test01_setup
    $tkey = TurnKey.new($env, :sv=>$sv)
    $fgu = FabGUI::Client.new($env)
    @@lots = []
    @@lots_bad = []
  end
  
  def test11_communication
    # test FabGUI - ERP communication
    ($log.warn "skipped"; return) unless @@run_communication_test == "true"
    $setup_ok = false
    assert_not_nil $fgu.lot_transfer(@@testlot), "FabGUI communication error?"
    # true or false does not matter, as long as the service replies
    # TODO!! XMLHash
    resp = $fgu._service_response($fgu.response)["lotTransferReturn"]["details"]["code"]
    assert resp.start_with?("ONEERP_MES_TKLOTTRANSFER_SERVICE_")
    $setup_ok = true
  end
  
  def test21_stress
    ($log.warn "skipped"; return) unless @@run_lottransfer_test == "true"
    # calculate test duration
    d = get_test_duration(nil)
    sleeptime = 10
    @@test_start = Time.now + sleeptime
    @@test_end_planned = @@test_start + d
    $log.info "test duration: #{d}s (#{@@test_duration}), ending at #{@@test_end_planned.iso8601}"
    sleep sleeptime
    # start test
    @@test_ok = true
    while Time.now < @@test_end_planned
      ok = lot_create_transfer
      $log.info "LotTransfer ok: #{ok.inspect}\n-------------------------------------------------"
      @@test_ok &= ok
      @@test_end = Time.now
      ##assert @@test_ok, "error creating or transferring lot"
    end
    @@test_end = Time.now
  end
  
  def test98_stats
    stats = Test_ERP_LotTransfer_Stress.stats
    puts "\n\n"
    puts "LotTransfer Stress Test from #{stats[:tstart].iso8601} .. #{stats[:tend].iso8601}"
    puts "=" * 83
    puts "processed #{stats[:lots].size} lots, test ok: #{stats[:ok]}"
    unless stats[:ok]
      puts "premature test end at #{stats[:tend].iso8601} instead of #{stats[:tend_planned].iso8601}"
      puts "#{stats[:lots_bad].size} out of #{stats[:lots].size} have not been processed correctly"
    end
    open("Test_ERP_LotTransfer_Stress", "wb") {|fh| fh.write(stats.to_yaml)}
  end
  
  
  def get_test_duration(s)
    # conver test duration from a string: seconds, minutes ("30 m") or hours ("1 h")
    # return duration in seconds as integer
    s ||= @@test_duration
    d = s.to_f
    d *= 60 if s.end_with?("m") or s.end_with?("min")
    d *= 3600 if s.end_with?("h")
    return d
  end
  
  def lot_create_transfer
    # create a new lot and transfer it
    # return true on successful transfer, nil on any other error
    lot = $tkey.new_lot(@@erpasmlot_j)
    return nil unless lot
    @@lots << lot
    ok = $tkey.lot_locate(lot, :lotshipdesig)
    return nil unless ok
    ret = $fgu.lot_transfer(lot)
    # remove lot from carrier
    $sv.delete_lot_family(lot, :delete=>false)
    @@lots_bad << lot unless ret
    return ret
  end
  
  def self.stats
    {:lots=>@@lots, :lots_bad=>@@lots_bad, 
      :tstart=>@@test_start, :tend_planned=>@@test_end_planned, :tend=>@@test_end, :ok=>@@test_ok}
  end
end
