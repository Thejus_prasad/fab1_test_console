=begin 
Test the jCAP TemporalSpaceIncreaser job.

(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.
    
    to run the tests from the test console:
    load_run_testcase "Test_jCAP_TempSpaceInc.rb", "itdc"
    
Author: Thomas Klose, 2011-03-08
=end

=begin
  test steps:
    1 create lots
    2 check split
    3 check separate, do separate
    4 check gate pass
    5 locate
    6 check combine, do combine
    7 check merge
    8 check gate pass
    
    Notes: carrier transfer status must not be "EI"
=end

require "RubyTestCase"
require "sjc"
require "monitor"

# Test the jCAP TemporalSpaceIncreaser job.
#
# test steps:
# 
# 1 create lots
#    
# 2 check split
#
# 3 check separate, do separate
#
# 4 check gate pass
#
# 5 locate
#
# 6 check combine, do combine  
#
# 7 check merge
#
# 8 check gate pass   
#
# Notes: carrier transfer status must not be "EI"

class Test_jCAP_TempSpaceInc < RubyTestCase
  Description = "Test jCAP Temporal Space Increaser job"
  
  include MonitorMixin
  
  # test cases
  @@test_small_lot = "true"
  @@test_normal_lot = "true"
  @@test_scrap_lot = "true"
  @@test_2nd_pd = "true"
  ##@@test_wo_combine = "trueX"
  
  # operations
  @@ope_separate = "5000.1100"
  @@ope_combine = "5000.1400"
  @@ope_separate2 = "5100.1100"
  @@ope_combine2 = "5100.1400"
  # route  
  @@route = "ITDC-JCAP-TEST.01"
  @@product = "3260CH.00"  
  
  
  def test11_start_tests
    # prepare test
    @@result = {}
    threads = []
    # SJC service and MDS connection
    $sjcmds = SJC::MDS.new($env)
    $sjcdel = SJC::SJCDelete.new($env)
    # start one test thread per lot
    threads << Thread.new {
      # 9 wafer lot, no combine needed #### BROKEN, NEED TO SPLIT BEFORE SEPARATE ssteidte, 2011-10-12
      lot = start_lot(9, @@ope_separate)
      if lot
        @@result[lot] = []
        @@result[lot] << nil #check_split(lot) unless @@result[lot].member?(false)
        @@result[lot] << check_separate(lot) unless @@result[lot].member?(false)
        @@result[lot] << check_gatepass(lot) unless @@result[lot].member?(false)
        @@result[lot] << locate_to_combine(lot, @@ope_combine) unless @@result[lot].member?(false)
        @@result[lot] << nil #check_combine(lot) unless @@result[lot].member?(false)
        @@result[lot] << nil #check_merge(lot) unless @@result[lot].member?(false)
        @@result[lot] << check_gatepass(lot) unless @@result[lot].member?(false)  
        $log.info("completed test with lot #{lot}")
      else
        @@result["test_small_lot"] = [false]
      end
    } if @@test_small_lot == "true"
    threads << Thread.new {
      # 25 wafer lot, happiest lot
      sleep 15
      lot = start_lot(25, @@ope_separate)
      if lot
        @@result[lot] = []
        @@result[lot] << check_split(lot) unless @@result[lot].member?(false)
        @@result[lot] << check_wafermap(lot) unless @@result[lot].member?(false)   # check slot map for lot
        @@result[lot] << check_wafermap(get_child_lot(lot)) unless @@result[lot].member?(false) # check slot map for child lot
        @@result[lot] << check_separate(lot) unless @@result[lot].member?(false)
        @@result[lot] << check_gatepass(lot) unless @@result[lot].member?(false)
        @@result[lot] << locate_to_combine(lot, @@ope_combine) unless @@result[lot].member?(false)
        @@result[lot] << check_combine(lot) unless @@result[lot].member?(false)
        @@result[lot] << check_merge(lot) unless @@result[lot].member?(false)
        @@result[lot] << check_gatepass(lot) unless @@result[lot].member?(false)
        $log.info("completed test with lot #{lot}")
      else
        @@result["test_normal_lot"] = [false]
      end
    } if @@test_normal_lot == "true"
    threads << Thread.new {
      # 25 wafer lot, scrap wafer between separate and combine
      sleep 30
      lot = start_lot(25, @@ope_separate)
      if lot
        @@result[lot] = []
        @@result[lot] << check_split(lot) unless @@result[lot].member?(false)
        @@result[lot] << check_separate(lot) unless @@result[lot].member?(false)
        @@result[lot] << check_gatepass(lot) unless @@result[lot].member?(false)
        # scrap wafer here
        scrap = $sv.lot_split(lot, 1)
        $sv.scrap_wafers(scrap)
        $sv.wafer_sort_req(scrap, "")
        @@result[lot] << locate_to_combine(lot, @@ope_combine) unless @@result[lot].member?(false)
        @@result[lot] << check_combine(lot) unless @@result[lot].member?(false)
        @@result[lot] << check_merge(lot) unless @@result[lot].member?(false)
        @@result[lot] << check_gatepass(lot) unless @@result[lot].member?(false)
        $log.info("completed test with lot #{lot}")
      else
        @@result["test_scrap_lot"] = [false]
      end
    } if @@test_scrap_lot == "true"
    threads << Thread.new {
      # 25 wafer lot, 2nd PD
      sleep 45
      lot = start_lot(25, @@ope_separate2)
      if lot
        @@result[lot] = []
        @@result[lot] << check_split(lot) unless @@result[lot].member?(false)
        @@result[lot] << check_separate(lot) unless @@result[lot].member?(false)
        clear_sr_lot(lot)
        @@result[lot] << check_gatepass(lot) unless @@result[lot].member?(false)
        @@result[lot] << locate_to_combine(lot, @@ope_combine2) unless @@result[lot].member?(false)
        @@result[lot] << check_combine(lot) unless @@result[lot].member?(false)
        @@result[lot] << check_merge(lot) unless @@result[lot].member?(false)
        @@result[lot] << check_gatepass(lot) unless @@result[lot].member?(false)
        $log.info("completed test with lot #{lot}")
      else
        @@result["test_2nd_pd"] = [false]
      end
    } if @@test_2nd_pd == "true"
    # wait for completion
    threads.each {|t| t.join}
    # show result
    test_ok = true
    puts
    puts "split, separate, gatepass, locate, combine, merge, gatepass"
    @@result.keys.sort.each {|k|
      puts "#{k}: #{@@result[k].inspect}"
      test_ok &= !@@result[k].member?(false)
    }
    assert test_ok, "at least one test failed"
  end
  
  def test99_cleanup
    # clean up all lots that passed testing
    @@result.each_pair {|lot, res|
      $sv.delete_lot_family(lot, :delete=>false) unless res.member?(false)
    }
  end

  def start_lot(wafercount, oper)
    $log.info "-- start_lot #{wafercount.inspect}, #{oper.inspect}"
    lot = $sv.new_lot_release(:stb=>true, :product=>@@product, :route=>@@route, :nwafers=>wafercount)
    return nil unless lot
    li = $sv.lot_info(lot)
    ok = clear_sr_carrier(li.carrier)
    ($log.warn "error deleting old sort requests"; return nil) unless ok
    sleep 60
    $sv.lot_hold_release(lot, nil)
    $sv.wafer_sort_req(lot, ($sv.lot_info(lot).carrier), :slots=>"shuffle")
    $sv.lot_opelocate(lot, oper)
    $log.info("  lot: #{lot}")
    return lot
  end
  
  def check_split(lot)
    # waiting for lot split operation from TSI
    $log.info "-- check_split #{lot.inspect}"
    nwafers = $sv.lot_info(lot).nwafers
    testresult = false
    60.times {
      li = $sv.lot_info(lot)
      if li.nwafers != nwafers
        testresult = true
        break
      end
      sleep 10
    }
    $log.info("  ok: #{testresult}")
    return testresult
  end
  
  def check_merge(lot)
    # waiting for merge operation from TSI
    # check wafers before and after merge
    $log.info "-- check_merge #{lot.inspect}"
    lots = $sv.lot_family(lot)
    nwafers = lots.inject(0) {|s,l| s + $sv.lot_info(l).nwafers}
    testresult = false
    60.times {
      if nwafers == $sv.lot_info(lot).nwafers.to_i
        testresult = true
        break
      end        
      sleep 10
    }
    $log.info("  check_merge #{lot.inspect} ok: #{testresult}")
    return testresult
  end
  
  def check_separate(lot)
    # check MDS for separate sort request
    $log.info "-- check_separate #{lot.inspect}"
    testresult = false
    60.times {
      res = $sjcmds.tsi_lot(:lot=>lot)
      if res and res[0].separate_srid > 0
        testresult = !!myseparate(res[0].separate_srid)
        break
      end
      sleep 10
    }
    $log.info("  ok: #{testresult}")
    return testresult
  end
  
  def check_gatepass(lot)
    # todo: check for every lot in lot family
    $log.info "-- check_gatepass #{lot.inspect}"
    testresult = false
    60.times {
      lh = $sv.lot_operation_history(lot)
      if lh[-1].category == "GatePass"
        testresult = true
        break
      end
      sleep 10
    }
    $log.info("  ok: #{testresult}")
    return testresult
  end
  
  def locate_to_combine(lot, oper)
    lotlist = $sv.lot_family(lot)
    $log.info "-- locate_to_combine #{lotlist.inspect}, #{oper.inspect}"
    ret = true
    lotlist.each {|l| 
      li = $sv.lot_info(l)
      unless li.states["Lot State"] == "FINISHED"
        res = $sv.lot_opelocate(l, oper)
        ret &= (res == 0)
      end
    }
    return ret
  end
  
  def check_combine(lot)
    # check mds for SortJob and combine
    $log.info "-- check_combine #{lot.inspect}"
    testresult = false
    60.times {
      res = $sjcmds.tsi_lot(:lot=>lot)
      if res and res[0].combine_srid > 0
        testresult = !!mycombine(res[0].combine_srid)
        break
      end
      sleep 10
    }
    $log.info("  ok: #{testresult}")
    return testresult
  end
  
  def myseparate(srid)
    $log.info "  -- myseparate #{srid.inspect}"
    ret = true
    sts = $sjcmds.sjc_tasks(:srid=>srid)
    return nil unless sts and sts.size > 0
    sts.each {|st|
      $log.debug "     taskid #{st.inspect}"
      wpos = $sjcmds.sjc_wafers(:stid=>st.stid)
      li = $sv.lot_info(st.srclot, :wafers=>true)
      slots = li.wafers.collect {|w| wpos.collect {|p| p.slot if p.wafer == w.wafer}.compact}.flatten
      $log.debug "       slots #{slots.inspect}"
      if slots.size > 0
        synchronize do
          newcarriers = $sv.carrier_list(:empty=>true, :status=>"AVAILABLE", :category=>"BEOL", :carrier=>"3%")
          $log.debug "      new carriers: #{newcarriers.inspect}"
          ($log.warn "myseparate: no empty 3% carriers available"; return false) if newcarriers.size == 0
          res = $sv.wafer_sort_req(st.srclot, newcarriers[rand(newcarriers.size)], :slots=>slots)
          ret &= (res == 0)
        end
      end
    }
    $sjcdel.delete(sts[0].srid, :user=>"TMPSPCINC") if ret
    $log.info("    ok: #{ret}")
    return ret
  end
  
  def mycombine(srid)
    $log.info "  -- mycombine #{srid.inspect}"
    ret = true
    sts = $sjcmds.sjc_tasks(:srid=>srid)
    return nil unless sts and sts.size > 0
    sts.each {|st|
      wpos = $sjcmds.sjc_wafers(:stid=>st.stid)
      li = $sv.lot_info(st.srclot, :wafers=>true)
      slots = li.wafers.collect {|w| wpos.collect {|p| p.slot if p.wafer == w.wafer}.compact}.flatten
      $log.debug "        slots #{slots.inspect}"
      if slots.size != 0
        res = $sv.wafer_sort_req(st.srclot, st.tgtcarrier, :slots=>slots)
        ret &= (res == 0)
      end
    }
    $sjcdel.delete(sts[0].srid, :user=>"TMPSPCINC") if ret
    $log.info("    ok: #{ret}")
    return ret
  end
  
  def clear_sr_lot(lot)
    lots = $sv.lot_family(lot)
    lots.each {|lot|
      sts = $sjcmds.sjc_tasks(:srclot=>lot)
      $sjcdel.delete(sts[0].srid, :user=>"TMPSPCINC") if sts and sts.size > 0
    }
    # verify
    sleep 10
    ret = true
    lots.each {|lot|
      sts = $sjcmds.sjc_tasks(:srclot=>lot)
      ret &= (sts == [])
    }
    return ret
  end
  
  def clear_sr_carrier(carrier)
    sts = $sjcmds.sjc_tasks(:srccarrier=>carrier) + $sjcmds.sjc_tasks(:tgtcarrier=>carrier)
    sts.each {|st| $sjcdel.delete(st.srid, :user=>"TMPSPCINC")}
    # verify
    sleep 10
    sts = $sjcmds.sjc_tasks(:srccarrier=>carrier) + $sjcmds.sjc_tasks(:tgtcarrier=>carrier)
    return (sts == [])
  end
  
  def check_wafermap(lot)
    wmap = $sv.lot_info(lot, :wafers=>true).wafers
    res = wmap.collect{|w| w.slot.to_i % $sv.lot_family(lot).size}.uniq
    return res.size == 1
  end

  def get_child_lot(lot)
    _clot = $sv.lot_family(lot) - [lot]
    return _clot[0]
  end  
  
  def self.result
    return @@result
  end
  
end
