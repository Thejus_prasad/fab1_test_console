=begin 
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.
    
Author: Steffen Steidten, 2013-07-04

=end

require "RubyTestCase"
require 'rtdclient'
require 'rtdqarules'

# Test the jCAP RtdServicesMds job and RTD Java User Exits

class Test_jCAP_RtdServicesMds < RubyTestCase
  Description = "Test the jCAP RtdServicesMds job and RTD Java User Exits"

  
  def test00_setup
    $setup_ok = false
    $rtdclient = RTD::Client.new($env) unless $rtdclient and $rtdclient.env == $rtdclient
    $mds = RTD::MDS.new($env) unless $mds and $mds.env == $env
    $setup_ok = true
  end
  
  def test01_getLotManager
    # no definitive way to check, except from log files 
    res = $rtdclient.juetest_getLotManager
    assert false, "no automated test, check the log file for evidence"
  end
  
  def test02_writeEqpNextTimeRtd
    res = $rtdclient.juetest_writeEqpNextTimeRtd
    assert_equal "QA_JUETest_writeEqpNextTimeRtd", res.rule, "wrong RTD rule called, setup error"
    assert_equal "OK", res.rows.last.last
  end
  
  def test03_writeEqpStandbySubstates
    res = $rtdclient.juetest_writeEqpStandbySubstates
    assert_equal "QA_JUETest_writeEqpStandbySubstates", res.rule, "wrong RTD rule called, setup error"
    assert_equal "OK", res.rows.last.last
  end
  
  def test04_writeEqpWipAuto3
    res = $rtdclient.juetest_writeEqpWipAuto3
    assert_equal "QA_JUETest_writeEqpWipAuto3", res.rule, "wrong RTD rule called, setup error"
    assert_equal "OK", res.rows.last.last
  end
  
  def test05_writeFoupTransferRequest
    # pick a carrier
    carrier = nil
    carriers = $sv.carrier_list(:empty=>true)
    carriers.each {|c|
      res = $mds.t_trans_job(:carrier=>c)
      (carrier = c; break) if res == []
    }
    assert carrier, "no suitable carrier found"
    $log.info "using carrier #{carrier}"
    # write test rule file
    qarule = RTD::QARule.new($env, :rule=>:juetest_writeFoupTransferRequest)
    qarule.write_file(["#ISS", [carrier, "", "STK", "DummyStocker"]])
    # call test rule triggering the JUE
    res = $rtdclient.juetest_writeFoupTransferRequest
    assert_equal "QA_JUETest_writeFoupTransferRequest", res.rule, "wrong RTD rule called, setup error"
    # verify result of the jCAP job's action
    assert wait_for(:timeout=>300) {$mds.t_trans_job(:carrier=>carrier).size == 1}
    res = $mds.t_trans_job(:carrier=>carrier).first
    assert_equal carrier, res.carrier
    assert (Time.now - res.last_update).abs < 10
  end
  
end
