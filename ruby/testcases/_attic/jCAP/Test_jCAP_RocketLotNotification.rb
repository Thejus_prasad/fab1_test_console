=begin 
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.
    
Author: Conny Stenzel, 2011-01-13

History:
  2015-05-19 adarwais,  cleanup
=end

require 'SiViewTestCase'
require 'derdack'

# Test the Rocket Lot Notification jCAP job
class Test_jCAP_RocketLotNotification < SiViewTestCase
  @@jcap_interval = 610
  @@latency_interval = 610
  # 
  @@hold_reason1 = 'ENG'
  @@hold_reason2 = 'AEHL'
  @@treshold = 4

  
  def self.after_all_passed
    @@testlots.each {|lot| $sv.delete_lot_family(lot)}
  end
    
  def test00_setup
    $setup_ok = false
    #
    $derdack = Derdack::EventDB.new($env)
    assert $derdack.db.connected?, "no connection to Derdack DB"
    assert @@testlots = $svtest.new_lots(2), "error creating test lots"
    @@rocket_lot, @@nonrocket_lot = @@testlots
    #
    $sv.schdl_change(@@rocket_lot, priority: 0)
    assert_equal '0', $sv.lot_info(@@rocket_lot).priority
    assert_equal 0, $sv.lot_hold(@@rocket_lot, @@hold_reason1)
    assert_equal 0, $sv.lot_hold(@@rocket_lot, @@hold_reason2)
    #
    $sv.schdl_change(@@nonrocket_lot, priority: @@treshold)
    assert_equal @@treshold.to_s, $sv.lot_info(@@nonrocket_lot).priority
    #
    assert_equal 0, $sv.lot_hold(@@nonrocket_lot, @@hold_reason1)
    #
    $setup_ok = true
  end
  
  def test11_rocketalerts_no_notification
    @@notification_time = Time.now - 10
    $log.info "sleeping #{@@latency_interval}s (latency interval for the hold and jCAP job interval"; sleep @@latency_interval
    evs = $derdack.get_events(@@notification_time, nil, 'LotID'=>@@rocket_lot)
    assert_equal [], evs, "notifications sent too early"
  end
  
  def test12_nonrocketalerts_no_notification
    evs = $derdack.get_events(@@notification_time, nil, "LotID"=>@@nonrocket_lot)    
    assert_equal [], evs,  "notification sent for non-rocket lot"
  end
  
  def test13_rocketalerts_notification
    $log.info "sleeping #{@@jcap_interval}s to wait for the jCAP job"; sleep @@jcap_interval
    evs = $derdack.get_events(@@notification_time, nil, 'LotID'=>@@rocket_lot)
    assert_equal 2, evs.size, "notifications not sent"
  end
  
  def test14_nonrocketalerts_no_notification
    evs = $derdack.get_events(@@notification_time, nil, 'LotID'=>@@nonrocket_lot)
    assert_equal [], evs,  "notification sent for non-rocket lot"
  end

  # release the hold of the nonrocketlot and check if there still notifications for the rocketlot
  def test15_rocketalerts_nonrocket_lot_hold_release_no_notification
    res = $sv.lot_hold_release(@@nonrocket_lot, nil)
    $setup_ok = (res == 0)
    assert $setup_ok
    @@notification_time = Time.now - 10
    $log.info "sleeping #{@@jcap_interval}s to wait for the next jCAP job"; sleep @@jcap_interval
    evs = $derdack.get_events(@@notification_time, nil, 'LotID'=>@@rocket_lot)
    assert_equal [], evs,  "notification sent again for rocket lot"
  end
      
  def test20_rocketalerts_rocket_lot_hold_release_no_notification
    # release rocket holds
    res = $sv.lot_hold_release(@@rocket_lot, nil)
    $setup_ok = (res == 0)
    assert $setup_ok
    @@notification_time = Time.now - 10
    # wait for jcap job
    $log.info "sleeping #{@@jcap_interval}s to wait for the next jCAP job"; sleep @@jcap_interval
    # set_rocket lot on hold
    $setup_ok = false
    assert_equal 0, $sv.lot_hold(@@rocket_lot, @@hold_reason1)
    @@notification_time = Time.now
    $log.info "sleeping #{@@latency_interval}s to pass the latency interval for the hold and to wait for the jCAP job"; sleep @@latency_interval
    $setup_ok = true
    evs = $derdack.get_events(@@notification_time, nil, 'LotID'=>@@rocket_lot)
    assert_equal [], evs,  "notification for rocket lot sent too early"
  end
  
  def test21_rocketalerts_notification
    $log.info "sleeping #{@@jcap_interval}s to wait for the jCAP job"; sleep @@jcap_interval
    evs = $derdack.get_events(@@notification_time, nil, 'LotID'=>@@rocket_lot)
    $log.info "notifications received: #{evs.inspect}"    
    assert_equal 1, evs.size,  "error, notification for rocket lot not sent"
  end
    
end
