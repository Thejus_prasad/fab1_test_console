=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author:   ssteidte, 2011-10-28
Version:  16.08

History:
  2016-09-05 sfrieske, added tests for MSR1022670 (LotNote, multiple cancel)
  2018-03-06 sfrieske, replaced user_parameter_delete_verify by user_parameter_delete
  2019-01-23 sfrieske, removed unused require derdack
=end

require 'SiViewTestCase'
require 'jcap/erpasnreceiver'
require 'jcap/turnkeybtf'


# Test the jCAP ErpAsnReceiver job.
#
# Docs: https://drive.google.com/drive/folders/0B2KH9XXBXcXGQzZueW5PYnVpekU
# WSDL: http://gfsvnp02:8081/nexus/service/local/repositories/releases/content/com/globalfoundries/jcapcorp/faberpitf/faberpitf-asn-wsdl/16.4.1/faberpitf-asn-wsdl-16.4.1-wsdl.wsdl
class Test_jCAP_ErpAsnReceiver < SiViewTestCase
  @@product = 'BAFFINS1MA.A0'
  @@route = 'FF-BAFFIN.01'

  @@src_env = 'f8stag'
  @@backup_env = 'itdc'
  @@customer = 'amd'
  @@erpitem = 'BAFFINS1MA-M01-JA0'
  @@order = '6010000548:01:01'
  @@scriptparams = {'CountryOfOrigin'=>'KR', 'ERPItem'=>@@erpitem, 'TurnkeyType'=>'J',
    'TurnkeySubconIDFab'=>'S1', 'TurnkeySubconIDBump'=>'GG', 'TurnkeySubconIDSort'=>'GG'
  }
  @@note_title = 'Created by ErpAsnReceiver'  # default value
  @@testlots = []


  def self.startup
    super(nosiview: true)
  end

  def self.after_all_passed
    @@testlots.each {|lot|
      @@sv_src.backup_delete_lot_family(lot, @@sv_back)
      @@sv_src.delete_lot_family(lot)
      @@sv_src.carrier_list(carrier: lot.split('.').first).each {|c| @@sv_src.durable_delete('Cassette', c)}
    }
  end


  def test00_setup
    $setup_ok = false
    #
    @@erpasn = ERPAsnReceiver::AsnNotification.new($env, customer: @@customer)
    assert_match /\d\.\d/, @@erpasn.get_version, 'no MQ connectivity'
    @@sv_src = SiView::MM.new(@@src_env)
    @@svtest_src = SiView::Test.new(@@src_env, sv: @@sv_src, product: @@product, route: @@route, customer: @@customer)
    @@sv_back = SiView::MM.new(@@backup_env)
    @@dbbtf = Turnkey::TestDB.new('itdc')
    #
    $setup_ok = true
  end

  def test11_happy
    $setup_ok = false
    #
    # send notification
    assert lot = @@sv_src.generate_new_lotids(@@customer).first
    @@testlots << lot
    shipno = "QA00#{unique_string}"
    assert @@erpasn.notification(lot, @@product, shipno: shipno)
    #
    # verify on src site lot is sent and script parameters are set
    assert li = @@sv_src.lot_info(lot, wafers: true, backup: true)
    assert li.backup.backup_proc, "wrong backup state"
    assert li.backup.current_loc, "wrong backup state"
    assert li.backup.transfering, "wrong backup state"
    assert_equal @@product, li.product
    @@scriptparams.each_pair {|name, value|
      $log.info "verifying script parameter #{name.inspect}: #{value.inspect}"
      assert_equal value, @@sv_src.user_parameter('Lot', lot, name: name).value
    }
    # verify lot note
    assert note = @@sv_src.lot_notes(lot).first
    assert_equal @@note_title, note.title
    assert_equal "ShipmentNumber=#{shipno}, ERPItem=#{@@erpitem}", note.desc
    # verify aliases (cannot be done at src site because slots are 0)
    li.wafers.each {|w| assert_match /^\d\d$/, w.alias}
    # # verify on backup site lot is receivable
    # assert li = @@sv_back.backup_send_requests(lot: lot).first, "lot is not on the send request list"
    # assert li.backup.backup_proc, "wrong backup state"
    # assert li.backup.current_loc, "wrong backup state"
    # assert li.backup.transfering, "wrong backup state"
    #
    # receive lot and set lot script parameters as FabGUI Receiving GUI would do
    assert_equal 0, @@sv_back.backupop_lot_send_receive(lot, @@sv_src)
    assert_equal 0, @@sv_back.user_parameter_change('Lot', lot, @@scriptparams)
    # move to LOTSHIPDESIG and set DieCountGood in SiView and TestDB
    assert_equal 0, @@sv_back.lot_opelocate(lot, nil, op: 'LOTSHIPDESIG.01')
    assert @@dbbtf.set_lot_wafer_bins(lot, op: 'LOTSHIPDESIG.01')
    assert_equal 0, @@sv_back.user_parameter_change('Lot', lot, 'ERPSalesOrder', @@order)
    #
    $setup_ok = true
  end

  def testman12_shipping
    assert false, "The test passed so far and the lot is ready for FabGUI shipping!"
  end

  def test13_no_timeout
    # send notification
    assert lots = @@sv_src.generate_new_lotids(@@customer, n: 10)
    @@testlots += lots
    $log.info "using lots #{lots}"
    tstart = Time.now
    @@erpasn.notification(lots, @@product, erpitem: '', timeout: 1000)
    assert Time.now > tstart + 300, "wrong test setup, not enough lots?"
    # no timeout msg
    assert_equal 'ADVANCE_SHIPPING_NOTICE_SERVICE_POST_PROCESSING_ERROR', @@erpasn.mq.service_response[:receiveAdvanceShipmentNotificationResponse][:return][:code][nil]
    # verify lots have been created
    lots.each {|lot| assert @@sv_src.lot_exists?(lot), "error creating lot #{lot}"}
  end

  def test21_cancel
    # send notification and verify lot has been created
    assert lot = @@sv_src.generate_new_lotids(@@customer).first
    @@testlots << lot
    $log.info "using lot #{lot.inspect}"
    assert @@erpasn.notification(lot, @@product)
    assert @@sv_src.lot_exists?(lot)
    #
    # send cancel notification and verifylot has been deleted
    assert @@erpasn.notification(lot, @@product, cancel: true)
    refute @@sv_src.lot_exists?(lot)
  end

  def test22_cancel_wrong_lot
    # create a lot like ErpAsnReceiver but without lot note
    assert lot = @@svtest_src.new_lot(erpitem: @@erpitem)
    #
    # send cancel notification and verify error msg
    @@erpasn.notification(lot, @@product, cancel: true)
    assert_equal 'ADVANCE_SHIPPING_NOTICE_SERVICE_PROCESSING_ERROR', @@erpasn.mq.service_response[:receiveAdvanceShipmentNotificationResponse][:return][:code][nil]
    msg = @@erpasn.service_response[:receiveAdvanceShipmentNotificationResponse][:return][:details][:msg]
    assert msg.include?('Lot Note could not be found'), "wrong msg"
  end

  def test23_multilot_cancel
    # create 4 lot ids
    assert lots = @@sv_src.generate_new_lotids(@@customer, n: 4)
    @@testlots += lots
    # 0: no lot
    # 1: good ASN lot
    assert @@erpasn.notification(lots[1], @@product)
    # 2: no ASN lot
    assert @@svtest_src.new_lot(lot: lots[2], erpitem: @@erpitem)
    # 3: good ASN lot again
    assert @@erpasn.notification(lots[3], @@product)
    #
    # send cancel notification and verify only ASN lots have been deleted
    refute @@erpasn.notification(lots, @@product, cancel: true)
    refute @@sv_src.lot_exists?(lots[1])
    assert @@sv_src.lot_exists?(lots[2])
    refute @@sv_src.lot_exists?(lots[3])
    # verify response
    msgs = @@erpasn.mq.service_response[:receiveAdvanceShipmentNotificationResponse][:return][:details]
    assert msgs[0][:msg].include?("#{lots[0]} not found")
    assert msgs[1][:msg].nil?
    assert msgs[2][:msg].include?("Lot Note could not be found")
    assert msgs[3][:msg].nil?
  end

  def test31_rollback
    # send notification with a wrong carrier
    assert lots = @@sv_src.generate_new_lotids(@@customer, n: 2)
    tstart = Time.now
    @@erpasn.notification(lots, @@product, carriers: ['', 'XXX.YYY'])
    assert_equal 'ADVANCE_SHIPPING_NOTICE_SERVICE_PROCESSING_ERROR', @@erpasn.mq.service_response[:receiveAdvanceShipmentNotificationResponse][:return][:code][nil]
    # all lots have been rolled back
    lots.each {|lot| refute @@sv_src.lot_exists?(lot)}
  end

  def test32_validation_lot
    # send notification with a duplicate lot
    assert lot = @@sv_src.generate_new_lotids(@@customer).first
    @@erpasn.notification([lot, lot], @@product)
    assert_equal 'ADVANCE_SHIPPING_NOTICE_SERVICE_VALIDATION_ERROR', @@erpasn.mq.service_response[:receiveAdvanceShipmentNotificationResponse][:return][:code][nil]
  end

  def test33_validation_wafers
    # send notification with a duplicate wafer
    assert lot = @@sv_src.generate_new_lotids(@@customer).first
    @@erpasn.notification(lot, @@product, wafers: Array.new(2, unique_string('WW')))
    assert_equal 'ADVANCE_SHIPPING_NOTICE_SERVICE_VALIDATION_ERROR', @@erpasn.mq.service_response[:receiveAdvanceShipmentNotificationResponse][:return][:code][nil]
  end

end
