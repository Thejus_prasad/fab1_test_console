=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author:   ssteidte, 2013-05-06

Version:  20.12

History:
  2016-05-09 sfrieske, minor cleanup
  2016-12-12 sfrieske, minor cleanup
  2018-03-12 sfrieske, minor cleanup
  2019-06-03 sfrieske, cleanup
  2021-02-17 sfrieske, fixed lot note checks because STBMfgAttributes now adds a note

Notes:
  Test the jCAP OneERPShipment job for BackupOperation Fab8<->Fab1, MSR 708379.
  ## still valid ?  The reply queue must be changed for testing so that ERP doesn't receive the reply messages!
=end

require 'SiViewTestCase'
require 'jcap/erpshipment'


class JCAP_OneERPShipment_Backup < SiViewTestCase
  @@sv_defaults = {product: 'UT-PRODUCT-FAB8.01', route: 'UTRT101.01', carriers: '%'}
  @@psend = {backup_opNo: '1000.200', backup_bank: 'W-BACKUPOPER'}
  @@psend_product = {backup_opNo: '1000.300', backup_bank: 'W-BACKUPOPER'}
  @@backup_bank_udata = 'BACKUPFAB8'
  @@wrong_bank = 'W-BACKUPOPER2'

  @@backup_env = 'f8stag'  # TODO: LET
  @@sv_defaults_bak = {product: 'UT-PRODUCT-FAB1.01', route: 'UTRT101.01', carriers: '%', stocker: 'UTSTO110'}
  @@psend_bak = {backup_opNo: '1000.200', backup_bank: 'O-BACKUPOPER'}

  @@testlots = []
  @@testlots_bop = []
  @@job_interval = 400  # includes ERP communication


  def self.after_all_passed
    @@testlots_bop.each {|lot| @@svbak.backup_delete_lot_family(lot, @@sv, @@psend_bak)}
    @@testlots.each {|lot| @@sv.backup_delete_lot_family(lot, @@svbak, @@psend)}
  end


  def test00_setup
    $setup_ok = false
    #
    udata = @@sv.user_data(:bank, @@psend[:backup_bank])['BankReportingType']
    assert udata.include?(@@backup_bank_udata), 'wrong bank UDATA BankReportingType'
    #
    assert @@erps = ERPShipment::PickShip.new($env, sv: @@sv)
    if $env == 'f8stag'
      @@erps[:banks] = {
        end_std: 'O-ENDFG', end_nonstd: 'O-SHIP-NS', end_nonstd_return: 'O-OUTFAB8', ship: 'O-SHIP'
      }
    end
    #
    assert @@erps.get_version, 'no MQ connectivity'
    #
    assert @@svtest_bak = SiView::Test.new(@@backup_env, @@sv_defaults_bak)
    @@svbak = @@svtest_bak.sv
    #
    $setup_ok = true
  end

  def test51_send_lot_product
    assert lot = @@svtest.new_lot, 'error creating test lot'
    @@testlots << lot
    # prepare lot for backup (nonprobankin, remove carrier)
    assert @@sv.prepare_for_backup(lot, @@psend_product)
    #
    # at this point FabGui would send a lotComplete msg to ERP
    # send pick and ship msg on behalf of ERP to OneERPShipment
    assert @@erps.pick_release(lot, nonstd: true), 'error picking lot'
    sleep 10
    assert @@erps.ship_complete(lot, nonstd: true), 'error shipping lot'
    # verify lot note
    note = @@sv.lot_notes(lot).first
    assert_equal 'Sent to Backup Site', note.title
    assert_equal 'SHIPPED', note.desc
    assert (Time.now - @@sv.siview_time(note.timestamp)).abs < 300, "wrong timestamp: #{note.inspect}"
    # check lot status
    li = @@sv.lot_info(lot, backup: true)
    assert li.backup.backup_proc, "wrong lot backup proc: #{li.pretty_inspect}"
    assert li.backup.current_loc, "wrong lot backup loc: #{li.pretty_inspect}"
    assert li.backup.transfering, "wrong lot backup transfer: #{li.pretty_inspect}"
    #
    # verify lot can be received at the backup site (normally done by FabGUI at the backup site)
    assert_equal 0, @@svbak.backupop_lot_send_receive(lot, @@sv)
  end

  def test52_lot_nonproduct
    assert lot = @@svtest.new_lot, 'error creating test lot'
    @@testlots << lot
    # prepare lot for backup (nonprobankin, remove carrier)
    assert @@sv.prepare_for_backup(lot, @@psend)
    #
    # at this point FabGui would send a lotComplete msg to ERP
    # send pick and ship msg on behalf of ERP to OneERPShipment
    assert @@erps.pick_release(lot, nonstd: true), 'error picking lot'
    sleep 10
    refute @@erps.ship_complete(lot, nonstd: true), 'wrong lot shipping'
    # verify response
    assert @@erps.mq.response.include?('no backup channel found'), 'wrong response msg'
    # verify lot note
    note = @@sv.lot_notes(lot).first
    assert_equal 'ONEERPSHIPMENT.ShipComplete.Error', note.title
    assert note.desc.include?("no backup channel found for #{lot}")
    # check lot status
    li = @@sv.lot_info(lot, backup: true)
    refute li.backup.backup_proc, "wrong lot backup proc: #{li.pretty_inspect}"
    assert li.backup.current_loc, "wrong lot backup loc: #{li.pretty_inspect}"
    refute li.backup.transfering, "wrong lot backup transfer: #{li.pretty_inspect}"
  end

  def test53_lot_carrier
    assert lot = @@svtest.new_lot, 'error creating test lot'
    @@testlots << lot
    # prepare lot for backup (locate, nonprobankin) but do not remove carrier
    #assert @@sv.prepare_for_backup(lot, @@psend_product)
    assert_equal 0, @@sv.lot_opelocate(lot, @@psend_product[:backup_opNo])
    assert_equal 0, @@sv.lot_nonprobankin(lot, @@psend_product[:backup_bank])
    #
    # at this point FabGui would send a lotComplete msg to ERP
    # send pick and ship msg on behalf of ERP to OneERPShipment
    assert @@erps.pick_release(lot, nonstd: true), 'error picking lot'
    sleep 10
    refute @@erps.ship_complete(lot, nonstd: true), 'error shipping lot'



    # verify lot note
    note = @@sv.lot_notes(lot).first
    assert_equal 'Sent to Backup Site', note.title
    assert_equal 'SHIPPED', note.desc
    assert (Time.now - @@sv.siview_time(note.timestamp)).abs < 300, "wrong timestamp: #{note.inspect}"
    # check lot status
    li = @@sv.lot_info(lot, backup: true)
    assert li.backup.backup_proc, "wrong lot backup proc: #{li.pretty_inspect}"
    assert li.backup.current_loc, "wrong lot backup loc: #{li.pretty_inspect}"
    assert li.backup.transfering, "wrong lot backup transfer: #{li.pretty_inspect}"
    #
    # verify lot can be received at the backup site (normally done by FabGUI at the backup site)
    assert_equal 0, @@svbak.backupop_lot_send_receive(lot, @@sv)
  end


  def test61_return_lot
    assert lot = @@svtest_bak.new_lot, 'error creating test lot'
    @@testlots_bop << lot
    # send lot to backup site and prepare for return
    assert @@svbak.backup_prepare_send_receive(lot, @@sv, @@psend_bak)
    assert @@sv.prepare_for_return(lot, return_bank: @@psend[:backup_bank])
    # at this point FabGui would send a lotComplete msg to ERP
    # send pick and ship msg on behalf of ERP to OneERPShipment
    assert @@erps.pick_release(lot, nonstd: true), 'error picking lot'
    sleep 10
    assert @@erps.ship_complete(lot, nonstd: true), 'error shipping lot'
    # verify lot note
    note = @@sv.lot_notes(lot).first
    assert_equal 'Return to Source Site', note.title
    assert_equal 'SHIPPED', note.desc
    assert (Time.now - @@sv.siview_time(note.timestamp)).abs < 300, "wrong timestamp: #{note.inspect}"
    # check lot status
    li = @@svbak.lot_info(lot, backup: true)
    assert li.backup.backup_proc, "wrong lot backup proc: #{li.pretty_inspect}"
    assert !li.backup.current_loc, "wrong lot backup loc: #{li.pretty_inspect}"
    assert li.backup.transfering, "wrong lot backup transfer: #{li.pretty_inspect}"
    assert_equal 'Return', li.backup.dest.state, "wrong backup dest: #{li.pretty_inspect}"
    #
    # verify lot can be received at the source site (normally done by FabGUI at the source site)
    assert_equal 0, @@svbak.backupop_lot_return_receive(lot, @@sv)
  end

  def test62_return_split_lot
    assert lot = @@svtest_bak.new_lot, 'error creating test lot'
    @@testlots_bop << lot
    # sent the lot to backup
    assert @@svbak.backup_prepare_send_receive(lot, @@sv, @@psend_bak)
    # split the lot in backup
    assert child = @@sv.lot_split(lot, 5), 'error splitting lot'
    [lot, child].each {|l|
      # prepare lot for return (nonprobankin, remove carrier)
      assert @@sv.prepare_for_return(l, return_bank: @@psend[:backup_bank])
      # at this point FabGui would send a lotComplete msg to ERP
      # send pick msg on behalf of ERP to OneERPShipment
      assert @@erps.pick_release(l, nonstd: true), "error picking lot #{l}"
    }
    # Shipment in any order works with SiView C6.17
    # at this point FabGui would send a lotComplete msg to ERP
    # send ship msg on behalf of ERP to OneERPShipment
    sleep 10
    [child, lot].each {|l|
      assert @@erps.ship_complete(l, nonstd: true), "error shipping lot #{l}"
      # verify lot note
      note = @@sv.lot_notes(l).first
      assert_equal 'Return to Source Site', note.title
      assert_equal 'SHIPPED', note.desc
      assert (Time.now - @@sv.siview_time(note.timestamp)).abs < 300, "wrong timestamp: #{note.inspect}"
      # check lot status, source site doesn't know about the child yet
      li = @@sv.lot_info(l, backup: true)
      assert li.backup.backup_proc, "wrong lot backup proc: #{li.pretty_inspect}"
      assert li.backup.current_loc, "wrong lot backup loc: #{li.pretty_inspect}"
      assert li.backup.transfering, "wrong lot backup transfer: #{li.pretty_inspect}"
      assert_equal 'Return', li.backup.src.state, "wrong backup dest: #{li.pretty_inspect}"
      assert (Time.now - @@sv.siview_time(note.timestamp)).abs < 300, "wrong timestamp: #{note.inspect}"
      # verify lot can be received at the source site (normally done by FabGUI at the source site)
      assert_equal 0, @@svbak.backupop_lot_return_receive(l, @@sv)
    }
  end

  def test63_complete_lot
    assert lot = @@svtest.new_lot, 'error creating test lot'
    @@testlots << lot
    # ensure lot is on a backup route
    assert @@sv.backup_prepare_send_receive(lot, @@svbak, @@psend)
    # complete lot at backup site
    assert_equal 0, @@sv.backup_complete(lot, @@svbak, bak_carrier: '9%', carrier_category: 'TKEY')
    # complete the lot at source site
    @@sv.lot_opelocate(lot, :last)
    @@sv.lot_bankin(lot) unless @@sv.lot_info(lot).status == 'COMPLETED'
    assert_equal 0, @@sv.lot_bank_move(lot, @@erps.banks[:end_std])
    # at this point ASMView would send a lotComplete msg to both ERP systems
    # send pick and ship msg on behalf of ERP to OneERPShipment (source only)
    assert @@erps.pick_release(lot), 'error picking lot'
    sleep 10
    assert @@erps.ship_complete(lot), 'error shipping lot'
    # verify lot status
    assert_equal 'SHIPPED', @@sv.lot_info(lot).status, 'lot not shipped in source fab'
    # other site did not get the pick/ship events, so lot will not be shipped
    assert_equal 'COMPLETED', @@svbak.lot_info(lot).status
  end

end
