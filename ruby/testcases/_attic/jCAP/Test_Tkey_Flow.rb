=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: Steffen Steidten, 2011-04-28

Version: 17.02

History:
  2016-08-29 sfrieske,  small maintenance due to changes in erpmes;
                        noticed many broken tests because setup and scenario have changed
=end

require 'SiViewTestCase'
require 'jcap/turnkey'
require 'derdack'

# Test the Turnkey jCAP job
class Test_Tkey_Flow < SiViewTestCase
  @@sv_defaults = {product: 'SHELBY21BTF.QA', route: 'C02-TKEY-ACTIVE.01', carrier_category: nil}
  @@erpitem = '*SHELBY21AE-M00-JB4'
  @@tkbump = 'CT'
  @@tksort = 'ST'

  @@sublottype_e = 'ET'
  @@sublottype_rd = 'RD'
  @@special_customer_bumpdispo = 'qgt'  # attention, currently qgt and gf are special customers
  @@special_customer_typeufilter = 'samsung'


  def self.after_all_passed
    @@testlots.each {|lot| $sv.delete_lot_family(lot)}
  end


  def test00_setup
    $setup_ok = false
    #
    @@carriers = (@@tkbump == 'GG') ? '3%' : '9%'
    assert $sv.carrier_list(empty: true, status: 'AVAILABLE', carrier: @@carriers).size > 8, "not enough empty carriers"
    assert @@testlots = $svtest.new_lots(9, erpitem: @@erpitem, carrier: @@carriers)
    @@lots = Hash[[:tktype_j, :tktype_jn, :tktype_l, :tktype_r, :tktype_s, :tktype_u, :sublot_e, :sublot_rd, :sublot_rds].zip(@@testlots)]
    #
    @@derdack = Derdack::EventDB.new($env)
    @@tkbranch = Turnkey::BranchStart.new($env, sv: $sv, route: $svtest.route)
    # tktype U
    assert @@tkbranch.lot_set_tkinfo(@@lots[:tktype_u], SiView::MM::LotTkInfo.new('U'))
    # tktype L
    assert @@tkbranch.lot_set_tkinfo(@@lots[:tktype_l], SiView::MM::LotTkInfo.new('L', @@tkbump, nil))
    # tktype S
    assert @@tkbranch.lot_set_tkinfo(@@lots[:tktype_s], SiView::MM::LotTkInfo.new('S', nil, @@tksort))
    # tktype R
    assert @@tkbranch.lot_set_tkinfo(@@lots[:tktype_r], SiView::MM::LotTkInfo.new('R', @@tkbump, @@tksort, @@tkbump))
    # tktype J
    tkinfo_j = SiView::MM::LotTkInfo.new('J', @@tkbump, @@tksort)
    assert @@tkbranch.lot_set_tkinfo(@@lots[:tktype_j], tkinfo_j)
    #  sublottype E*
    assert @@tkbranch.lot_set_tkinfo(@@lots[:sublot_e], tkinfo_j)
    assert_equal 0, $sv.sublottype_change(@@lots[:sublot_e], @@sublottype_e)
    #  sublottype RD
    assert @@tkbranch.lot_set_tkinfo(@@lots[:sublot_rd], tkinfo_j)
    assert_equal 0, $sv.sublottype_change(@@lots[:sublot_rd], @@sublottype_rd)
    #  sublottype RD, for special customers
    assert @@tkbranch.lot_set_tkinfo(@@lots[:sublot_rds], tkinfo_j)
    $sv.lot_mfgorder_change(@@lots[:sublot_rds], sublottype: @@sublottype_rd, customer: @@special_customer_bumpdispo)
    assert_equal @@sublottype_rd, $sv.lot_info(@@lots[:sublot_rds]).sublottype
    assert_equal @@special_customer_bumpdispo, $sv.lot_info(@@lots[:sublot_rds]).customer
    # tktype J, no subcons
    assert @@tkbranch.lot_set_tkinfo(@@lots[:tktype_jn], SiView::MM::LotTkInfo.new('J', nil, nil))
    #
    ##@@testlots.each {|lot| assert_equal 0, $sv.user_parameter_change('Lot', lot, 'ERPItem', @@erpitem)}
    #
    $setup_ok = true
    $log.info "testing with subcons #{@@tkbump}, #{@@tksort}"
  end

  def test10_move_tkbumpdispo
    $setup_ok = false
    #
    @@lots.values.each {|lot| assert_equal 0, $sv.lot_cleanup(lot, op: @@tkbranch.ops[:tkbumpdispo])}
    @@tstart = Time.now
    #
    $setup_ok = true
  end

  # L, J: subcon GG: move to :erpstatechgbump (tmp: BumpPD), subcon != GG: move to TK-OQA, carrier independent
  def test12j_tkbumpdispo
    lot = @@lots[:tktype_j]
    assert @@tkbranch.wait_lot_move(lot, :tkbumpdispo, tstart: @@tstart), "unexpected final operation"
    if @@tkbump == 'GG'
      # previous op is :erpstatechgbump
      assert_equal @@tkbranch.ops[:trc4b], $sv.lot_info(lot).op
    else
      # previous op is :tkbumpdispo
      lop = $sv._guess_op_route(lot, -1)[0]
      assert_equal @@tkbranch.ops[:tkbumpdispo], lop.op
    end
  end

  # special treatment for lots with missing subcon
  def test12jn_tkbumpdispo_nosubcons
    lot = @@lots[:tktype_jn]
    assert !@@tkbranch.wait_lot_move(lot, :tkbumpdispo, tstart: @@tstart), "unexpected final operation"
    assert @@tkbranch.wait_lot_hold(lot, 'O-TK', tstart: @@tstart), "lot #{lot} status must be ONHOLD"
    assert verify_derdack_msg(lot, @@tstart), "error in Derdack notification"
  end

  # L, J: subcon GG: move to :erpstatechgbump (tmp: BumpPD), subcon != GG: move to TK-OQA, carrier independent
  def test12l_tkbumpdispo
    lot = @@lots[:tktype_l]
    assert @@tkbranch.wait_lot_move(lot, :tkbumpdispo, tstart: @@tstart), "lot #{lot} has not moved"
    if @@tkbump == 'GG'
      # previous op is :erpstatechgbump
      assert_equal @@tkbranch.ops[:trc4b], $sv.lot_info(lot).op
    else
      # previous op is :tkbumpdispo
      lop = $sv._guess_op_route(lot, -1)[0]
      assert_equal @@tkbranch.ops[:tkbumpdispo], lop.op
    end
  end

  def test12r_tkbumpdispo
    # R: subcon GG: move to :erpstatechgbump (tmp: BumpPD), subcon != GG: move to TK-OQA, carrier independent
    lot = @@lots[:tktype_r]
    assert @@tkbranch.wait_lot_move(lot, :tkbumpdispo, tstart: @@tstart), "unexpected final operation"
    if @@tkbump == 'GG'
      # previous op is :erpstatechgbump
      assert_equal @@tkbranch.ops[:trc4b], $sv.lot_info(lot).op
    else
      # previous op is :tkbumpdispo
      lop = $sv._guess_op_route(lot, -1)[0]
      assert_equal @@tkbranch.ops[:tkbumpdispo], lop.op
    end
  end

  # S: move to :sortdispo, carrier independent
  def test12s_tkbumpdispo
    lot = @@lots[:tktype_s]
    assert @@tkbranch.wait_lot_move(lot, :tkbumpdispo, tstart: @@tstart), "unexpected final operation"
    op = $sv.lot_info(lot).op
    if @@tksort == 'GG'
      # in fact the lot will be forwarded to tksortdispo, but too fast for us it will be off there
      assert [@@tkbranch.ops[:trc4s], @@tkbranch.ops[:tksortdispo]].member?(op)
    else
      # previous op is :tkbumpdispo
      assert_equal @@tkbranch.ops[:tksortdispo], op
    end
  end

  # U: move to TK-OQA, carrier independent
  def test12u_tkbumpdispo
    lot = @@lots[:tktype_u]
    assert @@tkbranch.wait_lot_move(lot, :tkbumpdispo, tstart: @@tstart), "unexpected final operation"
    # previous op is :tkbumpdispo
    lop = $sv._guess_op_route(lot, -1)[0]
    assert_equal @@tkbranch.ops[:tkbumpdispo], lop.op
  end

  # # # # #  sublottypes
  # special treatment for sublottypes E* and RD, except special customer "samsung"

  def test15e_tkbumpdispo
    lot = @@lots[:sublot_e]
    assert !@@tkbranch.wait_lot_move(lot, :tkbumpdispo, tstart: @@tstart), "unexpected final operation"
    refute_equal 'ONHOLD', $sv.lot_info(lot).status, "lot #{lot} status must not be ONHOLD"
  end

  def test15rd_tkbumpdispo
    lot = @@lots[:sublot_rd]
    assert !@@tkbranch.wait_lot_move(lot, :tkbumpdispo, tstart: @@tstart), "unexpected final operation"
    refute_equal 'ONHOLD', $sv.lot_info(lot).status, "lot #{lot} status must not be ONHOLD"
  end

  def test15rds_tkbumpdispo_special_customer
    lot = @@lots[:sublot_rds]
    assert @@tkbranch.wait_lot_move(lot, :tkbumpdispo, tstart: @@tstart), "unexpected final operation"
    if @@tkbump == 'GG'
      # previous op is :erpstatechgbump
      assert_equal @@tkbranch.ops[:trc4b], $sv.lot_info(lot).op
    else
      # previous op is :tkbumpdispo
      lop = $sv._guess_op_route(lot, -1)[0]
      assert_equal @@tkbranch.ops[:tkbumpdispo], lop.op
    end
  end

  def test20_move_tktypeufilter
    $setup_ok = false
    @@lots.values.each {|lot|
      assert_equal 0, $sv.lot_cleanup(lot, op: @@tkbranch.ops[:tktypeufilter], carrier: @@carriers, carrier_category: nil)
    }
    $sv.lot_mfgorder_change(@@lots[:sublot_rds], sublottype: @@sublottype_rd, customer: @@special_customer_typeufilter)
    assert_equal @@sublottype_rd, $sv.lot_info(@@lots[:sublot_rds]).sublottype
    assert_equal @@special_customer_typeufilter, $sv.lot_info(@@lots[:sublot_rds]).customer
    @@tstart = Time.now
    $setup_ok = true
  end

  def test22j_tktypeufilter
    # L, S, J: move to :tkbumpshipdispo
    lot = @@lots[:tktype_j]
    assert @@tkbranch.wait_lot_move(lot, :tktypeufilter, tstart: @@tstart), "unexpected final operation"
    assert_equal @@tkbranch.ops[:tkbumpshipdispo], $sv.lot_info(lot).op
  end

  def test22l_tktypeufilter
    # L, S, J: move to :tkbumpshipdispo
    lot = @@lots[:tktype_l]
    assert @@tkbranch.wait_lot_move(lot, :tktypeufilter, tstart: @@tstart), "unexpected final operation"
    assert_equal @@tkbranch.ops[:tkbumpshipdispo], $sv.lot_info(lot).op
  end

  def test22r_tktypeufilter
    # L, S, J, R: move to :tkbumpshipdispo
    lot = @@lots[:tktype_r]
    assert @@tkbranch.wait_lot_move(lot, :tktypeufilter, tstart: @@tstart), "unexpected final operation"
    assert_equal @@tkbranch.ops[:tkbumpshipdispo], $sv.lot_info(lot).op
  end

  def test22s_tktypeufilter
    # L, S, J: move to :tkbumpshipdispo
    lot = @@lots[:tktype_s]
    assert @@tkbranch.wait_lot_move(lot, :tktypeufilter, tstart: @@tstart), "unexpected final operation"
    assert_equal @@tkbranch.ops[:tkbumpshipdispo], $sv.lot_info(lot).op
  end

  def test22u_tktypeufilter
    # U: move to :chkplnhld
    lot = @@lots[:tktype_u]
    assert @@tkbranch.wait_lot_move(lot, :tktypeufilter, tstart: @@tstart), "unexpected final operation"
    assert_equal @@tkbranch.ops[:chkplnhld], $sv.lot_info(lot).op
  end

  # # # # #  sublottypes
  def test25e_tktypeufilter
    # special treatment for sublottypes E* and RD
    lot = @@lots[:sublot_e]
    assert !@@tkbranch.wait_lot_move(lot, :tktypeufilter, tstart: @@tstart), "unexpected final operation"
    refute_equal 'ONHOLD', $sv.lot_info(lot).status, "lot #{lot} status must not be ONHOLD"
  end

  def test25rd_tktypeufilter
    # special treatment for sublottypes E* and RD
    lot = @@lots[:sublot_rd]
    assert !@@tkbranch.wait_lot_move(lot, :tktypeufilter, tstart: @@tstart), "unexpected final operation"
    refute_equal 'ONHOLD', $sv.lot_info(lot).status, "lot #{lot} status must not be ONHOLD"
  end

  def test25rds_tktypeufilter_special_customer
    # special treatment for sublottypes E* and RD, except special customers (L, S, J move to :tkbumpshipdispo)
    lot = @@lots[:sublot_rds]
    assert @@tkbranch.wait_lot_move(lot, :tktypeufilter, tstart: @@tstart), "unexpected final operation"
    assert_equal @@tkbranch.ops[:tkbumpshipdispo], $sv.lot_info(lot).op
  end

  # :tkbumpshipdispo handled by FabGUI, removes carriers and moves lot on to :bumpshipinit
  def test30_move_tkbumpshipinit
    $setup_ok = false
    @@lots.values.each {|lot|
      assert_equal 0, $sv.lot_cleanup(lot, op: @@tkbranch.ops[:tkbumpshipinit], carrier: @@carriers, carrier_category: nil)
      # shuffle wafers so both wafer ids and wafer aliases are out of order and not matching each other
      li = $sv.lot_info(lot)
      $sv.wafer_sort_req(lot, li.carrier, slots: 'shuffle')
      $sv.wafer_alias_set(lot)
      $sv.wafer_sort_req(lot, li.carrier, slots: 'shuffle')
      # remove carrier
      assert_equal 0, $sv.wafer_sort_req(lot, '')
    }
    @@tstart = Time.now
    $setup_ok = true
  end

  def test32j_tkbumpshipinit
    # L, J, subcon !GG: move to TKEY bumproute, subcon GG ??
    # send shipmentToSubCon message
    lot = @@lots[:tktype_j]
    ($log.warn "does not happen for subcon GG"; return) if @@tkbump == 'GG'
    # send shipment message (like FabGUI)
    assert @@tkbranch.shipment_to_subcon(lot), "not processed"
    assert @@tkbranch.verify_lot_state(lot, :tkbumpshipinit, 'NonProBank')
    aliases = $sv.lot_info(lot, wafers: true).wafers.collect {|w| w.alias.to_i}
    assert_equal (1..25).to_a, aliases, "wafers are not ordered by alias"
  end

  def test32l_tkbumpshipinit
    # L, J, subcon !GG: move to TJEY bumproute, subcon GG ??
    # send shipmentToSubCon message
    lot = @@lots[:tktype_l]
    ($log.warn "does not happen for subcon GG"; return) if @@tkbump == 'GG'
    # send shipment message (like FabGUI)
    assert @@tkbranch.shipment_to_subcon(lot), "not processed"
    assert @@tkbranch.verify_lot_state(lot, :tkbumpshipinit, 'NonProBank')
    aliases = $sv.lot_info(lot, wafers: true).wafers.collect {|w| w.alias.to_i}
    assert_equal (1..25).to_a, aliases, "wafers are not ordered by alias"
  end

  def test32r_tkbumpshipinit
    # L, J, subcon !GG: move to TJEY bumproute, subcon GG ??
    # send shipmentToSubCon message
    lot = @@lots[:tktype_r]
    ($log.warn "does not happen for subcon GG"; return) if @@tkbump == 'GG'
    # send shipment message (like FabGUI)
    assert @@tkbranch.shipment_to_subcon(lot), "not processed"
    assert @@tkbranch.verify_lot_state(lot, :tkbumpshipinit, 'NonProBank')
    aliases = $sv.lot_info(lot, wafers: true).wafers.collect {|w| w.alias.to_i}
    assert_equal (1..25).to_a, aliases, "wafers are not ordered by alias"
  end

  def test32s_tkbumpshipinit
    # U, S: error msg only
    # send shipmentToSubCon message, must fail for types U, S
    lot = @@lots[:tktype_s]
    assert !@@tkbranch.shipment_to_subcon(lot), "msg must be rejected"
    assert @@tkbranch.verify_lot_state(lot, :tkbumpshipinit, 'Waiting')
  end

  def test32u_tkbumpshipinit
    # U, S: error msg only
    # send shipmentToSubCon message, must fail for types U, S
    lot = @@lots[:tktype_u]
    assert !@@tkbranch.shipment_to_subcon(lot), "msg must be rejected"
    assert @@tkbranch.service_response[:shipmentToSubConResponse][:return][:reason][nil].include?("turnkeyType is U"), "wrong message"
    assert @@tkbranch.verify_lot_state(lot, :tkbumpshipinit, 'Waiting')
  end


  # ERP activated: set all lots on hold because of a wrong Oracle part name
  # handling of happy lots is tested in Test_Tkey_FabGUI
  # note: only one lot is handled per jCAP job cycle

  def test40_move_erpstatechgbump
    $setup_ok = false
    @@lots.values.each {|lot|
      assert_equal 0, $sv.lot_cleanup(lot, opNo: :first, carrier: @@carriers, carrier_category: nil)
    }
    $setup_ok = true
  end

  def test42j_erpstatechgbump
    lot = @@lots[:tktype_j]
    assert_equal 0, $sv.lot_cleanup(lot, op: @@tkbranch.ops[:erpstatechgbump]), "error preparing lot #{lot}"
    assert @@tkbranch.wait_lot_hold(lot, 'O-TK'), "lot #{lot} status must be ONHOLD"
  end

  def test42l_erpstatechgbump
    lot = @@lots[:tktype_l]
    assert_equal 0, $sv.lot_cleanup(lot, op: @@tkbranch.ops[:erpstatechgbump]), "error preparing lot #{lot}"
    assert @@tkbranch.wait_lot_hold(lot, 'O-TK'), "lot #{lot} status must be ONHOLD"
  end

  def test42r_erpstatechgbump
    lot = @@lots[:tktype_r]
    assert_equal 0, $sv.lot_cleanup(lot, op: @@tkbranch.ops[:erpstatechgbump]), "error preparing lot #{lot}"
    assert @@tkbranch.wait_lot_hold(lot, 'O-TK'), "lot #{lot} status must be ONHOLD"
  end

  def test42s_erpstatechgbump
    lot = @@lots[:tktype_s]
    assert_equal 0, $sv.lot_cleanup(lot, op: @@tkbranch.ops[:erpstatechgbump]), "error preparing lot #{lot}"
    assert @@tkbranch.wait_lot_hold(lot, 'O-TK'), "lot #{lot} status must be ONHOLD"
  end

  def test42u_erpstatechgbump
    lot = @@lots[:tktype_u]
    assert_equal 0, $sv.lot_cleanup(lot, op: @@tkbranch.ops[:erpstatechgbump]), "error preparing lot #{lot}"
    assert @@tkbranch.wait_lot_hold(lot, 'O-TK'), "lot #{lot} status must be ONHOLD"
  end


  def test60_move_tksortdispo
    $setup_ok = false
    @@lots.values.each {|lot|
      assert_equal 0, $sv.lot_cleanup(lot, op: @@tkbranch.ops[:tksortdispo], carrier: @@carriers, carrier_category: nil)
    }
    @@tstart = Time.now
    $setup_ok = true
  end

  def test62j_tksortdispo
    # J: subcon bump and sort != GG: do nothing (is been moved to TKEY sortroute by B2B)
    # S, J: subcon sort GG: move to :erpstatechgsort (tmp: following PD)
    lot = @@lots[:tktype_j]
    moved = @@tkbranch.wait_lot_move(lot, :tksortdispo, tstart: @@tstart)
    li = $sv.lot_info(lot)
    if @@tksort == 'GG'
      assert_equal @@tkbranch.ops[:trc4s], li.op
    else
      assert !moved, "lot has been moved unexpectedly"
      refute_equal 'ONHOLD', li.status, "lot #{lot} status must not be ONHOLD"
    end
  end

  def test62jn_tksortdispo_nosubcons
    # special treatment for lots with missing subcon
    lot = @@lots[:tktype_jn]
    assert !@@tkbranch.wait_lot_move(lot, :tksortdispo, tstart: @@tstart), "unexpected final operation"
    assert @@tkbranch.wait_lot_hold(lot, 'O-TK', tstart: @@tstart), "lot #{lot} status must be ONHOLD"
    assert verify_derdack_msg(lot, @@tstart), "error in Derdack notification"
  end

  def test62l_tksortdispo
    # L: carrier 9*: move to :lotshipdesig, else: not yet defined
    # check current op, virtual carriers (starting with "9") get moved to :lotshipdesig, others undefined
    lot = @@lots[:tktype_l]
    assert @@tkbranch.wait_lot_move(lot, :tksortdispo, tstart: @@tstart), "lot #{lot} has not been moved"
    li = $sv.lot_info(lot)
    if @@tksort == 'GG'
      ##assert_equal @@tkbranch.ops[:sortshipinit], $sv.lot_info(lot).op], "unexpected final operation"
    else
      if li.carrier.start_with?('9')
        assert_equal @@tkbranch.ops[:lotshipdesig], li.op
      end
    end
  end

  def test62r_tksortdispo
    # J: subcon bump and sort != GG: do nothing (is been moved to TKEY sortroute by B2B)
    # S, J: subcon sort GG: move to :erpstatechgsort (tmp: following PD)
    lot = @@lots[:tktype_r]
    moved = @@tkbranch.wait_lot_move(lot, :tksortdispo, tstart: @@tstart)
    li = $sv.lot_info(lot)
    if @@tksort == 'GG'
      assert_equal @@tkbranch.ops[:trc4s], li.op
    else
      assert !moved, "lot has been moved unexpectedly"
      refute_equal 'ONHOLD', li.status, "lot #{lot} status must not be ONHOLD"
    end
  end

  def test62s_tksortdispo
    # S: subcon != GG (no 9er carrier!): move to OQA (next PD), S: 9er carrier (should not happen): do nothing
    # S, J: subcon sort GG: move to :erpstatechgsort (tmp: following PD)
    lot = @@lots[:tktype_s]
    moved = @@tkbranch.wait_lot_move(lot, :tksortdispo, tstart: @@tstart)
    li = $sv.lot_info(lot)
    if @@tksort == 'GG'
      assert moved, "lot #{lot} has not been moved"
      assert_equal @@tkbranch.ops[:trc4s], li.op
    else
      if li.carrier.start_with?('9')
        # should not happen
        assert !moved, "lot has been moved unexpectedly"
      else
        # regular case
        lop = $sv._guess_op_route(lot, -1)[0]
        assert_equal @@tkbranch.ops[:tksortdispo], lop.op
      end
    end
  end

  def test62u_tksortdispo
    # U: not moved
    lot = @@lots[:tktype_u]
    assert !@@tkbranch.wait_lot_move(lot, :tksortdispo, tstart: @@tstart), "unexpected lot move"
    assert @@tkbranch.verify_lot_state(lot, :tksortdispo, 'Waiting')
  end

  def test65e_tksortdispo_sublottype
    # special treatment for sublottypes E* and RD
    lot = @@lots[:sublot_e]
    assert !@@tkbranch.wait_lot_move(lot, :tksortdispo, tstart: @@tstart), "unexpected final operation"
    assert @@tkbranch.verify_lot_state(lot, :tksortdispo, 'Waiting')
  end

  def test65rd_tksortdispo_sublottype
    # special treatment for sublottypes E* and RD
    lot = @@lots[:sublot_rd]
    assert !@@tkbranch.wait_lot_move(lot, :tksortdispo, tstart: @@tstart), "unexpected final operation"
    assert @@tkbranch.verify_lot_state(lot, :tksortdispo, 'Waiting')
  end

  def test65rds_tksortdispo_sublottype_rd_cust
    # special treatment for sublottypes E* and RD, no exception here for special customers!
    lot = @@lots[:sublot_rds]
    assert !@@tkbranch.wait_lot_move(lot, :tksortdispo, tstart: @@tstart), "unexpected final operation"
    assert @@tkbranch.verify_lot_state(lot, :tksortdispo, 'Waiting')
  end


  def test70_move_tktypelfilter
    $setup_ok = false
    @@lots.values.each {|lot|
      assert_equal 0, $sv.lot_cleanup(lot, op: @@tkbranch.ops[:tktypelfilter], carrier: @@carriers, carrier_category: nil)
    }
    @@tstart = Time.now
    $setup_ok = true
  end

  def test72j_tktypelfilter
    # U, S, J: move to :tksortshipdispo
    lot = @@lots[:tktype_j]
    assert @@tkbranch.wait_lot_move(lot, :tktypelfilter, tstart: @@tstart), "unexpected final operation"
    assert_equal @@tkbranch.ops[:tksortshipdispo], $sv.lot_info(lot).op
  end

  def test72l_tktypelfilter
    # L: move to :chkplnhld if carrier 9%, but another jCAP job moves the lot on
    lot = @@lots[:tktype_l]
    assert @@tkbranch.wait_lot_move(lot, :tktypelfilter, tstart: @@tstart), "unexpected final operation"
    assert_equal @@tkbranch.ops[:chkplnhld], $sv.lot_info(lot).op
    ##assert false # ??? [@@tkbranch.ops[:chkplnhld], @@tkbranch.ops[:trc4r]].member?(lc.op), "unexpected final operation"
  end

  def test72r_tktypelfilter
    # U, S, J: move to :tksortshipdispo
    lot = @@lots[:tktype_j]
    assert @@tkbranch.wait_lot_move(lot, :tktypelfilter, tstart: @@tstart), "unexpected final operation"
    assert_equal @@tkbranch.ops[:tksortshipdispo], $sv.lot_info(lot).op
  end

  def test72s_tktypelfilter
    # U, S, J: move to :tksortshipdispo
    lot = @@lots[:tktype_s]
    assert @@tkbranch.wait_lot_move(lot, :tktypelfilter, tstart: @@tstart), "unexpected final operation"
    assert_equal @@tkbranch.ops[:tksortshipdispo], $sv.lot_info(lot).op
  end

  def test72u_tktypelfilter
    # U, S, J: move to :tksortshipdispo
    lot = @@lots[:tktype_u]
    assert @@tkbranch.wait_lot_move(lot, :tktypelfilter, tstart: @@tstart), "unexpected final operation"
    assert_equal @@tkbranch.ops[:tksortshipdispo], $sv.lot_info(lot).op
  end

  # :tksortshipdispo handled by FabGUI, moved on to :tksortshipinit

  def test80_move_tksortshipinit
    $setup_ok = false
    @@lots.values.each {|lot|
      assert_equal 0, $sv.lot_cleanup(lot, op: @@tkbranch.ops[:tksortshipinit], carrier: @@carriers, carrier_category: nil)
      # shuffle wafers so both wafer ids and wafer aliases are out of order and not matching each other
      li = $sv.lot_info(lot)
      $sv.wafer_sort_req(lot, li.carrier, slots: 'shuffle')
      $sv.wafer_alias_set(lot)
      $sv.wafer_sort_req(lot, li.carrier, slots: 'shuffle')
      # remove carrier
      assert_equal 0, $sv.wafer_sort_req(lot, '')
    }
    @@tstart = Time.now
    $setup_ok = true
  end

  def test82j_tksortshipinit
    # J: should not happen, treat like S
    # send shipmentToSubCon message
    lot = @@lots[:tktype_j]
    ($log.warn "does not happen for subcon GG"; return) if @@tksort == 'GG'
    # send shipment message (like FabGUI)
    assert @@tkbranch.shipment_to_subcon(lot), "not processed"
    assert @@tkbranch.verify_lot_state(lot, :tksortshipinit, 'NonProBank')
    aliases = $sv.lot_info(lot, wafers: true).wafers.collect {|w| w.alias.to_i}
    assert_equal (1..25).to_a, aliases, "wafers are not ordered by alias"
  end

  def test82l_tksortshipinit
    # U, L: do nothing
    # send shipmentToSubCon message, must fail for types U, L
    lot = @@lots[:tktype_l]
    assert !@@tkbranch.shipment_to_subcon(lot), "msg must be rejected"
    assert @@tkbranch.verify_lot_state(lot, :tksortshipinit, 'Waiting')
  end

  def test82r_tksortshipinit
    # J: should not happen, treat like S
    # send shipmentToSubCon message
    lot = @@lots[:tktype_r]
    ($log.warn "does not happen for subcon GG"; return) if @@tksort == 'GG'
    # send shipment message (like FabGUI)
    assert @@tkbranch.shipment_to_subcon(lot), "not processed"
    assert @@tkbranch.verify_lot_state(lot, :tksortshipinit, 'NonProBank')
    aliases = $sv.lot_info(lot, wafers: true).wafers.collect {|w| w.alias.to_i}
    assert_equal (1..25).to_a, aliases, "wafers are not ordered by alias"
  end

  def test82s_tksortshipinit
    # S (happy case, expecting lots without carrier (or 9*): subcon != GG: move to TKEY sortroute
    #   subcon GG: does not happen
    # send shipmentToSubCon message
    lot = @@lots[:tktype_s]
    ($log.warn "does not happen for subcon GG"; return) if @@tksort == 'GG'
    # send shipment message (like FabGUI)
    assert @@tkbranch.shipment_to_subcon(lot), "not processed"
    assert @@tkbranch.verify_lot_state(lot, :tksortshipinit, 'NonProBank')
    aliases = $sv.lot_info(lot, wafers: true).wafers.collect {|w| w.alias.to_i}
    assert_equal (1..25).to_a, aliases, "wafers are not ordered by alias"
  end

  def test82u_tksortshipinit
    # U, L: do nothing
    # send shipmentToSubCon message, must fail for types U, L
    lot = @@lots[:tktype_u]
    assert !@@tkbranch.shipment_to_subcon(lot), "msg must be rejected"
    assert @@tkbranch.verify_lot_state(lot, :tksortshipinit, 'Waiting')
  end

  # ERP activated: set all lots on hold because of a wrong Oracle part name
  # handling of happy lots is tested in Test_Tkey_FabGUI
  # note: only one lot is handled per jCAP job cycle

  def test90_move_erpstatechgsort
    $setup_ok = false
    @@lots.values.each {|lot|
      assert_equal 0, $sv.lot_cleanup(lot, opNo: :first, carrier: @@carriers, carrier_category: nil)
    }
    $setup_ok = true
  end

  def test92j_erpstatechgsort
    lot = @@lots[:tktype_j]
    assert_equal 0, $sv.lot_cleanup(lot, op: @@tkbranch.ops[:erpstatechgsort]), "error preparing lot #{lot}"
    assert @@tkbranch.wait_lot_hold(lot, 'O-TK'), "lot #{lot} status must be ONHOLD"
  end

  def test92l_erpstatechgsort
    lot = @@lots[:tktype_l]
    assert_equal 0, $sv.lot_cleanup(lot, op: @@tkbranch.ops[:erpstatechgsort]), "error preparing lot #{lot}"
    assert @@tkbranch.wait_lot_hold(lot, 'O-TK'), "lot #{lot} status must be ONHOLD"
  end

  def test92r_erpstatechgsort
    lot = @@lots[:tktype_r]
    assert_equal 0, $sv.lot_cleanup(lot, op: @@tkbranch.ops[:erpstatechgsort]), "error preparing lot #{lot}"
    assert @@tkbranch.wait_lot_hold(lot, 'O-TK'), "lot #{lot} status must be ONHOLD"
  end

  def test92s_erpstatechgsort
    lot = @@lots[:tktype_s]
    assert_equal 0, $sv.lot_cleanup(lot, op: @@tkbranch.ops[:erpstatechgsort]), "error preparing lot #{lot}"
    assert @@tkbranch.wait_lot_hold(lot, 'O-TK'), "lot #{lot} status must be ONHOLD"
  end

  def test92u_erpstatechgsort
    lot = @@lots[:tktype_u]
    assert_equal 0, $sv.lot_cleanup(lot, op: @@tkbranch.ops[:erpstatechgsort]), "error preparing lot #{lot}"
    assert @@tkbranch.wait_lot_hold(lot, 'O-TK'), "lot #{lot} status must be ONHOLD"
  end

  # :chkplnhld is handled by separate jCAP job (MoveToBank?)

  def test99_info
    $log.info "tested with subcons #{@@tkbump}, #{@@tksort}"
  end


  def verify_msg_parameter(lce, msg, parameter)
    msge = msg.params[parameter]
    return true if lce == msge
    return true if ['', 'null', nil].member?(msge) && ['', nil].member?(lce)
    $log.warn "  wrong #{parameter}: #{msge.inspect} instead of #{lce.inspect}"
    return false
  end

  def verify_derdack_msg(lot, tstart)
    # return true on success
    $log.info "verify_derdack_msg for lot #{lot}, starttime #{tstart.inspect}"
    test_ok = true
    msg = @@derdack.get_events(tstart - 10, nil, 'Event'=>'TurnkeyDispoMissingSubconIdEvent', 'LotID'=>lot).first
    ($log.warn "  error accessing Derdack db"; return) unless msg
    lc = $sv.lot_characteristics(lot)
    test_ok &= verify_msg_parameter(lc.sublottype, msg, 'SubLotType')
    test_ok &= verify_msg_parameter(lc.erpitem, msg, 'ERPItemID')
    test_ok &= verify_msg_parameter(lc.tkinfo.tktype, msg, 'TurnKeyType')
    test_ok &= verify_msg_parameter(lc.tkinfo.tkbump, msg, 'SubConBumpID')
    test_ok &= verify_msg_parameter(lc.tkinfo.tksort, msg, 'SubConSortID')
    return test_ok
  end
end
