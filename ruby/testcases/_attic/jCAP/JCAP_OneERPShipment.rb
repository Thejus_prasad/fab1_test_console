=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author:   ssteidte, 2011-10-28

Version:  20.12

History:
  2015-05-12 ssteidte, cleaned up and verified the tests
  2015-11-26 sfrieske, added test cases for 15.11
  2016-05-09 sfrieske, added test cases for 16.04
  2016-12-12 sfrieske, minor cleanup
  2018-03-12 sfrieske, fixed test43
  2019-01-23 sfrieske, use new DerdackEvents
  2019-06-20 sfrieske, simplified test setup and added test34
  2019-07-23 sfrieske, test for lot script parameter removal (MSR 1140518)
  2020-07-02 sfrieske, added lot not check for error responses (MSR 1585494)
  2021-02-17 sfrieske, fixed lot note checks because STBMfgAttributes now adds a note
=end

require 'derdack'
require 'jcap/erpshipment'
require 'SiViewTestCase'


class JCAP_OneERPShipment < SiViewTestCase
  @@sv_defaults = {carrier_category: nil, carriers: '%'}
  @@bank_end_wrong = 'UT-USE'
  @@bank_label = 'XY-PC-DISPO'   # identified by UDATA 'BankReportingType' containing 'STD-PRINT'
  @@ship_params = {
    'ShipToFirm'=>'QA', 'ShipToEngineer'=>'QA', 'ShipLabelNote'=>'QA', 'ShipComment'=>'QA'
  }
  @@ship_params_delete = ['ShipLabelNote']  # configurable property, Prod: 'ShipLabelNote'
  @@gid_customer = 'qaeric'                 # a customer with UDATA GID_SOURCE 'LOT_LABEL'

  @@job_interval = 400  # includes ERP communication
  @@testlots = []


  def self.after_all_passed
    @@testlots.each {|lot| @@sv.delete_lot_family(lot)}
  end


  def test00_setup
    $setup_ok = false
    #
    @@notifications = DerdackEvents.new($env)
    assert @@notifications.db.connected?, 'no Derdack DB connection'
    #
    @@prepackage = ERPShipment::PrePackage.new($env)
    assert @@prepackage.get_version, 'no MQ connection to jCAP (PrePackage)'
    #
    assert @@sv.user_data(:bank, @@bank_label)['BankReportingType'].include?('STD-PRINT'), 'wrong bank UDATA BankReportingType'
    # need 2 9% carriers and 1 1% carrier
    assert @@svtest.get_empty_carrier(carrier: '9%', n: 2, carrier_category: nil), 'not enough empty 9% carriers'
    assert @@svtest.get_empty_carrier(carrier: '10%', carrier_category: 'TEST'), 'not enough empty 1% carriers'
    #
    assert @@erps = ERPShipment::PickShip.new($env, sv: @@sv)
    @@erps[:banks] = {end_std: 'O-ENDFG', end_nonstd: 'O-SHIP-NS', end_nonstd_return: 'O-OUTFAB8', ship: 'O-SHIP'} if $env == 'f8stag'
    #
    assert @@erps.get_version, 'no MQ connection to JCAP Shipment'
    #
    assert @@lot = @@svtest.new_lot, 'error creating test lot'
    @@testlots << @@lot
    #
    $setup_ok = true
  end


  # pick confirm (pickLotsForShipment)

  def test11_pick_confirm_std
    ['9%', 'Z%', 'F%', 'U%'].each {|carrier|
      $log.info "\n-- standard, carrier #{carrier.inspect}"
      assert @@erps.pick_confirm_verify(@@lot, carrier: carrier)
    }
  end

  def test12_pick_confirm_nonstd
    ['9%', 'Z%', 'F%', 'U%'].each {|carrier|
      $log.info "\n-- standard, carrier #{carrier.inspect}"
      assert @@erps.pick_confirm_verify(@@lot, carrier: carrier, nonstd: true)
    }
  end

  def test13_pick_confirm_STUB
    assert @@erps.pick_confirm_verify(@@lot, carrier: 'Z%', shipto: 'STUB')
    assert @@erps.pick_confirm_verify(@@lot, carrier: 'Z%', shipto: 'STUB', nonstd: true)
  end


  # pick release

  def test21_pick_std
    lot = @@lot
    assert @@erps.prepare_picking(lot), 'error preparing lot'
    # pick
    order = unique_string
    shipto = unique_string
    assert @@erps.pick_release(lot, order: order, shipto: shipto), 'error sending pick_release msg'
    assert_equal @@erps.banks[:ship], @@sv.lot_info(lot).bank, 'lot has not been moved to ship bank'
    assert_equal order, @@sv.user_parameter('Lot', lot, name: 'ERPSalesOrder').value, 'wrong script parameter ERPSalesOrder'
    assert_equal shipto, @@sv.user_parameter('Lot', lot, name: 'ShipToFirm').value, 'wrong script parameter ShipToFirm'
    # pick update
    order = unique_string
    shipto = unique_string
    assert @@erps.pick_release(lot, order: order, shipto: shipto), 'error sending pick_release msg'
    assert_equal @@erps.banks[:ship], @@sv.lot_info(lot).bank, 'lot has not been moved to ship bank'
    assert_equal order, @@sv.user_parameter('Lot', lot, name: 'ERPSalesOrder').value, 'wrong script parameter ERPSalesOrder'
    assert_equal shipto, @@sv.user_parameter('Lot', lot, name: 'ShipToFirm').value, 'wrong script parameter ShipToFirm'
    # pick from wrong end bank
    assert_equal 0, @@sv.lot_bank_move(lot, @@bank_end_wrong)
    refute @@erps.pick_release(lot, order: order, shipto: shipto), 'picking must be refused'
    assert resp = @@erps.mq.service_response[:pickReleaseResponse][:return]
    assert_equal 'ONEERP_MES_SHIPMENT_VALIDATION_ERROR', resp[:code][nil], 'wrong error message'
    assert_equal @@bank_end_wrong, @@sv.lot_info(lot).bank, 'lot must not be moved from end bank'
    assert_equal resp[:details][:msg], @@sv.lot_notes(lot).first.desc, 'wrong lot note'
  end

  def test22_pick_nonstd
    lot = @@lot
    assert @@erps.prepare_picking(lot, nonstd: true), 'error preparing lot'
    # pick (try as std, must fail)
    refute @@erps.pick_release(lot, nonstd: false), 'picking must be refused'
    assert resp = @@erps.mq.service_response[:pickReleaseResponse][:return]
    assert_equal 'ONEERP_MES_SHIPMENT_VALIDATION_ERROR', resp[:code][nil], 'wrong error message'
    assert_equal resp[:details][:msg], @@sv.lot_notes(lot).first.desc, 'wrong lot note'
    # pick non-std
    assert @@erps.pick_release(lot, nonstd: true), 'error picking lot'
    assert_equal @@erps.banks[:end_nonstd], @@sv.lot_info(lot).bank, 'lot must not be moved from end bank'
    assert_empty @@sv.lot_hold_list(lot)
    # pick update
    assert @@erps.pick_release(lot, nonstd: true), 'error sending pick_release msg'
    assert_equal @@erps.banks[:end_nonstd], @@sv.lot_info(lot).bank, 'lot must not be moved from end bank'
    assert_empty @@sv.lot_hold_list(lot)
  end

  def test23_pick_nonstd_return
    lot = @@lot
    assert @@erps.prepare_picking_nonstd_return(lot, carrier: '%'), 'error preparing lot'
    # pick (try as std, must fail)
    order = unique_string
    shipto = unique_string
    refute @@erps.pick_release(lot, order: order, shipto: shipto, nonstd: false), 'picking must be refused'
    assert resp = @@erps.mq.service_response[:pickReleaseResponse][:return]
    assert_equal 'ONEERP_MES_SHIPMENT_VALIDATION_ERROR', resp[:code][nil], 'wrong error message'
    assert_equal resp[:details][:msg], @@sv.lot_notes(lot).first.desc, 'wrong lot note'
    # pick non-std
    assert @@erps.pick_release(lot, order: order, shipto: shipto, nonstd: true), 'error picking lot'
    assert_equal @@erps.banks[:end_nonstd_return], @@sv.lot_info(lot).bank, 'lot must not be moved from end bank'
    assert_empty @@sv.lot_hold_list(lot, type: 'LotHold')
    # pick update
    order = unique_string
    shipto = unique_string
    assert @@erps.pick_release(lot, order: order, shipto: shipto, nonstd: true), 'error sending pick_release msg'
    assert_equal @@erps.banks[:end_nonstd_return], @@sv.lot_info(lot).bank, 'lot must not be moved from end bank'
    assert_empty @@sv.lot_hold_list(lot, type: 'LotHold')
  end

  def test24_pick_confirm_subcon
    # shipment from subcon to customer, lot is in a 9% carrier
    assert lot = @@svtest.new_lot(carrier: '9%'), 'error creating test lot'
    @@testlots << lot
    assert @@erps.prepare_picking(lot, carrier: '9%'), 'error preparing lot'
    # pick confirm
    order = unique_string
    shipto = unique_string
    assert @@erps.pick_confirm(lot, order: order, shipto: shipto), 'error sending pick_confirm msg'
    assert_equal @@erps.banks[:ship], @@sv.lot_info(lot).bank, 'lot has not been moved to ship bank'
    assert_equal order, @@sv.user_parameter('Lot', lot, name: 'ERPSalesOrder').value, 'wrong script parameter ERPSalesOrder'
    assert_equal shipto, @@sv.user_parameter('Lot', lot, name: 'ShipToFirm').value, 'wrong script parameter ShipToFirm'
    # pick update
    order = unique_string
    shipto = unique_string
    assert @@erps.pick_confirm(lot, order: order, shipto: shipto), 'error sending msg'
    assert_equal @@erps.banks[:ship], @@sv.lot_info(lot).bank, 'lot has not been moved to ship bank'
    assert_equal order, @@sv.user_parameter('Lot', lot, name: 'ERPSalesOrder').value, 'wrong script parameter ERPSalesOrder'
    assert_equal shipto, @@sv.user_parameter('Lot', lot, name: 'ShipToFirm').value, 'wrong script parameter ShipToFirm'
    # pick from wrong end bank
    assert_equal 0, @@sv.lot_bank_move(lot, @@bank_end_wrong)
    refute @@erps.pick_confirm(lot), 'picking must be refused'
    assert resp = @@erps.mq.service_response[:pickLotsForShipmentResponse][:return]
    assert_equal 'ONEERP_MES_SHIPMENT_VALIDATION_ERROR', resp[:code][nil], 'wrong error message'
    assert_equal @@bank_end_wrong, @@sv.lot_info(lot).bank, 'lot must not be moved from end bank'
    assert_equal resp[:details][:msg], @@sv.lot_notes(lot).first.desc, 'wrong lot note'
  end

  def test29_pick_notification
    # pick a wrong lot
    tstart = Time.now
    lot = 'DUMMY'
    assert_equal false, @@erps.pick_release(lot), 'error sending pick_release msg'
    code = @@erps.mq.service_response[:pickReleaseResponse][:return][:code][nil]
    assert_equal 'ONEERP_MES_SHIPMENT_PROCESSING_ERROR', code
    sleep 90
    msgs = @@notifications.query(tstart: tstart - 20, filter: {'Application'=>'JCAP', 'Event'=>'OneErpPickEvent', 'LotID'=>lot})
    assert_equal 1, msgs.size, 'missing or wrong notification'
  end


  # ship complete

  def test31_ship_std
    ['9%', '10%', 'U%', '3%'].each {|carrier|
      $log.info "\n-- standard, carrier #{carrier.inspect}"
      assert @@erps.pick_ship_verify(@@lot, carrier: carrier, ship_params_delete: @@ship_params_delete)
    }
  end

  def test32_ship_nonstd
    ['9%', '10%', 'U%', '3%'].each {|carrier|
      $log.info "\n-- nonstandard, carrier #{carrier.inspect}"
      assert @@erps.pick_ship_verify(@@lot, nonstd: true, carrier: carrier)
    }
  end

  def test33_ship_nonstd_return
    # prepare
    lot = @@lot
    assert @@erps.prepare_picking_nonstd_return(lot, carrier: '%'), 'error preparing lot'
    # pick
    assert @@erps.pick_release(lot, nonstd: true), 'error picking lot'
    assert_equal @@erps.banks[:end_nonstd_return], @@sv.lot_info(lot).bank, 'wrong bank'
    # ship non-std
    assert_equal 0, @@sv.user_parameter_change('Lot', lot, @@ship_params), 'error setting script parameters'
    sleep 10
    assert @@erps.ship_complete(lot, nonstd: true), 'error shipping lot'  # fixed in 20.12
    #
    li = @@sv.lot_info(lot)
    assert_equal 'NonProBank', li.status, 'wrong lot status'
    assert_equal @@erps.banks[:end_nonstd_return], li.bank, 'lot is not on ns return bank'
    assert_empty @@sv.lot_hold_list(lot, type: 'LotHold'), 'error releasing lot hold for non-std shipment'
    assert @@sv.lot_notes(lot).select {|n| n.title == 'Non-Standard Shipped'}.first, 'lot note not written'
    uparams = @@sv.user_parameter('Lot', lot, all: true)
    @@ship_params_delete.each {|k|
      refute uparams.find {|e| e.name == k}.valueflag, "#{k} has not been deleted"
    }
  end

  def test34_transrunning_final  # JCORP-2348
    # create lot with special user_parameters, complete and move it to the ship bank
    assert lot = @@svtest.new_lot(user_parameters: {'TurnkeyType'=>'UU', 'TurnkeySubconIDBump'=>'G7'})
    @@testlots << lot
    assert_equal 0, @@sv.lot_opelocate(lot, :last)
    assert_equal 0, @@sv.lot_bank_move(lot, @@erps.banks[:ship]), 'bank move error'
    # send ship complete with transStatus Trans-Running
    #   shipto is GB or G7 as in job properties transRunningShipToLocFinalShipment
    assert_equal 0, @@sv.user_parameter_change('Lot', lot, @@ship_params), 'error setting script parameters'
    sleep 10
    assert @@erps.ship_complete(lot, transstatus: 'Trans-Running', shipto: 'G7'), 'error shipping lot'
    # verify lot status
    assert_equal 'SHIPPED', @@sv.lot_info(lot).status, 'wrong lot status'
    uparams = @@sv.user_parameter('Lot', lot, all: true)
    @@ship_params_delete.each {|k|
      refute uparams.find {|e| e.name == k}.valueflag, "#{k} has not been deleted"
    }
  end

  def test35_transrunning_final_notgg
    # create lot with special user_parameters, complete and move it to the ship bank
    # carrier must be 1% or 9% (job properties)
    assert lot = @@svtest.new_lot(carrier: '1%', user_parameters: {'TurnkeyType'=>'UU', 'TurnkeySubconIDBump'=>'AS'})
    @@testlots << lot
    assert_equal 0, @@sv.lot_opelocate(lot, :last)
    assert_equal 0, @@sv.lot_bank_move(lot, @@erps.banks[:ship]), 'bank move error'
    # send ship complete with transStatus Trans-Running
    #   shipto is NOT GB or G7 as in job properties transRunningShipToLocFinalShipment
    assert_equal 0, @@sv.user_parameter_change('Lot', lot, @@ship_params), 'error setting script parameters'
    sleep 10
    assert @@erps.ship_complete(lot, transstatus: 'Trans-Running', shipto: 'AS'), 'error shipping lot'
    # verify lot status
    assert_equal 'SHIPPED', @@sv.lot_info(lot).status, 'wrong lot status'
    uparams = @@sv.user_parameter('Lot', lot, all: true)
    @@ship_params_delete.each {|k|
      refute uparams.find {|e| e.name == k}.valueflag, "#{k} has not been deleted"
    }
  end

  def test36_transrunning_notgg  # MSR 1783625
    # create lot on a route with PD TKEYBUMPSHIPINIT
    assert lot = @@svtest.new_lot(product: 'TKEY01.A0', route: 'C02-TKEY.01',
      user_parameters: {'TurnkeyType'=>'J', 'TurnkeySubconIDBump'=>'AS', 'TurnkeySubconIDSort'=>'AS'})
    @@testlots << lot
    assert_equal 0, @@sv.lot_opelocate(lot, nil, op: 'TKEYBUMPSHIPINIT.01')
    assert_equal 0, @@sv.lot_nonprobankin(lot, 'O-TKYWBANK'), 'bankin error'
    # send ship complete with transStatus Trans-Running
    #   shipto is NOT GB or G7 as in job properties transRunningShipToLocFinalShipment
    assert_equal 0, @@sv.user_parameter_change('Lot', lot, @@ship_params), 'error setting script parameters'
    sleep 10
    assert @@erps.ship_complete(lot, transstatus: 'Trans-Running', shipto: 'AS'), 'error shipping lot'
    # verify lot status
    assert_equal 'NonProBank', @@sv.lot_info(lot).status, 'wrong lot status'
    li = @@sv.lot_info(lot)
    assert li.carrier.start_with?('9'), 'carrier has not been changed to 9*'
  end

  def testdev38_ship_nonstd_return_GID_SOURCE  # UNDER DEV, NOT WORKING
    # prepare
    assert lot = @@svtest.new_lot(customer: @@gid_customer), 'error creating test lot'
    @@testlots << lot
    refute_empty label = @@sv.lot_label(lot), 'lot has no LOT_LABEL'
    assert @@erps.prepare_picking_nonstd_return(lot, carrier: '%'), 'error preparing lot'
    # pick
    assert @@erps.pick_release(label, nonstd: true), 'error picking lot'
    assert_equal @@erps.banks[:end_nonstd_return], @@sv.lot_info(lot).bank, 'wrong bank'
    # ship non-std
    assert_equal 0, @@sv.user_parameter_change('Lot', lot, @@ship_params), 'error setting script parameters'
    assert @@erps.ship_complete(lot, nonstd: true), 'error shipping lot'
    li = @@sv.lot_info(lot)
    assert_equal 'NonProBank', li.status, 'wrong lot status'
    assert_equal @@erps.banks[:end_nonstd_return], li.bank, 'lot is not on ns return bank'
    assert_empty @@sv.lot_hold_list(lot, type: 'LotHold'), 'error releasing lot hold for non-std shipment'
    assert @@sv.lot_notes(lot).select {|n| n.title == 'Non-Standard Shipped'}.first, 'lot note not written'
    uparams = @@sv.user_parameter('Lot', lot, all: true)
    @@ship_params_delete.each {|k| refute uparams.find {|e| e.name == k}.valueflag, "#{k} has not been deleted"}
  end

  def testdev39_ship_bsm
    assert @@erps.pick_ship_verify(@@lot, shipto: 'F1-BTF')  # gets shipped but should not ??
  end

  def test41_preliminary_shipment
    # for label printing, MSRs 680564, 828023
    skip 'not applicable in f8stag' if $env == 'f8stag'
    # prepare lots
    assert lots = @@svtest.new_lots(2), 'error creating test lots'
    @@testlots += lots
    assert_equal 0, @@sv.user_parameter_change('Lot', lots[0], 'CountryOfOrigin', 'DE')
    assert_equal 0, @@sv.user_parameter_change('Lot', lots[1], 'CountryOfOrigin', 'US')
    lots.each {|lot|
      assert_equal 0, @@sv.lot_nonprobankin(lot, @@bank_label)
    }
    lots.each {|lot|
      # check for note, StbmfgAttributes has set the first note
      nlotnotes = 2
      assert wait_for(timeout: @@job_interval) {@@sv.lot_notes(lot).size == nlotnotes}
      note = @@sv.lot_notes(lot).first
      assert_equal 'LotCompletion.LabelPrintWfrCount', note.title
      assert_equal '25', note.desc
      # split lot
      assert lc = @@sv.lot_split(lot, 5, onhold: true), 'error splitting lot'
      # check for new lot note on child, the first note is inherited
      nlotnotes += 1
      assert wait_for(timeout: @@job_interval) {@@sv.lot_notes(lc).size == nlotnotes}
      note = @@sv.lot_notes(lc).first
      assert_equal '5', note.desc
      # check for new lot note on parent
      assert wait_for(timeout: @@job_interval) {@@sv.lot_notes(lot).size == nlotnotes}
      note = @@sv.lot_notes(lot).first
      assert_equal '20', note.desc
      # merge lot back
      assert_equal 0, @@sv.lot_merge(lot, lc)
      nlotnotes += 1
      assert wait_for(timeout: @@job_interval) {@@sv.lot_notes(lot).size == nlotnotes}
      note = @@sv.lot_notes(lot).first
      assert_equal '25', note.desc
      # set FabGUI note and split
      assert_equal 0, @@sv.lot_note_register(lot, 'LotCompletion', 'Test')
      nlotnotes += 1
      # assert_equal nlotnotes, @@sv.lot_notes(lot).size, 'wrong number of lotnotes'
      assert lc = @@sv.lot_split(lot, 5, onhold: true), 'error splitting lot'
      # new notes (ignoring FabGUI's LotCompletion), since v 14.1
      nlotnotes += 1
      assert wait_for(timeout: @@job_interval) {@@sv.lot_notes(lot).size == nlotnotes}
      note = @@sv.lot_notes(lot).first
      assert_equal '20', note.desc
      # ship lot, move to label bank and split
      assert_equal 0, @@sv.lot_nonprobankout(lot)
      assert_equal 0, @@sv.lot_opelocate(lot, :last)
      @@sv.lot_bankin(lot) if @@sv.lot_info(lot).status != 'COMPLETED'
      assert_equal 'COMPLETED', @@sv.lot_info(lot).status
      assert_equal 0, @@sv.lot_bank_move(lot, @@bank_label)
      assert lc = @@sv.lot_split(lot, 5), 'error splitting lot'
      # no new note because lot is COMPLETED,  v 14.1
      $log.info "waiting #{@@job_interval} s for the job to run"; sleep @@job_interval
      assert_equal nlotnotes, @@sv.lot_notes(lot).size, 'wrong number of lot notes'
    }
  end

  def test42_preliminary_virtual_no
    # dont ship lots in virtual carriers on the label printing bank, MSR 853256
    skip 'not applicable in f8stag' if $env == 'f8stag'
    assert lot = @@svtest.new_lot(carrier: '9%'), 'error creating test lot'
    @@testlots << lot
    assert_equal 0, @@sv.lot_nonprobankin(lot, @@bank_label)
    # check for no new note (1st note is created by STBMfgAttributes)
    $log.info "waiting #{@@job_interval} s for the job to run"; sleep @@job_interval
    assert_equal 1, @@sv.lot_notes(lot).size, 'wrong lot note'
  end

  # like test41 but with trigger instead of poll
  def test43_prepackage_trigger
    skip 'not applicable in f8stag' if $env == 'f8stag'
    assert lot = @@svtest.new_lot, 'error creating test lot'
    @@testlots << lot
    assert_equal 0, @@sv.lot_nonprobankin(lot, @@bank_label)
    # 1 lot
    assert @@prepackage.trigger(lot), 'error in prepackage trigger'
    assert note = @@sv.lot_notes(lot).first, 'no lot note'
    assert_equal 'LotCompletion.LabelPrintWfrCount', note.title
    assert_equal '25', note.desc
    # multiple lots
    assert lcs = 5.times.collect {@@sv.lot_split(lot, 1, onhold: true)}
    # assert @@prepackage.trigger(lcs.take(4)), 'error in prepackage trigger'
    # no assert, ERP may be not up and running
    @@prepackage.trigger(lcs.take(4))
    #
    $log.info 'verifying lot notes (triggerSendLotDataToERPResponse errors are ignored)'
    lcs.take(4).each {|lot|
      assert note = @@sv.lot_notes(lot).first, 'no Lot Note'
      assert_equal 'LotCompletion.LabelPrintWfrCount', note.title
      assert_equal '1', note.desc
    }
    # parent gets more notes (number depending on job speed), last one with '20'
    assert wait_for(timeout: @@job_interval) {
      @@sv.lot_notes(lot).first.desc == '20'
    }, 'wrong latest note on parent lot'
    # last child has one note inherited at split and a new one with correct number of wafers
    assert wait_for {@@sv.lot_notes(lcs.last).first.desc == '1'}, 'wrong latest note on child lot'
  end

  # relies on messages received in test33
  def testman44_msg_properties
    mprops = @@prepackage.mq_response.msg_properties
    assert_equal 1, mprops[:delivery_mode], 'message must have WMQConstants::DELIVERY_NOT_PERSISTENT'
    assert mprops[:expiration] < Time.now + 691200, 'expiration time is more than 8 days from now'
    assert mprops[:expiration] > Time.now + 86400, 'expiration time is less than 1 day'
  end

end
