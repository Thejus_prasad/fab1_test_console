=begin 
(c) Copyright 2011 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.
    
Author: Daniel Steger, 2011-05-16 (cleanup 2013-05-15)
        refactored by Adel Darwaish, 2012-06-15
   
Note: undeployed in Fab1 in 2013        
=end

require "RubyTestCase"

# Test jCAP PilotSplit job

class Test_jCAP_PilotSplit < RubyTestCase

  Description = "Test jCAP PilotSplit job"

  @@jcap_interval = 310
  
  @@lots = ["U94UG.00", "U94UH.00", "U94UJ.00", "U94UK.00", "U94UL.00"]
  
  @@current_product = "3260CH.00"

  @@route = "ITDC-JCAP-TEST.01"
  
  @@opNoStart = "6000.1000"
  @@opNoNext  = "6000.1200"
  @@stocker = "UTSTO11"
  @@hold_reason_MGHL = "MGHL"

  def setup
    super
    
    $setup_ok = ($setup_ok != false)
    assert $setup_ok, "test setup is not correct"
  end
  
  def test01_prepare
    $setup_ok = false
    if $env == "f8stag"
      @@current_product = "ITDC-JCAP-TEST.01"
      @@route = "ITDC-JCAP-TEST.01"
      @@lots = ["8XYK20045.000", "8XYK20046.000", "8XYK20047.000", "8XYK20048.000", "8XYK20049.000"]
      @@stocker = "UTSTO110"
    end
    
    assert Test_jCAP_PilotSplit.cleanup, "clean up failed"    
    
    #
    # lot 1 (@@lots[0]: normal split creteria
    assert_equal 0, $sv.lot_opelocate(@@lots[0], @@opNoStart)
    script = "#{@@route}_#{@@opNoStart}_1_#{@@lots[0]}_AutoPilot"  
    # lot must contain at least 2 wafers
    li = $sv.lot_info(@@lots[0], :wafers=>true)
    assert li.nwafers.to_i > 2    
    li.wafers.take(2).each do |w|  
      assert_equal 0, $sv.user_parameter_change("Wafer", w.wafer, "MarkSplit",script), "failed to set script parameter for wafer #{w.wafer}"
    end
    # set lot X-PL ONHOLD 
    assert_equal 0, $sv.lot_hold(@@lots[0], "X-PL"), "failed to set lot #{@@lots[0]} ONHOLD, reason: X-PL"
    
    #
    # lot 2 (@@lots[1]: normal split no Hold
    assert_equal 0, $sv.lot_opelocate(@@lots[1], @@opNoStart)
    script = "#{@@route}_#{@@opNoStart}_1_#{@@lots[1]}_AutoPilot"  
    # lot must contain at least 2 wafers
    li = $sv.lot_info(@@lots[1], :wafers=>true)
    assert li.nwafers.to_i > 2    
    li.wafers.take(2).each do |w|  
      assert_equal 0, $sv.user_parameter_change("Wafer", w.wafer, "MarkSplit",script), "failed to set script parameter for wafer #{w.wafer}"
    end
    
    #
    # lot 3 (@@lots[2]: normal split creteria with second hold
    assert_equal 0, $sv.lot_opelocate(@@lots[2], @@opNoStart)
    script = "#{@@route}_#{@@opNoStart}_1_#{@@lots[2]}_AutoPilot"  
    # lot must contain at least 2 wafers
    li = $sv.lot_info(@@lots[2], :wafers=>true)
    assert li.nwafers.to_i > 2    
    li.wafers.take(2).each do |w|  
      assert_equal 0, $sv.user_parameter_change("Wafer", w.wafer, "MarkSplit",script), "failed to set script parameter for wafer #{w.wafer}"
    end
    # set lot X-PL ONHOLD 
    assert_equal 0, $sv.lot_hold(@@lots[2], "X-PL"), "failed to set lot #{@@lots[2]} ONHOLD, reason: X-PL"
    # set lot X-PL ONHOLD 
    assert_equal 0, $sv.lot_hold(@@lots[2], "ENG"), "failed to set lot #{@@lots[2]} ONHOLD, reason: ENG"
    
    #
    # lot 4 (@@lots[3]: wrong script parameter (invalid OpNo)
    assert_equal 0, $sv.lot_opelocate(@@lots[3], @@opNoStart)
    script = "#{@@route}_#{@@opNoNext}_1_#{@@lots[3]}_AutoPilot"  
    # lot must contain at least 2 wafers
    li = $sv.lot_info(@@lots[3], :wafers=>true)
    assert li.nwafers.to_i > 2    
    li.wafers.take(2).each do |w|  
      assert_equal 0, $sv.user_parameter_change("Wafer", w.wafer, "MarkSplit",script), "failed to set script parameter for wafer #{w.wafer}"
    end
    # set lot X-PL ONHOLD 
    assert_equal 0, $sv.lot_hold(@@lots[3], "X-PL"), "failed to set lot #{@@lots[3]} ONHOLD, reason: X-PL"    
    
    #    
    # wait for jCAP job
    $log.info "waiting #{@@jcap_interval}s for the jCAP job to run"
    sleep @@jcap_interval
    
    $setup_ok = true
  end
  
  def test101_APC_pilot_split
    #lot1 = @@lots[0]
    lots = $sv.lot_family(@@lots[0])
    assert_equal 2, lots.size, "lot was not split #{lots.inspect}"
    child = lots - [@@lots[0]]
    fhl = $sv.lot_futurehold_list(@@lots[0], :reason=>@@hold_reason_MGHL)
    assert_equal 1, fhl.size, "wrong future holds for lot: #{@@lots[0]}"
    fhl = $sv.lot_futurehold_list(child[0], :reason=>@@hold_reason_MGHL)
    assert_equal 1, fhl.size, "wrong future holds for child lot: #{child}"
  end

  def test102_APC_pilot_split_no_hold
    #lot1 = @@lots[0]
    lots = $sv.lot_family(@@lots[1])    
    assert_equal 1, lots.size, "lot was split #{lots.inspect}"
  end
  
  def test103_APC_pilot_split_second_hold
    #lot1 = @@lots[0]
    lots = $sv.lot_family(@@lots[2])    
    assert_equal 1, lots.size, "lot was split #{lots.inspect}"
  end
  
  def test104_APC_pilot_split_wrong_script_parameter
    #lot1 = @@lots[0]
    lots = $sv.lot_family(@@lots[3])    
    assert_equal 1, lots.size, "lot was split #{lots.inspect}"
  end    
  
  def self.cleanup
    ret = true
    $log.info ""    
    $log.info "start cleaning up lots..................................................."    
    #set all lots to the start default PD    
    # ensure lots are clean and not on hold
    @@lots.each {|l|
      li = $sv.lot_info(l)
      $sv.lot_nonprobankout(l) if li.status == "NonProBank"
      # set carrier state to EO on equipment
      $sv.carrier_xfer_status_change(li.carrier, "EO", li.eqp) if li.xfer_status == "EI"
      li = $sv.lot_info(l)      
      # set carrier state to MI on stocker
      ret &= $sv.carrier_xfer_status_change(li.carrier, "MI", @@stocker)
      # 
      ret &= $sv.carrier_status_change(li.carrier, "AVAILABLE", :check=>true)      
      #
      $sv.lot_family(l).each{|lot|
        # release holds, including holds from jCAP jobs for all family members
        res = $sv.lot_hold_release(lot, nil)
        $log.info "  lot_hold_release #{lot}: #{res}"
        #cancel futureholds, including futurehold from jCAP jobs for all family members
        res = $sv.lot_futurehold_cancel(lot, nil)
        $log.info "  lot_futurehold_cancel #{lot}: #{res}"            
        li2 = $sv.lot_info(lot)
        # reset product
        if(li2.product != @@current_product)        
          res = $sv.schdl_change(lot, :product=>@@current_product, :route=>@@route) 
          $log.info "  reset product for #{lot} to #{@@current_product} : #{res}"    
        end        
        # cancel existing sjs
        if(li2.sjexists)
          sjl = $sv.sj_list(lot)
          sjl.each{|sj|
            $sv.sj_status_change(sj.sorter, sj.jobid, "Error")
            $sv.sj_cancel(sj.jobid)
          }        
        end        
      }            
      ret &= $sv.merge_lot_family(l)
      ret &= $sv.lot_experiment_delete(l)      
      # move lots to current point PD
      ret &= $sv.lot_opelocate(l, @@opNoStart)          
      $log.warn "RET: #{ret}, current lot: #{l}" unless ret      
      return ret unless ret
    }
    $log.warn "RET: #{ret}" unless ret
    
    $log.info ""    
    $log.info "finished cleaning up lots..................................................."    
    
    return ret
    
  end  
end
  

