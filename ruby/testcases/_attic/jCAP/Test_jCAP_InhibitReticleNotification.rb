=begin 
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.
    
Author: Conny Stenzel, 2011-01-13

History:
  2015-05-26 adarwais,	cleaned up and verified test cases with R15
=end

require 'RubyTestCase'
require 'derdack'

# Test the InhibitReticleNotification jCAP job
class Test_jCAP_InhibitReticleNotification < RubyTestCase  
  @@jcap_interval = 905
  @@latency_interval = 600
  @@reticle = '2110AQ2NBB2' #nil    # must not use the reticle from Test_jCAP_InhibitReticle
  @@inhibit_reason = 'X-RD'
  @@eqp = 'ALC300'    # only for correct transfer status, not used directly
  
  def self.after_all_passed
    $sv.inhibit_cancel :reticle, @@reticle
    $sv.inhibit_cancel :reticle_eqp, [@@reticle, @@eqp]
  end
  
  def test00_setup
    $setup_ok = false
    #
    $derdack = Derdack::EventDB.new($env)
    assert $derdack.db.connected?, "no connection to Derdack database"
    # identify a reticle
    unless @@reticle
      reticles = $sv.reticle_list
      reticles.each {|r|
        if $sv.inhibit_list('Reticle', r) == []
          @@reticle = r
          break
        end
      }
    end
    assert @@reticle, "no eligible reticle found"
    $log.info "using reticle #{@@reticle}"
    # reticle cleanup
    $sv.inhibit_cancel(:reticle, @@reticle)
    $sv.inhibit_cancel(:reticle_eqp, @@reticle)
    #
    $setup_ok = true
  end
  
  def test11_inhibit
    # register inhibit
    assert_equal [], $sv.inhibit_list(:reticle, @@reticle), "reticle already has inhibits"
    assert_equal 0, $sv.inhibit_entity(:reticle, @@reticle, reason: @@inhibit_reason, memo: 'QA test 1'), "error inhibiting reticle"
    @@notification_time = Time.now
    t = @@jcap_interval + @@latency_interval
    $log.info "sleeping #{t}s for the Derdack message to appear"; sleep t
    # verify notification
    notifications = $derdack.get_events(@@notification_time, nil, 'ReticleID'=>@@reticle)
    assert_equal 1, notifications.size, "notification events are not unique"
    nparams = notifications[0].params
    assert_equal 'ReticleInhibit', nparams['Category']
    assert_equal @@inhibit_reason, nparams['ReasonCode']
  end
  
  def test12_no_new_notification
    @@notification_time = Time.now
    $log.info "sleeping #{@@jcap_interval}s for the jCAP job"; sleep @@jcap_interval
    # verify that no further notifications have been sent
    notifications = $derdack.get_events(@@notification_time, nil, 'ReticleID'=>@@reticle)
    assert_equal [], notifications, "should not send a new notification"
  end
  
  def test13_new_inhibit
    # cancel current inhibit
    $sv.inhibit_cancel 'Reticle', @@reticle
    assert_equal [], $sv.inhibit_list(:reticle, @@reticle), "reticle is still inhibited"
    # wait jCAP interval to forget this reticle
    $log.info "waiting #{@@jcap_interval}s for the jCAP job to forget the reticle"; sleep @@jcap_interval
    # register a new inhibit
    assert_equal [], $sv.inhibit_list(:reticle, @@reticle), "reticle already has inhibits"
    #
    memo = 'QA test 2'
    assert_equal 0, $sv.inhibit_entity(:reticle, @@reticle, reason: @@inhibit_reason, memo: memo), "error inhibiting reticle"
    @@notification_time = Time.now
    t = @@jcap_interval + @@latency_interval
    $log.info "waiting #{t}s for the Derdack message to appear"; sleep t
    # verify notification
    notifications = $derdack.get_events(@@notification_time, nil, 'ReticleID'=>@@reticle)
    assert_equal 1, notifications.size, "notification events are not unique"
    nparams = notifications[0].params
    assert_equal 'ReticleInhibit', nparams['Category']
    assert_equal @@inhibit_reason, nparams['ReasonCode']
  end

  def test14_new_inhibit_combination_reticleid_and_equipid
    # cancel current inhibit
    $sv.inhibit_cancel :reticle, @@reticle
    assert_equal [], $sv.inhibit_list(:reticle, @@reticle), "reticle is still inhibited"
    # wait jCAP interval to forget this reticle
    $log.info "waiting #{@@jcap_interval}s for the jCAP job to forget the reticle"; sleep @@jcap_interval
    # ensure there is no reticle inhibit
    assert_equal [], $sv.inhibit_list(:reticle, @@reticle), "reticle already has inhibits"
    # register a new inhibit
    assert_equal 0, $sv.inhibit_entity(:reticle_eqp, [@@reticle, @@eqp], reason: @@inhibit_reason, memo: "QA"), "error setting inhibit"
    @@notification_time = Time.now
    t = @@jcap_interval + @@latency_interval
    $log.info "waiting #{t}s for the Derdack message to appear"; sleep t
    # verify notification
    notifications = $derdack.get_events(@@notification_time, nil, 'ReticleID'=>@@reticle)
    assert_equal [], notifications, "notification events are not unique"
  end
  
end
