=begin 
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.
    
Author: Steffen Steidten, 2014-05-13

Version: 14.12

History:
=end

require 'RubyTestCase'
require 'jcap/aqvtest'


# Performance test for the jCAP AQV job
class Test_jCAP_AQV1 < RubyTestCase
  @@spec = "C07-00000766"
  @@sorter = "UTS001"
  @@stocker = "QA-STO-M1"  
  
  @@timeout = 120
  @@perf_count = 1000

  
  def setup
    $aqvtest.sap.mq_request.delete_msgs(nil) if $aqvtest
  end
  
  def test00_setup
    $setup_ok = false
    $aqvtest = AQV::Test.new($env, sv: $sv, sorter: @@sorter, stocker: @@stocker, timeout: @@timeout)
    # cleanup queues 
    $aqvtest.wbtqtest.client.mq_response.delete_msgs(nil)
    # ensure WBTQ is configured to use the test (not SAP) response queue
    res = $aqvtest.wbtqtest.client.stb_trigger('QAQA', timeout: 60)
    assert $aqvtest.wbtqtest.verify_response(res, '200', 'No kit context'), "wrong WBTQ response queue?"
    $setup_ok = true
  end

  def test901_spcevent_perf_success
    lot = 'T50003A.00'
    # pick a valid operation
    assert lop = $aqvtest.mds.aqv_lot_oper(lot: lot)[1], "lot #{lot} has no AQV_LOT_OPER entries"
    assert duration = $aqvtest.spcevent_performance(lot, lop, count: @@perf_count, result: true)
    $log.info "duration for #{@@perf_count} messages: #{duration} s"
  end

  def test902_spcevent_perf_fail
    lot = 'T50003A.00'
    # pick a valid operation
    assert lop = $aqvtest.mds.aqv_lot_oper(lot: lot)[1], "lot #{lot} has no AQV_LOT_OPER entries"
    assert duration = $aqvtest.spcevent_performance(lot, lop, count: @@perf_count, result: false)
    $log.info "duration for #{@@perf_count} messages: #{duration} s"
  end
end
