=begin
(c) Copyright 2011 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.


Author: Steffen Steidten, 2015-04-17

Version: 12.11

History:
  2017-06-01 sfrieske, minor fixes
=end

require 'SiViewTestCase'
require 'remote'


# Test the Lingering Transport Jobs jCAP job, # note: the job starts at 9:00!!
class Test_jCAP_LingeringTransportJobs < SiViewTestCase
  @@stocker2 = 'UTSTO12'
  @@resultfile = '/reports/Control_Files/MACS_LIST'
  @@header = 'job_id|job_status|transport_type|carrier_job_id|carrier_job_status|carrier_id|zone_type|from_machine_id|from_port_id|to_stocker_group|to_machine_id|to_port_id|AMHS_Bay priority|estimate_strt_time|estimate_end_time|start'


  @@job_interval = 320


  def self.startup
    super
    @@jcapjob = JCAPJob.new($env, 'lingeringtransportjobs')
    $dummyamhs = Dummy::AMHS.new($env)
    @@carrier = nil
  end

  def self.shutdown
    $sv.carrier_xfer_jobs_delete(@@carrier) if @@carrier
    $dummyamhs.set_variable('intrabaytime', 15000)
  end



  def test10_formal_check
    $setup_ok = false
    #
    # verify the job writes files with every run
    mtime = @@jcapjob.srv.file_mtime(@@resultfile)
    assert mtime + @@job_interval > Time.now, "server not running, resultfile too old: #{mtime}"
    # get the current file check formal correctness
    assert s = @@jcapjob.srv.read_file(@@resultfile)
    lines = s.split("\n")
    assert_equal @@header, lines.first, "wrong header"
    assert lines.size > 1, "if data is missing there must be a line like '|||||..."
    assert lines.first.include?('|'), "wrong format"
    #
    $setup_ok = true
  end

  def test11_xferjob
    # ensure carrier xfer job duration is long enough
    assert $dummyamhs.set_variable('intrabaytime', @@job_interval * 2 * 1000) # in ms
    # get an empty carrier create an xfer job and wait for the jCAP job
    assert @@carrier = $svtest.get_empty_carrier, "no empty carrier"
    assert $svtest.cleanup_carrier(@@carrier, status: 'NOTAVAILABLE')
    assert_equal 0, $sv.carrier_xfer(@@carrier, $svtest.stocker, '', @@stocker2, '')
    $log.info "waiting #{@@job_interval} s for the jCAP job"; sleep @@job_interval
    #
    mtime = @@jcapjob.srv.file_mtime(@@resultfile)
    assert mtime + @@job_interval > Time.now, "server not running, resultfile too old: #{mtime}"
    assert s = @@jcapjob.srv.read_file(@@resultfile)
    assert line = s.split("\n").find {|l| l.include?(@@carrier)}, "no xfer job with carrier #{@@carrier}"
    # verify key fields
    ff = line.split('|')
    assert_equal 16, ff.size
    assert_equal @@carrier, ff[5]
    assert_equal $svtest.stocker, ff[7]
    assert_equal @@stocker2, ff[10]
    assert_equal mtime, Time.parse(ff.last.split.last + 'Z')
  end
end
