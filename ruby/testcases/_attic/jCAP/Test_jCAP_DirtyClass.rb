=begin 
Test the jCAP DirtyFoupTransfer job.

To make the tests easy there are 23 empty carriers of the category UTFEOL,
20 empty UTBEOL carriers and 3 UTBEOL carriers with lots and
20 empty UTMOL carriers and 3 UTMOL carriers with lots.
45 empty UTDIRTY3 carriers and 5 UTDIRTY3 carriers with lots

The lots in the UTBEOL carriers must be of product "UT-DIRTY_FOUP.01" on the
route "UTRT-DIRTY-FOUP.01" with Required Carrier Category UTBEOL at the first operation.
The second operation must have the UDATA "AutomationType" NOFOUPOPEN

Create lots (example): 
  $sv.new_lot_release product: "UT-DIRTY-FOUP.01", route: "UTRT-DIRTY-FOUP.01", stb: true, carrier: "AZ1%"


(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.
    
Authors: Daniel Steger, 2013-01-10
=end

require "SiViewTestCase"
require 'dirtytest'

# Test the jCAP DirtyFoupTransfer job.
# classification rules of Fab8
class Test_jCAP_DirtyClass < SiViewTestCase
        
  # jCAP job interval is 60 seconds, SiView -> MDS sync may need more time
  @@inuse_interval = 80
  
  # time interval in seconds to be used shortly before jCAP job starts to set candidate carriers to INUSE for auxialiary manual test.
  @@job_offset_sleeptime = "2"
  
  # jCAP report update interval in seconds. 
  @@report_interval = 610  # prod value: "2700", takes longer as configured if there are many carriers
    
  @@pm_interval = 20
  
  # configurable job parameters
  # max number of INUSE empty carriers  
  @@jcap_max_inuse = 12
  # and for protocol zone
  @@jcap_max_inuse_utdirty3 = 5
  
  # max number of SJR not empty carriers 
  @@jcap_max_SJR = 3
  
  # minimum number of carriers per category that must stay AVAILABLE
  @@jcap_min_avail = 20
  
  # number of seconds a carrier becomes a candidate before PM is due
  @@trigger_time_before_need = 10080   # 7 days
  
  #inquire sj request history / Not used right now
  @@sjc_from_history = false

  @@use_route = "PB-CUPLT-QUAL.01"
  @@use_product = "PB-MPX265X-QUAL-CELL3.01"
  @@rework_route = "UTRW001.01"

  # QTime operation
  @@opNo_qtime = "2000.1000"
  @@opNo_nofoupopen = "2000.1500"

  @@proc_eqp = "UTC001"

  # Required category != carrier category
  @@opNo_wrong_cat = '2000.2000'

  @@bank_wrong_cat = "UT-RAW"
  @@bank_cat = "UT-SHIP"
  @@nonprobank = "UT-NONPROD"

  @@opNo_empy_cat = '2000.3000'
  @@opNo_matching_cat = '2000.1000'

  @@product = "UT-DIRTY-FOUP.01"
  @@route = "UTRT-DIRTY-FOUP.01"
  @@product_noclean = "UT-DIRTY-FOUP.02"

  @@opNo_utfeol = '1000.600'

  @@jcap_max_sjr_utmol = 4
  
  @@lots =
  {  1 => ["8XYK03001.001", nil],  # Lot on rework route - no classification
     2 => ["8XYK03001.002", "Q"],  # Lot on use route, not longer than 7 days
     3 => ["8XYK03001.003", "Q"],  # One lot in carrier is rocket
     4 => ["8XYK03001.004", "Q"],  # One lot in carrier is super rocket 
     5 => ["8XYK03001.005", "Q"],  # One lot in carrier has QTime
     6 => ["8XYK03001.006", "Q"],  # Lot is on PD with AutomationType UDATA contains "NOFOUPOPEN"
     7 => ["8XYK03001.007", "WSR"],# One waferid in carrier starts with "WSR"
     8 => ["8XYK03001.008", "NO"], # Lot is scrapped, no Q-criteria
     9 => ["8XYK03001.009", "NO"], # Lot is InProcessing (early foup removal)
    10 => ["8XYK03001.00A", "NO"], # Lot is ONHOLD with X-JC
    11 => ["8XYK03001.00B", "NO"], # Required category is not empty and does not match carrier category
    12 => ["8XYK03001.00C", "NO"], # Lot is in bank and bank carrier category does not match carrier category
    13 => ["8XYK03001.00D", "NO"], # Product has UDATA Sorter Automation -CLEAN
    14 => ["T205WFC.00", "SJ"],    # One lot in carrier is on a use route (longer than 7 days), should not be moved!!
    15 => ["8XYK03001.00E", "SJ"], # Lot is ONHOLD with ENG
    16 => ["8XYK03001.00F", "SJ"], # Lot is in non pro bank and bank carrier category matches carrier category
    17 => ["8XYK03001.00G", "SJ"], # Lot is COMPLETED and bank carrier category matches carrier category
    18 => ["8XYK03001.00H", "SJ"], # Lot is waiting on operation with empty required category, NO in Fab1
    19 => ["8XYK03001.00K", "SJ"], # Lot is waiting on operation with required category and matches carrier category
    20 => ["8XYK03001.000", "SJ"], # Lot is waiting on operation with required category and matches carrier category    
    21 => ["8XYK03002.000", "WSR"],# One waferid in carrier starts with "WSR"
    22 => ["8XYK03002.001", "WSR"] # One waferid in carrier starts with "WSR"
  }
    
  # Note: first lot is not moved, only 2nd lot is moved
  @@multilots = 
  {
    1 => [3,7, "Q"],     # Rocket lot  beats WSR
    2 => [4,8, "Q"],     # Super Rocket lot and scrapped
    3 => [17,5, "Q"],    # QTime beats SJ criteria
    4 => [21,10, "WSR"], # WSR beats NO criteria 
    5 => [22,15, "WSR"], # WSR beats SJ criteria
    6 => [16,11, "NO"],  # NO criteria beats SJ criteria
    7 => [19,20, "SJ"]   # SJ criteria
  }

  def self.startup(params={})
    unless $dirtytest and $dirtytest.env == $env
      $dirtytest = JCAP::DirtyFoupTest.new($env, product: @@product, 
        min_avail: @@jcap_min_avail,
        pm_max: (@@trigger_time_before_need + @@pm_interval),
        report_timeout: @@report_interval, inuse_timeout: @@inuse_interval)
      $sv = $dirtytest.sv
    end
    load_fixtures
    @@testlots = []
  end
  
  def self.create_carriers
    feol = $sv.get_carrierids "ZZ00", 23
    $sv.durable_register(feol, "UTFEOL", usagecheck: true, pm_max: 10, use_max: 4000, run_max: 172800)
    feol = $sv.get_carrierids "ZZ000", 13
    $sv.durable_register(feol, "UTFEOL", usagecheck: true, pm_max: 10000, use_max: 4000, run_max: 172800)
    beol = $sv.get_carrierids "ZZ10", 23
    $sv.durable_register(beol, "UTBEOL", usagecheck: true, pm_max: 10, use_max: 4000, run_max: 172800)
    mol = $sv.get_carrierids "ZZ20", 23
    $sv.durable_register(mol, "UTMOL", usagecheck: true, pm_max: 10, use_max: 4000, run_max: 172800)
    dirt = $sv.get_carrierids "ZZ300", 36
    $sv.durable_register(dirt, "UTDIRTY3", usagecheck: true, pm_max: 10, use_max: 4000, run_max: 172800)
  end

  def self.create_lots
    $sv.new_lot_release product: "UT-DIRTY-FOUP.01", route: "UTRT-DIRTY-FOUP.01", carrier: "ZZ15", stb: true
    $sv.new_lot_release product: "UT-DIRTY-FOUP.01", route: "UTRT-DIRTY-FOUP.01", carrier: "ZZ1G", stb: true
    $sv.new_lot_release product: "UT-DIRTY-FOUP.01", route: "UTRT-DIRTY-FOUP.01", carrier: "ZZ1M", stb: true
    $sv.new_lot_release product: "UT-DIRTY-FOUP.01", route: "UTRT-DIRTY-FOUP.01", carrier: "ZZ2K", stb: true
    $sv.new_lot_release product: "UT-DIRTY-FOUP.01", route: "UTRT-DIRTY-FOUP.01", carrier: "ZZ2P", stb: true
    $sv.new_lot_release product: "UT-DIRTY-FOUP.01", route: "UTRT-DIRTY-FOUP.01", carrier: "ZZ2R", stb: true
    $sv.new_lot_release product: "UT-DIRTY-FOUP.01", route: "UTRT-DIRTY-FOUP.01", carrier: "ZZ301", stb: true
    $sv.new_lot_release product: "UT-DIRTY-FOUP.01", route: "UTRT-DIRTY-FOUP.01", carrier: "ZZ302", stb: true
    $sv.new_lot_release product: "UT-DIRTY-FOUP.01", route: "UTRT-DIRTY-FOUP.01", carrier: "ZZ303", stb: true
    $sv.new_lot_release product: "UT-DIRTY-FOUP.01", route: "UTRT-DIRTY-FOUP.01", carrier: "ZZ304", stb: true
    $sv.new_lot_release product: "UT-DIRTY-FOUP.01", route: "UTRT-DIRTY-FOUP.01", carrier: "ZZ305", stb: true
  end
  
  def self.after_all_passed
    @@testlots.each { |lot| $sv.delete_lot_family(lot) }
  end
  
  def test10_empty_carriers_inuse_by_pmage
    cat = "UTFEOL"
    # Setup
    assert $dirtytest.prepare_general_conditions(cat, @@jcap_max_inuse), "Preconditions not fulfilled."
    $log.info "All UTFEOL empty foups are clean."

    # Test
    t_sleep_before_dirty = ((@@pm_interval+1)*60 - @@report_interval)
    $log.info "Additional time to wait for foups becoming dirty: #{t_sleep_before_dirty/60} min"
    sleep t_sleep_before_dirty

    $log.info "waiting #{@@report_interval/60} minutes for the dirty foup report update"
    sleep @@report_interval.to_i

    assert $dirtytest.verify_inuse_carriers(cat, @@jcap_max_inuse)

    c_all = $sv.carrier_list(category: cat)
    assert $dirtytest.verify_dirty_foup_report(cat, @@lots, c_all), "wrong classification"

    # Additional test
    # Reset inuse carriers and wait for inuse job
    carriers = $sv.carrier_list(category: cat, sjcheck: false, carrierinfo: true)
    carriers.each {|cs|      
      $sv.carrier_status_change(cs.carrier, "AVAILABLE") unless cs.status == "AVAILABLE"
    }
    $log.info "waiting #{@@inuse_interval/60} minutes for the dirty foup inuse"
    sleep @@inuse_interval

    assert $dirtytest.verify_inuse_carriers(cat, @@jcap_max_inuse)
  end

  def test11_empty_carriers_inuse_by_with_opestart
    cat = "UTFEOL"
    carrier = "ZZ001"
    # Setup
    assert $dirtytest.prepare_general_conditions(cat, @@jcap_max_inuse, opestart: [carrier]), "Preconditions not fulfilled."
    $log.info "All UTFEOL empty foups are clean."

    $sv.carrier_status(carrier).lots.each { |lot| $sv.delete_lot_family(lot)} # cleanup carrier first
    # process
    assert lot = $dirtytest.new_lot(carrier: carrier, no_cache: true), "failed to get new test lot"
    2.times { assert process_lot_in_carrier(carrier, @@opNo_utfeol, lot) }
    assert_equal(2, $sv.carrier_status(carrier).starts, "invalid opestart count for #{carrier}")
    assert_equal 0, $sv.delete_lot_family(lot)
    assert $sv.carrier_xfer_status_recover(carrier, $dirtytest.stocker), "failed to stockin carrier" # need to stockin again
    
    # Test
    t_sleep_before_dirty = ((@@pm_interval+1)*60 - @@report_interval)
    $log.info "Additional time to wait for foups becoming dirty: #{t_sleep_before_dirty/60} min"
    sleep t_sleep_before_dirty

    $log.info "waiting #{@@report_interval/60} minutes for the dirty foup report update"
    sleep @@report_interval.to_i

    assert $dirtytest.verify_inuse_carriers(cat, @@jcap_max_inuse)

    c_subset = $sv.carrier_list(category: cat).take(@@jcap_max_inuse)
    assert $dirtytest.verify_dirty_foup_report(cat, @@lots, c_subset), "wrong classification"

    # Additional test
    # Reset inuse carriers and wait for inuse job
    carriers = $sv.carrier_list(category: cat, sjcheck: false, carrierinfo: true)
    carriers.each {|cs|      
      $sv.carrier_status_change(cs.carrier, "AVAILABLE") unless cs.status == "AVAILABLE"
    }
    $log.info "waiting #{@@inuse_interval/60} minutes for the dirty foup inuse"
    sleep @@inuse_interval

    assert $dirtytest.verify_inuse_carriers(cat, @@jcap_max_inuse)
  end
  
  def test20_carrier_classification_single_lot
    cat = "UTDIRTY"
    # Setup
    assert self.class.verify_lot_conditions(cat), "failed to prepare lots"
    assert $dirtytest.prepare_general_conditions(cat, @@jcap_max_inuse, lots: @@lots, excluded_carriers: ["ADFT29"]), "Preconditions not fulfilled."
    $log.info "All #{cat} foups are clean."

    #assert Test_jCAP_DirtyClass.verify_lot_conditions, "Unable to set correct carrier conditions"

    # Test
    t_sleep_before_dirty = (@@pm_interval*60 - @@report_interval)
    $log.info "Additional time to wait for foups becoming dirty: #{t_sleep_before_dirty/60} min"
    sleep t_sleep_before_dirty

    time_start = Time.now
    $log.info "waiting #{@@report_interval/60} minutes for the dirty foup report update"
    sleep @@report_interval.to_i
    
    c_all = $sv.carrier_list(category: cat, sjcheck: false)
    # filter lot-carriers with 'nil' classification
    c_filtered = c_all - @@lots.each_value.select {|l| l[1] == nil}.collect {|l| $sv.lot_info(l[0]).carrier}

    assert wait_for { $dirtytest.verify_dirty_foup_report(cat, @@lots, c_filtered) }, "wrong classification"
    time_end = Time.now

    # Additional Test for SJ
    #find carriers with SJ
    @@lots.each_value.select {|l| l[1] == "SJ"}.collect {|l| $sv.lot_info(l[0]).carrier}

    lot_sj = @@lots.values.select {|cl| cl[1] == "SJ" }.map {|cl| cl[0]}    
    assert wait_for(timeout: 120) { $dirtytest.verify_sjr(@@jcap_max_SJR, cat, lot_sj, time_start, time_end) }, "Invalid carrier list"
  end

  def test30_carrier_classification_multi_lot
    cat = "UTDIRTY"
    assert self.class.verify_lot_conditions(cat), "failed to prepare lots for classification"
    mlots = @@lots.clone
    # Combine into multiple lot foups
    @@multilots.each_value do |ml|
      ilot0, ilot1, status = ml
      lot0 = @@lots[ilot0][0]
      lot1 = @@lots[ilot1][0]
      mlots[ilot0] = [lot0,status]
      mlots[ilot1] = [lot1,status]

      # Move lot1 to lot0
      $setup_ok &= $sv.wafer_sort_req(lot1, $sv.lot_info(lot0).carrier) == 0
    end

    # Setup
    assert $dirtytest.prepare_general_conditions(cat, @@jcap_max_inuse, lots: mlots, excluded_carriers: ["ADFT29"]), "Preconditions not fulfilled."
    $log.info "All #{cat} foups are clean."

    #assert Test_jCAP_DirtyClass.verify_lot_conditions, "Unable to set correct carrier conditions"

    # Test
    t_sleep_before_dirty = (@@pm_interval*60 - @@report_interval)
    $log.info "Additional time to wait for foups becoming dirty: #{t_sleep_before_dirty/60} min"
    sleep t_sleep_before_dirty

    time_start = Time.now
    $log.info "waiting #{@@report_interval/60} minutes for the dirty foup report update"
    sleep @@report_interval.to_i

    c_all = $sv.carrier_list(category: cat, sjcheck: false)
    # filter lot-carriers with 'nil' classification
    c_filtered = c_all - mlots.each_value.select {|l| l[1] == nil}.collect {|l| $sv.lot_info(l[0]).carrier}

    assert wait_for { $dirtytest.verify_dirty_foup_report(cat, mlots, c_filtered) }, "wrong classification"
    time_end = Time.now

    # Additional Test for SJ
    #find carriers with SJ
    lot_sj = mlots.values.select {|cl| cl[1] == "SJ" }.map {|cl| cl[0]}
    assert $dirtytest.verify_sjr(@@jcap_max_SJR, cat, lot_sj, time_start, time_end), "Invalid carrier list"
  end
  

  def test40_pz_max_inuse_limit
    cat = "UTDIRTY3"
    # Setup
    lots = prepare_lots_incat(4, cat, "SJ")
    min_empty = @@jcap_min_avail + @@jcap_max_inuse_utdirty3
    assert $sv.carrier_list(category: cat, empty: true).count > min_empty, "not enough empty cassettes needed: #{min_empty}"
    
    assert $dirtytest.prepare_general_conditions(cat, @@jcap_max_inuse_utdirty3), "Preconditions not fulfilled."
    $log.info "All #{cat} empty foups are clean."

    # Test
    t_sleep_before_dirty = (@@pm_interval*60 - @@report_interval)
    $log.info "Additional time to wait for foups becoming dirty: #{t_sleep_before_dirty/60} min"
    sleep t_sleep_before_dirty

    time_start = Time.now
    $log.info "waiting #{@@report_interval/60} minutes for the dirty foup report update"
    sleep @@report_interval.to_i
    
    c_all = $sv.carrier_list(category: cat, sjcheck: false)
    assert $dirtytest.verify_dirty_foup_report(cat, lots, c_all), "wrong classification"
    time_end = Time.now

    # Inuse test
    assert $dirtytest.verify_inuse_carriers(cat, @@jcap_max_inuse_utdirty3, maximum: true)

    # Additional Test for SJ / no sort jobs should exist
    assert $dirtytest.verify_sjr(0, cat, {}, time_start, time_end), "Invalid carrier list"
  end
  
  def test50_pz_max_sj_limit
    cat = "UTMOL"
    # Setup
    lots = prepare_lots_incat(5, cat, "SJ")
    max_empty = @@jcap_min_avail + @@jcap_max_inuse - @@jcap_max_sjr_utmol
    assert $sv.carrier_list(category: cat, empty: true).count <= max_empty, "Too many available empty cassettes needed: #{max_empty}"
    
    assert $dirtytest.prepare_general_conditions(cat, @@jcap_max_inuse), "Preconditions not fulfilled."
    $log.info "All #{cat} empty foups are clean."

    # Test
    t_sleep_before_dirty = (@@pm_interval*60 - @@report_interval)
    $log.info "Additional time to wait for foups becoming dirty: #{t_sleep_before_dirty/60} min"
    sleep t_sleep_before_dirty

    time_start = Time.now
    $log.info "waiting #{@@report_interval/60} minutes for the dirty foup report update"
    sleep @@report_interval.to_i
    
    c_all = $sv.carrier_list(category: cat, sjcheck: false)
    assert $dirtytest.verify_dirty_foup_report(cat, lots, c_all), "wrong classification"
    time_end = Time.now

    # Additional Test for max SJ
    lot_sj = lots.values.select {|cl| cl[1] == "SJ" }.map {|cl| cl[0]}
    assert $dirtytest.verify_sjr(@@jcap_max_sjr_utmol, cat, lot_sj, time_start, time_end), "Invalid carrier list"
  end

  def test60_pz_ignored
    cat = "UTBEOL"
    
    # Setup
    lots = prepare_lots_incat(2, cat, "SJ")    
    assert $dirtytest.prepare_general_conditions(cat, @@jcap_max_inuse), "Preconditions not fulfilled."
    $log.info "All #{cat} empty foups are clean."

    # Test
    t_sleep_before_dirty = (@@pm_interval*60 - @@report_interval)
    $log.info "Additional time to wait for foups becoming dirty: #{t_sleep_before_dirty/60} min"
    sleep t_sleep_before_dirty

    time_start = Time.now
    $log.info "waiting #{@@report_interval/60} minutes for the dirty foup report update"
    sleep @@report_interval.to_i

    c_all = $sv.carrier_list(category: cat, sjcheck: false)
    assert $dirtytest.verify_dirty_foup_report(cat, lots, c_all), "wrong classification"
    time_end = Time.now

    # Inuse test
    assert $dirtytest.verify_inuse_carriers(cat, 0)

    # Additional Test for SJ
    assert $dirtytest.verify_sjr(0, cat, [], time_start, time_end), "Invalid carrier list"
  end
   
   
  # Helper method to increase opestart count by processing a lot
  def process_lot_in_carrier(carrier, opNo, lot)
    $sv.lot_opelocate(lot, opNo)
    if $sv.claim_process_lot(lot, carrier: carrier, claim_carrier: true, mode: 'Off-Line-1', notify: false)
      verify_equal 0, $sv.wafer_sort_req(lot, carrier), "failed to process carrier #{carrier}"
    else 
      $log.error "processing failed for #{carrier}"
      return false
    end
  end
  
  def self.verify_lot_conditions(cat)
    c_empty = $sv.carrier_list(category: cat, empty: true)
    c_none_empty = $sv.carrier_list(category: cat, notempty: true)
    c_none_empty.each {|c|
      lots = $sv.carrier_status(c).lots
      # Do not move lot #14
      unless lots.member?(@@lots[14][0])
        # Separate to single carriers
        lots[1..-1].each {|l| $sv.wafer_sort_req l, c_empty.pop}
      end
    }

    # Q Lots:
    #
    # Lot is on rework route
    res = verify_equal(@@rework_route, $sv.lot_info(@@lots[1][0]).route, "Lot #{@@lots[1][0]} should be on rework route")

    # Lot is on use route (<7 days claimed)
    res &= verify_equal(0, $sv.schdl_change(@@lots[2][0], route: @@use_route, product: @@use_product, opNo: "1000.1000"), "Lot #{@@lots[2][0]} should be on use route")

    # Lot is rocket rocket    
    res &= verify_equal(0, $sv.schdl_change(@@lots[3][0], priority: 1), "Lot #{@@lots[3][0]} should be a rocket")

    # Lot is super rocket
    res &= verify_equal(0, $sv.schdl_change(@@lots[4][0], priority: 0), "Lot #{@@lots[4][0]} should be a super rocket")

    # Lot has qtime
    $sv.lot_hold_release(@@lots[5][0], nil)
    $sv.lot_opelocate(@@lots[5][0], @@opNo_qtime)
    $sv.claim_process_lot(@@lots[5][0], claim_carrier: true)
    linfo = $sv.lot_info(@@lots[5][0])
    res &= verify_equal(true, linfo.qtime, "Lot #{@@lots[5][0]} should have qtime")
    $sv.carrier_xfer_status_recover(linfo.carrier, $dirtytest.stocker)

    # PD Udata NOFOUPOPEN
    res &= verify_equal(0, $sv.lot_opelocate(@@lots[6][0], @@opNo_nofoupopen), "Lot #{@@lots[6][0]} should be at PD with NOFOUPOPEN")
    res &= verify_equal("NOFOUPOPEN", $sv.user_data_pd($sv.lot_info(@@lots[6][0]).op)["AutomationType"], "PD should have AutomationType with NOFOUPOPEN")

      # --> All other lots do not fullfill one of these conditions

    # WSR Lot:
    #
    # WaferID with WSR...
    [7,21,22].each {|i|
      wafers = $sv.lot_info(@@lots[i][0], wafers: true).wafers
      res &= verify_condition("#{@@lots[i][0]}: No wafer starting with WSR...") { wafers.select {|w| w.wafer =~ /^WSR/}.count > 0 }
    }
    
    # --> All other lots do not fullfill one of these conditions

    # NO Lots:
    #
    # Scrap lot
    $sv.scrap_wafers(@@lots[8][0])
    res &= verify_equal("SCRAPPED", $sv.lot_info(@@lots[8][0]).status, "Lot #{@@lots[8][0]} should be scrapped")

    # lot is in processing
    $sv.lot_opelocate(@@lots[9][0], @@opNo_qtime)
    $sv.claim_process_lot @@lots[9][0], eqp: @@proc_eqp, earlyfoup: true, mode: "Auto-1", claim_carrier: true
    res &= verify_equal("Processing", $sv.lot_info(@@lots[9][0]).status, "Lot #{@@lots[9][0]} should be in processing")

    # lot is onhold with X-SJC
    $sv.lot_hold_release(@@lots[10][0], nil)
    res &= verify_equal(0, $sv.lot_hold(@@lots[10][0], "X-JC"), "Lot #{@@lots[10][0]} should be onhold with X-SJC")

    # required and carrier category do not match (nonempty)
    res &= verify_equal(0, $sv.lot_opelocate(@@lots[11][0], @@opNo_wrong_cat), "Lot #{@@lots[11][0]} should be at PD with wrong carrier category")

    # required and carrier category do not match (nonempty)
    res &= verify_condition("Bank should not have matching category") { !($sv.user_data_bank(@@bank_wrong_cat)["CarrierCategory"] =~ /UTDIRTY/)}
    res &= verify_equal("COMPLETED", $sv.lot_info(@@lots[12][0]).status, "Lot #{@@lots[12][0]} should be on bank with wrong category")
    $sv.lot_bank_move(@@lots[12][0], @@bank_wrong_cat)
    res &= verify_equal(@@bank_wrong_cat, $sv.lot_info(@@lots[12][0]).bank, "Lot #{@@lots[12][0]} should be on bank with wrong category")

    # Product UDATA with -CLEAN
    res &= verify_equal(0, $sv.schdl_change(@@lots[13][0], product: @@product_noclean, route: @@route), "Lot #{@@lots[13][0]} should be on product with UDATA -CLEAN")
    res &= verify_equal("-CLEAN", $sv.user_data_product(@@product_noclean)["SorterAutomation "], "Lot #{@@lots[13][0]} should be on product with UDATA -CLEAN")

    # SJ Lots:
    #
    # Lot is on use route (>7 days claimed)
    # TDB: This needs to be added to the check
    verify_condition("Lot #{@@lots[14][0]} is not on use route for more than 7 days") {
      linfo = $sv.lot_info(@@lots[14][0])
      (linfo.route == @@use_route) && (Time.now - siview_time(linfo.claim_time) > 3600*24*7)
    }

    # lot is onhold with ENG
    $sv.lot_hold_release(@@lots[15][0], nil)
    $sv.lot_opelocate(@@lots[15][0], @@opNo_matching_cat)
    res &= verify_equal(0, $sv.lot_hold(@@lots[15][0], "ENG"), "Lot #{@@lots[15][0]} should be onhold with ENG")

    # required and carrier category do match / non pro bank
    res &= verify_condition("Bank should have matching category") { !($sv.user_data_bank(@@nonprobank)["CarrierCategory"] =~ /UTDIRTY/)}
    $sv.lot_nonprobankin(@@lots[16][0], @@nonprobank)
    res &= verify_equal(@@nonprobank, $sv.lot_info(@@lots[16][0]).bank, "Lot #{@@lots[16][0]} should be on nonprobank")

    # required and carrier category do not match / bank
    res &= verify_condition("Bank should have matching category") { $sv.user_data_bank(@@bank_cat)["CarrierCategory"] =~ /UTDIRTY/}
    res &= verify_equal("COMPLETED", $sv.lot_info(@@lots[17][0]).status, "Lot #{@@lots[17][0]} should be on bank with category")
    res &= verify_equal(@@bank_cat, $sv.lot_info(@@lots[17][0]).bank, "Lot #{@@lots[17][0]} should be on bank with category")

    # required carrier category is empty
    res &= verify_equal(0, $sv.lot_opelocate(@@lots[18][0], @@opNo_empy_cat), "Lot #{@@lots[18][0]} should be at PD with empty carrier category")

    # required carrier category matches
    res &= verify_equal(0, $sv.lot_opelocate(@@lots[19][0], @@opNo_matching_cat), "Lot #{@@lots[19][0]} should be at PD with matching carrier category")
    res &= verify_equal(0, $sv.lot_opelocate(@@lots[20][0], @@opNo_matching_cat), "Lot #{@@lots[20][0]} should be at PD with matching carrier category")
    res
  end
    
  # helper method to prepare lots for a specific carrier category test
  def prepare_lots_incat(lot_count, ccategory, classification)    
    cat_carriers = $sv.carrier_list(category: ccategory, carrierinfo: true)
    lots = cat_carriers.select { |c| !c.isempty }.map { |c| $sv.lot_list_incassette(c.carrier).first }
    empty_carriers = cat_carriers.select { |c| c.isempty }.take(lot_count - lots.count)
    # if the number of carriers is not sufficient, create lots
    empty_carriers.each do |cs|
      assert lot = $sv.new_lot(product: @@product, route: @@route, carrier: cs.carrier, no_cache: true), "failed to create new lot"      
      lots << lot
    end
    # find an operation with a matching carrier category
    assert ops = $sm.object_info(:mainpd, @@route).first.specific[:operations], "failed to query #{@@route} in SM"
    assert op = ops.find { |op| op.carrier_category == ccategory && $sv.user_data_pd(op.op)['AutomationType'] !~ /NOFOUPOPEN/ }, "failed to find operation for #{ccategory}"
    assert_equal lot_count, lots.count, "not enough test lots"
    lot_hash = {}
    lots.each_with_index do |lot, i|
      assert_ok $sv.lot_cleanup(lot, opNo: op.opNo)
      lot_hash[i] = [lot, classification]
    end
    $log.info "#{lot_hash.inspect}"
    lot_hash
  end
end
