=begin
(c) Copyright 2011 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: Daniel Steger, 2011-07-29

History:
  2013-03-04 ssteidte,  refactored for better testability
  2013-05-16 dsteger, improved error handling added tests from old plan
=end

require "RubyTestCase"
require "rsm"
##require "rexml/document"

# Test jCAP MDSEvent2MQ to RMS
#
# Ensure gfsvevd has been stopped by an admin.
#
# !!! This test will wipe out any other messages on the given MQ queue !!!

class Test_jCAP_MDSEvent2MQBridge < RubyTestCase
  Description = "Test jCAP MDSEvent2MQBridge for RSM"

  @@reticle = "UTMDSEVENTRETICLE5"
  @@reticle2 = "UTMDSEVENTRETICLE4"
  @@reticlegroup = "UTMDSEVENTRETICLEGROUP"

  @@rpod1 = "UTR8"
  @@rpod2 = "UTR6"
  @@rpod_new = "UTRM"

  @@eqp = "UTR002"
  @@barereticlestocker = "UTBRS120"
  @@rport = "RP1"
  @@counter = "DefScanCtDown"
  @@count = 100

  @@mds_waittime = 40

  def setup
    super
    $rsm = RSM::Test.new($env, :sv=>$sv, :read_delay=>@@mds_waittime) unless $rsm and $rsm.env == $env
    $smf = $rsm.smfacade
    $rsm.q.delete_msgs(nil)
  end

  def test01_setup
    $setup_ok = false
    # cleanup any old data
    [@@rpod1, @@rpod2].each {|pod| $rsm.cleanup(pod)}
    # create reticles from scratch
    [@@reticle, @@reticle2].each {|rcl| assert $rsm.cleanup_reticle(rcl)}
    assert $smf.delete(@@reticle)
    assert $smf.delete(@@reticle2)
    assert $smf.create(@@reticle, @@reticlegroup)
    assert $smf.create(@@reticle2, @@reticlegroup)
    $sv.eqp_mode_change(@@eqp, 'Off-Line-1', notify: false)
    $sv.reticle_pod_delete(@@rpod_new)
    $rsm.read_msgs
    $setup_ok = true
  end

  def test11_reticle_status
    $setup_ok = false
    # preparation
    [@@reticle, @@reticle2].each {|rcl| $rsm.cleanup_reticle(rcl)}
    $smf.delete(@@reticle, @@reticlegroup)
    $smf.delete(@@reticle2, @@reticlegroup)
    assert !$smf.reticle_exists?(@@reticle)
    assert !$smf.reticle_exists?(@@reticle2)
    assert !$smf.reticlegroup_exists?(@@reticlegroup)
    $rsm.read_msgs
    # create
    assert $smf.create(@@reticle, @@reticlegroup)
    assert $rsm.read_verify('I', 'RETICLE', @@reticle)
    assert $smf.create(@@reticle2, @@reticlegroup)
    assert $rsm.read_verify('I', 'RETICLE', @@reticle2)
    # update status
    assert_equal 0, $sv.reticle_status_change(@@reticle, "AVAILABLE")
    assert $rsm.read_verify('U', 'RETICLE', @@reticle, nil, $sv.user)
    assert_equal 0, $sv.reticle_status_change(@@reticle, "NOTAVAILABLE")
    assert $rsm.read_verify('U', 'RETICLE', @@reticle, nil, $sv.user)
    assert_equal 0, $sv.reticle_status_change(@@reticle, "AVAILABLE")
    assert $rsm.read_verify('U', 'RETICLE', @@reticle, nil, $sv.user)
    # delete
    assert $smf.delete(@@reticle2)
    assert $rsm.read_verify('D', 'RETICLE', @@reticle2)
    # re-create for later tests
    assert $smf.create(@@reticle, @@reticlegroup)
    assert $smf.create(@@reticle2, @@reticlegroup)
    $setup_ok = true
  end

  def test12_reticle_inout
    # reticle just in, creates 2 messages
    assert_equal 0, $sv.reticle_justinout(@@reticle, @@rpod1, 1, "in")
    $rsm.read_msgs
    assert $rsm.verify_msg('U', 'RETICLEPOD', @@rpod1, nil, $sv.user)
    assert $rsm.verify_msg('U', 'RETICLE', @@reticle, nil, $sv.user)
    # reticle sort, creates 3 messages
    assert_equal 0, $sv.reticle_sort(@@reticle, @@rpod2)
    $rsm.read_msgs
    assert $rsm.verify_msg('U', 'RETICLEPOD', @@rpod1, nil, $sv.user)
    assert $rsm.verify_msg('U', 'RETICLEPOD', @@rpod2, nil, $sv.user)
    assert $rsm.verify_msg('U', 'RETICLE', @@reticle, nil, $sv.user)
    # reticle just out, creates 2 messages
    assert_equal 0, $sv.reticle_justinout(@@reticle, @@rpod2, 1, "out")
    $rsm.read_msgs
    assert $rsm.verify_msg('U', 'RETICLEPOD', @@rpod2, nil, $sv.user)
    assert $rsm.verify_msg('U', 'RETICLE', @@reticle, nil, $sv.user)
  end

  def test21_reticle_pod_create_delete
    return unless $env == "f8stag"
    $sv.reticle_pod_register(@@rpod_new, "MAIN")
    assert $rsm.read_verify('I', 'RETICLEPOD', @@rpod_new)
    assert_equal 0, $sv.reticle_pod_delete(@@rpod_new)
    assert $rsm.read_verify('D', 'RETICLEPOD', @@rpod_new)
  end

  def test22_reticle_pod_status_change
    # reticle pod status change including substates
    assert_equal 0, $sv.reticle_pod_multistatus_change(@@rpod1, "NOTAVAILABLE", :substate=>'DIRTY')
    assert $rsm.read_verify('U', 'RETICLEPOD', @@rpod1, nil, $sv.user)
#    assert_equal 0, $sv.reticle_pod_multistatus_change(@@rpod1, "NOTAVAILABLE", :substate=>'CLEANING')  # does not create events
#    assert $rsm.read_verify('U', 'RETICLEPOD', @@rpod1, nil, $sv.user)
#    assert_equal 0, $sv.reticle_pod_multistatus_change(@@rpod1, "NOTAVAILABLE", :substate=>'OTHER')     # does not create events
#    assert $rsm.read_verify('U', 'RETICLEPOD', @@rpod1, nil, $sv.user)
    assert_equal 0, $sv.reticle_pod_multistatus_change(@@rpod1, "AVAILABLE")
    assert $rsm.read_verify('U', 'RETICLEPOD', @@rpod1, nil, $sv.user)
  end

  def test31_old_reticle_pod_xfer_status_change
    # old xfer status change TXs
    assert_equal 0, $sv.reticle_pod_xfer_status_change(@@rpod1, "EI", :eqp=>@@eqp)
    assert $rsm.read_verify('U', 'RETICLEPOD', @@rpod1, nil, $sv.user)
    assert_equal 0, $sv.reticle_pod_xfer_status_change(@@rpod1, "EO", :eqp=>@@eqp)
    assert $rsm.read_verify('U', 'RETICLEPOD', @@rpod1, nil, $sv.user)
    # load 2 reticles into a pod
    assert_equal 0, $sv.reticle_justinout(@@reticle, @@rpod1, 1, "in")
    assert_equal 0, $sv.reticle_justinout(@@reticle2, @@rpod1, 2, "in")
    $rsm.read_msgs
    assert $rsm.verify_msg('U', 'RETICLE', @@reticle, nil, $sv.user)
    assert $rsm.verify_msg('U', 'RETICLE', @@reticle2, nil, $sv.user)
    assert $rsm.verify_msg('U', 'RETICLEPOD', @@rpod1, nil, $sv.user)
    assert $rsm.verify_msg('U', 'RETICLEPOD', @@rpod1, nil, $sv.user)
    # xfer_status_changes
    assert_equal 0, $sv.reticle_pod_xfer_status_change(@@rpod1, "EI", :eqp=>@@eqp)
    $rsm.read_msgs
    assert $rsm.verify_msg('U', 'RETICLE', @@reticle, nil, $sv.user)
    assert $rsm.verify_msg('U', 'RETICLE', @@reticle2, nil, $sv.user)
    assert $rsm.verify_msg('U', 'RETICLEPOD', @@rpod1, nil, $sv.user)
  end

  def test32_new_reticle_xfer_events
    return unless $env == "f8stag" # seems to be unsafe in ITDC
    _move_reticles_to_pod(@@rpod1, [@@reticle, @@reticle2])

    # offline events
    assert_equal 0, $sv.reticle_pod_offline_load(@@eqp, 'RP1', @@rpod1)
    assert $rsm.read_verify('U', 'RETICLEPOD', @@rpod1, nil, $sv.user)
    assert_equal 0, $sv.reticle_offline_store(@@eqp, 'RP1', @@rpod1, :reticles=>[@@reticle])
    assert_equal 0, $sv.reticle_offline_store(@@eqp, 'RP1', @@rpod1, :reticles=>[@@reticle2])
    $rsm.read_msgs
    assert $rsm.verify_msg('U', 'RETICLE', @@reticle, nil, $sv.user)
    assert $rsm.verify_msg('U', 'RETICLEPOD', @@rpod1, nil, $sv.user)
    assert $rsm.verify_msg('U', 'RETICLE', @@reticle2, nil, $sv.user)
    assert $rsm.verify_msg('U', 'RETICLEPOD', @@rpod1, nil, $sv.user)
    assert_equal 0, $sv.reticle_offline_retrieve(@@eqp, 'RP1', @@rpod1, [@@reticle, @@reticle2])
    $rsm.read_msgs
    assert $rsm.verify_msg('U', 'RETICLE', @@reticle, nil, $sv.user)
    assert $rsm.verify_msg('U', 'RETICLEPOD', @@rpod1, nil, $sv.user)
    assert $rsm.verify_msg('U', 'RETICLE', @@reticle2, nil, $sv.user)
    assert_equal 0, $sv.reticle_pod_offline_unload(@@eqp, 'RP1', @@rpod1)
    assert $rsm.read_verify('U', 'RETICLEPOD', @@rpod1, nil, $sv.user)
  end


  def test41_reticle_counter
    # delete counter to clean up
    $sv.user_parameter_delete_verify("Reticle", @@reticle, @@counter)
    # create reticle counter
    $sv.user_parameter_set_verify("Reticle", @@reticle, @@counter, @@count)
    assert $rsm.read_verify('I', 'RETICLE', @@reticle, @@counter, @@count.to_s, :sp=>true)
    # delete reticle counter
    $sv.user_parameter_delete_verify("Reticle", @@reticle, @@counter)
    assert $rsm.read_verify('D', 'RETICLE', @@reticle, @@counter, "", :sp=>true)
  end

  def test42_reticle_counter_order
    # count down reticle counter
    $sv.user_parameter_set_verify("Reticle", @@reticle, @@counter, @@count)
    $rsm.read_msgs
    c = @@count - 1
    c.downto(0) {|i|
      assert_equal 0, $sv.user_parameter_change("Reticle", @@reticle, @@counter, i)
    }
    $log.info "waiting 60s"
    sleep 60
    $rsm.read_msgs
    # verify message order
    cc = $rsm.msgs.collect {|msg| msg.sp_value.to_i if msg.members.member?(:sp_value)}.compact
    assert_equal c.downto(0).to_a, cc, "wrong message order: #{cc.inspect}"
  end


  def test43_filtered_user
    $sv.reticle_status_change(@@reticle, "NOTAVAILABLE")
    $rsm.read_msgs
    #
    sv2 = SiView::MM.new($env, :user=>"X-ICADA", :password=>"BEBDJ.Y")
    assert_equal 0, sv2.reticle_pod_xfer_status_change(@@rpod1, "EI", :eqp=>@@eqp)
    assert_equal 0, sv2.reticle_pod_xfer_status_change(@@rpod1, "EO", :eqp=>@@eqp)
    assert_equal 0, sv2.reticle_status_change(@@reticle, "AVAILABLE")
    assert_equal 0, sv2.reticle_status_change(@@reticle, "NOTAVAILABLE")
    assert_equal 0, sv2.user_parameter_change("Reticle", @@reticle, @@counter, 1000)
    # check that no message are generated
    $rsm.read_msgs
    assert !$rsm.verify_msg("U", 'RETICLEPOD', @@rpod1)
    assert !$rsm.verify_msg("U", 'RETICLE', @@reticle)
    assert !$rsm.verify_msg("U", 'RETICLE', @@reticle, :sp=>true)
  end

  def test50_barereticlestocker
    return unless $env == "f8stag" # seems to be unsafe in ITDC
    [@@reticle, @@reticle2].each {|rcl| $rsm.cleanup_reticle(rcl)}
    _move_reticles_to_pod(@@rpod2, [@@reticle, @@reticle2])

    $sv.brs_mode_change(@@barereticlestocker, "On-Line Remote", :notify=>false)
    assert_equal 0, $sv.eqp_rsp_port_status_change(@@barereticlestocker, @@rport, "LoadComp")
    $rsm.read_msgs

    # new xfer state changes with reticle (online)
    assert_equal 0, $sv.reticle_pod_load(@@rpod2, :brs=>@@barereticlestocker, :rsc=>@@rport)
    assert $rsm.read_verify('U', 'RETICLEPOD', @@rpod2, nil, $sv.user)
    #
    assert_equal 0, $sv.reticle_store(@@rpod2, :brs=>@@barereticlestocker, :rsc=>@@rport, :reticles=>[@@reticle])
    assert_equal 0, $sv.reticle_store(@@rpod2, :brs=>@@barereticlestocker, :rsc=>@@rport, :reticles=>[@@reticle2])
    $rsm.read_msgs
    assert $rsm.verify_msg('U', 'RETICLE', @@reticle, nil, $sv.user)
    assert $rsm.verify_msg('U', 'RETICLEPOD', @@rpod2, nil, $sv.user)
    assert $rsm.verify_msg('U', 'RETICLE', @@reticle2, nil, $sv.user)
    assert $rsm.verify_msg('U', 'RETICLEPOD', @@rpod2, nil, $sv.user)
    #
    assert_equal 0, $sv.eqp_rsp_port_status_change(@@barereticlestocker, @@rport, "UnloadReq")
    assert_equal 0, $sv.reticle_pod_unload(@@rpod2, :brs=>@@barereticlestocker, :rsc=>@@rport)
    assert_equal 0, $sv.eqp_rsp_port_status_change(@@barereticlestocker, @@rport, "LoadReq")
    assert $rsm.read_verify('U', 'RETICLEPOD', @@rpod2, nil, $sv.user)
    #
    assert_equal 0, $sv.eqp_rsp_port_status_change(@@barereticlestocker, @@rport, "LoadComp")
    assert_equal 0, $sv.reticle_pod_load(@@rpod2, :brs=>@@barereticlestocker, :rsc=>@@rport)
    assert $rsm.read_verify('U', 'RETICLEPOD', @@rpod2, nil, $sv.user)
    #
    assert_equal 0, $sv.reticle_retrieve(@@rpod2, [@@reticle, @@reticle2], :brs=>@@barereticlestocker, :rsc=>@@rport)
    $rsm.read_msgs
    assert $rsm.verify_msg('U', 'RETICLE', @@reticle, nil, $sv.user)
    assert $rsm.verify_msg('U', 'RETICLEPOD', @@rpod2, nil, $sv.user)
    assert $rsm.verify_msg('U', 'RETICLE', @@reticle2, nil, $sv.user)
    #
    assert_equal 0, $sv.reticle_pod_unload(@@rpod2, :brs=>@@barereticlestocker, :rsc=>@@rport)
    assert_equal 0, $sv.eqp_rsp_port_status_change(@@barereticlestocker, @@rport, "LoadReq")
    assert $rsm.read_verify('U', 'RETICLEPOD', @@rpod2, nil, $sv.user)
  end

  def _move_reticles_to_pod(pod, reticles)
      # cleanup and load 2 reticles into the pod
    $rsm.cleanup(pod)
    reticles.each_with_index do |r,i|
      assert $rsm.prepare_reticle(r, pod, i+1, @@eqp)
    end
    $rsm.read_msgs
  end
end
