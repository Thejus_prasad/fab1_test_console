=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: Steffen Steidten, 2015-01-14

History:
  2016-08-29 sfrieske,  minor code cleanup
=end

require 'SiViewTestCase'
require 'jcap/f18smgwy'


# Test the Fab18SpecGateway
class Test_jCAP_Fab18SpecGateway < SiViewTestCase
  @@loops = 500  # approx 5 minutes

  def self.startup
    super
    @@gwys = 3.times.collect {JCAP::Fab18SpecGateway.new($env)}
  end


  def test00_setup
    $setup_ok = false
    #
    @@gwy = @@gwys.first
    @@product = $svtest.product
    assert @@pg = $sm.object_info(:product, @@product).first.specific[:productgroup]
    assert @@technology = $sm.object_info(:productgroup, @@pg).first.specific[:technology]
    #
    $setup_ok = true
  end

  def test01_happy
    $setup_ok = false
    #
    assert res = @@gwy.object_info(:product, @@product)
    assert_equal @@product, res[:id]
    assert res = @@gwy.object_info(:productgroup, @@pg)
    assert_equal @@pg, res[:id]
    assert res = @@gwy.object_info(:technology, @@technology)
    assert_equal @@technology, res[:id]
    #
    $setup_ok = true
  end

  def test02_persistence
    assert res = @@gwy.object_info(:product, @@product)
    assert_equal 1, @@gwy.mq_response.msg_properties[:delivery_mode], "response message is persitent"
  end

  def test91_load
    tt = []
    [[:product, @@product], [:productgroup, @@pg], [:technology, @@technology]].each_with_index {|e, i|
      tt << Thread.new {
        @@loops.times {assert_equal e.last, @@gwys[i].object_info(*e)[:id]}
      }
    }
    tt.each {|t| t.join}
  end
end
