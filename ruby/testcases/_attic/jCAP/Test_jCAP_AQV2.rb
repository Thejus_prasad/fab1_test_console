=begin 
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.
    
Author: Steffen Steidten, 2015-02-24

Version: 14.12

History:
=end

require 'RubyTestCase'
require 'jcap/aqvtest'

# Test jCAP AQV / SAP integration
class Test_jCAP_AQV2 < RubyTestCase
  @@spec = "C07-00000766"
  @@sorter = "UTS001"
  @@stocker = "QA-STO-M1"  
  
  # full carriers
  @@carrier = "TQ01"
  @@carrier2 = "TQ02"

  @@mds_sync = 45
  @@timeout = 120

  
  def test00_setup
    $setup_ok = false
    $aqvtest = AQV::Test.new($env, sv: $sv, sorter: @@sorter, stocker: @@stocker, timeout: @@timeout)
    $setup_ok = true
  end

  def test201_aqvupqual_accept
    assert $aqvtest.wbtqtest.prepare_resources(@@carrier)
    ($log.info "waiting #{@@mds_sync}s for the MDS to sync"; sleep @@mds_sync)
    # execute manual steps
    tstart = Time.now
    puts "\n\nExecute the first set of manual test steps now, when ready press ENTER to continue!"
    gets
    # wait for kit creation and send SAP message
    assert $aqvtest.wait_kits_sapmessage('ACCEPT', tstart: tstart, text: ["QA Test with ACCEPT", "TC201"])
    puts "\n\nVerify the KitBuild order has been closed in SAP!"
    gets
  end

  def test202_aqvupqual_reject
    assert $aqvtest.wbtqtest.prepare_resources(@@carrier)
    ($log.info "waiting #{@@mds_sync}s for the MDS to sync"; sleep @@mds_sync)
    # execute manual steps
    tstart = Time.now
    puts "\n\nExecute the first set of manual test steps now, when ready press ENTER to continue!"
    gets
    # wait for kit creation and send SAP message
    assert $aqvtest.wait_kits_sapmessage('REJECT', tstart: tstart, text: ["QA Test with REJECT", "TC202"])
    puts "\n\nVerify the KitBuild order has been closed in SAP!"
    gets
  end

  def test211_2kits_happy
    assert $aqvtest.wbtqtest.prepare_resources([@@carrier, @@carrier2])
    ($log.info "waiting #{@@mds_sync}s for the MDS to sync"; sleep @@mds_sync)
    # execute manual steps
    tstart = Time.now
    puts "\n\nExecute the first set of manual test steps now, when ready press ENTER to continue!"
    gets
    # wait for kit creation and send SAP ACCEPT messages
    assert $aqvtest.wait_kits_sapmessage(['ACCEPT', 'ACCEPT'], tstart: tstart)
    puts "\n\nVerify the KitBuild orders have been closed in SAP!"
    gets
  end

  def test212_2kits_reject
    assert $aqvtest.wbtqtest.prepare_resources([@@carrier, @@carrier2])
    ($log.info "waiting #{@@mds_sync}s for the MDS to sync"; sleep @@mds_sync)
    # execute manual steps
    tstart = Time.now
    puts "\n\nExecute the first set of manual test steps now, when ready press ENTER to continue!"
    gets
    # wait for kit creation and send SAP REJECT messages
    assert $aqvtest.wait_kits_sapmessage(['REJECT', 'REJECT'], tstart: tstart)
    puts "\n\nVerify the KitBuild orders have status NOK in SAP!"
    gets
    # send SAP ACCEPT messages
    assert $aqvtest.wait_kits_sapmessage(['ACCEPT', 'ACCEPT'], tstart: tstart)
    puts "\n\nVerify the KitBuild orders are closed but still have status NOK in the Upqual!"
    gets
  end

  def test213_2kits_mixed
    assert $aqvtest.wbtqtest.prepare_resources([@@carrier, @@carrier2])
    ($log.info "waiting #{@@mds_sync}s for the MDS to sync"; sleep @@mds_sync)
    # execute manual steps
    tstart = Time.now
    puts "\n\nExecute the first set of manual test steps now, when ready press ENTER to continue!"
    gets
    # wait for kit creation and send SAP ACCEPT messages
    assert $aqvtest.wait_kits_sapmessage(['ACCEPT', 'REJECT'], tstart: tstart)
    puts "\n\nVerify the KitBuild orders have been closed in SAP and have status OK/NOK resp.!"
    gets
  end

  def test301_big_integration
    # ensure WBTQ is configured to use the test (not SAP) response queue
    assert !$aqvtest.wbtqtest.client.stb_trigger('QAQA', timeout: 30), "WBTQ queue must be connected to SAP"
    puts "\n\nEnsure the AQV to SAP queue is the real SAP queue, when ready press ENTER to continue!"
    gets
    assert $aqvtest.wbtqtest.prepare_resources(@@carrier)
    ($log.info "waiting #{@@mds_sync}s for the MDS to sync"; sleep @@mds_sync)
    #
    # create orders in SAP
    tstart = Time.now
    puts "\n\nExecute the first set of manual test steps now, when ready press ENTER to continue!"
    gets
    # wait for kit creation
    nlots = $aqvtest.wbtqtest.sfc.kit_info(name: $aqvtest.kit, eqp: $aqvtest.wbtqtest.eqp).kit_slots.keys.size
    aqvlots = nil
    $log.info "waiting for the kit lots to appear in the AQV tables"
    assert wait_for(timeout: 600) {
      aqvlots = $aqvtest.mds.aqv_lot(tstart: tstart, status: -1)
      aqvlots.size >= nlots
    }
    assert_equal nlots, aqvlots.size
    #
    # process lot in SiView and send DCRs
    aqvlots.each {|aqvlot| assert $aqvtest.complete_lot(aqvlot.lot, nowait: true, sil: true), "error processing lot"}
    #
    puts "\n\nVerify SAP order status, when ready press ENTER to continue!"
    gets
  end

end
