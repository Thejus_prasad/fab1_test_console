=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: sfrieske, 2016-10-07

Version: 19.03

History:
  2018-03-12 sfrieske, use Siview methods directly to create and configure lots, remove use of erpmes/turnkey.rb
=end

require 'jcap/turnkeybtftest'
require 'SiViewTestCase'


# Test the BtfTurnkey jCAP job with Backup Operation lots
class JCAP_BtfTurnkey_Backup < SiViewTestCase
  @@env_av = 'erpstage'    # AsmView test env
  @@env_src = 'f8stag'
  # no need to match MDM data  // 'BAFFINS1MA.A0' 'FF-BAFFIN.01'
  @@svsrc_defaults = {
    product: 'SHELBY3CC.01', route: 'FF-28LPQ-45M.01', customer: 'qgt', carriers: '%',
    erpitem: '*SHELBY3CC-J01'
  }
  @@backup_op = /TKEYBUMPSHIPINIT/
  @@backup_bank = 'O-BACKUPOPER'
  # waferindex => bin1, bin2, bin3; use small indexes because of splits
  @@binning_data = {1=>[628, 0, 618], 2=>[2332, 0, 1234], 4=>[1005, 222, 56]}

  @@jobdelay = 410
  @@backup_testlots = []


  def self.startup
    super
    # note: @@sv is in the backup system role
    @@tktest = Turnkey::Test.new($env, sv: @@sv, env_av: @@env_av, jobdelay: @@jobdelay)
    @@svtest_src = SiView::Test.new(@@env_src, @@svsrc_defaults)
    @@sv_src = @@svtest_src.sv
    #
    @@tkparams = {'TurnkeyType'=>'J', 'TurnkeySubconIDBump'=>'GG', 'TurnkeySubconIDSort'=>'GG'}
  end

  def self.after_all_passed
    @@backup_testlots.each {|lot| @@sv_src.backup_delete_lot_family(lot, @@sv)}
  end

  def test00_setup
    ##assert @@sv_src.user_data(:bank, @@backup_bank)['BankReportingType'].include?(@@backup_bank_udata), "wrong bank (BankReportingType)"
  end

  def test81_happy_backup  # incl test for http://gfjira/browse/JCORP-285, will fail on start_btf
    # create test lot at src site, send it to backup and move to TTB
    assert lot = @@svtest_src.new_lot(user_parameters: @@tkparams)
    @@backup_testlots << lot
    li = @@sv_src.lot_info(lot)
    assert_equal 'FF', @@sv_src.user_data(:productgroup, li.productgroup)['RouteType'], 'wrong route type'
    assert @@sv_src.backup_prepare_send_receive(lot, @@sv,
      backup_op: @@backup_op, backup_bank: @@backup_bank, carrier_category: 'C4')
    assert_equal 0, @@sv.user_parameter_change('Lot', lot, @@tkparams.merge('ERPItem'=>@@svtest_src.erpitem))
    # lot appears in AsmView (includes call to src JCAPRemote.getLotInfo)
    assert @@tktest.start_btf(lot, fabcode: 'F8', coo: 'US'), 'error starting test lot, MQ issues?'
    # process to DSS (TKYOQAS.1 in AsmView)
    assert @@tktest.process_btf(lot), 'error processing lot, setup error?'
  end

end
