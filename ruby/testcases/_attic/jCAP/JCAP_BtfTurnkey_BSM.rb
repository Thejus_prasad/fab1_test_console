=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: Steffen Frieske, 2017

Version: 19.03

History:
=end

require 'jcap/turnkeybtftest'
require 'jcap/turnkeybtf'
require 'SiViewTestCase'


# Test the BtfTurnkey jCAP job with BSM lots
class JCAP_BtfTurnkey_BSM < SiViewTestCase
  @@sv_defaults = {product: 'UT-PRODUCT000.01', route: 'UTRT001.01', customer: 'qgt', 
    carriers: '%', carrier_category: 'C4'}
  @@slt = 'QW'  # QW or 'PW'
  @@av_env = 'erpstage'
  
  @@jobdelay = 410
  @@testlots = []


  def self.startup
    super
    @@tkbsm = Turnkey::BSMTest.new($env, sv: @@sv, sm: @@sm, av_env: @@av_env, jobdelay: @@jobdelay)
    @@tkbtf = Turnkey::BTF.new($env, sv: @@sv)
    @@dbbtf = Turnkey::TestDB.new('itdc', sv: @@sv)
    @@av = @@tkbsm.av
    #
    @@tkparams = {
      'AH'=>{'TurnkeyType'=>'AH', 'TurnkeySubconIDBump'=>'GG'},
      'AJ'=>{'TurnkeyType'=>'AJ', 'TurnkeySubconIDSort'=>'GG'},
      'AP'=>{'TurnkeyType'=>'AP', 'TurnkeySubconIDBump'=>'GG', 'TurnkeySubconIDSort'=>'GG'}
    }
    # no need to match MDM data, but must match TK type because STBMfgAttributes sets the TK type
    @@stbparams = {
      'AH'=>{erpitem: 'DRAW12MC-AHB2', product: 'DRAW12MC-UB1.01', route: 'UTRT-TKBTF-BUMP.01', sublottype: @@slt},
      'AJ'=>{erpitem: 'CHEEL1AK-AJBI', product: 'CHEEL1AK-UBI.01', route: 'UTRT-TKBTF-SORT.01', sublottype: @@slt},
      'AP1'=>{erpitem: 'DRAW12MC-APB2', product: 'DRAW12MC-UB1.01', route: 'UTRT-TKBTF-BUMP.01', sublottype: @@slt},
      'AP2'=>{erpitem: 'DRAW12MC-APB2', product: 'CHEEL1AK-UBI.01', route: 'UTRT-TKBTF-SORT.01', sublottype: @@slt}
    }
  end

  def self.after_all_passed
    @@testlots.each {|lot| @@sv.delete_lot_family(lot)}
  end

  def test51_bsm_bump
    # create fab lot
    assert lot = @@svtest.new_lot, 'error creating test lot'
    @@testlots << lot
    # 
    # finish, start on BUMP layer and finish BUMP
    assert @@tkbsm.start_bsm(lot, @@tkparams['AH'], @@stbparams['AH']), 'error creating BSM lot'
    assert @@tkbsm.process_bsm(lot), 'error processing BSM lot'  # until incl BUMP SHIP
    assert_equal 0, @@sv.lot_gatepass(lot, mandatory: true)
    # verify
    $log.info "waiting up to #{@@tkbsm.jobdelay} s for the lot to move to LOTSHIPD in AsmView"
    assert wait_for(timeout: @@tkbsm.jobdelay) {@@av.lot_info(lot).op == 'LOTSHIPD.1'}, '(av) wrong op'
  end

  def test52_bsm_sort
    # create lot on BUMP layer
    assert lot = @@svtest.new_lot(@@stbparams['AH']), 'error creating test lot'
    @@testlots << lot
    #
    # finish, start on SORT layer and finish SORT
    assert @@tkbsm.start_bsm(lot, @@tkparams['AJ'], @@stbparams['AJ']), 'error creating BSM lot'
    assert @@tkbsm.process_bsm(lot, 'error processing BSM lot')  # until incl SORT SHIP
    #
    # set binning data, send the update_sort_result message and verify
    assert sortdata = @@dbbtf.set_lot_wafer_bins(lot)
    assert @@tkbtf.update_sort_result(lot), 'error updating sort result by BtfTurnkey'
    assert_equal 'LOTSHIPD.1', @@av.lot_info(lot).op, '(av) wrong op'
  end

  def test53_bsm_AH_AJ  # fails at re-stb on SORT, av lot is at LOTSHIP on route BSM-TURNKEY
    # create fab lot
    assert lot = @@svtest.new_lot, 'error creating test lot'
    @@testlots << lot
    #
    # finish, start on BUMP layer and finish BUMP
    assert @@tkbsm.start_bsm(lot, @@tkparams['AH'], @@stbparams['AH']), 'error creating BSM lot'
    assert @@tkbsm.process_bsm(lot), 'error processing BSM lot'  # until incl BUMP SHIP
    assert_equal 0, @@sv.lot_gatepass(lot, mandatory: true)  # remove?
    assert_equal 0, @@sv.user_parameter_delete('Lot', lot, 'TurnkeySubconIDBump')
    # lot stays at BUMP SHIP in AsmView (??)
    # assert wait_for(timeout: @@tkbsm.jobdelay) {@@av.lot_info(lot).op == 'LOTSHIPD.1'}, '(av) wrong op'
    #
    # start on SORT layer and finish SORT
    assert @@tkbsm.start_bsm(lot, @@tkparams['AJ'], @@stbparams['AJ'])   # fails, lot is at LOTSHIP on route BSM-TURNKEY


    assert @@tkbsm.process_bsm(lot), 'error processing BSM lot'  # until incl SORT SHIP
    #
    # set binning data, send the update_sort_result message and verify
    assert sortdata = @@dbbtf.set_lot_wafer_bins(lot)
    assert @@tkbtf.update_sort_result(lot), 'error updating sort result by BtfTurnkey'
    assert_equal 'LOTSHIPD.1', @@av.lot_info(lot).op, '(av) wrong op'
  end

  def test54_bsm_AP
    # create fab lot
    assert lot = @@svtest.new_lot, 'error creating test lot'
    @@testlots << lot
    #
    # finish, start on BUMP layer with TK type AP and finish BUMP
    assert @@tkbsm.start_bsm(lot, @@tkparams['AP'], @@stbparams['AP1']), 'error creating BSM lot'
    assert @@tkbsm.process_bsm(lot), 'error processing BSM lot'  # until incl BUMP SHIP
    # gatepass, lot goes to B-SORT route in AsmView
    assert_equal 0, @@sv.lot_gatepass(lot, mandatory: true)
    #
    # start on SORT layer and finish SORT
    assert @@tkbsm.start_bsm(lot, @@tkparams['AP'], @@stbparams['AP2']), 'error creating BSM lot'
    assert @@tkbsm.process_bsm(lot), 'error processing BSM lot'  # until incl SORT SHIP
    #
    # set binning data and send the update_sort_result message
    assert sortdata = @@dbbtf.set_lot_wafer_bins(lot)
    assert @@tkbtf.update_sort_result(lot), 'error updating sort result by BtfTurnkey'
    assert_equal 'LOTSHIPD.1', @@av.lot_info(lot).op, '(av) wrong op'
  end

end
