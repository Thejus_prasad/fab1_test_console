=begin
Test the QT rules. TC10

(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: Thomas Klose 2015-12-09

todo:

=end

require "SiViewTestCase"
require "sjc"
require "thread"

class Test_WD_QTTC10 < SiViewTestCase
  include MonitorMixin
  @@sv_defaults = {product: "A-QT-PRODUCT-WD-TC10.01", route: "A-QT-MAINPD-WD-TC10.01", carriers: 'QT%'}

  @@tc100 = true  #process at start pd, process at end pd. No hold
  @@tc110 = true  #process at start pd, qt expires. Lot on hold
  @@tc120 = false
  @@tc130 = true  #process at start pd, locate to meas pd after end pd. Lot on hold.
  @@tc140 = true  # PSM in QT area, process at start pd, process in PSM after retrigger!
  @@tc150 = true  # PSM in QT area, process at start pd, process in PSM after retrigger, return to orig PD!

  @@tc999 = true # delete all created lots when testing is finished

  @@continue_test = false

  @@Hold_Code1 = "QTHL"   #WD test aktiv!
  #@@Hold_Code1 = "QTOV"
  @@Hold_Code2 = "QTOV"

  @@C1 = "C1"
  @@C2 = "C2"
  @@C4 = "C4"
  @@QT_time = 15 # in minutes. Configured in SiView for the related PD. default 10 min for ITDC


  def self.after_all_passed
    @@testlots.each {|lot| $sv.delete_lot_family(lot)}
  end


  def test010_prepare
    @@qtstart = {}
    @@qthold = {}
    @@holds = {}
    @@lots = {}
    @@threads = []
    assert @@testlots = $svtest.new_lots(6)

    mutex = Mutex.new

    # standard test: process at start pd, process at end pd. No hold
    @@threads << Thread.new {
      sv = SiView::MM.new($env)
      @@lots['tc100'] = l = @@testlots[0]
      sv.lot_opelocate(l, "1000.1200")
      sv.claim_process_lot(l, :happylot=>true, :synchronize=>mutex)
      sleep 60
      sv.lot_opelocate(l, "2000.1400")
      sv.claim_process_lot(l, :happylot=>true, :synchronize=>mutex)
      $log.info("-- tc100 (lot #{l}) sleeps for 600 seconds")
      sleep 600
    } if @@tc100


    # standard test: process at start pd, qt expires. Lot on hold
    @@threads << Thread.new {
      sv = SiView::MM.new($env)
      @@lots['tc110'] = l = @@testlots[1]
      sv.lot_opelocate(l, "1000.1200")
      sv.claim_process_lot(l, :happylot=>true, :synchronize=>mutex)
      @@qtstart[l] = $sv.siview_time(sv.lot_info(l).claim_time)
      $log.info("-- tc110 (lot #{l}) sleeps for 600 seconds")
      sleep 600
    } if @@tc110

    #@@threads << Thread.new {
      #sv = SiView::MM.new($env)
      # @@lots['tc120'] = l = @@testlots[2]
      # sv.lot_opelocate(l, "1000.1200")
      # sv.claim_process_lot(l, :happylot=>true, :synchronize=>mutex)
      # @@qtstart[l] = $sv.siview_time(sv.lot_info(l).claim_time)
      # $log.info("-- tc120 (lot #{l}) sleeps for 600 seconds")
      # sleep 600
    #} if @@tc120

    # PSM in QT area, process at start pd, process in PSM before replace!
    @@threads << Thread.new {
      sv = SiView::MM.new($env)
      @@lots['tc130'] = l = @@testlots[3]
      sv.lot_experiment(l, 25, "eA-QT-ERF-A0-WD.01", "1000.1400", "2000.1400")
      sv.lot_opelocate(l, "1000.1200")
      sv.claim_process_lot(l, :happylot=>true, :synchronize=>mutex)
      @@qtstart[l] = $sv.siview_time(sv.lot_info(l).claim_time)
      sv.lot_opelocate(l, "1000.1400")
      sv.claim_process_lot(l, :happylot=>true, :synchronize=>mutex)
      $log.info("-- tc130 (lot #{l}) sleeps for 600 seconds")
      sleep 600
      @@qthold[l] = $sv.siview_time(sv.lot_info(l).claim_time)
    } if @@tc130

    # PSM in QT area, process at start pd, process in PSM after retrigger!
    @@threads << Thread.new {
      sv = SiView::MM.new($env)
      @@lots['tc140'] = l = @@testlots[4]
      sv.lot_experiment(l, 25, "eA-QT-ERF-A0-WD.01", "1000.1400", "2000.1400")
      sv.lot_opelocate(l, "1000.1200")
      sv.claim_process_lot(l, :happylot=>true, :synchronize=>mutex)
      sv.lot_opelocate(l, "1000.1400")
      sv.claim_process_lot(l, :happylot=>true, :synchronize=>mutex)  # jump to PSM
      sleep 60
      sv.lot_opelocate(l, "1000.2200")
      sv.claim_process_lot(l, :happylot=>true, :synchronize=>mutex)  # retrigger
      @@qtstart[l] = $sv.siview_time(sv.lot_info(l).claim_time)
      $log.info("-- tc140 (lot #{l}) sleeps for 600 seconds")
      sleep 660
      @@qthold[l] = $sv.siview_time(sv.lot_info(l).claim_time)
    } if @@tc140

    # PSM in QT area, process at start pd, process in PSM after retrigger, return to orig PD!
    @@threads << Thread.new {
      sv = SiView::MM.new($env)
      @@lots['tc150'] = l = @@testlots[5]
      sv.lot_experiment(l, 25, "eA-QT-ERF-A0-WD.01", "1000.1400", "2000.1400")
      sv.lot_opelocate(l, "1000.1200")
      sv.claim_process_lot(l, :happylot=>true, :synchronize=>mutex)
      sv.lot_opelocate(l, "1000.1400")
      sv.claim_process_lot(l, :happylot=>true, :synchronize=>mutex)  # jump to PSM
      sleep 60
      sv.lot_opelocate(l, "1000.2200")
      sv.claim_process_lot(l, :happylot=>true, :synchronize=>mutex)  # retrigger
      @@qtstart[l] = $sv.siview_time(sv.lot_info(l).claim_time)
      sleep 60
      sv.claim_process_lot(l, :happylot=>true, :synchronize=>mutex)  # jump to orig PD
      $log.info("-- tc150 (lot #{l}) sleeps for 600 seconds")
      sleep 600
      @@qthold[l] = $sv.siview_time(sv.lot_info(l).claim_time)
    } if @@tc150
  end

  def test020_wait_for_threads
    # wait for all threads to complete
    @@threads.each {|t| t.join}
    #$log.info("-- sleep for 60 seconds")
    #sleep 60   # for qt ctrl
    $log.info "******* created following lots: #{@@lots.inspect}"
  end

  def test100_checklots
    return if @@tc100 == false
    l = @@lots['tc100']
    $log.info("checking tc100 with lot #{l}")
    assert_equal nil, checkhold(l), "no hold expected"
  end

  def test110_checklots
    return if @@tc110 == false
    l = @@lots['tc110']
    $log.info("checking tc110 with lot #{l}")
    $log.info "found lot hold with reason: #{checkhold(l)}"
    assert_equal @@Hold_Code1, checkhold(l), "hold expected"
  end

  def test120_checklots
    return #if @@tc120 == false  # disabled!
    l = @@lots['tc120']
    $log.info("checking tc120 with lot #{l}")
    $log.info "found lot hold with reason: #{checkhold(l)}"
    assert_equal @@Hold_Code1, checkhold(l),  "hold expected"
  end

  def test130_checklots
    return if @@tc130 == false
    l = @@lots['tc130']
    $log.info("checking tc130 with lot #{l}")
    $log.info "found lot hold with reason: #{checkhold(l)}"
    assert_equal @@Hold_Code1, checkhold(l),  "hold expected"
    t = calc_qt(l)
    $log.info "qt time: #{t}"
  end

  def test140_checklots
    return if @@tc140 == false
    l = @@lots['tc140']
    $log.info("checking tc140 with lot #{l}")
    $log.info "found lot hold with reason: #{checkhold(l)}"
    assert_equal @@Hold_Code1, checkhold(l),  "hold expected"
    t = calc_qt(l)
    $log.info "qt time: #{t}"
  end

  def test150_checklots
    return if @@tc150 == false
    l = @@lots['tc150']
    $log.info("checking tc150 with lot #{l}")
    $log.info "found lot hold with reason: #{checkhold(l)}"
    assert_equal @@Hold_Code1, checkhold(l),  "hold expected"
    t = calc_qt(l)
    $log.info "qt time: #{t}"
  end

  def test999_cleanup
    return unless @@tc999
    puts "\ndelete all lots: #{@@lots.inspect} (j/n)?"
    answer = STDIN.gets
    if "jy".index(answer.chomp.downcase)
      @@lots.keys.each{|tc| $sv.delete_lot_family(@@lots[tc], :sleeptime=>2)}
    end
  end

  def get_child_lot(lot)
    _clot = []
    _clot = $sv.lot_family(lot)
    _clot.delete(lot)
    return _clot[0]
  end

  def checkhold(lot)
    #$log.info "method checkhold sleep 10s"
    #sleep 10
    ret=$sv.lot_hold_list(lot, :reason=>@@Hold_Code1)
    if ret.size == 0
        return nil
    else
        r = ret[0]
        #refute_equal "", r['memo'], "hold memo for QTHL hold is empty"   # WatchDog doesn�t provide a memo
        return r['reason']
    end
  end

  def checkfuturehold(lot)
    #$log.info "method checkfuturehold sleep 10s"
    #sleep 10
    ret=$sv.lot_futurehold_list(lot, :reason=>@@Hold_Code1)
    if ret.size == 0
        return nil
    else
        r = ret[0]
        refute_equal "", r['memo'], "hold memo for QTHL hold is empty"
        return r['reason']
    end
  end

  def checkhold_criticality_memo(lot, criticality, params={})
    # e.g.: @@claim_memo_criticality = "time of %s min exceeded at %s (hold action: %s, target operation %s, DLIS comment: %s), Criticality =%s"
    #memo = @@claim_memo_criticality %[@@QT_time,@@carrier0,@@carrier5,@@requestor,@@sorter,@@pg]
    #$log.info "method checkhold_criticality_memo sleep 30s"
    #sleep 30
    hold = (params[:hold] or "")

    if hold == "future"
      ret=$sv.lot_futurehold_list(lot, :reason=>@@Hold_Code1)
    else
      ret=$sv.lot_hold_list(lot, :reason=>@@Hold_Code1)
    end

    if ret.size == 0
        return false
    else
        r = ret[0]
        end_memo = "criticality = #{criticality}"
        refute_equal "", r['reason'], "hold memo for QTHL hold is empty"
        assert r['memo'].start_with?("Queue time of #{@@QT_time} min"), "wrong start of claim memo for QTHL hold"
        assert r['memo'].end_with?(end_memo), "wrong end of claim memo for QTHL hold"
        return true
    end
  end

  def calc_qt(lot)
    return @@qthold[lot] - @@qtstart[lot]
  end
end
