=begin
(c) Copyright 2011 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: Daniel Steger, 2011-03-17

History:
  2015-04-01 ssteidte,  cleanup
=end

require 'SiViewTestCase'
require 'derdack'

# Test jCAP GatePassOperation job
class Test_jCAP_GatePassOperation < SiViewTestCase
  @@product = 'ITDC-JCAP-TEST.01'
  @@route = 'ITDC-JCAP-TEST.01'
  @@opNoStart = '4000.1000'
  @@opNoMandatory = '4000.1300'
  @@candidate_pds = %w(e0000-DUMMYSKIP.01 REMEASURE.01 LOCATE.01 GETDICD-SKIP.01 A-SKIP.01 P-STID-SKIP.01
    SORTPPSMPL-SKIP.01 SORTSMPL-SKIP.01 FINA-FWETDONE.01 FAB1-OUT.01 BUMP-OUT.01 SORT-OUT.01 CHKLOTFAM.01)
  @@eqp = 'UTF001'

  @@jcap_interval = 300


  def self.startup
    super
    $derdack = Derdack::EventDB.new($env)
  end

  def self.after_all_passed
    $sv.delete_lot_family(@@lot)
  end


  def test00_setup
    $setup_ok = false
    #
    assert $svtest.cleanup_eqp(@@eqp, mode: 'Off-Line-1')
    assert @@lot = $svtest.new_lot(product: @@product, route: @@route), "error creating test lot"
    @@carrier = $sv.lot_info(@@lot).carrier
    assert $svtest.cleanup_carrier(@@carrier)
    @@pds_seen = []
    #
    $setup_ok = true
  end

  def test11_gatepass
    $setup_ok = false
    assert_equal 0, $sv.lot_opelocate(@@lot, @@opNoStart), "error locating lot"
    assert wait_check_gatepass('4000.1100')
    $setup_ok = true
  end

  def test12_futurehold
    $setup_ok = false
    assert_equal 0, $sv.lot_futurehold(@@lot, "4000.1200", nil), "error setting futurehold"
    assert wait_check_gatepass('4000.1200')
    $setup_ok = true
  end

  def test13_hold
    $setup_ok = false
    # lot stays on operation because of hold
    assert !wait_check_gatepass('4000.1300')
    # after release, lot is moved to next operation
    $sv.lot_hold_release(@@lot, nil)
    assert wait_check_gatepass('4000.1300')
    $setup_ok = true
  end

  def test14_mandatory
    # stay on a mandatory operation
    tstart = Time.now
    assert_equal 0, $sv.lot_opelocate(@@lot, @@opNoMandatory), "error locationg lot #{@@lot} to #{@@opNoMandatory}"
    assert !wait_check_gatepass('4000.1400')
    # a Derdack message must be sent
    msgs = $derdack.get_events(tstart, Time.now, 'Application'=>'GatePassOperation', 'Category'=>'GatePassFailed')
    assert msgs.size > 0, "no Derdack message sent"
  end

  def test15_reserveuser
    $setup_ok = false
    carrier = $sv.lot_info(@@lot).carrier
    assert_equal 0, $sv.carrier_reserve(carrier), "error reserving carrier #{carrier}"
    assert_equal 0, $sv.lot_opelocate(@@lot, '4000.1400'), "error locating lot #{@@lot}"
    assert !wait_check_gatepass('4000.1500')
    assert_equal 0, $sv.carrier_reserve_cancel(carrier), "error canceling carrier reservation for #{carrier}"
    assert wait_check_gatepass('4000.1500')
    $setup_ok = true
  end

  def test16_xferstate
    # GatePass when "EI"
    carrier = $sv.lot_info(@@lot).carrier
    $sv.carrier_xfer_status_change(carrier, 'EO', @@eqp)
    assert_equal 0, $sv.eqp_load(@@eqp, nil, carrier, purpose: 'Other'), "error loading lot"
    # ensure the new carrier xfer status = "EI" after loading on eqp.
    assert wait_check_gatepass('4000.1600') # changed according to the MSR 772046. no check by jCAP on xfer state before sending Tx GatePass to  SiView
    assert_equal 'EI',  $sv.carrier_status(carrier).xfer_status, "wrong xfer status for carrier #{carrier}"
    # unload carrier for next tests
    assert_equal 0, $sv.eqp_unload(@@eqp, nil, carrier), "error unloading lot"
  end

  def test17_nonprobank
    $setup_ok = false
    # no GatePass when banked in
    assert_equal 0, $sv.lot_nonprobankin(@@lot, $svtest.nonprobank), "error banking in lot"
    assert !wait_check_gatepass('4000.1700')
    # bankout, GatePass
    assert_equal 0, $sv.lot_nonprobankout(@@lot), "error banking out lot"
    assert wait_check_gatepass('4000.1700')
    $setup_ok = true
  end

  def test18_remaining_PDs
    # gate pass all other operations of the module
    $sv.lot_opelocate(@@lot, '4000.1700')
    assert wait_check_gatepass('4000.1800')
    assert wait_check_gatepass('4000.1900')
    assert wait_check_gatepass('4000.2000')
    assert wait_check_gatepass('4000.2100')
    assert wait_check_gatepass('4000.2200')
    assert wait_check_gatepass('5000.1000')
  end


  def test99_summary
    diff = @@candidate_pds - @@pds_seen
    assert_equal [], diff, "not all candidate PDs have been checked, missing #{diff.inspect}"
  end


  def wait_check_gatepass(opNo)
    @@pds_seen << $sv.lot_info(@@lot).op
    t = @@jcap_interval + 60
    $log.info "waiting up to #{t}s for the jCAP job to complete (opNo #{opNo.inspect})"
    wait_for(timeout: t) {$sv.lot_info(@@lot).opNo == opNo}
  end
end
