=begin 
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.
    
Author: Steffen Steidten, 2011-04-28
=end

require_relative 'Test_Tkey_Flow'

# Test TurnKey flow with  subcon ids GG
class Test_Tkey_Flow_GG < Test_Tkey_Flow
  @@tkbump = 'GG'
  @@tksort = 'GG'
end
