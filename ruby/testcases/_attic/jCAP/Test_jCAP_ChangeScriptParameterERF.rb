=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: Steffen Steidten, 2011-05-30

Version: 1.1.0

History:
  2015-03-31 ssteidte, cleaned up
  2017-06-01 sfrieske, minor fixes
=end

require 'SiViewTestCase'

# Test jCAP ChangeScriptParameterERF job
class Test_jCAP_ChangeScriptParameterERF < SiViewTestCase
  @@product = 'ITDC-JCAP-ERF-TEST.01'
  @@route = 'ITDC-JCAP-TEST-ERF.01'
  @@opno_start = '3000.1000'

  @@route_erf1 = 'eA259-ITDC-JCAP-TEST-SCRIPTPARAMETERERF-A0.01'
  @@ppcd_erf1 = '123456789'
  @@opno_split1 = '3000.1100'
  @@opno_merge1 = '3000.1300'

  @@route_erf2 = 'eA261-ITDC-JCAP-TEST-SCRIPTPARAMETERERF-A0.01'
  @@ppcd_erf2 = '12345678'
  @@opno_split2 = '3100.1000'
  @@opno_merge2 = '3100.1300'

  @@route_erf3 = 'eA261-ITDC-JCAP-TEST-SCRIPTPARAMETERERF-A1.01'
  @@opno_split3 = '1000.1100'
  @@opno_merge3 = '1000.1300'

  @@split_all_wafers = false
  @@sleeptime = 120   # wait for MDS


  def self.after_all_passed
    @@testlots.each {|lot| $sv.delete_lot_family(lot)}
  end


  def test00_setup
    $setup_ok = false
    #
    $log.info "testing with split_all_wafers: #{@@split_all_wafers}"
    assert @@testlots = $svtest.new_lots(6, route: @@route, product: @@product)
    #
    $setup_ok = true
  end

  def test11_ERFID
    lot = @@testlots.first
    # set PSM
    nwafers = @@split_all_wafers ? 25 : 10
    assert_equal 0, $sv.lot_experiment(lot, nwafers, @@route_erf1, @@opno_split1, @@opno_merge1)
    assert_equal 0, $sv.user_parameter_change('Lot', lot, 'ERFID', 'ASSIGNED')
    assert_equal 0, $sv.user_parameter_change('Lot', lot, 'PPCD', @@ppcd_erf1)
    # locate lot to split op
    ##assert_equal 0, $sv.lot_hold_release(lot, nil)
    assert_equal 0, $sv.lot_opelocate(lot, @@opno_split1)
    lots = $sv.lot_family(lot)
    unless @@split_all_wafers
      assert_equal 2, lots.size, "error splitting lot at ERF split op"
    end
    # read script parameter
    $log.info "waiting #{@@sleeptime} s for the MDS to synch"; sleep @@sleeptime
    lots.each {|l|
      assert_equal 'EXECUTED', $sv.user_parameter('Lot', l, name: 'ERFID').value, "ERFID not set at lot #{l}"
    }
  end

  def test12_merge
    lot = @@testlots.first
    # merge at merge op
    assert_equal 0, $sv.lot_hold_release(lot, nil)
    lots = $sv.lot_family(lot)
    lots.each {|l| assert_equal 0, $sv.lot_opelocate(l, @@opno_merge1, route: @@route)}
    assert_equal 0, $sv.lot_merge(lot, nil), "error merging lots #{lots.inspect}"
    # verify script parameter is still set
    $log.info "waiting #{@@sleeptime} s for the MDS to synch"; sleep @@sleeptime
    assert_equal 'EXECUTED', $sv.user_parameter('Lot', lot, name: 'ERFID').value, "ERFID not set at lot #{lot}"
  end

  def test21_ERF2
    lot = @@testlots.first
    # delete 1st PSM
    assert $sv.lot_experiment_delete(lot)
    $log.info "waiting #{@@sleeptime} s for the MDS to synch"; sleep @@sleeptime
    assert_equal 'EXECUTED', $sv.user_parameter('Lot', lot, name: 'ERFID').value, "ERFID not set at lot #{lot}"
    # define 2nd PSM and verify sript parameter is still set
    nwafers = @@split_all_wafers ? 25 : 10
    assert_equal 0, $sv.lot_experiment(lot, nwafers, @@route_erf2, @@opno_split2, @@opno_merge2)
    $log.info "waiting #{@@sleeptime} s for the MDS to synch";sleep @@sleeptime
    assert_equal 'EXECUTED', $sv.user_parameter('Lot', lot, name: 'ERFID').value, "ERFID not set at lot #{lot}"
    # locate lot to split op
    assert_equal 0, $sv.lot_opelocate(lot, @@opno_split2)
    lots = $sv.lot_family(lot)
    unless @@split_all_wafers
      assert_equal 2, lots.size, "error splitting lot at ERF split op"
    end
    # read script parameter
    $log.info "waiting #{@@sleeptime} s for the MDS to synch"; sleep @@sleeptime
    lots.each {|l|
      assert_equal 'EXECUTED', $sv.user_parameter('Lot', l, name: 'ERFID').value, "ERFID not set at lot #{l}"
    }
  end

  def test22_merge2
    lot = @@testlots.first
    lots = $sv.lot_family(lot)
    # merge at merge op
    lots.each {|l| assert_equal 0, $sv.lot_opelocate(l, @@opno_merge2, route: @@route)}
    assert_equal 0, $sv.lot_merge(lot, nil), "error merging lots #{lots.inspect}"
    # verify script parameter is still set
    $log.info "waiting #{@@sleeptime} s for the MDS to synch"; sleep @@sleeptime
    assert_equal 'EXECUTED', $sv.user_parameter('Lot', lot, name: 'ERFID').value, "ERFID not set at lot #{lot}"
  end

  def test31_4_lots
    @@testlots[1..4].each {|lot|
      # set PSM
      nwafers = @@split_all_wafers ? 25 : 10
      assert_equal 0, $sv.lot_experiment(lot, nwafers, @@route_erf1, @@opno_split1, @@opno_merge1)
      assert $sv.user_parameter_set_verify('Lot', lot, 'ERFID', 'ASSIGNED')
      assert $sv.user_parameter_set_verify('Lot', lot, 'PPCD', @@ppcd_erf1)
      # locate lot to split op
      assert_equal 0, $sv.lot_opelocate(lot, @@opno_split1)
      lcs = $sv.lot_family(lot)
      unless @@split_all_wafers
        assert_equal 2, lcs.size, "error splitting lot at ERF split op"
      end
    }
    # read script parameter
    $log.info "waiting #{@@sleeptime} s for the MDS to synch"; sleep @@sleeptime
    @@testlots[1..4].each {|lot|
      $sv.lot_family(lot).each {|l|
        assert_equal 'EXECUTED', $sv.user_parameter('Lot', l, name: 'ERFID').value, "ERFID not set at lot #{l}"
      }
    }
  end

  def test41_branch_to_branch
    lot = @@testlots[5]
    # set PSMs
    nwafers = @@split_all_wafers ? 25 : 10
    assert_equal 0, $sv.lot_experiment(lot, nwafers, @@route_erf2, @@opno_split2, @@opno_merge2)
    assert_equal 0, $sv.lot_experiment(lot, 4, @@route_erf3, @@opno_split3, @@opno_merge3,
                          split_route: @@route_erf2, original_route: @@route, original_opNo: @@opno_split2)
    assert $sv.user_parameter_set_verify('Lot', lot, 'ERFID', 'ASSIGNED')
    assert $sv.user_parameter_set_verify('Lot', lot, 'PPCD', @@ppcd_erf2)
    # first split
    assert_equal 0, $sv.lot_opelocate(lot, @@opno_split2)
    lcs = $sv.lot_family(lot)
    unless @@split_all_wafers
      assert_equal 2, lcs.size, "error splitting lot at ERF split op"
    end
    # second split
    lc1 = lcs[-1]
    assert_equal 0, $sv.lot_opelocate(lc1, @@opno_split3)
    lcs = $sv.lot_family(lot)
    nlots = @@split_all_wafers ? 2 : 3
    assert_equal nlots, lcs.size, "error splitting lot at second ERF split op"
    # check script parameter
    $log.info "waiting #{@@sleeptime} s for the MDS to synch"; sleep @@sleeptime
    lcs.each {|l|
      assert_equal 'EXECUTED', $sv.user_parameter('Lot', l, name: 'ERFID').value, "ERFID not set at lot #{l}"
    }
  end

end
