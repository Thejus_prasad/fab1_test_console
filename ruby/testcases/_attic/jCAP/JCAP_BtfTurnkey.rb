=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: Ulrich Koerner (Saxonia AG), 2013-02-05

Version: 19.06

History:
  2015-07-01 ssteidte, better configuration, added test for 15.07
  2016-08-04 sfrieske, added lot_characteristics to AsmView::MM, no need to use AsmView::JCAP any more
  2016-11-08 sfrieske, minor cleanup
  2018-03-06 sfrieske, replaced user_parameter_delete_verify by user_parameter_delete
  2018-03-12 sfrieske, use Siview methods directly to create and configure lots, reduce use of erpmes.rb
  2019-02-27 sfrieske, add tests for GID_SOURCE
  2019-05-03 sfrieske, rewrite test12 for MSR 1351735 (HOLD instead of resync)
  2019-06-24 sfrieske, added tests for MSR 1564401 and JCORP-2355
  2019-10-18 aschiebe, added test cases for partial rework and multiple events

Notes:
  for GID_SOURCE tests: runtest 'JCAP_BtfTurnkey', 'itdc', parameters: {av_gid: 'LOT_LABEL'}
=end

require 'jcap/turnkeybtf'  # for TestDB
require 'jcap/turnkeybtftest'
require 'SiViewTestCase'


# Test the BtfTurnkey jCAP job, incl. MSR835281, MSR844465 (versioning), MSR849405 (part changes)
class JCAP_BtfTurnkey < SiViewTestCase
  # no need to match MDM data, customer gf seems to work (else use qgt)
  # @@sv_defaults = {customer: 'qgt', route: 'C02-TKEY-ACTIVE.01', product: 'SHELBY21BTF.QA', carriers: '%', erpitem: '*SHELBY21AE-M00-JB4'}
  @@sv_defaults = {customer: 'qgt', route: 'UTRT-TKBTF.01', product: 'SHELBY-TKBTF.QA', carriers: '%', erpitem: '*SHELBY21AE-M00-JB4'}
  @@av_env = 'erpstage'
  @@av_gid = nil                            # nil, LOT_ID or LOT_LABEL
  @@gid_customer = 'qaeric'                 # a customer with UDATA GID_SOURCE 'LOT_LABEL'
  @@gid_customer2 = 'qaeric2'               # another customer with same data
  @@gid_product = 'SHELBY-TKBTF.QAGID'      # 'SHELBY21BTF.QAGID'
  @@gid_product2 = 'SHELBY21BTF.QA2GID'
  @@gid_product_l = 'SHELBY-TKBTF-L.QAGID'  # 'SHELBY21BTF-L.QAGID'
  #
  # product must be configured in btfturnkey.properties, e.g. SHELBY, FRODO, GIMLI, no need to match MDM data
  @@erpitem2 = '*SHELBY3CC-M00-JB4'
  @@product2 = 'SHELBY21BTF.QA2'            # 'SHELBY3CC.QA2'
  @@product_sor2 = 'SHELBY21BTF.SOR2'       # 'SHELBY21BTF.QA' has also SOR2
  @@route_sor2 = 'C02-TKEY-ACTIVE-SOR2.01'
  @@sublottype2 = 'PX'
  @@customer2 = 'gf'
  @@product_l = 'SHELBY-TKBTF-L.QA'         # 'SHELBY21BTF-L.QA'
  @@route_l = 'UTRT-TKBTF-L.01'             # 'C02-TKEY-ACTIVE-L.01'
  @@route_rwk = 'UTRWK01.01'
  @@holdreason1 = 'AEHL'
  @@holdreasons = ['FSC', 'LTHL', 'SMPL']   # sorted list
  # UBM -> SLD rework w/o stages
  @@rwk2_rte = 'UTRWK01.01'
  @@rwk2_ret = '6310.2000'
  #
  # backup operations
  @@env_src = 'f8stag'
  # customers 'qgt' and 'mtk' not working in AsmView (lot hold before TTS)
  @@svsrc_defaults = {product: 'SHELBY3CC.01', route: 'FF-28LPQ-45M.01', carriers: '%', erpitem: '*SHELBY3CC-J01'}
  @@psend_rcv = {backup_op: /TKEYBUMPSHIPINIT/, backup_bank: 'O-BACKUPOPER'}
  #
  @@tkparams = {'TurnkeyType'=>'J', 'TurnkeySubconIDBump'=>'GG', 'TurnkeySubconIDSort'=>'GG'}
  # waferindex => bin1, bin2, bin3; use small indexes because of splits
  @@binning_data = {1=>[628, 0, 618], 2=>[2332, 0, 1234], 4=>[1005, 222, 56]}
  #
  @@jobdelay = 410
  @@testlots = []
  @@testlots_bop = []


  def self.after_all_passed
    @@testlots_bop.each {|lot| @@sv_src.backup_delete_lot_family(lot, @@sv)}
    @@testlots.each {|lot| @@sv.delete_lot_family(lot)}
  end

  def test00_setup
    $setup_ok = false
    #
    assert_nil @@sv.user_data(:product, @@svtest.product)['MaskSetCustomer'], 'wrong product setup'
    assert_nil @@sv.user_data(:product, @@product2)['MaskSetCustomer'], 'wrong product setup'
    assert_nil @@sv.user_data(:product, @@product_l)['MaskSetCustomer'], 'wrong product setup'
    @@tktest = Turnkey::BTFTest.new($env, sv: @@sv, av_env: @@av_env, av_gid: @@av_gid, jobdelay: @@jobdelay)
    @@av = @@tktest.av
    @@tkbtf = Turnkey::BTF.new($env, sv: @@sv)
    @@dbbtf = Turnkey::TestDB.new('itdc', sv: @@sv)
    #
    if @@av_gid == 'LOT_LABEL'
      assert_equal 'ON', @@sv.environment_variable(var: 'CS_SP_IBMULL_FOR_LOTID_ENABLED'), 'wrong MM env setting'
      assert_equal 'LOT_LABEL', @@sv.user_data(:customer, @@gid_customer)['GID_SOURCE'], 'wrong customer setup'
      @@svtest.customer = @@gid_customer  # need to change customer to get the LOT_LABEL set at STB
      @@svtest.product = @@gid_product
      assert_equal @@gid_customer, @@sv.user_data(:product, @@gid_product)['MaskSetCustomer'], 'wrong product setup'
      assert_equal @@gid_customer, @@sv.user_data(:product, @@gid_product2)['MaskSetCustomer'], 'wrong product setup'
      assert_equal @@gid_customer, @@sv.user_data(:product, @@gid_product_l)['MaskSetCustomer'], 'wrong product setup'
    end
    #
    $setup_ok = true
  end

  def test01_smoke_J
    $setup_ok = false
    #
    assert lot = @@svtest.new_lot(user_parameters: @@tkparams)
    @@testlots << lot
    assert_equal 0, @@sv.lot_opelocate(lot, 1), 'error locating lot'  # for MSRs 1499295, 1499302
    # process lot to (sv) LOTSHIPDESIG.01, (av) TKYSORT.1
    assert @@tktest.start_btf(lot), 'error starting test lot'
    assert @@tktest.process_btf(lot)
    # set binning data and send the update_sort_result message
    assert sortdata = @@dbbtf.set_lot_wafer_bins(lot, binning_data: @@binning_data, goodbins: [0, 1])
    #
    # regression from 19.05 on, lots goes ONHOLD 'updating binning information failed' after ~ 2 min
    $log.info "waiting #{@@tktest.jobdelay} s to verify lot is not ONHOLD"; sleep @@tktest.jobdelay
    assert_equal 'NOTONHOLD', @@sv.lot_info(lot).states['Lot Hold State'], 'wrong lot hold'
    #
    assert @@tkbtf.update_sort_result(lot), 'error updating sort result by BtfTurnkey'
    assert avlot = @@tktest.get_avlot(lot), "(sv) missing LOT_LABEL for #{lot}"
    clid = @@av.user_parameter('Lot', avlot, name: 'CustomerLotID').value
    assert_empty clid, 'CustomerLotID must be empty' if @@av_gid == 'LOT_LABEL'  # MSR 1564401
    assert_equal 'LOTSHIPD.1', @@av.lot_info(avlot).op, 'wrong op in AsmView'
    assert @@tktest.verify_diecount(avlot, sortdata), 'binning data mismatch in AsmView'
    # MSR 1591196
    $log.info "waiting #{@@tktest.jobdelay} s to verify lot is still not ONHOLD"; sleep @@tktest.jobdelay
    assert_equal 'Waiting', @@av.lot_info(avlot).status, 'wrong lot status'
    assert @@av.user_parameter('Lot', avlot, name: 'OQAGoodDie').valueflag, 'OQAGoodDie not set'
    #
    $setup_ok = true
  end

  def test02_sort2_J ### NOT obsolete  ###, included in test01
    # create test lot and locate it to TTB
    assert lot = @@svtest.new_lot(user_parameters: @@tkparams)
    @@testlots << lot
    assert_equal 0, @@sv.lot_opelocate(lot, 1), 'error locating lot'  # for MSRs 1499295, 1499302 (MDS)
    assert @@tktest.start_btf(lot), 'error starting test lot'
    # process to TKYOQAS.1 in AsmView, same stages as for J + SOR2 after SOR
    stages = {}
    @@tktest.stages['J'].each_pair {|k, v|
      stages[k] = v
      stages['SOR2'] = ['TKYSORT.1', false] if k == 'SOR'
    }
    assert @@tktest.process_btf(lot, stages: stages)
    # overwrite binning data and send the update_sort_result message
    ## not necessary:  assert @@tkbtf.lot_locate(lot, :lotshipdesig)
    assert sortdata = @@dbbtf.set_lot_wafer_bins(lot, binning_data: @@binning_data, goodbins: [0, 1])
    assert @@tkbtf.update_sort_result(lot), 'error updating sort result by BtfTurnkey'
    assert avlot = @@tktest.get_avlot(lot), "missing LOT_LABEL for #{lot}"
    assert_equal 'LOTSHIPD.1', @@av.lot_info(avlot).op, 'wrong op in AsmView'
    assert @@tktest.verify_diecount(avlot, sortdata), 'binning data mismatch in AsmView'
  end

  def test03_nobinning  # JCORP-2355
    assert lot = @@svtest.new_lot(user_parameters: @@tkparams)
    @@testlots << lot
    # process lot to (sv) LOTSHIPDESIG.01, (av) TKYSORT.1
    assert @@tktest.start_btf(lot), 'error starting test lot'
    assert @@tktest.process_btf(lot)  # lot is at (sv) LOTSHIPDESIG.01, (av) TKYSORT.1
    # do not set binning data and send the update_sort_result message
    assert @@tkbtf.update_sort_result(lot), 'error updating sort result by BtfTurnkey'
    assert avlot = @@tktest.get_avlot(lot), "(sv) missing LOT_LABEL for #{lot}"
    assert_equal 'LOTSHIPD.1', @@av.lot_info(avlot).op, '(av) wrong op'
    assert_empty @@av.asm_diecount(avlot).values.flatten, '(av) wrong die count'
  end

  def test04_smoke_L
    # create test lot with TK type L and locate it to TTB
    uparams_l = {'TurnkeyType'=>'L', 'TurnkeySubconIDBump'=>'GG'}
    product = @@av_gid == 'LOT_LABEL' ? @@gid_product_l : @@product_l
    assert lot = @@svtest.new_lot(route: @@route_l, product: product, user_parameters: uparams_l)
    @@testlots << lot
    assert_equal 0, @@sv.lot_opelocate(lot, 1), 'error locating lot'  # for MSRs 1499295, 1499302 (MDS)
    assert @@tktest.start_btf(lot), 'error starting test lot'
    # process it along the BUMPONLY stages to TKYOQAB.1 in AsmView
    assert @@tktest.process_btf(lot)
  end

  def test05_smoke_R3
    # create test lot with TK type L and locate it to TTB
    uparams_r3 = {'TurnkeyType'=>'R3', 'TurnkeySubconIDBump'=>'GG', 'TurnkeySubconIDSort'=>'GG'}
    assert lot = @@svtest.new_lot(user_parameters: uparams_r3)
    @@testlots << lot
    assert_equal 0, @@sv.lot_opelocate(lot, 1), 'error locating lot'  # for MSRs 1499295, 1499302 (MDS)
    assert @@tktest.start_btf(lot, stages: @@tktest.stages['J']), 'error starting test lot'
    # process to DSS (TKYOQAS.1 in AsmView)
    assert @@tktest.process_btf(lot, stages: @@tktest.stages['J'])
  end

  # currently fails due to AsmTurnkey, lot stays at TKYIQAS.1
  def testdev06_smoke_N
    # create test lot with TK type N and locate it to TTB
    uparams_n = {'TurnkeyType'=>'N', 'TurnkeySubconIDBump'=>'GG', 'TurnkeySubconIDSort'=>'GG'}
    assert lot = @@svtest.new_lot(route: @@route_sor2, product: @@product_sor2, user_parameters: uparams_n)
    @@testlots << lot
    assert @@tktest.start_btf(lot), 'error starting test lot'
    # process to OQAS (TKYOQAS.1 in AsmView)
    assert @@tktest.process_btf(lot)
    # locate to LOTSHIPDESIG in SiView, overwrite binning data and send the update_sort_result message
    ## not necessary:  assert @@tkbtf.lot_locate(lot, :lotshipdesig)
    assert sortdata = @@dbbtf.set_lot_wafer_bins(lot, binning_data: @@binning_data, goodbins: [0, 1])
    assert @@tkbtf.update_sort_result(lot), 'error updating sort result by BtfTurnkey'
    assert @@tktest.verify_diecount(lot, sortdata), 'binning data mismatch in AsmView'
  end

  def test10_actions
    ##$setup_ok = false  tc seems to be flaky
    #
    # create lot
    assert lot = @@svtest.new_lot(carrier_category: 'MOL', user_parameters: @@tkparams), 'error creating test lot'
    @@testlots << lot
    assert_equal 0, @@sv.lot_opelocate(lot, 1), 'error locating lot'  # for MSRs 1499295, 1499302 (MDS)
    # process it, else BtfTurnkey cannot handle it
    assert @@sv.claim_process_lot(lot)
    # split off a child and scrap it
    assert lc = @@sv.lot_split(lot, [25])
    assert_equal 0, @@sv.scrap_wafers(lc), "error scrapping #{lc}"
    assert_equal 0, @@sv.wafer_sort(lc, ''), "error removing scrap lot #{lc}"
    assert_equal 0, @@sv.sublottype_change(lc, 'SCRAPQA'), "error changing sublottype #{lc}"
    sleep 10
    assert wait_for(timeout: 60) {@@sv.lot_delete(lc) == 0}, "error deleteting lot #{lc}"
    # split off another child
    assert lc = @@sv.lot_split(lot, 12), 'error splitting lot'
    lots = [lc, lot]
    #
    assert avlot = @@tktest.get_avlot(lot), "missing LOT_LABEL for #{lot}"
    assert avlc = @@tktest.get_avlot(lc), "missing LOT_LABEL for #{lc}"
    @@tktest.stages['J'].keys.each_with_index {|svstage, i|
      $log.info "-- next stage: #{svstage.inspect}"
      # locate and process lots as required
      assert @@tktest.move_wait(lots, svstage), '(av) lot has not been moved'
      # execute and verify non-move actions at first stage
      if i == 0
        # hold
        assert @@sv.lot_info(lc).status != 'ONHOLD', "(sv) #{lc} is ONHOLD"
        assert wait_for {@@av.lot_info(avlc).status != 'ONHOLD'}, "(av) #{lc} is ONHOLD"
        # in SiView: set first hold and release immediately, claim memo with umlauts
        assert_equal 0, @@sv.lot_hold(lc, @@holdreason1, memo: "H\xc4ll\xf6"), "(sv) #{lc} error setting hold"
        assert_equal 0, @@sv.lot_hold_release(lc, @@holdreason1), "(sv) #{lc} error releasing hold"
        # in SiView: set the other holds
        @@holdreasons.each {|r| assert_equal 0, @@sv.lot_hold(lc, r, memo: "H\xc4ll\xf6"), "(sv) #{lc} error setting hold"}
        # wait for the holds (all except first one) in AsmView
        assert wait_for {
          @@av.lot_hold_list(avlc).collect {|h| h.reason}.sort == @@holdreasons
        }, "(av) #{lc} wrong holds, expected #{@@holdreasons}"
        # release last hold
        assert_equal 0, @@sv.lot_hold_release(lc, @@holdreasons.last), "(sv) #{lc} error releasing hold"
        assert wait_for {@@av.lot_hold_list(avlc, reason: @@holdreasons.last).empty?}, "(av) #{lc} error releasing hold"
        # release all other holds
        assert_equal 0, @@sv.lot_hold_release(lc, nil), "(sv) #{lc} error releasing holds"
        assert wait_for {@@av.lot_hold_list(avlc).empty?}, "(av) #{lc} error releasing holds"
        # split
        assert lgc = @@sv.lot_split(lc, 2), "(sv) error splitting #{lc}"
        assert avlgc = @@tktest.get_avlot(lgc), "missing LOT_LABEL for #{lgc}"
        assert wait_for(timeout: 600) {@@av.lot_exists?(avlgc)}, "(av) no STB for #{lgc}"
        assert @@tktest.verify_asmview_lot(lc)
        assert @@tktest.verify_asmview_lot(lgc)
        # merge
        assert_equal 0, @@sv.lot_merge(lc, lgc), "(sv) error merging #{lgc}"
        assert wait_for {@@av.lot_info(avlgc).status == 'EMPTIED'}, "(av) error merging #{lgc}"
        assert @@tktest.verify_asmview_lot(lc)
        # scrap
        assert lgc = @@sv.lot_split(lc, 1)
        assert avlgc = @@tktest.get_avlot(lgc), "missing LOT_LABEL for #{lgc}"
        assert_equal 0, @@sv.scrap_wafers(lgc)
        assert_equal 0, @@sv.wafer_sort(lgc, '')
        assert wait_for(timeout: 600) {
          @@av.lot_exists?(avlgc) && @@av.lot_info(avlgc).status == 'SCRAPPED'
        }, "(av) error scrapping #{lgc}"
        # ERPItem, product, sublottype and customer changes
        assert_equal 0, @@sv.user_parameter_change('Lot', lot, 'ERPItem', @@erpitem2)
        product2 = @@av_gid == 'LOT_LABEL' ? @@gid_product2 : @@product2
        @@sv.schdl_change(lot, product: product2, route: @@svtest.route)
        assert_equal product2, @@sv.lot_info(lot).product, 'error changing product'
        assert_equal 0, @@sv.sublottype_change(lot, @@sublottype2), 'error changing sublottype'
        customer2 = @@av_gid == 'LOT_LABEL' ? @@gid_customer2 : @@customer2
        @@sv.lot_mfgorder_change(lot, customer: customer2)
        assert_equal customer2, @@sv.lot_info(lot).customer, 'error changing customer'
        #
        assert wait_for {@@av.lot_info(avlot).product == product2}, '(av) no product change'
        assert wait_for {@@av.lot_info(avlot).sublottype == @@sublottype2}, '(av) no sublottype change'
        assert wait_for {@@av.lot_info(avlot).customer == customer2}, '(av) no customer change'
        # ERPItem change is picked up along with any other change only (action, product, etc.)
        assert_equal @@erpitem2, @@av.user_parameter('Lot', avlot, name: 'ERPItem').value, '(av) no ERPItem change'
      end
      lots.reverse!
    }
    #
    # lots are at LOTSHIPDESIG in SiView, wait until status is Processing in AsmView (at TKYSORT)
    tstart = Time.now
    [avlot, avlc].each {|l|
      assert wait_for(timeout: 600, tstart: tstart) {
        @@av.lot_info(l).status == 'Waiting' && @@av.user_parameter('Lot', l, name: 'ProcState').value == 'Processing'
      }, "lot #{l} is not 'Processing'"
    }
    #
    # for one lot, send binning data (like FabGUI) with last bin flagged as bad (goodbin=0) and verify in av
    assert sortdata = @@dbbtf.set_lot_wafer_bins(lot, binning_data: @@binning_data, goodbins: [0, 1])
    assert @@tkbtf.update_sort_result(lot)
    assert @@tktest.verify_diecount(avlot, sortdata)
    #
    # for the other lot send binning data with all 3 bins good and 0 qty in bin2, but wrong DieCountGood value
    assert @@dbbtf.set_lot_wafer_bins(lc, binning_data: @@binning_data, set_dcg: false)
    assert !@@tkbtf.update_sort_result(lc), "#{lc} update of sort result must fail"
    msg = @@tkbtf.response_msg[:updateSortResultResponse][:sortResultUpdateReturn][:msg][nil]
    assert msg.include?('inconsistencies between reported die count'), 'wrong msg'
    #
    # correct DieCountGood values, send the binning data again and verify in av
    assert sortdata = @@dbbtf.set_lot_wafer_bins(lc, binning_data: @@binning_data)
    assert @@tkbtf.update_sort_result(lc)
    assert @@tktest.verify_diecount(avlc, sortdata)
    #
    $setup_ok = true
  end

  def test11_fast_moves
    # create lot, move to TTB and wait for STB in AsmView
    assert lot = @@svtest.new_lot(user_parameters: @@tkparams), 'error creating test lot'
    @@testlots << lot
    assert_equal 0, @@sv.lot_opelocate(lot, 1), 'error locating lot'  # for MSRs 1499295, 1499302 (MDS)
    assert @@tktest.start_btf(lot), 'error starting test lot'
    #
    # split off first child
    assert ch1 = @@sv.lot_split(lot, 16), "(sv) error splitting #{lot}"
    stages = @@tktest.stages['J']
    #
    [lot, ch1].each {|l| assert_equal 0, @@sv.lot_opelocate(l, nil, stage: stages.keys[1]), 'error locating lot'}
    [lot, ch1].each {|l| assert wait_for(timeout: 600) {
      @@av.lot_info(@@tktest.get_avlot(l)).op == stages.values[1].first
    }, "(av) #{l} at unexpected PD"}
    [lot, ch1].each {|l| assert @@sv.claim_process_lot(l), "(sv) error processing #{l}"}
    #
    # do no wait for lot move in AsmView
    [lot, ch1].each {|l| assert_equal 0, @@sv.lot_opelocate(l, nil, stage: stages.keys[2]), 'error locating lot'}
    assert(@@sv.claim_process_lot(lot), "(sv) error processing #{lot}") if stages.values[2][1]
    assert ch2 = @@sv.lot_split(ch1, 10), "(sv) error splitting #{ch1}"
    assert(@@sv.claim_process_lot(ch2), "(sv) error processing #{ch2}") if stages.values[2][1]
    #
    # do no wait for lot move in AsmView
    [lot, ch2].each {|l| assert_equal 0, @@sv.lot_opelocate(l, nil, stage: stages.keys[3]), 'error locating lot'}
    assert(@@sv.claim_process_lot(lot), "(sv) error processing #{lot}") if stages.values[3][1]
    assert ch3 = @@sv.lot_split(ch2, 5), "(sv) lot error splitting #{ch2}"
    assert(@@sv.claim_process_lot(ch3), "(sv) error processing #{ch3}") if stages.values[3][1]
    #
    [lot, ch3].each {|l| assert_equal 0, @@sv.lot_opelocate(l, nil, stage: stages.keys[4]), 'error locating lot'}
    #
    $log.info "waiting 15 min for the lots to move in AsmView"; sleep 900
    assert wait_for {@@av.lot_info(@@tktest.get_avlot(ch1)).op == stages.values[2].first}, '(av) lot at wrong PD'
    assert wait_for {@@av.lot_info(@@tktest.get_avlot(ch2)).op == stages.values[3].first}, '(av) lot at wrong PD'
    assert wait_for {@@av.lot_info(@@tktest.get_avlot(ch3)).op == stages.values[4].first}, '(av) lot at wrong PD'
    assert wait_for {@@av.lot_info(@@tktest.get_avlot(lot)).op == stages.values[4].first}, '(av) lot at wrong PD'
  end

  def test12_force_locate_backward  # MSR 735255
    # create lot, move to TTB and wait for STB in AsmView
    assert lot = @@svtest.new_lot(user_parameters: @@tkparams)
    @@testlots << lot
    assert_equal 0, @@sv.lot_opelocate(lot, 1), 'error locating lot'  # for MSRs 1499295, 1499302 (MDS)
    avlot = @@tktest.get_avlot(lot)
    assert @@tktest.start_btf(lot), 'error starting test lot'
    stages = @@tktest.stages['J']
    # move to second stage, processing with running hold
    assert @@tktest.move_wait(lot, stages.keys[1], running_hold: true), "(av) error moving lot #{lot}"
    $log.info "wating up to #{@@jobdelay} s to become ONHOLD in AsmView"
    sleep 20  # wait for ForceOpeComp to get processed to catch the FCHL hold instead of the RNHL
    assert wait_for(timeout: @@jobdelay) {@@av.lot_info(avlot).status == 'ONHOLD'}, "(av) #{lot} is not ONHOLD"
    # locate back to first stage - AsmView is out of sync
    assert_equal 0, @@sv.lot_opelocate(lot, nil, stage: stages.keys.first, force: true), '(sv) error locating lot'
    # release lot and locate to stage 2 - not resynced since 19.04, MSR 1351735
    assert_equal 0, @@sv.lot_hold_release(lot, nil), '(sv) error releasing hold'
    assert_equal 0, @@sv.lot_opelocate(lot, nil, stage: stages.keys[1]), "(sv) error locating #{lot}"
    assert wait_for {@@av.lot_info(avlot).op == stages.values[1].first}, "(av) #{lot} not at expected PD"
    assert wait_for {@@av.lot_info(avlot).status != 'ONHOLD'}, "(av) lot #{lot} is still ONHOLD"  # av released the FCHL
    # lot is set ONHOLD in sv and later av
    $log.info "waiting for the X-BB hold"
    assert wait_for(timeout: 120) {@@sv.lot_hold_list(lot, reason: 'X-BB').first}, '(sv) missing X-BB hold'
    assert wait_for(timeout: @@tktest.jobdelay) {@@av.lot_hold_list(avlot, reason: 'X-BB').first}, '(av) missing X-BB hold'
    # # move to stage 3
    # assert @@tktest.move_wait(lot, stages.keys[2]), "(av) error moving lot #{lot}"
  end

  def test13_force_locate_forward
    # create lot, move to TTB and wait for STB in AsmView
    assert lot = @@svtest.new_lot(user_parameters: @@tkparams)
    @@testlots << lot
    assert_equal 0, @@sv.lot_opelocate(lot, 1), 'error locating lot'  # for MSRs 1499295, 1499302 (MDS)
    avlot = @@tktest.get_avlot(lot)
    assert @@tktest.start_btf(lot), 'error starting test lot'
    stages = @@tktest.stages['J']
    # hold, force locate to second stage, release, process, wait for PD and state
    assert_equal 0, @@sv.lot_hold(lot, @@holdreasons.first), "(sv) #{lot} not ONHOLD"
    assert wait_for {@@av.lot_info(avlot).status == 'ONHOLD'}, "(av) #{lot} not ONHOLD"
    assert_equal 0, @@sv.lot_opelocate(lot, nil, stage: stages.keys[1], force: true)
    assert_equal 0, @@sv.lot_hold_release(lot, nil)
    assert @@tktest.move_wait(lot, stages.keys[1]), "error moving lot #{lot}"
    assert wait_for {
      @@av.lot_info(avlot).status == 'Waiting' && @@av.user_parameter('Lot', avlot, name: 'ProcState').value == 'Processing'
      }, "(av) #{lot} is not 'Processing'"
    # move to third stage
    assert @@tktest.move_wait(lot, stages.keys[2]), "error moving lot #{lot}"
  end

  def test14_split_merge_onhold
    # create lot, move to TTB and wait for STB in AsmView
    assert lot = @@svtest.new_lot(user_parameters: @@tkparams)
    @@testlots << lot
    assert_equal 0, @@sv.lot_opelocate(lot, 1), 'error locating lot'  # for MSRs 1499295, 1499302 (MDS)
    assert avlot = @@tktest.get_avlot(lot)
    assert @@tktest.start_btf(lot), 'error starting test lot'
    # hold and split while ONHOLD
    assert_equal 0, @@sv.lot_hold(lot,  @@holdreasons.first)
    assert lc = @@sv.lot_split(lot, 2, onhold: true)
    avlc = @@tktest.get_avlot(lc)
    assert wait_for {@@av.lot_exists?(avlc) && (@@av.lot_info(avlc).status == 'ONHOLD')}, "(av) #{lc} not on hold"
    # merge while ONHOLD
    assert_equal 0, @@sv.lot_merge(lot, lc)
    assert wait_for {@@av.lot_info(avlc).status == 'EMPTIED'}, "(av) error merging lot #{lc}"
    assert_equal 'ONHOLD', @@av.lot_info(avlot).status, "(av) wrong status of lot #{lot}"
  end

  def test15_child_carrier
    # create lot, move to TTB and wait for STB in AsmView
    assert lot = @@svtest.new_lot(user_parameters: @@tkparams)
    @@testlots << lot
    assert_equal 0, @@sv.lot_opelocate(lot, 1), 'error locating lot'  # for MSRs 1499295, 1499302 (MDS)
    assert @@tktest.start_btf(lot), 'error starting test lot'
    # split 2 children and wait for visibility in asmview
    lcs = 2.times.collect {@@sv.lot_split(lot, 1)}.compact
    assert_equal 2, lcs.count, "(sv) error splitting #{lot}"
    assert avlcs = lcs.collect {|l| @@tktest.get_avlot(l)}
    assert wait_for(timeout: 900) {
      avlcs.inject(true) {|ok, l| ok && @@av.lot_exists?(l)}
    }, "(av) error building #{lcs}"
    # scrap child 1 and verify the carrier is deleted in AsmView
    assert_equal 0, @@sv.scrap_wafers(lcs[0]), "(sv) lot #{lcs[0]} error in scrap"
    assert wait_for {@@av.lot_info(avlcs[0]).status == 'SCRAPPED'}, "(av) lot #{lcs[0]} not scrapped"
    assert wait_for {@@av.carrier_list(carrier: avlcs[0]) == []}, "(av) carrier #{lcs[0]} not deleted"
    # merge lot and child 2 and verify the carrier is deleted in AsmView
    assert_equal 0, @@sv.lot_merge(lot, lcs[1]), "(sv) cannot merge lots #{lot} and #{lcs[1]}"
    assert wait_for {@@av.lot_info(avlcs[1]).status == 'EMPTIED'}, "(av) lot #{lcs[1]} not merged"
    assert wait_for {@@av.carrier_list(carrier: avlcs[1]).empty?}, "(av) carrier #{lcs[1]} not deleted"
  end

  def test16_child_sublottype_part_customer_change # MSR 819362
    ### create lot and process it, else BtfTurnkey cannot handle it
    # create lot and gatepass it, else BtfTurnkey cannot handle it
    assert lot = @@svtest.new_lot(user_parameters: @@tkparams)
    @@testlots << lot
    assert avlot = @@tktest.get_avlot(lot)
    assert @@sv.lot_gatepass(lot, mandatory: true)
    stages = @@tktest.stages['J']
    # split off three child lots
    assert lcs = 3.times.collect {@@sv.lot_split(lot, 2)}, "(sv) error splitting #{lot}"
    assert avlcs = lcs.collect {|l| @@tktest.get_avlot(l)}
    # move parent to TTB and verify the whole lot family is created in AsmView
    assert_equal 0, @@sv.lot_opelocate(lot, nil, stage: stages.keys.first), "(sv) error locating #{lot}"
    assert wait_for(timeout: 900) {
      (avlcs + [avlot]).inject(true) {|ok, l| ok && @@av.lot_exists?(l)}
    }, "(av) error building #{lcs}"
    # child 1: change part name
    product2 = @@av_gid == 'LOT_LABEL' ? @@gid_product2 : @@product2
    @@sv.schdl_change(lcs[0], product: product2, route: @@svtest.route)
    assert_equal product2, @@sv.lot_info(lcs[0]).product, "(sv) product for #{lcs[0]} not changed"  # fails for GID_SOURCE
    # child 2: change sublot type
    assert_equal 0, @@sv.sublottype_change(lcs[1], @@sublottype2), "(sv) sublottype for #{lcs[1]} not changed"
    # child 3: change customer code
    customer2 = @@av_gid == 'LOT_LABEL' ? @@gid_customer2 : @@customer2
    @@sv.lot_mfgorder_change(lcs[2], customer: customer2)
    assert_equal customer2, @@sv.lot_info(lcs[2]).customer, "(sv) customer for #{lcs[2]} not changed"
    #
    # move lcs into turnkey area to the 2nd stage and wait for BTFTurnkey to pick up the changes
    lcs.each {|l| assert_equal 0, @@sv.lot_opelocate(l, nil, stage: stages.keys[1]), "(sv) error locating #{l}"}
    avlcs.each {|l| assert wait_for {@@av.lot_info(l).status != 'NonProBank'}, "(av) error locating #{l}"}
    #
    # verify lot info in AsmView
    $log.info "waiting for lot changes to appear in AsmView"
    tstart = Time.now
    assert wait_for(timeout: 300, tstart: tstart) {
      @@av.lot_info(avlcs[0]).product == product2
    }, "(av) #{lcs[0]} partname not changed to #{product2}"
    assert wait_for(timeout: 300, tstart: tstart) {
      @@av.lot_info(avlcs[1]).sublottype == @@sublottype2
    }, "(av) #{lcs[1]} sublottype not changed to  #{@@sublottype2}"
    assert wait_for(timeout: 300, tstart: tstart) {
      @@av.lot_info(avlcs[2]).customer == customer2
    }, "(av) #{lcs[2]} customer not changed to #{customer2}"
  end

  # does not yet work for F8 lots in Btf
  def test21_U_to_J
    # create lot type U and locate it to TTB
    assert lot = @@svtest.new_lot(user_parameters: {'TurnkeyType'=>'U'})
    @@testlots << lot
    assert avlot = @@tktest.get_avlot(lot)
    assert_equal 0, @@sv.lot_opelocate(lot, nil, stage: 'TTB', offset: -1)
    assert_equal 0, @@sv.lot_gatepass(lot, mandatory: true)
    $log.info "waiting #{@@jobdelay} s to verify lot #{lot} does not appear in AsmView"; sleep @@jobdelay
    refute @@av.lot_exists?(avlot), 'lot must not appear in AsmView'
    # change tktype to J, stage move as trigger event
    assert_equal 0, @@sv.user_parameter_change('Lot', lot, @@tkparams)
    assert_equal 0, @@sv.lot_opelocate(lot, nil, stage: 'UBM'), 'error locating lot'
    $log.info "verifying lot #{lot} appears in AsmView"
    assert wait_for(timeout: @@jobdelay) {@@av.lot_exists?(avlot)}, 'lot must appear in AsmView'
  end

  def test22_U_J_U
    # create lot type U and locate it to TTB
    assert lot = @@svtest.new_lot(user_parameters: {'TurnkeyType'=>'U'})
    @@testlots << lot
    assert avlot = @@tktest.get_avlot(lot)
    assert_equal 0, @@sv.lot_opelocate(lot, nil, stage: 'TTB', offset: -1)
    assert_equal 0, @@sv.lot_gatepass(lot, mandatory: true)
    $log.info "waiting #{@@jobdelay} s to verify lot #{lot} does not appear in AsmView"; sleep @@jobdelay
    refute @@av.lot_exists?(avlot), 'lot must not appear in AsmView'
    # change tktype to J and back to J, stage move as trigger event
    assert_equal 0, @@sv.user_parameter_change('Lot', lot, @@tkparams)
    sleep 10
    assert_equal 0, @@sv.user_parameter_change('Lot', lot, {'TurnkeyType'=>'U'})
    assert_equal 0, @@sv.lot_opelocate(lot, nil, stage: 'UBM'), 'error locating lot'
    $log.info "waiting #{@@jobdelay} s to verify lot #{lot} does not appear in AsmView"; sleep @@jobdelay
    refute @@av.lot_exists?(avlot), 'lot must not appear in AsmView'
  end

  def test23_downgrade_slow
    # create lot and move it to TTB
    assert lot = @@svtest.new_lot(user_parameters: @@tkparams)
    @@testlots << lot
    assert avlot = @@tktest.get_avlot(lot)
    assert @@tktest.start_btf(lot), 'error starting test lot'
    # scrap lot and wait for AsmView
    assert_equal 0, @@sv.lot_note_register(lot, 'ScrapExcursion', 'QA Test')  # prevent jCAP AutoScrap from canceling
    assert_equal 0, @@sv.scrap_wafers(lot)
    assert wait_for(timeout: @@jobdelay) {@@av.lot_info(avlot).status == 'SCRAPPED'}
    # downgrade lot
    assert_equal 0, @@sv.user_parameter_change('Lot', lot, {'TurnkeyType'=>'U', 'ERPItem'=>'ENGINEERING-U00'})
    sltold = @@sv.lot_info(lot).sublottype
    assert_equal 0, @@sv.sublottype_change(lot, 'EQ')
    # scrap cancel
    assert_equal 0, @@sv.scrap_wafers_cancel(lot)
    $log.info "waiting #{@@jobdelay} s to get all changes processed"; sleep @@jobdelay
    assert_equal @@svtest.erpitem, @@av.user_parameter('Lot', avlot, name: 'ERPItem').value
    assert_equal sltold, @@av.lot_info(avlot).sublottype
    assert_equal 'SCRAPPED', @@av.lot_info(avlot).status
  end

  def test24_downgrade_fast
    # create lot and move it to TTB
    assert lot = @@svtest.new_lot(user_parameters: @@tkparams)
    @@testlots << lot
    assert avlot = @@tktest.get_avlot(lot)
    assert @@tktest.start_btf(lot), 'error starting test lot'
    # downgrade scrap and scrap cancel lot
    assert_equal 0, @@sv.scrap_wafers(lot)
    sleep 10  # for SiView wtdg (!)
    assert_equal 0, @@sv.user_parameter_change('Lot', lot, {'TurnkeyType'=>'U', 'ERPItem'=>'ENGINEERING-U00'})
    sltold = @@sv.lot_info(lot).sublottype
    assert_equal 0, @@sv.sublottype_change(lot, 'PR')
    sleep 10  # for SiView wtdg (!)
    assert_equal 0, @@sv.scrap_wafers_cancel(lot)
    # ensure lot has the old sublottype and stays scrapped in AsmView
    $log.info "waiting #{@@jobdelay} s to get all changes processed"; sleep @@jobdelay
    assert_equal @@svtest.erpitem, @@av.user_parameter('Lot', avlot, name: 'ERPItem').value
    assert_equal sltold, @@av.lot_info(avlot).sublottype
    assert_equal 'SCRAPPED', @@av.lot_info(avlot).status
  end

  def test31_LOT_LABEL
    skip 'not relevant for GID_SOURCE' unless @@av_gid.nil?
    # create test lot
    assert lot = @@svtest.new_lot(user_parameters: @@tkparams)
    @@testlots << lot
    # set LOT_LABEL
    assert_equal 0, @@sv.user_data_update(:lot, lot, {'LOT_LABEL'=>'QAQA'})
    # set CustomerWaferIDs (may already be set for customer qgt)
    li = @@sv.lot_info(lot, wafers: true)
    li.wafers.each {|w|
      assert_equal 0, @@sv.user_parameter_change('Wafer', w.wafer, 'CustomerWaferID', "QA#{w.wafer}")
    }
    # locate lot to BTF (incl first operation)
    assert @@tktest.start_btf(lot), 'error starting test lot'
    # verify BTFTurnkey sent the LOT_LABEL and AsmTurnkey wrote it to the lot script parameter
    assert wait_for(timeout: 120) {
      @@av.user_parameter('Lot', lot, name: 'CustomerLotID').value == 'QAQA'
    }, '(av) wrong LOT_LABEL'
    # verify AsmTurnkey got the CustomerWaferIDs from Turnkey
    li.wafers.each {|w|
      assert_equal @@sv.user_parameter('Wafer', w.wafer, name: 'CustomerWaferID').value, 
                   @@av.user_parameter('Wafer', w.wafer, name: 'CustWaferId').value, '(av) wrong CustWaferId'
    }
    # change LOT_LABEL and verify the change gets picked up with the next move
    assert_equal 0, @@sv.user_data_update(:lot, lot, {'LOT_LABEL'=>'QAQA2'})
    t = 750  # 600 short, 900 works, 480 according to properties
    $log.info "waiting #{t} s for the lot info cache to expire"; sleep t
    assert @@tktest.move_wait(lot, 'UBM'), '(av) lot not moved'
    assert wait_for(timeout: 120) {
      @@av.user_parameter('Lot', lot, name: 'CustomerLotID').value == 'QAQA2'
    }, '(av) wrong LOT_LABEL'
  end

  def test35_happy_backup
    skip 'not relevant for GID_SOURCE' unless @@av_gid.nil?
    @@svtest_src = SiView::Test.new(@@env_src, @@svsrc_defaults)
    @@sv_src = @@svtest_src.sv
    # create test lot at src site, send it to backup and move to TTB
    assert lot = @@svtest_src.new_lot(user_parameters: @@tkparams)
    @@testlots_bop << lot
    li = @@sv_src.lot_info(lot)
    assert_equal 'FF', @@sv_src.user_data(:productgroup, li.productgroup)['RouteType'], 'wrong route type'
    assert @@sv_src.backup_prepare_send_receive(lot, @@sv, @@psend_rcv), 'error receiving backup lot'
      # backup_op: @@backup_op, backup_bank: @@backup_bank, carrier_category: 'C4')
    # FabGUI (?) receiving, set ERPItem and TurnKey lot script params  TODO: set COO and SourceFabCode (AsmView issues with F8/US (?))
    assert_equal 0, @@sv.user_parameter_change('Lot', lot, @@tkparams.merge('ERPItem'=>@@svtest_src.erpitem))
    # lot appears in AsmView (includes call to src JCAPRemote.getLotInfo)
    assert @@tktest.start_btf(lot), 'error starting test lot, MQ issues?'
    # process to DSS (TKYOQAS.1 in AsmView)
    assert @@tktest.process_btf(lot), 'error processing lot, setup error?'
  end


  # basic GID_SOURCE tests

  def test41_GID_LotScriptParam
    skip 'not relevant for GID_SOURCE' unless @@av_gid.nil?
    # create test lot, locate it to TTB and start in BTF -> LOT_LABEL lot is created in AsmView
    assert lot = @@svtest.new_lot(customer: @@gid_customer, user_parameters: @@tkparams)
    assert_equal 0, @@sv.user_parameter_change('Lot', lot, 'GID_SOURCE', 'LOT_LABEL')
    @@testlots << lot
    assert @@tktest.start_btf(lot, av_gid: 'LOT_LABEL'), 'error starting test lot'
  end

  def test42_GID_MaskSetCustomer
    skip 'not relevant for GID_SOURCE' unless @@av_gid.nil?
    # create test lot, locate it to TTB and start in BTF -> LOT_LABEL lot is created in AsmView
    assert lot = @@svtest.new_lot(customer: @@gid_customer, product: @@gid_product, user_parameters: @@tkparams)
    @@testlots << lot
    assert @@tktest.start_btf(lot, av_gid: 'LOT_LABEL'), 'error starting test lot'
  end
  
  def test61_multiple_events_on_same_claim_time
    # MSR1509410
    assert lot = @@svtest.new_lot(user_parameters: @@tkparams)
    @@testlots << lot
    assert avlot = @@tktest.get_avlot(lot)
    # process lot to (sv) LOTSHIPDESIG.01, (av) TKYSORT.1
    assert @@tktest.start_btf(lot), 'error starting test lot'
    assert @@tktest.process_btf(lot, stop_before: 'SLD')
    #
    # assert @@sv.lot_opelocate(lot, '6310.1900'), 'locate to 6310.1900 (BUMPDEVDI.01) failed!'
    assert_equal 0, @@sv.lot_opelocate(lot, nil, op: 'BUMPDEVDI.01'), '(sv) error locating lot'
    assert wait_for(timeout: 320) {
      avli = @@av.lot_info(avlot)
      avli.status == 'ONHOLD' && avli.op == 'TKYRW1.1'
    }, "(av) lot is not at op TKYRW1.1 and ONHOLD"
  end
  
  def test71_partial_rework_with_rework_cancel
    partial_rework(locate: true, rework_cancel: true)
  end
  
  def test72_partial_rework_without_hold_release
    partial_rework(hold1: true)
  end
  
  def test73_partial_rework_without_hold_release_multiple_holds
    partial_rework(hold1: true, hold2: true)
  end
  
  def test74_partial_rework_without_hold_release_with_futurehold
    partial_rework(hold1: true, futurehold: true)
  end
  
  def test75_partial_rework_with_hold_release
    partial_rework(hold1: true, hold2: true, holdrelease: true)
  end
  

  # aux methods
  
  def partial_rework(hold1: false, hold2: false, holdrelease: false, locate: false, rework_cancel: false, futurehold: false)
    # MSR1518369
    # create lot and move it to stage UBM
    assert lot = @@svtest.new_lot(user_parameters: @@tkparams)
    @@testlots << lot
    assert_equal 0, @@sv.lot_opelocate(lot, 1), 'error locating lot' if locate  # for MSRs 1499295, 1499302 (MDS)
    assert avlot = @@tktest.get_avlot(lot)
    assert @@tktest.start_btf(lot), 'error starting test lot'
    assert @@tktest.move_wait(lot, @@tktest.stages['J'].keys[1])
    # set holds
    assert_equal 0, @@sv.lot_hold(lot, 'AEHL') if hold1
    assert_equal 0, @@sv.lot_hold(lot, 'APCF') if hold2
    # partial rework
    assert lc = @@sv.lot_partial_rework(lot, 2, @@route_rwk, onhold: (hold1 || hold2), release: holdrelease)
    assert avlc = @@tktest.get_avlot(lc)
    assert_equal 0, @@sv.lot_hold_release(lc, 'AEHL'), "hold release for child #{lc} failed!" if hold1 && !holdrelease
    assert_equal 0, @@sv.lot_hold_release(lc, 'APCF'), "hold release for child #{lc} failed!" if hold2 && !holdrelease
    assert wait_for {
      @@av.lot_exists?(avlc) && @@av.lot_info(avlc).op == @@av.lot_info(avlot).op
    }, "(av) child lot #{avlc} is not at #{@@av.lot_info(avlot).op}"
    assert_equal 0, @@sv.lot_futurehold(lc, 'RW1.e500', 'C-ENG') if futurehold
    holds_parent = hold2 ? ['AEHL', 'APCF', 'XREH'] : hold1 ? ['AEHL', 'XREH'] : ['XREH']
    holds_parent = ['XREH'] if holdrelease
    $log.info '-- verifying parent holds after partial rework'
    assert check_holds(avlot, holds_parent), "wrong lot holds for #{avlot}"
    if rework_cancel
      # partial rework cancel
      assert_equal 0, @@sv.lot_partial_rework_cancel(lot, lc)
      assert wait_for {@@av.lot_info(avlc).status == 'EMPTIED'}, "(av) child #{avlc} not merged"
      assert wait_for {@@av.lot_info(avlot).status != 'ONHOLD'}, "(av) lot #{avlot} is ONHOLD"
      # partial rework again
      assert lc = @@sv.lot_partial_rework(lot, 2, @@route_rwk)
      assert avlc = @@tktest.get_avlot(lc)
      assert wait_for {@@av.lot_exists?(avlc) && @@av.lot_info(avlc).op == @@av.lot_info(avlot).op}
      assert wait_for {@@av.lot_info(avlot).status == 'ONHOLD'}, "lot #{lot} is not ONHOLD"
    end
    # process child to return op and merge
    while @@sv.lot_info(lc).route == @@route_rwk
      assert_equal 0, @@sv.lot_gatepass(lc, mandatory: true)
      assert_equal 0, @@sv.lot_hold_release(lc, 'C-ENG'), "hold release for child #{lc} failed" if futurehold && @@sv.lot_info(lc).op == 'UTBPD04.01'
    end
    $log.info '-- verifying parent holds before merge'
    assert check_holds(avlot, holds_parent), "wrong lot holds for #{avlot}"
    assert_equal 0, @@sv.lot_merge(lot, lc)
    $log.info '-- verifying parent holds after merge'
    assert check_holds(avlot, holds_parent), "wrong lot holds for #{avlot}"
    assert wait_for {@@av.lot_info(avlc).status == 'EMPTIED'}, "(av) lot #{lc} not merged"
    assert_equal @@tktest.stages['J'].values[1].first, @@av.lot_info(avlot).op
  end

  def check_holds(avlot, reasons)
    return wait_for {
      hh = @@av.lot_hold_list(avlot)
      next(false) if hh.empty?
      ok = (reasons.size == hh.size)
      hh.each {|hold|
        reason = reasons.find {|h| h == hold.reason}
        ($log.warn "(av) unexpected hold for lot #{avlot}: #{hold.inspect}"; ok=false) if reason.nil?
      }
      ok
    }
  end

end
