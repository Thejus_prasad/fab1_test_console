=begin
Automated testing of the ETSpace parser.
Requirements:

(c) Copyright 2011 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Authors: Adel Darwaish, 2013-04-30
Note: this test program will be exeucted under the production enviornement. No data changes or process executions will be done.
Just connect to the ETSPace and read some data where some channels and their properties and infos can be gathered.
=end

require "RubyTestCase"
require "etspacetest"
require "misc"


class Test_ETSpace_Downtime < RubyTestCase
  Description = "Test ET Space/SIL"

  # @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ change constants data to match the production enviornment.     @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
  # under construction...............................................................
  @@technology = "QA"
  @@pg = "PGQA1"
  @@op1 = "FINA-FWET.01"
  @@op2 = "M1-SWET.01"
  @@pg2 = "SHELBY2-1F"
  # @@product1 = "QAPRODUCT.A0"
  @@product1 = "SHELBY21AD.B1"
  @@product2 = "SHELBY21AD.B2"
  @@product3 = "SHELBY21AD.B3"
  @@qa_pgqa1_fwet = "/QA/PGQA1/FINA-FWET.01"
  #
  @@opNo = "5600.4600"
  @@eqp = "ATC102"    # etester machine
  @@lot = "U89K6.00"
  #
  @@corrective_actions = ["AutoLotHold", "LotHold", "ReleaseLotHold","CancelLotHold"]
  @@local_dir = 'tmp/etspace'
  @@folder = ''
  #
  @@site_count = 17
  @@parameter_count = 10
  @@curr_sample_no = 0
  @@slot_id = 5
  @@prio = 0
  #
  @@wait_parser = 4800
  @@wait_router = 4800
  # limits
  @@lcl = -3
  @@center = 0.0
  @@ucl = 3
  @@lsl = -5
  @@usl = 5

  @@file_name = "FAB1_qgt_SHELBY21AD.B2_U282V.00_FINA-FWET.01_UTF001-20130312-0009"
  @@next_test_opNo = "5600.4750"
  @@next_test_pd = "FIN-FWETSEP.01"

  #
  def test00_setup
    $setup_ok = false
    #
    ($spc.close; $ptest = nil) if $spc and $spc.env != $env
    $ptest = ETSpace::TestParser.new($env, :nctype=>:nc) unless $ptest and $ptest.env == $env
    $dis = $ptest.dis
    $spc = $ptest.spc
    $lds = $spc.lds("ET_Fab1")
    refute_nil $lds, "LDS not found"
    @@folder = ["", @@technology, @@pg, @@op1].join('/')
    @@folder2 = ["", @@technology, @@pg2, @@op2].join('/')
    $log.info "using folder #{@@folder}"
    $log.info "using folder2 #{@@folder2}"
    $folder = $lds.folder(@@folder)
    $folder2 = $lds.folder(@@folder2)
    ($log.info "folder #{f} does not exist"; next) unless $folder
    #
    $setup_ok = true
  end

 def test100_get_etspace_channels_info
    site_values = []
    values = []
    ucl_sigma = 0.6
    my_prios = []
    previous_params_samples = []
    expect_params_samples = []
    #
    $log.info "get one sample channel info for parameter Param_0001. wait..."
    sleep 30
    ch = $folder.spc_channel(:parameter=>"Param_0001")
    refute_nil ch, "returned nil channel for param Param_0001"
    #
    param_names = (1..@@parameter_count).collect {|i| "Param_%4.4d" % i}
    $log.info "param_names: #{param_names.inspect}"
    #
    # override control limits
    cs = $folder.spc_channels
    cs.each{|ch|
      next if !param_names.member?(ch.parameter)
      $log.info " channel #{ch.parameter}"
      fields = ch.customer_fields.merge("Criticality"=>"Critical", "Maverick check"=>"1;1;15", "Maverick-Action"=>"email+hold", "AQL-Action"=>"email+hold")
    }
    #
    # excluded parameters will not have new samples populated to space
    expect_params_samples = all_ch.collect{|ch|
      if excluded_params != nil
        if excluded_params.member?(ch.parameter)
          s= ch.samples(:days=>30).size
        else
          s= ch.samples(:days=>30).size + 1
        end
      else
        s= ch.samples(:days=>30).size + 1
      end
    }
    $log.info "prepare package..."
    #
    # send data package
    # wait until all expected samples are populated successfully
    assert $ptest.check_channel_samples(expect_params_samples, param_names, :waiting_time=>700), "check samples failed"
    #
    result = $ptest.extract_sample_chart_values("Result_AQL", -1)
    assert result[:mean] != 0, "no AQL violation found"
    #
    fhl = check_futurehold(@@lot)
    assert fhl.size > 0, "no future folds for lot #{@@lot}"
    # ensure there is at least one fh with reason = "X-E1"
    etspace_fh = false
    fh_op = ""
    fhl.each {|fh|
      if fh.reason.start_with?("X-E")
        etspace_fh = true
        fh_op = fh.rsp_op
        break
      end
    }
  end

 end
