=begin
Automated testing of the ETSpace parser.
Requirements:

(c) Copyright 2011 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Authors: Adel Darwaish, 2012-08-30
=end

require "RubyTestCase"
require "etspacetest"
require "misc"


class Test_ETSpace_Parser02 < RubyTestCase
  Description = "Test ET Space/SIL"

  @@technology = "QA"
  @@pg = "PGQA1"
  @@op1 = "FINA-FWET.01"
  @@op2 = "M1-SWET.01"
  @@pg2 = "SHELBY2-1F"
  @@product1 = "SHELBY21AD.B1"
  @@product2 = "SHELBY21AD.B2"
  @@product3 = "SHELBY21AD.B3"
  @@qa_pgqa1_fwet = "/QA/PGQA1/FINA-FWET.01"
  #
  @@lots = ["U282V.00","U9018.00", "U1372.00", "U136Z.00", "U0028.00", "U0030.00"]
  @@lots_info =
  { "U282V.00"=>[@@product1, @@op1],
    "U9018.00"=>[@@product2, @@op1],
    "U1372.00"=>[@@product1, @@op1],
    "U136Z.00"=>[@@product2, @@op1],
    "U0028.00"=>[@@product3, @@op1],
    "U0030.00"=>[@@product2, @@op1]
  }
  #
  @@opNo = "5600.4600"
  @@eqp = "ATC102"    # etester machine
  @@lot = "U89K6.00"
  #
  @@corrective_actions = ["AutoLotHold", "LotHold", "ReleaseLotHold","CancelLotHold"]
  @@local_dir = 'tmp/etspace'
  @@folder = ''
  #
  @@site_count = 17
  @@parameter_count = 10
  @@curr_sample_no = 0
  @@slot_id = 5
  @@prio = 0
  #
  @@wait_parser = 4800
  @@wait_router = 4800
  # limits
  @@lcl = -3
  @@center = 0.0
  @@ucl = 3
  @@lsl = -5
  @@usl = 5

  @@file_name = "FAB1_qgt_SHELBY21AD.B2_U282V.00_FINA-FWET.01_UTF001-20130312-0009"
  @@next_test_opNo = "5600.4750"
  @@next_test_pd = "FIN-FWETSEP.01"

  def test00_setup
    $setup_ok = false
    #
    ($spc.close; $ptest = nil) if $spc and $spc.env != $env
    $ptest = ETSpace::TestParser.new($env, :nctype=>:nc) unless $ptest and $ptest.env == $env
    $dis = $ptest.dis
    $spc = $ptest.spc
    $lds = $spc.lds("ET_Fab1")
    refute_nil $lds, "LDS not found"
    @@folder = ["", @@technology, @@pg, @@op1].join('/')
    @@folder2 = ["", @@technology, @@pg2, @@op2].join('/')
    $log.info "using folder #{@@folder}"
    $log.info "using folder2 #{@@folder2}"
    $folder = $lds.folder(@@folder)
    $folder2 = $lds.folder(@@folder2)
    ($log.info "folder #{f} does not exist"; next) unless $folder
    #
    $setup_ok = true
  end

  def test100_create_varify_channels_nc
    # clean up channels
    $ptest.delete_channels()
    #
    # send data
    create_data_packages("nc", false)
    assert check_results(true), "failed to get results"
  end

  def test101_create_channels_mnc
    #
    # delete channels to clean up
    # $ptest.delete_channels()
    #
    # send data
    create_data_packages("mnc", false)
    assert check_results(true, :timeout=>10), "failed get results"
  end

  def test102_set_channels_corrective_actions
    $ptest.set_corrective_actions(@@corrective_actions)
  end

  def test103_multiple_samples_different_lots_single_wafer_nc
    values = []
    #
    # delete channels to clean up
    $ptest.delete_channels()
    #
    # create data
    create_data_packages("nc", false)
    assert check_results(), "failed to get results"
    #
    # check that each parameter/channel has 3 samples
    channels = $folder.spc_channels
    channels.each{|ch|
      assert_equal 1, ch.samples.size, "wrong sample count for channel #{ch.name}"
    }
  end

  def test104_multiple_samples_different_lots_single_wafer_mnc
    values = []
    #
    # delete channels to clean up
    $ptest.delete_channels()
    #
    # send data
    create_data_packages("mnc", false)
    assert check_results(), "failed to get results"
    #
    # check that each parameter/channel has 1 samples
    channels = $folder.spc_channels
    channels.each{|ch|
      assert_equal 1, ch.samples.size, "wrong sample count for channel #{ch.name}"
    }
  end

  # create mutiple channel values with a spec limit violation on the first channel parameter
  # at the end of creation set corrective actions
  def test105_multiple_samples_mnc_spec_violation
    values = []
    #
    # delete channels to clean up
    $ptest.delete_channels()
    #
    # send data
    create_data_packages("mnc", true)
    assert check_results(), "failed to get results"
    #
    # set corrective actions
    f = @@qa_pgqa1_fwet
    $folder =  $lds.folder(f)
    set_corrective_actions(@@corrective_actions)
  end

  # 1. clean up existing channels
  # 2. create one data package with start valid parameters to create new channels
  # 3. create another sample of the same data package to add new values to the existing channels with violation on parameter 1
  # 4. check if space has assigned a future hold for the related lot.
  def test106_mnc_control_limit_violation_auto_ca
    values = []
    #
    # delete channels to clean up
    $ptest.delete_channels()
    # cancel lot future holds
    def_lot=@@lot
    assert 0 , $ptest.cancel_futrueholds(def_lot), "could not cancel future holds" unless $ptest.check_futurehold(def_lot) == []
    #
    # create channels with defaul data
    create_data_packages("mnc", false, :what=>:parser)
    assert check_results(), "failed to get results"
    #
    # set corrective actions
    set_corrective_actions(@@corrective_actions)
    #
    # set control limits for all parameter (channels) of the folder.
    # second param is the desired value for the MEAN_VALUE_UCL limit
    cs = $folder.spc_channels
    cs.each{|c|
      c.set_limits({"MEAN_VALUE_UCL"=>60, "MEAN_VALUE_CENTER"=>1.0, "MEAN_VALUE_LCL"=>0.0})
    }
    #
    # Send new samples data values with control limit violation of parameter 1
    create_data_packages("mnc", false, :value1=>72, :what=>:parser)
    assert check_results(), "failed to get results"
    # wait until all expected samples are populated successfully
    limit_data = $ptest.get_param_names()
    $ptest.check_channel_samples(2,limit_data, :waiting_time=>300)
    #
    # check lots future hold
    fhl = $ptest.check_futurehold(@@lot)
    refute_equal 0 , fhl.size, "no future holds for lot #{@@lot}"
    assert_equal 1, fhl.size, "more than 1 future hold found for lot #{@@lot}"
    assert_equal "X-E1", fhl[0].reason, "wrong future hold reason found for lot #{@@lot}"
  end

  # 1. clean up existing channels
  # 2. create one data package with start valid parameters to create new channels
  # 3. create another sample of the same data package to add new values to the existing channels with violation on parameter 1
  # 4. check if space has assigned a future hold for the related lot.
  def test107_mnc_control_limit_violation_manual_ca
    values = []
    #
    # delete channels to clean up
    $ptest.delete_channels()
    # cancel lot future holds
    def_lot=@@lot
    assign 0 , $ptest.cancel_futrueholds(def_lot), "could not cancel future holds" unless $ptest.check_futurehold(def_lot) == []
    #
    # create channels with defaul data
    create_data_packages("mnc", false)
    assert check_results(), "failed to get results"
    #
    # set corrective actions
    $ptest.set_corrective_actions([@@corrective_actions[1], @@corrective_actions[2]])
    #
    # set control limits for all parameter (channels) of the folder.
    # second param is the desired value for the MEAN_VALUE_UCL limit
    cs = $folder.spc_channels
    cs.each{|c|
      c.set_limits({"MEAN_VALUE_UCL"=>60, "MEAN_VALUE_CENTER"=>1.0, "MEAN_VALUE_LCL"=>0.0})
    }
    #
    # Send new samples data values with control limit violation of parameter 1
    create_data_packages("mnc", false, :value1=>72)
    assert check_results(), "failed to get results"
    # no lot hold expected
    fhl = $ptest.check_lothold(@@lot)
    assert_equal 0 , fhl.size, "lot holds for lot #{@@lot} found"
    #
    # assign or execute the corrective action "LotHold"
    channels = $folder.spc_channels
    channels.each{|ch|
      s = ch.samples(:ckc=>true)[-1]
      #  set LotHold
      assert s.assign_ca($spc.corrective_action(@@corrective_actions[1])), "error assigning corective action";
    }
    # no lot hold expected
    fhl = $ptest.check_futurehold(@@lot, :reason=>"X-E1")
    assert fhl.size > 0 , "no future holds for lot #{@@lot} with reason code = X-E1 found"
  end

  # manual corrective actions can be executed over the space api
  def test108_execute_corrective_actions
    channels = $folder.spc_channels
    channels.each{|ch|
      s = ch.samples(:ckc=>true)[-1]
      #
      # CancelLotHold (from automatic action)
      assert s.assign_ca($spc.corrective_action("CancelLotHold")), "error assigning corective action";
    }
    # check lots future hold
    fhl = $ptest.check_futurehold(@@lot, :reason=>"X-E1")
    assert_equal 0 , fhl.size, "future holds of reason = X-E1 found for lot #{@@lot}"
  end

  # do not delete existing channels
  def test109_multiple_control_limit_violations
    values = []
    #
    # create channels with defaul data
    create_data_packages("mnc", false, :value1=>72, :value2=>80)
    assert check_results(), "failed to get results"
  end

  def test110_limit_with_trgt_column
    # delete channels to clean up
    $ptest.delete_channels()
    #
    # create channels with defaul data
    create_data_packages("mnc", false, :trgt=>120)
    assert check_results(), "failed to get results"
  end

  # Test what action is to be taken by ETParser when for a recieved .nc/mnc file no matching Wet Step mapping information
  # in parserPath.xml configuration file exist.
  # use an mnc with a not configured PD in the parserPath.xml
  def test111_spccheck_mnc_with_invalid_PD
  end

 def Xtest_999_check_channels
    # send data
    def_lot=@@lot
    with_spec_violation = true
    $log.info "with_spec_violation = #{with_spec_violation}"
    # read limits data out of limit file for comparison
    dir = File.expand_path(@@local_dir)
    $dis.read(@@file_name, dir)
    data = []
    limit_data = $dis.limit[0].limitfile_to_data
    refute_nil limit_data, "returns nil limit_data"
    limits = limit_data[:wet_limit].to_a
    limits.each{|l|
      data << l.to_string
    }
    limit_data = data
    #
    # check parameters names in space
    if with_spec_violation
      limit_data.each {|l|
        # get the name of the parameter
        parameter_name = l.split('|')[1]
        if (parameter_name != "Param_0001")
          refute_nil $folder.spc_channel(:parameter=>parameter_name), "limit parameter #{parameter_name} not populated to space"
        else
          $log.info "parameter Param_0001 shall not be populated"
          ch = $folder.spc_channel(:parameter=>parameter_name)
          $log.info "ch = #{ch.inspect}, type = #{ch.type}"
          assert ch == nil, "limit parameter #{parameter_name} is populated to space"
        end
      }
    else
      limit_data.each {|l|
        # get the name of the parameter
        parameter_name = l.split('|')[1]
        refute_nil $folder.spc_channel(:parameter=>parameter_name), "limit parameter #{parameter_name} not populated to space"
      }
    end
  end


  #
  # **************** Test 2 NextTestPD	 START ****************************************************************************************************
  def test200_valid_NextTestPD_etparser_nc
    create_check_package_with_NextTestPD(@@op1, :nc, :parser2)
  end

  def test201_invalid_NextTestPD_etparser_nc
    create_check_package_with_NextTestPD("123%$/&", :nc, :parser2)
  end

  def test202_valid_NextTestPD_etparser_mnc
    create_check_package_with_NextTestPD(@@op1, :mnc, :parser2)
  end

  def test203_invalid_NextTestPD_etparser_mnc
    create_check_package_with_NextTestPD("123%$/&", :mnc, :parser)
  end

  def test204_valid_NextTestPD_aql_violation_with_CA_FutureHold_another_PD_mnc
    site_values = []
    values = []
    ucl_sigma = 0.6
    my_prios = []
    previous_params_samples = []
    expect_params_samples = []
    #
    # create basic channels
    assert $ptest.create_verify_data(1, false, :what=>:parser2, :lot=>@@lot, :slot=>@@slot_id, :nctype=>:mnc, :slot=>@@slot_id, :op=>@@op1, :product=>@@product1)
    # prepare data values for the next package
    (0..16).each {|i|
      (0..9).each {|j|
        if(j == 0)
          (values << @@ucl + i* 0.2 ) if i.even?
          (values << @@lcl - i* 0.2 ) if i.odd?
        else
          (values << @@ucl - i* 0.2 - j*0.2) if i.even?
          (values << @@lcl + i* 0.2 + j*0.2) if i.odd?
        end
      }
      site_values << values
      values = []
    }
    # set criticality and prios
    $log.info "get one sample channel info for parameter Param_0001. wait..."
    sleep 30
    ch = $folder.spc_channel(:parameter=>"Param_0001")
    refute_nil ch, "returned nil channel for param Param_0001"
    #
    assert_equal nil, ch.set_customer_fields(ch.customer_fields), "failed to set customer field Criticality for channel parameter Param_0001"
    (1..@@parameter_count - 1).each {|j|
      my_prios << 50
    }
    #
    param_names = $ptest.get_param_names
    refute_equal 0, param_names.size, "no param names found"
    $log.info "param_names: #{param_names.inspect}"
    #
    # override control limits
    cs = $folder.spc_channels
    cs.each{|ch|
      next if !param_names.member?(ch.parameter)
      assert ch.set_limits({"MEAN_VALUE_UCL"=>@@ucl, "MEAN_VALUE_CENTER"=>@@center, "MEAN_VALUE_LCL"=>@@lcl, "SIGMA_CENTER"=>nil, "SIGMA_LCL"=>nil}), "failed to set limits for channel #{ch.name}"
      # set customer fields
      fields = ch.customer_fields.merge("Criticality"=>"Critical", "Maverick check"=>"1;1;15", "Maverick-Action"=>"email+hold", "AQL-Action"=>"email+hold")
      assert_equal nil, ch.set_customer_fields(fields), "failed to set customer field 'Maverick check' for channel #{ch.name}"
      assert ch.customer_fields.member?("Maverick check"), "customer field 'Maverick check' for channel #{ch.name} was not set!"
    }
    lot_cleanup(true)
    # create cj on the ATC102
    cj = $sv.claim_process_lot(@@lot, :eqp=>@@eqp, :mode=>"Off-Line-1",:proctime=>5)
    refute_nil cj, "could not create cj for lot #{@@lot} on equipment #{@@eqp}"
    #
    # restore current sample count for later verification
    $log.info "wait to get all space channels..."
    all_ch = param_names.collect {|p| ch = $folder.spc_channel(:parameter=>p) }
    refute_equal [], all_ch, "empty channels list returned"
    #
    # excluded parameters will not have new samples populated to space
    expect_params_samples = all_ch.collect{|ch|
      if excluded_params != nil
        if excluded_params.member?(ch.parameter)
          s= ch.samples(:days=>30).size
        else
          s= ch.samples(:days=>30).size + 1
        end
      else
        s= ch.samples(:days=>30).size + 1
      end
    }
    $log.info "prepare package..."
    #
    # send data package
    params = {:lot=>@@lot, :nctype=>:mnc, :cj=>cj, :slot=>@@slot_id, :site_values=>site_values, :llow=>@@lsl, :lhigh=>@@usl, :prio=>@@prio, :product=>@@product1, :prios=>prios, :next_pd=>@@next_test_pd, :what=>:parser2}.merge(params)
    assert $ptest.create_verify_data(1, false, params), "failed to create data package"
    #
    refute_nil param_names, "failed to get parameter names"
    #
    # wait until all expected samples are populated successfully
    assert $ptest.check_channel_samples(expect_params_samples, param_names, :waiting_time=>700), "check samples failed"
    #
    result = $ptest.extract_sample_chart_values("Result_AQL", -1)
    assert result[:mean] != 0, "no AQL violation found"
    #
    fhl = check_futurehold(@@lot)
    assert fhl.size > 0, "no future folds for lot #{@@lot}"
    # ensure there is at least one fh with reason = "X-E1"
    etspace_fh = false
    fh_op = ""
    fhl.each {|fh|
      if fh.reason.start_with?("X-E")
        etspace_fh = true
        fh_op = fh.rsp_op
        break
      end
    }
    assert etspace_fh, "no future hold with reason start with X-E set by etspace"
    refute_equal fh_op, @@next_test_opNo, "invalid op for future hold"
  end

  def test205_empty_NextTestPD_etparser_mnc
    $ptest.create_verify_data(1,false,:pg=>@@pg, :lot=>@@lots[0], :nctype=>:mnc, :product=>@@product1, :op=>@@op1,  :what=>:parser2)
    data = $ptest.dis.dis_file_names[0].slice!(".mnc")
    nc_folder_etp1 = File.join($ptest.dis.indir[:parser], "rejected")
    $log.info "wait for etparser to reject the package (with max. waiting time of #{@@wait_parser/60}) min ..."
    # fast_wait_for is not faster!, ssteidte 2013-07-29
    ##assert fast_wait_for(:timeout=>@@wait_parser){!!$ptest.check_files_in_folder(data, nc_folder_etp1, "", "", :nctype=>"mnc")}, "file #{data} not found in rejected folder within the timeout of #{@@wait_parser}"
    assert wait_for(:timeout=>@@wait_parser) {$ptest.check_files_in_folder(data, nc_folder_etp1, "", "", :nctype=>"mnc")}, "file #{data} not found in rejected folder within the timeout of #{@@wait_parser}"  end
  # **************** Test 2 NextTestPD	 END ****************************************************************************************************
  #
  # **************** Test 3 Retest packages	 START ****************************************************************************************************
  #
  # send two packages with the same context and the same wafer number but with different lotstart time (LOTSTTI) to both etparsers (1 to each parser)
  # check if the second package can be found in the /retest folder
  # Ensure that all retested wafers are flaged as retested on space
  def test300_retest_2_pckg_same_context_retest_all_wafers_same_data_different_LOTSTTI
    @@all_data_names = {}
    wafers = []
    (1..10).each{|i| wafers << "#{@@lots[0]}.0#{i.to_s}"}
    # send first package
    $ptest.create_verify_data(1,false,:pg=>@@pg, :lot=>@@lots[0], :nctype=>:mnc, :product=>@@product1, :op=>@@op1, :what=>:parser_qa1, :wafers=>wafers)
    data = $ptest.dis.dis_file_names[0].slice!(".mnc")
    @@all_data_names[@@lots[0]] = [$ptest.dis.dis_file_names[0], Time.now]
    folders = [ "#{$ptest.dis.indir[:parser_qa1]}/out"]
    wait_for_package_movements_all(:folders=>folders)
    #
    # send second package
    @@all_data_names = {}
    $ptest.create_verify_data(1,false,:pg=>@@pg, :lot=>@@lots[0], :nctype=>:mnc, :product=>@@product1, :op=>@@op1,  :what=>:parser_qa2, :wafers=>wafers)
    data = $ptest.dis.dis_file_names[0].slice!(".mnc")
    @@all_data_names[@@lots[0]] = [$ptest.dis.dis_file_names[0], Time.now]
    folders = ["#{$ptest.dis.indir[:parser_qa2]}/out"]
    wait_for_package_movements_all(:folders=>folders)

    # wait_for_package_movements_all

    #check retest folder (manually)
    # check space channels and samples and flags (manually)
  end

  # send two packages with the same context but with the half of wafer number for pckg 2 and with different lotEndtime time (LOTENTI)  to both etparsers
  # (1 to each parser)
  # check if the second package can be found in the /retest folder
  # Ensure that only 5 wafers of 10 are flaged as retested  on space
  def test301_retest_2_pckg_same_context_retest_5_wafers_same_data_different_LOTENTI
    @@all_data_names = {}
    wafers = []
    (1..10).each{|i| wafers << "#{@@lots[0]}.0#{i.to_s}"}
    # send first package
    $ptest.create_verify_data(1,false,:pg=>@@pg, :lot=>@@lots[0], :nctype=>:mnc, :product=>@@product1, :op=>@@op1, :what=>:parser_qa1, :wafers=>wafers)
    data = $ptest.dis.dis_file_names[0].slice!(".mnc")
    @@all_data_names[@@lots[0]] = [$ptest.dis.dis_file_names[0], Time.now]
    folders = [ "#{$ptest.dis.indir[:parser_qa1]}/out"]
    wait_for_package_movements_all(:folders=>folders)
    #
    # send second package
    wafers = []
    @@all_data_names = {}
    (1..5).each{|i| wafers << "#{@@lots[0]}.0#{i.to_s}"}
    $ptest.create_verify_data(1,false,:pg=>@@pg, :lot=>@@lots[0], :nctype=>:mnc, :product=>@@product1, :op=>@@op1,  :what=>:parser_qa2, :wafers=>wafers)
    data = $ptest.dis.dis_file_names[0].slice!(".mnc")
    @@all_data_names[@@lots[0]] = [$ptest.dis.dis_file_names[0], Time.now]
    folders = ["#{$ptest.dis.indir[:parser_qa1]}/out", "#{$ptest.dis.indir[:parser_qa2]}/out"]
    wait_for_package_movements_all(:folders=>folders)
  end

  def test302_retest_2_pckg_same_context_retest_5_wafers_same_data_different_LOTENTI
    @@all_data_names = {}
    wafers = []
    #
    # send second package
    (4..5).each{|i| wafers << "#{@@lots[0]}.0#{i.to_s}"}
    $ptest.create_verify_data(1,false,:pg=>@@pg, :lot=>@@lots[0], :nctype=>:mnc, :product=>@@product1, :op=>@@op1,  :what=>:parser_qa2, :wafers=>wafers)
    data = $ptest.dis.dis_file_names[0].slice!(".mnc")
    @@all_data_names[@@lots[0]] = [$ptest.dis.dis_file_names[0], Time.now]
    folders = ["#{$ptest.dis.indir[:parser_qa1]}/out", "#{$ptest.dis.indir[:parser_qa2]}/out"]
    wait_for_package_movements_all(:folders=>folders)
  end

  # **************** Test 3 Retest packages	 END ****************************************************************************************************

  # *********** Aux methods *****************************************
  # general method to create mutiple data parameter samples
  # input param with_spec_violation = true if required to provoke spec violation
  def create_data_packages(nctype, with_spec_violation, params={})
    # send data
    def_lot=(params[:lot] or @@lots[0])
    count = (params[:count] or 10)
    #
    values = []
    mywafers = (params[:wafers] or nil)
    $log.info "********************* create_data_packages:  nctype: #{nctype} *********************************"
    (nctype == "nc")? mynctype = :nc : mynctype = :mnc
    what = (params[:what] or :filter)
    $log.info "what = #{what}"
    $ptest.dis = ETSpace::DIS2.new($env, :pg=>@@pg, :lot=>def_lot, :wafers=>mywafers, :nctype=>mynctype)
    $dis = $ptest.dis
    # generate 10 parameter values randomly between clow & chigh
    (1..count).each{|i|
      a = [-rand(1e2), rand(1e2)]
      v = a[rand(a.size)]
      values << v
    }
    #
    # may it is required to provoke control limits violation on the second & third params
    values[1] = params[:value1] if params[:value1] != nil
    values[2] = params[:value2] if params[:value2] != nil
    #
    # provoke spec limit violation for the first parameter if desired.
    $log.info "with_spec_violation = #{with_spec_violation}"
    #
    # assign value with spec violation
    values[0] = 1e21 if with_spec_violation
    #
    trgt = params[:trgt]
    params = {:llow=>-1e2, :lhigh=>1e2,:clow=>-1e2, :chigh=>1e2, :trgt=>trgt}.merge(params)
    $dis.add_values(values, params)
    $dis.remote_write(what)
  end

  def check_results(check_channel_num, params={})
    # verify channel creation
    excluded_params = (params[:excluded_params] or nil)
    timeout = (params[:timeout] or @@wait_parser )
    #
    $log.info "waiting for the parser to parse all parameters...."
    start_time = Time.now
    if check_channel_num
      while $dis.limit[0].wet_limit_size * 2 > $folder.spc_channels.size && Time.now < start_time + timeout
        sleep 2
      end
    end
    #
    # read limits data out of limit file for comparison
    param_names = $ptest.get_param_names()
    $log.info "1. returned param_names = #{param_names.inspect}"
    return false if param_names == nil
    #
    # check parameters names in space
    param_names.each {|p|
      # get the name of the parameter
      $log.info "1.1 ............param_name: #{p}"
      if $folder.spc_channel(:parameter=>p) == nil
        $log.warn "limit parameter #{p} not populated to space"
        return false
      end
    }
    $log.info "2. ............"
    all_ch = param_names.collect {|p| ch = $folder.spc_channel(:parameter=>p) }
    if all_ch == nil
      $log.info "empty channels list returned"
      return false
    end
    $log.info "3. ............"
    #
    # excluded parameters will not have new samples populated to space
    expect_params_samples = all_ch.collect{|ch|
      if excluded_params != nil
        if excluded_params.member?(ch.parameter)
          s= ch.samples(:days=>30).size
        else
          s= ch.samples(:days=>30).size + 1
        end
      else
        s= ch.samples(:days=>30).size + 1
      end
    }
    $log.info "4. ............"
    #
    if !$ptest.check_channel_samples(s, param_names, :waiting_time=>timeout)
      $log.info "check samples failed"
      return false
    end
    $log.info "5. ............"
    return true
  end

  def wait_for_user(text)
    puts text + " .continue? y/n"
    results = gets
  end

  def create_check_package_with_NextTestPD(nexttest_pd, nctype, what)
    $ptest.delete_channels()
    local_write = false
    if nexttest_pd != nil
      $ptest.create_verify_data(1,false,:pg=>@@pg, :lot=>@@lots[0], :nctype=>nctype, :product=>@@product1, :op=>@@op1, :local_write=>local_write,
        :next_pd=>nexttest_pd, :what=>what)
    else
      $ptest.create_verify_data(1,false,:pg=>@@pg, :lot=>@@lots[0], :nctype=>nctype, :product=>@@product1, :op=>@@op1, :local_write=>local_write,
        :what=>what)
    end
    #
    if !local_write
      $log.info "wait 300 sec for channels creation on space"
      assert wait_for(:timeout=>300) {$folder.spc_channels.size > 0}, "channels are not created during time interval of 300 secs"
      #
      param_names = $ptest.get_param_names()
      assert param_names != nil || param_names.size > 0, "empty parameters names returned"
      assert $ptest.check_channel_samples(1, param_names, :waiting_time=>1000), "failed to check samples"
    end
  end

  def lot_cleanup(release_holds, params={})
    $log.info "lot_cleanup...@@lot = #{@@lot}"
    li = $sv.lot_info(@@lot)
    if release_holds
      assert $sv.lot_hold_release(@@lot, nil) == 0, "failed to release lot holds"
      assert $ptest.cancel_futrueholds(@@lot) == 0, "failed to cancel future holds" #unless $ptest.check_futurehold(@@lot) == []
    end
    if li.xfer_status == "EI"
      assert $sv.eqp_unload(li.eqp, nil)
      assert $sv.carrier_xfer_status_change(li.carrier, "EO", li.opEqps[0]) == 0, "failed to change xfer status of carrier #{li.carrier} to EO"
    elsif li.xfer_status == "MI"
      assert $sv.carrier_xfer_status_change(li.carrier, "EO", li.opEqps[0]) == 0, "failed to change xfer status of carrier #{li.carrier} to EO"
    end
    if li.xfer_status == "EI"
      assert $sv.eqp_unload(li.eqp, nil)
    end
    force = (params[:force] or false)
    assert $sv.lot_opelocate(@@lot, @@opNo, :force=>force) == 0, "failed to locate lot #{} to op #{@@opNo}"
  end

	# Aux help methods
    # example to call:
      # folders = [ "#{$ptest.dis.indir[:parser_qa1]}/out",
                  # "#{$ptest.dis.indir[:parser_qa2]}/out"]
      # wait_for_package_movements_all (:folders=>folders)
    def wait_for_package_movements_all(params={})
      @@finished = []
      cur_folder_info = {}
      @@pckg_time_info = {}
      def_folders = [ $ptest.dis.indir[:accepted],
                      $ptest.dis.indir[:published],
                      $ptest.dis.indir[:parser_qa1],
                      $ptest.dis.indir[:parser_qa2],
                      "#{$ptest.dis.indir[:parser_qa1]}/out",
                      "#{$ptest.dis.indir[:parser_qa2]}/out"]

      all_folders = (params[:folders] or def_folders)
      stop_f = (params[:stop_f] or "")
      start_time1 = Time.now
      start_time = Time.now
      $log.info "*************************************************************************************************************"
      $log.info "**************************** wait_for_package_movements_all ..."
      # prepare general hash list
      all_folders.each {|f| @@pckg_time_info[f] = {} }
      while @@finished.size < @@all_data_names.size && Time.now < start_time1 + @@wait_parser
        #
        @@all_data_names.keys.each {|lot|
          next if @@finished.member?(lot)
          data = @@all_data_names[lot][0]
          #
          all_folders.each {|f|
            data_folder = "#{f}/data"
            limit_folder = "#{f}/limit"
            next if @@pckg_time_info[f].size == @@all_data_names.size or @@pckg_time_info[f].keys.member?(lot)
            # ext_log = true if f.end_with?("published")
            if !!$ptest.check_files_in_folder(data, f, data_folder, limit_folder, :nctype=>"mnc")
              if (!stop_f.empty? && f == stop_f) or f.end_with?("out") or f == all_folders[-1]
                @@finished << lot
                $log.info ".... finished lots: #{@@finished.inspect}"
              end
              #
              file_name = "#{f}/#{data}.mnc"
              cmd = "stat -c '%Y' /#{file_name}"
              timestamp = $ptest.dis.host.exec!(cmd)
              @@pckg_time_info[f][lot] = [@@all_data_names[lot][1], Time.now, f, timestamp]
              # reset start time for the next package
              start_time = Time.now
              $log.info "found pckg of lot: #{lot} in folder: #{f}"
            end
          }
        }
      end
      $pckg_time_info = @@pckg_time_info
      $log.info "used DISRouter time per data:"
      $log.info "Lot|start time|end time|nc_folder|timestamp"
      puts pp $pckg_time_info
      return @@finished
    end
end
