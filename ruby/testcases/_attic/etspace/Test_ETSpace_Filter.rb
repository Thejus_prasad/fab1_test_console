=begin
Automated testing of the ETSpace filter.
Requirements:

(c) Copyright 2011 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Authors: Adel Darwaish, Steffen Steidten, 2012-07-17
=end

require 'RubyTestCase'
require 'etspacetest'

# Automated testing of the ETSpace filter.

class Test_ETSpace_Filter < RubyTestCase

  @@filter_timeout = 60
  @@standalone = true

  # source files combinations as input for DISFilter
  @@data_tc11 = 'tc_over3000_mixed_QA'
  @@data_tc12 = 'tc_under3000_mixed'
  @@data_tc13 = 'tc_over3000_critical'
  @@data_tc14 = 'tc_under3000_critical'
  @@data_tc15 = 'tc_over3000_notcritical'
  @@data_tc16 = 'tc_under3000_notcritical'
  # not all files
  @@data_tc17 = 'tc_over3000_mixed'   # same file like tc11
  @@data_tc18 = 'tc_over3000_critical'   # same file like tc13
  @@data_tc19 = 'tc_over3000_mixed' # same file like tc11

  # inconsistencies
  @@data_tc20 = 'tc_over3000_mixed_invalid_limit_param'
  @@data_tc21 = 'tc_invalid_WaferNC'
  @@data_tc22 = 'tc_over3000_mixed' # same file like tc11
  @@data_tc23 = 'tc_over3000_mixed_invalid_WaferDATA'
  #
  #
  @@product1 = 'SHELBY21AD.B1'
  @@product2 = 'SHELBY21AD.B2'
  @@product3 = 'SHELBY21AD.B3'
  @@technology = 'QA'
  @@pg = 'PGQA1'
  # @@pg = 'QAPG1'
  @@op1 = 'FINA-FWET.01'
  @@op2 = 'FINA-FWETSEP.01'
  @@pg2 = 'SHELBY2-1F'
  @@qa_pgqa1_fwet = '/QA/PGQA1/FINA-FWET.01'

  #
  @@lots = ['U282V.00','U9018.00', 'U1372.00', 'U136Z.00', 'U0028.00', 'U0030.00', 'U18GU.00', 'U89UB.00']
  @@lots_info =
  { 'U282V.00'=>[@@product1, @@op1],
    'U9018.00'=>[@@product2, @@op1],
    'U1372.00'=>[@@product1, @@op1],
    'U136Z.00'=>[@@product2, @@op1],
    'U0028.00'=>[@@product3, @@op1],
    'U0030.00'=>[@@product2, @@op1]
  }
  #
  # lots info for nc/mnc files: product, op, productgroup, sublottype, sitepattern, insertionname, accepted or ignored result
  @@lots_info_pre_filter =
  { @@lots[0]=>[@@product1, "FINA-FWETSEP.01","MPW12","PR","S17A","FWET", "ignored"],
    @@lots[1]=>[@@product2, "FINA-FWET.01","PGQA1","ES", "S17A","FWET_L","ignored"],
    @@lots[2]=>[@@product1, "FINA-FWET.01","SHELBY3-1F","PX","S13A","SWET_D","published"],
    @@lots[3]=>[@@product2, "M1-SWET.01","PGQA1","PR","VARI","SWET","ignored"],
    @@lots[4]=>[@@product3, "M4-SWET.01","PGQA1","PR","S17A","EFUSE","ignored" ],
    @@lots[5]=>[@@product2, "M5-SWET.01", "SHELBY2-2F","PR","S17A","SWET_L","ignored"],
    @@lots[6]=>[@@product2, "FINA-FWET.01", "SHELBY3-1F","PR", "S17A","SWET_L","published"],
    @@lots[7]=>[@@product1, "FINA-FWETSEP.01", "SHELBY2-2F","DEV","S17A","SWET_L","ignored"],
  }
  #
  @@folder = ''
  #
  @@refresh_time = 60
  @@wait_filter = 300
  @@wait_parser = 4800
  @@wait_router = 4800


  def self.startup
    super(nosiview: true)
  end


  def test00_setup
    $setup_ok = false
    ##($log.warn "Env MUST be 'itdc_standalone'"; return) if @@standalone && $env != 'itdc_standalone'
    $disfilter = ETSpace::TestFilter.new($env, nctype: :nc, standalone: @@standalone)
    if $env == "itdc"
      ($spc.close; $ptest = nil) if $spc and $spc.env != $env
      $ptest = ETSpace::TestParser.new($env, nctype: :nc, standalone: @@standalone) unless $ptest and $ptest.env == $env
      $dis = $ptest.dis
      $spc = $ptest.spc
      assert $lds = $spc.lds('ET_Fab1'), "LDS not found"
      @@folder = ["", @@technology, @@pg, @@op1].join('/')
      @@folder2 = ["", @@technology, @@pg2, @@op2].join('/')
      $log.info "using folder #{@@folder}"
      $log.info "using folder2 #{@@folder2}"
      $folder = $lds.folder(@@folder)
      ($log.info "folder #{f} does not exist") unless $folder
      $folder2 = $lds.folder(@@folder2)
    end
    # Test_ETSpace_Filter.cleanupall(:what=>:all)
    #
    $setup_ok = true
  end

  def test11_over3000_mix
    data = @@data_tc11
    assert $disfilter.submit_data_verify(data, cleanup: true, expect_filter: true), "failed test"
  end

  def test12_under3000_mix
    data = @@data_tc12
    assert $disfilter.submit_data_verify(data, cleanup: true, expect_filter: false), "failed test"
  end

  def test13_over3000_critical
    data = @@data_tc13
    assert $disfilter.submit_data_verify(data, cleanup: true, expect_filter: true), "failed test"
  end

  def test14_under3000_critical
    data = @@data_tc14
    assert $disfilter.submit_data_verify(data, cleanup: true, expect_filter: false), "failed test"
  end

  def test15_over3000_notcritical
    data = @@data_tc15
    result = $disfilter.submit_data(data, cleanup: true)
    $log.info "result of submit data = #{result}"
    assert result, "failed test"
    $log.info "  waiting #{@@filter_timeout}s for the filter to run"
    sleep @@filter_timeout
    # files must be moved to rejected folder
    folder = $disfilter.dis.indir[:rejected]
    assert $disfilter.check_files_in_folder(data,"#{folder}/nc", "#{folder}/data", "#{folder}/limit"),
      "#{data} files not found in folder: #{folder}"
    #
	  sleep 5
    folder = $disfilter.dis.indir[:processing]
    assert_equal nil, $disfilter.check_files_not_in_folder_old(data, folder),"#{data} files found in folder: #{folder}"
  end

  def test16_under3000_notcritical
    data = @@data_tc16
    result = $disfilter.submit_data(data, cleanup: true)
    $log.info "result of submit data = #{result}"
    assert result, "failed test"
    $log.info "  waiting #{@@filter_timeout}s for the filter to run"
    # sleep @@filter_timeout
    # files must be moved to published folder
    folder = $disfilter.dis.indir[:published]
	  wait_for(:timeout=>@@filter_timeout + 5) {
      $disfilter.check_files_in_folder(data, "#{folder}/nc", "#{folder}/data", "#{folder}/limit")
    }
    assert $disfilter.check_files_in_folder(data, "#{folder}", "#{folder}/data", "#{folder}/limit"),
        "#{data} files not found in folder #{folder}"
	  sleep 5
    folder = $disfilter.dis.indir[:processing]
    assert_equal nil, $disfilter.check_files_not_in_folder_old(data, folder),"#{data} files found in folder #{folder}"
  end

  def test17_limit_file_does_not_exist
    data = @@data_tc17
    $log.info "  using data file: #{data}"
    # do not write limit file to server for DISFilter input
    assert_equal true, $disfilter.submit_data(data, cleanup: true, nolimit: true), "failed test"
    $log.info "  waiting #{@@filter_timeout}s for the filter to run"
    sleep @@filter_timeout

    # nc file must be removed to rejected folder. data file must remain in iput folder
    folder = $disfilter.dis.indir[:rejected]
    filename = "#{data}.nc"
    full_filename = "#{folder}/nc/#{filename}"
    refute_equal nil, $disfilter.file_found_in_folder?(full_filename),"#{filename} file not found in folder: #{folder}"

    # data file remain in input (filter) folder
    folder = $disfilter.dis.indir[:filter]
    filename = "#{data}.data.dis"
    full_filename = "#{folder}/data/#{filename}"
    refute_equal nil, $disfilter.file_found_in_folder?(full_filename),"#{filename} file not found in folder: #{folder}"
    # a copy of data file will be sent to rejected folder
    folder = $disfilter.dis.indir[:rejected]
    filename = "#{data}.data.dis"
    full_filename = "#{folder}/data/#{filename}"
    refute_equal nil, $disfilter.file_found_in_folder?(full_filename),"#{filename} file not found in folder: #{folder}"

    # no file shall remain in processing folder
    folder = $disfilter.dis.indir[:processing]
    assert_equal nil, $disfilter.check_files_not_in_folder(data, folder),"#{data} files found in folder: #{folder}"
  end

  def test18_data_file_does_not_exist
    data = @@data_tc18
    $log.info "  using data file: #{data}"
    # do not write data file to server for DISFilter input
    assert_equal true, $disfilter.submit_data(data, cleanup: true, nodata: true), "failed test"
    $log.info "  waiting #{@@filter_timeout}s for the filter to run"
    sleep @@filter_timeout
    # nc file must be moved to rejected folder. data file must remain in iput folder
    folder = $disfilter.dis.indir[:rejected]
    filename = "#{data}.nc"
    full_filename = "#{folder}/nc/#{filename}"
    refute_equal nil, $disfilter.file_found_in_folder?(full_filename),"#{filename} file not found in folder: #{folder}"
    #
    # limit file must be moved to rejected folder
    folder = $disfilter.dis.indir[:rejected]
    filename = "#{data}.limit.dis"
    full_filename = "#{folder}/limit/#{filename}"
    refute_equal nil, $disfilter.file_found_in_folder?(full_filename),"#{filename} file not found in folder: #{folder}"
    #
    # no file shall remain in processing folder
    folder = $disfilter.dis.indir[:processing]
    assert_equal nil, $disfilter.check_files_not_in_folder(data, folder),"#{data} files found in folder: #{folder}"
  end

  def test19_override_existing_files_in_output_folder
    data = @@data_tc19
    prev_timestamps = {}
    new_timestamps = {}
    # just prepare files to exist in folder to be overriden later.
    result = $disfilter.submit_data(data, cleanup: true)
    $log.info "  waiting #{@@filter_timeout}s for the filter to run"
    sleep @@filter_timeout
    #
    #check if files already exist and get their timestamps
    folder = $disfilter.dis.indir[:published]
    prev_timestamps = $disfilter.get_files_timestamp(data, folder)
    refute_equal nil, prev_timestamps, "returned previous time stamps is nil!!"
    assert $disfilter.submit_data_verify(data, expect_filter: true), "failed test"
    #
    # now check the new overriden files again
    new_timestamps = $disfilter.get_files_timestamp(data, folder)
    refute_equal nil, new_timestamps, "returned new time stamp is nil"
    # compare timestamps. overriden files must have new time in seconds
    assert_equal prev_timestamps.size, new_timestamps.size, "previous and new timestamps length no match"
    prev_timestamps.each_pair {|key, v| refute_equal v, new_timestamps[key], "time stamp not changed for file #{key}"}
  end

  def test20_over3000_mixed_invalid_limit_param
    data = @@data_tc20
    $log.info "  using data file: #{data}"
	  sleep 5
    assert $disfilter.submit_data_verify(data, cleanup: true, expect_filter: true, subdir: 'rejected', extlog: true)
  end

  def test21_invalid_WaferNC_in_nc
    data = @@data_tc21
    $log.info "  using data file: #{data}"
    result = $disfilter.submit_data(data, nodata: true, nolimit: true, cleanup: true)
    $log.info "result of submit data = #{result}"
    assert result, "failed test"
    $log.info "  waiting #{@@filter_timeout}s for the filter to run"
    sleep @@filter_timeout
    #
    # nc file must be removed to rejected folder. data & limit files must remain in iput folder
    folder = $disfilter.dis.indir[:rejected]
    filename = "#{data}.nc"
    full_filename = "#{folder}/nc/#{filename}"
    refute_nil $disfilter.file_found_in_folder?(full_filename), "#{filename} file not found in folder: #{folder}"
    # data file remain in input (filter) folder
    folder = $disfilter.dis.indir[:filter]
    filename = "#{data}.data.dis"
    full_filename = "#{folder}/data/#{filename}"
    assert_equal nil, $disfilter.file_found_in_folder?(full_filename),"#{filename} file found in folder: #{folder}"
    # limit file remain in input (filter) folder
    folder = $disfilter.dis.indir[:filter]
    filename = "#{data}.limit.dis"
    full_filename = "#{folder}/limit/#{filename}"
    assert_equal nil, $disfilter.file_found_in_folder?(full_filename),"#{filename} file not found in folder: #{folder}"
    #
    # no file shall remain in processing folder
    folder = $disfilter.dis.indir[:processing]
    assert_equal nil, $disfilter.check_files_not_in_folder(data, folder),"#{data} files found in folder: #{folder}"
  end

  def test22_over3000_mix_mnc_file
    data = @@data_tc22
    $disfilter = nil
    $disfilter_mnc = ETSpace::TestFilter.new($env, nctype: :mnc, standalone: @@standalone)
    assert $disfilter_mnc.submit_data_verify(data, cleanup: true, expect_filter: true), "failed test"
  end

  def test23_mnc_file_invalid_WaferDATA
    data = @@data_tc23
    $log.info "  using data file: #{data}"
	  $disfilter_mnc = ETSpace::TestFilter.new($env, nctype: :mnc, standalone: @@standalone)  if $disfilter_mnc == nil
    result = $disfilter_mnc.submit_data(data, cleanup: true)
    $log.info "result of submit data = #{result}"
    assert result, "failed test"
    $log.info "  waiting #{@@filter_timeout}s for the filter to run"
    sleep @@filter_timeout
    #
    # nc file must be removed to rejected folder. data & limit files must remain in iput folder
    folder = $disfilter_mnc.dis.indir[:rejected]
    filename = "#{data}.mnc"
    full_filename = "#{folder}/nc/#{filename}"
    refute_equal nil, $disfilter_mnc.file_found_in_folder?(full_filename),"#{filename} file not found in folder: #{folder}"
    # data file remain in input (filter) folder
    folder = $disfilter_mnc.dis.indir[:filter]
    filename = "#{data}.data.dis"
    full_filename = "#{folder}/data/#{filename}"
    refute_equal nil, $disfilter_mnc.file_found_in_folder?(full_filename),"#{filename} file not found in folder: #{folder}"
    # limit file remain in input (filter) folder
    folder = $disfilter_mnc.dis.indir[:filter]
    filename = "#{data}.limit.dis"
    full_filename = "#{folder}/limit/#{filename}"
    refute_equal nil, $disfilter_mnc.file_found_in_folder?(full_filename),"#{filename} file not found in folder: #{folder}"
    # no file shall remain in processing folder
    folder = $disfilter_mnc.dis.indir[:processing]
    assert_equal nil, $disfilter_mnc.check_files_not_in_folder(data, folder),"#{data} files found in folder: #{folder}"
  end

  # DISFilter 2.3 ignore pattern
  def test300_filtering_multiple_packages_with_ignored_and_accepted_packages_nc
    filtering_multiple_packages(:nc, standalone: @@standalone)
  end

  def test301_filtering_multiple_packages_with_ignored_and_accepted_packages_mnc
    filtering_multiple_packages(:mnc, standalone: @@standalone)
  end

  # under construction. to be coded
  def XXtest302_extended_filtering_for_not_configured_products
    #to
  end

  # **************** Test 4 DISRouter & load-balancing ETParsers START  ******************************************************
  #
  def test400_disrouter_load_balancing_two_packages_different_products_contexts
    @@all_data_names = {}
    local_write = false
    # delete channels to clean up
    # $ptest.delete_channels()
    #
  	@@lots_info_pre_filter.each {|p|
  		puts p[1][6] if p[1][6] == "published"
  		if p[1][6] == "published"
  			l = p[0]
  			product = p[1][0]
  			op = p[1][1]
  			pg = p[1][2]
  			$ptest.create_verify_data(1,false,:pg=>pg, :lot=>l, nctype: :mnc, :product=>product, :op=>op, :local_write=>local_write, standalone: @@standalone)
  			$ptest.dis.dis_file_names[0].slice!(".mnc")
  			@@all_data_names[l] = $ptest.dis.dis_file_names[0]
  		end
  	}
    # (0..1).each {|i|
      # l = @@lots_info.keys[i]
      # product = @@lots_info[l][0]
      # op = @@lots_info[l][1]
      # $ptest.create_verify_data(1,false,:pg=>@@pg, :lot=>l, nctype: :mnc, :product=>product, :op=>op, :local_write=>local_write, standalone: @@standalone)
      # $ptest.dis.dis_file_names[0].slice!(".mnc")
      # @@all_data_names[@@lots[i]] = $ptest.dis.dis_file_names[0]
    # }
	  return
    #
    # **** get DISFilter results - outputs in .../disfilter/published/
    # wait_for_disfilter
    wait_for_package_movements_all
    assert @@finished.size == @@all_data_names.size, "not all files are filtered during timeout interval of #{@@wait_router.to_s}"
    # check load-balancing: verify that products are routed to the correct folders
    # check routing: the same products in the same etparser's folder
    assert_equal 1, $pckg_time_info["/T36_WET_YM/space_dev/etparser"].size, "packages count is incorrect in folder /T36_WET_YM/space_dev/qa1"
    assert_equal 1, $pckg_time_info["/T36_WET_YM/space_dev/etparser2"].size, "packages count is incorrect in folder /T36_WET_YM/space_dev/qa2"
    refute_equal $pckg_time_info["/T36_WET_YM/space_dev/etparser"].keys[0], $pckg_time_info["/T36_WET_YM/space_dev/etparser2"].keys[0], "same package in etp folders"
    return
    #
    # check if channels are created on Space
    log.info "wait for channels to be created on Space..."
    start_time = Time.now
    assert wait_for(:timeout=>300) {$folder.spc_channels.size > 0}, "channels are not created during time interval of 300 secs"
    #
    param_names = $ptest.get_param_names()
    assert param_names != nil || param_names.size > 0, "empty parameters names returned"
    #
    assert $ptest.check_channel_samples(@@finished.size, param_names, :waiting_time=>600), "failed to check samples"
  end

  # check also queuing of packages
  def test401_disrouter_load_balancing_4_packages_different_products_contexts
     # delete channels to clean up
    @@all_data_names = {}
    local_write = false
    # $ptest.delete_channels()
    #
    # create 4 packages for ETParser1 & ETParser2
    (0..3).each {|i|
      l = @@lots_info.keys[i]
      product = @@lots_info[l][0]
      op = @@lots_info[l][1]
      $ptest.create_verify_data(1,false,:pg=>@@pg, :lot=>l, nctype: :mnc, :product=>product, :op=>op, :local_write=>local_write)
      $ptest.dis.dis_file_names[0].slice!(".mnc")
      @@all_data_names[@@lots[i]] = $ptest.dis.dis_file_names[0]
    }
    #
    $log.info "created packges for 4 lots: #{@@all_data_names.inspect}"
    # **** get results
    wait_for_package_movements_all
    assert @@finished.size == @@all_data_names.size, "not all files are filtered during timeout interval of #{@@wait_router.to_s}"
    assert @@pckg_time_info["/T36_WET_YM/space_dev/qa1"].size > 0, "packages count is incorrect in folder /T36_WET_YM/space_dev/qa1"
    assert @@pckg_time_info["/T36_WET_YM/space_dev/qa2"].size > 0, "packages count is incorrect in folder /T36_WET_YM/space_dev/qa2"
    refute_equal $pckg_time_info["/T36_WET_YM/space_dev/qa1"].keys[0], $pckg_time_info["/T36_WET_YM/space_dev/qa2"].keys[0], "same package in etp folders"
    refute_equal $pckg_time_info["/T36_WET_YM/space_dev/qa1"].keys[-1], $pckg_time_info["/T36_WET_YM/space_dev/qa2"].keys[-1], "same package in etp folders"
    return
    #
    #
    # check if channels are created on Space
    $log.info "wait until all channels are created on Space"
    assert wait_for(:timeout=>300) {$folder.spc_channels.size > 0}, "channels are not created during time interval of 300 secs"
    assert $folder.spc_channels.size > 0, "no channels are created in  "
    #
    param_names = $ptest.get_param_names()
    assert param_names != nil || param_names.size > 0, "empty parameters names returned"
    #
    assert $ptest.check_channel_samples(@@finished.size, param_names, :waiting_time=>600), "failed to check samples"
  end

 # send pckgs in this order: A1(huge), B1, A2, B2. Expect DISFilter/Router output to Etparsers in this order: B1, A1, B2, A2
  def test402_disrouter_load_balancing_4_packages_2_products_contexts_unorderd_delivery
    @@all_data_names = {}
    local_write = false
    huge_count = 100
    # $ptest.delete_channels()
    #
    my_prios = []
    (0..huge_count -1).each {|i| my_prios[i] = 50 }
    #pkg A1
    $ptest.create_verify_data(1,false,:pg=>@@pg, :lot=>@@lots[0], nctype: :mnc, :product=>@@product1, :op=>@@op1, :local_write=>local_write,
      :count=>huge_count, :prios=>my_prios)
    $ptest.dis.dis_file_names[0].slice!(".mnc")
    @@all_data_names[@@lots[0]] = $ptest.dis.dis_file_names[0]
    #
    # pckg B1
    $ptest.create_verify_data(1,false,:pg=>@@pg, :lot=>@@lots[1], nctype: :mnc, :product=>@@product2, :op=>@@op2, :local_write=>local_write)
    $ptest.dis.dis_file_names[0].slice!(".mnc")
    @@all_data_names[@@lots[1]] = $ptest.dis.dis_file_names[0]
    $log.info "wait for 30s to create the next pckges..."
    sleep 30
    #
    # pckg A2
    $ptest.create_verify_data(1,false,:pg=>@@pg, :lot=>@@lots[2], nctype: :mnc, :product=>@@product1, :op=>@@op1, :local_write=>local_write)
    $ptest.dis.dis_file_names[0].slice!(".mnc")
    @@all_data_names[@@lots[2]] = $ptest.dis.dis_file_names[0]
    #
    # pckg B2
    $ptest.create_verify_data(1,false,:pg=>@@pg, :lot=>@@lots[3], nctype: :mnc, :product=>@@product2, :op=>@@op2, :local_write=>local_write)
    $ptest.dis.dis_file_names[0].slice!(".mnc")
    @@all_data_names[@@lots[3]] = $ptest.dis.dis_file_names[0]
    $log.info "wait for 10s to to start verifying results..."
    #
    wait_for_disfilter
    assert_equal @@finished.size, @@all_data_names.size, "not all files are filtered during timeout interval of #{@@wait_router.to_s}"
    #
    # **** get DISRouter results
    nc_folder_etp1 = $ptest.dis.indir[:parser_qa1]
    nc_folder_etp2 = $ptest.dis.indir[:parser_qa2]
    data_folder = "" # not required
    limit_folder = "" # not required
    wait_for_package_movements(data_folder, limit_folder, :folder_etp1=>nc_folder_etp1, :folder_etp2=>nc_folder_etp2)
    assert_equal @@finished.size, @@all_data_names.size, "not all files are processed during timeout interval of #{@@wait_router.to_s}"
    #
    # check routing: the same products in the same etparser's folder
    folders = []
    @@pckg_time_info.each {|pckg| folders << pckg[1][2] if !folders.member?(pckg[1][2]) }
    $log.info "folders are: #{folders.inspect}"
    assert_equal 2, folders.size, "DISFilter failed to load-balance (route) the packages to different etparser instances"

    # Check timestamps: Expect DISFilter/Router output to Etparsers folders in this order: B1, A1, B2, A2
    #
    # B1 (@@lot2[1]) older than A1 (@@lots[0])
    assert @@pckg_time_info[@@lots[1]][3] < @@pckg_time_info[@@lots[0]][3], "invalid queuing. epxtected B1 (of lot: #{@@lots[1]}) older than A1 (of lot #{@@lots[0]})"
    # A1 (@@lot2[0]) older than B2 (@@lot2[3])
    assert @@pckg_time_info[@@lots[0]][3] < @@pckg_time_info[@@lots[3]][3], "invalid queuing. epxtected A1 (of lot: #{@@lots[0]}) older than B2 (of lot #{@@lots[3]})"
    # B2 (@@lot2[3]) older than A2 (@@lot2[2])
    assert @@pckg_time_info[@@lots[3]][3] < @@pckg_time_info[@@lots[2]][3], "invalid queuing. epxtected B2 (of lot: #{@@lots[3]}) older than A2 (of lot #{@@lots[2]})"
  end

  # send pckgs in this order: A1, B1, A2, B2, C2. Expect DISFilter/Router output to Etparsers in this order: A1, B1, C2, A2, B2
  # A2 can either be routed to any free etp after A1 & C2 are completed by their etp. C2 will be sent immediately to etp3
  def test403_disrouter_load_balancing_5_packages_3_products_contexts_2_etp
    @@all_data_names = {}
    local_write = false
    # $ptest.delete_channels()
    #
    #pkg A1
    $ptest.create_verify_data(1,false, :pg=>@@pg, :lot=>@@lots[0], nctype: :mnc, :product=>@@product1, :op=>@@op1, :local_write=>local_write)
    $ptest.dis.dis_file_names[0].slice!(".mnc")
    @@all_data_names[@@lots[0]] = $ptest.dis.dis_file_names[0]
    #
    # pckg B1
    $ptest.create_verify_data(1,false, :pg=>@@pg, :lot=>@@lots[1], nctype: :mnc, :product=>@@product2, :op=>@@op1, :local_write=>local_write)
    $ptest.dis.dis_file_names[0].slice!(".mnc")
    @@all_data_names[@@lots[1]] = $ptest.dis.dis_file_names[0]
    $log.info "wait for 30s to create the next pckges..."
    sleep 30
    #
    # pckg A2
    $ptest.create_verify_data(1,false, :pg=>@@pg, :lot=>@@lots[2], nctype: :mnc, :product=>@@product1, :op=>@@op1, :local_write=>local_write)
    $ptest.dis.dis_file_names[0].slice!(".mnc")
    @@all_data_names[@@lots[2]] = $ptest.dis.dis_file_names[0]
    #
    # pckg B2
    $ptest.create_verify_data(1,false, :pg=>@@pg, :lot=>@@lots[3], nctype: :mnc, :product=>@@product2, :op=>@@op1, :local_write=>local_write)
    $ptest.dis.dis_file_names[0].slice!(".mnc")
    @@all_data_names[@@lots[3]] = $ptest.dis.dis_file_names[0]
    $log.info "wait for 10s to to start verifying results..."
    sleep 10
    #
    # pckg C2
    $ptest.create_verify_data(1,false,:pg=>@@pg, :lot=>@@lots[4], nctype: :mnc, :product=>@@product3, :op=>@@op1, :local_write=>local_write)
    $ptest.dis.dis_file_names[0].slice!(".mnc")
    @@all_data_names[@@lots[4]] = $ptest.dis.dis_file_names[0]
    $log.info "wait for 10s to to start verifying results..."
    sleep 10
    #
    # wait_for_disfilter
    # assert_equal @@finished.size, @@all_data_names.size, "not all files are filtered during timeout interval of #{@@wait_router.to_s}"
    #
    # **** get DISRouter results
    nc_folder_etp1 = $ptest.dis.indir[:parser_qa1]
    nc_folder_etp2 = $ptest.dis.indir[:parser_qa2]
    data_folder = ""  # not required
    limit_folder = ""     # not required
    wait_for_package_movements(data_folder, limit_folder, :folder_etp1=>nc_folder_etp1, :folder_etp2=>nc_folder_etp2)
    assert_equal @@finished.size, @@all_data_names.size, "not all files are processed during timeout interval of #{@@wait_router.to_s}"
    #
    # get the etp folder of pckg A1
    a1_etp_folder = @@pckg_time_info[@@lots[0]][2]
    #
    # ensure that pckg A2 is sent to either etp of A1 or the etp2
    a2_etp_folder = @@pckg_time_info[@@lots[2]][2]
    assert [nc_folder_etp1, nc_folder_etp2].member?(a2_etp_folder), "wrong etp used for A2 package: current: #{a2_etp_folder}, expected one of #{nc_folder_etp1} or #{nc_folder_etp2}"

    # Check timestamps: Expect DISFilter/Router output to Etparsers folders in this order: B1, A1, B2, A2
    sorted_times1 = []
    sorted_times2 = []
    pckg5_ts = 0
    @@pckg_time_info.each {|lot|
      sorted_times1 << lot[1][3].to_i if @@lots_info[lot[0]][0] == @@product1
      sorted_times2 << lot[1][3].to_i if @@lots_info[lot[0]][0] == @@product2
      pckg5_ts = lot[1][3].to_i if @@lots_info[lot[0]][0] == @@product3
    }
    # A1 older than A2
    assert sorted_times1[0] < sorted_times1[1], "Queuing of DISrouter is not correct in folder #{nc_folder_etp1}"
    # B1 older than B2
    assert sorted_times2[0] < sorted_times2[1], "Queuing of DISrouter is not correct in folder #{nc_folder_etp2}"
    # C2 older than A2
    assert pckg5_ts < sorted_times1[1], "Queuing of DISrouter is not correct in folder for package C2"
    #
    # **** ETParser results
    out_folder_etp1 = "#{$ptest.dis.indir[:parser_qa1]}/out"
    out_folder_etp2 = "#{$ptest.dis.indir[:parser_qa2]}/out"
    data_folder = ""
    limit_folder = ""
    wait_for_package_movements(data_folder, limit_folder, :folder_etp1=>out_folder_etp1, :folder_etp2=>out_folder_etp2)
    assert_equal @@finished.size, @@all_data_names.size, "not all files are processed during timeout interval of #{@@wait_router.to_s}"
    #
    # check the queuing of etparsers
    sorted_times1 = []
    sorted_times2 = []
    pckg5_ts = 0
    @@pckg_time_info.each {|lot|
      sorted_times1 << lot[1][3].to_i if @@lots_info[lot[0]][0] == @@product1
      sorted_times2 << lot[1][3].to_i if @@lots_info[lot[0]][0] == @@product2
      pckg5_ts = lot[1][3].to_i if @@lots_info[lot[0]][0] == @@product3
    }
    # A1 older than A2
    assert sorted_times1[0] < sorted_times1[1], "Processing order of etparser1 is not correct in folder #{nc_folder_etp1}"
    # B1 older than B2
    assert sorted_times2[0] < sorted_times2[1], "Processing order of etparser2 is not correct in folder #{nc_folder_etp2}"
    # C2 older than A2
    assert pckg5_ts < sorted_times1[1], "Processing order is not correct in folder for package C2"
  end

  # send pckgs in this order: A1, B1, A2, B2. Expect DISFilter/Router output to Etparsers in this order: A1, B1, A2, B2,
  # A2 can either be routed to etp1, etp2 or etp3 after A1 is completed by its etp
  # for future tests: needs to start a 3rd instance of Etparser. Will be checked with Space Admin
  def Xtest404_disrouter_load_balancing_4_packages_2_products_contexts_3_ETParsers
    @@all_data_names = {}
    local_write = false
    # $ptest.delete_channels()
    #
    #pkg A1
    $ptest.create_verify_data(1,false,:pg=>@@pg, :lot=>@@lots[0], nctype: :mnc, :product=>@@product1, :op=>@@op1, :local_write=>local_write)
    $ptest.dis.dis_file_names[0].slice!(".mnc")
    @@all_data_names[@@lots[0]] = $ptest.dis.dis_file_names[0]
    #
    # pckg B1
    $ptest.create_verify_data(1,false,:pg=>@@pg, :lot=>@@lots[1], nctype: :mnc, :product=>@@product2, :op=>@@op2, :local_write=>local_write)
    $ptest.dis.dis_file_names[0].slice!(".mnc")
    @@all_data_names[@@lots[1]] = $ptest.dis.dis_file_names[0]
    $log.info "wait for 30s to create the next pckges..."
    sleep 30
    #
    # pckg A2
    $ptest.create_verify_data(1,false,:pg=>@@pg, :lot=>@@lots[2], nctype: :mnc, :product=>@@product1, :op=>@@op1, :local_write=>local_write)
    $ptest.dis.dis_file_names[0].slice!(".mnc")
    @@all_data_names[@@lots[2]] = $ptest.dis.dis_file_names[0]
    #
    # pckg B2
    $ptest.create_verify_data(1,false,:pg=>@@pg, :lot=>@@lots[3], nctype: :mnc, :product=>@@product2, :op=>@@op2, :local_write=>local_write)
    $ptest.dis.dis_file_names[0].slice!(".mnc")
    @@all_data_names[@@lots[3]] = $ptest.dis.dis_file_names[0]
    $log.info "wait for 10s to to start verifying results..."
    sleep 10
    #
    wait_for_disfilter
    assert_equal @@finished.size, @@all_data_names.size, "not all files are filtered during timeout interval of #{@@wait_router.to_s}"
    #
    # **** get DISRouter results
    nc_folder_etp1 = $ptest.dis.indir[:parser_qa1]
    nc_folder_etp2 = $ptest.dis.indir[:parser_qa2]
    data_folder = "#{$ptest.dis.indir[:parser]}/data"
    limit_folder = "#{$ptest.dis.indir[:parser]}/limit"
    wait_for_package_movements(data_folder, limit_folder, :folder_etp1=>nc_folder_etp1, :folder_etp2=>nc_folder_etp2)
    assert_equal @@finished.size, @@all_data_names.size, "not all files are processed during timeout interval of #{@@wait_router.to_s}"
    #
    # get the etp folder of pckg A1
    a1_etp_folder = @@pckg_time_info[@@lots[0]][2]
    #
    # ensure that pckg A2 is sent to either etp of A1 or the etp3
    a2_etp_folder = @@pckg_time_info[@@lots[2]][2]
    assert [nc_folder_etp1, nc_folder_etp3].member?(a2_etp_folder), "wrong etp used for A2 package: current: #{a2_etp_folder}, expected one of #{nc_folder_etp1} or #{nc_folder_etp3}"

    # Check timestamps: Expect DISFilter/Router output to Etparsers folders in this order: B1, A1, B2, A2
    sorted_times1 = []
    sorted_times2 = []
    @@pckg_time_info.each {|lot|
      sorted_times1 << lot[3].to_i if @@lots_info[lot][0] == @@product1
      sorted_times2 << lot[3].to_i if @@lots_info[lot][0] == @@product2
    }
    # A1 older than A2
    assert sorted_times1[0] < sorted_times1[1], "Queuing of DISrouter is not correct in folder #{nc_folder_etp1}"
    # B1 older than B2
    assert sorted_times2[0] < sorted_times2[1], "Queuing of DISrouter is not correct in folder #{nc_folder_etp2}"
    #
    # **** ETParser results
    out_folder_etp1 = "#{$ptest.dis.indir[:parser_qa1]}/out"
    out_folder_etp2 = "#{$ptest.dis.indir[:parser2_qa2]}/out"
    out_folder_etp3 = "#{$ptest.dis.indir[:parser3_qa3]}/out"
    data_folder = "#{$ptest.dis.indir[:parser]}/data"
    limit_folder = "#{$ptest.dis.indir[:parser]}/limit"
    wait_for_package_movements(data_folder, limit_folder, :folder_etp1=>out_folder_etp1, :folder_etp2=>out_folder_etp2, :folder_etp3=>out_folder_etp3)
    assert_equal @@finished.size, @@all_data_names.size, "not all files are processed during timeout interval of #{@@wait_router.to_s}"
    #
    # check the queuing of etparsers
    sorted_times1 = []
    sorted_times2 = []
    @@pckg_time_info.each {|lot|
      sorted_times1 << lot[3].to_i if @@lots_info[lot][0] == @@product1
      sorted_times2 << lot[3].to_i if @@lots_info[lot][0] == @@product2
    }
    # A1 older than A2
    assert sorted_times1[0] < sorted_times1[1], "Queuing of DISrouter is not correct in folder #{nc_folder_etp1}"
    # B1 older than B2
    assert sorted_times2[0] < sorted_times2[1], "Queuing of DISrouter is not correct in folder #{nc_folder_etp2}"
  end

  # under construction: this test case shows that one huge packge takes longer than several small packges to be processed.
  def test405_load_balancing_multiple_MNC_packages_one_huge_and_several_small
    # init
    local_write = false
    huge_count = 1500
    small_count = 5
    @@all_data_names = {}
    # delete channels to clean up
    # $ptest.delete_channels()
    #
    $log.info "create one huge package with 3500 parameters................................"
    my_prios = []
    (0..huge_count -1).each {|i| my_prios[i] = 50 }
    # create channels with default data
    $ptest.create_verify_data(1,false,:pg=>@@pg, :lot=>@@lot, nctype: :mnc,:llow=>-1e2, :product=>@@product1, :op=>@@op1,
      :lhigh=>1e2,:clow=>-1e2, :chigh=>1e2, :trgt=>120, :prios=>my_prios, :count=>huge_count, :local_write=>local_write)
    #
    # save data names for later verification
    $ptest.dis.dis_file_names[0].slice!(".mnc")
    @@all_data_names[@@lot] = $ptest.dis.dis_file_names[0]
    #
    my_prios = []
    (0..small_count - 1).each {|i| my_prios[i] = 50 }
    $log.info "create 5 small packages with 10 parameters each..............................."
    # create 5 packages for ETParser1 & ETParser2
    (0..4).each {|i|
      l = @@lots_info.keys[i]
      product = @@lots_info[l][0]
      op = @@lots_info[l][1]
      $ptest.create_verify_data(1,false,:pg=>@@pg, :lot=>l, nctype: :mnc, :product=>product, :op=>op, :prios=>my_prios, :local_write=>local_write)
      $ptest.dis.dis_file_names[0].slice!(".mnc")
      @@all_data_names[@@lots[i]] = $ptest.dis.dis_file_names[0]
    }
    $log.info "created data: \n #{@@all_data_names.inspect}"
    #
    # **** ETParser results - outputs in .../etparser/out/ and .../etparser2/out/ for nc/mnc files. data & limits files remain in /etparser folder
    @@pckg_time_info = {}
    data_folder = "#{$ptest.dis.indir[:parser]}/data"
    limit_folder = "#{$ptest.dis.indir[:parser]}/limit"
    out_folder_etp1 = "#{$ptest.dis.indir[:parser]}/out"
    out_folder_etp2 = "#{$ptest.dis.indir[:parser2]}/out"
    wait_for_package_movements(data_folder, limit_folder, :folder_etp1=>out_folder_etp1, :folder_etp2=>out_folder_etp2)
    assert @@finished.size == @@all_data_names.size, "not all files are processed during timeout interval of #{@@wait_router.to_s}"
    #
    $log.info "Time in secs has been taken for each lot sample:"
    @@pckg_time_info.each {|t| puts "      #{t.inspect}, "}
    #
    # check timestamps. Ensure the huge package has the biggest timestamp, which means: it has taken the longest time than the others.
    sorted_times = []
    @@pckg_time_info.each {|lot| sorted_times << lot[1][3].to_i}
    max_ts = sorted_times.max
    assert_equal max_ts, @@pckg_time_info[@@lot][3], "the huge package did not take the longest time to be processed as expected"
  end

  def test406_1_huge_pckg_same_context_4_pckgs_another_context
  # init
    #lots_info_pre_filter
     # init
    local_write = false
    huge_count = 1500
    small_count = 5
    @@all_data_names = {}
    # delete channels to clean up
    # $ptest.delete_channels()
    #
    $log.info "create one huge package with 3500 parameters................................"
    my_prios = []
    (0..huge_count -1).each {|i| my_prios[i] = 50 }
    # create channels with default data

    $ptest.create_verify_data(1,false, :lot=>@@lots[0], nctype: :mnc,:llow=>-1e2, :product=>@@lots_info_pre_filter[@@lots[2]][0], :op=>@@lots_info_pre_filter[@@lots[2]][1], :pg=>@@lots_info_pre_filter[@@lots[2]][2], :slt=>@@lots_info_pre_filter[@@lots[2]][3], :sp=>@@lots_info_pre_filter[@@lots[2]][4], :ins_name=>@@lots_info_pre_filter[@@lots[2]][5], :lhigh=>1e2,:clow=>-1e2, :chigh=>1e2, :trgt=>120, :prios=>my_prios, :count=>huge_count, :local_write=>local_write)      #
    # save data names for later verification
    $ptest.dis.dis_file_names[0].slice!(".mnc")
    @@all_data_names[@@lots[0]] = $ptest.dis.dis_file_names[0]
    #
    my_prios = []
    (0..small_count - 1).each {|i| my_prios[i] = 50 }
    $log.info "create 5 small packages with 10 parameters each..............................."
    # create 5 packages for ETParser1 & ETParser2
    (1..4).each {|i|
      l = @@lots_info.keys[i]
      product = @@lots_info[l][0]
      op = @@lots_info[l][1]
      $ptest.create_verify_data(1,false, :lot=>l, nctype: :mnc, :product=>@@lots_info_pre_filter[@@lots[6]][0], :op=>@@lots_info_pre_filter[@@lots[6]][1], :pg=>@@lots_info_pre_filter[@@lots[6]][2], :slt=>@@lots_info_pre_filter[@@lots[6]][3], :sp=>@@lots_info_pre_filter[@@lots[6]][4], :ins_name=>@@lots_info_pre_filter[@@lots[6]][5], :prios=>my_prios, :local_write=>local_write)
      $ptest.dis.dis_file_names[0].slice!(".mnc")
      @@all_data_names[@@lots[i]] = $ptest.dis.dis_file_names[0]
    }
    $log.info "*********** created following data: \n #{@@all_data_names.inspect}"
    #
    # **** ETParser results - outputs in .../etparser/out/ and .../etparser2/out/ for nc/mnc files. data & limits files remain in /etparser folder
    @@pckg_time_info = {}
    all_folders = [$ptest.dis.indir[:accepted], $ptest.dis.indir[:published], $ptest.dis.indir[:parser],
      $ptest.dis.indir[:parser2], "#{$ptest.dis.indir[:parser]}/out", "#{$ptest.dis.indir[:parser2]}/out"]

    wait_for_package_movements_all(:folders=>all_folders)
    assert @@finished.size == @@all_data_names.size, "not all files are filtered during timeout interval of #{@@wait_router.to_s}"
    assert @@pckg_time_info["/T36_WET_YM/space_dev/etparser"].size > 0, "packages count is incorrect in folder /T36_WET_YM/space_dev/etparser"
    assert @@pckg_time_info["/T36_WET_YM/space_dev/etparser2"].size > 0, "packages count is incorrect in folder /T36_WET_YM/space_dev/etparser2"
    refute_equal $pckg_time_info["/T36_WET_YM/space_dev/etparser"].keys[0], $pckg_time_info["/T36_WET_YM/space_dev/etparser2"].keys[0], "same package in etp folders"
    refute_equal $pckg_time_info["/T36_WET_YM/space_dev/etparser"].keys[-1], $pckg_time_info["/T36_WET_YM/space_dev/etparser2"].keys[-1], "same package in etp folders"
  end

  # **************** sorting-queuing ************************************************************* #
  # the DISRouter processing order should be: @@lots[0] -> @@lots[1] -> @@lots[2] -> @@lots[3] according to LOTSTTI (starttime)
  def test500_disrouter_package_sorting_queuing_LotStartTime_same_product_context
     # delete channels to clean up
    @@all_data_names = {}
    # local write because they will be uploaded to the host separately
    local_write = true
    # $ptest.delete_channels()
    #
    # create 1st package for ETParser1 locally
    (0..5).each {|i|
      $ptest.create_verify_data(1,false,:pg=>@@pg, :lot=>@@lots[i], nctype: :mnc, :product=>@@product1, :op=>@@op1, :local_write=>local_write, :tstart=>Time.now + i*120)
      $ptest.dis.dis_file_names[0].slice!(".mnc")
      @@all_data_names[@@lots[i]] = $ptest.dis.dis_file_names[0]
      $log.info "wait 120s to create the next package..."
    }
    #
    # upload files to the server in an unordered sequence
    $log.info "Uploading files in unordered sequence. Wait 5 secs ..."
    upload_files(:filter, "data", "limit", "", @@all_data_names[@@lots[4]])
    upload_files(:filter, "data", "limit", "", @@all_data_names[@@lots[2]])
    upload_files(:filter, "data", "limit", "", @@all_data_names[@@lots[3]])
    upload_files(:filter, "data", "limit", "", @@all_data_names[@@lots[5]])
    upload_files(:filter, "data", "limit", "", @@all_data_names[@@lots[0]])
    upload_files(:filter, "data", "limit", "", @@all_data_names[@@lots[1]])
    #
    # *** get DISFilter results
    # wait_for_disfilter(:sub_folder=>:accepted)
    # assert_equal @@finished.size, @@all_data_names.size, "not all files are filtered during timeout interval of #{@@wait_router.to_s}"

    # # @@pckg_time_info.each {|t| puts "      #{t.inspect}, "}
    # # verify that packages are queued by DISRouter in the order according to the lot start time "LOTSTTI". Check the timestamps
    # sorted_times = []
    # @@pckg_time_info.each {|lot| sorted_times << lot[1][2].to_i}
    # (0..3).each {|i|
      # assert_equal sorted_times.sort[i], @@pckg_time_info[@@lots[i]][2].to_i,
            # "Filter accepted: expected timestamps are wrong. Queuing is not correct for data of lot #{@@lots[i]}"
    # }
    #
    # **** get DISRouter results to etparser
    nc_folder_etp1 = $ptest.dis.indir[:parser]
    nc_folder_etp2 = $ptest.dis.indir[:parser2]
    data_folder = "#{$ptest.dis.indir[:parser]}/data"
    limit_folder = "#{$ptest.dis.indir[:parser]}/limit"
    wait_for_package_movements(data_folder, limit_folder, :folder_etp1=>nc_folder_etp1, :folder_etp2=>nc_folder_etp2)
    assert_equal @@finished.size, @@all_data_names.size, "not all files are processed during timeout interval of #{@@wait_router.to_s}"
    $log.info "Time in secs has been taken for each lot sample to be moved to etparser:"
    @@pckg_time_info.each {|t| puts "      #{t.inspect}, "}
    #
    # verify that products are routed to the correct folders. Since all pckges have the same context, all will be routed to the same etparser's folder
    # used_folder = @@pckg_time_info[@@lots[0]][2]   # the folder of the first pckage will be used for other following packages
    # @@pckg_time_info.each {|l| assert l[2] == used_folder, "wrong folder for product of lot #{l[0]}"}
    #
    # verify that packages are queued by DISRouter in the order according to the lot start time "LOTSTTI". Check the timestamps
    sorted_times = []
    @@pckg_time_info.each {|lot| sorted_times << lot[1][3].to_i}
    (0..5).each {|i|
      assert_equal sorted_times.sort[i], @@pckg_time_info[@@lots[i]][3].to_i,
            "expected timestamps are wrong. Queuing is not correct for data of lot #{@@lots[i]}"
    }
    return
    #
    # **** ETParser results
    data_folder = "#{$ptest.dis.indir[:parser]}/data"
    limit_folder = "#{$ptest.dis.indir[:parser]}/limit"
    out_folder_etp1 = "#{$ptest.dis.indir[:parser]}/out"
    out_folder_etp2 = "#{$ptest.dis.indir[:parser2]}/out"
    wait_for_package_movements(data_folder, limit_folder, :folder_etp1=>out_folder_etp1, :folder_etp2=>out_folder_etp2)
    assert_equal @@finished.size, @@all_data_names.size, "not all files are processed during timeout interval of #{@@wait_router.to_s}"
    $log.info "Time in secs has been taken for each lot sample to be processed by etparser:"
    #
    # check if channels are created on Space
    $log.info "wait until all channels are created on Space"
    assert wait_for(:timeout=>300) {$folder.spc_channels.size > 0}, "channels are not created during time interval of 300 secs"
    #
    param_names = $ptest.get_param_names()
    assert param_names != nil || param_names.size > 0, "empty parameters names returned"
    #
    assert $ptest.check_channel_samples(4, param_names, :waiting_time=>600), "failed to check samples"
  end

  # The DISRouter processing order should be: @@lots[1] -> @@lots[0] -> @@lots[3] -> @@lots[2] according to LOTENTI ( endtime)
  # The oldest lot will be processed at first (FIFO)
  def test501_disrouter_package_sorting_queuing_LotEndTime_same_product_context
    @@all_data_names = {}
    local_write = false
    #
    # $ptest.delete_channels()
    $ptest.create_verify_data(1,false,:pg=>@@pg, :lot=>@@lots[0], nctype: :mnc, :product=>@@product1, :op=>@@op1, :local_write=>local_write,
      :tstart=>Time.now, :timestamp=>Time.now + 20*60)
    $ptest.dis.dis_file_names[0].slice!(".mnc")
    @@all_data_names[@@lots[0]] = $ptest.dis.dis_file_names[0]
    #
    $ptest.create_verify_data(1,false,:pg=>@@pg, :lot=>@@lots[1], nctype: :mnc, :product=>@@product1, :op=>@@op1, :local_write=>local_write,
      :tstart=>Time.now + 5*60, :timestamp=>Time.now + 15*60)
    $ptest.dis.dis_file_names[0].slice!(".mnc")
    @@all_data_names[@@lots[1]] = $ptest.dis.dis_file_names[0]
    #
    $ptest.create_verify_data(1,false,:pg=>@@pg, :lot=>@@lots[2], nctype: :mnc, :product=>@@product1, :op=>@@op1, :local_write=>local_write,
      :tstart=>Time.now + 10*60, :timestamp=>Time.now + 40*60)
    $ptest.dis.dis_file_names[0].slice!(".mnc")
    @@all_data_names[@@lots[2]] = $ptest.dis.dis_file_names[0]
    #
    $ptest.create_verify_data(1,false,:pg=>@@pg, :lot=>@@lots[3], nctype: :mnc, :product=>@@product1, :op=>@@op1, :local_write=>local_write,
      :tstart=>Time.now + 25*60, :timestamp=>Time.now + 35*60)
    $ptest.dis.dis_file_names[0].slice!(".mnc")
    @@all_data_names[@@lots[3]] = $ptest.dis.dis_file_names[0]
    #
    wait_for_disfilter(:sub_folder=>:accepted)
    assert @@finished.size == @@all_data_names.size, "not all files are filtered during timeout interval of #{@@wait_router.to_s}"
    #
    # verify that packages are queued by DISRouter in the order according to the lot endtime "LOTENTI". Check the timestamps
    sorted_times = []
    @@pckg_time_info.each {|lot| sorted_times << lot[1][2].to_i}
    # the DISRouter processing order should be: @@lots[1] -> @@lots[0] -> @@lots[3] -> @@lots[2] according to LOTENTI ( endtime)
    assert_equal sorted_times.sort[0], @@pckg_time_info[@@lots[1]][2].to_i, "Filter accepted: expected timestamps are wrong. Queuing is not correct for data of lot #{@@lots[1]}"
    assert_equal sorted_times.sort[1], @@pckg_time_info[@@lots[0]][2].to_i, "Filter accepted: expected timestamps are wrong. Queuing is not correct for data of lot #{@@lots[0]}"
    assert_equal sorted_times.sort[2], @@pckg_time_info[@@lots[3]][2].to_i, "Filter accepted: expected timestamps are wrong. Queuing is not correct for data of lot #{@@lots[3]}"
    assert_equal sorted_times.sort[3], @@pckg_time_info[@@lots[2]][2].to_i, "Filter accepted: expected timestamps are wrong. Queuing is not correct for data of lot #{@@lots[2]}"
    #
    # **** get DISRouter results
    data_folder = "#{$ptest.dis.indir[:parser]}/data"
    limit_folder = "#{$ptest.dis.indir[:parser]}/limit"
    nc_folder_etp1 = $ptest.dis.indir[:parser]
    nc_folder_etp2 = $ptest.dis.indir[:parser2]
    wait_for_package_movements(data_folder, limit_folder, :folder_etp1=>nc_folder_etp1, :folder_etp2=>nc_folder_etp2)
    assert @@finished.size == @@all_data_names.size, "not all files are processed during timeout interval of #{@@wait_router.to_s}"
    #
    # verify that products are routed to the correct folders. Since all pckges have the same context, all will be routed to the same etparser's folder
    used_folder = @@pckg_time_info[@@lots[0]][2]   # the folder of the first pckage will be used for other following packages
    @@pckg_time_info.each {|l| assert l[2] == used_folder, "wrong folder for product of lot #{l[0]}"}
    #
    # verify that packages are queued by DISRouter in the order according to the lot endtime "LOTENTI". Check the timestamps
    sorted_times = []
    @@pckg_time_info.each {|lot| sorted_times << lot[1][3].to_i}
    # the DISRouter processing order shall be: @@lots[1] -> @@lots[0] -> @@lots[3] -> @@lots[2] according to LOTENTI ( endtime)
    assert_equal sorted_times.sort[0], @@pckg_time_info[@@lots[1]][3].to_i, "Parser: expected timestamps are wrong. Queuing is not correct for data of lot #{@@lots[1]}"
    assert_equal sorted_times.sort[1], @@pckg_time_info[@@lots[0]][3].to_i, "Parser: expected timestamps are wrong. Queuing is not correct for data of lot #{@@lots[0]}"
    assert_equal sorted_times.sort[2], @@pckg_time_info[@@lots[3]][3].to_i, "Parser: expected timestamps are wrong. Queuing is not correct for data of lot #{@@lots[3]}"
    assert_equal sorted_times.sort[3], @@pckg_time_info[@@lots[2]][3].to_i, "Parser: expected timestamps are wrong. Queuing is not correct for data of lot #{@@lots[2]}"
    #
    # **** ETParser results
    data_folder = "#{$ptest.dis.indir[:parser]}/data"
    limit_folder = "#{$ptest.dis.indir[:parser]}/limit"
    out_folder_etp1 = "#{$ptest.dis.indir[:parser]}/out"
    out_folder_etp2 = "#{$ptest.dis.indir[:parser2]}/out"
    wait_for_package_movements(data_folder, limit_folder, :folder_etp1=>out_folder_etp1, :folder_etp2=>out_folder_etp2)
    assert @@finished.size == @@all_data_names.size, "not all files are processed during timeout interval of #{@@wait_router.to_s}"
    return
    #
    # check if channels are created on Space
    $log.info "wait until all channels are created"
    wait_for(timeout: 300) {$folder.spc_channels.size > 0}
    #
    param_names = $ptest.get_param_names()
    assert param_names != nil || param_names.size > 0, "empty parameters names returned"
    #
    assert $ptest.check_channel_samples(4, param_names, :waiting_time=>600), "failed to check samples"
  end

  # the DISRouter processing order should be: @@lots[0] -> @@lots[1] -> @@lots[2] -> @@lots[3] according to LOTSTTI (starttime)
  def test502_disrouter_package_sorting_queuing_LotStartTime_2_products_context
    @@all_data_names = {}
    local_write = true
    # delete channels to clean up
    # $ptest.delete_channels()
    #
    # create 4 packages for ETParser1 & ETParser2
    (0..3).each {|i|
      l = @@lots_info.keys[i]
      product = @@lots_info[l][0]
      op = @@lots_info[l][1]
      $ptest.create_verify_data(1,false,:pg=>@@pg, :lot=>l, nctype: :mnc, :product=>product, :op=>op, :local_write=>local_write, :tstart=>Time.now + i*120)
      $ptest.dis.dis_file_names[0].slice!(".mnc")
      @@all_data_names[@@lots[i]] = $ptest.dis.dis_file_names[0]
    }
    #
    # upload files to the server in an unordered sequence
    $log.info "Uploading files in unordered sequence. Wait 40 secs ..."
    upload_files(:filter, "data", "limit", "", @@all_data_names[@@lots[2]])
    upload_files(:filter, "data", "limit", "", @@all_data_names[@@lots[3]])
    upload_files(:filter, "data", "limit", "", @@all_data_names[@@lots[0]])
    upload_files(:filter, "data", "limit", "", @@all_data_names[@@lots[1]])
    #
    # *** get DISFilter results
    finished = wait_for_disfilter(:sub_folder=>:accepted)
    assert_equal finished.size, @@all_data_names.size, "not all files are filtered during timeout interval of #{@@wait_router.to_s}"
    # @@pckg_time_info.each {|t| puts "      #{t.inspect}, "}
    # verify that packages are queued by DISRouter in the order according to the lot start time "LOTSTTI". Check the timestamps
    sorted_times = []
    @@pckg_time_info.each {|lot| sorted_times << lot[1][2].to_i}
    (0..3).each {|i|
      assert_equal sorted_times.sort[i], @@pckg_time_info[@@lots[i]][2].to_i,
            "Filter accepted: expected timestamps are wrong. Queuing is not correct for data of lot #{@@lots[i]}"
    }
    #
    # **** get DISRouter results
    nc_folder_etp1 = $ptest.dis.indir[:parser]
    nc_folder_etp2 = $ptest.dis.indir[:parser2]
    data_folder = "#{$ptest.dis.indir[:parser]}/data"
    limit_folder = "#{$ptest.dis.indir[:parser]}/limit"
    wait_for_package_movements(data_folder, limit_folder, :folder_etp1=>nc_folder_etp1, :folder_etp2=>nc_folder_etp2)
    assert @@finished.size == @@all_data_names.size, "not all files are processed during timeout interval of #{@@wait_router.to_s}"
    #
    # check load-balancing: verify that products are routed to the correct folders
    etp1_count = 0
    etp2_count = 0
    @@pckg_time_info.each {|l|
      etp1_count +=1 if l[1][2] == nc_folder_etp1
      etp2_count +=1 if l[1][2] == nc_folder_etp2
    }
    assert etp1_count == 2, "expected number of packages does not match for folder etparser1"
    assert etp2_count == 2, "expected number of packages does not match for folder etparser2"
    #
    # verify that packages are queued by DISRouter in the order according to the lot start time "LOTSTTI". Check the timestamps
    sorted_times1 = []
    sorted_times2 = []
    @@pckg_time_info.each {|lot|
      sorted_times1 << lot[3].to_i if @@lots_info[lot][0] == @@product1
      sorted_times2 << lot[3].to_i if @@lots_info[lot][0] == @@product2
    }
    assert sorted_times1[0] < sorted_times1[1], "Queuing of DISrouter is not correct in folder #{nc_folder_etp1}"
    assert sorted_times2[0] < sorted_times2[1], "Queuing of DISrouter is not correct in folder #{nc_folder_etp2}"
    #
    # **** ETParser results
    out_folder_etp1 = "#{$ptest.dis.indir[:parser]}/out"
    out_folder_etp2 = "#{$ptest.dis.indir[:parser2]}/out"
    data_folder = "#{$ptest.dis.indir[:parser]}/data"
    limit_folder = "#{$ptest.dis.indir[:parser]}/limit"
    wait_for_package_movements(data_folder, limit_folder, :folder_etp1=>out_folder_etp1, :folder_etp2=>out_folder_etp2)
    assert @@finished.size == @@all_data_names.size, "not all files are processed during timeout interval of #{@@wait_router.to_s}"
    #
    # check if channels are created on Space
    $log.info "wait until all channels are created on Space"
    assert wait_for(:timeout=>300) {$folder.spc_channels.size > 0}, "channels are not created during time interval of 300 secs"
    #
    param_names = $ptest.get_param_names()
    assert param_names != nil || param_names.size > 0, "empty parameters names returned"
    #
    assert $ptest.check_channel_samples(4, param_names, :waiting_time=>600), "failed to check samples"
  end

  def test503_check_published_disrouter_sorting_queuing_LotStartTime_2_products_context
    @@all_data_names = {}
    local_write = true
    # delete channels to clean up
    # $ptest.delete_channels()
    #
    # create 4 packages for ETParser1 & ETParser2
    (0..3).each {|i|
      l = @@lots_info.keys[i]
      product = @@lots_info[l][0]
      op = @@lots_info[l][1]
      $ptest.create_verify_data(1,false,:pg=>@@pg, :lot=>l, nctype: :mnc, :product=>product, :op=>op, :local_write=>local_write, :tstart=>Time.now + i*120)
      $ptest.dis.dis_file_names[0].slice!(".mnc")
      @@all_data_names[@@lots[i]] = $ptest.dis.dis_file_names[0]
    }
    #
    # upload files to the server in an unordered sequence
    $log.info "Uploading files in unordered sequence. Wait 40 secs ..."
    upload_files(:filter, "data", "limit", "", @@all_data_names[@@lots[2]])
    upload_files(:filter, "data", "limit", "", @@all_data_names[@@lots[3]])
    upload_files(:filter, "data", "limit", "", @@all_data_names[@@lots[0]])
    upload_files(:filter, "data", "limit", "", @@all_data_names[@@lots[1]])
    #
    # *** get DISFilter results
    finished = wait_for_disfilter
    assert_equal finished.size, @@all_data_names.size, "not all files are filtered during timeout interval of #{@@wait_router.to_s}"
    # verify that packages are queued by DISRouter in the order according to the lot start time "LOTSTTI". Check the timestamps
    sorted_times = []
    @@pckg_time_info.each {|lot| sorted_times << lot[1][2].to_i}
    (0..3).each {|i|
      assert_equal sorted_times.sort[i], @@pckg_time_info[@@lots[i]][2].to_i,
            "Filter published: expected timestamps are wrong. Queuing is not correct for data of lot #{@@lots[i]}"
    }
  end

  # Expected all in DISFilter/accepted folder in the order: @@lots[2]->@@lots[3]->@@lots[1]->@@lots[0]
  # The DISRouter processing order should be: EP1: @@lots[2] -> @@lots[0], EP2: @@lots[3] -> @@lots[1] according to LOTENTI ( endtime)
  # The oldest lot will be processed at first (FIFO)
  def test504_disrouter_package_sorting_queuing_LotEndTime_2_products_context
    @@all_data_names = {}
    local_write = false
    # $ptest.delete_channels()
    #
    $ptest.create_verify_data(1,false,:pg=>@@pg, :lot=>@@lots[0], nctype: :mnc, :product=>@@product1, :op=>@@op1, :local_write=>local_write,
      :tstart=>Time.now, :timestamp=>Time.now + 50*60)
    $ptest.dis.dis_file_names[0].slice!(".mnc")
    @@all_data_names[@@lots[0]] = $ptest.dis.dis_file_names[0]
    #
    $ptest.create_verify_data(1,false,:pg=>@@pg, :lot=>@@lots[1], nctype: :mnc, :product=>@@product2, :op=>@@op1, :local_write=>local_write,
      :tstart=>Time.now + 5*60, :timestamp=>Time.now + 35*60)
    $ptest.dis.dis_file_names[0].slice!(".mnc")
    @@all_data_names[@@lots[1]] = $ptest.dis.dis_file_names[0]
    #
    $ptest.create_verify_data(1,false,:pg=>@@pg, :lot=>@@lots[2], nctype: :mnc, :product=>@@product1, :op=>@@op1, :local_write=>local_write,
      :tstart=>Time.now + 10*60, :timestamp=>Time.now + 10*60)
    $ptest.dis.dis_file_names[0].slice!(".mnc")
    @@all_data_names[@@lots[2]] = $ptest.dis.dis_file_names[0]
    #
    $ptest.create_verify_data(1,false,:pg=>@@pg, :lot=>@@lots[3], nctype: :mnc, :product=>@@product2, :op=>@@op1, :local_write=>local_write,
      :tstart=>Time.now + 15*60, :timestamp=>Time.now + 20*60)
    $ptest.dis.dis_file_names[0].slice!(".mnc")
    @@all_data_names[@@lots[3]] = $ptest.dis.dis_file_names[0]
    #
    $log.info "expected all in DISFilter/accepted folder in the order: @@lots[2]->@@lots[3]->@@lots[1]->@@lots[0]"
    wait_for_disfilter(:sub_folder=>:accepted)
    assert @@finished.size == @@all_data_names.size, "not all files are filtered during timeout interval of #{@@wait_router.to_s}"
    #
    # verify that packages are queued by DISRouter in the order according to the lot endtime "LOTENTI". Check the timestamps
    sorted_times = []
    @@pckg_time_info.each {|lot| sorted_times << lot[1][2].to_i}
    # the DISRouter processing order should be: @@lots[2]->@@lots[3]->@@lots[1]->@@lots[0] according to LOTENTI ( endtime)
    assert_equal sorted_times.sort[0], @@pckg_time_info[@@lots[2]][2].to_i, "Filter accepted: expected timestamps are wrong. Queuing is not correct for data of lot #{@@lots[1]}"
    assert_equal sorted_times.sort[1], @@pckg_time_info[@@lots[3]][2].to_i, "Filter accepted: expected timestamps are wrong. Queuing is not correct for data of lot #{@@lots[0]}"
    assert_equal sorted_times.sort[2], @@pckg_time_info[@@lots[1]][2].to_i, "Filter accepted: expected timestamps are wrong. Queuing is not correct for data of lot #{@@lots[3]}"
    assert_equal sorted_times.sort[3], @@pckg_time_info[@@lots[0]][2].to_i, "Filter accepted: expected timestamps are wrong. Queuing is not correct for data of lot #{@@lots[2]}"
    #
    # **** get DISRouter results
    nc_folder_etp1 = $ptest.dis.indir[:parser]
    nc_folder_etp2 = $ptest.dis.indir[:parser2]
    data_folder = "#{$ptest.dis.indir[:parser]}/data"
    limit_folder = "#{$ptest.dis.indir[:parser]}/limit"
    wait_for_package_movements(data_folder, limit_folder, :folder_etp1=>nc_folder_etp1, :folder_etp2=>nc_folder_etp2)
    assert @@finished.size == @@all_data_names.size, "not all files are processed during timeout interval of #{@@wait_router.to_s}"
    #
    # check load-balancing: verify that products are routed to different folders
    # check routing: the same products in the same etparser's folder
    assert_equal @@pckg_time_info[@@lots[0]][2], @@pckg_time_info[@@lots[2]][2], "different etparsers for pakgs @@lots[0]: (#{@@lots[0]}) & @@lots[2]: (#{@@lots[2]})"
    assert_equal @@pckg_time_info[@@lots[1]][2], @@pckg_time_info[@@lots[3]][2], "different etparsers for pakgs @@lots[0]: (#{@@lots[0]}) & @@lots[2]: (#{@@lots[2]})"
    #
    # verify that packages are queued by DISRouter in the order according to the lot endtime "LOTENTI". Check the timestamps
    sorted_times1 = []
    sorted_times2 = []
    @@pckg_time_info.each {|lot|
      sorted_times1 << lot[3].to_i if @@lots_info[lot[0]][0] == @@product1
      sorted_times2 << lot[3].to_i if @@lots_info[lot[0]][0] == @@product2
    }
    assert sorted_times1[1] < sorted_times1[0], "Queuing of DISrouter is not correct in folder #{nc_folder_etp1}"
    assert sorted_times2[1] < sorted_times2[0], "Queuing of DISrouter is not correct in folder #{nc_folder_etp2}"
    #
    # **** ETParser results
    out_folder_etp1 = "#{$ptest.dis.indir[:parser]}/out"
    out_folder_etp2 = "#{$ptest.dis.indir[:parser2]}/out"
    data_folder = "#{$ptest.dis.indir[:parser]}/data"
    limit_folder = "#{$ptest.dis.indir[:parser]}/limit"
    wait_for_package_movements(data_folder, limit_folder, :folder_etp1=>out_folder_etp1, :folder_etp2=>out_folder_etp2)
    assert_equal @@finished.size, @@all_data_names.size, "not all files are processed during timeout interval of #{@@wait_router.to_s}"
    #
    # check if channels are created on Space
    $log.info "wait until all channels are created on Space"
    assert wait_for(:timeout=>300) {$folder.spc_channels.size > 0}, "channels are not created during time interval of 300 secs"
    #
    param_names = $ptest.get_param_names()
    assert param_names != nil || param_names.size > 0, "empty parameters names returned"
    #
    assert $ptest.check_channel_samples(4, param_names, :waiting_time=>600), "failed to check samples"
  end

  def test505_disrouter_package_sorting_queuing_LotEndTime_2_products_context
    @@all_data_names = {}
    local_write = false
    # $ptest.delete_channels()
    #
    $ptest.create_verify_data(1,false,:pg=>@@pg, :lot=>@@lots[0], nctype: :mnc, :product=>@@product1, :op=>@@op1, :local_write=>local_write,
      :tstart=>Time.now, :timestamp=>Time.now + 50*60)
    $ptest.dis.dis_file_names[0].slice!(".mnc")
    @@all_data_names[@@lots[0]] = $ptest.dis.dis_file_names[0]
    #
    $ptest.create_verify_data(1,false,:pg=>@@pg, :lot=>@@lots[1], nctype: :mnc, :product=>@@product2, :op=>@@op1, :local_write=>local_write,
      :tstart=>Time.now + 5*60, :timestamp=>Time.now + 35*60)
    $ptest.dis.dis_file_names[0].slice!(".mnc")
    @@all_data_names[@@lots[1]] = $ptest.dis.dis_file_names[0]
    #
    $ptest.create_verify_data(1,false,:pg=>@@pg, :lot=>@@lots[2], nctype: :mnc, :product=>@@product1, :op=>@@op1, :local_write=>local_write,
      :tstart=>Time.now + 10*60, :timestamp=>Time.now + 10*60)
    $ptest.dis.dis_file_names[0].slice!(".mnc")
    @@all_data_names[@@lots[2]] = $ptest.dis.dis_file_names[0]
    #
    $ptest.create_verify_data(1,false,:pg=>@@pg, :lot=>@@lots[3], nctype: :mnc, :product=>@@product2, :op=>@@op1, :local_write=>local_write,
      :tstart=>Time.now + 15*60, :timestamp=>Time.now + 20*60)
    $ptest.dis.dis_file_names[0].slice!(".mnc")
    @@all_data_names[@@lots[3]] = $ptest.dis.dis_file_names[0]
    #
    $log.info "expected all in DISFilter/accepted folder in the order: @@lots[2]->@@lots[3]->@@lots[1]->@@lots[0]"
    wait_for_disfilter_all(:accepted=>true, :published=>true)
    assert_equal @@pckg_time_info.size, 2, "not all folders are checked..."
  end


  # aux methods

  def filtering_multiple_packages(nctype, params={})
    $all_data  = []
    @@all_data_names = {}
    local_write = false
    curr_nctype = (nctype == :nc)? "nc" : "mnc"
    # delete channels to clean up
    # $ptest.delete_channels()
    # Prepare 8 data packages with MNC/NC files and with ignored and accepted context pattern
    @@lots_info_pre_filter.each {|pckg|
      # product, op, productgroup, sublottype, sitepattern, insertionname
      $log.info "create packgae for lot #{pckg[0]} with config: #{pckg[1][0]},  #{pckg[1][1]},  #{pckg[1][2]} , #{pckg[1][3]}, #{pckg[1][4]}, #{pckg[1][5]}"
      $ptest.create_verify_data(1,false, {:lot=>pckg[0], :product=>pckg[1][0], :op=>pckg[1][1], :pg=>pckg[1][2], :slt=>pckg[1][3], :sp=>pckg[1][4], :ins_name=>pckg[1][5], nctype: nctype, :local_write=>local_write}.merge(params))
      $ptest.dis.dis_file_names[0].slice!(".#{curr_nctype}")
      @@all_data_names[pckg[0]] = $ptest.dis.dis_file_names[0]
    }
    $log.info "@@all_data_names = #{@@all_data_names.inspect}"
    # save pckage names in $all_data for manually cleaning up all files from server if required.
    @@all_data_names.each {|data| $all_data << data[1]}
    # return
    sleep 5
    #
    # wait for packages to be removed from DISFilter's input
    folder = $ptest.dis.indir[:filter]
    wait_for_files_not_in_folder(folder)
    assert_equal @@finished.size, @@all_data_names.size, "not all files are filtered during timeout interval of #{@@wait_filter.to_s} secs"
    sleep 5
	#
    # ensure the packages to be ignored are moved to the ../ignored folder and other are moved to ../accepted folder
    @@lots_info_pre_filter.each {|pckg|
      data = @@all_data_names[pckg[0]]
      folder = $ptest.dis.indir[:filter]
      nc_folder = File.join(File.dirname(folder), pckg[1][6])
      $log.info "nc_folder = #{nc_folder}:\n wait 10s for next package of lot #{pckg[0]}"
	  sleep 10

      data_folder = File.join(nc_folder, "data")
      limit_folder = File.join(nc_folder, "limit")
      assert $ptest.check_files_in_folder(data, nc_folder, data_folder, limit_folder, nctype: curr_nctype), "missing files of package for lot #{pckg[0]} in folder #{nc_folder}#"
    }
  end

  def self.cleanup(data)
    $ptest.dis.dis.remote_cleanup(data, :filter)
  end

  def self.cleanupall(params={})
    all = (params[:data] or [@@data_tc11,@@data_tc12,@@data_tc13,@@data_tc14,@@data_tc15,@@data_tc16,@@data_tc17,@@data_tc18,@@data_tc20,@@data_tc21])
    what = (params[:what] or :filter)
    all.each {|data|
      $ptest.dis.remote_cleanup_def(data, what)
    }
  end

  # **** DISFilter results
  def wait_for_files_not_in_folder(folder, params={})
    @@finished = []
    @@times = {}
    start_time1 = Time.now
    start_time = Time.now
    $log.info "wait_for DISFilter to remove files from folder #{folder} ..."
    $log.info "@@all_data_names.size = #{@@all_data_names.size}"
    while @@finished.size < @@all_data_names.size && Time.now < start_time1 + @@wait_filter
      @@all_data_names.keys.each {|lot|
        next if @@finished.member?(lot)
        data = @@all_data_names[lot]
        if wait_for(:timeout=>0.001) {!$ptest.check_files_not_in_folder(data, folder, params)}
          @@finished << lot
          $log.info "....finished lots: #{@@finished.inspect}"
        end
      }
    end
    return @@finished
  end

  # **** DISFilter results
  # check if files in DISRouter folder (../published) as filtered pckgs
  def wait_for_disfilter(params={})
    @@finished = []
    @@pckg_time_info = {}
    start_time1 = Time.now
    start_time = Time.now
    subfolder = (params[:sub_folder] or :published)
    $log.info "wait for DISFilter results..."
    $log.info "@@all_data_names.size = #{@@all_data_names.size}"
    $log.info "Time in secs has been taken for each lot sample to be moved to accepted or published folder:"
    folder = $ptest.dis.indir[subfolder]
    nc_folder = folder
    data_folder = "#{folder}/data"
    limit_folder = "#{folder}/limit"
    while @@finished.size < @@all_data_names.size && Time.now < start_time1 + @@wait_router
      @@all_data_names.keys.each {|lot|
        next if @@finished.member?(lot)
        data = @@all_data_names[lot]
        if !!$ptest.check_files_in_folder(data, nc_folder, data_folder, limit_folder, nctype: "mnc")
          @@finished << lot
          file_name = "#{nc_folder}/#{data}.mnc"
          cmd = "stat -c '%Y' /#{file_name}"
          timestamp = $ptest.dis.host.exec!(cmd)
          @@pckg_time_info[lot] = [start_time, Time.now, timestamp]
          # reset start time for the next package
          start_time = Time.now
          $log.info "....finished lots: #{@@finished.inspect}"
        end
      }
    end
    $log.info "used DISFilter time per data:"
    $log.info "Lot|start time|end time"
    @@pckg_time_info.each{|t|
      j = 0
      @@lots.size.times{|i|
        if t[0] == @@lots[i]
          j = i
          break
        end
      }
      $log.info "#{t.inspect}, @@lots[#{j}]|"
    }
    #
    return @@finished
  end

  # check files in etparser folder (which one? depends on the configered product)
  def wait_for_package_movements(data_folder, limit_folder, params={})
    @@finished = []
    @@pckg_time_info = {}
    nc_folder_etp1 = (params[:folder_etp1] or "")
    nc_folder_etp2 = (params[:folder_etp2] or "")
    nc_folder_etp3 = (params[:folder_etp3] or "")
    start_time1 = Time.now
    start_time = Time.now
    $log.info "*************************************************************************************************************"
    $log.info "**************************** wait_for_disrouter or etparsers results of nc_folder_etp1: #{nc_folder_etp1} ..."
    # data & limit files for all etparsers are in the same folder of etparser1
    while @@finished.size < @@all_data_names.size && Time.now < start_time1 + @@wait_parser
      @@all_data_names.keys.each {|lot|
        next if @@finished.member?(lot)
        data = @@all_data_names[lot]
        # check packages for etparser1
        if !nc_folder_etp1.empty?
          if !!$ptest.check_files_in_folder(data, nc_folder_etp1, data_folder, limit_folder, nctype: "mnc")
            @@finished << lot
            # get DISRouter timestamp
            file_name = "#{nc_folder_etp1}/#{data}.mnc"
            cmd = "stat -c '%Y' /#{file_name}"
            timestamp = $ptest.dis.host.exec!(cmd)
            @@pckg_time_info[lot] = [start_time, Time.now, nc_folder_etp1, timestamp]
            # reset start time for the next package
            start_time = Time.now
            $log.info "etp1.... finished lots: #{@@finished.inspect}"
          elsif !nc_folder_etp1.end_with?("out") && !!$ptest.check_files_in_folder(data, "#{nc_folder_etp1}/out", data_folder, limit_folder, nctype: "mnc") &&
            !@@finished.member?(lot)

            @@finished << lot
            # get DISRouter timestamp
            file_name = "#{nc_folder_etp1}/out/#{data}.mnc"
            cmd = "stat -c '%Y' /#{file_name}"
            timestamp = $ptest.dis.host.exec!(cmd)
            @@pckg_time_info[lot] = [start_time, Time.now, "#{nc_folder_etp1}/out", timestamp]
            # reset start time for the next package
            start_time = Time.now
            $log.info "etp1.... finished lots: #{@@finished.inspect}"
          end
        end
        # check packages for etparser2
        next if @@finished.member?(lot)
        if !nc_folder_etp2.empty?
          if !!$ptest.check_files_in_folder(data, nc_folder_etp2, data_folder, limit_folder, nctype: "mnc")
            @@finished << lot
            # get timestamp
            file_name = "#{nc_folder_etp2}/#{data}.mnc"
            cmd = "stat -c '%Y' /#{file_name}"
            timestamp = $ptest.dis.host.exec!(cmd)
            @@pckg_time_info[lot] = [start_time, Time.now, nc_folder_etp2, timestamp]
            # reset start time for the next package
            start_time = Time.now
            $log.info "etp2.... finished lots: #{@@finished.inspect}"
          elsif !nc_folder_etp2.end_with?("out") && !!$ptest.check_files_in_folder(data, "#{nc_folder_etp2}/out", data_folder, limit_folder, nctype: "mnc") &&
            ! @@finished.member?(lot)

            @@finished << lot
            # get timestamp
            file_name = "#{nc_folder_etp2}/out/#{data}.mnc"
            cmd = "stat -c '%Y' /#{file_name}"
            timestamp = $ptest.dis.host.exec!(cmd)
            @@pckg_time_info[lot] = [start_time, Time.now, "#{nc_folder_etp2}/out", timestamp]
            # reset start time for the next package
            start_time = Time.now
            $log.info "etp2.... finished lots: #{@@finished.inspect}"
          end
        end
        # check packages for etparser3
        next if @@finished.member?(lot)
        if !nc_folder_etp3.empty?
          if !!$ptest.check_files_in_folder(data, nc_folder_etp3, data_folder, limit_folder, nctype: "mnc")
            @@finished << lot
            # get timestamp
            file_name = "#{nc_folder_etp3}/#{data}.mnc"
            cmd = "stat -c '%Y' /#{file_name}"
            timestamp = $ptest.dis.host.exec!(cmd)
            @@pckg_time_info[lot] = [start_time, Time.now, nc_folder_etp3, timestamp]
            # reset start time for the next package
            start_time = Time.now
            $log.info "etp3.... finished lots: #{@@finished.inspect}"
          elsif !nc_folder_etp3.end_with?("out") && !!$ptest.check_files_in_folder(data, "#{nc_folder_etp3}/out", data_folder, limit_folder, nctype: "mnc") &&
            !@@finished.member?(lot)

            @@finished << lot
            # get timestamp
            file_name = "#{nc_folder_etp3}/out/#{data}.mnc"
            cmd = "stat -c '%Y' /#{file_name}"
            timestamp = $ptest.dis.host.exec!(cmd)
            @@pckg_time_info[lot] = [start_time, Time.now, "#{nc_folder_etp3}/out", timestamp]
            # reset start time for the next package
            start_time = Time.now
            $log.info "etp3.... finished lots: #{@@finished.inspect}"
          end
        end
      }
    end
    $log.info "used DISRouter time per data:"
    $log.info "Lot|start time|end time|nc_folder|timestamp"
    @@pckg_time_info.each{|t|
      j = 0
      @@lots.size.times{|i|
        if t[0] == @@lots[i]
          j = i
          break
        end
      }
      $log.info "#{t.inspect}, @@lots[#{j}]|"
    }
    return @@finished
  end

  # example to call:
    # folders = [ "#{$ptest.dis.indir[:parser_qa1]}/out",
                # "#{$ptest.dis.indir[:parser_qa2]}/out"]
    # wait_for_package_movements_all (:folders=>folders)
  def wait_for_package_movements_all(params={})
    @@finished = []
    cur_folder_info = {}
    @@pckg_time_info = {}
    def_folders = [ $ptest.dis.indir[:accepted],
                    $ptest.dis.indir[:published],
                    $ptest.dis.indir[:parser_qa1],
                    $ptest.dis.indir[:parser_qa2],
                    "#{$ptest.dis.indir[:parser_qa1]}/out",
                    "#{$ptest.dis.indir[:parser_qa2]}/out"]

    all_folders = (params[:folders] or def_folders)
    stop_f = (params[:stop_f] or "")
    start_time1 = Time.now
    start_time = Time.now
    $log.info "*************************************************************************************************************"
    $log.info "**************************** wait_for_package_movements_all ..."
    # prepare general hash list
    all_folders.each {|f| @@pckg_time_info[f] = {} }
    while @@finished.size < @@all_data_names.size && Time.now < start_time1 + @@wait_parser
      #
      @@all_data_names.keys.each {|lot|
        next if @@finished.member?(lot)
        data = @@all_data_names[lot]
        #
        all_folders.each {|f|
          data_folder = "#{f}/data"
          limit_folder = "#{f}/limit"
          next if @@pckg_time_info[f].size == @@all_data_names.size or @@pckg_time_info[f].keys.member?(lot)
          # ext_log = true if f.end_with?("published")
          if !!$ptest.check_files_in_folder(data, f, data_folder, limit_folder, nctype: "mnc")
            if (!stop_f.empty? && f == stop_f) or f.end_with?("out") or f == all_folders[-1]
              @@finished << lot
              $log.info ".... finished lots: #{@@finished.inspect}"
            end
            #
            file_name = "#{f}/#{data}.mnc"
            cmd = "stat -c '%Y' /#{file_name}"
            timestamp = $ptest.dis.host.exec!(cmd)
            @@pckg_time_info[f][lot] = [start_time, Time.now, f, timestamp]
            # reset start time for the next package
            start_time = Time.now
            $log.info "found pckg of lot: #{lot} in folder: #{f}"
          end
        }
      }
    end
    $pckg_time_info = @@pckg_time_info
    $log.info "used DISRouter time per data:"
    $log.info "Lot|start time|end time|nc_folder|timestamp"
    puts pp $pckg_time_info
    return @@finished
  end

  # upload etest parameters files to the space server
  def upload_files(what, data_dest, limit_dest, nc_dest, data, params={})
    $log.info "upload_files #{what.inspect} , data: #{data}"
    return nil unless what
    #
    if what == :parser or what == :parser2
      what_ext = :parser
    else
      what_ext = what
    end
    tmpdir = $ptest.dis.tmpdir
    nc_file = File.join(tmpdir,"#{data}.mnc")
    data_file = File.join(tmpdir,"#{data}_0.data.dis")
    limit_file = File.join(tmpdir,"#{data}_0.limit.dis")
    #
    # upload
    $ptest.dis.host.upload!(data_file, File.join($ptest.dis.indir[what_ext], data_dest)) unless params[:nodata]
    $ptest.dis.host.upload!(limit_file, File.join($ptest.dis.indir[what_ext], limit_dest)) unless params[:nolimit]
    $ptest.dis.host.upload!(nc_file, File.join($ptest.dis.indir[what], nc_dest)) unless params[:nonc]
  end

end
