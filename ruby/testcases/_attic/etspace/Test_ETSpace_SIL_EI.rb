=begin
Automated testing of the ETSpace parser.
Requirements:

(c) Copyright 2011 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Authors: Adel Darwaish, 2012-10-08
=end

require "RubyTestCase"
require "etspacetest"
require "etspace/etspace_ei"
require "misc"
require 'xmlhash'

class Test_ETSpace_SIL_EI < RubyTestCase
  attr_accessor :curr_sample_no

  include XMLHash

  Description = "Test ET Space/SIL"

  @@technology = "QA"
  @@pg = "PGQA1"
  @@op = "FINA-FWET.01"
  @@eqp = "UTF001"
  @@lot = "U282V.00"
  @@opNo = "1000.1000"

  @@corrective_actions = ["AutoLotHold", "LotHold", "ReleaseLotHold","CancelLotHold"]
  @@local_dir = 'tmp/etspace'
  @@folder = ''
  @@wait_parser = 240
  @@refresh_time = 60

  @curr_sample_no
  # alternative switch: execute all test cases: >0: execute, 0: do not execute
  @@all_tc = 0

  def setup
    puts "\n"
    $setup_ok = ($setup_ok != false)
    assert $setup_ok, "test setup is not correct"
  end

  def test100_setup
    $setup_ok = false
    #
    ($spc.close; $ptest = nil) if $spc and $spc.env != $env
    $ptest = ETSpace::TestParser.new($env, :nctype=>:nc) unless $ptest and $ptest.env == $env
    $dis = $ptest.dis
    $spc = $ptest.spc
    $lds = $spc.lds("ET_Fab1")
    $ei = ETSpace::EI.new($env)
    refute_nil $lds, "LDS not found"
    @@wait_parser = @@wait_parser.to_i
    @@folder = ["", @@technology, @@pg, @@op].join('/')
    $log.info "using folder #{@@folder}"
    $folder = $lds.folder(@@folder)
    ($log.info "folder #{f} does not exist"; next) unless $folder
    #
    lot_cleanup(true)
    @curr_sample_no = 0
    $setup_ok = true
  end

  def test201_send_spccheck_request_PASS
    filename = "c:/Temp/ETest_spc_response_#{@@eqp}_#{$env}.xml"

    sample_count = 1
    #
    # clean up channels
    delete_channels
    #
    # create cj on the UTF001
    cj = $sv.claim_process_lot(@@lot, :eqp=>@@eqp, :mode=>"Off-Line-1",:proctime=>5)
    refute_nil cj, "could not create cj for lot #{@@lot} on equipment #{@@eqp}"
    #
    # send data
    create_data_packages(sample_count, "mnc", false, :cj=>cj, :slot=>5)
    #
    # send spccheck request to SIL over MQ
    $log.info "send SpcCheck request to SIL. Waiting for response...."
    response = $ei.status_query(:lot=>@@lot, :eqp=>@@eqp, :cj=>cj, :op=>@@op)
    refute_nil response, "no status response received from SIL"
    # save payload content in an xml file:
    File.open(filename,"w") do |f|
      f.write(response)
    end
    #
    $log.info "sent msg to SIL is : #{$ei.request.inspect}"
    $log.info "resived response #{response.inspect}"
    assert response[:statusQueryResponse][:return][:status] == "PASS", "received response with status = FAIL"
    assert !response[:statusQueryResponse][:return][:comment].start_with?("Error"), "received response with error"
    comment = response[:statusQueryResponse][:return][:comment].split("\n")
    $log.info "response comment = #{comment}"
    #
    assert_equal "PASS", get_comment_result(response,"AQL"), "response result for AQL is not PASS"
    assert_equal "PASS", get_comment_result(response,"WLC"), "response result for WLC is not PASS"
    assert_equal "PASS", get_comment_result(response,"MAV"), "response result for MAV is not PASS"
  end

  # create new cj on SiView, send SPCCheck request,
  # send data package with control limit violation to space, expect error
  def test202_send_spccheck_response_error_climit_violation_FAIL
    sample_count = 1
    ch = $folder.spc_channel(:parameter=>"Param_0001")
    @curr_sample_no = ch.samples.size unless ch == nil
    #
    lot_cleanup(true)
    #
    # create cj on the UTF001
    cj = $sv.claim_process_lot(@@lot, :eqp=>@@eqp, :mode=>"Off-Line-1",:proctime=>5)
    refute_nil cj, "could not create cj for lot #{@@lot} on equipment #{@@eqp}"
    #
    # set corrective actions
    set_corrective_actions(@@corrective_actions)
    #
    # set control limits for all parameter (channels) of the folder.
    # second param is the desired value for the MEAN_VALUE_UCL limit
    cs = $folder.spc_channels
    cs.each{|c|
      c.set_limits({"MEAN_VALUE_UCL"=>60, "MEAN_VALUE_CENTER"=>1.0, "MEAN_VALUE_LCL"=>0.0})
    }
    #
    # Send new samples data values with control limit violation of parameter 1
    create_data_packages(sample_count, "mnc", false, :v1=>72, :cj=>cj, :slot=>5)
    # wait until all expected samples are populated successfully
    param_names = get_param_names()
    check_channel_samples(@curr_sample_no+1,param_names)
    #
    # check lots future hold
    fhl = $ptest.check_futurehold(@@lot)
    assert fhl.size > 0, "no future holds for lot #{@@lot}"
    assert_equal 1, fhl.size, "more than 1 future hold found for lot #{@@lot}"
    assert_equal "X-E1", fhl[0].reason, "wrong future hold reason found for lot #{@@lot}"
    #
    # send spccheck request to SIL over MQ
    $log.info "send SpcCheck request to SIL. Waiting for response...."
    response = $ei.status_query(:lot=>@@lot, :eqp=>@@eqp, :cj=>cj, :op=>@@op)
    refute_nil response, "no status response received from SIL"
    $log.info "sent msg to SIL is : #{$ei.request.inspect}"
    $log.info "resived response #{response.inspect}"
    assert response[:statusQueryResponse][:return][:status] == "FAIL", "received response with status not equal FAIL"
    comment = response[:statusQueryResponse][:return][:comment].split("\n")
    $log.info "response comment = #{comment}"
    assert_equal "HOLD", get_comment_result(response, "VAR"), "response result for var is not HOLD"
  end

  # create new cj on SiView, send SPCCheck request,
  # send data package with control limit violation to space, expect fail, EIMustPutLotOnHold = YES
  def test203_SIL_response_EI_Hold
    sample_count = 1
    # clean up channels
    delete_channels
    #
    lot_cleanup(true)
    #
    # create cj on the UTF001
    cj = $sv.claim_process_lot(@@lot, :eqp=>@@eqp, :mode=>"Off-Line-1",:proctime=>5)
    refute_nil cj, "could not create cj for lot #{@@lot} on equipment #{@@eqp}"
    #
    # send data
    create_data_packages(sample_count, "mnc", false, :cj=>cj, :slot=>5)
    # save current
    ch = $folder.spc_channel(:parameter=>"Param_0001")
    @curr_sample_no = ch.samples.size unless ch == nil
    #
    # set corrective actions
    set_corrective_actions(@@corrective_actions)
    #
    # set control limits for all parameter (channels) of the folder.
    # second param is the desired value for the MEAN_VALUE_UCL limit
    cs = $folder.spc_channels
    cs.each{|c|
      c.set_limits({"MEAN_VALUE_UCL"=>60, "MEAN_VALUE_CENTER"=>1.0, "MEAN_VALUE_LCL"=>0.0})
    }
    #
    # set all ETspace future holds
    reason = ""
    (1..9).each{|i|
      reason = "X-E" + i.to_s
      # use the after next Op to avoid setting the lot On Hold when current PD process completed.
      assert $sv.lot_futurehold(@@lot, "1200.1000", reason) == 0, "failed to set future hold for lot with reason = #{reason}"
    }
    lot_cleanup(false)
    #
    # create cj on the UTF001
    cj = $sv.claim_process_lot(@@lot, :eqp=>@@eqp, :mode=>"Off-Line-1",:proctime=>5)
    refute_nil cj, "could not create cj for lot #{@@lot} on equipment #{@@eqp}"
    #
    # Send new samples data values with control limit violation of parameter 1
    create_data_packages(sample_count, "mnc", false, :v2=>72, :cj=>cj, :slot=>5)
    # wait until all expected samples are populated successfully
    param_names = get_param_names()
    check_channel_samples(@curr_sample_no+1,param_names)
    #
    # send spccheck request to SIL over MQ
    $log.info "send SpcCheck request to SIL. Waiting for response...."
    response = $ei.status_query(:lot=>@@lot, :eqp=>@@eqp, :cj=>cj, :op=>@@op)
    refute_nil response, "no status response received from SIL"
    $log.info "sent msg to SIL is : #{$ei.request.inspect}"
    $log.info "resived response #{response.inspect}"
    assert response[:statusQueryResponse][:return][:status] == "FAIL", "received response with status not equal FAIL"
    comment = response[:statusQueryResponse][:return][:comment].split("\n")
    assert_equal "HOLD", get_comment_result(response, "VAR"), "response result for var is not HOLD"
    assert response[:statusQueryResponse][:return][:EIMustPutLotOnHold] == "YES", "EIMustPutLotOnHold is not YES"
  end

  # Test what action to be taken by ETParser when for a received .nc/mnc file no matching Wet Step mapping information in parserPath.xml configuration file exist.
  # use an mnc with a not configured PD in the parserPath.xml
  def test204_spccheck_mnc_with_invalid_PD
  end

  # deactivated test case. not required for the moment
  # create new cj on SiView, send SPCCheck request without sending data packages to space, Expect nil response
  def xtest20X_send_spccheck_request_no_response
    # clean up channels
    delete_channels
    #
    lot_cleanup(true)
    # create cj on the UTF001
    cj = $sv.claim_process_lot(@@lot, :eqp=>@@eqp, :mode=>"Off-Line-1",:proctime=>5)
    refute_nil cj, "could not create cj for lot #{@@lot} on equipment #{@@eqp}"
    # #
    # send spccheck request to SIL over MQ
    $log.info "send SpcCheck request to SIL. Waiting for response...."
    response = $ei.status_query(:lot=>@@lot, :eqp=>@@eqp, :cj=>cj, :op=>@@op)
    $log.info "sent msg to SIL is : #{$ei.request.inspect}"
    $log.info "resived response #{response.inspect}"
    assert_nil response, "a response received from SIL for job #{cj}, lot #{@@lot}"
  end

  # no data package and no SiView job is required. just use the existing channels from previous tests to assign
  # CAs for the same sample.
  def test205_SIL_mutiple_CA_lothold_same_sample
    # clean up future holds
    $ptest.cancel_futrueholds(@@lot)
    sleep 10
    # save current
    ch = $folder.spc_channel(:parameter=>"Param_0002")
    #
    # set corrective actions
    set_corrective_actions(@@corrective_actions)
    #
    s = ch.samples(:days=>60)[-1]
    # set the first ca lothold
    assert s.assign_ca($spc.corrective_action(@@corrective_actions[1])), "error assigning corective action";
    sleep 5
    fhl = $ptest.check_futurehold(@@lot, :reason=>"X-E1")
    assert fhl.size > 0 , "no future holds for lot #{@@lot} with reason code = X-E1 found"

    # set mutiple ca lothold for the same sample. must not accepted by SIL.
    (1..9).each {|i|
    #
    #  set LotHold
      assert_equal false, s.assign_ca($spc.corrective_action(@@corrective_actions[1])), "assigning corective action accepted which should not";
    }
  end

  def test206_SIL_mutiple_CA_Cancellothold_same_sample
    # get channel for Param_0002
    ch = $folder.spc_channel(:parameter=>"Param_0002")
    #
    # set corrective actions
    set_corrective_actions(@@corrective_actions)
    #
    s = ch.samples(:days=>60)[-1]
    # get futurehold list
    fhl = $ptest.check_futurehold(@@lot)
    # quit test if there is no futurehold for the lot.
    return if fhl.size == 0
    #
    (1..fhl.size).each {|h|
    #  set LotHold
      assert s.assign_ca($spc.corrective_action(@@corrective_actions[3])), "assigning corective action #{@@corrective_actions[3]} failed for sample id #{s.sid}";
    }
    fhl = $ptest.check_futurehold(@@lot)
    return if fhl.size == 0
    #
    fhl.each {|fh|
        assert_equal false, fh.reason.start_with?("X-E"), "found futurehold with reason start with X-E for lot #{@@lot}"
    }
  end

  # *********** Aux methods *****************************************
  def delete_channels
    # delete channels and verify deletion
    $log.info "start deleting channels. wait for a while......"
    while $folder.spc_channels.size > 0 do
      assert $folder.delete_channels, "error deleting channnels in folder #{@@folder}"
    end
    @curr_sample_no = 0
  end

  # general method to create mutiple data parameter samples
  # input param with_spec_violation = true if required to provoke spec violation
  def create_data_packages(sample_count, nctype, with_spec_violation, params={})
    # send data
    lot=@@lot
    $log.info "Enter method create_data_packages. inputs: #{sample_count}, #{nctype}, #{with_spec_violation}, #{params.inspect}"
    (1..sample_count).each {|i|
      values = []
      mywafers = (params[:wafers] or nil)
      slot = (params[:slot] or 5)

      $log.info "*****************************************************************"
      $log.info "*** creating data sample number: #{i.to_s} for lot: #{lot} *********"
      $log.info "*****************************************************************"
      (nctype == "nc")? mynctype = :nc : mynctype = :mnc
      $dis = ETSpace::DIS2.new($env, :pg=>@@pg, :lot=>lot, :wafers=>mywafers, :nctype=>mynctype, :cj=>params[:cj], :slot=>slot)
      # $dis.add_defaults(:clow=>-1e2, :chigh=>1e2)
      # generate 10 parameter values randomly between clow & chigh
      (1..10).each {|i|
        a = [-rand(1e2), rand(1e2)]
        v = a[rand(a.size)]
        values << v
      }
      $log.info "1st values = #{values.inspect}"
      #
      # may it is required to provoke control limits violation on any parameter
      values[1] = params[:v1] if params[:v1] != nil
      values[2] = params[:v2] if params[:v2] != nil
      values[3] = params[:v3] if params[:v3] != nil
      values[4] = params[:v4] if params[:v4] != nil
      values[5] = params[:v5] if params[:v5] != nil
      values[6] = params[:v6] if params[:v6] != nil
      values[7] = params[:v7] if params[:v7] != nil
      values[8] = params[:v8] if params[:v8] != nil
      values[9] = params[:v9] if params[:v9] != nil
      values[10] = params[:v10] if params[:v10] != nil
      #
      # provoke spec limit violation for the first parameter if desired.
      $log.info "with_spec_violation = #{with_spec_violation}"
      if with_spec_violation
        values[0] = 1e21
        # values[0] += 1e2 if values[0] <= 1e2 && values[0] >= 0
        # values[0] -= 1e2 if values[0] >= -1e2 && values[0] <=0
      end

      $log.info "2nd values = #{values.inspect}"
      $dis.add_values(values, :llow=>-1e2, :lhigh=>1e2,:clow=>-1e2, :chigh=>1e2)
      $dis.remote_write(:parser)
      #
      # verify channel creation
      $log.info "waiting for ETParser...."
      while $dis.limit[0].wet_limit_size * 2 > $folder.spc_channels.size do
        sleep 10
      end
      #
      # read limits data out of limit file for comparison
      param_names = get_param_names()
      #
      # check parameters names in space
      param_names.each {|p|
        # get the name of the parameter
        refute_nil $folder.spc_channel(:parameter=>p), "limit parameter #{p} not populated to space"
      }
    }
  end

  # returns an array of the limits data records of the specified file
  def get_param_names
    $log.info "enter method get_param_names"
    data = []
    param_names = $dis.limit[0].limitfile_to_data
    refute_nil param_names, "returns nil param_names"
    limits = param_names[:wet_limit].to_a
    limits.each{|l|
      data << l.to_string.split('|')[1]
    }
    return data
  end

  def set_corrective_actions(corrective_actions)
    cs = $folder.spc_channels
    cs.each{|c|
      if c.name.start_with?("Param_") && !c.name["_Loss"]
        $log.info "handle channel: #{c.name}"
        refute_equal 0, c.valuations.size, "no valuations defined for channel #{c.name}"
        #
        # cleanup corrective actions -> this is done by overrite them with empty array
        assert_nil $spc.set_corrective_actions(c, []), "could not remove corrective action for channel #{c.name}"
        #
        # verify that no corrective actions still exist
        c.valuations.each{|v|
          ca = $spc.corrective_actions(v)
          if ca.size > 0 then
            $log.warn "corrective action exist for the valuation = #{v.name} with vid = #{v.vid} of channel #{c.name}";
            next
          end
        }
        #
        # set new corrective action
        assert_nil $spc.set_corrective_actions(c, corrective_actions), "could not set corrective action for channel #{c.name}"
        # verify the assignement of the CAs for all valuation
        c.valuations.each{|v|
          ca = $spc.corrective_actions(v)
          refute_equal 0, ca.size, "no corrective action was set for the valuation = #{v.name} with vid = #{v.vid} of channel #{c.name}"
        }
      end
    }
  end

  def check_channel_samples (count, data)
    $log.info "check channel samples...."
    parameters = []
    parameters = data
    while parameters.size > 0 do
      data.each {|p|
            ch = $folder.spc_channel(:parameter=>p)
            parameters = parameters - [p] if ch.samples.size == count
        }
    end
  end

  def get_comment_result(response, type, params={})
    comment = response[:statusQueryResponse][:return][:comment].split("\n")
    char = (params[:char] or " ")
    comment.each {|c|
      if(c.split(char)[0].strip == type)
        return c.split(char)[1].gsub(/\s+/, "")   # gsub is to remove all spaces
      end
    }
    return nil
  end

  def lot_cleanup(release_holds, params={})
    li = $sv.lot_info(@@lot)
    if release_holds
      assert $sv.lot_hold_release(@@lot, nil) == 0, "failed to release lot holds"
      assert $ptest.cancel_futrueholds(@@lot) == 0, "failed to cancel future holds" #unless $ptest.check_futurehold(@@lot) == []
    end
    if li.xfer_status == "EI" or li.xfer_status == "MI"
      assert $sv.carrier_xfer_status_change(li.carrier, "EO", li.opEqps[0]) == 0, "failed to change xfer status of carrier #{li.carrier} to EO"
    end
    force = (params[:force] or false)
    assert $sv.lot_opelocate(@@lot, @@opNo, :force=>force) == 0, "failed to locate lot #{} to op #{@@opNo}"
  end
end
