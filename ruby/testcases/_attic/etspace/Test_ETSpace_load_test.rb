=begin
Automated testing of the ETSpace parser.
Requirements: 

(c) Copyright 2011 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Authors: Adel Darwaish, 2012-10-04
         Daniel Steger, 2014-08-04 added Fab8 setup
=end

require "RubyTestCase"
require "etspacetest"
require "misc"
require "etspaceload"

class Test_ETSpace_load_test < RubyTestCase
  Description = "Test ET Space/SIL"
    
  def test00_setup
    $setup_ok = false
    $etspace = ETSpace::Exerciser.new($env, nctype: :mnc) unless $etspace and $etspace.env == $env
    $setup_ok = true
  end
  
    
  def test10_performance_Test_mnc_1Pckg_1_wafer_10Params
    res = $etspace.load_test(:perflog_filter=>"log/etspace_perf_filter_#{Time.now.utc.iso8601.gsub(':', '-')}_1Pck_1W_10Par.csv", :perflog_parser=>"log/etspace_perf_parser_#{Time.now.utc.iso8601.gsub(':', '-')}_1Pck_1W_10Par.csv", :timeout=>1000, :speed=>1, :iterations=>0, :duration=>60, 
              :packages=>1, :wafers=>1, :parameters=>10, :violations=>0, :recipefine=>true, :nctype=>:mnc)
  end   
  def test11_performance_Test_mnc_1Pckg_25_wafer_10Params
    res = $etspace.load_test(:perflog_filter=>"log/etspace_perf_filter_#{Time.now.utc.iso8601.gsub(':', '-')}_1Pck_25W_10Par.csv", :perflog_parser=>"log/etspace_perf_parser_#{Time.now.utc.iso8601.gsub(':', '-')}_1Pck_25W_10Par.csv", :timeout=>1000, :speed=>1, :iterations=>0, :duration=>60, 
              :packages=>1, :wafers=>25, :parameters=>10, :violations=>0, :recipefine=>true, :nctype=>:mnc)
  end   

  def test12_performance_Test_mnc_3Pckg_1_wafer_10Params
    res = $etspace.load_test(:perflog_filter=>"log/etspace_perf_filter_#{Time.now.utc.iso8601.gsub(':', '-')}_3Pck_1W_10Par.csv", :perflog_parser=>"log/etspace_perf_parser_#{Time.now.utc.iso8601.gsub(':', '-')}_3Pck_1W_10Par.csv", :timeout=>1000, :speed=>1, :iterations=>0, :duration=>60, 
              :packages=>3, :wafers=>1, :parameters=>10, :violations=>0, :recipefine=>true, :nctype=>:mnc)
  end   
  
  def test13_performance_Test_mnc_3Pckg_25_wafer_10Params
    res = $etspace.load_test(:perflog_filter=>"log/etspace_perf_filter_#{Time.now.utc.iso8601.gsub(':', '-')}_3Pck_25W_10Par.csv", :perflog_parser=>"log/etspace_perf_parser_#{Time.now.utc.iso8601.gsub(':', '-')}_3Pck_25W_10Par.csv", :timeout=>1000, :speed=>1, :iterations=>0, :duration=>60, 
              :packages=>3, :wafers=>25, :parameters=>10, :violations=>0, :recipefine=>true, :nctype=>:mnc)
  end   

  def test14_performance_Test_mnc_5Pckg_1_wafer_10Params
    res = $etspace.load_test(:perflog_filter=>"log/etspace_perf_filter_#{Time.now.utc.iso8601.gsub(':', '-')}_5Pck_1W_10Par.csv", :perflog_parser=>"log/etspace_perf_parser_#{Time.now.utc.iso8601.gsub(':', '-')}_5Pck_1W_10Par.csv", :timeout=>1000, :speed=>1, :iterations=>0, :duration=>60, 
              :packages=>5, :wafers=>1, :parameters=>10, :violations=>0, :recipefine=>true, :nctype=>:mnc)
  end   

  def test15_performance_Test_mnc_5Pckg_25_wafer_10Params
    res = $etspace.load_test(:perflog_filter=>"log/etspace_perf_filter_#{Time.now.utc.iso8601.gsub(':', '-')}_5Pck_25W_10Par.csv", :perflog_parser=>"log/etspace_perf_parser_#{Time.now.utc.iso8601.gsub(':', '-')}_5Pck_25W_10Par.csv", :timeout=>1000, :speed=>1, :iterations=>0, :duration=>60, 
              :packages=>5, :wafers=>25, :parameters=>10, :violations=>0, :recipefine=>true, :nctype=>:mnc)
  end   
  
end
