=begin
Automated testing of the ETSpace parser.
Requirements:

(c) Copyright 2011 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Authors: Adel Darwaish, 2012-10-04
=end

require "RubyTestCase"
require "etspacetest"
require "misc"


class Test_ETSpace_Parser_perf < RubyTestCase
  Description = "Test ET Space/SIL"

  @@technology = "QA"
  @@pg = "PGQA1"
  @@op = "FINA-FWET.01"
  @@product1 = "SHELBY21AD.B1"
  @@product2 = "SHELBY21AD.B2"
  @@product3 = "SHELBY21AD.B3"
  @@lot = "U282V.00"
  @@dummy_lot = "LOT"

  @@wait_parser = 240
  @@refresh_time = 30

  # # @@nparams = [10, 20, 50, 100, 200, 500, 1000, 3000]
  @@nparams = [5, 3, 3, 3, 3]

  @@m_wafers_per_lot = 5
  @@n_lots = 5

  @@lcl = -3
  @@center = 0.0
  @@ucl = 3
  @@lsl = -5
  @@usl = 5

  @@corrective_actions = ["AutoLotHold", "LotHold", "ReleaseLotHold","CancelLotHold"]
  @@filename = "c:/Temp/Adels Own/GF/Etest/Expirements/etspace_perf_report.txt"
  @@rep_start_text = "*****************************************************************************************\n"

  def setup
    puts "\n"
    $setup_ok = ($setup_ok != false)
    assert $setup_ok, "test setup is not correct"
  end

  def test00_setup
    $setup_ok = false
    #
    ($spc.close; $ptest = nil) if $spc and $spc.env != $env
    $ptest = ETSpace::TestParser.new($env, :nctype=>:mnc) unless $ptest and $ptest.env == $env
    $dis = $ptest.dis
    $spc = $ptest.spc
    $lds = $spc.lds("ET_Fab1")
    refute_nil $lds, "LDS not found"
    @@wait_parser = @@wait_parser.to_i
    @@folder = ["", @@technology, @@pg, @@op].join('/')
    $log.info "using folder #{@@folder}"
    $folder = $lds.folder(@@folder)
    ($log.info "folder #{f} does not exist"; next) unless $folder
    #
    @@rep_start_text += "* ETSpace ETPasrer and DISFilter performance report \n"
    @@rep_start_text += "* Created on: #{Time.now.strftime("%d/%m/%Y %H:%M")} \n"
    @@rep_start_text += "*\n"
    @@rep_start_text += "*****************************************************************************************\n"
    @@rep_start_text += "
    "
    res = create_new_file(@@rep_start_text)
    assert $setup_ok =  res > 0, "failed to create a new file for reporting. returned res: #{res}"
    $setup_ok = true
  end

  def test10_performance_Test_mnc
    process_times = Hash.new
    @@nparams.each {|c|
      # delete channels to clean up
      while $folder.spc_channels.size > 0 do
        assert $folder.delete_channels, "error deleting channnels in folder #{@@folder}"
      end
      # send data
      $dis = ETSpace::DIS2.new($env, :pg=>@@pg, :lot=>@@lot, :nctype=>:mnc)
      $dis.add_defaults(:count=>c)
      $dis.remote_write(:parser)
      #
      $log.info "waiting #{30}s for the parser to start....."
      sleep 30
      #
      # hold start time
      start_time = Time.now
      $log.info "***************************************************************************"
      $log.info "********** loap and refresh channels every #{@@refresh_time}s for the parser. Number of parameters to parse is #{c} *******"
      $log.info "***************************************************************************"
      while $dis.limit[0].wet_limit_size * 2 > $folder.spc_channels.size do
        sleep @@refresh_time
      end
      used_time = (Time.now - start_time)/60
      $log.info "time used for the parser to parse #{c} params is #{'%.2f' % used_time} min"
      process_times[c] = used_time
    }
    $log.info "used time for diffrent parameters count:"
    $log.info ""
    $log.info "params count | time/min"
    $log.info "-------------|-----"
    process_times.each_key {|k|
      $log.info "   #{k}        | #{'%.2f' % process_times[k]}"
    }
  end

  def test11_m_wafers_1_lot_1_mnc
    # delete channels to clean up
    # assert $folder.delete_channels, "error deleting channnels in folder #{@@folder}"
    #
    # send data
    wafers = []
    @@m_wafers_per_lot.times{|i| wafers << "#{lot}.#{i.to_s}"}

    $dis = ETSpace::DIS2.new($env, :pg=>@@pg, :lot=>@@lot, :wafers=>wafers, :nctype=>:mnc)
    $dis.add_defaults
    $dis.write()
    return
    #
    # verify channel creation
    $log.info "waiting #{@@wait_parser}s for the parser"
    sleep @@wait_parser
    assert $dis.limit[0].wet_limit_size * 2 <= $folder.spc_channels.size, "channels in #{@@folder} have not been created"
    #
  end

  def test12_m_wafers_n_lots_n_mnc
    # delete channels to clean up
    # assert $folder.delete_channels, "error deleting channnels in folder #{@@folder}"
    #
    # send data
    lots = []
    @@n_lots.times{|j|
      wafers = []
      lot = @@dummy_lot + j.to_s
      @@m_wafers_per_lot.times{|i| wafers << "#{lot}.#{i.to_s}"}
      #
      $dis = ETSpace::DIS2.new($env, :pg=>@@pg, :lot=>lot, :wafers=>wafers, :nctype=>:mnc)
      $dis.add_defaults
      $dis.write()
    }
    return
    #
    # verify channel creation
    $log.info "waiting #{@@wait_parser}s for the parser"
    sleep @@wait_parser
    assert $dis.limit[0].wet_limit_size * 2 <= $folder.spc_channels.size, "channels in #{@@folder} have not been created"
    #
  end

  def test13_create_n_packages_x_AQL_violations_same_product_10_params
    n_pckgs = 5
    #
    # 10% of packages have aql violation
    perc_aql = (0.2) * n_pckgs
    #
    aql_pckgs = []
    @@all_data_names = {}
    local_write = false
    write_report_info("test case: test13_create_n_packages_x_AQL_violations_same_product_10_params")
    #
    # delete channels to clean up
    # $ptest.delete_channels()
    #
    (perc_aql.to_i).times {|i| aql_pckgs << i*10 }
    #
    # create basic package for channels auto charting on space
    assert $ptest.create_verify_data(1,false, :pg=>@@pg, :lot=>@@dummy_lot + 0.to_s, :nctype=>:mnc, :product=>@@product1, :op=>@@op,
      :local_write=>local_write, :check_channels=>true, :lcl=>-@@lcl, :ucl=>@@ucl, :what=>:parser), "failed to create channesl in space during timeout"
    #
    set_limit_and_customer_fields if !local_write
    $ptest.set_corrective_actions(@@corrective_actions)
    # create several packages
    $log.info "created aql_packgs index array: #{aql_pckgs.inspect}"
    n_pckgs.times {|j|
      site_values = []
      wafers = []
      lot = @@dummy_lot + j.to_s
      if aql_pckgs[0] == j
        site_values = create_aql_violation_values()
        refute_equal 0, site_values.size, "returned empty site values of aql violation"
        aql_pckgs.delete_at(0)
      end
      @@m_wafers_per_lot.times{|i| wafers << "#{lot}.#{i.to_s}"}
      if site_values.size > 0
        assert $ptest.create_verify_data(1,false, :pg=>@@pg, :lot=>lot, :wafers=>wafers, :nctype=>:nc, :product=>@@product1, :op=>@@op, :site_values=>site_values,        :local_write=>local_write , :what=>:parser)
        $log.info "created package no. #{j} for lot #{lot} with AQL violation"
      else
        $ptest.create_verify_data(1,false, :pg=>@@pg, :lot=>lot, :wafers=>wafers, :nctype=>:nc, :product=>@@product1, :op=>@@op,
          :local_write=>local_write, :what=>:parser)
      end
      $ptest.dis.dis_file_names[0].slice!(".nc")
      @@all_data_names[lot] = $ptest.dis.dis_file_names[0]
    }
    #
    $log.info "******************************************************************************"
    $log.info "*************** following data package names are created: "
    $log.info "******************************************************************************"
    @@all_data_names.each {|data| $log.info "#{data}"}
    return if local_write
    #
    # wait untill all samples are created on space charts
    param_names = $ptest.get_param_names()
    assert param_names != nil || param_names.size > 0, "empty parameters names returned"
    assert $ptest.check_channel_samples(n_pckgs+1, param_names, :waiting_time=>700), "failed to check samples"
    #
    # get start time of the first sample for the first parameter
    all_ch = $ptest.folder.spc_channels
    all_ch.each {|ch|
      first_ch = ch if ch.parameter == param_names[0]
      last_ch = ch if ch.parameter == param_names[-1]
    }
    start_time = first_ch.samples[0].time
    end_time = last_ch.samples[-1].time
    used_time = (end_time - start_time)/60
    $log.info "time in minutes used to process #{n_pckgs+1} packages is #{used_time.to_s} mins"
    used_time = used_time/60
    $log.info "time in hours used to process #{n_pckgs+1} packages is #{used_time.to_s} hrs"
  end

  def test14_write_in_report
    write_report_info("test case: test14_write_in_report")
    write_report_info("     start_time: #{Time.now}")
    sleep 2
    write_report_info("     end_time: #{Time.now}")
    write_report_info("------------------------------")
  end

  def test15_write_in_report
    write_report_info("test case: test15_write_in_report")
    write_report_info("     start_time: #{Time.now}")
    sleep 2
    write_report_info("     end_time: #{Time.now}")
    write_report_info("------------------------------")
  end

  # Aux help methods
  def set_limit_and_customer_fields
    $log.info "enter method set_limit_and_customer_fields. wait..."
    param_names = $ptest.get_param_names
    cs = $folder.spc_channels
    cs.each{|ch|
      next if !param_names.member?(ch.parameter)
      assert ch.set_limits({"MEAN_VALUE_UCL"=>@@ucl, "MEAN_VALUE_CENTER"=>0.0, "MEAN_VALUE_LCL"=>@@lcl, "SIGMA_UCL"=>1.0, "SIGMA_CENTER"=>nil, "SIGMA_LCL"=>nil}), "failed to set limit for channel #{ch.name}"
      # set customer fields
      fields = ch.customer_fields.merge("Maverick check"=>"1;1;15", "Maverick-Action"=>"email+hold", "Criticality"=> "Critical")
      assert_equal nil, ch.set_customer_fields(fields), "failed to set customer field 'Maverick check' for channel #{ch.name}"
      assert ch.customer_fields.member?("Maverick check"), "customer field 'Maverick check' for channel #{ch.name} was not set!"
    }
    $log.info "leave method set_limit_and_customer_fields"
  end

  def create_aql_violation_values
    site_values = []
    values = []
    (0..16).each {|i|
      (0..9).each {|j|
        if(j == 0)
          (values << @@ucl + i* 0.2 ) if i.even?
          (values << @@lcl - i* 0.2 ) if i.odd?
        else
          (values << @@ucl - i* 0.2 - j*0.2) if i.even?
          (values << @@lcl + i* 0.2 + j*0.2) if i.odd?
        end
      }
      site_values << values
      values = []
    }
    return site_values
  end

  def create_new_file(text)
    file = File.open(@@filename,"w")do |f|
        f.write(text)
    end
    return file
  end

  def write_report_info(info)
      File.open(@@filename,"a")do |f|
        f.write(Time.now.strftime("%d/%m/%Y %H:%M") + ": " + info + "\n")
        f.close
      end
  end
end
