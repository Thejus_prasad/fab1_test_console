=begin
Automated testing of the ETSpace parser.
Requirements:

(c) Copyright 2011 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Authors: Daniel Steger, 2013-03-13
=end

require "RubyTestCase"
require "etspacetest"
require "misc"


class Test_ETSpace_F8 < RubyTestCase
  Description = "Test ET Space/SIL for Fab8"

  @@technology = "QA"
  @@pg = "QAPG1"
  @@op = "TD-WET-OBANPK.01"
  @@product = "SILPROD.01"
  @@opno_wet = "6460.4720"
  @@opno_dispo = "6460.4730"
  @@op_next = "6460.5000" #next operation after DISPO

  @@eqp = "ATC22100"

  @@lot = "8XYK16003.000"

  @@wait_parser = 180

  @@nc_type = :mnc

  @@corrective_actions = ["AutoLotHold", "ReleaseLotHold"]



  def setup
    super
    puts "\n"
    $setup_ok = ($setup_ok != false)
    assert $setup_ok, "test setup is not correct"
  end

  def self.create_lot

  end

  def test0000_setup
    $setup_ok = false
    @@lot = $sv.new_lot_release(:template=>"SIL006.00", :stb=>true, :waferids=>"t7m12", :sublottype=>"qgt") unless $sv.lot_info(@@lot)
    @@wafers = $sv.lot_info(@@lot, :wafers=>true).wafers.map {|w| w.wafer}
    #
    ($spc.close; $ptest = nil) if $spc and $spc.env != $env
    @@folder_attr = ["", @@technology, @@pg, @@op, "ATTR"].join('/')
    @@folder_vari = ["", @@technology, @@pg, @@op, "VARI"].join('/')
    $ptest = ETSpace::TestParser.new($env, :lds=>"Fab8_ETest", :nctype=>@@nc_type, :eqp=>@@eqp, :lot=>@@lot, :folder=>@@folder_attr) unless $ptest and $ptest.env == $env
    $dis = $ptest.dis
    $spc = $ptest.spc
    $lds = $ptest.lds
    refute_nil $lds, "LDS not found"
    $folder_attr = $lds.folder(@@folder_attr)
    $folder_vari = $lds.folder(@@folder_vari)
    # Spec needs to be valid
    #assert $spctest.verify_speclimit(:specid=>"M14-00020060", :specversion=>12, :all_active=>true), "Speclimits do not exist"
    #
    #
    $setup_ok = true
  end

  def test0001_single_wafer_nc_dispo_operation
    _prepare_lot(@@lot)

    # delete channels to clean up
    assert $folder_attr.delete_channels, "error deleting channnels in folder #{@@folder_attr}"
    assert_equal 0, $folder_attr.spc_channels.size, "channels in #{@@folder_attr} have not been deleted"
    #
    # send data
    $dis = ETSpace::DIS2.new($env, :pg=>@@pg, :product=>@@product, :eqp=>@@eqp, :lot=>@@lot, :wafers=>@@wafers[0], :nctype=>@@nc_type,
      :flow=>"FAB8", :mfg_area=>"FAB8", :op=>@@op)
    $dis.add_defaults
    txtime = Time.now
    $dis.remote_write(:filter)
    #
    # verify channel creation
    $log.info "waiting #{@@wait_parser}s for the parser"
    sleep @@wait_parser

    channels = $lds.all_spc_channels("Param_00*")

    # Only first 5 parameters are critical for upload (2 channels for each are created)
    assert_equal 2*$dis.limit[0].wet_limit_size, channels.size, "channels in #{@@folder_attr} have not been created"

    # Check no gatepass or lothold, but lot note
    assert $ptest.verify_lot_state(@@lot, @@op_next), "Wrong lot state"
    assert $ptest.verify_no_lot_note(@@lot, @@op, txtime), "Lot should not have note"
  end

  def test0002_single_wafer_nc_other_operation
    _prepare_lot(@@lot)
    assert $sv.lot_opelocate(@@lot, @@op_next), "failed to locate"

    # delete channels to clean up
    assert $folder.delete_channels, "error deleting channnels in folder #{@@folder}"
    assert_equal 0, $folder.spc_channels.size, "channels in #{@@folder} have not been deleted"
    #
    # send data
    $dis = ETSpace::DIS2.new($env, :pg=>@@pg, :product=>@@product, :eqp=>@@eqp, :lot=>@@lot, :wafers=>@@wafers[0], :nctype=>:nc, :flow=>"FAB8", :mfg_area=>"FAB8", :op=>@@op)
    $dis.add_defaults
    txtime = Time.now
    $dis.remote_write(:filter)
    #
    # verify channel creation
    $log.info "waiting #{@@wait_parser}s for the parser"
    sleep @@wait_parser

    channels = $lds.all_spc_channels("Param_00*")

    # Only first 5 parameters are critical for upload (2 channels for each are created)
    assert_equal $dis.limit[0].wet_limit_size, channels.size, "channels in #{@@folder} have not been created"

    # Check no gatepass or lothold, but lot note
    assert $ptest.verify_lot_state(@@lot, @@op_next), "Wrong lot state"
    assert $ptest.verify_lot_note(@@lot, @@op, txtime), "No lot note found"
  end

  def Xtest0003_multiple_wafers_nc
    # delete channels to clean up
    assert $folder.delete_channels, "error deleting channnels in folder #{@@folder}"
    #
    # send data
    $dis = ETSpace::DIS2.new($env, :pg=>@@pg, :lot=>@@lot, :wafers=>["#{@@lot}.03", "#{@@lot}.04"], :nctype=>:nc,
      :mfg_area=>"FAB8", :op=>@@op, :wet=>"SWET" )
    $dis.add_defaults
    $dis.remote_write(:filter)
    #
    # verify channel creation
    $log.info "waiting #{@@wait_parser}s for the parser"
    sleep @@wait_parser
    assert $dis.limit[0].wet_limit_size * 2 <= $folder.spc_channels.size, "channels in #{@@folder} have not been created"
    #
  end

  def Xtest0003_multiple_wafers_mnc
    # delete channels to clean up
    assert $folder.delete_channels, "error deleting channnels in folder #{@@folder}"
    #
    # send data
    $dis = ETSpace::DIS2.new($env, :pg=>@@pg, :lot=>@@lot, :wafers=>[["#{@@lot}.03"], ["#{@@lot}.04"], ["#{@@lot}.15"]], :nctype=>:mnc)
    $dis.add_defaults
    $dis.remote_write(:filter)
    #
    # verify channel creation
    $log.info "waiting #{@@wait_parser}s for the parser"
    sleep @@wait_parser
    assert $dis.limit[0].wet_limit_size * 2 <= $folder.spc_channels.size, "channels in #{@@folder} have not been created"
    #
  end

  def test0004_multiple_wafers_mnc_1_lot
    # delete channels to clean up
    assert $folder.delete_channels, "error deleting channnels in folder #{@@folder}"
    #
    # send data
    $dis = ETSpace::DIS2.new($env, :pg=>@@pg, :lot=>@@lot, :wafers=>[["#{@@lot}.03","#{@@lot}.04","#{@@lot}.15"]], :nctype=>:mnc)
    $dis.add_defaults
    $dis.remote_write(:filter)
    #
    # verify channel creation
    $log.info "waiting #{@@wait_parser}s for the parser"
    sleep @@wait_parser
    assert $dis.limit[0].wet_limit_size * 2 <= $folder.spc_channels.size, "channels in #{@@folder} have not been created"
    #
  end

  def test0010_single_wafer_nc_next_test_pd
    _prepare_lot(@@lot)

    _cleanup_channels
    assert_equal 0, $folder.spc_channels.size, "channels in #{@@folder} have not been deleted"
    #
    # send data
    $dis = ETSpace::DIS2.new($env, :pg=>@@pg, :product=>@@product, :eqp=>@@eqp, :lot=>@@lot, :nctype=>:nc, :flow=>"FAB8", :mfg_area=>"FAB8", :op=>@@op)
    $dis.add_defaults
    txtime = Time.now
    $dis.remote_write(:filter)
    #
    # verify channel creation
    $log.info "waiting #{@@wait_parser}s for the parser"
    sleep @@wait_parser

    channels = $lds.all_spc_channels("Param_00*")

    # Only first 5 parameters are critical for upload (2 channels for each are created)
    assert_equal 2 * $dis.limit[0].wet_limit_size, channels.size, "channels in #{@@folder_attr}/#{@@folder_vari} have not been created"

    # Check no gatepass or lothold, but lot note
    assert $ptest.verify_lot_state(@@lot, @@op_next), "Wrong lot state"
    assert $ptest.verify_no_lot_note(@@lot, @@op, txtime), "No lot note found"
  end

  def _prepare_lot(lot)
    _linfo = $sv.lot_info(lot)
    assert $sv.lot_opelocate(lot, @@opno_wet, :force=>_linfo.status == "ONHOLD"), "failed to locate to WET operation"
    assert $sv.lot_hold_release(lot, nil), "failed to release lot"
    assert $sv.lot_futurehold_cancel(lot, nil), "failed to cancel future holds"
    cj = $sv.claim_process_lot(lot, :eqp=>@@eqp, :mode=>"Off-Line-1", :notify=>false, :claim_carrier=>true)
    assert cj, "failed to process"
    return cj
  end

  # Channel successfully created - no violation / GatePass
  def test100_single_wafer
    $setup_ok = false
    cj = _prepare_lot(@@lot)
    # delete channels to clean up
    _cleanup_channels
    #
    # send data
    channels, txtime = _create_data_packages(@@nc_type, false, :cj=>cj)
    # Parameters are critical
    assert_equal 2 * $dis.limit[0].wet_limit_size, channels.size, "channels in #{@@folder_attr} and #{@@folder_vari} have not been created"
    # todo: Check samples

    assert $ptest.verify_lot_state(@@lot, @@op_next), "Wrong lot state"
    assert $ptest.verify_no_lot_note(@@lot, @@op, txtime), "Lot should not have note"
    $setup_ok = true
  end

  def test101_single_wafer_oos
    param_channels = $folder_vari.spc_channels
    _assign_verify_cas(param_channels, @@corrective_actions)

    cj = _prepare_lot(@@lot)
    channels, txtime = _create_data_packages(@@nc_type, true, :cj=>cj)

    # Parameter loss
    param_names = $ptest.get_param_names
    assert $ptest.verify_parameter_loss($folder_attr, param_names[0,1], 1.0, $dis.data[0].wet_wafer.wet_site.size)
    assert $ptest.verify_parameter_loss($folder_attr, param_names[1..-1], 0.0, $dis.data[0].wet_wafer.wet_site.size)
  end

  def test102_single_wafer_ooc
    param_channels = $folder_vari.spc_channels
    _assign_verify_cas(param_channels, @@corrective_actions)

    param_channels.each {|c|
      c.set_limits({"MEAN_VALUE_UCL"=>60, "MEAN_VALUE_CENTER"=>1.0, "MEAN_VALUE_LCL"=>0.0})
    }
    cj = _prepare_lot(@@lot)
    channels, txtime = _create_data_packages(@@nc_type, false, :v_1=>72, :cj=>cj)

    # verify comment for last sample
    s = $folder_attr.spc_channel(:name=>"Result_Summary").samples[-1]
    assert $ptest.verify_ecomment_sample(s, "LOT_HELD")
    assert $ptest.verify_lot_state(@@lot, @@opno_dispo, :hold=>true, :reason=>"X-E1"), "Wrong lot state"
  end

  def test103_other_pd
    cj = _prepare_lot(@@lot)
    assert $sv.lot_opelocate(@@lot, @@op_next), "failed to locate"

    # delete channels to clean up
    _cleanup_channels
    #
    # send data
    channels, txtime = _create_data_packages(@@nc_type, false, :cj=>cj)
    # Parameters are critical
    assert_equal 2 * $dis.limit[0].wet_limit_size, channels.size, "channels in #{@@folder_attr} and #{@@folder_vari} have not been created"
    # todo: Check samples

    # Check no gatepass or lothold, but lot note
    assert $ptest.verify_lot_state(@@lot, @@op_next), "Wrong lot state"
    assert $ptest.verify_lot_note(@@lot, @@op, txtime), "No lot note found"
  end

  # Future Hold / missing data
  def test200_single_wafer_future_hold
    $setup_ok = false
    cj = _prepare_lot(@@lot)
    # delete channels to clean up
    _cleanup_channels
    #
    # send data
    channels, txtime = _create_data_packages(@@nc_type, false, :wafers=>@@wafers[0], :cj=>cj)
    # Parameters are critical
    assert_equal 2 * $dis.limit[0].wet_limit_size, channels.size, "channels in #{@@folder_attr} and #{@@folder_vari} have not been created"
    # todo: Check samples

    assert $ptest.verify_lot_state(@@lot, @@op_next), "Wrong lot state"
    assert $ptest.verify_no_lot_note(@@lot, @@op, txtime), "Lot should not have note"
    $setup_ok = true
  end



  def _cleanup_channels
    # delete channels to clean up
    assert $folder_attr.delete_channels, "error deleting channnels in folder #{@@folder_attr}"
    assert $folder_vari.delete_channels, "error deleting channnels in folder #{@@folder_vari}"
  end

  # general method to create mutiple data parameter samples
  # input param with_spec_violation = true if required to provoke spec violation
  def _create_data_packages(nctype, with_spec_violation, params={})
    $log.info "#{__method__} #{nctype.inspect}, #{with_spec_violation}, #{params.inspect}"
    # send data
    count = (params[:count] or 10)
    #
    values = []
    $log.info "********************* create_data_packages:  nctype: #{nctype} *********************************"
    what = (params[:what] or :filter)
    $log.info "what = #{what}"

    _params = {:pg=>@@pg, :product=>@@product, :eqp=>@@eqp, :lot=>@@lot, :wafers=>@@wafers[0], :nctype=>nctype,
      :flow=>"FAB8", :mfg_area=>"FAB8", :op=>@@op}
    _params.merge!(params)

    $ptest.dis = ETSpace::DIS2.new($env, _params)

    $dis = $ptest.dis
    # generate 10 parameter values randomly between clow & chigh
    (1..count).each{|i|
      if params["v_#{i}".to_sym]
        v = params["v_#{i}".to_sym]
      else
        a = [-rand(1e2), rand(1e2)]
        v = a[rand(a.size)]
      end
      values << v
    }
    # provoke spec limit violation for the first parameter if desired.
    $log.info "with_spec_violation = #{with_spec_violation}"
    #
    # assign value with spec violation
    values[0] = 1e21 if with_spec_violation
    #
    trgt = params[:trgt]
    params = {:llow=>-1e2, :lhigh=>1e2,:clow=>-1e2, :chigh=>1e2, :trgt=>trgt}.merge(params)
    $dis.add_values(values, params)
    $dis.limit.pop if params[:missing_limit]
    $dis.data.pop if params[:missing_data]

    txtime = Time.now
    $dis.remote_write(what, params)

    #
    # verify channel creation
    $log.info "waiting #{@@wait_parser}s for the parser"
    sleep @@wait_parser

    return [$lds.all_spc_channels("Param_00*"), txtime]
  end

  def _assign_verify_cas(channels, actions, params={})
    channels.each do |ch|
      $spc.set_corrective_actions(ch, actions, params)
      vals = ch.valuations
      assert vals.size > 0, "no channel valuations defined, check channel and template"
      aa = $spc.corrective_actions(vals[0]).collect {|a| a.name}.sort
      assert_equal actions.sort, aa, "error setting corrective actions"
      ch.set_state "Offline"
    end
    $log.info "#{actions.inspect} set correctly"
  end

  def check_results(check_channel_num, params={})
    # verify channel creation
    excluded_params = (params[:excluded_params] or nil)
    timeout = (params[:timeout] or @@wait_parser )
    #
    $log.info "waiting for the parser to parse all parameters...."
    start_time = Time.now
    if check_channel_num
      while $dis.limit[0].wet_limit_size * 2 > $folder.spc_channels.size && Time.now < start_time + timeout
        sleep 2
      end
    end
    #
    # read limits data out of limit file for comparison
    param_names = $ptest.get_param_names()
    $log.info "1. returned param_names = #{param_names.inspect}"
    return false if param_names == nil
    #
    # check parameters names in space
    param_names.each {|p|
      # get the name of the parameter
      $log.info "1.1 ............param_name: #{p}"
      if $folder.spc_channel(:parameter=>p) == nil
        $log.warn "limit parameter #{p} not populated to space"
        return false
      end
    }
    $log.info "2. ............"
    all_ch = param_names.collect {|p| ch = $folder.spc_channel(:parameter=>p) }
    if all_ch == nil
      $log.info "empty channels list returned"
      return false
    end
    $log.info "3. ............"
    #
    # excluded parameters will not have new samples populated to space
    expect_params_samples = all_ch.collect{|ch|
      if excluded_params != nil
        if excluded_params.member?(ch.parameter)
          s= ch.samples(:days=>30).size
        else
          s= ch.samples(:days=>30).size + 1
        end
      else
        s= ch.samples(:days=>30).size + 1
      end
    }
    $log.info "4. ............"
    #
    if !$ptest.check_channel_samples(s, param_names, :waiting_time=>timeout)
      $log.info "check samples failed"
      return false
    end
    $log.info "5. ............"
    return true
  end

end
