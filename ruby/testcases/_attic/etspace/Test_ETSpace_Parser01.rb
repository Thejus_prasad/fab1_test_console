=begin
Automated testing of the ETSpace parser.
Requirements:

(c) Copyright 2011 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Authors: Adel Darwaish, Steffen Steidten, 2012-08-30
=end

require "RubyTestCase"
require "etspacetest"
require "misc"


class Test_ETSpace_Parser01 < RubyTestCase
  Description = "Test ET Space/SIL"

  @@technology = "QA"
  @@pg = "PGQA2"
  @@op = "FINA-FWET.01"

  @@lot = "QA002.00"

  @@wait_parser = 180

  def setup
    puts "\n"
    $setup_ok = ($setup_ok != false)
    assert $setup_ok, "test setup is not correct"
  end

  def test0000_setup
    $setup_ok = false
    #
    ($spc.close; $ptest = nil) if $spc and $spc.env != $env
    $ptest = ETSpace::TestParser.new($env, :nctype=>:nc) unless $ptest and $ptest.env == $env
    $dis = $ptest.dis
    $spc = $ptest.spc
    $lds = $spc.lds("ET_Fab1")
    refute_nil $lds, "LDS not found"
    @@wait_parser = @@wait_parser.to_i
    @@folder = ["", @@technology, @@pg, @@op].join('/')
    $log.info "using folder #{@@folder}"
    $folder = $lds.folder(@@folder)
    ($log.info "folder #{f} does not exist"; next) unless $folder
    #
    $setup_ok = true
  end

  def test0002_create_delete_channels_single_wafer_nc
    $setup_ok = false
    # delete channels to clean up
    assert $folder.delete_channels, "error deleting channnels in folder #{@@folder}"
    assert_equal 0, $folder.spc_channels.size, "channels in #{@@folder} have not been deleted"
    #
    # send data
    $dis = ETSpace::DIS2.new($env, :pg=>@@pg, :lot=>@@lot, :nctype=>:nc)
    $dis.add_defaults
    $dis.remote_write(:filter)
    #
    # verify channel creation
    $log.info "waiting #{@@wait_parser}s for the parser"
    sleep @@wait_parser
    assert $dis.limit[0].wet_limit_size * 2 <= $folder.spc_channels.size, "channels in #{@@folder} have not been created"
    #
    # delete channels and verify deletion
    assert $folder.delete_channels, "error deleting channnels in folder #{@@folder}"
    assert_equal 0, $folder.spc_channels.size, "channels in #{@@folder} have not been deleted"
    $setup_ok = true
  end

  def test0003_multiple_wafers_nc
    # delete channels to clean up
    assert $folder.delete_channels, "error deleting channnels in folder #{@@folder}"
    #
    # send data
    $dis = ETSpace::DIS2.new($env, :pg=>@@pg, :lot=>@@lot, :wafers=>["#{@@lot}.03", "#{@@lot}.04"], :nctype=>:nc)
    $dis.add_defaults
    $dis.remote_write(:filter)
    #
    # verify channel creation
    $log.info "waiting #{@@wait_parser}s for the parser"
    sleep @@wait_parser
    assert $dis.limit[0].wet_limit_size * 2 <= $folder.spc_channels.size, "channels in #{@@folder} have not been created"
    #
  end

  def test0003_multiple_wafers_mnc
    # delete channels to clean up
    assert $folder.delete_channels, "error deleting channnels in folder #{@@folder}"
    #
    # send data
    $dis = ETSpace::DIS2.new($env, :pg=>@@pg, :lot=>@@lot, :wafers=>[["#{@@lot}.03"], ["#{@@lot}.04"], ["#{@@lot}.15"]], :nctype=>:mnc)
    $dis.add_defaults
    $dis.remote_write(:filter)
    #
    # verify channel creation
    $log.info "waiting #{@@wait_parser}s for the parser"
    sleep @@wait_parser
    assert $dis.limit[0].wet_limit_size * 2 <= $folder.spc_channels.size, "channels in #{@@folder} have not been created"
    #
  end

  def test0004_multiple_wafers_mnc_1_lot
    # delete channels to clean up
    assert $folder.delete_channels, "error deleting channnels in folder #{@@folder}"
    #
    # send data
    $dis = ETSpace::DIS2.new($env, :pg=>@@pg, :lot=>@@lot, :wafers=>[["#{@@lot}.03","#{@@lot}.04","#{@@lot}.15"]], :nctype=>:mnc)
    $dis.add_defaults
    $dis.remote_write(:filter)
    #
    # verify channel creation
    $log.info "waiting #{@@wait_parser}s for the parser"
    sleep @@wait_parser
    assert $dis.limit[0].wet_limit_size * 2 <= $folder.spc_channels.size, "channels in #{@@folder} have not been created"
    #
  end

end
