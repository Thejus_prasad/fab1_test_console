=begin
Automated testing of the ETSpace parser.
Requirements:

(c) Copyright 2011 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Authors: Adel Darwaish, 2012-10-15
=end

require "RubyTestCase"
require "etspacetest"

class Test_ETSpace_MAV_AQL < RubyTestCase
  attr_accessor :curr_sample_no

  include XMLHash

  Description = "Test ET Space/MAV/AQL"

  @@lds = "ET_Fab1"
  @@technology = "QA"
  @@pg = "PGQA1"
  @@op = "FINA-FWET.01"
  @@eqp = "UTF001"
  @@lot = "U282V.00"
  @@opNo = "1000.1000"

  @@corrective_actions = ["AutoLotHold", "LotHold", "ReleaseLotHold","CancelLotHold"]
  @@local_dir = 'tmp/etspace'
  @@wait_parser = 240
  @@refresh_time = 60
  @@site_count = 17
  @@slot_id = 5
  @@prio = 50

  # limits
  @@lcl = -3
  @@center = 0.0
  @@ucl = 3
  @@lsl = -5
  @@usl = 5

  # this is required to write the data package to the local folder and not on the server in case the ETParser is stocking.
  @@local_write = false

  def test100_setup
    $setup_ok = false
    if $env == "f8stag"
      @@lds = "Fab8_ETest"
      @@technology = "QA"
      @@pg = "QAPG1"
      @@op = "TD-WET-OBANPK.01"
      @@product = "SILPROD.01"
      @@opno_wet = "6460.4720"
      @@opno_dispo = "6460.4730"
      @@op_next = "6460.5000" #next operation after DISPO
      @@eqp = "ATC22100"
      @@lot = "8XYK16003.000"
      @@wait_parser = 180
      @@nc_type = :mnc
    end
    @@wafers = $sv.lot_info(@@lot, :wafers=>true).wafers.map {|w| w.wafer}
    ($spc.close; $ptest = nil) if $spc and $spc.env != $env
    @@folder_attr = ["", @@technology, @@pg, @@op, "ATTR"].join('/')
    @@folder_vari = ["", @@technology, @@pg, @@op, "VARI"].join('/')
    $ptest = ETSpace::TestParser.new($env, :lds=>@@lds, :nctype=>@@nc_type, :eqp=>@@eqp, :lot=>@@lot, :folder=>@@folder_attr) unless $ptest and $ptest.env == $env
    $dis = $ptest.dis
    $spc = $ptest.spc
    $lds = $ptest.lds
    refute_nil $lds, "LDS not found"
    $log.info "using folder #{@@folder_attr}"
    $folder = $lds.folder(@@folder_attr)
    ($log.info "folder #{@@folder_attr} does not exist"; next) unless $folder
    #
    @@curr_sample_no = 0
    $setup_ok = true
  end

  def test200_prepare_default_channels
    $setup_ok = false
    sample_count = 1
    # clean up channels
    $ptest.delete_channels
    cj = _prepare_lot(@@lot)
    params = {:pg=>@@pg, :product=>@@product, :eqp=>@@eqp, :lot=>@@lot, :wafers=>@@wafers[0],
      :flow=>"FAB8", :mfg_area=>"FAB8", :op=>@@op,
      :nctype=>:mnc, :cj=>cj, :slot=>@@slot_id, :llow=>@@lsl, :lhigh=>@@usl, :prio=>@@prio}
    #
    assert $ptest.create_verify_data(sample_count, false, params), "failed to create data package"
    # set corrective actions
    $ptest.set_corrective_actions(@@corrective_actions)
    #
    # set control limits and customer filed "Maverick check" for all parameters (channels) of the folder.
    # second param is the desired value for the MEAN_VALUE_UCL limit
    param_names = $ptest.get_param_names
    cs = $folder.spc_channels
    cs.each {|ch|
      next if !param_names.member?(ch.parameter)
      assert ch.set_limits({"MEAN_VALUE_UCL"=>@@ucl, "MEAN_VALUE_CENTER"=>0.0, "MEAN_VALUE_LCL"=>@@lcl, "SIGMA_UCL"=>1.0, "SIGMA_CENTER"=>nil, "SIGMA_LCL"=>nil}), "failed to set limit for channel #{ch.name}"
      # set customer fields
      fields = ch.customer_fields.merge("Maverick check"=>"1;1;15", "Maverick-Action"=>"email+hold")
      assert_equal nil, ch.set_customer_fields(fields), "failed to set customer field 'Maverick check' for channel #{ch.name}"
      assert ch.customer_fields.member?("Maverick check"), "customer field 'Maverick check' for channel #{ch.name} was not set!"
    }
    $setup_ok = true
  end

  # depends on the previous test case 200. The channels must exist as pre-condition
  def test201_check_customer_field_criticality
    criticality = "Critical" if @@prio > 0
    criticality = "Monitoring" if @@prio == 0
    refute_equal 0, $folder.spc_channels.size, "empty channels in folder #{@@folder_attr}"
    $folder.spc_channels.each {|ch|
      assert_equal criticality, ch.customer_fields["Criticality"], "wrong customer field 'criticality' value"
    }
  end

  # (FAB1) < 50% of sites of a param outside of the control limits > UCL & < LCL
  # (FAB8) take AQL factor from default config
  # The channels must exist as pre-condition
  def test202_AQL_violation
    $folder.spc_channels.each {|ch|
      ch.set_customer_fields({"AQL Level"=>".05"})
    }
    # some inits
    sample_count = 10
    n = 1 # parameters fail
    #
    cj = _prepare_lot(@@lot)
    #
    # prepare data values
    site_values = (1..11).collect {|i|
      values = []
      # 2 sites on parameter 0001 should fail
      (1..n).each do |j|
        values << (i.even? ? (@@usl + (2-i)* 0.2) : (@@lsl - (2-i)* 0.2))
      end
      # other sites are in limits
      ((n+1)..10).each do |j|
        values << (i.even? ? (@@usl - i* 0.2 - j*0.2) : (@@lsl + i* 0.2 + j*0.2))
      end
      values
    }
    # restore current sample count for later verification
    ch = $folder.spc_channel(:parameter=>"Param_0001")
    @@curr_sample_no = ch.samples.size unless ch == nil
    #
    # send data package
    params = {:pg=>@@pg, :product=>@@product, :eqp=>@@eqp, :lot=>@@lot, :wafers=>@@wafers[0],
      :flow=>"FAB8", :mfg_area=>"FAB8", :op=>@@op,
      :nctype=>:mnc, :cj=>cj, :slot=>@@slot_id, :site_values=>site_values,
      :llow=>@@lsl, :lhigh=>@@usl, :prio=>@@prio}

    assert $ptest.create_verify_data(sample_count, false, params), "failed to create data package"
    # wait until all expected samples are populated successfully
    param_names = $ptest.get_param_names()
    refute_nil param_names, "failed to get parameter names"
    assert $ptest.check_channel_samples(@@curr_sample_no+1,param_names), "check samples failed"

    return
    #
    # check AQL mean value of param Result_AQL (sample_no = -1 means the last one)
    result = extract_sample_chart_values("Result_AQL", -1)
    assert result[:mean] != 0, "no AQL violation found"
    if ch.customer_fields["Criticality"] != "Critical"
      $log.warn "wrong value of customer field 'Criticality'. Expected Critical, but found #{ch.customer_fields["Criticality"]}"
    end
  end

  # > 100% of sites of the param > UCL
  # The channels must exist as pre-condition
  def test203_MAVERICK_violation_1_sample
    # some inits
    site_values = []
    values = []
    sample_count = 1
    #
    #
    lot_cleanup(true)
    # create cj on the UTF001
    cj = $sv.claim_process_lot(@@lot, :eqp=>@@eqp, :mode=>"Off-Line-1",:proctime=>5)
    refute_nil cj, "could not create cj for lot #{@@lot} on equipment #{@@eqp}"
    #
    # prepare data values
    (1..17).each {|i|
      # 1st param value > ucl (violation on all sites) using 6 sigmas
      # values << @@ucl + (@@ucl - @@lcl)/6
      values << @@ucl + 1.5 if i.even?
      values << @@lcl - 1.5 if i.odd?
      #
      # the rest 9 params are between control limits (no violation on all remain sites)
      (1..9).each {|j|
        (values << @@ucl - i* 0.2 - j*0.2) if i.even?
        (values << @@lcl + i* 0.2 + j*0.2) if i.odd?
        # values << @@ucl + 0.5
      }
      site_values << values
      values = []
    }
    # restore current sample count for later verification
    ch = $folder.spc_channel(:parameter=>"Param_0001")
    @curr_sample_no = ch.samples.size unless ch == nil
    #
    # send data package
    params = {:lot=>@@lot, :nctype=>:mnc, :cj=>cj, :slot=>@@slot_id, :site_values=>site_values, :llow=>@@lsl, :lhigh=>@@usl, :prio=>@@prio, :product=>"SHELBY21AD.B2"}
    assert $ptest.create_verify_data(sample_count, false, params), "failed to create data package"
    # wait until all expected samples are populated successfully
    param_names = $ptest.get_param_names()
    refute_nil param_names, "failed to get parameter names"
    assert $ptest.check_channel_samples(@curr_sample_no+1,param_names), "check samples failed"
    #
    # check MAVERICK mean value of param Result_MAV (sample_no = -1 means the last one)
    result = extract_sample_chart_values("Result_MAV", -1)
    assert result[:mean] != 0, "no MAV violation found"
 end

  # > 100% of sites of the param > UCL
  # The channels must exist as pre-condition
  def test204_MAVERICK_violation_multi_samples
    # some inits
    sample_count = 1
    #
    #
    (1..4).each {|s|
      site_values = []
      values = []
      lot_cleanup(true)
      # create cj on the UTF001
      cj = $sv.claim_process_lot(@@lot, :eqp=>@@eqp, :mode=>"Off-Line-1",:proctime=>5)
      refute_nil cj, "could not create cj for lot #{@@lot} on equipment #{@@eqp}"
      $log.info "*****************************************************************"
      $log.info "*** creating data sample number: #{s.to_s} for lot: #{@@lot} *********"
      $log.info "*****************************************************************"
      #
      # prepare data values
      (1..17).each {|i|
        # 1st param value > @@ucl (violation on all sites)
        values << @@ucl + 1.5 if i.even?
        values << @@lcl - 1.5 if i.odd?
        # the rest 9 params are between control limits (no violation on all remain sites)
        (1..9).each {|j|
          (values << @@ucl - i* 0.2 - j*0.2) if i.even?
          (values << @@lcl + i* 0.2 + j*0.2) if i.odd?
        }
        site_values << values
        values = []
      }
      # restore current sample count for later verification
      ch = $folder.spc_channel(:parameter=>"Param_0001")
      @curr_sample_no = ch.samples.size unless ch == nil
      #
      # send data package
      params = {:lot=>@@lot, :nctype=>:mnc, :cj=>cj, :slot=>@@slot_id, :site_values=>site_values, :llow=>@@lsl, :lhigh=>@@usl, :prio=>@@prio, :product=>"SHELBY21AD.B2"}
      assert $ptest.create_verify_data(sample_count, false, params), "failed to create data package"
      # wait until all expected samples are populated successfully
      param_names = $ptest.get_param_names()
      refute_nil param_names, "failed to get parameter names"
      assert $ptest.check_channel_samples(@curr_sample_no+1,param_names), "check samples failed"
    }
    #
    # check MAVERICK mean value of param Result_MAV (sample_no = -1 means the last one)
    result = extract_sample_chart_values("Result_MAV", -1)
    $log.info "sample_chart_values = #{result.inspect}"
    assert result[:mean] != 0, "no MAV violation found"
  end

  # > 100% of sites of the param > UCL
  # The channels must exist as pre-condition
  def test205_no_MAVERICK_violation_1_sample2
    # some inits
    site_values = []
    values = []
    ucl_sigma = 0.6
    #
    # prepare data values
    (1..17).each {|i|
      # 1st param value > ucl (violation on all sites) using 6 sigmas
      values << @@ucl + 1.5*ucl_sigma if (i.even? && i != 10)
      values << @@lcl - 1.2*ucl_sigma if (i.odd?  && i != 13)
      values << @@ucl + 100*ucl_sigma if i == 10
      values << @@lcl - 100*ucl_sigma if i == 13
      #
      # the rest 9 params are between control limits (no violation on all remain sites)
      (1..9).each {|j|
        (values << @@ucl - i* 0.2 - j*0.2) if i.even?
        (values << @@lcl + i* 0.2 + j*0.2) if i.odd?
      }
      site_values << values
      values = []
    }
    execute_test_package(site_values, :ucl_sigma=>ucl_sigma, :mav_check_config=>"1;1;15", :check_mav_violation=>false)
  end

  def test206_CF_MAV_check_field_equal_auto
    # some inits
    site_values = []
    values = []
    ucl_sigma = 0.5
    mylcl = 3
    mycenter = 4
    myucl = 5
    # mav_check = "1;1;5"
    mav_check = "auto"
    #
    # prepare data values
    (1..17).each {|i|
      # 1st param value > ucl (violation on all sites)
      if(i != 10 && i != 13)
        values << myucl + 101
      else
        (values << myucl - i* 0.2) if i == 10
        (values << mylcl + i* 0.2) if i == 13
      end
      # the rest 9 params are between control limits (no violation on all remain sites)
      (1..9).each {|j|
        (values << myucl - i* 0.2 - j*0.2) if i.even?
        (values << mylcl + i* 0.2 + j*0.2) if i.odd?
      }
      site_values << values
      values = []
    }
    #
    execute_test_package(site_values, :lcl=>mylcl, :center=>mycenter, :ucl=>myucl, :ucl_sigma=>ucl_sigma, :check_mav_violation=>true, :mav_check_config=>mav_check)
  end

def test207_MAVERICK_violation_values2
    # some inits
    site_values = []
    values = []
    ucl_sigma = 0.5
    mylcl = 3
    mycenter = 4
    myucl = 5
    mav_check = "1;1;9"
    # mav_check = "auto"
    #
    # prepare data values
    (1..17).each {|i|
      # 1st param value > ucl (violation on all sites)
      if(i != 10 && i != 13)
        values << myucl + 101
      else
        (values << myucl - i* 0.2) if i == 10
        (values << mylcl + i* 0.2) if i == 13
      end
      # the rest 9 params are between control limits (no violation on all remain sites)
      (1..9).each {|j|
        (values << myucl - i* 0.6 - j*0.6) if i.even?
        (values << mylcl + i* 0.6 + j*0.6) if i.odd?
      }
      site_values << values
      values = []
    }
    #
    execute_test_package(site_values, :lcl=>mylcl, :center=>mycenter, :ucl=>myucl, :ucl_sigma=>ucl_sigma, :check_mav_violation=>true, :mav_check_config=>mav_check)
  end

   # *********** Aux methods *****************************************
  def execute_test_package(site_values,with_spec_violation, params={})
    # some inits
    sample_count = 1
    previous_params_samples = []
    expect_params_samples = []
    #
    $log.info "execute_test_package, #{site_values.size} parameters, params #{params.inspect}"
    return
    # get possible params
    ucl_sigma = (params[:ucl_sigma] or 0.6)
    mav_check_config = (params[:mav_check_config] or "auto")
    check_mav_violation = (params[:check_mav_violation] or false)
    check_aql_violation = (params[:check_aql_violation] or false)
    aql_action = (params[:aql_action] or "email+hold")
    mav_action = (params[:mav_action] or "email+hold")
    lcl = (params[:lcl] or @@lcl)
    ucl = (params[:ucl] or @@ucl)
    center = (params[:center] or @@center)
    prios = (params[:prios] or [])
    excluded_params = (params[:excluded_params] or nil)

    #
    # override control limits
    # param_names = $ptest.get_param_names
    param_names = (1..@@parameter_count).collect {|i| "Param_%4.4d" % i}
    assert param_names.size > 0, "no param names found"
    #
    cs = $folder.spc_channels
    cs.each{|ch|
      next if !param_names.member?(ch.parameter)
      assert ch.set_limits({"MEAN_VALUE_UCL"=>ucl, "MEAN_VALUE_CENTER"=>center, "MEAN_VALUE_LCL"=>lcl, "SIGMA_UCL"=>ucl_sigma, "SIGMA_CENTER"=>nil, "SIGMA_LCL"=>nil}), "failed to set limits for channel #{ch.name}"
      # set customer fields
      fields = ch.customer_fields.merge("Maverick check"=>mav_check_config, "Maverick-Action"=>mav_action, "AQL-Action"=>aql_action)
      assert_equal nil, ch.set_customer_fields(fields), "failed to set customer field 'Maverick check' for channel #{ch.name}"
      assert ch.customer_fields.member?("Maverick check"), "customer field 'Maverick check' for channel #{ch.name} was not set!"
    }
    #
    lot_cleanup(true)
    # create cj on the UTF001
    cj = $sv.claim_process_lot(@@lot, :eqp=>@@eqp, :mode=>"Off-Line-1",:proctime=>5)
    refute_nil cj, "could not create cj for lot #{@@lot} on equipment #{@@eqp}"
    #
    # restore current sample count for later verification
    all_ch = param_names.collect {|p| ch = $folder.spc_channel(:parameter=>p) }
    refute_equal [], all_ch, "empty channels list returned"
    #
    # excluded parameters will not have new samples populated to space
    expect_params_samples = all_ch.collect{|ch|
      if excluded_params != nil
        if excluded_params.member?(ch.parameter)
          s= ch.samples(:days=>30).size
        else
          s= ch.samples(:days=>30).size + 1
        end
      else
        s= ch.samples(:days=>30).size + 1
      end
    }
    #
    # send data package
    params = {:lot=>@@lot, :nctype=>:mnc, :cj=>cj, :slot=>@@slot_id, :site_values=>site_values, :llow=>@@lsl, :lhigh=>@@usl, :prio=>@@prio, :product=>"SHELBY21AD.B2", :what=>:parser, :prios=>prios, :local_write=>@@local_write}
    assert $ptest.create_verify_data(sample_count, with_spec_violation, params), "failed to create data package"
    #
    refute_nil param_names, "failed to get parameter names"
    #
    return true if @@local_write
    #
    # wait until all expected samples are populated successfully
    assert $ptest.check_channel_samples(expect_params_samples, param_names, :waiting_time=>700), "check samples failed"
    #
    # check MAVERICK mean value of param Result_MAV (sample_no = -1 means the last one)
    if check_mav_violation
      result = extract_sample_chart_values("Result_MAV", -1)
      assert result[:mean] != 0, "no MAV violation found"
      # check lot hold
      assert $ptest.check_futurehold(@@lot).size > 0, "no lot hold for lot #{@@lot}"
    end
    #
    if check_aql_violation
      result = extract_sample_chart_values("Result_AQL", -1)
      assert result[:mean] != 0, "no AQL violation found"
    end
  end

  def _prepare_lot(lot)
    assert_equal 0, $sv.lot_cleanup(lot, opNo: @@opno_wet), "failed to cleanup lot"
    cj = $sv.claim_process_lot(lot, :eqp=>@@eqp, :mode=>"Off-Line-1", :notify=>false, :claim_carrier=>true)
    assert cj, "failed to process"
    return cj
  end


  def lot_cleanup(release_holds, params={})
    $log.info "lot_cleanup...@@lot = #{@@lot}"
    li = $sv.lot_info(@@lot)
    if release_holds
      assert $sv.lot_hold_release(@@lot, nil) == 0, "failed to release lot holds"
      assert $ptest.cancel_futrueholds(@@lot) == 0, "failed to cancel future holds" #unless $ptest.check_futurehold(@@lot) == []
    end
    if li.xfer_status == "EI"
      assert $sv.eqp_unload(li.eqp, nil)
      assert $sv.carrier_xfer_status_change(li.carrier, "EO", li.opEqps[0]) == 0, "failed to change xfer status of carrier #{li.carrier} to EO"
    elsif li.xfer_status == "MI"
      assert $sv.carrier_xfer_status_change(li.carrier, "EO", li.opEqps[0]) == 0, "failed to change xfer status of carrier #{li.carrier} to EO"
    end
    if li.xfer_status == "EI"
      assert $sv.eqp_unload(li.eqp, nil)
    end
    force = (params[:force] or false)
    assert $sv.lot_opelocate(@@lot, @@opNo, :force=>force) == 0, "failed to locate lot #{} to op #{@@opNo}"
  end


  def extract_sample_chart_values (param_name, sample_no)
    result = Hash.new
    ch = $folder.spc_channel(:parameter=>param_name)
    s = ch.samples[sample_no]
    # get mean value
    result[:mean] = s.statistics.mean
    #
    # get mean ucl. Other values are similar
    result[:ucl] = s.limits["MEAN_VALUE_UCL"]
    result[:lcl] = s.limits["MEAN_VALUE_LCL"]
    #
    # get USL (TARGET & LSL are similare to get)
    specs = s.specs
    {:usl=>s.specs["USL"]}.merge(result)
    result[:usl] = s.specs["USL"]
    result[:lsl] = s.specs["LSL"]
    return result
  end
end
