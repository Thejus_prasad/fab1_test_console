=begin
Automated testing of the ETSpace parser.
Requirements:

(c) Copyright 2011 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Authors: Adel Darwaish, 2013-3-01
=end

require "RubyTestCase"
require "etspacetest"
require "etspace/etspace_ei"
require "misc"
require 'xmlhash'

class Test_ETSpace_Regression_AQL_MAV < RubyTestCase
  attr_accessor :curr_sample_no

  include XMLHash

  Description = "Test ET Space/MAV/AQL"

  @@technology = "QA"
  @@pg = "QAPG1"
  @@op = "FINA-FWET.01"
  @@eqp = "UTF001"
  @@lot = "U282V.00"
  @@opNo = "1000.1000"

  @@corrective_actions = ["AutoLotHold", "LotHold", "ReleaseLotHold","CancelLotHold"]
  @@local_dir = 'tmp/etspace'
  @@folder = ''
  @@wait_parser = 240
  @@refresh_time = 60
  @@site_count = 17
  @@parameter_count = 3500
  @@curr_sample_no = 0
  @@slot_id = 5
  @@prio = 0

  # limits
  @@lcl = -3
  @@center = 0.0
  @@ucl = 3
  @@lsl = -5
  @@usl = 5

  # this is required to write the data package to the local folder and not on the server in case the ETParser is stocking.
  @@local_write = false

  def setup
    puts "\n"
    $setup_ok = ($setup_ok != false)
    assert $setup_ok, "test setup is not correct"
  end

  def test00_setup
    $setup_ok = false
    #
    ($spc.close; $ptest = nil) if $spc and $spc.env != $env
    @@folder = ["", @@technology, @@pg, @@op].join('/')
    $ptest = ETSpace::TestParser.new($env, :nctype=>:nc, :folder=>@@folder) unless $ptest and $ptest.env == $env
    $dis = $ptest.dis
    $spc = $ptest.spc
    $lds = $spc.lds("ET_Fab1")
    $ei = ETSpace::EI.new($env)
    refute_nil $lds, "LDS not found"
    @@wait_parser = @@wait_parser.to_i
    $log.info "using folder #{@@folder}"
    $folder = $lds.folder(@@folder)
    ($log.info "folder #{f} does not exist"; next) unless $folder
    #
    lot_cleanup(true)
    @@curr_sample_no = 0
    $setup_ok = true
  end

  def test100_prepare_default_channels
    sample_count = 1
    #
    # clean up channels
    $ptest.delete_channels
    #
    lot_cleanup(true)
    # create cj on the UTF001
    cj = $sv.claim_process_lot(@@lot, :eqp=>@@eqp, :mode=>"Off-Line-1",:proctime=>5)
    refute_nil cj, "could not create cj for lot #{@@lot} on equipment #{@@eqp}"
    #
    params = {:lot=>@@lot, :nctype=>:mnc, :cj=>cj, :slot=>@@slot_id, :llow=>@@lsl, :lhigh=>@@usl, :prio=>@@prio, :product=>"SHELBY21AD.B2", :what=>:parser}
    #
    assert $ptest.create_verify_data(sample_count, false, params), "failed to create data package"
    # set corrective actions
    $ptest.set_corrective_actions(@@corrective_actions)
    #
    # set control limits and customer filed "Maverick check" for all parameters (channels) of the folder.
    # second param is the desired value for the MEAN_VALUE_UCL limit
    param_names = $ptest.get_param_names
    cs = $folder.spc_channels
    cs.each {|ch|
      next if !param_names.member?(ch.parameter)
      assert ch.set_limits({"MEAN_VALUE_UCL"=>@@ucl, "MEAN_VALUE_CENTER"=>0.0, "MEAN_VALUE_LCL"=>@@lcl, "SIGMA_UCL"=>1.0, "SIGMA_CENTER"=>nil, "SIGMA_LCL"=>nil}), "failed to set limit for channel #{ch.name}"
      # set customer fields
      fields = ch.customer_fields.merge("Maverick check"=>"1;1;15", "Maverick-Action"=>"email+hold")
      assert_equal nil, ch.set_customer_fields(fields), "failed to set customer field 'Maverick check' for channel #{ch.name}"
      assert ch.customer_fields.member?("Maverick check"), "customer field 'Maverick check' for channel #{ch.name} was not set!"
    }
  end

  # depends on the previous test case 200. The channels must exist as pre-condition
  def test101_check_customer_field_criticality
    criticality = "Critical" if @@prio > 0
    criticality = "Monitoring" if @@prio == 0
    param_names = $ptest.get_param_names
    refute_equal 0, $folder.spc_channels.size, "empty channels in folder #{@@folder}"
    $folder.spc_channels.each {|ch|
      next if !param_names.member?(ch.parameter)
      assert_equal criticality, ch.customer_fields["Criticality"], "wrong customer field 'criticality' value"
    }
  end

  # out of control
  def test211_ETS759_parameter_critical_AQL_OOC
  # some inits
    site_values = []
    values = []
    ucl_sigma = 0.6
    my_prios = []
    #
    # prepare data values
    (0..@@site_count - 1).each {|i|
      (0..@@parameter_count - 1).each {|j|
        if(j == 0)
          (values << @@ucl + i* 0.2 ) if i.even?
          (values << @@lcl - i* 0.2 ) if i.odd?
        else
          (values << @@ucl - i* 0.2 - j*0.2) if i.even?
          (values << @@lcl + i* 0.2 + j*0.2) if i.odd?
        end
      }
      site_values << values
      values = []
    }
    # set criticality and prios
    my_prios << 50
    ch = $folder.spc_channel(:parameter=>"Param_0001")
    fields = ch.customer_fields.merge("Criticality"=>"Critical")
    assert_equal nil, ch.set_customer_fields(fields), "failed to set customer field Criticality for channel parameter Param_0001"
    (1..@@parameter_count - 1).each {|j|
      my_prios << @@prio
    }
    execute_test_package(site_values, false, :ucl_sigma=>ucl_sigma, :mav_check_config=>"1;1;15", :check_mav_violation=>false,:check_aql_violation=>true, :prios=>my_prios)
  end

 # out of spec. Parameter with oos will be execluded by ETParser and is not going to be sent to space.
 # param 1 is OOS with Prio > 0. all values of this parameter are excluded; no upload for this parameter!
  def test212_ETS759_parameter_critical_OOS
  # some inits
    site_values = []
    values = []
    ucl_sigma = 0.6
    my_prios = []
    #
    # prepare data values
    (0..@@site_count - 1).each {|i|
      (0..@@parameter_count - 1).each {|j|
        if(j == 0)
          (values << @@usl + i* 0.2 ) if i.even?
          (values << @@lsl - i* 0.2 ) if i.odd?
        else
          (values << @@usl - i* 0.2 - j*0.2) if i.even?
          (values << @@lsl + i* 0.2 + j*0.2) if i.odd?
        end
      }
      site_values << values
      values = []
    }
    # this parameter will excluded and is not going to be uploaded to space.
    ch = $folder.spc_channel(:parameter=>"Param_0001")
    fields = ch.customer_fields.merge("Criticality"=>"Critical")
    assert_equal nil, ch.set_customer_fields(fields), "failed to set customer field Criticality for channel parameter Param_0001"
    # set criticality and prios
    my_prios << 50
    (1..@@parameter_count - 1).each {|j|
      my_prios << @@prio
    }
    execute_test_package(site_values, false, :ucl_sigma=>ucl_sigma, :mav_check_config=>"1;1;15", :prios=>my_prios, :excluded_params=>[ch.parameter])
  end

  # out of spec
  def test213_ETS759_parameter_critical_MAV
   # some inits
    site_values = []
    values = []
    ucl_sigma = 0.6
    my_prios = []
    site_values = []
    values = []
    ucl_sigma = 0.5
    mylcl = 3
    mycenter = 4
    myucl = 5
    mav_check = "1;1;9"
    #
    # prepare data values
   (0..@@site_count - 1).each {|i|
      (0..@@parameter_count - 1).each {|j|
       if(j == 0)
          if(i != 10 && i != 13)
            values << myucl + 101
          else
            (values << myucl - i* 0.2) if i == 10
            (values << mylcl + i* 0.2) if i == 13
          end
      # the rest 9 params are between control limits (no violation on all remain sites)
      else
        (values << @@ucl - i* 0.2 - j*0.2) if i.even?
        (values << @@lcl + i* 0.2 + j*0.2) if i.odd?
      end
      }
      site_values << values
      values = []
    }
    # set different my_prios
    (0..@@parameter_count - 1).each {|i|
      prio = 50 if (i == 0 || i == 9)
      prio = @@prio if (i != 0 && i != 9)
      my_prios << prio
    }
    $log.info "my_prios = #{my_prios}"

    execute_test_package(site_values, false, :lcl=>mylcl, :center=>mycenter, :ucl=>myucl, :ucl_sigma=>ucl_sigma, :mav_check_config=>mav_check, :check_mav_violation=>true, :prios=>my_prios)
  end

  def test214_ETS761_parameter_critical_MAV_different_prio
   # some inits
    site_values = []
    values = []
    ucl_sigma = 0.6
    my_prios = []
    #
    # prepare data values
    (0..@@site_count - 1).each {|i|
      (0..@@parameter_count - 1).each {|j|
        #
        # param 1, 5 & 10 shall have MAV violation
        if (j == 0 || j == 4 || j == 9)
          # 1st param value > ucl (violation on all sites) using 6 sigmas
          values << @@ucl + 1.5*ucl_sigma if (i.even? && i != 10)
          values << @@lcl - 1.2*ucl_sigma if (i.odd?  && i != 13)
          values << @@ucl + 100*ucl_sigma if i == 10
          values << @@lcl - 100*ucl_sigma if i == 13
        else
          # params are between control limits (no violation on all remain sites)
          (values << @@ucl - i* 0.2 - j*0.2) if i.even?
          (values << @@lcl + i* 0.2 + j*0.2) if i.odd?
        end
      }
      site_values << values
      values = []
    }
    #
    # set different my_prios
    (0..@@parameter_count - 1).each {|i|
      prio = 50 if (i == 0 || i == 9)
      prio = @@prio if (i != 0 && i != 9)
      my_prios << prio
    }
    $log.info "my_prios = #{my_prios}"
    execute_test_package(site_values, false, :ucl_sigma=>ucl_sigma, :mav_check_config=>"1;1;15", :check_mav_violation=>true, :check_aql_violation=>true, :prios=>my_prios)
  end

  # IQR
  def test215_ETS866_values_eliminated_due_to_out_of_error_sanity_limits
   # some inits
    site_values = []
    values = []
    ucl_sigma = 0.6
    my_prios = []
    #
    # prepare data values
    (0..@@site_count - 1).each {|i|
      (0..@@parameter_count - 1).each {|j|
        #
        # param 1, 5 & 10 shall have IQR values "far a way from the spec limits".
        if (j == 0 || j == 4 || j == 9)
          if (i == 0 || i == 1 || i == 4 || i == 10 || i == 12)
            values << @@ucl + 100*@@usl  if i == 0
            values << @@lcl - 100*@@lsl  if i == 1
          else
            (values << @@ucl - i* 0.2 - j*0.2) if i.even?
            (values << @@lcl + i* 0.2 + j*0.2) if i.odd?
          end
        else
          # rest params are between control limits (no violation on all remain sites)
          (values << @@ucl - i* 0.2 - j*0.2) if i.even?
          (values << @@lcl + i* 0.2 + j*0.2) if i.odd?
        end
      }
      site_values << values
      values = []
    }
    #
    # set different my_prios
    (0..@@parameter_count - 1).each {|i|
      my_prios[i] = 50 if (i == 0 || i == 9)
      my_prios[i] = @@prio if (i != 0 && i != 9)
    }
    $log.info "my_prios = #{my_prios}"
    execute_test_package(site_values, false, :ucl_sigma=>ucl_sigma, :mav_check_config=>"1;1;15", :prios=>my_prios)
  end

 def test216_CF_MAV_check_field_equal_auto
    # some inits
    site_values = []
    values = []
    ucl_sigma = 0.5
    mylcl = 3
    mycenter = 4
    myucl = 5
    mav_check = "auto"
    #
    # prepare data values
    (0..@@site_count - 1).each {|i|
      (0..@@parameter_count - 1).each {|j|
        if (j == 0)
          if(i != 10 && i != 13)
            values << myucl + 101
          else
            (values << myucl - i* 0.2) if i == 10
            (values << mylcl + i* 0.2) if i == 13
          end
        else
          (values << myucl - i* 0.2 - j*0.2) if i.even?
          (values << mylcl + i* 0.2 + j*0.2) if i.odd?
        end
      }
      site_values << values
      values = []
    }
    #
    execute_test_package(site_values,false, :lcl=>mylcl, :center=>mycenter, :ucl=>myucl, :ucl_sigma=>ucl_sigma, :check_mav_violation=>true, :mav_check_config=>mav_check)
  end

  def test217_CF_MAV_check_field_equal_auto_parser2
    # some inits
    site_values = []
    values = []
    ucl_sigma = 0.5
    mylcl = 3
    mycenter = 4
    myucl = 5
    mav_check = "auto"
    #
    # prepare data values
    (0..@@site_count - 1).each {|i|
      (0..@@parameter_count - 1).each {|j|
        if (j == 0)
          if(i != 10 && i != 13)
            values << myucl + 101
          else
            (values << myucl - i* 0.2) if i == 10
            (values << mylcl + i* 0.2) if i == 13
          end
        else
          (values << myucl - i* 0.2 - j*0.2) if i.even?
          (values << mylcl + i* 0.2 + j*0.2) if i.odd?
        end
      }
      site_values << values
      values = []
    }
    #
    params = {:lcl=>mylcl, :center=>mycenter, :ucl=>myucl, :ucl_sigma=>ucl_sigma, :check_mav_violation=>true, :mav_check_config=>mav_check, :what=>:parser2}
    execute_test_package(site_values, false, params)
  end

  #
  # *********** Aux methods *****************************************
  def execute_test_package(site_values,with_spec_violation, params={})
    # some inits
    sample_count = 1
    previous_params_samples = []
    expect_params_samples = []
    #
    $log.info "method execute_test_package: params: #{params.inspect}"
    # get possible params
    ucl_sigma = (params[:ucl_sigma] or 0.6)
    mav_check_config = (params[:mav_check_config] or "auto")
    check_mav_violation = (params[:check_mav_violation] or false)
    check_aql_violation = (params[:check_aql_violation] or false)
    aql_action = (params[:aql_action] or "email+hold")
    mav_action = (params[:mav_action] or "email+hold")
    lcl = (params[:lcl] or @@lcl)
    ucl = (params[:ucl] or @@ucl)
    center = (params[:center] or @@center)
    prios = (params[:prios] or [])
    excluded_params = (params[:excluded_params] or nil)
    #
    # override control limits
    # param_names = $ptest.get_param_names
    param_names = (1..@@parameter_count).collect {|i| "Param_%4.4d" % i}
    assert param_names.size > 0, "no param names found"
    #
    cs = $folder.spc_channels
    cs.each{|ch|
      next if !param_names.member?(ch.parameter)
      assert ch.set_limits({"MEAN_VALUE_UCL"=>ucl, "MEAN_VALUE_CENTER"=>center, "MEAN_VALUE_LCL"=>lcl, "SIGMA_UCL"=>ucl_sigma, "SIGMA_CENTER"=>nil, "SIGMA_LCL"=>nil}), "failed to set limits for channel #{ch.name}"
      # set customer fields
      fields = ch.customer_fields.merge("Maverick check"=>mav_check_config, "Maverick-Action"=>mav_action, "AQL-Action"=>aql_action)
      assert_equal nil, ch.set_customer_fields(fields), "failed to set customer field 'Maverick check' for channel #{ch.name}"
      assert ch.customer_fields.member?("Maverick check"), "customer field 'Maverick check' for channel #{ch.name} was not set!"
    }
    #
    lot_cleanup(true)
    # create cj on the UTF001
    cj = $sv.claim_process_lot(@@lot, :eqp=>@@eqp, :mode=>"Off-Line-1",:proctime=>5)
    refute_nil cj, "could not create cj for lot #{@@lot} on equipment #{@@eqp}"
    #
    # restore current sample count for later verification
    all_ch = param_names.collect {|p| ch = $folder.spc_channel(:parameter=>p) }
    refute_equal [], all_ch, "empty channels list returned"
    #
    # excluded parameters will not have new samples populated to space
    expect_params_samples = all_ch.collect{|ch|
      if excluded_params != nil
        if excluded_params.member?(ch.parameter)
          s= ch.samples(:days=>30).size
        else
          s= ch.samples(:days=>30).size + 1
        end
      else
        s= ch.samples(:days=>30).size + 1
      end
    }
    #
    # send data package
    params = {:lot=>@@lot, :nctype=>:mnc, :cj=>cj, :slot=>@@slot_id, :site_values=>site_values, :llow=>@@lsl, :lhigh=>@@usl, :prio=>@@prio, :product=>"SHELBY21AD.B2", :prios=>prios, :local_write=>@@local_write}.merge(params)
    assert $ptest.create_verify_data(sample_count, with_spec_violation, params), "failed to create data package"
    #
    refute_nil param_names, "failed to get parameter names"
    #
    return true if @@local_write
    # return
    #
    # wait until all expected samples are populated successfully
    assert $ptest.check_channel_samples(expect_params_samples, param_names, :waiting_time=>700), "check samples failed"
    #
    # check MAVERICK mean value of param Result_MAV (sample_no = -1 means the last one)
    if check_mav_violation
      result = extract_sample_chart_values("Result_MAV", -1)
      assert result[:mean] != 0, "no MAV violation found"
      # check lot hold
      assert $ptest.check_futurehold(@@lot).size > 0, "no lot hold for lot #{@@lot}"
    end
    #
    if check_aql_violation
      result = extract_sample_chart_values("Result_AQL", -1)
      assert result[:mean] != 0, "no AQL violation found"
    end
  end

  def lot_cleanup(release_holds, params={})
    $log.info "lot_cleanup...@@lot = #{@@lot}"
    li = $sv.lot_info(@@lot)
    if release_holds
      assert $sv.lot_hold_release(@@lot, nil) == 0, "failed to release lot holds"
      assert $ptest.cancel_futrueholds(@@lot) == 0, "failed to cancel future holds" #unless $ptest.check_futurehold(@@lot) == []
    end
    if li.xfer_status == "EI"
      assert $sv.eqp_unload(li.eqp, nil)
      assert $sv.carrier_xfer_status_change(li.carrier, "EO", li.opEqps[0]) == 0, "failed to change xfer status of carrier #{li.carrier} to EO"
    elsif li.xfer_status == "MI"
      assert $sv.carrier_xfer_status_change(li.carrier, "EO", li.opEqps[0]) == 0, "failed to change xfer status of carrier #{li.carrier} to EO"
    end
    if li.xfer_status == "EI"
      assert $sv.eqp_unload(li.eqp, nil)
    end
    force = (params[:force] or false)
    assert $sv.lot_opelocate(@@lot, @@opNo, :force=>force) == 0, "failed to locate lot #{} to op #{@@opNo}"
  end

  def extract_sample_chart_values (param_name, sample_no)
    result = Hash.new
    ch = $folder.spc_channel(:parameter=>param_name)
    s = ch.samples[sample_no]
    # get mean value
    result[:mean] = s.statistics.mean
    #
    # get mean ucl. Other values are similar
    result[:ucl] = s.limits["MEAN_VALUE_UCL"]
    result[:lcl] = s.limits["MEAN_VALUE_LCL"]
    #
    # get USL (TARGET & LSL are similare to get)
    specs = s.specs
    {:usl=>s.specs["USL"]}.merge(result)
    result[:usl] = s.specs["USL"]
    result[:lsl] = s.specs["LSL"]
    return result
  end
end
