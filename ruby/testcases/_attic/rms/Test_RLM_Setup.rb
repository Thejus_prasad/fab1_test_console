=begin
 automated test of Camline RLM

(c) Copyright 2015 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

 author      - conny.stenzel@globalfoundries.com
 date        - 2015/11/04
=end

require 'RubyTestCase'
require 'rmstest'
require 'rmsdb'

# RLM tests
class Test_RLM_Setup < RubyTestCase
  Description = 'Test RecipeLifecycleManager'

  @@eqp = 'ALC400'
  @@department = 'Training'
  @@rlm_wq_flags_and_states = Array.new
  @@rlm_wq_flags_and_states[0] = {mark_archive: 0, mark_delete: 0, state: 'SUCCESS', tostate: 'ACTIVE'}
  @@rlm_wq_flags_and_states[1] = {mark_archive: 0, mark_delete: 0, state: 'SUCCESS', tostate: 'INACTIVE'}
  @@rlm_wq_flags_and_states[2] = {mark_archive: 0, mark_delete: 1, state: 'SUCCESS', tostate: 'INACTIVE'}
  @@rlm_wq_flags_and_states[3] = {mark_archive: 1, mark_delete: 1, state: 'SUCCESS', tostate: 'INACTIVE'}
  @@rlm_archive_path = '/amddata/rms/rlm/LET/MSS/RECIPE/ROB/'
  
  # config/preparation task
  def self.startup
    $rmstest = RmsTest.new($env, @@eqp, @@department) unless $rmstest && $rmstest.env == $env
    $ril = $rmstest.ril
    $rms = $rmstest.rms
    $rms.login
    #$rmsserver = $rmstest.rmsserver
    
    $rmsdb ||= RMS::DB.new($env)
  end

  def calculate_intervalls(interval, intervalbase, params={}) 
    #$log.info "#{__method__}"
    intervals = Array.new
    if interval.include?('/')
      time = interval.split('/').last.to_i
      intervalbase.times {|i| 
        time_candidate = time * (i+1)
        intervals << time_candidate if time_candidate <= intervalbase
      }
    elsif interval.include?('*')
      intervalbase.times {|i| intervals << i}
    else
      intervals << interval.to_i
    end
    return intervals
  end
  
  def next_purging()
    purger_wait = 129600 #$rmstest.rmsserver.systemprops['rlm.purger.min.time.inactive']
    purger_hour = 9 #$rmstest.rmsserver.systemprops['rlm.purger.timer.hour']
    purger_minute = 12 #$rmstest.rmsserver.systemprops['rlm.purger.timer.minute']
    purger_second = 30 #$rmstest.rmsserver.systemprops['rlm.purger.timer.second']
    $log.info "purger_wait: #{purger_wait}, purger_hour: #{purger_hour}, purger_minute: #{purger_minute}, purger_second: #{purger_second}"
    
    purger_hours = calculate_intervalls(purger_hour, 24, params={}) 
    purger_minutes = calculate_intervalls(purger_minute, 60, params={}) 
    purger_seconds = calculate_intervalls(purger_second, 60, params={})
    $log.info "Hours: #{purger_hours}, Minutes: #{purger_minutes}, Seconds: #{purger_seconds}"
    
    purging_times = Array.new
    purger_hours.each {|ph| 
      purger_minutes.each {|pm|
        purger_seconds.each {|ps|
          pm = 00 if pm == 60 
          purging_times << '%02i' % ph + ':' + '%02i' % pm + ':' + '%02i' % ps
        }
      }
    }
    $log.debug "Purging times: #{purging_times.inspect}"
    
    purging_timers = Array.new
    time = Time.now
    wait_until = time + purger_wait.to_i * 60
    purging_times.each {|pt|
      if Time.parse(pt) < wait_until
        purging_timers << Time.parse(pt) if Time.parse(pt) > Time.now
        next
      else
        wait_until = Time.parse(pt)
        break
      end
    }
    $log.info "Next Purge at: #{wait_until}"
    return wait_until, purging_timers
  end
end
