=begin
 automated test of setup.FC API 

(c) Copyright 2011 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

 author      - daniel.steger@globalfoundries.com
 date        - 2013/07/19
 
 preparation: login to f36asd27 as cei
              cd qa/junit_testing 
              ./bin/start_eirqserver_itdc.sh (start the ALC2460 EI with mappings to TRK2460 STP2460. 
              Single start with: start_ei ALC2460 TRK2460 STP2460 on f36asd27 (as cei) telnet 22224.
=end

require_relative 'Test_RMS_Setup'
require 'misc'

# RMS Recipe management tests - Autolinking recipes
class Test_RMS_Autolinks < Test_RMS_Setup
  @@stplink = { 'ProcessDefinition' => '$', 'ToolPrefix' => 'ALC', 'Equipment' => '$', 'ProductID_EC' => '$', 'Chamber' => 'STP', 'RecipeType' => 'ScannerMain', 'ToolVendor' => 'ASML', 'MainProcessDefinition' => '$', 'PhotoLayer' => '$' }

  @@upload_tool = 'TRK2460'
  
  def test0801_autolinking_no_value  
    robdef = $rms.find_object_with_data_exp({ 'Department' => 'MSS', 'ToolPrefix' => 'ALC', 'ToolVendor' => 'SOKUDO', 'state' => 'ACTIVE', 'SoftRev' => 'TC01-1' }, registration: 'RCP_DEF_PROD')
    rob = $rms.trigger_recipe_upload(@@upload_tool, robdef, 'WaferFlow\\PRD\\OBAN\\PUVII950----001')
    $rms.get_data(rob)
    #
    assert_equal robdef.linked_contexts, rob.linked_contexts, 'links do not match' 
  end
  
  def test0802_autolinking_multiple_values
    robdef = $rms.find_object_with_data_exp({'Department'=>'MSS', 'ToolPrefix'=>'ALC', 'ToolVendor'=>'SOKUDO', 'state'=>'ACTIVE', 'SoftRev'=>'TC01-2'}, registration: 'RCP_DEF_PROD')
    rob = $rms.trigger_recipe_upload(@@upload_tool, robdef, 'WaferFlow\\PRD\\OBAN\\PUVII950----001')
    $rms.get_data(rob)
    # find pre-existing link
    assert robdef.linked_contexts.inject(true) {|res, link|
      l = rob.linked_contexts.find {|rlink| rlink == link}
      $log.warn "  link does not match: #{link.inspect}" unless l
      res && !!l
      # res & verify_condition('link does not match') { rob.linked_contexts.find { |rlink| rlink == link } }
    }, ' failed to verify pre-existing links'
    # find generated links
    assert rob.dataitems['RecipeValue'].value.inject(true) { |res, v|
      if v == '' # skip empty values
        res
      else
        links = rob.linked_contexts.select { |c| c['ToolRecipeName'] == v }
        res & verify_equal(1, links.count, "expected one link for '#{v}', but found #{links.inspect}")
      end
    }, 'failed to verify links'
  end

  def test0803_autolinking_one_value
    robdef = $rms.find_object_with_data_exp({'Department'=>'MSS', 'ToolPrefix'=>'ALC', 'ToolVendor'=>'SOKUDO', 'state'=>'ACTIVE', 'SoftRev'=>'TC01-3'}, registration: 'RCP_DEF_PROD')
    rob = $rms.trigger_recipe_upload(@@upload_tool, robdef, 'WaferFlow\\PRD\\OBAN\\PUVII950----001')
    $rms.get_data(rob)
    # find pre-existing link
    assert robdef.linked_contexts.inject(true) {|res, link|
      l = rob.linked_contexts.find {|rlink| rlink == link}
      $log.warn "  link does not match: #{link.inspect}" unless l
      res && !!l
      # res & verify_condition('link does not match') { rob.linked_contexts.find { |rlink| rlink == link } }
    }, ' failed to verify pre-existing links'
    # find generated link
    v = rob.dataitems['RecipeName'].value
    links = rob.linked_contexts.select { |c| c['ToolRecipeName'] == v }
    assert_equal 1, links.count, "expected one link for '#{v}', but found #{links.inspect}"
  end
  
  def test0804_autolinking_precedence
    robdef = $rms.find_object_with_data_exp({'Department'=>'MSS', 'ToolPrefix'=>'ALC', 'ToolVendor'=>'SOKUDO', 'state'=>'ACTIVE', 'SoftRev'=>'TC01-4'}, registration: 'RCP_DEF_PROD')
    rob = $rms.trigger_recipe_upload(@@upload_tool, robdef, 'WaferFlow\\PRD\\OBAN\\PUVII950----001')
    $rms.get_data(rob)
    # find pre-existing link
    assert robdef.linked_contexts.inject(true) {|res, link|
      l = rob.linked_contexts.find {|rlink| rlink == link}
      $log.warn "  link does not match: #{link.inspect}" unless l
      res && !!l
      # res & verify_condition('link does not match') { rob.linked_contexts.find { |rlink| rlink == link } }
    }, ' failed to verify pre-existing links'
    # find generated link
    v = rob.dataitems['RecipeName'].value
    links = rob.linked_contexts.select { |c| c['ToolRecipeName'] == v }
    assert_equal 1, links.count, "expected one link for '#{v}', but found #{links.inspect}"
  end

  def test0805_new_autolink_1_n
    $robdef = robdef = $rms.find_object_with_data_exp({'Department'=>'MSS', 'ToolPrefix'=>'ALC', 'ToolVendor'=>'SOKUDO', 'state'=>'ACTIVE', 'SoftRev'=>'TC01-5'}, registration: 'RCP_DEF_PROD')
    $rob = rob = $rms.trigger_recipe_upload(@@upload_tool, robdef, 'WaferFlow\\PRD\\OBAN\\PUVII950----001')
    $rms.get_data(rob)
    assert $rmstest.verify_existing_links(robdef, rob), ' failed to verify pre-existing links'
    # find generated link
    assert $rmstest.verify_many_links(rob, 'RecipeValue', 'ToolRecipeName'), 'failed to verify links'
    count = rob.dataitems['RecipeValue'].value.reject { |p| p == '' }.count
    assert $rmstest.verify_many_links(rob, 'RecipeName', 'RecipeName', count), 'failed to verify links'
  end
  
  def test0806_new_autolink_combined_1_n
    robdef, rob = _prepare('ToolRecipeName' => 'C:\\QATEST\\#RecipeName#\\$RecipeValue$', 'Equipment' => '$')

    assert $rmstest.verify_existing_links(robdef, rob), ' failed to verify pre-existing links'
    # find generated link
    assert $rmstest.verify_many_links(rob, 'RecipeValue', 'ToolRecipeName'), 'failed to verify links'
    count = rob.dataitems['RecipeValue'].value.reject { |p| p == '' }.count
    assert $rmstest.verify_many_links(rob, 'RecipeName', 'ToolRecipeName', count), 'failed to verify links'

    # do a second upload
    rob = $rms.trigger_recipe_upload(@@upload_tool, robdef, 'WaferFlow\\PRD\\OBAN\\PUVII950----001')
    $rms.get_data(rob)
    # no change to links
    assert $rmstest.verify_existing_links(robdef, rob), ' failed to verify pre-existing links'
    # find generated link
    assert $rmstest.verify_many_links(rob, 'RecipeValue', 'ToolRecipeName'), 'failed to verify links'
    count = rob.dataitems['RecipeValue'].value.reject { |p| p == '' }.count
    assert $rmstest.verify_many_links(rob, 'RecipeName', 'ToolRecipeName', count), 'failed to verify links'

    # do a third upload, remove one template link
    links = rob.linked_contexts[0..-1]
    rob = $rms.put_data(rob, {}, linked_contexts: links)
    # no change to links
    assert $rmstest.verify_existing_links(robdef, rob), ' failed to verify pre-existing links'
    # find generated link
    assert $rmstest.verify_many_links(rob, 'RecipeValue', 'ToolRecipeName'), 'failed to verify links'
    count = rob.dataitems['RecipeValue'].value.reject { |p| p == '' }.count
    assert $rmstest.verify_many_links(rob, 'RecipeName', 'ToolRecipeName', count), 'failed to verify links'
  end
  
  def test0807_new_autolink_not_found    
    e = assert_raises RmsAPI::RmsError do
      robdef, rob = _prepare('ToolRecipeName' => '$NotFound$', 'Equipment' => '$')
      $rmstest.verify_existing_links(robdef, rob)
    end
    assert_equal 'RIL-AUTOLINK_ERROR: Errors during AutoLink: Reason:Following parameter placeholders do not have values:[NotFound]', e.message
  end
  
  def test0808_new_autolink_combined_n_m
    robdef, rob = _prepare('ToolRecipeName' => '$RecipeValue$-$Chambers(Top/Bottom)$', 'Equipment' => '$')

    assert $rmstest.verify_existing_links(robdef, rob), ' failed to verify pre-existing links'
    # find generated link
    assert $rmstest.verify_many_links(rob, 'RecipeValue', 'ToolRecipeName'), 'failed to verify links'
    assert $rmstest.verify_many_links(rob, 'Chambers(Top/Bottom)', 'ToolRecipeName'), 'failed to verify links'  
  end

  def test0809_new_autolink_empty_parameter_value
    e = assert_raises RmsAPI::RmsError do 
      robdef = $rms.find_object_with_data_exp({'Department'=>'MSS', 'ToolPrefix'=>'ALC', 'ToolVendor'=>'SOKUDO', 'state'=>'ACTIVE', 'SoftRev'=>'TC01-7'}, registration: 'RCP_DEF_PROD')

      rob = $rms.trigger_recipe_upload(@@upload_tool, robdef, 'WaferFlow\\PRD\\OBAN\\PUVII950----001')
      $rms.get_data(rob)

      assert $rmstest.verify_existing_links(robdef, rob, tmpl_links: true), ' failed to verify pre-existing links'
    end
    assert_equal 'RIL-AUTOLINK_ERROR: Errors during AutoLink: Reason:Following parameter placeholders do not have values:[NoValueLink]', e.message
  end
  
  def test0811_new_autolink_multiple_links
    robdef, rob = _prepare([{ 'ToolRecipeName' => '-$RecipeValue$-', 'Equipment' => '$' },
                            { 'ToolRecipeName' => '_$Chambers(Top/Bottom)$_', 'Equipment' => '$' }])

    assert $rmstest.verify_existing_links(robdef, rob), ' failed to verify pre-existing links'
    # find generated link
    assert $rmstest.verify_many_links(rob, 'RecipeValue', 'ToolRecipeName'), 'failed to verify links'
    count = rob.dataitems['RecipeValue'].value.reject { |p| p == '' }.count
    assert $rmstest.verify_many_links(rob, 'Chambers(Top/Bottom)', 'ToolRecipeName'), 'failed to verify links'
  end
  
  # create new robdef version and upload
  def _prepare(links)
    robdef = $rms.find_object_with_data_exp({'Department'=>'MSS', 'ToolPrefix'=>'ALC', 'ToolVendor'=>'SOKUDO', 'state'=>'ACTIVE', 'SoftRev'=>'TC01-6'}, registration: 'RCP_DEF_PROD')
    # modify robdef
    stplinks = [@@stplink]
    if links.is_a?(Array)
      stplinks += links
    else
      stplinks << links
    end
    $robdef = robdef = $rms.create_work_copy(robdef, {}, {}, linked_contexts: stplinks, ignorelinks: true, checklinks: true)
    assert $rmstest.recipe_approval(robdef), 'failed to modify robdef'

    $rob = rob = $rms.trigger_recipe_upload(@@upload_tool, robdef, 'WaferFlow\\PRD\\OBAN\\PUVII950----001')
    $rms.get_data(rob)
    [robdef, rob]
  end
  
end