=begin
 automated test of setup.FC API 

(c) Copyright 2011 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

 author      - daniel.steger@globalfoundries.com
 date        - 2013/09/06
=end

require "Test_RMS_Setup"


# RMS Recipe management tests
class Test_RMS_Compare < Test_RMS_Setup
  Description = "Reparse bodies to test ROSI parsing"
  
  def test1201_parse_varian
    # find ROBDEF
    tooltype/vendor/softrev
    
    recipes = trigger_recipe_list(eqp, robdef)
    
    Dir["/testcasedata/recipes/#{eqp}/*"].each do |body|
      body = File.open("testcasedata/recipes/#{@@bodies[r]}") { |io| io.read }
      $rms.create_recipe
    
      #$rms.get_data ?
      # check the UnknownCCode
      # validate recipe
    end
  end
  
  
end