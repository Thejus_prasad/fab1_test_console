=begin
 automated test of setup.FC API 

(c) Copyright 2011 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

 author      - daniel.steger@globalfoundries.com
 date        - 2012/08/28
=end

require_relative "Test_RMS_Setup"
require "misc"

# RMS Recipe management tests
class Test_RMS_Restore < Test_RMS_Setup
  Description = "Restore ROBs and ROBDEFs"

  def setup
    super
    $setup_ok = ($setup_ok != false)
    assert $setup_ok, "test setup is not correct"
  end

  # config/preparation task
  def test000_setup
    $setup_ok = false
    $rmstest ||= RmsTest.new($env, @@eqp)
    $ril = $rmstest.ril
    $rms = $rmstest.rms
    $setup_ok = true
  end

  def test01_restore_robdefs
      $rmstest.restore_robdefs
  end

  def test02_restore_robs
      $rmstest.restore_robs
  end
end
