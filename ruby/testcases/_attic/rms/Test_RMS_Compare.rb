=begin
(c) Copyright 2011 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: Daniel Steger, 2012-08-28

History:
  2016-01-26 sfrieske,  added load test case
=end

require_relative 'Test_RMS_Setup'


# Test RIL-EI Compare Interface
class Test_RMS_Compare < Test_RMS_Setup
  @@bodies = ["IMP2100/P-BF-07T007-5E15-00Q", "IMP2100/P-BF-07T007-5E15-00Q-A", "IMP2100/P-BF-07T007-5E15-00Q-B", "IMP2100/P-BF-07T007-5E15-00Q-D"]
  @@department = 'MSS' #MSS
  
  def _generate_compare_recipe(eqp)
    time = Time.now.to_i    
    cmp_recipes = {}    
    ctx = { "RecipeType"=>"Main", "Equipment"=>eqp, "RecipeNameSpaceID"=>"QA-TEST" }
    cmp_recipes = {
      "IMP2100/P-BF-07T007-5E15-00Q" => RmsAPI::RobObject.new( nil,
        ctx.merge({ "ToolRecipeName"=>"#{time}\\P-BF-07T007-5E15-00Q", "RecipeName"=>"IMP-COMPARE-01-#{Time.now.to_i}"}),
        { "Dose" => 7.5E14, "Dose_ci"=>[0.0, 8E15], "Energy" => 7.0, "Species"=>"P"},
        nil),
      "IMP2100/P-BF-07T007-5E15-00Q-C1" => RmsAPI::RobObject.new( nil,
        ctx.merge({ "ToolRecipeName"=>"#{time}\\P-BF-07T007-5E15-00Q", "RecipeName"=>"IMP-COMPARE-02-#{Time.now.to_i}"}),
        { "Dose" => 7.5E14, "Dose_ci"=>[0.0, 8E15], "Energy" => 7.0, "Species"=>"P"},
        nil),
      "IMP2100/P-BF-07T007-5E15-00Q-C2" => RmsAPI::RobObject.new( nil,
        ctx.merge({ "ToolRecipeName"=>"#{time}\\P-BF-07T007-5E15-00Q", "RecipeName"=>"IMP-COMPARE-03-#{Time.now.to_i}"}),
        { "Dose" => 7.5E14, "Dose_ci"=>[0.0, 8E15], "Energy" => 7.0, "Species"=>"P"},
        nil),
    }
    return cmp_recipes
  end
  
  def _compare_setup
    # check if recipes with context exist
    robs = $rms.find_object_list( {"RecipeNameSpaceID"=>"QA-TEST", "Department"=>@@department, "ToolPrefix"=>"IMP-HC", "ToolVendor"=>"VARIAN", "SoftRev"=>"TC11-1", "state"=>"ACTIVE"})
    return robs if robs.count == 3
    assert($rms.deactivate(robs), "failed to remove old setup") unless robs.count == 0
    
    recipe = self._generate_compare_recipe("IMP2100")
    robs = $rmstest.recipe_create(recipe, :tool_prefix=>"IMP-HC", :tool_vendor=>"VARIAN", :softrev=>"TC11-1")
    robs = $rmstest.recipe_approval(robs)
    assert robs, "Recipes not created successfully"    
    return robs
  end
  
  def _run_single_compare_scenario(exp_res, compare_info, onhold=false)
    rob = self._compare_setup.first
    rob = $rms.create_work_copy(rob, {}, {})
    rob = $rms.put_data(rob, {"ProductionCompareInfo"=>compare_info})
    rob = $rmstest.recipe_approval(rob)
    if onhold
      rob = $rms.set_onhold(rob, ["SPC_PROBLEM"])[0]
    else
      rob = $rms.set_onhold(rob, [])[0]
    end
    assert rob, "failed to approve"    
    exp_res.each_with_index do |exp,i|      
      body = File.open("testcasedata/recipes/#{@@bodies[i]}") { |io| io.read }        
      res = $ril.compare_recipes({rob.rmshandle=>Base64.encode64(body)})
      $log.info "exp: #{exp}"
      assert_equal exp, res[:compareRecipesResponse]["totalComparisonResult"], "failed to compare the recipe, #{res.inspect}"
      assert_equal exp, $rms.upload_compare(rob.rmshandle, body).to_s, "failed to compare the recipe, #{res.inspect} in memory"
    end
  end
  
  def _run_multi_compare_scenario(rob_indices, exp_res, compare_info)
    robs = self._compare_setup
    robs.map! {|rob|
      rob = $rms.create_work_copy(rob, {}, {})
      $rms.put_data(rob, {"ProductionCompareInfo"=>compare_info})
    }
    robs = $rmstest.recipe_approval(robs)
    assert robs, "failed to approve"
    rcp = Hash[rob_indices.map.each_with_index {|r,i|
      body = File.open("testcasedata/recipes/#{@@bodies[r]}") { |io| io.read }
      [robs[i].rmshandle, Base64.encode64(body)]
    }]
    res = $ril.compare_recipes(rcp)
    assert_equal exp_res, res[:compareRecipesResponse]["totalComparisonResult"], "failed to compare the recipe, #{res.inspect}"    
  end  
  
  def test1101_compare_simple
    self._run_single_compare_scenario(["true", "false", "false", "false"], {"Body."=>true, "Charge"=>true, "Dose"=>true, "Energy"=>true, "Species"=>true, "UseAngleCorrection"=>true})
  end 
  
  def test1102_compare_simple
    self._run_single_compare_scenario(["true", "true", "true", "false"], {"Body."=>false, "Charge"=>false, "Dose"=>false, "Energy"=>false, "Species"=>false, "UseAngleCorrection"=>false})
  end
  
  def test1103_compare_simple
    self._run_single_compare_scenario(["true", "false", "false", "false"], {"Body"=>false, "Body."=>true, "Charge"=>false, "Dose"=>true, "Energy"=>true, "Species"=>true, "UseAngleCorrection"=>false})
  end
  
  def test1104_compare_simple
    self._run_single_compare_scenario(["true", "true", "true", "false"], {"Body"=>false, "Body."=>false, "Charge"=>false, "Dose"=>true, "Energy"=>true, "Species"=>true, "UseAngleCorrection"=>false})
  end
  
  def test1105_compare_simple
    self._run_single_compare_scenario(["true", "false", "false"], {"Body"=>true})
  end
  
  def test1106_compare_simple_onhold
    self._run_single_compare_scenario(["true", "false", "false"], {"Body"=>true}, true)
  end
  
  def test1110_compare_multi    
    self._run_multi_compare_scenario([0,1,2], "false", {"Body."=>true, "Charge"=>true, "Dose"=>true, "Energy"=>true, "Species"=>true, "UseAngleCorrection"=>true})
  end
  
  def test1111_compare_multi    
    self._run_multi_compare_scenario([0,1,2], "true", {"Body."=>false, "Charge"=>false, "Dose"=>false, "Energy"=>false, "Species"=>false, "UseAngleCorrection"=>false})
  end
  
  def test1112_compare_multi    
    self._run_multi_compare_scenario([0,1,2], "true", {"Body."=>false, "Charge"=>false, "Dose"=>true, "Energy"=>true, "Species"=>true, "UseAngleCorrection"=>false})
  end
  
  def test1113_compare_multi    
    self._run_multi_compare_scenario([0,1,2], "false", {"Body."=>false, "Charge"=>false, "Dose"=>false, "Energy"=>false, "Species"=>false, "UseAngleCorrection"=>true})
  end
  
  def test1114_compare_multi    
    self._run_multi_compare_scenario([0,1,2], "false", {"Body."=>false, "Charge"=>true, "Dose"=>false, "Energy"=>false, "Species"=>false, "UseAngleCorrection"=>false})
  end
  
  def test1115_compare_multi    
    self._run_multi_compare_scenario([0,1,3], "false", {"Body."=>false, "Charge"=>false, "Dose"=>true, "Energy"=>true, "Species"=>true, "UseAngleCorrection"=>false})
  end
  
  def test1120_compare_different_version
    robs = $rms.find_object_list( {"RecipeNameSpaceID"=>"QA-TEST", "Department"=>@@department, "ToolPrefix"=>"IMP-HC", "ToolVendor"=>"VARIAN", "SoftRev"=>"TC11-2", "state"=>"ACTIVE"})
    assert_equal 2, robs.count, "missing setup. expected 2 robs"
    robs.each {|rob|
      body = $rms.get_data(rob).dataitems["Body"]
      res = $ril.compare_recipes({rob.rmshandle=>Base64.encode64(body)})
      assert_equal "true", res[:compareRecipesResponse]["totalComparisonResult"], "failed to compare the recipe, #{res.inspect}"
      assert_equal true, $rms.upload_compare(rob.rmshandle, body), "failed to compare the recipe, #{res.inspect} in memory"
    }
    
  end

  # load test

  def testman1190_load
    loops = 10000
    loops.times {|loop|
      $log.info "\n-- loop #{loop+1}/#{loops}"
      _run_single_compare_scenario(['true', 'true', 'true', 'false'], 
        {'Body.'=>false, 'Charge'=>false, 'Dose'=>false, 'Energy'=>false, 'Species'=>false, 'UseAngleCorrection'=>false})
    }
  end

end
