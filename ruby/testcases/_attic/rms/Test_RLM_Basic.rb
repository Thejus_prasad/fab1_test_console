=begin
 automated test of Camline RLM

(c) Copyright 2015 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

 author      - conny.stenzel@globalfoundries.com
 date        - 2015/11/04
=end

require 'RubyTestCase'
require 'rmstest'
require 'rmsdb'
require 'misc'
require 'remote'

# RMS Recipe management tests
class Test_RLM_Basic < RubyTestCase
  Description = 'Test RecipeLifecycleManager'

  @@eqp = 'ALC400'
  @@department = 'MSS' #'Training'
  @@rlm_wq_flags_and_states = Array.new
  @@rlm_wq_flags_and_states[0] = {mark_archive: 0, mark_delete: 0, state: 'SUCCESS', tostate: 'ACTIVE'}
  @@rlm_wq_flags_and_states[1] = {mark_archive: 0, mark_delete: 0, state: 'SUCCESS', tostate: 'INACTIVE'}
  @@rlm_wq_flags_and_states[2] = {mark_archive: 0, mark_delete: 1, state: 'SUCCESS', tostate: 'INACTIVE'}
  @@rlm_wq_flags_and_states[3] = {mark_archive: 1, mark_delete: 1, state: 'SUCCESS', tostate: 'INACTIVE'}
  @@rlm_archive_path = "/amddata/rms/rlm/#{$env}/#{@@department}/RECIPE/ROB/"

  # config/preparation task
  def test000_setup
    $setup_ok = false
    $rmstest = RmsTest.new($env, @@eqp, @@department) unless $rmstest && $rmstest.env == $env
    $ril = $rmstest.ril
    $rms = $rmstest.rms
    $rms.login
    $rmsdb.db.close if $rmsdb
    $rmsdb = nil
    $rmsdb = RMS::DB.new($env)
    $rmsserver = RemoteHost.new "rms.#{$env}"
    $setup_ok = true
  end
  
  def test001
    _rob_purging
  end
  
  def testman002_load_test
    runs = 200
    runs.times {_rob_purging}
  end
  
  def _rob_purging
    timeout = 28800
    purger_successor_versions = 5 #'rlm.purger.min.number.version'
    time = Time.now.to_i
    rlm_recipe = nil
    
    rms_recipe = {
      'IMP2100/P-BF-07T007-5E15-00Q' => RmsAPI::RobObject.new( nil,
        { 'ToolRecipeName'=>"#{time}\\P-BF-07T007-5E15-00Q", 'RecipeType'=>'Main', 'RecipeNameSpaceID'=>'QA-TEST', 'RecipeName'=>"IMP-HC-RECIPE-#{time}"},
        { 'Dose' => 7.5E14, 'Energy' => 7.0, 'Species'=>'8', 'QAParamInteger' => 7, 'QAParamBool'=>false, 'QAParamTime'=>Time.now },
        nil),
    }
    rob = $rmstest.recipe_create(rms_recipe, :tool_prefix=>'IMP-HC', :tool_vendor=>'VARIAN', :softrev=>'TC02-1')
    
    assert_equal '', $rms.get_keys(rob[0]).context['builtin']['approval_cycle_name'], 'no approval cycle should be set'
    rob = $rmstest.recipe_approval(rob).first
    assert rob, 'Recipes not created successfully'
    
    #rob = $rmstest.recipe_release(rob)
    rmshandle = rob.rmshandle.split('2c').first
    first_version_handle = rmshandle
    
    assert_equal 'true', $rms.get_keys(rob).context['builtin']['last_in_branch'], "wrong value for last_in_branch #{rmshandle}"
    assert_match(/AC_ROB_[0-9]+_[0-9]+/, $rms.get_keys(rob).context['builtin']['approval_cycle_name'], 'no approval cycle should be set')
    
    #wait for an ACTIVE archived ROB zip file was written
    assert wait_for(:timeout=>timeout) {
      $rmsserver.exec!("test -f #{@@rlm_archive_path}#{$env}_#{first_version_handle}_ACTIVE.zip && echo OK")
    }, "Archive #{@@rlm_archive_path}#{$env}_#{first_version_handle}_ACTIVE.zip not found within #{timeout}s, please check by hand"
    
    robs = [rob]
    rob_new = nil
    purger_successor_versions.times {|i|
      rob_new = $rmstest.recipe_update(robs[i])
      robs << rob_new
      #wait for an ACTIVE archived ROB zip file was written
      assert wait_for(:timeout=>timeout) {
        $rmsserver.exec!("test -f #{@@rlm_archive_path}#{$env}_#{rob_new.rmshandle.split('2c').first}_ACTIVE.zip && echo OK")
      }, "Archive #{@@rlm_archive_path}#{$env}_#{rob_new.rmshandle.split('2c').first}_ACTIVE.zip not found within #{timeout}s, please check by hand"
      assert rob_new, 'Recipes not created successfully'
      assert_equal 'false', $rms.get_keys(robs[i]).context['builtin']['last_in_branch'], "wrong value for last_in_branch in #{robs[i].rmshandle}"
      assert_equal 'true', $rms.get_keys(rob_new).context['builtin']['last_in_branch'], "wrong value for last_in_branch #{rob_new.rmshandle}"
      $log.info "New ROB created by recipe update: #{rob_new.rmshandle}"
    }
    
    $log.info "..waiting (max. #{timeout}s) for database entries"
    assert wait_for(:timeout=>timeout) {
      rlm_recipe = $rmsdb.rlm_recipe(robhandle: rmshandle)
      rlm_recipe.size > 0
    }, "No database entry found for #{rmshandle} within #{timeout}s, please check by hand"
    
    rob_del = $rms.deactivate(rob_new).first
    assert_equal 'true', $rms.get_keys(rob_del).context['builtin']['last_in_branch'], "wrong value for last_in_branch #{rob_del.rmshandle}"
    assert_match(/AC_ROB_[0-9]+_[0-9]+/, $rms.get_keys(rob_del).context['builtin']['approval_cycle_name'], 'no approval cycle should be set')
    $log.info "Deleted/Deactivated ROB: #{rob_new.rmshandle}"
    
    rmshandle = rob_del.rmshandle.split('2c').first
    purgings = $rmstest.next_purging
    verify_next_no_purge_at = purgings[1][0]
    $log.info "verify_next_no_purge_at: #{verify_next_no_purge_at}"
    wait_for_no_purge = verify_next_no_purge_at - Time.now
    
    assert wait_for(:timeout=>timeout) {
      $rmsserver.exec!("test -f #{@@rlm_archive_path}#{$env}_#{first_version_handle}_INACTIVE.zip && echo OK")
    }, "Archive #{@@rlm_archive_path}#{$env}_#{first_version_handle}_INACTIVE.zip not found within #{timeout}s, please check by hand"
    
    next_purging_at = purgings[0]
    $log.info "next_purging_at: #{next_purging_at}"
    wait_for_purge = (next_purging_at - Time.now) - wait_for_no_purge
    
    $log.info "..sleeping #{wait_for_no_purge} for next purger run without purging"
    sleep wait_for_no_purge
    assert wait_for(:timeout=>timeout) {
      rlm_recipe = $rmsdb.rlm_recipe(robhandle: rmshandle)
      rlm_recipe.first[:state] == 'INACTIVE'
    }, "Wrong state (#{rlm_recipe.first[:state]}) in table T_RLM_RECIPE for handle #{rmshandle}, expected: INACTIVE"
    assert rlm_recipe.size == 1, "Too less/much (#{rlm_recipe.size}) recipe entries: #{rlm_recipe.inspect} in table T_RLM_RECIPE for handle #{rmshandle}, expected: 1"
    assert rlm_recipe.first[:version] == "1.#{purger_successor_versions.to_i + 1}", "Wrong version (#{rlm_recipe.first[:version]}) in table T_RLM_RECIPE for handle #{rmshandle}, expected: 1.#{purger_successor_versions.to_i + 1}"
    
    $log.info "..sleeping #{wait_for_purge} for next purging of rob: #{first_version_handle}"
    sleep wait_for_purge
    rlm_recipe = $rmsdb.rlm_recipe(robhandle: rmshandle)
    assert rlm_recipe.size == 1, "Too less/much (#{rlm_recipe.size}) recipe entries: #{rlm_recipe.inspect} in table T_RLM_RECIPE for handle #{first_version_handle}, expected: 1"
    assert rlm_recipe.first[:state] == 'INACTIVE', "Wrong state (#{rlm_recipe.first[:state]}) in table T_RLM_RECIPE for handle #{first_version_handle}, expected: INACTIVE"
    assert rlm_recipe.first[:version] == "1.#{purger_successor_versions.to_i + 1}", "Wrong version (#{rlm_recipe.first[:version]}) in table T_RLM_RECIPE for handle #{first_version_handle}, expected: 1.#{purger_successor_versions.to_i + 1}"
    rlm_workqueue = $rmsdb.rlm_workqueue(robhandle: first_version_handle)
    assert rlm_workqueue.size >= 2, "Too less/much (#{rlm_workqueue.size}) recipe entries: #{rlm_workqueue.inspect} in table T_RLM_WORKQUEUE for handle #{first_version_handle}, expected: 2"
    rlm_workqueue.size.times {|i|
      assert rlm_workqueue[i][:mark_archive] == @@rlm_wq_flags_and_states[i][:mark_archive], "Wrong flag (#{rlm_workqueue[i][:mark_archive]}) in table T_RLM_WORKQUEUE for handle #{first_version_handle}, expected: #{@@rlm_wq_flags_and_states[i][:mark_archive]}"
      assert rlm_workqueue[i][:mark_delete] == @@rlm_wq_flags_and_states[i][:mark_delete], "Wrong flag (#{rlm_workqueue[i][:mark_delete]}) in table T_RLM_WORKQUEUE for handle #{first_version_handle}, expected: #{@@rlm_wq_flags_and_states[i][:mark_delete]}"
      assert rlm_workqueue[i][:state] == @@rlm_wq_flags_and_states[i][:state], "Wrong state (#{rlm_workqueue[i][:state]}) in table T_RLM_WORKQUEUE for handle #{first_version_handle}, expected: #{@@rlm_wq_flags_and_states[i][:state]}"
      assert rlm_workqueue[i][:tostate] == @@rlm_wq_flags_and_states[i][:tostate], "Wrong tostate (#{rlm_workqueue[i][:tostate]}) in table T_RLM_WORKQUEUE for handle #{first_version_handle}, expected: #{@@rlm_wq_flags_and_states[i][:tostate]}"
    }
    rlm_recipe = $rmsdb.rlm_recipe(robhandle: first_version_handle)
    assert rlm_recipe.size == 1, "Too less/much (#{rlm_recipe.size}) recipe entries: #{rlm_recipe.inspect} in table T_RLM_RECIPE for handle #{first_version_handle}, expected: 1"
    # wait a certain amount of time to let RLM do the purging, because RLM could be busy with purging of other ROBs
    $log.info "..waiting (max. #{timeout}s) for database entries"
    assert wait_for(:timeout=>timeout) {
      rlm_recipe = $rmsdb.rlm_recipe(robhandle: first_version_handle)
      rlm_recipe.first[:state] == 'MARKED_FOR_DELETE'
    }, "No database entry found for #{first_version_handle} within #{timeout}s, please check by hand"
    
    assert rlm_recipe.first[:state] == 'MARKED_FOR_DELETE', "Wrong state (#{rlm_recipe.first[:state]}) in table T_RLM_RECIPE for handle #{first_version_handle}, expected: MARKED_FOR_DE#{$env}E"
    assert rlm_recipe.first[:version] == "1.#{purger_successor_versions.to_i - 4}", "Wrong version (#{rlm_recipe.first[:version]}) in table T_RLM_RECIPE for handle #{first_version_handle}, expected: 1.#{purger_successor_versions.to_i + 1}"
    rlm_workqueue = $rmsdb.rlm_workqueue(robhandle: first_version_handle)
    assert rlm_workqueue.size == 4, "Too less/much (#{rlm_workqueue.size}) recipe entries: #{rlm_workqueue.inspect} in table T_RLM_WORKQUEUE for handle #{first_version_handle}, expected: 3"
    rlm_workqueue.size.times {|i|
      assert rlm_workqueue[i][:mark_archive] == @@rlm_wq_flags_and_states[i][:mark_archive], "Wrong flag (#{rlm_workqueue[i][:mark_archive]}) in table T_RLM_WORKQUEUE for handle #{first_version_handle}, expected: #{@@rlm_wq_flags_and_states[i][:mark_archive]}"
      assert rlm_workqueue[i][:mark_delete] == @@rlm_wq_flags_and_states[i][:mark_delete], "Wrong flag (#{rlm_workqueue[i][:mark_delete]}) in table T_RLM_WORKQUEUE for handle #{first_version_handle}, expected: #{@@rlm_wq_flags_and_states[i][:mark_delete]}"
      assert rlm_workqueue[i][:state] == @@rlm_wq_flags_and_states[i][:state], "Wrong state (#{rlm_workqueue[i][:state]}) in table T_RLM_WORKQUEUE for handle #{first_version_handle}, expected: #{@@rlm_wq_flags_and_states[i][:state]}"
      assert rlm_workqueue[i][:tostate] == @@rlm_wq_flags_and_states[i][:tostate], "Wrong tostate (#{rlm_workqueue[i][:tostate]}) in table T_RLM_WORKQUEUE for handle #{first_version_handle}, expected: #{@@rlm_wq_flags_and_states[i][:tostate]}"
    }
    
    assert wait_for(:timeout=>timeout) {
      $rmsserver.exec!("test -f #{@@rlm_archive_path}#{$env}_#{first_version_handle}_MARKED_FOR_ARCHIVE.zip && echo OK")
    }, "Archive #{@@rlm_archive_path}#{$env}_#{first_version_handle}_MARKED_FOR_ARCHIVE.zip not found within #{timeout}s, please check by hand"
    $log.info "rlm_recipe: #{rlm_recipe.inspect}"
    $log.info "rlm_workqueue: #{rlm_workqueue.inspect}"
  end
  
  def test003_robdef_synchronising
    robdefs = []
    file = 'testcasedata/robdefs/TC01-6-ALC SOKUDO WaferFlow - TC01-4 Link generation multiple values1.xml'
    $log.info "importing #{file}..."
    data = File.open(file, 'r') {|fh| fh.read}
    robdefs = $rms.import(data)
    robdef = robdefs[0]
    
    purger_successor_versions = 5 #'rlm.purger.min.number.version'
    
    assert $rmstest.recipe_approval(robdef), 'failed to modify robdef'
    
    robdefs = [robdef]
    robdef_new = nil
    purger_successor_versions.times {|i|
      robdef_new = $rmstest.recipe_update(robdefs[i])
      robdefs << robdef_new
      assert robdef_new, 'Recipes not created successfully'
      
      assert_equal 'false', $rms.get_keys(robdefs[i]).context['builtin']['last_in_branch'], "wrong value for last_in_branch in #{robdefs[i].rmshandle}"
      assert_equal 'true', $rms.get_keys(robdef_new).context['builtin']['last_in_branch'], "wrong value for last_in_branch #{robdef_new.rmshandle}"
    }
    
    $log.info "Robdef: #{robdef_new.inspect}"
    
    robdef_del = $rms.deactivate(robdef_new).first
    
    rmshandle = robdef_del.rmshandle.split('2c').first
    $log.info '..waiting (max. 3600s) for database entries'
    wait_for(:timeout=>3600) {
      rlm_recipe = $rmsdb.rlm_recipe(robhandle: rmshandle)
      rlm_recipe.size > 0
    }
    sleep 10
    rlm_recipe = $rmsdb.rlm_recipe(robhandle: rmshandle)
    rlm_workqueue = $rmsdb.rlm_workqueue(robhandle: rmshandle)
    assert rlm_recipe.size == 1, "Too less/much (#{rlm_recipe.size}) recipe entries in table T_RLM_RECIPE for handle #{rmshandle}, expected: 1"
    assert rlm_recipe.first[:state] == 'INACTIVE', "Wrong state (#{rlm_recipe.first[:state]}) in table T_RLM_RECIPE for handle #{rmshandle}, expected: INACTIVE"
    rlm_workqueue = $rmsdb.rlm_workqueue(robhandle: rmshandle)
    assert rlm_workqueue.size == 1, "Too less/much (#{rlm_workqueue.size}) recipe entries in table T_RLM_WORKQUEUE for handle #{rmshandle}, expected: 1"
    assert rlm_workqueue.first[:mark_archive] == 0, "Wrong flag (#{rlm_workqueue.first[:mark_archive]}) in table T_RLM_WORKQUEUE for handle #{rmshandle}, expected: 0"
    assert rlm_workqueue.first[:mark_delete] == 0, "Wrong flag (#{rlm_workqueue.first[:mark_delete]}) in table T_RLM_WORKQUEUE for handle #{rmshandle}, expected: 0"
    assert rlm_workqueue.first[:state] == 'SUCCESS', "Wrong state (#{rlm_workqueue.first[:state]}) in table T_RLM_WORKQUEUE for handle #{rmshandle}, expected: SUCCESS"
    assert rlm_workqueue.first[:tostate] == 'INACTIVE', "Wrong tostate (#{rlm_workqueue.first[:tostate]}) in table T_RLM_WORKQUEUE for handle #{rmshandle}, expected: ACTIVE"
    $log.info "rlm_recipe: #{rlm_recipe.inspect}"
    $log.info "rlm_workqueue: #{rlm_workqueue.inspect}"
  end
end
