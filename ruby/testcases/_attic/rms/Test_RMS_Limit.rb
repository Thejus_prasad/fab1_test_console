=begin
 automated test of setup.FC API 

(c) Copyright 2011 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

 author      - daniel.steger@globalfoundries.com
 date        - 2013/10/15
 history
    2017-01-25 dsteger  added fallback ei parameter name config
=end

require_relative "Test_RMS_Setup"
require "misc"

class Test_RMS_Limit < Test_RMS_Setup
  
  @@context = {"RecipeNameSpaceID"=>"QA-TEST", "RecipeName"=>"IMP-HC-LIMITS"}
  @@einame_fallback = false # configuration in RIL
  
  def test500_create_recipe_with_limits
    $setup_ok = false
    #
    imp_recipe = {
      "IMP2100/P-BF-07T007-5E15-00Q" => RmsAPI::RobObject.new( nil,
        @@context.merge({ "ToolRecipeName"=>"IMP-HC-LIMITS\\P-BF-07T007-5E15-00Q", "RecipeType"=>"Main"}),
        { "DoseTrim_ci"=>[-1,1], "QAIntLimit"=>77, "QAStringLimit"=>"TEST1" },
        nil),
    }
    robs = $rms.find_object_list( imp_recipe["IMP2100/P-BF-07T007-5E15-00Q"].context, :with_keys=>true)
     
    unless robs.count == 1    
      robs = $rmstest.recipe_create(imp_recipe, {"SoftRev"=>"TC05-1", "ToolVendor"=>"VARIAN", "ToolPrefix"=>"IMP-HC"})
      assert_equal true, $rms.validate(robs)[0][1], "rob should be valid #{robs[0].rmshandle}"
      robs = $rmstest.recipe_approval(robs)
    end
    rob = robs.first
        
    rob = $rms.create_work_copy(rob, {},{})
    
    # missing value
    res = _check_valid_rob(rob.rmshandle, "QAIntLimit", 78)    
    res &= _check_valid_rob(rob.rmshandle, "QAStringLimit", "TEST2")
    assert res, "failed validation with missing value"
    
    # outside limit
    res &= _check_valid_rob(rob.rmshandle, "QAIntLimit", 77, -34)
    res &= _check_valid_rob(rob.rmshandle, "QAIntLimit", 77, Integer)
    res &= _check_valid_rob(rob.rmshandle, "QAStringLimit", "TEST2", "TEST4")    
    res &= _check_valid_rob(rob.rmshandle, "QAStringLimit", "TEST2", String)    
    assert res, "failed validation out side limits"
    
    # override limits
    res = _check_valid_rob(rob.rmshandle, "QAIntLimit_ci", [1,99], [-1,1])
    res = _check_valid_rob(rob.rmshandle, "QAStringLimit_ci", Set.new(["TEST1", "TEST2"]))
    assert res, "failed validation limit override"
    
    assert $rmstest.recipe_approval(rob), "failed to approve recipe"

    $setup_ok = true
  end
  
  def test510_new_robdef_version
    robdef = $rms.find_object_with_data_exp({"ToolPrefix"=>"IMP-HC", "ToolVendor"=>"VARIAN", "SoftRev"=>"TC05-1"}, registration: 'RCP_DEF_PROD')
    assert robdef, "robdef not found to run test"
   
    robdef = $rms.create_work_copy(robdef, {},{})
    $rms.put_data(robdef, {"QAIntLimit"=>Integer})
    verify_equal false, $rms.validate(robdef)[0][1], "  robdef should be valid"
    assert $rms.delete_work(robdef), "failed to delete ROBDEF"    
  end
  
  def test520_runtime_getpayload
    rcp = $ril.get_recipe_payload_request(@@context)
    assert $rmstest.verify_rms_recipe(rcp), "recipe check failed"
    assert $rmstest.check_recipe_parameters(rcp, einame: @@einame_fallback), "parameter check failed"
  end
  
  def _check_valid_rob(rmshandle, item, value, new_value=nil)
    $log.info "checking #{rmshandle}, #{item}"
    rob = RmsAPI::RobObject.new(rmshandle)
    if new_value
      rob = $rms.put_data(rob,{item=>new_value})
    else
      rob = $rms.del_data(rob,[item])
    end
    res = verify_equal false, $rms.validate(rob)[0][1], "  rob should be invalid"
    
    rob = RmsAPI::RobObject.new(rmshandle)
    rob = $rms.put_data(rob,{item=>value}) 
    res &= verify_equal true, $rms.validate(rob)[0][1], "  rob should be valid"
    return res
  end
end