=begin
 automated test of RMS

(c) Copyright 2011 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

 author      - daniel.steger@globalfoundries.com
 date        - 2013/07/19
=end

require_relative "Test_RMS_Setup"
require "misc"

# RMS Recipe management tests - Context propagation recipes
# TODO: Add recipe generation here
class Test_RMS_SP < Test_RMS_Setup
  # Global config for shared parameters
  SP_Parameter_Strict = true
  
  @@context = { "RecipeNameSpaceID"=>"QA-TEST", "RecipeName"=>"BSBEOLCA" }
  
  
  def _generate_sp_recipes()      
    robs = []
    # all overridden
    robs << $rmstest.shared_parameter_set_create({"RecipeParameter1"=>"NONE", "RecipeParameter2"=>0, "RecipeParameter2_ci"=>[0,10], 
        "RecipeParameter3"=>0.0, "RecipeParameter3_ci"=>[-1e-1, 2e3], 
        "RecipeParameter4"=>false, "RecipeParameter5"=>Time.at(18), 
        "__options__RecipeParameter1"=>"Module=PM1;Steps=1,1,1", "__options__RecipeParameter2"=>"Module=PM2;Steps=2,2,2",
        "__options__RecipeParameter3"=>"Module=PM3;Steps=3,3,3", "__options__RecipeParameter4"=>"Module=PM4;Steps=4,4,4", 
        "__options__RecipeParameter5"=>"Module=PM5;Steps=5,5,5"
      },
      {"BST2100" => {"RecipeParameter1"=>"BST2100", "RecipeParameter2"=>2, "RecipeParameter3"=>2.1, 
          "RecipeParameter4"=>true, "RecipeParameter5"=>Time.at(18)},
       "BST2600" => {"RecipeParameter1"=>"BST2600", "RecipeParameter2"=>4, "RecipeParameter3"=>4.1, 
          "RecipeParameter4"=>false, "RecipeParameter5"=>Time.at(18)}}, 
      :recipename_space=>"QA-SP1", :softrev=>"TC10-1") unless self._sp_exists?("QA-SP1")
    
    # partial override
    robs << $rmstest.shared_parameter_set_create({"RecipeParameter1"=>"NONE", "RecipeParameter2"=>0, "RecipeParameter2_ci"=>[0,10], 
        "RecipeParameter3"=>0.0, "RecipeParameter3_ci"=>[-1e-1, 2e3], 
        "RecipeParameter4"=>false, "RecipeParameter5"=>Time.at(18), 
        "__options__RecipeParameter1"=>"Module=PM1;Steps=1,1,1", "__options__RecipeParameter2"=>"Module=PM2;Steps=2,2,2",
        "__options__RecipeParameter3"=>"Module=PM3;Steps=3,3,3", "__options__RecipeParameter4"=>"Module=PM4;Steps=4,4,4", 
        "__options__RecipeParameter5"=>"Module=PM5;Steps=5,5,5"
      },
      {"BST2100" => {"RecipeParameter3"=>6.1, 
          "RecipeParameter4"=>true, "RecipeParameter5"=>Time.at(18)},
       "BST2600" => {"RecipeParameter1"=>"BST2600", "RecipeParameter2"=>4}}, 
      :recipename_space=>"QA-SP2", :softrev=>"TC10-1") unless self._sp_exists?("QA-SP2")
    
    # additional parameters
    robs << $rmstest.shared_parameter_set_create({"RecipeParameter6"=>"NONE", "RecipeParameter7"=>0, "RecipeParameter7_ci"=>[0,10], 
        "RecipeParameter8"=>0.0, "RecipeParameter8_ci"=>[-1e-1, 2e3], 
        "RecipeParameter9"=>false, "RecipeParameter10"=>Time.at(18), 
        "__options__RecipeParameter6"=>"Module=PM1;Steps=1,1,1", "__options__RecipeParameter7"=>"Module=PM2;Steps=2,2,2",
        "__options__RecipeParameter8"=>"Module=PM3;Steps=3,3,3", "__options__RecipeParameter9"=>"Module=PM4;Steps=4,4,4", 
        "__options__RecipeParameter10"=>"Module=PM5;Steps=5,5,5"
      },
      {"BST2100" => {"RecipeParameter6"=>"BST2100", "RecipeParameter7"=>2, "RecipeParameter8"=>2.1, 
          "RecipeParameter9"=>true, "RecipeParameter10"=>Time.at(18)},
       "BST2601" => {"RecipeParameter6"=>"BST2601", "RecipeParameter7"=>4, "RecipeParameter8"=>4.1, 
          "RecipeParameter9"=>false, "RecipeParameter10"=>Time.at(18)}}, 
      :recipename_space=>"QA-SP3", :softrev=>"TC10-2") unless self._sp_exists?("QA-SP3")
    
    # override the strict attribute false
    robs << $rmstest.shared_parameter_set_create({"RecipeParameter1"=>"NONE", "RecipeParameter2"=>0, "RecipeParameter2_ci"=>[0,10], 
        "RecipeParameter3"=>0.0, "RecipeParameter3_ci"=>[-1e-1, 2e3], 
        "RecipeParameter4"=>false, "RecipeParameter5"=>Time.at(18), 
        "__options__RecipeParameter1"=>"Module=PM1;Steps=1,1,1", "__options__RecipeParameter2"=>"Module=PM2;Steps=2,2,2",
        "__options__RecipeParameter3"=>"Module=PM3;Steps=3,3,3", "__options__RecipeParameter4"=>"Module=PM4;Steps=4,4,4", 
        "__options__RecipeParameter5"=>"Module=PM5;Steps=5,5,5"
      },
      {"BST2100" => {"RecipeParameter1"=>"BST2100", "RecipeParameter2"=>2, "RecipeParameter3"=>2.1, 
          "RecipeParameter4"=>true, "RecipeParameter5"=>Time.at(18)},
       "BST2600" => {"RecipeParameter1"=>"BST2600", "RecipeParameter2"=>4, "RecipeParameter3"=>4.1, 
          "RecipeParameter4"=>false, "RecipeParameter5"=>Time.at(18)}}, 
      :recipename_space=>"QA-SP5", :softrev=>"TC10-4") unless self._sp_exists?("QA-SP5")
    
    robs << $rmstest.shared_parameter_set_create({"RecipeParameter1"=>"NONE", "RecipeParameter2"=>0, "RecipeParameter2_ci"=>[0,10], 
        "RecipeParameter3"=>0.0, "RecipeParameter3_ci"=>[-1e-1, 2e3], 
        "RecipeParameter4"=>false, "RecipeParameter5"=>Time.at(18), 
        "__options__RecipeParameter1"=>"Module=PM1;Steps=1,1,1", "__options__RecipeParameter2"=>"Module=PM2;Steps=2,2,2",
        "__options__RecipeParameter3"=>"Module=PM3;Steps=3,3,3", "__options__RecipeParameter4"=>"Module=PM4;Steps=4,4,4", 
        "__options__RecipeParameter5"=>"Module=PM5;Steps=5,5,5"
      },
      {"BST2100" => {"RecipeParameter1"=>"BST2100", "RecipeParameter2"=>2, "RecipeParameter3"=>2.1, 
          "RecipeParameter4"=>true, "RecipeParameter5"=>Time.at(18)},
       "BST2600" => {"RecipeParameter1"=>"BST2600", "RecipeParameter2"=>4, "RecipeParameter3"=>4.1, 
          "RecipeParameter4"=>false, "RecipeParameter5"=>Time.at(18)}}, 
      :recipename_space=>"QA-SP6", :softrev=>"TC10-6") unless self._sp_exists?("QA-SP6")
      
    # additional parameters
    robs << $rmstest.shared_parameter_set_create({"RecipeParameter11"=>"NONE", "RecipeParameter12"=>0, "RecipeParameter12_ci"=>[0,10], 
        "RecipeParameter13"=>0.0, "RecipeParameter13_ci"=>[-1e-1, 2e3], 
        "RecipeParameter14"=>false, "RecipeParameter15"=>Time.at(18), 
        "__options__RecipeParameter11"=>"Module=PM1;Steps=1,1,1", "__options__RecipeParameter12"=>"Module=PM2;Steps=2,2,2",
        "__options__RecipeParameter13"=>"Module=PM3;Steps=3,3,3", "__options__RecipeParameter14"=>"Module=PM4;Steps=4,4,4", 
        "__options__RecipeParameter15"=>"Module=PM5;Steps=5,5,5"
      },
      {"BST2100" => {"RecipeParameter11"=>"BST2100", "RecipeParameter12"=>2, "RecipeParameter13"=>2.1, 
          "RecipeParameter14"=>true, "RecipeParameter15"=>Time.at(18)},
       "BST2601" => {"RecipeParameter11"=>"BST2601", "RecipeParameter12"=>4, "RecipeParameter13"=>4.1, 
          "RecipeParameter14"=>false, "RecipeParameter15"=>Time.at(18)}}, 
      :recipename_space=>"QA-SP7", :softrev=>"TC10-5") unless self._sp_exists?("QA-SP7")
    
    # none-strict modeling
    robs << $rmstest.shared_parameter_set_create({"RecipeParameter1"=>"BST2100", "RecipeParameter2"=>2, "RecipeParameter2_ci"=>[0,10], 
        "RecipeParameter3"=>2.1, "RecipeParameter3_ci"=>[-1e-1, 2e3], 
        "RecipeParameter4"=>true, "RecipeParameter5"=>Time.at(18), 
        "__options__RecipeParameter1"=>"Module=PM1;Steps=1,1,1", "__options__RecipeParameter2"=>"Module=PM2;Steps=2,2,2",
        "__options__RecipeParameter3"=>"Module=PM3;Steps=3,3,3", "__options__RecipeParameter4"=>"Module=PM4;Steps=4,4,4", 
        "__options__RecipeParameter5"=>"Module=PM5;Steps=5,5,5"
      },{}, 
      :recipename_space=>"QA-SP8", :softrev=>"TC10-7", :eqp=>"BST2100") unless self._sp_exists?("QA-SP8", 3)
    robs << $rmstest.shared_parameter_set_create({"RecipeParameter1"=>"BST2101", "RecipeParameter2"=>4, "RecipeParameter2_ci"=>[0,10], 
        "RecipeParameter3"=>4.1, "RecipeParameter3_ci"=>[-1e-1, 2e3], 
        "RecipeParameter4"=>false, "RecipeParameter5"=>Time.at(18), 
        "__options__RecipeParameter1"=>"Module=PM1;Steps=1,1,1", "__options__RecipeParameter2"=>"Module=PM2;Steps=2,2,2",
        "__options__RecipeParameter3"=>"Module=PM3;Steps=3,3,3", "__options__RecipeParameter4"=>"Module=PM4;Steps=4,4,4", 
        "__options__RecipeParameter5"=>"Module=PM5;Steps=5,5,5"
      },{}, 
      :recipename_space=>"QA-SP8", :softrev=>"TC10-7", :eqp=>"BST2101") unless self._sp_exists?("QA-SP8", 3)
    robs << $rmstest.shared_parameter_set_create({"RecipeParameter1"=>"BST2600", "RecipeParameter2"=>4, "RecipeParameter2_ci"=>[0,10], 
        "RecipeParameter3"=>4.1, "RecipeParameter3_ci"=>[-1e-1, 2e3], 
        "RecipeParameter4"=>false, "RecipeParameter5"=>Time.at(18), 
        "__options__RecipeParameter1"=>"Module=PM1;Steps=1,1,1", "__options__RecipeParameter2"=>"Module=PM2;Steps=2,2,2",
        "__options__RecipeParameter3"=>"Module=PM3;Steps=3,3,3", "__options__RecipeParameter4"=>"Module=PM4;Steps=4,4,4", 
        "__options__RecipeParameter5"=>"Module=PM5;Steps=5,5,5"
      },{}, 
      :recipename_space=>"QA-SP8", :softrev=>"TC10-7", :eqp=>"BST2600") unless self._sp_exists?("QA-SP8", 3)
    
    assert $rmstest.recipe_approval(robs), "failed to release setup" unless robs.count == 0
  end
  
  
  def _sp_exists?(rcp_namespace, count=1)
    robs = $rms.find_object_list( {"Department"=>"MSS", "ToolVendor"=>"LAM", "ToolPrefix"=>"BST", 
       "RecipeNameSpaceID"=>rcp_namespace, "RecipeType"=>"SharedParameterSet", "state"=>"ACTIVE"})
    if robs.count == count
      return true
    elsif robs.count > 0
      assert $rms.deactivate(robs), "failed to remove old setup"
    end
    return false
  end
  
  def test1000_setup
    $setup_ok = false
    self._generate_sp_recipes
    $setup_ok = true
  end
  
  def test1001_single_shared_parameter_spec
    self._create_link_setup("QA-SP1")
    self._run_payload_test("BST2100")
    if SP_Parameter_Strict
      self._run_no_payload_test("BST2101", "Recipe model violation: CEI context didn't hit a variant in SharedParameterSet ROB")
    else
      self._run_payload_test("BST2101", (0..0), nil, (1..1), true)
    end
    self._run_payload_test("BST2600")
  end
  
  def test1002_multiple_shared_parameter_specs_overlap
    self._create_link_setup(["QA-SP1", "QA-SP2"])
    self._run_no_payload_test("BST2100", "Recipe model violation: Ambiguous appearance of shared Parameter RecipeParameter")
  end
  
  def test1003_multiple_shared_parameter_specs
    self._create_link_setup(["QA-SP1", "QA-SP3"])
    self._run_payload_test("BST2100", (0..1))
    if SP_Parameter_Strict
      self._run_no_payload_test("BST2600", "Recipe model violation: CEI context didn't hit a variant in SharedParameterSet ROB")
      self._run_no_payload_test("BST2601", "Recipe model violation: CEI context didn't hit a variant in SharedParameterSet ROB")
    else
      self._run_payload_test("BST2600", (0..1), nil, (1..1), [false, true])
      self._run_payload_test("BST2601", (0..1), nil, (1..1), [true, false])
    end
  end
  
  # TODO: Need ability to remove links from subrob
  def test1004_subrecipe_shared_parameter
    self._create_link_setup("QA-SP1", ["QA-SP3"])
    self._run_payload_test("BST2100", (0..0), "d:\\Lam\\data\\PM2\\Recipes\\P-BSBEOLCA-V01.rcp", (1..1))
    if SP_Parameter_Strict
      self._run_no_payload_test("BST2101", "Recipe model violation: CEI context didn't hit a variant in SharedParameterSet ROB")
      self._run_no_payload_test("BST2600", "Recipe model violation: CEI context didn't hit a variant in SharedParameterSet ROB")
    else
      self._run_payload_test("BST2101", (0..0), "d:\\Lam\\data\\PM2\\Recipes\\P-BSBEOLCA-V01.rcp", (1..1), [true,true])
      self._run_payload_test("BST2600", (0..0), "d:\\Lam\\data\\PM2\\Recipes\\P-BSBEOLCA-V01.rcp", (1..1), [false,true])
    end
  end
  
 def test1005_multiple_shared_parameter_specs
    self._create_link_setup(["QA-SP1", "QA-SP3", "QA-SP7"])
    self._run_payload_test("BST2100", (0..2))
    if SP_Parameter_Strict
      self._run_no_payload_test("BST2600", "Recipe model violation: CEI context didn't hit a variant in SharedParameterSet ROB")
      self._run_no_payload_test("BST2601", "Recipe model violation: CEI context didn't hit a variant in SharedParameterSet ROB")
    else
      self._run_payload_test("BST2600", (0..2), nil, (1..1), [false,true,true])
      self._run_payload_test("BST2601", (0..2), nil, (1..1), [true,false,false])
    end    
  end
  
  def test1006_none_strict_attribute
    self._create_link_setup("QA-SP5")
    self._run_payload_test("BST2100")
    self._run_payload_test("BST2101", (0..0), nil, (1..1), [true]) # no variance, but still successful
  end
  
  def test1007_strict_attribute
    self._create_link_setup("QA-SP6")
    self._run_payload_test("BST2100")    
    self._run_no_payload_test("BST2101", "Recipe model violation: CEI context didn't hit a variant in SharedParameterSet ROB")    
  end

  # TODO: Need improved check
  def test1008_multiple_shared_parameter_specs_mixed_strict
    self._create_link_setup(["QA-SP3", "QA-SP5"])
    self._run_payload_test("BST2100", (0..1))
    self._run_payload_test("BST2601", (0..1), nil, (1..1), [true, false]) # no variance, but still successful
  end
  
  def test1009_none_strict_default
    self._create_link_setup("QA-SP8")
    if SP_Parameter_Strict
      self._run_no_payload_test("BST2100", "Recipe model violation: CEI context didn't hit a variant in SharedParameterSet ROB")    
      self._run_no_payload_test("BST2101", "Recipe model violation: CEI context didn't hit a variant in SharedParameterSet ROB")    
      self._run_no_payload_test("BST2600", "Recipe model violation: CEI context didn't hit a variant in SharedParameterSet ROB")    
    else
      self._run_payload_test("BST2100")
      self._run_payload_test("BST2101")
      self._run_payload_test("BST2600")
    end
    self._run_no_payload_test("BST2601", "No recipes found in RMS for that context: Registration RCP_PROD")
  end
  
  def _gen_param_values(eqp, psets, default_flag=false)
    p = []
    _default = default_flag.kind_of?(Array) ? default_flag : [default_flag] *psets.count
    psets.each {|j|      
      _val = _default[j-psets.first] ? "NONE": eqp
      i = j*5
      p << RMS::RIL::Parameter.new("RecipeParameter#{i+1}", "PARAMETER#{i+1}", nil, nil, _val, "Tuning", {"Steps"=>"1,1,1", "Module"=>"PM1"})
      _val = _default[j-psets.first] ? 0 : ((eqp == "BST2100") ? 2 : 4)
      p << RMS::RIL::Parameter.new("RecipeParameter#{i+2}", "PARAMETER#{i+2}", 0.0, 10.0, _val, "Tuning", {"Steps"=>"2,2,2", "Module"=>"PM2"})
      _val = _default[j-psets.first] ? 0.0 : ((eqp == "BST2100") ? 2.1 : 4.1)
      p << RMS::RIL::Parameter.new("RecipeParameter#{i+3}", "PARAMETER#{i+3}", -1e-1, 2e3, _val, "Tuning", {"Steps"=>"3,3,3", "Module"=>"PM3"})
      p << RMS::RIL::Parameter.new("RecipeParameter#{i+4}", "PARAMETER#{i+4}", nil, nil, (eqp == "BST2100"), "Tuning", {"Steps"=>"4,4,4", "Module"=>"PM4"})
      p << RMS::RIL::Parameter.new("RecipeParameter#{i+5}", "PARAMETER#{i+5}", nil, nil, Time.at(18).utc.strftime('%FT%T.%LZ'), "Tuning", {"Steps"=>"5,5,5", "Module"=>"PM5"})    
    }
    return p
  end
  
  def _run_payload_test(eqp, main_set=(0..0), recipename=nil, sub_set=(1..1), default=false)
    recipe = $ril.get_recipe_payload_request(@@context.merge({"Equipment"=>eqp}))
    
    p = self._gen_param_values(eqp, main_set, default)
    assert $rmstest.verify_recipe_parameters(p, recipe), "failed parameter check"
    
    if recipename
      recipe = recipe.linkedrecipes.find {|r| r.rid == recipename}
      assert recipe, " subrecipe with name #{recipename} not found" 
      _default = default.kind_of?(Array) ? default[-1] : false
      assert $rmstest.verify_recipe_parameters(self._gen_param_values(eqp, sub_set, _default), recipe), "failed parameter check"
    end    
  end
  
  
  def _create_link_setup(rcp_namespaces, subrob_links=[])
    rcp_namespaces = [rcp_namespaces] unless rcp_namespaces.kind_of?(Array)
    rob = $rms.find_object_with_data_exp(@@context.merge({"Department"=>"MSS", "ToolVendor"=>"LAM", "ToolPrefix"=>"BST"}))
    assert rob, "failed to find configured recipes"
    contexts = rcp_namespaces.map {|rcp_namespace|
      {"CEIRecipeType"=>"SharedParameterSet", "Equipment"=>"$", "RecipeNameSpaceID"=>rcp_namespace, "ToolPrefix"=>"BST", "ToolVendor"=>"LAM"}
    }
    assert $rmstest.recipe_update(rob, :sp_contexts=>contexts, :checklinks=>false, :ignorelinks=>true)
      
    # Update subrob    
    rob = $rms.find_object_with_data_exp({"Department"=>"MSS", "ToolVendor"=>"LAM", "ToolPrefix"=>"BST", 
      "RecipeName"=>"P-BSBEOLCA-V01", "Chamber"=>"PM2"})
    assert rob, "failed to find configured subrecipe"
    contexts = subrob_links.map {|rcp_namespace|
      {"CEIRecipeType"=>"SharedParameterSet", "Equipment"=>"$", "RecipeNameSpaceID"=>rcp_namespace, "ToolPrefix"=>"BST", "ToolVendor"=>"LAM"}
    }
    unless contexts == rob.shared_params
      if contexts == []
        assert $rmstest.recipe_update(rob, :rm_dataitems=>["SharedParameterLink"], :checklinks=>false, :ignorelinks=>true)
      else
        assert $rmstest.recipe_update(rob, :sp_contexts=>contexts, :checklinks=>false, :ignorelinks=>true)
      end
    end
  end
  
  def _run_no_payload_test(eqp, msg)
    e = assert_raises RMS::RilError do
      $ril.get_recipe_payload_request(@@context.merge({"Equipment"=>eqp}))
    end
    assert_match /#{msg}/, e.message
  end
end