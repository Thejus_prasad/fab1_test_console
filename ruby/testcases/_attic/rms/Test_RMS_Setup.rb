=begin
 automated test of RMS 

(c) Copyright 2011 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

 author      - daniel.steger@globalfoundries.com
 date        - 2012/08/28
=end

require 'RubyTestCase'
require 'rmstest'


# RMS Recipe management tests
class Test_RMS_Setup < RubyTestCase
  Description = 'Test RMS Setup Interface'

  @@department = 'MSS'
  @@release = 'RM5-ECC211'
  #@@eqp =  'THK2101'
  #@@eqp = 'ALC2600'
  @@eqp = 'ALC400' #ETX002 mtqa
  @@loop_count = 2
  @@robdef_keys =  {'Department'=>@@department, 'ToolPrefix'=>'IMP-MC', 'ToolVendor'=>'VARIAN', 'RecipeType'=>'Main'}
  @@upload_eqp = 'IMP2200'

  @@recipe_count = 20
  @@replication_time = 60
  
  # Default EI contexts
  @@default_ei_ctx = {
      'Equipment'=>'BST2100',
      'ProcessDefinition'=>'PROCESS.01',
      'PhotoLayer'=>'PH',
      'ReticleID'=>'RETICLE01',
      'MainPD'=>'ROUTE.01',
      'ProductID.EC'=>'RIL-PRODUCT.01',
      'ProductGroup'=>'RILPG',
      'Technology'=>'20NM'
    }
    
  #@@default_ei_ctx = {
  #    'Equipment'=>'LETCL1001',
  #    'ProcessDefinition'=>'',
  #    'PhotoLayer'=>'',
  #    'ReticleID'=>'',
  #    'MainPD'=>'',
  #    'ProductID.EC'=>'',
  #    'ProductGroup'=>'',
  #    'Technology'=>''
  #  }
  
  @@default_ctx = {
      'Equipment'=>'BST2100',
      'ProcessDefinition'=>'PROCESS.01',
      'PhotoLayer'=>'PH',
      'ReticleID'=>'RETICLE01',
      'MainProcessDefinition'=>'ROUTE.01',
      'ProductID_EC'=>'RIL-PRODUCT.01',
      'ProductGroup'=>'RILPG',
      'Technology'=>'20NM'
    }

  @@lam_recipe = {
    'Q-PM2-PC-MECH.rcp' => RmsAPI::RobObject.new(nil,
      { 'ToolRecipeName' => 'd:\\Lam\\data\\PM2\\Recipes\\Q-PM2-PC-MECH.rcp', 'RecipeType' => 'Chamber' },
      { 'ModuleID' => 'PM2', 'DCPPath' => '/path/to/system/dcp' }, nil),
    'Q-PM2-PC-MECH.flw' => RmsAPI::RobObject.new(nil,
      { 'ToolRecipeName' => 'd:\\Lam\\data\\Flows\\Q-PM2-PC-MECH.flw', 'RecipeType' => 'WaferFlow', 'RecipeNameSpaceID' => 'QA-TEST', 'RecipeName' => "LAM-RECIPE-#{Time.now.to_i}" },
      { 'ModuleCombination' => 'PM2' },
      [{ 'RecipeType' => 'Chamber', 'ToolRecipeName' => 'd:\\Lam\\data\\PM2\\Recipes\\Q-PM2-PC-MECH.rcp' }])
  }
  
  # config/preparation task
  def self.startup
    $rmstest = RmsTest.new($env, @@eqp, @@department)    
    $ril = $rmstest.ril
    $rms = $rmstest.rms
    $rms.login
  end
end
