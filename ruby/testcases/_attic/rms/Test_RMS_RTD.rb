=begin
 automated test of setup.FC API 

(c) Copyright 2011 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

 author      - daniel.steger@globalfoundries.com
 date        - 2012/08/28
=end

require_relative 'Test_RMS_Setup'

# RMS Recipe management tests
class Test_RMS_RTD < Test_RMS_Setup
  Description = 'Test RIL-EI Interface'
  
  @@loop_count = 2
  @@robdef_keys =  {'Department'=>'MSS', 'ToolPrefix'=>'IMP-MC', 'ToolVendor'=>'VARIAN', 'RecipeType'=>'Main'}
  @@recipe_count = 10
  @@replication_time = 180
  
  def _wait_and_verify(recipe, handle, deactivated=false, waittime=@@replication_time)
    $log.info "wait for replication... #{waittime}"
    assert wait_for(:timeout=>waittime) {
      $rmstest.verify_rtd_recipe_parameters(recipe, handle, :deactivated=>deactivated)
    }, 'verification failed'
  end

  def test0310_rtd_parameters_no_rtdrule
    $setup_ok = false
    #$rmstest.setup_litho_recipes
    time = Time.now.to_i
    # new style - tags but no __purpose__ and options
    imp_recipe = {
      'IMP2100/P-BF-07T007-5E15-00Q' => RmsAPI::RobObject.new(nil,
      @@default_ctx.merge('ToolPrefix' => 'IMP-HC', 'ToolRecipeName' => "#{time}\\P-BF-07T007-5E15-00Q", 'RecipeType' => 'Main', 'RecipeNameSpaceID' => 'QA-TEST', 'RecipeName' => "IMP-HC-RECIPE-#{time}"),
      { 'QAParamString' => RmsAPI::DataItem.new('A', ['Tuning']), 'QAParamDouble' => RmsAPI::DataItem.new(7.0, ['Tuning']), 'QAParamInteger' => RmsAPI::DataItem.new(7, ['Tuning']), 
      'QAParamBool' => RmsAPI::DataItem.new(false, ['Tuning']), 'QAParamTime' => RmsAPI::DataItem.new(Time.now, ['Tuning']) }, nil)
    }
    $robs = $rmstest.recipe_create(imp_recipe, tool_prefix: 'IMP-HC', tool_vendor: 'VARIAN', softrev: 'TC02-1')
    assert $robs = $rmstest.recipe_approval($robs), 'Recipes not created successfully'
    # Check RTD parameters (waferflow recipe only)
    assert_equal 1, $robs.count, 'No rob found'    
    assert self._verify_rtd_rob_structure($robs, [{0=>[]}])
    # Update parameter value
    new_parameters = { 'QAParamString' => RmsAPI::DataItem.new('B', ['Tuning']),  'QAParamDouble' => RmsAPI::DataItem.new(8.0, ['Tuning']), 'QAParamInteger' => RmsAPI::DataItem.new(8, ['Tuning']), 
      'QAParamBool' => RmsAPI::DataItem.new(true, ['Tuning']), 'QAParamTime' => RmsAPI::DataItem.new(Time.now, ['Tuning']) }
    $robs[0] = $rmstest.recipe_update($robs[0], dataitems: new_parameters)
    imp_recipe['IMP2100/P-BF-07T007-5E15-00Q'].dataitems.merge!(new_parameters)
    assert self._verify_rtd_rob_structure($robs, [{0=>[]}])
    # Update recipe context
    new_ctx = { 'RecipeNameSpaceID'=>'QA-TEST2', 'ProductID_EC'=>'RIL-PRODUCT.02' }
    $robs[0] = $rmstest.recipe_update($robs[0], context: new_ctx)
    imp_recipe['IMP2100/P-BF-07T007-5E15-00Q'].context.merge!(new_ctx)
    assert self._verify_rtd_rob_structure($robs, [{0=>[]}])
    # put it onhold
    $robs = $rms.set_onhold($robs[0], ['DEFAULT', 'SPC_PROBLEM'])
    assert self._verify_rtd_rob_structure($robs, [{0=>[]}], onhold_flag: true)
    # still onhold
    $robs = $rms.set_onhold($robs[0], ['SPC_PROBLEM'])
    assert self._verify_rtd_rob_structure($robs, [{0=>[]}], onhold_flag: true)
    # release the hold
    $robs = $rms.set_onhold($robs[0], [])
    assert self._verify_rtd_rob_structure($robs, [{0=>[]}], onhold_flag: false)
    # delete it
    $robs = $rmstest.rms.deactivate($robs)
    assert_equal 1, $robs.count, 'Recipe not successfully deactivated'
    assert self._verify_rtd_rob_structure($robs, [{0=>[]}], deactivated: true)
    $setup_ok = true
  end

  def test0311_rtd_parameters_main_qamain
    #$rmstest.setup_litho_recipes
    time = Time.now.to_i
    imp_recipe = {
      'IMP2100/P-BF-07T007-5E15-00Q' => RmsAPI::RobObject.new(nil,
      @@default_ctx.merge('ToolPrefix' => 'IMP-MC', 'ToolRecipeName' => "#{time}\\P-BF-07T007-5E15-00Q", 'RecipeType' => 'Main', 'RecipeNameSpaceID' => 'QA-TEST', 'RecipeName' => "IMP-HC-RECIPE-#{time}"),
      { 'QAParamString' => RmsAPI::DataItem.new('A', ['Tuning']), 'QAParamDouble' => RmsAPI::DataItem.new(7.0, ['Tuning']), 'QAParamInteger' => RmsAPI::DataItem.new(7, ['Tuning']), 
      'QAParamBool' => RmsAPI::DataItem.new(false, ['Tuning']), 'QAParamTime' => RmsAPI::DataItem.new(Time.now, ['Tuning']) }, nil)
    }
    $robs = $rmstest.recipe_create(imp_recipe, tool_prefix: 'IMP-MC', tool_vendor: 'VARIAN', softrev: 'TC02-4')
    assert $robs = $rmstest.recipe_approval($robs), 'Recipes not created successfully'
    # Check RTD parameters (waferflow recipe only)
    assert_equal 1, $robs.count, 'No rob found'    
    assert self._verify_rtd_rob_structure($robs, [{0=>[]}])
    # delete it
    $robs = $rmstest.rms.deactivate($robs)
    assert_equal 1, $robs.count, 'Recipe not successfully deactivated'
    assert self._verify_rtd_rob_structure($robs, [{0=>[]}], deactivated: true)    
  end
  
  def test0312_rtd_parameters_main_qanone
    #$rmstest.setup_litho_recipes
    time = Time.now.to_i
    imp_recipe = {
      'IMP2100/P-BF-07T007-5E15-00Q' => RmsAPI::RobObject.new( nil,
        @@default_ctx.merge({ 'ToolRecipeName'=>"#{time}\\P-BF-07T007-5E15-00Q", 'RecipeType'=>'Main', 'RecipeNameSpaceID'=>'QA-TEST', 'RecipeName'=>"IMP-HC-RECIPE-#{time}"}),
        { 'Species'=>'8', 'QAParamInteger' => 7, 'QAParamBool'=>false, 'QAParamTime'=>Time.now },
        nil),
    }
    $robs = $rmstest.recipe_create(imp_recipe, :tool_prefix=>'IMP-QA', :tool_vendor=>'VARIAN', :softrev=>'TC02-5')
    $robs = $rmstest.recipe_approval($robs)
    assert $robs, 'Recipes not created successfully'

    # Check RTD parameters (waferflow recipe only)
    assert self._verify_rtd_rob_structure($robs, [{0=>[]}], :deactivated=>true)
    
    # delete it
    $robs = $rmstest.rms.deactivate($robs)
    assert_equal 1, $robs.count, 'Recipe not successfully deactivated'

    assert self._verify_rtd_rob_structure($robs, [{0=>[]}], :deactivated=>true) 
  end
  
  def test0313_rtd_no_parameters
    time = Time.now.to_i
    imp_recipe = {
      'IMP2100/P-BF-07T007-5E15-00Q' => RmsAPI::RobObject.new(nil,
      @@default_ctx.merge('ToolPrefix' => 'IMP-HC', 'ToolRecipeName' => "#{time}\\P-BF-07T007-5E15-00Q", 'RecipeType' => 'Main', 'RecipeNameSpaceID' => 'QA-TEST', 'RecipeName' => "IMP-HC-RECIPE-#{time}"),
      { 'QAParamString' => RmsAPI::DataItem.new('A', ['Tuning']), 'QAParamDouble' => RmsAPI::DataItem.new(7.0, ['Tuning']), 'QAParamInteger' => RmsAPI::DataItem.new(7, ['Tuning']), 
      'QAParamBool' => RmsAPI::DataItem.new(false, ['Tuning']), 'QAParamTime' => RmsAPI::DataItem.new(Time.now, ['Tuning']) }, nil)
    }
    robs = $rmstest.recipe_create(imp_recipe, :tool_prefix=>'IMP-HC', :tool_vendor=>'VARIAN', :softrev=>'TC03-13')
    robs = $rmstest.recipe_approval(robs)
    assert robs, 'Recipes not created successfully'

    # Check RTD parameters (waferflow recipe only)
    assert self._verify_rtd_rob_structure(robs, [{0=>[]}])
    
    # Update recipe context
    new_parameters = { 'QAParamDouble' => RmsAPI::DataItem.new(8.0, ['Tuning']), 'QAParamString' => RmsAPI::DataItem.new('C', ['Tuning']) }
    robs = self._update_recipe(imp_recipe, 'IMP2100/P-BF-07T007-5E15-00Q', robs, new_parameters)  
    assert self._verify_rtd_rob_structure(robs, [{0=>[]}])

    robs = $rmstest.rms.deactivate(robs)
    assert_equal 1, robs.count, 'Recipe not successfully deactivated'

    assert self._verify_rtd_rob_structure(robs, [{0=>[]}], :deactivated=>true)
  end
  
  def test0320_fixed_link_qasub
    time = Time.now.to_i
    lam_recipe = {
      'BST2100/PM2/Q-PM2-PC-MECH.rcp' => RmsAPI::RobObject.new( nil,
        { 'ToolRecipeName'=>"d:\\Lam\\data\\PM2\\Recipes\\Q-PM2-PC-MECH-#{time}.rcp", 'RecipeType'=>'Chamber_Etch', 'RecipeName'=>"Q-PM2-PC-MECH-#{time}.rcp" },
        { 'ModuleID' => RmsAPI::DataItem.new('PM2', ['EI Control']), 'DCPPath' => '/path/to/system/dcp', 'QASubParam1' => RmsAPI::DataItem.new('QATest', ['EI Control'])}, nil),
      'BST2100/Q-PM2-PC-MECH.flw' => RmsAPI::RobObject.new( nil,
        { 'ToolRecipeName' => 'd:\\Lam\\data\\Flows\\Q-PM2-PC-MECH.flw', 'RecipeType'=>'WaferFlow', 'RecipeNameSpaceID'=>'QA-TEST', 'RecipeName'=>"LAM-RECIPE-#{time}" },
        { 'ModuleCombination' => RmsAPI::DataItem.new('PM2', ['EI Control'])},
        [{'Chamber'=>'PM2', 'RecipeType'=>'Chamber_Etch', 'ToolRecipeName'=>"d:\\Lam\\data\\PM2\\Recipes\\Q-PM2-PC-MECH-#{time}.rcp"}]),
    }

    robs = _do_approval_cycle(lam_recipe, [{1=>0}], eval_subrobs: true, softrev: 'TC03-20', nodeactivation: true)
    # update sub-recipe
    robs = _update_recipe(lam_recipe, 'BST2100/PM2/Q-PM2-PC-MECH.rcp', robs, {'QASubParam1' => 'QA2', 'QASubParam2' => '12345'})    
    assert _verify_rtd_rob_structure(robs, [{1=>0}], eval_subrobs: true)

    # put sub recipe onhold    
    robs = _update_recipe(lam_recipe, 'BST2100/PM2/Q-PM2-PC-MECH.rcp', robs, {}, true)
    assert _verify_rtd_rob_structure(robs, [{1=>0}], eval_subrobs: true, onhold_flag: true)
    
    # update main-recipe
    robs = _update_recipe(lam_recipe, 'BST2100/Q-PM2-PC-MECH.flw', robs, {'ModuleCombination' => RmsAPI::DataItem.new('PMX', ['EI Control'])})    
    assert _verify_rtd_rob_structure(robs, [{1=>0}], eval_subrobs: true, onhold_flag: true)

    robs = $rmstest.rms.deactivate(robs)
    assert robs, 'Recipes not successfully deactivated'
    assert _verify_rtd_rob_structure(robs, [{1=>0}], eval_subrobs: true, deactivated: true)
  end
  
  def _update_recipe(rcp_hash, recipename, robs, new_parameters, put_onhold=false)
    trcp_name = rcp_hash[recipename]
    rob = robs.find {|r| r.context['RecipeName'] == trcp_name.context['RecipeName']}
    if put_onhold
      rob_new = $rms.set_onhold(rob, ['DEFAULT'])[0]
    else
      rob_new = $rmstest.recipe_update(rob, :dataitems=>new_parameters)
    end
    
    robs.map! {|r| r.rmshandle == rob.rmshandle ? rob_new : r }
    rcp_hash[recipename].dataitems.merge!(new_parameters)
    return robs
  end

  def test0322_propagated_link_no_match_qamain
    # Wildcard Recipe with 0 subrecipes
    time = Time.now.to_i
    lam_recipe = {
      'BST2100/Q-PM2-PC-MECH.flw' => RmsAPI::RobObject.new( nil,
      { 'ToolRecipeName' => 'd:\\Lam\\data\\Flows\\Q-PM2-PC-MECH.flw', 'RecipeType' => 'WaferFlow', 'RecipeNameSpaceID' => 'QA-TEST', 'RecipeName' => "LAM-RECIPE-#{time}" },
      { 'ModuleCombination' => RmsAPI::DataItem.new('PM3', ['EI Control']) },
      [{ 'RecipeType' => 'Chamber_Etch', 'Equipment' => '$', 'ProductID_EC' => '$', 'ToolRecipeName' => 'd:\\Lam\\data\\PM2\\Recipes\\Q-PM2-PC-MECH.flw' }])
    }
    _do_approval_cycle(lam_recipe, [{ 0 => [] }], eval_subrobs: false, tool_prefix: 'BST-QA', softrev: 'TC02-7')
  end
  
  def test0323_sub_parameter_group
    time = Time.now.to_i
    all_recipes = {
      'MDX2900/CH/1P-AL28K' => RmsAPI::RobObject.new(nil,
                                                     { 'ToolRecipeName' => "1P-AL28K-#{time}", 'RecipeType' => 'Module_PVD',
                                                       'RecipeName' => '1P-AL28K' },
                                                     { 'ModuleID' => RmsAPI::DataItem.new('CH4,CH5', ['EI Control']), 'ConditioningActive' => RmsAPI::DataItem.new('YES', ['EI Control']), 
                                                       'ConditioningType' => RmsAPI::DataItem.new('QATest', ['EI Control']), 'ConditioningMaxIdleTime' => RmsAPI::DataItem.new(77, ['EI Control']) }, nil),
      'MDX2900/CH/1P-CHABCOOL' => RmsAPI::RobObject.new(nil,
                                                        { 'ToolRecipeName' => "1P-CHABCOOL-#{time}", 'RecipeType' => 'Module_Cool',
                                                          'RecipeName' => '1P-CHABCOOL' },
                                                        { 'ModuleID' => RmsAPI::DataItem.new('CHB', ['EI Control']) }, nil),
      'MDX2900/CH/1P-DEGAS300C' => RmsAPI::RobObject.new(nil,
                                                         { 'ToolRecipeName' => "1P-DEGAS300C-#{time}", 'RecipeType' => 'Module_Degas',
                                                           'RecipeName' => '1P-DEGAS300C' },
                                                         { 'ModuleID' => RmsAPI::DataItem.new('CHE,CHF', ['EI Control']) }, nil),
      'MDX2900/CH/1P-PCXTLP50' => RmsAPI::RobObject.new(nil,
                                                        { 'ToolRecipeName' => "1P-PCXTLP50-#{time}", 'RecipeType' => 'Module_PCXT',
                                                          'RecipeName' => '1P-PCXTLP50' },
                                                        { 'ModuleID' => RmsAPI::DataItem.new('CHC,CHD', ['EI Control']) }, nil),
      'MDX2900/CH/1P-TAN500' => RmsAPI::RobObject.new(nil,
                                                      { 'ToolRecipeName' => "1P-TAN500-#{time}", 'RecipeType' => 'Module_PVD',
                                                        'RecipeName' => '1P-TAN500' },
                                                      { 'ModuleID' => RmsAPI::DataItem.new('CH1', ['EI Control']), 'ConditioningActive' => RmsAPI::DataItem.new('NO', ['EI Control']), 'ConditioningMaxIdleTime' => RmsAPI::DataItem.new(30, ['EI Control']) }, nil),
      'MDX2900/CH/1P-TI250' => RmsAPI::RobObject.new(nil,
                                                     { 'ToolRecipeName' => "1P-TI250-#{time}", 'RecipeType' => 'Module_PVD',
                                                       'RecipeName' => '1P-TI250' },
                                                     { 'ModuleID' => RmsAPI::DataItem.new('CH2', ['EI Control']), 'ConditioningActive' => RmsAPI::DataItem.new('YES', ['EI Control']), 
                                                        'ConditioningType' => RmsAPI::DataItem.new('QATest', ['EI Control']), 'ConditioningMaxIdleTime' => RmsAPI::DataItem.new(60, ['EI Control']) }, nil),
      'MDX2900/CH/1P-TIN250' => RmsAPI::RobObject.new(nil,
                                                      { 'ToolRecipeName' => "1P-TIN250-#{time}", 'RecipeType' => 'Module_PVD',
                                                        'RecipeName' => '1P-TIN250' },
                                                      { 'ModuleID' => RmsAPI::DataItem.new('CH3', ['EI Control']) }, nil),
      'MDX2900/F-P-20NMTERMD-28KAL' => RmsAPI::RobObject.new(nil,
                                                             { 'ToolRecipeName' => 'F-P-20NMTERMD-28KAL', 'RecipeType' => 'WaferFlow', 'RecipeNameSpaceID' => 'QA-TEST', 'RecipeName' => "MDX-RECIPE-#{time}" },
                                                             { 'ModuleCombination' => RmsAPI::DataItem.new('[CHE:CHF][CHC:CHD]CHA:CH1:CH2:CH3[CH4:CH5]CHB', ['EI Control']) },
                                                             [{ 'ToolRecipeName' => "1P-AL28K-#{time}", 'Equipment' => '$' },
                                                              { 'ToolRecipeName' => "1P-CHABCOOL-#{time}", 'Equipment' => '$' },
                                                              { 'ToolRecipeName' => "1P-DEGAS300C-#{time}", 'Equipment' => '$' },
                                                              { 'ToolRecipeName' => "1P-PCXTLP50-#{time}", 'Equipment' => '$' },
                                                              { 'ToolRecipeName' => "1P-TAN500-#{time}", 'Equipment' => '$' },
                                                              { 'ToolRecipeName' => "1P-TI250-#{time}", 'Equipment' => '$' },
                                                              { 'ToolRecipeName' => "1P-TIN250-#{time}", 'Equipment' => '$' }])
    }

    _do_approval_cycle(all_recipes, [{ 7 => [0, 1, 2, 3, 4, 5, 6] }], tool_vendor: 'AMAT', tool_prefix: 'MDX', softrev: 'TC02-6', eval_subrobs: true)
  end

  def test0324_no_link_qasub
    time = Time.now.to_i
    lam_recipe = {      
      'BST2100/Q-PM2-PC-MECH.flw' => RmsAPI::RobObject.new(nil, { 'ToolRecipeName' => 'd:\\Lam\\data\\Flows\\Q-PM2-PC-MECH.flw', 'RecipeType' => 'WaferFlow', 'RecipeNameSpaceID' => 'QA-TEST', 'RecipeName' => "LAM-RECIPE-#{time}" },
        { 'ModuleCombination' => RmsAPI::DataItem.new('PM2', ['EI Control']) }, [])
    }

    _do_approval_cycle(lam_recipe, [{ 0 => [] }], eval_subrobs: false, softrev: 'TC02-3')
  end
  
  def test0325_sub_level_three_group
    time = Time.now.to_i
    all_recipes = {
      'MDX2900/CH/1P-AL28K' => RmsAPI::RobObject.new(nil,
                                                     { 'ToolRecipeName' => "1P-AL28K-#{time}", 'RecipeType' => 'Module_PVD',
                                                       'RecipeName' => '1P-AL28K' },
                                                     { 'ModuleID' => RmsAPI::DataItem.new('CH4,CH5', ['EI Control']), 'ConditioningActive' => RmsAPI::DataItem.new('YES', ['EI Control']), 
                                                       'ConditioningType' => RmsAPI::DataItem.new('QATest', ['EI Control']), 'ConditioningMaxIdleTime' => RmsAPI::DataItem.new(77, ['EI Control']) }, nil),
      'MDX2900/CH/1P-AL28K-2' => RmsAPI::RobObject.new(nil,
                                                       { 'ToolRecipeName' => "1P-AL28K-#{time}-2", 'RecipeType' => 'Module_PVD', 'Chamber' => 'CH4',
                                                         'RecipeName' => '1P-AL28K-2' },
                                                       { 'ModuleID' => RmsAPI::DataItem.new('CH4,CH5', ['EI Control']), 'ConditioningActive' => RmsAPI::DataItem.new('YES', ['EI Control']), 
                                                         'ConditioningType' => RmsAPI::DataItem.new('QATest', ['EI Control']), 'ConditioningMaxIdleTime' => RmsAPI::DataItem.new(88, ['EI Control']) }, nil),
      'MDX2900/CH/1P-CHABCOOL' => RmsAPI::RobObject.new(nil,
                                                        { 'ToolRecipeName' => "1P-CHABCOOL-#{time}", 'RecipeType' => 'Module_Cool',
                                                          'RecipeName' => '1P-CHABCOOL' },
                                                        { 'ModuleID' => RmsAPI::DataItem.new('CHB', ['EI Control']) }, nil),
      'MDX2900/CH/1P-DEGAS300C' => RmsAPI::RobObject.new(nil,
                                                         { 'ToolRecipeName' => "1P-DEGAS300C-#{time}", 'RecipeType' => 'Module_Degas',
                                                           'RecipeName' => '1P-DEGAS300C' },
                                                         { 'ModuleID' => RmsAPI::DataItem.new('CHE,CHF', ['EI Control']) },
                                                         [{ 'ToolRecipeName' => "1P-AL28K-#{time}-2", 'Chamber' => '$', 'Equipment' => '$' }]),
      'MDX2900/CH/1P-PCXTLP50' => RmsAPI::RobObject.new(nil,
                                                        { 'ToolRecipeName' => "1P-PCXTLP50-#{time}", 'RecipeType' => 'Module_PCXT',
                                                          'RecipeName' => '1P-PCXTLP50' },
                                                        { 'ModuleID' => RmsAPI::DataItem.new('CHC,CHD', ['EI Control']) }, nil),
      'MDX2900/CH/1P-TAN500' => RmsAPI::RobObject.new(nil,
                                                      { 'ToolRecipeName' => "1P-TAN500-#{time}", 'RecipeType' => 'Module_PVD',
                                                        'RecipeName' => '1P-TAN500' },
                                                      { 'ModuleID' => RmsAPI::DataItem.new('CH1', ['EI Control']), 'ConditioningActive' => RmsAPI::DataItem.new('NO', ['EI Control']), 'ConditioningMaxIdleTime' => RmsAPI::DataItem.new(30, ['EI Control']) }, nil),
      'MDX2900/CH/1P-TI250' => RmsAPI::RobObject.new(nil,
                                                     { 'ToolRecipeName' => "1P-TI250-#{time}", 'RecipeType' => 'Module_PVD',
                                                       'RecipeName' => '1P-TI250' },
                                                     { 'ModuleID' => RmsAPI::DataItem.new('CH2', ['EI Control']), 'ConditioningActive' => RmsAPI::DataItem.new('YES', ['EI Control']), 'ConditioningType' => RmsAPI::DataItem.new('QATest', ['EI Control']), 
                                                       'ConditioningMaxIdleTime' => RmsAPI::DataItem.new(60, ['EI Control']) }, nil),
      'MDX2900/CH/1P-TIN250' => RmsAPI::RobObject.new(nil,
                                                      { 'ToolRecipeName' => "1P-TIN250-#{time}", 'RecipeType' => 'Module_PVD',
                                                        'RecipeName' => '1P-TIN250' },
                                                      { 'ModuleID' => RmsAPI::DataItem.new('CH3', ['EI Control']) }, nil),
      'MDX2900/F-P-20NMTERMD-28KAL' => RmsAPI::RobObject.new(nil,
                                                             { 'ToolRecipeName' => 'F-P-20NMTERMD-28KAL', 'RecipeType' => 'WaferFlow', 'RecipeNameSpaceID' => 'QA-TEST', 'RecipeName' => "MDX-RECIPE-#{time}" },
                                                             { 'ModuleCombination' => RmsAPI::DataItem.new('[CHE:CHF][CHC:CHD]CHA:CH1:CH2:CH3[CH4:CH5]CHB', ['EI Control']) },
                                                             [{ 'ToolRecipeName' => "1P-AL28K-#{time}", 'Equipment' => '$' },
                                                              { 'ToolRecipeName' => "1P-CHABCOOL-#{time}", 'Equipment' => '$' },
                                                              { 'ToolRecipeName' => "1P-DEGAS300C-#{time}", 'Equipment' => '$', 'Chamber' => 'CH4' },
                                                              { 'ToolRecipeName' => "1P-PCXTLP50-#{time}", 'Equipment' => '$' },
                                                              { 'ToolRecipeName' => "1P-TAN500-#{time}", 'Equipment' => '$' },
                                                              { 'ToolRecipeName' => "1P-TI250-#{time}", 'Equipment' => '$' },
                                                              { 'ToolRecipeName' => "1P-TIN250-#{time}", 'Equipment' => '$' }])
    }

    _do_approval_cycle(all_recipes, [{ 8 => [0, 1, 2, 3, 4, 5, 6, 7] }], tool_vendor: 'AMAT', tool_prefix: 'MDX', softrev: 'TC02-6', eval_subrobs: true)
  end
  
  def test0330_rtd_parameters_variance_qamain
    #$rmstest.setup_litho_recipes
    time = Time.now.to_i
    imp_recipe = {
      'IMP2100/P-BF-07T007-5E15-00Q' => RmsAPI::RobObject.new(nil,
      {'ToolRecipeName' => "#{time}\\P-BF-07T007-5E15-00Q", 'RecipeType' => 'Main', 'RecipeNameSpaceID' => 'QA-TEST', 'RecipeName' => "IMP-HC-RECIPE-#{time}"},
      { 'QAParamString' => RmsAPI::DataItem.new('A', ['Tuning']), 'QAParamDouble' => RmsAPI::DataItem.new(7.0, ['Tuning']), 'QAParamInteger' => RmsAPI::DataItem.new(7, ['Tuning']), 
      'QAParamBool' => RmsAPI::DataItem.new(false, ['Tuning']), 'QAParamTime' => RmsAPI::DataItem.new(Time.now, ['Tuning']) }, nil)
    }
    imp_recipe_v = {
      'IMP2100/P-BF-07T007-5E15-00Q' =>
        [RmsAPI::RobObject.new(nil, { 'Equipment' => 'IMP2101' }, 'QAParamDouble' => RmsAPI::DataItem.new(8.0, ['Tuning'])),
         RmsAPI::RobObject.new(nil, { 'Equipment' => 'IMP2102' }, 'QAParamDouble' => RmsAPI::DataItem.new(9.0, ['Tuning']))]
    }
    $robs = $rmstest.recipe_create(imp_recipe, variants: imp_recipe_v, tool_prefix: 'IMP-MC', tool_vendor: 'VARIAN', softrev: 'TC02-4')
    $robs = $rmstest.recipe_approval($robs)
    assert $robs, 'Recipes not created successfully'

    # Check RTD parameters (Main recipe only)
    assert_equal 1, $robs.count, 'No rob found'    
    assert self._verify_rtd_rob_structure($robs, [{0=>[]}])
   
    # Update variant parameter value    
    $robs[0] = $rms.create_work_copy($robs[0], {}, {})
    vrobs = $rms.get_variants($robs[0])
    assert_equal 2, vrobs.count, 'expected 2 variants'
    $rms.put_data(vrobs[0], { 'QAParamDouble' => 8.8 })
    $rms.put_data(vrobs[1], { 'QAParamInteger' => 8 })
    $robs[0] = $rmstest.recipe_approval($robs[0])
    $robs[0] = $rms.get_keys($robs[0])
    $robs[0] = $rms.get_data($robs[0])
    
    # Check RTD parameters (Main recipe only)
    assert_equal 1, $robs.count, 'No rob found'    
    assert self._verify_rtd_rob_structure($robs, [{0=>[]}])
    
    # Update variant recipe contexts
    $robs[0] = $rms.create_work_copy($robs[0], {}, {})
    vrobs = $rms.get_variants($robs[0])
    $robs[0] = $rms.del_variant(vrobs[0])
    rob_v = $rms.put_variant_keys($robs[0], {'Technology'=>'20NM'})
    $rms.put_data(rob_v, { 'QAParamBool'=>true })    
    $robs[0] = $rmstest.recipe_approval($robs[0])
    $robs[0] = $rms.get_keys($robs[0])
    $robs[0] = $rms.get_data($robs[0])
    
    # Check RTD parameters (Main recipe only)
    assert_equal 1, $robs.count, 'No rob found'    
    assert self._verify_rtd_rob_structure($robs, [{0=>[]}])
    
    # delete it
    $robs = $rmstest.rms.deactivate($robs)
    assert_equal 1, $robs.count, 'Recipe not successfully deactivated'

    assert self._verify_rtd_rob_structure($robs, [{0=>[]}], :deactivated=>true)    
  end
  
  # recipe hash has same order of keys as created robs
  def _do_approval_cycle(all_recipes, rob_struct, params={})
    robs = $rmstest.recipe_create(all_recipes, params.merge(ignorelinks: true, checklinks: true))
    assert robs, 'ROBs could not be created'
    robs = $rmstest.recipe_approval(robs)
    assert robs, 'Recipes not created successfully'

    self._verify_rtd_rob_structure(robs, rob_struct, params)
    
    unless params[:nodeactivation]
      robs = $rmstest.rms.deactivate(robs)
      assert robs, 'Recipes not successfully deactivated'
      self._verify_rtd_rob_structure(robs, rob_struct, params.merge(deactivated: true))
    end
    robs
  end

  # Check RTD parameters (main and sub)
  def _verify_rtd_rob_structure(robs, rob_struct, params={})
    $log.info "wait for replication... #{@@replication_time}"
    return wait_for(sleeptime: 20, timeout: @@replication_time) {
      rob_struct.inject(true) {|res, r_hash|
        mrob = r_hash.keys.first
        srobs = r_hash.values.flatten
        res &= $rmstest.verify_rtd_recipe_parameters(robs[mrob], srobs.map {|i| robs[i]}, params)        
      }
    }
  end

  def test0390_rtd_parameters_stress
    #$rmstest.setup_litho_recipes
    imp_recipes = []
    $robs = []
    @@recipe_count.times {|i|
      time = Time.now.to_i
      imp_recipe = {
        'IMP2100/P-BF-07T007-5E15-00Q' => RmsAPI::RobObject.new(nil,
        {'ToolRecipeName' => "#{time}\\P-BF-07T007-5E15-00Q", 'RecipeType' => 'Main', 'RecipeNameSpaceID' => 'QA-TEST', 'RecipeName' => "IMP-HC-RECIPE-#{time}"},
        { 'QAParamString' => RmsAPI::DataItem.new('A', ['Tuning']), 'QAParamDouble' => RmsAPI::DataItem.new(7.0, ['Tuning']), 'QAParamInteger' => RmsAPI::DataItem.new(7, ['Tuning']), 
        'QAParamBool' => RmsAPI::DataItem.new(false, ['Tuning']), 'QAParamTime' => RmsAPI::DataItem.new(Time.now, ['Tuning']) }, nil)
      }
      $robs += $rmstest.recipe_create(imp_recipe, tool_prefix: 'IMP-HC', tool_vendor: 'VARIAN', softrev: 'TC02-1')
      imp_recipes << imp_recipe
    }
    assert $robs, 'Recipes not created successfully'
    $robs = $rmstest.recipe_approval($robs)
    assert $robs, 'Recipes not approved successfully'

    # Check RTD parameters
    assert self._verify_rtd_rob_structure($robs, @@recipe_count.times.collect {|i| {i=>[]}})
    
    $robs = $rmstest.rms.deactivate($robs)
    assert self._verify_rtd_rob_structure($robs, @@recipe_count.times.collect {|i| {i=>[]}}, deactivated: true)
  end

  def _parallel_recipe_create()
    10.times.map {|i|
      Thread.new do
        time = Time.now.to_i
        ok = true
        rmstest = RmsTest.new($env, @@eqp)
        imp_recipe = {
          'IMP2100/P-BF-07T007-5E15-00Q' => RmsAPI::RobObject.new( nil,
            { 'ToolRecipeName'=>"#{time}\\P-BF-07T007-5E15-00Q", 'RecipeType'=>'Main', 'RecipeNameSpaceID'=>'QA-TEST', 'RecipeName'=>"IMP-HC-STRESS-#{time}-#{i}"},
            { 'Dose' => 7.5E14, 'Energy' => 7.0, 'Species'=>'8', 'QAParamInteger' => 7, 'QAParamBool'=>false, 'QAParamTime'=>Time.now },
            nil),
        }
        robs = rmstest.recipe_create(imp_recipe, :tool_prefix=>'IMP-HC', :tool_vendor=>'VARIAN', :softrev=>'TC02-1')        
        sleep 2
        robs = rmstest.recipe_approval(robs) if robs
        sleep 2
        if robs
          ok &= self._verify_rtd_rob_structure(robs, [{0=>[]}])
          sleep 2
          rmstest.rms.deactivate(robs)
          ok &= self._verify_rtd_rob_structure(robs, [{0=>[]}], :deactivated=>true)        
        else
          ok = false
        end
        yield robs, ok        
      end
    }.each {|t| t.join}
  end
  
  def test0391_rtd_parameters_stress
    res = true
    self._parallel_recipe_create {|robs, ok| 
      $log.info "#{robs.inspect}: #{ok}"      
      res &= ok
    }
    assert res, '  failed to create recipes'
  end

  
end
