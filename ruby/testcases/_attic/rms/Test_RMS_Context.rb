=begin
 automated test of RMS

(c) Copyright 2011 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

 author      - daniel.steger@globalfoundries.com
 date        - 2013/07/19
=end

require_relative "Test_RMS_Setup"
require "misc"

# RMS Recipe management tests - Context propagation recipes
class Test_RMS_Context < Test_RMS_Setup
  
  def _generate_ctx_recipe(eqp)
    time = Time.now.to_i
    ctx_recipes = {}
    keys = RMS::ContextKeys.values[3..-1] #skip recipename/namespace
    
    toolrcp_name = "/test/recipe/main"
    ctx_recipes[toolrcp_name] = RmsAPI::RobObject.new( nil,
      { "ToolRecipeName"=>toolrcp_name, "RecipeType"=>"WaferFlow", "RecipeName"=>"CONTEXT-RECIPE", "RecipeNameSpaceID"=>"QA-TEST", "Equipment"=>eqp, "OperationMode"=>"RecipeSelect"},
      { "DCPPath" => "/path/to/system/dcp" },       
      [{"RecipeType"=>"Chamber_Etch", keys[0]=>"$"}])
    toolrcp_name = "/test/recipe/all"
    ctx_recipes[toolrcp_name] = RmsAPI::RobObject.new( nil,
      { "ToolRecipeName"=>toolrcp_name, "RecipeType"=>"WaferFlow", "RecipeName"=>"CONTEXT-RECIPE", "RecipeNameSpaceID"=>"QA-TEST", "OperationMode"=>"RecipeSelect"},
      { "ModuleID" => "PMX", "DCPPath" => "/path/to/system/dcp" }, 
      [])    
    toolrcp_name = "/test/recipe/override"
    linked = {"RecipeType"=>"Chamber_Etch"}
    keys.each do |k| 
      linked[k] = "#{k}.OVR"
    end
    linked["Chamber"] = "PM2"
    ctx_recipes[toolrcp_name] = RmsAPI::RobObject.new( nil,
      { "ToolRecipeName"=>toolrcp_name, "RecipeType"=>"WaferFlow", "RecipeName"=>"CONTEXT-RECIPE-OVR", "RecipeNameSpaceID"=>"QA-TEST", "OperationMode"=>"RecipeSelect"},
      { "ModuleID" => "PMX", "DCPPath" => "/path/to/system/dcp" }, 
      [linked])
    
    self._generate_ctx_subrecipe(ctx_recipes, 0, keys)
    return ctx_recipes
  end
  
      
  def _generate_ctx_subrecipe(r_hash, i, keys)
    linked = []
    if i+1 < keys.count 
      l_ctx_key = keys[i+1]
      linked = [{"RecipeType"=>"Chamber_Etch", "Equipment"=>"$", l_ctx_key=>"$"}]
      $log.info "#{linked.inspect}"
    end
    
    toolrcp_name = "/test/recipe/#{keys[i]}/0"
    r_hash[toolrcp_name] = RmsAPI::RobObject.new( nil,
      { "OperationMode"=>"RecipeSelect", "ToolRecipeName"=>toolrcp_name, "RecipeType"=>"Chamber_Etch", "RecipeName"=>"-", "RecipeNameSpaceID"=>"QA-TEST", keys[i]=>"#{keys[i]}.0"},
      { "ModuleID" => "PM1", "DCPPath" => "/path/to/system/dcp" }, 
      linked)
    
    toolrcp_name = "/test/recipe/#{keys[i]}/1"
    r_hash[toolrcp_name] = RmsAPI::RobObject.new( nil,
      { "OperationMode"=>"RecipeSelect", "ToolRecipeName"=>toolrcp_name, "RecipeType"=>"Chamber_Etch", "RecipeName"=>"-", "RecipeNameSpaceID"=>"QA-TEST", keys[i]=>"#{keys[i]}.1"},
      { "ModuleID" => "PM2", "DCPPath" => "/path/to/system/dcp" }, 
      linked)
    
    toolrcp_name = "/test/recipe/#{keys[i]}/OVR"
    c_keys = { "OperationMode"=>"RecipeSelect", "ToolRecipeName"=>toolrcp_name, "RecipeType"=>"Chamber_Etch", "RecipeName"=>"-", "RecipeNameSpaceID"=>"QA-TEST", 
        keys[i]=>"#{keys[i]}.OVR"}
    if i == 0  
      c_keys["Chamber"] = "PM2"
    end
      
    r_hash[toolrcp_name] = RmsAPI::RobObject.new( nil,
      c_keys,
      { "ModuleID" => "PM2", "DCPPath" => "/path/to/system/dcp" }, 
      linked)
    self._generate_ctx_subrecipe(r_hash, i+1, keys) if linked != []
  end
  
    
  # For Context tests
  def test0900_ctx_setup
    $setup_ok = false
    #
    # check if recipes with context exist
    robs = $rms.find_object_list({"Department"=>"MSS", "ToolPrefix"=>"BST", "ToolVendor"=>"LAM", "SoftRev"=>"TC09-1"})
    #$log.info "#{robs}"
    if robs.count != 24
      assert $rms.deactivate(robs), "failed to remove old setup" unless robs.count == 0
      
      recipe = self._generate_ctx_recipe("BST2100")
      $robs = $rmstest.recipe_create(recipe, :softrev=>"TC09-1", :recipe_body=>nil, :ignorelinks=>true, :checklinks=>false)
      $robs = $rmstest.recipe_approval($robs)
      assert $robs, "Recipes not created successfully"    
      #
    end
    $setup_ok = true
  end
  
  
  def _generate_fixed_recipe(eqp)
    time = Time.now.to_i
    ctx_recipes = {}
    keys = RMS::ContextKeys.values[3..-1] #skip recipename/namespace
    toolrcp_name = "/test/recipe/fixed"
    ctx_recipes[toolrcp_name] = RmsAPI::RobObject.new( nil,
      { "ToolRecipeName"=>toolrcp_name, "RecipeType"=>"WaferFlow", "RecipeName"=>"FIXED-RECIPE", "RecipeNameSpaceID"=>"QA-TEST", "Equipment"=>eqp, "OperationMode"=>"RecipeSelect"},
      { "DCPPath" => "/path/to/system/dcp" },       
      [{"RecipeType"=>"Chamber_Etch", keys[0]=>"#{keys[0]}.A", "ToolRecipeName"=>"/test/recipe/#{keys[0]}/A"}])

    self._generate_fixed_subrecipe(eqp, ctx_recipes, 0, keys)
    return ctx_recipes
  end
  
  def _generate_fixed_subrecipe(eqp, r_hash, i, keys)
    linked = []
    if i+1 < keys.count 
      l_ctx_key = keys[i+1]
      val = "#{l_ctx_key}.A"
      linked = [{"RecipeType"=>"Chamber_Etch", "Equipment"=>eqp, l_ctx_key=>val, "ToolRecipeName"=>"/test/recipe/#{l_ctx_key}/A"}]
      $log.info "#{linked.inspect}"
    end
    
    toolrcp_name = "/test/recipe/#{keys[i]}/A"
    r_hash[toolrcp_name] = RmsAPI::RobObject.new( nil,
      { "OperationMode"=>"RecipeSelect", "ToolRecipeName"=>toolrcp_name, "RecipeType"=>"Chamber_Etch", "RecipeName"=>"-", "RecipeNameSpaceID"=>"QA-TEST", keys[i]=>"#{keys[i]}.A"},
      { "ModuleID" => "PM1", "DCPPath" => "/path/to/system/dcp" }, 
      linked)
    
    self._generate_fixed_subrecipe(eqp, r_hash, i+1, keys) if linked != []
  end


  
  def test0901_context
    tool_rcp_names = ["/test/recipe/main"]
    ctx = Hash[*[RMS::ContextKeys.keys[3..-1].map {|k| 
          v = RMS::ContextKeys[k]
          tool_rcp_names << "/test/recipe/#{v}/1"  
          [k, "#{v}.1"]
        }]]
    ctx = ctx.merge({"RecipeName"=>"CONTEXT-RECIPE", "RecipeNameSpaceID"=>"QA-TEST", "Equipment"=>"BST2100"})
    $data = $ril.get_recipe_payload_request(ctx)
    assert $rmstest.verify_tool_recipe_names($data, tool_rcp_names), "failed to validate tool recipe names"
  end
  
  def test0902_context
    tool_rcp_names = ["/test/recipe/main"]
    ctx = Hash[*[RMS::ContextKeys.keys[3..-1].map {|k| 
          v = RMS::ContextKeys[k]
          tool_rcp_names << "/test/recipe/#{v}/0"
          [k, "#{v}.0"]
        }]]
    ctx = ctx.merge({"RecipeName"=>"CONTEXT-RECIPE", "RecipeNameSpaceID"=>"QA-TEST", "Equipment"=>"BST2100"})
    $data = $ril.get_recipe_payload_request(ctx)
    assert $rmstest.verify_tool_recipe_names($data, tool_rcp_names), "failed to validate tool recipe names"
  end
  
  def test0903_context
    tool_rcp_names = ["/test/recipe/main"]
    ctx = Hash[*[RMS::ContextKeys.keys[3..-1].map {|k| 
          v = RMS::ContextKeys[k]
          tool_rcp_names << "/test/recipe/#{v}/x"  
          [k, "#{v}.x"]
        }]]
    ctx = ctx.merge({"RecipeName"=>"CONTEXT-RECIPE", "RecipeNameSpaceID"=>"QA-TEST", "Equipment"=>"BST2100"})
    assert_raises RMS::RilError do
      $ril.get_recipe_payload_request(ctx)
    end
  end
  
  def test0904_context_override
    tool_rcp_names = ["/test/recipe/override"]
    ctx = Hash[*[RMS::ContextKeys.keys[3..-1].map {|k| 
          v = RMS::ContextKeys[k]
          tool_rcp_names << "/test/recipe/#{v}/OVR"
          [k, "#{v}.0"]
        }]]
    ctx = ctx.merge({"RecipeName"=>"CONTEXT-RECIPE-OVR", "RecipeNameSpaceID"=>"QA-TEST", "Equipment"=>"BST2100"})
    $data = $ril.get_recipe_payload_request(ctx)
    assert $rmstest.verify_tool_recipe_names($data, tool_rcp_names), "failed to validate tool recipe names"
  end
  
  def test0905_context_override
    tool_rcp_names = ["/test/recipe/override"]
    ctx = Hash[*[RMS::ContextKeys.keys[3..-1].map {|k| 
          v = RMS::ContextKeys[k]
          tool_rcp_names << "/test/recipe/#{v}/OVR"
          [k, "#{v}.1"]
        }]]
    ctx = ctx.merge({"RecipeName"=>"CONTEXT-RECIPE-OVR", "RecipeNameSpaceID"=>"QA-TEST", "Equipment"=>"BST2100"})
    $data = $ril.get_recipe_payload_request(ctx)
    assert $rmstest.verify_tool_recipe_names($data, tool_rcp_names), "failed to validate tool recipe names"
  end
  
  def test0906_context_override
    tool_rcp_names = ["/test/recipe/override"]
    ctx = Hash[*[RMS::ContextKeys.keys[3..-1].map {|k| 
          v = RMS::ContextKeys[k]
          tool_rcp_names << "/test/recipe/#{v}/OVR"
          [k, "#{v}.OVR"]
        }]]
    ctx = ctx.merge({"RecipeName"=>"CONTEXT-RECIPE-OVR", "RecipeNameSpaceID"=>"QA-TEST", "Equipment"=>"BST2100"})
    $data = $ril.get_recipe_payload_request(ctx)
    assert $rmstest.verify_tool_recipe_names($data, tool_rcp_names), "failed to validate tool recipe names"
  end
  
  def test0910_chained_links
    recipename, trcp_names = self._generate_chained_recipe()
    $log.info "context=<#{recipename}, #{trcp_names.inspect}>"
    10.times do
      $data = $ril.get_recipe_payload_request({"RecipeNameSpaceID"=>"QA-TEST", "RecipeName"=>recipename, "Equipment"=>"MDX2900"})
      assert $rmstest.verify_tool_recipe_names($data, trcp_names), "failed to validate tool recipe names"
    end
  end
  
  def test0911_chained_3rdlevel_links
    recipename, trcp_names = self._generate_chained_3rdlevel_recipe()
    $log.info "context=<#{recipename}, #{trcp_names.inspect}>"
    10.times do
      $data = $ril.get_recipe_payload_request({"RecipeNameSpaceID"=>"QA-TEST", "RecipeName"=>recipename, "Equipment"=>"MDX2900"})
      assert $rmstest.verify_tool_recipe_names($data, trcp_names), "failed to validate tool recipe names"
    end
  end
  
  def test0912_chained_3rdlevel_onhold
    recipename, trcp_names = self._generate_chained_3rdlevel_recipe()
    $log.info "context=<#{recipename}, #{trcp_names.inspect}>"
    
    # find main rob
    rob = $rms.find_object_with_data({"RecipeNameSpaceID"=>"QA-TEST", "RecipeName"=>recipename})
    assert $rms.set_onhold(rob, ["DEFAULT", "TOOL_REASON"]), "failed to set hold reason"

    assert $ril.get_recipe_payload_request({"RecipeNameSpaceID"=>"QA-TEST", "RecipeName"=>recipename, "Equipment"=>"MDX2900"}, {rawxml: true})
    assert_equal 'RIL-RECIPE_ON_HOLD', $ril._service_response[:Fault][:detail][:RILException][:fault][:errorCode]
  end
  
  def test0913_chained_3rdlevel_onhold_subrob
    recipename, trcp_names = self._generate_chained_3rdlevel_recipe()
    $log.info "context=<#{recipename}, #{trcp_names.inspect}>"
    
    # find main rob
    rob = $rms.find_object_with_data({"RecipeNameSpaceID"=>"QA-TEST", "ToolRecipeName"=>trcp_names.first})
    assert $rms.set_onhold(rob, ["DEFAULT", "TOOL_REASON"]), "failed to set hold reason"

    assert $ril.get_recipe_payload_request({"RecipeNameSpaceID"=>"QA-TEST", "RecipeName"=>recipename, "Equipment"=>"MDX2900"}, {rawxml: true})
    assert_equal 'RIL-RECIPE_ON_HOLD', $ril._service_response[:Fault][:detail][:RILException][:fault][:errorCode] 
  end
  
  def test0920_cycle_links
    recipename = self._generate_recipe_cycle()

    assert $ril.get_recipe_payload_request({"RecipeNameSpaceID"=>"QA-TEST", "RecipeName"=>recipename, "Equipment"=>"ALC2350"}, {rawxml: true})
    assert_equal 'RIL-CYCLE_IN_PAYLOAD', $ril._service_response[:Fault][:detail][:RILException][:fault][:errorCode]
  end
  
  # TODO: create recipe automatically. Currently it has to be created manually.
  def test0921_wrong_link_name
    recipename = "QA-RECIPE-LINK"
    e = assert_raises RMS::RilError do    
      $ril.get_recipe_payload_request({"RecipeNameSpaceID"=>"QA-TEST", "RecipeName"=>recipename, "Equipment"=>"BST2100"})
    end
     assert_match /Recipe model violation: Illegal link name 'Recipes'/, e.message
  end
  
  def test0930_fixed_setup
    $setup_ok = false
    #
    # check if recipes with context exist
    robs = $rms.find_object_list( {"Department"=>"MSS", "ToolPrefix"=>"BST", "ToolVendor"=>"LAM", "SoftRev"=>"TC09-2"})
    return if robs.count == 8
    assert $rms.deactivate(robs), "failed to remove old setup" unless robs.count == 0
    
    recipe = self._generate_fixed_recipe("BST2100")
    $robs = $rmstest.recipe_create(recipe, :softrev=>"TC09-2", :recipe_body=>nil, :ignorelinks=>false, :checklinks=>true)
    $robs = $rmstest.recipe_approval($robs)
    assert $robs, "Recipes not created successfully"
    #
    $setup_ok = true
  end

  
  def test0931_fixed_links
    tool_rcp_names = ["/test/recipe/fixed"]
    RMS::ContextKeys.keys[3..-1].each do |k| 
      v = RMS::ContextKeys[k]
      tool_rcp_names << "/test/recipe/#{v}/A"
    end

    $data = $ril.get_recipe_payload_request("RecipeName"=>"FIXED-RECIPE", "RecipeNameSpaceID"=>"QA-TEST", "Equipment"=>"BST2100")
    assert $rmstest.verify_tool_recipe_names($data, tool_rcp_names), "failed to validate tool recipe names"
  end
  
  # Allow to add multiple times same link > 10 links
  def _generate_chained_recipe
    time = Time.now.to_i
    recipename = "MDX-RECIPE-#{time}"
    all_recipes = {
      "MDX2900/CH/1P-AL28K" => RmsAPI::RobObject.new( nil,
        { "ToolRecipeName"=>"1P-AL28K-#{time}", "RecipeType"=>"Module_PVD", 
          "RecipeName"=>"1P-AL28K"},
        { "ModuleID"=>"CH4,CH5", "ConditioningActive"=>"YES","ConditioningType"=>"QATest","ConditioningMaxIdleTime"=>77}, nil),
      "MDX2900/CH/1P-CHABCOOL" => RmsAPI::RobObject.new( nil,
        { "ToolRecipeName"=>"1P-CHABCOOL-#{time}", "RecipeType"=>"Module_Cool", 
          "RecipeName"=>"1P-CHABCOOL"},
        { "ModuleID" => "CHB"}, nil),
      "MDX2900/CH/1P-DEGAS300C" => RmsAPI::RobObject.new( nil,
        { "ToolRecipeName"=>"1P-DEGAS300C-#{time}", "RecipeType"=>"Module_Degas", 
          "RecipeName"=>"1P-DEGAS300C"},
        { "ModuleID" => "CHE,CHF"}, nil),
      "MDX2900/CH/1P-PCXTLP50" => RmsAPI::RobObject.new( nil,
        { "ToolRecipeName"=>"1P-PCXTLP50-#{time}", "RecipeType"=>"Module_PCXT", 
          "RecipeName"=>"1P-PCXTLP50"},
        { "ModuleID" => "CHC,CHD"}, nil),
      "MDX2900/CH/1P-TAN500" => RmsAPI::RobObject.new( nil,
        { "ToolRecipeName"=>"1P-TAN500-#{time}", "RecipeType"=>"Module_PVD", 
          "RecipeName"=>"1P-TAN500"},
        { "ModuleID" => "CH1", "ConditioningActive"=>"NO","ConditioningMaxIdleTime"=>30}, nil),
      "MDX2900/CH/1P-TI250" => RmsAPI::RobObject.new( nil,
        { "ToolRecipeName"=>"1P-TI250-#{time}", "RecipeType"=>"Module_PVD", 
          "RecipeName"=>"1P-TI250"},
        { "ModuleID" => "CH2", "ConditioningActive"=>"YES","ConditioningType"=>"QATest","ConditioningMaxIdleTime"=>60}, nil),
      "MDX2900/CH/1P-TIN250" => RmsAPI::RobObject.new( nil,
        { "ToolRecipeName"=>"1P-TIN250-#{time}", "RecipeType"=>"Module_PVD", 
          "RecipeName"=>"1P-TIN250"},
        { "ModuleID" => "CH3"}, nil),
      "MDX2900/F-P-20NMTERMD-28KAL" => RmsAPI::RobObject.new( nil,
        { "ToolRecipeName" => "F-P-20NMTERMD-28KAL", "RecipeType"=>"WaferFlow", "RecipeNameSpaceID"=>"QA-TEST", "RecipeName"=>recipename },
        { "ModuleCombination" => "[CHE:CHF][CHC:CHD]CHA:CH1:CH2:CH3[CH4:CH5]CHB"},
        [{"ToolRecipeName"=>"1P-AL28K-#{time}", "Equipment"=>"$"},
         {"ToolRecipeName"=>"1P-CHABCOOL-#{time}", "Equipment"=>"$"},
         {"ToolRecipeName"=>"1P-DEGAS300C-#{time}", "Equipment"=>"$"},
         {"ToolRecipeName"=>"1P-PCXTLP50-#{time}", "Equipment"=>"$"},
         {"ToolRecipeName"=>"1P-TAN500-#{time}", "Equipment"=>"$"},
         {"ToolRecipeName"=>"1P-TI250-#{time}", "Equipment"=>"$"},
         {"ToolRecipeName"=>"1P-TIN250-#{time}", "Equipment"=>"$"},
         {"ToolRecipeName"=>"1P-CHABCOOL-#{time}", "Equipment"=>"$"},
         {"ToolRecipeName"=>"1P-AL28K-#{time}", "Equipment"=>"$"},
         {"ToolRecipeName"=>"1P-PCXTLP50-#{time}", "Equipment"=>"$"},
         {"ToolRecipeName"=>"1P-DEGAS300C-#{time}", "Equipment"=>"$"},
         {"ToolRecipeName"=>"1P-TI250-#{time}", "Equipment"=>"$"},
         {"ToolRecipeName"=>"1P-TAN500-#{time}", "Equipment"=>"$"},         
         {"ToolRecipeName"=>"1P-TIN250-#{time}", "Equipment"=>"$"}
        ])
    }
    # tool recipe order
    trcp_names = ["F-P-20NMTERMD-28KAL"]
    all_recipes["MDX2900/F-P-20NMTERMD-28KAL"].linked_contexts.each {|l| trcp_names << l["ToolRecipeName"]}
    
    # check if recipes with context exist
    robs = $rms.find_object_list( {"Department"=>"MSS", "ToolVendor"=>"AMAT", "ToolPrefix"=>"MDX", "SoftRev"=>"TC02-6"}, :with_keys=>true)
    if robs.count == 8
      return robs.find {|r| r.context["KnownBySiView"] == "TRUE" }.context["RecipeName"], trcp_names
    elsif robs.count > 0
      assert $rms.deactivate(robs), "failed to remove old setup"
    end
    
    $robs = $rmstest.recipe_create(all_recipes, :tool_vendor=>"AMAT", :tool_prefix=>"MDX", :softrev=>"TC02-6", :ignorelinks=>true, :checklinks=>true)
    $robs = $rmstest.recipe_approval($robs)
    assert $robs, "Recipes not created successfully"
    return recipename, trcp_names
  end
  
  def _generate_chained_3rdlevel_recipe
    time = Time.now.to_i
    recipename = "MDX-RECIPE-#{time}"
    all_recipes = {
      "MDX2900/CH/1P-AL28K" => RmsAPI::RobObject.new( nil,
        { "ToolRecipeName"=>"1P-AL28K-#{time}", "RecipeType"=>"Module_PVD", 
          "RecipeName"=>"1P-AL28K"},
        { "ModuleID"=>"CH4,CH5", "ConditioningActive"=>"YES","ConditioningType"=>"QATest","ConditioningMaxIdleTime"=>77}, nil),
      "MDX2900/CH/1P-CHABCOOL" => RmsAPI::RobObject.new( nil,
        { "ToolRecipeName"=>"1P-CHABCOOL-#{time}", "RecipeType"=>"Module_Cool", 
          "RecipeName"=>"1P-CHABCOOL"},
        { "ModuleID" => "CHB"}, nil),
      "MDX2900/CH/1P-DEGAS300C" => RmsAPI::RobObject.new( nil,
        { "ToolRecipeName"=>"1P-DEGAS300C-#{time}", "RecipeType"=>"Module_Degas", 
          "RecipeName"=>"1P-DEGAS300C"},
        { "ModuleID" => "CHE,CHF"}, 
        [{"ToolRecipeName"=>"1P-PCXTLP50-#{time}", "Equipment"=>"$"},
         {"ToolRecipeName"=>"1P-TAN500-#{time}", "Equipment"=>"$"},
         {"ToolRecipeName"=>"1P-TI250-#{time}", "Equipment"=>"$"},
         {"ToolRecipeName"=>"1P-TIN250-#{time}", "Equipment"=>"$"}]
      ),
      "MDX2900/CH/1P-PCXTLP50" => RmsAPI::RobObject.new( nil,
        { "ToolRecipeName"=>"1P-PCXTLP50-#{time}", "RecipeType"=>"Module_PCXT", 
          "RecipeName"=>"1P-PCXTLP50"},
        { "ModuleID" => "CHC,CHD"}, nil),
      "MDX2900/CH/1P-TAN500" => RmsAPI::RobObject.new( nil,
        { "ToolRecipeName"=>"1P-TAN500-#{time}", "RecipeType"=>"Module_PVD", 
          "RecipeName"=>"1P-TAN500"},
        { "ModuleID" => "CH1", "ConditioningActive"=>"NO","ConditioningMaxIdleTime"=>30}, nil),
      "MDX2900/CH/1P-TI250" => RmsAPI::RobObject.new( nil,
        { "ToolRecipeName"=>"1P-TI250-#{time}", "RecipeType"=>"Module_PVD", 
          "RecipeName"=>"1P-TI250"},
        { "ModuleID" => "CH2", "ConditioningActive"=>"YES","ConditioningType"=>"QATest","ConditioningMaxIdleTime"=>60}, nil),
      "MDX2900/CH/1P-TIN250" => RmsAPI::RobObject.new( nil,
        { "ToolRecipeName"=>"1P-TIN250-#{time}", "RecipeType"=>"Module_PVD", 
          "RecipeName"=>"1P-TIN250"},
        { "ModuleID" => "CH3"}, nil),
      "MDX2900/F-P-20NMTERMD-28KAL" => RmsAPI::RobObject.new( nil,
        { "ToolRecipeName" => "F-P-20NMTERMD-28KAL", "RecipeType"=>"WaferFlow", "RecipeNameSpaceID"=>"QA-TEST", "RecipeName"=>recipename },
        { "ModuleCombination" => "[CHE:CHF][CHC:CHD]CHA:CH1:CH2:CH3[CH4:CH5]CHB"},
        [{"ToolRecipeName"=>"1P-AL28K-#{time}", "Equipment"=>"$"},
         {"ToolRecipeName"=>"1P-CHABCOOL-#{time}", "Equipment"=>"$"},
         {"ToolRecipeName"=>"1P-DEGAS300C-#{time}", "Equipment"=>"$"}         
        ])
    }
    # tool recipe order
    trcp_names = ["F-P-20NMTERMD-28KAL"]
    all_recipes["MDX2900/F-P-20NMTERMD-28KAL"].linked_contexts.each {|l| trcp_names << l["ToolRecipeName"]}
    all_recipes["MDX2900/CH/1P-DEGAS300C"].linked_contexts.each {|l| trcp_names << l["ToolRecipeName"]}
    
    # check if recipes with context exist
    robs = $rms.find_object_list( {"Department"=>"MSS", "ToolVendor"=>"AMAT", "ToolPrefix"=>"MDX", "RecipeNameSpaceID"=>"QA-TEST", "SoftRev"=>"TC02-6"}, :with_keys=>true)
        
    if robs.count == 8      
      robs = $rms.set_onhold(robs, []) # release any holds
      return robs.find {|r| r.context["KnownBySiView"] == "TRUE" }.context["RecipeName"], trcp_names
    elsif robs.count > 0
      assert $rms.deactivate(robs), "failed to remove old setup"
    end
    
    $robs = $rmstest.recipe_create(all_recipes, :tool_vendor=>"AMAT", :tool_prefix=>"MDX", :softrev=>"TC02-6", :ignorelinks=>true, :checklinks=>true)
    $robs = $rmstest.recipe_approval($robs)
    assert $robs, "Recipes not created successfully"
    return recipename, trcp_names
  end
  
    
  def _generate_recipe_cycle()
    time = Time.now.to_i
    recipename = "TEL-RECIPE-#{time}"
    tel_recipe = {
      "ALC2350/Spinner%2FBCT%2FRUN-CK" => RmsAPI::RobObject.new( nil,
        { "ToolRecipeName"=>"Spinner/BCT/QUAL/04N1-HM14-200-0-1", "RecipeType"=>"Spinner", "Equipment"=>"ALC2350", "ProductID_EC"=>"RIL-PRODUCT.01"},
        { "ModuleID" => "PM2", "DCPPath" => "/path/to/system/dcp" }, 
        [{"RecipeType"=>"Spinner", "Equipment"=>"$", "ToolRecipeName"=>"Spinner/BCT/QUAL/04N1-HM14-200-0-2"}]),
      "ALC2350/Spinner%2FBCT%2FTELRUN-CK" => RmsAPI::RobObject.new( nil,
        { "ToolRecipeName"=>"Spinner/BCT/QUAL/04N1-HM14-200-0-2", "RecipeType"=>"Spinner", "Equipment"=>"ALC2350", "ProductID_EC"=>"RIL-PRODUCT.01"},
        { "ModuleID" => "PM2", "DCPPath" => "/path/to/system/dcp" }, 
        [{"RecipeType"=>"Spinner", "Equipment"=>"$", "ToolRecipeName"=>"Spinner/BCT/QUAL/04N1-HM14-200-0-3"}]),
      "ALC2350/Spinner%2FBCT%2FQUAL%2F04N1-HM14-200-0" => RmsAPI::RobObject.new( nil,
        { "ToolRecipeName"=>"Spinner/BCT/QUAL/04N1-HM14-200-0-3", "RecipeType"=>"Spinner", "Equipment"=>"ALC2350", "ProductID_EC"=>"RIL-PRODUCT.01"},
        { "ModuleID" => "PM2", "DCPPath" => "/path/to/system/dcp" }, 
        [{"RecipeType"=>"Spinner", "Equipment"=>"$", "ToolRecipeName"=>"Spinner/BCT/QUAL/04N1-HM14-200-0-1"}]),
      "ALC2350/WaferFlow%2FPRD%2FTD_20NM%2FPHM14200----TD0" => RmsAPI::RobObject.new( nil,
        { "ToolRecipeName" => "WaferFlow/PRD/TD_20NM/PHM14200----TD0", "RecipeType"=>"WaferFlow", "RecipeNameSpaceID"=>"QA-TEST", "RecipeName"=>recipename },
        { "ModuleCombination" => "TRK"},
        [{"RecipeType"=>"Spinner", "Equipment"=>"$", "ToolRecipeName"=>"Spinner/BCT/QUAL/04N1-HM14-200-0-1"}])
    }
    return $rmstest.find_or_create_recipe({"ToolVendor"=>"TEL", "ToolPrefix"=>"ALC", "SoftRev"=>"TC00-3"}, tel_recipe, "WaferFlow/PRD/TD_20NM/PHM14200----TD0")
  end

  def test_overide_parent_context
  end
  
  def test_override_child_context
  end
  
  def test_override_parent_child
  end
  
  def test_contradict_context
  end
  
  def test_other_context_chamber
  end
  
end