=begin
 automated test of setup.FC API 

(c) Copyright 2011 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

 author      - daniel.steger@globalfoundries.com
 date        - 2012/08/28
=end

require "RubyTestCase"
require "rmstest"


# RMS Recipe management tests
class Test_RMS_Perf < RubyTestCase
  Description = "Test RMS Performance tests"

  @@count = 10
  @@compares = 8
  @@release = "Sprint5"
  @@eqps =  "THK2102 THK2150 THK2500 THK2501 THK2502 THK2600 THK2601 THK2650 THK2800 UVC2500" #"ETX002"
  @@loop_count = 2
  @@robdef_keys =  {"Department"=>"MSS", "ToolPrefix"=>"IMP-MC", "ToolVendor"=>"VARIAN", "RecipeType"=>"Main"}
  @@upload_eqp = "IMP2200"

  @@recipe_count = 20
  @@replication_time = 60
  
  @@lam_recipe = {
    "Q-PM2-PC-MECH.rcp" => RmsAPI::RobObject.new( nil, 
      { "ToolRecipeName"=>"d:\\Lam\\data\\PM2\\Recipes\\Q-PM2-PC-MECH.rcp", "RecipeType"=>"Chamber"}, 
      { "ModuleID" => "PM2", "DCPPath" => "/path/to/system/dcp" }, nil),
    "Q-PM2-PC-MECH.flw" => RmsAPI::RobObject.new( nil, 
      { "ToolRecipeName" => "d:\\Lam\\data\\Flows\\Q-PM2-PC-MECH.flw", "RecipeType"=>"WaferFlow", "RecipeNameSpaceID"=>"QA-TEST", "RecipeName"=>"LAM-RECIPE-#{Time.now.to_i}" },
      { "ModuleCombination" => "PM2"},
      [{"RecipeType"=>"Chamber", "ToolRecipeName"=>"d:\\Lam\\data\\PM2\\Recipes\\Q-PM2-PC-MECH.rcp"}]),
  }
  
  def setup
    super
    $setup_ok = ($setup_ok != false)
    assert $setup_ok, "test setup is not correct"
    @@eqps = @@eqps.split unless @@eqps.kind_of?(Array)
    $rmstest ||= RmsTest.new($env, @@eqps.first) unless $rmstest and $rmstest.env == $env
    $ril = $rmstest.ril
    $rms = $rmstest.rms
  end

  
  # config/preparation task

  def test900_get_payload
    logfile = File.open("log/rms-stats-#{Time.now.to_i}.log", "w")
    #contexts = $rms.get_reg_entries(:filters=>{"Department"=>"LIT", "RecipeType"=>"WaferFlow", "ToolPrefix"=>"ALT", "RecipePurpose"=>"ToolQual", "KnownBySiView"=>"TRUE"}, :columns=>["RecipeNameSpaceID", "RecipeName"])
    contexts = $rms.get_reg_entries(:filters=>{"Department"=>"DIF", "RecipeType"=>"Main", "KnownBySiView"=>"TRUE"}, :columns=>["RecipeNameSpaceID", "RecipeName"])
    part = contexts.count / @@count
    (0..@@compares).each do |compares|
      threads = @@count.times.collect {|i|
        Thread.new {
          get_payload = []
          compare_recipes = []
          ril = RMS::RIL.new($env, @@eqps[i])
          contexts[i*part,part].each {|(rspace, rname, handle)|
            payload = ril.get_recipe_payload_request({"RecipeNameSpaceID" => rspace, "RecipeName" => rname, "Equipment"=>"ALT2600"})
            get_payload << ril.responsetime
            # Compare threads
            if i < compares
              ril.compare_recipes(payload)
              compare_recipes << ril.responsetime
            end
            #result << ril.responsetime
          }
          ril.mq_request.disconnect
          ril.mq_response.disconnect
          [get_payload, compare_recipes]
        }
      }
      
      get_payload = []
      compare_recipes = []
      threads.each do |t| 
        v1, v2 = t.value
        get_payload += v1
        compare_recipes += v2
      end
      
      logfile << "compare threads=#{compares}/#{@@count}\n"
      logfile << "get_payload count=#{get_payload.count} avg=#{get_payload.average} min=#{get_payload.min} max=#{get_payload.max}\n"
      logfile << "compare_recipes count=#{compare_recipes.count} avg=#{compare_recipes.average} min=#{compare_recipes.min} max=#{compare_recipes.max}\n"
    end
    logfile.close
  end
  
  def test901_rmsservices
    logfile = File.open("log/rms-stats-#{Time.now.to_i}.log", "w")
    contexts = $rms.get_reg_entries(:filters=>{"Department"=>"DIF", "ToolPrefix"=>"IMP-HC", "KnownBySiView"=>"TRUE"}, :columns=>[])
    
    recipe_file = "testcasedata/recipes/IMP2100/P-BF-07T007-5E15-00Q"
    (1..10).each do |count|
      part = contexts.count / count
      threads = count.times.collect {|i|
        Thread.new {
          create_work = []
          delete_work = []
          rms = RmsAPI::Session.new($env)
          contexts[i*part,10].each do |handle|
            org_rob = RmsAPI::RobObject.new(handle.first)
            $log.info "#{org_rob}"            
            rob = rms.create_work_copy(org_rob, {}, {}, :recipe_file=>recipe_file)
            create_work << rms.responsetime
            rms.delete_work(rob)  
            delete_work << rms.responsetime
          end
          rms.close
          [create_work, delete_work]
        }
      }
      create_work = []
      delete_work = []
      threads.each do |t| 
        v1, v2 = t.value
        create_work += v1
        delete_work += v2
      end
      
      logfile << "threads=#{count}\n"
      logfile << "create_work count=#{create_work.count} avg=#{create_work.average} min=#{create_work.min} max=#{create_work.max}\n"
      logfile << "delete_work count=#{delete_work.count} avg=#{delete_work.average} min=#{delete_work.min} max=#{delete_work.max}\n"
    end
    logfile.close
  end
  
  def test902_mq_corba_rob_data
    logfile = File.open("log/rms-corba-#{Time.now.to_i}.log", "w")
    contexts = $rms.get_reg_entries(:filters=>{"Department"=>"DIF", "ToolPrefix"=>"IMP-HC", "KnownBySiView"=>"TRUE"}, :columns=>[])
    
    (1..5).each do |count|
      part = contexts.count / count
      threads = count.times.collect {|i|
        Thread.new {
          get_keys = []
          get_data = []
          rms = RmsAPI::Session.new($env)
          contexts[i*part,10].each do |handle|
            rob = RmsAPI::RobObject.new(handle.first)
            $log.info "#{rob}"            
            rms.get_keys(rob)
            get_keys << rms.responsetime
            rms.get_data(rob)  
            get_data << rms.responsetime
          end
          rms.close
          [get_keys, get_data]
        }
      }
      get_keys = []
      get_data = []
      threads.each do |t| 
        v1, v2 = t.value
        get_keys += v1
        get_data += v2
      end
      
      logfile << "threads=#{count}\n"
      logfile << "get_keys count=#{get_keys.count} avg=#{get_keys.average} min=#{get_keys.min} max=#{get_keys.max}\n"
      logfile << "get_data count=#{get_data.count} avg=#{get_data.average} min=#{get_data.min} max=#{get_data.max}\n"
    end
    logfile.close
  end
  
  def test903_mq_corba_rob_data
    logfile = File.open("log/rms-corba-#{Time.now.to_i}.log", "w")
    tool_vend= ["AMAT", "ASM", "ASML", "AXCELIS", "VARIAN", "DNS", "EBARA", "LAM", "KLA", "NOVA", "NOVELLUS", "SOKUDO", "TEL", "VARIAN"]
    
    (1..5).each do |count|
      part = tool_vend.count / count
      threads = count.times.collect {|i|
        Thread.new {
          get_reg_entries = []          
          rms = RmsAPI::Session.new($env)
          
          tool_vend[i*part,10].each do |vendor|
            robs = rms.get_reg_entries(:filters=>{"ToolVendor"=>vendor, "state"=>"ACTIVE"}, 
              :columns=>[:RecipeType, :Department, :state, :ToolPrefix, :ToolVendor, :CEIRecipeType, :Equipment, :ProductID_EC, :RecipeName, :RecipeNameSpaceID, :RecipePurpose])
            get_reg_entries << rms.responsetime
            $log.info "#{robs.count}"
          end
          rms.close
          get_reg_entries
        }
      }
      get_reg_entries = []
      threads.each do |t| 
        get_reg_entries += t.value
      end
      
      logfile << "threads=#{count}\n"
      logfile << "get_reg_entries count=#{get_reg_entries.count} avg=#{get_reg_entries.average} min=#{get_reg_entries.min} max=#{get_reg_entries.max}\n"      
    end
    logfile.close
  end
end
