=begin
 automated test of setup.FC API 

(c) Copyright 2011 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

 author      - daniel.steger@globalfoundries.com
 date        - 2012/08/28
=end

require "RubyTestCase"
require "rmstest"


# RMS Recipe management tests
class Test_RMS_RecipeSetup < RubyTestCase
  Description = "Test RMS Setup Interface"

  @@release = "Sprint5"
  @@eqp =  "THK2101" #"ETX002"
  @@loop_count = 2
  @@robdef_keys =  {"Department"=>"MSS", "ToolPrefix"=>"IMP-MC", "ToolVendor"=>"VARIAN", "RecipeType"=>"Main"}
  @@upload_eqp = "IMP2200"

  @@recipe_count = 20
  @@replication_time = 60
  
  @@lam_recipe = {
    "Q-PM2-PC-MECH.rcp" => RmsAPI::RobObject.new( nil, 
      { "ToolRecipeName"=>"d:\\Lam\\data\\PM2\\Recipes\\Q-PM2-PC-MECH.rcp", "RecipeType"=>"Chamber"}, 
      { "ModuleID" => "PM2", "DCPPath" => "/path/to/system/dcp" }, nil),
    "Q-PM2-PC-MECH.flw" => RmsAPI::RobObject.new( nil, 
      { "ToolRecipeName" => "d:\\Lam\\data\\Flows\\Q-PM2-PC-MECH.flw", "RecipeType"=>"WaferFlow", "RecipeNameSpaceID"=>"QA-TEST", "RecipeName"=>"LAM-RECIPE-#{Time.now.to_i}" },
      { "ModuleCombination" => "PM2"},
      [{"RecipeType"=>"Chamber", "ToolRecipeName"=>"d:\\Lam\\data\\PM2\\Recipes\\Q-PM2-PC-MECH.rcp"}]),
  }
  
  # config/preparation task
  def self.startup
    $rmstest ||= RmsTest.new($env, @@eqp)
    $ril = $rmstest.ril
    $rms = $rmstest.rms
  end

  def _generate_lam_recipe1(rnamespace, rname)
    time = Time.now.to_i
    lam_recipe = {
      "BST2100/PM2/Q-PM2-PC-MECH.rcp" => RmsAPI::RobObject.new( nil,
        { "ToolRecipeName"=>"d:\\Lam\\data\\PM2\\Recipes\\Q-PM2-PC-MECH-#{time}.rcp", "RecipeType"=>"Chamber"},
        { "ModuleID" => "PM2" }, nil),
      "BST2100/Q-PM2-PC-MECH.flw" => RmsAPI::RobObject.new( nil,
        { "ToolRecipeName" => "d:\\Lam\\data\\Flows\\Q-PM2-PC-MECH.flw", "RecipeType"=>"WaferFlow", "RecipeNameSpaceID"=>rnamespace, "RecipeName"=>rname },
        { "ModuleCombination" => "PM2"},
        [{"RecipeType"=>"Chamber", "ToolRecipeName"=>"d:\\Lam\\data\\PM2\\Recipes\\Q-PM2-PC-MECH.rcp"}]),
    }
    return lam_recipe
  end
  
  def _generate_lam_recipe2(rnamespace, rname)
    time = Time.now.to_i
    lam_recipe = {
      "BST2100/PM2/Q-PM2-PC-MECH.rcp" => RmsAPI::RobObject.new( nil,
        { "ToolRecipeName"=>"d:\\Lam\\data\\PM2\\Recipes\\Q-PM2-PC-MECH-#{time}.rcp", "RecipeType"=>"Chamber"},
        { "ModuleID" => "PM2" }, nil),
      "BST2100/Q-PM2-PC-MECH.flw" => RmsAPI::RobObject.new( nil,
        { "ToolRecipeName" => "d:\\Lam\\data\\Flows\\Q-PM2-PC-MECH.flw", "RecipeType"=>"WaferFlow", "RecipeNameSpaceID"=>rnamespace, "RecipeName"=>"#{rname}-1" },
        { "ModuleCombination" => "PM2"},
        [{ "Equipment"=>"$", "Chamber"=>"$", "ToolRecipeName"=>"d:\\Lam\\data\\PM2\\Recipes\\Q-PM2-PC-MECH-#{time}.rcp"}]),
      "BST2100/Q-PM2-PC-MECH-2.flw" => RmsAPI::RobObject.new( nil,
        { "ToolRecipeName" => "d:\\Lam\\data\\Flows\\Q-PM2-PC-MECH.flw", "RecipeType"=>"WaferFlow", "RecipeNameSpaceID"=>rnamespace, "RecipeName"=>"#{rname}-2" },
        { "ModuleCombination" => "PM2"},
        [{"RecipeType"=>"Chamber", "ToolRecipeName"=>"d:\\Lam\\data\\PM2\\Recipes\\Q-PM2-PC-MECH-#{time}.rcp"}]),
      "BST2100/Q-PM2-PC-MECH-3.flw" => RmsAPI::RobObject.new( nil,
        { "ToolRecipeName" => "d:\\Lam\\data\\Flows\\Q-PM2-PC-MECH.flw", "RecipeType"=>"WaferFlow", "RecipeNameSpaceID"=>rnamespace, "RecipeName"=>"#{rname}-3" },
        { "ModuleCombination" => "PM2"},
        [{"RecipeType"=>"Chamber", "ToolRecipeName"=>"d:\\Lam\\data\\PM2\\Recipes\\Q-PM2-PC-MECH-#{time}.rcp"}])
    }
    return lam_recipe
  end
  
  def _generate_recipe_cycle()
    time = Time.now.to_i
    tel_recipe = {
      "ALC2350/Spinner%2FBCT%2FRUN-CK" => RmsAPI::RobObject.new( nil,
        { "ToolRecipeName"=>"Spinner/BCT/QUAL/04N1-HM14-200-0-1", "RecipeType"=>"Spinner", "Equipment"=>"ALC2350", "ProductID_EC"=>"RIL-PRODUCT.01"},
        { "ModuleID" => "PM2", "DCPPath" => "/path/to/system/dcp" }, 
        [{"RecipeType"=>"Spinner", "Equipment"=>"$", "ToolRecipeName"=>"Spinner/BCT/QUAL/04N1-HM14-200-0-2"}]),
      "ALC2350/Spinner%2FBCT%2FTELRUN-CK" => RmsAPI::RobObject.new( nil,
        { "ToolRecipeName"=>"Spinner/BCT/QUAL/04N1-HM14-200-0-2", "RecipeType"=>"Spinner", "Equipment"=>"ALC2350", "ProductID_EC"=>"RIL-PRODUCT.01"},
        { "ModuleID" => "PM2", "DCPPath" => "/path/to/system/dcp" }, 
        [{"RecipeType"=>"Spinner", "Equipment"=>"$", "ToolRecipeName"=>"Spinner/BCT/QUAL/04N1-HM14-200-0-3"}]),
      "ALC2350/Spinner%2FBCT%2FQUAL%2F04N1-HM14-200-0" => RmsAPI::RobObject.new( nil,
        { "ToolRecipeName"=>"Spinner/BCT/QUAL/04N1-HM14-200-0-3", "RecipeType"=>"Spinner", "Equipment"=>"ALC2350", "ProductID_EC"=>"RIL-PRODUCT.01"},
        { "ModuleID" => "PM2", "DCPPath" => "/path/to/system/dcp" }, 
        [{"RecipeType"=>"Spinner", "Equipment"=>"$", "ToolRecipeName"=>"Spinner/BCT/QUAL/04N1-HM14-200-0-1"}]),
      "ALC2350/WaferFlow%2FPRD%2FTD_20NM%2FPHM14200----TD0" => RmsAPI::RobObject.new( nil,
        { "ToolRecipeName" => "WaferFlow/PRD/TD_20NM/PHM14200----TD0", "RecipeType"=>"WaferFlow", "RecipeNameSpaceID"=>"QA-TEST", "RecipeName"=>"TEL-RECIPE-#{time}" },
        { "ModuleCombination" => "TRK"},
        [{"RecipeType"=>"Spinner", "Equipment"=>"$", "ToolRecipeName"=>"Spinner/BCT/QUAL/04N1-HM14-200-0-1"}])
    }
    return tel_recipe  
  end
  
  


  def _generate_ers_recipe(rnamespace, rname, params={})
    r_params = { "EngRcpSelect" => "Yes" }.merge((params[:r_params] or {}))
    lam_recipe = {
      nil => RmsAPI::RobObject.new( nil,
        { "ToolRecipeName" => "Engineering Recipe Select", "RecipeType"=>"WaferFlow-ERS", "RecipeNameSpaceID"=>rnamespace, "RecipeName"=>rname,
          "RecipePurpose"=>"Engineering", "OperationMode" => "RecipeSelect" },
        r_params),
    }
    return lam_recipe
  end


  def test001_ers_setup
    lam_recipe = _generate_ers_recipe("F-ERSA", "BST2100")
    robs = $rmstest.recipe_create(lam_recipe, :softrev=>"QA", :recipe_file=>nil)

    lam_recipe = _generate_ers_recipe("P-ERS", "BST", :r_params=>{"ModuleCombination" => "PM2", "EngRcpFilter" => "*MECH*"})
    robs = $rmstest.recipe_create(lam_recipe, :softrev=>"QA", :recipe_file=>nil)
  end

  def test002_lam_setup
    lam_recipe = _generate_lam_recipe("E-Q-BST-LAM-CORONUS-210X", "Q-PM2-PC-MECH")
    robs = $rmstest.recipe_create(lam_recipe, :softrev=>"QA")
  end
  
  # For APM Tests
  def test003_lam_setup
    lam_recipe = _generate_lam_recipe2("E-Q-BST-LAM-CORONUS-210X", "Q-PM2-PC-MECH")
    assert $rmstest.recipe_create(lam_recipe, :ignorelinks=>true, :checklinks=>false, :softrev=>"TC03-1"), "failed to setup recipes"
  end

  def test004_tel_setup
    recipe = _generate_recipe_cycle()
    $robs = $rmstest.recipe_create(recipe, :softrev=>"TC00-3", :tool_prefix=>"ALC", :tool_vendor=>"TEL", :ignorelinks=>true, :checklinks=>false)
    $robs = $rmstest.recipe_approval($robs)
    assert $robs, "Recipes not created successfully"
  end
  

  
  def test100_varian_setup
    #$rmstest.setup_litho_recipes
    time = Time.now.to_i
    imp_recipe = {
      "IMP2100/P-BF-07T007-5E15-00Q" => RmsAPI::RobObject.new( nil,
        { "ToolRecipeName"=>"#{time}\\P-BF-07T007-5E15-00Q", "RecipeType"=>"Main", "RecipeNameSpaceID"=>"QA-TEST", "RecipeName"=>"IMP-HC-RECIPE-#{time}"},
        { "Dose" => 7.5E14, "Energy" => 7.0, "Species"=>"8" },
        nil),
    }
    $robs = $rmstest.recipe_create(imp_recipe, :tool_prefix=>"IMP-HC", :tool_vendor=>"VARIAN", :softrev=>"")
    return
    $robs = $rmstest.recipe_approval($robs)
    assert $robs, "Recipes not created successfully"

    # Check RTD parameters (waferflow recipe only)
    rob = $robs
    assert_equal 1, rob.count, "No rob found"
    rob = rob[0]
    _wait_and_verify(imp_recipe["IMP2100/P-BF-07T007-5E15-00Q"], rob.rmshandle)

    # Update recipe context
    new_parameters = { "Dose" => 7.5E15, "Energy" => 17.0, "Species"=>"P" }
    rob = $rmstest.recipe_update(rob,:dataitems=>new_parameters)
    imp_recipe["IMP2100/P-BF-07T007-5E15-00Q"].dataitems.merge!(new_parameters)

    _wait_and_verify(imp_recipe["IMP2100/P-BF-07T007-5E15-00Q"], rob.rmshandle)

    $robs = $rmstest.rms.deactivate(rob)
    assert_equal 1, $robs.count, "Recipe not successfully deactivated"

    _wait_and_verify(imp_recipe["IMP2100/P-BF-07T007-5E15-00Q"], $robs[0].rmshandle, true)
  end
  
end
