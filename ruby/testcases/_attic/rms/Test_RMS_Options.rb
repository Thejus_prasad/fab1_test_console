=begin
 automated test of setup.FC API 

(c) Copyright 2011 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

 author      - daniel.steger@globalfoundries.com
 date        - 2013/10/15
=end

require_relative "Test_RMS_Setup"
require "misc"

class Test_RMS_Options < Test_RMS_Setup
  @@context = { 'RecipeNameSpaceID' => 'QA-TEST', 'RecipeName' => 'OPTIONS-01' }
  @@einame_fallback = true # configuration in RIL
  
  def _find_rob
    #
    rcm_recipe = {
      'RCM2100/P-BF-07T007-5E15-00Q' => RmsAPI::RobObject.new(nil,
                                                              @@context.merge('ToolRecipeName' => 'test\\RCM-Options01', 'RecipeType' => 'Main'),
                                                              {},
                                                              nil)
    }
    robs = $rms.find_object_list('Department' => 'MSS', 'ToolPrefix' => 'RCM', 'ToolVendor' => 'CHEMCHANGE', 'SoftRev' => 'TC12-1')

    unless robs.count == 1
      robs = $rmstest.recipe_create(rcm_recipe, 'SoftRev' => 'TC12-1', 'ToolVendor' => 'CHEMCHANGE', 'ToolPrefix' => 'RCM', :recipe_body => nil)
      assert_equal true, $rms.validate(robs)[0][1], "rob should be valid #{robs[0].rmshandle}"
      robs = $rmstest.recipe_approval(robs)
    end
    rob = robs.first
    assert rob, 'failed to find ROB'
    rob
  end
  
  def test51_manyoptions
    rob = _find_rob
    ctx = {"RecipeName"=>"OPTIONS-01"}
    rob = $rms.create_work_copy(rob, ctx,{})
    
    rob = $rms.del_data(rob,["__options__MaterialDictionary", "__options__UserDictionary"])
    options = 2000.times.collect {|i| "TEST %04d=CODE %04d" % [i,i] }
    rob = $rms.put_data(rob, {"__options__MaterialDictionary"=>options})
    
    assert $rmstest.recipe_approval(rob), "failed to approve recipe"
    assert verify_recipe_options("MaterialDictionary", options, ctx)
  end
  
  def test52_emptyoptions
    rob = _find_rob
    ctx = {"RecipeName"=>"OPTIONS-02"}
    rob = $rms.create_work_copy(rob, ctx,{})
    
    rob = $rms.del_data(rob,["__options__MaterialDictionary", "__options__UserDictionary"])
    options = 10.times.collect {|i| "TEST %03d=" % i }
    options += (11..20).collect {|i| "TEST %03d=CODE %03d" % [i,i] }
    rob = $rms.put_data(rob, {"__options__MaterialDictionary"=>options})
    
    assert $rmstest.recipe_approval(rob), "failed to approve recipe"
    assert verify_recipe_options("MaterialDictionary", options, ctx)
  end
  
  def test53_mixedoptions
    rob = _find_rob
    ctx = {"RecipeName"=>"OPTIONS-03"}
    rob = $rms.create_work_copy(rob, ctx,{})
    
    rob = $rms.del_data(rob,["__options__MaterialDictionary", "__options__UserDictionary"])
    options = 10.times.collect {|i| 
      (i*10..i*10+9).collect {|j| "TEST %03d=CODE %03d" % [j,j] }.join(";")
    }    
    rob = $rms.put_data(rob, {"__options__MaterialDictionary"=>options})
    
    assert $rmstest.recipe_approval(rob), "failed to approve recipe"
    assert verify_recipe_options("MaterialDictionary", options.map {|a| a.split(";")}.flatten, ctx)
  end
  
  def test54_adhocoption
    rob = _find_rob
    ctx = {"RecipeName"=>"OPTIONS-04"}
    rob = $rms.create_work_copy(rob, ctx,{})
    
    rob = $rms.del_data(rob,["__options__MaterialDictionary", "__options__UserDictionary"])
    options = 10.times.collect {|i| 
      (i*10..i*10+9).collect {|j| "TEST %03d=CODE %03d" % [j,j] }.join(";")
    }    
    rob = $rms.put_data(rob, {"__options__UserDictionary"=>options})
    
    assert $rmstest.recipe_approval(rob), "failed to approve recipe"
    e = assert_raises RMS::RilError do    
      $ril.get_recipe_payload_request(@@context.merge(ctx))
    end
    assert_match /RIL-RECIPE_MODEL_VIOLATION/, e.message
  end
  
  def test55_duplicateoptions
    rob = _find_rob
    ctx = {"RecipeName"=>"OPTIONS-05"}
    rob = $rms.create_work_copy(rob, ctx,{})
    
    rob = $rms.del_data(rob,["__options__MaterialDictionary", "__options__UserDictionary"])
    options = 10.times.collect {|i| "TEST 007=CODE %02d" % i }
    rob = $rms.put_data(rob, {"__options__MaterialDictionary"=>options})
    
    assert $rmstest.recipe_approval(rob), "failed to approve recipe"
    e = assert_raises RMS::RilError do    
      $ril.get_recipe_payload_request(@@context.merge(ctx))
    end
    assert_match /RIL-RECIPE_MODEL_VIOLATION/, e.message
  end
    
  def verify_recipe_options(parameter, options, context)
    res = true
    rcp = $ril.get_recipe_payload_request(@@context.merge(context))
    res &= $rmstest.verify_rms_recipe(rcp)
    _param = rcp.parameters.find {|r| r.pid == parameter}
    _options = Hash[options.map {|a| x,y = a.split("="); [x,y]}]
    res &= verify_hash(_options, _param.options, "options do not match")
    return res
  end
  
  def XXXtest52_runtime_getpayload
    rcp = $ril.get_recipe_payload_request(@@context)
    assert $rmstest.verify_rms_recipe(rcp), "recipe check failed"
    assert $rmstest.check_recipe_parameters(rcp, einame: @@einame_fallback), "parameter check failed"
  end
  
  def _check_valid_rob(rmshandle, item, value, new_value=nil)
    $log.info "checking #{rmshandle}, #{item}"
    rob = RmsAPI::RobObject.new(rmshandle)
    if new_value
      rob = $rms.put_data(rob,{item=>new_value})
    else
      rob = $rms.del_data(rob,[item])
    end
    res = verify_equal false, $rms.validate(rob)[0][1], "  rob should be invalid"
    
    rob = RmsAPI::RobObject.new(rmshandle)
    rob = $rms.put_data(rob,{item=>value}) 
    res &= verify_equal true, $rms.validate(rob)[0][1], "  rob should be valid"
    return res
  end
end