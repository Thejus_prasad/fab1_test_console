=begin
 automated test of setup.FC API 

(c) Copyright 2011 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

 author      - daniel.steger@globalfoundries.com
 date        - 2012/08/28
=end

require_relative 'Test_RMS_Setup'
require 'misc'

# RMS Recipe management tests
class Test_RMS_Basic < Test_RMS_Setup
  Description = 'Test RIL-EI Interface basic tests'

  @@toolprefix = 'ETX ALC TRK IMP-HC MPX MDX RTA CDS MES'
  @@nthreads = 8

  @@contexts = [{
      'Equipment'=>'RTA2300',
      'RecipeName'=>'F-P-220C60SN220LP',
      'RecipeNameSpaceID'=>'F-P-RTA230X'
    },
    {
      'Equipment'=>'RTA2301',
      'RecipeName'=>'F-E-420C30SN2-PM1',
      'RecipeNameSpaceID'=>'F-E-RTA230X'
    },
    {
      'Equipment'=>'ALC2400',
      'RecipeName'=>'P-HD4004-COAT',
      'RecipeNameSpaceID'=>'L-P-TRK-PI'
    },
    {
      'Equipment'=>'ALC2400',
      'RecipeName'=>'POLY-TPR-COAT-2',
      'RecipeNameSpaceID'=>'L-Q-TRK-PI'
    },
    {
      'Equipment'=>'ALT2600',
      'RecipeName'=>'PHM14200----TD0',
      'RecipeNameSpaceID'=>'L-P-ALT-TEL'
    },
    {
      'Equipment'=>'ETX2100',
      'RecipeName'=>'Q-PM2-ER-OPL',
      'RecipeNameSpaceID'=>'E-Q-ETX-LAM-KIYO-EX-STI'
    },
    {
      'Equipment'=>'ETX2101',
      'RecipeName'=>'Q-PM2-ER-2KPOLY',
      'RecipeNameSpaceID'=>'E-Q-ETX-LAM-KIYO-EX-STI'
    },
    {
      'RecipeName'=>'P-AS-01T020-8E15-00Q',
      'RecipeNameSpaceID'=>'D-P-IMP10X',
      'Equipment'=>'IMP2100'
    },
    {
      'RecipeName'=>'E-BF-00T035-2E12-00S',
      'RecipeNameSpaceID'=>'D-E-IMP20X',
      'Equipment'=>'IMP2200'
    },
    {
      'RecipeName'=>'PUV14400----TD0',
      'RecipeNameSpaceID'=>'L-P-ALC-248-S',
      'Equipment'=>'ALC2460',
      'ProductID.EC'=>'SHELBY3CC.01',
      'ProductGroup'=>'SHELBY3',
      'PhotoLayer'=>'2LW'
    }
    ]

  @@csv_file = 'testcasedata/rms_ctx_itdc.csv'

  # executed on demand only
  def testman010_simple_export
    @@contexts.each_with_index do |context,i|
      payload = $ril.get_recipe_payload_request(@@default_ei_ctx.merge(context), :export=>"log/%02d-getpayload-request.xml" % i, :rawunpacked=>true)
      if payload.index('RILException')
        $log.error("#{context}:\n#{payload}")
      else
        File.open("log/%02d-getpayload-reply.xml" % i, 'w') {|fh| fh.write(payload)}
        rcp = $ril.payload_to_recipe(payload)
        result = $ril.compare_recipes(rcp, :export=>"log/%02d-compare-request.xml" % i, :rawunpacked=>true)
        File.open("log/%02d-compare-reply.xml" % i, 'w') {|fh| fh.write(result)}
      end
    end
  end

  def test020_simple_check
    $setup_ok = false
    File.open(@@csv_file) {|fh| fh.readlines }.reject {|line| line.start_with?('#')}.each do |line|
      ctx = {}
      data = line.split(',')
      RMS::ContextKeys.keys().each_with_index do |k,i|
        ctx[k] = (data[i].nil? || data[i].strip() == '') ? @@default_ei_ctx[k] : data[i]
      end
      rcp = $ril.get_recipe_payload_request(ctx)
      assert rcp, 'failed to get payload'
      assert $ril.mq_response.duration < 60, 'Call longer than 60s'
      assert $rmstest.verify_rms_recipe(rcp), 'recipe check failed'
      assert $rmstest.check_recipe_parameters(rcp), 'parameter check failed'
      result = $ril.compare_recipes(rcp)
      assert $ril.mq_response.duration < 60, 'Call longer than 60s'
      assert result, 'failed to get compare result'
      assert_equal 'true', result[:compareRecipesResponse]['totalComparisonResult'], "comparison failed with #{result.inspect}"
    end
    $setup_ok = true
  end
  
  def test040_compare_recipes
    $rms.login
    rmshandles = $rms.get_reg_entries(:filters=>{'state'=>'ACTIVE', 'OperationMode'=>'RecipeCompare'}).take(10) #, , 'Department'=>'TFM''OperationMode'=>'RecipeCompare'})
    failed_cmp = []
    errored_cmp = []
    stats = {} 
    rmshandles.each do |(rmshandle)|
      begin    
        rob = $rms.get_data(RmsAPI::RobObject.new(rmshandle))
        ($log.info "no body found for #{rmshandle}"; next) unless rob.dataitems['Body']&.value
        rob_keys = $rms.get_keys(rob) # refresh context keys
        body = Base64.encode64(rob.dataitems['Body'].value)
        res = $ril.compare_recipes(rmshandle => body)
        k = "#{rob.context['ToolVendor']}_#{rob.context['RecipeType']}"
        if stats.has_key?(k) 
          stats[k] << $ril.mq_response.duration
        else
          stats[k] = [$ril.mq_response.duration]
        end
        failed_cmp << rmshandle unless verify_equal('true', res[:compareRecipesResponse]['totalComparisonResult'], "#{res.inspect}")
      rescue => e
        $log.error "failed for #{rmshandle}, #{e.message}"
        errored_cmp << rmshandle
      end
    end
    # Stats
    #File.open("testcasedata/rms_perf_cmp_#{@@release}.csv", 'w') do |fh|
    #  fh.write "ToolVendor_RecipeType, #min, #avg, #max, #count\n"
    #  stats.sort.map do |k,v|
    #    fh.write "#{k}, #{v.min}, #{v.avg}, #{v.max}, #{v.count}\n"
    #  end
    #end
    # Compare results
    res = verify_equal 0, errored_cmp.count, "no errors expected: #{errored_cmp.inspect}"
    res &= verify_equal 0, failed_cmp.count, "no failures expected: #{failed_cmp.inspect}"
    assert res, 'failed comparision'
  end
  
  def test041_compare_recipes_in_mem
    $rms.login
    rmshandles = $rms.get_reg_entries(:filters=>{'state'=>'ACTIVE', 'OperationMode'=>'RecipeCompare'}).take(10) #, , 'Department'=>'TFM''OperationMode'=>'RecipeCompare'})
    failed_cmp = []
    errored_cmp = []
    stats = {}   
    rmshandles.each do |(rmshandle)|
      begin        
        rob = $rms.get_data(RmsAPI::RobObject.new(rmshandle))
        ($log.warn "no body found for #{rmshandle}"; next) unless rob.dataitems['Body']&.value
        rob_keys = $rms.get_keys(rob) # refresh context keys
        res = $rms.upload_compare(rmshandle, rob.dataitems['Body'].value)        
        k = "#{rob.context['ToolVendor']}_#{rob.context['RecipeType']}"
        if stats.has_key?(k) 
          stats[k] << $rms.mq_response.duration
        else
          stats[k] = [$rms.mq_response.duration]
        end
        failed_cmp << rmshandle unless verify_equal(true, res, "#{res.inspect}")
      rescue => e
        $log.error "failed for #{rmshandle}, #{e.message}"
        errored_cmp << rmshandle
      end
    end
    # Stats
    #File.open("testcasedata/rms_perf_cmp_inmem_#{@@release}.csv", 'w') do |fh|
    #  fh.write "ToolVendor_RecipeType, #min, #avg, #max, #count\n"
    #  stats.sort.map do |k,v|
    #    fh.write "#{k}, #{v.min}, #{v.avg}, #{v.max}, #{v.count}\n"
    #  end
    #end
    # Compare results
    res = verify_equal 0, errored_cmp.count, "no errors expected: #{errored_cmp.inspect}"
    res &= verify_equal 0, failed_cmp.count, "no failures expected: #{failed_cmp.inspect}"
    assert res, 'failed comparision'
  end
  
  def test050_create_recipe_and_new_version_prod
    time = Time.now.to_i
    imp_recipe = {
      'IMP2100/P-BF-07T007-5E15-00Q' => RmsAPI::RobObject.new( nil,
        { 'ToolRecipeName'=>"#{time}\\P-BF-07T007-5E15-00Q", 'RecipeType'=>'Main', 'RecipeNameSpaceID'=>'QA-TEST', 'RecipeName'=>"IMP-HC-RECIPE-#{time}"},
        { 'EnergyTrim' => 7.0, 'Species'=>'8', 'QAParamDouble' => 7.7, 'QAParamString' => 'A', 'QAParamInteger' => 7, 'QAParamBool'=>false, 'QAParamTime'=>Time.now },
        nil),
    }
    robs = $rmstest.recipe_create(imp_recipe, tool_prefix: 'IMP-HC', tool_vendor: 'VARIAN', softrev: 'TC02-1')
    assert_equal '', $rms.get_keys(robs.first).context['builtin']['approval_cycle_name'], 'no approval cycle should be set'
    assert rob = $rmstest.recipe_approval(robs).first, 'Recipes not created successfully'
    assert_equal 'true', $rms.get_keys(rob).context['builtin']['last_in_branch'], "wrong value for last_in_branch #{rob.rmshandle}"
    assert_match(/AC_ROB_[0-9]+_[0-9]+/, $rms.get_keys(rob).context['builtin']['approval_cycle_name'], 'no approval cycle should be set')
    
    rob_new = $rmstest.recipe_update(rob)
    assert rob_new, 'Recipes not created successfully'
    assert_equal 'false', $rms.get_keys(rob).context['builtin']['last_in_branch'], "wrong value for last_in_branch in #{rob.rmshandle}"
    assert_equal 'true', $rms.get_keys(rob_new).context['builtin']['last_in_branch'], "wrong value for last_in_branch #{rob_new.rmshandle}"
    rob_del = $rms.deactivate(rob_new).first
    assert_equal 'true', $rms.get_keys(rob_del).context['builtin']['last_in_branch'], "wrong value for last_in_branch #{rob_del.rmshandle}"
    assert_match(/AC_ROB_[0-9]+_[0-9]+/, $rms.get_keys(rob_del).context['builtin']['approval_cycle_name'], 'no approval cycle should be set')
  end
  
  def test051_create_recipe_and_new_version_eitest
    time = Time.now.to_i
    imp_recipe = {
      'IMP2100/P-BF-07T007-5E15-00Q' => RmsAPI::RobObject.new( nil,
        { 'ToolRecipeName'=>"#{time}\\P-BF-07T007-5E15-00Q", 'RecipeType'=>'Main', 'RecipeNameSpaceID'=>'QA-TEST', 'RecipeName'=>"IMP-HC-RECIPE-#{time}"},
        { 'Dose' => 7.5E14, 'Energy' => 7.0, 'Species'=>'8'},
        nil),
    }
    robs = $rmstest.recipe_create(imp_recipe, tool_prefix: 'IMP-HC', tool_vendor: 'VARIAN', softrev: 'TC07-3', target_name: 'EITest')
    assert_equal '', $rms.get_keys(robs.first).context['builtin']['approval_cycle_name'], 'no approval cycle should be set'
    assert rob = $rms.release(robs).first, 'Recipes not created successfully'
    assert_equal 'true', $rms.get_keys(rob).context['builtin']['last_in_branch'], "wrong value for last_in_branch #{rob.rmshandle}"
    assert_equal '', $rms.get_keys(rob).context['builtin']['approval_cycle_name'], 'no approval cycle should be set'
    
    rob_new = $rms.create_work_copy(rob, {}, {}, {:target_name=>'EITest', :target_type=>'EITest', :registration=>'RCP_EI_TEST'})
    rob_new = $rms.release(rob_new).first
    assert rob_new, 'Recipes not created successfully'
    assert_equal 'false', $rms.get_keys(rob).context['builtin']['last_in_branch'], "wrong value for last_in_branch in #{rob.rmshandle}"
    assert_equal 'true', $rms.get_keys(rob_new).context['builtin']['last_in_branch'], "wrong value for last_in_branch #{rob_new.rmshandle}"
    rob_del = $rms.deactivate(rob_new).first
    assert_equal 'true', $rms.get_keys(rob_del).context['builtin']['last_in_branch'], "wrong value for last_in_branch #{rob_del.rmshandle}"
    assert_equal '', $rms.get_keys(rob_del).context['builtin']['approval_cycle_name'], 'no approval cycle should be set'
  end
  
  def testman091_perf
    #$rmstest.contexts_to_csv ["TFM"], 'ctx_20141016.csv', 500
    contexts = $rmstest.read_context_file(@@csv_file)
    #assert $rmstest.load_test(contexts: contexts, eqp: @@eqp, numbers: [64, 80, 96, 112, 128, 144, 160, 176, 192, 208, 224, 240, 256, 272, 288, 304, 320, 336, 352, 368])
    #assert $rmstest.load_test(contexts: contexts, eqp: @@eqp, numbers: [112, 128, 144, 160, 176, 192, 208, 224, 240, 256, 272, 288, 304, 320, 336, 352, 368], tolerant: true, shift: 10)
    #assert $rmstest.load_test(contexts: contexts, eqp: @@eqp, numbers: [1, 2, 4, 8, 16, 32, 64, 128, 256, 384, 512, 640, 768, 896, 1024])
    #assert $rmstest.load_test(contexts: contexts, eqp: @@eqp, numbers: [112, 128, 144], tolerant: true, shift: 10)
    assert $rmstest.load_test(contexts: contexts, eqp: @@eqp, numbers: [1, 2, 4, 8, 16, 32, 64, 128, 256, 384, 512, 640, 768, 896, 1024], tolerant: true, shift: 10)
  end

  # Check for derdacks in the last 24 hours
  def testman099_derdack_events
    assert $rmstest.verify_notification('RIL/RM health check failed', nil, :tstart=>Time.now - 24*3600, :raw=>true).count > 0, 'Expected some derdack messages'
  end
end
