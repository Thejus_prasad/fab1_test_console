=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: Steffen Steidten, 2014-06-16
=end

require 'warptest'
require 'RubyTestCase'


class Test_Warp < RubyTestCase
  @@tag = nil
  @@pdname = 'FICSQA.Flow.Test1.v1_0_0'
  @@data = {'priority'=>'EMPTY', 'tool'=>'EMPTY', 'state'=>'EMPTY'}
  @@task1 = "FICSQA.Flow.Test1.Request"
  @@task2 = "FICSQA.Flow.Test1.Approve"
  # users must be member of LDAP group FC1.APP.WARP.D.QA.GROUP1
  @@user = "sfrieske@gfoundries.com"   # few tasks
  @@perf_user = "sfrieske@gfoundries.com"  # "sschoen3@gfoundries.com"   # many assigned tasks from other workflows
  @@perf_sessions = [1, 2, 5, 10, 20, 50, 100]


  def self.startup
    super(nosiview: true)
    @@wtest = Warp::PerfTest.new($env, user: @@user)
    @@wc = @@wtest.client
    @@tag ||= $env.upcase
  end

  def setup
    #@@wc.process_instances(@@pdname).each {|pid| @@wc.delete_process_instance(pid)}
    @@wtest.pid_cleanup
    super
  end

  def test11_happy1 # short path, calling all elements
    $setup_ok = false
    #
    # verify setup, clean up
    pdefs = @@wc.process_definitions
    assert pdef = pdefs.find {|e| e.definition == @@pdname}
    assert @@wc.process_instances(@@pdname).empty?
    # start new instance, delete and create again
    assert pid = @@wc.start_process_instance(@@pdname, data: @@data)
    assert @@wc.delete_process_instance(pid)
    assert @@wc.process_instances(@@pdname).empty?
    assert pid = @@wc.start_process_instance(@@pdname, data: @@data)
    # first task is assignable
    tasks = @@wc.instance_tasks(pid)
    assert_equal 1, tasks.size
    task = tasks.first
    assert @@wc.assignable_tasks(@@user).size > 0  # tasks from other workflows may be assignable
    # claim and release task
    assert @@wc.claim_user_task(@@user, task)
    task = @@wc.instance_tasks(pid).first
    assert_equal @@user, task.assignee
    assert_equal @@task1, task.name
    assert @@wc.release_user_task(@@user, task)
    assert_equal '', @@wc.instance_tasks(pid)[0].assignee
    # claim and complete first task
    assert @@wc.claim_user_task(@@user, task)
    assert @@wc.complete_user_task(@@user, task, data: {'priority'=>'HIGH'}) # any prio except "LOW"
    # @@wc.process_instance_dataset(pid) => compare??
    # flow went the "High Priority" path, now at final gateway waiting for a signal
    assert @@wc.signal_process_instance(pid, data: {'feedback'=>'QAFeedback HIGH'})
    assert @@wc.process_instances(@@pdname).empty?
    #
    $setup_ok = true
  end

  def test12_happy2 # path with 2 consecutive tasks
    # clean up
    assert @@wc.process_instances(@@pdname).empty?
    # start new instance
    assert pid = @@wc.start_process_instance(@@pdname)
    # first task
    assert task = @@wc.instance_tasks(pid).first
    assert @@wc.claim_user_task(@@user, task)
    assert task = @@wc.instance_tasks(pid).first
    assert_equal @@task1, task.name
    assert @@wc.complete_user_task(@@user, task, data: {'priority'=>'LOW'})
    # wait for the 2nd task to appear
    sleep 3
    # 2nd task, approve
    assert task = @@wc.instance_tasks(pid).first
    assert_equal @@task2, task.name
    assert @@wc.claim_user_task(@@user, task)
    assert @@wc.complete_user_task(@@user, task, data: {'state'=>'APPROVED'})
    # flow went the "Low Priority -> Approved" path, now at final gateway waiting for a signal
    assert @@wc.signal_process_instance(pid, data: {'feedback'=>'QAFeedback LOW'})
    assert @@wc.process_instances(@@pdname).empty?
  end

  def test13_happy3 # path with more tasks
    # clean up
    assert @@wc.process_instances(@@pdname).empty?
    # start new instance
    assert pid = @@wc.start_process_instance(@@pdname)
    # first task
    assert task = @@wc.instance_tasks(pid).first
    assert @@wc.claim_user_task(@@user, task)
    assert task = @@wc.instance_tasks(pid).first
    assert_equal @@task1, task.name
    assert @@wc.complete_user_task(@@user, task, data: {'priority'=>'LOW'})
    # 2nd task, disapprove
    sleep 3
    assert task = @@wc.instance_tasks(pid).first
    assert_equal @@task2, task.name
    assert @@wc.claim_user_task(@@user, task)
    assert @@wc.complete_user_task(@@user, task, data: {'state'=>'DISAPPROVED'})
    # remaining tasks
    while task = @@wc.instance_tasks(pid).first
      assert @@wc.claim_user_task(@@user, task)
      assert @@wc.complete_user_task(@@user, task)
    end
    sleep 3
    assert @@wc.process_instances(@@pdname).empty?
  end

  def test91_perf_sessions
    assert @@wtest.load_sessions(numbers: @@perf_sessions, user: @@perf_user, tag: @@tag)
  end

  def test92_perf_tasklist
    assert @@wtest.load_tasklist(numbers: @@perf_sessions, user: @@perf_user, tag: @@tag)
  end

end
