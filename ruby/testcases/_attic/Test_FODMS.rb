=begin 
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.
    
Author: Daniel Steger

Version: FOD-MS recalculation
=end

require 'RubyTestCase'
require 'fodms'

class Test_FODMS < RubyTestCase

  @@route = "C02-0342.01"
  @@product = "SHELBY21SB.F0"

  
  @@product_2 = "SHELBY21SB.B1"
  @@route_2 ="C02-0342.02"
  
  @@rework_route = "R-LS-PS-HF-SPM-APM.01"
  @@rework_op = "1300.3100"
  
  
  def self.startup
    super
    $fodms = FODMS::Test.new($env) unless $fodms && $fodms.env == $env
    if $env == "f8stag"
      @@route = "FF-GIMLI2.01"
      @@product = "GIMLI21MA.A0"
      @@product_2 = "GIMLI21TA.A0"
      
      @@route_3 = "FF-THORIN1.01"
      @@product_3 = "THORIN11MA.A2"
      
      @@rework_route = "R-LF-RXLITOPL28BK.01"
      @@rework_op = "1100.0920"
      
      @@rework_ret = "1100.0900"
    end
    @@lot = "8XYL18006.000"#"UXYL18004.000"
  end

  def test01_recalc_events
    sod_date = (Time.now+30*24*3600).strftime("%Y-%m-%d")
    tstart = Time.now
    @@lot = $sv.new_lot_release(:stb=>true, :route=>@@route, :product=>@@product)
    assert $sv.user_parameter_set_verify("Lot", @@lot, "FOD_COMMIT", sod_date)
    assert $fodms.verify_recalculated(@@lot, "STB", tstart: tstart)#, "STB recalculation failed"
    
    # jCAP hold release
    sleep 10
    $sv.lot_hold_release(@@lot, nil)
    
    tstart = Time.now
    assert_equal 0, $sv.schdl_change(@@lot, route: @@route, product: @@product_2), "failed to do product change"
    assert $fodms.verify_recalculated(@@lot, "SCHEDULE_CHANGE", tstart: tstart)#, "Product change recalculation failed"
    
    tstart = Time.now
    assert_equal 0, $sv.schdl_change(@@lot, route: @@route_3, product: @@product_3), "failed to do route change"
    assert $fodms.verify_recalculated(@@lot, "SCHEDULE_CHANGE", tstart: tstart)#, "Route change recalculation failed"
    
    tstart = Time.now
    child = $sv.lot_split(@@lot, 12)    
    assert child, "failed to split @@lot #{@@lot}"
    assert $fodms.verify_recalculated(@@lot, "SPLIT_PARENT", tstart: tstart)#, "Route change recalculation failed"
    assert $fodms.verify_recalculated(child, "SPLIT_CHILD", parent: @@lot, tstart: tstart)#, "Route change recalculation failed"
    
    tstart = Time.now
    assert_equal 0, $sv.lot_merge(@@lot, child), "failed to merge lot"
    assert $fodms.verify_recalculated(@@lot, "MERGE", tstart: tstart)#, "Route change recalculation failed"
    
    tstart = Time.now
    $sv.lot_opelocate(@@lot, "first")
    assert_equal 0, $sv.lot_gatepass(@@lot), "failed to gatepass lot"
    child = $sv.lot_split(@@lot, 12)    
    assert child, "failed to split @@lot #{@@lot}"
    assert $fodms.verify_recalculated(@@lot, "SPLIT_PARENT", tstart: tstart)#, "Route change recalculation failed"
    assert $fodms.verify_recalculated(child, "SPLIT_CHILD", parent: @@lot, tstart: tstart)#, "Route change recalculation failed"
    
    tstart = Time.now
    assert_equal 0, $sv.lot_merge(@@lot, child), "failed to merge lot"
    assert $fodms.verify_recalculated(@@lot, "MERGE", tstart: tstart)#, "Route change recalculation failed"
  end  
    
  def test02_rework
    # reset the product and route    
    tstart = Time.now
    assert_equal 0, $sv.schdl_change(@@lot, route: @@route, product: @@product), "failed to do product change"
    assert_equal 0, $sv.lot_opelocate(@@lot, "first")
    assert $fodms.verify_recalculated(@@lot, "SCHEDULE_CHANGE", tstart: tstart)#, "Route change recalculation failed"
    t1 = $fodms.mds.lot_track(@@lot).schedule
    #
    tstart = Time.now
    $sv.lot_opelocate(@@lot, @@rework_op) # recalc after lot on rework routes
    assert_equal 0, $sv.lot_rework(@@lot, @@rework_route, return_opNo: @@rework_ret), "failed to rework lot"
    assert_equal 0, $sv.lot_requeue(@@lot), "failed to requeue lot"
    assert $fodms.verify_recalculated(@@lot, "REQUEUE", tstart: tstart)#, "Route change recalculation failed"
    t2 = $fodms.mds.lot_track(@@lot).schedule
    assert t1[-1].ope_end_time_plan != t2[-1].ope_end_time_plan, "recalculation of planned end time at last PD expected"
    
    tstart = Time.now
    $sv.lot_opelocate(@@lot, 2) # move lot back to main route
    assert_equal 0, $sv.lot_requeue(@@lot), "failed to requeue lot"
    assert $fodms.verify_recalculated(@@lot, "REQUEUE", tstart: tstart)#, "Route change recalculation failed"
    t3 = $fodms.mds.lot_track(@@lot).schedule
    assert t2[-1].ope_end_time_plan != t3[-1].ope_end_time_plan, "recalculation of planned end time at last PD expected"
  end
  
  def self.after_all_passed
    $sv.delete_lot_family @@lot
  end
end