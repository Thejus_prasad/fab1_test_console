=begin 
Test Space/SIL.

(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.
    
Author: Daniel Steger, 2012-01-12
=end


require_relative 'Test_Space_Setup'

# Test Space/SIL - setup.FC integration and test setup

class Test_Space08 < Test_Space_Setup
  Description = "Test Space/SIL - setup.FC integration and test setup"

  def specs_from_prod
    $spctest.sfc.specs_to_file "prod_specs", :id=>"M07-00000600"
  end
  
  
  def workaround_mtqa(spec_id, version)
    sfc_queue = MQ::QueueManager.new("sfc_space.#{$env}", :write=>true)
    sfc_queue.send_msg '<?xml version="1.0" encoding="UTF-8" standalone="yes"?><setupfcMessage message="RELEASE" 
       replyRequired="false" specificationVersion="'"#{version}"'" specificationId="'"#{spec_id}"'" recipient="SPACE" 
       sender="SETUPFC_WF_ENGINE" correlationId="3272" xmlns="http://setupfc.globalfoundries.com/xml/message"/>'
    sfc_queue.disconnect
  end
  
  def test081_sfc_ccp
    $log.info "CCP from prod upload"
    
    specs = ["prod_specs/M02-CCP-OBANSK-PT3.xml", "prod_specs/M02-CCP-OBANPK-PT3.xml"]
    process_specs(specs, "02", "ContaminationControlPlan", "M02-1", "Test CCP - SIL #{@@sil_version}")
  end
  
  def test082_sfc_ecp
    $log.info "ECP from prod upload"

    #specs = ["prod_specs/M14-00000204.xml"]#, "prod_specs/M14-00000131.xml", "prod_specs/M14-00000174.xml"]
    specs = ["prod_specs/M14-00020074.xml", "prod_specs/M14-00020075.xml", "prod_specs/M14-00020076.xml"]
    process_specs(specs, "14", "ElectricalControlPlan", "M14-1", "Test ECP - SIL #{@@sil_version}")
  end
  
  def test083_sfc_pcp
    $log.info "PCP from prod upload"
    
    specs = [
      "prod_specs/M14-00000048.xml", 
      "prod_specs/M14-00000021.xml",
      "prod_specs/M14-00000067.xml"
      ]
    process_specs(specs, "14", "ProcessControlPlan", "M14-2", "Test PCP - SIL #{@@sil_version}", 1)
  end
  
  def test084_sfc_tcp
    $log.info "TCP from prod upload"
    
    specs = [
      "prod_specs/M07-00001295.xml",
      "prod_specs/M07-00000488.xml",
      "prod_specs/M07-00000503.xml",
      "prod_specs/M07-00000512.xml",
      "prod_specs/M07-00000586.xml"
    ]
    specs = [ "prod_specs/M07-00000637.xml" ]
    process_specs(specs, "07", "ToolControlPlan", "M07-5", "Test TCP - SIL #{@@sil_version}", 2)
  end

  def test085_empty_spec
    $log.info "CCP from prod upload"

    specs = ["prod_specs/M02-CCP-RIDGE2.xml"]
    process_specs(specs, "02", "ContaminationControlPlan", "M02-1", "Test CCP - SIL #{@@sil_version}", nil, true)
  end
    
  # Process a list of specs and check the result (reject the given version)
  def process_specs(specs, specclass, subclass, workflow, title, cancel=nil, rejected=false)
    $log.info "#{specs.inspect}"
    
    id = specclass == "02" ? "M02-CCP-TEST#{Time.now.to_i}" : nil
    add_keys = subclass == "ElectricalControlPlan" ? [:nexttestpd] : []
    
    spec_id = $spctest.sfc.create_new_specification(specclass, subclass, workflow, "Table", :title=>title, :id=>id)
    version = 0
    
    specs.each_with_index do |s,i|
      $log.info("+++++++++++++++++++++++++++  Checking #{s} +++++++++++++++++++++++++++++++++++")
      
      $spctest.sfc.content_change(spec_id)
      spec, version = $spctest.sfc.submit_specification(spec_id, :type=>"SPACE", :filename=>s)
      $log.info "#{spec_id}, Version is #{version}"
      
      sleep 45
      ok = true
      ok &= $spctest.verify_spec(:specid=>spec_id, :specversion=>version, :all_active=>false, :add_valkeys=>add_keys) unless rejected
      ok &= $spctest.verify_spec(:specid=>spec_id, :specversion=>version-1, :all_active=>true, :add_valkeys=>add_keys) if version > 1
      assert ok, "Verification after spec submit failed for prod spec #{s} (#{spec_id}, #{version})"

      if rejected
        $log.info "Spec (#{spec_id}, #{version}) should have been rejected"
        ok &= $spctest.verify_no_spec_data(:specid=>spec_id, :specversion=>version)
        attach = $spctest.sfc.get_spec_data(spec_id, version)[:result][:attachments]
        if attach          
          text = $spctest.sfc.get_attachment(attach, spec_id, version)
          ok &= verify_match(/Spec tables do not match any of configured table types/, text, " text does not match")
          $spctest.sfc.human_workflow_action(spec_id, :action=>"CANCEL", :timeout=>30, :polling=>true)
          sleep 20
        else
          $log.warn(" no attachment found")
          ok = false
        end
      elsif cancel == i+1
        $log.info "Rollback version #{cancel}"
        $spctest.sfc.human_workflow_action(spec_id, :action=>"REJECT", :timeout=>30, :polling=>true)
        $spctest.sfc.human_workflow_action(spec_id, :action=>"CANCEL", :timeout=>30, :polling=>true)
        sleep 20
      else
        $spctest.sfc.human_workflow_action(spec_id, :timeout=>30, :polling=>true)
            
        workaround_mtqa(spec_id, version) if $env == "mtqa"
          
        sleep 45
        ok = $spctest.verify_spec(:specid=>spec_id, :specversion=>version, :all_active=>true, :add_valkeys=>add_keys)
        ok &= $spctest.verify_no_spec_data(:specid=>spec_id, :specversion=>version-1) if version > 1
        assert ok, "Verification after spec approval for prod spec #{s} (#{spec_id}, #{version})."
        
        $spctest.sfc.acknowledge(spec_id, :timeout=>30, :polling=>true)        
      end     
    end
        
    $spctest.sfc.terminate(spec_id)    
    $spctest.sfc.human_workflow_action(spec_id, :action=>"SUBMIT", :timeout=>30, :polling=>true)
    $spctest.sfc.human_workflow_action(spec_id, :timeout=>30, :polling=>true)
    sleep 30    
    assert $spctest.verify_no_spec_data(:specid=>spec_id, :specversion=>version), "Verification after terminate failed for (#{spec_id}, #{version})"
    
    $spctest.sfc.acknowledge(spec_id, :timeout=>30, :polling=>true)    
  end
  
  
  
  def self.spec_search(dirname, pattern)
    specs = []
    Find.find(dirname) {|f| specs << f if (File.file?(f) and f =~ pattern) }
    return specs
  end
  
  

  
end