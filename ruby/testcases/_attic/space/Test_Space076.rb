=begin 
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.
    
Author: U. Koerner, 2012-01-11
=end

require_relative 'Test_Space_Setup'


# Test Space/SIL CAs related to future hold
class Test_Space076 < Test_Space_Setup
  @@datacheck_parameter_inline_enabled_is_set = false  # override in Test_Space076.par to match advanced.properties
  @@mpd_qa = 'QA-MB-FTDEPM-LIS-CAEX.01'
  @@chambers = {'PM1'=>5, 'PM2'=>6, 'CHX'=>7}
  @@resp_opnum_1 = '1000.3500'
  @@resp_opnum_2 = '2700.1000'
  @@resp_start_2 = '2000.1000'
  @@resp_opnum_3 = ['2000.1000', '2700.1000', '2700.2000', '2800.1000']
  @@resp_start_4 = '2500.1000'

  
  def setup
    super
    return unless $lds && self.class.class_variable_defined?(:@@lot) # bypass test0000_setup
    assert_equal 0, $sv.eqp_cleanup(@@eqp)
    assert_equal 0, $sv.lot_cleanup(@@lot)
    assert_equal 0, $sv.inhibit_cancel(:route, $sv.lot_info(@@lot).route)
    #
    $lds.all_spc_channels('QA_PARAM_90*').each {|ch| assert ch.delete}
    folder = $lds.folder(@@autocreated_qa)
    assert folder.delete_channels, "error cleaning channels" if folder
  end


  def test0700_setup
    $setup_ok = false
    #
    assert @@ppd_qa = $sildb.pd_reference(mpd: @@mpd_qa).first, "no PD reference found for #{@@mpd_qa}"
    #
    ['AutoHoldAtFuturePD-TC0760', 'AutoHoldAtFuturePD-TC0761', 'AutoHoldAtFuturePD-TC0762', 
      'AutoHoldAtFuturePD-TC0763', 'AutoHoldAtFuturePD-TC0764', 'AutoHoldAtFuturePD-TC0765',
      'AutoHoldAtFuturePD-TC0766-1', 'AutoHoldAtFuturePD-TC0766-2', 'AutoHoldAtFuturePD-TC0766-3', 'AutoHoldAtFuturePD-TC0767'    
    ].each {|action|
      assert $spc.corrective_action(action), "CA #{action.inspect} not defined"
    }
    #
    if $env == 'f8stag'
      @@cafailure_lothold_fallback = false
      @@resp_opnum_1 = '1000.1410'
      @@resp_opnum_2 = '1010.1200'
      @@resp_start_2 = '1010.1175'
      @@resp_opnum_3 = ['1010.1175', '1010.1200', '1010.1250', '1010.3300']
    end
    #
    $setup_ok = true
  end
  

  def test0760_no_wildcards
    channels = create_clean_channels
    assert_equal 0, $sv.eqp_cleanup(@@eqp)
    assert_equal 0, $sv.lot_cleanup(@@lot)
    assert $spctest.assign_verify_cas(channels,
      ['AutoHoldAtFuturePD-TC0760', 'CancelLotHold', 'ReleaseLotHold'])
    dcr = verify_send_DCRs
    #
    # verify future holds
    folder = $lds.folder(@@autocreated_qa)
    ecommment_correct = nil
    @@parameters.each {|p|
      memo = "#{p}[(Raw above specification)"
      assert $spctest.verify_futurehold2(@@lot, memo, dcr_lot: dcr.find_lot_context(@@lot)), "lot must have a future hold"
      hh = $sv.lot_futurehold_list(@@lot, memo: memo)
      assert_equal 1, hh.size, "wrong number of lot holds"
      assert_equal @@resp_opnum_1, hh[0].rsp_op, "wrong responsible operation"
      assert !hh[0].post, "lot hold must not have the POST flag set"
      ecommment_correct = $spctest.verify_ecomment(folder.spc_channel(parameter: p), "FUTURE_HOLD: #{@@lot} #{@@resp_opnum_1}:  reason={X-S")
    }
    # locate lot to responsible PD
    # since SIL 4.12, we separated the reason code for missing param and a violation, therefore, this is necessary now:
    $sv.lot_futurehold_cancel(@@lot, 'X-MP')
    #
    assert_equal 0, $sv.lot_opelocate(@@lot, @@resp_opnum_1)
    assert $sv.lot_hold_list(@@lot, type: "FutureHold").size > 0, "lot must be on hold"
    # release actions
    @@parameters.each {|p|
      ch = folder.spc_channel(parameter: p)
      s = ch.samples(ckc: true)[-1]
      assert s.assign_ca($spc.corrective_action("ReleaseLotHold")), "error assigning corective action"
    }
    assert $spctest.verify_lothold_released(@@lot, nil), "no further holds expected"
    # Check this at the end of the test because the test has to be completed first
    # known issue
    ##assert(ecommment_correct, "The external comment for the future hold from above has to be something like: FutureHold set for PD xyz. LOT_HELD: reason={X-S is not correct here.") if ecommment_correct != nil
  end
  
  def test0761_missing_filter
    channels = create_clean_channels
    assert_equal 0, $sv.eqp_cleanup(@@eqp)
    assert_equal 0, $sv.lot_cleanup(@@lot)
    assert $spctest.assign_verify_cas(channels,
      ['AutoHoldAtFuturePD-TC0761', 'HoldAtFuturePD-TC0761'])
    verify_send_DCRs
    #
    # verify future hold
    memo = "Execution of [AutoHoldAtFuturePD-TC0761] failed"
    # verify future holds
    folder = $lds.folder(@@autocreated_qa)
    if @@cafailure_lothold_fallback
      assert $spctest.verify_futurehold2(@@lot, memo), "lot must have a future hold"
      channels = @@parameters.collect {|p| folder.spc_channel(parameter: p)}
      assert $spctest.verify_ecomment(channels, "LOT_HELD: #{@@lot}: reason={X-S")
      $log.info "Future hold set as fallback"
    else
      @@parameters.each {|p|
        assert $spctest.verify_notification(memo, p, msgcount: 2 * @@wafer_values2.count), "no notification for #{p}"
      }
    end
    #Manual CA
    ch = folder.spc_channel(parameter: @@parameters[-1])
    s = ch.samples(ckc: true)[-1]
    assert !s.assign_ca($spc.corrective_action("HoldAtFuturePD-TC0761")), "assigning corective action should return an error"
  end
  
  def test0762_post
    channels = create_clean_channels
    assert_equal 0, $sv.eqp_cleanup(@@eqp)
    assert_equal 0, $sv.lot_cleanup(@@lot)
    assert $spctest.assign_verify_cas(channels,
      ['AutoHoldAtFuturePD-TC0762', 'HoldAtFuturePD-TC0762'])
    dcr = verify_send_DCRs
    #
    # verify future holds
    ecommment_correct = nil
    folder = $lds.folder(@@autocreated_qa)
    @@parameters.each {|p|
      memo = "#{p}[(Raw above specification)"
      assert $spctest.verify_futurehold2(@@lot, memo, dcr_lot: dcr.find_lot_context(@@lot)), "lot must have a future hold"
      hh = $sv.lot_futurehold_list(@@lot, memo: memo)
      assert_equal 1, hh.size, "wrong number of lot holds"
      assert_equal @@resp_opnum_1, hh[0].rsp_op, "wrong responsible operation"
      assert hh[0].post, "lot hold must have the POST flag set"
      ecommment_correct = $spctest.verify_ecomment(folder.spc_channel(parameter: p), "FUTURE_HOLD: #{@@lot} #{@@resp_opnum_1}:  reason={X-S")
    }
    ch = folder.spc_channel(parameter: @@parameters[-1])
    set_verify_manual_ca(ch, dcr, "HoldAtFuturePD-TC0762", @@resp_opnum_1, true)
    # Check this at the end of the test because the test has to be completed first.
    # known issue
    ##assert(ecommment_correct, "The external comment for the future hold from above has to be something like: FutureHold set for PD xyz. LOT_HELD: reason={X-S is not correct here.") if ecommment_correct != nil
  end
  
  def test0763_post_wildcards
    channels = create_clean_channels
    assert_equal 0, $sv.eqp_cleanup(@@eqp)
    assert_equal 0, $sv.lot_cleanup(@@lot)
    assert_equal 0, $sv.lot_opelocate(@@lot, @@resp_start_2)
    assert $spctest.assign_verify_cas(channels,
      ['AutoHoldAtFuturePD-TC0763', 'HoldAtFuturePD-TC0763'])
    dcr = verify_send_DCRs
    #
    # verify future holds
    ecommment_correct = nil
    folder = $lds.folder(@@autocreated_qa)
    @@parameters.each {|p|
      memo = "#{p}[(Raw above specification)"
      assert $spctest.verify_futurehold2(@@lot, memo, dcr_lot: dcr.find_lot_context(@@lot)), "lot must have a future hold"
      hh = $sv.lot_futurehold_list(@@lot, memo: memo)
      assert_equal 1, hh.size, "wrong number of lot holds"
      assert_equal @@resp_opnum_2, hh[0].rsp_op, "wrong responsible operation"
      assert hh[0].post, "lot hold must have the POST flag set"
      ecommment_correct = $spctest.verify_ecomment(folder.spc_channel(parameter: p), "FUTURE_HOLD: #{@@lot} #{@@resp_opnum_1}:  reason={X-S")
    }
    #Manual CA
    ch = folder.spc_channel(parameter: @@parameters[0])
    set_verify_manual_ca(ch, dcr, "HoldAtFuturePD-TC0763", @@resp_opnum_2, true)
    # Check this at the end of the test because the test has to be completed first.
    # known issue
    ##assert(ecommment_correct, "The external comment for the future hold from above has to be something like: FutureHold set for PD xyz. LOT_HELD: reason={X-S is not correct here.") if ecommment_correct != nil
  end
  
  def test0764_no_wildcards_pre
    channels = create_clean_channels
    assert_equal 0, $sv.eqp_cleanup(@@eqp)
    assert_equal 0, $sv.lot_cleanup(@@lot)
    assert $spctest.assign_verify_cas(channels,
      ['AutoHoldAtFuturePD-TC0764', 'HoldAtFuturePD-TC0764'])
    dcr = verify_send_DCRs
    #
    # verify future holds
    ecommment_correct = nil
    folder = $lds.folder(@@autocreated_qa)
    @@parameters.each {|p|
      memo = "#{p}[(Raw above specification)"
      assert $spctest.verify_futurehold2(@@lot, memo, dcr_lot: dcr.find_lot_context(@@lot)), "lot must have a future hold"
      hh = $sv.lot_futurehold_list(@@lot, memo: memo)
      assert_equal 1, hh.size, "wrong number of lot holds"
      assert_equal @@resp_opnum_1, hh[0].rsp_op, "wrong responsible operation"
      assert !hh[0].post, "lot hold must not have the POST flag set"
      assert !hh[0].single, "lot hold must not have the SINGLE flag set"
      ecommment_correct = $spctest.verify_ecomment(folder.spc_channel(parameter: p), "FUTURE_HOLD: #{@@lot} #{@@resp_opnum_1}:  reason={X-S")
    }
    #Manual CA
    ch = folder.spc_channel(parameter: @@parameters[-1])
    set_verify_manual_ca(ch, dcr, "HoldAtFuturePD-TC0764", @@resp_opnum_1, false, false)
    # Check this at the end of the test because the test has to be completed first.
    # known issue
    ##assert(ecommment_correct, "The external comment for the future hold from above has to be something like: FutureHold set for PD xyz. LOT_HELD: reason={X-S is not correct here.") if ecommment_correct != nil
  end
  
  def test0765_pre_wildcards
    channels = create_clean_channels
    assert_equal 0, $sv.eqp_cleanup(@@eqp)
    assert_equal 0, $sv.lot_cleanup(@@lot)
    assert_equal 0, $sv.lot_opelocate(@@lot, @@resp_start_2)
    assert $spctest.assign_verify_cas(channels,
      ['AutoHoldAtFuturePD-TC0765', 'HoldAtFuturePD-TC0765'])
    dcr = verify_send_DCRs
    #
    # verify future holds
    ecommment_correct = nil
    folder = $lds.folder(@@autocreated_qa)
    @@parameters.each {|p|
      memo = "#{p}[(Raw above specification)"
      assert $spctest.verify_futurehold2(@@lot, memo, dcr_lot: dcr.find_lot_context(@@lot)), "lot must have a future hold"
      hh = $sv.lot_futurehold_list(@@lot, memo: memo)
      assert_equal 1, hh.size, "wrong number of lot holds"
      assert_equal @@resp_opnum_2, hh[0].rsp_op, "wrong responsible operation"
      assert !hh[0].post, "lot hold must not have the POST flag set"
      assert !hh[0].single, "lot hold must not have the SINGLE flag set"
      ecommment_correct = $spctest.verify_ecomment(folder.spc_channel(parameter: p), "FUTURE_HOLD: #{@@lot} #{@@resp_opnum_1}:  reason={X-S")
    }
    #Manual CA
    ch = folder.spc_channel(parameter: @@parameters[-1])
    set_verify_manual_ca(ch, dcr, "HoldAtFuturePD-TC0765", @@resp_opnum_2, false, false)
    # Check this at the end of the test because the test has to be completed first.
    # known issue
    ##assert(ecommment_correct, "The external comment for the future hold from above has to be something like: FutureHold set for PD xyz. LOT_HELD: reason={X-S is not correct here.") if ecommment_correct != nil
  end
  
  def test0766_pre_post_wildcards
    channels = create_clean_channels
    assert_equal 0, $sv.eqp_cleanup(@@eqp)
    assert_equal 0, $sv.lot_cleanup(@@lot)
    assert $spctest.assign_verify_cas(channels,
      ['AutoHoldAtFuturePD-TC0766-1', 'AutoHoldAtFuturePD-TC0766-2', 'AutoHoldAtFuturePD-TC0766-3',
      'HoldAtFuturePD-TC0766-1', 'HoldAtFuturePD-TC0766-2', 'HoldAtFuturePD-TC0766-3'])
    assert_equal 0, $sv.lot_opelocate(@@lot, @@resp_start_2)
    # send process DCR
    dcrp = Space::DCR.new(eqp: @@eqp, eqptype: 'PROCESS-CHAMBER', lot: @@lot, op: @@ppd_qa)
    dcrp.add_parameters(@@parameters, @@wafer_values, nocharting: true)
    assert $spctest.send_dcr_verify(dcrp)
    # send DCR with OOC values
    dcr = Space::DCR.new(eqp: @@mtool, lot: @@lot, op: @@mpd_qa)    
    dcr.add_parameter(@@parameters[0], @@wafer_valuesooc)
    assert $spctest.send_dcr_verify(dcr, nsamples: @@wafer_valuesooc.size, nooc: 'all*2'), "DCR not submitted successfully"
    #
    # since SIL 4.12, we separated the reason code for missing param and a violation, therefore, this is necessary now:
    $sv.lot_futurehold_cancel(@@lot, 'X-MP')
    $sv.lot_futurehold_cancel(@@lot, 'X-CC')
    # verify future holds
    hh = $sv.lot_futurehold_list(@@lot)
    # dependency on datacheck_parameter_inline_enabled_is_set seems to be the wrong logic (4.7.x)
    # if @@datacheck_parameter_inline_enabled_is_set
    #   num = 4
    #   resp_ops = @@resp_opnum_3
    # else
       num = 3
       resp_ops = @@resp_opnum_3[1..-1]
    # end
    assert num <= hh.size, "wrong number of future holds"
    ops = hh.collect {|h| h.rsp_op}.sort
    assert_equal resp_ops, ops, "wrong hold operation"
    #
    #Manual CA
    assert_equal 0, $sv.lot_cleanup(@@lot, op: :current)
    folder = $lds.folder(@@autocreated_qa)
    ch = folder.spc_channel(parameter: @@parameters[0])
    s = ch.samples(ckc: true)[-1]
    ["HoldAtFuturePD-TC0766-1", "HoldAtFuturePD-TC0766-2", "HoldAtFuturePD-TC0766-3"].each {|ca|
      assert s.assign_ca($spc.corrective_action(ca)), "error assigning corective action"
    }
    #
    hh = nil
    assert wait_for(timeout: 120) {hh = $sv.lot_futurehold_list(@@lot); hh.size == num}, "wrong number of future holds"
    ops = hh.collect {|h| h.rsp_op}.sort
    assert_equal resp_ops, ops, "wrong hold operation"
  end
  
  def test0767_pre_no_wildcards_and_lothold
    channels = create_clean_channels
    assert_equal 0, $sv.eqp_cleanup(@@eqp)
    assert_equal 0, $sv.lot_cleanup(@@lot)
    assert $spctest.assign_verify_cas(channels,
      ['AutoHoldAtFuturePD-TC0767', 'AutoLotHold'])
    verify_send_DCRs
    # verify future holds
    assert @@spc_hold_reasons.size <= $sv.lot_futurehold_list(@@lot).size
  end
  
  # MSR 550533
  def test0768_hold_at_pd_no_wildcards
    channels = create_clean_channels
    assert_equal 0, $sv.eqp_cleanup(@@eqp)
    assert_equal 0, $sv.lot_cleanup(@@lot)
    assert $spctest.assign_verify_cas(channels,
      ['HoldAtFuturePD-TC0768'])
    dcr = verify_send_DCRs
    #    
    ecommment_correct = nil
    sample_comments = {}
    channels.each {|ch|
      s = ch.samples(ckc: true)[-1]
      sample_comments[s.sid] = "ReleaseHold #{s.sid}"
      # assign CA for future hold
      assert s.assign_ca($spc.corrective_action("HoldAtFuturePD-TC0768"), comment: sample_comments[s.sid]), "error assigning corective action"
      #
      memo = "#{s.parameter}[(" #Raw above specification
      assert $spctest.verify_futurehold2(@@lot, memo, dcr_lot: dcr.find_lot_context(@@lot)), "lot must have a future hold"
      hh = $sv.lot_futurehold_list(@@lot, memo: memo)
      assert_equal 1, hh.size, "wrong number of lot holds"
      assert_equal @@resp_opnum_1, hh[0].rsp_op, "wrong responsible operation"
      assert !hh[0].post, "lot hold must not have the POST flag set"
      sleep 10
      s = ch.samples(ckc: true)[-1]
      ecommment_correct = $spctest.verify_ecomment_sample(s, "FUTURE_HOLD: #{@@lot} #{@@resp_opnum_1}:  reason={X-S"), "Wrong comment"
    }
    # since SIL 4.12, we separated the reason code for missing param and a violation, therefore, this is necessary now:
    $sv.lot_futurehold_cancel(@@lot, 'X-MP')
    $sv.lot_futurehold_cancel(@@lot, 'X-CC')
    # locate lot to responsible PD
    assert_equal 0, $sv.lot_opelocate(@@lot, @@resp_opnum_1)
    assert $sv.lot_hold_list(@@lot, type: "FutureHold").size > 0, "lot must be on hold"
    # release actions
    channels.each {|ch|
      s = ch.samples(ckc: true)[-1]
      assert s.assign_ca($spc.corrective_action("ReleaseLotHold")), "error assigning corective action"
    }
    assert $spctest.verify_lothold_released(@@lot, nil), "no further holds expected"    
    # Check this at the end of the test because the test has to be completed first.
    assert(ecommment_correct, "The external comment for the future hold from above has to be something like: FutureHold set for PD xyz. LOT_HELD: reason={X-S is not correct here.") if ecommment_correct != nil
  end
  

  # currently not for Fab1 (yields 10 holds in Fab1 ??)
  def test07690_lookahead
    skip "not applicaple in #{$env}" if ['itdc', 'let'].member?($env)
    channels = create_clean_channels
    assert_equal 0, $sv.eqp_cleanup(@@eqp)
    assert_equal 0, $sv.lot_cleanup(@@lot)
    assert $spctest.assign_verify_cas(channels,
      ['AutoHoldAtFuturePD-10PDS', 'AutoHoldAtFuturePD-120PDS'])
    dcr = verify_send_DCRs
    #
    # verify future holds
    ecommment_correct = nil
    folder = $lds.folder(@@autocreated_qa)
    @@parameters.each {|p|
      memo = "#{p}[(Raw above specification)"
      assert $spctest.verify_futurehold2(@@lot, memo, dcr_lot: dcr.find_lot_context(@@lot)), "lot must have a future hold"
      hh = $sv.lot_futurehold_list(@@lot, memo: memo)
      assert_equal 1, hh.size, "wrong number of lot holds"
      assert_equal @@resp_opnum_1, hh[0].rsp_op, "wrong responsible operation"
      assert !hh[0].post, "lot hold must not have the POST flag set"
      ecommment_correct = $spctest.verify_ecomment(folder.spc_channel(parameter: p), "FUTURE_HOLD: #{@@lot} #{@@resp_opnum_1}:  reason={X-S")
    }
    assert(ecommment_correct, "The external comment for the future hold from above has to be something like: FutureHold set for PD xyz. LOT_HELD: reason={X-S is not correct here.") if ecommment_correct != nil
  end
  
  def test07691_post_wildcards
    skip "not applicaple in #{$env}" if ['itdc', 'let'].member?($env)
    return if $env.start_with?("f8stag")
    
    channels = create_clean_channels
    assert_equal 0, $sv.eqp_cleanup(@@eqp)
    assert_equal 0, $sv.lot_cleanup(@@lot)
    assert_equal 0, $sv.lot_opelocate(@@lot, @@resp_start_4)
    assert $spctest.assign_verify_cas(channels,
      ['AutoHoldAtFuturePD-TC0769', 'HoldAtFuturePD-TC0769'])
    dcr = verify_send_DCRs
    #
    # verify future holds
    ecommment_correct = nil
    folder = $lds.folder(@@autocreated_qa)
    @@parameters.each {|p|
      memo = "#{p}[(Raw above specification)"
      assert $spctest.verify_futurehold2(@@lot, memo, dcr_lot: dcr.find_lot_context(@@lot)), "lot must have a future hold"
      hh = $sv.lot_futurehold_list(@@lot, memo: memo)
      assert_equal 1, hh.size, "wrong number of lot holds"
      assert_equal @@resp_start_4, hh[0].rsp_op, "wrong responsible operation"
      assert hh[0].post, "lot hold must have the POST flag set"
      ecommment_correct = $spctest.verify_ecomment(folder.spc_channel(parameter: p), "FUTURE_HOLD: #{@@lot} #{@@resp_start_4}:  reason={X-S")
    }
    #Manual CA
    #ch = folder.spc_channel(parameter: @@parameters[0])
    #set_verify_manual_ca(ch, dcr, "HoldAtFuturePD-TC0769", @@resp_start_4, true)
    # Check this at the end of the test because the test has to be completed first.
    #assert(ecommment_correct, "The external comment for the future hold from above has to be something like: FutureHold set for PD xyz. LOT_HELD: reason={X-S is not correct here.") if ecommment_correct != nil
  end
  

  def verify_send_DCRs
    # send process DRC
    dcrp = Space::DCR.new(eqp: @@eqp, eqptype: 'PROCESS-CHAMBER', lot: @@lot, op: @@ppd_qa)
    dcrp.add_parameters(@@parameters, @@wafer_values, nocharting: true)
    assert $spctest.send_dcr_verify(dcrp, exporttag: "#{caller_locations(1,1)[0].label}_p"), "error sending DCR"
    # send DCR with OOC values
    dcr = Space::DCR.new(lot: @@lot, eqp: @@mtool, op: @@mpd_qa).add_parameters(@@parameters, @@wafer_valuesooc)
    assert $spctest.send_dcr_verify(dcr, nsamples: @@parameters.count*@@wafer_valuesooc.size, nooc: 'all*2', 
      exporttag: "#{caller_locations(1,1)[0].label}_m"), "error sending DCR"
    return dcr
  end

  # MSR691919 
  def set_verify_manual_ca(channel, dcr, ca, resp_op, post=true, single=true)
    assert_equal 0, $sv.lot_cleanup(@@lot, op: :current)
    s = channel.samples(ckc: true)[-1]
    assert s.assign_ca($spc.corrective_action(ca)), "error assigning corective action"
    #
    memo = "#{s.parameter}["
    assert $spctest.verify_futurehold2(@@lot, memo, dcr_lot: dcr.find_lot_context(@@lot)), "lot must have a future hold with correct department"
    hh = $sv.lot_futurehold_list(@@lot, memo: memo)
    assert_equal 1, hh.size, "wrong number of lot holds"
    assert_equal resp_op, hh[0].rsp_op, "wrong responsible operation"
    assert_equal post, hh[0].post, "lot hold must #{!post ? "not " : ""}have the POST flag set"
    assert_equal single, hh[0].single, "lot hold must #{!single ? "not " : ""}have the SINGLE flag set"
    ##s = channel.samples(ckc: true)[-1]
    # known issue
    #assert($spctest.verify_ecomment_sample(s, "FUTURE_HOLD: #{@@lot} #{@@resp_opnum_1}:  reason={X-S"), "Wrong comment")
    $spctest.verify_ecomment_sample(s, "FUTURE_HOLD: #{@@lot} #{@@resp_opnum_1}:  reason={X-S")
  end
end
