=begin 
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.
    
Author: Steffen Steidten, 2011-11-25
=end

require_relative 'Test_Space_Setup'


# Test Space/SIL.
class Test_Space03 < Test_Space_Setup
  

  def setup
    super
    return unless $lds && self.class.class_variable_defined?(:@@lot) # bypass test0000_setup
    assert_equal 0, $sv.eqp_cleanup(@@eqp)
    assert_equal 0, $sv.lot_cleanup(@@lot)
    #
    $lds.all_spc_channels('QA_PARAM_90*').each {|ch| assert ch.delete}
    folder = $lds.folder(@@autocreated_qa)
    assert folder.delete_channels, "error cleaning channels" if folder
  end


  def test0300_module_default_template
    $setup_ok = false
    #
    # ensure module default template exist
    folder = $lds.folder(@@templates_folder)
    folder.spc_channel("#{@@default_template_qa}_renamed").name = @@default_template_qa unless folder.spc_channel(@@default_template_qa)
    assert folder.spc_channel(@@default_template_qa), "missing template {@@default_template_qa}"
    #
    # ensure parameters to be used are not configured for autocharting
    parameters = @@parameters.collect {|p| "#{p}DEF"}
    parameters.each {|p| assert_empty $sildb.autocharting(parameter: p), "parameter #{p} has an autochart configuration"}
    $log.info "using parameters #{parameters.inspect}"
    #
    # clean up test folders
    ['AutoCreated_null', @@module_qa, @@autocreated_nodep, @@autocreated_qa].each {|f|
      folder = $lds.folder(f)
      assert folder.delete_channels, "error deleting channels in #{folder.name}" if folder
    }
    #
    # build and submit DCR
    dcr = Space::DCR.new(eqp: @@eqp, lot: @@lot)
    dcr.add_parameters(parameters, @@wafer_values)
    assert $spctest.send_dcr_verify(dcr, nsamples: parameters.size*@@wafer_values.size), "DCR not submitted successfully"
    #
    # verify spc channels
    folder = $lds.folder(@@autocreated_qa)
    parameters.each {|p|
      ch = folder.spc_channel(parameter: p)
      ref_ch = Space::SpcApi::ChannelData.new(nil, nil, "Auto-created using template: #{@@default_template_qa}", p)
      assert $spctest.verify_channel(ch, ref_ch, dcr, @@wafer_values), "wrong channel data"
    }
    #
    $setup_ok = true
  end
  
  def test0302_no_templates
    # rename templates
    folder = $lds.folder(@@templates_folder)
    [@@default_template_qa, @@default_template].each {|t|
      $log.info "rename channel #{t}"
      ch = folder.spc_channel(t)
      ch.name = "#{t}_renamed" if ch
      assert folder.spc_channel("#{t}_renamed"), "error renaming template #{t}"
    }
    #
    # build and submit DCR
    parameters = @@parameters.collect {|p| "#{p}DEF"}
    dcr = Space::DCR.new(eqp: @@eqp, lot: @@lot)
    dcr.add_parameters(parameters, @@wafer_values)
    assert $spctest.send_dcr_verify(dcr, spcerror: true)
    #
    # verify no spc channels in AutoCreated_QA
    folder = $lds.folder(@@autocreated_qa)
    parameters.each {|p| assert_empty folder.spc_channels(parameter: p), "no SPC channel must be created for #{p}"}
  end
  
  def test0303_no_templates_listening    ## OBSOLETE but works
    # build and submit DCR
    parameters = @@parameters.collect {|p| "#{p}DEF"}
    dcr = Space::DCR.new(eqp: @@eqp, lot: @@lot)
    dcr.add_parameters(parameters, @@wafer_values, space_listening: true)
    assert $spctest.send_dcr_verify(dcr, spcerror: true)
    #
    # verify no spc channels in AutoCreated_QA
    folder = $lds.folder(@@autocreated_qa)
    parameters.each {|p| assert_empty folder.spc_channels(parameter: p), "no SPC channel must be created for #{p}"}
  end

  def test0310_no_department
    folder = $lds.folder('AutoCreated_null')
    folder.delete_channels if folder
    # need to reset templates first (??)
    assert $spctest.templates_exist($lds.folder(@@templates_folder), [@@default_template, @@default_template_qa, @@params_template])
    #
    # build and submit DCR
    parameters = @@parameters.collect {|p| "#{p}DEF"}
    dcr = Space::DCR.new(eqp: @@eqp, lot: @@lot, department: nil)
    dcr.add_parameters(parameters, @@wafer_values)
    assert $spctest.send_dcr_verify(dcr, nsamples: parameters.size*@@wafer_values.size), "DCR not submitted successfully"
    #
    # verify spc channels
    folder = $lds.folder('AutoCreated_null')
    parameters.each {|p|
      ch = folder.spc_channel(parameter: p)
      ref_ch = Space::SpcApi::ChannelData.new(nil, nil, "Auto-created using template: #{@@default_template}", p)
      assert $spctest.verify_channel(ch, ref_ch, dcr, @@wafer_values), "wrong channel data"
    }
  end

end
