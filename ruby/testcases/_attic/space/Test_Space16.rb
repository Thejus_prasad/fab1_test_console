=begin 
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.
    
Author: Daniel Steger, 2012-02-09
=end

require_relative 'Test_Space_Setup'


# Test Space/SIL NonWIP DCRs
class Test_Space16 < Test_Space_Setup
  @@file_generic_keys_dc_params_nonwip = 'etc/testcasedata/space/GenericKeySetup_QA_DC_PARAMS.SpecID-NonWip.xlsx'
  @@file_generic_keys_fixed_params_nonwip = 2.times.collect{|i| "etc/testcasedata/space/GenericKeySetup_nonwip_FIXED_#{i}.SpecID.xlsx"}
  @@reticle = 'QAReticle0001'
  @@wafer_parameters = ['QA_PARAM_900W', 'QA_PARAM_901W', 'QA_PARAM_902W', 'QA_PARAM_903W', 'QA_PARAM_904W']
  @@eqp_parameters = ['QA_PARAM_900E', 'QA_PARAM_901E', 'QA_PARAM_902E', 'QA_PARAM_903E', 'QA_PARAM_904E']
  @@lot_parameters = ['QA_PARAM_900L', 'QA_PARAM_901L', 'QA_PARAM_902L', 'QA_PARAM_903L', 'QA_PARAM_904L']


  def setup
    super
    return unless $lds && self.class.class_variable_defined?(:@@lot) # bypass test0000_setup
    assert_equal 0, $sv.eqp_cleanup(@@eqp)
    assert_equal 0, $sv.lot_cleanup(@@lot)
    assert_equal 0, $sv.inhibit_cancel(:reticle, @@reticle)
    #
    [$lds, $lds_setup].each {|lds|
      lds.all_spc_channels('QA_PARAM_90*').each {|ch| assert ch.delete}
      folder = lds.folder(@@autocreated_qa)
      assert folder.delete_channels, 'error cleaning channels' if folder
    }
    folder = $lds_setup.folder(@@autocreated_nonwip)
    folder.delete_channels if folder
  end


  def test1600_setup
    $setup_ok = false
    #
    ['ReticleInhibit', 'AutoReticleInhibit', 'ReleaseReticleInhibit'].each {|action|
      assert $spc.corrective_action(action), "CA #{action.inspect} not defined"
    }
    #
    $setup_ok = true
  end
  
  #see MSR512888  
  def test1601_noneqp1
    eqp_value = {@@eqp=>'22.0'}
    dcr = Space::DCR.new(eqp: @@eqp, lot: @@lot, eventReportingName: 'non_wip_dcr', jobsetup: false)
    dcr.add_parameters(@@parameters, eqp_value, add_wafer: false, reading_type: 'Equipment', value_type: 'double')
    res = $client.send_dcr(dcr, export: :caller)
    assert $spctest.verify_dcr_accepted(res, @@parameters.size), 'DCR not submitted successfully'
      
    ekeys = {'PTool'=>@@eqp, 'Event'=>dcr.context['eventReportingName']}
    assert $spctest.verify_parameters(@@parameters, $lds_setup.folder(@@autocreated_nonwip), eqp_value.size, ekeys)
  end
  
  # MSR558175
  def test1602_nonwip_nocharting
    eqp_value = {@@eqp=>'22.0'}
    dcr = Space::DCR.new(eqp: @@eqp, lot: @@lot, jobsetup: false)
    dcr.add_parameters(@@parameters, eqp_value, add_wafer: false, reading_type: 'Equipment', value_type: 'double', nocharting: true)
    res = $client.send_dcr(dcr, export: :caller)
    assert $spctest.verify_dcr_accepted(res, 0), 'No samples should be uploaded'
  end

  # MSR558175
  def test1603_nonwip_invalid
    eqp_value = {@@eqp=>[22.0,30.1,23.0]}
    dcr = Space::DCR.new(eqp: @@eqp, lot: @@lot, jobsetup: false)
    #Reading is invalid
    dcr.add_parameter(@@parameters[0], eqp_value, add_wafer: false, reading_type: 'Equipment', value_type: 'double')
    dcr.add_readings({@@eqp=>102.21}, reading_type: 'Equipment', value_type: 'double', reading_valid: false)
    # Parameter invalid attribute should be ignored, readings are valid
    dcr.add_parameter(@@parameters[1], eqp_value, add_wafer: false, reading_type: 'Equipment', value_type: 'double', parameter_valid: false)
    # All readings invalid
    dcr.add_parameter(@@parameters[2], eqp_value, add_wafer: false, reading_type: 'Equipment', value_type: 'double', reading_valid: false)
           
    res = $client.send_dcr(dcr, export: :caller)
    assert $spctest.verify_dcr_accepted(res, 1), 'DCR not submitted successfully'
     
    $spctest.verify_parameters(@@parameters[0], $lds_setup.folder(@@autocreated_nonwip), 1)
  end
    
  # DCR with job context, but only equipment parameters
  def test1605_nonwip_jobcontext
    eqp_value = {@@eqp=>'28.0'}
    dcr = Space::DCR.new(eqp: @@eqp, lot: @@lot)
    dcr.add_parameters(@@parameters, eqp_value, add_wafer: false, reading_type: 'Equipment', value_type: 'double')
    res = $client.send_dcr(dcr, export: :caller)
    assert $spctest.verify_dcr_accepted(res, @@parameters.size, wip: 'mixed'), 'DCR not submitted successfully'
    $spctest.verify_parameters(@@parameters[0], $lds_setup.folder(@@autocreated_nonwip), 1, {'PTool'=>@@eqp})
  end

  def test1606_chamber_subprocarea
    assert $spctest.sil_upload_verify_generickey(@@file_generic_keys_fixed_params_nonwip[0])

    eqp_value = {@@eqp=>'22.0'}
    dcr = Space::DCR.new(eqp: @@eqp, lot: @@lot, eventReportingName: 'non_wip_dcr', jobsetup: false)
    dcr.add_parameters(@@parameters, eqp_value, processing_areas: 'cycle_parameter', reading_type: 'Equipment', value_type: 'double')
    res = $client.send_dcr(dcr, export: :caller)
    assert $spctest.verify_dcr_accepted(res, @@parameters.size), 'DCR not submitted successfully'

    folder = $lds_setup.folder(@@autocreated_nonwip)
    @@parameters.each_with_index do |p, i|
      ch = folder.spc_channel(parameter: p)
      cha = @@chambers.keys[i % @@chambers.keys.count]
      
      samples = ch.samples
      assert_equal eqp_value.size, samples.size, "wrong number of samples in #{ch.inspect}"
      samples.each {|s|
        assert $spctest.verify_data(s.extractor_keys, {'PTool'=>@@eqp, 'PChamber'=>cha, 'Event'=>dcr.context['eventReportingName'],
          'Reserve4'=>"#{cha}_SIDE1", 'Reserve5'=>@@eqp, 'Reserve6'=>cha, 'Reserve7'=>dcr.context['reportingTimeStamp'], 'Reserve8'=>dcr.context['dcpArmorPath']})
        assert $spctest.verify_data(s.data_keys, {'DatReserve4'=>"#{cha}_SIDE1", 'DatReserve5'=>@@eqp, 'DatReserve6'=>cha,
            'DatReserve7'=>dcr.context['reportingTimeStamp'], 'DatReserve8'=>dcr.context['dcpArmorPath']})
      }
    end
  end

  def test1607_chamber_subprocarea_multireading
    assert $spctest.sil_upload_verify_generickey(@@file_generic_keys_fixed_params_nonwip[0])

    eqp_value = {@@eqp=>[22.0,21.0,20.0]}
    dcr = Space::DCR.new(eqp: @@eqp, lot: @@lot, eventReportingName: 'non_wip_dcr', jobsetup: false)
    dcr.add_parameter(@@parameters[0], eqp_value, reading_type: 'Equipment', value_type: 'double', processing_areas: 'cycle_reading')
    res = $client.send_dcr(dcr, export: :caller)
    assert $spctest.verify_dcr_accepted(res, 1), 'DCR not submitted successfully'

    folder = $lds_setup.folder(@@autocreated_nonwip)
    ch = folder.spc_channel(parameter: @@parameters[0])
    samples = ch.samples
    assert_equal 1, samples.size, "wrong number of samples in #{ch.inspect}"
    samples.each_with_index do |s,i|
      cha = @@chambers.keys.join(':')
      sub_cha = @@chambers.keys.map{|i| i += '_SIDE1'}.join(':')
      assert $spctest.verify_data(s.extractor_keys, {'PTool'=>@@eqp, 'PChamber'=>cha, 'Event'=>dcr.context['eventReportingName'],
        'Reserve4'=>sub_cha, 'Reserve5'=>@@eqp, 'Reserve6'=>cha, 'Reserve7'=>dcr.context['reportingTimeStamp'], 'Reserve8'=>dcr.context['dcpArmorPath']})
      assert $spctest.verify_data(s.data_keys, {'DatReserve4'=>sub_cha, 'DatReserve5'=>@@eqp, 'DatReserve6'=>cha,
        'DatReserve7'=>dcr.context['reportingTimeStamp'], 'DatReserve8'=>dcr.context['dcpArmorPath']})
    end

  end
  
  #DCR with job context, but only equipment parameters
  def test1608_dummydcr
    parameter = 'Dummy'
    wafer_values = {'2502J493KO'=>'', '2502J494KO'=>'', '2502J495KO'=>'', 
      '2502J496KO'=>'', '2502J497KO'=>'', '2502J498KO'=>''}
    
    dcr = Space::DCR.new(eqp: @@eqp, lot: @@lot)
    dcr.add_parameter(parameter, wafer_values, value_type: 'string', nocharting: false)
    assert $spctest.send_dcr_verify(dcr), 'DCR submitted successfully, no samples uploaded'  
  end
  
  # MSR669435
  def test16090_nonwip_but_parameters_and_generickey_dc_double
    # upload setup file with invalid key
    assert $spctest.sil_upload_verify_generickey(@@file_generic_keys_dc_params_nonwip), 'file upload failed'
    
    parameters = ['QA-NonWip-Test-01', 'QA-NonWip-Test-02', 'QA-NonWip-Test-03', 'QA-NonWip-Test-04', 'QA-NonWip-Test-05', 'QA-NonWip-Test-06', 'QA-NonWip-Test-07', 'QA-NonWip-Test-08']
    gkvals = [22.0, 23.0, 24.0, 25.0, 26.0, 27.0, 28.0, 29.0]
    
    dcr = Space::DCR.new(eqp: @@eqp, lot: @@lot, jobsetup: false)
    parameters.size.times {|i| 
      dcr.add_parameter(parameters[i], {@@eqp=>[gkvals[i]]}, add_wafer: false, reading_type: 'Equipment', value_type: 'double', space_listening: false, nocharting: true)
    }
    dcr.add_parameter(@@parameters[0], {@@eqp=>[555]}, add_wafer: false, reading_type: 'Equipment', value_type: 'double', space_listening: true, nocharting: false)
    res = $client.send_dcr(dcr, export: :caller)
    assert $spctest.verify_dcr_accepted(res, 1), 'DCR not submitted successfully'
    
    folder = $lds_setup.folder(@@autocreated_nonwip)
    ch = folder.spc_channel(parameter: @@parameters[0])
    samples = ch.samples
    assert_equal 1, samples.size, "wrong number of samples in #{ch.inspect}"
    # verify sample generic keys
    samples.each {|s|
      assert(s.extractor_keys['Reserve1'] == gkvals[5].to_s, '')
      assert(s.extractor_keys['Reserve2'] == gkvals[6].to_s, '')
      assert(s.extractor_keys['Reserve3'] == gkvals[7].to_s, '')
      assert(s.extractor_keys['Reserve4'] == gkvals[0].to_s, '')
      assert(s.extractor_keys['Reserve5'] == gkvals[1].to_s, '')
      assert(s.extractor_keys['Reserve6'] == gkvals[2].to_s, '')
      assert(s.extractor_keys['Reserve7'] == gkvals[3].to_s, '')
      assert(s.extractor_keys['Reserve8'] == gkvals[4].to_s, '')
      assert(s.data_keys['DatReserve1'] == gkvals[5].to_s, '')
      assert(s.data_keys['DatReserve2'] == gkvals[6].to_s, '')
      assert(s.data_keys['DatReserve3'] == gkvals[7].to_s, '')
      assert(s.data_keys['DatReserve4'] == gkvals[0].to_s, '')
      assert(s.data_keys['DatReserve5'] == gkvals[1].to_s, '')
      assert(s.data_keys['DatReserve6'] == gkvals[2].to_s, '')
      assert(s.data_keys['DatReserve7'] == gkvals[3].to_s, '')
      assert(s.data_keys['DatReserve8'] == gkvals[4].to_s, '')
    }
  end
  
  # MSR669435
  def test16091_nonwip_but_parameters_and_generickey_dc_string
    # upload setup file with invalid key
    assert $spctest.sil_upload_verify_generickey(@@file_generic_keys_dc_params_nonwip), 'file upload failed'
    
    parameters = ['QA-NonWip-Test-01', 'QA-NonWip-Test-02', 'QA-NonWip-Test-03', 'QA-NonWip-Test-04', 'QA-NonWip-Test-05', 'QA-NonWip-Test-06', 'QA-NonWip-Test-07', 'QA-NonWip-Test-08']
    gkvals = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H']
    
    dcr = Space::DCR.new(eqp: @@eqp, lot: @@lot, jobsetup: false)
    parameters.size.times {|i| 
      dcr.add_parameter(parameters[i], {@@eqp=>[gkvals[i]]}, add_wafer: false, reading_type: 'Equipment', value_type: 'string', space_listening: false, nocharting: true)
    }
    dcr.add_parameter(@@parameters[1], {@@eqp=>[555]}, add_wafer: false, reading_type: 'Equipment', value_type: 'double', space_listening: true, nocharting: false)
    res = $client.send_dcr(dcr, export: :caller)
    assert $spctest.verify_dcr_accepted(res, 1), 'DCR not submitted successfully'
    
    folder = $lds_setup.folder(@@autocreated_nonwip)
    ch = folder.spc_channel(parameter: @@parameters[1])
    samples = ch.samples
    assert_equal 1, samples.size, "wrong number of samples in #{ch.inspect}"
    # verify sample generic keys
    samples.each {|s|
      assert(s.extractor_keys['Reserve1'] == gkvals[5].to_s, '')
      assert(s.extractor_keys['Reserve2'] == gkvals[6].to_s, '')
      assert(s.extractor_keys['Reserve3'] == gkvals[7].to_s, '')
      assert(s.extractor_keys['Reserve4'] == gkvals[0].to_s, '')
      assert(s.extractor_keys['Reserve5'] == gkvals[1].to_s, '')
      assert(s.extractor_keys['Reserve6'] == gkvals[2].to_s, '')
      assert(s.extractor_keys['Reserve7'] == gkvals[3].to_s, '')
      assert(s.extractor_keys['Reserve8'] == gkvals[4].to_s, '')
      assert(s.data_keys['DatReserve1'] == gkvals[5].to_s, '')
      assert(s.data_keys['DatReserve2'] == gkvals[6].to_s, '')
      assert(s.data_keys['DatReserve3'] == gkvals[7].to_s, '')
      assert(s.data_keys['DatReserve4'] == gkvals[0].to_s, '')
      assert(s.data_keys['DatReserve5'] == gkvals[1].to_s, '')
      assert(s.data_keys['DatReserve6'] == gkvals[2].to_s, '')
      assert(s.data_keys['DatReserve7'] == gkvals[3].to_s, '')
      assert(s.data_keys['DatReserve8'] == gkvals[4].to_s, '')
    }
  end
  
  def test1610_reticle_dcr
    reticle_values = {@@reticle=>'1'}
    
    dcr = Space::DCR.new(eqp: @@eqp, lot: @@lot, jobsetup: false)
    dcr.add_parameters(@@parameters, reticle_values, add_wafer: false, reading_type: 'Reticle', value_type: 'unsigned integer')
    res = $client.send_dcr(dcr, export: "log/#{__method__}_1.xml")
    assert $spctest.verify_dcr_accepted(res, @@parameters.size), 'DCR not submitted successfully'

    folder = $lds_setup.folder(@@autocreated_nonwip)
    channels = @@parameters.collect{|p| folder.spc_channel(parameter: p)}
    channels.each do |ch|      
      samples = ch.samples
      assert_equal reticle_values.size, samples.size, "wrong number of samples in #{ch.inspect}"
      reticle = reticle_values.keys
      samples.each_with_index {|s, i| 
        assert $spctest.verify_data(s.extractor_keys, {'PTool'=>@@eqp, 'Event'=>dcr.context['eventReportingName']})
        assert $spctest.verify_data(s.data_keys, {'MTool'=>@@eqp, 'Reticle'=>reticle[i]})
      }
    end  
    
    # Assign control limits and corrective action 'Reticle Inhibit'
    channels.each do |ch|      
      ch.set_state('Online')
      ch.set_limits({'MEAN_VALUE_UCL'=>2.0, 'MEAN_VALUE_CENTER'=>1.0, 'MEAN_VALUE_LCL'=>0.0})
      $spc.set_corrective_actions(ch, ['ReticleInhibit', 'AutoReticleInhibit', 'ReleaseReticleInhibit'])
    end
    
    # Send new sample ooc
    reticle_values = {@@reticle=>'5'}
    dcr = Space::DCR.new(eqp: @@eqp, lot: @@lot, jobsetup: false)
    dcr.add_parameters(@@parameters, reticle_values, add_wafer: false, reading_type: 'Reticle', value_type: 'unsigned integer')
    res = $client.send_dcr(dcr, export: "log/#{__method__}_2.xml")
    assert $spctest.verify_dcr_accepted(res, @@parameters.size), 'DCR not submitted successfully'
    
    # Verify corrective action
    assert $spctest.verify_inhibit(@@reticle, channels.size, class: 'Reticle', timeout: @@ca_time_out, msg: '(Mean above control limit)', details: true, dcr: dcr, lot: @@lot, spc_run: res, nonwip: true), 'reticle must have an inhibit'
    assert $spctest.verify_ecomment(channels, "RETICLE_HELD:#{@@reticle}: reason={X-S")
    $log.info 'ReticleInhibit automatically set'
    
    # Verify release action    
    #
    # release inhibit
    channels.each{|ch|
      s = ch.samples(ckc: true)[-1]
      assert s.assign_ca($spc.corrective_action('ReleaseReticleInhibit')), 'error assigning corrective action'
    }
    assert $spctest.verify_inhibit(@@reticle, 0, {class: 'Reticle', timeout: @@ca_time_out}), 'reticle must not have an inhibit'
    $log.info 'ReticleInhibit released'
    #
    # set inhibit (manual)
    s = channels[0].samples(ckc: true)[-1]
    assert s.assign_ca($spc.corrective_action('ReticleInhibit')), 'error assigning corrective action'
    assert $spctest.verify_inhibit(@@reticle, 1, {class: 'Reticle', timeout: @@ca_time_out}), 'reticle must have an inhibit'
    assert $spctest.verify_ecomment(channels, "RETICLE_HELD:#{@@reticle}: reason={X-S")
    $log.info 'ReticleInhibit set (manually)'
    #
    # release inhibit
    assert s.assign_ca($spc.corrective_action('ReleaseReticleInhibit')), 'error assigning corective action'
    assert $spctest.verify_inhibit(@@reticle, 0, {class: 'Reticle', timeout: @@ca_time_out}), 'reticle must not have an inhibit'
    $log.info 'ReticleInhibit released'
  end

  def test1612_reticle_dcr_nocharting
    reticle_values = {@@reticle=>'1'}
    dcr = Space::DCR.new(eqp: @@eqp, lot: @@lot, jobsetup: false)
    dcr.add_parameters(@@parameters, reticle_values, add_wafer: false, reading_type: 'Reticle', value_type: 'unsigned integer', nocharting: true)
    assert $spctest.send_dcr_verify(dcr), 'No runs should be uploaded to space'
  end

    #see MSR512888
  def test1613_reticle_dcr_invalid
    reticle_values = {@@reticle=>'1'}

    dcr = Space::DCR.new(eqp: @@eqp, lot: @@lot, jobsetup: false)
    #Reading is invalid
    dcr.add_parameter(@@parameters[0], reticle_values, add_wafer: false, reading_type: 'Reticle', value_type: 'double')
    dcr.add_readings({@@reticle=>102.21}, reading_type: 'Reticle', value_type: 'double', reading_valid: false)
    # Parameter invalid attribute should be ignored, readings are valid
    dcr.add_parameter(@@parameters[1], reticle_values, add_wafer: false, reading_type: 'Reticle', value_type: 'double', parameter_valid: false)
    # All readings invalid
    dcr.add_parameter(@@parameters[2], reticle_values, add_wafer: false, reading_type: 'Reticle', value_type: 'double', reading_valid: false)

    assert $spctest.send_dcr_verify(dcr, nsamples: 1), 'error sending DCR'
    $spctest.verify_parameters(@@parameters[0], $lds_setup.folder(@@autocreated_nonwip), 1)
  end

  def test1614_chamber_subprocarea_sap
    assert $spctest.sil_upload_verify_generickey(@@file_generic_keys_fixed_params_nonwip[1])
    sapicid = '0010'
    sapir = 'OK'
    sapoo = '0095'
    sapdcpid = '60012916'
    sapon = '67853957'
    eqp_value = {@@eqp=>'22.0'}
    dcr = Space::DCR.new(eqp: @@eqp, lot: @@lot, eventReportingName: 'non_wip_dcr', sapjob: true, cj: nil)
    dcr.add_parameters(@@parameters, eqp_value, processing_areas: 'cycle_parameter', reading_type: 'Equipment', value_type: 'double')
    res = $client.send_dcr(dcr, export: :caller)
    assert $spctest.verify_dcr_accepted(res, @@parameters.size), 'DCR not submitted successfully'

    folder = $lds_setup.folder(@@autocreated_nonwip)
    @@parameters.each_with_index do |p, i|
      ch = folder.spc_channel(parameter: p)
      cha = @@chambers.keys[i % @@chambers.keys.count]
      samples = ch.samples
      assert_equal eqp_value.size, samples.size, "wrong number of samples in #{ch.inspect}"
      samples.each {|s|
        assert $spctest.verify_data(s.extractor_keys, {'PTool'=>@@eqp, 'PChamber'=>cha, 'Event'=>dcr.context['eventReportingName'],
          'Reserve4'=>"#{cha}_SIDE1", 'Reserve5'=>@@eqp, 'Reserve6'=>cha, 'Reserve7'=>dcr.context['reportingTimeStamp'], 
          'Reserve8'=>sapdcpid, 'Reserve1'=>sapoo, 'Reserve2'=>sapon, 'Reserve3'=>sapicid})
        assert $spctest.verify_data(s.data_keys, {'DatReserve4'=>"#{cha}_SIDE1", 'DatReserve5'=>@@eqp, 'DatReserve6'=>cha,
          'DatReserve7'=>dcr.context['reportingTimeStamp'], 'DatReserve8'=>sapdcpid, 'DatReserve1'=>sapoo,
          'DatReserve2'=>sapon, 'DatReserve3'=>sapicid})
      }
    end
  end
  
  #DCR with job context, equipment parameters, wafer parameters, lot parameters
  def test1615_nonwip_jobcontext
    eqp_value = {@@eqp=>'28.0'}
    lot_value = {@@lot=>'32.0'}
    dcr = Space::DCR.new(eqp: @@eqp, lot: @@lot)
    dcr.add_parameters(@@wafer_parameters, @@wafer_values)
    dcr.add_parameters(@@lot_parameters, lot_value, add_wafer: false, reading_type: 'Lot', value_type: 'double')
    dcr.add_parameters(@@eqp_parameters, eqp_value, add_wafer: false, reading_type: 'Equipment', value_type: 'double')
    res = $client.send_dcr(dcr, export: :caller)
    nsamples = @@wafer_parameters.size * @@wafer_values.size + @@lot_parameters.size * lot_value.size + @@eqp_parameters.size * eqp_value.size
    assert $spctest.verify_dcr_accepted(res, nsamples, wip: 'mixed'), 'DCR not submitted successfully'
    
    folder = $lds.folder(@@autocreated_qa)
    $spctest.verify_parameters(@@wafer_parameters, folder, @@wafer_values.size, {'Technology'=>'32NM'})
    $spctest.verify_parameters(@@lot_parameters, folder, lot_value.size, {'Technology'=>'32NM'})
    $spctest.verify_parameters(@@parameters[0], $lds_setup.folder(@@autocreated_nonwip), 1, {'Technology'=>'-', 'PTool'=>@@eqp})
  end
  
  # DCR with job context, equipment parameters, lot parameters
  def test1616_nonwip_jobcontext
    eqp_value = {@@eqp=>'28.0'}
    lot_value = {@@lot=>'32.0'}
    dcr = Space::DCR.new(eqp: @@eqp, lot: @@lot)
    dcr.add_parameters(@@lot_parameters, lot_value, add_wafer: false, reading_type: 'Lot', value_type: 'double')
    dcr.add_parameters(@@eqp_parameters, eqp_value, add_wafer: false, reading_type: 'Equipment', value_type: 'double')
    res = $client.send_dcr(dcr, export: :caller)
    nsamples = @@lot_parameters.size * lot_value.size + @@eqp_parameters.size * eqp_value.size
    assert $spctest.verify_dcr_accepted(res, nsamples, wip: 'mixed'), 'DCR not submitted successfully'
    
    $spctest.verify_parameters(@@lot_parameters, $lds.folder(@@autocreated_qa), lot_value.size, {'Technology'=>'32NM'})
    $spctest.verify_parameters(@@eqp_parameters, $lds_setup.folder(@@autocreated_nonwip), 1, {'Technology'=>'-', 'PTool'=>@@eqp})
  end
  
  def test1617_noneqp_chartcheck
    parameters = ['QA_PARAM_900_NONWIP_ECP', 'QA_PARAM_901_NONWIP_ECP', 'QA_PARAM_902_NONWIP_ECP', 'QA_PARAM_903_NONWIP_ECP', 'QA_PARAM_904_NONWIP_ECP']
    eqp_value = {@@eqp=>'50.0'}
    dcr = Space::DCR.new(eqp: @@eqp, lot: @@lot, eventReportingName: 'non_wip_dcr', jobsetup: false)
    dcr.add_parameters(parameters, eqp_value, add_wafer: false, reading_type: 'Equipment', value_type: 'double')
    res = $client.send_dcr(dcr, export: :caller)
    assert $spctest.verify_dcr_accepted(res, parameters.size), 'DCR not submitted successfully'
    
    folder = $lds_setup.folder(@@autocreated_nonwip)
    channels = parameters.collect {|p|
      assert ch = folder.spc_channel(parameter: p), "channel for parameter #{p} missing in folder #{@@autocreated_nonwip}"
      ch
    }
    
    channels.each {|ch| assert ch.set_state('Study'), "cannot set channel state Study"}
    
    res = $client.send_dcr(dcr, export: :caller)
    assert $spctest.verify_dcr_accepted(res, parameters.size), 'DCR not submitted successfully'
      
    ekeys = {'PTool'=>@@eqp, 'Event'=>dcr.context['eventReportingName']}
    assert $spctest.verify_parameters(parameters, $lds_setup.folder(@@autocreated_nonwip), eqp_value.size * 2, ekeys)
  end
end
