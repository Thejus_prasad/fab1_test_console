=begin 
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.
    
    
Author: Steffen Steidten, 2011-11-25
=end

require_relative 'Test_Space_Setup'


# Test Space/SIL Generic Keys
# http://myteamsmst/sites/MST/IPMO/prj/space/Dev_Documentation/GenericKeysSetupUseGuide.xlsx
class Test_Space15 < Test_Space_Setup
  @@mpd = 'QA-MB-FTDEPM.01'
  @@mtool = 'QAMeas'

  @@files_generic_keys_fixed = 10.times.collect {|i| "etc/testcasedata/space/GenericKeySetup_QA_FIXED_#{i}.SpecID.xlsx"}
  @@file_generic_keys_fixed_invalid = 'etc/testcasedata/space/GenericKeySetup_QA_FIXED_invalid.SpecID.xlsx'
  @@file_generic_keys_fixed_route = 'etc/testcasedata/space/GenericKeySetup_QA_FIXED_routeudata.SpecID.xlsx'
  @@files_generic_keys_fixed_product = 4.times.collect {|i| "etc/testcasedata/space/GenericKeySetup_QA_FIXED_productudata_#{i}.SpecID.xlsx"}
  
  @@files_generic_keys_dc = 7.times.collect {|i| "etc/testcasedata/space/GenericKeySetup_QA_DC_#{i}.SpecID.xlsx"}
  @@file_generic_keys_dc_tool_second_pd = 'etc/testcasedata/space/GenericKeySetup_QA_DC_tool.SpecID.xlsx'
  @@file_generic_keys_dc_lds = 'etc/testcasedata/space/GenericKeySetup_QA_DC_lds.SpecID.xlsx'  
  @@file_generic_keys_dc_inline = 'etc/testcasedata/space/GenericKeySetup_QA_DC_inline.SpecID.xlsx' 
  @@file_generic_keys_dc_setup = 'etc/testcasedata/space/GenericKeySetup_QA_DC_setup.SpecID.xlsx' 
  @@file_generic_keys_dc_invalid = 'etc/testcasedata/space/GenericKeySetup_QA_DC_invalid.SpecID.xlsx'
  @@file_generic_keys_dc_params = 'etc/testcasedata/space/GenericKeySetup_QA_DC_PARAMS.SpecID.xlsx'
  @@file_generic_keys_dc_params_current = 'etc/testcasedata/space/GenericKeySetup_QA_DC_PARAMS_CURRENT.SpecID.xlsx'
  @@file_generic_keys_dc_wafer_param_lot_param = 'etc/testcasedata/space/GenericKeySetup_QA_DC_WaferParamLotParam.SpecID.xlsx'
  @@file_generic_keys_datatypes = 'etc/testcasedata/space/GenericKeySetup_QA_Datatypes.SpecID.xlsx'
  
  @@files_generic_keys_group = 4.times.collect {|i| "etc/testcasedata/space/GenericKeySetup_QA_Group_#{i}.SpecID.xlsx"}
  @@file_generic_keys_group_route = 'etc/testcasedata/space/GenericKeySetup_QA_GROUP_routeudata.SpecID.xlsx' # Fab1

  @@file_generic_keys_boatslot = 'etc/testcasedata/space/GenericKeySetup_QA_BoatSlot.SpecID.xlsx'
  @@file_generic_keys_chamber = 'etc/testcasedata/space/GenericKeySetup_QA_Chamber.SpecID.xlsx'
  
  @@generic_ekeys_fixed = {
    'Reserve4'=>['PMachineRecipe','PToolChamber','sapdcpid','moduleprocess','product','waferid','subprocessingarea','machinerecipe','component','customer'],
    'Reserve5'=>['department','equipment','timestampstring','process','productgroup','slot','controljob','operationpasscount','saporderoperation','saporderoperation'], 
    'Reserve6'=>['dcparmorpath','facility','runid','operationnumber','foup','port','processjob','logicalrecipe','sapordernumber','sapordernumber'],
    'Reserve7'=>['dcparmorpathwithoutversion','operator','contextid','operationname','batch','buffer','Reticle','manufacturinglayer','sapinspectioncharacteristicid','sapinspectioncharacteristicid'],
    'Reserve8'=>['dcpname','SubLotType','mainprocess','technology','lotid','processingarea','rmsrecipe','photolayer','maskedlotid','maskedlotid']
  }
  @@generic_dkeys_fixed = Hash[@@generic_ekeys_fixed.each_pair.collect {|k, v| ["Dat#{k}", v]}]

  @@generic_ekeys_fixed_invalid = {'Reserve4'=>'F-CVD60X-P.F-P-FTEOS8K15.00', 'Reserve5'=>'-', 
    'Reserve6'=>'/Job DCP/DCP Repository/SEM-AMAT/C-ALL/SEM-YM/11', 'Reserve7'=>'/Job DCP/DCP Repository/SEM-AMAT/C-ALL/SEM-YM', 'Reserve8'=>'SEM-YM'}
  @@generic_dkeys_fixed_invalid = Hash[@@generic_ekeys_fixed_invalid.each_pair.collect {|k, v| ["Dat#{k}", v]}]
      
  @@generic_ekeys_dc = {
    'Reserve1'=>['dcpname','mainprocess','batch','controljob','photolayer','SubLotType','SubLotType'],
    'Reserve2'=>['equipment','moduleprocess','lotid','processjob','component','product','PE10State'],
    'Reserve3'=>['facility','process','waferid','Reticle','saporderoperation','PToolChamber','PToolChamber'],
    'Reserve4'=>['Tool','operator','operationnumber','slot','rmsrecipe','sapordernumber','sapordernumber'], 
    'Reserve5'=>['Chamber','sapdcpid','operationname','port','machinerecipe','sapinspectioncharacteristicid','sapinspectioncharacteristicid'],
    'Reserve6'=>['LogRecipe','timestampstring','technology','buffer','operationpasscount','maskedlotid','maskedlotid'],
    'Reserve7'=>['dcparmorpath','runid','productgroup','processingarea','logicalrecipe','customer','customer'],
    'Reserve8'=>['dcparmorpathwithoutversion','contextid','foup','subprocessingarea','manufacturinglayer','department','department']
  }
  @@generic_dkeys_dc = Hash[@@generic_ekeys_dc.each_pair.collect {|k, v| ["Dat#{k}", v]}]
    
  @@generic_ekeys_dc_setup = {'Reserve1'=>'processingarea', 'Reserve2'=>'logicalrecipe', 'Reserve3'=>'foup', 'Reserve4'=>'equipment', 'Reserve5'=>'logicalrecipe', 
    'Reserve6'=>'mainprocess', 'Reserve7'=>'process', 'Reserve8'=>'dcpname'}
  @@generic_dkeys_dc_setup = Hash[@@generic_ekeys_dc_setup.each_pair.collect {|k, v| ["Dat#{k}", v]}]
    
  @@generic_ekeys_dc_inline = {'Reserve1'=>'customer', 'Reserve2'=>'dcpname', 'Reserve3'=>'department', 'Reserve4'=>'technology', 'Reserve5'=>'product', 
    'Reserve6'=>'productgroup', 'Reserve7'=>'operationpasscount', 'Reserve8'=>'SubLotType'}
  @@generic_dkeys_dc_inline = Hash[@@generic_ekeys_dc_inline.each_pair.collect {|k, v| ["Dat#{k}", v]}]

  @@generic_ekeys_dc_invalid = {'Reserve1'=>'CHA:CHB:CHC', 'Reserve2'=>'CHA_SIDE1:CHA_SIDE2:CHB_SIDE1:CHB_SIDE2:CHC_SIDE1:CHC_SIDE2', 'Reserve3'=>'-', 'Reserve4'=>'-', 
    'Reserve5'=>'-', 'Reserve6'=>'CHA:CHB:CHC', 'Reserve7'=>'CHA_SIDE1:CHA_SIDE2:CHB_SIDE1:CHB_SIDE2:CHC_SIDE1:CHC_SIDE2', 'Reserve8'=>'-'}
  @@generic_dkeys_dc_invalid = Hash[@@generic_ekeys_dc_invalid.each_pair.collect {|k, v| ["Dat#{k}", v]}]
    
  @@generic_ekeys_dc_tool = [{'Reserve1'=>'SEM-YM01', 'Reserve2'=>'UTCSIL01_1', 'Reserve3'=>'Customer_01', 
    'Reserve4'=>'UTCSIL01_1', 'Reserve5'=>'CHD:CHE:CHF', 'Reserve6'=>'F-P-FTEOS8K15.01', 
    'Reserve7'=>'/Job DCP/DCP Repository/SEM-AMAT/C-ALL/SEM-YM01/01', 
    'Reserve8'=>'/Job DCP/DCP Repository/SEM-AMAT/C-ALL/SEM-YM01'},
    {'Reserve1'=>'SEM-YM00', 'Reserve2'=>'UTCSIL01_0', 'Reserve3'=>'Customer_00', 
    'Reserve4'=>'UTCSIL01_0', 'Reserve5'=>'CHA:CHB:CHC', 'Reserve6'=>'F-P-FTEOS8K15.00', 
    'Reserve7'=>'/Job DCP/DCP Repository/SEM-AMAT/C-ALL/SEM-YM00/00', 
    'Reserve8'=>'/Job DCP/DCP Repository/SEM-AMAT/C-ALL/SEM-YM00'}, 
    {'Reserve1'=>'SEM-YM01', 'Reserve2'=>'UTCSIL01_1', 'Reserve3'=>'Customer_01', 
    'Reserve4'=>'UTCSIL01_1', 'Reserve5'=>'CHD:CHE:CHF', 'Reserve6'=>'F-P-FTEOS8K15.01', 
    'Reserve7'=>'/Job DCP/DCP Repository/SEM-AMAT/C-ALL/SEM-YM01/01', 
    'Reserve8'=>'/Job DCP/DCP Repository/SEM-AMAT/C-ALL/SEM-YM01'},
    {'Reserve1'=>'SEM-YM02', 'Reserve2'=>'UTCSIL01_2', 'Reserve3'=>'Customer_02', 
    'Reserve4'=>'UTCSIL01_2', 'Reserve5'=>'CHG:CHH:CHI', 'Reserve6'=>'F-P-FTEOS8K15.02', 
    'Reserve7'=>'/Job DCP/DCP Repository/SEM-AMAT/C-ALL/SEM-YM02/02', 
    'Reserve8'=>'/Job DCP/DCP Repository/SEM-AMAT/C-ALL/SEM-YM02'},
    {'Reserve1'=>'SEM-YM03', 'Reserve2'=>'UTCSIL01_3', 'Reserve3'=>'Customer_03', 
    'Reserve4'=>'UTCSIL01_3', 'Reserve5'=>'CHJ:CHK:CHL', 'Reserve6'=>'F-P-FTEOS8K15.03', 
    'Reserve7'=>'/Job DCP/DCP Repository/SEM-AMAT/C-ALL/SEM-YM03/03', 
    'Reserve8'=>'/Job DCP/DCP Repository/SEM-AMAT/C-ALL/SEM-YM03'}]
  @@generic_dkeys_dc_tool = [{'DatReserve1'=>'SEM-YM00', 'DatReserve2'=>'UTCSIL01_0', 'DatReserve3'=>'Customer_00', 
    'DatReserve4'=>'UTCSIL01_0', 'DatReserve5'=>'CHA:CHB:CHC', 'DatReserve6'=>'F-P-FTEOS8K15.00', 
    'DatReserve7'=>'/Job DCP/DCP Repository/SEM-AMAT/C-ALL/SEM-YM00/00', 
    'DatReserve8'=>'/Job DCP/DCP Repository/SEM-AMAT/C-ALL/SEM-YM00'},
    {'DatReserve1'=>'SEM-YM01', 'DatReserve2'=>'UTCSIL01_1', 'DatReserve3'=>'Customer_01', 
    'DatReserve4'=>'UTCSIL01_1', 'DatReserve5'=>'CHD:CHE:CHF', 'DatReserve6'=>'F-P-FTEOS8K15.01', 
    'DatReserve7'=>'/Job DCP/DCP Repository/SEM-AMAT/C-ALL/SEM-YM01/01', 
    'DatReserve8'=>'/Job DCP/DCP Repository/SEM-AMAT/C-ALL/SEM-YM01'}, 
    {'DatReserve1'=>'SEM-YM00', 'DatReserve2'=>'UTCSIL01_0', 'DatReserve3'=>'Customer_00', 
    'DatReserve4'=>'UTCSIL01_0', 'DatReserve5'=>'CHA:CHB:CHC', 'DatReserve6'=>'F-P-FTEOS8K15.00', 
    'DatReserve7'=>'/Job DCP/DCP Repository/SEM-AMAT/C-ALL/SEM-YM00/00', 
    'DatReserve8'=>'/Job DCP/DCP Repository/SEM-AMAT/C-ALL/SEM-YM00'}, 
    {'DatReserve1'=>'SEM-YM03', 'DatReserve2'=>'UTCSIL01_3', 'DatReserve3'=>'Customer_03', 
    'DatReserve4'=>'UTCSIL01_3', 'DatReserve5'=>'CHJ:CHK:CHL', 'DatReserve6'=>'F-P-FTEOS8K15.03', 
    'DatReserve7'=>'/Job DCP/DCP Repository/SEM-AMAT/C-ALL/SEM-YM03/03', 
    'DatReserve8'=>'/Job DCP/DCP Repository/SEM-AMAT/C-ALL/SEM-YM03'}, 
    {'DatReserve1'=>'SEM-YM02', 'DatReserve2'=>'UTCSIL01_2', 'DatReserve3'=>'Customer_02', 
    'DatReserve4'=>'UTCSIL01_2', 'DatReserve5'=>'CHG:CHH:CHI', 'DatReserve6'=>'F-P-FTEOS8K15.02', 
    'DatReserve7'=>'/Job DCP/DCP Repository/SEM-AMAT/C-ALL/SEM-YM02/02', 
    'DatReserve8'=>'/Job DCP/DCP Repository/SEM-AMAT/C-ALL/SEM-YM02'}]

  @@generic_ekeys_dc_all_hyphen = {'Reserve1'=>'-', 'Reserve2'=>'-', 'Reserve3'=>'-', 'Reserve4'=>'-', 'Reserve5'=>'-', 
    'Reserve6'=>'-', 'Reserve7'=>'-', 'Reserve8'=>'-'}
  @@generic_dkeys_dc_all_hyphen = Hash[@@generic_ekeys_dc_all_hyphen.each_pair.collect {|k, v| ["Dat#{k}", v]}]

  #@@generic_ekeys_dc_params = {'Reserve4'=>'QA_PARAM_GKDC_0', 'Reserve5'=>'QA_PARAM_GKDC_1', 
  #  'Reserve6'=>'QA_PARAM_GKDC_2', 'Reserve7'=>'QA_PARAM_GKDC_3', 'Reserve8'=>'QA_PARAM_GKDC_4'}
  #@@generic_dkeys_dc_params = Hash[@@generic_ekeys_dc_params.each_pair.collect {|k, v| ["Dat#{k}", v]}]
  
  @@generic_ekeys_group = {
    'Reserve4'=>['UTCSIL0x','SIL_LOT','SIL_PRODUCT','UTCSIL0x'], 
    'Reserve5'=>['QA-MB-xTDEP.0x','SIL_WAFER','QA-MB-xTDEP.0x','UTC_Chamber'], 
    'Reserve6'=>['QA-MB-FTDEPM.0x','F-P-FTEOS8K15.0x','C-DR-SEMeVeNt','CHx'], 
    'Reserve7'=>['QAProducts','xxNM','DummyLayer','QAProducts'], 
    'Reserve8'=>['-','SIL_ROUTE','UTCSpecial-FIRST','S2']
  }
  @@generic_dkeys_group = {
    'DatReserve4'=>['UTCSIL0x','SIL_LOT','SIL_PRODUCT','UTCSIL0x'],
    'DatReserve5'=>['QA-MB-xTDEP.0x','SIL_WAFER','QA-MB-xTDEP.0x','UTC_Chamber'], 
    'DatReserve6'=>['QA-MB-FTDEPM.0x','F-P-FTEOS8K15.0x','C-DR-SEMeVeNt','CHx'],
    'DatReserve7'=>['QAProducts','xxNM','DummyLayer','QAProducts'],
    'DatReserve8'=>['UTCSpecial','SIL_ROUTE','UTCSpecial-FIRST','S2']
  }

  @@generic_ekeys_boatslot = {'Reserve4'=>'D-BoatSlot0', 'Reserve5'=>'D-BoatSlot1', 
    'Reserve6'=>'D-BoatSlot2', 'Reserve7'=>'D-BoatSlot3', 'Reserve8'=>'D-BoatSlotM'}
  @@generic_dkeys_boatslot = Hash[@@generic_ekeys_boatslot.each_pair.collect {|k, v| ["Dat#{k}", v]}]



  def setup
    super
    return unless $lds && self.class.class_variable_defined?(:@@lot) # bypass test0000_setup
    assert_equal 0, $sv.eqp_cleanup(@@eqp)
    assert_equal 0, $sv.lot_cleanup(@@lot)
    #
    [$lds, $lds_setup].each {|lds|
      lds.all_spc_channels('QA_PARAM_90*').each {|ch| assert ch.delete}
      folder = lds.folder(@@autocreated_qa)
      assert folder.delete_channels, 'error cleaning channels' if folder
    }
  end


  def test15000_0setup
    $setup_ok = false
    #
    # get process PDs referenced by MPD
    @@ppds = $sildb.pd_reference(mpd: @@mpd)
    assert_equal 9, @@ppds.size, "wrong PD references found for #{@@mpd}: #{@@ppds}"
    $log.info "using process PDs #{@@ppds.inspect}"
    #
    $setup_ok = true
  end

  def test15000_generic_keys_fixed
    $setup_ok = false
    _send_scenario_fixed(0)
    $setup_ok = true
  end
  
  def test15001_generic_keys_fixed
    _send_scenario_fixed(1)
  end
  
  def test15002_generic_keys_fixed
    _send_scenario_fixed(2)
  end
  
  def test15003_generic_keys_fixed
    _send_scenario_fixed(3)
  end
  
  def test15004_generic_keys_fixed
    _send_scenario_fixed(4)
  end
  
  def test15005_generic_keys_fixed
    _send_scenario_fixed(5)
  end
  
  def test15006_generic_keys_fixed
    _send_scenario_fixed(6)
  end
  
  def test15007_generic_keys_fixed
    _send_scenario_fixed(7)
  end
  
  def test15008_generic_keys_fixed
    _send_scenario_fixed(8)
  end
  
  def test15009_generic_keys_fixed
    _send_scenario_fixed(9)
  end
  
  def test15010_generic_keys_fixed_invalid  # obsolete with setup.FC
    # upload setup file with invalid key
    assert $spctest.sil_upload_verify_generickey(@@file_generic_keys_fixed_invalid), 'file upload failed'
    #
    # build and submit DCRs for 5 process PDs
    dcrps = @@ppds.each_with_index.collect {|ppd, i|
      create_process_dcr(ppd: ppd, eqp: "#{@@eqp}_#{i}", logical_recipe: "F-P-FTEOS8K15.0#{i}", machine_recipe: "F-CVD60X-P.F-P-FTEOS8K15.0#{i}")
    }
    assert $spctest.send_dcrs_verify(dcrps), 'error submitting DCRs'
    # 
    # build and submit DCR for related MPD
    dcr = Space::DCR.new(eqp: @@mtool, lot: @@lot, op: @@mpd)
    dcr.add_parameters(@@parameters, @@wafer_values)
    assert $spctest.send_dcr_verify(dcr, nsamples: @@parameters.size * @@wafer_values.size), 'DCR not submitted successfully'
    #
    # verify sample generic keys
    ekeys = @@generic_ekeys_fixed_invalid # dcrps[0].sample_generickeys(mapping: @@generic_ekeys_fixed_invalid)
    dkeys = @@generic_dkeys_fixed_invalid # dcrps[0].sample_generickeys(mapping: @@generic_dkeys_fixed_invalid))
    folder = $lds.folder(@@autocreated_qa)
    assert $spctest.verify_parameters(@@parameters, folder, @@wafer_values.size, ekeys, dkeys)
  end
  
  def test15020_generic_keys_dc
    _send_scenario_dc(0)
  end
  
  def test15021_generic_keys_dc
    _send_scenario_dc(1)
  end
  
  def test15022_generic_keys_dc
    _send_scenario_dc(2)
  end
  
  def test15023_generic_keys_dc
    _send_scenario_dc(3)
  end
  
  def test15024_generic_keys_dc
    _send_scenario_dc(4)
  end
  
  def test15025_generic_keys_dc
    _send_scenario_dc(5)
  end
  
  def test15026_generic_keys_dc
    _send_scenario_dc(6, :pchambers=>{'PM1'=>5, 'PM2'=>6, 'PM3'=>7, 'PM4'=>8, 'PM5'=>9, 'PM6'=>10})
  end
  
  def test15027_generic_keys_dc
    _send_scenario_dc(6, :pchambers=>{'PM1'=>5, 'PM2'=>6, 'PM3'=>7, 'PM4'=>8, 'PM5'=>9, 'PM6'=>10}, technology: 'TEST')
  end
  
  def test15030_generic_keys_dc_invalid  # obsolete with setup.FC 
    # upload setup file
    assert $spctest.sil_upload_verify_generickey(@@file_generic_keys_dc_invalid), 'file upload failed'
    lot = 'SIL006.00'
    assert_equal 0, $sv.lot_cleanup(lot)
    #
    # build and submit DCRs for 5 process PDs (wafer history should not report subprocessingarea)
    dcrps = @@ppds.each_with_index.collect {|ppd, i|
      chambers = (i == 1) ? {} : @@chambers
      create_process_dcr(ppd: ppd, lot: lot, chambers: chambers, eqp: "#{@@eqp}_#{i}", 
        logical_recipe: "F-P-FTEOS8K15.0#{i}", machine_recipe: "F-CVD60X-P.F-P-FTEOS8K15.0#{i}")
    }
    assert $spctest.send_dcrs_verify(dcrps), 'error submitting DCRs'
    # 
    # build and submit DCR for related MPD
    dcr = Space::DCR.new(eqp: @@mtool, lot: lot, op: @@mpd)
    dcr.add_parameters(@@parameters, @@wafer_values)
    res = $client.send_dcr(dcr, export: :caller)
    assert $spctest.verify_dcr_accepted(res, @@parameters.size * @@wafer_values.size), 'DCR not submitted successfully'
    #
    # verify sample generic keys
    ekeys = @@generic_ekeys_dc_invalid
    dkeys = @@generic_dkeys_dc_invalid
    folder = $lds.folder(@@autocreated_qa)
    assert $spctest.verify_parameters(@@parameters, folder, @@wafer_values.size, ekeys, dkeys)
  end
  
  def test15040_generic_keys_group
    _send_scenario_group(0)
  end
  
  def test15041_generic_keys_group
    _send_scenario_group(1)
  end
  
  def test15042_generic_keys_group
    _send_scenario_group(2)
  end
  
  def test15043_generic_keys_group
    _send_scenario_group(3)
  end
  
  def test15050_generic_keys_boatslot
    # upload setup file
    assert $spctest.sil_upload_verify_generickey(@@file_generic_keys_boatslot), 'file upload failed'
    #
    # build and submit DCRs for 5 process PDs
    wvs = []
    dcrps = @@ppds.each_with_index.collect {|ppd, i|
      dcrp = Space::DCR.new(eqp: "#{@@eqp}_#{i}", lot: @@lot, op: ppd)
      wafer_value = Hash[@@wafer_values.keys.zip(["#{i}01", "#{i}02", "#{i}03", "#{i}04", "#{i}05", "#{i}06"])]
      wvs << wafer_value
      dcrp.add_parameter("D-BoatSlot#{i}", wafer_value, nocharting: true)
      dcrp.add_parameters(@@parameters, @@wafer_values, nocharting: true)
    }
    assert $spctest.send_dcrs_verify(dcrps), 'error submitting DCRs'
    # 
    # build and submit DCR for related MPD
    dcr = Space::DCR.new(eqp: @@mtool, lot: @@lot, op: @@mpd)
    wafer_value = Hash[@@wafer_values.keys.zip(['901', '902', '903', '904', '905', '906'])]
    wvs << wafer_value
    dcr.add_parameter('D-BoatSlotM', wafer_value, nocharting: true)    
    dcr.add_parameters(@@parameters, @@wafer_values)
    res = $client.send_dcr(dcr, export: :caller)
    assert $spctest.verify_dcr_accepted(res, @@parameters.size * @@wafer_values.size), 'error submitting DCR'
    #
    # verify sample generic keys
    folder = $lds.folder(@@autocreated_qa)
    @@parameters.each {|p|
      ch = folder.spc_channel(parameter: p)
      samples = ch.samples
      assert_equal @@wafer_values.size, samples.size, "wrong number of samples in #{ch.inspect}"
      samples.each {|s|
        w = s.extractor_keys['Wafer']
        ekeys = {'Reserve4'=>wvs[0][w], 'Reserve5'=>wvs[1][w], 'Reserve6'=>wvs[2][w], 'Reserve7'=>wvs[3][w], 'Reserve8'=>wvs[5][w]}
        assert_equal 5, ekeys.values.compact.size, "wrong or missing wafer data"
        assert $spctest.verify_data(s.extractor_keys, ekeys)
        dkeys = {'DatReserve4'=>wvs[0][w], 'DatReserve5'=>wvs[1][w], 'DatReserve6'=>wvs[2][w], 'DatReserve7'=>wvs[3][w], 'DatReserve8'=>wvs[5][w]}
        assert_equal 5, dkeys.values.compact.size, "wrong or missing wafer data"
        assert $spctest.verify_data(s.data_keys, dkeys)
      }
    }
  end
  
  #TODO: need subprocessing area
  def XXtest15060_generic_keys_chamber
    # upload setup file
    assert $spctest.sil_upload_verify_generickey(@@file_generic_keys_chamber), 'file upload failed'
    #
    # build and submit DCRs for 5 process PDs
    dcrs = @@ppds.each_with_index.collect {|ppd, i|
      dcrp = Space::DCR.new(eqp: "#{@@eqp}_#{i}", lot: @@lot, op: ppd, 
        logical_recipe: "F-P-FTEOS8K15.0#{i}", machine_recipe: "F-CVD60X-P.F-P-FTEOS8K15.0#{i}")
      dcrp.add_parameters(@@parameters, @@wafer_values, nocharting: true, processing_areas: 'cycle_wafer')
    }
    assert $spctest.send_dcrs_verify(dcrs), 'error submitting DCRs'
    # 
    # build and submit DCR for related MPD
    dcr = Space::DCR.new(eqp: @@mtool, lot: @@lot, op: @@mpd)
    dcrs.unshift(dcr)
    dcr.add_parameters(@@parameters, @@wafer_values, processing_areas: [])
    res = $client.send_dcr(dcr, export: :caller)
    assert $spctest.verify_dcr_accepted(res, @@parameters.size * @@wafer_values.size), 'DCR not submitted successfully'
    #
    # verify sample generic keys
    @@parameters.each {|p|
      ch = folder.spc_channel(parameter: p)
      samples = ch.samples
      assert_equal @@wafer_values.size, samples.size, "wrong number of samples in #{ch.inspect}"
      @@wafer_values.each_with_index {|w, i|
        _ch = dcrs[0].processing_areas[i % dcrs[0].processing_areas.size]
        _sub = dcrs[0].subprocessing_areas(_ch).join ':'
        _sub2 = dcrs[2].subprocessing_areas(_ch).join ':'
        _ws = ch.samples(wafer: w)
        _ws.each {|s| 
          generic_ekeys_chamber = {'Reserve4'=>"#{@@eqp}_0#{_ch}", 'Reserve5'=>_sub, 
                              'Reserve6'=>'SIDE2', 'Reserve7'=>'CHx', 'Reserve8'=>_sub2}
          assert $spctest.verify_data(s.extractor_keys, generic_ekeys_chamber)
          generic_dkeys_chamber = {'DatReserve4'=>"#{@@eqp}_0#{_ch}", 'DatReserve5'=>_sub, 
                              'DatReserve6'=>'SIDE2', 'DatReserve7'=>'CHx', 'DatReserve8'=>_sub2}
          assert $spctest.verify_data(s.data_keys, generic_dkeys_chamber)
        }
      }
    }
  end
  
  def test15070_generic_keys_dc_params_wafer_history_exists
    # upload setup file
    assert $spctest.sil_upload_verify_generickey(@@file_generic_keys_dc_params), 'file upload failed'
    #
    2.times {|n|
      $log.info "-- loop #{n}"
      folder = $lds.folder(@@autocreated_qa)
      assert folder.delete_channels if folder
      # build and submit DCRs for 5 process PDs
      wvs = []
      @@ppds.each_with_index {|ppd, i|
        dcrp = Space::DCR.new(eqp: "#{@@eqp}_#{i}", lot: @@lot, op: ppd)
        wafer_value = Hash[@@wafer_values.keys.zip(["GK#{n}_#{i}01", "GK#{n}_#{i}02", "GK#{n}_#{i}03", 
          "GK#{n}_#{i}04", "GK#{n}_#{i}05", "GK#{n}_#{i}06", ])]
        generickey_dc_param = "QA_PARAM_GKDC_#{i}"    
        $log.info "using process values for #{generickey_dc_param} #{wafer_value.inspect}"  
        dcrp.add_parameter(generickey_dc_param, wafer_value, nocharting: true, value_type: 'string')
        wvs << wafer_value
      
        wafer_values_new = Hash[@@wafer_values.collect {|w, v| [w, "#{i}#{v}"]}]
        dcrp.add_parameters(@@parameters, wafer_values_new, nocharting: true, value_type: 'unsigned integer')
        res = $client.send_dcr(dcrp, export: "log/#{__method__}_p_1_#{i}.xml")
        assert $spctest.verify_dcr_accepted(res, 0), "DCR #{i} not submitted successfully"
      }
      # 
      # build and submit DCR for related MPD
      dcr = Space::DCR.new(eqp: @@mtool, lot: @@lot, op: @@mpd)
      wafer_values_meas = Hash[@@wafer_values.collect {|w, v| [w, "6#{v}"]}]
      $log.info "using measurement values #{wafer_values_meas.inspect}"
      dcr.add_parameters(@@parameters, wafer_values_meas, value_type: 'unsigned integer')
      res = $client.send_dcr(dcr, export: "log/#{__method__}_1_m.xml")
      assert $spctest.verify_dcr_accepted(res, @@parameters.size * @@wafer_values.size), 'DCR not submitted successfully'
      #
      # verify sample generic keys
      assert folder = $lds.folder(@@autocreated_qa)
      @@parameters.each {|p|
        ch = folder.spc_channel(parameter: p)
        samples = ch.samples
        assert_equal @@wafer_values.size, samples.size, "wrong number of samples in #{ch.inspect}"
        samples.each {|s|
          w = s.extractor_keys['Wafer']        
          ekeys = {'Reserve4'=>wvs[0][w], 'Reserve5'=>wvs[1][w], 'Reserve6'=>wvs[2][w], 'Reserve7'=>wvs[3][w], 'Reserve8'=>'-'}
          assert_equal 5, ekeys.values.compact.size, "wrong or missing wafer data"
          assert $spctest.verify_data(s.extractor_keys, ekeys)
          dkeys = {'DatReserve4'=>wvs[0][w], 'DatReserve5'=>wvs[1][w], 'DatReserve6'=>wvs[2][w], 'DatReserve7'=>wvs[3][w], 'DatReserve8'=>'-'}
          assert_equal 5, dkeys.values.compact.size, "wrong or missing wafer data"
          assert $spctest.verify_data(s.data_keys, dkeys)
        }
      }
    }
  end
  
  def test15080_generic_keys_dc_params_no_wafer_history_exists
    # upload setup file
    assert $spctest.sil_upload_verify_generickey(@@file_generic_keys_dc_params), 'file upload failed'
    #
    s = Time.now.strftime('%Y%m%d%H%M%S')
    waferids = 6.times.collect {|i| "#{s}#{i}"}
    $log.info "using wafer IDs #{waferids.inspect}"
    gk_wafer_values = Hash[waferids.zip(@@wafer_values.values)]
    #
    2.times {|n|
      $log.info "-- loop #{n}"
      folder = $lds.folder(@@autocreated_qa)
      assert folder.delete_channels if folder
      # build and submit DCRs for 5 process PDs
      dcrs = []
      wvs = []
      @@ppds.each_with_index {|ppd, i|
        dcrp = Space::DCR.new(eqp: "#{@@eqp}_#{i}", lot: @@lot, op: ppd)
        dcrs << dcrp
        wafer_value = Hash[waferids.zip(["GK#{n}_#{i}1_#{waferids[0]}", "GK#{n}_#{i}2_#{waferids[1]}", 
          "GK#{n}_#{i}3_#{waferids[2]}", "GK#{n}_#{i}4_#{waferids[3]}", "GK#{n}_#{i}5_#{waferids[4]}", "GK#{n}_#{i}6_#{waferids[5]}"])]
        generickey_dc_param = "QA_PARAM_GKDC_#{i}"    
        $log.info "using process values for #{generickey_dc_param} #{wafer_value.inspect}"  
        dcrp.add_parameter(generickey_dc_param, wafer_value, nocharting: true, value_type: 'string')
        wvs << wafer_value
      
        wafer_values_new = Hash[gk_wafer_values.collect {|w, v| [w, "#{i}#{v}"]}]
        dcrp.add_parameters(@@parameters, wafer_values_new, nocharting: true, value_type: 'unsigned integer')
        res = $client.send_dcr(dcrp, export: "log/#{__method__}_p_1_#{i}.xml")
        assert $spctest.verify_dcr_accepted(res, 0), "DCR #{i} not submitted successfully"
      }
      # 
      # build and submit DCR for related MPD
      dcr = Space::DCR.new(eqp: @@mtool, lot: @@lot, op: @@mpd)
      dcrs.unshift dcr
      wafer_values_meas = Hash[gk_wafer_values.collect {|w, v| [w, "6#{v}"]}]
      $log.info "using measurement values #{wafer_values_meas.inspect}"
      dcr.add_parameters(@@parameters, wafer_values_meas, value_type: 'unsigned integer')
      res = $client.send_dcr(dcr, export: "log/#{__method__}_1_m.xml")
      assert $spctest.verify_dcr_accepted(res, @@parameters.size * gk_wafer_values.size), 'DCR not submitted successfully'
      #
      # verify sample generic keys
      assert folder = $lds.folder(@@autocreated_qa)
      @@parameters.each {|p|
        ch = folder.spc_channel(parameter: p)
        samples = ch.samples
        assert_equal gk_wafer_values.size, samples.size, "wrong number of samples in #{ch.inspect}"
        samples.each {|s|
          w = s.extractor_keys['Wafer']        
          ekeys = {'Reserve4'=>wvs[0][w], 'Reserve5'=>wvs[1][w], 'Reserve6'=>wvs[2][w], 'Reserve7'=>wvs[3][w], 'Reserve8'=>'-'}
          assert_equal 5, ekeys.values.compact.size, "wrong or missing wafer data"
          assert $spctest.verify_data(s.extractor_keys, ekeys)
          dkeys = {'DatReserve4'=>wvs[0][w], 'DatReserve5'=>wvs[1][w], 'DatReserve6'=>wvs[2][w], 'DatReserve7'=>wvs[3][w], 'DatReserve8'=>'-'}
          assert_equal 5, dkeys.values.compact.size, "wrong or missing wafer data"
          assert $spctest.verify_data(s.data_keys, dkeys)
        }
      }
    }
  end

  def test15090_generic_keys_dc_params_data_types
    # upload setup file
    test_ok = true
    assert $spctest.sil_upload_verify_generickey(@@file_generic_keys_datatypes), 'file upload failed'
    #
    # build and submit DCRs for 5 process PDs
    wafer_values = []
    waferids = @@wafer_values.keys.sort
    @@ppds.each_with_index {|ppd, i|
      dcrp = Space::DCR.new(eqp: "#{@@eqp}_#{i}", lot: @@lot, op: ppd,
        logical_recipe: "F-P-FTEOS8K15.0#{i}", machine_recipe: "F-CVD60X-P.F-P-FTEOS8K15.0#{i}")
            
      wafer_value = {waferids[0]=>2**32-1, waferids[1]=>-2**32, waferids[2]=>0,
        waferids[3]=>1, waferids[4]=>-1, waferids[5]=>2**10}

      generickey_dc_param = 'PARAM_SINT'
      dcrp.add_parameter(generickey_dc_param, wafer_value, nocharting: true, value_type: 'signed integer')
      wafer_values << wafer_value

      wafer_value = {waferids[0]=>2**32-1, waferids[1]=>2**64-1, waferids[2]=>0,
        waferids[3]=>1, waferids[4]=>5, waferids[5]=>2**10}

      generickey_dc_param = 'PARAM_UINT'
      dcrp.add_parameter(generickey_dc_param, wafer_value, nocharting: true, value_type: 'unsigned integer')
      wafer_values << wafer_value

      wafer_value = {waferids[0]=>1.1213232345343E-130, waferids[1]=>1.1213232345343E125, waferids[2]=>0.0,
        waferids[3]=>-1.0, waferids[4]=>2e10, waferids[5]=>2.1213232345343}
          
      generickey_dc_param = 'PARAM_FLOAT'
      dcrp.add_parameter(generickey_dc_param, wafer_value, nocharting: true, value_type: 'float')
      wafer_values << wafer_value

      generickey_dc_param = 'PARAM_DOUBLE'
      dcrp.add_parameter(generickey_dc_param, wafer_value, nocharting: true, value_type: 'double')
      wafer_values << wafer_value

      wafer_value = {waferids[0]=>'1010101', waferids[1]=>'0', waferids[2]=>'1',
        waferids[3]=>'1111111111', waferids[4]=>'10101010', waferids[5]=>'10101010'}

      generickey_dc_param = 'PARAM_BINARY'
      dcrp.add_parameter(generickey_dc_param, wafer_value, nocharting: true, value_type: 'binary')
      wafer_values << wafer_value

      wafer_value = {waferids[0]=>'yes', waferids[1]=>'y', waferids[2]=>'n',
        waferids[3]=>'N', waferids[4]=>'n', waferids[5]=>'1'}

      generickey_dc_param = 'PARAM_BOOL'
      dcrp.add_parameter(generickey_dc_param, wafer_value, nocharting: true, value_type: 'boolean')
      wafer_values << wafer_value

      wafer_values_new = Hash[*@@wafer_values.collect {|w,v| [w,"#{i}#{v}"]}.flatten]
      #$log.info "using process values #{wafer_values_new.inspect}"
      dcrp.add_parameters(@@parameters, wafer_values_new, nocharting: true, value_type: 'unsigned integer')
      res = $client.send_dcr(dcrp, export: "log/#{__method__}_p_1_#{i}.xml")
      assert $spctest.verify_dcr_accepted(res, 0), "DCR #{i} not submitted successfully"
    }
    #
    # build and submit DCR for related MPD
    dcr = Space::DCR.new(eqp: @@mtool, lot: @@lot, op: @@mpd)
    number = 0
    wafer_values_meas = Hash[*@@wafer_values.collect {|w, v| [w,"#{number += 1}#{v}"]}.flatten]
    $log.info "using measurement values #{wafer_values_meas.inspect}"
    dcr.add_parameters(@@parameters, wafer_values_meas, value_type: 'unsigned integer')
    res = $client.send_dcr(dcr, export: "log/#{__method__}_1_m.xml")
    $spctest.verify_dcr_accepted(res, @@parameters.size * wafer_values_meas.size)
    #
    # verify sample generic keys
    folder = $lds.folder(@@autocreated_qa)
    @@parameters.each {|p|
      ch = folder.spc_channel(parameter: p)
      samples = ch.samples
      assert_equal wafer_values_meas.size, samples.size, "wrong number of samples in #{ch.inspect}"
      samples.each {|s|
        w = s.extractor_keys['Wafer']
        $log.info "  using wafer: #{w}"
        generic_ekeys_dc_param = {'Reserve4'=>wafer_values[0][w].to_f, 'Reserve5'=>wafer_values[1][w].to_f,
          'Reserve6'=>wafer_values[2][w].to_f, 'Reserve7'=>wafer_values[3][w].to_f, 'Reserve8'=>wafer_values[4][w]}
        ekeys = {'Reserve4'=>s.extractor_keys['Reserve4'].to_f, 'Reserve5'=>s.extractor_keys['Reserve5'].to_f,
          'Reserve6'=>s.extractor_keys['Reserve6'].to_f, 'Reserve7'=>s.extractor_keys['Reserve7'].to_f, 'Reserve8'=>s.extractor_keys['Reserve8']
        }
        test_ok &= $spctest.verify_data(ekeys, generic_ekeys_dc_param)
        dkeys = {'DatReserve4'=>s.data_keys['DatReserve4'], 'DatReserve5'=>s.data_keys['DatReserve5'].to_f,
          'DatReserve6'=>s.data_keys['DatReserve6'].to_f, 'DatReserve7'=>s.data_keys['DatReserve7'], 'DatReserve8'=>s.data_keys['DatReserve8']
        }
        bool_val = ['yes', 'true', '1', 'y'].member?(wafer_values[5][w])  ? 'Y' : 'N'
        generic_dkeys_dc_param = {'DatReserve4'=>bool_val, 'DatReserve5'=>wafer_values[0][w].to_f,
          'DatReserve6'=>wafer_values[2][w].to_f, 'DatReserve7'=>wafer_values[4][w], 'DatReserve8'=>bool_val}
        test_ok &= $spctest.verify_data(dkeys, generic_dkeys_dc_param)
      }
    }
    assert test_ok, 'wrong extractor key or data key data'
  end
  
  # MSR640634
  def test15100_generic_keys_dc_tool_is_2_parameter_fine_pd_config
    # upload setup file
    assert $spctest.sil_upload_verify_generickey(@@file_generic_keys_dc_tool_second_pd), 'file upload failed'
    lot = 'SIL006.00'
    assert_equal 0, $sv.lot_cleanup(lot)
    # setup chambers PD dependant
    chambers = [{'CHA'=>5, 'CHB'=>6, 'CHC'=>7}, {'CHD'=>5, 'CHE'=>6, 'CHF'=>7}, {'CHG'=>5, 'CHH'=>6, 'CHI'=>7}, {'CHJ'=>5, 'CHK'=>6, 'CHL'=>7}]
    #
    # build and submit DCRs for 2 process PDs (wafer history should not report subprocessingarea)
    ppds = ['QA-PARAM-HIERTEST.01', 'QA-PARAM-HIERTEST.02', 'QA-PARAM-HIERTEST.03', 'QA-PARAM-HIERTEST.04']
    dcrps = ppds.each_with_index.collect {|ppd, i|
      create_process_dcr(ppd: ppd, eqp: "#{@@eqp}_#{i}", lot: lot, chambers: chambers[i], logical_recipe: "F-P-FTEOS8K15.0#{i}", 
        machine_recipe: "F-CVD60X-P.F-P-FTEOS8K15.0#{i}", dcp_plan_version: "0#{i}", dcp_plan_name: "SEM-YM0#{i}", 
        customer: "Customer_0#{i}", export: "log/#{__method__}_p_#{i}.xml")
    }
    assert $spctest.send_dcrs_verify(dcrps), 'error submitting DCRs'
    # 
    # build and submit DCR for related MPD
    dcr = Space::DCR.new(eqp: @@mtool, lot: lot, op: 'QA-PARAM-HIERTESTM.01')
    dcr.add_parameters(@@parameters, @@wafer_values)
    res = $client.send_dcr(dcr, export: :caller)
    assert $spctest.verify_dcr_accepted(res, @@parameters.size * @@wafer_values.size), 'DCR not submitted successfully'
    #
    # verify sample generic keys
    folder = $lds.folder(@@autocreated_qa)
    @@parameters.each_with_index {|p, i|
      ch = folder.spc_channel(parameter: p)
      samples = ch.samples
      assert_equal @@wafer_values.size, samples.size, "wrong number of samples in #{ch.inspect}"
      samples.each_with_index {|s, j| 
        $log.info "Parameter #{p} (index: #{i}), Sampleindex: #{j}"
        assert $spctest.verify_data(s.extractor_keys, @@generic_ekeys_dc_tool[i])
        assert $spctest.verify_data(s.data_keys, @@generic_dkeys_dc_tool[i])
      }
    }
  end
  
  def test15110_generic_keys_dc_params_from_current_dcr
    # upload setup file
    test_ok = true
    assert $spctest.sil_upload_verify_generickey(@@file_generic_keys_dc_params_current), 'file upload failed'
    #
    # build and submit DCRs for 5 process PDs
    wafer_values = []
    dcrps = @@ppds.each_with_index.collect {|ppd, i|
      dcrp = Space::DCR.new(eqp: "#{@@eqp}_#{i}", lot: @@lot, op: ppd, 
        logical_recipe: "F-P-FTEOS8K15.0#{i}", machine_recipe: "F-CVD60X-P.F-P-FTEOS8K15.0#{i}")
    
      wafer_value = {'2502J493KO'=>"FIRST_GK_#{i}01", '2502J494KO'=>"FIRST_GK_#{i}02", '2502J495KO'=>"FIRST_GK_#{i}03", 
      '2502J496KO'=>"FIRST_GK_#{i}04", '2502J497KO'=>"FIRST_GK_#{i}05", '2502J498KO'=>"FIRST_GK_#{i}06"}      
      generickey_dc_param = "QA_PARAM_GKDC_#{i}"    
      $log.info "using process values for #{generickey_dc_param} #{wafer_value.inspect}"  
      dcrp.add_parameter(generickey_dc_param, wafer_value, nocharting: true, value_type: 'string')
      wafer_values << wafer_value
    
      wafer_values_new = Hash[@@wafer_values.collect {|w,v| [w,"#{i}#{v}"]}]
      #$log.info "using process values #{wafer_values_new.inspect}"
      dcrp.add_parameters(@@parameters, wafer_values_new, nocharting: true, value_type: 'unsigned integer')
    }
    assert $spctest.send_dcrs_verify(dcrps), 'error submitting DCRs'
    # 
    # build and submit DCR for related MPD
    dcr = Space::DCR.new(eqp: @@mtool, lot: @@lot, op: @@mpd)
    number = 0
    wafer_values_meas = Hash[@@wafer_values.collect {|w,v| [w,"#{number += 1}#{v}"]}]
    $log.info "using measurement values #{wafer_values_meas.inspect}"
    dcr.add_parameters(@@parameters, wafer_values_meas, value_type: 'unsigned integer')
    wafer_value = {'2502J493KO'=>'CURRENT_DCR1_01', '2502J494KO'=>'CURRENT_DCR1_02', '2502J495KO'=>'CURRENT_DCR1_03', 
      '2502J496KO'=>'CURRENT_DCR1_04', '2502J497KO'=>'CURRENT_DCR1_05', '2502J498KO'=>'CURRENT_DCR1_06'}      
    $log.info "using additinal values #{wafer_value.inspect}"    
    dcr.add_parameter('QA_PARAM_GKDC_CURRENT1', wafer_value, nocharting: true, value_type: 'string')
    res = $client.send_dcr(dcr, export: "log/#{__method__}_1_m.xml")
    assert $spctest.verify_dcr_accepted(res, @@parameters.size * wafer_values_meas.size), 'DCR not submitted successfully'
    #
    # verify sample generic keys
    folder = $lds.folder(@@autocreated_qa)
    @@parameters.each {|p|
      ch = folder.spc_channel(parameter: p)
      samples = ch.samples
      assert_equal wafer_values_meas.size, samples.size, "wrong number of samples in #{ch.inspect}"
      samples.each {|s|
        w = s.extractor_keys['Wafer']
        $log.info "  using wafer: #{w}"
        generic_ekeys_dc_param_current = {'Reserve4'=>wafer_value[w], 'Reserve5'=>wafer_value[w], 
          'Reserve6'=>wafer_value[w], 'Reserve7'=>wafer_value[w], 'Reserve8'=>wafer_value[w]}
        test_ok &= $spctest.verify_data(s.extractor_keys, generic_ekeys_dc_param_current)
        generic_dkeys_dc_param_current = {'DatReserve4'=>'-', 'DatReserve5'=>'-', 
          'DatReserve6'=>'-', 'DatReserve7'=>'-', 'DatReserve8'=>'-'}
        test_ok &= $spctest.verify_data(s.data_keys, generic_dkeys_dc_param_current)
      }
    }
    #
    # delete channels
    folder.delete_channels if folder
    #
    # build and submit DCRs for 5 process PDs
    wafer_values = []
    dcrs = @@ppds.each_with_index.collect {|ppd, i|
      dcrp = Space::DCR.new(eqp: "#{@@eqp}_#{i}", lot: @@lot, op: ppd, 
        logical_recipe: "F-P-FTEOS8K15.0#{i}", machine_recipe: "F-CVD60X-P.F-P-FTEOS8K15.0#{i}")
    
      wafer_value = {'2502J493KO'=>"SECOND_GK_#{i}01", '2502J494KO'=>"SECOND_GK_#{i}02", '2502J495KO'=>"SECOND_GK_#{i}03", 
      '2502J496KO'=>"SECOND_GK_#{i}04", '2502J497KO'=>"SECOND_GK_#{i}05", '2502J498KO'=>"SECOND_GK_#{i}06"}      
      generickey_dc_param = "QA_PARAM_GKDC_#{i}"    
      $log.info "using process values for #{generickey_dc_param} #{wafer_value.inspect}"  
      dcrp.add_parameter(generickey_dc_param, wafer_value, nocharting: true, value_type: 'string')
      wafer_values << wafer_value
    
      wafer_values_new = Hash[@@wafer_values.collect {|w, v| [w, "#{i}#{v}"]}]
      #$log.info "using process values #{wafer_values_new.inspect}"
      dcrp.add_parameters(@@parameters, wafer_values_new, nocharting: true, value_type: 'unsigned integer')
    }
    assert $spctest.send_dcrs_verify(dcrs), 'error submitting DCRs'
    # 
    # build and submit DCR for related MPD
    dcr = Space::DCR.new(eqp: @@mtool, lot: @@lot, op: @@mpd)
    dcrs.unshift(dcr)
    number = 0
    wafer_values_meas = Hash[@@wafer_values.collect {|w,v| [w,"#{number += 1}#{v}"]}]
    $log.info "using measurement values #{wafer_values_meas.inspect}"
    dcr.add_parameters(@@parameters, wafer_values_meas, value_type: 'unsigned integer')
    wafer_value = {'2502J493KO'=>'CURRENT_DCR2_01', '2502J494KO'=>'CURRENT_DCR2_02', '2502J495KO'=>'CURRENT_DCR2_03', 
      '2502J496KO'=>'CURRENT_DCR2_04', '2502J497KO'=>'CURRENT_DCR2_05', '2502J498KO'=>'CURRENT_DCR2_06'}      
    $log.info "using additinal values #{wafer_value.inspect}"    
    dcr.add_parameter('QA_PARAM_GKDC_CURRENT2', wafer_value, nocharting: true, value_type: 'string')
    res = $client.send_dcr(dcr, export: "log/#{__method__}_2_m.xml")
    assert $spctest.verify_dcr_accepted(res, @@parameters.size * wafer_values_meas.size), 'DCR not submitted successfully'
    #
    # verify sample generic keys
    @@parameters.each {|p|
      ch = folder.spc_channel(parameter: p)
      samples = ch.samples
      assert_equal wafer_values_meas.size, samples.size, "wrong number of samples in #{ch.inspect}"
      samples.each {|s|
        w = s.extractor_keys['Wafer']        
        gkeys = {'Reserve4'=>'-', 'Reserve5'=>'-', 'Reserve6'=>'-', 'Reserve7'=>'-', 'Reserve8'=>'-'}
        test_ok &= $spctest.verify_data(s.extractor_keys, gkeys)
        gkeys = {'DatReserve4'=>wafer_value[w], 'DatReserve5'=>wafer_value[w], 
          'DatReserve6'=>wafer_value[w], 'DatReserve7'=>wafer_value[w], 'DatReserve8'=>wafer_value[w]}
        test_ok &= $spctest.verify_data(s.data_keys, gkeys)
      }
    }
    assert test_ok, 'wrong extractor key or data key data'
  end

  def test15200_process_wafer_params_metrology_lot_param # MSR 545085
    # upload setup file
    assert $spctest.sil_upload_verify_generickey(@@file_generic_keys_dc_wafer_param_lot_param), 'file upload failed'
    a_parameter = 'QA_LOT_PARAMETER'
    processing_areas_behaviour = 'cycle_wafer'
    dcrp = Space::DCR.new(eqp: @@eqp, eqptype: 'PROCESS-CHAMBER', lot: @@lot, op: @@ppds.first,
      logical_recipe: 'F-P-FTEOS8K15.00', machine_recipe: 'F-CVD60X-P.F-P-FTEOS8K15.00')
    wafer_values = [{'2502J593KO'=>'COT3-01', '2502J594KO'=>'COT3-02', '2502J595KO'=>'COT3-03', '2502J596KO'=>'COT3-04', '2502J597KO'=>'COT3-05', '2502J598KO'=>'COT3-06'},
                    {'2502J593KO'=>'COT4-01', '2502J594KO'=>'COT4-02', '2502J595KO'=>'COT4-03', '2502J596KO'=>'COT4-04', '2502J597KO'=>'COT4-05', '2502J598KO'=>'COT4-06'},
                    {'2502J593KO'=>'COT5-01', '2502J594KO'=>'COT5-02', '2502J595KO'=>'COT5-03', '2502J596KO'=>'COT5-04', '2502J597KO'=>'COT5-05', '2502J598KO'=>'COT5-06'},
                    {'2502J593KO'=>'COT6-01', '2502J594KO'=>'COT6-02', '2502J595KO'=>'COT6-03', '2502J596KO'=>'COT6-04', '2502J597KO'=>'COT6-05', '2502J598KO'=>'COT6-06'},
                    {'2502J593KO'=>'COT7-01', '2502J594KO'=>'COT7-02', '2502J595KO'=>'COT7-03', '2502J596KO'=>'COT7-04', '2502J597KO'=>'COT7-05', '2502J598KO'=>'COT7-06'},
                    {'2502J593KO'=>'COT8-01', '2502J594KO'=>'COT8-02', '2502J595KO'=>'COT8-03', '2502J596KO'=>'COT8-04', '2502J597KO'=>'COT8-05', '2502J598KO'=>'COT8-06'},
                    {'2502J593KO'=>'COT9-01', '2502J594KO'=>'COT9-02', '2502J595KO'=>'COT9-03', '2502J596KO'=>'COT9-04', '2502J597KO'=>'COT9-05', '2502J598KO'=>'COT9-06'},
                    {'2502J593KO'=>'COT10-01', '2502J594KO'=>'COT10-02', '2502J595KO'=>'COT10-03', '2502J596KO'=>'COT10-04', '2502J597KO'=>'COT10-05', '2502J598KO'=>'COT10-06'}]      
    coater_params = ['QA_COATER_0', 'QA_COATER_1', 'QA_COATER_2', 'QA_COATER_3', 'QA_COATER_4', 'QA_COATER_5', 'QA_COATER_6', 'QA_COATER_7']
    coater_params.size.times {|i|
      dcrp.add_parameter(coater_params[i], wafer_values[i], nocharting: true, value_type: 'string', processing_areas: processing_areas_behaviour)
    }
    #
    dcrp.add_parameters(@@parameters, @@wafer_values, nocharting: true, processing_areas: processing_areas_behaviour)
    dcrp.add_parameter(a_parameter, {@@lot=>100}, reading_type: 'Lot', nocharting: true, processing_areas: processing_areas_behaviour)
    res = $client.send_dcr(dcrp, export: "log/#{__method__}_p.xml")
    assert $spctest.verify_dcr_accepted(res, 0), 'DCR not submitted successfully'  
    #
    dcrm = Space::DCR.new(eqp: @@mtool, lot: @@lot, op: @@mpd)
    dcrm.add_parameters(@@parameters, @@wafer_values)
    dcrm.add_parameter(a_parameter, {@@lot=>100}, reading_type: 'Lot')
    res = $client.send_dcr(dcrm, export: :caller)
    assert $spctest.verify_dcr_accepted(res, @@parameters.size * @@wafer_values.size + 1), 'DCR not submitted successfully'
    pchambers = []
    folder = $lds.folder(@@autocreated_qa)
    @@parameters.each {|p|
      ch = folder.spc_channel(parameter: p)
      samples = ch.samples
      assert_equal(@@wafer_values.size, samples.size, "wrong number of samples in #{ch.inspect}")
      samples.each {|s| 
        pchambers << s.extractor_keys['PChamber']
        $log.info "parameter #{p}"
        h_ek = {'Reserve1'=>wafer_values[5][s.extractor_keys['Wafer']], 'Reserve2'=>wafer_values[6][s.extractor_keys['Wafer']], 
                'Reserve3'=>wafer_values[7][s.extractor_keys['Wafer']], 'Reserve4'=>wafer_values[0][s.extractor_keys['Wafer']], 
                'Reserve5'=>wafer_values[1][s.extractor_keys['Wafer']], 'Reserve6'=>wafer_values[2][s.extractor_keys['Wafer']],
                'Reserve7'=>wafer_values[3][s.extractor_keys['Wafer']], 'Reserve8'=>wafer_values[4][s.extractor_keys['Wafer']]}
        h_dk = {'DatReserve1'=>wafer_values[0][s.data_keys['Wafer']], 'DatReserve2'=>wafer_values[1][s.data_keys['Wafer']], 
                'DatReserve3'=>wafer_values[2][s.data_keys['Wafer']], 'DatReserve4'=>wafer_values[3][s.data_keys['Wafer']], 
                'DatReserve5'=>wafer_values[4][s.data_keys['Wafer']], 'DatReserve6'=>wafer_values[5][s.data_keys['Wafer']], 
                'DatReserve7'=>wafer_values[6][s.data_keys['Wafer']], 'DatReserve8'=>wafer_values[7][s.data_keys['Wafer']]} 
        assert $spctest.verify_data(s.extractor_keys, h_ek)
        assert $spctest.verify_data(s.data_keys, h_dk)
      }
    }
    pchambers.uniq!
    assert_equal dcrp.processing_areas.size, pchambers.size, "Amount of PChambers not as expected at wafer parameters: #{pchambers.inspect}"
    #
    assert $spctest.verify_parameters(a_parameter, folder, 1, @@generic_ekeys_dc_all_hyphen, @@generic_dkeys_dc_all_hyphen)
    ch = folder.spc_channel(parameter: a_parameter)
    ch.samples.each {|s| pchambers << s.extractor_keys['PChamber']}
    pchambers.uniq!
    assert_equal dcrp.processing_areas.size + 1, pchambers.size, "Amount of PChambers not as expected at lot and wafer parameters: #{pchambers.inspect}"
    assert_equal '-', pchambers.last, "PChamber not as expected for lot parameters: #{pchambers.last}"
  end

  def test15201_process_wafer_params_metrology_lot_param  # MSR 545085
    # upload setup file
    assert $spctest.sil_upload_verify_generickey(@@file_generic_keys_dc_wafer_param_lot_param), 'file upload failed'
    a_parameter = 'QA_LOT_PARAMETER'
    processing_areas_behaviour = 'cycle_parameter'
    #
    dcrp = Space::DCR.new(eqp: "#{@@eqp}", lot: @@lot, op: @@ppds.first,
      logical_recipe: 'F-P-FTEOS8K15.00', machine_recipe: 'F-CVD60X-P.F-P-FTEOS8K15.00')
    wafer_values = [{'2502J593KO'=>'COT3-01', '2502J594KO'=>'COT3-02', '2502J595KO'=>'COT3-03', '2502J596KO'=>'COT3-04', '2502J597KO'=>'COT3-05', '2502J598KO'=>'COT3-06'},
                    {'2502J593KO'=>'COT4-01', '2502J594KO'=>'COT4-02', '2502J595KO'=>'COT4-03', '2502J596KO'=>'COT4-04', '2502J597KO'=>'COT4-05', '2502J598KO'=>'COT4-06'},
                    {'2502J593KO'=>'COT5-01', '2502J594KO'=>'COT5-02', '2502J595KO'=>'COT5-03', '2502J596KO'=>'COT5-04', '2502J597KO'=>'COT5-05', '2502J598KO'=>'COT5-06'},
                    {'2502J593KO'=>'COT6-01', '2502J594KO'=>'COT6-02', '2502J595KO'=>'COT6-03', '2502J596KO'=>'COT6-04', '2502J597KO'=>'COT6-05', '2502J598KO'=>'COT6-06'},
                    {'2502J593KO'=>'COT7-01', '2502J594KO'=>'COT7-02', '2502J595KO'=>'COT7-03', '2502J596KO'=>'COT7-04', '2502J597KO'=>'COT7-05', '2502J598KO'=>'COT7-06'},
                    {'2502J593KO'=>'COT8-01', '2502J594KO'=>'COT8-02', '2502J595KO'=>'COT8-03', '2502J596KO'=>'COT8-04', '2502J597KO'=>'COT8-05', '2502J598KO'=>'COT8-06'},
                    {'2502J593KO'=>'COT9-01', '2502J594KO'=>'COT9-02', '2502J595KO'=>'COT9-03', '2502J596KO'=>'COT9-04', '2502J597KO'=>'COT9-05', '2502J598KO'=>'COT9-06'},
                    {'2502J593KO'=>'COT10-01', '2502J594KO'=>'COT10-02', '2502J595KO'=>'COT10-03', '2502J596KO'=>'COT10-04', '2502J597KO'=>'COT10-05', '2502J598KO'=>'COT10-06'}]      
    coater_params = ['QA_COATER_0', 'QA_COATER_1', 'QA_COATER_2', 'QA_COATER_3', 'QA_COATER_4', 'QA_COATER_5', 'QA_COATER_6', 'QA_COATER_7']
    coater_params.size.times {|i|
      dcrp.add_parameter(coater_params[i], wafer_values[i], nocharting: true, value_type: 'string', processing_areas: processing_areas_behaviour)
    }
    #
    dcrp.add_parameters(@@parameters, @@wafer_values, nocharting: true, processing_areas: processing_areas_behaviour)
    dcrp.add_parameter(a_parameter, {@@lot=>100}, reading_type: 'Lot', nocharting: true, processing_areas: processing_areas_behaviour)
    res = $client.send_dcr(dcrp, export: "log/#{__method__}_p.xml")
    assert $spctest.verify_dcr_accepted(res, 0), 'DCR not submitted successfully'  
    #
    dcrm = Space::DCR.new(eqp: @@mtool, lot: @@lot, op: @@mpd) #, chambers: chambers)
    dcrm.add_parameters(@@parameters, @@wafer_values)
    dcrm.add_parameter(a_parameter, {@@lot=>100}, reading_type: 'Lot')
    res = $client.send_dcr(dcrm, export: "log/#{__method__}_m.xml")
    assert $spctest.verify_dcr_accepted(res, @@parameters.size * @@wafer_values.size + 1), 'DCR not submitted successfully'
    pchambers = []
    folder = $lds.folder(@@autocreated_qa)
    @@parameters.each {|p|
      ch = folder.spc_channel(parameter: p)
      samples = ch.samples
      assert_equal(@@wafer_values.size, samples.size, "wrong number of samples in #{ch.inspect}")
      samples.each {|s| 
        pchambers << s.extractor_keys['PChamber']
        $log.info "parameter #{p}"
        h_ek = {'Reserve1'=>wafer_values[5][s.extractor_keys['Wafer']], 'Reserve2'=>wafer_values[6][s.extractor_keys['Wafer']], 
                'Reserve3'=>wafer_values[7][s.extractor_keys['Wafer']], 'Reserve4'=>wafer_values[0][s.extractor_keys['Wafer']], 
                'Reserve5'=>wafer_values[1][s.extractor_keys['Wafer']], 'Reserve6'=>wafer_values[2][s.extractor_keys['Wafer']],
                'Reserve7'=>wafer_values[3][s.extractor_keys['Wafer']], 'Reserve8'=>wafer_values[4][s.extractor_keys['Wafer']]}
        h_dk = {'DatReserve1'=>wafer_values[0][s.data_keys['Wafer']], 'DatReserve2'=>wafer_values[1][s.data_keys['Wafer']], 
                'DatReserve3'=>wafer_values[2][s.data_keys['Wafer']], 'DatReserve4'=>wafer_values[3][s.data_keys['Wafer']], 
                'DatReserve5'=>wafer_values[4][s.data_keys['Wafer']], 'DatReserve6'=>wafer_values[5][s.data_keys['Wafer']], 
                'DatReserve7'=>wafer_values[6][s.data_keys['Wafer']], 'DatReserve8'=>wafer_values[7][s.data_keys['Wafer']]} 
        assert $spctest.verify_data(s.extractor_keys, h_ek)
        assert $spctest.verify_data(s.data_keys, h_dk)
      }
    }
    pchambers.uniq!
    assert_equal 1, pchambers.size, "PChamber not uniq for all samples of wafer parameters: #{pchambers.inspect}"
    #
    assert $spctest.verify_parameters(a_parameter, folder, 1, @@generic_ekeys_dc_all_hyphen, @@generic_dkeys_dc_all_hyphen)
    ch = folder.spc_channel(parameter: a_parameter)
    ch.samples.each {|s| pchambers << s.extractor_keys['PChamber']}
    pchambers.uniq!
    assert_equal 1, pchambers.size, "PChamber not uniq for all samples of lot and wafer parameters: #{pchambers.inspect}"
  end

  def test15202_process_wafer_params_metrology_lot_param  # MSR 545085
    # upload setup file
    assert $spctest.sil_upload_verify_generickey(@@file_generic_keys_dc_wafer_param_lot_param), 'file upload failed'
    #
    a_parameter = 'QA_LOT_PARAMETER'
    chambers = {'CHA'=>5}
    dcrp = Space::DCR.new(eqp: "#{@@eqp}", lot: @@lot, op: @@ppds.first, chambers: chambers,
      logical_recipe: 'F-P-FTEOS8K15.00', machine_recipe: 'F-CVD60X-P.F-P-FTEOS8K15.00')
    wafer_values = [{'2502J593KO'=>'COT3-01', '2502J594KO'=>'COT3-02', '2502J595KO'=>'COT3-03', '2502J596KO'=>'COT3-04', '2502J597KO'=>'COT3-05', '2502J598KO'=>'COT3-06'},
                    {'2502J593KO'=>'COT4-01', '2502J594KO'=>'COT4-02', '2502J595KO'=>'COT4-03', '2502J596KO'=>'COT4-04', '2502J597KO'=>'COT4-05', '2502J598KO'=>'COT4-06'},
                    {'2502J593KO'=>'COT5-01', '2502J594KO'=>'COT5-02', '2502J595KO'=>'COT5-03', '2502J596KO'=>'COT5-04', '2502J597KO'=>'COT5-05', '2502J598KO'=>'COT5-06'},
                    {'2502J593KO'=>'COT6-01', '2502J594KO'=>'COT6-02', '2502J595KO'=>'COT6-03', '2502J596KO'=>'COT6-04', '2502J597KO'=>'COT6-05', '2502J598KO'=>'COT6-06'},
                    {'2502J593KO'=>'COT7-01', '2502J594KO'=>'COT7-02', '2502J595KO'=>'COT7-03', '2502J596KO'=>'COT7-04', '2502J597KO'=>'COT7-05', '2502J598KO'=>'COT7-06'},
                    {'2502J593KO'=>'COT8-01', '2502J594KO'=>'COT8-02', '2502J595KO'=>'COT8-03', '2502J596KO'=>'COT8-04', '2502J597KO'=>'COT8-05', '2502J598KO'=>'COT8-06'},
                    {'2502J593KO'=>'COT9-01', '2502J594KO'=>'COT9-02', '2502J595KO'=>'COT9-03', '2502J596KO'=>'COT9-04', '2502J597KO'=>'COT9-05', '2502J598KO'=>'COT9-06'},
                    {'2502J593KO'=>'COT10-01', '2502J594KO'=>'COT10-02', '2502J595KO'=>'COT10-03', '2502J596KO'=>'COT10-04', '2502J597KO'=>'COT10-05', '2502J598KO'=>'COT10-06'}]      
    coater_params = ['QA_COATER_0', 'QA_COATER_1', 'QA_COATER_2', 'QA_COATER_3', 'QA_COATER_4', 'QA_COATER_5', 'QA_COATER_6', 'QA_COATER_7']
    coater_params.size.times {|i|
      dcrp.add_parameter(coater_params[i], wafer_values[i], nocharting: true, value_type: 'string')
    }
    #
    dcrp.add_parameters(@@parameters, @@wafer_values, nocharting: true)
    dcrp.add_parameter(a_parameter, {@@lot=>100}, reading_type: 'Lot', nocharting: true)
    res = $client.send_dcr(dcrp, export: "log/#{__method__}_p.xml")
    assert $spctest.verify_dcr_accepted(res, 0), 'DCR not submitted successfully'  
    #
    dcrm = Space::DCR.new(eqp: @@mtool, lot: @@lot, op: @@mpd, chambers: chambers)
    dcrm.add_parameters(@@parameters, @@wafer_values)
    dcrm.add_parameter(a_parameter, {@@lot=>100}, reading_type: 'Lot')
    res = $client.send_dcr(dcrm, export: "log/#{__method__}_m.xml")
    assert $spctest.verify_dcr_accepted(res, @@parameters.size * @@wafer_values.size + 1), 'DCR not submitted successfully'
    pchambers = []
    folder = $lds.folder(@@autocreated_qa)
    @@parameters.each {|p|
      ch = folder.spc_channel(parameter: p)
      samples = ch.samples
      assert_equal @@wafer_values.size, samples.size, "wrong number of samples in #{ch.inspect}"
      samples.each {|s| 
        pchambers << s.extractor_keys['PChamber']
        $log.info "parameter #{p}"
        h_ek = {'Reserve1'=>wafer_values[5][s.extractor_keys['Wafer']], 'Reserve2'=>wafer_values[6][s.extractor_keys['Wafer']], 
                'Reserve3'=>wafer_values[7][s.extractor_keys['Wafer']], 'Reserve4'=>wafer_values[0][s.extractor_keys['Wafer']], 
                'Reserve5'=>wafer_values[1][s.extractor_keys['Wafer']], 'Reserve6'=>wafer_values[2][s.extractor_keys['Wafer']],
                'Reserve7'=>wafer_values[3][s.extractor_keys['Wafer']], 'Reserve8'=>wafer_values[4][s.extractor_keys['Wafer']]}
        h_dk = {'DatReserve1'=>wafer_values[0][s.data_keys['Wafer']], 'DatReserve2'=>wafer_values[1][s.data_keys['Wafer']], 
                'DatReserve3'=>wafer_values[2][s.data_keys['Wafer']], 'DatReserve4'=>wafer_values[3][s.data_keys['Wafer']], 
                'DatReserve5'=>wafer_values[4][s.data_keys['Wafer']], 'DatReserve6'=>wafer_values[5][s.data_keys['Wafer']], 
                'DatReserve7'=>wafer_values[6][s.data_keys['Wafer']], 'DatReserve8'=>wafer_values[7][s.data_keys['Wafer']]} 
        assert $spctest.verify_data(s.extractor_keys, h_ek)
        assert $spctest.verify_data(s.data_keys, h_dk)
      }
    }
    pchambers.uniq!
    assert_equal 1, pchambers.size, "PChamber not uniq for all samples of wafer parameters: #{pchambers.inspect}"
    #
    assert $spctest.verify_parameters(a_parameter, folder, 1, @@generic_ekeys_dc_all_hyphen, @@generic_dkeys_dc_all_hyphen)
    ch = folder.spc_channel(parameter: a_parameter)
    ch.samples.each {|s| pchambers << s.extractor_keys['PChamber']}
    pchambers.uniq!
    assert_equal 1, pchambers.size, "PChamber not uniq for all samples of lot and wafer parameters: #{pchambers.inspect}"
  end
  
  def test15210_process_lot_params_metrology_wafer_param  # MSR 549756
    # upload setup file
    #assert $spctest.sil_upload_verify_generickey(@@file_generic_keys_dc_wafer_param_lot_param), 'file upload failed'
    #
    a_parameter = 'QA_LOT_PARAMETER'
    lot_value = 'COAT101'
    dcrp = Space::DCR.new(eqp: "#{@@eqp}", lot: @@lot, op: @@ppds.first, 
      logical_recipe: 'F-P-FTEOS8K15.00', machine_recipe: 'F-CVD60X-P.F-P-FTEOS8K15.00')
    dcrp.add_parameter('QA_COATER_0', {@@lot=>lot_value}, nocharting: true, value_type: 'string', reading_type: 'Lot')
    dcrp.add_parameters(@@parameters, @@wafer_values, nocharting: true)
    res = $client.send_dcr(dcrp, export: "log/#{__method__}_p.xml")
    assert $spctest.verify_dcr_accepted(res, 0), 'DCR not submitted successfully'  
    #
    dcrm = Space::DCR.new(eqp: @@mtool, lot: @@lot, op: @@mpd)
    dcrm.add_parameters(@@parameters, @@wafer_values)
    dcrm.add_parameter(a_parameter, {@@lot=>100}, reading_type: 'Lot')
    res = $client.send_dcr(dcrm, export: "log/#{__method__}_m.xml")
    assert $spctest.verify_dcr_accepted(res, @@parameters.size * @@wafer_values.size + 1), 'DCR not submitted successfully'
    #
    h_ek = @@generic_ekeys_dc_all_hyphen.clone
    h_ek['Reserve4'] = lot_value
    h_dk = @@generic_dkeys_dc_all_hyphen.clone
    h_dk['DatReserve4'] = lot_value
    folder = $lds.folder(@@autocreated_qa)
    assert $spctest.verify_parameters(@@parameters, folder, @@wafer_values.size, h_ek, h_dk)
    assert $spctest.verify_parameters(a_parameter, folder, 1, h_ek, h_dk)
  end
  
  def test15300_generic_keys_dc_inline_test
    assert $spctest.sil_upload_verify_generickey(@@file_generic_keys_dc_lds), 'file upload failed'
    _send_dc_scenario('TEST', @@generic_ekeys_dc_setup, @@generic_dkeys_dc_setup)
    setup
    _send_dc_scenario('20NM', @@generic_ekeys_dc_inline, @@generic_dkeys_dc_inline)
  end

  def test15301_generic_keys_dc_test
    assert $spctest.sil_upload_verify_generickey(@@file_generic_keys_dc_setup), 'file upload failed'
    _send_dc_scenario('TEST', @@generic_ekeys_dc_setup, @@generic_dkeys_dc_setup)
    setup
    _send_dc_scenario('20NM', nil, nil)
  end
    
  def test15302_generic_keys_dc_inline
    assert $spctest.sil_upload_verify_generickey(@@file_generic_keys_dc_inline), 'file upload failed'
    _send_dc_scenario('TEST', nil, nil)
    setup
    _send_dc_scenario('20NM', @@generic_ekeys_dc_inline, @@generic_dkeys_dc_inline)
  end
  
  def test15303_generic_keys_dc_nolds # obsolete with Setup.FC
    assert $spctest.sil_upload_verify_generickey(@@files_generic_keys_dc[0]), 'file upload failed'
    ekeys = Hash[@@generic_ekeys_dc.each_pair.collect {|k, v| [k, v.first]}]
    dkeys = Hash[@@generic_dkeys_dc.each_pair.collect {|k, v| [k, v.first]}]
    _send_dc_scenario('TEST', ekeys, dkeys)
    setup
    _send_dc_scenario('20NM', ekeys, dkeys)
  end
  
  def test15400_generic_keys_fixed_routeudata_empty
    _send_scenario_route('SIL-0001.01', 'fixed')
  end

  def test15401_generic_keys_fixed_routeudata_noroute
    _send_scenario_route('SIL-XXXX.01', 'fixed')
  end

  def test15402_generic_keys_fixed_routeudata
    route = ($env == 'f8stag') ? 'FF-OBANPK.01' : 'C02-1238.01'
    _send_scenario_route(route, 'fixed')
  end

  def test15403_generic_keys_fixed_routeudata_empty_product_with_routeudata
    #product with route (C02-1116.01/FF-OBANPK.01) with udata set
    product = ($env == 'f8stag') ? 'OBANPK.01' : '4098AB.A1'
    product_route = ($env == 'f8stag') ? 'FF-OBANPK.01' : 'C02-1116.01'
    _send_scenario_route('SIL-0001.01', 'fixed', product, product_route)
  end

  def test15404_generic_keys_fixed_routeudata_empty_product_with_routeudata_erfroute
    #product with route (C02-1116.01/FF-OBANPK.01) with udata set
    product = ($env == 'f8stag') ? 'OBANPK.01' : '4098AB.A1'
    product_route = ($env == 'f8stag') ? 'FF-OBANPK.01' : 'C02-1116.01'
    _send_scenario_route('e1234-QA-SIL.01', 'fixed', product, product_route)
  end

  def test15500_generic_keys_group_routeudata_empty
    _send_scenario_route('SIL-0001.01', 'group')
  end

  def test15501_generic_keys_group_routeudata_noroute
    _send_scenario_route('SIL-XXXX.01', 'group')
  end

  def test15502_generic_keys_group_routeudata
    route = ($env == 'f8stag') ? 'FF-OBANPK.01' : 'C02-1238.01'
    _send_scenario_route(route, 'group')
  end

  # TODO: Add test case for multiple lots
  
  def test15600_generic_keys_fixed_productudata
    _send_scenario_product(0, 'SILPROD.01', 'QAPG1', 'fixed')
    setup
    _send_scenario_product(1, 'SILPROD.01', 'QAPG1', 'fixed')
    setup
    _send_scenario_product(2, 'SILPROD.01', 'QAPG1', 'fixed')
  end
  
  def test15601_generic_keys_fixed_productudata_empty
    _send_scenario_product(2, 'SILPROD.02', 'QAPG2', 'fixed')
    assert $spctest.verify_notification('Error retrieving', 'QAPG2', raw: true).size > 0, 'no derdack for product group, please check SIL log manually for derdack message'
    assert $spctest.verify_notification('Error retrieving', 'SILPROD.02', raw: true).size > 0, 'no derdack for product, please check SIL log manually for derdack message'
  end

  def test15602_generic_keys_fixed_productudata_invalid
    _send_scenario_product(3, 'SILPROD.01', 'QAPG1', 'fixed')
    assert $spctest.verify_notification('Error retrieving', 'QAPG1', raw: true).size > 0, 'no derdack for product group, please check SIL log manually for derdack message'
    assert $spctest.verify_notification('Error retrieving', 'SILPROD.01', raw: true).size > 0, 'no derdack for product, please check SIL log manually for derdack message'
  end

  def test15603_generic_keys_fixed_productudata_multilot
    product = ($env == 'f8stag') ? 'OBANPK.01' : '3260CAC.F0'
    pg  = ($env == 'f8stag') ? 'OBANPK' : 'SL3260C'
    _send_scenario_product(2, 'SILPROD.01', 'QAPG1', 'fixed', addlot: 'SIL006.00', addproduct: product, addproduct_group: pg)
  end

  def test15604_generic_keys_fixed_productudata_null
    ($log.info 'Samples wont be uploaded. Cannot run test!'; return) unless @@parameters_missingspeclimit_upload_inline_enabled
    _send_scenario_product(2, nil, nil, 'fixed')
  end
  

  # scenarios

  def _send_scenario_fixed(scenario, params={})
    # load setup file
    assert $spctest.sil_upload_verify_generickey(@@files_generic_keys_fixed[scenario]), 'file upload failed'
    # build and submit DCRs for 5 process PDs
    dcrps = @@ppds.each_with_index.collect {|ppd, i|
      create_process_dcr(ppd: ppd, eqp: "#{@@eqp}_#{i}", logical_recipe: "F-P-FTEOS8K15.0#{i}", machine_recipe: "F-CVD60X-P.F-P-FTEOS8K15.0#{i}")
    }
    assert $spctest.send_dcrs_verify(dcrps, exporttag: caller_locations(1,1)[0].label), 'error submitting DCRs'
    #
    # build and submit DCR for related MPD
    dcr = Space::DCR.new(eqp: @@mtool, lot: @@lot, op: @@mpd)
    dcr.add_parameters(@@parameters, @@wafer_values)
    res = $client.send_dcr(dcr, export: "log/#{caller_locations(1,1)[0].label}_m.xml")
    assert $spctest.verify_dcr_accepted(res, @@parameters.size * @@wafer_values.size), 'DCR not submitted successfully'
    # verify sample generic keys
    ekeys = dcrps[0].sample_generickeys(mapping: Hash[@@generic_ekeys_fixed.each_pair.collect {|k, v| [k, v[scenario]]}])
    dkeys = dcrps[0].sample_generickeys(mapping: Hash[@@generic_dkeys_fixed.each_pair.collect {|k, v| [k, v[scenario]]}])
    assert $spctest.verify_parameters(@@parameters, $lds.folder(@@autocreated_qa), @@wafer_values.size, ekeys, dkeys)
  end
  
  def _send_scenario_dc(scenario, params={})
    # upload setup file
    assert $spctest.sil_upload_verify_generickey(@@files_generic_keys_dc[scenario]), 'file upload failed'
    chambers = params[:pchambers] || @@chambers
    technology = params[:technology] || '32NM'
    lds = (technology == 'TEST') ? $lds_setup : $lds
    # build and submit DCRs for 5 process PDs
    dcrps = @@ppds.each_with_index.collect {|ppd, i|
      create_process_dcr(ppd: ppd, eqp: "#{@@eqp}_#{i}", logical_recipe: "F-P-FTEOS8K15.0#{i}", machine_recipe: "F-CVD60X-P.F-P-FTEOS8K15.0#{i}", chambers: chambers, technology: technology)
    }
    assert $spctest.send_dcrs_verify(dcrps, exporttag: caller_locations(1,1)[0].label), 'error submitting DCRs'
    # build and submit DCR for related MPD
    dcr = Space::DCR.new(eqp: @@mtool, lot: @@lot, op: @@mpd, technology: technology)
    dcrps.unshift dcr
    dcr.add_parameters(@@parameters, @@wafer_values)
    res = $client.send_dcr(dcr, export: "log/#{caller_locations(1,1)[0].label}_m.xml")
    assert $spctest.verify_dcr_accepted(res, @@parameters.size * @@wafer_values.size), 'DCR not submitted successfully'
    # verify sample generic keys
    $dcrps = dcrps
    # broken!!, just returns {Reservex: nil, ..}, should be fixed
    ekeys = $spctest.get_sample_generickeys_dc(dcrps, @@generic_ekeys_dc)
    dkeys =$spctest.get_sample_generickeys_dc(dcrps, @@generic_dkeys_dc)
    #
    folder = lds.folder(@@autocreated_qa)
    assert $spctest.verify_parameters(@@parameters, folder, @@wafer_values.size, ekeys, dkeys), 'parameter verification failed'
  end
  
  def _send_scenario_group(scenario, params={})
    # upload setup file
    assert $spctest.sil_upload_verify_generickey(@@files_generic_keys_group[scenario]), 'file upload failed'
    # build and submit DCRs for 5 process PDs
    dcrps = @@ppds.each_with_index.collect {|ppd, i|
      create_process_dcr(ppd: ppd, eqp: "#{@@eqp}_#{i}", logical_recipe: "F-P-FTEOS8K15.0#{i}", machine_recipe: "F-CVD60X-P.F-P-FTEOS8K15.0#{i}")
    }
    assert $spctest.send_dcrs_verify(dcrps, exporttag: caller_locations(1,1)[0].label), 'error submitting DCRs'
    # build and submit DCR for related MPD
    dcr = Space::DCR.new(eqp: @@mtool, lot: @@lot, op: @@mpd)
    dcr.add_parameters(@@parameters, @@wafer_values)
    res = $client.send_dcr(dcr, export: "log/#{caller_locations(1,1)[0].label}_m.xml")
    assert $spctest.verify_dcr_accepted(res, @@parameters.size * @@wafer_values.size), 'DCR not submitted successfully'
    # verify sample generic keys
    ekeys = Hash[@@generic_ekeys_group.each_pair.collect {|k, v| [k, v[scenario]]}]
    dkeys = Hash[@@generic_dkeys_group.each_pair.collect {|k, v| [k, v[scenario]]}]
    assert $spctest.verify_parameters(@@parameters, $lds.folder(@@autocreated_qa), @@wafer_values.size, ekeys, dkeys)
  end
  
  def _send_dc_scenario(technology, generic_ekeys, generic_dkeys)
    # upload setup file
    lds = (technology == 'TEST') ? $lds_setup : $lds
    # build and submit DCRs for 5 process PDs
    dcrs = @@ppds.each_with_index.collect {|ppd, i|
      create_process_dcr(ppd: ppd, eqp: "#{@@eqp}_#{i}", technology: technology, logical_recipe: "F-P-FTEOS8K15.0#{i}", machine_recipe: "F-CVD60X-P.F-P-FTEOS8K15.0#{i}")
    }
    assert $spctest.send_dcrs_verify(dcrs, exporttag: caller_locations(1,1)[0].label), 'error submitting DCRs'
    # 
    # build and submit DCR for related MPD
    dcr = Space::DCR.new(eqp: @@mtool, lot: @@lot, technology: technology, op: @@mpd)
    dcrs.unshift(dcr)
    dcr.add_parameters(@@parameters, @@wafer_values)
    res = $client.send_dcr(dcr, export: "log/#{caller_locations(1,1)[0].label}_m.xml")
    assert $spctest.verify_dcr_accepted(res, @@parameters.size * @@wafer_values.size), 'DCR not submitted successfully'
    #
    # verify sample generic keys
    ekeys = generic_ekeys ? $spctest.get_sample_generickeys_dc(dcrs, generic_ekeys) : @@generic_ekeys_dc_all_hyphen
    dkeys = generic_dkeys ? $spctest.get_sample_generickeys_dc(dcrs, generic_dkeys) : @@generic_dkeys_dc_all_hyphen
    folder = lds.folder(@@autocreated_qa)
    assert $spctest.verify_parameters(@@parameters, folder, @@wafer_values.size, ekeys, dkeys)
  end

  def _send_scenario_route(route, gkmethod, product='SILPROD.01', product_route=nil)
    # load setup file
    if gkmethod == 'fixed'
      assert $spctest.sil_upload_verify_generickey(@@file_generic_keys_fixed_route), 'file upload failed'
    elsif gkmethod == 'group'
      assert $spctest.sil_upload_verify_generickey(@@file_generic_keys_group_route), 'file upload failed'
    end
    #
    # build and submit DCRs for process PDs
    dcrps = @@ppds.each_with_index.collect {|ppd, i|
      create_process_dcr(ppd: ppd, eqp: "#{@@eqp}_#{i}", route: route, product: product, 
        logical_recipe: "F-P-FTEOS8K15.0#{i}", machine_recipe: "F-CVD60X-P.F-P-FTEOS8K15.0#{i}")
    }
    assert $spctest.send_dcrs_verify(dcrps, exporttag: caller_locations(1,1)[0].label), 'error submitting DCRs'
    #
    # build and submit DCR for related MPD
    dcr = Space::DCR.new(eqp: @@mtool, lot: @@lot, op: @@mpd, route: route)
    dcr.add_parameters(@@parameters, @@wafer_values)
    res = $client.send_dcr(dcr, export: "log/#{caller_locations(1,1)[0].label}_m.xml")
    assert $spctest.verify_dcr_accepted(res, @@parameters.size * @@wafer_values.size), 'DCR not submitted successfully'
    #
    # verify sample generic keys
    udataroute = product_route.nil? ? route : product_route
    $log.info "udataroute: #{udataroute}"
    udata = $sv.user_data_route(udataroute)
    programid = udata ? udata['RouteProgramID'] : nil
    $log.info "programid: #{programid}"
    if gkmethod == 'group'
      if programid != nil
        programid = '45XX' if programid.start_with?('45')
        programid = '32XX' if programid.start_with?('32')
      end
    end
    folder = $lds.folder(@@autocreated_qa)
    assert $spctest.verify_parameters(@@parameters, folder, @@wafer_values.size, {'Reserve4'=>programid}, {'DatReserve4'=>programid}), 'Wrong data in key'
  end
 
  def _send_scenario_product(scenario, product, productgroup, gkmethod, params={})
    # load setup file
    if gkmethod == 'fixed'
     assert $spctest.sil_upload_verify_generickey(@@files_generic_keys_fixed_product[scenario]), 'file upload failed'
    elsif gkmethod == 'group'
     fail 'not configured'
    end
    # build and submit DCRs for process PDs
    dcrps = @@ppds.each_with_index.collect {|ppd, i|
      create_process_dcr(ppd: ppd, eqp: "#{@@eqp}_#{i}", product: product, productgroup: productgroup)
    }
    assert $spctest.send_dcrs_verify(dcrps, exporttag: caller_locations(1,1)[0].label), 'error submitting DCRs'
    #
    if params[:addlot]
      wafer_values =  {"#{params[:addlot]}01"=>10, "#{params[:addlot]}02"=>11, "#{params[:addlot]}03"=>12}
      dcrps = @@ppds.each_with_index.collect {|ppd, i|
        create_process_dcr(ppd: ppd, eqp: "#{@@eqp}_#{i}", lot: params[:addlot], product: params[:addproduct], 
        productgroup: params[:addproduct_group], wafer_values: wafer_values)
      }
    end
    assert $spctest.send_dcrs_verify(dcrps, exporttag: caller_locations(1,1)[0].label), 'error submitting DCRs'
    #
    # build and submit DCR for related MPD
    dcr = Space::DCR.new(eqp: @@mtool, lot: @@lot, op: @@mpd, product: product, productgroup: productgroup)
    dcr.add_parameters(@@parameters, @@wafer_values)
    nsamples = @@parameters.size * @@wafer_values.size
    if params[:addlot]
      dcr.add_lot(params[:addlot], op: @@mpd, eqp: @@mtool, product: params[:addproduct], productgroup: params[:addproduct_group])
      wafer_values = {"#{params[:addlot]}01"=>40, "#{params[:addlot]}02"=>41, "#{params[:addlot]}03"=>42}
      dcr.add_parameters(@@parameters, wafer_values)
      nsamples += @@parameters.size * wafer_values.size
    end
    res = $client.send_dcr(dcr, export: "log/#{caller_locations(1,1)[0].label}_m.xml")
    assert $spctest.verify_dcr_accepted(res, nsamples), 'DCR not submitted successfully'
    
    product ||= $sv.lot_info(@@lot).product
    productgroup ||= $sv.lot_info(@@lot).productgroup
    udatas = $sv.user_data(:product, product).merge($sv.user_data(:productgroup, productgroup))
    $log.info "#{product}, #{productgroup}, #{udatas.inspect}"
    #
    # verify sample generic keys
    keysetup_mapping = Hash.new
    $spctest.generickey_from_file(filename: @@files_generic_keys_fixed_product[scenario]).each {|line| 
      keysetup_mapping[line.name.gsub('GenericExKey','Reserve').gsub('GenericDataKey','DatReserve').next.next.next] = 
        line.fixed_property.gsub('ProductID-', '').gsub('ProductGroupID-', '')
    }
    datreserve = Hash[keysetup_mapping.collect {|k,v| [k,udatas[v]] if k.start_with?('Dat')}.compact]
    reserve = Hash[keysetup_mapping.collect {|k,v| [k,udatas[v]] if k.start_with?('Dat') == false}.compact]
    folder = $lds.folder(@@autocreated_qa)
    if params[:addlot]
      @@parameters.each {|p|
        ch = folder.spc_channel(parameter: p)
        samples = ch.samples
        samples.each {|s|
          lot = s.extractor_keys['Lot']
          # Update the udatas
          if params[:addlot] == lot
            udatas = $sv.user_data(:product, params[:addproduct]).merge($sv.user_data(:productgroup, params[:addproduct_group]))
            datreserve2 = Hash[keysetup_mapping.collect {|k,v| [k,udatas[v]] if k.start_with?('Dat')}.compact]
            reserve2 = Hash[keysetup_mapping.collect {|k,v| [k,udatas[v]] if k.start_with?('Dat') == false}.compact]
            assert $spctest.verify_data(s.extractor_keys, reserve2)
            assert $spctest.verify_data(s.data_keys, datreserve2)
          else
            assert $spctest.verify_data(s.extractor_keys, reserve)
            assert $spctest.verify_data(s.data_keys, datreserve)
          end          
        }
      }
    else   
      assert $spctest.verify_parameters(@@parameters, folder, @@wafer_values.size, reserve, datreserve), 'wrong data in key'
    end
  end
  
end
