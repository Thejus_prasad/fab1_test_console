=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: U. Koerner, 2012-01-11

History:
  2014-02-05 ssteidte,  separated Test_Space07, code cleanup
=end

require_relative 'Test_Space_Setup'


# Test Space/SIL.
class Test_Space073 < Test_Space_Setup
  @@mpd_qa = 'QA-MB-FTDEPM-LIS-CAEX.01'
  @@reticle = 'QAReticle0001'
  @@mrecipe = nil  # assigned later
  @@route = nil    # assigned later

  
  def setup
    super
    return unless $lds && self.class.class_variable_defined?(:@@lot) # bypass test0000_setup
    assert_equal 0, $sv.eqp_cleanup(@@eqp)
    assert_equal 0, $sv.lot_cleanup(@@lot)
    assert_equal 0, $sv.inhibit_cancel(:route, @@route)
    assert_equal 0, $sv.inhibit_cancel(:recipe, @@mrecipe)
    assert_equal 0, $sv.inhibit_cancel(:reticle, @@reticle)
    #
    $lds.all_spc_channels('QA_PARAM_90*').each {|ch| assert ch.delete}
    folder = $lds.folder(@@autocreated_qa)
    assert folder.delete_channels, "error cleaning channels" if folder
  end

  def teardown
    $sv.inhibit_cancel(:route, @@route)
    $sv.inhibit_cancel(:recipe, @@mrecipe)
    $sv.inhibit_cancel(:reticle, @@reticle)
  end

  def test0700_setup
    $setup_ok = false
    #
    assert @@ppd_qa = $sildb.pd_reference(mpd: @@mpd_qa).first, "no PD reference found for #{@@mpd_qa}"
    #
    ['LotHold', 'AutoLotHold', 'ReleaseLotHold', 'CancelLotHold',
      'EquipmentInhibit', 'AutoEquipmentInhibit', 'ReleaseEquipmentInhibit', 'ReleaseEquipmentInhibit-noDefault',
      'ChamberInhibit', 'AutoChamberInhibit', 'ReleaseChamberInhibit', 'ReleaseChamberInhibit-noDefault',
      'ReticleInhibit', 'AutoReticleInhibit', 'ReleaseReticleInhibit',
      'RouteInhibit', 'AutoRouteInhibit', 'ReleaseRouteInhibit',
      'MachineRecipeInhibit', 'AutoMachineRecipeInhibit', 'ReleaseMachineRecipeInhibit', 'ReleaseMachineRecipeInhibit-noDefault',
      'AutoSetWaferScriptParameter-OOC', 'AutoSetLotScriptParameter-OOC', 'AutoSetLotScriptParameter-OOCInteger', 'SetLotScriptParameter-OOC',
      'AutoUnknown', 'Unknown', 'CACollection-ReleaseAll',      
      'AutoChamberAndRecipeInhibit-Multi',
      'ReleaseEquipmentAndRecipeInhibit', 'ReleaseEquipmentAndRecipeInhibit-noDeflt', 'CACollection-ReleaseAllRecipeInhibits'
    ].each {|action|
      assert $spc.corrective_action(action), "CA #{action.inspect} not defined"
    }
    #
    # resubmit of the corresponding chamber filter file
    if ['xxitdc', 'let'].member?($env)
      (human_workflow_action(@@importantChambersTable, 'CANCEL', :user=>@@approvers[0]); sleep 30) if $sfc.wait_for_approve(@@importantChambersTable, :user=>@@approvers[0], :timeout=>1, :sleeptime=>1)
      (human_workflow_action(@@importantChambersTable, 'CANCEL', :user=>@@approvers[0]); sleep 30) if $sfc.wait_for_edit_content(@@importantChambersTable, :user=>@@approvers[0], :timeout=>1, :sleeptime=>1)
      (human_workflow_action(@@importantChambersTable, 'CANCEL'); sleep 30) if $sfc.wait_for_edit_content(@@importantChambersTable, :timeout=>1, :sleeptime=>1)
      edit_submit_approve(@@importantChambersTable, @@approvers[0])
    end
    #
    assert @@mrecipe = $sv.machine_recipes.sample
    assert @@route = $sv.lot_info(@@lot).route
    #
    $setup_ok = true
  end

  def test0730_machine_recipe_inhibit # MSR521808
    channels = create_clean_channels
    assert $spctest.assign_verify_cas(channels,
      ['MachineRecipeInhibit', 'AutoMachineRecipeInhibit', 'ReleaseMachineRecipeInhibit', 'ReleaseMachineRecipeInhibit-noDefault'])
    #
    dcrp = submit_process_dcr(@@ppd_qa, wafer_values: @@wafer_values2, machine_recipe: @@mrecipe)
    # send DCR with OOC values
    dcr = Space::DCR.new(lot: @@lot, eqp: @@mtool, op: @@mpd_qa, machine_recipe: @@mrecipe)
    dcr.add_parameters(@@parameters, @@wafer_values2ooc)
    res = $client.send_dcr(dcr, export: :caller)
    assert $spctest.verify_dcr_accepted(res, @@parameters.size*@@wafer_values2ooc.size, nooc: 'all*2'), "DCR not submitted successfully"
    #
    # verify comment and inhibit is set
    assert $spctest.verify_inhibit(@@mrecipe, channels.size, class: :recipe, details: true, dcr: dcrp, lot: @@lot, spc_run: res, cj: dcr.controljob), "Machine Recipe must have an inhibit"
    assert $spctest.verify_ecomment(channels, "RECIPE_HELD: #{@@mrecipe}: reason={X-S")
    #
    channels.each {|ch|
      # try to release inhibit noDefault (from automatic action)
      s = ch.samples(ckc: true)[-1]
      assert !s.assign_ca($spc.corrective_action("ReleaseMachineRecipeInhibit-noDefault")), "wrongly assigned corective action"
    }
    assert $spctest.verify_inhibit(@@mrecipe, channels.size, class: :recipe), "machine recipe must still have an inhibits"
    #
    # release inhibit with noDefault attribute - with comment
    channels.each {|ch|
      s = ch.samples(ckc: true)[-1]
      assert s.assign_ca($spc.corrective_action("ReleaseMachineRecipeInhibit-noDefault"), comment: "[Release=All]"), "error assigning corrective action"
    }
    assert $spctest.verify_inhibit(@@mrecipe, 0, class: :recipe), "machine recipe must not have an inhibit"
    #
    assert_equal 0, $sv.lot_cleanup(@@lot)
    # send DCR with OOC values
    dcr = Space::DCR.new(lot: @@lot, eqp: @@mtool, op: @@mpd_qa, machine_recipe: @@mrecipe)
    dcr.add_parameters(@@parameters, @@wafer_values2ooc)
    assert $spctest.send_dcr_verify(dcr, nsamples: @@parameters.size*@@wafer_values2ooc.size, nooc: 'all*2'), "error sending DCR"
    #
    # verify comment and inhibit is set
    assert $spctest.verify_inhibit(@@mrecipe, channels.size, class: :recipe), "Machine Recipe must have an inhibit"
    assert $spctest.verify_ecomment(channels, "RECIPE_HELD: #{@@mrecipe}: reason={X-S")
    #
    channels.each {|ch|
      # release inhibit (from automatic action)
      s = ch.samples(ckc: true)[-1]
      assert s.assign_ca($spc.corrective_action("ReleaseMachineRecipeInhibit")), "error assigning corective action"
    }
    assert $spctest.verify_inhibit(@@mrecipe, 0, class: :recipe), "machine recipe must not have an inhibit"
    #
    # set inhibit (manual)
    s = channels[0].samples(ckc: true)[-1]
    assert s.assign_ca($spc.corrective_action("MachineRecipeInhibit"), comment: "RECIPE_HELD: #{@@mrecipe}: reason={X-S5} - #{Time.now.utc.iso8601}"), "error assigning corective action"
    assert $spctest.verify_inhibit(@@mrecipe, 1, class: :recipe), "machine recipe must have an inhibit"
    assert $spctest.verify_ecomment(channels, "RECIPE_HELD: #{@@mrecipe}: reason={X-S")
    #
    # release inhibit
    assert s.assign_ca($spc.corrective_action("ReleaseMachineRecipeInhibit")), "error assigning corective action"
    assert $spctest.verify_inhibit(@@mrecipe, 0, class: :recipe), "machine recipe must not have an inhibit"
  end

  def test0731_reticle_inhibit  # MSR524857, MSR529698
    #file_generic_keys_fixed = "etc/testcasedata/space/GenericKeySetup_QA_FIXED_Full.SpecID.xlsx"
    #assert $spctest.sil_upload_verify_generickey(file_generic_keys_fixed), "file upload failed"
    rparam = "L-STP-RETICLEID1"
    #
    channels = create_clean_channels(department: 'LIT', mtool: @@eqp)
    assert $spctest.assign_verify_cas(channels, ["ReticleInhibit", "AutoReticleInhibit", "ReleaseReticleInhibit"])
    #
    # send DCR with OOC values
    dcr = Space::DCR.new(lot: @@lot, eqp: @@eqp, department: 'LIT', op: @@mpd_lit)
    dcr.add_parameters(@@parameters, @@wafer_values2ooc)
    dcr.add_parameter(rparam, {@@lot=>@@reticle}, nocharting: true, reading_type: 'Lot', value_type: "string")
    dcr.parameters[1][:Reading][0][:Reticle] = @@reticle
    res = $client.send_dcr(dcr, export: :caller)
    assert $spctest.verify_dcr_accepted(res, @@parameters.size*@@wafer_values2ooc.size, nooc: 'all*2'), "DCR not submitted successfully"
    #
    # verify comment and inhibit is set
    assert $spctest.verify_inhibit(@@reticle, channels.size, class: :reticle), "reticle must have an inhibit"
    assert $spctest.verify_ecomment(channels, "RETICLE_HELD:#{@@reticle}: reason={X-S")
    #
    # release inhibit
    channels.each {|ch|
      s = ch.samples(ckc: true)[-1]
      assert s.assign_ca($spc.corrective_action("ReleaseReticleInhibit")), "error assigning corective action"
    }
    assert $spctest.verify_inhibit(@@reticle, 0, class: :reticle), "reticle must not have an inhibit"
    #
    # set inhibit (manual)
    s = channels[0].samples(ckc: true)[-1]
    assert s.assign_ca($spc.corrective_action("ReticleInhibit")), "error assigning corective action"
    assert $spctest.verify_inhibit(@@reticle, 1, class: :reticle), "reticle must have an inhibit"
    assert $spctest.verify_ecomment(channels, "RETICLE_HELD:#{@@reticle}: reason={X-S")
    #
    # release inhibit
    assert s.assign_ca($spc.corrective_action("ReleaseReticleInhibit")), "error assigning corective action"
    assert $spctest.verify_inhibit(@@reticle, 0, class: :reticle), "reticle must not have an inhibit"
  end

  def test0732_route_inhibit
    channels = create_clean_channels
    assert $spctest.assign_verify_cas(channels,
      ['RouteInhibit', 'AutoRouteInhibit', 'ReleaseRouteInhibit'])
    #
    submit_process_dcr(@@ppd_qa, wafer_values: @@wafer_values2)
    # send DCR with OOC values
    dcr = Space::DCR.new(lot: @@lot, eqp: @@eqp, op: @@mpd_qa, route: @@route)
    dcr.add_parameters(@@parameters, @@wafer_values2ooc)
    res = $client.send_dcr(dcr, export: :caller)
    assert $spctest.verify_dcr_accepted(res, @@parameters.size*@@wafer_values2ooc.size, nooc: 'all*2'), "DCR not submitted successfully"
    #
    # verify comment and inhibit is set
    assert $spctest.verify_inhibit(@@route, channels.size, class: :route), "route must have an inhibit"
    assert $spctest.verify_ecomment(channels, "ROUTE_HELD:#{@@route}: reason={X-S")
    #
    # release inhibit
    channels.each {|ch|
      s = ch.samples(ckc: true)[-1]
      assert s.assign_ca($spc.corrective_action("ReleaseRouteInhibit")), "error assigning corective action"
    }
    assert $spctest.verify_inhibit(@@route, 0, class: :route), "route must not have an inhibit"
    #
    # set inhibit (manual), verify inhibit and comment
    s = channels[0].samples(ckc: true)[-1]
    assert s.assign_ca($spc.corrective_action("RouteInhibit")), "error assigning corective action"
    assert $spctest.verify_inhibit(@@route, 1, class: :route), "route must have an inhibit"
    assert $spctest.verify_ecomment(channels, "ROUTE_HELD:#{@@route}: reason={X-S")
    #
    # release inhibit
    assert s.assign_ca($spc.corrective_action("ReleaseRouteInhibit")), "error assigning corective action"
    assert $spctest.verify_inhibit(@@route, 0, class: :route), "route must not have an inhibit"
  end

  def test0733_set_lot_script_parameter_ooc
    channels = create_clean_channels
    assert $spctest.assign_verify_cas(channels,
      ['AutoSetLotScriptParameter-OOC'])
    assert $sv.user_parameter_delete_verify('Lot', @@lot, 'OOC')
    assert $sv.user_parameter_delete_verify('Lot', @@lot, "OOCInteger")
    #
    submit_process_dcr(@@ppd_qa, wafer_values: @@wafer_values) #2
    # send DCR with OOC values
    dcr = Space::DCR.new(lot: @@lot, eqp: @@mtool, op: @@mpd_qa)
    dcr.add_parameters(@@parameters, @@wafer_values2ooc)  # @@wafer_valuesooc ?
    res = $client.send_dcr(dcr, export: :caller)
    assert $spctest.verify_dcr_accepted(res, @@parameters.size*@@wafer_values2ooc.size, nooc: 'all*2'), "DCR not submitted successfully"
    #
    # verify lot script parameter is set
    assert_equal 'Yes', $sv.user_parameter('Lot', @@lot, name: 'OOC').value, "lot script parameter not set"
    # verify external comment
    assert $spctest.verify_ecomment(channels, "SET_LOT_SCRIPT_PARAMETER: reason={OOC/Yes}"), "external comment not as expected"
    #
    s = channels[0].samples(ckc: true)[-1]
    assert s.assign_ca($spc.corrective_action("SetLotScriptParameter-OOC")), "error assigning corective action"
    sleep 10
    assert_equal 'No', $sv.user_parameter('Lot', @@lot, name: 'OOC').value, "lot script parameter not set"
    s = channels[0].samples(ckc: true)[-1]
    assert s.ecomment.include?("SET_LOT_SCRIPT_PARAMETER={OOC/No}"), "external comment not as expected"
  end

  def test0734_set_wafer_script_parameter_ooc
    channels = create_clean_channels
    assert $spctest.assign_verify_cas(channels,
      ['AutoSetWaferScriptParameter-OOC'])

    li = $sv.lot_info(@@lot, wafers: true)
    li.wafers.each {|w| assert $sv.user_parameter_delete_verify('Wafer', w.wafer, 'OOC')}
    wafer_values = Hash[li.wafers.collect {|w| [w.wafer, 50]}]
    #
    submit_process_dcr(@@ppd_qa, wafer_values: wafer_values)
    # send dcr with some ooc values
    wafer_values = Hash[wafer_values.collect {|w, v| [w, v + (wafer_values.keys.index(w).even? ? 100 : 0)]}]
    cnt_ooc = 0
    wafer_values.each {|w, v| cnt_ooc += 1 if wafer_values.keys.index(w).even?}
    cnt_ooc *= @@parameters.size
    dcr = Space::DCR.new(lot: @@lot, eqp: @@eqp, op: @@mpd_qa, chambers: @@chambers)
    dcr.add_parameters(@@parameters, wafer_values)
    res = $client.send_dcr(dcr, export: :caller)
    assert $spctest.verify_dcr_accepted(res, @@parameters.size*wafer_values.size, nooc: cnt_ooc*2), "DCR not submitted successfully"
    #
    # verify wafer script parameters set
    test_ok = true
    wafer_values.each_key {|w|
      res = $sv.user_parameter('Wafer', w, name: 'OOC')
      if wafer_values.keys.index(w).even?
        test_ok &= (res.value =='Yes')
      else
        test_ok &= !res.valueflag
      end
    }
    assert test_ok, "wafer script parameters not set as expected"
    # verify external comments
    assert $spctest.verify_ecomment(channels, "SET_WAFER_SCRIPT_PARAMETER: reason={OOC/Yes}")
  end

  def test0735_dont_set_lot_script_parameter_ooc_integer
    channels = create_clean_channels
    assert $spctest.assign_verify_cas(channels,
      ['AutoSetLotScriptParameter-OOCInteger'])
    assert $sv.user_parameter_delete_verify('Lot', @@lot, "OOCInteger"), "cannot cleanup script parameter"
    #
    submit_process_dcr(@@ppd_qa, wafer_values: @@wafer_values)
    # send DCR with OOC values
    dcr = Space::DCR.new(lot: @@lot, eqp: @@mtool, op: @@mpd_qa)
    dcr.add_parameters(@@parameters, @@wafer_values2ooc)  # @@wafer_valuesooc ?
    res = $client.send_dcr(dcr, export: :caller)
    assert $spctest.verify_dcr_accepted(res, @@parameters.size*@@wafer_values2ooc.size, nooc: 'all*2'), "DCR not submitted successfully"
    #
    # verify lot script parameter is not set and not messed up
    res = $sv.user_parameter('Lot', @@lot, name: "OOCInteger")
    assert (res && !res.valueflag), "lot script parameter has been messed up"
  end

  def test0736_set_and_release_all_ca_collection
    chambers = {"PM1"=>5, "PM2"=>6}
    rparam = "L-STP-RETICLEID1"
    channels = create_clean_channels(department: 'LIT', mtool: @@eqp, chambers: chambers)
    sleep 30
    assert $spctest.assign_verify_cas(channels,
      ['AutoLotHold', 'AutoEquipmentInhibit', 'AutoChamberInhibit', 'AutoReticleInhibit', 'AutoRouteInhibit', 'CACollection-ReleaseAll'])
    #
    # send DCR with OOC values
    dcr = Space::DCR.new(eqp: @@eqp, eqptype: 'PROCESS-CHAMBER', chambers: chambers, lot: @@lot, department: 'LIT', op: @@mpd_lit, route: @@route)
    dcr.add_parameters(@@parameters, @@wafer_values2ooc)
    dcr.add_parameter(rparam, {@@lot=>@@reticle}, reading_type: 'Lot', space_listening: true)
    dcr.parameters[1][:Reading][0][:Reticle] = @@reticle
    assert $spctest.send_dcr_verify(dcr, nsamples: @@parameters.size*@@wafer_values2ooc.size, nooc: 'all*2'), "error sending DCR"
    #
    sleep 120
    # verify equipment inhibit and comment
    assert $spctest.verify_inhibit(@@eqp, channels.size), "equipment must have an inhibit"
    assert $spctest.verify_ecomment(channels, "TOOL_HELD: #{@@eqp}: reason={X-S")
    #
    # verify futurehold and comment
    assert $spctest.verify_futurehold2(@@lot, "[(Raw above specification)"), "lot must have a future hold"
    assert $spctest.verify_ecomment(channels, "LOT_HELD: #{@@lot}: reason={X-S")
    #
    # verify chamber inhibit and comment
    assert $spctest.verify_inhibit(@@eqp, chambers.count*channels.size, class: :eqp_chamber), "chamber must have an inhibit"
    assert $spctest.verify_ecomment(channels, "TOOL+CHAMBER_HELD: #{@@eqp}/PM1: reason={X-S")
    #
    # verify reticle comment and inhibit
    assert $spctest.verify_inhibit(@@reticle, channels.size, class: :reticle), "reticle must have an inhibit"
    assert $spctest.verify_ecomment(channels, "RETICLE_HELD:#{@@reticle}: reason={X-S")
    #
    # verify route comment and inhibit is set
    assert $spctest.verify_inhibit(@@route, channels.size, class: :route), "route must have an inhibit"
    assert $spctest.verify_ecomment(channels, "ROUTE_HELD:#{@@route}: reason={X-S")
    #    
    sleep 120
    channels.each {|ch|
      s = ch.samples(ckc: true)[-1]
      assert s.assign_ca($spc.corrective_action("CACollection-ReleaseAll")), "error assigning corective action"
      #assignca = s.assign_ca($spc.corrective_action("CACollection-ReleaseAll"))
      #assert assignca, "error assigning corective action"
      #$log.info "Assign CACollection-ReleaseAll result: #{assignca.inspect}"
    }
    s = channels[0].samples(ckc: true)[-1]
    assert $spctest.verify_futurehold_cancel(@@lot, "[(Raw above specification)"), "lot must have no future hold"
    assert $spctest.verify_inhibit(@@eqp, 0), "equipment must not have an inhibit"
    assert $spctest.verify_inhibit(@@eqp, 0, class: :eqp_chamber), "chamber must not have an inhibit"
    assert $spctest.verify_inhibit(@@reticle, 0, class: :reticle), "reticle must not have an inhibit"
    assert $spctest.verify_inhibit(@@route, 0, class: :route), "route must not have an inhibit"
    #
    # LotHold (manual action, prepare release)
    assert s.assign_ca($spc.corrective_action("LotHold")), "error assigning corective action"
    assert $spctest.verify_futurehold2(@@lot, ""), "lot must have a future hold"
    #
    # ReleaseLotHold with CACollection-ReleaseAll
    assert_equal 0, $sv.lot_gatepass(@@lot), "SiView GatePass error"
    assert $spctest.verify_futurehold_cancel(@@lot, nil), "lot must have no future hold"
    count = $sv.lot_hold_list(@@lot, type: 'FutureHold').size
    assert count > 0, "lot must be on hold"
    $log.info "Holds: #{$sv.lot_hold_list(@@lot, type: 'FutureHold')}"
    #
    assert s.assign_ca($spc.corrective_action("CACollection-ReleaseAll")), "error assigning corective action"
    sleep 240
    lhl = $sv.lot_hold_list(@@lot, type: 'FutureHold').size
    $log.info "Holds: #{$sv.lot_hold_list(@@lot, type: 'FutureHold')}"
    assert lhl < count, "lot must not be on hold"
  end

  def test0737_set_one_and_release_all_ca_collection
    chambers = {"PM1"=>5, "PM2"=>6}
    rparam = "L-STP-RETICLEID1"
    #
    channels = create_clean_channels(department: 'LIT', mtool: @@eqp, chambers: chambers)
    assert $spctest.assign_verify_cas(channels, ['AutoChamberInhibit', 'CACollection-ReleaseAll'])
    #
    # send DCR with OOC values
    dcr = Space::DCR.new(eqp: @@eqp, eqptype: 'PROCESS-CHAMBER', chambers: chambers, lot: @@lot, department: 'LIT', op: @@mpd_lit, route: @@route)
    dcr.add_parameters(@@parameters, @@wafer_values2ooc)
    dcr.add_parameter(rparam, {@@lot=>@@reticle}, reading_type: 'Lot') # ??, space_listening: true)
    dcr.parameters[1][:Reading][0][:Reticle] = @@reticle
    assert $spctest.send_dcr_verify(dcr, nsamples: @@parameters.size*@@wafer_values2ooc.size, nooc: 'all*2'), "error sending DCR"
    #
    # verify chamber inhibit and comment
    assert $spctest.verify_inhibit(@@eqp, chambers.count*channels.size, class: :eqp_chamber), "chamber must have an inhibit"
    assert $spctest.verify_ecomment(channels, "TOOL+CHAMBER_HELD: #{@@eqp}/PM1: reason={X-S")
    #
    channels.each {|ch|
      s = ch.samples(ckc: true)[-1]
      s.assign_ca($spc.corrective_action("CACollection-ReleaseAll"))
    }
    assert $spctest.verify_inhibit(@@eqp, 0, class: :eqp_chamber), "chamber must not have an inhibit"
  end

  def test0738_set_one_and_release_all_ca_collection
    chambers = {"PM1"=>5, "PM2"=>6}
    rparam = "L-STP-RETICLEID1"
    #
    channels = create_clean_channels(department: 'LIT', mtool: @@eqp, chambers: chambers)
    assert $spctest.assign_verify_cas(channels, ['AutoLotHold', 'CACollection-ReleaseAll'])
    #
    # send DCR with OOC values
    dcr = Space::DCR.new(lot: @@lot, eqp: @@eqp, department: 'LIT', op: @@mpd_lit, route: @@route, chambers: chambers)
    dcr.add_parameters(@@parameters, @@wafer_values2ooc)
    dcr.add_parameter(rparam, {@@lot=>@@reticle}, reading_type: 'Lot', space_listening: true, Reticle: @@reticle)
    dcr.parameters[1][:Reading][0][:Reticle] = @@reticle
    assert $spctest.send_dcr_verify(dcr, nsamples: @@parameters.size*@@wafer_values2ooc.size, nooc: 'all*2'), "error sending DCR"
    #
    # verify futurehold and comment
    assert $spctest.verify_futurehold2(@@lot, "[(Raw above specification)"), "lot must have a future hold"
    assert $spctest.verify_ecomment(channels, "LOT_HELD: #{@@lot}: reason={X-S")
    #
    channels.each {|ch|
      s = ch.samples(ckc: true)[-1]
      s.assign_ca($spc.corrective_action("CACollection-ReleaseAll"))
    }
    s = channels[0].samples(ckc: true)[-1]
    assert $spctest.verify_futurehold_cancel(@@lot, "[(Raw above specification)"), "lot must have no future hold"
  end

end
