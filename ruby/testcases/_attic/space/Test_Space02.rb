=begin 
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.
    
Author: Steffen Steidten, 2011-11-25
        Daniel Steger, 2012-07-05
=end

require_relative 'Test_Space_Setup'


# Test Space/SIL.
class Test_Space02 < Test_Space_Setup
  @@file_generic_keys_dc_lot_grouping = 'etc/testcasedata/space/GenericKeySetup_QA_DC_LOTGrouping.SpecID.xlsx'
  
  
  def setup
    super
    return unless $lds && self.class.class_variable_defined?(:@@lot) # bypass test0000_setup
    assert_equal 0, $sv.eqp_cleanup(@@eqp)
    assert_equal 0, $sv.lot_cleanup(@@lot)
    #
    $lds.all_spc_channels('QA_PARAM_90*').each {|ch| assert ch.delete}
    folder = $lds.folder(@@autocreated_qa)
    assert folder.delete_channels, 'error cleaning channels' if folder
  end


  def test0200_module_template
    $setup_ok = false
    #
    assert $spctest.templates_exist($lds.folder(@@templates_folder), [@@params_template]), 'missing template'
    #
    # clean up test folders
    [@@autocreated_nodep, @@autocreated_qa, @@module_qa].each {|f|
      folder = $lds.folder(f)
      assert folder.delete_channels, "error deleting channels in #{folder.name}" if folder
    }
    #
    # build and submit DCR
    dcr = Space::DCR.new(eqp: @@eqp, lot: @@lot)
    dcr.add_parameters(@@parameters, @@wafer_values)
    res = $client.send_dcr(dcr, export: :caller)
    assert $spctest.verify_dcr_accepted(res, @@parameters.size*@@wafer_values.size), 'DCR not submitted successfully'
    #
    # verify spc channels
    folder = $lds.folder(@@autocreated_qa)
    dcrl = dcr.context[:JobSetup][:SiViewControlJob][:FOUP][0][:Lot][0]
    sleep 10
    @@parameters.each {|p|
      ch = folder.spc_channel(parameter: p)
      # get the unit from speclimit and verify
      assert limits = $spctest.sildb.speclimit(parametername: p, active: 'Y', pd: dcrl['operationID'], technology: dcrl['technology'], 
        route: dcrl['route'], product: dcrl['productID'], productgroup: dcrl['productGroupID']), 'no limits'
      assert_equal 1, limits.size, "limits are not uniqe for parametername: #{p}, active: 'Y', pd: #{dcrl['operationID']}, technology: #{dcrl['technology']}, 
        route: #{dcrl['route']}, product: #{dcrl['productID']}, productgroup: #{dcrl['productGroupID']}"
      unit = limits[0].unit || '-'
      unit = "#{unit[0..8]}*"  if unit.length > 10 # Is this the correct behavior?
      assert_equal unit, ch.unit, "wrong unit for #{p}"
      #
      ref = {desc: "Auto-created using template: #{@@params_template}", parameter: p}
      assert $spctest.verify_channel(ch, ref, dcr, @@wafer_values), 'wrong channel data'
      # ensure spec and sample limits are set
      para_slimits = Hash[@@parameters.collect {|p| [p, [25.0, 45.0, 75.0]]}]
      ch.samples.each_with_index {|s, i|
        assert $spctest.verify_sample_speclimits(s, 25.0, 45.0, 75.0)
        assert $spctest.verify_dcr_speclimits(res, para_slimits)
        limits = s.limits
        refute_nil limits['MEAN_VALUE_UCL'], "no limit UCL for parameter #{p}, sample #{i}"
        refute_nil limits['MEAN_VALUE_CENTER'], "no limit CENTER for parameter #{p}, sample #{i}"
        refute_nil limits['MEAN_VALUE_LCL'], "no limit LCL for parameter #{p}, sample #{i}"
      }
    }
    #
    $setup_ok = true
  end

  def xxxtest0200a_module_template_chart_check
    $setup_ok = false
    #
    assert $spctest.templates_exist($lds.folder(@@templates_folder), [@@params_template]), 'missing template'
    #
    # clean up test folders
    [@@autocreated_nodep, @@autocreated_qa, @@module_qa].each {|f|
      folder = $lds.folder(f)
      assert folder.delete_channels, "error deleting channels in #{folder.name}" if folder
    }
    #
    # build and submit DCR
    parameters = ['QA_PARAM_900no', 'QA_PARAM_901no', 'QA_PARAM_902no', 'QA_PARAM_903no', 'QA_PARAM_904no']
    dcr = Space::DCR.new(eqp: @@eqp, lot: @@lot, technology: 'TEST', op: 'xxx')
    dcr.add_parameters(parameters, @@wafer_values)
    res = $client.send_dcr(dcr, export: :caller)
    assert $spctest.verify_dcr_accepted(res, parameters.size*@@wafer_values.size), 'DCR not submitted successfully'
    #
    # verify spc channels
    folder = $lds.folder(@@autocreated_qa)
    dcrl = dcr.context[:JobSetup][:SiViewControlJob][:FOUP][0][:Lot][0]
    sleep 10
    parameters.each {|p|
      ch = folder.spc_channel(parameter: p)
      # get the unit from speclimit and verify
      assert limits = $spctest.sildb.speclimit(parametername: p, active: 'Y', pd: dcrl['operationID'], technology: dcrl['technology'], 
        route: dcrl['route'], product: dcrl['productID'], productgroup: dcrl['productGroupID']), 'no limits'
      assert_equal 1, limits.size, "limits are not uniqe for parametername: #{p}, active: 'Y', pd: #{dcrl['operationID']}, technology: #{dcrl['technology']}, 
        route: #{dcrl['route']}, product: #{dcrl['productID']}, productgroup: #{dcrl['productGroupID']}"
      unit = limits[0].unit || '-'
      unit = "#{unit[0..8]}*"  if unit.length > 10 # Is this the correct behavior?
      assert_equal unit, ch.unit, "wrong unit for #{p}"
      #
      ref = {desc: "Auto-created using template: #{@@params_template}", parameter: p}
      assert $spctest.verify_channel(ch, ref, dcr, @@wafer_values), 'wrong channel data'
      # ensure spec and sample limits are set
      para_slimits = Hash[parameters.collect {|p| [p, [25.0, 45.0, 75.0]]}]
      ch.samples.each_with_index {|s, i|
        assert $spctest.verify_sample_speclimits(s, 25.0, 45.0, 75.0)
        assert $spctest.verify_dcr_speclimits(res, para_slimits)
        limits = s.limits
        refute_nil limits['MEAN_VALUE_UCL'], "no limit UCL for parameter #{p}, sample #{i}"
        refute_nil limits['MEAN_VALUE_CENTER'], "no limit CENTER for parameter #{p}, sample #{i}"
        refute_nil limits['MEAN_VALUE_LCL'], "no limit LCL for parameter #{p}, sample #{i}"
      }
    }
    #
    $setup_ok = true
  end

  def test0201_invalid_wafer_readings
    # build and submit DCR
    dcr = Space::DCR.new(eqp: @@eqp, lot: @@lot)
    # All readings invalid
    dcr.add_parameter(@@parameters[0], @@wafer_values, reading_valid: false)
    # Parameter invalid
    dcr.add_parameter(@@parameters[1], @@wafer_values, reading_valid: true, parameter_valid: false)
    # Some readings invalid
    dcr.add_parameter(@@parameters[2], @@wafer_values)
    dcr.add_readings({'INVALIDWAFER' => 222}, reading_valid: false)
    dcr.add_readings({@@wafer_values.keys[0] => 443}, reading_valid: false)
    # send DCR
    assert $spctest.send_dcr_verify(dcr, nsamples: @@wafer_values.size), 'DCR not submitted successfully'
    #
    # verify spc channels
    folder = $lds.folder(@@autocreated_qa)
    # No new data for param 0 and 1
    assert $spctest.verify_parameters(@@parameters[0..1], folder, 0)
    # New data for valid readings of parameter 2
    assert $spctest.verify_parameters(@@parameters[2], folder, @@wafer_values.size)
    # Check parameter channel with new data
    ch = folder.spc_channel(parameter: @@parameters[2])
    ref_ch = Space::SpcApi::ChannelData.new(nil, nil, "Auto-created using template: #{@@params_template}", @@parameters[2])
    # passes as of 4.7.3
    assert $spctest.verify_channel(ch, ref_ch, dcr, @@wafer_values, nsamples: 1, last_sample_only: true), 'wrong channel data'
  end
  
  def test0202_invalid_lot_readings
    lot_value = {@@lot=>22.0}
    #
    # build and submit DCR
    dcr = Space::DCR.new(eqp: @@eqp, lot: @@lot, department: 'QA')
    # All readings invalid
    dcr.add_parameter(@@parameters[0], lot_value, reading_type: 'Lot', reading_valid: false)
    # Parameter invalid
    dcr.add_parameter(@@parameters[1], lot_value, reading_type: 'Lot', reading_valid: true, parameter_valid: false)
    # Some readings invalid
    dcr.add_parameter(@@parameters[2], lot_value, reading_type: 'Lot')
    dcr.add_readings({'INVALIDLOT' => 222}, reading_type: 'Lot', reading_valid: false)
    dcr.add_readings({@@lot => 443}, reading_type: 'Lot', reading_valid: false)
    #
    assert $spctest.send_dcr_verify(dcr, nsamples: 1), 'DCR not submitted successfully'
    #
    # verify spc channel   
    assert $spctest.verify_parameters([@@parameters[2]], $lds.folder(@@autocreated_qa), 1) 
  end
    
  def test0205_csv_routes_in_spec_limits
    route = 'SIL-0002.01'
    #
    # build and submit DCR
    dcr = Space::DCR.new(eqp: @@eqp, lot: @@lot, route: route)
    dcr.add_parameters(@@parameters, @@wafer_values, space_listening: true)
    assert $spctest.send_dcr_verify(dcr, nsamples: @@parameters.size*@@wafer_values.size), 'DCR not submitted successfully'
    #
    folder = $lds.folder(@@autocreated_qa)
    @@parameters.each {|p|
      ch = folder.spc_channel(parameter: p)
      # ensure spec and sample limits are set
      ch.samples.each_with_index {|s, i|
        specs = s.specs
        refute_nil specs['USL'], "no USL for parameter #{p}, sample #{i}"
        refute_nil specs['TARGET'], "no TARGET for parameter #{p}, sample #{i}"
        refute_nil specs['LSL'], "no LSL for parameter #{p}, sample #{i}"
        limits = s.limits
        refute_nil limits['MEAN_VALUE_UCL'], "no limit UCL for parameter #{p}, sample #{i}"
        refute_nil limits['MEAN_VALUE_CENTER'], "no limit CENTER for parameter #{p}, sample #{i}"
        refute_nil limits['MEAN_VALUE_LCL'], "no limit LCL for parameter #{p}, sample #{i}"
      }
    }
  end

  def test0206_unconfigured_route
    skip "not applicable in #{$env}" unless ['itdc', 'let'].member?($env)
    route = 'SIL-0003.01'
    #
    # create channels
    dcr = Space::DCR.new(eqp: @@eqp, lot: @@lot)
    dcr.add_parameters(@@parameters, @@wafer_values, space_listening: true)
    assert $spctest.send_dcr_verify(dcr, nsamples: @@parameters.size*@@wafer_values.size), 'DCR not submitted successfully'
    #
    # build and submit DCR
    dcr = Space::DCR.new(eqp: @@eqp, lot: @@lot, route: route)
    dcr.add_parameters(@@parameters, @@wafer_values, space_listening: true)
    assert $spctest.send_dcr_verify(dcr, nsamples: @@parameters.size*@@wafer_values.size), 'DCR not submitted successfully'
    #
    folder = $lds.folder(@@autocreated_qa)
    @@parameters.each {|p|
      ch = folder.spc_channel(parameter: p)
      # ensure spec and sample limits are set
      s = ch.samples[-1]
      specs = s.specs
      assert_nil specs['USL'], "USL for parameter #{p} must not be set, check if speclimitlookup.route.required=true"
      assert_nil specs['TARGET'], "TARGET for parameter #{p} must not be set, check if speclimitlookup.route.required=true"
      assert_nil specs['LSL'], "no LSL for parameter #{p} must not be set, check if speclimitlookup.route.required=true"
      limits = s.limits
      refute_nil limits['MEAN_VALUE_UCL'], "no limit UCL for parameter #{p}"
      refute_nil limits['MEAN_VALUE_CENTER'], "no limit CENTER for parameter #{p}"
      refute_nil limits['MEAN_VALUE_LCL'], "no limit LCL for parameter #{p}"
    }
  end
  
  def test0207a_parameter_extension_minus
    route = 'SIL-0001.01'
    #
    # build and submit DCR
    p = 'QA_PARAM_900'
    ctrl = {'MEAN'=>41, 'STDDEV'=>1, 'MIN'=>31, 'MAX'=>55, 'COUNT'=>5}
    
    # To test MSR873848/GIRA MTQA-2272 enable this instead
    #ctrl = {'MEAN'=>41, 'STDDEV'=>999999, 'MIN'=>31, 'MAX'=>55, 'COUNT'=>5}
    #
    dcr = Space::DCR.new(eqp: @@eqp, lot: @@lot, route: route)
    ctrl.each {|k, v| dcr.add_parameter("#{p}-#{k}", {'2502J493KO'=>v}, space_listening: true) }
    res = $client.send_dcr(dcr, export: :caller)
    # To test MSR873848/GIRA MTQA-2272 enable this instead
    # $log.info "Result: #{res}"
    assert $spctest.verify_dcr_accepted(res, 1), 'DCR not submitted successfully'
    #
    folder = $lds.folder(@@autocreated_qa)
    ch = folder.spc_channel(parameter: p)
    s = ch.samples.last
    assert $spctest.verify_sample_statistics(s, ctrl)
    assert $spctest.verify_sample_speclimits(s, 25.0, 45.0, 75.0)
    assert $spctest.verify_dcr_speclimits(res, {p=>[25.0, 45.0, 75.0]})
  end
  
  def test0207b_parameter_extension_minus_test
    route = 'SIL-0207b.01'
    #
    # build and submit DCR
    p = 'QA_PARAM_0207b_ECP'
    ctrl = {'MEAN'=>41, 'STDDEV'=>1, 'MIN'=>31, 'MAX'=>55, 'COUNT'=>5}
    #
    dcr = Space::DCR.new(eqp: @@eqp, lot: @@lot, route: route, technology: 'TEST')
    ctrl.each {|k, v| dcr.add_parameter("#{p}-#{k}", {'2502J493KO'=>v}, space_listening: true) }
    res = $client.send_dcr(dcr, export: :caller)
    assert $spctest.verify_dcr_accepted(res, 1), 'DCR not submitted successfully'
    #
    folder = $lds_setup.folder(@@autocreated_qa)
    ch = folder.spc_channel(parameter: p)
    s = ch.samples.last
    assert $spctest.verify_sample_statistics(s, ctrl)
    assert $spctest.verify_sample_speclimits(s, 25.0, 45.0, 75.0)
    assert $spctest.verify_dcr_speclimits(res, {p=>[25.0, 45.0, 75.0]})
    if @@autoqual_reporting_setup_enabled == true
      assert $spctest.verify_aqv_message(dcr: dcr, lot: @@lot, wafer: ['2502J493KO'], result: 'success'), 
        'AQV message not as expected'
    end
  end
  
  def test0208_parameter_extension_mean_only
    route = 'SIL-0001.01'
    #
    # build and submit DCR
    p = 'QA_PARAM_901'
    value = 41
    #
    dcr = Space::DCR.new(eqp: @@eqp, lot: @@lot, route: route)
    dcr.add_parameter("#{p}-MEAN", {'2502J493KO'=>value}, space_listening: true)
    res = $client.send_dcr(dcr, export: :caller)
    assert $spctest.verify_dcr_accepted(res, 1), 'DCR not submitted successfully'
    folder = $lds.folder(@@autocreated_qa)
    ch = folder.spc_channel(parameter: p)
    s = ch.samples[-1]
    assert $spctest.verify_sample_statistics(s, {'MEAN'=>value, 'STDDEV'=>0, 'MIN'=>value, 'MAX'=>value, 'COUNT'=>1})
    assert $spctest.verify_sample_speclimits(s, 25.0, 45.0, 75.0)
    assert $spctest.verify_dcr_speclimits(res, {p=>[25.0, 45.0, 75.0]})
  end
  
  def test0210_parameter_extension_wafer_aggregated_only
    route = 'SIL-0001.01'
    p = 'QA_PARAM_902'
    ctrl = {'MEAN'=>41, 'STDDEV'=>1, 'MIN'=>31, 'MAX'=>55}
    #
    dcr = Space::DCR.new(eqp: @@eqp, lot: @@lot, route: route)
    ctrl.each {|k,v| dcr.add_parameter("#{p}-#{k}", {'2502J493KO'=>v}) }
    res = $client.send_dcr(dcr, export: :caller)
    assert $spctest.verify_dcr_accepted(res, 1), 'DCR not submitted successfully'
    # ensure spec and sample limits are set
    folder = $lds.folder(@@autocreated_qa)
    ch = folder.spc_channel(parameter: p)
    s = ch.samples[-1]
    assert $spctest.verify_sample_statistics(s, ctrl.merge('COUNT'=>2))
    assert $spctest.verify_sample_speclimits(s, 25.0, 45.0, 75.0)
    assert $spctest.verify_dcr_speclimits(res, {p=>[25.0, 45.0, 75.0]})
  end

  def test0211_parameter_extension_lot_aggregated_only
    route = 'SIL-0001.01'
    p = 'QA_PARAM_902'
    ctrl = {'MEAN'=>41, 'STDDEV'=>1, 'MIN'=>31, 'MAX'=>55}
    #
    dcr = Space::DCR.new(eqp: @@eqp, lot: @@lot, route: route)
    ctrl.each {|k, v| dcr.add_parameter("#{p}-#{k}", {@@lot=>v}, reading_type: 'Lot')}
    res = $client.send_dcr(dcr, export: :caller)
    assert $spctest.verify_dcr_accepted(res, 1), 'DCR not submitted successfully'
    # ensure spec and sample limits are set
    folder = $lds.folder(@@autocreated_qa)
    ch = folder.spc_channel(parameter: p)
    s = ch.samples[-1]
    assert $spctest.verify_sample_statistics(s, ctrl.merge('COUNT'=>2))
    assert $spctest.verify_sample_speclimits(s, 25.0, 45.0, 75.0)
    assert $spctest.verify_dcr_speclimits(res, {p=>[25.0, 45.0, 75.0]})
  end
  
  #MSR554733: TODO expected behavior yet unclear
  def XXXtest0212_parameter_extension_lot_stddev_only
    route = 'SIL-0001.01'
    p = 'QA_PARAM_902'
    value = 1
    #
    # build and submit DCR
    dcr = Space::DCR.new(eqp: @@eqp, lot: @@lot, route: route)
    dcr.add_parameter("#{p}-STDDEV", {@@lot=>value}, reading_type: 'Lot')
    res = $client.send_dcr(dcr, export: :caller)
    assert $spctest.verify_dcr_accepted(res, 1), 'DCR not submitted successfully'
    # ensure spec and sample limits are set
    folder = $lds.folder(@@autocreated_qa)
    ch = folder.spc_channel(parameter: p)
    s = ch.samples[-1]
    assert $spctest.verify_sample_statistics(s, {'MEAN'=>0, 'STDDEV'=>value, 'MIN'=>0, 'MAX'=>0, 'COUNT'=>1})
    assert $spctest.verify_sample_speclimits(s, 25.0, 45.0, 75.0)
    assert $spctest.verify_dcr_speclimits(res, {p=>[25.0, 45.0, 75.0]})
  end
  
  def test0220_parameter_extension_lot_only_single_chamber_sites
    #processing_areas_behaviour = 'cycle_wafer'
    processing_areas_behaviour = 'cycle_parameter'
    chambers = {'CHA'=>5}
    sample_count = (processing_areas_behaviour == 'cycle_wafer') ? chambers.size : 1
    #
    # build and submit DCR
    parameters = 5.times.collect {|i| "QA_PARAM_90#{i}-LOT"}
    nsamples = parameters.size
    nsamples *= chambers.size if processing_areas_behaviour == 'cycle_wafer'
    #
    lot = "RW#{Time.now.to_i}.00"
    wafer_values, wvalues, wafer_stats = _generate_wafer_values(25, sample_count, 3)
    dcr = Space::DCR.new(eqp: @@eqp, eqptype: 'PROCESS-CHAMBER', lot: lot, op: @@ppd_qa, chambers: chambers)
    dcr.add_parameters(parameters, wafer_values, processing_areas: processing_areas_behaviour, sites: true)
    assert $spctest.send_dcr_verify(dcr, nsamples: nsamples)
    # verify comment is set
    folder = $lds.folder(@@autocreated_qa)
    channels = parameters.collect {|p| folder.spc_channel(parameter: p)}
    assert $spctest.verify_ecomment(channels, 'LOT_HELD: reason={X-S')
    #
    parameters.each {|p|
      ch = folder.spc_channel(parameter: p)
      # ensure spec and sample limits are set
      assert_equal sample_count, ch.samples.count, ' wrong sample count'
      ch.samples.each_with_index {|s, i|
        assert $spctest.verify_data(s.statistics, wafer_stats[i]), "statistics for #{s.inspect} do not match"
        assert verify_sample_raw_values(s, wvalues[i], true), "raw values for #{s.inspect} do not match"
        assert_equal '-', s.extractor_keys['Wafer'], 'wrong WaferID value in extractor key wafer'
        assert_equal '-', s.data_keys['MSlot'], 'wrong WaferID value in extractor key wafer'
        assert_equal '-', s.data_keys['PSequence'], 'wrong PSequence value in extractor key wafer'
        assert_equal chambers.keys[i], s.extractor_keys['PChamber'], 'wrong ChamberID value in extractor key PChamber'
        assert_equal dcr.context[:Equipment]['id'], s.extractor_keys['PTool'], 'wrong ToolID value in extractor key PTool'
      }
    }
  end
  
  def test0221_parameter_extension_lot_only_multiple_chambers
    processing_areas_behaviour = 'cycle_wafer'
    #processing_areas_behaviour = 'cycle_parameter'
    chambers = {'CHA'=>5, 'CHB'=>6, 'CHC'=>7}
    sample_count = (processing_areas_behaviour == 'cycle_wafer') ? chambers.size : 1
    #
    # build and submit DCR
    wafer_values, wvalues, wafer_stats = _generate_wafer_values(24, sample_count)    
    parameters = 5.times.collect {|i| "QA_PARAM_90#{i}-LOT"}
    nsamples = parameters.size
    nsamples *= chambers.size if processing_areas_behaviour == 'cycle_wafer'
    #
    dcr = Space::DCR.new(eqp: @@eqp, eqptype: 'PROCESS-CHAMBER', lot: @@lot, op: @@ppd_qa, chambers: chambers)
    dcr.add_parameters(parameters, wafer_values, processing_areas: processing_areas_behaviour, space_listening: true)
    assert $spctest.send_dcr_verify(dcr, nsamples: nsamples)
    #
    folder = $lds.folder(@@autocreated_qa)
    parameters.each {|p|
      ch = folder.spc_channel(parameter: p)
      # ensure spec and sample limits are set
      assert_equal sample_count, ch.samples.count, ' wrong sample count'
      chambers.keys.each_with_index {|ck, i|
        s = ch.samples(chamber: ck)[0]
        assert $spctest.verify_data(s.statistics, wafer_stats[i]), "Statistics for #{s.inspect} do not match"
        assert verify_sample_raw_values(s, wvalues[i], false), "Raw values for #{s.inspect} do not match"
        assert_equal '-', s.extractor_keys['Wafer'], 'wrong WaferID value in extractor key wafer'
        assert_equal '-', s.data_keys['MSlot'], 'wrong WaferID value in extractor key wafer'
        assert_equal '-', s.data_keys['PSequence'], 'wrong PSequence value in extractor key wafer'
        assert_equal ck, s.extractor_keys['PChamber'], 'wrong ChamberID value in extractor key PChamber'
        assert_equal dcr.context[:Equipment]['id'], s.extractor_keys['PTool'], 'wrong ToolID value in extractor key PTool'
      }
    }
  end
  
  def test0222_parameter_extension_lot_only_multiple_chambers
    #processing_areas_behaviour = 'cycle_wafer'
    processing_areas_behaviour = 'cycle_parameter'
    chambers = {'CHA'=>5, 'CHB'=>6, 'CHC'=>7}
    sample_count = (processing_areas_behaviour == 'cycle_wafer') ? chambers.size : 1
    #
    # build and submit DCR
    wafer_values, wvalues, wafer_stats = _generate_wafer_values(25, sample_count)
    parameters = 5.times.collect {|i| "QA_PARAM_90#{i}-LOT"}
    nsamples = parameters.size
    nsamples *= chambers.size if processing_areas_behaviour == 'cycle_wafer'
    #
    dcr = Space::DCR.new(eqp: @@eqp, eqptype: 'PROCESS-CHAMBER', lot: @@lot, op: @@ppd_qa, chambers: chambers)
    dcr.add_parameters(parameters, wafer_values, processing_areas: processing_areas_behaviour, space_listening: true)
    assert $spctest.send_dcr_verify(dcr, nsamples: nsamples)
    #
    folder = $lds.folder(@@autocreated_qa)
    parameters.each {|p|
      ch = folder.spc_channel(parameter: p)
      # ensure spec and sample limits are set
      assert_equal sample_count, ch.samples.count, ' wrong sample count'
      ch.samples.each_with_index {|s, i|
        assert $spctest.verify_data(s.statistics, wafer_stats[i]), "Statistics for #{s.inspect} do not match"
        assert verify_sample_raw_values(s, wvalues[i], false), "Raw values for #{s.inspect} do not match"
        assert_equal '-', s.extractor_keys['Wafer'], 'wrong WaferID value in extractor key wafer'
        assert_equal '-', s.data_keys['MSlot'], 'wrong WaferID value in extractor key wafer'
        assert_equal '-', s.data_keys['PSequence'], 'wrong PSequence value in extractor key wafer'
        assert_equal chambers.keys.join(':'), s.extractor_keys['PChamber'], 'wrong ChamberID value in extractor key PChamber'
        assert_equal dcr.context[:Equipment]['id'], s.extractor_keys['PTool'], 'wrong ToolID value in extractor key PTool'
      }
    }
  end
  
  def test0223_parameter_extension_lot_single_chamber_generic_key_1_parameter
    assert $spctest.sil_upload_verify_generickey(@@file_generic_keys_dc_lot_grouping), 'file upload failed'
    chambers = {'NotSetupCHA'=>5}
    sample_count = 1
    wafer_count = 3
    parameter_count = 1
    wafer_values, wvalues, wafer_stats = _generate_wafer_values(wafer_count, sample_count)
    parameters = ['QA_PARAM_900-LOT']
    #
    # build and submit DCR for process PD
    dcrp = Space::DCR.new({eqp: @@eqp, lot: @@lot, op: @@ppd_qa, chambers: chambers, eqptype: 'PROCESS-CHAMBER'})
    dcrp.add_parameters(parameters, wafer_values, nocharting: false)
    wafer_count.times {|i| 
      dcrp.add_parameter("QA_COATER_#{i}", {"2502J49#{i}LE"=>"QA_CoaterUnit_#{i}"}, nocharting: true, value_type: 'string')
      dcrp.add_parameter("QA_WAFER_#{i}", {"2502J49#{i}LE"=>"WAFER:2502J49#{i}LE"}, nocharting: true, value_type: 'string')
    }
    dcrp.add_parameter('QA_LOT_PARAMETER', {@@lot=>'QA_LotValue'}, reading_type: 'Lot', nocharting: true, value_type: 'string')
    assert $spctest.send_dcr_verify(dcrp, nsamples: parameters.size), 'DCR not submitted successfully'
    #
    folder = $lds.folder(@@autocreated_qa)
    parameters.each {|p|
      ch = folder.spc_channel(parameter: p)
      # ensure spec and sample limits are set
      assert_equal sample_count, ch.samples.count, ' wrong sample count'
      ch.samples.each_with_index {|s, i|
        assert $spctest.verify_data(s.statistics, wafer_stats[i]), "Statistics for #{s.inspect} do not match"
        assert verify_sample_raw_values(s, wvalues[i], false), "Raw values for #{s.inspect} do not match"
        assert_equal '-', s.extractor_keys['Wafer'], 'wrong WaferID value in extractor key wafer'
        assert_equal '-', s.data_keys['MSlot'], 'wrong WaferID value in extractor key wafer'
        assert_equal '-', s.data_keys['PSequence'], 'wrong PSequence value in extractor key wafer'
        assert_equal '-', s.extractor_keys['PChamber'], 'wrong ChamberID value in extractor key PChamber'
        assert_equal dcrp.context[:Equipment]['id'], s.extractor_keys['PTool'], 'wrong ToolID value in extractor key PTool'
      }
    }
  end
  
  def test0224_parameter_extension_lot_single_chamber_generic_key_1_parameter
    assert $spctest.sil_upload_verify_generickey(@@file_generic_keys_dc_lot_grouping), 'file upload failed'
    chambers = {'CHA'=>5}
    sample_count = 1
    wafer_count = 3
    wafer_values, wvalues, wafer_stats = _generate_wafer_values(wafer_count, sample_count)
    parameters = ['QA_PARAM_900-LOT']
    #
    # build and submit DCR for process PD
    dcrp = Space::DCR.new(eqp: @@eqp, lot: @@lot, op: @@ppd_qa, chambers: chambers, eqptype: 'PROCESS-CHAMBER')
    dcrp.add_parameters(parameters, wafer_values)
    wafer_count.times {|i| 
      dcrp.add_parameter("QA_COATER_#{i}", {"2502J49#{i}LE"=>"QA_CoaterUnit_#{i}"}, nocharting: true)
      dcrp.add_parameter("QA_WAFER_#{i}", {"2502J49#{i}LE"=>"WAFER:2502J49#{i}LE"}, nocharting: true)
    }
    dcrp.add_parameter('QA_LOT_PARAMETER', {@@lot=>'QA_LotValue'}, reading_type: 'Lot', nocharting: true)
    #res = $client.send_dcr(dcrp, export: :caller)
    assert $spctest.send_dcr_verify(dcrp, nsamples: parameters.size), 'DCR not submitted successfully'
    #
    folder = $lds.folder(@@autocreated_qa)
    parameters.each {|p|
      ch = folder.spc_channel(parameter: p)
      assert_equal sample_count, ch.samples.count, ' wrong sample count'
      ch.samples.each_with_index {|s, i|
        assert $spctest.verify_data(s.statistics, wafer_stats[i]), "Statistics for #{s.inspect} do not match"
        assert verify_sample_raw_values(s, wvalues[i], false), "Raw values for #{s.inspect} do not match"
        assert_equal '-', s.extractor_keys['Wafer'], 'wrong WaferID value in extractor key wafer'
        assert_equal '-', s.data_keys['MSlot'], 'wrong WaferID value in extractor key wafer'
        assert_equal '-', s.data_keys['PSequence'], 'wrong PSequence value in extractor key wafer'
        assert_equal chambers.keys[i], s.extractor_keys['PChamber'], 'wrong ChamberID value in extractor key PChamber'
        assert_equal dcrp.context[:Equipment]['id'], s.extractor_keys['PTool'], 'wrong ToolID value in extractor key PTool'
      }
    }
  end
  
  def test0225_parameter_extension_lot_only_single_chamber
    #processing_areas_behaviour = 'cycle_wafer'
    processing_areas_behaviour = 'cycle_parameter'
    chambers = {'CHA'=>5}
    sample_count = (processing_areas_behaviour == 'cycle_wafer') ? chambers.size : 1
    #
    # build and submit DCR
    wafer_values, wvalues, wafer_stats = _generate_wafer_values(25, sample_count)
    parameters = 5.times.collect {|i| "QA_PARAM_90#{i}_LOT"}
    nsamples = parameters.size
    nsamples *= chambers.size if processing_areas_behaviour == 'cycle_wafer'
    #
    dcr = Space::DCR.new(eqp: @@eqp, eqptype: 'PROCESS-CHAMBER', lot: @@lot, op: @@ppd_qa, chambers: chambers)
    dcr.add_parameters(parameters, wafer_values, processing_areas: processing_areas_behaviour, space_listening: true)
    assert $spctest.send_dcr_verify(dcr, nsamples: nsamples)
    #
    folder = $lds.folder(@@autocreated_qa)
    parameters.each {|p|
      ch = folder.spc_channel(parameter: p)
      # ensure spec and sample limits are set
      assert_equal sample_count, ch.samples.count, 'wrong sample count'
      ch.samples.each_with_index {|s, i|
        assert $spctest.verify_data(s.statistics, wafer_stats[i]), "Statistics for #{s.inspect} do not match"
        assert verify_sample_raw_values(s, wvalues[i], false), "Raw values for #{s.inspect} do not match"
        assert_equal '-', s.extractor_keys['Wafer'], 'wrong WaferID value in extractor key wafer'
        assert_equal '-', s.data_keys['MSlot'], 'wrong WaferID value in extractor key wafer'
        assert_equal '-', s.data_keys['PSequence'], 'wrong PSequence value in extractor key wafer'
        assert_equal chambers.keys[i], s.extractor_keys['PChamber'], 'wrong ChamberID value in extractor key PChamber'
        assert_equal dcr.context[:Equipment]['id'], s.extractor_keys['PTool'], 'wrong ToolID value in extractor key PTool'
      }
    }
  end
  
  def test0226_parameter_extension_lot_only_multiple_chambers
    processing_areas_behaviour = 'cycle_wafer'
    #processing_areas_behaviour = 'cycle_parameter'
    chambers = {'CHA'=>5, 'CHB'=>6, 'CHC'=>7}
    sample_count = (processing_areas_behaviour == 'cycle_wafer') ? chambers.size : 1
    #
    # build and submit DCR
    wafer_values, wvalues, wafer_stats = _generate_wafer_values(25, sample_count)
    parameters = 5.times.collect {|i| "QA_PARAM_90#{i}_LOT"}
    nsamples = parameters.size
    nsamples *= chambers.size if processing_areas_behaviour == 'cycle_wafer'
    #
    dcr = Space::DCR.new(eqp: @@eqp, eqptype: 'PROCESS-CHAMBER', lot: @@lot, op: @@ppd_qa, chambers: chambers)
    dcr.add_parameters(parameters, wafer_values, processing_areas: processing_areas_behaviour, space_listening: true)
    assert $spctest.send_dcr_verify(dcr, nsamples: nsamples)
    #
    folder = $lds.folder(@@autocreated_qa)
    parameters.each {|p|
      ch = folder.spc_channel(parameter: p)
      # ensure spec and sample limits are set
      assert_equal sample_count, ch.samples.count, 'wrong sample count'
      chambers.keys.each_with_index {|ck, i|
        s = ch.samples(chamber: ck)[0]
        assert $spctest.verify_data(s.statistics, wafer_stats[i]), "Statistics for #{s.inspect} do not match"
        assert verify_sample_raw_values(s, wvalues[i], false), "Raw values for #{s.inspect} do not match"
        assert_equal '-', s.extractor_keys['Wafer'], 'wrong WaferID value in extractor key wafer'
        assert_equal '-', s.data_keys['MSlot'], 'wrong WaferID value in extractor key wafer'
        assert_equal '-', s.data_keys['PSequence'], 'wrong PSequence value in extractor key wafer'
        assert_equal ck, s.extractor_keys['PChamber'], 'wrong ChamberID value in extractor key PChamber'
        assert_equal dcr.context[:Equipment]['id'], s.extractor_keys['PTool'], 'wrong ToolID value in extractor key PTool'
      }
    }
  end
  
  def test0227_parameter_extension_lot_only_multiple_chambers
    #processing_areas_behaviour = 'cycle_wafer'
    processing_areas_behaviour = 'cycle_parameter'
    chambers = {'CHA'=>5, 'CHB'=>6, 'CHC'=>7}
    sample_count = (processing_areas_behaviour == 'cycle_wafer') ? chambers.size : 1
    chamber_keys = chambers.keys.sort
    #
    # build and submit DCR
    wafer_values, wvalues, wafer_stats = _generate_wafer_values(25, sample_count)
    parameters = 5.times.collect {|i| "QA_PARAM_90#{i}_LOT"}
    nsamples = parameters.size
    nsamples *= chambers.size if processing_areas_behaviour == 'cycle_wafer'
    #
    dcr = Space::DCR.new(eqp: @@eqp, eqptype: 'PROCESS-CHAMBER', lot: @@lot, op: @@ppd_qa, chambers: chambers)
    dcr.add_parameters(parameters, wafer_values, processing_areas: processing_areas_behaviour, space_listening: true)
    assert $spctest.send_dcr_verify(dcr, nsamples: nsamples)
    #
    folder = $lds.folder(@@autocreated_qa)
    parameters.each {|p|
      ch = folder.spc_channel(parameter: p)
      # ensure spec and sample limits are set
      assert_equal sample_count, ch.samples.count, 'wrong sample count'
      ch.samples.each_with_index {|s, i|
        assert $spctest.verify_data(s.statistics, wafer_stats[i]), "Statistics for #{s.inspect} do not match"
        assert verify_sample_raw_values(s, wvalues[i], false), "Raw values for #{s.inspect} do not match"
        assert_equal '-', s.extractor_keys['Wafer'], 'wrong WaferID value in extractor key wafer'
        assert_equal '-', s.data_keys['MSlot'], 'wrong WaferID value in extractor key wafer'
        assert_equal '-', s.data_keys['PSequence'], 'wrong PSequence value in extractor key wafer'
        assert_equal chambers.keys.join(':'), s.extractor_keys['PChamber'], 'wrong ChamberID value in extractor key PChamber'
        assert_equal dcr.context[:Equipment]['id'], s.extractor_keys['PTool'], 'wrong ToolID value in extractor key PTool'
      }
    }
  end
  
  def test0228_parameter_extension_lot_single_chamber_generic_key_1_parameter
    assert $spctest.sil_upload_verify_generickey(@@file_generic_keys_dc_lot_grouping), 'file upload failed'
    chambers = {'NotSetupCHA'=>5}
    sample_count = 1
    wafer_count = 3
    values_count = 13
    #
    # build and submit DCR
    wafer_values, wvalues, wafer_stats = _generate_wafer_values(wafer_count, sample_count)
    parameters = ['QA_PARAM_900_LOT']
    #
    # build and submit DCR for process PD
    dcrp = Space::DCR.new(eqp: @@eqp, lot: @@lot, op: @@ppd_qa, chambers: chambers, eqptype: 'PROCESS-CHAMBER')
    dcrp.add_parameters(parameters, wafer_values)
    wafer_count.times {|i| 
      dcrp.add_parameter("QA_COATER_#{i}", {"2502J49#{i}LE"=>"QA_CoaterUnit_#{i}"}, nocharting: true, value_type: 'string')
      dcrp.add_parameter("QA_WAFER_#{i}", {"2502J49#{i}LE"=>"WAFER:2502J49#{i}LE"}, nocharting: true, value_type: 'string')
    }
    dcrp.add_parameter('QA_LOT_PARAMETER', {@@lot=>'QA_LotValue'}, reading_type: 'Lot', nocharting: true, value_type: 'string')
    assert $spctest.send_dcr_verify(dcrp, nsamples: parameters.size), 'DCR not submitted successfully'
    #
    folder = $lds.folder(@@autocreated_qa)
    parameters.each {|p|
      ch = folder.spc_channel(parameter: p)
      # ensure spec and sample limits are set
      assert_equal sample_count, ch.samples.count, 'wrong sample count'
      ch.samples.each_with_index {|s, i|
        assert $spctest.verify_data(s.statistics, wafer_stats[i]), "Statistics for #{s.inspect} do not match"
        assert verify_sample_raw_values(s, wvalues[i], false), "Raw values for #{s.inspect} do not match"
        assert_equal '-', s.extractor_keys['Wafer'], 'wrong WaferID value in extractor key wafer'
        assert_equal '-', s.data_keys['MSlot'], 'wrong WaferID value in extractor key wafer'
        assert_equal '-', s.data_keys['PSequence'], 'wrong PSequence value in extractor key wafer'
        assert_equal '-', s.extractor_keys['PChamber'], 'wrong ChamberID value in extractor key PChamber'
        assert_equal dcrp.context[:Equipment]['id'], s.extractor_keys['PTool'], 'wrong ToolID value in extractor key PTool'
      }
    }
  end
  
  def test0229_parameter_extension_lot_single_chamber_generic_key_1_parameter
    assert $spctest.sil_upload_verify_generickey(@@file_generic_keys_dc_lot_grouping), 'file upload failed'
    chambers = {'CHA'=>5}
    sample_count = 1
    wafer_count = 3
    values_count = 13
    #
    # build and submit DCR
    wafer_values, wvalues, wafer_stats = _generate_wafer_values(wafer_count, sample_count)
    parameters = ['QA_PARAM_900_LOT']
    #
    # build and submit DCR for process PD
    dcrp = Space::DCR.new(eqp: @@eqp, lot: @@lot, op: @@ppd_qa, chambers: chambers, eqptype: 'PROCESS-CHAMBER')
    dcrp.add_parameters(parameters, wafer_values)
    wafer_count.times {|i| 
      dcrp.add_parameter("QA_COATER_#{i}", {"2502J49#{i}LE"=>"QA_CoaterUnit_#{i}"}, nocharting: true, value_type: 'string')
      dcrp.add_parameter("QA_WAFER_#{i}", {"2502J49#{i}LE"=>"WAFER:2502J49#{i}LE"}, nocharting: true, value_type: 'string')
    }
    dcrp.add_parameter('QA_LOT_PARAMETER', {@@lot=>'QA_LotValue'}, reading_type: 'Lot', nocharting: true, value_type: 'string')
    assert $spctest.send_dcr_verify(dcrp, nsamples: parameters.size), 'DCR not submitted successfully'
    #
    folder = $lds.folder(@@autocreated_qa)
    parameters.each {|p|
      ch = folder.spc_channel(parameter: p)
      # ensure spec and sample limits are set
      assert_equal sample_count, ch.samples.count, 'wrong sample count'
      ch.samples.each_with_index {|s, i|
        assert $spctest.verify_data(s.statistics, wafer_stats[i]), "Statistics for #{s.inspect} do not match"
        assert verify_sample_raw_values(s, wvalues[i], false), "Raw values for #{s.inspect} do not match"
        assert_equal '-', s.extractor_keys['Wafer'], 'wrong WaferID value in extractor key wafer'
        assert_equal '-', s.data_keys['MSlot'], 'wrong WaferID value in extractor key wafer'
        assert_equal '-', s.data_keys['PSequence'], 'wrong PSequence value in extractor key wafer'
        assert_equal chambers.keys[i], s.extractor_keys['PChamber'], 'wrong ChamberID value in extractor key PChamber'
        assert_equal dcrp.context[:Equipment]['id'], s.extractor_keys['PTool'], 'wrong ToolID value in extractor key PTool'
      }
    }
  end

  #returns all wafer values and per sample mapping of values and wafers
  def _generate_wafer_values(wafer_count, sample_count, value_count=1)
    t = Time.now.to_i
    gen_val = wafer_count.times.collect do |i| 
      if value_count == 1
        values = 100 + i 
      else
        values = value_count.times.collect {|j| 100+i + 1.0*j/value_count}
      end
      #['2502J49%02dLE' %i, values]
      ["RW#{t}%02dLE" % i, values]
    end
    wafer_values = Hash[*[gen_val]]
    $log.info "using wafer values #{wafer_values.inspect}"
    wvalues = sample_count.times.collect { {} }
    wafer_values.each_with_index {|e, i| 
      k,v = e
      ri = i % sample_count
      wvalues[ri][k] = [] unless wvalues[ri].has_key?(k)
      if v.is_a?(Array)
        wvalues[ri][k] += v 
      else
        wvalues[ri][k] << v
      end
    }    
    wafer_stats = wvalues.collect do |wvalue|
      wv = wvalue.values.flatten
      Space::SpcApi::SpcSampleStats.new(wv.mean, wv.median, wv.min, wv.max, wv.range, wv.standard_deviation, wv.size)
    end
    return [wafer_values, wvalues, wafer_stats]
  end

  # return true if wafer values match (checks also site data if needed) (used in Test_Space02 only)
  def verify_sample_raw_values(sample, wafer_values, sites=false)
    ret = true
    raws = sample.raws
    wafer_values.each_pair do |wafer, value|
      wafer_value = value
      wafer_value = [wafer_value] unless wafer_value.is_a?(Array)      
      if sites        
        wafer_value.each_with_index do |wv,i|
          rval = raws.find {|r| r.data_keys['SiteID'] == "#{wafer}_#{i+1}"}.value          
          ($log.warn "  wrong data for #{wafer}/site#{i+1}: #{rval}"; ret = false) unless $spctest._compare(rval, wv)
        end
      else
        wafer_raws = raws.find_all {|r| r.data_keys['SiteID'] == wafer}
        rvals = wafer_raws.collect {|r| r.value}
        _wv = wafer_value.sort
        _rv = rvals.sort
        _wv.each_with_index do |wv,i|
          ($log.warn "  wrong data for #{wafer}: #{_rv[i]}"; ret = false) unless $spctest._compare(_rv[i], wv)
        end
      end
    end
    return ret
  end
  
end
