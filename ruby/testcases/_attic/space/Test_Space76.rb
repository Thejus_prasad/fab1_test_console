=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: C.Stenzel, D. Steger, extracted from 07 tests
=end

require_relative 'Test_Space_Setup'


# Test Space/SIL, sometimes unstable
class Test_Space76 < Test_Space_Setup
  @@mpd = 'QA-MB-MSR663212M.01'
  @@ptools = ['UTCSIL01_0', 'UTCSIL01_1', 'UTCSIL01_2', 'UTCSIL01_3', 'UTCSIL01_4']
  @@valid_chambers = {'PM4'=>8, 'PM5'=>9}
  @@chambers = {'PM1'=>5, 'PM2'=>6, 'PM3'=>7, 'PM4'=>8, 'PM5'=>9}
  @@recipe = nil    # assigned later
  @@recipe_m = nil  # assigned later

  
  def setup
    super
    return unless $lds && self.class.class_variable_defined?(:@@lot) # bypass test0000_setup
    ([@@eqp] + @@ptools).each {|eqp| assert_equal 0, $sv.eqp_cleanup(eqp)}
    assert_equal 0, $sv.lot_cleanup(@@lot)
    assert_equal 0, $sv.inhibit_cancel(:recipe, @@recipe)
    assert_equal 0, $sv.inhibit_cancel(:recipe, @@recipe_m)
    #
    $lds.all_spc_channels('QA_PARAM_90*').each {|ch| assert ch.delete}
    folder = $lds.folder(@@autocreated_qa)
    assert folder.delete_channels, 'error cleaning channels' if folder
  end

  def teardown
    $sv.inhibit_cancel(:recipe, @@recipe)
    $sv.inhibit_cancel(:recipe, @@recipe_m)
  end


  def test7600_setup
    $setup_ok = false
    #
    ['AutoEquipmentInhibit-MSR663212-6', 'AutoEquipmentInhibit-MSR663212-7', 'AutoChamberInhibit-MSR663212-1', 'AutoChamberInhibit-MSR663212-2',
     'AutoChamberInhibit-MSR663212-3', 'AutoMachineRecipeInhibit-MSR663212-8', 'AutoMachineRecipeInhibit-MSR663212-9', 'AutoEquipmentAndRecipeInhibit-M663212-10',
     'AutoEquipmentAndRecipeInhibit-M663212-11', 'AutoEquipmentAndRecipeInhibit-M663212-12', 'AutoEquipmentAndRecipeInhibit-M663212-13', 'AutoChamberAndRecipeInhibit-MSR663212-14',
     'AutoChamberAndRecipeInhibit-MSR663212-15', 'AutoChamberAndRecipeInhibit-MSR663212-16', 'AutoEquipmentInhibit-M663212-16',
     'ReleaseEquipmentInhibit', 'ReleaseChamberInhibit', 'ReleaseMachineRecipeInhibit',
     'ReleaseEquipmentAndRecipeInhibit', 'ReleaseChamberAndRecipeInhibit'
    ].each {|action|
      assert $spc.corrective_action(action), "CA #{action.inspect} not defined"
    }
    #
    file_generic_keys_dc_MSR663212 = 'etc/testcasedata/space/GenericKeySetup_QA_DC_MSR663212.SpecID.xlsx'
    assert $spctest.sil_upload_verify_generickey(file_generic_keys_dc_MSR663212), 'gk file upload failed'
    file_chamber_filter_MSR663212 = 'etc/testcasedata/space/ChamberFilter_QA_MSR663212.ChamberFilter.xlsx'
    assert $spctest.sil_upload_verify_chamberfilter(file_chamber_filter_MSR663212), 'chamberfilter file upload failed'
    #
    recipes = $sv.machine_recipes
    assert @@recipe = recipes[2]
    assert @@recipe_m = recipes[3]
    #
    $setup_ok = true
  end
  
  # Tests for MSR663212 - usage of different ldskeys
  
  # Equipment inhibit for ExKey04 which is PTool in Fab1
  def test7601_equipment_inhibit_ldskey_ptool
    channels, ptool, pchamber, rest = _submit_dcrs(actions: 'AutoEquipmentInhibit-MSR663212-6')
    #
    assert $spctest.verify_inhibit(ptool, channels.size), 'equipment must have an inhibit'
    assert $spctest.verify_ecomment(channels, "TOOL_HELD: #{ptool}: reason={X-S"), 'wrong ecomment'
    # release inhibits
    channels.each {|ch|
      s = ch.samples(ckc: true).last
      assert s.assign_ca($spc.corrective_action('ReleaseEquipmentInhibit')), 'error assigning corrective action'
      sleep 2   # allow for SIL execution time
    }
    assert $spctest.verify_inhibit(ptool, 0), 'equipment must not have an inhibit'
  end

  # No Equipment inhibit for ExKey04 which is PTool in Fab1, because LDSKey attribute is combined with attribute ResponsiblePD in the CA
  def test7602_no_equipment_inhibit_ldskey_ptool_and_responsiblepd_attribute
    channels, ptool, pchamber, rest = _submit_dcrs(actions: 'AutoEquipmentInhibit-MSR663212-7')
    #
    assert $spctest.verify_inhibit(ptool, 0), 'equipment must have no inhibit'
    assert $spctest.verify_notification('Execution of [', 'LDSKey attributes combined', msgcount: 60), 'notifications not as expected'
  end

  # No Equipment inhibit for ExKey04 which is PTool in Fab1 - PTool is invalid
  def test7603_no_equipment_inhibit_ldskey_ptool
    action = 'AutoEquipmentInhibit-MSR663212-6'
    channels, ptool, pchamber, rest = _submit_dcrs(actions: action, ptools: ['UTCSIL01_A', 'UTCSIL01_B', 'UTCSIL01_C', 'UTCSIL01_D', 'UTCSIL01_E'])
    #
    assert $spctest.verify_inhibit(ptool, 0), 'equipment must have no inhibit'
    assert $spctest.verify_ecomment(channels, "Execution of [#{action}] failed"), 'wrong ecomment'
    assert $spctest.verify_futurehold_cancel(@@lot, nil), 'lot must have no future hold'
  end
  
  # EquipmentChamber inhibit for ExKey04 which is PTool in Fab1 and ExKey05 which is PChamber in Fab1
  def test7604_chamber_inhibit_MSR663212_ldskey_ptool_and_pchamber
    channels, ptool, pchamber, rest = _submit_dcrs(actions: 'AutoChamberInhibit-MSR663212-1')
    #
    assert_equal @@chambers.keys.sort.join(':'), pchamber, 'wrong PChamber'
    assert $spctest.verify_inhibit(ptool, channels.size*@@chambers.size, class: :eqp_chamber), 'equipment+chamber must have an inhibit'
    assert $spctest.verify_ecomment(channels, "TOOL+CHAMBER_HELD: #{ptool}/PM"), 'wrong ecomment'
    # release inhibits
    channels.each {|ch|
      s = ch.samples(ckc: true).last
      assert s.assign_ca($spc.corrective_action('ReleaseChamberInhibit')), 'error assigning corrective action'
      sleep 2   # allow for SIL execution time
    }
    assert $spctest.verify_inhibit(ptool, 0, class: :eqp_chamber), 'equipment+chamber must have no inhibit'
  end
  
  # EquipmentChamber inhibit for ExKey04 which is PTool in Fab1 and ExKey05 which is PChamber in Fab1
  def test7605_chamber_inhibit_MSR663212_ldskey16_ptoolpchamber
    channels, ptool, pchamber, rest = _submit_dcrs(actions: 'AutoChamberInhibit-MSR663212-2')
    #
    s = channels.last.samples(ckc: true).last
    assert_equal "#{ptool}-#{@@chambers.keys.sort.join(':')}", s.extractor_keys['Reserve4'], 'wrong PChamber'
    assert $spctest.verify_inhibit(ptool, channels.size*@@chambers.size, class: :eqp_chamber), 'equipment+chamber must have an inhibit'
    assert $spctest.verify_ecomment(channels, "TOOL+CHAMBER_HELD: #{ptool}/#{@@chambers.keys[0]}: reason={X-S"), 'wrong ecomment'
    # release inhibit
    channels.each {|ch|
      s = ch.samples(ckc: true).last
      assert s.assign_ca($spc.corrective_action('ReleaseChamberInhibit')), 'error assigning corrective action'
      sleep 2   # allow for SIL execution time
    }
    assert $spctest.verify_inhibit(ptool, 0, class: :eqp_chamber), 'equipment+chamber must not have an inhibit'
  end
  
  # No EquipmentChamber inhibit for ExKey04 which is PTool in Fab1 and ExKey05 which is PChamber in Fab1, because LDSKey attribute is combined with attribute ResponsiblePD in the CA
  def test7606_no_chamber_inhibit_MSR663212_ldskey16_ptoolpchamber
    channels, ptool, pchamber, rest = _submit_dcrs(actions: 'AutoChamberInhibit-MSR663212-3')
    #
    s = channels.last.samples(ckc: true).last
    assert_equal "#{ptool}-#{@@chambers.keys.sort.join(':')}", s.extractor_keys['Reserve4'], 'wrong PChamber'
    assert $spctest.verify_inhibit(ptool, 0, class: :eqp_chamber), 'equipment+chamber must not have an inhibit'
    assert $spctest.verify_notification('Execution of [', 'LDSKey attributes combined', msgcount: 60), 'notifications not as expected'
  end
  
  # No EquipmentChamber inhibit for ExKey04 which is PTool in Fab1 and ExKey05 which is PChamber in Fab1, because of invalid chambers
  def test7607_no_chamber_inhibit_MSR663212_ldskey16_ptoolpchamber
    action = 'AutoChamberInhibit-MSR663212-2'
    chambers = {'INV1'=>5, 'INV2'=>6, 'INV3'=>7, 'PM4'=>8, 'PM5'=>9}  # invalid and valid chambers mixed
    channels, ptool, pchamber, rest = _submit_dcrs(actions: action, chambers: chambers)
    #
    s = channels.last.samples(ckc: true).last
    assert_equal "#{ptool}-#{chambers.keys.sort.join(':')}", s.extractor_keys['Reserve4'], 'wrong PChamber'
    assert $spctest.verify_inhibit(ptool, channels.size*@@valid_chambers.size, class: :eqp_chamber), 'equipment+chamber must have an inhibit'
    assert $spctest.verify_ecomment(channels, "TOOL+CHAMBER_HELD: #{ptool}/#{@@valid_chambers.keys[0]}: reason={X-S"), 'wrong ecomment'
    assert $spctest.verify_ecomment(channels, "Execution of [#{action}] failed"), 'wrong ecomment'
    assert $spctest.verify_inhibit(ptool, 1, class: :eqp), 'equipment must have an inhibit (Fallback CA)'
    # release inhibit
    channels.each {|ch|
      s = ch.samples(ckc: true).last
      assert s.assign_ca($spc.corrective_action('ReleaseChamberInhibit')), 'error assigning corrective action'
      assert s.assign_ca($spc.corrective_action('ReleaseEquipmentInhibit')), 'error assigning corrective action'
      sleep 2   # allow for SIL execution time
    }
    assert $spctest.verify_inhibit(ptool, 0, class: :eqp_chamber), 'equipment+chamber must not have an inhibit'
    assert $spctest.verify_inhibit(ptool, 0, class: :eqp), 'equipment+chamber must not have an inhibit'
  end
  
  # No EquipmentChamber and Equipment inhibit for ExKey04 which is PTool in Fab1 and ExKey05 which is PChamber in Fab1, because of invalid chambers and invalid ptool
  def test7608_no_chamber_inhibit_MSR663212_ldskey16_ptoolpchamber
#    ptools = ['UTCSIL01_A']
    action = 'AutoChamberInhibit-MSR663212-2'
    chambers = {'INV1'=>5, 'INV2'=>6, 'INV3'=>7}  # invalid chambers 
    channels, ptool, pchamber, rest = _submit_dcrs(actions: action, ptools: ['UTCSIL01_A'], chambers: chambers, nooc: nil)
    #
    s = channels.last.samples(ckc: true).last
    assert_equal "#{ptool}-#{chambers.keys.sort.join(':')}", s.extractor_keys['Reserve4'], 'wrong PChamber'
    assert $spctest.verify_inhibit(ptool, 0, class: :eqp_chamber), 'equipment+chamber must have no inhibit'
    assert $spctest.verify_inhibit(ptool, 0, class: :eqp), 'equipment must have no inhibit'
    assert $spctest.verify_ecomment(channels, "Execution of [#{action}] failed"), 'wrong ecomment'
    assert $spctest.verify_ecomment(channels, "Equipment #{ptool} Information has not been found."), 'wrong ecomment'
    assert $spctest.verify_futurehold_cancel(@@lot, nil), 'lot must have no future hold'
  end

  # Machine Recipe inhibit for Reticle which is measurement recipe in Fab1
  def test7609_machinerecipe_inhibit_ldskey_reticle
    channels, ptool, pchamber, recipe, recipe_m = _submit_dcrs(actions: 'AutoMachineRecipeInhibit-MSR663212-8')
    #
    s = channels.last.samples(ckc: true).last
    assert_equal recipe, s.data_keys['DatReserve4'], 'wrong Recipe in DatReserve4'
    assert_equal recipe_m, s.data_keys['DatReserve5'], 'wrong Recipe in DatReserve5'
    assert_equal recipe_m, s.data_keys['Reticle'], 'wrong Recipe in Reticle'
    assert $spctest.verify_inhibit(recipe_m, channels.size, class: :recipe), 'measurement recipe must have an inhibit'
    assert $spctest.verify_ecomment(channels, "RECIPE_HELD: #{recipe_m}: reason={X-S"), 'wrong ecomment'
    # release inhibits
    channels.each {|ch|
      s = ch.samples(ckc: true).last
      assert s.assign_ca($spc.corrective_action('ReleaseMachineRecipeInhibit')), 'error assigning corrective action'
      sleep 2   # allow for SIL execution time
    }
    assert $spctest.verify_inhibit(recipe, 0, class: :recipe), 'machine recipe must not have an inhibit'
    assert $spctest.verify_inhibit(recipe_m, 0, class: :recipe), 'measurement recipe must not have an inhibit'
  end

  # No Machine Recipe inhibit for Reticle which is measurement recipe in Fab1 because of CA attribute ResponsiblePD=1
  def test7610_machinerecipe_inhibit_ldskey_reticle
    channels, ptool, pchamber, recipe, recipe_m = _submit_dcrs(actions: 'AutoMachineRecipeInhibit-MSR663212-9')
    #
    s = channels.last.samples(ckc: true).last
    assert_equal recipe, s.data_keys['DatReserve4'], 'wrong Recipe in DatReserve4'
    assert_equal recipe_m, s.data_keys['DatReserve5'], 'wrong Recipe in DatReserve5'
    assert_equal recipe_m, s.data_keys['Reticle'], 'wrong Recipe in Reticle'
    assert $spctest.verify_inhibit(recipe_m, 0, class: :recipe), 'measurement recipe must not have an inhibit'
    assert $spctest.verify_notification('Execution of [', 'LDSKey attributes combined', msgcount: 60), 'notifications not as expected'
  end

  # No Machine Recipe inhibit for Reticle which is measurment recipe in Fab1 because of invalid recipe
  def test7611_machinerecipe_inhibit_ldskey_reticle
    action = 'AutoMachineRecipeInhibit-MSR663212-8'
    channels, ptool, pchamber, recipe, recipe_m = _submit_dcrs(actions: action, recipe_m: 'notexisting')
    #
    s = channels.last.samples(ckc: true).last
    assert_equal recipe, s.data_keys['DatReserve4'], 'wrong Recipe in DatReserve4'
    assert_equal recipe_m, s.data_keys['DatReserve5'], 'wrong Recipe in DatReserve5'
    assert_equal recipe_m, s.data_keys['Reticle'], 'wrong Recipe in Reticle'
    assert $spctest.verify_inhibit(recipe_m, 0, class: :recipe), 'measurement recipe must not have an inhibit'
    assert $spctest.verify_ecomment(channels, "Execution of [#{action}] failed"), 'wrong ecomment'
  end

  # Equipment & Machine Recipe inhibit for ExKey04 which is PTool in Fab1 and DaKey08 which is Reticle in Fab1
  def test7612_1_equipment_and_recipe_inhibit_ldskey_ptool_reticle
    channels, ptool, pchamber, recipe, recipe_m = _submit_dcrs(actions: 'AutoEquipmentAndRecipeInhibit-M663212-10')
    #
    s = channels.last.samples(ckc: true).last
    assert_equal recipe, s.data_keys['DatReserve4'], 'wrong Recipe in DatReserve4'
    assert_equal recipe_m, s.data_keys['DatReserve5'], 'wrong Recipe in DatReserve5'
    assert_equal recipe_m, s.data_keys['Reticle'], 'wrong Recipe in Reticle'
    assert $spctest.verify_inhibit([ptool,recipe_m], channels.size, class: :eqp_recipe), 'equipment and measurement recipe must have an inhibit'
    assert $spctest.verify_ecomment(channels, 'TOOL+RECIPE_HELD: reason={X-S'), 'wrong ecomment'
    assert $spctest.verify_ecomment(channels, ptool), 'wrong ecomment'
    assert $spctest.verify_ecomment(channels, recipe_m), 'wrong ecomment'
    # release inhibits
    channels.each {|ch|
      s = ch.samples(ckc: true).last
      assert s.assign_ca($spc.corrective_action('ReleaseEquipmentAndRecipeInhibit')), 'error assigning corrective action'
      sleep 2   # allow for SIL execution time
    }
    assert $spctest.verify_inhibit([ptool,recipe], 0, class: :eqp_recipe), 'equipment and machine recipe must not have an inhibit'
    assert $spctest.verify_inhibit([ptool,recipe_m], 0, class: :eqp_recipe), 'equipment and measurement recipe must not have an inhibit'
  end

  # Equipment & Machine Recipe inhibit for DaKey02 which is MTool in Fab1 and DaKey08 which is Reticle in Fab1
  def test7612_2_equipment_and_recipe_inhibit_ldskey_mtool_reticle
    channels, ptool, pchamber, recipe, recipe_m = _submit_dcrs(actions: 'AutoEquipmentAndRecipeInhibit-M663212-11')
    #
    s = channels.last.samples(ckc: true).last
    assert_equal recipe, s.data_keys['DatReserve4'], 'wrong Recipe in DatReserve4'
    assert_equal recipe_m, s.data_keys['DatReserve5'], 'wrong Recipe in DatReserve5'
    assert_equal recipe_m, s.data_keys['Reticle'], 'wrong Recipe in Reticle'
    assert_equal @@eqp, s.data_keys['MTool'], 'wrong MTool'
    assert $spctest.verify_inhibit([@@eqp, recipe_m], channels.size, class: :eqp_recipe), 'equipment and measurement recipe must have an inhibit'
    assert $spctest.verify_ecomment(channels, 'TOOL+RECIPE_HELD: reason={X-S'), 'wrong ecomment'
    assert $spctest.verify_ecomment(channels, @@eqp), 'wrong ecomment'
    assert $spctest.verify_ecomment(channels, recipe_m), 'wrong ecomment'
    # release inhibits
    channels.each {|ch|
      s = ch.samples(ckc: true).last
      assert s.assign_ca($spc.corrective_action('ReleaseEquipmentAndRecipeInhibit')), 'error assigning corrective action'
      sleep 2   # allow for SIL execution time
    }
    assert $spctest.verify_inhibit([@@eqp, recipe], 0, class: :eqp_recipe), 'equipment and machine recipe must not have an inhibit'
    assert $spctest.verify_inhibit([@@eqp, recipe_m], 0, class: :eqp_recipe), 'equipment and measurement recipe must not have an inhibit'
  end

  # Equipment & Machine Recipe inhibit for ExKey04 which is PTool in Fab1 and DaKey16 which is PRecipe
  def test7612_3_equipment_and_recipe_inhibit_ldskey_ptool_precipe
    channels, ptool, pchamber, recipe, recipe_m = _submit_dcrs(actions: 'AutoEquipmentAndRecipeInhibit-M663212-12')
    #
    s = channels.last.samples(ckc: true).last
    assert_equal recipe, s.data_keys['DatReserve4'], 'wrong Recipe in DatReserve4'
    assert_equal recipe_m, s.data_keys['DatReserve5'], 'wrong Recipe in DatReserve5'
    assert_equal recipe_m, s.data_keys['Reticle'], 'wrong Recipe in Reticle'
    assert $spctest.verify_inhibit([ptool, recipe], channels.size, class: :eqp_recipe), 'equipment and machine recipe must have an inhibit'
    assert $spctest.verify_ecomment(channels, 'TOOL+RECIPE_HELD: reason={X-S'), 'wrong ecomment'
    assert $spctest.verify_ecomment(channels, ptool), 'wrong ecomment'
    assert $spctest.verify_ecomment(channels, recipe), 'wrong ecomment'
    $log.info 'EquipmentAndMachineRecipeInhibit automatically set'  
    # release inhibits
    channels.each {|ch|
      s = ch.samples(ckc: true).last
      assert s.assign_ca($spc.corrective_action('ReleaseEquipmentAndRecipeInhibit')), 'error assigning corrective action'
      sleep 2   # allow for SIL execution time
    }
    assert $spctest.verify_inhibit([ptool, recipe], 0, class: :eqp_recipe), 'equipment and machine recipe must not have an inhibit'
    assert $spctest.verify_inhibit([ptool, recipe_m], 0, class: :eqp_recipe), 'equipment and measurement recipe must not have an inhibit'
  end

  # No Equipment & Machine Recipe inhibit for DaKey02 which is MTool in Fab1 and DaKey08 which is Reticle in Fab1 because of attribute ResponsiblePD=1
  def test7613_no_equipment_and_recipe_inhibit_ldskey_mtool_reticle_and_responsiblepd
    channels, ptool, pchamber, recipe, recipe_m = _submit_dcrs(actions: 'AutoEquipmentAndRecipeInhibit-M663212-13')
    #
    s = channels.last.samples(ckc: true).last
    assert_equal recipe, s.data_keys['DatReserve4'], 'wrong Recipe in DatReserve4'
    assert_equal recipe_m, s.data_keys['DatReserve5'], 'wrong Recipe in DatReserve5'
    assert_equal recipe_m, s.data_keys['Reticle'], 'wrong Recipe in Reticle'
    assert_equal @@eqp, s.data_keys['MTool'], 'wrong MTool'
    assert $spctest.verify_inhibit([@@eqp, recipe_m], 0, class: :eqp_recipe), 'equipment and measurement recipe must not have an inhibit'
    assert $spctest.verify_notification('Execution of [', 'LDSKey attributes combined', msgcount: 60), 'notifications not as expected'
  end

  # No Equipment & Machine Recipe inhibit for DaKey02 which is MTool in Fab1 and DaKey08 which is Reticle in Fab1 because of invalid recipe
  def test7614_no_equipment_and_recipe_inhibit_ldskey_mtool_reticle_and_invalid_recipe
    action = 'AutoEquipmentAndRecipeInhibit-M663212-11'
    channels, ptool, pchamber, recipe, recipe_m = _submit_dcrs(actions: action, recipe_m: 'notexisting')
    #
    s = channels.last.samples(ckc: true).last
    assert_equal recipe, s.data_keys['DatReserve4'], 'wrong Recipe in DatReserve4'
    assert_equal recipe_m, s.data_keys['DatReserve5'], 'wrong Recipe in DatReserve5'
    assert_equal recipe_m, s.data_keys['Reticle'], 'wrong Recipe in Reticle'
    assert_equal @@eqp, s.data_keys['MTool'], 'wrong MTool'
    assert $spctest.verify_inhibit([@@eqp, recipe_m], 0, class: :eqp_recipe), 'equipment and measurement recipe must not have an inhibit'
    assert $spctest.verify_ecomment(channels, "Execution of [#{action}] failed"), 'wrong ecomment'
  end

  # Chamber & Machine Recipe inhibit for ExKey04 which is PTool in Fab1, ExKey05 which is PChamber and DaKey08 which is Reticle in Fab1
  def test7615_chamber_and_recipe_inhibit_ldskey_ptool_pchamber_and_reticle
    channels, ptool, pchamber, recipe, recipe_m = _submit_dcrs(actions: 'AutoChamberAndRecipeInhibit-MSR663212-14')
    #
    s = channels.last.samples(ckc: true).last
    assert_equal recipe, s.data_keys['DatReserve4'], 'wrong Recipe in DatReserve4'
    assert_equal recipe_m, s.data_keys['DatReserve5'], 'wrong Recipe in DatReserve5'
    assert_equal recipe_m, s.data_keys['Reticle'], 'wrong Recipe in Reticle'
    assert_equal pchamber, @@chambers.keys.sort.join(':'), 'wrong PChamber'
    @@chambers.keys.each {|ch|
      assert $spctest.verify_inhibit([ptool, recipe_m], channels.size, class: :eqp_chamber_recipe, attrib: [ch]), 'chamber and measurement recipe must have an inhibit'
      assert $spctest.verify_ecomment(channels, "TOOL+CHAMBER+RECIPE_HELD: #{ptool}/#{ch}/#{recipe_m}: reason={X-S"), 'wrong ecomment'
    }
    # release inhibits
    channels.each {|ch|
      s = ch.samples(ckc: true).last
      assert s.assign_ca($spc.corrective_action('ReleaseChamberAndRecipeInhibit')), 'error assigning corrective action'
      sleep 2   # allow for SIL execution time
    }
    assert $spctest.verify_inhibit([pchamber, recipe], 0, class: :eqp_chamber_recipe), 'chamber and machine recipe must not have an inhibit'
    assert $spctest.verify_inhibit([pchamber, recipe_m], 0, class: :eqp_chamber_recipe), 'chamber and measurement recipe must not have an inhibit'
  end

  # Chamber & Machine Recipe inhibit for ExKey16 which is PToolChamber in Fab1 and DaKey08 which is Reticle in Fab1
  def test7616_chamber_and_recipe_inhibit_ldskey_ptoolchamber_and_reticle
    channels, ptool, pchamber, recipe, recipe_m = _submit_dcrs(actions: 'AutoChamberAndRecipeInhibit-MSR663212-15')
    #
    s = channels.last.samples(ckc: true).last
    assert_equal recipe, s.data_keys['DatReserve4'], 'wrong Recipe in DatReserve4'
    assert_equal recipe_m, s.data_keys['DatReserve5'], 'wrong Recipe in DatReserve5'
    assert_equal recipe_m, s.data_keys['Reticle'], 'wrong Recipe in Reticle'
    assert_equal "#{ptool}-#{@@chambers.keys.sort.join(':')}", s.extractor_keys['Reserve4'], 'wrong PChamber'
    assert $spctest.verify_inhibit([ptool, recipe_m], channels.size*@@chambers.size, class: :eqp_chamber_recipe), 'chamber and measurement recipe must have an inhibit'
    assert $spctest.verify_ecomment(channels, 'TOOL+CHAMBER+RECIPE_HELD: '), 'wrong ecomment'
    # release inhibits
    channels.each {|ch|
      s = ch.samples(ckc: true).last
      assert s.assign_ca($spc.corrective_action('ReleaseChamberAndRecipeInhibit')), 'error assigning corrective action'
      sleep 2   # allow for SIL execution time
    }
    assert $spctest.verify_inhibit([ptool, recipe], 0, class: :eqp_chamber_recipe), 'chamber and machine recipe must not have an inhibit'
    assert $spctest.verify_inhibit([ptool, recipe_m], 0, class: :eqp_chamber_recipe), 'chamber and measurement recipe must not have an inhibit'
  end

  # No Chamber & Machine Recipe inhibit for ExKey16 which is PToolChamber in Fab1 and DaKey08 which is Reticle in Fab1 because of attribute ResponsiblePD=1
  def test7617_no_chamber_and_recipe_inhibit_ldskey_ptoolchamber_and_reticle_and_responsiblepd
    channels, ptool, pchamber, recipe, recipe_m = _submit_dcrs(actions: 'AutoChamberAndRecipeInhibit-MSR663212-16')
    #
    s = channels.last.samples(ckc: true).last
    assert_equal recipe, s.data_keys['DatReserve4'], 'wrong Recipe in DatReserve4'
    assert_equal recipe_m, s.data_keys['DatReserve5'], 'wrong Recipe in DatReserve5'
    assert_equal recipe_m, s.data_keys['Reticle'], 'wrong Recipe in Reticle'
    assert_equal "#{ptool}-#{@@chambers.keys.sort.join(':')}", s.extractor_keys['Reserve4'], 'wrong PChamber'
    assert $spctest.verify_inhibit([ptool, recipe_m], 0, class: :eqp_chamber_recipe), 'chamber and measurement recipe must not have an inhibit'
    assert $spctest.verify_notification('Execution of [', 'LDSKey attributes combined', msgcount: 60), 'notifications not as expected'
  end

  # No Chamber & Machine Recipe inhibit for ExKey16 which is PToolChamber in Fab1 and DaKey08 which is Reticle in Fab1 because of invalid recipe
  def test7618_no_chamber_and_recipe_inhibit_ldskey_ptoolchamber_and_reticle
    action = 'AutoChamberAndRecipeInhibit-MSR663212-15'
    channels, ptool, pchamber, recipe, recipe_m = _submit_dcrs(actions: action, recipe_m: 'notexisting')
    #
    s = channels.last.samples(ckc: true).last
    assert_equal recipe, s.data_keys['DatReserve4'], 'wrong Recipe in DatReserve4'
    assert_equal recipe_m, s.data_keys['DatReserve5'], 'wrong Recipe in DatReserve5'
    assert_equal recipe_m, s.data_keys['Reticle'], 'wrong Recipe in Reticle'
    assert_equal "#{ptool}-#{@@chambers.keys.sort.join(':')}", s.extractor_keys['Reserve4'], 'wrong PChamber'
    assert $spctest.verify_inhibit([ptool, recipe_m], 0, class: :eqp_chamber_recipe), 'chamber and measurement recipe must not have an inhibit'
    assert $spctest.verify_inhibit(ptool, @@chambers.size, class: :eqp_chamber), 'equipment+chamber must have an inhibit'
    assert $spctest.verify_ecomment(channels, "Execution of [#{action}] failed"), 'wrong ecomment'
    assert $spctest.verify_ecomment(channels, 'AutoChamberInhibit instead executed successfully'), 'wrong ecomment'
    # release inhibits
    channels.each {|ch|
      s = ch.samples(ckc: true).last
      assert s.assign_ca($spc.corrective_action('ReleaseChamberInhibit')), 'error assigning corrective action'
      sleep 2   # allow for SIL execution time
    }
    assert $spctest.verify_inhibit(ptool, 0, class: :eqp_chamber), 'equipment+chamber must not have an inhibit'
  end

  # No Chamber & Machine Recipe inhibit for ExKey16 which is PToolChamber in Fab1 and DaKey08 which is Reticle in Fab1 because of invalid chambers
  def test7619_no_chamber_and_recipe_inhibit_ldskey_ptoolchamber_and_reticle
    action = 'AutoChamberAndRecipeInhibit-MSR663212-15'
    chambers = {'INV1'=>5, 'INV2'=>6, 'INV3'=>7}
    channels, ptool, pchamber, recipe, recipe_m = _submit_dcrs(actions: action, chambers: chambers, nooc: nil)
    #
    s = channels.last.samples(ckc: true).last
    assert_equal recipe, s.data_keys['DatReserve4'], 'wrong Recipe in DatReserve4'
    assert_equal recipe_m, s.data_keys['DatReserve5'], 'wrong Recipe in DatReserve5'
    assert_equal recipe_m, s.data_keys['Reticle'], 'wrong Recipe in Reticle'
    assert_equal "#{ptool}-#{chambers.keys.sort.join(':')}", s.extractor_keys['Reserve4'], 'wrong PChamber'
    assert $spctest.verify_inhibit([ptool, recipe_m], 0, class: :eqp_chamber_recipe), 'chamber and measurement recipe must not have an inhibit'
    assert $spctest.verify_ecomment(channels, "Execution of [#{action}] failed"), 'wrong ecomment'
    assert $spctest.verify_ecomment(channels, "Error Occurred while performing inhibitChamber for: #{ptool}/"), "ecomment not as expected: Error Occurred while performing inhibitChamber for: #{ptool}/"
  end

  # Equipment inhibit for MTool which is MTool in Fab1
  def test7620_equipment_inhibit_ldskey_mtool
    channels, ptool, pchamber, recipe, recipe_m = _submit_dcrs(actions: 'AutoEquipmentInhibit-M663212-16')
    #
    s = channels.last.samples(ckc: true).last
    assert_equal @@eqp, s.data_keys['MTool'], 'wrong MTool'
    assert $spctest.verify_inhibit(ptool, 0, class: :eqp), 'equipment must not have an inhibit'
    assert $spctest.verify_inhibit(@@eqp, channels.size, class: :eqp), 'equipment must have an inhibit'
    assert $spctest.verify_ecomment(channels, "TOOL_HELD: #{@@eqp}: reason={X-S"), 'wrong ecomment'
    # release inhibits
    channels.each {|ch|
      s = ch.samples(ckc: true).last
      assert s.assign_ca($spc.corrective_action('ReleaseEquipmentInhibit')), 'error assigning corrective action'
      sleep 2   # allow for SIL execution time
    }
    assert $spctest.verify_inhibit(@@eqp, 0), 'equipment must not have an inhibit'
  end
  
  
  def _submit_dcrs(params={})
    caller = caller_locations(1,1)[0].label
    ptools = params[:ptools] || @@ptools
    mpd = params[:mpd] || @@mpd
    #
    channels = create_clean_channels
    assert $spctest.assign_verify_cas(channels, params[:actions])
    #
    # send process DCRs
    pdrefs = $sildb.pd_reference(mpd: @@mpd)
    assert_equal 5, pdrefs.size, 'not enough responsible pds found'
    pdrefs[2] = pdrefs[1].succ
    ptools.each_with_index {|eqp, i|
      submit_process_dcr(pdrefs[i], eqp: eqp, wafer_values: @@wafer_values2, chambers: params[:chambers] || @@chambers,
        machine_recipe: @@recipe, export: "log/#{caller}_#{i}_p.xml")
    }
    #
    # send DCR with OOC values
    dcr = Space::DCR.new(lot: @@lot, eqp: @@eqp, op: @@mpd, machine_recipe: params[:recipe_m] || @@recipe_m)
    dcr.add_parameters(@@parameters, @@wafer_values2ooc)
    res = $client.send_dcr(dcr, export: "log/#{caller}.xml")
    #
    nooc = params.has_key?(:nooc) ? params[:nooc] : 'all*2'   # no OOC check if all chambers are invalid
    assert $spctest.verify_dcr_accepted(res, @@parameters.size*@@wafer_values2ooc.size, nooc: nooc), 'DCR not submitted successfully'
    #
    s = channels.last.samples(ckc: true).last
    ptool = s.extractor_keys['PTool']
    assert_equal ptool, ptools[0], 'PTool not correct'
    pchamber = s.extractor_keys['PChamber']
    $log.info "PTool: #{ptool.inspect}, PChamber: #{pchamber.inspect}"
    return [channels, ptool, pchamber, @@recipe, params[:recipe_m] || @@recipe_m]
  end
  
end
