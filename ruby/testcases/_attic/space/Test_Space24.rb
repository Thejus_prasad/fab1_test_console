=begin 
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.
    
Author: Conny Stenzel, 2012-08-07
=end

require_relative 'Test_Space_Setup'


# Test Space/SIL Responsible PDs - ERF
class Test_Space24 < Test_Space_Setup
  @@messages = ["[(Mean above control limit)", "[(Mean below control limit)", "[(Raw above specification)", "[(Raw below specification)"]
  
  @@erfRouteOne = "e1234-QASIL-A0.01"                 #Standard Pattern: e*-*
  @@erfRouteTwo = "e1235A-QASIL-A1.01"                 #Standard Pattern: e*-*
  @@erfRouteThree = "e1236-QASIL-A1.01"                 #Standard Pattern: e*-*
  
  # set up in PDSetup.SpecHierarchy with 5 resp.PDs
  @@mpd = "QA-MB-FTDEPM.01"
  #NOT set up in PDSetup.SpecHierarchy
  @@mpdErfOne = "e1234-#{@@mpd.split(".")[0]}-A0.01"
  # set up in PDSetup.SpecHierarchy with 5 resp.erfPDs
  @@mpdErfTwo = "e1235A-#{@@mpd.split(".")[0]}-A1.01"
  # NOT set up in PDSetup.SpecHierarchy
  @@mpdErfThree = "e1236-#{@@mpd.split(".")[0]}-A1.01"
  
  @@files_generic_keys_dc = 1.times.collect{|i| "etc/testcasedata/space/GenericKeySetup_QA_DC_#{i}.SpecID.xlsx"}
  
  @@generic_res4vals_dc = ["Tool"]
  @@generic_res5vals_dc = ["Chamber"]
  @@generic_res6vals_dc = ["LogRecipe"]
  @@generic_res7vals_dc = ["dcparmorpath"]
  @@generic_res8vals_dc = ["dcparmorpathwithoutversion"]
    
  @@generic_ekeys_dc = {"Reserve4"=>@@generic_res4vals_dc, "Reserve5"=>@@generic_res5vals_dc, 
    "Reserve6"=>@@generic_res6vals_dc, "Reserve7"=>@@generic_res7vals_dc, "Reserve8"=>@@generic_res8vals_dc}
  @@generic_dkeys_dc = {"DatReserve4"=>@@generic_res4vals_dc, "DatReserve5"=>@@generic_res5vals_dc, 
    "DatReserve6"=>@@generic_res6vals_dc, "DatReserve7"=>@@generic_res7vals_dc, "DatReserve8"=>@@generic_res8vals_dc}


  def setup
    super
    return unless $lds && self.class.class_variable_defined?(:@@lot) # bypass test0000_setup
    assert_equal 0, $sv.eqp_cleanup(@@eqp)
    assert_equal 0, $sv.lot_cleanup(@@lot)
    #
    [$lds, $lds_setup].each {|lds|
      lds.all_spc_channels('QA_PARAM_90*').each {|ch| assert ch.delete}
      folder = lds.folder(@@autocreated_qa)
      assert folder.delete_channels, "error cleaning channels" if folder
    }
  end


  def test2400_setup
    $setup_ok = false
    #
    ## defaults per setup.FC
    assert $spctest.sil_upload_verify_generickey(@@files_generic_keys_dc[0]), "file upload failed"
    #
    @@ppds = $sildb.pd_reference(mpd: @@mpd)[0..4].compact
    assert_equal 5, @@ppds.size, "no PD reference found for #{@@mpd}"
    #
    $setup_ok = true
  end

  def test2410_MSR582790_erfroute_procpds_erfmeaspd
    # no process PD referenced by erfMPDOne
    assert_empty $sildb.pd_reference(mpd: @@mpdErfOne, all: true), "MPD #{@@mpdErfOne} does wrongly reference some process PDs"
    _send_erf_dcr(@@erfRouteOne, [@@mpdErfOne], @@ppds, @@generic_ekeys_dc, @@generic_dkeys_dc, true, true, false)
  end

  def test2411_MSR582790_erfroute_procpds_measpd
    # no process PD referenced by erfMPDOne
    assert_empty $sildb.pd_reference(mpd: @@mpdErfOne, all: true), "MPD #{@@mpdErfOne} does wrongly reference some process PDs"
    _send_erf_dcr(@@erfRouteOne, [@@mpd], @@ppds, @@generic_ekeys_dc, @@generic_dkeys_dc, true, true, false)
  end

  def test2412_MSR582790_erfroute_erfprocpds_measpd
    # no process PD referenced by erfMPDOne
    assert_empty $sildb.pd_reference(mpd: @@mpdErfOne, all: true), "MPD #{@@mpdErfOne} does wrongly reference some process PDs"
    ppds = ["e1234-QA-MB-FTDEP-A0.01","e1234-QA-MB-FTDEP-A0.02","e1234-QA-MB-FTDEP-A0.03","e1234-QA-MB-FTDEP-A0.04","e1234-QA-MB-FTDEP-A0.05"]
    $log.info "using process PDs #{ppds.inspect}"
    #
    _send_erf_dcr(@@erfRouteOne, [@@mpd], ppds, @@generic_ekeys_dc, @@generic_dkeys_dc, true, true, false)
  end

  def test2413_MSR582790_erfroute_erfprocpds_with_non_configured_origpd
    # no process PD referenced by erfMPDThree
    assert_empty $sildb.pd_reference(mpd: @@mpdErfThree, all: true), "MPD #{@@mpdErfThree} does wrongly reference some process PDs"
    ppds = ["e1236-QA-MB-FTDEP-A1.01","e1236-QA-MB-FTDEP-A1.02","e1236-QA-MB-FTDEP-A1.03","e1236-QA-MB-FTDEP-A1.04","e1236-QA-MB-FTDEP-A1.05"]
    $log.info "using process PDs #{ppds.inspect}"
    #
    _send_erf_dcr(@@erfRouteThree, [@@mpdErfThree], ppds, @@generic_ekeys_dc, @@generic_dkeys_dc, false, false, false)
  end

  def test2414_MSR582790_erfroute_overwrite1
    # get a process PD referenced by MPD
    ppds = ["e1235A-QA-MB-FTDEP-A1.01", "e1235A-QA-MB-FTDEP-A1.02", "QA-MB-FTDEP.01", "e1235A-QA-MB-FTDEP-A1.03"]
    $log.info "using process PDs #{ppds.inspect}"
    #
    _send_erf_dcr(@@erfRouteThree, [@@mpdErfTwo, "QA-ERF-FTDEPM.01"], ppds, @@generic_ekeys_dc, @@generic_dkeys_dc, false, true, true)
  end
  
  
  def _send_erf_dcr(route, mpds, ppds, generic_ekeys, generic_dkeys, ooc_expected, valid_pvalues, alternating)
    # build and submit DCRs for 5 process PDs
    dcrs = ppds.each_with_index.collect {|ppd, i|
      submit_process_dcr(ppd, eqp: "#{@@eqp}_#{i}", lot: @@lot,
        logical_recipe: "F-P-FTEOS8K15.0#{i}", machine_recipe: "F-CVD60X-P.F-P-FTEOS8K15.0#{i}",
        dcp_plan_version: "0#{i}", dcp_plan_name: "SEM-YM_#{i}", chambers: {"PM#{i+1}"=>5+i}, export: "log/#{__method__}_p_#{i}.xml")
    }
    #
    # build and submit measurement DCR with all parameters
    folder = $lds.folder(@@autocreated_qa)
    mpds.each_with_index {|mpd, i|
      folder.delete_channels if folder
      dcr = Space::DCR.new(eqp: "QAMeas", lot: @@lot, route: route, op: mpd)
      dcrs.unshift dcr
      wafer_values = Hash[*@@wafer_values.collect {|w, v| [w, v + 100]}.flatten]
      dcr.add_parameters(@@parameters, wafer_values)
      res = $client.send_dcr(dcr, export: "log/#{__method__}_m_#{i}.xml")
      nsamples = @@parameters.size * wafer_values.size
      if ooc_expected
        assert $spctest.verify_dcr_accepted(res, nsamples, nooc: nsamples*2), "DCR not submitted successfully"
        assert $spctest.verify_futurehold(@@lot, "No Message here", nil, expected_holds: @@parameters.size, messages: @@messages), "error setting future hold"
      else
        assert $spctest.verify_dcr_accepted(res, nsamples), "DCR not submitted successfully"
      end
      #
      # check for flags
      alternate_number = 0
      @@parameters.each {|p|
        if alternating
          if alternate_number == 0
            ppd = (i == 0) ? ppds[0] : ppds[2]
            pChamber = (i == 0) ? "PM1" : "PM3"
            pTool = (i == 0) ? "#{@@eqp}_0" : "#{@@eqp}_2"
            pLogRecipe = (i == 0) ? "F-P-FTEOS8K15.00" : "F-P-FTEOS8K15.02"
            alternate_number = 1
          else
            ppd = (i == 0) ? ppds[1] : ppds[3]
            pChamber = (i == 0) ? "PM2" : "PM4"
            pTool = (i == 0) ? "#{@@eqp}_1" : "#{@@eqp}_3"
            pLogRecipe = (i == 0) ? "F-P-FTEOS8K15.01" : "F-P-FTEOS8K15.03"
            alternate_number = 0
          end
        end
        ch = folder.spc_channel(parameter: p)
        samples = ch.samples
        samples.each {|s|
          assert $spctest.verify_data(s.extractor_keys, $spctest.get_sample_generickeys_dc(dcrs, generic_ekeys))
          assert $spctest.verify_data(s.data_keys, $spctest.get_sample_generickeys_dc(dcrs, generic_dkeys))
          # must match last PD and tool
          if alternating
            assert $spctest.verify_data(s.extractor_keys, {"POperationID"=>ppd, "PChamber"=>pChamber,
              "PTool"=>pTool, "PLogRecipe"=>pLogRecipe})
          elsif valid_pvalues
            assert $spctest.verify_data(s.extractor_keys, {"POperationID"=>ppds[0], "PChamber"=>"PM1",
              "PTool"=>"#{@@eqp}_0", "PLogRecipe"=>"F-P-FTEOS8K15.00"})
          else
            assert $spctest.verify_data(s.extractor_keys, {"POperationID"=>"-", "PChamber"=>"-", "PTool"=>"-", "PLogRecipe"=>"-"})
          end
        }
      }
    }
  end
end
