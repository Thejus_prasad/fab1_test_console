
# Test Space MRB test.

##require 'lib/capybara-selenium-gems.jar'
require 'RubyTestCase'
require 'capybara'
require 'capybara/dsl'
require 'selenium-webdriver'




class Test_MRB < RubyTestCase
  include Capybara::DSL

  def self.startup
    Capybara.default_driver = :selenium
    Capybara.app_host = 'http://fc8mrbwbq01.gfoundries.com'
    Capybara.run_server = false
  end
  
  def basic_auth(name, password)
    if page.driver.respond_to?(:basic_auth)
      page.driver.basic_auth(name, password)
    elsif page.driver.respond_to?(:basic_authorize)
      page.driver.basic_authorize(name, password)
    elsif page.driver.respond_to?(:browser) && page.driver.browser.respond_to?(:basic_authorize)
      page.driver.browser.basic_authorize(name, password)
    else
      raise "I don't know how to log in!"
    end
  end
  
  test "simple test" do
    basic_auth('gmstest01', 'Password5')
    visit("/WMRB/MRBForm.aspx?MRB_ID=200001853")
    assert_equal 200, page.status_code
  end
  
  def teardown
    Capybara.reset_sessions!
    Capybara.use_default_driver
  end

end