=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

=end

require_relative 'Test_Space_Setup'

# Test Space/SIL.
class Test_Space_Experimental < Test_Space_Setup
  @@mpd_qa = 'QA-MB-FTDEPM-LIS-CAEX.01'
  @@mpd_lit = 'LIT-MGT-NMETDEPDR.LIT'
	@@chamber_a = 'PM1'
	@@chamber_b = 'PM2'
	@@chamber_d = 'PM3'
	@@chamber_e = 'PM4'
	@@chamber_c = 'CHX'
  @@chambers = {@@chamber_a=>5, @@chamber_b=>6, @@chamber_c=>7}
  @@lit_used = false
  @@mtool = 'UTFSIL01'
  @@cafailure_lothold_fallback = true
  @@wafer_values2 = {'SIL007.00.01'=>40, 'SIL007.00.02'=>41, 'SIL007.00.03'=>42, 'SIL007.00.04'=>43, 'SIL007.00.05'=>44, 'SIL007.00.06'=>45}
  @@real_wafer_values = {}
  @@lotcount = 2
  

  def setup
    super
    return unless $lds && self.class.class_variable_defined?(:@@lot) # bypass test0000_setup
    assert_equal 0, $sv.eqp_cleanup(@@eqp)
    assert_equal 0, $sv.lot_cleanup(@@lot)
    #
    #$lds.all_spc_channels('QA_PARAM_90*').each {|ch| assert ch.delete}
    #folder = $lds.folder(@@autocreated_qa)
    #assert folder.delete_channels, 'error cleaning channels' if folder
  end


  def testex0000_setup
    $setup_ok = false
    
    if $env == 'f8stag'
      @@cafailure_lothold_fallback = false
      @@wafer_values2 = {'WWWWSIL00700'=>40, 'WWWWSIL00701'=>41, 'WWWWSIL00702'=>42, 'WWWWSIL00703'=>43, 'WWWWSIL00704'=>44, 'WWWWSIL00705'=>45}
    end
    
    @@seclot = 'SILSL' + @@sil_version.split('.').collect {|e| '%02d' % e}.join('') + '%02d' % @@test_run + '.000'
    assert @@seclot = $svtest.new_lot(lot: @@seclot) unless $sv.lot_exists?(@@seclot)

    assert @@ppd_qa = $sildb.pd_reference(:mpd=>@@mpd_qa).first, "no PD reference found for #{@@mpd_qa}"
    assert $sildb.pd_reference(:mpd=>@@mpd_lit, :all=>true).length >= 1, "No PD reference found for #{@@mpd_lit}!"

    ['LotHold', 'AutoLotHold', 'ReleaseLotHold', 'CancelLotHold',
      'EquipmentInhibit', 'AutoEquipmentInhibit', 'ReleaseEquipmentInhibit', 'ReleaseEquipmentInhibit-noDefault',
      'ChamberInhibit', 'AutoChamberInhibit', 'ReleaseChamberInhibit', 'ReleaseChamberInhibit-noDefault',
      'ReticleInhibit', 'AutoReticleInhibit', 'ReleaseReticleInhibit',
      'RouteInhibit', 'AutoRouteInhibit', 'ReleaseRouteInhibit',
      'MachineRecipeInhibit', 'AutoMachineRecipeInhibit', 'ReleaseMachineRecipeInhibit', 'ReleaseMachineRecipeInhibit-noDefault',
      'AutoSetWaferScriptParameter-OOC', 'AutoSetLotScriptParameter-OOC', 'AutoSetLotScriptParameter-OOCInteger', 'SetLotScriptParameter-OOC',
      'AutoUnknown', 'Unknown', 'CACollection-ReleaseAll',
      'AutoEquipmentAndRecipeInhibit-Single', 'AutoEquipmentAndRecipeInhibit-Multi', 'AutoEquipmentAndRecipeInhibit-Wildcard',
      'AutoEquipmentAndRecipeInhibit-Precipe',
      'AutoEquipmentAndRecipeInhibit-Error', 'AutoEquipmentAndRecipeInhibit-ErrorMulti', 'AutoEquipmentAndRecipeInhibit-ErrorWild',
      'AutoChamberAndRecipeInhibit-Multi',
      'ReleaseEquipmentAndRecipeInhibit', 'ReleaseEquipmentAndRecipeInhibit-noDeflt', 'CACollection-ReleaseAllRecipeInhibits'
    ].each {|action|
      assert $spc.corrective_action(action), "CA #{action.inspect} not defined"
    }
    $sv.lot_info(@@lot, :wafers=>true).wafers.each_with_index {|w,i| @@real_wafer_values[w.wafer] = i + 1}
    $setup_ok = true
  end

  def testex0001_lot_hold_child_lots_all_with_ooc_LOT_parameters
		parameters = @@parameters.collect{|p| p + '_LOT'}
		
    folder = $lds.folder(@@autocreated_qa)
    folder.delete_channels if folder
    channels = parameters.collect{|p| folder.spc_channel(:parameter=>p)}
    
		childs = 1
		parentwafers = 25 - childs
		childeqps = Array.new
		childs.times {|i| childeqps << "UTCSIL0#{i+2}"}
		parenteqp = 'UTFSIL01'
		
		child_lots = childs.times.collect {$sv.lot_split(@@lot, 1)}.compact
		assert_equal childs, child_lots.size, "error splitting lot #{@lot}"
		$log.info "child lots: #{child_lots.inspect}"
		lot_family = [@@lot] + child_lots
		
		wafer_values = Array.new
		parent_wafer_values = {}
		parentwafers.times {|i| parent_wafer_values["Waf#{i.to_s.rjust(4,'0')}"] = i + 800}
		childs.times {|i|
			wafer_values << {"Waf#{(24 - i).to_s.rjust(4,'0')}" => (3 + i) * 100}
			}
		wafer_values << parent_wafer_values
		
    submit_process_dcr(@@ppd_qa, :eqp=>parenteqp, :wafer_values=>wafer_values.last, :chambers=>@@chambers, :processing_areas=>@@chambers.keys, :export=>"log/#{__method__}_p_parent.xml")
		childs.times {|i|
			submit_process_dcr(@@ppd_qa, :eqp=>childeqps[i], :wafer_values=>wafer_values[i], :chambers=>@@chambers, :processing_areas=>@@chambers.keys, :export=>"log/#{__method__}_p_child_#{i}.xml")
		}
		
		all_wafer_values = {}
		wafer_values.each {|h| all_wafer_values.merge!(h)}
		
		wafer_equipments = {}
		parentwaferids = all_wafer_values.keys.sort[0..parentwafers]
		parentwaferids.each {|w| wafer_equipments[w] = parenteqp}
		childs.times {|i| wafer_equipments[wafer_values[i].keys.first] = childeqps[i]}
		
		$log.info "Wafer equipment relations: #{wafer_equipments.inspect}"
		
		res = $sv.merge_lot_family(@@lot)
		$log.info "Executed Merge for lot: #{@@lot} with result: #{res.pretty_inspect}"

    dcr = Space::DCR.new(:lot=>@@lot, :eqp=>@@mtool, :op=>@@mpd_qa, :productgroup=>'QAPG1')
    dcr.add_parameters(parameters, all_wafer_values)
    res = $client.send_dcr(dcr, :export=>"log/#{__method__}.xml")
    #assert $spctest.verify_dcr_accepted(res, parameters.size*(wafer_values.size), :nooc=>parameters.size*(wafer_values.size)*2), 'DCR not submitted successfully'
    #
    folder = $lds.folder(@@autocreated_qa)
    parameters.each{|par|
      ch = folder.spc_channel(:parameter=>par)
			
      next unless ch
      samples = ch.samples      
      samples.each do |s|
        w = s.extractor_keys['Wafer']
				eqp = wafer_equipments[w]
        # must match tool
        assert $spctest.verify_data(s.extractor_keys, {'PTool'=>eqp})
      end
      }
  end

  def testex0002_lot_hold_child_lots_all_with_ooc_LOT_parameters
		parameters = @@parameters.collect{|p| p}
    
    folder = $lds.folder(@@autocreated_qa)
    folder.delete_channels if folder
    channels = parameters.collect{|p| folder.spc_channel(:parameter=>p)}
    
		childs = 1
		parentwafers = 25 - childs
		childeqps = Array.new
		childs.times {|i| childeqps << "UTCSIL0#{i+2}"}
		parenteqp = 'UTCSIL01'
		
		child_lots = childs.times.collect {$sv.lot_split(@@lot, 1)}.compact
		assert_equal childs, child_lots.size, "error splitting lot #{@lot}"
		$log.info "child lots: #{child_lots.inspect}"
		lot_family = [@@lot] + child_lots
		
		wafer_values = Array.new
		parent_wafer_values = {}
		parentwafers.times {|i| parent_wafer_values["Waf#{i.to_s.rjust(4,'0')}"] = i + 800}
		childs.times {|i|
			wafer_values << {"Waf#{(24 - i).to_s.rjust(4,'0')}" => (3 + i) * 100}
			}
		wafer_values << parent_wafer_values
		
    submit_process_dcr(@@ppd_qa, :eqp=>parenteqp, :wafer_values=>wafer_values.last, :chambers=>@@chambers, :processing_areas=>@@chambers.keys, :export=>"log/#{__method__}_p_parent.xml")
		childs.times {|i|
			submit_process_dcr(@@ppd_qa, :eqp=>childeqps[i], :wafer_values=>wafer_values[i], :chambers=>@@chambers, :processing_areas=>@@chambers.keys, :export=>"log/#{__method__}_p_child_#{i}.xml")
		}
		
		all_wafer_values = {}
		wafer_values.each {|h| all_wafer_values.merge!(h)}
		
		wafer_equipments = {}
		parentwaferids = all_wafer_values.keys.sort[0..parentwafers]
		parentwaferids.each {|w| wafer_equipments[w] = parenteqp}
		childs.times {|i| wafer_equipments[wafer_values[i].keys.first] = childeqps[i]}
		
		$log.info "Wafer equipment relations: #{wafer_equipments.inspect}"
		
		res = $sv.merge_lot_family(@@lot)
		$log.info "Executed Merge for lot: #{@@lot} with result: #{res.pretty_inspect}"

    dcr = Space::DCR.new(:lot=>@@lot, :eqp=>@@mtool, :op=>@@mpd_qa, :productgroup=>'QAPG1')
    dcr.add_parameters(parameters, all_wafer_values)
    res = $client.send_dcr(dcr, :export=>"log/#{__method__}.xml")
    #assert $spctest.verify_dcr_accepted(res, parameters.size*(wafer_values.size), :nooc=>parameters.size*(wafer_values.size)*2), 'DCR not submitted successfully'
    #
    folder = $lds.folder(@@autocreated_qa)
    parameters.each{|par|
      ch = folder.spc_channel(:parameter=>par)
			
      next unless ch
      samples = ch.samples      
      samples.each do |s|
        w = s.extractor_keys['Wafer']
				eqp = wafer_equipments[w]
        # must match tool
        assert $spctest.verify_data(s.extractor_keys, {'PTool'=>eqp})
      end
      }
  end

  def testex0003_equipment_and_recipe_inhibit
    _create_chan(dpt='QA', params={:eqp=>"#{@@eqp}_1", :machine_recipe=>'P-UTC001_1.UTMR000.01'})
    #
    assert_equal 0, $sv.eqp_cleanup("#{@@eqp}_1")
    assert_equal 0, $sv.inhibit_cancel(:recipe, 'P-UTC001_1.UTMR000.01')
    folder = $lds.folder(@@autocreated_qa)
    channels = @@parameters.collect{|p| folder.spc_channel(:parameter=>p)}
    #
    # ensure corrective actions are set
    actions = ['AutoEquipmentAndRecipeInhibit-PD1']
    assert $spctest.assign_verify_cas(channels, actions)

    #
    submit_process_dcr(@@ppd_qa, :wafer_values=>@@wafer_values2, :chambers=>@@chambers, :eqp=>"#{@@eqp}_1", :machine_recipe=>'P-UTC001_1.UTMR000.01', :export=>"log/#{__method__}_p.xml")
    # send DCR with OOC values
    wafer_values = Hash[*@@wafer_values2.collect {|w,v| [w,v+100]}.flatten]
    dcr = Space::DCR.new(:lot=>@@lot, :eqp=>@@mtool, :op=>@@mpd_qa, :productgroup=>'QAPG1')
    dcr.add_parameters(@@parameters, wafer_values)
    res = $client.send_dcr(dcr, :export=>"log/#{__method__}.xml")
		$log.info '-------------------------------------------------'
		$log.info "Result: #{res.inspect}"
		$log.info '-------------------------------------------------'
    assert $spctest.verify_dcr_accepted(res, @@parameters.count*wafer_values.size, :nooc=>@@parameters.count*wafer_values.size*2), 'DCR not submitted successfully'
  end
	
	def testex0004_fast_equipment_and_recipe_inhibit_and_release
		chambers = ['PM1', 'PM2', 'PM3','PM4', 'PM5', 'PM6']
		reasoncodes = ['X-S1', 'X-S2', 'X-S3', 'X-S4', 'X-S5']
		#reasoncodes = ['CINH', 'DEDI', 'FRCR', 'GDEF', 'GRET']
		eqp = 'UTCSIL01'
		
		$sv.inhibit_cancel(:eqp_chamber, eqp)
		inhibits = $sv.inhibit_list(:eqp_chamber, eqp)
		assert inhibits.count==0, "Unexpected count of inhibits: #{inhibits.count} instead of 0"
		
		duration = 1800
		runs = 0
		starttime = Time.now
		
		while Time.now - starttime < duration
			$log.info "---- Starting run #{runs+1} now ---- #{duration - (Time.now - starttime)} seconds remaining..."
			$log.info '---- ---- ----'
			chambers.each {|c|
				reasoncodes.each {|rc| 
					$sv.inhibit_entity(:eqp_chamber, eqp, :reason=>rc, :attrib=>c)
				}
			}
			
			inhibits = $sv.inhibit_list(:eqp_chamber, eqp)
			assert inhibits.count==chambers.count*reasoncodes.count, "Unexpected count of inhibits: #{inhibits.count} instead of #{chambers.count*reasoncodes.count}"
			
			sleep 1
			
			chambers.each {|c|
				reasoncodes.each {|rc| 
					$sv.inhibit_cancel(:eqp_chamber, eqp, :inhibit_reason=>rc, :attrib=>c)
				}
			}
			inhibits = $sv.inhibit_list(:eqp_chamber, eqp)
			assert inhibits.count==0, "Unexpected count of inhibits: #{inhibits.count} instead of 0"
			runs+=1
		end
		
		$log.info "   --------- #{runs} runs successfully executed ---------   "
	end

  def testex0005_create_dcr_with_wh_bigger_then_processed_wafers_and_waferparam
		wafer_values = {'2502J502KO'=>41}
		wafer_values_wafers = {'2502J502KO'=>'2502J502KO', '2502J503KO'=>'2502J503KO', '2502J504KO'=>'2502J504KO'}
		
		mpd = 'QA-MB-FTDEPM.01'
    expected_pd_count = 5
    pdrefs = $sildb.pd_reference(:mpd=>mpd)
    assert_equal expected_pd_count, pdrefs.count, "not enough PD references found for #{mpd}"
		#
		@@mtool = 'UTFSIL01'
    #
    # build and submit DCR
    dcr = Space::DCR.new(:op=>pdrefs[0], :lot=>@@lot, :eqp=>@@eqp)
    dcr.add_parameters(@@parameters, wafer_values, :space_listening=>true, :nocharting=>true)
		dcr.add_wafer('2502J503KO', :processing_areas=>[], :processed=>false)
		dcr.add_wafer('2502J504KO', :processing_areas=>[], :processed=>false)
		dcr.add_parameter('C-WafersInFoup', wafer_values_wafers, :space_listening=>false, :aspect_listening=>false, :nocharting=>true, :value_type=>'string')
    res = $client.send_dcr(dcr, :export=>"log/#{__method__}.xml")
    #
    # build and submit DCR
		wafer_values = {'2502J502KO'=>141, '2502J503KO'=>142, '2502J504KO'=>143}
    dcr = Space::DCR.new(:op=>mpd, :lot=>@@lot, :eqp=>@@mtool)
    dcr.add_parameters(@@parameters, wafer_values, :space_listening=>false, :aspect_listening=>false)
    res = $client.send_dcr(dcr, :export=>"log/#{__method__}_m.xml")
    assert $spctest.verify_dcr_accepted(res, wafer_values.size*@@parameters.size), 'DCR not submitted successfully'
  end

  def testex0006_create_dcr_with_wh_equal_to_processed_wafers_and_no_waferparam
		wafer_values = {'2502J496KO'=>41}
		
		mpd = 'QA-MB-FTDEPM.01'
    expected_pd_count = 5
    pdrefs = $sildb.pd_reference(:mpd=>mpd)
    assert_equal expected_pd_count, pdrefs.count, "not enough PD references found for #{mpd}"
		#
		@@mtool = 'UTFSIL01'
    #
    # build and submit DCR
    dcr = Space::DCR.new(:op=>pdrefs[0], :lot=>@@lot, :eqp=>@@eqp)
    dcr.add_parameters(@@parameters, wafer_values, :space_listening=>true, :nocharting=>true)
    res = $client.send_dcr(dcr, :export=>"log/#{__method__}.xml")
    #
    # build and submit DCR
		wafer_values = {'2502J496KO'=>141, '2502J497KO'=>142, '2502J498KO'=>143}
    dcr = Space::DCR.new(:op=>mpd, :lot=>@@lot, :eqp=>@@mtool)
    dcr.add_parameters(@@parameters, wafer_values, :space_listening=>false, :aspect_listening=>false)
    res = $client.send_dcr(dcr, :export=>"log/#{__method__}_m.xml")
    assert $spctest.verify_dcr_accepted(res, wafer_values.size*@@parameters.size), 'DCR not submitted successfully'
  end

  def testex0007_create_dcr_with_wh_bigger_then_processed_wafers_and_no_waferparam
		wafer_values = {'2502J499KO'=>41}
		
		mpd = 'QA-MB-FTDEPM.01'
    expected_pd_count = 5
    pdrefs = $sildb.pd_reference(:mpd=>mpd)
    assert_equal expected_pd_count, pdrefs.count, "not enough PD references found for #{mpd}"
		#
		@@mtool = 'UTFSIL01'
    #
    # build and submit DCR
    dcr = Space::DCR.new(:op=>pdrefs[0], :lot=>@@lot, :eqp=>@@eqp)
    dcr.add_parameters(@@parameters, wafer_values, :space_listening=>true, :nocharting=>true)
		dcr.add_wafer('2502J500KO', :processing_areas=>[], :processed=>false)
		dcr.add_wafer('2502J501KO', :processing_areas=>[], :processed=>false)
    res = $client.send_dcr(dcr, :export=>"log/#{__method__}.xml")
    #
    # build and submit DCR
		wafer_values = {'2502J499KO'=>141, '2502J500KO'=>142, '2502J501KO'=>143}
    dcr = Space::DCR.new(:op=>mpd, :lot=>@@lot, :eqp=>@@mtool)
    dcr.add_parameters(@@parameters, wafer_values, :space_listening=>false, :aspect_listening=>false)
    res = $client.send_dcr(dcr, :export=>"log/#{__method__}_m.xml")
    assert $spctest.verify_dcr_accepted(res, wafer_values.size*@@parameters.size), 'DCR not submitted successfully'
  end

  def testex0008_send_dcr_with_many_samples_many_times
		wafer_id_pre = '2502J'
		wafer_id_count = 800
		wafer_id_post = 'KO'
		wafer_value_start = 45
		wafers = 1
		loops = 1
		
		loops.times {
			wafer_ids = []
			wafer_values = []
			wafer_id_value_pairs = {}
			wafers.times {|i| wafer_ids << wafer_id_pre + (wafer_id_count + i).to_s + wafer_id_post}
			wafers.times {wafer_values << wafer_value_start}
			wafers.times {|i| wafer_id_value_pairs[wafer_ids[i]] = wafer_values[i]}
			
			mpd = 'QA-MB-FTDEPM.01'
			expected_pd_count = 5
      pdrefs = $sildb.pd_reference(:mpd=>mpd)
      assert_equal expected_pd_count, pdrefs.count, "not enough PD references found for #{mpd}"
			#
			@@mtool = 'UTFSIL01'
			#
			# build and submit DCR
			dcr = Space::DCR.new(:op=>pdrefs[0], :lot=>@@lot, :eqp=>@@eqp)
			dcr.add_parameters(@@parameters, wafer_id_value_pairs, :space_listening=>true, :nocharting=>true)
			res = $client.send_dcr(dcr, :export=>"log/#{__method__}.xml")
			#
			# build and submit DCR
			wafer_values = []
			wafer_id_value_pairs = {}
			wafers.times {|i| wafer_values << wafer_value_start + 200 + i}
			wafers.times {|i| wafer_id_value_pairs[wafer_ids[i]] = wafer_values[i]}
			$log.info "Wafers: #{wafer_id_value_pairs.inspect}"

			dcr = Space::DCR.new(:op=>mpd, :lot=>@@lot, :eqp=>@@mtool)
			dcr.add_parameters(@@parameters, wafer_id_value_pairs, :space_listening=>false, :aspect_listening=>false)
			res = $client.send_dcr(dcr, :export=>"log/#{__method__}_m.xml")
			$spctest.verify_dcr_accepted(res, wafers*@@parameters.size)
		}
  end

  def testex0009_equipment_and_recipe_inhibit
    _create_chan(dpt='QA', params={:eqp=>"#{@@eqp}_1", :machine_recipe=>'P-UTC001_1.UTMR000.01'})
    #
    assert_equal 0, $sv.eqp_cleanup("#{@@eqp}_1")
    assert_equal 0, $sv.inhibit_cancel(:recipe, 'P-UTC001_1.UTMR000.01')
    folder = $lds.folder(@@autocreated_qa)
    channels = @@parameters.collect {|p| folder.spc_channel(:parameter=>p)}
    #
    # ensure corrective actions are set
    actions = ['AutoEquipmentAndRecipeInhibit-PD0']
    assert $spctest.assign_verify_cas(channels, actions)

    #
    submit_process_dcr(@@ppd_qa, :wafer_values=>@@wafer_values2, :chambers=>@@chambers, :eqp=>"#{@@eqp}_1", :machine_recipe=>'P-UTC001_1.UTMR000.01', :export=>"log/#{__method__}_p.xml")
    # send DCR with OOC values
    wafer_values = Hash[*@@wafer_values2.collect {|w,v| [w,v+100]}.flatten]
    dcr = Space::DCR.new(:lot=>@@lot, :eqp=>@@mtool, :op=>@@mpd_qa, :productgroup=>'QAPG1')
    dcr.add_parameters(@@parameters, wafer_values)
    res = $client.send_dcr(dcr, :export=>"log/#{__method__}.xml")
		$log.info '-------------------------------------------------'
		$log.info "Result: #{res.inspect}"
		$log.info '-------------------------------------------------'
    assert $spctest.verify_dcr_accepted(res, @@parameters.count*wafer_values.size, :nooc=>@@parameters.count*wafer_values.size*2), 'DCR not submitted successfully'
  end

  # Send some measurement without violations and then send a measurement with a violation which was measured before the last non violated measurement
  def testex0010_n_of_m_measurement_time_out_of_synch
    parameters = ['QA_PARAM_NofM_900', 'QA_PARAM_NofM_901', 'QA_PARAM_NofM_902', 'QA_PARAM_NofM_903', 'QA_PARAM_NofM_904']
    channels = _create_chan('QA', {:parameters=>parameters})
    #
    # ensure corrective actions are set
    actions = ['LotHold', 'AutoLotHold', 'ReleaseLotHold', 'CancelLotHold']
    assert $spctest.assign_verify_cas(channels, actions)

    #
    submit_process_dcr(@@ppd_qa, :wafer_values=>@@wafer_values2, :chambers=>@@chambers, :export=>"log/#{__method__}_p.xml")
    # send DCR with OOC values
    wafer_values = Hash[*@@wafer_values2.collect {|w,v| [w,v+11]}.flatten]
    dcr = Space::DCR.new(:lot=>@@lot, :eqp=>@@mtool, :op=>@@mpd_qa, :productgroup=>'QAPG1')
    dcr.add_parameters(parameters, wafer_values)
    res = $client.send_dcr(dcr, :export=>"log/#{__method__}.xml")
		$log.info '-------------------------------------------------'
		$log.info "Result: #{res.inspect}"
		$log.info '-------------------------------------------------'
    assert $spctest.verify_dcr_accepted(res, parameters.count*wafer_values.size), 'DCR not submitted successfully'
    #
    # verify futurehold from AutoLotHold
    assert $spctest.verify_futurehold2(@@lot, '[(N of M in zone A or B upper) LDS->INLI', :timeout=>@@ca_time_out, :dcr_lot=>dcr.find_lot_context(@@lot)), 'lot must have a future hold'
    #
    # verify comment is set
    assert $spctest.verify_ecomment(channels, "LOT_HELD: #{@@lot}: reason={X-S")

		$log.info "Holds for Lot #{@@lot}: #{$sv.lot_futurehold_list @@lot}"
		
    channels.each{|ch|
      s = ch.samples(:ckc=>true)[-1]
      #
      # CancelLotHold (from automatic action)
      assert s.assign_ca($spc.corrective_action('CancelLotHold')), 'error assigning corective action'
    }
		
    s = channels[0].samples(:ckc=>true)[-1]
    assert $spctest.verify_futurehold_cancel(@@lot, '[(Raw above specification)', :timeout=>@@ca_time_out), 'lot must have no future hold'
    #
    # LotHold (manual action)
    assert s.assign_ca($spc.corrective_action('LotHold')), 'error assigning corective action'
    assert $spctest.verify_futurehold2(@@lot, '', :timeout=>@@ca_time_out, :dcr_lot=>dcr.find_lot_context(@@lot), :parameter=>s.parameter), 'lot must have a future hold'
    #
    # CancelLotHold (from manual action)
    assert s.assign_ca($spc.corrective_action('CancelLotHold')), 'error assigning corective action'
    assert $spctest.verify_futurehold_cancel(@@lot, nil, :timeout=>@@ca_time_out), 'lot must have no future hold'
    #
    # LotHold (manual action, prepare release)
    assert s.assign_ca($spc.corrective_action('LotHold')), 'error assigning corective action'
    assert $spctest.verify_futurehold2(@@lot, '', :timeout=>@@ca_time_out, :dcr_lot=>dcr.find_lot_context(@@lot), :parameter=>s.parameter), 'lot must have a future hold'
    #
    # ReleaseLotHold
    assert_equal 0, $sv.lot_gatepass(@@lot), 'SiView GatePAss error'
    assert $spctest.verify_futurehold_cancel(@@lot, nil, :timeout=>@@ca_time_out), 'lot must have no future hold'
    assert $sv.lot_hold_list(@@lot, :type=>'FutureHold').size > 0, 'lot must be on hold'
    #
    assert s.assign_ca($spc.corrective_action('ReleaseLotHold')), 'error assigning corective action'
    sleep 60
    assert $sv.lot_hold_list(@@lot, :type=>'FutureHold').size == 0, 'lot must not be on hold'
  end

  # Send some measurement without violations and then send a measurement with a violation which was measured before the last non violated measurement
  def testex0011_n_of_m_measurement_time_out_of_synch
    parameters = ['QA_PARAM_NofM_900', 'QA_PARAM_NofM_901', 'QA_PARAM_NofM_902', 'QA_PARAM_NofM_903', 'QA_PARAM_NofM_904']
    channels = _create_chan('QA', {:parameters=>parameters, :method=>"#{__method__}_createchannel"})
    #
    @@lots.each {|l| assert_equal 0, $sv.lot_cleanup(l)}
    #
    # ensure corrective actions are set
    actions = ['LotHold', 'AutoLotHold', 'ReleaseLotHold', 'CancelLotHold']
    assert $spctest.assign_verify_cas(channels, actions)

    #
    submit_process_dcr(@@ppd_qa, :lot=>@@lots[0], :wafer_values=>@@wafer_values2, :chambers=>@@chambers, :export=>"log/#{__method__}_1_lot0_p.xml")
    
    # send DCR without OOC values
    wafer_values = Hash[*@@wafer_values2.collect {|w,v| [w,v+1]}.flatten]
    dcr = Space::DCR.new(:lot=>@@lots[0], :eqp=>@@mtool, :op=>@@mpd_qa, :productgroup=>'QAPG1')
    dcr.add_parameters(parameters, wafer_values)
    res = $client.send_dcr(dcr, :export=>"log/#{__method__}_1_lot0_noviolation.xml")
		$log.info '-------------------------------------------------'
		$log.info "Result: #{res.inspect}"
		$log.info '-------------------------------------------------'
    assert $spctest.verify_dcr_accepted(res, parameters.count*wafer_values.size), 'DCR not submitted successfully'
    #
    # verify futurehold from AutoLotHold
    assert !$spctest.verify_futurehold2(@@lots[0], '[(N of M in zone A or B upper) LDS->INLI', :timeout=>@@ca_time_out, :dcr_lot=>dcr.find_lot_context(@@lots[0])), 'lot must have no future hold'
    
    submit_process_dcr(@@ppd_qa, :lot=>@@lots[0], :wafer_values=>@@wafer_values2, :chambers=>@@chambers, :export=>"log/#{__method__}_2_lot0_p.xml")
    sleep 30
    submit_process_dcr(@@ppd_qa, :lot=>@@lots[1], :wafer_values=>@@wafer_values2, :chambers=>@@chambers, :export=>"log/#{__method__}_3_lot1_p.xml")
    
    ##### Measure Lot 0 now - but send the dcr later
    sleep 30
    # send DCR with OOC values
    wafer_values = Hash[*@@wafer_values2.collect {|w,v| [w,v+11]}.flatten]
    dcr_ts = Space::DCR.new(:lot=>@@lots[0], :eqp=>@@mtool, :op=>@@mpd_qa, :productgroup=>'QAPG1')
    dcr_ts.add_parameters(parameters, wafer_values)
    sleep 30
    ##### --------------------------------------------
    
    ##### Measure Lot 1 now and send the MSR immediately
    # send DCR with OOC values
    wafer_values = Hash[*@@wafer_values2.collect {|w,v| [w,v+1]}.flatten]
    dcr = Space::DCR.new(:lot=>@@lots[1], :eqp=>@@mtool, :op=>@@mpd_qa, :productgroup=>'QAPG1')
    dcr.add_parameters(parameters, wafer_values)
    res = $client.send_dcr(dcr, :export=>"log/#{__method__}_2_lot1_noviolation.xml")
		$log.info '-------------------------------------------------'
		$log.info "Result: #{res.inspect}"
		$log.info '-------------------------------------------------'
    assert $spctest.verify_dcr_accepted(res, parameters.count*wafer_values.size), 'DCR not submitted successfully'
    #
    # verify futurehold from AutoLotHold
    #assert $spctest.verify_futurehold2(@@lots[1], '[(N of M in zone A or B upper) LDS->INLI', :timeout=>@@ca_time_out, :dcr_lot=>dcr.find_lot_context(@@lots[1])), 'lot must have a future hold'
    #
    # verify comment is set
    #assert $spctest.verify_ecomment(channels, "LOT_HELD: #{@@lots[1]}: reason={X-S")
    channels.each{|ch|
      s = ch.samples(:ckc=>true)[-1]
      #
      # CancelLotHold (from automatic action)
      #assert s.assign_ca($spc.corrective_action('CancelLotHold')), 'error assigning corective action'
    }
    s = channels[0].samples(:ckc=>true)[-1]
    #assert $spctest.verify_futurehold_cancel(@@lots[1], '[(Raw above specification)', :timeout=>@@ca_time_out), 'lot must have no future hold'
    $log.info 'sleep 120 seconds now - please check the chart'
    sleep 120
    ##### ---------------------------------------------
    
    ##### send the DCR for lot 0 now - has to be shown in chart before lot 1 samples
    res = $client.send_dcr(dcr_ts, :export=>"log/#{__method__}_3_lot0_violation.xml")
		$log.info '-------------------------------------------------'
		$log.info "Result: #{res.inspect}"
		$log.info '-------------------------------------------------'
    assert $spctest.verify_dcr_accepted(res, parameters.count*wafer_values.size), 'DCR not submitted successfully'
    #
    # verify futurehold from AutoLotHold
    assert $spctest.verify_futurehold2(@@lots[0], '[(N of M in zone A or B upper) LDS->INLI', :timeout=>@@ca_time_out, :dcr_lot=>dcr_ts.find_lot_context(@@lots[0])), 'lot must have a future hold'
    #
    # verify comment is set
    #assert $spctest.verify_ecomment(channels, "LOT_HELD: #{@@lots[0]}: reason={X-S")
    channels.each{|ch|
      s = ch.samples(:ckc=>true)[-1]
      #
      # CancelLotHold (from automatic action)
      assert s.assign_ca($spc.corrective_action('CancelLotHold')), 'error assigning corective action'
    }
    s = channels[0].samples(:ckc=>true)[-1]
    assert $spctest.verify_futurehold_cancel(@@lots[0], '[(Raw above specification)', :timeout=>@@ca_time_out), 'lot must have no future hold'
    #
    #assert $sv.lot_hold_list(@@lot, :type=>'FutureHold').size == 0, 'lot must not be on hold'
  end

  def testex0012_dcr_with_real_waferids
    #channels = create_clean_channels(:delete=>false)
    submit_process_dcr(@@ppd_qa, :wafer_values=>@@real_wafer_values, :chambers=>@@chambers, :export=>"log/#{__method__}_p.xml")
    # send DCR with OOC values
    wafer_values = Hash[*@@real_wafer_values.collect {|w,v| [w,v]}.flatten]
    dcr = Space::DCR.new(:lot=>@@lot, :eqp=>@@mtool, :op=>@@mpd_qa)
    dcr.add_parameters(@@parameters, wafer_values)
    res = $client.send_dcr(dcr, :export=>"log/#{__method__}.xml")
  end
  
  def testex0013_dcrs_with_different_tools_two_specs_with_same_parameter
  	assert_equal 0, $sv.eqp_cleanup('UTCSIL02')
    submit_process_dcr(@@ppd_qa, :eqp=>'UTCSIL01', :lot=>@@lot, :technology=>'32NM', :export=>"log/#{__method__}_p.xml")
    # send DCR with OOC values
    wafer_values = Hash[*@@wafer_values.collect {|w,v| [w,v]}.flatten]
    dcr = Space::DCR.new(:lot=>@@lot, :eqp=>@@mtool, :op=>@@mpd_qa)
    dcr.add_parameters(['QA_P900_ECP_COMPTest'], wafer_values)
    res = $client.send_dcr(dcr, :export=>"log/#{__method__}.xml")
    
    submit_process_dcr(@@ppd_qa, :eqp=>'UTCSIL02', :lot=>@@lot, :technology=>'32NM', :export=>"log/#{__method__}_p.xml")
    # send DCR with OOC values
    wafer_values = Hash[*@@wafer_values.collect {|w,v| [w,v]}.flatten]
    dcr = Space::DCR.new(:lot=>@@lot, :eqp=>@@mtool, :op=>@@mpd_qa)
    dcr.add_parameters(['QA_P900_ECP_COMPTest'], wafer_values)
    res = $client.send_dcr(dcr, :export=>"log/#{__method__}.xml")
    
    submit_process_dcr(@@ppd_qa, :eqp=>'UTCSIL02', :lot=>@@lot, :technology=>'32NM', :export=>"log/#{__method__}_p.xml")
    # send DCR with OOC values
    wafer_values = Hash[*@@wafer_values.collect {|w,v| [w,v]}.flatten]
    dcr = Space::DCR.new(:lot=>@@lot, :eqp=>@@mtool, :op=>@@mpd_qa)
    dcr.add_parameters(['QA_P900_ECP_COMPTest'], wafer_values)
    res = $client.send_dcr(dcr, :export=>"log/#{__method__}.xml")
    
    submit_process_dcr(@@ppd_qa, :eqp=>'UTCSIL01', :lot=>@@lot, :technology=>'32NM', :export=>"log/#{__method__}_p.xml")
    # send DCR with OOC values
    wafer_values = Hash[*@@wafer_values.collect {|w,v| [w,v]}.flatten]
    dcr = Space::DCR.new(:lot=>@@lot, :eqp=>@@mtool, :op=>@@mpd_qa)
    dcr.add_parameters(['QA_P900_ECP_COMPTest'], wafer_values)
    res = $client.send_dcr(dcr, :export=>"log/#{__method__}.xml")
    
    submit_process_dcr(@@ppd_qa, :eqp=>'UTCSIL03', :lot=>@@lot, :technology=>'32NM', :export=>"log/#{__method__}_p.xml")
    # send DCR with OOC values
    wafer_values = Hash[*@@wafer_values.collect {|w,v| [w,v]}.flatten]
    dcr = Space::DCR.new(:lot=>@@lot, :eqp=>@@mtool, :op=>@@mpd_qa)
    dcr.add_parameters(['QA_P900_ECP_COMPTest'], wafer_values)
    res = $client.send_dcr(dcr, :export=>"log/#{__method__}.xml")
  end
  
  def testex0014_dcrs_with_multiple_lots_and_different_keys
  	assert_equal 0, $sv.lot_cleanup(@@seclot)
    folder = $lds.folder('AutoCreated_QA')
    parameters = ['QA_PARAM_900_ECP', 'QA_PARAM_901_ECP', 'QA_PARAM_902_ECP', 'QA_PARAM_903_ECP', 'QA_PARAM_904_ECP']
    
    ppd = $sildb.pd_reference(:mpd=>'QA-MB-AQV-MultiLot-M.01').first
    assert_not_nil ppd, 'no PD reference found for QA-MB-FTDEPM.01'
    $log.info "using process PD #{ppd}"
    #
    # build and submit DCR for process PD
    wv1=Hash[*@@wafer_values.keys.slice(0..2).collect {|e|[e,@@wafer_values[e]]}.flatten]
    wv2=Hash[*@@wafer_values.keys.slice(3..5).collect {|e|[e,@@wafer_values[e]]}.flatten]
    
    submit_process_dcr(ppd, :eqp=>@@eqp, :lot=>@@lot, :wafer_values=>wv1, :technology=>'TEST', :route=>'AQV_Multilot.01',
      :addlots=>[@@seclot], :addwafer_values=>[wv2], :seclot_productgroup=>'QAPG-NOECP', :export=>"log/#{__method__}_p.xml") #:seclot_productgroup=>'QAPG-NOECP'
        
    # send DCR with OOC values
    dcr = Space::DCR.new(:lot=>@@lot, :eqp=>@@mtool, :op=>'QA-MB-AQV-MultiLot-M.01', :technology=>'TEST', :route=>'AQV_Multilot.01')
    dcr.add_parameters(parameters, wv1)
    dcr.add_lot(@@seclot, {:eqp=>@@mtool, :op=>'QA-MB-AQV-MultiLot-M.01', :productgroup=>'QAPG-NOECP', :technology=>'TEST', :route=>'AQV_Multilot.01'}) #:productgroup=>'QAPG-NOECP'
    dcr.add_parameters(parameters, wv2)
    res = $client.send_dcr(dcr, :export=>"log/#{__method__}.xml")
  end
  
  def testex0015_dcrs_with_multiple_lots_and_different_keys_mainlotooc
  	assert_equal 0, $sv.lot_cleanup(@@seclot)
    folder = $lds.folder('AutoCreated_QA')
    parameters = ['QA_PARAM_900_ECP', 'QA_PARAM_901_ECP', 'QA_PARAM_902_ECP', 'QA_PARAM_903_ECP', 'QA_PARAM_904_ECP']
    
    ppd = $sildb.pd_reference(:mpd=>'QA-MB-AQV-MultiLot-M.01').first
    assert_not_nil ppd, 'no PD reference found for QA-MB-FTDEPM.01'
    $log.info "using process PD #{ppd}"
    #
    # build and submit DCR for process PD
    wv1=Hash[*@@wafer_values.keys.slice(0..2).collect {|e|[e,@@wafer_values[e] + 100]}.flatten]
    wv2=Hash[*@@wafer_values.keys.slice(3..5).collect {|e|[e,@@wafer_values[e]]}.flatten]
    
    submit_process_dcr(ppd, :eqp=>@@eqp, :lot=>@@lot, :wafer_values=>wv1, :technology=>'TEST', :route=>'AQV_Multilot.01',
      :addlots=>[@@seclot], :addwafer_values=>[wv2], :seclot_productgroup=>'QAPG-NOECP', :export=>"log/#{__method__}_p.xml") #:seclot_productgroup=>'QAPG-NOECP'
        
    # send DCR with OOC values
    dcr = Space::DCR.new(:lot=>@@lot, :eqp=>@@mtool, :op=>'QA-MB-AQV-MultiLot-M.01', :technology=>'TEST', :route=>'AQV_Multilot.01')
    dcr.add_parameters(parameters, wv1)
    dcr.add_lot(@@seclot, {:eqp=>@@mtool, :op=>'QA-MB-AQV-MultiLot-M.01', :productgroup=>'QAPG-NOECP', :technology=>'TEST', :route=>'AQV_Multilot.01'}) #:productgroup=>'QAPG-NOECP'
    dcr.add_parameters(parameters, wv2)
    res = $client.send_dcr(dcr, :export=>"log/#{__method__}.xml")
  end
  
  def testex0016_many_ckcs
    # In template _Template_QA_CKCS please remove PLogRecipe and add Route instead! Withdraw after this test.
    parameters = ['QA_PARAM_700', 'QA_PARAM_701', 'QA_PARAM_702', 'QA_PARAM_703', 'QA_PARAM_704']
    wafer_values = @@wafer_values2 #{'SIL008.00.01'=>40}
    1.times {|i|
      dcr = Space::DCR.new(lot: 'SIL0411000201.000', eqp: 'UTFSIL01', route: "SIL0001-#{i}.01")
      dcr.add_parameters(parameters, wafer_values)
      res = $client.send_dcr(dcr, delete: false)
    }
  end
	

  # send dcr with non-ooc values to setup channels
  def _create_chan(dpt='QA', params={})
		delete = false
		delete = params[:delete] if params[:delete] == false
    parameters = (params[:parameters] or @@parameters)
		wafer_values =(params[:wafer_values] or @@wafer_values2)
		space_listening =(params[:space_listening] or false)
    method = (params[:method] or __method__)
    ##wafer_values = Hash[wafer_values.each_pair.collect {|k,p| ['AA'+k,p]}]
    if (@@lit_used)
      #folder = $lds.folder('AutoCreated_LIT')
	    folder = $lds.folder(@@autocreated_qa)
	    folder.delete_channels if folder
      @@lit_used = false
    end
    #
    folder = $lds.folder("AutoCreated_#{dpt}")
		if delete == true
			parameters.each {|p| folder.spc_channels(parameter: p).each {|ch| ret &= (ch.delete == 0)}}
		end
    #
    chambers = (params[:chambers] or @@chambers)
    mtool = (params[:mtool] or @@mtool)
    ptool = (params[:eqp] or @@eqp)
    mpd = (params[:mpd] or @@mpd_qa)
    nsamples = (params[:nsamples] or (parameters.size*wafer_values.size))
    #
    lot = @@lot##.sub('SIL', 'SOL')
    #
    @@lit_used = (dpt=='LIT')
    #
    if @@lit_used
      mpd = @@mpd_lit
    else
      submit_process_dcr(@@ppd_qa, :lot=>lot, :wafer_values=>wafer_values, :chambers=>chambers, :eqp=>ptool, :export=>"log/#{method}_p.xml")
    end

    dcr = Space::DCR.new(:lot=>lot, :eqp=>mtool, :department=>dpt, :op=>mpd, :productgroup=>'QAPG1', :space_listening=>space_listening)
    dcr.add_parameters(parameters, wafer_values)
    res = $client.send_dcr(dcr, :export=>"log/#{method}_m.xml")
		$log.info '-------------------------------------------------'
		$log.info "Result: #{res.inspect}"
		$log.info '-------------------------------------------------'
    assert $spctest.verify_dcr_accepted(res, nsamples), 'DCR not submitted successfully'
    #
    folder = $lds.folder("AutoCreated_#{dpt}")  # need to get folder again
    return parameters.collect {|p|
      ch = folder.spc_channel(:parameter=>p)
      assert ch, "channel for parameter #{p} missing in folder AutoCreated_#{dpt}"
      ch
    }
  end
	
	def test7999_cleanup
		res = $sv.merge_lot_family(@@lot)
		$log.info "Executed Merge for lot: #{@@lot} with result: #{res.pretty_inspect}"
	end

#TODO
# AutoHoldAtNextDR_PRE
# AutoHoldAtNextDR_POST
# AutoHoldAtPD_PRE
# AutoHoldAtPD_POST
end
