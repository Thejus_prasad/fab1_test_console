=begin 
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.
    
Author: Daniel Steger, 2012-01-09
=end


require_relative 'Test_Space_Setup'

# Test Space/SIL, Fab8 only
class Test_Space062 < Test_Space_Setup
  @@derdack_missingspeclimitsoff = true
  @@datacheck_parameter_inline_enabled_is_set = false
  @@datacheck_parameter_setup_enabled_is_set = false
  
  def test06200_setup
    skip "not applicable in #{$env}" unless ['mtqa', 'f8stag'].member?($env)
    ##return unless check_env_f8
    
    $setup_ok = false
    assert $spctest.verify_speclimit(:specid=>"M07-00020002", :specversion=>6, :all_active=>true), "Speclimits do not exist"
    $setup_ok = true
  end

  
  def test06201_speclimit_f8_match_product
    skip "not applicable in #{$env}" unless ['mtqa', 'f8stag'].member?($env)
    assert_equal 0, $sv.lot_cleanup(@@lot)
    send_check_measurement_dcr(:productgroup=>"C-EM-PG1", :product=>"CF-DFU-PROD1.01", :op=>"CF-MONITORPROC.01", :usl=>111, :target=>61, :lsl=>12)
    send_check_measurement_dcr(:productgroup=>"C-EM-PG2", :product=>"CF-DFU-PROD2.01", :op=>"CF-MONITORPROC.01", :usl=>102, :target=>52, :lsl=>3)
  end

  #MSR542436
  def test06202_speclimit_f8_match_productgroup
    skip "not applicable in #{$env}" unless ['mtqa', 'f8stag'].member?($env)
    assert_equal 0, $sv.lot_cleanup(@@lot)
    send_check_measurement_dcr(:productgroup=>"C-EM-PG2", :product=>"CF-DFU-PROD0X.01", :op=>"CF-MONITORPROC.01", :usl=>101, :target=>51, :lsl=>2)
    send_check_measurement_dcr(:productgroup=>"C-EM-PG4", :product=>"CF-DFU-PROD0Y.01", :op=>"CF-MONITORPROC.01", :usl=>105, :target=>55, :lsl=>6)    
  end
  
  def test06203_speclimit_f8_match_default_no_limits
    skip "not applicable in #{$env}" unless ['mtqa', 'f8stag'].member?($env)
    assert_equal 0, $sv.lot_cleanup(@@lot)
    send_check_measurement_dcr(:productgroup=>"C-EM-PGX", :product=>"CF-DFU-PRODY.01", :op=>"CF-MONITORPROC.01", :usl=>nil, :target=>0.0, :lsl=>nil)
    if @@datacheck_missingspeclimits_inline_enabled
      assert( $spctest.verify_notification("Error code : NO_SPEC_LIMITS", "Process : CF-MONITORPROC.01"), "no notification found." ) unless @@derdack_missingspeclimitsoff
    end
  end
  

  def test06204_speclimit_f8_match_default_no_limits
    skip "not applicable in #{$env}" unless ['mtqa', 'f8stag'].member?($env)
    assert_equal 0, $sv.lot_cleanup(@@lot)
    send_check_measurement_dcr(:setup=>true, :technology=>"TEST", :productgroup=>"C-EM-PGX", :product=>"CF-DFU-PRODY.01", :op=>"CF-MONITORPROC.01", :usl=>nil, :target=>0.0, :lsl=>nil)
    if @@datacheck_missingspeclimits_setup_enabled
      assert( $spctest.verify_notification("Error code : NO_SPEC_LIMITS", "Process : CF-MONITORPROC.01"), "no notification found." ) unless @@derdack_missingspeclimitsoff
    end
  end
    
  def test06205_speclimit_f8_match_random
    skip "not applicable in #{$env}" unless ['mtqa', 'f8stag'].member?($env)
    assert_equal 0, $sv.lot_cleanup(@@lot)
    10.times { |i|
      if rand >= 0.5
        $log.info "match_product"
        send_check_measurement_dcr(:productgroup=>"C-EM-PG1", :product=>"CF-DFU-PROD1.01", :op=>"CF-MONITORPROC.01", :usl=>111, :target=>61, :lsl=>12)
      else
        $log.info "match_productgroup"
        send_check_measurement_dcr(:productgroup=>"C-EM-PG2", :product=>"CF-DFU-PROD05.01", :op=>"CF-MONITORPROC.01", :usl=>101, :target=>51, :lsl=>2)
      end
    }
  end
  
  # Not used - only for special testing
  def YXXtest0625_speclimit_f8_match_random
    skip "not applicable in #{$env}" unless ['mtqa', 'f8stag'].member?($env)
    assert_equal 0, $sv.lot_cleanup(@@lot)
    100.times {|i|
        $log.info "match_productgroup"
        send_check_measurement_dcr(:productgroup=>"C-EM-PG2", :product=>"CF-DFU-PROD05.01", :op=>"CF-MONITORPROC.01", :usl=>101, :target=>51, :lsl=>2)
      
    }
    
    100.times { |i|
      $log.info "match_product"
      send_check_measurement_dcr(:productgroup=>"C-EM-PG1", :product=>"CF-DFU-PROD1.01", :op=>"CF-MONITORPROC.01", :usl=>111, :target=>61, :lsl=>12)
    } 
    
  end
  
  #MSR 539408 
  def test06206_speclimit_f8_match_withlotparam
    skip "not applicable in #{$env}" unless ['mtqa', 'f8stag'].member?($env)
    assert_equal 0, $sv.lot_cleanup(@@lot)

    send_check_measurement_dcr(:productgroup=>"C-EM-PG1", :product=>"CF-DFU-PROD7.01", :op=>"CF-MONITORPROC.01", :usl=>115, :target=>65, :lsl=>16, :sendlotparam=>true)
    
    #Check for no MISSING_PARAMETER Message
    assert !$spctest.verify_futurehold(@@lot, "Missing parameter in DCR"), "future hold must not be set."
    assert !$spctest.verify_notification("Error code : MISSING_DCR_PARAMETERS", @@parameters[0]), "notification missing"
  end
    
  def test06207_speclimit_f8_multilot_wafer
    skip "not applicable in #{$env}" unless ['mtqa', 'f8stag'].member?($env)
    
    send_check_measurement_dcr_multlot(
      {"SIL006.01" => {:productgroup=>"C-EM-PG1", :product=>"CF-DFU-PROD1.01", :op=>"CF-MONITORPROC.01", 
          :wafer=>{"2502J493KO"=>41, "2502J494KO"=>32}, :limits=>{:usl=>111, :target=>61, :lsl=>12}},
        "SIL006.02" => {:productgroup=>"C-EM-PG2", :product=>"CF-DFU-PRODX.01", :op=>"CF-MONITORPROC.01", 
          :wafer=>{"2502J495KO"=>50, "2502J496KO"=>34}, :limits=>{:usl=>101, :target=>51, :lsl=>2}},
       "SIL006.03" => {:productgroup=>"C-EM-PG2", :product=>"CF-DFU-PROD2.01", :op=>"CF-OTHERPROC.01", 
          :wafer=>{"2502J497KO"=>55, "2502J498KO"=>36}, :limits=>{:usl=>116, :target=>66, :lsl=>17}},
      })
  end
  
  def test06208_speclimit_f8_multilot_lot
    skip "not applicable in #{$env}" unless ['mtqa', 'f8stag'].member?($env)
    
    send_check_measurement_dcr_multlot(
      {"SIL006.01" => {:productgroup=>"C-EM-PG1", :product=>"CF-DFU-PROD1.01", :op=>"CF-MONITORPROC.01", 
          :value=>20, :limits=>{:usl=>111, :target=>61, :lsl=>12}},
        "SIL006.02" => {:productgroup=>"C-EM-PG2", :product=>"CF-DFU-PRODX.01", :op=>"CF-MONITORPROC.01", 
          :value=>43, :limits=>{:usl=>101, :target=>51, :lsl=>2}},
       "SIL006.03" => {:productgroup=>"C-EM-PG2", :product=>"CF-DFU-PROD2.01", :op=>"CF-OTHERPROC.01", 
          :value=>35, :limits=>{:usl=>116, :target=>66, :lsl=>17}},
      })
  end
  
  # MSR584398 has been cancelled / so current behavior is no limits
  def test06209_speclimit_f8_product_group_null
    skip "not applicable in #{$env}" unless ['mtqa', 'f8stag'].member?($env)

    send_check_measurement_dcr(:productgroup=>"C-EM-PG5", :product=>"CF-DFU-PROD6.01", :op=>"CF-MONITORPROC.01", 
      :usl=>nil, :target=>nil, :lsl=>nil)
      #:usl=>108, :target=>58, :lsl=>9)
  end
 
  def test06210_speclimit_f8_missing_parameter
    skip "not applicable in #{$env}" unless ['mtqa', 'f8stag'].member?($env)
    assert_equal 0, $sv.lot_cleanup(@@lot)
    send_check_measurement_dcr(:productgroup=>"C-EM-PG5", :product=>"CF-DFU-PROD8.01", :op=>"CF-MONITORPROC.01", 
      :usl=>118, :target=>68, :lsl=>19)
      
    #Check for MISSING_PARAMETER Message
    if @@datacheck_parameter_inline_enabled_is_set
      assert $spctest.verify_futurehold(@@lot, "Missing parameter in DCR"), "future hold must be set."
      assert $spctest.verify_notification("Error code : MISSING_DCR_PARAMETERS", @@parameters[0]), "notification missing"
    end
  end
  
  def test06211_speclimit_f8_missing_parameter
    skip "not applicable in #{$env}" unless ['mtqa', 'f8stag'].member?($env)

    assert_equal 0, $sv.lot_cleanup(@@lot)
    send_check_measurement_dcr_multiparam(
      { "DFU_SumAllDefectsOblique" => [19,68,118], "MissingParameter" => [18,67,117]},
      :productgroup=>"C-EM-PG5", :product=>"CF-DFU-PROD8.01", :op=>"CF-MONITORPROC.01")
      
    #Check for MISSING_PARAMETER Message
    assert !$spctest.verify_futurehold(@@lot, "Missing parameter in DCR"), "future hold must be set."
    assert !$spctest.verify_notification("Error code : MISSING_DCR_PARAMETERS", @@parameters[0]), "notification missing"
  end

  def test06220_speclimit_f8_nospeclimit
    skip "not applicable in #{$env}" unless ['mtqa', 'f8stag'].member?($env)

    assert_equal 0, $sv.lot_cleanup(@@lot)
    send_check_measurement_dcr_multiparam(
      { "DFU_NoLimits" => [nil, nil, nil],
        "DFU_NoTarget" => [0, nil, 20],
        "DFU_NoLowerUpper" => [nil, 10, nil],
        "DFU_NoUpper" => [10, 15, nil],
        "DFU_NoLower" => [nil, 10, 20]
      },
      :productgroup=>"C-EM-PG1", :product=>"CF-DFU-PROD1.01", :op=>"CF-LIMITPROC.01")
    if @@datacheck_missingspeclimits_setup_enabled
      assert( $spctest.verify_notification("Error code : NO_SPEC_LIMITS", "Process : CF-LIMITPROC.01"), "no notification found." ) unless @@derdack_missingspeclimitsoff
    end
  end

  def send_check_measurement_dcr(params)
    param = (params[:parameter] or "DFU_SumAllDefectsOblique")
    lparam = "DFU_SumAllLotDefects"
    lds = params[:setup] ? $lds_setup : $lds
    tech = (params[:technology] or "32NM")

    folder = lds.folder(@@autocreated_qa)
    folder.delete_channels if folder
    ###ch = folder.spc_channel(:parameter=>param)    
    ###chs_old = (ch == nil) ? 0 : ch.size
     # build and submit DCR
    dcrparams = params.merge({:eqp=>@@eqp, :lot=>@@lot, :department=>"QA", :technology=>tech})
    dcr = Space::DCR.new(dcrparams)    
    dcr.add_parameter(param, @@wafer_values)
    dcr.add_parameter(lparam, {@@lot=>20.0}, :reading_type=>'Lot') if params[:sendlotparam]
    
    res = $client.send_dcr(dcr, :export=>"log/speclimit_#{Time.now.to_i}.xml")
    n_samples = @@wafer_values.size + (params[:sendlotparam] ? 1 : 0)

    if !params[:lsl] and !params[:usl]
      if (params[:setup] and !@@parameters_missingspeclimit_upload_setup_enabled) or
        !@@parameters_missingspeclimit_upload_inline_enabled
        assert $spctest.verify_dcr_accepted(res, 0), "No samples should be uploaded"
        return
      end
    end
    assert $spctest.verify_dcr_accepted(res, n_samples), "DCR not submitted successfully"
          
    #
    # verify spc channels
    ch = folder.spc_channel(:parameter=>param)
    ###assert_equal chs_old+6, ch.samples.size, "wrong number of samples for parameter #{param}, channel #{ch.name}"
    # 
    # ensure spec limits are correct
    ch.samples[-6..-1].each_with_index {|s,i|
      assert $spctest.verify_sample_speclimits(s, params[:lsl], params[:target], params[:usl])
    }
    
    if params[:sendlotparam]
      # verify spc channels
      ch = folder.spc_channel(:parameter=>lparam)
      ###assert_equal chs_old+6, ch.samples.size, "wrong number of samples for parameter #{param}, channel #{ch.name}"
      # 
      # ensure spec limits are correct for lot parameter
      s = ch.samples[-1]
      assert $spctest.verify_sample_speclimits(s, params[:lsl], params[:target], params[:usl])
    end
  end
    
  def send_check_measurement_dcr_multlot(lots, params={})
    param = "DFU_SumAllDefectsOblique"
        
    folder = $lds.folder(@@autocreated_qa)
    folder.delete_channels if folder
    
    ###ch = folder.spc_channel(:parameter=>param)    
    ###chs_old = (ch == nil) ? 0 : ch.size
     # build and submit DCR
    dcrm = Space::DCR.new(:lot=>nil)
    n_samples = 0
    lots.each_with_index do |p, i|
      l,v = p 
      dcrm.add_lot(l, v)
      if v[:wafer]
        dcrm.add_parameter(param, v[:wafer], :ilot=>i)
        n_samples += v[:wafer].size
      else
        dcrm.add_parameter(param, {l=>v[:value]}, :reading_type=>'Lot')
        n_samples += 1
      end
    end
      
    res = $client.send_dcr(dcrm, :export=>"log/speclimit_#{Time.now.to_i}.xml")
    assert $spctest.verify_dcr_accepted(res, n_samples), "DCR not submitted successfully"
          
    #
    # verify spc channels
    ch = folder.spc_channel(:parameter=>param)

    ###assert_equal chs_old+6, ch.samples.size, "wrong number of samples for parameter #{param}, channel #{ch.name}"
    # 
    # ensure spec limits are correct
    lots.each_key do |lot|
      ch.samples(:lot=>lot).each_with_index do |s,i|
        assert $spctest.verify_sample_speclimits(s, lots[lot][:limits][:lsl], lots[lot][:limits][:target], lots[lot][:limits][:usl])
      end
    end  
  end
  
  def send_check_measurement_dcr_multiparam(parameters, params={})
    lds = params[:setup] ? $lds_setup : $lds
    tech = (params[:technology] or "32NM")

    folder = lds.folder(@@autocreated_qa)
    folder.delete_channels if folder
    
    # build and submit DCR
    dcrparams = params.merge({:eqp=>@@eqp, :lot=>@@lot, :department=>"QA", :technology=>tech})
    dcr = Space::DCR.new(dcrparams)    
    dcr.add_parameters(parameters.keys, @@wafer_values)
    
    res = $client.send_dcr(dcr, :export=>"log/speclimit_#{Time.now.to_i}.xml")
    n_samples = parameters.keys.size * @@wafer_values.size


    assert $spctest.verify_dcr_accepted(res, n_samples), "DCR not submitted successfully"
          
    #
    # verify spc channels
    parameters.keys.each do |p|
      ch = folder.spc_channel(:parameter=>p)
      # 
      # ensure spec limits are correct
      ch.samples[-6..-1].each_with_index {|s,i|
        l,t,u = parameters[p]
        assert $spctest.verify_sample_speclimits(s, l, t, u)
      }
    end
  end
end
