=begin 
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.
    
Author: Daniel Steger, 2012-02-09
=end

require_relative 'Test_Space_Setup'


# Test Space/SIL AuoCharting Rules
class Test_Space17 < Test_Space_Setup
  # spec limits defined or not
  @@autocharting_ignorespeccheck = true
  @@mpd = @@autocharting_ignorespeccheck ? 'QA_RULE-CHECK.01' : 'QA_RULE-CHECK.02'

  #Check criticality for template when creating a new channel
  @@templatecheck_criticality_inline_enabled = true
  @@templatecheck_criticality_setup_enabled = true
  #Check alarm rules for template when creating a new channel
  @@templatecheck_alarmrules_inline_enabled = false
  @@templatecheck_alarmrules_setup_enabled = false
  #Check corrective actions for template when creating a new channel
  @@templatecheck_correctiveactions_inline_enabled = true
  @@templatecheck_correctiveactions_setup_enabled = true

  @@ekeys = [Space::SpcApi::SpcCKey.new(4, '*'), Space::SpcApi::SpcCKey.new(5, '*'), Space::SpcApi::SpcCKey.new(6, 'FIXED'), Space::SpcApi::SpcCKey.new(10, 'FIXED')]
  @@template = '_Template_QA_RULE_CHECK'

  
  def test1700_setup
    $setup_ok = false
    #
    [$lds, $lds_setup].each {|lds|
      assert $spctest.templates_exist(lds.folder(@@templates_folder), [@@template]), "Cannot find #{@@template}"
      tmpl = lds.folder(@@templates_folder).spc_channel(@@template)
      tmpl.set_extractor_keys(@@ekeys)
    }
    #
    $setup_ok = true
  end

  #see MSR512888  
  def test1701_business_rules_template_checks
    run_business_rule_test('32NM', :criticality, @@templatecheck_criticality_inline_enabled, "Criticality")
  end
  
  def test1702_business_rules_template_checks
    run_business_rule_test('TEST', :criticality, @@templatecheck_criticality_setup_enabled, "Criticality")
  end
  
  def test1703_business_rules_template_checks
    run_business_rule_test('32NM', :ca, @@templatecheck_correctiveactions_inline_enabled, "No Valid Corrective Action assigned")
  end
  
  def test1704_business_rules_template_checks
    run_business_rule_test('TEST', :ca, @@templatecheck_correctiveactions_setup_enabled, "No Valid Corrective Action assigned")
  end
  
  def test1705_business_rules_template_checks
    run_business_rule_test('32NM', :alarms, @@templatecheck_alarmrules_inline_enabled, "Rule is not assigned")
  end
  
  def test1706_business_rules_template_checks
    run_business_rule_test('TEST', :alarms, @@templatecheck_alarmrules_setup_enabled, "Rule is not assigned")
  end
    
    
  def test1710_exclude_value
    parameters = ['QA_Rule_Param1', 'QA_Rule_Param2']
    tfolder = $lds.folder(@@templates_folder)
    tmpl = tfolder.spc_channel(@@template)
    setup_template(tmpl)
    # none matching exclude criteria
    tmpl.set_extractor_keys(@@ekeys + [Space::SpcApi::SpcCKey.new(7, nil, ['~e*', 'USIL-0001.01'])])
    #
    submit_dcr_verify_successful('32NM', 'SIL-0001.01', parameters, @@template)
    submit_dcr_verify_excluded('32NM', 'eSIL-0001.01', parameters, @@template)
    submit_dcr_verify_excluded('32NM', 'USIL-0001.01', parameters, @@template)
  end
  
  def test1711_include_value
    parameters = ['QA_Rule_Param1', 'QA_Rule_Param2']
    tfolder = $lds.folder(@@templates_folder)
    tmpl = tfolder.spc_channel(@@template)
    setup_template(tmpl)
    # set channel extractor keys, none matching exclude criteria
    tmpl.set_extractor_keys(@@ekeys + [Space::SpcApi::SpcCKey.new(7, nil, ["~!e*", "~!b*"])])
    #
    submit_dcr_verify_successful('32NM', 'eSIL-0001.01', parameters, @@template)
    submit_dcr_verify_excluded('32NM', 'SIL-0001.01', parameters, @@template)
  end
  

  def setup_template(ch, params={})
    ch.set_customer_fields(params[:criticality] ? {'Module'=>'CFM'} : {'Criticality'=>'Key', 'Module'=>'CFM'})
    vals = Space::SpcApi::Channel.all_valuations.find_all {|v| v.vid <= 6}
    vals = vals.drop(1) if params[:alarms]
    ch.set_valuations(vals)
    $spc.set_corrective_actions(ch, params[:ca] ? [] : ['AutoLotHold'])
  end

  def run_business_rule_test(technology, rule, config, msg=nil)
    parameters = ['QA_Rule_Param1', 'QA_Rule_Param2']
    lds = (technology == 'TEST') ? $lds_setup : $lds
    tfolder = lds.folder(@@templates_folder)
    tmpl = tfolder.spc_channel(@@template)
    # remove feature
    setup_template(tmpl, rule=>true)
    $log.info("send data to #{lds.name}")
    if config
      submit_dcr_verify_failed(technology, parameters, msg)      
    else
      submit_dcr_verify_successful(technology, "SIL-0001.01", parameters, @@template)
    end
    # add the feature back
    setup_template(tmpl)
    submit_dcr_verify_successful(technology, "SIL-0001.01", parameters, @@template)
  end 
  
  def submit_dcr_verify_failed(technology, parameters, msg=nil)
    assert_equal 0, $sv.lot_cleanup(@@lot)
    lds = (technology == 'TEST') ? $lds_setup : $lds
    folder = lds.folder(@@autocreated_qa)
    folder.delete_channels if folder
    # build and submit DCR
    dcr = Space::DCR.new(eqp: @@eqp, lot: @@lot, op: @@mpd, department: 'QA', technology: technology)
    dcr.add_parameters(parameters, @@wafer_values)
    assert $spctest.send_dcr_verify(dcr, spcerror: true), "DCR should not have been submitted"
    assert $spctest.verify_notification("SpcException in processing engine", msg), "notification missing"
  end
  
  def submit_dcr_verify_excluded(technology, route, parameters, template)
    assert_equal 0, $sv.lot_cleanup(@@lot)
    lds = (technology == 'TEST') ? $lds_setup : $lds
    folder = lds.folder(@@autocreated_qa)
    folder.delete_channels if folder
    # build and submit DCR
    dcr = Space::DCR.new(eqp: @@eqp, lot: @@lot, op: @@mpd, department: 'QA', technology: technology, route: route)
    dcr.add_parameters(parameters, @@wafer_values)
    assert $spctest.send_dcr_verify(dcr), "DCR should have been submitted"
    #
    # verify spc channels
    folder = lds.folder(@@autocreated_qa)
    parameters.each {|p|
      ch = folder.spc_channel(parameter: p)
      # ref = Space::SpcApi::ChannelData.new(nil, nil, "Auto-created using template: #{template}", p)
      # better:  ref = {desc: "Auto-created using template: #{template}", parameter: p}
      #          assert verify_hash(ch.to_hash, ref, nil, [:chid, :name, :unit, :division]), "wrong channel data"
      ## even better:
      assert_equal "Auto-created using template: #{template}", ch.config.description
      # not necessary as p is search criteria  assert_equal p, ch.parameter
    }
  end
  
  def submit_dcr_verify_successful(technology, route, parameters, template)
    assert_equal 0, $sv.lot_cleanup(@@lot)
    lds = (technology == 'TEST') ? $lds_setup : $lds
    folder = lds.folder(@@autocreated_qa)
    folder.delete_channels if folder
    # build and submit DCR
    dcr = Space::DCR.new(eqp: @@eqp, lot: @@lot, op: @@mpd, department: 'QA', technology: technology, route: route)
    dcr.add_parameters(parameters, @@wafer_values)
    assert $spctest.send_dcr_verify(dcr, nsamples: parameters.size*@@wafer_values.size), "DCR not submitted successfully"
    #
    # verify spc channels
    folder = lds.folder(@@autocreated_qa)
    parameters.each {|p|
      ch = folder.spc_channel(parameter: p)
      ref = Space::SpcApi::ChannelData.new(nil, nil, "Auto-created using template: #{template}", p)
      assert $spctest.verify_channel(ch, ref, dcr, @@wafer_values), "wrong channel data"
      # ensure spec and sample limits are set
      ch.samples.each_with_index {|s,i|
        specs = s.specs
        refute_nil specs["USL"], "no USL for parameter #{p}, sample #{i}"
        refute_nil specs["TARGET"], "no TARGET for parameter #{p}, sample #{i}"
        refute_nil specs["LSL"], "no LSL for parameter #{p}, sample #{i}"
        limits = s.limits
        refute_nil limits["MEAN_VALUE_UCL"], "no limit UCL for parameter #{p}, sample #{i}"
        refute_nil limits["MEAN_VALUE_CENTER"], "no limit CENTER for parameter #{p}, sample #{i}"
        refute_nil limits["MEAN_VALUE_LCL"], "no limit LCL for parameter #{p}, sample #{i}"
      }
    }
  end
end
