=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: U. Koerner, 2012-01-11

History:
  2014-02-05 ssteidte,  separated Test_Space07, code cleanup
=end

require_relative 'Test_Space_Setup'

# Test Space/SIL CAs
class Test_Space070 < Test_Space_Setup
  @@mpd_qa = "QA-MB-FTDEPM-LIS-CAEX.01"

  def setup
    super
    return unless $lds && self.class.class_variable_defined?(:@@lot) # bypass test0000_setup
    assert_equal 0, $sv.eqp_cleanup(@@eqp)
    assert_equal 0, $sv.lot_cleanup(@@lot)
    assert_equal 0, $sv.inhibit_cancel(:route, $sv.lot_info(@@lot).route)
    #
    $lds.all_spc_channels('QA_PARAM_90*').each {|ch| assert ch.delete}
    folder = $lds.folder(@@autocreated_qa)
    assert folder.delete_channels, "error cleaning channels" if folder
  end

  def teardown
    $log.info "\nteardown"
    super
    $sv.merge_lot_family(@@lot)
  end

  def test0700_setup
    $setup_ok = false
    #
    assert @@ppd_qa = $sildb.pd_reference(mpd: @@mpd_qa).first, "no PD reference found for #{@@mpd_qa}"
    #
    ['LotHold', 'AutoLotHold', 'ReleaseLotHold', 'CancelLotHold'].each {|action|
      assert $spc.corrective_action(action), "CA #{action.inspect} not defined"
    }
    $setup_ok = true
  end

  def test0701_hold
    assert_equal 0, $sv.lot_opelocate(@@lot, 'first')
    channels = create_clean_channels
    assert $spctest.assign_verify_cas(channels, ['LotHold', 'AutoLotHold', 'ReleaseLotHold', 'CancelLotHold'])
    #
    submit_process_dcr(@@ppd_qa, wafer_values: @@wafer_values2)
    # send DCR with OOC values
    dcr = Space::DCR.new(lot: @@lot, eqp: @@mtool, op: @@mpd_qa)
    dcr.add_parameters(@@parameters, @@wafer_values2ooc)
    res = $client.send_dcr(dcr, export: :caller)
    assert $spctest.verify_dcr_accepted(res, @@parameters.count*@@wafer_values2ooc.size, nooc: 'all*2'), "DCR not submitted successfully"
    #assert $spctest.verify_aqv_message(dcr: dcr, lot: @@lot, wafer: wafer_values.keys.sort, result: "fail"), "AQV message not as expected"
    #
    # verify futurehold from AutoLotHold
    assert $spctest.verify_futurehold2(@@lot, "[(Raw above specification)", dcr_lot: dcr.find_lot_context(@@lot)), "lot must have a future hold"
    # verify comment is set
    assert $spctest.verify_ecomment(channels, "LOT_HELD: #{@@lot}: reason={X-S")
    # cancel LotHold from automatic action
    channels.each {|ch|
      s = ch.samples(ckc: true)[-1]
      assert s.assign_ca($spc.corrective_action('CancelLotHold')), "error assigning corrective action"
    }
    s = channels[0].samples(ckc: true)[-1]
    sleep 30
    assert $spctest.verify_futurehold_cancel(@@lot, "[(Raw above specification)"), "lot must have no future hold"
    #
    # LotHold (manual action)
    assert s.assign_ca($spc.corrective_action('LotHold')), "error assigning corective action"
    assert $spctest.verify_futurehold2(@@lot, '', dcr_lot: dcr.find_lot_context(@@lot), parameter: s.parameter), "lot must have a future hold"
    #
    # CancelLotHold (from manual action)
    assert s.assign_ca($spc.corrective_action('CancelLotHold')), "error assigning corective action"
    assert $spctest.verify_futurehold_cancel(@@lot, "[(Raw above specification)"), "lot must have no future hold"
    #
    # LotHold (manual action, prepare release)
    assert s.assign_ca($spc.corrective_action('LotHold')), "error assigning corective action"
    assert $spctest.verify_futurehold2(@@lot, '', dcr_lot: dcr.find_lot_context(@@lot), parameter: s.parameter), "lot must have a future hold"
    #
    # ReleaseLotHold
    assert_equal 0, $sv.lot_gatepass(@@lot), "SiView GatePass error"
    assert $spctest.verify_futurehold_cancel(@@lot, "[(Raw above specification)"), "lot must have no future hold"
    lotholdlistsize = $sv.lot_hold_list(@@lot, type: 'FutureHold').size
    assert $sv.lot_hold_list(@@lot, type: 'FutureHold').size > 0, "lot must be on hold"
    #
    assert s.assign_ca($spc.corrective_action('ReleaseLotHold')), "error assigning corective action"
    sleep 60
    assert $sv.lot_hold_list(@@lot, type: 'FutureHold').size < lotholdlistsize, "lot must not be on hold"
  end

  def test0702_hold_multiple_lots_all_ooc
    second_lot = "SIL007.00"
    assert_equal 0, $sv.lot_cleanup(second_lot)

    channels = create_clean_channels
    assert $spctest.assign_verify_cas(channels, ['AutoLotHold'])
    #
    wv1 = Hash[@@wafer_values2.keys[0..2].collect {|e| [e, @@wafer_values2[e]+100]}]
    wv2 = Hash[@@wafer_values2.keys[3..5].collect {|e| [e, @@wafer_values2[e]+100]}]
    #
    submit_process_dcr(@@ppd_qa, wafer_values: wv1, addlots: [second_lot], addwafer_values: [wv2])
    # create and send DCR, each lot with ooc
    dcr = Space::DCR.new(lot: @@lot, eqp: @@mtool, op: @@mpd_qa)
    dcr.add_parameters(@@parameters, wv1)
    dcr.add_lot(second_lot, eqp: @@mtool, op: @@mpd_qa)
    dcr.add_parameters(@@parameters, wv2)
    res = $client.send_dcr(dcr, export: :caller)
    assert $spctest.verify_dcr_accepted(res, @@parameters.size*(wv1.size+wv2.size), nooc: @@parameters.size*(wv1.size+wv2.size)*2), "DCR not submitted successfully"
    #
    assert $spctest.verify_futurehold2(@@lot, "[(Raw above specification)"), "lot must have a future hold"
    assert $spctest.verify_futurehold2(second_lot, "[(Raw above specification)"), "lot must have a future hold"
  end

  def test0703_hold_multiple_lots_one_ooc
    second_lot = "SIL007.00"
    assert_equal 0, $sv.lot_cleanup(second_lot)

    channels = create_clean_channels
    assert $spctest.assign_verify_cas(channels, ['AutoLotHold'])
    #
    submit_process_dcr(@@ppd_qa, wafer_values: @@wafer_values2)
    # send dcr - each lot with ooc
    wv1 = Hash[@@wafer_values2.keys.slice(0..2).collect {|e|[e,@@wafer_values2[e]+100]}]
    wv2 = Hash[@@wafer_values2.keys.slice(3..5).collect {|e|[e.gsub("006","007"),@@wafer_values2[e]]}]
    dcr = Space::DCR.new(lot: @@lot, eqp: @@mtool, op: @@mpd_qa)
    dcr.add_parameters(@@parameters, wv1)
    dcr.add_lot(second_lot, op: @@mpd_qa)
    dcr.add_parameters(@@parameters, wv2)
    res = $client.send_dcr(dcr, export: :caller)
    assert $spctest.verify_dcr_accepted(res, @@parameters.size*(wv1.size+wv2.size), nooc: @@parameters.size*wv1.size*2), "DCR not submitted successfully"
    #
    assert $spctest.verify_futurehold2(@@lot, "[(Raw above specification)"), "lot must have a future hold"
    assert $spctest.verify_futurehold_cancel(second_lot, "[(Raw above specification)"), "lot #{second_lot} must have no future hold"
  end

  def test0704_hold_multiple_lots_all_ooc
    second_lot = "SIL007.00"
    assert_equal 0, $sv.lot_cleanup(second_lot)

    channels = create_clean_channels
    assert $spctest.assign_verify_cas(channels, ['AutoLotHold'])
    
    wv1 = Hash[*@@wafer_values2.keys.slice(0..2).collect {|e|[e,@@wafer_values2[e]+100]}.flatten]
    wv2 = Hash[*@@wafer_values2.keys.slice(3..5).collect {|e|[e.gsub("006","007"),@@wafer_values2[e]+100]}.flatten]

    submit_process_dcr(@@ppd_qa, wafer_values: wv1, addlots: [second_lot], addwafer_values: [wv2])
    # create and send DCR, each lot with ooc
    dcr = Space::DCR.new(lot: @@lot, eqp: @@mtool, op: @@mpd_qa)
    dcr.add_parameters(@@parameters, wv1)
    dcr.add_lot(second_lot, eqp: @@mtool, op: @@mpd_qa)
    dcr.add_parameters(@@parameters, wv2)
    res = $client.send_dcr(dcr, export: :caller)
    assert $spctest.verify_dcr_accepted(res, @@parameters.size*(wv1.size+wv2.size), nooc: @@parameters.size*(wv1.size+wv2.size)*2), "DCR not submitted successfully"
    #
    assert $spctest.verify_futurehold2(@@lot, "[(Raw above specification)"), "lot must have a future hold"
    assert $spctest.verify_futurehold2(second_lot, "[(Raw above specification)"), "lot must have a future hold"
  end

  def test0705_some_different_ca_with_hold
    route = $sv.lot_info(@@lot).route
    assert_equal 0, $sv.inhibit_cancel(:route, route)

    channels = create_clean_channels
    # set different corrective actions 
    ch1 = channels.select {|e| channels.index(e).odd?}
    ch2 = channels.select {|e| channels.index(e).even?}
    ch3 = [ch2.delete_at(1)]
    assert $spctest.assign_verify_cas(ch1, 'AutoEquipmentInhibit')
    assert $spctest.assign_verify_cas(ch2, 'AutoRouteInhibit')
    assert $spctest.assign_verify_cas(ch3, 'AutoLotHold')
    #
    submit_process_dcr(@@ppd_qa, wafer_values: @@wafer_values2)
    # send DCR with OOC values
    dcr = Space::DCR.new(lot: @@lot, eqp: @@mtool, op: @@mpd_qa, route: route)
    dcr.add_parameters(@@parameters, @@wafer_values2ooc)
    res = $client.send_dcr(dcr, export: :caller)
    assert $spctest.verify_dcr_accepted(res, @@parameters.size*@@wafer_values2ooc.size, nooc: 'all*2'), "DCR not submitted successfully"
    #
    assert $spctest.verify_futurehold2(@@lot, ''), "lot must have a future hold"
    assert $spctest.verify_inhibit(route, ch2.size, class: :route), "route must have an inhibit"
    assert $spctest.verify_inhibit(@@eqp, ch1.size), "equipment must have an inhibit"
    assert $spctest.verify_ecomment(ch1, "TOOL_HELD: #{@@eqp}: reason={X-S")
    assert $spctest.verify_ecomment(ch2, "ROUTE_HELD:#{route}: reason={X-S")
    assert $spctest.verify_ecomment(ch3, "LOT_HELD: #{@@lot}: reason={X-S")
  end
  
  def test0706_some_different_ca_multiple_referenced_pds
    lot = 'SIL' + @@sil_version.split('.').collect {|e| '%02d' % e}.join('') + '%02d' % @@test_run + '1' + '.000'
    assert lot = $svtest.new_lot(lot: lot) unless $sv.lot_exists?(lot)
    $log.info "using lot #{lot.inspect} as second lot"
    #
    mpd = "QA-MB-FTDEPM.01"
    mtool = "QAMeas"
    wafer_values = {"#{lot}01"=>41, "#{lot}02"=>42, "#{lot}03"=>43, "#{lot}04"=>44, "#{lot}05"=>45, "#{lot}06"=>46}
    chambers = ["CHA", "CHB", "CHC"]
    lrs = ["C-P-4710B-MGT-NMETDEPDR.01", "C-P-4710B-MGT-NMETDEPDR.02", "C-P-4710B-MGT-NMETDEPDR.03"]
    #
    # get process PDs referenced by MPD
    ppds = $sildb.pd_reference(mpd: mpd)[0..2]
    assert_equal ppds.size, ppds.compact.size, "no PD reference found for #{mpd}"
    $log.info "using process PDs #{ppds.inspect}"
    #
    # build and submit DCRs for 3 process PDs
    ppds.each_with_index {|ppd, i|
      dcrp = Space::DCR.new(eqp: "#{@@eqp}_#{i}", eqptype: 'PROCESS-CHAMBER', lot: lot, op: ppd, logical_recipe: lrs[i])
      dcrp.add_parameters(@@parameters, wafer_values, processing_areas: chambers[i], nocharting: true)
      res = $client.send_dcr(dcrp, export: "log/#{__method__}_p_#{i}.xml")
      assert $spctest.verify_dcr_accepted(res, 0), "DCR not submitted successfully"
    }
    #
    assert_equal 0, $sv.lot_cleanup(lot)
    # build and submit DCR for related MPD
    dcr = Space::DCR.new(eqp: mtool, lot: lot, op: mpd)
    dcr.add_parameters(@@parameters, wafer_values, processing_areas: "CHC")
    res = $client.send_dcr(dcr, export: :caller)
    assert $spctest.verify_dcr_accepted(res, @@parameters.size * wafer_values.size), "DCR not submitted successfully"
    #
    # verify sample extractor keys
    folder = $lds.folder(@@autocreated_qa)
    @@parameters.each {|p|
      ch = folder.spc_channel(parameter: p)
      samples = ch.samples
      assert_equal wafer_values.size, samples.size, "wrong number of samples in #{ch.inspect}"
      samples.each {|s|
        assert $spctest.verify_data(s.extractor_keys, {"POperationID"=>ppds[0], "PChamber"=>chambers[0], "PTool"=>"#{@@eqp}_0", "PLogRecipe"=>lrs[0]})
      }
    }
  end

  def test0707_hold_childlots_all_ooc_LOT_parameters
    parameters = @@parameters.collect{|p| p + '_LOT'}
    channels = create_clean_channels(parameters: parameters, nsamples: 5, space_listening: true)
    assert $spctest.assign_verify_cas(channels, ['AutoLotHold'])

    #
    childlots = 5.times.collect {$sv.lot_split(@@lot, 1)}.compact
    assert_equal 5, childlots.size, "error splitting lot #{@lot}"
    #
    wafervalues_lot = Hash[20.times.collect {|i| ["Waf%4.4d" % i, 800+i]}]
    wafervalues_childlots = [{"Waf0020"=>300}, {"Waf0021"=>400}, {"Waf0022"=>500}, {"Waf0023"=>600}, {"Waf0024"=>700}]
    
    submit_process_dcr(@@ppd_qa, wafer_values: wafervalues_lot, addlots: childlots, addwafer_values: wafervalues_childlots)
    # create and send DCR, each lot with ooc
    dcr = Space::DCR.new(lot: @@lot, eqp: @@mtool, op: @@mpd_qa)
    dcr.add_parameters(parameters, wafervalues_lot)
    childlots.each_with_index {|l, i|
      dcr.add_lot(l, eqp: @@mtool, op: @@mpd_qa) #, route: "RP-Test" for rework route test
      dcr.add_parameters(parameters, wafervalues_childlots[i])
    }
    res = $client.send_dcr(dcr, export: :caller)
    #
    ([@@lot] + childlots).each {|l| 
      assert $spctest.verify_futurehold2(l, "[(Raw above specification)"), "lot must have a future hold"
    }
  end

  def test0708_hold_childlots_all_ooc_LOT_grouping
    channels = create_clean_channels
    assert $spctest.assign_verify_cas(channels, ['AutoLotHold'])

    childlots = 5.times.collect {$sv.lot_split(@@lot, 1)}.compact
    assert_equal 5, childlots.size, "error splitting lot #{@lot}"
    #
    wafervalues_lot = Hash[20.times.collect {|i| ["Waf%4.4d" % i, 800+i]}]
    wafervalues_childlots = [{"Waf0020"=>300}, {"Waf0021"=>400}, {"Waf0022"=>500}, {"Waf0023"=>600}, {"Waf0024"=>700}]
    
    submit_process_dcr(@@ppd_qa, wafer_values: wafervalues_lot, addlots: childlots, addwafer_values: wafervalues_childlots)
    # create and send DCRm each lot with ooc
    dcr = Space::DCR.new(lot: @@lot, eqp: @@mtool, op: @@mpd_qa)
    dcr.add_parameters(@@parameters, {@@lot=>221.0}, reading_type: 'Lot')
    childlots.each_with_index {|l,i|
      dcr.add_lot(l, eqp: @@mtool, op: @@mpd_qa)
      dcr.add_parameters(@@parameters, {l=>222.0 + i}, reading_type: 'Lot')
    }
    res = $client.send_dcr(dcr, export: :caller)
    #
    ([@@lot] + childlots).each {|l| 
      assert $spctest.verify_futurehold2(l, "[(Raw above specification)"), "lot must have a future hold"
    }
  end

  def test0709_hold_childlots_all_ooc_LOT_parameters_grouping
    channels = create_clean_channels
    assert $spctest.assign_verify_cas(channels, ['AutoLotHold'])

    parameters = @@parameters.collect {|p| p + '_LOT'}
    childlots = 5.times.collect {$sv.lot_split(@@lot, 1)}.compact
    assert_equal 5, childlots.size, "error splitting lot #{@lot}"
    #
    wafervalues_lot = Hash[20.times.collect {|i| ["Waf%4.4d" % i, 800+i]}]
    wafervalues_childlots = [{"Waf0020"=>300}, {"Waf0021"=>400}, {"Waf0022"=>500}, {"Waf0023"=>600}, {"Waf0024"=>700}]
    
    submit_process_dcr(@@ppd_qa, wafer_values: wafervalues_lot, addlots: childlots, addwafer_values: wafervalues_childlots)
    # create and send DCR, each lot with ooc
    dcr = Space::DCR.new(lot: @@lot, eqp: @@mtool, op: @@mpd_qa)
    dcr.add_parameters(parameters, {@@lot=>221.0}, reading_type: 'Lot')
    childlots.each_with_index {|l, i|
      dcr.add_lot(l, eqp: @@mtool, op: @@mpd_qa)
      dcr.add_parameters(parameters, {l=>222.0 + i}, reading_type: 'Lot')
    }
    res = $client.send_dcr(dcr, export: :caller)
    #
    ([@@lot] + childlots).each {|l| 
      assert $spctest.verify_futurehold2(l, "[(Raw above specification)"), "lot must have a future hold"
    }
  end
  
end
