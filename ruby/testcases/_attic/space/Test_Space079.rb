=begin
Test Space/SIL.

(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: D.Steger, 2012-04-19
=end

require_relative 'Test_Space_Setup'

# Test Space/SIL.

class Test_Space079 < Test_Space_Setup

  @@values = [20, 14, 22, 23, 14, 15]
  @@add_lots = ["SIL005.00", "SIL006.00", "SIL007.00"]
  #@@mpd_qa = "QA-MGT-NMETDEPDR.QA"
	@@mpd_qa = "QA-MB-FTDEPM.01"
  #@@mpd_qa = "QA-CLAIM-MEMO.01"
  @@chambers = {"PM1"=>5, "PM2"=>6, "CHX"=>7}
  @@mtool = "UTF001"
  @@product = "SILPROD.01"
  @@productgroup = "QAPG1"
  @@param_depts = {"QA_MEMO_900"=>"LIT", "QA_MEMO_901"=>"TFM", "QA_MEMO_902"=>"QA", "QA_MEMO_903"=>"ETC", "QA_MEMO_904"=>"QA"}
  @@proc_ope = "1000.6200"
  @@holdreason = "ENG"

  # send dcr with non-ooc values to setup channels
  def _create_chan(parameters, dpt="QA", params={})
    #
    $lds.all_spc_channels("QA_MEMO_9*").each {|c| c.delete }

    #
    chambers = (params[:chambers] or @@chambers)
    mtool = (params[:mtool] or @@mtool)
    mpd = (params[:mpd] or @@mpd_qa)#
    #
    submit_process_dcr(@@ppd_qa, :lot=>@@lot, :wafer_values=>@@wafer_values2, :chambers=>chambers)

    dcr = Space::DCR.new(:lot=>@@lot, :eqp=>mtool, :department=>dpt, :op=>mpd, :product=>@@product, :productgroup=>@@productgroup)
    dcr.add_parameters(parameters, @@wafer_values2)
    res = $client.send_dcr(dcr, :export=>"log/#{__method__}_m.xml")
    assert $spctest.verify_dcr_accepted(res, parameters.size*@@wafer_values2.size), "DCR not submitted successfully"
    #
    channels = []
    @@param_depts.each_pair do |p, dept|
      ch = $lds.folder("AutoCreated_#{dept}").spc_channel(:parameter=>p)
      assert ch, "no channel found for #{p} in #{dept}"
      channels << ch
    end

    return channels
  end

  def test0790_setup
    $setup_ok = false

    if $env == "f8stag"
      @@mpd_qa = "QA-CLAIM-MEMO.01"
      assert $spctest.verify_speclimit(:specid=>"M14-00020003", :specversion=>7, :all_active=>true), "Speclimits do not exist"
    end
    #
    @@ppd_qa = $sildb.pd_reference(:mpd=>@@mpd_qa).first
    refute_nil @@ppd_qa, "no PD reference found for #{@@mpd_qa}"

    @@wafer_values2 = Hash.new
    $sv.lot_info(@@lot,:wafers=>true).wafers[0..5].each_with_index do |w,i|
      @@wafer_values2[w.wafer] = @@values[i]
    end

    $setup_ok = true
  end

  def test0791_batchhold
    parameters = ["QA_MEMO_900", "QA_MEMO_901", "QA_MEMO_902", "QA_MEMO_903", "QA_MEMO_904"]
    #
    channels = _create_chan( parameters, "QA" )
    #
    assert_equal 0, $sv.lot_cleanup(@@lot)
    @@add_lots.each {|l| assert_equal 0, $sv.lot_cleanup(l)}

    _run_scenario(@@add_lots, channels)
  end

  def test0792_batchhold_otherlot
    _run_scenario(@@add_lots, true, false)
  end

  def test0793_batchhold_splitlot
    _run_scenario(@@add_lots, false, true)
  end

  def _cleanup_lotfamily(lot)
    $sv.lot_family(lot).each do |l|
      l_info =  $sv.lot_info(l)
      l_status = l_info.status
      if l_status == "COMPLETED"
        $sv.lot_bankin_cancel(l)
        $sv.lot_opelocate l, "first"
      elsif l_status == "Waiting"
        $sv.lot_opelocate l, "first"
      elsif l_status == "Processing"
        $sv.eqp_opestart_cancel(l_info.eqp, l_info.cj)
        port = $sv.eqp_info(l_info.eqp).ports.find {|p| p.loaded_carrier == l_info.carrier}
        $sv.eqp_unload(l_info.eqp, port.port, l_info.carrier)
        $sv.lot_opelocate l, "first"
      end
    end
    assert $sv.merge_lot_family(lot), "Prepare lot failed"
  end

  def _run_scenario(add_lots, split_before=false, split_after_process=false)

    excluded_lots = []
    _cleanup_lotfamily(add_lots[-1])

    parameters = ["QA_MEMO_900", "QA_MEMO_901", "QA_MEMO_902", "QA_MEMO_903", "QA_MEMO_904"]
    #
    channels = _create_chan( parameters, "QA" )
    #
    assert_equal 0, $sv.lot_cleanup(@@lot)
    @@add_lots.each {|l| assert_equal 0, $sv.lot_cleanup(l)}

    if split_before
      other_lot = $sv.lot_split(add_lots[-1], 1)
      $log.info "Created lot not in batch #{other_lot}"
    end

    #
    # ensure corrective actions are set
    actions = ["AutoLotHold", "AutoBatchLotHold", "ReleaseLotHold", "ReleaseBatchLotHold"]
    channels.each{|ch|
      $spc.set_corrective_actions(ch, actions)
      vals = ch.valuations
      assert vals.size > 0, "no channel valuations defined, check channel and template"
      aa = $spc.corrective_actions(vals[0]).collect {|a| a.name}.sort
      assert_equal actions.sort, aa, "error setting corrective actions"
    }
    $log.info "#{actions.inspect} set correctly"

    wafer_values = add_lots.collect {|l| Hash[*[$sv.lot_info(l,:wafers=>true).wafers.collect{|w| [w.wafer, 10]}]]}
    $log.info "#{wafer_values.inspect}"

    #
    submit_process_dcr(@@ppd_qa, :wafer_values=>@@wafer_values2, :chambers=>@@chambers,
      :addlots=>add_lots, :addwafer_values=>wafer_values)


    if split_after_process
      lot = add_lots[-1]
      #Waiting
      new_lot_1 = $sv.lot_split(lot, 1)
      add_lots.push(new_lot_1)
      #Completed
      new_lot_2 = $sv.lot_split(lot, 1)
      assert $sv.lot_opelocate(new_lot_2, "last")
      excluded_lots.push(new_lot_2)
      #Processing
      new_lot_3 = $sv.lot_split(lot, 1)
      $sv.lot_opelocate(new_lot_3, @@proc_ope)
      assert $sv.claim_process_lot(new_lot_3, :nounload=>true, :noopecomp=>true, :mode=>"Off-Line-1", :notify=>false)
      add_lots.push(new_lot_3)
      $log.info "Created lot in batch #{new_lot_1}, #{new_lot_2}, #{new_lot_3}"
    end

    # send DCR with OOC values
    wafer_values = Hash[*@@wafer_values2.collect {|w,v| [w,v+100]}.flatten]
    dcr = Space::DCR.new(:lot=>@@lot, :eqp=>@@mtool, :op=>@@mpd_qa, :product=>@@product, :productgroup=>@@productgroup)
    dcr.add_parameters(parameters, wafer_values)
    res = $client.send_dcr(dcr, :export=>"log/#{__method__}.xml")

    assert $spctest.verify_dcr_accepted(res, parameters.count*wafer_values.size, :nooc=>parameters.count*wafer_values.size*2), "DCR not submitted successfully"
    params_speclimits = {}
    parameters.each {|p| params_speclimits[p] = [10.0,20.0,30.0]}
    assert $spctest.verify_dcr_speclimits(res, params_speclimits)
    #
    # verify futurehold from AutoLotBatchHold
    assert $spctest.verify_futurehold2(@@lot, "BATCH_LOTS->#{add_lots.join(',')}", :dcr_lot=>dcr.find_lot_context(@@lot)), "lot must have a future hold"
    # verify futureholds from AutoLotHold
    assert $spctest.verify_futurehold(@@lot, "[(Raw above specification", nil, :expected_holds=>parameters.count)
    # verify no lot hold for other lot
    assert $spctest.verify_futurehold_cancel(other_lot, nil), "Expected no hold for #{other_lot}, because it is not in the batch" if other_lot

    # check batch holds
    add_lots.each do |lot|
      l_status = $sv.lot_info(lot).status
      if ["Processing"].member?(l_status)
        assert $spctest.verify_futurehold2(lot, "MONITORING_LOT->#{@@lot}", :dcr_lot=>dcr.find_lot_context(@@lot)), "lot must have a future hold"
        l_info = $sv.lot_info(lot)
        $sv.eqp_data_speccheck(l_info.eqp, l_info.cj)
        $sv.eqp_opecomp(l_info.eqp, l_info.cj)
        port = $sv.eqp_info(l_info.eqp).ports.find {|p| p.loaded_carrier == l_info.carrier}
        $sv.eqp_unload(l_info.eqp, port.port, l_info.carrier)
      elsif ["Waiting", "ONHOLD","NonProBank"].member?(l_status)
        assert $spctest.verify_lothold(lot, "MONITORING_LOT->#{@@lot}", :dcr_lot=>dcr.find_lot_context(@@lot)), "lot must be on hold"
      else
        $log.error "no check for #{lot}"
      end
    end

    excluded_lots.each do |lot|
      assert_equal(parameters.count, $spctest.verify_notification("", "Batch cannot be placed on Hold because of their current state:", :raw=>true).count, "no notifcation found for #{lot}")
      $log.warn "Check for no SiView error (lot is emptied, scrapped or shipped"
    end

    assert_equal 0, $sv.lot_gatepass(@@lot), "SiView GatePass error"
    assert $spctest.verify_futurehold_cancel(@@lot, nil, :timeout=>@@ca_time_out), "lot must have no future hold"
    assert $sv.lot_hold_list(@@lot, :type=>"FutureHold").size > 0, "lot must be on hold"

    sample_comments = {}
    #
    # ReleaseLotHold
    channels.each{|ch|
      s = ch.samples(:ckc=>true)[-1]
      sample_comments[s.sid] = "ReleaseHold #{s.sid}"
      #
      # CancelLotHold (from automatic action)
      assert s.assign_ca($spc.corrective_action("ReleaseLotHold"), :comment=>sample_comments[s.sid]), "error assigning corective action"
    }
    sleep 60
    assert $spctest.verify_sapphire_messages(sample_comments)
    assert $spctest.verify_lothold_released(@@lot, nil, :timeout=>@@ca_time_out), "no further holds expected"

    # check batch holds are still present
    add_lots.each do |lot|
      assert $spctest.verify_lothold(lot, "MONITORING_LOT->#{@@lot}"), "lot must still be onhold"
    end

    # take one sample
    s = channels[-1].samples(:ckc=>true)[-1]
    assert s.assign_ca($spc.corrective_action("ReleaseBatchLotHold"), :comment=>sample_comments[s.sid]), "error assigning corective action"

    add_lots.each do |lot|
      assert $spctest.verify_lothold_released(lot, nil, :timeout=>@@ca_time_out), "no further holds expected"
    end
  end

end
