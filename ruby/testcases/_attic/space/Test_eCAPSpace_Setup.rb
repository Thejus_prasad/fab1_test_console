=begin 
Test Space/SIL.

(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.
    
Author: Steffen Steidten, 2011-11-25
=end

require 'RubyTestCase'
require 'spacetest'
require 'misc'

# Test Space/SIL/eCAP.

class Test_eCAPSpace_Setup < RubyTestCase
  Description = 'Test Space/SIL/eCAP'

  @@test_run = 15
	
	@@sil_carriers_to_exclude = ['SIL000', 'SIL001', 'SIL002']

  @@lots = {}
  @@lot = 'SIL006.00'
  @@lots['default'] = @@lot
  
  @@eqp = 'UTCSIL01'
  @@mtool = 'UTFSIL01'
  #According to the TDM schema, the following equipment types can be used in the DCR equipment tag attribute type
  #METROLOGY and RETICLE will not store the wafer history for processing areas and sub processing areas. For details please refer to MSR529229
  
  @@parameters = ['QA_eCAP_900', 'QA_eCAP_901', 'QA_eCAP_902', 'QA_eCAP_903', 'QA_eCAP_904']
  @@wafer_values = {
    '250UF500EC'=>40, 
    '250UF501EC'=>41, 
    '250UF502EC'=>42,
    '250UF503EC'=>43,
    '250UF504EC'=>44,
    '250UF505EC'=>45
    }
  @@chambers = {'CHA'=>5, 'CHB'=>6, 'CHC'=>7}
  @@lds_inline = 'Inline_Fab1'
  @@lds_setup = 'Setup_Fab1'

  @@templates_folder = '_TEMPLATES'
  @@default_template = '_DEFAULT_TEMPLATE'
  @@default_template_qa = 'DEFAULT_QA_TEMPLATE'
  @@params_template = '_Template_QA_PARAMS_900'
 
  @@module_qa = 'QA'
  @@module_lit = 'LIT'
  @@module_cfm = 'CFM'
  @@module_nodep = 'NODEP'
  
  @@autocreated_qa = "AutoCreated_#{@@module_qa}"
  @@autocreated_nodep = "AutoCreated_#{@@module_nodep}"
  @@autocreated_lit = "AutoCreated_#{@@module_lit}"
  @@autocreated_cfm = "AutoCreated_#{@@module_cfm}"
  
  @@spc_hold_reasons = %w(X-S1 X-S2 X-S3 X-S4 X-S5 X-S6 X-S7 X-S8 X-S9)
  
  # create carriers and lots
  @@lot_mask = 'UFE'
  @@carrier_mask = 'UFE'
  @@carrier_count = 100

  # timeout for CA check
  @@ca_time_out = 120

	# wait for derdack
  @@derdack_sleep = 300

  def test0000_setup
    $setup_ok = false
    #
    ($spc.close; $spctest = nil) if $spc and $spc.env != $env
    $spctest ||= SpaceTest.new($env, :sv=>$sv)
    $spc = $spctest.spc
    $sildb = $spctest.sildb
    $client = $spctest.client
    $sv = $spctest.sv
    assert ($spc and $spc.env == $env)
    # 
    @@spc_hold_reasons << 'X-SA' unless ($env == 'f8stag')
    #
    $lds = $spc.lds(@@lds_inline)
    $lds_setup = $spc.lds(@@lds_setup)
    #
    # find or create test lots and carriers
    lot = @@lot_mask + '-' + 'DFLT' + '_%02d' % @@test_run + '.00'
    @@lots['default'] = lot
    lot = @@lot_mask + '-' + 'OOS' + '_%02d' % @@test_run + '.00'
    @@lots['oos'] = lot
    lot = @@lot_mask + '-' + 'MOOC' + '_%02d' % @@test_run + '.00'
    @@lots['mooc'] = lot
    lot = @@lot_mask + '-' + 'SOOC' + '_%02d' % @@test_run + '.00'
    @@lots['sooc'] = lot
    @@lots.each_value {|l|
      unless $sv.lot_exists?(l)
        # ensure empty carriers exist
        c = $sv._get_empty_carrier("#{@@carrier_mask}%")
        assert c, "no empty #{@@carrier_mask}% carrier found"
        @@lot = $sv.new_lot_release(:lot=>l, :carrier=>c, :template=>'SIL006.00', :waferids=>'t7m12', :stb=>true) 
      end
      assert @@lot, 'unable to find or create a test lot'
    }
    #
    $log.info '* * * * * * * * * * * * * * * * * * * * * * *'
    $log.info '* testing eCAP'
    $log.info "* using lots #{@@lots.inspect}"
    $log.info '*   obtain new lots by changing @@test_run'
    $log.info '* * * * * * * * * * * * * * * * * * * * * * *'
    #
    $setup_ok = true
  end
  
  def test0001_LDS
    $setup_ok = false
    assert_not_nil $lds, "LDS #{@@lds_inline} not found"
    assert_not_nil $lds_setup, "LDS #{@@lds_setup} not found"
    $setup_ok = true
  end
  
  def test9999_Derdack
    if $spctest.derdack.db.connected?
      $log.info 'Derdack notification has been tested'
    else
      $log.warn 'Derdack notification is not tested because of missing DB connection' 
    end
  end
  
  
  # submit & check a process dcr (standard parameters in nocharting only if not specified in params)
  def submit_process_dcr(ppd, params={})
    $log.info "using process PD #{ppd}"
    #
    wafer_values = (params[:wafer_values] or Hash[*@@wafer_values.collect {|w,v| [w, v+12]}.flatten])
    parameters = (params[:parameters] or @@parameters)
    chambers =  (params[:chambers] or @@chambers)
    export = (params[:export] or "log/#{__method__}_p.xml")
    lot = (params[:lot] or @@lot)
    eqp = (params[:eqp] or @@eqp)
    nocharting = !(params[:nocharting] == false)
    expected_samples = ((params[:expected_samples]) or 0)
    #
    # build DCR for process PD
    dcrp = Space::DCR.new({:eqp=>eqp, :lot=>lot, :op=>ppd}.merge(params))
    dcrp.add_parameters(parameters, wafer_values, {:nocharting=>nocharting}.merge(params))
    # additional lots and wafers
    if params[:addlots] and params[:addwafer_values]
      params[:addlots].each_with_index do |l, i|
        dcrp.add_lot(l, {:eqp=>eqp, :op=>ppd}.merge(params))
        wafer_values_add = Hash[*params[:addwafer_values][i].collect {|w,v| [w, v+12]}.flatten]
        dcrp.add_parameters(parameters, wafer_values_add, {:nocharting=>nocharting}.merge(params))
      end
    end
    # chambers
    dcrp.context[:Equipment][:ProcessingArea]=[]
    chambers.each { |c,l| dcrp.add_eqp_processing_area(c, l)}
    #
    # submit DCR
    res = $client.send_dcr(dcrp, :export=>export)
    $log.info "Result (Process): #{res.inspect}"
    #
    # verify in SPACE
    assert $spctest.verify_dcr_accepted(res, expected_samples), "DCR #{export} not submitted successfully"
    return dcrp
  end
  
	def tc_number(method)
		tc = method.to_s.sub('test', '')
		tc_ind = tc.index('_') - 1
		tc = tc[0..tc_ind]
		return tc
	end
	
	def cleanup_all_sil_carriers_but_excluded
		cl = $sv.carrier_list(:carrier=>'UFE%', :status=>'AVAILABLE', :notempty=>true)
		cl = cl - @@sil_carriers_to_exclude
		ll = Array.new
		cl.each {|carrier| $sv.carrier_status(carrier).lots.each {|l| ll << l}}
		ll.each {|lot| $sv.delete_lot_family(lot)}
	end
  
  def self.cleanup_channels(department='CFM')
    folder = $lds.folder("AutoCreated_#{department}")
    folder.delete_channels
    folder.delete_channels(:deleted=>true)
  end
end
