=begin 
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.
    
Author: Steffen Steidten, 2011-11-25
=end

require_relative 'Test_Space_Setup'

# Test Space/SIL.
class Test_Space13 < Test_Space_Setup
  @@mtool = 'QAMeas'

  def setup
    super
    return unless $lds && self.class.class_variable_defined?(:@@lot) # bypass test0000_setup
    assert_equal 0, $sv.eqp_cleanup(@@eqp)
    assert_equal 0, $sv.lot_cleanup(@@lot)
    #
    $lds.all_spc_channels('QA_PARAM_90*').each {|ch| assert ch.delete}
    folder = $lds.folder(@@autocreated_qa)
    assert folder.delete_channels, 'error cleaning channels' if folder
  end

  def test1300_setup
    $setup_ok = false
    #
    assert @@ppd_qa = $sildb.pd_reference(mpd: @@mpd_qa).first, "no PD reference found for #{@@mpd_qa}"
    #
    $setup_ok = true
  end

  def test1301_referenced_pd
    lrecipe = 'C-P-PROCESS-RECIPE.01'
    #
    # build and submit DCR for process PD
    dcrp = Space::DCR.new(eqp: @@eqp, eqptype: 'PROCESS-CHAMBER', lot: @@lot, op: @@ppd_qa, logical_recipe: lrecipe)
    dcrp.add_parameters(@@parameters, @@wafer_values, processing_areas: 'CHC', nocharting: true)
    assert $spctest.send_dcr_verify(dcrp, exporttag: "#{__method__}_p"), 'error sending DCR'
    # 
    # build and submit DCR for related MPD
    dcr = Space::DCR.new(eqp: @@mtool, lot: @@lot, op: @@mpd_qa)
    dcr.add_parameters(@@parameters, @@wafer_values, processing_areas: 'CHA')
    assert $spctest.send_dcr_verify(dcr, nsamples: @@parameters.size*@@wafer_values.size), 'error sending DCR'
    #
    # verify sample extractor keys
    ekeys = {'POperationID'=>@@ppd_qa, 'PLogRecipe'=>lrecipe,'PChamber'=>'CHC', 'PTool'=>@@eqp}
    assert $spctest.verify_parameters(@@parameters, $lds.folder(@@autocreated_qa), @@wafer_values.size, ekeys)
  end
  
  def test1302_referenced_pd_lotparam
    lparams = ['QA_PARAM_900','QA_PARAM_901']
    #
    # build and submit DCR for process PD
    dcrp = Space::DCR.new(eqp: @@eqp, eqptype: 'PROCESS-CHAMBER', lot: @@lot, op: @@ppd_qa)
    dcrp.add_parameters(@@parameters, @@wafer_values, processing_areas: 'CHC', nocharting: true)
    assert $spctest.send_dcr_verify(dcrp, exporttag: "#{__method__}_p"), 'error sending DCR'
    # 
    # build and submit DCR for related MPD
    dcr = Space::DCR.new(eqp: @@mtool, lot: @@lot, op: @@mpd_qa)
    dcr.add_parameters(lparams, {@@lot=>7.0}, reading_type: 'Lot')
    assert $spctest.send_dcr_verify(dcr, nsamples: lparams.size), 'error sending DCR'
    #
    # verify sample extractor keys
    ekeys = {'POperationID'=>@@ppd_qa, 'PChamber'=>'-', 'PTool'=>@@eqp}
    assert $spctest.verify_parameters(lparams, $lds.folder(@@autocreated_qa), 1, ekeys)
  end
  
  # Fab8 Prod Problem yet unknown how to reproduce - empty wafer history on Process tool could be the cause
  def XXXtest1303_referenced_pd_lotparam
    lrecipe = 'C-P-PROCESS-RECIPE.01'
    #
    # build and submit DCR for process PD
    dcrp = Space::DCR.new(eqp: @@eqp, eqptype: 'PROCESS-CHAMBER', lot: 'UXX0023.012', op: @@ppd_qa, logicalRecipeID: lrecipe, technology: 'TEST')
    dcrp.add_lot(@@lot, technology: 'TEST')
    dcrp.add_parameter('Dummy', {@@lot=>7.0, 'UXX0023.012'=>10.2}, processing_areas: [], reading_type: 'Lot', nocharting: true)
    @@wafer_values.each_key {|w| dcrp.add_wafer(w, ilot: -1) }
    dcrp.add_wafer('DUMMY', ilot: -2)
    res = $client.send_dcr(dcrp, export: "log/#{__method__}_p.xml")
    assert $spctest.verify_dcr_accepted(res, 0), 'DCR not submitted successfully'
    # 
    # build and submit DCR for related MPD
    dcr = Space::DCR.new(eqp: @@mtool, lot: @@lot, op: @@mpd_qa, technology: 'TEST')
    dcr.add_parameters(@@parameters, @@wafer_values, processing_areas: ['CHA'])
    res = $client.send_dcr(dcr, export: "log/#{__method__}_m.xml")
    assert $spctest.verify_dcr_accepted(res, @@parameters.size * @@wafer_values.size), 'DCR not submitted successfully'
    #
    # verify sample extractor keys
    ekeys = {'POperationID'=>@@ppd_qa, 'PLogRecipe'=>lrecipe,'PTool'=>@@eqp}
    assert $spctest.verify_parameters(@@parameters, $lds.folder(@@autocreated_qa), @@wafer_values.size, ekeys)
  end
  
  def test1310_multiple_referenced_pds
    lot = 'SIL' + @@sil_version.split('.').collect {|e| '%02d' % e}.join('') + '%02d' % @@test_run + '1' + '.000'
    assert lot = $svtest.new_lot(lot: lot) unless $sv.lot_exists?(lot)
    $log.info "using lot #{lot.inspect} as second lot"
    #
    wafer_values = {"#{lot}01"=>41, "#{lot}02"=>42, "#{lot}03"=>50, "#{lot}04"=>44, "#{lot}05"=>55, "#{lot}06"=>46}
    chambers = ['CHA', 'CHB', 'CHC']
    lrecipes = ['C-P-4710B-MGT-NMETDEPDR.01','C-P-4710B-MGT-NMETDEPDR.02','C-P-4710B-MGT-NMETDEPDR.03']
    #
    # get process PDs referenced by MPD
    ppds = $sildb.pd_reference(mpd: @@mpd_qa)[0..2]
    assert_equal ppds.size, ppds.compact.size, "no PD reference found for #{@@mpd_qa}"
    $log.info "using process PDs #{ppds.inspect}"
    #
    # build and submit DCRs for 3 process PDs
    ppds.each_with_index {|ppd, i|
      dcrp = Space::DCR.new(eqp: "#{@@eqp}_#{i}", eqptype: 'PROCESS-CHAMBER', lot: lot, op: ppd, logical_recipe: lrecipes[i])
      dcrp.add_parameters(@@parameters, wafer_values, processing_areas: chambers[i], nocharting: true)
      assert $spctest.send_dcr_verify(dcrp, exporttag: "#{__method__}_p_#{i}"), 'error sending DCR'
    }
    #
    assert_equal 0, $sv.eqp_cleanup(@@eqp)
    assert_equal 0, $sv.lot_cleanup(lot)
    #
    # build and submit DCR for related MPD
    dcr = Space::DCR.new(eqp: @@mtool, lot: lot, op: @@mpd_qa)
    dcr.add_parameters(@@parameters, wafer_values, processing_areas: 'CHC')
    assert $spctest.send_dcr_verify(dcr, nsamples: @@parameters.size*wafer_values.size), 'error sending DCR'
    #
    # verify sample extractor keys
    ekeys = {'POperationID'=>ppds[0], 'PChamber'=>chambers[0], 'PTool'=>"#{@@eqp}_0", 'PLogRecipe'=>lrecipes[0]}
    assert $spctest.verify_parameters(@@parameters, $lds.folder(@@autocreated_qa), wafer_values.size, ekeys)
  end
  
  def test1310a_all_referenced_pds
    lot = 'SIL' + @@sil_version.split('.').collect {|e| '%02d' % e}.join('') + '%02d' % @@test_run + '1' + '.000'
    assert lot = $svtest.new_lot(lot: lot) unless $sv.lot_exists?(lot)
    $log.info "using lot #{lot.inspect} as second lot"
    #
    wafer_values = {"#{lot}01"=>41, "#{lot}02"=>42, "#{lot}03"=>50, "#{lot}04"=>44, "#{lot}05"=>55, "#{lot}06"=>46}
    chambers = ['CHA', 'CHB', 'CHC']
    lrecipes = ['C-P-4710B-MGT-NMETDEPDR.01','C-P-4710B-MGT-NMETDEPDR.02','C-P-4710B-MGT-NMETDEPDR.03']
    #
    # get process PDs referenced by MPD
    ppds = $sildb.pd_reference(mpd: @@mpd_qa)
    assert_equal ppds.size, ppds.compact.size, "no PD reference found for #{@@mpd_qa}"
    $log.info "using process PDs #{ppds.inspect}"
    #
    assert_equal 0, $sv.eqp_cleanup(@@eqp)
    assert_equal 0, $sv.lot_cleanup(lot)
    #
    # build and submit DCRs for all process PDs
    ppds.each_with_index {|ppd, i|
      dcrp = Space::DCR.new(eqp: "#{@@eqp}_#{i}", eqptype: 'PROCESS-CHAMBER', lot: lot, op: ppd, logical_recipe: lrecipes[i])
      dcrp.add_parameters(@@parameters, wafer_values, processing_areas: chambers[i], nocharting: true)
      assert $spctest.send_dcr_verify(dcrp, exporttag: "#{__method__}_p_#{i}"), 'error sending DCR'
    }
    #
    assert_equal 0, $sv.eqp_cleanup(@@eqp)
    assert_equal 0, $sv.lot_cleanup(lot)
    #
    # build and submit DCR for related MPD
    dcr = Space::DCR.new(eqp: @@mtool, lot: lot, op: @@mpd_qa)
    dcr.add_parameters(@@parameters, wafer_values, processing_areas: 'CHC')
    assert $spctest.send_dcr_verify(dcr, nsamples: @@parameters.size*wafer_values.size), 'error sending DCR'
    #
    # verify sample extractor keys
    ekeys = {'POperationID'=>ppds[0], 'PChamber'=>chambers[0], 'PTool'=>"#{@@eqp}_0", 'PLogRecipe'=>lrecipes[0]}
    assert $spctest.verify_parameters(@@parameters, $lds.folder(@@autocreated_qa), wafer_values.size, ekeys)
  end
  
  def test1311_referenced_pd_multiple_chambers
    # build and submit DCR for process PD
    dcrp = Space::DCR.new(eqp: @@eqp, eqptype: 'PROCESS-CHAMBER', lot: @@lot, op: @@ppd_qa)
    dcrp.add_parameters(@@parameters, @@wafer_values, processing_areas: ['CHA', 'CHB'], nocharting: true)
    assert $spctest.send_dcr_verify(dcrp, exporttag: "#{__method__}_p"), 'error sending DCR'
    #
    # build and submit DCR for related MPD
    dcr = Space::DCR.new(eqp: @@mtool, lot: @@lot, op: @@mpd_qa)
    dcr.add_parameters(@@parameters, @@wafer_values, processing_areas: 'CHC')
    assert $spctest.send_dcr_verify(dcr, nsamples: @@parameters.size*@@wafer_values.size), 'error sending DCR'
    #
    # verify sample extractor keys
    ekeys = {'POperationID'=>@@ppd_qa, 'PChamber'=>'CHA:CHB', 'PTool'=>@@eqp}
    assert $spctest.verify_parameters(@@parameters, $lds.folder(@@autocreated_qa), @@wafer_values.size, ekeys)
  end
  
  def test1312_referenced_pd_single_chamber_and_multiple_chambers #MSR538960
    lot = 'SIL' + @@sil_version.split('.').collect {|e| '%02d' % e}.join('') + '%02d' % @@test_run + '1' + '.000'
    assert lot = $svtest.new_lot(lot: lot) unless $sv.lot_exists?(lot)
    $log.info "using lot #{lot.inspect} as second lot"
    #
    pchambers = [['CHA'], ['CHA', 'CHB', 'CHC'], ['CHA']]
    wafer_values = {"#{lot}01"=>41, "#{lot}02"=>42, "#{lot}03"=>50, "#{lot}04"=>44, "#{lot}05"=>55, "#{lot}06"=>46}
    #
    pchambers.each_with_index {|chambers, i|
      $log.info "-- chambers: #{chambers}"
      # build and submit DCR for process PD
      #
      assert_equal 0, $sv.eqp_cleanup(@@eqp)
      assert_equal 0, $sv.lot_cleanup(lot)
      #
      dcrp = Space::DCR.new(eqp: @@eqp, eqptype: 'PROCESS-CHAMBER', lot: lot, op: @@ppd_qa)
      dcrp.add_parameters(@@parameters, wafer_values, processing_areas: chambers, nocharting: true)
      assert $spctest.send_dcr_verify(dcrp, exporttag: "#{__method__}_#{i}_p"), 'error sending DCR'
      # 
      #
      assert_equal 0, $sv.eqp_cleanup(@@eqp)
      assert_equal 0, $sv.lot_cleanup(lot)
      #
      # build and submit DCR for related MPD
      dcr = Space::DCR.new(eqp: @@mtool, lot: lot, op: @@mpd_qa)
      dcr.add_parameters(@@parameters, wafer_values, processing_areas: ['CHC'])
      assert $spctest.send_dcr_verify(dcr, nsamples: @@parameters.size*wafer_values.size, exporttag: "#{__method__}_#{i}_m"), 'error sending DCR'
      #
      # verify sample extractor keys
      ekeys = {'POperationID'=>@@ppd_qa, 'PChamber'=>chambers.join(':'), 'PTool'=>@@eqp}
      folder = $lds.folder(@@autocreated_qa)
      assert $spctest.verify_parameters(@@parameters, folder, wafer_values.size*(i+1), ekeys, nil, samples_last: wafer_values.size)
    }
  end
  
  def test1313_measurement_pd_same_as_process_pd_single_chamber_and_multiple_chambers #MSR538960
    lot = 'SIL%05d.000' % Time.now.to_i
    pchambers = [['CHA'], ['CHA', 'CHB', 'CHC'], ['CHA']]
    wafer_values = {"#{lot}01"=>41, "#{lot}02"=>32, "#{lot}03"=>50, "#{lot}04"=>34, "#{lot}05"=>55, "#{lot}06"=>36}
    #
    pchambers.each_with_index {|chambers, i|
      $log.info "-- chambers: #{chambers}"
      # build and submit DCR for process PD
      dcrp = Space::DCR.new(eqp: @@eqp, eqptype: 'PROCESS-CHAMBER', lot: @@lot, op: @@ppd_qa)
      dcrp.add_parameters(@@parameters, wafer_values, processing_areas: chambers)
      assert $spctest.send_dcr_verify(dcrp, nsamples: @@parameters.size*wafer_values.size, exporttag: "#{__method__}_#{i}"), 'error sending DCR'
      # 
      # no separate measurement PD
      #
      # verify sample extractor keys
      ekeys = {'POperationID'=>@@ppd_qa, 'PChamber'=>chambers.join(':'), 'PTool'=>@@eqp}
      folder = $lds.folder(@@autocreated_qa)
      assert $spctest.verify_parameters(@@parameters, folder, wafer_values.size*(i+1), ekeys, nil, samples_last: wafer_values.size)
    }
  end
  
  def test1314_not_configured_measurement_pd
    mpd = 'QAXX-MB-FTDEPX.99'
    # 
    # build and submit DCR
    dcr = Space::DCR.new(lot: @@lot, op: mpd)
    dcr.add_parameters(@@parameters, @@wafer_values)
    assert $spctest.send_dcr_verify(dcr, nsamples: @@parameters.size*@@wafer_values.size), 'error sending DCR'
    #
    # verify sample extractor keys
    ekeys = {'POperationID'=>'-', 'PChamber'=>'-', 'PTool'=>'-'}
    assert $spctest.verify_parameters(@@parameters, $lds.folder(@@autocreated_qa), @@wafer_values.size, ekeys)
  end
  
  def test1315_measurement_pd_same_as_process_pd
    mpd = 'QA-SPEC-HIER-METROLOGY-01.01'
    # 
    # build and submit DCR
    dcr = Space::DCR.new(eqptype: 'PROCESS-CHAMBER', lot: @@lot, op: mpd)
    dcr.add_parameters(@@parameters, @@wafer_values)
    assert $spctest.send_dcr_verify(dcr, nsamples: @@parameters.size*@@wafer_values.size), 'error sending DCR'
    #
    # verify sample extractor keys
    folder = $lds.folder(@@autocreated_qa)
    @@parameters.each {|p|
      ch = folder.spc_channel(parameter: p)
      samples = ch.samples
      assert_equal @@wafer_values.size, samples.size, "wrong number of samples in #{ch.inspect}"
      samples.each {|s|
        ekeys = s.extractor_keys
        # must match process PD and tool
        refekeys = dcr.sample_ekeys(wafer: ekeys['Wafer'], responsible_pd: true)
        assert_equal 1, refekeys.size, "wrong extractor keys for sample #{s.inspect}"
        ref = refekeys[0]
        assert $spctest.verify_data(s.extractor_keys, ref)
        # psequence missing
      }
    }
  end
 
  def test1320_measurement_pd_same_as_process_pd_multiple_dcr # MSR536451 part 1
    mpd = 'QA-SPEC-HIER-METROLOGY-01.01'
    #
    # build and submit DCR
    dcr=nil
    p = 'QA_PARAM_900'
    wafer_values = {'2502J493KO'=>41}
    count = 10
    count.times {|i|
      wafer_values.each {|k, v| wafer_values[k] = v + 1}
      dcr = Space::DCR.new(lot: @@lot, op: mpd, eqptype: 'PROCESS-CHAMBER')
      dcr.add_parameter(p, wafer_values)
      assert $spctest.send_dcr_verify(dcr, nsamples: 1), 'error sending DCR'
    }
    # verify last value 
    folder = $lds.folder(@@autocreated_qa)
    ch = folder.spc_channels(parameter: p).first
    samples = ch.samples
    assert_equal count, samples.size, "wrong number of samples in #{ch.inspect}"
    assert_equal samples.last.raw_values.first, wafer_values.values.first, 'value not the last sended'
  end

  def test1321_referenced_pd # MSR536451 part 2
    p = 'QA_PARAM_900'
    eqps = 7.times.collect {|i| "#{@@eqp}_#{i}"}
    chas = ['CHA', 'CHB', 'CHC']
    folder = $lds.folder(@@autocreated_qa)
    10.times {|i|
      # build and submit DCR for process PD
      eqp = eqps.sample
      cha = chas.sample
      $log.info "-- #{eqp} #{cha}"
      value = 41 + i
      #
      #
      assert_equal 0, $sv.eqp_cleanup(@@eqp)
      assert_equal 0, $sv.lot_cleanup(@@lot)
      #
      dcrp = Space::DCR.new(eqp: eqp, eqptype: 'PROCESS-CHAMBER', lot: @@lot, op: @@ppd_qa)
      dcrp.add_parameter(p, {'2702J493KO'=>value}, processing_areas: cha, nocharting: true)
      assert $spctest.send_dcr_verify(dcrp, exporttag: "#{__method__}_#{i}_p"), 'error sending DCR'
      # 
      #
      assert_equal 0, $sv.eqp_cleanup(@@eqp)
      assert_equal 0, $sv.lot_cleanup(@@lot)
      #
      # build and submit DCR for related MPD
      dcr = Space::DCR.new(eqp: @@mtool, lot: @@lot, op: @@mpd_qa)
      dcr.add_parameter(p, {'2702J493KO'=>value}, processing_areas: chas.first)
      assert $spctest.send_dcr_verify(dcr,nsamples: 1, exporttag: "#{__method__}_#{i}_m"), 'error sending DCR'
      #
      # verify sample extractor keys
      ch = folder.spc_channel(parameter: p)
      samples = ch.samples
      assert_equal i + 1, samples.size, "wrong number of samples in #{ch.inspect}"
      assert_equal samples.last.raw_values.first, value, 'value not the last sended'
      assert $spctest.verify_data(samples.last.extractor_keys, {'POperationID'=>@@ppd_qa, 'PTool'=>eqp, 'PChamber'=>cha})
      sleep 3
    }
  end

  # See MSR542002
  def test1330_referenced_pd_responsiblepd2to5
    mpd = 'QA-999999.01'
    p = 'QA_PARAM_901'
    ppds = ['QA-999999.03','QA-999999.04','QA-999999.05','QA-999999.06']
    ekeys = {'PTool'=>'-', 'POperationID'=>'-', 'PChamber'=>'-'}
    folder = $lds.folder(@@autocreated_qa)
    #
    ppds.each {|ppd|
      $log.info "-- using process PD #{ppd}"
      folder.delete_channels
      # build and submit DCR for process PD
      dcrp = Space::DCR.new(eqp: @@eqp, eqptype: 'PROCESS-CHAMBER', lot: @@lot, op: ppd)
      dcrp.add_parameter(p, @@wafer_values, processing_areas: ['CHA'], nocharting: true)
      assert $spctest.send_dcr_verify(dcrp, exporttag: "#{__method__}_p"), 'error sending DCR'
      # 
      # build and submit DCR for related MPD
      dcr = Space::DCR.new(eqp: @@mtool, lot: @@lot, op: mpd)
      dcr.add_parameter(p, @@wafer_values, processing_areas: ['CHC'])
      assert $spctest.send_dcr_verify(dcr, nsamples: @@wafer_values.size, exporttag: "#{__method__}_m"), 'error sending DCR'
      #
      # verify sample extractor keys
      folder = $lds.folder(@@autocreated_qa)
      assert $spctest.verify_parameters([p], folder, @@wafer_values.size, ekeys, nil, samples_last: 1)
    }
  end
  
  def test1331_referenced_pd_multiple_resppd1
    folder = $lds.folder(@@autocreated_qa)
    mpd = 'QA-MULT-PD-MB-FTDEPM.01'
    p = 'QA_PARAM_901'
    # get a process PD referenced by MPD
    ppds = $sildb.pd_reference(mpd: mpd)
    refute_empty ppds, "no PD reference found for #{mpd}"
    $log.info "using process PD #{ppds.inspect}"
    ppds.each_with_index {|ppd, i|
      $log.info "using process PD #{ppd}"
      eqp = "#{@@eqp}_#{i}"
      cha = ['CHA', 'CHB', 'CHC'][i]
      folder.delete_channels
      #
      # build and submit DCR for process PD
      dcrp = Space::DCR.new(eqp: eqp, eqptype: 'PROCESS-CHAMBER', lot: @@lot, op: ppd)
      dcrp.add_parameter(p, @@wafer_values, processing_areas: cha, nocharting: true)
      assert $spctest.send_dcr_verify(dcrp, exporttag: "#{__method__}_#{i}_p"), 'error sending DCR'
      # 
      # build and submit DCR for related MPD
      dcr = Space::DCR.new(eqp: @@mtool, lot: @@lot, op: mpd)
      dcr.add_parameter(p, @@wafer_values, processing_areas: [])
      assert $spctest.send_dcr_verify(dcr, nsamples: @@wafer_values.size, exporttag: "#{__method__}_#{i}_m"), 'error sending DCR'
      #
      # verify sample extractor keys
      ekeys = {'PTool'=>eqp, 'POperationID'=>ppd, 'PChamber'=>cha}
      folder = $lds.folder(@@autocreated_qa)
      assert $spctest.verify_parameters([p], folder, @@wafer_values.size, ekeys, nil, samples_last: 1)
    }
  end
  
  def test1332_multiple_referenced_pd_multiple_resppd1
    folder = $lds.folder(@@autocreated_qa)
    mpds = ['QA-MULT-PD-MB-FTDEPM.01','QA-MULT-PD-MB-FTDEPM.02']
    chambers = @@chambers.keys
    p = 'QA_PARAM_901'
    # get a process PD referenced by MPD
    ppds = []
    mpds.each {|mpd|
      res = $sildb.pd_reference(mpd: mpd, all: true)
      assert (res and res.size > 0 and res[0].pdrefs.size > 0), "MPD #{mpd} does not reference any process PDs"
      ppds << res[0].pdrefs.flatten
      ppds.flatten!
    }
    ppds.uniq!
    ppds.sort!
    $log.info "using PPD #{ppds.inspect}"
    #
    mpds.each {|mpd|
      ppds.each_with_index {|ppd, i|
        eqp = "#{@@eqp}_#{i}"
        cha = chambers[i]
        folder.delete_channels
        #
        $log.info "-- using process PD #{ppd}, MPD #{mpd}"
        # build and submit DCR for process PD
        dcrp = Space::DCR.new(eqp: eqp, eqptype: 'PROCESS-CHAMBER', lot: @@lot, op: ppd)
        dcrp.add_parameter(p, @@wafer_values, processing_areas: cha, nocharting: true)
        assert $spctest.send_dcr_verify(dcrp, exporttag: "#{__method__}_#{i}_p"), 'error sending DCR'
        # 
        # build and submit DCR for related MPD
        dcr = Space::DCR.new(eqp: @@mtool, lot: @@lot, op: mpd)
        dcr.add_parameter(p, @@wafer_values, processing_areas: [])
        assert $spctest.send_dcr_verify(dcr, nsamples: @@wafer_values.size, exporttag: "#{__method__}_#{i}_m"), 'error sending DCR'
        #
        # verify sample extractor keys
        ekeys = {'PTool'=>eqp, 'POperationID'=>ppd, 'PChamber'=>cha}
        folder = $lds.folder(@@autocreated_qa)
        assert $spctest.verify_parameters([p], folder, @@wafer_values.size, ekeys, nil, samples_last: 1)
      }
    }
  end
  
  #See use cases for MSR517251
  def test1340_referenced_pd_parameter
    mpd = 'QA-PARAM-FTDEPM.01'
    folder = $lds.folder(@@autocreated_qa)
    # get a process PD referenced by MPD
    pd_setup = $sildb.pd_reference(mpd: mpd, all: true)
    assert (pd_setup and pd_setup.size > 0 and pd_setup[0].pdrefs.size > 0), "MPD #{mpd} does not reference any process PDs"
    $log.info "using process PD #{pd_setup.inspect}"

    test_ok = true
    chambers = @@chambers.keys
    # build and submit DCR for process PD
    pd_setup.each_with_index {|pdref, i|
      eqp = "#{@@eqp}_#{i}"
      ppd = pdref.pdrefs.flatten[0]
      parameter = pdref.parameter || 'QA_PARAM_904'
      folder.delete_channels
      $log.info "-- using process PD #{ppd}"
      dcrp = Space::DCR.new(eqp: eqp, eqptype: 'PROCESS-CHAMBER', lot: @@lot, op: ppd)
      dcrp.add_parameter('Dummy', @@wafer_values, processing_areas: chambers[i % chambers.count], nocharting: true)
      assert $spctest.send_dcr_verify(dcrp, exporttag: "#{__method__}_#{i}_p"), 'error sending DCR'
    }
    #
    # build and submit DCR for related MPD
    dcr = Space::DCR.new(eqp: @@mtool, lot: @@lot, op: mpd)
    dcr.add_parameters(@@parameters, @@wafer_values, processing_areas: [])
    assert $spctest.send_dcr_verify(dcr, nsamples: @@parameters.size*@@wafer_values.size), 'error sending DCR'
    #
    # verify sample extractor keys
    folder = $lds.folder(@@autocreated_qa)
    @@parameters.each do |p|
      ch = folder.spc_channel(parameter: p)
      samples = ch.samples
      assert_equal @@wafer_values.size, samples.size, "wrong number of samples in #{ch.inspect}"
      # check last sample only, process PD and tool must be "-"
      s = samples.last
      s_param = (p == 'QA_PARAM_904') ? nil : p
      ppd_i = pd_setup.find_index {|pdref| pdref.parameter == s_param}
      ppd = pd_setup[ppd_i].pdrefs.flatten[0]
      $log.info "verifying PPD #{ppd}"
      cha = chambers[ppd_i%chambers.count]
      test_ok &= $spctest.verify_data(s.extractor_keys, {'PTool'=>"#{@@eqp}_#{ppd_i}", 'POperationID'=>ppd, 'PChamber'=>cha})
      test_ok &= $spctest.verify_data(s.data_keys, {'PSequence'=>"#{cha}:#{cha}_SIDE1:#{cha}_SIDE2"})
    end
    assert test_ok, 'wrong extractor key data'
  end

  # Fab8 only

  #See use cases for MSR517251
  def test1341_referenced_pd_parameter_product_group
    skip "not applicable in #{$env}" unless $env == 'f8stag'
    #
    mpd = 'QA-PROD-FTDEPM.01'
    folder = $lds.folder(@@autocreated_qa)
    # get a process PD referenced by MPD
    pd_setup = $sildb.pd_reference(mpd: mpd, all: true)
    assert (pd_setup and pd_setup.size > 0 and pd_setup[0].pdrefs.size > 0), "MPD #{mpd} does not reference any process PDs"
    $log.info "using process PD #{pd_setup.inspect}"

    test_ok = true
    chambers = @@chambers.keys
    # build and submit DCRs for process PD
    pd_setup.each_with_index {|pdref,i|
      eqp = "#{@@eqp}_#{i}"
      ppd = pdref.pdrefs.flatten[0]

      product = (pdref.product or 'SILPROD.01')
      productgroup = (pdref.productgroup or 'QAPG1')

      $log.info "using process PD #{ppd}"
      dcrp = Space::DCR.new(eqp: eqp, eqptype: 'PROCESS-CHAMBER', lot: @@lot, op: ppd, product: product, productgroup: productgroup)

      dcrp.add_parameter('Dummy', @@wafer_values, processing_areas: chambers[i%chambers.count], nocharting: true)
      res = $client.send_dcr(dcrp, export: "log/#{__method__}_p_#{i}.xml")
      assert $spctest.verify_dcr_accepted(res, 0), 'DCR not submitted successfully'
    }
    #
    # build and submit DCRs for related MPD
    pd_setup.each_with_index do |pdref,i|
      product = (pdref.product or 'SILPROD.01')
      productgroup = (pdref.productgroup or 'QAPG1')
      parameter = (pdref.parameter or 'QA_PARAM_904')
      folder.delete_channels

      dcr = Space::DCR.new(eqp: @@mtool, lot: @@lot, op: mpd, product: product, productgroup: productgroup)
      dcr.add_parameter(parameter, @@wafer_values, processing_areas: ['CHA'])
      res = $client.send_dcr(dcr, export: "log/#{__method__}_m_#{i}.xml")
      assert $spctest.verify_dcr_accepted(res, @@wafer_values.size), 'DCR not submitted successfully'

      # verify sample extractor keys
      ch = folder.spc_channel(parameter: parameter)
      samples = ch.samples
      assert_equal @@wafer_values.size, samples.size, "wrong number of samples in #{ch.inspect}"
      # check last sample only, process PD and tool must be "-"
      s = samples[-1]
      eqp = "#{@@eqp}_#{i}"
      ppd = pdref.pdrefs.flatten[0]
      $log.info "verifying PPD #{ppd}"
      cha = chambers[i%chambers.count]
      seq = "#{cha}:#{cha}_SIDE1:#{cha}_SIDE2"
      test_ok &= $spctest.verify_data(s.extractor_keys, {'PTool'=>eqp, 'POperationID'=>ppd, 'PChamber'=>cha})
      test_ok &= $spctest.verify_data(s.data_keys, {'PSequence'=>seq})
    end
    assert test_ok, 'wrong extractor key data. Check lookup.resppd.usingproductcontext=true'
  end
  
  #Additional cases for MSR517251 based on TCP spec
  #http://fc8sfcqv:8080/setupfc/pages/restricted/viewSpec.seam?SpecId=M07-00000565&SpecVersion=1&cid=4343
  def test1342_referenced_pd_parameter_product_group
    skip "not applicable in #{$env}" unless $env == 'f8stag'
    assert $spctest.verify_speclimit(specid: 'M07-00020001', specversion: 5, all_active: true), 'Speclimits do not exist'
    #
    mpds = ['QA_DIF-PADOXDI2.01', 'QA_DIF-PADOXDI3.01']    
    folder = $lds_setup.folder(@@autocreated_qa)
    test_ok = true    
    mpds.each do |mpd|    
      # get a process PD referenced by MPD
      pd_setup = $sildb.pd_reference(mpd: mpd, all: true)
      assert (pd_setup and pd_setup.size > 0 and pd_setup[0].pdrefs.size > 0), "MPD #{mpd} does not reference any process PDs"
      $log.info "using process PD #{pd_setup.inspect}"
      # build and submit DCRs for process PD
      pd_setup.each_with_index {|pdref,i|
        eqp = "#{@@eqp}_#{i}"
        ppd = pdref.pdrefs.flatten[0]
        product = (pdref.product or 'SILPROD.01')
        productgroup = (pdref.productgroup or 'QAPG1')
        $log.info "using process PD #{ppd}"
        dcrp = Space::DCR.new(eqp: eqp, eqptype: 'PROCESS-CHAMBER', lot: @@lot, op: ppd, technology: 'TEST', product: product, productgroup: productgroup)
        dcrp.add_parameter('Dummy', @@wafer_values, processing_areas: ['CHB'], nocharting: true)
        res = $client.send_dcr(dcrp, export: "log/#{__method__}_p_#{i}.xml")
        assert $spctest.verify_dcr_accepted(res, 0), 'DCR not submitted successfully'
      }
      # 
      # build and submit DCRs for related MPD
      pd_setup.each_with_index do |pdref,i|
        product = (pdref.product or 'SILPROD.01')
        productgroup = (pdref.productgroup or 'QAPG1')
        parameter = (pdref.parameter or 'QA_PARAM_900')
        
        folder.delete_channels

        dcr = Space::DCR.new(eqp: @@mtool, lot: @@lot, op: mpd, technology: 'TEST', product: product, productgroup: productgroup)
        dcr.add_parameter(parameter, @@wafer_values, processing_areas: ['CHA'])
        res = $client.send_dcr(dcr, export: "log/#{__method__}_m_#{i}.xml")
        assert $spctest.verify_dcr_accepted(res, @@wafer_values.size), 'DCR not submitted successfully'
              
        # verify sample extractor keys
        ch = folder.spc_channel(parameter: parameter)
        samples = ch.samples
        assert_equal @@wafer_values.size, samples.size, "wrong number of samples in #{ch.inspect}"
        # check last sample only, process PD and tool must be "-"
        s = samples[-1]
        eqp = "#{@@eqp}_#{i}"
        ppd = pdref.pdrefs.flatten[0]
        $log.info "verifying PPD #{ppd}"      
        test_ok &= $spctest.verify_data(s.extractor_keys, {'PTool'=>eqp, 'POperationID'=>ppd, 'PChamber'=>'CHB'})
      end
    end
    assert test_ok, 'wrong extractor key data. Check lookup.resppd.usingproductcontext=true'
  end
  
  def test1343_referenced_pd_parameter_premeasure
    skip "not applicable in #{$env}" unless $env == 'f8stag'
    assert $spctest.verify_speclimit(specid: 'M07-00020001', specversion: 5, all_active: true), 'Speclimits do not exist'
    #
    mpd = 'QA_DIF-PADOXDI4.01'
    parameter = 'QA_PARAM_900'
    product = 'SILPROD6.01'
    #
    # get process PDs referenced by MPD
    res = $sildb.pd_reference(mpd: mpd)
    assert res.size > 1, "MPD #{mpd} does not reference enough process PDs"
    ppds = ['QA_DIF-PREMEAS.01', 'QA_DIF-PADOX9.01']
    $log.info "using process PDs #{ppds.inspect}"
    #
    # build and submit DCRs for pre and process PDs
    $drcp = dcrp = nil
    ppds.each_with_index {|ppd, i|
      dcrp = Space::DCR.new(eqp: "#{@@eqp}_#{i}", eqptype: 'PROCESS-CHAMBER', lot: @@lot, op: ppd, product: product)
      dcrp.add_parameters(@@parameters, @@wafer_values, processing_areas: 'CHA', nocharting: true)
      res = $client.send_dcr(dcrp, export: "log/#{__method__}_p_#{i}.xml")
      assert $spctest.verify_dcr_accepted(res, 0), 'DCR not submitted successfully'
    }
    # 
    # build and submit DCR for related MPD
    dcr = Space::DCR.new(eqp: @@mtool, lot: @@lot, op: mpd, product: product)
    dcr.add_parameter(parameter, @@wafer_values, processing_areas: 'CHC')
    res = $client.send_dcr(dcr, export: "log/#{__method__}_m.xml")
    assert $spctest.verify_dcr_accepted(res, @@wafer_values.size), 'DCR not submitted successfully'
    #
    # verify sample extractor keys
    folder = $lds.folder(@@autocreated_qa)
    ch = folder.spc_channel(parameter: parameter)
    samples = ch.samples
    assert_equal @@wafer_values.size, samples.size, "wrong number of samples in #{ch.inspect}"
    samples.each {|s|
      # must match last PD and tool
      assert $spctest.verify_data(s.extractor_keys, {'POperationID'=>ppds[-1], 'PChamber'=>'CHA', 'PTool'=>"#{@@eqp}_#{ppds.size-1}"})
    }
  end
end
