=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: Steffen Steidten, 2011-11-25
=end

require_relative 'Test_Space_Setup'


# Test Space/SIL. It contains tests with manual interaction that can be executed by
#
#   run_test 'Test_Space00', $env, tp: /setup|testman/
class Test_Space00 < Test_Space_Setup
  #@@mpd_qa = 'QA-MB-FTDEPM-DUMMY.01'
  @@mpd_state = 'QA-MB-FTDEPM-STATE.01'  # has no PPD, using DCR builtin default for PPDs
  @@long_parameters = ['QA_FORTY-----16|-------------32||----40|',
    'QA_ONEHUNDERTTWENTYEIGHT-----32||----------------------------64||----------------------------96||---------------------------128|',
    'QA_TWOHUNDERTFIFTYSIX--------32||----------------------------64||----------------------------96||---------------------------128||---------------------------160||---------------------------192||---------------------------224||---------------------------256|',
    'QA_FIVEHUNDERTTWELVE---------32||----------------------------64||----------------------------96||---------------------------128||---------------------------160||---------------------------192||---------------------------224||---------------------------256||---------------------------288||---------------------------320||---------------------------352||---------------------------384||---------------------------416||---------------------------448||---------------------------480||---------------------------512|'
  ]
  @@email_addresses = '<M23434>'   # dl QA Fab1    '<M3016>'  # ITDC Conny Stenzel

  def test0001_properties
    $setup_ok = false
    #
    props = $spctest.silserver.read_jobprops
    # global chartcheck properties
    # inline LDS
    # as per mt(qa) spreadsheet
    @@chartcheck_inline_enable = props[:'chartcheck.inline.enable'] || nil
    # as per https://docs.google.com/a/globalfoundries.com/document/d/15x64AjZ03WX0fj7M8qvSNixTx4pL0SmfFKc84MPd-UU/edit?usp=sharing
    @@chartcheck_inline_active = props[:'chartcheck.inline.qa.active'] || nil
    @@chartcheck_inline_trace = props[:'chartcheck.inline.qa.active'] || nil
    @@chartcheck_inline_notification = props[:'chartcheck.inline.qa.active'] || nil
    # setup LDS
    # as per mt(qa) spreadsheet
    @@chartcheck_setup_enable = props[:'chartcheck.setup.enable'] || nil
    # as per https://docs.google.com/a/globalfoundries.com/document/d/15x64AjZ03WX0fj7M8qvSNixTx4pL0SmfFKc84MPd-UU/edit?usp=sharing
    @@chartcheck_setup_active = props[:'chartcheck.setup.qa.active'] || nil
    @@chartcheck_setup_trace = props[:'chartcheck.setup.qa.active'] || nil
    @@chartcheck_setup_notification = props[:'chartcheck.setup.qa.active'] || nil

    # module specific chartcheck properties
    # inline LDS
    @@chartcheck_inline_qa_active = props[:'chartcheck.inline.qa.active'] || nil
    @@chartcheck_inline_qa_trace = props[:'chartcheck.inline.qa.active'] || nil
    @@chartcheck_inline_qa_notification = props[:'chartcheck.inline.qa.active'] || nil
    @@chartcheck_inline_etc_active = props[:'chartcheck.inline.etc.active'] || nil
    @@chartcheck_inline_etc_trace = props[:'chartcheck.inline.etc.active'] || nil
    @@chartcheck_inline_etc_notification = props[:'chartcheck.inline.etc.active'] || nil
    # setup LDS
    @@chartcheck_setup_qa_active = props[:'chartcheck.setup.qa.active'] || nil
    @@chartcheck_setup_qa_trace = props[:'chartcheck.setup.qa.active'] || nil
    @@chartcheck_setup_qa_notification = props[:'chartcheck.setup.qa.active'] || nil
    @@chartcheck_setup_etc_active = props[:'chartcheck.setup.etc.active'] || nil
    @@chartcheck_setup_etc_trace = props[:'chartcheck.setup.etc.active'] || nil
    @@chartcheck_setup_etc_notification = props[:'chartcheck.setup.etc.active'] || nil
    #
    assert $spctest.verify_sil_properties, 'properties have changed'
    #
    $setup_ok = true
  end

  def test0002_extractor_keys
    test_ok = true
    [@@lds_inline, @@lds_setup].each {|l|
      $log.info "LDS: #{l}"
      lds = $spc.ldses(l)[0]
      ekeys = lds.extractor_keys
      assert ekeys.size > 2, 'wrong number of extractor keys'
      #if $spc.params[:flavour] == 'inline70'  ## also applies to inline71!
        test_ok &= $spctest.verify_data(ekeys[0], Space::SpcApi::SpcEKey.new(1, 'Lot', nil, 'Y'))
        test_ok &= $spctest.verify_data(ekeys[1], Space::SpcApi::SpcEKey.new(2, 'Wafer', nil, 'Y'))
      #else
      #  test_ok &= $spctest.verify_data(ekeys[0], Space::SpcApi::SpcEKey.new(1, 'Lot', nil, true))
      #  test_ok &= $spctest.verify_data(ekeys[1], Space::SpcApi::SpcEKey.new(2, 'Wafer', nil, true))
      #end
    }
    assert test_ok, 'wrong extractor keys'
  end

  def test0003_templates
    $lds = $spc.lds(@@lds_inline)
    test_ok = true
    folder = $lds.folder(@@templates_folder)
    [@@default_template_qa, @@params_template].each {|t|
      ch = folder.spc_channel(t)
      ch.name = "#{t}_renamed" if ch
    }
    [@@default_template_qa, @@params_template].each {|t|
      cname = "#{t}_renamed"
      if !folder.spc_channel(cname)
        test_ok = false
        $log.warn "template channel #{t.inspect} does not exist"
      end
    }
    if !folder.spc_channel(@@default_template)
      test_ok = false
      $log.warn "default template #{@@default_template} does not exist"
    end
    #
    assert test_ok, 'renamed test templates do not exist'
  end

  def test0004_delete_channels
    [@@autocreated_nodep, @@autocreated_qa].each {|f|
      folder = $lds.folder(f)
      ($log.info "folder #{f} does not exist"; next) unless folder
      assert_equal 0, folder.delete, "error deleting folder #{f}"
    }
    assert folder = $lds.folder(@@module_qa), "folder #{@@module_qa} does not exist"
    assert folder.delete_channels, "error deleting channels in #{folder.name}"
  end

  def test0008_create_autocharting_folder
    assert_equal 0, $sv.eqp_cleanup(@@eqp)
    assert_equal 0, $sv.lot_cleanup(@@lot)
    dpt = 'QADummy'
    parameter = 'QA_DummyPARAM1'
    technology = 'dummytech'
    #
    # ensure autocharting folder does not exist
    folder = $lds.folder("AutoCreated_#{dpt}")
    assert_equal 0, folder.delete, "Autocharting folder 'AutoCreated_#{dpt}' already exists" if folder
    #
    # build and submit DCR
    dcr = Space::DCR.new(lot: @@lot, department: dpt, technology: technology, op: @@mpd_state)
    dcr.add_parameter(parameter, @@wafer_values)  #, space_listening: true)
    res = $client.send_dcr(dcr, export: :caller)
    assert $spctest.verify_dcr_accepted(res, @@wafer_values.size), 'DCR not submitted successfully'
    #
    # verify autocharting folder creation
    assert folder = $lds.folder("AutoCreated_#{dpt}"), 'Autocharting folder has not been created'
    # verify spc channel and sample data
    chs = folder.spc_channels(parameter: parameter)
    assert_equal 1, chs.size, "wrong number of channels in 'AutoCreated_#{dpt}'"
    wafer_unit = ($env == 'f8stag') ? 'WAFER' : '-'
    ref_ch = Space::SpcApi::ChannelData.new(nil, nil, "Auto-created using template: #{@@default_template}", parameter, wafer_unit)
    assert $spctest.verify_channel(chs[0], ref_ch, dcr, @@wafer_values), 'wrong channel data'
    # clean up
    assert_equal 0, folder.delete, "error deleting folder 'AutoCreated_#{dpt}'"
  end

  def test0009_create_autocharting_folders_load
    n = ($env == 'let') ? 20 : 5
    #
    dpt = 'QADummy'
    parameter = 'QA_DummyPARAM'
    pg = 'QAPG'
    product = 'SILPROD.0'
    technology = 'dummytech'
    #
    # ensure autocharting folders do not exist
    n.times {|i|
      folder = $lds.folder("AutoCreated_#{dpt}#{i}")
      assert_equal 0, folder.delete, "error deleting folder 'AutoCreated_#{dpt}#{i}'" if folder
    }
    #
    # send DCRs
    test_ok = true
    n.times.collect {|i|
      $sv.lot_cleanup(@@lot)
      Thread.new {
        client = Space::Client.new($env, timeout: 900)
        dcr = Space::DCR.new(lot: @@lot, department: "#{dpt}#{i}", technology: technology, productgroup: "#{pg}#{i}", product: "#{product}#{i}", op: @@mpd_state)
        dcr.add_parameter("#{parameter}#{i}", @@wafer_values)  #, space_listening: true)
        test_ok &= $spctest.send_dcr_verify(dcr, nsamples: @@wafer_values.size, export: "log/#{__method__}_#{i}.xml")
      }
    }.each {|t| t.join}
    assert test_ok
    #
    # verify autocharting folder creation
    test_ok = true
    n.times {|i|
      folder = $lds.folder("AutoCreated_#{dpt}#{i}")
      ($log.warn "missing folder AutoCreated_#{dpt}#{i}"; test_ok = false) unless folder
      chs = folder.spc_channels
      ($log.warn "wrong number of channels in AutoCreated_#{dpt}#{i}"; test_ok = false) unless chs && chs.size == 1
    }
    # clean up
    n.times {|i|
      folder = $lds.folder("AutoCreated_#{dpt}#{i}")
      folder.delete if folder
    }
    #
    assert test_ok
  end

  # currently sometimes fails, run it manually by passing tp: /setup|testman0010/
  def testman0010_create_autocharting_folders_load_ooc
    n = @@spc_hold_reasons.size
    # set this to 5 or more to test autocharting performance
    nparameters = 5
    $log.info "n is #{n}, reasons: #{@@spc_hold_reasons.inspect}"
    assert_equal 0, $sv.lot_cleanup(@@lot)
    #
    _dpmt = 'QADummy'
    parameter = 'QA_DummyPARAM'
    pg = 'QAPG'
    product = 'SILPROD.0'
    technology = '28SLP1STM'
    op = 'QA-MB-FTDEPM-DUMMY'
    #
    # ensure autocharting folders do not exist
    n.times {|i|
      folder = $lds.folder("AutoCreated_#{_dpmt}#{i}")
      assert_equal(0, folder.delete, "error deleting folder 'AutoCreated_#{_dpmt}#{i}'") if folder
    }
    #
    # set this to 100 or more to test autocharting performance
    wafer_values = Hash[nparameters.times.collect {|i| ["2502B#{100+i}KO", 100+i]}]
    # send DCRs
    n.times.collect {|i|
      Thread.new {
        client = Space::Client.new($env, timeout: 900)
        dcr = Space::DCR.new(lot: @@lot, department: "#{_dpmt}#{i}", technology: technology, productgroup: "#{pg}#{i}", product: "#{product}#{i}", op: "#{op}#{i}.01")
        dcr.add_parameter("#{parameter}#{i}", wafer_values)  #, space_listening: false)
        client.send_dcr(dcr, export: "log/#{__method__}_#{i}.xml")
      }
    }.each {|t| t.join}
    #
    # verify autocharting folder creation
    test_ok = true
    n.times {|i|
      folder = $lds.folder("AutoCreated_#{_dpmt}#{i}")
      if folder
        chs = folder.spc_channels
        ($log.warn "wrong number of channels in AutoCreated_#{_dpmt}#{i}"; test_ok = false) unless chs && chs.size == nparameters
        #assert $spctest.verify_ecomment(chs, "LOT_HELD: #{@@lot}: reason={X-S")
      else
        $log.warn "missing folder AutoCreated_#{_dpmt}#{i}"
        test_ok = false
      end
    }
    assert test_ok
		assert $spctest.verify_futurehold(@@lot, '[(Raw above specification)', nil, expected_holds: n), 'lot must have future holds'
    #
    # clean up
    n.times {|i|
      folder = $lds.folder("AutoCreated_#{_dpmt}#{i}")
      assert_equal 0, folder.delete, "#{folder.inspect} still exists" if folder
    }
  end

  # currently fails (not implemented), run it manually by passing tp: /setup|testdev0011/
  def test0011_create_autocharting_folder_long_names
    assert_equal 0, $sv.eqp_cleanup(@@eqp)
    assert_equal 0, $sv.lot_cleanup(@@lot)
    folder = $lds.folder(@@autocreated_qa)
    assert folder.delete_channels if folder
    #
    # build and submit DCR
		@@long_parameters[0..2].each_with_index {|p, i|
			$log.info "Sending DCR with parameter name length #{p.size}"
      assert_equal 0, $sv.lot_cleanup(@@lot)
			dcr = Space::DCR.new(lot: @@lot, eqp: @@mtool, op: @@mpd_state, technology: '32nm')
			dcr.add_parameter(p, @@wafer_values)
			res = $client.send_dcr(dcr, export: "log/#{__method__}_#{i}.xml")
			assert $spctest.verify_dcr_accepted(res, @@wafer_values.size), "DCR with parameter name length of #{p.size} not submitted successfully"
    }
    # verify channel names are not truncated(MSR 743545)
    folder = $lds.folder(@@autocreated_qa)
    @@long_parameters[0..2].each {|p|
      ch = folder.spc_channel(parameter: p)
      assert ch, 'spc channel has not been created'
      assert ch.name.size >= p.size, "channel name #{ch.name} truncated to #{ch.name.size} chars"
		}
  end

  def test0013a_nooc_statetest_archived
    check_channel_behaviour('Archived', 'NOOC', @@wafer_values2)
  end

  def test0013b_ooc_statetest_archived
    check_channel_behaviour('Archived', 'OOC', @@wafer_values2ooc)
  end

  def test0014a_nooc_statetest_frozen
    check_channel_behaviour('Frozen', 'NOOC', @@wafer_values2)
  end

  def test0014b_ooc_statetest_frozen
    check_channel_behaviour('Frozen', 'OOC', @@wafer_values2ooc)
  end

  def test0015a_nooc_statetest_offline_ca_nomail
    folder = $lds_setup.folder(@@templates_folder)
    ch = folder.spc_channel(name: @@params_template)
    ch.set_email_addresses('')
    ch.valuations.each {|v| assert_empty v.email_addresses, 'no email address must be configured'}
    #
    check_channel_behaviour('Offline', 'NOOC', @@wafer_values2, aqvresult: 'success')
  end

  def test0015a_ooc_statetest_offline_ca_nomail
    folder = $lds_setup.folder(@@templates_folder)
    ch = folder.spc_channel(name: @@params_template)
    ch.set_email_addresses('')
    ch.valuations.each {|v| assert_empty v.email_addresses, 'no email address must be configured'}
    #
    check_channel_behaviour('Offline', 'OOC', @@wafer_values2ooc, aqvresult: 'fail')
  end

  def test0015b_nooc_statetest_offline_ca_mail
    folder = $lds_setup.folder(@@templates_folder)
    ch = folder.spc_channel(name: @@params_template)
    ch.set_email_addresses(@@email_addresses)
    ch.valuations.each {|v| refute_empty v.email_addresses, 'email address must be configured'}
    #
    check_channel_behaviour('Offline', 'NOOC', @@wafer_values2, aqvresult: 'success')
  end

  def test0015b_ooc_statetest_offline_ca_mail
    folder = $lds_setup.folder(@@templates_folder)
    ch = folder.spc_channel(name: @@params_template)
    ch.set_email_addresses(@@email_addresses)
    ch.valuations.each {|v| refute_empty v.email_addresses, 'email address must be configured'}
    #
    check_channel_behaviour('Offline', 'OOC', @@wafer_values2ooc, aqvresult: 'fail')
  end

  def test0015c_ooc_statetest_offline_canoteffective_mail
    folder = $lds_setup.folder(@@templates_folder)
    ch = folder.spc_channel(name: @@params_template)
    ch.set_email_addresses(@@email_addresses)
    ch.valuations.each {|v| refute_empty v.email_addresses, 'email address must be configured'}
    #
    check_channel_behaviour('Offline', 'OOC', @@wafer_values2ooc, actions: [], aqvresult: 'success') {|channels|
      # set AutoCA to "Sigma below...", which will not be activated
      channels.each {|ch| $spc.set_corrective_actions(ch, 'AutoLotHold', valuations: [17])}
    }
  end

  def test0016a_nooc_statetest_study
    check_channel_behaviour('Study', 'NOOC', @@wafer_values2) #, department: 'QAGKFX1')
  end

  def test0016b_ooc_statetest_study
    check_channel_behaviour('Study', 'OOC', @@wafer_values2ooc)
  end

  def test0017a_nooc_statetest_4study_1offline
    check_channel_behaviour('Study', 'NOOC', @@wafer_values2, exceptState: 'Offline')
  end

  def test0017b_ooc_statetest_4study_1offline
    check_channel_behaviour('Study', 'OOC', @@wafer_values2ooc, exceptState: 'Offline')
  end

  def test0018a_nooc_statetest_offline_noca_nomail
    folder = $lds_setup.folder(@@templates_folder)
    ch = folder.spc_channel(name: @@params_template)
    ch.set_email_addresses('')
    ch.valuations.each {|v| assert_empty v.email_addresses, 'email address must be empty'}
    #
    # wrong behavior in 4.9.3, returns 'success'
    check_channel_behaviour('Offline', 'NOOC', @@wafer_values2, actions: [], aqvresult: 'fail')
  end

  def test0018b_ooc_statetest_offline_noca_nomail
    folder = $lds_setup.folder(@@templates_folder)
    ch = folder.spc_channel(name: @@params_template)
    ch.set_email_addresses('')
    ch.valuations.each {|v| assert_empty v.email_addresses, 'email address must be empty'}
    #
    check_channel_behaviour('Offline', 'OOC', @@wafer_values2ooc, actions: [], aqvresult: 'fail')
  end

  def test0019a_nooc_statetest_offline_noca_mail
    folder = $lds_setup.folder(@@templates_folder)
    ch = folder.spc_channel(name: @@params_template)
    ch.set_email_addresses(@@email_addresses)
    ch.valuations.each {|v| refute_empty v.email_addresses, 'email address must be configured'}
    #
    # wrong behavior in 4.9.3, returns 'success'
    check_channel_behaviour('Offline', 'NOOC', @@wafer_values2, actions: [], email: true, aqvresult: 'fail')
  end

  def test0019b_ooc_statetest_offline_noca_mail
    folder = $lds_setup.folder(@@templates_folder)
    ch = folder.spc_channel(name: @@params_template)
    ch.set_email_addresses(@@email_addresses)
    ch.valuations.each {|v| refute_empty v.email_addresses, 'email address must be configured'}
    #
    check_channel_behaviour('Offline', 'OOC', @@wafer_values2ooc, actions: [], email: true, aqvresult: 'fail')
  end

  def test0020a_nooc_statetest_4offline_1study
    check_channel_behaviour('Offline', 'NOOC', @@wafer_values2, exceptState: 'Study')
  end

  def test0020b_ooc_statetest_4offline_1study
    check_channel_behaviour('Offline', 'OOC', @@wafer_values2ooc, exceptState: 'Study')
  end

  # manual test
  def testman0021a_ooc_nooc_statetest_5offline_1study
    assert_equal 0, $sv.eqp_cleanup(@@eqp)
    assert_equal 0, $sv.lot_cleanup(@@lot)
    parameters = ['QA_PARAM_900_ECP', 'QA_PARAM_901_ECP', 'QA_PARAM_902_ECP', 'QA_PARAM_903_ECP', 'QA_PARAM_904_ECP']
    dcr = nil
    2.times {|i|
      dcrp = Space::DCR.new(eqp: @@eqp, eqptype: 'PROCESS-CHAMBER', lot: @@lot, op: nil, technology: 'TEST')  # @@ppd_qa was originally not set!
      dcrp.add_parameters(parameters, @@wafer_values2, nocharting: true)
      assert $spctest.send_dcr_verify(dcrp)

      dcr = Space::DCR.new(lot: @@lot, eqp: @@mtool, op: 'QA-MB-AQV-MultiLot-M.01', technology: 'TEST', route: 'AQV_ECP.01')
      dcr.add_parameters(parameters, @@wafer_values2)
      res = $client.send_dcr(dcr, export: :caller)

      if i == 0
        $log.info 'Please copy one of the existing channels in a way that 1 parameter will be in two channels and set one channel to Offline, the other one to Study then press any key to continue.'
        gets
      end
    }
    assert $spctest.verify_aqv_message(dcr: dcr, lot: @@lot, wafer: @@wafer_values2.keys.sort, result: 'success'), 'AQV message not as expected, expectedResult: success'
  end

  # manual test
  def testman0021b_ooc_nooc_statetest_5offline_1study_multilot
    assert_equal 0, $sv.eqp_cleanup(@@eqp)
    assert_equal 0, $sv.lot_cleanup(@@lot)
    parameters = ['QA_PARAM_900_ECP', 'QA_PARAM_901_ECP', 'QA_PARAM_902_ECP', 'QA_PARAM_903_ECP', 'QA_PARAM_904_ECP']

    lot2 = 'SIL' + @@sil_version.split('.').collect {|e| '%02d' % e}.join('') + '%02d' % @@test_run + '1' + '.000'
    assert lot2 = $svtest.new_lot(lot: lot2) unless $sv.lot_exists?(lot2)
    $log.info "using lot #{lot2.inspect} as second lot"

    dcr = nil
    2.times {|i|
      wv = @@wafer_values2 if i == 0
      wv = @@wafer_values2ooc if i == 1
      dcrp = Space::DCR.new(eqp: @@eqp, eqptype: 'PROCESS-CHAMBER', lot: @@lot, op: nil, technology: 'TEST')  # @@ppd_qa was originally not set!
      dcrp.add_parameters(parameters, wv, nocharting: true)
      dcrp.add_lot(lot2, op: nil, technology: 'TEST')
      dcrp.add_parameters(parameters, wv, nocharting: true)
      assert $spctest.send_dcr_verify(dcrp)

      dcr = Space::DCR.new(lot: @@lot, eqp: @@mtool, op: 'QA-MB-AQV-MultiLot-M.01', technology: 'TEST', route: 'AQV_ECP.01')
      dcr.add_parameters(parameters, wv)
      dcrp.add_lot(lot2, op: 'QA-MB-AQV-MultiLot-M.01', technology: 'TEST', route: 'AQV_ECP.01')
      dcrp.add_parameters(parameters, wv, nocharting: true)
      res = $client.send_dcr(dcr, export: :caller)

      if i == 0
        $log.info 'Please copy one of the existing channels in a way that 1 parameter will be in two channels and set one channel to Offline, the other one to Study then press any key to continue.'
        gets
        assert $spctest.verify_aqv_message(dcr: dcr, lot: @@lot, wafer: @@wafer_values2.keys.sort, result: 'success'), 'AQV message not as expected, expectedResult: success'
      elsif
        assert $spctest.verify_aqv_message(dcr: dcr, lot: @@lot, wafer: @@wafer_values2.keys.sort, result: 'fail'), 'AQV message not as expected, expectedResult: fail'
      end
    }
  end

  # manual test
  def testman0022a_nooc_statetest_offline_novaluation
    $log.info 'Please remove all valuations from the channel template (_Template_QA_PARAMS_900) then press any key to continue.'
    gets
    check_channel_behaviour('Offline', 'NOOC', @@wafer_values2, noca: true, actions: [], valuations: nil)
  end

  # manual test
  def testman0022b_ooc_statetest_offline_novaluation
    $log.info 'Please remove all valuations from the channel template (_Template_QA_PARAMS_900) then press any key to continue.'
    gets
    check_channel_behaviour('Offline', 'OOC', @@wafer_values2ooc, noca: true, actions: [], valuations: nil)
  end

  # chart check tests - needs further development:
  # - add derdack check
  # - check SIL chart check related switches
  # - add departments for testing different switch config at once
  # Chart Check Failure can be generated for example by
  #   - using study channels for active parameters
  #   - using offline channels with parameters without limits in PCP/ECP, but then Prerun State has to be false for channel and ckc
  #   - control plan exists, but no valuations
  # MSR1142273, MSR1079189

  # PCP, Study & Inline
  def test0030a_cc_inline_pcp_nooc_statetest_study
    parameters = ['QA_PARAM_900_PCP', 'QA_PARAM_901_PCP', 'QA_PARAM_902_PCP', 'QA_PARAM_903_PCP', 'QA_PARAM_904_PCP']
    # aqv result will be checked only if aqv is enabled for inline!
    check_channel_behaviour('Study', 'NOOC', @@wafer_values2, technology: '32NM', mpd: 'QA-MB-CC-M-PCP.01', ppd: 'QA-MB-CC.01',
      department: 'QACCALL', parameters: parameters, aqvresult: 'fail', ccholdexpected: true, ccderdackexpected: true)
  end

  # PCP, Offline & Inline
  def test0030b_cc_inline_pcp_nooc_statetest_offline
    parameters = ['QA_PARAM_900_PCP', 'QA_PARAM_901_PCP', 'QA_PARAM_902_PCP', 'QA_PARAM_903_PCP', 'QA_PARAM_904_PCP']
    # aqv result will be checked only if aqv is enabled for inline!
    check_channel_behaviour('Offline', 'NOOC', @@wafer_values2, technology: '32NM', mpd: 'QA-MB-CC-M-PCP.01', ppd: 'QA-MB-CC.01',
      department: 'QACCALL', parameters: parameters, aqvresult: 'success', ccholdexpected: false, ccderdackexpected: false)
  end

  # PCP, Study & Setup
  def test0030c_cc_testlds_pcp_nooc_statetest_study
    parameters = ['QA_PARAM_900_PCP', 'QA_PARAM_901_PCP', 'QA_PARAM_902_PCP', 'QA_PARAM_903_PCP', 'QA_PARAM_904_PCP']
    # aqv result will be checked only if aqv is enabled for setup!
    check_channel_behaviour('Study', 'NOOC', @@wafer_values2, technology: 'TEST', mpd: 'QA-MB-CC-M-PCP.01', ppd: 'QA-MB-CC.01',
      department: 'QACCALL', parameters: parameters, aqvresult: 'fail', ccholdexpected: true, ccderdackexpected: true)
  end

  # PCP, Offline & Setup
  def test0030d_cc_testlds_pcp_nooc_statetest_offline
    parameters = ['QA_PARAM_900_PCP', 'QA_PARAM_901_PCP', 'QA_PARAM_902_PCP', 'QA_PARAM_903_PCP', 'QA_PARAM_904_PCP']
    # aqv result will be checked only if aqv is enabled for setup!
    check_channel_behaviour('Offline', 'NOOC', @@wafer_values2, technology: 'TEST', mpd: 'QA-MB-CC-M-PCP.01', ppd: 'QA-MB-CC.01',
      department: 'QACCALL', parameters: parameters, aqvresult: 'success', ccholdexpected: false, ccderdackexpected: false)
  end

  # PCP, Study & Inline, Criticality = B (BACKGROUND)
  def test0030e_cc_inline_pcp_nooc_statetest_study
    parameters = ['QA_PARAM_900_PCP', 'QA_PARAM_901_PCP', 'QA_PARAM_902_PCP', 'QA_PARAM_903_PCP', 'QA_PARAM_904_PCP']
    check_channel_behaviour('Study', 'NOOC', @@wafer_values2, technology: '32NM', mpd: 'QA-MB-CC-M-BACKGROUND.01', ppd: 'QA-MB-CC.01',
      department: 'QACCALL', parameters: parameters, aqvresult: 'success', ccholdexpected: false, ccderdackexpected: false)
  end

  # PCP, Offline & Inline, Criticality = B (BACKGROUND)
  def test0030f_cc_inline_pcp_nooc_statetest_offline
    parameters = ['QA_PARAM_900_PCP', 'QA_PARAM_901_PCP', 'QA_PARAM_902_PCP', 'QA_PARAM_903_PCP', 'QA_PARAM_904_PCP']
    check_channel_behaviour('Offline', 'NOOC', @@wafer_values2, technology: '32NM', mpd: 'QA-MB-CC-M-BACKGROUND.01', ppd: 'QA-MB-CC.01',
      department: 'QACCALL', parameters: parameters, aqvresult: 'success', ccholdexpected: false, ccderdackexpected: false)
  end

  # PCP, Study & Setup, Criticality = B (BACKGROUND)
  def test0030g_cc_testlds_pcp_nooc_statetest_study
    parameters = ['QA_PARAM_900_PCP', 'QA_PARAM_901_PCP', 'QA_PARAM_902_PCP', 'QA_PARAM_903_PCP', 'QA_PARAM_904_PCP']
    check_channel_behaviour('Study', 'NOOC', @@wafer_values2, technology: 'TEST', mpd: 'QA-MB-CC-M-BACKGROUND.01', ppd: 'QA-MB-CC.01',
      department: 'QACCALL', parameters: parameters, aqvresult: 'success', ccholdexpected: false, ccderdackexpected: false)
  end

  # PCP, Offline & Setup, Criticality = B (BACKGROUND)
  def test0030h_cc_testlds_pcp_nooc_statetest_offline
    parameters = ['QA_PARAM_900_PCP', 'QA_PARAM_901_PCP', 'QA_PARAM_902_PCP', 'QA_PARAM_903_PCP', 'QA_PARAM_904_PCP']
    check_channel_behaviour('Offline', 'NOOC', @@wafer_values2, technology: 'TEST', mpd: 'QA-MB-CC-M-BACKGROUND.01', ppd: 'QA-MB-CC.01',
      department: 'QACCALL', parameters: parameters, aqvresult: 'success', ccholdexpected: false, ccderdackexpected: false)
  end

  # ECP, Study & Inline
  def test0031a_cc_inline_ecp_nooc_statetest_study
    parameters = ['QA_PARAM_900_ECP', 'QA_PARAM_901_ECP', 'QA_PARAM_902_ECP', 'QA_PARAM_903_ECP', 'QA_PARAM_904_ECP']
    check_channel_behaviour('Study', 'NOOC', @@wafer_values2, technology: '32NM', mpd: 'QA-MB-CC-M-ECP.01', ppd: 'QA-MB-CC.01',
      department: 'QACCALL', parameters: parameters, aqvresult: 'fail', ccholdexpected: true, ccderdackexpected: true)
  end

  # ECP, Offline & Inline
  def test0031b_cc_testlds_ecp_nooc_statetest_offline
    parameters = ['QA_PARAM_900_ECP', 'QA_PARAM_901_ECP', 'QA_PARAM_902_ECP', 'QA_PARAM_903_ECP', 'QA_PARAM_904_ECP'] #'QA_PARAM_901_ECP',
    check_channel_behaviour('Offline', 'NOOC', @@wafer_values2, technology: '32NM', mpd: 'QA-MB-CC-M-ECP.01', ppd: 'QA-MB-CC.01',
      department: 'QACCALL', parameters: parameters, aqvresult: 'success', ccholdexpected: false, ccderdackexpected: false)
  end

  # ECP, Study & Setup
  def test0031c_cc_testlds_ecp_multilot_nooc_statetest_study
    parameters = ['QA_PARAM_900_ECP'] #'QA_PARAM_901_ECP', 'QA_PARAM_902_ECP', 'QA_PARAM_903_ECP', 'QA_PARAM_904_ECP'
    check_channel_behaviour('Study', 'NOOC', @@wafer_values2, technology: 'TEST', mpd: 'QA-MB-CC-M-ECP.01', ppd: 'QA-MB-CC.01',
      department: 'QACCALL', parameters: parameters, multilot: true, aqvresult: 'fail', ccholdexpected: true, ccderdackexpected: true)
  end

  # ECP, Offline & Setup
  def test0031d_cc_testlds_ecp_multilot_nooc_statetest_offline
    parameters = ['QA_PARAM_900_ECP', 'QA_PARAM_901_ECP', 'QA_PARAM_902_ECP', 'QA_PARAM_903_ECP', 'QA_PARAM_904_ECP']
    check_channel_behaviour('Offline', 'NOOC', @@wafer_values2, technology: 'TEST', mpd: 'QA-MB-CC-M-ECP.01', ppd: 'QA-MB-CC.01',
      department: 'QACCALL', parameters: parameters, multilot: true, aqvresult: 'success', ccholdexpected: false, ccderdackexpected: false)
  end

  # ECP, Study & Inline, Criticality = B (BACKGROUND)
  def test0031e_cc_inline_ecp_nooc_statetest_study
    parameters = ['QA_PARAM_900_ECP', 'QA_PARAM_901_ECP', 'QA_PARAM_902_ECP', 'QA_PARAM_903_ECP', 'QA_PARAM_904_ECP']
    check_channel_behaviour('Study', 'NOOC', @@wafer_values2, technology: '32NM', mpd: 'QA-MB-CC-M-ECP-BACKGROUND.01', ppd: 'QA-MB-CC.01',
      department: 'QACCALL', parameters: parameters, aqvresult: 'success', ccholdexpected: false, ccderdackexpected: false)
  end

  # ECP, Offline & Inline, Criticality = B (BACKGROUND)
  def test0031f_cc_inline_ecp_nooc_statetest_offline
    parameters = ['QA_PARAM_900_ECP', 'QA_PARAM_901_ECP', 'QA_PARAM_902_ECP', 'QA_PARAM_903_ECP', 'QA_PARAM_904_ECP']
    check_channel_behaviour('Offline', 'NOOC', @@wafer_values2, technology: '32NM', mpd: 'QA-MB-CC-M-ECP-BACKGROUND.01', ppd: 'QA-MB-CC.01',
      department: 'QACCALL', parameters: parameters, aqvresult: 'success', ccholdexpected: false, ccderdackexpected: false)
  end

  # ECP, Study & Setup, Criticality = B (BACKGROUND)
  def test0031g_cc_testlds_ecp_nooc_statetest_study
    parameters = ['QA_PARAM_900_ECP', 'QA_PARAM_901_ECP', 'QA_PARAM_902_ECP', 'QA_PARAM_903_ECP', 'QA_PARAM_904_ECP']
    check_channel_behaviour('Study', 'NOOC', @@wafer_values2, technology: 'TEST', mpd: 'QA-MB-CC-M-ECP-BACKGROUND.01', ppd: 'QA-MB-CC.01',
      department: 'QACCALL', parameters: parameters, aqvresult: 'success', ccholdexpected: false, ccderdackexpected: false)
  end

  # ECP, Offline & Setup, Criticality = B (BACKGROUND)
  def test0031h_cc_testlds_ecp_nooc_statetest_offline
    parameters = ['QA_PARAM_900_ECP', 'QA_PARAM_901_ECP', 'QA_PARAM_902_ECP', 'QA_PARAM_903_ECP', 'QA_PARAM_904_ECP']
    check_channel_behaviour('Offline', 'NOOC', @@wafer_values2, technology: 'TEST', mpd: 'QA-MB-CC-M-ECP-BACKGROUND.01', ppd: 'QA-MB-CC.01',
      department: 'QACCALL', parameters: parameters, aqvresult: 'success', ccholdexpected: false, ccderdackexpected: false)
  end

  # Prerun, Inline, Study
  def testman0032a_cc_inline_prerun_nooc_statetest_study
    parameters = ['QA_PARAM_900_PRERUN', 'QA_PARAM_901_PRERUN', 'QA_PARAM_902_PRERUN', 'QA_PARAM_903_PRERUN', 'QA_PARAM_904_PRERUN']
    check_prerun_behaviour('Study', 'NOOC', @@wafer_values2, technology: '32NM',
      department: 'QACCALL', parameters: parameters, prerunoff: true, prerundcrs: 25, aqvresult: 'success', ccholdexpected: false, ccderdackexpected: false)
  end

  # Prerun, Inline, Offline
  def testman0032b_cc_inline_prerun_nooc_statetest_offline
    parameters = ['QA_PARAM_900_PRERUN', 'QA_PARAM_901_PRERUN', 'QA_PARAM_902_PRERUN', 'QA_PARAM_903_PRERUN', 'QA_PARAM_904_PRERUN']
    check_prerun_behaviour('Offline', 'NOOC', @@wafer_values2, technology: '32NM',
      department: 'QACCALL', parameters: parameters, prerunoff: true, prerundcrs: 25, aqvresult: 'success', ccholdexpected: false, ccderdackexpected: false)
  end

  # Prerun, Setup, Study
  def testman0032c_cc_testlds_prerun_nooc_statetest_stidy
    parameters = ['QA_PARAM_900_PRERUN', 'QA_PARAM_901_PRERUN', 'QA_PARAM_902_PRERUN', 'QA_PARAM_903_PRERUN', 'QA_PARAM_904_PRERUN']
    check_prerun_behaviour('Study', 'NOOC', @@wafer_values2, technology: 'TEST',
      department: 'QACCALL', parameters: parameters, prerunoff: true, prerundcrs: 25, aqvresult: 'success', ccholdexpected: false, ccderdackexpected: false)
  end

  # Prerun, Setup, Offline
  def testman0032d_cc_testlds_prerun_nooc_statetest_offline
    parameters = ['QA_PARAM_900_PRERUN', 'QA_PARAM_901_PRERUN', 'QA_PARAM_902_PRERUN', 'QA_PARAM_903_PRERUN', 'QA_PARAM_904_PRERUN']
    check_prerun_behaviour('Offline', 'NOOC', @@wafer_values2, technology: 'TEST',
      department: 'QACCALL', parameters: parameters, prerunoff: true, prerundcrs: 25, aqvresult: 'success', ccholdexpected: false, ccderdackexpected: false)
  end

  # No Control Plan, Test, Study
  def test0033a_cc_testlds_noControlPlan_study
    assert_equal 0, $sv.eqp_cleanup(@@eqp)
    assert_equal 0, $sv.lot_cleanup(@@lot)
    parameters = ['QA_PARAM_900_CC_noCP', 'QA_PARAM_901_CC_noCP', 'QA_PARAM_902_CC_noCP', 'QA_PARAM_903_CC_noCP', 'QA_PARAM_904_CC_noCP']
    check_channel_behaviour('Study', 'NOOC', @@wafer_values2, technology: 'TEST', mpd: 'QA-MB-CC-M-NOCP.01', ppd: 'QA-MB-CC.01',
      department: 'QACCALL', parameters: parameters, aqvresult: 'success', ccholdexpected: false)
  end

  # No Control Plan, Test, Offline
  def test0033b_cc_testlds_noControlPlan_offline
    assert_equal 0, $sv.eqp_cleanup(@@eqp)
    assert_equal 0, $sv.lot_cleanup(@@lot)
    parameters = ['QA_PARAM_900_CC_noCP', 'QA_PARAM_901_CC_noCP', 'QA_PARAM_902_CC_noCP', 'QA_PARAM_903_CC_noCP', 'QA_PARAM_904_CC_noCP']
    check_channel_behaviour('Offline', 'NOOC', @@wafer_values2, technology: 'TEST', mpd: 'QA-MB-CC-M-NOCP.01', ppd: 'QA-MB-CC.01',
      department: 'QACCALL', parameters: parameters, aqvresult: 'success', ccholdexpected: false)
  end

  # PCP, Space Scenario, Inline
  # All channels have entries from speclimits.ignorecontrolplan.spacescenariosettings OR chartcheck.ignorecontrolplan.spacescenariosettings
  # results in no chart check fail/no limits
  def test0034a_cc_inline_pcp_nooc_statetest_offline
    assert_equal 0, $sv.eqp_cleanup(@@eqp)
    assert_equal 0, $sv.lot_cleanup(@@lot)
    parameters = ['QA_PARAM_900', 'QA_PARAM_901', 'QA_PARAM_902', 'QA_PARAM_903', 'QA_PARAM_904']
    check_channel_behaviour('Offline', 'NOOC', @@wafer_values2, technology: '32NM', mpd: 'QA-MB-CC-M-STD.01', ppd: 'QA-MB-CC.01',
      department: 'QACCALL', parameters: parameters, aqvresult: 'success', ccholdexpected: false)
  end

  # PCP, Space Scenario, Inline
  # All channels have entries from chartcheck.ignorecontrolplan.spacescenariosettings (NoChart,NoCC)
  # results in no chart check fail
  def test0034b_cc_inline_pcp_nooc_statetest_offline
    assert_equal 0, $sv.eqp_cleanup(@@eqp)
    assert_equal 0, $sv.lot_cleanup(@@lot)
    parameters = ['QA_PARAM_900', 'QA_PARAM_901', 'QA_PARAM_902', 'QA_PARAM_903', 'QA_PARAM_904']
    check_channel_behaviour('Offline', 'NOOC', @@wafer_values2, technology: '32NM', mpd: 'QA-MB-CC-M-STD-NoCC.01', ppd: 'QA-MB-CC.01',
      department: 'QACCALL', parameters: parameters, aqvresult: 'success', ccholdexpected: false)
  end

  # PCP, Space Scenario, Inline
  # All channels have entries from speclimits.ignorecontrolplan.spacescenariosettings or chartcheck.ignorecontrolplan.spacescenariosettings (MANUAL_SPECLIMIT_SPACE, CALCULATION)
  # results in no chart check fail
  def test0034c_cc_inline_pcp_nooc_statetest_offline
    assert_equal 0, $sv.eqp_cleanup(@@eqp)
    assert_equal 0, $sv.lot_cleanup(@@lot)
    parameters = ['QA_PARAM_900', 'QA_PARAM_901', 'QA_PARAM_902', 'QA_PARAM_903', 'QA_PARAM_904']
    check_channel_behaviour('Offline', 'NOOC', @@wafer_values2, technology: '32NM', mpd: 'QA-MB-CC-M-STD-MANLIM.01', ppd: 'QA-MB-CC.01',
      department: 'QACCALL', parameters: parameters, aqvresult: 'success', ccholdexpected: false)
  end

  # PCP, Space Scenario, Inline
  # All channels have entries from speclimits.ignorecontrolplan.spacescenariosettings/chartcheck.ignorecontrolplan.spacescenariosettings/missingparametercheck.ignorecontrolplan.spacescenariosettings (IGNORE_ALL_PARAMETER_CHECK)
  # results in no chart check fail, no limits, no missing param fail
  def test0034d_cc_inline_pcp_nooc_statetest_offline
    assert_equal 0, $sv.eqp_cleanup(@@eqp)
    assert_equal 0, $sv.lot_cleanup(@@lot)
    parameters = ['QA_PARAM_900', 'QA_PARAM_901', 'QA_PARAM_902', 'QA_PARAM_903', 'QA_PARAM_904']
    check_channel_behaviour('Offline', 'NOOC', @@wafer_values2, technology: '32NM', mpd: 'QA-MB-CC-M-STD-IGNORE.01', ppd: 'QA-MB-CC.01',
      department: 'QACCALL', parameters: parameters, aqvresult: 'success', ccholdexpected: false)
  end

  # PCP, Space Scenario, Setup
  # All channels have entries from speclimits.ignorecontrolplan.spacescenariosettings OR chartcheck.ignorecontrolplan.spacescenariosettings
  # results in no chart check fail/no limits
  def test0034e_cc_inline_pcp_nooc_statetest_offline
    assert_equal 0, $sv.eqp_cleanup(@@eqp)
    assert_equal 0, $sv.lot_cleanup(@@lot)
    parameters = ['QA_PARAM_900', 'QA_PARAM_901', 'QA_PARAM_902', 'QA_PARAM_903', 'QA_PARAM_904']
    check_channel_behaviour('Offline', 'NOOC', @@wafer_values2, technology: 'TEST', mpd: 'QA-MB-CC-M-STD.01', ppd: 'QA-MB-CC.01',
      department: 'QACCALL', parameters: parameters, aqvresult: 'success', ccholdexpected: false)
  end

  # PCP, Space Scenario, Setup
  # All channels have entries from chartcheck.ignorecontrolplan.spacescenariosettings (NoChart,NoCC)
  # results in no chart check fail
  def test0034f_cc_inline_pcp_nooc_statetest_offline
    assert_equal 0, $sv.eqp_cleanup(@@eqp)
    assert_equal 0, $sv.lot_cleanup(@@lot)
    parameters = ['QA_PARAM_900', 'QA_PARAM_901', 'QA_PARAM_902', 'QA_PARAM_903', 'QA_PARAM_904']
    check_channel_behaviour('Offline', 'NOOC', @@wafer_values2, technology: 'TEST', mpd: 'QA-MB-CC-M-STD-NoCC.01', ppd: 'QA-MB-CC.01',
      department: 'QACCALL', parameters: parameters, aqvresult: 'success', ccholdexpected: false)
  end

  # PCP, Space Scenario, Setup
  # All channels have entries from speclimits.ignorecontrolplan.spacescenariosettings or chartcheck.ignorecontrolplan.spacescenariosettings (MANUAL_SPECLIMIT_SPACE, CALCULATION)
  # results in no chart check fail
  def test0034g_cc_inline_pcp_nooc_statetest_offline
    assert_equal 0, $sv.eqp_cleanup(@@eqp)
    assert_equal 0, $sv.lot_cleanup(@@lot)
    parameters = ['QA_PARAM_900', 'QA_PARAM_901', 'QA_PARAM_902', 'QA_PARAM_903', 'QA_PARAM_904']
    check_channel_behaviour('Offline', 'NOOC', @@wafer_values2, technology: 'TEST', mpd: 'QA-MB-CC-M-STD-MANLIM.01', ppd: 'QA-MB-CC.01',
      department: 'QACCALL', parameters: parameters, aqvresult: 'success', ccholdexpected: false)
  end

  # PCP, Space Scenario, Setup
  # All channels have entries from speclimits.ignorecontrolplan.spacescenariosettings/chartcheck.ignorecontrolplan.spacescenariosettings/missingparametercheck.ignorecontrolplan.spacescenariosettings (IGNORE_ALL_PARAMETER_CHECK)
  # results in no chart check fail, no limits, no missing param fail
  def test0034h_cc_inline_pcp_nooc_statetest_offline
    assert_equal 0, $sv.eqp_cleanup(@@eqp)
    assert_equal 0, $sv.lot_cleanup(@@lot)
    parameters = ['QA_PARAM_900', 'QA_PARAM_901', 'QA_PARAM_902', 'QA_PARAM_903', 'QA_PARAM_904']
    check_channel_behaviour('Offline', 'NOOC', @@wafer_values2, technology: 'TEST', mpd: 'QA-MB-CC-M-STD-IGNORE.01', ppd: 'QA-MB-CC.01',
      department: 'QACCALL', parameters: parameters, aqvresult: 'success', ccholdexpected: false)
  end

  # PCP, Space Scenario, Setup
  def test0034i_cc_inline_pcp_nooc_statetest_study
    assert_equal 0, $sv.eqp_cleanup(@@eqp)
    assert_equal 0, $sv.lot_cleanup(@@lot)
    parameters = ['QA_PARAM_900', 'QA_PARAM_901', 'QA_PARAM_902', 'QA_PARAM_903', 'QA_PARAM_904']
    check_channel_behaviour('Study', 'NOOC', @@wafer_values2, technology: 'TEST', mpd: 'QA-MB-CC-M-STD.01', ppd: 'QA-MB-CC.01', department: 'QACCALL', parameters: parameters, aqvresult: 'success', ccholdexpected: false)
  end

  # PCP, Space Scenario, Setup
  def test0034j_cc_inline_pcp_nooc_statetest_study
    assert_equal 0, $sv.eqp_cleanup(@@eqp)
    assert_equal 0, $sv.lot_cleanup(@@lot)
    parameters = ['QA_PARAM_900', 'QA_PARAM_901', 'QA_PARAM_902', 'QA_PARAM_903', 'QA_PARAM_904']
    check_channel_behaviour('Study', 'NOOC', @@wafer_values2, technology: 'TEST', mpd: 'QA-MB-CC-M-STD-NoCC.01', ppd: 'QA-MB-CC.01', department: 'QACCALL', parameters: parameters, aqvresult: 'success', ccholdexpected: false)
  end

  # PCP, Space Scenario, Setup
  def test0034k_cc_inline_pcp_nooc_statetest_study
    assert_equal 0, $sv.eqp_cleanup(@@eqp)
    assert_equal 0, $sv.lot_cleanup(@@lot)
    parameters = ['QA_PARAM_900', 'QA_PARAM_901', 'QA_PARAM_902', 'QA_PARAM_903', 'QA_PARAM_904']
    check_channel_behaviour('Study', 'NOOC', @@wafer_values2, technology: 'TEST', mpd: 'QA-MB-CC-M-STD-MANLIM.01', ppd: 'QA-MB-CC.01', department: 'QACCALL', parameters: parameters, aqvresult: 'success', ccholdexpected: false)
  end

  # PCP, Space Scenario, Setup
  def test0034l_cc_inline_pcp_nooc_statetest_study
    assert_equal 0, $sv.eqp_cleanup(@@eqp)
    assert_equal 0, $sv.lot_cleanup(@@lot)
    parameters = ['QA_PARAM_900', 'QA_PARAM_901', 'QA_PARAM_902', 'QA_PARAM_903', 'QA_PARAM_904']
    check_channel_behaviour('Study', 'NOOC', @@wafer_values2, technology: 'TEST', mpd: 'QA-MB-CC-M-STD-IGNORE.01', ppd: 'QA-MB-CC.01', department: 'QACCALL', parameters: parameters, aqvresult: 'success', ccholdexpected: false)
  end

  def test0099_templates
    folder = $lds.folder(@@templates_folder)
    [@@default_template_qa + '_renamed', @@params_template + '_renamed'].each {|t|
      ch = folder.spc_channel(t)
      ch.name = ch.name.sub('_renamed', '') if ch
    }
    test_ok = true
    [@@default_template, @@default_template_qa, @@params_template].each {|t|
      ($log.warn "template channel #{t.inspect} does not exist"; test_ok=false) unless folder.spc_channel(t)
    }
    #
    assert test_ok, 'original named test templates do not exist'
  end

  def check_channel_behaviour(state, type, values, params={}, &blk)
    assert_equal 0, $sv.eqp_cleanup(@@eqp)
    assert_equal 0, $sv.lot_cleanup(@@lot)
    caller = caller_locations(1,1)[0].label
    $log.info "-- check_channel_behaviour for state #{state.inspect}}"
    exceptState = params[:exceptState] || state
    technology = params[:technology] || 'TEST'
    parameters = params[:parameters] || @@parameters
    mpd = params[:mpd] || @@mpd_state
    ppd = params[:ppd] || nil
    department = params[:department] || 'QA'
    actions = params[:actions] || ['LotHold', 'AutoLotHold', 'ReleaseLotHold', 'CancelLotHold']
    actions = nil if params[:noca] == true
    email = params[:email]
    ccholdexpected = params[:ccholdexpected] || nil
    ccderdackexpected = params[:ccderdackexpected] || nil
    #
    $log.info "!!!!!\n     Attention: INLINE AQV is turned OFF in SIL Config, test results might not be meaningful enough!\n     !!!!!" if technology != 'TEST' && @@aqv_inline_active == false
    $log.info "!!!!!\n     Attention: SETUP AQV is turned OFF in SIL Config, test results might not be meaningful enough!\n     !!!!!" if technology == 'TEST' && @@aqv_setup_active == false
    #
    channels = create_clean_channels(params.merge(mpd: mpd, actions: actions, technology: technology, parameters: parameters))
    assert_equal 0, $sv.eqp_cleanup(@@eqp)
    assert_equal 0, $sv.lot_cleanup(@@lot)
    # set channels' state
    channels[0..-2].each {|ch| assert ch.set_state(state), "cannot set channel state #{state.inspect}"}
    assert channels[-1].set_state(exceptState), "cannot set channel state #{state.inspect}"
    # special channel setup
    blk.call(channels) if blk
    #
    sleep 10  # to separate DCRs for AQV
    # send process DCR
    dcrp = Space::DCR.new(eqp: @@eqp, eqptype: 'PROCESS-CHAMBER', lot: @@lot, technology: technology, op: ppd, department: department) # @@ppd_qa was originally not set!
    dcrp.add_parameters(parameters, @@wafer_values2, nocharting: true)

    lotcount = 1
    if params[:multilot]
      lotcount = 2
      lot2 = 'SIL' + @@sil_version.split('.').collect {|e| '%02d' % e}.join('') + '%02d' % @@test_run + '%02d' % 2 + '.000'
      assert lot2 = $svtest.new_lot(lot: lot2) unless $sv.lot_exists?(lot2)
      assert_equal 0, $sv.lot_cleanup(lot2)
      dcrp.add_lot(lot2, technology: technology, op: ppd, department: department)
      wvalues = @@wafer_values.values
      wafer_values2 = {}
      lot_wafers = $sv.lot_info(lot2,:wafers=>true).wafers.take(@@wafer_values.count)
      lot_wafers.each_with_index {|w,j| wafer_values2[w.wafer] = wvalues[j]}
      dcrp.add_parameters(parameters, wafer_values2, separate_lot: true, nocharting: true)
    end

    assert $spctest.send_dcr_verify(dcrp, exporttag: "#{caller}_p")
    # send DCR with OOC values
    dcr = Space::DCR.new(eqp: @@mtool, lot: @@lot, op: mpd, technology: technology, department: department)
    dcr.add_parameters(parameters, values)
    if params[:multilot]
      dcr.add_lot(lot2, technology: technology, op: mpd, department: department)
      wvalues = @@wafer_values.values
      wafer_values2 = {}
      lot_wafers = $sv.lot_info(lot2,:wafers=>true).wafers.take(@@wafer_values.count)
      lot_wafers.each_with_index {|w,j| wafer_values2[w.wafer] = wvalues[j]}
      dcr.add_parameters(parameters, wafer_values2, separate_lot: true)
    end
    res = $client.send_dcr(dcr, export: "log/#{caller}_m.xml")
    #
    if [state, exceptState].member?('Study') #  state == 'Study' or exceptState == 'Study'
      assert $spctest.verify_dcr_accepted(res, parameters.count*values.size*lotcount), 'DCR not submitted successfully'
      aqvresult = params[:aqvresult] || 'fail'
      if technology == 'TEST' && @@aqv_setup_active == true
        assert $spctest.verify_aqv_message(dcr: dcr, lot: @@lot, wafer: @@wafer_values2.keys.sort, result: aqvresult, exporttag: "#{caller}"),
          "AQV message not as expected, state: #{state}, expectedResult: #{aqvresult}, valuetype: #{type}"
      elsif technology != 'TEST' && @@aqv_inline_active == true
        assert $spctest.verify_aqv_message(dcr: dcr, lot: @@lot, wafer: @@wafer_values2.keys.sort, result: aqvresult, exporttag: "#{caller}"),
          "AQV message not as expected, state: #{state}, expectedResult: #{aqvresult}, valuetype: #{type}"
      end
    elsif state == 'Archived'
      aqvresult = params[:aqvresult] || 'fail'
      assert $spctest.verify_aqv_message(dcr: dcr, lot: @@lot, wafer: @@wafer_values2.keys.sort, result: aqvresult, exporttag: "#{caller}_p"),
        "AQV message not as expected, state: #{state}, expectedResult: #{aqvresult}, valuetype: #{type}"
    else
      assert $spctest.verify_dcr_accepted(res, parameters.count*values.size*lotcount), 'DCR not submitted successfully'
      aqvresult = params[:aqvresult]
      aqvresult ||= ((type == 'NOOC') && ((!actions.nil? && !actions.empty?) || email)) ? 'success' : 'fail'
      if technology == 'TEST' && @@aqv_setup_active == true
        assert $spctest.verify_aqv_message(dcr: dcr, lot: @@lot, wafer: @@wafer_values2.keys.sort, result: aqvresult, exporttag: "#{caller}_p"),
          "wrong AQV message, state: #{state}, expectedResult: #{aqvresult}, valuetype: #{type}, cas: #{!actions.nil? && !actions.empty?}"
      elsif technology != 'TEST' && @@aqv_inline_active == true
        assert $spctest.verify_aqv_message(dcr: dcr, lot: @@lot, wafer: @@wafer_values2.keys.sort, result: aqvresult, exporttag: "#{caller}_p"),
          "wrong AQV message, state: #{state}, expectedResult: #{aqvresult}, valuetype: #{type}, cas: #{!actions.nil? && !actions.empty?}"
      end
    end
    check_hold(ccholdexpected)
    check_derdack(ccderdackexpected, parameters, lotcount)
  end

  # It is important to get samples which are not flagged
  def check_prerun_behaviour(state, type, values, params={})
    assert_equal 0, $sv.eqp_cleanup(@@eqp)
    assert_equal 0, $sv.lot_cleanup(@@lot)
    caller = caller_locations(1,1)[0].label
    $log.info "-- check_prerun_behaviour for state #{state.inspect}}"
    technology = params[:technology] || 'TEST'
    parameters = params[:parameters] || @@parameters
    mpd = 'QA-MB-CC-M-PRERUN.1' # start value - counter will be added at the end
    ppd = 'QA-MB-CC.1' # start value - counter will be added at the end
    prerundcrs = params[:prerundcrs] || 1
    department = params[:department] || 'QA'
    actions = params[:actions] || ['LotHold', 'AutoLotHold', 'ReleaseLotHold', 'CancelLotHold']
    actions = nil if params[:noca] == true
    ccholdexpected = params[:ccholdexpected] || nil
    ccderdackexpected = params[:ccderdackexpected] || nil
    wafer_values2 = @@wafer_values2
    #
    $log.info "!!!!!\n     Attention: INLINE AQV is turned OFF in SIL Config, test results might not be meaningful enough!\n     !!!!!" if technology != 'TEST' && @@aqv_inline_active == false
    $log.info "!!!!!\n     Attention: SETUP AQV is turned OFF in SIL Config, test results might not be meaningful enough!\n     !!!!!" if technology == 'TEST' && @@aqv_setup_active == false
    #
    channels = create_clean_channels(params.merge(mpd: 'QA-MB-CC-M-PRERUN.101', actions: actions, technology: technology, parameters: parameters))
    assert_equal 0, $sv.eqp_cleanup(@@eqp)
    assert_equal 0, $sv.lot_cleanup(@@lot)
    # set channels' state
    channels.each {|ch| assert ch.set_state(state), "cannot set channel state #{state.inspect}"}
    #
    sleep 10  # to separate DCRs for AQV

    # send DCR with OOC values
    lot_wafers = $sv.lot_info(@@lot,:wafers=>true).wafers.take(@@wafer_values.count)
    dcr = nil
    count = 0
    prerundcrs.times {|i|
      $log.info "Sending DCR ##{i}"
      assert_equal 0, $sv.lot_cleanup(@@lot)
      wvalues = @@wafer_values.values
      wafer_values2 = {}
      wafer_append = ''
      wafer_append = '%02d' % (i+1) if prerundcrs > 1
      lot_wafers.each_with_index {|w,j| wafer_values2[w.wafer + wafer_append] = wvalues[j]}
      #
      dcrp = Space::DCR.new(eqp: @@eqp, eqptype: 'PROCESS-CHAMBER', lot: @@lot, technology: technology, op: ppd + wafer_append, department: department)
      dcrp.add_parameters(parameters, wafer_values2, nocharting: true)
      $spctest.send_dcr_verify(dcrp, exporttag: "#{caller}_p_#{i}")
      #
      dcr = Space::DCR.new(eqp: @@mtool, lot: @@lot, op: mpd + wafer_append, technology: technology, department: department)
      dcr.add_parameters(parameters, wafer_values2)
      res = $client.send_dcr(dcr, export: "log/#{caller}_m_#{i}.xml")
      count = i
      #assert $spctest.verify_dcr_accepted(res, parameters.count*values.size), 'DCR not submitted successfully'
    }
    $log.info "Please 1. enable LDS (#{technology}) prerun, 2. stop prerun at all channels, 3. disable LDS prerun again an press any key to continue."
    gets
    $log.info 'Sleeping 10 Minutes'
    sleep 600
    # send the last dcrs again
    count = count + 1
    wvalues = @@wafer_values.values
    wafer_values2 = {}
    wafer_append = '%02d' % (count)
    lot_wafers.each_with_index {|w,j| wafer_values2[w.wafer + wafer_append] = wvalues[j]}
    #
    dcrp = Space::DCR.new(eqp: @@eqp, eqptype: 'PROCESS-CHAMBER', lot: @@lot, technology: technology, op: ppd + wafer_append, department: department)
    dcrp.add_parameters(parameters, wafer_values2, nocharting: true)
    $spctest.send_dcr_verify(dcrp, exporttag: "#{caller}_p_#{count}")
    #
    dcr = Space::DCR.new(eqp: @@mtool, lot: @@lot, op: mpd + wafer_append, technology: technology, department: department)
    dcr.add_parameters(parameters, wafer_values2)
    res = $client.send_dcr(dcr, export: "log/#{caller}_m_#{count}.xml")
    #
    if [state].member?('Study')
      aqvresult = params[:aqvresult] || 'fail'
      if technology == 'TEST' && @@aqv_setup_active == true
        assert $spctest.verify_aqv_message(dcr: dcr, lot: @@lot, wafer: wafer_values2.keys.sort, result: aqvresult, exporttag: "#{caller}"),
          "AQV message not as expected, state: #{state}, expectedResult: #{aqvresult}, valuetype: #{type}"
      elsif technology != 'TEST' && @@aqv_inline_active == true
        assert $spctest.verify_aqv_message(dcr: dcr, lot: @@lot, wafer: wafer_values2.keys.sort, result: aqvresult, exporttag: "#{caller}"),
          "AQV message not as expected, state: #{state}, expectedResult: #{aqvresult}, valuetype: #{type}"
      end
    else
      aqvresult = params[:aqvresult]
      aqvresult ||= ((type == 'NOOC') && ((!actions.nil? && !actions.empty?) || email)) ? 'success' : 'fail'
      if technology == 'TEST' && @@aqv_setup_active == true
        assert $spctest.verify_aqv_message(dcr: dcr, lot: @@lot, wafer: wafer_values2.keys.sort, result: aqvresult, exporttag: "#{caller}_p"),
          "wrong AQV message, state: #{state}, expectedResult: #{aqvresult}, valuetype: #{type}, cas: #{!actions.nil? && !actions.empty?}"
      elsif technology != 'TEST' && @@aqv_inline_active == true
        assert $spctest.verify_aqv_message(dcr: dcr, lot: @@lot, wafer: wafer_values2.keys.sort, result: aqvresult, exporttag: "#{caller}_p"),
          "wrong AQV message, state: #{state}, expectedResult: #{aqvresult}, valuetype: #{type}, cas: #{!actions.nil? && !actions.empty?}"
      end
    end
    check_hold(ccholdexpected)
    check_derdack(ccderdackexpected, parameters, 1)
  end

  def check_hold(ccholdexpected)
    if ccholdexpected == true
      assert $spctest.verify_futurehold2(@@lot, "Chart Check issue for"), "lot must have a future hold"
    elsif ccholdexpected == false
      assert !$spctest.verify_futurehold2(@@lot, "Chart Check issue for"), "lot must have no future hold"
    end
  end

  def check_derdack(ccderdackexpected, parameters, lotcount)
    if ccderdackexpected == true
      assert $spctest.verify_notification('Chart Check Failures for Lot', nil, msgcount: lotcount), 'notification missing'
    elsif ccderdackexpected == false
      assert !$spctest.verify_notification('Chart Check Failures for Lot', nil, msgcount: lotcount), 'notification missing'
    end
  end

end
