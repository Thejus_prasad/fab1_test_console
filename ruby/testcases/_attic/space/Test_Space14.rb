=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: Steffen Steidten, 2011-11-25
=end

require_relative 'Test_Space_Setup'


# Test Space/SIL Remeasurement and Rework
class Test_Space14 < Test_Space_Setup
  #Flagging switches
  @@inline_remeasurement_autoflagging_enabled = true
  @@setup_remeasurement_autoflagging_enabled = true
  @@inline_rework_autoflagging_enabled = true
  @@setup_rework_autoflagging_enabled = true

  @@mpd = 'QA-MB-FTDEPM.01'
  @@mtool = 'QAMeas'
  @@ppd = nil
  @@seclot = nil
  @@remeasure_sleep = 5
  @@erf_mpd = 'e1234-QA-MB-FTDEPM-A0.01'
  @@erf_ppd = 'e1234-QA-MB-FTDEP-A0.01'
  @@erf_route = 'e1234-QASIL-A0.01'
  # SIL log: Calling Resp. PD Lookup with ERF_Original_PD: QA_MB-FTDEPM.01
  # 2016-06-17 09:10:01,729 INFO  [com.globalfoundries.sil.mes.util.siview.SiViewRemote](WorkManager(2)-8) BEGIN external system call: SiView#CS_TxBranchOperationForLotInq (lot = 'RW1466154590.01')
  # 2016-06-17 09:10:01,768 INFO  [com.globalfoundries.sil.mes.util.siview.SiViewRemote](WorkManager(2)-8) END external system call: SiView#CS_TxBranchOperationForLotInq returnCode=[1450] reasonText=[] messageText=[0000282E:Lot  information
  # has not been found. ].
  # 2016-06-17 09:10:01,768 DEBUG [com.globalfoundries.sil.mes.siview.SiViewServiceBean](WorkManager(2)-8) Located a main route
  # 2016-06-17 09:10:01,768 DEBUG [com.globalfoundries.sil.setup.SetupServiceBean](WorkManager(2)-8) MainPD for lotId RW1466154590.01 is:
  # 2016-06-17 09:10:01,768 DEBUG [com.globalfoundries.sil.setup.SetupServiceBean](WorkManager(2)-8) speclimitlookup.route.required is true
  # 2016-06-17 09:10:01,768 DEBUG [com.globalfoundries.sil.setup.SetupServiceBean](WorkManager(2)-8) pd is e1234-QA-MB-FTDEPM-A0.01 and technology is 32NM and route is  productgroup is QAPG1  product is SILPROD.01
  # 2016-06-17 09:10:01,774 DEBUG [com.globalfoundries.sil.setup.SetupServiceBean](WorkManager(2)-8) SpecLimits frolm main PD are []
  # 2016-06-17 09:10:01,779 DEBUG [com.globalfoundries.sil.processing.services.SpecLimitSetup](WorkManager(2)-8) [QA_PARAM_904, QA_PARAM_902, QA_PARAM_903, QA_PARAM_901, QA_PARAM_900] - Parameters returned by spec limit lookup
  # 2016-06-17 09:10:01,780 DEBUG [com.globalfoundries.sil.processing.services.SpecLimitSetup](WorkManager(2)-8) Finished Parameter & Spec limit lookup
  # 2016-06-17 09:10:01,880 DEBUG [com.globalfoundries.sil.processing.services.GenericKeySetup](WorkManager(2)-8) module - QA genericKeyDescription - [GenericKeyDescriptionImpl [id=100410], GenericKeyDescriptionImpl [id=99944], GenericKeyDes
  # criptionImpl [id=100411], GenericKeyDescriptionImpl [id=99945], GenericKeyDescriptionImpl [id=99943], GenericKeyDescriptionImpl [id=99946], GenericKeyDescriptionImpl [id=100412], GenericKeyDescriptionImpl [id=100409], GenericKeyDescripti
  # onImpl [id=100408], GenericKeyDescriptionImpl [id=100407]]
  # 2016-06-17 09:10:01,984 DEBUG [com.globalfoundries.sil.pdref.PDReferenceServiceBean](WorkManager(2)-8) lookup.resppd.usingproductcontext is true
  # 2016-06-17 09:10:01,984 DEBUG [com.globalfoundries.sil.pdref.PDReferenceServiceBean](WorkManager(2)-8) pd is e1234-QA-MB-FTDEPM-A0.01 productgroup is QAPG1  product is SILPROD.01
  # 2016-06-17 09:10:02,039 DEBUG [com.globalfoundries.sil.processing.services.ResponsibleRunSetup](WorkManager(2)-8) Calling Resp. PD Lookup with ERF_Original_PD: QA_MB-FTDEPM.01



  def setup
    super
    return unless $lds && self.class.class_variable_defined?(:@@lot) # bypass test0000_setup
    assert_equal 0, $sv.eqp_cleanup(@@eqp)
    assert_equal 0, $sv.lot_cleanup(@@lot)
    assert_equal 0, $sv.lot_cleanup(@@seclot) if @@seclot
    #
    $lds.all_spc_channels('QA_PARAM_90*').each {|ch| assert ch.delete}
    folder = $lds.folder(@@autocreated_qa)
    assert folder.delete_channels, 'error cleaning channels' if folder
    #
    $lds_setup.all_spc_channels('QA_PARAM_90*').each {|ch| assert ch.delete}
    folder = $lds_setup.folder(@@autocreated_qa)
    assert folder.delete_channels, 'error cleaning channels' if folder
  end


  def test1400_setup
    $setup_ok = false
    #
    @@seclot = 'SILSL' + @@sil_version.split('.').collect {|e| '%02d' % e}.join('_') + '_%02d' % @@test_run + '.00'
    assert @@seclot = $svtest.new_lot(lot: @@seclot) unless $sv.lot_exists?(@@seclot)
    $log.info "using 2nd lot #{@@seclot}"
    #
    @@mpd, @@ppd = @@mpd_qa, @@ppd_qa
    #
    # props = $spctest.silserver.read_jobprops
    # assert_equal @@inline_rework_autoflagging_enabled.to_s, props[:'inline.rework.autoflagging.enabled'], 'wrong setup'
    # assert_equal @@setup_rework_autoflagging_enabled.to_s, props[:'setup.rework.autoflagging.enabled'], 'wrong setup'
    #
    $setup_ok = true
  end

  def test1401_remeasure
    parameters = @@parameters.collect {|p| p + '_REMEASURE'}
    submit_process_dcr(@@ppd)
    #
    3.times {|i| remeasure_scenario("log/#{__method__}_m_#{i+1}.xml", parameters: parameters)}
  end

  def test1402_3wafers
    submit_process_dcr(@@ppd)
    #
    remeasure_scenario("log/#{__method__}_m_1.xml")
    remeasure_scenario("log/#{__method__}_m_2.xml", wafer_values: Hash[@@wafer_values.to_a[0..2]])
    remeasure_scenario("log/#{__method__}_m_3.xml", wafer_values: Hash[@@wafer_values.to_a[0..2]], mtool: 'QAMeas2')
    remeasure_scenario("log/#{__method__}_m_4.xml", wafer_values: Hash[@@wafer_values.to_a[2..4]])
  end

  def test1403_multilot
    # build and submit DCR for process PD
    wv1 = Hash[@@wafer_values.keys.slice(0..2).collect {|e| [e, @@wafer_values[e]]}]
    wv2 = Hash[@@wafer_values.keys.slice(3..5).collect {|e| [e, @@wafer_values[e]]}]
    submit_process_dcr(@@ppd, wafer_values: wv1, addlots: [@@seclot], addwafer_values: [wv2])
    #
    remeasure_scenario("log/#{__method__}_m_1.xml", lot: @@seclot, wafer_values: wv2)
    remeasure_scenario("log/#{__method__}_m_2.xml", lot: @@seclot, wafer_values: wv2)
    remeasure_scenario("log/#{__method__}_m_3.xml", wafer_values: wv1)
  end

  def test1404_referenced_pd
    referenced_pd_scenario
  end

  def test1405_internalflag_and_remeasure
    skip 'inline.remeasurement.autoflagging.enabled=false' unless @@inline_remeasurement_autoflagging_enabled
    #
    submit_process_dcr(@@ppd)
    #
    3.times {|i| remeasure_scenario("log/#{__method__}_m_#{i+1}.xml", internalflag: true)}
  end

  def test1406_remeasure_test
    parameters = @@parameters.collect {|p| p + '_REMEASURE'}
    submit_process_dcr(@@ppd, technology: 'TEST')
    #
    3.times {|i| remeasure_scenario("log/#{__method__}_m_#{i+1}.xml", parameters: parameters, technology: 'TEST')}
  end

  def test1407_3wafers_test
    # build and submit DCR for process PD
    submit_process_dcr(@@ppd, eqp: @@eqp, technology: 'TEST', export: "log/#{__method__}_p.xml")
    #
    remeasure_scenario("log/#{__method__}_m_1.xml", technology: 'TEST')
    remeasure_scenario("log/#{__method__}_m_2.xml", wafer_values: Hash[@@wafer_values.to_a[0..2]], technology: 'TEST')
    remeasure_scenario("log/#{__method__}_m_3.xml", wafer_values: Hash[@@wafer_values.to_a[0..2]], mtool: 'QAMeas2', technology: 'TEST')
    remeasure_scenario("log/#{__method__}_m_4.xml", wafer_values: Hash[@@wafer_values.to_a[2..4]], technology: 'TEST')
  end

  def test1408_multilot_test
    # build and submit DCR for process PD
    wv1 = Hash[@@wafer_values.keys.slice(0..2).collect {|e| [e, @@wafer_values[e]]}]
    wv2 = Hash[@@wafer_values.keys.slice(3..5).collect {|e| [e, @@wafer_values[e]]}]
    submit_process_dcr(@@ppd, wafer_values: wv1, addlots: [@@seclot], addwafer_values: [wv2], technology: 'TEST')
    #
    remeasure_scenario("log/#{__method__}_m_1.xml", lot: @@seclot, wafer_values: wv2, technology: 'TEST')
    remeasure_scenario("log/#{__method__}_m_2.xml", lot: @@seclot, wafer_values: wv2, technology: 'TEST')
    remeasure_scenario("log/#{__method__}_m_3.xml", wafer_values: wv1, technology: 'TEST')
  end

  def test1409_referenced_pd_test
    referenced_pd_scenario(technology: 'TEST')
  end

  def test1410_internalflag_and_remeasure_test
    skip 'inline.remeasurement.autoflagging.enabled=false' unless @@inline_remeasurement_autoflagging_enabled
    # build and submit DCR for process PD
    submit_process_dcr(@@ppd, technology: 'TEST')
    #
    3.times {|i| remeasure_scenario("log/#{__method__}_m_#{i+1}.xml", internalflag: true, technology: 'TEST')}
  end

  def test1411_rework
    skip 'inline.rework.autoflagging.enabled=false' unless @@inline_rework_autoflagging_enabled
    #
    parameters = @@parameters.collect {|p| p + '_REWORK'}
    #
    lot = 'RW' + Time.now.to_i.to_s + '.000'
    assert lot = $svtest.new_lot(lot: lot) unless $sv.lot_exists?(lot)
    $log.info "using lot #{lot.inspect} as second lot"
    #
    wafer_values = {}
    @@wafer_values.values.each_with_index {|v, i| wafer_values["#{lot[0..9]}%02d" % i] = v}
    $log.info "#{lot}, #{wafer_values.inspect}"

    3.times {|i| rework_scenario(__method__, i, lot, wafer_values, parameters: parameters)}
  end

  def test1412_rework_3wafers
    skip 'inline.rework.autoflagging.enabled=false' unless @@inline_rework_autoflagging_enabled
    #
    lot = 'RW' + Time.now.to_i.to_s + '.000'
    assert lot = $svtest.new_lot(lot: lot) unless $sv.lot_exists?(lot)
    $log.info "using lot #{lot.inspect} as second lot"
    #
    wafer_values = {}
    @@wafer_values.values.each_with_index {|v, i| wafer_values["#{lot[0..9]}%02d" % i] = v}
    $log.info "#{lot}, #{wafer_values.inspect}"

    rework_scenario(__method__, 1, lot, wafer_values)
    rework_scenario(__method__, 2, lot, Hash[wafer_values.to_a[0..2]], lot: true)
    rework_scenario(__method__, 3, lot, Hash[wafer_values.to_a[0..2]], mtool: 'QAMeas2', lot: true)
    rework_scenario(__method__, 4, lot, Hash[wafer_values.to_a[2..4]], lot: true)
  end

  def test1413_rework_splitlots
    skip 'inline.rework.autoflagging.enabled=false' unless @@inline_rework_autoflagging_enabled
    #
    lot = 'RW' + Time.now.to_i.to_s + '.000'
    assert lot = $svtest.new_lot(lot: lot) unless $sv.lot_exists?(lot)
    $log.info "using lot #{lot.inspect} as second lot"
    #
    wafer_values = {}
    @@wafer_values.values.each_with_index {|v, i| wafer_values["#{lot[0..9]}%02d" % i] = v}
    $log.info "#{lot}, #{wafer_values.inspect}"

    first_child = $sv.lot_split(lot, 3)
    sec_child = $sv.lot_split(lot, 3)
    third_child = $sv.lot_split(lot, 3)

    rework_scenario(__method__, 1, "#{lot}", wafer_values)
    rework_scenario(__method__, 2, "#{first_child}", Hash[wafer_values.to_a[0..2]])
    rework_scenario(__method__, 3, "#{sec_child}", Hash[wafer_values.to_a[0..2]], mtool: 'QAMeas2')
    rework_scenario(__method__, 4, "#{third_child}", Hash[wafer_values.to_a[2..4]])
  end

  def test1414_rework_pilot
    skip 'inline.rework.autoflagging.enabled=false' unless @@inline_rework_autoflagging_enabled
    #
    lot = 'RW' + Time.now.to_i.to_s + '.000'
    assert lot = $svtest.new_lot(lot: lot) unless $sv.lot_exists?(lot)
    $log.info "using lot #{lot.inspect} as second lot"
    #
    wafer_values = {}
    @@wafer_values.values.each_with_index {|v, i| wafer_values["#{lot[0..9]}%02d" % i] = v}
    $log.info "#{lot}, #{wafer_values.inspect}"

    first_child = $sv.lot_split(lot, 3)

    rework_scenario(__method__, 1, "#{first_child}", Hash[wafer_values.to_a[0..0]])
    rework_scenario(__method__, 2, "#{first_child}", Hash[wafer_values.to_a[0..0]])
    rework_scenario(__method__, 3, "#{lot}", Hash[wafer_values.to_a[1..5]])
  end

  def test1415_rework_multilot
    skip 'inline.rework.autoflagging.enabled=false' unless @@inline_rework_autoflagging_enabled
    #
    seclot = 'SL' + Time.now.to_i.to_s + '.000'
    assert seclot = $svtest.new_lot(lot: seclot) unless $sv.lot_exists?(seclot)
    $log.info "using lot #{seclot.inspect} as second lot"
    sl_first_child = $sv.lot_split(seclot, 3)
    #
    #
    lot = 'RW' + Time.now.to_i.to_s + '.000'
    assert lot = $svtest.new_lot(lot: lot) unless $sv.lot_exists?(lot)
    $log.info "using lot #{lot.inspect} as second lot"
    first_child = $sv.lot_split(lot, 3)
    #
    wafer_values = {}
    @@wafer_values.values.each_with_index {|v, i| wafer_values["#{lot[0..9]}%02d" % i] = v}
    $log.info "#{lot}, #{wafer_values.inspect}"
    wv1 = Hash[wafer_values.keys.slice(0..2).collect {|e| [e,wafer_values[e]]}]
    wv2 = Hash[wafer_values.keys.slice(3..5).collect {|e| [e,wafer_values[e]]}]

    rework_scenario(__method__, 1, "#{first_child}", wv1, add_lot: ["#{sl_first_child}"], wafer_values: [wv2])
    rework_scenario(__method__, 2, "#{first_child}", wv1, add_lot: ["#{sl_first_child}"], wafer_values: [wv2])
    rework_scenario(__method__, 3, "#{sl_first_child}", wv2)
    rework_scenario(__method__, 4, "#{first_child}", wv1)
  end

  def test1416_rework_erf_lots1
    lot1 = 'RWSIL' + @@sil_version.split('.').collect {|e| '%02d' % e}.join('') + '%02d' % @@test_run + '%02d' % '1' + '.000'
    lot1 = $svtest.new_lot(lot: lot1) unless $sv.lot_exists?(lot1)
    lot0 = 'RWSIL' + @@sil_version.split('.').collect {|e| '%02d' % e}.join('') + '%02d' % @@test_run + '%02d' % '0' + '.000'
    lot0 = $svtest.new_lot(lot: lot0) unless $sv.lot_exists?(lot0)

    $sv.lot_opelocate lot1, '2500.1000'
    $sv.lot_opelocate lot0, '2500.1000'
    $sv.lot_branch lot1, "e1234-QASIL-A0.01"

    values = @@wafer_values.values
    wafer_values1 = {}
    lot_wafers = $sv.lot_info(lot1,:wafers=>true).wafers.take(@@wafer_values.count)
    lot_wafers.each_with_index {|w,j| wafer_values1[w.wafer] = values[j]}
    $log.info "#{lot1}, #{wafer_values1.inspect}"

    wafer_values0 = {}
    lot_wafers = $sv.lot_info(lot0,:wafers=>true).wafers.take(@@wafer_values.count)
    lot_wafers.each_with_index {|w,j| wafer_values0[w.wafer] = values[j]}
    $log.info "#{lot0}, #{wafer_values0.inspect}"

    rework_scenario(__method__, 1, "#{lot1}", wafer_values1, mpd: @@erf_mpd, route: @@erf_route)
    rework_scenario(__method__, 2, "#{lot0}", wafer_values0)
  end

  def test1417_rework_erf_lots2
    lot1 = 'RWSIL' + @@sil_version.split('.').collect {|e| '%02d' % e}.join('') + '%02d' % @@test_run + '%02d' % '1' + '.000'
    lot1 = $svtest.new_lot(lot: lot1) unless $sv.lot_exists?(lot1)
    first_child = $sv.lot_split(lot1, 3)
    lot0 = 'RWSIL' + @@sil_version.split('.').collect {|e| '%02d' % e}.join('') + '%02d' % @@test_run + '%02d' % '0' + '.000'
    lot0 = $svtest.new_lot(lot: lot0) unless $sv.lot_exists?(lot0)

    $sv.lot_opelocate lot1, '2500.1000'
    $sv.lot_opelocate lot0, '2500.1000'
    $sv.lot_branch lot1, "e1234-QASIL-A0.01"

    values = @@wafer_values.values
    wafer_values1 = {}
    lot_wafers = $sv.lot_info(lot1,:wafers=>true).wafers.take(@@wafer_values.count)
    lot_wafers.each_with_index {|w,j| wafer_values1[w.wafer] = values[j]}
    $log.info "#{lot1}, #{wafer_values1.inspect}"

    wafer_values0 = {}
    lot_wafers = $sv.lot_info(lot0,:wafers=>true).wafers.take(@@wafer_values.count)
    lot_wafers.each_with_index {|w,j| wafer_values0[w.wafer] = values[j]}
    $log.info "#{lot0}, #{wafer_values0.inspect}"

    rework_scenario(__method__, 1, "#{first_child}", wafer_values1, ppd: @@erf_ppd, route: @@erf_route)
    rework_scenario(__method__, 2, "#{lot0}", wafer_values0)
  end

  def test1418_rework_testlds
    skip 'setup.rework.autoflagging.enabled=false' unless @@setup_rework_autoflagging_enabled
    #
    parameters = @@parameters.collect {|p| p + '_REWORK'}
    #
    lot = 'RW' + Time.now.to_i.to_s + '.000'
    assert lot = $svtest.new_lot(lot: lot) unless $sv.lot_exists?(lot)
    $log.info "using lot #{lot.inspect} as second lot"
    #
    wafer_values = {}
    @@wafer_values.values.each_with_index {|v, i| wafer_values["#{lot[0..11]}%02d" % i] = v}
    $log.info "#{lot}, #{wafer_values.inspect}"

    3.times {|i| rework_scenario(__method__, i, lot, wafer_values, parameters: parameters, technology: 'TEST')}
  end

  def test1419_no_rework
    skip 'setup.rework.autoflagging.enabled=false' unless @@setup_rework_autoflagging_enabled
    #
    parameters = @@parameters.collect {|p| p + '_REWORK'}
    20.times {|j|
      #
      lot = 'RW' + Time.now.to_i.to_s + '.000'
      assert lot = $svtest.new_lot(lot: lot) unless $sv.lot_exists?(lot)
      #
      wafer_values = {}
      @@wafer_values.values.each_with_index {|v, i| wafer_values["#{lot[0..11]}%02d" % i] = v}
      $log.info "#{lot}, #{wafer_values.inspect}"

      rework_scenario(__method__, j+1, lot, wafer_values, parameters: parameters, rework: false)
    }
  end

  def test1420_rework_testlds
    skip 'setup.rework.autoflagging.enabled=false' unless @@setup_rework_autoflagging_enabled
    #
    parameters = @@parameters.collect {|p| p + '_REWORK'}
    #
    lot = 'RW' + Time.now.to_i.to_s + '.000'
    assert lot = $svtest.new_lot(lot: lot) unless $sv.lot_exists?(lot)
    $log.info "using lot #{lot.inspect} as second lot"
    #
    wafer_values = {}
    @@wafer_values.values.each_with_index {|v, i| wafer_values["#{lot[0..11]}%02d" % i] = v}
    $log.info "#{lot}, #{wafer_values.inspect}"

    20.times {|j|
      rework_scenario(__method__, j+1, lot, wafer_values, parameters: parameters, technology: 'TEST')
    }
  end


  def remeasure_scenario(export, params={})
    lot = params[:lot] || @@lot
    wafer_values = params[:wafer_values] || @@wafer_values
    mpd = params[:mpd] || @@mpd
    mtool = params[:mtool] || @@mtool
    technology = params[:technology] || '32NM'
    internalflag = !!params[:internalflag]
    parameters = params[:parameters] || @@parameters
    folder = $lds.folder(@@autocreated_qa) if technology != 'TEST'
    folder = $lds_setup.folder(@@autocreated_qa) if technology == 'TEST'

    assert_equal 0, $sv.eqp_cleanup(@@eqp)
    assert_equal 0, $sv.lot_cleanup(lot)

    old_nsamples = {}
    parameters.each {|p|
      folder = $lds.folder(@@autocreated_qa) if technology != 'TEST'
      folder = $lds_setup.folder(@@autocreated_qa) if technology == 'TEST'
      ch = folder.spc_channel(parameter: p)
      old_nsamples[p] = ch ? ch.samples.size : 0
      ch.samples.each {|s| s.flag} if internalflag && ch
    }
    #
    # build and submit DCR for related MPD
    dcr = Space::DCR.new(eqp: mtool, lot: lot, op: mpd, technology: technology)
    dcr.add_parameters(parameters, wafer_values, processing_areas: ['CHC'])
    res = $client.send_dcr(dcr, export: export)
    assert $spctest.verify_dcr_accepted(res, parameters.size * wafer_values.size), 'DCR not submitted successfully'
    # neccessary because of some chaing mechanism
    folder = $lds.folder(@@autocreated_qa) if technology != 'TEST'
    folder = $lds_setup.folder(@@autocreated_qa) if technology == 'TEST'
    # Need to wait for flagging
    $log.info "waiting #{@@remeasure_sleep}s for flagging service"; sleep @@remeasure_sleep
    #
    # check for flags
    parameters.each {|p|
      folder = $lds.folder(@@autocreated_qa) if technology != 'TEST'
      folder = $lds_setup.folder(@@autocreated_qa) if technology == 'TEST'
      ch = folder.spc_channel(parameter: p)
      samples = ch.samples
      assert_equal (old_nsamples[p] + wafer_values.size), samples.size, "wrong number of samples in #{ch.inspect}"
      if technology != 'TEST'
        if @@inline_remeasurement_autoflagging_enabled
          assert verify_channel_remeasured(ch), 'error in autoflagging, samples have to be flagged for remeasurement'
        else
          if old_nsamples[p] > 0
            samples[0..(old_nsamples[p]-1)].each {|s|
              assert verify_remeasured(s, false), 'error in autoflagging, samples do not have to be flagged for remeasurement'
            }
          end
        end
      else
        if @@setup_remeasurement_autoflagging_enabled
          assert verify_channel_remeasured(ch), 'error in autoflagging, samples have to be flagged for remeasurement'
        else
          if old_nsamples[p] > 0
            samples[0..(old_nsamples[p]-1)].each {|s|
              assert verify_remeasured(s, false), 'error in autoflagging, samples do not have to be flagged for remeasurement'
            }
          end
        end
      end
    }
  end

  def rework_scenario(method, i, lot, wafer_values, params={})
    mpd = params[:mpd] || @@mpd
    ppd = params[:ppd] || @@ppd
    mtool = params[:mtool] || @@mtool
    technology = params[:technology] || '32NM'
    parameters = params[:parameters] || @@parameters
    route = params[:route] || 'SIL-0001.01'
    rework = true
    if params[:rework]
      rework = (params[:rework] and rework)
    end
    $log.info "rework expected: #{rework}"
    folder = $lds.folder(@@autocreated_qa) if technology != 'TEST'
    folder = $lds_setup.folder(@@autocreated_qa) if technology == 'TEST'

    assert_equal 0, $sv.eqp_cleanup(@@eqp)
    assert_equal 0, $sv.lot_cleanup(lot)
    #
    old_nsamples = {}
    parameters.each {|p|
      ch = folder.spc_channel(parameter: p)
      old_nsamples[p] = ch ? ch.samples.size : 0
    }
    #
    # build and submit DCR for process PD
    submit_process_dcr(ppd, lot: lot, wafer_values: wafer_values, export: "log/#{method}_p_#{i}.xml",
        addlots: params[:add_lot], addwafer_values: params[:wafer_values], technology: technology)
    #
    # build and submit DCR for related MPD
    dcr = Space::DCR.new(eqp: mtool, lot: lot, op: mpd, route: route, technology: technology)
    dcr.add_parameters(parameters, wafer_values, processing_areas: ['CHC'])
    if params[:add_lot] && params[:wafer_values]
      dcr.add_lot(params[:add_lot][0], op: mpd, technology: technology)
      dcr.add_parameters(parameters, params[:wafer_values][0], processing_areas: ['CHC'])
      assert_equal 0, $sv.lot_cleanup(params[:add_lot][0])
    end
    #
    assert_equal 0, $sv.lot_cleanup(lot)
    #
    res = $client.send_dcr(dcr, export: "log/#{method}_m_#{i}.xml")
    wsize = wafer_values.size
    wsize += params[:wafer_values][0].size if params[:wafer_values]
    assert $spctest.verify_dcr_accepted(res, parameters.size * wsize), "DCR #{i} not submitted successfully"
    # neccessary because of some chaing mechanism
    folder = $lds.folder(@@autocreated_qa) if technology != 'TEST'
    folder = $lds_setup.folder(@@autocreated_qa) if technology == 'TEST'
    #
    # in 1.10 autoflagging seems to need some time when called after a while
    $log.info "waiting #{@@remeasure_sleep}s for flagging service"; sleep @@remeasure_sleep
    # check for flags
    parameters.each {|p|
      assert ch = folder.spc_channel(parameter: p), "no channel for #{p} in #{folder}"
      samples = ch.samples
      assert_equal old_nsamples[p] + wsize, samples.size, "wrong number of samples in #{ch.inspect}"
      if technology != 'TEST'
        if @@inline_rework_autoflagging_enabled && rework
          assert verify_channel_reworked(ch, lot: params[:lot]), "error in autoflagging after DCR #{i}, samples have to be flagged for rework"
        else
          if old_nsamples[p] > 0
            samples[0..(old_nsamples[p]-1)].each {|s|
              assert verify_reworked(s,false), 'error in autoflagging, samples do not have to be flagged for rework'
            }
          end
        end
        if @@inline_remeasurement_autoflagging_enabled
          assert verify_channel_remeasured(ch), 'error in autoflagging, samples have to be flagged for remeasurement'
        else
          if old_nsamples[p] > 0
            samples[0..(old_nsamples[p]-1)].each {|s|
              assert verify_remeasured(s, false), 'error in autoflagging, samples do not have to be flagged for remeasurement'
            }
          end
        end
      else
        if @@setup_rework_autoflagging_enabled && rework
          assert verify_channel_reworked(ch, lot: params[:lot]), "error in autoflagging after DCR #{i}, samples have to be flagged for rework"
        else
          if old_nsamples[p] > 0
            samples[0..(old_nsamples[p]-1)].each {|s|
              assert verify_reworked(s,false), 'error in autoflagging, samples do not have to be flagged for rework'
            }
          end
        end
        if @@setup_remeasurement_autoflagging_enabled
          assert verify_channel_remeasured(ch), 'error in autoflagging, samples have to be flagged for remeasurement'
        else
          if old_nsamples[p] > 0
            samples[0..(old_nsamples[p]-1)].each {|s|
              assert verify_remeasured(s, false), 'error in autoflagging, samples do not have to be flagged for remeasurement'
            }
          end
        end
      end
    }
  end

  def referenced_pd_scenario(params={})
    caller = caller_locations(1,1)[0].label
    mdcrcount = params[:mdcrcount] || 5
    technology = params[:technology] || '32NM'
    folder = $lds.folder(@@autocreated_qa) if technology != 'TEST'
    folder = $lds_setup.folder(@@autocreated_qa) if technology == 'TEST'

    mdcrcount.times {|i|
      assert_equal 0, $sv.eqp_cleanup(@@eqp)
      assert_equal 0, $sv.lot_cleanup(@@lot)
      assert_equal 0, $sv.lot_cleanup(@@seclot) if @@seclot

      count = i + 1
      # build and submit DCR for process PD
      submit_process_dcr(@@ppd, technology: technology, export: "log/#{caller}_p_1.xml") if count == 1
      submit_process_dcr(@@ppd, technology: technology, export: "log/#{caller}_p_2.xml") if count == 4
      old_nsamples = {}
      @@parameters.each {|p|
        ch = folder.spc_channel(parameter: p)
        old_nsamples[p] = ch ? ch.samples.size : 0
      }
      # build and submit DCR for related MPD
      dcr = Space::DCR.new(eqp: @@mtool, lot: @@lot, op: @@mpd, technology: technology)
      dcr.add_parameters(@@parameters, @@wafer_values, processing_areas: ['CHC'])
      res = $client.send_dcr(dcr, export: "log/#{caller}_m_#{count}.xml")
      assert $spctest.verify_dcr_accepted(res, @@parameters.size * @@wafer_values.size), 'DCR not submitted successfully'
      # neccessary because of some chaing mechanism
      folder = $lds.folder(@@autocreated_qa) if technology != 'TEST'
      folder = $lds_setup.folder(@@autocreated_qa) if technology == 'TEST'
      #
      # check for flags
      $log.info "waiting #{@@remeasure_sleep}s for flagging service"; sleep @@remeasure_sleep
      @@parameters.each {|p|
        ch = folder.spc_channel(parameter: p)
        samples = ch.samples
        assert_equal count*@@wafer_values.size, samples.size, "wrong number of samples in #{ch.inspect}"
        if @@setup_remeasurement_autoflagging_enabled
          assert verify_channel_remeasured(ch), "error in autoflagging after DCR #{count}, samples have to be flagged for remeasurement"
        else
          if old_nsamples[p] > 0
            samples[0..(old_nsamples[p]-1)].each {|s|
              assert verify_remeasured(s, false), "error in autoflagging after DCR #{count}, samples do not have to be flagged for remeasurement"
            }
          end
        end
      }
    }
  end

  # criteria for remeasurement: Sample time, PTool, POperation, Lot, Wafer
  def verify_channel_remeasured(ch)
    rem_hash = {}
    ch.samples.each do |s|
      key = "#{s.time.to_i}:#{s.extractor_keys['PTool']}:#{s.extractor_keys['POperationID']}:#{s.extractor_keys['Lot']}:#{s.extractor_keys['Wafer']}"
      key = [s.time.to_i, s.extractor_keys['PTool'], s.extractor_keys['POperationID'], s.extractor_keys['Lot'], s.extractor_keys['Wafer']].join(':')
      (rem_hash[key] ||= []) << s
    end
    ret = true
    rem_hash.each_pair do |t,v|
      v.sort! {|a,b| a.data_keys['MTime'] <=> b.data_keys['MTime']}
      v.each_with_index {|s, i| ret &= verify_remeasured(s, i<v.length-1)}
    end
    return ret
  end

  # criteria for rework: PTool, POperation, MOperation, (Lot or Wafer)
  def verify_channel_reworked(ch, params={})
    rem_hash = {}
    ch.samples.each do |s|
      if params[:lot]
        lotwafer = ":#{s.extractor_keys['Lot']}"
      else
        lotwafer = ":#{s.extractor_keys['Wafer']}"
      end
      key = "#{s.extractor_keys['PTool']}:#{s.extractor_keys['POperationID']}:#{s.data_keys['MOperationID']}"+ lotwafer
      if rem_hash.has_key?(key)
        rem_hash[key] << s
      else
        rem_hash[key] = [s]
      end
    end

    ret = true
    rem_hash.each_pair do |t,v|
      v.sort! {|a,b| a.data_keys['MTime'] <=> b.data_keys['MTime']}
      stime = v[-1].time #sample time of newest sample should be reference
      ret = v.inject(ret) do |ret, s|
        ret &= verify_reworked(s, s.time<stime)
      end
    end
    return ret
  end

  def verify_remeasured(sample, remeasured)
    ret = true
    if remeasured
      ($log.warn "verify_remeasured #{sample.sid} #{remeasured}: missing flag"; ret = false) unless sample.iflag
      ret = verify_condition("verify_remeasured #{sample.sid} #{remeasured}: missing comment") {
        sample.icomment and sample.icomment.index('Remeasurement')
      }
    else
      ret = verify_condition("verify_remeasured #{sample.sid} #{remeasured}: flag must not be set") {
        not (sample.iflag and sample.icomment and sample.icomment.index('Remeasurement'))
      }
    end
    return ret
  end

  def verify_reworked(sample, reworked)
    ret = true
    if reworked
      ($log.warn "verify_reworked #{sample.sid} #{reworked}: missing flag"; ret = false) unless sample.iflag
      ret = verify_condition("verify_reworked #{sample.sid} #{reworked}: missing comment") {
        sample.icomment and sample.icomment.index('Rework')
      }
    else
      ret = verify_condition("verify_reworked #{sample.sid} #{reworked}: flag must not be set") {
        not (sample.iflag and sample.icomment.index('Rework'))
      }
    end
    return ret
  end

end
