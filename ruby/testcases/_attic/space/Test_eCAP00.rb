=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: U. Koerner, 2012-01-11
=end

require "Test_eCAPSpace_Setup"


class Test_eCAP00 < Test_eCAPSpace_Setup
  
  def test0000_setup
    $setup_ok = false
    #
    ["LotHold", "AutoLotHold", "ReleaseLotHold", "CancelLotHold"].each {|action|
      assert $spc.corrective_action(action), "CA #{action.inspect} not defined"
    }
    $setup_ok = true
  end

  def test0001_process_move_measure_move_lot
    use_lot_params = false
    
    lot_values = {@@lots["default"]=>"32.0", @@lots["oos"]=>"100.0", @@lots["mooc"]=>"70.0", @@lots["sooc"]=>"10.0"}
    
    process_pd = "2500.1000"
    meas_pd = "2500.6000"
    hold_pd = "2500.7100"
    
    @@lots.each_value {|l|
      assert_equal $sv.lot_cleanup(l)
      wafer_values = Hash.new
      li = $sv.lot_info(l, :wafers=>true)
      li.wafers.each {|w| wafer_values[w.wafer] = w.slot.to_i + 24}
      
      $sv.lot_opelocate(l, process_pd)
      submit_process_dcr(@@ppd_qa, :lot=>l, :wafer_values=>wafer_values, :department=>@@module_cfm, :nocharting=>true, 
        :technology=>"45NM", :opNo=>"2500.6000", :productgroup=>"3260C", :export=>"log/#{__method__}_#{l}_p.xml")
      wafer_values = Hash.new
      wafers = li.wafers.take(1)
      wafers.each {|w| wafer_values[w.wafer] = lot_values[l]}
      
      $sv.lot_opelocate(l, meas_pd)
      dcr = Space::DCR.new(:eqp=>@@eqp, :lot=>l, :op=>@@mpd_qa, :opNo=>meas_pd, :productgroup=>"3260C",
        :technology=>"45NM", :department=>@@module_cfm)
      if use_lot_params
        dcr.add_parameters(@@parameters, {l=>lot_values[l]}, :add_wafer=>false, :reading_type=>"Lot", :value_type=>'double')
        nsamples = @@parameters.size
      else
        dcr.add_parameters(@@parameters, wafer_values, :add_wafer=>true, :value_type=>'double')
        nsamples = @@parameters.size * wafer_values.size
      end
      res = $client.send_dcr(dcr, :export=>"log/#{__method__}_#{l}.xml")
      assert $spctest.verify_dcr_accepted(res, nsamples), "DCR not submitted successfully"
      
      $sv.claim_process_lot(l, happylot: true)
      #$sv.lot_opelocate(l, hold_pd)
    }
  end
  
end
