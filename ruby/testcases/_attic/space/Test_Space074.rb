=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: U. Koerner, 2012-01-11

History:
  2014-02-05 ssteidte,  separated Test_Space07, code cleanup
=end

require_relative 'Test_Space_Setup'


# Test Space/SIL. Merge with Test_Space075 ?
class Test_Space074 < Test_Space_Setup
  @@mpd_qa = 'QA-MB-FTDEPM-LIS-CAEX.01'

  
  def setup
    super
    return unless $lds && self.class.class_variable_defined?(:@@lot) # bypass test0000_setup
    assert_equal 0, $sv.eqp_cleanup(@@eqp)
    assert_equal 0, $sv.lot_cleanup(@@lot)
    assert_equal 0, $sv.inhibit_cancel(:route, $sv.lot_info(@@lot).route)
    #
    $lds.all_spc_channels('QA_PARAM_90*').each {|ch| assert ch.delete}
    folder = $lds.folder(@@autocreated_qa)
    assert folder.delete_channels, "error cleaning channels" if folder
  end


  def test0700_setup
    $setup_ok = false
    #
    assert @@ppd_qa = $sildb.pd_reference(mpd: @@mpd_qa).first, "no PD reference found for #{@@mpd_qa}"
    ['AutoUnknown', 'Unknown'].each {|action|
      assert $spc.corrective_action(action), "CA #{action.inspect} not defined"
    }
    #
    $setup_ok = true
  end

  def test0740_unknown_auto
    channels = create_clean_channels
    assert $spctest.assign_verify_cas(channels,
      ['AutoUnknown'])
    #
    # send process DCR
    dcrp = Space::DCR.new(eqp: @@eqp, eqptype: 'PROCESS-CHAMBER', lot: @@lot, op: @@ppd_qa)
    dcrp.add_parameters(@@parameters, @@wafer_values2, nocharting: true)
    assert $spctest.send_dcr_verify(dcrp)
    #
    # send DCR with OOC values
    dcr = Space::DCR.new(lot: @@lot, eqp: @@eqp, op: @@mpd_qa).add_parameters(@@parameters, @@wafer_values2ooc)
    assert $spctest.send_dcr_verify(dcr, nsamples: @@parameters.size*@@wafer_values2ooc.size, nooc:'all*2'), "DCR not accepted"
    if @@cafailure_lothold_fallback
      assert $spctest.verify_futurehold2(@@lot, '', timeout: @@ca_time_out), "lot must have a future hold as fallback"
      assert $spctest.verify_ecomment(channels, "LOT_HELD: #{@@lot}: reason={X-S")
    else
      @@parameters.each {|p|
        assert $spctest.verify_notification("Execution of [AutoUnknown] failed", nil, raw: true).count > 0, "no notification for #{p}"
      }
    end
  end

  def test0741_unknown_manual
    channels = create_clean_channels(actions: 'Unknown')
    assert $spctest.assign_verify_cas(channels,
      ['Unknown'])
    #
    # send process DCR
    dcrp = Space::DCR.new(eqp: @@eqp, eqptype: 'PROCESS-CHAMBER', lot: @@lot, op: @@ppd_qa)
    dcrp.add_parameters(@@parameters, @@wafer_values2, nocharting: true)
    assert $spctest.send_dcr_verify(dcrp)
    #
    # send DCR with OOC values
    dcr = Space::DCR.new(lot: @@lot, eqp: @@eqp, op: @@mpd_qa).add_parameters(@@parameters, @@wafer_values2ooc)
    assert $spctest.send_dcr_verify(dcr, nsamples: @@parameters.size*@@wafer_values2ooc.size, nooc: 'all*2'), "DCR not submitted successfully"
    assert $spctest.verify_futurehold_cancel(@@lot, "[(Raw above specification)", timeout: @@ca_time_out), "lot must have no future hold"
    #
    s = channels[0].samples(ckc: true)[-1]
    assert !s.assign_ca($spc.corrective_action('Unknown')), "assigning corective action should throw an error"
  end
end
