=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: Steffen Steidten, 2011-11-25

History:
  2016-12-07 sfrieske,  change mqinstance to instance in test00_setup
=end

require 'RubyTestCase'
require 'space'
require 'spacedcr'


# Test SIL Message Consumer EI
class Test_Space_ConsumerEI < RubyTestCase

  @@interval = 120
  @@msg_number = 500    # listening and active, each

  def setup
    super
    $setup_ok = ($setup_ok != false)
    assert $setup_ok, "test setup is not correct"
  end

  def test00_setup
    $client = Space::Client.new($env, instance: "spaceconsumer.#{$env}")
    @@interval = @@interval.to_i
  end

  def test01_2messages
    $setup_ok = false
    # clean up queue
    $client.mq_response.delete_msgs nil
    $dcr1 = Space::DCR.new
    $dcr1.add_parameter("Parameter1", {"W1"=>99}, :space_listening=>true)
    # msg spaceListeningOnly='true'
    $client.send_dcr($dcr1)
    # msg spaceListeningOnly='false'
    $dcr1.space_listening(false)
    msg_orig = $dcr1.to_xml
    $client.send_dcr(msg_orig)
    # wait for EI
    $log.info "waiting #{@@interval}s for the EI to run"
    sleep @@interval
    # compare
    assert_equal 1, $client.mq_response.qdepth, "no msg on the queue"
    msg = $client.mq_response.receive_msg
    assert msg == msg_orig, "msg has been altered"
    $setup_ok = true
  end

  def test02_many_messages_alterning
    $setup_ok = false
    # clean up queue
    $client.mq_response.delete_msgs nil
    $dcr1 = Space::DCR.new
    n = @@msg_number.to_i
    $log.info "sending #{n} messages for listening and active mode each"
    $msgs_orig = n.times.collect {|i|
      $dcr1.add_parameter("Parameter1", {"W1"=>i}, :space_listening=>true)
      $dcr1.space_listening(true)
      # msg spaceListeningOnly='true'
      $client.send_dcr($dcr1)
      # msg spaceListeningOnly='false'
      $dcr1.space_listening(false)
      msg_orig = $dcr1.to_xml
      $client.send_dcr(msg_orig)
      msg_orig
    }
    # wait for EI
    $log.info "waiting #{@@interval}s for the EI to run"
    sleep @@interval
    # compare
    assert_equal n, $client.mq_response.qdepth, "wrong number of messages on the queue"
    nok = 0
    $msgs_orig.each_with_index {|msg_orig, i|
      msg = $client.mq_response.receive_msg
      if msg == msg_orig
        nok += 1
      else
        $log.warn "msg #{i} has been altered"
      end
    }
    assert_equal $msgs_orig.size, nok, "messages have been altered on the queue"
    $setup_ok = true
  end

  def test03_many_messages_active_first
    # clean up queue
    $client.mq_response.delete_msgs nil
    $dcr1 = Space::DCR.new
    n = @@msg_number.to_i
    $log.info "sending #{n} messages for active mode"
    n.times {|i|
      $dcr1.add_parameter("Parameter1", {"W1"=>i}, :space_listening=>false)
      $client.send_dcr($dcr1)
    }
    $log.info "sending #{n} messages for listening mode"
    $dcr1 = Space::DCR.new
    n.times {|i|
      $dcr1.add_parameter("Parameter1", {"W1"=>i})
      $dcr1.space_listening(true)
      $client.send_dcr($dcr1)
    }
    # wait for EI
    $log.info "waiting #{@@interval}s for the EI to run"
    sleep @@interval
    # compare queue size only
    assert_equal n, $client.mq_response.qdepth, "wrong number of messages on the queue"
  end

end
