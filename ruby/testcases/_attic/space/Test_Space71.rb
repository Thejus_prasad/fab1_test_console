=begin 
Test Space/SIL.

(c) Copyright 2012 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.
    
Author: D.Steger, 2012-08-08
=end

require_relative 'Test_Space_Setup'
require 'xsitetest'

# Test Space/SIL.

class Test_Space71 < Test_Space_Setup
  @@wafer_values2 = {"SIL007.00.01"=>40, "SIL007.00.02"=>31, "SIL007.00.03"=>32, "SIL007.00.04"=>43, "SIL007.00.05"=>64, "SIL007.00.06"=>45}
  @@mpd_qa = "QA-MB-FTDEPM-LIS-CAEX.01"
	@@chamber_a = "PM1"
	@@chamber_b = "PM2"
	@@chamber_c = "CHX"
  @@chambers = {@@chamber_a=>5, @@chamber_b=>6, @@chamber_c=>7}
  @@mtool = "UTF001"
  

  def setup
    super
    return unless $lds && self.class.class_variable_defined?(:@@lot) # bypass test0000_setup
    assert_equal 0, $sv.eqp_cleanup(@@eqp)
    assert_equal 0, $sv.lot_cleanup(@@lot)
    #
    [$lds, $lds_setup].each {|lds|
      lds.all_spc_channels('QA_PARAM_90*').each {|ch| assert ch.delete}
      folder = lds.folder(@@autocreated_qa)
      assert folder.delete_channels, "error cleaning channels" if folder
    }
  end


  # send dcr with non-ooc values to setup channels
  def _create_chan(parameters, dpt="QA", params={})    
    #
    folder = $lds.folder(@@autocreated_qa)
    folder.delete_channels if folder
    #
    chambers = (params[:chambers] or @@chambers)
    mtool = (params[:mtool] or @@mtool)
    mpd = (params[:mpd] or @@mpd_qa)
    nsamples = (params[:nsamples] or (parameters.size*@@wafer_values2.size))

    #
    submit_process_dcr(@@ppd_qa, :wafer_values=>@@wafer_values2, :chambers=>chambers, :processing_areas=>"cycle_wafer", :export=>"log/#{__method__}_p.xml")
    
    dcr = Space::DCR.new(:lot=>@@lot, :eqp=>mtool, :department=>dpt, :op=>mpd, :productgroup=>"QAPG1")
    dcr.add_parameters(parameters, @@wafer_values2)
    res = $client.send_dcr(dcr, :export=>"log/#{__method__}_m.xml")
    assert $spctest.verify_dcr_accepted(res, nsamples), "DCR not submitted successfully"
    #
    folder = $lds.folder("AutoCreated_#{dpt}")  # need to get folder again
    parameters.each{|p|
      ch = folder.spc_channel(:parameter=>p)
      assert ch, "channel for parameter #{p} missing in folder AutoCreated_#{dpt}"
    }
  end

  def test7100_setup
    $setup_ok = false
    skip "not applicable in #{$env}" unless ['mtqa', 'f8stag'].member?($env)
    ##return unless check_env_f8
    
    ($xs.disconnect; $xstest = nil) if $xs and $xs.env != $xs
    $xstest ||= XsiteTest.new($env, :sv=>$sv)
    $xs = $xstest.xs
    #
    assert @@ppd_qa = $sildb.pd_reference(:mpd=>@@mpd_qa).first, "no PD reference found for #{@@mpd_qa}"

    ["AutoSetE10StateToolDown", "AutoSetE10StateChamberDown-Exclude","AutoSetE10StateChamberDown-Include",
      "SetE10StateToolDown", "SetE10StateChamberDown-Include"
    ].each {|action|
      assert $spc.corrective_action(action), "CA #{action.inspect} not defined"
    }
    #
    $setup_ok = true
  end
  
  def test7101_equipment_down
    _run_scenario(@@chambers, ["AutoSetE10StateToolDown"], true)
  end
  
  def test7102_chamber_down
    _run_scenario({"PM2"=>5, "PM3"=>6}, ["AutoSetE10StateChamberDown-Exclude"], false, true)
  end
  
  def test7103_chamber_down_excluded
    _run_scenario(@@chambers, ["AutoSetE10StateChamberDown-Exclude"], false, true, ["PM1", "CHX"])
  end
    
  def test7104_equipment_chamber_down
    _run_scenario({"PM2"=>5}, ["AutoSetE10StateToolDown", "AutoSetE10StateChamberDown-Exclude"], true, true)
  end

  def test7105_manual_equipment_chamber_down
    channels = _run_scenario({"PM2"=>5}, ["SetE10StateToolDown", "SetE10StateChamberDown-Include"], false, false)
    assert $spctest.verify_inhibit(@@eqp, 0, {:class=>"Equipment"}), "tool must not have more inhibits"
    assert $spctest.verify_inhibit(@@eqp, 0, {:class=>"Chamber"}), "chamber must not have more inhibits"

    # Tool down
    channels.each_with_index do |ch,i|
      sample = ch.samples(:ckc=>true)[-1]
      assert sample.assign_ca($spc.corrective_action("SetE10StateToolDown")), "error assigning corective action"
      sleep 10
      sample = ch.samples(:ckc=>true)[-1]
      assert sample.ecomment.index("E10_STATE_SET: #{@@eqp}: UDT"), "no comment for E10_STATE_SET found for #{sample.sid}"
    end
    
    assert $spctest.verify_inhibit(@@eqp, 0, {:class=>"Equipment"}), "tool must not have more inhibits"
    assert $xstest.verify_work_request(@@eqp, "SPC Violation UDT.DELAY"), "work request not found"

    # Chamber down
    channels.each_with_index do |ch,i|
      sample = ch.samples(:ckc=>true)[-1]
      assert sample.assign_ca($spc.corrective_action("SetE10StateChamberDown-Include")), "error assigning corective action"
      sleep 10
      sample = ch.samples(:ckc=>true)[-1]
      ["PM2", "PM3"].each do |cha|
        assert sample.ecomment.index("E10_STATE_SET: #{@@eqp}/#{cha}: UDT"), "no comment for E10_STATE_SET found for #{sample.sid}"
      end
    end
    # included chamber PM3
    ["PM2", "PM3"].each do |cha|      
      assert $xstest.verify_work_request(@@eqp, "SPC Violation UDT.DELAY", :chamber=>cha), "work request not found"
    end
    assert $spctest.verify_inhibit(@@eqp, 0, {:class=>"Chamber"}), "chamber must not have more inhibits"
  end

  def _run_scenario(chambers, actions, eqp_down, chamber_down=false, excluded=[], included=[])
    $xstest.cleanup_work_requests(@@eqp, :chambers=>true)
    #
    _create_chan @@parameters, "QA", :chambers=>chambers
    #
    folder = $lds.folder(@@autocreated_qa)
    channels = @@parameters.collect{|p| folder.spc_channel(:parameter=>p)}
    #
    # ensure corrective actions are set
    assert $spctest.assign_verify_cas(channels, actions)
    #
    # send process DCR
    submit_process_dcr(@@ppd_qa, :wafer_values=>@@wafer_values2, :chambers=>chambers)
    # send DCR with OOC values
    wafer_values = Hash[*@@wafer_values2.collect {|w,v| [w,v+100]}.flatten]
    dcr = Space::DCR.new(:lot=>@@lot, :eqp=>@@eqp, :op=>@@mpd_qa, :productgroup=>"QAPG1")
		dcr.add_parameters(@@parameters, wafer_values)
    res = $client.send_dcr(dcr, :export=>"log/#{__method__}.xml")
    $log.info "Result (Meas): #{res.inspect}"
    assert $spctest.verify_dcr_accepted(res, @@parameters.size*wafer_values.size, :nooc=>@@parameters.size*wafer_values.size*2), "DCR not submitted successfully"
    #
    $log.info "Check Xsite set"
    if eqp_down
      assert $xstest.verify_work_request(@@eqp, "SPC Violation UDT.DELAY"), "work request not found"
      assert $spctest.verify_ecomment(channels, "E10_STATE_SET: #{@@eqp}: UDT")
      assert $spctest.verify_inhibit(@@eqp, 0, {:class=>"Equipment"}), "tool must not have an inhibit"
    end
    if chamber_down
      chas = chambers.keys - excluded + included
      chas.each do |cha|
        assert $xstest.verify_work_request(@@eqp, "SPC Violation UDT.DELAY", :chamber=>cha), "work request not found"
        assert $spctest.verify_ecomment(channels, "E10_STATE_SET: #{@@eqp}/#{cha}: UDT")
      end
      excluded.each do |cha|
        assert !$xstest.verify_work_request(@@eqp, "SPC Violation UDT.DELAY", :chamber=>"PM1"), "work request not expected"
      end
      assert $spctest.verify_inhibit(@@eqp, 0, {:class=>"Chamber"}), "chamber must not have an inhibit"
    end
    assert $spctest.verify_futurehold_cancel(@@lot, nil), "No future hold"
    return channels
  end

  def test7106_chamber_inhibit_parameter_extension_lot
    $xstest.cleanup_work_requests(@@eqp, :chambers=>true)

    processing_areas_behaviour = "cycle_wafer"
    #processing_areas_behaviour = "cycle_parameter"
    parameters = @@parameters.map {|p| p + "_LOT"}
    chambers = {"CHA"=>5, "CHB"=>6, "PM1"=>7}
    expected_samples = parameters.size * chambers.size if processing_areas_behaviour == "cycle_wafer"
    
    _create_chan parameters, "QA", :chambers=>chambers, :nsamples=>expected_samples
    #
    folder = $lds.folder(@@autocreated_qa)
    channels = parameters.collect{|p| folder.spc_channel(:parameter=>p)}
    #
    # ensure corrective actions are set
    actions = ["AutoSetE10StateChamberDown-Exclude"]
    assert $spctest.assign_verify_cas(channels, actions)

    #
    $dcr = dcr = submit_process_dcr(@@ppd_qa, :wafer_values=>@@wafer_values2, :chambers=>chambers, :processing_areas=>processing_areas_behaviour)
    #

    # send DCR with OOC values
    wafer_values = Hash[*@@wafer_values2.collect {|w,v| [w,v+100]}.flatten]
    dcr = Space::DCR.new(:lot=>@@lot, :eqp=>@@eqp, :op=>@@mpd_qa, :productgroup=>"QAPG1")
		dcr.add_parameters(parameters, wafer_values)
    res = $client.send_dcr(dcr, :export=>"log/#{__method__}.xml")
    $log.info "Result (Meas): #{res.inspect}"
    assert $spctest.verify_dcr_accepted(res, expected_samples, :nooc=>expected_samples*2), "DCR not submitted successfully"
    #
    chas = chambers.keys - ["PM1"]
    chas.each do |cha|
      assert $xstest.verify_work_request(@@eqp, "SPC Violation UDT.DELAY", :chamber=>cha), "work request not found"
    end
    assert !$xstest.verify_work_request(@@eqp, "SPC Violation UDT.DELAY", :chamber=>"PM1"), "work request not expected"

    assert $spctest.verify_inhibit(@@eqp, 0, {:class=>"Chamber"}), "chamber must not have an inhibit"
  end

  def test7107_chamber_down_included
    _run_scenario({"PM1"=>5}, ["AutoSetE10StateChamberDown-Include"], false, true, [], ["PM2"])
  end

  def self.after_all_passed
  
  end
end