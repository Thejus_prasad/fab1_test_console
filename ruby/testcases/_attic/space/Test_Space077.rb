=begin
Test Space/SIL.

(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: D.Steger, 2012-04-19
=end

require_relative 'Test_Space_Setup'

# Test Space/SIL.

class Test_Space077 < Test_Space_Setup

  @@wafer_values2 = {"SIL007.00.01"=>20, "SIL007.00.02"=>14, "SIL007.00.03"=>22, "SIL007.00.04"=>23, "SIL007.00.05"=>14, "SIL007.00.06"=>15}
  #@@mpd_qa = "QA-MGT-NMETDEPDR.QA"
  @@mpd_qa = "QA-CLAIM-MEMO.01"
  @@chambers = {"PM1"=>5, "PM2"=>6, "CHX"=>7}
  @@mtool = "UTF001"
  @@product = "SILPROD.01"
  @@productgroup = "QAPG1"
  @@param_depts = {"QA_MEMO_900"=>"LIT", "QA_MEMO_901"=>"TFM", "QA_MEMO_902"=>"QA", "QA_MEMO_903"=>"ETC", "QA_MEMO_904"=>"QA"}

  # send dcr with non-ooc values to setup channels
  def _create_chan(parameters, dpt="QA", params={})
    #
    $lds.all_spc_channels("QA_MEMO_9*").each {|c| c.delete }

    #
    chambers = (params[:chambers] or @@chambers)
    mtool = (params[:mtool] or @@mtool)
    mpd = (params[:mpd] or @@mpd_qa)#
    #
    submit_process_dcr(@@ppd_qa, :lot=>@@lot, :wafer_values=>@@wafer_values2, :chambers=>chambers, :export=>"log/#{__method__}_p.xml")

    dcr = Space::DCR.new(:lot=>@@lot, :eqp=>mtool, :department=>dpt, :op=>mpd, :product=>@@product, :productgroup=>@@productgroup)
    dcr.add_parameters(parameters, @@wafer_values2)
    res = $client.send_dcr(dcr, :export=>"log/#{__method__}_m.xml")
    assert $spctest.verify_dcr_accepted(res, parameters.size*@@wafer_values2.size), "DCR not submitted successfully"
    #
    channels = []
    @@param_depts.each_pair do |p, dept|
      ch = $lds.folder("AutoCreated_#{dept}").spc_channel(:parameter=>p)
      assert ch, "no channel found for #{p} in #{dept}"
      channels << ch
    end

    return channels
  end

  def test0700_setup
    $setup_ok = false
    #
    @@ppd_qa = $sildb.pd_reference(:mpd=>@@mpd_qa).first
    refute_nil @@ppd_qa, "no PD reference found for #{@@mpd_qa}"

    actions = ["LotHold", "AutoLotHold", "ReleaseLotHold", "CancelLotHold"]
    assert $spctest.verify_corrective_actions_defined(actions)

    #assert $spctest.verify_speclimit(:specid=>"M14-00020003", :specversion=>7, :all_active=>true), "Speclimits do not exist"
    $setup_ok = true
  end

  def test0770_lot_hold_claim_memo
    parameters = ["QA_MEMO_900", "QA_MEMO_901", "QA_MEMO_902", "QA_MEMO_903", "QA_MEMO_904"]
    #
    channels = _create_chan( parameters, "QA" )
    #
    assert_equal 0, $sv.lot_cleanup(@@lot)
    #
    # ensure corrective actions are set
    actions = ["LotHold", "AutoLotHold", "ReleaseLotHold", "CancelLotHold"]
    channels.each{|ch|
      $spc.set_corrective_actions(ch, actions)
      vals = ch.valuations
      assert vals.size > 0, "no channel valuations defined, check channel and template"
      aa = $spc.corrective_actions(vals[0]).collect {|a| a.name}.sort
      assert_equal actions.sort, aa, "error setting corrective actions"
    }
    $log.info "#{actions.inspect} set correctly"
    #
    submit_process_dcr(@@ppd_qa, :wafer_values=>@@wafer_values2, :chambers=>@@chambers, :export=>"log/#{__method__}_p.xml")
    # send DCR with OOC values
    wafer_values = Hash[*@@wafer_values2.collect {|w,v| [w,v+100]}.flatten]
    dcr = Space::DCR.new(:lot=>@@lot, :eqp=>@@mtool, :op=>@@mpd_qa, :product=>@@product, :productgroup=>@@productgroup)
    dcr.add_parameters(parameters, wafer_values)
    res = $client.send_dcr(dcr, :export=>"log/#{__method__}.xml")
    $log.info "#{res.inspect}"
    assert $spctest.verify_dcr_accepted(res, parameters.count*wafer_values.size, :nooc=>parameters.count*wafer_values.size*2), "DCR not submitted successfully"
    params_speclimits = {}
    parameters.each {|p| params_speclimits[p] = [10.0,20.0,30.0]}
    assert $spctest.verify_dcr_speclimits(res, params_speclimits)
    #
    # verify futurehold from AutoLotHold
    assert $spctest.verify_futurehold2(@@lot, "[(Raw above specification)", :timeout=>@@ca_time_out, :dcr_lot=>dcr.find_lot_context(@@lot)), "lot must have a future hold"

    #
    claimtime = Time.now
    assert_equal 0, $sv.lot_gatepass(@@lot), "SiView GatePass error"

    assert $spctest.verify_futurehold_cancel(@@lot, nil, :timeout=>@@ca_time_out), "lot must have no future hold"
    assert $sv.lot_hold_list(@@lot, :type=>"FutureHold").size > 0, "lot must be on hold"

    sample_comments = {}
    #
    # ReleaseLotHold
    channels.each{|ch|
      s = ch.samples(:ckc=>true)[-1]
      sample_comments[s.sid] = "ReleaseHold #{s.sid}"
      #
      # CancelLotHold (from automatic action)
      assert s.assign_ca($spc.corrective_action("ReleaseLotHold"), :comment=>sample_comments[s.sid]), "error assigning corective action"
    }
    sleep 80
    assert $sv.lot_hold_list(@@lot, :type=>"FutureHold").size == 0, "lot must not be on hold"
    assert $spctest.verify_sapphire_messages(sample_comments)

    assert_equal 0, $sv.lot_opelocate(@@lot, "first")


    channels.each{|ch|
      s = ch.samples(:ckc=>true)[-1]
      #
      # CACollection-ReleaseAll (from automatic actions)
      assert s.assign_ca($spc.corrective_action("LotHold")), "error assigning corective action"

      assert $spctest.verify_futurehold2(@@lot, "", :timeout=>@@ca_time_out, :dcr_lot=>dcr.find_lot_context(@@lot), :parameter=>s.parameter), "lot must have a future hold"


      assert s.assign_ca($spc.corrective_action("CancelLotHold")), "error assigning corective action"

      assert $spctest.verify_futurehold_cancel(@@lot, "", :timeout=>@@ca_time_out), "lot must have no future hold"

    }



    fail "Check the lot hold claim memo in FabView. Time: #{claimtime}"
  end
end
