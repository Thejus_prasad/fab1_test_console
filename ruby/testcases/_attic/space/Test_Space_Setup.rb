=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: Steffen Steidten, 2011-11-25

History:
  2016-02-18 sfrieske,  minor cleanup
  2016-12-07 sfrieske,  require 'sfc' instead of 'setupfc', thus using rqrsp_http instead if the deprecated rqrsp
=end

require 'SiViewTestCase'
require 'spacetest'
require 'misc'


# Test Space/SIL.
class Test_Space_Setup < SiViewTestCase
  @@sil_version = nil  # set in testparameters/$env/space/Test_Space_Setup.par
  @@test_run = 0

  @@route = 'SIL-0001.01'
  @@product = 'SILPROD.01'
  @@sil_carriers = 'SIL%'

  @@mpd_lit = 'LIT-MGT-NMETDEPDR.LIT'
  @@mpd_qa = 'QA-MB-FTDEPM.01'
  @@ppd_qa = nil    # will be set in test0000_setup

  @@eqp = 'UTCSIL01'
  @@mtool = 'UTFSIL01'
  #According to the TDM schema, the following equipment types can be used in the DCR equipment tag attribute type
  #METROLOGY and RETICLE will not store the wafer history for processing areas and sub processing areas. For details please refer to MSR529229
  ##@@eqptypes = ['PROCESS', 'METROLOGY', 'PROCESS-CHAMBER', 'PROCESS-BATCH', 'PROCESS-SINGLEWAFER', 'PROCESS-IM', 'PROCESS-CHAMBER-IM',
  ##  'PROCESS-BATCH-IM', 'PROCESS-SINGLEWAFER-IM', 'RETICLE']
  @@parameters = ['QA_PARAM_900', 'QA_PARAM_901', 'QA_PARAM_902', 'QA_PARAM_903', 'QA_PARAM_904']
  @@wafer_values = {'2502J599KO'=>41, '2502J600KO'=>42, '2502J601KO'=>50, '2502J602KO'=>44, '2502J603KO'=>55, '2502J604KO'=>46}
  @@wafer_values2 = {'SIL008.00.01'=>40, 'SIL008.00.02'=>41, 'SIL008.00.03'=>42, 'SIL008.00.04'=>43, 'SIL008.00.05'=>44, 'SIL008.00.06'=>45}
  @@chambers = {'CHA'=>5, 'CHB'=>6, 'CHC'=>7}
  @@lds_inline = 'Inline_Fab1'
  @@lds_setup = 'Setup_Fab1'

  @@templates_folder = '_TEMPLATES'
  @@default_template = '_DEFAULT_TEMPLATE'
  @@default_template_qa = 'DEFAULT_QA_TEMPLATE'
  @@params_template = '_Template_QA_PARAMS_900'

  @@autocreated_qa = 'AutoCreated_QA'
  @@autocreated_nodep = 'AutoCreated_NODEP'
  @@autocreated_nonwip = 'AutoCreated_nonwip'
  @@module_qa = 'QA'

  @@spc_hold_reasons = %w(X-S1 X-S2 X-S3 X-S4 X-S5 X-S6 X-S7 X-S8 X-S9 X-SA)

  # These server properties need to be checked in the SIL properties

  # Autocharting configs
  @@parameters_missingspeclimit_upload_inline_enabled = true
  @@parameters_missingspeclimit_upload_setup_enabled = true
  # check for presence of spec limits as found in the SIL spec and targets database
  @@datacheck_missingspeclimits_inline_enabled = true
  @@datacheck_missingspeclimits_setup_enabled = true
  # check for presence of all parameters in DCR as defined in spec
  @@datacheck_parameter_inline_enabled = true
  @@datacheck_parameter_setup_enabled = true
  #
  @@cafailure_lothold_fallback = true    # F1 true, F8 false
  # AQV message enabled/disabled for LDS x
  @@autoqual_reporting_inline_enabled = false
  @@autoqual_reporting_setup_enabled = true

  @@ca_time_out = 240    # timeout for CA check
  @@derdack_sleep = 120  # timeout for derdack

  #setupfc settings
  @@sfcVersion        = '1.11.1'
  @@sfcDepartment     = 'FAT'
  @@sfcDefaultUser    = 'cstenzel'
  @@approvers         = ['qauser01', 'qauser02']
  @@speclimitlookup_route_required = true
  @@genKeysTables = nil
  @@monitoringSpecLimitsTable = nil
  @@specLimitsTable = nil
  @@specLimitsHierarchyTable = nil
  @@specLimitsSPCScenario0Table = nil
  @@specLimitsSPCScenario1Table = nil
  @@specLimitsMPCTable = nil
  @@specLimitsMRBTable = nil
  @@importantChambersTable = nil
  @@responsiblePDTable = nil

  @@aqv_inline_active = false
  @@aqv_setup_active = true


  def self.shutdown
    @@testlots.each {|lot| $sv.delete_lot_family(lot)}
  end


  def test0000_setup
    $setup_ok = false
    #
    @@testlots = []    # lots to be deleted after the tests
    #
    if $env == 'f8stag'
      @@sfcDefaultUser = 'dsteger'
      @@spc_hold_reasons.delete('X-SA')
    end
    #
    $svtest.route = @@route
    $svtest.product = @@product
    $svtest.carriers = @@sil_carriers
    #
    # keep connection open
    #$spc.close if $spc
    unless $spctest && $spctest.env == $env
      $spctest = SpaceTest.new($env, sv: $sv, ca_timeout: @@ca_time_out, notification_timeout: @@derdack_sleep)
    end
    $spc = $spctest.spc
    $sildb = $spctest.sildb
    $client = $spctest.client
    assert $lds = $spc.lds(@@lds_inline), "LDS #{@@lds_inline} not found"
    assert $lds_setup = $spc.lds(@@lds_setup), "LDS #{@@lds_setup} not found"
    #
    # calculate the default OOC values after classes and override parameters have been loaded
    @@wafer_valuesooc = Hash[@@wafer_values.collect {|w, v| [w, v + 100]}]
    @@wafer_values2ooc = Hash[@@wafer_values2.collect {|w, v| [w, v + 100]}]
    #
    # find or create a test lot
    lotcount = '1'
    @@lot = 'SIL' + @@sil_version.split('.').collect {|e| '%02d' % e}.join('') + '%02d' % @@test_run + '%02d' % lotcount + '.000'
    assert @@lot = $svtest.new_lot(lot: @@lot) unless $sv.lot_exists?(@@lot)
    $log.info "testing SIL #{@@sil_version}"
    $log.info "using lot #{@@lot.inspect}"
    $log.info '  create new lots by changing test_run in Test_Space_Setup.par'
    #
    $log.info "requesting ppd for mpd: #{@@mpd_qa}"
    assert @@ppd_qa = $sildb.pd_reference(mpd: @@mpd_qa).first, "no PD reference found for #{@@mpd_qa}"
    $log.info "using QA process PD #{@@ppd_qa}"
    #
    if $spctest.derdack.db.connected?
      $log.info 'Derdack notification will be tested'
    else
      $log.warn 'Derdack notification is not tested because of missing DB connection'
    end
    #
    $setup_ok = true
  end

  def test0001_setup
    ##assert $lds = $spc.lds(@@lds_inline), "LDS #{@@lds_inline} not found"
    ##assert $lds_setup = $spc.lds(@@lds_setup), "LDS #{@@lds_setup} not found"
    [$lds.folder(@@autocreated_nodep), $lds_setup.folder(@@autocreated_nonwip)].compact.each {|folder| folder.delete_channels(silent: true)} if $env != 'let'
    [$lds, $lds_setup].each {|lds| lds.folders('*_QA*').each {|folder| folder.delete_channels(silent: true)}}
  end


  # create a process DCR, returns the DCR
  def create_process_dcr(params={})
    ppd = params[:ppd] || @@ppd_qa
    wafer_values = params[:wafer_values] || @@wafer_values # Hash[@@wafer_values.collect {|w, v| [w, v + 12]}]
    parameters = params[:parameters] || @@parameters
    chambers =  params[:chambers] || @@chambers
    lot = params[:lot] || @@lot
    eqp = params[:eqp] || @@eqp
    nocharting = params[:nocharting] != false
    seclot_productgroup = params[:seclot_productgroup] || 'QAPG1'
    #
    $log.info "create_process_dcr ppd: #{ppd.inspect}, lot: #{lot.inspect}, department: #{(params[:department] || 'QA').inspect}"
    dcr = Space::DCR.new({eqp: eqp, lot: lot, op: ppd, eqptype: 'PROCESS-CHAMBER', chambers: chambers}.merge(params))
    dcr.add_parameters(parameters, wafer_values, {nocharting: nocharting}.merge(params))
    if params[:addlots] && params[:addwafer_values]
      params[:addlots].each_with_index {|l, i|
        dcr.add_lot(l, {eqp: eqp, op: ppd, productgroup: seclot_productgroup}.merge(params))
        wafer_values_add = Hash[params[:addwafer_values][i].collect {|w, v| [w, v+12]}]
        dcr.add_parameters(parameters, wafer_values_add, {nocharting: nocharting}.merge(params))
      }
    end
    return dcr
  end

  # submit and check a process dcr (standard parameters in nocharting only if not specified in params)
  def submit_process_dcr(ppd, params={})
    dcrp = create_process_dcr({ppd: ppd}.merge(params))
    res = $client.send_dcr(dcrp, silent: true, export: params[:export] || "log/#{caller_locations(1,1)[0].label}_p.xml")
    assert $spctest.verify_dcr_accepted(res, params[:nsamples] || 0), 'DCR not submitted successfully'
    return dcrp
  end

  # delete channels and send a dcr with non-ooc values to setup clean channels fo CA assignment
  #
  # returns the channels created
  def create_clean_channels(params={})
    dpt = params[:department] || 'QA'
    parameters = params[:parameters] || @@parameters
    wafer_values = params[:wafer_values] || @@wafer_values2
    chambers = params[:chambers] || @@chambers
    mtool = params[:mtool] || @@mtool
    ptool = params[:eqp] || @@eqp
    mpd = params[:mpd] || @@mpd_qa
    ppd = params[:ppd] || @@ppd_qa
    nsamples = params[:nsamples] || (parameters.size * wafer_values.size)
    technology = params[:technology] || '32NM'
    lds = params.has_key?(:lds) ? params[:lds] : (technology == 'TEST') ? $lds_setup : $lds
    $log.info "create_clean_channels technology: #{technology.inspect}, parameters: #{parameters}"
    #
    unless params[:delete] == false
      [$lds, $lds_setup].each {|_lds|
        parameters.collect {|p| _lds.all_spc_channels("#{p}*")}.flatten.each {|ch| ch.delete(silent: true)}
      }
    end
    if dpt == 'LIT'
      # no process DCR
      mpd = @@mpd_lit
    else
      dcrp = Space::DCR.new(eqp: ptool, eqptype: 'PROCESS-CHAMBER', chambers: chambers, lot: @@lot, op: ppd, technology: technology)
      dcrp.add_parameters(parameters, wafer_values, processing_areas: params[:processing_areas], nocharting: true)
      assert $spctest.send_dcr_verify(dcrp, exporttag: 'create_clean_channels_p')
    end
    dcr = Space::DCR.new(eqp: mtool, chambers: chambers, lot: @@lot, op: mpd, technology: technology, department: dpt)
    dcr.add_parameters(parameters, wafer_values)
    assert $spctest.send_dcr_verify(dcr, nsamples: nsamples, exporttag: 'create_clean_channels_m'), 'error sending DCR'
    # department channels
    channels = if params[:dept_hash]
      params[:dept_hash].each_pair.collect {|p, dpt|
        assert ch = lds.folder("AutoCreated_#{dpt}").spc_channel(parameter: p), "no channel found for #{p} in AutoCreated_#{dpt}"
        ch
      }
    else
      folder = lds.folder("AutoCreated_#{dpt}")
      parameters.collect {|p|
        assert ch = folder.spc_channel(parameter: p), "channel for parameter #{p} missing in folder AutoCreated_#{dpt}"
        ch
      }
    end
    assert $spctest.assign_verify_cas(channels, params[:actions]) if params[:actions] ## NONO && !params[:valuations].nil?
    assert_equal 0, $sv.eqp_cleanup(@@eqp)
    assert_equal 0, $sv.lot_cleanup(@@lot)
    return channels
  end

end
