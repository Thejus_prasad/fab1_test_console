=begin
Test Space/SIL.

(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: D. Steger, 2013-02-20
=end

require_relative 'Test_Space_Setup'

# Test Space/SIL.
# CA for Rework operation.
class Test_Space74 < Test_Space_Setup
  
  @@rework_op1 = "2500.7100"
  @@rework_op2 = "2500.7200"
  @@rework_op3 = "2500.7300"
  @@rework_op4 = "2500.7400"
  @@release_non_sil_holds = false
  @@other_hold = "AEHL"
  @@main_route = "SIL-0001.01"
  
  def test07000_setup
    $setup_ok = false
    
    if $env == "f8stag"
      @@rework_op1 = "1005.2100"
      @@rework_op2 = "1005.2200"
      @@rework_op3 = "1005.2300"
      @@rework_op4 = "1005.2400"
      @@main_route = "FF-OBANPK.01"
    end
    
    ["AutoLotHold", "ReleaseLotHold", "FullLotRework", "FullLotRework-Error"].each {|action|
      assert $spc.corrective_action(action), "CA #{action.inspect} not defined"
    }
    #
    $setup_ok = true
  end


  def test7401_rework
    
    channels = _run_rework_scenario("QA-MB-REWORK1.01", @@rework_op1)
    
    sample_comments = {}
    #
    # FullLotRework
    s = channels[-1].samples(:ckc=>true)[-1]
    sample_comments[s.sid] = "FullLotRework #{s.sid}"    
    assert s.assign_ca($spc.corrective_action("FullLotRework"), :comment=>sample_comments[s.sid]), "error assigning corective action"

    verify_rework(channels[-1], sample_comments)    
  end
  
  def test7402_rework_2_routes
    channels = _run_rework_scenario("QA-MB-REWORK2.01", @@rework_op2)

    sample_comments = {}
    #
    # FullLotRework
    s = channels[-1].samples(:ckc=>true)[-1]
    sample_comments[s.sid] = "FullLotRework #{s.sid}"    
    assert s.assign_ca($spc.corrective_action("FullLotRework"), :comment=>sample_comments[s.sid]), "error assigning corective action"

    sleep 30
    s = channels[-1].samples(:ckc=>true)[-1]
    assert $spctest.verify_ecomment_sample(s, "Execution of [FullLotRework] failed: Found multiple connected rework routes"), "Wrong comment"    
    assert $spctest.verify_sapphire_messages(sample_comments)    
  end
  
  def test7403_rework_no_routes
    channels = _run_rework_scenario("QA-MB-REWORK3.01", @@rework_op3)

    sample_comments = {}
    #
    # FullLotRework
    s = channels[-1].samples(:ckc=>true)[-1]
    sample_comments[s.sid] = "FullLotRework #{s.sid}"    
    assert s.assign_ca($spc.corrective_action("FullLotRework"), :comment=>sample_comments[s.sid]), "error assigning corective action"

    sleep 30
    s = channels[-1].samples(:ckc=>true)[-1]
    assert $spctest.verify_ecomment_sample(s, "Execution of [FullLotRework] failed: Could not find connected rework route"), "Wrong comment"    
    assert $spctest.verify_sapphire_messages(sample_comments)    
  end
  
  def test7404_rework_ca_error
    channels = _run_rework_scenario("QA-MB-REWORK1.01", @@rework_op1)

    sample_comments = {}
    #
    # FullLotRework
    s = channels[0].samples(:ckc=>true)[-1]
    sample_comments[s.sid] = "FullLotRework #{s.sid}"    
    assert s.assign_ca($spc.corrective_action("FullLotRework-Error"), :comment=>sample_comments[s.sid]), "error assigning corective action"

    sleep 30
    s = channels[0].samples(:ckc=>true)[-1]
    assert $spctest.verify_ecomment_sample(s, "Execution of [FullLotRework] failed: Missing CorrectiveAction attributes"), "Wrong comment"    
    assert $spctest.verify_sapphire_messages(sample_comments)    
  end
  
  def test7405_rework_wrong_ope
    channels = _run_rework_scenario("QA-MB-REWORK1.01", @@rework_op2)

    sample_comments = {}
    #
    # FullLotRework
    s = channels[0].samples(:ckc=>true)[-1]
    sample_comments[s.sid] = "FullLotRework #{s.sid}"    
    assert s.assign_ca($spc.corrective_action("FullLotRework"), :comment=>sample_comments[s.sid]), "error assigning corective action"

    sleep 30
    s = channels[0].samples(:ckc=>true)[-1]
    assert $spctest.verify_ecomment_sample(s, "Execution of [FullLotRework] failed: Previous operation of the lot #{@@lot} is not the expected metrology operation"), "Wrong comment"    
    assert $spctest.verify_sapphire_messages(sample_comments)    
  end
  
  def test7406_rework_no_hold
    channels = _run_rework_scenario("QA-MB-REWORK1.01", @@rework_op1)
    $sv.lot_hold_release @@lot, nil
    
    sample_comments = {}
    #
    # FullLotRework
    s = channels[0].samples(:ckc=>true)[-1]
    sample_comments[s.sid] = "FullLotRework #{s.sid}"    
    assert s.assign_ca($spc.corrective_action("FullLotRework"), :comment=>sample_comments[s.sid]), "error assigning corective action"

    sleep 30
    s = channels[0].samples(:ckc=>true)[-1]
    assert $spctest.verify_ecomment_sample(s, "Execution of [FullLotRework] failed: Lot #{@@lot} is not on Hold"), "Wrong comment"    
    assert $spctest.verify_sapphire_messages(sample_comments)    
  end
  
  def test7407_rework_other_hold
    channels = _run_rework_scenario("QA-MB-REWORK1.01", @@rework_op1)
    assert $sv.lot_hold(@@lot, @@other_hold), "Cannot set hold"
    
    sample_comments = {}
    #
    # FullLotRework
    s = channels[0].samples(:ckc=>true)[-1]
    sample_comments[s.sid] = "FullLotRework #{s.sid}"    
    assert s.assign_ca($spc.corrective_action("FullLotRework"), :comment=>sample_comments[s.sid]), "error assigning corective action"

    sleep 30
    s = channels[0].samples(:ckc=>true)[-1]
    if @@release_non_sil_holds
      verify_rework(channels[0], sample_comments)   
    else
      assert $spctest.verify_ecomment_sample(s, "Execution of [FullLotRework] failed: Lot is on hold with non-SIL reason codes"), "Wrong comment"    
      assert $spctest.verify_sapphire_messages(sample_comments)    
    end
  end
  
  def test7408_rework_siview_error
    channels = _run_rework_scenario("QA-MB-REWORK4.01", @@rework_op4)

    sample_comments = {}
    #
    # FullLotRework
    s = channels[0].samples(:ckc=>true)[-1]
    sample_comments[s.sid] = "FullLotRework #{s.sid}"    
    assert s.assign_ca($spc.corrective_action("FullLotRework"), :comment=>sample_comments[s.sid]), "error assigning corective action"

    sleep 30
    s = channels[0].samples(:ckc=>true)[-1]
    assert $spctest.verify_ecomment_sample(s, "Execution of [FullLotRework] failed: TxReworkWithHoldReleaseReq failed"), "Wrong comment"    
    assert $spctest.verify_sapphire_messages(sample_comments)    
  end
  
  def verify_rework(channel, sample_comments)
    assert wait_for(:timeout=>60) { 
      linfo = $sv.lot_info(@@lot)
      linfo.states["Lot Production State"] == "INREWORK" && linfo.status == "Waiting" 
    }, "Lot is not in rework"
    s = channel.samples(:ckc=>true)[-1]
    assert $spctest.verify_ecomment_sample(s, "FullLotRework[lotId = '#{@@lot}']"), "Wrong comment"    
    assert $spctest.verify_sapphire_messages(sample_comments)
  end
  
  def _run_rework_scenario(mpd, openo)
    folder = $lds.folder(@@autocreated_qa)
    assert folder.delete_channels, "error cleaning channels"
    assert ppd = $sildb.pd_reference(:mpd=>mpd).first, "no PD reference found for #{mpd}"
    ##channels = create_ca_channels(mpd, ppd)
    channels = create_clean_channels(:mpd=>mpd, :ppd=>ppd)
    #
    assert_equal 0, $sv.lot_cleanup(@@lot, op: :current)
    assert_equal 0, $sv.eqp_cleanup(@@eqp)
    $sv.lot_rework_cancel(@@lot)
    
    assert_equal 0, $sv.lot_opelocate(@@lot, openo)
    
    #
    # ensure corrective actions are set
    actions = ["AutoLotHold", "ReleaseLotHold", "FullLotRework"]
    assert $spctest.assign_verify_cas(channels, actions)

    #
    submit_process_dcr(@ppd, :wafer_values=>@@wafer_values2, :chambers=>@@chambers, :export=>"log/#{__method__}_p.xml")
    # send DCR with OOC values
    wafer_values = Hash[*@@wafer_values2.collect {|w,v| [w,v+100]}.flatten]
    dcr = Space::DCR.new(:lot=>@@lot, :eqp=>@@mtool, :op=>mpd, :productgroup=>"QAPG1", :route=>@@main_route)
    dcr.add_parameters(@@parameters, wafer_values)
    res = $client.send_dcr(dcr, :export=>"log/#{__method__}.xml")
    assert $spctest.verify_dcr_accepted(res, @@parameters.count*wafer_values.size, :nooc=>@@parameters.count*wafer_values.size*2), "DCR not submitted successfully"
    #
    # verify futurehold from AutoLotHold
    assert $spctest.verify_futurehold2(@@lot, "[(Raw above specification)", :timeout=>@@ca_time_out, :dcr_lot=>dcr.find_lot_context(@@lot)), "lot must have a future hold"
    #
    # verify comment is set
    assert $spctest.verify_ecomment(channels, "LOT_HELD: #{@@lot}: reason={X-S")
    
    assert_equal 0, $sv.lot_gatepass(@@lot), "SiView GatePAss error"
    
    assert $spctest.verify_futurehold_cancel(@@lot, nil, :timeout=>@@ca_time_out), "lot must have no future hold"
    assert $sv.lot_hold_list(@@lot, :type=>"FutureHold").size > 0, "lot must be on hold"
    
    return channels
  end
    
end