=begin 
Test Space/SIL.

(c) Copyright 2012 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.
    
Author: D.Steger, 2012-08-08
=end

require_relative 'Test_Space_Setup'

# Test Space/SIL.

class Test_Space72 < Test_Space_Setup
  ##@@wafer_values2 = {'SIL007.00.01'=>40, 'SIL007.00.02'=>31, 'SIL007.00.03'=>32, 'SIL007.00.04'=>43, 'SIL007.00.05'=>64, 'SIL007.00.06'=>45}
  #@@mpd_qa = 'QA-MGT-NMETDEPDR.QA'
  @@mpd_qa = 'QA-MB-FTDEPM-LIS-CAEX.01'
	@@chamber_a = 'PM1'
	@@chamber_b = 'PM2'
	@@chamber_c = 'CHX'
  @@chambers = {@@chamber_a=>5, @@chamber_b=>6, @@chamber_c=>7}
  
  def setup
    super
    return unless $lds && self.class.class_variable_defined?(:@@lot) # bypass test0000_setup
    assert_equal 0, $sv.eqp_cleanup(@@eqp)
    assert_equal 0, $sv.lot_cleanup(@@lot)
    #
    [$lds, $lds_setup].each {|lds|
      lds.all_spc_channels('QA_PARAM_90*').each {|ch| assert ch.delete}
      folder = lds.folder(@@autocreated_qa)
      assert folder.delete_channels, 'error cleaning channels' if folder
    }
  end


  def test7200_setup
    $setup_ok = false
    #
    assert @@ppd_qa = $sildb.pd_reference(:mpd=>@@mpd_qa).first, "no PD reference found for #{@@mpd_qa}"

    ['AutoChamberAndRecipeInhibit-Single', 'AutoChamberAndRecipeInhibit-Multi', 'AutoChamberAndRecipeInhibit-Wildcard',
      'AutoChamberAndRecipeInhibit-Precipe',
      'AutoChamberAndRecipeInhibit-Error', 'AutoChamberAndRecipeInhibit-ErrorMulti', 'AutoChamberAndRecipeInhibit-ErrorWild',
      'ReleaseChamberAndRecipeInhibit'
    ].each {|action|
      assert $spc.corrective_action(action), "CA #{action.inspect} not defined"
    }
    $setup_ok = true
  end
  
  def test7201_chamber_recipe_inhibit_single
    _scenario_equipment_recipes(__method__, 'AutoChamberAndRecipeInhibit-Single', {'PM1'=>5, 'PM2'=>6}, ['P-UTC001_0.UTMR000.01'], ['PM1'], ['PM3'])
  end

  def test7202_chamber_recipe_inhibit_multi
    _scenario_equipment_recipes(__method__, 'AutoChamberAndRecipeInhibit-Multi', {'PM2'=>5, 'PM3'=>6}, ['P-UTC001_0.UTMR000.01', 'P-UTC001_1.UTMR000.01'])
  end

  def test7203_chamber_recipe_inhibit_wildcard
    _scenario_equipment_recipes(__method__, 'AutoChamberAndRecipeInhibit-Wildcard', {'PM2'=>5, 'PM3'=>6}, ['P-UTC001_0.UTMR000.01',
        'P-UTC001_1.UTMR000.01', 'P-UTC001_2.UTMR000.01', 'P-UTC001_3.UTMR000.01', 'P-UTC001_4.UTMR000.01'])
  end

  def test7204_chamber_recipe_inhibit_precipe
    _scenario_equipment_recipes(__method__, 'AutoChamberAndRecipeInhibit-Precipe', {'PM2'=>5, 'PM3'=>6}, [])
  end


  def _scenario_equipment_recipes(method, ca, chambers, mrecipes, excluded=[], included=[])
    folder = $lds.folder(@@autocreated_qa)
    assert folder.delete_channels, 'error cleaning channels'
    ## channels = create_ca_channels(@@mpd_qa, @@ppd_qa, :chambers=>chambers, :processing_areas=>'cycle_wafer')
    channels = create_clean_channels(:chambers=>chambers, :processing_areas=>'cycle_wafer')
    #
    precipe = 'P.UTMR000.01'
    no_precipe_inhibit = true
    if mrecipes == []
      mrecipes = [precipe]
      no_precipe_inhibit = false
    end
    assert_equal 0, $sv.inhibit_cancel(:recipe, precipe)
    mrecipes.each {|mrecipe| assert_equal 0, $sv.inhibit_cancel(:recipe, mrecipe)}
    #
    # ensure corrective actions are set
    actions = [ca, 'ReleaseChamberAndRecipeInhibit']
    assert $spctest.assign_verify_cas(channels, actions)
    #
    submit_process_dcr(@@ppd_qa, :wafer_values=>@@wafer_values2, :chambers=>chambers, :machine_recipe=>precipe)
    #
    # send DCR with OOC values
    wafer_values = Hash[*@@wafer_values2.collect {|w,v| [w,v+100]}.flatten]
    dcr = Space::DCR.new(:lot=>@@lot, :eqp=>@@mtool, :op=>@@mpd_qa, :productgroup=>'QAPG1')
    dcr.add_parameters(@@parameters, wafer_values)
    res = $client.send_dcr(dcr, :export=>"log/#{method}.xml")
    assert $spctest.verify_dcr_accepted(res, @@parameters.size*wafer_values.size, :nooc=>@@parameters.size*wafer_values.size*2), 'DCR not submitted successfully'
    #
    # verify comment and inhibit is set
    filtered_chas = chambers.keys - excluded + included
    filtered_chas.each do |cha|
      mrecipes.each do |mrecipe|
        assert $spctest.verify_inhibit([@@eqp, mrecipe], filtered_chas.count*@@parameters.count, {:class=>:eqp_chamber_recipe, :timeout=>@@ca_time_out}), 'chamber+recipe must have an inhibit'
        assert $spctest.verify_ecomment(channels, "TOOL+CHAMBER+RECIPE_HELD: #{@@eqp}/#{cha}/#{mrecipe}: reason={X-S")
      end
    end

    $log.info 'Chamber and Recipe Inhibit automatically set'
    if no_precipe_inhibit
      assert $spctest.verify_inhibit([@@eqp, precipe], 0, {:class=>:eqp_chamber_recipe, :timeout=>@@ca_time_out}), 'chamber+precipe must not have an inhibit'
    end

    sample_comments={}
    
    channels.each do |ch|
      #
      # release inhibit (from automatic action)
      s = ch.samples(:ckc=>true)[-1]
      sample_comments[s.sid] = "FullLotRework #{s.sid}"
      assert s.assign_ca($spc.corrective_action('ReleaseChamberAndRecipeInhibit'), :comment=>sample_comments[s.sid]), 'error assigning corective action'
      sleep 5#to avoid SiView error
    end
    mrecipes.each do |mrecipe|
      assert $spctest.verify_inhibit([@@eqp, mrecipe], 0, {:class=>:eqp_chamber_recipe, :timeout=>@@ca_time_out}), 'chamber+recipe must not have an inhibit'
    end

    assert $spctest.verify_sapphire_messages(sample_comments)
  end

  def test7210_chamber_recipe_inhibit_recipe_error_single
    _scenario_equipment_recipes_not_found('AutoChamberAndRecipeInhibit-Error', 'Execution of [AutoChamberAndRecipeInhibit-Error] failed', {'PM2'=>5, 'PM3'=>6}, [])
  end

  def test7211_chamber_recipe_inhibit_recipe_error_multi
    _scenario_equipment_recipes_not_found('AutoChamberAndRecipeInhibit-ErrorMulti', 'Execution of [AutoChamberAndRecipeInhibit-ErrorMulti] failed', {'PM2'=>5, 'PM3'=>6}, ['P-UTC001_0.UTMR000.01'])
  end

  def test7212_chamber_recipe_inhibit_recipe_error_wildcard
    _scenario_equipment_recipes_not_found('AutoChamberAndRecipeInhibit-ErrorWild', 'Execution of [AutoChamberAndRecipeInhibitAction] error', {'PM2'=>5, 'PM3'=>6}, [])
  end


  def _scenario_equipment_recipes_not_found(ca, excomment, chambers, mrecipes)
    folder = $lds.folder(@@autocreated_qa)
    assert folder.delete_channels, 'error cleaning channels'
    #
##    channels = create_ca_channels(@@mpd_qa, @@ppd_qa, :chambers=>chambers, :processing_areas=>'cycle_wafer')
    channels = create_clean_channels(:chambers=>chambers, :processing_areas=>'cycle_wafer')
    
    #
    precipe = 'P.UTMR000.01'
    assert_equal 0, $sv.inhibit_cancel(:recipe, precipe)
    mrecipes.each {|mrecipe| assert_equal 0, $sv.inhibit_cancel(:recipe, mrecipe)}
    #
    # ensure corrective actions are set
    actions = [ca, 'ReleaseChamberAndRecipeInhibit', 'ReleaseChamberInhibit']
    assign_verify_cas(channels, actions)
    #
    submit_process_dcr(@@ppd_qa, :wafer_values=>@@wafer_values2, :chambers=>chambers, :machine_recipe=>precipe, :export=>"log/#{__method__}_p.xml")
    #
    # send DCR with OOC values
    wafer_values = Hash[*@@wafer_values2.collect {|w,v| [w,v+100]}.flatten]
    dcr = Space::DCR.new(:lot=>@@lot, :eqp=>@@mtool, :op=>@@mpd_qa, :productgroup=>'QAPG1')
    dcr.add_parameters(@@parameters, wafer_values)
    res = $client.send_dcr(dcr, :export=>"log/#{__method__}.xml")
    assert $spctest.verify_dcr_accepted(res, @@parameters.size*wafer_values.size, :nooc=>@@parameters.size*wafer_values.size*2), 'DCR not submitted successfully'
    #
    # verify comment and inhibit is set
    assert $spctest.verify_ecomment(channels, excomment, true), 'no comment indicating the error'
    chambers.keys.each do |cha|
      mrecipes.each do |mrecipe|
        assert $spctest.verify_inhibit([@@eqp, mrecipe], chambers.keys.count*@@parameters.count, {:class=>:eqp_chamber_recipe, :timeout=>@@ca_time_out}), 'chamber+recipe must have an inhibit'
        assert $spctest.verify_ecomment(channels, "TOOL+CHAMBER+RECIPE_HELD: #{@@eqp}/#{cha}/#{mrecipe}: reason={X-S")
      end
      assert $spctest.verify_ecomment(channels, "CHAMBER_HELD: #{@@eqp}/#{cha}: reason={X-S", false)
    end
    $log.info 'Chamber Inhibit automatically set'
    assert $spctest.verify_inhibit(@@eqp, chambers.keys.count, {:class=>['Chamber']}), 'chamber must have an inhibit'
    assert $spctest.verify_inhibit([@@eqp, precipe], 0, {:class=>:eqp_chamber_recipe}), 'chamber+precipe must not have an inhibit'
    assert $spctest.verify_futurehold_cancel(@@lot, nil), 'lot must have no future hold'


    unless (mrecipes == [])
      channels.each do |ch|
        #
        # release inhibit (from automatic action)
        s = ch.samples(:ckc=>true)[-1]
        assert s.assign_ca($spc.corrective_action('ReleaseChamberAndRecipeInhibit')), 'error assigning corective action'
        sleep 5 # to avoid SiView errors
      end
    end

    # TODO: Include release chamber is only
    # Right now it is only possible to release when finding the correct sample

    mrecipes.each do |mrecipe|
      assert $spctest.verify_inhibit([@@eqp, mrecipe], 0, {:class=>:eqp_chamber_recipe, :timeout=>@@ca_time_out}), 'chamber+recipe must not have an inhibit'
    end
    $log.info 'Chamber and Recipe Inhibit released'

    assert $spctest.verify_notification("Execution of [#{ca}] failed", 'NOT-FOUND',:msgcount=>5), 'wrong notification see MSR549577'
  end


  def test7220_chamber_recipe_inhibit_chamber_not_found
    folder = $lds.folder(@@autocreated_qa)
    assert folder.delete_channels, 'error cleaning channels'
    chambers = {'CHX'=>5, 'PM3'=>6}
    mrecipes = ['P-UTC001_0.UTMR000.01']
    #
##    channels = create_ca_channels(@@mpd_qa, @@ppd_qa, :chambers=>chambers, :processing_areas=>'cycle_wafer')
    channels = create_clean_channels(:chambers=>chambers, :processing_areas=>'cycle_wafer')
    #
    precipe = 'P.UTMR000.01'

    assert_equal 0, $sv.inhibit_cancel(:recipe, precipe)
    mrecipes.each {|mrecipe| assert_equal 0, $sv.inhibit_cancel(:recipe, mrecipe)}
    #
    # ensure corrective actions are set
    actions = ['AutoChamberAndRecipeInhibit-Single', 'ReleaseEquipmentAndRecipeInhibit']
    assert $spctest.assign_verify_cas(channels, actions)
    #
    submit_process_dcr(@@ppd_qa, :wafer_values=>@@wafer_values2, :chambers=>chambers, :machine_recipe=>precipe)
    #
    # send DCR with OOC values
    wafer_values = Hash[*@@wafer_values2.collect {|w,v| [w,v+100]}.flatten]
    dcr = Space::DCR.new(:lot=>@@lot, :eqp=>@@mtool, :op=>@@mpd_qa, :productgroup=>'QAPG1')
    dcr.add_parameters(@@parameters, wafer_values)
    res = $client.send_dcr(dcr, :export=>"log/#{__method__}.xml")
    assert $spctest.verify_dcr_accepted(res, @@parameters.count*@@wafer_values2.count, :spcerror=>true), 'DCR should throw an error'

    mrecipes.each do |mrecipe|
      assert $spctest.verify_inhibit([@@eqp, mrecipe], @@parameters.count, {:class=>:eqp_chamber_recipe, :timeout=>@@ca_time_out}), 'chamber+recipe must have an inhibit'
      assert $spctest.verify_ecomment(channels, "TOOL+CHAMBER+RECIPE_HELD: #{@@eqp}/PM3/#{mrecipe}: reason={X-S")
    end
  end


end