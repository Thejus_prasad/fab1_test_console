=begin 
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.
    
Author: Daniel Steger, 2012-01-13
=end

require_relative 'Test_Space_Setup'


# Test Space/SIL.
class Test_Space10 < Test_Space_Setup

  # def self.after_all_passed
  #   folder = $lds.folder('AutoCreated_LIT')
  #   @@parameters.each {|p| $lds.all_spc_channels(p).each {|ch| assert ch.delete}}
  # end


  def setup
    super
    return unless $lds && self.class.class_variable_defined?(:@@lot) # bypass test0000_setup
    assert_equal 0, $sv.eqp_cleanup(@@eqp)
    assert_equal 0, $sv.lot_cleanup(@@lot)
    #
    $lds.all_spc_channels('QA_PARAM_90*').each {|ch| assert ch.delete}
    folder = $lds.folder(@@autocreated_qa)
    assert folder.delete_channels, "error cleaning channels" if folder
  end


  def test1000_setup
    file_generic_keys_fixed = "etc/testcasedata/space/GenericKeySetup_QA_FIXED_Full.SpecID.xlsx"
    assert $spctest.sil_upload_verify_generickey(file_generic_keys_fixed), "file upload failed"
  end
    
  def test1001_reticle_lot
    mpd = "QA-PARAM-FTDEPM.01"
    mtool = "QAMeas"
    rparam = "L-STP-RETICLEID1"
    pparams = ["L-STP-ENERGY1-WFR", "L-STP-ENERGY2-WFR", "L-STP-ENERGY3-WFR", "L-STP-ENERGY4-WFR", "L-STP-ENERGY5-WFR"]
    reticle = "QAReticle0001"
    folder = $lds.folder('AutoCreated_LIT')
    (@@parameters + pparams).each {|p| folder.spc_channels(parameter: p).each {|ch| assert ch.delete}}
    #
    # get a process PD referenced by MPD
    pd_setup = $sildb.pd_reference(mpd: mpd, all: true)
    assert (pd_setup and pd_setup.size > 0 and pd_setup[0].pdrefs.size > 0), "MPD #{mpd} does not reference any process PDs"

    pd_setup.each_with_index do |pdref,i|
      ppd = pdref.pdrefs.flatten[0]
      $log.info "using process PD #{ppd}"
      reticle = "QAReticle00%02d" % i
      p = pparams[i]
      #
      # build and submit DCR for process PD
      dcrp = Space::DCR.new(eqp: @@eqp, lot: @@lot, op: ppd, department: 'LIT')
      dcrp.add_parameter(p, @@wafer_values, processing_areas: ["CHA"])
      dcrp.add_parameter(rparam, {@@lot=>reticle, "8OTHER.LOT"=>"NORETICLE"}, reading_type: 'Lot', value_type: "string", processing_areas: [], nocharting: true)
      $spctest.send_dcr_verify(dcrp, nsamples: @@wafer_values.size, exporttag: "#{__method__}_#{i}_p")

      ch = folder.spc_channel(parameter: p)
      s = ch.samples[-1]
      wafer = s.extractor_keys["Wafer"]
      assert $spctest.verify_sample(s, {"Reticle"=>reticle}, {"PTool"=>"-" }, @@wafer_values[wafer]), "Sample data does not match"
    end
    #
    # build and submit DCR for related MPD
    dcr = Space::DCR.new(eqp: mtool, lot: @@lot, op: mpd, department: "LIT")
    dcr.add_parameters(@@parameters, @@wafer_values, processing_areas: [])
    dcr.add_parameter(rparam, {@@lot=>"OTHERRETICLE"}, reading_type: 'Lot', value_type: "string", processing_areas: [], nocharting: true)
    $spctest.send_dcr_verify(dcr, nsamples: @@parameters.size*@@wafer_values.size)
    #
    # verify sample extractor keys
    @@parameters.each do |p|
      ch = folder.spc_channel(parameter: p)
      samples = ch.samples
      assert_equal @@wafer_values.size, samples.size, "wrong number of samples in #{ch.inspect}"
      # check last sample only
      s = samples[-1]
      s_param = (p == "QA_PARAM_904") ? nil : p
      ppd_i = pd_setup.find_index {|pdref| pdref.parameter == s_param}
      ppd = pd_setup[ppd_i].pdrefs.flatten[0]
      reticle = "QAReticle00%02d" % ppd_i
      test_ok &= $spctest.verify_data(s.extractor_keys, {"PTool"=>@@eqp, "POperationID"=>ppd})
      test_ok &= $spctest.verify_data(s.data_keys, {"Reticle"=>reticle})
    end
  end
  
  def test1002_reticle_wafer
    mpd = "QA-FIRST-PD-MB-FTDEPM.01"
    mtool = "QAMeas"
    rparam = "L-STP-RETICLEID1"
    pparams = ["L-STP-ENERGY1-WFR"]
    folder = $lds.folder("AutoCreated_LIT")
    (@@parameters + pparams).each {|p| folder.spc_channels(parameter: p).each {|ch| assert ch.delete}}

    #
    assert ppd = $sildb.pd_reference(mpd: mpd).first, "no PD reference found for #{mpd}"
    $log.info "using process PD #{ppd}"
    #
    # build and submit DCR for process PD
    wafer_values_p = Hash[@@wafer_values.collect {|w, v| [w, "QAReticle0077"]}]
    dcrp = Space::DCR.new(eqp: @@eqp, lot: @@lot, op: ppd, department: 'LIT')
    dcrp.add_parameters(pparams, @@wafer_values, processing_areas: ["CHA"])
    dcrp.add_parameter(rparam, wafer_values_p, value_type: "string", processing_areas: [])
    $spctest.send_dcr_verify(dcrp, nsamples: pparams.size*wafer_values_p.size, exporttag: "#{__method__}_p")
    # 
    # build and submit DCR for related MPD
    dcr = Space::DCR.new(eqp: mtool, lot: @@lot, op: mpd, department: 'LIT')
    dcr.add_parameters(@@parameters, @@wafer_values, processing_areas: [])
    $spctest.send_dcr_verify(dcr, nsamples: @@parameters.size*@@wafer_values.size, exporttag: "#{__method__}_m")
    #
    # verify sample extractor keys
    ret = true
    @@parameters.each {|p|
      ch = folder.spc_channel(parameter: p)
      samples = ch.samples
      assert_equal @@wafer_values.size, samples.size, "wrong number of samples in #{ch.inspect}"
      samples.each {|s|
        w = s.extractor_keys["Wafer"]        
        ret &= $spctest.verify_data(s.extractor_keys, {"POperationID"=>ppd, "PTool"=>@@eqp})
        ret &= $spctest.verify_data(s.data_keys, {"Reticle"=>wafer_values_p[w]})
      }
    }
    pparams.each {|p|
      ch = folder.spc_channel(parameter: p)
      samples = ch.samples
      assert_equal wafer_values_p.size, samples.size, "wrong number of samples in #{ch.inspect}"
      samples.each {|s|
        w = s.extractor_keys["Wafer"]        
        ret &= $spctest.verify_data(s.extractor_keys, {"PTool"=>"-" })
        ret &= $spctest.verify_data(s.data_keys, {"Reticle"=>wafer_values_p[w]})
      }
    }
    assert ret, "Sample validation failed"
  end
  
end
