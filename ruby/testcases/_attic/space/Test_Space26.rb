=begin 
Test Space/SIL.

(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.
    
Author: Steffen Steidten, 2011-11-25
=end

require_relative 'Test_Space_Setup'

# Test Space/SIL.

class Test_Space26 < Test_Space_Setup
   
  def test2608_create_autocharting_folder
    dpmt = "QACollect"
    parameter = "QA_PARAM_Collect"
    #
    # ensure autocharting folder does not exist
    folder = $lds.folder("AutoCreated_#{dpmt}")
    #
    # build and submit DCR
    dcr = Space::DCR.new(:department=>dpmt)
    dcr.add_parameter(parameter, @@wafer_values, :space_listening=>true)
    res = $client.send_dcr(dcr, :export=>"log/#{__method__}.xml")
    assert $spctest.verify_dcr_accepted(res, @@wafer_values.size), "DCR not submitted successfully"
    #
    # verify autocharting folder creation
    folder = $lds.folder("AutoCreated_#{dpmt}")
    assert_not_nil folder, "Autocharting folder has not been created"
  end

  def test2609_create_autocharting_folders_load
    #n=20 is good for LET, 3 works for ITDC, higher values are not tested in ITDC but may work
    n = 20
    n = 20 if $env == "let"
    #    
    _dpmt = "QACollect"
    parameter = "QA_PARAM_Collect"
    productgroup = "QAPG"
    product = "SILPROD.0"
    #
    # send DCRs
    test_ok = true
    $threads = n.times.collect {|i|
      Thread.new {
        client = Space::Client.new($env, :timeout=>900)
        dcr = Space::DCR.new(:department=>"#{_dpmt}", :technology=>"#{i}NM", :productgroup=>"#{productgroup}#{i}", :product=>"#{product}#{i}")
        dcr.add_parameter("#{parameter}", @@wafer_values, :space_listening=>true)
        res = client.send_dcr(dcr, :export=>"log/#{__method__}_#{i}.xml")
        ok = $spctest.verify_dcr_accepted(res, @@wafer_values.size)
        $log.warn "#{i}: #{res.inspect}" unless ok
        test_ok &= ok
      }
    }
    $threads.each {|t| t.join}
    assert test_ok
    #
    # verify autocharting folder creation
    test_ok = true
    folder = $lds.folder("AutoCreated_#{_dpmt}")
    ($log.warn "missing folder AutoCreated_#{_dpmt}"; test_ok = false) unless folder
    assert test_ok
    #
  end
end
