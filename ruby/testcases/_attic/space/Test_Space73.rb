=begin
Test Space/SIL.

(c) Copyright 2012 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: D.Steger, 2012-10-08
=end

require_relative 'Test_Space_Setup'

# Test WebMRB interface of SIL
class Test_Space73 < Test_Space_Setup
  #@@mpd_qa = "QA-MGT-NMETDEPDR.QA"
  @@mpd_qa = "QA-CLAIM-MEMO.01"
  @@chambers = {"PM1"=>5, "PM2"=>6, "CHX"=>7}
  @@mtool = "UTF001"
  @@product = "SILPROD.01"
  @@productgroup = "QAPG1"
  @@param_depts = {"QA_MEMO_900"=>"LIT", "QA_MEMO_901"=>"TFM", "QA_MEMO_902"=>"QA", "QA_MEMO_903"=>"ETC", "QA_MEMO_904"=>"QA"}
  @@add_lots = ["SIL005.00", "SIL006.00", "SIL007.00"]
  @@manual_check = false

  def test7300_setup
    $setup_ok = false
    #
    assert @@ppd_qa = $sildb.pd_reference(:mpd=>@@mpd_qa).first, "no PD reference found for #{@@mpd_qa}"

    ["AutoRaiseMRB", "RaiseMRB", "AutoLotHold"].each {|action|
      assert $spc.corrective_action(action), "CA #{action.inspect} not defined"
    }
    #specid = @@specLimitsMRBTable
    #specid = "M14-00020003" if $env == "f8stag"

    #content = nil
    #version = nil
    #$log.info "Getting spec: #{specid}"
    #content,version = $sfc.get_specification(specid)
    #refute_nil version, "No Spec #{specid} found on SetupFC"
    #refute_nil content, "No Spec #{specid} content found on SetupFC"
    #assert $spctest.verify_speclimit(specid: specid, specversion: version.to_i, :all_active=>true), "Speclimits #{specid}, version #{version} does not exist"

    values = [20, 14, 22, 23, 14, 15]
    @@wafer_values2 = {}
    lot_wafers = $sv.lot_info(@@lot,:wafers=>true).wafers.take(values.count)
    lot_wafers.each_with_index {|w,j| @@wafer_values2[w.wafer] = values[j]}
    #
    $setup_ok = true
  end

  # Auto raise an MRB grouped by parameter, equipment, module
  # the sublottype is configured to raise an MRB
  def test7301_autoraisemrb_on_oos
    _scenario_autoraise_mrb(__method__, "MW", true)
  end

  # OOC is ignored
  def test7302_no_mrb_autoraised_on_ooc
    _scenario_autoraise_mrb(__method__, "MW", false, ooc_only: true)
  end

  # Auto raise an MRB grouped by parameter, equipment, module
  # the sublottype is not configured to raise an MRB
  def test7303_no_mrb_autoraised_on_filtered_sublottype
    _scenario_autoraise_mrb(__method__, "EC", false)
  end

  def test7304_lot_parameter_produces_invalid_mrb_data
    _scenario_autoraise_mrb(__method__, "MW", false, lot_parameter: true)
  end

  def test7305_multiple_lots_should_autoraise_only_one_mrb_by_channel
    _scenario_autoraise_mrb(__method__, "MW", true, addlot: "SIL005.00", addlotsublottype: "PO")
  end

  def test7306_multiple_lots_should_autoraise_only_one_mrb_and_if_filtered_by_sublottype
    _scenario_autoraise_mrb(__method__, "MW", true, addlot: "SIL005.00", addlotsublottype: "EC", addlot_filtered: true)
  end

  def test7307_multiple_lots_should_autoraise_two_mrbs_if_different_equipments_are_involved
    _scenario_autoraise_mrb(__method__, "MW", true, addlot: "SIL005.00", addlotsublottype: "PO", other_eqp: true)
  end

  # Manually raise an MRB.
  #
  # Sublottype should not be filtered.
  def test7310_raise_mrb_will_not_filter_sublottype
    _scenario_raise_mrb("EC", true, division: "LIT")
    _scenario_raise_mrb("EC", true, division: "MTR")
    _scenario_raise_mrb("EC", true, division: "DIF")
  end

  def test7311_no_mrb_raised_on_ooc
    _scenario_raise_mrb("EC", false, ooc_only: true)
  end

  # Manually raise an batch MRB.
  # Sublottype should not be filtered.
  def test7312_batch_raise_mrb_for_all_lotswafers_in_process_run
    _scenario_raise_mrb("EC", true, add_lots: @@add_lots)
  end

  # Manually raise an MRB. Right now this is only for preparation.
  # Sublottype should not be filtered.
  def test7320_raise_mrb_from_ecap
    _scenario_raise_mrb("PO", true, ecap: true)
  end

  def _scenario_autoraise_mrb(method, sublottype, mrb_created, params={})
    $log.info "scenario_autoraise_mrb #{method.inspect}, #{sublottype}, #{mrb_created}, #{params.inspect}"
    parameters = ["QA_MEMO_900", "QA_MEMO_901", "QA_MEMO_902", "QA_MEMO_903", "QA_MEMO_904"]
    addlot = params[:addlot]
    addsublottype = params[:addlotsublottype]
    addlot_filtered = (params[:addlot] and params[:addlot_filtered])
    lotparameter = (params[:lot_parameter] or false)
    othereqp = (params[:other_eqp] or false)
    ooc = (params[:ooc_only] or false)

    #
    channels = create_clean_channels(parameters: parameters, dept_hash: @@param_depts) #_create_chan( parameters, "QA" )
    #
    assert_equal 0, $sv.lot_cleanup(@@lot)
    assert_equal 0, $sv.eqp_cleanup(@@eqp)
    assert_equal 0, $sv.lot_cleanup(addlot) if addlot
    assert_equal 0, $sv.eqp_cleanup("#{@@eqp}_1") if othereqp

    assert $spctest.assign_verify_cas(channels, ["AutoRaiseMRB", "RaiseMRB", "AutoLotHold"])

    # need actual wafer ids
    wafer_values = {}
    keys = $sv.lot_info(@@lot,:wafers=>true).wafers.take(@@wafer_values2.values.count)
    keys.each_with_index {|w,j| wafer_values[w.wafer] = @@wafer_values2.values[j]}

    #
    submit_process_dcr(@@ppd_qa, :wafer_values=>@@wafer_values2, :chambers=>@@chambers)
    # additional lot
    addlot_wafer_values = {}
    if addlot
      lot_wafers = $sv.lot_info(addlot,:wafers=>true).wafers.take(@@wafer_values2.values.count)
      lot_wafers.each_with_index {|w,j| addlot_wafer_values[w.wafer] = @@wafer_values2.values[j]}
      eqp = @@eqp
      eqp += "_1" if othereqp
      submit_process_dcr(@@ppd_qa, :eqp=>eqp, :lot=>addlot, :wafer_values=>addlot_wafer_values,
        :chambers=>@@chambers, export: "log/#{caller_locations(1,1)[0].label}_p.xml")
    end

    # send DCR with OOS values
    wafer_values = Hash[*wafer_values.collect {|w,v| [w,v+(ooc ? 6.5 : 100)]}.flatten]
    dcr = Space::DCR.new(:lot=>@@lot, :eqp=>@@mtool, :op=>@@mpd_qa,
        :product=>@@product, :productgroup=>@@productgroup, :sublottype=>sublottype)

    if lotparameter
      dcr.add_parameters(parameters, {@@lot=>70.0}, :reading_type=>'Lot')
      wafer_count = 1
    else
      dcr.add_parameters(parameters, wafer_values)
      wafer_count = wafer_values.size
    end

    if addlot
      dcr.add_lot(addlot, :sublottype=>addsublottype, :op=>@@mpd_qa, :product=>@@product, :productgroup=>@@productgroup)
      wafer_values = {}
      addlot_wafer_values.each_pair do |w,v|
        wafer_values[w] = v+ (ooc ? 6.5 : 100)
        dcr.add_wafer(w)
      end
      wafer_count += wafer_values.size
      dcr.add_parameters(parameters, wafer_values)
    end

    res = $client.send_dcr(dcr, :export=>"log/#{caller_locations(1,1)[0].label}_m.xml")
    oocs = parameters.count*(ooc ? 2 : 2*wafer_count) # Mean above control limit / N over M
    assert $spctest.verify_dcr_accepted(res, parameters.count*wafer_count, :nooc=>oocs), "DCR not submitted successfully"

    #
    # verify future hold from AutoLotHold
    msg = ooc ? "[(Mean above control limit)" : "[(Raw above specification)"
    assert $spctest.verify_futurehold2(@@lot, msg, :timeout=>@@ca_time_out, :dcr_lot=>dcr.find_lot_context(@@lot)), "lot must have a future hold"

    if mrb_created
      # verify MRB
      m_count = (othereqp ? 2 : 1) * parameters.count
      assert $spctest.verify_mrbs(channels, m_count), "MRBs not found in comment"

      $log.info("Check that MRBs are created for #{@@lot} and each parameter #{parameters.inspect}")
      $log.info("Check that additional MRBs are created for #{addlot} and each parameter #{parameters.inspect}") if othereqp
      ####gets
      if addlot_filtered
        assert $spctest.verify_ecomment(channels, "WEB_MRB: Mrb created.", lot: @@lot), "invalid comments"
        assert $spctest.verify_ecomment(channels, "WEB_MRB: Sample filtered", lot: addlot), "invalid comments"
      else
        assert $spctest.verify_ecomment(channels, "WEB_MRB: Mrb created."), "invalid comments"
      end
    elsif lotparameter
      $spctest.verify_ecomment channels, "WEB_MRB: Mrb creation failed"
      parameters.each do |p|
        assert $spctest.verify_notification("An error message was received from the MRB System", p, timeout: 5), "wrong notification for #{p}"
      end
    elsif not ooc
      assert $spctest.verify_ecomment(channels, "WEB_MRB: Sample filtered"), "invalid comments"
      $log.info("Check that no new MRB was raised")
      gets
    end
  end

  def _scenario_raise_mrb(sublottype, mrb_created, params={})
    parameters = @@param_depts.keys
    ecap = (params[:ecap] or false)
    ooc = (params[:ooc_only] or false)
    add_lots = (params[:add_lots] or [])
    #
    channels = create_clean_channels(parameters: parameters, dept_hash: @@param_depts) #_create_chan( parameters, "QA" )
    #
    assert_equal 0, $sv.lot_cleanup(@@lot)
    assert_equal 0, $sv.eqp_cleanup(@@eqp)
    add_lots.each {|lot|  assert_equal 0, $sv.lot_cleanup(lot)}

    if ecap
      assign_events(channels, ["AutoLotHold", "eCAP: QA Raise MRB:MRG"], :valuations=>[1,2])      #, "eCAP: QA MRB"
    else
      unless ooc
        # Since Space API does not allow to assign CAs per violation, need to remove all other then OOC
        vals = Space::SpcApi::Channel.all_valuations.find_all { |v| v.vid <=2 }
        channels.each {|ch| ch.set_valuations(vals)}
      end
      assign_events(channels, ["RaiseMRB", "AutoLotHold"])
    end
    # need actual wafer ids
    wafer_values = {}
    keys = $sv.lot_info(@@lot,:wafers=>true).wafers.take(@@wafer_values2.values.count)
    keys.each_with_index {|w,j| wafer_values[w.wafer] = @@wafer_values2.values[j]}

    # additional lots
    add_wafer_values = add_lots.collect {|l| Hash[*[$sv.lot_info(l,:wafers=>true).wafers.collect{|w| [w.wafer, 10]}]]}
    $log.info " added wafers: #{add_wafer_values.inspect}"
    #
    submit_process_dcr(@@ppd_qa, :wafer_values=>wafer_values, :chambers=>@@chambers, :addlots=>add_lots,
      :addwafer_values=>add_wafer_values, :export=>"log/#{caller_locations(1,1)[0].label}_p.xml") #old ruby 1.8 style: caller[0][/`([^']*)'/, 1].to_filename
    # send DCR with OOC values
    wafer_values = Hash[*wafer_values.collect {|w,v| [w,v+(ooc ? 6.5 : 100)]}.flatten]

    #wafer_values = {}
    ##@@wafer_values2.each_pair {|w,v| wafer_values[w] = 5.times.collect {|i| i == 0 ? v+10 : v } }

    dcr = Space::DCR.new(:lot=>@@lot, :eqp=>@@mtool, :op=>@@mpd_qa,
      :product=>@@product, :productgroup=>@@productgroup, :sublottype=>sublottype)
    dcr.add_parameters(parameters, wafer_values)

    wafer_count = wafer_values.size
    # add_lots.each do |addlot|
      # dcr.add_lot(addlot, :op=>@@mpd_qa, :product=>@@product, :productgroup=>@@productgroup)
      # lot_wafers = $sv.lot_info(addlot,:wafers=>true).wafers.take(wafer_values.values.count)
      # addlot_wafer_values = {}
      # lot_wafers.each_with_index {|w,j|
        # dcr.add_wafer(w.wafer)
        # addlot_wafer_values[w.wafer] = @@wafer_values2.values[j]
      # }
      # wafer_count += addlot_wafer_values.size
      # dcr.add_parameters(parameters, addlot_wafer_values)
    # end

    res = $client.send_dcr(dcr, :export=>"log/#{caller_locations(1,1)[0].label}_m.xml")

    ooc_count = parameters.count*(ooc ? 2 : wafer_values.size)
    ooc_count = ooc_count*2 if ecap
    assert $spctest.verify_dcr_accepted(res, parameters.count*wafer_count, :nooc=>ooc_count), "DCR not submitted successfully"

    #
    # verify future hold from AutoLotHold
    msg = ooc ? "[(Mean above control limit)" : "[(Raw above specification)"
    assert $spctest.verify_futurehold2(@@lot, msg, :timeout=>@@ca_time_out, :dcr_lot=>dcr.find_lot_context(@@lot)), "lot must have a future hold"

    m_count = 0
    if mrb_created
      if ecap
        m_count = ooc_count
        assert wait_for(:timeout=>60) {
          $spctest.verify_ecomment channels, "WEB_MRB: Mrb created."
        }, "invalid comments for #{channels.inspect}"
      else
        sample_comments = {}

        ch = channels[-1]
        ch.division = params[:division] if params[:division]
        s = ch.samples(:ckc=>true).select {|s| s.violations.size > 0 }.last
        assert s, "no sample with violation found"
        sample_comments[s.sid] = "RaiseMRB #{s.sid}"
        assert s.assign_ca($spc.corrective_action("RaiseMRB"), :comment=>sample_comments[s.sid]), "error assigning corrective action"

        m_count = 1
        assert wait_for(:timeout=>60) {
          s = ch.samples(:ckc=>true).select {|a| a.sid == s.sid}.first
          s.ecomment.index("WEB_MRB: Mrb created.")
        }, "no MRB found"
        assert $spctest.verify_sapphire_messages(sample_comments)
      end
    end
    assert $spctest.verify_mrbs(channels, m_count), "wrong number of mrbs"
  end

  def assign_events(channels, actions, params={})
    channels.each {|ch|
      $spc.set_events(ch, actions, params)
      vals = ch.valuations
      assert vals.size > 0, "no channel valuations defined, check channel and template"
      aa = $spc.events(vals[0]).collect {|a| a.name}.sort
      assert_equal actions.sort, aa, "error setting corrective actions"
      ch.set_state('Offline')
    }
    $log.info "#{actions.inspect} set correctly"
  end

end
