=begin 
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.
    
Author: Conny Stenzel, 2012-10-09
=end

require_relative 'Test_Space_Setup'


# =Testcases for SPACE/SIL - SpecLimits Hierarchy - Wildcard tests for PD/Technology/Route
#
# <i>Initially wildcards were not requested as requirement. However, the implementation as it currently is, 
# works in the way that an empty cell at the columns technology or route is interpreted as a wildcard. The most specified line wins. 
# Empty PDs instead are allowed but result in no spec limits.</i>
class Test_Space251 < Test_Space_Setup
  @@control_limit_coef_1 = 0.4
  @@control_limit_coef_2 = 0.875
  @@control_limit_coef_3 = 1.125
  @@twosigma_factor = 0.60
  @@file_generic_keys_dc_ecp = 'etc/testcasedata/space/GenericKeySetup_QA_DC_ECP.SpecID.xlsx'
  @@file_generic_keys_group_ecp = 'etc/testcasedata/space/GenericKeySetup_QA_Group_ECP.SpecID.xlsx'
  

  def setup
    super
    return unless $lds && self.class.class_variable_defined?(:@@lot) # bypass test0000_setup
    assert_equal 0, $sv.eqp_cleanup(@@eqp)
    assert_equal 0, $sv.lot_cleanup(@@lot)
    #
    [$lds, $lds_setup].each {|lds|
      lds.all_spc_channels('QA_PARAM_90*').each {|ch| assert ch.delete}
      folder = lds.folder(@@autocreated_qa)
      assert folder.delete_channels, "error cleaning channels" if folder
    }
  end


  def test251000_0_setup
    $setup_ok = false
    #
    #assert($spctest.verify_monitoringspeclimit(specid: @@ecp_spec_id, specversion: @@ecp_spec_version, all_active: true), "Please submit Spec #{@@ecp_spec_id} with SetupFC") if $spctest.env == "itdc"
    #assert $spctest.sil_upload_verify_generickey(@@file_generic_keys_dc_ecp), "file upload failed"
    #
    $setup_ok = true
  end
  
  def test251000_speclimits_INLINE
    params = ['QA_PARAM_901_ECP', 'QA_PARAM_902_ECP']
    specs = [400, 500, 600]
    _send_speclimits_dcr(params, specs, '45NM', 'QA-SPEC-HIER-METROLOGY-04.01', 'SIL-0002.01', true)
  end
  
  def test251001_speclimits_INLINE
    params = ['QA_PARAM_901_ECP', 'QA_PARAM_902_ECP']
    specs = []
    _send_speclimits_dcr(params, specs, 'xxNM', 'QA-SPEC-HIER-METROLOGY-04.01', 'SIL-0002.01', false)
  end
  
  def test251002_speclimits_INLINE
    params = ['QA_PARAM_901_ECP', 'QA_PARAM_902_ECP']
    specs = []
    _send_speclimits_dcr(params, specs, '45NM', 'QA-SPEC-HIER-METROLOGY-04.01', 'SIL-xxx2.01', false)
  end
  
  def test251003_speclimits_INLINE
    params = ['QA_PARAM_901_ECP', 'QA_PARAM_902_ECP']
    specs = []
    _send_speclimits_dcr(params, specs, 'xxNM', 'QA-SPEC-HIER-METROLOGY-04.01', 'SIL-xxx2.01', false)
  end
  
  def test251010_speclimits_INLINE
    params = ['QA_PARAM_903_ECP', 'QA_PARAM_904_ECP']
    specs = [499, 599, 699]
    _send_speclimits_dcr(params, specs, '45NM', 'QA-SPEC-HIER-METROLOGY-04.01', 'SIL-0002.01', true)
  end
  
  def test251011_speclimits_INLINE
    params = ['QA_PARAM_903_ECP', 'QA_PARAM_904_ECP']
    specs = [500, 600, 700]
    _send_speclimits_dcr(params, specs, 'xxNM', 'QA-SPEC-HIER-METROLOGY-04.01', 'SIL-0002.01', true)
  end
  
  def test251012_speclimits_INLINE
    params = ['QA_PARAM_903_ECP', 'QA_PARAM_904_ECP']
    specs = [501, 601, 701]
    _send_speclimits_dcr(params, specs, '45NM', 'QA-SPEC-HIER-METROLOGY-04.01', 'SIL-xxx2.01', true)
  end
  
  def test251013_speclimits_INLINE
    params = ['QA_PARAM_903_ECP', 'QA_PARAM_904_ECP']
    specs = [502, 602, 702]
    _send_speclimits_dcr(params, specs, 'xxNM', 'QA-SPEC-HIER-METROLOGY-04.01', 'SIL-xxx2.01', true)
  end
  
  def test251020_speclimits_INLINE
    params = ['QA_PARAM_905_ECP', 'QA_PARAM_906_ECP']
    specs = [600, 700, 800]
    _send_speclimits_dcr(params, specs, '45NM', 'QA-SPEC-HIER-METROLOGY-04.01', 'SIL-0002.01', true)
  end
  
  def test251021_speclimits_INLINE
    params = ['QA_PARAM_905_ECP', 'QA_PARAM_906_ECP']
    specs = []
    _send_speclimits_dcr(params, specs, 'xxNM', 'QA-SPEC-HIER-METROLOGY-04.01', 'SIL-0002.01', false)
  end
  
  def test251022_speclimits_INLINE
    params = ['QA_PARAM_905_ECP', 'QA_PARAM_906_ECP']
    specs = [600, 700, 800]
    _send_speclimits_dcr(params, specs, '45NM', 'QA-SPEC-HIER-METROLOGY-04.01', 'SIL-xxx2.01', true)
  end
  
  def test251023_speclimits_INLINE
    params = ['QA_PARAM_905_ECP', 'QA_PARAM_906_ECP']
    specs = []
    _send_speclimits_dcr(params, specs, 'xxNM', 'QA-SPEC-HIER-METROLOGY-04.01', 'SIL-xxx2.01', false)
  end
  
  def test251030_speclimits_INLINE
    params = ['QA_PARAM_907_ECP', 'QA_PARAM_908_ECP']
    specs = [700, 800, 900]
    _send_speclimits_dcr(params, specs, '45NM', 'QA-SPEC-HIER-METROLOGY-04.01', 'SIL-0002.01', true)
  end
  
  def test251031_speclimits_INLINE
    params = ['QA_PARAM_907_ECP', 'QA_PARAM_908_ECP']
    specs = [700, 800, 900]
    _send_speclimits_dcr(params, specs, 'xxNM', 'QA-SPEC-HIER-METROLOGY-04.01', 'SIL-0002.01', true)
  end
  
  def test251032_speclimits_INLINE
    params = ['QA_PARAM_907_ECP', 'QA_PARAM_908_ECP']
    specs = [700, 800, 900]
    _send_speclimits_dcr(params, specs, '45NM', 'QA-SPEC-HIER-METROLOGY-04.01', 'SIL-xxx2.01', true)
  end
  
  def test251033_speclimits_INLINE
    params = ['QA_PARAM_907_ECP', 'QA_PARAM_908_ECP']
    specs = [700, 800, 900]
    _send_speclimits_dcr(params, specs, 'xxNM', 'QA-SPEC-HIER-METROLOGY-04.01', 'SIL-xxx2.01', true)
  end
  
  def test251040_speclimits_INLINE
    params = ['QA_PARAM_909_ECP', 'QA_PARAM_910_ECP']
    specs = [499, 599, 699]
    _send_speclimits_dcr(params, specs, '45NM', 'QA-SPEC-HIER-METROLOGY-04.01', 'SIL-0002.01', true)
  end
  
  def test251041_speclimits_INLINE
    params = ['QA_PARAM_909_ECP', 'QA_PARAM_910_ECP']
    specs = [500, 600, 700]
    _send_speclimits_dcr(params, specs, 'xxNM', 'QA-SPEC-HIER-METROLOGY-04.01', 'SIL-0002.01', true)
  end
  
  def test251042_speclimits_INLINE
    params = ['QA_PARAM_909_ECP', 'QA_PARAM_910_ECP']
    specs = [501, 601, 701]
    _send_speclimits_dcr(params, specs, '45NM', 'QA-SPEC-HIER-METROLOGY-04.01', 'SIL-xxx2.01', true)
  end
  
  def test251043_speclimits_INLINE
    params = ['QA_PARAM_909_ECP', 'QA_PARAM_910_ECP']
    specs = [502, 602, 702]
    _send_speclimits_dcr(params, specs, 'xxNM', 'QA-SPEC-HIER-METROLOGY-04.01', 'SIL-xxx2.01', true)
  end
  
  def test251050_speclimits_INLINE
    params = ['QA_PARAM_911_ECP', 'QA_PARAM_912_ECP']
    specs = [400, 500, 600]
    _send_speclimits_dcr(params, specs, '45NM', 'QA-SPEC-HIER-METROLOGY-05.01', 'SIL-0002.01', true)
  end
  
  def test251051_speclimits_INLINE
    params = ['QA_PARAM_911_ECP', 'QA_PARAM_912_ECP']
    specs = [400, 500, 600]
    _send_speclimits_dcr(params, specs, 'xxNM', 'QA-SPEC-HIER-METROLOGY-05.01', 'SIL-0002.01', true)
  end
  
  def test251052_speclimits_INLINE
    params = ['QA_PARAM_911_ECP', 'QA_PARAM_912_ECP']
    specs = [400, 500, 600]
    _send_speclimits_dcr(params, specs, '45NM', 'QA-SPEC-HIER-METROLOGY-05.01', 'SIL-xxx2.01', true)
  end
  
  def test251053_speclimits_INLINE
    params = ['QA_PARAM_911_ECP', 'QA_PARAM_912_ECP']
    specs = [400, 500, 600]
    _send_speclimits_dcr(params, specs, 'xxNM', 'QA-SPEC-HIER-METROLOGY-05.01', 'SIL-xxx2.01', true)
  end
  
  def test251060_speclimits_INLINE
    params = ['QA_PARAM_913_ECP', 'QA_PARAM_914_ECP']
    specs = []
    _send_speclimits_dcr(params, specs, '45NM', 'QA-SPEC-HIER-METROLOGY-05.01', 'SIL-0002.01', false)
  end
  
  def test251070_speclimits_INLINE
    params = ['QA_PARAM_915_ECP', 'QA_PARAM_916_ECP']
    specs = [600, 700, 800]
    _send_speclimits_dcr(params, specs, '45NM', 'QA-SPEC-HIER-METROLOGY-05.01', 'SIL-0002.01', true)
  end
  
  def test251080_speclimits_INLINE
    params = ['QA_PARAM_917_ECP', 'QA_PARAM_918_ECP']
    specs = [700, 800, 900]
    _send_speclimits_dcr(params, specs, '45NM', 'QA-SPEC-HIER-METROLOGY-05.01', 'SIL-0002.01', true)
  end
  
  def test251100_speclimits_TEST
    params = ['QA_PARAM_901_ECP', 'QA_PARAM_902_ECP']
    specs = [400, 500, 600]
    _send_speclimits_dcr(params, specs, 'TEST', 'QA-SPEC-HIER-METROLOGY-04.01', 'SIL-0002.01', true)
  end
  
  def test251101_speclimits_TEST
    params = ['QA_PARAM_903_ECP', 'QA_PARAM_904_ECP']
    specs = [500, 600, 700]
    _send_speclimits_dcr(params, specs, 'TEST', 'QA-SPEC-HIER-METROLOGY-04.01', 'SIL-0002.01', true)
  end
  
  def test251102_speclimits_TEST
    params = ['QA_PARAM_905_ECP', 'QA_PARAM_906_ECP']
    specs = [600, 700, 800]
    _send_speclimits_dcr(params, specs, 'TEST', 'QA-SPEC-HIER-METROLOGY-04.01', 'SIL-0002.01', true)
  end
  
  def test251103_speclimits_TEST
    params = ['QA_PARAM_907_ECP', 'QA_PARAM_908_ECP']
    specs = [700, 800, 900]
    _send_speclimits_dcr(params, specs, 'TEST', 'QA-SPEC-HIER-METROLOGY-04.01', 'SIL-0002.01', true)
  end
  
  def test251104_speclimits_TEST
    params = ['QA_PARAM_911_ECP', 'QA_PARAM_912_ECP']
    specs = [400, 500, 600]
    _send_speclimits_dcr(params, specs, 'TEST', 'QA-SPEC-HIER-METROLOGY-05.01', 'SIL-0002.01', true)
  end
  
  def test251105_speclimits_TEST
    params = ['QA_PARAM_913_ECP', 'QA_PARAM_914_ECP']
    specs = [500, 600, 700]
    _send_speclimits_dcr(params, specs, 'TEST', 'QA-SPEC-HIER-METROLOGY-05.01', 'SIL-0002.01', true)
  end
  
  def test251106_speclimits_TEST
    params = ['QA_PARAM_915_ECP', 'QA_PARAM_916_ECP']
    specs = [600, 700, 800]
    _send_speclimits_dcr(params, specs, 'TEST', 'QA-SPEC-HIER-METROLOGY-05.01', 'SIL-0002.01', true)
  end
  
  def test251107_speclimits_TEST
    params = ['QA_PARAM_917_ECP', 'QA_PARAM_918_ECP']
    specs = [700, 800, 900]
    _send_speclimits_dcr(params, specs, 'TEST', 'QA-SPEC-HIER-METROLOGY-05.01', 'SIL-0002.01', true)
  end
  
  def test251108_speclimits_INLINE
    params = ['QA_PARAM_919_ECP', 'QA_PARAM_920_ECP']
    specs = [750, 850, 950]
    _send_speclimits_dcr(params, specs, '45NM', 'QA-SPEC-HIER-METROLOGY-05.xx', 'SIL-0002.01', true)
  end
  
  def test251109_speclimits_TEST
    params = ['QA_PARAM_919_ECP', 'QA_PARAM_920_ECP']
    specs = [750, 850, 950]
    _send_speclimits_dcr(params, specs, 'TEST', 'QA-SPEC-HIER-METROLOGY-05.xx', 'SIL-0002.01', true)
  end
  
  def test251110_speclimits_INLINE
    params = ['QA_PARAM_996_ECP', 'QA_PARAM_997_ECP']
    specs = [750, 850, 950]
    $spctest.sil_upload_verify_generickey(@@file_generic_keys_group_ecp)
    _send_speclimits_dcr(params, specs, '45NM', 'QA-MGT-NMETDEPDR.01', 'SIL-0002.01', true, prodgroup: 'QAPGECP1')
  end
  
  def test251111_speclimits_INLINE
    params = ['QA_PARAM_996_ECP', 'QA_PARAM_997_ECP']
    specs = [751, 851, 951]
    #$spctest.sil_upload_verify_generickey(@@file_generic_keys_group_ecp)
    _send_speclimits_dcr(params, specs, '32NM', 'QA-MGT-NMETDEPDR.01', 'SIL-0002.01', true, prodgroup: 'QAPGECP1')
  end
  
  def test251112_speclimits_INLINE
    params = ['QA_PARAM_996_ECP', 'QA_PARAM_997_ECP']
    specs = [752, 852, 952]
    #$spctest.sil_upload_verify_generickey(@@file_generic_keys_group_ecp)
    _send_speclimits_dcr(params, specs, '22NM', 'QA-MGT-NMETDEPDR.01', 'SIL-0002.01', true, prodgroup: 'QAPGECP1')
  end
  
  def test251113_speclimits_INLINE
    params = ['QA_PARAM_996_ECP', 'QA_PARAM_997_ECP']
    specs = []
    _send_speclimits_dcr(params, specs, '88NM', 'QA-MGT-NMETDEPDR.01', 'SIL-0002.01', false, prodgroup: 'QAPGECP1')
  end
  
  def test251114_speclimits_TEST
    params = ['QA_PARAM_996_ECP', 'QA_PARAM_997_ECP']
    specs = [753, 853, 953]
    $spctest.sil_upload_verify_generickey(@@file_generic_keys_dc_ecp)
    _send_speclimits_dcr(params, specs, 'TEST', 'QA-MGT-NMETDEPDR.01', 'SIL-0002.01', true, prodgroup: 'QAPGECP1')
  end
  
  def test251115_speclimits_INLINE
    params = ['QA_PARAM_996_ECP', 'QA_PARAM_997_ECP']
    specs = []
    $spctest.sil_upload_verify_generickey(@@file_generic_keys_dc_ecp)
    _send_speclimits_dcr(params, specs, '200KM', 'QA-MGT-NMETDEPDR.01', 'SIL-0002.01', false)
  end
  
  def test251116_speclimits_INLINE
    params = ['QA_PARAM_1000_ECP']
    specs = [753, 853, 953]
    $spctest.sil_upload_verify_generickey(@@file_generic_keys_dc_ecp)
    _send_speclimits_dcr(params, specs, 'NoTech', 'QA-MGT-NMETDEPDR.01', 'NoRoute', false)
  end
  
  def test251117_speclimits_INLINE
    params = ['QA_PARAM_1001_ECP']
    specs = [753, 853, 953]
    $spctest.sil_upload_verify_generickey(@@file_generic_keys_dc_ecp)
    _send_speclimits_dcr(params, specs, 'INLINE', 'NoRespPD.01', 'NoRoute', false, lds: $lds)
  end
  
  def test251300_speclimits_TEST_WildCards
    params = ['QA_PARAM_TG251_TC251300_004']
    specs = [5, 50, 100]
    _send_speclimits_dcr(params, specs, 'TEST', 'QA-SPEC-HIER-METROLOGY-01.01', 'SIL-0002.01', false, prodgroup: 'PG251300')
  end
  
  
  def _send_speclimits_dcr(parameters, specs, technology, op, route, expect_limits, params={})
    [$lds, $lds_setup].each {|lds| 
      folder = lds.folder(@@autocreated_qa)
      folder.delete_channels if folder
      parameters.collect {|p| lds.all_spc_channels("#{p}*")}.flatten.each {|ch| ch.delete}
    }
    #
    # build and submit DCR with one parameter only to identify the parameter in the hold claim memo
    specs = [0, 0, 0] if specs.nil? || specs.empty?
    prodgroup = params[:prodgroup] || "3261C"
    prod = "SILPROD2.01"
    dcr = Space::DCR.new(eqp: @@eqp, lot: @@lot, technology: technology, op: op, route: route, productgroup: prodgroup, product: prod)
    wafer_values = Hash[@@wafer_values.collect {|w, v| [w, v + 650]}]
    values_oos = 0
    wafer_values.each {|w, v| values_oos += 1 if (v < specs[0]) || (v > specs[2])}
    values_oos = values_oos * parameters.count
    
    xbar_UCL = specs[1] + @@control_limit_coef_1 * (specs[2] - specs[1])
    xbar_LCL = specs[1] - @@control_limit_coef_1 * (specs[1] - specs[0])
    values_ooc = 0
    wafer_values.each {|w, v| values_ooc += 1 if (v < xbar_LCL) || (v > xbar_UCL)}
    values_ooc = values_ooc * parameters.count
    
    dcr.add_parameters(parameters, wafer_values)
    res = $client.send_dcr(dcr, export: "log/#{caller_locations(1,1)[0].label}.xml")
    nsamples = parameters.size * wafer_values.size
    nooc = expect_limits ? (values_oos + values_ooc) : 0
    assert $spctest.verify_dcr_accepted(res, nsamples, nooc: nooc), "DCR not submitted successfully"
    
    if expect_limits
      lds = params[:lds] || ((technology == 'TEST') ? $lds_setup : $lds)
      folder = lds.folder(@@autocreated_qa)
      parameters.each {|p|
        ch = folder.spc_channel(parameter: p)
        assert $spctest.verify_sample_speclimits(ch.samples.last, specs[0], specs[1], specs[2])
      }
    end
  end
  
end
