=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: D. Steger, 2012-08-03
=end

require_relative 'Test_Space_Setup'


# eCAP Test cases
class Test_Space22 < Test_Space_Setup

  @@wafer_values2 = {"WWWWSIL00700"=>40, "WWWWSIL00701"=>31, "WWWWSIL00702"=>32, "WWWWSIL00703"=>43, "WWWWSIL00704"=>64, "WWWWSIL00705"=>32}

  #@@mpd_qa = "QA-MGT-NMETDEPDR.QA"
  @@mpd_qa = "QA-MB-FTDEPM-LIS-CAEX.01"
	@@chamber_a = "PM1"
	@@chamber_b = "PM2"
	@@chamber_c = "CHX"
  @@chambers = {@@chamber_a=>5, @@chamber_b=>6, @@chamber_c=>7}

  def setup
    super
    return unless $lds && self.class.class_variable_defined?(:@@lot) # bypass test0000_setup
    assert_equal 0, $sv.eqp_cleanup(@@eqp)
    assert_equal 0, $sv.lot_cleanup(@@lot)
    #
    [$lds, $lds_setup].each {|lds|
      lds.all_spc_channels('QA_PARAM_90*').each {|ch| assert ch.delete}
      folder = lds.folder(@@autocreated_qa)
      assert folder.delete_channels, "error cleaning channels" if folder
    }
  end


  def test2200_setup
    $setup_ok = false
    #
    assert @@ppd_qa = $sildb.pd_reference(mpd: @@mpd_qa).first, "no PD reference found for #{@@mpd_qa}"
    $log.info "using QA process PD #{@@ppd_qa}"
    #
    assert $spc.tsg("eCAP: QA TW Grading"), "TSG #{name.inspect} not defined"
    #
    $setup_ok = true
  end
  
  def test2201_ecap_tw_grading
    channels = create_clean_channels
    channels = @@parameters.collect{|p| folder.spc_channel(:parameter=>p)}
    @@wafer_values2.keys.each do |w|
      assert $sv.user_parameter_delete_verify("Wafer", w, "WfrGrade"), "wafer script parameter not clean"
    end
    #
    # set TSGs
    actions = ["eCAP: QA TW Grading"]
    ##assign_verify_tsgs(channels, actions)
    channels.each do |ch|
      $spc.set_tsgs(ch, actions)
      vals = ch.valuations
      assert vals.size > 0, "no channel valuations defined, check channel and template"
      aa = $spc.tsgs(vals[0]).collect {|a| a.name}.sort
      assert_equal actions.sort, aa, "error setting trouble shooting guides"
      ch.set_state "Offline"
    end
    $log.info "#{actions.inspect} set correctly"
    #
    submit_process_dcr(@@ppd_qa, :wafer_values=>@@wafer_values2, :chambers=>@@chambers, :processing_areas=>@@chambers.keys)
    # send dcr with some ooc values
    wafer_values = Hash[*@@wafer_values2.collect {|w,v| [w,v+(@@wafer_values2.keys.index(w).even? ? 100 : 0)]}.flatten]
    cnt_ooc = 0
    wafer_values.each {|w,v| cnt_ooc += 1 if (wafer_values.keys.index(w).even?) }
    cnt_ooc *= @@parameters.size
    dcr = Space::DCR.new(:lot=>@@lot, :eqp=>@@eqp, :op=>@@mpd_qa, :productgroup=>"QAPG1")
    dcr.context[:Equipment][:ProcessingArea]=[]
    @@chambers.each { |c,l| dcr.add_eqp_processing_area(c, l)}
    dcr.add_parameters(@@parameters, wafer_values)
    res = $client.send_dcr(dcr, :export=>"log/#{__method__}.xml")
    assert $spctest.verify_dcr_accepted(res, @@parameters.size*wafer_values.size, :nooc=>cnt_ooc*2), "DCR not submitted successfully"
    #
    # verify wafer script parameters set    
    assert wait_for {
      test_ok = true
      wafer_values.each_key {|w|
        res = $sv.user_parameter("Wafer", w, :name=>"WfrGrade")
        if (wafer_values.keys.index(w).even?)
          $log.info "#{w} - #{wafer_values[w]}: #{res.value}"
          if wafer_values[w] < 140
            test_ok &= (res.valueflag && res.value=="Good")
          elsif wafer_values[w] < 150
            test_ok &= (res.valueflag && res.value=="Medium")
          elsif wafer_values[w] >= 150
            test_ok &= (res.valueflag && res.value=="Bad")
          end
        else
          test_ok &= (!res.valueflag)
        end
      }
      test_ok
    }, "  wafer script parameters not set as expected"
    
    #
    # verify external comments
    assert wait_for {
      $spctest.verify_ecomment(channels, "SET_WAFER_SCRIPT_PARAMETER={WfrGrade")
    }, "  external comments not correct"
  end
  
  def test2202_manual_ecap_remeasure
    channels = create_clean_channels(actions: ["AutoLotHold", "ReleaseLotHold"])
    channels = @@parameters.collect{|p| folder.spc_channel(:parameter=>p)}
    #
    submit_process_dcr(@@ppd_qa, :wafer_values=>@@wafer_values2, :chambers=>@@chambers, :processing_areas=>@@chambers.keys)
    # send dcr with some ooc values
    wafer_values = Hash[*@@wafer_values2.collect {|w,v| [w,v+(@@wafer_values2.keys.index(w).even? ? 100 : 0)]}.flatten]
    cnt_ooc = 0
    wafer_values.each {|w,v| cnt_ooc += 1 if (wafer_values.keys.index(w).even?) }
    cnt_ooc *= @@parameters.size
    dcr = Space::DCR.new(:lot=>@@lot, :eqp=>@@eqp, :op=>@@mpd_qa, :productgroup=>"QAPG1")
    dcr.context[:Equipment][:ProcessingArea]=[]
    @@chambers.each { |c,l| dcr.add_eqp_processing_area(c, l)}
    dcr.add_parameters(@@parameters, wafer_values)
    res = $client.send_dcr(dcr, :export=>"log/#{__method__}.xml")
    assert $spctest.verify_dcr_accepted(res, @@parameters.size*wafer_values.size, :nooc=>cnt_ooc*2), "DCR not submitted successfully"
    assert $sv.lot_gatepass(@@lot), "failed to complete process"
    $log.info "Waiting for eCAP to run..."
    gets
    $log.info "Resend metro data for last wafer..."
    dcr = Space::DCR.new(:lot=>@@lot, :eqp=>@@eqp, :op=>@@mpd_qa, :productgroup=>"QAPG1")
    dcr.context[:Equipment][:ProcessingArea]=[]
    @@chambers.each { |c,l| dcr.add_eqp_processing_area(c, l)}
    dcr.add_parameters(@@parameters, wafer_values)
    res = $client.send_dcr(dcr, :export=>"log/#{__method__}.xml")
    assert $spctest.verify_dcr_accepted(res, @@parameters.size*wafer_values.size, :nooc=>cnt_ooc*2), "DCR not submitted successfully"
  end
end
