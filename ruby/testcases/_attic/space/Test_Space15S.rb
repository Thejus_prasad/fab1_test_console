=begin 
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.
    
    
Author: Steffen Steidten, 2011-11-25
=end

require_relative 'Test_Space_Setup'


# Test Space/SIL Generic Keys - FIXED, DC, GROUP with Setup.FC
# http://myteamsmst/sites/MST/IPMO/prj/space/Dev_Documentation/GenericKeysSetupUseGuide.xlsx
class Test_Space15S < Test_Space_Setup
  @@mpd = 'QA-MB-FTDEPM.01'
  @@mtool = 'QAMeas'
  
  # keys map to (Dat)Reserve1..8, must be confugured in DCR#sample_generickeys, else nil is returned and the test bypassed!
  @@ekey_keys = (1..8).collect {|i| "Reserve#{i}"}
  @@dkey_keys = (1..8).collect {|i| "DatReserve#{i}"}

  @@keys_fixed = {
    'QAGKFX1'=>['equipment','port','PToolChamber','PMachineRecipe','department','dcparmorpath','dcparmorpathwithoutversion','dcpname'],
    'QAGKFX2'=>['sapdcpid','sapordernumber','saporderoperation','sapinspectioncharacteristicid','machinerecipe','lotid','customer','SubLotType'],
    'QAGKFX3'=>['process','productgroup','product','controljob','operationpasscount','logicalrecipe','operationnumber','operationname'],
    # is not verfied, no test case yet
    #'QAGKFX4'=>['facility','foup','subprocessingarea','processjob','waferid','slot','operator','contextid'],
    #'QAGKFX5'=>['runid','batch','buffer','moduleprocess','manufacturinglayer','Reticle','timestampstring','component'],
    #'QAGKFX6'=>['mainprocess','technology','processingarea','rmsrecipe','photolayer','maskedlotid','PE10State'],
  }
  
  @@keys_fixed_specid = {'QAGKFXS'=>['SpecID','SpecIDandRev']}

  @@keys_dc = {
    'QAGKDC1'=>@@keys_fixed['QAGKFX1'],
    #'QAGKDC2'=>@@keys_fixed['QAGKFX2'],
    #'QAGKDC3'=>@@keys_fixed['QAGKFX3'],
    #'QAGKDC4'=>@@keys_fixed['QAGKFX4'],
    #'QAGKDC5'=>@@keys_fixed['QAGKFX5'],
    #'QAGKDC6'=>@@keys_fixed['QAGKFX6'],
  }
  
  # keys that are not filled by SIL with DC
  @@keys_dc_empty = ['PMachineRecipe','sapdcpid','sapordernumber','saporderoperation','sapinspectioncharacteristicid']

  @@keys_group = {
    'QAGKGP1'=>['SIL_LOT', 'SIL_WAFER', 'F-P-FTEOS8K15.0x', 'UTCSIL0x', 'QA-MB-xTDEP.0x', 'QA-MB-FTDEPM.0x', 'QAProducts', '-'],
    'QAGKGP2'=>['UTC_Chamber', 'CHx', 'S2', 'xxNM', 'SIL_ROUTE', 'SIL_PRODUCT', 'QA-MB-xTDEP.0x', 'DummyLayer']
  }
  
  @@route_udata = 'C02-1238.01'        # F8: 'FF-OBANPK.01'   route with UDATA
  @@product_route_udata = '4098AB.A1'  # F8: 'OBANPK.01'      product with a route with UDATA

  def setup
    super
    return unless $lds && self.class.class_variable_defined?(:@@lot) # bypass test0000_setup
    #
    [$lds, $lds_setup].each {|lds| lds.folders('*QA*').each {|folder| assert folder.delete_channels(silent: true)} }
  end


  def test15000_setup
    $setup_ok = false
    #
    assert_equal 0, $sv.eqp_cleanup(@@eqp)
    assert_equal 0, $sv.lot_cleanup(@@lot)
    # get process PDs referenced by MPD
    @@ppds = $sildb.pd_reference(mpd: @@mpd)
    assert_equal 9, @@ppds.size, "wrong PD references found for #{@@mpd}: #{@@ppds}"
    $log.info "using process PDs #{@@ppds.inspect}"
    #
    $setup_ok = true
  end

  def test15800
    _send_scenario_dc('QAGKDC1')
  end
  
  # method FIXED

  def test15811_fixed  # replaces 15000..09, 10 is obsolete
    $setup_ok = false
    @@keys_fixed.keys.each {|department|
      setup
      _send_scenario_fixed(department)
    }
    $setup_ok = true
  end
  
  def test15812_fixed_specid_pcp
    $setup_ok = false
    @@keys_fixed_specid.keys.each {|department|
      setup
      _send_scenario_fixed_specid_pcp(department)
    }
    $setup_ok = true
  end
  
  def test15813_fixed_specid_ecp
    $setup_ok = false
    @@keys_fixed_specid.keys.each {|department|
      setup
      _send_scenario_fixed_specid_ecp(department)
    }
    $setup_ok = true
  end

  # method DC

  def test15821_dc_inline  # replaces 15020..25
    @@keys_dc.keys.each {|department|
      setup
      _send_scenario_dc(department)
    }
  end

  def test15821_dc_test  # replaces 15020..25
    @@keys_dc.keys.each {|department|
      setup
      _send_scenario_dc(department, technology: 'TEST')
    }
  end

  def test15822_dc_chambers  # replaces 15026
    _send_scenario_dc('QAGKDC1', :chambers=>{'PM1'=>5, 'PM2'=>6, 'PM3'=>7, 'PM4'=>8, 'PM5'=>9, 'PM6'=>10})
  end

  def test15823_dc_test  # replaces 15027, 15030 is obsolete
    _send_scenario_dc('QAGKDC1', technology: 'TEST')
  end

  def test15824_dc_boatslot  # replaces 15050
    department = 'QAGKDCB'
    # build and submit DCRs for 9 process PDs
    wvs = []
    dcrps = @@ppds.each_with_index.collect {|ppd, i|
      dcrp = Space::DCR.new(eqp: "#{@@eqp}_#{i}", lot: @@lot, op: ppd, department: department)
      wv = Hash[@@wafer_values.keys.zip(["#{i}01", "#{i}02", "#{i}03", "#{i}04", "#{i}05", "#{i}06"])]
      wvs << wv
      dcrp.add_parameter("D-BoatSlot#{i}", wv, nocharting: true)
      dcrp.add_parameters(@@parameters, @@wafer_values, nocharting: true)
    }
    assert $spctest.send_dcrs_verify(dcrps), 'error submitting DCRs'
    # 
    # build and submit DCR for related MPD
    dcr = Space::DCR.new(eqp: @@mtool, lot: @@lot, op: @@mpd, department: department)
    wv = Hash[@@wafer_values.keys.zip(['901', '902', '903', '904', '905', '906'])]
    wvs << wv
    dcr.add_parameter('D-BoatSlotM', wv, nocharting: true)    
    dcr.add_parameters(@@parameters, @@wafer_values)
    res = $client.send_dcr(dcr, export: :caller)
    assert $spctest.verify_dcr_accepted(res, @@parameters.size * @@wafer_values.size), 'error submitting DCR'
    #
    # verify sample generic keys
    folder = $lds.folder("AutoCreated_#{department}")
    @@parameters.each {|p|
      ch = folder.spc_channel(parameter: p)
      samples = ch.samples
      assert_equal @@wafer_values.size, samples.size, "wrong number of samples in #{ch.inspect}"
      samples.each {|s|
        w = s.extractor_keys['Wafer']
        ekeys = {'Reserve4'=>wvs[0][w], 'Reserve5'=>wvs[1][w], 'Reserve6'=>wvs[2][w], 'Reserve7'=>wvs[3][w], 'Reserve8'=>wvs[9][w]}
        assert_equal 5, ekeys.values.compact.size, "wrong or missing wafer data"
        assert $spctest.verify_data(s.extractor_keys, ekeys)
        dkeys = {'DatReserve4'=>wvs[0][w], 'DatReserve5'=>wvs[1][w], 'DatReserve6'=>wvs[2][w], 'DatReserve7'=>wvs[3][w], 'DatReserve8'=>wvs[9][w]}
        assert_equal 5, dkeys.values.compact.size, "wrong or missing wafer data"
        assert $spctest.verify_data(s.data_keys, dkeys)
      }
    }
  end
  
  def test15825_dc_params_wafer_history  # replaces 15070
    department = 'QAGKDCP1'
    2.times {|n|
      $log.info "-- loop #{n}"
      folder = $lds.folder("AutoCreated_#{department}")
      assert folder.delete_channels if folder
      # build and submit DCRs for 5 process PDs
      wvs = []
      @@ppds.each_with_index {|ppd, i|
        dcrp = Space::DCR.new(eqp: "#{@@eqp}_#{i}", lot: @@lot, op: ppd, department: department)
        wv = Hash[@@wafer_values.keys.zip(["GK#{n}_#{i}01", "GK#{n}_#{i}02", "GK#{n}_#{i}03", 
          "GK#{n}_#{i}04", "GK#{n}_#{i}05", "GK#{n}_#{i}06", ])]
        dcparam = "Q-PARAM_GKDC_#{i}"    
        $log.info "using process values for #{dcparam} #{wv.inspect}"  
        dcrp.add_parameter(dcparam, wv, nocharting: true, value_type: 'string')
        wvs << wv
      
        wafer_values_new = Hash[@@wafer_values.collect {|w, v| [w, "#{i}#{v}"]}]
        dcrp.add_parameters(@@parameters, wafer_values_new, nocharting: true, value_type: 'unsigned integer')
        res = $client.send_dcr(dcrp, export: "log/#{__method__}_p_1_#{i}.xml")
        assert $spctest.verify_dcr_accepted(res, 0), "DCR #{i} not submitted successfully"
      }
      # 
      # build and submit DCR for related MPD
      dcr = Space::DCR.new(eqp: @@mtool, lot: @@lot, op: @@mpd, department: department)
      wafer_values_meas = Hash[@@wafer_values.collect {|w, v| [w, "9#{v}"]}]
      $log.info "using measurement values #{wafer_values_meas.inspect}"
      dcr.add_parameters(@@parameters, wafer_values_meas, value_type: 'unsigned integer')
      res = $client.send_dcr(dcr, export: "log/#{__method__}_1_m.xml")
      assert $spctest.verify_dcr_accepted(res, @@parameters.size * @@wafer_values.size), 'DCR not submitted successfully'
      #
      # verify sample generic keys
      assert folder = $lds.folder("AutoCreated_#{department}")
      @@parameters.each {|p|
        ch = folder.spc_channel(parameter: p)
        samples = ch.samples
        assert_equal @@wafer_values.size, samples.size, "wrong number of samples in #{ch.inspect}"
        samples.each {|s|
          w = s.extractor_keys['Wafer']        
          ekeys = {'Reserve4'=>wvs[0][w], 'Reserve5'=>wvs[1][w], 'Reserve6'=>wvs[2][w], 'Reserve7'=>wvs[3][w], 'Reserve8'=>'-'}
          assert_equal 5, ekeys.values.compact.size, "wrong or missing wafer data"
          assert $spctest.verify_data(s.extractor_keys, ekeys)
          dkeys = {'DatReserve4'=>wvs[0][w], 'DatReserve5'=>wvs[1][w], 'DatReserve6'=>wvs[2][w], 'DatReserve7'=>wvs[3][w], 'DatReserve8'=>'-'}
          assert_equal 5, dkeys.values.compact.size, "wrong or missing wafer data"
          assert $spctest.verify_data(s.data_keys, dkeys)
        }
      }
    }
  end
  
  def test15826_dc_params_no_wafer_history  # replaces 15080
    department = 'QAGKDCP1'
    s = Time.now.strftime('%Y%m%d%H%M%S')
    waferids = 6.times.collect {|i| "#{s}#{i}"}
    $log.info "using wafer IDs #{waferids.inspect}"
    gk_wafer_values = Hash[waferids.zip(@@wafer_values.values)]
    #
    2.times {|n|
      $log.info "-- loop #{n}"
      folder = $lds.folder("AutoCreated_#{department}")
      assert folder.delete_channels if folder
      # build and submit DCRs for 5 process PDs
      dcrs = []
      wvs = []
      @@ppds.each_with_index {|ppd, i|
        dcrp = Space::DCR.new(eqp: "#{@@eqp}_#{i}", lot: @@lot, op: ppd, department: department)
        dcrs << dcrp
        wafer_value = Hash[waferids.zip(["GK#{n}_#{i}1_#{waferids[0]}", "GK#{n}_#{i}2_#{waferids[1]}", 
          "GK#{n}_#{i}3_#{waferids[2]}", "GK#{n}_#{i}4_#{waferids[3]}", "GK#{n}_#{i}5_#{waferids[4]}", "GK#{n}_#{i}6_#{waferids[5]}"])]
        generickey_dc_param = "Q-PARAM_GKDC_#{i}"    
        $log.info "using process values for #{generickey_dc_param} #{wafer_value.inspect}"  
        dcrp.add_parameter(generickey_dc_param, wafer_value, nocharting: true, value_type: 'string')
        wvs << wafer_value
      
        wafer_values_new = Hash[gk_wafer_values.collect {|w, v| [w, "#{i}#{v}"]}]
        dcrp.add_parameters(@@parameters, wafer_values_new, nocharting: true, value_type: 'unsigned integer')
        res = $client.send_dcr(dcrp, export: "log/#{__method__}_p_1_#{i}.xml")
        assert $spctest.verify_dcr_accepted(res, 0), "DCR #{i} not submitted successfully"
      }
      # 
      # build and submit DCR for related MPD
      dcr = Space::DCR.new(eqp: @@mtool, lot: @@lot, op: @@mpd, department: department)
      dcrs.unshift dcr
      wafer_values_meas = Hash[gk_wafer_values.collect {|w, v| [w, "6#{v}"]}]
      $log.info "using measurement values #{wafer_values_meas.inspect}"
      dcr.add_parameters(@@parameters, wafer_values_meas, value_type: 'unsigned integer')
      res = $client.send_dcr(dcr, export: "log/#{__method__}_1_m.xml")
      assert $spctest.verify_dcr_accepted(res, @@parameters.size * gk_wafer_values.size), 'DCR not submitted successfully'
      #
      # verify sample generic keys
      assert folder = $lds.folder("AutoCreated_#{department}")
      @@parameters.each {|p|
        ch = folder.spc_channel(parameter: p)
        samples = ch.samples
        assert_equal gk_wafer_values.size, samples.size, "wrong number of samples in #{ch.inspect}"
        samples.each {|s|
          w = s.extractor_keys['Wafer']        
          ekeys = {'Reserve4'=>wvs[0][w], 'Reserve5'=>wvs[1][w], 'Reserve6'=>wvs[2][w], 'Reserve7'=>wvs[3][w], 'Reserve8'=>'-'}
          assert_equal 5, ekeys.values.compact.size, "wrong or missing wafer data"
          assert $spctest.verify_data(s.extractor_keys, ekeys)
          dkeys = {'DatReserve4'=>wvs[0][w], 'DatReserve5'=>wvs[1][w], 'DatReserve6'=>wvs[2][w], 'DatReserve7'=>wvs[3][w], 'DatReserve8'=>'-'}
          assert_equal 5, dkeys.values.compact.size, "wrong or missing wafer data"
          assert $spctest.verify_data(s.data_keys, dkeys)
        }
      }
    }
  end

  # method GROUP

  def test15041_group  # replaces 15040..43
    department = 'QAGKGP1'
    _send_scenario_group(department)
  end

  def test15042_group  # replaces 15040..43
    department = 'QAGKGP2'
    _send_scenario_group(department)
  end
  
  # route UDATA

  def test15411_fixed_routeudata_empty
    route = 'SIL-0001.01'
    assert_nil $sv.user_data_route(route)['RouteProgramID'], "wrong route UDATA"
    _send_scenario_route('QAGKFXR', route, nil)
  end

  def test15412_fixed_routeudata_noroute
    route = 'SIL-XXXX.01'
    assert_nil $sv.route_info(route), "route must not exist"
    _send_scenario_route('QAGKFXR', route, nil)
  end

  def test15413_fixed_routeudata
    assert programid = $sv.user_data_route(@@route_udata)['RouteProgramID'], "wrong route UDATA"
    _send_scenario_route('QAGKFXR', @@route_udata, programid)
  end

  def testman15414_fixed_routeudata_empty_product_with_routeudata
    #product with route (C02-1116.01/FF-OBANPK.01) with udata set - failing since 4.7
    assert product_route = $sv.CS_product_list(product:  @@product_route_udata).first.route, "wrong setup"
    assert programid = $sv.user_data_route(product_route)['RouteProgramID'], "missing route UDATA"
    _send_scenario_route('QAGKFXR', 'SIL-0001.01', programid, product:  @@product_route_udata)
  end

  def testman15415_generic_keys_fixed_routeudata_empty_product_with_routeudata_erfroute
    #product with route (C02-1116.01/FF-OBANPK.01) with udata set - failing since 4.7
    assert product_route = $sv.CS_product_list(product:  @@product_route_udata).first.route, "wrong setup"
    assert programid = $sv.user_data_route(product_route)['RouteProgramID'], "missing route UDATA"
    _send_scenario_route('QAGKFXR', 'e1234-QA-SIL.01', programid, product:  @@product_route_udata)
  end

  def test15431_group_routeudata_empty
    route = 'SIL-0001.01'
    assert_nil $sv.user_data_route(route)['RouteProgramID'], "wrong route UDATA"
    _send_scenario_route('QAGKGPR', route, nil)
  end

  def test15432_group_routeudata_noroute
    route = 'SIL-XXXX.01'
    assert_nil $sv.route_info(route), "route must not exist"
    _send_scenario_route('QAGKGPR', route, nil)
  end

  def test15433_group_routeudata
    assert programid = $sv.user_data_route(@@route_udata)['RouteProgramID'], "missing route UDATA"
    _send_scenario_route('QAGKGPR', @@route_udata, "#{programid[0..1]}XX")  # 45L1 -> 45XX
  end

  
  # product UDATA

  def test1601_fixed_udata
  end
  

  # aux methods
  
  def _send_scenario_fixed(department, params={})
    # build and submit DCRs for 5 process PDs
    dcrp = create_process_dcr(ppd: @@ppds.first, eqp: @@eqp, sapjob: true, department: department)
    assert $spctest.send_dcr_verify(dcrp, exporttag: caller_locations(2, 1)[0].label), 'error submitting DCRs'
    #
    assert_equal 0, $sv.lot_cleanup(@@lot)
    #
    # build and submit DCR for related MPD
    dcr = Space::DCR.new(eqp: @@mtool, lot: @@lot, op: @@mpd, department: department)
    dcr.add_parameters(@@parameters, @@wafer_values)
    res = $client.send_dcr(dcr, export: "log/#{caller_locations(2, 1)[0].label}_m.xml")
    assert $spctest.verify_dcr_accepted(res, @@parameters.size * @@wafer_values.size), 'DCR not submitted successfully'
    # verify sample generic keys
    keys = @@keys_fixed[department]
    $log.info "verifying generic keys #{keys.inspect}"
    ekeys = dcrp.sample_generickeys(mapping: Hash[@@ekey_keys.zip(keys)])
    assert_equal keys.size, ekeys.values.compact.size, 'missing configuration in DCR#sample_generickeys'
    dkeys = dcrp.sample_generickeys(mapping: Hash[@@dkey_keys.zip(keys)])
    assert_equal keys.size, dkeys.values.compact.size, 'missing configuration in DCR#sample_generickeys'
    assert $spctest.verify_parameters(@@parameters, $lds.folder("AutoCreated_#{department}"), @@wafer_values.size, ekeys, dkeys)
  end
  
  def _send_scenario_fixed_specid_pcp(department, params={})
    parameters = ['QA_PARAM_900', 'QA_PARAM_901', 'QA_PARAM_902', 'QA_PARAM_903', 'QA_PARAM_904']
    # build and submit DCRs for 5 process PDs
    dcrp = create_process_dcr(ppd: @@ppds.first, eqp: @@eqp, sapjob: true, department: department, parameters: parameters)
    assert $spctest.send_dcr_verify(dcrp, exporttag: caller_locations(2, 1)[0].label), 'error submitting DCRs'
    # build and submit DCR for related MPD
    dcr = Space::DCR.new(eqp: @@mtool, lot: @@lot, op: @@mpd, department: department)
    dcr.add_parameters(parameters, @@wafer_values)
    res = $client.send_dcr(dcr, export: "log/#{caller_locations(2, 1)[0].label}_m.xml")
    assert $spctest.verify_dcr_accepted(res, parameters.size * @@wafer_values.size), 'DCR not submitted successfully'
    # verify sample generic keys
    keys = @@keys_fixed_specid[department]
    $log.info "verifying generic keys #{keys.inspect}"
    ekeys = dcrp.sample_generickeys(mapping: Hash[@@ekey_keys.zip(keys)])
    assert_equal keys.size, ekeys.values.compact.size, 'missing configuration in DCR#sample_generickeys'
    dkeys = dcrp.sample_generickeys(mapping: Hash[@@dkey_keys.zip(keys)])
    assert_equal keys.size, dkeys.values.compact.size, 'missing configuration in DCR#sample_generickeys'
    assert $spctest.verify_parameters(parameters, $lds.folder("AutoCreated_#{department}"), @@wafer_values.size, ekeys, dkeys)
  end
  
  def _send_scenario_fixed_specid_ecp(department, params={})
    parameters = ['QA_PARAM_900_ECP', 'QA_PARAM_901_ECP', 'QA_PARAM_902_ECP', 'QA_PARAM_903_ECP', 'QA_PARAM_904_ECP']
    # build and submit DCRs for 5 process PDs
    dcrp = create_process_dcr(ppd: @@ppds.first, eqp: @@eqp, sapjob: true, department: department, parameters: parameters)
    assert $spctest.send_dcr_verify(dcrp, exporttag: caller_locations(2, 1)[0].label), 'error submitting DCRs'
    # build and submit DCR for related MPD
    dcr = Space::DCR.new(eqp: @@mtool, lot: @@lot, op: @@mpd, department: department)
    dcr.add_parameters(parameters, @@wafer_values)
    res = $client.send_dcr(dcr, export: "log/#{caller_locations(2, 1)[0].label}_m.xml")
    assert $spctest.verify_dcr_accepted(res, parameters.size * @@wafer_values.size), 'DCR not submitted successfully'
    # verify sample generic keys
    keys = @@keys_fixed_specid[department]
    $log.info "verifying generic keys #{keys.inspect}"
    ekeys = dcrp.sample_generickeys(mapping: Hash[@@ekey_keys.zip(keys)])
    assert_equal keys.size, ekeys.values.compact.size, 'missing configuration in DCR#sample_generickeys'
    dkeys = dcrp.sample_generickeys(mapping: Hash[@@dkey_keys.zip(keys)])
    assert_equal keys.size, dkeys.values.compact.size, 'missing configuration in DCR#sample_generickeys'
    assert $spctest.verify_parameters(parameters, $lds.folder("AutoCreated_#{department}"), @@wafer_values.size, ekeys, dkeys)
  end

  def _send_scenario_dc(department, params={})
    chambers = params[:chambers] || @@chambers
    technology = params[:technology] || '32NM'
    lds = (technology == 'TEST') ? $lds_setup : $lds
    # build and submit DCRs for 5 process PDs
    $dcrps = dcrps = @@ppds.each_with_index.collect {|ppd, i|
      create_process_dcr(ppd: ppd, eqp: "#{@@eqp}_#{i}", logical_recipe: "F-P-FTEOS8K15.0#{i}", machine_recipe: "F-CVD60X-P.F-P-FTEOS8K15.0#{i}", 
        chambers: chambers, technology: technology, department: department)
    }
    assert $spctest.send_dcrs_verify(dcrps, exporttag: caller_locations(2, 1)[0].label), 'error submitting DCRs'
    #
    assert_equal 0, $sv.lot_cleanup(@@lot)
    #
    # build and submit DCR for related MPD
    dcr = Space::DCR.new(eqp: @@mtool, lot: @@lot, op: @@mpd, technology: technology, department: department)
    dcrps.unshift dcr
    dcr.add_parameters(@@parameters, @@wafer_values)
    res = $client.send_dcr(dcr, export: "log/#{caller_locations(2, 1)[0].label}_m.xml")
    assert $spctest.verify_dcr_accepted(res, @@parameters.size * @@wafer_values.size), 'DCR not submitted successfully'
    # verify sample generic keys
    keys = @@keys_dc[department]
    $log.info "verifying generic keys #{keys.inspect}"
    ekeys = $spctest.get_sample_generickeys_dc(dcrps, Hash[@@ekey_keys.zip(keys)], empty: @@keys_dc_empty)
    assert_equal keys.size, ekeys.values.compact.size, 'missing configuration in DCR#sample_generickeys'
    dkeys = $spctest.get_sample_generickeys_dc(dcrps, Hash[@@dkey_keys.zip(keys)], empty: @@keys_dc_empty)
    assert_equal keys.size, dkeys.values.compact.size, 'missing configuration in DCR#sample_generickeys'
    assert $spctest.verify_parameters(@@parameters, lds.folder("AutoCreated_#{department}"), @@wafer_values.size, ekeys, dkeys)
  end
  
  def _send_scenario_group(department, params={})
    # build and submit DCRs for 5 process PDs
    dcrps = @@ppds.each_with_index.collect {|ppd, i|
      create_process_dcr(ppd: ppd, eqp: "#{@@eqp}_#{i}", logical_recipe: "F-P-FTEOS8K15.0#{i}",
        machine_recipe: "F-CVD60X-P.F-P-FTEOS8K15.0#{i}", department: department)
    }
    assert_equal 0, $sv.lot_cleanup(@@lot)
    assert $spctest.send_dcrs_verify(dcrps, exporttag: caller_locations(1, 1)[0].label), 'error submitting DCRs'
    # build and submit DCR for related MPD
    dcr = Space::DCR.new(eqp: @@mtool, lot: @@lot, op: @@mpd, department: department)
    dcr.add_parameters(@@parameters, @@wafer_values)
    res = $client.send_dcr(dcr, export: "log/#{caller_locations(1, 1)[0].label}_m.xml")
    assert $spctest.verify_dcr_accepted(res, @@parameters.size * @@wafer_values.size), 'DCR not submitted successfully'
    # verify sample generic keys
    keys = @@keys_group[department]
    $log.info "verifying generic keys #{keys}"
    ekeys = Hash[keys.each_with_index.collect {|k, i| ["Reserve#{i+1}", k]}]
    dkeys = Hash[keys.each_with_index.collect {|k, i| ["DatReserve#{i+1}", k]}]
    assert $spctest.verify_parameters(@@parameters, $lds.folder("AutoCreated_#{department}"), @@wafer_values.size, ekeys, dkeys)
  end
  
  def _send_scenario_route(department, route, programid, params={})
    # build and submit DCRs for process PDs
    product = params[:product] || 'SILPROD.01'
    dcrps = @@ppds.each_with_index.collect {|ppd, i|
      create_process_dcr(ppd: ppd, eqp: "#{@@eqp}_#{i}", route: route, product: product, department: department)
    }
    assert $spctest.send_dcrs_verify(dcrps, exporttag: caller_locations(1, 1)[0].label), 'error submitting DCRs'
    #
    assert_equal 0, $sv.lot_cleanup(@@lot)
    #
    # build and submit DCR for related MPD
    dcr = Space::DCR.new(eqp: @@mtool, lot: @@lot, op: @@mpd, route: route, department: department)
    dcr.add_parameters(@@parameters, @@wafer_values)
    res = $client.send_dcr(dcr, export: "log/#{caller_locations(1, 1)[0].label}_m.xml")
    assert $spctest.verify_dcr_accepted(res, @@parameters.size * @@wafer_values.size), 'DCR not submitted successfully'
    # verify sample generic keys
    $log.info "verifying programid #{programid.inspect}"
    ekeys = {'Reserve4'=>programid}
    dkeys = {'DatReserve4'=>programid}
    assert $spctest.verify_parameters(@@parameters, $lds.folder("AutoCreated_#{department}"), @@wafer_values.size, ekeys, dkeys), 'wrong data'
  end
 
  def _send_scenario_product(department, product, productgroup, params={})
    # load setup file
    if gkmethod == 'fixed'
     assert $spctest.sil_upload_verify_generickey(@@files_generic_keys_fixed_product[scenario]), 'file upload failed'
    elsif gkmethod == 'group'
     fail 'not configured'
    end
    # build and submit DCRs for process PDs
    dcrps = @@ppds.each_with_index.collect {|ppd, i|
      create_process_dcr(ppd: ppd, eqp: "#{@@eqp}_#{i}", product: product, productgroup: productgroup)
    }
    assert $spctest.send_dcrs_verify(dcrps, exporttag: caller_locations(1,1)[0].label), 'error submitting DCRs'
    #
    if params[:addlot]
      wafer_values =  {"#{params[:addlot]}01"=>10, "#{params[:addlot]}02"=>11, "#{params[:addlot]}03"=>12}
      dcrps = @@ppds.each_with_index.collect {|ppd, i|
        create_process_dcr(ppd: ppd, eqp: "#{@@eqp}_#{i}", lot: params[:addlot], product: params[:addproduct], 
        productgroup: params[:addproduct_group], wafer_values: wafer_values)
      }
    end
    assert $spctest.send_dcrs_verify(dcrps, exporttag: caller_locations(1,1)[0].label), 'error submitting DCRs'
    #
    # build and submit DCR for related MPD
    dcr = Space::DCR.new(eqp: @@mtool, lot: @@lot, op: @@mpd, product: product, productgroup: productgroup)
    dcr.add_parameters(@@parameters, @@wafer_values)
    nsamples = @@parameters.size * @@wafer_values.size
    if params[:addlot]
      dcr.add_lot(params[:addlot], op: @@mpd, eqp: @@mtool, product: params[:addproduct], productgroup: params[:addproduct_group])
      wafer_values = {"#{params[:addlot]}01"=>40, "#{params[:addlot]}02"=>41, "#{params[:addlot]}03"=>42}
      dcr.add_parameters(@@parameters, wafer_values)
      nsamples += @@parameters.size * wafer_values.size
    end
    res = $client.send_dcr(dcr, export: "log/#{caller_locations(1,1)[0].label}_m.xml")
    assert $spctest.verify_dcr_accepted(res, nsamples), 'DCR not submitted successfully'
    
    product ||= $sv.lot_info(@@lot).product
    productgroup ||= $sv.lot_info(@@lot).productgroup
    udatas = $sv.user_data(:product, product).merge($sv.user_data(:productgroup, productgroup))
    $log.info "#{product}, #{productgroup}, #{udatas.inspect}"
    #
    # verify sample generic keys
    keysetup_mapping = Hash.new
    $spctest.generickey_from_file(filename: @@files_generic_keys_fixed_product[scenario]).each {|line| 
      keysetup_mapping[line.name.gsub('GenericExKey','Reserve').gsub('GenericDataKey','DatReserve').next.next.next] = 
        line.fixed_property.gsub('ProductID-', '').gsub('ProductGroupID-', '')
    }
    datreserve = Hash[keysetup_mapping.collect {|k,v| [k,udatas[v]] if k.start_with?('Dat')}.compact]
    reserve = Hash[keysetup_mapping.collect {|k,v| [k,udatas[v]] if k.start_with?('Dat') == false}.compact]
    folder = $lds.folder(@@autocreated_qa)
    if params[:addlot]
      @@parameters.each {|p|
        ch = folder.spc_channel(parameter: p)
        samples = ch.samples
        samples.each {|s|
          lot = s.extractor_keys['Lot']
          # Update the udatas
          if params[:addlot] == lot
            udatas = $sv.user_data(:product, params[:addproduct]).merge($sv.user_data(:productgroup, params[:addproduct_group]))
            datreserve2 = Hash[keysetup_mapping.collect {|k,v| [k,udatas[v]] if k.start_with?('Dat')}.compact]
            reserve2 = Hash[keysetup_mapping.collect {|k,v| [k,udatas[v]] if k.start_with?('Dat') == false}.compact]
            assert $spctest.verify_data(s.extractor_keys, reserve2)
            assert $spctest.verify_data(s.data_keys, datreserve2)
          else
            assert $spctest.verify_data(s.extractor_keys, reserve)
            assert $spctest.verify_data(s.data_keys, datreserve)
          end          
        }
      }
    else   
      assert $spctest.verify_parameters(@@parameters, folder, @@wafer_values.size, reserve, datreserve), 'wrong data in key'
    end
  end

end
