=begin 
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.
    
Author: Steffen Steidten, 2011-11-25
        Daniel Steger, 2012-07-05
=end

require_relative 'Test_Space_Setup'

# Test Space/SIL.

class Test_Space_FabView < Test_Space_Setup
	@@parameters = ["FV_PARAM_900", "FV_PARAM_901", "FV_PARAM_902", "FV_PARAM_903", "FV_PARAM_904"]
  
  # Required effective specs: M14-00020002
  def test0001_submit_dcr
    $setup_ok = false
	lot = determine_lot
    $log.info "* * * * * * * * * * * * * * * * * * * * * * * * *"
    $log.info "* using lot #{lot}"
    $log.info "* * * * * * * * * * * * * * * * * * * * * * * * *"
    assert_equal $sv.lot_cleanup(lot)		
    #
    # ensure module template exist
    assert $spctest.templates_exist($lds.folder(@@templates_folder), [@@params_template]), "missing template"
    folder = $lds.folder("AutoCreated_FabView")
    assert folder.delete_channels, "error deleting channels in #{folder.name}" if folder
    #
    # build and submit DCR
    dcr = Space::DCR.new(:eqp=>@@eqp, :lot=>lot, :department=>"FabView")
    dcr.add_parameters(@@parameters, @@wafer_values)
    res = $client.send_dcr(dcr, :export=>"log/#{__method__}.xml")
    assert $spctest.verify_dcr_accepted(res, @@parameters.size*@@wafer_values.size), "DCR not submitted successfully"
    #
    $setup_ok = true
  end
  
  def determine_lot
    lot = "FV" + @@sil_version.split("\.").collect{|e| "%02d" % e}.join("_") + "_%02d" % @@test_run + ".00"
    $sv.new_lot_release :lot=>lot, :carrier=>"FVW%", :template=>"SIL006.00", :waferids=>"t7m12", :stb=>true if $sv.lot_info(lot).nil?
    return lot
  end
end
