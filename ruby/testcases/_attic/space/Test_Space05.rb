=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: Steffen Steidten, 2011-11-25
=end

require_relative 'Test_Space_Setup'


class Test_Space05 < Test_Space_Setup

  def setup
    super
    return unless $lds && self.class.class_variable_defined?(:@@lot) # bypass test0000_setup
    assert_equal 0, $sv.eqp_cleanup(@@eqp)
    assert_equal 0, $sv.lot_cleanup(@@lot)
    #
    [$lds_setup, $lds].each {|lds|
      lds.all_spc_channels('QA_PARAM_90*').each {|ch| assert ch.delete}
      folder = lds.folder(@@autocreated_qa)
      assert folder.delete_channels, "error cleaning channels" if folder
    }
  end


  def test0500_multi_lot
    $setup_ok = false
    assert_equal 0, $sv.eqp_cleanup(@@eqp)
    assert_equal 0, $sv.lot_cleanup(@@lot)
    #
    lot2 = 'SIL' + @@sil_version.split('.').collect {|e| '%02d' % e}.join('') + '%02d' % @@test_run + '1' + '.000'
    assert lot2 = $svtest.new_lot(lot: lot2) unless $sv.lot_exists?(lot2)
    $log.info "using lot #{lot2.inspect} as second lot"
    assert_equal 0, $sv.lot_cleanup(lot2)
    # build and submit DCR
    dcr = Space::DCR.new(eqp: @@eqp, lot: @@lot)
    dcr.add_parameters(@@parameters, @@wafer_values, space_listening: true)
    dcr.add_lot(lot2)
    wafer_values2 = Hash[@@wafer_values.collect {|w, v| ["#{w}2", v]}]
    dcr.add_parameters(@@parameters, wafer_values2, space_listening: true)
    assert $spctest.send_dcr_verify(dcr, nsamples: @@parameters.size*@@wafer_values.size*2), "DCR not submitted successfully"
    #
    # verify spc channels
    folder = $lds.folder(@@autocreated_qa)
    @@parameters.each {|p|
      ch = folder.spc_channel(parameter: p)
      ref_ch = Space::SpcApi::ChannelData.new(nil, nil, "Auto-created using template: #{@@params_template}", p)
      assert $spctest.verify_channel(ch, ref_ch, dcr, @@wafer_values, ilot: 0), "wrong channel data"
      assert $spctest.verify_channel(ch, ref_ch, dcr, wafer_values2, ilot: 1), "wrong channel data"
    }
    #
    $setup_ok = true
  end

  def test0501_multi_lot_TEST
    # ensure Setup templates exist
    assert $spctest.templates_exist($lds_setup.folder(@@templates_folder), [@@default_template, @@default_template_qa, @@params_template]), "missing template"
    assert_equal 0, $sv.eqp_cleanup(@@eqp)
    assert_equal 0, $sv.lot_cleanup(@@lot)
    #
    lot2 = 'SIL' + @@sil_version.split('.').collect {|e| '%02d' % e}.join('') + '%02d' % @@test_run + '1' + '.000'
    assert lot2 = $svtest.new_lot(lot: lot2) unless $sv.lot_exists?(lot2)
    $log.info "using lot #{lot2.inspect} as second lot"
    assert_equal 0, $sv.lot_cleanup(lot2)
    # build and submit DCR
    dcr = Space::DCR.new(eqp: @@eqp, lot: @@lot, technology: 'TEST')
    dcr.add_parameters(@@parameters, @@wafer_values, space_listening: true)
    dcr.add_lot(lot2, technology: 'TEST')
    wafer_values2 = Hash[@@wafer_values.collect {|w, v| ["#{w}2", v]}]
    dcr.add_parameters(@@parameters, wafer_values2, space_listening: true)
    assert $spctest.send_dcr_verify(dcr, nsamples: @@parameters.size*@@wafer_values.size*2), "DCR not submitted successfully"
    #
    # verify spc channels
    folder = $lds_setup.folder(@@autocreated_qa)
    @@parameters.each {|p|
      ch = folder.spc_channel(parameter: p)
      ref_ch = Space::SpcApi::ChannelData.new(nil, nil, "Auto-created using template: #{@@params_template}", p)
      assert $spctest.verify_channel(ch, ref_ch, dcr, @@wafer_values, ilot: 0), "wrong channel data"
      assert $spctest.verify_channel(ch, ref_ch, dcr, wafer_values2, ilot: 1), "wrong channel data"
    }
    #
    # verify there are no spc channels in Inline DS
    folder = $lds.folder(@@autocreated_qa)
    @@parameters.each {|p|
      chs = folder.spc_channels(parameter: p)
      assert_equal 0, chs.size, "no channel must be created for parameter #{p}"
    }
  end

  def test0502_multi_lot_notech
    # clean up nonwip channels
    [$lds_setup, $lds].each {|lds|
      folder = $lds_setup.folder(@@autocreated_nonwip)
      folder.delete_channels if folder
    }
    assert_equal 0, $sv.eqp_cleanup(@@eqp)
    assert_equal 0, $sv.lot_cleanup(@@lot)
    #
    lot2 = 'SIL' + @@sil_version.split('.').collect {|e| '%02d' % e}.join('') + '%02d' % @@test_run + '1' + '.000'
    assert lot2 = $svtest.new_lot(lot: lot2) unless $sv.lot_exists?(lot2)
    $log.info "using lot #{lot2.inspect} as second lot"
    assert_equal 0, $sv.lot_cleanup(lot2)
    # build and submit DCR
    dcr = Space::DCR.new(eqp: @@eqp, lot: @@lot, technology: nil)
    dcr.add_parameters(@@parameters, @@wafer_values, space_listening: true)
    dcr.add_lot(lot2, technology: nil)
    wafer_values2 = Hash[@@wafer_values.collect {|w, v| ["#{w}2", v]}]
    dcr.add_parameters(@@parameters, wafer_values2, space_listening: true)
    assert $spctest.send_dcr_verify(dcr, nsamples: @@parameters.size*@@wafer_values.size*2), "DCR not submitted successfully"
    #
    # verify spc channels
    folder = $lds_setup.folder(@@autocreated_qa)
    @@parameters.each {|p|
      ch = folder.spc_channel(parameter: p)
      ref_ch = Space::SpcApi::ChannelData.new(nil, nil, "Auto-created using template: #{@@params_template}", p)
      assert $spctest.verify_channel(ch, ref_ch, dcr, @@wafer_values, ilot: 0), "wrong channel data"
      assert $spctest.verify_channel(ch, ref_ch, dcr, wafer_values2, ilot: 1), "wrong channel data"
    }
    #
    # verify there are no spc channels in Inline DS
    folder = $lds.folder(@@autocreated_qa)
    @@parameters.each {|p|
      chs = folder.spc_channels(parameter: p)
      assert_equal 0, chs.size, "no channel must be created for parameter #{p}"
    }
  end

  def test0503_multi_lot_tech_empty
    # build and submit DCR
    assert_equal 0, $sv.eqp_cleanup(@@eqp)
    assert_equal 0, $sv.lot_cleanup(@@lot)
    #
    lot2 = 'SIL' + @@sil_version.split('.').collect {|e| '%02d' % e}.join('') + '%02d' % @@test_run + '1' + '.000'
    assert lot2 = $svtest.new_lot(lot: lot2) unless $sv.lot_exists?(lot2)
    $log.info "using lot #{lot2.inspect} as second lot"
    assert_equal 0, $sv.lot_cleanup(lot2)
    #
    dcr = Space::DCR.new(eqp: @@eqp, lot: @@lot, technology: '')
    dcr.add_parameters(@@parameters, @@wafer_values, space_listening: true)
    dcr.add_lot(lot2, technology: '')
    wafer_values2 = Hash[@@wafer_values.collect {|w, v| ["#{w}2", v]}]
    dcr.add_parameters(@@parameters, wafer_values2, space_listening: true)
    assert $spctest.send_dcr_verify(dcr, nsamples: @@parameters.size*@@wafer_values.size*2), "DCR not submitted successfully"
    #
    # verify spc channels
    sleep 10
    folder = $lds.folder(@@autocreated_qa)
    @@parameters.each {|p|
      ch = folder.spc_channels(parameter: p).first
      ref_ch = Space::SpcApi::ChannelData.new(nil, nil, nil, p)
      assert $spctest.verify_channel(ch, ref_ch, dcr, @@wafer_values, ilot: 0), "wrong channel data"
      assert $spctest.verify_channel(ch, ref_ch, dcr, wafer_values2, ilot: 1), "wrong channel data"
    }
  end

  def test0504_multi_lot_invalid_parameter_3
    # build and submit DCR
    assert_equal 0, $sv.eqp_cleanup(@@eqp)
    assert_equal 0, $sv.lot_cleanup(@@lot)
    dcr = Space::DCR.new(eqp: @@eqp, lot: @@lot)
    wafer_values2 = Hash[@@wafer_values.collect {|w, v| ["#{w}2", v]}]
    wafer_values3 = Hash[@@wafer_values.collect {|w, v| ["#{w}3", v]}]
    # Parameter invalid (some wafers, lot1)
    dcr.add_parameter(@@parameters[1], @@wafer_values, reading_valid: true, parameter_valid: false)

    lot2 = 'SIL' + @@sil_version.split('.').collect {|e| '%02d' % e}.join('') + '%02d' % @@test_run + '1' + '.000'
    assert lot2 = $svtest.new_lot(lot: lot2) unless $sv.lot_exists?(lot2)
    $log.info "using lot #{lot2.inspect} as second lot"
    assert_equal 0, $sv.lot_cleanup(lot2)
    #
    dcr.add_lot(lot2)
    # Parameter valid (other wafers, lot2)
    dcr.add_parameter(@@parameters[1], wafer_values2, reading_valid: true, parameter_valid: true, separate_lot: true)

    lot3 = 'SIL' + @@sil_version.split('.').collect {|e| '%02d' % e}.join('') + '%02d' % @@test_run + '2' + '.000'
    assert lot3 = $svtest.new_lot(lot: lot3) unless $sv.lot_exists?(lot3)
    $log.info "using lot #{lot3.inspect} as second lot"
    assert_equal 0, $sv.lot_cleanup(lot3)
    #
    dcr.add_lot(lot3)
    # Parameter invalid (other wafers, lot3)
    dcr.add_parameter(@@parameters[1], wafer_values3, reading_valid: true, parameter_valid: false, separate_lot: true)

    # send DCR
    assert $spctest.send_dcr_verify(dcr, nsamples: wafer_values2.size), "DCR not submitted successfully"
    #
    # verify spc channels
    folder = $lds.folder(@@autocreated_qa)
    # No new data for param 0 and 1 for lot, only for second lot
    assert $spctest.verify_parameters(@@parameters[1], folder, wafer_values2.size, {'Lot'=>lot2})
    # Check parameter channel with new data
    ch = folder.spc_channel(parameter: @@parameters[1])
    ref_ch = Space::SpcApi::ChannelData.new(nil, nil, "Auto-created using template: #{@@params_template}", @@parameters[1])
    # passes as of 4.7.3
    assert $spctest.verify_channel(ch, ref_ch, dcr, wafer_values2, nsamples: 1, last_sample_only: true, ilot: 1),
      "wrong channel data, check filter.parameter=Y and filter.measurement=Y"
  end

  def test0505_multi_lot_invalid_parameter_lot
    # build and submit DCR
    assert_equal 0, $sv.eqp_cleanup(@@eqp)
    assert_equal 0, $sv.lot_cleanup(@@lot)
    dcr = Space::DCR.new(eqp: @@eqp, lot: @@lot)
    # Parameter invalid (some wafers, lot1)
    dcr.add_parameter(@@parameters[1], {@@lot=>21.0}, reading_type: 'Lot', reading_valid: true, parameter_valid: false)

    lot2 = 'SIL' + @@sil_version.split('.').collect {|e| '%02d' % e}.join('') + '%02d' % @@test_run + '1' + '.000'
    assert lot2 = $svtest.new_lot(lot: lot2) unless $sv.lot_exists?(lot2)
    $log.info "using lot #{lot2.inspect} as second lot"
    assert_equal 0, $sv.lot_cleanup(lot2)
    #
    dcr.add_lot(lot2)
    # Parameter valid (other wafers, lot2)
    dcr.add_parameter(@@parameters[1], {lot2=>22.0}, reading_type: 'Lot', reading_valid: true, parameter_valid: true, separate_lot: true)

    lot3 = 'SIL' + @@sil_version.split('.').collect {|e| '%02d' % e}.join('') + '%02d' % @@test_run + '2' + '.000'
    assert lot3 = $svtest.new_lot(lot: lot3) unless $sv.lot_exists?(lot3)
    $log.info "using lot #{lot3.inspect} as third lot"
    assert_equal 0, $sv.lot_cleanup(lot3)

    dcr.add_lot(lot3)
    # Parameter invalid (other wafers, lot3)
    dcr.add_parameter(@@parameters[1], {lot3=>23.0}, reading_type: 'Lot', reading_valid: true, parameter_valid: false, separate_lot: true)


    # send DCR
    assert $spctest.send_dcr_verify(dcr, nsamples: 1), "DCR not submitted successfully"
    #
    # verify spc channels
    folder = $lds.folder(@@autocreated_qa)
    # No new data for param 0 and 1 for lot, only for second lot
    assert $spctest.verify_parameters(@@parameters[1], folder, 1, {'Lot'=>lot2})
    # Check parameter channel with new data
    ch = folder.spc_channel(parameter: @@parameters[1])
    ref_ch = Space::SpcApi::ChannelData.new(nil, nil, "Auto-created using template: #{@@params_template}", @@parameters[1])
    # passes as of 4.7.3
    assert $spctest.verify_channel(ch, ref_ch, dcr, nil, nsamples: 1),
      "wrong channel data, check filter.parameter=Y and filter.measurement=Y"
  end

  def test0506_multi_lot_invalid_parameter_3_testlds
    # build and submit DCR
    assert_equal 0, $sv.eqp_cleanup(@@eqp)
    assert_equal 0, $sv.lot_cleanup(@@lot)
    dcr = Space::DCR.new(eqp: @@eqp, lot: @@lot, technology: 'TEST')
    # Parameter invalid (some wafers, lot1)
    values = @@wafer_values.values
    wafer_values = {}
    lot_wafers = $sv.lot_info(@@lot,:wafers=>true).wafers.take(@@wafer_values.count)
    lot_wafers.each_with_index {|w,j| wafer_values[w.wafer] = values[j]}
    #
    dcr.add_parameter(@@parameters[1], wafer_values, reading_valid: true, parameter_valid: false)

    # find or create a second test lot
    lotcount = '2'
    lot2 = 'SIL' + @@sil_version.split('.').collect {|e| '%02d' % e}.join('') + '%02d' % @@test_run + '%02d' % lotcount + '.000'
    assert lot2 = $svtest.new_lot(lot: lot2) unless $sv.lot_exists?(lot2)
    assert_equal 0, $sv.lot_cleanup(lot2)
    dcr.add_lot(lot2, technology: 'TEST')
    # Parameter valid (other wafers, lot2)
    values = @@wafer_values.values
    wafer_values2 = {}
    lot_wafers = $sv.lot_info(lot2,:wafers=>true).wafers.take(@@wafer_values.count)
    lot_wafers.each_with_index {|w,j| wafer_values2[w.wafer] = values[j]}
    #
    dcr.add_parameter(@@parameters[1], wafer_values2, reading_valid: true, parameter_valid: true, separate_lot: true)

    # find or create a third test lot
    lotcount = '3'
    lot3 = 'SIL' + @@sil_version.split('.').collect {|e| '%02d' % e}.join('') + '%02d' % @@test_run + '%02d' % lotcount + '.000'
    assert lot3 = $svtest.new_lot(lot: lot3) unless $sv.lot_exists?(lot3)
    assert_equal 0, $sv.lot_cleanup(lot3)
    dcr.add_lot(lot3, technology: 'TEST')
    # Parameter invalid (other wafers, lot3)
    values = @@wafer_values.values
    wafer_values3 = {}
    lot_wafers = $sv.lot_info(lot3,:wafers=>true).wafers.take(@@wafer_values.count)
    lot_wafers.each_with_index {|w,j| wafer_values3[w.wafer] = values[j]}
    #
    dcr.add_parameter(@@parameters[1], wafer_values3, reading_valid: true, parameter_valid: false, separate_lot: true)

    # send DCR
    assert $spctest.send_dcr_verify(dcr, nsamples: wafer_values2.size), "DCR not submitted successfully"
    #
    # verify spc channels
    folder = $lds_setup.folder(@@autocreated_qa)
    # No new data for param 0 and 1 for lot, only for second lot
    assert $spctest.verify_parameters(@@parameters[1], folder, wafer_values2.size, {'Lot'=>lot2})
    # Check parameter channel with new data
    ch = folder.spc_channel(parameter: @@parameters[1])
    ref_ch = Space::SpcApi::ChannelData.new(nil, nil, "Auto-created using template: #{@@params_template}", @@parameters[1])
    # passes as of 4.7.3
    assert $spctest.verify_channel(ch, ref_ch, dcr, wafer_values2, nsamples: 1, last_sample_only: true, ilot: 1),
      "wrong channel data, check filter.parameter=Y and filter.measurement=Y"
    # get AQV messasges and verify
    sleep 120
    assert msgs = $spctest.mqaqv.read_msgs
    assert $spctest.verify_aqv_message(msgs: msgs, dcr: dcr, lot: @@lot, wafer: wafer_values.keys.sort, result: 'fail',
      comment: 'MISSING_DCR_PARAMETERS', exporttag: "#{__method__}_#{@@lot}"), 'wrong AQV message (fail)'
    assert $spctest.verify_aqv_message(msgs: msgs, dcr: dcr, lot: lot2, wafer: wafer_values2.keys.sort, result: 'fail',
      comment: 'MISSING_DCR_PARAMETERS', exporttag: "#{__method__}_#{lot2}"), 'wrong AQV message (fail)'
    assert $spctest.verify_aqv_message(msgs: msgs, dcr: dcr, lot: lot3, wafer: wafer_values3.keys.sort, result: 'fail',
      comment: 'MISSING_DCR_PARAMETERS', exporttag: "#{__method__}_#{lot3}"), 'wrong AQV message (fail)'
  end

  def test0507_multi_lot_invalid_parameter_lot_testlds
    assert_equal 0, $sv.lot_cleanup(@@lot)
    # build and submit DCR
    dcr = Space::DCR.new(eqp: @@eqp, lot: @@lot, technology: 'TEST')
    # Parameter invalid (some wafers, lot1)
    dcr.add_parameter(@@parameters[1], {@@lot=>21.0}, reading_type: 'Lot', reading_valid: true, parameter_valid: false)

    # find or create a second test lot
    lotcount = '2'
    lot2 = 'SIL' + @@sil_version.split('.').collect {|e| '%02d' % e}.join('') + '%02d' % @@test_run + '%02d' % lotcount + '.000'
    assert lot2 = $svtest.new_lot(lot: lot2) unless $sv.lot_exists?(lot2)
    assert_equal 0, $sv.lot_cleanup(lot2)
    dcr.add_lot(lot2, technology: 'TEST')
    # Parameter valid (other wafers, lot2)
    dcr.add_parameter(@@parameters[1], {lot2=>22.0}, reading_type: 'Lot', reading_valid: true, parameter_valid: true, separate_lot: true)

    # find or create a third test lot
    lotcount = '3'
    lot3 = 'SIL' + @@sil_version.split('.').collect {|e| '%02d' % e}.join('') + '%02d' % @@test_run + '%02d' % lotcount + '.000'
    assert lot3 = $svtest.new_lot(lot: lot3) unless $sv.lot_exists?(lot3)
    assert_equal 0, $sv.lot_cleanup(lot3)
    dcr.add_lot(lot3, technology: 'TEST')
    # Parameter invalid (other wafers, lot3)
    dcr.add_parameter(@@parameters[1], {lot3=>23.0}, reading_type: 'Lot', reading_valid: true, parameter_valid: false, separate_lot: true)

    # send DCR
    assert $spctest.send_dcr_verify(dcr, nsamples: 1), "DCR not submitted successfully"
    #
    # verify spc channels
    folder = $lds_setup.folder(@@autocreated_qa)
    # No new data for param 0 and 1 for lot, only for second lot
    assert $spctest.verify_parameters(@@parameters[1], folder, 1, {'Lot'=>lot2})
    # Check parameter channel with new data
    ch = folder.spc_channel(parameter: @@parameters[1])
    ref_ch = Space::SpcApi::ChannelData.new(nil, nil, "Auto-created using template: #{@@params_template}", @@parameters[1])
    # passes as of 4.7.3
    assert $spctest.verify_channel(ch, ref_ch, dcr, nil, nsamples: 1),
      "wrong channel data, check filter.parameter=Y and filter.measurement=Y"
    # get AQV messasges and verify
    sleep 120
    assert msgs = $spctest.mqaqv.read_msgs
    assert $spctest.verify_aqv_message(msgs: msgs, dcr: dcr, lot: @@lot, result: 'fail',
      comment: 'MISSING_DCR_PARAMETERS', exporttag: "#{__method__}_#{@@lot}"), 'wrong AQV message (fail)'
    assert $spctest.verify_aqv_message(msgs: msgs, dcr: dcr, lot: lot2, result: 'fail',
      comment: 'MISSING_DCR_PARAMETERS', exporttag: "#{__method__}_#{lot2}"), 'wrong AQV message (fail)'
    assert $spctest.verify_aqv_message(msgs: msgs, dcr: dcr, lot: lot3, result: 'fail',
      comment: 'MISSING_DCR_PARAMETERS', exporttag: "#{__method__}_#{lot3}"), 'wrong AQV message (fail)'
  end

  def test0508a_multi_lot_invalid_parameters_3_testlds
    # build and submit DCR
    assert_equal 0, $sv.eqp_cleanup(@@eqp)
    assert_equal 0, $sv.lot_cleanup(@@lot)
    dcr = Space::DCR.new(eqp: @@eqp, lot: @@lot, technology: 'TEST')
    # Parameter invalid (some wafers, lot1)
    values = @@wafer_values.values
    wafer_values = {}
    lot_wafers = $sv.lot_info(@@lot,:wafers=>true).wafers.take(@@wafer_values.count)
    lot_wafers.each_with_index {|w,j| wafer_values[w.wafer] = values[j]}
    #
    dcr.add_parameter(@@parameters[0], wafer_values, reading_valid: true, parameter_valid: false)
    dcr.add_parameter(@@parameters[1], wafer_values, reading_valid: true, parameter_valid: false)
    dcr.add_parameter(@@parameters[2], wafer_values, reading_valid: true, parameter_valid: false)
    dcr.add_parameter(@@parameters[3], wafer_values, reading_valid: true, parameter_valid: false)
    dcr.add_parameter(@@parameters[4], wafer_values, reading_valid: true, parameter_valid: false)

    # find or create a second test lot
    lotcount = '2'
    lot2 = 'SIL' + @@sil_version.split('.').collect {|e| '%02d' % e}.join('') + '%02d' % @@test_run + '%02d' % lotcount + '.000'
    assert lot2 = $svtest.new_lot(lot: lot2) unless $sv.lot_exists?(lot2)
    assert_equal 0, $sv.lot_cleanup(lot2)
    dcr.add_lot(lot2, technology: 'TEST')
    # Parameter valid (other wafers, lot2)
    values = @@wafer_values.values
    wafer_values2 = {}
    lot_wafers = $sv.lot_info(lot2,:wafers=>true).wafers.take(@@wafer_values.count)
    lot_wafers.each_with_index {|w,j| wafer_values2[w.wafer] = values[j]}
    #
    dcr.add_parameter(@@parameters[0], wafer_values2, reading_valid: true, parameter_valid: true, separate_lot: true)
    dcr.add_parameter(@@parameters[1], wafer_values2, reading_valid: true, parameter_valid: true, separate_lot: true)
    dcr.add_parameter(@@parameters[2], wafer_values2, reading_valid: true, parameter_valid: true, separate_lot: true)
    dcr.add_parameter(@@parameters[3], wafer_values2, reading_valid: true, parameter_valid: true, separate_lot: true)
    dcr.add_parameter(@@parameters[4], wafer_values2, reading_valid: true, parameter_valid: true, separate_lot: true)

    # find or create a third test lot
    lotcount = '3'
    lot3 = 'SIL' + @@sil_version.split('.').collect {|e| '%02d' % e}.join('') + '%02d' % @@test_run + '%02d' % lotcount + '.000'
    assert lot3 = $svtest.new_lot(lot: lot3) unless $sv.lot_exists?(lot3)
    assert_equal 0, $sv.lot_cleanup(lot3)
    dcr.add_lot(lot3, technology: 'TEST')
    # Parameter invalid (other wafers, lot3)
    values = @@wafer_values.values
    wafer_values3 = {}
    lot_wafers = $sv.lot_info(lot3,:wafers=>true).wafers.take(@@wafer_values.count)
    lot_wafers.each_with_index {|w,j| wafer_values3[w.wafer] = values[j]}
    #
    dcr.add_parameter(@@parameters[0], wafer_values3, reading_valid: false, parameter_valid: true, separate_lot: true)
    dcr.add_parameter(@@parameters[1], wafer_values3, reading_valid: false, parameter_valid: true, separate_lot: true)
    dcr.add_parameter(@@parameters[2], wafer_values3, reading_valid: false, parameter_valid: true, separate_lot: true)
    dcr.add_parameter(@@parameters[3], wafer_values3, reading_valid: false, parameter_valid: true, separate_lot: true)
    dcr.add_parameter(@@parameters[4], wafer_values3, reading_valid: false, parameter_valid: true, separate_lot: true)

    # send DCR
    assert $spctest.send_dcr_verify(dcr, nsamples: wafer_values2.size * 5), "DCR not submitted successfully"
    #
    # verify spc channels
    folder = $lds_setup.folder(@@autocreated_qa)
    # No new data for param 0 and 1 for lot, only for second lot
    assert $spctest.verify_parameters(@@parameters[1], folder, wafer_values2.size, {'Lot'=>lot2})
    # Check parameter channel with new data
    ch = folder.spc_channel(parameter: @@parameters[1])
    ref_ch = Space::SpcApi::ChannelData.new(nil, nil, "Auto-created using template: #{@@params_template}", @@parameters[1])
    # passes as of 4.7.3
    assert $spctest.verify_channel(ch, ref_ch, dcr, wafer_values2, nsamples: 1, last_sample_only: true, ilot: 1),
      "wrong channel data, check filter.parameter=Y and filter.measurement=Y"
    #AQV check
    assert $spctest.verify_aqv_message(dcr: dcr, lot: @@lot, wafer: wafer_values.keys.sort, result: 'fail',
      comment: 'Error code : MISSING_DCR_PARAMETERS', exporttag: __method__),'AQV message not as expected(fail)'
    assert $spctest.verify_aqv_message(dcr: dcr, lot: lot2, wafer: wafer_values2.keys.sort, result: 'fail',
      comment: 'Error code : MISSING_DCR_PARAMETERS', exporttag: __method__),'AQV message not as expected(fail)'
    assert $spctest.verify_aqv_message(dcr: dcr, lot: lot3, wafer: wafer_values3.keys.sort, result: 'fail',
      comment: 'Error code : MISSING_DCR_PARAMETERS', exporttag: __method__),'AQV message not as expected(fail)'
  end

  def test0508b_multi_lot_invalid_parameters_3_testlds_ooc
    # build and submit DCR
    assert_equal 0, $sv.eqp_cleanup(@@eqp)
    assert_equal 0, $sv.lot_cleanup(@@lot)
    dcr = Space::DCR.new(eqp: @@eqp, lot: @@lot, technology: 'TEST')
    # Parameter invalid (some wafers, lot1)
    values = @@wafer_values.values
    wafer_values = {}
    lot_wafers = $sv.lot_info(@@lot,:wafers=>true).wafers.take(@@wafer_values.count)
    lot_wafers.each_with_index {|w,j| wafer_values[w.wafer] = values[j] + 100}
    #
    dcr.add_parameter(@@parameters[0], wafer_values, reading_valid: true, parameter_valid: false)
    dcr.add_parameter(@@parameters[1], wafer_values, reading_valid: true, parameter_valid: false)
    dcr.add_parameter(@@parameters[2], wafer_values, reading_valid: true, parameter_valid: false)
    dcr.add_parameter(@@parameters[3], wafer_values, reading_valid: true, parameter_valid: false)
    dcr.add_parameter(@@parameters[4], wafer_values, reading_valid: true, parameter_valid: false)

    # find or create a second test lot
    lotcount = '2'
    lot2 = 'SIL' + @@sil_version.split('.').collect {|e| '%02d' % e}.join('') + '%02d' % @@test_run + '%02d' % lotcount + '.000'
    assert lot2 = $svtest.new_lot(lot: lot2) unless $sv.lot_exists?(lot2)
    assert_equal 0, $sv.lot_cleanup(lot2)
    dcr.add_lot(lot2, technology: 'TEST')
    # Parameter valid (other wafers, lot2)
    values = @@wafer_values.values
    wafer_values2 = {}
    lot_wafers = $sv.lot_info(lot2,:wafers=>true).wafers.take(@@wafer_values.count)
    lot_wafers.each_with_index {|w,j| wafer_values2[w.wafer] = values[j] + 100}
    #
    dcr.add_parameter(@@parameters[0], wafer_values2, reading_valid: true, parameter_valid: true, separate_lot: true)
    dcr.add_parameter(@@parameters[1], wafer_values2, reading_valid: true, parameter_valid: true, separate_lot: true)
    dcr.add_parameter(@@parameters[2], wafer_values2, reading_valid: true, parameter_valid: true, separate_lot: true)
    dcr.add_parameter(@@parameters[3], wafer_values2, reading_valid: true, parameter_valid: true, separate_lot: true)
    dcr.add_parameter(@@parameters[4], wafer_values2, reading_valid: true, parameter_valid: true, separate_lot: true)

    # find or create a third test lot
    lotcount = '3'
    lot3 = 'SIL' + @@sil_version.split('.').collect {|e| '%02d' % e}.join('') + '%02d' % @@test_run + '%02d' % lotcount + '.000'
    assert lot3 = $svtest.new_lot(lot: lot3) unless $sv.lot_exists?(lot3)
    assert_equal 0, $sv.lot_cleanup(lot3)
    dcr.add_lot(lot3, technology: 'TEST')
    # Parameter invalid (other wafers, lot3)
    values = @@wafer_values.values
    wafer_values3 = {}
    lot_wafers = $sv.lot_info(lot3,:wafers=>true).wafers.take(@@wafer_values.count)
    lot_wafers.each_with_index {|w,j| wafer_values3[w.wafer] = values[j] + 100}
    #
    dcr.add_parameter(@@parameters[0], wafer_values3, reading_valid: false, parameter_valid: true, separate_lot: true)
    dcr.add_parameter(@@parameters[1], wafer_values3, reading_valid: false, parameter_valid: true, separate_lot: true)
    dcr.add_parameter(@@parameters[2], wafer_values3, reading_valid: false, parameter_valid: true, separate_lot: true)
    dcr.add_parameter(@@parameters[3], wafer_values3, reading_valid: false, parameter_valid: true, separate_lot: true)
    dcr.add_parameter(@@parameters[4], wafer_values3, reading_valid: false, parameter_valid: true, separate_lot: true)

    # send DCR
    assert $spctest.send_dcr_verify(dcr, nsamples: wafer_values2.size * 5), "DCR not submitted successfully"
    #
    # verify spc channels
    folder = $lds_setup.folder(@@autocreated_qa)
    # No new data for param 0 and 1 for lot, only for second lot
    assert $spctest.verify_parameters(@@parameters[1], folder, wafer_values2.size, {'Lot'=>lot2})
    # Check parameter channel with new data
    ch = folder.spc_channel(parameter: @@parameters[1])
    ref_ch = Space::SpcApi::ChannelData.new(nil, nil, "Auto-created using template: #{@@params_template}", @@parameters[1])
    # passes as of 4.7.3
    assert $spctest.verify_channel(ch, ref_ch, dcr, wafer_values2, nsamples: 1, last_sample_only: true, ilot: 1),
      "wrong channel data, check filter.parameter=Y and filter.measurement=Y"
    #AQV check
    assert $spctest.verify_aqv_message(dcr: dcr, lot: @@lot, wafer: wafer_values.keys.sort, result: 'fail',
      comment: 'Error code : MISSING_DCR_PARAMETERS', exporttag: __method__),'AQV message not as expected(fail)'
    assert $spctest.verify_aqv_message(dcr: dcr, lot: lot2, wafer: wafer_values2.keys.sort, result: 'fail',
      comment: 'Found 60 OOS/OOC violation(s).', exporttag: __method__),'AQV message not as expected(fail)'
    assert $spctest.verify_aqv_message(dcr: dcr, lot: lot3, wafer: wafer_values3.keys.sort, result: 'fail',
      comment: 'Error code : MISSING_DCR_PARAMETERS', exporttag: __method__),'AQV message not as expected(fail)'
  end

  def test0508c_multi_lot_invalid_parameters_3_testlds
    parameters = ['QA_PARAM_900_TG05', 'QA_PARAM_901_TG05', 'QA_PARAM_902_TG05', 'QA_PARAM_903_TG05', 'QA_PARAM_904_TG05']
    # build and submit DCR
    assert_equal 0, $sv.eqp_cleanup(@@eqp)
    assert_equal 0, $sv.lot_cleanup(@@lot)
    dcr = Space::DCR.new(eqp: @@eqp, lot: @@lot, technology: 'TEST', op: 'QA-MultiLot-TG05.01')
    # Parameter invalid (some wafers, lot1)
    values = @@wafer_values.values
    wafer_values = {}
    lot_wafers = $sv.lot_info(@@lot,:wafers=>true).wafers.take(@@wafer_values.count)
    lot_wafers.each_with_index {|w,j| wafer_values[w.wafer] = values[j]}
    #
    dcr.add_parameter(parameters[0], wafer_values, reading_valid: true, parameter_valid: false)
    dcr.add_parameter(parameters[1], wafer_values, reading_valid: true, parameter_valid: false)
    dcr.add_parameter(parameters[2], wafer_values, reading_valid: true, parameter_valid: false)
    dcr.add_parameter(parameters[3], wafer_values, reading_valid: true, parameter_valid: false)
    dcr.add_parameter(parameters[4], wafer_values, reading_valid: true, parameter_valid: false)

    # find or create a second test lot
    lotcount = '2'
    lot2 = 'SIL' + @@sil_version.split('.').collect {|e| '%02d' % e}.join('') + '%02d' % @@test_run + '%02d' % lotcount + '.000'
    assert lot2 = $svtest.new_lot(lot: lot2) unless $sv.lot_exists?(lot2)
    assert_equal 0, $sv.lot_cleanup(lot2)
    dcr.add_lot(lot2, technology: 'TEST', op: 'QA-MultiLot-TG05.01')
    # Parameter valid (other wafers, lot2)
    values = @@wafer_values.values
    wafer_values2 = {}
    lot_wafers = $sv.lot_info(lot2,:wafers=>true).wafers.take(@@wafer_values.count)
    lot_wafers.each_with_index {|w,j| wafer_values2[w.wafer] = values[j]}
    #
    dcr.add_parameter(parameters[0], wafer_values2, reading_valid: true, parameter_valid: true, separate_lot: true)
    dcr.add_parameter(parameters[1], wafer_values2, reading_valid: true, parameter_valid: true, separate_lot: true)
    dcr.add_parameter(parameters[2], wafer_values2, reading_valid: true, parameter_valid: true, separate_lot: true)
    dcr.add_parameter(parameters[3], wafer_values2, reading_valid: true, parameter_valid: true, separate_lot: true)
    dcr.add_parameter(parameters[4], wafer_values2, reading_valid: true, parameter_valid: true, separate_lot: true)

    # find or create a third test lot
    lotcount = '3'
    lot3 = 'SIL' + @@sil_version.split('.').collect {|e| '%02d' % e}.join('') + '%02d' % @@test_run + '%02d' % lotcount + '.000'
    assert lot3 = $svtest.new_lot(lot: lot3) unless $sv.lot_exists?(lot3)
    assert_equal 0, $sv.lot_cleanup(lot3)
    dcr.add_lot(lot3, technology: 'TEST', op: 'QA-MultiLot-TG05.01')
    # Parameter invalid (other wafers, lot3)
    values = @@wafer_values.values
    wafer_values3 = {}
    lot_wafers = $sv.lot_info(lot3,:wafers=>true).wafers.take(@@wafer_values.count)
    lot_wafers.each_with_index {|w,j| wafer_values3[w.wafer] = values[j]}
    #
    dcr.add_parameter(parameters[0], wafer_values3, reading_valid: false, parameter_valid: true, separate_lot: true)
    dcr.add_parameter(parameters[1], wafer_values3, reading_valid: false, parameter_valid: true, separate_lot: true)
    dcr.add_parameter(parameters[2], wafer_values3, reading_valid: false, parameter_valid: true, separate_lot: true)
    dcr.add_parameter(parameters[3], wafer_values3, reading_valid: false, parameter_valid: true, separate_lot: true)
    dcr.add_parameter(parameters[4], wafer_values3, reading_valid: false, parameter_valid: true, separate_lot: true)

    # send DCR
    assert $spctest.send_dcr_verify(dcr, nsamples: wafer_values2.size * 5), "DCR not submitted successfully"
    #
    # verify spc channels
    folder = $lds_setup.folder(@@autocreated_qa)
    # No new data for param 0 and 1 for lot, only for second lot
    assert $spctest.verify_parameters(parameters[1], folder, wafer_values2.size, {'Lot'=>lot2})
    # Check parameter channel with new data
    ch = folder.spc_channel(parameter: parameters[1])
    ref_ch = Space::SpcApi::ChannelData.new(nil, nil, "Auto-created using template: #{@@params_template}", parameters[1])
    # passes as of 4.7.3
    assert $spctest.verify_channel(ch, ref_ch, dcr, wafer_values2, nsamples: 1, last_sample_only: true, ilot: 1),
      "wrong channel data, check filter.parameter=Y and filter.measurement=Y"
    #AQV check
    assert $spctest.verify_aqv_message(dcr: dcr, lot: @@lot, wafer: wafer_values.keys.sort, result: 'fail',
      comment: 'Error code : MISSING_DCR_PARAMETERS', exporttag: __method__),'AQV message not as expected(fail)'
    assert $spctest.verify_aqv_message(dcr: dcr, lot: lot2, wafer: wafer_values2.keys.sort, result: 'success', exporttag: __method__),
      'AQV message not as expected(success)'
    assert $spctest.verify_aqv_message(dcr: dcr, lot: lot3, wafer: wafer_values3.keys.sort, result: 'fail',
      comment: 'Error code : MISSING_DCR_PARAMETERS', exporttag: __method__),'AQV message not as expected(fail)'
  end

  def test0508d_multi_lot_invalid_parameters_3_testlds_ooc
    parameters = ['QA_PARAM_900_TG05', 'QA_PARAM_901_TG05', 'QA_PARAM_902_TG05', 'QA_PARAM_903_TG05', 'QA_PARAM_904_TG05']
    # build and submit DCR
    assert_equal 0, $sv.eqp_cleanup(@@eqp)
    assert_equal 0, $sv.lot_cleanup(@@lot)
    dcr = Space::DCR.new(eqp: @@eqp, lot: @@lot, technology: 'TEST', op: 'QA-MultiLot-TG05.01')
    # Parameter invalid (some wafers, lot1)
    values = @@wafer_values.values
    wafer_values = {}
    lot_wafers = $sv.lot_info(@@lot,:wafers=>true).wafers.take(@@wafer_values.count)
    lot_wafers.each_with_index {|w,j| wafer_values[w.wafer] = values[j] + 100}
    #
    dcr.add_parameter(parameters[0], wafer_values, reading_valid: true, parameter_valid: false)
    dcr.add_parameter(parameters[1], wafer_values, reading_valid: true, parameter_valid: false)
    dcr.add_parameter(parameters[2], wafer_values, reading_valid: true, parameter_valid: false)
    dcr.add_parameter(parameters[3], wafer_values, reading_valid: true, parameter_valid: false)
    dcr.add_parameter(parameters[4], wafer_values, reading_valid: true, parameter_valid: false)

    # find or create a second test lot
    lotcount = '2'
    lot2 = 'SIL' + @@sil_version.split('.').collect {|e| '%02d' % e}.join('') + '%02d' % @@test_run + '%02d' % lotcount + '.000'
    assert lot2 = $svtest.new_lot(lot: lot2) unless $sv.lot_exists?(lot2)
    assert_equal 0, $sv.lot_cleanup(lot2)
    dcr.add_lot(lot2, technology: 'TEST', op: 'QA-MultiLot-TG05.01')
    # Parameter valid (other wafers, lot2)
    values = @@wafer_values.values
    wafer_values2 = {}
    lot_wafers = $sv.lot_info(lot2,:wafers=>true).wafers.take(@@wafer_values.count)
    lot_wafers.each_with_index {|w,j| wafer_values2[w.wafer] = values[j] + 100}
    #
    dcr.add_parameter(parameters[0], wafer_values2, reading_valid: true, parameter_valid: true, separate_lot: true)
    dcr.add_parameter(parameters[1], wafer_values2, reading_valid: true, parameter_valid: true, separate_lot: true)
    dcr.add_parameter(parameters[2], wafer_values2, reading_valid: true, parameter_valid: true, separate_lot: true)
    dcr.add_parameter(parameters[3], wafer_values2, reading_valid: true, parameter_valid: true, separate_lot: true)
    dcr.add_parameter(parameters[4], wafer_values2, reading_valid: true, parameter_valid: true, separate_lot: true)

    # find or create a third test lot
    lotcount = '3'
    lot3 = 'SIL' + @@sil_version.split('.').collect {|e| '%02d' % e}.join('') + '%02d' % @@test_run + '%02d' % lotcount + '.000'
    assert lot3 = $svtest.new_lot(lot: lot3) unless $sv.lot_exists?(lot3)
    assert_equal 0, $sv.lot_cleanup(lot3)
    dcr.add_lot(lot3, technology: 'TEST', op: 'QA-MultiLot-TG05.01')
    # Parameter invalid (other wafers, lot3)
    values = @@wafer_values.values
    wafer_values3 = {}
    lot_wafers = $sv.lot_info(lot3,:wafers=>true).wafers.take(@@wafer_values.count)
    lot_wafers.each_with_index {|w,j| wafer_values3[w.wafer] = values[j] + 100}
    #
    dcr.add_parameter(parameters[0], wafer_values3, reading_valid: false, parameter_valid: true, separate_lot: true)
    dcr.add_parameter(parameters[1], wafer_values3, reading_valid: false, parameter_valid: true, separate_lot: true)
    dcr.add_parameter(parameters[2], wafer_values3, reading_valid: false, parameter_valid: true, separate_lot: true)
    dcr.add_parameter(parameters[3], wafer_values3, reading_valid: false, parameter_valid: true, separate_lot: true)
    dcr.add_parameter(parameters[4], wafer_values3, reading_valid: false, parameter_valid: true, separate_lot: true)

    # send DCR
    assert $spctest.send_dcr_verify(dcr, nsamples: wafer_values2.size * 5), "DCR not submitted successfully"
    #
    # verify spc channels
    folder = $lds_setup.folder(@@autocreated_qa)
    # No new data for param 0 and 1 for lot, only for second lot
    assert $spctest.verify_parameters(parameters[1], folder, wafer_values2.size, {'Lot'=>lot2})
    # Check parameter channel with new data
    ch = folder.spc_channel(parameter: parameters[1])
    ref_ch = Space::SpcApi::ChannelData.new(nil, nil, "Auto-created using template: #{@@params_template}", parameters[1])
    # passes as of 4.7.3
    assert $spctest.verify_channel(ch, ref_ch, dcr, wafer_values2, nsamples: 1, last_sample_only: true, ilot: 1),
      "wrong channel data, check filter.parameter=Y and filter.measurement=Y"
    #AQV check
    assert $spctest.verify_aqv_message(dcr: dcr, lot: @@lot, wafer: wafer_values.keys.sort, result: 'fail',
      comment: 'Error code : MISSING_DCR_PARAMETERS', exporttag: __method__),'AQV message not as expected(fail)'
    assert $spctest.verify_aqv_message(dcr: dcr, lot: lot2, wafer: wafer_values2.keys.sort, result: 'fail',
      comment: 'Found 60 OOS/OOC violation(s).', exporttag: __method__),'AQV message not as expected(fail)'
    assert $spctest.verify_aqv_message(dcr: dcr, lot: lot3, wafer: wafer_values3.keys.sort, result: 'fail',
      comment: 'Error code : MISSING_DCR_PARAMETERS', exporttag: __method__),'AQV message not as expected(fail)'
  end

  def test0508e_multi_lot_valid_string_parameters_3_testlds
    parameters = ['QA_PARAM_900_TG05', 'QA_PARAM_901_TG05', 'QA_PARAM_902_TG05', 'QA_PARAM_903_TG05', 'QA_PARAM_904_TG05']
    # build and submit DCR
    assert_equal 0, $sv.eqp_cleanup(@@eqp)
    assert_equal 0, $sv.lot_cleanup(@@lot)
    dcr = Space::DCR.new(eqp: @@eqp, lot: @@lot, technology: 'TEST', op: 'QA-MultiLot-TG05.01')
    # Parameter invalid (some wafers, lot1)
    values = @@wafer_values.values
    wafer_values = {}
    lot_wafers = $sv.lot_info(@@lot,:wafers=>true).wafers.take(@@wafer_values.count)
    lot_wafers.each_with_index {|w,j| wafer_values[w.wafer] = values[j]}
    #
    dcr.add_parameter(parameters[0], wafer_values, reading_valid: true, parameter_valid: true, value_type: 'string')
    dcr.add_parameter(parameters[1], wafer_values, reading_valid: true, parameter_valid: true, value_type: 'string')
    dcr.add_parameter(parameters[2], wafer_values, reading_valid: true, parameter_valid: true, value_type: 'string')
    dcr.add_parameter(parameters[3], wafer_values, reading_valid: true, parameter_valid: true, value_type: 'string')
    dcr.add_parameter(parameters[4], wafer_values, reading_valid: true, parameter_valid: true, value_type: 'string')

    # find or create a second test lot
    lotcount = '2'
    lot2 = 'SIL' + @@sil_version.split('.').collect {|e| '%02d' % e}.join('') + '%02d' % @@test_run + '%02d' % lotcount + '.000'
    assert lot2 = $svtest.new_lot(lot: lot2) unless $sv.lot_exists?(lot2)
    assert_equal 0, $sv.lot_cleanup(lot2)
    dcr.add_lot(lot2, technology: 'TEST', op: 'QA-MultiLot-TG05.01')
    # Parameter valid (other wafers, lot2)
    values = @@wafer_values.values
    wafer_values2 = {}
    lot_wafers = $sv.lot_info(lot2,:wafers=>true).wafers.take(@@wafer_values.count)
    lot_wafers.each_with_index {|w,j| wafer_values2[w.wafer] = values[j]}
    #
    dcr.add_parameter(parameters[0], wafer_values2, reading_valid: true, parameter_valid: true, separate_lot: true)
    dcr.add_parameter(parameters[1], wafer_values2, reading_valid: true, parameter_valid: true, separate_lot: true)
    dcr.add_parameter(parameters[2], wafer_values2, reading_valid: true, parameter_valid: true, separate_lot: true)
    dcr.add_parameter(parameters[3], wafer_values2, reading_valid: true, parameter_valid: true, separate_lot: true)
    dcr.add_parameter(parameters[4], wafer_values2, reading_valid: true, parameter_valid: true, separate_lot: true)

    # find or create a third test lot
    lotcount = '3'
    lot3 = 'SIL' + @@sil_version.split('.').collect {|e| '%02d' % e}.join('') + '%02d' % @@test_run + '%02d' % lotcount + '.000'
    assert lot3 = $svtest.new_lot(lot: lot3) unless $sv.lot_exists?(lot3)
    assert_equal 0, $sv.lot_cleanup(lot3)
    dcr.add_lot(lot3, technology: 'TEST', op: 'QA-MultiLot-TG05.01')
    # Parameter invalid (other wafers, lot3)
    values = @@wafer_values.values
    wafer_values3 = {}
    lot_wafers = $sv.lot_info(lot3,:wafers=>true).wafers.take(@@wafer_values.count)
    lot_wafers.each_with_index {|w,j| wafer_values3[w.wafer] = values[j]}
    #
    dcr.add_parameter(parameters[0], wafer_values3, reading_valid: true, parameter_valid: true, separate_lot: true, value_type: 'string')
    dcr.add_parameter(parameters[1], wafer_values3, reading_valid: true, parameter_valid: true, separate_lot: true, value_type: 'string')
    dcr.add_parameter(parameters[2], wafer_values3, reading_valid: true, parameter_valid: true, separate_lot: true, value_type: 'string')
    dcr.add_parameter(parameters[3], wafer_values3, reading_valid: true, parameter_valid: true, separate_lot: true, value_type: 'string')
    dcr.add_parameter(parameters[4], wafer_values3, reading_valid: true, parameter_valid: true, separate_lot: true, value_type: 'string')

    # send DCR
    assert $spctest.send_dcr_verify(dcr, nsamples: 30), "DCR not submitted successfully"
    #
    # verify spc channels
    folder = $lds_setup.folder(@@autocreated_qa)
    # No new data for param 0 and 1 for lot, only for second lot
    # assert $spctest.verify_parameters(parameters[1], folder, wafer_values2.size, {'Lot'=>lot2})
    # Check parameter channel with new data
    # ch = folder.spc_channel(parameter: parameters[1])
    # ref_ch = Space::SpcApi::ChannelData.new(nil, nil, "Auto-created using template: #{@@params_template}", parameters[1])
    # passes as of 4.7.3
    # assert $spctest.verify_channel(ch, ref_ch, dcr, wafer_values2, nsamples: 1, last_sample_only: true, ilot: 1),
    #   "wrong channel data, check filter.parameter=Y and filter.measurement=Y"
    #AQV check
    assert $spctest.verify_aqv_message(dcr: dcr, lot: @@lot, wafer: wafer_values.keys.sort, result: 'fail',
      comment: 'Error code : MISSING_DCR_PARAMETERS', exporttag: __method__),'AQV message not as expected(fail)'
    assert $spctest.verify_aqv_message(dcr: dcr, lot: lot2, wafer: wafer_values2.keys.sort, result: 'success', exporttag: __method__),
      'AQV message not as expected(success)'
    assert $spctest.verify_aqv_message(dcr: dcr, lot: lot3, wafer: wafer_values3.keys.sort, result: 'fail',
      comment: 'Error code : MISSING_DCR_PARAMETERS', exporttag: __method__),'AQV message not as expected(fail)'
  end

  def test0509_no_lot
    skip "not applicable in #{$env}" if $env == "let"
    # clean up nonwip channels
    assert_equal 0, $sv.eqp_cleanup(@@eqp)
    assert_equal 0, $sv.lot_cleanup(@@lot)
    folder = $lds_setup.folder(@@autocreated_nonwip)
    folder.delete_channels
    eqp = "QAEqpNonWip"
    value = 99
    #
    # build and submit DCR
    dcr = Space::DCR.new(eqp: eqp, lot: @@lot, cj: nil)
    dcr.add_parameters(@@parameters, {nil=>value}, space_listening: true)
    # this has never worked correctly:
    pnonwip = {"QA-NonWip-Test-01"=>value+10, "QA-NonWip-Test-02"=>value+20,
      "QA-NonWip-Test-03"=>value+30, "QA-NonWip-Test-04"=>value+40, "QA-NonWip-Test-05"=>value+50}
    dcr.add_parameters(pnonwip, reading_type: 'Wafer', space_listening: true, value_type: 'string')
    #
    assert $spctest.send_dcr_verify(dcr, nsamples: @@parameters.size), "DCR not submitted successfully"
    #
    # verify spc channels
    folder = $lds_setup.folder(@@autocreated_nonwip)
    @@parameters.each {|p|
      ch = folder.spc_channels(parameter: p).first
      samples = ch.samples
      assert_equal 1, samples.size, "wrong number of samples in channel #{ch.inspect}"
      assert_equal eqp, samples[0].extractor_keys['PTool'], "wrong PTool in channel #{ch.inspect}"
    }
  end

end
