=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: U. Koerner, 2012-01-11
=end

require_relative 'Test_Space_Setup'

# Test Space/SIL.
class Test_Space77 < Test_Space_Setup
  #@@mpd_qa = "QA-MB-FTDEPM-LIS-CAEX.01"
  @@cafailure_lothold_fallback = true
  

  def teardown
    eqp = "LETCL1001"
    assert_equal 0, $sv.lot_cleanup(@@lot)
    assert_equal 0, $sv.eqp_cleanup(eqp)
  end


  def test7700_setup
    $setup_ok = false
    
    if $env == "f8stag"
      @@cafailure_lothold_fallback = false
      @@wafer_values2 = {"WWWWSIL00700"=>40, "WWWWSIL00701"=>41, "WWWWSIL00702"=>42, "WWWWSIL00703"=>43, "WWWWSIL00704"=>44, "WWWWSIL00705"=>45}
    end
    @@ppds_qa = $sildb.pd_reference(mpd: @@mpd_qa)[0..4]
    assert_equal @@ppds_qa.size, @@ppds_qa.compact.size, "no PD reference found for #{@@mpd_qa}"
    
    @@ppd_qa = $sildb.pd_reference(:mpd=>@@mpd_qa).first
    assert_not_nil @@ppd_qa, "no PD reference found for #{@@mpd_qa}"
    assert_operator $sildb.pd_reference(:mpd=>@@mpd_lit, :all=>true).length, :>=, 1, "No PD reference found for #{@@mpd_lit}!"

    actions = ['AutoCancelPToolQueuedJobs', 'AutoCancelPToolQueuedJobs-RespPD13', 'EquipmentInhibit', 'AutoEquipmentInhibit', 'ReleaseEquipmentInhibit', 'AutoEquipmentInhibit-PD1', 'AutoEquipmentInhibit-PD3']
    actions.each {|action|
      assert $spc.corrective_action(action), "CA #{action.inspect} not defined"
    }
    #
    $setup_ok = true
  end

  def test7701_cancel_all_cjobs_and_equipment_inhibit
    eqp = "LETCL1001"
    # EQP should be Auto-2
    # Carriers to be used: APT8, APT9....
    assert_equal 0, $sv.lot_cleanup(@@lot)
    assert_equal 0, $sv.eqp_cleanup(eqp)
    channels = create_clean_channels(:eqp=>eqp)
    #
    actions = ['AutoCancelPToolQueuedJobs', 'EquipmentInhibit', 'AutoEquipmentInhibit', 'ReleaseEquipmentInhibit']
    assert $spctest.assign_verify_cas(channels, actions)
    #
    # send process DCR
    submit_process_dcr(@@ppd_qa, :eqp=>eqp, :wafer_values=>@@wafer_values2, :chambers=>@@chambers, :processing_areas=>@@chambers.keys)
    # send DCR with OOC values
    $log.info " -~-~- Please reserve JOB now!!! -~-~- "
    wafer_values = Hash[*@@wafer_values2.collect {|w,v| [w,v+100]}.flatten]
    dcr = Space::DCR.new(:lot=>@@lot, :eqp=>@@mtool, :op=>@@mpd_qa)
    dcr.add_parameters(@@parameters, wafer_values)
    res = $client.send_dcr(dcr, :export=>:caller)
    assert $spctest.verify_dcr_accepted(res, @@parameters.size*wafer_values.size, :nooc=>@@parameters.size*wafer_values.size*2), "DCR not submitted successfully"
    #
    # verify inhibit and comment
    assert $spctest.verify_inhibit(eqp, channels.size, :timeout=>@@ca_time_out), "equipment must have an inhibit"
    assert_equal channels.size, $sv.inhibit_list(:eqp, eqp).count, "equipment must have an inhibit"
    assert $spctest.verify_ecomment(channels, "TOOL_HELD: #{eqp}: reason={X-S")
    $log.info "EquipmentInhibit automatically set"
    #
    # release inhibit
    channels.each {|ch|
      s = ch.samples(:ckc=>true)[-1]
			assert s.assign_ca($spc.corrective_action("ReleaseEquipmentInhibit")), "error assigning corrective action"
    }
		assert $spctest.verify_inhibit(eqp, 0, :timeout=>@@ca_time_out), "equipment must still have all inhibits"
		$log.info "EquipmentInhibits correctly not released"
    #
    # send process DCR again
    submit_process_dcr(@@ppd_qa, :eqp=>eqp, :wafer_values=>@@wafer_values2, :chambers=>@@chambers, :processing_areas=>@@chambers.keys)
    # send DCR with OOC values
    wafer_values = Hash[*@@wafer_values2.collect {|w,v| [w,v+100]}.flatten]
    dcr = Space::DCR.new(:lot=>@@lot, :eqp=>@@mtool, :op=>@@mpd_qa)
    dcr.add_parameters(@@parameters, wafer_values)
    res = $client.send_dcr(dcr, :export=>:caller)
    assert $spctest.verify_dcr_accepted(res, @@parameters.size*wafer_values.size, :nooc=>@@parameters.size*wafer_values.size*2), "DCR not submitted successfully"
    #
    # verify inhibit and comment
    assert $spctest.verify_inhibit(eqp, channels.size, :timeout=>@@ca_time_out), "equipment must have an inhibit"
    assert_equal channels.size, $sv.inhibit_list(:eqp, eqp).count, "equipment must have an inhibit"
    assert $spctest.verify_ecomment(channels, "TOOL_HELD: #{eqp}: reason={X-S")
    #
    # release inhibits
    channels.each {|ch|
      s = ch.samples(:ckc=>true)[-1]
      assert s.assign_ca($spc.corrective_action("ReleaseEquipmentInhibit")), "error assigning corrective action"
    }
    assert $spctest.verify_inhibit(eqp, 0, :timeout=>@@ca_time_out), "equipment must not have an inhibit"
  end

  def test7702_cancel_all_cjobs_and_equipment_inhibit_responsible_pds
    eqp = 'LETCL100'
    mpd = @@mpd_qa
    ppds = @@ppds_qa
    # EQP should be Auto-2
    # Carriers to be used: APT8, APT9....
    assert_equal 0, $sv.lot_cleanup(@@lot)
    5.times {|i|
      assert_equal 0, $sv.eqp_cleanup("#{eqp}#{i}")
    }
    channels = create_clean_channels(:eqp=>"#{eqp}0")
    #
    actions = ['AutoCancelPToolQueuedJobs-RespPD13',]
    assert $spctest.assign_verify_cas(channels, actions)
    #
    # send process DCR
    5.times {|i|
      $log.info "process dcr with ppd: #{ppds[i]}, eqp: #{eqp}#{i}"
      submit_process_dcr(ppds[i], :eqp=>"#{eqp}#{i}", :wafer_values=>@@wafer_values2, :chambers=>@@chambers, :processing_areas=>@@chambers.keys)
    }
    
    # send DCR with OOC values
    $log.info " -~-~- Please reserve JOB now!!! -~-~- "
    wafer_values = Hash[*@@wafer_values2.collect {|w,v| [w,v+100]}.flatten]
    dcr = Space::DCR.new(:lot=>@@lot, :eqp=>@@mtool, :op=>mpd)
    dcr.add_parameters(@@parameters, wafer_values)
    res = $client.send_dcr(dcr, :export=>:caller)
    assert $spctest.verify_dcr_accepted(res, @@parameters.size*wafer_values.size, :nooc=>@@parameters.size*wafer_values.size*2), "DCR not submitted successfully"
  end
  
end
