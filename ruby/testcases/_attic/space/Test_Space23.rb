=begin 
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.
    
Author: Conny Stenzel, 2012-08-06
=end

require_relative 'Test_Space_Setup'


# Test Space/SIL for MSR588074 ERF AutoFlagging
class Test_Space23 < Test_Space_Setup
  @@experiment_autoflagging_enabled=true

  @@mpd = 'QA-MB-FTDEPM.01'
  @@messages = ['[(Mean above control limit)', '[(Mean below control limit)', '[(Raw above specification)', '[(Raw below specification)']
  @@route = 'SIL-0001.01'
  @@erfRoute = 'e1234-QASIL-A0.01'                #Standard Pattern: e*-*
  @@missmatchErfRoute = 'e1234_QA_SIL_A0.01'
  @@secondPatternErfRoute = 'e1234+QA+SIL+A0.01'    #Second Pattern: e*+*

  def self.shutdown
    @@lots.each {|lot| $sv.delete_lot_family(lot)}
  end

  def setup
    super
    return unless $lds && self.class.class_variable_defined?(:@@lot) # bypass test0000_setup
    assert_equal 0, $sv.eqp_cleanup(@@eqp)
    assert_equal 0, $sv.lot_cleanup(@@lot)
    #
    [$lds, $lds_setup].each {|lds|
      lds.all_spc_channels('QA_PARAM_90*').each {|ch| assert ch.delete}
      folder = lds.folder(@@autocreated_qa)
      assert folder.delete_channels, 'error cleaning channels' if folder
    }
  end

  def test2300_setup
    $setup_ok = false
    #
    assert @@experiment_autoflagging_enabled, 'autoflagging is disabled, tests do not apply'
    props = $spctest.silserver.read_jobprops
    assert_equal @@experiment_autoflagging_enabled.to_s, props[:'experiment.autoflagging.enabled'], 'wrong setup'
    @@use_sublottype = (props[:'script.parameter.process.autoflag.usesublottype'] == 'true')
    @@sublotprops = {'jumper' => props[:'sublot.autoflag.autoflaglottype.jumper'], 
      'minesweeper' => props[:'sublot.autoflag.autoflaglottype.minesweeper'], 
      'proto' => props[:'sublot.autoflag.autoflaglottype.proto']}
    @@scriptprops = {'jumper' => props[:'script.parameter.autoflaglottype.jumper'], 
      'minesweeper' => props[:'script.parameter.autoflaglottype.minesweeper'], 
      'proto' => props[:'script.parameter.autoflaglottype.proto']}
    $log.info "Config switch script.parameter.process.autoflag.usesublottype = #{@@use_sublottype}"
    $log.info "Config switches sublotprops = #{@@sublotprops}"
    $log.info "Config switches scriptprops = #{@@scriptprops}"
    #
    assert $sv.user_parameter_delete_verify('Lot', @@lot, 'Jumper'), ' failed to reset jumper flag'
    #
    assert @@ppd = $sildb.pd_reference(mpd: @@mpd).first, "no PD reference found for #{@@mpd}"
    @@lots = []
    #
    $setup_ok = true
  end

  def test2310_MSR588074_erfroute_erfpd
    empd = 'e1234-QA-MB-FTDEPM-A0.01'
    _erf_experiment_flagging(@@erfRoute, empd, @@ppd, experiment: @@experiment_autoflagging_enabled, exp_pd: true, ooc: true)
  end

  def test2311_MSR588074_noerfroute_noerfpd_noflagging
    _erf_experiment_flagging(@@route, @@mpd, @@ppd)
  end

  def test2312_MSR588074_erfroute_noerfpd
    _erf_experiment_flagging(@@erfRoute, @@mpd, @@ppd, experiment: @@experiment_autoflagging_enabled, exp_route: true)
  end

  def test2313_MSR588074_noerfroute_erfpd
    empd = 'e1234-QA-MB-FTDEPM-A0.01'
    _erf_experiment_flagging(@@route, empd, @@ppd, exp_pd: true)
  end

  def test2314_MSR588074_missmatcherfroute_erfpd
    empd = 'e1234-QA-MB-FTDEPM-A0.01'
    _erf_experiment_flagging(@@missmatchErfRoute, empd, @@ppd, exp_pd: true)
  end

  def test2315_MSR588074_secondpatternerfroute_erfpd
    skip "not applicable in #{$env}" unless ['let', 'itdc'].member?($env)
    empd = 'e1234-QA-MB-FTDEPM-A0.01'
    _erf_experiment_flagging(@@secondPatternErfRoute, empd, @@ppd, experiment: @@experiment_autoflagging_enabled, exp_pd: true, exp_route: true)
  end

  def test2320_engineering_recipe_select_meas
    _erf_experiment_flagging(@@route, @@mpd, @@ppd, ers: true)
  end

  def test2321_engineering_recipe_select_proc_meas
    _erf_experiment_flagging(@@route, @@mpd, @@ppd, ers: true, ers_proc: true)
  end

  def test2322_engineering_recipe_select_proc
    _erf_experiment_flagging(@@route, @@mpd, @@ppd, ers_proc: true)
  end

  def test2330_jumper
    skip 'not applicable due to use_sublottype parameter' unless @@use_sublottype
    @@sublotprops['jumper'].split(',').each {|slt|
      lot = 'S' + slt + @@sil_version.split('.').collect {|e| '%02d' % e}.join('_') + '_%02d' % @@test_run + '.00'
      lot = $svtest.new_lot(lot: lot, sublottype: slt) unless $sv.lot_exists?(lot)
      assert lot, 'unable to find or create a test lot'
      @@lots << lot
      assert $sv.user_parameter_set_verify('Lot', lot, 'Jumper', 'J2'), ' failed to set jumper flag'
      $log.info "using lot #{lot.inspect}"
      _erf_experiment_flagging(@@route, @@mpd, @@ppd, jumper: true, lot: lot, sublottype: slt)
    }
  end

  def test2331_jumper_cheel
    skip 'not applicable due to use_sublottype parameter' unless @@use_sublottype
    @@sublotprops['jumper'].split(',').each {|slt|
      lot = 'S' + slt + @@sil_version.split('.').collect {|e| '%02d' % e}.join('_') + '_%02d' % @@test_run + '.00'
      lot = $svtest.new_lot(lot: lot, sublottype: slt) unless $sv.lot_exists?(lot)
      assert lot, 'unable to find or create a test lot'
      @@lots << lot
      assert $sv.user_parameter_set_verify('Lot', lot, 'Jumper', 'CHEEL:J2'), ' failed to set jumper flag'
      $log.info "using lot #{lot.inspect}"
      _erf_experiment_flagging(@@route, @@mpd, @@ppd, jumper: true, lot: lot, sublottype: slt, experiment: true)
    }
  end

  def test2332_proto
    skip 'not applicable due to use_sublottype parameter' unless @@use_sublottype
    @@sublotprops['proto'].split(',').each {|slt|
      lot = 'S' + slt + @@sil_version.split('.').collect {|e| '%02d' % e}.join('_') + '_%02d' % @@test_run + '.00'
      lot = $svtest.new_lot(lot: lot, sublottype: slt) unless $sv.lot_exists?(lot)
      assert lot, 'unable to find or create a test lot'
      @@lots << lot
      assert $sv.user_parameter_set_verify('Lot', lot, 'Jumper', 'P1'), ' failed to set jumper flag'
      $log.info "using lot #{lot.inspect}"
      _erf_experiment_flagging(@@route, @@mpd, @@ppd, proto: true, lot: lot, sublottype: slt)
    }
  end

  def test2333_proto_cheel
    skip 'not applicable due to use_sublottype parameter' unless @@use_sublottype
    @@sublotprops['proto'].split(',').each {|slt|
      lot = 'S' + slt + @@sil_version.split('.').collect {|e| '%02d' % e}.join('_') + '_%02d' % @@test_run + '.00'
      lot = $svtest.new_lot(lot: lot, sublottype: slt) unless $sv.lot_exists?(lot)
      assert lot, 'unable to find or create a test lot'
      @@lots << lot
      assert $sv.user_parameter_set_verify('Lot', lot, 'Jumper', 'CHEEL:P1'), ' failed to set jumper flag'
      $log.info "using lot #{lot.inspect}"
      _erf_experiment_flagging(@@route, @@mpd, @@ppd, proto: true, lot: lot, sublottype: slt)
    }
  end

  def test2336_minesweeper
    skip 'not applicable due to use_sublottype parameter' unless @@use_sublottype
    @@sublotprops['minesweeper'].split(',').each {|slt|
      lot = 'S' + slt + @@sil_version.split('.').collect {|e| '%02d' % e}.join('_') + '_%02d' % @@test_run + '.00'
      lot = $svtest.new_lot(lot: lot, sublottype: slt) unless $sv.lot_exists?(lot)
      assert lot, 'unable to find or create a test lot'
      @@lots << lot
      assert lot, 'unable to find or create a test lot'
      assert $sv.user_parameter_set_verify('Lot', lot, 'Jumper', 'J1'), ' failed to set jumper flag'
      $log.info "using lot #{lot.inspect}"
      _erf_experiment_flagging(@@route, @@mpd, @@ppd, minesweeper: true, lot: lot, sublottype: slt)
    }
  end

  def test2337_minesweeper_cheel
    skip 'not applicable due to use_sublottype parameter' unless @@use_sublottype
    @@sublotprops['minesweeper'].split(',').each {|slt|
      lot = 'S' + slt + @@sil_version.split('.').collect {|e| '%02d' % e}.join('_') + '_%02d' % @@test_run + '.00'
      lot = $svtest.new_lot(lot: lot, sublottype: slt) unless $sv.lot_exists?(lot)
      assert lot, 'unable to find or create a test lot'
      @@lots << lot
      assert $sv.user_parameter_set_verify('Lot', lot, 'Jumper', 'CHEEL:J1'), ' failed to set jumper flag'
      $log.info "using lot #{lot.inspect}"
      _erf_experiment_flagging(@@route, @@mpd, @@ppd, minesweeper: true, lot: lot, sublottype: slt, experiment: @@experiment_autoflagging_enabled)
    }
  end

  def test2340_multiflagging
    skip 'not applicable due to use_sublottype parameter' unless @@use_sublottype
    @@sublotprops['jumper'].split(',').each {|slt|
      lot = 'S' + slt + @@sil_version.split('.').collect {|e| '%02d' % e}.join('_') + '_%02d' % @@test_run + '.00'
      lot = $svtest.new_lot(lot: lot, sublottype: slt) unless $sv.lot_exists?(lot)
      assert lot, 'unable to find or create a test lot'
      @@lots << lot
      assert $sv.user_parameter_set_verify('Lot', lot, 'Jumper', 'J3'), ' failed to set jumper flag'
      $log.info "using lot #{lot.inspect}"
      empd = 'e1234-QA-MB-FTDEPM-A0.01'
      _erf_experiment_flagging(@@route, empd, @@ppd, exp_pd: true, ers: true, jumper: true, lot: lot, sublottype: slt)
    }
  end

  def test2350_test_multiflagging
    skip 'not applicable due to use_sublottype parameter' unless @@use_sublottype
    @@sublotprops['jumper'].split(',').each {|slt|
      lot = 'S' + slt + @@sil_version.split('.').collect {|e| '%02d' % e}.join('_') + '_%02d' % @@test_run + '.00'
      lot = $svtest.new_lot(lot: lot, sublottype: slt) unless $sv.lot_exists?(lot)
      assert lot, 'unable to find or create a test lot'
      @@lots << lot
      assert $sv.user_parameter_set_verify('Lot', lot, 'Jumper', 'J4'), ' failed to set jumper flag'
      $log.info "using lot #{lot.inspect}"
      empd = 'e1234-QA-MB-FTDEPM-A0.01'
      _erf_experiment_flagging(@@route, empd, @@ppd, exp_pd: true, ers: true, jumper: true, technology: 'TEST', lot: lot, sublottype: slt)
    }
  end

  def test2360_jumper
    skip 'not applicable due to use_sublottype parameter' if @@use_sublottype
    lot = @@lot
    @@scriptprops['jumper'].split(',').each {|slt|
      assert $sv.user_parameter_set_verify('Lot', lot, 'Jumper', slt), ' failed to set jumper flag'
      _erf_experiment_flagging(@@route, @@mpd, @@ppd, jumper: true, lot: lot, experiment: @@experiment_autoflagging_enabled)
    }
  end

  def test2370_minesweeper
    skip 'not applicable due to use_sublottype parameter' if @@use_sublottype
    lot = @@lot
    @@scriptprops['minesweeper'].split(',').each {|slt|
      assert $sv.user_parameter_set_verify('Lot', lot, 'Jumper', slt), ' failed to set jumper flag'
      _erf_experiment_flagging(@@route, @@mpd, @@ppd, minesweeper: true, lot: lot, experiment: @@experiment_autoflagging_enabled)
    }
  end

  def test2380_proto
    skip 'not applicable due to use_sublottype parameter' if @@use_sublottype
    lot = @@lot
    @@scriptprops['proto'].split(',').each {|slt|
      assert $sv.user_parameter_set_verify('Lot', lot, 'Jumper', slt), ' failed to set jumper flag'
      _erf_experiment_flagging(@@route, @@mpd, @@ppd, proto: true, lot: lot)
    }
  end

  def _erf_experiment_flagging(route, mpd, ppd, params={})
    lot = params[:lot] || @@lot
    experiment = params[:experiment]
    ers = !!params[:ers]
    ers_proc = !!params[:ers_proc]
    tech = params[:technology] || '32NM'
    sublottype = params[:sublottype] || 'PO'
    parameters = ['QA_P900_ECP_TG23', 'QA_P901_ECP_TG23', 'QA_P902_ECP_TG23', 'QA_P903_ECP_TG23', 'QA_P904_ECP_TG23']
    ooc = params[:ooc]
    #parameters = @@parameters
    #
    # submit the process DCR
    submit_process_dcr(ppd, eqp: @@eqp, lot: lot, ers: ers_proc, technology: tech, sublottype: sublottype)
    #
    # build and submit measurement DCR with all parameters
    dcr = Space::DCR.new(eqp: @@eqp, lot: lot, route: route, op: mpd, ers: ers, technology: tech, sublottype: sublottype)
    wafer_values = Hash[*@@wafer_values.collect {|w,v| [w,v+100]}.flatten]
    dcr.add_parameters(parameters, wafer_values)
    res = $client.send_dcr(dcr, export: "log/#{__method__}.xml")
    nsamples = parameters.size * wafer_values.size
    oocs = ooc ? nsamples*2 : 0
    assert $spctest.verify_dcr_accepted(res, nsamples, nooc: oocs), 'DCR not submitted successfully'
    #
    if oocs > 0
      assert $spctest.verify_futurehold(lot, nil, nil, expected_holds: parameters.size, messages: @@messages), 'error setting future hold'
    end
    # check for flags
    sleep 2 # sleep because some kind of flagging needs some seconds to be performed
    folder = (tech == 'TEST') ? $lds_setup.folder(@@autocreated_qa) : $lds.folder(@@autocreated_qa)
    parameters.each {|p|
      ch = folder.spc_channel(parameter: p)
      samples = ch.samples
      samples.each {|s|
        assert verify_autoflagging_type(s, params), 'error in autoflagtype'
        assert verify_experiment(s, experiment), 'error in autoflagging'
      }
    }
  end

  def verify_autoflagging_type(sample, params)
    flags = sample.data_keys['AutoflagLotType'].split(' | ')
    flag_table = { :exp_pd => 'ExperimentPD',
      :exp_route => 'ExperimentRoute',
      :ers => 'EngRecipe',
      :ers_proc => 'EngRecipeProc',
      :jumper => 'Jumper',
      :proto => 'PROTO',
      :minesweeper => 'Minesweeper'
    }
    index = 0
    res = true
    flag_table.each_pair do |flag,v|
      next unless params.has_key?(flag)
      # preceeding rules
      next if flag == :exp_route and params.has_key?(:exp_pd)
      next if flag == :ers_proc and params.has_key?(:ers)
      if index < flags.count and (flags[index] == v)
        index += 1
      else
        ($log.warn "  Sample #{sample.sid} AutoflagLotType should contain #{v} instead it was: #{flags[index]}"; res = false)
      end
    end
    ($log.warn "  Sample #{sample.sid} AutoflagLotType contains unexpected flags #{flags[index..flags.count-1].inspect}"; res = false) if index < flags.count-1
    return res
  end

  def verify_experiment(sample, experiment)
    ret = true
    if experiment
      ($log.warn "  Sample #{sample.sid} #{experiment}: missing flag"; ret = false) unless sample.iflag
      ($log.warn "  Sample #{sample.sid} #{experiment}: missing comment (Experiment)"; ret = false) unless sample.icomment == 'Experiment'
    else
      ($log.warn "  Sample #{sample.sid} #{experiment}: flag must not be set"; ret = false) if sample.iflag
    end
    return ret
  end

end
