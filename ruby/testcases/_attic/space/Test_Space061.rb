=begin 
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.
    
Author: Conny Stenzel, 2012-10-09
=end

require_relative 'Test_Space_Setup'

# =Testcases for SPACE/SIL - SpecLimits Hierarchy in the config file - Wildcard tests for PD/Technology/Route
#
# <i>Initially wildcards were not requested as requirement. However, the implementation as it currently is, 
# works in the way that an empty cell at the columns technology or route is interpreted as a wildcard. The most specified line wins. 
# Empty PDs instead are allowed but result in no spec limits.</i>
# 2015-11-16 Fab 1 with SetupFC: No gaps are allowed in hierarchy on the left - means: fields have to be filled from left to right
class Test_Space061 < Test_Space_Setup

  def setup
    super
    if $lds && self.class.class_variable_defined?(:@@lot) # bypass test0000_setup
      assert_equal 0, $sv.eqp_cleanup(@@eqp)
      assert_equal 0, $sv.lot_cleanup(@@lot)
      [$lds_setup, $lds].each {|lds|
        folder = $lds.folder(@@autocreated_qa)
        assert folder.delete_channels, 'error cleaning channels' if folder
      }
    end
  end
  
  def test0610000_setup
    #props = $spctest.silserver.read_jobprops
    #@@speclimitlookup_route_required = false if @@speclimitlookup_route_required.to_s != props[:'speclimitlookup.route.required']
    #$log.info "Route required: #{@@speclimitlookup_route_required}"
    # resubmit of the corresponding Spec Limits file
    if ['xxxitdc', 'let'].member?($env)
      (human_workflow_action(@@specLimitsHierarchyTable, 'CANCEL', :user=>@@approvers[0]); sleep 30) if $sfc.wait_for_approve(@@specLimitsHierarchyTable, :user=>@@approvers[0], :timeout=>1, :sleeptime=>1)
      (human_workflow_action(@@specLimitsHierarchyTable, 'CANCEL', :user=>@@approvers[0]); sleep 30) if $sfc.wait_for_edit_content(@@specLimitsHierarchyTable, :user=>@@approvers[0], :timeout=>1, :sleeptime=>1)
      (human_workflow_action(@@specLimitsHierarchyTable, 'CANCEL'); sleep 30) if $sfc.wait_for_edit_content(@@specLimitsHierarchyTable, :timeout=>1, :sleeptime=>1)
      edit_submit_approve(@@specLimitsHierarchyTable, @@approvers[0])
    end
  end
  
  def test061000_speclimits_INLINE
    technology = 'xxNM'
    route = 'SIL-xxx2.01'
    productgroup = '32xxC'
    product = 'SILPRODx.01'
    specs = [400, 500, 600]
    send_dcr_verify(technology: technology, route: route, productgroup: productgroup, product: product, specs: specs)
  end
  
  def test061000_no_speclimits_INLINE
    pd = 'QA-SPEC-HIER-METROLOGY-99.01'
    technology = 'xxNM'
    route = 'SIL-xxx2.01'
    productgroup = '32xxC'
    product = 'SILPRODx.01'
    send_dcr_verify(pd: pd, technology: technology, route: route, productgroup: productgroup, product: product)
  end
  
  def test061001_speclimits_INLINE
    route = 'SIL-xxx2.01'
    productgroup = '32xxC'
    product = 'SILPRODx.01'
    specs = [401, 501, 601]
    send_dcr_verify(route: route, productgroup: productgroup, product: product, specs: specs)
  end
  
  def test061002_speclimits_INLINE
    productgroup = '32xxC'
    product = 'SILPRODx.01'
    specs = [402, 502, 602]
    send_dcr_verify(productgroup: productgroup, product: product, specs: specs)
  end
  
  def test061003_speclimits_INLINE
    product = 'SILPRODx.01'
    specs = [403, 503, 603]
    send_dcr_verify(product: product, specs: specs)
  end
  
  def test061004_speclimits_INLINE
    specs = [404, 504, 604]
    send_dcr_verify(specs: specs)
  end
  
  def test061101_speclimits_INLINE
    technology = 'TEST'
    route = 'SIL-xxx2.01'
    productgroup = '32xxC'
    product = 'SILPRODx.01'
    specs = [701, 801, 901]
    send_dcr_verify(technology: technology, route: route, productgroup: productgroup, product: product, specs: specs)
  end
  
  def test061102_speclimits_INLINE
    technology = 'TEST'
    productgroup = '32xxC'
    product = 'SILPRODx.01'
    specs = [702, 802, 902]
    send_dcr_verify(technology: technology, productgroup: productgroup, product: product, specs: specs)
  end
  
  def test061103_speclimits_INLINE
    technology = 'TEST'
    product = 'SILPRODx.01'
    specs = [703, 803, 903]
    send_dcr_verify(technology: technology, product: product, specs: specs)
  end
  
  def test061104_speclimits_INLINE
    technology = 'TEST'
    specs = [704, 804, 904]
    send_dcr_verify(technology: technology, specs: specs)
  end
  
  # send DCR and verify spec limits, returns nothing
  def send_dcr_verify(params={})
    $log.info "#{__method__}: #{params}"
    # build and submit DCR with one parameter only to identify it easily in the hold claim memo
    pd = params[:pd] || 'QA-SPEC-HIER-METROLOGY-04.01'
    technology = params[:technology] || '32NM'
    route = params[:route] || 'SIL-0002.01'
    productgroup = params[:productgroup] || '3261C'
    product = params[:product] || 'SILPROD2.01'
    parameters = params[:parameters] || ['QA_PARAM_950', 'QA_PARAM_951']
    specs = params[:specs] || []
    #
    dcr = Space::DCR.new(eqp: @@eqp, lot: @@lot, productgroup: productgroup, product: product, technology: technology, op: pd, route: route)
    dcr.add_parameters(parameters, @@wafer_valuesooc)
    res = $client.send_dcr(dcr, export: "log/#{caller_locations(1,1)[0].label}.xml")
    # verify DCR is accepted
    nooc = specs.empty? ? 0 : 'all*2'
    assert $spctest.verify_dcr_accepted(res, parameters.size*@@wafer_valuesooc.size, nooc: nooc), 'DCR not submitted successfully'
    #
    # verify limits if specs is not empty    
    unless specs.empty?
      lds = (technology == 'TEST') ? $lds_setup : $lds
      folder = lds.folder(@@autocreated_qa)
      parameters.each {|p|
        assert ch = folder.spc_channel(parameter: p), "no channel for parameter #{p.inspect}"
        assert $spctest.verify_sample_speclimits(ch.samples.last, specs[0], specs[1], specs[2]), 'wrong spec limits'
      }
    end
  end
  
end
