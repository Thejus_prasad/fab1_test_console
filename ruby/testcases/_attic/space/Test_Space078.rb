=begin 
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.
    
Author: D.Steger, 2012-05-30
=end

require_relative 'Test_Space_Setup'


# Test Space/SIL.
class Test_Space078 < Test_Space_Setup
  @@chambers = {'PM1'=>5, 'PM2'=>6, 'PM3'=>7, 'PM4'=>8, 'PM5'=>9}
  
  
  def setup
    super
    return unless $lds && self.class.class_variable_defined?(:@@lot) # bypass test0000_setup
    assert_equal 0, $sv.eqp_cleanup(@@eqp)
    assert_equal 0, $sv.lot_cleanup(@@lot)
    #
    $lds.all_spc_channels('QA_PARAM_90*').each {|ch| assert ch.delete}
    folder = $lds.folder(@@autocreated_qa)
    assert folder.delete_channels, 'error cleaning channels' if folder
  end


  def test0780_setup
    $setup_ok = false
    #
    @@ppds_qa = $sildb.pd_reference(mpd: @@mpd_qa)[0..4]
    assert_equal @@ppds_qa.size, @@ppds_qa.compact.size, "no PD reference found for #{@@mpd_qa}"
    ['AutoEquipmentInhibit-PD1', 'AutoEquipmentInhibit-PD2', 'AutoEquipmentInhibit-PD3', 
      'AutoEquipmentInhibit-PD4', 'AutoEquipmentInhibit-PD5', 'AutoEquipmentInhibit-PDall', 'ReleaseEquipmentInhibit',
      'AutoChamberInhibit-PD1', 'AutoChamberInhibit-PD2', 'AutoChamberInhibit-PD3', 
      'AutoChamberInhibit-PD4', 'AutoChamberInhibit-PD5', 'AutoChamberInhibit-PDall', 'ReleaseChamberInhibit',
      'AutoChamberAndRecipeInhibit-PD1', 'AutoChamberAndRecipeInhibit-PD2', 'AutoChamberAndRecipeInhibit-PD3',
      'AutoChamberAndRecipeInhibit-PD4', 'AutoChamberAndRecipeInhibit-PD5', 'AutoChamberAndRecipeInhibit-PDall', 'ReleaseChamberAndRecipeInhibit',
      'ReleaseEquipmentInhibit', 'ReleaseChamberInhibit', 'ReleaseMachineRecipeInhibit', 'ReleaseEquipmentAndRecipeInhibit'
    ].each {|action|
      assert $spc.corrective_action(action), "CA #{action.inspect} not defined"
    }
    #
    $lds.all_spc_channels('QA_PARAM_90*').each {|ch| ch.delete}
    folder = $lds.folder('AutoCreated_QA')
    folder.delete_channels if folder
    #
    ## TODO Speclimits exist
    $setup_ok = true
  end
  
  def test0781_ca_equipment_inhibit_per_pd
    channels = _run_scenario_per_pd('Equipment')
    channels.each_with_index do |ch,i|
      eqp = "#{@@eqp}_#{i}"
      #
      # verify inhibit and comment
      assert $spctest.verify_inhibit(eqp, 1), 'equipment must have an inhibit'
      assert $spctest.verify_ecomment([ch], "TOOL_HELD: #{eqp}: reason={X-S")
      $log.info 'EquipmentInhibit automatically set'
      #
      # release inhibit
      s = ch.samples(ckc: true)[-1]
      assert s.assign_ca($spc.corrective_action('ReleaseEquipmentInhibit')), 'error assigning corective action'
      assert $spctest.verify_inhibit(eqp, 0), 'equipment must not have an inhibit'
      $log.info 'EquipmentInhibit released'
    end
  end
  
  def test0782_ca_chamber_inhibit_per_pd
    channels = _run_scenario_per_pd('Chamber')
    channels.each_with_index do |ch,i|
      eqp = "#{@@eqp}_#{i}"
      cha = "PM#{i+1}"
      #
      # verify inhibit and comment
      if i.odd?
        assert $spctest.verify_inhibit(eqp, 0, class: :eqp_chamber), 'equipment-chamber must not have an inhibit'
      else
        assert $spctest.verify_inhibit(eqp, 1, class: :eqp_chamber), 'equipment-chamber must have an inhibit'
        assert $spctest.verify_ecomment([ch], "TOOL+CHAMBER_HELD: #{eqp}/#{cha}: reason={X-S")
        $log.info 'ChamberInhibit automatically set'
      end
      #
      # release inhibit
      if not i.odd?
        s = ch.samples(ckc: true)[-1]
        assert s.assign_ca($spc.corrective_action('ReleaseChamberInhibit')), 'error assigning corrective action'
        assert $spctest.verify_inhibit(eqp, 0, class: :eqp_chamber), 'chamber must not have an inhibit'
        $log.info 'ChamberInhibit released'
      end
      sleep 5
      assert $spctest.verify_inhibit(eqp, 0), "equipment: #{eqp} must not have an inhibit. See MSR808585."
    end
  end
  
  def test0783_ca_recipe_inhibit_per_pd
    channels = _run_scenario_per_pd('MachineRecipe')
    channels.each_with_index do |ch,i|
      mrecipe = "P-UTC001_#{i}.UTMR000.01"
      #
      # verify inhibit and comment
      assert $spctest.verify_inhibit(mrecipe, 1, class: :recipe), 'equipment must have an inhibit'
      assert $spctest.verify_ecomment([ch], "RECIPE_HELD: #{mrecipe}: reason={X-S")
      $log.info 'MachineRecipe Inhibit automatically set'
      #
      # release inhibit
      s = ch.samples(ckc: true)[-1]
      assert s.assign_ca($spc.corrective_action('ReleaseMachineRecipeInhibit')), 'error assigning corective action'
      assert $spctest.verify_inhibit(mrecipe, 0, class: :recipe), 'equipment must not have an inhibit'
      $log.info 'MachineRecipe Inhibit released'
    end
  end
  
  def test0784_ca_toolrecipe_inhibit_per_pd
    channels = _run_scenario_per_pd('EquipmentAndRecipe')
    channels.each_with_index do |ch,i|
      eqp = "#{@@eqp}_#{i}" 
      mrecipe = "P-UTC001_#{i}.UTMR000.01"
      #
      # verify inhibit and comment
      assert $spctest.verify_inhibit([eqp, mrecipe], 1, class: :eqp_recipe), 'equipment+recipe must have an inhibit'
      assert $spctest.verify_ecomment([ch], 'TOOL+RECIPE_HELD: reason={X-S')
      assert $spctest.verify_ecomment([ch], @@eqp)
      assert $spctest.verify_ecomment([ch], mrecipe) 
      $log.info 'Equipment and Recipe Inhibit automatically set'
      #
      # release inhibit
      s = ch.samples(ckc: true)[-1]
      assert s.assign_ca($spc.corrective_action('ReleaseEquipmentAndRecipeInhibit')), 'error assigning corective action'
      assert $spctest.verify_inhibit([eqp, mrecipe], 0, class: :eqp_recipe), 'equipment+recipe must not have an inhibit'
      $log.info 'Equipment and Recipe Inhibit released'
    end
  end

  def test0785_ca_all
    5.times {|i| 
      assert_equal 0, $sv.eqp_cleanup("#{@@eqp}_#{i}")
      assert_equal 0, $sv.inhibit_cancel(:recipe, "P-UTC001_#{i}.UTMR000.01")
    }
    channels = _create_chan
    actions = ['AutoEquipmentInhibit-PDall', 'AutoChamberInhibit-PDall', 'AutoMachineRecipeInhibit-PDall', 'AutoEquipmentAndRecipeInhibit-PDall',
      'AutoChamberAndRecipeInhibit-PDall', 'ReleaseEquipmentInhibit', 'ReleaseChamberInhibit', 'ReleaseMachineRecipeInhibit', 
      'ReleaseEquipmentAndRecipeInhibit', 'ReleaseChamberAndRecipeInhibit']
    assert $spctest.assign_verify_cas(channels, actions)
    #
    dcrps = @@ppds_qa.each_with_index.collect {|ppd, i|
      create_process_dcr(ppd: ppd, eqp: "#{@@eqp}_#{i}", chambers: @@chambers, wafer_values: @@wafer_values2, 
        processing_areas: "PM#{i+1}", logical_recipe: "F-P-FTEOS8K15.0#{i}", machine_recipe: "P-UTC001_#{i}.UTMR000.01")
    }
    assert $spctest.send_dcrs_verify(dcrps), 'error sending DCRs'
    assert_equal 0, $sv.eqp_cleanup(@@eqp)
    assert_equal 0, $sv.lot_cleanup(@@lot)
    # send DCR with OOC values
    dcr = Space::DCR.new(lot: @@lot, eqp: @@mtool, op: @@mpd_qa)
    dcr.add_parameters(@@parameters, @@wafer_values2ooc)
    assert $spctest.send_dcr_verify(dcr, nsamples: @@parameters.count*@@wafer_values2ooc.size, nooc: 'all*2'), 'error sending DCRs'

    # verify inhibits and comments
    5.times do |i|
      eqp = "#{@@eqp}_#{i}"
      cha = "PM#{i+1}"
      mrecipe = "P-UTC001_#{i}.UTMR000.01"
      #
      assert $spctest.verify_inhibit(eqp, @@parameters.count), 'equipment must have an inhibit'
      assert $spctest.verify_ecomment(channels, "TOOL_HELD: #{eqp}: reason={X-S")
      
      if i.odd?
        assert $spctest.verify_inhibit(eqp, @@parameters.count, class: :eqp_chamber), 'chamber must have an inhibit'
        assert $spctest.verify_ecomment(channels, "TOOL+CHAMBER_HELD: #{eqp}/#{cha}: reason={X-S")
      else
        assert $spctest.verify_inhibit(eqp, 0, {class: :eqp_chamber}), 'chamber must NOT have an inhibit'
      end
      
      assert $spctest.verify_inhibit(mrecipe, @@parameters.count, class: :recipe), 'machine recipe must have an inhibit'
      assert $spctest.verify_ecomment(channels, "RECIPE_HELD: #{mrecipe}: reason={X-S")

      assert $spctest.verify_inhibit([eqp, mrecipe], @@parameters.count, class: :eqp_recipe), 'equipment+recipe must have an inhibit'
      assert $spctest.verify_ecomment(channels, 'TOOL+RECIPE_HELD: reason={X-S')
      assert $spctest.verify_ecomment(channels, eqp)
      assert $spctest.verify_ecomment(channels, mrecipe) 

      assert $spctest.verify_inhibit([eqp, mrecipe], @@parameters.count, class: :eqp_chamber_recipe), 'chamber+recipe must have an inhibit'
      assert $spctest.verify_ecomment(channels, "TOOL+CHAMBER+RECIPE_HELD: #{eqp}/#{cha}/#{mrecipe}: reason={X-S")
    end
    
    # release a single entity
    n = @@parameters.count - 1
    channels.each_with_index do |ch, i|
      eqp = "#{@@eqp}_#{i}"
      cha = "PM#{i+1}"
      mrecipe = "P-UTC001_#{i}.UTMR000.01"

      s = ch.samples(ckc: true)[-1]
      assert s.assign_ca($spc.corrective_action('ReleaseMachineRecipeInhibit'), comment: "[RELEASE=#{mrecipe}]"), 'error assigning corective action'
      assert $spctest.verify_inhibit(mrecipe, n, {class: :recipe}), 'incorrect count of machine recipe inhibits'

      if i.odd?
        s = ch.samples(ckc: true)[-2]
        assert s.assign_ca($spc.corrective_action('ReleaseChamberInhibit'), comment: "[RELEASE=#{eqp}/#{cha}]"), 'error assigning corective action'
        assert $spctest.verify_inhibit(eqp, n, {class: :eqp_chamber}), 'chamber must not have an inhibit'
      end

      s = ch.samples(ckc: true)[-3]
      assert s.assign_ca($spc.corrective_action('ReleaseEquipmentInhibit'), comment: "[RELEASE=#{eqp}]"), 'error assigning corective action'
      assert $spctest.verify_inhibit(eqp, n), 'equipment must not have an inhibit'

      s = ch.samples(ckc: true)[-4]
      assert s.assign_ca($spc.corrective_action('ReleaseEquipmentAndRecipeInhibit'), comment: "[RELEASE=#{eqp}/(#{mrecipe})]"), 'error assigning corective action'
      ## BROKEN      assert $spctest.verify_inhibit([eqp, mrecipe], n, class: :eqp_recipe), 'equipment+recipe must not have an inhibit'
    end

    # add release action with comment [Release=All] for the same samples
    channels.each_with_index do |ch, i|
      eqp = "#{@@eqp}_#{i}"
      cha = "PM#{i+1}"
      mrecipe = "P-UTC001_#{i}.UTMR000.01"
      s = ch.samples(ckc: true)[-1]
      assert s.assign_ca($spc.corrective_action('ReleaseMachineRecipeInhibit'), comment: '[RELEASE=All]'), 'error assigning corrective action'
      s = ch.samples(ckc: true)[-3]
      assert s.assign_ca($spc.corrective_action('ReleaseEquipmentInhibit'), comment: '[RELEASE=All]'), 'error assigning corrective action'
      s = ch.samples(ckc: true)[-4]
      assert s.assign_ca($spc.corrective_action('ReleaseEquipmentAndRecipeInhibit'), comment: '[RELEASE=All]'), 'error assigning corrective action'
      $log.info "all inhibit release actions assigned for #{ch}"
    end
    
    # need extra assign for chambers
    5.times.select {|i| i.odd?}.each do |i|
      eqp = "#{@@eqp}_#{i}"
      cha = "PM#{i+1}"
      channels.each {|ch|
        s = ch.samples(ckc: true)[-2]
        assert s.assign_ca($spc.corrective_action('ReleaseChamberInhibit'), comment: '[RELEASE=All]'), 'error assigning corrective action'
      }
    end
    
    channels.each_with_index do |ch, i|
      eqp = "#{@@eqp}_#{i}"
      cha = "PM#{i+1}"
      mrecipe = "P-UTC001_#{i}.UTMR000.01"
      assert $spctest.verify_inhibit(mrecipe, 0, class: :recipe)
      assert $spctest.verify_inhibit(eqp, 0, class: :eqp_chamber, attrib: cha) if i.odd?
      assert $spctest.verify_inhibit(eqp, 0)
      assert $spctest.verify_inhibit([eqp, mrecipe], 0, class: :eqp_recipe)
    end
  end

  def test0786_ca_no_responsible_run
    5.times {|i| 
      assert_equal 0, $sv.eqp_cleanup("#{@@eqp}_#{i}")
      assert_equal 0, $sv.inhibit_cancel(:recipe, "P-UTC001_#{i}.UTMR000.01")
    }
    mpd = 'QA-MB-FTDEPM-LIS-CAEX.01'
    refute_empty ppds = $sildb.pd_reference(mpd: mpd), "no PD reference found for #{mpd}"
    channels = _create_chan(mpd: mpd, ppds: ppds)
    channels.each_with_index {|ch, i|
      actions = ["AutoEquipmentInhibit-PD#{i+1}", 'AutoMachineRecipeInhibit-PDall', 'ReleaseEquipmentInhibit', 'ReleaseMachineRecipeInhibit', 'ReleaseLotHold']
      assert $spctest.assign_verify_cas(ch, actions)
    }
    #
    dcrps = ppds.each_with_index.collect {|ppd, i|
      create_process_dcr(ppd: ppd, eqp: "#{@@eqp}_#{i}", chambers: @@chambers, wafer_values: @@wafer_values2, 
        processing_areas: "PM#{i+1}", logical_recipe: "F-P-FTEOS8K15.0#{i}", machine_recipe: "P-UTC001_#{i}.UTMR000.01")
    }
    assert $spctest.send_dcrs_verify(dcrps), 'error sending DCRs'
    assert_equal 0, $sv.lot_cleanup(@@lot)
    #
    # send DCR with OOC values
    dcr = Space::DCR.new(lot: @@lot, eqp: @@mtool, op: mpd)
    dcr.add_parameters(@@parameters, @@wafer_values2ooc)
    assert $spctest.send_dcr_verify(dcr, nsamples: @@parameters.count*@@wafer_values2ooc.size, nooc: 'all*2'), 'error sending DCRs'
    #  
    channels.each_with_index do |ch,i|
      eqp = "#{@@eqp}_#{i}"
      mrecipe = 'P-UTC001_0.UTMR000.01'
      #
      # verify equipment inhibit and comment
      if i == 0
        assert $spctest.verify_inhibit(eqp, 1, {class: :eqp}), 'equipment+recipe must have an inhibit'
        assert $spctest.verify_ecomment([ch], "TOOL_HELD: #{eqp}: reason={X-S")
        $log.info 'Equipment and Recipe Inhibit automatically set'
      # lot hold to be checked only once
      elsif i == 1
        if @@cafailure_lothold_fallback
          assert $spctest.verify_futurehold2(@@lot, 'Execution of [AutoEquipmentInhibit-PD'), 'lot must have a future hold'
          assert $spctest.verify_ecomment(channels[1..4], "LOT_HELD: #{@@lot}: reason={X-S")
          $log.info 'Future hold set as fallback'
        else # TODO currently multiple derdacks are sent
          $spctest.verify_notification( 'Execution of [AutoEquipmentInhibit-PD', ch.chid.to_s )
        end
      end
      # all channels have machine recipe inhibit
      assert $spctest.verify_inhibit(mrecipe, @@parameters.count, class: :recipe), 'machine recipe must have an inhibit'
      assert $spctest.verify_ecomment(channels, "RECIPE_HELD: #{mrecipe}: reason={X-S")
    end

    if @@cafailure_lothold_fallback
      assert_equal 0, $sv.lot_gatepass(@@lot), 'SiView GatePass error'
      assert $sv.lot_hold_list(@@lot, type: 'FutureHold').size > 0, 'lot must be on hold'
    end
    
    # release actions
    channels.each_with_index do |ch,i|
      eqp = "#{@@eqp}_#{i}"
      mrecipe = 'P-UTC001_0.UTMR000.01'
      s = ch.samples(ckc: true)[-1]
      # ReleaseLotHold
      assert s.assign_ca($spc.corrective_action('ReleaseLotHold')), 'error assigning corective action'

      s = ch.samples(ckc: true)[-1]
      # release inhibit / lot hold
      if i == 0
        assert s.assign_ca($spc.corrective_action('ReleaseEquipmentInhibit')), 'error assigning corective action'
        assert $spctest.verify_inhibit(eqp, 0, class: :eqp), 'equipment+recipe must not have an inhibit'
        $log.info 'Equipment Inhibit released'
      elsif i == 1 and @@cafailure_lothold_fallback
        assert $spctest.verify_lothold_released(@@lot, 'Execution of [AutoEquipmentInhibit-PD'), 'lot should be released'
      else
        assert $spctest.verify_inhibit(eqp, 0, class: :eqp), 'equipment must not have an inhibit'
      end

      # all channels can release recipe inhibit
      s = ch.samples(ckc: true)[-2]
      assert s.assign_ca($spc.corrective_action('ReleaseMachineRecipeInhibit')), 'error assigning corective action'
      assert $spctest.verify_inhibit(mrecipe, @@parameters.count-i-1, class: :recipe), 'incorrect count of machine recipe inhibits'
      $log.info 'Recipe released'
    end
    #
    assert $spctest.verify_lothold_released(@@lot, '[(Raw above specification)'), 'no further holds expected'
  end

  def test0787_ca_chamberrecipe_inhibit_per_pd
    channels = _run_scenario_per_pd('ChamberAndRecipe')
    channels.each_with_index do |ch,i|
      eqp = "#{@@eqp}_#{i}"
      cha = "PM#{i+1}"
      mrecipe = "P-UTC001_#{i}.UTMR000.01"
      #
      # verify inhibit and comment
      assert $spctest.verify_inhibit([eqp, mrecipe], 1, {class: :eqp_chamber_recipe}), 'chamber+recipe must have an inhibit'
      assert $spctest.verify_ecomment([ch], "TOOL+CHAMBER+RECIPE_HELD: #{eqp}/#{cha}/#{mrecipe}: reason={X-S")
      $log.info 'Chamber and Recipe Inhibit automatically set'
      #
      # release inhibit
      s = ch.samples(ckc: true)[-1]
      assert s.assign_ca($spc.corrective_action('ReleaseChamberAndRecipeInhibit')), 'error assigning corective action'
      assert $spctest.verify_inhibit([eqp, mrecipe], 0, {class: :eqp_chamber_recipe}), 'chamber+recipe must not have an inhibit'
      $log.info 'Chamber and Recipe Inhibit released'
    end
  end

  #Build in to test RecipeEqpInhibit standalone - not as in 0785 together with many other CAs
  def test0788_ca_all
    5.times {|i| 
      assert_equal 0, $sv.eqp_cleanup("#{@@eqp}_#{i}")
      assert_equal 0, $sv.inhibit_cancel(:recipe, "P-UTC001_#{i}.UTMR000.01")
    }
    channels = _create_chan
    actions = ['AutoEquipmentAndRecipeInhibit-PDall', 'ReleaseEquipmentAndRecipeInhibit-noDeflt']
    assert $spctest.assign_verify_cas(channels, actions)
    #
    dcrps = @@ppds_qa.each_with_index.collect {|ppd, i|
      create_process_dcr( ppd: ppd, eqp: "#{@@eqp}_#{i}", chambers: @@chambers, wafer_values: @@wafer_values2, 
        processing_areas: 'PM1', logical_recipe: "F-P-FTEOS8K15.0#{i}", machine_recipe: "P-UTC001_#{i}.UTMR000.01")
    }
    assert $spctest.send_dcrs_verify(dcrps), 'error sending DCRs'
    assert_equal 0, $sv.eqp_cleanup(@@eqp)
    assert_equal 0, $sv.lot_cleanup(@@lot)
    # send DCR with OOC values
    dcr = Space::DCR.new(lot: @@lot, eqp: @@mtool, op: @@mpd_qa)
    dcr.add_parameters(@@parameters, @@wafer_values2ooc)
    assert $spctest.send_dcr_verify(dcr, nsamples: @@parameters.count*@@wafer_values2ooc.size, nooc: 'all*2'), 'error sending DCRs'

    5.times do |i|
      eqp = "#{@@eqp}_#{i}"
      mrecipe = "P-UTC001_#{i}.UTMR000.01"
      assert $spctest.verify_inhibit([eqp, mrecipe], @@parameters.count, class: :eqp_recipe), 'equipment+recipe must have an inhibit'
      assert $spctest.verify_ecomment(channels, 'TOOL+RECIPE_HELD: reason={X-S')
      assert $spctest.verify_ecomment(channels, eqp)
      assert $spctest.verify_ecomment(channels, mrecipe)
    end

    # release a single entity
    channels.each_with_index do |ch, i|
      eqp = "#{@@eqp}_#{i}"
      mrecipe = "P-UTC001_#{i}.UTMR000.01"

      s = ch.samples(ckc: true)[-4]
      assert s.assign_ca($spc.corrective_action('ReleaseEquipmentAndRecipeInhibit-noDeflt'), comment: "[Release=#{eqp}/(#{mrecipe})]"), 'error assigning corective action'
    end

    # add release action with comment Release=[All] for the same samples
    channels.each_with_index do |ch, i|
      eqp = "#{@@eqp}_#{i}"
      mrecipe = "P-UTC001_#{i}.UTMR000.01"
      s = ch.samples(ckc: true)[-4]
      assert s.assign_ca($spc.corrective_action('ReleaseEquipmentAndRecipeInhibit-noDeflt'), comment: '[Release=All]'), 'error assigning corrective action'
      $log.info "all inhibits released for #{ch}"
    end
    
    channels.each_with_index do |ch, i|
      eqp = "#{@@eqp}_#{i}"
      mrecipe = "P-UTC001_#{i}.UTMR000.01"
      assert $spctest.verify_inhibit([eqp, mrecipe], 0, class: :eqp_recipe), 'no inhibit expected'
    end
  end


  # send DCRs with non-ooc values to setup channels
  def _create_chan(params={})    
    mpd = params[:mpd] || @@mpd_qa
    ppds = params[:ppds] || @@ppds_qa
    $log.info "using MPD #{mpd.inspect} and PPDs #{ppds.inspect}"
    #
    dcrps = ppds.each_with_index.collect {|ppd, i|
      create_process_dcr(ppd: ppd, eqp: "#{@@eqp}_#{i}", lot: @@lot, chambers: @@chambers, processing_areas: "PM#{i+1}", 
        wafer_values: @@wafer_values2, logical_recipe: "F-P-FTEOS8K15.0#{i}", machine_recipe: "F-CVD60X-P.F-P-FTEOS8K15.0#{i}")
    }
    assert $spctest.send_dcrs_verify(dcrps), 'error sending DCR'

    dcr = Space::DCR.new(lot: @@lot, eqp: @@mtool, op: mpd)
    dcr.add_parameters(@@parameters, @@wafer_values2)
    assert $spctest.send_dcr_verify(dcr, nsamples: @@parameters.size*@@wafer_values2.size), 'DCR not submitted successfully'
    #
    folder = $lds.folder(@@autocreated_qa)
    @@parameters.collect {|p|
      assert ch = folder.spc_channel(parameter: p), "channel for parameter #{p} missing in folder AutoCreated_QA"
      ch
    }
  end
  
  def _run_scenario_per_pd(entity)
    5.times {|i|
      assert_equal 0, $sv.eqp_cleanup("#{@@eqp}_#{i}")
      assert_equal 0, $sv.inhibit_cancel(:recipe, "P-UTC001_#{i}.UTMR000.01")
    }
    channels = _create_chan
    channels.each_with_index {|ch, i|
      assert $spctest.assign_verify_cas(ch, ["Auto#{entity}Inhibit-PD#{i+1}", "Release#{entity}Inhibit"])
    }
    # send process DCRs
    dcrps = (@@ppds_qa.each_with_index.collect {|ppd, i|
      create_process_dcr(ppd: ppd, eqp: "#{@@eqp}_#{i}", chambers: @@chambers, wafer_values: @@wafer_values2, 
        processing_areas: "PM#{i+1}", logical_recipe: "F-P-FTEOS8K15.0#{i}", machine_recipe: "P-UTC001_#{i}.UTMR000.01")
    })
    assert $spctest.send_dcrs_verify(dcrps, exporttag: "#{caller_locations(1,1)[0].label}_p"), 'error sending DCR'
    # send DCR with OOC values
    assert_equal 0, $sv.eqp_cleanup(@@eqp)
    assert_equal 0, $sv.lot_cleanup(@@lot)
    #
    dcr = Space::DCR.new(lot: @@lot, eqp: @@mtool, op: @@mpd_qa)    
    dcr.add_parameters(@@parameters, @@wafer_values2ooc)
    nsamples = @@parameters.count * @@wafer_values2ooc.size
    assert $spctest.send_dcr_verify(dcr, nsamples: nsamples, nooc: 'all*2', exporttag: "#{caller_locations(1,1)[0].label}_m"), 'error sending DCR'
    return channels   
  end
end
