=begin 
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.
    
Author: Daniel Steger, 2012-07-31
=end

require_relative 'Test_Space_Setup'

class Test_Space20 < Test_Space_Setup

  def self.shutdown
    @@lots.each {|lot| $sv.delete_lot_family(lot)}
  end


  def setup
    super
    return unless $lds && self.class.class_variable_defined?(:@@lot) # bypass test0000_setup
    assert_equal 0, $sv.eqp_cleanup(@@eqp)
    assert_equal 0, $sv.lot_cleanup(@@lot)
    #
    [$lds, $lds_setup].each {|lds|
      lds.all_spc_channels('QA_PARAM_90*').each {|ch| assert ch.delete}
      folder = lds.folder(@@autocreated_qa)
      assert folder.delete_channels, "error cleaning channels" if folder
    }
  end


  def test2000_channel_create
		wafer_values = [{"2502J493KO"=>10, "2502J494KO"=>15, "2502J495KO"=>5, "2502J496KO"=>10, "2502J497KO"=>5, "2502J498KO"=>11}]
		mpd = "QA_PRERUN-PD.01"
		parameters = ["QA_PRERUN_900", "QA_PRERUN_901", "QA_PRERUN_902", "QA_PRERUN_903", "QA_PRERUN_904"]
    #
    # build and submit DCR with all parameters
    dcr = Space::DCR.new(:op=>mpd, :eqp=>@@eqp, :lot=>@@lot)
    wafer_values = Hash[*wafer_values[0].collect {|w,v| [w,v]}.flatten]
    dcr.add_parameters(parameters, wafer_values)
    res = $client.send_dcr(dcr, :export=>"log/#{__method__}.xml")
    nsamples = parameters.size * wafer_values.size
    assert $spctest.verify_dcr_accepted(res, nsamples), "DCR not submitted successfully"
    #
  end
  
  def test2001_send
		wafer_values = [
		  {"2502J499KO"=>10, "2502J500KO"=>15, "2502J501KO"=>5, "2502J502KO"=>10, "2502J503KO"=>5, "2502J504KO"=>11},
		  {"2502J505KO"=>10, "2502J506KO"=>15, "2502J507KO"=>5, "2502J508KO"=>10, "2502J509KO"=>5, "2502J510KO"=>11},
		  {"2502J511KO"=>10, "2502J512KO"=>15, "2502J513KO"=>5, "2502J514KO"=>10, "2502J515KO"=>5, "2502J516KO"=>11},
		  {"2502J517KO"=>10, "2502J518KO"=>15, "2502J519KO"=>5, "2502J520KO"=>10, "2502J521KO"=>5, "2502J522KO"=>11},
		  {"2502J523KO"=>10, "2502J524KO"=>15, "2502J525KO"=>5, "2502J526KO"=>10, "2502J527KO"=>5, "2502J528KO"=>11},
		  {"2502J529KO"=>10, "2502J530KO"=>15, "2502J531KO"=>5, "2502J532KO"=>10, "2502J533KO"=>5, "2502J534KO"=>11},
		  {"2502J535KO"=>10, "2502J536KO"=>15, "2502J537KO"=>5, "2502J538KO"=>10, "2502J539KO"=>5, "2502J540KO"=>11}
    ]
		mpd = "QA_PRERUN-PD.01"
		parameters = ["QA_PRERUN_900", "QA_PRERUN_901", "QA_PRERUN_902", "QA_PRERUN_903", "QA_PRERUN_904"]
    assert lot = $svtest.new_lot
    @@lots = [lot]
    wafer_values.count.times {|i|
      wafer_values[i].each_pair {|k,v|  wafer_values[i][k] = v + 2.0*(rand - 0.5)}
      # build and submit DCR with all parameters
      dcr = Space::DCR.new(:op=>mpd, :eqp=>@@eqp, :lot=>lot)
      wafer_values[i] = Hash*wafer_values[i].collect {|w,v| [w,v]}]
			$log.info "Wafervalues calculated: #{wafer_values[i].inspect}"
      dcr.add_parameters(parameters, wafer_values[i])
      res = $client.send_dcr(dcr, :export=>"log/#{__method__}.xml")
      assert $spctest.verify_dcr_accepted(res, parameters.size * wafer_values[i].size), "DCR not submitted successfully"
    }

		timeout = 5*60+1
    $log.info "need to wait for #{timeout}s..."
    sleep timeout
    assert lot = $svtest.new_lot
    @@lots << lot
    # build and submit DCR with all parameters
    dcr = Space::DCR.new(:op=>mpd, :eqp=>@@eqp, :lot=>lot)
    wafer_values[6] = Hash[wafer_values[6].collect {|w,v| [w,v]}]
    dcr.add_parameters(parameters, wafer_values[6])
    res = $client.send_dcr(dcr, :export=>"log/#{__method__}.xml")
    nsamples = parameters.size * wafer_values[6].size
    assert $spctest.verify_dcr_accepted(res, nsamples), "DCR not submitted successfully"

    folder = $lds.folder(@@autocreated_qa)
    parameters.each do |p|
      ch = folder.spc_channel(:parameter=>p)
      ref_ch = Space::SpcApi::ChannelData.new(nil, nil, "Auto-created using template: _Template_QA_PRERUN", p)
      assert $spctest.verify_channel(ch, ref_ch, dcr, wafer_values[6], :nsamples=>2, :last_sample_only=>true), "wrong channel data"
      s = ch.samples(:ckc=>true)[-1]
      limits = s.limits
      assert_not_nil limits["MEAN_VALUE_UCL"], "no limit UCL for parameter #{p}, sample #{s}"
      assert_not_nil limits["MEAN_VALUE_CENTER"], "no limit CENTER for parameter #{p}, sample #{s}"
      assert_not_nil limits["MEAN_VALUE_LCL"], "no limit LCL for parameter #{p}, sample #{s}"
    end
  end
  
  def test2002_ooc_count
    wafer_values = {"2502J493KO"=>10, "2502J494KO"=>12, "2502J495KO"=>14, "2502J496KO"=>15, "2502J497KO"=>16, "2502J498KO"=>25}
		mpd = "QA_PRERUN-PD.01"
		parameters = ["QA_PRERUN_900", "QA_PRERUN_901", "QA_PRERUN_902", "QA_PRERUN_903", "QA_PRERUN_904"]
    # build and submit DCR with all parameters
    dcr = Space::DCR.new(:op=>mpd, :eqp=>@@eqp, :lot=>@@lot)
    dcr.add_parameters(parameters, wafer_values)
    res = $client.send_dcr(dcr, :export=>"log/#{__method__}.xml")
    nsamples = parameters.size * wafer_values.size
    assert $spctest.verify_dcr_accepted(res, nsamples, :nooc=>4*parameters.count), "DCR not submitted successfully"

    assert $spctest.verify_futurehold(@@lot, "QA_PRERUN", "(Mean above control limit)", :expected_holds=>parameters.count), "expected CA lothold"
    
    folder = $lds.folder(@@autocreated_qa)
    parameters.each do |p|
      ch = folder.spc_channel(:parameter=>p)
      s = ch.samples(:ckc=>true)[-1]
      assert $spctest.verify_ecomment([ch], "LOT_HELD: #{@@lot}: reason={X-S"), "external comment not found"
      assert s.assign_ca($spc.corrective_action("ReleaseLotHold")), "failed to release lot"
    end
    assert $spctest.verify_lothold_released(@@lot, nil), "no further holds expected"
  end
  
end
