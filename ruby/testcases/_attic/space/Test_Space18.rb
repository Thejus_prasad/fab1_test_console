=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: U. Koerner, D. Steeger, 2012-02-14
=end

require_relative 'Test_Space_Setup'


# Test Space/SIL DataCheck
class Test_Space18 < Test_Space_Setup
  @@pd_ecp = 'QA-PARAM-CHECK-ECP.01'
  @@missingparametercheck_inline_active = nil
  @@missingparametercheck_inline_trace = nil
  @@missingparametercheck_inline_notification = nil
  @@missingparametercheck_setup_active = nil
  @@missingparametercheck_setup_trace = nil
  @@missingparametercheck_setup_notification = nil
  @@missingparametercheck_monitoringspeclimits_wildcards_enabled = nil
  @@missingparametercheck_inline_qa_active = nil
  @@missingparametercheck_setup_qa_active = nil
  @@missingparametercheck_monitoringspeclimits_wildcards_enabled = nil
  @@autoqual_missingparametercheck_monitoringspeclimits_wildcards_enabled = nil

  @@missing_param_error_code = 'Error code : MISSING_DCR_PARAMETERS'
  @@no_param_uploaded_error_code = 'No parameter is uploaded to SPACE - a missing parameter check is not possible'
  @@missing_param_lothold_comment = 'Missing parameter in DCR'

  def setup
    super
    return unless $lds && self.class.class_variable_defined?(:@@lot) # bypass test0000_setup
    assert_equal 0, $sv.eqp_cleanup(@@eqp)
    assert_equal 0, $sv.lot_cleanup(@@lot)
    #
    [$lds, $lds_setup].each {|lds|
      lds.all_spc_channels('QA_PARAM_90*').each {|ch| assert ch.delete}
      folder = lds.folder(@@autocreated_qa)
      assert folder.delete_channels, 'error cleaning channels' if folder
    }
  end

  def test1800_setup
    $setup_ok = false
    #
    props = $spctest.silserver.read_jobprops
    @@datacheck_parameter_inline_enabled = props[:'datacheck.parameter.inline.enabled']
    @@datacheck_parameter_setup_enabled = props[:'datacheck.parameter.setup.enabled']

    @@missingparametercheck_inline_active = props[:'missingparametercheck.inline.active']
    @@missingparametercheck_inline_trace = props[:'missingparametercheck.inline.trace']
    @@missingparametercheck_inline_notification = props[:'missingparametercheck.inline.notification']
    @@missingparametercheck_setup_active = props[:'missingparametercheck.setup.active']
    @@missingparametercheck_setup_trace = props[:'missingparametercheck.setup.trace']
    @@missingparametercheck_setup_notification = props[:'missingparametercheck.setup.notification']
    @@missingparametercheck_monitoringspeclimits_wildcards_enabled = props[:'missingparametercheck.monitoringspeclimits.wildcards.enabled']
    @@missingparametercheck_inline_qa_active = props[:'missingparametercheck.inline.qa.active']
    @@missingparametercheck_setup_qa_active = props[:'missingparametercheck.setup.qa.active']
    @@missingparametercheck_monitoringspeclimits_wildcards_enabled = props[:'missingparametercheck.monitoringspeclimits.wildcards.enabled']
    @@autoqual_missingparametercheck_monitoringspeclimits_wildcards_enabled = props[:'autoqual.missingparametercheck.monitoringspeclimits.wildcards.enabled']
    #
    assert $spctest.verify_sil_properties, 'properties have changed'
    #
    $setup_ok = true
  end

  def test1801_config_module_template
    setup_channel_and_verify($lds_setup)
  end

  # PCP (Spec Limits) Test Cases (1802x)
  def test1802a_config_missing_parameter_wafer_active
    #Spec: C05-00001650
    parameters = ['QA_PARAM_900_TC1802a', 'QA_PARAM_901_TC1802a', 'QA_PARAM_902_TC1802a', 'QA_PARAM_903_TC1802a', 'QA_PARAM_904_TC1802a']
    wafer_values = {'2502J599KO'=>18048, '2502J600KO'=>18049, '2502J601KO'=>18050, '2502J602KO'=>18051, '2502J603KO'=>18052, '2502J604KO'=>18053}

    dcr = send_missing_parameter(parameters[1..-1], {}, $lds_setup, op: 'QA-PARAM-CHECK-TC1802a.01', wafer_values: wafer_values).first
    #
    check_missing_param_activities_config(dcr, parameters: parameters, wafer_values: wafer_values, parameter_missing: true)
  end

  def test1802b_config_missing_parameter_wafer_active_spacescenario_CALCULATION
    #Spec: C05-00001650
    #ignorecontrolplan.spacescenariosettings.enabled: 'true'
    #speclimits.ignorecontrolplan.spacescenariosettings: '%MANUAL_SPECLIMIT_SPACE%,%CALCULATION%,%IGNORE_ALL_PARAMETER_CHECK%,%BACKGROUND%,%Background%,%ManSL%'
    #missingparametercheck.ignorecontrolplan.spacescenariosettings: 'NoMP','MANUAL_SPECLIMIT_SPACE','CALCULATION','IGNORE_ALL_PARAMETER_CHECK','BACKGROUND','B','Background'
    #param QA_PARAM_900_TC1802b space scenario set to CALCULATION - no missing parameter expected
    parameters = ['QA_PARAM_900_TC1802b', 'QA_PARAM_901_TC1802b', 'QA_PARAM_902_TC1802b', 'QA_PARAM_903_TC1802b', 'QA_PARAM_904_TC1802b']
    wafer_values = {'2502J599KO'=>18048, '2502J600KO'=>18049, '2502J601KO'=>18050, '2502J602KO'=>18051, '2502J603KO'=>18052, '2502J604KO'=>18053}

    dcr = send_missing_parameter(parameters[1..-1], {}, $lds_setup, op: 'QA-PARAM-CHECK-TC1802b.01', wafer_values: wafer_values).first
    #
    check_missing_param_activities_config(dcr, parameters: parameters, wafer_values: wafer_values, parameter_missing: false, aqv_result: 'success')
  end

  def test1802c_config_missing_parameter_wafer_active_spacescenario_notempty
    #Spec: C05-00001650
    #ignorecontrolplan.spacescenariosettings.enabled: 'true'
    #speclimits.ignorecontrolplan.spacescenariosettings: '%MANUAL_SPECLIMIT_SPACE%,%CALCULATION%,%IGNORE_ALL_PARAMETER_CHECK%,%BACKGROUND%,%Background%,%ManSL%'
    #missingparametercheck.ignorecontrolplan.spacescenariosettings: 'NoMP','MANUAL_SPECLIMIT_SPACE','CALCULATION','IGNORE_ALL_PARAMETER_CHECK','BACKGROUND','B','Background'
    #param QA_PARAM_900_TC1802c space scenario set to NoCC - missing parameter expected
    parameters = ['QA_PARAM_900_TC1802c', 'QA_PARAM_901_TC1802c', 'QA_PARAM_902_TC1802c', 'QA_PARAM_903_TC1802c', 'QA_PARAM_904_TC1802c']
    wafer_values = {'2502J599KO'=>18048, '2502J600KO'=>18049, '2502J601KO'=>18050, '2502J602KO'=>18051, '2502J603KO'=>18052, '2502J604KO'=>18053}

    dcr = send_missing_parameter(parameters[1..-1], {}, $lds_setup, op: 'QA-PARAM-CHECK-TC1802c.01', wafer_values: wafer_values).first
    #
    check_missing_param_activities_config(dcr, parameters: parameters, wafer_values: wafer_values, parameter_missing: true)
  end

  def test1802d_config_missing_parameter_wafer_active_spacescenario_B
    #Spec: C05-00001650
    #ignorecontrolplan.spacescenariosettings.enabled: 'true'
    #speclimits.ignorecontrolplan.spacescenariosettings: '%MANUAL_SPECLIMIT_SPACE%,%CALCULATION%,%IGNORE_ALL_PARAMETER_CHECK%,%BACKGROUND%,%Background%,%ManSL%'
    #missingparametercheck.ignorecontrolplan.spacescenariosettings: 'NoMP','MANUAL_SPECLIMIT_SPACE','CALCULATION','IGNORE_ALL_PARAMETER_CHECK','BACKGROUND','B','Background'
    #param QA_PARAM_900_TC1802d space scenario set to B - no missing parameter expected
    parameters = ['QA_PARAM_900_TC1802d', 'QA_PARAM_901_TC1802d', 'QA_PARAM_902_TC1802d', 'QA_PARAM_903_TC1802d', 'QA_PARAM_904_TC1802d']
    wafer_values = {'2502J599KO'=>18048, '2502J600KO'=>18049, '2502J601KO'=>18050, '2502J602KO'=>18051, '2502J603KO'=>18052, '2502J604KO'=>18053}

    dcr = send_missing_parameter(parameters[1..-1], {}, $lds_setup, op: 'QA-PARAM-CHECK-TC1802d.01', wafer_values: wafer_values).first
    #
    check_missing_param_activities_config(dcr, parameters: parameters, wafer_values: wafer_values, parameter_missing: false, aqv_result: 'success')
  end

  def test1802e_config_missing_parameter_wafer_active_spacescenario_BACKGROUND
    #Spec: C05-00001650
    #ignorecontrolplan.spacescenariosettings.enabled: 'true'
    #speclimits.ignorecontrolplan.spacescenariosettings: '%MANUAL_SPECLIMIT_SPACE%,%CALCULATION%,%IGNORE_ALL_PARAMETER_CHECK%,%BACKGROUND%,%Background%,%ManSL%'
    #missingparametercheck.ignorecontrolplan.spacescenariosettings: 'NoMP','MANUAL_SPECLIMIT_SPACE','CALCULATION','IGNORE_ALL_PARAMETER_CHECK','BACKGROUND','B','Background'
    #param QA_PARAM_900_TC1802e space scenario set to BACKGROUND - no missing parameter expected
    parameters = ['QA_PARAM_900_TC1802e', 'QA_PARAM_901_TC1802e', 'QA_PARAM_902_TC1802e', 'QA_PARAM_903_TC1802e', 'QA_PARAM_904_TC1802e']
    wafer_values = {'2502J599KO'=>18048, '2502J600KO'=>18049, '2502J601KO'=>18050, '2502J602KO'=>18051, '2502J603KO'=>18052, '2502J604KO'=>18053}

    dcr = send_missing_parameter(parameters[1..-1], {}, $lds_setup, op: 'QA-PARAM-CHECK-TC1802e.01', wafer_values: wafer_values).first
    #
    check_missing_param_activities_config(dcr, parameters: parameters, wafer_values: wafer_values, parameter_missing: false, aqv_result: 'success')
  end

  def test1802f_config_missing_parameter_wafer_active_spacescenario_criticality_B
    #Spec: C05-00001650
    #ignorecontrolplan.spacescenariosettings.enabled: 'true'
    #speclimits.ignorecontrolplan.spacescenariosettings: '%MANUAL_SPECLIMIT_SPACE%,%CALCULATION%,%IGNORE_ALL_PARAMETER_CHECK%,%BACKGROUND%,%Background%,%ManSL%'
    #missingparametercheck.ignorecontrolplan.spacescenariosettings: 'NoMP','MANUAL_SPECLIMIT_SPACE','CALCULATION','IGNORE_ALL_PARAMETER_CHECK','BACKGROUND','B','Background'
    #ignore.background.criticality: 'true'
    #ignore.criticality.values: B,BACKGROUND
    #no missing parameters expected
    parameters = ['QA_PARAM_900_TC1802f', 'QA_PARAM_901_TC1802f', 'QA_PARAM_902_TC1802f', 'QA_PARAM_903_TC1802f', 'QA_PARAM_904_TC1802f']
    wafer_values = {'2502J599KO'=>18048, '2502J600KO'=>18049, '2502J601KO'=>18050, '2502J602KO'=>18051, '2502J603KO'=>18052, '2502J604KO'=>18053}

    dcr = send_missing_parameter(parameters[1..-1], {}, $lds_setup, op: 'QA-PARAM-CHECK-TC1802f.01', wafer_values: wafer_values).first
    #
    check_missing_param_activities_config(dcr, parameters: parameters, wafer_values: wafer_values, parameter_missing: false, aqv_result: 'success')
  end

  # PCP (Spec Limits) Test Cases (1803x) - Multilot - using same params and ops like Single Lot TCs
  def test1803a_config_missing_parameter_wafer_active_multilot
    #Spec: C05-00001650
    parameters = ['QA_PARAM_900_TC1802a', 'QA_PARAM_901_TC1802a', 'QA_PARAM_902_TC1802a', 'QA_PARAM_903_TC1802a', 'QA_PARAM_904_TC1802a']
    wafer_values = {'2502J599KO'=>18048, '2502J600KO'=>18049, '2502J601KO'=>18050, '2502J602KO'=>18051, '2502J603KO'=>18052, '2502J604KO'=>18053}

    dcr, lot2 = send_missing_parameter(parameters[1..-1], {}, $lds_setup, op: 'QA-PARAM-CHECK-TC1802a.01', wafer_values: wafer_values, multilot: true)
    #
    check_missing_param_activities_config(dcr, parameters: parameters, wafer_values: wafer_values, parameter_missing: true, multilot: true, lot2: lot2)
  end

  def test1803b_config_missing_parameter_wafer_active_spacescenario_CALCULATION_multilot
    #Spec: C05-00001650
    #ignorecontrolplan.spacescenariosettings.enabled: 'true'
    #speclimits.ignorecontrolplan.spacescenariosettings: '%MANUAL_SPECLIMIT_SPACE%,%CALCULATION%,%IGNORE_ALL_PARAMETER_CHECK%,%BACKGROUND%,%Background%,%ManSL%'
    #missingparametercheck.ignorecontrolplan.spacescenariosettings: 'NoMP','MANUAL_SPECLIMIT_SPACE','CALCULATION','IGNORE_ALL_PARAMETER_CHECK','BACKGROUND','B','Background'
    #param QA_PARAM_900_TC1802b space scenario set to CALCULATION - no missing parameter expected
    parameters = ['QA_PARAM_900_TC1802b', 'QA_PARAM_901_TC1802b', 'QA_PARAM_902_TC1802b', 'QA_PARAM_903_TC1802b', 'QA_PARAM_904_TC1802b']
    wafer_values = {'2502J599KO'=>18048, '2502J600KO'=>18049, '2502J601KO'=>18050, '2502J602KO'=>18051, '2502J603KO'=>18052, '2502J604KO'=>18053}

    dcr, lot2 = send_missing_parameter(parameters[1..-1], {}, $lds_setup, op: 'QA-PARAM-CHECK-TC1802b.01', wafer_values: wafer_values, multilot: true)
    #
    check_missing_param_activities_config(dcr, parameters: parameters, wafer_values: wafer_values, parameter_missing: false, aqv_result: 'success', multilot: true, lot2: lot2)
  end

  def test1803c_config_missing_parameter_wafer_active_spacescenario_notempty_multilot
    #Spec: C05-00001650
    #ignorecontrolplan.spacescenariosettings.enabled: 'true'
    #speclimits.ignorecontrolplan.spacescenariosettings: '%MANUAL_SPECLIMIT_SPACE%,%CALCULATION%,%IGNORE_ALL_PARAMETER_CHECK%,%BACKGROUND%,%Background%,%ManSL%'
    #missingparametercheck.ignorecontrolplan.spacescenariosettings: 'NoMP','MANUAL_SPECLIMIT_SPACE','CALCULATION','IGNORE_ALL_PARAMETER_CHECK','BACKGROUND','B','Background'
    #param QA_PARAM_900_TC1802c space scenario set to NoCC - missing parameter expected
    parameters = ['QA_PARAM_900_TC1802c', 'QA_PARAM_901_TC1802c', 'QA_PARAM_902_TC1802c', 'QA_PARAM_903_TC1802c', 'QA_PARAM_904_TC1802c']
    wafer_values = {'2502J599KO'=>18048, '2502J600KO'=>18049, '2502J601KO'=>18050, '2502J602KO'=>18051, '2502J603KO'=>18052, '2502J604KO'=>18053}

    dcr, lot2 = send_missing_parameter(parameters[1..-1], {}, $lds_setup, op: 'QA-PARAM-CHECK-TC1802c.01', wafer_values: wafer_values, multilot: true)
    #
    check_missing_param_activities_config(dcr, parameters: parameters, wafer_values: wafer_values, parameter_missing: true, multilot: true, lot2: lot2)
  end

  def test1803d_config_missing_parameter_wafer_active_spacescenario_B_multilot
    #Spec: C05-00001650
    #ignorecontrolplan.spacescenariosettings.enabled: 'true'
    #speclimits.ignorecontrolplan.spacescenariosettings: '%MANUAL_SPECLIMIT_SPACE%,%CALCULATION%,%IGNORE_ALL_PARAMETER_CHECK%,%BACKGROUND%,%Background%,%ManSL%'
    #missingparametercheck.ignorecontrolplan.spacescenariosettings: 'NoMP','MANUAL_SPECLIMIT_SPACE','CALCULATION','IGNORE_ALL_PARAMETER_CHECK','BACKGROUND','B','Background'
    #param QA_PARAM_900_TC1802d space scenario set to B - no missing parameter expected
    parameters = ['QA_PARAM_900_TC1802d', 'QA_PARAM_901_TC1802d', 'QA_PARAM_902_TC1802d', 'QA_PARAM_903_TC1802d', 'QA_PARAM_904_TC1802d']
    wafer_values = {'2502J599KO'=>18048, '2502J600KO'=>18049, '2502J601KO'=>18050, '2502J602KO'=>18051, '2502J603KO'=>18052, '2502J604KO'=>18053}

    dcr, lot2 = send_missing_parameter(parameters[1..-1], {}, $lds_setup, op: 'QA-PARAM-CHECK-TC1802d.01', wafer_values: wafer_values, multilot: true)
    #
    check_missing_param_activities_config(dcr, parameters: parameters, wafer_values: wafer_values, parameter_missing: false, aqv_result: 'success', multilot: true, lot2: lot2)
  end

  def test1803e_config_missing_parameter_wafer_active_spacescenario_BACKGROUND_multilot
    #Spec: C05-00001650
    #ignorecontrolplan.spacescenariosettings.enabled: 'true'
    #speclimits.ignorecontrolplan.spacescenariosettings: '%MANUAL_SPECLIMIT_SPACE%,%CALCULATION%,%IGNORE_ALL_PARAMETER_CHECK%,%BACKGROUND%,%Background%,%ManSL%'
    #missingparametercheck.ignorecontrolplan.spacescenariosettings: 'NoMP','MANUAL_SPECLIMIT_SPACE','CALCULATION','IGNORE_ALL_PARAMETER_CHECK','BACKGROUND','B','Background'
    #param QA_PARAM_900_TC1802e space scenario set to BACKGROUND - no missing parameter expected
    parameters = ['QA_PARAM_900_TC1802e', 'QA_PARAM_901_TC1802e', 'QA_PARAM_902_TC1802e', 'QA_PARAM_903_TC1802e', 'QA_PARAM_904_TC1802e']
    wafer_values = {'2502J599KO'=>18048, '2502J600KO'=>18049, '2502J601KO'=>18050, '2502J602KO'=>18051, '2502J603KO'=>18052, '2502J604KO'=>18053}

    dcr, lot2 = send_missing_parameter(parameters[1..-1], {}, $lds_setup, op: 'QA-PARAM-CHECK-TC1802e.01', wafer_values: wafer_values, multilot: true)
    #
    check_missing_param_activities_config(dcr, parameters: parameters, wafer_values: wafer_values, parameter_missing: false, aqv_result: 'success', multilot: true, lot2: lot2)
  end

  def test1803f_config_missing_parameter_wafer_active_spacescenario_criticality_B_multilot
    #Spec: C05-00001650
    #ignorecontrolplan.spacescenariosettings.enabled: 'true'
    #speclimits.ignorecontrolplan.spacescenariosettings: '%MANUAL_SPECLIMIT_SPACE%,%CALCULATION%,%IGNORE_ALL_PARAMETER_CHECK%,%BACKGROUND%,%Background%,%ManSL%'
    #missingparametercheck.ignorecontrolplan.spacescenariosettings: 'NoMP','MANUAL_SPECLIMIT_SPACE','CALCULATION','IGNORE_ALL_PARAMETER_CHECK','BACKGROUND','B','Background'
    #ignore.background.criticality: 'true'
    #ignore.criticality.values: B,BACKGROUND
    #no missing parameters expected
    parameters = ['QA_PARAM_900_TC1802f', 'QA_PARAM_901_TC1802f', 'QA_PARAM_902_TC1802f', 'QA_PARAM_903_TC1802f', 'QA_PARAM_904_TC1802f']
    wafer_values = {'2502J599KO'=>18048, '2502J600KO'=>18049, '2502J601KO'=>18050, '2502J602KO'=>18051, '2502J603KO'=>18052, '2502J604KO'=>18053}

    dcr, lot2 = send_missing_parameter(parameters[1..-1], {}, $lds_setup, op: 'QA-PARAM-CHECK-TC1802f.01', wafer_values: wafer_values, multilot: true)
    #
    check_missing_param_activities_config(dcr, parameters: parameters, wafer_values: wafer_values, parameter_missing: false, aqv_result: 'success', multilot: true, lot2: lot2)
  end

  def test1806a_config_missing_parameter_wafer_active_allarenocharting
    dcr = send_missing_parameter(@@parameters[1..-1], [], $lds_setup, nochartingparams: @@parameters[1..-1]).first
    #
    check_missing_param_activities_config(dcr, parameter_missing: false, aqv_result: 'fail', aqv_comment: @@no_param_uploaded_error_code)
  end

  def test1806b_config_missing_parameter_wafer_active_missingisnocharting
    dcr = send_missing_parameter(@@parameters[1..-1], [], $lds_setup, nochartingparams: [@@parameters[0]]).first
    #
    check_missing_param_activities_config(dcr, parameter_missing: true)
  end

  def test1806c_config_missing_parameter_wafer_active_missingisstring
    dcr = send_missing_parameter(@@parameters[1..-1], [], $lds_setup, stringparams: [@@parameters[0]]).first
    #
    check_missing_param_activities_config(dcr, parameter_missing: true)
  end

  def test1806d_config_missing_parameter_wafer_nocharting_string   # must behave like 1803a
    dcr = send_missing_parameter([], [], $lds_setup, nochartingparams: @@parameters[1..-1], stringparams: [@@parameters[0]]).first
    #
    check_missing_param_activities_config(dcr, parameter_missing: false, aqv_result: 'fail', aqv_comment: @@no_param_uploaded_error_code)
  end

  def test1808_config_missing_parameter_lot_active
    dcr = send_missing_parameter([], @@parameters[1..-1], $lds_setup).first
    #
    check_missing_param_activities_config(dcr, parameter_missing: true)
  end

  def test1809_config_missing_parameter_lot_wafer_active
    dcr = send_missing_parameter(@@parameters[1..-2], @@parameters[-1..-1], $lds_setup).first
    #
    check_missing_param_activities_config(dcr, parameter_missing: true)
  end

  # ECP (Monitoring Spec Limits) Test Cases (1810x)
  def test1810a_config_missing_parameter_wafer_active
    #Spec: C05-00001658
    parameters = ['QA_PARAM_900_TC1810a', 'QA_PARAM_901_TC1810a', 'QA_PARAM_902_TC1810a', 'QA_PARAM_903_TC1810a', 'QA_PARAM_904_TC1810a']
    wafer_values = {'2502J599KO'=>18048, '2502J600KO'=>18049, '2502J601KO'=>18050, '2502J602KO'=>18051, '2502J603KO'=>18052, '2502J604KO'=>18053}

    dcr = send_missing_parameter(parameters[1..-1], {}, $lds_setup, op: 'QA-PARAM-CHECK-TC1810a.01', wafer_values: wafer_values).first
    #
    check_missing_param_activities_config(dcr, parameters: parameters, wafer_values: wafer_values, parameter_missing: true)
  end

  def test1810b_config_missing_parameter_wafer_active_spacescenario_CALCULATION
    #Spec: C05-00001658
    #ignorecontrolplan.spacescenariosettings.enabled: 'true'
    #speclimits.ignorecontrolplan.spacescenariosettings: '%MANUAL_SPECLIMIT_SPACE%,%CALCULATION%,%IGNORE_ALL_PARAMETER_CHECK%,%BACKGROUND%,%Background%,%ManSL%'
    #missingparametercheck.ignorecontrolplan.spacescenariosettings: 'NoMP','MANUAL_SPECLIMIT_SPACE','CALCULATION','IGNORE_ALL_PARAMETER_CHECK','BACKGROUND','B','Background'
    #param QA_PARAM_900_TC1810b space scenario set to CALCULATION - no missing parameter expected
    parameters = ['QA_PARAM_900_TC1810b', 'QA_PARAM_901_TC1810b', 'QA_PARAM_902_TC1810b', 'QA_PARAM_903_TC1810b', 'QA_PARAM_904_TC1810b']
    wafer_values = {'2502J599KO'=>18048, '2502J600KO'=>18049, '2502J601KO'=>18050, '2502J602KO'=>18051, '2502J603KO'=>18052, '2502J604KO'=>18053}

    dcr = send_missing_parameter(parameters[1..-1], {}, $lds_setup, op: 'QA-PARAM-CHECK-TC1810b.01', wafer_values: wafer_values).first
    #
    check_missing_param_activities_config(dcr, parameters: parameters, wafer_values: wafer_values, parameter_missing: false, aqv_result: 'success')
  end

  def test1810c_config_missing_parameter_wafer_active_spacescenario_notempty
    #Spec: C05-00001658
    #ignorecontrolplan.spacescenariosettings.enabled: 'true'
    #speclimits.ignorecontrolplan.spacescenariosettings: '%MANUAL_SPECLIMIT_SPACE%,%CALCULATION%,%IGNORE_ALL_PARAMETER_CHECK%,%BACKGROUND%,%Background%,%ManSL%'
    #missingparametercheck.ignorecontrolplan.spacescenariosettings: 'NoMP','MANUAL_SPECLIMIT_SPACE','CALCULATION','IGNORE_ALL_PARAMETER_CHECK','BACKGROUND','B','Background'
    #param QA_PARAM_900_TC1810c space scenario set to NoCC - missing parameter expected
    parameters = ['QA_PARAM_900_TC1810c', 'QA_PARAM_901_TC1810c', 'QA_PARAM_902_TC1810c', 'QA_PARAM_903_TC1810c', 'QA_PARAM_904_TC1810c']
    wafer_values = {'2502J599KO'=>18048, '2502J600KO'=>18049, '2502J601KO'=>18050, '2502J602KO'=>18051, '2502J603KO'=>18052, '2502J604KO'=>18053}

    dcr = send_missing_parameter(parameters[1..-1], {}, $lds_setup, op: 'QA-PARAM-CHECK-TC1810c.01', wafer_values: wafer_values).first
    #
    check_missing_param_activities_config(dcr, parameters: parameters, wafer_values: wafer_values, parameter_missing: true)
  end

  def test1810d_config_missing_parameter_wafer_active_spacescenario_NoMP
    #Spec: C05-00001658
    #ignorecontrolplan.spacescenariosettings.enabled: 'true'
    #speclimits.ignorecontrolplan.spacescenariosettings: '%MANUAL_SPECLIMIT_SPACE%,%CALCULATION%,%IGNORE_ALL_PARAMETER_CHECK%,%BACKGROUND%,%Background%,%ManSL%'
    #missingparametercheck.ignorecontrolplan.spacescenariosettings: 'NoMP','MANUAL_SPECLIMIT_SPACE','CALCULATION','IGNORE_ALL_PARAMETER_CHECK','BACKGROUND','B','Background'
    #param QA_PARAM_900_TC1810d space scenario set to NoMP - no missing parameter expected
    parameters = ['QA_PARAM_900_TC1810d', 'QA_PARAM_901_TC1810d', 'QA_PARAM_902_TC1810d', 'QA_PARAM_903_TC1810d', 'QA_PARAM_904_TC1810d']
    wafer_values = {'2502J599KO'=>18048, '2502J600KO'=>18049, '2502J601KO'=>18050, '2502J602KO'=>18051, '2502J603KO'=>18052, '2502J604KO'=>18053}

    dcr = send_missing_parameter(parameters[1..-1], {}, $lds_setup, op: 'QA-PARAM-CHECK-TC1810d.01', wafer_values: wafer_values).first
    #
    check_missing_param_activities_config(dcr, parameters: parameters, wafer_values: wafer_values, parameter_missing: false, aqv_result: 'success')
  end

  def test1810e_config_missing_parameter_wafer_active_spacescenario_BACKGROUND
    #Spec: C05-00001658
    #ignorecontrolplan.spacescenariosettings.enabled: 'true'
    #speclimits.ignorecontrolplan.spacescenariosettings: '%MANUAL_SPECLIMIT_SPACE%,%CALCULATION%,%IGNORE_ALL_PARAMETER_CHECK%,%BACKGROUND%,%Background%,%ManSL%'
    #missingparametercheck.ignorecontrolplan.spacescenariosettings: 'NoMP','MANUAL_SPECLIMIT_SPACE','CALCULATION','IGNORE_ALL_PARAMETER_CHECK','BACKGROUND','B','Background'
    #param QA_PARAM_900_TC1810e space scenario set to BACKGROUND - no missing parameter expected
    parameters = ['QA_PARAM_900_TC1810e', 'QA_PARAM_901_TC1810e', 'QA_PARAM_902_TC1810e', 'QA_PARAM_903_TC1810e', 'QA_PARAM_904_TC1810e']
    wafer_values = {'2502J599KO'=>18048, '2502J600KO'=>18049, '2502J601KO'=>18050, '2502J602KO'=>18051, '2502J603KO'=>18052, '2502J604KO'=>18053}

    dcr = send_missing_parameter(parameters[1..-1], {}, $lds_setup, op: 'QA-PARAM-CHECK-TC1810e.01', wafer_values: wafer_values).first
    #
    check_missing_param_activities_config(dcr, parameters: parameters, wafer_values: wafer_values, parameter_missing: false, aqv_result: 'success')
  end

  def test1810f_config_missing_parameter_wafer_active_criticality_B
    #Spec: C05-00001658
    #ignorecontrolplan.spacescenariosettings.enabled: 'true'
    #speclimits.ignorecontrolplan.spacescenariosettings: '%MANUAL_SPECLIMIT_SPACE%,%CALCULATION%,%IGNORE_ALL_PARAMETER_CHECK%,%BACKGROUND%,%Background%,%ManSL%'
    #missingparametercheck.ignorecontrolplan.spacescenariosettings: 'NoMP','MANUAL_SPECLIMIT_SPACE','CALCULATION','IGNORE_ALL_PARAMETER_CHECK','BACKGROUND','B','Background'
    #ignore.background.criticality: 'true'
    #ignore.criticality.values: B,BACKGROUND
    #no missing parameters expected
    parameters = ['QA_PARAM_900_TC1810f', 'QA_PARAM_901_TC1810f', 'QA_PARAM_902_TC1810f', 'QA_PARAM_903_TC1810f', 'QA_PARAM_904_TC1810f']
    wafer_values = {'2502J599KO'=>18048, '2502J600KO'=>18049, '2502J601KO'=>18050, '2502J602KO'=>18051, '2502J603KO'=>18052, '2502J604KO'=>18053}

    dcr = send_missing_parameter(parameters[1..-1], {}, $lds_setup, op: 'QA-PARAM-CHECK-TC1810f.01', wafer_values: wafer_values).first
    #
    check_missing_param_activities_config(dcr, parameters: parameters, wafer_values: wafer_values, parameter_missing: false, aqv_result: 'success')
  end

  def test1810g_config_missing_parameter_wafer_active_wildcard
    #Spec: C05-00001658
    #ignorecontrolplan.spacescenariosettings.enabled: 'true'
    #speclimits.ignorecontrolplan.spacescenariosettings: '%MANUAL_SPECLIMIT_SPACE%,%CALCULATION%,%IGNORE_ALL_PARAMETER_CHECK%,%BACKGROUND%,%Background%,%ManSL%'
    #missingparametercheck.ignorecontrolplan.spacescenariosettings: 'NoMP','MANUAL_SPECLIMIT_SPACE','CALCULATION','IGNORE_ALL_PARAMETER_CHECK','BACKGROUND','B','Background'
    #missingparametercheck.monitoringspeclimits.wildcards.enabled: 'true'
    #autoqual.missingparametercheck.monitoringspeclimits.wildcards.enabled: 'false'
    #different entries for ptool with and without wildcard
    #only one parameter without ptool entry is sent
    #missing param for all params (incl. *) (log, derdack, hold) expected if missingparametercheck.monitoringspeclimits.wildcards.enabled: 'true'
    #missing param for all params (incl. *) (aqv) expected if autoqual.missingparametercheck.monitoringspeclimits.wildcards.enabled: 'true'
    #if config switches = false -> missing param checks for params without wildcards expected only
    #
    $log.info ' ~?~?~ Please test each *wildcard* combination and check details manually! ~?~?~ '
    parameters = ['QA_PARAM_900_TC1810g', 'QA_PARAM_901_TC1810g', 'QA_PARAM_902_TC1810g', 'QA_PARAM_903_TC1810g', 'QA_PARAM_904_TC1810g']
    wafer_values = {'2502J599KO'=>18048, '2502J600KO'=>18049, '2502J601KO'=>18050, '2502J602KO'=>18051, '2502J603KO'=>18052, '2502J604KO'=>18053}

    dcr = send_missing_parameter([parameters[-1]], {}, $lds_setup, op: 'QA-PARAM-CHECK-TC1810g.01', wafer_values: wafer_values).first
    #
    check_missing_param_activities_config(dcr, parameters: parameters, wafer_values: wafer_values, parameter_missing: true, aqv_result: 'fail')
  end

  def test1810h_config_missing_parameter_lot_active_wildcard
    #Spec: C05-00001658
    #ignorecontrolplan.spacescenariosettings.enabled: 'true'
    #speclimits.ignorecontrolplan.spacescenariosettings: '%MANUAL_SPECLIMIT_SPACE%,%CALCULATION%,%IGNORE_ALL_PARAMETER_CHECK%,%BACKGROUND%,%Background%,%ManSL%'
    #missingparametercheck.ignorecontrolplan.spacescenariosettings: 'NoMP','MANUAL_SPECLIMIT_SPACE','CALCULATION','IGNORE_ALL_PARAMETER_CHECK','BACKGROUND','B','Background'
    #missingparametercheck.monitoringspeclimits.wildcards.enabled: 'true'
    #autoqual.missingparametercheck.monitoringspeclimits.wildcards.enabled: 'false'
    #different entries for ptool with and without wildcard
    #only one parameter without ptool entry is sent
    #missing param for all params (incl. *) (log, derdack, hold) expected if missingparametercheck.monitoringspeclimits.wildcards.enabled: 'true'
    #missing param for all params (incl. *) (aqv) expected if autoqual.missingparametercheck.monitoringspeclimits.wildcards.enabled: 'true'
    #if config switches = false -> missing param checks for params without wildcards expected only
    #
    $log.info ' ~?~?~ Please test each *wildcard* combination and check details manually! ~?~?~ '
    parameters = ['QA_PARAM_900_TC1810g', 'QA_PARAM_901_TC1810g', 'QA_PARAM_902_TC1810g', 'QA_PARAM_903_TC1810g', 'QA_PARAM_904_TC1810g']
    wafer_values = {'2502J599KO'=>18048, '2502J600KO'=>18049, '2502J601KO'=>18050, '2502J602KO'=>18051, '2502J603KO'=>18052, '2502J604KO'=>18053}

    dcr = send_missing_parameter({}, [parameters[-1]], $lds_setup, op: 'QA-PARAM-CHECK-TC1810g.01', wafer_values: wafer_values, lot_parameter_value: 18050.0).first
    #
    check_missing_param_activities_config(dcr, parameters: parameters, wafer_values: wafer_values, parameter_missing: true, aqv_result: 'fail')
  end

  def test1810i_config_missing_parameter_wafer_lot_active_wildcard
    #Spec: C05-00001658
    #ignorecontrolplan.spacescenariosettings.enabled: 'true'
    #speclimits.ignorecontrolplan.spacescenariosettings: '%MANUAL_SPECLIMIT_SPACE%,%CALCULATION%,%IGNORE_ALL_PARAMETER_CHECK%,%BACKGROUND%,%Background%,%ManSL%'
    #missingparametercheck.ignorecontrolplan.spacescenariosettings: 'NoMP','MANUAL_SPECLIMIT_SPACE','CALCULATION','IGNORE_ALL_PARAMETER_CHECK','BACKGROUND','B','Background'
    #missingparametercheck.monitoringspeclimits.wildcards.enabled: 'true'
    #autoqual.missingparametercheck.monitoringspeclimits.wildcards.enabled: 'false'
    #different entries for ptool with and without wildcard
    #only one parameter without ptool entry is sent
    #missing param for all params (incl. *) (log, derdack, hold) expected if missingparametercheck.monitoringspeclimits.wildcards.enabled: 'true'
    #missing param for all params (incl. *) (aqv) expected if autoqual.missingparametercheck.monitoringspeclimits.wildcards.enabled: 'true'
    #if config switches = false -> missing param checks for params without wildcards expected only
    #
    $log.info ' ~?~?~ Please test each *wildcard* combination and check details manually! ~?~?~ '
    parameters = ['QA_PARAM_900_TC1810g', 'QA_PARAM_901_TC1810g', 'QA_PARAM_902_TC1810g', 'QA_PARAM_903_TC1810g', 'QA_PARAM_904_TC1810g']
    wafer_values = {'2502J599KO'=>18048, '2502J600KO'=>18049, '2502J601KO'=>18050, '2502J602KO'=>18051, '2502J603KO'=>18052, '2502J604KO'=>18053}

    # aqv success in case of parameters[0] and fail in case of any other param
    dcr = send_missing_parameter([parameters[1]], [parameters[-1]], $lds_setup, op: 'QA-PARAM-CHECK-TC1810g.01', wafer_values: wafer_values, lot_parameter_value: 18050.0).first
    #
    check_missing_param_activities_config(dcr, parameters: parameters, wafer_values: wafer_values, parameter_missing: true, aqv_result: 'fail')
  end

  def test1810j_config_missing_parameter_1wafer_active_wildcard
    #Spec: C05-00001658
    #ignorecontrolplan.spacescenariosettings.enabled: 'true'
    #speclimits.ignorecontrolplan.spacescenariosettings: '%MANUAL_SPECLIMIT_SPACE%,%CALCULATION%,%IGNORE_ALL_PARAMETER_CHECK%,%BACKGROUND%,%Background%,%ManSL%'
    #missingparametercheck.ignorecontrolplan.spacescenariosettings: 'NoMP','MANUAL_SPECLIMIT_SPACE','CALCULATION','IGNORE_ALL_PARAMETER_CHECK','BACKGROUND','B','Background'
    #missingparametercheck.monitoringspeclimits.wildcards.enabled: 'true'
    #autoqual.missingparametercheck.monitoringspeclimits.wildcards.enabled: 'false'
    #different entries for ptool with and without wildcard
    #only one parameter without ptool entry is sent
    #missing param for all params (incl. *) (log, derdack, hold) expected if missingparametercheck.monitoringspeclimits.wildcards.enabled: 'true'
    #missing param for all params (incl. *) (aqv) expected if autoqual.missingparametercheck.monitoringspeclimits.wildcards.enabled: 'true'
    #if config switches = false -> missing param checks for params without wildcards expected only
    #
    $log.info ' ~?~?~ Please test each *wildcard* combination and check details manually! ~?~?~ '
    parameters = ['QA_PARAM_900_TC1810g', 'QA_PARAM_901_TC1810g', 'QA_PARAM_902_TC1810g', 'QA_PARAM_903_TC1810g', 'QA_PARAM_904_TC1810g']
    wafer_values = {'2502J601KO'=>18050}

    dcr = send_missing_parameter([parameters[-1]], {}, $lds_setup, op: 'QA-PARAM-CHECK-TC1810g.01', wafer_values: wafer_values).first
    #
    check_missing_param_activities_config(dcr, parameters: parameters, wafer_values: wafer_values, parameter_missing: true, aqv_result: 'fail')
  end

  # ECP (Monitoring Spec Limits) Test Cases (1811x) - Multilot - using same params and ops like Single Lot TCs
  def test1811a_config_missing_parameter_wafer_active_multilot
    #Spec: C05-00001658
    parameters = ['QA_PARAM_900_TC1810a', 'QA_PARAM_901_TC1810a', 'QA_PARAM_902_TC1810a', 'QA_PARAM_903_TC1810a', 'QA_PARAM_904_TC1810a']
    wafer_values = {'2502J599KO'=>18048, '2502J600KO'=>18049, '2502J601KO'=>18050, '2502J602KO'=>18051, '2502J603KO'=>18052, '2502J604KO'=>18053}

    dcr, lot2 = send_missing_parameter(parameters[1..-1], {}, $lds_setup, op: 'QA-PARAM-CHECK-TC1810a.01', wafer_values: wafer_values, multilot: true)
    #
    check_missing_param_activities_config(dcr, parameters: parameters, wafer_values: wafer_values, parameter_missing: true, multilot: true, lot2: lot2)
  end

  def test1811b_config_missing_parameter_wafer_active_spacescenario_CALCULATION_multilot
    #Spec: C05-00001658
    #ignorecontrolplan.spacescenariosettings.enabled: 'true'
    #speclimits.ignorecontrolplan.spacescenariosettings: '%MANUAL_SPECLIMIT_SPACE%,%CALCULATION%,%IGNORE_ALL_PARAMETER_CHECK%,%BACKGROUND%,%Background%,%ManSL%'
    #missingparametercheck.ignorecontrolplan.spacescenariosettings: 'NoMP','MANUAL_SPECLIMIT_SPACE','CALCULATION','IGNORE_ALL_PARAMETER_CHECK','BACKGROUND','B','Background'
    #param QA_PARAM_900_TC1810b space scenario set to CALCULATION - no missing parameter expected
    parameters = ['QA_PARAM_900_TC1810b', 'QA_PARAM_901_TC1810b', 'QA_PARAM_902_TC1810b', 'QA_PARAM_903_TC1810b', 'QA_PARAM_904_TC1810b']
    wafer_values = {'2502J599KO'=>18048, '2502J600KO'=>18049, '2502J601KO'=>18050, '2502J602KO'=>18051, '2502J603KO'=>18052, '2502J604KO'=>18053}

    dcr, lot2 = send_missing_parameter(parameters[1..-1], {}, $lds_setup, op: 'QA-PARAM-CHECK-TC1810b.01', wafer_values: wafer_values, multilot: true)
    #
    check_missing_param_activities_config(dcr, parameters: parameters, wafer_values: wafer_values, parameter_missing: false, aqv_result: 'success', multilot: true, lot2: lot2)
  end

  def test1811c_config_missing_parameter_wafer_active_spacescenario_notempty_multilot
    #Spec: C05-00001658
    #ignorecontrolplan.spacescenariosettings.enabled: 'true'
    #speclimits.ignorecontrolplan.spacescenariosettings: '%MANUAL_SPECLIMIT_SPACE%,%CALCULATION%,%IGNORE_ALL_PARAMETER_CHECK%,%BACKGROUND%,%Background%,%ManSL%'
    #missingparametercheck.ignorecontrolplan.spacescenariosettings: 'NoMP','MANUAL_SPECLIMIT_SPACE','CALCULATION','IGNORE_ALL_PARAMETER_CHECK','BACKGROUND','B','Background'
    #param QA_PARAM_900_TC1810c space scenario set to NoCC - missing parameter expected
    parameters = ['QA_PARAM_900_TC1810c', 'QA_PARAM_901_TC1810c', 'QA_PARAM_902_TC1810c', 'QA_PARAM_903_TC1810c', 'QA_PARAM_904_TC1810c']
    wafer_values = {'2502J599KO'=>18048, '2502J600KO'=>18049, '2502J601KO'=>18050, '2502J602KO'=>18051, '2502J603KO'=>18052, '2502J604KO'=>18053}

    dcr, lot2 = send_missing_parameter(parameters[1..-1], {}, $lds_setup, op: 'QA-PARAM-CHECK-TC1810c.01', wafer_values: wafer_values, multilot: true)
    #
    check_missing_param_activities_config(dcr, parameters: parameters, wafer_values: wafer_values, parameter_missing: true, multilot: true, lot2: lot2)
  end

  def test1811d_config_missing_parameter_wafer_active_spacescenario_NoMP_multilot
    #Spec: C05-00001658
    #ignorecontrolplan.spacescenariosettings.enabled: 'true'
    #speclimits.ignorecontrolplan.spacescenariosettings: '%MANUAL_SPECLIMIT_SPACE%,%CALCULATION%,%IGNORE_ALL_PARAMETER_CHECK%,%BACKGROUND%,%Background%,%ManSL%'
    #missingparametercheck.ignorecontrolplan.spacescenariosettings: 'NoMP','MANUAL_SPECLIMIT_SPACE','CALCULATION','IGNORE_ALL_PARAMETER_CHECK','BACKGROUND','B','Background'
    #param QA_PARAM_900_TC1810d space scenario set to NoMP - no missing parameter expected
    parameters = ['QA_PARAM_900_TC1810d', 'QA_PARAM_901_TC1810d', 'QA_PARAM_902_TC1810d', 'QA_PARAM_903_TC1810d', 'QA_PARAM_904_TC1810d']
    wafer_values = {'2502J599KO'=>18048, '2502J600KO'=>18049, '2502J601KO'=>18050, '2502J602KO'=>18051, '2502J603KO'=>18052, '2502J604KO'=>18053}

    dcr, lot2 = send_missing_parameter(parameters[1..-1], {}, $lds_setup, op: 'QA-PARAM-CHECK-TC1810d.01', wafer_values: wafer_values, multilot: true)
    #
    check_missing_param_activities_config(dcr, parameters: parameters, wafer_values: wafer_values, parameter_missing: false, aqv_result: 'success', multilot: true, lot2: lot2)
  end

  def test1811e_config_missing_parameter_wafer_active_spacescenario_BACKGROUND_multilot
    #Spec: C05-00001658
    #ignorecontrolplan.spacescenariosettings.enabled: 'true'
    #speclimits.ignorecontrolplan.spacescenariosettings: '%MANUAL_SPECLIMIT_SPACE%,%CALCULATION%,%IGNORE_ALL_PARAMETER_CHECK%,%BACKGROUND%,%Background%,%ManSL%'
    #missingparametercheck.ignorecontrolplan.spacescenariosettings: 'NoMP','MANUAL_SPECLIMIT_SPACE','CALCULATION','IGNORE_ALL_PARAMETER_CHECK','BACKGROUND','B','Background'
    #param QA_PARAM_900_TC1810e space scenario set to BACKGROUND - no missing parameter expected
    parameters = ['QA_PARAM_900_TC1810e', 'QA_PARAM_901_TC1810e', 'QA_PARAM_902_TC1810e', 'QA_PARAM_903_TC1810e', 'QA_PARAM_904_TC1810e']
    wafer_values = {'2502J599KO'=>18048, '2502J600KO'=>18049, '2502J601KO'=>18050, '2502J602KO'=>18051, '2502J603KO'=>18052, '2502J604KO'=>18053}

    dcr, lot2 = send_missing_parameter(parameters[1..-1], {}, $lds_setup, op: 'QA-PARAM-CHECK-TC1810e.01', wafer_values: wafer_values, multilot: true)
    #
    check_missing_param_activities_config(dcr, parameters: parameters, wafer_values: wafer_values, parameter_missing: false, aqv_result: 'success', multilot: true, lot2: lot2)
  end

  def test1811f_config_missing_parameter_wafer_active_criticality_B_multilot
    #Spec: C05-00001658
    #ignorecontrolplan.spacescenariosettings.enabled: 'true'
    #speclimits.ignorecontrolplan.spacescenariosettings: '%MANUAL_SPECLIMIT_SPACE%,%CALCULATION%,%IGNORE_ALL_PARAMETER_CHECK%,%BACKGROUND%,%Background%,%ManSL%'
    #missingparametercheck.ignorecontrolplan.spacescenariosettings: 'NoMP','MANUAL_SPECLIMIT_SPACE','CALCULATION','IGNORE_ALL_PARAMETER_CHECK','BACKGROUND','B','Background'
    #ignore.background.criticality: 'true'
    #ignore.criticality.values: B,BACKGROUND
    #no missing parameters expected
    parameters = ['QA_PARAM_900_TC1810f', 'QA_PARAM_901_TC1810f', 'QA_PARAM_902_TC1810f', 'QA_PARAM_903_TC1810f', 'QA_PARAM_904_TC1810f']
    wafer_values = {'2502J599KO'=>18048, '2502J600KO'=>18049, '2502J601KO'=>18050, '2502J602KO'=>18051, '2502J603KO'=>18052, '2502J604KO'=>18053}

    dcr, lot2 = send_missing_parameter(parameters[1..-1], {}, $lds_setup, op: 'QA-PARAM-CHECK-TC1810f.01', wafer_values: wafer_values, multilot: true)
    #
    check_missing_param_activities_config(dcr, parameters: parameters, wafer_values: wafer_values, parameter_missing: false, aqv_result: 'success', multilot: true, lot2: lot2)
  end

  def test1811g_config_missing_parameter_wafer_active_multilot_first_lot_fail_second_lot_success
    #Spec: C05-00001658
    parameters = ['QA_PARAM_901_TC1810g', 'QA_PARAM_900_TC1810g', 'QA_PARAM_902_TC1810g', 'QA_PARAM_903_TC1810g', 'QA_PARAM_904_TC1810g']
    wafer_values = {'2502J599KO'=>18048, '2502J600KO'=>18049, '2502J601KO'=>18050, '2502J602KO'=>18051, '2502J603KO'=>18052, '2502J604KO'=>18053}

    dcr, lot2 = send_missing_parameter(parameters[1..-1], {}, $lds_setup, op: 'QA-PARAM-CHECK-TC1810g.01', wafer_values: wafer_values, multilot: true)
    #
    check_missing_param_activities_config(dcr, parameters: parameters, wafer_values: wafer_values, parameter_missing: true, multilot: true, lot2: lot2)
  end

  def test1820_inline_module_template
    folder = $lds.folder(@@autocreated_qa)
    folder.delete_channels if folder
    setup_channel_and_verify($lds)
  end

  def test1821_inline_missing_parameter_wafer_active
    dcr = send_missing_parameter(@@parameters[1..-1], [], $lds).first
    #
    check_missing_param_activities_inline(dcr, parameter_missing: true)
  end

  def test1823_inline_missing_parameter_lot_active
    dcr = send_missing_parameter([], @@parameters[1..-1], $lds).first
    #
    check_missing_param_activities_inline(dcr, parameter_missing: true)
  end

  def test1825_inline_missing_parameter_lot_wafer_active
    dcr = send_missing_parameter(@@parameters[1..-2], @@parameters[-1..-1], $lds).first
    #
    check_missing_param_activities_inline(dcr, parameter_missing: true)
  end

  def test1841_config_module_template_ecp
    return if $env == 'f8stag'
    parameters = @@parameters.collect {|p| p + '_ECP'}
    setup_channel_and_verify($lds_setup, parameters: parameters, op: @@pd_ecp)
  end

  def test1842_config_missing_parameter_wafer_active_ecp
    return if $env == 'f8stag'
    parameters = @@parameters.collect {|p| p + '_ECP'}
    dcr = send_missing_parameter(parameters[1..-1], [], $lds_setup, op: @@pd_ecp).first
    #
    check_missing_param_activities_config(dcr, parameters: parameters, parameter_missing: true)
  end

  def test1844_config_missing_parameter_lot_active_ecp
    return if $env == 'f8stag'
    parameters = @@parameters.collect {|p| p + '_ECP'}
    dcr = send_missing_parameter([], parameters[1..-1], $lds_setup, op: @@pd_ecp).first
    #
    check_missing_param_activities_config(dcr, parameters: parameters, parameter_missing: true)
  end

  def test1846_config_missing_parameter_lot_wafer_active_ecp
    return if $env == 'f8stag'
    parameters = @@parameters.collect {|p| p + '_ECP'}
    dcr = send_missing_parameter(parameters[1..-2], parameters[-1..-1], $lds_setup, op: @@pd_ecp).first
    #
    check_missing_param_activities_config(dcr, parameters: parameters, parameter_missing: true)
  end

  def test1860_inline_module_template_ecp
    return if $env == 'f8stag'
    parameters = @@parameters.collect {|p| p + '_ECP'}
    dcr = setup_channel_and_verify($lds, parameters: parameters, op: @@pd_ecp)
  end

  def test1861_inline_missing_parameter_wafer_active_ecp
    return if $env == 'f8stag'
    parameters = @@parameters.collect {|p| p + '_ECP'}
    dcr = send_missing_parameter(parameters[1..-1], [], $lds, op: @@pd_ecp).first
    #
    check_missing_param_activities_inline(dcr, parameters: parameters, parameter_missing: true)
  end

  def test1863_inline_missing_parameter_lot_active_ecp
    return if $env == 'f8stag'
    parameters = @@parameters.collect {|p| p + '_ECP'}
    dcr = send_missing_parameter([], parameters[1..-1], $lds, op: @@pd_ecp).first
    #
    check_missing_param_activities_inline(dcr, parameters: parameters, parameter_missing: true)
  end

  def test1865_inline_missing_parameter_lot_wafer_active_ecp
    return if $env == 'f8stag'
    parameters = @@parameters.collect {|p| p + '_ECP'}
    dcr = send_missing_parameter(parameters[1..-2], parameters[-1..-1], $lds, op: @@pd_ecp).first
    #
    check_missing_param_activities_inline(dcr, parameters: parameters, parameter_missing: true)
  end

  def test1870_inline_missingspeclimit_ecp_pcp_filled
    # parameters.missingspeclimit.upload.inline.enabled=true
    # parameters.missingspeclimit.upload.setup.enabled=true

    # b1) SpecLimit (PCP) und MontiringSpecLimits (ECP)
    # Param1 is defined in PCP (zTableSPCLimits01 setupFC) with LSL/Target/USL --> chart is filled
    # Param2 is defined in ECP (zTableSPCLimits02 setupFC) with LSL/Target/USL --> chart is filled
    # Template per entry in spec: _Template_QA_PARAMS_900
    return if $env == 'f8stag'
    parameters = ['QA_PARAM_900_TG18', 'QA_PARAM_900_TG18_ECP']
    params_and_limits = {parameters[0] => [1820,1850,1880], parameters[1] => [1800,1850,1899]}

    dcr = send_missing_parameter(parameters, {}, $lds, op: 'QA-PARAM-CHECK-TG18.01').first

    folder = $lds.folder(@@autocreated_qa)
    parameters.each {|p|
      ch = folder.spc_channel(parameter: p)
      ref_ch = Space::SpcApi::ChannelData.new(nil, nil, "Auto-created using template: #{@@params_template}", p)
      assert $spctest.verify_channel(ch, ref_ch, dcr, @@wafer_values, last_sample_only: true), 'wrong channel data'
      assert $spctest.verify_sample_speclimits(ch.samples, params_and_limits[p][0], params_and_limits[p][1], params_and_limits[p][2]), 'wrong spec limits'
    }
  end

  def test1871_inline_missingspeclimit_ecp_pcp_empty
    # parameters.missingspeclimit.upload.inline.enabled=true
    # parameters.missingspeclimit.upload.setup.enabled=true

    # b2)
    # Param1 is defined in PCP (zTableSPCLimits01 setupFC) with LSL/Target/USL EMPTY--> chart is filled
    # Param2 is deinfed in ECP (zTableSPCLimits02 setupFC) with LSL/Target/USL EMPTY --> chart is filled
    # Template per entry in spec: _Template_QA_PARAMS_900
    return if $env == 'f8stag'
    parameters = ['QA_PARAM_901_TG18', 'QA_PARAM_901_TG18_ECP']
    params_and_limits = {parameters[0] => [nil, nil, nil], parameters[1] => [nil, nil, nil]}

    dcr = send_missing_parameter(parameters, {}, $lds, op: 'QA-PARAM-CHECK-TG18.01').first

    folder = $lds.folder(@@autocreated_qa)
    parameters.each {|p|
      ch = folder.spc_channel(parameter: p)
      ref_ch = Space::SpcApi::ChannelData.new(nil, nil, "Auto-created using template: #{@@params_template}", p)
      assert $spctest.verify_channel(ch, ref_ch, dcr, @@wafer_values, last_sample_only: true), 'wrong channel data'
      assert $spctest.verify_sample_speclimits(ch.samples, params_and_limits[p][0], params_and_limits[p][1], params_and_limits[p][2]), 'wrong spec limits'
    }
  end

  def test1872_inline_missingspeclimit_ecp_pcp_filled_spacescenariofilled
    # parameters.missingspeclimit.upload.inline.enabled=true
    # parameters.missingspeclimit.upload.setup.enabled=true
    # b4) Space Scenario empty/filled --> same as b1/b2 - data should be charted - even when Space Scenario filled
    # Template per entry in spec: _Template_QA_PARAMS_900
    return if $env == 'f8stag'
    parameters = ['QA_PARAM_902_TG18', 'QA_PARAM_902_TG18_ECP']
    params_and_limits = {parameters[0] => [nil, nil, nil], parameters[1] => [nil, nil, nil]}

    dcr = send_missing_parameter(parameters, {}, $lds, op: 'QA-PARAM-CHECK-TG18.01').first

    folder = $lds.folder(@@autocreated_qa)
    parameters.each {|p|
      ch = folder.spc_channel(parameter: p)
      ref_ch = Space::SpcApi::ChannelData.new(nil, nil, "Auto-created using template: #{@@params_template}", p)
      assert $spctest.verify_channel(ch, ref_ch, dcr, @@wafer_values, last_sample_only: true), 'wrong channel data'
      assert $spctest.verify_sample_speclimits(ch.samples, params_and_limits[p][0], params_and_limits[p][1], params_and_limits[p][2]), 'wrong spec limits'
    }
  end

  def test1873_inline_missingspeclimit_ecp_pcp_empty_spacescenariofilled
    # parameters.missingspeclimit.upload.inline.enabled=true
    # parameters.missingspeclimit.upload.setup.enabled=true

    # b4) Space Scenario empty/filled --> same as b1/b2 - data should be charted - even when Space Scenario filled
    # Template per entry in spec: _Template_QA_PARAMS_900
    return if $env == 'f8stag'
    parameters = ['QA_PARAM_903_TG18', 'QA_PARAM_903_TG18_ECP']
    params_and_limits = {parameters[0] => [nil, nil, nil], parameters[1] => [nil, nil, nil]}

    dcr = send_missing_parameter(parameters, {}, $lds, op: 'QA-PARAM-CHECK-TG18.01').first

    folder = $lds.folder(@@autocreated_qa)
    parameters.each {|p|
      ch = folder.spc_channel(parameter: p)
      ref_ch = Space::SpcApi::ChannelData.new(nil, nil, "Auto-created using template: #{@@params_template}", p)
      assert $spctest.verify_channel(ch, ref_ch, dcr, @@wafer_values, last_sample_only: true), 'wrong channel data'
      assert $spctest.verify_sample_speclimits(ch.samples, params_and_limits[p][0], params_and_limits[p][1], params_and_limits[p][2]), 'wrong spec limits'
    }
  end

  def test1875_config_missingspeclimit_ecp_pcp_filled
    # parameters.missingspeclimit.upload.inline.enabled=true
    # parameters.missingspeclimit.upload.setup.enabled=true

    # b1) SpecLimit (PCP) und MontiringSpecLimits (ECP)
    # Param1 is deinfed in PCP (zTableSPCLimits01 setupFC) with LSL/Target/USL --> chart is filled
    # Param2 is deinfed in ECP (zTableSPCLimits02 setupFC) with LSL/Target/USL --> chart is filled
    # Template per entry in spec: _Template_QA_PARAMS_900
    return if $env == 'f8stag'
    parameters = ['QA_PARAM_900_TG18', 'QA_PARAM_900_TG18_ECP']
    params_and_limits = {parameters[0] => [1830,1850,1880], parameters[1] => [1810,1850,1899]}

    dcr = send_missing_parameter(parameters, {}, $lds_setup, op: 'QA-PARAM-CHECK-TG18.01').first

    folder = $lds_setup.folder(@@autocreated_qa)
    parameters.each {|p|
      ch = folder.spc_channel(parameter: p)
      ref_ch = Space::SpcApi::ChannelData.new(nil, nil, "Auto-created using template: #{@@params_template}", p)
      assert $spctest.verify_channel(ch, ref_ch, dcr, @@wafer_values, last_sample_only: true), 'wrong channel data'
      assert $spctest.verify_sample_speclimits(ch.samples, params_and_limits[p][0], params_and_limits[p][1], params_and_limits[p][2]), 'wrong spec limits'
    }
  end

  def test1876_config_missingspeclimit_ecp_pcp_empty
    # parameters.missingspeclimit.upload.inline.enabled=true
    # parameters.missingspeclimit.upload.setup.enabled=true

    # b2)
    # Param1 is defined in PCP (zTableSPCLimits01 setupFC) with LSL/Target/USL EMPTY--> chart is filled
    # Param2 is deinfed in ECP (zTableSPCLimits02 setupFC) with LSL/Target/USL EMPTY --> chart is filled
    # Template per entry in spec: _Template_QA_PARAMS_900
    return if $env == 'f8stag'
    parameters = ['QA_PARAM_901_TG18', 'QA_PARAM_901_TG18_ECP']
    params_and_limits = {parameters[0] => [nil, nil, nil], parameters[1] => [nil, nil, nil]}

    dcr = send_missing_parameter(parameters, {}, $lds_setup, op: 'QA-PARAM-CHECK-TG18.01').first

    folder = $lds_setup.folder(@@autocreated_qa)
    parameters.each {|p|
      ch = folder.spc_channel(parameter: p)
      ref_ch = Space::SpcApi::ChannelData.new(nil, nil, "Auto-created using template: #{@@params_template}", p)
      assert $spctest.verify_channel(ch, ref_ch, dcr, @@wafer_values, last_sample_only: true), 'wrong channel data'
      assert $spctest.verify_sample_speclimits(ch.samples, params_and_limits[p][0], params_and_limits[p][1], params_and_limits[p][2]), 'wrong spec limits'
    }
  end

  def test1877_config_missingspeclimit_ecp_pcp_filled_spacescenariofilled
    # parameters.missingspeclimit.upload.inline.enabled=true
    # parameters.missingspeclimit.upload.setup.enabled=true
    # b4) Space Scenario empty/filled --> same as b1/b2 - data should be charted - even when Space Scenario filled
    # Template per entry in spec: _Template_QA_PARAMS_900
    return if $env == 'f8stag'
    parameters = ['QA_PARAM_902_TG18', 'QA_PARAM_902_TG18_ECP']
    params_and_limits = {parameters[0] => [nil, nil, nil], parameters[1] => [nil, nil, nil]}

    dcr = send_missing_parameter(parameters, {}, $lds_setup, op: 'QA-PARAM-CHECK-TG18.01').first

    folder = $lds_setup.folder(@@autocreated_qa)
    parameters.each {|p|
      ch = folder.spc_channel(parameter: p)
      ref_ch = Space::SpcApi::ChannelData.new(nil, nil, "Auto-created using template: #{@@params_template}", p)
      assert $spctest.verify_channel(ch, ref_ch, dcr, @@wafer_values, last_sample_only: true), 'wrong channel data'
      assert $spctest.verify_sample_speclimits(ch.samples, params_and_limits[p][0], params_and_limits[p][1], params_and_limits[p][2]), 'wrong spec limits'
    }
  end

  def test1878_config_missingspeclimit_ecp_pcp_empty_spacescenariofilled
    # parameters.missingspeclimit.upload.inline.enabled=true
    # parameters.missingspeclimit.upload.setup.enabled=true

    # b4) Space Scenario empty/filled --> same as b1/b2 - data should be charted - even when Space Scenario filled
    # Template per entry in spec: _Template_QA_PARAMS_900
    return if $env == 'f8stag'
    parameters = ['QA_PARAM_903_TG18', 'QA_PARAM_903_TG18_ECP']
    params_and_limits = {parameters[0] => [nil, nil, nil], parameters[1] => [nil, nil, nil]}

    dcr = send_missing_parameter(parameters, {}, $lds_setup, op: 'QA-PARAM-CHECK-TG18.01').first

    folder = $lds_setup.folder(@@autocreated_qa)
    parameters.each {|p|
      ch = folder.spc_channel(parameter: p)
      ref_ch = Space::SpcApi::ChannelData.new(nil, nil, "Auto-created using template: #{@@params_template}", p)
      assert $spctest.verify_channel(ch, ref_ch, dcr, @@wafer_values, last_sample_only: true), 'wrong channel data'
      assert $spctest.verify_sample_speclimits(ch.samples, params_and_limits[p][0], params_and_limits[p][1], params_and_limits[p][2]), 'wrong spec limits'
    }
  end

  # default inline, setup possible if parameter true
  def setup_channel_and_verify(lds, params={})
    technology = (lds == $lds_setup) ? 'TEST' : '32NM'
    op = params[:op] || 'QA-PARAM-CHECK.01'
    parameters = params[:parameters] || @@parameters
    #
    # ensure module template exist
    assert $spctest.templates_exist(lds.folder(@@templates_folder), [@@params_template]), 'missing template'
    #
    # build and submit DCR
    dcr = Space::DCR.new(eqp: @@eqp, eqptype: 'PROCESS-CHAMBER', lot: @@lot, technology: technology, op: op)
    dcr.add_parameters(parameters, @@wafer_values)
    res = $client.send_dcr(dcr, export: "log/#{caller_locations(1,1)[0].label}_p.xml")
    assert $spctest.verify_dcr_accepted(res, parameters.size*@@wafer_values.size), 'DCR not submitted successfully'
    #
    # verify spc channels
    folder = lds.folder(@@autocreated_qa)
    parameters.each {|p|
      ch = folder.spc_channel(parameter: p)
      ref = {desc: "Auto-created using template: #{@@params_template}", parameter: p}
      assert $spctest.verify_channel(ch, ref, dcr, @@wafer_values, silent: true), 'wrong channel data'
      # ensure spec and sample limits are set
      ch.samples.each_with_index {|s,i|
        refute_nil s.specs['USL'], "no USL for parameter #{p}, sample #{i}"
        refute_nil s.specs['TARGET'], "no TARGET for parameter #{p}, sample #{i}"
        refute_nil s.specs['LSL'], "no LSL for parameter #{p}, sample #{i}"
        refute_nil s.limits['MEAN_VALUE_UCL'], "no limit UCL for parameter #{p}, sample #{i}"
        refute_nil s.limits['MEAN_VALUE_CENTER'], "no limit CENTER for parameter #{p}, sample #{i}"
        refute_nil s.limits['MEAN_VALUE_LCL'], "no limit LCL for parameter #{p}, sample #{i}"
      }
    }
  end

  def send_missing_parameter(wafer_params, lot_params, lds, params={})
    assert_equal 0, $sv.lot_cleanup(@@lot)
    op = params[:op] || 'QA-PARAM-CHECK.01'
    nochartingparams = params[:nochartingparams]
    stringparams = params[:stringparams]
    technology = (lds == $lds_setup) ? 'TEST' : '32NM'
    wafer_values = params[:wafer_values] || @@wafer_values
    lot_parameter_value = params[:lot_parameter_value] || 35.0
    #
    # build and submit DCR
    dcr = Space::DCR.new(eqp: @@eqp, eqptype: 'PROCESS-CHAMBER', lot: @@lot, technology: technology, op: op)
    if wafer_params == nochartingparams
      wafer_params = []
      dcr.add_parameters(nochartingparams, wafer_values, nocharting: true)
    else
      dcr.add_parameters(wafer_params, wafer_values)
      dcr.add_parameters(nochartingparams, wafer_values, nocharting: true) if nochartingparams
      dcr.add_parameters(stringparams, Hash[wafer_values.each_pair.collect {|k, v| [k, v.to_s]}]) if stringparams
    end
    dcr.add_parameters(lot_params, {@@lot=>lot_parameter_value}, reading_type: 'Lot')
    lotcount = 1
    lot2 = nil
    if params[:multilot]
      lotcount = 2
      lot2 = 'SIL' + @@sil_version.split('.').collect {|e| '%02d' % e}.join('') + '%02d' % @@test_run + '%02d' % 2 + '.000'
      assert lot2 = $svtest.new_lot(lot: lot2) unless $sv.lot_exists?(lot2)
      assert_equal 0, $sv.lot_cleanup(lot2)
      dcr.add_lot(lot2, technology: technology, op: op)
      wvalues = wafer_values.values
      wafer_values2 = {}
      lot_wafers = $sv.lot_info(lot2,:wafers=>true).wafers.take(wafer_values.count)
      lot_wafers.each_with_index {|w,j| wafer_values2[w.wafer] = wvalues[j]}
      dcr.add_parameters(wafer_params, wafer_values2)
      dcr.add_parameters(nochartingparams, wafer_values2, nocharting: true) if nochartingparams
      dcr.add_parameters(stringparams, Hash[wafer_values2.each_pair.collect {|k, v| [k, v.to_s]}]) if stringparams
      dcr.add_parameters(lot_params, {lot2=>lot_parameter_value}, reading_type: 'Lot')
    end
    res = $client.send_dcr(dcr, export: "log/#{caller_locations(1,1)[0].label}_p.xml")
    assert $spctest.verify_dcr_accepted(res, lotcount*wafer_params.size*wafer_values.size+lot_params.size), "DCR not submitted successfully; res: #{res.inspect}"
    #
    # verify spc channels
    folder = lds.folder(@@autocreated_qa)
    wafer_params.each_with_index {|p, i|
      ch = folder.spc_channel(parameter: p)
      assert_equal wafer_values.size*lotcount, ch.samples.size, "wrong number of samples for parameter #{p}, channel #{ch.name}" unless i == 0
    }
    lot_params.each_with_index {|p, i|
      ch = folder.spc_channel(parameter: p)
      assert_equal 1*lotcount, ch.samples.size, "wrong number of samples for parameter #{p}, channel #{ch.name}" unless i == 0
    }
    return dcr, lot2
  end

  def check_missing_param_activities_config(dcr, params={})
    parameters = params[:parameters] || @@parameters
    wafer_values = params[:wafer_values] || @@wafer_values
    parameter_missing = params[:parameter_missing] || false
    aqv_result = params[:aqv_result] || 'fail'
    aqv_comment = params[:aqv_comment] || @@missing_param_error_code
    multilot = params[:multilot] || false
    lot2 = params[:lot2] || nil
    lots = [@@lot]
    lots << lot2 if lot2 != nil
    #
    lots.each {|l|
      if @@datacheck_parameter_setup_enabled == 'true'
        if @@missingparametercheck_setup_active == 'true' and parameter_missing == true
          assert $spctest.verify_futurehold(l, @@missing_param_lothold_comment, parameters[0]), 'error setting future hold'
        elsif (@@missingparametercheck_setup_active == 'false' and @@missingparametercheck_setup_qa_active == 'true' and parameter_missing == true)
          assert $spctest.verify_futurehold(l, @@missing_param_lothold_comment, parameters[0]), 'error setting future hold'
        else
          assert !$spctest.verify_futurehold(l, @@missing_param_lothold_comment), 'future hold must not be set'
        end
        if @@autoqual_reporting_setup_enabled == 'true' and aqv_result == 'fail'
          assert $spctest.verify_aqv_message(dcr: dcr, lot: l, wafer: wafer_values.keys.sort, result: aqv_result,
            comment: aqv_comment, exporttag: __method__),'AQV message not as expected(fail)'
        elsif @@autoqual_reporting_setup_enabled == 'true' and aqv_result == 'success'
          assert $spctest.verify_aqv_message(dcr: dcr, lot: l, wafer: wafer_values.keys.sort, result: aqv_result,
            exporttag: __method__),'AQV message not as expected(success)'
        end
        if @@missingparametercheck_setup_notification == 'true' and parameter_missing == true
          assert $spctest.verify_notification(@@missing_param_error_code, parameters[0], timeout: 300), 'notification missing'
        else
          assert !$spctest.verify_notification(@@missing_param_error_code, parameters[0]), 'no notification expected'
        end
      else
        $log.info ' --- Please check log file manually for NO missing parameters entries! --- '
        assert !$spctest.verify_futurehold(l, @@missing_param_lothold_comment), 'future hold must not be set'
        assert !$spctest.verify_notification(@@missing_param_error_code, parameters[0]), 'no notification expected'
      end
    }
  end

  def check_missing_param_activities_inline(dcr, params={})
    parameters = params[:parameters] || @@parameters
    wafer_values = params[:wafer_values] || @@wafer_values
    parameter_missing = params[:parameter_missing] || false
    aqv_result = params[:aqv_result] || 'fail'
    aqv_comment = params[:aqv_comment] || @@missing_param_error_code
    multilot = params[:multilot] || false
    lot2 = params[:lot2] || nil
    lots = [@@lot]
    lots << lot2 if lot2 != nil
    #
    lots.each {|l|
      if @@datacheck_parameter_inline_enabled == 'true'
        if @@missingparametercheck_inline_active == 'true' and parameter_missing == true
           assert $spctest.verify_futurehold(l, @@missing_param_lothold_comment, parameters[0]), 'error setting future hold'
        elsif (@@missingparametercheck_inline_active == 'false' and @@missingparametercheck_inline_qa_active == 'true' and parameter_missing == true)
          assert $spctest.verify_futurehold(l, @@missing_param_lothold_comment, parameters[0]), 'error setting future hold'
        else
          assert !$spctest.verify_futurehold(l, @@missing_param_lothold_comment), 'future hold must not be set'
        end
        if @@autoqual_reporting_inline_enabled == 'true' and aqv_result == 'fail'
          assert $spctest.verify_aqv_message(dcr: dcr, lot: l, wafer: wafer_values.keys.sort, result: aqv_result,
            comment: aqv_comment, exporttag: __method__),'AQV message not as expected(fail)'
        elsif @@autoqual_reporting_inline_enabled == 'true' and aqv_result == 'success'
         assert $spctest.verify_aqv_message(dcr: dcr, lot: l, wafer: wafer_values.keys.sort, result: aqv_result,
            exporttag: __method__),'AQV message not as expected(success)'
        end
        if @@missingparametercheck_inline_notification == 'true' and parameter_missing == true
          assert $spctest.verify_notification(@@missing_param_error_code, parameters[0], timeout: 300), 'notification missing'
        else
          assert !$spctest.verify_notification(@@missing_param_error_code, parameters[0]), 'no notification expected'
        end
      else
        $log.info ' --- Please check log file manually for NO missing parameters entries! --- '
        assert !$spctest.verify_futurehold(l, @@missing_param_lothold_comment), 'future hold must not be set'
        assert !$spctest.verify_notification(@@missing_param_error_code, parameters[0]), 'no notification expected'
      end
    }
  end
end
