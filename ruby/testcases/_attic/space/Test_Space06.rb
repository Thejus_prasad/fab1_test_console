=begin 
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.
    
Author: Steffen Steidten, 2011-11-25
=end

require_relative 'Test_Space_Setup'


class Test_Space06 < Test_Space_Setup
  @@messages = ['[(Mean above control limit)', '[(Mean below control limit)', '[(Raw above specification)', '[(Raw below specification)']
  @@mtool = 'QAMeas'
  @@mpd = 'QA-MB-FTDEPM-LIS-CAEX.01'


  def setup
    super
    if $lds && self.class.class_variable_defined?(:@@lot) # bypass test0000_setup
      assert_equal 0, $sv.eqp_cleanup(@@eqp)
      assert_equal 0, $sv.lot_cleanup(@@lot)
      folder = $lds.folder(@@autocreated_qa)
      assert folder.delete_channels, 'error cleaning channels' if folder
    end
  end


  def test0600_0_setup
    $setup_ok = false
    #
    # resubmit of the corresponding Spec Limits file
    if ['xxitdc', 'let'].member?($env)
      (human_workflow_action(@@specLimitsTable, 'CANCEL', :user=>@@approvers[0]); sleep 30) if $sfc.wait_for_approve(@@specLimitsTable, :user=>@@approvers[0], :timeout=>1, :sleeptime=>1)
      (human_workflow_action(@@specLimitsTable, 'CANCEL', :user=>@@approvers[0]); sleep 30) if $sfc.wait_for_edit_content(@@specLimitsTable, :user=>@@approvers[0], :timeout=>1, :sleeptime=>1)
      (human_workflow_action(@@specLimitsTable, 'CANCEL'); sleep 30) if $sfc.wait_for_edit_content(@@specLimitsTable, :timeout=>1, :sleeptime=>1)
      edit_submit_approve(@@specLimitsTable, @@approvers[0])
    end
    $spctest.sil_upload_verify_autocharting('etc/testcasedata/space/Auto_Charting_STAG.SpecID.xlsx') if $spctest.env == 'f8stag'
    #
    assert @@ppd = $sildb.pd_reference(mpd: @@mpd).first, 'no PD reference found for #{@@mpd}'
    #
    $setup_ok = true
  end

  def test0600_speclimits_1
    # build and submit DCR with one parameter only to identify the parameter in the hold claim memo
    assert_equal 0, $sv.eqp_cleanup(@@eqp)
    assert_equal 0, $sv.lot_cleanup(@@lot)
    dcr = Space::DCR.new(eqp: @@eqp, lot: @@lot)
    dcr.add_parameter(@@parameters[0], @@wafer_valuesooc)
    res = $client.send_dcr(dcr, export: :caller)
    assert $spctest.verify_dcr_accepted(res, @@wafer_valuesooc.size, nooc: 'all*2'), 'DCR not submitted successfully'
    assert $spctest.verify_futurehold(@@lot, nil, @@parameters[0], messages: @@messages), 'error setting future hold'
  end

  def test0601_speclimits_all
    # build and submit DCR with all parameters
    dcr = Space::DCR.new(eqp: @@eqp, lot: @@lot)
    dcr.add_parameters(@@parameters, @@wafer_valuesooc)
    res = $client.send_dcr(dcr, export: :caller)
    assert $spctest.verify_dcr_accepted(res, @@parameters.size * @@wafer_valuesooc.size, nooc: 'all*2'), 'DCR not submitted successfully'
    assert $spctest.verify_futurehold(@@lot, nil, nil, expected_holds: @@parameters.size, messages: @@messages), 'error setting future hold'
  end

  def test0602_speclimits_all_4times_USL
    # build and submit DCR with all parameters
    dcr = Space::DCR.new(eqp: @@eqp, lot: @@lot)
    dcr.add_parameters(@@parameters, @@wafer_valuesooc)
    res = $client.send_dcr(dcr, export: :caller)
    assert $spctest.verify_dcr_accepted(res, @@parameters.size * @@wafer_valuesooc.size, nooc: 'all*2'), 'DCR not submitted successfully'
    assert $spctest.verify_futurehold(@@lot, nil, nil, expected_holds: @@parameters.size, messages: @@messages), 'error setting future hold'
  end

  def test0603_speclimits_all_1000times_USL
    # build and submit DCR with all parameters
    dcr = Space::DCR.new(eqp: @@eqp, lot: @@lot)
    dcr.add_parameters(@@parameters, @@wafer_valuesooc)
    res = $client.send_dcr(dcr, export: :caller)
    assert $spctest.verify_dcr_accepted(res, @@parameters.size * @@wafer_valuesooc.size, nooc: 'all*2'), 'DCR not submitted successfully'
    assert $spctest.verify_futurehold(@@lot, nil, nil, expected_holds: @@parameters.size, messages: @@messages), 'error setting future hold'
  end
  
  def test0604_speclimits_all_4times_USL_sites
    # build and submit DCR
    wafer_values = Hash[@@wafer_values.keys.collect {|w| [w, [410,420,430,440,450,460]] }]
    dcr = Space::DCR.new(eqp: @@eqp, lot: @@lot)
    @@parameters.each {|p| dcr.add_parameter(p, wafer_values, sites: true)}
    res = $client.send_dcr(dcr, export: :caller)
    nsamples = @@parameters.size * wafer_values.size
    assert $spctest.verify_dcr_accepted(res, nsamples, nooc: nsamples*3), 'DCR not submitted successfully'
    assert $spctest.verify_futurehold(@@lot, nil, nil, expected_holds: @@parameters.size, messages: @@messages), 'error setting future hold'
  end
  
  def test0605_speclimits_1_4times_USL_sites
    # build and submit DCR
    wafer_values = Hash[@@wafer_values.keys.collect {|w| [w, [41,42,43,44,45,460]] }]
    dcr = Space::DCR.new(eqp: @@eqp, lot: @@lot)
    @@parameters.each {|p| dcr.add_parameter(p, wafer_values, sites: true)}
    res = $client.send_dcr(dcr, export: :caller)
    nsamples = @@parameters.size * wafer_values.size
    assert $spctest.verify_dcr_accepted(res, nsamples, nooc: nsamples*3), 'DCR not submitted successfully'
    assert $spctest.verify_futurehold(@@lot, nil, nil, expected_holds: @@parameters.size, messages: @@messages), 'error setting future hold'
  end
  
  def test0606_parameter_extension_minus
    # build and submit DCR
    parameters = ['QA_PARAM_900', 'QA_PARAM_901']
    ctrl = {'MEAN'=>410, 'STDDEV'=>1000, 'MIN'=>-550, 'MAX'=>550, 'COUNT'=>5000}
    #
    dcr = Space::DCR.new(eqp: @@eqp, lot: @@lot)
    parameters.each {|p|
      ctrl.each {|k, v| dcr.add_parameter("#{p}-#{k}", {'2502J493KO'=>v}, space_listening: true) }
    }
    assert $spctest.send_dcr_verify(dcr, nsamples: 2), 'DCR not submitted successfully'
    #
    folder = $lds.folder(@@autocreated_qa)
    parameters.each {|p|
      ch = folder.spc_channel(parameter: p)
      # ensure spec and sample limits are set
      s = ch.samples[-1]
      assert $spctest.verify_sample_speclimits(s, 25, 45, 75)
    }
  end

  def test0610_speclimits_TEST
    # build and submit DCR with all parameters, technology TEST
    dcr = Space::DCR.new(eqp: @@eqp, lot: @@lot, technology: 'TEST')
    dcr.add_parameters(@@parameters, @@wafer_valuesooc)
    res = $client.send_dcr(dcr, export: :caller)
    assert $spctest.verify_dcr_accepted(res, @@parameters.size * @@wafer_valuesooc.size, nooc: 'all*2'), 'DCR not submitted successfully'
    #
    # verify spc channels are created in AutoCreated_QA
    assert $spctest.verify_parameters(@@parameters, $lds_setup.folder(@@autocreated_qa), @@wafer_valuesooc.size), 'error creating channels'
    assert $spctest.verify_futurehold(@@lot, nil, nil, expected_holds: @@parameters.size, messages: @@messages), 'error setting future hold'
  end
  
  def test0612_speclimits_active_wafer_and_lot_based
    # build and submit DCR for related MPD
    dcr = Space::DCR.new(eqp: @@mtool, lot: @@lot, op: @@mpd)
    dcr.add_parameters(@@parameters[0..-2], @@wafer_valuesooc, processing_areas: ['CHC'])
    dcr.add_parameter(@@parameters[-1], {@@lot=>7.0}, reading_type: 'Lot', space_listening: false)
    res = $client.send_dcr(dcr, export: :caller)
    assert $spctest.verify_dcr_accepted(res, (@@parameters.size - 1) * @@wafer_valuesooc.size + 1), 'DCR not submitted successfully'
    #
    assert $spctest.verify_futurehold(@@lot, nil, nil, expected_holds: @@parameters.size, messages: @@messages), 'error setting future hold for OOC'
  end
  
  #MSR513786
  def test0630_spec_limits_without_space_scenario_non_ooc_all_params
    #MSR513786.QA 513786NM  MSR-513786.01 MSR513786PG|MSR513786PROD.01  QA_PARAM_990    250 450 750 WAFER             
    #MSR513786.QA 513786NM  MSR-513786.01 MSR513786PG|MSR513786PROD.01  QA_PARAM_991    250 450 750 WAFER             
    #MSR513786.QA 513786NM  MSR-513786.01 MSR513786PG|MSR513786PROD.01  QA_PARAM_992    250 450 750 WAFER             
    #MSR513786.QA 513786NM  MSR-513786.01 MSR513786PG|MSR513786PROD.01  QA_PARAM_993    250 450 750 WAFER             
    #MSR513786.QA 513786NM  MSR-513786.01 MSR513786PG|MSR513786PROD.01  QA_PARAM_994    250 450 750 WAFER             
    #MSR513786.QA 513786NM  MSR-513786.01 MSR513786PG|MSR513786PROD.01  QA_PARAM_995    250 450 750 WAFER   
    if ['xxxitdc', 'let'].member?($env)
      (human_workflow_action(@@specLimitsSPCScenario0Table, 'CANCEL', :user=>@@approvers[0]); sleep 30) if $sfc.wait_for_approve(@@specLimitsSPCScenario0Table, :user=>@@approvers[0], :timeout=>1, :sleeptime=>1)
      (human_workflow_action(@@specLimitsSPCScenario0Table, 'CANCEL', :user=>@@approvers[0]); sleep 30) if $sfc.wait_for_edit_content(@@specLimitsSPCScenario0Table, :user=>@@approvers[0], :timeout=>1, :sleeptime=>1)
      (human_workflow_action(@@specLimitsSPCScenario0Table, 'CANCEL'); sleep 30) if $sfc.wait_for_edit_content(@@specLimitsSPCScenario0Table, :timeout=>1, :sleeptime=>1)
      edit_submit_approve(@@specLimitsSPCScenario0Table, @@approvers[0])
    end
    mpd = 'MSR513786.QA'
    technology = '32NM'
    route = 'MSR-513786.01'
    productgroup = 'MSR513786PG'
    product = 'MSR513786PROD.01'
    parameters = ['QA_PARAM_990', 'QA_PARAM_991', 'QA_PARAM_992', 'QA_PARAM_993', 'QA_PARAM_994', 'QA_PARAM_995']
    #
    # build and submit DCR with all parameters
    dcr = Space::DCR.new(eqp: @@eqp, lot: @@lot, op: mpd, productgroup: productgroup, product: product, technology: technology, route: route)
    wafer_values = Hash[@@wafer_values.collect {|w, v| [w, v + 300]}]
    dcr.add_parameters(parameters, wafer_values)
    res = $client.send_dcr(dcr, export: :caller)
    assert $spctest.verify_dcr_accepted(res, parameters.size * wafer_values.size), 'DCR not submitted successfully'
    #
    # verify spc channels
    folder = $lds.folder(@@autocreated_qa)
    parameters.each {|p|
      ch = folder.spc_channel(parameter: p)
      ref_ch = Space::SpcApi::ChannelData.new(nil, nil, "Auto-created using template: #{@@params_template}", p)
      assert $spctest.verify_channel(ch, ref_ch, dcr, wafer_values, last_sample_only: true), 'wrong channel data'
      assert $spctest.verify_sample_speclimits(ch.samples, 250, 450, 750), 'wrong spec limits'
    }
  end

  #MSR513786
  def test0631_spec_limits_with_space_scenario_non_ooc_all_params
    skip 'not applicable in #{$env}' unless ['itdc', 'let'].member?($env)
    #MSR513786.QA 513786NM  MSR-513786.01 MSR513786PG|MSR513786PROD.01  QA_PARAM_990    250 450 750 WAFER             
    #MSR513786.QA 513786NM  MSR-513786.01 MSR513786PG|MSR513786PROD.01  QA_PARAM_991    250 450 750 WAFER             CALCULATION
    #MSR513786.QA 513786NM  MSR-513786.01 MSR513786PG|MSR513786PROD.01  QA_PARAM_992    250 450 750 WAFER             MANUAL_SPECLIMIT_SPACE
    #MSR513786.QA 513786NM  MSR-513786.01 MSR513786PG|MSR513786PROD.01  QA_PARAM_993    250 450 750 WAFER             MANUAL_SPECLIMIT_SPACE
    #MSR513786.QA 513786NM  MSR-513786.01 MSR513786PG|MSR513786PROD.01  QA_PARAM_994    250 450 750 WAFER             IGNORE_ALL_PARAMETER_CHECK
    #MSR513786.QA 513786NM  MSR-513786.01 MSR513786PG|MSR513786PROD.01  QA_PARAM_995    250 450 750 WAFER             IGNORE_ALL_PARAMETER_CHECK 
    if ['xxxitdc', 'let'].member?($env)
      (human_workflow_action(@@specLimitsSPCScenario1Table, 'CANCEL', :user=>@@approvers[0]); sleep 30) if $sfc.wait_for_approve(@@specLimitsSPCScenario1Table, :user=>@@approvers[0], :timeout=>1, :sleeptime=>1)
      (human_workflow_action(@@specLimitsSPCScenario1Table, 'CANCEL', :user=>@@approvers[0]); sleep 30) if $sfc.wait_for_edit_content(@@specLimitsSPCScenario1Table, :user=>@@approvers[0], :timeout=>1, :sleeptime=>1)
      (human_workflow_action(@@specLimitsSPCScenario1Table, 'CANCEL'); sleep 30) if $sfc.wait_for_edit_content(@@specLimitsSPCScenario1Table, :timeout=>1, :sleeptime=>1)
      edit_submit_approve(@@specLimitsSPCScenario1Table, @@approvers[0])
    end
    mpd = 'MSR513786_1.QA'
    technology = '32NM'
    route = 'MSR-513786.01'
    productgroup = 'MSR513786PG'
    product = 'MSR513786PROD.01'
    parameters = ['QA_PARAM_990', 'QA_PARAM_991', 'QA_PARAM_992', 'QA_PARAM_993', 'QA_PARAM_994', 'QA_PARAM_995']
    #
    # build and submit DCR with all parameters
    dcr = Space::DCR.new(eqp: @@eqp, lot: @@lot, op: mpd, productgroup: productgroup, product: product, technology: technology, route: route)
    wafer_values = Hash[@@wafer_values.collect {|w, v| [w, v + 300]}]
    dcr.add_parameters(parameters, wafer_values)
    res = $client.send_dcr(dcr, export: :caller)
    assert $spctest.verify_dcr_accepted(res, parameters.size * wafer_values.size), 'DCR not submitted successfully'
    #
    # verify spc channels
    folder = $lds.folder(@@autocreated_qa)
    p = parameters.first
    ch = folder.spc_channel(parameter: p)
    ref_ch = Space::SpcApi::ChannelData.new(nil, nil, "Auto-created using template: #{@@params_template}", p)
    assert $spctest.verify_channel(ch, ref_ch, dcr, wafer_values, last_sample_only: true), 'wrong channel data'
    assert $spctest.verify_sample_speclimits(ch.samples, 250, 450, 750), 'wrong spec limits'
      
    parameters[1..-1].each {|p|
      ch = folder.spc_channel(parameter: p)
      ref_ch = Space::SpcApi::ChannelData.new(nil, nil, "Auto-created using template: #{@@params_template}", p)
      assert $spctest.verify_channel(ch, ref_ch, dcr, wafer_values, last_sample_only: true), 'wrong channel data'
      assert $spctest.verify_sample_speclimits(ch.samples, nil, nil, nil), 'wrong spec limits'
    }
  end

  #MSR513786
  def test0632_spec_limits_with_space_scenario_ooc_all_params
    skip 'not applicable in #{$env}' unless ['itdc', 'let'].member?($env)
    #MSR513786.QA 513786NM  MSR-513786.01 MSR513786PG|MSR513786PROD.01  QA_PARAM_990    250 450 750 WAFER             
    #MSR513786.QA 513786NM  MSR-513786.01 MSR513786PG|MSR513786PROD.01  QA_PARAM_991    250 450 750 WAFER             CALCULATION
    #MSR513786.QA 513786NM  MSR-513786.01 MSR513786PG|MSR513786PROD.01  QA_PARAM_992    250 450 750 WAFER             MANUAL_SPECLIMIT_SPACE
    #MSR513786.QA 513786NM  MSR-513786.01 MSR513786PG|MSR513786PROD.01  QA_PARAM_993    250 450 750 WAFER             MANUAL_SPECLIMIT_SPACE
    #MSR513786.QA 513786NM  MSR-513786.01 MSR513786PG|MSR513786PROD.01  QA_PARAM_994    250 450 750 WAFER             IGNORE_ALL_PARAMETER_CHECK
    #MSR513786.QA 513786NM  MSR-513786.01 MSR513786PG|MSR513786PROD.01  QA_PARAM_995    250 450 750 WAFER             IGNORE_ALL_PARAMETER_CHECK
    if ['xxxitdc', 'let'].member?($env)
      (human_workflow_action(@@specLimitsSPCScenario1Table, 'CANCEL', :user=>@@approvers[0]); sleep 30) if $sfc.wait_for_approve(@@specLimitsSPCScenario1Table, :user=>@@approvers[0], :timeout=>1, :sleeptime=>1)
      (human_workflow_action(@@specLimitsSPCScenario1Table, 'CANCEL', :user=>@@approvers[0]); sleep 30) if $sfc.wait_for_edit_content(@@specLimitsSPCScenario1Table, :user=>@@approvers[0], :timeout=>1, :sleeptime=>1)
      (human_workflow_action(@@specLimitsSPCScenario1Table, 'CANCEL'); sleep 30) if $sfc.wait_for_edit_content(@@specLimitsSPCScenario1Table, :timeout=>1, :sleeptime=>1)
      edit_submit_approve(@@specLimitsSPCScenario1Table, @@approvers[0])
    end
    mpd = 'MSR513786_1.QA'
    technology = '32NM'
    route = 'MSR-513786.01'
    productgroup = 'MSR513786PG'
    product = 'MSR513786PROD.01'
    parameters = ['QA_PARAM_990', 'QA_PARAM_991', 'QA_PARAM_992', 'QA_PARAM_993', 'QA_PARAM_994', 'QA_PARAM_995']
    #
    # build and submit DCR with all parameters
    dcr = Space::DCR.new(eqp: @@eqp, lot: @@lot, op: mpd, productgroup: productgroup, product: product, technology: technology, route: route)
    wafer_values = Hash[@@wafer_values.collect {|w, v| [w, v + 800]}]
    dcr.add_parameters(parameters, wafer_values)
    res = $client.send_dcr(dcr, export: :caller)
    assert $spctest.verify_dcr_accepted(res, parameters.size * wafer_values.size), 'DCR not submitted successfully'
    #
    # verify spc channels
    folder = $lds.folder(@@autocreated_qa)
    p = parameters.first
    ch = folder.spc_channel(parameter: p)
    ref_ch = Space::SpcApi::ChannelData.new(nil, nil, "Auto-created using template: #{@@params_template}", p)
    assert $spctest.verify_channel(ch, ref_ch, dcr, wafer_values, last_sample_only: true), 'wrong channel data'
    assert $spctest.verify_sample_speclimits(ch.samples, 250, 450, 750), 'wrong spec limits'
      
    parameters[1..-1].each {|p|
      ch = folder.spc_channel(parameter: p)
      ref_ch = Space::SpcApi::ChannelData.new(nil, nil, "Auto-created using template: #{@@params_template}", p)
      assert $spctest.verify_channel(ch, ref_ch, dcr, wafer_values, last_sample_only: true), 'wrong channel data'
      assert $spctest.verify_sample_speclimits(ch.samples, nil, nil, nil), 'wrong spec limits'
    }
  end

  def test0640_speclimits_all_rework_route_match
    # build and submit DCR with all parameters
    parameters = ['QA_PARAM_900', 'QA_PARAM_901', 'QA_PARAM_902', 'QA_PARAM_903', 'QA_PARAM_904']
    dcr = Space::DCR.new(eqp: @@eqp, lot: @@lot, route: 'RP-Test')
    dcr.add_parameters(parameters, @@wafer_valuesooc)
    res = $client.send_dcr(dcr, export: :caller)
    assert $spctest.verify_dcr_accepted(res, parameters.size * @@wafer_valuesooc.size, nooc: 'all*2'), 'DCR not submitted successfully'
    assert $spctest.verify_futurehold(@@lot, nil, nil, expected_holds: parameters.size, messages: @@messages), 'error setting future hold'
  end

  def test0641_speclimits_all_rework_route_no_match_no_limits
    skip 'not applicable in #{$env}' unless ['itdc', 'let'].member?($env)
    # build and submit DCR with all parameters
    dcr = Space::DCR.new(eqp: @@eqp, lot: @@lot, route: 'RO-Test')
    dcr.add_parameters(@@parameters, @@wafer_valuesooc)
    res = $client.send_dcr(dcr, export: :caller)
    assert $spctest.verify_dcr_accepted(res, @@parameters.size * @@wafer_valuesooc.size, nooc: 0), 'DCR not submitted successfully'
    assert $spctest.verify_futurehold(@@lot, nil, nil, expected_holds: 0, messages: @@messages), 'error setting future hold'
  end

  def test0642_speclimits_all_rework_standard_route
    # build and submit DCR with all parameters
    dcr = Space::DCR.new(eqp: @@eqp, lot: @@lot)
    dcr.add_parameters(@@parameters, @@wafer_valuesooc)
    res = $client.send_dcr(dcr, export: :caller)
    assert $spctest.verify_dcr_accepted(res, @@parameters.size * @@wafer_valuesooc.size, nooc: 'all*2'), 'DCR not submitted successfully'
    assert $spctest.verify_futurehold(@@lot, nil, nil, expected_holds: @@parameters.size, messages: @@messages), 'error setting future hold'
  end

  def test0643_speclimits_all_erf_route_match
    # build and submit DCR with all parameters
    dcr = Space::DCR.new(eqp: @@eqp, lot: @@lot, route: 'e1234-SIL001.01')
    dcr.add_parameters(@@parameters, @@wafer_valuesooc)
    res = $client.send_dcr(dcr, export: :caller)
    assert $spctest.verify_dcr_accepted(res, @@parameters.size * @@wafer_valuesooc.size, nooc: 'all*2'), 'DCR not submitted successfully'
    assert $spctest.verify_futurehold(@@lot, nil, nil, expected_holds: @@parameters.size, messages: @@messages), 'error setting future hold'
  end
  
end
