=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: C.Stenzel, 2016-09-22
MSR: 1115870

History:
  ---
=end

require_relative 'Test_Space_Setup'

# Test Space/SIL CAs
class Test_Space0711 < Test_Space_Setup
  @@mpd_qa = 'QA-MB-FTDEPM-LIS-CAEX.01'
  @@chamber_a = 'PM1'
  @@chamber_b = 'PM2'
  @@chamber_c = 'CHX'
  @@chambers = {@@chamber_a=>5, @@chamber_b=>6, @@chamber_c=>7}
  @@reticle = 'QAReticle0001'
  @@mrecipe = nil  # assigned later
  @@route = nil    # assigned later
  @@technology = 'TEST'

  def setup
    super
    return unless $lds && self.class.class_variable_defined?(:@@lot) # bypass test0000_setup
    assert_equal 0, $sv.eqp_cleanup(@@eqp)
    assert_equal 0, $sv.lot_cleanup(@@lot)
    assert_equal 0, $sv.inhibit_cancel(:route, $sv.lot_info(@@lot).route)
    #
    $lds.all_spc_channels('QA_PARAM_90*').each {|ch| assert ch.delete}
    folder = $lds.folder(@@autocreated_qa)
    assert folder.delete_channels, 'error cleaning channels' if folder
  end

  def teardown
    $log.info "\nteardown"
    super
    $sv.merge_lot_family(@@lot)
  end

  def test07110_setup
    $setup_ok = false
    #
    assert @@ppd_qa = $sildb.pd_reference(mpd: @@mpd_qa).first, "no PD reference found for #{@@mpd_qa}"
    #
    ['AutoLotHold-NoAQV', 'CancelLotHold',
    'AutoEquipmentInhibit-NoAQV', 'ReleaseEquipmentInhibit',
    'AutoChamberInhibit-NoAQV', 'ReleaseChamberInhibit',
    'AutoReticleInhibit-NoAQV', 'ReleaseReticleInhibit',
    'AutoRouteInhibit-NoAQV', 'ReleaseRouteInhibit',
    'AutoMachineRecipeInhibit-NoAQV', 'ReleaseMachineRecipeInhibit',
    'AutoSetLotScriptParameter-OOC-NoAQV',
    'AutoSetWaferScriptParameter-OOC-NoAQV'].each {|action|
      assert $spc.corrective_action(action), "CA #{action.inspect} not defined"
    }
    $setup_ok = true
  end

  def test07111_autolothold_noaqv
    assert_equal 0, $sv.lot_opelocate(@@lot, 'first')
    channels = create_clean_channels(technology: @@technology)
    assert $spctest.assign_verify_cas(channels, ['AutoLotHold-NoAQV', 'CancelLotHold'])
    #
    assert_equal 0, $sv.eqp_cleanup(@@eqp)
    assert_equal 0, $sv.lot_cleanup(@@lot)
    submit_process_dcr(@@ppd_qa, wafer_values: @@wafer_values2, technology: @@technology)
    # send DCR with OOC values
    dcr = Space::DCR.new(lot: @@lot, eqp: @@mtool, op: @@mpd_qa, technology: @@technology)
    dcr.add_parameters(@@parameters, @@wafer_values2ooc)
    res = $client.send_dcr(dcr, export: :caller)
    assert $spctest.verify_dcr_accepted(res, @@parameters.count*@@wafer_values2ooc.size, nooc: 'all*2'), 'DCR not submitted successfully'
    assert $spctest.verify_aqv_message(dcr: dcr, lot: @@lot, wafer: @@wafer_values2ooc.keys.sort, result: "success"), 'AQV message not as expected (success)'
    #
    # verify futurehold from AutoLotHold-NoAQV
    assert $spctest.verify_futurehold2(@@lot, '[(Raw above specification)', dcr_lot: dcr.find_lot_context(@@lot)), 'lot must have a future hold'
    # verify comment is set
    assert $spctest.verify_ecomment(channels, "LOT_HELD: #{@@lot}: reason={X-S")
    # cancel LotHold from automatic action
    channels.each {|ch|
      s = ch.samples(ckc: true)[-1]
      assert s.assign_ca($spc.corrective_action('CancelLotHold')), 'error assigning corective action'
    }
    s = channels[0].samples(ckc: true)[-1]
    sleep 30
    assert $spctest.verify_futurehold_cancel(@@lot, '[(Raw above specification)'), 'lot must have no future hold'
  end

  def test07112_autoequipmentinhibit_noaqv
    channels = create_clean_channels(technology: @@technology)
    assert $spctest.assign_verify_cas(channels, ['AutoEquipmentInhibit-NoAQV', 'ReleaseEquipmentInhibit'])
    #
    assert_equal 0, $sv.eqp_cleanup(@@eqp)
    assert_equal 0, $sv.lot_cleanup(@@lot)
    dcrp = submit_process_dcr(@@ppd_qa, wafer_values: @@wafer_values2, technology: @@technology)
    # send DCR with OOC values
    dcr = Space::DCR.new(lot: @@lot, eqp: @@mtool, op: @@mpd_qa, technology: @@technology)
    dcr.add_parameters(@@parameters, @@wafer_values2ooc)
    res = $client.send_dcr(dcr, export: :caller)
    assert $spctest.verify_dcr_accepted(res, @@parameters.size*@@wafer_values2ooc.size, nooc: 'all*2'), 'DCR not submitted successfully'
    assert $spctest.verify_aqv_message(dcr: dcr, lot: @@lot, wafer: @@wafer_values2ooc.keys.sort, result: 'success'), 'AQV message not as expected (success)'
    #
    # verify inhibit and comment
    assert $spctest.verify_inhibit(@@eqp, channels.size, details: true, dcr: dcrp, lot: @@lot, spc_run: res, cj: dcr.controljob), 'equipment must have an inhibit'
    assert_equal channels.size, $sv.inhibit_list(:eqp, @@eqp).count, 'equipment must have an inhibit'
    assert $spctest.verify_ecomment(channels, "TOOL_HELD: #{@@eqp}: reason={X-S")
    $log.info 'EquipmentInhibit automatically set'
    #
    # release inhibit
    channels.each {|ch|
      s = ch.samples(ckc: true)[-1]
      assert s.assign_ca($spc.corrective_action('ReleaseEquipmentInhibit')), 'error assigning CA'
    }
    assert $spctest.verify_inhibit(@@eqp, 0), 'equipment must not have an inhibit'
  end

  def test07113_autochamberinhibit_noaqv
    chambers = {@@chamber_a=>5, @@chamber_b=>6}
    channels = create_clean_channels(chambers: chambers, technology: @@technology)
    assert $spctest.assign_verify_cas(channels, ['AutoChamberInhibit-NoAQV', 'ReleaseChamberInhibit'])
    #
    assert_equal 0, $sv.eqp_cleanup(@@eqp)
    assert_equal 0, $sv.lot_cleanup(@@lot)
    # send process DCR
    dcrp = submit_process_dcr(@@ppd_qa, wafer_values: @@wafer_values2, chambers: chambers, technology: @@technology)
    # send DCR with OOC values
    dcr = Space::DCR.new(lot: @@lot, eqp: @@eqp, op: @@mpd_qa, technology: @@technology)
		dcr.add_parameters(@@parameters, @@wafer_values2ooc)
    res = $client.send_dcr(dcr, export: :caller)
    assert $spctest.verify_dcr_accepted(res, @@parameters.size*@@wafer_values2ooc.size, nooc: 'all*2'), 'DCR not submitted successfully'
    assert $spctest.verify_aqv_message(dcr: dcr, lot: @@lot, wafer: @@wafer_values2ooc.keys.sort, result: 'success'), 'AQV message not as expected (success)'
    #
    assert $spctest.verify_inhibit(@@eqp, chambers.count*channels.size, class: :eqp_chamber, details: true, dcr: dcrp, lot: @@lot, spc_run: res, cj: dcr.controljob), 'chamber must have an inhibit'
    assert $spctest.verify_ecomment(channels, "TOOL+CHAMBER_HELD: #{@@eqp}/#{@@chamber_a}: reason={X-S")
    #
    # release inhibit with noDefault attribute - with comment
    channels.each {|ch|
      s = ch.samples(ckc: true)[-1]
      assert s.assign_ca($spc.corrective_action('ReleaseChamberInhibit')), 'error assigning corrective action'
    }
    assert $spctest.verify_inhibit(@@eqp, 0, class: :eqp_chamber), 'chamber must not have an inhibit'
  end

  def test07114_automachinerecipeinhibit_noaqv
    channels = create_clean_channels(technology: @@technology)
    assert $spctest.assign_verify_cas(channels, ['AutoMachineRecipeInhibit-NoAQV', 'ReleaseMachineRecipeInhibit'])
    #
    assert_equal 0, $sv.eqp_cleanup(@@eqp)
    assert_equal 0, $sv.lot_cleanup(@@lot)
    dcrp = submit_process_dcr(@@ppd_qa, wafer_values: @@wafer_values2, machine_recipe: @@mrecipe, technology: @@technology)
    # send DCR with OOC values
    dcr = Space::DCR.new(lot: @@lot, eqp: @@mtool, op: @@mpd_qa, machine_recipe: @@mrecipe, technology: @@technology)
    dcr.add_parameters(@@parameters, @@wafer_values2ooc)
    res = $client.send_dcr(dcr, export: :caller)
    assert $spctest.verify_dcr_accepted(res, @@parameters.size*@@wafer_values2ooc.size, nooc: 'all*2'), 'DCR not submitted successfully'
    assert $spctest.verify_aqv_message(dcr: dcr, lot: @@lot, wafer: @@wafer_values2ooc.keys.sort, result: 'success'), 'AQV message not as expected (success)'
    #
    # verify comment and inhibit is set
    assert $spctest.verify_inhibit(@@mrecipe, channels.size, class: :recipe, details: true, dcr: dcrp, lot: @@lot, spc_run: res, cj: dcr.controljob), 'Machine Recipe must have an inhibit'
    assert $spctest.verify_ecomment(channels, "RECIPE_HELD: #{@@mrecipe}: reason={X-S")
    #
    # release inhibit
    channels.each {|ch|
      s = ch.samples(ckc: true)[-1]
      assert s.assign_ca($spc.corrective_action('ReleaseMachineRecipeInhibit')), 'error assigning corrective action'
    }
    assert $spctest.verify_inhibit(@@mrecipe, 0, class: :recipe), 'machine recipe must not have an inhibit'
  end

  def test07115_autoreticleinhibit_noaqv
    rparam = 'L-STP-RETICLEID1'
    #
    channels = create_clean_channels(department: 'LIT', mtool: @@eqp, technology: @@technology)
    assert $spctest.assign_verify_cas(channels, ['AutoReticleInhibit-NoAQV', 'ReleaseReticleInhibit'])
    #
    assert_equal 0, $sv.eqp_cleanup(@@eqp)
    assert_equal 0, $sv.lot_cleanup(@@lot)
    # send DCR with OOC values
    dcr = Space::DCR.new(lot: @@lot, eqp: @@eqp, department: 'LIT', op: @@mpd_lit, technology: @@technology)
    dcr.add_parameters(@@parameters, @@wafer_values2ooc)
    dcr.add_parameter(rparam, {@@lot=>@@reticle}, nocharting: true, reading_type: 'Lot', value_type: 'string')
    dcr.parameters[1][:Reading][0][:Reticle] = @@reticle
    res = $client.send_dcr(dcr, export: :caller)
    assert $spctest.verify_dcr_accepted(res, @@parameters.size*@@wafer_values2ooc.size, nooc: 'all*2'), 'DCR not submitted successfully'
    assert $spctest.verify_aqv_message(dcr: dcr, lot: @@lot, wafer: @@wafer_values2ooc.keys.sort, result: 'success'), 'AQV message not as expected (success)'
    #
    # verify comment and inhibit is set
    assert $spctest.verify_inhibit(@@reticle, channels.size, class: :reticle), 'reticle must have an inhibit'
    assert $spctest.verify_ecomment(channels, "RETICLE_HELD:#{@@reticle}: reason={X-S")
    #
    # release inhibit
    channels.each {|ch|
      s = ch.samples(ckc: true)[-1]
      assert s.assign_ca($spc.corrective_action('ReleaseReticleInhibit')), 'error assigning corective action'
    }
    assert $spctest.verify_inhibit(@@reticle, 0, class: :reticle), 'reticle must not have an inhibit'
  end

  def test07116_autorouteinhibit_noaqv
    channels = create_clean_channels(technology: @@technology)
    assert $spctest.assign_verify_cas(channels, ['AutoRouteInhibit-NoAQV', 'ReleaseRouteInhibit'])
    #
    assert_equal 0, $sv.eqp_cleanup(@@eqp)
    assert_equal 0, $sv.lot_cleanup(@@lot)
    submit_process_dcr(@@ppd_qa, wafer_values: @@wafer_values2, technology: @@technology)
    # send DCR with OOC values
    dcr = Space::DCR.new(lot: @@lot, eqp: @@eqp, op: @@mpd_qa, route: @@route, technology: @@technology)
    dcr.add_parameters(@@parameters, @@wafer_values2ooc)
    res = $client.send_dcr(dcr, export: :caller)
    assert $spctest.verify_dcr_accepted(res, @@parameters.size*@@wafer_values2ooc.size, nooc: 'all*2'), 'DCR not submitted successfully'
    assert $spctest.verify_aqv_message(dcr: dcr, lot: @@lot, wafer: @@wafer_values2ooc.keys.sort, result: 'success'), 'AQV message not as expected (success)'
    #
    # verify comment and inhibit is set
    assert $spctest.verify_inhibit(@@route, channels.size, class: :route), 'route must have an inhibit'
    assert $spctest.verify_ecomment(channels, "ROUTE_HELD:#{@@route}: reason={X-S")
    #
    # release inhibit
    channels.each {|ch|
      s = ch.samples(ckc: true)[-1]
      assert s.assign_ca($spc.corrective_action('ReleaseRouteInhibit')), 'error assigning corective action'
    }
    assert $spctest.verify_inhibit(@@route, 0, class: :route), 'route must not have an inhibit'
  end

  def test07117_autosetlotscriptparameter_noaqv
    channels = create_clean_channels(technology: @@technology)
    assert $spctest.assign_verify_cas(channels, ['AutoSetLotScriptParameter-OOC-NoAQV'])
    assert $sv.user_parameter_delete_verify('Lot', @@lot, 'OOC')
    assert $sv.user_parameter_delete_verify('Lot', @@lot, 'OOCInteger')
    #
    assert_equal 0, $sv.eqp_cleanup(@@eqp)
    assert_equal 0, $sv.lot_cleanup(@@lot)
    submit_process_dcr(@@ppd_qa, wafer_values: @@wafer_values, technology: @@technology)
    # send DCR with OOC values
    dcr = Space::DCR.new(lot: @@lot, eqp: @@mtool, op: @@mpd_qa, technology: @@technology)
    dcr.add_parameters(@@parameters, @@wafer_values2ooc)
    res = $client.send_dcr(dcr, export: :caller)
    assert $spctest.verify_dcr_accepted(res, @@parameters.size*@@wafer_values2ooc.size, nooc: 'all*2'), 'DCR not submitted successfully'
    assert $spctest.verify_aqv_message(dcr: dcr, lot: @@lot, wafer: @@wafer_values2ooc.keys.sort, result: 'success'), 'AQV message not as expected (success)'
    #
    # verify lot script parameter is set
    assert_equal 'Yes', $sv.user_parameter('Lot', @@lot, name: 'OOC').value, 'lot script parameter not set'
    # verify external comment
    assert $spctest.verify_ecomment(channels, 'SET_LOT_SCRIPT_PARAMETER: reason={OOC/Yes}'), 'external comment not as expected'
  end

  def test07118_autosetwaferscriptparameter_noaqv
    channels = create_clean_channels(technology: @@technology)
    assert $spctest.assign_verify_cas(channels, ['AutoSetWaferScriptParameter-OOC-NoAQV'])

    assert_equal 0, $sv.eqp_cleanup(@@eqp)
    assert_equal 0, $sv.lot_cleanup(@@lot)
    li = $sv.lot_info(@@lot, wafers: true)
    li.wafers.each {|w| assert $sv.user_parameter_delete_verify('Wafer', w.wafer, 'OOC')}
    wafer_values = Hash[li.wafers.collect {|w| [w.wafer, 50]}]
    #
    submit_process_dcr(@@ppd_qa, wafer_values: wafer_values, technology: @@technology)
    # send dcr with some ooc values
    wafer_values = Hash[wafer_values.collect {|w, v| [w, v + (wafer_values.keys.index(w).even? ? 100 : 0)]}]
    cnt_ooc = 0
    wafer_values.each {|w, v| cnt_ooc += 1 if wafer_values.keys.index(w).even?}
    cnt_ooc *= @@parameters.size
    dcr = Space::DCR.new(lot: @@lot, eqp: @@eqp, op: @@mpd_qa, chambers: @@chambers, technology: @@technology)
    dcr.add_parameters(@@parameters, wafer_values)
    res = $client.send_dcr(dcr, export: :caller)
    assert $spctest.verify_dcr_accepted(res, @@parameters.size*wafer_values.size, nooc: cnt_ooc*2), 'DCR not submitted successfully'
    assert $spctest.verify_aqv_message(dcr: dcr, lot: @@lot, wafer: wafer_values.keys.sort, result: 'success'), 'AQV message not as expected (success)'
    #
    # verify wafer script parameters set
    test_ok = true
    wafer_values.each_key {|w|
      res = $sv.user_parameter('Wafer', w, name: 'OOC')
      if wafer_values.keys.index(w).even?
        test_ok &= (res.value =='Yes')
      else
        test_ok &= !res.valueflag
      end
    }
    assert test_ok, 'wafer script parameters not set as expected'
    # verify external comments
    assert $spctest.verify_ecomment(channels, 'SET_WAFER_SCRIPT_PARAMETER: reason={OOC/Yes}')
  end
end
