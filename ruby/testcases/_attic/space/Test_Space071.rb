=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: U. Koerner, 2012-01-11

History:
  2014-02-05 ssteidte,  separated Test_Space07, code cleanup
=end

require_relative 'Test_Space_Setup'


# Test Space/SIL CAs
class Test_Space071 < Test_Space_Setup
  @@mpd_qa = 'QA-MB-FTDEPM-LIS-CAEX.01'


  def setup
    super
    return unless $lds && self.class.class_variable_defined?(:@@lot) # bypass test0000_setup
    assert_equal 0, $sv.eqp_cleanup(@@eqp)
    assert_equal 0, $sv.lot_cleanup(@@lot)
    assert_equal 0, $sv.inhibit_cancel(:route, $sv.lot_info(@@lot).route)
    #
    $lds.all_spc_channels('QA_PARAM_90*').each {|ch| assert ch.delete}
    folder = $lds.folder(@@autocreated_qa)
    assert folder.delete_channels, "error cleaning channels" if folder
    @@chamber_a = 'PM1'
    @@chamber_b = 'PM2'
    @@chamber_c = 'CHX'
    @@chambers = {@@chamber_a=>5, @@chamber_b=>6, @@chamber_c=>7}
  end


  def test0700_setup
    $setup_ok = false
    #
    assert @@ppd_qa = $sildb.pd_reference(mpd: @@mpd_qa).first, "no PD reference found for #{@@mpd_qa}"
    #
    ['LotHold', 'AutoLotHold', 'ReleaseLotHold', 'CancelLotHold',
     'EquipmentInhibit', 'AutoEquipmentInhibit', 'ReleaseEquipmentInhibit', 'ReleaseEquipmentInhibit-noDefault',
     'ChamberInhibit', 'AutoChamberInhibit', 'ReleaseChamberInhibit', 'ReleaseChamberInhibit-noDefault',
     'ReticleInhibit', 'AutoReticleInhibit', 'ReleaseReticleInhibit',
     'RouteInhibit', 'AutoRouteInhibit', 'ReleaseRouteInhibit',
     'MachineRecipeInhibit', 'AutoMachineRecipeInhibit', 'ReleaseMachineRecipeInhibit', 'ReleaseMachineRecipeInhibit-noDefault',
     'AutoSetWaferScriptParameter-OOC', 'AutoSetLotScriptParameter-OOC', 'AutoSetLotScriptParameter-OOCInteger', 'SetLotScriptParameter-OOC',
     'AutoUnknown', 'Unknown', 'CACollection-ReleaseAll',
     'AutoChamberAndRecipeInhibit-Multi',
     'ReleaseEquipmentAndRecipeInhibit', 'ReleaseEquipmentAndRecipeInhibit-noDeflt', 'CACollection-ReleaseAllRecipeInhibits'
    ].each {|action|
      assert $spc.corrective_action(action), "CA #{action.inspect} not defined"
    }
    #
    $setup_ok = true
  end

  def test0710_equipment_inhibit
    channels = create_clean_channels
    assert $spctest.assign_verify_cas(channels,
      ['EquipmentInhibit', 'AutoEquipmentInhibit', 'ReleaseEquipmentInhibit', 'ReleaseEquipmentInhibit-noDefault']) #'AutoChamberInhibit',
    #
    dcrp = submit_process_dcr(@@ppd_qa, wafer_values: @@wafer_values2)
    # send DCR with OOC values
    dcr = Space::DCR.new(lot: @@lot, eqp: @@mtool, op: @@mpd_qa) #'MPD-NO-PROCPD.01')
    dcr.add_parameters(@@parameters, @@wafer_values2ooc)
    res = $client.send_dcr(dcr, export: :caller)
    assert $spctest.verify_dcr_accepted(res, @@parameters.size*@@wafer_values2ooc.size, nooc: 'all*2'), "DCR not submitted successfully"
    # #
    # verify inhibit and comment
    assert $spctest.verify_inhibit(@@eqp, channels.size, details: true, dcr: dcrp, lot: @@lot, spc_run: res, cj: dcr.controljob), "equipment must have an inhibit"
    assert_equal channels.size, $sv.inhibit_list(:eqp, @@eqp).count, "equipment must have an inhibit"
    assert $spctest.verify_ecomment(channels, "TOOL_HELD: #{@@eqp}: reason={X-S")
    $log.info "EquipmentInhibit automatically set"
    #
    # try to release inhibit with noDefault attribute
    channels.each {|ch|
      s = ch.samples(ckc: true)[-1]
      assert !s.assign_ca($spc.corrective_action("ReleaseEquipmentInhibit-noDefault")), "wrongly assigned corrective action"
    }
    assert $spctest.verify_inhibit(@@eqp, channels.size, details: false, dcr: dcrp, lot: @@lot, spc_run: res), "equipment must still have all inhibits"
    $log.info "EquipmentInhibits correctly not released"
    #
    # release inhibit with noDefault attribute - with comment
    channels.each {|ch|
      s = ch.samples(ckc: true)[-1]
      assert s.assign_ca($spc.corrective_action("ReleaseEquipmentInhibit-noDefault"), comment: "[Release=All]"), "error assigning CA"
    }
    assert $spctest.verify_inhibit(@@eqp, 0), "equipment must not have an inhibit"
    $log.info "EquipmentInhibits released"
    #
    # send process DCR again
    dcrp = submit_process_dcr(@@ppd_qa, wafer_values: @@wafer_values2)
    #
    assert_equal 0, $sv.lot_cleanup(@@lot)
    # send DCR with OOC values
    dcr = Space::DCR.new(lot: @@lot, eqp: @@mtool, op: @@mpd_qa)
    dcr.add_parameters(@@parameters, @@wafer_values2ooc)
    res = $client.send_dcr(dcr, export: :caller)
    assert $spctest.verify_dcr_accepted(res, @@parameters.size*@@wafer_values2ooc.size, nooc: 'all*2'), "DCR not submitted successfully"
    #
    # verify inhibit and comment
    assert $spctest.verify_inhibit(@@eqp, channels.size, details: false, dcr: dcrp, lot: @@lot, spc_run: res), "equipment must have an inhibit"
    assert_equal channels.size, $sv.inhibit_list(:eqp, @@eqp).count, "equipment must have an inhibit"
    assert $spctest.verify_ecomment(channels, "TOOL_HELD: #{@@eqp}: reason={X-S")
    $log.info "EquipmentInhibit automatically set"
    #
    # release inhibit
    channels.each {|ch|
      s = ch.samples(ckc: true)[-1]
      assert s.assign_ca($spc.corrective_action("ReleaseEquipmentInhibit")), "error assigning CA"
    }
    assert $spctest.verify_inhibit(@@eqp, 0), "equipment must not have an inhibit"
    $log.info "EquipmentInhibit released"
    #
    # set inhibit (manual)
    s = channels[0].samples(ckc: true)[-1]
    assert s.assign_ca($spc.corrective_action("EquipmentInhibit")), "error assigning CA"
    assert $spctest.verify_inhibit(@@eqp, 1, details: false, dcr: dcrp, lot: @@lot, spc_run: res), "equipment must have an inhibit"
    assert $spctest.verify_ecomment(channels, "TOOL_HELD: #{@@eqp}: reason={X-S")
    $log.info "EquipmentInhibit set (manually)"
    #
    # release inhibit
    assert s.assign_ca($spc.corrective_action("ReleaseEquipmentInhibit")), "error assigning CA"
    assert $spctest.verify_inhibit(@@eqp, 0), "equipment must not have an inhibit"
    $log.info "EquipmentInhibit released"
    #
    # set inhibit again (manual)
    assert s.assign_ca($spc.corrective_action("EquipmentInhibit")), "error assigning CA"
    assert $spctest.verify_inhibit(@@eqp, 1, details: false, dcr: dcrp, lot: @@lot, spc_run: res), "equipment must have an inhibit"
    assert $spctest.verify_ecomment(channels, "TOOL_HELD: #{@@eqp}: reason={X-S")
    $log.info "EquipmentInhibit set (manually)"
    #
    # release inhibit with noDefault attribute
    assert !s.assign_ca($spc.corrective_action("ReleaseEquipmentInhibit-noDefault")), "wrongly assigned corrective action"
    assert $spctest.verify_inhibit(@@eqp, 1, details: false, dcr: dcrp, lot: @@lot, spc_run: res), "equipment must still have an inhibit"
  end

  def test0711_equipment_inhibit_fallback
    mpd = "QA-NO-RESP-PD.01"
    assert_equal 0, $sv.eqp_cleanup(@@mtool)
    channels = create_clean_channels(mpd: mpd)
    assert $spctest.assign_verify_cas(channels, ['EquipmentInhibit', 'AutoEquipmentInhibit', 'ReleaseEquipmentInhibit'])
    #
    # send DCR with OOC values
    dcr = Space::DCR.new(lot: @@lot, eqp: @@mtool, op: mpd)
    dcr.add_parameters(@@parameters, @@wafer_values2ooc)
    res = $client.send_dcr(dcr, export: :caller)
    $log.info "Result: #{res.inspect}"
    assert $spctest.verify_dcr_accepted(res, @@parameters.size*@@wafer_values2ooc.size, nooc: 'all*2'), "DCR not submitted successfully"
    #
    # verify inhibit and comment
    assert $spctest.verify_inhibit(@@mtool, 0), "equipment must have no inhibit"
    assert_equal 0, $sv.inhibit_list(:eqp, @@mtool).count, "equipment must have no inhibit"
    if @@cafailure_lothold_fallback
      assert $spctest.verify_futurehold2(@@lot, "Execution of [AutoEquipmentInhibit] failed"), "lot must have a future hold"
      assert $spctest.verify_ecomment(channels, "LOT_HELD: #{@@lot}: reason={X-S")
    else
      @@parameters.each {|p|
        assert $spctest.verify_notification("Execution of [AutoEquipmentInhibit] failed", p), "no notification for #{p}"
      }
    end
    assert $spctest.verify_ecomment(channels, "Execution of [AutoEquipmentInhibit] failed")
  end

  def test0712_auto_and_manual_lot_and_equipment
    channels = create_clean_channels
    assert $spctest.assign_verify_cas(channels,
      ['EquipmentInhibit', 'AutoEquipmentInhibit', 'ReleaseEquipmentInhibit', 'LotHold', 'AutoLotHold', 'CancelLotHold'])
    #
    # send DCR with OOC values
    dcr = Space::DCR.new(lot: @@lot, eqp: @@mtool, op: @@mpd_qa)
    dcr.add_parameters(@@parameters, @@wafer_values2ooc)
    res = $client.send_dcr(dcr, export: :caller)
    assert $spctest.verify_dcr_accepted(res, @@parameters.size*@@wafer_values2ooc.size, nooc: 'all*2'), "DCR not submitted successfully"
    #
    assert $spctest.verify_futurehold2(@@lot, "[(Raw above specification)"), "lot must have a future hold"
    assert $spctest.verify_inhibit(@@eqp, 5), "equipment must have an inhibit"
    # check two ext. comments
    assert $spctest.verify_ecomment(channels, "LOT_HELD: #{@@lot}: reason={X-S")
    assert $spctest.verify_ecomment(channels, "TOOL_HELD: #{@@eqp}: reason={X-S")
    #
    # set lot hold and tool inhibit manually
    channels.each {|ch|
      s = ch.samples(ckc: true)[-1]
      sleep 5
      assert s.assign_ca($spc.corrective_action("LotHold")), "error assigning corective action"
      sleep 5
      assert s.assign_ca($spc.corrective_action("EquipmentInhibit")), "error assigning corective action"
    }
    # check for two lot holds
    count = @@spc_hold_reasons.size
    #sleep 10
    assert count <= $sv.lot_futurehold_list(@@lot).count, "count of future holds not as expected"
    assert $spctest.verify_futurehold2(@@lot, "[(Raw above specification)"), "lot must have a future hold"
    # check for two tool inhibits
    assert $spctest.verify_inhibit(@@eqp, count), "equipment must have an inhibit"
    # check for four ext comments
    assert $spctest.verify_ecomment(channels, "LOT_HELD: #{@@lot}: reason={X-S")
    assert $spctest.verify_ecomment(channels, "TOOL_HELD: #{@@eqp}: reason={X-S")
    # cancel lot holds
    channels.each {|ch|
      # CancelLotHold (from manual action)
      s = ch.samples(ckc: true)[-1]
      assert s.assign_ca($spc.corrective_action("CancelLotHold")), "error assigning corective action"
      # CancelLotHold (from automatic action)
      s = ch.samples(ckc: true)[-2]
      assert s.assign_ca($spc.corrective_action("CancelLotHold")), "error assigning corective action"
    }
    # check for no lot holds
    assert $spctest.verify_futurehold_cancel(@@lot, "[(Raw above specification)"), "lot must have no future hold"
    # release inhibits
    channels.each {|ch|
      # ReleaseInhibit (from manual action)
      s = ch.samples(ckc: true)[-1]
      assert s.assign_ca($spc.corrective_action("ReleaseEquipmentInhibit")), "error assigning corective action"
      # ReleaseInhibit (from automatic action)
      s = ch.samples(ckc: true)[-2]
      assert s.assign_ca($spc.corrective_action("ReleaseEquipmentInhibit")), "error assigning corective action"
    }
    # check for no tool inhibits
    assert $spctest.verify_inhibit(@@eqp, 0), "equipment must not have an inhibit"
  end

  def test0713_auto_and_manual_lot_and_equipment_eqp_inhibit_fails
    assert_equal 0, $sv.lot_opelocate(@@lot, "first")
    nopd = "QA-NO-RESP-PD.01"
    channels = create_clean_channels(mpd: nopd)
    assert $spctest.assign_verify_cas(channels,
      ['EquipmentInhibit', 'AutoEquipmentInhibit', 'ReleaseEquipmentInhibit', 'LotHold', 'AutoLotHold', 'CancelLotHold'])
    #
    # send DCR with OOC values
    dcr = Space::DCR.new(lot: @@lot, eqp: @@mtool, op: nopd)
    dcr.add_parameters(@@parameters, @@wafer_values2ooc)
    res = $client.send_dcr(dcr, export: :caller)
    assert $spctest.verify_dcr_accepted(res, @@parameters.size*@@wafer_values2ooc.size, nooc: 'all*2'), "DCR not submitted successfully"
    # check lot hold
    assert $spctest.verify_futurehold2(@@lot, "[(Raw above specification)"), "lot must have a future hold"
    count = @@cafailure_lothold_fallback ? 6 : 5
    assert count <= $sv.lot_futurehold_list(@@lot).count, "wrong number of future holds"
    # check two ext. comments
    assert $spctest.verify_ecomment(channels, "LOT_HELD: #{@@lot}: reason={X-S")
    assert $spctest.verify_ecomment(channels, "Execution of [AutoEquipmentInhibit] failed: reason={Error in SiView")
    #
    # release lot holds
    assert_equal 0, $sv.lot_gatepass(@@lot), "SiView GatePass error"
    assert $sv.lot_hold_list(@@lot, type: "FutureHold").size > 0, "lot must be on hold"
    channels.each {|ch|
      s = ch.samples(ckc: true)[-1]
      assert s.assign_ca($spc.corrective_action("ReleaseLotHold")), "error assigning corective action"
    }
    assert $spctest.verify_lothold_released(@@lot, "[(Raw above specification)"), "no further holds expected"
    assert $spctest.verify_futurehold_cancel(@@lot, "[(Raw above specification)"), "lot must have no future hold"
  end

  def test0714_equipment_inhibit_route_inhibit
    route = $sv.lot_info(@@lot).route
    assert_equal 0, $sv.inhibit_cancel(:route, route)

    channels = create_clean_channels
    ch1 = channels.select {|e| channels.index(e).odd?}
    ch2 = channels.select {|e| channels.index(e).even?}
    assert $spctest.assign_verify_cas(ch1, "AutoEquipmentInhibit")
    assert $spctest.assign_verify_cas(ch2, "AutoRouteInhibit")
    # send process DCRs
    submit_process_dcr(@@ppd_qa, wafer_values: @@wafer_values2)
    # send DCR with OOC values
    dcr = Space::DCR.new(lot: @@lot, eqp: @@mtool, op: @@mpd_qa, route: route)
    dcr.add_parameters(@@parameters, @@wafer_values2ooc)
    res = $client.send_dcr(dcr, export: :caller)
    assert $spctest.verify_dcr_accepted(res, @@parameters.size*@@wafer_values2ooc.size, nooc: 'all*2'), "DCR not submitted successfully"
    #
    assert $spctest.verify_inhibit(route, ch2.size, class: :route), "route must have an inhibit"
    assert $spctest.verify_inhibit(@@eqp, ch1.size), "equipment must have an inhibit"
    assert $spctest.verify_ecomment(ch1, "TOOL_HELD: #{@@eqp}: reason={X-S")
    assert $spctest.verify_ecomment(ch2, "ROUTE_HELD:#{route}: reason={X-S")
  end

  def test0715_additional_inhibit # modified test0710
    channels = create_clean_channels
    assert $spctest.assign_verify_cas(channels,
      ["EquipmentInhibit", "AutoEquipmentInhibit", "ReleaseEquipmentInhibit", "ReleaseEquipmentInhibit-noDefault"])
    # set "external" inhibit with reason code X-S1 and no end date
    assert_equal 0, $sv.inhibit_entity(:eqp, @@eqp, tend: '', reason: 'X-S1')
    #
    submit_process_dcr(@@ppd_qa, wafer_values: @@wafer_values2, chambers: @@chambers, processing_areas: @@chambers.keys)
    # send DCR with OOC values
    dcr = Space::DCR.new(lot: @@lot, eqp: @@mtool, op: @@mpd_qa)
    dcr.add_parameters(@@parameters, @@wafer_values2ooc)
    res = $client.send_dcr(dcr, export: :caller)
    assert $spctest.verify_dcr_accepted(res, @@parameters.size*@@wafer_values2ooc.size, nooc: 'all*2'), "DCR not submitted successfully"
    #
    # verify inhibit count
    assert wait_for {$sv.inhibit_list(:eqp, @@eqp).size == channels.size + 1}, "wrong inhibit count"
    # release inhibit with noDefault attribute - with comment
    channels.each {|ch|
      s = ch.samples(ckc: true)[-1]
      assert s.assign_ca($spc.corrective_action("ReleaseEquipmentInhibit-noDefault"), comment: "[Release=All]"), "error assigning CA"
    }
    assert wait_for {$sv.inhibit_list(:eqp, @@eqp).size == 1}, "wrong inhibit count"
    assert_equal 'X-S1', $sv.inhibit_list(:eqp, @@eqp).first.reason
    #
    # send process DCR again
    #
    submit_process_dcr(@@ppd_qa, wafer_values: @@wafer_values2)
    #
    assert_equal 0, $sv.lot_cleanup(@@lot)
    # send DCR with OOC values
    dcr = Space::DCR.new(lot: @@lot, eqp: @@mtool, op: @@mpd_qa)
    dcr.add_parameters(@@parameters, @@wafer_values2ooc)
    res = $client.send_dcr(dcr, export: :caller)
    assert $spctest.verify_dcr_accepted(res, @@parameters.size*@@wafer_values2ooc.size, nooc: 'all*2'), "DCR not submitted successfully"
    #
    # verify inhibit count
    assert wait_for {$sv.inhibit_list(:eqp, @@eqp).size == channels.size + 1}, "wrong inhibit count"
    #
    # release inhibit
    channels.each {|ch|
      s = ch.samples(ckc: true)[-1]
      assert s.assign_ca($spc.corrective_action("ReleaseEquipmentInhibit")), "error assigning CA"
    }
    assert wait_for {$sv.inhibit_list(:eqp, @@eqp).size == 1}, "wrong inhibit count"
    assert_equal 'X-S1', $sv.inhibit_list(:eqp, @@eqp).first.reason
    $log.info "EquipmentInhibit released"
    #
    # set inhibit (manual)
    s = channels[0].samples(ckc: true)[-1]
    assert s.assign_ca($spc.corrective_action("EquipmentInhibit")), "error assigning CA"
    assert wait_for {$sv.inhibit_list(:eqp, @@eqp).size == 2}, "wrong inhibit count"
    $log.info "EquipmentInhibit set (manually)"
    #
    # release inhibit
    assert s.assign_ca($spc.corrective_action("ReleaseEquipmentInhibit")), "error assigning CA"
    assert wait_for {$sv.inhibit_list(:eqp, @@eqp).size == 1}, "wrong inhibit count"
    assert_equal 'X-S1', $sv.inhibit_list(:eqp, @@eqp).first.reason
    $log.info "EquipmentInhibit released"
  end

  def xxtest0716_equipment_inhibit_multilot
    channels = create_clean_channels
    assert $spctest.assign_verify_cas(channels,
      ['EquipmentInhibit', 'AutoEquipmentInhibit', 'ReleaseEquipmentInhibit', 'ReleaseEquipmentInhibit-noDefault'])
    #
    submit_process_dcr(@@ppd_qa, wafer_values: @@wafer_values2)
    # send DCR with OOC values
    dcr = Space::DCR.new(lot: @@lot, eqp: @@mtool, op: @@mpd_qa)

    values = @@wafer_values2ooc.values
    wafer_values = {}
    lot_wafers = $sv.lot_info(@@lot,:wafers=>true).wafers.take(@@wafer_values2ooc.count)
    lot_wafers.each_with_index {|w,j| wafer_values[w.wafer] = values[j]}
    dcr.add_parameters(@@parameters, wafer_values)

    additional_lots = 2
    additional_lots.times {|i|
      lotcount = (i + 1).to_s
      lot = 'SIL' + @@sil_version.split('.').collect {|e| '%02d' % e}.join('') + '%02d' % @@test_run + '%02d' % lotcount + '.000'
      lot = $svtest.new_lot(lot: lot) unless $sv.lot_exists?(lot)
      dcr.add_lot(lot)
      values = @@wafer_values2ooc.values
      wafer_values = {}
      lot_wafers = $sv.lot_info(lot,:wafers=>true).wafers.take(@@wafer_values2ooc.count)
      lot_wafers.each_with_index {|w,j| wafer_values[w.wafer] = values[j]}
      #
      dcr.add_parameters(@@parameters, wafer_values, separate_lot: true)
    }

    res = $client.send_dcr(dcr, export: :caller)
    assert $spctest.verify_dcr_accepted(res, @@parameters.size*@@wafer_values2ooc.size*(additional_lots+1), nooc: 'all*2'), "DCR not submitted successfully"
    #
    # verify inhibit and comment
    assert $spctest.verify_inhibit(@@eqp, channels.size), "equipment must have an inhibit"
    assert_equal channels.size, $sv.inhibit_list(:eqp, @@eqp).count, "equipment must have an inhibit"
    assert $spctest.verify_ecomment(channels, "TOOL_HELD: #{@@eqp}: reason={X-S")
    $log.info "EquipmentInhibit automatically set"
    #
    $log.info 'Please verify that EquipmentInhibit is set with 3 associated lots. When ready press any key to continue.'
    gets
    # try to release inhibit with noDefault attribute
    channels.each {|ch|
      s = ch.samples(ckc: true)[-1]
      assert !s.assign_ca($spc.corrective_action("ReleaseEquipmentInhibit-noDefault")), "wrongly assigned corrective action"
    }
    assert $spctest.verify_inhibit(@@eqp, channels.size), "equipment must still have all inhibits"
    $log.info "EquipmentInhibits correctly not released"
    #
    # release inhibit with noDefault attribute - with comment
    channels.each {|ch|
      s = ch.samples(ckc: true)[-1]
      assert s.assign_ca($spc.corrective_action("ReleaseEquipmentInhibit-noDefault"), comment: "[Release=All]"), "error assigning CA"
    }
    assert $spctest.verify_inhibit(@@eqp, 0), "equipment must not have an inhibit"
    $log.info "EquipmentInhibits released"
    #
    # send process DCR again
    submit_process_dcr(@@ppd_qa, wafer_values: @@wafer_values2)
    # send DCR with OOC values
    dcr = Space::DCR.new(lot: @@lot, eqp: @@mtool, op: @@mpd_qa)
    dcr.add_parameters(@@parameters, @@wafer_values2ooc)
    res = $client.send_dcr(dcr, export: :caller)
    assert $spctest.verify_dcr_accepted(res, @@parameters.size*@@wafer_values2ooc.size, nooc: 'all*2'), "DCR not submitted successfully"
    #
    # verify inhibit and comment
    assert $spctest.verify_inhibit(@@eqp, channels.size), "equipment must have an inhibit"
    assert_equal channels.size, $sv.inhibit_list(:eqp, @@eqp).count, "equipment must have an inhibit"
    assert $spctest.verify_ecomment(channels, "TOOL_HELD: #{@@eqp}: reason={X-S")
    $log.info "EquipmentInhibit automatically set"
    #
    # release inhibit
    channels.each {|ch|
      s = ch.samples(ckc: true)[-1]
      assert s.assign_ca($spc.corrective_action("ReleaseEquipmentInhibit")), "error assigning CA"
    }
    assert $spctest.verify_inhibit(@@eqp, 0), "equipment must not have an inhibit"
    $log.info "EquipmentInhibit released"
    #
    # set inhibit (manual)
    s = channels[0].samples(ckc: true)[-1]
    assert s.assign_ca($spc.corrective_action("EquipmentInhibit")), "error assigning CA"
    assert $spctest.verify_inhibit(@@eqp, 1), "equipment must have an inhibit"
    assert $spctest.verify_ecomment(channels, "TOOL_HELD: #{@@eqp}: reason={X-S")
    $log.info "EquipmentInhibit set (manually)"
    #
    # release inhibit
    assert s.assign_ca($spc.corrective_action("ReleaseEquipmentInhibit")), "error assigning CA"
    assert $spctest.verify_inhibit(@@eqp, 0), "equipment must not have an inhibit"
    $log.info "EquipmentInhibit released"
    #
    # set inhibit again (manual)
    assert s.assign_ca($spc.corrective_action("EquipmentInhibit")), "error assigning CA"
    assert $spctest.verify_inhibit(@@eqp, 1), "equipment must have an inhibit"
    assert $spctest.verify_ecomment(channels, "TOOL_HELD: #{@@eqp}: reason={X-S")
    $log.info "EquipmentInhibit set (manually)"
    #
    # release inhibit with noDefault attribute
    assert !s.assign_ca($spc.corrective_action("ReleaseEquipmentInhibit-noDefault")), "wrongly assigned corrective action"
    assert $spctest.verify_inhibit(@@eqp, 1), "equipment must still have an inhibit"
  end

end
