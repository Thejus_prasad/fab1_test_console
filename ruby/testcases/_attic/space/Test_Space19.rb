=begin 
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.
    
Author: Conny Stenzel, 2012-06-27
=end

require_relative 'Test_Space_Setup'


# MPC Chart release test, manual interaction required
class Test_Space19 < Test_Space_Setup
  def setup
    super
    return unless $lds && self.class.class_variable_defined?(:@@lot) # bypass test0000_setup
    assert_equal 0, $sv.eqp_cleanup(@@eqp)
    assert_equal 0, $sv.lot_cleanup(@@lot)
    #
    [$lds, $lds_setup].each {|lds|
      lds.all_spc_channels('QA_PARAM_90*').each {|ch| assert ch.delete}
      folder = lds.folder(@@autocreated_qa)
      assert folder.delete_channels, 'error cleaning channels' if folder
    }
  end

  def test1901_mpc_ooc
    parameters = ['QA_PARAM_920_TG19', 'QA_PARAM_921_TG19', 'QA_PARAM_922_TG19', 'QA_PARAM_923_TG19', 'QA_PARAM_924_TG19']
    channels = create_clean_channels(department: 'QAMPC', mtool: @@mtool, eqp: @@eqp, mpd: 'QA-MPCM.01', ppd: 'QA-MPC.01', 
      wafer_values: @@wafer_values, parameters: parameters,
      actions: ['LotHold', 'AutoLotHold', 'ReleaseLotHold', 'CancelLotHold', 'EquipmentInhibit', 'AutoEquipmentInhibit', 'ReleaseEquipmentInhibit'])
    #
    # build and submit DCR
    dcr = Space::DCR.new(eqp: @@mtool, lot: @@lot, department: 'QAMPC', op: 'QA-MPCM.01')
    parameters.each_with_index {|p, i|
      wafer_values = @@wafer_values
      wafer_values = Hash[*@@wafer_values.collect {|w, v| [w, v + (i * i + 100)]}.flatten] if i.even?
      dcr.add_parameter(p, wafer_values)
    }
    res = $client.send_dcr(dcr, export: :caller)
    assert $spctest.verify_dcr_accepted(res, parameters.size * @@wafer_values.size), 'DCR not submitted successfully'
    #
    # verify number of samples
    folder = $lds.folder('AutoCreated_QAMPC')
    parameters.each_with_index {|p,i|
      ch = folder.spc_channel(parameter: p)
      samples = ch.samples
      assert_equal @@wafer_values.size*2, samples.size, "wrong number of samples in #{ch.inspect}"
    }
    $log.info "equipment inhibits: #{$sv.inhibit_list(:eqp, @@mtool).pretty_inspect}"
    $log.info 'Manual interaction neccessary: Open an MPC chart in AutoCreated_QAMPC (press M in Navigator) from a CKC.'
    $log.info 'Select ReleaseEquipmentInhibit and then CancelLotHold. When ready press any key to continue.'
    gets
    $log.info 'waiting 60s...'
    sleep 60
    assert_equal 0, $sv.lot_futurehold_list(@@lot).size, 'lot must have no future hold'
    assert $spctest.verify_inhibit(@@mtool, 0), 'equipment must not have an inhibit'
  end
end
