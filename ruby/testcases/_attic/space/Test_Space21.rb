=begin 
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.
    
Author: Conny Stenzel, 2012-07-25
=end

require_relative 'Test_Space_Setup'


# Test Space/SIL.
class Test_Space21 < Test_Space_Setup
  # default.mean.control.limit.coef is used for requirement 1-5, normal condition where at least 2 values exist.
  # default.mean.partialBounds.control.limit.coef.2 & default.mean.partialBounds.control.limit.coef.3 are used for requirement 6-9, 
  # calculation where only 1 value exists and / or the value is < 0 (negative)
  @@default_mean_partialBounds_control_limit_coef_2 = 0.875
  @@default_mean_partialBounds_control_limit_coef_3 = 1.125
  #Mean - If NO parameter name matches
  @@default_mean_control_limit_coef = 0.4
  @@default_mean_control_limit_center_divisor = 2
  #Range - If NO parameter name matches
  @@default_range_control_limit_center_divisor = 2 
  @@default_range_control_limit_center_coef = 2    
  #Sigma - If NO parameter name matches
  @@default_stdev_control_limit_divisor = 3.0  
  @@default_stdev_control_limit_FullBounds_center_divisor = 6    
  @@default_stdev_control_limit_PartialBounds_center_divisor = 3
  
  #Single parameter setup
  prm900modifieres = {autoUCL_MEAN: 0.1, autoCenterDiv_MEAN: 1, autoUCL_STDEV: 1, autoFullBoundsCenterDiv_STDEV: 1, 
                                autoPartialBoundsCenterDiv_STDEV: 1, autoCenterDiv_RANGE: 1, autoCenterCoeff_RANGE: 1}
  prm901modifieres = {autoUCL_MEAN: 0.2, autoCenterDiv_MEAN: 2, autoUCL_STDEV: 2, autoFullBoundsCenterDiv_STDEV: 2, 
                                autoPartialBoundsCenterDiv_STDEV: 2, autoCenterDiv_RANGE: 2, autoCenterCoeff_RANGE: 2}
  prm902modifieres = {autoUCL_MEAN: 0.3, autoCenterDiv_MEAN: 3, autoUCL_STDEV: 3, autoFullBoundsCenterDiv_STDEV: 3, 
                                autoPartialBoundsCenterDiv_STDEV: 3, autoCenterDiv_RANGE: 3, autoCenterCoeff_RANGE: 3}
  prm903modifieres = {autoUCL_MEAN: 0.4, autoCenterDiv_MEAN: 4, autoUCL_STDEV: 4, autoFullBoundsCenterDiv_STDEV: 4, 
                                autoPartialBoundsCenterDiv_STDEV: 4, autoCenterDiv_RANGE: 4, autoCenterCoeff_RANGE: 4}
  prm904modifieres = {autoUCL_MEAN: 0.5, autoCenterDiv_MEAN: 5, autoUCL_STDEV: 5, autoFullBoundsCenterDiv_STDEV: 5, 
                                autoPartialBoundsCenterDiv_STDEV: 5, autoCenterDiv_RANGE: 5, autoCenterCoeff_RANGE: 5}
  @@prms_mods = {"QA_PARAM_900MOD" => prm900modifieres, "QA_PARAM_901MOD" => prm901modifieres, "QA_PARAM_902MOD" => prm902modifieres,
               "QA_PARAM_903MOD" => prm903modifieres, "QA_PARAM_904MOD" => prm904modifieres}
  
  def setup
    super
    return unless $lds && self.class.class_variable_defined?(:@@lot) # bypass test0000_setup
    assert_equal 0, $sv.eqp_cleanup(@@eqp)
    assert_equal 0, $sv.lot_cleanup(@@lot)
    #
    [$lds, $lds_setup].each {|lds|
      lds.all_spc_channels('QA_PARAM_90*').each {|ch| assert ch.delete}
      folder = lds.folder(@@autocreated_qa)
      assert folder.delete_channels, 'error cleaning channels' if folder
    }
  end

  def test2100_setup
    $setup_ok = false
    #
    $spctest.sil_upload_verify_autocharting 'etc/testcasedata/space/Auto_Charting_STAG.SpecID.xlsx' if $spctest.env == 'f8stag'
    #$spctest.sil_upload_verify_speclimit('etc/testcasedata/space/SpecLimits_ITDC.SpecID.xlsx') if $spctest.env == 'itdc' or $spctest.env == 'let'
    #
    props = $spctest.silserver.read_spiseppprops
    @@default_mean_partialBounds_control_limit_coef_2 = props[:'default.mean.partialBounds.control.limit.coef.2'].to_f
    @@default_mean_partialBounds_control_limit_coef_3 = props[:'default.mean.partialBounds.control.limit.coef.3'].to_f
    @@default_mean_control_limit_coef = props[:'default.mean.control.limit.coef'].to_f
    @@default_mean_control_limit_center_divisor = props[:'default.mean.control.limit.center.divisor'].to_f
    @@default_range_control_limit_center_divisor = props[:'default.range.control.limit.center.divisor'].to_f
    @@default_range_control_limit_center_coef = props[:'default.range.control.limit.center.coef'].to_f
    @@default_stdev_control_limit_divisor = props[:'default.stdev.control.limit.divisor'].to_f
    @@default_stdev_control_limit_FullBounds_center_divisor = props[:'default.stdev.control.limit.FullBounds.center.divisor'].to_f
    @@default_stdev_control_limit_PartialBounds_center_divisor = props[:'default.stdev.control.limit.PartialBounds.center.divisor'].to_f
    #
    @@modifier_params = ['QA_PARAM_900MOD', 'QA_PARAM_901MOD', 'QA_PARAM_902MOD', 'QA_PARAM_903MOD', 'QA_PARAM_904MOD']
    channel_modifier_parameternames = props[:'channel.modifier.parameternames'].split(',')
    # check if the parameters with special coefs/divisors are configured in the spisepp.properties file
    assert channel_modifier_parameternames & @@modifier_params == @@modifier_params
    
    prm900modifieres = {autoUCL_MEAN: props[:'QA_PARAM_900MOD.AutoUCL_MEAN'].to_f, autoCenterDiv_MEAN: props[:'QA_PARAM_900MOD.AutoCenterDiv_MEAN'].to_f, 
                        autoUCL_STDEV: props[:'QA_PARAM_900MOD.AutoUCL_STDEV'].to_f, autoFullBoundsCenterDiv_STDEV: props[:'QA_PARAM_900MOD.AutoFullBoundsCenterDiv_STDEV'].to_f, 
                        autoPartialBoundsCenterDiv_STDEV: props[:'QA_PARAM_900MOD.AutoFullBoundsCenterDiv_STDEV'].to_f, autoCenterDiv_RANGE: props[:'QA_PARAM_900MOD.AutoCenterDiv_RANGE'].to_f, 
                        autoCenterCoeff_RANGE: props[:'QA_PARAM_900MOD.AutoCenterCoeff_RANGE'].to_f}
    prm901modifieres = {autoUCL_MEAN: props[:'QA_PARAM_901MOD.AutoUCL_MEAN'].to_f, autoCenterDiv_MEAN: props[:'QA_PARAM_901MOD.AutoCenterDiv_MEAN'].to_f, 
                        autoUCL_STDEV: props[:'QA_PARAM_901MOD.AutoUCL_STDEV'].to_f, autoFullBoundsCenterDiv_STDEV: props[:'QA_PARAM_901MOD.AutoFullBoundsCenterDiv_STDEV'].to_f, 
                        autoPartialBoundsCenterDiv_STDEV: props[:'QA_PARAM_901MOD.AutoFullBoundsCenterDiv_STDEV'].to_f, autoCenterDiv_RANGE: props[:'QA_PARAM_901MOD.AutoCenterDiv_RANGE'].to_f, 
                        autoCenterCoeff_RANGE: props[:'QA_PARAM_901MOD.AutoCenterCoeff_RANGE'].to_f}
    prm902modifieres = {autoUCL_MEAN: props[:'QA_PARAM_902MOD.AutoUCL_MEAN'].to_f, autoCenterDiv_MEAN: props[:'QA_PARAM_902MOD.AutoCenterDiv_MEAN'].to_f, 
                        autoUCL_STDEV: props[:'QA_PARAM_902MOD.AutoUCL_STDEV'].to_f, autoFullBoundsCenterDiv_STDEV: props[:'QA_PARAM_902MOD.AutoFullBoundsCenterDiv_STDEV'].to_f, 
                        autoPartialBoundsCenterDiv_STDEV: props[:'QA_PARAM_902MOD.AutoFullBoundsCenterDiv_STDEV'].to_f, autoCenterDiv_RANGE: props[:'QA_PARAM_902MOD.AutoCenterDiv_RANGE'].to_f, 
                        autoCenterCoeff_RANGE: props[:'QA_PARAM_902MOD.AutoCenterCoeff_RANGE'].to_f}
    prm903modifieres = {autoUCL_MEAN: props[:'QA_PARAM_903MOD.AutoUCL_MEAN'].to_f, autoCenterDiv_MEAN: props[:'QA_PARAM_903MOD.AutoCenterDiv_MEAN'].to_f, 
                        autoUCL_STDEV: props[:'QA_PARAM_903MOD.AutoUCL_STDEV'].to_f, autoFullBoundsCenterDiv_STDEV: props[:'QA_PARAM_903MOD.AutoFullBoundsCenterDiv_STDEV'].to_f, 
                        autoPartialBoundsCenterDiv_STDEV: props[:'QA_PARAM_903MOD.AutoFullBoundsCenterDiv_STDEV'].to_f, autoCenterDiv_RANGE: props[:'QA_PARAM_903MOD.AutoCenterDiv_RANGE'].to_f, 
                        autoCenterCoeff_RANGE: props[:'QA_PARAM_903MOD.AutoCenterCoeff_RANGE'].to_f}
    prm904modifieres = {autoUCL_MEAN: props[:'QA_PARAM_904MOD.AutoUCL_MEAN'].to_f, autoCenterDiv_MEAN: props[:'QA_PARAM_904MOD.AutoCenterDiv_MEAN'].to_f, 
                        autoUCL_STDEV: props[:'QA_PARAM_904MOD.AutoUCL_STDEV'].to_f, autoFullBoundsCenterDiv_STDEV: props[:'QA_PARAM_904MOD.AutoFullBoundsCenterDiv_STDEV'].to_f, 
                        autoPartialBoundsCenterDiv_STDEV: props[:'QA_PARAM_904MOD.AutoFullBoundsCenterDiv_STDEV'].to_f, autoCenterDiv_RANGE: props[:'QA_PARAM_904MOD.AutoCenterDiv_RANGE'].to_f, 
                        autoCenterCoeff_RANGE: props[:'QA_PARAM_904MOD.AutoCenterCoeff_RANGE'].to_f}
    @@prms_mods = {"QA_PARAM_900MOD" => prm900modifieres, "QA_PARAM_901MOD" => prm901modifieres, "QA_PARAM_902MOD" => prm902modifieres,
               "QA_PARAM_903MOD" => prm903modifieres, "QA_PARAM_904MOD" => prm904modifieres}
      
    #
    $log.info "mean control coef: #{@@default_mean_control_limit_coef}"
    $setup_ok = true
  end

  def test2101_speclimits_sigma_value
    _send_check_limits({'SIGMA_LCL'=>2.0, 'RANGE_LCL'=>3.0}, ['QA_PARAM_940'], :fixed=>true)
    _send_check_limits({'SIGMA_LCL'=>2.0, 'RANGE_LCL'=>3.0}, ['QA_PARAM_940'], :fixed=>false)
    _send_check_limits({'SIGMA_LCL'=>0.0, 'RANGE_LCL'=>0.0}, ['QA_PARAM_940'], :fixed=>false)
  end

  def test2102_speclimits_sigma_disabled
    _send_check_limits({'SIGMA_LCL'=>nil, 'RANGE_LCL'=>nil}, ['QA_PARAM_940'], :fixed=>true, :enabled=>false)
    _send_check_limits({'SIGMA_LCL'=>nil, 'RANGE_LCL'=>nil}, ['QA_PARAM_940'], :fixed=>false, :enabled=>false)
  end

  def test2103_no_speclimits
    if @@parameters_missingspeclimit_upload_inline_enabled
      _send_check_limits({'SIGMA_LCL'=>2.0, 'RANGE_LCL'=>3.0}, ['QA_PARAM_NO_LIMITS'], :fixed=>true, :nolimits=>true)
    else
      $log.warn 'parameters without speclimits will not be uploaded'
    end
  end

  def _send_check_limits(sigma, parameters, params={})
    $log.info "Sigma: #{sigma}, Parameters: #{parameters}, params: #{params}"
    [$lds, $lds_setup].each {|lds|
      lds.all_spc_channels('QA_PARAM_90*').each {|ch| assert ch.delete}
      folder = lds.folder(@@autocreated_qa)
      assert folder.delete_channels, 'error cleaning channels' if folder
    }
    template = '_Template_QA_Limits'
    tmpl = $lds.folder(@@templates_folder).spc_channel(template)
    tmpl.set_limits(sigma, params)
    #
    # build and submit DCR with all parameters		
    dcr = Space::DCR.new(:eqp=>@@eqp, :lot=>@@lot, :department=>'QA')
    dcr.add_parameters(parameters, @@wafer_values)
    res = $client.send_dcr(dcr, :export=>"log/#{__method__}.xml")
		$log.info "Result: #{res.inspect}"
    nsamples = parameters.size * @@wafer_values.size
    assert $spctest.verify_dcr_accepted(res, nsamples), 'DCR not submitted successfully'
    # 
    folder = $lds.folder(@@autocreated_qa)
    parameters.each{|par|
      ch = folder.spc_channel(:parameter=>par)

      # ensure channel limits
      ch_limits = ch.limits(params)
      sigma.each_pair do |k,v|
        if v and not params[:nolimits]
          assert_in_delta v, ch_limits[k], 0.001, 'channel limits do not match'
        else
          assert_nil ch_limits[k], 'channel limits do not match' 
        end
      end

      break if params[:nolimits]

      # ensure spec and sample limits are set
      s = ch.samples[-1]
      
      # check also specs
      specs = s.specs
      assert_equal 45.0, specs['TARGET'], 'Target not as expected'
      assert_equal 75.0, specs['USL'], 'USL not as expected'
      assert_equal 25.0, specs['LSL'], 'LSL not as expected'
			
			limits = s.limits
			xbar_Target = specs['TARGET']
			xbar_UCL = specs['TARGET']+@@default_mean_control_limit_coef*(specs['USL']-specs['TARGET'])
			xbar_LCL = specs['TARGET']-@@default_mean_control_limit_coef*(specs['TARGET']-specs['LSL'])
			
			assert_equal xbar_Target.to_s, limits['MEAN_VALUE_CENTER'].to_s, 'MEAN Target not as expected'
      assert_equal xbar_UCL.to_s, limits['MEAN_VALUE_UCL'].to_s, 'MEAN UCL not as expected'
      assert_equal xbar_LCL.to_s, limits['MEAN_VALUE_LCL'].to_s, 'MEAN LCL not as expected'
			
			sigma_UCL = (specs['USL']-specs['LSL'])/@@default_stdev_control_limit_divisor
			sigma_CL = (specs['USL']-specs['LSL'])/@@default_stdev_control_limit_FullBounds_center_divisor

      assert ((sigma_UCL - limits['SIGMA_UCL']).abs <   0.0000000000002), 'SIGMA UCL not as expected'
      assert ((sigma_CL - limits['SIGMA_CENTER']).abs <   0.0000000000002), 'SIGMA CENTER not as expected'
      if sigma['SIGMA_LCL']
        assert_in_delta sigma['SIGMA_LCL'], limits['SIGMA_LCL'], 0.001, 'SIGMA LCL not as expected'
      else
        assert_nil limits['SIGMA_LCL'], 'No SGIMA LCL expected'
      end

			range_UCL = specs['USL']-specs['LSL']
			range_CL = (specs['USL']-specs['LSL'])/@@default_range_control_limit_center_divisor
      if sigma['RANGE_LCL']
        assert_in_delta sigma['RANGE_LCL'], limits['RANGE_LCL'], 0.001, 'RANGE_LCL not as expected'
      else
        assert_nil limits['RANGE_LCL'], 'No RANGE LCL expected'
      end

      assert_equal range_UCL.to_s, limits['RANGE_UCL'].to_s, 'RANGE UCL not as expected'
      assert_equal range_CL.to_s, limits['RANGE_CENTER'].to_s, 'RANGE CENTER not as expected'
      }
  end

  def test2110_speclimits_sigma_value
    
    _send_check_modified_limits({'SIGMA_LCL'=>0.0, 'RANGE_LCL'=>0.0}, @@modifier_params, :fixed=>false)
  end

  def _send_check_modified_limits(sigma, parameters, params={})
    $log.info "Sigma: #{sigma}, Parameters: #{parameters}, params: #{params}"
    [$lds, $lds_setup].each {|lds|
      lds.all_spc_channels('QA_PARAM_90*').each {|ch| assert ch.delete}
      folder = lds.folder(@@autocreated_qa)
      assert folder.delete_channels, 'error cleaning channels' if folder
    }
    template = '_Template_QA_Limits'
    tmpl = $lds.folder(@@templates_folder).spc_channel(template)
    tmpl.set_limits(sigma, params)
    #
    # build and submit DCR with all parameters		
    dcr = Space::DCR.new(:eqp=>@@eqp, :lot=>@@lot, :department=>'QA')
    dcr.add_parameters(parameters, @@wafer_values)
    res = $client.send_dcr(dcr, :export=>"log/#{__method__}.xml")
    nsamples = parameters.size * @@wafer_values.size
    assert $spctest.verify_dcr_accepted(res, nsamples), 'DCR not submitted successfully'
    # 
    folder = $lds.folder(@@autocreated_qa)
    parameters.each{|p|
      ch = folder.spc_channel(:parameter=>p)

      # ensure channel limits
      ch_limits = ch.limits(params)
      sigma.each_pair do |k,v|
        if v and not params[:nolimits]
          assert_in_delta v, ch_limits[k], 0.001, 'channel limits do not match'
        else
          assert_nil ch_limits[k], 'channel limits do not match' 
        end
      end

      break if params[:nolimits]

      # ensure spec and sample limits are set
      s = ch.samples[-1]
      
      # check also specs
      specs = s.specs
      assert_equal 50.0, specs['TARGET'], "#{p}: Target not as expected"
      assert_equal 95.0, specs['USL'], "#{p}: USL not as expected"
      assert_equal 15.0, specs['LSL'], "#{p}: LSL not as expected"
			
			limits = s.limits
			xbar_Target = specs['TARGET']
			xbar_UCL = specs['TARGET']+@@prms_mods[p][:autoUCL_MEAN]*(specs['USL']-specs['TARGET'])
			xbar_LCL = specs['TARGET']-@@prms_mods[p][:autoUCL_MEAN]*(specs['TARGET']-specs['LSL'])
			
			#xbar_UCL = specs['TARGET']+(@@default_mean_control_limit_coef)*(specs['USL']-specs['TARGET'])
			#xbar_LCL = specs['TARGET']-(@@default_mean_control_limit_coef)*(specs['TARGET']-specs['LSL'])
			
			assert_equal xbar_Target.to_s, limits['MEAN_VALUE_CENTER'].to_s, "#{p}: MEAN Target not as expected"
      assert_equal xbar_UCL.to_s, limits['MEAN_VALUE_UCL'].to_s, "#{p}: MEAN UCL not as expected"
      assert_equal xbar_LCL.to_s, limits['MEAN_VALUE_LCL'].to_s, "#{p}: MEAN LCL not as expected"
			
			limit_range = specs['USL']-specs['LSL']
			#$log.info "limit range: #{limit_range}, divider: #{@@prms_mods[p][:autoUCL_STDEV]}"
			sigma_UCL = limit_range/@@prms_mods[p][:autoUCL_STDEV]
			sigma_CL = limit_range/@@prms_mods[p][:autoFullBoundsCenterDiv_STDEV]
			
      assert_in_delta sigma_UCL, limits['SIGMA_UCL'].to_f, SpaceTest::Epsilon, "#{p}: SIGMA UCL not as expected"
      assert_in_delta sigma_CL, limits['SIGMA_CENTER'].to_f, SpaceTest::Epsilon, "#{p}: SIGMA CENTER not as expected"
      if sigma['SIGMA_LCL']
        assert_in_delta sigma['SIGMA_LCL'], limits['SIGMA_LCL'], 0.001, "#{p}: SIGMA LCL not as expected"
      else
        assert_nil limits['SIGMA_LCL'], "#{p}: No SIGMA LCL expected"
      end
      
			range_UCL = specs['USL']-specs['LSL']
			range_CL = (specs['USL']-specs['LSL'])/@@prms_mods[p][:autoCenterDiv_RANGE]
      if sigma['RANGE_LCL']
        assert_in_delta sigma['RANGE_LCL'], limits['RANGE_LCL'], 0.001, "#{p}: RANGE_LCL not as expected"
      else
        assert_nil limits['RANGE_LCL'], "#{p}: No RANGE LCL expected"
      end

      assert_equal range_UCL.to_s[0..10], limits['RANGE_UCL'].to_s[0..10], "#{p}: RANGE UCL not as expected"
      assert_equal range_CL.to_s[0..10], limits['RANGE_CENTER'].to_s[0..10], "#{p}: RANGE CENTER not as expected"
    }
  end
  
end
