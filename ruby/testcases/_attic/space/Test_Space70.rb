=begin
Test Space/SIL.

(c) Copyright 2012 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: D.Steger, 2012-08-08
=end

require_relative 'Test_Space_Setup'
require 'xsitetest' if $env == 'f8stag'


# Test SIL CKC specific Corrective Actions
class Test_Space70 < Test_Space_Setup
  @@mpd_qa = 'QA-MB-FTDEPM-LIS-CAEX.01'
  @@chambers = {'PM1'=>5, 'PM2'=>6, 'PM3'=>7, 'PM4'=>8, 'PM5'=>9}
  @@parameters = ['QA_PARAM_700', 'QA_PARAM_701', 'QA_PARAM_702', 'QA_PARAM_703', 'QA_PARAM_704']
  @@lots = ['SIL005.00', 'SIL006.00', 'SIL007.00']


  def test7000_setup
    $setup_ok = false

    if $env == 'f8stag'
      ($xs.disconnect; $xstest = nil) if $xs and $xs.env != $xs
      $xstest ||= XsiteTest.new($env, sv: $sv)
      $xs = $xstest.xs
    end
    #
    assert @@ppd_qa = $sildb.pd_reference(mpd: @@mpd_qa).first, "no PD reference found for #{@@mpd_qa}"
    #
    ['AutoLotHold', 'ReleaseLotHold',
      'AutoEquipmentInhibit', 'ReleaseEquipmentInhibit',
      'AutoChamberInhibit', 'ReleaseChamberInhibit',
      #TODO:'AutoReticleInhibit', 'ReleaseReticleInhibit',
      #TODO:'AutoRouteInhibit', 'ReleaseRouteInhibit',
      'AutoMachineRecipeInhibit', 'ReleaseMachineRecipeInhibit',
      'AutoSetWaferScriptParameter-OOC', 'AutoSetLotScriptParameter-OOC'
    ].each {|action|
      assert $spc.corrective_action(action), "CA #{action.inspect} not defined"
    }
    #
    $setup_ok = true
  end

  def test7001_ckc_per_chamber
    eqp = "#{@@eqp}_0"
    _setup_template_ekeys(5)
    channels = _run_scenario_per_pd([@@lot], ['AutoChamberInhibit', 'ReleaseChamberInhibit'])

    #
    # verify inhibit and comment
    (1..5).each do |ckcid|
      assert $spctest.verify_inhibit(eqp, channels.size, class: :eqp_chamber, msg: "CKC_ID->#{ckcid}"), 'chamber must have an inhibit'
    end
    return

    channels.each_with_index do |ch, i|
      ch.ckcs.each do |ckc|
        assert verify_ecomment_ckc(ckc, "TOOL+CHAMBER_HELD: #{eqp}/#{ckc.name}: reason={X-S")
        # Release violated sample in CKC
        s = ckc.samples[-1]
        assert s.assign_ca($spc.corrective_action('ReleaseChamberInhibit')), 'error assigning corective action'
        $log.info "ReleaseChamberInhibit at sample #{s} in channel #{ch} and ckc #{ckc}"
        sleep 5
      end
      assert $spctest.verify_inhibit(eqp, 5*(channels.size-i-1), class: :eqp_chamber, timeout: @@ca_time_out, msg: 'CKC_ID->'), 'chamber must have an inhibit'
    end
    assert $spctest.verify_inhibit(eqp, 0, class: :eqp_chamber), ' all chambers should be released'
  end

  def test7002_ckc_per_chamber_waferscriptparameter
    _setup_template_ekeys(5)
    lot_wafers = $sv.lot_info(@@lot,wafers: true).wafers.take(@@wafer_values2.values.count)

    lot_wafers.each {|w| assert $sv.user_parameter_delete_verify('Wafer', w.wafer, 'OOC')}

    _run_scenario_per_pd([@@lot], ['AutoSetWaferScriptParameter-OOC'])

    res = lot_wafers.inject(true) do |res,w|
      val = $sv.user_parameter('Wafer', w.wafer, name: 'OOC')
      res &= val.valueflag and (val.value == 'Yes')
    end
    assert res, ' not all script parameters set'
  end

  def test7003_ckc_per_recipe
    3.times {|i| assert $sv.inhibit_cancel(:recipe, "P-UTC001_#{i}.UTMR000.01")}

    _setup_template_ekeys(3)
    channels = _run_scenario_per_pd(@@lots, ['AutoMachineRecipeInhibit', 'ReleaseMachineRecipeInhibit'])
    # All, but first CKC has violations
    (2..4).each do |ckcid|
      mrecipe = "P-UTC001_#{ckcid-2}.UTMR000.01"
      assert $spctest.verify_inhibit(mrecipe, channels.size, class: :recipe, msg: "CKC_ID->#{ckcid}"), 'mrecipe must have an inhibit'
    end

    channels.each_with_index do |ch,i|
      ch.ckcs.each do |ckc|
        next if ckc.ckcid < 2
        mrecipe = "P-UTC001_#{ckc.ckcid-2}.UTMR000.01"
        assert verify_ecomment_ckc(ckc, "RECIPE_HELD: #{mrecipe}: reason={X-S")

        # Release violated sample in CKC
        s = ckc.samples[-1]
        assert s.assign_ca($spc.corrective_action('ReleaseMachineRecipeInhibit')), 'error assigning corective action'
      end
      (2..4).each do |ckcid|
        mrecipe = "P-UTC001_#{ckcid-2}.UTMR000.01"
        assert $spctest.verify_inhibit(mrecipe, channels.size-i-1, class: :recipe, timeout: @@ca_time_out, msg: "CKC_ID->#{ckcid}"), 'mrecipe must have an inhibit'
      end
    end
  end

  def test7004_ckc_per_tool
    _setup_template_ekeys(4)
    channels = _run_scenario_per_pd(@@lots, ['AutoEquipmentInhibit', 'ReleaseEquipmentInhibit'])

    # All, but first CKC has violations
    assert $spctest.verify_inhibit(@@eqp, 0), 'no inhibit on first tool'

    (2..4).each_with_index do |ckcid,i|
      eqp = "#{@@eqp}_#{i}"
      assert $spctest.verify_inhibit(eqp, channels.size, class: :eqp, msg: "CKC_ID->#{ckcid}"), 'mrecipe must have an inhibit'
    end

    channels.each_with_index do |ch,i|
      ch.ckcs.each do |ckc|
        next if ckc.ckcid < 2
        eqp = "#{@@eqp}_#{ckc.ckcid-2}"
        assert verify_ecomment_ckc(ckc, "TOOL_HELD: #{eqp}: reason={X-S")

        # Release violated sample in CKC
        s = ckc.samples[-1]
        assert s.assign_ca($spc.corrective_action('ReleaseEquipmentInhibit')), 'error assigning corective action'
      end
      (2..4).each_with_index do |ckcid,j|
        eqp = "#{@@eqp}_#{j}"
        assert $spctest.verify_inhibit(eqp, channels.size-i-1, class: :eqp, timeout: @@ca_time_out, msg: "CKC_ID->#{ckcid}"), 'tool must have an inhibit'
      end
    end
  end

  def test7005_ckc_per_product
    _setup_template_ekeys(9)
    channels = _run_scenario_per_pd(@@lots, ['AutoLotHold', 'ReleaseLotHold'])

    (2..4).each_with_index do |ckcid, i|
      assert $spctest.verify_futurehold(@@lots[i], "CKC_ID->#{ckcid}", nil, expected_holds: channels.count), ' expecting future hold'
    end

    @@lots.each {|lot| assert_equal 0, $sv.lot_gatepass(lot), 'SiView GatePass error'}

    channels.each_with_index do |ch,i|
      ch.ckcs.each_with_index do |ckc,k|
        next if ckc.ckcid < 2
        assert verify_ecomment_ckc(ckc, "LOT_HELD: #{@@lots[ckc.ckcid-2]}: reason={X-S")

        # Release violated sample in CKC
        s = ckc.samples[-1]
        assert s.assign_ca($spc.corrective_action('ReleaseLotHold')), 'error assigning corective action'
      end

      (2..4).each_with_index do |ckcid,j|
        if i > 3
          assert $spctest.verify_lothold_released(@@lots[j], nil, timeout: @@ca_time_out)
        else
          assert $sv.lot_hold_list(@@lots[j], type: 'FutureHold').count > 0, 'lot must be on hold'
        end
      end
    end
  end

  def test7006_ckc_per_product_lotscriptparameter
    _setup_template_ekeys(9)
    (@@lots+[@@lot]).each {|lot| assert $sv.user_parameter_delete_verify('Lot', lot, 'OOC')}
    _run_scenario_per_pd(@@lots, ['AutoSetLotScriptParameter-OOC'])

    res = @@lots.inject(true) do |res, lot|
      val = $sv.user_parameter('Lot', lot, name: 'OOC')
      res && val.valueflag && (val.value == 'Yes')
    end
    assert res, ' not all script parameters set'
    assert !$sv.user_parameter('Lot', @@lot, name: 'OOC').valueflag, 'script parameter should not be set'
  end

  def test7007_ckc_per_product_lot_parameter
    _setup_template_ekeys(9)
    lots = [@@lot] + @@lots
    channels = _run_scenario_per_pd(lots, ['AutoLotHold', 'ReleaseLotHold'], true, true)
    _check_futureholds(channels, lots)
  end

  def test7008_ckc_per_product_wafer_parameter
    _setup_template_ekeys(9)
    lots = [@@lot] + @@lots
    channels = _run_scenario_per_pd(lots, ['AutoLotHold', 'ReleaseLotHold'], true)
    _check_futureholds(channels, lots)
  end


  def test7009_ckc_per_tool_e10
    skip "not applicable in #{$env}" unless ['mtqa', 'f8stag'].member?($env)

    $xstest.cleanup_work_requests(@@eqp, chambers: true)
    _setup_template_ekeys(4)
    channels = _run_scenario_per_pd(@@lots, ['AutoSetE10StateToolDown'])

    # All, but first CKC has violations
    assert $spctest.verify_inhibit(@@eqp, 0), 'no inhibit on first tool'

    (2..4).each_with_index do |ckcid,i|
      eqp = "#{@@eqp}_#{i}"
      assert $spctest.verify_inhibit(eqp, 0, class: :eqp, msg: "CKC_ID->#{ckcid}"), 'no inhibt inhibit'
    end

    channels.each_with_index do |ch,i|
      ch.ckcs.each do |ckc|
        if ckc.ckcid < 2
          assert !$xstest.verify_work_request(@@eqp, 'SPC Violation UDT.DELAY'), 'work request not expected'
        else
          eqp = "#{@@eqp}_#{ckc.ckcid-2}"
          assert verify_ecomment_ckc(ckc, "E10_STATE_SET: #{eqp}: UDT")
        end
      end
    end
  end


  def _setup_template_ekeys(index)
    template = '_Template_QA_CKCS'
    ekeys = [Space::SpcApi::SpcCKey.new(index,'*'), Space::SpcApi::SpcCKey.new(6,'FIXED'), Space::SpcApi::SpcCKey.new(10,'FIXED')]
    tmpl = $lds.folder(@@templates_folder).spc_channel(template)
    assert tmpl, "Cannot find #{template}"
    tmpl.set_extractor_keys(ekeys)
  end

  def _check_futureholds(channels, lots)
    lots.each_with_index do |lot,i|
      if i.even?
        assert $spctest.verify_futurehold(lot, 'CKC_ID->', nil, expected_holds: channels.count), ' expecting future hold'
      else
        assert $spctest.verify_futurehold_cancel(lot, nil)
      end
    end

    channels.each_with_index do |ch,i|
      ch.ckcs.each do |ckc|
        next if ckc.ckcid < 2
        assert verify_ecomment_ckc(ckc, 'LOT_HELD:')
      end
    end
  end


  # verify ecomment per CKC, used in Test_Space70 only, moved here  sf 2017-08-04  TODO: cleanup
  def verify_ecomment_ckc(ckc, comment)
    return ckc.samples.inject(true) do |res, s|
      if s.violations.count > 0
        ($log.warn " #{s.parameter} #{s.extractor_keys["Wafer"]}: no external comment"; res = false) if s.ecomment.nil?
        found_index = s.ecomment.index(comment)
        if found_index
          if s.ecomment[found_index+1..-1].index(comment)
            ($log.warn " #{s.parameter} #{s.extractor_keys["Wafer"]}: comment more than once (#{comment.inspect}): #{s.ecomment.inspect}"; res = false)
          end
        else
          ($log.warn " #{s.parameter} #{s.extractor_keys["Wafer"]}: comment not as expected (#{comment.inspect}): #{s.ecomment.inspect}"; res = false)
        end
      end
      res
    end
  end



  def _run_scenario_per_pd(lots, actions, multilot_dcr=false, lot_parameters=false)
    caller = caller_locations(1,1)[0].label
    assert_equal 0, $sv.eqp_cleanup(@@eqp)
    assert_equal 0, $sv.lot_cleanup(@@lot)
    lots.each_with_index {|lot, i|
      assert_equal 0, $sv.eqp_cleanup("#{@@eqp}_#{i}")
      assert_equal 0, $sv.lot_cleanup(lot)
    }
    channels = create_clean_channels(processing_areas: 'cycle_wafer') # need cycling per wafer for correct ckc setup
    assert $spctest.assign_verify_cas(channels, actions)
    lot_wafer_values = []
    # send process DCR
    lots.each_with_index do |lot, i|
      lot_wafers = $sv.lot_info(lot, wafers: true).wafers.take(@@wafer_values2.values.count)
      wafer_values = {}
      lot_wafers.each_with_index {|w, j| wafer_values[w.wafer] = @@wafer_values2.values[j]}

      submit_process_dcr(@@ppd_qa, wafer_values: wafer_values, processing_areas: 'cycle_wafer',
        lot: lot, logical_recipe: "F-P-FTEOS8K15.0#{i}", machine_recipe: "P-UTC001_#{i}.UTMR000.01", product: "SILPROD#{i}.01",
        eqp: "#{@@eqp}_#{i}", export: "log/#{caller}_p#{i}.xml")
      lot_wafer_values << wafer_values
    end
    $log.info "  lot_wafer_values: #{lot_wafer_values}"


    # send DCR with OOC values
    if multilot_dcr
      dcr = Space::DCR.new(eqp: @@mtool, op: @@mpd_qa, lot: nil)
      lots.each_with_index do |lot,i|
        dcr.add_lot(lot, op: @@mpd_qa, product: "SILPROD#{i}.01")
        if lot_parameters
          lotvalue = @@wafer_values2.values[i]
          lotvalue += 100 if i.even?
          dcr.add_parameters(@@parameters, {lot=>lotvalue}, reading_type: 'Lot')
        else
          if i.even?
            wafer_values = Hash[*lot_wafer_values[i].collect {|w,v| [w,v+100]}.flatten]
          else
            wafer_values = lot_wafer_values[i]
          end
          dcr.add_parameters(@@parameters, wafer_values)
        end
      end
      res = $client.send_dcr(dcr, export: "log/#{caller}_m.xml")
      if lot_parameters
        nsamples = lots.count*@@parameters.count
      else
        nsamples = lots.count*@@parameters.count*@@wafer_values2.size
      end
      assert $spctest.verify_dcr_accepted(res, nsamples, nooc: nsamples), 'DCR not submitted successfully...'
    else
      lots.each_with_index do |lot,i|
        wafer_values = Hash[*lot_wafer_values[i].collect {|w,v| [w,v+100]}.flatten]
        $log.info "  wafer_values: #{wafer_values}"
        dcr = Space::DCR.new(lot: lot, eqp: @@mtool, op: @@mpd_qa, product: "SILPROD#{i}.01")
        dcr.add_parameters(@@parameters, wafer_values)
        res = $client.send_dcr(dcr, export: "log/#{caller}_#{i}.xml")
        assert $spctest.verify_dcr_accepted(res, @@parameters.count*wafer_values.size, nooc: @@parameters.count*wafer_values.size*2), 'DCR not submitted successfully'
      end
    end
    return channels
  end
end
