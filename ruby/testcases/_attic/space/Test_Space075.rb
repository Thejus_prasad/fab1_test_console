=begin 
(c) Copyright 2012 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.
    
Author: D.Steger, 2012-08-08
=end

require_relative 'Test_Space_Setup'


class Test_Space075 < Test_Space_Setup
  @@mpd_qa = 'QA-MB-FTDEPM.01'
  @@other_recipe = 'P-UTMR001.01'
  @@eqp_no_recipe = 'UTFSIL04'


  def setup
    super
    return unless $lds && self.class.class_variable_defined?(:@@lot) # bypass test0000_setup
    assert_equal 0, $sv.eqp_cleanup(@@eqp)
    assert_equal 0, $sv.lot_cleanup(@@lot)
    assert_equal 0, $sv.inhibit_cancel(:route, $sv.lot_info(@@lot).route)
    #
    $lds.all_spc_channels('QA_PARAM_90*').each {|ch| assert ch.delete}
    folder = $lds.folder(@@autocreated_qa)
    assert folder.delete_channels, 'error cleaning channels' if folder
  end


  def test07500_setup
    $setup_ok = false
    #
    assert @@ppd_qa = $sildb.pd_reference(mpd: @@mpd_qa).first, "no PD reference found for #{@@mpd_qa}"
    ['AutoEquipmentAndRecipeInhibit-Single', 'AutoEquipmentAndRecipeInhibit-Multi', 'AutoEquipmentAndRecipeInhibit-Wildcard',
      'AutoEquipmentAndRecipeInhibit-Precipe',
      'AutoEquipmentAndRecipeInhibit-Error', 'AutoEquipmentAndRecipeInhibit-ErrorMulti', 'AutoEquipmentAndRecipeInhibit-ErrorWild',
      'AutoChamberAndRecipeInhibit-Multi',
      'AutoEquipmentAndRecipeInhibit-Eqp', 'AutoEquipmentAndRecipeInhibit-WildcardE', 'AutoEquipmentAndRecipeInhibit-ErrorWildE',
      'ReleaseEquipmentAndRecipeInhibit', 'ReleaseEquipmentAndRecipeInhibit-noDeflt', 'CACollection-ReleaseAllRecipeInhibits'
    ].each {|action|
      assert $spc.corrective_action(action), "CA #{action.inspect} not defined"
    }
    #
    $setup_ok = true
  end


  def test07551_tool_recipe_inhibit_single
    _scenario_equipment_recipes('AutoEquipmentAndRecipeInhibit-Single', ['P-UTC001_0.UTMR000.01'])
  end

  def test07552_tool_recipe_inhibit_multi
    _scenario_equipment_recipes('AutoEquipmentAndRecipeInhibit-Multi', ['P-UTC001_0.UTMR000.01', 'P-UTC001_1.UTMR000.01'])
  end

  def test07553_tool_recipe_inhibit_wildcard
    _scenario_equipment_recipes('AutoEquipmentAndRecipeInhibit-Wildcard', ['P-UTC001_0.UTMR000.01',
        'P-UTC001_1.UTMR000.01', 'P-UTC001_2.UTMR000.01', 'P-UTC001_3.UTMR000.01', 'P-UTC001_4.UTMR000.01'])
  end

  def test07554_tool_recipe_inhibit_precipe
    _scenario_equipment_recipes('AutoEquipmentAndRecipeInhibit-Precipe', [])
  end
  
  # check only precipe is inhibited, but no other recipe matching the pattern
  def test07555_tool_recipe_inhibit_wildcard_eqp
    eqp = @@eqp #'UTC001'
    assert recipes = $sv.eqp_related_recipes(eqp), "no recipe found for #{eqp}"
    $log.info "Recipes for eqp #{eqp}: #{recipes}"
    assert !recipes.include?(@@other_recipe), "recipe #{@@other_recipe} should not be assigned to #{eqp}"
    _scenario_equipment_recipes("AutoEquipmentAndRecipeInhibit-WildcardE", [], [@@other_recipe])
  end
  
  # check not only precipe is inhibited
  def test07556_tool_recipe_inhibit_eqp
    eqp = @@eqp #'UTC001'
    assert recipes = $sv.eqp_related_recipes(eqp), "no recipe found for #{eqp}"
    $log.info "Recipes for eqp #{eqp}: #{recipes}"
    assert recipes.count > 0, "at least 2 recipes expected"
    _scenario_equipment_recipes("AutoEquipmentAndRecipeInhibit-Eqp", recipes)
  end

  def test07561_tool_recipe_inhibit_recipe_error_single
    _scenario_equipment_recipes_not_found('AutoEquipmentAndRecipeInhibit-Error', 'Execution of [AutoEquipmentAndRecipeInhibit-Error] failed', [])
  end

  def test07562_tool_recipe_inhibit_recipe_error_multi
    _scenario_equipment_recipes_not_found('AutoEquipmentAndRecipeInhibit-ErrorMulti', 'Execution of [AutoEquipmentAndRecipeInhibit-ErrorMulti] failed', ['P-UTC001_0.UTMR000.01'])
  end

  def test07563_tool_recipe_inhibit_recipe_error_wildcard
    _scenario_equipment_recipes_not_found('AutoEquipmentAndRecipeInhibit-ErrorWild', 'Execution of [AutoEquipmentAndRecipeInhibitAction] error', [])
  end
  
  # no assigned recipe matching the pattern found
  def test07564_tool_recipe_inhibit_recipe_pattern_eqp
    _scenario_equipment_recipes_not_found("AutoEquipmentAndRecipeInhibit-ErrorWildE", "Execution of [AutoEquipmentAndRecipeInhibitAction] error", [], @@eqp_no_recipe)
  end
  
  # setup error: no recipe assigned to tool
  def test07565_tool_recipe_inhibit_recipe_eqp
    assert_equal [], $sv.eqp_related_recipes(@@eqp_no_recipe), "no recipes should be assigned to #{@@eqp_no_recipe}"
    _scenario_equipment_recipes_not_found("AutoEquipmentAndRecipeInhibit-Eqp", 
      "Execution of [AutoEquipmentAndRecipeInhibitAction] error", [], @@eqp_no_recipe)
  end

  def test07571_recipe_inhibit_all
    chambers = {'PM2'=>5, 'PM3'=>6}
    mrecipes = ['P-UTC001_0.UTMR000.01', 'P-UTC001_1.UTMR000.01']
    precipe = 'P.UTMR000.01'
    (mrecipes + [precipe]).each {|r| assert_equal 0, $sv.inhibit_cancel(:recipe, r)}
    channels = create_clean_channels(chambers: chambers)
    assert $spctest.assign_verify_cas(channels,
      ['AutoMachineRecipeInhibit', 'AutoEquipmentAndRecipeInhibit-Multi', 'AutoChamberAndRecipeInhibit-Multi', 'CACollection-ReleaseAllRecipeInhibits'])
    #
    # send process DCR
    dcrp = Space::DCR.new(eqp: @@eqp, eqptype: 'PROCESS-CHAMBER', chambers: chambers, lot: @@lot, op: @@ppd_qa, machine_recipe: precipe)
    dcrp.add_parameters(@@parameters, @@wafer_values2, nocharting: true)
    assert $spctest.send_dcr_verify(dcrp)
    #
    # send DCR with OOC values
    dcr = Space::DCR.new(lot: @@lot, eqp: @@mtool, op: @@mpd_qa)
    dcr.add_parameters(@@parameters, @@wafer_values2ooc)
    res = $client.send_dcr(dcr, export: :caller)
    assert $spctest.verify_dcr_accepted(res, @@parameters.size*@@wafer_values2ooc.size, nooc: 'all*2'), "DCR not submitted successfully"
    #
    # verify comment and inhibit is set
    chambers.keys.each do |cha|
      mrecipes.each do |mrecipe|
        assert $spctest.verify_ecomment(channels, 'TOOL+RECIPE_HELD: reason={X-S')
        assert $spctest.verify_ecomment(channels, "#{@@eqp}/")
        assert $spctest.verify_ecomment(channels, mrecipe) 
        assert $spctest.verify_inhibit([@@eqp, mrecipe], chambers.count*@@parameters.count, class: :eqp_chamber_recipe, details: true, dcr: dcrp, lot: @@lot, spc_run: res, cj: dcr.controljob), 'equipment+recipe must have an inhibit'
      end
    end
    mrecipes.each do |mrecipe|
      assert $spctest.verify_inhibit([@@eqp, mrecipe], @@parameters.count, class: :eqp_recipe, details: true, dcr: dcrp, lot: @@lot, spc_run: res, cj: dcr.controljob), 'equipment+recipe must have an inhibit'
      assert $spctest.verify_ecomment(channels, 'TOOL+RECIPE_HELD: reason={X-S')
      assert $spctest.verify_ecomment(channels, "#{@@eqp}/")
      assert $spctest.verify_ecomment(channels, mrecipe) 
    end
    assert $spctest.verify_inhibit(precipe, @@parameters.count, class: :recipe), 'equipment+precipe must not have an inhibit'
    $log.info 'Equipment and Recipe Inhibit automatically set'
    #sleep 10
    channels.each {|ch|
      # release all inhibits
      s = ch.samples(ckc: true)[-1]
      assert s.assign_ca($spc.corrective_action('CACollection-ReleaseAllRecipeInhibits')), 'error assigning corective action'
      #sleep 10
    }
    mrecipes.each {|mrecipe|
      assert $spctest.verify_inhibit([@@eqp, mrecipe], 0, class: :eqp_recipe), 'equipment+recipe must not have an inhibit'
      assert $spctest.verify_inhibit([@@eqp, mrecipe], 0, class: :eqp_chamber_recipe), 'chamber+recipe must not have an inhibit'
    }
    assert $spctest.verify_inhibit(precipe, 0, class: :recipe), 'equipment+precipe must not have an inhibit'
    $log.info 'Equipment and Recipe Inhibit released'
  end
    

  def _scenario_equipment_recipes(ca, mrecipes, not_inhibited_recipes=[])
    channels = create_clean_channels
    assert $spctest.assign_verify_cas(channels,
      [ca, 'ReleaseEquipmentAndRecipeInhibit', 'ReleaseEquipmentAndRecipeInhibit-noDeflt'])
    precipe = 'P.UTMR000.01'
    no_precipe_inhibit = true
    if mrecipes == []
      mrecipes = [precipe]
      no_precipe_inhibit = false
    elsif mrecipes.include?(precipe)
      no_precipe_inhibit = false
    end
    (mrecipes + [precipe] + not_inhibited_recipes).each {|r| assert_equal 0, $sv.inhibit_cancel(:recipe, r)}
    #
    assert_equal 0, $sv.lot_cleanup(@@lot)
    #
    # send process DCR
    dcrp = Space::DCR.new(eqp: @@eqp, eqptype: 'PROCESS-CHAMBER', lot: @@lot, op: @@ppd_qa, machine_recipe: precipe)
    dcrp.add_parameters(@@parameters, @@wafer_values2, nocharting: true)
    assert $spctest.send_dcr_verify(dcrp, exporttag: "#{caller_locations(1,1)[0].label}_p")
    #
    assert_equal 0, $sv.lot_cleanup(@@lot)
    #
    # send DCR with OOC values
    dcr = Space::DCR.new(lot: @@lot, eqp: @@mtool, op: @@mpd_qa)
    dcr.add_parameters(@@parameters, @@wafer_values2ooc)
    nsamples = @@parameters.size * @@wafer_values2ooc.size
    assert $spctest.send_dcr_verify(dcr, nsamples: nsamples, nooc: 'all*2', exporttag: caller_locations(1,1)[0].label), 'error sending DCR'
    #
    # verify comment and inhibit is set
    mrecipes.each {|mrecipe|
      assert $spctest.verify_inhibit([@@eqp, mrecipe], @@parameters.count, class: :eqp_recipe), 'equipment+recipe must have an inhibit'
      assert $spctest.verify_ecomment(channels, 'TOOL+RECIPE_HELD: reason={X-S')
      assert $spctest.verify_ecomment(channels, "#{@@eqp}/")
      assert $spctest.verify_ecomment(channels, mrecipe)
    }
    $log.info 'Equipment and Recipe Inhibit automatically set'
    if no_precipe_inhibit
      assert $spctest.verify_inhibit([@@eqp, precipe], 0, class: :eqp_recipe), 'equipment+precipe must not have an inhibit'
    end
    not_inhibited_recipes.each { |mrecipe|
      assert $spctest.verify_inhibit([@@eqp, mrecipe], 0, iclass: :eqp_recipe), "#{mrecipe} should not be inhibited"
    }
    assert $spctest.verify_dfs_message(channels, { channels=>mrecipes.map { |r| "TOOL+RECIPE_HELD: #{@@eqp}/#{r}" } }, nooc: 2*nsamples), "failed to verify DFS message"
    channels.each {|ch|
      # try to release inhibit noDefault
      s = ch.samples(ckc: true)[-1]
      assert !s.assign_ca($spc.corrective_action('ReleaseEquipmentAndRecipeInhibit-noDeflt')), 'wrongly assigned corective action'
    }
    mrecipes.each {|mrecipe|
      assert $spctest.verify_inhibit([@@eqp, mrecipe], channels.size, class: :eqp_recipe), 'equipment+recipe must still have an inhibit'
    }
    #
    # release inhibit with noDefault attribute - with comment
    channels.each {|ch|
      s = ch.samples(ckc: true)[-1]
      #works - however - i think the comment doesnt matter when using ReleaseEquipmentAndRecipeInhibit
      #assert s.assign_ca($spc.corrective_action('ReleaseEquipmentAndRecipeInhibit'), comment: "RELEASE=[#{@@eqp}/(#{mrecipes[0]})]"), 'error assigning corrective action'
      #works - however - i think the comment doesnt matter when using ReleaseEquipmentAndRecipeInhibit
      #assert s.assign_ca($spc.corrective_action('ReleaseEquipmentAndRecipeInhibit'), comment: 'RELEASE=[ALL]'), 'error assigning corrective action'
      #dont work
      #assert s.assign_ca($spc.corrective_action('ReleaseEquipmentAndRecipeInhibit-noDeflt'), comment: 'RELEASE=[ALL]'), 'error assigning corrective action'
      #works
      #assert s.assign_ca($spc.corrective_action('ReleaseEquipmentAndRecipeInhibit-noDeflt'), comment: '[Release=All]'), 'error assigning corrective action'
      
      # because of the order of the recipes is somehow random - need to read the ecomment to identify the inhibit
      ecomment = s.ecomment.split('toolRecipeList:').last.gsub('\n', '')
      cacomment = "#{@@eqp}/(#{mrecipes.join(',')})"
      if ecomment != cacomment
        $log.warn "The ecomment at the sample: #{ecomment} is not exactly the same as expected: #{cacomment}. Please check manually!"
      end
      assert s.assign_ca($spc.corrective_action('ReleaseEquipmentAndRecipeInhibit-noDeflt'), comment: "[Release=#{ecomment}]"), 'error assigning corrective action'
    }
    mrecipes.each {|mrecipe|
      assert $spctest.verify_inhibit([@@eqp, mrecipe], 0, class: :eqp_recipe), 'equipment+recipe must not have an inhibit'
    }
    #
    # send process and measurement DCRs again
    assert_equal 0, $sv.lot_cleanup(@@lot)
    #
    assert $spctest.send_dcr_verify(dcrp, exporttag: "#{caller_locations(1,1)[0].label}_p")
    assert_equal 0, $sv.lot_cleanup(@@lot)
    #
    assert $spctest.send_dcr_verify(dcr, nsamples: nsamples, nooc: 'all*2', exporttag: caller_locations(1,1)[0].label), 'error sending DCR'
    #
    # release inhibit
    channels.each {|ch|
      s = ch.samples(ckc: true)[-1]
      assert s.assign_ca($spc.corrective_action('ReleaseEquipmentAndRecipeInhibit')), 'error assigning corective action'
    }
    mrecipes.each {|mrecipe|
      assert $spctest.verify_inhibit([@@eqp, mrecipe], 0, class: :eqp_recipe), 'equipment+recipe must not have an inhibit'
    }
    $log.info 'Equipment and Recipe Inhibit released'
  end


  def _scenario_equipment_recipes_not_found(ca, excomment, mrecipes, eqp=@@eqp)
    channels = create_clean_channels
    assert $spctest.assign_verify_cas(channels, [ca, 'ReleaseEquipmentAndRecipeInhibit'])
    precipe = 'P.UTMR000.01'
    (mrecipes + [precipe]).each {|r| assert_equal 0, $sv.inhibit_cancel(:recipe, r)}
    #
    assert_equal 0, $sv.lot_cleanup(@@lot)
    #
    # send process DCR
    dcrp = Space::DCR.new(eqp: eqp, eqptype: 'PROCESS-CHAMBER', lot: @@lot, op: @@ppd_qa, machine_recipe: precipe)
    dcrp.add_parameters(@@parameters, @@wafer_values2, nocharting: true)
    assert $spctest.send_dcr_verify(dcrp, exporttag: "#{caller_locations(1,1)[0].label}_p")
    #
    assert_equal 0, $sv.lot_cleanup(@@lot)
    #
    # send DCR with OOC values
    dcr = Space::DCR.new(lot: @@lot, eqp: @@mtool, op: @@mpd_qa)
    dcr.add_parameters(@@parameters, @@wafer_values2ooc)
    nsamples = @@parameters.size * @@wafer_values2ooc.size
    assert $spctest.send_dcr_verify(dcr, nsamples: nsamples, nooc: 'all*2', exporttag: caller_locations(1,1)[0].label), 'error sending DCR'
    #
    # verify comment and inhibit is set
    assert $spctest.verify_ecomment(channels, excomment), 'no comment indicating the error'
    mrecipes.each {|mrecipe|
      assert $spctest.verify_inhibit([eqp, mrecipe], @@parameters.count, class: :eqp_recipe), 'equipment+recipe must have an inhibit'
      assert $spctest.verify_ecomment(channels, 'TOOL+RECIPE_HELD: reason={X-S')
      assert $spctest.verify_ecomment(channels, "#{eqp}/")
      assert $spctest.verify_ecomment(channels, mrecipe)
    }
    $log.info 'Equipment and Recipe Inhibit automatically set'
    assert $spctest.verify_inhibit([eqp, precipe], 0, class: :eqp_recipe), 'equipment+precipe must not have an inhibit'
    assert $spctest.verify_futurehold_cancel(@@lot, '[(Raw above specification)'), 'lot must have no future hold'

    unless mrecipes.empty?
      # release inhibit (from automatic action)
      channels.each {|ch|
        s = ch.samples(ckc: true)[-1]
        assert s.assign_ca($spc.corrective_action('ReleaseEquipmentAndRecipeInhibit')), 'error assigning CA'
      }
    end
    mrecipes.each {|mrecipe|
      assert $spctest.verify_inhibit([eqp, mrecipe], 0, class: :eqp_recipe), 'equipment+recipe must not have an inhibit'
    }
    $log.info 'Equipment and Recipe Inhibit released'
    assert $spctest.verify_notification("Execution of [#{ca}] failed", 'NOT-FOUND', msgcount: channels.count), 'wrong notification see MSR549577, please check SIL log for DERDACK message too'
  end

end
