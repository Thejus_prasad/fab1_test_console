=begin 
Test Space/SIL.

(c) Copyright 2015 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.
    
Author: C.Stenzel, 2015-12-02
=end

require_relative 'Test_Space_Setup'

# Test WebMRB interface of SIL
class Test_Space0710 < Test_Space_Setup
  @@autocreated_mrb = 'AutoCreated_QAMRB'
  @@mpd_qa = 'QA-WEBMRB.01'

  
  def test07100_inline
    assert_equal 0, $sv.eqp_cleanup(@@eqp)
    assert_equal 0, $sv.lot_cleanup(@@lot)
    _real_wafer_values
    
    $lds.all_spc_channels('QA_PARAM_90*').each {|ch| assert ch.delete}
    folder = $lds.folder(@@autocreated_mrb)
    assert folder.delete_channels, 'error cleaning channels' if folder
    #
    parameters = ['QA_PARAM_900_MRB', 'QA_PARAM_901_MRB', 'QA_PARAM_902_MRB', 'QA_PARAM_903_MRB', 'QA_PARAM_904_MRB']
    department = 'QAMRB'
    assert @@ppd_qa = $sildb.pd_reference(:mpd=>@@mpd_qa).first, "no PD reference found for #{@@mpd_qa}"
    
    channels = create_clean_channels(department: department, mpd: @@mpd_qa, ppd: @@ppd_qa, 
      wafer_values: @@wafer_values, parameters: parameters,
      actions: ['AutoRaiseMRB', 'RaiseMRB'], delete: false)
      
    submit_process_dcr(@@ppd_qa, wafer_values: @@real_wafer_values)
    # send DCR with OOC values
    dcr = Space::DCR.new(lot: @@lot, eqp: @@mtool, op: @@mpd_qa)
    dcr.add_parameters(parameters, @@real_wafer_values_ooc)
    res = $client.send_dcr(dcr, export: :caller)
    assert $spctest.verify_dcr_accepted(res, parameters.count*@@real_wafer_values_ooc.size, nooc: 'all*2'), 'DCR not submitted successfully'
  end
  
  def test07101_test
    assert_equal 0, $sv.eqp_cleanup(@@eqp)
    assert_equal 0, $sv.lot_cleanup(@@lot)
    _real_wafer_values
    
    $lds_setup.all_spc_channels('QA_PARAM_90*').each {|ch| assert ch.delete}
    folder = $lds_setup.folder(@@autocreated_mrb)
    assert folder.delete_channels, 'error cleaning channels' if folder
    #
    parameters = ['QA_PARAM_900_MRB', 'QA_PARAM_901_MRB', 'QA_PARAM_902_MRB', 'QA_PARAM_903_MRB', 'QA_PARAM_904_MRB']
    department = 'QAMRB'
    technology = 'TEST'
    assert @@ppd_qa = $sildb.pd_reference(:mpd=>@@mpd_qa).first, "no PD reference found for #{@@mpd_qa}"
    
    channels = create_clean_channels(department: department, mpd: @@mpd_qa, ppd: @@ppd_qa, technology: technology,
      wafer_values: @@wafer_values, parameters: parameters,
      actions: ['AutoRaiseMRB', 'RaiseMRB'], delete: false)
      
    submit_process_dcr(@@ppd_qa, wafer_values: @@real_wafer_values, technology: technology)
    # send DCR with OOC values
    dcr = Space::DCR.new(lot: @@lot, eqp: @@mtool, op: @@mpd_qa, technology: technology)
    dcr.add_parameters(parameters, @@real_wafer_values_ooc)
    res = $client.send_dcr(dcr, export: :caller)
    assert $spctest.verify_dcr_accepted(res, parameters.count*@@real_wafer_values_ooc.size, nooc: 'all*2'), 'DCR not submitted successfully'
  end
  
  def _real_wafer_values
    @@real_wafer_values = Hash.new
    @@real_wafer_values_ooc = Hash.new
    $sv.lot_info(@@lot, wafers: true).wafers[0..5].each_with_index {|w, i| 
      @@real_wafer_values[w.wafer] = i + 40
      @@real_wafer_values_ooc[w.wafer] = i + 140
    }
    $log.info "real_wafer_values: #{@@real_wafer_values}"
    $log.info "real_wafer_values: #{@@real_wafer_values_ooc}"
  end
end
