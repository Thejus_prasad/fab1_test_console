=begin 
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.
    
Author: Steffen Steidten, 2011-11-25
=end

require_relative 'Test_Space_Setup'


# Test Space/SIL Monitoring Speclimits (ECP)
class Test_Space25 < Test_Space_Setup
  #Check if upload for twosigma charts is enabled or not; configuration is only for setup;Two Sigma charts are not supported for inline LDS
  @@spaceupload_twosigmachart_setup_enabled=false
  #Ignore base channels with listed states for 2S charting 
  @@spaceupload_twosigmachart_channel_state_ignored = ['Study', 'Frozen']
  @@twosigma_prefix = '2S@'
  @@messages = ['[(Mean above control limit)', '[(Mean below control limit)', '[(Raw above specification)', '[(Raw below specification)']
  @@parameters = @@parameters.collect {|p| p + '_ECP'}
  @@mpd_qa = 'QA-MGT-NMETDEPDR.QA'
  @@mpd_gzv = 'QA-GZV-FTDEPM.01'
  @@ppd = nil

  def setup
    super
    return unless $lds && self.class.class_variable_defined?(:@@lot) # bypass test0000_setup
    assert_equal 0, $sv.eqp_cleanup(@@eqp)
    assert_equal 0, $sv.lot_cleanup(@@lot)
    #
    $lds.all_spc_channels('QA_PARAM_90*').each {|ch| assert ch.delete}
    folder = $lds.folder(@@autocreated_qa)
    assert folder.delete_channels, 'error cleaning channels' if folder
  end

  def self.shutdown
    super
    $sv.eqp_cleanup(@@eqp)
  end

  def test2500_0_setup
    $setup_ok = false
    #
    $spctest.sil_upload_verify_autocharting 'etc/testcasedata/space/Auto_Charting_STAG.SpecID.xlsx' if $env == 'f8stag'  
    assert @@ppd = $sildb.pd_reference(mpd: @@mpd_gzv).first, "no PD reference found for #{@@mpd_gzv}"
    $log.info "using process PD #{@@ppd}"
    #
    $setup_ok = true
  end  
  
  def test2500_speclimits_1
    # build and submit DCR with one parameter only to identify the parameter in the hold claim memo
    dcr = Space::DCR.new(eqp: @@eqp, lot: @@lot, op: @@mpd_qa)
    dcr.add_parameter(@@parameters[0], @@wafer_valuesooc)
    res = $client.send_dcr(dcr, export: :caller)
    assert $spctest.verify_dcr_accepted(res, @@wafer_valuesooc.size, nooc: 'all*2'), 'DCR not submitted successfully'
    assert $spctest.verify_futurehold(@@lot, nil, @@parameters[0], messages: @@messages), 'error setting future hold'
  end

  def test2501_speclimits_all
    # build and submit DCR with all parameters
    dcr = Space::DCR.new(eqp: @@eqp, lot: @@lot, op: @@mpd_qa)
    dcr.add_parameters(@@parameters, @@wafer_valuesooc)
    res = $client.send_dcr(dcr, export: :caller)
    nsamples = @@parameters.size * @@wafer_valuesooc.size
    assert $spctest.verify_dcr_accepted(res, nsamples, nooc: 'all*2'), 'DCR not submitted successfully'
    assert $spctest.verify_futurehold(@@lot, nil, nil, count: @@parameters.size, messages: @@messages), 'error setting future hold'
  end

  def test2502_speclimits_all_4times_USL
    # build and submit DCR with all parameters
    dcr = Space::DCR.new(eqp: @@eqp, lot: @@lot, op: @@mpd_qa)
    wafer_values = Hash[@@wafer_values.collect {|w, v| [w, (v+100)*4]}]
    dcr.add_parameters(@@parameters, wafer_values)
    res = $client.send_dcr(dcr, export: :caller)
    nsamples = @@parameters.size * wafer_values.size
    assert $spctest.verify_dcr_accepted(res, nsamples, nooc: 'all*2'), 'DCR not submitted successfully'
    assert $spctest.verify_futurehold(@@lot, nil, nil, count: @@parameters.size, messages: @@messages), 'error setting future hold'
  end

  def test2503_speclimits_all_1000times_USL
    # build and submit DCR with all parameters
    dcr = Space::DCR.new(eqp: @@eqp, lot: @@lot, op: @@mpd_qa)
    wafer_values = Hash[@@wafer_values.collect {|w, v| [w, (v+100)*1000]}]
    dcr.add_parameters(@@parameters, wafer_values)
    res = $client.send_dcr(dcr, export: :caller)
    nsamples = @@parameters.size * wafer_values.size
    assert $spctest.verify_dcr_accepted(res, nsamples, nooc: 'all*2'), 'DCR not submitted successfully'
    assert $spctest.verify_futurehold(@@lot, nil, nil, count: @@parameters.size, messages: @@messages), 'error setting future hold'
  end
  
  def test2504_speclimits_all_4times_USL_sites
    # build and submit DCR
    wafer_values = {}
    wafer_values = Hash[@@wafer_values.keys.collect {|w| [w, [410,420,430,440,450,460]]}]
    dcr = Space::DCR.new(eqp: @@eqp, lot: @@lot, op: @@mpd_qa)
    dcr.add_parameters(@@parameters, wafer_values, sites: true)
    res = $client.send_dcr(dcr, export: :caller)
    nsamples = @@parameters.size * wafer_values.size
    assert $spctest.verify_dcr_accepted(res, nsamples, nooc: nsamples * 3), 'DCR not submitted successfully'
    assert $spctest.verify_futurehold(@@lot, nil, nil, count: @@parameters.size, messages: @@messages), 'error setting future hold'
  end
  
  def test2505_speclimits_1_4times_USL_sites
    # build and submit DCR
    wafer_values = Hash[@@wafer_values.keys.collect {|w| [w, [41,42,43,44,45,460]]}]
    dcr = Space::DCR.new(eqp: @@eqp, lot: @@lot, op: @@mpd_qa)
    dcr.add_parameters(@@parameters, wafer_values, sites: true)
    res = $client.send_dcr(dcr, export: :caller)
    nsamples = @@parameters.size * wafer_values.size
    assert $spctest.verify_dcr_accepted(res, nsamples, nooc: nsamples * 3), 'DCR not submitted successfully'
    assert $spctest.verify_futurehold(@@lot, nil, nil, count: @@parameters.size, messages: @@messages), 'error setting future hold'
  end
  
  def test2506_parameter_extension_minus
    # build and submit DCR
    parameters = ['QA_PARAM_900_ECP', 'QA_PARAM_901_ECP']
    ctrl = {'MEAN'=>410, 'STDDEV'=>1000, 'MIN'=>-550, 'MAX'=>550, 'COUNT'=>5000}
    #
    dcr = Space::DCR.new(eqp: @@eqp, lot: @@lot, op: @@mpd_qa)
    parameters.each {|p|
      ctrl.each {|k, v| dcr.add_parameter("#{p}-#{k}", {'2502J493KO'=>v}, space_listening: true)}
    }
    res = $client.send_dcr(dcr, export: :caller)
    assert $spctest.verify_dcr_accepted(res, 2), 'DCR not submitted successfully'
    #
    folder = $lds.folder(@@autocreated_qa)
    parameters.each {|p|
      ch = folder.spc_channel(parameter: p)
      s = ch.samples.last
      assert $spctest.verify_sample_statistics(s, ctrl)
      assert $spctest.verify_sample_speclimits(s, 25, 45, 75)
    }
  end
    
  def test2510_speclimits_TEST
    # build and submit DCR with all parameters, technology TEST
    dcr = Space::DCR.new(eqp: @@eqp, lot: @@lot, op: @@mpd_qa, technology: 'TEST')
    dcr.add_parameters(@@parameters, @@wafer_valuesooc)
    res = $client.send_dcr(dcr, export: :caller)
    nsamples = @@parameters.size * @@wafer_valuesooc.size
    assert $spctest.verify_dcr_accepted(res, nsamples, nooc: 'all*2'), 'DCR not submitted successfully'
    #
    # verify spc channels are created in AutoCreated_QA
    assert $spctest.verify_parameters(@@parameters, $lds_setup.folder(@@autocreated_qa), @@wafer_valuesooc.size), 'error creating channels'
    assert $spctest.verify_futurehold(@@lot, nil, nil, count: @@parameters.size, messages: @@messages), 'error setting future hold'
  end

  def test2511_speclimits_listening_wafer_based
    # build and submit DCR with all parameters
    dcr = Space::DCR.new(eqp: @@eqp, lot: @@lot, op: @@mpd_qa)
    dcr.add_parameters(@@parameters, @@wafer_valuesooc, space_listening: true)
    res = $client.send_dcr(dcr, export: :caller)
    assert $spctest.verify_dcr_accepted(res, @@parameters.size*@@wafer_valuesooc.size, nooc: 0), 'DCR not submitted successfully'
    # since SIL 4.12, we separated the reason code for missing param and a violation, therefore, this is necessary now:
    $sv.lot_futurehold_cancel(@@lot, 'X-MP')
    assert_empty $sv.lot_futurehold_list(@@lot), 'lot must have no future hold'
    assert_empty $sv.inhibit_list(:eqp, @@eqp), 'eqp must not be inhibited'
  end
  
  def test2512_speclimits_active_wafer_and_lot_based
    mtool = 'QAMeas'
    mpd = 'QA-MB-FTDEPM-LIS-CAEX.01'
    assert ppd = $sildb.pd_reference(mpd: mpd).first, "no PD reference found for #{mpd}"
    $log.info "using process PD #{ppd}"
    # 
    # build and submit DCR for related MPD
    dcr = Space::DCR.new(eqp: mtool, lot: @@lot, op: mpd)
    dcr.add_parameters(@@parameters[0..-2], @@wafer_valuesooc, processing_areas: ['CHC'])
    dcr.add_parameter(@@parameters[-1], {@@lot=>7.0}, reading_type: 'Lot')
    res = $client.send_dcr(dcr, export: :caller)
    assert $spctest.verify_dcr_accepted(res, (@@parameters.size - 1) * @@wafer_valuesooc.size + 1), 'DCR not submitted successfully'
    assert $spctest.verify_futurehold(@@lot, nil, nil, count: @@parameters.size, messages: @@messages), 'error setting future hold for OOC'
    #assert !$spctest.verify_futurehold(@@lot, 'Missing parameter in DCR:'), 'wrong future hold because of missing parameter'
  end
  
  def test2513_speclimits_listening_wafer_and_lot_based
    mtool = 'QAMeas'
    mpd = 'QA-MB-FTDEPM-LIS-CAEX.01'
    assert ppd = $sildb.pd_reference(mpd: mpd).first, "no PD reference found for #{mpd}"
    $log.info "using process PD #{ppd}"
    # build and submit DCR for related MPD
    dcr = Space::DCR.new(eqp: mtool, lot: @@lot, op: mpd)
    dcr.add_parameters(@@parameters[0..-2], @@wafer_valuesooc, processing_areas: ['CHC'], space_listening: true)
    dcr.add_parameter(@@parameters[-1], {@@lot=>7.0}, reading_type: 'Lot', space_listening: true)
    res = $client.send_dcr(dcr, export: :caller)
    assert $spctest.verify_dcr_accepted(res, (@@parameters.size - 1) * @@wafer_valuesooc.size + 1), 'DCR not submitted successfully'
    # since SIL 4.12, we separated the reason code for missing param and a violation, therefore, this is necessary now:
    $sv.lot_futurehold_cancel(@@lot, 'X-MP')
    assert_empty $sv.lot_futurehold_list(@@lot), 'lot must not be onhold'
    assert_empty $sv.inhibit_list(:eqp, @@eqp), 'eqp must not be inhibited'
  end
  
  def test2514_speclimits_listening_lot_based
    mtool = 'QAMeas'
    mpd = 'QA-MB-FTDEPM-LIS-CAEX.01'
    assert ppd = $sildb.pd_reference(mpd: mpd).first, "no PD reference found for #{mpd}"
    $log.info "using process PD #{ppd}"
    # 
    # build and submit DCR for related MPD
    dcr = Space::DCR.new(eqp: mtool, lot: @@lot, op: mpd)
    dcr.add_parameters(@@parameters, {@@lot=>7.0}, reading_type: 'Lot', space_listening: true)
    res = $client.send_dcr(dcr, export: :caller)
    assert $spctest.verify_dcr_accepted(res, 5), 'DCR not submitted successfully'
    # since SIL 4.12, we separated the reason code for missing param and a violation, therefore, this is necessary now:
    $sv.lot_futurehold_cancel(@@lot, 'X-MP')
    assert_empty $sv.lot_futurehold_list(@@lot), 'lot must not be onhold'
    assert_empty $sv.inhibit_list(:eqp, @@eqp), 'eqp must not be inhibited'
  end

  # MSR548412 <<< CANCELED --- to be reworked with MSR831687 @ SIL4.4 Release
  def XXtest2520_speclimits_2sigma_listening
    $log.info 'MSR548412 <<< CANCELED --- to be reworked with MSR831687 @ SIL4.4 Release'
    assert false, 'NOT TESTABLE until MSR831687 is released' # ???????
    #####
    eqp = 'UTF001'
    mpd = 'QA-2SIG-DEPDR.01'
    assert ppd = $sildb.pd_reference(mpd: mpd).first, "no PD reference found for #{mpd}"
    #E10 state
    $sv.eqp_status_change_req(eqp, '2NDP')
    $sv.eqp_status_change_req(eqp, '4PMQ')
    submit_process_dcr(ppd, eqp: eqp)
    #
    # build and submit DCR with all parameters
    dcr = Space::DCR.new(eqp: @@eqp, lot: @@lot, op: mpd, technology: 'TEST')
    dcr.add_parameters(@@parameters, @@wafer_valuesooc, space_listening: true)
    res = $client.send_dcr(dcr, export: :caller)
    assert $spctest.verify_dcr_accepted(res, @@parameters.size*@@wafer_valuesooc.size, nooc: 0), 'DCR not submitted successfully'
    assert $spctest.verify_parameters(@@parameters, $lds_setup.folder(@@autocreated_qa), @@wafer_valuesooc.size), 'error creating channels'
    assert_empty $sv.lot_futurehold_list(@@lot), 'lot must not be onhold'
    assert_empty $sv.inhibit_list(:eqp, @@eqp), 'eqp must not be inhibited'
    if @@spaceupload_twosigmachart_setup_enabled
      $log.error 'Check for 2-sigma charts is not implemented yet/not working'
    end
  end

  # MSR548412 <<< CANCELED --- to be reworked with MSR831687 @ SIL4.4 Release
  def XXtest2521_speclimits_2sigma_active
    $log.info 'MSR548412 <<< CANCELED --- to be reworked with MSR831687 @ SIL4.4 Release'
    assert false, 'NOT TESTABLE until MSR831687 is released'
    #####
    return if $env == 'let'
    skip "not applicable in #{$env}" unless ['itdc', 'let'].member?($env)
    ## return unless check_env_f1
    eqp = 'UTF001'
    mpd = 'QA-2SIG-DEPDR.01'
    assert ppd = $sildb.pd_reference(mpd: mpd).first, "no PD reference found for #{mpd}"
    parameters = @@parameters + ['QA_PARAM_1_ECP']

    folder = $lds_setup.folder('AutoCreated_nonwip')
    folder.delete_channels if folder

    #E10 state
    $sv.eqp_status_change_req eqp, '2NDP'
    # eqp -> status='SDT.4PMQ'
    $sv.eqp_status_change_req eqp, '4PMQ'

    # submit the process DCR
    submit_process_dcr(ppd, eqp: eqp)
    #
    # build and submit DCR with all parameters
    dcr = Space::DCR.new(eqp: @@eqp, lot: @@lot, op: mpd, technology: 'TEST')
    wafer_values = Hash[*@@wafer_values.collect {|w,v| [w,v+100]}.flatten]
    dcr.add_parameters(parameters, wafer_values)
    res = $client.send_dcr(dcr, export: :caller)
    assert $spctest.verify_dcr_accepted(res, parameters.size*wafer_values.size, nooc: 'all*2'), 'DCR not submitted successfully'
    #
    # verify spc channels are created in AutoCreated_QA
    assert $spctest.verify_parameters(parameters, $lds_setup.folder(@@autocreated_qa), wafer_values.size), 'error creating channels'
    #
    assert_empty $sv.lot_futurehold_list(@@lot), 'lot must not be onhold'
    assert_empty $sv.inhibit_list(:eqp, @@eqp), 'eqp must not be inhibited'

    if @@spaceupload_twosigmachart_setup_enabled
      $log.error 'Check for 2-sigma charts is not implemented yet/not working'
    end
  end
  
  #MSR513786
  def test2530_spec_limits_without_space_scenario_non_ooc_all_params
    #MSR513786.QA 513786NM  MSR-513786.01 MSR513786PG|MSR513786PROD.01  QA_PARAM_980_ECP    250 450 750 WAFER             
    #MSR513786.QA 513786NM  MSR-513786.01 MSR513786PG|MSR513786PROD.01  QA_PARAM_981_ECP    250 450 750 WAFER             
    #MSR513786.QA 513786NM  MSR-513786.01 MSR513786PG|MSR513786PROD.01  QA_PARAM_982_ECP    250 450 750 WAFER             
    #MSR513786.QA 513786NM  MSR-513786.01 MSR513786PG|MSR513786PROD.01  QA_PARAM_983_ECP    250 450 750 WAFER             
    #MSR513786.QA 513786NM  MSR-513786.01 MSR513786PG|MSR513786PROD.01  QA_PARAM_984_ECP    250 450 750 WAFER             
    #MSR513786.QA 513786NM  MSR-513786.01 MSR513786PG|MSR513786PROD.01  QA_PARAM_985_ECP    250 450 750 WAFER
    #
    # build and submit DCR with all parameters
    # special  LSL, TARGET and USL values
    parameters = ['QA_PARAM_980_ECP', 'QA_PARAM_981_ECP', 'QA_PARAM_982_ECP', 'QA_PARAM_983_ECP', 'QA_PARAM_984_ECP', 'QA_PARAM_985_ECP']
    mpd = 'MSR513786.QA'
    technology = '513786NM'
    route = 'MSR-513786.01'
    productgroup = 'MSR513786PG'
    product = 'MSR513786PROD.01'
    dcr = Space::DCR.new(eqp: @@eqp, lot: @@lot, op: mpd, productgroup: productgroup, product: product, technology: technology, route: route)
    wafer_values = Hash[@@wafer_values.collect {|w, v| [w, v+300]}]
    dcr.add_parameters(parameters, wafer_values)
    res = $client.send_dcr(dcr, export: :caller)
    assert $spctest.verify_dcr_accepted(res, parameters.size * wafer_values.size), 'DCR not submitted successfully'
    #
    # verify spc channels
    folder = $lds.folder(@@autocreated_qa)
    parameters.each {|p|
      ch = folder.spc_channel(parameter: p)
      ref_ch = Space::SpcApi::ChannelData.new(nil, nil, "Auto-created using template: #{@@params_template}", p)
      assert $spctest.verify_channel(ch, ref_ch, dcr, wafer_values, last_sample_only: true), 'wrong channel data'
      assert $spctest.verify_sample_speclimits(ch.samples, 250, 450, 750), 'wrong spec limits'
    }
  end

  #MSR513786
  def test2531_spec_limits_with_space_scenario_non_ooc_all_params
    skip "not applicable in #{$env}" unless ['itdc', 'let'].member?($env)
    #MSR513786.QA 513786NM  MSR-513786.01 MSR513786PG|MSR513786PROD.01  QA_PARAM_990_ECP    250 450 750 WAFER             
    #MSR513786.QA 513786NM  MSR-513786.01 MSR513786PG|MSR513786PROD.01  QA_PARAM_991_ECP    250 450 750 WAFER             CALCULATION
    #MSR513786.QA 513786NM  MSR-513786.01 MSR513786PG|MSR513786PROD.01  QA_PARAM_992_ECP    250 450 750 WAFER             MANUAL_SPECLIMIT_SPACE
    #MSR513786.QA 513786NM  MSR-513786.01 MSR513786PG|MSR513786PROD.01  QA_PARAM_993_ECP    250 450 750 WAFER             MANUAL_SPECLIMIT_SPACE
    #MSR513786.QA 513786NM  MSR-513786.01 MSR513786PG|MSR513786PROD.01  QA_PARAM_994_ECP    250 450 750 WAFER             IGNORE_ALL_PARAMETER_CHECK
    #MSR513786.QA 513786NM  MSR-513786.01 MSR513786PG|MSR513786PROD.01  QA_PARAM_995_ECP    250 450 750 WAFER             IGNORE_ALL_PARAMETER_CHECK
    #
    # build and submit DCR with all parameters
    parameters = ['QA_PARAM_990_ECP', 'QA_PARAM_991_ECP', 'QA_PARAM_992_ECP', 'QA_PARAM_993_ECP', 'QA_PARAM_994_ECP', 'QA_PARAM_995_ECP']
    mpd = 'MSR513786.QA'
    technology = '513786NM'
    route = 'MSR-513786.01'
    productgroup = 'MSR513786PG'
    product = 'MSR513786PROD.01'
    dcr = Space::DCR.new(eqp: @@eqp, lot: @@lot, op: mpd, productgroup: productgroup, product: product, technology: technology, route: route)
    wafer_values = Hash[@@wafer_values.collect {|w, v| [w, v+300]}]
    dcr.add_parameters(parameters, wafer_values)
    res = $client.send_dcr(dcr, export: :caller)
    assert $spctest.verify_dcr_accepted(res, parameters.size * wafer_values.size), 'DCR not submitted successfully'
    #
    folder = $lds.folder(@@autocreated_qa)
    # verify spc channels for 1st parameter
    p = parameters.first
    ch = folder.spc_channel(parameter: p)
    ref_ch = Space::SpcApi::ChannelData.new(nil, nil, "Auto-created using template: #{@@params_template}", p)
    assert $spctest.verify_channel(ch, ref_ch, dcr, wafer_values, last_sample_only: true), 'wrong channel data'
    assert $spctest.verify_sample_speclimits(ch.samples, 250, 450, 750), 'wrong spec limits'
    # verify spc channels for remaining parameters
    parameters[1..-1].each {|p|
      ch = folder.spc_channel(parameter: p)
      ref_ch = Space::SpcApi::ChannelData.new(nil, nil, "Auto-created using template: #{@@params_template}", p)
      assert $spctest.verify_channel(ch, ref_ch, dcr, wafer_values, last_sample_only: true), 'wrong channel data'
      assert $spctest.verify_sample_speclimits(ch.samples, nil, nil, nil), 'wrong spec limits'
    }
  end

  #MSR513786
  def test2532_spec_limits_with_space_scenario_ooc_all_params
    skip "not applicable in #{$env}" unless ['itdc', 'let'].member?($env)
    #MSR513786.QA 513786NM  MSR-513786.01 MSR513786PG|MSR513786PROD.01  QA_PARAM_990_ECP    250 450 750 WAFER             
    #MSR513786.QA 513786NM  MSR-513786.01 MSR513786PG|MSR513786PROD.01  QA_PARAM_991_ECP    250 450 750 WAFER             CALCULATION
    #MSR513786.QA 513786NM  MSR-513786.01 MSR513786PG|MSR513786PROD.01  QA_PARAM_992_ECP    250 450 750 WAFER             MANUAL_SPECLIMIT_SPACE
    #MSR513786.QA 513786NM  MSR-513786.01 MSR513786PG|MSR513786PROD.01  QA_PARAM_993_ECP    250 450 750 WAFER             MANUAL_SPECLIMIT_SPACE
    #MSR513786.QA 513786NM  MSR-513786.01 MSR513786PG|MSR513786PROD.01  QA_PARAM_994_ECP    250 450 750 WAFER             IGNORE_ALL_PARAMETER_CHECK
    #MSR513786.QA 513786NM  MSR-513786.01 MSR513786PG|MSR513786PROD.01  QA_PARAM_995_ECP    250 450 750 WAFER             IGNORE_ALL_PARAMETER_CHECK
    mpd = 'MSR513786.QA'
    technology = '513786NM'
    route = 'MSR-513786.01'
    productgroup = 'MSR513786PG'
    product = 'MSR513786PROD.01'
    parameters = ['QA_PARAM_990_ECP', 'QA_PARAM_991_ECP', 'QA_PARAM_992_ECP', 'QA_PARAM_993_ECP', 'QA_PARAM_994_ECP', 'QA_PARAM_995_ECP']
    #
    # build and submit DCR with all parameters
    dcr = Space::DCR.new(eqp: @@eqp, lot: @@lot, op: mpd, productgroup: productgroup, product: product, technology: technology, route: route)
    wafer_values = Hash[@@wafer_values.collect {|w, v| [w, v+800]}]
    dcr.add_parameters(parameters, wafer_values)
    res = $client.send_dcr(dcr, export: :caller)
    assert $spctest.verify_dcr_accepted(res, parameters.size * wafer_values.size), 'DCR not submitted successfully'
    #
    folder = $lds.folder(@@autocreated_qa)
    # verify spc channels for 1st parameter
    p = parameters.first
    ch = folder.spc_channel(parameter: p)
    ref_ch = Space::SpcApi::ChannelData.new(nil, nil, "Auto-created using template: #{@@params_template}", p)
    assert $spctest.verify_channel(ch, ref_ch, dcr, wafer_values, last_sample_only: true), 'wrong channel data'
    assert $spctest.verify_sample_speclimits(ch.samples, 250, 450, 750), 'wrong spec limits'
    # verify spc channels for remaining parameters
    parameters[1..-1].each {|p|
      ch = folder.spc_channel(parameter: p)
      ref_ch = Space::SpcApi::ChannelData.new(nil, nil, "Auto-created using template: #{@@params_template}", p)
      assert $spctest.verify_channel(ch, ref_ch, dcr, wafer_values, last_sample_only: true), 'wrong channel data'
      assert $spctest.verify_sample_speclimits(ch.samples, nil, nil, nil), 'wrong spec limits'
    }
  end

  def test2540_speclimits_all_rework_route_match  # currently fails in Fab1
    dcr = Space::DCR.new(eqp: @@eqp, lot: @@lot, route: 'RP-Test', op: 'MGT-NMETDEPDS.QA')
    dcr.add_parameters(@@parameters, @@wafer_valuesooc)
    res = $client.send_dcr(dcr, export: :caller)
    assert $spctest.verify_dcr_accepted(res,  @@parameters.size * @@wafer_valuesooc.size, nooc:'all*2'), 'DCR not submitted successfully'
    assert $spctest.verify_futurehold(@@lot, nil, nil, count: @@parameters.size, messages: @@messages), 'error setting future hold'
  end

  def test2541_speclimits_all_rework_route_no_match_no_limits
    skip "not applicable in #{$env}" unless ['itdc', 'let'].member?($env)
    #
    dcr = Space::DCR.new(eqp: @@eqp, lot: @@lot, route: 'RO-Test')
    dcr.add_parameters(@@parameters, @@wafer_valuesooc)
    res = $client.send_dcr(dcr, export: :caller)
    assert $spctest.verify_dcr_accepted(res, @@parameters.size * @@wafer_valuesooc.size, nooc: 0), 'DCR not submitted successfully'
    assert $spctest.verify_futurehold(@@lot, nil, nil, count: 0, messages: @@messages), 'error setting future hold'
  end

  def test2542_speclimits_all_rework_standard_route
    dcr = Space::DCR.new(eqp: @@eqp, lot: @@lot, op: @@mpd_qa)
    dcr.add_parameters(@@parameters, @@wafer_valuesooc)
    res = $client.send_dcr(dcr, export: :caller)
    assert $spctest.verify_dcr_accepted(res,  @@parameters.size * @@wafer_valuesooc.size, nooc: 'all*2'), 'DCR not submitted successfully'
    assert $spctest.verify_futurehold(@@lot, nil, nil, count: @@parameters.size, messages: @@messages), 'error setting future hold'
  end

  def test2543_speclimits_all_erf_route_match
    dcr = Space::DCR.new(eqp: @@eqp, lot: @@lot, op: @@mpd_qa, route: 'e1234-SIL001.01')
    dcr.add_parameters(@@parameters, @@wafer_valuesooc)
    res = $client.send_dcr(dcr, export: :caller)
    assert $spctest.verify_dcr_accepted(res,  @@parameters.size * @@wafer_valuesooc.size, nooc: 'all*2'), 'DCR not submitted successfully'
    assert $spctest.verify_futurehold(@@lot, nil, nil, count: @@parameters.size, messages: @@messages), 'error setting future hold'
  end
  
  
  # 2S Tests: After first run (when no 2S@ channels exist in Setup LDS/AutoCreated_QA folder) the channels have to be created manually 
  # by going to the exclude list and using "Create SPC Channel" (multiselect works)
  # Select one of the original channels in the AutoCreated_QA folder as template for the new channels and check box "create CKC"
  # After the channels have been created go to channel properties of each channel -> tab Settings -> uncheck "Inherit" 
  # at Control Limit Mode and choose External.
  # The last step can be skipped when other 2S@ channels already exists which have been modified in the way like this
  # When this last step is not performed - no CKC Mean Limits will be in the 2S@ channel
  def test2550_speclimits_TEST_2S_R
    lot = @@lot
    parameters = ['QA_PARAM_800_ECP', 'QA_PARAM_801_ECP', 'QA_PARAM_802_ECP', 'QA_PARAM_803_ECP', 'QA_PARAM_804_ECP']
    twosigma_parameters = parameters.collect {|p| @@twosigma_prefix + p}
    
    folder = $lds_setup.folder(@@autocreated_qa)
    mpd = @@mpd_gzv
    mtool = @@mtool
    eqp = @@eqp
    technology = 'TEST'
    pchambers = {'PM1'=>5, 'PM2'=>6, 'PM3'=>7, 'PM4'=>8, 'PM5'=>9, 'PM6'=>10}
    newlimits = {'MEAN_VALUE_UCL'=>50.0, 'MEAN_VALUE_LCL'=>40.0}
    res = nil
    expected_e10_state = 'SDT.4PMQ'

    #E10 state change
    ['2NDP','4PMQ'].each {|s| $sv.eqp_status_change_req(eqp, s)}
    ['2NDP','4PMQ'].each {|s| $sv.chamber_status_change_req(eqp, pchambers.keys, s)}
    #The state change needs some seconds, maybe
    sleep 10
    
    res = _create_2S_channels(folder, parameters, lot: lot, mpd: mpd, mtool: mtool, eqp: eqp, technology: technology, pchambers: pchambers, newlimits: newlimits)
    # verify 2S@ spc channels are created in AutoCreated_QA
    ch_not_exist = []
    twosigma_parameters.each {|p|
      ch = folder.spc_channel(parameter: p)
      ch_not_exist << p if ch == nil
    }
    if ch_not_exist.size > 0
      $log.warn "Channels #{ch_not_exist} not found in #{folder.inspect}. Please create it out of the exclude list as described in TC header and then press some key to continue"
      gets
      #send data to the newly created channels
      res = _create_2S_channels(folder, parameters, lot: lot, mpd: mpd, mtool: mtool, eqp: eqp, technology: technology, pchambers: pchambers, newlimits: newlimits)
    end
    folder = $lds_setup.folder(@@autocreated_qa)
    
    _verify_runids(res, expected_e10_state: expected_e10_state)
    
    baselimits = nil
    basetimes = []
    #check standard channel CKC mean limits, set by this test
    parameters.each {|p|
      ch = folder.spc_channel(parameter: p)
      # ensure control limits are set
      s = ch.samples[-1]
      basetimes << s.time
      baselimits = s.limits
      assert_equal(newlimits['MEAN_VALUE_UCL'].to_s, baselimits['MEAN_VALUE_UCL'].to_s, 'MEAN UCL not as expected')
      assert_equal(newlimits['MEAN_VALUE_LCL'].to_s, baselimits['MEAN_VALUE_LCL'].to_s, 'MEAN LCL not as expected')
    }
    
    gzvtimes = []
    #check 2S@ channel CKC mean limits, calculated by using green zone value
    twosigma_parameters.each {|p|
      ch = folder.spc_channel(parameter: p)
      # ensure control limits are set
      s = ch.samples[-1]
      monitoringspeclimits = $sildb.greenzonevalue(parameter: p.sub(@@twosigma_prefix,''), gzt: 'R', specid: @@monitoringSpecLimitsTable, active: 'Y')
      assert_equal(1, monitoringspeclimits.size, 'Greenzonevalue DB result not uniqe! Please check why.')
      greenzonevalue = monitoringspeclimits[0].greenzonevalue
      gzvtimes << s.time
      limits = s.limits
      xbar_UCL = baselimits['MEAN_VALUE_CENTER']+greenzonevalue*(baselimits['MEAN_VALUE_UCL']-baselimits['MEAN_VALUE_CENTER'])
      xbar_LCL = baselimits['MEAN_VALUE_CENTER']-greenzonevalue*(baselimits['MEAN_VALUE_CENTER']-baselimits['MEAN_VALUE_LCL'])
      assert_equal(xbar_UCL.to_s, limits['MEAN_VALUE_UCL'].to_s, '2S MEAN UCL not as expected')
      assert_equal(xbar_LCL.to_s, limits['MEAN_VALUE_LCL'].to_s, '2S MEAN LCL not as expected')
    }
    
    diff = []
    parameters.size.times {|i|
      diff << gzvtimes[i] - basetimes[i]
      diff.keep_if {|e| e >= 0}
    }
    assert_equal true, diff.size == parameters.size, "Not enough new samples (#{diff.size}) in GZV channel!"
  end
    
  def test2551_speclimits_TEST_2S_U
    lot = @@lot
    parameters = ['QA_PARAM_810_ECP', 'QA_PARAM_811_ECP', 'QA_PARAM_812_ECP', 'QA_PARAM_813_ECP', 'QA_PARAM_814_ECP']
    twosigma_parameters = parameters.collect {|p| @@twosigma_prefix + p}
    
    folder = $lds_setup.folder(@@autocreated_qa)
    mpd = @@mpd_gzv
    mtool = @@mtool
    eqp = @@eqp
    technology = 'TEST'
    pchambers = {'PM1'=>5, 'PM2'=>6, 'PM3'=>7, 'PM4'=>8, 'PM5'=>9, 'PM6'=>10}
    res = nil
    expected_e10_state = 'SDT.4PMQ'

    #E10 state change
    ['2NDP','4PMQ'].each {|s| $sv.eqp_status_change_req(eqp, s)}
    ['2NDP','4PMQ'].each {|s| $sv.chamber_status_change_req(eqp, pchambers.keys, s)}
    #The state change needs some seconds, maybe
    sleep 10
    
    res = _create_2S_channels(folder, parameters, lot: lot, mpd: mpd, mtool: mtool, eqp: eqp, technology: technology, pchambers: pchambers)
    
    # verify 2S@ spc channels are created in AutoCreated_QA
    ch_not_exist = []
    twosigma_parameters.each {|p|
      ch = folder.spc_channel(parameter: p)
      ch_not_exist << p if ch == nil
    }
    if ch_not_exist.size > 0
      $log.warn "Channels #{ch_not_exist} not found in SetupLDS/#{@@autocreated_qa}. Please create it out of the exclude list as described in TC header and then press some key to continue"
      gets
      #send data to the newly created channels
      res = _create_2S_channels(folder, parameters, lot: lot, mpd: mpd, mtool: mtool, eqp: eqp, technology: technology, pchambers: pchambers)
    end
    folder = $lds_setup.folder(@@autocreated_qa)
    
    _verify_runids(res, expected_e10_state: expected_e10_state)
    
    #check 2S@ channel CKC mean limits, calculated by using green zone value
    twosigma_parameters.each {|p|
      ch = folder.spc_channel(parameter: p)
      # ensure spec and sample limits are set
      s = ch.samples[-1]
      monitoringspeclimits = $sildb.greenzonevalue(parameter: p.sub(@@twosigma_prefix,''), gzt: 'U', specid: @@monitoringSpecLimitsTable, active: 'Y')
      assert_equal(1, monitoringspeclimits.size, "Greenzonevalue DB result not uniqe (#{monitoringspeclimits.size} entries found)! Please check why.")
      greenzonevalue = monitoringspeclimits[0].greenzonevalue
      
      limits = s.limits
      assert_equal(greenzonevalue.to_s, limits['MEAN_VALUE_UCL'].to_s, '2S MEAN UCL not as expected')
    }
  end
    
  def test2552_speclimits_TEST_2S_D
    lot = @@lot
    parameters = ['QA_PARAM_820_ECP', 'QA_PARAM_821_ECP', 'QA_PARAM_822_ECP', 'QA_PARAM_823_ECP', 'QA_PARAM_824_ECP']
    twosigma_parameters = parameters.collect {|p| @@twosigma_prefix + p}
    
    folder = $lds_setup.folder(@@autocreated_qa)
    mpd = @@mpd_gzv
    mtool = @@mtool
    eqp = @@eqp
    technology = 'TEST'
    pchambers = {'PM1'=>5, 'PM2'=>6, 'PM3'=>7, 'PM4'=>8, 'PM5'=>9, 'PM6'=>10}
    res = nil
    expected_e10_state = 'SDT.4PMQ'

    #E10 state change
    ['2NDP','4PMQ'].each {|s| $sv.eqp_status_change_req(eqp, s)}
    ['2NDP','4PMQ'].each {|s| $sv.chamber_status_change_req(eqp, pchambers.keys, s)}
    #The state change needs some seconds, maybe
    sleep 10
    
    res = _create_2S_channels(folder, parameters, lot: lot, mpd: mpd, mtool: mtool, eqp: eqp, technology: technology, pchambers: pchambers)
    
    # verify 2S@ spc channels are created in AutoCreated_QA
    ch_not_exist = []
    twosigma_parameters.each {|p|
      ch = folder.spc_channel(parameter: p)
      ch_not_exist << p if ch == nil
    }
    if ch_not_exist.size > 0
      $log.warn "Channels #{ch_not_exist} not found in SetupLDS/#{@@autocreated_qa}. Please create it out of the exclude list as described in TC header and then press some key to continue"
      gets
      #send data to the newly created channels
      res = _create_2S_channels(folder, parameters, lot: lot, mpd: mpd, mtool: mtool, eqp: eqp, technology: technology, pchambers: pchambers)
    end
    folder = $lds_setup.folder(@@autocreated_qa)
    
    _verify_runids(res, expected_e10_state: expected_e10_state)
    
    #check 2S@ channel CKC mean limits, calculated by using green zone value
    twosigma_parameters.each {|p|
      ch = folder.spc_channel(parameter: p)
      # ensure spec and sample limits are set
      s = ch.samples[-1]
      monitoringspeclimits = $sildb.greenzonevalue(parameter: p.sub(@@twosigma_prefix,''), gzt: 'D', specid: @@monitoringSpecLimitsTable, active: 'Y')
      assert_equal(1, monitoringspeclimits.size, "Greenzonevalue DB result not uniqe (#{monitoringspeclimits.size} entries found)! Please check why.")
      greenzonevalue = monitoringspeclimits[0].greenzonevalue
      
      limits = s.limits
      assert_equal(greenzonevalue.to_s, limits['MEAN_VALUE_UCL'].to_s, '2S MEAN UCL not as expected')
    }
  end
    
  def test2553_speclimits_TEST_2S_N
    lot = @@lot
    parameters = ['QA_PARAM_830_ECP', 'QA_PARAM_831_ECP', 'QA_PARAM_832_ECP', 'QA_PARAM_833_ECP', 'QA_PARAM_834_ECP']
    twosigma_parameters = parameters.collect {|p| @@twosigma_prefix + p}
    
    folder = $lds_setup.folder(@@autocreated_qa)
    mpd = @@mpd_gzv
    mtool = @@mtool
    eqp = @@eqp
    technology = 'TEST'
    pchambers = {'PM1'=>5, 'PM2'=>6, 'PM3'=>7, 'PM4'=>8, 'PM5'=>9, 'PM6'=>10}
    res = nil
    expected_e10_state = 'SDT.4PMQ'

    #E10 state change
    ['2NDP','4PMQ'].each {|s| $sv.eqp_status_change_req(eqp, s)}
    ['2NDP','4PMQ'].each {|s| $sv.chamber_status_change_req(eqp, pchambers.keys, s)}
    #The state change needs some seconds, maybe
    sleep 10
    
    res = _create_2S_channels(folder, parameters, lot: lot, mpd: mpd, mtool: mtool, eqp: eqp, technology: technology, pchambers: pchambers)
    
    # verify 2S@ spc channels are not created in AutoCreated_QA
    ch_not_exist = []
    twosigma_parameters.each {|p|
      ch = folder.spc_channel(parameter: p)
      if ch == nil
        ch_not_exist << p
      end
    }
    if ch_not_exist.size < twosigma_parameters.size
      $log.warn "!More channels found in SetupLDS/#{@@autocreated_qa} then expected!"
    end
    
    _verify_runids(res, expected_e10_state: expected_e10_state)
    
    #check 2S@ channel CKC mean limits, calculated by using green zone value
    twosigma_parameters.each {|p|
      monitoringspeclimits = $sildb.greenzonevalue(parameter: p.sub(@@twosigma_prefix,''), gzt: 'N', specid: @@monitoringSpecLimitsTable, active: 'Y')
      assert_equal(1, monitoringspeclimits.size, "Greenzonevalue DB result not uniqe (#{monitoringspeclimits.size} entries found)! Please check why.")
    }
      
    $log.warn "Please verify that there are no 2S@QA_PARAM_83X_ECP channels in the exclude list, and not in the SetupLDS/#{@@autocreated_qa} folder too, then press any key"
    gets
  end
    
  def test2554_speclimits_TEST_2S_NoGZVSetup
    lot = @@lot
    parameters = ['QA_PARAM_840_ECP', 'QA_PARAM_841_ECP', 'QA_PARAM_842_ECP', 'QA_PARAM_843_ECP', 'QA_PARAM_844_ECP']
    twosigma_parameters = parameters.collect {|p| @@twosigma_prefix + p}
    
    folder = $lds_setup.folder(@@autocreated_qa)
    mpd = @@mpd_gzv
    mtool = @@mtool
    eqp = @@eqp
    technology = 'TEST'
    pchambers = {'PM1'=>5, 'PM2'=>6, 'PM3'=>7, 'PM4'=>8, 'PM5'=>9, 'PM6'=>10}
    res = nil
    expected_e10_state = 'SDT.4PMQ'

    #E10 state change
    ['2NDP','4PMQ'].each {|s| $sv.eqp_status_change_req(eqp, s)}
    ['2NDP','4PMQ'].each {|s| $sv.chamber_status_change_req(eqp, pchambers.keys, s)}
    #The state change needs some seconds, maybe
    sleep 10
    
    res = _create_2S_channels(folder, parameters, lot: lot, mpd: mpd, mtool: mtool, eqp: eqp, technology: technology, pchambers: pchambers)
    
    # verify 2S@ spc channels are not created in AutoCreated_QA
    ch_not_exist = []
    twosigma_parameters.each {|p|
      ch = folder.spc_channel(parameter: p)
      ch_not_exist << p if ch == nil
    }
    $log.warn "!More channels found in SetupLDS/#{@@autocreated_qa} then expected!" if ch_not_exist.size < twosigma_parameters.size
    
    _verify_runids(res, expected_e10_state: expected_e10_state)
    
    $log.warn "Please verify that there are no 2S@QA_PARAM_84X_ECP channels in the exclude list, and not in the SetupLDS/#{@@autocreated_qa} folder too, then press any key"
    gets
  end
  
  def test2555_speclimits_TEST_2S_R_AllChambersDown_ToolUp
    lot = @@lot
    parameters = ['QA_PARAM_800_ECP', 'QA_PARAM_801_ECP', 'QA_PARAM_802_ECP', 'QA_PARAM_803_ECP', 'QA_PARAM_804_ECP']
    twosigma_parameters = parameters.collect {|p| @@twosigma_prefix + p}
    
    folder = $lds_setup.folder(@@autocreated_qa)
    mpd = @@mpd_gzv
    mtool = @@mtool
    eqp = @@eqp
    technology = 'TEST'
    pchambers = {'PM1'=>5, 'PM2'=>6, 'PM3'=>7, 'PM4'=>8, 'PM5'=>9, 'PM6'=>10}
    newlimits = {'MEAN_VALUE_UCL'=>50.0, 'MEAN_VALUE_LCL'=>40.0}
    res = nil
    expected_eqp_e10_state = 'SBY.2WPR'
    expected_chamber_e10_state = 'SDT.4PMQ'
  
    #E10 state change
    ['2NDP','2WPR'].each {|s| $sv.eqp_status_change_req(eqp, s)}
    ['2NDP','4PMQ'].each {|s| $sv.chamber_status_change_req(eqp, pchambers.keys, s)}
    #The state change needs some seconds, maybe
    sleep 10
    
    res = _create_2S_channels(folder, parameters, lot: lot, mpd: mpd, mtool: mtool, eqp: eqp, technology: technology, pchambers: pchambers, newlimits: newlimits)
    
    # verify 2S@ spc channels are created in AutoCreated_QA
    ch_not_exist = []
    twosigma_parameters.each {|p|
      ch = folder.spc_channel(parameter: p)
      ch_not_exist << p if ch == nil
    }
    if ch_not_exist.size > 0
      $log.warn "Channels #{ch_not_exist} not found in SetupLDS/#{@@autocreated_qa}. Please create it out of the exclude list as described in TC header and then press some key to continue"
      gets
      #send data to the newly created channels
      res = _create_2S_channels(folder, parameters, lot: lot, mpd: mpd, mtool: mtool, eqp: eqp, technology: technology, pchambers: pchambers, newlimits: newlimits)
    end
    
    _verify_runids(res, expected_eqp_e10_state: expected_eqp_e10_state, expected_chamber_e10_state: expected_chamber_e10_state)
    
    baselimits = nil
    basetimes = []
    #check standard channel CKC mean limits, set by this test
    parameters.each {|p|
      ch = folder.spc_channel(parameter: p)
      # ensure control limits are set
      s = ch.samples[-1]
      basetimes << s.time
      baselimits = s.limits
      assert_equal(newlimits['MEAN_VALUE_UCL'].to_s, baselimits['MEAN_VALUE_UCL'].to_s, 'MEAN UCL not as expected')
      assert_equal(newlimits['MEAN_VALUE_LCL'].to_s, baselimits['MEAN_VALUE_LCL'].to_s, 'MEAN LCL not as expected')
    }
    
    gzvtimes = []
    #check 2S@ channel CKC mean limits, calculated by using green zone value
    twosigma_parameters.each {|p|
      ch = folder.spc_channel(parameter: p)
      # ensure control limits are set
      s = ch.samples[-1]
      monitoringspeclimits = $sildb.greenzonevalue(parameter: p.sub(@@twosigma_prefix,''), gzt: 'R', specid: @@monitoringSpecLimitsTable, active: 'Y')
      assert_equal(1, monitoringspeclimits.size, 'Greenzonevalue DB result not uniqe! Please check why.')
      greenzonevalue = monitoringspeclimits[0].greenzonevalue
      gzvtimes << s.time
      limits = s.limits
      xbar_UCL = baselimits['MEAN_VALUE_CENTER']+greenzonevalue*(baselimits['MEAN_VALUE_UCL']-baselimits['MEAN_VALUE_CENTER'])
      xbar_LCL = baselimits['MEAN_VALUE_CENTER']-greenzonevalue*(baselimits['MEAN_VALUE_CENTER']-baselimits['MEAN_VALUE_LCL'])
      assert_equal(xbar_UCL.to_s, limits['MEAN_VALUE_UCL'].to_s, '2S MEAN UCL not as expected')
      assert_equal(xbar_LCL.to_s, limits['MEAN_VALUE_LCL'].to_s, '2S MEAN LCL not as expected')
    }
    
    diff = []
    parameters.size.times {|i|
      diff << gzvtimes[i] - basetimes[i]
      diff.keep_if {|e| e >= 0}
    }
    assert_equal true, diff.size == parameters.size, "Not enough new samples (#{diff.size}) in GZV channel!"
  end
  
  def test2556_speclimits_TEST_2S_R_OneChamberDown_ToolUp
    lot = @@lot
    parameters = ['QA_PARAM_800_ECP', 'QA_PARAM_801_ECP', 'QA_PARAM_802_ECP', 'QA_PARAM_803_ECP', 'QA_PARAM_804_ECP']
    twosigma_parameters = parameters.collect {|p| @@twosigma_prefix + p}
    
    folder = $lds_setup.folder(@@autocreated_qa)
    mpd = @@mpd_gzv
    mtool = @@mtool
    eqp = @@eqp
    technology = 'TEST'
    pchambers = {'PM1'=>5, 'PM2'=>6, 'PM3'=>7, 'PM4'=>8, 'PM5'=>9, 'PM6'=>10}
    newlimits = {'MEAN_VALUE_UCL'=>50.0, 'MEAN_VALUE_LCL'=>40.0}
    res = nil
    expected_eqp_e10_state = 'SBY.2WPR'
    expected_chamber_e10_state = 'SDT.4PMQ'
  
    #E10 state change
    ['2NDP','2WPR'].each {|s| $sv.eqp_status_change_req(eqp, s)}
    ['2NDP','2WPR'].each {|s| $sv.chamber_status_change_req(eqp, pchambers.keys, s)}
    ['2NDP','4PMQ'].each {|s| $sv.chamber_status_change_req(eqp, pchambers.keys.first, s)}
    #The state change needs some seconds, maybe
    sleep 10
    
    res = _create_2S_channels(folder, parameters, lot: lot, mpd: mpd, mtool: mtool, eqp: eqp, technology: technology, pchambers: pchambers, newlimits: newlimits)
    
    # verify 2S@ spc channels are created in AutoCreated_QA
    ch_not_exist = []
    twosigma_parameters.each {|p|
      ch = folder.spc_channel(parameter: p)
      ch_not_exist << p if ch == nil
    }
    if ch_not_exist.size > 0
      $log.warn "Channels #{ch_not_exist} not found in SetupLDS/#{@@autocreated_qa}. Please create it out of the exclude list as described in TC header and then press some key to continue"
      gets
      #send data to the newly created channels
      res = _create_2S_channels(folder, parameters, lot: lot, mpd: mpd, mtool: mtool, eqp: eqp, technology: technology, pchambers: pchambers, newlimits: newlimits)
    end
    
    _verify_runids(res, expected_eqp_e10_state: expected_eqp_e10_state, expected_chamber_e10_state: expected_chamber_e10_state, multichamberstates: true)
    
    baselimits = nil
    basetimes = []
    #check standard channel CKC mean limits, set by this test
    parameters.each {|p|
      ch = folder.spc_channel(parameter: p)
      # ensure control limits are set
      s = ch.samples[-1]
      basetimes << s.time
      baselimits = s.limits
      assert_equal(newlimits['MEAN_VALUE_UCL'].to_s, baselimits['MEAN_VALUE_UCL'].to_s, 'MEAN UCL not as expected')
      assert_equal(newlimits['MEAN_VALUE_LCL'].to_s, baselimits['MEAN_VALUE_LCL'].to_s, 'MEAN LCL not as expected')
    }
    
    gzvtimes = []
    #check 2S@ channel CKC mean limits, calculated by using green zone value
    twosigma_parameters.each {|p|
      ch = folder.spc_channel(parameter: p)
      # ensure control limits are set
      s = ch.samples[-1]
      monitoringspeclimits = $sildb.greenzonevalue(parameter: p.sub(@@twosigma_prefix,''), gzt: 'R', specid: @@monitoringSpecLimitsTable, active: 'Y')
      assert_equal(1, monitoringspeclimits.size, 'Greenzonevalue DB result not uniqe! Please check why.')
      greenzonevalue = monitoringspeclimits[0].greenzonevalue
      gzvtimes << s.time
      limits = s.limits
      xbar_UCL = baselimits['MEAN_VALUE_CENTER']+greenzonevalue*(baselimits['MEAN_VALUE_UCL']-baselimits['MEAN_VALUE_CENTER'])
      xbar_LCL = baselimits['MEAN_VALUE_CENTER']-greenzonevalue*(baselimits['MEAN_VALUE_CENTER']-baselimits['MEAN_VALUE_LCL'])
      assert_equal(xbar_UCL.to_s, limits['MEAN_VALUE_UCL'].to_s, '2S MEAN UCL not as expected')
      assert_equal(xbar_LCL.to_s, limits['MEAN_VALUE_LCL'].to_s, '2S MEAN LCL not as expected')
    }
    
    diff = []
    parameters.size.times {|i|
      diff << gzvtimes[i] - basetimes[i]
      diff.keep_if {|e| e >= 0}
    }
    assert_equal true, diff.size == parameters.size, "Not enough new samples (#{diff.size}) in GZV channel!"
  end
  
  def test2557_speclimits_TEST_2S_R_AllChambersUp_ToolDown
    lot = @@lot
    parameters = ['QA_PARAM_800_ECP', 'QA_PARAM_801_ECP', 'QA_PARAM_802_ECP', 'QA_PARAM_803_ECP', 'QA_PARAM_804_ECP']
    twosigma_parameters = parameters.collect {|p| @@twosigma_prefix + p}
    
    folder = $lds_setup.folder(@@autocreated_qa)
    mpd = @@mpd_gzv
    mtool = @@mtool
    eqp = @@eqp
    technology = 'TEST'
    pchambers = {'PM1'=>5, 'PM2'=>6, 'PM3'=>7, 'PM4'=>8, 'PM5'=>9, 'PM6'=>10}
    newlimits = {'MEAN_VALUE_UCL'=>50.0, 'MEAN_VALUE_LCL'=>40.0}
    res = nil
    expected_eqp_e10_state = 'SDT.4PMQ'
    expected_chamber_e10_state = 'SBY.2WPR'
  
    #E10 state change
    ['2NDP','4PMQ'].each {|s| $sv.eqp_status_change_req(eqp, s)}
    ['2NDP','2WPR'].each {|s| $sv.chamber_status_change_req(eqp, pchambers.keys, s)}
    #The state change needs some seconds, maybe
    sleep 10
    
    res = _create_2S_channels(folder, parameters, lot: lot, mpd: mpd, mtool: mtool, eqp: eqp, technology: technology, pchambers: pchambers, newlimits: newlimits)
    
    # verify 2S@ spc channels are created in AutoCreated_QA
    ch_not_exist = []
    twosigma_parameters.each {|p|
      ch = folder.spc_channel(parameter: p)
      ch_not_exist << p if ch == nil
    }
    if ch_not_exist.size > 0
      $log.warn "Channels #{ch_not_exist} not found in SetupLDS/#{@@autocreated_qa}. Please create it out of the exclude list as described in TC header and then press some key to continue"
      gets
      #send data to the newly created channels
      res = _create_2S_channels(folder, parameters, lot: lot, mpd: mpd, mtool: mtool, eqp: eqp, technology: technology, pchambers: pchambers, newlimits: newlimits)
    end
    
    _verify_runids(res, expected_eqp_e10_state: expected_eqp_e10_state, expected_chamber_e10_state: expected_chamber_e10_state)
    
    baselimits = nil
    basetimes = []
    #check standard channel CKC mean limits, set by this test
    parameters.each {|p|
      ch = folder.spc_channel(parameter: p)
      # ensure control limits are set
      s = ch.samples[-1]
      basetimes << s.time
      baselimits = s.limits
      assert_equal(newlimits['MEAN_VALUE_UCL'].to_s, baselimits['MEAN_VALUE_UCL'].to_s, 'MEAN UCL not as expected')
      assert_equal(newlimits['MEAN_VALUE_LCL'].to_s, baselimits['MEAN_VALUE_LCL'].to_s, 'MEAN LCL not as expected')
    }
    
    gzvtimes = []
    #check 2S@ channel CKC mean limits, calculated by using green zone value
    twosigma_parameters.each {|p|
      ch = folder.spc_channel(parameter: p)
      # ensure control limits are set
      s = ch.samples[-1]
      monitoringspeclimits = $sildb.greenzonevalue(parameter: p.sub(@@twosigma_prefix,''), gzt: 'R', specid: @@monitoringSpecLimitsTable, active: 'Y')
      assert_equal(1, monitoringspeclimits.size, 'Greenzonevalue DB result not uniqe! Please check why.')
      greenzonevalue = monitoringspeclimits[0].greenzonevalue
      gzvtimes << s.time
      limits = s.limits
      xbar_UCL = baselimits['MEAN_VALUE_CENTER']+greenzonevalue*(baselimits['MEAN_VALUE_UCL']-baselimits['MEAN_VALUE_CENTER'])
      xbar_LCL = baselimits['MEAN_VALUE_CENTER']-greenzonevalue*(baselimits['MEAN_VALUE_CENTER']-baselimits['MEAN_VALUE_LCL'])
      assert_equal(xbar_UCL.to_s, limits['MEAN_VALUE_UCL'].to_s, '2S MEAN UCL not as expected')
      assert_equal(xbar_LCL.to_s, limits['MEAN_VALUE_LCL'].to_s, '2S MEAN LCL not as expected')
    }
    
    diff = []
    parameters.size.times {|i|
      diff << gzvtimes[i] - basetimes[i]
      diff.keep_if {|e| e >= 0}
    }
    assert_equal true, diff.size == parameters.size, "Not enough new samples (#{diff.size}) in GZV channel!"
  end
  
  def test2558_speclimits_TEST_2S_R_AllUp_No2SCharting
    lot = @@lot
    parameters = ['QA_PARAM_800_ECP', 'QA_PARAM_801_ECP', 'QA_PARAM_802_ECP', 'QA_PARAM_803_ECP', 'QA_PARAM_804_ECP']
    twosigma_parameters = parameters.collect {|p| @@twosigma_prefix + p}
    
    folder = $lds_setup.folder(@@autocreated_qa)
    mpd = @@mpd_gzv
    mtool = @@mtool
    eqp = @@eqp
    technology = 'TEST'
    pchambers = {'PM1'=>5, 'PM2'=>6, 'PM3'=>7, 'PM4'=>8, 'PM5'=>9, 'PM6'=>10}
    newlimits = {'MEAN_VALUE_UCL'=>50.0, 'MEAN_VALUE_LCL'=>40.0}
    res = nil
    expected_eqp_e10_state = 'SBY.2WPR'
    expected_chamber_e10_state = 'SBY.2WPR'
  
    #E10 state change
    ['2NDP','2WPR'].each {|s| $sv.eqp_status_change_req(eqp, s)}
    ['2NDP','2WPR'].each {|s| $sv.chamber_status_change_req(eqp, pchambers.keys, s)}
    #The state change needs some seconds, maybe
    sleep 10
    
    res = _create_2S_channels(folder, parameters, lot: lot, mpd: mpd, mtool: mtool, eqp: eqp, technology: technology, pchambers: pchambers, newlimits: newlimits)
    
    # verify 2S@ spc channels are created in AutoCreated_QA
    ch_not_exist = []
    twosigma_parameters.each {|p|
      ch = folder.spc_channel(parameter: p)
      ch_not_exist << p if ch == nil
    }
    if ch_not_exist.size > 0
      $log.warn "Channels #{ch_not_exist} not found in SetupLDS/#{@@autocreated_qa}. Please create it out of the exclude list as described in TC header and then press some key to continue"
      gets
      #send data to the newly created channels
      res = _create_2S_channels(folder, parameters, lot: lot, mpd: mpd, mtool: mtool, eqp: eqp, technology: technology, pchambers: pchambers, newlimits: newlimits)
    end
    
    _verify_runids(res, expected_eqp_e10_state: expected_eqp_e10_state, expected_chamber_e10_state: expected_chamber_e10_state)
    
    baselimits = nil
    basetimes = []
    #check standard channel CKC mean limits, set by this test
    parameters.each {|p|
      ch = folder.spc_channel(parameter: p)
      # ensure control limits are set
      s = ch.samples[-1]
      basetimes << s.time
      baselimits = s.limits
      assert_equal(newlimits['MEAN_VALUE_UCL'].to_s, baselimits['MEAN_VALUE_UCL'].to_s, 'MEAN UCL not as expected')
      assert_equal(newlimits['MEAN_VALUE_LCL'].to_s, baselimits['MEAN_VALUE_LCL'].to_s, 'MEAN LCL not as expected')
    }
    
    gzvtimes = []
    #check 2S@ channel CKC mean limits, calculated by using green zone value
    twosigma_parameters.each {|p|
      ch = folder.spc_channel(parameter: p)
      # ensure control limits are set
      s = ch.samples[-1]
      monitoringspeclimits = $sildb.greenzonevalue(parameter: p.sub(@@twosigma_prefix,''), gzt: 'R', specid: @@monitoringSpecLimitsTable, active: 'Y')
      assert_equal(1, monitoringspeclimits.size, 'Greenzonevalue DB result not uniqe! Please check why.')
      greenzonevalue = monitoringspeclimits[0].greenzonevalue
      gzvtimes << s.time
      limits = s.limits
      xbar_UCL = baselimits['MEAN_VALUE_CENTER']+greenzonevalue*(baselimits['MEAN_VALUE_UCL']-baselimits['MEAN_VALUE_CENTER'])
      xbar_LCL = baselimits['MEAN_VALUE_CENTER']-greenzonevalue*(baselimits['MEAN_VALUE_CENTER']-baselimits['MEAN_VALUE_LCL'])
      assert_equal(xbar_UCL.to_s, limits['MEAN_VALUE_UCL'].to_s, '2S MEAN UCL not as expected')
      assert_equal(xbar_LCL.to_s, limits['MEAN_VALUE_LCL'].to_s, '2S MEAN LCL not as expected')
    }
    
    diff = []
    parameters.size.times {|i|
      diff << gzvtimes[i] - basetimes[i]
      diff.keep_if {|e| e >= 0}
    }
    assert_equal true, diff.size == 0, "Unexpected new samples (#{diff.size}) in GZV channel!"
  end
  
  def test2559_speclimits_TEST_2S_R_SingleChamber_ChamberDown_ToolUp
    lot = @@lot
    parameters = ['QA_PARAM_800_ECP', 'QA_PARAM_801_ECP', 'QA_PARAM_802_ECP', 'QA_PARAM_803_ECP', 'QA_PARAM_804_ECP']
    twosigma_parameters = parameters.collect {|p| @@twosigma_prefix + p}
    
    folder = $lds_setup.folder(@@autocreated_qa)
    mpd = @@mpd_gzv
    mtool = @@mtool
    eqp = @@eqp
    technology = 'TEST'
    pchambers = {'PM1'=>5}
    newlimits = {'MEAN_VALUE_UCL'=>50.0, 'MEAN_VALUE_LCL'=>40.0}
    res = nil
    expected_eqp_e10_state = 'SBY.2WPR'
    expected_chamber_e10_state = 'SDT.4PMQ'
  
    #E10 state change
    ['2NDP','2WPR'].each {|s| $sv.eqp_status_change_req(eqp, s)}
    ['2NDP','4PMQ'].each {|s| $sv.chamber_status_change_req(eqp, pchambers.keys, s)}
    #The state change needs some seconds, maybe
    sleep 10
    
    res = _create_2S_channels(folder, parameters, lot: lot, mpd: mpd, mtool: mtool, eqp: eqp, technology: technology, pchambers: pchambers, newlimits: newlimits)
    
    # verify 2S@ spc channels are created in AutoCreated_QA
    ch_not_exist = []
    twosigma_parameters.each {|p|
      ch = folder.spc_channel(parameter: p)
      ch_not_exist << p if ch == nil
    }
    if ch_not_exist.size > 0
      $log.warn "Channels #{ch_not_exist} not found in SetupLDS/#{@@autocreated_qa}. Please create it out of the exclude list as described in TC header and then press some key to continue"
      gets
      #send data to the newly created channels
      res = _create_2S_channels(folder, parameters, lot: lot, mpd: mpd, mtool: mtool, eqp: eqp, technology: technology, pchambers: pchambers, newlimits: newlimits)
    end
    
    _verify_runids(res, expected_eqp_e10_state: expected_eqp_e10_state, expected_chamber_e10_state: expected_chamber_e10_state)
    
    baselimits = nil
    basetimes = []
    #check standard channel CKC mean limits, set by this test
    parameters.each {|p|
      ch = folder.spc_channel(parameter: p)
      # ensure control limits are set
      s = ch.samples[-1]
      basetimes << s.time
      baselimits = s.limits
      assert_equal(newlimits['MEAN_VALUE_UCL'].to_s, baselimits['MEAN_VALUE_UCL'].to_s, 'MEAN UCL not as expected')
      assert_equal(newlimits['MEAN_VALUE_LCL'].to_s, baselimits['MEAN_VALUE_LCL'].to_s, 'MEAN LCL not as expected')
    }
    
    gzvtimes = []
    #check 2S@ channel CKC mean limits, calculated by using green zone value
    twosigma_parameters.each {|p|
      ch = folder.spc_channel(parameter: p)
      # ensure control limits are set
      s = ch.samples[-1]
      monitoringspeclimits = $sildb.greenzonevalue(parameter: p.sub(@@twosigma_prefix,''), gzt: 'R', specid: @@monitoringSpecLimitsTable, active: 'Y')
      assert_equal(1, monitoringspeclimits.size, 'Greenzonevalue DB result not uniqe! Please check why.')
      greenzonevalue = monitoringspeclimits[0].greenzonevalue
      gzvtimes << s.time
      limits = s.limits
      xbar_UCL = baselimits['MEAN_VALUE_CENTER']+greenzonevalue*(baselimits['MEAN_VALUE_UCL']-baselimits['MEAN_VALUE_CENTER'])
      xbar_LCL = baselimits['MEAN_VALUE_CENTER']-greenzonevalue*(baselimits['MEAN_VALUE_CENTER']-baselimits['MEAN_VALUE_LCL'])
      assert_equal(xbar_UCL.to_s, limits['MEAN_VALUE_UCL'].to_s, '2S MEAN UCL not as expected')
      assert_equal(xbar_LCL.to_s, limits['MEAN_VALUE_LCL'].to_s, '2S MEAN LCL not as expected')
    }
    
    diff = []
    parameters.size.times {|i|
      diff << gzvtimes[i] - basetimes[i]
      diff.keep_if {|e| e >= 0}
    }
    assert_equal true, diff.size == parameters.size, "Not enough new samples (#{diff.size}) in GZV channel!"
  end
  
  def test2560_speclimits_TEST_2S_R_basechannel_study
    $log.warn '* * * * * * * * * * * * * * * * * * * * * '
    $log.warn 'Verify the settings accordingly to SIL'
    $log.warn "spaceupload.twosigmachart.channel.state.ignored = #{@@spaceupload_twosigmachart_channel_state_ignored.join(',')}"
    $log.warn '* * * * * * * * * * * * * * * * * * * * * '
    lot = @@lot
    parameters = ['QA_PARAM_800_ECP', 'QA_PARAM_801_ECP', 'QA_PARAM_802_ECP', 'QA_PARAM_803_ECP', 'QA_PARAM_804_ECP']
    twosigma_parameters = parameters.collect {|p| @@twosigma_prefix + p}
    
    folder = $lds_setup.folder(@@autocreated_qa)
    mpd = @@mpd_gzv
    mtool = @@mtool
    eqp = @@eqp
    technology = 'TEST'
    pchambers = {'PM1'=>5, 'PM2'=>6, 'PM3'=>7, 'PM4'=>8, 'PM5'=>9, 'PM6'=>10}
    newlimits = {'MEAN_VALUE_UCL'=>50.0, 'MEAN_VALUE_LCL'=>40.0}
    res = nil
    expected_e10_state = 'SDT.4PMQ'

    #E10 state change
    ['2NDP','4PMQ'].each {|s| $sv.eqp_status_change_req(eqp, s)}
    ['2NDP','4PMQ'].each {|s| $sv.chamber_status_change_req(eqp, pchambers.keys, s)}
    #The state change needs some seconds, maybe
    sleep 10
    
    res = _create_2S_channels(folder, parameters, lot: lot, mpd: mpd, mtool: mtool, eqp: eqp, technology: technology, pchambers: pchambers, newlimits: newlimits, state: 'Study')
    # verify 2S@ spc channels are created in AutoCreated_QA
    ch_not_exist = []
    twosigma_parameters.each {|p|
      ch = folder.spc_channel(parameter: p)
      ch_not_exist << p if ch == nil
    }
    if ch_not_exist.size > 0
      $log.warn "Channels #{ch_not_exist} not found in SetupLDS/#{@@autocreated_qa}. Please create it out of the exclude list as described in TC header and then press some key to continue"
      gets
      #send data to the newly created channels
      res = _create_2S_channels(folder, parameters, lot: lot, mpd: mpd, mtool: mtool, eqp: eqp, technology: technology, pchambers: pchambers, newlimits: newlimits, state: 'Study')
    end
    
    _verify_runids(res, expected_e10_state: expected_e10_state)
    
    baselimits = nil
    basetimes = []
    #check standard channel CKC mean limits, set by this test
    parameters.each {|p|
      ch = folder.spc_channel(parameter: p)
      # ensure control limits are set
      s = ch.samples[-1]
      basetimes << s.time
      baselimits = s.limits
      assert_equal(newlimits['MEAN_VALUE_UCL'].to_s, baselimits['MEAN_VALUE_UCL'].to_s, 'MEAN UCL not as expected')
      assert_equal(newlimits['MEAN_VALUE_LCL'].to_s, baselimits['MEAN_VALUE_LCL'].to_s, 'MEAN LCL not as expected')
    }
    
    gzvtimes = []
    #check 2S@ channel CKC mean limits, calculated by using green zone value
    twosigma_parameters.each {|p|
      ch = folder.spc_channel(parameter: p)
      # ensure control limits are set
      s = ch.samples[-1]
      monitoringspeclimits = $sildb.greenzonevalue(parameter: p.sub(@@twosigma_prefix,''), gzt: 'R', specid: @@monitoringSpecLimitsTable, active: 'Y')
      assert_equal(1, monitoringspeclimits.size, 'Greenzonevalue DB result not uniqe! Please check why.')
      greenzonevalue = monitoringspeclimits[0].greenzonevalue
      gzvtimes << s.time
      limits = s.limits
      xbar_UCL = baselimits['MEAN_VALUE_CENTER']+greenzonevalue*(baselimits['MEAN_VALUE_UCL']-baselimits['MEAN_VALUE_CENTER'])
      xbar_LCL = baselimits['MEAN_VALUE_CENTER']-greenzonevalue*(baselimits['MEAN_VALUE_CENTER']-baselimits['MEAN_VALUE_LCL'])
      assert_equal(xbar_UCL.to_s, limits['MEAN_VALUE_UCL'].to_s, '2S MEAN UCL not as expected')
      assert_equal(xbar_LCL.to_s, limits['MEAN_VALUE_LCL'].to_s, '2S MEAN LCL not as expected')
    }
    
    diff = []
    parameters.size.times {|i|
      diff << gzvtimes[i] - basetimes[i]
      diff.keep_if {|e| e >= 0}
    }
    if @@spaceupload_twosigmachart_channel_state_ignored.include?('Study')
      assert_equal true, diff.size == 0, "Unexpected new samples (#{diff.size}) found in GZV channel!"
    else
      assert_equal true, diff.size == parameters.size, "Not enough new samples (#{diff.size}) in GZV channel!"
    end
  end
  
  def test2561_speclimits_TEST_2S_R_basechannel_archived
    lot = @@lot
    parameters = ['QA_PARAM_800_ECP', 'QA_PARAM_801_ECP', 'QA_PARAM_802_ECP', 'QA_PARAM_803_ECP', 'QA_PARAM_804_ECP']
    twosigma_parameters = parameters.collect {|p| @@twosigma_prefix + p}
    
    folder = $lds_setup.folder(@@autocreated_qa)
    mpd = @@mpd_gzv
    mtool = @@mtool
    eqp = @@eqp
    technology = 'TEST'
    pchambers = {'PM1'=>5, 'PM2'=>6, 'PM3'=>7, 'PM4'=>8, 'PM5'=>9, 'PM6'=>10}
    newlimits = {'MEAN_VALUE_UCL'=>50.0, 'MEAN_VALUE_LCL'=>40.0}
    res = nil
    expected_e10_state = 'SDT.4PMQ'

    #E10 state change
    ['2NDP','4PMQ'].each {|s| $sv.eqp_status_change_req(eqp, s)}
    ['2NDP','4PMQ'].each {|s| $sv.chamber_status_change_req(eqp, pchambers.keys, s)}
    #The state change needs some seconds, maybe
    sleep 10
    
    res = _create_2S_channels(folder, parameters, lot: lot, mpd: mpd, mtool: mtool, eqp: eqp, technology: technology, pchambers: pchambers, newlimits: newlimits, state: 'Archived')
    # verify 2S@ spc channels are created in AutoCreated_QA
    ch_not_exist = []
    twosigma_parameters.each {|p|
      ch = folder.spc_channel(parameter: p)
      ch_not_exist << p if ch == nil
    }
    if ch_not_exist.size > 0
      $log.warn "Channels #{ch_not_exist} not found in SetupLDS/#{@@autocreated_qa}. Please create it out of the exclude list as described in TC header and then press some key to continue"
      gets
      #send data to the newly created channels
      res = _create_2S_channels(folder, parameters, lot: lot, mpd: mpd, mtool: mtool, eqp: eqp, technology: technology, pchambers: pchambers, newlimits: newlimits, state: 'Archived')
    end
    
    _verify_runids(res, expected_e10_state: expected_e10_state)
    
    baselimits = nil
    basetimes = []
    #check standard channel CKC mean limits, set by this test
    parameters.each {|p|
      ch = folder.spc_channel(parameter: p)
      # ensure control limits are set
      s = ch.samples[-1]
      basetimes << s.time
      baselimits = s.limits
      assert_equal(newlimits['MEAN_VALUE_UCL'].to_s, baselimits['MEAN_VALUE_UCL'].to_s, 'MEAN UCL not as expected')
      assert_equal(newlimits['MEAN_VALUE_LCL'].to_s, baselimits['MEAN_VALUE_LCL'].to_s, 'MEAN LCL not as expected')
    }
    
    gzvtimes = []
    #check 2S@ channel CKC mean limits, calculated by using green zone value
    twosigma_parameters.each {|p|
      ch = folder.spc_channel(parameter: p)
      # ensure control limits are set
      s = ch.samples[-1]
      monitoringspeclimits = $sildb.greenzonevalue(parameter: p.sub(@@twosigma_prefix,''), gzt: 'R', specid: @@monitoringSpecLimitsTable, active: 'Y')
      assert_equal(1, monitoringspeclimits.size, 'Greenzonevalue DB result not uniqe! Please check why.')
      greenzonevalue = monitoringspeclimits[0].greenzonevalue
      gzvtimes << s.time
      limits = s.limits
      xbar_UCL = baselimits['MEAN_VALUE_CENTER']+greenzonevalue*(baselimits['MEAN_VALUE_UCL']-baselimits['MEAN_VALUE_CENTER'])
      xbar_LCL = baselimits['MEAN_VALUE_CENTER']-greenzonevalue*(baselimits['MEAN_VALUE_CENTER']-baselimits['MEAN_VALUE_LCL'])
      assert_equal(xbar_UCL.to_s, limits['MEAN_VALUE_UCL'].to_s, '2S MEAN UCL not as expected')
      assert_equal(xbar_LCL.to_s, limits['MEAN_VALUE_LCL'].to_s, '2S MEAN LCL not as expected')
    }
    
    diff = []
    parameters.size.times {|i|
      diff << gzvtimes[i] - basetimes[i]
      diff.keep_if {|e| e >= 0}
    }
    assert_equal true, diff.size == 0, "Unexpected new samples (#{diff.size}) found in GZV channel!"
  end
  
  def test2562_speclimits_TEST_2S_R_3basechannels_archived_study_offline
    $log.warn '* * * * * * * * * * * * * * * * * * * * * '
    $log.warn 'Verify the settings accordingly to SIL'
    $log.warn "spaceupload.twosigmachart.channel.state.ignored = #{@@spaceupload_twosigmachart_channel_state_ignored.join(',')}"
    $log.warn '* * * * * * * * * * * * * * * * * * * * * '
    $log.warn ' -->>>'
    $log.info 'Please copy the base channels for each parameter two times so that there are 4 channels/parameter: 2S, Archived, Study and Offline. Press Enter to continue.'
    gets
    lot = @@lot
    parameters = ['QA_PARAM_800_ECP', 'QA_PARAM_801_ECP', 'QA_PARAM_802_ECP', 'QA_PARAM_803_ECP', 'QA_PARAM_804_ECP']
    twosigma_parameters = parameters.collect {|p| @@twosigma_prefix + p}
    
    folder = $lds_setup.folder(@@autocreated_qa)
    mpd = @@mpd_gzv
    mtool = @@mtool
    eqp = @@eqp
    technology = 'TEST'
    pchambers = {'PM1'=>5, 'PM2'=>6, 'PM3'=>7, 'PM4'=>8, 'PM5'=>9, 'PM6'=>10}
    newlimits = {'MEAN_VALUE_UCL'=>50.0, 'MEAN_VALUE_LCL'=>40.0}
    res = nil
    expected_e10_state = 'SDT.4PMQ'

    #E10 state change
    ['2NDP','4PMQ'].each {|s| $sv.eqp_status_change_req(eqp, s)}
    ['2NDP','4PMQ'].each {|s| $sv.chamber_status_change_req(eqp, pchambers.keys, s)}
    #The state change needs some seconds, maybe
    sleep 10
    
    res = _create_2S_channels(folder, parameters, lot: lot, mpd: mpd, mtool: mtool, eqp: eqp, technology: technology, pchambers: pchambers, newlimits: newlimits, state: 'nostatechange', multichannel: true)
    # verify 2S@ spc channels are created in AutoCreated_QA
    ch_not_exist = []
    twosigma_parameters.each {|p|
      ch = folder.spc_channel(parameter: p)
      ch_not_exist << p if ch == nil
    }
    if ch_not_exist.size > 0
      $log.warn "Channels #{ch_not_exist} not found in SetupLDS/#{@@autocreated_qa}. Please create it out of the exclude list as described in TC header and then press some key to continue"
      gets
      #send data to the newly created channels
      res = _create_2S_channels(folder, parameters, lot: lot, mpd: mpd, mtool: mtool, eqp: eqp, technology: technology, pchambers: pchambers, newlimits: newlimits, state: 'nostatechange', multichannel: true)
    end
    
    _verify_runids(res, expected_e10_state: expected_e10_state)
    
    if @@spaceupload_twosigmachart_channel_state_ignored.include?('Study')
      $log.info 'Please verify that 2S charting was done, Offline and Study channel received new samples and that the 2S control limits are calculated based on the Offline-Channel limits, then press P for Pass or a reason for Fail.'
    else
      $log.info 'Please verify that 2S charting was done, Offline and Study channel received new samples and that the 2S control limits are calculated based on the Study-Channel limits, then press P for Pass or a reason for Fail.'
    end
    res = gets
    assert res.downcase.start_with?('p'), "Failed because: #{res}"
  end
  
  def test2563_speclimits_TEST_AllUp_No2SCharting_3basechannels_archived_study_offline
    $log.warn '* * * * * * * * * * * * * * * * * * * * * '
    $log.warn 'Verify the settings accordingly to SIL'
    $log.warn "spaceupload.twosigmachart.channel.state.ignored = #{@@spaceupload_twosigmachart_channel_state_ignored.join(',')}"
    $log.warn '* * * * * * * * * * * * * * * * * * * * * '
    $log.warn ' -->>>'
    $log.info 'Please copy the base channels for each parameter two times so that there are 4 channels/parameter: 2S, Archived, Study and Offline. Press Enter to continue.'
    gets
    lot = @@lot
    parameters = ['QA_PARAM_800_ECP', 'QA_PARAM_801_ECP', 'QA_PARAM_802_ECP', 'QA_PARAM_803_ECP', 'QA_PARAM_804_ECP']
    twosigma_parameters = parameters.collect {|p| @@twosigma_prefix + p}
    
    folder = $lds_setup.folder(@@autocreated_qa)
    mpd = @@mpd_gzv
    mtool = @@mtool
    eqp = @@eqp
    technology = 'TEST'
    pchambers = {'PM1'=>5, 'PM2'=>6, 'PM3'=>7, 'PM4'=>8, 'PM5'=>9, 'PM6'=>10}
    newlimits = {'MEAN_VALUE_UCL'=>50.0, 'MEAN_VALUE_LCL'=>40.0}
    res = nil
    expected_e10_state = 'SBY.2WPR'
  
    #E10 state change
    ['2NDP','2WPR'].each {|s| $sv.eqp_status_change_req(eqp, s)}
    ['2NDP','2WPR'].each {|s| $sv.chamber_status_change_req(eqp, pchambers.keys, s)}
    #The state change needs some seconds, maybe
    sleep 10
    
    res = _create_2S_channels(folder, parameters, lot: lot, mpd: mpd, mtool: mtool, eqp: eqp, technology: technology, pchambers: pchambers, newlimits: newlimits, state: 'nostatechange')
    # verify 2S@ spc channels are created in AutoCreated_QA
    ch_not_exist = []
    twosigma_parameters.each {|p|
      ch = folder.spc_channel(parameter: p)
      ch_not_exist << p if ch == nil
    }
    if ch_not_exist.size > 0
      $log.warn "Channels #{ch_not_exist} not found in SetupLDS/#{@@autocreated_qa}. Please create it out of the exclude list as described in TC header and then press some key to continue"
      gets
      #send data to the newly created channels
      res = _create_2S_channels(folder, parameters, lot: lot, mpd: mpd, mtool: mtool, eqp: eqp, technology: technology, pchambers: pchambers, newlimits: newlimits, state: 'nostatechange')
    end
    
    _verify_runids(res, expected_e10_state: expected_e10_state)
    $log.info 'Please verify that NO 2S charting was done, but Offline and Study channel received new samples, then press P for Pass or a reason for Fail.'
    res = gets
    assert res.downcase.start_with?('p'), "Failed because: #{res}"
  end
  

  def _create_2S_channels(folder, parameters, params={})
    lot = (params[:lot] or @@lot)
    mpd = (params[:mpd] or @@mpd_gzv)
    mtool = (params[:mtool] or @@mtool)
    eqp = (params[:eqp] or @@eqp)
    technology = (params[:technology] or 'TEST')
    pchambers = (params[:pchambers] or {'PM1'=>5, 'PM2'=>6, 'PM3'=>7, 'PM4'=>8, 'PM5'=>9, 'PM6'=>10})
    newlimits = (params[:newlimits] or nil)
    state = (params[:state] or 'Offline')
    multichannel = (params[:multichannel] or false)
    
    unless state == 'nostatechange'
      # set channels state
      $log.info "set base channel(s) state to #{state}"
      parameters.each {|p|
        ch = folder.spc_channel(parameter: p)
        if ch 
          ch.set_state(state)
        else
          $log.warn "  channel for parameter #{p} missing in #{folder.inspect}, state #{state} not set, try again or set channels/states manually"
        end
      }
    end
    assert_equal 0, $sv.lot_cleanup(lot) # ???
    # build and submit DCR for process PD
    dcrp = create_process_dcr(ppd: @@ppd, eqp: eqp, lot: lot, wafer_values: @@wafer_values, chambers: pchambers, technology: technology)
    resp = $client.send_dcr(dcrp, silent: true, export: "log/#{caller_locations(1,1)[0].label}_p.xml")
    assert $spctest.verify_dcr_accepted(resp, 0)
    ##resp = submit_process_dcr(@@ppd, eqp: eqp, lot: lot, wafer_values: @@wafer_values, chambers: pchambers, return_value: 'res', export: "log/#{caller_locations(1,1)[0].label}_p.xml", technology: technology)
    #
    # build and submit DCR with all parameters, technology TEST
    dcr = Space::DCR.new(eqp: mtool, lot: lot, op: mpd, technology: technology)
    dcr.add_parameters(parameters, @@wafer_valuesooc)
    resm = $client.send_dcr(dcr, export: "log/#{caller_locations(1,1)[0].label}_m.xml")
    assert $spctest.verify_dcr_accepted(resm, nil), 'DCR not submitted successfully'
    if newlimits != nil
      parameters.each {|p|
        ch = folder.spc_channel(parameter: p)
        if ch
          limits = ch.limits
          limits['MEAN_VALUE_UCL'] = newlimits['MEAN_VALUE_UCL']
          limits['MEAN_VALUE_LCL'] = newlimits['MEAN_VALUE_LCL']
          ch.set_limits(limits)
          $log.info "channel limits will be set to (parameter #{p}): #{limits.inspect}"
          limits = ch.limits
          $log.info "channel limits now set to (parameter #{p}): #{limits.inspect}"
        else
          $log.warn "  channel for parameter #{p} missing in #{folder.inspect}, new limits #{newlimits.inspect} not set"
        end
      }
    end
    resm = $client.send_dcr(dcr, export: "log/#{caller_locations(1,1)[0].label}_m.xml")
    assert $spctest.verify_dcr_accepted(resm, nil), 'DCR not submitted successfully'
    return resp
  end
  
  def _verify_runids(res, params={})
    expected_e10_state = params[:expected_e10_state] || 'SDT.4PMQ'
    expected_chamber_e10_state = params[:expected_chamber_e10_state] || expected_e10_state
    expected_eqp_e10_state = params[:expected_eqp_e10_state] || expected_e10_state
    multichamberstates = !!params[:multichamberstates]
    runids = $spctest.runids(res)
    #if runids != false
    runids.each {|rid| 
      assert pal = $sildb.processingarealookup(rid), "no processingarealookup entry found for runid #{rid}"
      $log.info "processingarealookup #{pal}"
      assert run = $sildb.run(rid), "no run entry found for runid #{rid}"
      $log.info "run #{run}"
      if multichamberstates
        pal.each_with_index {|p, i|
          if i == 0
            assert_equal(p.e10, expected_chamber_e10_state, "E10 state in processingarealookup table #{p.e10} not as expected: #{expected_chamber_e10_state}")
          else
            assert_equal(p.e10, expected_eqp_e10_state, "E10 state in processingarealookup table #{p.e10} not as expected: #{expected_eqp_e10_state} for #{p.processingarea}")
          end
        }
      else
        pal.each {|p| assert_equal(p.e10, expected_chamber_e10_state, "E10 state in processingarealookup table #{p.e10} not as expected: #{expected_chamber_e10_state}")}
      end
      run.each {|r| assert_equal(r.e10, expected_eqp_e10_state, "E10 state in run table #{r.e10} not as expected: #{expected_eqp_e10_state}")}
    }
    #end
  end
  
end
