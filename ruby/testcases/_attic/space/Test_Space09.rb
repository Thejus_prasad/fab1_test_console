=begin 
Test Space/SIL.

(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.
    
Author: Steffen Steidten, 2011-11-25
=end

require_relative 'Test_Space_Setup'

# Test Space/SIL.

class Test_Space09 < Test_Space_Setup
	@@radius_lower_limit = 10000
	@@radius_upper_limit = 70000
	@@thickness_limit = 0.8
  @@productgroups = ($env == 'f8stag') ? ['QAPG1'] : ['QAPG2', 'QAPG3', 'QAPG4', 'QAPG5', 'QAPG6']
  @@mtools = ($env == 'f8stag') ? ['UTF001'] : ['UTFSIL01', 'UTFSIL02', 'UTFSIL03', 'UTFSIL04', 'UTFSIL05']
	@@eqps = ($env == 'f8stag') ? ['UTC001'] : ['UTCSIL01', 'UTCSIL02', 'UTCSIL03', 'UTCSIL04', 'UTCSIL05']
  @@mpd_qa = 'QA-MB-FTDEPM-LIS-CAEX.01'


  def setup
    super
    return unless $lds && self.class.class_variable_defined?(:@@lot) # bypass test0000_setup
    assert_equal 0, $sv.eqp_cleanup(@@eqp)
    assert_equal 0, $sv.lot_cleanup(@@lot)
    #
    $lds.all_spc_channels('QA_PARAM_90*').each {|ch| assert ch.delete}
    folder = $lds.folder(@@autocreated_qa)
    assert folder.delete_channels, 'error cleaning channels' if folder
  end


  def test0900_setup
    calc_expected = [
      SpaceAPI::CalculatedParam.new('WaferBIAS', 'QA-BIAS_QAParam914-QAParam913', '#mean', 'QA_PARAM_914', ['QA_PARAM_913']),
      SpaceAPI::CalculatedParam.new('SiteDifference', 'QA_PARAM_911-QA_PARAM_910', '@T-@R1;TriggerSpec', 'QA_PARAM_911', ['QA_PARAM_910']),
      SpaceAPI::CalculatedParam.new('CALCThickness', 'QA_CALCThickness', "Limit=#{@@thickness_limit}", 'QA_PARAM_930', ['QA_PARAM_931']),
      SpaceAPI::CalculatedParam.new('GOFRadius', 'QA_GOFRadius', "LowerLimit=#{@@radius_lower_limit};UpperLimit=#{@@radius_upper_limit}", 'QA_PARAM_932', [])
    ]
    
    $setup_ok = false
    #Check for existing calculated parameters
    folder = $lds.folder('AutoCreated_QACALC')
    calcs = folder.calculated_parameters
    calc_expected.each do |c|
      cdef = calcs.find {|calc| calc.name == c.name}
      assert_equal c, cdef, 'Calculated parameter not found'
    end
		
    assert @@ppd_qa = $sildb.pd_reference(mpd: @@mpd_qa).first, "no PD reference found for #{@@mpd_qa}"
    
    $setup_ok = true
  end

  def test0901_mean
    dep = 'QACALC'
    folder = $lds.folder("AutoCreated_#{dep}")
    parameters = ['QA_PARAM_910', 'QA_PARAM_911', 'QA_PARAM_912', 'QA_PARAM_913', 'QA_PARAM_914']
		
    submit_process_dcr(@@ppd_qa, :eqp=>@@eqps[0], :parameters=>parameters, :export=>"log/#{__method__}_p.xml")
    #
    # current channel sizes
    old_chs = parameters.collect {|p|
      ch = folder.spc_channel(:parameter=>p) if folder
      ch ? ch.samples.size : 0
    }
    ch = folder.spc_channel(:name=>'-/32NM/ProdGroup*') if folder
    old_chs << (ch ? ch.samples.size : 0)
    ch = folder.spc_channel(:parameter=>'QA-BIAS_QAParam914-QAParam913') if folder
    old_chs << (ch ? ch.samples.size : 0)
    ch = folder.spc_channel(:parameter=>'QA_PARAM_911-QA_PARAM_910') if folder
    old_chs << (ch ? ch.samples.size : 0)
    #
    # build and submit DCR
    dcr = Space::DCR.new(:eqp=>@@mtools[0], :lot=>@@lot, :productgroup=>@@productgroups[0], :department=>dep, :op=>@@mpd_qa)
    dcr.add_parameter('QA_PARAM_910', @@wafer_values)
    wafer_values_p2 = Hash[*@@wafer_values.collect {|w,v| [w, v+12]}.flatten]
    dcr.add_parameter('QA_PARAM_911', wafer_values_p2)
    wafer_values_p3 = Hash[*wafer_values_p2.collect {|w,v| [w, v+12]}.flatten]
    dcr.add_parameter('QA_PARAM_912', wafer_values_p3)
    wafer_values_p4 = Hash[*wafer_values_p3.collect {|w,v| [w, v+12]}.flatten]
    dcr.add_parameter('QA_PARAM_913', wafer_values_p4)
    wafer_values_p5 = Hash[*wafer_values_p4.collect {|w,v| [w, v+12]}.flatten]
    dcr.add_parameter('QA_PARAM_914', wafer_values_p5)
    res = $client.send_dcr(dcr, :export=>"log/#{__method__}.xml")
    assert $spctest.verify_dcr_accepted(res, (parameters.size+2) * @@wafer_values.size), 'DCR not submitted successfully'
    #
    # verify sample extractor keys
    parameters.each_with_index {|p,i|
      ch = folder.spc_channel(:parameter=>p)
      $log.info "Check for parameter: #{p}, channel: #{ch.inspect}"
      samples = ch.samples
      assert_equal old_chs[i]+@@wafer_values.size, samples.size, "wrong number of samples in #{ch.inspect}"
    }
    ch = folder.spc_channel(:name=>'-/32NM/ProdGroup*')
    assert_equal old_chs[-3]+@@wafer_values.size, ch.samples.size, 'wrong number of samples in -/32NM/ProdGroup*'
    ch = folder.spc_channel(:parameter=>'QA-BIAS_QAParam914-QAParam913')
    assert_equal old_chs[-2]+@@wafer_values.size, ch.samples.size, 'wrong number of samples in QA-BIAS_..'
    samples = ch.samples( :tstart=>$client.txtime )
    samples.each do |s|
      assert_equal s.statistics.mean, 12.0, 'mean value should be 12'
    end
    
    ch = folder.spc_channel(:parameter=>'QA_PARAM_911-QA_PARAM_910')
    assert_equal old_chs[-1]+@@wafer_values.size, ch.samples.size, 'wrong number of samples in QA_PARAM_911-QA_PARAM_910...'
    samples = ch.samples( :tstart=>$client.txtime )
    samples.each do |s|
      s.raw_values.each do |v|
        assert_equal v, 12.0, 'site specific value should be 12'
      end
    end
    #
    # build and submit some more DCRs with different tools end product groups for Hans-Juergen Mahlig to test Stargate
		if $env == 'itdc'
			j = @@productgroups.size - 1
			j.times {|j|
				i = @@eqps.size - 1
				i.times {|i|
					submit_process_dcr(@@ppd_qa, :eqp=>@@eqps[i+1], :productgroup=>@@productgroups[j+1], :parameters=>parameters, :export=>"log/#{__method__}_p.xml")
					dcr = Space::DCR.new(:eqp=>@@mtools[i+1], :lot=>@@lot, :productgroup=>@@productgroups[j+1], :department=>dep, :op=>@@mpd_qa)
					dcr.add_parameter('QA_PARAM_910', @@wafer_values)
					wafer_values_p2 = Hash[*@@wafer_values.collect {|w,v| [w, v+12]}.flatten]
					dcr.add_parameter('QA_PARAM_911', wafer_values_p2)
					wafer_values_p3 = Hash[*wafer_values_p2.collect {|w,v| [w, v+12]}.flatten]
					dcr.add_parameter('QA_PARAM_912', wafer_values_p3)
					wafer_values_p4 = Hash[*wafer_values_p3.collect {|w,v| [w, v+12]}.flatten]
					dcr.add_parameter('QA_PARAM_913', wafer_values_p4)
					wafer_values_p5 = Hash[*wafer_values_p4.collect {|w,v| [w, v+12]}.flatten]
					dcr.add_parameter('QA_PARAM_914', wafer_values_p5)
					$log.info "submit another DCR with mtool: #{@@mtools[i+1]}, ptool: #{@@eqps[i+1]} and productgroup: #{@@productgroups[j+1]} for Hans-Juergen Mahlig to test Stargate"
					res = $client.send_dcr(dcr, :export=>"log/#{__method__}.xml")
				}
			}
		end
  end
  
  def test0902_sites
    dep = 'QACALC'
    folder = $lds.folder("AutoCreated_#{dep}")
    parameters = ['QA_PARAM_910', 'QA_PARAM_911', 'QA_PARAM_912', 'QA_PARAM_913', 'QA_PARAM_914']
		
    submit_process_dcr(@@ppd_qa, :eqp=>@@eqps[0], :parameters=>parameters, :export=>"log/#{__method__}_p.xml")
    #
    # current channel sizes
    old_chs = parameters.collect {|p|
      ch = folder.spc_channel(:parameter=>p)
      ch ? ch.samples.size : 0
    }
    ch = folder.spc_channel(:name=>'-/32NM/ProdGroup*')
    old_chs << (ch ? ch.samples.size : 0)
    ch = folder.spc_channel(:parameter=>'QA_PARAM_911-QA_PARAM_910')
    old_chs << (ch ? ch.samples.size : 0)
    #
    # build and submit DCR
    
    wafer_values = {}
    @@wafer_values.each_key {|w| wafer_values[w] = [41,42,43,44,45,46] }
    
    dcr = Space::DCR.new(:eqp=>@@mtools[0], :lot=>@@lot, :productgroup=>@@productgroups[0], :department=>dep, :op=>@@mpd_qa)
    dcr.add_parameter('QA_PARAM_910', wafer_values, :sites=>true)
    dcr.add_parameter('QA_PARAM_911', wafer_values, :sites=>true)
    dcr.add_parameter('QA_PARAM_912', wafer_values, :sites=>true)
    dcr.add_parameter('QA_PARAM_913', wafer_values, :sites=>true)
    dcr.add_parameter('QA_PARAM_914', wafer_values, :sites=>true)
    
    res = $client.send_dcr(dcr, :export=>"log/#{__method__}.xml")

    assert $spctest.verify_dcr_accepted(res, (parameters.size+2) * wafer_values.size), 'DCR not submitted successfully'
    #
    # verify sample extractor keys
    parameters.each_with_index {|p,i|
      ch = folder.spc_channel(:parameter=>p)
      samples = ch.samples
      assert_equal old_chs[i]+wafer_values.size, samples.size, "wrong number of samples in #{ch.inspect}"
    }
    ch = folder.spc_channel(:name=>'-/32NM/ProdGroup*')
    assert_equal old_chs[-2]+wafer_values.size, ch.samples.size, 'wrong number of samples in -/32NM/ProdGroup*'
    
    ch = folder.spc_channel(:parameter=>'QA_PARAM_911-QA_PARAM_910')
    assert_equal old_chs[-1]+wafer_values.size, ch.samples.size, 'wrong number of samples in QA_PARAM_911-QA_PARAM_910...'
    samples = ch.samples( :tstart=>$client.txtime )
    samples.each do |s|
      # check the spec limits from trigger step
      assert $spctest.verify_sample_speclimits(s, 25,45,75), 'wrong spec limits'
      assert $spctest.verify_dcr_speclimits(res, {'QA_PARAM_911-QA_PARAM_910'=> [25.0,45.0,75.0]})
      s.raw_values.each do |v|
				$log.info "Value: #{v}"
        assert_equal v, 0.0, 'site specific value should be 0.0'
      end
    end
    #
    # build and submit some more DCRs with different tools end product groups for Hans-Juergen Mahlig to test Stargate
		if $env == 'itdc'
			j = @@productgroups.size - 1
			j.times {|j|
				i = @@eqps.size - 1
				i.times {|i|
					submit_process_dcr(@@ppd_qa, :eqp=>@@eqps[i+1], :productgroup=>@@productgroups[j+1], :parameters=>parameters, :export=>"log/#{__method__}_p.xml")
					dcr = Space::DCR.new(:eqp=>@@mtools[i+1], :lot=>@@lot, :productgroup=>@@productgroups[j+1], :department=>dep, :op=>@@mpd_qa)
					dcr.add_parameter('QA_PARAM_910', wafer_values, :sites=>true)
					dcr.add_parameter('QA_PARAM_911', wafer_values, :sites=>true)
					dcr.add_parameter('QA_PARAM_912', wafer_values, :sites=>true)
					dcr.add_parameter('QA_PARAM_913', wafer_values, :sites=>true)
					dcr.add_parameter('QA_PARAM_914', wafer_values, :sites=>true)
					$log.info "submit another DCR with mtool: #{@@mtools[i+1]}, ptool: #{@@eqps[i+1]} and productgroup: #{@@productgroups[j+1]} for Hans-Juergen Mahlig to test Stargate"
					res = $client.send_dcr(dcr, :export=>"log/#{__method__}.xml")
				}
			}
		end
  end
  
  def test0903_gof
    dep = 'QACALC'
		sites = 9
    folder = $lds.folder("AutoCreated_#{dep}")
    parameters = ['QA_PARAM_930', 'QA_PARAM_931', 'QA_PARAM_932', 'QA_PARAM_933', 'QA_PARAM_934']
		wafer_values = @@wafer_values.clone
		wafer_values.each_pair {|k,v| wafer_values[k] = sites.times.collect{v}}
    #
    # current channel sizes
    old_chs = parameters.collect {|p|
      ch = folder.spc_channel(:parameter=>p)
      ch ? ch.samples.size : 0
    }    
    ch = folder.spc_channel(:parameter=>'QA_CALCThickness')
    old_chs << (ch ? ch.samples.size : 0)
    ch = folder.spc_channel(:parameter=>'QA_GOFRadius')
    old_chs << (ch ? ch.samples.size : 0)
    #
    # build and submit DCR
    gof_values = {'2502J493KO'=>sites.times.collect{0.5}, '2502J494KO'=>sites.times.collect{0.8}, '2502J495KO'=>sites.times.collect{0.95}, 
				'2502J496KO'=>sites.times.collect{0.79}, '2502J497KO'=>sites.times.collect{0.83}, '2502J498KO'=>sites.times.collect{0.99}}
    
    dcr = Space::DCR.new(:eqp=>@@eqp, :lot=>@@lot, :productgroup=>'QAPG1', :department=>dep)
    dcr.add_parameter(parameters[0], gof_values, :sites=>true)
    dcr.add_parameters(parameters[1..4], wafer_values, :sites=>true)
    
    res = $client.send_dcr(dcr, :export=>"log/#{__method__}.xml")
  
    assert $spctest.verify_dcr_accepted(res, parameters.size * wafer_values.size + 9), 'DCR not submitted successfully'
    #
    # verify sample extractor keys
    parameters.each_with_index {|p,i|
      ch = folder.spc_channel(:parameter=>p)
      samples = ch.samples
      assert_equal old_chs[i]+wafer_values.size, samples.size, "wrong number of samples in #{ch.inspect}"
    }
    
    ch = folder.spc_channel(:parameter=>'QA_CALCThickness')
    assert_equal old_chs[-2]+3, ch.samples.size, 'wrong number of samples in QA_CALCThickness...'
    samples = ch.samples( :tstart=>$client.txtime )
    samples.each do |s|
      # check the spec limits from trigger step      
      s.raw_values.each do |v|
        w = s.extractor_keys['Wafer']
        assert gof_values[w][0] > 0.8, "site specific value should be filtered out: #{w}, #{wafer_values[w]}"
      end
    end
    
    ch = folder.spc_channel(:parameter=>'QA_GOFRadius')
    assert_equal old_chs[-1]+6, ch.samples.size, 'wrong number of samples in QA_GOFRadius...'
    samples = ch.samples( :tstart=>$client.txtime )
    samples.each_with_index do |s, i|
      $log.info 'check the radius filter functionality from trigger step' if i == 0
			s.raw_value_data_keys.each {|dk| 
				radius = ((dk['X'].to_f**2) + (dk['Y'].to_f**2))**(0.5)
				$log.info("Radius: #{radius}")
				assert (radius >= @@radius_lower_limit and radius <= @@radius_upper_limit), "radius #{radius} is not within filter limits 
					(#{@@radius_lower_limit}..#{@@radius_upper_limit})"
			}
		end
		$log.info '  the correct raw values passed the radius filter'
  end
end
