=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: U. Koerner, 2012-01-11

History:
  2014-02-05 ssteidte,  separated Test_Space07, code cleanup
=end

require_relative 'Test_Space_Setup'


# Test Space/SIL.
class Test_Space072 < Test_Space_Setup
  @@chamber_a = 'PM1'
  @@chamber_b = 'PM2'
  @@chamber_c = 'CHX'
  @@chambers = {@@chamber_a=>5, @@chamber_b=>6, @@chamber_c=>7}
  @@assign_manual_ca = true
    
  def setup
    super
    return unless $lds && self.class.class_variable_defined?(:@@lot) # bypass test0000_setup
    assert_equal 0, $sv.eqp_cleanup(@@eqp)
    assert_equal 0, $sv.lot_cleanup(@@lot)
    assert_equal 0, $sv.inhibit_cancel(:route, $sv.lot_info(@@lot).route)
    #
    $lds.all_spc_channels('QA_PARAM_90*').each {|ch| assert ch.delete}
    folder = $lds.folder(@@autocreated_qa)
    assert folder.delete_channels, "error cleaning channels" if folder
  end

  def test0700_setup
    $setup_ok = false
    #
    assert @@ppd_qa = $sildb.pd_reference(mpd: @@mpd_qa).first, "no PD reference found for #{@@mpd_qa}"
    #
    ['LotHold', 'AutoLotHold', 'ReleaseLotHold', 'CancelLotHold',
      'EquipmentInhibit', 'AutoEquipmentInhibit', 'ReleaseEquipmentInhibit', 'ReleaseEquipmentInhibit-noDefault',
      'ChamberInhibit', 'AutoChamberInhibit', 'ReleaseChamberInhibit', 'ReleaseChamberInhibit-noDefault',
      'ReticleInhibit', 'AutoReticleInhibit', 'ReleaseReticleInhibit',
      'RouteInhibit', 'AutoRouteInhibit', 'ReleaseRouteInhibit',
      'MachineRecipeInhibit', 'AutoMachineRecipeInhibit', 'ReleaseMachineRecipeInhibit', 'ReleaseMachineRecipeInhibit-noDefault',
      'AutoSetWaferScriptParameter-OOC', 'AutoSetLotScriptParameter-OOC', 'AutoSetLotScriptParameter-OOCInteger', 'SetLotScriptParameter-OOC',
      'AutoUnknown', 'Unknown', 'CACollection-ReleaseAll',      
      'AutoChamberAndRecipeInhibit-Multi',
      'ReleaseEquipmentAndRecipeInhibit', 'ReleaseEquipmentAndRecipeInhibit-noDeflt', 'CACollection-ReleaseAllRecipeInhibits'
    ].each {|action|
      assert $spc.corrective_action(action), "CA #{action.inspect} not defined"
    }
    #
    # resubmit of the corresponding chamber filter file
    if ['xxitdc', 'let'].member?($env)
      (human_workflow_action(@@importantChambersTable, 'CANCEL', :user=>@@approvers[0]); sleep 30) if $sfc.wait_for_approve(@@importantChambersTable, :user=>@@approvers[0], :timeout=>1, :sleeptime=>1)
      (human_workflow_action(@@importantChambersTable, 'CANCEL', :user=>@@approvers[0]); sleep 30) if $sfc.wait_for_edit_content(@@importantChambersTable, :user=>@@approvers[0], :timeout=>1, :sleeptime=>1)
      (human_workflow_action(@@importantChambersTable, 'CANCEL'); sleep 30) if $sfc.wait_for_edit_content(@@importantChambersTable, :timeout=>1, :sleeptime=>1)
      edit_submit_approve(@@importantChambersTable, @@approvers[0])
    end
    #
    $setup_ok = true
  end

  def test0720_chamber_inhibit
    chambers = {@@chamber_a=>5, @@chamber_b=>6}
    channels = create_clean_channels(chambers: chambers)
    assert $spctest.assign_verify_cas(channels,
      ['ChamberInhibit', 'AutoChamberInhibit', 'ReleaseChamberInhibit', 'ReleaseChamberInhibit-noDefault'])
    #
    # send process DCR
    dcrp = submit_process_dcr(@@ppd_qa, wafer_values: @@wafer_values2, chambers: chambers)
    # send DCR with OOC values
    dcr = Space::DCR.new(lot: @@lot, eqp: @@eqp, op: @@mpd_qa)
		dcr.add_parameters(@@parameters, @@wafer_values2ooc)
    res = $client.send_dcr(dcr, export: :caller)
    assert $spctest.verify_dcr_accepted(res, @@parameters.size*@@wafer_values2ooc.size, nooc: 'all*2'), "DCR not submitted successfully"
    #
    assert $spctest.verify_inhibit(@@eqp, chambers.count*channels.size, class: :eqp_chamber, details: true, dcr: dcrp, lot: @@lot, spc_run: res, cj: dcr.controljob), "chamber must have an inhibit"
    assert $spctest.verify_ecomment(channels, "TOOL+CHAMBER_HELD: #{@@eqp}/#{@@chamber_a}: reason={X-S")
    #
    # release inhibit with noDefault attribute
    if @@assign_manual_ca
      channels.each {|ch|
        s = ch.samples(ckc: true)[-1]
        assert !s.assign_ca($spc.corrective_action("ReleaseChamberInhibit-noDefault")), "wrongly assigned corective action"
      }
      assert $spctest.verify_inhibit(@@eqp, channels.size*chambers.size, class: :eqp_chamber), "chamber must still have all inhibits"
      #
      # release inhibit with noDefault attribute - with comment
      channels.each {|ch|
        s = ch.samples(ckc: true)[-1]
        assert s.assign_ca($spc.corrective_action("ReleaseChamberInhibit-noDefault"), comment: "[Release=All]"), "error assigning corrective action"
      }
      assert $spctest.verify_inhibit(@@eqp, 0, class: :eqp_chamber), "chamber must not have an inhibit"
      #
      submit_process_dcr(@@ppd_qa, wafer_values: @@wafer_values2, chambers: chambers)
      #
      assert_equal 0, $sv.lot_cleanup(@@lot)
      # send DCR with OOC values
      ##wafer_values = Hash[*@@wafer_values2.collect {|w,v| [w,v+100]}.flatten]
      dcr = Space::DCR.new(lot: @@lot, eqp: @@eqp, op: @@mpd_qa)
      dcr.add_parameters(@@parameters, @@wafer_values2ooc)
      res = $client.send_dcr(dcr, export: :caller)
      assert $spctest.verify_dcr_accepted(res, @@parameters.size*@@wafer_values2ooc.size, nooc: 'all*2'), "DCR not submitted successfully"
      #
      assert $spctest.verify_inhibit(@@eqp, chambers.count*channels.size, class: :eqp_chamber), "chamber must have an inhibit"
      assert $spctest.verify_ecomment(channels, "TOOL+CHAMBER_HELD: #{@@eqp}/#{@@chamber_a}: reason={X-S")
      #
      # release inhibit
      channels.each {|ch|
        s = ch.samples(ckc: true)[-1]
        assert s.assign_ca($spc.corrective_action("ReleaseChamberInhibit")), "error assigning corrective action"
      }
      assert $spctest.verify_inhibit(@@eqp, 0, class: :eqp_chamber), "chamber must not have an inhibit"
      #
      # set inhibit (manual)
      s = channels[0].samples(ckc: true)[-1]
      assert s.assign_ca($spc.corrective_action("ChamberInhibit")), "error assigning corective action"
      assert $spctest.verify_inhibit(@@eqp, chambers.count, class: :eqp_chamber), "chamber must have an inhibit"
      assert $spctest.verify_ecomment(channels, "TOOL+CHAMBER_HELD: #{@@eqp}/#{@@chamber_a}: reason={X-S")
      #
      # release inhibit
      assert s.assign_ca($spc.corrective_action("ReleaseChamberInhibit")), "error assigning corrective action"
      assert $spctest.verify_inhibit(@@eqp, 0, class: :eqp_chamber), "chamber must not have an inhibit"
      #
      # set inhibit again (manual)
      assert s.assign_ca($spc.corrective_action("ChamberInhibit")), "error assigning corective action"
      assert $spctest.verify_inhibit(@@eqp, chambers.count, class: :eqp_chamber), "chamber must have an inhibit"
      assert $spctest.verify_ecomment(channels, "TOOL+CHAMBER_HELD: #{@@eqp}/#{@@chamber_a}: reason={X-S")
      #
      # release inhibit with noDefault attribute
      assert !s.assign_ca($spc.corrective_action("ReleaseChamberInhibit-noDefault")), "wrongly assigned corective action"
      assert $spctest.verify_inhibit(@@eqp, chambers.count, class: :eqp_chamber), "chambers must still have an inhibit"
    end
  end

  def test0722_one_chamber_not_existing
    chambers = {@@chamber_a=>5, @@chamber_c=>7}
    channels = create_clean_channels(chambers: chambers)
    assert $spctest.assign_verify_cas(channels, "AutoChamberInhibit")
    #
    submit_process_dcr(@@ppd_qa, wafer_values: @@wafer_values2, chambers: chambers)
    #
    dcr = Space::DCR.new(lot: @@lot, eqp: @@eqp, op: @@mpd_qa)
    dcr.add_parameters(@@parameters, @@wafer_values2ooc)
    res = $client.send_dcr(dcr, export: :caller)
    #
    assert $spctest.verify_dcr_accepted(res, @@parameters.size*@@wafer_values2ooc.size, nooc: 'all*2'), "DCR not submitted successfully"
    assert $spctest.verify_inhibit(@@eqp, 1), "equipment must have an inhibit"
    assert $spctest.verify_ecomment(channels, "TOOL+CHAMBER_HELD: #{@@eqp}/#{@@chamber_a}: reason={X-S")
		assert $spctest.verify_ecomment(channels, "Execution of [AutoChamberInhibit] failed: reason={Error in SiView call caused by")
    assert $spctest.verify_ecomment(channels, "TOOL_HELD: #{@@eqp}: reason={X-S")
  end

  def test0723_some_chambers_not_existing
    chambers = @@chambers
    #chambers = {"PM2"=>5, "PM1"=>6, "A123456789"=>7, "B123456789"=>8, "C123456789"=>9, "D123456789"=>10, "E123456789"=>11, "F123456789"=>12, "G123456789"=>13}
    #chambers = {"A123456789"=>7, "B123456789"=>8, "C123456789"=>9, "D123456789"=>10, "E123456789"=>11, "F123456789"=>12, "G123456789"=>13}
    #chambers = {"X123456789"=>7, "B123456789"=>8, "C123456789"=>9, "D123456789"=>10, "E123456789"=>11, "F123456789"=>12, "G123456789"=>13}
    #chambers = {"A9"=>7, "Z1"=>8, "B8"=>9, "Y2"=>10, "C7"=>11, "X3"=>12, "D6"=>13, "W4"=>14, "E5"=>15, "V5"=>16, "F4"=>17, "U6"=>18, "G3"=>19, "T7"=>20, "H2"=>21, "S8"=>22, "I1"=>23, "R9"=>24}
    channels = create_clean_channels(chambers: chambers)
    assert $spctest.assign_verify_cas(channels, "AutoChamberInhibit")
    #
    ## wafer_values = Hash[*@@wafer_values2.collect {|w,v| [w,v+100]}.flatten]
    dcr = Space::DCR.new(lot: @@lot, eqp: @@eqp, op: @@mpd_qa, chambers: chambers)
    dcr.add_parameters(@@parameters, @@wafer_values2ooc)
    res = $client.send_dcr(dcr, export: :caller)
    #
    assert $spctest.verify_dcr_accepted(res, @@parameters.size*@@wafer_values2ooc.size, nooc: 'all*2'), "DCR not submitted successfully"
    assert $spctest.verify_inhibit(@@eqp, 1), "equipment must have an inhibit"
    assert $spctest.verify_inhibit(@@eqp, (chambers.count-1)*channels.size, {class: :eqp_chamber}), "chamber must have an inhibit"
    assert $spctest.verify_ecomment(channels, "TOOL+CHAMBER_HELD: #{@@eqp}/#{@@chamber_a}: reason={X-S")
		assert $spctest.verify_ecomment(channels, "TOOL+CHAMBER_HELD: #{@@eqp}/#{@@chamber_b}: reason={X-S")
		assert $spctest.verify_ecomment(channels, "Execution of [AutoChamberInhibit] failed: reason={Error in SiView call caused by")
		assert $spctest.verify_ecomment(channels, "TOOL_HELD: #{@@eqp}: reason={X-S")
  end

  def test0724_one_not_existing_chamber_and_also_lot_hold
    chambers = @@chambers
    channels = create_clean_channels(chambers: chambers)
    assert $spctest.assign_verify_cas(channels, ['AutoLotHold', 'AutoChamberInhibit'])
    #
    dcr = Space::DCR.new(lot: @@lot, eqp: @@eqp, op: @@mpd_qa, chambers: chambers)
    dcr.add_parameters(@@parameters, @@wafer_values2ooc)
    res = $client.send_dcr(dcr, export: :caller)
    #
    assert $spctest.verify_dcr_accepted(res, @@parameters.size*@@wafer_values2ooc.size, nooc: 'all*2'), "DCR not submitted successfully"
    assert $spctest.verify_futurehold2(@@lot, "[(Raw above specification)"), "lot must have a future hold"
    assert $spctest.verify_inhibit(@@eqp, (chambers.count-1)*channels.size, class: :eqp_chamber), "chamber must have an inhibit"
    assert $spctest.verify_ecomment(channels, "LOT_HELD: #{@@lot}: reason={X-")
		assert $spctest.verify_ecomment(channels, "TOOL+CHAMBER_HELD: #{@@eqp}/#{@@chamber_a}: reason={X-S")
		assert $spctest.verify_ecomment(channels, "TOOL+CHAMBER_HELD: #{@@eqp}/#{@@chamber_b}: reason={X-S")
		assert $spctest.verify_ecomment(channels, "Execution of [AutoChamberInhibit] failed: reason={Error in SiView call caused by")
		assert $spctest.verify_ecomment(channels, "TOOL_HELD: #{@@eqp}: reason={X-S")
  end

  def test0725_some_chambers_not_existing_and_also_lot_hold
    chambers = @@chambers
    channels = create_clean_channels(chambers: chambers)
    assert $spctest.assign_verify_cas(channels, ['AutoLotHold', 'AutoChamberInhibit'])
    #
    # send ooc values
    dcr = Space::DCR.new(lot: @@lot, eqp: @@eqp, op: @@mpd_qa, chambers: chambers)
    dcr.add_parameters(@@parameters, @@wafer_values2ooc)
    res = $client.send_dcr(dcr, export: :caller)
    #
    assert $spctest.verify_dcr_accepted(res, @@parameters.size*@@wafer_values2ooc.size, nooc: 'all*2'), "DCR not submitted successfully"
    assert $spctest.verify_futurehold2(@@lot, "[(Raw above specification)"), "lot must have a future hold"
    assert $spctest.verify_inhibit(@@eqp, (chambers.count-1)*channels.size, class: :eqp_chamber), "chamber must have an inhibit"
    assert $spctest.verify_ecomment(channels, "LOT_HELD: #{@@lot}: reason={X-S")
    assert $spctest.verify_ecomment(channels, "TOOL+CHAMBER_HELD: #{@@eqp}/PM1: reason={X-S")
  end

  def test0726_exclude_list
    # MSR 546359
    chambers = {"PM1"=>5, "PM2"=>6, "PM3"=>7}
    channels = create_clean_channels(chambers: chambers)
    # exclude list is as follows: ExcludeChamber=PM1, ExcludeChamber=PM2_Side1, ExcludeChamber=PM2_Side2
    assert $spctest.assign_verify_cas(channels, ['AutoChamberInhibit-Exclude', 'ReleaseChamberInhibit'])
    #
    # send ooc values
    dcr = Space::DCR.new(lot: @@lot, eqp: @@eqp, op: @@mpd_qa, chambers: chambers)
    dcr.add_parameters(@@parameters, @@wafer_values2ooc)
    res = $client.send_dcr(dcr, export: :caller)
    #
    assert $spctest.verify_dcr_accepted(res, @@parameters.size*@@wafer_values2ooc.size, nooc: 'all*2'), "DCR not submitted successfully"
    assert $spctest.verify_inhibit(@@eqp, (chambers.count-1)*@@parameters.size, class: :eqp_chamber), "chamber must have an inhibit"
    assert $spctest.verify_ecomment(channels, "TOOL+CHAMBER_HELD: #{@@eqp}/PM")
    #
    $log.info "lot has #{(chambers.count-1)*@@parameters.size} chamber inhibits, external comments set"
    # release inhibit
    if @@assign_manual_ca
      channels.each {|ch|
        s = ch.samples(ckc: true)[-1]
        assert s.assign_ca($spc.corrective_action("ReleaseChamberInhibit")), "error assigning corrective action"
      }
      assert $spctest.verify_inhibit(@@eqp, 0, class: :eqp_chamber), "chamber must not have an inhibit"
    end
  end

  def test0727_chamber_inhibit_parameter_extension_lot
    pa_vehaviour = "cycle_wafer"
    #pa_vehaviour = "cycle_parameter"
    parameters = @@parameters.collect {|p| p + "_LOT"}
    chambers = {"PM1"=>5, "PM2"=>6, "PM3"=>7}
    expected_samples = parameters.size * chambers.size if pa_vehaviour == "cycle_wafer"
    wafer_values = Hash[@@wafer_values2.collect {|w,v| [w,v+20]}]

    channels = create_clean_channels(chambers: chambers, parameters: parameters, nsamples: 5, wafer_values: wafer_values)
    assert $spctest.assign_verify_cas(channels, ['ChamberInhibit', 'AutoChamberInhibit', 'ReleaseChamberInhibit'])
    #
    submit_process_dcr(@@ppd_qa, wafer_values: wafer_values, chambers: chambers, processing_areas: pa_vehaviour)
    #
    # send DCR with OOC values
    dcr = Space::DCR.new(lot: @@lot, eqp: @@mtool, op: @@mpd_qa)
		dcr.add_parameters(parameters, @@wafer_values2ooc)
    res = $client.send_dcr(dcr, export: :caller)
    assert $spctest.verify_dcr_accepted(res, expected_samples, nooc: expected_samples*2), "DCR not submitted successfully"
    #
    assert $spctest.verify_inhibit(@@eqp, chambers.count*channels.size, class: :eqp_chamber), "chamber must have an inhibit"
    assert $spctest.verify_ecomment(channels, "TOOL+CHAMBER_HELD: #{@@eqp}")
    #
    if @@assign_manual_ca
      # release inhibit (each chamber/sample)
      channels.each {|ch|
        ch.samples(ckc: true).each {|s|
          if s.violations.size > 0
            assert s.assign_ca($spc.corrective_action("ReleaseChamberInhibit")), "error assigning corrective action"
          end
        }
      }
      assert $spctest.verify_inhibit(@@eqp, 0, class: :eqp_chamber), "chamber must not have an inhibit"
      #
      # set inhibit (manual)
      channels[0].samples(ckc: true).each {|s|
        next unless s.violations.size > 0
        assert s.assign_ca($spc.corrective_action('ChamberInhibit')), "error assigning corective action"
        chambers = s.extractor_keys['PChamber'].split(':')
        assert $spctest.verify_inhibit(@@eqp, chambers.count, class: :eqp_chamber), "chamber must have an inhibit"
        assert $spctest.verify_ecomment(channels[0], "TOOL+CHAMBER_HELD: #{@@eqp}")
        # release inhibit
        assert s.assign_ca($spc.corrective_action("ReleaseChamberInhibit")), "error assigning corrective action"
        assert $spctest.verify_inhibit(@@eqp, 0, class: :eqp_chamber), "chamber must not have an inhibit"
      }    
    end
  end

  def test0728_include_list
    # MSR 546359
    chambers = {"PM1"=>5}
    allchambers = ["PM1", "PM2", "PM3"]
    channels = create_clean_channels(chambers: chambers)
    assert $spctest.assign_verify_cas(channels, ['AutoChamberInhibit-Include', 'ReleaseChamberInhibit'])
    #
    # send ooc values
    dcr = Space::DCR.new(lot: @@lot, eqp: @@eqp, op: @@mpd_qa, chambers: chambers)
    dcr.add_parameters(@@parameters, @@wafer_values2ooc)
    res = $client.send_dcr(dcr, export: :caller)
    #
    assert $spctest.verify_dcr_accepted(res, @@parameters.size*@@wafer_values2ooc.size, nooc: 'all*2'), "DCR not submitted successfully"
		sleep 100
    assert $spctest.verify_inhibit(@@eqp, allchambers.count*@@parameters.size, class: :eqp_chamber), "chamber must have an inhibit"
    allchambers.each {|cha| assert $spctest.verify_ecomment(channels, "TOOL+CHAMBER_HELD: #{@@eqp}/#{cha}")}
    #
    # release inhibit
    if @@assign_manual_ca
      channels.each {|ch|
        s = ch.samples(ckc: true)[-1]
        assert s.assign_ca($spc.corrective_action('ReleaseChamberInhibit')), "error assigning corrective action"
      }
      $spctest.verify_inhibit(@@eqp, 0, class: :eqp_chamber)
    end
  end

  def test0729_exclude_include_list
    # MSR 546359
    chambers = {"PM1"=>5, "PM2"=>6, "PM3"=>7}
    channels = create_clean_channels(chambers: chambers)
    # exclude list is as follows: ExcludeChamber=PM1, ExcludeChamber=PM2_Side1, ExcludeChamber=PM2_Side2
    assert $spctest.assign_verify_cas(channels,
      ['AutoChamberInhibit-ExcludeInclude', 'ChamberInhibit-ExcludeInclude', 'ReleaseChamberInhibit'])
    #
    # send ooc values
    dcr = Space::DCR.new(lot: @@lot, eqp: @@eqp, op: @@mpd_qa, chambers: chambers)
    dcr.add_parameters(@@parameters, @@wafer_values2ooc)
    res = $client.send_dcr(dcr, export: :caller)
    #
    assert $spctest.verify_dcr_accepted(res, @@parameters.size*@@wafer_values2ooc.size, nooc: 'all*2'), "DCR not submitted successfully"
    assert $spctest.verify_inhibit(@@eqp, chambers.count*@@parameters.size, class: :eqp_chamber), "chamber must have an inhibit"
    chambers.keys.each {|cha| assert $spctest.verify_ecomment(channels, "TOOL+CHAMBER_HELD: #{@@eqp}/#{cha}")}
    #
    # release inhibit
    if @@assign_manual_ca
      channels.each {|ch|
        s = ch.samples(ckc: true)[-1]
        assert s.assign_ca($spc.corrective_action('ReleaseChamberInhibit')), "error assigning corrective action"
      }
      $spctest.verify_inhibit(@@eqp, 0, class: :eqp_chamber)
      # set inhibit (manual)
      channels[0].samples(ckc: true).each {|s|
        next unless s.violations.size > 0
        assert s.assign_ca($spc.corrective_action('ChamberInhibit-ExcludeInclude')), "error assigning corective action"
        # 1 inhibit for PM1
        assert $spctest.verify_inhibit(@@eqp, 1, class: :eqp_chamber), "chamber must have an inhibit"
        assert $spctest.verify_ecomment(channels[0], "TOOL+CHAMBER_HELD: #{@@eqp}")
        # release inhibit
        assert s.assign_ca($spc.corrective_action("ReleaseChamberInhibit")), "error assigning corrective action"
        assert $spctest.verify_inhibit(@@eqp, 0, class: :eqp_chamber), "chamber must not have an inhibit"
      }
    end
  end
  
end
