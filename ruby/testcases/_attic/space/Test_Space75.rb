=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: D. Steger, 2013-02-20
=end

require_relative 'Test_Space_Setup'


# Test Space/SIL CA for Alarm limits
class Test_Space75 < Test_Space_Setup
  @@mpd_qa = 'QA-ALARMLMTS.01'

  def setup
    super
    return unless $lds && self.class.class_variable_defined?(:@@lot) # bypass test0000_setup
    assert_equal 0, $sv.eqp_cleanup(@@eqp)
    assert_equal 0, $sv.lot_cleanup(@@lot)
    assert_equal 0, $sv.inhibit_cancel(:route, $sv.lot_info(@@lot).route)
    #
    [$lds, $lds_setup].each {|lds|
      lds.all_spc_channels('QA_PARAM_90*').each {|ch| assert ch.delete}
      folder = lds.folder(@@autocreated_qa)
      assert folder.delete_channels, 'error cleaning channels' if folder
    }
  end


  def test075000_setup
    $setup_ok = false
    #
    ['AutoLotHold', 'ReleaseLotHold'].each {|action| assert $spc.corrective_action(action), "CA #{action.inspect} not defined"}
    assert @@ppd_qa = $sildb.pd_reference(mpd: @@mpd_qa).first, "no PD reference found for #{@@mpd_qa}"
    #
    $setup_ok = true
  end

  def test075001_alarmlimits_ooc_inline
    wafer_values = Hash[@@wafer_values2.collect {|w,v| [w,v+7]}]
    dcr = send_alarmlimit_dcr(wafer_values: wafer_values)
    if @@autoqual_reporting_inline_enabled == true
      assert $spctest.verify_aqv_message(dcr: dcr, lot: @@lot, wafer: wafer_values.keys.sort, result: 'fail', comment: 'Found 5 OOS/OOC violation(s)'), 
        'AQV message not as expected'
    end
  end

  def test075002_alarmlimits_ooc_testlds
    wafer_values = Hash[@@wafer_values2.collect {|w,v| [w,v+7]}]
    dcr = send_alarmlimit_dcr(wafer_values: wafer_values, technology: 'TEST')
    if @@autoqual_reporting_setup_enabled == true
      assert $spctest.verify_aqv_message(dcr: dcr, lot: @@lot, wafer: wafer_values.keys.sort, result: 'fail', comment: 'Found 10 OOS/OOC violation(s)'), 
        'AQV message not as expected'
    end
  end

  def test075003_alarmlimits_nooc_testlds
    wafer_values = Hash[@@wafer_values2.collect {|w,v| [w,v+7]}]
    dcr = send_alarmlimit_dcr(wafer_values: wafer_values, technology: 'TEST', limits: {'RAW_UAL'=>55.0, 'MEAN_VALUE_UAL'=>54.0}, nooc: 0)
    if @@autoqual_reporting_setup_enabled == true
      assert $spctest.verify_aqv_message(dcr: dcr, lot: @@lot, wafer: wafer_values.keys.sort, result: 'success'), 
        'AQV message not as expected'
    end
  end
  
  def send_alarmlimit_dcr(params={})
    limits = params[:limits] || {'RAW_UAL'=>55.0, 'MEAN_VALUE_UAL'=>50.0}
    technology = params[:technology] || '32NM'
    calist = ['AutoLotHold', 'ReleaseLotHold']
    ooc = params[:ooc] || true
    wafer_values = params[:wafer_values] || Hash[@@wafer_values2.collect {|w,v| [w,v+7]}]
    nooc = params[:nooc] || @@parameters.count*2
    #
    channels = create_clean_channels(technology: technology)
    #vals = Space::SpcApi::Channel.all_valuations.select {|v| v.vid <= 5 || (v.vid >= 45 && v.vid <= 48)}
    vals = Space::SpcApi::Channel.all_valuations.select {|v| v.vid >= 45 && v.vid <= 48}
    assert vals.size > 0, 'no eligible valuations '
    channels.each {|c|
      c.set_limits(limits)
      c.set_valuations(vals)
    }
    # assign CAs after the controls have been set!
    assert $spctest.assign_verify_cas(channels, calist)
    #
    submit_process_dcr(@@ppd_qa, technology: technology, export: "log/#{caller_locations(1,1)[0].label}_p.xml")
    #
    # send DCR with OOC values
    dcr = Space::DCR.new(lot: @@lot, eqp: @@mtool, op: @@mpd_qa, technology: technology)
    dcr.add_parameters(@@parameters, wafer_values)
    res = $client.send_dcr(dcr, export: "log/#{caller_locations(1,1)[0].label}.xml")
    assert $spctest.verify_dcr_accepted(res, @@parameters.count*wafer_values.size, nooc: nooc)
    #
    if nooc > 0
      # verify futurehold from AutoLotHold
      assert $spctest.verify_futurehold2(@@lot, 'QA_PARAM_9', dcr_lot: dcr.find_lot_context(@@lot)), 'lot must have a future hold'
      # verify comment is set
      assert $spctest.verify_ecomment(channels, "LOT_HELD: #{@@lot}: reason={X-S")
      
      assert_equal 0, $sv.lot_gatepass(@@lot), 'SiView GatePass error'
      #
      assert $spctest.verify_futurehold_cancel(@@lot, nil), 'lot must have no future hold'
      assert $sv.lot_hold_list(@@lot, type: 'FutureHold').size > 0, 'lot must be on hold'
    end
    #
    return dcr
  end
end