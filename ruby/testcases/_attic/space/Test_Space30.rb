=begin 
Test Space/SIL.

(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.
    
Author: Daniel Steger, 2012-07-31
=end

require_relative 'Test_Space_Setup'


# Stargate channels
class Test_Space30 < Test_Space_Setup
  Dept = 'QASG'

  
  def test3001_send_data
    ##cleanup_autocreated_channels(Dept)
    folder = $lds.folder("AutoCreated_#{Dept}")
    assert folder.delete_channels, 'error cleaning channels'

    queue = Queue.new
    n = 10#30 #number of all DCRS
    nthreads = 1#5 #number of threads
    p = 10 #number of parameters
    r = 10 #number of raw values
    target = 45
    ooc = 15
    oos = 30#10

    technology = '14BK'
    _parameter = 'QA_SGPARAM_'
    pd = 'QA-MB-FTDEP.01'
    pd2 = 'QA-MB-FTDEPM.01'
    eqp = 'UTCSIL01'

    #
   
    (1..n).each do |i|
      if i.odd? # pre-build process DCR
        wafer_values = Hash[*[25.times.collect {|j| waferid='WAFERID%04d' % [j]; [waferid, j]}]]
        $log.info "#{wafer_values.inspect}"
        dcr = Space::DCR.new(:technology=>technology, :department=>Dept, :eqp=>"#{eqp}_#{i}", :op=>pd, :eqptype=>'PROCESS-CHAMBER')
        dcr.add_parameter('DUMMY', wafer_values, :nocharting=>true)
      else # pre-build metro DCR 
        dcr = Space::DCR.new(:technology=>technology, :department=>Dept, :productgroup=>"QAPG#{(i/2)%10}", :op=>pd2)
        p.times.collect{|j| 
          param = "#{_parameter}%03d" %(100+j)        
          raw_values = r.times.collect {|k| ((i+j+k) % oos) == 0 ? 100 : target + 7.5 * 0.rand_gauss}
          wafer_values = Hash[*[25.times.collect {|k| 
             offset = ((i+j+k) % ooc) == 0 ? 100 : (5.0*rand - 2.5)           
             waferid='WAFERID%04d' % [k]; [waferid, raw_values.map {|i| i + offset}]
           }]]
          dcr.add_parameter(param, wafer_values, :nocharting=>false)
        }
      end
      queue << dcr
    end

    #
    # send DCRs
    test_ok = true
    $threads = nthreads.times.collect {|i|
      Thread.new {
        client = Space::Client.new($env, :timeout=>900)
        #sleep i
        
        while !queue.empty? do
          dcr = queue.pop
          res = client.send_dcr(dcr, :export=>"log/dcr_#{Time.now.to_i}.xml")
          if res
            runID = res[:submitRunResponse][:submitRunReturn][:runID]
            ok = (runID != '0' || runID != '-1')
          else
            ok = false
          end
          $log.warn "t#{i+1}-#{dcr.inspect}: #{res.inspect}" unless ok
          test_ok &= ok
          sleep 1
        end
      }
    }
    $threads.each {|t| t.join}
  end
  
end


class Numeric
  # normal distribution (using Box-Mueller Transform); returns N(0,1) distributed value
  @@iset = 0
  def rand_gauss    
    v = 1.0
    if @@iset == 0
      while (v >= 1.0 || v == 0.0)
        u1 = 2.0*rand-1.0
        u2 = 2.0*rand-1.0
        v = u1**2 + u2**2
      end    
      fac = Math.sqrt(-2.0*Math.log(v)/v)
      @@gset = u1*fac
      @@iset = 1
      return u2*fac
    else
      @@iset = 0
      return @@gset
    end
  end
end


