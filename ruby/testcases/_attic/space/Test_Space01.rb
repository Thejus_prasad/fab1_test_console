=begin 
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.
    
Author: Steffen Steidten, 2011-11-25
=end

require_relative 'Test_Space_Setup'


# Test Space/SIL.
class Test_Space01 < Test_Space_Setup
  @@ibeqp = 'UTI001'
  @@sampledata_module_lookup_enabled = false
  @@derdack_missingparametersoff = false
  @@derdack_missingspeclimitsoff = true


  def setup
    super
    return unless $lds && self.class.class_variable_defined?(:@@lot) # bypass test0000_setup
    assert_equal 0, $sv.eqp_cleanup(@@eqp)
    assert_equal 0, $sv.lot_cleanup(@@lot)
    #
    $lds.all_spc_channels('QA_PARAM_90*').each {|ch| assert ch.delete}
    [@@autocreated_nonwip, @@autocreated_nodep, @@autocreated_qa, @@module_qa].each {|f|
      [$lds, $lds_setup].each {|lds|
        folder = lds.folder(f)
        folder.delete_channels if folder
      }
    }
  end


  def test0100_setup
    $setup_ok = false
    #
    if $env == "f8stag"
      assert $spctest.verify_spec(specid: "M14-00020001", specversion: 6, all_active: true), "Setup wrong, spec not found!"
    end
    #
    props = $spctest.silserver.read_jobprops
    # need props[:'parameters.missingspeclimit.upload.inline.enabled']
    # need props[:'parameters.missingspeclimit.upload.setup.enabled']
    #
    $setup_ok = true
  end
  
  def test0101_default_template # merged with 0102_resend
    $setup_ok = false
    assert_equal 0, $sv.eqp_cleanup(@@eqp)
    assert_equal 0, $sv.lot_cleanup(@@lot)
    #
    # build and submit DCR
    dcr = Space::DCR.new(eqp: @@eqp, eqptype: 'PROCESS-CHAMBER', lot: @@lot, department: 'NODEP')
    dcr.add_parameters(@@parameters, @@wafer_values)
    assert $spctest.send_dcr_verify(dcr, nsamples: @@parameters.size * @@wafer_values.size), "DCR not submitted successfully"
    #
    # verify autocharting folder
    assert folder = $lds.folder(@@autocreated_nodep), "Autocharting folder has not been created"
    # verify spc channels
    sleep 10
    @@parameters.each {|p|
      chs = folder.spc_channels(parameter: p)
      assert_equal 1, chs.size, "wrong number of channels for parameter #{p} in folder #{folder.name}"
      ref_ch = Space::SpcApi::ChannelData.new(nil, nil, "Auto-created using template: #{@@default_template}", p)
      assert $spctest.verify_channel(chs.first, ref_ch, dcr, @@wafer_values), "wrong channel data for #{p}"
    }
    #
    # resend DCR and verify spc channels
    assert_equal 0, $sv.eqp_cleanup(@@eqp)
    assert_equal 0, $sv.lot_cleanup(@@lot)
    #
    assert $spctest.send_dcr_verify(dcr, nsamples: @@parameters.size * @@wafer_values.size), "DCR not submitted successfully"
    @@parameters.each {|p|
      ch = folder.spc_channels(parameter: p)[0]
      ref_ch = Space::SpcApi::ChannelData.new(nil, nil, "Auto-created using template: #{@@default_template}", p)
      assert $spctest.verify_channel(ch, ref_ch, dcr, @@wafer_values, nsamples: 2, last_sample_only: true), "wrong channel data"
    }
    #
    $setup_ok = true
  end

  def test0103_channel_move
    $setup_ok = false
    #
    # clean up Module QA folder
    tfolder = $lds.folder(@@module_qa) || $lds.create_folder(@@module_qa)
    assert tfolder.delete_channels, "error deleting channels in #{tfolder.name}"
    # create channels
    dcr = Space::DCR.new(eqp: @@eqp, eqptype: 'PROCESS-CHAMBER', lot: @@lot, department: 'NODEP')
    dcr.add_parameters(@@parameters, @@wafer_values)
    assert $spctest.send_dcr_verify(dcr, nsamples: @@parameters.size * @@wafer_values.size), "DCR not submitted successfully"
    #
    # move channels
    folder = $lds.folder(@@autocreated_nodep)
    chs = folder.spc_channels
    assert_equal 5, chs.size, "wrong number of channels in #{folder.name}"
    chs.each {|ch| $spc.move_spc_channel(ch, tfolder)}
    assert_equal 5, tfolder.spc_channels.size, "wrong number of channels in #{tfolder.name}"
    #
    $setup_ok = true
  end

  def test0104_resend_to_moved_channels
    # build and submit DCR
    assert_equal 0, $sv.eqp_cleanup(@@eqp)
    assert_equal 0, $sv.lot_cleanup(@@lot)
    #
    dcr = Space::DCR.new(eqp: @@eqp, eqptype: 'PROCESS-CHAMBER', lot: @@lot, department: 'NODEP')
    dcr.add_parameters(@@parameters, @@wafer_values)
    assert $spctest.send_dcr_verify(dcr, nsamples: @@parameters.size * @@wafer_values.size), "DCR not submitted successfully"
    # move channels
    tfolder = $lds.folder(@@module_qa) || $lds.create_folder(@@module_qa)
    folder = $lds.folder(@@autocreated_nodep)
    folder.spc_channels.each {|ch| $spc.move_spc_channel(ch, tfolder)}
    assert_equal 5, tfolder.spc_channels.size, "wrong number of channels in #{tfolder.name}"
    #
    # resend DCR
    assert_equal 0, $sv.eqp_cleanup(@@eqp)
    assert_equal 0, $sv.lot_cleanup(@@lot)
    #
    assert $spctest.send_dcr_verify(dcr, nsamples: @@parameters.size * @@wafer_values.size), "DCR not submitted successfully"
    #
    # verify there are no new spc channels in @@autocreated_nodep
    assert_empty folder.spc_channels, "no channels must be created in #{@@autocreated_nodep}"
    @@parameters.each {|p|
      ch = tfolder.spc_channel(parameter: p)
      ref_ch = Space::SpcApi::ChannelData.new(nil, nil, "Auto-created using template: #{@@default_template}", p)
      ## 3 samples after manual move, one sample afer automated move (with sample deletetion)
      assert $spctest.verify_channel(ch, ref_ch, dcr, @@wafer_values, nsamples: 1), "wrong channel data"
    }
  end
  
  def test0106_default_template_internal_buffer
    $setup_ok = false
    #
    # build and submit DCR
    dcr = Space::DCR.new(eqp: @@ibeqp, eqptype: 'PROCESS-CHAMBER', lot: @@lot, department: 'NODEP')
    dcr.add_parameters(@@parameters, @@wafer_values)
    assert $spctest.send_dcr_verify(dcr, nsamples: @@parameters.size * @@wafer_values.size), "DCR not submitted successfully"
    #
    # verify autocharting folder
    assert folder = $lds.folder(@@autocreated_nodep), "Autocharting folder has not been created"
    # verify spc channels
    @@parameters.each {|p|
      chs = folder.spc_channels(parameter: p)
      assert_equal 1, chs.size, "wrong number of channels for parameter #{p} in folder #{folder.name}"
      ref_ch = Space::SpcApi::ChannelData.new(nil, nil, "Auto-created using template: #{@@default_template}", p)
      assert $spctest.verify_channel(chs[0], ref_ch, dcr, @@wafer_values), "wrong channel data"
    }
    #
    $setup_ok = true
  end

  def test0110_autocharting_module
    parameters = {"QA_AUTOCHART_900"=>"QA", "QA_AUTOCHART_901"=>"LIT", 
      "QA_AUTOCHART_902"=>"MTR", "QA_AUTOCHART_903"=>"ETC", "QA_AUTOCHART_904"=>"CFM"}
    $lds.all_spc_channels('QA_AUTOCHART_90*').each {|ch| assert ch.delete}
    #
    # build and submit DCR
    dcr = Space::DCR.new(eqp: @@eqp, eqptype: 'PROCESS-CHAMBER', lot: @@lot, department: 'NODEP', op: "QA_AUTOCHARTING.01")
    dcr.add_parameters(parameters.keys, @@wafer_values)
    assert $spctest.send_dcr_verify(dcr, nsamples: parameters.size * @@wafer_values.size), "DCR not submitted successfully"
    #
    # verify spc channels    
    channels = $lds.all_spc_channels('*QA_AUTOCHART_90*')
    assert_equal parameters.count, channels.count, "There should be as many parameters as channels been created"
    channels.each {|ch|
      p = ch.parameter
      dpt = @@sampledata_module_lookup_enabled ? parameters[p] : 'NODEP'
      assert_equal "AutoCreated_#{dpt}", ch.channel.parent.config.name, "Channel not created in correct folder"
      templ = @@sampledata_module_lookup_enabled ? "_Template_QA_AUTOCHART_#{dpt}" : "_Template_QA_PARAMS_900"
      ref_ch = Space::SpcApi::ChannelData.new(nil, nil, "Auto-created using template: #{templ}", p)
      assert $spctest.verify_channel(ch, ref_ch, dcr, @@wafer_values), "wrong channel data"
    }
  end

  def test0111_autocharting_module_default_template
    return if $env == "let"
    parameters = {"QA_AUTOCHART_900F"=>"QA", "QA_AUTOCHART_901F"=>"LIT", 
      "QA_AUTOCHART_902F"=>"MTR", "QA_AUTOCHART_903F"=>"ETC", "QA_AUTOCHART_904F"=>"CFM"}
    # ensure module template exist
    assert $spctest.templates_exist($lds.folder(@@templates_folder), ["DEFAULT_QA_TEMPLATE"]), "missing template"
    $lds.all_spc_channels('QA_AUTOCHART_90*').each {|ch| assert ch.delete}
    #
    # build and submit DCR
    dcr = Space::DCR.new(eqp: @@eqp, eqptype: 'PROCESS-CHAMBER', lot: @@lot, department: 'NODEP', op: "QA_AUTOCHARTING.01")
    dcr.add_parameters(parameters.keys, @@wafer_values)
    assert $spctest.send_dcr_verify(dcr, nsamples: parameters.size * @@wafer_values.size), "DCR not submitted successfully"
    #
    # verify spc channels
    channels = $lds.all_spc_channels("*QA_AUTOCHART_90*")
    assert_equal parameters.count, channels.count, "There should be as many parameters as channels been created"
    channels.each {|ch|
      p = ch.parameter
      dpt = @@sampledata_module_lookup_enabled ? parameters[p] : 'NODEP'
      assert_equal "AutoCreated_#{dpt}", ch.channel.parent.config.name, "Channel not created in correct folder"
      templatename = dpt =~ /NODEP/  ? @@default_template : "DEFAULT_#{dpt}_TEMPLATE"
      ref_ch = Space::SpcApi::ChannelData.new(nil, nil, "Auto-created using template: #{templatename}", p)
      assert $spctest.verify_channel(ch, ref_ch, dcr, @@wafer_values), "wrong channel data"
    }
  end

  def test01201_autocharting_noupload_inline
    parameters = ['QA_PARAM_900NO', 'QA_PARAM_901NO', 'QA_PARAM_902NO', 'QA_PARAM_903NO', 'QA_PARAM_904NO']
    #
    dcr = Space::DCR.new(lot: @@lot)
    # verify the parameters have no spec limits
    dcrl = dcr.context[:JobSetup][:SiViewControlJob][:FOUP][0][:Lot][0]
    qq = {active: 'Y', pd: dcrl['operationID'], product: dcrl['productID'], productgroup: dcrl['productGroupID'], route: dcrl['route']}
    parameters.each {|p| assert_empty $spctest.sildb.speclimit(qq.merge(parametername: p))}
    # build and submit DCR
    dcr.add_parameters(parameters, @@wafer_values)
    res = $client.send_dcr(dcr, export: :caller)
    if @@parameters_missingspeclimit_upload_inline_enabled
      assert $spctest.verify_dcr_accepted(res, parameters.size*@@wafer_values.size), "DCR not submitted successfully"
      if @@datacheck_missingspeclimits_inline_enabled
        if @@derdack_missingspeclimitsoff == false
          $log.warn "MissingSpecLimits Check: no notification found." if $spctest.verify_notification("Error code : NO_SPEC_LIMITS") == false
        else
          $log.warn "MissingSpecLimits Check: unexpected notification(s) found." if $spctest.verify_notification("Error code : NO_SPEC_LIMITS") == true
        end
        if @@derdack_missingparametersoff == false
          $log.warn "MissingParameters Check: no notification found." if $spctest.verify_notification("Error code : MISSING_DCR_PARAMETERS") == false
        else
          $log.warn "MissingParameters Check: unexpected notification(s) found." if $spctest.verify_notification("Error code : MISSING_DCR_PARAMETERS") == true
        end
      end
    else
      assert $spctest.verify_dcr_accepted(res, 0), "No parameters should be uploaded"
			assert !$spctest.verify_notification("Error code : NO_SPEC_LIMITS"), "no notification expected."
    end
  end

  def test01202_autocharting_noupload_testlds
    parameters = ['QA_PARAM_900NO', 'QA_PARAM_901NO', 'QA_PARAM_902NO', 'QA_PARAM_903NO', 'QA_PARAM_904NO']
    #
    dcr = Space::DCR.new(lot: @@lot, technology: 'TEST')
    # verify the parameters have no spec limits
    dcrl = dcr.context[:JobSetup][:SiViewControlJob][:FOUP][0][:Lot][0]
    qq = {active: 'Y', pd: dcrl['operationID'], product: dcrl['productID'], productgroup: dcrl['productGroupID'], route: dcrl['route']}
    parameters.each {|p| assert_empty $spctest.sildb.speclimit(qq.merge(parametername: p))}
    # build and submit DCR
    dcr.add_parameters(parameters, @@wafer_values)
    res = $client.send_dcr(dcr, export: :caller)
    if @@parameters_missingspeclimit_upload_setup_enabled
      assert $spctest.verify_dcr_accepted(res, parameters.size*@@wafer_values.size), "DCR not submitted successfully"
      if @@datacheck_missingspeclimits_setup_enabled
				$log.info "datacheck_missingspeclimits_setup_enabled: #{@@datacheck_missingspeclimits_setup_enabled}"
        if @@derdack_missingspeclimitsoff == false
          $log.warn "no notification found." if $spctest.verify_notification("Error code : NO_SPEC_LIMITS") == false
        end
      end
    else
      assert $spctest.verify_dcr_accepted(res, 0), "No parameters should be uploaded"
			assert !$spctest.verify_notification("Error code : NO_SPEC_LIMITS"), "no notification expected."
    end
  end

  def test01211_autocharting_mixed_upload_inline
    params_noupload = ["QA_PARAM_900NO", "QA_PARAM_901NO"]
    params_upload = ["QA_PARAM_902", "QA_PARAM_903", "QA_PARAM_904"]
    parameters = params_noupload + params_upload
    #
    dcr = Space::DCR.new(lot: @@lot)
    # verifiy spec limits
    dcrl = dcr.context[:JobSetup][:SiViewControlJob][:FOUP][0][:Lot][0]
    qq = {active: 'Y', pd: dcrl['operationID'], product: dcrl['productID'], productgroup: dcrl['productGroupID'], route: dcrl['route']}
    params_noupload.each {|p| assert_empty $spctest.sildb.speclimit(qq.merge(parametername: p))}
    params_upload.each {|p| assert $spctest.sildb.speclimit(qq.merge(parametername: p)).first}
    # build and submit DCR
    dcr.add_parameters(parameters, @@wafer_values)
    res = $client.send_dcr(dcr, export: :caller)
    if @@parameters_missingspeclimit_upload_inline_enabled
      assert $spctest.verify_dcr_accepted(res, parameters.size*@@wafer_values.size), "DCR not submitted successfully"
      if @@datacheck_missingspeclimits_inline_enabled
				$log.info "datacheck_missingspeclimits_inline_enabled: #{@@datacheck_missingspeclimits_inline_enabled}"
        if @@derdack_missingspeclimitsoff == false
          $log.warn "no notification found." if $spctest.verify_notification("Error code : NO_SPEC_LIMITS") == false
        end
      end
    else
      assert $spctest.verify_dcr_accepted(res, params_upload.size*@@wafer_values.size), "Some parameters should be uploaded"
      assert !$spctest.verify_notification("Error code : NO_SPEC_LIMITS"), "no notification expected."
    end
  end

  def test01212_autocharting_mixed_upload_testlds
    params_noupload = ["QA_PARAM_900NO", "QA_PARAM_901NO"]
    params_upload = ["QA_PARAM_902", "QA_PARAM_903", "QA_PARAM_904"]
    parameters = params_noupload + params_upload
    #
    dcr = Space::DCR.new(lot: @@lot, technology: 'TEST')
    # verifiy spec limits
    dcrl = dcr.context[:JobSetup][:SiViewControlJob][:FOUP][0][:Lot][0]
    qq = {active: 'Y', pd: dcrl['operationID'], product: dcrl['productID'], productgroup: dcrl['productGroupID'], route: dcrl['route']}
    params_noupload.each {|p| assert_empty $spctest.sildb.speclimit(qq.merge(parametername: p))}
    params_upload.each {|p| assert $spctest.sildb.speclimit(qq.merge(parametername: p)).first}
    # build and submit DCR
    dcr.add_parameters(parameters, @@wafer_values)
    res = $client.send_dcr(dcr, export: :caller)
    if @@parameters_missingspeclimit_upload_setup_enabled
      assert $spctest.verify_dcr_accepted(res, parameters.size*@@wafer_values.size), "DCR not submitted successfully"
      if @@datacheck_missingspeclimits_setup_enabled
				$log.info "datacheck_missingspeclimits_inline_enabled: #{@@datacheck_missingspeclimits_setup_enabled}"
        if @@derdack_missingspeclimitsoff == false
          $log.warn "no notification found." if $spctest.verify_notification("Error code : NO_SPEC_LIMITS") == false
        end
      end
    else
      assert $spctest.verify_dcr_accepted(res, params_upload.size*@@wafer_values.size), "Some parameters should be uploaded"
      assert !$spctest.verify_notification("Error code : NO_SPEC_LIMITS"), "no notification expected."
    end
  end
  
end
