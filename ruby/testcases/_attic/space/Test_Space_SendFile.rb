=begin
Test Space/SIL.

(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: Conny Stenzel, Ulrich Koerner 2012-02-17
=end

require_relative 'Test_Space_Setup'

# Test Space/SIL.

class Test_Space_SendFile < Test_Space_Setup

  @@passes = 5
  @@mq_timeout = 900

  @@template_folder = "etc/testcasedata/space/"
  @@dcr_folder = "etc/testcasedata/space/tooldcrs/"

  @@dcrs_msr528659 = [ "DCR_MDX802_110611_21_20.xml", "DCR_MDX805_110711_15_49.xml",
    "DCR_MPX610_110811_11_10.xml", "DCR_POL705_110811_11_10.xml", "DCR_POL707_110811_11_09.xml" ]
  @@dcrs_msr542638 = [ "MSR542638_p_0.xml", "MSR542638_p_1.xml", "MSR542638_p_2.xml",
    "MSR542638_p_3.xml", "MSR542638_p_4.xml", "MSR542638_m.xml" ]
  @@dcrs_msr513786 = [ "U194P.00.CDS506.xml" ]
  @@dcrs_nullpointerexception = ["RTA300.xml"]
  @@dcrs_spaceuploadexception = ["OVL4101.xml"]
  @@dcrs_ovl_nosamples = ["OVL1105.xml", "OVL300.xml"]
  @@dcrs_msr590237 = ["space_DCR_U2712.00_ETX1608_20120612_non_ooc.xml",
		"space_DCR_U2712.00_ETX1608_20120612_pm2_ooc.xml",
		"space_DCR_U2712.00_ETX1608_20120612_pm3_ooc.xml",
		"space_DCR_U2712.00_ETX1608_20120612_pm4_ooc.xml",
		"space_DCR_U2712.00_ETX1608_20120612_pm2and3_ooc.xml",
		"space_DCR_U2712.00_ETX1608_20120612_pm2and3_ooc_no-pm4.xml",
		"space_DCR_U2712.00_ETX1608_20120612_pm2and4_ooc.xml",
		"space_DCR_U2712.00_ETX1608_20120612_pm3and4_ooc.xml",
		"space_DCR_U2712.00_ETX1608_20120612_all_ooc.xml"]
	@@dcrs_active_listening = ["MES503--U25J2.00_active_vs_listening.xml"]
	@@dcrs_no_gk = ["NoGK_ALC420-08-23-50.xml", "NoGK_OVL300.xml"]
	@@dcrs_wrong_lothold = ["WrongLotHold_ALC410_space_21_20.xml", "WrongLotHold_ALC410_space_21_52.xml", "WrongLotHold_OVL200_space_22_23.xml"]
	@@dcrs_wrong_template = ["ALC301_U29GB.xml"]
	@@dcrs_gkdata_notinspace = ["DCR_01_POL203_Space_UR24199.000.xml", "DCR_02_POL203_Space_UR24199.000.xml"]
	@@dcrs_invalidparamvalue = ["OPI903_20122510_1533_DCR_invalidparametervalue.xml"]
	@@dcrs_nonwip = ["FVX1424_1.xml"]
	@@dcrs_nointeger = ["DFU121.xml"]
	@@dcrs_nodata = ["OVL300 SPACE DCR 10.Jan2013 T303110.00.xml", "OVN300.xml"]
	@@dcrs_cdsem = ["CDS1605_U1DL5.00.xml", "CDS1605_U1DL5.00_m.xml"]
	@@dcr_no_pd = ["test07010_lot_hold.xml"]
	@@dcr_mpc_timeproblem = ["2013-06-17_10-54-29.869_POL1402.xml", "2013-06-17_11-54-29.506_THK1512.xml"]
	@@dcr_no_ptool2 = ["ACS102.xml", "CVD906.xml", "SEM1521.xml"]
  @@dcr_slow = ["SEM4121_U37U3.01.xml", "SEM4121_U37U3.02.xml", "SEM4121_U37U4.02.xml", "SEM4121_U37U4.03.xml", "DFS614_UR34446.000.xml", "POL1713_UR34318.000.xml", "POL1713_UR34340.000.xml", "POL204_U38M9.00.xml", "SEM511_UR35157.000.xml", "SEM515_U33KD.00.xml"]
  @@dcr_slow2 = ["SEM112 UR36665.xml", "SEM1121 U37N8.xml", "SEM1121 UBCK46003.xml", "SEM1121 UR38032.xml", "SEM1521 U381E.xml", "SEM1521 U38K5.xml", "SEM1521 U38K5_SEM1521-20131112-0026.xml", "SEM1521 UR35239.xml", "SEM502 UR35126.xml", "SEM502 UR35263.xml"]
	@@dcr_wipnonwip = ["FVX_nonwip_wip.xml", "FVX_nonwip_wip2.xml", "OVL_nonwip_wip.xml"]
  @@dcr_err191 = ["CVD806_DCR1.xml", "CVD806_DCR2.xml"]
  @@dcr_multickc = ["multickctest_p.xml", "multickctest_m.xml"]
  @@dcr_nolimit = ["SNK920.xml", "RES910.xml"]
  @@dcr_no2Sdata = ["IMP4112_20150206.xml", "DFU1100_20150206.xml"]
  @@dcr_aqvsuccess_but_missingparam = ["RunID_145999212_ETX4920.xml", "RunID_145999215_SEM160.xml"]
  @@dcr_lotdot = ["lotdot.xml"]
  @@dcr_multilotaqvfail = ["THK4501.xml"]
  @@dcr_invalid_eqptag = ["test0710_equipment_inhibit.xml"]
  @@dcr_testlds_prerun = ["test0016_cc_testlds_prerun_nooc_statetest_offline_m.xml"]
  @@dcr_testlds_aqv_multilot = ["test1811a_config_missing_parameter_wafer_active_multilot_p.xml"]
  @@dcr_testlds_aqv_multilot_orig = ["POL706.xml"]
  @@dcr_testlds_aqv_multilot_g = ["test1811g_config_missing_parameter_wafer_active_multilot_first_lot_fail_second_lot_success_p.xml"]
  @@dcr_MSR1293012 = ["MSR1293012.xml"]

	# duration in itdc @@passes * 180s
  def xtest001_msr542638
    ergs = []
    @@passes.times{|i|
      erg = {}
      @@dcrs_msr542638.each_with_index{|fn,j|
        $log.info "--- pass #{i} --- dcr #{j} ---"
        res = send_dcr_and_measure_duration(fn)
        assert @@mq_timeout>res, "possible timeout"
        erg[fn] = res
      }
      ergs << erg
    }
    create_chart("MSR542638",ergs)
  end

  # until now always timeout in itdc and let
  def xtest002_msr528659
    ergs = []
    @@passes.times{|i|
      erg = {}
      @@dcrs_msr528659.each_with_index{|fn,j|
        $log.info "--- pass #{i} --- dcr #{j} ---"
        res = send_dcr_and_measure_duration(fn)
        assert @@mq_timeout>res, "possible timeout"
        erg[fn] = res
      }
      ergs << erg
    }
    create_chart("MSR528659",ergs)
  end

  def xtest003_msr513786
    @@dcrs_msr513786.each_with_index{|fn,j|
      $log.info "--- dcr #{j} ---"
      res = send_dcr_and_measure_duration(fn)
      assert @@mq_timeout>res, "possible timeout"
    }
  end

  def xtest004_nullpointerexception
    @@dcrs_nullpointerexception.each_with_index{|fn,j|
      $log.info "--- dcr #{j} ---"
      res = send_dcr_and_measure_duration(fn)
      assert @@mq_timeout>res, "possible timeout"
    }
  end

  def xtest005_spaceuploadexception
    @@dcrs_spaceuploadexception.each_with_index{|fn,j|
      $log.info "--- dcr #{j} ---"
      res = send_dcr_and_measure_duration(fn)
      assert @@mq_timeout>res, "possible timeout"
    }
  end

  def xtest006_spaceuploadexception
		@@dcrs_ovl_nosamples.each_with_index{|fn,j|
      $log.info "--- dcr #{j} ---"
      res = send_dcr_and_measure_duration(fn)
      assert @@mq_timeout>res, "possible timeout"
    }
  end

  def xtest007_msr590237
		@@dcrs_msr590237.each_with_index{|fn,j|
      $log.info "--- dcr #{j} ---"
			if j == 8
				res = send_dcr_and_measure_duration(fn)
				assert @@mq_timeout>res, "possible timeout"
			end
    }
  end

	def xtest008_active_listening
		@@dcrs_active_listening.each_with_index{|fn,j|
      $log.info "--- dcr #{j} ---"
			res = send_dcr_and_measure_duration(fn)
			assert @@mq_timeout>res, "possible timeout"
    }
  end

	def xtest009_no_gk
		@@dcrs_no_gk.each_with_index{|fn,j|
      $log.info "--- dcr #{j} ---"
			res = send_dcr_and_measure_duration(fn)
			assert @@mq_timeout>res, "possible timeout"
    }
  end

	def xtest010_wrong_lothold
		@@dcrs_wrong_lothold.each_with_index{|fn,j|
      $log.info "--- dcr #{j} ---"
			res = send_dcr_and_measure_duration(fn)
			assert @@mq_timeout>res, "possible timeout"
    }
  end

	def xtest011_wrong_template
		@@dcrs_wrong_template.each_with_index{|fn,j|
      $log.info "--- dcr #{j} ---"
			res = send_dcr_and_measure_duration(fn)
			assert @@mq_timeout>res, "possible timeout"
    }
  end

	def xtest012_gkdata_notinspace
		@@dcrs_gkdata_notinspace.each_with_index{|fn,j|
      $log.info "--- dcr #{j} ---"
			res = send_dcr_and_measure_duration(fn)
			assert @@mq_timeout>res, "possible timeout"
    }
  end

	def xtest013_invalidparamvalue
		@@dcrs_invalidparamvalue.each_with_index{|fn,j|
      $log.info "--- dcr #{j} ---"
			res = send_dcr_and_measure_duration(fn)
			assert @@mq_timeout>res, "possible timeout"
    }
  end

	def xtest014_nonwip
		@@dcrs_nonwip.each_with_index{|fn,j|
      $log.info "--- dcr #{j} ---"
			res = send_dcr_and_measure_duration(fn)
			assert @@mq_timeout>res, "possible timeout"
    }
  end

	def xtest015_nointeger
		@@dcrs_nointeger.each_with_index{|fn,j|
      $log.info "--- dcr #{j} ---"
			res = send_dcr_and_measure_duration(fn)
			assert @@mq_timeout>res, "possible timeout"
    }
  end

	def xtest016_nointeger
		@@dcrs_nodata.each_with_index{|fn,j|
      $log.info "--- dcr #{j} ---"
			res = send_dcr_and_measure_duration(fn)
			assert @@mq_timeout>res, "possible timeout"
    }
  end

	def xtest017_cdsem
		@@dcrs_cdsem.each_with_index{|fn,j|
      $log.info "--- dcr #{j} ---"
			res = send_dcr_and_measure_duration(fn)
			assert @@mq_timeout>res, "possible timeout"
    }
  end

	def xtest018_nopd
		@@dcr_no_pd.each_with_index{|fn,j|
      $log.info "--- dcr #{j} ---"
			res = send_dcr_and_measure_duration(fn)
			assert @@mq_timeout>res, "possible timeout"
    }
  end

	def xtest019_mpc_timeproblem
		@@dcr_mpc_timeproblem.each_with_index{|fn,j|
      $log.info "--- dcr #{j} ---"
			res = send_dcr_and_measure_duration(fn)
			assert @@mq_timeout>res, "possible timeout"
    }
  end

	def xtest020_no_ptool2
		@@dcr_no_ptool2.each_with_index{|fn,j|
      $log.info "--- dcr #{j} ---"
			res = send_dcr_and_measure_duration(fn)
			assert @@mq_timeout>res, "possible timeout"
    }
  end

	def xtest021_slow
		@@dcr_slow.each_with_index{|fn,j|
      $log.info "--- dcr #{j} ---"
			res = send_dcr_and_measure_duration(fn)
			assert @@mq_timeout>res, "possible timeout"
    }
  end

	def xtest022_slow
		@@dcr_slow2.each_with_index{|fn,j|
      $log.info "--- dcr #{j} ---"
			res = send_dcr_and_measure_duration(fn)
			assert @@mq_timeout>res, "possible timeout"
    }
  end

	def xtest023_wipnonwip
		@@dcr_wipnonwip.each_with_index{|fn,j|
      $log.info "--- dcr #{j} ---"
			res = send_dcr_and_measure_duration(fn)
			assert @@mq_timeout>res, "possible timeout"
    }
  end

	def xtest024_err191
		@@dcr_err191.each_with_index{|fn,j|
      $log.info "--- dcr #{j} ---"
			res = send_dcr_and_measure_duration(fn)
			assert @@mq_timeout>res, "possible timeout"
    }
  end

	def xtest025_multickc
		@@dcr_multickc.each_with_index{|fn,j|
      $log.info "--- dcr #{j} ---"
			res = send_dcr_and_measure_duration(fn)
			assert @@mq_timeout>res, "possible timeout"
    }
  end

	def xtest026_nolimit
		@@dcr_nolimit.each_with_index{|fn,j|
      $log.info "--- dcr #{j} ---"
			res = send_dcr_and_measure_duration(fn)
			assert @@mq_timeout>res, "possible timeout"
    }
  end

	def xtest027_no2Sdata
		@@dcr_no2Sdata.each_with_index {|fn,j|
      $log.info "--- dcr #{j} ---"
      assert_equal 0, $sv.lot_cleanup(@@lot)
      assert_equal 0, $sv.eqp_cleanup(@@eqp)
      pchambers = {"PM1"=>5, "PM2"=>6, "PM3"=>7, "PM4"=>8, "PM5"=>9, "PM6"=>10}
      ["2NDP","4PMQ"].each {|s| $sv.eqp_status_change_req(@@eqp, s)}
      ["2NDP","4PMQ"].each {|s| $sv.chamber_status_change_req(@@eqp, pchambers.keys, s)}
			res = send_dcr_and_measure_duration(fn)
			assert @@mq_timeout>res, "possible timeout"
    }
  end

  def xtest028_dcr_aqvsuccess_but_missingparam
		@@dcr_aqvsuccess_but_missingparam.each_with_index{|fn,j|
      $log.info "--- dcr #{j} ---"
			res = send_dcr_and_measure_duration(fn)
			assert @@mq_timeout>res, "possible timeout"
    }
  end

  def xtest029_dcr_lotdot
		@@dcr_lotdot.each_with_index{|fn,j|
      $log.info "--- dcr #{j} ---"
			res = send_dcr_and_measure_duration(fn)
			assert @@mq_timeout>res, "possible timeout"
    }
  end

  def xtest030_dcr_multilotaqvfail
    50.times {
      @@dcr_multilotaqvfail.each_with_index{|fn,j|
        $log.info "--- dcr #{j} ---"
        assert_equal 0, $sv.lot_cleanup('SIL04090102.000')
        res = send_dcr_and_measure_duration(fn)
        assert @@mq_timeout>res, "possible timeout"
      }
    }
  end

  def xtest031_dcr_invalid_eqptag
    @@dcr_invalid_eqptag.each_with_index{|fn,j|
      $log.info "--- dcr #{j} ---"
      assert_equal 0, $sv.lot_cleanup('SIL0411000201.000')
      res = send_dcr_and_measure_duration(fn)
      assert @@mq_timeout>res, "possible timeout"
    }
  end

  def xtest032_dcr_testlds_prerun
    @@dcr_testlds_prerun.each_with_index{|fn,j|
      $log.info "--- dcr #{j} ---"
      assert_equal 0, $sv.lot_cleanup('SIL0410002501.000')
      res = send_dcr_and_measure_duration(fn)
      assert @@mq_timeout>res, "possible timeout"
    }
  end

  def xxtest033_dcr_testlds_prerun
    @@dcr_testlds_aqv_multilot.each_with_index{|fn,j|
      $log.info "--- dcr #{j} ---"
      assert_equal 0, $sv.lot_cleanup('SIL0410002501.000')
      res = send_dcr_and_measure_duration(fn)
      assert @@mq_timeout>res, "possible timeout"
    }
  end

  def xxtest034_dcr_testlds_prerun
    @@dcr_testlds_aqv_multilot_orig.each_with_index{|fn,j|
      $log.info "--- dcr #{j} ---"
      assert_equal 0, $sv.lot_cleanup('SIL0410002501.000')
      res = send_dcr_and_measure_duration(fn)
      assert @@mq_timeout>res, "possible timeout"
    }
  end

  def xxtest035_dcr_testlds_prerun
    @@dcr_testlds_aqv_multilot_g.each_with_index{|fn,j|
      $log.info "--- dcr #{j} ---"
      assert_equal 0, $sv.lot_cleanup('SIL0412000001.000')
      assert_equal 0, $sv.lot_cleanup('SIL0412000002.000')
      res = send_dcr_and_measure_duration(fn)
      assert @@mq_timeout>res, "possible timeout"
    }
  end

  def test036_dcr_MSR1293012
    @@dcr_MSR1293012.each_with_index{|fn,j|
      $log.info "--- dcr #{j} ---"
      res = send_dcr_and_measure_duration(fn)
      assert @@mq_timeout>res, "possible timeout"
    }
  end

  # returns duration
  def send_dcr_and_measure_duration(file)
    t1 = Time.now
    $log.info "send: #{file}"
    fname = @@dcr_folder + file
    res = $client.send_receive(File.binread(@@dcr_folder + file))
    ##res = $client.send_dcr_file(fname)
    File.binwrite("log/#{file}_response.xml", $client.response)
    ##open("#{fname}_respone.xml", "wb") {|fh| fh.write $client.response}
    $log.debug "#{res.inspect}"
    $log.info "#{file} - start: #{t1.strftime("%H:%M:%S.%L")} end: #{Time.now.strftime("%H:%M:%S.%L")} difference: #{$client.responsetime} s"
    return $client.responsetime
  end

  # need to have a msr_template.xlsx-file
  # actual values in msr_yyyymmdd_hhmmss_env.xlsx
  def create_chart(msr, ergs)
    ext = ".xlsx"
    fn_in = @@template_folder+"#{msr}_template#{ext}" # "etc/testcasedata/space/MSR542638_template.xlsx"
    fn_out = "log/#{msr}_#{Time.now.strftime("%Y%m%d_%H%M%S")}_#{$env}#{ext}"
    #
    b = POI::Workbook.open(fn_in)
    s = b.worksheets[0]
    #
    ergs.each_with_index{|erg,i|
      erg.each_with_index{|e,j|
        m = e[0]
        v = e[1]
        s.rows[j+4].cells[0].set_cell_value(m)
        s.rows[j+4].cells[i+1].set_cell_value(v)
      }
    }
    #
    b.save_as(fn_out)
    $log.info "#{fn_out} - contains results and chart"
  end
end
