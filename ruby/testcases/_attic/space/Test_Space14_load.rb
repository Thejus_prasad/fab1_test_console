=begin 
Test Space/SIL.

(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.
    
Author: Steffen Steidten, 2011-11-25
=end

require_relative 'Test_Space_Setup'

# Test Space/SIL.

class Test_mySpace14 < Test_Space_Setup
  @@mpd = "QA_MB-FTDEPM.01"
  @@mtool = "QAMeas"
  @@ppd = nil
  @@remeasure_sleep = 5 #10
  @@erf_mpd = "e1234QA_MB-FTDEPM.01"
  @@erf_ppd = "e1234QA_MB-FTDEP.01"

  def test1400_setup
    $setup_ok = false

    if $env == "f8stag"
      @@erf_mpd = "e1234-QA_MB-FTDEPM-A0.01"
      @@erf_ppd = "e1234-QA_MB-FTDEP-A0.01"
    end

    @@ppd = $sildb.pd_reference(:mpd=>@@mpd).first
    assert_not_nil @@ppd, "no PD reference found for #{@@mpd}"
    $log.info "using process PD #{@@ppd}"
    
    $setup_ok = true
  end

  def test1401_remeasure_inline
    m_count = 200
    $t_meas = []

    prop = :'inline.remeasurement.autoflagging.enabled'
    ($log.info "test skipped because #{prop}=false"; return) unless @@inline_remeasurement_autoflagging_enabled
    if ["itdc"].member?($env)
      $sil = Space::SIL.new($env)
      assert_equal "true", $sil.properties.advanced[prop], "advanced SIL property #{prop} must be set"
    end
    #
    assert clean_up(:lot=>@@lot, :eqp=>@@eqp)
    parameters = @@parameters.collect {|p| p + "_REMEASURE"}
    folder = $lds.folder(@@autocreated_qa)
    folder.delete_channels if folder
    #
    submit_process_dcr(@@ppd, :eqp=>@@eqp, :lot=>@@lot)
    #
    m_count.times{|n|
      $log.info("run #{n+1}/#{m_count}")
      remeasure_scenario(nil, folder, :parameters=>parameters)
    }
    
    # save results
    ts = Time.now.to_i
    $files = [] if $files.nil?
    $t_meas.first.count.times{|i|
      fn = "t_meas_#{ts}_#{i}.yaml"
      $files << fn
      f = File.open(fn,'w'){|f| f.puts $t_meas.collect{|e| e[i]}.to_yaml}
    }
  end

  def test1406_remeasure_test
    m_count = 200

    # init time measurement
    $t_meas = []
  
    prop = :'setup.remeasurement.autoflagging.enabled'
    ($log.info "test skipped because #{prop}=false"; return) unless @@setup_remeasurement_autoflagging_enabled
    if ["itdc"].member?($env)
      $sil = Space::SIL.new($env)
      assert_equal "true", $sil.properties.advanced[prop], "advanced SIL property #{prop} must be set"
    end
    
    # prepare remeasurement
    assert clean_up(:lot=>@@lot, :eqp=>@@eqp)
    folder = $lds_setup.folder("AutoCreated_QA")
    folder.delete_channels if folder
    parameters = @@parameters.collect {|p| p + "_REMEASURE"}
    submit_process_dcr(@@ppd, :eqp=>@@eqp, :lot=>@@lot, :technology=>"TEST")
    
    # remeasure
    m_count.times{|n|
      $log.info("run #{n+1}/#{m_count}")
      remeasure_scenario(nil, folder, :parameters=>parameters, :technology=>"TEST")
    }
    
    # save results
    ts = Time.now.to_i
    $files = [] if $files.nil?
    $t_meas.first.count.times{|i|
      fn = "t_meas_#{ts}_#{i}.yaml"
      $files << fn
      f = File.open(fn,'w'){|f| f.puts $t_meas.collect{|e| e[i]}.to_yaml}
    }
  end
  
  def test1490_save_chart
    require 'charts.rb'
    c = Chart::XY.new nil
    c.add_data_file $files[0], :addcount=>true, :name=>"#{$env} inline sending dcr"
    c.add_data_file $files[1], :addcount=>true, :name=>"#{$env} inline sending dcr and checking flags"
    c.add_data_file $files[2], :addcount=>true, :name=>"#{$env} setup sending dcr"
    c.add_data_file $files[3], :addcount=>true, :name=>"#{$env} setup sending dcr and checking flags"
    c.create_chart :title=>'space remeasurement', :x=>'i-th value', :y=>'t[s]'
    c.write_with_legend "remeasurement_#{$env}.png"
  end

  # # # # #

  # added parameter timemeasurement for load tests
  def remeasure_scenario(export, folder, params={})
    lot = (params[:lot] or @@lot)
    wafer_values = (params[:wafer_values] or @@wafer_values)
    mpd = (params[:mpd] or @@mpd)
    mtool = (params[:mtool] or @@mtool)
    technology = (params[:technology] or "32NM")
    internalflag = (params[:internalflag] or false)
    parameters = (params[:parameters] or @@parameters)
    
    assert clean_up(:lot=>lot) if lot != "OTHERLOT.01"
  
    old_nsamples = {}
    chs = folder.spc_channels
    parameters.each {|p|
      ch = chs.select{|c| c.parameter==p}.first
      old_nsamples[p] = ch ? ch.samples.size : 0
      ch.samples.each {|s| s.flag} if internalflag && ch
    }
    # 
    # build and submit DCR for related MPD
    dcr = Space::DCR.new(:eqp=>mtool, :lot=>lot, :op=>mpd, :technology=>technology)
    dcr.add_parameters(parameters, wafer_values, :processing_areas=>["CHC"])
    $t_start = Time.now.to_f if params[:timemeasurement]
    $t_meas << [] if params[:timemeasurement]
    res = $client.send_dcr(dcr, :export=>export)
    assert $spctest.verify_dcr_accepted(res, parameters.size * wafer_values.size), "DCR not submitted successfully"
    #
    # check for flags
    $t_meas.last << Time.now.to_f - $t_start if params[:timemeasurement]
    assert wait_for(:sleeptime=>1){verify_autoflagging(folder, old_nsamples, :parameters=>parameters, :technology=>technology)}
    $t_meas.last << Time.now.to_f - $t_start if params[:timemeasurement]
  end
  
  def verify_autoflagging(folder, old_nsamples, params={})
    parameters = (params[:parameters] or @@parameters)
    chs = []
    wait_for(:sleeptime=>1){!(chs = folder.spc_channels).count.zero?}
    res = true
    parameters.each {|p|
      ch = chs.select{|c| c.parameter==p}.first
      #assert_equal (old_nsamples[p] + wafer_values.size), ch.samples.size, "wrong number of samples in #{ch.inspect}"
      res &= verify_autoflagging_channel(ch, old_nsamples[p], params)
    }
  end
  
  def verify_autoflagging_channel(ch, old_nsamples, params={})
    technology = (params[:technology] or "32NM")
    remeasurement_enabled = ((technology != "TEST") & @@inline_remeasurement_autoflagging_enabled) | 
      ((technology == "TEST") & @@setup_remeasurement_autoflagging_enabled)
    res = true
    if remeasurement_enabled
      res &= $spctest.verify_channel_remeasured(ch)
      $log.error("error in autoflagging, samples have to be flagged for remeasurement") unless res
    elsif old_nsamples > 0
      ch.samples[0..(old_nsamples-1)].each {|s|
        res &= $spctest.verify_remeasured(s,false)
        $log.error("error in autoflagging, samples do not have to be flagged for remeasurement") unless res
      }
    end
    res
  end
end
