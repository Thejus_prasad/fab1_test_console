=begin 
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.
    
Author: Daniel Steger, 2012-05-16
=end

require_relative 'Test_Space_Setup'

# Test Space/SIL ERFs, for MSR547158, MSR581025
class Test_Space12 < Test_Space_Setup
  @@file_generic_keys_dc_0 = 'etc/testcasedata/space/GenericKeySetup_QA_DC_0.SpecID.xlsx'
  @@file_generic_keys_boatslot = 'etc/testcasedata/space/GenericKeySetup_QA_BoatSlot.SpecID.xlsx'
  @@generic_ekeys_dc_0 = {'Reserve4'=>'equipment', 'Reserve5'=>'logicalrecipe',
    'Reserve6'=>'mainprocess', 'Reserve7'=>'process', 'Reserve8'=>'dcpname'}
  @@generic_dkeys_dc_0 = {'DatReserve4'=>'equipment', 'DatReserve5'=>'logicalrecipe',
    'DatReserve6'=>'mainprocess', 'DatReserve7'=>'process', 'DatReserve8'=>'dcpname'}

  def setup
    super
    return unless $lds && self.class.class_variable_defined?(:@@lot) # bypass test0000_setup
    assert_equal 0, $sv.eqp_cleanup(@@eqp)
    assert_equal 0, $sv.lot_cleanup(@@lot)
    #
    $lds.all_spc_channels('QA_PARAM_90*').each {|ch| assert ch.delete}
    folder = $lds.folder(@@autocreated_qa)
    assert folder.delete_channels, 'error cleaning channels' if folder
  end
    
  def test1201_lot_multiprocess1
    plot_wafers = {
      'SILSPLIT01.000' => ['SILSPLIT01.000.01', 'SILSPLIT01.000.02', 'SILSPLIT01.000.03'],
      'SILSPLIT02.000' => ['SILSPLIT02.000.01', 'SILSPLIT02.000.02', 'SILSPLIT02.000.03'],
      'SILSPLIT03.000' => ['SILSPLIT03.000.01', 'SILSPLIT03.000.02', 'SILSPLIT03.000.03']
    }
    mlot_wafers = {
      'SILSPLIT01.000' => ['SILSPLIT01.000.02', 'SILSPLIT01.000.03'],
      'SILSPLIT01.001' => ['SILSPLIT01.000.01'],
      'SILSPLIT02.001' => ['SILSPLIT02.000.01', 'SILSPLIT02.000.02', 'SILSPLIT02.000.03'],
      'SILSPLIT03.001' => ['SILSPLIT03.000.01'],
      'SILSPLIT03.002' => ['SILSPLIT03.000.02'],
      'SILSPLIT03.003' => ['SILSPLIT03.000.03']
    }
    _submit_lot_split_scenario(plot_wafers, mlot_wafers)
  end

  def test1202_lot_multiprocess2
    plot_wafers = {
      'SILSPLIT01.000' => ['SILSPLIT01.000.01', 'SILSPLIT01.000.07', 'SILSPLIT01.000.05'],
      'SILSPLIT01.001' => ['SILSPLIT01.000.02', 'SILSPLIT01.000.04'],
      'SILSPLIT01.002' => ['SILSPLIT01.000.03', 'SILSPLIT01.000.06']
    }
    mlot_wafers = {
      'SILSPLIT01.000' => ['SILSPLIT01.000.01', 'SILSPLIT01.000.03', 'SILSPLIT01.000.05', 'SILSPLIT01.000.07','SILSPLIT01.000.06'],
      'SILSPLIT01.001' => ['SILSPLIT01.000.02', 'SILSPLIT01.000.04']
    }
    _submit_lot_split_scenario(plot_wafers, mlot_wafers)
  end

  def test1203_lot_multiprocess3
    plot_wafers = {
      'SILSPLIT01.001' => ['SILSPLIT01.000.01', 'SILSPLIT01.000.04'],
      'SILSPLIT01.002' => ['SILSPLIT01.000.03', 'SILSPLIT01.000.02']
    }
    mlot_wafers = {
      'SILSPLIT01.000' => ['SILSPLIT01.000.01', 'SILSPLIT01.000.02', 'SILSPLIT01.000.03', 'SILSPLIT01.000.04']
    }
    _submit_lot_split_scenario(plot_wafers, mlot_wafers)
  end

  def test1204_lot_multiprocess4
    plot_wafers = {
      'SILSPLIT01.001' => ['SILSPLIT01.000.01', 'SILSPLIT01.000.04'],
      'SILSPLIT01.002' => ['SILSPLIT01.000.03', 'SILSPLIT01.000.02'],
      'SILSPLIT01.003' => ['SILSPLIT01.000.05', 'SILSPLIT01.000.06'],
    }
    mlot_wafers = {
      'SILSPLIT01.002' => [],
      'SILSPLIT01.003' => []
    }
    _submit_lot_split_scenario(plot_wafers, mlot_wafers)
  end

  def test1205_lot_multiprocess5
    plot_wafers = {
      'SILSPLIT01.001' => ['SILSPLIT01.000.01', 'SILSPLIT01.000.04'],
      'SILSPLIT01.002' => ['SILSPLIT01.000.03', 'SILSPLIT01.000.02'],
      'SILSPLIT01.003' => ['SILSPLIT01.000.05', 'SILSPLIT01.000.06'],
    }
    mlot_wafers = {
      'SILSPLIT01.000' => ['SILSPLIT01.000.01', 'SILSPLIT01.000.02', 'SILSPLIT01.000.03', 'SILSPLIT01.000.04'],
      'SILSPLIT01.003' => []
    }
    _submit_lot_split_scenario(plot_wafers, mlot_wafers)
  end


  def test1206_lot_split_singleprocess1
    plot_wafers = {
      'SILSPLIT01.000' => ['SILSPLIT01.000.01', 'SILSPLIT01.000.02', 'SILSPLIT01.000.03'],
      'SILSPLIT02.000' => ['SILSPLIT02.000.01', 'SILSPLIT02.000.02', 'SILSPLIT02.000.03'],
      'SILSPLIT03.000' => ['SILSPLIT03.000.01', 'SILSPLIT03.000.02', 'SILSPLIT03.000.03']
    }
    mlot_wafers = {
      'SILSPLIT01.000' => ['SILSPLIT01.000.02', 'SILSPLIT01.000.03'],
      'SILSPLIT01.001' => ['SILSPLIT01.000.01'],
      'SILSPLIT02.001' => ['SILSPLIT02.000.01', 'SILSPLIT02.000.02', 'SILSPLIT02.000.03'],
      'SILSPLIT03.001' => ['SILSPLIT03.000.01'],
      'SILSPLIT03.002' => ['SILSPLIT03.000.02'],
      'SILSPLIT03.003' => ['SILSPLIT03.000.03']
    }
    _submit_lot_split_scenario(plot_wafers, mlot_wafers, single_process: true)
  end

  def test1207_lot_split_singleprocess2
    plot_wafers = {
      'SILSPLIT01.000' => ['SILSPLIT01.000.01', 'SILSPLIT01.000.07', 'SILSPLIT01.000.05'],
      'SILSPLIT01.001' => ['SILSPLIT01.000.02', 'SILSPLIT01.000.04'],
      'SILSPLIT01.002' => ['SILSPLIT01.000.03', 'SILSPLIT01.000.06']
    }
    mlot_wafers = {
      'SILSPLIT01.000' => ['SILSPLIT01.000.01', 'SILSPLIT01.000.03', 'SILSPLIT01.000.05', 'SILSPLIT01.000.07','SILSPLIT01.000.06'],
      'SILSPLIT01.001' => ['SILSPLIT01.000.02','SILSPLIT01.000.04']
    }
    _submit_lot_split_scenario(plot_wafers, mlot_wafers, single_process: true)
  end

  def test1208_lot_split_singleprocess3
    plot_wafers = {
      'SILSPLIT01.001' => ['SILSPLIT01.000.01', 'SILSPLIT01.000.04'],
      'SILSPLIT01.002' => ['SILSPLIT01.000.03', 'SILSPLIT01.000.02']
    }
    mlot_wafers = {
      'SILSPLIT01.000' => ['SILSPLIT01.000.01', 'SILSPLIT01.000.02', 'SILSPLIT01.000.03', 'SILSPLIT01.000.04']
    }
    _submit_lot_split_scenario(plot_wafers, mlot_wafers, single_process: true)
  end

  def test1209_lot_split_singleprocess4
    plot_wafers = {
      'SILSPLIT01.001' => ['SILSPLIT01.000.01', 'SILSPLIT01.000.04'],
      'SILSPLIT01.002' => ['SILSPLIT01.000.03', 'SILSPLIT01.000.02'],
      'SILSPLIT01.003' => ['SILSPLIT01.000.05', 'SILSPLIT01.000.06'],
    }
    mlot_wafers = {
      'SILSPLIT01.002' => [],
      'SILSPLIT01.003' => []
    }
    _submit_lot_split_scenario(plot_wafers, mlot_wafers, single_process: true)
  end

  def test1210_lot_split_singleprocess5
    plot_wafers = {
      'SILSPLIT01.001' => ['SILSPLIT01.000.01','SILSPLIT01.000.04'],
      'SILSPLIT01.002' => ['SILSPLIT01.000.03','SILSPLIT01.000.02'],
      'SILSPLIT01.003' => ['SILSPLIT01.000.05','SILSPLIT01.000.06'],
    }
    mlot_wafers = {
      'SILSPLIT01.000' => ['SILSPLIT01.000.01', 'SILSPLIT01.000.02', 'SILSPLIT01.000.03', 'SILSPLIT01.000.04'],
      'SILSPLIT01.003' => []
    }
    _submit_lot_split_scenario(plot_wafers, mlot_wafers, single_process: true)
  end
  
  def test1220_generic_keys_dc
    # load setup file
    assert $spctest.sil_upload_verify_generickey(@@file_generic_keys_dc_0), 'file upload failed'
    #
    mpd = 'QA-MB-FTDEPM.01'
    mtool = 'QAMeas'
    lotparameter = 'QA_LOT_PARAM'
    #
    ppds = $sildb.pd_reference(mpd: mpd)[0..4]
    assert_equal 5, ppds.size, "no PD references found for #{mpd}"
    $log.info "using process PDs #{ppds.inspect}"
    #
    # build and submit DCRs for 1 process PDs
    dcrs = []
    ppds.each_with_index {|ppd, i|
      lot = @@lot.sub('.00', ".0#{i+1}")
      wafer_values1 = Hash[(@@wafer_values.to_a[0..i])]
      wafer_values2 = Hash[(@@wafer_values.to_a[i+1..-1])]
      $log.info "#{lot}, #{wafer_values1.inspect}, #{wafer_values2.inspect}"
      dcrs << submit_process_dcr(ppd, eqp: "#{@@eqp}_#{i}", lot: @@lot,
        logical_recipe: "F-P-FTEOS8K15.0#{i}", machine_recipe: "F-CVD60X-P.F-P-FTEOS8K15.0#{i}",
        wafer_values: wafer_values1, addlots: [lot], addwafer_values: [wafer_values2], export: "log/#{__method__}_p_#{i}.xml")
    }
    #
    # build and submit DCR for related MPD
    dcr = Space::DCR.new(eqp: mtool, lot: @@lot, op: mpd)
    dcrs.unshift dcr
    dcr.add_parameters(@@parameters, @@wafer_values)
    dcr.add_parameter(lotparameter, {@@lot=>7.0}, reading_type: 'Lot')
    res = $client.send_dcr(dcr, export: :caller)
    assert $spctest.verify_dcr_accepted(res, @@parameters.size * @@wafer_values.size + 1), 'DCR not submitted successfully'

    folder = $lds.folder(@@autocreated_qa)
    ekeys = $spctest.get_sample_generickeys_dc(dcrs, @@generic_ekeys_dc_0)
    dkeys = $spctest.get_sample_generickeys_dc(dcrs, @@generic_dkeys_dc_0)
    $spctest.verify_parameters(@@parameters, folder, @@wafer_values.size, ekeys, dkeys)
    $spctest.verify_parameters([lotparameter], folder, 1, ekeys, dkeys)
  end

  def test1221_generic_keys_boatslot
    # upload setup file
    assert $spctest.sil_upload_verify_generickey(@@file_generic_keys_boatslot), 'file upload failed'
    #
    mpd = 'QA-MB-FTDEPM.01'
    mtool = 'QAMeas'
    #
    ppds = $sildb.pd_reference(mpd: mpd)[0..4]
    assert_equal ppds.size, ppds.compact.size, "no PD references found for #{mpd}"
    $log.info "using process PDs #{ppds.inspect}"
    #
    # build and submit DCRs for 5 process PDs
    wafer_values = []
    $dcrs = dcrs = []
    ppds.each_with_index {|ppd, i|
      dcrp = Space::DCR.new(eqp: "#{@@eqp}_#{i}", eqptype: 'PROCESS-CHAMBER', lot: @@lot, op: ppd,
        logical_recipe: "F-P-FTEOS8K15.0#{i}", machine_recipe: "F-CVD60X-P.F-P-FTEOS8K15.0#{i}")
      dcrs << dcrp

      wafer_value = {'2502J493KO'=>"#{i}01", '2502J494KO'=>"#{i}02", '2502J495KO'=>"#{i}03",
      '2502J496KO'=>"#{i}04", '2502J497KO'=>"#{i}05", '2502J498KO'=>"#{i}06"}

      wafer_values1 = Hash[*(wafer_value.to_a[0..i].flatten)]
      wafer_values2 = Hash[*(wafer_value.to_a[i+1..-1].flatten)]
      lot = @@lot.sub('.00', ".0#{i+1}")
      
      boatslot_param = "D-BoatSlot#{i}"
      dcrp.add_parameter(boatslot_param, wafer_values1, nocharting: true, value_type: 'string')
      dcrp.add_lot(lot, op: ppd, logical_recipe: "F-P-FTEOS8K15.0#{i}")
      dcrp.add_parameter(boatslot_param, wafer_values2, nocharting: true, value_type: 'string')

      $log.info "#{lot}, #{wafer_values1.inspect}, #{wafer_values2.inspect}"

      wafer_values << wafer_value

      #dcrp.add_parameters(@@parameters, @@wafer_values, nocharting: true)
      res = $client.send_dcr(dcrp, export: "log/#{__method__}_p_#{i}.xml")
      assert $spctest.verify_dcr_accepted(res, 0), "DCR #{i} not submitted successfully"
    }
    #
    # build and submit DCR for related MPD
    dcr = Space::DCR.new(eqp: mtool, lot: @@lot, op: mpd)
    dcrs.unshift dcr

    wafer_value = {'2502J493KO'=>'501', '2502J494KO'=>'502', '2502J495KO'=>'503',
      '2502J496KO'=>'504', '2502J497KO'=>'505', '2502J498KO'=>'506'}
    boatslot_param = 'D-BoatSlotM'
    dcr.add_parameter(boatslot_param, wafer_value, nocharting: true, value_type: 'string')
    wafer_values << wafer_value

    dcr.add_parameters(@@parameters, @@wafer_values)
    res = $client.send_dcr(dcr, export: "log/#{__method__}_m.xml")
    assert $spctest.verify_dcr_accepted(res, @@parameters.size * @@wafer_values.size), 'DCR not submitted successfully'
    #
    # verify sample generic keys
    folder = $lds.folder(@@autocreated_qa)
    @@parameters.each {|p|
      ch = folder.spc_channel(parameter: p)
      samples = ch.samples
      assert_equal @@wafer_values.size, samples.size, "wrong number of samples in #{ch.inspect}"
      samples.each {|s|
        w = s.extractor_keys['Wafer']
        generic_ekeys_boatslot = {'Reserve4'=>wafer_values[0][w], 'Reserve5'=>wafer_values[1][w],
          'Reserve6'=>wafer_values[2][w], 'Reserve7'=>wafer_values[3][w], 'Reserve8'=>wafer_values[5][w]}
        assert $spctest.verify_data(s.extractor_keys, generic_ekeys_boatslot)
        generic_dkeys_boatslot = {'DatReserve4'=>wafer_values[0][w], 'DatReserve5'=>wafer_values[1][w],
          'DatReserve6'=>wafer_values[2][w], 'DatReserve7'=>wafer_values[3][w], 'DatReserve8'=>wafer_values[5][w]}
        assert $spctest.verify_data(s.data_keys, generic_dkeys_boatslot)
      }
    }
  end

  def _submit_lot_split_scenario(plot_wafers, mlot_wafers, params={})
    mpd = 'QA-MULT-PD-MB-FTDEPM.01'
    mtool = 'QAMeas'
    lotparameter = 'QA_LOT_PARAM'
    ppds = $sildb.pd_reference(mpd: mpd)
    refute_empty ppds, "MPD #{mpd} does not reference any process PDs"
    caller = caller_locations(1,1)[0].label

    addlots = [] if params[:single_process]
    addwafer_values = [] if params[:single_process]
    wafer_cha = {}
    wafer_ope = {}
    pindex = 0
    #
    # build and submit DCR for process PD
    plot_wafers.each_pair do |lot, wafers|
      eqp = "#{@@eqp}_#{pindex}"
      wafer_values = {}
      wafers.each_with_index do |w,i|
        wafer_values[w] = i
        wafer_ope[w] = [ppds[pindex], eqp]
      end
      if params[:single_process]
        addlots << lot
        addwafer_values << wafer_values
      else
        submit_process_dcr(ppds[pindex], lot: lot, parameters: ['Dummy'], eqp: eqp, wafer_values: wafer_values,
          processing_areas: 'cycle_wafer', export: "log/#{caller}_#{pindex}_p.xml")
        wafers.each_with_index {|w,i| wafer_cha[w] = @@chambers.keys[i % @@chambers.keys.count]}
        pindex += 1
      end
    end
    if params[:single_process]
      submit_process_dcr(ppds[pindex], lot: addlots[0], parameters: ['Dummy'], eqp: "#{@@eqp}_0", wafer_values: addwafer_values[0],
        processing_areas: 'cycle_wafer', export: "log/#{caller}_#{pindex}_p.xml",
        addlots: addlots[1..-1], addwafer_values: addwafer_values[1..-1])
      plot_wafers.values.flatten.each_with_index {|w, i| wafer_cha[w] = @@chambers.keys[i % @@chambers.keys.count]}
    end
    # 
    # build and submit DCR for related MPD    
    dcr = Space::DCR.new(eqp: mtool, lot: nil)
    lsamples = 0
    mlot_wafers.each_pair do |lot, wafers|
      wafer_values = {}
      dcr.add_lot(lot, op: mpd)
      if wafers != []
        wafers.each_with_index do |w,i|
          dcr.add_wafer(w)
          wafer_values[w] = i
        end
        dcr.add_parameters(@@parameters, wafer_values)
      else
        dcr.add_parameter(lotparameter, {lot=>7.0}, reading_type: 'Lot')
        lsamples += 1
      end
    end
    wsamples = mlot_wafers.values.flatten.size # ignore all lot only parameters
        
    res = $client.send_dcr(dcr, export: "log/#{caller}_m.xml")
    assert $spctest.verify_dcr_accepted(res, @@parameters.size*wsamples + lsamples), 'DCR not submitted successfully'
    #
    # verify sample extractor keys
    folder = $lds.folder(@@autocreated_qa)
    @@parameters.each_with_index do |p, i|
      ch = folder.spc_channel(parameter: p)
      next unless ch
      samples = ch.samples      
      assert_equal wsamples, samples.size, "wrong number of samples in #{ch.inspect}"
      samples.each do |s|
        w = s.extractor_keys['Wafer']
        ope, eqp = wafer_ope[w]
        # must match process PD and tool
        assert $spctest.verify_data(s.extractor_keys, {'POperationID'=>ope, 'PTool'=>eqp, 'PChamber'=>wafer_cha[w]})
      end
    end
    # verify lot parameter
    ch = folder.spc_channel(parameter: lotparameter)
    if ch
      samples = ch.samples
      assert_equal lsamples, samples.size, "wrong number of samples in #{ch.inspect}"
      samples.each do |s|
        l = s.extractor_keys['Lot']
        w = plot_wafers[l][0]
        ope, eqp = wafer_ope[w]
        # must match process PD and tool
        assert $spctest.verify_data(s.extractor_keys, {'POperationID'=>ope, 'PTool'=>eqp, 'PChamber'=>'-'})
      end
    end
  end

end
