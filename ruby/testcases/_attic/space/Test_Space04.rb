=begin
(c) Copyright 2011 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.
=end

require_relative 'Test_Space_Setup'

# automated test of setup.FC <-> SIL interaction, cf. Test_SetupFC_Setup
# main purpose is to submit neccessary standard specs
class Test_Space04 < Test_Space_Setup
  def self.startup
    super
    @@specsAndApprovers = {
      @@monitoringSpecLimitsTable => @@approvers[1],
      @@responsiblePDTable => @@approvers[0],
      @@specLimitsTable => @@approvers[0],
      @@specLimitsHierarchyTable => @@approvers[0],
      @@importantChambersTable => @@approvers[0]
    }
  end

  def test00_cleanup
    @@specsAndApprovers.each_pair {|spec, approver|
      if $sfc.wait_for_approve(spec, user: approver, timeout: 1, sleeptime: 1)
        $sfc.human_workflow_action(spec, action: 'CANCEL', user: approver, polling: true)
        sleep 30
      end
      if $sfc.wait_for_edit_content(spec, user: approver, timeout: 1, sleeptime: 1)
        $sfc.human_workflow_action(spec, action: 'CANCEL', user: approver, polling: true)
        sleep 30
      end
      if $sfc.wait_for_edit_content(spec, user: approver, timeout: 1, sleeptime: 1)
        $sfc.human_workflow_action(spec, action: 'CANCEL', user: approver, polling: true)
        sleep 30
      end
    }
  end

  def test01_edit_submit_approve_all
    @@specsAndApprovers.each_pair {|spec, approver|
      version = $sfc.get_specification(spec)[1]
      $log.info "Start edit workflow for #{spec} v#{version}"
      $sfc.content_change(spec)
      res, version = $sfc.submit_specification(spec)
      $log.info "new version: #{version}"
      $sfc.human_workflow_action(spec, action: 'APPROVE', user: approver, polling: true)
      sleep 30
      assert wait_for{$sfc.get_specification(spec)[1] == version}
    }
  end

  def test02_getspec_and_compare
    $log.info "Getting spec: #{@@specLimitsHierarchyTable}"
    content,version = $sfc.get_specification(@@specLimitsHierarchyTable)
    $log.info "Spec Content: #{content.inspect}"
    $log.info "Spec Version: #{version.inspect}"
  end
end
