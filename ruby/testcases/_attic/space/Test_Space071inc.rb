=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: U. Koerner, 2012-01-11

History:
  2014-02-05 ssteidte,  separated Test_Space07, code cleanup
=end

require_relative 'Test_Space_Setup'


# Test Space/SIL CAs
class Test_Space071inc < Test_Space_Setup
  @@mpd_qa = 'QA-MB-FTDEPM-LIS-CAEX.01'


  def setup
    super
    return unless $lds && self.class.class_variable_defined?(:@@lot) # bypass test0000_setup
    assert_equal 0, $sv.eqp_cleanup(@@eqp)
    assert_equal 0, $sv.lot_cleanup(@@lot)
    assert_equal 0, $sv.inhibit_cancel(:route, $sv.lot_info(@@lot).route)
    #
    $lds.all_spc_channels('QA_PARAM_90*').each {|ch| assert ch.delete}
    folder = $lds.folder(@@autocreated_qa)
    assert folder.delete_channels, "error cleaning channels" if folder
    @@chamber_a = 'PM1'
    @@chamber_b = 'PM2'
    @@chamber_c = 'CHX'
    @@chambers = {@@chamber_a=>5, @@chamber_b=>6, @@chamber_c=>7}
  end


  def test0700_setup
    $setup_ok = false
    #
    assert @@ppd_qa = $sildb.pd_reference(mpd: @@mpd_qa).first, "no PD reference found for #{@@mpd_qa}"
    #
    ['LotHold', 'AutoLotHold', 'ReleaseLotHold', 'CancelLotHold',
     'EquipmentInhibit', 'AutoEquipmentInhibit', 'ReleaseEquipmentInhibit', 'ReleaseEquipmentInhibit-noDefault',
     'ChamberInhibit', 'AutoChamberInhibit', 'ReleaseChamberInhibit', 'ReleaseChamberInhibit-noDefault',
     'ReticleInhibit', 'AutoReticleInhibit', 'ReleaseReticleInhibit',
     'RouteInhibit', 'AutoRouteInhibit', 'ReleaseRouteInhibit',
     'MachineRecipeInhibit', 'AutoMachineRecipeInhibit', 'ReleaseMachineRecipeInhibit', 'ReleaseMachineRecipeInhibit-noDefault',
     'AutoSetWaferScriptParameter-OOC', 'AutoSetLotScriptParameter-OOC', 'AutoSetLotScriptParameter-OOCInteger', 'SetLotScriptParameter-OOC',
     'AutoUnknown', 'Unknown', 'CACollection-ReleaseAll',
     'AutoChamberAndRecipeInhibit-Multi',
     'ReleaseEquipmentAndRecipeInhibit', 'ReleaseEquipmentAndRecipeInhibit-noDeflt', 'CACollection-ReleaseAllRecipeInhibits'
    ].each {|action|
      assert $spc.corrective_action(action), "CA #{action.inspect} not defined"
    }
    #
    $setup_ok = true
  end

  def test0709_special_temp_discard
    channels = create_clean_channels
    assert $spctest.assign_verify_cas(channels,
      ['EquipmentInhibit', 'AutoEquipmentInhibit', 'ReleaseEquipmentInhibit', 'LotHold', 'AutoLotHold', 'CancelLotHold', 'ReleaseEquipmentInhibit-noDefault'])
    #
    # send DCR with OOC values
    dcr = Space::DCR.new(lot: @@lot, eqp: @@mtool, op: @@mpd_qa)
    dcr.add_parameters(@@parameters, @@wafer_values2ooc)
    res = $client.send_dcr(dcr, export: :caller)
    assert $spctest.verify_dcr_accepted(res, @@parameters.size*@@wafer_values2ooc.size, nooc: 'all*2'), "DCR not submitted successfully"
    #
    assert $spctest.verify_futurehold2(@@lot, "[(Raw above specification)"), "lot must have a future hold"
    assert $spctest.verify_inhibit(@@eqp, 5), "equipment must have an inhibit"
    # check two ext. comments
    assert $spctest.verify_ecomment(channels, "LOT_HELD: #{@@lot}: reason={X-S")
    assert $spctest.verify_ecomment(channels, "TOOL_HELD: #{@@eqp}: reason={X-S")
  end
end
