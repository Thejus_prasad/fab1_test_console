=begin 
Test Space/SIL.

(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.
    
Author: Steffen Steidten, 2011-11-25
=end

require_relative 'Test_Space_Setup'

# Test Space/SIL.

class Test_Space00load < Test_Space_Setup
  Dept = "QA"
  
  def test006_load_on_server
    #n=20 is good for LET, 3 works for ITDC, higher values are not tested in ITDC but may work
    n = 3
    n = 20 if $env == "let"
    count = 1000 # DCR count per thread
    
    #    
    dpmt = "QADummy"
    parameter = "QA_DummyPARAM"
    productgroup = "QAPG"
    product = "SILPROD.0"
    #
    # ensure autocharting folders do not exist
    folder = $lds.folder("AutoCreated_#{dpmt}")
    assert_equal(0, folder.delete, "error deleting folder 'AutoCreated_#{dpmt}'") if folder
    
    #
    # send DCRs    
    $threads = n.times.collect {|i|
      Thread.new {
        test_ok = true
        client = Space::Client.new($env, :timeout=>900)
        count.times.collect {|j|
          dcr = Space::DCR.new(:department=>"#{dpmt}", :technology=>"#{i}NM", :productgroup=>"#{productgroup}#{i}", :product=>"#{product}#{i}")
          dcr.add_parameter("#{parameter}#{i}", @@wafer_values)
          res = client.send_dcr(dcr, :export=>"log/#{__method__}_#{i}.xml")
          ok = $spctest.verify_dcr_accepted(res, @@wafer_values.size)
          $log.warn "#{i}: #{res.inspect}" unless ok
          test_ok &= ok
        }
        test_ok
      }      
    }
    assert $threads.inject(true) {|res,t| res &= t.value}
  end

  def test007_remeasure_flagging
    folder = $lds.folder("AutoCreated_#{Dept}")
    folder.spc_channels.each_with_index {|ch, i|
      $log.info "#{i} deleting #{ch.chid}..."
      ch.delete(cascade: true)
    }

    n = 50 #number of all DCRS
    m = 1 #number of process DCRs
    nthreads = 5 #number of threads
    p = 20 #number of parameters

    _parameter = "QA_PARAML"
    pd = "QA_MB-FTDEP.01"
    pd2 = "QA_MB-FTDEPM.01"
    eqp = "UTC001"

    #
    # pre-build process DCRs
    dcrs = (1..m).collect {|i|
      ts = Time.now - (50 + i)*60
      wafer_values = Hash[*[25.times.collect {|j| waferid="WAFERID%04d" % [j]; [waferid, j]}]]
      $log.info "#{wafer_values.inspect}"
      dcr = Space::DCR.new(:department=>Dept, :eqp=>eqp, :op=>pd, :eqptype=>"PROCESS-CHAMBER", :timestamp=>ts)
      dcr.add_parameters(p.times.collect{|j| "#{_parameter}_#{j+1}"}, wafer_values, :nocharting=>true)
      dcr
    }

    # pre-build metrology DCRs with many lots
    dcrs += (m+1..n).collect {|i|
      ts = Time.now - (50 + i)*60
      wafer_values = Hash[*[25.times.collect {|j| waferid="WAFERID%04d" % [j]; [waferid, j]}]]
      $log.info "#{wafer_values.inspect}"
      dcr = Space::DCR.new(:department=>Dept, :op=>pd2, :timestamp=>ts)
      dcr.add_parameter("#{_parameter}_#{i%p+1}", wafer_values, :nocharting=>false)
      dcr
    }

    #
    # send DCRs
    test_ok = true
    $threads = nthreads.times.collect {|i|
      Thread.new {
        client = Space::Client.new($env, :timeout=>900)
        #sleep i
        a = n/nthreads
        (a*i..a*(i+1)-1).each do |index|
          res = client.send_dcr(dcrs[index], :export=>"log/lll_%02d.xml" % index)
          if res
            runID = res[:submitRunResponse][:submitRunReturn][:runID]
            ok = (runID != "0")
          else
            ok = false
          end
          $log.warn "t#{i+1}-#{index}: #{res.inspect}" #unless ok
          test_ok &= ok
          sleep 1
        end
      }
    }
    $threads.each {|t| t.join}
    channels = $lds.all_spc_channels("#{_parameter}*")
    channels.each { |ch| assert $spctest.verify_channel_remeasured(ch), "invalid flagging" }
    assert test_ok
  end

  def test008_persist_dcrs_deadlock
    n = 50 #number of all DCRS
    m = 0#n/2 #number of process DCRs
    nthreads = 10 #number of threads
    l = 25 #number of lots
    p = 20 #number of parameters

    _parameter = "QA_PARAML"
    pd = "QA_MB-FTDEP.01"
    pd2 = "QA_MB-FTDEPM.01"
    eqp = "UTC001"
    
    #
    # pre-build process DCRs
    dcrs = (1..m).collect {|i|
      wafer_values = Hash[*[25.times.collect {|j| waferid="WAFERID%04d" % [j]; [waferid, j]}]]
      $log.info "#{wafer_values.inspect}"
      dcr = Space::DCR.new(:department=>Dept, :eqp=>eqp, :op=>pd, :eqptype=>"PROCESS-CHAMBER")
      dcr.add_parameters(p.times.collect{|j| "#{_parameter}_#{j+1}"}, wafer_values, :nocharting=>true)
      dcr
    }

    # pre-build metrology DCRs with many lots
    dcrs += (m+1..n).collect {|i|
      wafer_values = Hash[*[1.times.collect {|j| waferid="WAFERID%04d" % [j]; [waferid, j]}]]
      $log.info "#{wafer_values.inspect}"
      dcr = Space::DCR.new(:department=>Dept, :op=>pd2)
      dcr.add_parameters(p.times.collect{|j| "#{_parameter}_#{j+1}"}, wafer_values, :nocharting=>false)
      (2..l).each {|l|
        dcr.add_lot("SPCDUMMY0.%02d" % l, :department=>Dept, :op=>pd2)
        dcr.add_parameters(p.times.collect{|j| "#{_parameter}_#{j+1}"}, wafer_values, :nocharting=>false)
      }
      dcr
    }

    #
    # send DCRs
    test_ok = true
    $threads = nthreads.times.collect {|i|
      Thread.new {        
        client = Space::Client.new($env, :timeout=>900)
        #sleep i
        a = n/nthreads
        (a*i..a*(i+1)-1).each do |index|
          res = client.send_dcr(dcrs[index], :export=>"log/lll_%02d.xml" % index)
          if res
            runID = res[:submitRunResponse][:submitRunReturn][:runID]
            ok = (runID != "0")
          else
            ok = false
          end
          $log.warn "t#{i+1}-#{index}: #{res.inspect}" #unless ok
          test_ok &= ok
          sleep 1
        end
      }
    }
    $threads.each {|t| t.join}
    assert test_ok
  end


  def test009_autocharting_templates
    n = 9
    _dpmt = "QAL"
    _parameter = "QA_PARAML"
    #
    # ensure autocharting folders do not exist
    50.times {|i|
      dpmt = "#{_dpmt}#{i+1}"
      folder = $lds.folder("AutoCreated_#{dpmt}")
      (assert_equal 0, folder.delete, "error deleting folder 'AutoCreated_#{dpmt}'") if folder
    }
    #
    # pre-build DCRs
    dcrs = n.times.collect {|i|
      dcr = Space::DCR.new(:department=>"#{_dpmt}#{i+1}", :technology=>"#{i+1}XNM")
      dcr.add_parameter("#{_parameter}#{i+1}", @@wafer_values, :space_listening=>true)
      dcr.to_xml
    }
    #
    # send DCRs
    test_ok = true
    $threads = n.times.collect {|i|
      Thread.new {
        client = Space::Client.new($env, :timeout=>900)
        res = client.send_dcr(:xml=>dcrs[i], :export=>"lll.xml")
        ok = $spctest.verify_dcr_accepted(res, @@wafer_values.size)
        $log.warn "#{i+1}: #{res.inspect}" unless ok
        test_ok &= ok
      }
    }
    $threads.each {|t| t.join}
    assert test_ok
    #
    # verify autocharting folder creation
    test_ok = true
    n.times {|i|
      dpmt = "#{_dpmt}#{i+1}"
      folder = $lds.folder("AutoCreated_#{dpmt}")
      ($log.warn "missing folder AutoCreated_#{dpmt}"; test_ok = false) unless folder
      chs = folder.spc_channels
      ($log.warn "wrong number of channels in AutoCreated_#{dpmt}"; test_ok = false) unless chs and chs.size == 1
    }
    assert test_ok
    #
    # clean up
    return
    n.times {|i|
      dpmt = "#{_dpmt}#{i+1}"
      folder = $lds.folder("AutoCreated_#{dpmt}")
      (assert_equal 0, folder.delete, "error deleting folder 'AutoCreated_#{dpmt}'") if folder
    }
  end
  
  def test010_upload
    10.times.collect{|i| 
      fname = "etc/testcasedata/space/GenericKeySetup_QA_FIXED_#{i}.SpecID.xlsx"
      assert $spctest.sil_upload_verify_generickey(fname), "file upload failed"
    }    
  end

end
