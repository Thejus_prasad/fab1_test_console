=begin 
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.
    
Author: Steffen Steidten, 2015-04-23
=end

require 'warptest'
require 'RubyTestCase'

class Test_EMSWorkflow < RubyTestCase
  @@pdname = 'EMS.ERF.APPROVE.1_0_7'
  @@tasks = {}
  @@user = 'userreq1@gfoundries.com'   # must be member of LDAP group .... (FC1.APP.WARP.D.QA.GROUP1)

  # ??? for ITDC
  @@specId = 'e0G112'  # aborted
  @@specRev = 'A'
  @@requestor = 'userreq1@gfoundries.com'
  

  def self.startup
    super(nosiview: true)
    $wtest = Warp::Test.new($env, pdname: @@pdname, tasks: @@tasks, user: @@user)
    $wc = $wtest.client
  end
  

  def test00_setup
    $setup_ok = false
    #
    assert pdefs = $wc.process_definitions, "no process definitions, WARP connection issues?"
    assert pdef = pdefs.find {|e| e.definition == @@pdname}, "process definition #{@@pdname} not found"
    #
    $setup_ok = true
  end

  def test11_happy
    assert pid = $wc.start_process_instance(@@pdname, data: {reviews: '', approvers: '', setups: ''})
    assert_empty $wc.instance_tasks(pid) # ??
  end
  
  def XXtest11_happy1 # short path, calling all elements
    # verify setup, clean up
    pdefs = $wc.process_definitions
    assert pdef = pdefs.find {|e| e.definition == @@pdname}
    assert $wc.process_instances(@@pdname).empty?
    # start new instance, delete and create again
    assert pid = $wc.start_process_instance(@@pdname, data: @@data)
    assert $wc.delete_process_instance(pid)
    assert $wc.process_instances(@@pdname).empty?
    assert pid = $wc.start_process_instance(@@pdname, data: @@data)
    # first task is assignable
    tasks = $wc.instance_tasks(pid)
    assert_equal 1, tasks.size
    task = tasks.first
    assert $wc.assignable_tasks(@@user).size > 0  # tasks from other workflows may be assignable
    # claim and release task
    assert $wc.claim_user_task(@@user, task)
    task = $wc.instance_tasks(pid).first
    assert_equal @@user, task.assignee
    assert_equal @@task1, task.name
    assert $wc.release_user_task(@@user, task)
    assert_equal '', $wc.instance_tasks(pid)[0].assignee
    # claim and complete first task
    assert $wc.claim_user_task(@@user, task)
    assert $wc.complete_user_task(@@user, task, data: {'priority'=>'HIGH'}) # any prio except "LOW"
    # $wc.process_instance_dataset(pid) => compare??
    # flow went the "High Priority" path, now at final gateway waiting for a signal
    assert $wc.signal_process_instance(pid, data: {'feedback'=>'QAFeedback HIGH'})
    assert $wc.process_instances(@@pdname).empty?
  end
  
end
