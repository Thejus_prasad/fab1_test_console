=begin
FabView load test

(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

    
Author: Thomas KLose, 2016-02-25

History:
  2017-12-08 sfrieske, simplified FabView::Client instantiation
=end

require 'RubyTestCase'
require 'fabview'
require 'thread'

class Test_FabViewPerf < RubyTestCase
  @@users = 10

  @@res = {}
  @@threads = []
  @@fv = []
  
  def test00_setup
    $setup_ok = false
    assert @@fv = @@users.times.collect {sleep 0.1; FabView::Client.new($env)}
    $setup_ok = true
  end
  
  def test10_loadtest_lothist
    threads = []
    res = []
    ll = $sv.lot_list
    @@fv.each {|fv|
      threads << Thread.new {
        100.times {
          tstart = Time.now
          x = fv.rest('lot', ll.sample)
          time = Time.now - tstart
          res << time
          }
        }
      }
    threads.each {|t| t.join}
    @@res["lot_hist"] = [result(res)] 
  end

  def test11_loadtest_slotstory
    threads = []
    res = []
    ll = $sv.lot_list
    @@fv.each {|fv|
      threads << Thread.new {
        100.times {
          tstart = Time.now
          x = fv.rest('lotStory', ll.sample)
          time = Time.now - tstart
          res << time
          }
        }
      }
    threads.each {|t| t.join}
    @@res["lot_hist"] = [result(res)] 
  end  
  
  def test20_loadtest_eqphist
    threads = []
    res = []
    el = $sv.eqp_list
    @@fv.each {|fv|
      threads << Thread.new {
        100.times {
          tstart = Time.now
          x = fv.rest('equipment', el.sample)
          time = Time.now - tstart
          res << time
          }
        }
      }
    threads.each {|t| t.join}
    @@res["eqp_hist"] = [result(res)] 
  end
  
  def test30_loadtest_rethist
    threads = []
    res = []
    rl = $sv.reticle_list
    @@fv.each {|fv|
      threads << Thread.new {
        100.times {
          tstart = Time.now
          x = fv.rest('reticle', rl.sample)
          time = Time.now - tstart
          res << time
          }
        }
      }
    threads.each {|t| t.join}
    @@res["ret_hist"] = [result(res)] 
  end

  def test40_loadtest_carthist
    threads = []
    res = []
    cl = $sv.carrier_list
    @@fv.each {|fv|
      threads << Thread.new {
        100.times {
          tstart = Time.now
          x = fv.rest('carrier', cl.sample)
          time = Time.now - tstart
          res << time
          }
        }
      }
    threads.each {|t| t.join}
    @@res["car_hist"] = [result(res)] 
  end
  
  def test99_result
    @@res.each{|k,v| puts (k + " " + v.inspect)}
  end
  
  def result(data)
    return data.min, data.max, data.mean
  end
end
