=begin 
Test PCL Applications (Toolwish, Lotguard, Sampling)

(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.
    
Author: Daniel Steger, 2013-04-11

=end

require "RubyTestCase"
require "traceeda"
require "e3data"

# Simple test for Trace Data Collection to E3
class Test_TraceEDA < RubyTestCase
  Description = "Trace Data Collection to E3"
  
  @@eqp = "QATEST"
  @@chamber = ["PM2", "PM3"]
  @@reportings = 100
  @@context = { 
      :cj => "#{@@eqp}-#{Time.now.to_i}",
      :lot => "TRACELOT1.00",
      :wafer => "WAFER0000001",
      :recipename => "Q-PM2-PC-MECH",
      :slot => "1",
      :recipestepno => "1"
   }
  
  def setup()
    super
    $e3test = E3::Test.new($env) unless $e3test and $e3test.env == $env    
  end
  
  def teardown()
    if $eas.sid
      $etc_pm2.unresolve_plan()
      $eas.close
    end
    super
  end
  
  def _init_plan(method, vars, events=nil)  
    $eas = TraceEDA::EAS.new($env, :plan=>"QA-#{Time.now.to_i}", :mappings=>vars, :event_map=>events)
    $eas.connect  
    $eas.store_plan :export=>"log/#{method}_abstr_map.xml"
    $etc_pm2 = TraceEDA::ETC.new(@@eqp, $eas, :chambers=>["PM2"], :subprocs=>["SIDE1"])
    assert $etc_pm2    
    $etc_pm2.plan_mapping :export=>"log/#{method}_plan_map.xml"
  end
  
  def test00_plan_activation
    vars = { "PA:[PA=ProcessingChamber]/PGC__CDAPressure_AI" => "float" }

    self._init_plan(__method__, vars)

    
  end

  def test01_data_reporting_existing_var
    vars = { "PA:[PA=ProcessingChamber]/PGC__CDAPressure_AI" => "float" }

    self._init_plan(__method__, vars)
    
    txtime = Time.now
    $etc_pm2.report_event "PA:PM2/ProcessingStarted_Coronus", @@context.merge({ :export=>"log/eve_procstart.xml" })
    $data = []
    10.times {|i|
      col = $etc_pm2.create_collections(:func=>lambda {|x,y,seed| Math.cos(seed+x+y)}, :ncollections=>3, :seed=>i)
      $etc_pm2.report :collections=>col, :export=>"log/trace_%02d.xml" % i
      $data << col
      sleep 1
    }
    $etc_pm2.report_event "PA:PM2/ProcessingCompleted_Coronus", @@context.merge({ :export=>"log/eve_proccomp.xml" })

    $log.info "Check E3 ..."
    assert wait_for(:sleeptime=>30) {
      $e3test.verify_run("#{@@eqp}_PM2", 1, ["ProcessStarted","ProcessCompleted"], @@context, txtime)
    }
    assert $e3test.verify_data("#{@@eqp}_PM2", "PGC__CDAPressure_AI", $data, txtime)
  end
  
  def test02_data_reporting_new_var
    variable = "PGC__CDAPressure_#{Time.now.to_i}"
    vars = { "PA:[PA=ProcessingChamber]/#{variable}" => "float" }

    self._init_plan(__method__, vars)
    
    txtime = Time.now
    $data = []
    $etc_pm2.report_event "PA:PM2/ProcessingStarted_Coronus", @@context.merge({ :export=>"log/eve_procstart.xml" })
    10.times {|i|
      col = $etc_pm2.create_collections(:func=>lambda {|x,y,seed| Math.cos(seed+x+y)}, :ncollections=>1, :seed=>i)
      $etc_pm2.report :collections=>col, :export=>"log/trace_%02d.xml" % i
      sleep 1
    }
    $etc_pm2.report_event "PA:PM2/ProcessingCompleted_Coronus", @@context.merge({ :export=>"log/eve_proccomp.xml" })

    $log.info "Check E3 ..."
    assert wait_for(:sleeptime=>30) {
      $e3test.verify_run("#{@@eqp}_PM2", 1, ["ProcessStarted","ProcessCompleted"], @@context, txtime)
    }
    assert $e3test.verify_data("#{@@eqp}_PM2", variable, $data, txtime)
  end
  
  def test03_data_reporting_subchamber
    vars = { "PA:PM2/SPA:[SPA=Side]/PGC__CDAPressure_AI" => "float" }
    evt_map = {}
    evt_map["PA:PM2/SPA:[SPA=Side]/ProcessingStarted_Coronus"] =
      { "PA:PM2/SPA:[SPA=Side]/ProcessStarted" =>
        ["PA:PM2/SPA:[SPA=Side]/LotID", "PA:PM2/SPA:[SPA=Side]/WaferID",
          "PA:PM2/SPA:[SPA=Side]/SlotID", "PA:PM2/SPA:[SPA=Side]/RecipeName",
         "PA:PM2/SPA:[SPA=Side]/ControlJobID"
        ]
      }
    evt_map["PA:PM2/SPA:[SPA=Side]/ProcessingCompleted_Coronus"] =
      { "PA:PM2/SPA:[SPA=Side]/ProcessCompleted" =>
        ["PA:PM2/SPA:[SPA=Side]/LotID", "PA:PM2/SPA:[SPA=Side]/WaferID",
          "PA:PM2/SPA:[SPA=Side]/SlotID", "PA:PM2/SPA:[SPA=Side]/RecipeName",
         "PA:PM2/SPA:[SPA=Side]/ControlJobID"
        ]
      }

    self._init_plan(__method__, vars, evt_map)
    
    txtime = Time.now
    $etc_pm2.report_event "PA:PM2/SPA:SIDE1/ProcessingStarted_Coronus", @@context.merge({ :export=>"log/eve_procstart.xml" })
    10.times {|i|
      col = $etc_pm2.create_collections(:func=>lambda {|x,y,seed| Math.cos(seed+x+y)}, :ncollections=>1, :seed=>i)
      $etc_pm2.report :collections=>col, :export=>"log/trace_%02d.xml" % i
      sleep 1
    }
    $etc_pm2.report_event "PA:PM2/SPA:SIDE1/ProcessingCompleted_Coronus", @@context.merge({ :export=>"log/eve_proccomp.xml" })

    $log.info "Check E3 ..."
    assert wait_for(:sleeptime=>30) {
      $e3test.verify_run("#{@@eqp}_PM2_SIDE1", 1, ["ProcessStarted","ProcessCompleted"], @@context, txtime)
    }
  end

  def test04_data_reporting_datatypes
    self._run_process_start_completed(
      __method__,
      { 
      "PA:[PA=ProcessingChamber]/QA_String_Var1" => "string",
      "PA:[PA=ProcessingChamber]/QA_Int_Var1" => "signed integer",
      "PA:[PA=ProcessingChamber]/QA_UInt_Var1" => "unsigned integer",
      "PA:[PA=ProcessingChamber]/QA_Bool_Var1" => "boolean",
      "PA:[PA=ProcessingChamber]/QA_Bin_Var1" => "binary",
      "PA:[PA=ProcessingChamber]/QA_Float_Var1" => "float",
      "PA:[PA=ProcessingChamber]/QA_Double_Var1" => "double"
    })
  end

  def _run_process_start_completed(method, vars)
    self._init_plan(method, vars)
    txtime = Time.now
    $etc_pm2.report_event "PA:PM2/ProcessingStarted_Coronus", @@context.merge({ :export=>"log/#{method}_procstart.xml" })
    $data = []
    10.times {|i|
      col = $etc_pm2.create_collections(:func=>lambda {|x,y,seed| Math.cos(seed+x+y)}, :ncollections=>3, :seed=>i)
      $etc_pm2.report :collections=>col, :export=>"log/#{method}_trace_%02d.xml" % i
      $data << col
      sleep 1
    }
    $etc_pm2.report_event "PA:PM2/ProcessingCompleted_Coronus", @@context.merge({ :export=>"log/#{method}_proccomp.xml" })

    $log.info "Check E3 ..."
    assert wait_for(:sleeptime=>30) {
      $e3test.verify_run("#{@@eqp}_PM2", 1, ["ProcessStarted","ProcessCompleted"], @@context, txtime)
    }
    var_names = vars.keys().map {|v| v.split("/").last }
    assert $e3test.verify_data("#{@@eqp}_PM2", var_names, $data, txtime)
  end

  def test05_data_reporting_order
    vars = { "PA:[PA=ProcessingChamber]/PGC__CDAPressure_AI" => "float" }

    self._init_plan(__method__, vars)

    txtime = Time.now
    $etc_pm2.report_event "PA:PM2/ProcessingStarted_Coronus", @@context.merge({ :export=>"log/eve_procstart.xml" })
    $data = []
    1000.times {|i|
      col = $etc_pm2.create_collections(:func=>lambda {|x,y,seed| Math.cos(seed/50.0+x+y)}, :ncollections=>1, :seed=>i)
      $data << col
      sleep 1
    }
    $data.reverse_each.each_with_index {|col,i|
      $etc_pm2.report :collections=>col #, :export=>"log/trace_%02d.xml" % i
      
    }
    $etc_pm2.report_event "PA:PM2/ProcessingCompleted_Coronus", @@context.merge({ :export=>"log/eve_proccomp.xml" })

    $log.info "Check E3 ..."
    assert wait_for(:sleeptime=>30) {
      $e3test.verify_run("#{@@eqp}_PM2", 1, ["ProcessStarted","ProcessCompleted"], @@context, txtime)
    }
    assert $e3test.verify_data("#{@@eqp}_PM2", "PGC__CDAPressure_AI", $data, txtime)
  end


  def test10_lot_event_reporting
    variable = "PH__Wert"
    vars = { "#{variable}" => "float" }
    context = {}
    context[:cj] = @@context[:cj]
    context[:lot] = @@context[:lot]

    self._init_plan(__method__, vars)
    data = []
    txtime = Time.now
    $etc_pm2.report_event "LotStarted_Coronus", context.merge({ :export=>"log/eve_lotstarted.xml" })
    100.times {|i|
      col = $etc_pm2.create_collections(:func=>lambda {|x,y,seed| Math.cos(seed/25.0+x+y)}, :ncollections=>1, :seed=>i)
      $etc_pm2.report :collections=>col, :export=>"log/trace_%02d.xml" % i
      data << col
      sleep 1
    }
    $etc_pm2.report_event "LotCompleted_Coronus", context.merge({ :export=>"log/eve_lotcomp.xml" })

    $log.info "Check E3 ..."
    assert wait_for(:sleeptime=>30) {
      $e3test.verify_run("#{@@eqp}", 1, ["LotStarted","LotCompleted"], context, txtime)
    }
    assert $e3test.verify_data("#{@@eqp}", variable, data, txtime)
  end
  
  def test90_event_reporting_loop
    vars = { "PA:[PA=ProcessingChamber]/PGC__CDAPressure_AI" => "float" }

    self._init_plan(__method__, vars)
    
    txtime = Time.now
    count = 1000
    count.times {
      $etc_pm2.report_event "PA:PM2/ProcessingStarted_Coronus", @@context.merge({ :export=>"log/eve_procstart.xml" })
      1.times {|i|
        $etc_pm2.report_event "PA:PM2/RecipeStepStarted_Coronus"#, :export=>"log/eve_recipestart.xml"
        col = $etc_pm2.create_collections(:func=>lambda {|x,y,seed| Math.cos(seed+x+y)}, :ncollections=>1, :seed=>i)
        $etc_pm2.report :collections=>col#, :export=>"log/trace_%02d.xml" % i
        #data << col
        sleep 1
        $etc_pm2.report_event "PA:PM2/RecipeStepCompleted_Coronus"#, :export=>"log/eve_recipecomp.xml"
      }
      $etc_pm2.report_event "PA:PM2/ProcessingCompleted_Coronus", @@context.merge({ :export=>"log/eve_proccomp.xml" })
    }
    $log.info "Check E3 ..."
    assert wait_for(:sleeptime=>30) {
      $e3test.verify_run("#{@@eqp}_PM2", count, ["ProcessStarted","ProcessStepStarted", "ProcessStepCompleted", "ProcessCompleted"], 
        @@context, txtime)
    }
  end

  def test91_event_reporting_loop_2
    vars = { "PA:[PA=ProcessingChamber]/PGC__CDAPressure_AI" => "float" }

    self._init_plan(__method__, vars)
    context = @@context.clone

    txtime = Time.now
    pcount = 1
    mcount = 2
    scount = 10
    pcount.times {
      $etc_pm2.report_event "PA:PM2/ProcessingStarted_Coronus", context.merge({ :export=>"log/eve_procstart.xml" })
      mcount.times {
        $etc_pm2.report_event "PA:PM2/MaterialStarted_Coronus", context.merge({ :export=>"log/eve_matstart.xml" })
        scount.times {|i|
        $etc_pm2.report_event "PA:PM2/RecipeStepStarted_Coronus", context.merge({:recipestepno=>(i+1).to_s})#, :export=>"log/eve_recipestart.xml"
        }

        $etc_pm2.report_event "PA:PM2/MaterialCompleted_Coronus", context.merge({ :export=>"log/eve_matcomp.xml" })
      }
      
      $etc_pm2.report_event "PA:PM2/ProcessingCompleted_Coronus", context.merge({ :export=>"log/eve_proccomp.xml" })
    }
    $log.info "Check E3 ..."
    assert wait_for(:sleeptime=>30) {
      $e3test.verify_run("#{@@eqp}_PM2", count, ["ProcessStarted"] + ["ProcessStepStarted"] * scount + ["ProcessCompleted"],
        context, txtime)
      #["PGC__CDAPressure_AI"], data,
    }
  end

  def test92_data_reporting_loop
    vars = { "PA:[PA=ProcessingChamber]/PGC__CDAPressure_AI" => "float" }

    self._init_plan(__method__, vars)
    context = @@context.clone


    data = []
    txtime = Time.now
    count = 10
    data_c = 1000
    count.times {
      $etc_pm2.report_event "PA:PM2/ProcessingStarted_Coronus", context.merge({ :export=>"log/eve_procstart.xml" })
      data_c.times {|i|
        #$etc_pm2.report_event "PA:PM2/RecipeStepStarted_Coronus"#, :export=>"log/eve_recipestart.xml"
        col = $etc_pm2.create_collections(:func=>lambda {|x,y,seed| Math.cos((seed+x+y)/1000.0)}, :ncollections=>1, :seed=>i)
        $etc_pm2.report :collections=>col#, :export=>"log/trace_%02d.xml" % i
        data << col
        #sleep 1
        #$etc_pm2.report_event "PA:PM2/RecipeStepCompleted_Coronus"#, :export=>"log/eve_recipecomp.xml"
      }
      $etc_pm2.report_event "PA:PM2/ProcessingCompleted_Coronus", context.merge({ :export=>"log/eve_proccomp.xml" })
    }
    $log.info "Check E3 ..."
    assert wait_for(:sleeptime=>30) {
      $e3test.verify_run("#{@@eqp}_PM2", count, ["ProcessStarted", "ProcessCompleted"],
        context, txtime)
    }
    $e3test.verify_data("#{@@eqp}_PM2", "PGC__CDAPressure_AI", data, txtime)
  end


  def XXXtest02_event_reporting
    $eas.store_plan :export=>"log/abstr_map.xml"
    $etc_pm2.plan_mapping :export=>"log/plan_map.xml"

    #$etc_pm2.report_event "LotStarted_Coronus", :export=>"log/eve_lotstart.xml"
    $etc_pm2.report_event "PA:PM2/ProcessingStarted_Coronus", :export=>"log/eve_procstart.xml"
    1.times {|i|
      $etc_pm2.report_event "PA:PM2/RecipeStepStarted_Coronus", :export=>"log/eve_recipestart.xml"
      col = $etc_pm2.create_collections(:ncollections=>1, :seed=>i)
      $etc_pm2.report :collections=>col
      sleep 1
      $etc_pm2.report_event "PA:PM2/RecipeStepCompleted_Coronus", :export=>"log/eve_recipecomp.xml"
    }
    $etc_pm2.report_event "PA:PM2/ProcessingCompleted_Coronus", :export=>"log/eve_proccomp.xml"
    #$etc_pm2.report_event "LotCompleted_Coronus", :export=>"log/eve_lotcomp.xml"

  end
  
end