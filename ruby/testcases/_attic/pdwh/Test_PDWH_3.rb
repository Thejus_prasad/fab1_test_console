# encoding: UTF-8

=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: Thomas Klose and Steffen Steidten, 2012-09-14
=end

require 'RubyTestCase'
require 'pdwhtest'

# test cases for PDWH 2.4 April 2015
class Test_PDWH_3 < RubyTestCase
  
  def self.startup
    super
    $pdwhtest = PDWH::Test.new($env, sv: $sv)
    @@lots = {}
  end

  def self.shutdown
    $log.info "test lots:\n#{@@lots.pretty_inspect}"
  end


  # BANKIN

  def test3001_bankin
    assert lots = 4.times.collect {$pdwhtest.new_lot}, "error creating test lots"
    @@lots['test3001'] = lots
    lots[0..-1].each {|lot| assert $sv.lot_opelocate(lot, :last)}
    sleep 60
    lots[1..-1].each {|lot| assert $sv.lot_bankin(lot)}
    sleep 60
    lots[2..-1].each {|lot| assert $sv.lot_bankin_cancel(lot)}
    sleep 60
    lots[3..-1].each {|lot| assert $sv.lot_bankin(lot)}
    $log.info "test lots: #{lots}"
  end
  
  # SHIP

  def test3011_PO_ship_OT_SHIP_NS
    assert lot = $pdwhtest.new_lot(nwafers: 2, sublottype: 'PO')
    @@lots['test3011'] = lot
    assert_equal 0, $sv.lot_opelocate(lot, :last)
    $sv.lot_bankin(lot)
    assert_equal 0, $sv.lot_bank_move(lot, 'OT-SHIP-NS')
    assert_equal 0, $sv.lot_ship(lot, 'OT-SHIP-NS')
    li = $sv.lot_info(lot, wafers: true)
    $log.info "processed lot #{lot}, wafer #{li.wafers.collect {|w| w.wafer}.inspect}" 
  end

  def test3012_PR_branch
    assert lot = $pdwhtest.new_lot(nwafers: 2, sublottype: 'PR', route: 'C02-TKEY.01', product: 'TKEY01.A0')
    @@lots['test3012'] = lot
    assert $sv.claim_process_lot(lot, happylot: true)
    assert_equal 0, $sv.lot_opelocate(lot, nil, op: 'TKEYBUMPSHIPINIT.01')
    assert_equal 0, $sv.lot_branch(lot, "TKEY-BUMP.01", return_opNo: '6350.1000')
    li = $sv.lot_info(lot, wafers: true)
    $log.info "processed lot #{lot}, wafer #{li.wafers.collect {|w| w.wafer}.inspect}" 
  end

  # Starts

  def test3021_BUM_FAB_PR
    assert lot = $pdwhtest.new_lot(route: "C02-1431.01", product: "SHELBY21AE.QA", sublottype: "PR", nwafers: 2)
    @@lots['test3021'] = lot
    $sv.lot_opelocate(lot, nil, op: 'FINA-FWETCLN-0921.01')
    assert_equal "TML", $sv.lot_info(lot).stage
    assert $sv.claim_process_lot(lot, happylot: true)
    li = $sv.lot_info(lot, wafers: true)
    assert_equal "TTB", li.stage
    $log.info "processed lot #{lot}, wafers #{li.wafers.collect {|w| w.wafer}.inspect}" 
  end
  
  def test3022_BUM_FAB_PR_RAWPREPARED
    assert lot = $pdwhtest.new_lot(route: "C02-1431.01", srcproduct: 'RAW-PREPARED.01', product: "SHELBY21AE.QA", sublottype: "PR", nwafers: 2)
    @@lots['test3022'] = lot
    $sv.lot_opelocate(lot, nil, op: 'FINA-FWETCLN-0921.01')
    assert_equal "TML", $sv.lot_info(lot).stage
    assert $sv.claim_process_lot(lot, happylot: true)
    li = $sv.lot_info(lot, wafers: true)
    assert_equal "TTB", li.stage
    $log.info "processed lot #{lot}, wafers #{li.wafers.collect {|w| w.wafer}.inspect}" 
  end
  
  def test3023_FAB_FAB_PR
    assert lot = $pdwhtest.new_lot(route: "C02-1431.01", product: "SHELBY21AE.QA", sublottype: "PR", nwafers: 2, srclotsleep: 5)
    @@lots['test3023'] = lot
    assert $sv.claim_process_lot(lot, happylot: true)
    li = $sv.lot_info(lot, wafers: true)
    $log.info "processed lot #{lot}, wafers #{li.wafers.collect {|w| w.wafer}.inspect}" 
  end
  
  def test3024_FAB_FAB_PR_RAWPREPARED
    assert lot = $pdwhtest.new_lot(route: "C02-1431.01", srcproduct: 'RAW-PREPARED.01', product: "SHELBY21AE.QA", sublottype: "PR", nwafers: 2)
    @@lots['test3024'] = lot
    assert $sv.claim_process_lot(lot, happylot: true)
    li = $sv.lot_info(lot, wafers: true)
    $log.info "processed lot #{lot}, wafers #{li.wafers.collect {|w| w.wafer}.inspect}" 
  end
  
  def test3025_FAB_FAB_PR_WAFERLOT
    assert srclot = $pdwhtest.new_lot(route: "UTRT001.01", product: "UT-PRODUCT000.01")
    assert_equal 0, $sv.lot_opelocate(srclot, :last)
    assert_equal 0, $sv.lot_bank_move(srclot, 'ON-RAW')
    assert lot = $pdwhtest.new_lot(route: "C02-1431.01", product: "SHELBY21AE.QA", srclot: srclot, sublottype: "PR", nwafers: 2)
    @@lots['test3025'] = lot
    assert $sv.claim_process_lot(lot, happylot: true)
    li = $sv.lot_info(lot, wafers: true)
    $log.info "processed lot #{lot}, wafers #{li.wafers.collect {|w| w.wafer}.inspect}" 
  end

  # misc, from sheet "Test Cases"

  def test3031_cj
    assert lot = $pdwhtest.new_lot(nwafers: 22)
    @@lots['test3031'] = lot
    assert $sv.claim_process_lot(lot, happylot: true, proctime: 60)
  end
  
  def test3032_multiple_holds
    sleeptime = 60
    assert lot = $pdwhtest.new_lot(nwafers: 10)
    @@lots['test3032'] = lot
    assert $sv.claim_process_lot(lot, happylot: true)
    sleep sleeptime
    assert_equal 0, $sv.lot_hold(lot, 'ENG')
    sleep sleeptime
    assert_equal 0, $sv.lot_hold(lot, 'X-DC')
    sleep sleeptime
    assert_equal 0, $sv.lot_hold_release(lot, 'ENG')
    sleep sleeptime
    assert_equal 0, $sv.lot_hold_release(lot, 'X-DC')
  end
  
  def test3033_bankin_out
    sleeptime = 60
    assert lot = $pdwhtest.new_lot(nwafers: 3)
    @@lots['test3033'] = lot
    assert $sv.claim_process_lot(lot, happylot: true)
    sleep sleeptime
    assert_equal 0, $sv.lot_opelocate(lot, :last)
    $sv.lot_bankin(lot)
    bank = $sv.lot_info(lot).bank
    refute_equal '', bank
    sleep sleeptime
    assert_equal 0, $sv.lot_bank_move(lot, 'OY-SHIP')
    sleep sleeptime
    assert_equal 0, $sv.lot_bank_move(lot, bank)
    sleep sleeptime
    assert_equal 0, $sv.lot_bankin_cancel(lot)
    sleep 5
    assert_equal 0, $sv.lot_opelocate(lot, -1)  # to prevent jCAP bankin
  end
  
  def test3034_bankin_hold
    sleeptime = 60
    assert lot = $pdwhtest.new_lot(nwafers: 18)
    @@lots['test3034'] = lot
    assert $sv.claim_process_lot(lot, happylot: true)
    sleep sleeptime
    assert_equal 0, $sv.lot_nonprobankin(lot, 'T-HOLD')
    sleep sleeptime
    assert_equal 0, $sv.lot_hold(lot, 'ENG')
    sleep sleeptime
    assert_equal 0, $sv.lot_nonprobankout(lot)
    sleep sleeptime
    assert_equal 0, $sv.lot_hold_release(lot, 'ENG')
  end

  def test3035_slr
    sleeptime = 60
    assert lot = $pdwhtest.new_lot(nwafers: 2)
    @@lots['test3035'] = lot
    eqp = $sv.lot_info(lot).opEqps.last
    assert cj = $sv.slr(eqp, lot: lot)
    sleep sleeptime
    assert_equal 0, $sv.slr_cancel(eqp, cj)
    sleep sleeptime
    assert $sv.claim_process_lot(lot, happylot: true, proctime: 60)
  end
  
  def test3036_running_hold
    sleeptime = 60
    assert lot = $pdwhtest.new_lot(nwafers: 16)
    @@lots['test3036'] = lot
    assert $sv.claim_process_lot(lot, happylot: true, proctime: 60, running_hold: true)
    sleep sleeptime
    assert_equal 0, $sv.lot_hold_release(lot, nil)
  end

  def test3037_hold_split
    sleeptime = 60
    assert lot = $pdwhtest.new_lot(nwafers: 10)
    @@lots['test3037'] = lot
    assert $sv.claim_process_lot(lot, happylot: true)
    sleep sleeptime
    assert_equal 0, $sv.lot_hold(lot, 'ENG')
    sleep sleeptime
    assert lc = $sv.lot_split(lot, 2, onhold: true)
    sleep sleeptime
    assert_equal 0, $sv.lot_hold_release(lc, nil)
    sleep sleeptime
    assert_equal 0, $sv.lot_hold_release(lot, nil)
    sleep sleeptime
    [lc, lot].each {|l| assert $sv.claim_process_lot(l, happylot: true)}
  end

  def test3038_bankin_split
    sleeptime = 60
    assert lot = $pdwhtest.new_lot(nwafers: 10)
    @@lots['test3038'] = lot
    assert $sv.claim_process_lot(lot, happylot: true)
    sleep sleeptime
    assert_equal 0, $sv.lot_nonprobankin(lot, 'T-HOLD')
    sleep sleeptime
    assert lc = $sv.lot_split(lot, 1, onhold: true)
    sleep sleeptime
    assert_equal 0, $sv.lot_nonprobankout(lc)
    sleep sleeptime
    assert_equal 0, $sv.lot_nonprobankout(lot)
  end

  def test3040_btfout
    # Tracken von 4 Losen �ber diese PD: BTF1OUT.01
    # a) Los1: OperationComplete an dieser PD 
    # b) Los2: GatePass an dieser PD 
    # c) Los3: ForceComp an dieser PD 
    # d) Los4: LocateForward an der Vorg�nger PD auf die BTF1OUT.01 PD; nochmaliges LocateForward auf eine andere PD
    
    res = {}
    assert lots = 4.times.collect {
      assert lot = $pdwhtest.new_lot(product: "PDWHPROD1.09", route: "P-PDWH-MAINPD.09")
      lot
    }, "error creating test lots"
    lot = lots.pop
    assert_equal 0, $sv.lot_opelocate(lot, "9800.1000") #BTF1OUT.01
    assert $sv.claim_process_lot(lot, happylot: true)
    res["ClaimProcessLot"] = lot
    
    lot = lots.pop
    assert_equal 0, $sv.lot_opelocate(lot, "9800.1000") #BTF1OUT.01
    assert_equal 0, $sv.lot_gatepass(lot)
    res["GatePass"] = lot
    
    lot = lots.pop
    assert_equal 0, $sv.lot_opelocate(lot, "9800.1000") #BTF1OUT.01
    assert $sv.claim_process_lot(lot, happylot: true, running_hold: true)
    res["ForceOpeComplete"] = lot
    
    lot = lots.pop
    assert_equal 0, $sv.lot_opelocate(lot, "4000.1300") #BTF1OUT.01 -1
    sleep 60
    assert_equal 0, $sv.lot_opelocate(lot, "9800.1000") #BTF1OUT.01
    sleep 60
    assert_equal 0, $sv.lot_opelocate(lot, +1) #BTF1OUT.01
    res["LocateForward"] = lot

    $log.info "\n#{res.pretty_inspect}"
  end
  
  def test3050_Bump_Starts_Raw_Prepared
    assert lot = $pdwhtest.new_lot(route: "C02-1431.01", srcproduct: 'RAW-PREPARED.01', product: "SHELBY21AE.QA", sublottype: "PR", nwafers: 4)
    assert $sv.claim_process_lot(lot, happylot: true)
    assert $sv.lot_opelocate(lot, "6150.1500")
    2.times {assert $sv.claim_process_lot(lot, happylot: true)}
    assert cl = $sv.lot_split(lot, 2)
    ##$log.info "sleep for 1 h"; sleep 3600
    sleep 60
    assert $sv.lot_opelocate(cl, "1000.100")
    assert $sv.claim_process_lot(cl, happylot: true)
    assert $sv.lot_opelocate(cl, "6150.1500")
    2.times {assert $sv.claim_process_lot(cl, happylot: true)}
    
    @@lots['test3050'] = [lot, cl]
  end
end
