=begin
moves a lot thrue the pDWH flow in itdc.
tasks
 - moves a lot thrue the flow
 - every move takes at least 5 minutes
 - changes the carrier at every stage
 - rnd based lot hold/ release, gatepass 
 
open questions 
 - route change, flow change. Is a change of a module PD a flow change or is a mfg_order_chg requiered?

load_run_testcase "Test_pDWH_lotmover", "itdc"
 
Author: Thomas Klose 2012-09-14
=end

require "RubyTestCase"

class Test_PDWH_lotmover < RubyTestCase
  Description = "Lot mover for pDWH route"

  @@mytime = 300   # normal run
  #@@mytime = 20    # fast run
  @@lot_hold_release = 5    # 5% probability for lot hold and lot release
  @@lot_schdl_change = 5    # 5% probability for route and product change
  
  
  def test10
    threads = []
    lots = 8      # lot count
    tools = {}
    
    lots.times {
      threads << Thread.new {
        l = start_lot()
        $sv.lot_opelocate(l, +1)
        lot_move(l)
        $sv.delete_lot_family(l, :delete=>false)
      }
      sleep @@mytime / 10 * 8
    }
    
    # wait for completion
    threads.each {|t| t.join}  
    $log.info("bye bye")
  end
  
  def lot_move(lot)
    stage = nil
    
    while true
      li = $sv.lot_info(lot)
      break if li.opNo == "6400.110" or li.status == "SCRAPPED"

      if stage != li.stage
        stage = li.stage
        carrier_change(lot)
      end
      
      lot_hold_release(lot) if rnd(@@lot_hold_release)
      route_change(lot) if rnd(@@lot_schdl_change)
      
      claim_process_lot(lot)      
    end
  end

  def lot_hold_release(lot)
    $sv.lot_hold(lot, "ENG")
    sleep @@mytime
    $sv.lot_hold_release(lot, nil)
  end
  
  def carrier_change(lot)
    cl = $sv.carrier_list(:status=>"AVAILABLE", :carrier=>"A%", :empty=>true)
    $sv.wafer_sort_req(lot, cl.first)
    li = $sv.lot_info(lot)
    $sv.carrier_xfer_status_change(li.carrier, "EO", li.opEqps[0]) if li.xfer_status != "EO"
    sleep @@mytime
  end
  
  
  def claim_process_lot(lot)
    begin
##      eqp, port = $sv._free_eqp_and_port(lot)
##      res = $sv.claim_process_lot(lot, :proctime=>@@mytime, :port=>port, :eqp=>eqp)    #proctime
      res = $sv.claim_process_lot(lot, slr: true, claim_carrier: true, proctime: @@mytime)
      sleep 10
    end while res == nil
  end
  
  def param_change(lot)
    
  end
  
  def route_change(lot)
    routes = "P-PDWH1.01 P-PDWH2.01 P-PDWH3.01".split
    product = "PDWHPROD1.01 PDWHPROD2.01 PDWHPROD3.01".split
    lroute = $sv.lot_info(lot).route
    newroute = (routes - [lroute]).shuffle.first
    $sv.schdl_change(lot, :route=>newroute, :product=>product[routes.index(newroute)])
    sleep @@mytime
  end
  
  def start_lot
    lot = nil
    #synchronize do
      lot = $sv.new_lot_release(:product=>"PDWHPROD1.01", :route=>"P-PDWH1.01", :stb=>true, :sublottype=>"PO")
    #end
    sleep 30
    $sv.lot_hold_release(lot, nil)
    li = $sv.lot_info(lot)
    $sv.carrier_xfer_status_change(li.carrier, "EO", li.opEqps[0]) if li.xfer_status != "EO"
    return lot
  end
  
  def rnd(percent)
    # returns randomly true
    return true if rand < percent / 100.0
  end 
end