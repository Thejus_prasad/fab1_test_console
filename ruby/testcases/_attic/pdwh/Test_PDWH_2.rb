=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: Thomas Klose and Steffen Steidten, 2012-09-14
=end

require 'RubyTestCase'
require 'pdwhtest'

class Test_PDWH_2 < RubyTestCase
  
  @@route = 'P-PDWH-MAINPD.08'
  @@product = 'PDWHPROD1.08'
  
  def self.lots
    @@lots
  end
  
  def self.startup
    super
    $pdwhtest = PDWH::Test.new($env, sv: $sv, route: @@route, product: @@product) unless $pdwhtest and $pdwhtest.env == $env
    @@lots ||= {}
  end
  
  def self.after_all_passed
  end
  
  # tests for MSR release 2.1
  
  def test2010_carrier_categories
    lot = $pdwhtest.new_lot(route: @@route, product: @@product)
    assert lot
    # process lot until HK is required
    assert_equal 0, $sv.lot_gatepass(lot)
    while $sv.lot_info(lot).cast_cat != 'HK'
      assert $sv.claim_process_lot(lot, happylot: true)
    end
    # process with carrier HK
    assert c = $sv._get_empty_carrier('%', carrier_category: 'HK'), "no empty HK carrier"
    assert_equal 0, $sv.wafer_sort_req(lot, c)
    assert $sv.claim_process_lot(lot, happylot: true)
    # branch, without required carrier category
    rte = $pdwhtest.subroutes[:branch][:dynamic][:nolayer]
    retop = $sv.lot_info(lot).opNo
    assert_equal 0, $sv.lot_branch(lot, rte, retop: retop, dynamic: true)
    # process on branch route
    while $sv.lot_info(lot).route == rte
      assert $sv.claim_process_lot(lot, happylot: true)
    end
    # process on main route
    assert $sv.claim_process_lot(lot, happylot: true)
    # branch, with required carrier category HK
    rte = $pdwhtest.subroutes[:branch][:dynamic][:layer]
    retop = $sv.lot_info(lot).opNo
    assert_equal 0, $sv.lot_branch(lot, rte, retop: retop, dynamic: true)
    # process on branch route
    while $sv.lot_info(lot).route == rte
      assert $sv.claim_process_lot(lot, happylot: true)
    end
    # process on main route until carrier category changes
    while $sv.lot_info(lot).cast_cat == 'HK'
      assert $sv.claim_process_lot(lot, happylot: true)
    end
    # process with carrier MOL
    assert c = $sv._get_empty_carrier('%', carrier_category: 'MOL'), "no empty HK carrier"
    assert_equal 0, $sv.wafer_sort_req(lot, c)
    assert $sv.claim_process_lot(lot, happylot: true)
    # branch, with required carrier category HK
    rte = $pdwhtest.subroutes[:branch][:dynamic][:layer]
    retop = $sv.lot_info(lot).opNo
    assert_equal 0, $sv.lot_branch(lot, rte, retop: retop, dynamic: true)
    # change carrier and process on branch route
    assert c = $sv._get_empty_carrier('%', carrier_category: 'HK'), "no empty HK carrier"
    assert_equal 0, $sv.wafer_sort_req(lot, c)
    while $sv.lot_info(lot).route == rte
      assert $sv.claim_process_lot(lot, happylot: true)
    end
    # process with carrier MOL on main route
    assert c = $sv._get_empty_carrier('%', carrier_category: 'MOL'), "no empty HK carrier"
    assert_equal 0, $sv.wafer_sort_req(lot, c)
    assert $sv.claim_process_lot(lot, happylot: true)
    #
    @@lots['2010'] = [lot]
  end
  
  # tests for MSR release 2.2
  def test2020_different_user_for_claim_process_lot()
    $svnox = SiView::MM.new($env, user: 'NOX-UNIT', password: :default) unless $svnox and $svnox.env == $env
    lot = $pdwhtest.new_lot(route: @@route, product: @@product)
    assert $sv.claim_process_lot(lot)
    assert $svnox.claim_process_lot(lot)    
  end
  
  def test2021_set_production_unit_for_pdwh_tools
    # get uparam for retest (change uparam)
    eqps = "PDWHA01 PDWHA02 PDWHA03 PDWHA04 PDWHA05 PDWHA06 PDWHA07 PDWHA08 PDWHA09 PDWHA10 " + 
           "PDWHA11 PDWHA12 PDWHA13 PDWHA14 PDWHA15 PDWHA16 PDWHA17 PDWHA18 PDWHA19 PDWHA20 " + 
           "PDWHM40 PDWHM41 PDWHP10 PDWHP11 PDWHP20 PDWHP21 PDWHP30 PDWHP31"
    uparams = "QA1 QA2 QA3".split()
    eqps = eqps.split()
    startparam = uparams.first
    res = $sv.user_parameter("Eqp", eqps.first())
    for p in res
      startparam = p.value if p.name == "OperatingPU"
    end
    for p in uparams
      break if startparam == uparams.last
      uparams.rotate!
    end
    eqps.each {|eqp|
      $sv.user_parameter_set_verify("Eqp", eqp, "OperatingPU", uparams.first)
      uparams.rotate!}
    assert true
  end
  
  def test2022_branch_and_scrap_on_postfab_pd
    st = 10
    lot = $pdwhtest.new_lot(route: "C02-TKEY.01", product: "TKEY01.A0")
    li = $sv.lot_info(lot)
    assert $sv.lot_experiment(lot, li.nwafers, "TKEY-SORT.01", "6350.1000", "6500.1000")
    sleep st
    assert $sv.lot_opelocate(lot, "6350.1000")
    sleep st
    assert $sv.scrap_wafers(lot)    
  end
  
  
  def test2099_summary
    $log.info "lots:\n#{@@lots.pretty_inspect}"
  end
end
