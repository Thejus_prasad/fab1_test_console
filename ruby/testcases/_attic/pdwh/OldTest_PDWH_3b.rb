# encoding: UTF-8

=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: Thomas Klose and Steffen Steidten, 2012-09-14
=end

require 'RubyTestCase'
require 'pdwhtest'

class Test_PDWH_3b < RubyTestCase
  Description = "execute PDWH test scenarios"
  
  @@proc_params = {:notify=>false, :mode=>"Off-Line-1", :claim_carrier=>true, :proctime=>1, :memo=>"QAäöüß"}
  @@opNo_meas = "1000.1000"
  @@opNo_proc = "2000.1000"
  @@opNo_other = "9900.1000"
  @@opNo_DSF = "9900.1000"

  def self.lots
    @@lots
  end
  
  def setup
    super
    $pdwhtest = PDWH::Test.new($env, :sv=>$sv) unless $pdwhtest and $pdwhtest.env == $env
    @@lots ||= {}
  end
  
  
  # Sprint 3b test cases
  
  # Starts SubKPI
  
  def test321_start_monitorlot
    # create monitorlot
    srcproduct = "UT-RAW.00"
    startbank = "UT-RAW"
    product = "PDWH-MN-321.01"
    lottype = "Equipment Monitor"
    sublottype = "Equipment Monitor"
    srclot = $sv.vendorlot_receive(startbank, srcproduct, 1, :prepare=>true, :carrier=>"PDWH%")
    lot = $sv.stb_controllot(product, lottype, :sublottype=>sublottype, :srclot=>srclot, :nwafers=>1)
    assert lot
    #
    li = $sv.lot_info(lot, :wafers=>true)
    assert_equal "LOTSTART.01", li.op
    assert $sv.claim_process_lot(lot, @@proc_params)
    $log.info "processed lot #{lot}, wafer #{li.wafers.collect {|w| w.wafer}.inspect}" 
  end
  
  def test322_LOTSTARTBEOL
    lot = $pdwhtest.new_lot(:route=>"C02-1070.01", :product=>"5675FA.R2", :sublottype=>"PR", :nwafers=>1)
    assert lot
    li = $sv.lot_info(lot, :wafers=>true)
    assert_equal "LOTSTARTBEOL.01", li.op
    assert $sv.claim_process_lot(lot, @@proc_params)
    $log.info "processed lot #{lot}, wafer #{li.wafers.collect {|w| w.wafer}.inspect}" 
  end

  def test323_LOTSTART_PZ
    lot = $pdwhtest.new_lot(:route=>"ITDC-LRNDM.01", :product=>"ITDC-LRNDM.01", :sublottype=>"PZ", :nwafers=>1)
    assert lot
    li = $sv.lot_info(lot, :wafers=>true)
    assert_equal "LOTSTART.01", li.op
    assert_equal "PZ", li.sublottype
    assert $sv.claim_process_lot(lot, @@proc_params)
    $log.info "processed lot #{lot}, wafer #{li.wafers.collect {|w| w.wafer}.inspect}" 
  end

  def test324_2_OpeStart
    lot = $pdwhtest.new_lot(:route=>"ITDC-LRNDM.01", :product=>"ITDC-LRNDM.01", :sublottype=>"PR", :nwafers=>3)
    assert lot
    lc1 = $sv.lot_split(lot, 1)
    assert $sv.claim_process_lot(lot, @@proc_params.merge(:slr=>true))
    li = $sv.lot_info(lot, :wafers=>true)
    $log.info "processed lot #{lot}, wafer #{li.wafers.collect {|w| w.wafer}.inspect}" 
    lc2 = $sv.lot_split(lot, 1)
    assert_equal 0, $sv.lot_opelocate(lc2, -1)
    assert_equal 0, $sv.lot_merge(lc1, lc2)
    assert $sv.claim_process_lot(lc1, @@proc_params.merge(:slr=>true))
    li = $sv.lot_info(lc1, :wafers=>true)
    $log.info "processed lot #{lc1}, wafer #{li.wafers.collect {|w| w.wafer}.inspect}" 
  end

  def test325_GatePass
    lot = $pdwhtest.new_lot(:route=>"ITDC-LRNDM.01", :product=>"ITDC-LRNDM.01", :sublottype=>"PR", :nwafers=>3)
    assert lot
    li = $sv.lot_info(lot, :wafers=>true)
    assert_equal "LOTSTART.01", li.op
    assert_equal 0, $sv.lot_gatepass(lot)
    $log.info "processed lot #{lot}, wafer #{li.wafers.collect {|w| w.wafer}.inspect}" 
  end
  
  def test327_BUM_BUM
    lot = $pdwhtest.new_lot(:route=>"C02-1431.01", :product=>"SHELBY3CC.01", :sublottype=>"PR", :nwafers=>1)
    assert lot
    $sv.lot_opelocate(lot, "98255.1000")
    li = $sv.lot_info(lot, :wafers=>true)
    assert_equal "TTB", li.stage
    assert $sv.claim_process_lot(lot, @@proc_params)
    li = $sv.lot_info(lot, :wafers=>true)
    assert_equal "UBM", li.stage
    $log.info "processed lot #{lot}, wafer #{li.wafers.collect {|w| w.wafer}.inspect}" 
  end

  def test328_FAB_FAB
    lot = $pdwhtest.new_lot(:route=>"C02-1431.01", :product=>"SHELBY3CC.01", :sublottype=>"PR", :nwafers=>1)
    assert lot
    $sv.lot_opelocate(lot, "6150.1450")
    li = $sv.lot_info(lot, :wafers=>true)
    assert_equal "TML", li.stage
    assert $sv.claim_process_lot(lot, @@proc_params)
    li = $sv.lot_info(lot, :wafers=>true)
    assert_equal "TML", li.stage
    $log.info "processed lot #{lot}, wafer #{li.wafers.collect {|w| w.wafer}.inspect}" 
  end

  def test329_BUM_FAB
    lot = $pdwhtest.new_lot(:route=>"C02-1431.01", :product=>"SHELBY3CC.01", :sublottype=>"PR", :nwafers=>2)
    assert lot
    $sv.lot_opelocate(lot, "6150.1500")
    li = $sv.lot_info(lot, :wafers=>true)
    assert_equal "TML", li.stage
    assert $sv.claim_process_lot(lot, @@proc_params.merge(:running_hold=>true))
    li = $sv.lot_info(lot, :wafers=>true)
    assert_equal "TTB", li.stage
    $log.info "processed lot #{lot}, wafer #{li.wafers.collect {|w| w.wafer}.inspect}" 
  end
  
  
  # Shipments SubKPI
  
  def test331_RD_ship
    lot = $pdwhtest.new_lot(:nwafers=>1, :sublottype=>"RD")
    assert lot
    $sv.lot_opelocate(lot, "last")
    $sv.lot_bankin(lot)
    assert_equal 0, $sv.lot_bank_move(lot, "OT-SHIP-NS")
    assert_equal 0, $sv.lot_ship(lot, "OT-SHIP-NS")
    li = $sv.lot_info(lot, :wafers=>true)
    $log.info "processed lot #{lot}, wafer #{li.wafers.collect {|w| w.wafer}.inspect}" 
  end

  def test332_PO_branch
    lot = $pdwhtest.new_lot(:nwafers=>1, :sublottype=>"PO")
    assert lot
    assert $sv.claim_process_lot(lot, @@proc_params)
    assert_equal 0, $sv.lot_branch(lot, "e2PDWH-DSTAGEID.01", :dynamic=>true)
    li = $sv.lot_info(lot, :wafers=>true)
    $log.info "processed lot #{lot}, wafer #{li.wafers.collect {|w| w.wafer}.inspect}" 
  end

  def test333_PO_ship_ON_RECLAIM
    lot = $pdwhtest.new_lot(:nwafers=>1, :sublottype=>"PO", :route=>"C02-TKEY.01", :product=>"TKEY01.A0")
    assert lot
    $sv.lot_opelocate(lot, "last")
    $sv.lot_bankin(lot)
    assert_equal 0, $sv.lot_bank_move(lot, "ON-RECLAIM")
    assert_equal 0, $sv.lot_ship(lot, "ON-RECLAIM")
    li = $sv.lot_info(lot, :wafers=>true)
    $log.info "processed lot #{lot}, wafer #{li.wafers.collect {|w| w.wafer}.inspect}" 
  end

  def test334_PO_ship_OT_SHIP_NS
    lot = $pdwhtest.new_lot(:nwafers=>2, :sublottype=>"PO")
    assert lot
    $sv.lot_opelocate(lot, "last")
    $sv.lot_bankin(lot)
    assert_equal 0, $sv.lot_bank_move(lot, "OT-SHIP-NS")
    assert_equal 0, $sv.lot_ship(lot, "OT-SHIP-NS")
    li = $sv.lot_info(lot, :wafers=>true)
    $log.info "processed lot #{lot}, wafer #{li.wafers.collect {|w| w.wafer}.inspect}" 
  end

  def test335_PR_branch
    lot = $pdwhtest.new_lot(:nwafers=>3, :sublottype=>"PR", :route=>"C02-TKEY.01", :product=>"TKEY01.A0")
    assert lot
    assert $sv.claim_process_lot(lot, @@proc_params)
    assert_equal 0, $sv.lot_opelocate(lot, nil, :op=>"TKEYBUMPSHIPINIT.01")
    assert_equal 0, $sv.lot_branch(lot, "TKEY-BUMP.01", :return_opNo=>"6350.1000")
    li = $sv.lot_info(lot, :wafers=>true)
    $log.info "processed lot #{lot}, wafer #{li.wafers.collect {|w| w.wafer}.inspect}" 
  end

  def test336_PR_branch_BY_LOCASS
    lot = $pdwhtest.new_lot(:nwafers=>2, :sublottype=>"PR")
    assert lot
    assert $sv.claim_process_lot(lot, @@proc_params)
    assert_equal 0, $sv.lot_branch(lot, "BY-LOCASS-BONDING.01", :dynamic=>true)
    li = $sv.lot_info(lot, :wafers=>true)
    $log.info "processed lot #{lot}, wafer #{li.wafers.collect {|w| w.wafer}.inspect}" 
  end

  def test337_PO_ship_cancel_split_ship
    lot = $pdwhtest.new_lot(:nwafers=>2, :sublottype=>"PO")
    assert lot
    $sv.lot_opelocate(lot, "last")
    $sv.lot_bankin(lot)
    assert_equal 0, $sv.lot_bank_move(lot, "OT-SHIP-NS")
    assert_equal 0, $sv.lot_ship(lot, "OT-SHIP-NS")
    assert_equal 0, $sv.lot_ship_cancel(lot, "OT-SHIP-NS")
    # split and ship parent (again)
    lc = $sv.lot_split(lot, 1)
    assert lc
    assert_equal 0, $sv.lot_ship(lot, "OT-SHIP-NS")
    # change sublottype and ship child (again)
    $sv.lot_mfgorder_change(lc, :sublottype=>"PR")
    assert_equal "PR", $sv.lot_info(lc).sublottype
    assert_equal 0, $sv.lot_ship(lc, "OT-SHIP-NS")
    #
    li = $sv.lot_info(lot, :wafers=>true)
    $log.info "processed lot #{lot}, wafer #{li.wafers.collect {|w| w.wafer}.inspect}" 
    li = $sv.lot_info(lc, :wafers=>true)
    $log.info "processed lot #{lc}, wafer #{li.wafers.collect {|w| w.wafer}.inspect}" 
  end

  # Outs
  
  def test341_monitor_lot
    # create monitorlot
    srcproduct = "UT-RAW.00"
    startbank = "UT-RAW"
    product = "PDWH-MN-341.01"
    lottype = "Equipment Monitor"
    sublottype = "Equipment Monitor"
    srclot = $sv.vendorlot_receive(startbank, srcproduct, 1, :prepare=>true, :carrier=>"PDWH%")
    lot = $sv.stb_controllot(product, lottype, :sublottype=>sublottype, :srclot=>srclot, :nwafers=>1)
    assert lot
    #
    assert_equal 0, $sv.lot_opelocate(lot, "100.500")
    assert $sv.claim_process_lot(lot, @@proc_params)
    li = $sv.lot_info(lot, :wafers=>true)
    $log.info "processed lot #{lot}, wafer #{li.wafers.collect {|w| w.wafer}.inspect}" 
  end
  
  def test342_PR_rework
    lot = "UXYK33121.000" #$pdwhtest.new_lot(:nwafers=>1, :sublottype=>"PR")
    assert lot
    assert_equal 0, $sv.lot_opelocate(lot, "3000.1000")
    assert_equal 0, $sv.lot_rework(lot, "P-PDWH-DRWK-STAGEID.02", :return_opNo=>"2000.1300", :dynamic=>true)
    assert_equal 0, $sv.lot_opelocate(lot, "1000.1400")
    assert $sv.claim_process_lot(lot, @@proc_params)
    li = $sv.lot_info(lot, :wafers=>true)
    assert_equal "P-PDWH-DRWK-STAGEID.02", li.route
    assert_equal "2000.1000", li.opNo
    $log.info "processed lot #{lot}, wafer #{li.wafers.collect {|w| w.wafer}.inspect}" 
  end

  def test343_BUM_FAB_PassCount_2
    lot = $pdwhtest.new_lot(:route=>"C02-1431.01", :product=>"SHELBY3CC.01", :sublottype=>"PO", :nwafers=>1)
    assert lot
    li = nil
    2.times {
      $sv.lot_opelocate(lot, "6150.1500")
      li = $sv.lot_info(lot, :wafers=>true)
      assert_equal "TML", li.stage
      assert $sv.claim_process_lot(lot, @@proc_params)
      li = $sv.lot_info(lot, :wafers=>true)
      assert_equal "TTB", li.stage
    }
    $log.info "processed lot #{lot}, wafer #{li.wafers.collect {|w| w.wafer}.inspect}" 
  end
  
  def test344_BUM_FAB_LTB
    lot = $pdwhtest.new_lot(:route=>"C02-1431LTB.01", :product=>"SHELBY3CCLTB.01", :sublottype=>"PO", :nwafers=>1)
    assert lot
    $sv.lot_opelocate(lot, "6150.1500")
    li = $sv.lot_info(lot, :wafers=>true)
    assert_equal "TML", li.stage
    assert $sv.claim_process_lot(lot, @@proc_params)
    li = $sv.lot_info(lot, :wafers=>true)
    assert_equal "TTB", li.stage
    assert_equal "LTB", $sv.user_data_route(li.route)["RouteMetallayer"]
    $log.info "processed lot #{lot}, wafer #{li.wafers.collect {|w| w.wafer}.inspect}" 
  end
  
  def test345_BUM_FAB_SCRAP
    lot = $pdwhtest.new_lot(:route=>"C02-1431.01", :product=>"SHELBY3CC.01", :sublottype=>"SCRAP", :nwafers=>1)
    assert lot
    $sv.lot_opelocate(lot, "6150.1500")
    li = $sv.lot_info(lot, :wafers=>true)
    assert_equal "TML", li.stage
    assert $sv.claim_process_lot(lot, @@proc_params)
    li = $sv.lot_info(lot, :wafers=>true)
    assert_equal "TTB", li.stage
    $log.info "processed lot #{lot}, wafer #{li.wafers.collect {|w| w.wafer}.inspect}" 
  end
  
  def test346_BUM_FAB_PR
    lot = $pdwhtest.new_lot(:route=>"C02-1431.01", :product=>"SHELBY3CC.01", :sublottype=>"PR", :nwafers=>2)
    assert lot
    $sv.lot_opelocate(lot, "6150.1500")
    li = $sv.lot_info(lot, :wafers=>true)
    assert_equal "TML", li.stage
    assert $sv.claim_process_lot(lot, @@proc_params)
    li = $sv.lot_info(lot, :wafers=>true)
    assert_equal "TTB", li.stage
    $log.info "processed lot #{lot}, wafers #{li.wafers.collect {|w| w.wafer}.inspect}" 
  end
  
  def test347_BUM_FAB_PR_Force
    lot = $pdwhtest.new_lot(:route=>"C02-1431.01", :product=>"SHELBY3CC.01", :sublottype=>"PR", :nwafers=>2)
    assert lot
    $sv.lot_opelocate(lot, "6150.1500")
    li = $sv.lot_info(lot, :wafers=>true)
    assert_equal "TML", li.stage
    assert $sv.claim_process_lot(lot, @@proc_params.merge(:running_hold=>true))
    li = $sv.lot_info(lot, :wafers=>true)
    assert_equal "TTB", li.stage
    $log.info "processed lot #{lot}, wafers #{li.wafers.collect {|w| w.wafer}.inspect}" 
  end
  
  def test348_BUM_FAB_PR_Locate
    lot = $pdwhtest.new_lot(:route=>"C02-1431.01", :product=>"SHELBY3CC.01", :sublottype=>"PR", :nwafers=>2)
    assert lot
    $sv.lot_opelocate(lot, "6150.1500")
    li = $sv.lot_info(lot, :wafers=>true)
    assert_equal "TML", li.stage
    assert_equal 0, $sv.lot_opelocate(lot, 1)
    li = $sv.lot_info(lot, :wafers=>true)
    assert_equal "TTB", li.stage
    $log.info "processed lot #{lot}, wafers #{li.wafers.collect {|w| w.wafer}.inspect}" 
  end
  
  def test349_BUM_FAB_PR_GatePass
    lot = $pdwhtest.new_lot(:route=>"C02-1431.01", :product=>"SHELBY3CC.01", :sublottype=>"PR", :nwafers=>2)
    assert lot
    $sv.lot_opelocate(lot, "6150.1500")
    li = $sv.lot_info(lot, :wafers=>true)
    assert_equal "TML", li.stage
    assert_equal 0, $sv.lot_gatepass(lot)
    li = $sv.lot_info(lot, :wafers=>true)
    assert_equal "TTB", li.stage
    $log.info "processed lot #{lot}, wafers #{li.wafers.collect {|w| w.wafer}.inspect}" 
  end
  
  def test350_DSF_BankIn
    lot = $pdwhtest.new_lot(:route=>"A-AP-MAINPD.01", :product=>"A-AP-PRODUCT.01", :sublottype=>"PR", :nwafers=>1)
    assert lot
    $sv.lot_opelocate(lot, "1300.1000")
    $sv.lot_bankin(lot)
    li = $sv.lot_info(lot, :wafers=>true)
    assert_equal "DSF", li.stage
    assert_equal "InBank", li.states["Lot Inventory State"]
    $log.info "processed lot #{lot}, wafers #{li.wafers.collect {|w| w.wafer}.inspect}" 
  end
  
  def test350a_DSF_BankIn
    lot = $pdwhtest.new_lot(:route=>"A-AP-MAINPD.01", :product=>"A-AP-PRODUCT.01", :sublottype=>"PR", :nwafers=>2)
    assert lot
    $sv.lot_opelocate(lot, "1300.1000")
    $sv.lot_bankin(lot)
    li = $sv.lot_info(lot, :wafers=>true)
    assert_equal "DSF", li.stage
    assert_equal "InBank", li.states["Lot Inventory State"]
    # bankin cancel and split
    assert_equal 0, $sv.lot_bankin_cancel(lot)
    assert_equal 0, $sv.lot_opelocate(lot, "1300.1000")
    lc = $sv.lot_split(lot, 1)
    assert lc
    # bankin both lots
    sleep 300
    [lot, lc].each {|l|
      $sv.lot_bankin(l)
      li = $sv.lot_info(l, :wafers=>true)
      assert_equal "InBank", li.states["Lot Inventory State"]
      $log.info "processed lot #{l}, wafers #{li.wafers.collect {|w| w.wafer}.inspect}"
    }
  end
  
  def test351_BUM_SOR_PR
    lot = $pdwhtest.new_lot(:route=>"C02-TKEY.01", :product=>"TKEY01.A0", :sublottype=>"PR", :nwafers=>2)
    assert lot
    $sv.lot_opelocate(lot, "6350.9500")
    li = $sv.lot_info(lot, :wafers=>true)
    assert_equal "BMP", li.stage
    assert $sv.claim_process_lot(lot, @@proc_params)
    li = $sv.lot_info(lot, :wafers=>true)
    assert_equal "SRT", li.stage
    $log.info "processed lot #{lot}, wafers #{li.wafers.collect {|w| w.wafer}.inspect}" 
  end
  
  def test352_BUM_SOR_PR_Force
    lot = $pdwhtest.new_lot(:route=>"C02-TKEY.01", :product=>"TKEY01.A0", :sublottype=>"PR", :nwafers=>2)
    assert lot
    ssert_equal 0, $sv.lot_opelocate(lot, "6350.9500")
    li = $sv.lot_info(lot, :wafers=>true)
    assert_equal "BMP", li.stage
    assert $sv.claim_process_lot(lot, @@proc_params.merge(:running_hold=>true))
    li = $sv.lot_info(lot, :wafers=>true)
    assert_equal "SRT", li.stage
    $log.info "processed lot #{lot}, wafers #{li.wafers.collect {|w| w.wafer}.inspect}" 
  end
  
  def test353_BUM_SOR_PR_Locate
    lot = $pdwhtest.new_lot(:route=>"C02-TKEY.01", :product=>"TKEY01.A0", :sublottype=>"PR", :nwafers=>2)
    assert lot
    ssert_equal 0, $sv.lot_opelocate(lot, "6350.9500")
    li = $sv.lot_info(lot, :wafers=>true)
    assert_equal "BMP", li.stage
    assert_equal 0, $sv.lot_opelocate(lot, 1)
    li = $sv.lot_info(lot, :wafers=>true)
    assert_equal "SRT", li.stage
    $log.info "processed lot #{lot}, wafers #{li.wafers.collect {|w| w.wafer}.inspect}" 
  end
  
  def test354_BUM_SOR_PR_GatePass
    lot = $pdwhtest.new_lot(:route=>"C02-TKEY.01", :product=>"TKEY01.A0", :sublottype=>"PR", :nwafers=>2)
    assert lot
    $sv.lot_opelocate(lot, "6350.9500")
    li = $sv.lot_info(lot, :wafers=>true)
    assert_equal "BMP", li.stage
    assert_equal 0, $sv.lot_gatepass(lot)
    li = $sv.lot_info(lot, :wafers=>true)
    assert_equal "SRT", li.stage
    $log.info "processed lot #{lot}, wafers #{li.wafers.collect {|w| w.wafer}.inspect}" 
  end
  
  def test355_DSB_BankIn
    lot = $pdwhtest.new_lot(:route=>"C02-QA00.01", :product=>"TKEY10.01", :sublottype=>"PR", :nwafers=>1)
    assert lot
    $sv.lot_opelocate(lot, "last")
    $sv.lot_bankin(lot)
    li = $sv.lot_info(lot, :wafers=>true)
    assert_equal "DSB", li.stage
    assert_equal "InBank", li.states["Lot Inventory State"]
    $log.info "processed lot #{lot}, wafers #{li.wafers.collect {|w| w.wafer}.inspect}" 
  end

  def test355a_DSB_BankIn
    lot = $pdwhtest.new_lot(:route=>"C02-QA00.01", :product=>"TKEY10.01", :sublottype=>"PR", :nwafers=>2)
    assert lot
    $sv.lot_opelocate(lot, "last")
    $sv.lot_bankin(lot)
    li = $sv.lot_info(lot, :wafers=>true)
    assert_equal "DSB", li.stage
    assert_equal "InBank", li.states["Lot Inventory State"]
    # bankin cancel and split
    assert_equal 0, $sv.lot_bankin_cancel(lot)
    assert_equal 0, $sv.lot_opelocate(lot, "last")
    lc = $sv.lot_split(lot, 1)
    assert lc
    # bankin both lots
    sleep 300
    [lot, lc].each {|l|
      $sv.lot_bankin(l)
      li = $sv.lot_info(l, :wafers=>true)
      assert_equal "InBank", li.states["Lot Inventory State"]
      $log.info "processed lot #{l}, wafers #{li.wafers.collect {|w| w.wafer}.inspect}"
    }
  end
  
  def test356_SOR_RFL_PR
    lot = $pdwhtest.new_lot(:route=>"QA-TKEY.01", :product=>"QATKEY01.A0", :sublottype=>"PR", :nwafers=>2)
    assert lot
    $sv.lot_opelocate(lot, "6400.2600")
    li = $sv.lot_info(lot, :wafers=>true)
    assert_equal "SRT", li.stage
    assert $sv.claim_process_lot(lot, @@proc_params)
    li = $sv.lot_info(lot, :wafers=>true)
    assert_equal "RFL", li.stage
    $log.info "processed lot #{lot}, wafers #{li.wafers.collect {|w| w.wafer}.inspect}" 
  end
  
  def test356a_FAB_BUM_SOR
    lot = $pdwhtest.new_lot(:route=>"C02-TKEY.01", :product=>"TKEY01.A0", :sublottype=>"PR", :nwafers=>2)
    assert lot
    assert_equal 0, $sv.lot_gatepass(lot)
    assert_equal 0, $sv.lot_opelocate(lot, "6150.1500")
    li = $sv.lot_info(lot, :wafers=>true)
    assert_equal "TML", li.stage
    assert $sv.claim_process_lot(lot)
    li = $sv.lot_info(lot, :wafers=>true)
    assert_equal "BMP", li.stage
    assert_equal 0, $sv.lot_opelocate(lot, "6400.0900")
    li = $sv.lot_info(lot, :wafers=>true)
    assert_equal "SRT", li.stage
    $log.info "processed lot #{lot}, wafers #{li.wafers.collect {|w| w.wafer}.inspect}" 
  end
  
  def test357_BUM_SOR_PR_Force
    lot = $pdwhtest.new_lot(:route=>"QA-TKEY.01", :product=>"QATKEY01.A0", :sublottype=>"PR", :nwafers=>2)
    assert lot
    $sv.lot_opelocate(lot, "6400.2600")
    li = $sv.lot_info(lot, :wafers=>true)
    assert_equal "SRT", li.stage
    assert $sv.claim_process_lot(lot, @@proc_params.merge(:running_hold=>true))
    li = $sv.lot_info(lot, :wafers=>true)
    assert_equal "RFL", li.stage
    $log.info "processed lot #{lot}, wafers #{li.wafers.collect {|w| w.wafer}.inspect}" 
  end
  
  def test358_BUM_SOR_PR_Locate
    lot = $pdwhtest.new_lot(:route=>"QA-TKEY.01", :product=>"QATKEY01.A0", :sublottype=>"PR", :nwafers=>2)
    assert lot
    $sv.lot_opelocate(lot, "6400.2600")
    li = $sv.lot_info(lot, :wafers=>true)
    assert_equal "SRT", li.stage
    assert_equal 0, $sv.lot_opelocate(lot, 1)
    li = $sv.lot_info(lot, :wafers=>true)
    assert_equal "RFL", li.stage
    $log.info "processed lot #{lot}, wafers #{li.wafers.collect {|w| w.wafer}.inspect}" 
  end
  
  def test359_BUM_SOR_PR_GatePass
    lot = $pdwhtest.new_lot(:route=>"QA-TKEY.01", :product=>"QATKEY01.A0", :sublottype=>"PR", :nwafers=>2)
    assert lot
    $sv.lot_opelocate(lot, "6400.2600")
    li = $sv.lot_info(lot, :wafers=>true)
    assert_equal "SRT", li.stage
    assert_equal 0, $sv.lot_gatepass(lot)
    li = $sv.lot_info(lot, :wafers=>true)
    assert_equal "RFL", li.stage
    $log.info "processed lot #{lot}, wafers #{li.wafers.collect {|w| w.wafer}.inspect}" 
  end
  
  def test360_DSS_BankIn
    lot = $pdwhtest.new_lot(:route=>"QA-TKEY.01", :product=>"QATKEY01.A0", :sublottype=>"PR", :nwafers=>1)
    assert lot
    $sv.lot_opelocate(lot, "last")
    $sv.lot_bankin(lot)
    li = $sv.lot_info(lot, :wafers=>true)
    assert_equal "DSS", li.stage
    assert_equal "InBank", li.states["Lot Inventory State"]
    $log.info "processed lot #{lot}, wafers #{li.wafers.collect {|w| w.wafer}.inspect}" 
  end

  def test360a_DSB_BankIn
    lot = $pdwhtest.new_lot(:route=>"QA-TKEY.01", :product=>"QATKEY01.A0", :sublottype=>"PR", :nwafers=>2)
    assert lot
    $sv.lot_opelocate(lot, "last")
    $sv.lot_bankin(lot)
    li = $sv.lot_info(lot, :wafers=>true)
    assert_equal "DSS", li.stage
    assert_equal "InBank", li.states["Lot Inventory State"]
    # bankin cancel and split
    assert_equal 0, $sv.lot_bankin_cancel(lot)
    assert_equal 0, $sv.lot_opelocate(lot, "last")
    lc = $sv.lot_split(lot, 1)
    assert lc
    # bankin both lots
    sleep 300
    [lot, lc].each {|l|
      $sv.lot_bankin(l)
      li = $sv.lot_info(l, :wafers=>true)
      assert_equal "InBank", li.states["Lot Inventory State"]
      $log.info "processed lot #{l}, wafers #{li.wafers.collect {|w| w.wafer}.inspect}"
    }
  end
  
  def test361_FINA_FWET_PR
    lot = $pdwhtest.new_lot(:route=>"QA-TKEY.01", :product=>"QATKEY01.A0", :sublottype=>"PR", :carrier=>"PDWHBEOL%", :nwafers=>2)
    assert lot
    assert_equal 0, $sv.lot_opelocate(lot, nil, :op=>"FINA-FWET.01")
    assert $sv.claim_process_lot(lot, @@proc_params)
    li = $sv.lot_info(lot, :wafers=>true)
    $log.info "processed lot #{lot}, wafers #{li.wafers.collect {|w| w.wafer}.inspect}" 
  end
  
  def test362_TMETFWET_PR
    lot = $pdwhtest.new_lot(:route=>"C02-0035.01", :product=>"2970GXU.01", :sublottype=>"PR", :carrier=>"PDWH0%", :nwafers=>2)
    assert lot
    assert_equal 0, $sv.lot_opelocate(lot, nil, :op=>"TMETFWET.01")
    assert $sv.claim_process_lot(lot, @@proc_params.merge(:running_hold=>true))
    li = $sv.lot_info(lot, :wafers=>true)
    $log.info "processed lot #{lot}, wafers #{li.wafers.collect {|w| w.wafer}.inspect}" 
  end
  
  
  def test363_FEOL_MOL_PR
    lot = $pdwhtest.new_lot(:route=>"C02-TKEY.01", :product=>"TKEY01.A0", :sublottype=>"PR", :carrier=>"PDWH0%", :nwafers=>2)
    assert lot
    assert_equal 0, $sv.lot_opelocate(lot, "4600.1100")
    assert $sv.claim_process_lot(lot, @@proc_params)
    li = $sv.lot_info(lot, :wafers=>true)
    $log.info "processed lot #{lot}, wafers #{li.wafers.collect {|w| w.wafer}.inspect}" 
  end
  
  def test364_FEOL_MOL_PR_ForceComp
    lot = $pdwhtest.new_lot(:route=>"C02-TKEY.01", :product=>"TKEY01.A0", :sublottype=>"PR", :carrier=>"PDWH0%", :nwafers=>2)
    assert lot
    assert_equal 0, $sv.lot_opelocate(lot, "4600.1100")
    assert $sv.claim_process_lot(lot, @@proc_params.merge(:running_hold=>true))
    li = $sv.lot_info(lot, :wafers=>true)
    $log.info "processed lot #{lot}, wafers #{li.wafers.collect {|w| w.wafer}.inspect}" 
  end
  
  def test365_FEOL_MOL_PR_GatePass
    lot = $pdwhtest.new_lot(:route=>"C02-TKEY.01", :product=>"TKEY01.A0", :sublottype=>"PR", :carrier=>"PDWH0%", :nwafers=>2)
    assert lot
    assert_equal 0, $sv.lot_opelocate(lot, "4600.1100")
    assert_equal 0, $sv.lot_gatepass(lot)
    li = $sv.lot_info(lot, :wafers=>true)
    $log.info "processed lot #{lot}, wafers #{li.wafers.collect {|w| w.wafer}.inspect}" 
  end
  
  def test366_FEOL_MOL_PR_Locate
    lot = $pdwhtest.new_lot(:route=>"C02-TKEY.01", :product=>"TKEY01.A0", :sublottype=>"PR", :carrier=>"PDWH0%", :nwafers=>2)
    assert lot
    assert_equal 0, $sv.lot_opelocate(lot, "4600.1100")
    assert_equal 0, $sv.lot_opelocate(lot, 1)
    li = $sv.lot_info(lot, :wafers=>true)
    $log.info "processed lot #{lot}, wafers #{li.wafers.collect {|w| w.wafer}.inspect}" 
  end
  

  def test367_MOL_BEOL_PR
    lot = $pdwhtest.new_lot(:route=>"C02-TKEY.01", :product=>"TKEY01.A0", :sublottype=>"PR", :carrier=>"PDWHM%", :nwafers=>2)
    assert lot
    assert_equal 0, $sv.lot_opelocate(lot, "4600.2300")
    assert $sv.claim_process_lot(lot, @@proc_params)
    li = $sv.lot_info(lot, :wafers=>true)
    $log.info "processed lot #{lot}, wafers #{li.wafers.collect {|w| w.wafer}.inspect}" 
  end
  
  def test368_MOL_BEOL_PR_ForceComp
    lot = $pdwhtest.new_lot(:route=>"C02-TKEY.01", :product=>"TKEY01.A0", :sublottype=>"PR", :carrier=>"PDWHM%", :nwafers=>2)
    assert lot
    assert_equal 0, $sv.lot_opelocate(lot, "4600.2300")
    assert $sv.claim_process_lot(lot, @@proc_params.merge(:eqp=>"SEM514", :running_hold=>true))
    li = $sv.lot_info(lot, :wafers=>true)
    $log.info "processed lot #{lot}, wafers #{li.wafers.collect {|w| w.wafer}.inspect}" 
  end
  
  def test369_MOL_BEOL_PR_GatePass
    lot = $pdwhtest.new_lot(:route=>"C02-TKEY.01", :product=>"TKEY01.A0", :sublottype=>"PR", :carrier=>"PDWHM%", :nwafers=>2)
    assert lot
    assert_equal 0, $sv.lot_opelocate(lot, "4600.2300")
    assert_equal 0, $sv.lot_gatepass(lot)
    li = $sv.lot_info(lot, :wafers=>true)
    $log.info "processed lot #{lot}, wafers #{li.wafers.collect {|w| w.wafer}.inspect}" 
  end
  
  def test370_MOL_BEOL_PR_Locate
    lot = $pdwhtest.new_lot(:route=>"C02-TKEY.01", :product=>"TKEY01.A0", :sublottype=>"PR", :carrier=>"PDWHM%", :nwafers=>2)
    assert lot
    assert_equal 0, $sv.lot_opelocate(lot, "4600.2300")
    assert_equal 0, $sv.lot_opelocate(lot, 1)
    li = $sv.lot_info(lot, :wafers=>true)
    $log.info "processed lot #{lot}, wafers #{li.wafers.collect {|w| w.wafer}.inspect}" 
  end
  

  def test371_BEOL_C4_PR
    lot = $pdwhtest.new_lot(:route=>"C02-TKEY.01", :product=>"TKEY01.A0", :sublottype=>"PR", :carrier=>"PDWHB%", :nwafers=>2)
    assert lot
    assert_equal 0, $sv.lot_opelocate(lot, "6150.1500")
    assert $sv.claim_process_lot(lot, @@proc_params)
    li = $sv.lot_info(lot, :wafers=>true)
    $log.info "processed lot #{lot}, wafers #{li.wafers.collect {|w| w.wafer}.inspect}" 
  end
  
  def test372_BEOL_C4_PR_ForceComp
    lot = $pdwhtest.new_lot(:route=>"C02-TKEY.01", :product=>"TKEY01.A0", :sublottype=>"PR", :carrier=>"PDWHB%", :nwafers=>2)
    assert lot
    assert_equal 0, $sv.lot_opelocate(lot, "6150.1500")
    assert $sv.claim_process_lot(lot, @@proc_params.merge(:running_hold=>true))
    li = $sv.lot_info(lot, :wafers=>true)
    $log.info "processed lot #{lot}, wafers #{li.wafers.collect {|w| w.wafer}.inspect}" 
  end
  
  def test373_BEOL_C4_PR_GatePass
    lot = $pdwhtest.new_lot(:route=>"C02-TKEY.01", :product=>"TKEY01.A0", :sublottype=>"PR", :carrier=>"PDWHB%", :nwafers=>2)
    assert lot
    assert_equal 0, $sv.lot_opelocate(lot, "6150.1500")
    assert_equal 0, $sv.lot_gatepass(lot)
    li = $sv.lot_info(lot, :wafers=>true)
    $log.info "processed lot #{lot}, wafers #{li.wafers.collect {|w| w.wafer}.inspect}" 
  end
  
  def test374_BEOL_C4_PR_Locate
    lot = $pdwhtest.new_lot(:route=>"C02-TKEY.01", :product=>"TKEY01.A0", :sublottype=>"PR", :carrier=>"PDWHB%", :nwafers=>2)
    assert lot
    assert_equal 0, $sv.lot_opelocate(lot, "6150.1500")
    assert_equal 0, $sv.lot_opelocate(lot, 1)
    li = $sv.lot_info(lot, :wafers=>true)
    $log.info "processed lot #{lot}, wafers #{li.wafers.collect {|w| w.wafer}.inspect}" 
  end
  
  # Skip
  
  def test381_OpeComp
    lot = $pdwhtest.new_lot(:carrier=>"PDWH0%", :nwafers=>24)
    assert lot
    assert_equal 0, $sv.lot_opelocate(lot, @@opNo_proc)
    li = $sv.lot_info(lot)
    assert $sv.claim_process_lot(lot, @@proc_params)
    $log.info "processed lot #{lot} at PD #{li.op}" 
  end
  
  def test382_GatePass_Rwk
    lot = $pdwhtest.new_lot(:carrier=>"PDWH0%", :nwafers=>24)
    assert lot
    assert_equal 0, $sv.lot_opelocate(lot, 1)
    assert_equal 0, $sv.lot_rework(lot, "P-PDWH-DRWK-STAGEID.01", :return_opNo=>"2000.1300", :dynamic=>true)
    li = $sv.lot_info(lot)
    assert_equal 0, $sv.lot_gatepass(lot)
    $log.info "processed lot #{lot} at PD #{li.op}" 
  end
  
  def test383_GatePass_2
    lot = $pdwhtest.new_lot(:carrier=>"PDWH0%", :nwafers=>24)
    assert lot
    assert_equal 0, $sv.lot_opelocate(lot, @@opNo_proc)
    assert $sv.claim_process_lot(lot, @@proc_params)
    assert_equal 0, $sv.lot_opelocate(lot, -1)
    li = $sv.lot_info(lot)
    assert_equal 0, $sv.lot_gatepass(lot)
    $log.info "processed lot #{lot} at PD #{li.op}" 
  end
  
  def test384_GatePass_monitorlot
    # create monitorlot
    srcproduct = "UT-RAW.00"
    startbank = "UT-RAW"
    product = "UT-MN-UTC001.01"
    lottype = "Equipment Monitor"
    sublottype = "Equipment Monitor"
    srclot = $sv.vendorlot_receive(startbank, srcproduct, 24, :prepare=>true, :carrier=>"PDWH0%")
    lot = $sv.stb_controllot(product, lottype, :sublottype=>sublottype, :srclot=>srclot, :nwafers=>24)
    assert lot
    #
    assert_equal 0, $sv.lot_opelocate(lot, nil, :op=>"UTPD000.01")
    li = $sv.lot_info(lot, :wafers=>true)
    assert_equal 0, $sv.lot_gatepass(lot)
    $log.info "processed lot #{lot}, wafer #{li.wafers.collect {|w| w.wafer}.inspect}" 
  end
  
  def test385_GatePass_TKEY
    lot = $pdwhtest.new_lot(:carrier=>"PDWH0%", :nwafers=>24, :route=>"C02-TKEY.01", :product=>"3006AA.A0")
    assert lot
    assert_equal 0, $sv.lot_opelocate(lot, nil, :op=>"TKEYBUMPSHIPINIT.01")
    assert_equal 0, $sv.lot_branch(lot, "TKEY-BUMP.01", :return_opNo=>"6350.1000")
    li = $sv.lot_info(lot)
    assert_equal 0, $sv.lot_gatepass(lot)
    $log.info "processed lot #{lot} at PD #{li.op}" 
  end
  
  def test386_GatePass_BYLOCASS
    lot = $pdwhtest.new_lot(:carrier=>"PDWH0%", :nwafers=>24)
    assert lot
    assert_equal 0, $sv.lot_branch(lot, "BY-LOCASS-BONDING.01", :dynamic=>true)
    assert_equal 0, $sv.lot_opelocate(lot, 1)
    li = $sv.lot_info(lot)
    assert_equal 0, $sv.lot_gatepass(lot)
    $log.info "processed lot #{lot} at PD #{li.op}" 
  end
  
  def test387_GatePass_process
    lot = $pdwhtest.new_lot(:carrier=>"PDWH%", :nwafers=>24)
    assert lot
    assert_equal 0, $sv.lot_opelocate(lot, @@opNo_proc)
    li = $sv.lot_info(lot)
    assert_equal 0, $sv.lot_gatepass(lot)
    $log.info "processed lot #{lot} at PD #{li.op}" 
  end
  
  def test388_GatePass_meas
    lot = $pdwhtest.new_lot(:carrier=>"PDWH%", :nwafers=>24)
    assert lot
    assert_equal 0, $sv.lot_opelocate(lot, @@opNo_meas)
    li = $sv.lot_info(lot)
    assert_equal 0, $sv.lot_gatepass(lot)
    $log.info "processed lot #{lot} at PD #{li.op}" 
  end

  def test389_GatePass_other
    lot = $pdwhtest.new_lot(:carrier=>"PDWH%", :nwafers=>24)
    assert lot
    assert_equal 0, $sv.lot_opelocate(lot, @@opNo_other)
    li = $sv.lot_info(lot)
    assert_equal 0, $sv.lot_gatepass(lot)
    $log.info "processed lot #{lot} at PD #{li.op}" 
  end
  
  def test391_split_merge
    lot = $pdwhtest.new_lot(:carrier=>"PDWH%")
    assert lot
    lc = $sv.lot_split(lot, 1)
    assert lc
    li = $sv.lot_info(lc, :wafers=>true)
    sleep 60
    assert_equal 0, $sv.lot_merge(lot, lc)
    $log.info "split/merged lot #{lc}, wafers #{li.wafers.collect {|w| w.wafer}.inspect}" 
  end

  def test392_scrap_cancel
    lot = $pdwhtest.new_lot(:carrier=>"PDWH%", :nwafers=>1)
    assert lot
    assert_equal 0, $sv.scrap_wafers(lot)
    sleep 60
    li = $sv.lot_info(lot, :wafers=>true)
    assert_equal 0, $sv.scrap_wafers_cancel(lot)
    $log.info "split/merged lot #{lot}, wafers #{li.wafers.collect {|w| w.wafer}.inspect}" 
  end
  
  def test393_psm_multiple
    lot = $pdwhtest.new_lot(:carrier=>"PDWH%")
    assert lot
    # assign PSMs
    splitwafers = ["01 02", "03 04"]
    rtes = [$pdwhtest.subroutes[:branch][:static][:layer], $pdwhtest.subroutes[:branch][:static][:nolayer]]
    retOpNos = ["4000.1000", "4000.1000"]
    assert_equal 0, $sv.lot_experiment(lot, splitwafers, rtes, "2000.1200", retOpNos)
    # split
    assert_equal 0, $sv.lot_opelocate(lot, "2000.1200")
    lf = $sv.lot_family(lot)
    assert_equal 3, lf.size
    # undo 1 split
    lc = lf.last
    assert_equal 0, $sv.lot_branch_cancel(lc)
  end

  def test394_psm_multiple_undo
    lot = $pdwhtest.new_lot(:carrier=>"PDWH%")
    assert lot
    # assign PSMs
    splitwafers = ["01 02", "03 04"]
    rtes = [$pdwhtest.subroutes[:branch][:static][:layer], $pdwhtest.subroutes[:branch][:static][:nolayer]]
    retOpNos = ["4000.1000", "4000.1000"]
    assert_equal 0, $sv.lot_experiment(lot, splitwafers, rtes, "2000.1200", retOpNos)
    # split
    assert_equal 0, $sv.lot_opelocate(lot, "2000.1200")
    lf = $sv.lot_family(lot)
    assert_equal 3, lf.size
    # undo splits and merge
    lf.each {|l|
      assert_equal(0, $sv.lot_branch_cancel(l))  if l != lot
      assert_equal 0, $sv.lot_futurehold_cancel(l, nil)
    }
    assert_equal 0, $sv.lot_merge(lot, nil)
  end
  
  def test395_script_param
    data = {"Lot"=>"UTstring", "Wafer"=>"UTstring", "Eqp"=>"TestTest", "Product"=>"MetrologyCategory", 
      "Route"=>"Owner", "Process"=>"PassCountMax"}
    data.each_pair {|classname, param|
      candidates = case classname
      when "Lot"
        $sv.lot_list(:status=>"Waiting")
      when "Wafer"
        $sv.lot_list(:status=>"Waiting").take(20).collect {|lot| 
          $sv.lot_info(lot, :wafers=>true).wafers.collect {|w| w.wafer}
        }.flatten
      when "Eqp"
        $sv.workarea_eqp_list("1S-BTF").eqp
      when "Product"
        $sv.product_list.collect {|p| p.product}
      when "Route"
        $sv.product_list.collect {|p| p.route if p.route != ""}.compact
      when "Process"
        $sv.product_list.take(20).collect {|p|
          $sv.route_operations(p.route).collect {|r| r.op} if p.route != ""
        }.flatten.compact
      end
      pids = []  
      candidates.each {|c|
        pids << c unless $sv.user_parameter(classname, c, :name=>param).valueflag
        break if pids.size == 3
      }
      assert $sv.user_parameter_set_verify(classname, pids[0], param, 1, :changetype=>1)
      assert $sv.user_parameter_set_verify(classname, pids[1], param, 1, :changetype=>1)
      assert $sv.user_parameter_set_verify(classname, pids[1], param, 2, :changetype=>2)
      assert $sv.user_parameter_set_verify(classname, pids[2], param, 1, :changetype=>1)
      assert $sv.user_parameter_delete_verify(classname, pids[2], param)
      $log.info "using #{classname.inspect} #{pids.inspect}"
    }
  end
  
  def test401_cycletime
    lot = $pdwhtest.new_lot(:carrier=>"PDWH%")
    assert lot
    sleeptime = 60
    route = $sv.lot_info(lot).route
    rops = $sv.route_operations(route)
    while true do
      sleep sleeptime
      assert $sv.claim_process_lot(lot)
      break if $sv.lot_info(lot).opNo == rops.last.opNo
      sleep sleeptime
      assert_equal 0, $sv.lot_hold(lot, "ENG")
      sleep sleeptime
      assert_equal 0, $sv.lot_hold(lot, "X-DC")
      sleep sleeptime
      assert_equal 0, $sv.lot_nonprobankin(lot, "T-HOLD")
      sleep sleeptime
      assert_equal 0, $sv.lot_hold_release(lot, "X-DC")
      sleep sleeptime
      assert_equal 0, $sv.lot_nonprobankout(lot)
      sleep sleeptime
      assert_equal 0, $sv.lot_hold_release(lot, "ENG")
    end
    sleep sleeptime
    $sv.lot_bankin(lot)
    sleep sleeptime
    assert_equal 0, $sv.lot_bank_move(lot, "OY-SHIP")
    sleep sleeptime
    assert_equal 0, $sv.lot_ship(lot, "OY-SHIP")
  end

  def test402_cycletime
    lot = $pdwhtest.new_lot(carrier: "PDWH%", route: "P-PDWH-MAINPD.05")
    assert lot
    sleeptime = 120
    #
    sleep sleeptime
    $sv.claim_process_lot(lot, @@proc_params)
    sleep sleeptime
    $sv.lot_nonprobankin(lot, "DN-USE")
    sleep sleeptime
    $sv.lot_bank_move(lot, "T-HOLD")
    sleep sleeptime
    $sv.lot_nonprobankout(lot)
    #
    sleep sleeptime
    $sv.claim_process_lot(lot, @@proc_params)
    sleep sleeptime
    $sv.lot_nonprobankin(lot, "T-HOLD")
    sleep sleeptime
    $sv.lot_nonprobankout(lot)
    #
    sleep sleeptime
    $sv.claim_process_lot(lot, @@proc_params)
    sleep sleeptime
    $sv.lot_hold(lot, "X-DC")
    sleep sleeptime
    $sv.lot_hold_release(lot, nil)
    #
    sleep sleeptime
    $sv.claim_process_lot(lot, @@proc_params)
    sleep sleeptime
    $sv.lot_hold(lot, "X-DC")
    sleep sleeptime
    $sv.lot_hold(lot, "PLTH")
    sleep sleeptime
    $sv.lot_hold_release(lot, "PLTH")
    sleep sleeptime
    $sv.lot_hold_release(lot, "X-DC")
    #
    nc = $sv.carrier_list(empty: true, category: "HK", status: "AVAILABLE").first
    $sv.wafer_sort_req(lot, nc)
    sleep sleeptime
    $sv.claim_process_lot(lot, @@proc_params)
    #
    nc = $sv.carrier_list(empty: true, category: "MOL", status: "AVAILABLE").first
    $sv.wafer_sort_req(lot, nc)
    sleep sleeptime
    $sv.claim_process_lot(lot, @@proc_params)
    sleep sleeptime
    $sv.lot_hold(lot, "X-DC")
    sleep sleeptime
    $sv.lot_nonprobankin(lot, "T-HOLD")
    sleep sleeptime
    $sv.lot_nonprobankout(lot)
    sleep sleeptime
    $sv.lot_hold_release(lot, nil)
    #
    sleep sleeptime
    $sv.claim_process_lot(lot, @@proc_params)
    sleep sleeptime
    $sv.lot_hold(lot, "X-DC")
    sleep sleeptime
    $sv.lot_nonprobankin(lot, "DN-USE")
    sleep sleeptime
    $sv.lot_nonprobankout(lot)
    sleep sleeptime
    $sv.lot_hold_release(lot, nil)
    #
    nc = $sv.carrier_list(empty: true, category: "BEOL", status: "AVAILABLE").first
    $sv.wafer_sort_req(lot, nc)
    sleep sleeptime
    $sv.claim_process_lot(lot, @@proc_params)
    sleep sleeptime
    $sv.lot_nonprobankin(lot, "DN-USE")
    sleep sleeptime
    $sv.lot_nonprobankout(lot)
    #
    sleep sleeptime
    $sv.claim_process_lot(lot, @@proc_params)
    #
    sleep sleeptime
    $sv.claim_process_lot(lot, @@proc_params)
    sleep sleeptime
    $sv.lot_hold(lot, "O-LH")
    sleep sleeptime
    $sv.lot_hold_release(lot, "O-LH")
    #
    sleep sleeptime
    $sv.lot_bankin(lot)
    sleep sleeptime
    $sv.lot_bank_move(lot, "OY-SHIP")
    sleep sleeptime
    $sv.lot_ship(lot, "OY-SHIP")
  end
  
  def test998_summary
    $log.info "lots:\n#{@@lots.pretty_inspect}"
  end
end
