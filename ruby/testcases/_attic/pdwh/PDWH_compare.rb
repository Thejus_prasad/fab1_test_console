=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: Thomas Klose, 2016-07-27

History:
  2017-11-20 sfrieske, use struct_to_hash instead of Struct.to_hash
=end

require 'RubyTestCase'
require 'pdwhtest'
require 'thread' # loader stuff
require_relative 'PDWHR' # not used here


# PDWH comparison tool for let and itdc.
# pdwh gets their data from ITDC only and stores in their own itdc and let env. 
# PDWH_compare should compare pdwh itdc and pdwh let environment
class PDWH_compare < PDWHR
  @@static_data = nil

  def self.startup
    super
    $env = 'itdc'
    @@loader_timeout = 1800 # for 'stress' DB typically < 15min
    
    # pdwh itdc section
    @@dbenv = $env
    $pdwhtest = PDWH::Test.new($env, sv: $sv, dbenv: @@dbenv, loader_timeout: @@loader_timeout)
    $lldb_itdc = $pdwhtest.lldb
    
    # pdwh let section
    @@dbenv = 'stress'
    $pdwhtest_let = PDWH::Test.new($env, sv: $sv, dbenv: @@dbenv, loader_timeout: @@loader_timeout)
    $lldb_let = $pdwhtest_let.lldb
    
    $sm = $pdwhtest.sm
    
    @@tstart = Time.now
  end

  #def self.shutdown
  #  super
  #  pass
  #end

  def xtest100
    $log.info("nothing to do here")
  end
  
  def xtest200_pd_description
    pd = 'P-PDWH-PD01.01'
    desc = Time.now.to_s
    # oi = $sm.object_info(:pd, pd)
    $sm.change_description(:pd, pd, desc)
    
    assert wait_loaders
    
  end

  def test210_udata
    pd = 'P-PDWH-PD01.01'
    pd_key = 'SpecificationID'
    
    lot = "U8ACK.00"
    lot_key = 'SpecificationID' 
    
    value = Time.now.to_s[0..15]
    # oi = $sm.object_info(:pd, pd)
    $sm.update_udata(:pd, pd, {pd_key => value})
    $sv.user_data_update(:lot, lot, {lot_key => value})
    
    wait_batch("SIVIEW_DIM")#, 'loader timeout'
    
    r_itdc = $lldb_itdc.udata(hist: true, name: pd_key, value: value)
    r_let  = $lldb_let.udata(hist: true, name: pd_key, value: value)
    assert compare_results(r_itdc, r_let, %w"cdc_date"), "PD Udata comparison failed"
    
    r_itdc = $lldb_itdc.udata(hist: true, name: lot_key, value: value)
    r_let  = $lldb_let.udata(hist: true, name: lot_key, value: value)
    assert compare_results(r_itdc, r_let), "Lot Udata comparison failed"
    
  end
  
  # helper methods
  def wait_batch(batch) # add paramter for loader
    t = []
    r = true
    
    t << Thread.new {
      # insert itdc wait_loader here!
      r &= $pdwhtest.wait_next_batch_run(batch)
      $log.info("ITDC batch returned") if r
    }
    
    t << Thread.new {
      # insert let wait_loader here!
      r &= $pdwhtest_let.wait_next_batch_run(batch)
      $log.info("LET batch returned") if r
    }
    
    t.each {|_t| _t.join}
    return r
  end
  
  def compare_results(r1, r2, exclude = [])
    # get keys    TODO: use verify_hash directly
    res = true
    r1.each {|_r1|
      _r1 = struct_to_hash(_r1)
      r2.each {|_r2|
        _r2 = struct_to_hash(_r2)
        _r1.keys.each {|k|
          unless exclude.member?(k.to_s)
            res &= _r1[k] == _r2[k] 
            $log.info("comparison failed for #{k}, r1 was #{_r1[k]} r2 was #{_r2[k]}")
          end
          }
        }
      }
    return res
  end
  
end
