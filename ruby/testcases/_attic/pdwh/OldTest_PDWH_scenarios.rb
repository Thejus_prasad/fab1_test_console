# encoding: UTF-8

=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: Thomas Klose and Steffen Steidten, 2012-09-14
=end

require "RubyTestCase"
require 'pdwhtest'

class OldTest_PDWH_scenarios < RubyTestCase
  
  @@proc_params = {:notify=>false, :mode=>"Off-Line-1", :claim_carrier=>true, :proctime=>1, :memo=>"QAäöüß"}
  @@opNo_meas = "1000.1000"
  @@opNo_proc = "2000.1000"
  @@opNo_other = "9900.1000"
  @@opNo_DSF = "9900.1000"
  @@op_other = "P-PDWH-PD21-OTH.01"
  @@route2 = "P-PDWH-MAINPD.02"
  @@separationtime = 60

  def self.lots
    @@lots
  end
  
  def setup
    super
    $pdwhtest = PDWH::Test.new($env, :sv=>$sv) unless $pdwhtest and $pdwhtest.env == $env
    @@lots ||= {}
  end
  
  def test100_basic
    return  # already called, creates 90+ lots
    assert $pdwhtest.run_scenarios(:all, :show=>true, :export=>"log/pdwh_scenarios1.list")
  end
  
  def test102_moves
    lot = $pdwhtest.new_lot #"UXYK30556.000"
    assert lot, "error creating lot"
    $log.info "using lot #{lot}"
    #
    # GatePass, Remeasure
    sleep @@separationtime
    assert_equal 0, $sv.lot_gatepass(lot)
    sleep @@separationtime
    assert_equal 0, $sv.lot_remeasure(lot)
    #
    # Locate, ForceLocate
    sleep @@separationtime
    assert_equal 0, $sv.lot_hold(lot, nil)
    assert_equal 0, $sv.lot_opelocate(lot, 1, :force=>true)
    sleep @@separationtime
    assert_equal 0, $sv.lot_hold_release(lot, nil)
    assert_equal 0, $sv.lot_opelocate(lot, -1)
    #
    # PSM, PSM cancel
    sleep @@separationtime
    lc = $pdwhtest.erf(lot, :erf_partial=>false, :erf_cancel=>true)
    assert lc
    sleep @@separationtime
    lc = $pdwhtest.erf(lot, :erf_partial=>true, :erf_cancel=>true)
    assert lc
    #
    # Rework, Rework cancel
    sleep @@separationtime
    lc = $pdwhtest.rework(lot, :rwk_partial=>false, :rwk_cancel=>true)
    assert lc
    sleep @@separationtime
    lc = $pdwhtest.rework(lot, :rwk_partial=>true, :rwk_cancel=>true)
    assert lc
    sleep @@separationtime
    lc = $pdwhtest.rework(lot, :rwk_partial=>true, :rwk_cancel=>true, :rwk_onhold=>true)
    assert lc
    #
    # Branch
    sleep @@separationtime
    lc = $pdwhtest.branch(lot, :branch_cancel=>true)
    assert lc
    #
    # OpeComp, ForceOpeComp
    sleep @@separationtime
    assert $sv.claim_process_lot(lot)
    sleep @@separationtime
    assert $sv.claim_process_lot(lot, :running_hold=>true)
    assert_equal 0, $sv.lot_hold_release(lot, nil)
    #
    # SchdlChange
    sleep @@separationtime
    li = $sv.lot_info(lot)
    assert li.route != @@route2
    $sv.schdl_change(lot, :product=>li.product, :route=>@@route2, :opNo=>@@opNo_meas)
    li2 = $sv.lot_info(lot)
    assert_equal @@route2, li2.route
    assert_equal @@opNo_meas, li2.opNo
    sleep @@separationtime
    $sv.schdl_change(lot, :product=>li.product, :route=>li.route, :opNo=>@@opNo_proc)
    li3 = $sv.lot_info(lot)
    assert_equal li.route, li3.route
    assert_equal @@opNo_proc, li3.opNo
  end
  
  def test103_locate_pre1
    lot = $pdwhtest.new_lot #"UXYK30556.000"
    assert lot, "error creating lot"
    $log.info "using lot #{lot}"
    #
    li = $sv.lot_info(lot)
    assert_equal 0, $sv.lot_futurehold(lot, li.opNo, nil, :post=>true)
    assert $sv.claim_process_lot(lot)
    assert_equal 0, $sv.lot_hold_release(lot, nil)
  end
  
  def test121
    #assert_equal ["5", "5"], $pdwhtest.db.snapshot_interval, "wrong snapshot interval"
    sleeptime = 60
    lot = $pdwhtest.new_lot
    assert lot, "error creating lot"
    assert $sv.claim_process_lot(lot, @@proc_params)
    $log.info "going to sleep #{sleeptime}s between each step"
    sleep sleeptime
    assert_equal 0, $sv.lot_hold(lot, "PLTH") 
    sleep sleeptime
    assert_equal 0, $sv.lot_hold(lot, "TST") 
    sleep sleeptime
    assert_equal 0, $sv.lot_hold_release(lot, "PLTH")
    sleep sleeptime
    assert_equal 0, $sv.lot_hold_release(lot, "TST")
    sleep sleeptime
    assert_equal 0, $sv.lot_hold(lot, "ENG") 
    sleep sleeptime
    assert_equal 0, $sv.lot_hold_release(lot, "ENG")
    sleep sleeptime
    assert_equal 0, $sv.lot_opelocate(lot, "last")
    $sv.lot_bankin(lot)
    @@lots[__method__] = lot
  end
  
  def test122
    assert_equal ["5", "5"], $pdwhtest.db.snapshot_interval, "wrong snapshot interval"
    sleeptime = 60
    lot = $pdwhtest.new_lot
    assert lot, "error creating lot"
    assert $sv.claim_process_lot(lot, @@proc_params)
    $log.info "going to sleep #{sleeptime}s between each step"
    sleep sleeptime
    assert_equal 0, $sv.lot_nonprobankin(lot, "TT-HOLD")
    sleep sleeptime
    assert_equal 0, $sv.lot_bank_move(lot, "TS-HOLD")
    sleep sleeptime
    assert_equal 0, $sv.lot_nonprobankout(lot)
    sleep sleeptime
    assert_equal 0, $sv.lot_nonprobankin(lot, "FS-USE")
    sleep sleeptime
    assert_equal 0, $sv.lot_nonprobankout(lot)
    sleep sleeptime
    assert_equal 0, $sv.lot_opelocate(lot, "last")
    $sv.lot_bankin(lot)
    @@lots[__method__] = lot
  end
  
  # sprint 2, LR
  def test200_report
    # for Heike Urban, run once per day
    # create test lots
    n = 12
    lots1 = n.times.collect {|i| $pdwhtest.new_lot(:nwafers=>i+1, :route=>"P-PDWH-MAINPD.01")}
    assert_equal n, lots1.compact.size, "error creating test lots"
    lots2 = n.times.collect {|i| $pdwhtest.new_lot(:nwafers=>i+1, :route=>"P-PDWH-MAINPD.02")}
    assert_equal n, lots2.compact.size, "error creating test lots"
    $log.info "test lots:\n#{lots1.inspect}\n#{lots2.inspect}"
    # actions along the route
    
  end
  
  
  
  def test201
    # use special PD setup to distinguish Tech/PG/Product resolution for LR
    op = "P-PDHW-P1.01"
    products = %w(PDWHPROD1.01 PDWHPROD2.01 PDWHPROD3.01 PDWHPROD4.01)
    ##lots = products.collect {|p| $pdwhtest.new_lot(:route=>nil, :product=>p)}
    lots = %w(UXYK22139.000 UXYK22140.000 UXYK22141.000 UXYK22142.000)
    assert_equal products.size, lots.compact.size, "error creating test lots"
    $log.info "using lots #{lots.inspect}"
    # locate lots to the PD, requeue in case there were setup changes
    lots.each {|lot| 
      assert_equal 0, $sv.lot_requeue(lot)
      assert_equal 0, $sv.lot_opelocate(lot, nil, :op=>op)
    }
    # collect LR, MR and eqps
    puts "\nLR, MR and eqps:"
    lots.each {|lot|
      li = $sv.lot_info(lot)
      wnn = $sv.what_next(li.opEqps.last)
      wn = wnn.find {|e| e.lot == lot}
      puts "#{li.lot}\t#{li.technology}\t#{li.productgroup}\t#{li.product}\t#{wn.lrecipe}\t#{wn.mrecipe}\t#{li.opEqps.inspect}"
    }
    puts ""
  end
  
  
  # sprint 3a, Activities_SubKPI
  
  def test301
    fail "manual test"
    @@lots[__method__] = lot
  end

  def test302
    lot = $pdwhtest.new_lot
    assert lot, "error creating lot"
    assert_equal 0, $sv.lot_gatepass(lot)
    @@lots[__method__] = lot
  end

  def test303
    lot = $pdwhtest.new_lot
    assert lot, "error creating lot"
    assert_equal 0, $sv.lot_opelocate(lot, @@opNo_meas)
    assert $sv.claim_process_lot(lot, @@proc_params)
    assert_equal 0, $sv.lot_opelocate(lot, @@opNo_proc)
    assert $sv.claim_process_lot(lot, @@proc_params)
    @@lots[__method__] = lot
  end

  def test304
    lot = $pdwhtest.new_lot
    assert lot, "error creating lot"
    assert_equal 0, $sv.lot_opelocate(lot, @@opNo_meas)
    assert $sv.claim_process_lot(lot, @@proc_params.merge(:running_hold=>true))
    assert_equal 0, $sv.lot_hold_release(lot, nil)
    assert_equal 0, $sv.lot_opelocate(lot, @@opNo_proc)
    assert $sv.claim_process_lot(lot, @@proc_params.merge(:running_hold=>true))
    assert_equal 0, $sv.lot_hold_release(lot, nil)
    @@lots[__method__] = lot
  end

  def test305
    lot = $pdwhtest.new_lot
    assert lot, "error creating lot"
    assert_equal 0, $sv.lot_opelocate(lot, @@opNo_other)
    assert $sv.claim_process_lot(lot, @@proc_params)
    @@lots[__method__] = lot
  end

  def test306
    lot = $pdwhtest.new_lot
    assert lot, "error creating lot"
    assert_equal 0, $sv.lot_opelocate(lot, @@opNo_proc)
    assert $sv.claim_process_lot(lot, @@proc_params)
    assert_equal 0, $sv.lot_opelocate(lot, -1)
    assert $sv.claim_process_lot(lot, @@proc_params)
  end

  def test307
    lot = $pdwhtest.new_lot
    assert lot, "error creating lot"
    assert $pdwhtest.rework(lot, :rwk_partial=>false)
    @@lots[__method__] = lot
  end

  def test308
    lot = $pdwhtest.new_lot
    assert lot, "error creating lot"
    2.times {
      assert $pdwhtest.rework(lot, :rwk_partial=>false, :rwk_stay=>true)
      assert $pdwhtest.claim_process_lot_to(lot, :op=>@@op_other)
      assert $sv.claim_process_lot(lot, @@proc_params.merge(:running_hold=>true))
      assert_equal 0, $sv.lot_hold_release(lot, nil)
    }
  end

  # sprint 3a, Inventory_SubKPI
  def test309
    # create lots
    $lots = lots = 8.times.collect {
      lot = $pdwhtest.new_lot(:product=>"PDWHPROD1.04", :route=>"P-PDWH-MAINPD.04")
      assert lot, "error creating lot"
      lot
    }
    @@lots[__method__] = lots
    $log.info "lots: #{lots.inspect}"
    # prepare lots
    assert_equal 0, $sv.lot_opelocate(lots[0], @@opNo_DSF)
    # lot[1]: nothing
    assert_equal 0, $sv.lot_hold(lots[2], "PLTH") 
    assert_equal 0, $sv.lot_hold(lots[3], "TST") 
    assert_equal 0, $sv.lot_hold(lots[4], "PLTH") 
    assert_equal 0, $sv.lot_hold(lots[4], "TST")
    assert_equal 0, $sv.lot_nonprobankin(lots[5], "TT-HOLD")
    assert_equal 0, $sv.lot_nonprobankin(lots[6], "FS-USE")
    assert_equal 0, $sv.lot_hold(lots[7], "PLTH") 
    assert_equal 0, $sv.lot_nonprobankin(lots[7], "TT-HOLD")
    $log.info "used lots: #{lots.inspect}"
  end
  
  # sprint 3a, Inventory_AL
  def test311
    assert_equal ["5", "5"], $pdwhtest.db.snapshot_interval, "wrong snapshot interval"
    # create lots
    lots = []
    claimtimes = {}
    slts = %w(PR PR EB EB PR PR EB EB)
    qtys = [20, 10, 25, 25, 5, 7, 12, 12]
    slts.each_with_index {|slt, i|
      lot = $pdwhtest.new_lot(:product=>"PDWHPROD7.01", :route=>"P-PDWH-MAINPD.02", :sublottype=>slt, :nwafers=>qtys[i])
      assert lot, "error creating lot"
      lots << lot
    }
    @@lots[__method__] = lots
    $log.info "lots: #{lots.inspect}"
    now = Time.now
    h = now.min < 8 ? now.hour : now.hour + 1
    starttime = Time.local(now.year, now.month, now.day, h, 8)
    #
    # process L1..L4 within one hour at xx:08"
    (0..3).each {|i|
      lot = lots[i]
      $log.info "process lot#{i+1} (#{lot}) at #{starttime}"
      sleep starttime - Time.now
      assert $sv.claim_process_lot(lot, @@proc_params)
      claimtimes[lot] = Time.now
      starttime += 10*60
    }
    #
    # process L5..L8 within the following hour at xx:08
    now = Time.now
    h = now.min < 8 ? now.hour : now.hour + 1
    starttime = Time.local(now.year, now.month, now.day, h, 8)
    (4..7).each {|i| 
      lot = lots[i]
      $log.info "process lot#{i+1} (#{lot}) at #{starttime}"
      sleep starttime - Time.now
      assert $sv.claim_process_lot(lot, @@proc_params)
      claimtimes[lot] = Time.now
      starttime += 10*60
    }
    $log.info "processed lots: #{claimtimes.inspect}"
  end
  
  # sprint 3a, Activities_AL
  def test312
    assert_equal ["5", "5"], $pdwhtest.db.snapshot_interval, "wrong snapshot interval"
    # create lots
    lots = []
    claimtimes = {}
    slts = %w(PR PR EB EB PR PR EB EB)
    qtys = [20, 10, 25, 25, 5, 7, 12, 12]
#    slts.each_with_index {|slt, i|
#      lot = $pdwhtest.new_lot(:product=>"PDWHPROD1.03", :route=>"P-PDWH-MAINPD.03", :sublottype=>slt, :nwafers=>qtys[i])
#      assert lot, "error creating lot"
#      lots << lot
#    }
#    @@lots[__method__] = lots
lots = ["UXYK17035.000", "UXYK17036.000", "UXYK17037.000", "UXYK17038.000", "UXYK17039.000", "UXYK17040.000", "UXYK17041.000", "UXYK17042.000"]
    $log.info "lots: #{lots.inspect}"
    #
    # move lots to process PD
    lots.each {|lot| assert_equal 0, $sv.lot_opelocate(lot, @@opNo_proc)}
    #
    # process L1, L2 within 5 min at xx:06
    now = Time.now
    h = now.min < 6 ? now.hour : now.hour + 1
    starttime = Time.local(now.year, now.month, now.day, h, 6)
    (0..1).each {|i|
      lot = lots[i]
      $log.info "process lot#{i+1} (#{lot}) at #{starttime}"
      sleep starttime - Time.now
      assert $sv.claim_process_lot(lot, @@proc_params)
      claimtimes[lot] = Time.now
      starttime += 2*60
    }
    #
    # process L3, L4 at xx:26
    starttime = Time.local(now.year, now.month, now.day, h, 26)
    (2..3).each {|i|
      lot = lots[i]
      $log.info "process lot#{i+1} (#{lot}) at #{starttime}"
      sleep starttime - Time.now
      assert $sv.claim_process_lot(lot, @@proc_params)
      claimtimes[lot] = Time.now
      starttime += 2*60
    }
    #
    # process L5, L6 at xx+1:06
    starttime = Time.local(now.year, now.month, now.day, h+1, 6)
    (4..5).each {|i|
      lot = lots[i]
      $log.info "process lot#{i+1} (#{lot}) at #{starttime}"
      sleep starttime - Time.now
      assert $sv.claim_process_lot(lot, @@proc_params)
      claimtimes[lot] = Time.now
      starttime += 2*60
    }
    #
    # process L7, L8 at xx+1:26
    starttime = Time.local(now.year, now.month, now.day, h+1, 26)
    (6..7).each {|i|
      lot = lots[i]
      $log.info "process lot#{i+1} (#{lot}) at #{starttime}"
      sleep starttime - Time.now
      assert $sv.claim_process_lot(lot, @@proc_params)
      claimtimes[lot] = Time.now
      starttime += 2*60
    }
    $log.info "processed lots: #{claimtimes.inspect}"
  end

  #load_run_testcase Test_PDWH_scenarios, tp:tc400
  def test400
    # test WP1 (scrap and scrap cancel)
    # created new mainpd *.06. added pd for FWET: 11METFWET.01
    # SM setup: PDWH-FWET.01	2ML	5000.1000	11METFWET.01
    lots = []
    #scrap one lot at any operation
    lot = $pdwhtest.new_lot(:product=>"PDWHPROD1.06", :route=>"P-PDWH-MAINPD.06")
    lots << lot
    assert lot, "error creating lot"
    assert $sv.scrap_wafers(lot)
    #scrap one lot at FWET operation
    lot = $pdwhtest.new_lot(:product=>"PDWHPROD1.06", :route=>"P-PDWH-MAINPD.06")
    lots << lot
    assert lot, "error creating lot"
    assert $sv.lot_opelocate(lot, "5000.1000")
    assert $sv.scrap_wafers(lot)
    #scrap one lot outside fab, STAGE ID GROUP ID is not FAB
    lot = $pdwhtest.new_lot(:product=>"PDWHPROD1.06", :route=>"P-PDWH-MAINPD.06")
    lots << lot
    assert lot, "error creating lot"
    assert $sv.lot_opelocate(lot, "9900.1000")
    assert $sv.scrap_wafers(lot)  

    #wafer scrap cancel at any operation
    lot = $pdwhtest.new_lot(:product=>"PDWHPROD1.06", :route=>"P-PDWH-MAINPD.06")
    lots << lot
    assert lot, "error creating lot"
    assert $sv.scrap_wafers(lot)
    sleep 60
    assert $sv.scrap_wafers_cancel(lot)
    #scrap one lot at FWET operation
    lot = $pdwhtest.new_lot(:product=>"PDWHPROD1.06", :route=>"P-PDWH-MAINPD.06")
    lots << lot
    assert lot, "error creating lot"
    assert $sv.lot_opelocate(lot, "5000.1000")
    assert $sv.scrap_wafers(lot)
    sleep 60
    assert $sv.scrap_wafers_cancel(lot)
    #scrap one lot outside fab, STAGE ID GROUP ID is not FAB
    lot = $pdwhtest.new_lot(:product=>"PDWHPROD1.06", :route=>"P-PDWH-MAINPD.06")
    lots << lot
    assert lot, "error creating lot"
    assert $sv.lot_opelocate(lot, "9900.1000")
    assert $sv.scrap_wafers(lot)   
    sleep 60
    assert $sv.scrap_wafers_cancel(lot)

    #scrap an equipment monitor lot
    lot = $sv.lot_list(:lottype=>"Equipment Monitor", :status=>"Waiting").first # use with care!
    assert lot, "error finding lot"
    lots << lot
    assert $sv.scrap_wafers(lot)  
    # 
    lot = $pdwhtest.new_lot(:product=>"PDWHPROD1.06", :route=>"P-PDWH-MAINPD.06")
    lots << lot
    assert lot, "error creating lot"
    assert $sv.claim_process_lot(lot)
    assert $sv.lot_mfgorder_change(lot, :sublottype=>"SCRAP")
    sleep 60
    assert $sv.scrap_wafers(lot)
    
    #end
    @@lots[__method__] = lots
    $log.info "lots:\n#{@@lots.pretty_inspect}"
  end
  
  # Scrap-Split-Test-Szenarios
  def test401
    pds = "1000.1000 2000.1000 2000.1100 2000.1200 2000.1300".split(" ")
    lots = []
    lot = $pdwhtest.new_lot(:product=>"PDWHPROD1.06", :route=>"P-PDWH-MAINPD.06", :nwafers=>8)    
    lots << lot
    
    2.times {assert $sv.claim_process_lot(lot)}    
    lota = $sv.lot_split(lot, "03 04".split)
    lotb = $sv.lot_split(lot, "05 06".split)
    lotc = $sv.lot_split(lot, "07 08".split)
    lots << [lota, lotb, lotc]
    
    
    # lot M
    assert $sv.claim_process_lot(lot, :slr=>true)
    assert $sv.scrap_wafers(lot)
    assert $sv.wafer_sort_req(lot, "")
    
    # lot A
    assert $sv.claim_process_lot(lota, :slr=>true)
    assert $sv.claim_process_lot(lota, :slr=>true)
    assert $sv.scrap_wafers(lota, :reasonop=>pds[3])
    assert $sv.wafer_sort_req(lota, "")
    
    # lot B
    assert $sv.claim_process_lot(lotb, :slr=>true)
    assert $sv.scrap_wafers(lotb, :reasonop=>pds[1])
    assert $sv.wafer_sort_req(lotb, "")
    
    # lot C
    lotca = $sv.lot_split(lotc, 1)
    lots << lotca
    assert $sv.scrap_wafers(lotc)
    assert $sv.scrap_wafers(lotca, :reasonop=>pds[1])
    assert $sv.wafer_sort_req(lotc, "")
    assert $sv.wafer_sort_req(lotca, "")
    
    
    @@lots[__method__] = lots
    $log.info "lots:\n#{@@lots.pretty_inspect}"
  end
  
  def test402
    epd = "e1PDWH-NOSTAGEID.01"
    wfr_psm1 = ["13 14 15 16 17 18".split, "19 20 21 22 23 24".split]
    wfr_psm2 = ["04 10 16 22".split, "05 11 17 23".split, "06 12 18 24".split]
    wfr_psm3 = "02 08 14 20".split
    ll = {}
    lot = $pdwhtest.new_lot(:product=>"PDWHPROD1.07", :route=>"P-PDWH-MAINPD.07", :nwafers=>24)
    $sv.lot_gatepass(lot)   # skipping lotstart
    ll.store("M0", lot)
    
        
    # adding psm's to lot
    assert $sv.lot_experiment(lot, ["13 14 15 16 17 18".split, "19 20 21 22 23 24".split], [epd]*2, "2000.1200", "3000.1000") 
    assert $sv.lot_experiment(lot, wfr_psm2, [epd]*3, "3000.1200", "4000.1000")
    assert $sv.lot_experiment(lot, wfr_psm3, epd, "4000.1000", "4000.1200")
    
    # T1
    2.times {$sv.claim_process_lot(lot, :slr=>true)}
    _, lotb, lotc = $sv.lot_family(lot)
    lota = $sv.lot_split(lot, "07 08 09 10 11 12".split)
    ll.store("M1", lota)
    ll.store("M2", lotb)
    ll.store("M3", lotc)
    
    # T2
    2.times {$sv.claim_process_lot(ll["M0"], :slr=>true)}   #M0
     $sv.claim_process_lot(ll["M1"], :slr=>true)   #M1
     $sv.lot_opelocate(ll["M1"], -1)
    2.times {$sv.claim_process_lot(ll["M1"], :slr=>true)}
    3.times {$sv.claim_process_lot(ll["M2"], :slr=>true)}  # M2
    $sv.claim_process_lot(ll["M3"], :slr=>true)   #M3
    $sv.lot_opelocate(ll["M3"], -1)
    3.times {$sv.claim_process_lot(ll["M3"], :slr=>true)}    

    # T3, T4
    $sv.lot_merge(lot, nil)
    2.times{$sv.claim_process_lot(lot)}
    
    # T5
    _, lota, lotb, lotc = $sv.lot_family(lot)
    ll.store("M6", lota)
    ll.store("M7", lotb)
    ll.store("M8", lotc)
    lotd = $sv.lot_split(lot, "02 08 14 20".split)
    ll.store("M4", lotd)
    lote = $sv.lot_split(lot, "03 09 15 21".split)
    ll.store("M5", lote)
    
    # T6 - process
    2.times {$sv.claim_process_lot(ll["M0"], :slr=>true)}  #M0
    2.times {$sv.claim_process_lot(ll["M4"], :slr=>true)}  #M4
    $sv.claim_process_lot(ll["M4"], :slr=>true)  # in ERF
    $sv.claim_process_lot(ll["M6"], :slr=>true)  #M6 in ERF
    $sv.claim_process_lot(ll["M7"], :slr=>true)  #M7 in ERF
    $sv.claim_process_lot(ll["M8"], :slr=>true)  #M8 in ERF
    
    # T7
    lota = $sv.lot_split(ll["M4"], "14 20".split)
    ll.store("M9", lota)
    
    # Scrap party has to be performed manual
    ll.each_pair {|k,v|
        puts(" " + k + ": " + v + " ")}
  end
  
  def test403
    epd = "e1PDWH-NOSTAGEID.01"
    rwpd = "P-PDWH-RWK-STAGEID.01"   # @ 1000.1100
    ll = {}
    lot = $pdwhtest.new_lot(:product=>"PDWHPROD1.07", :route=>"P-PDWH-MAINPD.07", :nwafers=>6)
    $sv.lot_gatepass(lot)   # skipping lotstart
    ll.store("M0", lot)
    
    # add psm to lot, T1, T2
    assert $sv.lot_experiment(lot, "03 04 05 06".split, epd, "2000.1200", "3000.1000")
    2.times {$sv.claim_process_lot(ll["M0"], :slr=>true)}
    _, ll["M1"] = $sv.lot_family(ll["M0"])  # getting child lot from PSM split
    2.times {$sv.claim_process_lot(ll["M0"], :slr=>true)}
    
    # T3
    2.times {$sv.claim_process_lot(ll["M1"], :slr=>true)}   # 2x
    $sv.lot_hold(ll["M1"], "ENG")
    
    # T4
    ll["M2"] = $sv.lot_partial_rework(ll["M1"], "03 04".split(), rwpd, :return_opNo=>"1000.1000", :onhold=>true)
    
    $sv.lot_hold_release(ll["M2"], nil)
    $sv.claim_process_lot(ll["M2"], :slr=>true)
    3.times {
        $sv.claim_process_lot(ll["M2"], :slr=>true)
        $sv.lot_opelocate(ll["M2"], -1)}
    $sv.claim_process_lot(ll["M2"], :slr=>true)  # prepare M2 to merge with M1
    $sv.lot_opelocate(ll["M2"], +1, :forward=>true)  # same opno
    $sv.lot_opelocate(ll["M2"], "1000.1200", :forward=>true)
    $sv.lot_merge(ll["M1"], ll["M2"])  # merge
    
    # T5 (redo T4 with wafer 3,4 & 5)
    ll["M3"] = $sv.lot_partial_rework(ll["M1"], "03 04 05".split(), rwpd, :return_opNo=>"1000.1000", :onhold=>true)
    
    $sv.lot_hold_release(ll["M3"], nil)
    $sv.claim_process_lot(ll["M3"], :slr=>true)
    2.times {
        $sv.claim_process_lot(ll["M3"], :slr=>true)
        $sv.lot_opelocate(ll["M3"], -1)}
    $sv.claim_process_lot(ll["M3"], :slr=>true)  # prepare M3 to merge with M1
    $sv.lot_opelocate(ll["M3"], +1, :forward=>true)  # same opno
    $sv.lot_opelocate(ll["M3"], "1000.1200", :forward=>true)
    $sv.lot_merge(ll["M1"], ll["M3"])  # merge   
    
    # T6
    $sv.lot_hold_release(ll["M1"], nil)
    $sv.claim_process_lot(ll["M1"], :slr=>true)
    
    # T7, T8
    $sv.lot_merge(ll["M0"], ll["M1"])  # merge   
    $sv.claim_process_lot(ll["M0"], :slr=>true)
    
    # Scrap party has to be performed manual
    ll.each_pair {|k,v|
        puts(" " + k + ": " + v)}   
    
    
  end
  
  def test998_summary
    $log.info "lots:\n#{@@lots.pretty_inspect}"
  end
end