=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: Thomas Klose and Steffen Steidten, 2012-09-14
=end

require 'RubyTestCase'
require 'pdwhtest'

class Test_PDWH_1 < RubyTestCase

  @@backup_env = 'f8stag'
  @@f8product = 'GIMLI21MA.QA'
  @@f8route = 'QA-GIMLI2.01'
  @@f8backup_opNo = '8500.0751'
  @@f8backup_bank = 'O-BACKUPOPER'

  def self.lots
    @@lots
  end

  def self.startup
    super
    $pdwhtest = PDWH::Test.new($env, sv: $sv) unless $pdwhtest and $pdwhtest.env == $env
    $svnox = SiView::MM.new($env, user: 'NOX-UNIT', password: :default) unless $svnox and $svnox.env == $env
    $svbak = SiView::MM.new(@@backup_env) unless $svbak and $svbak.env == @@backup_env
    @@lots ||= {}
  end

  def self.after_all_passed
  end

  # tests for MSR release 2.1

  def test1010_scriptparams
    lot = $pdwhtest.new_lot
    assert lot
    # split lot twice and scrap 2nd lot
    lc = $sv.lot_split(lot, 12)
    assert lc
    lgc = $sv.lot_split(lc, 6)
    assert lgc
    assert_equal 0, $sv.scrap_wafers(lc)
    assert_equal 0, $sv.wafer_sort_req(lc, '')
    # set different lot script parameters for child lot
    ts = Time.now.utc.iso8601
    assert $sv.user_parameter_set_verify('Lot', lgc, 'ProgramID', "QAPID3 #{ts}")
    assert $sv.user_parameter_set_verify('Lot', lgc, 'DevProjectID', "QADEV3 #{ts}")
    # process the lots
    assert $pdwhtest.lot_activities(lot)
    assert $pdwhtest.lot_activities(lgc)
    $log.info "used lots #{lot.inspect}, #{lgc.inspect}"
    @@lots['1010'] = [lot, lgc]
  end

  def test1011_scriptparams_noship
    lot = $pdwhtest.new_lot
    assert lot
    # split lot twice and scrap 2nd lot
    lc = $sv.lot_split(lot, 12)
    assert lc
    lgc = $sv.lot_split(lc, 6)
    assert lgc
    assert_equal 0, $sv.scrap_wafers(lc)
    assert_equal 0, $sv.wafer_sort_req(lc, '')
    # set different lot script parameters for child lot
    ts = Time.now.utc.iso8601
    assert $sv.user_parameter_set_verify('Lot', lgc, 'ProgramID', "QAPID3 #{ts}")
    assert $sv.user_parameter_set_verify('Lot', lgc, 'DevProjectID', "QADEV3 #{ts}")
    # process the lots
    assert $pdwhtest.lot_activities(lot, noship: true)
    assert $pdwhtest.lot_activities(lgc, noship: true)
    assert_equal 0, $sv.lot_hold(lot, nil)
    assert_equal 0, $sv.lot_hold(lgc, nil)
    $log.info "used lots #{lot.inspect}, #{lgc.inspect}"
    @@lots['1011'] = [lot, lgc]
  end

  def test1012_scriptparams_scrap # ex test1030
    lot = $pdwhtest.new_lot
    assert lot
    # split lot twice and scrap 2nd lot
    lc = $sv.lot_split(lot, 12)
    assert lc
    lgc = $sv.lot_split(lc, 6)
    assert lgc
    assert_equal 0, $sv.scrap_wafers(lc)
    assert_equal 0, $sv.wafer_sort_req(lc, '')
    # set different lot script parameters for child lot
    ts = Time.now.utc.iso8601
    assert $sv.user_parameter_set_verify('Lot', lgc, 'ProgramID', "QAPID3 #{ts}")
    assert $sv.user_parameter_set_verify('Lot', lgc, 'DevProjectID', "QADEV3 #{ts}")
    # process the lots
    assert $pdwhtest.lot_activities(lot, scrap: true)
    assert $pdwhtest.lot_activities(lgc, scrap: true)
    $log.info "used lots #{lot.inspect}, #{lgc.inspect}"
    @@lots['1012'] = [lot, lgc]
  end

  def test1020_backup
    # create F8 lot
    lot = $svbak.new_lot_release(stb: true, product: @@f8product, route: @@f8route)
    assert lot
    liorig = $svbak.lot_info(lot)
    sleep 33
    assert $svbak.user_parameter_set_verify('Lot', lot, 'SourceFabCode', 'F8')
    assert_equal 0, $svbak.lot_hold_release(lot, nil)
    assert_equal 0, $svbak.lot_opelocate(lot, @@f8backup_opNo)
    # move to backup site
    assert $svbak.backup_prepare_send_receive(lot, $sv, backup_bank: @@f8backup_bank)
    # set source fab code to F8
    assert $sv.user_parameter_set_verify('Lot', lot, 'SourceFabCode', 'F8')
    # process the lot
    assert $pdwhtest.lot_activities(lot)
    # complete backup processing at source site
    $log.info '-- complete backup processing at the source site'
    assert_equal 0, $svbak.backup_lot_status_rpt(lot)
    assert_equal 0, $svbak.wafer_sort_req(lot, liorig.carrier)
    assert_equal 0, $svbak.lot_nonprobankout(lot)
    @@lots['1020'] = [lot]
  end

  def test1021_backup_hold
    # create F8 lot
    lot = $svbak.new_lot_release(stb: true, product: @@f8product, route: @@f8route)
    assert lot
    liorig = $svbak.lot_info(lot)
    sleep 33
    assert $svbak.user_parameter_set_verify('Lot', lot, 'SourceFabCode', 'F8')
    assert_equal 0, $svbak.lot_hold_release(lot, nil)
    assert_equal 0, $svbak.lot_opelocate(lot, @@f8backup_opNo)
    # move to backup site
    assert $svbak.backup_prepare_send_receive(lot, $sv, backup_bank: @@f8backup_bank)
    # set source fab code to F8
    assert $sv.user_parameter_set_verify('Lot', lot, 'SourceFabCode', 'F8')
    # process the lot
    assert $pdwhtest.lot_activities(lot, noship: true)
    assert_equal 0, $sv.lot_hold(lot, nil)
    @@lots['1021'] = [lot]
  end

  def test1040_wafer_scriptparameter_diecountgood
    assert lot = $pdwhtest.new_lot
    li = $sv.lot_info(lot, wafers: true)
    li.wafers.each {|w| assert $sv.user_parameter_set_verify('Wafer', w.wafer, 'DieCountGood', rand(150).to_s)}
    # gate_pass the lot
    assert_equal 0, $sv.lot_gatepass(lot)
    lc = $sv.lot_split(lot, 12)
    assert lc
    li.wafers.each {|w| assert $sv.user_parameter_set_verify('Wafer', w.wafer, 'DieCountGood', rand(100).to_s)}
    assert $sv.claim_process_lot(lot)
    assert $sv.claim_process_lot(lc)
    li.wafers.each {|w| assert $sv.user_parameter_set_verify('Wafer', w.wafer, 'DieCountGood', rand(50).to_s)}
    assert $pdwhtest.lot_activities(lot)
    assert $pdwhtest.lot_activities(lc)
    $log.info "used lots #{lot.inspect}, #{lc.inspect}"
    @@lots['1040'] = [lot, lc]
  end

  def test1050_lot_movement_while_lot_onhold #:-)
    # force locate
    lot1 = $pdwhtest.new_lot
    assert lot1
    assert $sv.carrier_xfer_status_change($sv.lot_info(lot1).carrier, "MO", "UTSTO11")
    assert $sv.claim_process_lot(lot1, eqp: "O-LOTSTART")
    assert $sv.lot_hold(lot1, nil)
    assert $sv.lot_opelocate(lot1, +1, force: true)
    assert $sv.lot_hold_release(lot1, nil)
    # running hold
    lot2 = $pdwhtest.new_lot
    assert lot2
    assert $sv.carrier_xfer_status_change($sv.lot_info(lot2).carrier, "MO", "UTSTO11")
    assert $sv.claim_process_lot(lot2, running_hold: true, eqp: "O-LOTSTART")
    assert $sv.lot_hold_release(lot2, nil)
    # nonprobank
    lot3 = $pdwhtest.new_lot
    assert lot3
    assert $sv.carrier_xfer_status_change($sv.lot_info(lot3).carrier, "MO", "UTSTO11")
    assert $sv.claim_process_lot(lot3, eqp: "O-LOTSTART")
    assert $sv.lot_hold(lot3, nil)
    assert $sv.lot_nonprobankin(lot3, $pdwhtest.nonprobank)
    assert $sv.lot_hold_release(lot3, nil)
    # process hold, force locate
    lot4 = $pdwhtest.new_lot
    assert lot4
    assert $sv.process_hold("C-ENG", route: $pdwhtest.route, opNo: "2000.1000", exechold: true)
    assert $sv.lot_opelocate(lot4, +1, force: true)
    phl = $sv.process_hold_list(route: $pdwhtest.route).first
    assert phl, "no process hold on route #{$pdwhtest.route}"
    assert $sv.process_hold_cancel(route: phl.route, opNo: phl.opNo, holdreason: "C-ENG", exechold: false)
    assert $sv.lot_hold_release(lot4, nil)
    #
    @@lots['1050'] = [lot1, lot2, lot3, lot4]
  end

  def test1051_lot_movement_with_various_holds
    assert lot = $pdwhtest.new_lot
    assert_equal 0, $sv.lot_gatepass(lot)
    assert $sv.process_hold("T-LH", route: $pdwhtest.route, opNo: "2000.1000", exechold: true)
    assert $sv.lot_opelocate(lot, +1, force: true)
    assert $sv.lot_hold(lot, "MGHL")
    assert $sv.lot_hold(lot, "X-DC")
    sleep 120
    assert $sv.lot_hold_release(lot, "MGHL")
    assert $sv.lot_opelocate(lot, +1, force: true)
    assert $sv.lot_hold(lot, "AD-H")
    assert $sv.lot_opelocate(lot, +1, force: true)
    assert $sv.lot_hold_release(lot, "X-DC")
    phl = $sv.process_hold_list(route: $pdwhtest.route).first
    assert phl, "no process hold on route #{$pdwhtest.route}"
    assert $sv.process_hold_cancel(route: phl.route, opNo: phl.opNo, holdreason: "T-LH", exechold: false)
    assert $sv.lot_hold_release(lot, "T-LH")
    assert $sv.lot_opelocate(lot, +1, force: true)
    assert $sv.lot_hold_release(lot, "AD-H")
    assert $sv.lot_opelocate(lot, +1)
    #
    @@lots['1051'] = [lot]
  end


  def test1060_scriptparams_scrap
    lot = $pdwhtest.new_lot
    assert lot
    # split lot with merge hold.
    lc = $sv.lot_split(lot, 12, mergeop: "2000.1200")
    assert lc
    assert $sv.claim_process_lot(lot)
    assert $sv.claim_process_lot(lot)
    assert $sv.claim_process_lot(lc)
    $log.info "used lots #{lot.inspect}, #{lgc.inspect}"
    @@lots['1060'] = [lot, lc]
  end

  def test1070_multiple_holds
    sleeptime = 60  # to separate events
    assert lot = $pdwhtest.new_lot
    assert_equal 0, $sv.lot_gatepass(lot)
    # same user, different reason codes
    $log.info "going to sleep #{sleeptime}s between each step"
    sleep sleeptime
    assert_equal 0, $sv.lot_hold(lot, 'ENG')
    sleep sleeptime
    assert_equal 0, $sv.lot_hold(lot, 'TST')
    sleep sleeptime
    assert_equal 0, $sv.lot_hold_release(lot, 'ENG')
    sleep sleeptime
    assert_equal 0, $sv.lot_hold_release(lot, 'TST')
    # same reason code, different users
    sleep sleeptime
    assert_equal 0, $sv.lot_hold(lot, 'X-S1')
    sleep sleeptime
    assert_equal 0, $svnox.lot_hold(lot, 'X-S1')
    sleep sleeptime
    assert_equal 0, $svnox.lot_hold_release(lot, 'X-S1')
    sleep sleeptime
    assert_equal 0, $sv.lot_hold_release(lot, 'X-S1')
    $log.info "used lot #{lot.inspect}"
    @@lots['1070'] = [lot]
  end

  def test1071_multiple_holds
    sleeptime = 60  # to separate events
    assert lot = $pdwhtest.new_lot
    assert_equal 0, $sv.lot_gatepass(lot)
    # same user, different reason codes
    $log.info "going to sleep #{sleeptime}s between each step"
    sleep sleeptime
    assert_equal 0, $sv.lot_hold(lot, 'O-LH')
    sleep sleeptime
    assert_equal 0, $sv.lot_hold(lot, 'ENG')
    sleep sleeptime
    assert_equal 0, $sv.lot_hold(lot, 'MGHL')
    sleep sleeptime
    assert_equal 0, $sv.lot_hold_release(lot, 'O-LH')
    sleep sleeptime
    assert_equal 0, $sv.lot_hold_release(lot, 'MGHL')
    sleep sleeptime
    assert_equal 0, $sv.lot_hold_release(lot, 'ENG')
    sleep sleeptime
    assert_equal 0, $sv.lot_hold(lot, 'T-RET', holdtype: 'ProcessHold')
    # same reason code, different users
    $log.info "going to sleep #{sleeptime*30}s between next steps"
    sleep sleeptime * 30    # 30min
    assert_equal 0, $svnox.lot_hold(lot, 'ENG')
    sleep sleeptime * 30    # 30min
    assert_equal 0, $svnox.lot_hold_release(lot, 'ENG')
    sleep sleeptime
    # release last hold from $sv user
    assert_equal 0, $sv.lot_hold_release(lot, 'T-RET', holdtype: 'ProcessHold')
    $log.info "used lot #{lot.inspect}"
    @@lots['1071'] = [lot]
  end

  def test1072_holds
    sleeptime = 60  # to separate events
    assert lot = $pdwhtest.new_lot
    assert_equal 0, $sv.lot_gatepass(lot)
    assert $sv.claim_process_lot(lot, happylot: true, running_hold: true)
    sleep sleeptime
    assert_equal 0, $sv.lot_hold(lot, 'ENG')
#    sleep sleeptime
#    assert_equal 0, $sv.lot_opelocate(lot, -1, force: true)
#    assert_equal 0, $sv.lot_hold_release(lot, 'FCHL')
  end

  def test1073_holds_psm
    sleeptime = 3600  # to separate events into different loader batches
    assert lot = $pdwhtest.new_lot
    assert_equal 0, $sv.lot_gatepass(lot)
    # attach PSM with 6 lots and locate lot to split op
    rte = $pdwhtest.subroutes[:branch][:static][:nolayer]
    splitwafers = [['01', '02'], ['03', '04'], ['05', '06'], ['07', '08'], ['09', '10'], ['11', '12']]
    assert_equal 0, $sv.lot_experiment(lot, splitwafers, [rte] * splitwafers.size, $pdwhtest.opNo_erf, "3000.1000")
    assert_equal 0, $sv.lot_opelocate(lot, $pdwhtest.opNo_erf)
    lcs = $sv.lot_family(lot) - [lot]
    assert_equal splitwafers.size, lcs.size
    # locate parent to merge op
    assert_equal 0, $sv.lot_opelocate(lot, $pdwhtest.opNo_erf_return)
    # locate children to merge op, wait for the hold to stay effective for a while and merge
    lcs.each {|lc|
      assert_equal 0, $sv.lot_opelocate(lc, $pdwhtest.opNo_erf_return)
      sleep sleeptime
      $sv.lot_merge(lot, lc)
    }
    assert_equal [lot], $sv.lot_family(lot)
    $log.info "used lots #{lot} and #{lcs.inspect}"
    @@lots['1073'] = [lot] + lcs
  end

  def test1099_summary
    $log.info "lots:\n#{@@lots.pretty_inspect}"
  end
end
