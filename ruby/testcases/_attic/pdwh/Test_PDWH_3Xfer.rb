=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: Thomas Klose and Steffen Steidten, 2012-09-14
=end

require 'SiViewTestCase'

# Xfer job test cases for PDWH 2.4 April 2015
class Test_PDWH_3Xfer < SiViewTestCase
  @@eqpmode = 'Semi-2'
  @@eqp = 'UTF001'
  @@port = 'P1'
  @@eqp2 = 'UTF002'
  @@port2 = 'P1'
  @@stocker2 = 'UTSTO12'
  @@stocker3 = 'UTSTO13'

  @@timeout = 180


  def self.startup
    super
    $dummyamhs = Dummy::AMHS.new($env)
    @@carriers = []
  end

  def self.shutdown
    @@carriers.each {|c| $svtest.cleanup_carrier(c)}
    $dummyamhs.set_variable('intrabaytime', 15000)
    $sm.update_udata(:eqp, [@@eqp, @@eqp2], {'Location'=>''})
    $sm.update_udata(:stocker, $svtest.stocker, {'Location'=>'F36'})
    $sm.update_udata(:stocker, @@stocker2, {'Location'=>'BTF'})
  end


  def setup
    @@carriers.each {|c| assert $svtest.cleanup_carrier(c, status: 'NOTAVAILABLE')}
  end

  def teardown
    $dummyamhs.set_variable('e84mode', false) # deactivate E84 failure mode
  end


  def test00_setup
    $setup_ok = false
    #
    assert @@carriers = $svtest.get_empty_carriers(2)
    [@@eqp, @@eqp2].each {|eqp|
      assert_equal 0, $sv.eqp_cleanup(eqp)
      assert $eis.restart_tool(eqp)
      assert_equal 0, $sv.eqp_mode_change(eqp, @@eqpmode)
    }
    $dummyamhs = Dummy::AMHS.new($env)
    assert $dummyamhs.register
    assert $dummyamhs.set_variable('intrabaytime', 45000)
    assert $dummyamhs.set_variable('stockintime', 1000)
    assert $dummyamhs.set_variable('stockouttime', 1000)
    #
    $setup_ok = true
  end

  # from XM regression tests

  def test14_carrier_e84_reroute
    assert $dummyamhs.set_variable('e84mode', true) # Activate E84 failure mode
    assert $dummyamhs.set_variable('fallbackstocker', @@stocker2)
    assert_equal 0, $sv.eqp_mode_change(@@eqp,  @@eqpmode)
    carrier = @@carriers[0]
    $sv.carrier_xfer(carrier, $svtest.stocker, '', @@eqp, @@port)
    # verify original xfer job to eqp
    assert job = $sv.carrier_xfer_jobs(carrier).first
    cjob = job.carrierjobs.first
    assert_equal @@eqp, cjob.tomachine
    # wait for xfer job change, DummyAMHS will reroute it to the target stocker
    assert wait_for(timeout: @@timeout) {
      job = $sv.carrier_xfer_jobs(carrier).first
      job && job.carrierjobs.find {|j| j.carrier == carrier && j.tomachine == @@stocker2}
    }, "reroute to stocker failed"
    # watit for job completion and ensure carrier is in the fallback stocker
    assert wait_for(timeout: @@timeout) {$sv.carrier_xfer_jobs(carrier).empty?}
    assert_equal @@stocker2, $sv.carrier_status(carrier).stocker, "wrong position"
  end

  def test31_single_xfer_reroute
    assert_equal 0, $sv.eqp_mode_change(@@eqp,  @@eqpmode)
    carrier = @@carriers[0]
    assert_equal 0, $sv.carrier_xfer(carrier, $svtest.stocker, '', @@eqp, @@port)
    assert wait_for(timeout: @@timeout) {$sv.carrier_xfer_jobs(carrier).empty?}
    # move from shelf to stocker
    assert_equal 0, $sv.carrier_xfer(carrier, @@eqp, @@port, $svtest.stocker, '')
    assert_equal $svtest.stocker, $sv.carrier_xfer_jobs(carrier).first.carrierjobs.first.tomachine
    # reroute to the other stocker
    assert_equal 0, $sv.carrier_xfer(carrier, @@eqp, @@port, @@stocker2, '')
    assert_equal @@stocker2, $sv.carrier_xfer_jobs(carrier).first.carrierjobs.first.tomachine
    # watit for job completion and ensure carrier is in stocker2
    assert wait_for(timeout: @@timeout) {$sv.carrier_xfer_jobs(carrier).empty?}
    assert_equal @@stocker2, $sv.carrier_status(carrier).stocker, "wrong position"
  end

  # OBSOLETE, now 4311
  def test41_batch_xfer
    # tools in Off-Line-2
    assert_equal 0, $sv.eqp_mode_change(@@eqp, 'Off-Line-2')
    assert_equal 0, $sv.eqp_mode_change(@@eqp2, 'Off-Line-2')
    # batch transfer
    assert_equal 0, $sv.carrier_xfer(@@carriers, '', '', [@@eqp, @@eqp2], [@@port, @@port2])
    assert wait_for(timeout: @@timeout) {$sv.carrier_xfer_jobs(@@carriers[0]).empty?}
    assert wait_for(timeout: @@timeout) {$sv.carrier_xfer_jobs(@@carriers[1]).empty?}
  end

  def test42_single_xfers_reroute_batch
    # tools in Off-Line-2
    assert_equal 0, $sv.eqp_mode_change(@@eqp, 'Off-Line-2')
    assert_equal 0, $sv.eqp_mode_change(@@eqp2, 'Off-Line-2')
    # start single xfers stocker-stocker, batch does not work!
    @@carriers.each {|c| assert_equal 0, $sv.carrier_xfer(c, $svtest.stocker, '', @@stocker2, '')}
    # batch reroute
    assert_equal 0, $sv.carrier_xfer(@@carriers, '', '', [@@eqp, @@eqp2], [@@port, @@port2])
    assert wait_for(timeout: @@timeout) {$sv.carrier_xfer_jobs(@@carriers[0]).empty?}
    assert wait_for(timeout: @@timeout) {$sv.carrier_xfer_jobs(@@carriers[1]).empty?}
  end

  # FSDR specific locations

  def test51_locations
    # set locations
    assert $sm.update_udata(:eqp, @@eqp, {'Location'=>'M1-L1-LIT'})
    assert $sm.update_udata(:stocker, $svtest.stocker, {'Location'=>'M1-L1-TST'})
    assert $sm.update_udata(:stocker, @@stocker2, {'Location'=>'BTF-L3-IMP'})
    assert $sm.update_udata(:eqp, @@eqp2, {'Location'=>'M2-L2-TST'})
    #
    carrier = @@carriers[0]
    assert_equal 0, $sv.carrier_xfer_status_change(carrier, 'EO', @@eqp, port: @@port)
    # eqp1 -> sto1
    assert_equal 0, $sv.carrier_xfer(carrier, @@eqp, @@port, $svtest.stocker, '')
    refute_equal [], $sv.carrier_xfer_jobs(carrier)
    assert wait_for(timeout: @@timeout) {$sv.carrier_xfer_jobs(carrier).empty?}
    # sto1 -> sto2
    assert_equal 0, $sv.carrier_xfer(carrier, $svtest.stocker, '', @@stocker2, '')
    refute_equal [], $sv.carrier_xfer_jobs(carrier)
    assert wait_for(timeout: @@timeout) {$sv.carrier_xfer_jobs(carrier).empty?}
    # sto2 -> eqp2
    assert_equal 0, $sv.carrier_xfer(carrier, @@stocker2, '', @@eqp2, @@port2)
    refute_equal [], $sv.carrier_xfer_jobs(carrier)
    assert wait_for(timeout: @@timeout) {$sv.carrier_xfer_jobs(carrier).empty?}
  end

  def test52_stocker_MACSreroute
    carrier = @@carriers[0]
    assert $svtest.cleanup_carrier(carrier)
    # sto1 -> sto3 (as reference only)
    assert_equal 0, $sv.carrier_xfer(carrier, $svtest.stocker, '', @@stocker3, '')
    refute_equal [], $sv.carrier_xfer_jobs(carrier)
    assert wait_for(timeout: @@timeout) {$sv.carrier_xfer_jobs(carrier).empty?}
    # sto1 -> sto3, reroute to sto2
    assert $svtest.cleanup_carrier(carrier)
    assert_equal 0, $sv.carrier_xfer(carrier, $svtest.stocker, '', @@stocker3, '')
    wait_for {$sv.carrier_status(carrier).xfer_status == 'SO'}
    xj1 = $sv.carrier_xfer_jobs(carrier).first
    $log.info "xj1: #{xj1}"
    assert_equal 0, $sv.carrier_xfer_status_change(carrier, 'MI', @@stocker2)
    assert_equal 0, $sv.carrier_xfer_status_change(carrier, 'SO', @@stocker2)
    xj2 = $sv.carrier_xfer_jobs(carrier).first
    $log.info "xj2: #{xj2}"
    assert_equal xj1.jobid, xj2.jobid
    assert wait_for(timeout: @@timeout) {$sv.carrier_xfer_jobs(carrier).empty?}
    assert_equal @@stocker3, $sv.carrier_status(carrier).stocker, 'wrong location'
  end

end
