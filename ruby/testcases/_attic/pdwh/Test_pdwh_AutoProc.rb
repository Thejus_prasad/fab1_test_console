=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

    load_run_testcase "Test_pdwh_AutoProc", "itdc"

Author: Thomas Klose, 2014-04-01
=end

require 'RubyTestCase'
require 'pdwhtest'

class Test_pdwh_AutoProc < RubyTestCase
  Description = "execute PDWH AutoProc test scenarios"

  @@separationtime = 300
  @@proc_params = {:notify=>false, :mode=>"Off-Line-1", :claim_carrier=>true, :proctime=>@@separationtime, :memo=>"no memo"}
  @@opNo_meas = "1000.1000"
  @@opNo_proc = "2000.1000"
  @@opNo_other = "9900.1000"
  @@opNo_DSF = "9900.1000"
  @@op_other = "P-PDWH-PD21-OTH.01"
  @@route2 = "P-PDWH-MAINPD.02"
  @@lot = nil

  def test01_setup
    $sv = SiView::MM.new($env)
    $xap = SiView::MM.new("itdc", user: "X-AUTOPROC", password: "fwsftfS3qvpg")
    $pdwhtest = PDWH::Test.new($env, :sv=>$sv) unless $pdwhtest and $pdwhtest.env == $env
    assert 0 == $xap.logon_check
    @@lot = $pdwhtest.new_lot
  end

  def test10_startreserve_and_process
    $xap.claim_process_lot(@@lot, @@proc_params)
    sleep @@separationtime
  end

  def test20_startreserve_and_forcecomplete
    $xap.claim_process_lot(@@lot, @@proc_params.merge(running_hold: true))
    sleep @@separationtime
    $sv.lot_hold_release(@@lot, nil)
    sleep @@separationtime
  end

  def test30_startreserve_and_opestartcancel
    $xap.claim_process_lot(@@lot, @@proc_params.merge(noopecomp: true))
    sleep @@separationtime
    li = $sv.lot_info(@@lot)
    $sv.eqp_opestart_cancel(li.eqp, li.cj)
    $sv.eqp_unload(li.eqp, nil, li.carrier)
    sleep @@separationtime
  end

  def test40_startreserve_and_process
    $xap.claim_process_lot(@@lot, @@proc_params)
    sleep @@separationtime
  end

  def test99_report
    $log.info("lotid: #{@@lot}")
  end
end
