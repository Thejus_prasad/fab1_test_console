=begin 
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

This testcase checks valid state changes from SiView.    

Author: Daniel Steger, 2011-05-16
=end

require 'Test_Xsite_Setup'


# Basic Xsite testcase from which real test cases inherit
#
# This testcase checks valid state changes from SiView

class Test_Xsite_Overdue < Test_Xsite_Setup
    Description = "Basic Xsite testcase from which real test cases inherit"

    @@eqp = "UTC001"
    @@lot1 = "8XYJ46002.000"
    @@lot2 = "T3000TC.00"
    @@openum = "1000.700"
    @@openum2 = "100.200"
    # Time to sleep until overdue
    @@overdue_time = 120
    
    @@checklist = "C0002479"

    def setup
      super
      $setup_ok = ($setup_ok != false)
      assert $setup_ok, "test setup is not correct"
    end
    
    def test100_setup
      $setup_ok = false
      $eis.restart_tool(@@eqp)
      @@chambers = @@chambers.split
      @@chamber = @@chambers.first
      assert $sv.eqp_mode_auto1(@@eqp)
      $setup_ok = true
    end


    def test100_wait_product_sdt_delay
      _prepare
      wo, txtime = _setup_overdue_wo()
      
      assert $xstest.verify_e10state(@@eqp, "4DLY"), " expected state to be 4DLY"
      _complete_pm(wo)
      #
      assert $xstest.verify_e10state_history(@@eqp, 
      ["2WPR", "2NDP", "2NDP", "4DLY"], txtime), "E10 history does not match"
    end

    def test110_wait_product_wait_product      
      _prepare(@@lot1, @@openum)
      cj = $sv.slr(@@eqp, :lot=>@@lot1)
      assert cj, " cannot create CJ"
      wo, txtime = _setup_overdue_wo()
      assert $xstest.verify_e10state(@@eqp, "2WPR"), " expected state to be 2WPR"
      assert $xstest.verify_inhibit(@@eqp), " inhibit expected"
      $sv.slr_cancel(@@eqp, cj)
      assert $xstest.verify_e10state(@@eqp, "4DLY"), " expected state to be 4DLY"
      #
      $log.info "20s for history"
      sleep 10
      assert $xstest.verify_e10state_history(@@eqp, ["2WPR", "2NDP", "2NDP", "4DLY"], txtime), "E10 history does not match"
    end

    def test120_stb_sup_sdt_delay
      _prepare
      assert $xstest.report_verify_e10state(@@eqp, "2NDP"), " expected state to be 2NDP"
      wo, txtime = _setup_overdue_wo()
      assert $xstest.verify_e10state(@@eqp, "4DLY"), " expected state to be 4DLY"
      #
      $log.info "20s for history"
      sleep 10
      assert $xstest.verify_e10state_history(@@eqp, ["4DLY"], txtime), "E10 history does not match"
    end

    def test130_stb_sup_stb_sup
      _prepare(@@lot1, @@openum)
      cj = $sv.slr(@@eqp, :lot=>@@lot1)
      assert cj, " cannot create CJ"
      assert $xstest.report_verify_e10state(@@eqp, "2SUP"), " expected state to be 2SUP"
      wo, txtime = _setup_overdue_wo()
      assert $xstest.verify_e10state(@@eqp, "2SUP"), " expected state to be 2SUP"
      assert $xstest.verify_inhibit(@@eqp), " inhibit expected"
      $sv.slr_cancel(@@eqp, cj)
      assert $xstest.verify_e10state(@@eqp, "4DLY"), " expected state to be 4DLY"
      
      #
      $log.info "20s for history"
      sleep 10
      assert $xstest.verify_e10state_history(@@eqp, 
        ["2SUP", "2NDP", "2NDP", "4DLY"], txtime), "E10 history does not match"
    end

    def test140_productive_sdt_delay
      _prepare(@@lot1, @@openum)
      cj = cj = $sv.claim_process_lot(@@lot1, :eqp=>@@eqp, :noopecomp=>true, :nounload=>true, :mode=>"Auto-1", :claim_carrier=>true)
      assert cj, " cannot create CJ"
      assert $xstest.report_verify_e10state(@@eqp, "1PRD"), " expected state to be 1PRD"
      wo, txtime = _setup_overdue_wo()
      assert $xstest.verify_e10state(@@eqp, "1PRD"), " expected state to be 1PRD"
      assert $xstest.verify_inhibit(@@eqp), " inhibit expected"
      assert $xstest.report_verify_e10state(@@eqp, "2WPR"), " expected state to be 2WPR"
      assert $sv.eqp_opecomp(@@eqp, cj), "Failed to complete operation"
      assert $xstest.verify_e10state(@@eqp, "4DLY"), " expected state to be 4DLY"
      #
      $log.info "20s for history"
      sleep 10
      assert $xstest.verify_e10state_history(@@eqp, 
        ["1PRD", "2WPR", "2NDP", "2NDP", "4DLY"], txtime), "E10 history does not match"
    end

        
    def test141_productive_pm_qual
      _prepare(@@lot1, @@openum)
      cj = cj = $sv.claim_process_lot(@@lot1, :eqp=>@@eqp, :noopecomp=>true, :nounload=>true, :mode=>"Auto-1", :claim_carrier=>true)
      assert cj, " cannot create CJ"
      assert $xstest.report_verify_e10state(@@eqp, "1PRD"), " expected state to be 1PRD"
      txtime = Time.now
      wo1 = $xs.create_work_order(@@eqp, :overdue=>Time.now + @@overdue_time, :activity=>"PM", :subactivity=>"MAINT")
      wo2, txtime2 = _setup_overdue_wo(:activity=>"QUAL", :subactivity=>"OTHER")
      
      assert $xstest.verify_e10state(@@eqp, "1PRD"), " expected state to be 1PRD"
      assert $xstest.verify_inhibit(@@eqp), " inhibit expected"
      #
      assert_equal({}, $xs.cancel_work_order(wo1), "failed to cancel work order")
      assert $xstest.verify_e10state(@@eqp, "1PRD"), " expected state to be 1PRD"
      assert $xstest.verify_inhibit(@@eqp), " inhibit expected"
      #      
      assert $xstest.report_verify_e10state(@@eqp, "2WPR"), " expected state to be 2WPR"
      assert $sv.eqp_opecomp(@@eqp, cj), "Failed to complete operation"
      assert $xstest.verify_e10state(@@eqp, "4QUL"), " expected state to be 4DLY"
      #
      $log.info "20s for history"
      sleep 10
      assert $xstest.verify_e10state_history(@@eqp, 
        ["1PRD", "1PRD", "2WPR", "2NDP", "2NDP", "4QUL"], txtime), "E10 history does not match"
    end
    
    def test142_productive_pm_online
      _prepare(@@lot1, @@openum)
      cj = cj = $sv.claim_process_lot(@@lot1, :eqp=>@@eqp, :noopecomp=>true, :nounload=>true, :mode=>"Auto-1", :claim_carrier=>true)
      assert cj, " cannot create CJ"
      assert $xstest.report_verify_e10state(@@eqp, "1PRD"), " expected state to be 1PRD"
      txtime = Time.now
      wo1 = $xs.create_work_order(@@eqp, :overdue=>Time.now + @@overdue_time, :activity=>"PM", :subactivity=>"MAINT")
      wo2, txtime2 = _setup_overdue_wo(:activity=>"ONLINE", :subactivity=>"OTHER")
      
      assert $xstest.verify_e10state(@@eqp, "1PRD"), " expected state to be 1PRD"
      assert $xstest.verify_inhibit(@@eqp), " inhibit expected"
      #
      assert_equal({}, $xs.cancel_work_order(wo1), "failed to cancel work order")
      assert $xstest.verify_e10state(@@eqp, "1PRD"), " expected state to be 1PRD"
      assert $xstest.verify_inhibit(@@eqp), " inhibit expected"
      #      
      assert $xstest.report_verify_e10state(@@eqp, "2WPR"), " expected state to be 2WPR"
      assert $sv.eqp_opecomp(@@eqp, cj), "Failed to complete operation"
      assert $xstest.verify_e10state(@@eqp, "4QUL"), " expected state to be 4DLY"
      #
      $log.info "20s for history"
      sleep 10
      assert $xstest.verify_e10state_history(@@eqp, 
        ["1PRD", "1PRD", "2WPR", "2NDP", "2NDP", "4QUL"], txtime), "E10 history does not match"
    end
    
    def test150_prd_sup_sdt_delay
      _prepare(@@lot1, @@openum)
      cj = $sv.claim_process_lot(@@lot1, :eqp=>@@eqp, :noopecomp=>true, :nounload=>true, :claim_carrier=>true)
      assert $xstest.report_verify_e10state(@@eqp, "1PRD"), " expected state to be 1PRD"
      assert $xstest.report_verify_e10state(@@eqp, "1SUP"), " expected state to be 1SUP"
      wo, txtime = _setup_overdue_wo()
      assert $xstest.verify_inhibit(@@eqp), " inhibit expected"
      assert $xstest.report_verify_e10state(@@eqp, "2SUP"), " expected state to be 2SUP"
      $sv.eqp_opecomp(@@eqp, cj)
      assert $xstest.verify_e10state(@@eqp, "4DLY"), " expected state to be 4DLY"
      #
      assert $xstest.verify_e10state_history(@@eqp, ["1SUP", "2SUP", "2NDP", "2NDP", "4DLY"], txtime), "E10 history does not match"
    end

    def test160_no_dispatch_sdt_delay
      _prepare
      assert $xstest.report_verify_e10state(@@eqp, "2NDP"), " expected state to be 2NDP"
      wo, txtime = _setup_overdue_wo()
      assert $xstest.verify_e10state(@@eqp, "4DLY"), " expected state to be 4QUL"
      #
      $log.info "20s for history"
      sleep 10
      assert $xstest.verify_e10state_history(@@eqp, ["4DLY"], txtime), "E10 history does not match"
    end

    def test170_sdt_delay_sdt_delay
      _prepare
      wo2 = $xs.create_work_order(@@eqp)
      assert wo2, "Failed to create work order."
      $xs.activate_work_order(wo2)
      $xs.activate_work_order(wo2)
      assert $xstest.verify_e10state(@@eqp, "4DLY"), " expected state to be 4QUL"
      wo, txtime = _setup_overdue_wo()
      assert $xstest.verify_e10state(@@eqp, "4DLY"), " expected state to be 4QUL"
      #
      $log.info "20s for history"
      sleep 10
      assert $xstest.verify_e10state_history(@@eqp, [], txtime), "E10 history does not match"
    end
    
    def test180_udt_cont_udt_cont
      _prepare
      wr = $xs.create_work_request(@@eqp, :activity=>"UNSCHED", :subactivity=>"CONT")
      wo2 = $xs.create_work_order(@@eqp, :activity=>"UNSCHED", :subactivity=>"CONT")
      assert $xstest.verify_e10state(@@eqp, "5DLY"), " expected state to be 5DLY"
      $xs.add_work_request_to_work_order(wr, wo2)
      $xs.clockon_work_order(wo2)
      assert $xstest.verify_e10state(@@eqp, "5CON"), " expected state to be 5CON"
      #
      wo, txtime = _setup_overdue_wo()
      assert $xstest.verify_e10state(@@eqp, "5CON"), " expected state to be 5CON"
      #
      $log.info "20s for history"
      sleep 10
      assert $xstest.verify_e10state_history(@@eqp, [], txtime), "E10 history does not match"
    end

    def test190_udt_cm_qual_udt_cm_qual
      _prepare(@@lot2, @@openum2)
      wr = $xs.create_work_request(@@eqp, :activity=>"UNSCHED", :subactivity=>"CONT")
      wo2 = $xs.create_work_order(@@eqp, :activity=>"UNSCHED", :subactivity=>"CONT")
      assert $xstest.verify_e10state(@@eqp, "5DLY"), " expected state to be 5DLY"
      $xs.add_work_request_to_work_order(wr, wo2)
      $xs.clockon_work_order(wo2)
      assert $xstest.verify_e10state(@@eqp, "5CON"), " expected state to be 5CON"
      $xs.copy_checklist_to_work_order(wo2, @@checklist)
      $xs.activate_checklist(wo2)
      assert $xstest.verify_e10state(@@eqp, "5CMQ"), " expected state to be 5CMQ"
      assert $xstest.verify_e10state(@@eqp, "5CMQ"), " expected state to be 5CMQ"
      #
      cj = $sv.claim_process_lot(@@lot2, :eqp=>@@eqp, :noopecomp=>true, :nounload=>true, :claim_carrier=>true)
      wo, txtime = _setup_overdue_wo()
      assert !$xstest.verify_inhibit(@@eqp), " no inhibit expected"
      assert $xstest.verify_e10state(@@eqp, "5CMQ"), " expected state to be 5CMQ"
      assert $sv.eqp_opecomp(@@eqp, cj), "Failed to complete operation"
      assert $xstest.verify_e10state(@@eqp, "5CMQ"), " expected state to be 5CMQ"
      $xs.begin_checklist(wo2)
      $xs.complete_checkliststep(wo2)
      $xs.complete_checklist(wo2)
      $xs.complete_work_order(wo2)
      assert $xstest.verify_e10state(@@eqp, "4DLY"), " expected state to be 4DLY"
      #
      $log.info "20s for history"
      sleep 10
      assert $xstest.verify_e10state_history(@@eqp, ["2WPR", "2NDP", "2NDP", "4DLY"], txtime), "E10 history does not match"
    end

    def test191_sdt_pm_qual_sdt_pm_qual
      self._prepare
      wo2 = $xs.create_work_order(@@eqp, :activity=>"PM", :subactivity=>"MAINT")
      $xs.activate_work_order(wo2)
      $xs.activate_work_order(wo2)
      assert $xstest.verify_e10state(@@eqp, "4DLY"), " expected state to be 4DLY"
      $xs.clockon_work_order wo2
      assert $xstest.verify_e10state(@@eqp, "4MTN"), " expected state to be 4MTN"
      $xs.copy_checklist_to_work_order(wo2, @@checklist)
      $xs.activate_checklist(wo2)
      assert $xstest.verify_e10state(@@eqp, "4PMQ"), " expected state to be 4PMQ"
      wo, txtime = self._setup_overdue_wo()
      assert $xstest.verify_e10state(@@eqp, "4PMQ"), " expected state to be 4PMQ"
      #CR45924
      self._complete_pm(wo, "4PMQ", "4PMQ")
      #
      $xs.complete_checkliststep(wo2)
      $xs.complete_checklist(wo2)
      $xs.complete_work_order(wo2)
      #
      $log.info "20s for history"
      sleep 10
      assert $xstest.verify_e10state_history(@@eqp, ["2WPR", "2NDP", "2NDP", "4DLY"], txtime), "E10 history does not match"
    end

    
    def _setup_overdue_wo(params={})
      txtime = Time.now
      wo = $xs.create_work_order(@@eqp, params.merge({:overdue=>Time.now + @@overdue_time}))
      assert wo, "Failed to create work order."

      sleep_time = @@overdue_time + 30
      $log.info "Sleeping #{sleep_time}"
      sleep sleep_time

      return wo, txtime
    end
    
    def _complete_pm(wo, start_state="4MTN", end_state="2WPR")
      $xs.clockon_work_order wo
      assert $xstest.verify_e10state(@@eqp, start_state), " expected state to be #{start_state}"
      $xs.copy_checklist_to_work_order(wo, @@checklist)
      $xs.activate_checklist(wo)
      assert $xstest.verify_e10state(@@eqp, "4PMQ"), " expected state to be 4PMQ"
      $xs.begin_checklist(wo)
      $xs.complete_checkliststep(wo)
      $xs.complete_checklist(wo)
      $xs.complete_work_order(wo)
      assert $xstest.verify_e10state(@@eqp, end_state), " expected state to be #{end_state}"
    end
end
