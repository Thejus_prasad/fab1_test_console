=begin 
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

This testcase checks valid state changes from SiView.    

Author: Daniel Steger, 2011-05-16
=end

require 'Test_Xsite_Setup'

# Xsite Autokitting test case

class Test_Xsite_AutoBuildKit < Test_Xsite_Setup
   Description = "Auto Build Kit Scenario"

  @@eqp = "QACLUST02"
  @@openum = "1000.700"
  # Time to sleep until overdue
  @@waittime = 120
  
  @@checklist_kit_02_due = "C0002482"
  @@checklist_kit_02_early = "C0002483"
  @@checklist_kit_02_late = "C0002484"
  @@checklist_kit_02_manual = "C0002485"
  @@checklist_kit_notfound = "C0002486"
  @@checklist_other = "C0002502"
  @@checklist_kit_09_due = "C0002504"
  @@checklist_kit_09_early = "C0002503"
  
  #STBonly
  @@checklist_kit_09_stb_due = "C0002506"
  @@checklist_kit_09_stb_early = "C0002505"


  @@carrier1 = "TQ30"
  @@carrier1_empty = "TQ31"

  @@carrier2 = "TQ32"
  @@carrier2_empty = "TQ33"

  
  @@spec = "M07-00020049"
  @@spec_kit1 =   {
    "U-100-CLUSTER-QAXSITECLUSTER.01" => [3],
    "U-100-CLUSTER-QAXSITECLUSTERCHAL.01" => [5, 9],
    "U-100-CLUSTER-QAXSITECLUSTERCHAR.01" => [6, 10],
  }
  @@spec_kit1_eng =   {
    "U-100-CLUSTER-QAXSITECLUSTERCHAL.01" => [5],
    "U-100-CLUSTER-QAXSITECLUSTERCHAR.01" => [10],
  }
  @@spec_kit2 =   {
    "U-200-CLUSTER-QAXSITECLUSTER.01" => [3],
    "U-200-CLUSTER-QAXSITECLUSTERCH1L.01" => [5, 9],
    "U-200-CLUSTER-QAXSITECLUSTERCH1R.01" => [6, 10],
  }

  @@spec_kit3 =   {
    "U-900-CLUSTER-QAXSITECLUSTER.01" => [2],
    "U-900-CLUSTER-QAXSITECLUSTERCH1L.01" => [4, 6],
    "U-900-CLUSTER-QAXSITECLUSTERCH1R.01" => [8, 10],
  }

  

  # source products
  @@products = "B-%-CLUSTER.01"
  @@product2 = "B-200-CLUSTER.01"
  @@product9 = "B-900-CLUSTER.01"
  
  @@product_used = "U-200-CLUSTER-QAXSITECLUSTER.01"
  
  def test800_setup
    $rtdempty.file = "/apf/apfhome/models/fab8/class/qa_empty.csv"
    
    # Sorter
    assert $sv.eqp_mode_auto1(@@sorter)

    assert $sv.eqp_mode_auto1(@@eqp)
    
    $setup_ok = true
  end

  #AutoBuildKit
  def _kit_struct1(checklist, timing)
    return [{ :kit=>"QA-KIT01-2", :spec_kit=>@@spec_kit2, :carriers=>[@@carrier1, @@carrier1_empty], :timing=> timing, :checklist=>checklist, :product=>@@product2 }]
  end

  #2 AutoBuildKit
  def _kit_struct2(checklist1, timing1, checklist2, timing2)
    return [{ :kit=>"QA-KIT01-2",  :spec_kit=>@@spec_kit2, :carriers=>[@@carrier1, @@carrier1_empty], :timing=> timing1, :checklist=>checklist1, :product=>@@product2 },
      { :kit=>"QA-KIT08-2", :spec_kit=>@@spec_kit3, :carriers=>[@@carrier2, @@carrier2_empty], :timing=> timing2, :checklist=>checklist2, :product=>@@product9 }
    ]
  end

  # AutoBuildKit and AutoSTB
  def _kit_struct3(checklist1, timing1, checklist2, timing2)
    return [{ :kit=>"QA-KIT01-2",  :spec_kit=>@@spec_kit2, :carriers=>[@@carrier1, @@carrier1_empty], :timing=> timing1, :checklist=>checklist1, :product=>@@product2 },
      { :kit=>"QA-KIT08-1", :spec_kit=>@@spec_kit3, :carriers=>[@@carrier2], :timing=> timing2, :checklist=>checklist2, :product=>@@product9, :stbonly=>true }
    ]
  end

  # ManualBuildKit
  def _kit_struct4(checklist, timing)
    return [{ :kit=>"QA-KIT07-2", :spec_kit=>@@spec_kit2, :carriers=>[@@carrier1, @@carrier1_empty], :timing=> timing, :checklist=>checklist, :product=>@@product2 }]
  end

  def test801_kit_not_found_error
    kit = _kit_struct1(@@checklist_kit_notfound, {:earlydue=>Time.now + @@waittime })
    cleanup_kits(kit)
    $sv.eqp_status_change_req @@eqp, "2WPR"
    assert $xstest.verify_e10state(@@eqp, "2WPR"), " expected state to be #{"2WPR"}"
    wo, txtime = setup_wo(kit)
    assert($xstest.verify_checklist_step_not_complete(wo, 0, "1", "Return Code: 204", :comment=>"Kit definition not found"),
      "checklist verficiation failed")
  end
  
  def test802_autokit_happy
    run_buildkit_test("2WPR", _kit_struct1(@@checklist_kit_02_early, {:earlydue=>Time.now + @@waittime }))
  end
  
  def test803_manual_kit_happy
    wo = run_buildkit_test("2WPR", _kit_struct4(@@checklist_kit_02_manual, {:earlydue=>Time.now + @@waittime }), nil, true, true)
    ctxs = $wbtqtest.mds.contexts(order_no: wo.gsub("P", "80"))
    assert ctxs.size == 1, "expected pending kit context #{ctxs.inspect}"
    #TODO: Add work order completion
    $log.info("Please open Xsite, clockon to WO #{wo} and execute the STB trigger. Then complete the WO.")
    $log.info("Press any key when completed.")
    gets
    assert $xstest.verify_checklist_completed wo, 0, "OK"
    ctxs = $wbtqtest.mds.contexts(order_no: wo.gsub("P", "80"))
    assert ctxs.size == 0, "expected no pending kit context #{ctxs.inspect}"
  end

  def test804_manual_override
    kit = _kit_struct1(@@checklist_kit_02_early, {:earlydue=>Time.now + @@waittime })
    cleanup_kits(kit)
    $sv.eqp_status_change_req @@eqp, "2WPR"
    assert $xstest.verify_e10state(@@eqp, "2WPR"), " expected state to be 2WPR"

    wo, txtime = setup_wo(kit)
    # Fab8 rule to determine order number
    ctxs = $wbtqtest.mds.contexts(order_no: wo.gsub("P", "80"))
    assert ctxs.size == 1, "expected only one order #{ctxs.inspect} in WBTQ"
    assert $wbtqtest.verify_sjc_request(ctxs[0].guid, @@spec_kit2, @@carrier1), "invalid sort request"
    assert $xstest.verify_checklist_step_not_complete(wo, 0, "1", "Return Code: 903", :comment=>"SortJobController sort request was created"), "invalid wo comment"
    $log.info "SJ Request #{ctxs[0].srid}"
    # Make Sort Job creation fail

    ##$sv._get_ports_in_pgs(@@sorter).each {|p| $sv.eqp_port_status_change(@@sorter, p, "LoadComp")}
    $sv.eqp_info(@@sorter).ports.each {|p| $sv.eqp_port_status_change(@@sorter, p.port, "LoadComp") if p.pg == @@pg}
    
    srid = ctxs[0].srid
    # replace empty
    $sjcqa.replace srid, "XXX", "BEOL"

    # send execute SOAP message
    assert $sjcexec.execute(srid, @@sorter, @@pg), "error sending SOAP message"

    sleep 10
    assert $xstest.verify_checklist_step_not_complete(wo, 0, "1", "Return Code: 301", :comment=>"Automated wafer sort operation failed."), "invalid wo comment"

    $log.info "Please clock on WO #{wo} in Xsite, click 1) Clean up and 2) Build kit"
    $log.info "Press any key"
    gets

    ##$sv._get_ports_in_pgs(@@sorter).each {|p| $sv.eqp_port_status_change(@@sorter, p, "LoadReq")}
    $sv.eqp_info(@@sorter).ports.each {|p| $sv.eqp_port_status_change(@@sorter, p.port, "LoadReq") if p.pg == @@pg}
    ctxs = $wbtqtest.mds.contexts(order_no: ctxs[0].order)
    $log.info "SJ Request #{ctxs[0].srid}"
    process_build_kit_request(ctxs[0].srid, @@carrier1_empty)
    $log.info "Please complete Checklist and WO #{wo} in Xsite"
    $log.info "Press any key"
    gets
    assert $xstest.verify_checklist_completed(wo, 0, "OK")
  end
  
  
  def test805_manual_other_checklist
    kit = _kit_struct1(@@checklist_kit_02_early, {:earlydue=>Time.now + @@waittime, :add_checklist=>@@checklist_other })
    cleanup_kits(kit)
    $sv.eqp_status_change_req @@eqp, "2WPR"
    assert $xstest.verify_e10state(@@eqp, "2WPR"), " expected state to be 2WPR"

    wo, txtime = setup_wo(kit)
    # Fab8 rule to determine order number
    ctxs = $wbtqtest.mds.contexts(order_no: wo.gsub("P", "80"))
    assert ctxs.size == 1, "expected only one order #{ctxs.inspect} in WBTQ"
    assert $wbtqtest.verify_sjc_request(ctxs[0].guid, @@spec_kit2, @@carrier1), "invalid sort request"
    assert $xstest.verify_checklist_step_not_complete(wo, 0, "1", "Return Code: 903", :comment=>"SortJobController sort request was created"), "invalid wo comment"
    #
    ctxs = $wbtqtest.mds.contexts(order_no: ctxs[0].order)
    $log.info "SJ Request #{ctxs[0].srid}"
    process_build_kit_request(ctxs[0].srid, @@carrier1_empty)
    $log.info "Please complete 2nd Checklist and WO #{wo} in Xsite"
    $log.info "Press any key"
    gets
    assert $xstest.verify_checklist_completed(wo, 0, "OK")
  end

  ## All Early Trigger Tests
  
  def test811_stb_waitproduct_early
    run_buildkit_test("2WPR", _kit_struct1(@@checklist_kit_02_early, {:earlydue=>Time.now + @@waittime }))
  end
  
  def test812_stb_waitproduct_sup_early    
    run_buildkit_test("2SUP", _kit_struct1(@@checklist_kit_02_early, {:earlydue=>Time.now + @@waittime })) 
  end  
  
  def test813_stb_productive_early    
    run_buildkit_test("1PRD", _kit_struct1(@@checklist_kit_02_early, {:earlydue=>Time.now + @@waittime }))  
  end
  
  def test814_stb_productive_sup_early    
    run_buildkit_test("1SUP", _kit_struct1(@@checklist_kit_02_early, {:earlydue=>Time.now + @@waittime }), "1PRD")  
  end

  def test815_no_dispatch_early
    run_buildkit_test("2NDP", _kit_struct1(@@checklist_kit_02_early, {:earlydue=>Time.now + @@waittime }), nil, false)
  end

  def test816_sdt_delay_early
    run_buildkit_test("4DLY", _kit_struct1(@@checklist_kit_02_early, {:earlydue=>Time.now + @@waittime }), nil, false)
  end

  def test817_udt_delay_early
    run_buildkit_test("5DLY", _kit_struct1(@@checklist_kit_02_early, {:earlydue=>Time.now + @@waittime }), nil, false)
  end

  ## All Due Trigger Tests
  
  def test821_stb_waitproduct_due
    run_buildkit_test("2WPR", _kit_struct1(@@checklist_kit_02_due, {:due=>Time.now + @@waittime }))  
  end
  
  def test822_stb_waitproduct_sup_due
    run_buildkit_test("2SUP", _kit_struct1(@@checklist_kit_02_due, {:due=>Time.now + @@waittime }))  
  end  
  
  def test823_stb_productive_due
    run_buildkit_test("1PRD", _kit_struct1(@@checklist_kit_02_due, {:due=>Time.now + @@waittime }))  
  end
  
  def test824_stb_productive_sup_due  
    run_buildkit_test("1SUP", _kit_struct1(@@checklist_kit_02_due, {:due=>Time.now + @@waittime }), "1PRD")  
  end
  
  def test825_no_dispatch_due
    run_buildkit_test("2NDP", _kit_struct1(@@checklist_kit_02_due, {:due=>Time.now + @@waittime }), nil, false)
  end

  def test826_sdt_delay_due
    run_buildkit_test("4DLY", _kit_struct1(@@checklist_kit_02_due, {:due=>Time.now + @@waittime }), nil, false)
  end

  def test827_udt_delay_due
    run_buildkit_test("5DLY", _kit_struct1(@@checklist_kit_02_due, {:due=>Time.now + @@waittime }), nil, false)
  end

  def test830_2_build_kits
    run_buildkit_test("2WPR", _kit_struct2(@@checklist_kit_02_early, {:earlydue=>Time.now + @@waittime },
      @@checklist_kit_09_early, {:earlydue=>Time.now + @@waittime }))
  end

  def test831_2_build_kits_different_trigger
    run_buildkit_test("2WPR", _kit_struct2(@@checklist_kit_02_early, {:earlydue=>Time.now + @@waittime/2 },
      @@checklist_kit_09_due, {:due=>Time.now + @@waittime }))
  end
  
  def test832_build_kit_stb_only
    run_buildkit_test("2WPR", _kit_struct3(@@checklist_kit_02_early, {:earlydue=>Time.now + @@waittime },
      @@checklist_kit_09_stb_early, {:earlydue=>Time.now + @@waittime }))
  end

  def test833_build_kit_stb_only_different_trigger
    run_buildkit_test("2WPR", _kit_struct3(@@checklist_kit_02_early, {:earlydue=>Time.now + @@waittime/2 },
      @@checklist_kit_09_stb_due, {:due=>Time.now + @@waittime }))
  end
  
  def XXXtest899_kit_stress
    cleanup_kits(_kit_struct1(@@checklist_kit_02_early, {:earlydue=>Time.now + @@waittime }))
    e10state = "2WPR"
    timing = {:due=>Time.now + 300 }
    4.times do |i|
      $sv.eqp_status_change_req @@eqp, e10state    
      assert $xstest.verify_e10state(@@eqp, e10state), " expected state to be #{e10state}"     
      wo = $xs.create_work_order(@@eqp, timing)
      $xs.copy_checklist_to_work_order(wo, @@checklist_kit_02_due) 
      $log.info "Set #{wo} for #{@@eqp}, #{timing}"
    end
  end

  def cleanup_kits(kits)
    $setup_ok = false

    # Cleanup
    _info = $sv.eqp_info(@@eqp)
    _info.rsv_cjs.each {|cj| $sv.slr_cancel(@@eqp, cj)}
    _info.cjs.each {|cj| $sv.eqp_opestart_cancel(@@eqp, cj)}
    _info.ports.each do |p|
      unless p.loaded_carrier == ""
        $sv.eqp_unload(@@eqp, p.port, p.loaded_carrier)
      end
      $sv.eqp_port_status_change @@eqp, p.port, "LoadReq"
    end

    $xstest.cleanup_work_orders(@@eqp)
    $xstest.cleanup_work_requests(@@eqp)

    assert $xstest.set_verify_wait_product(@@eqp), "Cannot set E10 state to wait-product. Please sync manually."

    assert set_carrier_status(kits.map {|k| k[:carriers]}.flatten)
    kits.each do |k|
      car, car_empty = k[:carriers]
      assert($sv.transfer_all_wafers(car_empty, car), "Cannot transfer wafers") if car_empty
      $wbtqtest.rework_monitor_lots(car, product: k[:product])
    end
    assert $wbtqtest.cancel_kit_verify(:eqp=>@@eqp), "error cleaning up WBTQ contexts"

    assert $eis.restart_tool(@@sorter), "error starting #{@@sorter}"

    $setup_ok = true
  end

  def set_carrier_status(carriers)
    # set the specified carriers AVAILABLE, all others NOTAVAILABLE
    carriers = carriers.split if carriers.kind_of?(String)
    ret = true
    # get all carriers with build products
    lots = $sv.lot_list(:product=>@@products)
    all_carriers = lots.collect {|l| $sv.lot_info(l).carrier}.uniq.sort

    all_carriers.delete("")
    # set the specified carriers AVAILABLE, all other carriers NOTAVAILABLE
    (all_carriers + carriers).each {|c|
      status = carriers.member?(c) ? "AVAILABLE" : "NOTAVAILABLE"
      ret &= ($sv.carrier_status_change(c, status, :check=>true) == 0)
    }
    # cancel NPW reserations
    rr = $sv.npw_reservation_state(@@eqp)
    rr.each {|c,x,y| $sv.npw_reserve_cancel(@@eqp, :carrier=>c) if c != ""}
    # cancel SLR reservations
    eqi = $sv.eqp_info(@@eqp)
    eqi.rsv_cjs.each {|cj| $sv.slr_cancel(@@eqp, cj)}
    # cancel sort jobs
    $sv.claim_sj_execute(@@sorter, :cancel=>true)
    # stockin
    carriers.each {|c| $sv.carrier_stockin(c, @@stocker)}

    return ret
  end
  
  
  def setup_wo(kits, params={})
    txtime = Time.now
    _wo_params = params
    kits.each {|k| _wo_params.merge!(k[:timing]) }
    wo = $xs.create_work_order(@@eqp, _wo_params)
    kits.each {|k| $xs.copy_checklist_to_work_order(wo, k[:checklist])}
    $xs.copy_checklist_to_work_order(wo, params[:add_checklist]) if params[:add_checklist]
    assert wo, "Failed to create work order."
    sleep_time = @@waittime + 40
    $log.info "#{wo} created. Sleeping #{sleep_time}"
    sleep sleep_time
    return wo, txtime
  end
    
  def run_buildkit_test(e10state, kits, istate=nil, created=true, manual=false)
    cleanup_kits(kits)
    $sv.eqp_status_change_req @@eqp, istate if istate
    if e10state == "4DLY"
      wo2 = $xs.create_work_order(@@eqp)
      assert wo2, "failed to create work order."
      $xs.activate_work_order(wo2)
      $xs.activate_work_order(wo2)
    elsif e10state == "5DLY"
      wr = $xs.create_work_request(@@eqp)
      assert wr, "failed to create work request"
    else
      $sv.eqp_status_change_req @@eqp, e10state
    end
    assert $xstest.verify_e10state(@@eqp, e10state), " expected state to be #{e10state}"
    wo, txtime = setup_wo(kits)
    # Fab8 rule to determine order number
    ctxs = $wbtqtest.mds.contexts(order_no: wo.gsub("P", "80"))
    kits.each_with_index do |k, i|
      if created
        if k[:stbonly]
          assert $xstest.verify_checklist_completed(wo, i, "OK"), "checklist not completed"
        else
          car, car_empty = k[:carriers]
          assert ctxs.size == kits.count, "expected #{kits.count} orders #{ctxs.inspect} in WBTQ"
          ctx = ctxs.find {|c| c.kit == k[:kit]}
          assert $wbtqtest.verify_sjc_request(ctx.guid, k[:spec_kit], car), "invalid sort request"
          assert $xstest.verify_checklist_step_not_complete(wo, i, "1", "Return Code: 903", :comment=>"SortJobController sort request was created"), "invalid wo comment"
          $log.info "SJ Request #{ctx.srid}"
          process_build_kit_request(ctx.srid, car_empty)
          return wo if manual
          ctxs = $wbtqtest.mds.contexts(order_no: ctx.order)          
          #Kit context should be cleared
          assert $xstest.verify_checklist_completed(wo, i, "OK"), "checklist not completed"
          if i == kits.select {|k| !k[:stbonly]}.count - 1
            assert ctxs.size == 0, "expected no pending kit context #{ctxs.inspect}"
          else
            ctx = ctxs.find {|ctx| ctx.kit == k[:kit]}
            assert_equal "FINISHED", ctx.state, "expected kit context to be completed #{ctx.inspect}"
          end
        end
      else
        assert_equal 0, ctxs.count, "expected no order in WBTQ"
        assert $xstest.verify_checklist_step_not_complete(wo, i, "1", ""), "invalid wo comment, should be empty"
      end
    end
  end
  
  def process_build_kit_request(srid, carrier_empty)
    _csrule = %W(source_carrier_id
      #{carrier_empty}
    )
    assert $rtdempty.write_file(_csrule), "error writing RTD rule file csrule_EMPTY"
    # replace empty
    $sjcqa.replace srid, carrier_empty, "BEOL"
  
    # sorter needs to be clean
    eqi = $sv.eqp_info(@@sorter)
    eqi.ports.each {|p|
      next unless p.mode.start_with?("Auto")
      $sv.eqp_port_status_change(@@sorter, p.port, "LoadReq")
    }

    # send execute SOAP message
    assert $sjcexec.execute(srid, @@sorter, @@pg), "error sending SOAP message"
    sleep 20
    # verify the sorter job has been created and wait for completion
    srs = $sjcdb.sjc_requests(:srid=>srid)
    sjid = srs[0].sjid
    assert sjid, "sorter job has not been created"
    assert $sv.claim_sj_execute(@@sorter)
    $log.info "waiting 150s"
    sleep 150    
  end
end