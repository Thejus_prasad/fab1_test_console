=begin 
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

This testcase checks valid state changes from SiView.    

Author: Daniel Steger, 2011-05-16
=end

require "RubyTestCase"
require 'xsitetest'
require 'dummytools'
require 'wbtqtest'


# Basic Xsite testcase from which real test cases inherit
#
# This testcase checks valid state changes from SiView

class Test_Xsite_Setup < RubyTestCase
  Description = "Basic Xsite testcase from which real test cases inherit"

  @@eqp = "UTC001"
  @@chambers = "PM1 PM2"
  @@lots = "8XYJ46002.000"
  @@operation = "1000.700"
  # Time to sleep until overdue
  @@overdue_time = 120

  @@checklist = "C0002479"
  
  @@sorter = "UTS002"
  @@pg = "PG1"
  @@stocker = "UTSTO110"

  def setup
    super
    $setup_ok = ($setup_ok != false)
    assert $setup_ok, "test setup is not correct"
  end

  def test000_setup
    $setup_ok = false
    #
    ($xs.disconnect; $xstest = nil) if $xs and $xs.env != $env
    $xstest ||= XsiteTest.new($env, :sv=>$sv)
    $xs = $xstest.xs
    $wbtqtest = WBTQ::Test.new($env, sorter: @@sorter, stocker: @@stocker, sv: $sv) unless $wbtqtest and $wbtqtest.env == $env
    $sjccreate = $wbtqtest.sjccreate
    $sjcexec = $wbtqtest.sjcexec
    $sjcdel = $wbtqtest.sjcdelete
    $sjcqa = $wbtqtest.sjcqa
    $sjcdb = $wbtqtest.mds
    $rtdempty = $wbtqtest.rtdempty
    $eis = $wbtqtest.eis

    assert ($xs and $xs.env == $env)
    $setup_ok = true
  end
  
  def _prepare(lots=[], opnum="", chamber=false)
    # Cleanup eqp
    _info = $sv.eqp_info(@@eqp)
    _info.rsv_cjs.each {|cj| $sv.slr_cancel(@@eqp, cj)}
    _info.cjs.each {|cj| $sv.eqp_opestart_cancel(@@eqp, cj)}
    _info.ports.each do |p|
      unless p.loaded_carrier == ""
        $sv.eqp_unload(@@eqp, p.port, p.loaded_carrier)
      end
      $sv.eqp_port_status_change @@eqp, p.port, "LoadReq"
    end
    $eis.set_cj_chambers(@@eqp)
    # Lots
    lots = lots.split unless lots.kind_of?(Array)
    lots.each {|l|
      l_info = $sv.lot_info l
      $sv.lot_bankin_cancel l if l_info.status == "COMPLETED"
      $sv.lot_hold_release(l, nil) if l_info.status == "ONHOLD"
      $sv.lot_opelocate(l, opnum)
    }
    $xstest.cleanup_work_orders(@@eqp)
    $xstest.cleanup_work_requests(@@eqp)

    assert $xstest.set_verify_wait_product(@@eqp), "Cannot set E10 state to wait-product. Please sync manually."
    return lots
  end

end
