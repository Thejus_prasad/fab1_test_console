=begin 
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

This testcase does simple lot processing in Offline-1 Mode, SiView.  

Author: Daniel Steger, 2011-05-16
=end

require "RubyTestCase"
require 'xsitetest'

# Sample Xsite testcases
#
# This testcase does simple lot processing in Offline-1 Mode, SiView.

class Test_Xsite_LotProcessingOffline < RubyTestCase
    Description = "Sample Xsite testcases"

    @@eqp="UTC001"
    @@lot=""
    @@operation=""
    
    def setup
      super
      #$setup_ok = ($setup_ok != false)
      #assert $setup_ok, "test setup is not correct"
    end

   def prepare_lot
     if @@lot == ""
       _list = $sv.what_next(@@eqp)
       assert _list.length > 0, "No lot found on dispatch list!"
       @@lot = _list[0].lot
       l_info = $sv.lot_info @@lot
       @@operation = l_info.opNo
     else
       l_info = $sv.lot_info @@lot
       $sv.lot_bankin_cancel @@lot if l_info.status == "COMPLETED"
       $sv.lot_opelocate(@@lot, @@operation)
     end     
   end  
    
    
    def test01_setup
      $setup_ok = false
      
      #
      ($xs.disconnect; $xstest = nil) if $xs and $xs.env != $xs
      $xstest ||= XsiteTest.new($env, :sv=>$sv)
      $xs = $xstest.xs
      
      assert ($xs and $xs.env == $env)
      
      # Tool needs to be online-remote
      $sv.eqp_mode_offline1 @@eqp, :notify=>false
      
      assert $xstest.set_verify_wait_product( @@eqp ), "Xsite E10 state does not match, please sync #{@@eqp} manually"
      
      $setup_ok = true
    end
        
    def test10_processlot
      prepare_lot
      
      eqpi, ports, pg, mode, linfo = $sv.claim_process_prepare @@lot, :eqp=>@@eqp, :claim_carrier=>true
      carrier = linfo[0].carrier      
      cj = $sv.eqp_opestart(@@eqp, carrier)
      check_E10state_Xsite "PRD-PRODUCT"
      
      sleep 10
      
      rc = $sv.eqp_opecomp(@@eqp, cj)      
      check_E10state_Xsite "SBY-WT-PRODUCT"
      
      rc = $sv.eqp_unload(@@eqp, ports[0], carrier)
    end
    
    def test10_processlot_cancel
      prepare_lot
      
      eqpi, ports, pg, mode, linfo = $sv.claim_process_prepare @@lot, :eqp=>@@eqp, :claim_carrier=>true      
      carrier = linfo[0].carrier
      cj = $sv.eqp_opestart(@@eqp, carrier)      
      check_E10state_Xsite "PRD-PRODUCT"
      
      sleep 10
      
      rc = $sv.eqp_opestart_cancel(@@eqp, cj)      
      check_E10state_Xsite "SBY-WT-PRODUCT"
      
      rc = $sv.eqp_unload(@@eqp, ports[0], carrier)
    end
    
    def check_E10state_Xsite(expected_state)
      # Check E10 state in Xsite
      sleep 2
      reply = $xs.eqp_check_status @@eqp
      assert_equal expected_state, reply.state
    end
        
    def test99_close
      $xs.disconnect
    end
    
    
end
