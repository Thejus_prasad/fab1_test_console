=begin 
Wireformat for Xsite tests
Author: Daniel Steger

=end

require "RubyTestCase"
require "wireformat"

# Test wire format

class Test__Wireformat < RubyTestCase
  Description = "test wire format"
  
  def test_int
    sample = '(testreply{:448074)::(:0):://l 0 XsiteGetScheduleTxn : value={ class=S4 -3 }' 
    result = {"value"=>-3}
    res = Xsite::Parser.new.parse(sample)
    assert_equal result, res
  end
  
  def test_float
    sample = '(testreply{:448074)::(:0):://l 0 XsiteGetScheduleTxn : value={ class=F8 0.3 }' 
    result = {"value"=>0.3}
    res = Xsite::Parser.new.parse(sample)
    assert_equal result, res
  end
  
  def test_array
    sample = '(testreply{:448074)::(:0):://l 0 XsiteGetScheduleTxn : value={ class=SORTED { class=A1 "Test1" } "Test2"  "Test3" }' 
    result = {"value"=>["Test1", "Test2", "Test3"]}
    res = Xsite::Parser.new.parse(sample)
    pp res
    assert_equal result, res
  end
  
  def test_hash
    sample = '(testreply{:448074)::(:0):://l 0 XsiteGetScheduleTxn : equipmentName="Etcher" reasonCodes={  { class=ASSOC { class=A1  "ActivityType" }  { class=A1 "ASDASD" } } { "Second" "Third" } { "Four" "Third" }} title="Missaligned"'
    result = {"equipmentName"=>"Etcher", "reasonCodes"=>{"ActivityType"=>"ASDASD", "Second"=>"Third", "Four"=>"Third"}, "title"=>"Missaligned"}
    res = Xsite::Parser.new.parse(sample)
    assert_equal result, res
  end

  def test_class
    sample = '(testreply{:448074)::(:0):://l 0 XsiteGetScheduleTxn : value={ class=SORTED { class=ASSOC { class=A1 "20120404 120000000" } { class=XsiteSchedule action=1 timeStamp=6 postCommitTime={ date=20120306 time=062516000 } txnName="XsiteGetScheduleTxn" scheduleType="WorkOrder" number="P0003147" userName="tgallagh" isIterated=F clockonUser={ } } } }'
    cl = Struct.new(:action,:timeStamp,:postCommitTime,:txnName,:scheduleType,:number,:userName,:isIterated,:clockonUser)
    result = {"value"=>{"20120404 120000000"=>cl.new(action="1", timeStamp="6", postCommitTime=Time.parse("Tue Mar 06 06:25:16 +0100 2012"), txnName="XsiteGetScheduleTxn", scheduleType="WorkOrder", number="P0003147", userName="tgallagh", isIterated=false, clockonUser=nil)}}
    res = Xsite::Parser.new.parse(sample)
    assert_equal result, res
  end
  
  def assert_equal(s1, s2, msg="")
    if s1.is_a?(Struct)
      assert s2.is_a?(Struct), "#{s2.inspect} is no class"
      super(s1.to_hash, s2.to_hash, msg)    
    elsif s1.is_a?(Hash)
      assert_instance_of Hash, s2
      s1.each_key {|k| assert_equal(s1[k], s2[k], msg) }
    elsif s1.is_a?(Array)
      assert_instance_of Array, s2
      s1.each_index {|i| assert_equal(s1[i], s2[i], msg) }
    else
      super(s1, s2, msg)
    end  
  end
  
  #load_run_testcase "Test__Logger", "let"
end