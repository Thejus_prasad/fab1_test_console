=begin 
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

This testcase checks valid state changes from SiView.    

Author: Daniel Steger, 2011-05-16
=end

require "RubyTestCase"
require 'xsitetest'

# Sample Xsite testcases (SiView -> Xsite)
#
# This testcase checks valid state changes from SiView.  

class Test_Xsite_E10statechanges_SiView < RubyTestCase
    Description = "Sample Xsite testcases (SiView -> Xsite)"

    @@eqps="UTC001:PM1 SNK29100:MODULE1 AMI2100"    
    @@stockers="STO110 GR21INTER"
    @@siview_e10states="1PRD 2WPR"
    @@xsite_e10states="PRD-PRODUCT SBY-WT-PRODUCT"
        
    @@xsite_waittime = 2
    
    def setup
      super
      $setup_ok = ($setup_ok != false)
      assert $setup_ok, "test setup is not correct"
    end
    
    def test01_setup
      $setup_ok = false
      
      #
      ($xs.disconnect; $xstest = nil) if $xs and $xs.env != $xs
      $xstest ||= XsiteTest.new($env, :sv=>$sv)
      $xs = $xstest.xs
      
      assert ($xs and $xs.env == $env)
      
      @@stockers.split.each do |stocker|        
        # clean up pending work orders
        $xstest.cleanup_work_orders( stocker )
        
        assert $xstest.set_verify_wait_product( stocker, :stocker=>true ), "Xsite E10 state does not match, please sync #{stocker} manually"
      end
      
      @@eqps.split.each do |e|
        eqp, chamber = e.split(':')
        
        # Tool needs to be online-remote
        rc = $sv.eqp_mode_auto1 eqp, :notify=>false
        assert_equal 0, rc, "Please cleanup the equipment #{eqp}"
        
        # clean up pending work orders
        $xstest.cleanup_work_orders( eqp, :chambers=>true )
        
        assert $xstest.set_verify_wait_product( eqp ), "Xsite E10 state does not match, please sync #{eqp} manually"
        if chamber
          assert $xstest.set_verify_wait_product( eqp, :chamber=>chamber ), "Xsite E10 chamber state does not match, please sync #{eqp}:#{chamber} manually"
        end
      end
      $setup_ok = true
    end
        
    def test10_E10statechangereq_tool
      # Change the E10 state of the tool with TxEqpStatusChangeReq
      @@eqps.split.each do |e|
        eqp, chamber = e.split(':')
        e10states = @@siview_e10states.split.zip(@@xsite_e10states.split)           
        e10states.each do |siview, xsite|        
          # E10 state change from SiView
          rc = $sv.eqp_status_change_req eqp, siview
          assert_equal 0, rc
          
          sleep @@xsite_waittime
          
          # Check E10 state in Xsite
          reply = $xs.eqp_check_status eqp
          assert_equal xsite, reply.state
        end      
      end  
    end
    
    def test11_E10statechangereq_stocker
      # Change the E10 state of the stocker/interbay with TxEqpStatusChangeReq
      @@stockers.split.each do |stocker|
        e10states = @@siview_e10states.split.zip(@@xsite_e10states.split)           
        e10states.each do |siview, xsite|        
          # E10 state change from SiView
          rc = $sv.eqp_status_change_req stocker, siview
          assert_equal 0, rc
          
          sleep @@xsite_waittime
          
          # Check E10 state in Xsite
          reply = $xs.eqp_check_status stocker
          assert_equal xsite, reply.state
        end      
      end  
    end
  
    
  
    def test20_E10statechangereq_chamber
      # Change the E10 state of the chamber with TxChamberStatusChangeReq
      @@eqps.split.each do |e|
        eqp, chamber = e.split(':')
        if chamber        
          e10states = @@siview_e10states.split.zip(@@xsite_e10states.split)           
          e10states.each do |siview, xsite|
            # E10 state change from SiView
            rc = $sv.chamber_status_change_req  eqp, chamber, siview
            assert_equal 0, rc
            
            sleep @@xsite_waittime
            
            # Check E10 state in Xsite
            reply = $xs.eqp_check_status "#{eqp}#{chamber}"
            assert_equal xsite, reply.state
          end
        end
      end
    end
    
    
    
    def test99_close
      $xs.disconnect
    end
    
    
end
