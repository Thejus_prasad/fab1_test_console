=begin 
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

This testcase does simple lot processing in Offline-1 Mode, SiView.  

Author: Daniel Steger, 2011-05-16

History:
        2014-03-28 dsteger, dual CMMS support (MSR841753)
=end

require "RubyTestCase"
require 'xsitetest'

# Create a new work order or request in Xsite
class Test_Xsite_WorkOrderCreate < RubyTestCase
    Description = "Sample Xsite testcases"

    @@eqps="UTA005:PM1 BST2601:PM3 AMI2101 EPI2201"
    @@eqp_sappm = "UTC003:PM1"
    
    @@e10states="SDT ENG UDT"
    @@dual_cmms = ""    # nil/OFF - is single CMMS or "ON"
    @@wo_sappm = "" # nil/OFF

   def prepare_lot
     if @@lot == ""
       _list = $sv.what_next(@@eqp)
       assert _list.length > 0, "No lot found on dispatch list!"
       @@lot = _list[0].lot
       l_info = $sv.lot_info @@lot
       @@operation = l_info.opNo
     else
       l_info = $sv.lot_info @@lot
       $sv.lot_bankin_cancel @@lot if l_info.status == "COMPLETED"
       $sv.lot_opelocate(@@lot, @@operation)
     end     
   end  
    
  def test01_setup
    $setup_ok = false
    
    # Environment var check
    assert_equal @@dual_cmms, ($sv.environment_variable[1]["CS_SP_DUALCMMS_ENABLE"] or ""), "incorrect value for DUALCMMS"
    assert_equal @@wo_sappm, ($sv.environment_variable[1]["CS_SP_WO_SAPPM_ENABLE"] or ""), "Work Orders to SAPPM should be disabled"
    if @@dual_cmms != ""
      # check for correct udata setup
      @@eqps.split.each do |e|
        eqp, chamber = e.split(':')
        u = $sv.user_data(:eqp, eqp)["CMMS"] || ""   # not reported when empty string
        assert_equal "Xsite", u, "UDATA CMMS not correctly set in SM to Xsite"
      end
      # sappm tool
      eqp_sappm, chamber = @@eqp_sappm.split(':')
      u = $sv.user_data(:eqp, eqp_sappm)["CMMS"] || ""   # not reported when empty string
      assert_equal "SAPPM", u, "UDATA CMMS not correctly set in SM to Xsite"      
    end
    
    #
    ($xs.disconnect; $xstest = nil) if $xs and $xs.env != $xs
    $xstest ||= XsiteTest.new($env, :sv=>$sv)
    $xs = $xstest.xs
    
    assert ($xs and $xs.env == $env)
    
    $setup_ok = true
  end
  
  def _cleanup
    @@eqps.split.each do |e|
      eqp, chamber = e.split(':')
      
      # Tool needs to be online-remote
      rc = $sv.eqp_mode_auto1 eqp, :notify=>false
      assert_equal 0, rc, "Please cleanup the equipment #{eqp}"
      
      $xstest.cleanup_work_requests eqp, :chambers=>true
      $xstest.cleanup_work_orders eqp, :chambers=>true
      
      assert $xstest.set_verify_wait_product( eqp ), "Xsite E10 state does not match, please sync #{eqp} manually"
      if chamber        
        assert $xstest.set_verify_wait_product( eqp, :chamber=>chamber ), "Xsite E10 chamber state does not match, please sync #{eqp}:#{chamber} manually"
      end
    end  
  end
        
  def test10_work_request_tools
    failed = []
    @@e10states.split.each do |e10state|
      self._cleanup
      @@eqps.split.each do |e|
        _res= _do_request(e, e10state, false)
        failed << "#{e} #{e10state}" unless _res
      end
    end
    assert_equal [], failed, "failed for #{failed.inspect}"
  end
  
  def test20_work_request_chambers
    failed = []
    @@e10states.split.each do |e10state|
      self._cleanup
      @@eqps.split.each do |e|
        _res = _do_request(e, e10state, true)
        failed << "#{e} #{e10state}" unless _res
      end
    end
    assert_equal [], failed, "failed for #{failed.inspect}"
  end
  
  def test30_multiple_work_requests
    failed = []
    self._cleanup
    eqp = @@eqps.split[0].split(':')[0]
    10.times do |i|
      _res = _do_request(eqp, "UDT", false)
      failed << "#{e} #{e10state}" unless _res
      _res = _do_request(eqp, "ENG", false, "5DLY")
      failed << "#{e} #{e10state}" unless _res
    end
    assert_equal [], failed, "failed for #{failed.inspect}"
  end
  
  def test40_work_request_sappm
    ($log.info "Skipped since not in Dual CMMS mode"; return) unless @@dual_cmms == "ON"
    eqp, ch = @@eqp_sappm.split(":")
    title = "QA Test #{Time.now}"
    $sv.CS_work_order_create(eqp, title: title, e10state: "UDT")
    assert_equal 12401, $sv.rc($sv._res), "Work request creation should fail with given error"
    $sv.CS_work_order_create(eqp, title: title, e10state: "UDT", chamber: ch)
    assert_equal 12401, $sv.rc($sv._res), "Work request creation should fail with given error"
  end


  def _do_request(e, e10state, is_chamber, sv_e10state=nil)
    eqp, chamber = e.split(':')
    title = "QA Test #{Time.now}"
    memo = "SiView test"
    params = {:memo=>memo}
    params[:chamber] = chamber if chamber and is_chamber
    res = verify_equal "", $sv.CS_work_order_create(eqp, params.merge({:title=>title, :e10state=>e10state})), "Workorder creation failed for #{e}"
    if e10state == "UDT"
      res &= $xstest.verify_work_request(eqp, title, params)
    elsif e10state == "ENG"
      res &= $xstest.verify_work_order(eqp, title, e10state, sv_e10state ? sv_e10state : "3OTH", params)
    elsif e10state == "SDT"
      res &= $xstest.verify_work_order(eqp, title, "PM", sv_e10state ? sv_e10state : "4DLY", params)
    else
      $log.error("invalid e10state #{e10state}")
      return false
    end
    return res
  end

  def self.shutdown
    $xs.disconnect
  end
    
    
end
