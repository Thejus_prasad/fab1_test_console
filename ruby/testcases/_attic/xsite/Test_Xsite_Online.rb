=begin 
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

This testcase checks valid state changes from SiView.    

Author: Daniel Steger, 2011-05-16
=end

require 'Test_Xsite_Setup'

# Test for online quals
class Test_Xsite_Online < Test_Xsite_Setup
  Description = "Online Quals"

  @@eqp = "UTC001"
  @@chambers = "PM1 PM2"
  @@lot = "8XYJ46002.000"
  @@openum = "1000.700"
  # Time to sleep until overdue
  @@overdue_time = 120

  @@checklist = "C0002479"

  def test200_setup
    $setup_ok = false
    $eis.restart_tool(@@eqp)
    @@chambers = @@chambers.split
    @@chamber = @@chambers.first
    assert $sv.eqp_mode_auto1(@@eqp)
    $setup_ok = true
  end

  def test201_wait_product_sdt_qual
    _prepare
    wo, txtime = _setup_online_qual()
    ###assert $xstest.verify_e10state(@@eqp, "2NDP"), " expected state to be 2NDP"

    ### process WO
    ###$xs.activate_work_order wo
    assert $xstest.verify_e10state(@@eqp, "4QUL"), " expected state to be 4QUL"
    _complete_online_qual(wo)
    #
    $log.info "20s for history"
    sleep 10
    assert $xstest.verify_e10state_history(@@eqp, 
      ["2WPR", "2NDP", "2NDP", "4QUL"], txtime), "E10 history does not match"
  end

  def test202_wait_product_wait_product
    _prepare(@@lot, @@openum)
    cj = $sv.slr(@@eqp, :lot=>@@lot)
    assert cj, " cannot create CJ"
    wo, txtime = _setup_online_qual()
    assert $xstest.verify_e10state(@@eqp, "2WPR"), " expected state to be 2WPR"
    assert $xstest.verify_inhibit(@@eqp), " inhibit expected"
    $sv.slr_cancel(@@eqp, cj)
    assert $xstest.verify_e10state(@@eqp, "4QUL"), " expected state to be 4QUL"
    #
    $log.info "20s for history"
    sleep 10
    assert $xstest.verify_e10state_history(@@eqp, ["2WPR", "2NDP", "2NDP", "4QUL"], txtime), "E10 history does not match"
  end
  
  def test203_no_dispatch_sdt_qual
    _prepare
    assert $xstest.report_verify_e10state(@@eqp, "2NDP"), " expected state to be 2NDP"
    wo, txtime = _setup_online_qual()
    assert $xstest.verify_e10state(@@eqp, "4QUL"), " expected state to be 4QUL"
    #
    $log.info "20s for history"
    sleep 10
    assert $xstest.verify_e10state_history(@@eqp, ["4QUL"], txtime), "E10 history does not match"
  end
  
  def test204_sdt_delay_sdt_delay
    _prepare
    wo2 = $xs.create_work_order(@@eqp)
    assert wo2, "Failed to create work order."
    $xs.activate_work_order(wo2)
    $xs.activate_work_order(wo2)
    assert $xstest.verify_e10state(@@eqp, "4DLY"), " expected state to be 4DLY"
    wo, txtime = _setup_online_qual()
    assert $xstest.verify_e10state(@@eqp, "4DLY"), " expected state to be 4DLY"
    #
    $log.info "20s for history"
    sleep 10
    assert $xstest.verify_e10state_history(@@eqp, [], txtime), "E10 history does not match"
  end
  
  def test205_sdt_qual_sdt_qual
    _prepare
    wo2 = $xs.create_work_order(@@eqp, :activity=>"QUAL", :subactivity=>"OTHER")
    assert wo2, "Failed to create work order."
    $xs.activate_work_order(wo2)
    $xs.activate_work_order(wo2)
    assert $xstest.verify_e10state(@@eqp, "4QUL"), " expected state to be 4QUL"
    wo, txtime = _setup_online_qual()
    assert $xstest.verify_e10state(@@eqp, "4QUL"), " expected state to be 4QUL"
    #
    $log.info "20s for history"
    sleep 10
    assert $xstest.verify_e10state_history(@@eqp, [], txtime), "E10 history does not match"
  end

  def test206_sdt_pm_qual_sdt_pm_qual
    _prepare
    wo2 = $xs.create_work_order(@@eqp)
    assert wo2, "Failed to create work order."
    $xs.activate_work_order(wo2)
    $xs.activate_work_order(wo2)
    assert $xstest.verify_e10state(@@eqp, "4DLY"), " expected state to be 4DLY"
    $xs.clockon_work_order wo2
    assert $xstest.verify_e10state(@@eqp, "4MTN"), " expected state to be 4MTN"
    $xs.copy_checklist_to_work_order(wo2, @@checklist)
    $xs.activate_checklist(wo2)
    assert $xstest.verify_e10state(@@eqp, "4PMQ"), " expected state to be 4PMQ"
    wo, txtime = _setup_online_qual()
    assert $xstest.verify_e10state(@@eqp, "4PMQ"), " expected state to be 4PMQ"
    #
    $log.info "20s for history"
    sleep 10
    assert $xstest.verify_e10state_history(@@eqp, [], txtime), "E10 history does not match"
  end
  
  def test207_stb_sup_sdt_delay_overdue
    _prepare
    assert $xstest.report_verify_e10state(@@eqp, "2SUP"), " expected state to be 2SUP"
    wo, txtime = _setup_online_qual()
    _complete_online_qual(wo, "4QUL", "2WPR")
    
    #
    $log.info "20s for history"
    sleep 10
    assert $xstest.verify_e10state_history(@@eqp, 
      ["2SUP", "2NDP", "2NDP", "4QUL"], txtime), "E10 history does not match"
  end

  def test208_stb_sup_stb_sup_overdue
    _prepare(@@lot, @@openum)
    cj = $sv.slr(@@eqp, :lot=>@@lot)
    assert cj, " cannot create CJ"
    assert $xstest.report_verify_e10state(@@eqp, "2SUP"), " expected state to be 2SUP"
    wo, txtime = _setup_online_qual()
    assert $xstest.verify_e10state(@@eqp, "2SUP"), " expected state to be 2SUP"
    assert $xstest.verify_inhibit(@@eqp), " inhibit expected"
    $sv.slr_cancel(@@eqp, cj)
    assert $xstest.verify_e10state(@@eqp, "4QUL"), " expected state to be 4QUL"
    
    #
    $log.info "20s for history"
    sleep 10
    assert $xstest.verify_e10state_history(@@eqp, 
      ["2SUP", "2NDP", "2NDP", "4QUL"], txtime), "E10 history does not match"
  end
  
  def test209_productive_productive
    _prepare(@@lot, @@openum)
    cj = $sv.claim_process_lot(@@lot, :eqp=>@@eqp, :noopecomp=>true, :nounload=>true, :mode=>"Auto-1")
    assert cj, " cannot create CJ"
    assert $xstest.report_verify_e10state(@@eqp, "1PRD"), " expected state to be 1PRD"
    wo1, txtime1 = _setup_online_qual()
    wo2, txtime2 = _setup_online_qual()
    assert $xstest.verify_e10state(@@eqp, "1PRD"), " expected state to be 2SUP"
    assert $xstest.verify_inhibit(@@eqp), " inhibit expected"

    _complete_online_qual(wo1, "1PRD", "1PRD")
    _complete_online_qual(wo2, "1PRD", "1PRD")
    assert $xstest.verify_inhibit(@@eqp, :count=>0), " no inhibit expected"

    assert $xstest.report_verify_e10state(@@eqp, "2WPR"), " expected state to be 2WPR"
    assert $sv.eqp_opecomp(@@eqp, cj), "Failed to complete operation"
    assert $xstest.verify_e10state(@@eqp, "2WPR"), " expected state to be 4QUL"
    #
    # the reporting is sometimes in wrong order
    $log.info "20s for history"
    sleep 10
    assert $xstest.verify_e10state_history(@@eqp, 
      ["1PRD", "1PRD", "2WPR"], txtime1), "E10 history does not match"
  end
  
  def test210_productive_sdt_delay
    _prepare(@@lot, @@openum)
    cj = $sv.claim_process_lot(@@lot, :eqp=>@@eqp, :noopecomp=>true, :nounload=>true, :mode=>"Auto-1")
    assert cj, " cannot create CJ"
    assert $xstest.report_verify_e10state(@@eqp, "1PRD"), " expected state to be 1PRD"
    wo1 = $xs.create_work_order(@@eqp, :overdue=>Time.now + @@overdue_time)
    wo2, txtime = _setup_online_qual()
    assert $xstest.verify_e10state(@@eqp, "1PRD"), " expected state to be 2SUP"
    assert $xstest.verify_inhibit(@@eqp), " inhibit expected"
    assert $xstest.report_verify_e10state(@@eqp, "2WPR"), " expected state to be 2WPR"
    assert $sv.eqp_opecomp(@@eqp, cj), "Failed to complete operation"
    assert $xstest.verify_e10state(@@eqp, "4DLY"), " expected state to be 4DLY"
    #
    $log.info "20s for history"
    sleep 10
    assert $xstest.verify_e10state_history(@@eqp, 
      ["1PRD", "2WPR", "2NDP", "2NDP", "4DLY"], txtime), "E10 history does not match"
  end

  def test211_prd_sup_prd_sup
    _prepare(@@lot, @@openum)
    cj = $sv.claim_process_lot(@@lot, :eqp=>@@eqp, :noopecomp=>true, :nounload=>true, :mode=>"Auto-1")
    assert cj, " cannot create CJ"
    assert $xstest.report_verify_e10state(@@eqp, "1PRD"), " expected state to be 1PRD"
    assert $xstest.report_verify_e10state(@@eqp, "1SUP"), " expected state to be 1SUP"
    wo1, txtime1 = _setup_online_qual()
    wo2, txtime2 = _setup_online_qual()
    assert $xstest.verify_e10state(@@eqp, "1SUP"), " expected state to be 2SUP"
    assert $xstest.verify_inhibit(@@eqp), " inhibit expected"

    _complete_online_qual(wo1, "1SUP", "1SUP")
    _complete_online_qual(wo2, "1SUP", "1SUP")
    assert $xstest.verify_inhibit(@@eqp, :count=>0), " no inhibit expected"

    assert $xstest.report_verify_e10state(@@eqp, "2WPR"), " expected state to be 2WPR"
    assert $sv.eqp_opecomp(@@eqp, cj), "Failed to complete operation"
    assert $xstest.verify_e10state(@@eqp, "2WPR"), " expected state to be 4QUL"
    #
    # the reporting is sometimes in wrong order
    $log.info "20s for history"
    sleep 10
    assert $xstest.verify_e10state_history(@@eqp,
      ["1SUP", "1SUP", "2WPR"], txtime1), "E10 history does not match"
  end

  def test212_udt_pm_qual_udt_pm_qual
    _prepare
    wr = $xs.create_work_request(@@eqp)
    wo2 = $xs.create_work_order(@@eqp, :activity=>"UNSCHED", :subactivity=>"MAINT")
    $xs.add_work_request_to_work_order(wr, wo2)
    assert $xstest.verify_e10state(@@eqp, "5DLY"), " expected state to be 5DLY"
    $xs.clockon_work_order wo2
    assert $xstest.verify_e10state(@@eqp, "5MTN"), " expected state to be 5MTN"
    $xs.copy_checklist_to_work_order(wo2, @@checklist)
    $xs.activate_checklist(wo2)
    assert $xstest.verify_e10state(@@eqp, "5CMQ"), " expected state to be 5CMQ"
    wo, txtime = _setup_online_qual()
    assert $xstest.verify_e10state(@@eqp, "5CMQ"), " expected state to be 5CMQ"
    #
    $xs.complete_checkliststep(wo2)
    $xs.complete_checklist(wo2)
    $xs.complete_work_order(wo2)    
    _complete_online_qual(wo, "4QUL", "2WPR")
  end

  def test213_stb_sup_qual
    _prepare
    assert $xstest.report_verify_e10state(@@eqp, "2SUP"), " expected state to be 2SUP"
    wo, txtime = _start_online_qual()
    _complete_online_qual(wo, "2SUP", "2SUP")
  end

  def _start_online_qual
    txtime = Time.now
    wo = $xs.create_work_order(@@eqp, :activity=>"ONLINE", :subactivity=>"OTHER")
    assert wo, "Failed to create work order."
    return wo, txtime
  end

  def _setup_online_qual
    txtime = Time.now
    wo = $xs.create_work_order(@@eqp, :activity=>"ONLINE", :subactivity=>"OTHER", :overdue=>Time.now + @@overdue_time)
    assert wo, "Failed to create work order."

    sleep_time = @@overdue_time + 30
    $log.info "Sleeping #{sleep_time}"
    sleep sleep_time

    return wo, txtime
  end
  
  def _complete_online_qual(wo, start_state="4QUL", end_state="2WPR")
    $xs.clockon_work_order wo
    assert $xstest.verify_e10state(@@eqp, start_state), " expected state to be #{start_state}"
    $xs.copy_checklist_to_work_order(wo, @@checklist)
    $xs.activate_checklist(wo)
    assert $xstest.verify_e10state(@@eqp, start_state), " expected state to be #{start_state}"
    $xs.begin_checklist(wo)
    $xs.complete_checkliststep(wo)
    $xs.complete_checklist(wo)
    $xs.complete_work_order(wo)
    assert $xstest.verify_e10state(@@eqp, end_state), " expected state to be #{end_state}"
  end 
end
