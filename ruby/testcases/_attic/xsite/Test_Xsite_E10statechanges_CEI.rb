=begin 
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

This testcase checks valid state changes from SiView.    

Author: Daniel Steger, 2011-05-16
=end

require "RubyTestCase"
require 'xsitetest'

# Sample Xsite testcases (SiView -> Xsite)
#
# This testcase checks valid state changes from SiView.  

class Test_Xsite_E10statechanges_CEI < RubyTestCase
    Description = "Sample Xsite testcases (SiView -> Xsite)"

    @@eqps="UTC001:PM2"
    
    @@siview_ei_e10states= "2WPR 1PRD 1SUP 1PRD 2WPR 2SUP 1SUP 2SUP 2WPR"
    @@xsite_waittime = 2
    
    $setup_ok = nil
    
    def setup
      super
      $setup_ok = ($setup_ok != false)
      assert $setup_ok, "test setup is not correct"
    end
    
    def test01_setup
      $setup_ok = false
      
      #
      ($xs.disconnect; $xstest = nil) if $xs and $xs.env != $xs
      $xstest ||= XsiteTest.new($env, :sv=>$sv)
      $xs = $xstest.xs
      
      assert ($xs and $xs.env == $env)
      
      @@eqps.split.each do |e|
        eqp, chamber = e.split(':')
        
        # Tool needs to be online-remote
        rc = $sv.eqp_mode_auto1 eqp, :notify=>false
        assert_equal 0, rc, "Please cleanup the equipment #{eqp}"

        assert $xstest.set_verify_wait_product( eqp ), "Xsite E10 state does not match, please sync manually"
        if chamber
          assert $xstest.set_verify_wait_product( eqp, :chamber=>chamber ), "Xsite E10 chamber state does not match, please sync manually"
        end
       
      end
      $setup_ok = true
    end
        
      
    def test12_E10statechangerpt_tool
      # Change the E10 state of the tool with TxEqpStatusChangeRpt
      @@eqps.split.each do |e|
        eqp, chamber = e.split(':')
        e10states = @@siview_ei_e10states.split           
        e10states.each do |siview|  
          # E10 state change from SiView
          rc = $sv.eqp_status_change_rpt eqp, siview
          assert_equal 0, rc
          
          sleep @@xsite_waittime
          
          # Check E10 state in Xsite
          reply = $xs.eqp_check_status eqp
          assert_equal $e10map[siview], reply.state
        end      
      end
    end
  
        
    def test21_E10statechangerpt_chamber
      # Change the E10 state of the chamber with TxChamberStatusChangeRpt
      @@eqps.split.each do |e|
        eqp, chamber = e.split(':')
        if chamber
          e10states = @@siview_ei_e10states.split     
          e10states.each do |siview|
            # E10 state change from SiView
            rc = $sv.chamber_status_change_rpt  eqp, chamber, siview
            assert_equal 0, rc
            
            sleep @@xsite_waittime
            
            # Check E10 state in Xsite
            reply = $xs.eqp_check_status "#{eqp}#{chamber}"
            assert_equal $e10map[siview], reply.state
          end
        end
      end      
    end
    
    def test99_close
      $xs.disconnect
    end
    
    
end
