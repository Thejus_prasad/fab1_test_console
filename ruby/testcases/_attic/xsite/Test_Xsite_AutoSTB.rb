=begin 
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

This testcase checks valid state changes from SiView.    

Author: Daniel Steger, 2011-05-16
=end

require 'Test_Xsite_Setup'


# Xsite Autokitting test case

class Test_Xsite_AutoSTB < Test_Xsite_Setup
   Description = "Auto STB Scenario"

  @@eqp = "QACLUST01"
  @@openum = "1000.700"
  # Time to sleep until overdue
  @@waittime = 180
  
  @@checklist_kit_01_due = "C0002509"
  @@checklist_kit_01_early = "C0002508"
  @@checklist_kit_notfound = "C0002501"
  @@checklist_kit_01_manual = "C0002510"
  @@checklist_other = "C0002502"
  @@checklist_kit_09_due = "C0002506"
  @@checklist_kit_09_early = "C0002505"

  @@carrier1 = "TQ34"
  @@carrier2 = "TQ35"
  
  @@spec = "M07-00020049"
  @@spec_kit1 =   {
    "U-100-CLUSTER-QAXSITECLUSTER.01" => [3],
    "U-100-CLUSTER-QAXSITECLUSTERCHAL.01" => [5, 9],
    "U-100-CLUSTER-QAXSITECLUSTERCHAR.01" => [6, 10],
  }
  @@spec_kit1_eng =   {
    "U-100-CLUSTER-QAXSITECLUSTERCHAL.01" => [5],
    "U-100-CLUSTER-QAXSITECLUSTERCHAR.01" => [10],
  }
  @@spec_kit2 =   {
    "U-200-CLUSTER-QAXSITECLUSTER.01" => [3],
    "U-200-CLUSTER-QAXSITECLUSTERCH1L.01" => [5, 9],
    "U-200-CLUSTER-QAXSITECLUSTERCH1R.01" => [6, 10],
  }
  
  @@spec_kit3 =   {
    "U-900-CLUSTER-QAXSITECLUSTER.01" => [3],
    "U-900-CLUSTER-QAXSITECLUSTERCH1L.01" => [5, 9],
    "U-900-CLUSTER-QAXSITECLUSTERCH1R.01" => [6, 10],
  }

  # source products
  @@products = "B-%-CLUSTER.01"
  @@product1 = "B-100-CLUSTER.01"
  @@product9 = "B-900-CLUSTER.01"
  @@product_used = "U-100-CLUSTER-QAXSITECLUSTER.01"
  
  
  #AutoSTB
  def _kit_struct1(checklist, timing)
    return [{ :kit=>"QA-KIT06-1", :spec_kit=>@@spec_kit1, :carriers=>[@@carrier1], :timing=> timing, :checklist=>checklist, :product=>@@product1 }]
  end

  #2 AutoSTB
  def _kit_struct2(checklist1, timing1, checklist2, timing2)
    return [{ :kit=>"QA-KIT06-1",  :spec_kit=>@@spec_kit1, :carriers=>[@@carrier1], :timing=> timing1, :checklist=>checklist1, :product=>@@product1 },
      { :kit=>"QA-KIT08-1", :spec_kit=>@@spec_kit3, :carriers=>[@@carrier2], :timing=> timing2, :checklist=>checklist2, :product=>@@product9 }
    ]
  end

  # ManualSTB
  def _kit_struct3(checklist, timing)
    return [{ :kit=>"QA-KIT07-1", :spec_kit=>@@spec_kit1, :carriers=>[@@carrier1], :timing=> timing, :checklist=>checklist, :product=>@@product1 }]
  end

  
  def test900_setup
    $setup_ok = false
    assert $sv.eqp_mode_auto1(@@eqp)

    $setup_ok = true
  end

  def test901_kit_not_found_error
    kit = _kit_struct1(@@checklist_kit_notfound, {:earlydue=>Time.now + @@waittime })
    cleanup_kits(kit)
    $sv.eqp_status_change_req @@eqp, "2WPR"
    assert $xstest.verify_e10state(@@eqp, "2WPR"), " expected state to be #{"2WPR"}"
    wo, txtime = setup_wo(kit)    
    assert($xstest.verify_checklist_step_not_complete(wo, 0, "1", "Return Code: 204", :comment=>"Kit definition not found"),
      "checklist verficiation failed")
  end
  
  def test902_autokit_happy
    run_autostb_test("2WPR", _kit_struct1(@@checklist_kit_01_early, {:earlydue=>Time.now + @@waittime }))
  end
  
  def test903_manualkit_happy
    run_autostb_test("2WPR", _kit_struct3(@@checklist_kit_01_manual, {:earlydue=>Time.now + @@waittime }))
  end

  def test904_manual_override
    kit = _kit_struct1(@@checklist_kit_01_early, {:earlydue=>Time.now + @@waittime })
    cleanup_kits(kit)
    $sv.eqp_status_change_req @@eqp, "2WPR"
    assert $xstest.verify_e10state(@@eqp, "2WPR"), " expected state to be 2WPR"
    
    # Make STB fail
    $sv.carrier_status_change @@carrier1, "NOTAVAILABLE"
    wo, txtime = setup_wo(kit)

    # Fab8 rule to determine order number
    ctxs = $wbtqtest.mds.contexts(order_no: wo.gsub("P", "80"))
    assert ctxs.size == 1, "expected only one order #{ctxs.inspect} in WBTQ"
    assert $xstest.verify_checklist_step_not_complete(wo, 0, "1", "Return Code: 208", :comment=>"Not enough source wafer available"), "invalid wo comment"

    $sv.carrier_status_change @@carrier1, "AVAILABLE"
    $log.info "Please complete Checklist and WO #{wo} in Xsite"
    $log.info "Press any key"
    gets
    assert $xstest.verify_checklist_completed(wo, 0, "OK")    
    ctxs = $wbtqtest.mds.contexts(order_no: ctxs[0].order)
    assert ctxs.size == 0, "expected only one order #{ctxs.inspect} in WBTQ"
  end

  def test905_manual_other_checklist
    kit = _kit_struct1(@@checklist_kit_01_early, {:earlydue=>Time.now + @@waittime, :add_checklist=>@@checklist_other })
    wo = run_autostb_test("2WPR", kit, nil, true, false)
    st = $xs.fetch_work_orders(wo)[0].state
    assert_equal "New", st, "WO should not be completed, but is #{st}"
    $log.info "Please complete 2nd Checklist and WO #{wo} in Xsite"
    $log.info "Press any key"
    gets
    assert $xstest.verify_checklist_completed(wo, 0, "OK")
  end
  
  ## All Early Trigger Tests
  
  def test911_stb_waitproduct_early
    run_autostb_test("2WPR", _kit_struct1(@@checklist_kit_01_early, {:earlydue=>Time.now + @@waittime }))
  end
  
  def test912_stb_waitproduct_sup_early
    run_autostb_test("2SUP", _kit_struct1(@@checklist_kit_01_early, {:earlydue=>Time.now + @@waittime }))
  end  
  
  def test913_stb_productive_early
    run_autostb_test("1PRD", _kit_struct1(@@checklist_kit_01_early, {:earlydue=>Time.now + @@waittime }))
  end
  
  def test914_stb_productive_sup_early
    run_autostb_test("1SUP", _kit_struct1(@@checklist_kit_01_early, {:earlydue=>Time.now + @@waittime }), "1PRD")
  end

  def test915_no_dispatch_early
    run_autostb_test("2NDP", _kit_struct1(@@checklist_kit_01_early, {:earlydue=>Time.now + @@waittime }), nil, false)
  end

  def test916_sdt_delay_early
    run_autostb_test("4DLY", _kit_struct1(@@checklist_kit_01_early, {:earlydue=>Time.now + @@waittime }), nil, false)
  end

  def test917_udt_delay_early
    run_autostb_test("5DLY", _kit_struct1(@@checklist_kit_01_early, {:earlydue=>Time.now + @@waittime }), nil, false)
  end

  ## All Due Trigger Tests
  
  def test921_stb_waitproduct_due
    run_autostb_test("2WPR", _kit_struct1(@@checklist_kit_01_due, {:due=>Time.now + @@waittime }))
  end
  
  def test922_stb_waitproduct_sup_due
    run_autostb_test("2SUP", _kit_struct1(@@checklist_kit_01_due, {:due=>Time.now + @@waittime }))
  end  
  
  def test923_stb_productive_due
    run_autostb_test("1PRD", _kit_struct1(@@checklist_kit_01_due, {:due=>Time.now + @@waittime }))
  end
  
  def test924_stb_productive_sup_due
    run_autostb_test("1SUP", _kit_struct1(@@checklist_kit_01_due, {:due=>Time.now + @@waittime }), "1PRD")
  end
  
  def test925_no_dispatch_due
    run_autostb_test("2NDP", _kit_struct1(@@checklist_kit_01_due, {:due=>Time.now + @@waittime }), nil, false)
  end

  def test926_sdt_delay_due
    run_autostb_test("4DLY", _kit_struct1(@@checklist_kit_01_due, {:due=>Time.now + @@waittime }), nil, false)
  end

  def test927_udt_delay_due
    run_autostb_test("5DLY", _kit_struct1(@@checklist_kit_01_due, {:due=>Time.now + @@waittime }), nil, false)
  end
  
  def test930_2_stbonly
    run_autostb_test("2WPR", _kit_struct2(@@checklist_kit_01_early, {:earlydue=>Time.now + @@waittime },
      @@checklist_kit_09_early, {:earlydue=>Time.now + @@waittime }))
  end

  def test931_2_stbonly_different_trigger
    run_autostb_test("2WPR", _kit_struct2(@@checklist_kit_01_early, {:earlydue=>Time.now + @@waittime/2 },
      @@checklist_kit_09_due, {:due=>Time.now + @@waittime }))
  end
  
  def XXXtest999_kit_stress
    cleanup_kits(kits)
    e10state = "2WPR"
    timing = {:due=>Time.now + 300 }
    4.times do |i|
      $sv.eqp_status_change_req @@eqp, e10state    
      assert $xstest.verify_e10state(@@eqp, e10state), " expected state to be #{e10state}"     
      wo = $xs.create_work_order(@@eqp, timing)
      $xs.copy_checklist_to_work_order(wo, @@checklist_kit_02_due) 
      $log.info "Set #{wo} for #{@@eqp}, #{timing}"
    end
  end

  def cleanup_kits(kits)
    $setup_ok = false

    # Cleanup
    _info = $sv.eqp_info(@@eqp)
    _info.rsv_cjs.each {|cj| $sv.slr_cancel(@@eqp, cj)}
    _info.cjs.each {|cj| $sv.eqp_opestart_cancel(@@eqp, cj)}
    _info.ports.each do |p|
      unless p.loaded_carrier == ""
        $sv.eqp_unload(@@eqp, p.port, p.loaded_carrier)
      end
      $sv.eqp_port_status_change @@eqp, p.port, "LoadReq"
    end

    $xstest.cleanup_work_orders(@@eqp)
    $xstest.cleanup_work_requests(@@eqp)

    assert $xstest.set_verify_wait_product(@@eqp), "Cannot set E10 state to wait-product. Please sync manually."

    assert set_carrier_status(kits.map {|k| k[:carriers]}.flatten)
    kits.each do |k|
      car, car_empty = k[:carriers]
      assert($sv.transfer_all_wafers(car_empty, car), "Cannot transfer wafers") if car_empty
      $wbtqtest.rework_monitor_lots(car, product: k[:product])
    end
    assert $wbtqtest.cancel_kit_verify(:eqp=>@@eqp), "error cleaning up WBTQ contexts"

    $setup_ok = true
  end
  
  def set_carrier_status(carriers)
    # set the specified carriers AVAILABLE, all others NOTAVAILABLE
    carriers = carriers.split if carriers.kind_of?(String)
    ret = true
    # get all carriers with build products
    lots = $sv.lot_list(:product=>@@products)
    all_carriers = lots.collect {|l| $sv.lot_info(l).carrier}.uniq.sort

    all_carriers.delete("")
    # set the specified carriers AVAILABLE, all other carriers NOTAVAILABLE
    all_carriers.each {|c|
      status = carriers.member?(c) ? "AVAILABLE" : "NOTAVAILABLE"
      ret &= ($sv.carrier_status_change(c, status, :check=>true) == 0)
    }
    # cancel NPW reserations
    rr = $sv.npw_reservation_state(@@eqp)
    rr.each {|c,x,y| $sv.npw_reserve_cancel(@@eqp, :carrier=>c) if c != ""}
    # cancel SLR reservations
    eqi = $sv.eqp_info(@@eqp)
    eqi.rsv_cjs.each {|cj| $sv.slr_cancel(@@eqp, cj)}
    # stockin
    carriers.each {|c| $sv.carrier_stockin(c, @@stocker)}

    return ret
  end
  
  def setup_wo(kits, params={})
    txtime = Time.now
    _wo_params = params
    kits.each {|k| _wo_params.merge!(k[:timing]) }
    wo = $xs.create_work_order(@@eqp, _wo_params)
    kits.each {|k| $xs.copy_checklist_to_work_order(wo, k[:checklist])}
    $xs.copy_checklist_to_work_order(wo, params[:add_checklist]) if params[:add_checklist]
    assert wo, "Failed to create work order."
    sleep_time = @@waittime + 40
    $log.info "#{wo} created. Sleeping #{sleep_time}"
    sleep sleep_time
    return wo, txtime
  end
    
  def run_autostb_test(e10state, kits, istate=nil, created=true, cleanup=true)
    cleanup_kits(kits)
    $sv.eqp_status_change_req @@eqp, istate if istate
    if e10state == "4DLY"
      wo2 = $xs.create_work_order(@@eqp)
      assert wo2, "failed to create work order."
      $xs.activate_work_order(wo2)
      $xs.activate_work_order(wo2)
    elsif e10state == "5DLY"
      wr = $xs.create_work_request(@@eqp)
      assert wr, "failed to create work request"
    else
      $sv.eqp_status_change_req @@eqp, e10state
    end
    assert $xstest.verify_e10state(@@eqp, e10state), " expected state to be #{e10state}"
    wo, txtime = setup_wo(kits)
    # Fab8 rule to determine order number
    ctxs = $wbtqtest.mds.contexts(order_no: wo.gsub("P", "80"))
    kits.each_with_index do |k, i|
      if created
        #Kit context should be cleared
        assert $xstest.verify_checklist_completed(wo, i, "OK")
        if cleanup
          assert ctxs.size == 0, "expected no pending kit context #{ctxs.inspect}"
        else
          assert ctxs.size == 1, "expected kit context not cleaned up #{ctxs.inspect}"
        end
      else
        assert_equal 0, ctxs.count, "expected no order in WBTQ"
        assert $xstest.verify_checklist_step_not_complete(wo, i, "1", ""), "invalid wo comment, should be empty"        
      end
    end
    return wo
  end
  
end