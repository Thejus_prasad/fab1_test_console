=begin 
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.
    
Author:  Steffen Steidten, 2013-11-22

Version: 16.01

History:
  2015-09-08 ssteidte,  cleanup
  2016-01-15 sfrieske,  adapted for v16.01
=end

require 'RubyTestCase'
require 'jcap/lmtest'


# Test data for XOAK
class Test_XOAK_LM < RubyTestCase
  @@rqid0 = 9966800

  # [remlots, expiration, model, pilot]
  nextday = Time.now + 86400
  prevday = Time.now - 86400
  @@lmreqdata = [
    [10, nextday, 'POL_MODEL_ID', 'xoakpilot'],
    [5,  nextday, 'POL_MODEL_ID', 'xoakpilot'],
    [5,  prevday, 'POL_MODEL_ID', ''],
    [10, nextday, 'POL_MODEL_ID', ''],
    [0,  nextday, 'POL_MODEL_ID', ''],
    [0,  nextday, 'POL_MODEL_ID', ''],
    [0,  nextday, 'POL_MODEL_ID', ''],
    [0,  nextday, 'POL_MODEL_ID', ''],
    
    [6, prevday, 'X', 'xoakpilot'],
    [6, prevday, 'X', 'xoakpilot'],
    [6, prevday, 'X', ''],
    [6, prevday, 'X', ''],
    [0, nextday, 'X', ''],
    [0, nextday, 'X', ''],
    [0, nextday, 'X', ''],
    [0, nextday, 'X', ''],
    
    [5, nextday, 'ALC_MODEL_ID', 'xoakpilot'],
    [5, nextday, 'ALC_MODEL_ID', ''],
    [5, nextday, 'ALC_MODEL_ID', ''],
    [0, nextday, 'ALC_MODEL_ID', ''],
    [0, nextday, 'ALC_MODEL_ID', ''],
    [0, nextday, 'ALC_MODEL_ID', ''],
    [10, nextday, 'X', ''],
    [0, prevday, 'X', 'xoakpilot'],
    [0, nextday, 'X', 'xoakpilot'],
    [0, nextday, 'X', ''],
    [0, nextday, 'X', '']
  ]

  # [lot, pd, eqp, app, level, value, init]
  @@OLDlsumdata = [
    ['U89Y2.00', 'CONTPOL.01', 'POL300', 'CMP_OxCMP', 'Chamber', 'POLC', 0],
    ['U89Y2.00', 'CONTPOL.01', 'POL300', 'CMP_OxCMP_UNI', 'Chamber', 'POLC', 0],
    ['U89Y2.00', 'CONTPOL.01', 'POL300', 'CMP_OxCMP', 'Chamber', 'POLA', 0],
    ['U89Y2.00', 'CONTPOL.01', 'POL300', 'CMP_OxCMP_UNI', 'Chamber', 'POLA', 0],
    ['U89Y2.00', 'CONTPOL.01', 'POL300', 'CMP_OxCMP', 'Chamber', 'POLC', 1],
    ['U89Y2.00', 'CONTPOL.01', 'POL300', 'CMP_OxCMP_UNI', 'Chamber', 'POLC', 1],
    ['U89Y2.00', 'CONTPOL.01', 'POL300', 'CMP_OxCMP', 'Chamber', 'POLA', 1],
    ['U89Y2.00', 'CONTPOL.01', 'POL300', 'CMP_OxCMP_UNI', 'Chamber', 'POLA', 1],
    
    ['U89RD.00', 'CONTPOL.01', 'POL300', 'CMP_OxCMP', 'Chamber', 'POLC', 0],
    ['U89RD.00', 'CONTPOL.01', 'POL300', 'CMP_OxCMP_UNI', 'Chamber', 'POLC', 0],
    ['U89RD.00', 'CONTPOL.01', 'POL300', 'CMP_OxCMP', 'Chamber', 'POLA', 0],
    ['U89RD.00', 'CONTPOL.01', 'POL300', 'CMP_OxCMP_UNI', 'Chamber', 'POLA', 0],
    ['U89RD.00', 'CONTPOL.01', 'POL300', 'CMP_OxCMP', 'Chamber', 'POLC', 1],
    ['U89RD.00', 'CONTPOL.01', 'POL300', 'CMP_OxCMP_UNI', 'Chamber', 'POLC', 1],
    ['U89RD.00', 'CONTPOL.01', 'POL300', 'CMP_OxCMP', 'Chamber', 'POLA', 1],
    ['U89RD.00', 'CONTPOL.01', 'POL300', 'CMP_OxCMP_UNI', 'Chamber', 'POLA', 1],
    
    ['U8AA0.00', 'NFSMSKD-NFS.01', 'ALC400', 'Litho_DICD', 'Reticle', '2970GUNFSC1', 0],
    ['U8AA0.00', 'NFSMSKD-NFS.01', 'ALC400', 'Litho_OVL', 'Reticle', '2970GUNFSC1', 0],
    ['U8AA0.00', 'NFSMSKD-NFS.01', 'ALC400', 'Litho_OVLGRID', 'Reticle', '2970GUNFSC1', 0],
    ['U8AA0.00', 'NFSMSKD-NFS.01', 'ALC400', 'Litho_DICD', 'Mainframe', 'ALC400', 1],
    ['U8AA0.00', 'NFSMSKD-NFS.01', 'ALC400', 'Litho_OVL', 'Mainframe', 'ALC400', 1],
    ['U8AA0.00', 'NFSMSKD-NFS.01', 'ALC400', 'Litho_OVLGRID', 'Mainframe', 'ALC400', 1],
    ['U8AA0.00', 'NFSMSKD-NFS.01', 'ALC400', 'Litho_DICD', 'Reticle', '2970GUNFSC3', 0],
    ['U8AA0.00', 'NFSMSKD-NFS.01', 'ALC400', 'Litho_OVL', 'Reticle', '2970GUNFSC3', 0],
    ['U8AA0.00', 'NFSMSKD-NFS.01', 'ALC400', 'Litho_OVLGRID', 'Mainframe', 'ALC400', 1],
    ['U8AA0.00', 'NFSMSKD-NFS.01', 'ALC400', 'Litho_DICD', 'Mainframe', 'ALC400', 1],
    ['U8AA0.00', 'NFSMSKD-NFS.01', 'ALC400', 'Litho_OVL', 'Mainframe', 'ALC400', 1]
  ]

  @@lsumdata = [
    ['U89Y2.00', 'CONTPOL.01', 'POL300', [
        ['CMP_OxCMP', 'Chamber', 'POLC', 0],
        ['CMP_OxCMP_UNI', 'Chamber', 'POLC', 0],
        ['CMP_OxCMP', 'Chamber', 'POLA', 0],
        ['CMP_OxCMP_UNI', 'Chamber', 'POLA', 0],
        ['CMP_OxCMP', 'Chamber', 'POLC', 1],
        ['CMP_OxCMP_UNI', 'Chamber', 'POLC', 1],
        ['CMP_OxCMP', 'Chamber', 'POLA', 1],
        ['CMP_OxCMP_UNI', 'Chamber', 'POLA', 1]
      ], ['CMP_OxCMP', 'CMP_OxCMP_UNI']
    ],
    ['U89RD.00', 'CONTPOL.01', 'POL300', [
        ['CMP_OxCMP', 'Chamber', 'POLC', 0],
        ['CMP_OxCMP_UNI', 'Chamber', 'POLC', 0],
        ['CMP_OxCMP', 'Chamber', 'POLA', 0],
        ['CMP_OxCMP_UNI', 'Chamber', 'POLA', 0],
        ['CMP_OxCMP', 'Chamber', 'POLC', 1],
        ['CMP_OxCMP_UNI', 'Chamber', 'POLC', 1],
        ['CMP_OxCMP', 'Chamber', 'POLA', 1],
        ['CMP_OxCMP_UNI', 'Chamber', 'POLA', 1],
      ], ['CMP_OxCMP', 'CMP_OxCMP_UNI']
    ],
    ['U8AA0.00', 'NFSMSKD-NFS.01', 'ALC400',  [
        ['Litho_DICD', 'Reticle', '2970GUNFSC1', 0],
        ['Litho_OVL', 'Reticle', '2970GUNFSC1', 0],
        ['Litho_OVLGRID', 'Reticle', '2970GUNFSC1', 0],
        ['Litho_DICD', 'Mainframe', 'ALC400', 1],
        ['Litho_OVL', 'Mainframe', 'ALC400', 1],
        ['Litho_OVLGRID', 'Mainframe', 'ALC400', 1],
        ['Litho_DICD', 'Reticle', '2970GUNFSC3', 0],
        ['Litho_OVL', 'Reticle', '2970GUNFSC3', 0],
        ['Litho_OVLGRID', 'Mainframe', 'ALC400', 1],
        ['Litho_DICD', 'Mainframe', 'ALC400', 1],
        ['Litho_OVL', 'Mainframe', 'ALC400', 1]
      ], ['Litho_DICD', 'Litho_OVL', 'Litho_OVLGRID']
    ]
  ]
  

  def self.startup
    # no MM
  end
  
  
  def test00_setup
    $setup_ok = false
    #
    $lmtest = LotManager::Test.new($env)
    assert $lmtest.transformer.get_version =~ /\d\.\d/, "no connection to LmTransformer"
    #
    $setup_ok = true
  end
  
  def test01
    #assert_equal @@lmreqdata.length, @@lsumdata.length, "data size mismatch"

    # send the LmRequests
    rqids = @@lmreqdata.each_index.collect {|i|
      $log.info "-- sending LmRequest #{i + 1}/#{@@lmreqdata.length}"
      rqid = @@rqid0 + i
      remlots, expiration, model, pilot = @@lmreqdata[i]

      lmrq = LotManager::LmRequest.new(rqid, 10, 20, -1, remlots, 10, 20, 0, 0, '', 2, expiration, Time.now, 
        'QA XOAK', Time.now + 86400, 1, Time.now + 86400, 1, rqid, '', pilot, '',
        [LotManager::LmRequestContext.new(rqid, 'APC_MODEL_ID', model)],
        []
      )
      # send LMRequest and return rqid
      assert $lmtest.transformer.update_lmrequests(data: lmrq)
      rqid
    }

    currqid = 0
    @@lsumdata.each_index {|i|
      $log.info "-- sending LotSummary #{i + 1}/#{@@lsumdata.length}"
      lot, pd, eqp, limitations, rqrequireds = @@lsumdata[i]
      lsum = LotManager::LotSummary.new(LotManager::LmContext.new(nil, lot, pd, eqp))
      lsum.limitations = limitations.collect {|limitation|
        app, level, value, init = limitation
        rqid = rqids[currqid]
        currqid += 1
        LotManager::LmLimitation.new(nil, nil, rqid, app, level, value, 0, init, Time.now, 'QA XOAK')
      }
      lsum.rqrequired = rqrequireds.collect {|app|
        LotManager::LmRqRequired.new(nil, nil, app, 'Limitation')
      }
      # send LotSummary
      assert $lmtest.transformer.update_lotsummary(data: lsum)
    }
  end
  
end
