=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.
    
Author: Paul Peschel, 2013-02-22 migrate from StressTxLotInfoInq.java
=end

require 'RubyTestCase'

class Stress_TxLotInfoInq < RubyTestCase
  @@required_size = 1600
  @@max_size = 3000
  
  def test1_stress
    # create a large list of lots
    lots = @@sv.lot_list
    lotids = []
    lotids += lots while lotids.size < @@max_size
    # increase list of lots passed to TxLotInfo from 100 to 4000 an see where it fails
    (100..@@max_size).step(500) {|i|
      $log.info "-- testing with #{i} lots"
      begin
        assert @@sv.lots_info_raw(lotids[0...i])
      rescue
        if i >= @@required_size
          $log.info "TxLotInfo failed for #{i} lots, but that is correct."
          break
        else
          $log.warn "TxLotInfo failed for #{i} lots"
          fail
        end
      end
    }
  end
end