=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: Paul Peschel, 2013-02-22 migrate from StressAMD_TxProductListInq.java

History:
  2014-09-03 ssteidte, replaced AMD by CS Tx
  2021-09-15 sfrieske, finally replaced $svtest by @@svtest, $sv by @@sv and $sm by @@sm
=end

require 'RubyTestCase'

class Stress_TxProductListInq < RubyTestCase

  @@loops = 10

  def test1_stress
    lottypes = ['Production', 'Dummy', 'Equipment Monitor', 'Process Monitor', 'Raw', 'Recycle']
    @@loops.times {|i|
      lottype = lottypes[rand(lottypes.length)]
      starttime = Time.now
      products = @@sv.CS_product_list(lottype: lottype)
      $log.info "#{lottype} #{products.size} #{Time.now - starttime}s"
    }
  end
end