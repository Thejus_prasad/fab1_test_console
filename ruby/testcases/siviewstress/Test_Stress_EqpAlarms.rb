=begin
Test Equipment Alarm Recipients (send TxEqpAlarmRpt)

(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: Steffen Steidten, 2011-10-05

=end

require 'RubyTestCase'


# Test Equipment Alarm Recipients (send TxEqpAlarmRpt)
class Test_Stress_EqpAlarms < RubyTestCase
  Description = "Send TxEqpAlarmRpt"

  @@test_duration = "60 h"
  @@tooltemplate = "LETCL1%3.3d"
  @@ntools = "10"
  @@alarms_per_day = "2000000"

  def setup
    puts "\n"
    $setup_ok = ($setup_ok != false)
    assert $setup_ok, "test setup is not correct"
  end

  def test01_setup
    $setup_ok = false
    @@results = {}
    # create tool list
    @@tools = @@ntools.to_i.times.collect {|i| @@tooltemplate % i}
    @@clients = @@tools.collect {|t| SiView::MM.new($env)}
    # clean up the queue
    $setup_ok = true
  end

  def test11_alarmmsg_stress
    d = get_test_duration(nil)
    sleeptime = 10
    @@test_start = Time.now + sleeptime
    @@test_end_planned = @@test_start + d
    @@sleeptime = (86400 * @@ntools.to_i) / @@alarms_per_day.to_f - 0.1
    $log.info "sleeptime between alarms: #{@@sleeptime}s"
    $log.info "test duration: #{d}s (#{@@test_duration}), ending at #{@@test_end_planned.strftime('%F_%T')}"
    sleep sleeptime
    # start test
    @@results = alarm_stress(@@clients, @@tools, :tend=>@@test_end_planned, :show=>true, :sleep=>@@sleeptime)
    assert @@results[:ok], "comm failure?"
  end


  def get_test_duration(s)
    # conver test duration from a string: seconds, minutes ("30 m") or hours ("1 h")
    # return duration in seconds as integer
    s ||= @@test_duration
    d = s.to_f
    d *= 60 if s.end_with?("m") or s.end_with?("min")
    d *= 3600 if s.end_with?("h")
    return d
  end

  def self.results(params={})
    fname = params[:file]
    open(fname, "wb") {|fh| fh.write(@@results.to_yaml)} if fname
    @@results
  end

end

def alarm_stress(clients, tools, params={})
  # run the stress test and return performance stats
  ##loops = (params[:loops] or 1).to_i
  tend = params[:tend]
  sleeptime = params[:sleep]
  res = []
  threads = []
  tstart = Time.now
  clients.each_with_index {|cl, i|
    #sleep 0.2 if threads.size > 0
    threads << Thread.new {
      while Time.now < tend
        sleep sleeptime if sleeptime
        _tstart = Time.now
        ok = (cl.eqp_alarm(tools[i]) == 0)
        res << (ok ? Time.now - _tstart : nil)
      end
    }
  }
  threads.each_with_index {|t, i|
    $log.info "joining thread #{i}"
    t.join
  }
  stats = {:tstart=>tstart.clone, :duration=>Time.now - tstart,
    :ntools=>clients.size, :responsetime=>res.clone, :ok=>!res.member?(nil)}
  #
  show_alarm_stats(stats) if params[:show]
  #
  return stats
end

def show_alarm_stats(stats)
  # display the results of an ECSV stress test
  res = stats[:responsetime]
  r = res.compact
  puts "\nTool Alarm Message Stress Test from #{stats[:tstart].strftime('%F_%T')}"
  puts "=" * 61
  puts "test duration:     #{stats[:duration].to_i}s, number of clients: #{stats[:ntools]}"
  puts "messages sent: #{res.size}, ok: #{r.size}, failed: #{res.size - r.size}"
  aph = r.size/stats[:duration]*3600
  puts "number of alarms: %d  (%d per hour, %d per day)" % [r.size, aph, aph*24]
  puts "average call duration: %dms +- %dms (%d%%)" % [
    r.average*1000, r.standard_deviation*1000, r.standard_deviation/r.average*100]
  puts
  puts
end
