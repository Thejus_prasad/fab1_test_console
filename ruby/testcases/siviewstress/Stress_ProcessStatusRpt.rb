=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: Daniel Steger, 2015-09-02

History:
  2015-11-04 sfrieske, leveraging claim_process_lot with a block to simplify code
  2021-09-15 sfrieske, finally replaced $svtest by @@svtest, $sv by @@sv and $sm by @@sm
=end

require 'SiViewTestCase'


# Stress test for MSR429683
class Stress_ProcessStatusRpt < SiViewTestCase
  @@eqp = 'UTC001'
  @@opNo = '1000.100'
  @@loops = 15


  def self.shutdown
    @@sv.eqp_cleanup(@@eqp)
    @@sv.delete_lot_family(@@lot)
  end


  def test01
    assert @@lot = @@svtest.new_lot, "failed to get a lot"
    parameters = [
      {eqp: @@eqp},
      {eqp: @@eqp, running_hold: true},
      {eqp: @@eqp, opestart_cancel: true}
    ]
    @@loops.times {|i|
      assert @@sv.lot_cleanup(@@lot, opNo: @@opNo)
      assert @@sv.eqp_cleanup(@@eqp)
      th = nil
      assert @@sv.claim_process_lot(@@lot, parameters[i % 3]) {|cj|
        th = Thread.new {
          rc = 0
          while (![324, 1231].include?(rc)) do
            rc = @@sv.process_status(@@eqp, cj, [@@lot], 'ProcessStart')
            rc = @@sv.process_status(@@eqp, cj, [@@lot], 'ProcessEnd') if rc != 324
          end
          rc != 324
        }
      }
      assert th.value, "no object lock error expected"
    }
  end

end