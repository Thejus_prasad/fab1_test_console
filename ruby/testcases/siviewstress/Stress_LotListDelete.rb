=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: ssteidte, 2014-01-20

History:
  2015-06-01 adarwais, cleanup
  2019-12-05 sfrieske, minor cleanup, renamed from Test092_LotListDelete
=end

require 'RubyTestCase'

# Test LotList with deleteFlag true, regression in customization since after C6.14
class Stress_LotListDelete < RubyTestCase
  @@nloops = 100

  def test01
    # ensure there are lots to delete by calling the old R8 method
    ll = @@sv.lot_list(delete: true)
    assert ll.size > 0, 'there are no lots eligible for deletion'
    $log.info "found #{ll.size} lots eligible for deletion"
    # call new method many times
    @@nloops.times {|i|
      $log.info "-- loop #{i+1}/#{@@nloops}"
      assert ll = @@sv.lot_list(delete: true), "error getting list of lots to delete (loop ##{i+1})"
      assert ll.size > 0, 'empty list returned, bug is not fixed'
    }
  end

end
