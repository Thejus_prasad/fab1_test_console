=begin 
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.
    
Author: Paul Peschel, 2013-02-20 migrate from StressTxRouteOperationListInq.java
=end

require 'SiViewTestCase'

class Stress_TxRouteOperationListInq < SiViewTestCase
  @@loops = 100
  
  def test1_stress
    @@loops.times {
      tstart = Time.now
      assert @@sv.route_operations(@@svtest.route, count: 10000), 'error executing TxRouteOperationListInq'
      $log.info "duraion: #{(Time.now - tstart)}"
    }
  end
end
