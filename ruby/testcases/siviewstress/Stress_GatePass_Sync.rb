=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: Steffen Frieske, 2015-10-22

History:
  2020-04-03 sfrieske, minor cleanup
=end

require 'util/waitfor'
require 'SiViewTestCase'


# Testcase to replicate carrier postprocessing error in C7.1.0 (MES-2602)
class Stress_GatePass_Sync < SiViewTestCase
  @@nthreads = 5    # 5 fails fastest
  @@loops = 100


  def self.shutdown
    @@sv.delete_lot_family(@@lot)
  end


  def test1
    assert @@lot = @@svtest.new_lot, 'error creating test lot'
    assert lots = (@@nthreads-1).times.collect {@@sv.lot_split(@@lot, 1)}
    lots.unshift(@@lot)
    #
    li = @@sv.lot_info(@@lot)
    opNo, route = li.opNo, li.route
    @@loops.times {|n|
      $log.info "\n\n--- loop #{n+1}/#{@@loops}"
      lots.each {|lot| assert_equal 0, @@sv.lot_opelocate(lot, opNo, route: route, forward: false)}
      lots.collect {|lot|
        Thread.new {@@sv.lot_gatepass(lot)}
      }.each {|t| t.join}
      assert wait_for(timeout: 90, sleeptime: 10) {
        !@@sv.lot_info(@@lot).pp_carrier
      }, 'wrong carrier post processing state'
    }
  end
end
