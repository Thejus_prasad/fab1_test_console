=begin 
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.
    
Author: Paul Peschel, 2013-02-20 migrate from StressAMD_TxModulePDInq.java

History:
  2020-02-12 sfrieske, minor cleanup
=end

require 'RubyTestCase'

# Stress test for AMD_TxModulePDInq
class Stress_AMD_TxModulePDInq < RubyTestCase
  @@loops = 10
  
  def test1_stress
    types = ['Correlation', 'Dummy', 'Engineering', 'Equipment Monitor', 'Production', 'Process Monitor', 'Recycle', 'Vendor']
  
    @@loops.times {|i|
      $log.info "-- loop #{i+1}/#{@@loops}"
      types.each {|t|
        @@sv.lot_list(lottype: t).take(250).each {|lot|
          li = @@sv.lot_info(lot)
          expected = li.route.empty? ? 1450 : 0 # Error returned only if route information is missing
          @@sv.AMD_modulepd(lot)
          assert_equal expected, @@sv.rc, "wrong return code from AMD_TxModulePDInq for lot #{lot}"
        }
      }
    }
  end

end
