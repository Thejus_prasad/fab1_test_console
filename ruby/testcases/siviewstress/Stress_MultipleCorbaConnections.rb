=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: Paul Peschel, 2013-02-21 migrate from StressMultipleCorbaConnections.java
=end

require 'RubyTestCase'

class Stress_MultipleCorbaConnections < RubyTestCase
  description = "Open a number of corba connections as many as specified with threads. Then each thread holds and releases lots that are retrieved with the lotlist tx."

  @@HoldReasonCode = "AEHL"
  @@ReleaseReasonCode = "ALL"
  @@LotRouteMask = "UT%"
  @@nthreads = 10
  @@duration = 10

  def test1_stress
    lots = @@sv.lot_list(:route=>@@LotRouteMask, :status=>"Waiting")
    assert lots.size >= 10
    @@duration = 30
    do_run = true
    svs = []
    ths = @@nthreads.times.collect {|i|
      Thread.new {
        svs[i] = SiView::MM.new($env)
        svs[i].lot_hold_release(lots[i], nil)
        while do_run
          assert_equal 0, svs[i].lot_hold(lots[i], @@HoldReasonCode)
          assert li = svs[i].lot_info(lots[i])
          assert_equal "ONHOLD", li.status
          assert_equal 0, svs[i].lot_hold_release(lots[i], @@HoldReasonCode, :release_reason=>@@ReleaseReasonCode)
          assert li = svs[i].lot_info(lots[i])
          assert_equal "Waiting", li.status
        end
      }
    }
    sleep @@duration
    do_run = false
    ths.each {|th| th.join}
  end
end