=begin 
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.
    
Author: Paul Peschel, 2013-02-21 migrate from StressEntityInhibits.java

History:
  2015-07-01 dsteger  implemented MSR390493 
=end

require 'SiViewTestCase'


class Stress_EntityInhibits < SiViewTestCase
  @@loops = 100
  @@eqp = 'UTC001'
  @@reason = 'PENG'
  
  def test1_stress
    running = true
    t = Thread.new {
      while running
        assert @@sv.inhibit_entity(:eqp, @@eqp, raw: true), "failed to set inhibit"
        assert_equal 0, @@sv.inhibit_cancel(:eqp, @@eqp), "failed to cancel inhibit"
      end
    }
    @@loops.times {
      assert @@sv.inhibit_list(:eqp), "failed to list"
    }
    running = false
    t.join    
  end
  
  def teardown
    @@sv.inhibit_cancel(:eqp, @@eqp)
  end

end
