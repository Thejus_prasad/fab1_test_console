=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: Daniel Steger 2005, Douglas Kaip 2010; Steffen Steidten 2013 (Ruby port)

History:
  2015-06-30 ssteidte,  changed from duration based to call based test
  2021-09-15 sfrieske, finally replaced $svtest by @@svtest, $sv by @@sv and $sm by @@sm
=end

require 'RubyTestCase'


class Stress_AMD_TxGetUDATAInq < RubyTestCase
  @@eqps = %w(UTF001 UTF002 UTC001 UTC002 UTI001 UTI002 UTR001 UTR002 UTS001 UTS002)
  @@threads = 10
  @@duration_seconds = 120
  @@ncalls = 2500  # per thread

  def XXtest1_stress_duration
    $log.info "using eqps #{@@eqps.inspect}"
    assert @@eqps.size >= @@threads, "not enough equipments to run a stress test"
    #svs = @@threads.times.collect {|i| SiView::MM.new($env)}
    counters = Array.new(@@threads, 0)
    do_run = true
    #
    ths = @@threads.times.collect {|i|
      Thread.new {
        while do_run
          assert @@sv.AMDGetUDATA('EQP', @@eqps[i % @@eqps.size], raw: true), "error calling Tx"
          counters[i] += 1
        end
      }
    }
    $log.info "running test with #{@@threads} threads for #{@@duration_seconds}s"
    sleep @@duration_seconds
    #
    $log.info "test time over, stopping all threads"
    do_run = false
    sleep 10
    ths.each {|t| t.kill}
    $log.info "test completed, number of Tx calls: #{counters.inject {|s,i| s+i}}"
  end

  def test1_stress_calls
    $log.info "using eqps #{@@eqps.inspect}"
    assert @@eqps.size >= @@threads, "not enough equipments to run a stress test"
    #
    ths = @@threads.times.collect {|i|
      Thread.new {
        @@ncalls.times {|n|
          $log.info "-- loop #{n}/#{@@ncalls}" if i == 0 && n % 100 == 0
          assert @@sv.AMDGetUDATA('EQP', @@eqps.sample, raw: true), "error calling Tx"
        }
      }
    }
    ths.each {|t| t.join}
  end
end
