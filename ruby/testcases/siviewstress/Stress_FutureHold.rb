=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: Paul Peschel, 2013-02-22 migrate from StressFutureHold.java
=end

require 'RubyTestCase'

# Repeated test for TxLotHoldListInq, TxFutureHoldListByKeyInq, TxLotOperationNoteInfoRegistReq and TxEnhancedFutureHoldReq
class Stress_FutureHold < RubyTestCase
  @@route = 'C%'
  @@nholds = 10
  @@reasons = 'X-S'

  def test1_stress
    assert lot = @@sv.lot_list(route: @@route, status: 'Waiting').first, "no lots found on route #{@@route}"
    @@nholds.times {|t|
      # get lot hold list
      res = @@sv.lot_hold_list(lot, raw: true)
      assert_equal 0, @@sv.rc
      # get the future hold list
      res = @@sv.lot_futurehold_list(lot, raw: true)
      assert_equal 0, @@sv.rc
      # set operation note
      li = @@sv.lot_info(lot)
      assert_equal 0, @@sv.lot_openote_register(lot, "xxx", "xx", route: li.route, op: li.op, opNo: li.opNo)
      # register future hold
      assert_equal 0, @@sv.lot_futurehold(lot, li.opNo, "#{@@reasons}#{t}", post: true)
    }
    # cancel all future holds
    assert_equal 0, @@sv.lot_futurehold_cancel(lot, nil)
  end
end
