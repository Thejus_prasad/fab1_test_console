=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: Paul Peschel, 2013-02-22 migrate from StressAMD_AllReticleListInq.java
=end

require "RubyTestCase"

class Stress_AMD_AllReticleListInq < RubyTestCase

  @@Loops = 10000

  def test1_stress
    lots = @@sv.lot_list
    @@Loops.times {|l|
      lot = lots[rand(lots.length)]
      $log.info("-- #{l}/#{@@Loops}") if (l % 100 == 0)
      res = @@sv.reticle_list(lot: lot, count: 9999, raw: true)
      assert [0, 1551, 1488].member?(@@sv.rc), "return code must be one of 0, 1551, 1488"
    }
  end
end