=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: Daniel Steger, 2015-02-16

History:
  2015-02-16 dsteger,  added test for MSR186121/MES-2473
  2016-07-12 sfrieske, clearer and quicker code, will detect failures faster
  2017-09-15 sfrieske, separated tests for GatePass/SLR (fixed) and GatePass/LotInfo (appeared again in C7.7)
  2019-04-09 sfrieske, minor cleanup
=end

require 'thread'
require "SiViewTestCase"


# Testcase to replicate deadlock when gate pass and start lots reservation are called at the same time
# MES2473, MSR186121 - SLR (TXTRC041) fails with RC 1468 or 2037
class Stress_GatePass_SLR < SiViewTestCase
  @@loops = 500  # until C7.5 << 100, from C7.6 on 300 (does not fail _always_ within 100)

  @@testlots = []


  def self.shutdown
    @@testlots.each {|lot| @@sv.delete_lot_family(lot)}
  end


  # passes in C7.6, not any more in C7.7
  def test11_gatepass_slr
    # prepare lot, eqp and carrier for slr, collect parameters to avoid later eqp_info and lot_info calls
    assert lot = @@svtest.new_lot, 'error creating test lot'
    @@testlots << lot
    assert_equal 0, @@sv.lot_cleanup(lot, opNo: :first)
    li = @@sv.lot_info(lot)
    carrier, opNo, route = li.carrier, li.opNo, li.route
    assert eqp = li.opEqps.find {|e| e.start_with?('UT')}, 'no suitable eqp'
    assert_equal 0, @@sv.eqp_cleanup(eqp, mode: 'Off-Line-1', notify: false)
    eqpi = @@sv.eqp_info(eqp)
    port, pg = eqpi.ports.first.port, eqpi.ports.first.pg
    assert_equal 0, @@sv.carrier_xfer_status_change(carrier, 'MO', @@svtest.stocker)
    # start SLR thread
    slr_ok = [0, 378, 923, 1254, 1413, 1457]  # pp, input value (wrong opNo), slrinfo pp, slr wrong opNo, slrinfo machine recipe    old test: != 324, 2037
    sv2 = SiView::MM.new($env)
    ok = true
    t = Thread.new {
      while ok
        cj = sv2.slr(eqp, carrier: carrier, lot: lot, port: port, pg: pg, slr_retries: 1)
        ok &= slr_ok.member?(sv2.rc)
        sv2.slr_cancel(eqp, cj) if cj
      end
    }
    # start gatepass
    ##locate_ok = [0, 1229, 1254]  # cj, pp    old test: != 1738
    gatepass_ok = [0, 1229, 1254, 1534]  # cj, pp, pp?
    @@loops.times {|n|
      $log.info "\n\n--- loop #{n+1}/#{@@loops}"
      ($log.warn "aborted in loop #{n}"; break) unless ok
      locok = -1
      while locok != 0
        locok = @@sv.lot_opelocate(lot, opNo, route: route, forward: false)
      end
      ##ok &= locate_ok.member?(@@sv.lot_opelocate(lot, opNo, route: route, forward: false))
      ok &= gatepass_ok.member?(@@sv.lot_gatepass(lot, route: route, opNo: opNo))
    }
    t.kill
    t.join
    assert ok, 'test failed'
  end

end
