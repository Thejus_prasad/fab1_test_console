=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: Daniel Steger, 2015-02-16

History:
  2015-02-16 dsteger,  added test for MSR186121/MES-2473
  2016-07-12 sfrieske, clearer and quicker code, will detect failures faster
  2017-09-15 sfrieske, separated tests for GatePass/SLR (fixed) and GatePass/LotInfo (appeared again in C7.7)
  2019-04-09 sfrieske, minor cleanup
  2021-02-09 sfrieske, removed :ondemand from new_lot call
=end

require 'thread'
require "SiViewTestCase"


# Testcase to replicate deadlock when gate pass and start lots reservation are called at the same time
# MES-2473, MSR186121, MES-3649
class Stress_GatePass_LotInfo < SiViewTestCase
  @@sv_defaults = {product: 'UT-PROD-GATEPASS.01', route: 'UTRT-GATEPASS.01'}
  @@loops = 500  # until C7.5 << 100, from C7.6 on 300 (does not fail _always_ within 100), C7.9: 500

  @@testlots = []


  def self.shutdown
    @@testlots.each {|lot| @@sv.delete_lot_family(lot)}
  end


  # MES-2473, seemed to pass in C7.6, fails again in C7.7 (LET, ITDC)
  def test12_gatepass_lotinfo
    assert lot = @@svtest.new_lot, 'error creating test lot'
    @@testlots << lot
    assert_equal 0, @@sv.lot_cleanup(lot, opNo: :first)
    li = @@sv.lot_info(lot)
    opNo, route = li.opNo, li.route
    # start lotinfo thread
    nli = 0
    sv2 = SiView::MM.new($env)
    ok = true
    t = Thread.new {
      while ok
        nli += 1
        sv2.lots_info_raw([lot], retries: 0) #, flags: :all)
        ok = false if sv2.rc == 1468
      end
    }
    # start gatepass
    @@loops.times {|n|
      $log.info "\n\n--- loop #{n+1}/#{@@loops}"
      ($log.warn "aborted in loop #{n}"; break) unless ok
      @@sv.lot_opelocate(lot, opNo, route: route, forward: false)
      @@sv.lot_gatepass(lot, route: route, opNo: opNo)
    }
    t.kill
    t.join
    $log.info "number of lotinfo queries: #{nli}"
    assert ok, 'test failed'
  end

end
