=begin 
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.
    
Author: Daniel Steger 2005, Douglas Kaip 2010; Steffen Steidten 2013 (Ruby port)
=end

require "RubyTestCase"


class Stress_TxEqpListInq < RubyTestCase
  Description = "Test TxEqpListInq (AMD_TxEqpListInq had a memory leak before MSR105031, now merged back to core)"

  @@loops = 10
  
  def test1_stress
    @@loops.times {|i|
      $log.info "-- loop #{i+1}/#{@@loops}"
      was = @@sv.workarea_list
      $log.info "found work areas #{was.inspect}"
      assert was.size > 0, "error getting work areas"
      was.each {|wa|
        eqps = @@sv.workarea_eqp_list(wa)
        assert eqps, "error retrieving equipment list for work area #{wa.inspect}" 
      }
    }
  end
end
