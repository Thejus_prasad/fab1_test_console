=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: Paul Peschel, 2013-02-22 migrate from StressGetUDATAMemoryLeak.java

=end

require 'RubyTestCase'

class Stress_GetUDATAMemoryLeak < RubyTestCase
  @@pds = %w(UTPD000.01 UTPD001.01 UTPD002.01 UTPD003.01 UTPD004.01 UTPD005.01 UTPD006.01 UTPD007.01 UTPD008.01 UTPD009.01 UTPD010.01)
  @@loops = 1000

  def test1_stress
    $log.info "testing for memory leaks with #{@@loops} tx calls"
    @@loops.times {|n|
      $log.info "-- loop #{n}/#{@@loops}" if n % 100 == 0
      assert @@sv.AMDGetUDATA('PD', @@pds.sample, raw: true)
    }
  end
end