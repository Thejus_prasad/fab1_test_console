=begin
Stress test the EqpInfoInq Tx

(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

    
Author: Steffen Steidten, 2011-08-08
=end

require "RubyTestCase"

# Stress test the EqpInfoInq Tx
class Test_Stress_EqpInfo < RubyTestCase

  Description = "Stress test the SiView TxEqpInfoInq"
  
  
  # test duration in seconds, minutes ("30 m") or hours ("1 h")
  @@test_duration = "1 h"
  @@parallel_threads = "10"
  
  def test01_setup
    @@tx_count = 0
    n = @@parallel_threads.to_i
    @@svclients = n.times.collect {SiView::MM.new($env)}
    # get all equipment
    @@work_areas = @@sv.workarea_list - ["QA-F1M1", "QA-F1M2"]
    @@all_eqp = @@work_areas.collect {|w| 
      @@sv.workarea_eqp_list(w).strAreaEqp.collect {|e| e.equipmentID.identifier}
    }.flatten
    # assign to the clients
    @@tool_lists = n.times.collect{[]}
    @@all_eqp.each_with_index {|eqp,i| @@tool_lists[i%n] << eqp}
  end
  
  def test11_eqpinfo_stress
    d = get_test_duration(nil)
    sleeptime = 10
    @@test_start = Time.now + sleeptime
    @@test_end_planned = @@test_start + d
    $log.info "test duration: #{d}s (#{@@test_duration}), ending at #{@@test_end_planned.iso8601}"
    sleep sleeptime
    # start test
    threads = []
    @@test_ok = true
    @@svclients.each_with_index{|sv,i|
      threads << Thread.new {
        while Time.now < @@test_end_planned
          @@tool_lists[i].each {|eqp|
            res = sv.eqp_info(eqp)
            $log.warn "error with eqp #{eqp}" unless res
            assert !!res 
            @@test_ok &= !!res
            @@tx_count += 1
          }
        end
      }
    }
    threads.each_with_index {|t, i| 
      $log.info "joining thread #{i}"
      t.join
    }
    @@test_end = Time.now
    $log.info "Test duration: #{@@test_end - @@test_start}s, Number of TxEqpInfoInq transactions: #{@@tx_count}"
    assert @@test_ok
  end
  

  def get_test_duration(s)
    # conver test duration from a string: seconds, minutes ("30 m") or hours ("1 h")
    # return duration in seconds as integer
    s ||= @@test_duration
    d = s.to_f
    d *= 60 if s.end_with?("m") or s.end_with?("min")
    d *= 3600 if s.end_with?("h")
    return d
  end
  
  def self.stats
    @@tx_count
  end
  
end
