=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: Paul Peschel, 2013-02-21 migrate from StressEqpInfoInternalBuffer.java

History:
  2015-09-03 dsteger,  MSR943986 added
  2015-10-01 sfrieske, added test for LotsInfoForStartLotsReservation (found in R15C7.1.0)
  2019-04-09 sfrieske, minor cleanup
  2020-08-27 sfrieske, removed _extract_eqp_info call
=end

require 'SiViewTestCase'


# Testcase to replicate error when doing equipment info while unloading lot report is ongoing.
# MSR943986 (eqp info during opecomp), MSR193114 (eqp load/unload) http://gfjira/browse/MES-2514
# fails in ITDC (MES-2514), fixed in C7.9
class Stress_EqpInfoInternalBuffer < SiViewTestCase
  @@sv_defaults = {product: 'UT-INHIBIT.01', route: 'UTRT-INHIBIT.01'}
  @@eqp = 'UTI004'
  @@opNo = '1000.2000'
  @@proctime = 0.2
  @@loops = 100

  @@testlots = []


  def self.shutdown
    @@testlots.each {|lot| @@sv.delete_lot_family(lot)}
  end


  def setup
    @@sv.eqp_cleanup(@@eqp)
    @@testlots.each {|lot| @@sv.lot_cleanup(lot, opNo: @@opNo)}
  end


  def test00_setup
    $setup_ok = false
    #
    # ensure eqp is Multiple Recipe
    assert_equal 'Multiple Recipe', @@sm.object_info(:eqp, @@eqp).first.specific[:mrtype], 'eqp type must be Multiple Recipe'
    # clean up eqp
    assert_equal 0, @@sv.eqp_cleanup(@@eqp, mode: 'Off-Line-1', notify: false)
    @@ports = @@sv.eqp_info(@@eqp).ports.collect {|p| p.port}
    assert @@ports.size > 1, "eqp #{@@eqp} must have at least 2 ports"
    $log.info "using eqp #{@@eqp} #{@@ports}"
    # create test lots
    assert @@testlots = @@svtest.new_lots(@@ports.size), 'error creating test lots'
    #
    $setup_ok = true
  end

  def test11_eqp_info
    testok = true
    @@running = true
    thread = Thread.new {
      Thread.current['name'] = 'eqpinfo'
      while @@running && testok
        $log.info 'calling eqp_info'
        testok &= !!@@sv.eqp_info_raw(@@eqp, ib: true)
      end
      @@running = false
    }
    #
    testok &= run_process_threads(@@testlots)
    thread.join
    assert testok
  end

  def test12_eqp_unload
    ports = @@sv.eqp_info(@@eqp).ports
    llot = @@testlots[-1]
    lcarrier = @@sv.lot_info(llot).carrier
    lport = @@ports[-1]
    assert_equal 0, @@sv.lot_opelocate(llot, @@opNo)
    assert_equal 0, @@sv.carrier_xfer_status_change(lcarrier, 'EO', @@eqp)
    testok = true
    @@running = true
    thread = Thread.new {
      Thread.current['name'] = 'eqpunload'
      while @@running && testok
        # $log.info 'calling eqp_load'
        testok &= (@@sv.eqp_load(@@eqp, lport, lcarrier, ib: true) == 0)
        testok &= (@@sv.eqp_unload(@@eqp, lport, lcarrier, ib: true) == 0)
      end
      @@running = false
    }
    #
    testok &= run_process_threads(@@testlots[0..-2])
    thread.join
    assert testok
  end

  # sometimes fails in R15C7.1.0
  def test13_lots_info_slr
    eqpiraw = @@sv._raw(@@eqp)
    port = eqpiraw.equipmentPortInfo.strEqpPortStatus.first.portID.identifier
    pg = eqpiraw.equipmentPortInfo.strEqpPortStatus.first.portGroup
    lot = @@testlots.last
    carrier = @@sv.lot_info(lot).carrier
    assert_equal 0, @@sv.lot_opelocate(lot, @@opNo)
    assert_equal 0, @@sv.carrier_xfer_status_change(carrier, 'EO', @@eqp)
    testok = true
    @@running = true
    thread = Thread.new {
      Thread.current['name'] = 'lotsinfoslr'
      while @@running && testok
        testok &= !!@@sv.lots_info_slr(@@eqp, [lot], port: port, pg: pg, eqpinfo: eqpiraw, carrier: carrier)
      end
      @@running = false
    }
    #
    testok &= run_process_threads(@@testlots[0..-2])
    thread.join
    assert testok
  end


  # process lots in parallel, return false on errors
  def run_process_threads(lots)
    testok = true
    lots.each_with_index.collect {|lot, i|
      Thread.new {
        Thread.current['name'] = "#{@@ports[i]}"
        lot = @@testlots[i]
        carrier = @@sv.lot_info(lot).carrier
        assert_equal 0, @@sv.carrier_xfer_status_change(carrier, 'EO', @@eqp)
        @@loops.times {|n|
          break unless @@running && testok
          $log.info "\n\n--- loop #{n+1}/#{@@loops}"
          @@sv.lot_opelocate(lot, @@opNo)
          testok &= !!@@sv.claim_process_lot(lot,
            eqp: @@eqp, port: @@ports[i], ib: true, proctime: @@proctime)
        }
        @@running = false
      }
    }.each {|th| th.join}
    @@running = false
    return testok
  end

end
