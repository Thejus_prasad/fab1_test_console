=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: Paul Peschel, 2013-02-22 migrate from StressTxPortStatusChangeRpt.java

History:
  2021-09-15 sfrieske, finally replaced $svtest by @@svtest, $sv by @@sv and $sm by @@sm
=end

require 'RubyTestCase'

class Stress_TxPortStatusChangeRpt < RubyTestCase

  @@EquipmentID = "UTF001"
  @@PortID1 = "P1"
  @@PortID2 = "P2"
  @@TestTimeSeconds = 10

  def test1_stress
    # eqp setup
    eq = @@sv.eqp_info(@@EquipmentID)
    eq.ports.each {|port|
      unless port.rsv_carrier == ""
        li = @@sv.lot_info(@@sv.carrier_status(port.rsv_carrier).lots)
        assert_equal(0, @@sv.slr_cancel(eqp, li[0].cj)) unless li[0].cj == ""
      end
    }
    assert_equal 0, @@sv.eqp_mode_change(@@EquipmentID, "Auto-1")
    # find test lot
    wn = @@sv.what_next(@@EquipmentID, :singlelot=>true).first
    assert_not_nil wn
    # start stress test
    do_run = true
    threads = []
    threads << Thread.new {
      sv1 = SiView::MM.new($env)
      while do_run
        cj = sv1.slr(@@EquipmentID, :port=>@@PortID1, :lot=>wn.lot, :carrier=>wn.carrier)
        assert_equal(0, sv1.slr_cancel(@@EquipmentID, cj)) if cj
      end
    }
    threads << Thread.new {
      sv2 = SiView::MM.new($env)
      while do_run
        assert_equal 0, sv2.eqp_port_status_change(@@EquipmentID, @@PortID2, "LoadReq")
        assert_equal 0, sv2.eqp_port_status_change(@@EquipmentID, @@PortID2, "UnloadReq")   #, :carrier=>wn.carrier)
      end
    }
    $log.info "test duration: #{@@TestTimeSeconds}s"
    sleep @@TestTimeSeconds
    do_run = false
    threads.each {|th| th.join}
  end
end