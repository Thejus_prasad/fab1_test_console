=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: Paul Peschel, 2013-02-21 migrate from StressCassetteHandling.java
=end

require "RubyTestCase"

class Stress_CassetteHandling < RubyTestCase
  Description = "This test simulates checks for concurrency issues in inquiring and updating the carrier status in non-HAS envs."

  @@stocker = "UTSTO11"
  @@interbay = "INTER1"
  @@carrier = "UT05"
  @@duration = 30

  def test1_stress
    do_run = true
    threads = []
    threads << Thread.new {
      while do_run
        assert @@sv.carrier_status(@@carrier, :raw=>true)
      end
    }
    threads << Thread.new {
      while do_run
        assert_equal 0, @@sv.carrier_xfer_status_change(@@carrier, "SI", @@stocker)
        assert_equal 0, @@sv.carrier_reserve(@@carrier)
        assert_equal 0, @@sv.carrier_xfer_status_change(@@carrier, "SO", @@interbay)
        assert_equal 0, @@sv.carrier_reserve_cancel(@@carrier)
      end
    }
    $log.info "running test for #{@@duration}s"
    sleep @@duration
    do_run = false
		threads.each {|th| th.join}
  end
end
