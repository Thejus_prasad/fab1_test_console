=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: Steffen Steidten, 2011-11-25

History:
  2017-08-01 cstenzel/sfrieske, refactored all tests
=end

require_relative 'SILSmoke'


# Channel behaviour (ECP)
class SILSmoke_12 < SILSmoke
  @@test_defaults = {
    mpd: 'QA-MB-CC-M-ECP.01', ppd: 'QA-MB-CC.01', department: 'QACCALL', technology: 'TEST',
    parameters: ['QA_PARAM_900_ECP', 'QA_PARAM_901_ECP', 'QA_PARAM_902_ECP', 'QA_PARAM_903_ECP', 'QA_PARAM_904_ECP'],
    cas: ['LotHold', 'AutoLotHold', 'CancelLotHold', 'ReleaseLotHold']
  }


  def test00_setup
    $setup_ok = false
    #
    assert @@lots = @@svtest.new_lots(2), 'error creating test lots'
    @@testlots += @@lots
    $log.info "using lots #{@@lots}"
    #
    assert @@siltest.set_defaults(@@test_defaults.merge(lot: @@lots[0]))
    #
    $setup_ok = true
  end

  def test12_study_background
    assert dcr = @@siltest.cleanup_send_dcrs(:default, state: 'Study', mpd: 'QA-MB-CC-M-ECP-BACKGROUND.01')  # Criticality=B (BACKGROUND)
    tstart = Time.now
    assert @@siltest.verify_aqv_message(dcr, 'success'), 'wrong AQV message'
    refute @@siltest.wait_futurehold('Chart Check issue', tstart: tstart), 'wrong future hold'
    refute @@siltest.wait_notification('Chart Check Failures for Lot', tstart: tstart), 'wrong notification'
  end

  def test17_inline_study_background
    assert dcr = @@siltest.cleanup_send_dcrs(:default, state: 'Study', technology: '32NM', mpd: 'QA-MB-CC-M-ECP-BACKGROUND.01')  # Criticality=B (BACKGROUND)
    tstart = Time.now
    assert @@siltest.verify_aqv_message(dcr, 'success'), 'wrong AQV message'
    refute @@siltest.wait_futurehold('Chart Check issue', tstart: tstart), 'wrong future hold'
    refute @@siltest.wait_notification('Chart Check Failures for Lot', tstart: tstart), 'wrong notification'
  end

  def test22_offline_background
    assert dcr = @@siltest.cleanup_send_dcrs(:default, state: 'Offline', mpd: 'QA-MB-CC-M-ECP-BACKGROUND.01')  # Criticality=B (BACKGROUND)
    tstart = Time.now
    assert @@siltest.verify_aqv_message(dcr, 'success'), 'wrong AQV message'
    refute @@siltest.wait_futurehold('Chart Check issue', tstart: tstart), 'wrong future hold'
    refute @@siltest.wait_notification('Chart Check Failures for Lot', tstart: tstart), 'wrong notification'
  end

  def test27_inline_offline
    assert dcr = @@siltest.cleanup_send_dcrs(:default, state: 'Offline', technology: '32NM')
    tstart = Time.now
    assert @@siltest.verify_aqv_message(dcr, 'success'), 'wrong AQV message'
    refute @@siltest.wait_futurehold('Chart Check issue', tstart: tstart), 'wrong future hold'
    refute @@siltest.wait_notification('Chart Check Failures for Lot', tstart: tstart), 'wrong notification'
  end

  def test28_inline_offline_background
    assert dcr = @@siltest.cleanup_send_dcrs(:default, state: 'Offline', technology: '32NM', mpd: 'QA-MB-CC-M-ECP-BACKGROUND.01')  # Criticality=B (BACKGROUND)
    tstart = Time.now
    assert @@siltest.verify_aqv_message(dcr, 'success'), 'wrong AQV message'
    refute @@siltest.wait_futurehold('Chart Check issue', tstart: tstart), 'wrong future hold'
    refute @@siltest.wait_notification('Chart Check Failures for Lot', tstart: tstart), 'wrong notification'
  end

  def test52_2lots_offline
    assert_equal 0, @@sv.lot_cleanup(@@lots[1])
    assert dcr = @@siltest.cleanup_send_dcrs(:default, state: 'Offline', lots: @@lots)
    tstart = Time.now
    assert @@siltest.verify_aqv_message(dcr, 'success'), "wrong AQV message"
    refute @@siltest.wait_futurehold('Chart Check issue', tstart: tstart), "missing future hold"
    refute @@siltest.wait_notification('Chart Check Failures for Lot', tstart: tstart), 'notification missing'
  end

end
