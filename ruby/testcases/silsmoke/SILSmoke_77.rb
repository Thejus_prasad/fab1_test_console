=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: U. Koerner, 2012-01-11

History:
  2014-02-05 ssteidte, separated Test_Space07, code cleanup
  2017-08-01 cstenzel/sfrieske, refactored all tests
=end

require_relative 'SILSmoke'


# CAs CKC specific actions
class SILSmoke_77 < SILSmoke
  @@test_defaults = {mpd: 'QA-MB-FTDEPM-LIS-CAEX.01',
    parameters: ['QA_PARAM_700', 'QA_PARAM_701', 'QA_PARAM_702', 'QA_PARAM_703', 'QA_PARAM_704']}
  @@template = '_Template_QA_CKCS'


  def test00_setup
    $setup_ok = false
    #
    assert folder = @@lds.folder(@@templates_folder), 'missing templates folder'
    assert @@tchannel = folder.spc_channel(name: @@template), 'missing template'
    #
    ['AutoLotHold', 'ReleaseLotHold', 'AutoSetLotScriptParameter-OOC', 'AutoSetWaferScriptParameter-OOC',
     'AutoEquipmentInhibit', 'ReleaseEquipmentInhibit', 'AutoChamberInhibit', 'ReleaseChamberInhibit',
     'AutoMachineRecipeInhibit', 'ReleaseMachineRecipeInhibit',
    ].each {|action| assert SIL::SpaceApi.ca(action), "CA #{action.inspect} not defined"}
    #
    assert @@lots = @@svtest.new_lots(3), 'error creating test lot'
    @@testlots += @@lots
    assert @@siltest.set_defaults(@@test_defaults.merge(lot: @@lots.first)), 'wrong default test data'
    #
    $setup_ok = true
  end

  def test11_chambers
    set_template_ekeys(5)
    # need cycling per wafer in setup_channels for clean CKC setup
    assert dcr = @@siltest.cleanup_send_dcrs(:ooc, processing_areas: 'cycle_wafer', cas: ['AutoChamberInhibit', 'ReleaseChamberInhibit'])
    #
    # verify inhibits, CKC ids start with 1
    @@siltest.chambers.size.times {|i|
      assert @@siltest.wait_inhibit(:eqp_chamber, @@siltest.ptool, "CKC_ID->#{i + 1}", n: @@siltest.parameters.size), 'missing inhibit'
    }
    #
    assert folder = @@lds.folder(@@autocreated_qa), 'missing folder'
    #
    @@siltest.parameters.each_with_index {|p, i|
      $log.info "parameter #{p}"
      assert ch = folder.spc_channel(p), 'missing channel'
      assert ckcs = ch.channel.allCKCs  # are Java objects!
      assert_equal @@siltest.chambers.size, ckcs.size, 'wrong number of CKCs'
      ckcs.each.each {|ckc|
        # CKC name must be a single chamber (PChamber) (this is the unique part of this TC compared to other CA tests)
        assert @@siltest.chambers.member?(ckc.config.name), 'wrong CKC name'
        # verify CKC sample ecomment
        csample = SIL::SpaceApi::SpcChannel.new(ckc).samples.last
        assert csample.ecomment.include?("TOOL+CHAMBER_HELD: #{@@siltest.ptool}/#{ckc.config.name}: reason={X-S"), 'wrong ecomment'
        # release inhibit from CKC sample
        assert csample.assign_ca('ReleaseChamberInhibit'), 'error assigning CA'
      }
      n =  SIL::DCR::DefaultWaferValues.size * (@@siltest.parameters.size - i - 1)
      assert @@siltest.wait_inhibit(:eqp_chamber, @@siltest.ptool, 'CKC_ID->', n: n), 'wrong inhibit'
    }
    assert @@siltest.wait_inhibit(:eqp_chamber, @@siltest.ptool, '', n: 0), 'wrong inhibit'
  end

  def test12_chambers_waferscriptparameter
    set_template_ekeys(5)
    # get real wafers and remove OOC parameter
    assert wafers = @@sv.lot_info(@@siltest.lot, wafers: true).wafers.take(3).collect {|w| w.wafer}
    wafers.each {|wafer|
      assert_equal 0, @@sv.user_parameter_delete('Wafer', wafer, 'OOC', check: true)
    }
    readings = {wafers[0]=>145, wafers[1]=>150, wafers[2]=>155}
    # set clean channels, need cycling per wafer in setup_channels for clean CKC setup, step by step because of real wafers used!
    assert @@siltest.submit_process_dcr(readings: readings, processing_areas: 'cycle_wafer')
    assert @@siltest.submit_meas_dcr(readings: readings)
    assert folder = @@lds.folder(@@autocreated_qa)
    assert channels = folder.spc_channels
    assert @@siltest.assign_verify_cas(channels, ['AutoSetWaferScriptParameter-OOC'])
    #
    # submit process and meas DCRs
    assert @@siltest.submit_process_dcr(readings: readings, processing_areas: 'cycle_wafer')
    assert @@siltest.submit_meas_dcr(readings: readings)
    # verify wafer script parameter is set
    wafers.each {|wafer| assert_equal 'Yes', @@sv.user_parameter('Wafer', wafer, name: 'OOC').value, 'wrong script parameter'}
  end

  def test13_recipes
    3.times {|i| assert_equal 0, @@sv.inhibit_cancel(:recipe, "P-UTC001_#{i}.UTMR000.01")}
    set_template_ekeys(3)
    assert channels = scenario_multiple_dcrs(@@lots, ['AutoMachineRecipeInhibit', 'ReleaseMachineRecipeInhibit'])
    # all but first CKC has violations
    (2..4).each {|ckcid|
      mrecipe = "P-UTC001_#{ckcid - 2}.UTMR000.01"
      assert @@siltest.wait_inhibit(:recipe, mrecipe, "CKC_ID->#{ckcid}", n: channels.size), 'missing inhibit'
    }
    #
    channels.each_with_index {|ch, i|
      assert ckcs = ch.channel.allCKCs  # are Java objects!
      ckcs.each {|ckc|
        next if ckc.ckc_id == 1
        # verify CKC sample ecomment
        csample = SIL::SpaceApi::SpcChannel.new(ckc).samples.last
        mrecipe = "P-UTC001_#{ckc.ckc_id - 2}.UTMR000.01"
        assert csample.ecomment.include?("RECIPE_HELD: #{mrecipe}: reason={X-S"), 'wrong ecomment'
        # release inhibit from CKC sample
        assert csample.assign_ca('ReleaseMachineRecipeInhibit'), 'error assigning CA'
      }
      (2..4).each {|ckcid|
        mrecipe = "P-UTC001_#{ckcid - 2}.UTMR000.01"
        assert @@siltest.wait_inhibit(:recipe, mrecipe, "CKC_ID->#{ckcid}", n: channels.size - i - 1), 'missing inhibit'
      }
    }
  end

  def test14_tools
    set_template_ekeys(4)
    assert channels = scenario_multiple_dcrs(@@lots, ['AutoEquipmentInhibit', 'ReleaseEquipmentInhibit'])
    # all but first CKC has violations
    (2..4).each {|ckcid|
      eqp = "#{@@siltest.ptool}_#{ckcid - 2}"
      assert @@siltest.wait_inhibit(:eqp, eqp, "CKC_ID->#{ckcid}", n: channels.size), 'missing inhibit'
    }
    assert_empty @@sv.inhibit_list(:eqp, @@siltest.ptool), 'wrong inhibit'
    #
    channels.each_with_index {|ch, i|
      assert ckcs = ch.channel.allCKCs  # are Java objects!
      ckcs.each {|ckc|
        next if ckc.ckc_id == 1
        # verify CKC sample ecomment
        csample = SIL::SpaceApi::SpcChannel.new(ckc).samples.last
        eqp = "#{@@siltest.ptool}_#{ckc.ckc_id - 2}"
        assert csample.ecomment.include?("TOOL_HELD: #{eqp}: reason={X-S"), 'wrong ecomment'
        # release inhibit from CKC sample
        assert csample.assign_ca('ReleaseEquipmentInhibit'), 'error assigning CA'
      }
      (2..4).each {|ckcid|
        eqp = "#{@@siltest.ptool}_#{ckcid - 2}"
        assert @@siltest.wait_inhibit(:eqp, eqp, "CKC_ID->#{ckcid}", n: channels.size - i - 1), 'missing inhibit'
      }
    }
  end

  def test15_products
    set_template_ekeys(9)
    assert channels = scenario_multiple_dcrs(@@lots, ['AutoLotHold', 'ReleaseLotHold'])
    #
    (2..4).each {|ckcid|
      assert @@siltest.wait_futurehold("CKC_ID->#{ckcid}", lot: @@lots[ckcid - 2], n: channels.size), 'missing future hold'
    }
    @@lots.each {|lot| assert_equal 0, @@sv.lot_gatepass(lot), 'SiView GatePass error'}  # for release action
    channels.each_with_index {|ch, i|
      assert ckcs = ch.channel.allCKCs  # are Java objects!
      ckcs.each {|ckc|
        next if ckc.ckc_id == 1
        # verify CKC sample ecomment
        csample = SIL::SpaceApi::SpcChannel.new(ckc).samples.last
        lot = @@lots[ckc.ckc_id - 2]
        assert csample.ecomment.include?("LOT_HELD: #{lot}: reason={X-S"), 'wrong ecomment'
        # release future hold from CKC sample
        assert csample.assign_ca('ReleaseLotHold'), 'error assigning CA'
      }
      (2..4).each {|ckcid|
        lot = @@lots[ckcid - 2]
        if i > 3
          assert @@siltest.wait_lothold_release("CKC_ID->#{ckcid}", lot: lot), 'wrong lot hold'
        else
          refute_empty @@sv.lot_hold_list(lot, type: 'FutureHold'), 'missing lot hold'  # TODO: remove X-SPC holds first!
        end
      }
    }
  end

  def test16_products_lotscriptparameter
    @@lots.each {|lot|
      assert_equal 0, @@sv.user_parameter_delete('Lot', lot, 'OOC', check: true)
    }
    set_template_ekeys(9)
    assert channels = scenario_multiple_dcrs(@@lots, ['AutoSetLotScriptParameter-OOC'])
    # verify lot script parameter is set
    @@lots.each {|lot| assert_equal 'Yes', @@sv.user_parameter('Lot', lot, name: 'OOC').value, 'wrong script parameter'}
  end

  def test17_products_multilot_lotparameter
    set_template_ekeys(9)
    @@lots.each {|lot| assert_equal 0, @@sv.lot_cleanup(lot)}
    # prepare channels
    # need cycling per wafer for correct ckc setup  ## TODO: really???
    assert channels = @@siltest.setup_channels(cas: ['AutoLotHold', 'ReleaseLotHold'])
    # send process DCRs with different products per lot
    @@lots.each_with_index {|lot, i| assert @@siltest.submit_process_dcr(lot: lot, product: "SILPROD#{i}.01")}
    # send multilot meas DCR (instead of multiple DCRs) with lot parameter
    dcr = SIL::DCR.new(eqp: @@siltest.mtool)
    @@lots.each_with_index {|lot, i|
      dcr.add_lot(lot, op: @@siltest.mpd, product: "SILPROD#{i}.01")
      lotvalue = i.even? ? 145 + i : 45 + i
      dcr.add_parameters(@@siltest.parameters, {lot=>lotvalue}, readingtype: 'Lot')
    }
    assert @@siltest.send_dcr_verify(dcr), 'error sending DCR'
    #
    # verify future holds
    @@lots.each_with_index {|lot, i|
      if i.even?
         assert @@siltest.wait_futurehold('CKC_ID->', lot: lot, n: channels.size), 'missing future hold'
      else
        assert_empty @@sv.lot_futurehold_list(lot), 'wrong future hold'
      end
    }
    # verify CKC sample ecomments
    channels.each {|ch|
      assert ckcs = ch.channel.allCKCs  # are Java objects!
      ckcs.each {|ckc|
        next if ckc.ckc_id == 1
        csample = SIL::SpaceApi::SpcChannel.new(ckc).samples.last
        if csample.ekeys['Product.EC'][-4].to_i.even?
          assert csample.ecomment.include?('LOT_HELD:'), 'wrong ecomment'
        else
          assert_nil csample.ecomment, 'wrong ecomment'
        end
      }
    }
  end

  def test18_products_multilot
    set_template_ekeys(9)
    @@lots.each {|lot| assert_equal 0, @@sv.lot_cleanup(lot)}
    # prepare channels
    # need cycling per wafer for correct ckc setup  ## TODO: really???
    assert channels = @@siltest.setup_channels(cas: ['AutoLotHold', 'ReleaseLotHold'])
    # send process DCRs with different products per lot
    @@lots.each_with_index {|lot, i| assert @@siltest.submit_process_dcr(lot: lot, product: "SILPROD#{i}.01")}
    # send multilot meas DCR (instead of multiple DCRs)
    dcr = SIL::DCR.new(eqp: @@siltest.mtool)
    @@lots.each_with_index {|lot, i|
      dcr.add_lot(lot, op: @@siltest.mpd, product: "SILPROD#{i}.01")
      dcr.add_parameters(@@siltest.parameters, i.even? ? :ooc : :default)
    }
    assert @@siltest.send_dcr_verify(dcr), 'error sending DCR'
    #
    # verify future holds
    @@lots.each_with_index {|lot, i|
      if i.even?
         assert @@siltest.wait_futurehold('CKC_ID->', lot: lot, n: channels.size), 'missing future hold'
      else
        assert_empty @@sv.lot_futurehold_list(lot), 'wrong future hold'
      end
    }
    $channels = channels
    # verify CKC sample ecomments
    channels.each {|ch|
      assert ckcs = ch.channel.allCKCs  # are Java objects!
      ckcs.each {|ckc|
        next if ckc.ckc_id == 1
        csample = SIL::SpaceApi::SpcChannel.new(ckc).samples.last
        if csample.ekeys['Product.EC'][-4].to_i.even?
          assert csample.ecomment.include?('LOT_HELD:'), 'wrong ecomment'
        else
          assert_nil csample.ecomment, 'wrong ecomment'
        end
      }
    }
  end


  # aux methods

  def set_template_ekeys(keyid)
    assert @@tchannel.keys = [{keyid: keyid, fixed: '*'}, {keyid: 6, fixed: 'FIXED'}, {keyid: 10, fixed: 'FIXED'}]
  end

  def scenario_multiple_dcrs(lots, cas)
    assert_equal 0, @@sv.eqp_cleanup(@@siltest.ptool)
    lots.each_with_index {|lot, i|
      assert_equal 0, @@sv.eqp_cleanup("#{@@siltest.ptool}_#{i}")
      assert_equal 0, @@sv.lot_cleanup(lot)
    }
    assert channels = @@siltest.setup_channels(cas: cas)
    # send process DCRs
    lots.each_with_index {|lot, i|
      assert @@siltest.submit_process_dcr(ptool: "#{@@siltest.ptool}_#{i}", lot: lot, product: "SILPROD#{i}.01",
        lrecipe: "F-P-FTEOS8K15.0#{i}", mrecipe: "P-UTC001_#{i}.UTMR000.01")
    }
    # send meas DCRs
    lots.each_with_index {|lot, i| assert @@siltest.submit_meas_dcr(lot: lot, product: "SILPROD#{i}.01", readings: :ooc)}
    #
    return channels
  end

end
