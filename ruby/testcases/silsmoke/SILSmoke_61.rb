=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: Conny Stenzel, 2012-06-27

History:
  2017-08-01 cstenzel/sfrieske, refactored all tests
=end

require_relative 'SILSmoke'


# Spec Limits (PCP)
class SILSmoke_61 < SILSmoke
  @@test_defaults = {mpd: 'QA-MB-FTDEPM-LIS-CAEX.01'}
  @@messages = ['[(Mean above control limit)', '[(Mean below control limit)', '[(Raw above specification)', '[(Raw below specification)']


  def test00_setup
    $setup_ok = false
    #
    assert lot = @@svtest.new_lot, 'error creating test lot'
    @@testlots << lot
    assert @@siltest.set_defaults(@@test_defaults.merge(lot: lot))  # work with default parameters
    #
    $setup_ok = true
  end

  def test11_speclimits_1
    assert dcr = @@siltest.submit_meas_dcr(parameters: [@@siltest.parameters.first], ooc: true)
    assert holds = @@siltest.wait_futurehold(@@messages), 'missing future hold'
    assert @@messages.inject(false) {|ok, m| ok || holds.first.memo.include?(m)}, 'wrong hold memo'
  end

  def test12_speclimits_all
    assert dcr = @@siltest.submit_meas_dcr(ooc: true)
    assert holds = @@siltest.wait_futurehold(@@messages, n: @@siltest.parameters.size), 'missing future hold'
    holds.each {|hold| assert @@messages.inject(false) {|ok, m| ok || hold.memo.include?(m)}, 'wrong hold memo'}
  end

  def test15_parameter_extension_minus
    parameters = @@siltest.parameters.take(2)
    ctrl = {'MEAN'=>410, 'STDDEV'=>1000, 'MIN'=>-550, 'MAX'=>550, 'COUNT'=>5000}
    # build and submit DCR
    dcr = SIL::DCR.new
    dcr.add_lot(@@siltest.lot)
    parameters.each {|p| ctrl.each_pair {|k, v| dcr.add_parameter("#{p}-#{k}", {'2502J493KO'=>v})}}
    assert @@siltest.send_dcr_verify(dcr, nsamples: 2), 'error submitting DCR'
    # verify sample statistics
    folder = @@lds.folder(@@autocreated_qa)
    parameters.each {|p|
      assert ch = folder.spc_channel(p)
      assert s = ch.samples.last
      assert s = ch.samples.last
      assert verify_hash(ctrl, s.statistics, nil, refonly: true), 'wrong sample statistics'
      assert verify_hash({'LSL'=>25.0, 'TARGET'=>45.0, 'USL'=>75.0}, s.specs, nil, refonly: true), 'wrong sample speclimits'
    }
  end

  def test16_speclimits_all_TEST
    assert dcr = @@siltest.submit_meas_dcr(ooc: true, technology: 'TEST')
    assert holds = @@siltest.wait_futurehold(@@messages, n: @@siltest.parameters.size), 'missing future hold'
    holds.each {|hold| assert @@messages.inject(false) {|ok, m| ok || hold.memo.include?(m)}, 'wrong hold memo'}
  end

  def test17_wafer_and_lot_based
    # build and submit meas DCR
    dcr = SIL::DCR.new
    dcr.add_lot(@@siltest.lot, op: @@siltest.mpd)
    dcr.add_parameters(@@siltest.parameters[0..-2], :ooc, processing_areas: ['CHC'])
    dcr.add_parameter(@@siltest.parameters[-1], {@@siltest.lot=>7.0}, readingtype: 'Lot')
    assert @@siltest.send_dcr_verify(dcr), 'error submitting DCR'
    assert holds = @@siltest.wait_futurehold(@@messages, n: @@siltest.parameters.size), 'missing future hold'
    holds.each {|hold| assert @@messages.inject(false) {|ok, m| ok || hold.memo.include?(m)}, 'wrong hold memo'}
  end

  # MSR513786
  def test30_speclimits_noscenario_nooc
    #MSR513786.QA 513786NM  MSR-513786.01 MSR513786PG|MSR513786PROD.01  QA_PARAM_990    250 450 750 WAFER
    #MSR513786.QA 513786NM  MSR-513786.01 MSR513786PG|MSR513786PROD.01  QA_PARAM_991    250 450 750 WAFER
    #MSR513786.QA 513786NM  MSR-513786.01 MSR513786PG|MSR513786PROD.01  QA_PARAM_992    250 450 750 WAFER
    #MSR513786.QA 513786NM  MSR-513786.01 MSR513786PG|MSR513786PROD.01  QA_PARAM_993    250 450 750 WAFER
    #MSR513786.QA 513786NM  MSR-513786.01 MSR513786PG|MSR513786PROD.01  QA_PARAM_994    250 450 750 WAFER
    #MSR513786.QA 513786NM  MSR-513786.01 MSR513786PG|MSR513786PROD.01  QA_PARAM_995    250 450 750 WAFER
    #
    # build and submit DCR with all parameters
    # special  LSL, TARGET and USL values
    parameters = ['QA_PARAM_990', 'QA_PARAM_991', 'QA_PARAM_992', 'QA_PARAM_993', 'QA_PARAM_994', 'QA_PARAM_995']
    mpd = 'MSR513786.QA'
    technology = '32NM'
    route = 'MSR-513786.01'
    productgroup = 'MSR513786PG'
    product = 'MSR513786PROD.01'
    # submit dcr
    dcr = SIL::DCR.new
    dcr.add_lot(:default, op: mpd, productgroup: productgroup, product: product, technology: technology, route: route)
    r = Hash[3.times.collect {|i| ["WAFER#{i}", 310 + i]}]
    dcr.add_parameters(parameters, r)
    assert @@siltest.send_dcr_verify(dcr), 'error submitting DCR'
    # verif channels and sample speclimits
    assert @@siltest.verify_channels(dcr, parameters: parameters) {|ch, sample|
      verify_hash({'LSL'=>250, 'TARGET'=>450, 'USL'=>750}, sample.specs, nil, refonly: true)
    }, 'wrong channel'
  end

  # MSR513786
  def test31_speclimits_scenario_nooc
    skip "not applicable in #{$env}" unless ['itdc', 'let'].member?($env)
    #MSR513786.QA 513786NM  MSR-513786.01 MSR513786PG|MSR513786PROD.01  QA_PARAM_990    250 450 750 WAFER
    #MSR513786.QA 513786NM  MSR-513786.01 MSR513786PG|MSR513786PROD.01  QA_PARAM_991    250 450 750 WAFER             CALCULATION
    #MSR513786.QA 513786NM  MSR-513786.01 MSR513786PG|MSR513786PROD.01  QA_PARAM_992    250 450 750 WAFER             MANUAL_SPECLIMIT_SPACE
    #MSR513786.QA 513786NM  MSR-513786.01 MSR513786PG|MSR513786PROD.01  QA_PARAM_993    250 450 750 WAFER             MANUAL_SPECLIMIT_SPACE
    #MSR513786.QA 513786NM  MSR-513786.01 MSR513786PG|MSR513786PROD.01  QA_PARAM_994    250 450 750 WAFER             IGNORE_ALL_PARAMETER_CHECK
    #MSR513786.QA 513786NM  MSR-513786.01 MSR513786PG|MSR513786PROD.01  QA_PARAM_995    250 450 750 WAFER             IGNORE_ALL_PARAMETER_CHECK
    #
    # build and submit DCR with all parameters
    parameters = ['QA_PARAM_990', 'QA_PARAM_991', 'QA_PARAM_992', 'QA_PARAM_993', 'QA_PARAM_994', 'QA_PARAM_995']
    mpd = 'MSR513786_1.QA'
    technology = '32NM'
    route = 'MSR-513786.01'
    productgroup = 'MSR513786PG'
    product = 'MSR513786PROD.01'
    # submit dcr
    dcr = SIL::DCR.new
    dcr.add_lot(:default, op: mpd, productgroup: productgroup, product: product, technology: technology, route: route)
    r = Hash[3.times.collect {|i| ["WAFER#{i}", 310 + i]}]
    dcr.add_parameters(parameters, r)
    assert @@siltest.send_dcr_verify(dcr), 'error submitting DCR'
    # verify channel and sample speclimits for 1st parameter
    assert @@siltest.verify_channels(dcr, parameters: [parameters.first]) {|ch, sample|
      verify_hash({'LSL'=>250, 'TARGET'=>450, 'USL'=>750}, sample.specs, nil, refonly: true)
    }, 'wrong channel'
    # verify channel and sample speclimits for remaining parameters
    assert @@siltest.verify_channels(dcr, parameters: parameters[1..-1]) {|ch, sample|
      verify_hash({'LSL'=>nil, 'TARGET'=>nil, 'USL'=>nil}, sample.specs, nil, refonly: true)
    }, 'wrong channel'
  end

  # MSR513786
  def test32_speclimits_scenario_ooc
    skip "not applicable in #{$env}" unless ['itdc', 'let'].member?($env)
    #MSR513786.QA 513786NM  MSR-513786.01 MSR513786PG|MSR513786PROD.01  QA_PARAM_990    250 450 750 WAFER
    #MSR513786.QA 513786NM  MSR-513786.01 MSR513786PG|MSR513786PROD.01  QA_PARAM_991    250 450 750 WAFER             CALCULATION
    #MSR513786.QA 513786NM  MSR-513786.01 MSR513786PG|MSR513786PROD.01  QA_PARAM_992    250 450 750 WAFER             MANUAL_SPECLIMIT_SPACE
    #MSR513786.QA 513786NM  MSR-513786.01 MSR513786PG|MSR513786PROD.01  QA_PARAM_993    250 450 750 WAFER             MANUAL_SPECLIMIT_SPACE
    #MSR513786.QA 513786NM  MSR-513786.01 MSR513786PG|MSR513786PROD.01  QA_PARAM_994    250 450 750 WAFER             IGNORE_ALL_PARAMETER_CHECK
    #MSR513786.QA 513786NM  MSR-513786.01 MSR513786PG|MSR513786PROD.01  QA_PARAM_995    250 450 750 WAFER             IGNORE_ALL_PARAMETER_CHECK
    #
    # build and submit DCR with all parameters
    parameters = ['QA_PARAM_990', 'QA_PARAM_991', 'QA_PARAM_992', 'QA_PARAM_993', 'QA_PARAM_994', 'QA_PARAM_995']
    mpd = 'MSR513786_1.QA'
    technology = '32NM'
    route = 'MSR-513786.01'
    productgroup = 'MSR513786PG'
    product = 'MSR513786PROD.01'
    # submit dcr
    dcr = SIL::DCR.new
    dcr.add_lot(:default, op: mpd, productgroup: productgroup, product: product, technology: technology, route: route)
    r = Hash[3.times.collect {|i| ["WAFER#{i}", 800 + i]}]
    dcr.add_parameters(parameters, r)
    assert @@siltest.send_dcr_verify(dcr), 'error submitting DCR'
    # verify channel and sample speclimits for 1st parameter
    assert @@siltest.verify_channels(dcr, parameters: [parameters.first]) {|ch, sample|
      verify_hash({'LSL'=>250, 'TARGET'=>450, 'USL'=>750}, sample.specs, nil, refonly: true)
    }, 'wrong channel'
    # verify channel and sample speclimits for remaining parameters
    assert @@siltest.verify_channels(dcr, parameters: parameters[1..-1]) {|ch, sample|
      verify_hash({'LSL'=>nil, 'TARGET'=>nil, 'USL'=>nil}, sample.specs, nil, refonly: true)
    }, 'wrong channel'
  end

end
