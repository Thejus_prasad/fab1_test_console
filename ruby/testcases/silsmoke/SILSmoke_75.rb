=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: U. Koerner, 2012-01-11

History:
  2014-02-05 ssteidte, separated Test_Space07, code cleanup
  2017-08-01 cstenzel/sfrieske, refactored all tests
=end

require_relative 'SILSmoke'


# CAs EqpAndRecipe and ChamberAndRecipe inhibits, TODO: optionally skip DFS msg checks (?)
class SILSmoke_75 < SILSmoke
  @@precipe = 'P.UTMR000.01'
  @@other_recipe = 'P-UTMR001.01'
  @@eqp_no_recipe = 'UTFSIL04'
  @@check_dfs = false


  def test00_setup
    $setup_ok = false
    #
    if @@check_dfs
      assert @@dfs = MQ::JMS.new("dfs_space.#{$env}"), 'no connection to DFS queue'
    end
    # Equipment CAs
    ['AutoEquipmentAndRecipeInhibit-Single', 'AutoEquipmentAndRecipeInhibit-Multi', 'AutoEquipmentAndRecipeInhibit-Wildcard',
      'AutoEquipmentAndRecipeInhibit-Precipe',
      'AutoEquipmentAndRecipeInhibit-Error', 'AutoEquipmentAndRecipeInhibit-ErrorMulti', 'AutoEquipmentAndRecipeInhibit-ErrorWild',
      'AutoEquipmentAndRecipeInhibit-Eqp', 'AutoEquipmentAndRecipeInhibit-WildcardE', 'AutoEquipmentAndRecipeInhibit-ErrorWildE',
      'ReleaseEquipmentAndRecipeInhibit', 'ReleaseEquipmentAndRecipeInhibit-noDeflt', 'CACollection-ReleaseAllRecipeInhibits',
      'EquipmentAndRecipeInhibit', 'EquipmentAndRecipeInhibit-Single', 'EquipmentAndRecipeInhibit-Multi', 'EquipmentAndRecipeInhibit-Wildcard',
      'EquipmentAndRecipeInhibit-Eqp', 'EquipmentAndRecipeInhibit-WildcardE'
    ].each {|action| assert SIL::SpaceApi.ca(action), "CA #{action.inspect} not defined"}
    # Chamber CAs
    ['AutoChamberAndRecipeInhibit-Single', 'AutoChamberAndRecipeInhibit-Multi',
      'AutoChamberAndRecipeInhibit-Precipe',
      'ReleaseChamberAndRecipeInhibit', 'CACollection-ReleaseAllRecipeInhibits',
      'ChamberAndRecipeInhibit', 'ChamberAndRecipeInhibit-Single', 'ChamberAndRecipeInhibit-Multi', 'ChamberAndRecipeInhibit-Wildcard',
      'ChamberAndRecipeInhibit-Eqp', 'ChamberAndRecipeInhibit-WildcardE'
    ].each {|action| assert SIL::SpaceApi.ca(action), "CA #{action.inspect} not defined"}
    assert lot = @@svtest.new_lot, 'error creating test lot'
    @@testlots << lot
    assert @@siltest.set_defaults(lot: lot), 'wrong default test data'
    #
    $setup_ok = true
  end

  def test11_single
    scenario_eqp_recipe('AutoEquipmentAndRecipeInhibit-Single', ['P-UTC001_0.UTMR000.01'], manual_cas: ['EquipmentAndRecipeInhibit-Single'])
  end

  def test12_multi
    scenario_eqp_recipe('AutoEquipmentAndRecipeInhibit-Multi', ['P-UTC001_0.UTMR000.01', 'P-UTC001_1.UTMR000.01'], manual_cas: ['EquipmentAndRecipeInhibit-Multi'])
  end

  def test13_wildcard
    scenario_eqp_recipe('AutoEquipmentAndRecipeInhibit-Wildcard', (0..4).collect {|i| "P-UTC001_#{i}.UTMR000.01"}, manual_cas: ['EquipmentAndRecipeInhibit-Wildcard'])
  end

  def test14_precipe
    scenario_eqp_recipe('AutoEquipmentAndRecipeInhibit-Precipe', [@@precipe], manual_cas: ['EquipmentAndRecipeInhibit'])
  end

  # check only precipe is inhibited, but no other recipe matching the pattern
  def test15_wildcard_eqp
    assert recipes = @@sv.eqp_related_recipes(@@siltest.ptool), "no recipe found for #{@@siltest.ptool}"
    $log.info "recipes for eqp #{@@siltest.ptool}: #{recipes}"
    refute recipes.include?(@@other_recipe), "wrong recipe #{@@other_recipe}"
    scenario_eqp_recipe('AutoEquipmentAndRecipeInhibit-WildcardE', [@@precipe], not_inhibited_recipes: [@@other_recipe], manual_cas: ['EquipmentAndRecipeInhibit-WildcardE'])
  end

  # check not only precipe is inhibited
  def test16_other_recipes
    assert recipes = @@sv.eqp_related_recipes(@@siltest.ptool), "no recipe found for #{@@siltest.ptool}"
    $log.info "recipes for eqp #{@@siltest.ptool}: #{recipes}"
    assert recipes.count > 1, "at least 2 recipes expected"
    scenario_eqp_recipe('AutoEquipmentAndRecipeInhibit-Eqp', recipes, manual_cas: ['EquipmentAndRecipeInhibit-Eqp'])
  end

  def test21_error_single
    scenario_eqp_recipe_notfound('AutoEquipmentAndRecipeInhibit-Error', 'Execution of [AutoEquipmentAndRecipeInhibit-Error] failed', [])
  end

  def test22_error_multi
    scenario_eqp_recipe_notfound('AutoEquipmentAndRecipeInhibit-ErrorMulti', 'Execution of [AutoEquipmentAndRecipeInhibit-ErrorMulti] failed', ['P-UTC001_0.UTMR000.01'])
  end

  def test23_error_wildcard
    scenario_eqp_recipe_notfound('AutoEquipmentAndRecipeInhibit-ErrorWild', 'Execution of [AutoEquipmentAndRecipeInhibitAction] error', [])
  end

  # no assigned recipe matching the pattern found
  def test24_error_pattern
    scenario_eqp_recipe_notfound('AutoEquipmentAndRecipeInhibit-ErrorWildE', 'Execution of [AutoEquipmentAndRecipeInhibitAction] error', [], @@eqp_no_recipe)
  end

  # setup error: no recipe assigned to tool
  def test25_error_no_recipe
    assert_empty @@sv.eqp_related_recipes(@@eqp_no_recipe), 'wrong recipe'
    scenario_eqp_recipe_notfound('AutoEquipmentAndRecipeInhibit-Eqp', 'Execution of [AutoEquipmentAndRecipeInhibitAction] error', [], @@eqp_no_recipe)
  end

  def test31_all
    mrecipes = ['P-UTC001_0.UTMR000.01', 'P-UTC001_1.UTMR000.01']
    (mrecipes + [@@precipe]).each {|r| assert_equal 0, @@sv.inhibit_cancel(:recipe, r)}
    #
    assert channels = @@siltest.setup_channels(cas: ['AutoMachineRecipeInhibit', 'AutoEquipmentAndRecipeInhibit-Multi',
      'AutoChamberAndRecipeInhibit-Multi', 'CACollection-ReleaseAllRecipeInhibits'])
    # submit process DCR
    assert dcrp = @@siltest.submit_process_dcr(mrecipe: @@precipe), 'error submitting DCR'
    # submit meas DCR with OOC values
    assert dcr = @@siltest.submit_meas_dcr(ooc: true), 'error submitting DCR'
    #
    # verify comment and inhibit is set
    @@siltest.chambers.each {|cha|
      mrecipes.each {|mrecipe|
        assert @@siltest.verify_ecomment(channels, 'TOOL+RECIPE_HELD: reason={X-S')
        assert @@siltest.verify_ecomment(channels, "#{@@siltest.ptool}/")
        assert @@siltest.verify_ecomment(channels, mrecipe)
        ninhs = @@siltest.chambers.size * @@siltest.parameters.size
        assert @@siltest.wait_inhibit(:eqp_chamber_recipe, [@@siltest.ptool, mrecipe], '', n: ninhs), 'wrong inhibit'
      }
    }
    mrecipes.each {|mrecipe|
      assert @@siltest.wait_inhibit(:eqp_recipe, [@@siltest.ptool, mrecipe], '', n: @@siltest.parameters.size), 'wrong inhibit'
      assert @@siltest.verify_ecomment(channels, 'TOOL+RECIPE_HELD: reason={X-S')
      assert @@siltest.verify_ecomment(channels, "#{@@siltest.ptool}/")
      assert @@siltest.verify_ecomment(channels, mrecipe)
    }
    assert @@siltest.wait_inhibit(:recipe, @@precipe, '', n: @@siltest.parameters.size), 'missing inhibit'
    #sleep 10
    # release all inhibits
    channels.each {|ch| assert ch.ckc_samples.last.assign_ca('CACollection-ReleaseAllRecipeInhibits'), 'error assigning CA'}
    mrecipes.each {|mrecipe|
      assert @@siltest.wait_inhibit(:eqp_recipe, [@@siltest.ptool, mrecipe], '', n: 0), 'wrong inhibit'
      assert @@siltest.wait_inhibit(:eqp_chamber_recipe, [@@siltest.ptool, mrecipe], '', n: 0), 'wrong inhibit'
    }
    assert @@siltest.wait_inhibit(:recipe, @@precipe, '', n: 0), 'wrong inhibit'
  end


  # aux methods

  def scenario_eqp_recipe(ca, mrecipes, params={})
    not_inhibited_recipes = params[:not_inhibited_recipes] || []
    manual_cas = params[:manual_cas] || []
    (mrecipes + [@@precipe] + not_inhibited_recipes).uniq.each {|r| assert_equal 0, @@sv.inhibit_cancel(:recipe, r)}
    #
    assert_equal 0, @@sv.eqp_cleanup(@@siltest.ptool)
    assert channels = @@siltest.setup_channels(cas: [ca, 'ReleaseEquipmentAndRecipeInhibit', 'ReleaseEquipmentAndRecipeInhibit-noDeflt'])
    # submit process DCR
    assert dcrp = @@siltest.submit_process_dcr(mrecipe: @@precipe), 'error submitting DCR'
    # submit meas DCR with OOC values
    assert dcr = @@siltest.submit_meas_dcr(ooc: true), 'error submitting DCR'
    #
    # verify comment and inhibit is set
    mrecipes.each {|mrecipe|
      assert @@siltest.wait_inhibit(:eqp_recipe, [@@siltest.ptool, mrecipe], '', n: @@siltest.parameters.size), 'missing inhibit'
      channels.each {|channel|
        assert sample = channel.ckc_samples.last, "no CKC samples in channel #{channel}"
        assert ecomment = sample.ecomment, 'missing ecomment'
        assert ecomment.include?('TOOL+RECIPE_HELD: reason={X-S'), 'wrong ecomment'
        assert ecomment.include?("#{@@siltest.ptool}/"), 'wrong ecomment'
        assert ecomment.include?(mrecipe), 'wrong ecomment'
      }
    }
    assert_empty @@sv.inhibit_list(:eqp_recipe, [@@siltest.ptool, @@precipe]), 'wrong inhibit' unless mrecipes.include?(@@precipe)
    #
    # this is pointless because the recipe does not exist in SiView! TODO
    not_inhibited_recipes.each {|mrecipe| assert_empty @@sv.inhibit_list(:eqp_recipe, [@@siltest.ptool, mrecipe]), 'wrong inhibit'}
    #
    if @@check_dfs
      assert verify_dfs_message(channels, mrecipes.collect {|r| "TOOL+RECIPE_HELD: #{@@siltest.ptool}/#{r}"}), 'wrong DFS message'
    end
    # try to release inhibit noDefault w/o comment
    channels.each {|ch| refute ch.ckc_samples.last.assign_ca('ReleaseEquipmentAndRecipeInhibit-noDeflt'), 'wrong CA assignment'}
    mrecipes.each {|mrecipe| assert_equal channels.size, @@sv.inhibit_list(:eqp_recipe, [@@siltest.ptool, mrecipe]).size, 'missing inhibit'}
    #
    # release inhibit noDefault with comment
    channels.each {|ch|
      # because of the order of the recipes is somehow random - need to read the ecomment to identify the inhibit
      ecomment = ch.ckc_samples.last.ecomment.split('toolRecipeList:').last.gsub("\n", '')
      mrecipes.each {|mrecipe| assert ecomment.include?(mrecipe), 'wrong ecomment'}
      assert ch.ckc_samples.last.assign_ca('ReleaseEquipmentAndRecipeInhibit-noDeflt', comment: "[Release=#{ecomment}]"), 'error assigning CA'
    }
    tstart = Time.now
    mrecipes.each {|mrecipe| assert @@siltest.wait_inhibit(:eqp_recipe, [@@siltest.ptool, mrecipe], '', n: 0, tstart: tstart), 'wrong inhibit'}
    #
    # send process and measurement DCRs again
    assert_equal 0, @@sv.lot_cleanup(@@siltest.lot)
    assert @@siltest.send_dcr_verify(dcrp), 'error sending DCR'
    assert_equal 0, @@sv.lot_cleanup(@@siltest.lot)
    assert @@siltest.send_dcr_verify(dcr), 'error sending DCR'
    #
    # assign CA to release inhibit and verify
    channels.each {|ch| assert ch.ckc_samples.last.assign_ca('ReleaseEquipmentAndRecipeInhibit'), 'error assigning CA'}
    mrecipes.each {|mrecipe| assert @@siltest.wait_inhibit(:eqp_recipe, [@@siltest.ptool, mrecipe], '', n: 0, tstart: tstart), 'wrong inhibit'}
    # manual cas corresponding to auto cas have been established with MSR1108802 in SIL 5.4
    # This functionality (SiView->ToolRecipeList->Inhibit all recipes found) seems to be time consuming.
    # Therefore the time can be measured.
    # Result with SIL 5.5 was ~52 seconds for inhibiting and ~52 seconds for releasing 7 recipes in 5 channels = 35 inhibits/releases.
    manual_cas.each {|manual_ca|
      #start = Time.now
      channels.each_with_index {|ch, i| 
        assert ch.ckc_samples.last.assign_ca(manual_ca), 'error assigning CA'
        mrecipes.each {|mrecipe| assert @@siltest.wait_inhibit(:eqp_recipe, [@@siltest.ptool, mrecipe], '', n: (i+1), tstart: tstart), 'wrong inhibit'}
      }
      #ende = Time.now
      #$log.info "Duration for inhibiting all eqps: #{ende - start}s"
      
      #start = Time.now
      channels.each_with_index {|ch, i| 
        assert ch.ckc_samples.last.assign_ca('ReleaseEquipmentAndRecipeInhibit'), 'error assigning CA'
        mrecipes.each {|mrecipe| assert @@siltest.wait_inhibit(:eqp_recipe, [@@siltest.ptool, mrecipe], '', n: channels.size-(i+1), tstart: tstart), 'wrong inhibit'}
      }
      #ende = Time.now
      #$log.info "Duration for release of all inhibits: #{ende - start}s"
      
      mrecipes.each {|mrecipe| assert @@siltest.wait_inhibit(:eqp_recipe, [@@siltest.ptool, mrecipe], '', n: 0, tstart: tstart), 'wrong inhibit'}
    }
  end

  # verify DFS message after OOC identified by channels, drops all other messages on the DFS queue, returns true on success
  def verify_dfs_message(channels, cas)
    $log.info "verify_dfs_message #{cas}"
    msgs = @@dfs.receive_msgs
    $log.info "received #{msgs.count} DFS messages"
    # find relevant message with all channel ids
    assert msg = msgs.find {|msg| channels.inject(true) {|ok, ch| ok && msg.include?("ChannelId: #{ch.chid}")} }, 'no suitable DFS message'
    # extract ooc information
    ooc_begin = /\n=========================\nOOC Information\n-------------------------\n/
    ooc_end = /\n-------------------------\nEnd of OOC Information\n=========================\n/
    oocs = msg.scan(/#{ooc_begin}(.+?)#{ooc_end}/m).collect {|ss| ss.first}
    assert_equal @@siltest.parameters.size * SIL::DCR::DefaultWaferValues.size * 2, oocs.count, 'wrong number of DFS OOCs'
    ca_begin = /Corrective Actions:\n-------------------------\n/
    ca_end = /\n\nCharts:/
    channels.each {|ch|
      oocs_by_channel = oocs.select {|ooc| ooc =~ /ChannelId: #{ch.chid}/}
      oocs_by_channel = oocs_by_channel.collect {|ooc| ooc.match(/#{ca_begin}(.+?)#{ca_end}/m) {|m| m[1]}}.compact
      oocs_by_channel.each {|ooc| cas.each {|ca| assert ooc.include?(ca)}}
    }
    return true
  end

  def scenario_eqp_recipe_notfound(ca, excomment, mrecipes, eqp=@@siltest.ptool)
    assert_equal 0, @@sv.eqp_cleanup(@@siltest.ptool)
    (mrecipes + [@@precipe]).uniq.each {|r| assert_equal 0, @@sv.inhibit_cancel(:recipe, r)}
    #
    assert channels = @@siltest.setup_channels(cas: [ca, 'ReleaseEquipmentAndRecipeInhibit'])
    # submit process DCR
    assert dcrp = @@siltest.submit_process_dcr(eqp: eqp, mrecipe: @@precipe), 'error submitting DCR'
    # submit meas DCR with OOC values
    assert dcr = @@siltest.submit_meas_dcr(ooc: true), 'error submitting DCR'
    # verify ecomment and inhibit are set
    mrecipes.each {|mrecipe|
      assert @@siltest.wait_inhibit(:eqp_recipe, [eqp, mrecipe], '', n: @@siltest.parameters.size), 'missing inhibit'
      channels.each {|channel|
        assert sample = channel.ckc_samples.last, "no CKC samples in channel #{channel}"
        assert ecomment = sample.ecomment, 'missing ecomment'
        assert ecomment.include?('TOOL+RECIPE_HELD: reason={X-S'), 'wrong ecomment'
        assert ecomment.include?("#{@@siltest.ptool}/"), 'wrong ecomment'
        assert ecomment.include?(mrecipe), 'wrong ecomment'
        assert ecomment.include?(excomment), 'missing ecomment for error'
      }
    }
    assert_empty @@sv.inhibit_list(:eqp_recipe, [eqp, @@precipe]), 'wrong inhibit'
    assert_empty @@sv.lot_futurehold_list(@@siltest.lot, memo: 'specification'), 'wrong future hold'
    ##assert @@siltest.verify_futurehold_cancel(@@lot, '[(Raw above specification)'), 'lot must have no future hold'
    # release inhibit (from automatic action)
    channels.each {|ch| assert ch.ckc_samples.last.assign_ca('ReleaseEquipmentAndRecipeInhibit'), 'error assigning CA'}
    mrecipes.each {|mrecipe| assert @@siltest.wait_inhibit(:eqp_recipe, [eqp, mrecipe], '', n: 0), 'wrong inhibit'}
    ## notification often fails, removing automatic check, sf 2017-09-04
    ##assert @@siltest.wait_notification("Execution of [#{ca}] failed", n: channels.count), 'wrong notification see MSR549577, please check SIL log for DERDACK message too'
  end

end
