=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: U. Koerner, 2012-01-11

History:
  2014-02-05 ssteidte, separated Test_Space07, code cleanup
=end

require_relative 'SILSmoke'


# CAs Lot Hold
class SILSmoke_70 < SILSmoke
  @@test_defaults = {mpd: 'QA-MB-FTDEPM-LIS-CAEX.01'}


  def test00_setup
    $setup_ok = false
    #
    assert lots = @@svtest.new_lots(2), 'error creating lots'
    @@testlots += lots
    @@lot2 = lots[1]
    #
    assert @@siltest.set_defaults(@@test_defaults.merge(lot: lots[0])), 'wrong default test data'
    #
    $setup_ok = true
  end

  def test13_hold_multilot_1ooc
    assert_equal 0, @@sv.lot_cleanup(@@lot2)
    assert channels = @@siltest.setup_channels(cas: 'AutoLotHold')
    assert @@siltest.submit_process_dcr
    assert @@siltest.submit_process_dcr(lot: @@lot2)
    # send meas DCR for lot1 OOC and lot2 NOOC
    assert dcr = @@siltest.submit_meas_dcr(ooc: true, nooc: 'all') {|dcr|
      dcr.add_lot(@@lot2, eqp: @@siltest.mtool, op: @@siltest.mpd)
      dcr.add_parameters(:default, :default)
    }, "error submitting DCR"
    # verify future holds for both lots
    assert @@siltest.wait_futurehold("[(Raw above specification)", n: channels.size), 'missing future hold'
    ## assert_empty @@sv.lot_futurehold_list(@@lot2), "wrong future hold"
    # skip over additional holds
    @@sv.lot_futurehold_list(@@lot2).each {|fh|
      refute fh.memo.include?("[(Raw above specification)"), 'wrong future hold'  
    }
  end

end
