=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: Steffen Steidten, 2011-11-25
=end

require_relative 'SILSmoke'


# Channel behaviour
class SILSmoke_14 < SILSmoke
  @@test_defaults = {ppd: 'QA-MB-CC.01', department: 'QACCALL', technology: 'TEST',
    cas: ['LotHold', 'AutoLotHold', 'CancelLotHold', 'ReleaseLotHold']}


  def test00_setup
    $setup_ok = false
    #
    assert lot = @@svtest.new_lot, 'error creating test lot'
    #
    assert @@siltest.set_defaults(@@test_defaults.merge(lot: lot))
    #
    $setup_ok = true
  end

  # All channels have entries from speclimits.ignorecontrolplan.spacescenariosettings or chartcheck.ignorecontrolplan.spacescenariosettings
  def test11_study
    assert dcr = @@siltest.cleanup_send_dcrs(:default, state: 'Study', mpd: 'QA-MB-CC-M-STD.01')
    tstart = Time.now
    assert @@siltest.verify_aqv_message(dcr, 'success'), 'wrong AQV message'
    refute @@siltest.wait_futurehold('Chart Check issue', tstart: tstart), 'wrong future hold'
  end

  # All channels have entries from chartcheck.ignorecontrolplan.spacescenariosettings (NoChart,NoCC)
  def test12_study_nocc
    assert dcr = @@siltest.cleanup_send_dcrs(:default, state: 'Study', mpd: 'QA-MB-CC-M-STD-NoCC.01')
    tstart = Time.now
    assert @@siltest.verify_aqv_message(dcr, 'success'), 'wrong AQV message'
    refute @@siltest.wait_futurehold('Chart Check issue', tstart: tstart), 'wrong future hold'
  end

  # All channels have entries from speclimits.ignorecontrolplan.spacescenariosettings
  #    or chartcheck.ignorecontrolplan.spacescenariosettings (MANUAL_SPECLIMIT_SPACE, CALCULATION)
  def test13_study_manlim
    assert dcr = @@siltest.cleanup_send_dcrs(:default, state: 'Study', mpd: 'QA-MB-CC-M-STD-MANLIM.01')
    tstart = Time.now
    assert @@siltest.verify_aqv_message(dcr, 'success'), 'wrong AQV message'
    refute @@siltest.wait_futurehold('Chart Check issue', tstart: tstart), 'wrong future hold'
  end

  # All channels have entries from speclimits.ignorecontrolplan.spacescenariosettings/
  #   chartcheck.ignorecontrolplan.spacescenariosettings/missingparametercheck.ignorecontrolplan.spacescenariosettings (IGNORE_ALL_PARAMETER_CHECK)
  def test14_study_ignore
    assert dcr = @@siltest.cleanup_send_dcrs(:default, state: 'Study', mpd: 'QA-MB-CC-M-STD-IGNORE.01')
    tstart = Time.now
    assert @@siltest.verify_aqv_message(dcr, 'success'), 'wrong AQV message'
    refute @@siltest.wait_futurehold('Chart Check issue', tstart: tstart), 'wrong future hold'
  end

  # All channels have entries from speclimits.ignorecontrolplan.spacescenariosettings or chartcheck.ignorecontrolplan.spacescenariosettings
  def test16_offline
    assert dcr = @@siltest.cleanup_send_dcrs(:default, state: 'Offline', mpd: 'QA-MB-CC-M-STD.01')
    tstart = Time.now
    assert @@siltest.verify_aqv_message(dcr, 'success'), 'wrong AQV message'
    refute @@siltest.wait_futurehold('Chart Check issue', tstart: tstart), 'wrong future hold'
  end

  # All channels have entries from chartcheck.ignorecontrolplan.spacescenariosettings (NoChart,NoCC)
  def test17_offline_nocc
    assert dcr = @@siltest.cleanup_send_dcrs(:default, state: 'Offline', mpd: 'QA-MB-CC-M-STD-NoCC.01')
    tstart = Time.now
    assert @@siltest.verify_aqv_message(dcr, 'success'), 'wrong AQV message'
    refute @@siltest.wait_futurehold('Chart Check issue', tstart: tstart), 'wrong future hold'
  end

  # All channels have entries from speclimits.ignorecontrolplan.spacescenariosettings
  #    or chartcheck.ignorecontrolplan.spacescenariosettings (MANUAL_SPECLIMIT_SPACE, CALCULATION)
  def test18_offline_manlim
    assert dcr = @@siltest.cleanup_send_dcrs(:default, state: 'Offline', mpd: 'QA-MB-CC-M-STD-MANLIM.01')
    tstart = Time.now
    assert @@siltest.verify_aqv_message(dcr, 'success'), 'wrong AQV message'
    refute @@siltest.wait_futurehold('Chart Check issue', tstart: tstart), 'wrong future hold'
  end

  # All channels have entries from speclimits.ignorecontrolplan.spacescenariosettings/
  #   chartcheck.ignorecontrolplan.spacescenariosettings/missingparametercheck.ignorecontrolplan.spacescenariosettings (IGNORE_ALL_PARAMETER_CHECK)
  def test19_offline_ignore
    assert dcr = @@siltest.cleanup_send_dcrs(:default, state: 'Offline', mpd: 'QA-MB-CC-M-STD-IGNORE.01')
    tstart = Time.now
    assert @@siltest.verify_aqv_message(dcr, 'success'), 'wrong AQV message'
    refute @@siltest.wait_futurehold('Chart Check issue', tstart: tstart), 'wrong future hold'
  end

  # All channels have entries from speclimits.ignorecontrolplan.spacescenariosettings or chartcheck.ignorecontrolplan.spacescenariosettings
  def test26_inline_offline
    assert dcr = @@siltest.cleanup_send_dcrs(:default, state: 'Offline', technology: '32NM', mpd: 'QA-MB-CC-M-STD.01')
    tstart = Time.now
    assert @@siltest.verify_aqv_message(dcr, 'success'), 'wrong AQV message'
    refute @@siltest.wait_futurehold('Chart Check issue', tstart: tstart), 'wrong future hold'
  end

  # All channels have entries from chartcheck.ignorecontrolplan.spacescenariosettings (NoChart,NoCC)
  def test27_inline_offline_nocc
    assert dcr = @@siltest.cleanup_send_dcrs(:default, state: 'Offline', technology: '32NM', mpd: 'QA-MB-CC-M-STD-NoCC.01')
    tstart = Time.now
    assert @@siltest.verify_aqv_message(dcr, 'success'), 'wrong AQV message'
    refute @@siltest.wait_futurehold('Chart Check issue', tstart: tstart), 'wrong future hold'
  end

  # All channels have entries from speclimits.ignorecontrolplan.spacescenariosettings
  #    or chartcheck.ignorecontrolplan.spacescenariosettings (MANUAL_SPECLIMIT_SPACE, CALCULATION)
  def test28_inline_offline_manlim
    assert dcr = @@siltest.cleanup_send_dcrs(:default, state: 'Offline', technology: '32NM', mpd: 'QA-MB-CC-M-STD-MANLIM.01')
    tstart = Time.now
    assert @@siltest.verify_aqv_message(dcr, 'success'), 'wrong AQV message'
    refute @@siltest.wait_futurehold('Chart Check issue', tstart: tstart), 'wrong future hold'
  end

  # All channels have entries from speclimits.ignorecontrolplan.spacescenariosettings/
  #   chartcheck.ignorecontrolplan.spacescenariosettings/missingparametercheck.ignorecontrolplan.spacescenariosettings (IGNORE_ALL_PARAMETER_CHECK)
  def test29_inline_offline_ignore
    assert dcr = @@siltest.cleanup_send_dcrs(:default, state: 'Offline', technology: '32NM', mpd: 'QA-MB-CC-M-STD-IGNORE.01')
    tstart = Time.now
    assert @@siltest.verify_aqv_message(dcr, 'success'), 'wrong AQV message'
    refute @@siltest.wait_futurehold('Chart Check issue', tstart: tstart), 'wrong future hold'
  end

end
