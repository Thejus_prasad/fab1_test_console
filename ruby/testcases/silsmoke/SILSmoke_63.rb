=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: Conny Stenzel, 2012-06-27

History:
  2017-08-01 cstenzel/sfrieske, refactored all tests
=end

require_relative 'SILSmoke'


# <i>Initially wildcards were not requested as requirement. However, the implementation as it currently is,
# works in the way that an empty cell at the columns technology or route is interpreted as a wildcard. The most specified line wins.
# Empty PDs instead are allowed but result in no spec limits.</i>
# 2015-11-16 Fab 1 with SetupFC: No gaps are allowed in hierarchy on the left - means: fields have to be filled from left to right

# Spec Limits Hierarchy (PCP) - Wildcard tests for PD/Technology/Route   # was: Test_Space061
class SILSmoke_63 < SILSmoke
  @@test_defaults = {mpd: 'QA-SPEC-HIER-METROLOGY-04.01', ppd: :default,
    route: 'SIL-0002.01', product: 'SILPROD2.01', productgroup: '3261C', technology: '32NM',
    parameters: ['QA_PARAM_950', 'QA_PARAM_951']
  }
  
  # @@limit_offset = 200 # 100 for ZTABLESPCLIMITS01, 200 for DCSPEC

  def test00_setup
    $setup_ok = false
    #
    assert lot = @@svtest.new_lot, 'error creating test lot'
    @@testlots << lot
    #
    assert @@siltest.set_defaults(@@test_defaults.merge(lot: lot))
    #
    $setup_ok = true
  end


  # INLINE

  # C05-00001583
  def test11_speclimits_INLINE
    lsl = 100 + 1
    send_dcr_verifylimits([lsl, lsl + 1, lsl + 2], technology: 'xxNM', route: 'SIL-xxx2.01', product: 'SILPRODx.01', productgroup: '32xxC')
  end
  
  # No match, no limits
  def test12_no_speclimits_INLINE
    send_dcr_verifylimits([], technology: 'xxNM', route: 'SIL-xxx2.01', product: 'SILPRODx.01', productgroup: '32xxC', mpd: 'QA-SPEC-HIER-METROLOGY-99.01')
  end
  

  # aux methods

  def send_dcr_verifylimits(specs, params={})
    # submit and verify DCR
    assert dcr = @@siltest.submit_meas_dcr(params.merge(ooc: true, nooc: specs.empty? ? 0 : 'all*2'))
    # verify spec limits
    unless specs.empty?
      specsref = Hash[['LSL', 'TARGET', 'USL'].zip(specs)]
      lds = dcr.lot_context['technology'] == 'TEST' ? @@lds_setup : @@lds
      assert folder = lds.folder(@@autocreated_qa)
      (params[:parameters] || @@siltest.parameters).each {|p|
        assert ch = folder.spc_channel(p)
        assert s = ch.samples.last
        $log.info "Expected specs: #{specs.inspect}"
        $log.info "Sample specs: #{s.specs.inspect}"
        assert verify_hash(specsref, s.specs, nil, refonly: true)
      }
    end
  end

end
