=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: Conny Stenzel, 2012-06-27

History:
  2017-08-01 cstenzel/sfrieske, refactored all tests
=end

require_relative 'SILSmoke'


# DataCheck ECP and PCP
class SILSmoke_49 < SILSmoke
  @@test_defaults = {ppd: 'QA-PARAM-CHECK-TG18.01'}


  def test00_setup
    $setup_ok = false
    #
    assert lot = @@svtest.new_lot, 'error creating test lot'
    @@testlots << lot
    assert @@siltest.set_defaults(@@test_defaults.merge(lot: lot))  # real lot required for test22
    #
    $setup_ok = true
  end

  # Inline

  def test11_missingspeclimit_ecp_pcp_filled
    # parameters.missingspeclimit.upload.inline.enabled=true
    # parameters.missingspeclimit.upload.setup.enabled=true
    # b1) SpecLimit (PCP) und MonitoringSpecLimits (ECP)
    # Param1 is defined in PCP (zTableSPCLimits01 setupFC) with LSL/Target/USL --> chart is filled
    # Param2 is defined in ECP (zTableSPCLimits02 setupFC) with LSL/Target/USL --> chart is filled
    # Template per entry in spec: _Template_QA_PARAMS_900
    parameters = ['QA_PARAM_900_TG18', 'QA_PARAM_900_TG18_ECP']
    specs = {
      parameters[0]=>{'LSL'=>1820.0, 'TARGET'=>1850.0, 'USL'=>1880.0},
      parameters[1]=>{'LSL'=>1800.0, 'TARGET'=>1850.0, 'USL'=>1899.0}
    }
    assert dcr = @@siltest.submit_process_dcr(nocharting: false, parameters: parameters)
    assert @@siltest.verify_channels(dcr, parameters: parameters) {|channel, sample|
      verify_hash(specs[channel.parameter], sample.specs, nil, refonly: true)
    }, 'wrong channel'
  end

  def test12_missingspeclimit_ecp_pcp_empty
    # parameters.missingspeclimit.upload.inline.enabled=true
    # parameters.missingspeclimit.upload.setup.enabled=true
    # b2)
    # Param1 is defined in PCP (zTableSPCLimits01 setupFC) with LSL/Target/USL EMPTY--> chart is filled
    # Param2 is deinfed in ECP (zTableSPCLimits02 setupFC) with LSL/Target/USL EMPTY --> chart is filled
    # Template per entry in spec: _Template_QA_PARAMS_900
    parameters = ['QA_PARAM_901_TG18', 'QA_PARAM_901_TG18_ECP']
    assert dcr = @@siltest.submit_process_dcr(nocharting: false, parameters: parameters)
    assert @@siltest.verify_channels(dcr, parameters: parameters) {|channel, sample|
      ['LSL', 'TARGET', 'USL'].each {|k| assert_nil sample.specs[k]}
    }, 'wrong channel'
  end


  # Setup (exactly the same as test1x with technology 'TEST')

  def test21_missingspeclimit_ecp_pcp_filled
    # parameters.missingspeclimit.upload.inline.enabled=true
    # parameters.missingspeclimit.upload.setup.enabled=true
    # b1) SpecLimit (PCP) und MontiringSpecLimits (ECP)
    # Param1 is defined in PCP (zTableSPCLimits01 setupFC) with LSL/Target/USL --> chart is filled
    # Param2 is defined in ECP (zTableSPCLimits02 setupFC) with LSL/Target/USL --> chart is filled
    # Template per entry in spec: _Template_QA_PARAMS_900
    parameters = ['QA_PARAM_900_TG18', 'QA_PARAM_900_TG18_ECP']
    specs = { # note: LSL is ther than in test11
      parameters[0]=>{'LSL'=>1830.0, 'TARGET'=>1850.0, 'USL'=>1880.0},
      parameters[1]=>{'LSL'=>1810.0, 'TARGET'=>1850.0, 'USL'=>1899.0}
    }
    assert dcr = @@siltest.submit_process_dcr(nocharting: false, parameters: parameters, technology: 'TEST')
    assert @@siltest.verify_channels(dcr, parameters: parameters) {|channel, sample|
      verify_hash(specs[channel.parameter], sample.specs, nil, refonly: true)
    }, 'wrong channel'
  end

  def test22_missingspeclimit_ecp_pcp_empty
    # parameters.missingspeclimit.upload.inline.enabled=true
    # parameters.missingspeclimit.upload.setup.enabled=true
    # b2)
    # Param1 is defined in PCP (zTableSPCLimits01 setupFC) with LSL/Target/USL EMPTY--> chart is filled
    # Param2 is deinfed in ECP (zTableSPCLimits02 setupFC) with LSL/Target/USL EMPTY --> chart is filled
    # Template per entry in spec: _Template_QA_PARAMS_900
    parameters = ['QA_PARAM_901_TG18', 'QA_PARAM_901_TG18_ECP']
    assert dcr = @@siltest.submit_process_dcr(nocharting: false, parameters: parameters, technology: 'TEST')
    assert @@siltest.verify_channels(dcr, parameters: parameters) {|channel, sample|
      ['LSL', 'TARGET', 'USL'].each {|k| assert_nil sample.specs[k]}
    }, 'wrong channel'
  end

end
