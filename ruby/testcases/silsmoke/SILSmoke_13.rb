=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: Steffen Steidten, 2011-11-25

History:
  2021-09-15 sfrieske, finally replaced $svtest by @@svtest
=end

require_relative 'SILSmoke'


# Channel behaviour, was Test_Space00 (PCP)
class SILSmoke_13 < SILSmoke
  @@test_defaults = {
    mpd: 'QA-MB-CC-M-PCP.01', ppd: 'QA-MB-CC.01', department: 'QACCALL', technology: 'TEST',
    parameters: ['QA_PARAM_900_PCP', 'QA_PARAM_901_PCP', 'QA_PARAM_902_PCP', 'QA_PARAM_903_PCP', 'QA_PARAM_904_PCP'],
    cas: ['LotHold', 'AutoLotHold', 'CancelLotHold', 'ReleaseLotHold']
  }


  def test00_setup
    $setup_ok = false
    #
    @@advanced_props = @@siltest.server.read_jobprops
    #
    assert lot = @@svtest.new_lot, "error creating test lot"  # seams to be really required e.g. test31
    @@testlots << lot
    $log.info "using lot #{lot.inspect}"
    #
    assert @@siltest.set_defaults(@@test_defaults.merge(lot: lot))
    #
    $setup_ok = true
  end

  def test12_study_background
    assert dcr = @@siltest.cleanup_send_dcrs(:default, state: 'Study', mpd: 'QA-MB-CC-M-BACKGROUND.01')  # Criticality=B (BACKGROUND)
    tstart = Time.now
    assert @@siltest.verify_aqv_message(dcr, 'success'), 'wrong AQV message'
    refute @@siltest.wait_futurehold('Chart Check issue', tstart: tstart), 'wrong future hold'
    refute @@siltest.wait_notification('Chart Check Failures for Lot', tstart: tstart), 'wrong notification'
  end

  def test17_inline_study_background
    assert dcr = @@siltest.cleanup_send_dcrs(:default, state: 'Study', technology: '32NM', mpd: 'QA-MB-CC-M-BACKGROUND.01')  # Criticality=B (BACKGROUND)
    tstart = Time.now
    assert @@siltest.verify_aqv_message(dcr, 'success'), 'wrong AQV message'
    refute @@siltest.wait_futurehold('Chart Check issue', tstart: tstart), 'wrong future hold'
    refute @@siltest.wait_notification('Chart Check Failures for Lot', tstart: tstart), 'wrong notification'
  end

  def test21_offline
    assert dcr = @@siltest.cleanup_send_dcrs(:default, state: 'Offline')
    tstart = Time.now
    assert @@siltest.verify_aqv_message(dcr, 'success'), 'wrong AQV message'
    refute @@siltest.wait_futurehold('Chart Check issue', tstart: tstart), 'wrong future hold'
    refute @@siltest.wait_notification('Chart Check Failures for Lot', tstart: tstart), 'wrong notification'
  end

  def test22_offline_background
    assert dcr = @@siltest.cleanup_send_dcrs(:default, state: 'Offline', mpd: 'QA-MB-CC-M-BACKGROUND.01')  # Criticality=B (BACKGROUND)
    tstart = Time.now
    assert @@siltest.verify_aqv_message(dcr, 'success'), 'wrong AQV message'
    refute @@siltest.wait_futurehold('Chart Check issue', tstart: tstart), 'wrong future hold'
    refute @@siltest.wait_notification('Chart Check Failures for Lot', tstart: tstart), 'wrong notification'
  end

  def test26_inline_offline
    assert dcr = @@siltest.cleanup_send_dcrs(:default, state: 'Offline', technology: '32NM')
    tstart = Time.now
    assert @@siltest.verify_aqv_message(dcr, 'success'), 'wrong AQV message'
    refute @@siltest.wait_futurehold('Chart Check issue', tstart: tstart), 'wrong future hold'
    refute @@siltest.wait_notification('Chart Check Failures for Lot', tstart: tstart), 'wrong notification'
  end

  def test27_inline_offline_background
    assert dcr = @@siltest.cleanup_send_dcrs(:default, state: 'Offline', technology: '32NM', mpd: 'QA-MB-CC-M-BACKGROUND.01')  # Criticality=B (BACKGROUND)
    tstart = Time.now
    assert @@siltest.verify_aqv_message(dcr, 'success'), 'wrong AQV message'
    refute @@siltest.wait_futurehold('Chart Check issue', tstart: tstart), 'wrong future hold'
    refute @@siltest.wait_notification('Chart Check Failures for Lot', tstart: tstart), 'wrong notification'
  end

  # MSR1321736, since SIL5.3 no property space.noupload.* is available in advanced properties. When set in pcp spec for a certain parameter, it will not be uploaded.
  # See C05-00001649 and advanced.properties for current setup, channel *902* (Calculation) not uploaded too for specific setup
  def test28_offline_noupload_for_firstparameter
    pname = :'parameters.missingspeclimit.upload.setup.enabled'
    assert prop = @@advanced_props[pname], "no such prop: #{pname.inspect}"
    nsamples = 12
    nsamples = 9 if prop == 'false'
    assert dcr = @@siltest.setup_channels(state: 'Offline', mpd: 'QA-MB-CC-M-NOUPLOAD.01', technology: 'TEST', nsamples: nsamples, paramfirstelement: 1, paramlastelement: -1)
  end

  # MSR1321736, since SIL5.3 no property space.noupload.* is available in advanced properties. When set in pcp spec for a certain parameter, it will not be uploaded.
  #
  def test29_inline_offline_noupload_for_firstparameter
    pname = :'parameters.missingspeclimit.upload.inline.enabled'
    assert prop = @@advanced_props[pname], "no such prop: #{pname.inspect}"
    nsamples = prop == 'false' ? 9 : 12
    assert dcr = @@siltest.setup_channels(state: 'Offline', mpd: 'QA-MB-CC-M-NOUPLOAD.01', technology: '32NM', nsamples: nsamples, paramfirstelement: 1, paramlastelement: -1)
  end

end
