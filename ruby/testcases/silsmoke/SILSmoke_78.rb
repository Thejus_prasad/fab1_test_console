=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: U. Koerner, 2012-01-11

History:
  2014-02-05 ssteidte, separated Test_Space07, code cleanup
  2017-08-01 cstenzel/sfrieske, refactored all tests
=end

require_relative 'SILSmoke'


# CAs with responsible PDs
class SILSmoke_78 < SILSmoke
  @@test_defaults = {chambers: (1..5).collect {|i| "PM#{i}"}}
  @@cafailure_lothold_fallback = true

  def test00_setup
    $setup_ok = false
    #
    ['AutoEquipmentInhibit-PD1', 'AutoEquipmentInhibit-PD2', 'AutoEquipmentInhibit-PD3',
      'AutoEquipmentInhibit-PD4', 'AutoEquipmentInhibit-PD5', 'AutoEquipmentInhibit-PDall',
      'AutoChamberInhibit-PD1', 'AutoChamberInhibit-PD2', 'AutoChamberInhibit-PD3',
      'AutoChamberInhibit-PD4', 'AutoChamberInhibit-PD5', 'AutoChamberInhibit-PDall',
      'AutoChamberAndRecipeInhibit-PD1', 'AutoChamberAndRecipeInhibit-PD2', 'AutoChamberAndRecipeInhibit-PD3',
      'AutoChamberAndRecipeInhibit-PD4', 'AutoChamberAndRecipeInhibit-PD5', 'AutoChamberAndRecipeInhibit-PDall',
      'AutoEquipmentAndPDAndProdGrpInhibit-PD1', 'AutoEquipmentAndPDAndProdGrpInhibit-PD2', 'AutoEquipmentAndPDAndProdGrpInhibit-PD3',
      'AutoEquipmentAndPDAndProdGrpInhibit-PD4', 'AutoEquipmentAndPDAndProdGrpInhibit-PD5',
      'AutoEquipmentAndPDAndProdGrpInhibit-PD12',
      'AutoEquipmentAndPDInhibit-PD1', 'AutoEquipmentAndPDInhibit-PD2', 'AutoEquipmentAndPDInhibit-PD3',
      'AutoEquipmentAndPDInhibit-PD4', 'AutoEquipmentAndPDInhibit-PD5',
      'AutoEquipmentAndPDInhibit-PD12',
      'ReleaseEquipmentInhibit', 'ReleaseChamberInhibit', 'ReleaseChamberAndRecipeInhibit',
      'ReleaseMachineRecipeInhibit', 'ReleaseEquipmentAndRecipeInhibit', 'ReleaseEquipmentAndPDAndProdGrpInhibit'
    ].each {|action| assert SIL::SpaceApi.ca(action), "CA #{action.inspect} not defined"}
    #
    assert lot = @@svtest.new_lot, 'error creating test lot'
    @@testlots << lot
    assert @@siltest.set_defaults(@@test_defaults.merge(lot: lot)), 'wrong default test data'
    @@ppds = @@siltest.db.pd_reference(mpd: @@siltest.mpd).take(5)
    assert_equal 5, @@ppds.size, 'wrong PPD setup'
    #
    $setup_ok = true
  end

  def test11_eqp_inhibit_perpd
    assert channels = send_scenario_ppds('Equipment')
    channels.each_with_index {|ch, i|
      eqp = "#{@@siltest.ptool}_#{i}"
      # verify inhibit and comment
      refute_empty @@siltest.wait_inhibit(:eqp, eqp, '', n: 1), 'missing inhibit'
      assert @@siltest.verify_ecomment([ch], "TOOL_HELD: #{eqp}: reason={X-S")
      # release inhibit
      assert ch.ckc_samples.last.assign_ca('ReleaseEquipmentInhibit'), 'error assigning CA'
      assert_empty @@siltest.wait_inhibit(:eqp, eqp, '', n: 0), 'wrong inhibit'
    }
  end

  def test12_eqp_chamber_inhibit_perpd
    assert channels = send_scenario_ppds('Chamber')
    channels.each_with_index {|ch, i|
      eqp = "#{@@siltest.ptool}_#{i}"
      cha = "PM#{i+1}"
      # verify inhibit and comment, chambers PM2, PM4 are excluded in the AutoChamberInhibit-PDx definition
      if i.even?  # applies to PM1, PM3, PM5
        refute_empty @@siltest.wait_inhibit(:eqp_chamber, eqp, '', n: 1), 'missing inhibit'
        assert @@siltest.verify_ecomment([ch], "TOOL+CHAMBER_HELD: #{eqp}/#{cha}: reason={X-S")
      else
        assert_empty @@siltest.wait_inhibit(:eqp_chamber, eqp, '', n: 0), 'wrong inhibit'
      end
      # release inhibit
      if i.even?
        assert ch.ckc_samples.last.assign_ca('ReleaseChamberInhibit'), 'error assigning CA'
        assert_empty @@siltest.wait_inhibit(:eqp_chamber, eqp, '', n: 0), 'wrong inhibit'
      end
      sleep 5
      assert_empty @@siltest.wait_inhibit(:eqp, eqp, '', n: 0), 'wrong inhibit'
    }
  end

  def test13_recipe_inhibit_perpd
    assert channels = send_scenario_ppds('MachineRecipe')
    channels.each_with_index {|ch, i|
      mrecipe = "P-UTC001_#{i}.UTMR000.01"
      # verify inhibit and comment
      refute_empty @@siltest.wait_inhibit(:recipe, mrecipe, '', n: 1), 'missing inhibit'
      assert @@siltest.verify_ecomment([ch], "RECIPE_HELD: #{mrecipe}: reason={X-S")
      # release inhibit
      assert ch.ckc_samples.last.assign_ca('ReleaseMachineRecipeInhibit'), 'error assigning CA'
      assert_empty @@siltest.wait_inhibit(:recipe, mrecipe, '', n: 0), 'wrong inhibit'
    }
  end

  def test14_eqp_recipe_inhibit_perpd
    assert channels = send_scenario_ppds('EquipmentAndRecipe')
    channels.each_with_index {|ch, i|
      eqp = "#{@@siltest.ptool}_#{i}"
      mrecipe = "P-UTC001_#{i}.UTMR000.01"
      # verify inhibit and comment
      refute_empty @@siltest.wait_inhibit(:eqp_recipe, [eqp, mrecipe], '', n: 1), 'missing inhibit'
      assert @@siltest.verify_ecomment([ch], 'TOOL+RECIPE_HELD: reason={X-S')
      assert @@siltest.verify_ecomment([ch], @@siltest.ptool)
      assert @@siltest.verify_ecomment([ch], mrecipe)
      # release inhibit
      assert ch.ckc_samples.last.assign_ca('ReleaseEquipmentAndRecipeInhibit'), 'error assigning CA'
      assert_empty @@siltest.wait_inhibit(:eqp_recipe, [eqp, mrecipe], '', n: 0), 'wrong inhibit'
    }
  end

  def test15_eqp_chamber_recipe_inhibit_perpd
    assert channels = send_scenario_ppds('ChamberAndRecipe')
    channels.each_with_index {|ch, i|
      eqp = "#{@@siltest.ptool}_#{i}"
      cha = "PM#{i+1}"
      mrecipe = "P-UTC001_#{i}.UTMR000.01"
      # verify inhibit and comment
      refute_empty @@siltest.wait_inhibit(:eqp_chamber_recipe, [eqp, mrecipe], '', n: 1), 'missing inhibit'
      assert @@siltest.verify_ecomment([ch], "TOOL+CHAMBER+RECIPE_HELD: #{eqp}/#{cha}/#{mrecipe}: reason={X-S")
      # release inhibit
      assert ch.ckc_samples.last.assign_ca('ReleaseChamberAndRecipeInhibit'), 'error assigning CA'
      assert_empty @@siltest.wait_inhibit(:eqp_chamber_recipe, [eqp, mrecipe], '', n: 0), 'wrong inhibit'
    }
  end

  # TODO: uses only 1 PPD, either overly complicated or broken!
  def test16_no_responsible_run
    5.times {|i|
      assert_equal 0, @@sv.eqp_cleanup("#{@@siltest.ptool}_#{i}")
      assert_equal 0, @@sv.inhibit_cancel(:recipe, "P-UTC001_#{i}.UTMR000.01")
    }
    # create channels and assign CAs
    mpd = 'QA-MB-FTDEPM-LIS-CAEX.01'
    ppds = @@siltest.db.pd_reference(mpd: mpd)
    assert ppds.size < 5, "no suitable PPDs for #{mpd}"
    channels = create_channels(mpd: mpd, ppds: ppds)
    channels.each_with_index {|ch, i|
      actions = ["AutoEquipmentInhibit-PD#{i+1}", 'AutoMachineRecipeInhibit-PDall', 'ReleaseEquipmentInhibit', 'ReleaseMachineRecipeInhibit', 'ReleaseLotHold']
      assert @@siltest.assign_verify_cas(ch, actions)
    }
    # send process DCRs
    assert ppds.each_with_index {|ppd, i|
      @@siltest.submit_process_dcr(ppd: ppd, ptool: "#{@@siltest.ptool}_#{i}",
        lrecipe: "F-P-FTEOS8K15.0#{i}", mrecipe: "P-UTC001_#{i}.UTMR000.01", processing_areas: "PM#{i+1}")
    }, 'error sending DCRs'
    # send meas DCR with OOC values
    assert @@siltest.submit_meas_dcr(mpd: mpd, ooc: true), 'error submitting DCR'
    #
    channels.each_with_index {|ch, i|
      eqp = "#{@@siltest.ptool}_#{i}"
      mrecipe = 'P-UTC001_0.UTMR000.01'
      # verify equipment inhibit and comment
      if i == 0
        refute_empty @@siltest.wait_inhibit(:eqp, eqp, '', n: 1), 'missing inhibit'
        assert @@siltest.verify_ecomment([ch], "TOOL_HELD: #{eqp}: reason={X-S")
      elsif i == 1  # lot hold to be checked only once
        if @@cafailure_lothold_fallback
          assert @@siltest.wait_futurehold('Execution of [AutoEquipmentInhibit-PD'), 'missing future hold'
          assert @@siltest.verify_ecomment(channels[1..4], "LOT_HELD: #{@@siltest.lot}: reason={X-S")
        else # TODO currently multiple derdacks are sent
          ##refute_empty @@siltest.wait_notification('Execution of [AutoEquipmentInhibit-PD', ch.chid.to_s )
        end
      end
      # all channels have machine recipe inhibit
      refute_empty @@siltest.wait_inhibit(:recipe, mrecipe, '', n: @@siltest.parameters.size), 'missing inhibit'
      assert @@siltest.verify_ecomment(channels, "RECIPE_HELD: #{mrecipe}: reason={X-S"), 'wrong ecomment'
    }
    if @@cafailure_lothold_fallback
      assert_equal 0, @@sv.lot_gatepass(@@siltest.lot), 'SiView GatePass error'
      refute_empty @@sv.lot_hold_list(@@siltest.lot, type: 'FutureHold'), 'missing lot hold'
    end
    #
    # release actions
    channels.each_with_index {|ch, i|
      eqp = "#{@@siltest.ptool}_#{i}"
      mrecipe = 'P-UTC001_0.UTMR000.01'
      # ReleaseLotHold
      assert ch.ckc_samples.last.assign_ca('ReleaseLotHold'), 'error assigning CA'
      # release inhibit / lot hold
      if i == 0
        assert ch.ckc_samples.last.assign_ca('ReleaseEquipmentInhibit'), 'error assigning CA'
        assert_empty @@siltest.wait_inhibit(:eqp, eqp, '', n: 0), 'wrong inhibit'
      elsif @@cafailure_lothold_fallback && i == 1
        assert @@siltest.wait_lothold_release('Execution of [AutoEquipmentInhibit-PD'), 'wrong lot hold'
      else
        assert_empty @@siltest.wait_inhibit(:eqp, eqp, '', n: 0), 'wrong inhibit'
      end
      # all channels can release recipe inhibit
      assert ch.ckc_samples[-2].assign_ca('ReleaseMachineRecipeInhibit'), 'error assigning CA'
      # inhibit number can be 0
      assert wait_for(timeout: 120) {@@sv.inhibit_list(:recipe, mrecipe).size == @@siltest.parameters.size - i - 1}, 'wrong or missing inhibit'
    }
    #
    assert @@siltest.wait_lothold_release('[(Raw above specification)'), 'wrong lot hold'
  end

  def test17A_eqp_pd_prdgrp_inhibit_perpd
    5.times {|i| assert_equal 0, @@sv.eqp_cleanup("#{@@siltest.ptool}_#{i}")}
    assert_equal 0, @@sv.eqp_cleanup(@@siltest.mtool)

    assert channels = send_scenario_ppds('EquipmentAndPDAndProdGrp')
    channels.each_with_index {|ch, i|
      eqp = "#{@@siltest.ptool}_#{i}"
      # verify inhibit and comment
      refute_empty @@siltest.wait_inhibit(:productgroup_pd_eqp, ['QAPG1', @@ppds[i], eqp], '', n: 1), 'missing inhibit'
      assert @@siltest.verify_ecomment([ch], "TOOL+PD+PRODGRP_HELD: #{eqp}/#{@@ppds[i]}/QAPG1: reason={X-S")
      # release inhibit
      assert ch.ckc_samples.last.assign_ca('ReleaseEquipmentAndPDAndProdGrpInhibit'), 'error assigning CA'
      assert_empty @@siltest.wait_inhibit(:productgroup_pd_eqp, ['QAPG1', @@ppds[i], eqp], '', n: 0), 'wrong inhibit'
    }

    # can be removed if release CA is working
    2.times {|i| assert_equal 0, @@sv.eqp_cleanup("#{@@siltest.ptool}_#{i}")}

    assert channels = send_scenario_ppds_manual('EquipmentAndPDAndProdGrp')
    channels.each_with_index {|ch, i|
      eqp = "#{@@siltest.ptool}_#{i}"
      # verify inhibit and comment
      refute_empty @@siltest.wait_inhibit(:productgroup_pd_eqp, ['QAPG1', @@ppds[i], eqp], '', n: 1), 'missing inhibit'
      assert @@siltest.verify_ecomment([ch], "TOOL+PD+PRODGRP_HELD: #{eqp}/#{@@ppds[i]}/QAPG1: reason={X-S")
      # release inhibit
      assert ch.ckc_samples.last.assign_ca('ReleaseEquipmentAndPDAndProdGrpInhibit'), 'error assigning CA'
      assert_empty @@siltest.wait_inhibit(:productgroup_pd_eqp, ['QAPG1', @@ppds[i], eqp], '', n: 0), 'wrong inhibit'
    }
  end

  def test21_ca_all
    5.times {|i|
      assert_equal 0, @@sv.eqp_cleanup("#{@@siltest.ptool}_#{i}")
      assert_equal 0, @@sv.inhibit_cancel(:recipe, "P-UTC001_#{i}.UTMR000.01")
      assert_equal 0, @@sv.inhibit_cancel(:eqp_chamber, "#{@@siltest.ptool}_#{i}")
      assert_equal 0, @@sv.inhibit_cancel(:eqp_chamber, @@siltest.ptool)
    }
    # create channels and assign CAs
    channels = create_channels
    actions = ['AutoEquipmentInhibit-PDall', 'AutoChamberInhibit-PDall', 'AutoMachineRecipeInhibit-PDall', 'AutoEquipmentAndRecipeInhibit-PDall',
      'AutoChamberAndRecipeInhibit-PDall', 'ReleaseEquipmentInhibit', 'ReleaseChamberInhibit', 'ReleaseMachineRecipeInhibit',
      'ReleaseEquipmentAndRecipeInhibit', 'ReleaseChamberAndRecipeInhibit']
    assert @@siltest.assign_verify_cas(channels, actions)
    # send process DCRs
    assert @@ppds.each_with_index {|ppd, i|
      @@siltest.submit_process_dcr(ppd: ppd, ptool: "#{@@siltest.ptool}_#{i}",
        lrecipe: "F-P-FTEOS8K15.0#{i}", mrecipe: "P-UTC001_#{i}.UTMR000.01", processing_areas: "PM#{i+1}")
    }, 'error sending DCRs'
    # send meas DCR with OOC values
    assert @@siltest.submit_meas_dcr(ooc: true), 'error submitting DCR'
    # verify inhibits and comments
    5.times {|i|
      eqp = "#{@@siltest.ptool}_#{i}"
      cha = "PM#{i+1}"
      mrecipe = "P-UTC001_#{i}.UTMR000.01"
      # eqp
      refute_empty @@siltest.wait_inhibit(:eqp, eqp, '', n: @@siltest.parameters.size), 'missing inhibit'
      assert @@siltest.verify_ecomment(channels, "TOOL_HELD: #{eqp}: reason={X-S")
      # eqp_chamber
      if i.odd?  # applies to PM2, PM4;	AutoChamberInhibit-PDall excludes PM1, PM3, PM5
        refute_empty @@siltest.wait_inhibit(:eqp_chamber, eqp, '', n: @@siltest.parameters.size), 'missing inhibit'
        assert @@siltest.verify_ecomment(channels, "TOOL+CHAMBER_HELD: #{eqp}/#{cha}: reason={X-S")
      else
        assert_empty @@siltest.wait_inhibit(:eqp_chamber, eqp, '', n: 0), 'wrong inhibit'
      end
      # recipe
      refute_empty @@siltest.wait_inhibit(:recipe, mrecipe, '', n: @@siltest.parameters.size), 'missing inhibit'
      assert @@siltest.verify_ecomment(channels, "RECIPE_HELD: #{mrecipe}: reason={X-S")
      # eqp_recipe
      refute_empty @@siltest.wait_inhibit(:eqp_recipe, [eqp, mrecipe], '', n: @@siltest.parameters.size), 'missing inhibit'
      assert @@siltest.verify_ecomment(channels, 'TOOL+RECIPE_HELD: reason={X-S')
      assert @@siltest.verify_ecomment(channels, eqp)
      assert @@siltest.verify_ecomment(channels, mrecipe)
      # eqp_chamber_recipe
      refute_empty @@siltest.wait_inhibit(:eqp_chamber_recipe, [eqp, mrecipe], '', n: @@siltest.parameters.size), 'missing inhibit'
      assert @@siltest.verify_ecomment(channels, "TOOL+CHAMBER+RECIPE_HELD: #{eqp}/#{cha}/#{mrecipe}: reason={X-S")
    }
    #
    # release a single entity
    n = @@siltest.parameters.count - 1
    channels.each_with_index {|ch, i|
      eqp = "#{@@siltest.ptool}_#{i}"
      cha = "PM#{i+1}"
      mrecipe = "P-UTC001_#{i}.UTMR000.01"
      #
      assert ch.ckc_samples[-1].assign_ca('ReleaseMachineRecipeInhibit', comment: "[RELEASE=#{mrecipe}]"), 'error assigning CA'
      refute_empty @@siltest.wait_inhibit(:recipe, mrecipe, '', n: n), 'missing or wrong inhibit'
      #
      if i.odd?
        assert ch.ckc_samples[-2].assign_ca('ReleaseChamberInhibit', comment: "[RELEASE=#{eqp}/#{cha}]"), 'error assigning CA'
        refute_empty @@siltest.wait_inhibit(:eqp_chamber, eqp, '', n: n), 'missing or wrong inhibit'
      end
      #
      assert ch.ckc_samples[-3].assign_ca('ReleaseEquipmentInhibit', comment: "[RELEASE=#{eqp}]"), 'error assigning CA'
      refute_empty @@siltest.wait_inhibit(:eqp, eqp, '', n: n), 'missing or wrong inhibit'
      #
      assert ch.ckc_samples[-3].assign_ca('ReleaseEquipmentAndRecipeInhibit', comment: "[RELEASE=#{eqp}/(#{mrecipe})]"), 'error assigning CA'
      refute_empty @@siltest.wait_inhibit(:eqp_recipe, [eqp, mrecipe], '', n: n), 'missing or wrong inhibit'
    }
    #
    # add release action with comment [Release=All] for the same samples
    channels.each {|ch|
      # 3 wafers, pick any sample out of these
      assert ch.ckc_samples[-1].assign_ca('ReleaseMachineRecipeInhibit', comment: '[RELEASE=All]'), 'error assigning CA'
      assert ch.ckc_samples[-2].assign_ca('ReleaseEquipmentInhibit', comment: '[RELEASE=All]'), 'error assigning CA'
      assert ch.ckc_samples[-3].assign_ca('ReleaseEquipmentAndRecipeInhibit', comment: '[RELEASE=All]'), 'error assigning CA'
    }
    # need extra assign for chambers
    channels.each {|ch|
      assert ch.ckc_samples[-2].assign_ca('ReleaseChamberInhibit', comment: '[RELEASE=All]'), 'error assigning CA'
    }
    # verify no inhibits
    channels.each_with_index {|ch, i|
      eqp = "#{@@siltest.ptool}_#{i}"
      cha = "PM#{i+1}"
      mrecipe = "P-UTC001_#{i}.UTMR000.01"
      assert_empty @@siltest.wait_inhibit(:eqp, eqp, '', n: 0), 'wrong inhibit'
      assert_empty @@siltest.wait_inhibit(:eqp_chamber, eqp, '', n: 0), 'wrong inhibit'
      assert_empty @@siltest.wait_inhibit(:recipe, mrecipe, '', n: 0), 'wrong inhibit'
      assert_empty @@siltest.wait_inhibit(:eqp_recipe, [eqp, mrecipe], '', n: 0), 'wrong inhibit'
    }
  end


  # aux methods

  # send DCRs with non-ooc values to setup channels
  def create_channels(params={})
    # submit DCRs for all process PDs
    assert (params[:ppds] || @@ppds).each_with_index {|ppd, i|
      @@siltest.submit_process_dcr(ppd: ppd, ptool: "#{@@siltest.ptool}_#{i}",
        lrecipe: "F-P-FTEOS8K15.0#{i}", mrecipe: "F-CVD60X-P.F-P-FTEOS8K15.0#{i}", processing_areas: "PM#{i+1}")
    }, 'error sending DCRs'
    # submit DCR for related MPD
    assert dcr = @@siltest.submit_meas_dcr(mpd: params[:mpd]), 'error submitting DCR'
    # collect channels
    assert folder = @@lds.folder(@@autocreated_qa)
    return @@siltest.parameters.collect {|p| folder.spc_channel(p)}
  end

  def send_scenario_ppds(entity)
    5.times {|i|
      assert_equal 0, @@sv.eqp_cleanup("#{@@siltest.ptool}_#{i}")
      assert_equal 0, @@sv.inhibit_cancel(:recipe, "P-UTC001_#{i}.UTMR000.01")
    }
    channels = create_channels
    channels.each_with_index {|ch, i| assert @@siltest.assign_verify_cas(ch, ["Auto#{entity}Inhibit-PD#{i+1}", "Release#{entity}Inhibit"])}
    # send process DCRs
    assert dcrps = @@ppds.each_with_index.collect {|ppd, i|
      @@siltest.submit_process_dcr(ppd: ppd, ptool: "#{@@siltest.ptool}_#{i}",
        lrecipe: "F-P-FTEOS8K15.0#{i}", mrecipe: "P-UTC001_#{i}.UTMR000.01", processing_areas: "PM#{i+1}")
    }, 'error sending DCRs'
    # send meas DCR with OOC values
    assert @@siltest.submit_meas_dcr(ooc: true), 'error submitting DCR'
    return channels
  end

  def send_scenario_ppds_manual(entity)
    5.times {|i|
      assert_equal 0, @@sv.eqp_cleanup("#{@@siltest.ptool}_#{i}")
      assert_equal 0, @@sv.inhibit_cancel(:recipe, "P-UTC001_#{i}.UTMR000.01")
    }
    channels = create_channels
    channels.each_with_index {|ch, i| assert @@siltest.assign_verify_cas(ch, ["#{entity}Inhibit-PD#{i+1}", "Release#{entity}Inhibit"])}
    # send process DCRs
    assert dcrps = @@ppds.each_with_index.collect {|ppd, i|
      @@siltest.submit_process_dcr(ppd: ppd, ptool: "#{@@siltest.ptool}_#{i}",
        lrecipe: "F-P-FTEOS8K15.0#{i}", mrecipe: "P-UTC001_#{i}.UTMR000.01", processing_areas: "PM#{i+1}")
    }, 'error sending DCRs'
    # send meas DCR with OOC values
    assert @@siltest.submit_meas_dcr(ooc: true), 'error submitting DCR'
    channels.each_with_index {|ch, i|
      assert ch.ckc_samples.last.assign_ca("#{entity}Inhibit-PD#{i+1}"), 'error assigning CA'
    }
    return channels
  end

end
