=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: U. Koerner, 2012-01-11

History:
  2014-02-05 ssteidte, separated Test_Space07, code cleanup
=end

require_relative 'SILSmoke'


# CAs Chamber Inhibit
class SILSmoke_72 < SILSmoke

  def test00_setup
    $setup_ok = false
    #
    assert lot = @@svtest.new_lot, 'error creating lot'
    @@testlots << lot
    #
    assert @@siltest.set_defaults(lot: lot), "no PPD for MPD #{@@siltest.mpd} ?"
    #
    $setup_ok = true
  end

  def test11_chamber_inhibit
    assert_equal 0, @@sv.eqp_cleanup(@@siltest.ptool)
    assert channels = @@siltest.setup_channels(cas: ['AutoLotHold', 'CancelLotHold', 'ChamberInhibit', 'AutoChamberInhibit', 'ReleaseChamberInhibit', 'ReleaseChamberInhibit-noDefault'])
    # send process DCR
    assert @@siltest.submit_process_dcr
    # send meas DCR with OOC values
    assert dcr = @@siltest.submit_meas_dcr(ooc: true, nooc: 'all*2'), 'error submitting DCR'
    assert @@siltest.wait_inhibit(:eqp_chamber, @@siltest.ptool, '', n: @@siltest.chambers.size*channels.size), 'chamber must have an inhibit'
    assert @@siltest.verify_ecomment(channels, "TOOL+CHAMBER_HELD: #{@@siltest.ptool}/#{@@siltest.chambers.first}: reason={X-S")
    # release inhibit with noDefault attribute
    channels.each {|ch| assert !ch.ckc_samples.last.assign_ca('ReleaseChamberInhibit-noDefault'), 'wrongly assigned corrective action'}
    assert @@siltest.wait_inhibit(:eqp_chamber, @@siltest.ptool, '', n: @@siltest.chambers.size*channels.size), 'chamber must still have all inhibits'
    # release inhibit with noDefault attribute - with comment
    channels.each {|ch| assert ch.ckc_samples.last.assign_ca('ReleaseChamberInhibit-noDefault', comment: '[Release=All]'), 'error assigning corrective action'}
    assert @@siltest.wait_inhibit(:eqp_chamber, @@siltest.ptool, '', n: 0), 'chamber must not have an inhibit'
    assert @@siltest.submit_process_dcr
    # send DCR with OOC values
    assert dcr = @@siltest.submit_meas_dcr(ooc: true, nooc: 'all*2'), 'error submitting DCR'
    assert @@siltest.wait_inhibit(:eqp_chamber, @@siltest.ptool, '', n: @@siltest.chambers.size*channels.size), 'chamber must have an inhibit'
    assert @@siltest.verify_ecomment(channels, "TOOL+CHAMBER_HELD: #{@@siltest.ptool}/#{@@siltest.chambers.first}: reason={X-S")
    # release inhibit
    channels.each {|ch| assert ch.ckc_samples.last.assign_ca('ReleaseChamberInhibit'), 'error assigning corrective action'}
    assert @@siltest.wait_inhibit(:eqp_chamber, @@siltest.ptool, '', n: 0), 'chamber must not have an inhibit'
    # set inhibit (manual)
    assert channels[0].ckc_samples.last.assign_ca('ChamberInhibit'), 'error assigning corrective action'
    assert @@siltest.wait_inhibit(:eqp_chamber, @@siltest.ptool, '', n: @@siltest.chambers.size), 'chamber must have an inhibit'
    assert @@siltest.verify_ecomment(channels, "TOOL+CHAMBER_HELD: #{@@siltest.ptool}/#{@@siltest.chambers.first}: reason={X-S")
    # release inhibit
    assert channels[0].ckc_samples.last.assign_ca('ReleaseChamberInhibit'), 'error assigning corrective action'
    assert @@siltest.wait_inhibit(:eqp_chamber, @@siltest.ptool, '', n: 0), 'chamber must not have an inhibit'
    # set inhibit again (manual)
    assert channels[0].ckc_samples.last.assign_ca('ChamberInhibit'), 'error assigning corrective action'
    assert @@siltest.wait_inhibit(:eqp_chamber, @@siltest.ptool, '', n: @@siltest.chambers.size), 'chamber must have an inhibit'
    assert @@siltest.verify_ecomment(channels, "TOOL+CHAMBER_HELD: #{@@siltest.ptool}/#{@@siltest.chambers.first}: reason={X-S")
    # release inhibit with noDefault attribute
    assert !channels[0].ckc_samples.last.assign_ca('ReleaseChamberInhibit-noDefault'), 'wrongly assigned corrective action'
    assert @@siltest.wait_inhibit(:eqp_chamber, @@siltest.ptool, '', n: @@siltest.chambers.size), 'chamber must still have all inhibits'
  end

  def test12_one_chamber_not_existing
    assert_equal 0, @@sv.eqp_cleanup(@@siltest.ptool)
    chambers = @@siltest.chambers + ['CHX']
    assert channels = @@siltest.setup_channels(cas: ['AutoChamberInhibit'], chambers: chambers)
    # send process DCR
    assert @@siltest.submit_process_dcr(chambers: chambers)
    # send meas DCR with OOC values
    assert dcr = @@siltest.submit_meas_dcr(ooc: true, nooc: 'all*2'), 'error submitting DCR'
    assert @@siltest.wait_inhibit(:eqp, @@siltest.ptool, '', n: 1), 'equipment must have an inhibit'
    assert @@siltest.verify_ecomment(channels, "TOOL+CHAMBER_HELD: #{@@siltest.ptool}/#{@@siltest.chambers.first}: reason={X-S")
		assert @@siltest.verify_ecomment(channels, 'Execution of [AutoChamberInhibit] failed: reason={Error in SiView call caused by')
    assert @@siltest.verify_ecomment(channels, "TOOL_HELD: #{@@siltest.ptool}: reason={X-S")
  end

end
