=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: Conny Stenzel, 2012-06-27

History:
  2017-08-01 cstenzel/sfrieske, refactored all tests
=end

require_relative 'SILSmoke'


# Remeasurement  was: Test_Space14 part 1
class SILSmoke_44 < SILSmoke
  @@test_defaults = {mpd: 'QA-MB-FTDEPM.01', mtool: 'QAMeas'}
  @@flagging_delay = 20
  @@inline_remeasurement_autoflagging_enabled = true
  @@setup_remeasurement_autoflagging_enabled = true


  def test00_setup
    $setup_ok = false
    #
    assert @@siltest.server.verify_property(:'inline.remeasurement.autoflagging.enabled', @@inline_remeasurement_autoflagging_enabled), 'wrong property'
    assert @@siltest.server.verify_property(:'setup.remeasurement.autoflagging.enabled', @@setup_remeasurement_autoflagging_enabled), 'wrong property'
    #
    assert @@siltest.set_defaults(@@test_defaults)
    #
    $setup_ok = true
  end

  def test11_remeasure
    assert @@siltest.submit_process_dcr, 'error submitting DCR'
    3.times {|i| remeasure_scenario(parameters: @@siltest.parameters.collect {|p| p + '_REMEASURE'})}
  end

  
  # aux methods

  def remeasure_scenario(params={})
    technology = params[:technology] || '32NM'
    lds = technology == 'TEST' ? @@lds_setup : @@lds
    parameters = params[:parameters] || @@siltest.parameters
    old_nsamples = {}
    assert folder = lds.folder(@@autocreated_qa)
    parameters.each {|p|
      ch = folder.spc_channel(p)
      old_nsamples[p] = ch ? ch.samples.size : 0
      ch.samples.each {|s| s.flag} if params[:internalflag] && ch
    }
    # submit meas DCR
    assert dcr = @@siltest.submit_meas_dcr(mtool: params[:mtool], mpd: params[:mpd], lot: params[:lot], technology: technology,
      parameters: params[:parameters], readings: params[:readings], nsamples: params[:nsamples]), 'error submitting DCR'
    # wait for flagging and check flags
    $log.info "waiting #{@@flagging_delay} s for flagging"; sleep @@flagging_delay
    assert folder = lds.folder(@@autocreated_qa)
    parameters.each {|p|
      assert ch = folder.spc_channel(p), 'no channel'
      samples = ch.samples
      dcrreadingssize = parameters.first.include?('-LOT') ? 1 : dcr.readings(p).size
      assert_equal old_nsamples[p] + dcrreadingssize, samples.size, "wrong number of samples in #{ch.inspect}"
      if technology != 'TEST' && @@inline_remeasurement_autoflagging_enabled || technology == 'TEST' &&  @@setup_remeasurement_autoflagging_enabled
        assert verify_channel_remeasured(ch), "samples not autoflagged for remeasurement - flag delay = #{@@flagging_delay}"
      else
        if old_nsamples[p] > 0
          samples[0..(old_nsamples[p]-1)].each {|s|
            assert !s.iflag || !s.icomment || !s.icomment.include?('Remeasurement'), 'wrong autoflagging for remeasurement'
          }
        end
      end
    }
  end

  # def remeasure_scenario_refpd(params={})
  #   technology = params[:technology] || '32NM'
  #   lds = technology == 'TEST' ? @@lds_setup : @@lds
  #   mdcrcount = 5
  #   mdcrcount.times {|i|
  #     $log.info "\n-- loop ##{i+1}/#{mdcrcount}"
  #     # submit process DCR
  #     assert @@siltest.submit_process_dcr(technology: technology) if [0, 3].include?(i)
  #     # count old samples
  #     old_nsamples = {}
  #     assert folder = lds.folder(@@autocreated_qa)
  #     @@siltest.parameters.each {|p|
  #       ch = folder.spc_channel(p)
  #       old_nsamples[p] = ch ? ch.samples.size : 0
  #     }
  #     # submit meas DCR
  #     assert dcr = @@siltest.submit_meas_dcr(technology: technology), 'error submitting DCR'
  #     # wait for flagging and check flags
  #     $log.info "waiting #{@@flagging_delay} s for flagging"; sleep @@flagging_delay
  #     assert folder = lds.folder(@@autocreated_qa)
  #     @@siltest.parameters.each {|p|
  #       assert ch = folder.spc_channel(p), 'no channel'
  #       samples = ch.samples
  #       assert_equal (i + 1) * dcr.readings(p).size, samples.size, "wrong number of samples in #{ch.inspect}"
  #       if @@setup_remeasurement_autoflagging_enabled
  #         assert verify_channel_remeasured(ch), "samples not autoflagged for remeasurement - flag delay = #{@@flagging_delay}"
  #       else
  #         if old_nsamples[p] > 0
  #           samples[0..(old_nsamples[p]-1)].each {|s|
  #             assert !s.iflag || !s.icomment || !s.icomment.include?('Remeasurement'), 'wrong autoflagging for remeasurement'
  #           }
  #         end
  #       end
  #     }
  #   }
  # end

  # def remeasure_scenario_nonwip(params={})
  #   technology = params[:technology] || '32NM'
  #   lds = @@lds_setup
  #   parameters = params[:parameters] || @@siltest.parameters
  #   assert folder = lds.folder(@@autocreated_nonwip)
  #   # submit nonwip DCR
  #   eqp = 'QATest1'
  #   dcr = SIL::DCR.new(eqp: eqp, event: 'non_wip_dcr')
  #   dcr.context.delete(:JobSetup) if !params[:mixed]
  #   dcr.add_parameters(:default, {eqp=>22.0}, readingtype: 'Equipment', valuetype: 'double')
  #   assert @@siltest.send_dcr_verify(dcr, nsamples: @@siltest.parameters.size), 'error sending DCR'
    
  #   # wait for flagging and check flags
  #   $log.info "waiting #{@@flagging_delay} s for flagging"; sleep @@flagging_delay
  #   assert folder = lds.folder(@@autocreated_nonwip)
  #   parameters.each {|p|
  #     assert ch = folder.spc_channel(p), 'no channel'
  #     samples = ch.samples
  #     samples.each {|s| 
  #       assert !s.iflag || !s.icomment || !s.icomment.include?('Remeasurement'), 'wrong autoflagging for remeasurement'
  #       assert !s.iflag || !s.icomment || !s.icomment.include?('Rework'), 'wrong autoflagging for rework'
  #     }
  #   }
  # end

  def verify_channel_remeasured(ch)
    rhash = {}
    ch.samples.each {|s|
      ekeys = s.ekeys
      rkey = [s.time.to_i, ekeys['PTool'], ekeys['POperationID'], ekeys['Lot'], ekeys['Wafer']].join(':')
      rhash[rkey] ||= []
      rhash[rkey] << s
    }
    ret = true
    rhash.each_pair {|rkey, samples|
      samples.sort! {|a, b| a.dkeys['MTime'] <=> b.dkeys['MTime']}.each_with_index {|s, i|
        if i < samples.size - 1
          ret &= s.iflag && s.icomment && s.icomment.include?('Remeasurement')
        else  # last sample
          ret &= !s.iflag || !s.icomment || !s.icomment.include?('Remeasurement')
        end
      }
    }
    return ret
  end

end
