=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: Conny Stenzel, 2012-06-27

History:
  2017-08-01 cstenzel/sfrieske, refactored all tests
=end

require_relative 'SILSmoke'


# Spec Limits Hierarchy (ECP) - Wildcard tests for PD/Technology/Route
#
# An empty cell at the columns technology or route is interpreted as a wildcard. The most specified line wins.
# Empty PDs instead are allowed but result in no spec limits.
class SILSmoke_64 < SILSmoke
  @@test_defaults = {department: 'QAGKGPECP'}
  @@control_limit_coef_1 = 0.4   # read from config


  def test00_setup
    $setup_ok = false
    #
    assert lot = @@svtest.new_lot, 'error creating test lot'
    @@testlots << lot
    #
    assert @@siltest.set_defaults(@@test_defaults.merge(lot: lot))
    #
    $setup_ok = true
  end

  # INLINE

  def test11_speclimits_INLINE
    params = ['QA_PARAM_901_ECP', 'QA_PARAM_902_ECP']
    send_dcr_verifylimits(params, [400, 500, 600], '45NM', 'QA-SPEC-HIER-METROLOGY-04.01')
  end

  def test12_no_speclimits_INLINE
    params = ['QA_PARAM_901_ECP', 'QA_PARAM_902_ECP']
    send_dcr_verifylimits(params, [], 'xxNM', 'QA-SPEC-HIER-METROLOGY-04.01')
  end


  # aux methods

  def send_dcr_verifylimits(parameters, specs, technology, op, params={})
    readings = Hash[SIL::DCR::DefaultWaferValues.each_with_index.collect {|v, i| ["WAFER#{i}", v + 650]}]
    #
    checklimits = params.has_key?(:checklimits) ? params[:checklimits] : !specs.empty?
    if checklimits
      # calculate spec limits
      specs = [0, 0, 0] if specs.empty?
      specsref = Hash[['LSL', 'TARGET', 'USL'].zip(specs)]
      # .. and nooc
      mUCL = specs[1] + @@control_limit_coef_1 * (specs[2] - specs[1])
      mLCL = specs[1] - @@control_limit_coef_1 * (specs[1] - specs[0])
      oos = readings.values.inject(0) {|s, v| v < specsref['LSL'] || v > specsref['USL'] ? s + 1 : s}
      ooc = readings.values.inject(0) {|s, v| v < mLCL || v > mUCL ? s + 1 : s}
      nooc = (oos + ooc) * parameters.size
    else
      nooc = 0
    end
    # submit and verify DCR
    assert @@siltest.submit_meas_dcr(mpd: op, product: 'SILPROD2.01', productgroup: params[:pg] || '3261C',
      route: params[:route] || 'SIL-0002.01', technology: technology, parameters: parameters, readings: readings, nooc: nooc)
    #
    if checklimits
      lds = technology == 'TEST' ? @@lds_setup : @@lds
      assert folder = lds.folder('AutoCreated_QAGKGPECP')
      parameters.each {|p|
        assert ch = folder.spc_channel(p)
        assert s = ch.samples.last
        assert verify_hash(specsref, s.specs, nil, refonly: true)
      }
    end
  end

end
