=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: Conny Stenzel, 2012-06-27

History:
  2017-08-01 cstenzel/sfrieske, refactored all tests
=end

require_relative 'SILSmoke'


# DataCheck
class SILSmoke_48 < SILSmoke
  @@test_defaults = {ppd: 'QA-PARAM-CHECK.01'}
  @@pd_ecp = 'QA-PARAM-CHECK-ECP.01'
# advanced props, TODO: clean up!
  @@datacheck_parameter_inline_enabled = true
  @@datacheck_parameter_setup_enabled = true
  @@missingparametercheck_inline_active = true
  @@missingparametercheck_inline_trace = true
  @@missingparametercheck_inline_notification = false
  @@missingparametercheck_setup_active = true
  @@missingparametercheck_setup_trace = true
  @@missingparametercheck_setup_notification = false
  @@missingparametercheck_monitoringspeclimits_wildcards_enabled = false
  @@autoqual_missingparametercheck_monitoringspeclimits_wildcards_enabled = false
  # not in advanced props
  @@autoqual_reporting_inline_enabled = false
  @@autoqual_reporting_setup_enabled = true

  @@missingparam_errormsg = 'Error code : MISSING_DCR_PARAMETERS'   # sent to AQV and notificatioon
  @@missingparam_memo = 'Missing Parameter: '                  # future hold claim memo
  @@no_param_uploaded = 'No parameter is uploaded to SPACE - a missing parameter check is not possible'


  def test00_setup
    $setup_ok = false
    #
    assert @@siltest.server.verify_property(:'datacheck.parameter.inline.enabled', @@datacheck_parameter_inline_enabled), 'wrong property'
    assert @@siltest.server.verify_property(:'datacheck.parameter.setup.enabled', @@datacheck_parameter_setup_enabled), 'wrong property'
    assert @@siltest.server.verify_property(:'missingparametercheck.inline.active', @@missingparametercheck_inline_active), 'wrong property'
    assert @@siltest.server.verify_property(:'missingparametercheck.inline.trace', @@missingparametercheck_inline_trace), 'wrong property'
    assert @@siltest.server.verify_property(:'missingparametercheck.inline.notification', @@missingparametercheck_inline_notification), 'wrong property'
    assert @@siltest.server.verify_property(:'missingparametercheck.setup.active', @@missingparametercheck_setup_active), 'wrong property'
    assert @@siltest.server.verify_property(:'missingparametercheck.setup.trace', @@missingparametercheck_setup_trace), 'wrong property'
    assert @@siltest.server.verify_property(:'missingparametercheck.setup.notification', @@missingparametercheck_setup_notification), 'wrong property'
    assert @@siltest.server.verify_property(:'missingparametercheck.monitoringspeclimits.wildcards.enabled', @@missingparametercheck_monitoringspeclimits_wildcards_enabled), 'wrong property'
    assert @@siltest.server.verify_property(:'autoqual.missingparametercheck.monitoringspeclimits.wildcards.enabled', @@autoqual_missingparametercheck_monitoringspeclimits_wildcards_enabled), 'wrong property'
    #
    # assert @@lots = $svtest.new_lots(2), 'error creating test lots'
    # @@testlots += @@lots
    # #
    # assert @@siltest.set_defaults(@@test_defaults.merge(lot: @@lots[0]))
    assert @@lot = @@svtest.new_lot, 'error creating test lot'
    @@testlots << @@lot
    assert @@siltest.set_defaults(@@test_defaults.merge(lot: @@lot))
    #
    ##[@@lds, @@lds_setup].each {|lds| assert lds.folder(@@templates_folder).spc_channel(name: @@params_template), 'missing template'}
    #
    $setup_ok = true
  end

  # LDS Setup

  def test01_simple
    assert dcr = @@siltest.submit_process_dcr(nocharting: false, technology: 'TEST')
    assert @@siltest.verify_channels(dcr) {|channel, sample|
      ['LSL', 'TARGET', 'USL'].each {|k| refute_nil sample.specs[k]}
      ['MEAN_VALUE_UCL', 'MEAN_VALUE_CENTER', 'MEAN_VALUE_LCL'].each {|k| refute_nil sample.limits[k]}
    }
  end

end
