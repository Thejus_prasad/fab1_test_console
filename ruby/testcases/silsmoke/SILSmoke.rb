=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: Steffen Steidten, 2011-11-25

History:
  2016-02-18 sfrieske, minor cleanup
  2016-12-07 sfrieske, require 'sfc' instead of 'setupfc', thus using rqrsp_http instead if the deprecated rqrsp
  2017-08-01 cstenzel/sfrieske, refactored all tests
  2019-01-23 sfrieske, minor cleanup
  2020-09-07 sfrieske, minor rework for extended smoke tests
  2021-04-08 aschmid3, fixed channel deletion
  2021-08-27 sfrieske, split verify_hash off of misc.rb

Notes:
  ITDC server is f36asd24
=end

require 'SiViewTestCase'
require 'sil/siltest'
require 'util/verifyhash'
require 'util/waitfor'


# Common setup SIL tests
class SILSmoke < SiViewTestCase
  @@sv_defaults = {route: 'SIL-0001.01', product: 'SILPROD.01', carriers: 'SIL%'}
  @@siltest_params = {
    sil_timeout: 900, ca_timeout: 240, notification_timeout: 120,
    parameter_template: '_Template_QA_PARAMS_900',
    mpd_lit: 'LIT-MGT-NMETDEPDR.LIT',
    mtool: 'UTFSIL01',
    ptool: 'UTCSIL01'
  }
  @@templates_folder = '_TEMPLATES'
  @@default_template = '_DEFAULT_TEMPLATE'
  @@default_template_qa = 'DEFAULT_QA_TEMPLATE'
  @@autocreated_qa = 'AutoCreated_QA'
  @@autocreated_nonwip = 'AutoCreated_nonwip'
  @@autocreated_nodep = 'AutoCreated_NODEP'

  @@testlots = []


  def self.startup
    super  # creates @@svtest for lot creation
    @@siltest = SIL::Test.new($env, @@siltest_params.merge(sv: @@sv))
    @@lds = @@siltest.lds_inline
    @@lds_setup = @@siltest.lds_setup
    # often used variables
    # removed: @@params_template = @@siltest.parameter_template
  end

  def self.shutdown
    ##SIL::SpaceApi.session.terminate  # leave open for devel, will be closed automatically at next run
    @@testlots.each {|lot| @@sv.delete_lot_family(lot)}
  end


  def setup
    # cleanup channels and folders
    [@@lds, @@lds_setup].each {|lds|
      # delete folders AutoCreated_QA, yy_QAxx and QA, be careful to not delete AutoCreated_QA<specials>
      fnames = ['*_QA*', 'QA', 'AutoCreated_null']
      fnames += [@@autocreated_nodep, @@autocreated_nonwip] if $env != 'let'  # too many data from prod data import
      fnames.each {|f| lds.folders(f).each {|folder| folder.delete_channels(silent: true, exclude: '2S@QA')}}
      lds.spc_channels(name: 'QA_PARAM_90*').each {|ch| ch.delete}
      lds.spc_channels(name: 'QA_PARAM_90*', deleted: true).each {|ch| ch.delete}
    }
    # cleanup route, lot
    assert_equal 0, @@sv.inhibit_cancel(:route, @@svtest.route, silent: true)
    if @@sv.lot_exists?(@@siltest.lot)
      assert_equal 0, @@sv.lot_cleanup(@@siltest.lot)
    end
  end

end
