=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: Steffen Steidten, 2011-11-25

History:
  2017-08-01 cstenzel/sfrieske, refactored all tests
=end

require_relative 'SILSmoke'


# Test Mulitilot DCRs with multiple parameter instances are handled correctly, MSR 1017508
class SILSmoke_05 < SILSmoke
  @@test_defaults = {mpd: 'MGT-NMETDEPDR.QA', ppd: :default}


  def test11_multilot
    assert @@siltest.set_defaults(@@test_defaults), 'wrong setup'   # ??
    assert dcr = @@siltest.submit_meas_dcr(lots: [:default, 'ANOTHERLOT.00']), 'error submitting DCR'
    assert @@siltest.verify_channels(dcr), 'wrong channel data'
  end

end
