=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: U. Koerner, 2012-01-11

History:
  2014-02-05 ssteidte, separated Test_Space07, code cleanup
  2017-08-01 cstenzel/sfrieske, refactored all tests
  2021-08-31 sfrieske, requiring 'time' for #iso8601
=end

require 'time'
require_relative 'SILSmoke'


# SIL misc CAs
class SILSmoke_73 < SILSmoke
  @@test_defaults = {mpd: 'QA-MB-FTDEPM-LIS-CAEX.01'}
  @@cafailure_lothold_fallback = nil
  @@dfs_ondemand_worklist = true

  @@reticle = 'QAReticle0001'
  @@mrecipe = nil  # assigned in test00_setup


  def self.shutdown
    super
    @@sv.inhibit_cancel(:route, @@siltest.defaults[:route])
    @@sv.inhibit_cancel(:recipe, @@mrecipe)
    @@sv.inhibit_cancel(:reticle, @@reticle)
  end


  def test00_setup
    $setup_ok = false
    #
    assert @@dfs = MQ::JMS.new("dfs_space.#{$env}"), 'no connection to DFS queue'
    ['LotHold', 'AutoLotHold', 'ReleaseLotHold', 'AutoEquipmentInhibit', 'AutoChamberInhibit',
      'ReticleInhibit', 'AutoReticleInhibit-RPD1', 'ReleaseReticleInhibit',
      'RouteInhibit', 'AutoRouteInhibit', 'ReleaseRouteInhibit',
      'MachineRecipeInhibit', 'AutoMachineRecipeInhibit', 'ReleaseMachineRecipeInhibit', 'ReleaseMachineRecipeInhibit-noDefault',
      'AutoSetWaferScriptParameter-OOC', 'AutoSetLotScriptParameter-OOC', 'AutoSetLotScriptParameter-OOCInteger', 'SetLotScriptParameter-OOC',
      'CACollection-ReleaseAll', 'AutoUnknown', 'Unknown',
      'SendPCLWfrProp-OOS', 'SendPCLWfrProp-OOC', 'SendPCLWfrProp-CH-OOSValidation', 'SendPCLLotProp-UpScan', 'SendPCLLotProp-MaxWfr5',
      'AutoDFSWorklist'
    ].each {|action| assert SIL::SpaceApi.ca(action), "CA #{action.inspect} not defined"}
    #
    assert lot = @@svtest.new_lot, 'error creating test lot'
    @@testlots << lot
    assert @@siltest.set_defaults(@@test_defaults.merge(lot: lot, route: @@svtest.route)), 'wrong default test data'
    #
    assert @@mrecipe = @@sv.machine_recipes.sample
    @@sv.inhibit_cancel(:recipe, [@@mrecipe])
    #
    $setup_ok = true
  end

  def test11_mrecipe_inhibit
    assert dcr = @@siltest.cleanup_send_dcrs(:ooc, mrecipe: @@mrecipe, cas:
      ['MachineRecipeInhibit', 'AutoMachineRecipeInhibit', 'ReleaseMachineRecipeInhibit', 'ReleaseMachineRecipeInhibit-noDefault'])
    # verify comment and inhibit are set
    assert @@siltest.wait_inhibit(:recipe, @@mrecipe, '', n: @@siltest.parameters.size), 'missing inhibit'
    assert folder = @@lds.folder(@@autocreated_qa)
    @@siltest.parameters.each {|p|
      assert ch = folder.spc_channel(p), 'missing channel'
      assert @@siltest.verify_ecomment(ch, "RECIPE_HELD: #{@@mrecipe}: reason={X-S")
      # try to release inhibit noDefault from automatic action
      s = ch.ckc_samples.last
      assert !s.assign_ca('ReleaseMachineRecipeInhibit-noDefault'), 'wrong CA'
    }
    assert @@siltest.wait_inhibit(:recipe, @@mrecipe, '', n: @@siltest.parameters.size), 'missing inhibit'
    #
    # release inhibit with noDefault attribute - with comment
    @@siltest.parameters.each {|p|
      assert ch = folder.spc_channel(p), 'missing channel'
      s = ch.ckc_samples.last
      assert s.assign_ca('ReleaseMachineRecipeInhibit-noDefault', comment: '[Release=All]'), 'error assigning CA'
    }
    assert wait_for {@@sv.inhibit_list(:recipe, @@mrecipe).empty?}, 'wrong inhibit'
    #
    # send DCR again
    assert @@siltest.send_dcr_verify(dcr), 'error sending DCR'
    # verify comment and inhibit are set
    assert @@siltest.wait_inhibit(:recipe, @@mrecipe, '', n: @@siltest.parameters.size), 'missing inhibit'
    assert folder = @@lds.folder(@@autocreated_qa)
    @@siltest.parameters.each {|p|
      assert ch = folder.spc_channel(p), 'missing channel'
      # release inhibit
      s = ch.ckc_samples.last
      assert s.assign_ca('ReleaseMachineRecipeInhibit'), 'error assigning CA'
    }
    assert wait_for {@@sv.inhibit_list(:recipe, @@mrecipe).empty?}, 'wrong inhibit'
    #
    # set an inhibit (manual)
    assert ch = folder.spc_channel(@@siltest.parameters.first), 'missing channel'
    assert s = ch.ckc_samples.last
    assert s.assign_ca('MachineRecipeInhibit', comment: "RECIPE_HELD: #{@@mrecipe}: reason={X-S5} - #{Time.now.utc.iso8601}"), 'error assigning CA'
    assert @@siltest.wait_inhibit(:recipe, @@mrecipe, ''), 'missing inhibit'
    assert @@siltest.verify_ecomment(ch, "RECIPE_HELD: #{@@mrecipe}: reason={X-S")
    # release inhibit
    assert s.assign_ca('ReleaseMachineRecipeInhibit'), 'error assigning CA'
    assert wait_for {@@sv.inhibit_list(:recipe, @@mrecipe).empty?}, 'wrong inhibit'
  end

  def test12_reticle_inhibit
    # note: department must be LIT (sf 2017-08-29)
    assert channels = @@siltest.setup_channels(department: 'LIT', cas: ['ReticleInhibit', 'AutoReticleInhibit', 'ReleaseReticleInhibit'])
    # send DCR with OOC values
    assert dcr = @@siltest.submit_meas_dcr(department: 'LIT', mpd: @@siltest.mpd_lit, readings: :ooc) {|dcr|
      dcr.add_parameter('L-STP-RETICLEID1', {@@siltest.lot=>@@reticle}, nocharting: true, readingtype: 'Lot')
      dcr.parameters[1][:Reading][0][:Reticle] = @@reticle
    }
    #
    # verify comment and inhibit is set
    refute_empty @@siltest.wait_inhibit(:reticle, @@reticle, '', n: channels.size), 'missing reticle inhibit'
    assert @@siltest.verify_ecomment(channels, "RETICLE_HELD:#{@@reticle}: reason={X-S")
    #
    # release inhibits
    channels.each {|ch| assert ch.ckc_samples.last.assign_ca('ReleaseReticleInhibit'), 'error assigning CA'}
    assert wait_for {@@sv.inhibit_list(:reticle, @@reticle).empty?}, 'wrong inhibit'
    #
    # set inhibit (manual)
    assert s = channels.first.ckc_samples.last
    assert s.assign_ca('ReticleInhibit'), 'error assigning CA'
    refute_empty @@siltest.wait_inhibit(:reticle, @@reticle, ''), 'missing inhibit'
    assert @@siltest.verify_ecomment(channels, "RETICLE_HELD:#{@@reticle}: reason={X-S")
    #
    # release inhibit
    assert s.assign_ca('ReleaseReticleInhibit'), 'error assigning CA'
    assert wait_for {@@sv.inhibit_list(:reticle, @@reticle).empty?}, 'wrong inhibit'
  end

  def test31_lot_scriptparam_ooc
    assert_equal 0, @@sv.user_parameter_delete('Lot', @@siltest.lot, 'OOC', check: true)
    assert_equal 0, @@sv.user_parameter_delete('Lot', @@siltest.lot, 'OOCInteger', check: true)
    #
    assert dcr = @@siltest.cleanup_send_dcrs(:ooc, cas: ['AutoSetLotScriptParameter-OOC'])
    #
    # verify lot script parameter is set
    assert_equal 'Yes', @@sv.user_parameter('Lot', @@siltest.lot, name: 'OOC').value, 'lot script parameter not set'
    # verify external comment
    assert folder = @@lds.folder(@@autocreated_qa)
    @@siltest.parameters.each {|p|
      assert ch = folder.spc_channel(p), 'missing channel'
      assert ch.ckc_samples.last.ecomment.include?('SET_LOT_SCRIPT_PARAMETER: reason={OOC/Yes}'), 'wrong ecomment'
    }
    #
    assert ch = folder.spc_channel(@@siltest.parameters.first), 'missing channel'
    assert ch.ckc_samples.last.assign_ca('SetLotScriptParameter-OOC'), 'error assigning CA'
    sleep 10
    assert_equal 'No', @@sv.user_parameter('Lot', @@siltest.lot, name: 'OOC').value, "lot script parameter not set"
    assert ch.ckc_samples.last.ecomment.include?('SET_LOT_SCRIPT_PARAMETER={OOC/No}'), 'wrong ecomment'
  end

  # MSR1305071 - Develop the ability to write Catalyst Material Properties using Toolbox WebService from SIL
  #
  # Send DCR in SIL for a Lot with 4 wfrs, 2 PChambers CHA/B (odd wfrs via CHA, even via CHB) and parameter1, such that Wfr1,2 go OOC, Wfr3 goes OOS for parameter1
  # Create 2 new manual CAs and trigger them for any of the above samples:
  # SendPCLWfrProp-OOS (PCLScenario=OOS) and SendPCLLotProp-UpScan (Name=SPCSamplingType, Value=UpScan)
  def test51_twochambers_oocandoos_sendpclwfrprop_oos_and_sendpcllotprop_upscan_manual
    pa_behaviour = 'cycle_wafer'
    li = @@sv.lot_info(@@siltest.lot, wafers: true)
    assert channels = @@siltest.setup_channels(processing_areas: pa_behaviour, cas: ['SendPCLWfrProp-OOS', 'SendPCLWfrProp-OOC', 'SendPCLWfrProp-CH-OOSValidation', 'SendPCLLotProp-UpScan', 'SendPCLLotProp-MaxWfr5'])
    # # send process DCR
    assert @@siltest.submit_process_dcr(processing_areas: pa_behaviour, readings: Hash[li.wafers.collect {|w| [w.wafer, 50]}])
    # send meas DCR with OOC values
    readings = Hash[li.wafers.each_with_index.collect {|w, i| [w.wafer, i.even? ? 60 : 150]}]
    assert dcr = @@siltest.submit_meas_dcr(readings: readings), 'error submitting DCR'
    #
    # send pcl cas (manual)
    channels[0].ckc_samples.each {|s|
      next unless s.violations.size > 0
      assert s.assign_ca('SendPCLWfrProp-OOS'), 'error assigning corective action'
      assert s.assign_ca('SendPCLLotProp-UpScan'), 'error assigning corective action'
    }
  end

  # MSR1305071 - Develop the ability to write Catalyst Material Properties using Toolbox WebService from SIL
  #
  # Send DCR in SIL for a Lot with 4 wfrs, 2 PChambers CHA/B (odd wfrs via CHA, even via CHB) and parameter1, such that Wfr1,2 go OOC, Wfr3 goes OOS for parameter1
  # Create 2 new auto CAs and trigger them for any of the above samples:
  # AutoSendPCLWfrProp-OOS (PCLScenario=OOS) and AutoSendPCLLotProp-UpScan (Name=SPCSamplingType, Value=UpScan)
  def test52_twochambers_oocandoos_sendpclwfrprop_oos_and_sendpcllotprop_upscan_auto
    pa_behaviour = 'cycle_wafer'
    li = @@sv.lot_info(@@siltest.lot, wafers: true)
    assert channels = @@siltest.setup_channels(processing_areas: pa_behaviour, cas: ['AutoSendPCLWfrProp-OOS', 'AutoSendPCLWfrProp-OOC', 'AutoSendPCLWfrProp-CH-OOSValidation', 'AutoSendPCLLotProp-UpScan', 'AutoSendPCLLotProp-MaxWfr5'])
    # # send process DCR
    assert @@siltest.submit_process_dcr(processing_areas: pa_behaviour, readings: Hash[li.wafers.collect {|w| [w.wafer, 50]}])
    # send meas DCR with OOC values
    readings = Hash[li.wafers.each_with_index.collect {|w, i| [w.wafer, i.even? ? 60 : 150]}]
    assert dcr = @@siltest.submit_meas_dcr(readings: readings), 'error submitting DCR'
  end

  # MSR1372948 - SIL-Catalyst CA to trigger OCAP efficiency elements (a) setup.FC OCAP Matrix (b) Space PreGenerated Screenshots
  #
  # Send DCR in SIL for a Lot with 4 wfrs, 2 PChambers CHA/B (odd wfrs via CHA, even via CHB) and parameter1, such that Wfr1,2 go OOC, Wfr3 goes OOS for parameter1
  # Create a new manual CA SendPCLData with CA attribute PCLScenario=Data and trigger them for any of the of the above samples.
  def test53_twochambers_oocandoos_sendpcldata_manual
    pa_behaviour = 'cycle_wafer'
    li = @@sv.lot_info(@@siltest.lot, wafers: true)
    assert channels = @@siltest.setup_channels(processing_areas: pa_behaviour, cas: ['AutoSendPCLData', 'SendPCLData'])
    # # send process DCR
    assert @@siltest.submit_process_dcr(processing_areas: pa_behaviour, readings: Hash[li.wafers.collect {|w| [w.wafer, 50]}])
    # send meas DCR with OOC values
    readings = Hash[li.wafers.each_with_index.collect {|w, i| [w.wafer, i.even? ? 60 : 150]}]
    assert dcr = @@siltest.submit_meas_dcr(readings: readings), 'error submitting DCR'
    #
    # send pcl cas (manual)
    channels[0].ckc_samples.each {|s|
      next unless s.violations.size > 0
      assert s.assign_ca('SendPCLData'), 'error assigning corective action'
    }
  end

  # MSR1372948 - SIL-Catalyst CA to trigger OCAP efficiency elements (a) setup.FC OCAP Matrix (b) Space PreGenerated Screenshots
  #
  # Send DCR in SIL for a Lot with 4 wfrs, 2 PChambers CHA/B (odd wfrs via CHA, even via CHB) and parameter1, such that Wfr1,2 go OOC, Wfr3 goes OOS for parameter1
  # Create a new auto CA SendPCLData with CA attribute PCLScenario=Data and trigger them for any of the of the above samples.
  def test54_twochambers_oocandoos_sendpcldata_auto
    pa_behaviour = 'cycle_wafer'
    li = @@sv.lot_info(@@siltest.lot, wafers: true)
    assert channels = @@siltest.setup_channels(processing_areas: pa_behaviour, cas: ['AutoSendPCLData'])
    # # send process DCR
    assert @@siltest.submit_process_dcr(processing_areas: pa_behaviour, readings: Hash[li.wafers.collect {|w| [w.wafer, 50]}])
    # send meas DCR with OOC values
    readings = Hash[li.wafers.each_with_index.collect {|w, i| [w.wafer, i.even? ? 60 : 150]}]
    assert dcr = @@siltest.submit_meas_dcr(readings: readings), 'error submitting DCR'
  end

  # MSR1211201 - Automatic sampling of potential contaminated product wafer after particle ooC at furnace tool
  def testman55_ooc_sendpcldata_batchproddi_two_wafers_multilot_meas
    assert lots = @@svtest.new_lots(6), 'error creating test lot'
    @@testlots += lots
    assert @@siltest.set_defaults(@@test_defaults.merge(lot: lots.first)), 'wrong default test data'
    lots.each {|lot| assert_equal 0, @@sv.lot_cleanup(lot)}

    assert channels = @@siltest.setup_channels(cas: ['SendPCLData-BatchProdDI']) #'AutoSendPCLData-BatchProdDI'

    # send multilot process DCR (instead of multiple DCRs) with lot parameter
    dcr = SIL::DCR.new(eqp: @@siltest.ptool)
    i = 0
    lots.each {|lot|
      li = @@sv.lot_info(lot, wafers: true)
      dcr.add_lot(lot, op: @@siltest.ppd)
      dcr.add_parameters(@@siltest.parameters, Hash[li.wafers[0..1].collect {|w| [w.wafer, 50]}])
      dcr.add_parameters("D-BOATSLOT", Hash[li.wafers[0..3].collect {|w|
          i+=1
          [w.wafer, i]
        }])
    }
    assert @@siltest.send_dcr_verify(dcr, exporttag: "p"), 'error sending DCR'

    # send multilot meas DCR (instead of multiple DCRs) with lot parameter
    dcr = SIL::DCR.new(eqp: @@siltest.mtool)
    lots.each_with_index {|lot, i|
      li = @@sv.lot_info(lot, wafers: true)
      dcr.add_lot(lot, op: @@siltest.mpd)
      readings = Hash[li.wafers[0..1].each_with_index.collect {|w, j| [w.wafer, j.even? ? 160+i : 150+i]}]
      dcr.add_parameters(@@siltest.parameters, readings)
    }
    assert @@siltest.send_dcr_verify(dcr, exporttag: "m"), 'error sending DCR'
    #
    # send pcl cas (manual)
    channels[0].ckc_samples.each {|s|
      next unless s.violations.size > 0
      assert s.assign_ca('SendPCLData-BatchProdDI'), 'error assigning corective action'
    }
    $log.info "Check channels"
    gets
  end

  # MSR1211201 - Automatic sampling of potential contaminated product wafer after particle ooC at furnace tool
  def testman56_ooc_sendpcldata_batchproddi_two_wafers_singlelot_meas
    lotcount = 7
    assert lots = @@svtest.new_lots(lotcount), 'error creating test lot'
    @@testlots += lots
    assert @@siltest.set_defaults(@@test_defaults.merge(lot: lots.first, technology: '90NM')), 'wrong default test data'
    lots.each {|lot| assert_equal 0, @@sv.lot_cleanup(lot)}

    assert channels = @@siltest.setup_channels(cas: ['SendPCLData-BatchProdDI'], technology: '90NM') #'AutoSendPCLData-BatchProdDI'

    # send multilot process DCR (instead of multiple DCRs) with lot parameter
    dcr = SIL::DCR.new(eqp: @@siltest.ptool, technology: '90NM')
    i = 0
    lots[1..lotcount].each {|lot|
      li = @@sv.lot_info(lot, wafers: true)
      dcr.add_lot(lot, op: @@siltest.ppd, technology: '90NM')
      dcr.add_parameters(@@siltest.parameters, Hash[li.wafers[0..1].collect {|w| [w.wafer, 50]}])
      dcr.add_parameters("D-BOATSLOT", Hash[li.wafers[0..3].collect {|w|
          i+=1
          [w.wafer, i]
        }])
    }
    assert @@siltest.send_dcr_verify(dcr, exporttag: "p"), 'error sending DCR'

    # send singlelot meas DCRs
    lots[1..lotcount].each_with_index {|lot, i|
      li = @@sv.lot_info(lot, wafers: true)
      dcr = SIL::DCR.new(eqp: @@siltest.mtool, technology: '90NM')
      dcr.add_lot(lot, op: @@siltest.mpd, technology: '90NM')
      readings = Hash[li.wafers[0..1].each_with_index.collect {|w, j| [w.wafer, j.even? ? 160+i : 150+i]}]
      dcr.add_parameters(@@siltest.parameters, readings)
      assert @@siltest.send_dcr_verify(dcr, exporttag: "m_#{i}"), 'error sending DCR'
    }
    # send pcl cas (manual)
    channels[0].ckc_samples.each {|s|
      next unless s.violations.size > 0
      assert s.assign_ca('SendPCLData-BatchProdDI'), 'error assigning corective action'
    }
    $log.info "Check channels"
    gets
  end

  def test70_auto_dfsworklist
    cas = ['AutoDFSWorklist']
    assert channels = @@siltest.setup_channels(cas: cas)   #AutoDFSWorklist
    # # send process DCR
    assert @@siltest.submit_process_dcr
    # send meas DCR with OOC values
    assert dcr = @@siltest.submit_meas_dcr(ooc: true, nooc: 'all*2'), 'error submitting DCR'
    # TODO: verify correct dfs message, maybe more than "cas" is needed to exactly verify
    if @@dfs_ondemand_worklist
      assert verify_dfs_message(channels, cas), 'wrong DFS message'
    end
  end

  def test80_fulllotrework
    @@test_defaults = {mpd: 'QA-MB-FTDEPM.01', ppd: 'QA-MB-FTDEP.01'}
    assert channels = @@siltest.setup_channels(cas: ['FullLotRework'])
    assert_equal 0, @@sv.lot_cleanup(@@siltest.lot)
    assert_equal 0, @@sv.lot_opelocate(@@siltest.lot, '2500.1000'), "SiView Lot Operation Locate error"
    assert @@siltest.submit_process_dcr(ppd: 'QA-MB-FTDEP.01')
    assert_equal 0, @@sv.lot_opelocate(@@siltest.lot, '2500.6000'), "SiView Lot Operation Locate error"
    # send meas DCR with OOC values
    assert dcr = @@siltest.submit_meas_dcr(ooc: true, mpd: 'QA-MB-FTDEPM.01'), 'error submitting DCR'
    assert_equal 0, @@sv.lot_futurehold_cancel(@@siltest.lot, nil), "SiView Lot Future Hold release error"
    assert_equal 0, @@sv.lot_opelocate(@@siltest.lot, 1), "SiView Lot Operation Locate error"
    assert_equal 0, @@sv.lot_hold(@@siltest.lot, 'FAT'), "SiView LotHold error"
    # verify manual actions with last sample in CKC
    s = channels.first.ckc_samples.last
    #
    # FullLotRework
    assert s.assign_ca('FullLotRework'), 'error assigning CA'
    sleep 5
    assert @@siltest.verify_ecomment(channels.first, "FullLotRework[lotId = '#{@@siltest.lot}']")
  end

  def test81_fulllotrework_error
    @@test_defaults = {mpd: 'QA-MB-FTDEPM.01', ppd: 'QA-MB-FTDEP.01'}
    assert channels = @@siltest.setup_channels(cas: ['FullLotRework-Error'])
    assert_equal 0, @@sv.lot_cleanup(@@siltest.lot)
    assert_equal 0, @@sv.lot_opelocate(@@siltest.lot, '2500.1000'), "SiView Lot Operation Locate error"
    assert @@siltest.submit_process_dcr(ppd: 'QA-MB-FTDEP.01')
    assert_equal 0, @@sv.lot_opelocate(@@siltest.lot, '2500.6000'), "SiView Lot Operation Locate error"
    # send meas DCR with OOC values
    assert dcr = @@siltest.submit_meas_dcr(ooc: true, mpd: 'QA-MB-FTDEPM.01'), 'error submitting DCR'
    assert_equal 0, @@sv.lot_futurehold_cancel(@@siltest.lot, nil), "SiView Lot Future Hold release error"
    assert_equal 0, @@sv.lot_opelocate(@@siltest.lot, 1), "SiView Lot Operation Locate error"
    assert_equal 0, @@sv.lot_hold(@@siltest.lot, 'FAT'), "SiView LotHold error"
    # verify manual actions with last sample in CKC
    s = channels.first.ckc_samples.last
    #
    # FullLotRework-Error
    assert s.assign_ca('FullLotRework-Error'), 'error assigning CA'
    sleep 5
    assert @@siltest.verify_ecomment(channels.first, "Execution of [FullLotRework] failed: Missing CorrectiveAction attributes")
  end


  # aux methods

  # verify DFS message after OOC idenified by channels, drops all other messages on the DFS queue, returns true on success
  def verify_dfs_message(channels, cas)
    $log.info "verify_dfs_message #{cas}"
    msgs = @@dfs.receive_msgs
    $log.info "received #{msgs.count} DFS messages"
    $log.info "Messages: #{msgs.inspect}"
    # find relevant message with all channel ids
    assert msg = msgs.find {|msg| channels.inject(true) {|ok, ch| ok && msg.include?("ChannelId: #{ch.chid}")} }, 'no suitable DFS message'
    # extract ooc information
    ooc_begin = /\n=========================\nOOC Information\n-------------------------\n/
    ooc_end = /\n-------------------------\nEnd of OOC Information\n=========================\n/
    oocs = msg.scan(/#{ooc_begin}(.+?)#{ooc_end}/m).collect {|ss| ss.first}
    assert_equal @@siltest.parameters.size * SIL::DCR::DefaultWaferValues.size * 2, oocs.count, 'wrong number of DFS OOCs'
    ca_begin = /Corrective Actions:\n-------------------------\n/
    ca_end = /\n\nCharts:/
    channels.each {|ch|
      oocs_by_channel = oocs.select {|ooc| ooc =~ /ChannelId: #{ch.chid}/}
      oocs_by_channel = oocs_by_channel.collect {|ooc| ooc.match(/#{ca_begin}(.+?)#{ca_end}/m) {|m| m[1]}}.compact
      oocs_by_channel.each {|ooc| cas.each {|ca| assert ooc.include?(ca)}}
    }
    return true
  end

end
