=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: Steffen Steidten, 2011-11-25
=end

require_relative 'SILSmoke'


# Channel behaviour, was Test_Space00 (No Control Plan)
class SILSmoke_16 < SILSmoke
  @@test_defaults = {
    mpd: 'QA-MB-CC-M-NOCP.01', ppd: 'QA-MB-CC.01', department: 'QACCALL', technology: 'TEST',
    parameters: ['QA_PARAM_900_CC_noCP', 'QA_PARAM_901_CC_noCP', 'QA_PARAM_902_CC_noCP', 'QA_PARAM_903_CC_noCP', 'QA_PARAM_904_CC_noCP'],
    cas: ['LotHold', 'AutoLotHold', 'CancelLotHold', 'ReleaseLotHold']
  }


  def test00_setup
    $setup_ok = false
    #
    @@advanced_props = @@siltest.server.read_jobprops
    #
    assert lot = @@svtest.new_lot, 'error creating test lot'
    @@testlots << lot
    $log.info "using lot #{lot}"
    #
    assert @@siltest.set_defaults(@@test_defaults.merge(lot: lot))
    #
    $setup_ok = true
  end

  # was: test0033a_cc_testlds_noControlPlan_study
  def test11_study
    pname = :'parameters.missingspeclimit.upload.setup.enabled'
    assert prop = @@advanced_props[pname], "no such prop: #{pname.inspect}"
    if prop == 'true'
      assert dcr = @@siltest.cleanup_send_dcrs(:default, state: 'Study')
      tstart = Time.now
      assert @@siltest.verify_aqv_message(dcr, 'success'), "wrong AQV message"
      refute @@siltest.wait_futurehold('Chart Check issue', tstart: tstart), "missing future hold"
      refute @@siltest.wait_notification('Chart Check Failures for Lot', tstart: tstart), 'notification missing'
    else
      $log.info "Please set advanced config param parameters.missingspeclimit.upload.setup.enabled = true and rerun this test."
      assert false
    end
  end

  # was: test0033b_cc_testlds_noControlPlan_offline
  def test21_offline
    pname = :'parameters.missingspeclimit.upload.setup.enabled'
    assert prop = @@advanced_props[pname], "no such prop: #{pname.inspect}"
    if prop == 'true'
      assert dcr = @@siltest.cleanup_send_dcrs(:default, state: 'Offline')
      tstart = Time.now
      assert @@siltest.verify_aqv_message(dcr, 'success'), "wrong AQV message"
      refute @@siltest.wait_futurehold('Chart Check issue', tstart: tstart), "missing future hold"
      refute @@siltest.wait_notification('Chart Check Failures for Lot', tstart: tstart), 'notification missing'
    else
      $log.info "Please set advanced config param parameters.missingspeclimit.upload.setup.enabled = true and rerun this test."
      assert false
    end
  end

end
