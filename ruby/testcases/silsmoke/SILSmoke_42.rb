=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: Conny Stenzel, 2012-06-27

History:
  2017-08-01 cstenzel/sfrieske, refactored all tests
=end

require_relative 'SILSmoke'

# ERFs, MSRs 547158, 581025  was: Test_Space12
class SILSmoke_42 < SILSmoke
  @@test_defaults = {mpd: 'QA-MULT-PD-MB-FTDEPM.01', mtool: 'QAMeas'}  # TODO: mtool required?
  @@generic_ekeys_dc_0 = {'Reserve4'=>'equipment', 'Reserve5'=>'logicalrecipe',
    'Reserve6'=>'mainprocess', 'Reserve7'=>'process', 'Reserve8'=>'dcpname'}
  @@generic_dkeys_dc_0 = {'DatReserve4'=>'equipment', 'DatReserve5'=>'logicalrecipe',
    'DatReserve6'=>'mainprocess', 'DatReserve7'=>'process', 'DatReserve8'=>'dcpname'}


  def test00_setup
    $setup_ok = false
    #
    assert @@siltest.set_defaults(@@test_defaults)
    @@ppds = @@siltest.db.pd_reference(mpd: @@siltest.mpd)
    assert @@ppds.size > 2, 'not enough related PPDs'
    #
    $setup_ok = true
  end


  # multi process

  def test11_lot_multiproces1
    plot_wafers = {
      'SILSPLIT01.000' => ['WAFER0101', 'WAFER0102', 'WAFER0103'],
      'SILSPLIT02.000' => ['WAFER0201', 'WAFER0202', 'WAFER0203'],
      'SILSPLIT03.000' => ['WAFER0301', 'WAFER0302', 'WAFER0303']
    }
    mlot_wafers = {
      'SILSPLIT01.000' => ['WAFER0102', 'WAFER0103'],
      'SILSPLIT01.001' => ['WAFER0101'],
      'SILSPLIT02.001' => ['WAFER0201', 'WAFER0202', 'WAFER0203'],
      'SILSPLIT03.001' => ['WAFER0301'],
      'SILSPLIT03.002' => ['WAFER0302'],
      'SILSPLIT03.003' => ['WAFER0303']
    }
    submit_multidcrs_verify(plot_wafers, mlot_wafers)
  end

  def test12_lot_multiprocess2
    plot_wafers = {
      'SILSPLIT01.000' => ['WAFER0101', 'WAFER0107', 'WAFER0105'],
      'SILSPLIT01.001' => ['WAFER0102', 'WAFER0104'],
      'SILSPLIT01.002' => ['WAFER0103', 'WAFER0106']
    }
    mlot_wafers = {
      'SILSPLIT01.000' => ['WAFER0101', 'WAFER0103', 'WAFER0105', 'WAFER0107','WAFER0106'],
      'SILSPLIT01.001' => ['WAFER0102', 'WAFER0104']
    }
    submit_multidcrs_verify(plot_wafers, mlot_wafers)
  end

  def test13_lot_multiprocess3
    plot_wafers = {
      'SILSPLIT01.001' => ['WAFER0101', 'WAFER0104'],
      'SILSPLIT01.002' => ['WAFER0103', 'WAFER0102']
    }
    mlot_wafers = {
      'SILSPLIT01.000' => ['WAFER0101', 'WAFER0102', 'WAFER0103', 'WAFER0104']
    }
    submit_multidcrs_verify(plot_wafers, mlot_wafers)
  end

  def test14_lot_multiprocess4
    plot_wafers = {
      'SILSPLIT01.001' => ['WAFER0101', 'WAFER0104'],
      'SILSPLIT01.002' => ['WAFER0103', 'WAFER0102'],
      'SILSPLIT01.003' => ['WAFER0105', 'WAFER0106'],
    }
    mlot_wafers = {
      'SILSPLIT01.002' => [],
      'SILSPLIT01.003' => []
    }
    submit_multidcrs_verify(plot_wafers, mlot_wafers)
  end

  def test15_lot_multiprocess5
    plot_wafers = {
      'SILSPLIT01.001' => ['WAFER0101', 'WAFER0104'],
      'SILSPLIT01.002' => ['WAFER0103', 'WAFER0102'],
      'SILSPLIT01.003' => ['WAFER0105', 'WAFER0106'],
    }
    mlot_wafers = {
      'SILSPLIT01.000' => ['WAFER0101', 'WAFER0102', 'WAFER0103', 'WAFER0104'],
      'SILSPLIT01.003' => []
    }
    submit_multidcrs_verify(plot_wafers, mlot_wafers)
  end


  # single process

  def test21_lot_split_singleprocess1
    plot_wafers = {
      'SILSPLIT01.000' => ['WAFER0101', 'WAFER0102', 'WAFER0103'],
      'SILSPLIT02.000' => ['WAFER0201', 'WAFER0202', 'WAFER0203'],
      'SILSPLIT03.000' => ['WAFER0301', 'WAFER0302', 'WAFER0303']
    }
    mlot_wafers = {
      'SILSPLIT01.000' => ['WAFER0102', 'WAFER0103'],
      'SILSPLIT01.001' => ['WAFER0101'],
      'SILSPLIT02.001' => ['WAFER0201', 'WAFER0202', 'WAFER0203'],
      'SILSPLIT03.001' => ['WAFER0301'],
      'SILSPLIT03.002' => ['WAFER0302'],
      'SILSPLIT03.003' => ['WAFER0303']
    }
    submit_singledcrs_verify(plot_wafers, mlot_wafers)
  end

  def test22_lot_split_singleprocess2
    plot_wafers = {
      'SILSPLIT01.000' => ['WAFER0101', 'WAFER0107', 'WAFER0105'],
      'SILSPLIT01.001' => ['WAFER0102', 'WAFER0104'],
      'SILSPLIT01.002' => ['WAFER0103', 'WAFER0106']
    }
    mlot_wafers = {
      'SILSPLIT01.000' => ['WAFER0101', 'WAFER0103', 'WAFER0105', 'WAFER0107','WAFER0106'],
      'SILSPLIT01.001' => ['WAFER0102','WAFER0104']
    }
    submit_singledcrs_verify(plot_wafers, mlot_wafers)
  end

  def test23_lot_split_singleprocess3
    plot_wafers = {
      'SILSPLIT01.001' => ['WAFER0101', 'WAFER0104'],
      'SILSPLIT01.002' => ['WAFER0103', 'WAFER0102']
    }
    mlot_wafers = {
      'SILSPLIT01.000' => ['WAFER0101', 'WAFER0102', 'WAFER0103', 'WAFER0104']
    }
    submit_singledcrs_verify(plot_wafers, mlot_wafers)
  end

  def test24_lot_split_singleprocess4
    plot_wafers = {
      'SILSPLIT01.001' => ['WAFER0101', 'WAFER0104'],
      'SILSPLIT01.002' => ['WAFER0103', 'WAFER0102'],
      'SILSPLIT01.003' => ['WAFER0105', 'WAFER0106'],
    }
    mlot_wafers = {
      'SILSPLIT01.002' => [],
      'SILSPLIT01.003' => []
    }
    submit_singledcrs_verify(plot_wafers, mlot_wafers)
  end

  def test25_lot_split_singleprocess5
    plot_wafers = {
      'SILSPLIT01.001' => ['WAFER0101','WAFER0104'],
      'SILSPLIT01.002' => ['WAFER0103','WAFER0102'],
      'SILSPLIT01.003' => ['WAFER0105','WAFER0106'],
    }
    mlot_wafers = {
      'SILSPLIT01.000' => ['WAFER0101', 'WAFER0102', 'WAFER0103', 'WAFER0104'],
      'SILSPLIT01.003' => []
    }
    submit_singledcrs_verify(plot_wafers, mlot_wafers)
  end


  # aux methods

  def submit_multidcrs_verify(plot_wafers, mlot_wafers)
    caller = File.basename(caller_locations(1, 1).last.path, '.rb') + '_' + caller_locations(1, 1).last.label
    lotparameter = 'QA_LOT_PARAM'
    wafer_eqp = {}
    wafer_cha = {}
    wafer_ope = {}
    #
    # submit process DCRs for each lot
    plot_wafers.each_with_index {|e, pindex|
      lot, wafers = e
      wafers.each_with_index {|w, i|
        wafer_ope[w] = @@ppds[pindex]
        wafer_eqp[w] = "#{@@siltest.ptool}_#{pindex}"
      }
      @@siltest.submit_process_dcr(ptool: "#{@@siltest.ptool}_#{pindex}", ppd: @@ppds[pindex], lot: lot, parameters: ['Dummy'],
        readings: Hash[wafers.each_with_index.to_a], processing_areas: 'cycle_wafer', export: "log/sil/#{caller}_#{pindex}_p.xml")
      wafers.each_with_index {|w, i| wafer_cha[w] = @@siltest.chambers[i % @@siltest.chambers.size]}
    }
    #
    # submit meas DCR for related MPD
    dcr = SIL::DCR.new(chambers: @@siltest.chambers)
    mlot_wafers.each_pair {|lot, wafers|
      dcr.add_lot(lot, op: @@siltest.mpd)
      if wafers != []
        dcr.add_parameters(@@siltest.parameters, Hash[wafers.each_with_index.to_a])  # {wafer1: 0, wafer2: 1, wafer3: 2}
      else
        dcr.add_parameter(lotparameter, {lot=>7.0}, readingtype: 'Lot')
      end
    }
    assert @@siltest.send_dcr_verify(dcr, export: "log/sil/#{caller}_m.xml"), 'error submitting DCR'
    #
    # verify sample extractor keys
    assert folder = @@lds.folder(@@autocreated_qa)
    @@siltest.parameters.each {|p|
      ch = folder.spc_channel(p) || next
      samples = ch.samples
      assert_equal mlot_wafers.values.flatten.size, samples.size, "wrong number of samples in #{ch.inspect}"
      samples.each {|s|
        w = s.ekeys['Wafer']
        assert verify_hash({'POperationID'=>wafer_ope[w], 'PTool'=>wafer_eqp[w], 'PChamber'=>wafer_cha[w]}, s.ekeys, nil, refonly: true), 'wrong ekeys'
      }
    }
    unless dcr.readings(lotparameter, readingtype: 'Lot').empty?
      # verify lot parameter channel, there may be multiple channels, was broken in Test_Space12!
      assert ch = folder.spc_channels(parameter: lotparameter).first
      assert s = ch.samples.first, 'no sample'
      l = s.ekeys['Lot']
      w = plot_wafers[l].first
      assert verify_hash({'POperationID'=>wafer_ope[w], 'PTool'=>wafer_eqp[w], 'PChamber'=>'-'}, s.ekeys, nil, refonly: true), 'wrong ekeys'
    end
  end

  def submit_singledcrs_verify(plot_wafers, mlot_wafers, params={})
    caller = File.basename(caller_locations(1, 1).last.path, '.rb') + '_' + caller_locations(1, 1).last.label
    lotparameter = 'QA_LOT_PARAM'
    ptool = "#{@@siltest.ptool}_0"
    ppd = @@ppds.first
    wafer_cha = {}
    #
    # submit 1 process DCR
    dcr = SIL::DCR.new(eqp: ptool, eqptype: :chamber, chambers: @@siltest.chambers)
    plot_wafers.each_pair {|lot, wafers|
      dcr.add_lot(lot, op: ppd)
      dcr.add_parameter('Dummy', Hash[wafers.each_with_index.to_a], processing_areas: 'cycle_wafer', nocharting: true)  # {wafer1: 0, wafer2: 1, wafer3: 2}
    }
    assert @@siltest.send_dcr_verify(dcr, nsamples: 0, export: "log/sil/#{caller}_p.xml"), 'error submitting DCR'
    plot_wafers.values.flatten.each_with_index {|w, i| wafer_cha[w] = @@siltest.chambers[i % @@siltest.chambers.size]}
    #
    # submit meas DCR for related MPD
    dcr = SIL::DCR.new(chambers: @@siltest.chambers)
    mlot_wafers.each_pair {|lot, wafers|
      dcr.add_lot(lot, op: @@siltest.mpd)
      if wafers != []
        dcr.add_parameters(@@siltest.parameters, Hash[wafers.each_with_index.to_a])  # {wafer1: 0, wafer2: 1, wafer3: 2}
      else
        dcr.add_parameter(lotparameter, {lot=>7.0}, readingtype: 'Lot')
      end
    }
    assert @@siltest.send_dcr_verify(dcr, export: "log/sil/#{caller}_m.xml"), 'error submitting DCR'
    #
    # verify sample extractor keys
    assert folder = @@lds.folder(@@autocreated_qa)
    @@siltest.parameters.each {|p|
      ch = folder.spc_channel(p) || next
      samples = ch.samples
      assert_equal mlot_wafers.values.flatten.size, samples.size, "wrong number of samples in #{ch.inspect}"
      samples.each {|s|
        w = s.ekeys['Wafer']
        assert verify_hash({'POperationID'=>ppd, 'PTool'=>ptool, 'PChamber'=>wafer_cha[w]}, s.ekeys, nil, refonly: true), 'wrong ekeys'
      }
    }
    unless dcr.readings(lotparameter, readingtype: 'Lot').empty?
      # verify lot parameter channel, there may be multiple channels, was broken in Test_Space12!
      assert ch = folder.spc_channels(parameter: lotparameter).first
      assert s = ch.samples.first, 'no sample'
      assert verify_hash({'POperationID'=>ppd, 'PTool'=>ptool, 'PChamber'=>'-'}, s.ekeys, nil, refonly: true), 'wrong ekeys'
    end
  end

end
