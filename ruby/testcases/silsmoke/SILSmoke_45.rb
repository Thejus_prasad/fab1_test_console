=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: Conny Stenzel, 2012-06-27

History:
  2017-08-01 cstenzel/sfrieske, refactored all tests
  2021-02-05 sfrieske, use instance_of instead of kind_of
=end

require_relative 'SILSmoke'


# Rework  was: Test_Space14 part2
# Note: lot ids must be unique because SIL keeps lot/wafer history even if all related channels have been deleted
class SILSmoke_45 < SILSmoke
  @@test_defaults = {mpd: 'QA-MB-FTDEPM.01', mtool: 'QAMeas'}
  # @@erf_mpd = 'e1234-QA-MB-FTDEPM-A0.01'
  # @@erf_ppd = 'e1234-QA-MB-FTDEP-A0.01'
  # @@erf_route = 'e1234-QASIL-A0.01'
  @@flagging_delay = 5
  @@inline_rework_autoflagging_enabled = true
  # @@setup_rework_autoflagging_enabled = true


  def test00_setup
    $setup_ok = false
    #
    assert @@siltest.server.verify_property(:'inline.rework.autoflagging.enabled', @@inline_rework_autoflagging_enabled), 'wrong property'
    # assert @@siltest.server.verify_property(:'setup.rework.autoflagging.enabled', @@setup_rework_autoflagging_enabled), 'wrong property'
    #
    assert @@siltest.set_defaults(@@test_defaults)
    #
    $setup_ok = true
  end

  def test11_rework
    skip 'inline.rework.autoflagging.enabled=false' unless @@inline_rework_autoflagging_enabled
    #
    assert lot = @@sv.generate_new_lotids('gf').first
    parameters = @@siltest.parameters.collect {|p| p + '_REWORK'}
    #
    3.times {|i| rework_scenario(lot, parameters: parameters, mpd: 'QA-MB-RW-M.01', ppd: 'QA-MB-RW.01', run: i+1)}
  end


  # aux methods

  def rework_scenario(lot, params={})
    lottag = lot
    lottag = lot.join('-') if lot.instance_of?(Array)
    run = params[:run] || '0'
    technology = params[:technology] || '32NM'
    lds = technology == 'TEST' ? @@lds_setup : @@lds
    parameters = params[:parameters] || @@siltest.parameters
    # count old samples
    assert folder = lds.folder(@@autocreated_qa)
    old_nsamples = {}
    parameters.each {|p|
      ch = folder.spc_channel(p)
      old_nsamples[p] = ch ? ch.samples.size : 0
    }
    # submit process DCR
    assert @@siltest.submit_process_dcr(lot: lot, ppd: params[:ppd], route: params[:route],
      technology: technology, parameters: params[:parameters], readings: params[:readings], exporttag: "#{lottag}-#{run}")
    # submit meas DCR
    assert dcr = @@siltest.submit_meas_dcr(lot: lot, mpd: params[:mpd], route: params[:route],
      technology: technology, parameters: params[:parameters], readings: params[:readings], nsamples: params[:nsamples], exporttag: "#{lottag}-#{run}")
    # wait for flagging and check flags
    $log.info "waiting #{@@flagging_delay} s for flagging"; sleep @@flagging_delay
    assert folder = lds.folder(@@autocreated_qa)
    # check for flags
    parameters.each {|p|
      assert ch = folder.spc_channel(p), 'no channel'
      samples = ch.samples
      dcrreadingssize = parameters.first.include?('-LOT') ? 1 : dcr.readings(p).size
      assert_equal old_nsamples[p] + dcrreadingssize, samples.size, "wrong number of samples in #{ch.inspect}"
#      if technology != 'TEST' && @@inline_rework_autoflagging_enabled || technology == 'TEST' &&  @@setup_rework_autoflagging_enabled
        assert verify_channel_reworked(ch, params[:ekey]), 'samples not autoflagged for rework'
      # else
      #   if old_nsamples[p] > 0
      #     samples[0..(old_nsamples[p]-1)].each {|s|
      #       assert !s.iflag || !s.icomment || !s.icomment.include?('Rework'), 'wrong autoflagging for rework'
      #     }
      #   end
      # end
    }
  end

  def verify_channel_reworked(ch, ekey, params={})
    rhash = {}
    ch.samples.each {|s|
      ekeys = s.ekeys
      lotwafer = s.ekeys[ekey || 'Wafer']
      rkey = [ekeys['Lot'], ekeys['Wafer'], ekeys['PTool'], ekeys['POperationID'], s.dkeys['MOperationID'], lotwafer].join(':')
      rhash[rkey] ||= []
      rhash[rkey] << s
    }
    ret = true
    rhash.each_pair {|rkey, samples|
      samples.sort! {|a, b| a.dkeys['MTime'] <=> b.dkeys['MTime']}
      reftime = samples.last.time # sample time of newest sample is the reference
      samples.each_with_index {|s, i|
        if s.time < reftime
          ret &= s.iflag && s.icomment && s.icomment.include?('Rework')
        else
          ret &= !s.iflag || !s.icomment || !s.icomment.include?('Rework')
        end
      }
    }
    return ret
  end

end
