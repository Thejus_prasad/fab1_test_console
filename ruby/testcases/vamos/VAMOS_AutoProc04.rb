=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.


Author: Steffen Steidten, 2014-03-03

History:
  2015-03-13 ssteidte, more reliable test preparation
  2018-05-24 sfrieske, minor cleanup, use @@vaptest instead of $vaptest
  2019-02-22 sfrieske, minor cleanup, renamed from Test_VAMOS.. to VAMOS...
=end

require_relative 'VAMOS_AutoProc'


# Target scenarios
class VAMOS_AutoProc04 < VAMOS_AutoProc

  def test400_setup
    $setup_ok = false
    #
    assert @@lot = @@svtest.new_lot, 'error creating test lot'
    @@testlots << @@lot
    # find opeartion/tool combinations for the test cases
    assert res = @@vaptest.find_operation_eqp(ib: false, e2e: true), 'no eqp found'
    @@opNo_e2e_fb, @@eqp_e2e_fb = res
    #
    $setup_ok = true
  end

  def test411_tgt_port
    lot = @@lot
    opNo = @@vaptest.operations.keys.first
    eqp = @@vaptest.operations[opNo]
    assert_equal 0, @@sv.lot_cleanup(lot, opNo: opNo, stocker: @@svtest.stocker, xjs: true)
    assert @@vaptest.prepare_eqp(eqp, mode: 'Auto-3')
    # register RTD emulator rule with a specific target port and wait for AutoProc
    tstart = Time.now
    port = @@sv.eqp_info(eqp).ports.last.port
    wn = RTD::WhatNextVapTarget.new(@@sv, lot, port: port)
    @@vaptest.rtdremote.add_emustation(eqp, method: wn)
    $log.info "waiting up to 5 min for AutoProc to call WhatNext for #{eqp}"
    assert wait_for(timeout: 300) {
      @@vaptest.mdswrite.write_eqpwip(eqp, 1)
      @@sv.lot_info(lot).status == 'Processing'
    }, "AutoProc did not call #{eqp}"
    #
    # verify the specified port is used
    assert_equal @@sv.lot_info(lot).carrier, @@sv.eqp_info(eqp).ports.last.loaded_carrier, 'wrong port'
  end

  def test412_invalid_port
    lot = @@lot
    opNo = @@vaptest.operations.keys.first
    eqp = @@vaptest.operations[opNo]
    assert_equal 0, @@sv.lot_cleanup(lot, opNo: opNo, stocker: @@svtest.stocker, xjs: true)
    assert @@vaptest.prepare_eqp(eqp, mode: 'Auto-3')
    # register RTD emulator rule with a wrong target port and wait for AutoProc
    tstart = Time.now
    wn = RTD::WhatNextVapTarget.new(@@sv, lot, port: 'XX1')  # wrong port
    @@vaptest.rtdremote.add_emustation(eqp, method: wn)
    assert wait_for(timeout: 300) {
      @@vaptest.mdswrite.write_eqpwip(eqp, 1)
      @@vaptest.vapdb.scenariolog(tstart: tstart, scenario: '%Pull%', tgteqp: eqp, msg: 'No free port%').first
    }, "AutoProc did not call #{eqp}"
  end

  def test421_tgt_eqp
    lot = @@lot
    opNo = @@opNo_e2e_fb
    eqp = @@vaptest.operations[opNo]
    opNo2 = @@vaptest.operations.keys[@@vaptest.operations.keys.index(opNo) + 1]
    eqp2 = @@vaptest.operations[opNo2]
    assert_equal 0, @@sv.lot_cleanup(lot, opNo: opNo, stocker: @@svtest.stocker, xjs: true)
    assert @@vaptest.prepare_eqp([eqp, eqp2], mode: 'Auto-3')
    # process lot at first eqp, don't yet unload
    port = @@sv.eqp_info(eqp).ports.first.port
    assert @@sv.claim_process_lot(lot, eqp: eqp, port: port, mode: 'Auto-3', nounload: true), 'error processing lot'
    #
    # register RTD emulator rule with a (not existing) tgteqp for eqp2 and switch port to UnloadReq
    tstart = Time.now
    assert_equal 0, @@sv.eqp_port_status_change(eqp, port, 'UnloadReq'), 'error switching port status'
    wn = RTD::WhatNextVapTarget.new(@@sv, lot, eqp: 'NOTEX')
    @@vaptest.rtdremote.add_emustation(eqp2, method: wn)
    assert wait_for {
      @@vaptest.mdswrite.write_eqpwip(eqp2, 1)
      @@vaptest.vapdb.scenariolog(tstart: tstart, scenario: '%Push%', tgteqp: 'NOTEX').first
    }, 'scenario error'
  end

  def test422_tgt_stocker
    lot = @@lot
    opNo = @@opNo_e2e_fb
    eqp = @@vaptest.operations[opNo]
    opNo2 = @@vaptest.operations.keys[@@vaptest.operations.keys.index(opNo) + 1]
    eqp2 = @@vaptest.operations[opNo2]
    stockers = @@sv.eqp_info_raw(eqp2).equipmentStockerInfo.strEqpStockerStatus.collect {|s| s.stockerID.identifier}
    assert stockers.size > 1, "eqp #{eqp2} must have at least 2 stockers configured"
    assert_equal 0, @@sv.lot_cleanup(lot, opNo: opNo, stocker: @@svtest.stocker, xjs: true)
    assert @@vaptest.prepare_eqp([eqp, eqp2], mode: 'Auto-3')
    # process lot at first eqp, don't yet unload
    port = @@sv.eqp_info(eqp).ports.first.port
    assert @@sv.claim_process_lot(lot, eqp: eqp, port: port, mode: 'Auto-3', nounload: true), 'error processing lot'
    #
    # register RTD emulator rule with target stocker for eqp2 and switch port to UnloadReq
    tstart = Time.now
    stocker = stockers[1]
    wn = RTD::WhatNextVapTarget.new(@@sv, lot, eqp: 'NoCandidate', stocker: stocker)
    @@vaptest.rtdremote.add_emustation(eqp2, method: wn)
    assert_equal 0, @@sv.eqp_port_status_change(eqp, port, 'UnloadReq'), 'error switching port status'
    carrier = @@sv.lot_info(lot).carrier
    assert wait_for {
      @@vaptest.mdswrite.write_eqpwip(eqp2, 1)
      !@@sv.carrier_xferjob(carrier).empty?
    }, 'no xfer job'
    #
    # verify scenario and xfer job
    assert wait_for {
      @@vaptest.vapdb.scenariolog(tstart: tstart, scenario: 'AutoPush', carrier: carrier, srceqp: eqp, tgteqp: stocker).first
    }
    assert xjs = @@sv.carrier_xferjob(carrier).first, 'no xfer job'
    assert_equal stocker, xjs.strCarrierJobResult.first.toMachine.identifier, 'wrong xfer job'
  end

end
