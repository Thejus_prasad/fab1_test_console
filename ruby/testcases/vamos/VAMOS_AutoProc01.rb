=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.


Author: Steffen Steidten, 2014-03-03

History:
  2015-03-13 ssteidte, more reliable test preparation
  2016-05-18 sfrieske, adapted msg lookup for AutoPullRsv
  2018-05-24 sfrieske, minor cleanup, use @@vaptest instead of $vaptest
  2019-02-22 sfrieske, minor cleanup, renamed from Test_VAMOS.. to VAMOS...
=end

require_relative 'VAMOS_AutoProc'


# Basic Auto-3 Scenarios
class VAMOS_AutoProc01 < VAMOS_AutoProc
  @@max_duration = 15 # minutes
  @@opEqps = /^UTA/   # eqpa at AutoProc PDs, including the secondary tools


  def teardown
    @@dummyamhs.set_variable('unloadtime', 5000)
  end


  def test100_setup
    $setup_ok = false
    #
    assert @@dummyamhs = Dummy::AMHS.new($env)
    # disable secondary tools
    @@vaptest.stop_test(@@sv.eqp_list(eqp: @@opEqps))
    #
    $setup_ok = true
  end

  def test101_single
    $stup_ok = false
    #
    assert @@dummyamhs.set_variable('unloadtime', 15000)  # better chance for AutoPush tool-to-tool
    # get the operation after the last test op to see if the test is complete
    lastop = @@vaptest.operations.keys.sort.last
    #
    assert lot = @@svtest.new_lot, 'error creating test lot'
    @@testlots << lot
    assert_equal 0, @@sv.lot_opelocate(lot, @@vaptest.operations.keys.first)
    assert @@vaptest.prepare_eqp(nil)
    tstart = Time.now
    #
    # test start: register RTD emulator rules to return only the test lot
    @@vaptest.set_rtdemu_filter(lot, nil)
    ##@@vaptest.rtdremote.add_emustation('VAMOS_WN_A', rule: 'WhereNextEquipment') # where_next for t2t, done in setup()
    $log.info "waiting up to #{@@max_duration} min for AutoProc to process lot #{lot.inspect}"
    assert wait_for(timeout: @@max_duration * 60) {
      li = @@sv.lot_info(lot)
      active = @@vaptest.operations.keys.member?(li.opNo)
      # re-enable WIP status (maybe)
      @@vaptest.operations.values.each {|eqp| @@vaptest.mdswrite.write_eqpwip(eqp, 1) if li.opEqps.member?(eqp)} if active
      # yield true if the end is reached
      !active
    }, "lot #{lot} has not been processed at all PDs"
    #
    # stop and cleanup rules
    @@vaptest.stop_test(nil)
    # get the lot's operation history
    $log.info("verifying operation history of lot #{lot.inspect}")
    hist = @@sv.lot_operation_history(lot, opNo: @@vaptest.operations.keys.first)
    opestarts = hist.collect {|e|
      [e.opNo, e.timestamp] if e.category == 'OperationStart' && @@vaptest.operations.keys.index(e.opNo) && e.timestamp > @@sv.siview_timestamp(tstart)
    }.compact
    #
    # verify lot has been processed on all PDs
    assert_equal @@vaptest.operations.length, opestarts.length, "wrong operation history: #{opestarts.inspect} instead of #{@@vaptest.operations.keys.inspect}"
    opestarts = opestarts.reverse[0...@@vaptest.operations.length].reverse
    assert_equal @@vaptest.operations.keys, opestarts.collect {|e| e[0]}, "operation history check failed for #{lot}"
    #
    # verify scenarios
    $log.info 'waiting 1 min for scenario log updates'; sleep 60
    sclog = @@vaptest.vapdb.scenariolog(tstart: tstart)
    failed = []
    failed << 'AutoPullRsv' if sclog.select {|sc|
      sc.scenario == 'AutoPull' && !sc.carrier.nil? && sc.msg.include?('Reservation (stocker-to-tool) done')
    }.empty?  # stocker -> UTA001
    failed << 'AutoPullIBRsv' if sclog.select {|sc|
      sc.scenario == 'AutoPullInternalBuffer' && sc.msg.include?('Reservation (stocker-to-tool) done')
    }.empty?  # stocker -> UTA005
    failed << 'AutoPushToStk' if sclog.select {|sc|
      sc.scenario == 'AutoPush' && sc.srctype == 'EQP' && sc.tgttype == 'STK' && sc.msg.include?('StockIn done')
    }.empty?  # UTA003 -> stocker
    failed << 'AutoPushToEqp' if sclog.select {|sc|
      sc.scenario == 'AutoPush' && sc.srctype == 'EQP' && sc.tgttype == 'EQP' && sc.msg.include?('Reservation (tool-to-tool) done')
    }.empty?  # UTA001 -> UTA002, UTA002 -> UTA003
    failed << 'AutoPushIBtoStk' if sclog.select {|sc|
      sc.scenario == 'AutoPushInternalBuffer' && sc.srctype == 'EQP' && sc.tgttype == 'STK'
    }.empty?  # UTA004 -> stocker
    failed << 'AutoPushIBtoEqp' if sclog.select {|sc|
      sc.scenario == 'AutoPushInternalBuffer' && sc.srctype == 'EQP' && sc.tgttype == 'EQP'
    }.empty?  # UTA005 -> UTA006
    assert_empty failed, "some scenarios did not occur: #{failed}"
    # NoWIP
    assert e = @@vaptest.vapdb.scenariolog(scenario: 'NoWIP', nowip: true).first, 'missing NoWip call'
    assert e.msg.start_with?('NoWIP call '), "wrong scenario text #{e.msg}"
    #
    $setup_ok = true
  end

  def test102_batch
    # find operation/tool combinations
    assert res = @@vaptest.find_operation_eqp(ib: false, max_batchsize: 2), 'no eqp found'
    opNo_fb, eqp_fb = res
    assert res = @@vaptest.find_operation_eqp(ib: true, max_batchsize: 2), 'no eqp found'
    opNo_ib, eqp_ib = res
    # create test lots
    assert lots = @@svtest.new_lots(2), 'error creating test lots'
    @@testlots += lots
    #
    [opNo_fb, opNo_ib].each {|opNo|
      eqp = @@vaptest.operations[opNo]
      lots.each {|lot| assert_equal 0, @@sv.lot_opelocate(lot, opNo)}
      assert @@vaptest.prepare_eqp(eqp, mode: 'Auto-3')
      #
      # test start: register RTD emulator rule to return only the test lots
      tstart = Time.now
      @@vaptest.set_rtdemu_filter(lots, eqp)
      $log.info "waiting up to 5 min for AutoProc to process lots #{lots.inspect}"
      assert wait_for(timeout: 300) {
        # re-enable WIP status (maybe)
        @@vaptest.mdswrite.write_eqpwip(eqp, 1)
        # verify lots have been processed
        lots.inject(true) {|ok, lot| ok && @@sv.lot_info(lot).opNo != opNo}
      }, "lots #{lots.inspect} have not been processed successfully on #{eqp}"
      # clean up and verify scenario log
      @@vaptest.stop_test(eqp)
      $log.info 'verifying scenario log'
      assert wait_for(timeout: 120) {
        @@vaptest.vapdb.scenariolog(tstart: tstart,
          scenario: 'AutoPull%', tgteqp: eqp, msg: '%Batch-Reservation (stocker-to-tool) done%').first
      }, "scenario error with #{eqp.inspect}"
    }
  end

end
