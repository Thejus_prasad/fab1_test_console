=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.


Author: Steffen Steidten, 2014-03-03

History:
  2015-03-13 ssteidte, more reliable test preparation
  2018-05-24 sfrieske, minor cleanup, use @@vaptest instead of $vaptest
  2019-02-22 sfrieske, minor cleanup, renamed from Test_VAMOS.. to VAMOS...
  2021-07-13 sfrieske, removed use of prepare_separate_child
=end

require_relative 'VAMOS_AutoProc'


# AutoPullTakeIn scenarios
class VAMOS_AutoProc05 < VAMOS_AutoProc

  def test500_setup
    $setup_ok = false
    #
    assert_equal '1', @@sv.environment_variable[1]['SP_DELIVERY_TAKE_OUTIN_ENABLE_FLAG'], 'wrong MM server env variable'
    assert_equal 'OFF', @@sv.environment_variable[1]['CS_SP_CHECK_EARLYMOVINGFOUP_ON_UNLOAD'], 'wrong SiView env var'
    # find operation/tool combinations for the test cases
    assert res = @@vaptest.find_operation_eqp(ib: false, takeoutin: true, max_batchsize: 1), 'no eqp found'
    @@opNo_takeoutin, @@eqp_takeoutin = res
    #
    $setup_ok = true
  end

  def test501_takeoutin_SI
    # 2nd carrier is MI or SI
    assert lot = @@svtest.new_lot
    @@testlots << lot
    opNo = @@opNo_takeoutin
    assert_equal 0,  @@sv.lot_opelocate(lot, opNo)
    li = @@sv.lot_info(lot)
    # create a child lot and move it into a separate carrier
    assert lc = @@sv.lot_split(lot, 2), 'error splitting lot'
    assert c = @@svtest.get_empty_carrier, 'no empty carrier found'
    assert_equal 0, @@sv.wafer_sort(lc, c)
    assert_equal 0, @@sv.carrier_xfer_status_change(c, '', @@svtest.stocker)
    # only one port enabled
    eqp = @@vaptest.operations[opNo]
    @@vaptest.rtdremote.add_emustation(eqp, method: 'empty_reply')
    assert @@vaptest.prepare_eqp(eqp, mode: 'Auto-3', portstatus: 'Down')
    port = @@sv.eqp_info(eqp).ports.first.port
    assert_equal 0, @@sv.eqp_port_status_change(eqp, port, 'LoadReq'), 'error switching port status'
    #
    # process the first lot, don't yet unload carrier
    tstart = Time.now
    assert @@sv.claim_process_lot(lot, eqp: eqp, port: port, mode: 'Auto-3', nounload: true), 'error processing lot'
    #
    # register RTD rule for 2nd lot and switch port to UnloadReq
    @@vaptest.set_rtdemu_filter(lc, eqp)
    assert_equal 0, @@sv.eqp_port_status_change(eqp, port, 'UnloadReq'), 'error switching port status'
    # wait for reservation, ensuring the xfer job still exists
    assert wait_for {
      @@vaptest.mdswrite.write_eqpwip(eqp, 1)
      !@@sv.lot_info(lc).cj.empty?
    }, "AutoProc did not call #{eqp}"
    # verify xfer jobs have been created
    assert wait_for {!@@sv.carrier_xferjob(li.carrier).empty?}, "no xfer job for #{li.carrier}"
    assert wait_for {@@sv.carrier_xferjob(c).empty?}, "no xfer job for #{c}"
    #
    # verify scenario
    assert wait_for {
      @@vaptest.vapdb.scenariolog(tstart: tstart,
        scenario: 'AutoPullTakeIn', carrier: c, tgteqp: eqp, msg: 'Transfer done.').first
    }
  end

  def test502_takeoutin_EarlyMovingFOUP
    # 2nd carrier is MI or SI, eqp UDATA EarlyMovingFoup YES
    assert lot = @@svtest.new_lot
    @@testlots << lot
    opNo = @@opNo_takeoutin
    assert_equal 0,  @@sv.lot_opelocate(lot, opNo), 'error locating lot'
    eqp = @@vaptest.operations[opNo]
    # set Udata for AutoProc!
    assert @@vaptest.svtest.sm.update_udata(:eqp, eqp, {'EarlyMovingFOUP'=>'YES'}), 'error writing SM UDATA'
    li = @@sv.lot_info(lot)
    # create a child lot and move it info a separate carrier
    assert lc = @@sv.lot_split(lot, 2), 'error splitting lot'
    assert c = @@svtest.get_empty_carrier, 'no empty carrier found'
    assert_equal 0, @@sv.wafer_sort(lc, c)
    assert_equal 0, @@sv.carrier_xfer_status_change(c, '', @@svtest.stocker)
    #
    # process the test lot at the test tool with port going to UnloadReq while lot is in Processing
    tstart = Time.now
    assert @@vaptest.prepare_eqp(eqp, mode: 'Auto-3', portstatus: 'Down')
    port = @@sv.eqp_info(eqp).ports.first.port
    assert_equal 0, @@sv.eqp_port_status_change(eqp, port, 'LoadReq'), 'error switching port status'
    assert @@sv.claim_process_lot(lot, eqp: eqp, port: port, mode: 'Auto-3', nounload: true, noopecomp: true), 'error processing lot'
    #
    # register RTD rule for 2nd lot, and and switch port to UnloadReq
    @@vaptest.set_rtdemu_filter(lc, eqp)
    assert_equal 0, @@sv.eqp_port_status_change(eqp, port, 'UnloadReq'), 'error switching port status'
    # wait for reservation
    assert wait_for {
      @@vaptest.mdswrite.write_eqpwip(eqp, 1)
      !@@sv.lot_info(lc).cj.empty?
    }, "AutoProc did not call #{eqp}"
    # verify xfer jobs have been created
    assert wait_for {!@@sv.carrier_xferjob(li.carrier).empty?}, "no xfer job for #{li.carrier}"
    assert wait_for {!@@sv.carrier_xferjob(c).empty?}, "no xfer job for #{c}"
    #
    # verify scenario
    assert wait_for {
      @@vaptest.vapdb.scenariolog(tstart: tstart,
        scenario: 'AutoPullTakeIn', carrier: c, tgteqp: eqp, msg: 'Transfer done.').first
    }
  end

  def test503_takeoutin_SO
    # 2nd carrier is SO
    assert lot = @@svtest.new_lot
    @@testlots << lot
    opNo = @@opNo_takeoutin
    assert_equal 0,  @@sv.lot_opelocate(lot, opNo)
    eqp = @@vaptest.operations[opNo]
    # stockers = @@sv.eqp_info(eqp).stockers
    stockers = @@sv.eqp_info_raw(eqp).equipmentStockerInfo.strEqpStockerStatus.collect {|s| s.stockerID.identifier}
    assert stockers.size > 1, "eqp #{eqp} must have at least 2 stockers configured"
    # create a child lot and move it info a separate carrier
    assert lc = @@sv.lot_split(lot, 2), 'error splitting lot'
    assert c = @@svtest.get_empty_carrier, 'no empty carrier found'
    assert_equal 0, @@sv.wafer_sort(lc, c)
    assert_equal 0, @@sv.carrier_xfer_status_change(c, '', @@svtest.stocker)
    # only one port enabled,
    ##@@vaptest.rtdremote.add_emustation(eqp, method: 'empty_reply')
    assert @@vaptest.prepare_eqp(eqp, mode: 'Auto-3', portstatus: 'Down')
    port = @@sv.eqp_info(eqp).ports.first.port
    assert_equal 0, @@sv.eqp_port_status_change(eqp, port, 'LoadReq'), 'error switching port status'
    #
    # process the first lot, don't yet unload carrier
    tstart = Time.now
    assert @@sv.claim_process_lot(lot, eqp: eqp, port: port, mode: 'Auto-3', nounload: true), 'error processing lot'
    #
    # register RTD rule for 2nd lot, create an xfer job for 2nd carrier and switch load port to UnloadReq
    @@vaptest.set_rtdemu_filter(lc, eqp)
    assert_equal 0, @@sv.carrier_xfer(c, @@svtest.stocker, '', stockers[1], '')
    assert_equal 0, @@sv.eqp_port_status_change(eqp, port, 'UnloadReq'), 'error switching port status'
    # wait for reservation, ensuring the xfer job still exists
    assert wait_for {
      @@vaptest.mdswrite.write_eqpwip(eqp, 1)
      ($log.warn "no xfer job for #{c}"; false) if @@sv.carrier_xferjob(c).empty?
      !@@sv.lot_info(lc).cj.empty?
    }, "AutoProc did not call #{eqp}"
    #
    # verify scenario
    assert wait_for {
      @@vaptest.vapdb.scenariolog(tstart: tstart,
        scenario: 'AutoPullTakeIn', carrier: c, tgteqp: eqp, msg: 'Transfer done.').first
    }
  end

  def test504_takeoutin_EI
    # 2nd carrier is still EI on the previous tool (which must have E2E enabled for SLR to work)
    assert lot = @@svtest.new_lot
    @@testlots << lot
    opNo = @@opNo_takeoutin
    assert_equal 0,  @@sv.lot_opelocate(lot, opNo)
    eqp = @@vaptest.operations[opNo]
    opNo0 = @@vaptest.operations.keys[@@vaptest.operations.keys.index(opNo) - 1]
    eqp0 = @@vaptest.operations[opNo0]
    assert @@sv.eqp_info_raw(eqp0).equipmentBRInfo.eqpToEqpTransferFlag, "tool #{eqp0} must have E2E enabled"
    # create a child lot and move it into a separate carrier
    assert lc = @@sv.lot_split(lot, 2), 'error splitting lot'
    assert c = @@svtest.get_empty_carrier, 'no empty carrier found'
    assert_equal 0, @@sv.wafer_sort(lc, c)
    assert_equal 0, @@sv.carrier_xfer_status_change(c, '', @@svtest.stocker)
    assert_equal 0, @@sv.lot_opelocate(lc, opNo0)
    # process the child lot at the previous tool, don't yet unload carrier
    port0 = @@sv.eqp_info(eqp0).ports.first.port
    assert @@vaptest.prepare_eqp(eqp0, mode: 'Auto-3', portstatus: 'LoadReq')
    assert @@sv.claim_process_lot(lc, eqp: eqp0, port: port0, mode: 'Auto-3', nounload: true), 'error processing lot'
    #
    # process the test lot at the test tool, don't yet unload carrier
    assert @@vaptest.prepare_eqp(eqp, mode: 'Auto-3', portstatus: 'Down')
    port = @@sv.eqp_info(eqp).ports.first.port
    assert_equal 0, @@sv.eqp_port_status_change(eqp, port, 'LoadReq'), 'error switching port status'
    assert @@sv.claim_process_lot(lot, eqp: eqp, port: port, mode: 'Auto-3', nounload: true), 'error processing lot'
    # prevent an AutoPush scenario for lc on eqp0
    wntarget = RTD::WhereNextVapTarget.new($env, @@sv.lot_info(lot).carrier, stocker: @@svtest.stocker, sv: @@sv)
    @@vaptest.rtdremote.add_emustation('VAMOS_WN_A', rule: 'WhereNextEquipment', method: wntarget)
    # register RTD rule for 2nd lot
    @@vaptest.set_rtdemu_filter(lc, eqp)
    tstart = Time.now
    # switch load port of the first tool (child lot) to UnloadReq and load port of the test tool to UnloadReq
    assert_equal 0, @@sv.eqp_port_status_change(eqp, port, 'UnloadReq'), 'error switching port status'
    assert_equal 0, @@sv.eqp_port_status_change(eqp0, port0, 'UnloadReq'), 'error switching port status'
    # wait for reservation
    assert wait_for(timeout: 180) {
      @@vaptest.mdswrite.write_eqpwip(eqp, 1)
      !@@sv.lot_info(lc).cj.empty?
    }, "AutoProc did not call #{eqp}"
    #
    # verify xfer job has been created
    $log.info "waiting for carrier xfer job"
    assert wait_for {!@@sv.carrier_xferjob(c).empty?}, "no xfer job for #{c}"
    #
    # verify scenario
    $log.info "verifying scenario log"
    sclog = nil
    assert wait_for {
      sclog = @@vaptest.vapdb.scenariolog(tstart: tstart,
        scenario: 'AutoPullTakeIn%', carrier: c, srceqp: "%#{eqp0}%", tgteqp: eqp).first
    }
    assert_equal 'Transfer done.', sclog.msg, 'wrong msg'
  end

  def test505_takeoutin_EO
    # 2nd carrier is EO on the previous tool (SiView error)
    assert lot = @@svtest.new_lot
    @@testlots << lot
    opNo = @@opNo_takeoutin
    assert_equal 0,  @@sv.lot_opelocate(lot, opNo)
    eqp = @@vaptest.operations[opNo]
    opNo0 = @@vaptest.operations.keys[@@vaptest.operations.keys.index(opNo) - 1]
    eqp0 = @@vaptest.operations[opNo0]
    # create a child lot and move it info a separate carrier
    assert lc = @@sv.lot_split(lot, 2), 'error splitting lot'
    assert c = @@svtest.get_empty_carrier, 'no empty carrier found'
    assert_equal 0, @@sv.wafer_sort(lc, c)
    assert_equal 0, @@sv.carrier_xfer_status_change(c, '', @@svtest.stocker)
    # child lot's carrier is EO at the previous tool
    assert_equal 0, @@sv.carrier_xfer_status_change(c, 'EO', eqp0)
    #
    # process the test lot at the test tool, don't yet unload carrier
    ##@@vaptest.rtdremote.add_emustation(eqp, method: 'empty_reply')
    assert @@vaptest.prepare_eqp(eqp, mode: 'Auto-3', portstatus: 'Down')
    port = @@sv.eqp_info(eqp).ports.first.port
    assert_equal 0, @@sv.eqp_port_status_change(eqp, port, 'LoadReq'), 'error switching port status'
    assert @@sv.claim_process_lot(lot, eqp: eqp, port: port, mode: 'Auto-3', nounload: true), 'error processing lot'
    #
    # register RTD rule for 2nd lot, switch load port of the test tool to UnloadReq
    tstart = Time.now
    @@vaptest.set_rtdemu_filter(lc, eqp)
    assert_equal 0, @@sv.eqp_port_status_change(eqp, port, 'UnloadReq'), 'error switching port status'
    #
    # wait for scenario call
    sclog = nil
    assert wait_for {
      @@vaptest.mdswrite.write_eqpwip(eqp, 1)
      sclog = @@vaptest.vapdb.scenariolog(tstart: tstart,
        scenario: 'AutoPullTakeIn', carrier: c, srceqp: "%#{eqp0}%", tgteqp: eqp).first
    }, "wrong AutoProc scenario"
    assert_equal 'Reservation failed.', sclog.msg, 'wrong msg'
    assert_equal '905', sclog.txrc, 'wrong reason code (wrong transfer status)'
    assert_empty @@sv.lot_info(lc).cj, "wrong reservation of lot #{lc}"
  end

  def test506_notakeoutin_multiple_ports
    # if more than 1 port is available the other one is taken (no takeoutin reservation)
    assert lot = @@svtest.new_lot
    @@testlots << lot
    opNo = @@opNo_takeoutin
    assert_equal 0,  @@sv.lot_opelocate(lot, opNo)
    li = @@sv.lot_info(lot)
    # create a child lot and move it into a separate carrier
    assert lc = @@sv.lot_split(lot, 2), 'error splitting lot'
    assert c = @@svtest.get_empty_carrier, 'no empty carrier found'
    assert_equal 0, @@sv.wafer_sort(lc, c)
    assert_equal 0, @@sv.carrier_xfer_status_change(c, '', @@svtest.stocker)
    # all ports enabled
    eqp = @@vaptest.operations[opNo]
    @@vaptest.rtdremote.add_emustation(eqp, method: 'empty_reply')
    assert @@vaptest.prepare_eqp(eqp, mode: 'Auto-3', portstatus: 'LoadReq')
    port = @@sv.eqp_info(eqp).ports.first.port
    assert_equal 0, @@sv.eqp_port_status_change(eqp, port, 'LoadReq'), 'error switching port status'
    #
    # process the first lot, don't yet unload carrier
    tstart = Time.now
    assert @@sv.claim_process_lot(lot, eqp: eqp, port: port, mode: 'Auto-3', nounload: true), 'error processing lot'
    #
    # register RTD rule for 2nd lot and switch port to UnloadReq
    @@vaptest.set_rtdemu_filter(lc, eqp)
    assert_equal 0, @@sv.eqp_port_status_change(eqp, port, 'UnloadReq'), 'error switching port status'
    # wait for reservation, ensuring the xfer job still exists
    assert wait_for {
      @@vaptest.mdswrite.write_eqpwip(eqp, 1)
      !@@sv.lot_info(lc).cj.empty?
    }, "AutoProc did not call #{eqp}"
    assert cj_port = @@sv.eqp_info(eqp).ports.find {|p| p.rsv_carrier == c}, 'cannot find port for cj'
    refute_equal port, cj_port.port, 'wrong reservation port'
    # verify xfer jobs have been created
    assert wait_for {!@@sv.carrier_xferjob(li.carrier).empty?}, "no xfer job for #{li.carrier}"
    assert wait_for {!@@sv.carrier_xferjob(c).empty?}, "no xfer job for #{c}"
    #
    # verify scenario
    assert wait_for {
      # logged as AutoPullTakeIn although no takeoutin reservation is done
      @@vaptest.vapdb.scenariolog(tstart: tstart,
        scenario: 'AutoPullTakeIn', carrier: c, tgteqp: eqp, msg: 'Transfer done.').first
    }
  end

  def test507_notakeoutin_auto2  # as of 6.4
    # no takeoutin if eqp mode is Auto-2
    assert lot = @@svtest.new_lot
    @@testlots << lot
    opNo = @@opNo_takeoutin
    assert_equal 0,  @@sv.lot_opelocate(lot, opNo)
    li = @@sv.lot_info(lot)
    # create a child lot and move it into a separate carrier
    assert lc = @@sv.lot_split(lot, 2), 'error splitting lot'
    assert c = @@svtest.get_empty_carrier, 'no empty carrier found'
    assert_equal 0, @@sv.wafer_sort(lc, c)
    assert_equal 0, @@sv.carrier_xfer_status_change(c, '', @@svtest.stocker)
    # only one port enabled
    eqp = @@vaptest.operations[opNo]
    @@vaptest.rtdremote.add_emustation(eqp, method: 'empty_reply')
    assert @@vaptest.prepare_eqp(eqp, mode: 'Auto-2', portstatus: 'Down')
    port = @@sv.eqp_info(eqp).ports.first.port
    # process the first lot, don't yet unload carrier
    tstart = Time.now
    assert_equal 0, @@sv.eqp_port_status_change(eqp, port, 'LoadReq'), 'error switching port status'
    assert @@sv.claim_process_lot(lot, eqp: eqp, port: port, mode: 'Auto-2', nounload: true), 'error processing lot'
    # register RTD rule for 2nd lot and switch port to UnloadReq
    @@vaptest.set_rtdemu_filter(lc, eqp)
    assert_equal 0, @@sv.eqp_port_status_change(eqp, port, 'UnloadReq'), 'error switching port status'
    # wait for AutoPush of 1st carrier
    assert wait_for {
      @@vaptest.mdswrite.write_eqpwip(eqp, 1)
      @@vaptest.vapdb.scenariolog(tstart: tstart, scenario: 'AutoPush', carrier: li.carrier, srceqp: eqp).first
    }, "wrong AutoProc scenario"
    # verify no takeoutin for 2nd carrier
    assert sclog = @@vaptest.vapdb.scenariolog(tstart: tstart, scenario: 'AutoPullTakeIn', carrier: c, tgteqp: eqp).first
    assert_equal 'No free port.', sclog.msg, 'no AutoPullTakeIn expected'
    assert_empty @@sv.lot_info(lc).cj, "wrong reservation of lot #{lc}"
  end

end
