=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: Steffen Steidten, 2013-08-26

Version: 6.12

History:
  2015-04-28 ssteidte, added lot hold tests
  2018-03-06 sfrieske, replaced user_parameter_set_verify by user_parameter_change
  2018-05-24 sfrieske, use @@vaptest instead of $vaptest, remove Corba client tests
  2019-02-22 sfrieske, minor cleanup, renamed from Test_VAMOS.. to VAMOS...
  2021-07-23 sfrieske, inherit from SiViewTestCase instad of VAMOS_AutoProc, more cleanup

Notes:
  see https://gfoundries.sharepoint.com/sites/AutoProc
  VamosDisplay (ITDC): http://vf36vamosd1:9089/vamos/vamosdsp.jsf
  endurance tests: runtest 'VAMOS_ToolDisplay', 'itdc', tp: /test00|test21/, parameters: {loops: 1000}
=end

require 'rtdserver/rtdemuremote'
require 'vamos/vamos'
require 'SiViewTestCase'


class VAMOS_ToolDisplay < SiViewTestCase
  @@dev = false  # must stay false, connect to dev instances by passing parameters: {dev: true}
  @@url = nil #'http://drsvitd2w71054:8080/vamos/intBuffToolDsp.jsf'  # nil  #'http://f1ws11:9080/vamos/vamosdsp.jsf' # for dev tests in ITDC
  @@sv_defaults = {carrier_category: 'BEOL', carriers: '%', route: 'A-AP-MAINPD.02', product: 'A-AP-PRODUCT.02'}
  @@eqp = 'UTA001'
  @@opNo = '1000.1000'
  @@badcarrier = 'QAQA01'    # must be newly created having neither eqp nor stocker info

  @@toolevent_delay = 12  # 10 s polling
  @@mdsevent_delay = 25
  @@loops = 1  # normally 1, for endurance tests: 100 (~8h) or 1000 (~3d)

  @@testlots = []


  def self.shutdown
    @@svtest.delete_registered_carrier(@@badcarrier)
    # @@sv.user_parameter_change('Eqp', @@eqp, {'Pull'=>'', 'Push'=>''})
    @@testlots.each {|lot| @@sv.delete_lot_family(lot)}
  end


  def test00_setup
    $setup_ok = false
    #
    @@rtdremote = RTD::EmuRemote.new($env)
    @@rtdremote.add_emustation(@@eqp, method: 'empty_reply')  # suppress AutoProc activities
    #
    # prepare clean lots
    assert @@testlots = @@svtest.new_lots(2), 'error creating test lots'
    #
    # set a lot hold to an unused lot in a carrier without eqp/transport eqp, to check the server is not confused by it
    if @@sv.carrier_list(carrier: @@badcarrier).empty?
      @@sv.durable_register('Cassette', @@badcarrier, 'FEOL')
    else
      @@sv.lot_list_incassette(@@badcarrier).each {|lot| assert_equal 0, @@sv.wafer_sort(lot, '')}
    end
    assert_equal 0, @@sv.carrier_status_change(@@badcarrier, 'AVAILABLE', check: true)
    lot = @@testlots[0]
    assert_equal 0, @@sv.wafer_sort(lot, @@badcarrier)
    assert_equal 0, @@sv.lot_hold(lot, nil)
    #
    @@lot = @@testlots[1]
    @@carrier = @@sv.lot_info(@@lot).carrier
    eqpi = @@sv.eqp_info(@@eqp)
    assert @@chamber = eqpi.chambers.first.chamber, "#{@@eqp} has no chambers"
    assert @@port = eqpi.ports.first.port
    # assert_equal 0, @@sv.user_parameter_change('Eqp', @@eqp, {'Pull'=>@@port, 'Push'=>@@port})
    #
    @@td = Vamos::ToolDisplay.new(@@dev ? 'dev' : $env, @@eqp, @@port, url: @@url)
    @@td.start_browser
    #
    $setup_ok = true
  end

  def test11_EImsgs
    # preparation
    assert_equal 0, @@sv.lot_cleanup(@@lot, opNo: @@opNo, stocker: @@svtest.stocker)
    assert @@svtest.cleanup_eqp(@@eqp, mode: 'Auto-1')
    li = @@sv.lot_info(@@lot, wafers: true)
    @@td.material_removed(@@carrier, li.wafers)
    $log.info "waiting #{@@toolevent_delay} s for the MDS to sync"; sleep @@toolevent_delay
    puts "\nOpen a VAMOS Display for #{@@eqp} #{@@port} and press ENTER!"
    gets
    #
    @@loops.times {|i|
      $log.info "\n\ntest #{i+1}/#{@@loops}\n"
      # SLR, carrier and its stocker show up at top of the reserved port in yellow
      assert cj = @@sv.slr(@@eqp, port: @@port, lot: @@lot), 'error reserving lot'
      sleep @@toolevent_delay
      # load carrier, upper area turns grey
      assert_equal 0, @@sv.carrier_xfer_status_change(@@carrier, 'MO', @@svtest.stocker)
      assert_equal 0, @@sv.eqp_port_status_change(@@eqp, @@port, 'LoadComp')
      sleep @@toolevent_delay
      # material_arrived message carrier, lots and slots, recipe show up in center area
      @@td.material_arrived(@@carrier, li.wafers, cj: cj)
      sleep @@toolevent_delay
      # eqp load and opestart don't change the picture
      assert_equal 0, @@sv.eqp_load(@@eqp, @@port, @@carrier)
      assert cj = @@sv.eqp_opestart(@@eqp, @@carrier)
      # 1st wafer_started, undermost slot turns yellow
      @@td.wafer_started(@@carrier, [li.wafers.first])
      sleep @@toolevent_delay
      # last wafer_started, topmost slot turns yellow
      @@td.wafer_started(@@carrier, [li.wafers.last])
      sleep @@toolevent_delay
      # last wafer completed, topmost slot turns green
      @@td.wafer_completed(@@carrier, [li.wafers.last])
      sleep @@toolevent_delay
      # 1st wafer completed, undermost slot turns green
      @@td.wafer_completed(@@carrier, [li.wafers.first])
      sleep @@toolevent_delay
      #
      # running hold, lot turns red
      assert_equal 0, @@sv.running_hold(@@lot, @@eqp, cj)
      sleep @@mdsevent_delay
      # release hold
      assert_equal 0, @@sv.lot_hold_release(@@lot, nil)
      sleep @@mdsevent_delay
      #
      # complete all (other) wafers, all slots are green
      li.wafers.each {|w| @@td.wafer_completed(@@carrier, [w])}
      sleep @@toolevent_delay
      # opecomp, carrier goes away from upper port zone
      assert_equal 0, @@sv.eqp_opecomp(@@eqp, cj)
      sleep @@toolevent_delay
      #
      # lot hold, lot turns red
      assert_equal 0, @@sv.lot_hold(@@lot, nil)
      sleep @@mdsevent_delay
      assert_equal 0, @@sv.lot_hold_release(@@lot, nil)
      sleep @@mdsevent_delay
      #
      # port status change, a red "Down" shows up
      @@sv.eqp_port_status_change(@@eqp, @@port, 'Down')
      sleep @@mdsevent_delay
      # unloadreq, carrier shows up in lower zone in yellow, main area starts to blink yellow after a while
      assert_equal 0, @@sv.eqp_port_status_change(@@eqp, @@port, 'UnloadReq')
      sleep @@mdsevent_delay
      # unload carrier (no change) and material removed, carrier goes away from central zone, blinking stops
      assert_equal 0, @@sv.eqp_unload(@@eqp, @@port, @@carrier)
      @@td.material_removed(@@carrier, li.wafers, cj: cj)
      sleep @@toolevent_delay
      # cleanup
      assert_equal 0, @@sv.lot_cleanup(@@lot, opNo: @@opNo, stocker: @@svtest.stocker)
      assert_equal 0, @@sv.eqp_cleanup(@@eqp)
    }
  end

  def test12_E10
    puts "\nWatch the VAMOS Display for #{@@eqp} #{@@port} for E10 state changes (press ENTER to continue)!"
    gets
    assert @@svtest.cleanup_eqp(@@eqp, mode: 'Auto-1')
    @@sv.chamber_status_change(@@eqp, @@chamber, '2NDP')
    @@sv.chamber_status_change(@@eqp, @@chamber, '2WPR')
    @@sv.eqp_status_change(@@eqp, '2NDP')
    @@sv.eqp_status_change(@@eqp, '2WPR')
    # turn chamber and tool state backgrounds red, blue, yellow, green, yellow
    colors = {'5DLY'=>'red', '2NDP'=>'blue', '2WPR'=>'yellow', '1PRD'=>'green'}
    %w(5DLY 2NDP 2WPR 1PRD 2WPR).each {|s|
      assert_equal 0, @@sv.chamber_status_change(@@eqp, @@chamber, s)
      assert_equal 0, @@sv.eqp_status_change(@@eqp, s)
      status = @@sv.eqp_info(@@eqp).status
      color = colors[status.split('.').last]
      $log.info "status: #{status}, color: #{color}"
      sleep @@mdsevent_delay
    }
  end

end
