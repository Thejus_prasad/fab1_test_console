=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.


Author: Steffen Steidten, 2014-03-03

History:
  2015-03-13 ssteidte, more reliable test preparation
  2018-05-24 sfrieske, minor cleanup, use @@vaptest instead of $vaptest
  2019-02-22 sfrieske, minor cleanup, renamed from Test_VAMOS.. to VAMOS...
  2021-01-29 sfrieske, removed use of prepare_separate_child
=end

require_relative 'VAMOS_AutoProc'


# Multi-lot scenarios
class VAMOS_AutoProc03 < VAMOS_AutoProc
  @@opNo_procmon = '1000.1000'


  def test300_setup
    $setup_ok = false
    #
    # find operation/tool combinations for the test cases
    assert res = @@vaptest.find_operation_eqp(ib: false, max_batchsize: 1), 'no eqp found'
    @@opNo_fb, @@eqp_fb = res
    assert res = @@vaptest.find_operation_eqp(ib: true, mrtype: 'Multiple Recipe'), 'no eqp found'
    @@opNo_ib, @@eqp_ib = res
    assert res = @@vaptest.find_operation_eqp(ib: true, max_batchsize: 2), 'no eqp found'
    @@opNo_ib_batch2, @@eqp_ib_batch2 = res
    assert res = @@vaptest.find_operation_eqp(ib: true, procmon: true), 'no eqp found'
    @@opNo_ib_procmon, @@eqp_ib_procmon = res
    #
    $setup_ok = true
  end


  # lots in the same carrier

  def testman301_multilot_separate # manual test, optional
    assert lot = @@svtest.new_lot, 'error creating test lot'
    @@testlots << lot
    assert lc = @@sv.lot_split(lot, 5), 'error splitting lot'
    [@@opNo_fb, @@opNo_ib].each {|opNo|
      [lot, lc].each {|l| assert_equal 0, @@sv.lot_cleanup(l, opNo: opNo, stocker: @@svtest.stocker, xjs: true)}
      # move the child lot away from the lot's operation
      assert_equal 0, @@sv.lot_opelocate(lc, 1)
      eqp = @@vaptest.operations[opNo]
      assert @@vaptest.prepare_eqp(eqp, mode: 'Auto-3')
      lci = @@sv.lot_info(lc)
      assert @@vaptest.eis.set_variable(eqp, 'logincomingstructs', true)
      # register RTD emulator rule, wait for AutoProc to reserve the lots
      tstart = Time.now
      @@vaptest.set_rtdemu_filter(lot, eqp)
      assert wait_for(timeout: 400) {
        @@vaptest.mdswrite.write_eqpwip(eqp, 1)
        @@sv.lot_info(lot).opNo != opNo
      }, "AutoProc did not call #{eqp}"
      assert @@vaptest.eis.set_variable(eqp, 'logincomingstructs', false)
      @@vaptest.stop_test(eqp)
      # verify only lot has been processed
      sleep 3
      assert_equal lci.opNo, @@sv.lot_info(lc).opNo, "child lot #{lc} has been processed erroneously"
      $log.info "Verify SLR operationStartFlags in the JavaEI log file for #{eqp}, #{lot} and press ENTER!"
      gets
    }
  end

  def test302_multilot_simultanously
    assert lot = @@svtest.new_lot, 'error creating test lot'
    @@testlots << lot
    assert lc = @@sv.lot_split(lot, 5), 'error splitting lot'
    [@@opNo_fb, @@opNo_ib].each {|opNo|
      [lot, lc].each {|l| assert_equal 0, @@sv.lot_cleanup(l, opNo: opNo, stocker: @@svtest.stocker, xjs: true)}
      eqp = @@vaptest.operations[opNo]
      assert @@vaptest.prepare_eqp(eqp, mode: 'Auto-3')
      # register RTD emulator rule, wait for AutoProc to reserve the lot
      tstart = Time.now
      @@vaptest.set_rtdemu_filter([lot, lc], eqp)
      assert wait_for(timeout: 400) {
        @@vaptest.mdswrite.write_eqpwip(eqp, 1)
        !@@sv.lot_info(lot).cj.empty?
      }, "AutoProc did not call #{eqp}"
      @@vaptest.stop_test(eqp)
      # verify lots have the same cj
      assert_equal @@sv.lot_info(lot).cj, @@sv.lot_info(lc).cj, 'different cjs'
      # verify both lots have been processed
      assert wait_for {@@sv.lot_info(lot).opNo != opNo}, "error processing lot #{lot}"
      assert wait_for {@@sv.lot_info(lc).opNo != opNo}, "error processing lot #{lc}"
    }
  end

  # lots in different carriers

  def test311_multifoup_fb
    assert lot = @@svtest.new_lot, 'error creating test lot'
    @@testlots << lot
    opNo = @@opNo_fb
    assert_equal 0,  @@sv.lot_opelocate(lot, opNo)
    eqp = @@vaptest.operations[opNo]
    assert @@vaptest.prepare_eqp(eqp, mode: 'Auto-3')
    # create a child lot and move it info a separate carrier
    assert lc = @@sv.lot_split(lot, 5), 'error splitting lot'
    assert c = @@svtest.get_empty_carrier, 'no empty carrier found'
    assert_equal 0, @@sv.wafer_sort(lc, c)
    assert_equal 0, @@sv.carrier_xfer_status_change(c, '', @@svtest.stocker, manualin: true)
    # register RTD emulator rule, wait for AutoProc to reserve the lots
    tstart = Time.now
    @@vaptest.set_rtdemu_filter([lot, lc], eqp)
    assert wait_for(timeout: 400) {
      @@vaptest.mdswrite.write_eqpwip(eqp, 1)
      !@@sv.lot_info(lot).cj.empty? && !@@sv.lot_info(lc).cj.empty?
    }, "AutoProc did not call #{eqp}"
    # verify lots have the same cj
    refute_equal @@sv.lot_info(lot).cj, @@sv.lot_info(lc).cj, 'same cj'
    # verify both lots have been processed
    assert wait_for {@@sv.lot_info(lot).opNo != opNo}, "error processing lot #{lot}"
    assert wait_for {@@sv.lot_info(lc).opNo != opNo}, "error processing lot #{lc}"
  end

  def test312_multifoup_fb_1pg
    assert lot = @@svtest.new_lot, 'error creating test lot'
    @@testlots << lot
    opNo = @@opNo_fb
    assert_equal 0,  @@sv.lot_opelocate(lot, opNo)
    eqp = @@vaptest.operations[opNo]
    assert @@vaptest.prepare_eqp(eqp, mode: 'Auto-3', portstatus: 'Down')
    assert_equal 0, @@sv.eqp_port_status_change(eqp, @@sv.eqp_info(eqp).ports.first.port, 'LoadReq'), 'error switching port status'
    # create a child lot and move it into a separate carrier
    assert lc = @@sv.lot_split(lot, 5), 'error splitting lot'
    assert c = @@svtest.get_empty_carrier, 'no empty carrier found'
    assert_equal 0, @@sv.wafer_sort(lc, c)
    assert_equal 0, @@sv.carrier_xfer_status_change(c, '', @@svtest.stocker, manualin: true)
    # register RTD emulator rule, wait for AutoProc to reserve the parent lot
    tstart = Time.now
    @@vaptest.set_rtdemu_filter([lot, lc], eqp)
    assert wait_for(timeout: 400) {
      @@vaptest.mdswrite.write_eqpwip(eqp, 1)
      !@@sv.lot_info(lot).cj.empty?
    }, "AutoProc did not call #{eqp}"
    # verify child lot has no cj
    assert_equal '', @@sv.lot_info(lc).cj, "#{lc} has a cj"
    # verify lot has been processed
    assert wait_for {@@sv.lot_info(lot).opNo != opNo}, "error processing lot #{lot}"
    # verify lc gets processed
    assert wait_for(timeout: 400) {
      @@vaptest.mdswrite.write_eqpwip(eqp, 1)
      !@@sv.lot_info(lc).cj.empty?
    }, "AutoProc did not call #{eqp}"
    # verify lot has been processed
    assert wait_for {@@sv.lot_info(lc).opNo != opNo}, "error processing lot #{lc}"
  end

  def test321_multifoup_ib
    assert lot = @@svtest.new_lot, 'error creating test lot'
    @@testlots << lot
    opNo = @@opNo_ib
    assert_equal 0,  @@sv.lot_opelocate(lot, opNo)
    eqp = @@vaptest.operations[opNo]
    assert @@vaptest.prepare_eqp(eqp, mode: 'Auto-3')
    # create a child lot and move it into a separate carrier
    assert ec = @@svtest.get_empty_carrier(stockin: true), 'no empty carrier'
    assert lc = @@sv.lot_split(lot, 2), 'error splitting lot'
    assert_equal 0, @@sv.wafer_sort(lc, ec)
    # register RTD emulator rule, wait for AutoProc to reserve the lots
    tstart = Time.now
    @@vaptest.set_rtdemu_filter([lot, lc], eqp)
    assert wait_for(timeout: 400) {
      @@vaptest.mdswrite.write_eqpwip(eqp, 1)
      !@@sv.lot_info(lot).cj.empty? && !@@sv.lot_info(lc).cj.empty?
    }, "AutoProc did not call #{eqp}"
    # verify lots have the same cj
    assert_equal @@sv.lot_info(lot).cj, @@sv.lot_info(lc).cj, 'different cjs'
    # verify both lots have been processed
    assert wait_for {@@sv.lot_info(lot).opNo != opNo}, "error processing lot #{lot}"
    assert wait_for {@@sv.lot_info(lc).opNo != opNo}, "error processing lot #{lc}"
  end

  def test322_multifoup_ib_batchsize
    assert lot = @@svtest.new_lot, 'error creating test lot'
    @@testlots << lot
    opNo = @@opNo_ib_batch2
    assert_equal 0,  @@sv.lot_opelocate(lot, opNo)
    eqp = @@vaptest.operations[opNo]
    assert @@vaptest.prepare_eqp(eqp, mode: 'Auto-3', portstatus: 'Down')
    # only one port enabled
    assert_equal 0, @@sv.eqp_port_status_change(eqp, @@sv.eqp_info(eqp).ports.first.port, 'LoadReq'), 'error switching port status'
    # create 2 child lots and move them into separate carriers
    lcs = []
    2.times {
      assert ec = @@svtest.get_empty_carrier(stockin: true), 'no empty carrier'
      assert lc = @@sv.lot_split(lot, 1), 'error splitting lot'
      assert_equal 0, @@sv.wafer_sort(lc, ec)
      lcs << lc
    }
    # register RTD emulator rule, wait for AutoProc to reserve the first 2 lots
    tstart = Time.now
    @@vaptest.set_rtdemu_filter(lcs + [lot], eqp)
    assert wait_for(timeout: 400) {
      @@vaptest.mdswrite.write_eqpwip(eqp, 1)
      lcs.inject(true) {|ok, lc| ok && !@@sv.lot_info(lc).cj.empty?}
    }, "AutoProc did not call #{eqp}"
    # verify 2 lots have the same cj and the other one has none
    assert_equal @@sv.lot_info(lcs[0]).cj, @@sv.lot_info(lcs[1]).cj, 'different cjs'
    assert_equal '', @@sv.lot_info(lot).cj, "lot #{lot} must have no cj"
    # verify both lots have been processed and the next one gets reserved
    assert wait_for {@@sv.lot_info(lcs[0]).opNo != opNo}, 'error processing lot'
    assert wait_for(timeout: 400) {
      @@vaptest.mdswrite.write_eqpwip(eqp, 1)
      !@@sv.lot_info(lot).cj.empty?
    }, "AutoProc did not call #{eqp}"
    refute_equal @@sv.lot_info(lot).cj, @@sv.lot_info(lcs[0]).cj
    refute_equal @@sv.lot_info(lot).cj, @@sv.lot_info(lcs[1]).cj
  end

  def test329_ib_procmon
    # note: lots must have 25 wafers to get the correct mltype in WhatsNext
    assert lot = @@svtest.new_lot(nwafers: 25), 'error creating test lot'
    @@testlots << lot
    opNo = @@opNo_ib_procmon
    assert_equal 0, @@sv.lot_opelocate(lot, opNo)
    assert procmon = @@svtest.new_controllot('Process Monitor', nwafers: 25)
    @@testlots << procmon
    assert_equal 0, @@sv.lot_opelocate(procmon, @@opNo_procmon)
    eqp = @@vaptest.operations[opNo]
    assert @@vaptest.prepare_eqp(eqp, mode: 'Auto-3')
    # register RTD emulator rule, wait for AutoProc to reserve the lot and monitor lot
    tstart = Time.now
    @@vaptest.set_rtdemu_filter(lot, eqp, procmon: procmon)
    cjs = nil
    assert wait_for(timeout: 400) {
      @@vaptest.mdswrite.write_eqpwip(eqp, 1)
      cjs = @@sv.eqp_info(eqp).cjs
      !cjs.empty?
    }, "AutoProc did not call #{eqp}"
    # verify process monitor is used correctly
    cjinfo = @@sv.lot_list_incontroljob(cjs)
    assert cjlot = cjinfo.first.strControlJobCassette.collect {|cjcarrier|
      cjcarrier.strControlJobLot.find {|cjlot| cjlot.lotID.identifier == lot}
    }.compact.first, 'lot not in cj'
    refute cjlot.monitorLotFlag, 'lot must not be flagged as monitor'
    assert cjlot = cjinfo.first.strControlJobCassette.collect {|cjcarrier|
      cjcarrier.strControlJobLot.find {|cjlot| cjlot.lotID.identifier == procmon}
    }.compact.first, 'procmon not in cj'
    assert cjlot.monitorLotFlag, 'procmon must be flagged as monitor'
  end

end
