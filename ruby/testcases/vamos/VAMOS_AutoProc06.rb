=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.


Author: Steffen Steidten, 2014-03-03

History:
  2015-03-13 ssteidte, more reliable test preparation
  2018-05-24 sfrieske, minor cleanup, use @@vaptest instead of $vaptest
  2019-02-22 sfrieske, minor cleanup, renamed from Test_VAMOS.. to VAMOS...
  2021-12-08 sfrieske, simplified MDS trans_job query
=end

require 'dummytools'
require 'misc/rtddb'
require_relative 'VAMOS_AutoProc'


# Other scenarios
class VAMOS_AutoProc06 < VAMOS_AutoProc
  @@stocker2 = 'UTSTO12'


  def teardown
    @@dummyamhs.set_variable('errormode', false)
  end


  def test600_setup
    $setup_ok = false
    #
    assert @@dummyamhs = Dummy::AMHS.new($env)
    assert @@rtddb = RTD::MDS.new($env)
    assert @@lot = @@svtest.new_lot, 'error creating test lot'
    @@testlots << @@lot
    #
    $setup_ok = true
  end

  def test601_rtd_error
    lot = @@lot
    opNo = @@vaptest.operations.keys.first
    eqp = @@vaptest.operations[opNo]
    assert_equal 0, @@sv.lot_cleanup(lot, opNo: opNo, stocker: @@svtest.stocker, xjs: true)
    assert @@vaptest.prepare_eqp(eqp, mode: 'Auto-3')
    # register RTD emulator rule with an ill formatted reply and wait for AutoProc
    tstart = Time.now
    assert @@vaptest.rtdremote.add_emustation(eqp) {|*args| ''}
    assert wait_for(timeout: 400) {
      @@vaptest.mdswrite.write_eqpwip(eqp, 1)
      @@vaptest.vapdb.scenariolog(tstart: tstart, scenario: '%Pull%', tgteqp: eqp, msg: 'RtdDispatchListInq failed%').first
    }, "AutoProc did not call #{eqp}"
  end

  def test602_cjcancel_fb
    lot = @@lot
    carrier = @@sv.lot_info(lot).carrier
    assert res = @@vaptest.find_operation_eqp(ib: false, max_batchsize: 1), 'no eqp found'
    opNo, eqp = res
    assert_equal 0, @@sv.lot_cleanup(lot, opNo: opNo, stocker: @@svtest.stocker, xjs: true)
    assert @@vaptest.prepare_eqp(eqp, mode: 'Auto-3')
    # register RTD emulator rule, wait for AutoProc to create an xfer job and delete it
    tstart = Time.now
    assert @@vaptest.set_rtdemu_filter(lot, eqp)
    assert wait_for(timeout: 400) {
      @@vaptest.mdswrite.write_eqpwip(eqp, 1)
      xj = @@sv.carrier_xferjob(carrier)
      @@sv.carrier_xferjob_delete(carrier) unless xj.empty?
      !xj.empty?
    }, 'no xfer job'
    @@vaptest.rtdremote.add_emustation(eqp, method: 'empty_reply')
    # verify lot and carrier status
    cj = @@sv.lot_info(lot).cj
    refute_empty  cj, "lot #{lot} has no cj"
    assert_empty @@sv.carrier_xferjob(carrier), "error deleting xfer jobs for carrier #{carrier}"
    assert wait_for {!@@sv.carrier_status(carrier).stocker.empty?}, "error stocking in carrier #{carrier}"
    # wait for cj do be deleted
    $log.info "waiting up to 6 min for the deletion of cj #{cj}"  # MDS configuration
    assert wait_for(timeout: 500) {@@sv.lot_info(lot).cj.empty?}, "error deleting cj #{cj}"
    # verify scenario
    assert wait_for(timeout: 120) {
      @@vaptest.vapdb.scenariolog(tstart: tstart, scenario: 'ControlJobCancel', carrier: cj).first
    }
  end

  def test603_cjcancel_ib
    lot = @@lot
    carrier = @@sv.lot_info(lot).carrier
    #opNo = @@vaptest.operations.keys.first
    #eqp = @@vaptest.operations[opNo]
    assert res = @@vaptest.find_operation_eqp(ib: true, max_batchsize: 2), 'no eqp found'
    opNo, eqp = res
    eqp = 'UTA014'
    assert_equal 0, @@sv.lot_cleanup(lot, opNo: opNo, stocker: @@svtest.stocker, xjs: true)
    assert @@vaptest.prepare_eqp(eqp, mode: 'Auto-3')
    # register RTD emulator rule, wait for AutoProc to create an xfer job and delete it
    tstart = Time.now
    assert @@vaptest.set_rtdemu_filter(lot, eqp)
    assert wait_for(timeout: 400) {
      @@vaptest.mdswrite.write_eqpwip(eqp, 1)
      xj = @@sv.carrier_xferjob(carrier)
      @@sv.carrier_xferjob_delete(carrier) unless xj.empty?
      !xj.empty?
    }, 'no xfer job'
    @@vaptest.rtdremote.add_emustation(eqp, method: 'empty_reply')
    # verify lot and carrier status
    cj = @@sv.lot_info(lot).cj
    refute_empty  cj, "lot #{lot} has no cj"
    assert_empty @@sv.carrier_xferjob(carrier), "error deleting xfer jobs for carrier #{carrier}"
    assert wait_for {!@@sv.carrier_status(carrier).stocker.empty?}, "error stocking in carrier #{carrier}"
    # wait for cj do be deleted
    $log.info "waiting up to 6 min for the deletion of cj #{cj}"  # MDS configuration
    assert wait_for(timeout: 500) {@@sv.lot_info(lot).cj.empty?}, "error deleting cj #{cj}"
    # verify scenario
    assert wait_for(timeout: 120) {@@vaptest.vapdb.scenariolog(tstart: tstart,
      scenario: 'ControlJobCancelInternalBuffer', carrier: cj).first
    }
  end

  def test604_cjcancel_ib_two_lots
    lot = @@lot
    lots = [lot, @@svtest.new_lot]
    carrier = @@sv.lot_info(lots.first).carrier
    assert res = @@vaptest.find_operation_eqp(ib: true, max_batchsize: 2), 'no eqp found'
    opNo, eqp = res
    lots.each {|lot|
      assert_equal 0, @@sv.lot_cleanup(lot, opNo: opNo, stocker: @@svtest.stocker, xjs: true)}
    assert @@vaptest.prepare_eqp(eqp, mode: 'Auto-3')
    # register RTD emulator rule, wait for AutoProc to create an xfer job and delete it
    tstart = Time.now
    assert @@vaptest.set_rtdemu_filter(lots, eqp)
    assert wait_for(timeout: 400) {
      @@vaptest.mdswrite.write_eqpwip(eqp, 1)
      xj = @@sv.carrier_xferjob(carrier)
      @@sv.carrier_xferjob_delete(carrier) unless xj.empty?
      !xj.empty?
    }, 'no xfer job'
    @@vaptest.rtdremote.add_emustation(eqp, method: 'empty_reply')
    # verify lot and carrier status
    cj0 = @@sv.lot_info(lots[0]).cj
    cj1 = @@sv.lot_info(lots[1]).cj
    refute_empty cj0, "lot #{lots[0]} has no cj"
    refute_empty cj1, "lot #{lots[1]} has no cj"
    assert_empty @@sv.carrier_xferjob(carrier), "error deleting xfer jobs for carrier #{carrier}"
    assert wait_for {!@@sv.carrier_status(carrier).stocker.empty?}, "error stocking in carrier #{carrier}"
    # wait for cj do be deleted
    $log.info "waiting up to 9 min for the deletion of cj #{cj0}"  # MDS configuration
    assert !wait_for(timeout: 500) {@@sv.lot_info(lots[0]).cj.empty?}, "error deleting cj #{cj0}"
    # verify scenario
    assert !wait_for(timeout: 120) {
      @@vaptest.vapdb.scenariolog(tstart: tstart, scenario: 'ControlJobCancelInternalBuffer', carrier: cj0).first
    }
    @@sv.delete_lot_family(lots[1])
  end

  def test611_rank
    lot = @@lot
    opNo = @@vaptest.operations.keys.first
    eqp = @@vaptest.operations[opNo]
    assert_equal 0, @@sv.lot_cleanup(lot, opNo: opNo, stocker: @@svtest.stocker, xjs: true)
    assert @@vaptest.prepare_eqp(eqp, mode: 'Auto-3')
    # register RTD emulator rule, wait for AutoProc to create an xfer job and delete it
    tstart = Time.now
    lrank, grank = 7, 11
    wn = RTD::WhatNextVapRank.new(@@sv, lot, lrank: lrank, grank: grank)
    @@vaptest.rtdremote.add_emustation(eqp, method: wn)
    assert wait_for(timeout: 400) {
      @@vaptest.mdswrite.write_eqpwip(eqp, 1)
      !@@sv.lot_info(lot).cj.empty?
    }, 'no reservation'
    # verify rank entries
    sleep 10
    assert e = @@rtddb.tool_dlis_hist(tstart: tstart, eqp: eqp, lot: lot).last, 'no db entry'
    assert_equal lrank, e[:lrank]
    assert_equal grank, e[:grank]
  end

  def test621_externalmove
    assert carrier = @@svtest.get_empty_carrier
    assert @@svtest.cleanup_carrier(carrier)
    tstart = Time.now
    sleep 10
    assert @@vaptest.mdswrite.write_fouptransfer(carrier, @@stocker2), 'error calling MdsWriteService'
    assert wait_for(timeout: 120) {!@@sv.carrier_xferjob(carrier).empty?}, 'no xfer job'
    assert sclog = @@vaptest.vapdb.scenariolog(tstart: tstart, scenario: 'ExternalMove', carrier: carrier).first, 'missing scenario'
    assert_equal @@stocker2, sclog.tgteqp, 'wrong stocker'
    assert_equal 'Transfer done.', sclog.msg, 'wrong msg'
    ## assert wait_for {@@sv.carrier_status(carrier).stocker == @@stocker2}, "carrier xfer failed"
  end

  def test622_externalmove_locked_foup # MSR1001730, MACS error (e.g. 4331), checked if != 0
    assert carrier = @@svtest.get_empty_carrier
    assert @@svtest.cleanup_carrier(carrier)
    assert @@dummyamhs.set_variable('errormode', true)
    # move carrier to create MDS event
    tstart1 = Time.now
    sleep 10
    # 1st move initiated by AutoProc, to be rejected by MACS
    assert @@vaptest.mdswrite.write_fouptransfer(carrier,  @@stocker2), 'error calling MdsWriteService'
    sclog = nil
    assert wait_for(timeout: 60) {
      sclog = @@vaptest.vapdb.scenariolog(tstart: tstart1, scenario: 'ExternalMove', carrier: carrier).first
    }, 'missing scenario'
    assert_equal 'Transfer failed.', sclog.msg, 'wrong msg'
    # verify job status is REJECTED (optional)
    assert tjob = @@rtddb.trans_job(carrier).first
    assert_equal 'REJECTED', tjob[:state]
    # verify carrier is locked
    assert lock = @@vaptest.vaprest.locks(carrier: carrier).first
    assert_equal 'INVALID', lock['lockState'], 'wrong lockState'
    # 2nd (valid) move while carrier is locked (30 s)
    assert @@dummyamhs.set_variable('errormode', false)
    tstart2 = Time.now
    #sleep 5
    assert @@vaptest.mdswrite.write_fouptransfer(carrier, @@stocker2), 'error calling MdsWriteService'
    sclog = nil
    assert wait_for(timeout: 60) {
      sclog = @@vaptest.vapdb.scenariolog(tstart: tstart2, scenario: 'ExternalMove', carrier: carrier).first
    }, 'missing scenario'
    assert_equal 'Transfer done.', sclog.msg, 'wrong msg'
    # verify carrier is still locked
    assert lock = @@vaptest.vaprest.locks(carrier: carrier).first
    assert_equal 'VALID', lock['lockState'], 'wrong lockState'
    assert tstart2 - tstart1 < 30, 'delay too long, wrong test condition'
  end

end
