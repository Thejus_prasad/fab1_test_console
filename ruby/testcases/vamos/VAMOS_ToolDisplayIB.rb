=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: Steffen Steidten, 2013-08-26

Version: 6.12

History:
  2015-08-31 ssteidte,  created test for IB tools
  2018-03-06 sfrieske, replaced user_parameter_set_verify by user_parameter_change
  2018-05-24 sfrieske, use @@vaptest instead of $vaptest, remove Corba client tests
  2019-02-22 sfrieske, minor cleanup, renamed from Test_VAMOS.. to VAMOS...
  2021-07-23 sfrieske, inherit from SiViewTestCase instad of VAMOS_AutoProc, more cleanup

Notes:
  start VamosDisplay (ITDC): http://vf36vamosd1:9089/vamos/vamosdsp.jsf
  endurance tests: runtest "Test_VAMOS_ToolDisplay", "itdc", tp: /test00|test10/, parameters: {loops: 1000}
=end

require 'rtdserver/rtdemuremote'
require 'vamos/vamos'
require 'SiViewTestCase'


class VAMOS_ToolDisplayIB < SiViewTestCase
  @@dev = false  # must stay false, connect to dev instances by passing parameters: {dev: true}
  @@url = nil #'http://drsvitd2w71054:8080/vamos/intBuffToolDsp.jsf'  # nil  #'http://f1ws11:9080/vamos/vamosdsp.jsf' # for dev tests in ITDC
  @@sv_defaults = {carrier_category: 'BEOL', carriers: '%', route: 'A-AP-MAINPD.02', product: 'A-AP-PRODUCT.02'}
  @@eqp = 'UTA004'
  @@opNo = '1000.4000'

  @@toolevent_delay = 12  # 10 s polling
  @@mdsevent_delay = 25
  @@loops = 1  # normally 1, for endurance tests: 100 (~8h) or 1000 (~3d)

  @@testlots = []


  def self.shutdown
    # @@sv.user_parameter_change('Eqp', @@eqp, {'Pull'=>'', 'Push'=>''})
    @@testlots.each {|lot| @@sv.delete_lot_family(lot)}
  end


  def test00_setup
    $setup_ok = false
    #
    @@rtdremote = RTD::EmuRemote.new($env)
    @@rtdremote.add_emustation(@@eqp, method: 'empty_reply')  # suppress AutoProc activities
    #
    # prepare a clean lot
    assert @@lot = @@svtest.new_lot
    @@testlots << @@lot
    @@carrier = @@sv.lot_info(@@lot).carrier
    eqpi = @@sv.eqp_info(@@eqp)
    assert @@port = eqpi.ports.first.port
    # assert_equal 0, @@sv.user_parameter_change('Eqp', @@eqp, {'Pull'=>@@port, 'Push'=>@@port})
    @@td = Vamos::ToolDisplay.new(env = @@dev ? 'dev' : $env, @@eqp, @@port, ib: true, url: @@url)
    @@td.start_browser
    #
    $setup_ok = true
  end

  def test11_EImsgs
    # preparation
    assert_equal 0, @@sv.lot_cleanup(@@lot, opNo: @@opNo, stocker: @@svtest.stocker)
    assert @@svtest.cleanup_eqp(@@eqp, mode: 'Auto-1')
    li = @@sv.lot_info(@@lot, wafers: true)
    assert @@td.material_removed(@@carrier, li.wafers)
    $log.info "waiting #{@@toolevent_delay} s for the MDS to sync"; sleep @@toolevent_delay
    puts "\nOpen a VAMOS Display for #{@@eqp} #{@@port} and press ENTER!"
    gets
    #
    @@loops.times {|i|
      $log.info "\n\ntest #{i+1}/#{@@loops}\n"
      # SLR, carrier and its stocker show up at top of the reserved port in yellow
      assert cj = @@sv.slr(@@eqp, port: @@port, lot: @@lot), 'error reserving lot'
      sleep @@toolevent_delay
      # load carrier, upper area turns grey
      assert_equal 0, @@sv.carrier_xfer_status_change(@@carrier, 'MO', @@svtest.stocker)
      assert_equal 0, @@sv.eqp_port_status_change(@@eqp, @@port, 'LoadComp')
      sleep @@toolevent_delay
      # material_arrived message carrier, lots and slots, recipe show up in center area
      assert @@td.material_arrived(@@carrier, li.wafers, cj: cj)
      sleep @@toolevent_delay
      # eqp load and opestart don't change the picture
      assert_equal 0, @@sv.eqp_load(@@eqp, @@port, @@carrier)
      assert cj = @@sv.eqp_opestart(@@eqp, @@carrier)
      # 1st wafer_started, undermost slot turns yellow
      assert @@td.wafer_started(@@carrier, [li.wafers.first])
      sleep @@toolevent_delay
      # last wafer_started, topmost slot turns yellow
      assert @@td.wafer_started(@@carrier, [li.wafers.last])
      sleep @@toolevent_delay
      # last wafer completed, topmost slot turns green
      assert @@td.wafer_completed(@@carrier, [li.wafers.last])
      sleep @@toolevent_delay
      # 1st wafer completed, undermost slot turns green
      assert @@td.wafer_completed(@@carrier, [li.wafers.first])
      sleep @@toolevent_delay
      #
      # complete all (other) wafers, all slots are green
      li.wafers.each {|w| assert @@td.wafer_completed(@@carrier, [w])}
      sleep @@toolevent_delay
      # opecomp, carrier goes away from upper port zone
      assert_equal 0, @@sv.eqp_opecomp(@@eqp, cj)
      sleep @@toolevent_delay
      #
      # port status change, a red "Down" shows up
      assert_equal 0, @@sv.eqp_port_status_change(@@eqp, @@port, 'Down')
      sleep @@mdsevent_delay
      #
      # move carrier from ib to port and change port status to UnloadReq, port is no longer red and carrier shows up in port area
      @@sv.eqp_carrierfromib_reserve(@@eqp, @@port, @@carrier)
      @@sv.eqp_carrierfromib(@@eqp, @@port, @@carrier)
      assert_equal 0, @@sv.eqp_port_status_change(@@eqp, @@port, 'UnloadReq')
      sleep @@mdsevent_delay
      #
      # unload carrier (no change) and material removed, carrier goes away from central zone
      assert_equal 0, @@sv.eqp_unload(@@eqp, @@port, @@carrier)
      assert @@td.material_removed(@@carrier, li.wafers, cj: cj)
      sleep @@toolevent_delay
      # cleanup
      assert_equal 0, @@sv.lot_cleanup(@@lot, opNo: @@opNo, stocker: @@svtest.stocker)
      assert_equal 0, @@sv.eqp_cleanup(@@eqp)
    }
  end

  def test13_loadcomp_short
    # preparation
    assert_equal 0, @@sv.lot_cleanup(@@lot, opNo: @@opNo, stocker: @@svtest.stocker)
    assert @@svtest.cleanup_eqp(@@eqp, mode: 'Auto-1')
    li = @@sv.lot_info(@@lot, wafers: true)
    assert @@td.material_removed(@@carrier, li.wafers)
    $log.info "waiting #{@@toolevent_delay} s for the MDS to sync"; sleep @@toolevent_delay
    puts "\nOpen a VAMOS Display for #{@@eqp} #{@@port} and press ENTER!"
    gets
    #
    # SLR, carrier and its stocker show up at top of the reserved port in yellow
    assert cj = @@sv.slr(@@eqp, port: @@port, lot: @@lot), 'error reserving lot'
    sleep @@toolevent_delay
    # load carrier, LoadComp state is very short
    assert_equal 0, @@sv.carrier_xfer_status_change(@@carrier, 'MO', @@svtest.stocker)
    assert_equal 0, @@sv.eqp_port_status_change(@@eqp, @@port, 'LoadComp')
    assert @@td.material_arrived(@@carrier, li.wafers, cj: cj)
    # eqp load: carrier goes away in HTTP viewer
    assert_equal 0, @@sv.eqp_load(@@eqp, @@port, @@carrier)
    $log.info 'carrier goes away'
    sleep 2 * @@toolevent_delay
  end

end
