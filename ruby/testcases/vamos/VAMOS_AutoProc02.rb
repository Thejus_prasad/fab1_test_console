=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.


Author: Steffen Steidten, 2014-03-03

History:
  2015-03-13 ssteidte, more reliable test preparation
  2016-05-26 sfrieske, added test cases for carrier EO and port not UnloadReq
  2018-05-24 sfrieske, minor cleanup, use @@vaptest instead of $vaptest
  2019-02-22 sfrieske, minor cleanup, renamed from Test_VAMOS.. to VAMOS...
=end

require_relative 'VAMOS_AutoProc'


# Basic Auto-3 Scenarios: ManualPush, ManualPull
class VAMOS_AutoProc02 < VAMOS_AutoProc

  def test200_setup
    $setup_ok = false
    #
    assert @@lot = @@svtest.new_lot, 'error creating test lot'
    @@testlots << @@lot
    # find opeartion/tool combinations for the test cases
    assert res = @@vaptest.find_operation_eqp(ib: false), 'no eqp found'
    @@opNo_fb, @@eqp_fb = res
    assert res = @@vaptest.find_operation_eqp(ib: true), 'no eqp found'
    @@opNo_ib, @@eqp_ib = res
    #
    $setup_ok = true
  end

  def test201_manualpull_stocker
    $setup_ok = false
    #
    lot = @@lot
    carrier = @@sv.lot_info(lot).carrier
    [@@opNo_fb, @@opNo_ib].each {|opNo|
      eqp = @@vaptest.operations[opNo]
      $log.info ">> test tool #{eqp}, lot #{lot}"
      assert_equal 0, @@sv.lot_cleanup(lot, opNo: opNo, stocker: @@svtest.stocker, xjs: true)
      assert @@vaptest.prepare_eqp(eqp, mode: 'Auto-1')
      tstart = Time.now
      @@vaptest.set_rtdemu_filter(lot, eqp)
      # run test 2 times, with carrier xfer status MI and MO each
      2.times {
        # trigger manual pull
        ['1PRD', '2WPR'].each {|stat| @@sv.eqp_status_change(eqp, stat)}
        # wait for reservation and carrier stock-out
        assert wait_for(timeout: 400) {!@@sv.lot_info(lot).cj.empty?}, 'no reservation'
        assert wait_for {@@sv.lot_info(lot).xfer_status == 'MO'}, "lot #{lot}: transfer status is not MO"
        # cancel reservation and wait for AutoProc to pick up the lot again
        assert_equal 0, @@sv.slr_cancel(eqp, @@sv.lot_info(lot).cj)
      }
      #
      @@vaptest.stop_test(eqp)
      $log.info 'verifying scenario log'
      assert wait_for(timeout: 60) {
        @@vaptest.vapdb.scenariolog(tstart: tstart, scenario: 'ManualPull%', carrier: carrier, tgteqp: eqp).first
      }, "scenario error with #{eqp.inspect}"
    }
    #
    $setup_ok = true
  end

  def test202_manualpull_EO
    lot = @@lot
    carrier = @@sv.lot_info(lot).carrier
    [@@opNo_fb, @@opNo_ib].each {|opNo|
      eqp = @@vaptest.operations[opNo]
      $log.info ">> test tool #{eqp}, lot #{lot}"
      assert_equal 0, @@sv.lot_cleanup(lot, opNo: opNo, stocker: @@svtest.stocker, xjs: true)
      assert eqp2 = (@@sv.lot_info(lot).opEqps - [eqp]).first, 'second eqp missing'
      assert @@svtest.cleanup_eqp(eqp2, mode: 'Auto-1', portstatus: 'LoadReq')
      assert_equal 0, @@sv.carrier_xfer_status_change(carrier, 'EO', eqp2)
      assert @@vaptest.prepare_eqp(eqp, mode: 'Auto-1')
      tstart = Time.now
      @@vaptest.set_rtdemu_filter(lot, eqp)
      # trigger manual pull and wait for reservation
      ['1PRD', '2WPR'].each {|stat| @@sv.eqp_status_change(eqp, stat)}
      assert wait_for(timeout: 400) {!@@sv.lot_info(lot).cj.empty?}, 'no reservation'
      #
      @@vaptest.stop_test(eqp)
      $log.info 'verifying scenario log'
      assert wait_for(timeout: 60) {
        @@vaptest.vapdb.scenariolog(tstart: tstart, scenario: 'ManualPull%', carrier: carrier, tgteqp: eqp).first
      }, "scenario error with #{eqp.inspect}"
    }
  end

  def test203_manualpull_mixed
    # get eqp with batch capability
    assert res = @@vaptest.find_operation_eqp(ib: false, max_batchsize: 2), 'no eqp found'
    opNo_fb, eqp_fb = res
    assert res = @@vaptest.find_operation_eqp(ib: true, max_batchsize: 2), 'no IB eqp found'
    opNo_ib, eqp_ib = res
    #
    lot = @@lot
    assert lot2 = @@svtest.new_lot, 'error creating test lot'
    @@testlots << lot2
    carrier = @@sv.lot_info(lot).carrier
    [opNo_fb, opNo_ib].each {|opNo|
      eqp = @@vaptest.operations[opNo]
      $log.info ">> test tool #{eqp}, lot #{lot}"
      assert_equal 0, @@sv.lot_cleanup(lot, opNo: opNo, stocker: @@svtest.stocker, xjs: true)
      assert_equal 0, @@sv.lot_cleanup(lot2, opNo: opNo, stocker: @@svtest.stocker, xjs: true)
      assert eqp2 = (@@sv.lot_info(lot).opEqps - [eqp]).first, 'second eqp missing'
      assert @@svtest.cleanup_eqp(eqp2, mode: 'Auto-1', portstatus: 'LoadReq')
      assert_equal 0, @@sv.carrier_xfer_status_change(carrier, 'EO', eqp2)
      assert @@vaptest.prepare_eqp(eqp, mode: 'Auto-1')
      tstart = Time.now
      @@vaptest.set_rtdemu_filter([lot, lot2], eqp)
      # trigger manual pull
      ['1PRD', '2WPR'].each {|stat| @@sv.eqp_status_change(eqp, stat)}
      # wait for reservation and carrier stock-out
      assert wait_for(timeout: 400) {!@@sv.lot_info(lot).cj.empty? && !@@sv.lot_info(lot2).cj.empty?}, 'no reservation'
      #
      @@vaptest.stop_test(eqp)
      $log.info 'verifying scenario log'
      assert wait_for {
        @@vaptest.vapdb.scenariolog(tstart: tstart, scenario: 'ManualPull%', carrier: carrier, tgteqp: eqp, msg: '%Batch-Res%').first
      }, "scenario error with #{eqp.inspect}"
    }
  end

  def test211_manualpush_to_stocker
    # get eqp with no batch capability because of the port 'Down' trick
    assert res = @@vaptest.find_operation_eqp(ib: false, max_batchsize: 1), 'no eqp found'
    opNo_fb, eqp_fb = res
    assert res = @@vaptest.find_operation_eqp(ib: true), 'no IB eqp found'
    opNo_ib, eqp_ib = res
    #
    lot = @@lot
    carrier = @@sv.lot_info(lot).carrier
    [opNo_fb, opNo_ib].each {|opNo|
      eqp = @@vaptest.operations[opNo]
      $log.info ">> eqp: #{eqp}, lot: #{lot}, carrier: #{carrier}"
      assert_equal 0, @@sv.lot_cleanup(lot, opNo: opNo, stocker: @@svtest.stocker, xjs: true)
      eqpi = @@sv.eqp_info(eqp)
      port = eqpi.ports.first.port
      # where next for carrier
      wntarget = RTD::WhereNextVapTarget.new(@@sv, carrier, stocker: @@svtest.stocker, entity: 'Push')
      @@vaptest.rtdremote.add_emustation('VAMOS_WN', rule: 'WhereNextEquipment', method: wntarget)
      # process lot at eqp, don't unload carrier
      tstart = Time.now
      assert @@vaptest.prepare_eqp(eqp, mode: 'Auto-1')  #, portstatus: 'Down')
      assert @@sv.claim_process_lot(lot, eqp: eqp, port: port, mode: 'Auto-1', nounload: true)
      assert_equal 0, @@sv.eqp_carrierfromib(eqp, port, carrier) if eqpi.ib
      assert_equal 0, @@sv.eqp_port_status_change(eqp, port, 'UnloadReq'), 'error switching port status'
      #
      $log.info 'verifying scenario log'
      scenario = nil
      assert wait_for(timeout: 120) {
        scenario = @@vaptest.vapdb.scenariolog(tstart: tstart, scenario: 'ManualPush%', carrier: carrier, srctype: 'EQP', tgttype: 'STK', srceqp: eqp).first
      }, "scenario error with #{eqp.inspect}, ensure there is a proper (emulated) WhereNextEquipment rule"
      assert_equal 'StockIn done.', scenario.msg
      @@vaptest.stop_test(eqp)
    }
  end

  def test212_manualpush_to_tool
    # get eqp with no batch capability because of the port 'Down' trick
    assert res = @@vaptest.find_operation_eqp(ib: false, max_batchsize: 1, e2e: true), 'no eqp found'
    opNo_fb, eqp_fb = res
    assert res = @@vaptest.find_operation_eqp(ib: true, e2e: true), 'no eqp found'
    opNo_ib, eqp_ib = res
    #
    lot = @@lot
    carrier = @@sv.lot_info(lot).carrier
    [opNo_fb, opNo_ib].each {|opNo|
      eqp = @@vaptest.operations[opNo]
      opNo2 = @@vaptest.operations.keys[@@vaptest.operations.keys.index(opNo) + 1]
      eqp2 = @@vaptest.operations[opNo2]
      $log.info ">> test tools #{eqp} (E2E), #{eqp2}, lot #{lot}"
      # where next for carrier
      wntarget = RTD::WhereNextVapTarget.new(@@sv, carrier, eqp: eqp2, entity: 'Push')
      @@vaptest.rtdremote.add_emustation('VAMOS_WN', rule: 'WhereNextEquipment', method: wntarget)
      # register RTD rule for eqp2
      assert @@vaptest.prepare_eqp(eqp2, mode: 'Auto-1')  # push and pull parameters MUST be set
      @@vaptest.set_rtdemu_filter(lot, eqp2)
      #
      assert_equal 0, @@sv.lot_cleanup(lot, opNo: opNo, stocker: @@svtest.stocker, xjs: true)
      eqpi = @@sv.eqp_info(eqp)
      port = eqpi.ports.first.port
      # process lot at eqp (with 'sufficient' proctime, don't unload carrier
      assert @@vaptest.prepare_eqp(eqp, mode: 'Auto-1')  ##, portstatus: 'Down')
      assert @@sv.claim_process_lot(lot, eqp: eqp, port: port, mode: 'Auto-1', nounload: true, proctime: 15)
      # switch load port of the first tool to UnloadReq
      @@sv.eqp_carrierfromib(eqp, port, carrier) if eqpi.ib
      tstart = Time.now
      assert_equal 0, @@sv.eqp_port_status_change(eqp, port, 'UnloadReq'), 'error switching port status'
      # wait for lot reservation at eqp2
      assert wait_for {@@sv.eqp_info(eqp2).rsv_carriers.member?(carrier)}, 'no reservation'
      #
      @@vaptest.stop_test([eqp, eqp2])
      $log.info 'verifying scenario log'
      scenario = nil
      assert wait_for(timeout: 120) {
        scenario = @@vaptest.vapdb.scenariolog(tstart: tstart, 
          scenario: 'ManualPush%', carrier: carrier, srceqp: eqp, tgteqp: eqp2).first
      }, "scenario for #{[eqp, eqp2]} not called"
      assert_equal 'Reservation (tool-to-tool) done.', scenario.msg, 'wrong msg'
    }
  end

end
