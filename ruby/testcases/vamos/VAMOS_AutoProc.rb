=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.


AutoProc config file (LET): f38asq10:/amdapps/vamos/VAP_601/config/autoproc.properties
VAMOS code SVN: https://gfsvnp01/svn/vamos/trunk/
AutoProc viewer in ITDC: http://f36asd5:8580/autoproc-war/index.jsf

Author: Steffen Steidten, 2014-03-03

History:
  2015-03-13 ssteidte, more reliable test preparation
  2018-05-24 sfrieske, use @@vaptest instead of $vaptest
  2019-02-22 sfrieske, minor cleanup, renamed from Test_VAMOS.. to VAMOS...
=end

require 'vamos/vaptest'
require 'SiViewTestCase'


# General AutoProc test setup and cleanup
class VAMOS_AutoProc < SiViewTestCase
  @@sv_defaults = {
    carrier_category: 'BEOL', carriers: '%',
    route: 'A-AP-MAINPD.02', product: 'A-AP-PRODUCT.02', product_procmonitor: 'UT-PR-UTA004.01', nwafers: 10
  }

  def self.startup
    super
    @@vaptest = Vamos::AutoProcTest.new($env, svtest: @@svtest)
    @@testlots = []   # lots to be deleted
  end

  def self.shutdown
    @@vaptest.stop_test(nil)  # no eqp specific rules
    @@vaptest.rtdremote.delete_emustation('VAMOS_WN', rule: 'WhereNextEquipment')
    @@vaptest.rtdremote.delete_emustation('VAMOS_WN_A', rule: 'WhereNextEquipment')
    @@testlots.each {|lot| @@sv.delete_lot_family(lot)}
  end


  def setup
    assert @@vaptest.verify_wtdg(nil), 'wrong FSDRWTDG_CONF config'
    # empty replies for all test eqps during test preparation
    @@vaptest.operations.values.each {|eqp|
      assert @@vaptest.rtdremote.add_emustation(eqp, method: 'empty_reply')
      assert @@sm.update_udata(:eqp, eqp, {'EarlyMovingFOUP'=>''}) if @@sv.user_data(:eqp, eqp)['EarlyMovingFOUP']  # !!
    }
    # ensure proper (emulated) WhereNextEquipment replies
    @@vaptest.rtdremote.add_emustation('VAMOS_WN', rule: 'WhereNextEquipment')
    @@vaptest.rtdremote.add_emustation('VAMOS_WN_A', rule: 'WhereNextEquipment')
  end

end
