=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: Steffen Frieske, 2016-10-26

History
  2020-10-19 sfrieske, CCE::Test does not inherit from SiView::Test
=end

require 'SiViewTestCase'
require 'cce/ccetest'


# CCE Performance Tests
class CCE_900 < SiViewTestCase
  @@sv_defaults = {carriers: '%', route: 'C02-TKEY.01', product: 'PANTHR11ZA.Z0'}
  @@export_klarf = true

  def self.startup
    super
    @@ccetest = CCE::Test.new($env, sv: @@sv)
    @@testlots = []
  end

  def self.manual_cleanup
    @@testlots.each {|lot| @@sv.delete_lot_family(lot)}
  end


  def test901
    # lot with 1 wafer
    assert lot = @@svtest.new_lot(nwafers: 1)
    @@testlots << lot
    assert @@ccetest.create_lot_data(lot, export: @@export_klarf, step: @@ccetest.steps.take(1))
  end

end
