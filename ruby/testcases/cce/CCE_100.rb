=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: Steffen Steidten, 2015-09-18

History
  2020-10-19 sfrieske, CCE::Test does not inherit from SiView::Test
=end

require 'SiViewTestCase'
require 'cce/ccetest'


# Create test lots for CCE tests, CCE URL in ITDC: http://f1oqacced1:8080/cce/#approve 
class CCE_100 < SiViewTestCase
  @@sv_defaults = {carriers: '%', route: 'C02-TKEY.01', product: 'PANTHR11ZA.Z0'}
  @@export_klarf = true

  def self.startup
    super
    @@ccetest = CCE::Test.new($env, sv: @@sv)
    @@testlots = []
  end

  def self.manual_cleanup
    @@testlots.each {|lot| @@sv.delete_lot_family(lot)}
  end

  # for dev, e.g. verify loaders work
  def testman010
    assert lot = @@svtest.new_lot(nwafers: 1)
    @@testlots << lot
    assert @@ccetest.create_lot_data(lot, export: @@export_klarf, step: 'FOUT-DI')
  end


  def test100
    # lot with 1 wafer
    assert lot = @@svtest.new_lot(nwafers: 1)
    @@testlots << lot
    assert @@ccetest.create_lot_data(lot, export: @@export_klarf, step: @@ccetest.steps.take(1))
    # lot with 3 wafers and 2 measurements each
    assert lot = @@svtest.new_lot(nwafers: 3)
    @@testlots << lot
    assert @@ccetest.create_lot_data(lot, export: @@export_klarf, step: @@ccetest.steps.take(2))
    # lot with 3 wafers and 5 measurements each
    assert lot = @@svtest.new_lot(nwafers: 3)
    @@testlots << lot
    assert @@ccetest.create_lot_data(lot, export: @@export_klarf, step: @@ccetest.steps.take(5))
    #
    $log.info "created test lots #{@@testlots}"
  end

  def test110_past
    # lot with 3 wafers and 2 measurements each, timestamp in the past
    assert lot = @@svtest.new_lot(nwafers: 3)
    @@testlots << lot
    ts = Time.now - 86400 * 30  # 30 days back
    assert @@ccetest.create_lot_data(lot, export: @@export_klarf, step: @@ccetest.steps.take(2), timestamp: ts)
  end

  def testman110
    # 3 lots each with 25 wafers and 5 measurements each
    3.times {
      assert lot = @@svtest.new_lot(nwafers: 25)
    @@testlots << lot
    assert @@ccetest.create_lot_data(lot, export: @@export_klarf, step: @@ccetest.steps.take(5))
  }
  end

  def testman120_before
    # before trigger pd
    assert lot = @@svtest.new_lot(nwafers: 3)
    @@testlots << lot
    assert @@ccetest.create_lot_data(lot, export: @@export_klarf, step: @@ccetest.steps.take(2))
  end
end
