=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: Steffen Frieske, 2017-05-10

History
  2020-10-19 sfrieske, CCE::Test does not inherit from SiView::Test
  2021-08-27 sfrieske, require waitfor instead of misc
=end

require 'cce/ccetest'
require 'util/waitfor'
require 'SiViewTestCase'


# Test LotEvent handling (for Backup Operation lots)
class CCE_LotEvent < SiViewTestCase
  # F8 is the source fab!
  @@svsrc_defaults = {product: 'UT-PRODUCT-FAB1.01', route: 'UTRT101.01', customer: 'qgt', carriers: '%'}
  @@product = 'PANTHR11ZA.Z0'
  @@route = 'C02-TKEY.01'
  @@env_src = 'f8stag'
  @@backup_opNo = '1000.200'
  @@backup_bank = 'O-BACKUPOPER'

  @@export_klarf = true
  @@cceimporter_interval = 360


  def self.startup
    super
    @@ccelotevent = CCE::LotEvent.new($env)
    @@ccetest = CCE::Test.new($env, sv: @@sv)
    @@cceimporter = CCE::Importer.new($env, sv: @@sv)
    #
    @@svtest_src = SiView::Test.new(@@env_src, @@svsrc_defaults)
    @@sv_src = @@svtest_src.sv
    #
    @@testlots = []
  end

  def self.manual_cleanup
    @@testlots.each {|lot| @@sv_src.backup_delete_lot_family(lot, @@sv)}
  end


  def test11_happy
    # create test lot in src Fab and send to backup Fab (ITDC)
    assert lot = @@svtest_src.new_lot(nwafers: 1), 'error creating lot'
    @@testlots << lot
    assert @@sv_src.backup_prepare_send_receive(lot, @@sv, backup_opNo: @@backup_opNo, backup_bank: @@backup_bank)
    # change product and route to match CCE setup
    assert_equal 0, @@sv.schdl_change(lot, product: @@product, route: @@route)
    # send lot event to a queue like MDS would do via MDSEventhandler and jCAPMsg2MQ, MDS delay 150s
    refute_nil @@ccelotevent.mq.delete_msgs(nil)
    assert @@ccelotevent.lot_event(lot), 'error sending LotEvent, MQ setup issue?'
    assert wait_for(timeout: 60) {@@ccelotevent.mq.qdepth.zero?}, 'msg still on the queue, CCE app not listening?'
    assert wait_for(timeout: 60) {@@cceimporter.appdb.lotimportstate(lot)}, "missing entry for #{lot} in LOTIMPORTSTATE"
    # send import data
    assert fname = @@cceimporter.send_import_data(lot, export: @@export_klarf)
    assert wait_for(timeout: @@cceimporter_interval) {
      @@cceimporter.ccesrv.exec!("test -f #{fname} && echo OK") == ''
    }, "good import file #{fname.inspect} not processed"
    # send KLARF data as preparation for further tests
    sleep 120
	  assert @@ccetest.create_lot_data(lot, export: @@export_klarf, step: @@ccetest.steps.take(5))
  end

  def testman21_wrong_file  # fails
    # create a dummy file and send it to the CCE importer
    fname = File.join(@@cceimporter.datadir, unique_string('QA'))
    $log.info "sending file #{fname.inspect}"
    assert @@cceimporter.ccesrv.write_file("QA test", fname)
    assert @@cceimporter.ccesrv.exec!("chmod a+r #{fname}")
    assert wait_for(timeout: @@cceimporter_interval) {
      @@cceimporter.ccesrv.exec!("test -f #{fname} && echo OK") == ''
    }, "corrupt import file #{fname.inspect} not removed"
  end

end
