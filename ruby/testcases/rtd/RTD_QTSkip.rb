=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: sfrieske, 2019-01-08
=end

require 'misc/ammo'
require 'SiViewTestCase'


class RTD_QTSkip < SiViewTestCase
  @@sv_defaults = {route: 'UTRT-QTSKIP.01', product: 'UT-QTSKIP.01', carriers: '%'}
  @@opNo_qt = '2000.1000'
  @@opNo_skip = '2000.1300'
  @@pdskip = 'UTPDQTSKIP01.01'
  @@skipdelay = 30  # minutes, QT duratation (60 DispatchPrecede) - RouteOpAutomation (LTBUFFER:30)
  @@slack = 330     # time for PCL to execute the skip, polls every 5 min

  @@testlots = []

  def self.after_all_passed
    @@testlots.each {|lot| @@sv.delete_lot_family(lot)}
  end

  def self.manual_cleanup
    @@testlots.each {|lot| @@sv.delete_lot_family(lot)}
  end


  def test00_setup
    $setup_ok = false
    #
    @@ammo = AMMO::Client.new($env)
    assert @@ammo.echo, 'no AMMO connection'
    #
    @@pdudata = @@sv.user_data_pd(@@pdskip)
    refute_empty @@pdudata['ProcessTypeWfrSelection'], 'ProcessTypeWfrSelection not set'
    refute_empty @@pdudata['ProcessLayer'], 'ProcessLayer not set'
    ropudata = @@sv.user_data(:route_opNo, [@@opNo_skip, @@svtest.route, 'Main'])
    assert_equal 'LTBUFFER:30', ropudata['RouteOpAutomation'], 'wrong route op UDATA'
    #
    $setup_ok = true
  end

  def test11_skip
    # prepare lot
    assert lot = @@svtest.new_lot(nwafers: 5), 'error creating testlot'
    @@testlots << lot
    # wafer selection
    li = @@sv.lot_info(lot, wafers: true)
    wafers = li.wafers.collect {|w| w.wafer}
    @@sv.lot_processlist_in_route(lot).each_pair {|opNo, op|
      assert pdudata = @@sv.user_data_pd(op), 'no PD UDATA?'
      refute_empty pdudata['ProcessTypeWfrSelection'], "missing ProcessTypeWfrSelection for PD #{op.inspect}"
      refute_empty pdudata['ProcessLayer'], "missing ProcessLayer for PD #{op.inspect}"
      assert @@ammo.set_wafer_selection(wafers, pdudata['ProcessTypeWfrSelection'], pdudata['ProcessLayer'], selectflag: 3)
    }
    #
    # start QT
    assert_equal 0, @@sv.lot_opelocate(lot, @@opNo_qt)
    assert_equal 0, @@sv.lot_gatepass(lot)
    refute_empty @@sv.lot_qtime_list(lot), 'lot has no QT'
    # move to QTSkip
    assert_equal 0, @@sv.lot_opelocate(lot, nil, op: @@pdskip, force: :ondemand)
    ($log.info "waiting #{@@skipdelay} min for PCL skip"; sleep @@skipdelay * 60 + @@slack)
    refute_equal @@pdskip, @@sv.lot_info(lot).op, 'lot has not been skipped'
  end

end
