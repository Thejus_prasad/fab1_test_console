=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: sfrieske, 2021-09-01

Version: 21.08

History:

Notes:
  see http://fast/request/CR/Main/Frame.asp?appId=2&requestId=87487
  run cycle: every 15 minutes
=end

require 'SiViewTestCase'
require 'rtdserver/rtdclient'
require 'util/waitfor'


class RTD_Rule_SKIP_ZG_RWK < SiViewTestCase
  # @@sv_defaults = {}
  @@opNo = '1000.700' # RouteOpUdata RouteOpSlotRestriction set to 'SlotDistance-2'
  @@rtd_rule = 'JCAP_SKIP_ZG_RWK_POS'
  @@rtd_category = 'DISPATCHER/JCAP'
  @@apf_delay = 15
  @@job_interval = 930  # JCAP_SKipProcessOperation

  @@testlots = []


  def self.after_all_passed
    @@testlots.each {|lot| @@sv.delete_lot_family(lot)}
  end


  def test00_setup
    $setup_ok = false
    #
    assert opinfo = @@sm.object_info(:mainpd, @@svtest.route, details: true).first.specific[:operations].find {|e| e.opNo == @@opNo}
    assert_equal 'SlotDistance-2', opinfo.udata['RouteOpSlotRestriction']
    #
    assert @@rtdclient = RTD::Client.new("#{$env}.emu")
    assert res = @@rtdclient.dispatch_list(nil, report: @@rtd_rule, category: @@rtd_category)
    assert_equal @@rtd_rule, res.rule, 'wrong rule, RTD setup?'
    #
    $setup_ok = true
  end

  def test11_happy
    assert lot = @@svtest.new_lot(nwafers: 3, slots: [1, 3, 5]), 'error creating lot'
    @@testlots << lot
    assert_equal 0, @@sv.lot_opelocate(lot, @@opNo), 'error locating lot'
    $log.info "waiting #{@@apf_delay} s for APF replication"; sleep @@apf_delay
    assert res = @@rtdclient.dispatch_list(nil, report: @@rtd_rule, category: @@rtd_category)
    assert res.rows.find {|e| e.first == lot}, "lot #{lot} not found by RTD"
    $log.info "waiting up to #{@@job_interval} s for the JCAP job to run"
    assert wait_for(timeout: @@job_interval) {
      @@sv.lot_info(lot).opNo != @@opNo
    }, 'job did not gatepass the lot'
  end

  def test12_wrong_slots
    assert lot = @@svtest.new_lot(nwafers: 3, slots: [1, 3, 4]), 'error creating lot'
    @@testlots << lot
    assert_equal 0, @@sv.lot_opelocate(lot, @@opNo), 'error locating lot'
    $log.info "waiting #{@@apf_delay} s for APF replication"; sleep @@apf_delay
    assert res = @@rtdclient.dispatch_list(nil, report: @@rtd_rule, category: @@rtd_category)
    assert_nil res.rows.find {|e| e.first == lot}, "wrong lot #{lot} returned by RTD"
  end

end
