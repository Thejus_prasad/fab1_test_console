=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author:  sfrieske, 2019-11-04

Version: 1.0.0

History:
=end

require 'jcap/sjctest'
require 'rtdserver/rtdclient'
require 'util/waitfor'
require 'SiViewTestCase'


# Test the RTD Java User Exit for Sort Requests with wafer slots; MSRs 1092314, 1340350
class RTD_JUE_SR < SiViewTestCase
  # @@rtd_defaults = {rule: 'QA_JUETest_createSortRequestSlotsTM', category: 'DISPATCHER/QA'}
  @@category = 'DISPATCHER/QA'
  @@report = 'QA_JUETest_createSortRequestSlotsTM'
  @@pnames = %w(seq sortType hint requestor source_carrier_id source_carrier_category
    lot_id wafer_id target_carrier_id target_carrier_category wafer_target_position)

  @@testcarriers = []
  @@testlots = []


  def self.shutdown
    @@sjctest.cleanup_eqp(@@sjctest.sorter)
  end

  def self.after_all_passed
    @@testcarriers.each {|c| @@sjctest.cleanup_sort_requests(c)}
    @@testlots.each {|lot| @@sv.delete_lot_family(lot)}
  end


  def test00_setup
    $setup_ok = false
    #
    assert @@sjctest = SJC::Test.new($env, @@sv_defaults.merge(sv: @@sv))  #, carriers: @@carriers))
    assert @@rtdclient = RTD::Client.new($env)
    assert lot = @@svtest.new_lot(nwafers: 3), 'error creating test lot'
    @@testlots << lot
    #
    $setup_ok = true
  end

  def test11
    lot = @@testlots[0]
    li = @@sv.lot_info(lot, wafers: true)
    wafers = li.wafers.collect {|w| w.wafer}
    tgtcarrier = @@svtest.get_empty_carrier
    tgtslots = [11, 13, 12]
    #
    # call QA test rule and verify response
    parameters = RTD_JUE_SR.create_parameters(lot, tgtcarrier, slots: tgtslots)
    assert res = @@rtdclient.dispatch_list('x', category: @@category, report: @@report, parameters: parameters)
    refute @@rtdclient.response.include?('ERROR') || @@rtdclient.response.include?('error'), 'rule execution error'
    assert_equal @@report, res.rule, 'wrong rule executed'
    assert_equal wafers.size, res.rows.size, 'wrong number of answer rows'
    res.rows.each {|row| assert_equal 'DONE', row.first, 'JUE error'}
    #
    # verify SR creation and execute SR and SJ
    st = nil
    assert wait_for(timeout: 60) {
      st = @@sjctest.mds.sjc_tasks(tgtcarrier: tgtcarrier).first
    }, "no SR for tgtcarrier #{tgtcarrier}"
    assert @@sjctest.execute_verify(srid: st.srid), 'error creating SiView SJ'
    assert @@sv.claim_sj_execute(eqp: @@sjctest.sorter), 'error executing SJ'
    #
    # verify new slots
    $log.info "verifying slot map"
    li_new = @@sv.lot_info(lot, wafers: true)
    tgtslots.each_with_index {|tgtslot, idx|
      w = li_new.wafers.find {|w| w.slot == tgtslot}
      assert_equal wafers[idx], w.wafer, 'wrong wafer slot'
    }
  end


  # aux methods

  def self.create_parameters(lot, tgtcarrier, params={})
    li = @@sv.lot_info(lot, wafers: true)
    n = li.nwafers
    srccarrier = li.carrier
    srccat = @@sv.carrier_status(srccarrier).category
    tgtcat = @@sv.carrier_status(tgtcarrier).category
    values = [
      Array.new(n, '124'),
      Array.new(n, 'SEPARATE'),
      Array.new(n, 'QAJUE'),
      Array.new(n, 'QA-REQUESTOR'),
      Array.new(n, srccarrier),
      Array.new(n, srccat),
      Array.new(n, lot),
      li.wafers.collect {|w| w.wafer},
      Array.new(n, tgtcarrier),
      Array.new(n, tgtcat),
      params[:slots] || li.wafers.collect {|w| w.slot},
    ]
    @@testcarriers << srccarrier << tgtcarrier
    ret = String.new
    @@pnames.each_with_index {|pname, idx|
      ret += '|' + pname + '|' + values[idx].join(',')
    }
    return ret
  end

end
