=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author:  sfrieske, 2016-01-25
Version: 16.02

History:
  2017-01-05 sfrieske, use new  RTD::EmuRemote
=end

require 'SiViewTestCase'
require 'rtdserver/rtdclient'
require 'rtdserver/rtdemuremote'


# Testcases for RTD GatePdBTF Rule
#
# spec ITDC: http://vf1sfcd01:10080/setupfc-rest/v1/specs/C08-00001015/content?format=html
#
# setup:
# PG UTRTDGATEBTF1, PROD: UTRTDGATEBTF1.00, WIP level: 103 wfrs, Shift rate: 1500 (125 per hr - QA per 5')
# PG *, PROD: *, ERPITEM: *  defines defaults for GatePD and BankId; code defaults are O-GATE
class RTD_Rule_GatePdBTF < SiViewTestCase
  @@sv_defaults = {route: 'UTRTDGATEBTF.01', product: 'UTRTDGATEBTF1.00', carriers: '%'}
  @@gatepd = 'UTBTFGATE.01'      # default in rule: BTFGATE.01, can be overwritten by '* * *' in spec
  @@endpd = 'SORTSRT1.01'
  @@nonprobank1 = 'UT-BTFGATE1'  # default in rule: O-GATE, can be overwritten by '* * *' in spec
  @@nonprobank2 = 'UT-BTFGATE2'
  @@product_nospec = 'UTRTDGATEBTF0.00'
  @@product2 = 'UTRTDGATEBTF2.00'
  @@product3 = 'UTRTDGATEBTF3.00'

  @@opNo_qt = '2000.1010'

  @@rule = 'QA_JCAP_GatePD_BTF'
  @@real_rule = 'JCAP_GatePD_BTF'
  @@category = 'DISPATCHER/JCAP'
  @@bankout_expiration = 120     # 2', prod: 30'
  @@openote_text = 'GatePD_BTF:GatePass'

  @@apf_sync = 15


  def self.startup
    super
    @@rtdclient = RTD::Client.new($env)
    @@rtdremote = RTD::EmuRemote.new($env)
    @@rtdremote.add_emustation(@@real_rule, method: 'empty_reply')
    @@testlots = []  # to be deleted after the tests
    @@lots = []
  end

  def self.after_all_passed
    @@testlots.each {|lot| @@sv.delete_lot_family(lot)}
    @@rtdremote.delete_emustation(@@real_rule)
  end


  def setup
    @@testlots.each {|lot|
      # as long as QT controller rule is active:
      li = @@sv.lot_info(lot)
      if li.qtime
        assert_equal 0, @@sv.lot_opelocate(lot, @@opNo_gate)
        assert_equal 0, @@sv.lot_gatepass(lot)
      end
      assert @@sv.merge_lot_family(lot) if li.nwafers < 25
      assert_equal 0, @@sv.lot_cleanup(lot, opNo: @@opNo_first, route: @@svtest.route, forward: false)
      assert_equal 0, @@sv.schdl_change(lot, priority: '4') if li.priority != '4'
    }
    # wait for bankouts to expire
    @@lots.each {|lot|
      e = @@sv.lot_operation_history(lot, category: 'NonProBankOut').last || next
      t = @@sv.siview_time(e.timestamp) + @@bankout_expiration - Time.now
      ($log.info "waiting #{t}s for BankOut to expire"; sleep t) if t > 0
    }
  end


  def test00_setup
    $setup_ok = false
    #
    # verify the rule is actually called (not 'Default')
    assert res = @@rtdclient.dispatch_list('X', report: @@rule, category: @@category)
    assert_equal @@rule, res.rule
    #
    # delete old lots on the test route
    @@sv.lot_list(route: @@svtest.route).each {|lot| @@sv.delete_lot_family(lot)}
    # create test lots # 5 or 6 ??
    assert @@lots = @@svtest.new_lots(6), 'error creating test lots'
    @@testlots = @@lots
    assert @@lots_other = @@svtest.new_lots(6, product: @@product_nospec)
    @@testlots += @@lots_other
    #
    ops = @@sv.lot_processlist_in_route(@@lots.first)
    @@opNo_first = ops.keys.first
    @@opNo_gate = ops.find {|opNo, op| op == @@gatepd}.first
    @@opNo_btf = ops.keys[ops.keys.index(@@opNo_gate).next]
    @@opNo_end = ops.find {|opNo, op| op == @@endpd}.first
    #
    $setup_ok = true
  end

  def test11_smoke
    # send 100 wafers
    @@lots.take(4).each {|lot| assert_equal 0, @@sv.lot_opelocate(lot, @@opNo_gate)}
    res = rtd_lots
    assert_equal @@lots.take(4), res['GatePass'].sort, 'wrong RTD response'
    assert_empty res['NonProdBankIn']
    assert_empty res['NonProdBankOut']
    assert_empty res['NonProdBankMove']
    # trying to send 125 wafers in 5 full lots, 103 allowed -> 1 bankin
    assert_equal 0, @@sv.lot_opelocate(@@lots[4], @@opNo_gate)
    res = rtd_lots
    assert_equal 4, res['GatePass'].size
    assert_equal 1, res['NonProdBankIn'].size
    assert_empty res['NonProdBankOut']
    assert_empty res['NonProdBankMove']
    assert_equal @@lots.take(5), (res['GatePass'] + res['NonProdBankIn']).sort, "wrong lots"
  end

  def test12_nospec
    # send 1 nospec lot alone
    assert_equal 0, @@sv.lot_opelocate(@@lots_other.first, @@opNo_gate)
    res = rtd_lots
    assert_equal [@@lots_other.first], res['GatePass']
    # send 125 nospec wafers
    @@lots_other.each {|lot| assert_equal 0, @@sv.lot_opelocate(lot, @@opNo_gate)}
    res = rtd_lots
    assert_equal @@lots_other, res['GatePass'].sort, 'wrong RTD response'
    # send along with 100 wafers with spec entry
    @@lots.take(4).each {|lot| assert_equal 0, @@sv.lot_opelocate(lot, @@opNo_gate)}
    res = rtd_lots
    assert_equal (@@lots.take(4) + @@lots_other).sort, res['GatePass'].sort, "wrong lots"
    # trying to send along with 125 wafers in 5 full lots, 103 allowed -> 1 bankin
    assert_equal 0, @@sv.lot_opelocate(@@lots[4], @@opNo_gate)
    res = rtd_lots
    assert_equal 4 + @@lots_other.size, res['GatePass'].size
    assert_equal 1, res['NonProdBankIn'].size
    assert @@lots.member?(res['NonProdBankIn'].first), 'wrong lot NonProdBankIn'
    assert_empty res['NonProdBankOut']
    assert_empty res['NonProdBankMove']
  end

  def test13_nocarrier
    # no carrier -> not returned (cannot be GatePassed nor banked in)
    [@@lots.first, @@lots_other.first].each {|lot|
      assert_equal 0, @@sv.lot_opelocate(lot, @@opNo_gate)
      assert_equal 0, @@sv.wafer_sort(lot, '')
    }
    res = rtd_lots
    assert_empty res['rows'], 'lots without carrier must not be returned'
  end

  def test14_scrapped
    # scrapped -> not returned (cannot be GatePassed nor banked in)
    [@@lots.first, @@lots_other.first].each {|lot|
      assert_equal 0, @@sv.lot_opelocate(lot, @@opNo_gate)
      assert_equal 0, @@sv.scrap_wafers(lot)
    }
    res = rtd_lots
    assert_empty res['rows'], 'scrapped lots must not be returned'
  end

  def test15_onhold
    # onhold -> not returned (cannot be GatePassed but banked in)
    [@@lots.first, @@lots_other.first].each {|lot|
      assert_equal 0, @@sv.lot_opelocate(lot, @@opNo_gate)
      assert_equal 0, @@sv.lot_hold(lot, nil)
    }
    res = rtd_lots
    assert_empty res['rows'], 'onhold lots must not be returned'
  end

  def test17_qt
    # try to send 150 wafers
    @@lots.take(6).each {|lot| assert_equal 0, @@sv.lot_opelocate(lot, @@opNo_gate)}
    res = rtd_lots
    assert_equal 2, res['NonProdBankIn'].size, '2 lots must be banked in'
    # try to send 150 wafers including a QT lot
    lot = res['NonProdBankIn'].last
    $log.info "QT lot: #{lot.inspect}"
    assert_equal 0, @@sv.lot_opelocate(lot, @@opNo_qt)
    assert_equal 0, @@sv.lot_gatepass(lot)
    assert_equal 0, @@sv.lot_opelocate(lot, @@opNo_gate)
    sleep 3
    assert @@sv.lot_info(lot).qtime, 'lot has no QTime'
    res = rtd_lots
    assert res['GatePass'].member?(lot), 'QT lot must be gate passed'
    assert_equal lot, res['GatePass'].first, 'QT lot must have highest prio'
    assert_equal 2, res['NonProdBankIn'].size, '2 lots must be banked in'
  end

  def test18_bankin  # this test can be run every 2 (prod: 30) minutes only with the same lots!
    # try to send 150 wafers
    @@lots.take(6).each {|lot| assert_equal 0, @@sv.lot_opelocate(lot, @@opNo_gate)}
    res = rtd_lots
    assert_equal 4, res['GatePass'].size, '2 lots must be banked in'
    assert_equal 2, res['NonProdBankIn'].size, '2 lots must be banked in'
    # bank the NonProdBankIn lots in -> remaining 4 lots appear again for GatePass
    $log.info "NonProBankIn lots: #{res['NonProdBankIn']}"
    res['NonProdBankIn'].each {|lot| assert_equal 0, @@sv.lot_nonprobankin(lot, @@nonprobank1)}
    res2 = rtd_lots
    assert_equal res['GatePass'].sort, res2['GatePass'].sort
    assert_empty res2['NonProdBankIn']
    # # bank the NonProdBankIn lots _out_ -> 2 other lots must be selected for bankin -> FAILS because lots are selected by prio
    res['NonProdBankIn'].each {|lot| assert_equal 0, @@sv.lot_nonprobankout(lot)}
    # res3 = rtd_lots
    # assert_equal 4, res3['GatePass'].size, '2 lots must be banked in'
    # assert_equal 2, res3['NonProdBankIn'].size, 'wrong number of lots'
    # assert_empty (res['NonProdBankIn'] & res3['NonProdBankIn']), 'picked wrong lots for bankin'
    # remove 2 GatePass lots from the gate PD -> banked out lots can be gate passed
    res['GatePass'].take(2).each {|lot| assert_equal 0, @@sv.lot_opelocate(lot, @@opNo_first)}
    res4 = rtd_lots
    assert_equal 4, res4['GatePass'].size, 'wrong number of lots'
    assert_equal res['NonProdBankIn'], res['NonProdBankIn'] & res4['GatePass'], 'wrong lots for GatePass'
    assert_empty res4['NonProdBankIn']
  end

  def test19_bankout
    @@lots.take(6).each {|lot|
      assert_equal 0, @@sv.lot_opelocate(lot, @@opNo_gate)
      assert_equal 0, @@sv.lot_nonprobankin(lot, @@nonprobank1)
    }
    res = rtd_lots
    assert_equal 0, res['GatePass'].size, 'wrong number of lots'
    assert_equal 0, res['NonProdBankIn'].size, 'wrong number of lots'
    assert_equal 4, res['NonProdBankOut'].size, 'wrong number of lots'
  end

  def test20_bank_move
    # put @@product2 lots on @@nonprobank1 -> moved to @@noprodbank2
    @@lots_other.each {|lot| assert_equal 0, @@sv.schdl_change(lot, product: @@product2, route: @@svtest.route)}
    @@lots_other.each {|lot|
      assert_equal 0, @@sv.lot_opelocate(lot, @@opNo_gate)
      assert_equal 0, @@sv.lot_nonprobankin(lot, @@nonprobank1)
    }
    res = rtd_lots
    assert_equal @@lots_other, res['NonProdBankMove'].sort, 'wrong lots'
  end

  def test21_prio # beware, wait 2 min after bankout tests!
    # try to send 150 wafers
    @@lots.take(6).each {|lot| assert_equal 0, @@sv.lot_opelocate(lot, @@opNo_gate)}
    res = rtd_lots
    assert_equal 2, res['NonProdBankIn'].size, '2 lots must be banked in'
    # send 100 wafers + prio 0 + prio 1 lots
    assert_equal 0, @@sv.schdl_change(@@lots[0], priority: 0)
    assert_equal 0, @@sv.schdl_change(@@lots[1], priority: 1)
    res = rtd_lots
    assert_equal @@lots.take(6), res['GatePass'].sort, 'all lots must pass'
    # try to send 100 wafers + prio4+5: 2 lots banked in
    assert_equal 0, @@sv.schdl_change(@@lots[0], priority: 5)
    assert_equal 0, @@sv.schdl_change(@@lots[1], priority: 4)
    res = rtd_lots
    assert_equal 2, res['NonProdBankIn'].size, '2 lots must be banked in'
    # send 100 wafers + prio 2 + prio 3 lots
    assert_equal 0, @@sv.schdl_change(@@lots[0], priority: 2)
    assert_equal 0, @@sv.schdl_change(@@lots[1], priority: 3)
    res = rtd_lots
    assert_equal @@lots.take(6), res['GatePass'].sort, 'all lots must pass'
  end

  def XXtest22_prio_order
    # try to send 150 wafers
    @@lots.take(6).each {|lot| assert_equal 0, @@sv.lot_opelocate(lot, @@opNo_gate)}
    res = rtd_lots
    assert_equal 2, res['NonProdBankIn'].size, '2 lots must be banked in'
    # change prio, lots must be GatePassed
    assert_equal 0, @@sv.schdl_change(res['NonProdBankIn'][0], priority: 2)
    assert_equal 0, @@sv.schdl_change(res['NonProdBankIn'][1], priority: 3)
    res2 = rtd_lots
    assert_equal 2, res2['NonProdBankIn'].size, '2 lots must be banked in'
    assert_empty res['NonProdBankIn'] & res2['NonProdBankIn'], 'wrong lots for bank in'
  end

  def test31_wip_level
    # WIP level: 103 wfrs
    # put 100 wafers in the BTF area
    @@lots[0..3].each {|lot| assert_equal 0, @@sv.lot_opelocate(lot, @@opNo_btf)}
    # 2 full lots at GatePD -> BankIn
    @@lots[4..5].each {|lot| assert_equal 0, @@sv.lot_opelocate(lot, @@opNo_gate)}
    res = rtd_lots
    assert_equal 0, res['GatePass'].size, 'no GatePass expected'
    assert_equal 2, res['NonProdBankIn'].size, '2 lots must be banked in'
    # create a split lot with 3 wafers and verify it gets GatePassed
    assert lc = @@sv.lot_split(@@lots[4], 3)
    res = rtd_lots
    assert_equal [lc], res['GatePass'], 'wrong GatePass lot'
    assert_equal @@lots[4..5], res['NonProdBankIn'].sort, 'wrong NonProdBankIn lots'
  end

  def test32_going_rate # FAILS
    # WIP level: 1000 wfrs (no effective limitation); Shift rate: 660 (10' for QA), 55 per hr - (QA per 100s)
    # put 4 full lots (100 wafers) in the BTF area
    @@lots_other.each {|lot| assert_equal 0, @@sv.schdl_change(lot, product: @@product3, route: @@svtest.route)}
    @@lots_other[0..3].each {|lot| assert_equal 0, @@sv.lot_opelocate(lot, @@opNo_btf)}
    # 2 full lots at GatePD -> 2 GatePass
    @@lots_other[4..5].each {|lot| assert_equal 0, @@sv.lot_opelocate(lot, @@opNo_gate)}
    res = rtd_lots
    assert_equal 2, res['GatePass'].size, '2 lots must be gate passed'
    # set openotes for 1 lot (25 wafers) -> 1 GatePass
    assert_equal 0, @@sv.lot_openote_register(@@lots_other[0], 'QA Test', @@openote_text)
    res = rtd_lots
    assert_equal 1, res['GatePass'].size, '1 lot must be gate passed'
    # set openotes for 2 lots (50 wafers) -> no GatePass
    assert_equal 0, @@sv.lot_openote_register(@@lots_other[1], 'QA Test', @@openote_text)
    res = rtd_lots
    assert_empty res['GatePass'], '1 lot must be gate passed'
  end


  # aux methods

  # call rule and return hash of lot: action
  def rtd_lots(params={})
    ($log.info "waiting #{@@apf_sync}s for APF replication"; sleep @@apf_sync) unless params[:nowait]
    res = @@rtdclient.dispatch_list('X', report: @@rule, category: @@category) || return
    ($log.warn 'wrong RTD rule called, setup issue?'; return) unless res.rule == @@rule
    # identify columns
    col_lot = res.headers.find_index {|e| e == 'lot_id'} || ($log.warn 'RTD column missing'; return)
    col_action = res.headers.find_index {|e| e == 'jCAP_scenario'} || ($log.warn 'RTD column missing'; return)
    #
    ret = {'GatePass'=>[], 'NonProdBankIn'=>[], 'NonProdBankOut'=>[], 'NonProdBankMove'=>[], 'rows'=>res.rows}
    res.rows.each {|r| ret[r[col_action]] << r[col_lot]}
    return ret
  end

end
