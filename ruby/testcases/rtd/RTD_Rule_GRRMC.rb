=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: sfrieske, 2021-05-05

Version: 21.04

History:

Notes:
  see http://f1onewiki:21080/display/MGA/JCAP%3A+GRR+Measurement+Controller
  return overall ListOfTools if [RemainingQT] > [90mins + CycleTime_to_TargetOper + 120min SafetyBuffer]
=end

require 'SiViewTestCase'
require 'rtdserver/rtdclient'


class RTD_Rule_GRRMC < SiViewTestCase
  @@sv_defaults = {product: 'UT-PRODUCT-GRR-PFL.01', route: 'UTRT-GRR-ORIG.01', nwafers: 3}
  @@original_pds = %w(UTPDM-GRR-ORIG-1T.01 UTPDM-GRR-ORIG-2T.01 UTPDM-GRR-ORIG-3T.01 UTPDM-GRR-ORIG-NT.01)
  @@route_qtshort = 'UTRT-GRR-ORIG-QT5.01'
  @@route_qtlong = 'UTRT-GRR-ORIG-QT100.01'
  @@opNo_qt = '1000.0010'

  @@rtd_rule = 'JCAP_GRRMC'
  @@rtd_category = 'DISPATCHER/JCAP'
  @@apf_delay = 15

  @@testlots = []


  def self.after_all_passed
    @@testlots.each {|lot| @@sv.delete_lot_family(lot)}
  end


  def test00_setup
    $setup_ok = false
    #
    assert @@rtdclient = RTD::Client.new("#{$env}.emu")
    #
    # lot preparation
    assert lots = @@svtest.new_lots(1), 'error creating test lots'
    @@testlots += lots
    #
    $setup_ok = true
  end

  def test11_happy
    lot = @@testlots[0]
    failed = []
    @@original_pds.each {|op|
      opeqps = []
      @@sv.eqp_list_byop(@@svtest.product, op).each {|e|
        next if e.equipmentCategory != 'Measurement'
        eqp = e.equipmentID.identifier
        # next if eqp == 'A-DUMMY'
        # ### currently the rule has no filtering, UTC003 is valid as of now:
        # next if eqp == 'UTC003'
        assert_equal 0, @@sv.eqp_cleanup(eqp, mode: 'Auto-3', notify: false)
        opeqps << eqp
      }
      $log.info "waiting #{@@apf_delay} s for APF replication"; sleep @@apf_delay
      assert rows = call_rtd(lot, op)
      # assert_equal opeqps.sort, rows.flatten.sort, "wrong response for op #{op}"
      if opeqps.sort != rows.flatten.sort
        $log.warn "wrong response for op #{op}: #{rows.flatten.sort}"
        failed << op
      end
    }
    assert_empty failed
  end

  def test12_filter_meas
    lot = @@testlots[0]
    op = 'UTPDM-GRR-ORIG-1T.01'
    eqps = @@sv.eqp_list_byop(@@svtest.product, op).collect {|e| e.equipmentID.identifier}.sort
    opeqps = []
    category_dummy_found = false
    category_process_found = false
    eqps.each {|eqp|
      eqpi = @@sv.eqp_info(eqp)
      if eqpi.category == 'Dummy'
        category_dummy_found = true
      elsif eqpi.category == 'Process'
        category_process_found = true
        assert_equal 0, @@sv.eqp_cleanup(eqp, mode: 'Auto-3', notify: false)
      else
        opeqps << eqp
        assert_equal 0, @@sv.eqp_cleanup(eqp, mode: 'Auto-3', notify: false)
      end
    }
    assert category_dummy_found, 'wrong recipe setup: no Dummy tool'
    assert category_process_found, 'wrong recipe setup: no Process tool'
    $log.info "waiting #{@@apf_delay} s for APF replication"; sleep @@apf_delay
    assert rows = call_rtd(lot, op)
    assert_equal opeqps, rows.flatten.sort, "wrong response for op #{op}"
  end

  def test21_offline
    lot = @@testlots[0]
    op = 'UTPDM-GRR-ORIG-3T.01'
    eqps = @@sv.eqp_list_byop(@@svtest.product, op).collect {|e| e.equipmentID.identifier}.sort
    eqps.each_with_index {|eqp, idx|
      mode = (idx == 1 ? 'Off-Line-1' : 'Auto-3')
      assert_equal 0, @@sv.eqp_cleanup(eqp, mode: mode, notify: false)
    }
    $log.info "waiting #{@@apf_delay} s for APF replication"; sleep @@apf_delay
    assert rows = call_rtd(lot, op)
    refute_equal eqps, rows.flatten.sort, "wrong response for op #{op}"
  end

  def test22_inhibit
    lot = @@testlots[0]
    op = 'UTPDM-GRR-ORIG-3T.01'
    eqps = @@sv.eqp_list_byop(@@svtest.product, op).collect {|e| e.equipmentID.identifier}.sort
    eqps.each {|eqp|
      assert_equal 0, @@sv.eqp_cleanup(eqp, mode: 'Auto-3', notify: false)
    }
    assert @@sv.inhibit_entity(:eqp, eqps[0])
    $log.info "waiting #{@@apf_delay} s for APF replication"; sleep @@apf_delay
    assert rows = call_rtd(lot, op)
    refute_equal eqps, rows.flatten.sort, "wrong response for op #{op}"
  end

  def test23_E10
    lot = @@testlots[0]
    op = 'UTPDM-GRR-ORIG-3T.01'
    eqps = @@sv.eqp_list_byop(@@svtest.product, op).collect {|e| e.equipmentID.identifier}.sort
    eqps.each {|eqp|
      assert_equal 0, @@sv.eqp_cleanup(eqp, mode: 'Auto-3', notify: false)
    }
    assert_equal 0, @@sv.eqp_status_change(eqps[0], '2NDP')
    $log.info "waiting #{@@apf_delay} s for APF replication"; sleep @@apf_delay
    assert rows = call_rtd(lot, op)
    refute_equal eqps, rows.flatten.sort, "wrong response for op #{op}"
  end

  def test31_QTlong
    assert lot = @@svtest.new_lot, 'error creating test lot'
    @@testlots << lot
    @@sv.schdl_change(lot, route: @@route_qtlong)
    assert_equal @@route_qtlong, @@sv.lot_info(lot).route
    assert_equal 0, @@sv.lot_opelocate(lot, @@opNo_qt), 'error locating lot'
    assert_equal 0, @@sv.lot_gatepass(lot), 'error gatepassing lot'
    refute_nil qtinfo = @@sv.lot_qtime_list(lot).first, 'lot has no QT'
    tend = @@sv.siview_time(qtinfo.strQtimeInfo.first.strQtimeActionInfo.first.qrestrictionTargetTimeStamp)
    assert tend - Time.now > 1800, 'QT too short'
    op = 'UTPDM-GRR-ORIG-3T.01'
    eqps = @@sv.eqp_list_byop(@@svtest.product, op).collect {|e| e.equipmentID.identifier}.sort
    eqps.each {|eqp|
      assert_equal 0, @@sv.eqp_cleanup(eqp, mode: 'Auto-3', notify: false)
    }
    $log.info "waiting #{@@apf_delay} s for APF replication"; sleep @@apf_delay
    assert rows = call_rtd(lot, 'UTPDM-GRR-ORIG-3T.01', opNo: '1000.0500')
    assert_equal eqps, rows.flatten.sort, "wrong response for op #{op}"
  end

  def test32_QTshort
    assert lot = @@svtest.new_lot, 'error creating test lot'
    @@testlots << lot
    @@sv.schdl_change(lot, route: @@route_qtshort)
    assert_equal @@route_qtshort, @@sv.lot_info(lot).route
    assert_equal 0, @@sv.lot_opelocate(lot, @@opNo_qt), 'error locating lot'
    assert_equal 0, @@sv.lot_gatepass(lot), 'error gatepassing lot'
    refute_nil qtinfo = @@sv.lot_qtime_list(lot).first, 'lot has no QT'
    tend = @@sv.siview_time(qtinfo.strQtimeInfo.first.strQtimeActionInfo.first.qrestrictionTargetTimeStamp)
    assert tend - Time.now < 1800, 'QT too long'
    op = 'UTPDM-GRR-ORIG-3T.01'
    eqps = @@sv.eqp_list_byop(@@svtest.product, op).collect {|e| e.equipmentID.identifier}.sort
    eqps.each {|eqp|
      assert_equal 0, @@sv.eqp_cleanup(eqp, mode: 'Auto-3', notify: false)
    }
    $log.info "waiting #{@@apf_delay} s for APF replication"; sleep @@apf_delay
    assert rows = call_rtd(lot, 'UTPDM-GRR-ORIG-3T.01', opNo: '1000.0500')
    assert_empty rows, "wrong response for op #{op}"
  end

  def test33_wrong_carrier_category
    assert ec = @@svtest.get_empty_carrier(carrier_category: 'C4', carrier: '%'), 'no empty carrier'
    assert lot = @@svtest.new_lot(carrier: ec), 'error creating test lot'
    @@testlots << lot
    op = 'UTPDM-GRR-ORIG-3T.01'
    eqps = @@sv.eqp_list_byop(@@svtest.product, op).collect {|e| e.equipmentID.identifier}.sort
    eqps.each {|eqp|
      assert_equal 0, @@sv.eqp_cleanup(eqp, mode: 'Auto-3', notify: false)
    }
    $log.info "waiting #{@@apf_delay} s for APF replication"; sleep @@apf_delay
    assert_empty call_rtd(lot, op)
  end


  # aux methods

  def call_rtd(lot, op, params={})
    li = @@sv.lot_info(lot)
    cparams = [
      'lot_id', lot, 'prodspec_id', params[:product] || li.product,
      'orig_mainpd_id', params[:route] || li.route,
      'orig_pd_id', op,
      'orig_ope_no', params[:opNo] || 'XX'  #@@grroriginalopenos[lot]
    ]
    assert res = @@rtdclient.dispatch_list(nil, report: @@rtd_rule, category: @@rtd_category, parameters: cparams)
    assert_equal @@rtd_rule, res.rule, 'wrong rule called'
    assert_equal ['eqp_id'], res.headers, 'wrong headers returned'
    return res.rows
  end

end
