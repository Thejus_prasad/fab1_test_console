=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author:  sfrieske, 2022-01-10

Version: 1.0.0

History:

Notes:
  MSR 1850955, https://gfoundries-my.sharepoint.com/personal/kwiedemu_gfoundries_com/Documents/MSR1850955.xlsx
  http://f1onewiki:21080/display/F1Disp/DokuJCAPMOVELOTS
=end

require 'rtdserver/rtdclient'
require 'rtdserver/rtdemuremote'
require 'SiViewTestCase'

# Under Construction!

class RTD_Rule_MoveLots < SiViewTestCase
  @@sv_defaults = {product: 'ITDC-JCAP-NONPROBANKINFORQUAL.01', route: 'ITDC-JCAP-NONPROBANKINFORQUAL.01'}
  #
  @@op_chkplnhld = 'CHKPLNHLD.01'
  @@chkplnhld_automationtype = 'CHKPLNHOLD'  # 'CHKPLNHOLD-ITDC'??
  #
  @@rtd_rule = 'JCAP_MOVE_LOTS'
  @@rtd_category = 'DISPATCHER/JCAP'  # will be moved to 'DISPATCHER/RULES'
  # @@columns = %w(lot_id dest_bank_id mainpd_id ope_no)
  # lots with O-PC hold by user X-JNOPBK will be generally discarded
  @@user_discard = 'X-JNOPBK'
  @@reason_discard = 'O-PC'
  # use cases in the dispatcher call and destination bank
  @@usecases = {
    towing:         ['NONPROBANKIN_TOWING_ONHOLD', 'O-TOWING'], # was JCAP_MoveLotsOnHoldToBank
    customerstage:  ['NONPROBANKIN_CUSTOMERSTAGE_ONHOLD', 'X-CUSTOMERSTAGE'], # new
    waferpack:      ['NONPROBANKIN_WAFERPACK_ALL', 'O-PACK'],                 # new
    chkplnhold:     ['NONPROBANKIN_CHKPLNHOLD_ALL', 'T-END'],   # was JCAP_NonProBankInQual
    skipchkplnhold: ['SKIP_CHKPLNHOLD_ALL', 'T-END']            # same bank as above, bank ignored by JCAP, was JCAP_NonProBankInQual
  }
  # TODO: JCAP_NonProBankInQual has slt dependent banks:  @@bank_dev = 'T-END' @@bank_tqual = 'XY-PLN-HOLD'
  #
  # lot parameters :towing
  @@slt_towing = 'RD'   # config/spec
  @@slt_notowing = 'PO' 


  @@rule_delay = 180  # min_family_not_moved_time, min_hold_time, RTD config!

  @@testlots = []


  def self.shutdown
    @@rtdremote.delete_emustation('', rule: @@rtd_rule)
  end

  def self.after_all_passed
    @@testlots.each {|lot| @@sv.delete_lot_family(lot)}
  end

  def setup
    return unless self.class.class_variable_defined?(:@@rtdremote)
    @@rtdremote.add_emustation('', rule: @@rtd_rule, method: 'empty_reply')
    @@sjctest.cleanup_sort_requests(creator: 'DEKIT')
  end

  def test00_setup
    $setup_ok = false
    #
    assert @@rtdclient = RTD::Client.new($env)  # ("#{$env}.emu")
    assert res = @@rtdclient.dispatch_list(nil, report: @@rtd_rule, category: @@rtd_category)
    assert_equal @@rtd_rule, res.rule, 'wrong rule, RTD setup?'
    assert @@rtdremote = RTD::EmuRemote.new($env)
    #
    # assert_equal @@chkplnhld_automationtype, @@sv.user_data_pd(@@op_chkplnhld)['AutomationType'], 'wrong PD UDATA AutomationType'
    #
    $setup_ok = true
  end

  def test11_towing
    usecase, bank = @@usecases[:towing]
    assert rows = self.class.call_rule(usecase)
    verify_result(rows, bank)
    # TODO: compare with JCAP_MOVE_LOTS_ONHOLD_TO_BANK, except O-PC holds
    assert @@rtdclient1 = RTD::Client.new($env)  # ("#{$env}.emu")
    assert $res = res1 = @@rtdclient1.dispatch_list(nil, report: 'JCAP_MOVE_LOTS_ONHOLD_TO_BANK', category: @@rtd_category)
    assert_equal rows.size, res1.rows.size, 'wrong number of rows'
  end

  def test12_towing_slt
    assert lot1 = @@svtest.new_lot(sublottype: @@slt_towing), 'error creating testlot'
    assert lot1 = @@svtest.new_lot(sublottype: @@slt_notowing), 'error creating testlot'
    #
    usecase, bank = @@usecases[:towing]
    assert rows = self.class.call_rule(usecase)
    verify_result(rows, bank)
  end

  def test21_customerstage
    usecase, bank = @@usecases[:customerstage]
    assert rows = self.class.call_rule(usecase)
    verify_result(rows, bank)
  end

  def test31_waferpack
    usecase, bank = @@usecases[:waferpack]
    assert rows = self.class.call_rule(usecase)
    verify_result(rows, bank)
  end

  def test41_chkplnhold
    usecase, bank = @@usecases[:chkplnhold]
    assert rows = self.class.call_rule(usecase)
    verify_result(rows, bank)
  end

  def test51_skipchkplnhold
    usecase, bank = @@usecases[:skipchkplnhold]
    assert rows = self.class.call_rule(usecase)
    verify_result(rows, bank)
  end


  # aux methods

  def self.call_rule(usecase)
    parameters = ['UseCase', usecase]
    res = @@rtdclient.dispatch_list(nil, report: @@rtd_rule, category: @@rtd_category, parameters: parameters)
    ($log.warn 'wrong rule, RTD setup?'; return) if res.rule != @@rtd_rule
    return res.rows
  end

  def verify_result(rows, bank)
    $log.info "verifying result, #{rows.size} rows, bank: #{bank.inspect}"
    refute_empty rows, 'no lots returned'
    failed = []
    rows.each {|row| failed << row if row[1] != bank}
    assert_empty failed, 'rows with wrong bank'
  end

end
