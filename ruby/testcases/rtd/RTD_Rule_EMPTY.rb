=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author:  sfrieske, 2015-11-30
Version: 21.03

History:
  2016-01-15 sfrieske, added testcases
  2016-10-06 sfrieske, added testcases
  2017-03-31 sfrieske, added testcases, use RTD::Test as in other rule tests
  2017-11-07 sfrieske, added testcase for creator (returned carriers are not empty if creator is MQSRA)
  2021-03-22 sfrieske, minor adjustments for v21.03

Notes:
  Testcases for RTD EMPTY Rule, OSN 66
  ITDC spec: http://vf1sfcd01:10080/setupfc-rest/v1/specs/C08-00001047/content?format=html
    class order: http://vf1sfcd01:10080/setupfc-rest/v1/specs/C08-00001060/content?format=html

  requirements: carrier MI/SI and AVAILABLE, AmhsArea of eqp and carrier's stocker must match,
  LogicalRecipe with recipe parameter DestCarrierType Class1 for class 1
=end

require 'misc/rtddb'
require 'rtdserver/rtdclient'
require 'siviewtest'
require 'SiViewTestCase'


class RTD_Rule_EMPTY < SiViewTestCase
  @@sv_defaults = {product: 'UT-PRODUCT000.AWS', route: 'UTRT001.AWS'} #, carriers: 'EQA%'}
  @@rtd_rule = 'EMPTY_QA'
  @@rtd_category = 'DISPATCHER/QA'
  @@carriers = 'EQA%'
  @@customerfosbs = 'EE%'
  @@op1 = 'UTPD009.01'  # has recipe UTLR003.01 with parameter DestCarrierType=Class1
  @@sorter = 'UTS001'
  @@sorter2 = 'UTS002'
  @@stocker = 'UTSTO11'
  @@stocker2 = 'UTSTO13'
  @@shelf = 'UTSHF01'
  @@rule = 'EMPTY_QA'
  @@id_tags = ['EqpID', 'SrtID']
  @@foupcount = 99  # to override rule builtin default of 2
  @@starts1 = 0     # for Class1 carrier preparation
  @@starts2 = 2     # for Class2 carrier preparation
  @@starts3 = 4     # for Class3 carrier preparation
  # for customer FOSBs
  ##@@spec1 = 'C08-00001036'
  # for spec checks
  @@spec_topics = %w(CARRIER_CLASS_DEFINITION CARRIER_CONTROL_LIMITS AGED_FOUP_LIMITS EMPTY_FOUP_RULE)
  @@spec_parameters = %w(oper_start_count_min oper_start_count_max total_min_age total_max_age)
  @@spec_limits = {
    'FEOL'=>{
      'Class1'=>[nil, 2, 0, 99], 'Class2'=>[2, 4, 0, 99], 'Class3.0'=>[4, nil, 0, 99]
    },
    'MOL'=>{
      'Class1'=>[nil, 2, 0, 99], 'Class2'=>[2, 4, 0, 99], 'Class3.0'=>[4, nil, 0, 99]
    },
    'BEOL'=>{
      'Class1'=>[nil, 2, 0, 99], 'Class2'=>[2, 4, 0, 99], 'Class3.0'=>[4, nil, 0, 99]
    },
    'HK'=>{
      'Class1'=>[nil, 2, 0, 99], 'Class2'=>[2, 4, 0, 99], 'Class3.0'=>[4, nil, 0, 99]
    },
  }

  @@apf_delay = 15
  @@testlots = []


  def self.shutdown
    @@testlots.each {|lot| @@sv.delete_lot_family(lot)}
    @@sv.eqp_status_change(@@stocker, '2WPR')
    @@sv.carrier_list(carrier: @@carriers).each {|c|
      @@sv.carrier_status_change(c, 'NOTAVAILABLE', check: true, silent: true)
    }
    @@sv.carrier_list(carrier: @@customerfosbs).each {|c|
      @@sv.carrier_status_change(c, 'NOTAVAILABLE', check: true, silent: true)
    }
    nil
  end


  def setup
    @@sv.carrier_list(carrier: @@carriers).each {|c|
      @@sv.carrier_status_change(c, 'NOTAVAILABLE', check: true, silent: true)
    }
  end


  def test00_setup
    $setup_ok = false
    #
    # verify spec setup
    @@mds = RTD::MDS.new($env)
    @@spec_topics.each {|topic|
      assert_equal 1, @@mds.specs_bytopic(topic).size, "ambigous specs for #{topic}"
    }
    @@spec_limits.each_pair {|category, cdata|
      $log.info "verifying spec limits for category #{category}"
      assert cdef = @@mds.carrier_class_definition(category), 'no data for this category'
      cdata.each_pair {|classname, classvalues|
        $log.info "  #{classname}: #{classvalues}"
        assert entry = cdef.find {|e| e['carrier_class'] == classname}, 'no data for this class'
        @@spec_parameters.each_with_index {|pname, i|
          if classvalues[i].nil?
            assert_nil entry[pname], "wrong #{pname}"
          else
            assert_equal classvalues[i], entry[pname], "wrong #{pname}"
          end
        }
      }
    }
    #
    @@rtdclient = RTD::Client.new($env)
    @@callparameters = ['cast_regex', @@carriers.sub('%', ''), 'FOUPCount', @@foupcount]   # override rule builtin default of 2
    @@callparameters_eqpid = @@callparameters + ['EqpID', @@sorter] 
    @@callparameters_srtid = @@callparameters + ['SrtID', @@sorter] 
    # verify correct rule will be called
    assert res = @@rtdclient.dispatch_list(nil, report: @@rtd_rule, category: @@rtd_category)
    assert_equal @@rtd_rule, res.rule, 'wrong rule, RTD setup?'
    #
    # verify AmhsAreas
    assert @@svtest.eqp_location(@@sorter, {'AmhsArea'=>'F36'})
    assert @@svtest.stocker_location(@@svtest.stocker, {'AmhsArea'=>'F36'})
    assert @@svtest.stocker_location(@@shelf, {'AmhsArea'=>'F36'})
    assert_equal 'Shelf', @@sv.stocker_info(@@shelf).stockerType, 'must be a shelf'
    assert @@svtest.eqp_location(@@sorter2, {'AmhsArea'=>'F38'})
    assert @@svtest.stocker_location(@@stocker2, {'AmhsArea'=>'F38'})
    #
    # verify recipe parameter
    assert lrcp = @@sm.object_info(:pd, @@op1).first.specific[:lrcps][nil]
    assert rparam = @@sm.object_info(:lrcp, lrcp).first.specific[:recipe].first[1]['DestCarrierType']
    assert_equal 'Class1', rparam, "wrong recipe parameter for #{lrcp} (op: #{@@op1})"
    #
    # create 2 FEOL lots and 1 monitor lot
    #@@sv.carrier_list(carrier: 'EQAFEOL%').take(3).each {|c| @@svtest.cleanup_carrier(c, make_empty: true)}
    assert lots = @@svtest.new_lots(2), 'error creating test lots'
    @@testlots += lots
    @@lot, @@lot1 = lots
    # lot1 stays at pd with the recipe parameter DestCarrierType=Class1
    assert_equal 0, @@sv.lot_opelocate(@@lot1, nil, op: @@op1), 'error locating lot'
    assert @@monitorlot = @@svtest.new_controllot('Equipment Monitor'), 'error creating monitor lot'
    @@testlots << @@monitorlot
    #
    $setup_ok = true
  end

  def test10_smoke
    $setup_ok = false
    #
    # use MOL (without lotlist all carriers are returned, FEOL has special rules)
    ccat = 'MOL'
    cc = @@sv.carrier_list(carrier: @@carriers, carrier_category: ccat).take(3)
    assert_equal 3, cc.size, "not enough carriers #{@@carriers}, ccat #{ccat}"
    assert age_carrier(cc[0], @@starts1)
    assert age_carrier(cc[1], @@starts2)
    assert age_carrier(cc[2], @@starts3)
    $log.info "waiting #{@@apf_delay} s for APF replication"; sleep @@apf_delay
    @@id_tags.each {|id_tag|
      # rparams = ['cast_regex', @@carriers.sub('%', ''), 'FOUPCount', @@foupcount, id_tag]
      # EqpID passed, at least one carrier returned  attn: Carrier Commision Date < 5 yrs or high value in spec
      refute_empty self.class.call_empty_rule(ccat, @@callparameters + [id_tag, @@sorter])
      # EqpID empty, no carriers returned
      assert_empty self.class.call_empty_rule(ccat, @@callparameters + [id_tag, ''])
      # sorter in other AmhsArea, no carriers returned
      assert_empty self.class.call_empty_rule(ccat, @@callparameters + [id_tag, @@sorter2])
    }
    # carriers NOTAVAILABLE, no carriers returned
    cc.each {|c| assert_equal 0, @@sv.carrier_status_change(c, 'NOTAVAILABLE')}
    $log.info "waiting #{@@apf_delay} s for APF replication"; sleep @@apf_delay
    @@id_tags.each {|id_tag|
      assert_empty self.class.call_empty_rule(ccat, @@callparameters + [id_tag, @@sorter]), 'GG replication not working correctly?'
    }
    #
    $setup_ok = true
  end

  def test11_no_limit
    # carriers: 1 class1, 1 class2, 1 class3; no limit
    ccat = 'MOL'
    cc = @@sv.carrier_list(carrier: @@carriers, carrier_category: ccat).take(3)
    assert_equal 3, cc.size, "not enough carriers #{@@carriers}, ccat #{ccat}"
    assert age_carrier(cc[0], @@starts1)
    assert age_carrier(cc[1], @@starts2)
    assert age_carrier(cc[2], @@starts3)
    $log.info "waiting #{@@apf_delay} s for APF replication"; sleep @@apf_delay
    @@id_tags.each {|id_tag|
      $log.info "-- testing with id_tag #{id_tag.inspect}"
      callparams = @@callparameters + [id_tag, @@sorter]
      ## no lotlist or monitor lot: all carriers classes 2 and 3
      # v21.03: no lotlist: not empty but not specified
      refute_empty self.class.call_empty_rule(ccat, callparams)
      # v21.03: monitor lot: carriers class 3, 2, 1
      assert_equal [cc[2], cc[1], cc[0]], self.class.call_empty_rule(ccat, callparams, lotlist: @@monitorlot)
      # lot at arbitrary op: class 2, 1, 3
      assert_equal [cc[1], cc[0], cc[2]], self.class.call_empty_rule(ccat, callparams, lotlist: @@lot)
      # lot at op with DestCarrierType=Class1: class 1 + 2
      assert_equal [cc[0], cc[1]], self.class.call_empty_rule(ccat, callparams, lotlist: @@lot1)
      assert_equal [cc[0], cc[1]], self.class.call_empty_rule(ccat, callparams, lotlist: "#{@@lot},#{@@lot1}")
      # WBTQ with backup (with lot at any op): class 2 + 1 + 3
      assert_equal [cc[1], cc[0], cc[2]], self.class.call_empty_rule(ccat, callparams, lotlist: @@lot, creator: 'WBTQ')
    }
  end

  def test12_limit
    # see class order spec!
    assert vlot = @@svtest.new_srclot(product: @@svtest.product), 'error creating vendor lot'
    @@testlots << vlot
    # carriers: 1 class1, 1 class2, 1 class3; limit class 1 90
    ccat = 'FEOL'  # must be FEOL because of AutoVenLotStart in the rule
    cc = @@sv.carrier_list(carrier: @@carriers, carrier_category: ccat).take(3)
    assert_equal 3, cc.size, "not enough carriers #{@@carriers}, ccat #{ccat}"
    assert age_carrier(cc[0], @@starts1)
    assert age_carrier(cc[1], @@starts2)
    assert age_carrier(cc[2], @@starts3)
    $log.info "waiting #{@@apf_delay} s for APF replication"; sleep @@apf_delay
    @@id_tags.each {|id_tag|
      $log.info "-- testing with id_tag #{id_tag.inspect}"
      callparams = @@callparameters + [id_tag, @@sorter]
      # v21.03: no lotlist: not empty but not specified
      refute_empty self.class.call_empty_rule(ccat, callparams)
      # v21.03: monitor lot: class 3 + 2
      assert_equal [cc[2], cc[1]], self.class.call_empty_rule(ccat, callparams, lotlist: @@monitorlot)
      # lot at arbitrary op: class 2 + 3 (not 1)
      assert_equal [cc[1], cc[2]], self.class.call_empty_rule(ccat, callparams, lotlist: @@lot)
      # lot at op with DestCarrierType=Class1: class 1 + 2 but below limit: class2 only
      assert_equal [cc[1]], self.class.call_empty_rule(ccat, callparams, lotlist: @@lot1)
      assert_equal [cc[1]], self.class.call_empty_rule(ccat, callparams, lotlist: "#{@@lot},#{@@lot1}")
      # AVLS empty scenario (with vendor lot): class 1
      assert_equal [cc[0]], self.class.call_empty_rule(ccat, callparams, lotlist: vlot, creator: 'AutoVenLotStart')
      # AVLS invalid scenario (with lot at any op): empty (no spec entry)
      assert_equal [], self.class.call_empty_rule(ccat, callparams, lotlist: vlot, creator: 'AutoVenLotStart', scenario: 'QAQAinvalid')
      # WBTQ with backup (with lot at any op): class 3 + 2 (not class 1 because of the limit)
      assert_equal [cc[2], cc[1]], self.class.call_empty_rule(ccat, callparams, lotlist: @@monitorlot, creator: 'WBTQ')
    }
  end

  def test14_no_class
    # carriers: 1 class1, 1 class2, 1 class3;  class1 has not enough carriers
    ccat = 'HK'
    cc = @@sv.carrier_list(carrier: @@carriers, carrier_category: ccat).take(3)
    assert_equal 3, cc.size, "not enough carriers #{@@carriers}, ccat #{ccat}"
    assert age_carrier(cc[0], @@starts1)
    assert age_carrier(cc[1], @@starts2)
    assert age_carrier(cc[2], @@starts3)
    $log.info "waiting #{@@apf_delay} s for APF replication"; sleep @@apf_delay
    @@id_tags.each {|id_tag|
      $log.info "-- testing with id_tag #{id_tag.inspect}"
      callparams = @@callparameters + [id_tag, @@sorter]
      # v21.03: no lotlist: not empty but not specified
      refute_empty self.class.call_empty_rule(ccat, callparams)
      # monitor lot: classes 3, 2, no class1 because of number limitation
      assert_equal [cc[2], cc[1]], self.class.call_empty_rule(ccat, callparams, lotlist: @@monitorlot)
      # lot at arbitrary op: class 2, 3
      assert_equal [cc[1], cc[2]], self.class.call_empty_rule(ccat, callparams, lotlist: @@lot)
      # lot at op with DestCarrierType=Class1: class2 only according to rule exceptions in spec 1060
      assert_equal [cc[1]], self.class.call_empty_rule(ccat, callparams, lotlist: @@lot1)
    }
  end

  def test21_carrier_used
    # use MOL (without lotlist all carriers are returned, FEOL has special rules)
    ccat = 'MOL'
    cc = @@sv.carrier_list(carrier: @@carriers, carrier_category: ccat).take(3)
    assert_equal 3, cc.size, "not enough carriers #{@@carriers}, ccat #{ccat}"
    # age carriers for class 2, so they become eligible for class 3 (no lot)
    cc.each {|c| assert age_carrier(c, @@starts2)}
    carrier = cc[1]
    # reference call
    # not in v21.03 assert_equal 3, self.class.call_empty_rule(ccat, params).size, "not enough carriers found"
    # foup count
    _p = ['FOUPCount', 2, 'EqpID', @@sorter, 'cast_regex', @@carriers.sub('%', '')]
    $log.info "waiting #{@@apf_delay} s for APF replication"; sleep @@apf_delay
    assert_equal 2, self.class.call_empty_rule(ccat, _p).size, 'wrong carrier count'
    # carrier not stocked in
    assert_equal 0, @@sv.carrier_xfer_status_change(carrier, 'EO', @@sorter)
    $log.info "waiting #{@@apf_delay} s for APF replication"; sleep @@apf_delay
    assert_equal [cc[0], cc[2]], self.class.call_empty_rule(ccat, @@callparameters_eqpid).sort, 'wrong carriers'
    assert_equal 0, @@sv.carrier_xfer_status_change(carrier, 'MI', @@stocker)
    # carrier not empty
    assert_equal 0, @@sv.lot_cleanup(@@lot, carrier: carrier)
    $log.info "waiting #{@@apf_delay} s for APF replication"; sleep @@apf_delay
    assert_equal [cc[0], cc[2]], self.class.call_empty_rule(ccat, @@callparameters_eqpid).sort, 'wrong carriers'
    assert_equal 0, @@sv.wafer_sort(@@lot, '')
    # carrier reserved
    assert @@svtest.cleanup_eqp(@@sorter, mode: 'Off-Line-1')
    assert_equal 0, @@sv.npw_reserve(@@sorter, nil, carrier)
    $log.info "waiting #{@@apf_delay} s for APF replication"; sleep @@apf_delay
    assert_equal [cc[0], cc[2]], self.class.call_empty_rule(ccat, @@callparameters_eqpid).sort, 'wrong carriers'
    assert_equal 0, @@sv.npw_reserve_cancel(@@sorter, carrier)
    # carrier is part of a SJ
    assert ec = @@svtest.get_empty_carrier # any standard carrier not part of this test
    assert_equal 0, @@sv.lot_cleanup(@@lot, carrier: ec)
    assert @@svtest.cleanup_eqp(@@sorter, mode: 'Auto-3', portstatus: 'UnloadReq')
    assert sj = @@sv.sj_create(@@sorter, 'P1', 'P2', @@lot, carrier), "error creating sorter job"
    $log.info "waiting #{@@apf_delay} s for APF replication"; sleep @@apf_delay
    assert_equal [cc[0], cc[2]], self.class.call_empty_rule(ccat, @@callparameters_eqpid).sort, 'wrong carriers'
    assert_equal 0, @@sv.sj_cancel(sj)
  end

  def test22_stocker_properties
    # use MOL (without lotlist all carriers are returned, FEOL has special rules)
    ccat = 'MOL'
    cc = @@sv.carrier_list(carrier: @@carriers, carrier_category: ccat).take(3)
    assert_equal 3, cc.size, "not enough carriers #{@@carriers}, ccat #{ccat}"
    # age carriers for class 2, so they become eligible for class 3 (no lot)
    cc.each {|c| assert age_carrier(c, @@starts2)}
    carrier = cc[1]
    # reference call
    $log.info "waiting #{@@apf_delay} s for APF replication"; sleep @@apf_delay
    assert_equal cc.sort, self.class.call_empty_rule(ccat, @@callparameters_eqpid).sort, 'wrong carriers'
    # carrier in stocker with wrong AmhsArea
    assert_equal 0, @@sv.carrier_xfer_status_change(carrier, 'MI', @@stocker2)
    $log.info "waiting #{@@apf_delay} s for APF replication"; sleep @@apf_delay
    assert_equal [cc[0], cc[2]], self.class.call_empty_rule(ccat, @@callparameters_eqpid).sort, 'wrong carriers'
    assert_equal 0, @@sv.carrier_xfer_status_change(carrier, 'MI', @@stocker)
    # carrier in shelf, no carriers returned although the AmhsArea is correct
    cc.each {|c| assert_equal 0, @@sv.carrier_xfer_status_change(c, 'MI', @@shelf)}
    $log.info "waiting #{@@apf_delay} s for APF replication"; sleep @@apf_delay
    assert_empty self.class.call_empty_rule(ccat, @@callparameters_eqpid)
    cc.each {|c| assert_equal 0, @@sv.carrier_xfer_status_change(c, 'MI', @@stocker)}
    # stocker is down
    assert_equal 0, @@sv.eqp_status_change(@@stocker, '2NDP')
    $log.info "waiting #{@@apf_delay} s for APF replication"; sleep @@apf_delay
    assert_empty self.class.call_empty_rule(ccat, @@callparameters_eqpid)
    assert_equal 0, @@sv.eqp_status_change(@@stocker, '2WPR')
  end

  def test31_carrier_condition
    # use MOL (without lotlist all carriers are returned, FEOL has special rules)
    ccat = 'MOL'
    cc = @@sv.carrier_list(carrier: @@carriers, carrier_category: ccat).take(3)
    assert_equal 3, cc.size, "not enough carriers #{@@carriers}, ccat #{ccat}"
    # age carriers for class 2
    cc.each {|c| assert age_carrier(c, @@starts2)}
    carrier = cc[1]
    # reference call
    $log.info "waiting #{@@apf_delay} s for APF replication"; sleep @@apf_delay
    assert_equal cc.sort, self.class.call_empty_rule(ccat, @@callparameters_eqpid).sort, 'wrong carriers'
    # # carrier Due; Due condition is currently ignored
    # assert_equal 0, @@sv.carrier_condition_change(carrier, 'Due')
    # $log.info "waiting #{@@apf_delay} s for APF replication"; sleep @@apf_delay
    # assert cc = self.class.call_empty_rule(ccat, @@callparameters_eqpid)
    # assert_equal carrier, cc.last, 'wrong order'
    # carrier Dirty
    assert_equal 0, @@sv.carrier_condition_change(carrier, 'Dirty')
    $log.info "waiting #{@@apf_delay} s for APF replication"; sleep @@apf_delay
    assert cc = self.class.call_empty_rule(ccat, @@callparameters_eqpid)
    refute cc.member?(carrier), 'wrong carrier found'
    # carrier Dirty, WBTQ (same as above)
    assert cc = self.class.call_empty_rule(ccat, @@callparameters_eqpid, wait: false, creator: 'WBTQ')
    refute cc.member?(carrier), 'wrong carrier found'
    ## not! assert_equal carrier, cc.last, "wrong order"
  end

  def test32_carrier_condition_2classes
    # use MOL (without lotlist all carriers are returned, FEOL has special rules), UseCase OnRoute
    ccat = 'MOL'
    cc = @@sv.carrier_list(carrier: @@carriers, carrier_category: ccat).take(6)
    assert_equal 6, cc.size, "not enough carriers #{@@carriers}, ccat #{ccat}"
    # 3 carriers for class 1 and 2 each
    cc1 = cc[0..2]
    cc1.each {|c| assert age_carrier(c, @@starts1)}
    cc2 = cc[3..5]
    cc2.each {|c| assert age_carrier(c, @@starts2)}
    # reference call
    $log.info "waiting #{@@apf_delay} s for APF replication"; sleep @@apf_delay
    res = self.class.call_empty_rule(ccat, @@callparameters_eqpid, sjc: false)
    assert_equal 6, res.size, 'wrong carriers'
    #
    c1due = cc1[1]  # 2nd carrier cat 1
    c2due = cc2[1]  # 2nd carrier cat 2
    # # carriers Due; Due condition is currently ignored
    # assert_equal 0, @@sv.carrier_condition_change(c1due, 'Due')
    # assert_equal 0, @@sv.carrier_condition_change(c2due, 'Due')
    # # test with Production lot, returns cat 1 then cat 2
    # $log.info "waiting #{@@apf_delay} s for APF replication"; sleep @@apf_delay
    # assert res = self.class.call_empty_rule(ccat, @@callparameters_eqpid, sjc: false, lotlist: @@lot)
    # assert_empty res[0..2] & cc1, 'first carriers must be cat1'
    # assert_empty res[3..5] & cc2, 'last carriers must be cat2'
    # assert_equal c1due, res[2], 'Due carrier must be last in ccat'
    # assert_equal c2due, res[5], 'Due carrier must be last in ccat'
    # # test with testwafer lot, returns cat 3 then cat 2
    # assert res = self.class.call_empty_rule(ccat, @@callparameters_eqpid, sjc: false, lotlist: @@monitorlot)
    # assert_empty res[0..2] & cc1, "first carriers must be cat3"
    # assert_empty res[3..5] & cc2, "last carriers must be cat2"
    # assert_equal c2due, res[2], "Due carrier must be last in ccat"
    # assert_equal c1due, res[5], "Due carrier must be last in ccat"
    #
    # carrier Dirty
    assert_equal 0, @@sv.carrier_condition_change(c1due, 'Dirty')
    assert_equal 0, @@sv.carrier_condition_change(c2due, 'Dirty')
    $log.info "waiting #{@@apf_delay} s for APF replication"; sleep @@apf_delay
    assert res =  self.class.call_empty_rule(ccat, @@callparameters_eqpid, sjc: false, lotlist: @@lot)
    assert_equal 4, res.size, 'wrong carriers'
    assert_nil res.find {|c| c == c1due}
    assert_nil res.find {|c| c == c2due}
  end

  def test33_carrier_condition_dirty
    # use MOL (without lotlist all carriers are returned, FEOL has special rules)
    ccat = 'MOL'
    cc = @@sv.carrier_list(carrier: @@carriers, carrier_category: ccat).take(4)
    assert_equal 4, cc.size, "not enough carriers #{@@carriers}, ccat #{ccat}"
    # age 3 carriers for class 2 and set one of them Dirty, the 4th one is class 1 (Cleaned)
    cc2 = cc.take(3)
    cc2.each {|c| assert age_carrier(c, @@starts2)}
    cdirty = cc2[1]
    assert_equal 0, @@sv.carrier_condition_change(cdirty, 'Dirty')
    assert age_carrier(cc.last, 0)
    #
    # the 2 not Dirty class 2 carriers + class 1 are returned
    $log.info "waiting #{@@apf_delay} s for APF replication"; sleep @@apf_delay
    assert res = self.class.call_empty_rule(ccat, @@callparameters_eqpid, creator: 'DEKIT')
    assert_equal (cc2 - [cdirty] + [cc.last]).sort, res.sort, 'wrong carriers returned'
    # the 3 class 2 carriers are returned, Dirty one first (lot required for Dirty carrier ordering to work)
    assert res = self.class.call_empty_rule(ccat, @@callparameters_eqpid, creator: 'DEKIT', lotlist: @@monitorlot)
    assert_equal (cc2 + [cc.last]).sort, res.sort, 'wrong carriers returned'
    assert_equal cdirty, res.first, 'Dirty carrier must be returned first'   # currently fails
    # age the 4th carrier to become class 2
    assert age_carrier(cc.last, @@starts2)
    # the 3 class 2 carriers are returned, Dirty one not
    $log.info "waiting #{@@apf_delay} s for APF replication"; sleep @@apf_delay
    assert res = self.class.call_empty_rule(ccat, @@callparameters_eqpid, creator: 'DEKIT')
    assert_equal (cc - [cdirty]).sort, res.sort, 'wrong carriers returned'
  end

  # AVLS carrier category mapping
  def test41_FOSB_FEOL
    # prepare carriers
    ccat = 'FOSB'
    cc = @@sv.carrier_list(carrier: @@carriers, carrier_category: ccat).take(3)
    assert_equal 3, cc.size, "not enough carriers #{@@carriers}, ccat #{ccat}"
    cc.each {|c| assert age_carrier(c, 0)}
    # prepare target carriers for AVLS
    tgtcategory = 'FEOL'
    tgtcc = @@sv.carrier_list(carrier: @@carriers, carrier_category: tgtcategory).take(3)
    assert_equal 3, tgtcc.size, "not enough carriers #{@@carriers}, ccat #{ccat}"
    tgtcc.each {|c| assert age_carrier(c, 0)}
    #
    # regular and AVLS calls
    params = {parameters: @@callparameters + ['EqpID', @@sorter, 'lotlist', @@lot]}
    $log.info "waiting #{@@apf_delay} s for APF replication"; sleep @@apf_delay
    assert_equal cc.sort, self.class.call_empty_rule(ccat, params).sort
    assert_equal tgtcc.sort, self.class.call_empty_rule(ccat, params.merge(wait: false, creator: 'AutoVenLotStart')).sort
    assert_equal tgtcc.sort, self.class.call_empty_rule(tgtcategory, params.merge(wait: false, creator: 'AutoVenLotStart')).sort
  end

  def test42_SBFE_FEOL
    # prepare carriers
    ccat = 'SBFE'
    cc = @@sv.carrier_list(carrier: @@carriers, carrier_category: ccat).take(3)
    assert_equal 3, cc.size, "not enough carriers #{@@carriers}, ccat #{ccat}"
    cc.each {|c| assert age_carrier(c, 0)}
    # prepare tgt carriers for AVLS
    tgtcategory = 'FEOL'
    tgtcc = @@sv.carrier_list(carrier: @@carriers, carrier_category: tgtcategory).take(3)
    assert_equal 3, tgtcc.size, "not enough carriers #{@@carriers}, ccat #{ccat}"
    tgtcc.each {|c| assert age_carrier(c, 0)}
    #
    # regular and AVLS calls
    params = {parameters: @@callparameters + ['EqpID', @@sorter, 'lotlist', @@lot]}
    $log.info "waiting #{@@apf_delay} s for APF replication"; sleep @@apf_delay
    assert_equal cc.sort, self.class.call_empty_rule(ccat, params).sort
    assert_equal tgtcc.sort, self.class.call_empty_rule(ccat, params.merge(wait: false, creator: 'AutoVenLotStart')).sort
    assert_equal tgtcc.sort, self.class.call_empty_rule(tgtcategory, params.merge(wait: false, creator: 'AutoVenLotStart')).sort
  end

  def test43_SBMO_MOL
    # prepare carriers
    ccat = 'SBMO'
    cc = @@sv.carrier_list(carrier: @@carriers, carrier_category: ccat).take(3)
    assert_equal 3, cc.size, "not enough carriers #{@@carriers}, ccat #{ccat}"
    cc.each {|c| assert age_carrier(c, 0)}
    # prepare tgt carriers for AVLS
    tgtcategory = 'MOL'
    tgtcc = @@sv.carrier_list(carrier: @@carriers, carrier_category: tgtcategory).take(3)
    assert_equal 3, tgtcc.size, "not enough carriers #{@@carriers}, ccat #{ccat}"
    tgtcc.each {|c| assert age_carrier(c, 0)}
    #
    params = {parameters: @@callparameters + ['EqpID', @@sorter, 'lotlist', @@lot]}
    $log.info "waiting #{@@apf_delay} s for APF replication"; sleep @@apf_delay
    assert_equal cc.sort, self.class.call_empty_rule(ccat, params).sort
    assert_equal tgtcc.sort, self.class.call_empty_rule(ccat, params.merge(wait: false, creator: 'AutoVenLotStart')).sort
    assert_equal tgtcc.sort, self.class.call_empty_rule(tgtcategory, params.merge(wait: false, creator: 'AutoVenLotStart')).sort
  end

  def test51_customer_fosb
    # specs http://vf1sfcd01:10080/setupfc-rest/v1/specs/C08-00001042/content?format=html, http://vf1sfcd01:10080/setupfc-rest/v1/specs/C08-00001036/content?format=html
    assert lot1 = @@svtest.new_lot(customer: 'amd')
    @@testlots << lot1
    assert lot2 = @@svtest.new_lot(customer: 'qgt')
    @@testlots << lot2
    technology = @@sv.lot_info(lot1).technology
    # pick 1 EEx carrier each (ccat SMBO) which's caondition will be 'Used'
    tgtcategory = 'SBMO'  # any FOSB ccat
    fosbcategory = 'MOL'  # any non-FOSB ccat, but limits apply and make FEOL not usable here
    ccused = [1, 2, 3, 4].collect {|i| @@sv.carrier_list(carrier: "EE#{i}%", carrier_category: tgtcategory).first}
    ccused_fosb = [1, 2, 3, 4].collect {|i| @@sv.carrier_list(carrier: "EE#{i}%", carrier_category: fosbcategory).first}
    # prepare all EE* carriers (SBMO and FEOL), 3rd letter in carrier name determines FOSB type in spec C08-00001041
    cc = @@sv.carrier_list(carrier: @@customerfosbs)
    cc.each {|c|
      assert @@svtest.cleanup_carrier(c, make_empty: true)
      cond = (ccused + ccused_fosb).member?(c) ? 'Used' : 'Cleaned'
      assert_equal 0, @@sv.carrier_condition_change(c, cond)
      assert_equal 0, @@sv.user_data_delete(:carrier, c, 'MaterialType')  # start without MaterialType!
    }
    #
    # lots' technology UDATA 'Geometry' '14'
    assert @@sm.update_udata(:technology, technology, {'Geometry'=>'14'})
    # any customer and Protokol MOL: all MOL carriers returned
    tgtcc = @@sv.carrier_list(carrier: @@customerfosbs, carrier_category: fosbcategory).sort
    $log.info "waiting #{@@apf_delay} s for APF replication"; sleep @@apf_delay
    assert_equal tgtcc, self.class.call_empty_rule(fosbcategory, @@callparameters_eqpid, lotlist: lot1).sort, 'wrong carriers'
    assert_equal tgtcc, self.class.call_empty_rule(fosbcategory, @@callparameters_eqpid, lotlist: lot2).sort, 'wrong carriers'
    # customer amd or qgt: no carriers returned because of missing MaterialType
    assert_empty self.class.call_empty_rule(tgtcategory, @@callparameters_eqpid, lotlist: lot1), 'wrong carriers'
    assert_empty self.class.call_empty_rule(tgtcategory, @@callparameters_eqpid, lotlist: lot2), 'wrong carriers'
    # set MaterialType for the following tests
    cc.each {|c| assert_equal 0, @@sv.user_data_update(:carrier, c, {'MaterialType'=>'PRIME-FOSB'})}
    # customer amd: Cleaned and Used EE1 carriers returned
    tgtcc = @@sv.carrier_list(carrier: 'EE1%', carrier_category: tgtcategory).sort
    $log.info "waiting #{@@apf_delay} s for APF replication"; sleep @@apf_delay
    assert_equal tgtcc, self.class.call_empty_rule(tgtcategory, @@callparameters_eqpid, lotlist: lot1).sort, 'wrong carriers'
    # customer qgt: only Cleaned EE3 carriers
    tgtcc = @@sv.carrier_list(carrier: 'EE3%', carrier_category: tgtcategory).sort - ccused
    assert_equal tgtcc, self.class.call_empty_rule(tgtcategory, @@callparameters_eqpid, lotlist: lot2).sort, 'wrong carriers'
    #
    # lots' technology UDATA 'Geometry' 'SOMETHING' -> mapped to ALL in spec
    assert @@sm.update_udata(:technology, technology, {'Geometry'=>'SOMETHING'})
    # any customer and Protokol MOL: all MOL carriers returned
    tgtcc = @@sv.carrier_list(carrier: @@customerfosbs, carrier_category: fosbcategory).sort
    $log.info "waiting #{@@apf_delay} s for APF replication"; sleep @@apf_delay
    assert_equal tgtcc, self.class.call_empty_rule(fosbcategory, @@callparameters_eqpid, lotlist: lot1).sort, 'wrong carriers'
    assert_equal tgtcc, self.class.call_empty_rule(fosbcategory, @@callparameters_eqpid, lotlist: lot2).sort, 'wrong carriers'
    # customer amd: Cleaned and Used EE2 carriers returned
    tgtcc = @@sv.carrier_list(carrier: 'EE2%', carrier_category: tgtcategory).sort
    assert_equal tgtcc, self.class.call_empty_rule(tgtcategory, @@callparameters_eqpid, lotlist: lot1).sort, 'wrong carriers'
    # customer qgt: only Cleaned EE4 carriers
    tgtcc = @@sv.carrier_list(carrier: 'EE4%', carrier_category: tgtcategory).sort - ccused
    assert_equal tgtcc, self.class.call_empty_rule(tgtcategory, @@callparameters_eqpid, lotlist: lot2).sort, 'wrong carriers'
  end


  # aux methods

  def age_carrier(carrier, starts)
    lot = @@lot
    @@svtest.cleanup_carrier(carrier, make_empty: true) || return
    @@sv.carrier_counter_reset(carrier).zero? || return
    if starts > 0
      @@sv.lot_cleanup(lot, carrier: carrier).zero? || return
      starts.times {
        @@sv.lot_opelocate(lot, :first).zero? || return
        @@sv.claim_process_lot(lot, proctime: 0) || return
      }
      @@sv.lot_opelocate(lot, :first).zero? || return
      @@sv.wafer_sort(lot, '').zero? || return
    end
    return @@sv.carrier_xfer_status_change(carrier, '', @@stocker).zero?
  end

  # call EMPTY rule for a carrier category (protocol zone), return the result as array of carriers
  def self.call_empty_rule(carrier_category, callparameters, params={})
    ##parameters = ['rtdDispLogicId', 'WhatNextLotList',
    parameters = ['Protokoll', carrier_category]
    parameters += ['SJC', 'YES'] if params[:sjc] != false   # spec UseCase SortJob or OnRoute
    parameters += ['lotlist', params[:lotlist]] if params[:lotlist]
    parameters += ['GENERATED_BY', params[:creator]] if params[:creator]
    parameters += ['SCENARIO', params[:scenario] || '']
    parameters += callparameters
    response = @@rtdclient.dispatch_list(@@rtd_rule, report: @@rtd_rule, category: @@rtd_category, parameters: parameters) || return
    ret = response.rows.collect {|r| r.first}
    $log.info "  response: #{ret}"
    return ret
  end

end
