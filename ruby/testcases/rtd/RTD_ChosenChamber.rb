=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author:  sfrieske, 2020-12-04

Version: 1.0.0

History:
=end

require 'dbsequel'
require 'util/waitfor'
require 'SiViewTestCase'


class RTD_ChosenChamber < SiViewTestCase
  @@eqp = 'UTC001'

  @@testlots = []


  def self.after_all_passed
    @@testlots.each {|lot| @@sv.delete_lot_family(lot)}
  end


  def test00_setup
    $setup_ok = false
    #
    assert @@mds = DB.new("mds.#{$env}")
    #
    $setup_ok = true
  end

  def test11_happy
    assert_equal 0, @@sv.eqp_cleanup(@@eqp), 'error preparing eqp'
    assert lot = @@svtest.new_lot(nwafers: 3), 'error creating test lot'
    @@testlots << lot
    #
    # slr: TARGET_CHAMBER
    tgtchamber = 'PM1'
    pprep = {eqp: @@eqp, target_chambers: {lot=>[tgtchamber]}}
    assert res = @@sv.claim_process_prepare(lot, pprep), 'error preparing lot'
    assert wait_chamberlist(lot, 'Created', tgtchamber), 'wrong MDS entry'
    #
    # opestart: CHOSEN_CHAMBER
    chchamber = 'QAQA2'  # does not need to exist
    cj, cjinfo, eqpiraw, pstatus, mode = res
    refute_nil @@sv.eqp_opestart(eqpiraw.equipmentID, @@sv.lot_info(lot).carrier)
    assert_equal 0, @@sv.user_data_update(:controljob, cj, {"CHOSEN_CHAMBER:#{lot}"=>chchamber})
    assert wait_chamberlist(lot, 'OperationStart', chchamber), 'wrong MDS entry'
    #
    # opecomp: CHOSEN_CHAMBER
    assert_equal 0, @@sv.eqp_opecomp(eqpiraw.equipmentID, cj)
    assert wait_chamberlist(lot, 'OperationComplete', chchamber), 'wrong MDS entry'
  end

  def test21_2lots
    assert_equal 0, @@sv.eqp_cleanup(@@eqp), 'error preparing eqp'
    assert lot = @@svtest.new_lot(nwafers: 4), 'error creating test lot'
    @@testlots << lot
    assert lc = @@sv.lot_split(lot, 2), 'error splitting lot'
    #
    # slr: TARGET_CHAMBER
    tgtchamber1 = 'PM1'
    tgtchamber2 = 'PM2'
    pprep = {eqp: @@eqp, target_chambers: {lot=>[tgtchamber1], lc=>[tgtchamber2]}}
    assert res = @@sv.claim_process_prepare([lot, lc], pprep), 'error preparing lot'
    assert wait_chamberlist(lot, 'Created', tgtchamber1), 'wrong MDS entry'
    assert wait_chamberlist(lc, 'Created', tgtchamber2), 'wrong MDS entry'
    #
    # opestart: CHOSEN_CHAMBER
    chchamber1 = 'QAQA1'
    chchamber2 = 'QAQA2'
    cj, cjinfo, eqpiraw, pstatus, mode = res
    refute_nil @@sv.eqp_opestart(eqpiraw.equipmentID, @@sv.lot_info(lot).carrier)
    assert_equal 0, @@sv.user_data_update(:controljob, cj,
                      {"CHOSEN_CHAMBER:#{lot}"=>chchamber1, "CHOSEN_CHAMBER:#{lc}"=>chchamber2})
    assert wait_chamberlist(lot, 'OperationStart', chchamber1), 'wrong MDS entry'
    assert wait_chamberlist(lc, 'OperationStart', chchamber2), 'wrong MDS entry'
    #
    # opecomp: CHOSEN_CHAMBER
    assert_equal 0, @@sv.eqp_opecomp(eqpiraw.equipmentID, cj)
    assert wait_chamberlist(lot, 'OperationComplete', chchamber1), 'wrong MDS entry'
    assert wait_chamberlist(lc, 'OperationComplete', chchamber2), 'wrong MDS entry'
  end


  # aux methods

  def wait_chamberlist(lot, cat, chamberlist)
    $log.info "wait_chamberlist #{lot.inspect}, #{cat.inspect}, #{chamberlist.inspect}"
    res = nil
    ret = wait_for(timeout: 150) { # MDS hard-coded: 120
      # res = @@mds.select("select OPE_CATEGORY, CHAMBER_LIST from T_LOT_MOVE_HIST where LOT_ID='#{lot}'").first
      res = @@mds.select("select OPE_CATEGORY, CHAMBER_LIST from V_RTD_LOT_MOVE_HIST where LOT_ID='#{lot}'").first
      res == [cat, chamberlist]
    }
    ($log.warn " wrong MDS data: #{res.inspect}"; return) if ret != true
    return true
  end

end
