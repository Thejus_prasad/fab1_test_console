=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: sfrieske, 2016-11-22
=end

require 'RubyTestCase'
require 'rtdsvrmgrclient'


# Stress test for the RTDServerManager interface of APF v9
class RTD_CorbaServerStress < RubyTestCase
    @@eqps = %w(ETX100 THK111 THK112 THK115 THK116 THK118
                THK117 THK513 DFU124 DFU120 DFU121
                LETCL1001 LETCL1002 LETCL1003 LETCL1004 QANOTEX1)
    @@request_timeout = 300
    @@loops = 10000


    def self.startup
      super(nosiview: true)
      @@svrmgrclient = RTD::ServerManagerClient.new($env)
      @@results = {tstart: nil, tend: nil, durations: {}}
      @@eqps.each {|eqp| @@results[:durations][eqp] = []}
    end


    def test11
      tstart = Time.now
      @@results[:tstart] = Time.now
      running = true
      @@loops.times {|i|
        $log.info "-- loop #{i+1}/#{@@loops}"
        @@eqps.collect {|eqp|
          Thread.new {
              @@results[:durations][eqp][i] = -1   # to catch timeouts
              tstart = Time.now
              res = @@svrmgrclient.dispatch_request(eqp, raw: true)
              if res.nil? || res.systemErrorCode != '0' || res.userErrorCode != '0' ## || @@results[:durations][eqp][i] == -1
                running = false
              else
                @@results[:durations][eqp][i] = Time.now - tstart
              end
          }
        }.each {|t| t.join(@@request_timeout)}
        dd = @@results[:durations].each_pair.collect {|eqp, durs| durs.last}
        $log.info "loop #{i+1} durations: #{dd}"
        break unless running
      }
      #
      @@results[:tend] = Time.now
      $log.info "TxDispatchRequest test results:\n#{@@results.pretty_inspect}"
      assert running, "test was aborted after an RTD server error"
    end
end
