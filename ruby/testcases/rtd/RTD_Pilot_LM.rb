=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author:  sfrieske, 2020-02-04

Version: 1.0.0

History:
=end

require 'SiViewTestCase'
require 'jcap/lmdb'
require 'jcap/lmtrans'
require 'rtdserver/rtdclient'
require 'util/waitfor'


# Testcases for RTD Automated PilotManager
class RTD_Pilot_LM < SiViewTestCase
  @@sv_defaults = {route: 'UTRT-PILOTMGR.01', product: 'UT-PRODUCT-PILOTMGR.01'}
  @@eqp = 'UTPILOTMGR1'
  @@ppd = 'UTPDPILOT01.01'
  @@appid = 'QARTDPILOTLM'  # APC controller ID, arbitrarily chosen
  @@rqid_min = 9000000000000

  @@category = 'DISPATCHER/QA'
  @@report = 'QA_autopilot_trigger'

  @@repl_duration = 280 # ??
  @@apf_delay = 30

  @@testlots = []
  @@testrqids = []


  def self.after_all_passed
    @@testlots.each {|lot| @@sv.delete_lot_family(lot)}
    @@testrqids.each {|rqid| @@lmtrans.delete_lmrequests(rqid)}
  end


  def test00_setup
    $setup_ok = false
    #
    assert @@lmtrans = LotManager::Transformer.new($env)
    assert @@lmdb = LotManager::DB.new($env)
    assert @@rtdclient = RTD::Client.new($env)
    #
    $setup_ok = true
  end

  def test11
    assert lot = @@svtest.new_lot, 'error creating testlot'
    @@testlots << lot
    # locate lot to the pilot PD and wait for LM to create the context
    assert_equal 0, @@sv.lot_opelocate(lot, nil, op: @@ppd), 'error locating lot'
    $log.info "waiting up to #{@@repl_duration} s for LM context creation"
    assert wait_for(timeout: @@repl_duration) {
      @@lmdb.lmcontext(lot: lot, eqp: @@eqp, pd: @@ppd.split('.').first).first
    }, 'LM feed not working?'
    #
    # get a free request ID (string), send LmRequestData to the LM Transformer jCAP job
    # rqid = 9000000000013
    assert rqid = @@lmdb.free_rqid(min: @@rqid_min), 'no suitable LM_REQUEST_ID found'
    @@testrqids << rqid
    now = Time.at(Time.now.to_i)
    expiration = now  # to have RTD trigger a pilot
    rqdata = LotManager::LmRequest.new(rqid, 15, -1, -1, 14, -1, '', -1, 0, '', 0, expiration, now, 'QA Test',
      nil, 0, nil, 0, nil, [], [], [], nil, [], []
    )
    assert @@lmtrans.update_lmrequests(data: rqdata)
    #
    # send LotSummary data to the LM Transformer jCAP job
    sumdata = LotManager::LotSummary.new(
      LotManager::LmContext.new(nil, lot, @@ppd.split('.').first, @@eqp), nil, nil, [], [], [], [], [],
      [LotManager::LmLimitation.new(rqid, @@appid, 'Mainframe', @@eqp, 0, 0, now, 'QA Test')],
      []
    )
    assert @@lmtrans.update_lotsummary(data: sumdata)
    #
    # trigger RTD, APC PilotManager sets the lot ONHOLD
    $log.info "waiting #{@@apf_delay} s for APF replication"; sleep @@apf_delay
    res = @@rtdclient.dispatch_list(@@eqp, category: @@category, report: @@report)
    assert_equal @@report, res.rule, 'wrong RTD rule called'
    refute_empty res.rows, 'wrong RTD response'
    assert wait_for {
      !@@sv.lot_hold_list(lot, raw: true).empty?
    }, 'lot is not ONHOLD'
    #
    # now the APC AutoPilot applications creates the pilot
    # verify: T_LM_REQUEST_LOT CATEGORY_ID: PLTMNGR.INITIATED
    # set expiration into future, release lot hold, call RTD trigger rule again -> RTD response empty, lot does not go onhold
  end

end
