=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: Thomas Klose and Steffen Frieske, 2016-07-06

History:
  2021-08-30 sfrieske, moved pdwhtest.rb and pdwhdb.rb to misc
=end

require 'misc/pdwhtest'
require 'RubyTestCase'


class Test_PDWH_SM < RubyTestCase
  @@pd1 = 'PADO-OXDI.01'   # any PD
  @@pd2 = 'PADN-NITDI.01'  # any PD
  @@mainflow_version_delay = 330  # mainflow loader creates a new version only after 5'

  def self.startup
    super
    @@pdwhtest = PDWH::Test.new($env)
    @@sm = @@pdwhtest.sm
  end


  def testsm11_UDATA_PD_OperationReportingType
    $log.info '-- step 0: reset UDATA'
    [@@pd1, @@pd2].each {|pd| assert @@sm.update_udata(:pd, pd, {'OperationReportingType'=>'QA0'})}
    #
    $log.info '-- step 1: set UDATA 1'
    $log.info "waiting #{@@mainflow_version_delay} s"; sleep @@mainflow_version_delay
    assert @@sm.update_udata(:pd, @@pd1, {'OperationReportingType'=>'MSKDF,AllowMultiPass'})
    assert @@sm.update_udata(:pd, @@pd2, {'OperationReportingType'=>'10VIAMSKDISPO,RWK'})
    #
    $log.info '-- step 2: set UDATA 2'
    $log.info "waiting #{@@mainflow_version_delay} s"; sleep @@mainflow_version_delay
    assert @@sm.update_udata(:pd, @@pd1, {'OperationReportingType'=>'Test'})
    assert @@sm.update_udata(:pd, @@pd2, {'OperationReportingType'=>'LS1,STP90X,EXP,165'})
    #
    $log.info '-- step 3: set UDATA 3'
    $log.info "waiting #{@@mainflow_version_delay} s"; sleep @@mainflow_version_delay
    assert @@sm.update_udata(:pd, @@pd1, {'OperationReportingType'=>'MSKAF,AllowMultiPass,-A,RWK'})
    assert @@sm.update_udata(:pd, @@pd2, {'OperationReportingType'=>'MSK-A'})
    #
    $log.info '-- step 4: delete UDATA'
    $log.info "waiting #{@@mainflow_version_delay} s"; sleep @@mainflow_version_delay
    assert @@sm.update_udata(:pd, @@pd1, {'OperationReportingType'=>''})
  end

end
