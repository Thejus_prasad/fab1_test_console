=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: Thomas Klose and Steffen Frieske, 2016-07-06

History:
  2020-05-07 sfrieske, changed wafer_history_report call
  2020-06-26 sfrieske, removed OLD tests, fixed use of wafer_history_report
  2020-07-02 sfrieske, renamed, use SiView::Test instead of PDWH::Test, can create Monitor lots
  2021-03-23 sfrieske, removed newlot_delay, call new_srclot if required
  2021-08-27 sfrieske, require uniquestring instead of misc

Notes:
  for Non-Prod lots: runtest 'PDWH_7', 'itdc', parameters: {lottype: 'Equipment Monitor'}
=end

require 'jcap/toolevents'
require 'misc/pdwhdb'
require 'util/uniquestring'
require 'SiViewTestCase'


# create Wafer History data for PDWH tests
class PDWH_7 < SiViewTestCase
  @@sv_defaults = {carriers: 'PDWH%', product: 'PDWHLOAD.A1', route: 'P-PDWHLOADA1.01'}
  @@eqp_meas = 'PDWHM40'
  @@eqp_meas2 = 'PDWHM41'
  @@eqp_proc = 'PDWHP10'
  @@eqp_proc2 = 'PDWHP11'

  @@lottype = nil
  @@dbenv = 'stress'  # pass 'stress' for tests with the STRESS (LET) instance, else nil or "itdc"

  @@testlots = []
  @@results = {}


  def self.startup
    super
    @@lldb = PDWH::LLDB.new(@@dbenv)
    @@wh = ToolEvents::WaferHistory.new($env, sv: @@sv)
  end

  def self.delete_testlots  # for manual cleanup
    @@testlots.each {|lot| @@sv.delete_lot_family(lot)}
  end

  def self.after_all_passed
    $log.info 'Results:'
    @@results.each_pair {|tc, res| puts "#{tc}: #{res}"}
  end


  # process with wafer history

  def test7011_wafer_history_meas
    assert_equal 0, @@sv.eqp_cleanup(@@eqp_meas), "error cleaning up #{@@eqp_meas}"
    assert lot = @@svtest.new_lot(nwafers: 9), 'error creating lot'
    @@testlots << lot
    assert_equal 0, @@sv.sublottype_change(lot, @@lottype) unless @@lottype.nil?
    refute_empty opeqps = @@sv.lot_eqplist_in_route(lot, eqp: @@eqp_meas), "no PD for #{@@eqp_meas}"
    assert_equal 0, @@sv.lot_opelocate(lot, opeqps.keys.first), 'error creating lot'
    assert cj = @@sv.claim_process_lot(lot, eqp: @@eqp_meas) {|cj, cjinfo, eqpiraw, pstatus|
      li = @@sv.lot_info(lot, wafers: true, select: [2, 3, 6])
      @@wh.wafer_history_report(cj, li, eqpiraw.equipmentID.identifier, pstatus.portID.identifier)
    }
    @@results['test7011'] = [lot, cj]
  end

  def XXtestman7011a_wafer_history_meas_oldlot
    eqp = 'UTC001'
    assert_equal 0, @@sv.eqp_cleanup(eqp), "error cleaning up #{eqp}"
    wn = @@sv.what_next(eqp)
    refute_empty wn, "no lot in WhatNext for #{eqp}"
    assert wne = wn.find {|e|
      ctime = @@sv.siview_time(@@sv.lot_info(e.lot).claim_time)
      ctime < (Time.now - 14 * 86400) && ctime > (Time.now - 720 * 86400)
    }, "no old lot"
    lot = wne.lot
    refute_empty opeqps = @@sv.lot_eqplist_in_route(lot, eqp: eqp), "no PD for #{eqp}"
    assert cj = @@sv.claim_process_lot(lot, eqp: eqp) {|cj, cjinfo, eqpiraw, pstatus|
      li = @@sv.lot_info(lot, wafers: true, select: [2, 3, 6])
      @@wh.wafer_history_report(cj, li, eqpiraw.equipmentID.identifier, pstatus.portID.identifier)
    }

    @@results['testman7011a'] = [lot, cj]
  end

  # sf 2020-06-29: broken, timeout waiting for loader MDS_LTH
  def XXtestdev7012_wafer_history_meas_2runs
    assert_equal 0, @@sv.eqp_cleanup(@@eqp_meas), "error cleaning up #{@@eqp_meas}"
    assert lot = @@svtest.new_lot(nwafers: 9), 'error creating lot'
    @@testlots << lot
    assert_equal 0, @@sv.sublottype_change(lot, @@lottype) unless @@lottype.nil?
    refute_empty opeqps = @@sv.lot_eqplist_in_route(lot, eqp: @@eqp_meas), "no such PD: #{@@eqp_meas}"
    assert_equal 0, @@sv.lot_opelocate(lot, opeqps.keys.first), 'error creating lot'
    assert cj = @@sv.claim_process_lot(lot, eqp: @@eqp_meas) {|cj, cjinfo, eqpiraw, pstatus|
      li = @@sv.lot_info(lot, wafers: true, select: [2])
      @@wh.wafer_history_report(cj, li, eqpiraw.equipmentID.identifier, pstatus.portID.identifier)
      ##$log.info "PAUSING, press enter to continue"; gets
      assert @@lldb.wait_next_batch_run('MDS_LTH', verbose: true)  # 2020-06-29: timeout
      li = @@sv.lot_info(lot, wafers: true, select: [3, 6])
      @@wh.wafer_history_report(cj, li, eqpiraw.equipmentID.identifier, pstatus.portID.identifier)
    }
    @@results['test7012'] = [lot, cj]
  end

  def test7013_wafer_history_proc_batch
    assert_equal 0, @@sv.eqp_cleanup(@@eqp_proc), "error cleaning up #{@@eqp_proc}"
    assert lot = @@svtest.new_lot(nwafers: 13), 'error creating lot'
    @@testlots << lot
    assert_equal 0, @@sv.sublottype_change(lot, @@lottype) unless @@lottype.nil?
    refute_empty opeqps = @@sv.lot_eqplist_in_route(lot, eqp: @@eqp_proc), "no such PD: #{@@eqp_proc}"
    assert_equal 0, @@sv.lot_opelocate(lot, opeqps.keys.first), 'error creating lot'
    assert lcs = 2.times.collect {@@sv.lot_split(lot, 4)}, 'error splitting lot'
    assert cj = @@sv.claim_process_lot([lot] + lcs, eqp: @@eqp_proc) {|cj, cjinfo, eqpiraw, pstatus|
      ([lot] + lcs).each {|l|
        li = @@sv.lot_info(l, wafers: true)
        @@wh.wafer_history_report(cj, li, eqpiraw.equipmentID.identifier, pstatus.portID.identifier)
      }
    }
    @@results['test7013'] = [[lot] + lcs, cj]
  end


  # opestart cancel, then process on other eqp with wafer history

  def test7021_wafer_history_meas
    assert_equal 0, @@sv.eqp_cleanup(@@eqp_meas), "error cleaning up #{@@eqp_meas}"
    assert_equal 0, @@sv.eqp_cleanup(@@eqp_meas2), "error cleaning up #{@@eqp_meas2}"
    assert lot = @@svtest.new_lot(nwafers: 9), 'error creating lot'
    @@testlots << lot
    assert_equal 0, @@sv.sublottype_change(lot, @@lottype) unless @@lottype.nil?
    refute_empty opeqps = @@sv.lot_eqplist_in_route(lot, eqp: @@eqp_meas), "no PD for #{@@eqp_meas}"
    assert_equal 0, @@sv.lot_opelocate(lot, opeqps.keys.first), 'error creating lot'
    # first try with opestart_cancel after 1 WaferComplete event
    assert cj = @@sv.claim_process_lot(lot, eqp: @@eqp_meas, opestart_cancel: true) {|cj, cjinfo, eqpiraw, pstatus|
      li = @@sv.lot_info(lot, wafers: true, select: [2])
      @@wh.wafer_history_report(cj, li, eqpiraw.equipmentID.identifier, pstatus.portID.identifier)
    }
    # 2nd try at 2nd eqp, with wafer history
    assert cj = @@sv.claim_process_lot(lot, eqp: @@eqp_meas2) {|cj, cjinfo, eqpiraw, pstatus|
      li = @@sv.lot_info(lot, wafers: true, select: [2, 3, 6])
      @@wh.wafer_history_report(cj, li, eqpiraw.equipmentID.identifier, pstatus.portID.identifier)
    }
    @@results['test7021'] = [lot, cj]
  end

  # modified for force_opecomp
  def test7022_wafer_history_proc
    assert_equal 0, @@sv.eqp_cleanup(@@eqp_proc), "error cleaning up #{@@eqp_proc}"
    assert_equal 0, @@sv.eqp_cleanup(@@eqp_proc2), "error cleaning up #{@@eqp_proc2}"
    assert lot = @@svtest.new_lot(nwafers: 13), 'error creating lot'
    @@testlots << lot
    assert_equal 0, @@sv.sublottype_change(lot, @@lottype) unless @@lottype.nil?
    refute_empty opeqps = @@sv.lot_eqplist_in_route(lot, eqp: @@eqp_proc), "no PD for #{@@eqp_proc}"
    assert_equal 0, @@sv.lot_opelocate(lot, opeqps.keys.first), 'error creating lot'
    # first try with opestart_cancel after 2 WaferComplete events
    assert cj = @@sv.claim_process_lot(lot, eqp: @@eqp_proc, opestart_cancel: true) {|cj, cjinfo, eqpi, pstatus|
      li = @@sv.lot_info(lot, wafers: true, select: [2, 5])
      @@wh.wafer_history_report(cj, li, eqpiraw.equipmentID.identifier, pstatus.portID.identifier)
    }
    # 2nd try at 2nd eqp, with wafer history
    assert cj = @@sv.claim_process_lot(lot, eqp: @@eqp_proc2, running_hold: true) {|cj, cjinfo, eqpiraw, pstatus|
      li = @@sv.lot_info(lot, wafers: true)
      @@wh.wafer_history_report(cj, li, eqpiraw.equipmentID.identifier, pstatus.portID.identifier)
    }
    @@results['test7022'] = [lot, cj]
  end

  def test7023_wafer_history_batch
    assert_equal 0, @@sv.eqp_cleanup(@@eqp_proc), "error cleaning up #{@@eqp_proc}"
    assert_equal 0, @@sv.eqp_cleanup(@@eqp_proc2), "error cleaning up #{@@eqp_proc2}"
    assert lot = @@svtest.new_lot(nwafers: 13), 'error creating lot'
    @@testlots << lot
    assert_equal 0, @@sv.sublottype_change(lot, @@lottype) unless @@lottype.nil?
    refute_empty opeqps = @@sv.lot_eqplist_in_route(lot, eqp: @@eqp_proc), "no PD for #{@@eqp_proc}"
    assert_equal 0, @@sv.lot_opelocate(lot, opeqps.keys.first), 'error creating lot'
    assert lcs = 2.times.collect {@@sv.lot_split(lot, 4)}, 'error splitting lot'
    # first try with opestart_cancel
    assert cj = @@sv.claim_process_lot([lot] + lcs, eqp: @@eqp_proc, opestart_cancel: true)
    # 2nd try at 2nd eqp, with wafer history
    assert cj = @@sv.claim_process_lot([lot] + lcs, eqp: @@eqp_proc2) {|cj, cjinfo, eqpiraw, pstatus, mode, linfo|
      ([lot] + lcs).each {|l|
        li = @@sv.lot_info(l, wafers: true)
        @@wh.wafer_history_report(cj, li, eqpiraw.equipmentID.identifier, pstatus.portID.identifier)
      }
    }
    @@results['test7023'] = [[lot] + lcs, cj]
  end


  # opestart cancel, split then process on other eqp with wafer history

  def test7031_wafer_history_meas
    assert_equal 0, @@sv.eqp_cleanup(@@eqp_meas), "error cleaning up #{@@eqp_meas}"
    assert_equal 0, @@sv.eqp_cleanup(@@eqp_meas2), "error cleaning up #{@@eqp_meas2}"
    assert lot = @@svtest.new_lot(nwafers: 9), 'error creating lot'
    @@testlots << lot
    assert_equal 0, @@sv.sublottype_change(lot, @@lottype) unless @@lottype.nil?
    refute_empty opeqps = @@sv.lot_eqplist_in_route(lot, eqp: @@eqp_meas), "no PD for #{@@eqp_meas}"
    assert_equal 0, @@sv.lot_opelocate(lot, opeqps.keys.first), 'error creating lot'
    # first try with opestart_cancel
    assert cj = @@sv.claim_process_lot(lot, eqp: @@eqp_meas, opestart_cancel: true)
    # split and 2nd try with wafer history
    assert lcsplit = @@sv.lot_split(lot, 1)
    assert cj = @@sv.claim_process_lot(lot, eqp: @@eqp_meas2) {|cj, cjinfo, eqpiraw, pstatus|
      li = @@sv.lot_info(lot, wafers: true, select: [2, 3, 6])
      @@wh.wafer_history_report(cj, li, eqpiraw.equipmentID.identifier, pstatus.portID.identifier)
    }
    @@results['test7031'] = [lot, cj]
  end

  def test7032_wafer_history_proc
    assert_equal 0, @@sv.eqp_cleanup(@@eqp_proc), "error cleaning up #{@@eqp_proc}"
    assert_equal 0, @@sv.eqp_cleanup(@@eqp_proc2), "error cleaning up #{@@eqp_proc2}"
    assert lot = @@svtest.new_lot(nwafers: 13), 'error creating lot'
    @@testlots << lot
    assert_equal 0, @@sv.sublottype_change(lot, @@lottype) unless @@lottype.nil?
    refute_empty opeqps = @@sv.lot_eqplist_in_route(lot, eqp: @@eqp_proc), "no PD for #{@@eqp_proc}"
    assert_equal 0, @@sv.lot_opelocate(lot, opeqps.keys.first), 'error creating lot'
    # first try with opestart_cancel
    assert cj = @@sv.claim_process_lot(lot, eqp: @@eqp_proc, opestart_cancel: true)
    # split and 2nd try with wafer history
    assert lcsplit = @@sv.lot_split(lot, 1)
    assert cj = @@sv.claim_process_lot(lot, eqp: @@eqp_proc2) {|cj, cjinfo, eqpiraw, pstatus|
      li = @@sv.lot_info(lot, wafers: true)
      @@wh.wafer_history_report(cj, li, eqpiraw.equipmentID.identifier, pstatus.portID.identifier)
    }
    @@results['test7032'] = [lot, cj]
  end

  def test7033_wafer_history_batch
    assert_equal 0, @@sv.eqp_cleanup(@@eqp_proc), "error cleaning up #{@@eqp_proc}"
    assert_equal 0, @@sv.eqp_cleanup(@@eqp_proc2), "error cleaning up #{@@eqp_proc2}"
    assert lot = @@svtest.new_lot(nwafers: 13), 'error creating lot'
    @@testlots << lot
    assert_equal 0, @@sv.sublottype_change(lot, @@lottype) unless @@lottype.nil?
    refute_empty opeqps = @@sv.lot_eqplist_in_route(lot, eqp: @@eqp_proc), "no PD for #{@@eqp_proc}"
    assert_equal 0, @@sv.lot_opelocate(lot, opeqps.keys.first), 'error creating lot'
    assert lcs = 2.times.collect {@@sv.lot_split(lot, 4)}, 'error splitting lot'
    # first try with opestart_cancel
    assert cj = @@sv.claim_process_lot([lot] + lcs, eqp: @@eqp_proc, opestart_cancel: true)
    # 2nd try with wafer history
    assert lcsplit = @@sv.lot_split(lot, 1)
    assert cj = @@sv.claim_process_lot([lot] + lcs, eqp: @@eqp_proc2) {|cj, cjinfo, eqpiraw, pstatus|
      ([lot] + lcs).each {|l|
        li = @@sv.lot_info(l, wafers: true)
        @@wh.wafer_history_report(cj, li, eqpiraw.equipmentID.identifier, pstatus.portID.identifier)
      }
    }
    @@results['test7033'] = [[lot] + lcs, cj]
  end


  # opestart cancel, then change carrier and process on same eqp with wafer history

  def test7041_wafer_history_meas
    assert_equal 0, @@sv.eqp_cleanup(@@eqp_meas), "error cleaning up #{@@eqp_meas}"
    assert lot = @@svtest.new_lot(nwafers: 9), 'error creating lot'
    @@testlots << lot
    assert_equal 0, @@sv.sublottype_change(lot, @@lottype) unless @@lottype.nil?
    refute_empty opeqps = @@sv.lot_eqplist_in_route(lot, eqp: @@eqp_meas), "no PD for #{@@eqp_meas}"
    assert_equal 0, @@sv.lot_opelocate(lot, opeqps.keys.first), 'error creating lot'
    # first try with opestart_cancel
    assert cj = @@sv.claim_process_lot(lot, eqp: @@eqp_meas, opestart_cancel: true)
    # 2nd try with changed carrier and wafer history
    assert ec = @@svtest.get_empty_carrier, 'no empty carrier'
    assert_equal 0, @@sv.wafer_sort(lot, ec)
    assert cj = @@sv.claim_process_lot(lot, eqp: @@eqp_meas) {|cj, cjinfo, eqpiraw, pstatus|
      li = @@sv.lot_info(lot, wafers: true, select: [2, 3, 6])
      @@wh.wafer_history_report(cj, li, eqpiraw.equipmentID.identifier, pstatus.portID.identifier)
    }
    @@results['test7041'] = [lot, cj]
  end

  def test7042_wafer_history_proc
    assert_equal 0, @@sv.eqp_cleanup(@@eqp_proc), "error cleaning up #{@@eqp_proc}"
    assert lot = @@svtest.new_lot(nwafers: 13), 'error creating lot'
    @@testlots << lot
    assert_equal 0, @@sv.sublottype_change(lot, @@lottype) unless @@lottype.nil?
    refute_empty opeqps = @@sv.lot_eqplist_in_route(lot, eqp: @@eqp_proc), "no PD for #{@@eqp_proc}"
    assert_equal 0, @@sv.lot_opelocate(lot, opeqps.keys.first), 'error creating lot'
    # first try with opestart_cancel
    assert cj = @@sv.claim_process_lot(lot, eqp: @@eqp_proc, opestart_cancel: true)
    # 2nd try with wafer history
    assert ec = @@svtest.get_empty_carrier, "no empty carrier"
    assert_equal 0, @@sv.wafer_sort(lot, ec)
    assert cj = @@sv.claim_process_lot(lot, eqp: @@eqp_proc) {|cj, cjinfo, eqpiraw, pstatus|
      li = @@sv.lot_info(lot, wafers: true)
      @@wh.wafer_history_report(cj, li, eqpiraw.equipmentID.identifier, pstatus.portID.identifier)
    }

    @@results['test7042'] = [lot, cj]
  end

  def test7043_wafer_history_batch
    assert_equal 0, @@sv.eqp_cleanup(@@eqp_proc), "error cleaning up #{@@eqp_proc}"
    assert lot = @@svtest.new_lot(nwafers: 13), 'error creating lot'
    @@testlots << lot
    assert_equal 0, @@sv.sublottype_change(lot, @@lottype) unless @@lottype.nil?
    refute_empty opeqps = @@sv.lot_eqplist_in_route(lot, eqp: @@eqp_proc), "no PD for #{@@eqp_proc}"
    assert_equal 0, @@sv.lot_opelocate(lot, opeqps.keys.first), 'error creating lot'
    assert lcs = 2.times.collect {@@sv.lot_split(lot, 4)}, 'error splitting lot'
    # first try with opestart_cancel
    assert cj = @@sv.claim_process_lot([lot] + lcs, eqp: @@eqp_proc, opestart_cancel: true)
    # 2nd try in different carrier, with wafer history
    assert ec = @@svtest.get_empty_carrier, 'no empty carrier'
    ([lot] + lcs).each {|l| assert_equal 0, @@sv.wafer_sort(l, ec)}
    assert cj = @@sv.claim_process_lot([lot] + lcs, eqp: @@eqp_proc) {|cj, cjinfo, eqpiraw, pstatus|
      ([lot] + lcs).each {|l|
        li = @@sv.lot_info(l, wafers: true)
        @@wh.wafer_history_report(cj, li, eqpiraw.equipmentID.identifier, pstatus.portID.identifier)
      }
    }
    @@results['test7043'] = [[lot] + lcs, cj]
  end


  # process with wafer history, remeasure

  def test7051_wafer_history_meas
    assert_equal 0, @@sv.eqp_cleanup(@@eqp_meas), "error cleaning up #{@@eqp_meas}"
    assert lot = @@svtest.new_lot(nwafers: 9), 'error creating lot'
    @@testlots << lot
    assert_equal 0, @@sv.sublottype_change(lot, @@lottype) unless @@lottype.nil?
    refute_empty opeqps = @@sv.lot_eqplist_in_route(lot, eqp: @@eqp_meas), "no PD for #{@@eqp_meas}"
    # first process
    assert_equal 0, @@sv.lot_opelocate(lot, opeqps.keys.first), 'error locating lot'
      li = @@sv.lot_info(lot, wafers: true, select: [2, 3, 6])
      @@wh.wafer_history_report(cj, li, eqpiraw.equipmentID.identifier, pstatus.portID.identifier)
    }
    # remeasure
    assert_equal 0, @@sv.lot_opelocate(lot, opeqps.keys.first), 'error locating lot'
    assert cj2 = @@sv.claim_process_lot(lot, eqp: @@eqp_meas) {|cj, cjinfo, eqpiraw, pstatus|
      li = @@sv.lot_info(lot, wafers: true, select: [2, 3, 6])
      @@wh.wafer_history_report(cj, li, eqpiraw.equipmentID.identifier, pstatus.portID.identifier)
    }
    @@results['test7051'] = [lot, [cj, cj2]]
  end

  def test7059_futurehold_and_hold
    assert lot = @@svtest.new_lot, 'error creating lot'
    @@testlots << lot
    assert_equal 0, @@sv.sublottype_change(lot, @@lottype) unless @@lottype.nil?
    assert nextopNo = @@sv.lot_operation_list(lot, forward: true, history: false, current: false).first.opNo
    memo = unique_string
    @@sv.lot_futurehold(lot, nextopNo, 'C-ENG', memo: memo)
    @@sv.lot_opelocate(lot, nextopNo)
    @@sv.lot_hold(lot, 'C-ENG', memo: memo)

    @@results['test7059'] = lot
  end

end
