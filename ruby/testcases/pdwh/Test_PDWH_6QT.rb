=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: Thomas Klose and Steffen Steidten, 2016-07-06

History:
  2017-09-11 sfrieske, added TCs 6170 and 6171
  2021-08-30 sfrieske, moved pdwhtest.rb and pdwhdb.rb to misc
=end

require 'misc/pdwhtest'
require 'RubyTestCase'


# test cases for PDWH 2.8; July 2016
class Test_PDWH_6QT < RubyTestCase

  def self.startup
    super
    @@pdwhtest = PDWH::Test.new($env, sv: @@sv, route: 'P-PDWH-QT-MAINPD.02', product: 'PDWHQT.02')
    @@lots = {}
  end

  def self.after_all_passed
    $log.info "\n#{@@lots.pretty_inspect}"
  end


  def test6010TL_DFLT_Route_Hold_NotExp
    # "Lot enters TL area (by OperationComplete)
    # defined TL duration is NOT expired
    # Lot leave the TL area"
    assert lot = @@pdwhtest.new_lot
    assert @@sv.lot_opelocate(lot, '2000.1000')
    assert @@sv.claim_process_lot(lot)
    sleep 60
    assert @@sv.lot_opelocate(lot, "2000.1200")
    assert @@sv.claim_process_lot(lot)

    @@lots["test6010TL_DFLT_Route_Hold_NotExp"] = lot
  end

  def test6020TL_DFLT_Route_Hold_Exp
    # "Lot enters TL area (by OperationComplete)
    # defined TL duration is expired
    # Lot is put on Hold
    # Lot is released (later on)
    # Lot leave the TL area"
    assert lot = @@pdwhtest.new_lot
    assert @@sv.lot_opelocate(lot, '2000.1000')
    assert @@sv.claim_process_lot(lot)
    sleep 360
    assert @@sv.lot_hold_release(lot, nil)
    sleep 60
    assert @@sv.lot_opelocate(lot, "2000.1200")
    assert @@sv.claim_process_lot(lot)

    @@lots["test6020TL_DFLT_Route_Hold_Exp"] = lot
  end

  def test6030TL_DFLT_Route_DispPrecede_NotExp
    # "Lot enters TL area (by OperationComplete)
    # defined TL duration is NOT expired
    # Lot leave the TL area"
    assert lot = @@pdwhtest.new_lot
    assert @@sv.lot_opelocate(lot, "3000.1000")
    assert @@sv.claim_process_lot(lot)
    sleep 60
    assert @@sv.lot_opelocate(lot, "3000.1200")
    assert @@sv.claim_process_lot(lot)

    @@lots["test6030TL_DFLT_Route_DispPrecede_NotExp"] = lot
  end

  def test6040TL_DFLT_Route_DispPrecede_Exp
    # "Lot enters TL area (by OperationComplete)
    # defined TL duration is expired
    # ???"
    assert lot = @@pdwhtest.new_lot
    assert @@sv.lot_opelocate(lot, "3000.1000")
    assert @@sv.claim_process_lot(lot)
    sleep 360
    assert @@sv.lot_opelocate(lot, "3000.1200")
    assert @@sv.claim_process_lot(lot)

    @@lots["test6040TL_DFLT_Route_DispPrecede_Exp"] = lot
  end

  def test6050TL_DFLT_Route_GatePass
    # "Lot enters TL area (by GatePass of Trigger PD)
    # defined TL duration is NOT expired
    # Lot leave the TL area"
    assert lot = @@pdwhtest.new_lot
    assert @@sv.lot_opelocate(lot, '2000.1000')
    assert @@sv.lot_gatepass(lot)
    sleep 60
    assert @@sv.lot_opelocate(lot, "2000.1200")
    assert @@sv.lot_gatepass(lot)

    @@lots["test6050TL_DFLT_Route_GatePass"] = lot
  end

  def test6060TL_DFLT_Route_LocateFwd
    # "Lot enters TL area (by LocateForward to anyPD after Trigger PD)
    # defined TL duration is NOT expired
    # Lot leave the TL area"
    assert lot = @@pdwhtest.new_lot
    assert @@sv.lot_opelocate(lot, '2000.1000')
    sleep 60
    assert @@sv.lot_opelocate(lot, "2000.1100")
    sleep 60
    assert @@sv.lot_opelocate(lot, "2000.1300")

    @@lots["test6060TL_DFLT_Route_LocateFwd"] = lot
  end

  def test6070TL_DFLT_Route_2Act_NotExp
    # "Lot enters TL area (by OperationComplete)
    # defined TL duration is NOT expired
    # Lot leave the TL area without triggering any action"
    assert lot = @@pdwhtest.new_lot
    assert @@sv.lot_opelocate(lot, "4000.1000")
    assert @@sv.claim_process_lot(lot)
    sleep 60
    assert @@sv.lot_opelocate(lot, "2000.1200")
    assert @@sv.claim_process_lot(lot)

    @@lots["test6070TL_DFLT_Route_2Act_NotExp"] = lot
  end

  def test6071TL_DFLT_Route_2Act_NotExp
    # "Lot enters TL area (by OperationComplete)
    # defined TL duration is NOT expired
    # Lot leave the TL area without triggering any action"
    assert lot = @@pdwhtest.new_lot
    assert @@sv.lot_opelocate(lot, "4000.1000")
    assert @@sv.lot_gatepass(lot)
    sleep 60
    assert @@sv.lot_opelocate(lot, "2000.1200")
    assert @@sv.claim_process_lot(lot)

    @@lots["test6071TL_DFLT_Route_2Act_NotExp"] = lot
  end

  def test6080TL_DFLT_Route_2Act_Exp
    # "Lot enters TL area (by OperationComplete)
    # defined TL duration is expired
    # FutureHold is performed
    # Later on ImmediateHold is performed"
    assert lot = @@pdwhtest.new_lot
    assert @@sv.lot_opelocate(lot, "4000.1000")
    assert @@sv.claim_process_lot(lot)
    sleep 360
    assert @@sv.lot_opelocate(lot, "4000.1200")
    assert @@sv.claim_process_lot(lot, proctime: 60*30)  # processtime should be 30min for ITDC

    @@lots["test6080TL_DFLT_Route_2Act_Exp"] = lot
  end

  def test6081TL_DFLT_Route_2Act_Exp
    # "Lot enters TL area (by OperationComplete)
    # defined TL duration is expired
    # FutureHold is performed
    # Later on ImmediateHold is performed"
    assert lot = @@pdwhtest.new_lot
    assert @@sv.lot_opelocate(lot, "4000.1000")
    assert @@sv.claim_process_lot(lot)
    sleep 360
    assert @@sv.lot_opelocate(lot, "4000.1200")
    assert @@sv.lot_gatepass(lot)
    sleep 60
    assert @@sv.lot_opelocate(lot, "4000.1200")
    assert @@sv.lot_gatepass(lot)

    @@lots["test6081TL_DFLT_Route_2Act_Exp"] = lot
  end

  def test6082TL_DFLT_Route_2Act_Exp
    # "Lot enters TL area (by OperationComplete)
    # defined TL duration is expired
    # FutureHold is performed
    # Later on ImmediateHold is performed"
    assert lot = @@pdwhtest.new_lot
    assert @@sv.lot_opelocate(lot, "4000.1000")
    assert @@sv.claim_process_lot(lot)
    sleep 360
    assert @@sv.lot_opelocate(lot, "4000.1200")
    assert @@sv.lot_gatepass(lot)
    sleep 60
    assert @@sv.lot_opelocate(lot, "4000.1200")
    # Test setup broken, lot already has a QT hold
    assert @@sv.claim_process_lot(lot)

    @@lots["test6082TL_DFLT_Route_2Act_Exp"] = lot
  end

  def test6090TL_DFLT_Route_2Act_PartExp
    # "Lot enters TL area (by OperationComplete)
    # defined TL duration is expired
    # FutureHold is performed
    # lot leave TL area without performing 2nd action"
    assert lot = @@pdwhtest.new_lot
    assert @@sv.lot_opelocate(lot, "5000.1000")
    assert @@sv.claim_process_lot(lot)
    sleep 120
    assert @@sv.lot_opelocate(lot, "5000.1200")
    assert @@sv.claim_process_lot(lot)

    @@lots["test6090TL_DFLT_Route_2Act_PartExp"] = lot
  end

  def test6100TL_ProdSpec_Route_Exp
    # "Lot with ProdSpec xy enters TL area (by OperationComplete)
    # defined TL duration is expired
    # Lot is put on Hold
    # Lot is released (later on)
    # Lot leave the TL area"
    assert lot = @@pdwhtest.new_lot
    assert @@sv.lot_opelocate(lot, "6000.1000")
    assert @@sv.claim_process_lot(lot)
    sleep 360
    assert @@sv.lot_hold_release(lot, nil)
    assert @@sv.lot_opelocate(lot, "6000.1200")
    assert @@sv.claim_process_lot(lot)

    @@lots["test6100TL_ProdSpec_Route_Exp"] = lot
  end

  def test6101TL_ProdSpec_Route_Exp
    # "Lot with ProdSpec xy enters TL area (by OperationComplete)
    # defined TL duration is expired
    # Lot is put on Hold
    # Lot is released (later on)
    # Lot leave the TL area"
    assert lot = @@pdwhtest.new_lot
    assert @@sv.lot_opelocate(lot, "6000.1000")
    assert @@sv.claim_process_lot(lot)
    sleep 360
    assert @@sv.lot_hold_release(lot, nil)
    assert @@sv.scrap_wafers(lot)

    @@lots["test6101TL_ProdSpec_Route_Exp"] = lot
  end

  def test6110TL_Tech_Module_Exp
    # "Lot from Tech abc enters TL area (by OperationComplete)
    # defined TL duration is expired
    # Lot is put on Hold
    # Lot is released (later on)
    # Lot leave the TL area"
    assert lot = @@pdwhtest.new_lot
    assert @@sv.lot_opelocate(lot, "7000.1000")
    assert @@sv.claim_process_lot(lot)
    sleep 360
    assert @@sv.lot_hold_release(lot, nil)
    assert @@sv.lot_opelocate(lot, "7000.1200")
    assert @@sv.claim_process_lot(lot)

    @@lots["test6110TL_Tech_Module_Exp"] = lot

  end

  def test6120TL_Prdgrp_Route_Exp
    # "Lot from Prodgroup abc enters TL area (by OperationComplete)
    # defined TL duration is expired
    # Lot is put on Hold
    # Lot is released (later on)
    # Lot leave the TL area"
    assert lot = @@pdwhtest.new_lot
    assert @@sv.lot_opelocate(lot, "8000.1000")
    assert @@sv.claim_process_lot(lot)
    sleep 360
    assert @@sv.lot_hold_release(lot, nil)
    assert @@sv.lot_opelocate(lot, "8000.1200")
    assert @@sv.claim_process_lot(lot)

    @@lots["test6120TL_Prdgrp_Route_Exp"] = lot
  end

  def test6130TL_DFLT_Route_Split
    # "Lot enters TL area (by OperationComplete)
    # Lot gets splitted whil TL time is NOT expired yet
    # Both lots are moved to the nextPD (still within TL area)
    # TL time is expired
    # Action (Hold) is done for both lots ?"
    assert lot = @@pdwhtest.new_lot
    assert @@sv.lot_opelocate(lot, '2000.1000')
    assert @@sv.claim_process_lot(lot)
    sleep 60
    cl = @@sv.lot_split(lot, 10)
    assert @@sv.claim_process_lot(lot)

    @@lots["test6130TL_DFLT_Route_Split"] = [lot, cl]
  end

  def test6140TL_DFLT_Route_OperationStartCancel
    # "Lot enters TL area (by OperationComplete)
    # TL time is NOT expired yet
    # On target PD perform 2x OperationStart/OperationStartCancel
    # TL time is expired --> Lot is put on Hold
    # Lot gets released and lot leve the TL area"
    assert lot = @@pdwhtest.new_lot
    assert @@sv.lot_opelocate(lot, '2000.1000')
    assert @@sv.claim_process_lot(lot)
    sleep 60
    assert @@sv.lot_opelocate(lot, "2000.1200")
    assert @@sv.claim_process_lot(lot, opestart_cancel: true)
    sleep 60
    assert @@sv.claim_process_lot(lot, opestart_cancel: true)
    sleep 300
    assert @@sv.lot_hold_release(lot, nil)
    assert @@sv.claim_process_lot(lot)

    @@lots["test6140TL_DFLT_Route_OperationStartCancel"] = lot
  end

  def test6150_TL_DFLT_Route_Merge
    # Split a lot into 2 childs BEFORE TL area
    # Lot1 enters TL area
    # Lot2 enters the same TL area (some seconds later)
    # Both lots are merged
    # TL time is expired --> Lot is put on Hold
    # Lot gets released and lot leave TL area
    assert lot = @@pdwhtest.new_lot
    cl = @@sv.lot_split(lot, 10)
    assert @@sv.lot_opelocate(lot, '2000.1000')
    assert @@sv.claim_process_lot(lot)
    sleep 60
    assert @@sv.lot_opelocate(cl, '2000.1000')
    assert @@sv.claim_process_lot(cl)
    sleep 60
    assert @@sv.lot_merge(lot, cl)
    sleep 300
    assert @@sv.lot_hold_release(lot, nil)
    assert @@sv.lot_opelocate(lot, "3000.1200")
    assert @@sv.claim_process_lot(lot)

    @@lots["test6150_TL_DFLT_Route_Merge"] = [lot, cl]
  end

  def test6160_TL_DFLT_Route_Merge_Childlot_First
    # Split a lot into 2 childs BEFORE TL area
    # Child Lot enters TL area
    # Parent Lot enters the same TL area (some seconds later)
    # Both lots are merged
    # TL time is expired --> Lot is put on Hold
    # Lot gets released and lot leave TL area
    assert lot = @@pdwhtest.new_lot
    cl = @@sv.lot_split(lot, 10)
    assert @@sv.lot_opelocate(cl, '2000.1000')
    assert @@sv.claim_process_lot(cl)
    sleep 60
    assert @@sv.lot_opelocate(lot, '2000.1000')
    assert @@sv.claim_process_lot(lot)
    sleep 60
    assert @@sv.lot_merge(lot, cl)
    sleep 300
    assert @@sv.lot_hold_release(lot, nil)
    assert @@sv.lot_opelocate(lot, "3000.1200")
    assert @@sv.claim_process_lot(lot)

    @@lots["test6160_TL_DFLT_Route_Merge_Childlot_First"] = [lot, cl]
  end

  def test6170_TL_DFLT_Route_MultipleSplit
    assert lot = @@pdwhtest.new_lot
    assert cl = @@sv.lot_split(lot, 1)
    # move parent to QT area
    assert_equal 0, @@sv.lot_opelocate(lot, '2000.1000')
    assert @@sv.claim_process_lot(lot)
    sleep 60
    # move child to QT area
    assert_equal 0, @@sv.lot_opelocate(cl, '2000.1000')
    assert @@sv.claim_process_lot(cl)
    sleep 60
    # merge
    assert_equal 0, @@sv.lot_merge(lot, cl)
    # 2nd split
    assert cl2 = @@sv.lot_split(lot, 1)
    # move child and parent out of TL area
    sleep 60
    assert @@sv.claim_process_lot(lot)
    sleep 30
    assert @@sv.lot_gatepass(cl2)
    sleep 30
    assert @@sv.claim_process_lot(lot)
    assert @@sv.lot_gatepass(cl2)

    @@lots["test6170_TL_DFLT_Route_MultipleSplit"] = [lot, cl, cl2]
  end

  def test6175_TL_DFLT_Route_MultipleSplit2
    assert lot = @@pdwhtest.new_lot
    assert_equal 0, @@sv.schdl_change(lot, route: 'P-PDWH-QT-MAINPD.01')   # 4000.1000
    assert cl = @@sv.lot_split(lot, 2)
    # move child to QT area
    assert_equal 0, @@sv.lot_opelocate(cl, '2000.1000')
    assert @@sv.claim_process_lot(cl)
    sleep 60
    # move parent to QT area
    assert_equal 0, @@sv.lot_opelocate(lot, '2000.1000')
    assert @@sv.claim_process_lot(lot)
    sleep 60
    # process both lots at PD2 within TL area and merge
    assert @@sv.claim_process_lot([lot, cl])
    assert_equal 0, @@sv.lot_merge(lot, cl)
    sleep 60
    # process merged lot at PD3
    assert @@sv.claim_process_lot(lot)
    sleep 60
    # split same wafers again and process both lots at PD4
    assert cl1 = @@sv.lot_split(lot, 2)
    assert @@sv.claim_process_lot([lot, cl1])
    sleep 60
    # split off a grandchild and process all 3 lots at PD5
    assert cl11 = @@sv.lot_split(cl1, 1)
    assert @@sv.claim_process_lot([lot, cl1, cl11])
    sleep 60
    # merge grandchild back, then child back and process at PD6
    assert_equal 0, @@sv.lot_merge(cl1, cl11)
    assert_equal 0, @@sv.lot_merge(lot, cl1)
    assert @@sv.claim_process_lot(lot)
    sleep 60
    # split cl wafers off again and scrap
    assert cl2 = @@sv.lot_split(lot, 2)
    assert_equal 0, @@sv.scrap_wafers(cl2)
    sleep 60
    # move lot out of the TL area
    3.times {assert @@sv.lot_gatepass(lot)}

    @@lots["test6175_TL_DFLT_Route_MultipleSplit2"] = [lot, cl, cl1, cl11, cl2]
  end

  def test6185_RetriggerChildLot
    assert lot = @@pdwhtest.new_lot
    #assert_equal 0, @@sv.schdl_change(lot, route: 'P-PDWH-QT-MAINPD.01')   # 4000.1000
    #move parent to QT area
    assert_equal 0, @@sv.lot_opelocate(lot, '2000.1000')
    assert @@sv.claim_process_lot(lot)
    sleep 180
    # locate to end of tl area
    assert_equal 0, @@sv.lot_opelocate(lot, '2000.1200')
    assert cl = @@sv.lot_split(lot, 2)
    assert_equal 0, @@sv.lot_opelocate(cl, '2000.1000')
    assert @@sv.lot_gatepass(cl)
    assert_equal 0, @@sv.lot_opelocate(cl, '2000.1200')
    sleep 60
    assert @@sv.lot_gatepass(lot)
    assert @@sv.lot_gatepass(cl)
    @@lots["test6185_RetriggerChildLot"] = [lot, cl]
  end
end
