=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: Thomas Klose and Steffen Frieske, 2015-11-25

History:
  2021-09-15 sfrieske, finally replaced $svtest by @@svtest, $sv by @@sv and $sm by @@sm
=end

require_relative 'PDWHR'


# PDWH Regression tests for historization of UDATA, for EQP, INHIBIT
class PDWHR112 < PDWHR
  @@mmudata = Hash[10.times.collect {|i| ["PDWHTest#{i}", '']}] # multiple udata to catch intermittend errors
  @@smudata = {'EqpReportType'=>''}
  @@eqp = 'PDWHR0'
  # inhibits
  @@inh = nil
  @@iid = nil
  @@isk = nil


  def self.shutdown
    super
    #@@sv.eqp_cleanup(@@eqp)
  end


  def test11200_setup
    $setup_ok = false
    #
    # remove all inhibits and test UDATA
    assert_equal 0, @@sv.eqp_cleanup(@@eqp)
    assert_equal 0, @@sv.user_data_delete(:eqp, @@eqp, @@mmudata)
    assert @@sm.update_udata(:eqp, @@eqp, @@smudata)  # delete
    # create an inhibit
    assert @@inh = @@sv.inhibit_entity(:eqp, @@eqp)
    @@iid = inh.entityInhibitID.identifier
    inhs = @@sv.inhibit_list(:eqp, @@eqp)
    assert_equal 1, inhs.size, 'wrong inhibits'
    $log.info "using inhibit iid #{@@iid.inspect}"
    #
    $setup_ok = true
  end

  def test11210_set_change
    2.times {|loop|
      $log.info "waiting #{@@separation} s to separate records"; sleep @@separation
      $log.info "-- loop #{loop}"
      tstart = Time.now
      # set the UDATA
      v = unique_string
      @@mmudata.keys.each {|k| @@mmudata[k] = v}
      @@smudata.keys.each {|k| @@smudata[k] = v}
      # eqp
      assert_equal 0, @@sv.user_data_update(:eqp, @@eqp, @@mmudata)
      assert @@sm.update_udata(:eqp, @@eqp, @@smudata)
      # inhibit
      rtime = @@sv.siview_timestamp(Time.now + 3600)
      owner = loop.zero? ? @@sv.user : ''  # first set then 'unset'
      assert_equal 0, @@sv.inhibit_update(@@inh, inhibit_owner: owner, release_time: rtime)
      # wait for eqp UDATA and verify
      @@smudata.merge(@@mmudata).each_pair {|k, v|
        $log.info "---- k=#{k.inspect}, v=#{v.inspect}"
        assert @@pdwhtest.wait_loader(tstart: tstart) {
          @@lldb.udata(name: k, value: v, srctable: 'EQP', tstart: tstart).last
        }, 'loader timeout'
        assert u = @@lldb.udata(name: k, value: v, srctable: 'EQP', tstart: tstart).last
        assert uhist = @@lldb.udata(name: k, value: v, srctable: 'EQP', tstart: tstart, hist: true).last
        assert_equal u.parentsk, uhist.parentsk
        assert u.update_date >= uhist.valid_from
        assert_equal 'N', u.deleted
      }
      # wait for inhibit in first loop only
      if loop == 0
        $log.info "---- iid=#{@@iid.inspect}"
        assert @@pdwhtest.wait_loader {@@lldb.inhibit(name: @@iid, tstart: tstart).last}, 'loader timeout'
        @@isk = @@lldb.inhibit(name: @@iid, tstart: tstart).last.sk
      end
      # wait for inhibit owner change amd verify; owner is '' in 2nd loop
      $log.info "---- inhibit owner=#{owner.inspect}"
      val = owner.empty? ? 'IS NULL' : owner
      assert @@pdwhtest.wait_loader {
        @@lldb.udata(parentsk: @@isk, srctable: 'INHIBIT', name: 'Inhibit Owner', value: val).last
      }, 'loader timeout'
      assert u = @@lldb.udata(parentsk: @@isk, srctable: 'INHIBIT', name: 'Inhibit Owner', value: val).last
      assert uhist = @@lldb.udata(parentsk: @@isk, srctable: 'INHIBIT', name: 'Inhibit Owner', value: val, hist: true).last
      val = owner.empty? ? nil : owner
      assert_equal u.update_date, uhist.valid_from
      assert_equal 'N', u.deleted
      # wait for inhibit rtime change amd verify; true update even in 2nd loop
      $log.info "---- inhibit rtime=#{rtime.inspect}"
      assert @@pdwhtest.wait_loader {
        @@lldb.udata(parentsk: @@isk, srctable: 'INHIBIT', name: 'Expected Release Time', value: rtime).last
      }, 'loader timeout'
      assert u = @@lldb.udata(parentsk: @@isk, srctable: 'INHIBIT', name: 'Expected Release Time', value: rtime).last
      assert uhist = @@lldb.udata(parentsk: @@isk, srctable: 'INHIBIT', name: 'Expected Release Time', value: rtime, hist: true).last
      assert_equal u.update_date, uhist.valid_from
      assert_equal 'N', u.deleted
    }
  end

  def test11220_delete
    $log.info "waiting #{@@separation} s to separate records"; sleep @@separation
    tstart = Time.now
    assert_equal 0, @@sv.user_data_delete(:eqp, @@eqp, @@mmudata)
    # assert @@sm.update_udata(:eqp, @@eqp, Hash[@@smudata.each_pair.collect {|k, v| [k, nil]}], delete: true)
    assert @@sm.update_udata(:eqp, @@eqp, Hash[@@smudata.each_pair.collect {|k, v| [k, '']}])
    assert_equal 0, @@sv.inhibit_cancel(:eqp, @@eqp)  # UDATA is only deleted when the inhibit is canceled
    # wait for eqp UDATA deletion and verify
    @@smudata.merge(@@mmudata).each_pair {|k, v|
      assert @@pdwhtest.wait_loader {
        @@lldb.udata(name: k, value: v, srctable: 'EQP', tstart: tstart, deleted: 'Y').last
      }, 'loader timeout'
      assert u = @@lldb.udata(name: k, value: v, srctable: 'EQP', tstart: tstart).last
      assert uhist = @@lldb.udata(name: k, value: v, srctable: 'EQP', tstart: tstart, hist: true).last
      assert_equal u.parentsk, uhist.parentsk
      assert_equal u.update_date, uhist.valid_to
      assert_equal 'Y', u.deleted
    }
    # wait for inhibit UDATA deletion and verify
    ['Inhibit Owner', 'Expected Release Time'].each {|name|
      assert @@pdwhtest.wait_loader {
        @@lldb.udata(parentsk: @@isk, srctable: 'INHIBIT', name: name, deleted: 'Y').last
      }
      assert u = @@lldb.udata(parentsk: @@isk, srctable: 'INHIBIT', name: name).last
      assert uhist = @@lldb.udata(parentsk: @@isk, srctable: 'INHIBIT', name: name, hist: true).last
      assert_equal u.update_date, uhist.valid_to
      assert_equal 'Y', u.deleted
    }
  end


  # trying to replicate a GG update issue
  def testman11290_perf
    udata = {'PDWHTest99'=>''}
    assert_equal 0, @@sv.user_data_delete(:eqp, @@eqp, udata)
    10.times {|loop|
      $log.info "-- loop #{loop}"
      # update eqp udata
      tstart = Time.now
      udata['PDWHTest99'] = unique_string
      assert_equal 0, @@sv.user_data_update(:eqp, @@eqp, udata)
      # update inhibit udata
      rtime = @@sv.siview_timestamp(Time.now + 3600)
      assert_equal 0, @@sv.inhibit_update(@@inh, inhibit_owner: @@sv.user, release_time: rtime)
      # wait for eqp udata
      k, v = 'PDWHTest99', udata['PDWHTest99']
      $log.info "---- k=#{k.inspect}, v=#{v.inspect}"
      assert @@pdwhtest.wait_loader {@@lldb.udata(name: k, value: v, srctable: 'EQP', tstart: tstart).last}, 'loader timeout'
      # wait for inhibit in first loop only
      if loop == 0
        $log.info "---- iid=#{@@iid.inspect}"
        assert @@pdwhtest.wait_loader {@@lldb.inhibit(name: @@iid, tstart: tstart).last}, 'loader timeout'
        @@isk = @@lldb.inhibit(name: @@iid, tstart: tstart).last.sk
      end
      # wait for inhibit udata
      $log.info "---- inhibit rtime=#{rtime.inspect}"
      assert @@pdwhtest.wait_loader {
        @@lldb.udata(parentsk: @@isk, srctable: 'INHIBIT', name: 'Expected Release Time', value: rtime).last
      }, 'loader timeout'
    }
  end

end
