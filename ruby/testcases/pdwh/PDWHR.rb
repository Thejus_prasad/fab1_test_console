=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: Thomas Klose and Steffen Frieske, 2015-11-25

History:
  2021-08-30 sfrieske, moved pdwhtest.rb and pdwhdb.rb to misc
=end

require 'misc/pdwhtest'
require 'RubyTestCase'


# Common resources for PDWH Regression tests.
class PDWHR < RubyTestCase
  @@dbenv = nil  # for ITDC only, do not change here but in testparameters
  @@separation = 11  # for record separation
  @@loader_timeout = 1200 # for 'stress' DB typically < 15min


  def self.startup
    super
    @@dbenv ||= 'stress' if $env == 'itdc'
    @@pdwhtest = PDWH::Test.new($env, sv: @@sv, dbenv: @@dbenv, loader_timeout: @@loader_timeout)
    @@sm = @@pdwhtest.sm
    @@lldb = @@pdwhtest.lldb
    @@testlots = []  # to be deleted after the tests ran successfully
  end

  def self.after_all_passed
    @@testlots.each {|lot| @@sv.delete_lot_family(lot)}
  end

end
