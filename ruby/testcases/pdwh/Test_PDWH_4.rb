# encoding: UTF-8

=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: Thomas Klose and Steffen Steidten, 2015-06-10

History:
  2018-11-20 sfrieske, several fixes
  2019-11-13 sfrieske, minor cleanup
  2021-08-30 sfrieske, moved pdwhtest.rb and pdwhdb.rb to misc

Notes:
  https://www.google.com/url?q=http%3A%2F%2Fmyteamsgf%2Fsites%2FIEKPI%2FProduct%2FTest%2520documents%2FBusinessTestCases_Rel2.5.xlsx&ust=1436525053713000&usg=AFQjCNHzRh2-Dk8GG1pWJPGelS1reVlRYA
=end

require 'pp'
require 'misc/pdwhtest'
require 'RubyTestCase'


# test cases for PDWH 2.5; April 2015
class Test_PDWH_4 < RubyTestCase
  @@backup_env = 'f8stag'
  @@backup_opNo = '1000.200'
  @@nprobank = 'W-BACKUPOPER'

  @@product = 'UT-PRODUCT-FAB8.01'
  @@route = 'UTRT101.01'
  @@customer = 'qgt'
  @@nprobank_return = 'O-BACKUPOPER'


  def self.startup
    super
    @@pdwhtest = PDWH::Test.new($env, sv: @@sv)
    @@sv_back = SiView::MM.new(@@backup_env)
    @@lots = {}
  end

  def self.after_all_passed
    $log.info "\n#{@@lots.pretty_inspect}"
  end

  # perform Hold Lot (with Hold Memo)
  # perform a second Hold on the lot
  # update the existing HoldMemo for the second Hold
  # update the existing HoldMemo for the first Hold
  # perform Release Lot of the first Hold
  # perform Release Lot of the second Hold
  def test4002_holdmemoupdate
    lot = @@pdwhtest.new_lot
    assert_equal 0, @@sv.lot_hold(lot, "E-ENG", memo: "E-ENG 1")
    sleep 60
    assert_equal 0, @@sv.lot_hold(lot, "C-ENG", memo: "C-ENG 2")
    sleep 60
    assert_equal 0, @@sv.lot_holdmemo_update(lot, "E-ENG 3", reason: "E-ENG")
    sleep 60
    assert_equal 0, @@sv.lot_holdmemo_update(lot, "C-ENG 4", reason: "C-ENG")
    sleep 60
    assert_equal 0, @@sv.lot_hold_release(lot, "E-ENG")
    sleep 60
    assert_equal 0, @@sv.lot_hold_release(lot, "C-ENG")
    @@lots['test4002'] = lot
  end

  # perform several transport jobs, see separate tests
  def xtest4003_transportdetails
    pass
  end

  # setup inhibits based on different entity types (equipment, chamber, reticle, ...) and based on two different sub lot types
  def xtest4005_inhibits
    eqp = 'POL705'
    eqpchamber = 'ALC4361'
    chamber = 'STP'
    mrcp = "F-SNK501-Q.F-Q-VPD.01"
    lrcp = 'B-ME-ILINE-DEV.01'
    customer = 'nu-front'
    tech = '40EDR1REL'
    productgroup = '4048A'
    product = '1E-DN-U-EPI-NI1K.ENG'
    route = 'C02-1115.01'
    modulepd = '45-STI-10.01'
    opNo = '1100.1400'
    pd = 'PADOXM-0087.01'
    rg = '1470FC14NA'
    reticle = '1460EU033A1'
    #
    assert_equal 0, @@sv.eqp_cleanup(eqp)
    assert_equal 0, @@sv.eqp_cleanup(eqpchamber)
    assert_equal 0, @@sv.inhibit_cancel(:productgroup_eqp_recipe, productgroup)
    assert_equal 0, @@sv.inhibit_cancel(:lrecipe_eqp, lrecipe)
    assert_equal 0, @@sv.inhibit_cancel(:module, modulepd)
    assert_equal 0, @@sv.inhibit_cancel(:reticle, reticle)
    assert_equal 0, @@sv.inhibit_cancel(:rg_eqp, [rg, eqp])
    #
    res = {}
    #
    assert @@sv.inhibit_entity(:productgroup_eqp_recipe, [productgroup, eqp, mrcp], tstart: '', tend: '')
    res[:productgroup_eqp_recipe] = @@sv.inhibit_list(:productgroup_eqp_recipe, [productgroup, eqp, mrcp])
    #
    assert @@sv.inhibit_entity(:lrecipe_eqp, [lrcp, eqp], tstart: Time.now, tend: Time.now + 86400*365)
    res[:lrecipe_eqp] = @@sv.inhibit_list(:lrecipe_eqp, [lrcp, eqp])
    #
    assert @@sv.inhibit_entity(:eqp_chamber_pd, [eqpchamber, pd], attrib: [chamber], tstart: Time.now, tend: Time.now + 86400)
    res[:eqp_chamber_pd] = @@sv.inhibit_list(:eqp_chamber_pd, [eqpchamber, pd])
    #
    assert @@sv.inhibit_entity(:route_opNo_product, [route, product], attrib: [opNo], tstart: Time.now - 86400*365, tend: Time.now - 3600)
    res[:route_opNo_product] = @@sv.inhibit_list(:route_opNo_product, [route, product])
    #
    assert @@sv.inhibit_entity(:module, modulepd, tstart: Time.now, tend: Time.now + 86400)
    res[:module] = @@sv.inhibit_list(:module, modulepd)
    #
    assert @@sv.inhibit_entity(:reticle, reticle, tstart: '', tend: '', sublottypes: 'PO', memo: 'QA1')
    inh = @@sv.inhibit_list(:reticle, reticle, raw: true).first
    res[:reticle] = inh.entityInhibitID.identifier
    assert_equal 0, @@sv.inhibit_update(inh, comment: 'new QA comment')
    res[:reticle2] = @@sv.inhibit_list(:reticle, reticle)
    #
    assert @@sv.inhibit_entity(:rg_eqp, [rg, eqp], tstart: '', tend: '', sublottypes: ['PO', 'EB'])
    res[:rg_eqp] = @@sv.inhibit_list(:rg_eqp, [rg, eqp])
    #
    assert @@sv.inhibit_entity(:customer_technology_eqp, [customer, tech, eqp])
    res[:customer_technology_eqp] = @@sv.inhibit_list(:customer_technology_eqp, [customer, tech, eqp])
    #
    $res = res
    $log.info "Inhibits:\n#{res.pretty_inspect}"
  end

  # create new UDATA
  # update UDATAA
  # delete UDATA
  def test4008_posudata
    pass
  end

  # perform several Scraps with specific ReasonCodes
  # perform Unscraps with specific Reason Codes
  def test4013_isnolyflag
    scrapcodes = %w(CCC ENDE EQUI EXTE HUMA INFR LAB SUPP TECH)
    scrapcancelcodes = %w(ALL ENGC MISC)
    lots = []
    assert lot = @@pdwhtest.new_lot
    lots << lot
    for sc in scrapcodes
      @@sv.scrap_wafers(lot, reason: sc)
      sleep 30
      @@sv.scrap_wafers_cancel(lot, reason: scrapcancelcodes.rotate!.first)
    end

    assert lot = @@pdwhtest.new_lot(sublottype: "PR")
    lots << lot
    @@sv.scrap_wafers(lot, reason: "LAB")  #, wafers: ["01"])
    sleep 30
    @@sv.scrap_wafers_cancel(lot, reason: "ALL")  #, wafers: ["01"])

    assert lot = @@pdwhtest.new_lot(sublottype: "PW")
    lots << lot
    @@sv.scrap_wafers(lot, reason: "LAB")  #, wafers: ["02","03"])
    sleep 30
    @@sv.scrap_wafers_cancel(lot, reason: "ALL")  #, wafers: ["02","03"])

    assert lot = @@pdwhtest.new_lot(sublottype: "PR")
    assert_equal 0, @@sv.user_parameter_change('Lot', lot, 'SourceFabCode', "F8")
    lots << lot
    @@sv.scrap_wafers(lot, reason: "LAB")  #, wafers: ["04","05"])
    sleep 30
    @@sv.scrap_wafers_cancel(lot, reason: "ALL")  #, wafers: ["04","05"])

    # assert lot = @@pdwhtest.new_lot(sublottype: "PR")
    # lots << lot
    # @@sv.scrap_wafers(lot, reason: "DGRD", wafers: ["06"])
    # @@sv.scrap_wafers(lot, reason: "XMRB", wafers: ["07"])
    # @@sv.scrap_wafers(lot, reason: "XLY", wafers: ["08"])
    # @@sv.scrap_wafers(lot, reason: "LAB", wafers: ["09"])
    # sleep 30
    # @@sv.scrap_wafers_cancel(lot, reason: "ALL", wafers: ["06","07","08","09"])

    # assert lot = @@pdwhtest.new_lot(sublottype: "PR")
    # lots << lot
    # @@sv.scrap_wafers(lot, reason: "NCST", wafers: ["10"])
    # @@sv.scrap_wafers(lot, reason: "YUN", wafers: ["11"])
    # @@sv.scrap_wafers(lot, reason: "YUE", wafers: ["12"])
    # sleep 30
    # @@sv.scrap_wafers_cancel(lot, reason: "NOLY", wafers: ["10","12"])

    @@lots['test4013'] = lots
  end

  def xtest4014_scrap_heike
    lots = []
    assert lot = @@pdwhtest.new_lot(sublottype: 'PR')
    lots << lot
    assert_equal 0, @@sv.scrap_wafers(lot, reason: 'SORT')
    assert_equal 0, @@sv.schdl_change(lot, sublottype: 'ES')
    #
    assert lot = @@pdwhtest.new_lot(sublottype: 'PR')
    lots << lot
    assert_equal 0, @@sv.scrap_wafers(lot, reason: 'YUE')
    assert_equal 0, @@sv.schdl_change(lot, sublottype: 'ES')
    #
    assert lot = @@pdwhtest.new_lot(sublottype: 'PR')
    lots << lot
    assert @@sv.user_parameter_change('Lot', lot, 'SourceFabCode', 'F8')
    assert_equal 0, @@sv.scrap_wafers(lot, reason: 'SORT')
    assert_equal 0, @@sv.schdl_change(lot, sublottype: 'ES')
    #
    assert lot = @@pdwhtest.new_lot(sublottype: 'PR')
    lots << lot
    assert_equal 0, @@sv.scrap_wafers(lot, reason: 'SORT')
    assert_equal 0, @@sv.schdl_change(lot, sublottype: 'ES')
    $log.info "waiting 1 day before scrap cancel"; sleep 86400 + 300
    @@sv.scrap_wafers_cancel(lot, reason: 'NOLY')
    #
    $log.info "test lots: #{lots}"
  end

  # set lot based Script Parameter "ERP_ITEM" to several lots
  # track the lot through a PD (so that it is counted as an activity)
  def xtest4021_erpitem
    lots = {}

    lot = @@pdwhtest.new_lot  # lot 1 - opecomplete
    assert @@sv.claim_process_lot(lot)
    lots["lot 1 opecomp"] = lot

    lot = @@pdwhtest.new_lot  # lot 2 - gatepass and scrap
    assert @@sv.lot_gatepass(lot)
    assert @@sv.scrap_wafers(lot, wafers: 1)
    lots["lot 2 gpass scrap"] = lot

    lot = @@pdwhtest.new_lot  # lot 3 - ship
    assert @@sv.lot_opelocate(lot, "9900.1200") # bankin
    assert @@sv.lot_bankin(lot)
    assert @@sv.lot_bank_move(lot, "OY-SHIP")
    assert @@sv.lot_ship(lot, "OY-SHIP")
    lots["lot 3 ship"] = lot

    lot = @@pdwhtest.new_lot(route: "P-PDWH-MAINPD.09", product: "PDWHPROD1.09")  # lot 4 - fabout pd
    assert @@sv.lot_opelocate(lot, "9800.2000") # fabout pd
    assert @@sv.claim_process_lot(lot)
    lots["lot 4 fabout pd"] = lot

    $log.info "test lots #{__method__}: \n#{lots.pretty_inspect}"

  end

  # BTFOUT

  def test4031_btfout_opecomp
    assert lot = @@pdwhtest.new_lot(product: 'PDWHPROD1.09', route: 'P-PDWH-MAINPD.09')
    @@lots['test4031'] = lot
    assert_equal 0, @@sv.lot_opelocate(lot, nil, op: 'BTF1OUT.01')
    assert @@sv.claim_process_lot(lot)
  end

  def test4032_btfout_gatepass
    assert lot = @@pdwhtest.new_lot(product: 'PDWHPROD1.09', route: 'P-PDWH-MAINPD.09')
    @@lots['test4032'] = lot
    assert_equal 0, @@sv.lot_opelocate(lot, nil, op: 'BTF1OUT.01')
    assert_equal 0, @@sv.lot_gatepass(lot)
  end

  def test4033_btfout_forceopecomp
    assert lot = @@pdwhtest.new_lot(product: 'PDWHPROD1.09', route: 'P-PDWH-MAINPD.09')
    @@lots['test4033'] = lot
    assert_equal 0, @@sv.lot_opelocate(lot, nil, op: 'BTF1OUT.01')
    assert @@sv.claim_process_lot(lot, running_hold: true)
  end

  def test4034_btfout_locate
    assert lot = @@pdwhtest.new_lot(product: 'PDWHPROD1.09', route: 'P-PDWH-MAINPD.09')
    @@lots['test4034'] = lot
    assert_equal 0, @@sv.lot_opelocate(lot, nil, op: 'BTF1OUT.01')
    assert_equal 0, @@sv.lot_opelocate(lot, 1)
  end

  def test4035_btfout_scrap_cancel
    assert lot = @@pdwhtest.new_lot(product: 'PDWHPROD1.09', route: 'P-PDWH-MAINPD.09')
    @@lots['test4035'] = lot
    assert_equal 0, @@sv.lot_opelocate(lot, nil, op: 'BTF1OUT.01')
    assert @@sv.claim_process_lot(lot)
    # split, locate child back to BTF1OUT - scrap/cancel - preocess child again
    assert lc = @@sv.lot_split(lot, 2)
    assert_equal 0, @@sv.lot_opelocate(lc, -1)
    assert_equal 0, @@sv.scrap_wafers(lc)
    sleep 10
    assert_equal 0, @@sv.scrap_wafers_cancel(lc)
    assert @@sv.claim_process_lot(lc)
  end

  # FOUT-GATE

  def test4041_foutgate_opecomp
    assert lot = @@pdwhtest.new_lot(product: 'PDWHPROD1.09', route: 'P-PDWH-MAINPD.09')
    @@lots['test4041'] = lot
    assert_equal 0, @@sv.lot_opelocate(lot, nil, op: 'FOUT-GATE.01')
    assert @@sv.claim_process_lot(lot)
  end

  def test4042_foutgate_gatepass
    assert lot = @@pdwhtest.new_lot(product: 'PDWHPROD1.09', route: 'P-PDWH-MAINPD.09')
    @@lots['test4042'] = lot
    assert_equal 0, @@sv.lot_opelocate(lot, nil, op: 'FOUT-GATE.01')
    assert_equal 0, @@sv.lot_gatepass(lot)
  end

  def test4043_foutgate_forceopecomp
    assert lot = @@pdwhtest.new_lot(product: 'PDWHPROD1.09', route: 'P-PDWH-MAINPD.09')
    @@lots['test4043'] = lot
    assert_equal 0, @@sv.lot_opelocate(lot, nil, op: 'FOUT-GATE.01')
    assert @@sv.claim_process_lot(lot, running_hold: true)
  end

  def test4044_foutgate_locate
    assert lot = @@pdwhtest.new_lot(product: 'PDWHPROD1.09', route: 'P-PDWH-MAINPD.09')
    @@lots['test4044'] = lot
    assert_equal 0, @@sv.lot_opelocate(lot, nil, op: 'FOUT-GATE.01')
    assert_equal 0, @@sv.lot_opelocate(lot, 1)
  end

  def test4045_foutgate_scrap_cancel
    assert lot = @@pdwhtest.new_lot(product: 'PDWHPROD1.09', route: 'P-PDWH-MAINPD.09')
    @@lots['test4045'] = lot
    assert_equal 0, @@sv.lot_opelocate(lot, nil, op: 'FOUT-GATE.01')
    assert @@sv.claim_process_lot(lot)
    # split, locate child back to BTF1OUT - scrap/cancel - preocess child again
    assert lc = @@sv.lot_split(lot, 2)
    assert_equal 0, @@sv.lot_opelocate(lc, -1)
    assert_equal 0, @@sv.scrap_wafers(lc)
    sleep 10
    assert_equal 0, @@sv.scrap_wafers_cancel(lc)
    assert @@sv.claim_process_lot(lc)
  end

  def xtest4046_fout_locateback_fout
    assert lot = @@pdwhtest.new_lot(product: 'PDWHPROD1.09', route: 'P-PDWH-MAINPD.09')
    @@lots['test4046'] = lot
    assert_equal 0, @@sv.lot_gatepass(lot)
    assert_equal 0, @@sv.lot_opelocate(lot, nil, op: 'FAB1OUT.01')
    assert @@sv.claim_process_lot(lot)
    # locate back after 1h
    $log.info "waiting 1h to separate loader runs"; sleep 3660
    assert_equal 0, @@sv.lot_opelocate(lot, :first)
    assert lc = @@sv.lot_split(lot, 5)
    sleep 90
    assert_equal 0, @@sv.lot_gatepass(lot)
    assert_equal 0, @@sv.lot_opelocate(lot, nil, op: 'FAB1OUT.01')
    assert @@sv.claim_process_lot(lot)
  end

  def test4047_outs_bum_sor  # new behavior since May 2016
    assert lot = @@pdwhtest.new_lot(product: '3160CA.A4', route: 'C02-1701.01', carrier_category: 'C4', carrier: '%')
    @@lots['test4047'] = lot
    assert_equal 0, @@sv.lot_opelocate(lot, nil, op: /2NB-RFL/)
    assert @@sv.claim_process_lot(lot)
    sleep 10
    assert_equal 0, @@sv.lot_opelocate(lot, nil, op: /SORTSRT2DISPO/)
    assert @@sv.claim_process_lot(lot)
  end



  # create Lot with ""XFER_FLAG"" = 0, CURRENT_LOC_FLAG =0
  # create Lot with ""XFER_FLAG"" = 1, CURRENT_LOC_FLAG =0
  # create Lot with ""XFER_FLAG"" = 0, CURRENT_LOC_FLAG =1
  # create Lot with ""XFER_FLAG"" = 1, CURRENT_LOC_FLAG =1
  # track all 4 lots through a PD
  # Check setup:
  # Backup-Operation scenario to be setup:
  # Track one lot through the whole scenarion; use additional lots to remain at a a certain point in between
  def xtest4055_kpiinvlocationflags
    def lot_1
      $log.info("processing lot 1")
      lot = @@pdwhtest.new_lot(product: @@product, route: @@route, customer: @@customer)
      sleep 10
      @@sv.lot_hold_release(lot, nil)
      @@sv.claim_process_lot(lot)

      assert_equal 0, @@sv.lot_opelocate(lot, @@backup_opNo)
      assert_equal 0, @@sv.lot_nonprobankin(lot, @@nprobank)
      assert_equal 0, @@sv.wafer_sort(lot, "")

      # send lot to backup site
      assert_equal 0, @@sv.backupop_lot_send(lot, @@sv_back)

      return lot
    end

    def lot_2
      $log.info("processing lot 2")
      lot = @@pdwhtest.new_lot(product: @@product, route: @@route, customer: @@customer)
      sleep 10
      @@sv.lot_hold_release(lot, nil)

      assert_equal 0, @@sv.lot_opelocate(lot, @@backup_opNo)
      assert_equal 0, @@sv.lot_nonprobankin(lot, @@nprobank)
      assert_equal 0, @@sv.wafer_sort(lot, "")

      # send lot to backup site and receive lot at backup site
      assert_equal 0, @@sv.backupop_lot_send(lot, @@sv_back)
      assert_equal 0, @@sv_back.backupop_lot_send_receive(lot, @@sv)

      return lot
    end

    def lot_3
      $log.info("processing lot 3")
      lot = @@pdwhtest.new_lot(product: @@product, route: @@route, customer: @@customer)
      sleep 10
      @@sv.lot_hold_release(lot, nil)

      assert_equal 0, @@sv.lot_opelocate(lot, @@backup_opNo)
      assert_equal 0, @@sv.lot_nonprobankin(lot, @@nprobank)
      assert_equal 0, @@sv.wafer_sort(lot, "")

      # send lot to backup site and receive lot at backup site
      assert_equal 0, @@sv.backupop_lot_send(lot, @@sv_back)
      assert_equal 0, @@sv_back.backupop_lot_send_receive(lot, @@sv)

      # process lot at backup site and prepare for return
      assert @@sv_back.claim_process_lot(lot, mode: 'Auto-1')
      assert_equal 0, @@sv_back.lot_nonprobankin(lot, @@nprobank_return)
      assert_equal 0, @@sv_back.wafer_sort(lot, "")
      assert_equal 0, @@sv_back.backupop_lot_return(lot, @@sv)

      return lot
    end

    def lot_4
      $log.info("processing lot 4 - reference lot")
      lot = @@pdwhtest.new_lot(product: @@product, route: @@route, customer: @@customer)
      sleep 10
      @@sv.lot_hold_release(lot, nil)

      return lot
    end

    lots = [lot_4]#, lot_4] #lot_1, lot_2,
    $log.info("\ntest4055_kpiinvlocationflags #{lots}")
  end

  # perform transaction ""Ship"" with bank_ID ""%SHIP%"" for a lot
  # perform transaction ""NonProBankIn"" and bank_id ""O-TKY%"" for second lot
  # perform transaction ""MoveBank"" and bank_id ""O-TKY%"" for a third lot
  # perform transaction ""Branch""  to MainPD ""BY-LOCASS%"" for a fourth lot
  # Several ship transactions; Counting per sub KPI - perform other kind of shipment for prev Turnkey or LocalAssembly shipments

  # O-TKY nonpro bankin
  def xtest4081_kpiships_bankin
    assert lot = @@pdwhtest.new_lot(nwafers: 2)
    @@lots['test4081'] = lot
    assert @@sv.claim_process_lot(lot)
    assert_equal 0, @@sv.lot_nonprobankin(lot, 'O-TKYCBANK')
  end

  # O-TKY bank_move
  def xtest4082_kpiships_bankmove
    assert lot = @@pdwhtest.new_lot(nwafers: 2)
    @@lots['test4082'] = lot
    assert_equal 0, @@sv.lot_opelocate(lot, :last)
    @@sv.lot_bankin(lot)
    assert_equal 0, @@sv.lot_bank_move(lot, 'O-TKYWBANK')
  end

  def xtest4083_kpiships_branch_BYLOCASS_ship
    assert lot = @@pdwhtest.new_lot(nwafers: 2)
    @@lots['test4083'] = lot
    assert @@sv.claim_process_lot(lot)
    assert_equal 0, @@sv.lot_branch(lot, 'BY-LOCASS-BONDING.01', dynamic: true)
    #
    $log.info "waiting 1h to separate loader runs"; sleep 3660
    assert_equal 0, @@sv.lot_opelocate(lot, :last)
    @@sv.lot_bankin(lot)
    assert_equal 0, @@sv.lot_bank_move(lot, 'OT-SHIP-NS')
    assert_equal 0, @@sv.lot_ship(lot, 'OT-SHIP-NS')
  end

  # perform DataCollection WaferCount for one lot
  # RemeasurementLocate (changes OpePassCount??)
  # perform a Remeasurement of the same lot with the same WaferCount
  # perform a Remeasurement of the same lot with different WaferCount
  def xtest4144_measuredwafer
    lots = []

    lot = @@pdwhtest.new_lot
    lots << lot
    assert @@sv.schdl_change(lot, route: "P-PDWH-MAINPD.06")   # route with measurement op @ 5000.1000
    @@sv.carrier_xfer_status_change(@@sv.lot_info(lot).carrier, "MO", "UTSTO11")
    assert @@sv.lot_opelocate(lot, "5000.1000")
    assert @@sv.claim_process_lot(lot)
    sleep 60
    assert @@sv.lot_remeasure(lot)
    sleep 60
    assert @@sv.claim_process_lot(lot)
    sleep 60
    lc = @@sv.lot_split(lot, 6)
    assert @@sv.lot_remeasure(lot)
    sleep 60
    assert @@sv.claim_process_lot(lot)
    sleep 60
    assert @@sv.lot_merge(lot, lc)

    lot = @@pdwhtest.new_lot
    lots << lot
    assert @@sv.schdl_change(lot, route: "P-PDWH-MAINPD.06")   # route with measurement op @ 5000.1000
    @@sv.carrier_xfer_status_change(@@sv.lot_info(lot).carrier, "MO", "UTSTO11")
    assert @@sv.lot_opelocate(lot, "5000.1000")
    assert @@sv.claim_process_lot(lot)
    sleep 60
    assert @@sv.lot_remeasure(lot)
    assert @@sv.claim_process_lot(lot, running_hold: true)

    $log.info "test lot: #{lots}"
  end

  def xtest4227_closed_lot_ly
    # +Los wird mit 25 wafern auf PD "LOTSTART.01"  gestartet
    # +Los wird einige PDs (2 weitere PDs sollten ausreichen) weiter prozessiert
    # + 2 wafer des Loses werden gescrapt
    # + die restlichen wafer werden auf FAB1OUT PD located und dann mit OperationComplete diese PD wieder verlassen
    lot = @@pdwhtest.new_lot
    assert @@sv.schdl_change(lot, route: "P-PDWH-MAINPD.09")   # route with FAB1OUT.01
    2.times {assert @@sv.claim_process_lot(lot)}
    assert @@sv.scrap_wafers(lot, reason: "LAB", wafers: ["01", "02"])
    assert @@sv.lot_opelocate(lot, nil, op: 'FAB1OUT.01')
    # fixme: wafers one and two needs to be separated from carrier
    assert @@sv.claim_process_lot(lot)
  end

end
