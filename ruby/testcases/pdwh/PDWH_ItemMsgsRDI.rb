=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: Steffen Frieske, 2018-02-24

=end

require 'misc/tooleventsitem'
require_relative 'PDWHR'


# Send RDI EI ITEM messages to PDWH. Reticle script parameter updates are prepared but not not yet sent.
#   to send data to the ITDC DB: runtest 'PDWH_ItemMsgsRDI', 'itdc', parameters: {dbenv: 'itdc'}
class PDWH_ItemMsgsRDI < PDWHR
  @@eqp = 'RDI1101'
  @@pod = 'R29N'
  @@pod2 = 'R29N'
  @@reticle = 'UTRETICLE0003'   # prod: 'FENDER10BVAZCKK1'
  @@processsteps = %w(Initial PlateAlign LightCal ImageCalibration)

  def self.startup
    super
    @@ti = ToolEvents::Item.new($env, @@eqp, sv: @@sv, dbenv: @@dbenv, tsformat: 0)  # points to LET as default if $env is 'itdc'
  end


  def test00_setup
    # set reticle script parameters (unrelated to the inspection)
    parms = {'DefectScan'=>'OK', 'DefScanCtDown'=>5, 'LastExposureTime'=>@@sv.siview_timestamp,
             'ReleaseState'=>'OK', 'TotalEnergy'=>77.0, 'TotalFlashes'=>78}
    assert_equal 0, @@sv.user_parameter_change('Reticle', @@reticle, parms)
  end

  def test11
    @@ti.defaultvalues['Foup'] = @@pod
    @@ti.defaultvalues['SubEquipment'] = "#{@@pod}.1"
    @@ti.defaultvalues['Port'] = '1'  # _not_ RP1
    @@ti.defaultvalues['ReticleId'] = @@ti.defaultvalues['Reticle'] = @@reticle
    @@ti.defaultvalues['MainProcessRecipeName'] = "#{@@reticle}-Standard-P090-X5"
    @@ti.defaultvalues['Job'] = @@ti.defaultvalues['VisitID'] = "#{@@eqp}_#{Time.now.utc.to_s.gsub(' ', '_')}"
    @@ti.export(true)
    #
    ## @@sv.rtd_dispatch_list("V_#{@@eqp}", parameters: ['requestingEntity=EI_RDI', 'PortID=1'])
    # assert_equal 0, @@sv.user_parameter_change('Reticle', @@reticle, 'ProcessingState', "LOADREQ:#{@@eqp}-1")
    # assert_equal 0, @@sv.user_parameter_change('Reticle', @@reticle, 'ZToolEvent', '')
    @@ti.send_msg('ReticleReserved')
    #
    # assert_equal 0, @@sv.user_parameter_change('Reticle', @@reticle, {'Container'=>@@pod, 'Location'=>"#{@@eqp}-1"})
    # assert_equal 0, reticle_pod_xfer_status_change(@@pod, 'EI', eqp: @@eqp)
    # assert_equal 0, @@sv.user_parameter_change('Reticle', @@reticle, 'ProcessingState', "QUEUED:#{@@eqp}-1")
    @@ti.send_msg('ReticlePodAt')
    @@ti.reticle_move(["#{@@pod}.1", 'ToolClamp', 'Gripper', 'ToolClamp', 'Library.7']) {|subeqp, idx|
      if subeqp == "#{@@pod}.1"
        # after ReticleFrom pod.1:
        # asert_equal 0, @@sv.reticle_justinout(@@reticle, @@pod, 1, 'Just-Out')
        # assert_equal 0, @@sv.user_parameter_change('Reticle', @@reticle, {'Container'=>@@eqp, 'Location'=>@@eqp})
      end
    }
    ## @@sv.rtd_dispatch_list('Default_WNR', parameters: ["ReticlePodID=#{@@pod}", 'ReticleID=Empty', 'requestingEntity=EI_RDI'])
    # assert_equal 0, reticle_pod_xfer_status_change(@@pod, 'EO', eqp: @@eqp)
    @@ti.send_msg('ReticlePodFrom', data: {'Reticle'=>''})
    #
    @@ti.send_msg('ReticleStarted', data: {'EquipmentOnlineMode'=>'nil', 'PortOperationMode'=>'nil'})
    @@ti.reticle_move(['Library.7', 'ToolClamp', 'Stage']) {|subeqp, idx|
      if subeqp == 'ToolClamp'
        # after ReticleFrom ToolClamp:
        # assert_equal 0, @@sv.user_parameter_change('Reticle', @@reticle, 'ProcessingState', "EXECUTING:#{@@eqp}-1")
      end
    }
    # assert_equal 0, @@sv.user_parameter_change('Reticle', @@reticle, 'ZToolEvent', '100 RDI1101: Inspection Setup started')
    %w(Initial PlateAlign LightCal ImageCalibration).each {|step|
      @@ti.send_msg('InspectionStepStarted', data: {'ProcessStep'=>step})
    }
    # assert_equal 0, @@sv.user_parameter_change('Reticle', @@reticle, 'ZToolEvent', '102 RDI1101: Inspection started')
    @@ti.send_msg('ReticleProcessStarted')
    # long time for processing
    # assert_equal 0, @@sv.user_parameter_change('Reticle', @@reticle, 'ZToolEvent', '103 RDI1101: Inspection completed')
    @@ti.send_msg('ReticleProcessCompleted')
    #
    @@ti.reticle_move(['Stage', 'ToolClamp', 'Library.7']) {|subeqp, idx|
      if subeqp == 'ToolClamp'
        # after ReticleFrom ToolClamp:
        ## @@sv.rtd_dispatch_list   ## unknown parameters
      end
    }
    @@ti.send_msg('ReticleCompleted', data: {'Foup'=>'unknown'})
    #
    # assert_equal 0, reticle_pod_xfer_status_change(@@pod2, 'EI', eqp: @@eqp)
    @@ti.send_msg('ReticlePodAt', data: {'Foup'=>@@pod2})
    @@ti.reticle_move(['Library.7', 'ToolClamp', 'Gripper', 'ToolClamp', "#{@@pod2}.1"]) {|subeqp, idx|
      if subeqp == 'ToolClamp' && idx == 3
        # after ReticleFrom ToolClamp (last):
        # assert_equal 0, @@sv.reticle_justinout(@@reticle, @@pod2, 1, 'Just-In')
        # assert_equal 0, @@sv.user_parameter_change('Reticle', @@reticle, {'Container'=>@@pod2, 'Location'=>"#{@@eqp}-1"})
      end
    }
    # assert_equal 0, @@sv.user_parameter_change('Reticle', @@reticle, {'Container'=>@@pod2, 'Location'=>"#{@@eqp}-1-RFP"})
    # assert_equal 0, @@sv.user_parameter_change('Reticle', @@reticle, 'ProcessingState', "EXECUTING:#{@@eqp}-1")  # sent again!
    # assert_equal 0, @@sv.user_parameter_change('Reticle', @@reticle, 'ProcessingState', "ScanDone:#{@@eqp}-1")
    ## @@sv.rtd_dispatch_list('Default_WNR', parameters: ["ReticlePodID=#{@@pod2}", "ReticleID=#{@@reticle}", 'requestingEntity=EI_RDI'])
    # assert_equal 0, reticle_pod_xfer_status_change(@@pod2, 'EO', eqp: @@eqp)
    @@ti.send_msg('ReticlePodFrom', data: {'Foup'=>@@pod2})
    @@ti.send_msg('ReticleLeftEquipment')
  end

end
