=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: Thomas Klose and Steffen Steidten, 2012-09-14

History:
  2021-09-15 sfrieske, finally replaced $svtest by @@svtest and $sm by @@sm
=end

require 'pp'
require 'misc/pdwhdb'
require 'SiViewTestCase'


# Xfer job test cases for PDWH 2.5
class Test_PDWH_4Xfer < SiViewTestCase
  @@eqpib = 'UTI002'
  @@opNo_ib = '1000.200'
  @@eqpmode = 'Semi-2'
  @@stocker2 = 'UTSTO12'
  @@stocker3 = 'UTSTO13'
  # changed this to match RTD_Rule_xx tests, needs verification (was 'Location'=>'F36' and 'BTF')
  @@loc1 = {'AmhsArea'=>'F36', 'Location'=>'M1-L3-MAIN'}
  @@loc2 = {'AmhsArea'=>'F38', 'Location'=>'M2-L3-MAIN'}
  # from PDWH_3XFER#test41
  @@eqp = 'UTF001'
  @@port = 'P1'
  @@eqp2 = 'UTF002'
  @@port2 = 'P1'


  @@timeout = 180


  def self.startup
    super
    $dummyamhs = Dummy::AMHS.new($env)
    $pdwhdb = PDWH::LLDB.new($env)
    @@carriers = []
    @@testlots = []  # will be cleaned up!
    @@xjs = {}
  end

  def self.shutdown
    @@carriers.each {|c| @@svtest.cleanup_carrier(c)}
    @@testlots.each {|lot| @@sv.delete_lot_family(lot)}
    $dummyamhs.set_variable('intrabaytime', 15000)
    $dummyamhs.set_variable('fallbackstocker', @@svtest.stocker)
  end

  def self.after_all_passed
    $log.info "\n#{@@xjs.pretty_inspect}"
  end

  def teardown
    $dummyamhs.set_variable('e84mode', false) # deactivate E84 failure mode
  end


  def test00_setup
    $setup_ok = false
    #
    assert @@carriers = @@svtest.get_empty_carrier(n: 4)
    @@carriers.each {|c| assert @@svtest.cleanup_carrier(c, status: 'NOTAVAILABLE')}
    [@@eqpib].each {|eqp| assert @@svtest.cleanup_eqp(eqp, mode: @@eqpmode)}
    $dummyamhs = Dummy::AMHS.new($env)
    assert $dummyamhs.register
    assert $dummyamhs.set_variable('intrabaytime', 45000)
    assert $dummyamhs.set_variable('stockintime', 1000)
    assert $dummyamhs.set_variable('stockouttime', 1000)
    assert @@sm.update_udata(:stocker, @@svtest.stocker, @@loc1)
    assert @@sm.update_udata(:stocker, @@stocker2, @@loc1)
    assert @@sm.update_udata(:stocker, @@stocker3, @@loc1)
    assert @@sm.update_udata(:eqp, @@eqpib, @@loc2)
    #
    $setup_ok = true
  end

  def test4301_sto_sto
    # same locations
    c = @@carriers[0]
    assert @@svtest.cleanup_carrier(c, stocker: @@stocker2)
    #assert_equal 0, @@sv.carrier_xfer_status_change(c, 'SI', @@stocker2)
    assert_equal 0, @@sv.carrier_reserve(c)
    assert_equal 0, @@sv.carrier_xfer(c, @@stocker2, '', @@stocker3, '')
    xjs = @@sv.carrier_xferjob_detail(c)
    @@xjs['4301'] = xjs.first.jobID
    assert wait_for {@@sv.carrier_xferjob_detail(c).empty?}
  end

  def test4302_sto_stother
    # different locations
    c = @@carriers[0]
    assert @@svtest.cleanup_carrier(c)
    #assert_equal 0, @@sv.carrier_xfer_status_change(c, 'SI', @@svtest.stocker)
    assert_equal 0, @@sv.carrier_xfer(c, @@svtest.stocker, '', @@stocker2, '')
    xjs = @@sv.carrier_xferjob_detail(c)
    @@xjs['4302'] = xjs.first.jobID
    assert wait_for {@@sv.carrier_xferjob_detail(c).empty?}
  end

  def test4303_sto_eqp_4
    # same locations, with cj
    assert lots = @@svtest.new_lots(4)
    @@testlots += lots
    carriers = []
    lots.each {|lot|
      c = @@sv.lot_info(lot).carrier
      carriers << c
      assert_equal 0, @@sv.carrier_xfer_status_change(c, 'MI', @@stocker2)
      assert_equal 0, @@sv.lot_opelocate(lot, @@opNo_ib)
    }
    assert_equal 0, @@sv.eqp_cleanup(@@eqpib, mode: @@eqpmode)
    assert @@sv.slr(@@eqpib, port: 'P1', lot: lots, carrier: carriers)
    #
    assert_equal 0, @@sv.carrier_xfer(carriers, @@stocker2, '', @@eqpib, 'P1')
    assert wait_for(timeout: 60) {!@@sv.carrier_xferjob(carriers.first).empty?}, "no xfer job"
    xjs = @@sv.carrier_xferjob_detail(c)
    @@xjs['4303'] = xjs.first.jobID
    assert wait_for {@@sv.carrier_xferjob_detail(c).empty?}
  end

  def test4304_reroute
    # same locations, with cj and reroute
    assert lot = @@svtest.new_lot
    @@testlots << lot
    assert_equal 0, @@sv.lot_opelocate(lot, @@opNo_ib)
    c = @@sv.lot_info(lot).carrier
    assert_equal 0, @@sv.carrier_xfer_status_change(c, 'MI', @@stocker2)
    assert_equal 0, @@sv.eqp_cleanup(@@eqpib, mode: @@eqpmode)
    assert_equal 0, @@sv.carrier_xfer(c, @@stocker2, '', @@stocker3, '')
    assert @@sv.slr(@@eqpib, port: 'P1', lot: lot, carrier: c)
    sleep 1
    xjs = @@sv.carrier_xferjob_detail(c)
    # $log.info "xj: #{xjs}"
    assert_equal 0, @@sv.carrier_xfer(c, '', '', @@eqpib, 'P1')
    xjs = @@sv.carrier_xferjob_detail(c)
    @@xjs['4304'] = xjs.first.jobID
    assert wait_for {@@sv.carrier_xferjob_detail(c).empty?}
  end

  def test4305_mcs_reroute
    # MCS reroute, with cj
    assert lot = @@svtest.new_lot
    @@testlots << lot
    assert_equal 0, @@sv.lot_opelocate(lot, @@opNo_ib)
    c = @@sv.lot_info(lot).carrier
    assert_equal 0, @@sv.carrier_xfer_status_change(c, 'MI', @@stocker2)
    $dummyamhs.set_variable('e84mode', true) # activate E84 failure mode
    assert $dummyamhs.set_variable('fallbackstocker', @@stocker3)
    assert_equal 0, @@sv.eqp_cleanup(@@eqpib)
    assert_equal 0, @@sv.carrier_xfer(c, @@stocker2, '', @@eqpib, 'P1')
    xjs = @@sv.carrier_xferjob_detail(c)
    @@xjs['4305'] = xjs.first.jobID
    assert wait_for {@@sv.carrier_xferjob_detail(c).empty?}
  end

  def test4306_sto_sto_cancel
    # same locations
    c = @@carriers[0]
    assert @@svtest.cleanup_carrier(c, stocker: @@stocker2)
    assert $dummyamhs.set_variable('fallbackstocker', @@stocker2)
    assert_equal 0, @@sv.carrier_xfer(c, @@stocker2, '', @@stocker3, '')
    xjs = @@sv.carrier_xferjob_detail(c)
    @@xjs['4306'] = xjs.first.jobID
    assert_equal 0, @@sv.carrier_xferjob_delete(c)
  end

  def test4307_sto_sto_cancel_after_pickup
    # same locations
    c = @@carriers[0]
    assert @@svtest.cleanup_carrier(c, stocker: @@stocker2)
    assert $dummyamhs.set_variable('fallbackstocker', @@stocker2)
    assert_equal 0, @@sv.carrier_xfer(c, @@stocker2, '', @@stocker3, '')
    xjs = @@sv.carrier_xferjob_detail(c)
    @@xjs['4307'] = xjs.first.jobID
    assert_equal 0, @@sv.carrier_xferjob_delete(c)
  end

  def test4308_sto_sto_port
    # same locations
    c = @@carriers[0]
    assert @@svtest.cleanup_carrier(c, stocker: @@stocker2)
    assert_equal 0, @@sv.carrier_xfer(c, @@stocker2, '', @@stocker3, 'P1')
    xjs = @@sv.carrier_xferjob_detail(c)
    @@xjs['4308'] = xjs.first.jobID
    assert wait_for {@@sv.carrier_xferjob_detail(c).empty?}
  end

  # was: PDWH_3XFER#test41_batch_xfer
  def test4311_batch_xfer
    # tools in Off-Line-2
    assert_equal 0, @@sv.eqp_mode_change(@@eqp, 'Off-Line-2')
    assert_equal 0, @@sv.eqp_mode_change(@@eqp2, 'Off-Line-2')
    # batch transfer
    assert_equal 0, @@sv.carrier_xfer(@@carriers, '', '', [@@eqp, @@eqp2], [@@port, @@port2])
    xjs = @@sv.carrier_xferjob_detail(@carriers[0])
    @@xjs['4311'] = xjs.first.jobID
    assert wait_for(timeout: @@timeout) {@@sv.carrier_xferjob(@@carriers[0]).empty?}
    assert wait_for(timeout: @@timeout) {@@sv.carrier_xferjob(@@carriers[1]).empty?}
  end

end
