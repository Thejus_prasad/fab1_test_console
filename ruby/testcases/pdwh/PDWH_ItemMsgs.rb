=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: Steffen Frieske, 2018-02-14

=end

require 'zip'
require 'misc/tooleventsitem'
require_relative 'PDWHR'


# Send EI ITEM messages to PDWH.
#   to send data to the ITDC DB: runtest 'PDWH_ItemMsgs', 'itdc', parameters: {dbenv: 'itdc'}
class PDWH_ItemMsgs < PDWHR
  @@tools = %w(WrongMsgs POL701 RTA300 RDI Alarms)  # for test11
  @@eqp = 'PDWHA01'
  @@dbenv = 'stress'  # instance name 'stress' points to physical LET instances for MQ and DB instances

  def self.startup
    super
    @@ti = ToolEvents::Item.new($env, @@eqp, sv: @@sv, dbenv: @@dbenv)  # points to LET as default if $env is 'itdc'
  end


  def test00_setup
  end

  def test11_just_send
    @@tools.each {|tool|
      fname = "testcasedata/pdwh/#{tool}.zip"
      assert zfile = Zip::File.open(fname), "error opening #{fname}"
      zfile.sort {|a, b| a.name <=> b.name}.each {|e|
        assert s = e.get_input_stream.read, "error reading entry #{e.name}"
        assert @@ti.mq.send_msg(s), "error sending msg from #{e.name}"
      }
      zfile.close
      $log.info "sent #{zfile.size} messages from #{fname.inspect}"
    }
  end

  def test21_integrated
    assert lot = @@pdwhtest.new_lot, 'error creating test lot'
    @@testlots << lot
    assert_equal 0, @@sv.lot_opelocate(lot, 1)
    assert @@pdwhtest.cleanup_eqp(@@eqp)
    @@ti.export(true)
    assert @@ti.claim_process_lot(lot, xfer: true)
  end

  def testdev91
    $events = {}
    @@tools.each {|tool|
      $events[tool] = []
      fname = "testcasedata/pdwh/#{tool}.zip"
      assert zfile = Zip::File.open(fname), "error opening #{fname}"
      zfile.sort {|a, b| a.name <=> b.name}.each {|e|
        assert s = e.get_input_stream.read, "error reading entry #{e.name}"
        s.split("\n").each {|line| $events[tool] << line.split('>')[1].split('<')[0] if line.include?('<EventName>')}
        ##assert @@mq.send_msg(s), "error sending msg from #{e.name}"
      }
      zfile.close
      $log.info "sent #{zfile.size} messages from #{fname.inspect}"
    }
  end

end
