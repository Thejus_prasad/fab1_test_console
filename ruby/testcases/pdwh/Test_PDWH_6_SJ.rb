=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: Thomas Klose and Steffen Steidten, 2016-07-06

History:
  2021-08-30 sfrieske, moved pdwhtest.rb and pdwhdb.rb to misc
=end

require 'pp'
require 'misc/pdwhtest'
require 'util/waitfor'
require 'RubyTestCase'


# test cases for PDWH 2.9
class Test_PDWH_6_SJ < RubyTestCase
  @@sorter = 'UTS001'

  def self.startup
    super
    @@pdwhtest = PDWH::Test.new($env, sv: @@sv)
    @@lots = {}
  end

  def self.after_all_passed
    $log.info "\n#{@@lots.pretty_inspect}"
  end

  def test6501_sj
    assert @@pdwhtest.cleanup_eqp(@@sorter, mode: 'Auto-2')
    assert lot = @@pdwhtest.new_lot(nwafers: 3)
    assert ec = @@pdwhtest.get_empty_carrier(carrier: 'PDWH%')
    assert_equal 0, @@sv.carrier_xfer_status_change(ec, '', 'UTSTO11', manualin: true)
    assert_equal 0, @@sv.carrier_xfer_status_change(@@sv.lot_info(lot).carrier, '', 'UTSTO11', manualin: true)
    assert sjid = @@sv.sj_create(@@sorter, 'P1', 'P2', lot, ec)
    assert wait_for(timeout: 900) {
      @@sv.sj_status(sjid).strSorterComponentJobListSequence.empty?
    }, 'error completing sj'
    @@lots['6501'] = lot
  end

  def test6502_sj2
    assert @@pdwhtest.cleanup_eqp(@@sorter, mode: 'Auto-2')
    assert lot = @@pdwhtest.new_lot(nwafers: 3)
    assert lcs = 2.times.collect {@@sv.lot_split(lot, 1)}
    assert carriers = @@pdwhtest.get_empty_carrier(n: 2, carrier: 'PDWH%')
    carriers.each {|c|
      assert_equal 0, @@sv.carrier_xfer_status_change(c, '', 'UTSTO11', manualin: true)
    }
    assert_equal 0, @@sv.carrier_xfer_status_change(@@sv.lot_info(lot).carrier, '', 'UTSTO11', manualin: true)
    assert sjid = @@sv.sj_create(@@sorter, 'P1', 'P2', lcs, carriers)
    # need to unload 1st target carrier and change port status from UnloadReq to LoadReq
    assert wait_for(timeout: 900) {
      if @@sv.sj_status(sjid).strSorterComponentJobListSequence.empty?
        true
      else
        p = @@sv.eqp_info(@@sorter).ports[1]
        if p.state == 'UnloadReq'
          @@sv.eqp_unload(@@sorter, 'P2', p.loaded_carrier)
          @@sv.eqp_port_status_change(@@sorter, 'P2', 'LoadReq')
        end
      end
    }
    @@lots['6502'] = @@sv.lot_family(lot)
  end

end
