# encoding: UTF-8

=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: Thomas Klose and Steffen Steidten, 2016-04-01

    https://www.google.com/url?q=http%3A%2F%2Fmyteamsgf%2Fsites%2FIEKPI%2FProduct%2FTest%2520documents%2FBusinessTestCases_Rel2.7.xlsx&sa=D&ust=1458733735233000&usg=AFQjCNEEY_pe1CBU237qkLduG2Z2pq21rg
    http://myteamsgf/sites/IEKPI/Product/Test%20documents/BusinessTestCases_Rel2.7.xlsx

History:
  2018-10-17 sfrieske, added test5034_rsv_autoproc_xj_cj
  2021-02-09 sfrieske, fixed use of stb_controllot
  2021-08-30 sfrieske, moved pdwhtest.rb and pdwhdb.rb to misc
=end

require 'pp'
require 'misc/pdwhtest'
require 'RubyTestCase'


# test cases for PDWH 2.5; April 2016
class Test_PDWH_5 < RubyTestCase

  def self.startup
    super
    @@pdwhtest = PDWH::Test.new($env, sv: @@sv)
    @@backup = SiView::MM.new('f8stag')
    @@lots = {}
  end

  def self.after_all_passed
    $log.info "\n#{@@lots.pretty_inspect}"
  end

  def test5005_tw_activities
    # L_A1
    $log.info("L_A1")
    lot = @@pdwhtest.new_lot
    li = @@sv.lot_info(lot)
    @@sv.carrier_xfer_status_change(li.carrier, "MO", "UTSTO11")
    @@sv.claim_process_lot(lot)
    @@lots["L_A1"] = lot

    # L_A2
    $log.info("L_A2")
    lot = @@pdwhtest.new_controllot('Equipment Monitor', product: "UT-MN-UTI004.01")
    @@sv.lot_opelocate(lot, +1)
    @@sv.lot_gatepass(lot)
    @@lots["L_A2"] = lot

    # L_A3
    $log.info("L_A3 - skipped") # OperComplete w/ LotType Vendor

    # L_A4
    $log.info("L_A4")
    lot = @@pdwhtest.new_controllot('Equipment Monitor', product: "UT-MN-UTI004.01")
    li = @@sv.lot_info(lot)
    @@sv.carrier_xfer_status_change(li.carrier, "MO", "UTSTO11")
    @@sv.claim_process_lot(lot)
    @@lots["L_A4"] = lot

    # L_A5
    $log.info("L_A5")
    product_procmonitor = 'UT-PR-UTC001.01'
    lot = @@pdwhtest.new_controllot('Process Monitor', product: product_procmonitor)
    @@sv.lot_opelocate(lot, "100.300")
    li = @@sv.lot_info(lot)
    @@sv.carrier_xfer_status_change(li.carrier, "MO", "UTSTO11")
    @@sv.claim_process_lot(lot, running_hold: true)
    @@sv.lot_hold_release(lot, nil)
    @@lots["L_A5"] = lot

    # L_A6
    $log.info("L_A6") # OperComplete w/ LotType Dummy
    lot = @@pdwhtest.new_controllot('Dummy')
    @@sv.schdl_change(lot, product: "1D-DN-RY-EPI.01", route: "MD-DN-RY-BANK-DISPO.01", opNo: "1100.100") # PD Type Other
    li = @@sv.lot_info(lot)
    @@sv.carrier_xfer_status_change(li.carrier, "MO", "UTSTO11")
    @@sv.claim_process_lot(lot)
    @@lots["L_A6"] = lot

    # L_A7
    $log.info("L_A7")
    product_procmonitor = 'UT-PR-UTC001.01'
    lot = @@pdwhtest.new_controllot('Process Monitor', product: product_procmonitor)
    li = @@sv.lot_info(lot)
    @@sv.carrier_xfer_status_change(li.carrier, "MO", "UTSTO11")
    @@sv.claim_process_lot(lot)
    @@sv.lot_opelocate(lot, -1)
    @@sv.claim_process_lot(lot)
    @@lots["L_A7"] = lot

    # L_A8
    $log.info("L_A8")
    product_procmonitor = 'UT-PR-UTC001.02'  # w/ rework attached
    rw_route = 'UTRW-REMEASURE.01'
    lot = @@pdwhtest.new_controllot('Process Monitor', product: product_procmonitor)
    @@sv.lot_opelocate(lot, "100.200")
    @@sv.lot_rework(lot, rw_route)
    li = @@sv.lot_info(lot)
    @@sv.carrier_xfer_status_change(li.carrier, "MO", "UTSTO11")
    @@sv.claim_process_lot(lot)
    @@sv.lot_opelocate(lot, -1)
    @@sv.claim_process_lot(lot)
    @@lots["L_A8"] = lot

    # L_A9
    $log.info("L_A9")
    product_procmonitor = 'UT-PR-UTC001.02'  # w/ rework attached
    rw_route = 'UTRW-REMEASURE.01'
    lot = @@pdwhtest.new_controllot('Process Monitor', product: product_procmonitor)
    @@sv.lot_opelocate(lot, "100.300")
    @@sv.lot_rework(lot, rw_route)
    @@sv.lot_opelocate(lot, +1)
    li = @@sv.lot_info(lot)
    @@sv.carrier_xfer_status_change(li.carrier, "MO", "UTSTO11")
    @@sv.claim_process_lot(lot, force: true, running_hold: true)
    @@sv.lot_hold_release(lot, nil)
    @@lots["L_A9"] = lot

    puts(@@lots.inspect)
  end

  def test5010_tw_starts
    # Lot_1
    lot = @@pdwhtest.new_lot(sublottype: "PR", nwafers: 1)
    @@lots["test5010_Lot1"] = lot

    # Lot_2
    lot = @@pdwhtest.new_controllot('Equipment Monitor', nwafers: 1)
    @@lots["test5010_Lot2"] = lot

    # Lot_3
    lot = @@pdwhtest.new_controllot('Equipment Monitor', nwafers: 1, sublottype: "VRT", ondemand: false)
    @@lots["test5010_Lot3"] = lot

    # Lot_4
    lot = @@pdwhtest.new_controllot("Process Monitor", nwafers: 1, ondemand: false)
    li = @@sv.lot_info(lot)
    @@sv.carrier_xfer_status_change(li.carrier, "MO", "UTSTO11")
    @@sv.claim_process_lot(lot)
    @@sv.claim_process_lot(lot)
    @@lots["test5010_Lot4"] = lot

    # Lot_5
    lot = @@pdwhtest.new_controllot('Equipment Monitor', nwafers: 2, ondemand: false)
    @@lots["test5010_Lot5"] = lot

    # Lot_6
    lot = @@pdwhtest.new_controllot('Equipment Monitor', nwafers: 2)
    @@sv.lot_opelocate(lot, "200.100")
    @@sv.lot_bank_move(lot, "UT-RAW")
    lot = @@sv.stb_controllot('Equipment Monitor', 'PDWH-MN-321.01', lot)
    @@lots["test5010_Lot6"] = lot

    puts(@@lots.inspect)
  end

  def xtest5015_tw_scrap
    # L_1
    lot = @@pdwhtest.new_lot(sublottype: "PR", nwafers: 5)
    @@sv.scrap_wafers(lot)
    @@lots["L_1"] = lot

    # L_2
    lot = @@pdwhtest.new_controllot('Equipment Monitor', nwafers: 5)
    @@sv.lot_opelocate(lot, "200.100")
    @@sv.lot_bank_move(lot, "UT-RAW")
    lot = @@sv.stb_controllot('Equipment Monitor', 'PDWH-MN-321.01', lot)
    @@sv.schdl_change(lot, sublottype: "SCRAP")
    @@sv.scrap_wafers(lot)
    @@lots["L_2"] = lot

    # L_3
    lot = @@pdwhtest.new_controllot('Equipment Monitor', nwafers: 5, ondemand: false)
    @@sv.scrap_wafers(lot)
    @@lots["L_3"] = lot

    # L_4
    lot = @@pdwhtest.new_controllot('Equipment Monitor', nwafers: 5)
    @@sv.lot_opelocate(lot, "200.100")
    @@sv.lot_bank_move(lot, "UT-RAW")
    lot = @@sv.stb_controllot('Equipment Monitor', 'PDWH-MN-321.01', lot)
    @@sv.scrap_wafers(lot)
    @@lots["L_4"] = lot

    # L_5
    lot = @@pdwhtest.new_controllot("Process Monitor", nwafers: 5)
    @@sv.scrap_wafers(lot)
    @@sv.scrap_wafers_cancel(lot)
    @@lots["L_5"] = lot

    puts(@@lots.inspect)
  end

  def test5020_nonfab
    backup_opNo = "1000.200"
    nprobank = "W-BACKUPOPER"
    nprobankspecial = "O-TKYWBANK"
    product = "UT-PRODUCT-FAB8.01"
    route = "UTRT101.01"
    customer = "qgt"
    nprobank_return = "O-BACKUPOPER"

    # L_1, current_loc_flag=1, xfer_flag=1, normal nprobank
    lot = @@pdwhtest.new_lot(sublottype: "PR", nwafers: 5, product: product, route: route, customer: customer)
    @@sv.lot_opelocate(lot, backup_opNo)
    @@sv.lot_nonprobankin(lot, nprobank)
    @@sv.wafer_sort(lot, "")
    @@sv.backupop_lot_send(lot, @@backup)
    @@lots["5020_L_1"] = lot

    # L_2, current_loc_flag=0, xfer_flag=1, normal nprobank
    lot = @@pdwhtest.new_lot(sublottype: "PR", nwafers: 5, product: product, route: route, customer: customer)
    @@sv.lot_opelocate(lot, backup_opNo)
    @@sv.lot_nonprobankin(lot, nprobank)
    @@sv.wafer_sort(lot, "")
    @@sv.backupop_lot_send(lot, @@backup)
    @@backup.backupop_lot_send_receive(lot, @@sv)
    @@backup.claim_process_lot(lot, mode: 'Auto-1')
    @@backup.lot_nonprobankin(lot, nprobank_return)
    @@backup.wafer_sort(lot, "")
    @@backup.backupop_lot_return(lot, @@sv)
    @@lots["5020_L_2"] = lot

    # L_3, current_loc_flag=1, xfer_flag=1, nprobankspecial
    lot = @@pdwhtest.new_lot(sublottype: "PR", nwafers: 5, product: product, route: route, customer: customer)
    @@sv.lot_opelocate(lot, backup_opNo)
    @@sv.lot_nonprobankin(lot, nprobankspecial)
    @@sv.wafer_sort(lot, "")
    @@sv.backupop_lot_send(lot, @@backup)
    @@lots["5020_L_3"] = lot

    # L_4, current_loc_flag=1, xfer_flag=0, nprobankspecial
    lot = @@pdwhtest.new_lot(sublottype: "PR", nwafers: 5, product: product, route: route, customer: customer)
    @@sv.lot_opelocate(lot, backup_opNo)
    @@sv.lot_nonprobankin(lot, nprobankspecial)
    @@lots["5020_L_4"] = lot

    # L_5, current_loc_flag=1, xfer_flag=0, nprobankspecial, lot onhold
    lot = @@pdwhtest.new_lot(sublottype: "PR", nwafers: 5, product: product, route: route, customer: customer)
    @@sv.lot_opelocate(lot, backup_opNo)
    @@sv.lot_nonprobankin(lot, nprobankspecial)
    @@sv.lot_hold(lot, "C-PENG")
    @@lots["5020_L_5"] = lot

    # L_6, current_loc_flag=1, xfer_flag=1, Equipment Moitor
    lot = @@pdwhtest.new_lot(sublottype: "PR", nwafers: 5, product: product, route: route, customer: customer)
    @@sv.sublottype_change(lot, 'Equipment Monitor')
    @@sv.lot_opelocate(lot, backup_opNo)
    @@sv.lot_nonprobankin(lot, nprobank)
    @@sv.wafer_sort(lot, "")
    @@sv.backupop_lot_send(lot, @@backup)

  end

  def xtest5025_fab_non_asset
    #L_1  Production  slt: PR
    lot = @@pdwhtest.new_lot(sublottype: "PR", nwafers: 3)
    @@lots["5025_L_1"] = lot

    #L_2  Equipment Monitor  slt:VRT
    lot = @@pdwhtest.new_controllot('Equipment Monitor', nwafers: 3, sublottype: "VRT")
    @@lots["5025_L_2"] = lot

    #L_3  Equipment Monitor slt: Equipment Monitor
    lot = @@pdwhtest.new_controllot('Equipment Monitor', nwafers: 3)
    @@lots["5025_L_3"] = lot

    #L_4  Process Monitor slt: Process Monitor, bankid: T-NPI, multiple Holds
    lot = @@pdwhtest.new_controllot("Process Monitor", nwafers: 3)
    @@sv.lot_nonprobankin(lot, "T-NPI")
    "C-PENG D-PENG E-PENG F-PENG L-PENG".split.each{|hc| @@sv.lot_hold(lot, hc)}
    @@lots["5025_L_4"] = lot

    #L_5  Dummy, OnHold
    lot = @@pdwhtest.new_controllot("Dummy", nwafers: 3)
    @@sv.lot_hold(lot, "C-PENG")
    @@lots["5025_L_5"] = lot
    #L_6  Production slt:PR, LOTSTART PD
    lot = @@pdwhtest.new_lot(sublottype: "PR", nwafers: 3, route: "P-PDWH-MAINPD.01", product: "PDWHPROD5.01")
    @@lots["5025_L_6"] = lot

    #L_7  Vendor OnHold  !!

    #L_8  Production slt: SCRAP
    lot = @@pdwhtest.new_lot(sublottype: "SCRAP", nwafers: 3)
    @@lots["5025_L_8"] = lot

    #L_9  Production slt: SCRAP, onhold, LotStartPD
    lot = @@pdwhtest.new_lot(sublottype: "SCRAP", nwafers: 3, route: "P-PDWH-MAINPD.01", product: "PDWHPROD5.01")
    @@sv.lot_hold(lot, "C-PENG")
    @@lots["5025_L_9"] = lot

    #L_10 Production  slt:PR
    backup_opNo = "1000.200"
    nprobank = "W-BACKUPOPER"
    product = "UT-PRODUCT-FAB8.01"
    route = "UTRT101.01"
    customer = "qgt"
    nprobank_return = "O-BACKUPOPER"
    lot = @@pdwhtest.new_lot(sublottype: "PR", nwafers: 3, product: product, route: route, customer: customer)
    @@sv.lot_opelocate(lot, backup_opNo)
    @@sv.lot_nonprobankin(lot, nprobank)
    @@sv.wafer_sort(lot, "")
    @@sv.backupop_lot_send(lot, @@backup)
    @@backup.backupop_lot_send_receive(lot, @@sv)
    @@lots["5025_L_10"] = lot

    #L_11  Production  PR, MainPD: BY-LOCASS-ASSEMBLY.01
    lot = @@pdwhtest.new_lot(sublottype: "PR", nwafers: 3, route: "P-PDWH6.01", product: "PDWHPROD6.01")
    @@sv.lot_opelocate(lot, "6500.1000") #LOTSHIPDESIG.01
    @@sv.lot_branch(lot, "BY-LOCASS-ASSEMBLY.01")
    @@lots["5025_L_11"] = lot

    puts(@@lots.inspect)
  end

  def test5031_cjs
    # special test for cj timestamps, v2.7.5
    assert lot = @@pdwhtest.new_lot
    @@lots['5031'] = lot
    assert_equal 0, @@sv.lot_opelocate(lot, nil, op: 'P-PDWH-PD02.01')
    li = @@sv.lot_info(lot)
    opNo, carrier, eqp = li.opNo, li.carrier, 'PDWHA02'
    assert @@pdwhtest.cleanup_eqp(eqp, mode: 'Off-Line-1')
    cjs = []
    100.times {|n|
      $log.info "\n-- loop #{n}"
      assert_equal 0, @@sv.lot_cleanup(lot, opNo: opNo)
      # cj for 8 min
      assert cj = @@sv.slr(eqp, lot: lot, carrier: carrier)
      cjs << cj
      $log.info "waiting 8min before canceling the cj"; sleep 480
      assert_equal 0, @@sv.slr_cancel(eqp, cj)
      sleep 60
      # cj for 5 min
      assert cj = @@sv.slr(eqp, lot: lot, carrier: carrier)
      cjs << cj
      $log.info "waiting 5min before canceling the cj"; sleep 300
      assert_equal 0, @@sv.slr_cancel(eqp, cj)
      sleep 60
      # cj for 1min with opecomp
      assert cj = @@sv.claim_process_lot(lot, eqp: eqp, proctime: 300)
      cjs << cj
    }
    $log.info "cjs:\n#{cjs.pretty_inspect}"
    @@sv.delete_lot_family(lot)
  end

  def test5032_xjs_empty
    # special test for xj timestamps, v2.7.5
    assert c = @@pdwhtest.get_empty_carrier
    eqp = 'UTF002'
    100.times {|n|
      $log.info "\n-- loop #{n}"
      assert @@pdwhtest.cleanup_carrier(c)
      assert_equal 0, @@sv.carrier_xfer(c, '', '', eqp, 'P1')
      assert @@sv.wait_carrier_xfer(c)
    }
  end

  def test5033_xj_cj
    # special test for cj timestamps, v2.7.5
    assert lot = @@pdwhtest.new_lot
    @@lots['5033'] = lot
    assert_equal 0, @@sv.lot_opelocate(lot, nil, op: 'P-PDWH-PD01.01')
    li = @@sv.lot_info(lot)
    opNo, carrier, eqp = li.opNo, li.carrier, 'PDWHA01'
    assert @@pdwhtest.cleanup_eqp(eqp, mode: 'Off-Line-1')
    port = 'P1'
    cjs = []
    10.times {|n|
      $log.info "\n-- loop #{n}"
      assert_equal 0, @@sv.lot_cleanup(lot, opNo: opNo, stocker: @@pdwhtest.stocker)
      assert cj = @@sv.slr(eqp, port: port, lot: lot, carrier: carrier)
      cjs << cj
      assert_equal 0, @@sv.carrier_xfer(carrier, '', '', eqp, port)
      assert @@sv.wait_carrier_xfer(carrier)
      assert cj = @@sv.claim_process_lot(lot, eqp: eqp, port: port, proctime: 480) # 8min
    }
    $log.info "cjs:\n#{cjs.pretty_inspect}"
    @@sv.delete_lot_family(lot)
  end

end
