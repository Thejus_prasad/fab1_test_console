=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: Thomas Klose and Steffen Frieske, 2016-07-06

History:
  2021-08-30 sfrieske, moved pdwhtest.rb and pdwhdb.rb to misc
=end

require 'misc/pdwhtest'
require 'RubyTestCase'


class Test_PDWH_8 < RubyTestCase
  @@route = 'UTRT-GATEPASS.02'
  @@opNo1 = '1000.2000'
  @@opNo2 = '1000.4000'
  @@opNo3 = '1000.6000'
  @@opNo4 = '1000.7000'
  #
  @@eqp_poc = 'UTF002'

  @@dbenv = 'stress'  # pass 'stress' for tests with the STRESS (LET) instance, else nil or "itdc"


  def self.startup
    super
    @@pdwhtest = PDWH::Test.new($env, dbenv: @@dbenv, sv: @@sv)
    @@testlots = []
    @@lots = {}
  end

  def self.delete_testlots  # for manual cleanup
    @@testlots.each {|lot| @@sv.delete_lot_family(lot)}
  end

  def self.after_all_passed
    $log.info 'Results:'
    @@lots.each_pair {|tc, res| puts "#{tc}: #{res}"}
  end


  def test8011_gatepass_pre1
    assert lot = @@pdwhtest.new_lot, 'error creating test lot'
    @@testlots << lot
    @@sv.schdl_change(lot, route: @@route)
    assert_equal @@route, @@sv.lot_info(lot).route, 'wrong lot route'
    assert_equal 0, @@sv.user_parameter_change('Lot', lot, 'UTSkip', 'YES')
    #
    # process at PD1, opecomp does a pre1 gatepass
    assert_equal 0, @@sv.lot_opelocate(lot, @@opNo1)
    assert @@sv.claim_process_lot(lot), 'error processing lot'
    assert_equal @@opNo2, @@sv.lot_info(lot).opNo, 'missing pre1 gatepass?'
    #
    # process at PD3 (same as above at over next PD)
    sleep 10  # for event separation
    assert @@sv.claim_process_lot(lot), 'error processing lot'
    assert_equal @@opNo3, @@sv.lot_info(lot).opNo, 'missing pre1 gatepass?'
    #
    # process at PD5 w/o pre1
    sleep 10  # for event separation
    assert @@sv.claim_process_lot(lot), 'error processing lot'
    assert_equal @@opNo4, @@sv.lot_info(lot).opNo, 'wrong pre1 gatepass?'
    #
    @@lots['test8011'] = lot
  end

  def test8111_partial_opecomp
    assert_equal '1', @@sv.environment_variable[1]['SP_PJCTRL_FUNC_ENABLED'], 'PJCTRL not enabled in MM env'
    assert lot = @@pdwhtest.new_lot, 'error creating test lot'
    @@testlots << lot
    assert_equal 0, @@sv.lot_opelocate(lot, 1)
    assert eqp = @@sv.lot_info(lot).opEqps.first
    assert @@sv.eqp_info(eqp).pjctrl, "PJ Level Control not enabled for #{eqp}"
    assert cj = @@sv.claim_process_lot(lot, eqp: eqp, partial_comp: true, opecancel: ['01', '02'], opecancel_hold: ['03', '04'], opecomp_hold: ['05', '06'])
    @@lots['test8111'] = cj
  end

  def test8211_rsv_autoproc_cj
    assert lot = @@pdwhtest.new_lot, 'error creating test lot'
    @@testlots << lot
    @@lots['test8211'] = lot
    eqp = 'PDWHA01'
    assert_equal 0, @@sv.lot_opelocate(lot, nil, op: 'P-PDWH-PD01.01')
    assert_equal 0, @@sv.eqp_cleanup(eqp, mode: 'Auto-1')
    # rsv as X-AUTOPROC
    assert @@svautoproc = SiView::MM.new($env, user: 'X-AUTOPROC', password: 'fwsftfS3qvpg')
    assert cj = @@svautoproc.slr(eqp, lot: lot)
    # wait for batch run
    # assert @@pdwhtest.wait_next_batch_run('SIVIEW_TRA'), 'loader timeout'
    sleep 60  # TODO: revert (Elke special)
    # OpeStart (process)
    assert @@sv.claim_process_lot(lot, eqp: eqp, mode: 'Auto-1', slr: false, timeout: 60)  # TODO: revert (Elke special)
    sleep 60
    @@sv.delete_lot_family(lot)
    @@lots['test8211'] = lot
  end

end
