=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: Thomas Klose and Steffen Frieske, 2018-02-25

History:
  2021-02-05 sfrieske, use instance_of instead of kind_of
=end

require_relative 'PDWHR'


# PDWH Regression tests for SiView data basics: DIM (lot splits).
#   see see https://docs.google.com/spreadsheets/d/14aJZnPwjrdO03jP90m_X3CgkXc4lnKk3VE_F2hktpU8/edit#gid=0
class PDWHR211 < PDWHR

  def self.startup
    super
    @@lots = {}  # for reporting
  end

  def self.after_all_passed
    super
    $log.info 'Results:'
    @@lots.each_pair {|tc, res| puts "#{tc}: #{res}"}
  end


  def test21110_split_merge_complex
    @@timestamps = {stb: Time.now}
    assert lot = @@pdwhtest.new_lot(nwafers: 10), 'error creating lot'
    @@testlots << lot
    @@wafers = @@sv.lot_info(lot, wafers: true).wafers
    @@timestamps[:stbafter] = Time.now
    # 1st run
    cl1 = @@sv.lot_split(lot, [6, 7])  # split wafers 6 and 7
    @@timestamps[:split1] = @@sv.lot_info(cl1).claim_time
    cl2 = @@sv.lot_split(cl1, [7])     # split wafer 7
    @@timestamps[:split2] = @@sv.lot_info(cl2).claim_time
    cl3 = @@sv.lot_split(lot, [9, 10]) # split wafers 9 and 10 from parent
    @@timestamps[:split3] = @@sv.lot_info(cl3).claim_time
    assert @@pdwhtest.wait_next_batch_run('SIVIEW_DIM'), 'loader timeout'
    # 2nd run
    cl4 = @@sv.lot_split(cl3, [10])    # split wafer 10 from cl3
    @@timestamps[:split4] = @@sv.lot_info(cl4).claim_time
    assert @@sv.lot_merge(cl2, cl4)
    @@timestamps[:merge1] = @@sv.lot_info(cl2).claim_time
    assert @@pdwhtest.wait_next_batch_run('SIVIEW_DIM'), 'loader timeout'
    # 3rd run
    assert @@sv.claim_process_lot([lot, cl1, cl2, cl3])   # process all lots
    cl5 = @@sv.lot_split(cl2, [7])     # split wafer 7 again
    @@timestamps[:split5] = @@sv.lot_info(cl5).claim_time
    assert @@sv.lot_merge(cl1, cl5)    #!
    @@timestamps[:merge2] = @@sv.lot_info(cl1).claim_time
    cl6 = @@sv.lot_split(lot, [4,5,8]) # split 4, 5, 8 from parent
    @@timestamps[:split6] = @@sv.lot_info(cl6).claim_time
    cl7 = @@sv.lot_split(lot, [2,3])   # split 2 and 3 from parent
    @@timestamps[:split7] = @@sv.lot_info(cl7).claim_time
    assert @@sv.lot_merge(cl6, cl7)
    @@timestamps[:merge3] = @@sv.lot_info(cl6).claim_time
    assert @@pdwhtest.wait_next_batch_run('SIVIEW_DIM'), 'loader timeout'
    # 4th run
    assert @@sv.claim_process_lot([lot, cl1, cl2, cl3, cl6])   # process all lots
    @@timestamps[:merge4pre] = Time.now
    assert @@sv.merge_lot_family(lot, nosort: true)   # merge them all
    # assert @@sv.lot_merge(lot, nil)    # merge them all
    @@timestamps[:merge4] = @@sv.lot_info(lot).claim_time
    sleep 10
    # scrap
    assert @@sv.delete_lot_family(lot)
    # valid_to for scrap is not the scrap timestamp but the timestamp of lot deletion by lot maintenance
    # but we still need to wait for the batch run!
    @@timestamps[:scrap] = @@sv.lot_info(lot).claim_time
    assert @@pdwhtest.wait_next_batch_run('SIVIEW_DIM'), 'loader timeout'
    #
    @@lots['test21110'] = [lot, cl1, cl2, cl3, cl4, cl5, cl6, cl7]
    #
    # verify lot entries
    verify_lot_wafer(lot, :stb, :split1, 10)
    verify_lot_wafer(lot, :split1, :split3, 8)
    verify_lot_wafer(cl1, :split1, :split2, 2)
    verify_lot_wafer(cl2, :split2, :merge1, 1)
    verify_lot_wafer(cl1, :split2, :merge2, 1)
    verify_lot_wafer(cl3, :split3, :split4, 2)
    verify_lot_wafer(lot, :split3, :split6, 6)
    verify_lot_wafer(cl4, :split4, :merge1, 1)
    verify_lot_wafer(cl3, :split4, :merge4, 1)
    verify_lot_wafer(cl2, :merge1, :split5, 2)
    verify_lot_wafer(cl5, :split5, :merge2, 1)
    verify_lot_wafer(cl2, :split5, :merge4, 1)
    verify_lot_wafer(cl1, :merge2, :merge4, 2)
    verify_lot_wafer(cl6, :split6, :merge3, 3)
    verify_lot_wafer(lot, :split6, :split7, 3)
    verify_lot_wafer(cl7, :split7, :merge3, 2)
    verify_lot_wafer(lot, :split7, :merge4, 1)
    verify_lot_wafer(cl6, :merge3, :merge4, 5)
    verify_lot_wafer(lot, :merge4, nil, 10)   # valid_to set at lot maintenance, don't compare
    #
    # verify wafer entries
    verify_wafer_lot(1, lot, :stb, nil)       # valid_to set at lot maintenance, don't compare
    verify_wafer_lot(2, lot, :stb, :split7)
    verify_wafer_lot(2, cl7, :split7, :merge3)
    verify_wafer_lot(2, cl6, :merge3, :merge4)
    verify_wafer_lot(2, lot, :merge4pre, nil) # valid_to set at lot maintenance, don't compare
    verify_wafer_lot(3, lot, :stb, :split7)
    verify_wafer_lot(3, cl7, :split7, :merge3)
    verify_wafer_lot(3, cl6, :merge3, :merge4)
    verify_wafer_lot(3, lot, :merge4pre, nil) # valid_to set at lot maintenance, don't compare
    verify_wafer_lot(4, lot, :stb, :split6)
    verify_wafer_lot(4, cl6, :split6, :merge4)
    verify_wafer_lot(4, lot, :merge4pre, nil) # valid_to set at lot maintenance, don't compare
    verify_wafer_lot(5, lot, :stb, :split6)
    verify_wafer_lot(5, cl6, :split6, :merge4)
    verify_wafer_lot(5, lot, :merge4pre, nil) # valid_to set at lot maintenance, don't compare
    verify_wafer_lot(6, lot, :stb, :split1)
    verify_wafer_lot(6, cl1, :split1, :merge4)
    verify_wafer_lot(6, lot, :merge4, nil)   # valid_to set at lot maintenance, don't compare
    verify_wafer_lot(7, lot, :stb, :split1)
    verify_wafer_lot(7, cl1, :split1, :split2)
    verify_wafer_lot(7, cl2, :split2, :split5)
    verify_wafer_lot(7, cl5, :split5, :merge2)
    verify_wafer_lot(7, cl1, :merge2, :merge4)
    verify_wafer_lot(7, lot, :merge4, nil)   # valid_to set at lot maintenance, don't compare
    verify_wafer_lot(8, lot, :stb, :split6)
    verify_wafer_lot(8, cl6, :split6, :merge4)
    verify_wafer_lot(8, lot, :merge4pre, nil) # valid_to set at lot maintenance, don't compare
    verify_wafer_lot(9, lot, :stb, :split3)
    verify_wafer_lot(9, cl3, :split3, :merge4)
    verify_wafer_lot(9, lot, :merge4pre, nil) # valid_to set at lot maintenance, don't compare
    verify_wafer_lot(10, lot, :stb, :split3)
    verify_wafer_lot(10, cl3, :split3, :split4)
    verify_wafer_lot(10, cl4, :split4, :merge1)
    verify_wafer_lot(10, cl2, :merge1, :merge4)
    verify_wafer_lot(10, lot, :merge4, nil)   # valid_to set at lot maintenance, don't compare
  end

  # aux methods

  def verify_lot_wafer(lot, symfrom, symto, nwafers)
    $log.info "verify_lot_wafer #{lot.inspect}, #{symfrom.inspect}, #{symto.inspect}, #{nwafers}"
    tfrom = @@timestamps[symfrom]
    tfrom = @@sv.siview_time(tfrom) if tfrom.instance_of?(String)
    tto = @@timestamps[symto]
    tto = @@sv.siview_time(tto) if tto.instance_of?(String)
    assert rr = @@lldb.lot_wafer(lot, nwafers: nwafers, valid_from: tfrom, valid_to: tto), 'no DB entry'
    $rr = rr  # for dev
    assert_equal 1, rr.size, 'multiple DB entries'
    assert_equal nwafers, rr.first.nwafers, 'wrong WAFER_COUNT'
  end

  def verify_wafer_lot(slot, lot, symfrom, symto)
    $log.info "verify_wafer_lot #{slot}, #{symfrom.inspect}, #{symto.inspect}"
    tfrom = @@timestamps[symfrom]
    tfrom = @@sv.siview_time(tfrom) if tfrom.instance_of?(String)
    tto = @@timestamps[symto]
    tto = @@sv.siview_time(tto) if tto.instance_of?(String)
    wafer = @@wafers.find {|w| w.slot == slot}.wafer
    assert rr = @@lldb.wafer_lot(wafer, lot: lot, valid_from: tfrom, valid_to: tto), 'no DB entry'
    $rr = rr  # for dev
    assert_equal 1, rr.size, 'multiple DB entries'
  end

end
