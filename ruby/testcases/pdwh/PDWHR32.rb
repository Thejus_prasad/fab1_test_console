=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: Thomas Klose and Steffen Frieske, 2015-11-25

History:
  2021-09-15 sfrieske, finally replaced $svtest by @@svtest, $sv by @@sv and $sm by @@sm
=end

require_relative 'PDWHR'


# PDWH Regression tests. Misc lots, kept for now (for manual tests).
class PDWHR32 < PDWHR


  def test32010_lot_split_mrb
    assert lot = @@pdwhtest.new_lot
    ##@@testlots << lot
    # split off wafers 9..25
    assert lc = @@sv.lot_split(lot, (9..25).to_a)
  end

end
