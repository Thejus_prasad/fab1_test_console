=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: Thomas Klose and Steffen Frieske, 2015-11-25

History:
  2021-09-15 sfrieske, finally replaced $svtest by @@svtest, $sv by @@sv and $sm by @@sm
=end

require_relative 'PDWHR'


# PDWH Regression tests for historization of sorter jobs
class PDWHR113 < PDWHR

  def self.shutdown
    super
    @@pdwhtest.cleanup_eqp(@@pdwhtest.sorter)
  end


  def test11300_setup
    assert @@lot = @@pdwhtest.new_lot, 'error creating test lot'
    @@testlots << @@lot
  end

  def test11310_wildcard_wait_error  # currently fails, PDWH2.7 TC3.5, 3.6
    assert @@pdwhtest.cleanup_eqp(@@pdwhtest.sorter, mode: 'Auto-2')  # Auto2: watchdog will not switch to executing
    # create wildcard sorter job, that will stay in 'Waiting To Executing'
    assert ec = @@pdwhtest.get_empty_carrier
    assert_equal 0, @@sv.carrier_xfer_status_change(ec, '', @@pdwhtest.stocker, manualin: true)
    tstart = Time.now
    assert sj = @@sv.sj_create('*', '*', '*', @@lot, ec)
    assert @@pdwhtest.wait_loader {@@lldb.sj_hist(sjid: sj.identifier).last}, 'loader timeout'
    ee = @@lldb.sj_hist(sjid: sj.identifier)
    assert_equal 1, ee.size, "wrong number of entries for sj #{sj.identifier}: #{ee}"
    e = ee.first
    assert_equal 'Wait To Executing', e.state
    refute_nil e.sj_created
    assert e.sj_created - tstart < 20
    assert_nil e.sj_started
    assert_nil e.sj_finished
    assert_nil e.sj_deleted
    assert_nil e.sj_error
    # change state from Wait.. to Error
    tstart = Time.now
    assert_equal 0, @@sv.sj_status_change(@@pdwhtest.sorter, sj, 'Error')
    # wait for loader and verify timestamps
    assert @@pdwhtest.wait_loader {@@lldb.sj_hist(sjid: sj).last.state == 'Error'}, 'loader timeout'
    ee = @@lldb.sj_hist(sjid: sj)
    assert_equal 1, ee.size, "wrong number of entries for sj #{sj}: #{ee}"
    e = ee.first
    refute_nil e.sj_created
    refute_nil e.sj_started
    assert_nil e.sj_finished
    refute_nil e.sj_deleted
    refute_nil e.sj_error
  end

  def test11320_completed  # PDWH2.7 TC3.4
    assert @@pdwhtest.cleanup_eqp(@@pdwhtest.sorter, mode: 'Auto-2')
    # create and execute SJ
    assert carriers = @@pdwhtest.get_empty_carrier(n: 2)
    carriers.each {|c|
      assert_equal 0, @@sv.carrier_xfer_status_change(c, '', @@pdwhtest.stocker, manualin: true)
    }
    assert lc = @@sv.lot_split(@@lot, 2)
    tstart = Time.now
    assert sj = @@sv.sj_create(@@pdwhtest.sorter, 'P1', 'P2', [@@lot, lc], carriers)
    assert @@sv.claim_sj_execute(sjid: sj, report: true)
    # wait for loader
    assert @@pdwhtest.wait_loader {
      e = @@lldb.sj_hist(sjid: sj.identifier).last
      e && e.state == 'Completed'
    }, 'loader timeout'
    # verify timestamps
    ee = @@lldb.sj_hist(sjid: sj.identifier)
    assert_equal 1, ee.size, "wrong number of entries for sj #{sj.identifier}: #{ee}"
    e = ee.first
    refute_nil e.sj_created
    refute_nil e.sj_started
    refute_nil e.sj_finished
    refute_nil e.sj_deleted
    assert_nil e.sj_error
  end

  def test11330_executing_error  # PDWH2.7 TC3.7
    assert @@pdwhtest.cleanup_eqp(@@pdwhtest.sorter, mode: 'Auto-1')  # Auto1: watchdog will switch to Executing
    # create SJ
    assert ec = @@pdwhtest.get_empty_carrier
    assert_equal 0, @@sv.carrier_xfer_status_change(ec, '', @@pdwhtest.stocker, manualin: true)
    tstart = Time.now
    assert sj = @@sv.sj_create(@@pdwhtest.sorter, 'P1', 'P2', @@lot, ec)
    sleep 1
    component = @@sv.sj_status(sj.sorterJobID).strSorterComponentJobListSequence.first
    assert_equal 'Executing', component.sorterComponentJobStatus, 'wrong SJ status'
    # assert_equal 'Executing', @@sv.sj_list(eqp: @@pdwhtest.sorter).first.sorterComponentJobStatus
    # wait for loader and verify timestamps
    assert @@pdwhtest.wait_loader {@@lldb.sj_hist(sjid: sj.identifier).last}, 'loader timeout'
    ee = @@lldb.sj_hist(sjid: sj.identifier)
    assert_equal 1, ee.size, "wrong number of entries for sj #{sj.identifier}: #{ee}"
    e = ee.first
    refute_nil e.sj_created
    refute_nil e.sj_started
    assert_nil e.sj_finished
    assert_nil e.sj_deleted
    assert_nil e.sj_error
    # change state from Executing to Error
    tstart = Time.now
    assert_equal 0, @@sv.sj_status_change(@@pdwhtest.sorter, sj, 'Error')
    # wait for loader and verify timestamps
    assert @@pdwhtest.wait_loader {@@lldb.sj_hist(sjid: sj.identifier).last.state == 'Error'}, 'loader timeout'
    ee = @@lldb.sj_hist(sjid: sj.identifier)
    assert_equal 1, ee.size, "wrong number of entries for sj #{sj.identifier}: #{ee}"
    e = ee.first
    refute_nil e.sj_created
    refute_nil e.sj_started
    assert_nil e.sj_finished
    refute_nil e.sj_deleted
    refute_nil e.sj_error
  end
end
