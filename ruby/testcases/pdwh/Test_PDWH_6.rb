# encoding: UTF-8

=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: Thomas Klose and Steffen Steidten, 2016-07-06

History:
  2017-11-07 sfrieske, relaxec carrier search for test6005 C4 carriers
  2018-11-20 sfrieske, fixes for test6075
  2019-11-13 sfrieske, remove assert for lot_bankin to honour autobankin
  2021-08-30 sfrieske, moved pdwhtest.rb and pdwhdb.rb to misc
  2022-01-04 sfrieske, set description at copy, removed @@sm.change_description
=end

require 'pp'
require 'misc/pdwhtest'
require 'RubyTestCase'


# test cases for PDWH 2.8; July 2016
class Test_PDWH_6 < RubyTestCase

  @@src_mfg_bump = {route: 'UTRT-BUMP01.01', product: 'UT-PRODUCT-BUMP.01', carrier: '%'}
  @@src_mfg_sort = {route: 'UTRT-SORT01.01', product: 'UT-PRODUCT-SORT.01', carrier: '%'}
  @@nonprobank = 'UT-NONPROD'


  def self.startup
    super
    @@pdwhtest = PDWH::Test.new($env, sv: @@sv, carriers: '%')
    @@sm = @@pdwhtest.sm
    @@lots = {}
  end

  def self.after_all_passed
    $log.info "\n#{@@lots.pretty_inspect}"
  end

  def self.manual_cleanup
    @@lots.each {|lot| @@sv.delete_lot_family(lot)}
  end


  def test6005_NewStarts_SubKPI_Assignment
    lot_label = Time.now.to_i.hash.to_s

    ##assert lot = @@pdwhtest.new_lot({carrier_category: 'BEOL', lotlabel: lot_label}.merge(@@src_mfg_bump))
    assert lot = @@pdwhtest.new_lot({carrier_category: 'BEOL'}.merge(@@src_mfg_bump))
    assert_equal 0, @@sv.user_data_update(:lot, lot, {'LOT_LABEL'=>lot_label})
    assert_equal 0, @@sv.schdl_change(lot, route: "C02-1597-BUMP.02", opNo: "1000.1000")
    li = @@sv.lot_info(lot)
    assert_equal 0, @@sv.carrier_xfer_status_change(li.carrier, "MO", "UTSTO11")
    assert @@sv.claim_process_lot(lot)
    @@lots["test6005_Lot1"] = lot

    assert lot = @@pdwhtest.new_lot({carrier_category: 'BEOL'}.merge(@@src_mfg_sort))
    assert_equal 0, @@sv.user_data_update(:lot, lot, {'LOT_LABEL'=>lot_label})
    assert_equal 0, @@sv.schdl_change(lot, route: "C02-1597-SORT.01", opNo: "1000.1000")
    assert_equal 0, @@sv.lot_gatepass(lot)
    @@lots["test6005_Lot3"] = lot

    assert lot = @@pdwhtest.new_lot(carrier_category: "C4", route: "C02-1512.01", product: "SHELBY31AF.B1")
    assert_equal 0, @@sv.carrier_xfer_status_change(@@sv.lot_info(lot).carrier, "MO", "UTSTO11")
    assert_equal 0, @@sv.lot_opelocate(lot, "98100.1200") # LV-CT.01
    assert @@sv.claim_process_lot(lot, eqp: "TRK902")
    assert_equal 0, @@sv.schdl_change(lot, route: "C02-1508.01", opNo: "98100.1900") # DV-CT.01
    assert @@sv.claim_process_lot(lot)
    @@lots["test6005_Lot4"] = lot

    assert lot = @@pdwhtest.new_lot(carrier_category: "C4", route: "P-PDWH-MAINPD.09", product: "PDWHPROD1.09")
    assert_equal 0, @@sv.user_data_update(:lot, lot, {'LOT_LABEL'=>lot_label})
    assert_equal 0, @@sv.carrier_xfer_status_change(@@sv.lot_info(lot).carrier, "MO", "UTSTO11")
    assert_equal 0, @@sv.lot_opelocate(lot, "9800.3000")
    assert @@sv.claim_process_lot(lot)
    assert_equal 0, @@sv.schdl_change(lot, route: "P-PDWH-MAINPD.10", opNo: "9800.3000")
    assert @@sv.claim_process_lot(lot)
    @@lots["test6005_Lot5"] = lot

    assert lot = @@pdwhtest.new_lot(carrier: 'PDWH%', carrier_category: "FOI", route: "P-PDWH6.01", product: "PDWHPROD6.01")
    assert_equal 0, @@sv.user_data_update(:lot, lot, {'LOT_LABEL'=>lot_label})
    assert_equal 0, @@sv.carrier_xfer_status_change(@@sv.lot_info(lot).carrier, "MO", "UTSTO11")
    assert_equal 0, @@sv.lot_opelocate(lot, "6350.2000")
    assert @@sv.claim_process_lot(lot)
    assert_equal 0, @@sv.schdl_change(lot, route: "C02-TKEY-RIWBA.01", opNo: "6250.3000")
    assert @@sv.claim_process_lot(lot)
    @@lots["test6005_Lot6"] = lot
  end

  def test6015_NewOuts_SubKPI_Assignment
    lot_label = Time.now.to_i.hash.to_s

    assert lot = @@pdwhtest.new_lot(route: "P-PDWH-MAINPD.11", product: "PDWHPROD11.01", sub_lottype: "PR")
    assert_equal 0, @@sv.user_data_update(:lot, lot, {'LOT_LABEL'=>lot_label})
    assert_equal 0, @@sv.carrier_xfer_status_change(@@sv.lot_info(lot).carrier, "MO", "UTSTO11")
    assert_equal 0, @@sv.lot_opelocate(lot, "9800.2000")
    assert @@sv.claim_process_lot(lot)
    assert_equal 0, @@sv.lot_opelocate(lot, -1)
    assert @@sv.claim_process_lot(lot)
    @@lots["test6015_Lot1"] = lot

    assert lot = @@pdwhtest.new_lot(carrier_category: "C4", route: "C02-1508.01", product: "SHELBY3LOL.A0")
    assert_equal 0, @@sv.user_data_update(:lot, lot, {'LOT_LABEL'=>lot_label})
    assert_equal 0,  @@sv.carrier_xfer_status_change(@@sv.lot_info(lot).carrier, "MO", "UTSTO11")
    assert_equal 0, @@sv.lot_opelocate(lot, "98100.2500")
    assert_equal 0, @@sv.lot_gatepass(lot)
    assert_equal 0, @@sv.schdl_change(lot, route: "C02-1512.01", opNo: "98100.1800")
    assert @@sv.claim_process_lot(lot)
    @@lots["test6015_Lot2"] = lot
  end

  def test6025_NewShips_SubKPI_Assignment
    lot_label = Time.now.to_i.hash.to_s # wtf?!

    assert lot = @@pdwhtest.new_lot(route: "BSM-FAB.01", product: "BSM-1FA.UAA00")
    assert_equal 0, @@sv.user_data_update(:lot, lot, {'LOT_LABEL'=>lot_label})
    assert_equal 0, @@sv.lot_opelocate(lot, "4000.1000")
    @@sv.lot_bankin(lot)
    assert_equal 0, @@sv.lot_ship(lot, "OY-SHIP")
    sleep 10
    assert_equal 0, @@sv.lot_ship_cancel(lot)
    assert_equal 0, @@sv.lot_bankin_cancel(lot)
    assert_equal 0, @@sv.schdl_change(lot, route: "C02-1597-BUMP.02", product: "BAGGER-1FA.BAA01", opNo: "99900.1000")
    assert_equal 0, @@sv.lot_bankin(lot)
    assert_equal 0, @@sv.lot_ship(lot, "OY-SHIP")
    @@lots["test6025_Lot1"] = lot

    assert lot = @@pdwhtest.new_lot({carrier_category: 'BEOL'}.merge(@@src_mfg_sort))
    assert_equal 0, @@sv.user_data_update(:lot, lot, {'LOT_LABEL'=>lot_label})
    assert_equal 0, @@sv.schdl_change(lot, route: "C02-1597-SORT.01", product: "BAGERA-1FA.SAA00", opNo: "99900.1000")
    @@sv.lot_bankin(lot)
    assert_equal 0, @@sv.lot_ship(lot, 'OY-SHIP')
    @@lots["test6025_Lot2"] = lot

    assert lot = @@pdwhtest.new_lot(route: "BSM-FAB.01", product: "BSM-1FA.UAA00")
    assert_equal 0, @@sv.user_data_update(:lot, lot, {'LOT_LABEL'=>lot_label})
    assert_equal 0, @@sv.lot_nonprobankin(lot, "O-TKYCBANK")
    sleep 10
    assert_equal 0, @@sv.lot_nonprobankout(lot)
    assert_equal 0, @@sv.schdl_change(lot, route: "C02-1597-BUMP.02", product: "BAGGER-1FA.BAA01", opNo: "1000.1000")
    assert_equal 0, @@sv.lot_nonprobankin(lot, "O-TKYCBANK")
    @@lots["test6025_Lot3"] = lot

    assert lot = @@pdwhtest.new_lot({carrier_category: 'BEOL'}.merge(@@src_mfg_sort))
    assert_equal 0, @@sv.user_data_update(:lot, lot, {'LOT_LABEL'=>lot_label})
    assert_equal 0, @@sv.schdl_change(lot, route: "C02-1597-SORT.01", product: "BAGERA-1FA.SAA00", opNo: "1000.1000")
    assert_equal 0, @@sv.lot_nonprobankin(lot, "O-TKYCBANK")
    @@lots["test6025_Lot4"] = lot

    assert lot = @@pdwhtest.new_lot(route: "BSM-FAB.01", product: "BSM-1FA.UAA00")
    assert_equal 0, @@sv.user_data_update(:lot, lot, {'LOT_LABEL'=>lot_label})
    assert_equal 0, @@sv.lot_opelocate(lot, "1000.1000")
    assert_equal 0, @@sv.lot_branch(lot, "BY-LOCASS-ASSEMBLY.01")
    @@lots["test6025_Lot5"] = lot

    assert lot = @@pdwhtest.new_lot({carrier_category: 'BEOL'}.merge(@@src_mfg_bump))
    assert_equal 0, @@sv.schdl_change(lot, route: "C02-1597-BUMP.02", product: "BAGGER-1FA.BAA01", opNo: "1000.1000")
    assert_equal 0, @@sv.user_data_update(:lot, lot, {'LOT_LABEL'=>lot_label})
    assert_equal 0, @@sv.lot_branch(lot, "BY-LOCASS-ASSEMBLY.01")
    @@lots["test6025_Lot6"] = lot

    assert lot = @@pdwhtest.new_lot({carrier_category: 'BEOL'}.merge(@@src_mfg_sort))
    assert_equal 0, @@sv.schdl_change(lot, route: "C02-1597-SORT.01", product: "BAGERA-1FA.SAA00", opNo: "1000.1000")
    assert_equal 0, @@sv.user_data_update(:lot, lot, {'LOT_LABEL'=>lot_label})
    assert_equal 0, @@sv.lot_branch(lot, "BY-LOCASS-ASSEMBLY.01")
    @@lots["test6025_Lot7"] = lot

    assert lot = @@pdwhtest.new_lot({carrier_category: 'BEOL'}.merge(@@src_mfg_sort))
    assert_equal 0, @@sv.schdl_change(lot, route: "PDWH.P006", product: "BAGERA-1FA.SAA00", opNo: "4000.1000")
    assert_equal 0, @@sv.user_data_update(:lot, lot, {'LOT_LABEL'=>lot_label})
    @@sv.lot_bankin(lot)
    assert_equal 0, @@sv.lot_ship(lot, "OT-SHIP")
    @@lots["test6025_Lot8"] = lot

    assert lot = @@pdwhtest.new_lot({carrier_category: 'BEOL'}.merge(@@src_mfg_sort))
    assert_equal 0, @@sv.schdl_change(lot, route: "PDWH.P006", product: "BAGERA-1FA.SAA00", opNo: "4000.1000")
    assert_equal 0, @@sv.user_data_update(:lot, lot, {'LOT_LABEL'=>lot_label})
    assert_equal 0, @@sv.lot_nonprobankin(lot, "O-TKYCBANK")
    @@lots["test6025_Lot9"] = lot

    assert lot = @@pdwhtest.new_lot({carrier_category: 'BEOL'}.merge(@@src_mfg_sort))
    assert_equal 0, @@sv.schdl_change(lot, route: "PDWH.P006", product: "BAGERA-1FA.SAA00", opNo: "1000.1000")
    assert_equal 0, @@sv.user_data_update(:lot, lot, {'LOT_LABEL'=>lot_label})
    assert_equal 0, @@sv.lot_branch(lot, "BY-LOCASS-ASSEMBLY.01")
    @@lots["test6025_Lot10"] = lot
  end

  def test6035_Lot_UDATA_UpdateScenario
    lot_label_old = Time.now.to_i.hash.to_s # wtf?!
    lot_label_new = Time.now.to_i.hash.to_s.reverse # wtf?!

    assert lot = @@pdwhtest.new_lot({carrier_category: 'BEOL'}.merge(@@src_mfg_sort))
    assert_equal 0, @@sv.user_data_update(:lot, lot, {'LOT_LABEL'=>lot_label_old})
    sleep 10
    assert_equal 0, @@sv.user_data_update(:lot, lot, {'LOT_LABEL'=>lot_label_new})
    sleep 10
    assert_equal 0, @@sv.user_data_delete(:lot, lot, 'LOT_LABEL')
    @@lots["test6035"] = lot
  end

  def test6045_LCRECIPE_UDATA_UpdateScenario
    lr = "P-LR-PDWHA01.01"

    assert @sm.update_udata(:lrcp, lr, {"SpecificationID"=>"Test"})
    sleep 10
    assert @sm.update_udata(:lrcp, lr, {"SpecificationID"=>"Test2"})
    sleep 10
    # assert @sm.update_udata(:lrcp, lr, {'SpecificationID'=>nil}, delete: true)
    assert @sm.update_udata(:lrcp, lr, {'SpecificationID'=>''})
    @@lots["test6045"] = lr
  end

  def XXXtest6055_INHIBIT_EXCEP_IntegrationTest # need to fix inhibit_update
    # 1. QA:  create new equipment based inhibit  for one sublottype
    # 2. QA:  create two inhibit exception lot entries (for 2 lots having the same sublottype like mentioned above)
    # 3. QA: process one of the exception lots at the equipment where the inhibit was specified
    # 4. QA: Modify the claim memo of the inihit
    # 5. QA: Process the second exception lot  at the equipment where the inhibit was specified

    route = "P-PDWH-MAINPD.01"
    product = "PDWHPROD5.01"

    assert lots = @@pdwhtest.new_lots(2, route: route, product: product, sublottype: "PO")
    assert_equal 0, @@sv.lot_gatepass(lots)

    eqps = @@sv.lot_info(lots.first).opEqps
    eqps.each {|e|
      assert_equal 0, @@sv.inhibit_cancel(:eqp, e)
      assert inh = @@sv.inhibit_entity(:eqp, e, sublottypes: "PO", memo: Time.now.to_s)
      lots.each {|l|
        assert_equal 0, @@sv.carrier_xfer_status_change(@@sv.lot_info(l).carrier, "MO", "UTSTO11")
        assert_equal 0, @@sv.inhibit_exception_register(l, inh)
      }
    }

    sleep 10
    assert @@sv.claim_process_lot(lots.first)

    sleep 10
    eqps.each {|e|
      assert_equal 0, @@sv.inhibit_update(:eqp, e, comment: Time.now.to_s)
      }

    sleep 10
    assert @@sv.claim_process_lot(lots.last)

    # remove inhibits
    sleep 10
    eqps.each {|e|
      assert_equal 0, @@sv.inhibit_cancel(:eqp, e)
      }

    @@lots["test6055"] = lots
  end

  def test6065_Closed_Lot_CT
    # lot1 skip MOL, lot2 will do OpeLocateBackward a lot
    assert lots = @@pdwhtest.new_lots(2, product: "PDWHPROD1.05", route: "P-PDWH-MAINPD.05")
    lot1, lot2 = lots
    assert_equal 0, @@sv.schdl_change(lot1, route: "P-PDWH-MAINPD.12")

    # lot 1
    assert_equal 0, @@sv.carrier_xfer_status_change(@@sv.lot_info(lot1).carrier, "MO", "UTSTO11")
    4.times {assert @@sv.claim_process_lot(lot1)} # FEOL
    li = @@sv.lot_info(lot1)
    $log.info("LotInfo")
    cl = @@sv.carrier_list(empty: true, carrier: "PDWH%", category: li.cast_cat, status: "AVAILABLE")
    assert_equal 0, @@sv.wafer_sort(lot1, cl.pop)
    sleep 5
    assert_equal 0, @@sv.carrier_xfer_status_change(@@sv.lot_info(lot1).carrier, "MO", "UTSTO11")
    assert @@sv.claim_process_lot(lot1) # HK

    # li = @@sv.lot_info(lot1)
    # cl = @@sv.carrier_list(empty: true, carrier: "PDWH%", category: li.cast_cat, status: "AVAILABLE")
    # @@sv.wafer_sort(lot1, cl.pop)
    # @@sv.carrier_xfer_status_change(@@sv.lot_info(lot1).carrier, "MO", "UTSTO11")
    # 2.times {@@sv.lot_opelocate(lot1, +1)}  # Locate MOL

    sleep 5
    li = @@sv.lot_info(lot1)
    c = @@sv.carrier_list(empty: true, carrier: "PDWH%", category: li.cast_cat, status: "AVAILABLE").pop
    assert_equal 0, @@sv.carrier_xfer_status_change(c, "MO", "UTSTO11")
    assert_equal 0, @@sv.wafer_sort(lot1, c)
    5.times {assert @@sv.claim_process_lot(lot1)} # BEOL and end

    # lot 2
    assert_equal 0, @@sv.carrier_xfer_status_change(@@sv.lot_info(lot2).carrier, "MO", "UTSTO11")
    assert @@sv.claim_process_lot(lot2) # FEOL
    assert_equal 0, @@sv.lot_opelocate(lot2, -1)
    3.times {assert @@sv.claim_process_lot(lot2)} # FEOL
    2.times {
      assert_equal 0, @@sv.lot_opelocate(lot2, -1)
      assert @@sv.claim_process_lot(lot2)
    } # FEOL
    assert @@sv.claim_process_lot(lot2)

    li = @@sv.lot_info(lot2)
    cl = @@sv.carrier_list(empty: true, carrier: "PDWH%", category: li.cast_cat, status: "AVAILABLE")
    assert_equal 0, @@sv.wafer_sort(lot2, cl.pop)
    assert_equal 0, @@sv.carrier_xfer_status_change(@@sv.lot_info(lot2).carrier, "MO", "UTSTO11")
    assert @@sv.claim_process_lot(lot2) # HK

    li = @@sv.lot_info(lot2)
    cl = @@sv.carrier_list(empty: true, carrier: "PDWH%", category: li.cast_cat, status: "AVAILABLE")
    assert_equal 0, @@sv.wafer_sort(lot2, cl.pop)
    assert_equal 0, @@sv.carrier_xfer_status_change(@@sv.lot_info(lot2).carrier, "MO", "UTSTO11")
    2.times {assert @@sv.claim_process_lot(lot2)}  # Locate MOL

    li = @@sv.lot_info(lot2)
    cl = @@sv.carrier_list(empty: true, carrier: "PDWH%", category: li.cast_cat, status: "AVAILABLE")
    assert_equal 0, @@sv.wafer_sort(lot2, cl.pop)
    assert_equal 0, @@sv.carrier_xfer_status_change(@@sv.lot_info(lot2).carrier, "MO", "UTSTO11")
    5.times {assert @@sv.claim_process_lot(lot2)} # BEOL and end

    @@lots["test6065"] = [lot1, lot2]
  end

  def test6075_scrap
    # 1. Scrap_PLY:
    # + Scrappe 3 wafer eines Loses an einer PD, welcher der neuen StageGrp. "PLY" zugeordnet ist
    # + Unscrappe 1 dieser 3 wafer wieder an der selben PD

    # 2. SCRAP_FAB:
    # + Scrappe 3 wafer eines Loses an einer PD, welcher der StageGrp. "FAB" zugeordnet ist
    # + Unscrappe 1 dieser 3 wafer wieder an der selben PD

    # 3. SCRAP_BUMP:
    # + Scrappe 3 wafer eines Loses an einer PD, welcher der StageGrp. "BUM" zugeordnet ist
    # + Unscrappe 1 dieser 3 wafer wieder an der selben PD

    # 4. SCRAP_FWET:
    # + Scrappe 3 wafer eines Loses an einer FWET PD (z.B. "LV-PREFWETRIE-9283.01")
    # + Unscrappe 1 dieser 3 wafer wieder an der selben PD

    assert lots = @@pdwhtest.new_lots(4, product: "PDWHPROD1.05", route: "P-PDWH-MAINPD.05")
    lot1, lot2, lot3, lot4 = lots

    assert_equal 0, @@sv.schdl_change(lot1, route: "P-PDWH-MAINPD.13")
    # assert_equal 0, @@sv.scrap_wafers(lot1, wafers: [1,2,3]), "scrap failed for #{lot1}"
    assert_equal 0, @@sv.scrap_wafers(lot1), "scrap failed for #{lot1}"
    sleep 10
    # assert_equal 0, @@sv.scrap_wafers_cancel(lot1, wafers: ['01'])  # not working any more
    @@sv.scrap_wafers_cancel(lot1)  # may be handled by autoscrapprocesshandler

    assert_equal 0, @@sv.schdl_change(lot2, route: "P-PDWH-MAINPD.13")
    assert_equal 0, @@sv.lot_opelocate(lot2, "2000.1000")
    # assert_equal 0, @@sv.scrap_wafers(lot2, wafers: [1,2,3]), 'scrap failed for #{lot1}'
    assert_equal 0, @@sv.scrap_wafers(lot2), "scrap failed for #{lot2}"
    sleep 10
    # assert_equal 0, @@sv.scrap_wafers_cancel(lot2, wafers: ['01'])
    @@sv.scrap_wafers_cancel(lot2)

    assert_equal 0, @@sv.schdl_change(lot3, route: "P-PDWH-MAINPD.14")  # for stage grp BUM
    assert_equal 0, @@sv.lot_opelocate(lot3, "9800.1000")
    # assert_equal 0, @@sv.scrap_wafers(lot3, wafers: [1,2,3]), 'scrap failed for #{lot1}'
    assert_equal 0, @@sv.scrap_wafers(lot3), "scrap failed for #{lot3}"
    sleep 10
    # assert_equal 0, @@sv.scrap_wafers_cancel(lot3, wafers: ['01'])
    @@sv.scrap_wafers_cancel(lot3)

    assert_equal 0, @@sv.schdl_change(lot4, route: "P-PDWH-MAINPD.07") # for FWET PD
    assert_equal 0, @@sv.lot_opelocate(lot4, "5000.1000")
    # assert_equal 0, @@sv.scrap_wafers(lot4, wafers: [1,2,3]), 'scrap failed for #{lot1}'
    assert_equal 0, @@sv.scrap_wafers(lot4), "scrap failed for #{lot4}"
    sleep 10
    # assert_equal 0, @@sv.scrap_wafers_cancel(lot4, wafers: ['01'])
    @@sv.scrap_wafers_cancel(lot4)

    @@lots["test6075"] = lots
    $log.info "test lots: #{lots}"
  end

  def test6076_scrap_bank_udata  # Equipment Monitor, route owner G-ETC, banked in
    ### UDATA ProductReportingType 'DEP-INT'
    assert lot = @@pdwhtest.new_lot(product: 'PDWHPROD-G-ETC.01', route: 'P-PDWH-MAINPD.01')
    assert_equal 0, @@sv.sublottype_change(lot, 'Equipment Monitor')
    assert_equal 0, @@sv.lot_nonprobankin(lot, @@nonprobank)
    # PD department not TWM
    assert oinfo = @sm.object_info(:pd, @@sv.lot_info(lot).op).first
    refute_equal 'TWM', oinfo.specific[:department]
    # scrap
    assert_equal 0, @@sv.scrap_wafers(lot)
    #
    @@lots["test6076"] = [lot]
  end

  def test6077_scrap_onroute_udata1  # Equipment Monitor, route owner G-ETC, NOT banked in
    ### UDATA ProductReportingType 'DEP-INT'
    assert lot = @@pdwhtest.new_lot(product: 'PDWHPROD-G-ETC.01', route: 'P-PDWH-MAINPD.01')
    assert_equal 0, @@sv.sublottype_change(lot, 'Equipment Monitor')
    # PD department not INT or ETC
    assert oinfo = @sm.object_info(:pd, @@sv.lot_info(lot).op).first
    refute_equal 'INT', oinfo.specific[:department]
    refute_equal 'ETC', oinfo.specific[:department]
    # scrap
    assert_equal 0, @@sv.scrap_wafers(lot)
    #
    @@lots["test6077"] = [lot]
  end

  def test6078_scrap_onroute_udata2  # Equipment Monitor, route owner G-ETC, NOT banked in
    ### UDATA ProductReportingType 'QATEST'
    assert lot = @@pdwhtest.new_lot(product: 'PDWHPROD-G-ETC.02', route: 'P-PDWH-MAINPD.01')
    assert_equal 0, @@sv.sublottype_change(lot, 'Equipment Monitor')
    # PD department not INT or ETC
    assert oinfo = @sm.object_info(:pd, @@sv.lot_info(lot).op).first
    refute_equal 'INT', oinfo.specific[:department]
    refute_equal 'ETC', oinfo.specific[:department]
    # scrap
    assert_equal 0, @@sv.scrap_wafers(lot)
    #
    @@lots["test6078"] = [lot]
  end

  def test6079_scrap_onroute_owner  # Equipment Monitor, route owner NOT G.., NOT banked in
    # no UDATA ProductReportingType, product owner not G...
    assert_nil @@sv.user_data(:product, 'PDWHPROD-TEST.01')['ProductReportingType']
    assert lot = @@pdwhtest.new_lot(product: 'PDWHPROD-TEST.01', route: 'P-PDWH-MAINPD.01')
    assert_equal 0, @@sv.sublottype_change(lot, 'Equipment Monitor')
    # PD department not INT or ETC
    assert oinfo = @sm.object_info(:pd, @@sv.lot_info(lot).op).first
    refute_equal 'INT', oinfo.specific[:department]
    refute_equal 'ETC', oinfo.specific[:department]
    # scrap
    assert_equal 0, @@sv.scrap_wafers(lot)
    #
    @@lots["test6079"] = [lot]
  end

  def test6085_skip_via_branch
    assert lot = @@pdwhtest.new_lot(product: "PDWHPROD1.05", route: "P-PDWH-MAINPD.05")
    assert_equal 0, @@sv.schdl_change(lot, route: "P-PDWH-MAINPD.15")
    assert @@sv.lot_experiment(lot, 25, "e3PDWH-SKIPONLY.01", "2000.1200", "2000.1300")
    assert @@sv.lot_opelocate(lot, "2000.1100")
    assert @@sv.claim_process_lot(lot)

    @@lots["test6085"] = [lot]
  end

  def test6105_eqp_monitor_in_bankin_onhold
    # (1) Setze ein Los onHold mit den folgenden Eigenschaften:
    # + LotType "Equipment Monitor"
    # + Los muss sich auf einer Bank befinden
    # + PD, wo das Los OnHold gesetzt wurde = "BANKIN.01"
    # + Department der PD "BANKIN.01" --> "PCO"
    # + HoldCode "ENG" / HoldType = LotHold
    # + RespOpeFlag = 0
    # + das Produkt des Loses muss das Product UDATA ProductReportingType auf  "DEP-INT" gesetzt haben
    # + das Produkt des Loses muss die Owner_ID "G-ETC" haben
    assert lot = @@pdwhtest.new_lot(product: 'PDWHPROD-G-ETC.01', route: 'P-PDWH-MAINPD.01')
    assert_equal 0, @@sv.sublottype_change(lot, 'Equipment Monitor')
    assert @@sv.lot_opelocate(lot, '9900.1200')
    assert @@sv.lot_bankin(lot)
    assert @@sv.lot_bankhold(lot, reason: 'ENG')

    @@lots["test6105"] = lot
  end

  def test6107_eqp_monitor_at_bankin_onhold
    # (2) Setze ein Los onHold mit den folgenden Eigenschaften:
    # + LotType "Equipment Monitor"
    # + Los darf sich nicht auf einer Bank befinden
    # + PD, wo das Los OnHold gesetzt wurde = "BANKIN.01"
    # + Department der PD "BANKIN.01" --> "PCO"
    # + HoldCode "ENG" / HoldType = LotHold
    # + RespOpeFlag = 0
    # + das Produkt des Loses muss das Product UDATA ProductReportingType auf  "DEP-INT" gesetzt haben
    # + das Produkt des Loses muss die Owner_ID "G-ETC" haben
    assert lot = @@pdwhtest.new_lot(product: 'PDWHPROD-G-ETC.01', route: 'P-PDWH-MAINPD.01')
    assert_equal 0, @@sv.sublottype_change(lot, 'Equipment Monitor')
    assert @@sv.lot_opelocate(lot, '9900.1200')
    assert @@sv.lot_hold(lot, 'ENG')

    @@lots["test6107"] = lot
  end

  def test6109_eqp_monitor_at_bankin_onhold_special_product
    # (3) Setze ein Los onHold mit den folgenden Eigenschaften:
    # + LotType "Equipment Monitor"
    # + Los darf sich nicht auf einer Bank befinden
    # + PD, wo das Los OnHold gesetzt wurde = "BANKIN.01"
    # + Department der PD "BANKIN.01" --> "PCO"
    # + HoldCode "ENG" / HoldType = LotHold
    # + RespOpeFlag = 0
    # + das Produkt des Loses  darf das Product UDATA ProductReportingType NICHT gesetzt haben
    # + die Owner_Id des Produktes des Loses darf NICHT mit "G-" anfangen
    assert lot = @@pdwhtest.new_lot(product: 'PDWHPROD-TEST.01', route: 'P-PDWH-MAINPD.01')
    assert_equal 0, @@sv.sublottype_change(lot, 'Equipment Monitor')
    assert @@sv.lot_opelocate(lot, '9900.1200')
    assert @@sv.lot_hold(lot, 'ENG')

    @@lots["test6109"] = lot
  end

  def test6111_eqp_monitor_opecomplete_at_bankin
    # (4) Erstelle ein Los vom LotType "Equipment Monitor"
    # + F�hre ein "OpeComplete" auf der PD "BANKIN.01" durch
    # + Setze das Los an der Folge PD "onHold" (RespOpeFlag = 1) --> responsible PD muss "BANKIN.01" sein
    # + HoldCode "ENG" / HoldType = LotHold
    # + das Produkt des Loses darf das Product UDATA ProductReportingType NICHT gesetzt haben
    # + das Produkt des Loses muss die Owner_ID "G-ETC" haben
    assert lot = @@pdwhtest.new_lot(product: 'PDWHPROD-G-ETC.01', route: 'P-PDWH-MAINPD.01')
    assert_equal 0, @@sv.schdl_change(lot, route: 'P-PDWH-MAINPD.16')
    assert_equal 0, @@sv.sublottype_change(lot, 'Equipment Monitor')
    assert @@sv.lot_opelocate(lot, '9900.1200')
    assert @@sv.claim_process_lot(lot)
    assert @@sv.lot_hold(lot, 'ENG', rsp_mark: 'P')

    @@lots["test6111"] = lot
  end

  def test6113_eqp_monitor_at_bankin
    # (5) Setze ein Los onHold mit den folgenden Eigenschaften:
    # + LotType "Equipment Monitor"
    # + Los darf sich nicht auf einer Bank befinden
    # + PD, wo das Los OnHold gesetzt wurde => irgendeine PD_ID (ausser "BANKIN.01")
    # + Department dieser PD --> "CMP"
    # + PD UDATA "CounterpartDept" dieser PD => "ICL"
    # + PD UDATA "ProcessGroup" dieser PD darf NICHT gesetzt sein
    # + HoldCode "ENG" / HoldType = LotHold
    # + RespOpeFlag = 0
    # + das Produkt des Loses muss das Product UDATA ProductReportingType auf  "DEP-INT" gesetzt haben
    # + das Produkt des Loses muss die Owner_ID "G-ETC" haben

    # PD 10METPOL.01 (5900.4100)
    # Route C02-1107.01
    # Produkt: 2220BS.Q1

    assert lot = @@pdwhtest.new_lot(product: '2220BS.Q1', route: 'C02-1107.01')
    assert_equal 0, @@sv.sublottype_change(lot, 'Equipment Monitor')
    assert @@sv.lot_opelocate(lot, '5900.4100')
    assert @@sv.lot_hold(lot, 'ENG')

    @@lots["test6113"] = lot
  end

  def test6115_lot_onhold_without_holdcode_grouping_udata
    # 1. Set a lot on Hold with:
    # +  RespOpeFlag =1
    # + Ensure that Code UDATA ""ReasonCodeGrouping"" is not defined
    # + Ensure that PD UDATAs ""CounterpartDept"" and ""ProcessGroup"" are not defined for the Hold PD
    # 2. Wait >1 hour
    # 3. Change department of Hold PD

    assert lot1 = @@pdwhtest.new_lot(product: '2220BS.Q1', route: 'C02-1107.01')
    assert @@sv.lot_opelocate(lot1, '2700.1000')
    assert @@sv.claim_process_lot(lot1)
    assert @@sv.lot_hold(lot1, 'DQE', rsp_mark: 'P')

    assert lot2 = @@pdwhtest.new_lot(product: '2220BS.Q1', route: 'C02-1107.01')
    assert @@sv.lot_opelocate(lot2, '2700.1000')
    assert @@sv.claim_process_lot(lot2)
    assert @@sv.lot_hold(lot2, 'DQE')

    @@lots["test6115"] = [lot1,lot2]
  end

  def test6120_ctcl
    # PD1 --> StageGrp=FAB
    # PD2 --> StageGrp=FAB
    # PD3 --> StageGrp=FAB
    # PD4 --> StageGrp=BUM
    # PD5 --> StageGrp=BUM

    # + OpeComplete von PD1
    # + GatePass von PD2
    # + GatePass von PD3
    # + OpeComplete von PD4
    # + Los befindet sich an PD5
    assert lot = @@pdwhtest.new_lot(product: 'PDWHPROD-G-ETC.01', route: 'P-PDWH-MAINPD.01', carrier_category: "MOL")
    assert_equal 0, @@sv.schdl_change(lot, route: 'P-PDWH-MAINPD.17')
    assert_equal 0, @@sv.lot_opelocate(lot, '4000.1100')
    assert @@sv.claim_process_lot(lot)
    assert_equal 0, @@sv.lot_gatepass(lot)
    assert_equal 0, @@sv.lot_gatepass(lot)
    assert @@sv.claim_process_lot(lot)

    @@lots["test6120"] = lot
  end

  def test6125_ctcl_pre1_skip
    # PD1 --> StageGrp=FAB
    # PD2 --> StageGrp=FAB
    # PD3 --> StageGrp=FAB
    # PD4 --> StageGrp=BUM
    # PD5 --> StageGrp=BUM

    # + OpeComplete von PD1
    # + 2* Pre1 Skip
    # + OpeComplete von PD4
    # + Los befindet sich an PD5
    assert lot = @@pdwhtest.new_lot(product: 'PDWHPROD-G-ETC.01', route: 'P-PDWH-MAINPD.01', carrier_category: "MOL")
    assert_equal 0, @@sv.schdl_change(lot, route: 'P-PDWH-MAINPD.18')
    assert_equal 0, @@sv.lot_opelocate(lot, '4000.1100')
    assert @@sv.claim_process_lot(lot)
    sleep 5  # Pre1 Skip
    assert @@sv.claim_process_lot(lot)

    @@lots["test6125"] = lot
  end

  def test6205_opecomplete_foitocvier
    assert lot = @@pdwhtest.new_lot(carrier_category: 'C4')
    assert_equal 0, @@sv.schdl_change(lot, route: 'C02-1219.01', opNo: '5520.1700') #, product: '')

    assert @@sv.claim_process_lot(lot)
    @@lots["test6205"] = lot
  end

  def test6215_opecomplete_lotstartfacht
    assert lot = @@pdwhtest.new_lot(carrier_category: 'BEOL')
    assert_equal 0, @@sv.schdl_change(lot, route: 'C02-1910.0001')
    assert @@sv.claim_process_lot(lot)

    @@lots["test6215"] = lot
  end

  def test6225_opecomplete_sortsrteins
    assert lot = @@pdwhtest.new_lot()
    assert_equal 0, @@sv.schdl_change(lot, route: 'P-PDWH6.01', opNo: '6400.1000')
    assert @@sv.claim_process_lot(lot)

    @@lots["test6225"] = lot
  end

  def test6305_rtcl_pod
    orig_podid = "PDWH"
    copy_podid = unique_string
    copy_desc = "copied reticle pod"
    reticles = %w(PDWHRETICLE1 PDWHRETICLE2)
    eqp = "ALC308"

    @@sv.reticle_xfer_status_change(reticles[0], "MO", stocker: "RET110")
    @@sv.reticle_xfer_status_change(reticles[1], "MO", stocker: "RET110")

    @@sm.delete_objects(:reticlepod, copy_podid)
    assert_empty @@sv.reticle_pod_list(pod: copy_podid), "new reticle pod already exists"
    assert @@sm.copy_object(:reticlepod, orig_podid, copy_podid, description: copy_desc)

    assert @@sv.reticle_pod_xfer_status_change(copy_podid, 'MO', stocker: "RET110")
    assert @@sv.reticle_pod_multistatus_change(copy_podid, "AVAILABLE")

    assert @@sv.reticle_justinout(reticles[0], copy_podid, 1, 'in')
    assert @@sv.reticle_justinout(reticles[1], copy_podid, 2, 'in')
    assert @@sv.reticle_pod_offline_load(eqp, "RP1", copy_podid)

    sleep 30 # wait some seconds

    assert @@sv.reticle_pod_offline_unload(eqp, "RP1", copy_podid)
    assert @@sv.reticle_sort(reticles[0], copy_podid, src_slot:1, tgt_slot: 3)
    assert @@sv.reticle_sort(reticles[1], copy_podid, src_slot:2, tgt_slot: 4)

    sleep 3600

    assert @@sv.reticle_justinout(reticles[0], copy_podid, 3, 'out')
    assert @@sv.reticle_pod_xfer_status_change(copy_podid, 'MI', stocker: "RET110")
    assert @@sv.reticle_justinout(reticles[1], copy_podid, 4, 'out')

    assert @@sv.reticle_pod_multistatus_change(copy_podid, "NOTAVAILABLE", substate: "OTHER")
    sleep 10
    @@sm.delete_objects(:reticlepod, copy_podid)
    @@lots["test6305"] = copy_podid
  end

  def test6315_bank_splt_hld_scrp
    lot = @@pdwhtest.new_lot(nwafers: 4)
    assert_equal 0, @@sv.lot_opelocate(lot, "9900.1200")
    assert_equal 0, @@sv.lot_bankin(lot)
    assert clot = @@sv.lot_split(lot, 2)
    sleep 30
    assert_equal 0, @@sv.lot_bankhold(lot)
    sleep 30
    assert_equal 0, @@sv.lot_bankhold(clot)
    assert slot = @@sv.lot_split(clot, 1, onhold: true)    #scrap lot
    assert_equal 0, @@sv.lot_bankhold_release(lot)
    assert_equal 0, @@sv.lot_bankhold_release(clot)
    assert_equal 0, @@sv.lot_bankhold_release(slot)
    assert_equal 0, @@sv.scrap_wafers(slot)
    assert_equal 0, @@sv.lot_merge(lot, clot)
    assert_equal 0, @@sv.lot_bankin_cancel(lot)

    @@lots["test6315"] = lot
  end

  def test6325_reticle_movement
    rpod = 'PDWH1'
    r1, r2, r3, r4 = %w(PDWHRETICLE11 PDWHRETICLE12 PDWHRETICLE13 PDWHRETICLE14)
    rall = [r1, r2, r3, r4]

    #cleanup
    assert @@sv.reticle_justinout(r1, rpod, 1, 'out')
    assert @@sv.reticle_justinout(r2, rpod, 2, 'out')
    assert @@sv.reticle_justinout(r3, rpod, 3, 'out')
    assert @@sv.reticle_justinout(r4, rpod, 4, 'out')

    #state 1
    rall.each{|r| @@sv.reticle_xfer_status_change(r, 'MO', stocker: 'RET110')}
    assert @@sv.reticle_pod_xfer_status_change(rpod, 'MO', stocker: 'RET110')
    assert @@sv.reticle_pod_multistatus_change(rpod, 'AVAILABLE')
    assert @@sv.reticle_pod_xfer_status_change(rpod, 'MO', stocker: 'RET110')
    assert @@sv.reticle_pod_multistatus_change(rpod, 'AVAILABLE')
    sleep 10

    #state 2
    assert @@sv.reticle_justinout(r1, rpod, 1, 'in')
    assert @@sv.reticle_justinout(r2, rpod, 2, 'in')
    assert @@sv.reticle_justinout(r3, rpod, 3, 'in')
    sleep 10

    #state 3
    assert @@sv.reticle_sort(r1, rpod, src_slot: 1, tgt_slot: 4)
    assert @@sv.reticle_sort(r2, rpod, src_slot: 2, tgt_slot: 1)
    assert @@sv.reticle_sort(r3, rpod, src_slot: 3, tgt_slot: 2)
    assert @@sv.reticle_sort(r1, rpod, src_slot: 4, tgt_slot: 3)
    sleep 10

    #state 4
    assert @@sv.reticle_justinout(r3, rpod, 2, 'out')
    assert @@sv.reticle_sort(r2, rpod, src_slot: 1, tgt_slot: 2)
    sleep 10

    #state 5
    assert @@sv.reticle_justinout(r3, rpod, 1, 'in')
    assert @@sv.reticle_justinout(r2, rpod, 2, 'out')
    assert @@sv.reticle_sort(r1, rpod, src_slot: 3, tgt_slot: 2)
    sleep 10

    #state 6
    assert @@sv.reticle_justinout(r3, rpod, 1, 'out')
    assert @@sv.reticle_justinout(r1, rpod, 2, 'out')
    assert @@sv.reticle_justinout(r2, rpod, 2, 'in')
    assert @@sv.reticle_justinout(r4, rpod, 4, 'in')
    sleep 10
  end

  def test6405_bre_lotcomp_lotgatepass
    lot1, lot2 = @@pdwhtest.new_lots(2)
    @@sv.schdl_change(lot1, route: "P-PDWH-MAINPD.19")
    @@sv.lot_opelocate(lot1, "9800.1000")
    @@sv.claim_process_lot(lot1)

    @@sv.schdl_change(lot2, route: "P-PDWH-MAINPD.19")
    @@sv.lot_opelocate(lot2, "9800.1000")
    @@sv.lot_gatepass(lot2)

    @@lots["test6405"] = [lot1, lot2]
  end

  def test6415_pass
    pass
  end
end
