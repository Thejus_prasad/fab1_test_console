=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: Thomas Klose and Steffen Frieske, 2015-11-25

History:
  2021-09-15 sfrieske, finally replaced $svtest by @@svtest, $sv by @@sv and $sm by @@sm
=end

require_relative 'PDWHR'


# PDWH Regression tests for historization of CAST, EQP, PRODUCT and lots
class PDWHR110 < PDWHR
  @@carrier0 = 'PDWH0'
  @@carrierx = 'PDWH0NEW'
  @@eqp0 = 'PDWHA01'
  @@eqpx = 'PDWHA01NEW'
  @@productx = 'PDWHPRODNEW.QA'
  @@srcproductx = 'PDWHSRCPRODNEW.QA'
  @@sublottype2 = 'ES'


  def self.shutdown
    super
    @@sv.carrier_status_change(@@carrier0, 'NOTAVAILABLE', check: true)
  end


  def test11000_setup
    $setup_ok = false
    #
    # delete the test objects, yields true even if they don't exist
    @@sm.delete_objects(:carrier, @@carrierx)
    @@sm.delete_objects(:eqp, @@eqpx)
    @@sm.delete_objects(:product, @@productx)
    @@sm.delete_objects(:product, @@srcproductx)
    # verify the SM objects to create don't exist
    assert_empty @@sv.carrier_list(carrier: @@carrierx), "new carrier already exists"
    assert_empty @@sv.eqp_list(eqp: @@eqpx), "new eqp already exists"
    assert_empty @@sv.product_list.find {|p| p.productID.identifier == @productx}, "new product already exists"
    # create new objects
    @@tstart = Time.now
    assert @@sm.copy_object(:carrier, @@carrier0, @@carrierx)
    assert @@sm.copy_object(:eqp, @@eqp0, @@eqpx)
    assert @@sm.copy_object(:product, @@pdwhtest.product, @@productx)
    assert @@sm.copy_object(:product, @@pdwhtest.product, @@srcproductx)
    assert @@sm.add_srcproduct(@@productx, @@srcproductx)
    # wait and delete the new objects
    $log.info "waiting #{@@separation}s to separate records"; sleep @@separation
    assert @@sm.delete_production(:carrier, @@carrierx)
    assert @@sm.delete_production(:eqp, @@eqpx)
    assert @@sm.remove_srcproduct(@@productx, @@srcproductx)
    sleep 3
    assert @@sm.delete_production(:product, @@productx)
    #
    $setup_ok = true
  end

  def test11010_carrier
    assert @@pdwhtest.wait_loader {@@lldb.cast(carrier: @@carrierx, tstart: @@tstart, deleted: 'Y').last}, 'loader timeout'
    assert c = @@lldb.cast(carrier: @@carrierx, tstart: @@tstart).last
    assert chist = @@lldb.cast(carrier: @@carrierx, tstart: @@tstart, hist: true).last
    assert_equal c.systemkey, chist.systemkey
    assert_equal c.claim_time, chist.valid_to
    assert_equal 'Y', c.deleted
  end

  def test11020_eqp
    assert @@pdwhtest.wait_loader {@@lldb.eqp(eqp: @@eqpx, tstart: @@tstart, deleted: 'Y').last}, 'loader timeout'
    assert e = @@lldb.eqp(eqp: @@eqpx, tstart: @@tstart).last
    assert ehist = @@lldb.eqp(eqp: @@eqpx, tstart: @@tstart, hist: true).last
    assert_equal e.systemkey, ehist.systemkey
    assert_equal e.update_date, ehist.valid_to
    assert_equal 'Y', e.deleted
  end

  def test11030_product
    assert @@pdwhtest.wait_loader {@@lldb.product(product: @@productx, tstart: @@tstart, deleted: 'Y').last}, 'loader timeout'
    assert e = @@lldb.product(product: @@productx, tstart: @@tstart).last
    assert ehist = @@lldb.product(product: @@productx, tstart: @@tstart, hist: true).last
    assert_equal e.update_date, ehist.valid_to
    assert_equal 'Y', e.deleted
  end

  def test11040_srcproduct
    assert @@pdwhtest.wait_loader {@@lldb.product(product: @@productx, tstart: @@tstart, deleted: 'Y').last}, 'loader timeout'
    assert productsk = @@lldb.product(product: @@productx, tstart: @@tstart).last.sk
    assert @@pdwhtest.wait_loader {@@lldb.srcproduct_hist(name: @@srcproductx, productsk: productsk, tstart: @@tstart).last}, 'loader timeout'
    ee = @@lldb.srcproduct_hist(name: @@srcproductx, productsk: productsk, tstart: @@tstart)
    assert_equal 1, ee.size, "wrong srcproducts"
    e = ee.last
    assert e.valid_from > @@tstart
    assert e.valid_to > @@tstart && e.valid_to < Time.now
  end


  def testman11110_lot
    assert lot = @@pdwhtest.new_lot, "error creating testlot"
    @@testlots << lot
    $log.info "waiting #{@@separation}s to separate records"; sleep @@separation
    assert_equal 0, @@sv.schdl_change(lot, sublottype: @@sublottype2)
    assert_equal @@sublottype2, @@sv.lot_info(lot).sublottype
    $log.info "waiting #{@@separation}s to separate records"; sleep @@separation
    assert_equal 0, @@sv.delete_lot_family(lot, delete: true)
    assert_empty @@sv.lot_list(lot: lot), "lot #{lot} has not been deleted"
    #
    $log.info "waiting up to #{@@loader_interval / 60}min for the loader to run"
  end

end
