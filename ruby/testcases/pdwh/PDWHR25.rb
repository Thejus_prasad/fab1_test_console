=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: Thomas Klose and Steffen Frieske, 2015-11-25

=end

require_relative 'PDWHR'


# PDWH Regression tests for SiView data basics: TRA.
class PDWHR25 < PDWHR
  @@route = 'UTRT001.01'
  @@product = 'UT-PRODUCT000.01'
  @@eqpib = 'UTI002'        # needs to be changed
  @@eqpib_port = 'P1'
  @@eqpmode = 'Off-Line-2'  # the JavaEI processes the lots even in Semi-2
  @@opNo_ib = '1000.200'
  @@stocker2 = 'UTSTO12'
  @@stocker3 = 'UTSTO13'
  # changed this to match RTD_Rule_xx tests, needs verification (was 'Location'=>'F36' and 'BTF')
  @@loc1 = {'AmhsArea'=>'F36', 'Location'=>'M1-L3-MAIN'}
  @@loc2 = {'AmhsArea'=>'F38', 'Location'=>'M2-L3-MAIN'}


  def self.startup
    super
    @@dummyamhs = Dummy::AMHS.new($env)
  end

  def self.shutdown
    super
    @@dummyamhs.set_variable('intrabaytime', 15000)
    @@dummyamhs.set_variable('fallbackstocker', @@pdwhtest.stocker)
  end


  def test25000_setup
    $setup_ok = false
    #
    [@@eqpib].each {|eqp| assert @@pdwhtest.cleanup_eqp(eqp, mode: @@eqpmode)}
    assert @@dummyamhs.register
    assert @@dummyamhs.set_variable('intrabaytime', 45000)
    assert @@dummyamhs.set_variable('stockintime', 1000)
    assert @@dummyamhs.set_variable('stockouttime', 1000)
    assert @@pdwhtest.stocker_location(@@pdwhtest.stocker, @@loc1)
    assert @@pdwhtest.stocker_location(@@stocker2, @@loc1)
    assert @@pdwhtest.stocker_location(@@stocker3, @@loc1)
    assert @@pdwhtest.eqp_location(@@eqpib, @@loc2)
    #
    $setup_ok = true
  end

  def test25030_sto_eqp_4  # replaces old test4303_sto_eqp_4
    # same locations, with cj and immediate transport
    assert lots = @@pdwhtest.new_lots(4, route: @@route, product: @@product)
    @@testlots += lots
    carriers = lots.collect {|lot| @@sv.lot_info(lot).carrier}
    carriers.each {|c| assert_equal 0, @@sv.carrier_xfer_status_change(c, 'MI', @@stocker2)}
    lots.each {|lot| assert_equal 0, @@sv.lot_opelocate(lot, @@opNo_ib)}
    assert @@pdwhtest.cleanup_eqp(@@eqpib, mode: @@eqpmode)
    tstart = Time.now
    # create cj
    assert cj = @@sv.slr(@@eqpib, port: @@eqpib_port, lot: lots, carrier: carriers)
    # create xfer job
    assert_equal 0, @@sv.carrier_xfer(carriers, @@stocker2, '', @@eqpib, @@eqpib_port)
    assert wait_for(timeout: 60) {!@@sv.carrier_xferjob(carriers.first).empty?}, 'no xfer job'
    xjs = carriers.collect {|c| @@sv.carrier_xferjob(c)}
    carriers.each {|c| assert wait_for {@@sv.carrier_xferjob(c).empty?}}
    # wait for next loader runs to complete
    assert @@pdwhtest.wait_next_batch_run('SIVIEW_DIM'), 'loader timeout'
    assert @@pdwhtest.wait_next_batch_run('SIVIEW_TRA'), 'loader timeout'  # for CJ hist
    assert @@pdwhtest.wait_next_batch_run('SIVIEW_TRN'), 'loader timeout'  # for XJ hist
    # process
    assert @@sv.claim_process_lot(lots, eqp: @@eqpib, port: @@eqpib_port)
    # verify
    $log.info "lots: #{lots}"
    $log.info "carriers: #{carriers}"
    $log.info "cj: #{cj.inspect}"
    assert @@pdwhtest.wait_loader {@@lldb.trnreq_hist(cj: cj).size >= 4}, 'loader timeout'
    assert @@pdwhtest.wait_loader {@@lldb.cj_hist(cj: cj, state: 'Completed').size == 4}, 'loader timeout'
    lots.each_with_index {|lot, i|
      assert cjdata = @@lldb.cj_hist(cj: cj, lot: lot, tstart: tstart).last
      assert xjdata = @@lldb.trnreq_hist(cj: cj, carrier: carriers[i], tstart: tstart).last
      xj = xjs[i].first
      assert_equal xj.first.jobID, xjdata.jobid
      assert_equal xj.first.strCarrierJobResult.first.carrierJobID, xjdata.cjobid
      assert_equal 'B', xjdata.type
      assert_equal @@stocker2, xjdata.frommachine
      assert_equal nil, xjdata.fromport
      assert_equal @@eqpib, xjdata.tomachine
      assert_equal @@eqpib_port, xjdata.toport
    }
  end

  def test25040_reroute # replaces old test4304_reroute
    # same locations, with cj and reroute
    assert lot = @@pdwhtest.new_lot(route: @@route, product: @@product)
    @@testlots << lot
    assert_equal 0, @@sv.lot_opelocate(lot, @@opNo_ib)
    tstart = Time.now
    c = @@sv.lot_info(lot).carrier
    assert_equal 0, @@sv.carrier_xfer_status_change(c, 'MI', @@stocker2)
    assert_equal 0, @@sv.eqp_cleanup(@@eqpib, mode: @@eqpmode)
    # create xfer job
    assert_equal 0, @@sv.carrier_xfer(c, @@stocker2, '', @@stocker3, '')
    # create cj
    assert cj = @@sv.slr(@@eqpib, port: @@eqpib_port, lot: lot, carrier: c)
    sleep 1
    xjs = @@sv.carrier_xferjob_detail(c)
    # re-route carrier
    assert_equal 0, @@sv.carrier_xfer(c, '', '', @@eqpib, @@eqpib_port)
    sleep 1
    xjs = @@sv.carrier_xferjob_detail(c)
    assert wait_for {@@sv.carrier_xferjob(c).empty?}
    # verify
    $log.info "lot: #{lot}"
    $log.info "carrier: #{c}"
    $log.info "cj: #{cj.inspect}"
    assert @@pdwhtest.wait_loader {@@lldb.trnreq_history(cj: cj, tstart: tstart).first}, 'loader timeout'
    xjdata = @@lldb.trnreq_history(cj: cj, tstart: tstart)
    assert_equal 1, xjdata.size, 'too many entries'
    assert_equal true, xjdata.first.rerouted
    assert_equal 1, xjdata.first.nreroutes
    assert_equal @@stocker2, xjdata.first.frommachine
    assert_equal @@eqpib, xjdata.first.tomachine
    assert_equal @@eqpib_port, xjdata.first.toport
  end

end
