=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: Steffen Steidten, Thomas Klose, 2010-06-23

Version: 1.6.0

History:
  2019-02-12 sfrieske, use siviewtest for lot creation directly
=end

require_relative 'Test_CP'


# Test Common Platform data feed Stage Move
#
# Requirements: http://myteamsmst/sites/MST/IPMO/prj/Common%20Platform%20Integration/Shared%20Documents/MFG%20TRACK/Requirement%20Details/CPDI_STAGEMOVE_requirements.xls
class Test_CP10 < Test_CP
  @@feed = 'lot_stage_move'

  @@rework_route = 'R-L-CPRW-LITH.01'
  @@event_sleep = 60


  def test09_report
    $setup_ok = false
    #
    # create test lots
    uparams = {'CustomerPriority'=>'QA_Prio'}
    assert @@testlots = @@svtest.new_lots(8, user_parameters: uparams), 'error creating test lots'
    @@lot1, @@lot2, @@lot3, @@lot4, @@lot5, @@lot6, @@lot7, @@lot8 = @@testlots

    @@testlots.each {|lot|
      # set die counts, opecomp to make new values visible
      @@sv.lot_info(lot, wafers: true).wafers.each {|w|
        assert_equal 0, @@sv.user_parameter_change('Wafer', w.wafer, 'DieCountGood', 100)
      }
      assert @@sv.claim_process_lot(lot)
    }
    #
    $log.info "waiting #{@@event_sleep} s to separate events"; sleep @@event_sleep
    #
    # locate
    @@oldstage1 = @@sv.lot_info(@@lot1).stage
    assert_equal 0, @@sv.lot_opelocate(@@lot1, '1100.1200')
    #
    # opecomplete with split lot
    assert_equal 0, @@sv.lot_opelocate(@@lot2, '1100.1200')
    assert @@lot2c = @@sv.lot_split(@@lot2, 5)
    # processes @@lot2 AND @@lot2c
    @@oldstage2 = @@sv.lot_info(@@lot2).stage
    assert @@sv.claim_process_lot(@@lot2, mode: 'Off-Line-1', notify: false), "error processing lot"
    assert @@sv.claim_process_lot(@@lot2c, mode: 'Off-Line-1', notify: false), "error processing lot"
    #
    # branch with stage ID
    assert_equal 0, @@sv.lot_experiment(@@lot3, 25, 'A-CP-BRANCH-WSTAGEID.01', '1100.1000', '1200.1100')
    # Get stage ID before the experiment
    assert opentry = @@sv.lot_operation_list(@@lot3).find {|o| o.opNo == '1100.1000'}, "no entry for stage"
    @@oldstage3 = opentry.stage
    assert_equal 0, @@sv.lot_opelocate(@@lot3, '1100.1000')
    #
    # branch without stage ID
    assert_equal 0, @@sv.lot_experiment(@@lot4, 25, 'A-CP-BRANCH-MAINPD.01', '1100.1000', '1200.1100')
    @@oldstage4 = @@sv.lot_info(@@lot4).stage
    assert_equal 0, @@sv.lot_opelocate(@@lot4, '1100.1000')
    #
    # locate, split child in new stage (issue #114, must not be reported)
    @@oldstage5 = @@sv.lot_info(@@lot5).stage
    assert_equal 0, @@sv.lot_opelocate(@@lot5, '1100.1200')
    @@li5 = @@sv.lot_info(@@lot5, wafers: true)
    @@lot5c = @@sv.lot_split(@@lot5, 5)
    #
    # locateBackward (issue #126)
    assert_equal 0, @@sv.lot_opelocate(@@lot6, '1100.1100')
    assert @@sv.claim_process_lot(@@lot6)
    @@oldstage6 = @@sv.lot_info(@@lot6).stage
    assert_equal 0, @@sv.lot_opelocate(@@lot6, '1000.1100')
    #
    # branch (rework) with stage NA on rework route and return to next stage on main route
    @@oldstage7 = @@sv.lot_info(@@lot7).stage
    assert_equal 0, @@sv.lot_opelocate(@@lot7, '1000.1200')
    assert_equal 0, @@sv.lot_rework(@@lot7, @@rework_route, return_opNo: '1100.1100', memo: Time.now.strftime('%F_%T'))
    while @@sv.lot_info(@@lot7).route != @@rework_route
      assert @@sv.claim_process_lot(@@lot7)
    end

    # branch w/o stage id
    assert_equal 0, @@sv.lot_experiment(@@lot8, 25, 'A-CP-BRANCH-MAINPD.01', '1100.1000', '1200.1100')
    assert_equal 0, @@sv.lot_opelocate(@@lot8, '1100.1000')
    @@oldstage8 = "2ML"  # fix me!
    3.times {assert @@sv.claim_process_lot(@@lot8)}

    #
    # run the loader
    assert @@cp.update_reports(@@sleeptime, @@feed) ## breaks the feed: "-PRODSPEC_ID 8901AA.A0 -DAYS_BACK 1")
    #
    $setup_ok = true
  end

  def test11_locate
    assert @@cptest.verify_stage_move_data(@@lot1, 'LocateForward', @@oldstage1)
  end

  def test12_opecomplete
    assert @@cptest.verify_stage_move_data(@@lot2, 'OperationComplete', @@oldstage2)
  end

  def test13_opecomplete_child
    assert @@cptest.verify_stage_move_data(@@lot2c, 'OperationComplete', @@oldstage2)
  end

  def test14_branch_valid_stage
    ## not easily testable here, but has been tested with lot1
    ##check_stage_move_data(@@lot3, 'LocateForward', @@lot3_time, oldstageid: @@lot3_oldstage, :stageid=>'2ML')
    #
    assert @@cptest.verify_stage_move_data(@@lot3, 'Branch', @@oldstage3)
  end

  def test15_branch_invalid_stage
    ## not easily testable here, but has been tested with lot1
    ##check_stage_move_data(@@lot4, "LocateForward", @@lot4_time, oldstageid: @@lot4_oldstage, :stageid=>"2ML")
    #
    # filter for OPE_CATEGORY Branch, this should be suppressed because of the stage ID 'NA'
    hops = @@sv.lot_operation_history(@@lot4, first: true, category: 'Branch')
    records = @@cp.reports[@@feed].get_records_lot(@@lot4, timestamp: hops.last.timestamp, spread: @@event_sleep)
    records = records.select {|r| r['OPE_CATEGORY'] == 'Branch'}
    assert_equal 0, records.size, "invalid stage move to NA reported for #{@@lot4}"
  end

  def test16_locate_and_split
    assert @@cptest.verify_stage_move_data(@@lot5, 'LocateForward', @@oldstage5, lotinfo: @@li5)
  end

  def test17_locate_backward
    assert @@cptest.verify_stage_move_data(@@lot6, 'LocateBackward', @@oldstage6)
  end

  def XXXtest18_branch_na_return
    # not fixed yet
    $log.info("Not fixed yet!")
    assert @@cptest.verify_stage_move_data(@@lot7, 'LocateForward', @@oldstage7)
  end

  def test19_branch_invalid_stage_back_return
    # fixed in CP1.51
    assert @@cptest.verify_stage_move_data(@@lot8, 'OperationComplete', @@oldstage8)
  end
end
