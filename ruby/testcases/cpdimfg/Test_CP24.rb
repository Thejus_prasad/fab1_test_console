=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: Steffen Steidten, Thomas Klose, 2011-04-19
Version: 1.13.0
=end

require_relative 'Test_CP'


# Test Common Platform data feed "3C Recipe ID List".
#
# Requirements: http://myteamsmst/sites/MST/IPMO/prj/Common%20Platform%20Integration/Shared%20Documents/MFG%20TRACK/Requirement%20Details/3C_RECIPEIDLIST_Requirements.xlsx
class Test_CP24 < Test_CP
  @@feed = 'recipeidlist'

  @@lrcp_process = 'EN-ETX201-MM1-MSK-PC-V1.01'
  @@lrcp_measurement = 'EN-DFU10X-OX120-ETX10X-PRE.01'
  @@lrcp_other = 'C-N-ME-FINGER.01'


  def test09_report
    assert $setup_ok = @@cp.update_reports(3, @@feed)
  end


  def test11_process
    assert @@cptest.verify_recipeidlist(@@lrcp_process, 'Process')
  end

  def test12_measurement
    assert @@cptest.verify_recipeidlist(@@lrcp_measurement, 'Measurement')
  end

  def test13_other
    assert @@cptest.verify_recipeidlist(@@lrcp_other, 'Other')
  end

end
