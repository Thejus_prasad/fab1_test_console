=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: Steffen Steidten, Thomas Klose, 2010-06-23

Version: 1.37

History:
  2019-02-12 sfrieske, use siviewtest for lot creation directly
  2019-05-02 sfrieske, use new_lot_release instead of new_lot(stb: false)
=end

require_relative 'Test_CP'

# Test Common Platform data feed Plan Start
#
# Requirements: http://myteamsmst/sites/MST/IPMO/prj/Common%20Platform%20Integration/Shared%20Documents/MFG%20TRACK/Requirement%20Details/CPDI_PLANSTART_requirements.xlsx
class Test_CP19 < Test_CP
  @@feed = 'plan_start'

  @@sublottype = 'PO'
  @@erpitem = 'ENGINEERING-U00'


  def test11
    # assert @@lot = @@svtest.new_lot(sublottype: @@sublottype, erpitem: @@erpitem, stb: false), 'error creating test lot'
    comment = "ERPITEM=#{@@erpitem},DPML=1.5,PLANNED_FSD=#{Time.now.strftime('%F')}"
    assert @@lot = @@sv.new_lot_release(product: @@svtest.product, route: @@svtest.route, sublottype: @@sublottype, comment: comment)
    @@testlots << @@lot
    #
    assert @@cp.update_reports(30, @@feed)
    assert @@cptest.verify_plan_start_data(@@lot, @@sublottype, @@erpitem)
  end

end
