=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: Steffen Steidten, Thomas Klose, 2010-06-23

Version: 1.37

History:
  2013-10-21 ssteidte, code cleanup, introduced spread variable to separate the meaning of time values
  2018-03-06 sfrieske, replaced user_parameter_set_verify by user_parameter_change
  2019-02-12 sfrieske, use siviewtest for lot creation directly
=end

require_relative 'Test_CP'

# Test Common Platform data feed Lot Attribute Change
#
# List of attributes changes: CustomerLotGrade, CustomerQualityCode, SUB_LOT_TYPE, product2
#
# Requirements: http://myteamsmst/sites/MST/IPMO/prj/Common%20Platform%20Integration/Shared%20Documents/MFG%20TRACK/Requirement%20Details/CPDI_LOTATTRIB_requirements.xls
class Test_CP14 < Test_CP
  @@feed = 'lot_attribute_change'

  @@customer_lot_grade = '70'
  @@customer_quality_code = 'CC0'
  @@sublottype = 'PX'
  @@new_sublottype = 'EQ'
  @@product2 = '8901AB.A0'

  @@event_sleep = 400          # must be high enough for the MDS to pick up changes from the SiView history
  @@spread = @@event_sleep * 0.7 # account for slight timestamp uncertainties, must be less than @@sleeptime!


  def test00_setup
    $setup_ok = false
    #
    # create test lots
    uparams = {'CustomerPriority'=>'QA_Prio'}
    assert @@testlots = @@svtest.new_lots(14, user_parameters: uparams), 'error creating test lots'
    @@lot1, @@lot2, @@lot3, @@lot4, @@lot5, @@lot6, @@lot7, @@lot8, @@lot9, @@lot10, @@lot11, @@lot12, @@lot13, @@lot14 = @@testlots
    # move to operation before stage move
    @@testlots.each {|lot| assert_equal 0, @@sv.lot_opelocate(lot, '1000.1200')}
    #
    # set dummy values that are going to be changed
    assert_equal 0, @@sv.user_parameter_delete('Lot', @@lot1, 'ExpediteOrder', check: true)
    assert_equal 0, @@sv.user_parameter_change('Lot', @@lot2, 'ExpediteOrder', '')
    assert_equal 0, @@sv.user_parameter_change('Lot', @@lot3, 'ExpediteOrder', 'N')
    assert_equal 0, @@sv.user_parameter_change('Lot', @@lot4, 'ExpediteOrder', 'Y')
    assert_equal 0, @@sv.user_parameter_change('Lot', @@lot1, 'CustomerLotGrade', 'Dummy')
    assert_equal 0, @@sv.user_parameter_change('Lot', @@lot2, 'CustomerQualityCode', 'Dummy')
    assert_equal 0, @@sv.user_parameter_change('Lot', @@lot3, 'ERPItem', 'Dummy')

    assert_equal 0, @@sv.user_parameter_change('Lot', @@lot6, 'TurnkeyType', 'na')
    assert_equal 0, @@sv.user_parameter_change('Lot', @@lot7, 'TurnkeySubconIDAssembly', 'na')
    assert_equal 0, @@sv.user_parameter_change('Lot', @@lot8, 'TurnkeySubconIDBump', 'na')
    assert_equal 0, @@sv.user_parameter_change('Lot', @@lot9, 'TurnkeySubconIDFab', 'na')
    assert_equal 0, @@sv.user_parameter_change('Lot', @@lot10, 'TurnkeySubconIDFinalTest', 'na')
    assert_equal 0, @@sv.user_parameter_change('Lot', @@lot11, 'TurnkeySubconIDReflow', 'na')
    assert_equal 0, @@sv.user_parameter_change('Lot', @@lot12, 'TurnkeySubconIDSort', 'na')
    assert_equal 0, @@sv.user_parameter_change('Lot', @@lot13, 'TurnkeySubconIDSort2', 'na')
    assert_equal 0, @@sv.user_parameter_change('Lot', @@lot14, 'TurnKeyQA', 'na')

    @@sv.lot_mfgorder_change(@@lot4, sublottype: @@new_sublottype)
    assert_equal @@new_sublottype, @@sv.lot_info(@@lot4).sublottype, 'sublot type not set correctly'
    @@sv.schdl_change(@@lot5, product: @@svtest.product, route: @@sv.lot_info(@@lot5).route)
    assert_equal @@svtest.product, @@sv.lot_info(@@lot5).product, "error setting up #{@@lot5}"
    #
    # split lots, opecomplete required for the stage to get assigned in the MDS
    @@testlots.each {|lot|
      assert @@sv.lot_split(lot, 2), "error splitting lot #{lot}"
      assert @@sv.claim_process_lot(lot), "error processing lot #{lot}"
    }
    #
    $setup_ok = true
  end

  def test09_report
    $setup_ok = false
    #
    $log.info "waiting #{@@event_sleep} s to separate events"; sleep @@event_sleep
    @@change_time = Time.now
    assert_equal 0, @@sv.user_parameter_change('Lot', @@lot1, 'CustomerLotGrade', @@customer_lot_grade)
    assert_equal 0, @@sv.user_parameter_change('Lot', @@lot2, 'CustomerQualityCode', @@customer_quality_code)
    assert_equal 0, @@sv.user_parameter_change('Lot', @@lot3, 'ERPItem', 'ENGINEERING-U00') # dummy change

    assert_equal 0, @@sv.user_parameter_change('Lot', @@lot6, 'TurnkeyType', 'TurnkeyType')
    assert_equal 0, @@sv.user_parameter_change('Lot', @@lot7, 'TurnkeySubconIDAssembly', 'TurnkeySubconIDAssembly')
    assert_equal 0, @@sv.user_parameter_change('Lot', @@lot8, 'TurnkeySubconIDBump', 'TurnkeySubconIDBump')
    assert_equal 0, @@sv.user_parameter_change('Lot', @@lot9, 'TurnkeySubconIDFab', 'TurnkeySubconIDFab')
    assert_equal 0, @@sv.user_parameter_change('Lot', @@lot10, 'TurnkeySubconIDFinalTest', 'TurnkeySubconIDFinalTest')
    assert_equal 0, @@sv.user_parameter_change('Lot', @@lot11, 'TurnkeySubconIDReflow', 'TurnkeySubconIDReflow')
    assert_equal 0, @@sv.user_parameter_change('Lot', @@lot12, 'TurnkeySubconIDSort', 'TurnkeySubconIDSort')
    assert_equal 0, @@sv.user_parameter_change('Lot', @@lot13, 'TurnkeySubconIDSort2', 'TurnkeySubconIDSort2')
    assert_equal 0, @@sv.user_parameter_change('Lot', @@lot14, 'TurnKeyQA', 'TurnKeyQA')
    #
    ##assert_equal 0, @@sv.sublottype_change(@@lot4, @@sublottype) # does not work!
    @@sv.lot_mfgorder_change(@@lot4, sublottype: @@sublottype, customer: @@sv.lot_info(@@lot4).customer)
    assert_equal @@sublottype, @@sv.lot_info(@@lot4).sublottype, 'sublot type not set correctly'
    assert_equal 0, @@sv.lot_requeue(@@lot4)
    #
    @@sv.schdl_change(@@lot5, product: @@product2, route: @@svtest.route)
    assert_equal @@product2, @@sv.lot_info(@@lot5).product, "error setting up #{@@lot5}"
    assert_equal 0, @@sv.lot_requeue(@@lot5)
    #
    # must be high enough for the MDS to pick up changes from the SiView history
    assert @@cp.update_reports([@@event_sleep, @@sleeptime].max, @@feed), "loader error"
    #
    $setup_ok = true
  end

  def test11_CUR_LOT_GRADE
    assert @@cptest.verify_lot_attribute_change_data(@@lot1, @@change_time, :CUR_LOT_GRADE, @@customer_lot_grade, spread: @@spread)
  end

  def test12_CUR_QUALITY_CODE
    assert @@cptest.verify_lot_attribute_change_data(@@lot2, @@change_time, :CUR_QUALITY_CODE, @@customer_quality_code, spread: @@spread)
  end

  def test13_ORACLE_ITEM
    # was a dummy change, ERPItem itself is not reported
    assert @@cptest.verify_lot_attribute_change_data(@@lot3, @@change_time, nil, nil, spread: @@spread)
  end

  def test14_SUB_LOT_TYPE
    assert @@cptest.verify_lot_attribute_change_data(@@lot4, @@change_time, :SUB_LOT_TYPE, @@sublottype, spread: @@spread)
  end

  def test15_product2
    assert @@cptest.verify_lot_attribute_change_data(@@lot5, @@change_time, :PRODSPEC_ID, @@product2, spread: @@spread)
  end

  def test16_turnkey
    assert @@cptest.verify_lot_attribute_change_data(@@lot6, @@change_time, :TURNKEY_TYPE, 'TurnkeyType', spread: @@spread)
  end

  def test17_turnkey_subcon_bump
    assert @@cptest.verify_lot_attribute_change_data(@@lot7, @@change_time, :TURNKEY_SUBCON_ASSEMBLY, 'TurnkeySubconIDAssembly', spread: @@spread)
  end

  def test18_turnkey_subcon_id_assembly

    assert @@cptest.verify_lot_attribute_change_data(@@lot8, @@change_time, :TURNKEY_SUBCON_BUMP, 'TurnkeySubconIDBump', spread: @@spread)
  end

  def test19_turnkey_subcon_id_fab
    assert @@cptest.verify_lot_attribute_change_data(@@lot9, @@change_time, :TURNKEY_SUBCON_FAB, 'TurnkeySubconIDFab', spread: @@spread)
  end

  def test20_turnkey_subcon_id_final_test
    assert @@cptest.verify_lot_attribute_change_data(@@lot10, @@change_time, :TURNKEY_SUBCON_FINAL_TEST, 'TurnkeySubconIDFinalTest', spread: @@spread)
  end

  def test21_turnkey_subcon_id_reflow
    assert @@cptest.verify_lot_attribute_change_data(@@lot11, @@change_time, :TURNKEY_SUBCON_REFLOW, 'TurnkeySubconIDReflow', spread: @@spread)
  end

  def test22_turnkey_subcon_id_sort
    assert @@cptest.verify_lot_attribute_change_data(@@lot12, @@change_time, :TURNKEY_SUBCON_SORT, 'TurnkeySubconIDSort', spread: @@spread)
  end

  def test23_turnkey_subcon_id_sort2
    assert @@cptest.verify_lot_attribute_change_data(@@lot13, @@change_time, :TURNKEY_SUBCON_SORT2, 'TurnkeySubconIDSort2', spread: @@spread)
  end

  def test24_turnkey_subcon_turnkey_qa
    refute @@cptest.verify_lot_attribute_change_data(@@lot14, @@change_time, :TURNKEY_TYPEQA, false, spread: @@spread)
  end
end
