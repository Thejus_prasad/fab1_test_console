=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: sfrieske, 2017-07-17

Version: 1.47

History:
=end

load 'cptest.rb' # allow to re-load changed verification methods
require 'RubyTestCase'


# Test Wip Status loader for BackupOperation lots
class Test_CP01_Backup < RubyTestCase
  @@feed = 'wip_status'
  #
  @@env_src = 'f8stag'
  @@product = 'UT-PRODUCT-FAB1.01'
  @@route = 'UTRT101.01'
  @@backup_opNo = '1000.200'
  @@backup_bank = 'O-BACKUPOPER'
  #
  @@sleeptime = 310


  def self.startup
    @@svtest_src = SiView::Test.new(@@env_src, product: @@product, route: @@route)
    @@sv_src = @@svtest_src.sv
    @@sv_back = SiView::MM.new($env)
    @@cptest = CPTest.new($env, sv: @@sv_back)
    @@cp = @@cptest.cp
    #
    @@testlots = []
  end

  def self.after_all_passed
    @@testlots.each {|lot| @@sv_src.backup_delete_lot_family(lot, @@sv_back)}
  end


  def test09_loader
    # create test lot at src site and send it to backup
    assert lot = @@svtest_src.new_lot
    @@testlots << lot
    assert @@sv_src.backup_prepare_send_receive(lot, @@sv_back, backup_opNo: @@backup_opNo, backup_bank: @@backup_bank)
    # need to set script parameters on behalf of FabGUI receiving
    assert_equal 0, @@sv_back.user_parameter_change('Lot', lot,
      {'SourceFabCode'=>'F8', 'ERPItem'=>'QATestItem', 'CustomerPriority'=>'QACustomerPrio', 'Purpose'=>'QATestPurpose'})
    #
    # start loader and verify output
    assert @@cp.update_reports(@@sleeptime, @@feed, runargs: "-PRODSPEC_ID #{@@product}")
    assert @@cptest.verify_wip_status_data(lot)
  end

end
