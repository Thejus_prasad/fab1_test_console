=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.


Author: Steffen Steidten, 2011-01-12

Version: 1.37

History:
  2018-03-06 sfrieske, replaced user_parameter_set_verify by user_parameter_change
  2018-05-09 sfrieske, minor adjustments for scrapped lots with AutoScrap jCAP job interfering
  2019-02-12 sfrieske, use siviewtest for lot creation directly
  2021-08-31 sfrieske, requiring 'time' for #iso8601
=end

require 'time'
require_relative 'Test_CP'


# Test Common Platform data feed Rework
# Requirements: http://myteamsmst/sites/MST/IPMO/prj/Common%20Platform%20Integration/Shared%20Documents/MFG%20TRACK/Requirement%20Details/CPDI_REWORK_requirements.xlsx
class Test_CP21 < Test_CP
  @@feed = 'rework'
  @@loader = 'lot_rework'

  @@slt_ignore = 'Equipment Monitor'
  @@product_rwfeed = 'A-CP-PRODUCT-RWFEED.01'
  @@route_rwfeed = 'A-CP-RWFEED.01'

  @@rework_litho1 = 'R-L-CPRW-LITH.01'
  @@rework_litho2 = 'R-L-CPNRW-LITH.01'
  @@rework_cmp = 'RP-CPRW-CMP.01'
  @@rework_dv = 'A-CP-RWPOLYDV.01'
  @@rework_fv = 'A-CP-RWPOLYFV.01'
  @@rework_lv = 'A-CP-RWPOLYLV.01'


  def test00_setup
    $setup_ok = false
    #
    # create test lots  ##, sublottype not PROD or QUAL due to AutoScrap jCAP job
    # lot1 -> litho RW
    # lot2 -> litho RW w/ NOWRK
    # lot3 -> cmp RW w/ split
    # lot4 -> cmp RW
    # lot5 -> C4B COAT
    # lot6 -> C4B EXPOSE
    # lot7 -> C4B DEVELOP
    # lot8 -> rework_cancel
    # lot9 -> wrong sublot type
    # lota -> scrap in rework operation
    assert @@testlots = @@svtest.new_lots(10, product: @@product_rwfeed, route: @@route_rwfeed), 'error creating test lots'
    @@testlots.each {|lot|
      assert_equal 0, @@sv.user_parameter_change('Lot', lot, 'ExpediteOrder', 'Y')
    }
    @@lot1, @@lot2, @@lot3, @@lot4, @@lot5, @@lot6, @@lot7, @@lot8, @@lot9, @@lota, @@lotb = @@testlots

    @@rework_time = Time.now
    assert_equal 0, @@sv.sublottype_change(@@lot9, @@slt_ignore)
    #
    # lots 1, 2, 8, 9, a
    2.times {
      assert @@sv.claim_process_lot(@@lot1)
      assert @@sv.claim_process_lot(@@lot2)
      assert @@sv.claim_process_lot(@@lot8)
      assert @@sv.claim_process_lot(@@lot9)
      assert @@sv.claim_process_lot(@@lota)
    }
    assert_equal 0, @@sv.lot_rework(@@lot1, @@rework_litho1, return_opNo: '1000.1000', memo: @@rework_time.iso8601)
    assert_equal 0, @@sv.lot_rework(@@lot2, @@rework_litho2, return_opNo: '1000.1000', memo: @@rework_time.iso8601)
    assert_equal 0, @@sv.lot_rework(@@lot8, @@rework_litho1, return_opNo: '1000.1000', memo: @@rework_time.iso8601)
    assert_equal 0, @@sv.lot_rework(@@lot9, @@rework_litho1, return_opNo: '1000.1000', memo: @@rework_time.iso8601)
    assert_equal 0, @@sv.lot_rework(@@lota, @@rework_litho1, return_opNo: '1000.1000', memo: @@rework_time.iso8601)
    assert_equal 0, @@sv.lot_rework_cancel(@@lot8)
    # scrap @@lota
    assert @@sv.claim_process_lot(@@lota)
    @@lia = @@sv.lot_info(@@lota)
    assert_equal 0, @@sv.scrap_wafers(@@lota)
    # lots 1, 2, 8, 9
    5.times {
      assert @@sv.claim_process_lot(@@lot1)
      assert @@sv.claim_process_lot(@@lot2)
      assert @@sv.claim_process_lot(@@lot8)
      assert @@sv.claim_process_lot(@@lot9)
    }
    # lots 3, 4
    5.times {
      assert @@sv.claim_process_lot(@@lot3)
      assert @@sv.claim_process_lot(@@lot4)
    }
    assert @@lot3c = @@sv.lot_partial_rework(@@lot3, 10, @@rework_cmp, return_opNo: '1200.1000', memo: @@rework_time.iso8601)
    assert_equal 0, @@sv.lot_rework(@@lot4, @@rework_cmp, return_opNo: '1200.1000', memo: @@rework_time.iso8601)
    2.times {
      assert @@sv.claim_process_lot(@@lot3c)
      assert @@sv.claim_process_lot(@@lot4)
    }
    1.times {
      assert @@sv.claim_process_lot(@@lot3)
      assert @@sv.claim_process_lot(@@lot4)
    }
    assert_equal 0, @@sv.lot_merge(@@lot3, @@lot3c)
    # lots 5, 6, 7
    assert_equal 0, @@sv.lot_opelocate(@@lot5, '1200.1000')
    assert_equal 0, @@sv.lot_opelocate(@@lot6, '1300.1000')
    assert_equal 0, @@sv.lot_opelocate(@@lot7, '1300.1000')
    2.times {assert @@sv.claim_process_lot(@@lot5)}
    2.times {assert @@sv.claim_process_lot(@@lot6)}
    5.times {assert @@sv.claim_process_lot(@@lot7)}
    assert_equal 0, @@sv.lot_rework(@@lot5, @@rework_dv, return_opNo: '1300.1000', memo: @@rework_time.iso8601)
    2.times {assert @@sv.claim_process_lot(@@lot5)}
    assert_equal 0, @@sv.lot_rework(@@lot6, @@rework_fv, return_opNo: '1400.1000', memo: @@rework_time.iso8601)
    # split lot 6 to test issue 111
    1.times {assert @@sv.claim_process_lot(@@lot6)}
    assert @@lot6c = @@sv.lot_split(@@lot6, 5)
    2.times {assert @@sv.claim_process_lot(@@lot6)}
    2.times {assert @@sv.claim_process_lot(@@lot6c)}
    # rework lot 7
    assert_equal 0, @@sv.lot_rework(@@lot7, @@rework_lv, return_opNo: '1500.1000', memo: @@rework_time.iso8601)
    3.times {assert @@sv.claim_process_lot(@@lot7)}
    #
    $setup_ok = true
  end

  def test09_report
    $setup_ok = false
    #
    $log.info "waiting #{@@sleeptime} s before running the #{@@loader} loader"; sleep @@sleeptime
    assert @@cp.run_loader(@@loader, "-PRODSPEC_ID #{@@product_rwfeed}")
    assert @@cp.get_reports(@@feed)
    #
    $setup_ok = true
  end


  def test21_lot1
    assert @@cptest.verify_rework(@@lot1, 'Litho', additional_rwk_ops(@@lot1), @@rework_time)
  end

  def test25_noreport_norework_route
    records = @@cp.reports[@@feed].get_records_lot(@@lot2, tfield: 'CLAIM_MEMO', t: @@rework_time)
    assert_empty records, "lot #{@@lot2} must not be reported (NOREWORK route)"
  end

  def test26_noreport_rework_cancel
    records = @@cp.reports[@@feed].get_records_lot(@@lot8, tfield: 'CLAIM_MEMO', t: @@rework_time)
    assert_empty records, "lot #{@@lot8} must not be reported (rework cancel)"
  end

  def test27_noreport_sublottype
    records = @@cp.reports[@@feed].get_records_lot(@@lot9, tfield: 'CLAIM_MEMO', t: @@rework_time)
    assert_empty records, "lot #{@@lot9} must not be reported (SLT)"
  end


  def test30_lot3
    assert @@cptest.verify_rework(@@lot3c, 'CMP', additional_rwk_ops(@@lot3c), @@rework_time, qty: '10')
  end

  def test35_lot4
    assert @@cptest.verify_rework(@@lot4, 'CMP', additional_rwk_ops(@@lot4), @@rework_time)
  end

  def test40_lot5
    assert @@cptest.verify_rework(@@lot5, 'Polyimide', additional_rwk_ops(@@lot5), @@rework_time)
  end

  def test45_lot6
    assert @@cptest.verify_rework(@@lot6, 'Polyimide', additional_rwk_ops(@@lot6), @@rework_time, qty: 'nocheck')
  end

  def test46_lot6c
    hist = [@@sv.lot_operation_list_fromhistory(@@lot6c).last]
    assert @@cptest.verify_rework(@@lot6c, 'Polyimide', hist, @@rework_time, qty: 'nocheck')
  end

  def test50_lot7
    assert @@cptest.verify_rework(@@lot7, 'Polyimide', additional_rwk_ops(@@lot7), @@rework_time)
  end

  def test55_lota
    # note: an invalid record is found due to scrap_cancel by the AutoScrap jCAP job
    hist = @@sv.lot_operation_list_fromhistory(@@lota).take(2)
    assert @@cptest.verify_rework(@@lota, 'Litho', hist, @@rework_time, lotinfo: @@lia)
  end


  # aux methods

  # # obsolete, remove!
  # def additional_rwk_opsOLD(lot)
  #   # get operations additionally executed due to rework
  #   li = @@sv.lot_info(lot)
  #   mainroute = li.route
  #   mainops = []
  #   rwkroute = nil
  #   rwend = nil
  #   rwstart = nil
  #   hist = @@sv.lot_operation_list_fromhistory(lot)
  #   # traversing history backwards
  #   hist.each_with_index {|h, i|
  #     if rwend
  #       # end of rework found, search start operation
  #       if h.route == mainroute and !mainops[0..-2].member?(h.opNo)
  #         rwstart = h.opNo == mainops.last ? i : i - 1
  #         break
  #       end
  #     else
  #       # searching start of rework
  #       if h.route == mainroute
  #         mainops << h.opNo
  #       else
  #         rwend = i
  #         rwkroute = h.route
  #       end
  #     end
  #   }
  #   return hist[rwend..rwstart]
  # end

  def additional_rwk_ops(lot)
    # get operations additionally executed due to rework
    li = @@sv.lot_info(lot)
    mainroute = li.route
    mainops = []
    rwkroute = nil
    rwend = nil
    rwstart = nil
    hist = @@sv.lot_operation_list_fromhistory(lot)
    # traversing history backwards
    hist.each_with_index {|h, i|
      if rwend
        # end of rework found, search start operation
        if h.routeID.identifier == mainroute and !mainops[0..-2].member?(h.operationNumber)
          rwstart = h.operationNumber == mainops.last ? i : i - 1
          break
        end
      else
        # searching start of rework
        if h.routeID.identifier == mainroute
          mainops << h.operationNumber
        else
          rwend = i
          rwkroute = h.routeID.identifier
        end
      end
    }
    return hist[rwend..rwstart]
  end

end
