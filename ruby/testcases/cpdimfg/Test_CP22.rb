=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: Steffen Steidten, Thomas Klose, 2011-02-23
Version: 1.43
=end

require_relative 'Test_CP'

# Test Common Platform data feed '3C Source Product'.
#
# Requirements: http://myteamsmst/sites/MST/IPMO/prj/Common%20Platform%20Integration/Shared%20Documents/MFG%20TRACK/Requirement%20Details/3C_SOURCE_PRODUCTS_Requirements.xlsx
class Test_CP22 < Test_CP
  @@feed = 'source_products'

  @@prod1 = '2110BG.D1'
  @@prod2 = '3260CAB.E3'
  @@prod3 = 'TESTEIAA.00'


  def test09_report
    assert $setup_ok = @@cp.update_reports(1, @@feed)
  end

  def test11_product1
    assert @@cptest.verify_source_product(@@prod1)
  end

  def test12_product2
    assert @@cptest.verify_source_product(@@prod2)
  end

  def test13_product3
    assert @@cptest.verify_source_product(@@prod3)
  end

  def test95_compare_record_count
    assert @@cp.compare_record_count(@@feed), 'wrong record count'
  end

end
