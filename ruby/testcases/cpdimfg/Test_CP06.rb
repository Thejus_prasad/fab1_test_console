=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: Steffen Steidten, Thomas Klose, 2010-06-23

Version: 1.45

History:
  2018-03-06 sfrieske, replaced user_parameter_set_verify by user_parameter_change
  2019-02-12 sfrieske, use siviewtest for lot creation directly
=end

require_relative 'Test_CP'

# Test Common Platform data feed WIP_ATTRIBUTE.
#
# Requirements: http://myteamsmst/sites/MST/IPMO/prj/Common%20Platform%20Integration/Shared%20Documents/MFG%20TRACK/Requirement%20Details/CPDI_WIPATTRIB_requirements.xlsx
class Test_CP06 < Test_CP
  @@feed = 'wip_attribute'


  def test09_loader
    $setup_ok = false
    #
    assert @@testlots = @@svtest.new_lots(2)
    # bankin lot2
    assert_equal 0, @@sv.lot_opelocate(@@testlots[1], :last)
    @@sv.lot_bankin(@@testlots[1]) unless @@sv.lot_info(@@testlots[1]).status == 'COMPLETED'
    assert_equal 'COMPLETED', @@sv.lot_info(@@testlots[1]).status, "error preparing lot #{@@testlots[1]}"
    # set script parameters
    @@testlots.each_with_index {|lot, i|
      assert_equal 0, @@sv.user_parameter_change('Lot', lot, 'ERFID', 'e500QA')
      assert_equal 0, @@sv.user_parameter_change('Lot', lot, 'PPCD', 'PPCD1 PPCD2')
      assert_equal 0, @@sv.user_parameter_change('Lot', lot, 'TurnkeyType', 'J')
      assert_equal 0, @@sv.user_parameter_change('Lot', lot, 'TurnkeySubconIDSort', 'GG')
      assert_equal 0, @@sv.user_parameter_change('Lot', lot, 'TurnkeySubconIDBump', 'AT')
      assert_equal 0, @@sv.user_parameter_change('Lot', lot, 'GateCD_Target_PLN', 'GCDTPLN')
      assert_equal 0, @@sv.user_parameter_change('Lot', lot, 'CustomerLotGrade', 'CLG')
      assert_equal 0, @@sv.user_parameter_change('Lot', lot, 'CustomerQualityCode', 'CQC')
      assert_equal 0, @@sv.user_parameter_change('Lot', lot, 'ShipLabelNote', "SL\nN")
      assert_equal 0, @@sv.user_parameter_change('Lot', lot, 'ExpediteOrder', {0=>'', 1=>'Y'}[i])
    }
    #
    # start loader and get output
    assert @@cp.update_reports(@@sleeptime, @@feed, runargs: "-PRODSPEC_ID #{@@svtest.product}")
    # NewLines are not reported correctly, change script parameter to the expected value
    @@testlots.each {|lot| @@sv.user_parameter_change('Lot', lot, 'ShipLabelNote', 'SL N')}
    #
    $setup_ok = true
  end

  def test11_wip_attribute_Waiting
    assert @@cptest.verify_wip_attribute_data(@@testlots[0])
  end

  def test12_wip_attribute_COMPLETED
    assert @@cptest.verify_wip_attribute_data(@@testlots[1])
  end

end
