=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: Steffen Steidten, Thomas Klose, 2010-06-23

Version: 1.20

History
  2014-04-10 ssteidte, code cleanup
  2014-12-05 ssteidte, create test lots on the fly
  2016-10-24 sfrieske, minor code maintenance
  2019-02-12 sfrieske, use siviewtest for lot creation directly
=end

require_relative 'Test_CP'


# Test Common Platform data feed LOT_HOLD_RELEASE.
#
# Requirements: http://myteamsmst/sites/MST/IPMO/prj/Common%20Platform%20Integration/Shared%20Documents/MFG%20TRACK/Requirement%20Details/CPDI_HOLDRELEASE_requirements.xlsx
class Test_CP07 < Test_CP
  @@feed = 'lot_hold_release'

  @@hold_reason = 'X-S1'
  @@rte_branch = 'A-CP-BRANCH-MAINPD.01'
  @@opNo_branch = '1100.1000'
  @@opNo_ret = '1200.1100'


  def test09_report
    $setup_ok = false
    #
    uparams = {'CustomerPriority'=>'QA_Prio'}
    assert @@testlots = @@svtest.new_lots(7, user_parameters: uparams), 'error creating test lots'
    @@lot1, @@lot2, @@lot3, @@lot4, @@lot5, @@lot6, @@lot7 = @@testlots
    #
    # opecomp to make current values visible
    @@testlots.each {|lot|
      assert @@sv.claim_process_lot(lot)
    }
    #
    # lot hold
    assert_equal 0, @@sv.lot_hold(@@lot1, @@hold_reason)
    #
    # split (and process child)
    @@lot2c = @@sv.lot_split(@@lot2, 5)
    assert @@sv.claim_process_lot(@@lot2c)
    assert_equal 0, @@sv.lot_hold(@@lot2, @@hold_reason)
    assert_equal 0, @@sv.lot_hold(@@lot2c, @@hold_reason)
    #
    # release the holds
    assert_equal 0, @@sv.lot_hold_release(@@lot1, @@hold_reason)
    assert_equal 0, @@sv.lot_hold_release(@@lot2, @@hold_reason)
    assert_equal 0, @@sv.lot_hold_release(@@lot2c, @@hold_reason)
    #
    # future hold (must fail)
    assert_equal 0, @@sv.lot_futurehold(@@lot3, '1200.1200', @@hold_reason)
    assert_equal 0, @@sv.lot_futurehold_cancel(@@lot3, nil)
    @@hold3_time = Time.now
    #
    # bank hold (must fail)
    @@sv.lot_opelocate(@@lot4, :last)
    @@sv.lot_bankhold(@@lot4)
    @@sv.lot_bankhold_release(@@lot4)
    @@sv.lot_bankin_cancel(@@lot4)
    @@sv.lot_opelocate(@@lot4, :first)  # prevent new bankin
    @@hold4_time = Time.now
    #
    # set merge hold
    lot = @@lot6
    assert_equal 0, @@sv.lot_experiment(lot, 5, @@rte_branch, @@opNo_branch, @@opNo_ret)
    assert_equal 0, @@sv.lot_opelocate(lot, @@opNo_branch, offset: -1)
    assert @@sv.claim_process_lot(lot), 'error processing lot'
    @@lot6c = @@sv.lot_family(lot).last
    refute_equal @@lot6,  @@lot6c
    # merge, releasing hold
    @@sv.lot_family(lot).each {|c| assert_equal 0, @@sv.lot_opelocate(c, @@opNo_ret)}
    assert_equal 0, @@sv.lot_merge(lot, @@lot6c)
    #
    # running hold with hold release and opestart_cancel
    lot = @@lot7
    assert @@sv.claim_process_lot(lot, running_hold: true, opestart_cancel: true) {|cj|
      @@sv.lot_hold_release(lot, 'RNHL')
    }
    #
    # run the report
    assert @@cp.update_reports(@@sleeptime, @@feed)
    #
    $setup_ok = true
  end


  def test11_hold
    assert @@cptest.verify_lot_hold_release_data(@@lot1, 'LotHold', @@hold_reason)
  end

  def test12_release
    assert @@cptest.verify_lot_hold_release_data(@@lot1,'LotHoldRelease', @@hold_reason)
  end

  def test21_split_lot
    assert @@cptest.verify_lot_hold_release_data(@@lot2, 'LotHold', @@hold_reason)
  end

  def test21_split_lot_child
    assert @@cptest.verify_lot_hold_release_data(@@lot2c,'LotHold',  @@hold_reason)
  end

  def test22_split_lot
    assert @@cptest.verify_lot_hold_release_data(@@lot2, 'LotHoldRelease', @@hold_reason)
  end

  def test22_split_lot_child
    assert @@cptest.verify_lot_hold_release_data(@@lot2c, 'LotHoldRelease', @@hold_reason)
  end

  def test31_no_futurehold
    records = @@cp.reports[@@feed].get_records_lot(@@lot3, timestamp: @@hold3_time)
    assert_equal 0, records.size, "wrong report of a FUTURE HOLD"
  end

  def test41_no_bankhold
    records = @@cp.reports[@@feed].get_records_lot(@@lot4, timestamp: @@hold4_time)
    assert_equal 0, records.size, "wrong report of a BANKHOLD"
  end

  def test61_mergehold
    assert @@cptest.verify_lot_hold_release_data(@@lot6, 'LotHold', 'MGHL', nwafers: 20)
  end

  def test61_mergehold_child
    assert @@cptest.verify_lot_hold_release_data(@@lot6c, 'LotHold', 'MGHL', nwafers: 5) # last valid
  end

  def test62_mergehold_release
    assert @@cptest.verify_lot_hold_release_data(@@lot6, 'LotHoldRelease', 'MGHL', nwafers: 20)
  end

  def test62_mergehold_release_child
    assert @@cptest.verify_lot_hold_release_data(@@lot6c, 'LotHoldRelease', 'MGHL', nwafers: 5)
  end

  def test71_runninghold
    assert @@cptest.verify_lot_hold_release_data(@@lot7, 'RunningHold', 'RNHL')
  end

  def test72_runninghold_release
    assert @@cptest.verify_lot_hold_release_data(@@lot7, 'LotHoldRelease', 'RNHL')
  end

  def test98_warn
    $log.warn 'tests 61 and 62 may fail due to issues with MDS entry ordering'
  end

end
