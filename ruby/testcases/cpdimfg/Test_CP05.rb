=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: Steffen Steidten, Thomas Klose, 2010-06-23

Version: 1.43

History:
  2018-03-06 sfrieske, replaced user_parameter_set_verify by user_parameter_change
  2019-02-12 sfrieske, use siviewtest for lot creation directly
=end

require_relative 'Test_CP'


# Test Common Platform data feed Fab Out.
#
# Requirements: http://myteamsmst/sites/MST/IPMO/prj/Common%20Platform%20Integration/Shared%20Documents/MFG%20TRACK/Requirement%20Details/CPDI_FABOUT_requirements.xlsx
# MSR 976214 (v1.43)
class Test_CP05 < Test_CP
  @@feed = 'fab_out'

  @@route = 'A-CP-FABOUT.01'
  @@product = 'A-CP-FABOUT.01'
  @@op_fabout = 'FAB1-OUT.01'  # GatePass, OpeComp or ForceComp required


  def test09_report
    $setup_ok = false
    #
    # create new lots
    assert @@testlots = @@svtest.new_lots(4, product: @@product, route: @@route), "error creating test lots"
    @@testlots.each_with_index {|lot, i|
      assert_equal 0, @@sv.user_parameter_change('Lot', lot, 'ExpediteOrder', {1=>'', 2=>'N', 3=>'Y'}[i]) if i > 0
      assert_equal 0, @@sv.lot_opelocate(lot, nil, op: @@op_fabout)
      assert_equal 0, @@sv.lot_gatepass(lot)
    }
    #
    # start loader and get output
    assert @@cp.update_reports(@@sleeptime, @@feed)
    #
    $setup_ok = true
  end

  def test11_fab_out0
    assert @@cptest.verify_fabout_data(@@testlots[0])
  end

  def test11_fab_out1
    assert @@cptest.verify_fabout_data(@@testlots[1])
  end

  def test11_fab_out2
    assert @@cptest.verify_fabout_data(@@testlots[2])
  end

  def test11_fab_out3
    assert @@cptest.verify_fabout_data(@@testlots[3])
  end

end
