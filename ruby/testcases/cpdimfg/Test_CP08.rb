=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: Steffen Steidten, Thomas Klose, 2010-06-23

Version: 1.20

History:
  2019-02-12 sfrieske, use siviewtest for lot creation directly
=end

require_relative 'Test_CP'


# Test Common Platform data feed ETest Event.
#
# Requirements: http://myteamsmst/sites/MST/IPMO/prj/Common%20Platform%20Integration/Shared%20Documents/MFG%20TRACK/Requirement%20Details/CPDI_ETEST-EVENT_requirements.xlsx
class Test_CP08 < Test_CP
  @@feed = 'etest_event'
  @@opNo = '1200.1100'    # pre GatePass operation


  def test09_report
    assert @@lot = @@svtest.new_lot, 'error creating test lot'
    @@testlots << @@lot
    # GatePass
    @@tstamp = Time.now
    assert_equal 0, @@sv.lot_opelocate(@@lot, @@opNo)
    assert @@sv.claim_process_lot(@@lot)
    assert_equal 0, @@sv.lot_gatepass(@@lot)
    # start loader and get output
    assert @@cp.update_reports(60, @@feed)
    # verify data
    assert @@cptest.verify_etest_event_data(@@lot, @@tstamp)
  end

end
