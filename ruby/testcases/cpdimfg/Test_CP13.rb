=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: Steffen Steidten, Thomas Klose, 2010-06-23

Version: 1.37

History:
  2018-03-06 sfrieske, replaced user_parameter_set_verify by user_parameter_change
  2019-02-12 sfrieske, use siviewtest for lot creation directly
=end

require_relative 'Test_CP'


# Test Common Platform data feed Lot Ship Details
#
# Requirements: http://myteamsmst/sites/MST/IPMO/prj/Common%20Platform%20Integration/Shared%20Documents/MFG%20TRACK/Requirement%20Details/CPDI_SHIPDETAILS_requirements.xls
class Test_CP13 < Test_CP
  @@feed = 'lot_ship_details'
  @@claim_memo = 'TestXX|<>'


  def test00_setup
    $setup_ok = false
    #
    # create test lot
    uparams = {
      'CustomerLotGrade'=>'70', 'CustomerPriority'=>'QA_Prio', 'CustomerQualityCode'=>'CCO', 'ExpediteOrder'=>'Y', 
      'TurnkeyType'=>'J', 'TurnkeySubconIDBump'=>'AT', 'TurnkeySubconIDSort'=>'AK'
    }
    assert @@lot = @@svtest.new_lot(user_parameters: uparams), 'error creating test lot'
    @@testlots << @@lot
    @@sv.lot_info(@@lot, wafers: true).wafers.each_with_index {|w, i|
      assert_equal 0, @@sv.user_parameter_change('Wafer', w.wafer, 'DieCountGood', i)
    }
    #
    $setup_ok = true
  end

  def test09_reports
    # bank in, split and ship parent
    assert_equal 0, @@sv.lot_opelocate(@@lot, :last)  # automatic bank in
    assert @@lotc = @@sv.lot_split(@@lot, 2)
    assert_equal 0, @@sv.lot_ship(@@lot, nil, memo: @@claim_memo)
    @@ship_time = Time.now
    #
    # start loader and get output
    assert @@cp.update_reports(@@sleeptime, @@feed)   # not necessary, runs fast: , runargs: "-PRODSPEC_ID #{li.product}")
  end

  def test11_shipped_lot_details
    assert @@cptest.verify_lot_ship_details_data(@@lot, @@ship_time)
    # clean up
    @@sv.delete_lot_family(@@lot)
  end

  def test12_child_not_reported
    assert_equal 0, @@cp.reports[@@feed].get_records_lot(@@lotc).size, "wrong lot #{@@lotc} in report"
  end

end
