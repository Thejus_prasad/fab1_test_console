=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: Steffen Steidten, Thomas Klose, 2010-09-23

Version: 1.6.0

History:
  2018-03-06 sfrieske, replaced user_parameter_set_verify by user_parameter_change
  2019-02-12 sfrieske, use siviewtest for lot creation directly
=end

require_relative 'Test_CP'


# Test Common Platform data feed Turnky Subcon Lot Transit
class Test_CP16 < Test_CP
  @@feed = 'tk_subcon_alert'
  @@sv_defaults = {route: 'C02-TKEY.01', product: 'TKEY01.A0', carriers: '9%', carrier_category: nil}
  @@erpitem =  'DRAW12MC-JB1'  # must have an MDM entry for customer/erpitem combination
  @@tkparams = {'TurnkeyType'=>'J', 'TurnkeySubconIDBump'=>'SCC', 'TurnkeySubconIDSort'=>'USC'}

  @@rtebump = 'TKEY-BUMP.01'
  @@brbump = '6250.9000'
  @@retbump = '6350.1000'

  @@rtesort = 'TKEY-SORT.01'
  @@brsort = '6350.1000'
  @@retsort = '6500.1000'

  @@event_sleep = 60


  def test09_report
    $setup_ok = false
    #
    assert @@testlots = @@svtest.new_lots(2, erpitem: @@erpitem, user_parameters: @@tkparams), 'error creating test lots'
    #
    @@lot1, @@lot2 = @@testlots
    assert_equal 0, @@sv.lot_experiment(@@lot1, 25, @@rtebump, @@brbump, @@retbump)
    assert_equal 0, @@sv.lot_opelocate(@@lot1, @@brbump)
    @@tbranch1 = Time.now
    #
    assert_equal 0, @@sv.lot_experiment(@@lot2, 25, @@rtesort, @@brsort, @@retsort)
    assert_equal 0, @@sv.lot_opelocate(@@lot2, @@brsort)
    @@tbranch2 = Time.now
    #
    # process lots
    $log.info "sleeping #{@@event_sleep} s to separate events"; sleep @@event_sleep
    @@tproc = Time.now
    @@testlots.each {|lot| assert @@sv.claim_process_lot(lot), 'error processing lot'}
    #
    assert @@cp.update_reports(@@sleeptime, @@feed)
    #
    $setup_ok = true
  end

  def test11_branch
    assert @@cptest.verify_tk_subcon_alert_data(@@lot1, @@tbranch1, etype: 'LOT_SUBCON_TRANSIT_WAIT')
  end

  def test12_process
    assert @@cptest.verify_tk_subcon_alert_data(@@lot1, @@tproc, etype: 'LOT_SUBCON_TRANSIT_RUN')
  end

  def test21_branch
    assert @@cptest.verify_tk_subcon_alert_data(@@lot2, @@tbranch2, etype: 'LOT_SUBCON_TRANSIT_WAIT')
  end

  def test22_process
    assert @@cptest.verify_tk_subcon_alert_data(@@lot2, @@tproc, etype: 'LOT_SUBCON_TRANSIT_RUN')
  end

  def test52_compare_record_count
    assert @@cp.compare_record_count(@@feed), 'wrong record count'
  end

end
