=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: Steffen Steidten, Thomas Klose, 2010-06-23
Version: 1.20
=end

require_relative 'Test_CP'


# Test Common Platform data feed Reticle vs Runsheet.
class Test_CP02 < Test_CP
  # does not depend on Test_CP01, it runs the wip_status loader (with product filter)
  @@feed = 'reticle_runsheet'
  @@fab = 'FAB1'
  @@customer = 'gf'
  @@reticles = [['CA', '4191ADUMMY', '4191ADUMMY1'],
                ['01', '8091AK001B', '8091AK001B1'],
                ['01', '8091AK005A', '8091AK005A1'],
                ['UA', '4191ADUMMY', '4191ADUMMY1'],
                ['UA', '4191AQ0FVA', '4191AQ0FVA1'],
                ['DLCOATONLY', 'UTDUMMYGROUP', 'UTDUMMYGROUP1']]


  def XXXtest00_setup
    # get reticle set of the product
    assert pinfo = @@cptest.sm.object_info(:product, @@svtest.product).first
    # get photolayer to reticle group mapping from reticle set
    assert rset = @@cptest.sm.object_info(:reticleset, pinfo.specific[:reticleset]).first
    # get photolayers of the route's operations
    assert rinfo = @@cptest.sm.object_info(:mainpd, @@svtest.route).first
    # ... find reticle to be used ???
  end

  def test09_report
    $setup_ok = false
    #
    # start loader and get output
    @@cp.update_reports(1, 'wip_status', runargs: "-PRODSPEC_ID #{@@svtest.product}")
    assert @@cp.get_reports(@@feed), 'no message'
    #
    $setup_ok = true
  end

  def test11_reticle_runsheet
    assert @@cptest.verify_reticle_runsheet_data(1, @@fab, @@svtest.product, @@customer, @@reticles[0])
  end

  def test12_reticle_runsheet
    assert @@cptest.verify_reticle_runsheet_data(2, @@fab, @@svtest.product, @@customer, @@reticles[1])
  end

  def test13_reticle_runsheet
    assert @@cptest.verify_reticle_runsheet_data(3, @@fab, @@svtest.product, @@customer, @@reticles[2])
  end

  def test14_reticle_runsheet
    assert @@cptest.verify_reticle_runsheet_data(4, @@fab, @@svtest.product, @@customer, @@reticles[3])
  end

end
