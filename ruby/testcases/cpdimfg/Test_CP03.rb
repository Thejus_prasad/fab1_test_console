=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: Steffen Steidten, Thomas Klose, 2010-06-23

Version: 1.45.2
=end

require_relative 'Test_CP'

# Test Common Platform data feed "Part".
#
# Requirements: http://myteamsmst/sites/MST/IPMO/prj/Common%20Platform%20Integration/Shared%20Documents/MFG%20TRACK/Requirement%20Details/CPDI_LOTSTART_requirements.xlsx
class Test_CP03 < Test_CP
  @@feed = 'part'
  @@product = 'SHELBY21AE.B4' # 'ASMVIEW.A0'
  @@product_udata = 'SHELBY31AB.A3'
  @@customer = 'gf'


  def test11_part
    assert @@cp.update_reports(1, @@feed)
    assert @@cptest.verify_part_data(@@product, @@customer)
    assert @@cp.compare_record_count(@@feed), "wrong record count"
  end

  def test12_part_udata
    assert @@cp.get_reports('part_udata')
    assert @@cptest.verify_part_udata_data(@@product_udata)
  end

end
