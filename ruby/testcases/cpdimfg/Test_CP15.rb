=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: Steffen Steidten, Thomas Klose, 2010-06-23

Version: 1.38
=end

require_relative 'Test_CP'

# Test Common Platform data feed Runsheet.
#
# Requirements: http://myteamsmst/sites/MST/IPMO/prj/Common%20Platform%20Integration/Shared%20Documents/MFG%20TRACK/Requirement%20Details/CPDI_RUNSHEET_requirements.xlsx
class Test_CP15 < Test_CP
  # does not depend on Test_CP01, if mask_current.dat is missing wip_status is run as part of the test
  @@feed = 'runsheet'

  @@fullrun = false   # set to true for special tests only
  ##@@feed_seq = @@fullrun ? 22 : 0   ## what happens to msg sequence on fullrun??


  def test09_report
    $log.info "cleaning AbInitio status for runsheet"
    @@cp.lserver.exec!(". #{@@cp._loader_env}; export PATH=$PATH:/usr/local/pkg/abinitio/prod/bin; m_rm $AI_MFS_STATIC/seed_in.dat; m_touch $AI_MFS_STATIC/seed_in.dat; rm $AI_SERIAL_STATIC_RUN/runsheet.lock")
    res = @@cp.lserver.exec!(". #{@@cp._loader_env}; ls -l $AI_SERIAL_STATIC/mask_current.dat")
    if res.include?('No such file or directory')
      $log.info "need to run wip_status"
      @@cp.run_loader('wip_status', "-PRODSPEC_ID #{@@svtest.product}")
      res = @@cp.lserver.exec!(". #{@@cp._loader_env}; ls -l $AI_SERIAL_STATIC/mask_current.dat")
      assert !res.include?('No such file or directory'), res
    end
    # run loader and get report, normally filtered by the product
    # params = {feed: "#{@@feed}_#{@@feed_seq}", msg_type: @@cp.msg_types['runsheet']}
    # params[:args] = "-PRODSPEC_ID #{@@svtest.product}" unless @@fullrun
    # assert @@cp.update_reports(10, @@feed, params)
    assert @@cp.run_loader(@@feed, @@fullrun ? nil : "-PRODSPEC_ID #{@@svtest.product}")
    assert @@cp.get_reports(@@feed)
  end

  def test11_runsheet
    assert @@cptest.verify_runsheet_data(@@svtest.route, @@svtest.product)
  end

end
