=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: Steffen Steidten, Thomas Klose, 2010-06-23

Version: 1.18

History:
  2019-02-12 sfrieske, use siviewtest for lot creation directly
=end

require_relative 'Test_CP'

# Test Common Platform data feed Lot Complete
#
# Requirements: http://myteamsmst/sites/MST/IPMO/prj/Common%20Platform%20Integration/Shared%20Documents/MFG%20TRACK/Requirement%20Details/CPDI_LOTCOMPLETE_requirements.xlsx
class Test_CP11 < Test_CP
  @@feed = 'lot_complete'
  @@event_sleep = 60


  def test00_setup
    $setup_ok = false
    #
    # create a new lot and set DieCountGood
    uparams = {'CustomerPriority'=>'QA_Prio'}
    assert @@lot = @@svtest.new_lot(user_parameters: uparams), 'error creating test lot'
    @@testlots << @@lot
    @@sv.lot_info(@@lot, wafers: true).wafers.each_with_index {|w, i|
      assert_equal 0, @@sv.user_parameter_change('Wafer', w.wafer, 'DieCountGood', i)
    }
    # move to bankin pd
    $log.info "sleeping #{@@event_sleep} s to separate events"; sleep @@event_sleep
    @@sv.lot_opelocate(@@lot, :last)
    # extra sleep for auto bankin
    sleep 10
    li = @@sv.lot_info(@@lot)
    assert_equal 'InBank', li.states['Lot Inventory State'], "lot #{@@lot} is not banked in"
    #
    $setup_ok = true
  end

  def test11_lot_complete
    assert @@cp.update_reports(@@sleeptime, @@feed)
    assert @@cptest.verify_lot_complete_data(@@lot)
  end

end
