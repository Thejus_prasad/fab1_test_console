=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: Steffen Steidten, Thomas Klose, 2010-09-23

Version: 1.37

History:
  2018-03-06 sfrieske, replaced user_parameter_set_verify by user_parameter_change
  2018-05-09 sfrieske, minor adjustments for scrapped lots with AutoScrap jCAP job interfering
  2019-02-12 sfrieske, use siviewtest for lot creation directly
=end

require_relative 'Test_CP'


# Test Common Platform data feed Scrap
#
# Requirements: http://myteamsmst/sites/MST/IPMO/prj/Common%20Platform%20Integration/Shared%20Documents/MFG%20TRACK/Requirement%20Details/CPDI_SCRAP_requirements.xlsx
class Test_CP18 < Test_CP
  @@feed = 'scrap'
  @@claim_memo = "TestXX #{Time.now}"

  @@event_sleep = 10  #400  # must be high enough for the MDS to pick up changes from the SiView history


  def test09_report
    $setup_ok = false
    #
    # create test lots, sublottype not PROD or QUAL due to AutoScrap jCAP job
    uparams = {
      'CustomerLotGrade'=>'70', 'CustomerPriority'=>'QA_Prio', 'CustomerQualityCode'=>'CCO', 
      'TurnkeyType'=>'J', 'TurnkeySubconIDBump'=>'AT', 'TurnkeySubconIDSort'=>'AK'
    }
    assert @@testlots = @@svtest.new_lots(6, sublottype: 'RD', user_parameters: uparams), 'error creating test lots'
    @@lot1, @@lot2, @@lot3, @@lot4, @@lot5, @@lot6 = @@testlots
    # prepare lots
    assert_equal 0, @@sv.user_parameter_delete('Lot', @@lot1, 'ExpediteOrder', check: true)
    assert_equal 0, @@sv.user_parameter_change('Lot', @@lot2, 'ExpediteOrder', '')
    assert_equal 0, @@sv.user_parameter_change('Lot', @@lot3, 'ExpediteOrder', 'N')
    assert_equal 0, @@sv.user_parameter_change('Lot', @@lot4, 'ExpediteOrder', 'Y')
    aliases = 25.downto(1).collect {|i| "%2.2d" % i}
    @@testlots.each {|lot|
      assert_equal 0, @@sv.user_parameter_change('Lot', lot, uparams)
      @@sv.lot_info(lot, wafers: true).wafers.each_with_index {|w, i|
        @@sv.user_parameter_change('Wafer', w.wafer, 'DieCountGood', i)
      }
      assert_equal 0, @@sv.wafer_alias_set(lot, aliases: aliases)
    }
    #
    @@lot2c = @@sv.lot_split(@@lot2, 5)
    ##@@testlots << @@lot2c
    assert_equal 0, @@sv.lot_experiment(@@lot3, 25, 'A-CP-BRANCH-WSTAGEID.01', '1100.1000', '1200.1100')
    assert_equal 0, @@sv.lot_opelocate(@@lot3, '1100.1000')
    assert_equal 0, @@sv.lot_experiment(@@lot4, 25, 'A-CP-BRANCH-MAINPD.01', '1100.1000', '1200.1100')
    assert_equal 0, @@sv.lot_opelocate(@@lot4, '1100.1000')
    $log.info "waiting #{@@event_sleep} s to separate events"; sleep @@event_sleep
    assert_equal 0, @@sv.scrap_wafers(@@lot1, memo: @@claim_memo), 'error scrapping lot'
    assert_equal 0, @@sv.scrap_wafers(@@lot2c, memo: @@claim_memo), 'error scrapping lot'
    assert_equal 0, @@sv.scrap_wafers(@@lot3, memo: @@claim_memo), 'error scrapping lot'
    assert_equal 0, @@sv.scrap_wafers(@@lot4, memo: @@claim_memo), 'error scrapping lot'
    assert_equal 0, @@sv.scrap_wafers(@@lot5, memo: @@claim_memo), 'error scrapping lot'
    assert_equal 0, @@sv.scrap_wafers(@@lot6), 'error scrapping lot'
    # get wafer data and move lot5 to a FOSB
    @@li5 = @@sv.lot_info(@@lot5, wafers: true)
    assert_equal 0, @@sv.wafer_sort(@@lot5, '')
    # scrap cancel lot6
    assert_equal 0, @@sv.scrap_wafers_cancel(@@lot6, memo: @@claim_memo)
    #
    assert @@cp.update_reports(@@sleeptime, @@feed)
    #
    $setup_ok = true
  end

  def test21
    assert @@cptest.verify_scrap_data(@@lot1)
  end

  def test22
    assert @@cptest.verify_scrap_data(@@lot2c)
  end

  def test23
    assert @@cptest.verify_scrap_data(@@lot3)
  end

  def test24
    assert @@cptest.verify_scrap_data(@@lot4)
  end

  def test25
    assert @@cptest.verify_scrap_data(@@lot5, lotinfo: @@li5)
  end

  def test26
    assert @@cptest.verify_scrap_data(@@lot6, scrap_cancel: true)
  end


  def test95_compare_record_count
    assert_equal true, @@cp.compare_record_count(@@feed), "wrong record count"
  end

end
