=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: Steffen Steidten, Thomas Klose, 2011-03-02

Version: 1.43

History:
  2018-03-06 sfrieske, replaced user_parameter_set_verify by user_paramer_change
  2018-03-08 sfrieske, separated (Un)TransferTitle tests into a dedicated testcase
  2018-05-09 sfrieske, minor adjustments for scrapped lots with AutoScrap jCAP job interfering
  2019-02-12 sfrieske, use siviewtest for lot creation directly
=end

require_relative 'Test_CP'


# Test Common Platform data feed 3C Lot History.
#
# Requirements: http://myteamsgf1/sites/MAT/APPS/cpdi/Shared%20Documents/Forms/AllItems.aspx
#
# Note: to reset data for a lot edit $AI_SERIAL_STATIC/last_lot_history_txn_timestamp.dat
class Test_CP23 < Test_CP
  @@feed = 'lot_history'

  @@sublottype0 = 'PX' # was PR
  @@sublottype1 = 'PO'
  @@sublottype2 = 'PW'   # for TransferTitle
  @@opNo_module_end = '1000.1400'
  @@separationtime = 60


  def test00_setup
    $setup_ok = false
    #
    uparams = {'ERPSalesOrder'=>'4321', 'ExpediteOrder'=>'Y'}
    assert @@testlots = @@svtest.new_lots(3, sublottype: @@sublottype0, user_parameters: uparams), 'error creating test lots'
    #
    lot = @@testlots[0]
    $log.info '-- STB'
    sleep @@separationtime
    $log.info '-- OrderChange'
    assert_equal 0, @@sv.lot_opelocate(lot, 2)
    assert_equal 0, @@sv.sublottype_change(lot, @@sublottype1)
    sleep @@separationtime
    $log.info '-- Split'
    assert clot = @@sv.lot_split(lot, 10), 'error splitting lot'
    sleep @@separationtime  ## 120
    $log.info '-- Merge'
    assert_equal 0, @@sv.lot_merge(lot, clot), 'error merging lot'
    sleep @@separationtime
    $log.info '-- OperationComplete'
    assert @@sv.claim_process_lot(lot), 'error processing lot'
    sleep @@separationtime
    $log.info '-- ForceComp'
    assert_equal 0, @@sv.lot_opelocate(lot, @@opNo_module_end)
    assert @@sv.claim_process_lot(lot, running_hold: true), 'error processing lot'
    assert_equal 0, @@sv.lot_hold_release(lot, nil)
    sleep @@separationtime
    $log.info '-- GatePass'
    assert_equal 0, @@sv.lot_gatepass(lot)
    sleep @@separationtime
    $log.info '-- WaferScrap'
    assert_equal 0, @@sv.scrap_wafers(lot)
    sleep @@separationtime
    $log.info '-- WaferScrapCancel'
    rc = @@sv.scrap_wafers_cancel(lot)
    assert [0, 2860].member?(rc)  # AutoScrap jCAP job might have canceled already
    sleep @@separationtime
    $log.info '-- Ship'
    assert_equal 0, @@sv.lot_opelocate(lot, :last)
    assert_equal 0, @@sv.lot_ship(lot, nil)
    #
    # for STBCancel
    assert vlot = @@sv.stb_cancel(@@testlots[1])
    @@testlots << vlot
    #
    # for virtual categories TransferTitle and UnTransferTitle
    lot = @@testlots[2]
    @@timestamps2 = []
    $log.info '-- OrderChange'
    @@timestamps2 << Time.now
    assert_equal 0, @@sv.lot_opelocate(lot, 2)
    assert_equal 0, @@sv.sublottype_change(lot, @@sublottype1)
    @@sv.lot_requeue(lot)
    sleep @@separationtime
    $log.info '-- TransferTitle'
    @@timestamps2 << Time.now
    assert_equal 0, @@sv.sublottype_change(lot, @@sublottype2)
    @@sv.lot_requeue(lot)
    sleep @@separationtime
    $log.info '-- UnTransferTitle'
    @@timestamps2 << Time.now
    assert_equal 0, @@sv.sublottype_change(lot, @@sublottype1)
    @@sv.lot_requeue(lot)
    #
    $log.info "waiting #{@@sleeptime + 150} s before running the #{@@feed} loader"; sleep @@sleeptime + 150
    #
    $setup_ok = true
  end

  def test11_lot
    lot = @@testlots[0]
    assert @@cp.update_reports(1, @@feed, runargs: "-LOT_ID #{lot}")
    assert @@cp.get_reports('wafer_history'), 'error reading wafer_history data'
    test_ok = @@cptest.verify_lot_history(lot, 'STB', sublottype: @@sublottype0)
    %w(WaferScrap WaferScrapCancel Ship OperationComplete ForceComp Split Merge OrderChange GatePass).each {|c|
      test_ok &= @@cptest.verify_lot_history(lot, c)
    }
    assert test_ok
  end

  def test12_lot_STBCancel
    lot = @@testlots[1]
    assert @@cp.update_reports(1, @@feed, runargs: "-LOT_ID #{lot}")
    assert @@cp.get_reports('wafer_history'), 'error reading wafer_history file'
    assert @@cptest.verify_lot_history(lot, 'STBCancelled')
  end

  def test13_transfertitle
    lot = @@testlots[2]
    assert @@cp.update_reports(1, @@feed, runargs: "-LOT_ID #{lot}")
    assert @@cp.get_reports('wafer_history'), 'error reading wafer_history file'
    assert @@cptest.verify_lot_history(lot, 'TransferTitle',
      tstart: @@timestamps2[1] - 20, tend: @@timestamps2[1] + 20, sublottype: @@sublottype2)
    assert @@cptest.verify_lot_history(lot, 'UnTransferTitle',
      tstart: @@timestamps2[2] - 20, tend: @@timestamps2[2] + 20)
  end

end
