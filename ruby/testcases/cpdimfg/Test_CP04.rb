=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: Steffen Steidten, Thomas Klose, 2010-06-23

Version: 1.20

History:
  2019-02-12 sfrieske, use siviewtest for lot creation directly
=end

require_relative 'Test_CP'

# Test Common Platform data feeds Lot Start and Lot Wafer Start.
#
# Requirements: http://myteamsmst/sites/MST/IPMO/prj/Common%20Platform%20Integration/Shared%20Documents/MFG%20TRACK/Requirement%20Details/CPDI_LOTSTART_requirements.xlsx
class Test_CP04 < Test_CP
  @@feed = 'lot_start'


  def test09_report
    $setup_ok = false
    #
    # create a new lot and start the loader
    uparams = {'CustomerPriority'=>'QA_Prio'}
    assert @@lot = @@svtest.new_lot(user_parameters: uparams), 'error creating test lot'
    @@testlots << @@lot
    assert @@cp.update_reports(60, @@feed, runargs: "-LOT_ID #{@@lot}")
    #
    $setup_ok = true
  end

  def test11_lot_start
    assert @@cptest.verify_lot_start_data(@@lot)
  end

  def test21_lot_wfr_start
    # additional feed generated by this loader
    @@cp.get_reports('lot_wfr_start')
    assert @@cptest.verify_lot_wfr_start_data(@@lot)
  end

  def test99_warn
    $log.warn 'ORDER_NO, CURR_SCH_DATE and ORIG_SCH_DATE are not covered'
  end

end
