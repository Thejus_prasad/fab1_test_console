=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: Steffen Steidten, Thomas Klose, 2010-09-23
Version: 1.19.0
=end

require_relative 'Test_CP'

# Test Common Platform data feed Equipment Current Info
#
# Requirements: http://myteamsmst/sites/MST/IPMO/prj/Common%20Platform%20Integration/Shared%20Documents/MFG%20TRACK/Requirement%20Details/CPDI_EQPCURINFO_requirements.xlsx
class Test_CP17 < Test_CP
  @@feed = 'equipment_current_info'

  @@fab = 'FAB1'
  @@eqp1 = 'SNK920'
  @@eqp2 = 'MET800'
  @@eqp3 = 'MET801'
  @@eqp4 = 'DCNT'


  def test09_report
    assert $setup_ok = @@cp.update_reports(1, @@feed)
  end

  def test11_eqp
    assert @@cptest.verify_equipment_current_info_data(@@eqp1, @@fab)
  end

  def test12_eqp_umlauts
    assert @@cptest.verify_equipment_current_info_data(@@eqp2, @@fab)
  end

  def test13_eqp_empty
    assert @@cptest.verify_equipment_current_info_data(@@eqp3, @@fab)
  end

  def test14_eqp_virtual
    assert @@cptest.verify_equipment_current_info_data(@@eqp4, @@fab)
  end

  def test95_compare_record_count
    assert @@cp.compare_record_count(@@feed), 'wrong record count'
  end
end
