=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: Steffen Steidten, Thomas Klose, Beate Kochinka, Daniel Steger

Version: 1.41

History:
  2016-10-18 sfrieske, change a lot's route and test for correct CURR_MASK_COUNT
  2019-02-12 sfrieske, use siviewtest for lot creation directly
  2021-08-31 sfrieske, requiring 'time' for #iso8601
=end

require 'time'
require_relative 'Test_CP'


# Test Common Platform data feeds Wip Status, Wip Wafer and basic tests for Lot Ship.
# Requirements: http://myteamsmst/sites/MST/IPMO/prj/Common%20Platform%20Integration/Shared%20Documents/MFG%20TRACK/Requirement%20Details/CPDI_WIPSTATUS_requirements.xlsx
class Test_CP01 < Test_CP
  @@feed = 'wip_status'

  @@product_long = '8901AALONG.A0'
  @@route_long = 'A-CP-MAINPD-LONG.01'
  @@route2 = 'A-CP-MAINPD.02'   # must be identical to the standard route

  @@rework_route = 'A-CP-RWL-MAINPD.01'
  @@rework_pd = '1000.1200'

  @@erf_route_stage = 'e2CP-STAGEID.01'       #'A-CP-BRANCH-MAINPD.01'
  @@erf_route_nostage = 'e1CP-NOSTAGEID.01'   #'A-CP-BRANCH-WSTAGEID.01'
  @@erf_split_opNo = '1100.1000'
  @@erf_ret_opNo = '1200.1100'

  @@quick = false   # keep it false to detect loader issues


  def test00_setup
    $setup_ok = false
    #
    # create test lots
    uparams = {'Purpose'=>'QA_Test', 'DueDate'=>(Time.now + 1000000).iso8601.slice(0, 10), 'CustomerPriority'=>'QA_Prio'}
    assert @@testlots = @@svtest.new_lots(10, user_parameters: uparams), 'error creating test lots'
    @@lot1, @@lot2, @@lot3, @@lot4, @@lot5, @@lot6, @@lot7, @@lot8, @@lot9, @@lot10 = @@testlots
    #
    @@testlots.each {|lot|
      assert_equal 0, @@sv.schdl_change(lot, priority: [1, 2, 3, 4, 5, 6, 7, 8, 9].sample)
    }
    # set die counts for lot 1
    @@sv.lot_info(@@lot1, wafers: true).wafers.each {|w|
      assert_equal 0, @@sv.user_parameter_change('Wafer', w.wafer, 'DieCountGood', w.slot)
    }
    # lot1 stays at 1000.1000, split required for both scenarios
    assert @@lot1c = @@sv.lot_split(@@lot1, 10), "error splitting #{@@lot1}"
    # change route for lot2 (for CUR_MASK_COUNT)
    assert_equal 0, @@sv.schdl_change(@@lot2, route: @@route2, product: @@svtest.product)
    assert_equal @@route2, @@sv.lot_info(@@lot2).route, "wrong route for #{@@lot2}"
    #
    $log.info "using lots #{@@testlots.inspect}"
    #
    $setup_ok = true
  end

  #
  # scenario 2
  #
  def test20_scenario2_setup
    $setup_ok = false
    #
    # lot2: locate to 1100.1100
    assert_equal 0, @@sv.lot_opelocate(@@lot2, '1100.1100')
    # lot3: locate to 1300.1000 (BANKIN)
    assert_equal 0, @@sv.lot_opelocate(@@lot3, '1300.1000')
    # lot4: locate to 1200.1200, do partial rework to A-CP-RWS_MAINPD.01
    # parent lot stays here, ONHOLD
    assert_equal 0, @@sv.lot_opelocate(@@lot4, '1200.1100')
    @@lot4_li = @@sv.lot_info(@@lot4, wafers: true) # get this information _before_ opecomp!
    assert @@sv.claim_process_lot(@@lot4), "error processing lot #{@@lot4}"
    @@lot4c = @@sv.lot_partial_rework(@@lot4, 5, @@rework_route, return_opNo: @@rework_pd)
    assert @@lot4c, "lot #{@@lot4} has no rework child"
    @@lot4c_li = @@sv.lot_info(@@lot4c, wafers: true)
    assert_equal 0, @@sv.lot_opelocate(@@lot4c, 1)
    # lot5: locate to 1200.1100, process and do rework to A-CP-RWL-MAINPD.01 at 1200.1200
    assert_equal 0, @@sv.lot_opelocate(@@lot5, '1200.1100')
    @@lot5_li = @@sv.lot_info(@@lot5, wafers: true) # get this information _before_ opecomp!
    assert @@sv.claim_process_lot(@@lot5), "error processing lot #{@@lot5}"
    assert_equal 0, @@sv.lot_rework(@@lot5, @@rework_route, return_opNo: @@rework_pd), "error doing rework for #{@@lot5}"
    assert_equal 0, @@sv.lot_opelocate(@@lot5, 1)
    # lot6: set PSM to ERF route w/o stage ID, locate to 1000.1100 at BRANCH_PD
    @@sv.lot_experiment(@@lot6, 5, @@erf_route_stage, @@erf_split_opNo, @@erf_ret_opNo)
    assert_equal 0, @@sv.lot_opelocate(@@lot6, @@erf_split_opNo)
    assert_equal 0, @@sv.lot_opelocate(@@lot6, 1)
    @@lot6c = @@sv.lot_family(@@lot6).last
    assert @@lot6c, "lot #{@@lot6} has no child lot"
    # lot7: set PSM to ERF route with stage ID, locate to 1000.1100 at BRANCH_PD
    assert_equal 0, @@sv.lot_experiment(@@lot7, 5, @@erf_route_nostage, @@erf_split_opNo, @@erf_ret_opNo)
    assert_equal 0, @@sv.lot_opelocate(@@lot7, @@erf_split_opNo)
    assert_equal 0, @@sv.lot_opelocate(@@lot7, 1)
    @@lot7c = @@sv.lot_family(@@lot7).last
    refute_equal @@lot7c, @@lot7, "lot #{@@lot7} has no child lot"
    # lot8: locate to 1300.1000 (BANKIN)
    assert_equal 0, @@sv.lot_opelocate(@@lot8, :last)
    # lot9: 2 different ERFs
    # ERF1
    assert_equal 0, @@sv.lot_experiment(@@lot9, 25, @@erf_route_nostage, @@erf_split_opNo, @@erf_ret_opNo)
    assert_equal 0, @@sv.lot_opelocate(@@lot9, @@erf_split_opNo)
    assert_equal 0, @@sv.lot_opelocate(@@lot9, 1)
    assert_equal 0, @@sv.lot_opelocate(@@lot9, -2)
    assert @@sv.lot_experiment_delete(@@lot9)
    # ERF2
    assert_equal 0, @@sv.lot_experiment(@@lot9, 25, @@erf_route_stage, @@erf_split_opNo, @@erf_ret_opNo)
    assert_equal 0, @@sv.lot_opelocate(@@lot9, @@erf_split_opNo)
    assert_equal 0, @@sv.lot_opelocate(@@lot9, 1)
    # lot10: process on long route and schdl_change back to regular route
    lot = @@lot10
    @@sv.schdl_change(lot, route: @@route_long, product: @@product_long, opNo: '1000.1000')
    li = @@sv.lot_info(lot)
    assert_equal [@@route_long, @@product_long], [li.route, li.product], "error in schdl_change"
    10.times {assert @@sv.claim_process_lot(lot)}
    @@sv.schdl_change(lot, route: @@svtest.route, product: @@svtest.product, opNo: '1000.1000')
    li = @@sv.lot_info(lot)
    assert_equal [@@svtest.route, @@svtest.product], [li.route, li.product], "error in schdl_change"
    #
    # start loader and get output, run _without_ args to detect loader issues
    assert @@cp.update_reports(@@sleeptime, @@feed, runargs: @@quick ? "-PRODSPEC_ID #{@@svtest.product}" : nil)
    #
    $setup_ok = true
  end

  def test21
    $setup_ok = false
    assert @@cptest.verify_wip_status_data(@@lot1)
    $setup_ok = true
  end

  def test21_child
    assert @@cptest.verify_wip_status_data(@@lot1c)
  end

  def test22
    assert @@cptest.verify_wip_status_data(@@lot2)
  end

  def test23
    assert @@cptest.verify_wip_status_data(@@lot3)
  end

  def test24
    assert @@cptest.verify_wip_status_data(@@lot4)
  end

  def XXtest24c
    # will fail
    @@lot4_li.lot = @@lot4c_li.lot
    @@lot4_li.nwafers = @@lot4c_li.nwafers
    assert @@cptest.verify_wip_status_data(@@lot4c, li: @@lot4_li)
  end

  def XXtest25
    # will fail
    assert @@cptest.verify_wip_status_data(@@lot5, li: @@lot5_li)  ##, currmaskcount: 3)
  end

  def test26
    assert @@cptest.verify_wip_status_data(@@lot6, erfid: @@erf_route_stage[0..3])
  end

  def test27
    assert @@cptest.verify_wip_status_data(@@lot7, erfid: @@erf_route_nostage[0..3])
  end

  def test28
    assert @@cptest.verify_wip_status_data(@@lot8)
  end

  def test29
    res = @@cptest.verify_wip_status_data(@@lot9, erfid: "#{@@erf_route_nostage[0..3]},#{@@erf_route_stage[0..3]}")
    $log.warn "@@cptest.verify_wip_status_data: #{res}, it is normal for this test to fail intermittently"
  end

  def test29a_route_change
    assert @@cptest.verify_wip_status_data(@@lot10)
  end


  #
  # scenario 3
  #
  def test30_scenario3
    $setup_ok = false
    #
    # lot1: split child again
    assert @@lot1cc = @@sv.lot_split(@@lot1c, 6)
    # lots 2,3 do nothing
    # lot4: identify child lot, locate child to merge operation
    assert_equal 0, @@sv.lot_opelocate(@@lot4c, '1200.1200')
    assert_equal 0, @@sv.lot_merge(@@lot4, @@lot4c)
    assert_equal 0, @@sv.lot_hold_release(@@lot4, 'AEHL')
    # lot5: locate foward
    assert_equal 0, @@sv.lot_opelocate(@@lot5, 3)
    # lots 6,7: locate to 1100.1100, delete branch
    assert_equal 0, @@sv.lot_opelocate(@@lot6, '1100.1100')
    assert_equal 0, @@sv.lot_opelocate(@@lot7, '1100.1100')
    # lot8: split and ship parent
    assert @@lot8c = @@sv.lot_split(@@lot8, 2)
    assert_equal 0, @@sv.lot_ship(@@lot8, nil)
    # lot9: ERF and rework
    # ERF
    lot = @@lot9
    assert @@sv.lot_experiment_delete(lot)
    assert_equal 0, @@sv.lot_experiment(lot, 10, @@erf_route_nostage, @@erf_split_opNo, @@erf_ret_opNo)
    assert_equal 0, @@sv.lot_opelocate(lot, @@erf_split_opNo)
    assert_equal 0, @@sv.lot_opelocate(lot, 1)
    @@lot9c = @@sv.lot_family(lot).last
    refute_equal @@lot9c, @@lot9, 'error splitting lot'
    assert_equal 0, @@sv.lot_opelocate(@@lot9c, 1)
    #
    # start loader and get output
    assert @@cp.update_reports(60, @@feed, runargs: "-PRODSPEC_ID #{@@svtest.product}")
    #
    $setup_ok = true
  end

  def test31
    assert @@cptest.verify_wip_status_data(@@lot1)
    assert @@cptest.verify_wip_status_data(@@lot1c)
    assert @@cptest.verify_wip_status_data(@@lot1cc)
  end

  def test32
    assert @@cptest.verify_wip_status_data(@@lot2)
  end

  def test33
    assert @@cptest.verify_wip_status_data(@@lot3)
  end

  def test34
    assert @@cptest.verify_wip_status_data(@@lot4)
  end

  def XXtest35
    # will fail
    assert @@cptest.verify_wip_status_data(@@lot5)   #, currmaskcount: 3)
  end

  def test36
    assert @@cptest.verify_wip_status_data(@@lot6)
  end

  def test37
    assert @@cptest.verify_wip_status_data(@@lot7)
  end

  def test38
    assert @@cptest.verify_wip_status_data(@@lot8c)
  end

  def test39
    assert @@cptest.verify_wip_status_data(@@lot9)
  end

  def XXtest39c
    # will fail
    assert @@cptest.verify_wip_status_data(@@lot9c)
  end

  #
  # WIP_WAFER
  #
  def test51_wip_wafer
    @@cp.get_reports('wip_wafer')
    assert @@testlots.inject(true) {|ok, lot| @@cptest.verify_wip_wafer_data(lot) && ok}
  end


  def testman91_performance
    # perf of full loader run w/o filters, no checks
    @@cp.run_loader('wip_status')
    assert false, "compare performance to the previous version!"
  end

end
