=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: Steffen Steidten, Thomas Klose, Beate Kochinka, 2010-05-07

Version: 1.45

History:
  2013-08-14 ssteidte, removed method run_CP_tests
  2014-12-15 ssteidte, delete old reports in $cp
  2016-10-20 sfrieske, minor adjustment for load run duration
  2018-05-09 sfrieske, require cptest instead of load
  2019-01-07 sfrieske, honour @@sv_defaults
  2019-02-12 sfrieske, use siviewtest for lot creation directly
  2021-08-11 sfrieske, use UX carriers instead of CPFEOL
  2021-08-30 sfrieske, moved cp.rb and cptest.rb to misc
=end

require 'misc/cptest'
require 'SiViewTestCase'


# Test Common Platform data feeds
class Test_CP < SiViewTestCase
  @@sv_defaults = {carriers: 'UX%', product: '8901AA.A0', route: 'A-CP-MAINPD.01'}
  @@sleeptime = 310   # wait for MDS to sync and run triggers, etc.
  @@feed = ''
  @@loader = ''


  def self.startup
    super
    @@cptest = CPTest.new($env, sv: @@sv, sm: @@sm)  ##, {sv: @@sv}.merge(@@sv_defaults))
    @@cp = @@cptest.cp
    @@testlots = []
  end

  def self.after_all_passed
    @@testlots.each {|lot| @@sv.delete_lot_family(lot)}
  end

  def self.shutdown
    duration = @@cp.loader_run_durations[@@loader] || @@cp.loader_run_durations[@@feed]
    $log.info "loader run duration for feed #{@@feed}: #{duration} s"
  end

end
