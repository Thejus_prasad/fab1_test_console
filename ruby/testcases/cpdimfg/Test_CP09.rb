=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: Steffen Steidten, Thomas Klose, 2010-08-25

Version: 1.20

History:
  2019-02-12 sfrieske, use siviewtest for lot creation directly
=end

require_relative 'Test_CP'

# Test Common Platform data feeds Lot Merge and Lot Split.
#
# Requirements: http://myteamsmst/sites/MST/IPMO/prj/Common%20Platform%20Integration/Shared%20Documents/MFG%20TRACK/Requirement%20Details/CPDI_SPLITMERGE_requirements.xlsx
class Test_CP09 < Test_CP
  @@loader = 'lot_split_merge'
  @@customer1 = 'ibm'
  @@customer2 = 'gf'


  def test00_setup
    $setup_ok = false
    #
    # create lots and set DieCountGood
    uparams = {'CustomerPriority'=>'QA_Prio'}
    assert @@testlots = @@svtest.new_lots(2, customer: @@customer1, user_parameters: uparams), 'error creating test lots'
    @@testlots.each {|lot|
      @@sv.lot_info(lot, wafers: true).wafers.each {|w| @@sv.user_parameter_change('Wafer', w.wafer, 'DieCountGood', 15)}
    }
    @@lot1, @@lot2 = @@testlots
    #
    # split
    @@split_time = Time.now
    assert @@lot1c = @@sv.lot_split(@@lot1, 2, memo: "CP Test #{@@split_time}"), "error splitting #{@@lot1}"
    assert @@lot2c = @@sv.lot_split(@@lot2, 2, memo: "CP Test #{@@split_time}"), "error splitting #{@@lot2}"
    sleep 15
    assert_equal 0, @@sv.lot_mfgorder_change(@@lot2c, customer: @@customer2)
    assert_equal 0, @@sv.lot_requeue(@@lot2c)
    # merge back lot1
    @@lot1cinfo = @@sv.lot_info(@@lot1c, wafers: true)
    @@merge_time = Time.now
    assert_equal 0, @@sv.lot_merge(@@lot1, @@lot1c), "error merging back #{@@lot1}"
    #
    $setup_ok = true
  end

  def test09_reports
    $setup_ok = false
    #
    # run loader, reports for split and merge are in different feeds
    $log.info "waiting #{@@sleeptime}s before running the #{@@loader} loader"
    sleep @@sleeptime
    lotfilter = @@lot1[0..4] + '%'
    assert @@cp.run_loader(@@loader, "-LOT_ID #{lotfilter}")
    assert @@cp.get_reports('lot_split_wafer')
    assert @@cp.get_reports('lot_merge_wafer')
    #
    $setup_ok = true
  end

  # check the lot and child lot data before the merge! ???
  def test11_lot_split_parent
    assert @@cptest.verify_lot_split_data(@@lot2, @@lot2c, @@split_time)
  end

  def test12_lot_split_child
    assert @@cptest.verify_lot_split_data(@@lot2c, @@lot2, @@split_time)
  end

  def test21_lot_merge_data
    assert @@cptest.verify_lot_merge_data(@@lot1, @@lot1cinfo, @@merge_time)
  end

end
