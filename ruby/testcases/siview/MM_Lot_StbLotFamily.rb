=begin
(c) Copyright 2016 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: Daniel Steger 2017-04-06

History:
  2017-05-08 sfrieske, fixed lot merge tests
  2019-01-22 sfrieske, minor cleanup
  2019-12-05 sfrieske, minor cleanup, renamed from Test006_STBtoLotFamily
  2020-08-24 sfrieske, minor cleanup, improved use of lot_operation_history
  2021-01-15 sfrieske, moved SiView::MM#new_srclot_for_product to SiView::Test#new_srclot
=end

require 'SiViewTestCase'


class MM_Lot_StbLotFamily < SiViewTestCase
  @@customer = 'samsung'
  @@reason = 'E-LA'
  @@slt2 = 'EM'
  @@sltscrap = 'SCRAPQA'

  @@testlots = []


  def self.shutdown
    @@sv.process_hold_list(route: @@svtest.route).each {|h| @@sv.process_hold_cancel(holdrecord: h)}
  end

  def self.after_all_passed
    @@testlots.each {|lot| @@sv.delete_lot_family(lot)}
  end


  def test01_stb_to_family
    assert srclot = @@svtest.new_srclot, 'error creating srclot'
    @@testlots << srclot
    assert lot = @@svtest.new_lot(nwafers: 5, srclot: srclot), 'failed to get new lot'
    @@testlots << lot
    # stb child lot
    assert srclot2 = @@sv.lot_split_notonroute(srclot, 5), 'failed to split vendor lot'
    tgtlot = lot.next
    assert child = @@sv.stb_lotfamily(lot, srclot2, tgtlot), "failed to create child lot #{tgtlot}"
    assert_equal tgtlot, child, "wrong child lot id"
    # try to stb agin
    refute @@sv.stb_lotfamily(lot, srclot, tgtlot), 'create child lot should fail'
    assert_equal 406, @@sv.rc, "wrong rc"
    # verify properties
    li = @@sv.lot_info(lot)
    lci = @@sv.lot_info(child)
    [:priority, :sublottype, :cast_cat, :carrier].each {|m| assert_equal li[m], lci[m], "wrong #{m} for #{child}"}
    # verify lot family
    assert_equal [lot, child], @@sv.lot_family(lot), 'wrong lot family'
    # TODO: check the alias
    # verify merge
    @@sv.user_parameter('Lot', lot).each {|p|
      @@sv.user_parameter_change('Lot', tgtlot, p.name, p.value, 1, datatype: p.datatype)
    }
    assert_equal 0, @@sv.lot_merge(lot, tgtlot), 'failed to merge lots'
    #
    # 2nd child
    assert srclot3 = @@sv.lot_split_notonroute(srclot, 5), 'failed to split vendor lot'
    tgtlot2 = tgtlot.next
    assert child2 = @@sv.stb_lotfamily(lot, srclot3, tgtlot2), "failed to create child lot #{tgtlot2}"
    assert_equal tgtlot2, child2, "wrong child lot id"
    lci2 = @@sv.lot_info(child2, wafers: true)
    child_wafers = lci2.wafers.map {|w| w.wafer}
    child_slots = lci2.wafers.map {|w| w.slot}
    # delete the 2nd child and STB again
    assert_equal 0, @@sv.sublottype_change(child2, @@sltscrap), "failed to change sublottype of #{child2}"
    assert_equal 0, @@sv.wafer_sort(child2, ''), "failed to remove #{child2} from carrier"
    assert_equal 0, @@sv.scrap_wafers(child2), "faild to scrap #{child2}"
    sleep 30 # wait for history watchdog
    assert_equal 0, @@sv.lot_delete(child2), "failed to delete #{child2}"
    #
    # STB the removed 2nd child again
    assert srclot4 = @@svtest.new_srclot(carrier: li.carrier,
      slots: child_slots, waferids: child_wafers), 'failed to create source lot'
    @@testlots << srclot4
    tstart = Time.now
    assert child3 = @@sv.stb_lotfamily(lot, srclot4, tgtlot2, historycheck: true), 'failed to create child lot'
    @@testlots << child3
    assert_equal tgtlot2, child3, 'wrong child lot id'
    lci3 = @@sv.lot_info(tgtlot2)
    # verify merge
    @@sv.user_parameter('Lot', lot).each {|p|
      @@sv.user_parameter_change('Lot', tgtlot2, p.name, p.value, 1, datatype: p.datatype)
    }
    assert_equal 0, @@sv.lot_merge(lot, tgtlot2), 'failed to merge lots'
    # verify history exists
    assert wait_for(timeout: 60) {
      history = @@sv.lot_operation_history(tgtlot2, opCategory: 'STB', opNo: lci3.opNo, route: lci3.route, pass: 1, raw: true)
      history.select {|e| @@sv.siview_time(e.reportTimeStamp) > tstart}.size == 1
    }, 'no STB history entry found'
  end

  def test02_wafercount
    assert srclot = @@svtest.new_srclot, 'error creating srclot'
    @@testlots << srclot
    assert lot = @@svtest.new_lot(nwafers: 5, srclot: srclot), 'error creating test lot'
    @@testlots << lot
    assert srclot2 = @@svtest.new_srclot, 'error creating srclot'
    @@testlots << srclot2
    tgtlot = lot.next
    refute @@sv.stb_lotfamily(lot, srclot2, tgtlot), "create child lot #{tgtlot} should fail"
    assert_equal 10509, @@sv.rc, 'wrong rc'
  end

  # MES-3480 not checked in SiView
  # def testmanual03_splitid
  #   lot, srclot = _create_parentlot
  #   # wrong child lot id
  #   if  /\A(.*)\.([0-9A-Z]+)\z/ === lot
  #     tgtlot = "#{$1}.000001"
  #   else
  #     fail "invalid parent lot id #{lot}"
  #   end
  #   refute @@sv.stb_lotfamily(lot, srclot, tgtlot, @@svtest.product, @@svtest.route), "create child lot should fail"
  #
  #   # wrong child lot id according to the customer lot rules
  #   assert_equal 'Numeric', @@sv.user_data_customer(@@customer)['LIG_SplitSeqRule'], "rule should be numerical"
  #   lot, srclot = _create_parentlot(customer: @@customer)
  #   # wrong child lot id
  #   if  /\A(.*)\.([0-9A-Z]+)\z/ === lot
  #     tgtlot = "#{$1}.00A"
  #   else
  #     fail "invalid parent lot id #{lot}"
  #   end
  #   refute @@sv.stb_lotfamily(lot, srclot, tgtlot, @@svtest.product, @@svtest.route), "create child lot should fail"
  # end

  def test04_noparent
    assert srclot = @@svtest.new_srclot, 'error creating srclot'
    @@testlots << srclot
    assert lot = @@sv.generate_new_lotids(@@customer).first, 'failed to get new lot id'
    @@testlots << lot
    tgtlot = lot.next
    @@testlots << tgtlot
    refute @@sv.stb_lotfamily(lot, srclot, tgtlot,
      product: @@svtest.product, route: @@svtest.route, lotowner: @@sv.user, priority: '0', sublottype: 'PO'), 'create original lot should fail'
    assert_equal 1572, @@sv.rc, 'wrong rc'
  end

  # must fail per design but passes. this behavior is accepted by users
  def testman05_testwafer  # fails, child monitor lot is created
    assert srclot = @@svtest.new_srclot(lottype: 'Equipment Monitor'), 'error creating srclot'
    @@testlots << srclot
    assert lot = @@svtest.new_controllot('Equipment Monitor', srclot: srclot, nwafers: 5), 'error creating test lot'
    @@testlots << lot
    tgtlot = lot.next
    li = @@sv.lot_info(lot)
    refute @@sv.stb_lotfamily(lot, srclot, tgtlot), "create child lot #{tgtlot} should fail"
  end

  def test06_stbcancel
    assert srclot = @@svtest.new_srclot, 'error creating srclot'
    @@testlots << srclot
    assert lot = @@svtest.new_lot(nwafers: 5, srclot: srclot), 'error creating test lot'
    @@testlots << lot
    # stb child lot
    assert srclot2 = @@sv.lot_split_notonroute(srclot, 5), 'failed to split vendor lot'
    tgtlot = lot.next
    assert child = @@sv.stb_lotfamily(lot, srclot2, tgtlot), "failed to create child lot #{tgtlot}"
    # STB cancel and verify lot family
    assert vlot = @@sv.stb_cancel(child), "failed to cancel STB for #{child}"
    @@testlots << vlot
    assert_equal [lot], @@sv.lot_family(lot), 'wrong lot family'
  end

  def test07_nohistory
    assert srclot = @@svtest.new_srclot, 'error creating srclot'
    @@testlots << srclot
    assert lot = @@svtest.new_lot(nwafers: 5, srclot: srclot), 'error creating test lot'
    @@testlots << lot
    # try to stb child lot
    assert srclot2 = @@sv.lot_split_notonroute(srclot, 5), 'failed to split vendor lot'
    tgtlot = lot.next
    refute @@sv.stb_lotfamily(lot, srclot2, tgtlot, historycheck: true), "wrong child lot #{tgtlot}"
    assert_equal 10986, @@sv.rc, 'wrong rc'
  end

  def test09_split
    assert srclot = @@svtest.new_srclot, 'error creating srclot'
    @@testlots << srclot
    assert lot = @@svtest.new_lot(nwafers: 5, srclot: srclot), 'error creating test lot'
    @@testlots << lot
    tgtlot = lot.next
    tgtlot2 = tgtlot.next
    tgtlot3 = tgtlot2.next
    tgtlot4 = tgtlot3.next
    # STB 2nd child w/o first child
    assert srclot2 = @@sv.lot_split_notonroute(srclot, 2), 'cannot split vendor lot'
    assert child = @@sv.stb_lotfamily(lot, srclot2, tgtlot2), "failed to create child lot #{tgtlot2}"
    assert_equal tgtlot2, child, 'wrong child id'
    # split parent lot, creates child #3
    assert child2 = @@sv.lot_split(lot, 2), 'failed to split parent'
    assert_equal tgtlot3, child2, 'wrong child id'
    # STB 1st child
    assert child3 = @@sv.stb_lotfamily(lot, srclot, tgtlot), "failed to create child lot #{tgtlot}"
    assert_equal tgtlot, child3, 'wrong child id'
    # split parent lot, creates child #4
    assert child4 = @@sv.lot_split(lot, 2), 'failed to split parent'
    assert_equal tgtlot4, child4, 'wrong child id'
  end

  def test10_invalid_wafers
    assert srclot = @@svtest.new_srclot, 'error creating srclot'
    @@testlots << srclot
    assert lot = @@svtest.new_lot(nwafers: 5, srclot: srclot), 'error creating test lot'
    @@testlots << lot
    tgtlot = lot.next
    # try to STB
    lot_wafer = @@sv.lot_info(lot, wafers: true).wafers.first.wafer
    srclot_wafer = @@sv.lot_info(srclot, wafers: true).wafers.first.wafer
    refute @@sv.stb_lotfamily(lot, srclot, tgtlot, wafers: ['NOTFOUND']), "child lot #{tgtlot} creation should fail"
    refute @@sv.stb_lotfamily(lot, srclot, tgtlot, wafers: [lot_wafer]), "child lot #{tgtlot} creation should fail"
    refute @@sv.stb_lotfamily(lot, srclot, tgtlot, wafers: [srclot_wafer]), "child lot #{tgtlot} creation should fail"
  end

  def test11_different_sublottype
    assert srclot = @@svtest.new_srclot, 'error creating srclot'
    @@testlots << srclot
    assert lot = @@svtest.new_lot(nwafers: 5, srclot: srclot), 'error creating test lot'
    @@testlots << lot
    # try to STB and verify sublottype
    tgtlot = lot.next
    assert child = @@sv.stb_lotfamily(lot, srclot, tgtlot, sublottype: @@slt2), "failed to create child lot #{tgtlot}"
    lci = @@sv.lot_info(child)
    assert_equal 'Production', lci.lottype
    assert_equal @@slt2, lci.sublottype
    refute_equal @@sv.lot_info(lot).sublottype, lci.sublottype, 'wrong setup'
  end

  def test19_processhold  # make it last
    assert srclot = @@svtest.new_srclot, 'error creating srclot'
    @@testlots << srclot
    assert lot = @@svtest.new_lot(nwafers: 5, srclot: srclot), 'error creating test lot'
    @@testlots << lot
    # register processhold
    li = @@sv.lot_info(lot)
    assert_equal 0, @@sv.process_hold(@@reason, product: li.product, opNo: li.opNo, route: li.route), 'failed to register process hold'
    # STB and verify hold
    tgtlot = lot.next
    assert child = @@sv.stb_lotfamily(lot, srclot, tgtlot), "error creating child lot #{tgtlot}"
    assert_equal 'ONHOLD', @@sv.lot_info(tgtlot).status, 'wrong lot status'
  end

end
