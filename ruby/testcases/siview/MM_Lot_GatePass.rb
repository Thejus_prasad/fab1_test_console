=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: Daniel Steger, 2015-09-07

History:
  2016-07-12 sfrieske, added test for MSR 445643 (GatePass held lots)
  2019-12-09 sfrieske, minor cleanup, renamed from Test320_GatePass
=end

require 'SiViewTestCase'


# Test GatePass incl MES-2136, MSR 445643
class MM_Lot_GatePass < SiViewTestCase
  @@reason = 'AEHL'
  @@testlots = []
  
  def self.after_all_passed
    @@testlots.each {|lot| @@sv.delete_lot_family(lot)}
  end


  def test00_setup
    $setup_ok = false
    #
    assert lots = @@svtest.new_lots(2), 'error creating test lots'
    @@testlots += lots
    opes = @@sv.lot_operation_list(lots.first)
    assert @@opNo_skip = opes.find {|ope| !ope.mandatory}.opNo, 'non-mandatory PD not found'
    assert @@opNo_mand = opes.find {|ope| ope.mandatory}.opNo, 'mandatory PD not found'
    #
    $setup_ok = true
  end


  # gatepass at a non-mandatory operation

  def test11_gatepass
    @@testlots.each {|l| assert_equal 0, @@sv.lot_cleanup(l, opNo: @@opNo_skip)}
    assert_equal 0, @@sv.lot_gatepass(@@testlots), 'error gatepassing lots'
  end

  def test12_gatepass_onhold1_fail
    # prepare lots, set 1 ONHOLD
    @@testlots.each {|l| assert_equal 0, @@sv.lot_cleanup(l, opNo: @@opNo_skip)}
    lot = @@testlots.first
    assert_equal 0, @@sv.lot_hold(lot, @@reason)
    # gatepass lot1
    assert_equal 931, @@sv.lot_gatepass(lot), 'gatepass should fail'
    assert_match /0000183E:Lot #{lot} holdStatus ONHOLD is invalid/, @@sv.msg_text
    # gatepass both
    assert_equal 931, @@sv.lot_gatepass(@@testlots), 'gatepass should fail'
    assert_match /0010045W:Some lot #{lot} have failed in doing the request./, @@sv.msg_text
  end
  
  def test13_getpass_onhold2_fail
    # prepare lots, set both ONHOLD
    @@testlots.each {|l| 
      assert_equal 0, @@sv.lot_cleanup(l, opNo: @@opNo_skip)
      assert_equal 0, @@sv.lot_hold(l, @@reason)
    }
    assert_equal 931, @@sv.lot_gatepass(@@testlots), 'gatepass should fail'
    assert_match /.*0000183E:Lot .* holdStatus ONHOLD is invalid for this request.*/, @@sv.msg_text
  end

  def test14_getpass_onhold2_pass  # MSR445643
    # prepare lots, set both ONHOLD
    @@testlots.each {|l| 
      assert_equal 0, @@sv.lot_cleanup(l, opNo: @@opNo_skip)
      assert_equal 0, @@sv.lot_hold(l, @@reason)
    }
    # call CS_TxGatePassWithoutHoldReleaseReq (since C7.4)
    assert_equal 0, @@sv.lot_gatepass(@@testlots, force: true), 'gatepass failed'
  end
  
  
  # gatepass at a mandatory operation

  def test21_gatepass_mandatory
    # puts lots at a mandatory PD
    @@testlots.each {|l| assert_equal 0, @@sv.lot_cleanup(l, opNo: @@opNo_mand)}
    # try to gatepass
    assert_equal 302, @@sv.lot_gatepass(@@testlots), 'gatepass should fail'
    assert_match /.*0000112E:.* operation is a mandatory operation and you cannot pass it.*/, @@sv.msg_text
    # gatepass with mandatory allowed (F7 customization)
    assert_equal 0, @@sv.lot_gatepass(@@testlots, mandatory: true), 'gatepass failed'
  end

  def test22_gatepass_mandatory_onhold
    # puts lots at a mandatory PD and set them ONHOLD
    @@testlots.each {|l| assert_equal 0, 
      @@sv.lot_cleanup(l, opNo: @@opNo_mand)
      assert_equal 0, @@sv.lot_hold(l, @@reason)
    }
    # gatepass with mandatory allowed, new TX ..WithoutHoldRelease
    assert_equal 0, @@sv.lot_gatepass(@@testlots, force: true, mandatory: true), 'gatepass failed'
  end

end
