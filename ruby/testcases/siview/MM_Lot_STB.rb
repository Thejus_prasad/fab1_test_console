=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: sfrieske, 2018-05-14

History:
  2021-09-15 sfrieske, finally replaced $svtest by @@svtest, $sv by @@sv and $sm by @@sm
=end

require 'SiViewTestCase'


class MM_Lot_STB < SiViewTestCase

  def self.shutdown
    @@testlots.each {|lot| @@sv.delete_lot_family(lot)}
  end


  def test01_STB_eqpmonitor  # MES-3674 (seems to be Fab7 specific, works in C7.9)
    $setup_ok = false
    #
    @@testlots = []
    ['Dummy', 'Process Monitor', 'Equipment Monitor'].each {|lottype|
      assert lot = @@svtest.new_controllot(lottype, nwafers: 15, ondemand: false), 'error creating test lot'
      @@testlots << lot
      assert vlot = @@sv.stb_cancel(lot)
      @@testlots << vlot
    }
    #
    $setup_ok = true
  end


end
