=begin
(c) Copyright 2016 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: Daniel Steger <daniel.steger@globalfoundries.com>

Version: 1.0.2

History:
  2016-06-24 dsteger,  added testcase
  2017-04-04 sfrieske, use @@sv.delete_lot_family to delete lot in test99
  2018-05-04 sfrieske, changed exception inheritance in C7.9 (MSR 1147819)
  2019-12-09 sfrieske, minor cleanup, renamed from Test089_ProcessHoldException
  2020-05-25 sfrieske, added @@txdelay for MDS tests
=end

require 'SiViewTestCase'


# Test MSR 799187 Product and Route specific process holds, Cannot register multiple process holds for same route,
# MSR351455, 1078714, 1147819
class MM_ProcessHoldException < SiViewTestCase
  @@product2 = 'UT-PRODUCT002.01'
  @@opNo = '1000.200'
  @@reason = 'E-LA'
  @@rework_route = 'UTRW001.01'
  @@rework_opNo = '1000.400'
  @@rework_ret = '1000.400'
  @@psm_route = 'UTBR001.01'
  @@psm_ret = '1000.300'
  @@ph_movement = '1'
  @@single = false          # TODO: C7.10 just remove it, false is the default
  @@check_history = false   # MMDB query not yet imlemented

  @@txdelay = 0             # 130 for MDS checks not related to MM server tests
  @@testlots = []


  def self.after_all_passed
    @@testlots.each {|lot| @@sv.delete_lot_family(lot)}
  end

  def setup
    if !@@testlots.empty? && @@txdelay > 0
      $log.info "waiting #{@@txdelay} s for TX separtion"; sleep @@txdelay
    end
    @@testlots.each {|lot|
      assert @@sv.merge_lot_family(lot)
      assert_equal 0, @@sv.lot_cleanup(lot)
    }
  end

  def teardown
    ($log.info "waiting #{@@txdelay} s for TX separation"; sleep @@txdelay) if @@txdelay > 0
    [@@route, @@psm_route, @@rework_route].each {|r|
      @@sv.process_hold_list(route: r).each {|h| @@sv.process_hold_cancel(holdrecord: h)}
    }
  end


  def test00_setup
    $setup_ok = false
    #
    assert_equal @@ph_movement, @@sv.environment_variable[1]['SP_PROCESSHOLD_ALLOW_LOTMOVEMENT'], 'wrong MM env'
    #
    @@product = @@svtest.product
    @@route = @@svtest.route
    refute_equal @@product, @@product2, 'products must be different'
    #
    assert @@testlots = @@svtest.new_lots(2, product: @@product), 'lots not created'
    #
    $setup_ok = true
  end

  def test01_product_specific
    assert_equal 0, @@sv.process_hold(@@reason, product: @@product, opNo: @@opNo, route: @@route), 'failed to register process hold'
    assert_equal 1, @@sv.process_hold_list(route: @@route).count
    #
    check_processhold_exception(@@testlots, @@opNo, product: @@product, route: @@route)
  end

  # MES-3028
  def test02_route_specific
    assert_equal 0, @@sv.process_hold(@@reason, opNo: @@opNo, route: @@route), 'failed to register process hold'
    assert_equal 1, @@sv.process_hold_list(route: @@route).count
    #
    check_processhold_exception(@@testlots, @@opNo, product: nil, route: @@route)
  end

  def test03_multiple_exceptions_products
    assert_equal 0, @@sv.process_hold(@@reason, opNo: @@opNo, route: @@route, product: @@product), 'failed to register process hold'
    assert_equal 0, @@sv.process_hold(@@reason, opNo: @@opNo, route: @@route, product: @@product2), 'failed to register process hold'
    assert_equal 2, @@sv.process_hold_list(route: @@route).count
    #
    lot = @@testlots.first
    ($log.info "waiting #{@@txdelay} s for TX separtion"; sleep @@txdelay) if @@txdelay > 0
    assert_equal 0, @@sv.process_hold_exception(lot, route: @@route, product: @@product2, single: @@single), 'failed to register exception'
    ($log.info "waiting #{@@txdelay} s for TX separtion"; sleep @@txdelay) if @@txdelay > 0
    assert_equal 0, @@sv.lot_opelocate(lot, @@opNo), "failed to locate to process hold operation"
    assert_equal 'ONHOLD', @@sv.lot_info(lot).status, "process hold should be active for #{lot}"
    ($log.info "waiting #{@@txdelay} s for TX separtion"; sleep @@txdelay) if @@txdelay > 0
    assert_equal 0, @@sv.lot_hold_release(lot, nil), "failed to release hold"
    assert_equal 'Waiting', @@sv.lot_info(lot).status, "process hold should be active for #{lot}"
    ($log.info "waiting #{@@txdelay} s for TX separtion"; sleep @@txdelay) if @@txdelay > 0
    assert_equal 0, @@sv.process_hold_exception(lot, route: @@route, product: @@product, single: @@single), 'failed to register exception'
    ($log.info "waiting #{@@txdelay} s for TX separtion"; sleep @@txdelay) if @@txdelay > 0
    assert_equal 0, @@sv.lot_opelocate(lot, :first), "failed to locate to first operation"
    assert_equal 0, @@sv.lot_opelocate(lot, @@opNo), "failed to locate to process hold operation"
    assert_equal 'Waiting', @@sv.lot_info(lot).status, "process hold should be active for #{lot}"
    assert_equal (@@single ? 1 : 2), @@sv.process_hold_exception_list(lot: lot).count
  end

  def test04_processhold_split
    assert_equal 0, @@sv.process_hold(@@reason, opNo: @@opNo, route: @@route, product: @@product), 'failed to register process hold'
    assert_equal 1, @@sv.process_hold_list(route: @@route).count
    #
    lot = @@testlots.first
    ($log.info "waiting #{@@txdelay} s for TX separtion"; sleep @@txdelay) if @@txdelay > 0
    assert_equal 0, @@sv.process_hold_exception(lot, route: @@route, product: @@product, single: @@single), 'failed to register exception'
    ($log.info "waiting #{@@txdelay} s for TX separtion"; sleep @@txdelay) if @@txdelay > 0
    assert_equal 0, @@sv.lot_opelocate(lot, @@opNo), "failed to locate to process hold operation"
    assert_equal 'Waiting', @@sv.lot_info(lot).status, "process hold should not be active for #{lot}"
    ($log.info "waiting #{@@txdelay} s for TX separtion"; sleep @@txdelay) if @@txdelay > 0
    assert child = @@sv.lot_split(lot, 2), "failed to split the lot"
    assert_equal 'Waiting', @@sv.lot_info(lot).status, "process hold should not be active for #{lot}"
    exp = @@sv.environment_variable[1]['CS_SP_INHERIT_EXCEPTION_ON_SPLIT'] == 'ON' ? 'Waiting' : 'ONHOLD'  # MSR1147819
    assert_equal exp, @@sv.lot_info(child).status, "process hold should be active for #{child}"
  end

  def test05_processhold_cancel
    # route specific process hold
    assert_equal 0, @@sv.process_hold(@@reason, opNo: @@opNo, route: @@route, product: @@product), 'failed to register process hold'
    assert holdrec = @@sv.process_hold_list(route: @@route, product: @@product).first, "no hold record found"
    #
    lot = @@testlots.first
    ($log.info "waiting #{@@txdelay} s for TX separtion"; sleep @@txdelay) if @@txdelay > 0
    assert_equal 0, @@sv.process_hold_exception(lot, route: @@route, product: @@product, single: @@single), 'failed to register exception'
    assert_equal 1, @@sv.process_hold_exception_list(lot: lot).count
    ($log.info "waiting #{@@txdelay} s for TX separtion"; sleep @@txdelay) if @@txdelay > 0
    assert_equal 0, @@sv.process_hold_cancel(holdrecord: holdrec), "failed to cancel process hold"
    assert_equal 0, @@sv.process_hold_exception_list(lot: lot).count
  end

  def test06_psm
    lot = @@testlots.first
    assert_equal 0, @@sv.lot_experiment(lot, [2], @@psm_route, @@opNo, @@psm_ret)
    assert_equal 0, @@sv.process_hold(@@reason, opNo: @@psm_ret, route: @@route), "failed to set process hold"
    assert_equal 0, @@sv.lot_opelocate(lot, @@opNo), "failed to move to PSM split opNo"
    assert child = @@sv.lot_family(lot).last, "PSM failed"
    # register exceptions
    ($log.info "waiting #{@@txdelay} s for TX separtion"; sleep @@txdelay) if @@txdelay > 0
    assert_equal 0, @@sv.process_hold_exception(child, route: @@route, opNo: @@psm_ret, single: @@single), 'failed to register exception'
    ($log.info "waiting #{@@txdelay} s for TX separtion"; sleep @@txdelay) if @@txdelay > 0
    assert_equal 0, @@sv.lot_opelocate(lot, @@psm_ret), "failed to move to PSM join"
    assert_equal 'ONHOLD', @@sv.lot_info(lot).status, "#{lot} should have a process hold"
    assert_equal 'Waiting', @@sv.lot_info(child).status, "#{child} should not have a process hold"
    ($log.info "waiting #{@@txdelay} s for TX separtion"; sleep @@txdelay) if @@txdelay > 0
    assert_equal 0, @@sv.lot_hold_release(lot, nil), "failed to release parent"
  end

  def test07_branch
    lot = @@testlots.first
    assert rops = @@sv.route_operations(@@psm_route), 'wrong route'
    assert_equal 0, @@sv.process_hold(@@reason, opNo: rops.first.operationNumber, route: @@psm_route), "failed to set process hold at main pd join"
    assert_equal 0, @@sv.lot_experiment(lot, @@sv.lot_info(lot).nwafers, @@psm_route, @@opNo, @@psm_ret)
    # register exceptions
    ($log.info "waiting #{@@txdelay} s for TX separtion"; sleep @@txdelay) if @@txdelay > 0
    assert_equal 0, @@sv.process_hold_exception(lot, route: @@psm_route, single: @@single), 'failed to register exception'
    ($log.info "waiting #{@@txdelay} s for TX separtion"; sleep @@txdelay) if @@txdelay > 0
    assert_equal 0, @@sv.lot_opelocate(lot, @@opNo), "failed to move to PSM split"
    assert_equal 'Waiting', @@sv.lot_info(lot).status, "wrong status of lot #{lot}"
    ($log.info "waiting #{@@txdelay} s for TX separtion"; sleep @@txdelay) if @@txdelay > 0
    assert_equal 0, @@sv.lot_opelocate(lot, @@psm_ret), "failed to move to PSM join opNo"
  end

  def test08_partial_rework
    lot = @@testlots.first
    assert_equal 0, @@sv.lot_opelocate(lot, @@rework_opNo), "failed to move to rework"
    assert_equal 0, @@sv.process_hold(@@reason, opNo: @@rework_ret, product: @@product, route: @@route), "failed to set process hold at main pd join"
    assert child = @@sv.lot_partial_rework(lot, 1, @@rework_route, return_opNo: @@rework_ret, onhold: true, inheritholds: false), "failed to do partial rework"
    # register exceptions
    ($log.info "waiting #{@@txdelay} s for TX separtion"; sleep @@txdelay) if @@txdelay > 0
    assert_equal 0, @@sv.process_hold_exception(child, route: @@route, product: @@product, opNo: @@rework_ret, single: @@single), 'failed to register exception'
    ($log.info "waiting #{@@txdelay} s for TX separtion"; sleep @@txdelay) if @@txdelay > 0
    assert_equal 0, @@sv.lot_opelocate(child, @@rework_ret), "failed to move to PSM join"
    assert_empty @@sv.lot_hold_list(child, type: 'ProcessHold'), "#{child} should not have a process hold"
    ($log.info "waiting #{@@txdelay} s for TX separtion"; sleep @@txdelay) if @@txdelay > 0
    assert_equal 0, @@sv.lot_hold_release(lot, @@reason), "failed to release parent"
  end

  def test99_exception_deleted
    assert_equal 0, @@sv.process_hold(@@reason, product: @@product, opNo: @@opNo, route: @@route), 'failed to register process hold'
    refute_empty @@sv.process_hold_list(route: @@route)
    lot = @@testlots.last
    ($log.info "waiting #{@@txdelay} s for TX separtion"; sleep @@txdelay) if @@txdelay > 0
    assert_equal 0, @@sv.process_hold_exception(lot, route: @@route, product: @@product), 'failed to register exception'
    refute_empty @@sv.process_hold_exception_list(route: @@route, product: @@product), "wrong process hold exception count"
    ($log.info "waiting #{@@txdelay} s for TX separtion"; sleep @@txdelay) if @@txdelay > 0
    assert_equal 0, @@sv.delete_lot_family(lot, delete: true), "unable to delete lot"
    assert_empty @@sv.process_hold_exception_list(route: @@route, product: @@product), "wrong process hold exception count"
  end


  # aux methods
  
  def check_processhold_exception(lots, opNo, product: nil, route: nil)
    ($log.info "waiting #{@@txdelay} s for TX separtion"; sleep @@txdelay) if @@txdelay > 0
    txstart = Time.now
    lots.each {|lot|
      assert_equal 0, @@sv.process_hold_exception(lot, route: route, product: product, single: @@single), "failed to register process hold for #{lot}"
      assert_equal 0, @@sv.process_hold_exception(lot, route: route, product: product, single: @@single), "failed to register process hold for #{lot}"
      assert_match /0010137E:Lot .* has been already registered. /, @@sv.msg_text
    }
    assert_equal lots.count, @@sv.process_hold_exception_list(route: route, product: product).count, "wrong process hold exception count"
    # check cancel
    ($log.info "waiting #{@@txdelay} s for TX separtion"; sleep @@txdelay) if @@txdelay > 0
    assert_equal 0, @@sv.process_hold_exception_cancel(lots: lots), "failed to cancel process hold"
    ($log.info "waiting #{@@txdelay} s for TX separtion"; sleep @@txdelay) if @@txdelay > 0
    lots.each {|lot|
      assert_equal 0, @@sv.process_hold_exception(lot, route: route, product: product, single: @@single), "failed to register process hold for #{lot}"
    }
    assert_equal lots.count, @@sv.process_hold_exception_list(route: route, product: product).count, "wrong process hold exception count"
    # check locate
    ($log.info "waiting #{@@txdelay} s for TX separtion"; sleep @@txdelay) if @@txdelay > 0
    lots.each {|lot|
      assert_equal 0, @@sv.lot_opelocate(lot, @@opNo), "failed to locate to process hold operation"
      assert_equal 'Waiting', @@sv.lot_info(lot).status, "process hold should be active for #{lot}"
      assert @@sv.process_hold_exception_list(lot: lot).first.usedFlag, "process hold exception should be used up" unless @@single
    }
    assert_equal (@@single ? 0 : lots.count), @@sv.process_hold_exception_list(lot: lots).count, "wrong process hold exception count"
    # check re-application
    ($log.info "waiting #{@@txdelay} s for TX separtion"; sleep @@txdelay) if @@txdelay > 0
    lots.each {|lot|
      assert_equal 0, @@sv.lot_opelocate(lot, :first), "failed to locate to process hold operation"
      assert_equal 0, @@sv.lot_opelocate(lot, @@opNo), "failed to locate to process hold operation"
      assert_equal (@@single ? 'ONHOLD' : 'Waiting'), @@sv.lot_info(lot).status, "wrong hold status for lot #{lot}"
    }
    #
    # if @@check_history   # not yet implemented
    #   lots.each {|lot|
    #     entries = $mmdb.process_exception_history(lot, tstart: txstart)
    #     assert_equal (@@single ? 4 : 3), entries.count, "wrong number of history entries"
    #   }
    # end
  end

end
