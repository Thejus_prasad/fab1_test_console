=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: Steffen Steidten, 2014-12-16

History:
  2019-12-05 sfrieske, minor cleanup, renamed from Test_Cleanup
=end

require 'SiViewTestCase'

# Delete leftover lots in test carriers
class SV_TestCleanup < SiViewTestCase

  def test00_cleanup
    cc = @@sv.carrier_list(carrier: @@svtest.carriers)
    cc.each {|c| @@sv.lot_list_incassette(c).each {|lot| @@sv.delete_lot_family(lot)}}
  end

end
