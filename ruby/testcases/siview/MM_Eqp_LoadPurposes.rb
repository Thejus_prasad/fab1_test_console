=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: Paul Peschel, 2013-01-10 migrate from Test040_LoadPurposeTypes.java

History:
  2015-06-24 ssteidte, merged with junit_testing repository and verified with R15
  2019-12-09 sfrieske, minor cleanup, renamed from Test040_LoadPurposeTypes
  2020-09-07 sfrieske, minor cleanup
  2021-08-27 sfrieske, split verify_equal off of misc.rb
=end

require 'util/verifyequal'
require 'SiViewTestCase'


# Test carrier loading for different load purposes
class MM_Eqp_LoadPurposes < SiViewTestCase
  @@eqp = 'UTF002'
  @@opNo = '1000.500'
  @@opNo_eqpmonitor = '100.100'

  CassetteLoadPurposes = ['Process Lot', 'Process Monitor Lot', 'Empty Cassette', 'Filler Dummy Lot', 'Side Dummy Lot', 'Waiting Monitor Lot', 'Other']

  RC_OK = 0
  RC_CAST_NOT_EMPTY = 310
  RC_INVALID_PROCMONITOR_COUNT = 2913
  RC_INVALID_PORT_LOADPURPOSE_FOR_NPW_XFER = 2965
  RC_LOT_BANK_DIFFERENT = 1237
  RC_LOT_NOT_IN_BANK = 1214
  RC_NOT_CANDIDATE_LOT_FOR_OPE_START = 2800
  RC_NOT_FOUND_LOT = 1450
  RC_NOT_SAME_LOAD_PURPOSE = 1523
  RC_INVALID_LOT_INVENTORYSTAT = 932

  RC_PROCESS = [
    # /* Process Lot */   /* Process Monitor */   /* Empty Cass */    /* Filler Dummy */    /* Side Dummy */    /* Waiting Monitor */   /* Other */
    [RC_OK, RC_INVALID_PROCMONITOR_COUNT, RC_CAST_NOT_EMPTY, RC_NOT_SAME_LOAD_PURPOSE, RC_NOT_SAME_LOAD_PURPOSE, RC_NOT_SAME_LOAD_PURPOSE, RC_OK],                # Any Purpose
    [RC_OK, RC_INVALID_PROCMONITOR_COUNT, RC_CAST_NOT_EMPTY, RC_NOT_SAME_LOAD_PURPOSE, RC_NOT_SAME_LOAD_PURPOSE, RC_NOT_SAME_LOAD_PURPOSE, RC_OK],                # Process Lot
    [RC_NOT_SAME_LOAD_PURPOSE, RC_NOT_SAME_LOAD_PURPOSE, RC_CAST_NOT_EMPTY, RC_NOT_SAME_LOAD_PURPOSE, RC_NOT_SAME_LOAD_PURPOSE, RC_NOT_SAME_LOAD_PURPOSE, RC_OK], # Empty Cass
    [RC_NOT_SAME_LOAD_PURPOSE, RC_NOT_SAME_LOAD_PURPOSE, RC_CAST_NOT_EMPTY, RC_LOT_NOT_IN_BANK, RC_NOT_SAME_LOAD_PURPOSE, RC_NOT_SAME_LOAD_PURPOSE, RC_OK],       # Filler Dummy
    [RC_NOT_SAME_LOAD_PURPOSE, RC_NOT_SAME_LOAD_PURPOSE, RC_CAST_NOT_EMPTY, RC_NOT_SAME_LOAD_PURPOSE, RC_LOT_NOT_IN_BANK, RC_NOT_SAME_LOAD_PURPOSE, RC_OK],       # Side Dummy
    [RC_NOT_SAME_LOAD_PURPOSE, RC_NOT_SAME_LOAD_PURPOSE, RC_CAST_NOT_EMPTY, RC_NOT_SAME_LOAD_PURPOSE, RC_NOT_SAME_LOAD_PURPOSE, RC_LOT_NOT_IN_BANK, RC_OK],       # Waiting Mon
    [RC_NOT_SAME_LOAD_PURPOSE, RC_NOT_SAME_LOAD_PURPOSE, RC_CAST_NOT_EMPTY, RC_NOT_SAME_LOAD_PURPOSE, RC_NOT_SAME_LOAD_PURPOSE, RC_NOT_SAME_LOAD_PURPOSE, RC_OK], # Other
  ]

  RC_PROCESS_NPW = [
    # /* Process Lot */   /* Process Monitor */   /* Empty Cass */    /* Filler Dummy */    /* Side Dummy */    /* Waiting Monitor */   /* Other */
    [RC_OK, RC_OK, RC_OK, RC_NOT_SAME_LOAD_PURPOSE, RC_NOT_SAME_LOAD_PURPOSE, RC_NOT_SAME_LOAD_PURPOSE, RC_OK], # Any Purpose
    [RC_INVALID_PORT_LOADPURPOSE_FOR_NPW_XFER, RC_INVALID_PORT_LOADPURPOSE_FOR_NPW_XFER, RC_INVALID_PORT_LOADPURPOSE_FOR_NPW_XFER, RC_INVALID_PORT_LOADPURPOSE_FOR_NPW_XFER, RC_INVALID_PORT_LOADPURPOSE_FOR_NPW_XFER, RC_INVALID_PORT_LOADPURPOSE_FOR_NPW_XFER, RC_INVALID_PORT_LOADPURPOSE_FOR_NPW_XFER], # Process Lot
    [RC_INVALID_PORT_LOADPURPOSE_FOR_NPW_XFER, RC_INVALID_PORT_LOADPURPOSE_FOR_NPW_XFER, RC_INVALID_PORT_LOADPURPOSE_FOR_NPW_XFER, RC_INVALID_PORT_LOADPURPOSE_FOR_NPW_XFER, RC_INVALID_PORT_LOADPURPOSE_FOR_NPW_XFER, RC_INVALID_PORT_LOADPURPOSE_FOR_NPW_XFER, RC_INVALID_PORT_LOADPURPOSE_FOR_NPW_XFER], # Empty Cass
    [RC_NOT_SAME_LOAD_PURPOSE, RC_NOT_SAME_LOAD_PURPOSE, RC_NOT_SAME_LOAD_PURPOSE, RC_INVALID_LOT_INVENTORYSTAT, RC_NOT_SAME_LOAD_PURPOSE, RC_NOT_SAME_LOAD_PURPOSE, RC_NOT_SAME_LOAD_PURPOSE],                                        # Filler Dummy
    [RC_NOT_SAME_LOAD_PURPOSE, RC_NOT_SAME_LOAD_PURPOSE, RC_NOT_SAME_LOAD_PURPOSE, RC_NOT_SAME_LOAD_PURPOSE, RC_INVALID_LOT_INVENTORYSTAT, RC_NOT_SAME_LOAD_PURPOSE, RC_NOT_SAME_LOAD_PURPOSE],                                        # Side Dummy
    [RC_NOT_SAME_LOAD_PURPOSE, RC_NOT_SAME_LOAD_PURPOSE, RC_NOT_SAME_LOAD_PURPOSE, RC_NOT_SAME_LOAD_PURPOSE, RC_NOT_SAME_LOAD_PURPOSE, RC_INVALID_LOT_INVENTORYSTAT, RC_NOT_SAME_LOAD_PURPOSE],                        # Waiting Mon
    [RC_OK, RC_OK, RC_OK, RC_OK, RC_OK, RC_OK, RC_OK],                     # Other
  ]

  RC_EMPTY = [
    # /* Process Lot */   /* Process Monitor */   /* Empty Cass */    /* Filler Dummy */    /* Side Dummy */    /* Waiting Monitor */   /* Other */
    [RC_NOT_FOUND_LOT, RC_NOT_FOUND_LOT, RC_OK, RC_NOT_FOUND_LOT, RC_NOT_FOUND_LOT, RC_NOT_FOUND_LOT, RC_OK],                    # Any Purpose
    [RC_NOT_FOUND_LOT, RC_NOT_FOUND_LOT, RC_NOT_SAME_LOAD_PURPOSE, RC_NOT_FOUND_LOT, RC_NOT_FOUND_LOT, RC_NOT_FOUND_LOT, RC_OK], # Process Lot
    [RC_NOT_FOUND_LOT, RC_NOT_FOUND_LOT, RC_OK, RC_NOT_FOUND_LOT, RC_NOT_FOUND_LOT, RC_NOT_FOUND_LOT, RC_OK],                    # Empty Cass
    [RC_NOT_FOUND_LOT, RC_NOT_FOUND_LOT, RC_NOT_SAME_LOAD_PURPOSE, RC_NOT_FOUND_LOT, RC_NOT_FOUND_LOT, RC_NOT_FOUND_LOT, RC_OK], # Filler Dummy
    [RC_NOT_FOUND_LOT, RC_NOT_FOUND_LOT, RC_NOT_SAME_LOAD_PURPOSE, RC_NOT_FOUND_LOT, RC_NOT_FOUND_LOT, RC_NOT_FOUND_LOT, RC_OK], # Side Dummy
    [RC_NOT_FOUND_LOT, RC_NOT_FOUND_LOT, RC_NOT_SAME_LOAD_PURPOSE, RC_NOT_FOUND_LOT, RC_NOT_FOUND_LOT, RC_NOT_FOUND_LOT, RC_OK], # Waiting Mon
    [RC_NOT_FOUND_LOT, RC_NOT_FOUND_LOT, RC_OK, RC_NOT_FOUND_LOT, RC_NOT_FOUND_LOT, RC_NOT_FOUND_LOT, RC_OK],                    # Other
  ]

  RC_EMPTY_NPW = [
    # /* Process Lot */   /* Process Monitor */   /* Empty Cass */    /* Filler Dummy */    /* Side Dummy */    /* Waiting Monitor */   /* Other */
    [RC_OK, RC_OK, RC_OK, RC_NOT_SAME_LOAD_PURPOSE, RC_NOT_SAME_LOAD_PURPOSE, RC_NOT_SAME_LOAD_PURPOSE, RC_OK], # Any Purpose
    [RC_INVALID_PORT_LOADPURPOSE_FOR_NPW_XFER, RC_INVALID_PORT_LOADPURPOSE_FOR_NPW_XFER, RC_INVALID_PORT_LOADPURPOSE_FOR_NPW_XFER, RC_INVALID_PORT_LOADPURPOSE_FOR_NPW_XFER, RC_INVALID_PORT_LOADPURPOSE_FOR_NPW_XFER, RC_INVALID_PORT_LOADPURPOSE_FOR_NPW_XFER, RC_INVALID_PORT_LOADPURPOSE_FOR_NPW_XFER], # Process Lot
    [RC_INVALID_PORT_LOADPURPOSE_FOR_NPW_XFER, RC_INVALID_PORT_LOADPURPOSE_FOR_NPW_XFER, RC_INVALID_PORT_LOADPURPOSE_FOR_NPW_XFER, RC_INVALID_PORT_LOADPURPOSE_FOR_NPW_XFER, RC_INVALID_PORT_LOADPURPOSE_FOR_NPW_XFER, RC_INVALID_PORT_LOADPURPOSE_FOR_NPW_XFER, RC_INVALID_PORT_LOADPURPOSE_FOR_NPW_XFER], # Empty Cass
    [RC_NOT_SAME_LOAD_PURPOSE, RC_NOT_SAME_LOAD_PURPOSE, RC_NOT_SAME_LOAD_PURPOSE, RC_OK, RC_NOT_SAME_LOAD_PURPOSE, RC_NOT_SAME_LOAD_PURPOSE, RC_NOT_SAME_LOAD_PURPOSE],                                        # Filler Dummy
    [RC_NOT_SAME_LOAD_PURPOSE, RC_NOT_SAME_LOAD_PURPOSE, RC_NOT_SAME_LOAD_PURPOSE, RC_NOT_SAME_LOAD_PURPOSE, RC_OK, RC_NOT_SAME_LOAD_PURPOSE, RC_NOT_SAME_LOAD_PURPOSE],                                        # Side Dummy
    [RC_NOT_SAME_LOAD_PURPOSE, RC_NOT_SAME_LOAD_PURPOSE, RC_NOT_SAME_LOAD_PURPOSE, RC_NOT_SAME_LOAD_PURPOSE, RC_NOT_SAME_LOAD_PURPOSE, RC_OK, RC_NOT_SAME_LOAD_PURPOSE],                        # Waiting Mon
    [RC_OK, RC_OK, RC_OK, RC_OK, RC_OK, RC_OK, RC_OK],                     # Other
  ]

  RC_DUMMY = [
    # /* Process Lot */   /* Process Monitor */   /* Empty Cass */    /* Filler Dummy */    /* Side Dummy */    /* Waiting Monitor */   /* Other */
    [RC_NOT_CANDIDATE_LOT_FOR_OPE_START, RC_NOT_CANDIDATE_LOT_FOR_OPE_START, RC_CAST_NOT_EMPTY, RC_NOT_SAME_LOAD_PURPOSE, RC_NOT_SAME_LOAD_PURPOSE, RC_NOT_SAME_LOAD_PURPOSE, RC_OK], # Any Purpose
    [RC_NOT_CANDIDATE_LOT_FOR_OPE_START, RC_NOT_CANDIDATE_LOT_FOR_OPE_START, RC_CAST_NOT_EMPTY, RC_NOT_SAME_LOAD_PURPOSE, RC_NOT_SAME_LOAD_PURPOSE, RC_NOT_SAME_LOAD_PURPOSE, RC_OK], # Process Lot
    [RC_NOT_SAME_LOAD_PURPOSE, RC_NOT_SAME_LOAD_PURPOSE, RC_CAST_NOT_EMPTY, RC_NOT_SAME_LOAD_PURPOSE, RC_NOT_SAME_LOAD_PURPOSE, RC_NOT_SAME_LOAD_PURPOSE, RC_OK],                     # Empty Cass
    [RC_NOT_SAME_LOAD_PURPOSE, RC_NOT_SAME_LOAD_PURPOSE, RC_CAST_NOT_EMPTY, RC_OK, RC_NOT_SAME_LOAD_PURPOSE, RC_NOT_SAME_LOAD_PURPOSE, RC_OK],                                        # Filler Dummy
    [RC_NOT_SAME_LOAD_PURPOSE, RC_NOT_SAME_LOAD_PURPOSE, RC_CAST_NOT_EMPTY, RC_NOT_SAME_LOAD_PURPOSE, RC_OK, RC_NOT_SAME_LOAD_PURPOSE, RC_OK],                                        # Side Dummy
    [RC_NOT_SAME_LOAD_PURPOSE, RC_NOT_SAME_LOAD_PURPOSE, RC_CAST_NOT_EMPTY, RC_NOT_SAME_LOAD_PURPOSE, RC_NOT_SAME_LOAD_PURPOSE, RC_LOT_BANK_DIFFERENT, RC_OK],                        # Waiting Mon
    [RC_NOT_SAME_LOAD_PURPOSE, RC_NOT_SAME_LOAD_PURPOSE, RC_CAST_NOT_EMPTY, RC_NOT_SAME_LOAD_PURPOSE, RC_NOT_SAME_LOAD_PURPOSE, RC_NOT_SAME_LOAD_PURPOSE, RC_OK],                     # Other
  ]

  RC_DUMMY_NPW = [
    # /* Process Lot */   /* Process Monitor */   /* Empty Cass */    /* Filler Dummy */    /* Side Dummy */    /* Waiting Monitor */   /* Other */
    [RC_OK, RC_OK, RC_OK, RC_NOT_SAME_LOAD_PURPOSE, RC_NOT_SAME_LOAD_PURPOSE, RC_NOT_SAME_LOAD_PURPOSE, RC_OK], # Any Purpose
    [RC_INVALID_PORT_LOADPURPOSE_FOR_NPW_XFER, RC_INVALID_PORT_LOADPURPOSE_FOR_NPW_XFER, RC_INVALID_PORT_LOADPURPOSE_FOR_NPW_XFER, RC_INVALID_PORT_LOADPURPOSE_FOR_NPW_XFER, RC_INVALID_PORT_LOADPURPOSE_FOR_NPW_XFER, RC_INVALID_PORT_LOADPURPOSE_FOR_NPW_XFER, RC_INVALID_PORT_LOADPURPOSE_FOR_NPW_XFER], # Process Lot
    [RC_INVALID_PORT_LOADPURPOSE_FOR_NPW_XFER, RC_INVALID_PORT_LOADPURPOSE_FOR_NPW_XFER, RC_INVALID_PORT_LOADPURPOSE_FOR_NPW_XFER, RC_INVALID_PORT_LOADPURPOSE_FOR_NPW_XFER, RC_INVALID_PORT_LOADPURPOSE_FOR_NPW_XFER, RC_INVALID_PORT_LOADPURPOSE_FOR_NPW_XFER, RC_INVALID_PORT_LOADPURPOSE_FOR_NPW_XFER], # Empty Cass
    [RC_NOT_SAME_LOAD_PURPOSE, RC_NOT_SAME_LOAD_PURPOSE, RC_NOT_SAME_LOAD_PURPOSE, RC_OK, RC_NOT_SAME_LOAD_PURPOSE, RC_NOT_SAME_LOAD_PURPOSE, RC_NOT_SAME_LOAD_PURPOSE],                                        # Filler Dummy
    [RC_NOT_SAME_LOAD_PURPOSE, RC_NOT_SAME_LOAD_PURPOSE, RC_NOT_SAME_LOAD_PURPOSE, RC_NOT_SAME_LOAD_PURPOSE, RC_OK, RC_NOT_SAME_LOAD_PURPOSE, RC_NOT_SAME_LOAD_PURPOSE],                                        # Side Dummy
    [RC_NOT_SAME_LOAD_PURPOSE, RC_NOT_SAME_LOAD_PURPOSE, RC_NOT_SAME_LOAD_PURPOSE, RC_NOT_SAME_LOAD_PURPOSE, RC_NOT_SAME_LOAD_PURPOSE, RC_LOT_BANK_DIFFERENT, RC_NOT_SAME_LOAD_PURPOSE],                        # Waiting Mon
    [RC_OK, RC_OK, RC_OK, RC_OK, RC_OK, RC_OK, RC_OK],                     # Other
  ]

  RC_MONITOR = [
    # /* Process Lot */   /* Process Monitor */   /* Empty Cass */    /* Filler Dummy */    /* Side Dummy */    /* Waiting Monitor */   /* Other */
    [RC_OK, RC_INVALID_PROCMONITOR_COUNT,RC_CAST_NOT_EMPTY, RC_NOT_SAME_LOAD_PURPOSE, RC_NOT_SAME_LOAD_PURPOSE, RC_NOT_SAME_LOAD_PURPOSE, RC_OK],                 # Any Purpose
    [RC_OK, RC_INVALID_PROCMONITOR_COUNT,RC_CAST_NOT_EMPTY, RC_NOT_SAME_LOAD_PURPOSE, RC_NOT_SAME_LOAD_PURPOSE, RC_NOT_SAME_LOAD_PURPOSE, RC_OK],                 # Process Lot
    [RC_NOT_SAME_LOAD_PURPOSE, RC_NOT_SAME_LOAD_PURPOSE, RC_CAST_NOT_EMPTY, RC_NOT_SAME_LOAD_PURPOSE, RC_NOT_SAME_LOAD_PURPOSE, RC_NOT_SAME_LOAD_PURPOSE, RC_OK], # Empty Cass
    [RC_NOT_SAME_LOAD_PURPOSE, RC_NOT_SAME_LOAD_PURPOSE, RC_CAST_NOT_EMPTY, RC_LOT_NOT_IN_BANK, RC_NOT_SAME_LOAD_PURPOSE, RC_NOT_SAME_LOAD_PURPOSE, RC_OK],       # Filler Dummy
    [RC_NOT_SAME_LOAD_PURPOSE, RC_NOT_SAME_LOAD_PURPOSE, RC_CAST_NOT_EMPTY, RC_NOT_SAME_LOAD_PURPOSE, RC_LOT_NOT_IN_BANK, RC_NOT_SAME_LOAD_PURPOSE, RC_OK],       # Side Dummy
    [RC_NOT_SAME_LOAD_PURPOSE, RC_NOT_SAME_LOAD_PURPOSE, RC_CAST_NOT_EMPTY, RC_NOT_SAME_LOAD_PURPOSE, RC_NOT_SAME_LOAD_PURPOSE, RC_LOT_NOT_IN_BANK, RC_OK],       # Waiting Mon
    [RC_NOT_SAME_LOAD_PURPOSE, RC_NOT_SAME_LOAD_PURPOSE, RC_CAST_NOT_EMPTY, RC_NOT_SAME_LOAD_PURPOSE, RC_NOT_SAME_LOAD_PURPOSE, RC_NOT_SAME_LOAD_PURPOSE, RC_OK], # Other
  ]

  RC_MONITOR_NPW = RC_PROCESS_NPW # same result codes as for process lot

  RC_WAITINGMONITOR = [
    # /* Process Lot */   /* Process Monitor */   /* Empty Cass */    /* Filler Dummy */    /* Side Dummy */    /* Waiting Monitor */   /* Other */
    [RC_NOT_CANDIDATE_LOT_FOR_OPE_START, RC_NOT_CANDIDATE_LOT_FOR_OPE_START, RC_CAST_NOT_EMPTY, RC_NOT_SAME_LOAD_PURPOSE, RC_NOT_SAME_LOAD_PURPOSE, RC_NOT_SAME_LOAD_PURPOSE, RC_OK], # Any Purpose
    [RC_NOT_CANDIDATE_LOT_FOR_OPE_START, RC_NOT_CANDIDATE_LOT_FOR_OPE_START, RC_CAST_NOT_EMPTY, RC_NOT_SAME_LOAD_PURPOSE, RC_NOT_SAME_LOAD_PURPOSE, RC_NOT_SAME_LOAD_PURPOSE, RC_OK], # Process Lot
    [RC_NOT_SAME_LOAD_PURPOSE, RC_NOT_SAME_LOAD_PURPOSE, RC_CAST_NOT_EMPTY, RC_NOT_SAME_LOAD_PURPOSE, RC_NOT_SAME_LOAD_PURPOSE, RC_NOT_SAME_LOAD_PURPOSE, RC_OK],                     # Empty Cass
    [RC_NOT_SAME_LOAD_PURPOSE, RC_NOT_SAME_LOAD_PURPOSE, RC_CAST_NOT_EMPTY, RC_LOT_BANK_DIFFERENT, RC_NOT_SAME_LOAD_PURPOSE, RC_NOT_SAME_LOAD_PURPOSE, RC_OK],                        # Filler Dummy
    [RC_NOT_SAME_LOAD_PURPOSE, RC_NOT_SAME_LOAD_PURPOSE, RC_CAST_NOT_EMPTY, RC_NOT_SAME_LOAD_PURPOSE, RC_LOT_BANK_DIFFERENT, RC_NOT_SAME_LOAD_PURPOSE, RC_OK],                        # Side Dummy
    [RC_NOT_SAME_LOAD_PURPOSE, RC_NOT_SAME_LOAD_PURPOSE, RC_CAST_NOT_EMPTY, RC_NOT_SAME_LOAD_PURPOSE, RC_NOT_SAME_LOAD_PURPOSE, RC_OK, RC_OK],                                        # Waiting Mon
    [RC_NOT_SAME_LOAD_PURPOSE, RC_NOT_SAME_LOAD_PURPOSE, RC_CAST_NOT_EMPTY, RC_NOT_SAME_LOAD_PURPOSE, RC_NOT_SAME_LOAD_PURPOSE, RC_NOT_SAME_LOAD_PURPOSE, RC_OK],                     # Other
  ]

  RC_WAITINGMONITOR_NPW = [
    # /* Process Lot */   /* Process Monitor */   /* Empty Cass */    /* Filler Dummy */    /* Side Dummy */    /* Waiting Monitor */   /* Other */
    [RC_OK, RC_OK, RC_OK, RC_NOT_SAME_LOAD_PURPOSE, RC_NOT_SAME_LOAD_PURPOSE, RC_NOT_SAME_LOAD_PURPOSE, RC_OK], # Any Purpose
    [RC_INVALID_PORT_LOADPURPOSE_FOR_NPW_XFER, RC_INVALID_PORT_LOADPURPOSE_FOR_NPW_XFER, RC_INVALID_PORT_LOADPURPOSE_FOR_NPW_XFER, RC_INVALID_PORT_LOADPURPOSE_FOR_NPW_XFER, RC_INVALID_PORT_LOADPURPOSE_FOR_NPW_XFER, RC_INVALID_PORT_LOADPURPOSE_FOR_NPW_XFER, RC_INVALID_PORT_LOADPURPOSE_FOR_NPW_XFER], # Process Lot
    [RC_INVALID_PORT_LOADPURPOSE_FOR_NPW_XFER, RC_INVALID_PORT_LOADPURPOSE_FOR_NPW_XFER, RC_INVALID_PORT_LOADPURPOSE_FOR_NPW_XFER, RC_INVALID_PORT_LOADPURPOSE_FOR_NPW_XFER, RC_INVALID_PORT_LOADPURPOSE_FOR_NPW_XFER, RC_INVALID_PORT_LOADPURPOSE_FOR_NPW_XFER, RC_INVALID_PORT_LOADPURPOSE_FOR_NPW_XFER], # Empty Cass
    [RC_NOT_SAME_LOAD_PURPOSE, RC_NOT_SAME_LOAD_PURPOSE, RC_NOT_SAME_LOAD_PURPOSE, RC_LOT_BANK_DIFFERENT, RC_NOT_SAME_LOAD_PURPOSE, RC_NOT_SAME_LOAD_PURPOSE, RC_NOT_SAME_LOAD_PURPOSE],                                        # Filler Dummy
    [RC_NOT_SAME_LOAD_PURPOSE, RC_NOT_SAME_LOAD_PURPOSE, RC_NOT_SAME_LOAD_PURPOSE, RC_NOT_SAME_LOAD_PURPOSE, RC_LOT_BANK_DIFFERENT, RC_NOT_SAME_LOAD_PURPOSE, RC_NOT_SAME_LOAD_PURPOSE],                                        # Side Dummy
    [RC_NOT_SAME_LOAD_PURPOSE, RC_NOT_SAME_LOAD_PURPOSE, RC_NOT_SAME_LOAD_PURPOSE, RC_NOT_SAME_LOAD_PURPOSE, RC_NOT_SAME_LOAD_PURPOSE, RC_OK, RC_NOT_SAME_LOAD_PURPOSE],                        # Waiting Mon
    [RC_OK, RC_OK, RC_OK, RC_OK, RC_OK, RC_OK, RC_OK],                     # Other
  ]

  @@testlots = []


  def self.after_all_passed
    @@testlots.each {|lot| @@sv.delete_lot_family(lot)}
  end

  def teardown
    @@sv.eqp_cleanup(@@eqp)
  end


  def test00_setup
    $setup_ok = false
    #
    # ensure eqp has all port load types configured exactly in the order of the cassette load purposes and clean it up
    eqpi = @@sv.eqp_info(@@eqp)
    CassetteLoadPurposes.each_with_index {|purpose, i|
      purpose = 'Any Purpose' if purpose == 'Process Lot'
      purpose = 'Process Lot' if purpose == 'Process Monitor Lot'
      assert_equal purpose, eqpi.ports[i].purpose, "wrong load purpose of #{@@eqp} port #{eqpi.ports[i]}, change eqp setup!"
    }
    assert @@svtest.cleanup_eqp(@@eqp, mode: 'Off-Line-1')
    #
    # get a process lot
    assert lot = @@svtest.new_lot, "error creating test lot"
    @@testlots << lot
    assert_equal 0, @@sv.lot_cleanup(lot, opNo: @@opNo)
    @@processLotCarrier = @@sv.lot_info(lot).carrier
    $log.info "process lot: #{lot}, carrier: #{@@processLotCarrier}"
    #
    # get monitor lots
    assert lot = @@svtest.new_controllot('Dummy', bankin: true)
    @@testlots << lot
    @@dummyLotCarrier = @@sv.lot_info(lot).carrier
    assert lot = @@svtest.new_controllot('Process Monitor', bankin: true)
    @@testlots << lot
    @@waitingMonitorLotCarrier = @@sv.lot_info(lot).carrier
    assert lot = @@svtest.new_controllot('Equipment Monitor')
    @@testlots << lot
    assert_equal 0, @@sv.lot_cleanup(lot, opNo: @@opNo_eqpmonitor)
    @@monitorLotCarrier = @@sv.lot_info(lot).carrier
    #
    # find empty carrier
    assert @@emptyCarrier = @@svtest.get_empty_carrier, "no empty carrier found"
    #
    $setup_ok = true
  end

  def test11_ProcessLot
    assert _run_test_load(@@processLotCarrier, RC_PROCESS)
  end

  def test12_ProcessLot_NPW
    assert _run_test_npw(@@processLotCarrier, RC_PROCESS_NPW, RC_PROCESS)
  end

  def test21_EmptyCarrier
    assert _run_test_load(@@emptyCarrier, RC_EMPTY)
  end

  def test22_EmptyCarrier_NPW
    assert _run_test_npw(@@emptyCarrier, RC_EMPTY_NPW, RC_EMPTY)
  end

  def test31_DummyLotCarrier
    assert _run_test_load(@@dummyLotCarrier, RC_DUMMY)
  end

  def test32_DummyLotCarrier_NPW
    assert _run_test_npw(@@dummyLotCarrier, RC_DUMMY_NPW, RC_DUMMY)
  end

  def test41_EquipmentMonitorLotCarrier
    assert _run_test_load(@@monitorLotCarrier, RC_MONITOR)
  end

  def test42_EquipmentMonitorLotCarrier_NPW
    assert _run_test_npw(@@monitorLotCarrier, RC_MONITOR_NPW, RC_MONITOR)
  end

  def test51_WaitingMonitorLotCarrier
    assert _run_test_load(@@waitingMonitorLotCarrier, RC_WAITINGMONITOR)
  end

  def test52_WaitingMonitorLotCarrier_NPW
    assert _run_test_npw(@@waitingMonitorLotCarrier, RC_WAITINGMONITOR_NPW, RC_WAITINGMONITOR)
  end

  # aux methods

  def _run_test_load(carrier, rc_list_load)
    ok = true
    assert_equal 0, @@sv.eqp_cleanup(@@eqp)
    assert @@svtest.cleanup_carrier(carrier)
    assert_equal 0, @@sv.carrier_xfer_status_change(carrier, 'EO', @@eqp)
    #
    eqpi = @@sv.eqp_info(@@eqp)
    eqpi.ports.each_with_index {|p, iport|
      $log.info "\n\n---- port load purpose #{p.purpose.inspect}"
      CassetteLoadPurposes.each_with_index {|purpose, ipurpose|
        $log.info "-- carrier load purpose #{purpose.inspect}"
        # load carrier and verify return code
        rc_load = @@sv.eqp_load(@@eqp, p.port, carrier, ib: eqpi.ib, purpose: purpose)
        ok &= verify_equal(rc_list_load[iport][ipurpose], rc_load, "wrong rc for LoadingLotRpt: port purpose #{p.purpose}, carrier load purpose #{purpose.inspect} [#{@@sv.msg_text}]")
        # clean up
        @@sv.eqp_unload(@@eqp, p.port, carrier, ib: eqpi.ib) if rc_load == 0
      }
      assert ok
    }
    return ok
  end

  def _run_test_npw(carrier, rc_list_npw, rc_list_load)
    ok = true
    assert_equal 0, @@sv.eqp_cleanup(@@eqp)
    assert @@svtest.cleanup_carrier(carrier)
    assert_equal 0, @@sv.carrier_xfer_status_change(carrier, 'EO', @@eqp)
    #
    eqpi = @@sv.eqp_info(@@eqp)
    eqpi.ports.each_with_index {|p, iport|
      $log.info "\n\n---- port load purpose #{p.purpose.inspect}"
      CassetteLoadPurposes.each_with_index {|purpose, ipurpose|
        $log.info "-- carrier load purpose #{purpose.inspect}"
        # reserve carrier and verify return code
        rc_npw = @@sv.npw_reserve(@@eqp, p.port, carrier, pg: p.pg, purpose: purpose)
        ok &= verify_equal(rc_list_npw[iport][ipurpose], rc_npw, "error for port #{p.port} (##{iport}), purpose: #{purpose.inspect} (##{ipurpose})")
        next unless rc_npw == 0
        # load carrier and verify return code
        rc_load = @@sv.eqp_load(@@eqp, p.port, carrier, ib: eqpi.ib, purpose: purpose)
        ok &= verify_equal(rc_list_load[iport][ipurpose], rc_load, "wrong rc for LoadingLotRpt: port purpose #{p.purpose}, carrier load purpose #{purpose.inspect} [#{@@sv.msg_text}]")
        # clean up
        if rc_load == 0
          @@sv.eqp_unload(@@eqp, p.port, carrier, ib: eqpi.ib)
        else
          @@sv.npw_reserve_cancel(@@eqp, carrier)
        end
      }
      assert ok
    }
    return ok
  end

end
