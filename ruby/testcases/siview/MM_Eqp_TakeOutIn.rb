=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: Daniel Steger, 2014-02-25

History:
  2016-02-05 sfrieske, cleanup, minor fixes
  2017-04-05 sfrieske, minor fixes
  2018-05-09 sfrieske, minor cleanup
  2019-12-09 sfrieske, minor cleanup, renamed from Test160_TakeOutIn
  2020-02-21 sfrieske, adapted for CS_SP_CHECK_EARLYMOVINGFOUP_ON_UNLOAD OFF (C7.14)
=end

require 'SiViewTestCase'


# Take out/in reservation test, MSRs 655359 46557
class MM_Eqp_TakeOutIn < SiViewTestCase
  @@eqp = 'UTF001'
  @@eqp2 = 'UTC003'
  @@opNo = '1000.600'
  @@stocker2 = 'UTSTO12'

  @@testlots = []


  def self.after_all_passed
    [@@eqp, @@eqp2].each {|eqp| @@sv.eqp_cleanup(eqp)}
    @@testlots.each {|lot| @@sv.delete_lot_family(lot)}
  end


  def test00_prepare
    $setup_ok = false
    #
    assert envvars = @@sv.environment_variable[1]
    assert_equal '1', envvars['SP_DELIVERY_TAKE_OUTIN_ENABLE_FLAG'], 'takeout in function is not active'
    assert_equal '1', envvars['SP_REROUTE_XFER_FLAG'], 'no support reroute for xfer_state SO'
    assert_equal 'OFF', envvars['CS_SP_CHECK_EARLYMOVINGFOUP_ON_UNLOAD'], 'wrong SiView env var'
    assert @@sm.update_udata(:eqp, @@eqp, {'EarlyMovingFOUP'=>''}) if @@sv.user_data(:eqp, @@eqp)['EarlyMovingFOUP']
    #
    # note: the flag is only used by dispatching. There is no check when doing TakeOutIn SLR
    assert @@sv.eqp_info_raw(@@eqp).equipmentBRInfo.takeInOutTransferFlag, "takeoutin flag not set for #{@@eqp}"
    assert @@sv.eqp_info_raw(@@eqp2).equipmentBRInfo.eqpToEqpTransferFlag, "e2e not enabled for #{@@eqp2}"
    [@@eqp, @@eqp2].each {|eqp| assert @@svtest.cleanup_eqp(eqp)}
    @@port = @@sv.eqp_info(@@eqp).ports.first.port
    #
    assert @@testlots = @@svtest.new_lots(2), 'error creating test lots'
    #
    $setup_ok = true
  end

  def test01_portstate_unloadreq
    run_portstate_scenario('UnloadReq', success: true)
  end

  def test02_portstate_unloadcomp
    run_portstate_scenario('UnloadComp', success: true)
  end

  def test03_portstate_loadreq
    run_portstate_scenario('LoadReq', success: true)
  end

  def test04_portstate_loadcomp
    run_portstate_scenario('LoadComp', success: false)
  end

  def test05_portstate_down
    run_portstate_scenario('Down', success: false)
  end

  def test06_earlyfoup_unloadreq
    run_portstate_scenario('UnloadReq', success: true, noopecomp: true)
  end

  def test07_cancel_cj
    run_portstate_scenario('UnloadReq', cancel: true)
  end

  def test08_eqp2_equipment_in
    run_portstate_scenario('UnloadReq', success: true, eqp_in: true)
  end

  def test09_reroute
    run_portstate_scenario('UnloadReq', success: true, reroute: true)
  end


  # aux methods

  def run_portstate_scenario(portstate, params)
    # prepare eqp and lots
    @@testlots.each {|lot| assert_equal 0, @@sv.lot_cleanup(lot, opNo: @@opNo, stocker: @@svtest.stocker, xjs: true)}
    assert_equal 0, @@sv.eqp_cleanup(@@eqp, mode: 'Auto-3'), 'error preparing eqp'
    # process lot, keep the carrier loaded
    assert @@sv.claim_process_lot(@@testlots[0], eqp: @@eqp, port: @@port, mode: 'Auto-3', nounload: true, noopecomp: params[:noopecomp]), 'error processing lot'
    # complete processing on eqp
    lport = @@sv.eqp_info(@@eqp).ports.find {|p| p.port == @@port}
    if ['UnloadComp', 'LoadReq'].member?(portstate)
      assert_equal 0, @@sv.eqp_unload(@@eqp, @@port, lport.loaded_carrier), 'failed to unload carrier'
    end
    if lport.state != portstate
      assert_equal 0, @@sv.eqp_port_status_change(@@eqp, @@port, portstate), "failed to switch portstate to #{portstate}"
    end
    # prepare 2nd carrier
    carrier = @@sv.lot_info(@@testlots[1]).carrier
    if params[:eqp_in]
      assert_equal 0, @@sv.eqp_cleanup(@@eqp2, mode: 'Off-Line-1')
      assert_equal 0, @@sv.carrier_xfer_status_change(carrier, 'EO', @@eqp2), 'error stocking in carrier'
      assert_equal 0, @@sv.eqp_load(@@eqp2, 'P1', carrier, purpose: 'Other'), 'failed to load carrier at other tool'
    else
      assert_equal 0, @@sv.carrier_xfer_status_change(carrier, 'SI', @@svtest.stocker), 'failed to prepare carrier'
      if params[:reroute]
        assert_equal 0, @@sv.carrier_xfer(carrier, @@svtest.stocker, '', @@stocker2, '')
        assert wait_for(timeout: 60, sleeptime: 5) {
          @@sv.carrier_status(carrier).xfer_status == 'SO'
        }, 'carrier not on intrabay'
      end
    end
    # SLR TakeOutIn
    cj = @@sv.slr_takeoutin(@@eqp, lot: @@testlots[1], port: @@port)
    if params[:success]
      assert cj, 'takeoutin failed'
      if portstate == 'UnloadComp'
        assert_equal 0, @@sv.eqp_port_status_change(@@eqp, @@port, 'LoadReq'), 'error preparing eqp'
      end
      assert_equal 0, @@sv.carrier_xfer(carrier, '', '', @@eqp, @@port, multicarrier: true, transport: 'L')
      assert_equal 'Dispatched', @@sv.eqp_info(@@eqp).ports.find {|p| p.port == @@port}.disp_state, 'wrong dispatch state'
      refute_empty xj = @@sv.carrier_xferjob(carrier), 'no xfer job'
      assert xj.first.strCarrierJobResult.find {|cj| 
        cj.carrierID.identifier == carrier && cj.toMachine.identifier == @@eqp && cj.toPortID.identifier == @@port
      }, 'wrong xfer job'
      unless ['UnloadComp', 'LoadReq'].member?(portstate)
        assert_equal 0, @@sv.eqp_unload(@@eqp, @@port, lport.loaded_carrier), 'error preparing eqp'
        if params[:noopecomp]
          assert_equal 0, @@sv.eqp_tempdata(@@eqp, lport.pg, lport.cj)
          assert_equal 0, @@sv.eqp_opecomp(@@eqp, lport.cj)
        end
        assert_equal 'Dispatched', @@sv.eqp_info(@@eqp).ports.find {|p| p.port == @@port}.disp_state, 'wrong dispatch state'
      end
    elsif params[:cancel]
      assert cj, 'takeoutin failed'
      assert_equal 0, @@sv.carrier_xfer(lport.loaded_carrier, @@eqp, @@port, @@svtest.stocker, ''), "failed to create unload job"
      assert_equal 0, @@sv.slr_cancel(@@eqp, cj), "failed to cancel take-in job"
      p = @@sv.eqp_info(@@eqp).ports.find {|p| p.port == @@port}
      assert_equal p.loaded_carrier, p.disp_unload_carrier, 'unload carrier expected at DispatchUnloadCarrierID'
      assert_equal 'Dispatched', p.disp_state, 'unload carrier expected at DispatchUnloadCarrierID'
      refute_empty xj = @@sv.carrier_xferjob(lport.loaded_carrier), 'no xfer job'
      assert xj.first.strCarrierJobResult.find {|cj|
        cj.carrierID.identifier == lport.loaded_carrier && cj.toMachine.identifier == @@svtest.stocker && cj.toPortID.identifier.empty?
      }, 'wrong xfer job'
    else
      assert_equal 946, @@sv.rc, 'SLR TakeOutIn should have failed'
      assert_equal 'Required', @@sv.eqp_info(@@eqp).ports.find {|p| p.port == @@port}.disp_state, 'wrong dispatch state'
    end
  end

end
