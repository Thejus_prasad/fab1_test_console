=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: Steffen Steidten, 2011-01-05

History:
  2013-03-07 dsteger,  added tests for MSR256958
  2015-05-20 ssteidte, separated MSR256958 (xfer status EI) into Test125_ReserveLotEI; verified with R10
  2017-04-05 sfrieske, minor fixes
  2019-12-03 sfrieske, minor fixes, renamed from Test124_ReserveCarrierSO
  2020-09-07 sfrieske, minor cleanup
=end

require 'SiViewTestCase'


# For carriers in SO allow SLR (MSR135069) and NPW (MSR 192633); MSR849535
class MM_Eqp_ReserveCarrierSO < SiViewTestCase
  @@eqp = 'UTC001'
  @@opNo = '1000.700'
  @@eqp_ib = 'UTI001'
  @@opNo_ib = '1000.300'
  @@stocker2 = 'UTSTO12'
  @@modes = ['Auto-1', 'Semi-1', 'Auto-2', 'Semi-2', 'Auto-3']


  def self.shutdown
    [@@eqp,  @@eqp_ib].each {|eqp| @@sv.eqp_cleanup(eqp, mode: 'Off-Line-1')}
    @@dummyamhs.set_variable('intrabaytime', 15000)
  end

  def self.after_all_passed
    @@svtest.cleanup_carrier(@@carrier)
    @@sv.delete_lot_family(@@lot)
  end


  def test00_setup
    $setup_ok = false
    #
    assert_equal '1', @@sv.environment_variable[1]['SP_REROUTE_XFER_FLAG'], 'server env setting forbids reroute for xfer_state SO'
    @@dummyamhs = Dummy::AMHS.new($env)
    @@dummyamhs.set_variable('intrabaytime', 45000)
    # cleanup eqps
    [@@eqp,  @@eqp_ib].each {|eqp| assert @@svtest.cleanup_eqp(eqp)}
    # create test lot
    assert @@lot = @@svtest.new_lot
    @@carrier = @@sv.lot_info(@@lot).carrier
    #
    $setup_ok = true
  end

  # for reference: carrier stocked in

  def test11_FB_SI
    $setup_ok = false
    assert_equal 0, @@sv.lot_opelocate(@@lot, @@opNo)
    assert @@svtest.cleanup_carrier(@@carrier)
    assert ['SI', 'MI'].member?(@@sv.carrier_status(@@carrier).xfer_status), "wrong xfer status of carrier #{@@carrier}"
    eqp = @@eqp
    port = @@sv.eqp_info(eqp).ports.first.port
    @@modes.each {|mode|
      $log.info "--- mode #{mode}"
      assert_equal 0, @@sv.eqp_cleanup(eqp, mode: mode)
      # SLR
      assert cj = @@sv.slr(eqp, lot: @@lot)
      assert @@sv.eqp_info(eqp).rsv_carriers.include?(@@carrier)
      assert_equal 0,  @@sv.slr_cancel(eqp, cj)
      # NPW
      assert_equal 0, @@sv.npw_reserve(eqp, nil, @@carrier)
      assert @@sv.eqp_info(eqp).disp_load_carriers.include?(@@carrier)
      assert_equal 0, @@sv.npw_reserve_cancel(eqp, @@carrier)
    }
    $setup_ok = true
  end

  def test12_IB_SI
    $setup_ok = false
    assert_equal 0, @@sv.lot_opelocate(@@lot, @@opNo_ib)
    assert @@svtest.cleanup_carrier(@@carrier)
    assert ['SI', 'MI'].member?(@@sv.carrier_status(@@carrier).xfer_status), "wrong xfer status of carrier #{@@carrier}"
    eqp = @@eqp_ib
    port = @@sv.eqp_info(eqp).ports.first.port
    @@modes.each {|mode|
      $log.info "--- mode #{mode}"
      assert_equal 0, @@sv.eqp_cleanup(eqp, mode: mode)
      # SLR, no NPW
      assert cj = @@sv.slr(eqp, lot: @@lot), 'error in SLR'
      assert @@sv.eqp_info(eqp).rsv_carriers.include?(@@carrier), 'wrong carrier'
      assert_equal 0, @@sv.slr_cancel(eqp, cj)
    }
    $setup_ok = true
  end

  # carrier SO, stocker-to-stocker move

  def test21_FB_SO
    assert_equal 0, @@sv.lot_cleanup(@@lot, opNo: @@opNo, stocker: @@svtest.stocker, xjs: true)
    eqp = @@eqp
    port = @@sv.eqp_info(eqp).ports.first.port
    assert_equal 0, @@sv.eqp_mode_change(eqp, 'Auto-3')
    assert_equal 0, @@sv.eqp_port_status_change(eqp, port, 'LoadReq')
    assert_equal 0, @@sv.carrier_xfer(@@carrier, '', '', @@stocker2, '')
    assert wait_for(timeout: 40, sleeptime: 3) {@@sv.carrier_status(@@carrier).xfer_status == 'SO'}, "no carrier xfer"
    @@modes.each {|mode|
      $log.info "--- mode #{mode}"
      assert_equal 0, @@sv.eqp_cleanup(eqp, mode: mode)
      # SLR
      assert cj = @@sv.slr(eqp, lot: @@lot)
      assert @@sv.eqp_info(eqp).rsv_carriers.include?(@@carrier)
      assert_equal 0,  @@sv.slr_cancel(eqp, cj)
      # NPW
      assert_equal 0, @@sv.npw_reserve(eqp, nil, @@carrier)
      assert @@sv.eqp_info(eqp).disp_load_carriers.include?(@@carrier)
      assert_equal 0, @@sv.npw_reserve_cancel(eqp, @@carrier)
    }
  end

  def test22_IB_SO
    assert_equal 0, @@sv.lot_cleanup(@@lot, opNo: @@opNo_ib, stocker: @@svtest.stocker, xjs: true)
    eqp = @@eqp_ib
    port = @@sv.eqp_info(eqp).ports.first.port
    assert_equal 0, @@sv.eqp_mode_change(eqp, 'Auto-3')
    assert_equal 0, @@sv.eqp_port_status_change(eqp, port, 'LoadReq')
    assert_equal 0, @@sv.carrier_xfer(@@carrier, '', '', @@stocker2, '')
    assert wait_for(timeout: 40, sleeptime: 3) {@@sv.carrier_status(@@carrier).xfer_status == 'SO'}, "no carrier xfer"
    @@modes.each {|mode|
      $log.info "--- mode #{mode}"
      assert_equal 0, @@sv.eqp_cleanup(eqp, mode: mode)
      # SLR, no NPW
      assert cj = @@sv.slr(eqp, lot: @@lot)
      assert @@sv.eqp_info(eqp).rsv_carriers.include?(@@carrier)
      assert_equal 0,  @@sv.slr_cancel(eqp, cj)
    }
  end

end
