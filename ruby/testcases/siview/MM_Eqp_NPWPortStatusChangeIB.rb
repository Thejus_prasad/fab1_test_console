=begin 
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.
    
Author: Paul Peschel, 2013-01-10 migrate from Test051_NPW_PortStatusChangeInternalBuffer.java

History:
  2013-07-17 ssteidte, (re-)start dummy EI
  2019-12-09 sfrieske, minor cleanup, renamed from Test051_NPWPortStatusChangeInternalBuffer
=end

require 'SiViewTestCase'
require 'dummytools'


# Test NPW reservation and changes the port states for IB Tools
class MM_Eqp_NPWPortStatusChangeIB < SiViewTestCase
  @@eqp = 'UTI002'
  @@stocker = 'UTSTO11'


  def self.after_all_passed
    @@sv.delete_lot_family(@@lot)
  end

  
  def test00_setup
    $setup_ok = false
    #
    assert @@svtest.cleanup_eqp(@@eqp)
    @@port = @@sv.eqp_info(@@eqp).ports.first.port
    #
    assert @@lot = @@svtest.new_controllot('Dummy', bankin: true)
    @@carrier = @@sv.lot_info(@@lot).carrier
    assert_equal 0, @@sv.carrier_xfer_status_change(@@carrier, 'MO',  @@stocker)
    assert_equal 0, @@sv.carrier_xfer_status_change(@@carrier, 'MI', @@stocker)
    $log.info "using dummy lot #{@@lot.inspect}, carrier #{@@carrier.inspect}"
    #
    $setup_ok = true
  end
  
  def test10
    # set eqp mode Auto-2, port status LoadReq
    assert_equal 0, @@sv.eqp_mode_change(@@eqp, 'Auto-2')
    assert_equal 0, @@sv.eqp_port_status_change(@@eqp, @@port, 'LoadReq')
    # do NPW reserve
    assert_equal 0, @@sv.npw_reserve(@@eqp, @@port, @@carrier, purpose: 'Side Dummy Lot')
    assert_equal [@@carrier], @@sv.eqp_info(@@eqp).ib.rsv_carriers
    # change port status to LoadComp
    assert_equal 0, @@sv.eqp_port_status_change(@@eqp, @@port, 'LoadComp')
    assert_equal [@@carrier], @@sv.eqp_info(@@eqp).ib.rsv_carriers
    # load carrier and change port status to UnloadReq, reservation is deleted
    assert_equal 0, @@sv.carrier_xfer_status_change(@@carrier, 'EO',  @@eqp)
    assert_equal 0, @@sv.eqp_load(@@eqp, @@port, @@carrier, purpose: 'Side Dummy Lot')
    assert_equal 0, @@sv.eqp_port_status_change(@@eqp, @@port, 'UnloadReq')
    assert_equal [], @@sv.eqp_info(@@eqp).ib.rsv_carriers
    # unload carrier and change port status to UnloadComp, reservation is still deleted
    assert_equal 0, @@sv.eqp_unload(@@eqp, @@port, @@carrier)
    assert_equal 0, @@sv.eqp_port_status_change(@@eqp, @@port, 'UnloadComp')
    assert_equal [], @@sv.eqp_info(@@eqp).ib.rsv_carriers
    # change port status to LoadReq, still no reservation
    assert_equal 0, @@sv.eqp_port_status_change(@@eqp, @@port, 'LoadReq')
    assert_equal [], @@sv.eqp_info(@@eqp).ib.rsv_carriers
    # clean up
    assert_equal 0, @@sv.eqp_cleanup(@@eqp)
    assert_equal 0, @@sv.eqp_mode_change(@@eqp, 'Off-Line-1')
  end
  
end
