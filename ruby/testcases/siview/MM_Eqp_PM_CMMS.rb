=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: Daniel Steger, 2013-03-25

History:
  2014-03-28 dsteger,  dual CMMS support via userid (MSR841753)
  2017-04-18 sfrieske, added test81
  2018-05-07 sfrieske, minor cleanup
  2019-11-25 sfrieske, fixed wrong asserts, changed default eqp
  2019-12-09 sfrieske, minor cleanup, renamed from Test211_PM_Requests
  2021-08-30 sfrieske, moved siviewmmdb.rb to siview/mmdb.rb
=end

require 'SiViewTestCase'


# Test that the CMMS support transactions are accepted by SiView (incl. MSR704108, MSR747147, MSR483163, MSR519974)
# Need to run for valid @@cmms_udata Xsite or SAPPM
# Need to run for Internal Buffer and Fixed Buffer eqps
class MM_Eqp_PM_CMMS < SiViewTestCase
  UserMap = {'Xsite' => 'X-XSITE', 'SAPPM' => 'X-SAP', 'Xsite7' => 'Xsite'}
  @@eqp = 'UTC002'
  @@e10_notify = 'ON'
  # need to test with all these entries, see MSR747147 and MSR483163
  @@clientnode = 'CMMS'  #'FC8XA5DESKQ01-dsteger'#''#'CMMS'
  @@dual_cmms = 'OFF'    # Fab1: OFF; nil/OFF - is single CMMS or 'ON'
  @@cmms_udata = 'SAPPM' # or Xsite
  # Fab7 uses chamber recipes
  @@chamber_recipes = {}
  @@state_down = '2NDP'
  @@state_sby = '2WPR'
  @@state_prd = '1PRD'
  @@opNo = nil

  @@testlots = []


  def self.after_all_passed
    @@sv.eqp_cleanup(@@eqp)
    @@svtest.eis.set_cj_chambers(@@eqp)
    @@testlots.each {|lot| @@sv.delete_lot_family(lot)}
  end


  def test00_setup
    $setup_ok = false
    #
    assert_equal @@e10_notify, @@sv.environment_variable[1]['CS_SP_EI_E10_NOTIFY'], 'incorrect server setting'
    assert_equal @@dual_cmms, (@@sv.environment_variable[1]['CS_SP_DUALCMMS_ENABLE'] || ''), 'incorrect value for DUALCMMS'
    assert @@user = UserMap[@@cmms_udata], 'invalid UDATA setup'
    if (@@dual_cmms != '') &&  (@@dual_cmms != 'OFF')
      # update udata according to setup
      assert @@sm.update_udata(:eqp, @@eqp, {'CMMS'=>@@cmms_udata}), 'error writing UDATA in SM'
      u = @@sv.user_data(:eqp, @@eqp)['CMMS'] || ''   # not reported when empty string
      assert_equal @@cmms_udata, u, 'wrong SM UDATA CMMS'
    end
    # prepare eqp, select chamber
    assert @@svtest.cleanup_eqp(@@eqp)
    @@chambers = @@sv.eqp_info(@@eqp).chambers.collect {|c| c.chamber}.take(2)
    assert @@chambers.size > 1, 'pick a tool with at least 2 chambers'
    @@chamber = @@chambers.first
    # create test lots, select opNo
    assert lots = @@svtest.new_lots(2), 'error creating test lots'
    @@testlots = lots
    @@sv.lot_eqplist_in_route(lots.first).each_pair {|k, v| (@@opNo = k; break) if v.include?(@@eqp)}
    assert @@opNo, "no operation for eqp #{@@eqp}"
    #
    $setup_ok = true
  end

  def test01_pm_stb_waitprod
    assert_equal 0, @@sv.cleanup_e10state(@@eqp), "failed to cleanup E10 state"
    assert_equal 0, @@sv.eqp_pm_event(@@eqp, state: @@state_down, client_node: @@clientnode, user: @@user), 'cannot set E10 state'
    # assert verify_e10_state(@@eqp, @@state_down)
    assert_equal @@state_down, @@sv.eqp_info(@@eqp).status.split('.').last, 'wrong SiView state'
    sleep 1 # EI sync
    assert_equal @@state_down, @@svtest.eis.e10state(@@eqp), '  wrong state reported to EI' if @@e10_notify == 'ON'
  end

  def test02_pm_stb_waitprod_slr
    _prepare
    assert_equal 0, @@sv.eqp_mode_change(@@eqp, 'Auto-1')
    assert cj = @@sv.slr(@@eqp, lot: @@testlots.first)
    assert_equal 0, @@sv.eqp_pm_event(@@eqp, state: @@state_down, client_node: @@clientnode, user: @@user), 'cannot set E10 state'
    assert verify_pm_request(@@eqp, @@state_sby), 'invalid PM data'
    assert_equal 0, @@sv.slr_cancel(@@eqp, cj)
    assert verify_pm_request(@@eqp, @@state_down, 0), 'invalid PM data'
  end

  def test03_pm_processing
    ['cancel', 'comp', 'forcecomp'].each {|func|
      _prepare
      assert cj = @@sv.claim_process_lot(@@testlots.first, eqp: @@eqp, noopecomp: true, nounload: true, mode: 'Auto-1', running_hold: func == 'forcecomp')
      assert_equal 0, @@sv.eqp_pm_event(@@eqp, state: @@state_down, client_node: @@clientnode, user: @@user), 'cannot set E10 state'
      assert verify_pm_request(@@eqp, @@state_sby), 'invalid PM data'
      _proc_command(func, cj)
      assert verify_pm_request(@@eqp, @@state_down, 0), 'invalid PM data'
    }
  end

  def test04_pm_no_dispatch
    assert_equal 0, @@sv.eqp_status_change(@@eqp, @@state_down), 'E10 state change failed'
    # assert verify_e10_state(@@eqp, @@state_down), 'E10 state change failed'
    sleep 1 # EI sync
    assert_equal @@state_down, @@svtest.eis.e10state(@@eqp), '  wrong state reported to EI' if @@e10_notify == 'ON'
    assert_equal 0, @@sv.eqp_pm_event(@@eqp, state: @@state_down, client_node: @@clientnode, user: @@user), 'cannot set E10 state'
    # assert verify_e10_state(@@eqp, @@state_down)
    assert_equal @@state_down, @@sv.eqp_info(@@eqp).status.split('.').last, 'wrong SiView state'
    sleep 1 # EI sync
    assert_equal @@state_down, @@svtest.eis.e10state(@@eqp), '  wrong state reported to EI' if @@e10_notify == 'ON'
end

  def test05_pm_processing_mutltiple_jobs
    assert_equal 0, @@sv.eqp_cleanup(@@eqp, mode: 'Auto-1'), 'error preparing eqp'
    ['cancel', 'comp', 'forcecomp'].each {|func|
      _prepare
      assert cjs = @@testlots.map {|lot|
        @@sv.claim_process_lot(lot, eqp: @@eqp, mode: 'Auto-1', noopecomp: true, nounload: true, running_hold: func == 'forcecomp')
      }
      assert_equal 0, @@sv.eqp_pm_event(@@eqp, state: @@state_down, client_node: @@clientnode, user: @@user), 'cannot set E10 state'
      cjs.each_with_index {|cj, i|
        assert verify_pm_request(@@eqp, @@state_prd), 'invalid PM data'
        # E10 state will not automatically switch to 2WPR
        if i == cjs.count - 1
          assert_equal 0, @@sv.eqp_status_change_rpt(@@eqp, @@state_sby), 'E10 state change failed'
          # assert verify_e10_state(@@eqp, @@state_sby), 'E10 state change failed'
          sleep 1 # EI sync
          assert_equal @@state_sby, @@svtest.eis.e10state(@@eqp), '  wrong state reported to EI' if @@e10_notify == 'ON'
        end
        _proc_command(func, cj)
      }
      assert verify_pm_request(@@eqp, @@state_down, 0), 'invalid PM data'
    }
  end

  def test06_pm_cancel_processing
    assert_equal 0, @@sv.eqp_cleanup(@@eqp, mode: 'Auto-1'), 'error preparing eqp'
    ['cancel', 'comp', 'forcecomp'].each {|func|
      _prepare
      assert cj = @@sv.claim_process_lot(@@testlots.first, eqp: @@eqp, mode: 'Auto-1', noopecomp: true, nounload: true, running_hold: (func=="forcecomp"))
      assert_equal 0, @@sv.eqp_pm_event(@@eqp, state: @@state_down, client_node: @@clientnode, user: @@user), 'cannot set E10 state'
      assert verify_pm_request(@@eqp, @@state_sby), 'invalid PM data'
      assert_equal 0, @@sv.eqp_pm_event_cancel(@@eqp, client_node: @@clientnode, user: @@user), "failed to cancel PM request"
      assert verify_pm_request(@@eqp, @@state_sby, 0), 'invalid PM data'
      _proc_command(func, cj)
      assert verify_pm_request(@@eqp, @@state_sby, 0), 'invalid PM data'
    }
  end

  ### Chamber states ###

  def test10_pm_chamber_stb_waitprod
    e10_state_waitprod(true)
    assert_equal 0, @@sv.chamber_pm_event(@@eqp, @@chamber, state: @@state_down, client_node: @@clientnode, user: @@user), "Can't set PM for chamber"
    sleep 2 # EI sync
    assert_equal @@state_down, @@svtest.eis.e10state(@@eqp, @@chamber), 'wrong EI state' if @@e10_notify == 'ON'
  end

  def test11_pm_chamber_stb_waitprod_slr
    _prepare(true)
    assert_equal 0, @@sv.eqp_mode_change(@@eqp, 'Auto-1')
    assert cj = @@sv.slr(@@eqp, lot: @@testlots.first)
    assert @@svtest.eis.set_cj_chambers(@@eqp, cj, [])
    assert_equal 0, @@sv.chamber_pm_event(@@eqp, @@chamber, state: @@state_down, client_node: @@clientnode, user: @@user), "Can't set PM for chamber"
    if @@sv.f7
      assert verify_pm_request_chamber(@@eqp, [@@chamber], @@state_sby), 'invalid PM data'
    else
      assert verify_pm_request_chamber(@@eqp, [@@chamber], @@state_down, 0), 'invalid PM data'
    end
    assert_equal 0, @@sv.slr_cancel(@@eqp, cj)
    assert verify_pm_request_chamber(@@eqp, [@@chamber], @@state_down, 0), 'invalid PM data'
  end

  def test12_pm_chamber_processing
    ['cancel', 'comp', 'forcecomp'].each {|func|
      _prepare(true)
      assert cj = @@sv.claim_process_lot(@@testlots.first, eqp: @@eqp, noopecomp: true, nounload: true, mode: "Auto-1", running_hold: (func=="forcecomp"))
      # Register chamber for control job
      assert @@svtest.eis.set_cj_chambers(@@eqp, cj, [@@chamber]), 'cannot set chamber'
      assert_equal 0, @@sv.chamber_pm_event(@@eqp, @@chamber, state: @@state_down, client_node: @@clientnode, user: @@user), "can't set PM for chamber"
      assert verify_pm_request_chamber(@@eqp, [@@chamber], @@state_sby), 'invalid PM data'
      _proc_command(func, cj)
      assert verify_pm_request_chamber(@@eqp, [@@chamber], @@state_down, 0), 'invalid PM data'
    }
  end

  def test13_pm_chamber_no_dispatch
    assert_equal 0, @@sv.chamber_status_change(@@eqp, @@chamber, @@state_down, client_node: @@clientnode), 'error changing chamber state'
    sleep 2 # EI sync
    assert_equal @@state_down, @@svtest.eis.e10state(@@eqp, @@chamber), 'wrong EI state' if @@e10_notify == 'ON'
    assert_equal 0, @@sv.chamber_pm_event(@@eqp, @@chamber, state: @@state_down, client_node: @@clientnode, user: @@user), "can't set PM for chamber"
    sleep 2 # EI sync
    assert_equal @@state_down, @@svtest.eis.e10state(@@eqp, @@chamber), 'wrong EI state' if @@e10_notify == 'ON'
end

  def test14_pm_chamber_processing_mutltiple_jobs_same_chamber
    ['cancel', 'comp', 'forcecomp'].each {|func|
      _prepare(true)
      assert cjs = @@testlots.map {|lot|
        cj = @@sv.claim_process_lot(lot, eqp: @@eqp, mode: 'Auto-1', running_hold: func == 'forcecomp', noopecomp: true, nounload: true)
        assert @@svtest.eis.set_cj_chambers(@@eqp, cj, [@@chamber]), 'cannot set chamber'
        cj
      }
      assert_equal 0, @@sv.chamber_pm_event(@@eqp, @@chamber, state: @@state_down, client_node: @@clientnode, user: @@user), "can't set PM for chamber"
      cjs.each_with_index {|cj, i|
        assert verify_pm_request_chamber(@@eqp, [@@chamber], @@state_sby), 'invalid PM data'
        # E10 state will not automatically switch to 2WPR
        if i == cjs.count - 1
          assert_equal 0, @@sv.eqp_status_change_rpt(@@eqp, @@state_sby), 'E10 state change failed'
          # assert verify_e10_state(@@eqp, @@state_sby), 'E10 state change failed'
          sleep 1 # EI sync
          assert_equal @@state_sby, @@svtest.eis.e10state(@@eqp), '  wrong state reported to EI' if @@e10_notify == 'ON'
        end
        _proc_command(func, cj)
      }
      assert verify_pm_request_chamber(@@eqp, [@@chamber], @@state_down, 0), 'invalid PM data'
    }
  end

  def test15_pm_chamber_processing_multiple_jobs_different_chambers
    ['cancel', 'comp', 'forcecomp'].each {|func|
      _prepare(true)
      assert cjs = @@testlots.each_with_index.map {|lot,i|
        params = @@sv.f7 ? {slr: true, mrecipe: @@chamber_recipes[@@chambers[i]]} : {}
        cj = @@sv.claim_process_lot(lot, params.merge(eqp: @@eqp, mode: 'Auto-1', running_hold:  func == 'forcecomp', noopecomp: true, nounload: true))
        assert @@svtest.eis.set_cj_chambers(@@eqp, cj, [@@chambers[i]]), 'cannot set chamber'
        cj
      }
      @@chambers.each {|chamber|
        assert_equal 0, @@sv.chamber_pm_event(@@eqp, chamber, state: @@state_down, client_node: @@clientnode, user: @@user), "can't set PM for chamber"
      }
      cjs.each_with_index {|cj, i|
        assert verify_pm_request_chamber(@@eqp, @@chambers[0, i], @@state_down, 0), "PM inhibit should be cleared"
        assert verify_pm_request_chamber(@@eqp, @@chambers[i .. -1], @@state_sby), "PM inhibit should exist"
        assert @@svtest.eis.set_cj_chambers(@@eqp, cj, []), 'cannot set chamber'
        # E10 state will not automatically switch to 2WPR
        if i == cjs.count - 1
          assert_equal 0, @@sv.eqp_status_change_rpt(@@eqp, @@state_sby), 'E10 state change failed'
          # assert verify_e10_state(@@eqp, @@state_sby), 'E10 state change failed'
          sleep 1 # EI sync
          assert_equal @@state_sby, @@svtest.eis.e10state(@@eqp), '  wrong state reported to EI' if @@e10_notify == 'ON'
        end
        _proc_command(func, cj)
      }
      assert verify_pm_request_chamber(@@eqp, @@chambers, @@state_down, 0), 'invalid PM data'
    }
  end

  def test16_pm_chamber_cancel_processing
    ['cancel', 'comp', 'forcecomp'].each {|func|
      _prepare(true)
      assert cj = @@sv.claim_process_lot(@@testlots.first, eqp: @@eqp, noopecomp: true, nounload: true, mode: 'Auto-1', running_hold: func == 'forcecomp')
      # register chamber for control job
      assert @@svtest.eis.set_cj_chambers(@@eqp, cj, [@@chamber]), 'cannot set chamber'
      assert_equal 0, @@sv.chamber_pm_event(@@eqp, @@chamber, state: @@state_down, client_node: @@clientnode, user: @@user), "can't set PM for chamber"
      assert verify_pm_request_chamber(@@eqp, [@@chamber], @@state_sby), 'invalid PM data'
      assert_equal 0, @@sv.eqp_pm_event_cancel(@@eqp, chamber: @@chamber, client_node: @@clientnode, user: @@user), "failed to chancel PM request"
      assert verify_pm_request_chamber(@@eqp, [@@chamber], @@state_sby, 0), 'invalid PM data'
      _proc_command(func, cj)
      assert verify_pm_request_chamber(@@eqp, [@@chamber], @@state_sby, 0), 'invalid PM data'
    }
  end

  # MSR519974
  def test17_pm_chamber_cancel_processing_multiple_jobs_different_chambers
    ['cancel', 'comp', 'forcecomp'].each {|func|
      _prepare(true)
      assert cjs = @@testlots.each_with_index.map {|lot, i|
        params = @@sv.f7 ? {slr: true, mrecipe: @@chamber_recipes[@@chambers[i]]} : {}
        cj = @@sv.claim_process_lot(lot, params.merge(eqp: @@eqp, mode: 'Auto-1', running_hold:  func == 'forcecomp', noopecomp: true, nounload: true))
        assert @@svtest.eis.set_cj_chambers(@@eqp, cj, [@@chambers[i]]), 'cannot set chamber'
        cj
      }
      @@chambers.each do |chamber|
        assert_equal 0, @@sv.chamber_pm_event(@@eqp, chamber, state: @@state_down, client_node: @@clientnode, user: @@user), "can't set PM for chamber"
      end
      # cancel only the first PM request
      @@sv.eqp_pm_event_cancel(@@eqp, chamber: @@chambers[0], client_node: @@clientnode, user: @@user)
      cjs.each_with_index do |cj, i|
        idx = [i-1,0].max # correct index since first chamber has no PM request
        assert verify_pm_request_chamber(@@eqp, @@chambers[0, 1], @@state_sby, 0), "No PM inhibit at all"
        assert verify_pm_request_chamber(@@eqp, @@chambers[1, idx], @@state_down, 0), "PM inhibit should be cleared"
        assert verify_pm_request_chamber(@@eqp, @@chambers[idx + 1 .. -1], @@state_sby), "PM inhibit should exist"
        assert @@svtest.eis.set_cj_chambers(@@eqp, cj, []), 'cannot set chamber'
        # E10 state will not automatically switch to 2WPR
        if i == cjs.count - 1
          assert_equal 0, @@sv.eqp_status_change_rpt(@@eqp, @@state_sby), 'E10 state change failed'
          # assert verify_e10_state(@@eqp, @@state_sby), 'E10 state change failed'
          sleep 1 # EI sync
          assert_equal @@state_sby, @@svtest.eis.e10state(@@eqp), '  wrong state reported to EI' if @@e10_notify == 'ON'
        end
        _proc_command(func, cj)
      end
      assert verify_pm_request_chamber(@@eqp, @@chambers[0, 1], @@state_sby, 0), "no PM request, chamber should be waiting"
      assert verify_pm_request_chamber(@@eqp, @@chambers[1 .. -1], @@state_down, 0), "chambers should be in no dispatch"
    }
  end

  # long existing issue, detected in C7.5 (only mainframe, works for chambers)
  def testman81_missing_claim_user
    require 'siview/mmdb.rb'
    assert @@mmdb = SiView::MMDB.new($env)
    FREQP = Struct.new(:eqp, :status, :user, :statechg_user)
    def freqp(eqp)
      qargs = []
      q = 'select EQP_ID, CUR_STATE_ID, CLAIM_USER_ID, STATE_CHG_USER_ID from FREQP'
      q, qargs = @db._q_restriction(q, qargs, 'EQP_ID', eqp)
      res = @@mmdb.db.select(q, *qargs) || return
      return res.collect {|r| FREQP.new(*r)}
    end
    assert_equal 0, @@sv.eqp_cleanup(@@eqp, mode: 'Auto-1')
    assert_equal 0, @@sv.lot_cleanup( @@testlots.first, opNo: @@opNo)
    # start processing lot and send PM event
    assert cj = @@sv.claim_process_lot( @@testlots.first, eqp: @@eqp, mode: 'Auto-1', noopecomp: true, nounload: true)
    assert_equal 0, @@sv.eqp_pm_event(@@eqp, state: @@state_down)
    # eqp status is 2WPR, inhibit exists, claim user is set in MMDB
    assert_equal 'SBY.2WPR', @@sv.eqp_info(@@eqp).status, "wrong eqp status"
    assert_equal 1, @@sv.inhibit_list(:eqp, @@eqp, reason: 'RQPM').size, "wrong or missing inhibit"
    assert e = freqp(@@eqp).first
    assert_equal @@sv.user, e.user, "missing claim user in MMDB entry for #{@@eqp}"
    assert_equal @@sv.user, e.statechg_user, "missing state chg user in MMDB entry for #{@@eqp}"
    # opecomp
    assert pg = @@sv.eqp_info(@@eqp).ports.find {|p| p.cj == cj}.pg
    assert_equal 0, @@sv.eqp_tempdata(@@eqp, pg, cj)
    assert_equal 0, @@sv.eqp_opecomp(@@eqp, cj)
    # eqp status is 2NDP, inhibit is gone, claim user is set in MMDB
    assert_equal 'ENG.2NDP', @@sv.eqp_info(@@eqp).status, "wrong eqp status"
    assert_empty @@sv.inhibit_list(:eqp, @@eqp, reason: 'RQPM'), "wrong inhibit"
    assert e = freqp(@@eqp).first
    assert_equal @@sv.user, e.user, "missing claim user in MMDB entry for #{@@eqp}"
    assert_equal @@sv.user, e.statechg_user, "missing state chg user in MMDB entry for #{@@eqp}"
  end


  # aux methods

  def _prepare(chamber=false)
    assert_equal 0, @@sv.eqp_cleanup(@@eqp)
    @@svtest.eis.set_cj_chambers(@@eqp)
    @@testlots.each {|lot| assert_equal 0, @@sv.lot_cleanup(lot, opNo: @@opNo)}
    e10_state_waitprod(chamber)
  end

  def _proc_command(func, cj)
    case func
    when 'cancel'
      assert_equal 0, @@sv.eqp_opestart_cancel(@@eqp, cj), 'cancel job failed'
    when 'comp'
      e_info = @@sv.eqp_info(@@eqp)
      unless e_info.ib
        pg = e_info.ports.find {|p| p.cj == cj}.pg
        @@sv.eqp_tempdata(@@eqp, pg, cj)
      end
      assert_equal 0, @@sv.eqp_opecomp(@@eqp, cj), 'completion of job failed'
    when 'forcecomp'
      e_info = @@sv.eqp_info(@@eqp)
      unless e_info.ib
        pg = e_info.ports.find {|p| p.cj == cj}.pg
        @@sv.eqp_tempdata(@@eqp, pg, cj)
      end
      assert_equal 0, @@sv.eqp_opecomp(@@eqp, cj, force: true), 'completion of forced job failed'
    end
  end

  def e10_state_waitprod(chamber=false)
    assert_equal 0, @@sv.eqp_status_change(@@eqp, @@state_down)
    ['4300', '4340'].each {|state| assert_equal 0, @@sv.eqp_status_change(@@eqp, state)} if @@sv.f7
    assert_equal 0, @@sv.eqp_status_change(@@eqp, @@state_sby), 'E10 state change failed'
    # assert verify_e10_state(@@eqp, @@state_sby), 'E10 state change failed'
    sleep 1 # EI sync
    assert_equal @@state_sby, @@svtest.eis.e10state(@@eqp), '  wrong state reported to EI' if @@e10_notify == 'ON'
    if chamber
      @@chambers.each {|chamber|
        assert_equal 0, @@sv.chamber_status_change(@@eqp, chamber, @@state_down, client_node: @@clientnode)
        ['4300', '4340'].each {|state| assert_equal 0, @@sv.chamber_status_change(@@eqp, chamber, state)} if @@sv.f7
        assert_equal 0, @@sv.chamber_status_change(@@eqp, chamber, @@state_sby, client_node: @@clientnode), 'E10 state change failed'
        sleep 2 # EI sync
        assert_equal @@state_sby, @@svtest.eis.e10state(@@eqp, chamber), 'wrong EI state' if @@e10_notify == 'ON'
      }
    end
  end

  def verify_pm_request(eqp, state, count=1)
    # res = verify_e10_state(eqp, state)
    assert_equal state, @@sv.eqp_info(eqp).status.split('.').last, 'wrong SiView state'
    sleep 2 # EI sync
    assert_equal state, @@svtest.eis.e10state(eqp), 'wrong EI state' if @@e10_notify == 'ON'
    assert_equal count, @@sv.inhibit_list(:eqp, @@eqp, reason: 'RQPM').size, 'wrong PM inhibit'
    return true
  end

  def verify_pm_request_chamber(eqp, chambers, state, count=1)
    $log.info "verify_pm_request_chamber #{eqp.inspect}, #{chambers}, #{state.inspect}, #{count}"
    sleep 2 # EI sync
    chinfos = @@sv.eqp_info(eqp).chambers
    res = true
    chambers.each {|chamber|
      assert chinfo = chinfos.find {|chi| chi.chamber == chamber}, "chamber not found: #{chamber.inspect}"
      ($log.warn "  wrong SiView state for #{eqp}:#{ch}"; res = false) if state != chinfo.status.split('.').last
      ($log.warn '  wrong EI state'; res = false) if @@e10_notify == 'ON' && state != @@svtest.eis.e10state(eqp, chamber)
      inhs = @@sv.inhibit_list(:eqp_chamber, @@eqp, reason: 'RQPM').select {|inh| inh.entities.first.attrib == chamber}
      ($log.warn '  wrong number of inhibits'; res = false) if inhs.size != count
    }
    return res
  end

end
