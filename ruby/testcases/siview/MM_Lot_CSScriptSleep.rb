=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: Daniel Steger, 2013-07-08

History:
  2015-06-23 ssteidte, minor cleanup, verified with R15
  2015-11-24 sfrieske, create source lots
  2016-12-13 sfrieske, minor cleanup, replaced assert_nothing_raised with begin/rescue
  2018-03-06 sfrieske, replaced user_parameter_set_verify by user_parameter_change
  2019-05-03 sfrieske, minor cleanup
  2019-12-09 sfrieske, minor cleanup, renamed from Test451_CSScript_Sleep
  2020-04-23 sfrieske, fixed parameters for new_srclot_for_product and stb
  2021-01-15 sfrieske, moved SiView::MM#new_srclot_for_product to SiView::Test#new_srclot
  2021-08-27 sfrieske, removed verify_equal
=end

require 'timeout'
require 'SiViewTestCase'


# Test for CSS scripting deadlocks in conjunction with external script parameter calls. Requires SM setup.
# MSR261803, MSR635745, MSR687680
class MM_Lot_CSScriptSleep < SiViewTestCase
  @@opNo = '1000.2000'
  @@reason = 'ENG'
  @@product = 'UT-PROD-SCRIPT.02'
  @@route = 'UTRT-SCRIPT.02'
  @@product2 = 'UT-PRODUCT000.01'
  @@route2 = 'UTRT001.01'
  @@product_eqpmonitor= 'UT-MN-SCRIPT.01'
  @@script_parms = %w(CustomerPriority FOD_INIT ShipToEngineer ERPItem CustomerLotGrade)
  @@skip_chain = '2000.1000'
  @@timeout = 90 # less than configured in the Pre1-script

  @@testlots = []


  def self.shutdown
    @@testlots.each {|lot| @@sv.delete_lot_family(lot)}
  end


  def test00_setup
    $setup_ok = false
    #
    assert_equal '1', @@sv.environment_variable[1]['SP_CREATE_PROPERTYSET_FOR_NEW_LOT'], 'wrong server env'
    assert_equal '1', @@sv.environment_variable[1]['SP_PROPERTYSET_CREATE_ENABLED'], 'wrong server env (HAS)'
    #
    $setup_ok = true
  end

  def test01_stb_prod
    $setup_ok = false
    #
    assert srclot = @@svtest.new_srclot(product: @@product)
    @@testlots << srclot
    assert lot = @@sv.new_lot_release(product: @@product, route: @@route, srclot: srclot)
    @@testlots << lot
    t = change_uparams(lot)
    assert @@sv.stb(lot), 'STB failed'
    t.join
    #
    $setup_ok = true
  end

  def test02_stb_prod_update
    assert srclot = @@svtest.new_srclot(product: @@product)
    @@testlots << srclot
    assert lot = @@sv.new_lot_release(product: @@product, route: @@route, srclot: srclot)
    @@testlots << lot
    t = change_uparams(lot, 1, 'other')
    assert @@sv.stb(lot), 'STB failed'
    t.join
    assert_equal 'other', @@sv.user_parameter('Lot', lot, name: 'UTstring').value
  end

  def test03_split
    assert srclot = @@svtest.new_srclot(product: @@product)
    @@testlots << srclot
    assert lot = @@sv.new_lot_release(product: @@product, route: @@route, srclot: srclot)
    @@testlots << lot
    assert @@sv.stb(lot)
    childlot = lot.next
    t = change_uparams(childlot)
    assert_equal childlot, @@sv.lot_split(lot, 5), 'error splitting lot'
    t.join
  end

  def test04_split_inheritance
    assert srclot = @@svtest.new_srclot(product: @@product)
    @@testlots << srclot
    assert lot = @@sv.new_lot_release(product: @@product, route: @@route, srclot: srclot)
    @@testlots << lot
    assert @@sv.stb(lot)
    childlot = lot.next
    assert_equal 0, @@sv.user_parameter_change('Lot', lot, 'UTstring', 'inherit')
    t = change_uparams(childlot, 1)
    assert_equal childlot, @@sv.lot_split(lot, 5), "failed to create child lot"
    t.join
    assert_equal 'inherit', @@sv.user_parameter('Lot', childlot, name: 'UTstring').value
  end

  def test05_ctrllot_stb
    assert srclot = @@svtest.new_srclot(product: @@product_eqpmonitor, lottype: 'Equipment Monitor')
    @@testlots << srclot
    t = change_uparams(srclot, 0, nil, @@sv.lot_info(srclot).carrier)
    assert lot = @@sv.stb_controllot('Equipment Monitor', @@product_eqpmonitor, srclot)
    @@testlots << lot
    t.join
  end

  def test06_ctrllot_stb_update
    assert srclot = @@svtest.new_srclot(product: @@product_eqpmonitor, lottype: 'Equipment Monitor')
    @@testlots << srclot
    t = change_uparams(srclot, 1, 'other', @@sv.lot_info(srclot).carrier)
    assert lot = @@sv.stb_controllot('Equipment Monitor', @@product_eqpmonitor, srclot)
    @@testlots << lot
    t.join
    assert_equal 'other', @@sv.user_parameter('Lot', lot, name: 'UTstring').value
  end

  def test10_parallel_script_param_update
    assert srclot = @@svtest.new_srclot(product: @@product2)
    @@testlots << srclot
    assert lot = @@sv.new_lot_release(product: @@product2, route: @@route2, srclot: srclot)
    @@testlots << lot
    assert @@sv.stb(lot)
    threads = @@script_parms.count.times.collect {|t|
      Thread.new {
        value = "Test_#{t}"
        @@sv.user_parameter_change('Lot', lot, @@script_parms[t], value)
        [@@script_parms[t], value]
      }
    }
    exp_script = threads.collect {|t| t.value}
    act_script = @@sv.user_parameter('Lot', lot)
    assert exp_script.inject(true) {|res, (name,value)|
      p = act_script.find {|_p| _p.name == name}
      ($log.warn "#{name} not found"; res=false; next) unless p
      # res &= verify_equal(value, p.value, "scriptparameter not correct")
      ($log.warn "wrong value, expected: #{value}, got: #{p.value}"; res=false) if p.value != value
      res
    }, "script parameter verification failed"
  end

  #MSR635745
  def test11_brscript_param_update
    assert lot = @@svtest.new_lot(product: @@product, route: @@route)
    @@testlots << lot
    name = 'UTstring'
    t = Thread.new {@@sv.lot_opelocate(lot, @@opNo)}
    assert_equal 0, @@sv.user_parameter_change('Lot', lot, name, 'extern'), "failed to set script parameter"
    t.join
    script = @@sv.user_parameter('Lot', lot, name: name)
    assert script.count > 0, "failed to get script parameter #{name}"
    assert_equal 'Script', script.value
  end

  def test20_parallel_update_with_script
    assert lot = @@svtest.new_lot(product: @@product, route: @@route)
    @@testlots << lot
    assert_equal 0, @@sv.user_parameter_change('Lot', lot, 'UTstring', 'test')
    t = Thread.new {@@sv.lot_opelocate(lot, @@skip_chain)}
    sleep 10 # script should be running
    begin
      Timeout.timeout(@@timeout) do
        10.times {|i|
          @@script_parms.each {|uparm| @@sv.user_parameter_change('Lot', lot, uparm, "Test_#{i}")}
        }
      end
    rescue
      assert false, "no timeout expected"
    end
    t.join
    @@script_parms.each {|uparm| assert_equal 'Test_9', @@sv.user_parameter('Lot', lot, name: uparm).value}
  end


  def change_uparams(lot, count=0, update_val=nil, carrier=nil)
    # setup script parameter calls
    Thread.new {
      assert wait_for(timeout: 3 * @@timeout) {
        _params = nil
        begin
          Timeout.timeout(@@timeout) {
            l = lot
            if carrier
              while lot == l
                l = @@sv.lot_list_incassette(carrier)[0]
                sleep 2
              end
            end
            @@sv.user_parameter_change('Lot', l, 'UTstring', update_val) if update_val
            _params = @@sv.user_parameter('Lot', l)
          }
        rescue
          assert false, "no timeout expected"
        end
        _params && _params.count == count
      }, "failed to get script parameters"
    }
  end

end
