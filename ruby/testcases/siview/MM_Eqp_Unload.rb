=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: Daniel Steger, 2013-07-15

History:
  2016-02-11 sfrieske, cleanup
  2019-12-09 sfrieske, minor cleanup, renamed from Test041_UnloadFixedBuffer
  2020-08-24 sfrieske, minor cleanup
=end

require 'SiViewTestCase'


# Test carrier loading of fixed buffer tool for load purposes MSR574607
class MM_Eqp_Unload < SiViewTestCase
  @@eqp = 'UTF002'
  @@recycle_bank = 'UT-RECYCLE'
  @@startbank = 'UT-RAW'
  @@srcproduct = 'UT-RAW.00'

  @@testlots = []


  def self.after_all_passed
    @@sv.eqp_cleanup(@@eqp)
    @@testlots.each {|lot| @@sv.delete_lot_family(lot)}
  end


  def test00_setup
    $setup_ok = false
    #
    assert @@carrier = @@svtest.get_empty_carrier, 'no empty carrier found'
    assert vl = @@sv.vendorlot_receive(@@startbank, @@srcproduct, 25)
    assert @@vlot = @@sv.vendorlot_prepare(vl, @@carrier)
    @@testlots << @@vlot
    # create 4 monitor lots, rest of the vendor lot is still in the carrier
    @@lots = []
    4.times {
      assert lot = @@svtest.new_controllot('Equipment Monitor', nwafers: 5, srclot: @@vlot, bankin: true)
      @@testlots << lot
      @@lots << lot
    }
    @@endbank = @@sv.lot_info(@@lots.first).bank
    assert @@opNo = @@sv.lot_eqplist_in_route(@@lots.first, eqp: @@eqp).keys.first, "no operation to process lot #{@@lots.first} at #{@@eqp}"
    $log.info "using process opNo #{@@opNo.inspect}"
    # check bank for control use state or recycle
    @@banks = {}
    @@recycle = []
    assert bankinfo = @@sv.bank_list
    assert brinfo = @@sv.eqp_info_raw(@@eqp).equipmentBRInfo
    [
      [@@recycle_bank, 'Recycle'], [brinfo.monitorBank.identifier, 'Waiting Monitor Lot'],
      [brinfo.dummyBank.identifier, 'Filler Dummy Lot'], [brinfo.dummyBank.identifier, 'Side Dummy Lot']
    ].each {|ebank, purpose|
      assert bi = bankinfo.find {|b| b.bankID.identifier == ebank}, "failed to find bank #{ebank}"
      @@recycle << bi.bankID.identifier if bi.recycleBankFlag  # add all banks with recycle set
      @@banks[purpose] = ebank
    }
    @@recycle.uniq!
    $log.info "#{@@banks.inspect}"
    #
    assert @@svtest.cleanup_eqp(@@eqp, mode: 'Off-Line-1')
    #
    $setup_ok = true
  end

  def test10_npw
    assert @@svtest.cleanup_carrier(@@carrier)
    ports = @@sv.eqp_info(@@eqp).ports
    assert_equal 0, @@sv.carrier_xfer_status_change(@@carrier, 'EO', @@eqp)
    ['Filler Dummy Lot', 'Side Dummy Lot', 'Waiting Monitor Lot'].each {|purpose|
      # find port to load
      assert p = ports.find {|p| p.purpose == purpose}, 'no port found'
      port = p.port
      $log.info "-- load purpose #{purpose.inspect}, #{port}"
      # all lots in carrier need to go to bank
      @@testlots.each {|lot|
        li = @@sv.lot_info(lot)
        unless li.status == 'COMPLETED'
          @@sv.lot_opelocate(lot, :last)
          @@sv.lot_bankin(lot)
        end
        assert_equal 0, @@sv.lot_bank_move(lot, @@banks[purpose])
        assert_equal 0, @@sv.lot_ctrl_status_change(lot, 'WaitUse')
      }
      #
      assert_equal 0, @@sv.npw_reserve(@@eqp, port, @@carrier, purpose: purpose)
      # load carrier
      assert_equal 0, @@sv.eqp_load(@@eqp, port, @@carrier, purpose: purpose), "error loading carrier with #{purpose}: #{@@sv.msg_text}"
      @@testlots.each {|lot|
        assert_equal 'InUse', @@sv.lots_info_raw(lot, ctrluse: true).strLotInfo.first.strLotControlUseInfo.controlUseState, "wrong control use state for #{lot}"
      }
      assert_equal 0, @@sv.eqp_unload(@@eqp, port, @@carrier)
      @@testlots.each {|lot|
        use_state = @@recycle.member?(@@banks[purpose]) ? 'WaitRecycle' : 'WaitUse'
        assert_equal use_state, @@sv.lots_info_raw(lot, ctrluse: true).strLotInfo.first.strLotControlUseInfo.controlUseState, "wrong control use state for #{lot}"
      }
    }
  end

  def test15_other
    # prepare lot
    plot = @@lots.first
    assert_equal 0, @@sv.lot_cleanup(plot, opNo: @@opNo, endbank: @@endbank)
    @@lots[1..-1].each_with_index {|lot, i| assert_equal 0, @@sv.lot_bank_move(lot, @@banks.values.uniq[i])}
    assert_equal 0, @@sv.lot_bank_move(@@vlot, @@startbank)
    #
    assert @@svtest.cleanup_carrier(@@carrier)
    assert_equal 0, @@sv.carrier_xfer_status_change(@@carrier, 'EO', @@eqp)
    @@sv.eqp_info(@@eqp).ports.each {|p|
      assert_equal 0, @@sv.eqp_load(@@eqp, p.port, @@carrier, purpose: 'Other'), "error loading carrier: #{@@sv.msg_text}"
      @@testlots.each {|lot|
        next if lot == plot # ignore processing lot
        li = @@sv.lot_info(lot)
        use_state = @@recycle.member?(li.bank) ? 'WaitRecycle' : 'WaitUse'
        assert_equal use_state, @@sv.lots_info_raw(lot, ctrluse: true).strLotInfo.first.strLotControlUseInfo.controlUseState, "wrong control use state for #{lot}"
      }
      assert_equal 0, @@sv.eqp_unload(@@eqp, p.port, @@carrier)
      @@testlots.each {|lot|
        next if lot == plot # ignore processing lot
        li = @@sv.lot_info(lot)
        use_state = @@recycle.member?(li.bank) ? 'WaitRecycle' : 'WaitUse'
        assert_equal use_state, @@sv.lots_info_raw(lot, ctrluse: true).strLotInfo.first.strLotControlUseInfo.controlUseState, "wrong control use state for #{lot}"
      }
    }
  end

  def test20_slr
    # prepare lot
    plot = @@lots.first
    assert_equal 0, @@sv.lot_cleanup(plot, opNo: @@opNo, endbank: @@endbank)
    @@lots[1..-1].each_with_index {|lot, i| assert_equal 0, @@sv.lot_bank_move(lot, @@banks.values.uniq[i])}
    assert_equal 0, @@sv.lot_bank_move(@@vlot, @@startbank)
    #
    assert p = @@sv.eqp_info(@@eqp).ports.find {|p| p.purpose == 'Any Purpose'}, "failed to find port for processing"
    assert @@sv.claim_process_prepare(plot, eqp: @@eqp, port: p.port)
    # check control use state
    @@testlots.each {|lot|
      next if lot == plot # ignore processing lot
      li = @@sv.lot_info(lot)
      use_state = @@recycle.member?(li.bank) ? 'WaitRecycle' : 'WaitUse'
      assert_equal use_state, @@sv.lots_info_raw(lot, ctrluse: true).strLotInfo.first.strLotControlUseInfo.controlUseState, "wrong control use state for #{lot}"
    }
    assert cj = @@sv.eqp_opestart(@@eqp, @@carrier), "error starting lots"
    assert_equal 0, @@sv.eqp_tempdata(@@eqp, p.pg, cj)
    assert_equal 0, @@sv.eqp_opecomp(@@eqp, cj)
    assert_equal 0, @@sv.eqp_unload(@@eqp, p.port, @@carrier)
    # check control use state
    @@testlots.each {|lot|
      next if lot == plot # ignore processing lot
      li = @@sv.lot_info(lot)
      use_state = @@recycle.member?(li.bank) ? 'WaitRecycle' : 'WaitUse'
      assert_equal use_state, @@sv.lots_info_raw(lot, ctrluse: true).strLotInfo.first.strLotControlUseInfo.controlUseState, "wrong control use state for #{lot}"
    }
  end

end
