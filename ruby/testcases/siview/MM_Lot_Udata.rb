=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: Steffen Steidten, 2015-06-29

History:
  2017-05-08 sfrieske, added tests for AutomotiveProduct and SecurityProduct
  2019-05-15 sfrieske, removed AutomotiveProduct and SecurityProduct tests (C7.12)
  2019-12-05 sfrieske, minor cleanup, renamed from Test_LotUdata
  TODO: merge with a suitable lot label test, # MSR 1168724, MES-3386 are removed with C7.12
=end

require 'SiViewTestCase'


# Test lot UDATA incl. LOT_LABEL
class MM_Lot_Udata < SiViewTestCase
  @@customer = 'gfull'
  # for Automotive and Security products
  @@sproduct = 'UT-PRODUCT000.SEC'
  @@sroute = 'UTRT001.01'
  @@testlots = []


  def self.after_all_passed
    @@testlots.each {|lot| @@sv.delete_lot_family(lot)}
  end


  def test00_setup
    $setup_ok = false
    #
    assert udata = @@sv.user_data(:customer, @@customer), 'customer not defined in SM?'
    assert_equal 'IBMULL', udata['CustomLIG_LotIdFormat'], "wrong UDATA CustomLIG_LotIdFormat for #{@@customer}"
    assert @@lot = @@svtest.new_lot(customer: @@customer), 'error creating test lot'
    @@testlots << @@lot
    #
    $setup_ok = true
  end

  def test11_lot_label
    $setup_ok = false
    #
    # lot label is generated
    assert @@sv.lot_label(@@lot)
    refute_empty @@sv.lot_label(@@lot), 'empty lot label'
    # child lot
    assert lc = @@sv.lot_split(@@lot, 4)
    assert lclabel = @@sv.lot_label(lc), 'lot label must be created'
    assert_equal 10, lclabel.size, 'wrong child lot label'
    # grandchild lot
    assert lgc = @@sv.lot_split(lc, 2)
    assert lgclabel = @@sv.lot_label(lgc), 'lot label must be created'
    assert_equal 10, lclabel.size, 'wrong child lot label'
    #
    $setup_ok = true
  end

  def test12_udata
    @@sv.lot_family(@@lot).each {|lot|
      assert_equal ['CustomLIG_LotIdFormatDetail', 'LOT_LABEL'], @@sv.user_data(:lot, lot).keys.sort, "wrong UDATA for #{lot}"
    }
  end

  def test21_nolabel
    # delete the lot's label
    assert_equal 0, @@sv.user_data_delete(:lot, @@lot, 'LOT_LABEL')
    assert_empty @@sv.lot_label(@@lot)
    # child lot: must have no lot label
    assert lc = @@sv.lot_split(@@lot, 4)
    assert_empty @@sv.lot_label(lc), 'child lot label must not be created'
    # grandchild lot: must have no lot label
    assert lgc = @@sv.lot_split(lc, 2)
    assert_empty @@sv.lot_label(lgc), 'grandchild lot label must not be created'
  end

  # MSR 1168724, MES-3386, removed with C7.12   TODO: remove
  def XXtest31_automotive
    assert $sm.update_udata(:product, @@sproduct, {'AutomotiveProduct'=>'N', 'SecurityProduct'=>'Y'}), "SM error"
    assert lot = @@svtest.new_lot(product: @@sproduct, route: @@sroute), "error creating test lot"
    @@testlots << lot
    # verify lot
    assert_equal 'N', @@sv.user_data(:lot, lot)['AutomotiveLot'], "wrong lot UDATA"
    assert_equal 'Y', @@sv.user_data(:lot, lot)['SecurityLot'], "wrong lot UDATA"
    # verify child lot
    assert lc = @@sv.lot_split(lot, 5), "error splitting lot"
    assert_equal 'N', @@sv.user_data(:lot, lc)['AutomotiveLot'], "wrong lot UDATA"
    assert_equal 'Y', @@sv.user_data(:lot, lc)['SecurityLot'], "wrong lot UDATA"
    # change child lot UDATA and try to merge
    assert_equal 0, @@sv.user_data_update(:lot, lc, {'AutomotiveLot'=>'Y', 'SecurityLot'=>'Y'})
    assert_equal 10139, @@sv.lot_merge(lot, lc), "merge must be rejected"
    assert_equal 0, @@sv.user_data_delete(:lot, lc, ['AutomotiveLot', 'SecurityLot'])
    assert_equal 10139, @@sv.lot_merge(lot, lc), "merge must be rejected"
    # same UDATA, merge succeeds
    assert_equal 0, @@sv.user_data_update(:lot, lc, {'AutomotiveLot'=>'N', 'SecurityLot'=>'Y'})
    assert_equal 0, @@sv.lot_merge(lot, lc), "merge must succeed"
  end

  # removed with C7.12   TODO: remove
  def XXtest32_noautomotive
    # create a regular lot and verify no automitive and security UDATA are set
    assert lot = @@svtest.new_lot
    @@testlots << lot
    assert_nil @@sv.user_data(:lot, lot)['AutomotiveLot'], "wrong lot UDATA"
    assert_nil @@sv.user_data(:lot, lot)['SecurityLot'], "wrong lot UDATA"
  end

end
