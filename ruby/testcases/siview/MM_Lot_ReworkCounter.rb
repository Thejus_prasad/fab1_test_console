=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: Daniel Steger, 2015-09-10

History:
  2016-02-18 dsteger,  Test case fixed for MES-2434
  2019-12-09 sfrieske, minor cleanup, renamed from Test420_ReworkCounter
  2020-08-13 sfrieske, more cleanup
  2021-02-05 sfrieske, pass wafers as slots instead of aliases
=end

require 'SiViewTestCase'


class MM_Lot_ReworkCounter < SiViewTestCase
  @@sv_defaults = {product: 'UT-PROD-REWORK.01', route: 'UTRT-REWORK.01'}
  @@rework_mod = %w(1000 1500 2000)
  @@rework_op = %w(1100 1300)
  @@join_op = %w(1000 1300)

  @@rework_route1 = 'UTRW001.01'
  @@rework_route2 = 'UTRW002.01'
  @@rework_dynamic = 'UTRW001D.01'

  @@testlots = []


  def self.after_all_passed
    @@testlots.each {|lot| @@sv.delete_lot_family(lot)}
  end


  def test01_rework
    assert lot = @@svtest.new_lot, 'error creating test lot'
    @@testlots << lot
    wafers = [1, 2]
    @@rework_mod.each {|mod|
      mod_counter = 0 # reset per module
      mod_counter_wafer = 0
      2.times {|i|
        is_dynamic = i.odd? && !@@sv.f7
        join_opNo = "#{mod}.#{@@join_op[0]}"
        rw_opNo = "#{mod}.#{@@rework_op[0]}"
        assert_equal 0, @@sv.lot_opelocate(lot, rw_opNo)
        rw_route = is_dynamic ? @@rework_dynamic : @@rework_route1
        # check the rework count (both have 0 or previous value)
        assert verify_rw_counter(lot, i, mod_counter, wafers, i, mod_counter_wafer)
        ## lot based rework
        assert_equal 0, @@sv.lot_rework(lot, rw_route, return_opNo: join_opNo, dynamic: is_dynamic)
        assert_equal 0, @@sv.lot_opelocate(lot, rw_opNo)
        # rework counters are updated
        mod_counter += 1
        mod_counter_wafer += 1
        # check the rework count (all increased)
        assert verify_rw_counter(lot, i+1, mod_counter, wafers, i + 1, mod_counter_wafer)

        join_opNo2 = "#{mod}.#{@@join_op[1]}"
        rw_route2 = is_dynamic ? @@rework_dynamic : @@rework_route2
        rw_opNo = "#{mod}.#{@@rework_op[1]}"
        assert_equal 0, @@sv.lot_opelocate(lot, rw_opNo)
        # check the rework count (still old value, cumulative counter already updated)
        assert verify_rw_counter(lot, 0, mod_counter, wafers, i, mod_counter_wafer)
        ## wafer level rework
        assert child = @@sv.lot_partial_rework(lot, wafers, rw_route2, return_opNo: join_opNo2, dynamic: is_dynamic), "failed to do partial rework"
        assert_equal 0, @@sv.lot_opelocate(child, rw_opNo)
        assert_equal 0, @@sv.lot_merge(lot, child), "failed to merge lots"
        mod_counter_wafer += 1
        # check the rework count (only cumulative counter updated)
        assert verify_rw_counter(lot, 0, mod_counter, wafers, i + 1, mod_counter_wafer)
      }
    }
  end

  def verify_rw_counter(lot, op_count, mod_count, wafers, w_op_count, w_mod_count)
    $log.info "verify_rw_counter #{lot.inspect}, #{op_count}, #{mod_count}, #{wafers.inspect}, #{w_op_count}, #{w_mod_count}"
    assert liraw = @@sv.lots_info_raw(lot, wafers: true)
    assert rwinfos = @@sv.lot_wafer_rework_count(lot), 'failed to get rework counter'
    rwinfos.each {|rwinfo|
      wattrs = liraw.strLotInfo.first.strLotWaferAttributes.find {|e| e.waferID.identifier == rwinfo.waferID.identifier}
      if wafers.find {|slot| rwinfo.position == slot}
        assert_equal w_op_count, wattrs.reworkCount, 'wrong operation reworkCount'
        assert_equal w_mod_count, rwinfo.reworkCount, 'wrong operation reworkCount'
      else
        assert_equal op_count, wattrs.reworkCount, 'wrong operation reworkCount'
        assert_equal mod_count, rwinfo.reworkCount, 'wrong operation reworkCount'
      end
    }
  end

end
