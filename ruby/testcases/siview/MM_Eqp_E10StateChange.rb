=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: Beate Kochinka 2010-08-13

History:
  2013-03-25 dsteger,  major rework
  2014-03-28 dsteger,  dual CMMS support via userid (MSR841753)
  2016-02-08 sfrieske, removed fixture shelf tests for Fab1, expanded test20
  2018-06-08 sfrieske, minor code fixes and cleanup
  2019-12-09 sfrieske, minor cleanup, renamed from Test212_E10StateChange
=end

require 'SiViewTestCase'


# Tests various E10 status changes, incl MSR704108 (transport eqp). Enabled tests for E10 EI notification
# Need to run for valid @@cmms_udata Xsite or SAPPM
# TODO: add partial operation completion
class MM_Eqp_E10StateChange < SiViewTestCase
  @@stocker_classes = ['Intra Bay', 'Auto', 'Shelf', 'BareReticle', 'ReticlePod', 'ReticleShelf', 'Fixture']
  @@eqps = ['UTS001', 'UTC001', 'UTI001']
  @@eqps_chamber = ['UTI001:MODULE1', 'UTC001:PM3']
  @@eqps_proc = ['UTC001', 'UTI001'] # fixed buffer and internal buffer (from @@eqps)
  @@opNos = ['1000.700', '1000.800']
  @@work_area = 'UT'
  @@e10_notify = 'ON'
  # Need to test with all these entries, see MSR747147 and MSR483163
  @@clientnode = 'CMMS'    #'FC8XA5DESKQ01-dsteger'#''#'CMMS'
  @@dual_cmms = 'OFF'      # nil/OFF - is single CMMS or 'ON'
  @@cmms_udata = 'SAPPM'   # or Xsite
  @@state_down = '2NDP'
  @@state_sby = '2WPR'
  @@state_prd = '1PRD'

  @@qual_state_trans = ['2NDP', '4DLY', '4MTN', '4QUL', '2NDP', '2WPR']


  def self.after_all_passed
    @@eqps_proc.each {|eqp| @@sv.eqp_cleanup(eqp)}
    @@sv.delete_lot_family(@@lot)
  end


  def test00_setup
    $setup_ok = false
    #
    @@stocker_classes.delete('Fixture') if $env.start_with?('itdc', 'let')
    #
    assert_equal @@e10_notify, @@sv.environment_variable[1]['CS_SP_EI_E10_NOTIFY'], "incorrect server setting"
    assert_equal @@dual_cmms, (@@sv.environment_variable[1]['CS_SP_DUALCMMS_ENABLE'] || ''), "incorrect value for DUALCMMS"
    if @@dual_cmms != '' && @@dual_cmms != 'OFF'
      # Update udata according setup
      @@eqps.each {|eqp|
        assert @@sm.update_udata(:eqp, eqp, {'CMMS'=>@@cmms_udata}), "error writing UDATA in SM"
        u = @@sv.user_data(:eqp, eqp)['CMMS'] || ''   # nil when empty string
        assert_equal @@cmms_udata, u, "UDATA CMMS not correctly set in SM"
      }
    end
    #
    @@eqps.each {|eqp| assert @@svtest.cleanup_eqp(eqp)}
    #
    assert @@lot = @@svtest.new_lot, 'error creating test lot'
    #
    $setup_ok = true
  end

  def test01_e10_state_changes_transport
    res = {}
    eqplist = @@sv.workarea_eqp_list(@@work_area)
    @@stocker_classes.each {|t|
      assert entity = eqplist.strAreaStocker.find {|s| s.stockerType == t}, " no #{t} found"
      res[t] = true
      if @@sv.f7
        if entity.stockerStatus.identifier == '4000'
          res[t] &= e10_state_change_stocker(entity.stockerID.identifier, '4300')
        end
        if entity.stockerStatus.identifier >= '4000'
          res[t] &= e10_state_change_stocker(entity.stockerID.identifier, '5000')
          res[t] &= e10_state_change_stocker(entity.stockerID.identifier, '5300')
          res[t] &= e10_state_change_stocker(entity.stockerID.identifier, '5340')
        end
      else
        res[t] &= e10_state_change_stocker(entity.stockerID.identifier, @@state_down)
      end
      res[t] &= e10_state_change_stocker(entity.stockerID.identifier, @@state_sby)
    }
    assert(res.inject(true) {|res, p| $log.info "#{p[0]}: #{p[1] ? 'OK' : 'NOK'}"; res && p[1]})
  end

  def test02_e10_eqp_state_change_req
    @@eqps.each {|eqp|
      @@qual_state_trans.each {|state|
        assert_equal 0, @@sv.eqp_status_change(eqp, state, client_node: @@clientnode), 'cannot change state'
        assert verify_e10state(eqp, state)
      }
    }
  end

  def test03_e10_eqp_state_change_rpt
    @@eqps.each {|eqp|
      # Tool needs to be online-remote
      assert_equal 0, @@sv.eqp_mode_change(eqp, 'Auto-1'), 'failed to switch mode'
      # First reset state to 2WPR
      assert_equal 0, @@sv.cleanup_e10state(eqp), "failed to reset E10 state of equipment"
      [@@state_sby, @@state_prd, @@state_sby].each {|state|
        assert_equal 0, @@sv.eqp_status_change_rpt(eqp, state, client_node: @@clientnode), 'cannot change state'
        assert verify_e10state(eqp, state)
      }
    }
  end

  def test04_e10_eqp_state_recover
    @@eqps.each {|eqp|
      # Tool needs to be offline
      assert_equal 0, @@sv.eqp_mode_change(eqp, 'Off-Line-1'), 'failed to switch mode'
      assert_equal 0, @@sv.eqp_status_change(eqp, @@state_prd, client_node: @@clientnode), 'cannot change state'
      assert verify_e10state(eqp, @@state_prd)
      assert_equal 0, @@sv.eqp_status_recover(eqp), 'failed to set E10 state to WAIT-PRODUCT'
      assert verify_e10state(eqp, @@state_sby)
    }
  end

  def test10_e10_change_state_change_req
    @@eqps_chamber.each {|ec|
      eqp, ch = ec.split(':')
      @@qual_state_trans.each {|state|
        assert e10_state_change_chamber(eqp, ch, state, :chamber_status_change), 'E10 state change failed'
      }
    }
  end

  def test11_e10_change_state_change_rpt
    @@eqps_chamber.each {|ec|
      eqp, ch = ec.split(":")
      # tool needs to be online-remote
      assert_equal 0, @@sv.eqp_mode_change(eqp, 'Auto-1'), 'failed to switch mode'
      assert_equal 0, @@sv.cleanup_e10state(eqp, chamber: ch), "failed to cleanup #{ch} E10 state"
      [@@state_sby, @@state_prd, @@state_sby].each {|state|
        assert e10_state_change_chamber(eqp, ch, state, :chamber_status_change_rpt), 'E10 state change failed'
      }
    }
  end

  def test12_e10_change_state_all_chambers
    @@eqps_chamber.each {|ec|
      eqp, ch = ec.split(':')
      chambers = @@sv.eqp_info(eqp).chambers.map {|c| c.chamber}
      @@qual_state_trans.each {|state|
        assert e10_state_change_chamber(eqp, chambers, state, :chamber_status_change), 'E10 state change failed'
      }
    }
  end

  def test21_offline_opecomp
    @@eqps_proc.each_with_index {|eqp, i|
      assert_equal 0, @@sv.eqp_cleanup(eqp, mode: 'Off-Line-1')
      assert_equal 0, @@sv.lot_cleanup(@@lot, opNo: @@opNos[i])
      assert cj = @@sv.claim_process_lot(@@lot, eqp: eqp) {verify_e10state(eqp, @@state_prd)}
      assert verify_e10state(eqp, @@state_sby)
    }
  end

  def test22_offline_forceopecomp
    @@eqps_proc.each_with_index {|eqp, i|
      assert_equal 0, @@sv.eqp_cleanup(eqp, mode: 'Off-Line-1')
      assert_equal 0, @@sv.lot_cleanup(@@lot, opNo: @@opNos[i])
      assert cj = @@sv.claim_process_lot(@@lot, eqp: eqp, running_hold: true) {verify_e10state(eqp, @@state_prd)}
      assert verify_e10state(eqp, @@state_sby)
    }
  end

  def test23_offline_opestart_cancel
    @@eqps_proc.each_with_index {|eqp, i|
      assert_equal 0, @@sv.eqp_cleanup(eqp, mode: 'Off-Line-1')
      assert_equal 0, @@sv.lot_cleanup(@@lot, opNo: @@opNos[i])
      assert cj = @@sv.claim_process_lot(@@lot, eqp: eqp, opestart_cancel: true) {verify_e10state(eqp, @@state_prd)}
      assert verify_e10state(eqp, @@state_sby)
    }
  end


  def verify_e10state(eqp, state)
    $log.info "verify_e10state #{eqp.inspect}, #{state.inspect}"
    assert_equal state, @@sv.eqp_info(eqp).status.split('.').last, 'wrong state in SiView'
    ##sleep 1 #to allow sync
    assert_equal state, @@svtest.eis.e10state(eqp), 'wrong state reported to EI' if @@e10_notify == 'ON'
    return true
  end

  def e10_state_change_chamber(eqp, chambers, state, func)
    chambers = [chambers] unless chambers.kind_of?(Array)
    assert_equal 0, @@sv.send(func, eqp, chambers, state, client_node: @@clientnode), 'cannot change E10 state'
    chis = @@sv.eqp_info(eqp).chambers
    chambers.each {|chamber|
      assert chi = chis.find {|ch| ch.chamber == chamber }, "chamber #{chamber.inspect} not found"
      assert_equal state, chi.status.split('.').last, "wrong state in SiView: #{chi}"
      ##sleep 1 #to allow sync
      assert_equal state, @@svtest.eis.e10state(eqp, chamber), 'wrong chamber state reported to EI' if @@e10_notify == 'ON'
    }
    return true
  end

  def e10_state_change_stocker(entity, state)
    assert_equal 0, @@sv.eqp_status_change(entity, state, client_node: @@clientnode), 'cannot change E10 state'
    eqplist = @@sv.workarea_eqp_list(@@work_area)
    entity = eqplist.strAreaStocker.find {|s| s.stockerID.identifier == entity}
    assert_equal state, entity.stockerStatus.identifier, "wrong E10 state for #{entity}"
    return true
  end

end
