=begin 
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.
    
Author: Steffen Steidten, 2011-01-05

History:
  2013-03-07 dsteger,  added MSR 256958
  2015-05-20 ssteidte, separated from Test124_ReserveCarrierSO
  2019-12-09 sfrieske, minor cleanup, renamed from Test125_ReserveLotEI
=end

require 'SiViewTestCase'

# SLR for lots on tools (equipment-to-equipment transfers for fixed buffer tools Auto-1 and Semi-1), MSR256958
#
# For a more comprehensive test run MM_Eqp_ReserveCarrierSO(Test124_ReserveCarrier).
class MM_Eqp_ReserveLotEI < SiViewTestCase
  @@eqp = 'UTC001'
  @@opNo = '1000.700'
  @@other_eqp = 'UTF001'
  @@port = 'P1'
  @@modes = ['Auto-1', 'Semi-1']
  @@modes_wrong = ['Auto-2', 'Semi-2', 'Auto-3']

  
  def self.shutdown
    [@@eqp, @@other_eqp].each {|eqp| @@svtest.cleanup_eqp(eqp, mode: 'Off-Line-1')}
  end

  def self.after_all_passed
    @@sv.delete_lot_family(@@lot)
  end


  def test00_setup
    $setup_ok = false
    #
    # cleanup eqp
    [@@eqp, @@other_eqp].each {|eqp| @@svtest.cleanup_eqp(eqp, mode: 'Off-Line-1')}
    # create test lot
    assert @@lot = @@svtest.new_lot
    # locate to opNo for @@eqp
    assert_equal 0, @@sv.lot_opelocate(@@lot, @@opNo)
    # load at other_eqp
    @@carrier = @@sv.lot_info(@@lot).carrier
    assert_equal 0, @@sv.carrier_xfer_status_change(@@carrier, 'EO', @@other_eqp)
    assert_equal 0, @@sv.eqp_load(@@other_eqp, nil, @@carrier, purpose: 'Other')
    #
    $setup_ok = true
  end


  def test11_allowed_modes
    @@modes.each {|mode|
      $log.info "--- mode #{mode}"
      assert_equal 0, @@sv.eqp_cleanup(@@eqp, mode: mode)
      assert cj = @@sv.slr(@@eqp, lot: @@lot)
      assert @@sv.eqp_info(@@eqp).rsv_carriers.include?(@@carrier)
      assert @@sv.slr_cancel(@@eqp, cj)
    }
  end

  def test12_wrong_modes
    @@modes_wrong.each {|mode|
      $log.info "--- mode #{mode}"
      assert_equal 0, @@sv.eqp_cleanup(@@eqp, mode: mode)
      assert_nil @@sv.slr(@@eqp, lot: @@lot)
    }
  end

end
