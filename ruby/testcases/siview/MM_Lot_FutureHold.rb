=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: Paul Peschel, 2013-07-25

History:
  2015-05-22 ssteidte, cleaned up and verified with R15
  2015-08-07 dsteger,  added Fab7 specific functions
  2016-07-12 sfrieske, added tests for MES-711/MSR711310 and renamed to Test097_FutureHold
  2017-05-03 sfrieske, added tests for Pre/Post and Single/Multiple flags (MSRs 1147724, 1143564)
  2019-12-05 sfrieske, minor cleanup, renamed from Test097_FutureHold  TODO: use variables for opNos!
=end

require 'SiViewTestCase'


# Test MSR 393401 FutureHold with merge and Split (fixed in Core R10.0.1-16)
class MM_Lot_FutureHold < SiViewTestCase
  @@reason = 'INT'
  @@opNo_merge = '1000.150'   # 1 op before @@psm_split, TODO: simplify/automate setup detection
  @@psm_route = 'UTBR002.01'
  @@psm_split = '1000.200'
  @@psm_between = '1000.300'
  @@psm_join = '1000.400'
  @@split_route = 'UTBR002.01'
  @@other_route = 'e2403-UT-SCRIPT-A0.01' # route not connected to main route

  @@testlots = []


  def self.after_all_passed
    @@testlots.each {|lot| @@sv.delete_lot_family(lot)}
  end


  def test00_setup
    $setup_ok = false
    #
    assert @@lot = @@svtest.new_lot, 'error creating test lot'
    @@testlots << @@lot
    #
    $setup_ok = true
  end

  def test11_parent_route
    # split lot, child on parent route, set merge point before future hold operation
    assert_equal 0, @@sv.lot_opelocate(@@lot, :first)
    assert_equal 0, @@sv.lot_futurehold(@@lot, "1000.200", @@reason)
    assert lc = @@sv.lot_split(@@lot, 5, mergeop: @@opNo_merge)
    assert_equal 0, @@sv.lot_opelocate(@@lot, @@opNo_merge)
    assert_equal 0, @@sv.lot_opelocate(lc, @@opNo_merge)
    check_automerge(@@lot, lc)
    assert_equal 0, @@sv.lot_opelocate(@@lot, "1000.200")
    assert_equal 0, @@sv.lot_hold_release(@@lot, @@reason)
    assert_equal 0, @@sv.lot_futurehold_cancel(@@lot, @@reason)
    # split lot, child on parent route, set merge point at future hold operation
    assert_equal 0, @@sv.lot_opelocate(@@lot, :first)
    assert_equal 0, @@sv.lot_futurehold(@@lot, @@opNo_merge, @@reason)
    assert lc = @@sv.lot_split(@@lot, 5, mergeop: @@opNo_merge)
    assert_equal 0, @@sv.lot_opelocate(@@lot, @@opNo_merge)
    assert_equal 0, @@sv.lot_opelocate(lc, @@opNo_merge)
    assert_equal 0, @@sv.lot_merge(@@lot, lc)
    assert_equal 0, @@sv.lot_hold_release(@@lot, @@reason)
    assert_equal 0, @@sv.lot_futurehold_cancel(@@lot, @@reason)
    # split lot, child on parent route, set merge point after future hold operation
    assert_equal 0, @@sv.lot_opelocate(@@lot, :first)
    assert_equal 0, @@sv.lot_futurehold(@@lot, @@opNo_merge, @@reason)
    assert lc = @@sv.lot_split(@@lot, 5, mergeop: "1000.200")
    assert_equal 0, @@sv.lot_opelocate(@@lot, @@opNo_merge)
    assert_equal 0, @@sv.lot_opelocate(lc, @@opNo_merge)
    assert_equal 0, @@sv.lot_hold_release(@@lot, @@reason)
    assert_equal 0, @@sv.lot_hold_release(lc, @@reason)
    assert_equal 0, @@sv.lot_opelocate(@@lot, "1000.200")
    assert_equal 0, @@sv.lot_opelocate(lc, "1000.200")
    check_automerge(@@lot, lc)
    assert_equal 0, @@sv.lot_futurehold_cancel(@@lot, @@reason)
  end

  def test12_sub_route
    # split lot, child on sub route, set merge point before future hold operation
    assert_equal 0, @@sv.lot_opelocate(@@lot, "1000.200")
    assert_equal 0, @@sv.lot_futurehold(@@lot, "1000.500", @@reason)
    lc = @@sv.lot_split(@@lot, 5, split_route: @@split_route, retop: "1000.400", mergeroute: @@svtest.route, mergeop: "1000.400")
    assert_equal 0, @@sv.lot_opelocate(@@lot, "1000.400")
    assert_equal 0, @@sv.lot_opelocate(lc, "1000.400")
    check_automerge(@@lot, lc)
    assert_equal 0, @@sv.lot_opelocate(@@lot, "1000.500")
    assert_equal 0, @@sv.lot_hold_release(@@lot, @@reason)
    assert_equal 0, @@sv.lot_futurehold_cancel(@@lot, @@reason)
    assert_equal 0, @@sv.lot_opelocate(@@lot, "1000.200")
    # split lot, child on sub route, set merge point at future hold operation
    assert_equal 0, @@sv.lot_opelocate(@@lot, "1000.200")
    assert_equal 0, @@sv.lot_futurehold(@@lot, "1000.400", @@reason)
    lc = @@sv.lot_split(@@lot, 5, split_route: @@split_route, retop: "1000.400", mergeroute: @@svtest.route, mergeop: "1000.400")
    assert_equal 0, @@sv.lot_opelocate(@@lot, "1000.400")
    assert_equal 0, @@sv.lot_opelocate(lc, "1000.400")
    assert_equal 0, @@sv.lot_hold_release(@@lot, @@reason)
    assert_equal 0, @@sv.lot_hold_release(lc, @@reason)
    assert_equal 0, @@sv.lot_merge(@@lot, lc)
    assert_equal 0, @@sv.lot_futurehold_cancel(@@lot, @@reason)
    assert_equal 0, @@sv.lot_opelocate(@@lot, "1000.200")
    # split lot, child on sub route, set merge point after future hold operation
    assert_equal 0, @@sv.lot_futurehold(@@lot, "1000.300", @@reason)
    lc = @@sv.lot_split(@@lot, 5, split_route: @@split_route, retop: "1000.400", mergeroute: @@svtest.route, mergeop: "1000.400")
    assert_equal 619, @@sv.rc
    assert_equal 0, @@sv.lot_futurehold_cancel(@@lot, @@reason)
  end

  def test13_psm
    assert_equal 0, @@sv.lot_opelocate(@@lot, :first)
    # create PSM, set merge point before future hold operation
    assert_equal 0, @@sv.lot_futurehold(@@lot, "1000.950", @@reason)
    @@sv.lot_experiment(@@lot, 5, @@psm_route, @@psm_split, @@psm_join)
    assert_equal 0, @@sv.lot_opelocate(@@lot, @@psm_split)
    assert_equal 0, @@sv.lot_opelocate(@@lot, @@psm_join)
    lc = @@sv.lot_family(@@lot).last
    assert_equal @@psm_route, @@sv.lot_info(lc).route, "lot #{lc} was not split out successfully"
    assert_equal 0, @@sv.lot_opelocate(lc, @@psm_join)
    check_automerge(@@lot, lc)
    assert_equal 0, @@sv.lot_opelocate(@@lot, "1000.950")
    assert_equal 0, @@sv.lot_hold_release(@@lot, @@reason)
    assert_equal 0, @@sv.lot_futurehold_cancel(@@lot, @@reason)
    assert_equal 0, @@sv.lot_cleanup(@@lot)
    # create PSM, set merge point at future hold operation
    assert_equal 0, @@sv.lot_futurehold(@@lot, @@psm_join, @@reason)
    @@sv.lot_experiment(@@lot, 5, @@psm_route, @@psm_split, @@psm_join)
    assert_equal 0, @@sv.lot_opelocate(@@lot, @@psm_split)
    assert_equal 0, @@sv.lot_opelocate(@@lot, @@psm_join)
    lc = @@sv.lot_family(@@lot).last
    assert_equal @@psm_route, @@sv.lot_info(lc).route, "lot #{lc} was not split out successfully"
    assert_equal 0, @@sv.lot_opelocate(lc, @@psm_join)
    assert_equal 0, @@sv.lot_hold_release(@@lot, @@reason)
    assert_equal 0, @@sv.lot_hold_release(lc, @@reason)
    assert_equal 0, @@sv.lot_merge(@@lot, lc)
    assert_equal 0, @@sv.lot_futurehold_cancel(@@lot, @@reason)
    assert_equal 0, @@sv.lot_cleanup(@@lot)
    # create PSM, set join point after future hold operation
    assert_equal 0, @@sv.lot_futurehold(@@lot, @@psm_between, @@reason)
    assert_equal 0, @@sv.lot_experiment(@@lot, 5, @@psm_route, @@psm_split, @@psm_join)
    res = @@sv.lot_opelocate(@@lot, @@psm_split)
    if @@sv.f7
      assert_equal 0, res, "No error expected, but got #{@@sv.msg_text}"
      assert @@sv.lot_hold_list(@@lot, reason: 'PSME').count > 0, "PSME hold expected"
    else
      assert_equal 1738, res, "Post processing error expected, but got #{@@sv.msg_text}"
      assert_equal 0, @@sv.pp_force_delete("", @@lot)
    end
    assert_equal 0, @@sv.lot_futurehold_cancel(@@lot, @@reason)
    assert_equal 0, @@sv.lot_cleanup(@@lot)
  end

  def test14_no_merge
    # split lot, child on parent route, set no merge point, set future hold
    assert_equal 0, @@sv.lot_opelocate(@@lot, :first)
    assert_equal 0, @@sv.lot_futurehold(@@lot, "1000.200", @@reason)
    lc = @@sv.lot_split(@@lot, 5)
    assert_equal 0, @@sv.lot_opelocate(@@lot, @@opNo_merge)
    assert_equal 0, @@sv.lot_opelocate(lc, @@opNo_merge)
    assert_equal 0, @@sv.lot_merge(@@lot, lc)
    assert_equal 0, @@sv.lot_opelocate(@@lot, "1000.200")
    assert_equal 0, @@sv.lot_hold_release(@@lot, @@reason)
    assert_equal 0, @@sv.lot_futurehold_cancel(@@lot, @@reason)
    # split lot, child on sub route, no merge point, but sub route ends after future hold operation
    assert_equal 0, @@sv.lot_opelocate(@@lot, "1000.200")
    assert_equal 0, @@sv.lot_futurehold(@@lot, "1000.500", @@reason)
    lc = @@sv.lot_split(@@lot, 5, split_route: @@split_route, retop: "1000.400")
    assert_equal 0, @@sv.lot_opelocate(@@lot, "1000.400")
    assert_equal 0, @@sv.lot_opelocate(lc, "1000.400")
    assert_equal 0, @@sv.lot_merge(@@lot, lc)
    assert_equal 0, @@sv.lot_opelocate(@@lot, "1000.500")
    assert_equal 0, @@sv.lot_hold_release(@@lot, @@reason)
    assert_equal 0, @@sv.lot_futurehold_cancel(@@lot, @@reason)
    assert_equal 0, @@sv.lot_opelocate(@@lot, "1000.200")
    # split lot, child on sub route, set no merge point, set future hold after end of Sub route
    assert_equal 0, @@sv.lot_futurehold(@@lot, "1000.300", @@reason)
    lc = @@sv.lot_split(@@lot, 5, split_route: @@split_route, retop: "1000.400")
    assert_equal 619, @@sv.rc
    assert_equal 0, @@sv.lot_futurehold_cancel(@@lot, @@reason)
  end

  # changed with MES-711/MSR711310
  def test20_futurehold_branch_routes
    assert_equal 0, @@sv.lot_cleanup(@@lot), "failed to cleanup lot"
    # route is connected / not connected
    assert !@@sv.lot_processlist_in_route(@@lot, connected_routes: true).member?(@@other_route), "#{@@other_route} should not be connected"
    [@@psm_route, @@other_route].each {|route|
      assert psm_opNo = @@sv.route_operations(route).first.operationNumber, "failed to get operation for #{route}"
      # passes since MES-711/MSR711310 (C7.4) for F1/F8 privileged users or F7
      assert_equal 0, @@sv.lot_futurehold(@@lot, psm_opNo, @@reason, route: route)
      assert_equal 0, @@sv.lot_futurehold_cancel(@@lot, @@reason)
      # F1/8 non-privileged user
      if !@@sv.f7
        assert_equal 2880, @@sv.lot_futurehold(@@lot, psm_opNo, @@reason, route: route, privileged: false)
      end
    }
  end

  def test21_futurehold_not_existing_openo
    opNo = '12345.1234'
    # privileged user in F1/8 or F7
    assert_equal 0, @@sv.lot_futurehold(@@lot, opNo, @@reason), "future hold registration failed"
    assert_equal 0, @@sv.lot_futurehold_cancel(@@lot, @@reason)
    # F1/8 non-privileged user
    if !@@sv.f7
      assert_equal 2880, @@sv.lot_futurehold(@@lot, opNo, @@reason, privileged: false), "wrong future hold"
    end
  end

  def test22_futurehold_past_ope
    li = @@sv.lot_info(@@lot)
    assert_equal 0, @@sv.lot_opelocate(@@lot, 2), "failed to locate lot"
    # privileged user in F1/8 or F7
    assert_equal 0, @@sv.lot_futurehold(@@lot, li.opNo, @@reason), "future hold registration failed"
    assert_equal 0, @@sv.lot_futurehold_cancel(@@lot, @@reason)
    # F1/8 non-privileged user
    if !@@sv.f7
      assert_equal 2880, @@sv.lot_futurehold(@@lot, li.opNo, @@reason, privileged: false), "wrong future hold"
    end
  end

  # route does not exist: http://gfjira/browse/MES-3122 (allowed in C7.4.0.2)
  def test23_futurehold_not_existing_route
    # privileged user in F1/8 or F7
    assert_equal 0, @@sv.lot_futurehold(@@lot, '1000.1000', @@reason, route: 'DOESNOTEXIST.01'), "future hold registration failed"
    assert_equal 0, @@sv.lot_futurehold_cancel(@@lot, @@reason)
    # F1/8 non-privileged user
    if !@@sv.f7
      assert_equal 2880, @@sv.lot_futurehold(@@lot, '1000.1000', @@reason, route: 'DOESNOTEXIST.01', privileged: false), "wrong future hold"
    end
  end

  # MSRs 1147724, 1143564 (C7.6)
  def test31_flags_memo
    # create lot and child
    assert lot = @@svtest.new_lot, "error creating test lot"
    @@testlots << lot
    assert child = @@sv.lot_split(lot, 5)
    # set future holds with different post flags
    assert opNo = @@sv.lot_operation_list(lot)[2].opNo
    assert_equal 0, @@sv.lot_futurehold(lot, opNo, @@reason, post: true)
    assert_equal 0, @@sv.lot_futurehold(child, opNo, @@reason, post: false)
    assert_equal 10323, @@sv.lot_merge(lot, child)
    [lot, child].each {|l| assert_equal 0, @@sv.lot_futurehold_cancel(l, @@reason)}
    assert_equal 0, @@sv.lot_futurehold(lot, opNo, @@reason, post: false)
    assert_equal 0, @@sv.lot_futurehold(child, opNo, @@reason, post: true)
    assert_equal 10323, @@sv.lot_merge(lot, child)
    # set future holds with different single flags
    [lot, child].each {|l| assert_equal 0, @@sv.lot_futurehold_cancel(l, @@reason)}
    assert_equal 0, @@sv.lot_futurehold(lot, opNo, @@reason, single: true)
    assert_equal 0, @@sv.lot_futurehold(child, opNo, @@reason, single: false)
    assert_equal 10323, @@sv.lot_merge(lot, child)
    [lot, child].each {|l| assert_equal 0, @@sv.lot_futurehold_cancel(l, @@reason)}
    assert_equal 0, @@sv.lot_futurehold(lot, opNo, @@reason, single: false)
    assert_equal 0, @@sv.lot_futurehold(child, opNo, @@reason, single: true)
    assert_equal 10323, @@sv.lot_merge(lot, child)
    # set future holds with different claim memos
    [lot, child].each {|l| assert_equal 0, @@sv.lot_futurehold_cancel(l, @@reason)}
    assert_equal 0, @@sv.lot_futurehold(lot, opNo, @@reason, memo: 'QA1')
    assert_equal 0, @@sv.lot_futurehold(child, opNo, @@reason, memo: 'QA2')
    assert_equal 10323, @@sv.lot_merge(lot, child)
    # update memo and merge
    assert_equal 0, @@sv.lot_futureholdmemo_update(lot, 'QA2', reason: @@reason)
    assert_equal 0, @@sv.lot_merge(lot, child)
  end

  
  # aux methods

  def check_automerge(lot, child)
    if @@sv.f7
      # check for automerge
      assert_equal 'EMPTIED', @@sv.lot_info(child).status, 'automerge failed'
    else
      # merge any remaining child (even if nil given)
      assert_equal 0, @@sv.lot_merge(lot, child), 'merge failed'
    end
  end

end
