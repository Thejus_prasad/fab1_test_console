=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: Paul Peschel, 2013-03-29

History:
  2015-09-01 dsteger,  added MSR tests
  2015-09-30 sfrieske, added test for MSR661541 (R15)
  2019-04-08 sfrieske, minor cleanup
  2019-12-09 sfrieske, minor cleanup, renamed from Test088_ProcessHolds
  2021-08-27 sfrieske, removed verify_equal
=end

require 'SiViewTestCase'


# Test MSR 799187 Product and Route specific process holds, Cannot register multiple process holds for same route,
# incl MSRs 351455, 661541, 799187
class MM_ProcessHolds < SiViewTestCase
    @@product2 = 'UT-PRODUCT002.01'
    @@opNo = '1000.200'
    @@reason = 'E-LA'
    @@reason_2 = 'BR'
    @@rework_route = 'UTRW001.01'
    @@rework_opNo = '1000.400'
    @@rework_ret = '1000.400'
    @@psm_branches = ['UTBR001.01', 'UTBR002.01']
    @@psm_return_opNos = ['1000.300', '1000.400']
    @@ph_movement = '1'

    @@testlots = []


    def self.startup
      super
      @@product = @@svtest.product
      @@route = @@svtest.route
    end

    def self.after_all_passed
      @@sv.process_hold_list(route: @@route).each {|h| @@sv.process_hold_cancel(holdrecord: h)}
      @@testlots.each {|lot| @@sv.delete_lot_family(lot)}
    end


    def setup
      @@sv.process_hold_list(route: @@route).each {|h| @@sv.process_hold_cancel(holdrecord: h)}
    end

    def teardown
      @@sv.process_hold_list(route: @@route).each {|h| @@sv.process_hold_cancel(holdrecord: h)}
    end


    def test00_setup
      $setup_ok = false
      #
      refute_equal @@product, @@product2, 'products must be different'
      assert_equal @@ph_movement, @@sv.environment_variable[1]['SP_PROCESSHOLD_ALLOW_LOTMOVEMENT'], 'wrong MM env variable'
      assert @@testlots = @@svtest.new_lots(2), 'error creating test lots'
      #
      $setup_ok = true
    end

    def test11_product_processhold_exists
      skip "MSR 799187 not in Fab7" if @@sv.f7
      # Product specific
      assert_equal 0, @@sv.process_hold(@@reason, product: @@product, opNo: @@opNo, route: @@route), 'error setting process hold'
      assert_equal 1, @@sv.process_hold_list(route: @@route).count
      assert_equal 10921, @@sv.process_hold(@@reason, opNo: @@opNo, route: @@route), 'wrong process hold'
      assert_equal 1, @@sv.process_hold_list(route: @@route).count
    end

    def test12_route_processhold_exists
      # Product specific
      assert_equal 0, @@sv.process_hold(@@reason, opNo: @@opNo, route: @@route), 'error setting process hold'
      assert_equal 1, @@sv.process_hold_list(route: @@route).count
      assert_equal 408, @@sv.process_hold(@@reason, opNo: @@opNo, route: @@route, product: @@product), "wrong process hold"
      assert_equal 1, @@sv.process_hold_list(route: @@route).count
    end

    def test13_same_processhold_exists
      # reason specific
      assert_equal 0, @@sv.process_hold(@@reason, opNo: @@opNo, route: @@route), 'error setting process hold'
      assert_equal 1, @@sv.process_hold_list(route: @@route).count
      assert_equal 0, @@sv.process_hold(@@reason_2, opNo: @@opNo, route: @@route), 'error setting process hold'
      assert_equal 2, @@sv.process_hold_list(route: @@route).count
      @@sv.process_hold_list(route: @@route).each {|h| @@sv.process_hold_cancel(holdrecord: h)}
      # product specific
      assert_equal 0, @@sv.process_hold(@@reason, opNo: @@opNo, route: @@route, product: @@product), 'error setting process hold'
      assert_equal 1, @@sv.process_hold_list(route: @@route).count
      assert_equal 0, @@sv.process_hold(@@reason, opNo: @@opNo, route: @@route, product: @@product2), 'error setting process hold'
      assert_equal 2, @@sv.process_hold_list(route: @@route).count
    end

    def test14_processhold_execute
      @@testlots.each {|lot| assert_equal 0, @@sv.lot_cleanup(lot)}
      #first lot at processhold operation
      assert_equal 0, @@sv.lot_opelocate(@@testlots[0], @@opNo), "error locationg #{@@testlots[0]}"
      # register process hold
      # Product specific
      assert_equal 0, @@sv.process_hold(@@reason, product: @@product, opNo: @@opNo, route: @@route, exechold: true), 'error setting process hold'
      assert_equal 1, @@sv.process_hold_list(route: @@route).count
      assert_equal 'ONHOLD', @@sv.lot_info(@@testlots[0]).status, "missing process hold for #{@@testlots[0]}"
      # execute process hold
      assert_equal 0, @@sv.lot_opelocate(@@testlots[1], @@opNo), "error locating #{@@testlots[1]}"
      assert_equal 'ONHOLD', @@sv.lot_info(@@testlots[1]).status, 'missing process hold'
      #
      assert_equal 0, @@sv.process_hold_cancel(holdreason: @@reason, product: @@product, opNo: @@opNo, route: @@route, exechold: true), 'error releasing process hold'
      @@testlots.each {|lot| assert_equal 'Waiting', @@sv.lot_info(lot).status, "wrong process hold for #{lot.inspect}"}
    end


    def XXtest15
      # "Add test for MSR 895553"
    end

    # MSR 949922
    def test16_multipsm_at_processhold
      lot = @@testlots.first
      assert_equal 0, @@sv.lot_cleanup(lot, opNo: @@opNo), 'error cleaning up lot'
      assert_equal 0, @@sv.lot_experiment(lot, [[1], [2]], @@psm_branches, @@opNo, @@psm_return_opNos)
      assert_equal 0, @@sv.process_hold(@@reason, opNo: @@opNo, route: @@route, exechold: true), 'error setting process hold'
      assert_equal 'ONHOLD', @@sv.lot_info(lot).status, "missing process hold for #{lot}"
      assert_equal 0, @@sv.lot_hold_release(lot, nil), 'error releasing process hold'
      @@sv.lot_family(lot).each {|l|
        assert_equal 'Waiting', @@sv.lot_info(l).status, 'wrong lot status'
      }
      assert @@sv.merge_lot_family(lot)
    end

    # MSR 664361
    def test17_multipsm_at_processhold
      lot = @@testlots.first
      assert @@sv.merge_lot_family(lot)
      assert_equal 0, @@sv.lot_cleanup(lot, opNo: @@opNo), 'error cleaning up lot'
      assert_equal 0, @@sv.lot_experiment(lot, [[1], [2]], @@psm_branches, @@opNo, @@psm_return_opNos)
      assert_equal 0, @@sv.process_hold(@@reason, opNo: @@opNo, route: @@route, exechold: true), 'error setting process hold'
      assert_equal 'ONHOLD', @@sv.lot_info(lot).status, "missing process hold for #{lot}"
      assert child = @@sv.lot_split(lot, [3], onhold: true, release: true), 'error splitting lot'
      assert (@@sv.lot_family(lot) - [child]).inject(true) {|res, lot|
        # res && verify_equal('Waiting', @@sv.lot_info(lot).status)
        stat = @@sv.lot_info(lot).status
        $log.warn "wrong status, expected: Waiting, got: #{stat}" if stat != 'Waiting'
        stat == 'Waiting'
      }, "wrong lot state for #{lot}"
      assert_equal 'ONHOLD', @@sv.lot_info(child).status, "missing process hold for #{child}"
      assert @@sv.merge_lot_family(lot)
    end

    # MSR351455
    def test20_inherit_hold
      lot = @@testlots.first
      assert_equal 0, @@sv.lot_cleanup(lot, opNo: @@opNo), 'error cleaning up lot'
      assert_equal 0, @@sv.process_hold(@@reason, opNo: @@opNo, route: @@route, exechold: true), 'error setting process hold'
      assert_equal 'ONHOLD', @@sv.lot_info(lot).status, "missing process hold for #{lot}"
      #
      child = @@sv.lot_split(lot, 2, onhold: true, inheritholds: true)
      if @@ph_movement == '1'
        assert child, "failed to split lot with hold record"
        assert_equal 'ONHOLD', @@sv.lot_info(child).status, "missing process hold for #{child}"
        assert_equal 0, @@sv.lot_hold_release(child, nil), 'error releasing process hold'
      else
        assert_nil child, 'split should not happen, because SP_PROCESSHOLD_ALLOW_LOTMOVEMENT=0'
        assert @@sv.msg_text.include?('This hold record cannot be inherited')
      end
      # with hold release must always pass
      assert lc = @@sv.lot_split(lot, 2, onhold: true, inheritholds: false)
      assert_empty @@sv.lot_hold_list(lc, reason: @@reason), 'wrong process hold'
      assert @@sv.merge_lot_family(lot)
    end

    # MSR351455
    def test21_partial_rework
      lot = @@testlots.first
      assert_equal 0, @@sv.lot_cleanup(lot, opNo: @@rework_opNo)
      assert_equal 0, @@sv.process_hold(@@reason, opNo: @@rework_opNo, route: @@route, exechold: true), 'error setting processhold'
      assert_equal 'ONHOLD', @@sv.lot_info(lot).status, "missing process hold for #{lot}"
      # partial rework without hold release
      lc = @@sv.lot_partial_rework(lot, 1, @@rework_route, return_opNo: @@rework_ret, onhold: true, inheritholds: true)
      if @@ph_movement == '1'
        refute_empty @@sv.lot_hold_list(lc, reason: @@reason)
        assert_equal 0, @@sv.lot_partial_rework_cancel(lot, lc)
      else
        assert_nil lc, 'split should not happen, because SP_PROCESSHOLD_ALLOW_LOTMOVEMENT=0'
        assert @@sv.msg_text.include?('This hold record cannot be inherited')
      end
      # partial rework with hold release
      assert lc = @@sv.lot_partial_rework(lot, 1, @@rework_route, return_opNo: @@rework_ret, onhold: true, inheritholds: false)
      assert_empty @@sv.lot_hold_list(lc, reason: @@reason)
      assert_equal 0, @@sv.lot_partial_rework_cancel(lot, lc)
    end

    # MSR 661541 (R15)
    def test31_nobankin
      lot = @@testlots.first
      assert_equal 0, @@sv.lot_cleanup(lot)
      # set processhold at last operation
      assert opNo = @@sv.lot_operation_list(lot).last.opNo
      assert_equal 0, @@sv.process_hold(@@reason, opNo: opNo, route: @@route)
      # locate lot and verify lot is onhold and not banked in
      assert_equal 0, @@sv.lot_opelocate(lot, opNo)
      li = @@sv.lot_info(lot)
      assert_equal 'ONHOLD', li.states['Lot Hold State'], "lot is not ONHOLD"
      assert_empty li.bank, "lot must not be banked in"
      # release hold, lot is banked in
      assert_equal 0, @@sv.lot_hold_release(lot, nil)
      assert_equal 'COMPLETED', @@sv.lot_info(lot).status, "lot must not be banked in"
      # (bankin lot and) split, with the bug SiView tries to apply the process hold to the child
      ##assert_equal 0, @@sv.lot_bankin(lot)
      assert @@sv.lot_split(lot, 2)
    end

end
