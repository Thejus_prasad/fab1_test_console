=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: Daniel Steger, 2014-12-05

History:
  2019-12-05 sfrieske, minor cleanup, renamed from Test920_CSBranchOperation
=end

require 'SiViewTestCase'

# Test CS_TxBranchOperationForLotInq, MSR851744
class MM_Lot_BranchOperation < SiViewTestCase
    @@opNo = '1000.600'
    @@rework_route = 'UTRW001.01'
    @@rework_last_opNo = '1000.400'
    @@rework_opNo = '1000.450'
    @@rework_return_opNo = '1000.300'
    @@psm_route = 'UTBR001.01'
    @@psm_split_opNo = '1000.200'
    @@psm_return_opNo = '1000.300'
    @@psm_route2 = 'UTBR002.01'
    @@psm_split2_opNo = '2000.400'
    @@psm_merge2_opNo = '2000.500'
    @@psm_dyn_opNo = '2000.100'         # operation with no branch or rework route defined
    @@psm_dyn_route = 'UTBR001D.01'
    @@rework_psm_route = 'UTRW001.01'
    @@psm_dyn_return_opNo = '2000.200'
    @@rework_dyn_route = 'UTRW001D.01'
    @@rework_dyn_opNo = '2000.100'
     
    
    def self.after_all_passed
      @@sv.delete_lot_family(@@lot)
    end


    def test00_setup
      $setup_ok = false
      #
      assert @@lot = @@svtest.new_lot, 'error creating test lot'
      @@route = @@svtest.route
      #
      $setup_ok = true
    end
    
    def test01_main_route
      $setup_ok = false
      #
      assert_equal 0, @@sv.lot_cleanup(@@lot, opNo: @@opNo), "failed to cleanup the lot"
      assert_equal [[@@route, @@opNo]], @@sv.lot_branch_ope(@@lot), "current route and operation expected"
      #
      $setup_ok = true
    end
    
    def test02_psm      
      assert_equal 0, @@sv.lot_cleanup(@@lot), "failed to cleanup the lot"
      routes = [[@@route, @@psm_split_opNo]] # list with expected child routes
      assert_equal 0, @@sv.lot_experiment(@@lot, 2, @@psm_route, @@psm_split_opNo, @@psm_return_opNo)      
      assert_equal 0, @@sv.lot_experiment(@@lot, 1, @@psm_route2, @@psm_split2_opNo, @@psm_split2_opNo, split_route: @@psm_route, original_opNo: @@psm_split_opNo, merge: @@psm_merge2_opNo)
      
      # execute PSMs
      assert_equal 0, @@sv.lot_opelocate(@@lot, @@psm_split_opNo)      
      assert clot1 = @@sv.lot_family(@@lot)[-1], "no child found for PSM"      
      assert_equal routes, @@sv.lot_branch_ope(@@lot), "current route and operation expected"
      #
      assert_equal 0, @@sv.lot_opelocate(clot1, @@psm_split2_opNo)
      routes << [@@psm_route, @@psm_split2_opNo]
      assert_equal routes, @@sv.lot_branch_ope(clot1), "psm route and operation expected"      
      assert clot2 = @@sv.lot_family(@@lot)[-1], "no child found for PSM2"
      assert_equal routes, @@sv.lot_branch_ope(clot1), "psm route and operation expected for #{clot1}"
      routes << [@@psm_route2, @@sv.lot_info(clot2).opNo]
      assert_equal routes, @@sv.lot_branch_ope(clot2), "psm route and operation expected for #{clot2}"
      #
      # cleanup
      [clot1,clot2].each {|l| assert_equal 0, @@sv.lot_opelocate(l, @@psm_merge2_opNo)}
      assert_equal 0, @@sv.lot_merge(clot1, clot2), "failed to merge #{clot1} with #{clot2}"
      [@@lot,clot1].each {|l| assert_equal 0, @@sv.lot_opelocate(l, @@psm_return_opNo)}
      assert_equal 0, @@sv.lot_merge(@@lot, clot1), "failed to merge #{@@lot} with #{clot1}"
    end
    
    def test03_dynamic_branch
      assert_equal 0, @@sv.lot_cleanup(@@lot), "failed to cleanup the lot"
      routes = [[@@route, @@psm_dyn_opNo]] # list with expected child routes
      assert_equal 0, @@sv.lot_experiment(@@lot, 2, @@psm_dyn_route, @@psm_dyn_opNo, @@psm_dyn_return_opNo, dynamic: true)      
      # execute PSM
      assert_equal 0, @@sv.lot_opelocate(@@lot, @@psm_dyn_opNo)      
      assert clot = @@sv.lot_family(@@lot)[-1], "no child found for PSM"      
      assert_equal routes, @@sv.lot_branch_ope(@@lot), "current route and operation expected"      
      routes << [@@psm_dyn_route, @@sv.lot_info(clot).opNo]
      assert_equal routes, @@sv.lot_branch_ope(clot), "psm route and operation expected for #{clot}"
      # do rework
      assert_equal 0, @@sv.lot_opelocate(clot, 1) # next operation for rework
      cur_op = @@sv.lot_info(clot).opNo
      assert_equal 0, @@sv.lot_rework(clot, @@rework_dyn_route, return_opNo: cur_op, dynamic: true), "failed to bring lot to rework on branch route"
      routes << [@@rework_dyn_route, @@sv.lot_info(clot).opNo]
      assert_equal routes, @@sv.lot_branch_ope(clot), "psm, rework route and operation expected for #{clot}"
      #
      # cleanup
      assert_equal 0, @@sv.lot_rework_cancel(clot), "failed to return lot from rework"      
      [@@lot, clot].each {|l| assert_equal 0, @@sv.lot_opelocate(l, @@psm_dyn_return_opNo, route: @@route)}
      assert_equal 0, @@sv.lot_merge(@@lot, clot), "failed to merge #{@@lot} with #{clot}"
    end
    
    def test04_rework
      assert_equal 0, @@sv.lot_cleanup(@@lot, opNo: @@rework_opNo), "failed to cleanup the lot"
      assert_equal 0, @@sv.lot_rework(@@lot, @@rework_route, return_opNo: @@rework_return_opNo), "failed to bring lot to rework"
      assert_equal [[@@route, @@rework_last_opNo], [@@rework_route, @@sv.lot_info(@@lot).opNo]], @@sv.lot_branch_ope(@@lot), "rework route expected for #{@@lot}"
      assert_equal 0, @@sv.lot_rework_cancel(@@lot), "failed to return lot from rework"
      assert_equal [[@@route, @@rework_opNo]], @@sv.lot_branch_ope(@@lot), "main route expected for #{@@lot}"
    end
    
    def test05_dynamic_rework
      assert_equal 0, @@sv.lot_cleanup(@@lot, opNo: @@rework_dyn_opNo), "failed to cleanup the lot"
      assert_equal 0, @@sv.lot_opelocate(@@lot, 1) # after rework definition
      cur_op = @@sv.lot_info(@@lot).opNo    
      assert_equal [[@@route, cur_op]], @@sv.lot_branch_ope(@@lot), "psm route and operation expected for #{@@lot}"
      assert_equal 0, @@sv.lot_rework(@@lot, @@rework_dyn_route, return_opNo: @@rework_dyn_opNo, dynamic: true), "failed to bring lot to rework"
      assert_equal [[@@route, @@rework_dyn_opNo], [@@rework_dyn_route, @@sv.lot_info(@@lot).opNo]], @@sv.lot_branch_ope(@@lot), "psm route and operation expected for #{@@lot}"
      assert_equal 0, @@sv.lot_rework_cancel(@@lot), "failed to return lot from rework"
    end

 end
 