=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: Daniel Steger, 2013-07-15

History:
  2016-02-05 sfrieske, cleanup
  2019-12-09 sfrieske, minor cleanup, renamed from Test046_UnloadInternalBuffer
  2020-08-24 sfrieske, minor cleanup
=end

require 'SiViewTestCase'


# Testclass for MSR574607 control use state
class MM_Eqp_UnloadIB < SiViewTestCase
  @@eqp = 'UTI006'
  @@port = 'P1'
  @@port_in = 'P3'
  @@port_out = 'P4'
  @@recycle_bank = 'UT-RECYCLE'
  @@startbank = 'UT-RAW'
  @@srcproduct = 'UT-RAW.00'

  @@testlots = []

  
  def self.after_all_passed
    @@sv.eqp_cleanup(@@eqp)
    @@testlots.each {|lot| @@sv.delete_lot_family(lot)}
  end


  def test00_setup
    $setup_ok = false
    #
    assert @@carrier = @@svtest.get_empty_carrier, 'no empty carrier found'
    assert vl = @@sv.vendorlot_receive(@@startbank, @@srcproduct, 25)
    assert @@vlot = @@sv.vendorlot_prepare(vl, @@carrier)
    @@testlots << @@vlot
    # create 4 monitor lots, rest of the vendor lot is still in the carrier
    @@lots = []
    4.times {
      assert lot = @@svtest.new_controllot('Equipment Monitor', nwafers: 5, srclot: @@vlot, bankin: true)
      @@testlots << lot
      @@lots << lot
    }
    @@endbank = @@sv.lot_info(@@lots.first).bank
    assert @@opNo = @@sv.lot_eqplist_in_route(@@lots.first, eqp: @@eqp).keys.first, "no operation to process lot #{@@lots.first} at #{@@eqp}"
    $log.info "using process opNo #{@@opNo.inspect}"
    # check bank for control use state or recycle
    @@banks = {'Recycle'=>@@recycle_bank}
    @@recycle = [@@recycle_bank]
    assert bankinfo = @@sv.bank_list
    assert brinfo = @@sv.eqp_info_raw(@@eqp, ib: true).equipmentBRInfoForInternalBuffer.controlBanks
    brinfo.each {|e|
      bank = e.controlBankID.identifier
      assert bi = bankinfo.find {|b| b.bankID.identifier == bank}, "failed to find bank #{bank}"
      @@recycle << bi.bankID.identifier if bi.recycleBankFlag  # add all banks with recycle set
      @@banks[e.controlLotType] = bank
    }
    @@recycle.uniq!
    $log.info "#{@@banks.inspect}"
    #
    assert @@svtest.cleanup_eqp(@@eqp, mode: 'Off-Line-1')
    #
    $setup_ok = true
  end

  def test10_npw_inoutport
    assert _run_npw_test(@@port)
  end

  def test11_npw_inport_outport
    assert _run_npw_test(@@port_in, @@port_out)
  end

  def test20_slr_inoutport
    assert _run_slr_test(@@port)
  end

  def test21_slr_inoutport
    assert _run_slr_test(@@port_in, @@port_out)
  end


  def _run_slr_test(loadport, unloadport=loadport)
    ok = true
    plot = @@lots.first  #_prepare_process_lot(@@banks.values.uniq + [@@startbank])
    assert_equal 0, @@sv.lot_cleanup(plot, opNo: @@opNo, endbank: @@endbank)
    @@lots[1..-1].each_with_index {|lot, i| assert_equal 0, @@sv.lot_bank_move(lot, @@banks.values.uniq[i])}
    assert_equal 0, @@sv.lot_bank_move(@@vlot, @@startbank)
    # SLR and load carrier
    assert @@sv.claim_process_prepare(plot, eqp: @@eqp, port: loadport)
    # check control use state
    @@testlots.each {|lot|
      next if lot == plot # ignore processing lot
      assert_equal 'InUse', @@sv.lots_info_raw(lot, ctrluse: true).strLotInfo.first.strLotControlUseInfo.controlUseState, "wrong control use state for #{lot}"
    }
    # process lot and unload carrier
    assert cj = @@sv.eqp_opestart(@@eqp, @@carrier)
    #assert_equal 0, @@sv.eqp_tempdata(@@eqp, p.pg, cj)
    assert_equal 0, @@sv.eqp_opecomp(@@eqp, cj)
    assert_equal 0, @@sv.eqp_unload(@@eqp, unloadport || loadport, @@carrier)
    # check control use state
    @@testlots.each {|lot|
      next if lot == plot # ignore processing lot
      li = @@sv.lot_info(lot)
      use_state = @@recycle.member?(li.bank) ? 'WaitRecycle' : 'WaitUse'
      assert_equal use_state, @@sv.lots_info_raw(lot, ctrluse: true).strLotInfo.first.strLotControlUseInfo.controlUseState, "wrong control use state for #{lot}"
    }
    return ok
  end

  def _run_npw_test(loadport, unloadport=loadport)
    ok = true
    # change carrier xfer status
    assert_equal 0, @@sv.carrier_xfer_status_change(@@carrier, 'EO', @@eqp)
    ['Filler Dummy Lot', 'Side Dummy Lot', 'Waiting Monitor Lot'].each {|purpose|
      $log.info "-- load purpose #{purpose.inspect}"
      # all lots in carrier need to go to bank
      @@testlots.each {|lot|
        li = @@sv.lot_info(lot)
        unless li.status == 'COMPLETED'
          @@sv.lot_opelocate(lot, :last)
          @@sv.lot_bankin(lot)
        end
        assert_equal 0, @@sv.lot_bank_move(lot, @@banks[purpose])
        assert_equal 0, @@sv.lot_ctrl_status_change(lot, 'WaitUse')
      }
      @@sv.npw_reserve(@@eqp, loadport, @@carrier, purpose: purpose)
      # load carrier
      assert_equal 0, @@sv.eqp_load(@@eqp, loadport, @@carrier, purpose: purpose), "error loading carrier with #{purpose}: #{@@sv.msg_text}"
      if @@sv.rc == 0
        @@testlots.each {|lot|
          assert_equal 'InUse', @@sv.lots_info_raw(lot, ctrluse: true).strLotInfo.first.strLotControlUseInfo.controlUseState, "wrong control use state for #{lot}"
        }
        assert_equal 0, @@sv.eqp_unload(@@eqp, unloadport, @@carrier)
        @@testlots.each {|lot|
          use_state = @@recycle.member?(@@banks[purpose]) ? 'WaitRecycle' : 'WaitUse'
          assert_equal use_state, @@sv.lots_info_raw(lot, ctrluse: true).strLotInfo.first.strLotControlUseInfo.controlUseState, "wrong control use state for #{lot}"
        }
      else
        # cleanup
        assert_equal 0, @@sv.npw_reserve_cancel(@@eqp, @@carrier)
      end
    }
    return ok
  end
end
