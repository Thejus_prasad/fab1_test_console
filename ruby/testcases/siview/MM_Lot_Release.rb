=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: Daniel Steger, 2016-02-27

History:
  2016-05-20 sfrieske, added test11 for MES-2546
  2018-05-19 sfrieske, moved test06 for MES-3692 here (from a separate test)
  2019-04-18 sfrieske, minor cleanup
  2019-12-05 sfrieske, minor cleanup, renamed from Test005_NewLotRelease
  2021-01-15 sfrieske, moved SiView::MM#new_srclot_for_product to SiView::Test#new_srclot
=end

require 'SiViewTestCase'


# Release new lots
class MM_Lot_Release < SiViewTestCase
  @@customer = 'ibm'
  @@customer2 = 'ibmca'
  @@sublottype = 'QX'
  @@sublottype2 = 'PO'
  @@product2 = 'UT-PRODUCT002.01'
  @@route2 = 'UTRT002.01'

  @@testlots = []


  def self.after_all_passed
    @@testlots.each {|lot| @@sv.delete_lot_family(lot)}
  end


  def test01_sublottype
    assert srclot = @@svtest.new_srclot, 'error creating srclot'
    @@testlots << srclot
    assert lot = @@sv.new_lot_release(product: @@svtest.product, sublottype: @@sublottype, srclot: srclot), 'error releasing new lot'
    @@testlots << lot
    assert_equal 0, @@sv.new_lot_release_update(lot, sublottype: @@sublottype2), "failed to update lot request"
    assert @@sv.stb(lot), "failed to STB lot"
    assert_equal @@sublottype2, @@sv.lot_info(lot).sublottype
  end

  def test02_product_route_comment
    assert srclot = @@svtest.new_srclot, 'error creating srclot'
    @@testlots << srclot
    comment = "ERPITEM=ENGINEERING-U00,DPML=1.5,PLANNED_FSD=#{Time.now.strftime('%F')}"
    assert lot = @@sv.new_lot_release(product: @@svtest.product, srclot: srclot), 'error releasing new lot'
    @@testlots << lot
    comment2 = "ERPITEM=ENG-J001,DPML=1.5,PLANNED_FSD=#{Time.now.strftime('%F')}"
    assert_equal 0, @@sv.new_lot_release_update(lot, sublottype: @@sublottype, product: @@product2, route: @@route2,
      comment: comment2
    ), "failed to update lot request"
    assert @@sv.stb(lot), "failed to STB lot"
    linfo = @@sv.lot_info(lot)
    assert_equal @@sublottype, linfo.sublottype
    assert_equal @@product2, linfo.product
    assert_equal @@route2, linfo.route
    assert @@sv.lot_comments(lot).find { |c| c.desc == comment2}, "lot comment was not updated"
  end

  def test03_planned_start_time
    assert srclot = @@svtest.new_srclot, 'error creating srclot'
    @@testlots << srclot
    t1 = Time.now + 86400
    assert lot = @@sv.new_lot_release(product: @@svtest.product, starttime: t1, srclot: srclot), 'error releasing new lot'
    @@testlots << lot
    t2 = t1 + 86400 + 3661
    assert_equal 0, @@sv.new_lot_release_update(lot, starttime: t2), "failed to update lot request"
    assert prq = @@sv.product_request(lot), "error getting product request for #{lot}"
    assert_equal @@sv.siview_timestamp(t2), prq.releaseTimeStamp, 'wrong planned start time'
    assert @@sv.stb(lot), "failed to STB lot"
  end

  def test04_customer
    assert srclot = @@svtest.new_srclot, 'error creating srclot'
    @@testlots << srclot
    assert lot = @@sv.new_lot_release(product: @@svtest.product, customer: @@customer, sublottype: @@sublottype, srclot: srclot), 'error releasing new lot'
    @@testlots << lot
    assert_equal 0, @@sv.new_lot_release_update(lot, customer: @@customer2, sublottype: @@sublottype), "failed to update lot request"
    assert @@sv.stb(lot), "failed to STB lot"
    linfo = @@sv.lot_info(lot)
    assert_equal @@sublottype, linfo.sublottype
    assert_equal @@customer2, linfo.customer
  end

  # fails for not existing customer in C7.7.x, MES-3714​ and MES-3692
  def test06_customer_notexisting
    # wrong customer, test must not abort due to MM server crash
    assert lot = @@sv.generate_new_lotids('gf').first
    @@testlots << lot
    assert_nil @@sv.new_lot_release(lot: lot, product: @@svtest.product, customer: 'QAQA')
    assert_equal 1431, @@sv.rc, 'wrong RC'
    # empty customer, MES-3692
    assert lot = @@sv.new_lot_release(lot: lot, product: @@svtest.product, customer: '')
    @@testlots << lot
  end

  def test11_multiple_srclots  # MES-2546
    assert srclot = @@svtest.new_srclot, 'error creating srclot'
    @@testlots << srclot
    assert vchild = @@sv.lot_split_notonroute(srclot, 5)
    assert lot = @@sv.new_lot_release(product: @@svtest.product, srclot: [srclot, vchild]), 'error releasing new lot'
    @@testlots << lot
    srclots = @@sv.product_request(lot).strSourceLotsAttributes.collect {|aa| aa.lotID.identifier}
    assert_equal [srclot, vchild], srclots, 'wrong srclots'
  end

end
