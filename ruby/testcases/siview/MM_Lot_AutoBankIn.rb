=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: Steffen Steidten, 2010-05-03

History:
  2014-10-30 ssteidte,  merge test cases for MSR145574 (lot_info)
  2017-07-18 sfrieske, added test for MES-3599 (autobankin after merge)
  2019-12-09 sfrieske, minor cleanup, renamed from Test310_AutoBankIn
=end

require 'SiViewTestCase'


# Test AutoBankIn works for OpeComp, OpeLocate and GatePass
# and lot list with 'bank in required flag' does not show the lot (MSR 145574)
class MM_Lot_AutoBankIn < SiViewTestCase

  def self.after_all_passed
    @@testlots.each {|lot| @@sv.delete_lot_family(lot)}
  end


  def test00_setup
    $setup_ok = false
    #
    @@testlots = []
    assert @@lot = @@svtest.new_lot, "error creating test lot"
    @@testlots << @@lot
    #
    rops = @@sv.route_operations(@@svtest.route).to_a
    @@opNo_prev = rops[-2].operationNumber
    @@opNo_last = rops[-1].operationNumber
    #
    $setup_ok = true
  end

  def test11_opelocate
    assert_equal 0, @@sv.lot_cleanup(@@lot, opNo: @@opNo_prev)
    assert_equal 0, @@sv.lot_opelocate(@@lot, 1), "error in opelocate"
    check_banked_in
  end

  def test12_gatepass
    assert_equal 0, @@sv.lot_cleanup(@@lot, opNo: @@opNo_prev)
    assert_equal 0, @@sv.lot_gatepass(@@lot), "error in gatepass"
    check_banked_in
  end

  def test13_opecomp
    assert_equal 0, @@sv.lot_cleanup(@@lot, opNo: @@opNo_prev)
    assert @@sv.claim_process_lot(@@lot), "error processing lot"
    check_banked_in
  end

  # MES-3599, fails in C7.6, to be resolved in C7.9
  def test21_merge
    assert_equal 0, @@sv.lot_cleanup(@@lot, opNo: @@opNo_prev)
    assert lc = @@sv.lot_split(@@lot, 2, mergeop: @@opNo_last), "error splitting lot"
    [@@lot, lc].each {|l| assert_equal 0, @@sv.lot_opelocate(l, @@opNo_last)}
    assert_equal 0, @@sv.lot_merge(@@lot, lc), "error merging lot"
    check_banked_in
  end


  # check lot has been banked in by the current test user, not jCAP
  def check_banked_in
    li = @@sv.lot_info(@@lot)
    assert_equal "InBank", li.states['Lot Inventory State'], "lot #{@@lot} not banked in"
    assert_equal @@sv.user, li.user, "lot #{@@lot} has been banked in by user '#{li.user}' instead of '#{@@sv.user}'"
    assert_empty @@sv.lot_list(lot: @@lot, bankin: true), "lot #{@@lot} not expected in filtered lot list"
  end

end
