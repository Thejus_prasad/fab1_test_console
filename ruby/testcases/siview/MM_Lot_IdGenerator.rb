=begin

(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.


Author: Conny Stenzel, 2010-12-06

History:
  2012-11-30 ssteidte, moved LIG logic into a separate library
  2015-07-22 dsteger,  added customers for F7 and R15 settings
  2016-04-07 sfrieske, testcase for AMD-SEC
  2019-04-08 sfrieske, use customer qaamd instead of amd
  2019-12-06 sfrieske, renamed from Test_LIG
=end

require 'siview/lig'
require 'RubyTestCase'


# Test customer lot ID generation, MSRs 424566, 429732, 808124, 1017528, see spec C08-00000171
class MM_Lot_IdGenerator < RubyTestCase
  @@customers = ['gf', 'qaamd', 'samsung']  # 'ibm'
  @@customer_sec = 'amd-secqa'
  @@sublottypes = nil  # ['EO', 'PO'] for Fab7
  @@carriers = 'LIG%'
  @@bank = 'UT-RAW'
  @@product = 'UT-PRODUCT000.01'
  @@vendor_product = 'UT-RAW.00'
  @@tw_product = 'UT-MN-UTC001.01'
  @@tw_lottype = 'Equipment Monitor'
  @@tw_srcproduct = 'UT-RAW.00'
  @@tw_startbank = 'UT-RAW'


  def self.after_all_passed
    carriers = @@sv.carrier_list(status: 'AVAILABLE', notempty: true, carriers: @@carriers)
    carriers.each {|c| @@sv.carrier_status(c).lots.each {|lot| @@sv.delete_lot_family(lot)}}
    carriers = @@sv.carrier_list(status: 'AVAILABLE', empty: true, carriers: @@carriers)
    carriers.each {|c| @@sv.carrier_status_change(c, 'NOTAVAILABLE')}
  end


  def test00_setup
    $setup_ok = false
    #
    assert_equal 'IO', @@sv.environment_variable[1]['CS_SP_INHIBIT_CHARACTERS'], 'wrong MM server environment' unless @@sv.f7
    carriers = @@sv.carrier_list(status: 'NOTAVAILABLE', empty: true, carriers: @@carriers, count: 51)
    carriers.each {|c| @@sv.carrier_status_change(c, 'AVAILABLE')}
    carriers = @@sv.carrier_list(status: 'AVAILABLE', empty: true, carriers: @@carriers)
    assert carriers.size > 50, "not enough carriers, need 50 empty carriers #{@@carriers.inspect}"
    #
    $setup_ok = true
  end

  def test11
    @@results = {}
    test_ok = true
    @@customers.each {|customer|
      c = LIG::Customer.new($env, customer: customer, product: @@product, carriers: @@carriers)
      if @@sublottypes
        @@sublottypes.each {|slt|
          @@results["#{customer}_#{slt}"] = c.verify_lig(sublottype: slt)
          test_ok &= @@results["#{customer}_#{slt}"]
        }
      else
        @@results[customer] = c.verify_lig
        test_ok &= @@results[customer]
      end
      c.delete_lots
      assert test_ok, 'test failed'
    }
    $log.info "test results\n#{@@results.pretty_inspect}"
    assert test_ok, 'failed to verify lot creation'
  end

  def test12_vendorlot
    if @@sv.f7
      customer = @@sv.environment_variable[1]['CS_SP_CUSTOMER_FOR_VENDOR_LOT']
      settings = LIG::CustomerSettingsF7[customer]
    else
      settings = LIG::VendorLotSettings
    end
    c = LIG::VendorLot.new($env, @@bank, product: @@vendor_product, carriers: @@carriers, settings: settings)
    assert c.verify_lig, 'failed to verify vendor lot creation'
    #
    c.delete_lots
  end

  def test13_ctrllot
    if @@sv.f7
      customer = @@sv.environment_variable[1]['CS_SP_CUSTOMER_FOR_CTRL_LOT']
      settings = LIG::CustomerSettingsF7[customer]
    else
      settings = LIG::ControlLotSettings
    end
    c = LIG::ControlLot.new($env, @@tw_lottype, product: @@tw_product, 
      srcproduct: @@tw_srcproduct, startbank: @@tw_startbank, carriers: @@carriers, settings: settings)
    assert c.verify_lig, 'failed to verify control lot creation'
    #
    c.delete_lots
  end

  def test21_sec
    skip "no SEC customer configured" unless @@customer_sec
    # regular LIG test
    c = LIG::Customer.new($env, customer: @@customer_sec, product: @@product, carriers: @@carriers)
    assert c.verify_lig(cleanup: false), 'failed to verify SEC lot creation'
    # try to merge 2 parents
    lot1 = c.generated_lots[1]
    lot2 = c.generated_lots[2]
    assert_equal 0, @@sv.wafer_sort(lot2, @@sv.lot_info(lot1).carrier, slots: 'empty')
    assert_equal 1203, @@sv.lot_merge(lot1, lot2), 'wrong merge'
    #
    c.delete_lots
  end

  def test22_sec_split_onhold # MES-3132  C7.4.05, can be appended to test21 once fixed
    skip "no SEC customer configured" unless @@customer_sec
    c = LIG::Customer.new($env, customer: @@customer_sec, product: @@product, carriers: @@carriers)
    assert c.verify_udata_calendar, 'wrong SM UDATA LIG_CalendarID'
    # create test lots
    assert lots = c.generate_lotids(parent_suffix: '.98', nlots: 3)
    # normal split
    assert c.verify_lot_creation(c.generated_lots[0], parent_suffix: '.98', children: 3)
    # split onhold with and without release
    assert c.verify_lot_creation(c.generated_lots[1], parent_suffix: '.98', children: 3, onhold: true)
    assert c.verify_lot_creation(c.generated_lots[2], parent_suffix: '.98', children: 3, onhold: true, release: true)
    #
    c.delete_lots
  end
  
end
