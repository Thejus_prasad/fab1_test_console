=begin 
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.
    
Author: ssteidte, 2012-05-31

History:
  2013-04-08 dsteger,  fixed query if less then max number of reticles
  2015-05-28 adarwais, cleaned up and verified test cases with R15 
  2019-12-09 sfrieske, minor cleanup, renamed from Test114_ReticleList
=end

require 'RubyTestCase'


# Test Tx AMD_TxAllReticleListInq and TxReticleListInq, MSR 550160
class MM_Reticle_List < RubyTestCase    
    @@rg = 'UTRETICLEGROUP4'
    @@eqp = 'UTR001'
    @@max_reticles = 2200   # as configured in MM env settings
    @@status = %w(AVAILABLE NOTAVAILABLE SCRAPPED INUSE)
    

    def test01_all_customized
      rl = @@sv.reticle_list(core: false)
      assert_equal @@max_reticles, [rl.count, @@max_reticles].min, "wrong number of reticles returned, #{rl.size} instead of #{@@max_reticles}"
      assert check_reticles(rl)
    end
    
    def test02_reticle_group
      rlcore = @@sv.reticle_list(core: true, rg: @@rg)
      rl = @@sv.reticle_list(core: false, rg: @@rg)
      assert_equal rlcore.size, rl.size, "different list size"
      assert check_reticles(rl)
    end
    
    def test03_eqp
      rlcore = @@sv.reticle_list(core: true, eqp: @@eqp)
      rl = @@sv.reticle_list(core: false, eqp: @@eqp)
      assert_equal rlcore.size, rl.size, "different list size"
      assert check_reticles(rl)
    end
    
    def test04_status
      @@status.each {|s|
        $log.info "verify for status #{s}"
        rlcore = @@sv.reticle_list(core: true, status: s)
        rl = (@@sv.reticle_list(core: false, status: s) or [])
        assert_equal rlcore.size, rl.size, "different list size"
        assert check_reticles(rl)
      }
    end
    
    def check_reticles(rl)
      # compare obtained list rl with reference list
      # due to size limitations, only reticle parameters with matching ids are compared,
      # there is no complaint if a reticle is not found in rl
      $log.info "check_reticles (#{rl.size})"
      test_ok = true
      rl.each_with_index {|ri, i|
        $log.debug "  #{i+1}/#{rl.size}"
        r = ri.reticleID.identifier
        # skip reticles with strange characters, not handled correctly
        next unless r.ascii_only?
        # find record, because reticle_list returns all reticles _starting_ with r, not just r 
        ref = @@sv.reticle_list(core: true, reticle: r).find {|e| e.reticleID.identifier == r}
        if ref.nil?
          $log.warn "  no core reticle info for #{r.inspect}"
          test_ok = false
        else
          # ref.members.each {|m|
          #   ($log.warn "  wrong info: #{ri.inspect} instead of #{ref.inspect}"; test_ok = false) if ri[m] != ref[m]
          # }
        end
      }
      return test_ok
    end
end
