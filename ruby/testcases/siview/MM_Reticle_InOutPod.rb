=begin
(c) Copyright 2013 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: Daniel Steger, 2013-04-04

History:
  2015-06-25 ssteidte, cleaned up and verified with R15
  2019-12-09 sfrieske, minor cleanup, renamed from Test123_ReticleInOutPod
  2020-08-26 sfrieske, minor cleanup
=end


require 'SiViewTestCase'

# Test reticle move into and out of a pod even when pod is unavailable, MSR316963
class MM_Reticle_InOutPod < SiViewTestCase
  @@reticle = 'UTRETICLE0003'
  @@rpod = 'R29N'
  @@eqp = 'UTR001'
  @@rport = 'RP1'


  def setup
    pinfo = @@sv.reticle_pod_status(@@rpod).reticlePodStatusInfo
    assert_equal 0, @@sv.reticle_pod_unload(@@rpod, eqp: pinfo.equipmentID.identifier, port: @@rport) if pinfo.transferStatus == 'EI'
    assert_equal 0, @@sv.reticle_pod_multistatus_change(@@rpod, 'AVAILABLE') if pinfo.reticlePodStatus != 'AVAILABLE'
    pod = @@sv.reticle_status(@@reticle).reticleStatusInfo.reticlePodID.identifier
    assert_equal 0, @@sv.reticle_justinout(@@reticle, pod, 1, 'out') if pod != ''
  end


  def test00_setup
    $setup_ok = false
    #
    assert_equal '1', @@sv.environment_variable[1]['CS_SP_RTCL_MOVE_NOTAVAILABLE_POD'], "wrong MM server env"
    assert @@svtest.cleanup_eqp(@@eqp, mode: 'Auto-1'), "cannot switch eqp mode"
    assert_equal 0, @@sv.reticle_inventory_upload(eqp: @@eqp)
    assert_equal 0, @@sv.eqp_mode_change(@@eqp, 'Off-Line-1'), "cannot switch eqp mode"
    @@sv.eqp_info_raw(@@eqp).strEquipmentAdditionalReticleAttribute.strReticlePodPortIDs.each {|rp|
      pod = rp.loadedReticlePodID.identifier
      assert_equal 0, @@sv.reticle_pod_offline_unload(@@eqp, rp.reticlePodPortID.identifier, pod) unless pod.empty?
    }
    pinfo = @@sv.reticle_pod_status(@@rpod).reticlePodStatusInfo
    assert_equal 'EO', pinfo.transferStatus, "wrong reticle pod xfer status"
    assert_equal 'EO', @@sv.reticle_status(@@reticle).reticleStatusInfo.transferStatus, "wrong reticle xfer status"
    #
    $setup_ok = true
  end


  def test01_justin_out
    assert_equal 0, @@sv.reticle_pod_multistatus_change(@@rpod, 'AVAILABLE', check: true)
    ["NOTAVAILABLE", "AVAILABLE", "INUSE"].each {|status|
      substate = (status == 'NOTAVAILABLE') ? 'DIRTY' : ''
      assert_equal 0, @@sv.reticle_pod_multistatus_change(@@rpod, status, substate: substate), "Failed to change status"
      assert_equal 0, @@sv.reticle_justinout(@@reticle, @@rpod, 1, "in")
      assert_equal 0, @@sv.reticle_justinout(@@reticle, @@rpod, 1, "out")
    }
    assert_equal 0, @@sv.reticle_pod_multistatus_change(@@rpod, "SCRAPPED"), "Failed to change status"
    assert_equal 2940, @@sv.reticle_justinout(@@reticle, @@rpod, 1, "in")
  end

  def test02_store_retrieve_offline
    assert_equal 0, @@sv.reticle_pod_multistatus_change(@@rpod, 'AVAILABLE', check: true)
    assert_equal 0, @@sv.reticle_justinout(@@reticle, @@rpod, 1, "in"), "Cannot move reticle to pod"
    assert_equal 0, @@sv.eqp_mode_change(@@eqp, 'Off-Line-1'), "can't switch #{@@eqp} to offline-1"
    assert_equal 0, @@sv.reticle_pod_offline_load(@@eqp, @@rport, @@rpod)
    ["NOTAVAILABLE", "AVAILABLE", "INUSE"].each {|status|
      substate = (status == 'NOTAVAILABLE') ? 'DIRTY' : ''
      assert_equal 0, @@sv.reticle_pod_multistatus_change(@@rpod, status, substate: substate), "Failed to change status"
      assert_equal 0, @@sv.reticle_offline_store(@@eqp, @@rport, @@rpod, @@reticle)
      assert_equal 0, @@sv.reticle_offline_retrieve(@@eqp, @@rport, @@rpod, [@@reticle])
    }
    assert_equal 0, @@sv.reticle_pod_offline_unload(@@eqp, @@rport, @@rpod)
  end

  def test03_store_retrieve_online
    assert_equal 0, @@sv.reticle_pod_multistatus_change(@@rpod, 'AVAILABLE', check: true)
    assert_equal 0, @@sv.reticle_justinout(@@reticle, @@rpod, 1, "in"), "Cannot move reticle to pod"
    assert_equal 0, @@sv.eqp_mode_change(@@eqp, 'Auto-1'), "can't switch #{@@eqp} to auto-1"
    assert_equal 0, @@sv.reticle_pod_load(@@rpod, eqp: @@eqp, port: @@rport)
    assert_equal 0, @@sv.eqp_rsp_port_status_change(@@eqp, @@rport, "LoadComp")
    ["NOTAVAILABLE", "AVAILABLE", "INUSE"].each {|status|
      substate = (status == 'NOTAVAILABLE') ? 'DIRTY' : ''
      assert_equal 0, @@sv.reticle_pod_multistatus_change(@@rpod, status, substate: substate), "Failed to change status"
      assert_equal 0, @@sv.reticle_store(@@eqp, @@rport, @@rpod, @@reticle)
      assert_equal 0, @@sv.reticle_retrieve(@@rpod, @@reticle, eqp: @@eqp, port: @@rport)
    }
    assert_equal 0, @@sv.reticle_pod_unload(@@rpod, eqp: @@eqp, port: @@rport)
    assert_equal 0, @@sv.eqp_rsp_port_status_change(@@eqp, @@rport, "LoadReq")
  end

end
