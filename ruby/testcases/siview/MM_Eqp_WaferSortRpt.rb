=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: Steffen Steidten, 2010-05-03

History:
  2018-05-31 sfrieske, fixed call to claim_process_lot
  2019-12-05 sfrieske, minor cleanup, renamed from Test810_WaferSortRpt
=end

require 'SiViewTestCase'


# Test Virtual Randomization for Measurement, Process and Internal Buffer Tools, MSR 358297 (not fixed in core R10.0.1)
class MM_Eqp_WaferSortRpt < SiViewTestCase
  @@pd_proc = '1000.500'
  @@eqp_proc = 'UTF002'

  @@pd_meas = '1000.600'
  @@eqp_meas = 'UTF001'

  @@pd_ib = '1000.200'
  @@eqp_ib = 'UTI002'

  @@pd_sorter = '1000.930'
  @@eqp_sorter = 'UTS001'


  def self.after_all_passed
    @@sv.delete_lot_family(@@lot)
  end


  def test00_setup
    $setup_ok = false
    #
    assert @@lot = @@svtest.new_lot, 'error creating test lot'
    #
    $setup_ok = true
  end
  
  def test11_process
    process_lot_with_wafer_sort_rpt(@@lot, @@pd_proc, @@eqp_proc, 'Process')
  end

  def test12_measurement
    process_lot_with_wafer_sort_rpt(@@lot, @@pd_meas, @@eqp_meas, 'Measurement')
  end

  def test13_ib
    process_lot_with_wafer_sort_rpt(@@lot, @@pd_ib, @@eqp_ib, 'Internal Buffer')
  end

  def test14_sorter
    process_lot_with_wafer_sort_rpt(@@lot, @@pd_sorter, @@eqp_sorter, 'Wafer Sorter')
  end


  # aux methods

  def process_lot_with_wafer_sort_rpt(lot, opNo, eqp, category)
    # prepare eqp
    assert_equal category, @@sv.eqp_info(eqp).category, "#{eqp} has wrong tool category"
    assert_equal 0, @@sv.eqp_cleanup(eqp)
    # prepare lot
    assert_equal 0, @@sv.lot_cleanup(lot, opNo: opNo)
    li_orig = @@sv.lot_info(lot, wafers: true)
    # process lot with wafer sort rpt
    assert @@sv.claim_process_lot(lot, eqp: eqp) {
      @@sv.wafer_sort_rpt(lot, li_orig.carrier, eqp: eqp, slots: (1..25).to_a.reverse)
    }, "error processing lot #{lot} on #{eqp} with wafer_sort_rpt"
    #
    # compare wafer positions
    sorted = true
    @@sv.lot_info(lot, wafers: true).wafers.each_with_index {|w, i|
      wo = li_orig.wafers[25-1-i]
      sorted &= ((w.alias == wo.alias) && (w.wafer == wo.wafer) && (w.slot == li_orig.wafers[i].slot))
    }
    assert sorted, "wafers have not been sorted correctly on #{eqp}"
  end

end