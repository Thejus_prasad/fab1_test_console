=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: Steffen Steidten, 2011-02-06

History:
  2015-05-27 adarwais, corrected shutdown method: replaced @@Carriers by either @@real_carriers or @@virtual_carriers
  2015-09-17 dsteger,  reworked transport job validation and added MES-2358 (Fab1 logic)
  2015-10-15 sfrieske, added more frommachine validations, minor cleanup
  2020-03-02 sfrieske, minor cleanup
  2021-09-15 sfrieske, finally replaced $svtest by @@svtest
=end

require 'SiViewTestCase'


# Test XM carrier transport.
# MSR367795, MSR368289, MSR35942, MSR437089 (tomachine after Transport Jobs Status query)
class XM_Regression < SiViewTestCase
  # real AMHS
  @@real_carriers = ['A3C8', 'A4MN'] # ['A315', 'B316']
  @@real_stockers = ['STO110', 'STO160']
  @@real_stocker3 = 'ZFG1010'
  @@real_interbay = "OHT01"
  @@real_tools = ['MES500', 'AWS001']
  @@real_ports = ['P1', 'P2']
  @@real_timeout = 600
  # emulated AMHS
  @@virtual_carriers = ['UX01', 'UX02']
  @@virtual_stockers = ['UTSTO11', 'UTSTO12']
  @@virtual_stocker3 = 'UTSTO13'
  @@virtual_interbay = 'INTER1'
  @@virtual_tools = ['UTF001', 'UTF002']
  @@virtual_ports = ['P2', 'P1']
  @@virtual_timeout = 180

  @@eqpmode = 'Off-Line-2'
  @@use_emulators = true
  @@run_manual_tests = false


  def self.shutdown
    [@@carrier1, @@carrier2].each {|c| @@svtest.cleanup_carrier(c)}
    [@@eqp1, @@eqp2].each {|eqp| @@sv.eqp_cleanup(eqp, mode: @@eqpmode)}
    $dummyamhs.set_variable('intrabaytime', 15000)
  end


  def teardown
    if @@use_emulators
      $dummyamhs.set_variable('e84mode', false) # deactivate E84 failure mode
    end
  end


  def test00_setup
    $setup_ok = false
    #
    assert_equal '0', @@sv.environment_variable[1]['CS_SP_SINGLEXFER_NO_FROM_MACHINEID_FOR_STK'] unless @@sv.f7
    if @@use_emulators
      @@carrier1, @@carrier2 = @@virtual_carriers
      @@stocker1, @@stocker2 = @@virtual_stockers
      @@interbay = @@virtual_interbay
      @@stocker3 = @@virtual_stocker3
      @@eqp1, @@eqp2 = @@virtual_tools
      @@port1, @@port2 = @@virtual_ports
      @@timeout = @@virtual_timeout
      [@@eqp1, @@eqp2].each {|eqp| assert @@svtest.cleanup_eqp(eqp, mode: @@eqpmode)}
      $dummyamhs = Dummy::AMHS.new($env)
      assert $dummyamhs.register
      assert $dummyamhs.set_variable('intrabaytime', 45000)
      assert $dummyamhs.set_variable('stockintime', 1000)
      assert $dummyamhs.set_variable('stockouttime', 1000)
    else
      @@carrier1, @@carrier2 = @@real_carriers
      @@stocker1, @@stocker2 = @@real_stockers
      @@interbay = @@real_interbay
      @@stocker3 = @@real_stocker3
      @@eqp1, @@eqp2 = @@real_tools
      @@port1, @@port2 = @@real_ports
      @@timeout = @@real_timeout
    end
    $log.info "use_emulators: #{@@use_emulators}"
    #
    $setup_ok = true
  end


  def test11_invalid_carrier
    assert_equal 1424, @@sv.carrier_xfer('INVALID', @@stocker1, '', @@stocker2, '')
  end

  def test12_single_xfer
    prepare_resources
    @@sv.carrier_xfer(@@carrier1, @@stocker1, '', @@stocker2, '')
    assert verify_carrier_xfer_job(@@carrier1, @@stocker1, @@stocker2, '', true)
    # verify again with mcs=false for MES-2358 (C7.1.1)
    assert verify_carrier_xfer_job(@@carrier1, @@stocker1, @@stocker2)
    # check that carrier is transported to stations and xfer state is correctly updated
    {@@stocker1=>/MI|SI/, @@interbay=>/SO/, @@stocker2=>/SI/}.each_pair {|station, xfer_state|
      assert wait_for(timeout: @@timeout) {
        cstat = @@sv.carrier_status(@@carrier1)
        jobs = @@sv.carrier_xferjob(@@carrier1)
        jobs.empty? || (station == cstat.stocker && cstat.xfer_status =~ xfer_state)
      }, "#{@@carrier1} not in expected state #{xfer_state}"
      $log.info "  verified xfer state of #{@@carrier1}: #{xfer_state.inspect} #{station.inspect}"
    }
    assert @@sv.wait_carrier_xfer(@@carrier1, timeout: @@timeout)
    verify_carrier_position(@@carrier1, @@stocker2)
  end

  def test13_single_xfer_src_dst
    prepare_resources
    # DummyAMHS accepts, MACS rejects
    res = @@sv.carrier_xfer(@@carrier1, @@stocker1, '', @@stocker1, '')
    assert [0, 4301].member?(res), 'AMHS must either create a job or reject with code 4301'
    assert @@sv.wait_carrier_xfer(@@carrier1, timeout: @@timeout)
    verify_carrier_position(@@carrier1, @@stocker1)
  end

  def test14_carrier_e84_reroute
    skip "skipping re-route test with real AMHS" unless @@use_emulators
    prepare_resources
    assert $dummyamhs.set_variable('e84mode', true) # Activate E84 failure mode
    fallback_stocker = $dummyamhs.variable('fallbackstocker')
    @@sv.carrier_xfer(@@carrier1, @@stocker1, '', @@eqp1, @@port1)
    assert verify_carrier_xfer_job(@@carrier1, @@stocker1, @@eqp1, @@port1), 'wrong xfer to eqp'
    assert wait_for(timeout: @@timeout) {
      @@sv.carrier_xferjob_detail(@@carrier1).first.strCarrierJobResult.first.toMachine.identifier == fallback_stocker
    }, 'reroute to stocker failed'
    verify_carrier_xfer_job(@@carrier1, @@stocker1, fallback_stocker, '', true)
    verify_carrier_xfer_job(@@carrier1, @@stocker1, fallback_stocker)
    assert @@sv.wait_carrier_xfer(@@carrier1, timeout: @@timeout)
    verify_carrier_position(@@carrier1, fallback_stocker)
  end

  def test15_xfer_to_eqp_to_stocker
    prepare_resources
    # xfer to eqp
    @@sv.carrier_xfer(@@carrier1, @@stocker1, '', @@eqp1, @@port1, multicarrier: true)
    assert verify_carrier_xfer_job(@@carrier1, @@stocker1, @@eqp1, @@port1, true)
    assert verify_carrier_xfer_job(@@carrier1, @@stocker1, @@eqp1, @@port1)
    assert @@sv.wait_carrier_xfer(@@carrier1, timeout: @@timeout)
    # return foup to stocker
    @@sv.carrier_xfer(@@carrier1, @@eqp1, @@port1, @@stocker1, '')
    assert verify_carrier_xfer_job(@@carrier1, @@eqp1, @@stocker1, '', true)
    assert verify_carrier_xfer_job(@@carrier1, @@eqp1, @@stocker1)
    assert @@sv.wait_carrier_xfer(@@carrier1, timeout: @@timeout)
    verify_carrier_position(@@carrier1, @@stocker1)
  end

  def test21_single_xfer_delete
    prepare_resources
    @@sv.carrier_xfer(@@carrier1, @@stocker1, '', @@eqp1, @@port1)
    assert verify_carrier_xfer_job(@@carrier1, @@stocker1, @@eqp1, @@port1)
    # delete job
    sleep 1
    assert_equal 0, @@sv.carrier_xferjob_delete(@@carrier1)
    assert_empty @@sv.carrier_xferjob(@@carrier1), "lingering transfer job for carrier #{@@carrier1}"
    # clean up: carrier is stocked in, but may be in a ZFG
    assert @@sv.wait_carrier_xfer(@@carrier1, timeout: @@timeout)
    cstat = @@sv.carrier_status(@@carrier1)
    unless cstat.stocker
      # move from ZFG to stocker
      assert_equal 0, carrier_stockin(@@carrier1, @@stocker1)
      assert @@sv.wait_carrier_xfer(@@carrier1, timeout: @@timeout)
    end
    cstat = @@sv.carrier_status(@@carrier1)
    assert_equal "SI", cstat.xfer_status
  end

  def test23_single_xfer_delete_new_xfer
    prepare_resources
    @@sv.carrier_xfer(@@carrier1, @@stocker1, '', @@eqp1, @@port1)
    assert verify_carrier_xfer_job(@@carrier1, @@stocker1, @@eqp1, @@port1)
    # delete job
    sleep 1
    assert_equal 0, @@sv.carrier_xferjob_delete(@@carrier1)
    assert_empty @@sv.carrier_xferjob(@@carrier1), "lingering transfer job for carrier #{@@carrier1}"
    # new xfer job
    sleep 8
    assert_equal 0, @@sv.carrier_xfer(@@carrier1, @@stocker1, '', @@eqp1, @@port1)
    assert @@sv.wait_carrier_xfer(@@carrier1, timeout: @@timeout), "error creating new xfer job, #{@@sv.carrier_status(@@carrier1)}"
    # clean up
    assert_equal 0, carrier_stockin(@@carrier1, @@stocker1, @@eqp1, @@port1)
    assert @@sv.wait_carrier_xfer(@@carrier1, timeout: @@timeout), "error cleaning up"
  end

  def test31_single_xfer_reroute
    prepare_resources
    assert_equal 0, @@sv.carrier_xfer(@@carrier1, @@stocker1, '', @@stocker3, '')
    assert @@sv.wait_carrier_xfer(@@carrier1, timeout: @@timeout)
    # move from stocker3 to stocker1
    assert_equal 0, @@sv.carrier_xfer(@@carrier1, @@stocker3, '', @@stocker1, '')
    assert verify_carrier_xfer_job(@@carrier1, @@stocker3, @@stocker1)
    job1 = @@sv.carrier_xferjob_detail(@@carrier1)[0].strCarrierJobResult[0]
    # reroute to the other stocker
    assert_equal 0, @@sv.carrier_xfer(@@carrier1, @@stocker3, '', @@stocker2, '')
    assert verify_carrier_xfer_job(@@carrier1, @@stocker3, @@stocker2)
    job2 = @@sv.carrier_xferjob_detail(@@carrier1)[0].strCarrierJobResult[0]
    ##assert_equal job1.estart, job2.estart, "start times don't match"
    ## not implemented
    ##assert job2.eend > job2.eend, "end times not updated"
    assert @@sv.wait_carrier_xfer(@@carrier1, timeout: @@timeout)
    verify_carrier_position(@@carrier1, @@stocker2)
  end

  def test33_stockout_other_stocker
    # in a real env this is a manual test, the carrier needs to be picked up!
    skip "skipping manual test" unless @@run_manual_tests or @@use_emulators
    prepare_resources
    cs_old = @@sv.carrier_status(@@carrier1)
    assert_equal @@stocker1, cs_old.stocker
    assert_equal 0, @@sv.carrier_xfer(@@carrier1, '', '', @@stocker2, 'P4')
    @@sv.wait_carrier_xfer(@@carrier1, timeout: @@timeout)
    cs_new = @@sv.carrier_status(@@carrier1)
    assert @@stocker2, cs_new.stocker
    assert ['MO', 'SO'].member?(cs_new.xfer_status)
    # ensure this works with real and dummy AMHS
    assert_equal 0, carrier_stockin(@@carrier1, @@stocker2)
    assert @@sv.wait_carrier_xfer([@@carrier1, @@carrier2], timeout: @@timeout)
  end

  def test41_batch_xfer
    prepare_resources
    assert_equal 0, carrier_stockin(@@carrier1, @@stocker1)
    assert_equal 0, carrier_stockin(@@carrier2, @@stocker1)
    assert @@sv.wait_carrier_xfer([@@carrier1, @@carrier2], timeout: @@timeout)
    verify_carrier_position(@@carrier2, @@stocker1)
    # batch transfer
    assert_equal 0, @@sv.carrier_xfer([@@carrier1, @@carrier2], '', '', [@@eqp1, @@eqp2], [@@port1, @@port2])
    sleep 10
    assert_equal 0, @@sv.carrier_xferjob_delete(@@carrier1)
    assert_equal 0, @@sv.carrier_xferjob_delete(@@carrier2)
    assert_equal 0, @@sv.npw_reserve_cancel(@@eqp1, @@carrier1)
    assert_equal 0, @@sv.npw_reserve_cancel(@@eqp2, @@carrier2)
    assert @@sv.wait_carrier_xfer([@@carrier1, @@carrier2], timeout: @@timeout)
    #
    # Send to tool ports (single xfer)
    assert_equal 0, @@sv.carrier_xfer(@@carrier1, '', '', @@eqp1, @@port1)
    assert_equal 0, @@sv.carrier_xfer(@@carrier2, '', '', @@eqp2, @@port2)
    assert @@sv.wait_carrier_xfer([@@carrier1, @@carrier2], timeout: @@timeout)
    #
    # clean up
    assert_equal 0, @@sv.carrier_xfer(@@carrier1, @@eqp1, @@port1, @@stocker1, '')
    assert_equal 0, @@sv.carrier_xfer(@@carrier2, @@eqp2, @@port2, @@stocker2, '')
    assert @@sv.wait_carrier_xfer([@@carrier1, @@carrier2], timeout: @@timeout)
  end

  def test42_single_xfers_reroute_batch
    prepare_resources
    assert_equal 0, carrier_stockin(@@carrier2, @@stocker1)
    assert @@sv.wait_carrier_xfer(@@carrier2, timeout: @@timeout)
    # start moves (batch does not work stocker to stocker)
    [@@carrier1, @@carrier2].each {|c|
      @@sv.carrier_xfer(c, @@stocker1, '', @@stocker2, '')
    }
    assert verify_carrier_xfer_job(@@carrier1, @@stocker1, @@stocker2)
    assert verify_carrier_xfer_job(@@carrier2, @@stocker1, @@stocker2)
    # reroute batch job
    sleep 1
    assert_equal 0, @@sv.carrier_xfer([@@carrier1, @@carrier2], '', '', [@@eqp1, @@eqp2], [@@port1, @@port2])
    # check transport jobs
    assert verify_carrier_xfer_job(@@carrier1, '', @@eqp1, @@port1)
    assert verify_carrier_xfer_job(@@carrier2, '', @@eqp2, @@port2)
    # clean up
    assert @@sv.wait_carrier_xfer([@@carrier1, @@carrier2], timeout: @@timeout)
    assert_equal 0, carrier_stockin(@@carrier1, @@stocker1, @@eqp1, @@port1)
    assert_equal 0, carrier_stockin(@@carrier2, @@stocker1, @@eqp2, @@port2)
    assert @@sv.wait_carrier_xfer([@@carrier1, @@carrier2])
  end

  def test43_one_carrier_xfer_reroute_batch
    prepare_resources
    assert_equal 0, carrier_stockin(@@carrier2, @@stocker1)
    assert @@sv.wait_carrier_xfer(@@carrier2, timeout: @@timeout)
    # start moves (batch does not work stocker to stocker)
    [@@carrier1, @@carrier2].each {|c| @@sv.carrier_xfer(c, @@stocker1, '', @@stocker2, '')}
    assert verify_carrier_xfer_job(@@carrier1, @@stocker1, @@stocker2)
    assert verify_carrier_xfer_job(@@carrier2, @@stocker1, @@stocker2)
    # reroute batch job
    sleep 1
    assert_equal 0, @@sv.carrier_xfer([@@carrier1, @@carrier2], '', '', [@@eqp1, @@eqp2], [@@port1, @@port2])
    # check transport jobs
    assert verify_carrier_xfer_job(@@carrier1, '', @@eqp1, @@port1)
    assert verify_carrier_xfer_job(@@carrier2, '', @@eqp2, @@port2)
    # clean up
    assert @@sv.wait_carrier_xfer([@@carrier1, @@carrier2], timeout: @@timeout)
    assert_equal 0, carrier_stockin(@@carrier1, @@stocker1, @@eqp1, @@port1)
    assert_equal 0, carrier_stockin(@@carrier2, @@stocker1, @@eqp2, @@port2)
    assert @@sv.wait_carrier_xfer([@@carrier1, @@carrier2], timeout: @@timeout)
  end


  # aux methods

  # move a carrier from its current location to a stocker
  # !This method involves AMHS. It might not work for SiView only carrier recovery.
  #
  # return result of carrier_xfer or nil if carrier is not on a port
  def carrier_stockin(carrier, stocker, last_known_tool='', last_known_port='')
    $log.info "carrier_stockin #{carrier}, #{stocker}, #{last_known_tool}, #{last_known_port}"
    ci = @@sv.carrier_status(carrier)
    frommachine, fromport = '', ''
    if !last_known_tool.empty?
      frommachine = last_known_tool
      fromport = last_known_port
    elsif !ci.eqp.empty?
      # move from tool port
      frommachine = ci.eqp
      eqpi = @@sv.eqp_info(ci.eqp)
      eqpi.ports.each {|p| fromport = p.port if p.loaded_carrier == carrier}
      fromport = eqpi.ports[0].port if fromport.empty?  # e.g. when EO
    elsif !ci.stocker.empty?
      # move from other location, e.g. INTER1
      frommachine, fromport = ci.stocker, ''
    end
    return @@sv.carrier_xfer(carrier, frommachine, fromport, stocker, '')
  end

  def prepare_resources
    $setup_ok = false
    assert_equal 0, @@sv.stocker_inventory_upload(@@stocker1)
    assert_equal 0, @@sv.stocker_inventory_upload(@@stocker2)
    # ensure correct carrier positions
    @@sv.wait_carrier_xfer([@@carrier1, @@carrier2], timeout: @@timeout)
    [@@carrier1, @@carrier2].each {|c|
      cstat = @@sv.carrier_status(c)
      unless [@@stocker1, @@stocker2].member?(cstat.stocker) && ['SI', 'MI', 'HI'].member?(cstat.xfer_status)
        if @@use_emulators && cstat.xfer_status =~ /[AMSHE]O/
          if @@sv.f7
            assert_equal 0, carrier_stockin(c, @@stocker1, @@eqp1, @@port1)
          else
            # automatically clean up strange carrier states
            @@sv.carrier_xfer_status_change(c, 'MI', @@stocker1)
          end
        else
          assert_equal 0, carrier_stockin(c, @@stocker1)
        end
      end
      # make carriers NOTAVAILABLE to prevent processing
      @@sv.carrier_status_change(c, 'NOTAVAILABLE', check: true)
    }
    assert @@sv.wait_carrier_xfer([@@carrier1, @@carrier2], timeout: @@timeout)
    # swap source and target stockers depending on current carrier position (speed up for real AMHS)
    cstat = @@sv.carrier_status(@@carrier1)
    @@stocker1, @@stocker2 = @@stocker2, @@stocker1 if cstat.stocker == @@stocker2
    verify_carrier_position(@@carrier1, @@stocker1)
    # clean up eqps and switch to Offline-2
    [@@eqp1, @@eqp2].each {|eqp|
      assert_equal 0, @@sv.eqp_cleanup(eqp, mode: @@eqpmode)
    }
    sleep 3
    $setup_ok = true
  end

  def verify_carrier_position(carrier, stocker)
    cstat = @@sv.carrier_status(carrier)
    assert_equal stocker, cstat.stocker, "carrier #{carrier} is not in stocker #{stocker}"
    assert ['MI', 'SI'].member?(cstat.xfer_status), "transfer status of carrier #{carrier} is not SI/MI"
  end

  def verify_carrier_xfer_job(carrier, source, target, port='', mcs=false)
    $log.info "verify_carrier_xfer_job #{carrier.inspect}, #{source.inspect}, #{target.inspect}, #{port.inspect}, mcs: #{mcs}"
    jobs = mcs ? @@sv.carrier_xferjob_detail(carrier) : @@sv.carrier_xferjob(carrier)
    assert_equal 1, jobs.size, 'wrong number of xfer jobs'
    cjobs = jobs.first.strCarrierJobResult.select {|j| j.carrierID.identifier == carrier}
    assert_equal 1, cjobs.count, 'expected one carrier job'
    cjob = cjobs.first
    assert_equal target, cjob.toMachine.identifier, 'wrong toMachine'
    assert_equal port, cjob.toPortID.identifier, 'wrong toPort'
    source = '' if @@sv.f7
    assert_equal source, cjob.fromMachineID.identifier, 'wrong fromMachine, regression from MES-2358'
    return true
  end

end
