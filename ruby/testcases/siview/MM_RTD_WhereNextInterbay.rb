=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: Paul Peschel, 2013-02-01 migrate from Test101_WhereNextInterbay.java

History:
  2016-04-26 sfrieske, create vendor lots
  2019-12-09 sfrieske, minor cleanup, renamed from Test101_WhereNextInterbay
  2020-08-13 sfrieske, minor cleanup
  2021-01-15 sfrieske, moved SiView::MM#new_srclot_for_product to SiView::Test#new_srclot
=end

require 'SiViewTestCase'


class MM_RTD_WhereNextInterbay < SiViewTestCase
  @@ProductionBankWithTransportEqp = 'UT-SHIP'      # production bank with transport equipment
  @@BankShip = 'OY-SHIP'                            # production bank with transport equipment
  @@BankScrap = 'TX-SCRAP'                          # production bank without transport equipment
  @@BankNonProduction = 'UT-USE'
  @@BankOnRaw = 'UT-RAW'                            # vendor lot bank
  @@ProductionBankWithOutTransportEqp = 'TX-SCRAP'  # bank without transport equipment
  @@min_carriers = 1
  @@max_carriers = 10

  @@testlots = []


  def self.after_all_passed
    @@testlots.each {|lot| @@sv.delete_lot_family(lot)}
  end


  def test00_setup
    $setup_ok = false
    #
    2.times {
      assert srclot = @@svtest.new_srclot
      @@testlots << srclot
    }
    assert_equal 0, @@sv.lot_bank_move(@@testlots[0], @@ProductionBankWithTransportEqp)
    assert_equal 0, @@sv.lot_bank_move(@@testlots[1], @@ProductionBankWithOutTransportEqp)
    #
    $setup_ok = true
  end


  # Tests where next on a certain lot state
  def test10_WhereNextVendorNoTpEqp
    skip 'not applicable for Fab7' if @@sv.f7
    runtest('V%', '', '', @@ProductionBankWithOutTransportEqp, false)
  end

  # Tests where next on a certain lot state
  def test11_WhereNextVendorHasTpEqp
    skip 'not applicable for Fab7' if @@sv.f7
    runtest('V%', '', '', @@ProductionBankWithTransportEqp, false)
  end

  # Tests where next on a certain lot state
  def test12_WhereNextLotCOMPLETED
    runtest('%', 'COMPLETED', '', @@ProductionBankWithTransportEqp, false)
  end

  # Tests where next on a certain lot state
  def test13_WhereNextLotSHIPPED
    runtest('%', 'SHIPPED', '', @@BankShip, false)
  end

  # Tests where next on a certain lot state
  def test14_WhereNextLotSCRAPPED
    runtest('%', 'SCRAPPED', '', @@BankScrap, false)
  end

  # Tests where next lots in a non productive bank
  def test15_WhereNextLotNonProBank
    runtest('%', '', '', @@BankNonProduction, false)
  end

  # Tests where next lots in a non productive bank
  def test16_WhereNextNoTransportEquipment
    runtest('%', '', '', @@ProductionBankWithOutTransportEqp, false)
  end

  # Tests where next for lots that are on hold
  def test17_WhereNextLotONHOLD
    runtest('%', 'ONHOLD', '', '', false)
  end

  # Tests where next for lots of a certain type
  def test18_WhereNextStandard
    runtest('%', '', '', @@ProductionBankWithTransportEqp, false)
  end

  # Tests where next for lots of a certain type
  def test19_WhereNextProductionLot
    runtest('%', 'Waiting', 'Production', '', false)
  end

  # Tests where next for lots of a certain type
  def test20_WhereNextEngineeringLot
    skip 'not applicable for Fab7' if @@sv.f7
    runtest('%', 'Waiting', 'Engineering', '', false)
  end

  # Tests where next for lots of a certain type
  def test21_WhereNextEqpMonitorLot
    runtest('%', 'Waiting', 'Equipment Monitor', '', false)
  end

  # Tests where next for lots of a certain type
  def test22_WhereNextProcessMonitorLot
    runtest('%', 'Waiting', 'Process Monitor', '', false)
  end

  # Tests where next for lots of a certain type
  def test23_WhereOnRawBank
    runtest('%', '', '', @@BankOnRaw, false)
  end

  # Tests where next for lots of a certain type
  def test24_WhereNextNoCarrier
    runtest('%', '', '', @@BankOnRaw, true)
  end


  # test where_next_interbay for lots matching the search criteria
  def runtest(lotPattern, lotState, lotType, bank, empty_carrier)
    $log.info "runtest lot: #{lotPattern.inspect}, status: #{lotState.inspect}, lottype: #{lotType.inspect}, bank: #{bank.inspect}  # empty carrier: #{empty_carrier}"
    list = @@sv.lot_list(lot: lotPattern, status: lotState, lottype: lotType, bank: bank, lotinfo: true)
    list = list.select {|l| (l.carrier == '' && empty_carrier) || (l.carrier != '' && !empty_carrier)}
    assert list.count >= @@min_carriers, "at least #{@@min_carriers} carrier must be available"
    max = [list.count, @@max_carriers].min
    $log.info "  #{list.count}, #{@@max_carriers}, #{max}"
    list.take(max).each {|li|
      $log.info "where_next_interbay #{li.lot.inspect} carrier: #{li.carrier.inspect}"
      @@sv.where_next_interbay(li.lot, carrier: li.carrier)
      assert [0, 1429].member?(@@sv.rc), 'wrong return code'
    }
  end

end
