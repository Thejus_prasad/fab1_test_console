=begin 
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.
    
Author: Paul Peschel, 2013-07-29

History:
  2019-12-05 sfrieske, renamed from Test094_Banklots
=end

require 'RubyTestCase'

# Test MSR 618951 Lots on Bank List
class MM_Lot_ListOnBank < RubyTestCase
  @@expected_lcount_max = 2000
  @@find_at_least = 1000
    
    
  def test11_lots_on_Bank
    assert_equal @@expected_lcount_max.to_s, @@sv.environment_variable[1]['SP_LOTLIST_MAX_SEQLEN_FOR_LOT_LIST_INQ'], "wrong SiView env"
    found = false
    @@sv.bank_list.each {|b|
      nlots = @@sv.lot_list(bank: b.bankID.identifier).size
      next if nlots < @@find_at_least
      found = true
      $log.info "found bank #{b.bankID.identifier}: #{nlots} lots"
      assert nlots <= @@expected_lcount_max, "lot_info(bank: #{b.bankID.identifier}) returns #{nlots}, expected #{@@expected_lcount_max}"
    }
    assert found, "No bank found with at least #{@@find_at_least} lots. Put more lots on a bank."
  end

end
