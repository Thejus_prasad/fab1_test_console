=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: Beate Kochinka

History:
  2014-01-28 dsteger
  2014-12-15 ssteidte, renamed from Test_MSR472192 to Test921_CSLotUpcomingInfo, added check for RC
  2014-12-16 ssteidte, get test data from lot_info and SM, works with Active Versioning
  2015-08-07 dsteger,  clean up
  2016-02-08 sfrieske, fixed verify method (SM object_info for recipe)
  2016-06-28 dsteger,  MSR 982586
  2017-01-04 sfrieske, adapted to modified setup
  2019-12-05 sfrieske, minor cleanup, renamed from Test921_CSLotUpcomingInfo
  2020-08-28 sfrieske, move def of RouteNestedOperation here (only use)
  2021-01-13 sfrieske, minor cleanup, fixed reticlegroup lookup
=end

require 'SiViewTestCase'


# Test CS_TxLotUpcomingOperationInfoForEquipmentInq, MSR 472192
class MM_Lot_UpcomingInfoForEqp < SiViewTestCase
  @@opNo = '1000.150'    #'1000.600'
  @@eqp_other = 'UTF001' #'UTC003' # invalid for lot, route [, operation]
  @@route_other = 'UTRT002.01' # other mainroute
  @@dyn_route = 'UTBR000D.01'
  @@dyn_pd = 'UTPD000.02'
  @@dyn_opNo = '100.150'

  RouteNestedOperation = Struct.new(:route, :route_type, :ret_opNo, :pd, :opNo, :op_type, :mandatory, :sub_route)


  def self.after_all_passed
    @@sv.delete_lot_family(@@lot)
  end


  def test00_setup
    $setup_ok = false
    #
    assert @@lot = @@svtest.new_lot, 'error creating test lot'
    @@route = @@svtest.route
    refute_equal @@route, @@route_other, 'choose different routes for setup'
    refute_equal @@opNo, @@sv.lot_info(@@lot).opNo, 'pick another opNo'
    # get reference data from the opNo to look up
    assert_equal 0, @@sv.lot_opelocate(@@lot, @@opNo)
    assert @@li = @@sv.lot_info(@@lot)
    @@rgs = @@sv.lots_info_raw(@@lot, rgs: true).strLotInfo.first.strLotRecipeInfo.reticleGroupSeq.collect {|e| e.identifier}
    assert @@li.opEqps.size > 1, "must have at least 2 equipments at #{@@opNo}"
    refute_empty @@rgs, 'no reticle found at operation'
    @@eqp1, @@eqp2 = @@li.opEqps[0..1]
    assert product = @@sm.object_info(:product, @@svtest.product).first, 'no such product'
    assert @@reticleset = @@sm.object_info(:reticleset, product.specific[:reticleset]).first, 'no reticleset for product'
    assert !@@li.opEqps.member?(@@eqp_other), "#{@@eqp_other} must not be eligible at #{@@opNo}"
    # locate the lot back
    assert_equal 0, @@sv.lot_opelocate(@@lot, :first)
    #
    $setup_ok = true
  end

  def test111_empty_lot_without_route
    @@sv.lot_upcoming_info('', @@opNo, @@eqp1)
    assert_equal 207, @@sv.rc, 'unexpected return code'
  end

  def test112_empty_lot_with_route
    @@sv.lot_upcoming_info('', @@opNo, @@eqp1, route: @@route)
    assert_equal 207, @@sv.rc, 'unexpected return code'
  end

  def test121_empty_opno_without_route
    @@sv.lot_upcoming_info(@@lot, '', @@eqp1)
    assert_equal 207, @@sv.rc, 'unexpected return code'
  end

  def test122_empty_opno_with_route
    @@sv.lot_upcoming_info(@@lot, '', @@eqp1, route: @@route)
    assert_equal 207, @@sv.rc, 'unexpected return code'
  end

  def test123_empty_eqp_without_route
    @@sv.lot_upcoming_info(@@lot, @@opNo, '')
    assert_equal 207, @@sv.rc, 'unexpected return code'
  end

  def test124_empty_eqp_with_route
    @@sv.lot_upcoming_info(@@lot, @@opNo, '', route: @@route)
    assert_equal 207, @@sv.rc, 'unexpected return code'
  end

  def test211_nonexisting_lot_without_route
    @@sv.lot_upcoming_info('nonexistingLot.00', @@opNo, @@eqp1)
    assert_equal 1450, @@sv.rc, 'unexpected return code'
  end

  def test212_nonexisting_lot_with_route
    @@sv.lot_upcoming_info('nonexistingLot.00', @@opNo, @@eqp1, route: @@route)
    assert_equal 1450, @@sv.rc, 'unexpected return code'
  end

  def test22_nonexistingRoute
    @@sv.lot_upcoming_info(@@lot, @@opNo, @@eqp1, route: 'nonexistingRoute.00')
    assert_equal 1490, @@sv.rc, 'unexpected return code'
  end

  def test231_nonexisting_opno_without_route
    @@sv.lot_upcoming_info(@@lot, 'nonexisting.opno', @@eqp1)
    assert_equal 1464, @@sv.rc, 'unexpected return code'
  end

  def test232_nonexisting_opno_with_route
    @@sv.lot_upcoming_info(@@lot, 'nonexisting.opno', @@eqp1, route: @@route)
    assert_equal 1464, @@sv.rc, 'unexpected return code'
  end

  def test311_invalid_eqp_without_route
    @@sv.lot_upcoming_info(@@lot, @@opNo, @@eqp_other)
    assert_equal 1457, @@sv.rc, 'unexpected return code'
  end

  def test312_invalid_eqp_with_route
    @@sv.lot_upcoming_info(@@lot, @@opNo, @@eqp_other, route: @@route)
    assert_equal 1457, @@sv.rc, 'unexpected return code'
  end

  def test32_other_mainroute
    assert data = @@sv.lot_upcoming_info(@@lot, @@opNo, @@eqp1, route: @@route_other)
    verify(data, @@eqp1, route: @@route_other, opNo: @@opNo)
  end

  def test33_connected_subroute
    # assert subroutes = @@sv.route_nested_operations(@@route, @@opNo, subroute_info: true).first, "no subroutes for #{@@route}"
    assert rinfo = @@sv.route_nested_operations(@@route, @@opNo, subroute_info: true)
    # TODO: simplify
    subroutes = rinfo.strOperationInformationList.collect {|op|
      RouteNestedOperation.new(rinfo.routeID.identifier, rinfo.routePDType, nil,
        op.operationID.identifier, op.operationNumber, op.operationPDType, op.mandatoryFlag,
        op.strNestedRouteInfoList.collect {|nrte|
          nrte.strNestedOperationInformationList.collect {|nop|
            RouteNestedOperation.new(nrte.routeID.identifier, nrte.routePDType, nrte.returnOperationNumber,
              nop.operationID.identifier, nop.operationNumber, nop.operationPDType, nop.mandatoryFlag,
              nop.strNested2RouteInfoList.collect {|n2rte|
                n2rte.strNested2OperationInformationList.collect {|n2op|
                  RouteNestedOperation.new(n2rte.routeID.identifier, n2rte.routePDType, n2rte.returnOperationNumber,
                    n2op.operationID.identifier, n2op.operationNumber, n2op.operationPDType, n2op.mandatoryFlag)
                }
              }.flatten(1)
            )
          }
        }.flatten(1)
      )
    }.first
    assert subroute = subroutes.sub_route.first, "no subroute at #{@@opNo}"
    assert data = @@sv.lot_upcoming_info(@@lot, subroute.opNo, @@eqp1, route: subroute.route)
    verify(data, @@eqp1, route: subroute.route, opNo: subroute.opNo, pd: subroute.pd, rgs: nil)
  end

  def test34_dynamic_subroute
    assert data = @@sv.lot_upcoming_info(@@lot, @@dyn_opNo, @@eqp1, route: @@dyn_route)
    verify(data, @@eqp1, route: @@dyn_route, opNo: @@dyn_opNo, pd: @@dyn_pd, rgs: nil)
  end


  def test41_valid_without_route
    assert data = @@sv.lot_upcoming_info(@@lot, @@opNo, @@eqp1), 'unexpected error'
    verify(data, @@eqp1)
  end

  def test42_valid_with_route
    assert data = @@sv.lot_upcoming_info(@@lot, @@opNo, @@eqp2, route: @@route), 'unexpected error'
    verify(data, @@eqp2)
  end


  # aux methods

  def verify(res, eqp, route: @@li.route, opNo: @@li.opNo, pd: @@li.op, rgs: @@rgs)
    assert_equal @@lot, res.lotID.identifier, 'invalid lotID'
    assert_equal @@li.product, res.productID.identifier, 'invalid product'
    assert_equal route, res.routeID.identifier, 'invalid route'
    assert_equal opNo, res.operationNumber, 'invalid opNo'
    assert_equal pd, res.operationID.identifier, 'invalid operationID'
    assert_equal eqp, res.equipmentID.identifier, 'invalid equipment'
    assert oinfo = @@sm.object_info(:pd, pd).first
    lrcp = oinfo.specific[:lrcps][nil]
    assert_equal lrcp, res.logicalRecipeID.identifier, 'invalid logical recipe'
    assert oinfo = @@sm.object_info(:lrcp, lrcp).first
    recipe = oinfo.specific[:recipe].keys.first
    assert_equal recipe, res.machineRecipeID.identifier, 'invalid machine recipe'
    if rgs
      assert_equal rgs, res.rtclGrpIDs.map {|rg| rg.identifier}, 'invalid reticle groups'
    else
      assert opinfo = @@sv.route_operations(route, opNo: opNo).first, 'error getting route operation info'
      unless opinfo.maskLevel.empty?
        rgs = @@reticleset.specific[:rgs].find {|e| e[:photolayer] == '01'}[:rgs]
        assert_equal rgs, data.rgs, 'invalid reticle groups'
      end
    end
  end

end
