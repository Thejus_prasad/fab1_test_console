=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: Pradeep Buddaraju, 2016-09-21

History
  2018-03-06 sfrieske, replaced user_parameter_set_verify by user_paramer_change
  2019-12-05 sfrieske, minor cleanup, renamed from Test080_CustomerWaferID
  2021-01-15 sfrieske, moved SiView::MM#new_srclot_for_product to SiView::Test#new_srclot
  2021-08-27 sfrieske, split verify_equal off of misc.rb
=end

require 'util/verifyequal'
require 'SiViewTestCase'


# Test lot Customer Wafer ID, MSR 949637
class MM_Lot_CustomerWaferID < SiViewTestCase
  @@customer = 'gf3'
  @@customer_other = 'gf'
  @@product = 'UT-PRODUCT-CUSTWAFER.01'
  @@sparam_wid = 'CustomerWaferID'
  @@udata_format = 'CustomerWaferID_Format'
  @@default_cwid = 'TEST-%s_ID'
  @@custom_prefix = 'L'
  @@valid_waferid_formats = [
    'TEST$ProductID[1,].1234',
    'TEST$ProductID[1,4].1234',
    'TEST$ProductID[3,2].1234',
    'TEST$ProductID[,].1234$SEMIcheck',
    'TEST$ProductID_fromR[1,].1234',
    'TEST$ProductID_fromR[1,4].1234',
    'TEST$ProductID_fromR[3,2].1234',
    'TEST$ProductID_fromR[,].1234$SEMIcheck',
    'TEST$WaferID[1,].1234',
    'TEST$WaferID[1,4].1234',
    'TEST$WaferID[3,2].1234',
    'TEST$WaferID[,].1234$SEMIcheck',
    'TEST$Lotfamily[1,].1234',
    'TEST$Lotfamily[1,4].1234',
    'TEST$Lotfamily[3,2].1234',
    'TEST$Lotfamily[,].1234$SEMIcheck',
    'TEST$Alias2char.1234',
    "TEST$Lotfamily[1,4].$Alias2char$SEMIcheck",
    "TEST$Lotfamily[1,]-$Alias2char$SEMIcheck",
    "ABCDEFGHIJKLMNOPQRSTUVWXYZ-0123456789.$SEMIcheck",
    # sorter formats
    '$Lotfamily[1,8]$Alias2char$SEMIcheck', #LotFamily8Char
    '$Lotfamily[,].$Alias2char$SEMIcheck',  #LotFamilyDot
    '$Lotfamily[,]-$Alias2char$SEMIcheck',  #LotFamilyDash
    '$Lotfamily[,]-$Alias2char-$SEMIcheck',  #LotFamilyDoubleDash
    '$ProductID[5,3]$ProductID_fromR[1,2]$Lotfamily[5,5]$Alias2char', #MPW12Char (only for customer gf)
    '$WaferID[1,10]$SEMIcheck', #TenCharacterWaferID
    "#{@@custom_prefix}$Lotfamily[1,]-$Alias2char$SEMIcheck", #PrefixLotFamily with hardcoded prefix 'L'
  ]
  @@invalid_waferid_formats = [
    ['Test$ProductID', 12207],                  # valid verb without position and length holders
    ['Test$ProductID[1 3].1234', 12207],        # with missing ',' delimiter in square braces
    ['Test$ProductID[1,.1234', 12207],          # with missing end square brace
    ['$INVALIDVERB[1,.1234]', 10983],           # with invalid verb with invalid format
    ['$INVALIDVERB[1,3].1234' ,12207],          # with invalid verb with valid format
    ['Test$ProductID[1,].1234$INVALIDVERB[1,2].1234]', 12207], # having one valid and one invalid verb
    ## Invalid length requires a long product id ["$ProductID[,]"*19,12207], #exceeding max length
    ['Test$WaferID', 12207],                    # valid verb without position and length holders
    ['Test$WaferID[1 3].1234', 12207],          # with missing ',' delimiter in square braces
    ['Test$WaferID[1,.1234', 12207],            # with missing end square brace
    ['Test$Lotfamily', 12207],                  # valid verb without position and length holders
    ['Test$Lotfamily[1 3].1234', 12207],        # with missing ',' delimiter in square braces
    ['Test$Lotfamily[1,.1234', 12207],          # with missing end square brace
    ['Test$ProductID_fromR', 12207],            # valid verb without position and length holders
    ['Test$ProductID_fromR[1 3].1234', 12207],  # with missing ',' delimiter in square braces
    ['Test$ProductID_fromR[1,.1234', 12207],    # with missing end square brace
    ['Test$Alias3char', 12207],                 # invalid verb without position and length holders
    ['Test$PRODUCTID[1,2]', 12207],             # valid verb in different case
    ['Test$PRODUCTID_FROMR[1,2]', 12207],       # valid verb in different case
    ['Test$WAFERID[1,2]', 12207],               # valid verb in different case
    ['Test$LOTFAMILY[1,2]', 12207],             # valid verb in different case
    ['Test$ALIAS2CHAR', 12207],                 # valid verb in different case
    ['Test$SEMIcheck$Alias2char', 10982],        # semi check not at the end
    ['$SEMICHECK$ALIAS2CHAR', 12207],            # semi check not at the end and wrong keyword
  ]

  @@testlots = []


  class CustomerWaferID
		attr_accessor :pattern, :product, :lotfamily

    Verbs = [
      /\$ProductID\[(?<startIndex>[0-9]+)?,(?<length>[0-9]+)?\]/,
      /\$ProductID_fromR\[(?<startIndex>[0-9]+)?,(?<length>[0-9]+)?\]/,
      /\$WaferID\[(?<startIndex>[0-9]+)?,(?<length>[0-9]+)?\]/,
      /\$Lotfamily\[(?<startIndex>[0-9]+)?,(?<length>[0-9]+)?\]/,
      /\$Alias2char/
    ]

    # regex description of available custom waferid fields similar to BNF
    Keyword_without_Bracket = /(?<alias>\$Alias2char)|(?<semi>\$SEMIcheck$)/
    Bracket = /(?<bracket>\[(?<start>\d+)?,(?<length>\d+)?\])/
    Keyword_B = /\$Lotfamily|\$ProductID|\$ProductID_fromR|\$WaferID/
    Keyword_with_Bracket = /(?<keywordB>#{Keyword_B})#{Bracket}/
    Keyword = /#{Keyword_with_Bracket}|#{Keyword_without_Bracket}/

    # expected to have valid pattern. If expected pattern is invalid, no need to use this class.
		def initialize(pattern, product, lotfamily)
		  @pattern = pattern
      @product = product
      @lotfamily = lotfamily
		end

    # parse the given pattern and generated a supposed custom waferid
    def generate_cwid(waferid, walias)
      p = @pattern
      index = 0
      custom_waferid = ''
      while (p = p[index..-1]) && p.length > 0
        k = Keyword.match(p)
        $log.debug k.inspect
        if k.nil? # no further match
          return custom_waferid + p
        elsif k.begin(0) > 0
          custom_waferid += p[0..k.begin(0)-1]
        end
        if k['keywordB']
          if k['bracket']
            start_pos = (k['start'] || '1').to_i
            if k['length']
              end_pos = start_pos - 2 + k['length'].to_i
            else
              end_pos = -1
            end
            $log.debug {" [#{start_pos-1},#{end_pos}]"}
            case k['keywordB']
            when '$ProductID'
              custom_waferid += @product[start_pos-1..end_pos]
            when '$ProductID_fromR'
              custom_waferid += @product.reverse[start_pos-1..end_pos].reverse
            when '$Lotfamily'
              custom_waferid += @lotfamily[start_pos-1..end_pos]
            when '$WaferID'
              custom_waferid += waferid[start_pos-1..end_pos]
            else
              raise RuntimeError.new("invalid keyword #{k['keywordB']}")
            end
          else
            raise RuntimeError.new("invalid pattern #{@pattern}")
          end
        elsif k['semi']
          raise RuntimeError.new("invalid char found in #{custom_waferid}") unless /\A[\-.0-9A-Z]+\Z/ === custom_waferid
          return custom_waferid + checksum(custom_waferid)
        elsif k['alias']
          custom_waferid += walias[-2..-1]
        end
        index = k.end(0)
      end
      custom_waferid
    end

    # calculating error digits based on SEMI-M12-1103
    def checksum(custom_waferid)
      # init check characters
      cc = 'A0'
      # get numeric value of each char + given check characters
      expression = (custom_waferid + cc).each_byte.collect {|b| b - 32}
      # Horner schema
      remainder = expression.inject(0) {|res, a| res * 8 + a} % 59
      if remainder > 0
        remainder = 59 - remainder
        # correct the remainder to be divisible by 59
        cc = (cc[0].ord + (remainder >> 3 & 7)).chr + (cc[1].ord + (remainder & 7)).chr
      end
      return cc
    end
	end


  def self.startup
    super
    @@svtest.product = @@product
    @@svtest.customer = @@customer
    pinfo = @@sv.AMD_product_details(@@product)
    @@pg = pinfo.productGroupID.identifier
    @@startbank = pinfo.startBankID.identifier
  end

  def self.shutdown
    [@@customer, @@customer_other].each {|customer| @@sm.update_udata(:customer, customer, {@@udata_format=>''})}
  end

  def self.after_all_passed
    @@testlots.each {|lot| @@sv.delete_lot_family(lot)}
  end

  def setup
    assert @@sm.update_udata(:customer, @@customer, {@@udata_format=>''}) if @@sv.user_data(:customer, @@customer)[@@udata_format]
    assert @@sm.update_udata(:productgroup, @@pg, {'MaskSetCustomer'=>''}) if @@sv.user_data(:productgroup, @@pg)['MaskSetCustomer']
  end


  def test01_valid_formats
    $setup_ok = false
    #
    assert srclot = @@svtest.new_lot
    @@testlots << srclot
    @@valid_waferid_formats.each {|f|
      assert @@sm.update_udata(:customer, @@customer, {@@udata_format=>f}), "failed to update format UDATA"
      assert prepare_srclot(srclot)
      assert lot = @@svtest.new_lot(srclot: srclot), "failed to stb new lot with format #{f}"
      @@testlots << lot
      assert verify_waferids(lot, f), "wrong customer wafer id for format #{f.inspect}"
      srclot = lot
    }
    #
    $setup_ok = true
  end

  def test02_invalid_formats
    # create srclot and schedule a new lot
    assert srclot = @@svtest.new_lot
    @@testlots << srclot
    assert prepare_srclot(srclot)
    assert lot = @@sv.new_lot_release(product: @@product, customer: @@customer, srclot: srclot)
    @@testlots << lot
    #
    @@invalid_waferid_formats.each {|f, rc|
      assert @@sm.update_udata(:customer, @@customer, {@@udata_format=>f}), "failed to update format UDATA"
      assert_nil @@sv.stb(lot), "stb of new lot with format #{f} should fail"
      assert_equal rc, @@sv.rc, "wrong return code for, stb of new lot with format #{f}"
    }
  end

  def test03_length_checks
    # create srclot and schedule a new lot
    assert srclot = @@svtest.new_lot
    @@testlots << srclot
    assert prepare_srclot(srclot)
    assert lot = @@sv.new_lot_release(product: @@product, customer: @@customer, srclot: srclot)
    @@testlots << lot
    # illegal formats based on product and lot id
    length = @@product.length
    formats = [
      ["$ProductID[1,#{length+1}]", 10983],
      ["$ProductID_fromR[1,#{length+1}]", 10983],
      ["$ProductID[2,#{length}]", 10983],
      ["$ProductID_fromR[2,#{length}]", 10983],
      ["$ProductID[#{length+1},1]", 10982],
      ["$ProductID_fromR[#{length+1},1]", 10982],
    ]
    lotlength = lot.split('.').first.length
    formats += [
      ["$Lotfamily[1,#{lotlength+1}]", 10983],
      ["$Lotfamily[1,#{lotlength+1}]", 10983],
      ["$Lotfamily[2,#{lotlength}]", 10983],
      ["$Lotfamily[#{lotlength+1},1]", 10982],
    ]
    formats.each {|f, rc|
      assert @@sm.update_udata(:customer, @@customer, {@@udata_format=>f}), "failed to update format UDATA"
      assert_nil @@sv.stb(lot), "stb of new lot with format #{f} should fail"
      assert_equal rc, @@sv.rc, "wrong return code for, stb of new lot with format #{f}"
    }
  end

  def test04_valid_formats_nooverride
    assert srclot = @@svtest.new_lot
    @@testlots << srclot
    @@valid_waferid_formats.each {|f|
      assert @@sm.update_udata(:customer, @@customer, {@@udata_format=>f}), "failed to update format UDATA"
      assert prepare_srclot(srclot)
      wids = @@sv.lot_info(srclot, wafers: true).wafers.select.with_index {|w, i| i.odd?}.map {|w|
        wid = @@default_cwid % w.wafer
        assert_equal 0, @@sv.user_parameter_change('Wafer', w.wafer, @@sparam_wid, wid)
        wid
      }
      assert lot = @@svtest.new_lot(srclot: srclot), "failed to stb new lot with format #{f}"
      @@testlots << lot
      assert verify_waferids(lot, f, wids: wids), "wrong customer wafer id for format #{f.inspect}"
      srclot = lot
    }
  end

  def test05_stb_cancel
    f = @@valid_waferid_formats.last
    assert @@sm.update_udata(:customer, @@customer, {@@udata_format=>f}), "failed to update format UDATA"
    assert lot = @@svtest.new_lot
    @@testlots << lot
    @@sv.lot_info(lot, wafers: true).wafers.each {|w|
      assert_equal 0, @@sv.user_parameter_delete('Wafer', w.wafer, @@sparam_wid, check: true)
    }
    assert vlot = @@sv.stb_cancel(lot), "failed to stb cancel the lot #{lot}"
    @@testlots << vlot
    @@sv.lot_info(vlot, wafers: true).wafers.each {|w|
      assert_equal false, @@sv.user_parameter('Wafer', w.wafer, name: @@sparam_wid).valueflag
    }
  end

  def test06_vendorlots
    f = @@valid_waferid_formats.last
    assert @@sm.update_udata(:customer, @@customer, {@@udata_format=>f}), "failed to update format UDATA"
    assert vlot = @@svtest.new_srclot(product: @@product), "failed to create vendor lot"
    @@testlots << vlot
    @@sv.lot_info(vlot, wafers: true).wafers.each {|w|
      assert_equal false, @@sv.user_parameter('Wafer', w.wafer, name: @@sparam_wid).valueflag
    }
    # vlots after vendorlot_preparation_cancel have no wafer ids
    # assert_equal 0, @@sv.wafer_sort_rpt(vlot, ''), "failed to remove lot from carrier"
    # assert vlots = @@sv.vendorlot_preparation_cancel(vlot), "failed to do vendor lot cancel"
    # vlots.each {|vlot|
    #   @@sv.lot_info(vlot, wafers: true).wafers.each {|w|
    #     assert_equal false, @@sv.user_parameter('Wafer', w.wafer, name: @@sparam_wid).valueflag
    #   }
    # }
  end

  def test07_multiple_vendorlots
    f = @@valid_waferid_formats.last
    assert @@sm.update_udata(:customer, @@customer, {@@udata_format=>f}), "failed to update format UDATA"
    assert srclot = @@svtest.new_lot
    @@testlots << srclot
    assert prepare_srclot(srclot)
    assert child2 = @@sv.lot_split(srclot, 5)
    assert child3 = @@sv.lot_split(srclot, 5)
    assert lot = @@svtest.new_lot(srclot: [srclot, child2, child3]), "failed to stb new lot with format #{f}"
    @@testlots << lot
    assert verify_waferids(lot, f), "wrong custom wafer id for format #{f.inspect}"
  end

  def test08_stb_monitorlot
    f = @@valid_waferid_formats.last
    assert @@sm.update_udata(:customer, @@customer, {@@udata_format=>f}), "failed to update format UDATA"
    assert tlot = @@svtest.new_controllot('Equipment Monitor'), "failed to create new monitor lot"
    @@testlots << tlot
    @@sv.lot_info(tlot, wafers: true).wafers.each {|w|
      assert_equal false, @@sv.user_parameter('Wafer', w.wafer, name: @@sparam_wid).valueflag
    }
  end

  def test09_product_group_override
    f1, f2 = @@valid_waferid_formats.take(2)
    assert @@sm.update_udata(:customer, @@customer, {@@udata_format=>f1}), "failed to update format UDATA"
    assert @@sm.update_udata(:customer, @@customer_other, {@@udata_format=>f2}), "failed to update format UDATA"
    # override
    assert @@sm.update_udata(:productgroup, @@pg, {'MaskSetCustomer'=>@@customer_other}), "failed to update wafer format for productgroup"
    #
    assert srclot = @@svtest.new_lot
    @@testlots << srclot
    # customer wafer ids are generated in customer_other format
    assert prepare_srclot(srclot)
    assert lot1 = @@svtest.new_lot(srclot: srclot), "failed to stb new lot"
    @@testlots << lot1
    assert verify_waferids(lot1, f2), "wrong custom wafer id for format #{f2.inspect}"
    # same for customer_other
    assert prepare_srclot(lot1)
    assert lot2 = @@svtest.new_lot(srclot: lot1, customer: @@customer_other), "failed to stb new lot"
    @@testlots << lot2
    assert verify_waferids(lot2, f2), "wrong custom wafer id for format #{f2.inspect}"
  end

  # MaskSetCustomer is now a chooser with pre-defined values
  def XXtest11_product_group_invalid_customer
    f = @@valid_waferid_formats.last
    assert @@sm.update_udata(:customer, @@customer, {@@udata_format=>f}), "failed to update format UDATA"
    # override
    assert @@sm.update_udata(:productgroup, @@pg, {'MaskSetCustomer'=>'invalid'}), "failed to update wafer format for productgroup"
    #
    assert srclot = @@svtest.new_lot
    @@testlots << srclot
    assert prepare_srclot(srclot)
    assert lot = @@svtest.new_lot(srclot: srclot), "failed to stb new lot"
    @@testlots << lot
    @@sv.lot_info(lot, wafers: true).wafers.each {|w|
      assert_equal false, @@sv.user_parameter('Wafer', w.wafer, name: @@sparam_wid).valueflag, "no custom waferids expected"
    }
  end

  def testmanual99_checksum
    require 'csv'
    res = true
    #CSV.foreach("/tmp/snapshot_wafer_last_hour_fab1.csv", col_sep: "\t") do |row|
    CSV.foreach("/tmp/CustomWaferID_F8PROD.csv", col_sep: "\t") do |row|
      $log.info "#{row.inspect}"
      wafer, product, lot, walias, lotfamily, customwaferid = row
      next if lot.start_with?('T','V')
      # guess pattern
      if customwaferid.start_with?(@@custom_prefix)
        pattern = "#{@@custom_prefix}$Lotfamily[1,]-$Alias2char$SEMIcheck"
      elsif customwaferid.start_with?(product[4,3])
        pattern = "$ProductID[5,3]$ProductID_fromR[1,2]$Lotfamily_fromR[1,5]$Alias2char"
      elsif /[A-Z0-9]+\.[0-9]{2}[0-9A-Z]{2}/ === customwaferid
        pattern = "$Lotfamily[1,].$Alias2char$SEMIcheck"
      elsif /[A-Z0-9]+\.[0-9]{2}/ === customwaferid
        pattern = "$Lotfamily[1,].$Alias2char"
      elsif /[A-Z0-9]+-[0-9]{2}[0-9A-Z]{2}/ === customwaferid
        pattern = "$Lotfamily[1,]-$Alias2char$SEMIcheck"
      elsif /[A-Z0-9]+-[0-9]{2}/ === customwaferid
        pattern = "$Lotfamily[1,]-$Alias2char"
      elsif customwaferid.start_with?(wafer[1.10])
        pattern = "$WaferID[1,10]$SEMIcheck"
      else
        $log.error "unknown pattern #{customwaferid}"
        next
      end
      c = CustomerWaferID.new(pattern, product, lotfamily.split(".").first)
      res &= verify_equal(customwaferid, c.generate_cwid(wafer, walias), "wrong custom waferid for #{wafer}")
    end
    assert res
  end


  # aux methods

  def prepare_srclot(lot)
    assert_equal 0, @@sv.lot_opelocate(lot, :last)
    assert_equal 0, @@sv.lot_bankin(lot), "failed to bankin lot" if @@sv.lot_info(lot).bank.empty?
    assert_equal 0, @@sv.lot_bank_move(lot, @@startbank), "failed to move lot to startbank"
    @@sv.lot_info(lot, wafers: true).wafers.each {|w|
      assert_equal 0, @@sv.user_parameter_delete('Wafer', w.wafer, @@sparam_wid, check: true)
    }
  end

  def verify_waferids(lot, format, wids: [])
    $log.info "verify_waferids #{lot.inspect}, #{format.inspect}"
    li = @@sv.lot_info(lot, wafers: true)
    c = CustomerWaferID.new(format, li.product, li.family.split('.').first)
    li.wafers.inject(true) {|res, w|
      actual = @@sv.user_parameter('Wafer', w.wafer, name: @@sparam_wid).value
      custom_wafer = wids.find {|c| c.include?(w.wafer)}
      wid = custom_wafer || c.generate_cwid(w.wafer, w.alias)
      res & verify_equal(wid, actual, "wrong custom waferid for #{w.wafer}")
    }
  end

end
