=begin 
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.
    
Author: Steffen Steidten, 2012-06-15

History:
  2018-10-08 sfrieske, minor cleanup, simplified handling of mode candidates
  2019-12-09 sfrieske, minor cleanup, renamed from Test520_EqpModeChange
=end

require 'RubyTestCase'


# Test TxEqpModeChangeReq
class MM_Eqp_ModeChange < RubyTestCase
  @@eqp = 'UTC001'
  @@mode_forbidden = 'Auto-4'
  @@mode_wrong = 'NotExisting'
  

  def test00_setup
    $setup_ok = false
    #
    assert_equal 0, @@sv.eqp_cleanup(@@eqp)
    assert @@sv.eqp_info(@@eqp).ports.size >= 2, "tool #{@@eqp} has not enough load ports"
    #
    $setup_ok = true
  end
  
  def test11_wrong_mode
    assert_equal 941, @@sv.eqp_mode_change(@@eqp, @@mode_wrong), 'wrong error code'
  end
  
  def test12_forbidden_mode
    assert_equal 941, @@sv.eqp_mode_change(@@eqp, @@mode_forbidden), 'wrong error code'
  end
  
  def test13_transitions
    assert_equal 0, @@sv.eqp_cleanup(@@eqp, mode: 'Off-Line-1')
    eqpi = @@sv.eqp_info(@@eqp)
    # ensure all ports are in "Off-Line-1" mode
    eqpi.ports.each {|p| assert_equal 'Off-Line-1', p.mode, 'wrong port mode'}
    # get all candidate modes
    allcands = @@sv.eqp_mode_candidates(@@eqp)
    # include current mode
    allcands.each_pair {|p, cands| cands << 'Off-Line-1'}
    #
    test_ok = true
    port = allcands.keys[0]
    allcands[port].each {
      allcands[port].each {|cand|
        eqpi = @@sv.eqp_info(@@eqp)
        current_mode = nil
        eqpi.ports.each {|p| current_mode = p.mode if p.port == port}
        $log.info "-- changing #{current_mode} to #{cand}"
        res = @@sv.eqp_mode_change(@@eqp, cand)
        if res != 0
          # ports may have different allowed modes
          allowed = true
          allcands.each_pair {|p, cc| allowed &= cc.collect {|c| c}.member?(cand)}
          if allowed
            $log.warn '  error'
            test_ok = false
          else
            $log.info '  forbidden transition'
          end
        end
      }
    }
    assert test_ok
  end
  
  def test14_same_transition
    assert_equal 0, @@sv.eqp_cleanup(@@eqp, mode: 'Off-Line-2')
    assert_equal 0, @@sv.eqp_mode_change(@@eqp, 'Off-Line-2')
  end

end
