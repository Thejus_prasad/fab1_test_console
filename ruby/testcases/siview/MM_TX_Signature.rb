=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: ssteidte, 2015-05-18

History:
  2015-10-26 sfrieske, added TxEntityInhibitListInq
  2018-05-18 sfrieske, added CS_TxLotInfoInq__160  # MES-3799
  2019-12-05 sfrieske, minor cleanup, renamed from Test023_TxSignature
=end

require 'RubyTestCase'


# Test selected TXs have correct signature and siInfo signature (R15 introduction)
class MM_TX_Signature < RubyTestCase
  @@reticle = 'UTRETICLE0001'
  @@rpod = 'R29N'
  @@inhibit_owner = 'gmstest01'

  def self.shutdown
    @@sv.inhibit_cancel(:reticle, @@reticle)
  end


  def test00_setup
    $setup_ok = false
    #
    refute_empty @@sv.reticle_list(reticle: @@reticle), "reticle #{@@reticle} not found"
    refute_empty @@sv.reticle_pod_list(pod: @@rpod), "reticle pod #{@@rpod} not found"
    #
    $setup_ok = true
  end

  def test11_reticle_list
    refute_empty @@sv.reticle_list(reticle: @@reticle), "reticle #{@@reticle} not found"
    sii = @@sv._res.strFoundReticle[0].siInfo
    assert_nil @@sv.extract_si(sii)
  end

  def test12_reticle_status
    assert @@sv.reticle_status(@@reticle), "defective reticle #{@@reticle}"
    sii = @@sv._res.siInfo
    assert_nil @@sv.extract_si(sii)
  end

  def test21_reticle_pod_list
    assert @@sv.reticle_pod_list(pod: @@rpod), "reticle pod #{@@rpod} not found"
    sii = @@sv._res.strReticlePodListInfo[0].siInfo
    assert_nil @@sv.extract_si(sii)
  end

  def test22_reticle_pod_status
    assert @@sv.reticle_pod_status(@@rpod), "defective reticle pod #{@@rpod}"
    sii = @@sv._res.siInfo
    assert_nil @@sv.extract_si(sii)
  end

  def test31_inhibit_list
    assert_equal 0, @@sv.inhibit_cancel(:reticle, @@reticle)
    assert inh = @@sv.inhibit_entity(:reticle, @@reticle)
    assert_equal 0, @@sv.inhibit_update(inh, inhibit_owner: @@inhibit_owner)
    # current version (__101 in R15)
    assert inh = @@sv.inhibit_list(:reticle, @@reticle).first
    refute_nil @@sv.extract_si(@@sv._res.strEntityInhibitions[0].siInfo), 'missing siInfo for inhibit owner'
    assert_equal @@inhibit_owner, inh.inhibit_owner, 'wrong inhibit owner'
    # old Tx (without __), fails in C7.1.2 .. C7.3
    # assert inh = @@sv.inhibit_list(:reticle, @@reticle, txr8: true).first
    # refute_nil @@sv.extract_si(@@sv._res.strEntityInhibitions[0].siInfo), "missing siInfo for inhibit owner"
    # assert_equal @@inhibit_owner, inh.inhibit_owner, "wrong inhibit owner"
  end

  def test41_CS_TxLotInfoInq__160  # MES-3799
    # must have signature different from TxLotInfoInq__160
    # call TxLotInfoInq__160
    @@sv.lot_info('NOTEX')
    assert_equal 'TXTRQ003', @@sv.tx_id, 'wrong TXID for TxLotInfoInq__160'
    # call CS_TxLotInfoInq__160
    assert res = @@sv.manager.CS_TxLotInfoInq__160(@@sv._user, @@sv.oids(['NOTEX']), *Array.new(16, true))
    assert_equal 'TXTRQ520', @@sv.tx_id(res), 'wrong TXID for CS_TxLotInfoInq__160'
  end

end
