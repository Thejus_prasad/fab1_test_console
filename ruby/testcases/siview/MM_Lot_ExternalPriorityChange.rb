=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.
    
Author: Paul Peschel, 2013-03-29

History:
  2014-11-03 ssteidte, cleaned up and added comments
  2019-12-09 sfrieske, minor cleanup, renamed from Test086_UserParameterHistory
=end

require 'SiViewTestCase'


# Test MSR 288283 TxLotExternalPriorityChangeReq (fixed in core R10.0.1)
class MM_Lot_ExternalPriorityChange < SiViewTestCase
  @@prio = 2

  def self.after_all_passed
    @@sv.delete_lot_family(@@lot)
  end

  
  def test00_setup
    $setup_ok = false
    #
    assert @@lot = @@svtest.new_lot, 'error creating test lot'
    #
    $setup_ok = true
  end
  
  def test11_change_lot_prio
    # change Lot Priority and check user and time
    assert_equal 0, @@sv.lot_externalpriority_change(@@lot, 3)
    tstart = Time.now
    assert_equal 0, @@sv.lot_externalpriority_change(@@lot, @@prio) 
    li = @@sv.lot_info(@@lot)
    assert_equal li.epriority, @@prio.to_s, 'wrong priority'
    assert_equal li.user, @@sv.user, 'wrong last user'
    diff = @@sv.siview_time(li.claim_time) - tstart  
    assert diff < 30, "Claim time too late: #{diff} s after Tx"
  end
end
