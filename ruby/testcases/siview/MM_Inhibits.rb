=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author:  Steffen Steidten, 2013-03-19

History:
  2013-04-08 dsteger,  added test for inhibit not applied
  2014-04-07 dsteger,  added test for reverse entity assignment
  2014-06-30 dsteger,  expanded scope to all inhibit classes
  2015-05-13 ssteidte, cleanup and test case verification
  2015-07-01 ssteidte, more cleanup, change setup to enable fixture tests in Fab1
  2016-07-05 sfrieske, re-activated test for invalid owner (MSR 1033118)
  2018-05-14 sfrieske, minor cleanup
  2018-12-02 sfrieske, adapted for inhibit_update end date updates (MES-3856)
  2019-12-05 sfrieske, added test for inhibit_update non-expiring timestamp, renamed from Test131_Inhibits
  2020-08-26 sfrieske, minor cleanup
  2021-01-20 sschoen3, ReturnCode changed from 3037 to 0 (MES-4403 - SiView C7.18)
  2021-08-27 sfrieske, split verify_equal off of misc.rb
=end

require 'util/verifyequal'  # TODO: remove
require 'SiViewTestCase'


# Test MSRs 613451, 613482, 613483 (ProductGroup), 832862 (technology, lrecipe, stage)
#   884685 (non-expiring inhibits), MSR832876 (inhibit owner), 99391
class MM_Inhibits < SiViewTestCase
  @@eqp = 'UTR003'               # 'UTI003RTCL'
  @@opNo = '1000.2000'
  @@route = 'UTRT-INHIBIT.01'    # 'UTRT-IB-RTCL.01'
  @@product = 'UT-INHIBIT.01'    # 'UT-IB-RTCL.01'
  @@reticle = 'UTRETICLEFBRTCL'  # 'UTRGIBRTCL002'
  @@fixture = 'UTFIXTURE0001'
  @@fixture_eqp = 'UTF003'
  @@fixture_opNo = '1100.1000'
  @@other_stage = 'UTNOTUSED'
  @@other_owner = 'gmstest01'

  # take one inhibit class für enddate checks
  @@iclass_enddate = :eqp_chamber_productgroup_recipe
  @@test_fixtures = true


  def self.after_all_passed
    @@sv.delete_lot_family(@@lot)
  end


  def test00_setup
    $setup_ok = false
    #
    #assert_equal '', @@sv.environment_variable[1]['CS_SP_INHIBIT_DEFAULT_ENDTIME'], "incorrect MM environment"
    assert ['', '1901-01-01-00.00.00.00000'].member?(@@sv.environment_variable[1]['CS_SP_INHIBIT_DEFAULT_ENDTIME']), 'wrong MM environment'
    # clean up eqps
    [@@eqp, @@fixture_eqp].each {|eqp| assert_equal 0, @@sv.eqp_cleanup(eqp, mode: 'Off-Line-1', notify: false)}
    # create test lot
    assert lot = @@svtest.new_lot(product: @@product, route: @@route), 'error creating test lot'
    @@lot = lot
    assert_equal 0, @@sv.lot_opelocate(lot, @@opNo)
    # get inbit entity values
    eqpi = @@sv.eqp_info(@@eqp)
    @@chamber = eqpi.chambers.first.chamber
    li = @@sv.lot_info(lot)
    @@productgroup = li.productgroup
    @@technology = li.technology
    @@customer = li.customer
    @@stage = li.stage
    @@product = li.product
    @@route = li.route
    @@module = @@sv.AMD_modulepd(lot)
    @@op = li.op
    assert wn = @@sv.what_next(@@eqp).find {|w| w.lot == lot}, "lot #{lot} is no candicate for #{@@eqp}"
    @@lrecipe = wn.lrecipe
    @@mrecipe = wn.mrecipe
    assert r = @@sv.eqp_info_raw(@@eqp).strEquipmentAdditionalReticleAttribute.strStoredReticles.find {|r|
      r.reticleID.identifier == @@reticle
    }, "reticle #{@@reticle} not loaded at #{@@eqp}"
    reticle_group = r.reticleGroupID.identifier
    assert fixture_group = @@sv.fixture_list(eqp: @@fixture_eqp).find {|e|
      e.fixtureStatusInfo.fixtureID.identifier == @@fixture
    }.fixtureGroupID.identifier
    #
    @@inhibit_entities = {}
    SiView::MM::InhibitClasses.keys.each {|iclass|
      attrib = []
      oids = SiView::MM::InhibitClasses[iclass].collect {|e|
        case e
        when 'Chamber'; attrib << @@chamber; @@eqp
        when 'Operation'; attrib << @@opNo; @@route
        when 'Equipment'; attrib << ''; iclass.to_s.start_with?('f') ? @@fixture_eqp : @@eqp
        when 'Fixture'; attrib << ''; @@fixture
        when 'Fixture Group'; attrib << ''; fixture_group
        when 'Logical Recipe'; attrib << ''; @@lrecipe
        when 'Machine Recipe'; attrib << ''; @@mrecipe
        when 'Module Process Definition'; attrib << ''; @@module
        when 'Route'; attrib << '*'; @@route
        when 'Process Definition'; attrib << ''; @@op
        when 'Product Specification'; attrib << ''; @@product
        when 'Product Group'; attrib << ''; @@productgroup
        when 'Reticle'; attrib << ''; @@reticle
        when 'Reticle Group'; attrib << ''; reticle_group
        when 'Stage'; attrib << ''; @@stage
        when 'Technology'; attrib << ''; @@technology
        when 'Customer'; attrib << ''; @@customer
        else
          flunk "unknown class #{e}"
        end
      }
      @@inhibit_entities[iclass] = [oids, attrib]
    }
    #
    # other entity values
    @@other_chamber = eqpi.chambers[1].chamber
    refute_equal @@chamber, @@other_chamber, "#{@@eqp} must have at least to chambers"
    assert lop = @@sv.lot_operation_list(lot).find {|i| i.opNo != @@opNo}
    @@other_opNo = lop.opNo
    @@other_op = lop.op
    assert @@other_productgroup = @@sv.object_list('PosProductGroup', '%').find {|pg| pg != @@productgroup}
    assert @@other_tech = @@sv.object_list('PosTechnology', '%').find {|t| t != @@technology}
    assert @@other_customer = @@sv.object_list('PosCustomer', '%').find {|c| c != @@customer}
    assert @@other_product = @@sv.object_list('PosProductSpecification', 'UT%').find {|p| p != @@product}
    assert @@other_route = @@sv.module_list(level: 'Main', id: 'UT%').find {|m| m != @@route}
    assert @@other_module = @@sv.module_list(level: 'Module', id: 'UT%').find {|m| m != @@module}
    assert @@other_mrecipe = @@sv.mrecipe_list(mrecipe: '%.UT%').find {|e|
      e.machineRecipeID.identifier != @@mrecipe
    }.machineRecipeID.identifier
    assert @@other_lrecipe = @@sv.object_list('PosLogicalRecipe', 'UT%').find {|l| l != @@lrecipe}
    assert lrtcl = @@sv.reticle_list(count: 5).find {|r| r.reticleID.identifier != @@reticle}
    @@other_reticle = lrtcl.reticleID.identifier
    @@other_rg = lrtcl.reticleGroupID.identifier
    assert finfo = @@sv.fixture_list(eqp: @@fixture_eqp).find {|e|
      e.fixtureStatusInfo.fixtureID.identifier != @@fixture
    }
    @@other_fixture = finfo.fixtureStatusInfo.fixtureID.identifier
    @@other_fg = finfo.fixtureGroupID.identifier
    #
    @@other_entities = {}
    SiView::MM::InhibitClasses.keys.each {|iclass|
      ooids = @@inhibit_entities[iclass][0].clone
      oattrib = @@inhibit_entities[iclass][1].clone
      # override first entity
      classname = SiView::MM::InhibitClasses[iclass].first
      case classname
        when 'Chamber'; oattrib[0] = @@other_chamber
        when 'Equipment'; ooids[0] = iclass.to_s.start_with?('f') ? @@eqp : @@fixture_eqp
        when 'Logical Recipe'; ooids[0] = @@other_lrecipe
        when 'Machine Recipe'; ooids[0] = @@other_mrecipe
        when 'Process Definition'; ooids[0] = @@other_op
        when 'Module Process Definition'; ooids[0] = @@other_module
        when 'Route'; ooids[0] = @@other_route
        when 'Operation'; oattrib[0] = @@other_opNo
        when 'Product Specification'; ooids[0] = @@other_product
        when 'Product Group'; ooids[0] = @@other_productgroup
        when 'Reticle'; ooids[0] = @@other_reticle
        when 'Reticle Group'; ooids[0] = @@other_rg
        when 'Fixture'; ooids[0] = @@other_fixture
        when 'Fixture Group'; ooids[0] = @@other_fg
        when 'Stage'; ooids[0] = @@other_stage
        when 'Technology'; ooids[0] = @@other_tech
        when 'Customer'; ooids[0] = @@other_customer
        else
          flunk "unknown class #{classname}"
      end
      @@other_entities[iclass] = [ooids, oattrib]
    }
    #
    $setup_ok = true
  end

  # genereate test1xx tests per inhibit class, except fixtures
  SiView::MM::InhibitClasses.keys.reject {|k| k.to_s.start_with?('f')}.each_with_index {|iclass, i|
    send(:define_method, "test#{i + 101}_#{iclass}") {
      assert verify_inhibit_class(iclass, 0, other: true) # inhibit for other entity
      whatnext = !SiView::MM::InhibitClasses[iclass].member?('Chamber')
      assert verify_inhibit_class(iclass, 979, whatnext: whatnext)  # inhibit for the entity
    }
  }

  # # generate test2xx test for fixture and fixture group
  # SiView::MM::InhibitClasses.keys.select {|k| k.to_s.start_with?('f')}.each_with_index {|iclass, i|
  #   send(:define_method, "test#{i + 201}_#{iclass}") {
  #     skip 'skipped fixture tests' unless @@test_fixtures
  #     assert verify_inhibit_class(iclass, 0, eqp: @@fixture_eqp, opNo: @@fixture_opNo, other: true) # inhibit for other entity
  #     assert verify_inhibit_class(iclass, 1407, eqp: @@fixture_eqp, opNo: @@fixture_opNo, whatnext: false) # inhibit for the entity
  #   }
  # }

  # fixture and fixture group inhibits

  def test201_fixture
    skip 'skipped fixture tests' unless @@test_fixtures
    assert verify_inhibit_class(:fixture, 0, eqp: @@fixture_eqp, opNo: @@fixture_opNo, other: true)
    assert verify_inhibit_class(:fixture, 1407, eqp: @@fixture_eqp, opNo: @@fixture_opNo, whatnext: false)
  end

  def test202_fixture_eqp
    skip 'skipped fixture tests' unless @@test_fixtures
    assert verify_inhibit_class(:fixture_eqp, 0, eqp: @@fixture_eqp, opNo: @@fixture_opNo, other: true)
    assert verify_inhibit_class(:fixture_eqp, 1407, eqp: @@fixture_eqp, opNo: @@fixture_opNo, whatnext: false)
  end

  def test203_fg
    skip 'skipped fixture tests' unless @@test_fixtures
    assert verify_inhibit_class(:fg, 0, eqp: @@fixture_eqp, opNo: @@fixture_opNo, other: true)
    assert verify_inhibit_class(:fg, 1407, eqp: @@fixture_eqp, opNo: @@fixture_opNo, whatnext: false)
  end

  def test204_fg_eqp
    skip 'skipped fixture tests' unless @@test_fixtures
    assert verify_inhibit_class(:fg_eqp, 0, eqp: @@fixture_eqp, opNo: @@fixture_opNo, other: true)
    assert verify_inhibit_class(:fg_eqp, 1407, eqp: @@fixture_eqp, opNo: @@fixture_opNo, whatnext: false)
  end

  # inhibit updates

  def test301_inhibit_update_enddate
    tend = Time.now + 300
    assert_equal 0, @@sv.inhibit_cancel(:eqp, @@eqp), 'error canceling inhibit'
    assert inh = @@sv.inhibit_entity(:eqp, @@eqp, tend: tend, raw: true), 'failed to register inhibit'
    # valid update, MES-3856
    assert_equal 0, @@sv.inhibit_update(inh, tend: tend + 300), 'error changing end time'
    assert inhnew = @@sv.inhibit_list(:eqp, @@eqp).first
    assert_equal @@sv.siview_timestamp(tend + 300), inhnew.tend, 'wrong tend update'
    # date in the past, failed before C13, MES-4017, note: the Tx is accepted but tend is not set
    assert_equal 3037, @@sv.inhibit_update(inh, tend: (Time.now - 3600))
    # nonexpiring date, this special date is accepted but not set
    assert_equal 0, @@sv.inhibit_update(inh, tend: '1901-01-01-00.00.00.000000'), 'inhibit_update error'
    # too far in the future - SiView C7.18 ReturnCode changed from 3037 to 0 (MES-4403)
    assert_equal 0, @@sv.inhibit_update(inh, tend: '2100-01-01-00.00.00.000000')
  end

  # known issue MES-4017, fix reverted in C7.13.10, won't fix
  def testman301a_invalid_enddate
    tend = Time.now + 300
    assert_equal 0, @@sv.inhibit_cancel(:eqp, @@eqp), 'error canceling inhibit'
    assert inh = @@sv.inhibit_entity(:eqp, @@eqp, tend: tend, raw: true), 'failed to register inhibit'
    # invalid timestamp, failed before C13, MES-4017
    assert_equal 3037, @@sv.inhibit_update(inh, tend: '01.01.2015 00:30:00.000')
  end

  def test302_inhibit_expired
    assert verify_inhibit_class(@@iclass_enddate, 0, expired: true)
  end

  def test303_inhibit_delayed
    whatnext = !SiView::MM::InhibitClasses[@@iclass_enddate].member?('Chamber')
    assert verify_inhibit_class(@@iclass_enddate, 979, whatnext: whatnext, delayed: true) # inhibit for the entity
  end

  def test304_inhibit_extended
    whatnext = !SiView::MM::InhibitClasses[@@iclass_enddate].member?('Chamber')
    assert verify_inhibit_class(@@iclass_enddate, 979, whatnext: whatnext, extended: true) # inhibit for the entity
  end

  def test305_inhibit_delayed_active
    whatnext = !SiView::MM::InhibitClasses[@@iclass_enddate].member?('Chamber')
    assert verify_inhibit_class(@@iclass_enddate, 979, whatnext: whatnext, delayed: true, extended: true) # inhibit for the entity
  end

  def test306_nonexpiring_timestamps
    # set nonexpiring inhibit with tstart and tend empty
    assert @@sv.inhibit_entity(:eqp, @@eqp, tend: '')
    # verify timestamps
    assert inh = @@sv.inhibit_list(:eqp, @@eqp).first
    assert_equal '1901-01-01-00.00.00.000000', inh.tstart
    assert_equal '1901-01-01-00.00.00.000000', inh.tend
    # verify the inhibit is effective
    assert_equal 0, @@sv.lot_cleanup(@@lot, opNo: @@opNo)
    @@sv.slr(@@eqp, lot: @@lot)
    assert_equal 979, @@sv.rc, 'wrong return code'
  end

  def test401_inhibit_update_udata  # MSR 832876
    # cancel old inhibits and register the test inhibit
    iclass = @@iclass_enddate
    oids, attrib = @@inhibit_entities[iclass]
    # assert_equal 0, @@sv.inhibit_cancel(iclass, oids, attrib: attrib), 'failed to cleanup inhibits'
    assert_equal 0, @@sv.inhibit_cancel(iclass, oids), 'failed to cleanup inhibits'
    assert inh = @@sv.inhibit_entity(iclass, oids, attrib: attrib, memo: 'QA Test 1'), 'failed to register inhibit'
    # update release_time
    ts = @@sv.siview_timestamp(Time.now)
    assert_equal 0, @@sv.inhibit_update(inh, inhibit_owner: @@sv.user, release_time: ts)
    assert verify_inhibit_attrs(iclass, oids, inh, inhibit_owner: @@sv.user, release_time: ts, memo: 'QA Test 1')
    # update owner and timestamp
    assert_equal 0, @@sv.inhibit_update(inh, inhibit_owner: @@other_owner, release_time: '')
    assert verify_inhibit_attrs(iclass, oids, inh, inhibit_owner: @@other_owner, release_time: '', memo: 'QA Test 1')
    # update memo (with original inhibit_owner ('')!)
    assert_equal 0, @@sv.inhibit_update(inh, comment: 'QA Test 2')
    assert verify_inhibit_attrs(iclass, oids, inh, inhibit_owner: '', release_time: '', memo: 'QA Test 2')
    # invalid owner, MSR 1033118
    assert_equal 1466, @@sv.inhibit_update(inh, inhibit_owner: 'nobody')
    # nonexpiring timestamp
    assert_equal 0, @@sv.inhibit_update(inh, inhibit_owner: @@sv.user, release_time: '1901-01-01-00.00.00.000000')
    assert verify_inhibit_attrs(iclass, oids, inh, inhibit_owner: @@sv.user, release_time: '1901-01-01-00.00.00.000000', memo: 'QA Test 1')
    # cancel inhibit, will cancel owner/timestamp
    assert_equal 0, @@sv.inhibit_cancel(nil, nil, inhibits: [inh]), 'failed to cancel inhibit'
    iid = inh.entityInhibitID.identifier
    assert_nil @@sv.user_data(:inhibit, iid), "no inhibit owner or release time for id #{iid} expected"
  end

  # known issue MES-4017, regression since C7.13, won't fix
  def testman401a_inhibit_update_release_time_invalid_ts
    # cancel old inhibits and register the test inhibit
    iclass = @@iclass_enddate
    oids, attrib = @@inhibit_entities[iclass]
    assert_equal 0, @@sv.inhibit_cancel(iclass, oids), 'failed to cleanup inhibits'
    assert inh = @@sv.inhibit_entity(iclass, oids, attrib: attrib, memo: 'QA Test 1'), 'failed to register inhibit'
    # invalid timestamp test fails, is accepted in C7.13
    assert_equal 1466, @@sv.inhibit_update(inh, release_time: '0815')
  end

  # aux methods

  def verify_inhibit_attrs(iclass, oids, inh, params)
    inhs = @@sv.inhibit_list(iclass, oids)
    assert inh = inhs.find {|i| i.iid == inh.entityInhibitID.identifier}, 'missing inhibit'
    res  = verify_equal(params[:inhibit_owner], inh.inhibit_owner, 'wrong owner')
    res &= verify_equal(params[:release_time], inh.release_time, 'wrong expected release time')
    res &= verify_equal(params[:memo], inh.memo, 'wrong memo')
    return res
  end

  # verify an inhibit class
  def verify_inhibit_class(iclass, exp_rc, params)
    eqp = params[:eqp] || @@eqp
    opNo = params[:opNo] || @@opNo
    is_avail = params[:other] || params[:expired] || params[:delayed] && !params[:extended]
    $log.info "verify_inhibit_class #{iclass.inspect}, #{exp_rc} is_available: #{is_avail}"
    #
    # cancel all inhit combinations for the test entities
    SiView::MM::InhibitClasses.each_pair {|iclass, v|
      oids, attrib = v
      assert_equal 0, @@sv.inhibit_cancel(iclass, oids, silent: true), 'error canceling old inhibit'
      ooids, oattrib = @@other_entities[iclass]  #  other_entity(iclass)
      assert_equal 0, @@sv.inhibit_cancel(iclass, ooids, silent: true), 'error canceling old inhibit'
    }
    # clean up lot and eqp, load carrier
    assert_equal 0, @@sv.lot_cleanup(@@lot, opNo: opNo), 'error cleaning up lot'
    li = @@sv.lot_info(@@lot)
    assert li.opEqps.member?(eqp), "wrong lot op, cannot be processed on #{eqp}"
    assert_equal 0, @@sv.eqp_cleanup(eqp, mode: 'Off-Line-1', notify: false), 'error cleaning up eqp'
    @@sv.carrier_xfer_status_change(li.carrier, 'EO', eqp)
    assert_equal 0, @@sv.eqp_load(eqp, nil, li.carrier), 'error loading carrier'
    #
    # register inhibit
    # oids, attrib = params[:other] ? other_entity(iclass) : @@inhibits[iclass]
    oids, attrib = params[:other] ? @@other_entities[iclass] : @@inhibit_entities[iclass]
    iparams = {attrib: attrib, raw: true}
    if params[:delayed]
      iparams[:tstart] = @@sv.siview_timestamp(Time.now + 60)
    elsif (params[:expired] || params[:extended])
      iparams[:tend] = @@sv.siview_timestamp(Time.now + 60)
    end
    assert inh = @@sv.inhibit_entity(iclass, oids, iparams), 'error registering inhibit'
    #
    # verify the inhibit is found by inhibit list for entity - MSR 99391
    assert inh = @@sv.inhibit_list(iclass, oids.collect {|e| e + '*'}, raw: true).first, 'inhibit_list error'
    if params[:extended]
      assert_equal 0, @@sv.inhibit_update(inh, tend: (Time.now + 180)), 'error updating inhibit end date'
    end
    ($log.info 'waiting 60 s for expiration'; sleep 60) if params[:expired] || params[:extended]
    # opestart
    cj = @@sv.eqp_opestart(eqp, li.carrier)
    ret = check_availability(iclass, cj, is_avail, @@sv.rc, exp_rc)
    @@sv.eqp_opestart_cancel(eqp, cj) if cj
    assert ret, 'wrong availability'
    # unload carrier
    res = @@sv.eqp_unload(eqp, nil, li.carrier)
    ($log.warn '  error unloading carrier'; return) if res != 0
    # what next
    if params[:whatnext]
      res = @@sv.what_next(eqp).find {|e| e.lot == @@lot}
      ($log.warn "  setup error, lot #{@@lot} not in WhatNext list of #{eqp}"; return) unless res
      assert_equal (is_avail ? 0 : 1), res.inhibits.size, "  wrong inhibits in WhatNext: #{res.inhibits}"
    end
    # slr, chamber availability
    cj = @@sv.slr(eqp, lot: @@lot)
    ret = check_availability(iclass, cj, is_avail, @@sv.rc, exp_rc)
    @@sv.slr_cancel(eqp, cj) if cj
    # cancel inhibit
    assert_equal 0, @@sv.inhibit_cancel(nil, nil, inhibits: [inh]), 'error canceling inhibit'
    # res = @@sv.inhibit_list(iclass, oids, attrib: attrib)
    inhs = @@sv.inhibit_list(iclass, oids)
    iid = inh.entityInhibitID.identifier
    assert_nil inhs.find {|i| i.iid == iid}, "error canceling inhibit #{iid.inspect}"
    return ret
  end

  def check_availability(iclass, cj, available, rc, exp_rc)
    if SiView::MM::InhibitClasses[iclass].member?('Chamber')
      # check chamber availability
      ($log.warn '  cj expected in case of chamber inhibit'; return false) unless cj
      lotcas = @@sv.AMD_chambers_availability(cj) || ($log.warn "  chamber availability check failed"; return)
      lotca = lotcas.find {|e| e.lotId == @@lot} || return
      cha = lotca.chamberAvailability.find {|a| a.AMD_ChamberName == @@chamber} || return
      # verify_equal(available ? '1' : '0', cha.AMD_ChamberStatus, "  chamber #{@@chamber} should be available: #{available}")
      if cha.AMD_ChamberStatus != (available ? '1' : '0')
        $log.warn "  wrong chamber availability: expected #{available}, got #{cha.AMD_ChamberStatus}"
        return
      end
    elsif available
      # check for CJ
      $log.warn "  cj expected despite of the inhibit, last rc: #{rc}" unless cj
      return !!cj
    else
      # verify_equal(nil, cj, '  no cj expected') && verify_equal(exp_rc, rc, '  wrong rc')
      ($log.warn '  no cj expected'; return) if cj
      ($log.warn "  wrong rc: expected #{exp_rc}, got #{rc}"; return) if rc != exp_rc
    end
    return true
  end

end
