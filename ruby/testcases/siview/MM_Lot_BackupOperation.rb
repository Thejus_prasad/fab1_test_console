=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: Daniel Steger, 2012-09-13

History:
  2012-09-21 tklose,   sublottype testcase added.
  2013-01-02 dsteger,  added requirements for MSR717973: PSM, SplitWithoutHoldRelease, Backup on Production Route
  2015-07-20 ssteidte, improved creation of non-admin user instances of SiView::MM
  2015-11-16 dsteger,  added lot label check
  2016-07-11 sfrieske, added test19 for MSR 861599
  2016-08-01 sfrieske, removed inconsistencies between prepare_for_backup and prepare_for_return
  2016-12-13 dsteger,  added MSRs 1098769 and 896526
  2017-11-20 sfrieske, use struct_to_hash instead of Struct.to_hash
  2018-09-26 sfrieske, fixed lot deletion (use sublottype SCRAPQA) and test04
  2019-06-14 sfrieske, cleaned up lot_info backup information
  2019-12-05 sfrieske, minor cleanup, renamed from Test700_BackupOperation
  2020-02-12 sfrieske, removed use of _guess_op_route
  2020-10-19 sfrieske, improved cleanup
  2021-08-27 sfrieske, split verify_equal off of misc.rb
=end

require 'util/verifyequal'  # TODO: remove
require 'SiViewTestCase'


# Test Backup Operation
class MM_Lot_BackupOperation < SiViewTestCase
  @@sv_defaults = {product: 'UT-PRODUCT-FAB8.01', route: 'UTRT101.01', customer: 'qgt'}

  @@backup_env = 'f8stag'
  @@src_user = 'F1-UNITTEST'
  @@bak_user = 'F8-UNITTEST'

  @@nprobank = 'W-BACKUPOPER'
  @@nprobank_return = 'O-BACKUPOPER'
  @@sublottype = 'QE'  # avoid AutoScrapHandler interference  'PO'
  @@sublottype2 = 'EO'

  @@backup_opNo = '1000.200'
  # unused @@backup_opNo_2 = '1000.500'  # '1000.550'
  @@at_backup_opNo = '1000.300'    # in backup
  @@after_backup_opNo = '1000.500' # after backup

  @@hold_reason = 'AEHL'
  @@rework_src_route = 'UTRW001.01'
  @@rework_src_op = '1000.350'
  @@rework_src_ret = '1000.400'
  @@rework_bak_route = 'UTRW001.01'
  @@rework_bak_op = '1000.400'
  @@rework_bak_ret = '1000.400'

  @@psm_bak_route = 'UTBR001.01'
  @@psm_bak_split_op = '1000.200'
  @@psm_bak_return_op = '1000.300'

  @@ship_bank = 'UT-SHIP'

  # additional setup
  @@product2 = 'UT-PRODUCT002.01'
  @@route2 = 'UTRT002.01'
  @@rework_route = 'UTRW002.01'
  @@sub_route = 'UTBR001.01'
  @@skip_opno = '2000.200'
  @@skip_opno_next = '2000.250'
  # PSM setup
  @@sub_route2 = 'UTBR002.01'
  @@psm_split_op = '2000.300'
  @@psm_merge_op = '2000.400'
  # FPC (in backup)
  @@fpc_op = '1000.200'
  @@fpc_eqp = 'UTR001'
  @@fpc_recipe = 'P.UTMR000.01'
  @@fpc_reticle = 'UTFPCRETICLE01'
  @@test_fpc = false

  @@testlots = []


  def self.after_all_passed
    @@testlots.each {|lot|
      @@sv.backup_delete_lot_family(lot, @@sv_back)
      @@sv.delete_lot_family(lot)
      @@sv_back.delete_lot_family(lot)
    }
  end


  def test00_setup
    $setup_ok = false
    #
    assert @@svsrc_user = SiView::MM.new($env, user: @@src_user, password: :default)
    assert @@sv_back = SiView::MM.new(@@backup_env)
    @@bak_user = 'X-UNITTEST' if @@backup_env == 'let'
    assert @@svback_user = SiView::MM.new(@@backup_env, user: @@bak_user, password: :default)
    #
    assert_equal '0', @@sv_back.environment_variable[1]['CS_SP_INVALID_BKVER'], "compatibility mode should be turned off"
    #
    @@route = @@svtest.route
    @@product = @@svtest.product
    @@customer = @@svtest.customer
    @@params = {return_bank: @@nprobank_return, backup_bank: @@nprobank, backup_opNo: @@backup_opNo}
    # lot for test0x and test1x
    assert @@lot = @@svtest.new_lot, 'error creating test lot'
    @@testlots << @@lot
    #
    $setup_ok = true
  end

  def test02_send_cancel_send_return
    $setup_ok = false
    #
    # prepare for backup
    assert_equal 0, @@sv.lot_opelocate(@@lot, @@backup_opNo)
    assert @@sv.lot_nonprobankin(@@lot, @@nprobank)
    assert @@sv.wafer_sort(@@lot, '')
    # send and send cancel
    assert_equal 0, @@sv.backupop_lot_send(@@lot, @@sv_back)
    assert_equal 0, @@sv.backupop_lot_send_cancel(@@lot, @@sv_back)
    # send and receive lot
    assert_equal 0, @@sv.backupop_lot_send(@@lot, @@sv_back)
    assert_equal 0, @@sv_back.backupop_lot_send_receive(@@lot, @@sv)
    # prepare for return
    assert @@sv_back.lot_nonprobankin(@@lot, @@nprobank_return)
    assert @@sv_back.wafer_sort(@@lot, '')
    # return and receive lot
    assert_equal 0, @@sv_back.backupop_lot_return(@@lot, @@sv)
    assert_equal 0, @@sv.backupop_lot_return_receive(@@lot, @@sv_back)
    #
    $setup_ok = true
  end

  def test03_send_receive_status
    $setup_ok = false
    #
    # prepare lot
    assert_equal 0, @@sv.lot_opelocate(@@lot, @@backup_opNo)
    assert_equal 0, @@sv.lot_nonprobankin(@@lot, @@nprobank)
    assert_equal 0, @@sv.wafer_sort(@@lot, '')
    #
    # send lot to backup site
    assert_equal 0, @@sv.backupop_lot_send(@@lot, @@sv_back)
    li = @@sv.lot_info(@@lot, backup: true)
    assert li.backup.transfering, "wrong backup state: #{li.pretty_inspect}"
    assert_equal 'Send', li.backup.dest.state, "wrong backup dest: #{li.pretty_inspect}"
    #
    # receive lot at backup site
    assert_equal 0, @@sv_back.backupop_lot_send_receive(@@lot, @@sv)
    li = @@sv.lot_info(@@lot, backup: true)
    assert li.backup.backup_proc, "wrong lot backup proc: #{li.pretty_inspect}"
    assert !li.backup.current_loc, "wrong lot backup loc: #{li.pretty_inspect}"
    assert_equal 'Receive', li.backup.dest.state, "wrong backup dest: #{li.pretty_inspect}"
    #
    # verify lot parameters at backup site
    li = @@sv_back.lot_info(@@lot, backup: true)
    assert li.backup.backup_proc, "wrong lot backup proc: #{li.pretty_inspect}"
    assert li.backup.current_loc, "wrong lot backup loc: #{li.pretty_inspect}"
    #
    # process lot at the backup site
    assert @@sv_back.claim_process_lot(@@lot), 'error processing lot'
    #
    # return lot from backup site
    assert_equal 0, @@sv_back.lot_nonprobankin(@@lot, @@nprobank_return)
    assert_equal 0, @@sv_back.wafer_sort(@@lot, '')
    assert_equal 0, @@sv_back.backupop_lot_return(@@lot, @@sv)
    li = @@sv.lot_info(@@lot, backup: true)
    assert li.backup.transfering, 'Lot should be in transfer'
    assert_equal 'Return', li.backup.dest.state, 'Lot should be sent back'
    liback = @@sv_back.lot_info(@@lot, backup: true)
    assert liback.backup.backup_proc, 'Lot should be on backup site'
    assert liback.backup.current_loc, 'Lot should be on backup site'
    #
    # receive lot at original site
    assert_equal 0, @@sv.backupop_lot_return_receive(@@lot, @@sv_back)
    li = @@sv.lot_info(@@lot, backup: true)
    assert !li.backup.backup_proc, 'Lot should be on source site'
    assert li.backup.current_loc, 'Lot should be on source site'
    assert_equal '-', li.backup.dest.state, 'Lot should be sent back'
    liback = @@sv_back.lot_info(@@lot, backup: true)
    assert !liback.backup.backup_proc, 'Lot should not be on backup site'
    assert !liback.backup.current_loc, 'Lot should not be on backup site'
    assert_equal '-', liback.backup.src.state, 'Lot should have been sent back'
    #
    $setup_ok = true
  end

  def test04_lot_attributes
    assert lot = @@svtest.new_lot, 'error creating test lot'
    @@testlots << lot
    # set lot label, comment, note at source site
    assert_equal 0, @@sv.user_data_update(:lot, lot, {'LOT_LABEL'=>'1234567890'}), 'error setting lot label'
    assert_equal 0, @@sv.lot_note_register(lot, 'Backup scenario', 'QA Test'), 'error registering lot note'
    #
    # verify and change lot parameters at backup site
    assert do_at_backup_site(lot, @@sv_back, @@params) {
      assert verify_lot_attributes(lot)
      # add lot note at the backup site
      assert_equal 0, @@svback_user.lot_note_register(lot, 'Return scenario', 'New comment'), 'error registering lot note'
    }
    # verify lot attribute changes at the source site
    assert verify_lot_attributes(lot)
  end

  # Need to run test with a new lot (never sent to backup site before)
  def test06_split_send_parent_return_send
    assert lot = @@svtest.new_lot, 'error creating test lot'
    @@testlots << lot
    assert child = @@sv.lot_split(lot, 5)
    sleep 5
    params = @@params.merge(src_carrier: @@sv.lot_info(lot).carrier)
    assert_equal 20, do_at_backup_site(lot, @@sv_back, params) {@@svback_user.lot_info(lot).nwafers}, 'wrong wafer count'
    # move to backup send operation and merge
    [child, lot].each {|l| assert_equal 0, @@sv.lot_opelocate(l, @@backup_opNo)}
    assert_equal 0, @@sv.lot_merge(lot, child)
    # check wafer count at remote site
    assert_equal 25, do_at_backup_site(lot, @@sv_back, params) {@@svback_user.lot_info(lot).nwafers}, 'wrong wafer count'
  end

  def test07_split_source_backup
    assert lot = @@svtest.new_lot, 'error creating test lot'
    @@testlots << lot
    #
    # split at source site
    src_children1 = do_split_actions(@@svsrc_user, lot, @@rework_src_route, @@rework_src_op, @@rework_src_ret)
    assert_equal 7, src_children1.count, 'error splitting lot'
    #
    # split at backup site
    bak_children1 = do_at_backup_site(lot, @@sv_back, @@params) {
      do_split_actions(@@svback_user, lot,
        @@rework_bak_route, @@rework_bak_op, @@rework_bak_ret,
        @@psm_bak_route, @@psm_bak_split_op, @@psm_bak_return_op, @@psm_bak_return_op
      )
    }
    assert_equal 9, bak_children1.count
    #
    # move to backup send operation and split again
    assert_equal 0, @@sv.lot_opelocate(lot, @@backup_opNo)
    src_children2 = do_split_actions(@@svsrc_user, lot, @@rework_src_route, @@rework_src_op, @@rework_src_ret)
    assert_equal 7, src_children2.count, 'error splitting lot'
    #
    # split again at backup site
    bak_children2 = do_at_backup_site(lot, @@sv_back, @@params) {
      do_split_actions(@@svback_user, lot, @@rework_bak_route, @@rework_bak_op, @@rework_bak_ret)
    }
    assert_equal 9, bak_children1.count  # TODO:  bak_children2.count ?
    #
    # check no additional childid was created on both sites
    assert_empty src_children1 & src_children2 & bak_children1 & bak_children2, 'overlapping child id'
  end

  # Need to run test with a new lot
  def test08_split_sequence_qualcomm
    lotid_sequence_test('qgt')
  end

  # Need to run test with a new lot
  def test09_split_sequence_samsung
    lotid_sequence_test('samsung', offset: 2)
  end

  # change sublottype at backup site
  def test10_mfg_order_change
    lot = @@lot
    @@svsrc_user.lot_mfgorder_change(lot, sublottype: @@sublottype)
    assert_equal @@sublottype, @@sv.lot_info(lot).sublottype
    sleep 5
    do_at_backup_site(lot, @@sv_back, @@params) {@@svback_user.lot_mfgorder_change(lot, sublottype: @@sublottype2)}
    assert_equal @@sublottype2, @@sv.lot_info(lot).sublottype, 'wrong sublottype'
  end

  # check changed priority is inherited to the backup site
  def test11_priority_change
    assert lot = @@svtest.new_lot, 'error creating test lot'
    @@testlots << lot
    assert_equal 0, @@svsrc_user.schdl_change(lot, priority: 0)
    assert_equal '0', @@sv.lot_info(lot).priority
    sleep 5
    assert_equal '0', do_at_backup_site(lot, @@sv_back, @@params) {@@svback_user.lot_info(lot).priority}
    @@sv.delete_lot_family(lot)
  end

  # check external priotity can be changed at backup site, TODO: merge with test11?
  def test12_external_priority_change
    lot = @@lot
    assert_equal 0, @@svsrc_user.lot_externalpriority_change(lot, 777)
    assert_equal '777', @@sv.lot_info(lot).epriority
    sleep 5
    assert do_at_backup_site(lot, @@sv_back, @@params) {@@svback_user.lot_externalpriority_change(lot, 1777)}
    assert_equal '1777', @@sv.lot_info(lot).epriority, "wrong external priority"
  end

  # MSR 686249
  def test13_priority_change_in_backup
    lot = @@lot
    assert_equal 0, @@svsrc_user.schdl_change(lot, priority: 4), "failed to prepare #{@@lot}"
    assert do_at_backup_site(lot, @@sv_back, @@params) {
      res = verify_equal(0, @@svback_user.schdl_change(lot, priority: 0), "priority change at backup site failed")
      li = @@svback_user.lot_info(lot)
      res &= verify_equal("0", li.priority, "wrong priority")
      # cannot change on source site
      res &= verify_equal(1247, @@svsrc_user.schdl_change(lot, priority: 1), "wrong priority change at source site")
      sleep 5
      res
    }
    # source site still has old priority class
    assert_equal '4', @@sv.lot_info(lot).priority, "wrong priority class"
    # cannot change on backup site
    assert_equal(1247, @@svback_user.schdl_change(lot, priority: 1), "wrong priority change at backup site")
    # send again to backup site
    assert_equal '0', do_at_backup_site(lot, @@sv_back, @@params) {
      @@svback_user.lot_info(lot).priority
    }, "backup priority should be preserved"
  end

  # MSR 686249, TODO: same as test13??
  def test14_priority_change_in_backup
    lot = @@lot
    assert_equal 0, @@svsrc_user.schdl_change(lot, priority: 2), "failed to prepare #{@@lot}"
    assert_equal '2', @@svsrc_user.lot_info(lot).priority, "wrong priority"
    # send and receive lot
    assert @@sv.backup_prepare_send_receive(lot, @@sv_back, @@params)
    # change prio at backup site
    assert_equal 0, @@svback_user.schdl_change(lot, priority: 0), "failed to prepare #{@@lot}"
    assert_equal '0', @@svback_user.lot_info(lot).priority, "wrong priority"
    # return lot, prio is still the old one
    assert @@sv_back.backup_prepare_return_receive(lot, @@sv, @@params)
    assert_equal '2', @@sv.lot_info(lot).priority, "wrong priority"
  end

  # MSR 717973
  def test15_product_change_in_backup
    lot = @@lot
    assert_equal 0, @@svsrc_user.schdl_change(lot, product: @@product, route: @@route), "failed to prepare #{@@lot}"
    assert do_at_backup_site(lot, @@sv_back, @@params) {
      res = verify_equal(0, @@svback_user.schdl_change(lot, product: @@product2, route: @@svback_user.lot_info(@@lot).route), "product change at backup site failed")
      li = @@svback_user.lot_info(lot)
      res &= verify_equal(@@product2, li.product, "wrong product")
      # cannot change on source site
      res &= verify_equal(1247, @@svsrc_user.schdl_change(lot, product: @@product2, route: @@route2), "product change at source site should have failed")
      sleep 5
      res
    }
    # source site still has old priority class
    assert_equal @@product2, @@sv.lot_info(lot).product, "wrong product at source site"
    # cannot change on backup site
    assert_equal(1247, @@svback_user.schdl_change(lot, product: @@product, route: @@route), "priority change at backup site should have failed")
    # change in source site succeeds
    assert_equal 0, @@svsrc_user.schdl_change(lot, product: @@product, route: @@route), "failed to change product for #{@@lot}"
    # send again to backup site
    assert_equal @@product, do_at_backup_site(lot, @@sv_back, @@params) {
      @@svback_user.lot_info(lot).product
    }, "backup product should be same as source site's product"
  end

  def test16_family_sync
    lot = @@lot
    child = @@svsrc_user.lot_split(lot, 2)
    assert @@sv.backup_prepare_send_receive(lot, @@sv_back, backup_bank: @@nprobank, backup_opNo: @@backup_opNo)
    child1 = @@svback_user.lot_split(lot, 2)
    child2 = @@svback_user.lot_split(lot, 2)
    child3 = @@svback_user.lot_split(lot, 2)
    child4 = @@svback_user.lot_split(lot, 2)
    child5 = @@svback_user.lot_split(lot, 2)
    child6 = @@svback_user.lot_split(lot, 2)
    assert_equal 0, @@svback_user.lot_merge(child1, child2), 'lot merge in backup failed'
    assert_equal 0, @@svback_user.scrap_wafers(child3), 'scrap wafers in backup failed'
    assert_equal 0, @@svback_user.lot_opelocate(child4, :last), 'error locating lot in backup'
    refute_empty @@svback_user.lot_info(child4).bank, 'lot not banked in in backup'
    assert_equal 0, @@svback_user.lot_hold(child5, @@hold_reason), 'lot hold in backup failed'
    assert_equal 0, @@svback_user.lot_nonprobankin(child6, @@nprobank_return), 'nonpro bankin in backup failed'
    #
    assert_equal 0, @@sv.backup_lot_family_sync(lot, @@sv_back), 'sync failed'
    verify_lot_family_insync(lot)
    # undo actions
    assert_equal 0, @@svback_user.scrap_wafers_cancel(child3), 'scrap cancel in backup failed'
    @@svback_user.lot_bankin_cancel(child4)
    assert_equal 0, @@svback_user.lot_opelocate(child4, :first), 'bankin cancel in backup failed'
    assert_equal 0, @@svback_user.lot_nonprobankout(child6), 'nonpro bankout in backup failed'
    assert @@svback_user.merge_lot_family(lot, nosort: true), 'error merging lots'
    # sync once more and verify
    assert_equal 0, @@sv.backup_lot_family_sync(lot, @@sv_back), 'sync failed'
    verify_lot_family_insync(lot)
    # return and verify sync
    assert @@sv_back.backup_prepare_return_receive(lot, @@sv, return_bank: @@nprobank_return)
    verify_lot_family_insync(lot)
    assert_equal 0, @@sv.lot_opelocate(lot, @@at_backup_opNo)
    assert @@svsrc_user.merge_lot_family(lot), 'error merging lots'
  end

  # change sublottype at backup site, MSR1098769, TODO: test ok??
  def test18_sublottype_change
    assert lot = @@svtest.new_lot(sublottype: 'EB'), 'error creating test lot'
    @@testlots << lot
    @@sv.prepare_for_backup(lot, @@params)
    assert_equal 11449, @@sv.backupop_lot_send(lot, @@sv_back, @@params), "backup send for #{lot} should fail"
    assert_equal 0, @@sv.backupop_lot_send_cancel(lot, @@sv_back, @@params), "cancel send should succeed"
  end

  # MSR 861599
  def test19_no_split_at_srcsite_allowed
    assert lot = @@svtest.new_lot, 'error creating test lot'
    @@testlots << lot
    carrier = @@sv.lot_info(lot).carrier
    assert lcs = 2.times.collect {@@sv.lot_split(lot, 5)}, "error splitting lot"
    params = @@params.merge(src_carrier: carrier)
    # send parent and children to backup site
    ([lot] + lcs).each {|l|
      assert @@sv.prepare_for_backup(l, params)
      assert_equal 0, @@sv.backupop_lot_send(l, @@sv_back, params)
      assert_equal 0, @@sv_back.backupop_lot_send_receive(l, @@sv, params)
    }
    # try to merge child1 and child2 at source site
    assert_equal 1247, @@sv.lot_merge(lcs[0], lcs[1]), "merge must be rejected"
    # try to split child1 without hold release at source site
    assert_nil @@sv.lot_split(lcs[1], 2, onhold: true)
    assert_equal 1247, @@sv.rc, "split must be rejected"
    # return to source site for cleanup
    ([lot] + lcs).each {|l|
      assert @@sv_back.prepare_for_return(l, params)
      assert_equal 0, @@sv_back.backupop_lot_return(l, @@sv, params)
      assert_equal 0, @@sv.backupop_lot_return_receive(l, @@sv_back, params)
    }
  end

  def test20_split_send_parent_and_child_return
    assert lot = @@svtest.new_lot, 'error creating test lot'
    @@testlots << lot
    carrier = @@sv.lot_info(lot).carrier
    child = @@sv.lot_split(lot, 5)
    params = @@params.merge(src_carrier: carrier)
    # send and receive split lot
    $log.info "----------------- Backup Site -----------------"
    [lot, child].each do |l|
      assert @@sv.prepare_for_backup(l, params)
      assert_equal 0, @@sv.backupop_lot_send(l, @@sv_back, params)
      assert_equal 0, @@sv_back.backupop_lot_send_receive(l, @@sv, params)
      count = (l == child) ? 5 : 20
      assert_equal count, @@svback_user.lot_info(l).nwafers, "Should have #{count} wafers"
    end
    # return and receive lot
    [lot, child].each do |l|
      assert @@sv_back.prepare_for_return(l, params)
      assert_equal 0, @@sv_back.backupop_lot_return(l, @@sv, params)
      assert_equal 0, @@sv.backupop_lot_return_receive(l, @@sv_back, params)
    end
    $log.info "----------------- Source Site -----------------"
    # move to backup send operation and merge
    assert_equal 0, @@sv.lot_merge(lot, child)
    # cleanup
    @@sv.delete_lot_family(lot)
  end

  def test21_send_parent_split_and_child_return
    assert lot = @@svtest.new_lot, 'error creating test lot'
    @@testlots << lot
    carrier = @@sv.lot_info(lot).carrier
    params = @@params.merge({src_carrier: carrier})
    # send complete lot
    assert @@sv.backup_prepare_send_receive(lot, @@sv_back, params), "failed to send lot to backup"
    $log.info "----------------- Backup Site -----------------"
    child = @@sv_back.lot_split(lot, 5)
    assert child, "expected child lot"
    # prepare lot and child for return
    [lot,child].each {|l| assert @@sv_back.prepare_for_return(l, params)}
    # no error when trying to return parent first (MSR717973)
    [lot, child].each do |l|
      assert @@sv_back.backupop_lot_return(l, @@sv, params), "failed to return #{l}"
      ##assert @@sv.backupop_lot_return_receive(l, @@sv_back, params), "failed to return receive #{l}"
    end
    return
    $log.info "----------------- Source Site -----------------"
    # move to backup send operation and merge
    assert_equal 0, @@sv.lot_merge(lot, child)
    # cleanup
    @@sv.delete_lot_family(lot)
  end

  # MSR 850953
  def test22_send_parent_with_child_and_new_child_return
    assert lot = @@svtest.new_lot, 'error creating test lot'
    @@testlots << lot
    child = @@sv.lot_split(lot, 10)
    params = @@params.merge(src_carrier: @@sv.lot_info(lot).carrier)
    # send and receive split lot
    [child, lot].each {|l|
      assert @@sv.backup_prepare_send_receive(l, @@sv_back, params), 'error sending lot to backup site'
      assert_equal @@sv.lot_info(l).nwafers, @@svback_user.lot_info(l).nwafers, 'wrong wafer count'
    }
    $log.info "----------------- Backup Site -----------------"
    child1 = @@svback_user.lot_split(lot, 5)
    child2 = @@svback_user.lot_split(child, 5)
    assert_equal 0, @@svback_user.wafer_sort(child2, @@svback_user.lot_info(child1).carrier, slots: 'empty'), "failed to move lot to other carrier"
    assert_equal 0, @@svback_user.lot_merge(child1, child2), "failed to merge in backup"
    ###@@sv.backup_lot_family_sync(lot, @@svback_user)
    # return and receive lot
    [child1, child, lot].each {|l|
      assert @@sv_back.backup_prepare_return_receive(l, @@sv, params)
      # res = @@sv_back.backup_prepare_return_receive(l, @@sv, params)
      # if res == nil # this is a workaround for current failure (still valid ??)
      #   @@sv.backupop_lot_return_receive(l, @@sv_back, recover: true)
      #   assert_equal 0, @@sv.backup_lot_family_sync(lot, @@svback_user), "failed to sync lot family"
      #   assert_equal 0, @@sv.backupop_lot_return_receive(l, @@sv_back, params), "backup return receive failed for #{l}"
      # end
    }
  end

  # MSR 850956
  def test23_send_parent_with_child_and_new_child_return_sync
    assert lot = @@svtest.new_lot, 'error creating test lot'
    @@testlots << lot
    child = @@sv.lot_split(lot, 10)
    params = @@params.merge(src_carrier: @@sv.lot_info(lot).carrier)
    # send and receive split lot
    [child, lot].each {|l|
      assert @@sv.backup_prepare_send_receive(l, @@sv_back, params), 'error sending lot to backup site'
      assert_equal @@sv.lot_info(l).nwafers, @@svback_user.lot_info(l).nwafers, 'wrong wafer count'
    }
    $log.info "----------------- Backup Site -----------------"
    # new child in backup
    child1 = @@svback_user.lot_split(lot, 5)
    # return lots, not yet receive them
    [child1, child, lot].each {|l|
      assert @@sv_back.prepare_for_return(l, params)
      assert_equal 0, @@sv_back.backupop_lot_return(l, @@sv, params)
    }
    # receive the lots (parent first --> this triggers a sync)
    [lot, child1, child].each {|l|
      assert_equal 0, @@sv.backupop_lot_return_receive(l, @@sv_back, params)
    }
  end

  # MSR 850953
  def test24_send_parent_and_new_child_return
    assert lot = @@svtest.new_lot, 'error creating test lot'
    @@testlots << lot
    child = @@sv.lot_split(lot, 10)
    params = @@params.merge(src_carrier: @@sv.lot_info(lot).carrier)
    # send and receive parent lot
    assert @@sv.backup_prepare_send_receive(lot, @@sv_back, params), 'error sending lot to backup site'
    $log.info "----------------- Backup Site -----------------"
    # split lot at backup site
    child1 = @@svback_user.lot_split(lot, 5)
    # return and receive new child and lot
    [child1, lot].each {|l| assert @@sv_back.backup_prepare_return_receive(l, @@sv, params)}
  end

  # MSR 850953, supersedes not implemented MSR 654074 (former testdev05)
  def test25_send_child_and_new_child_return
    assert lot = @@svtest.new_lot, 'error creating test lot'
    @@testlots << lot
    child = @@sv.lot_split(lot, 10)
    params = @@params.merge(src_carrier: @@sv.lot_info(lot).carrier)
    # send and receive child lot
    assert @@sv.backup_prepare_send_receive(child, @@sv_back, params), 'error sending lot to backup site'
    $log.info "----------------- Backup Site -----------------"
    child1 = @@svback_user.lot_split(child, 5)
    # return and receive child and new child
    [child1, child].each {|l| assert @@sv_back.backup_prepare_return_receive(l, @@sv, params)}
  end

  # MSR850953
  def test26_send_parent_with_child_and_new_child_return_after_parent_returned
    assert lot = @@svtest.new_lot, 'error creating test lot'
    @@testlots << lot
    child = @@sv.lot_split(lot, 10)
    params = @@params.merge(src_carrier: @@sv.lot_info(lot).carrier)
    # send and receive split lot
    [child, lot].each {|l|
      assert @@sv.backup_prepare_send_receive(l, @@sv_back, params), 'error sending lot to backup site'
      assert_equal @@sv.lot_info(l).nwafers, @@svback_user.lot_info(l).nwafers, 'wrong wafer count'
    }
    $log.info "----------------- Backup Site -----------------"
    # return the parent
    assert @@sv_back.backup_prepare_return_receive(lot, @@sv, params), "failed to return parent"
    # split off a new grandchild
    child1 = @@svback_user.lot_split(child, 5)
    ###@@sv.backup_lot_family_sync(lot, @@svback_user)
    # return and receive child and grandchild
    [child1, child].each {|l| assert @@sv_back.backup_prepare_return_receive(l, @@sv, params)}
  end

  # MSR850953
  def test27_send_parent_with_child_and_merged_parent_returned
    assert lot = @@svtest.new_lot, 'error creating test lot'
    @@testlots << lot
    child1 = @@sv.lot_split(lot, 5)
    child2 = @@sv.lot_split(lot, 5)
    params = @@params.merge(src_carrier: @@sv.lot_info(lot).carrier)
    # send and receive split lot
    [child1, child2, lot].each {|l|
      assert @@sv.backup_prepare_send_receive(l, @@sv_back, params), 'error sending lot to backup site'
      assert_equal @@sv.lot_info(l).nwafers, @@svback_user.lot_info(l).nwafers, 'wrong wafer count'
    }
    $log.info "----------------- Backup Site -----------------"
    # merge the children
    assert_equal 0, @@svback_user.wafer_sort(child2, @@svback_user.lot_info(child1).carrier, slots: 'empty'), "failed to move #{child2} to other carrier"
    assert_equal 0, @@svback_user.lot_merge(child1, child2)
    # return and receive lot and merged child
    [lot, child1].each {|l| assert @@sv_back.backup_prepare_return_receive(l, @@sv, params)}
    $log.info "----------------- Source Site -----------------"
    # merge
    assert_equal 0, @@sv.lot_merge(lot, child1)
    assert_equal 25, @@svsrc_user.lot_info(lot).nwafers, "all wafers should be in parent lot #{lot}"
    assert_equal 'EMPTIED', @@svsrc_user.lot_info(child2).status, "lot should be emptied"
  end

  def test29_split_send_child_split_merge_return
    # need to run test with a new lot (never sent to backup site before)
    assert lot = @@svtest.new_lot, 'error creating test lot'
    @@testlots << lot
    child = @@sv.lot_split(lot, 5)
    params = @@params.merge(src_carrier: @@sv.lot_info(lot).carrier)
    sleep 5
    # Send child only
    assert_equal 5, do_at_backup_site(child, @@sv_back, params) {
      assert c2 = @@svback_user.lot_split(child, 1), "failed to split child lot"
      assert_equal 0, @@svback_user.lot_merge(child, c2), "failed to merge child lot"
      @@svback_user.lot_info(child).nwafers
    }, 'wrong wafer count'
  end

  def test30_future_hold_in
    assert lot = @@svtest.new_lot, 'error creating test lot'
    @@testlots << lot
    ops = @@sv.lot_operation_list(lot, raw: true)
    opNo_idx = ops.find_index {|e| e.operationNumber == @@backup_opNo}
    opNo_hold = ops[opNo_idx + 1].operationNumber
    assert_equal 0, @@sv.lot_futurehold(lot, opNo_hold, nil), 'error setting future hold'
    assert !@@sv.backup_prepare_send_receive(lot, @@sv_back, @@params), 'lot must not be sent to backup site'
    # cleanup
    @@sv.delete_lot_family(lot)
  end

  def test31_future_hold_at
    assert lot = @@svtest.new_lot, 'error creating test lot'
    @@testlots << lot
    assert_equal 0, @@sv.lot_futurehold(lot, @@at_backup_opNo, nil), 'error setting future hold'
    assert do_at_backup_site(lot, @@sv_back, @@params), 'error sending or returning lot'
    assert_equal 'ONHOLD', @@sv.lot_info(lot).status, "Lot should be onhold"
    # cleanup
    @@sv.delete_lot_family(lot)
  end

  def test32_future_hold_after
    assert lot = @@svtest.new_lot, 'error creating test lot'
    @@testlots << lot
    assert_equal 0, @@sv.lot_futurehold(lot, @@after_backup_opNo, nil), 'error setting future hold'
    assert do_at_backup_site(lot, @@sv_back, @@params), 'error sending or returning lot'
    assert_equal 0, @@sv.lot_opelocate(lot, @@after_backup_opNo), "Cannot locate"
    assert_equal 'ONHOLD', @@sv.lot_info(lot).status, "Lot should be onhold"
    # cleanup
    @@sv.delete_lot_family(lot)
  end

  def test40_scrap_in_backup
    assert lot = @@svtest.new_lot, 'error creating test lot'
    @@testlots << lot
    assert @@sv.backup_prepare_send_receive(lot, @@sv_back, @@params), 'error sending lot to backup site'
    # prepare lot for return and scrap it
    # (note: the wafers are actually moved to a carrier in source site. In MMOPI this is prevented by the GUI.)
    assert @@sv_back.prepare_for_return(lot, @@params)
    assert_equal 0, @@svback_user.scrap_wafers(lot), 'error scrapping lot'
    # return the scrapped lot
    assert_equal 0, @@sv_back.backupop_lot_return(lot, @@sv), 'error returning lot'
    assert_equal 0, @@sv.backupop_lot_return_receive(lot, @@sv_back), 'error receiving returned lot'
    # check lot state at source site
    assert_equal 'SCRAPPED', @@sv.lot_info(lot).status, 'wrong lot status'
    # cleanup
    @@sv.delete_lot_family(lot)
  end

  def test41_scrap_child_in_backup
    assert lot = @@svtest.new_lot, 'error creating test lot'
    @@testlots << lot
    child = nil
    assert do_at_backup_site(lot, @@sv_back, @@params) {
      # split and scrap wafers, sync with source site
      child = @@svback_user.lot_split(lot, 2)
      @@svback_user.scrap_wafers(child)
      @@sv.backup_lot_family_sync(lot, nil)
    }
    # Check lot state
    assert_equal 'SCRAPPED', @@sv.lot_info(child).status, "Child should be scrapped"
    assert_equal 'Waiting', @@sv.lot_info(lot).status, "Lot should be scrapped"
    assert_equal 23, @@sv.lot_info(lot).nwafers, 'wrong wafer count'
    # cleanup
    @@sv.delete_lot_family(lot)
  end

  def test42_delete_at_backup  # MSR 896526
    assert lot = @@svtest.new_lot, 'error creating test lot'
    @@testlots << lot
    assert do_at_backup_site(lot, @@sv_back, @@params), 'error sending or returning lot'
    # delete the lot at source site
    assert_equal 0, @@sv.sublottype_change(lot, 'SCRAPQA')
    # slt = @@sv.lot_info(lot).sublottype
    # assert $sm.change_sublottype(slt, 0)
    assert_equal 0, @@sv.wafer_sort(lot, '')
    assert_equal 0, @@sv.scrap_wafers(lot)
    sleep 60
    assert_equal 0, @@sv.lot_delete(lot), 'error deleting lot'
    # res = @@sv.lot_delete(lot)
    # assert $sm.change_sublottype(slt, 1)
    # assert_equal 0, res, "deletion of lot #{lot} failed"
    assert_equal lot, @@svback_user.lot_list(lot: lot).first, "lot #{lot} not found at backup site"
  end

  def test50_force_locate
    assert lot = @@svtest.new_lot, 'error creating test lot'
    @@testlots << lot
    assert_equal 0, do_at_backup_site(lot, @@sv_back, @@params) {
      @@svback_user.lot_hold(lot, @@hold_reason)
      @@svback_user.lot_opelocate(lot, @@rework_bak_ret, force: true)
      @@svback_user.lot_hold_release(lot, nil)
    }, "failed to force locate lot in backup processing"
    # cleanup
    @@sv.delete_lot_family(lot)
  end

  # lot actions at src and backup site
  def test60_virtual_actions
    lot = @@lot
    #
    # lot returned, on route (was: test60_production_route)
    assert @@sv.backup_prepare_send_receive(lot, @@sv_back, {backup_bank: @@nprobank, backup_opNo: @@backup_opNo})
    assert @@sv_back.backup_prepare_return_receive(lot, @@sv, {return_bank: @@nprobank_return})
    assert do_virtual_actions(lot, @@svback_user, false, @@at_backup_opNo), 'operations at backup site should fail'
    #
    # lot at backup site, on route (was: test61_backup_complete)
    assert @@sv.backup_prepare_send_receive(lot, @@sv_back, {backup_bank: @@nprobank, backup_opNo: @@backup_opNo})
    assert do_virtual_actions(lot, @@sv, false), 'operations at source site should fail while in backup'
    #
    # lot COMPLETED at backup, banked out at source site
    assert_equal 0, @@sv.backup_complete(lot, @@sv_back)
    assert_equal 'COMPLETED', @@svback_user.lot_info(lot).status, "failed to complete lot in backup"
    assert_equal 'Waiting', @@sv.lot_info(lot).status, "failed to do virtual return"
    # operations at source site are now allowed
    assert do_virtual_actions(lot, @@sv), 'operations at source site failed'
    #
    # ship at backup site
    do_lot_shipment(lot, @@svback_user)
    #
    # lot COMPLETE at source, ship at source site
    assert_equal 0, @@sv.lot_opelocate(lot, :last)
    do_lot_shipment(lot, @@sv)
    # check history at backup site
    refute_empty @@sv.backup_lot_operation_list(lot, @@backup_opNo, @@svback_user), 'no history found at backup site'
  end

  def test62_backup_complete_new_child
    assert lot = @@svtest.new_lot, 'error creating test lot'
    @@testlots << lot
    child = @@sv.lot_split(lot, 10)
    # send and receive split lot
    [child, lot].each {|l|
      assert @@sv.backup_prepare_send_receive(l, @@sv_back, {backup_bank: @@nprobank, backup_opNo: @@backup_opNo})
      count = (l == child) ? 10 : 15
      assert_equal count, @@svback_user.lot_info(l).nwafers, 'wrong wafer count'
    }
    child1 = @@svback_user.lot_split(lot, 5)
    child2 = @@svback_user.lot_split(child, 5)
    carrier1 = @@svback_user.lot_info(child1).carrier
    assert_equal 0, @@svback_user.wafer_sort(child2, carrier1, slots: 'empty'), 'wafer_sort error'
    assert_equal 0, @@svback_user.lot_merge(child1, child2), 'failed to merge in backup'
    # sync the lot family
    assert_equal 0, @@sv.backup_lot_family_sync(lot, @@svback_user), 'failed to sync family'
    # complete and ship in backup
    [child1, child, lot].each {|l|
      assert_equal 0, @@sv.backup_complete(l, @@svback_user), 'error completing lot'
      do_lot_shipment(l, @@svback_user)
    }
    # ship at source
    [child1, child, lot].each {|l|
      assert_equal 0, @@sv.lot_opelocate(l, :last), 'error locating lot'  # Autobankin
      do_lot_shipment(l, @@sv)
    }
  end

  # can register PSM at source site even if Lot is in Backup Processing and execute later
  def test70_psm_after_return
    assert lot = @@svtest.new_lot, 'error creating test lot'
    @@testlots << lot
    assert_equal 0, do_at_backup_site(lot, @@sv_back, @@params) {
      @@sv.lot_experiment(lot, 2, @@sub_route2, @@psm_split_op, @@psm_merge_op)
    }, "failed to force lot in backup processing"
    # execute PSM
    assert_equal 0, @@sv.lot_opelocate(lot, @@psm_split_op)
    child = @@sv.lot_family(lot)[-1]
    refute_equal lot, child, "experiment failed"
    [lot, child].each { |l| @@sv.lot_opelocate(l, @@psm_merge_op) }
    assert_equal 0, @@sv.lot_merge(lot, child), "failed to merge experiment"
    # cleanup
    @@sv.delete_lot_family(lot)
  end

  # can do PSM at at backup site for child lot only (without parent)
  def test71_psm_child_at_backup
    assert lot = @@svtest.new_lot, 'error creating test lot'
    @@testlots << lot
    params = @@params.merge(src_carrier: @@sv.lot_info(lot).carrier)
    child = @@sv.lot_split(lot, 10)
    assert do_at_backup_site(child, @@sv_back, params) {
      assert_equal 0, @@sv_back.lot_experiment(child, 1, @@psm_bak_route, @@psm_bak_split_op, @@psm_bak_return_op), "Failed to setup PSM"
      assert_equal 0, @@sv_back.lot_opelocate(child, @@psm_bak_split_op), "Failed to do PSM split"
      sublot = @@sv_back.lot_family(child)[-1]
      [child, sublot].each { |l| assert_equal 0, @@sv_back.lot_opelocate(l, @@psm_bak_return_op), "Failed to locate #{l} to return" }
      assert_equal 0, @@sv_back.lot_merge(child, sublot), 'error merging lots'
      @@sv_back.lot_experiment_delete(child)
    }, "failed to force lot in backup processing"
  end

  def test80_fpc_inbackup
    skip "skipped FPC tests" unless @@test_fpc
    skip "backup site not configured for this" unless @@sv_back.environment_variable[1]['SP_FPC_ADAPTATION_FLAG'] == '1'
    assert_equal 0, @@svback_user.eqp_cleanup(@@fpc_eqp), "failed to clean up #{@@fpc_eqp}"
    params = {return_bank: @@nprobank_return, backup_bank: @@nprobank, backup_opNo: @@backup_opNo}
    assert lot = @@svtest.new_lot, 'error creating test lot'
    @@testlots << lot
    assert do_at_backup_site(lot, @@sv_back, params) {
      assert @@svback_user.fpc_update(lot, @@fpc_op, eqp: @@fpc_eqp, mrecipe: @@fpc_recipe, reticle: @@fpc_reticle, wafers: 2), 'failed to setup FPC'
      assert_equal 0, @@svback_user.lot_opelocate(lot, @@fpc_op), "failed to locate to FPC operation"
      child = @@svback_user.lot_family(lot)[-1]
      assert_equal 0, @@svback_user.lot_gatepass(lot), "failed to gate pass parent"
      # make sure reticle is loaded
      r_info = @@svback_user.reticle_status(@@fpc_reticle).reticleStatusInfo
      @@svback_user.reticle_status_change(@@fpc_reticle, 'AVAILABLE') unless r_info.reticleStatus == 'AVAILABLE'
      @@svback_user.reticle_xfer_status_change(@@fpc_reticle, "EI", eqp: @@fpc_eqp) unless r_info.equipmentID.identifier == @@fpc_eqp && r_info.transferStatus == "EI"
      assert @@svback_user.claim_process_lot(child, eqp: @@fpc_eqp), 'error procesing child lot'
      @@svback_user.lot_merge(lot, child).zero?
    }, 'failed'
    # cleanup
    @@sv.delete_lot_family(lot)
  end


  # aux methods

  # Send a lot to the backup site, execute the block and return the lot to the source site.
  # returns result of the block, nil if something goes wrong
  def do_at_backup_site(lot, backup_sv, params={}, &blk)
    @@sv.backup_prepare_send_receive(lot, backup_sv, params) || return
    # call block (equivalent to yield)
    ret = block_given? ? blk.call(lot) : nil
    # return lot to source site
    backup_sv.backup_prepare_return_receive(lot, @@sv, params) || return
    # return result of the block
    return block_given? ? ret : true
  end

  # operations after virtual return, for test60
  def do_virtual_actions(lot, sv=@@sv, is_allowed=true, future_op=@@skip_opno_next)
    if is_allowed
      _rc  = 0
      _rc2 = 0
      _rc3 = 0
      _rc4 = 0
      _rc5 = 0
      _rc6 = 0
      _rc7 = 0
    else
      _rc  = 1247
      _rc2 = 932
      _rc3 = 303
      _rc4 = 345
      _rc5 = 1429
      _rc6 = 356
      _rc7 = 2860
    end
    # NonProbanking now returns 1247, MSR732754
    assert_equal _rc, sv.lot_nonprobankin(lot, @@nprobank), 'TxNonProBankInReq'
    assert_equal _rc6, sv.lot_nonprobankout(lot), 'TxNonProBankOutReq'
    #
    assert_equal _rc, sv.schdl_change(lot, priorityclass: 4), 'TxLotSchdlChangeReq'
    assert_equal 0, sv.lot_mfgorder_change(lot, sublottype: 'QX'), 'TxLotMfgOrderChangeReq'
    assert_equal _rc2, sv.lot_requeue(lot), 'TxLotRequeueReq'
    # lot is moving to the 2nd route
    org_route = sv.lot_info(lot).route
    assert_equal _rc, sv.schdl_change(lot, product: @@product2, route: @@route2, opNo: @@skip_opno), 'TxLotSchdlChangeReq'
    if _rc == 0
      assert_equal @@route2, sv.lot_info(lot).route, 'schedule change failed'
      assert_equal 0, sv.schdl_change(lot, product: @@product, route: @@route, opNo: @@skip_opno_next)
    end
    #
    child = sv.lot_split(lot, 5) # with no hold
    assert_equal _rc3, sv.rc, 'TxSplitWaferLotReq'
    assert_equal _rc, sv.lot_merge(lot, child), 'TxMergeWaferLotReq' if child
    #
    if is_allowed
      assert_equal 0, sv.lot_hold(lot, @@hold_reason), "failed to set lot on hold"
    end
    child = sv.lot_split(lot, 5, onhold: true) # with hold inherit
    assert_equal _rc, sv.rc, "TxSplitWaferLotWithoutHoldReleaseReq"
    assert_equal _rc, sv.lot_merge(lot, child), "TxMergeWaferLotReq" if child
    #
    #assert sv.lot_hold(lot, @@hold_reason)
    child = sv.lot_split(lot, 5, onhold: true, release: true) # with hold release
    assert_equal _rc4, sv.rc, "TxSplitWaferLotWithHoldReleaseReq"
    assert_equal _rc, sv.lot_merge(lot, child), "TxMergeWaferLotReq" if child
    #
    assert_equal _rc, sv.lot_hold(lot, @@hold_reason)
    if is_allowed
      assert_equal 0, sv.lot_hold_release(lot, nil), 'hold release failed'
    end
    #
    # assert_equal 0, sv.lot_opelocate(lot, @@skip_opno_next), "error locating lot (#{sv.inspect})"
    child = sv.lot_partial_rework(lot, 5, @@rework_route)  # with hold inherit | hold release | no hold
    assert_equal _rc5, sv.rc, "TxPartialReworkReq"
    assert_equal _rc, sv.lot_partial_rework_cancel(lot, child), "TxPartialReworkCancelReq" if child
    if is_allowed
      assert_equal 0, sv.lot_hold(lot, @@hold_reason), "failed to prepare lot hold"
    end
    child = sv.lot_partial_rework(lot, 5, @@rework_route, onhold: true) # with hold inherit
    assert_equal _rc5, sv.rc, "TxPartialReworkWithoutHoldReleaseReq"
    assert_equal _rc, sv.lot_partial_rework_cancel(lot, child), "TxPartialReworkCancelReq" if child
    child = sv.lot_partial_rework(lot, 5, @@rework_route, onhold: true, release: true) # with hold inherit | hold release | no hold
    assert_equal _rc4, sv.rc, "TxPartialReworkWithHoldReleaseReq"
    assert_equal _rc, sv.lot_partial_rework_cancel(lot, child), "TxPartialReworkCancelReq" if child
    if is_allowed
      assert_equal 0, sv.lot_hold_release(lot, @@hold_reason), "failed to release lot hold"
    end
    assert_equal _rc5, sv.lot_rework(lot, @@rework_route), "TxReworkReq"
    assert_equal _rc, sv.lot_rework_cancel(lot), "TxReworkWholeLotCancelReq" if _rc == 0 # no need to check if rework was not executed
    assert_equal _rc5, sv.lot_opelocate(lot, @@skip_opno), "TxOpeLocateReq"
    assert_equal _rc5, sv.lot_branch(lot, @@sub_route), "TxBranchReq"
    assert_equal _rc5, sv.lot_branch_cancel(lot), "TxSubRouteBranchCancelReq"
    assert_equal 0, sv.lot_futurehold(lot, future_op, nil)
    assert_equal _rc5, sv.lot_gatepass(lot), "TxGatePassReq"
    if is_allowed
      assert_equal _rc, sv.lot_hold_release(lot, nil) # with Pre1-script using locate or skip
    else
      assert_equal 0, sv.lot_futurehold_cancel(lot, nil)  # cancel remaining future holds
    end
    assert_equal _rc, sv.scrap_wafers(lot), "TxScrapWaferReq"
    assert_equal _rc7, sv.scrap_wafers_cancel(lot), "TxScrapWaferCancelReq"
    return true
  end

  # for test61 and test62
  def do_lot_shipment(lot, sv)
    sv.lot_bank_move(lot, @@ship_bank)
    assert_equal 0, sv.lot_ship(lot, @@ship_bank), 'failed to do virtual shipment'
    assert_equal 0, sv.lot_ship_cancel(lot), 'failed to cancel virtual shipment'
    assert_equal 'COMPLETED', sv.lot_info(lot).status, 'lot status should be completed'
    # sv.wafer_sort(lot, '') # avoid the ship cancel error
    assert_equal 0, sv.lot_ship(lot, @@ship_bank), 'failed to do virtual shipment'
    assert_equal 'SHIPPED', sv.lot_info(lot).status, 'lot status should be shipped'
  end

  # for test04
  def verify_lot_attributes(lot)
    $log.info "verify_lot_attributes #{lot.inspect}"
    ret = true
    lcs1 = @@sv.lot_comments(lot)
    lcs2 = @@sv_back.lot_comments(lot)
    ($log.warn '  wrong # of lot comments'; ret = false) if lcs2.count != lcs1.count  # is actually 1
    if ret
      lcs1.each_index {|i|
        # timstamp is not set correctly, see MSR 882534
        ['lot', 'title', 'desc', 'user'].each {|k| ($log.warn "  wrong lot comment #{k}"; ret = false) if lcs1[i][k] != lcs2[i][k]}
      }
    end
    ($log.warn '  wrong LOT_LABEL'; ret = false) if @@sv_back.user_data(:lot, lot)['LOT_LABEL'] != @@sv.user_data(:lot, lot)['LOT_LABEL']
    lns1 = @@sv.lot_notes(lot)
    lns2 = @@sv_back.lot_notes(lot)
    ($log.warn '  wrong # of lot notes'; ret = false) if lns2.count != lns1.count
    if ret
      lns1.each_index {|i|
        # user is different, new issue ? tests have been wrong  sf 2018-09-27
        ['lot', 'title', 'desc', 'timestamp'].each {|k| ($log.warn "  wrong lot note #{k}"; ret = false) if lns1[i][k] != lns2[i][k]}
      }
    end
    return ret
  end

  # for test07
  def do_split_actions(svuser, lot, rework_route, rework_op, rework_ret, branch_route=nil, psm_split_op=nil, psm_return_op=nil, psm_merge_op=nil)
    children = []
    # Normal split
    children << svuser.lot_split(lot, 1)
    # Onhold split
    assert_equal 0, svuser.lot_hold(lot, @@hold_reason), 'error setting lot hold'
    # MSR717973: SplitWithoutLotHoldRelease
    children << svuser.lot_split(lot, 1, onhold: true)
    children << svuser.lot_split(lot, 1, onhold: true, release: true)
    # Auto split
    children << svuser.AMD_WaferSorterAutoSplitReq(lot, 1)
    # MSR717973: PSM in backup
    if branch_route
      assert_equal 0, svuser.lot_experiment(lot, 1, branch_route, psm_split_op, psm_return_op, merge: psm_merge_op), 'failed to setup PSM'
      2.times {|i|
        if i > 0
          assert_equal 0, svuser.lot_experiment_reset(lot, psm_split_op), 'failed to reset PSM'
        end
        assert_equal 0, svuser.lot_opelocate(lot, psm_split_op), 'failed to do PSM split'
        sublot = svuser.lot_family(lot)[-1]
        children << sublot
        [lot,sublot].each {|lot| assert_equal 0, svuser.lot_opelocate(lot, psm_return_op), 'error locating lot'}
        assert_equal 0, svuser.lot_merge(lot, sublot), 'error merging lots'
      }
      assert svuser.lot_experiment_delete(lot), 'error deleting PSM'
    end
    # MSR717973: Split on bank -> NA
    # FPC --> extra test
    # Partial Rework
    assert_equal 0, svuser.lot_opelocate(lot, rework_op), 'error locating lot'
    child = svuser.lot_partial_rework(lot, 1, rework_route, return_opNo: rework_ret)
    children << child
    assert_equal 0, svuser.lot_partial_rework_cancel(lot, child), 'rework cancel error'
    # Partial Rework w/o hold release
    assert_equal 0, svuser.lot_hold(lot, @@hold_reason), 'error setting lot hold'
    child = svuser.lot_partial_rework(lot, 1, rework_route, return_opNo: rework_ret, onhold: true)
    children << child
    assert_equal 0, svuser.lot_partial_rework_cancel(lot, child), 'rework cancel error'
    # Partial Rework with hold release
    child = svuser.lot_partial_rework(lot, 1, rework_route, return_opNo: rework_ret, onhold: true, release: true)
    children << child
    assert_equal 0, svuser.lot_partial_rework_cancel(lot, child), 'rework cancel error'
    # Merge
    svuser.merge_lot_family(lot, nosort: true)
    return children
  end

  # for test08 and test09
  def lotid_sequence_test(customer, params={})
    offset = params[:offset] || 1
    assert lot = @@svtest.new_lot(customer: customer), 'error creating test lot'
    @@testlots << lot
    assert_equal 0, @@sv.lot_opelocate(lot, @@backup_opNo)
    children = []
    # splits at the backup site
    assert do_at_backup_site(lot, @@sv_back, @@params) {
      30.times {
        child = @@svback_user.lot_split(lot, 1)
        @@svback_user.lot_merge(lot, child)
        children << child
      }
    }
    assert_equal 30, children.count, 'not enough children'
    # splits at the source site
    30.times {
      child = @@svsrc_user.lot_split(lot, 1)
      @@svsrc_user.lot_merge(lot, child)
      children << child
    }
    assert_equal 60, children.count, 'not enough children'
    $log.info "#{children.inspect}"
    # qualcomm lots are "numeric"
    children.each_with_index {|c, i|
      splitid = c.split('.')[1]
      assert_equal i+offset, splitid.to_i, "wrong child id #{c}"
    }
    return nil
  end

  # for test16
  def verify_lot_family_insync(lot_family)
    $log.info "verify_lot_family_insync #{lot_family}"
    # check bak lots equal source lots (exclude empty lots)
    res = true
    lf = @@sv.lot_family(lot_family, raw: true).collect {|e| e.lotID.identifier}
    blf = @@sv_back.lot_family(lot_family, raw: true).collect {|e| e.lotID.identifier}
    @@sv_back.lots_info(blf).each {|bli|
      next if lf.include?(bli.lot) || bli.status == 'EMPTIED'
      $log.warn "lot #{bli.lot} not found at source site"
      res = false
    }
    assert res, 'missing child lot'
    # check src lots equal backup lots (exclude empty lots)
    lf.each {|lot|
      li = @@sv.lot_info(lot, backup: true, wafers: true) # we need to use actual lot info tx here
      if blf.include?(lot)
        _binfo = @@sv_back.lot_info(lot, wafers: true) # we need to use actual lot info tx here
        if ['SCRAPPED', 'EMPTIED'].member?(_binfo.status)
          assert_equal li.states['Lot State'], _binfo.states['Lot State'], "wrong lot state for #{lot}"
          assert_equal li.states['Lot Finished State'], _binfo.states['Lot Finished State'], "wrong finished state for #{lot}"
        end
        _bwafers = _binfo.wafers.sort {|a, b| a.wafer <=> b.wafer }
        li.wafers.sort {|a, b| a.wafer <=> b.wafer}.each_with_index {|w, i|
          assert_equal _bwafers[i].wafer, w.wafer, 'wrong wafer id'
          assert_equal _bwafers[i].alias, w.alias, 'wrong wafer alias'
          # slot is known to be different
        }
        assert_equal li.product, _binfo.product, "wrong product for #{lot}"
      else
        next if li.status == 'EMPTIED' || !li.backup.backup_proc
        fail "lot #{lot} not found at backup site"
      end
    }
    return true
  end

end
