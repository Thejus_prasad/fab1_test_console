=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: sfrieske, 2017-05-09

History:
  2019-12-05 sfrieske, minor cleanup, renamed from Test_LotVirtualOperation
=end

require 'SiViewTestCase'

# Test lot list and processing at a virtual operation, MES-3436, MSR 315242
class MM_VirtualOperation < SiViewTestCase
  @@eqp = 'UTC001'
  @@e10_ok = ['1PRD', '2WPR']
  @@e10_fail = ['2NDP', '4DLY', '5DLY']
  @@testlots = []


  def self.shutdown
    @@sv.eqp_cleanup(@@eqp, mode: 'Off-Line-1')
  end

  def self.after_all_passed
    @@testlots.each {|lot| @@sv.delete_lot_family(lot)}
  end


  def test00_setup
    $setup_ok = false
    #
    # create test lot
    assert lot = @@svtest.new_lot, 'error creating test lot'
    @@testlots << lot
    # set PD UDATA
    li = @@sv.lot_info(lot)
    assert @@sm.update_udata(:pd, li.op, {'VirtualOperationAvailable'=>'Yes'}), 'SM setup error'
    assert li.opEqps.include?(@@eqp), 'wrong eqp'
    #
    $setup_ok = true
  end

  def test11_e10
    assert_equal 0, @@sv.eqp_cleanup(@@eqp, mode: 'Off-Line-1')
    lot = @@testlots[0]
    li = @@sv.lot_info(lot)
    @@e10_ok.each {|stat|
      assert_equal 0, @@sv.eqp_status_change(@@eqp, stat), "error changing E10 state to #{stat}"
      assert cj = @@sv.eqp_opestart(@@eqp, li.carrier), 'error in opestart'
      assert_equal 0, @@sv.eqp_opestart_cancel(@@eqp, cj), 'error in opstart_cancel'
    }
    @@e10_fail.each {|stat|
      assert_equal 0, @@sv.eqp_status_change(@@eqp, stat), "error changing E10 state to #{stat}"
      refute @@sv.eqp_opestart(@@eqp, li.carrier), 'error in opestart'
    }
  end

  def test12_port_down
    assert_equal 0, @@sv.eqp_cleanup(@@eqp, mode: 'Auto-1', portstatus: 'Down')
    lot = @@testlots[0]
    li = @@sv.lot_info(lot)
    assert cj = @@sv.eqp_opestart(@@eqp, li.carrier), 'error in opestart'
    assert_equal 0, @@sv.eqp_opestart_cancel(@@eqp, cj), 'error in opestart_cancel'
  end

  def testdev21_cj  # MES-3452, MSR 1322917
    assert_equal 0, @@sv.eqp_cleanup(@@eqp, mode: 'Auto-1')
    lot = @@testlots[0]
    li = @@sv.lot_info(lot)
    assert cj = @@sv.eqp_opestart(@@eqp, li.carrier), 'error in opestart'
    refute_empty @@sv.lot_info(lot).cj, 'missing cj in lot_info'
    refute_empty @@sv.eqp_info(@@eqp).cjs, 'missing cjs in eqp_info'  # fails
  end

  # test for MES-3650, error if exactly this parameter set is passed: lot: '', status: <any>, virtualop: true (lot: '%' works)
  def test31_lot_status_virtualop
    assert @@sv.lot_list(lot: '', status: 'Waiting', virtualop: true), 'error in lot_list with empty lot parameter'
  end

end
