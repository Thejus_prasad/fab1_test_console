=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: Daniel Steger, 2013-03-27

History:
  2013-07-17 ssteidte, minor fixes
  2014-11-10 ssteidte, no fixed test lot anymore
  2017-05-03 sfrieske, added test31 for MSR 871333
  2017-11-20 sfrieske, use struct_to_hash instead of Struct.to_hash
  2018-12-12 sfrieske, minor cleanup
  2019-12-05 sfrieske, minor cleanup, renamed from Test096_HoldClaimMemo
  2020-08-10 sfrieske, improved use of lot_operation_history
=end

require 'SiViewTestCase'


# Test MSRs 546117, 750161 for lot hold claim memo update
class MM_Lot_HoldClaimMemo < SiViewTestCase
  @@nonprobank = 'UT-NONPROD'
  @@sleeptime = 40  # for history update
  @@testlots = []


  def self.after_all_passed
    @@testlots.each {|lot| @@sv.delete_lot_family(lot)}
  end


  def test00_prepare
    $setup_ok = false
    #
    assert @@svnox = SiView::MM.new($env, user: 'NOX-UNITTEST', password: :default)
    assert lots = @@svtest.new_lots(3), 'error creating test lots'
    @@testlots += lots
    #
    $setup_ok = true
  end


  def test10_lothold_memoupdate
    lot = @@testlots[0]
    assert_equal 0, @@sv.lot_cleanup(lot)
    assert_equal 0, @@sv.lot_hold(lot, nil, memo: "QA_TEST#{Time.now.iso8601}"), 'failed to set lot hold'
    set_compare_history(lot)
  end

  def test11_lothold_nomemo
    lot = @@testlots[0]
    assert_equal 0, @@sv.lot_cleanup(lot)
    assert_equal 0, @@sv.lot_hold(lot, nil), 'failed to set lot hold'
    set_compare_history(lot)
  end

  def test12_futurehold_memoupdate
    lot = @@testlots[0]
    assert_equal 0, @@sv.lot_cleanup(lot)
    assert opNo_future = @@sv.lot_operation_list(lot)[2].opNo
    assert_equal 0, @@sv.lot_futurehold(lot, opNo_future, nil), 'failed to set future hold'
    assert_equal 0, @@sv.lot_opelocate(lot, opNo_future), 'failed to locate lot'
    refute_empty @@sv.lot_hold_list(lot, type: 'FutureHold'), 'lot is not ONHOLD'
    set_compare_history(lot)
  end

  def test13_bankhold_memoupdate
    lot = @@testlots[0]
    assert_equal 0, @@sv.lot_cleanup(lot, opNo: :last)
    assert_equal 0, @@sv.lot_bankhold(lot, memo: "QA_TEST#{Time.now.iso8601}"), 'failed to set bankhold'
    set_compare_history(lot)
  end

  def test14_nonprobankhold_memoupdate
    lot = @@testlots[0]
    assert_equal 0, @@sv.lot_cleanup(lot)
    assert_equal 0, @@sv.lot_nonprobankin(lot, @@nonprobank, memo: "QA_TEST#{Time.now.iso8601}"), 'failed to nonpro bankin'
    set_compare_history(lot)
  end

  def test21_other_user
    lot = @@testlots[0]
    assert_equal 0, @@sv.lot_cleanup(lot)
    assert_equal 0, @@sv.lot_hold(lot, nil), 'failed to set lot hold'
    memo = "QA_TEST#{Time.now.iso8601}"
    # verify 2nd user can update
    assert_equal 0, @@svnox.lot_holdmemo_update(lot, memo)
    # verify both users can read the memo
    $log.info "waiting #{@@sleeptime} s for history"; sleep @@sleeptime
    assert_equal memo, @@sv.lot_holdmemo_history(lot).first.memo
    assert_equal memo, @@svnox.lot_holdmemo_history(lot).first.memo
    # verify 1st user can still release the hold
    assert_equal 0, @@sv.lot_hold_release(lot, nil)
  end

  # 2 holds with same reason and timestamp must be updated separately, MSR 750161 in C6.21
  def test22_2_holds
    lot = @@testlots[1]
    # assert lot = @@svtest.new_lot, 'error creating test lot'
    # @@testlots << lot
    assert_equal 0, @@sv.lot_hold(lot, nil, memo: 'U1_0'), 'failed to set lot hold'
    assert_equal 0, @@svnox.lot_hold(lot, nil, memo: 'U2_0'), 'failed to set lot hold'
    assert lc = @@sv.lot_split(lot, 1, onhold: true), "error splitting lot #{lot}"
    memo_sv = 'QA_TEST_U1'
    memo_svnox = 'QA_TEST_U2'
    # verify other user can update the child memo
    assert_equal 0, @@svnox.lot_holdmemo_update(lc, memo_svnox, user: @@sv.user)
    assert_equal 0, @@sv.lot_holdmemo_update(lc, memo_sv, user: @@svnox.user)
    # verify both users can read the memo
    $log.info "waiting #{@@sleeptime} s for history"; sleep @@sleeptime
    [@@sv.user, @@svnox.user].each {|user|
      assert hist = @@sv.lot_holdmemo_history(lc, user: user)
      assert_equal 4, hist.size, 'wrong holdmemo history size'  # 2 original memos + 1 update per user
      assert_equal memo_sv, @@sv.lot_holdmemo_history(lc, user: user).first.memo
      assert_equal memo_sv, @@svnox.lot_holdmemo_history(lc, user: user).first.memo
    }
    # verify original user can still release his hold
    assert_equal 0, @@sv.lot_hold_release(lc, nil, user: @@sv.user)
    assert_equal 0, @@svnox.lot_hold_release(lc, nil, user: @@svnox.user)
  end

  # MSR 871333
  def test31_memo
    lot = @@testlots[2]
    # assert lot = @@svtest.new_lot, 'error creating test lot'
    # @@testlots << lot
    assert child = @@sv.lot_split(lot, 5)
    assert_equal 0, @@sv.lot_hold(lot, nil, memo: 'QA1')
    assert reason = @@sv.lot_hold_list(lot).first.reason
    assert_equal 0, @@sv.lot_hold(child, reason, memo: 'QA2')
    assert_equal 10324, @@sv.lot_merge(lot, child)
    assert_equal 0, @@sv.lot_holdmemo_update(lot, 'QA2', reason: reason)
    assert_equal 0, @@sv.lot_merge(lot, child)
  end


  # aux methods

  def set_compare_history(lot)
    holds = @@sv.lot_hold_list(lot)
    assert_equal 1, holds.count, 'wrong number of holds'
    # update the memo
    tstart = Time.now
    10.times {|i|
      sleep 1 # for better readability
      memo = "QA_TEST#{Time.now.iso8601}"
      assert_equal 0, @@sv.lot_holdmemo_update(lot, memo)
      hh = @@sv.lot_hold_list(lot)
      assert_equal 1, hh.count, 'wrong number of holds'
      assert_equal memo, hh.first.memo, 'wrong memo'
      holds << hh.first
    }
    $log.info "waiting #{@@sleeptime} s for history"; sleep @@sleeptime
    records = @@sv.lot_holdmemo_history(lot).sort {|a, b| a.timestamp <=> b.timestamp}
    assert_equal holds.count, records.count, 'wrong number of history entries'
    records.each_with_index {|h, i|
      fields = [:type, :reason, :related_lot, :user, :post, :single, :memo]
      fields += [:rsp_route, :rsp_op] if h.type != 'BankHold'  # not set for bank holds
      fields.each {|f|
        if exp = holds[i][f]
          assert_equal exp, h[f], "wrong hold history entry\nexpected: #{holds[i]}\ngot: #{h}"
        else
          assert_nil h[f], "wrong hold history entry\nexpected: #{holds[i]}\ngot: #{h}"
        end
      }
    }
    li = @@sv.lot_info(lot)
    history = @@sv.lot_operation_history(lot, opNo: li.opNo, raw: true).select {|e|
      @@sv.siview_time(e.reportTimeStamp) > tstart && e.operationCategory == 'HoldMemoUpdate'
    }
    assert_equal history.count, records.count - 1, 'wrong number of operation history entries for memo update'
  end

end
