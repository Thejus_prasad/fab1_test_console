=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: Daniel Steger, 2016-02-26

History
  2016-02-10 dsteger,  remove copy on vendor lot receive MES-2121
  2016-07-21 sfrieske, cleaned up test preparation to use SM UDATA only
  2017-04-25 sfrieske, fixed enum_lot_labels, more cleanup
  2019-12-05 sfrieske, minor cleanup, renamed from Test081_CustomLotNumber
  2021-01-15 sfrieske, moved SiView::MM#new_srclot_for_product to SiView::Test#new_srclot

Notes:
  TODO: merge with Lot_Label and Lot_Udata?
=end

require 'SiViewTestCase'


# Test Customer Lot ID (MSR938719 (BSM) (MES-2721: Fab1)), there is an additional test for ULL
class MM_Lot_CustomLotNumber < SiViewTestCase
  @@foundry = 'H'
  @@split_loops = 34  # 34 - overflow

  @@zero_bank_product = 'UT-PRODUCT-ZERO.01'
  @@fab_product = 'UT-PRODUCT-FAB.01'
  @@bump_product = 'UT-PRODUCT-BUMP.01'
  @@sort_product = 'UT-PRODUCT-SORT.01'
  @@assembly_product = 'UT-PRODUCT-ASSEMBLY.01'
  @@customer = 'qgt-bsm'
  @@prefixes = {'BUMP'=>'X', 'SORT'=>'0X'}
  @@start_bank = 'UT-RAW'

  @@hold_reason = 'AEHL'

  @@rework_route = 'UTRW001.01'
  @@rework_opNo = '1000.400'

  @@psm_route = 'UTBR001.01'
  @@psm_split_opNo = '1000.200'
  @@psm_return_opNo = '1000.300'

  @@fpc_product = 'UT-PROD-FPC.01'
  @@fpc_eqp = 'UTR001'
  @@fpc_mrecipe = 'P.UTMR000.01'
  @@fpc_opNo = '1000.200'
  @@fpc_mgr_opNo = '1000.300'

  @@testlots = []


  class CustomerLotNumber
    attr_accessor :sv, :customer, :prefixes, :run_seq, :pattern

    def initialize(sv, customer, foundry, run_seq, allowed_chars, prefixes)
      @sv = sv
      @customer = customer
      @prefixes = prefixes
      @run_seq = run_seq
      lotidformat = @sv.user_data(:customer, @customer)['CustomLIG_LotIdFormat'] || ''
      lotidformat.sub!('$CFoundryPrefix', foundry)
      lotidformat.sub!('$CYearCode', "(?<year_code>[0-9A-Z]{1})")
      lotidformat.sub!('$CWorkWeek', "(?<work_week>[0-9]{2})")
      lotidformat.sub!('$CRunSeq', "(?<run_seq>[#{allowed_chars}]{#{@run_seq}})")
      @pattern = lotidformat
    end

    # determine regular matching pattern for specific layer
    def reg_pattern(mfg_layer, lot, is_bumped: true)
      lotidformat = @pattern
      lotidformat = lotidformat.sub('$CFoundryLotId', lot)
      # Override rules (defaults to 0 with length of mfg prefixes)
      if mfg_layer == 'SORT' && !is_bumped
        lotidformat = lotidformat.sub("$CPrefix_BUMP", "0" * @prefixes['BUMP'].length)
      elsif mfg_layer == 'BUMP' || mfg_layer == 'RAW' || mfg_layer == @prod_layer
        lotidformat = lotidformat.sub("$CPrefix_SORT", "0" * @prefixes['SORT'].length)
      end
      # substitute rules
      @prefixes.each_pair {|prefix, data|
        lotidformat = lotidformat.sub("$CPrefix_#{prefix}", data)
        lotidformat = lotidformat.sub("$CSplit_#{prefix}", "(?<split_#{prefix}>[0-9A-Z]{1})")
      }
      return Regexp.new(lotidformat)
    end

    # returns an array of lot labels that are altered in one position each
    def enum_lot_labels(lot, lot_label, mfg_layer, is_bumped: true)
      lot_pattern = reg_pattern(mfg_layer, lot, is_bumped: is_bumped)
      if m = lot_pattern.match(lot_label)
        indices = m.names.select {|n| n.start_with?('split_') }.collect {|n| m.begin(n)}
        i_fablot = lot_label.index(lot)
        indices += (lot_label.index('.', i_fablot) +1 .. i_fablot + lot.length-1).to_a
        lot_label.length.times.reject {|i| indices.include?(i)}.collect {|i|
          start = i > 0 ? lot_label[0..i-1] : ''
          replace_char = lot_label[i] == 'A' ? 'B' : 'A'
          start + replace_char + lot_label[i+1..-1]
        }
      else
        raise RuntimeError.new("Invalid label #{lot_label} for #{lot} (#{lot_pattern})")
      end
    end

    def next_split(fab_lot, lot_label, mfg_layer, is_bumped: true)
      lot_pattern = reg_pattern(mfg_layer, fab_lot, is_bumped: is_bumped)
      lot_pattern.match(lot_label) {|m|
        l = "split_#{mfg_layer}"
        if m.names.include?(l)
          lot_label[m.begin(l.to_sym)] = next_seq(m[l.to_sym])
        end
        return lot_label
      }
      raise RuntimeError.new("Invalid label #{lot_label} for #{fab_lot} (#{lot_pattern})")
    end

    def next_seq(char)
      a = char
      return 'A' if a == '9'
      begin
        a.next!
      end while (a =~ /[IO]/)
      return a
    end

    def custom_lot_label_format(lot)
      blen = @prefixes['BUMP'].length
      slen = @prefixes['SORT'].length
      pos = 3
      str = "CBB$CFoundryPrefix[#{pos},1]"
      pos += 1
      str += "$CPrefix_BUMP[#{pos},#{blen}]"
      pos += blen
      str += "$CYearCode[#{pos},1]"
      pos += 1
      str += "$CWorkWeek[#{pos},2]"
      pos += 2
      str += "$CRunSeq[#{pos},3]."
      pos += 4
      str += "$CPrefix_SORT[#{pos},#{slen}]."
      pos += 1 + slen
      str += "$CSplit_BUMP[#{pos},1]"
      pos += 1
      str += "$CSplit_SORT[#{pos},1]\#"
      pos += 2
      str += "$CFoundryLotId[#{pos},#{lot.length}]"
      return str
    end

  end


  def self.startup
    super
    @@sv.user_data_delete(:customer, @@customer, @@prefixes.keys.collect {|p| "CustomLIG_Prefix_#{p}"})
    @@bsm_data = CustomerLotNumber.new(@@sv, @@customer, @@foundry, 3, '23456789ABCDEFGHJKLMNPQRSTUVWXYZ', @@prefixes)
  end

  def self.after_all_passed
    @@testlots.uniq.each {|lot| @@sv.delete_lot_family(lot)}
  end


  def setup
    assert @@sm.update_udata(:customer, @@customer, Hash[@@prefixes.each_pair.collect {|p, v| ["CustomLIG_Prefix_#{p}", v]}])
  end


  def test10_multiple_layers
    $setup_ok = false
    #
    # zero bank (???)
    assert zero_lot = @@svtest.new_lot(product: @@zero_bank_product), "failed to create new zero bank lot"
    @@testlots << zero_lot
    # fab layer
    move_to_start_bank(zero_lot)
    assert fab_lot = @@svtest.new_lot(product: @@fab_product, customer: @@customer, srclot: zero_lot), "failed to STB wafer lot"
    @@testlots << fab_lot
    assert_empty @@sv.lot_label(fab_lot), "wrong lot label for #{fab_lot}"
    # bump layer
    move_to_start_bank(fab_lot)
    bump_lot = new_bsmlayer_lot(fab_lot, @@bump_product, 'BUMP')
    # sort layer
    move_to_start_bank(bump_lot)
    sort_lot = new_bsmlayer_lot(bump_lot, @@sort_product, 'SORT')
    # assembly layer
    move_to_start_bank(sort_lot)
    assy_lot = new_bsmlayer_lot(sort_lot, @@assembly_product, 'ASSEMBLY')
    #
    $setup_ok = true
  end

  def test11_split_actions_bump_sort
    assert fab_lot = @@svtest.new_lot(product: @@fab_product, customer: @@customer), "failed to create test lot"
    @@testlots << fab_lot
    assert_empty @@sv.lot_label(fab_lot), "wrong lot label"
    # bump layer
    move_to_start_bank(fab_lot)
    bump_lot = new_bsmlayer_lot(fab_lot, @@bump_product, 'BUMP')
    lot_label = @@sv.lot_label(bump_lot)
    children = split_actions(bump_lot)
    children.each {|child, tx|
      lot_label = @@bsm_data.next_split(bump_lot, lot_label, 'BUMP')
      assert_equal lot_label, @@sv.lot_label(child), "wrong lot label for #{child} (#{tx})"
    }
    assert @@sv.merge_lot_family(bump_lot, nosort: true)
    # sort layer
    move_to_start_bank(bump_lot)
    sort_lot = new_bsmlayer_lot(bump_lot, @@sort_product, 'SORT')
    lot_label = @@sv.lot_label(sort_lot)
    children = split_actions(sort_lot)
    children.each {|child, tx|
      lot_label = @@bsm_data.next_split(sort_lot, lot_label, 'SORT')
      assert_equal lot_label, @@sv.lot_label(child), "wrong lot label for #{child} (#{tx})"
    }
    assert @@sv.merge_lot_family(sort_lot, nosort: true)
  end

  def test12_split_all_inherit
    assert fab_lot = @@svtest.new_lot(product: @@fab_product, customer: @@customer), "failed to create test lot"
    @@testlots << fab_lot
    assert_empty @@sv.lot_label(fab_lot), "wrong lot label"
    # bump layer
    move_to_start_bank(fab_lot)
    bump_lot = new_bsmlayer_lot(fab_lot, @@bump_product, 'BUMP')
    assert lot = @@sv.lot_split(bump_lot, 10), "failed to split #{bump_lot}"
    assert check_custom_lot_number(lot, 'BUMP', lot_family: bump_lot)
    lot_label = @@sv.lot_label(lot)
    children = split_merge_all(lot, max_seq: 32)
    children.each {|child|
      lot_label = @@bsm_data.next_split(bump_lot, lot_label, 'BUMP')
      assert_equal lot_label, @@sv.lot_label(child), "wrong lot label for #{child}"
    }
    # sort layer
    move_to_start_bank(lot)
    sort_lot = new_bsmlayer_lot(lot, @@sort_product, 'SORT', fab_lot: bump_lot)
    assert lot = @@sv.lot_split(sort_lot, 3), "failed to split #{bump_lot}"
    assert check_custom_lot_number(lot, 'SORT', lot_family: bump_lot)
    lot_label = @@sv.lot_label(lot)
    children = split_merge_all(lot, max_seq: 32)
    children.each {|child|
      lot_label = @@bsm_data.next_split(bump_lot, lot_label, 'SORT')  # yes, bump_lot!
      assert_equal lot_label, @@sv.lot_label(child), "wrong lot label for #{child}"
    }
  end

  def test13_split_all_sort_only
    sort_lot = new_bsmlayer_lot(nil, @@sort_product, 'SORT', is_bumped: false)
    lot_label = @@sv.lot_label(sort_lot)
    children = split_merge_all(sort_lot)
    children.each {|child|
      lot_label = @@bsm_data.next_split(sort_lot, lot_label, 'SORT', is_bumped: false)
      assert_equal lot_label, @@sv.lot_label(child), "wrong lot label for #{child}"
    }
  end

  def XXtest14_start_bump_lot # covered by test10
    bump_lot = new_bsmlayer_lot(nil, @@bump_product, 'BUMP')
    move_to_start_bank(bump_lot)
    new_bsmlayer_lot(bump_lot, @@sort_product, 'SORT')
  end

  def test15_start_children_in_sort_and_merge
    bump_lot = new_bsmlayer_lot(nil, @@bump_product, 'BUMP')
    move_to_start_bank(bump_lot)
    children = 24.times.collect {|i|
      assert child = @@sv.lot_split(bump_lot, 1)
      move_to_start_bank(child)
      new_bsmlayer_lot(child, @@sort_product, 'SORT', fab_lot: bump_lot)
    }
    sort_lot = new_bsmlayer_lot(bump_lot, @@sort_product, 'SORT')
    children.each {|lot| assert_equal 0, @@sv.lot_merge(sort_lot, lot), "failed to merge to parent"}
  end

  def test16_multiple_children_stbed
    assert fab_lot = @@svtest.new_lot(product: @@fab_product, customer: @@customer), "failed to create test lot"
    @@testlots << fab_lot
    assert_empty @@sv.lot_label(fab_lot), "wrong lot label"
    move_to_start_bank(fab_lot)
    children = 24.times.collect {|i|
      assert child = @@sv.lot_split(fab_lot, 1)
      ##move_to_start_bank(child)
      new_bsmlayer_lot(child, @@bump_product, 'BUMP')
    }
    bump_lot = new_bsmlayer_lot(fab_lot, @@bump_product, 'BUMP')
    # try to merge child lots
    children.each {|child| assert_equal 159, @@sv.lot_merge(bump_lot, child)}
  end

  def test17_copy_on_stb_from_vendor_lot
    assert srclot = @@svtest.new_srclot(product: @@bump_product)
    @@testlots << srclot
    udata = {'LOT_LABEL'=>"CBBHX609238.00.00\##{srclot}",
             'CustomLIG_LotIdFormatDetail'=>"CBB$CFoundryPrefix[3,1]$CPrefix_BUMP[4,1]$CYearCode[5,1]$CWorkWeek[6,2]$CRunSeq[8,3].$CPrefix_SORT[12,2].$CSplit_BUMP[15,1]$CSplit_SORT[16,1]\#$CFoundryLotId[18,#{srclot.length}]"
    }
    assert_equal 0, @@sv.user_data_update(:lot, srclot, udata), "failed to set lot label"
    assert child = @@sv.lot_split_notonroute(srclot, 1)
    @@testlots << child
    #
    assert lot = @@svtest.new_lot(srclot: child, product: @@bump_product, customer: @@customer, lotGenerationType: 'By Source Lot')
    @@testlots << lot
    check_custom_lot_number(lot, 'BUMP', lot_family: srclot)
    #
    assert lot = @@svtest.new_lot(srclot: srclot, product: @@bump_product, customer: @@customer, lotGenerationType: 'By Source Lot')
    @@testlots << lot
    check_custom_lot_number(lot, 'BUMP', lot_family: srclot)
  end

  def test20_merge_checks
    assert lot = @@svtest.new_lot(product: @@bump_product, customer: @@customer, lotGenerationType: 'By Source Lot')
    @@testlots << lot
    assert_match @@bsm_data.reg_pattern('BUMP', lot), @@sv.lot_label(lot)
    assert child = @@sv.lot_split(lot, 1)
    assert try_merge_wrong_labels(lot, child, 'BUMP') {@@sv.lot_merge(lot, child)}
  end

  def test21_merge_checks_notonroute
    assert srclot = @@svtest.new_srclot(product: @@bump_product)
    @@testlots << srclot
    udata = {'LOT_LABEL'=>"CBBH#{@@prefixes['BUMP']}609239.#{'0' * @@prefixes['SORT'].length}.00\##{srclot}",
             'CustomLIG_LotIdFormatDetail'=>@@bsm_data.custom_lot_label_format(srclot)}
    assert_equal 0, @@sv.user_data_update(:lot, srclot, udata), "failed to set lot label"
    assert child = @@sv.lot_split_notonroute(srclot, 1)
    assert try_merge_wrong_labels(srclot, child, 'RAW') {@@sv.lot_merge_notonroute(srclot, child)}
  end

  def test22_merge_checks_partial_rework
    assert lot = @@svtest.new_lot(product: @@bump_product, customer: @@customer, lotGenerationType: 'By Source Lot')
    @@testlots << lot
    assert_match @@bsm_data.reg_pattern('BUMP', lot), @@sv.lot_label(lot)
    assert_equal 0, @@sv.lot_opelocate(lot, @@rework_opNo), "failed to locate lot"
    assert child = @@sv.lot_partial_rework(lot, 1, @@rework_route)
    assert try_merge_wrong_labels(lot, child, 'BUMP') {@@sv.lot_partial_rework_cancel(lot, child)}
  end

  def test30_stbcancel
    # only works if started from vendor lot
    assert bump_lot = @@svtest.new_lot(product: @@bump_product, customer: @@customer, lotGenerationType: 'By Source Lot')
    @@testlots << bump_lot
    assert lot = @@sv.stb_cancel(bump_lot), "failed to cancel STB"
    @@testlots << lot
    assert_empty @@sv.lot_label(lot), "wrong lot label for #{lot}"
  end

  def test40_no_start_from_multiple_sources
    # create bump lot and split it
    assert bump_lot = @@svtest.new_lot(product: @@bump_product, customer: @@customer)
    @@testlots << bump_lot
    move_to_start_bank(bump_lot)
    assert bump_child = @@sv.lot_split(bump_lot, 1)
    refute_empty @@sv.lot_label(bump_lot)
    refute_empty @@sv.lot_label(bump_child)
    # schedule lot and try to STB with 2 srclots
    assert sort_lot = @@sv.new_lot_release(product: @@sort_product, customer: @@customer,
      lotGenerationType: 'By Source Lot', srclot: [bump_lot, bump_child])
    assert_nil @@sv.stb(sort_lot), 'STB must fail'
    assert_equal 10318, @@sv.rc, 'wrong error msg'
  end

  def xxtest50_fpc_by_wafer
    skip "FPC not enabled" unless @@sv.environment_variable[1]['SP_FPC_ADAPTATION_FLAG'] == '1'
    bump_lot = new_bsmlayer_lot(nil, @@bump_product, 'BUMP')
    lot_label = @@sv.lot_label(bump_lot)
    assert fpc_id = @@sv.fpc_update(bump_lot, @@fpc_opNo, wafers: 2, eqp: @@fpc_eqp, mrecipe: @@fpc_mrecipe), "failed to create FPC"
    assert_equal 0, @@sv.lot_opelocate(bump_lot, @@fpc_opNo), "failed to locate to FPC operation"
    child = @@sv.lot_family(bump_lot)[-1]
    child_lot_label = @@bsm_data.next_split(bump_lot, lot_label, 'BUMP')
    assert_equal child_lot_label, @@sv.lot_label(child), "wrong lot label for #{child}"
  end

  def test60_invalid_custom_prefix
    assert @@sm.update_udata(:customer, @@customer, {'CustomLIG_Prefix_BUMP'=>''})
    assert srclot = @@svtest.new_srclot(product: @@bump_product)
    @@testlots << srclot
    # schedule lot and try to STB
    assert lot = @@sv.new_lot_release(product: @@sort_product, customer: @@customer,
      lotGenerationType: 'By Source Lot', srclot: srclot)
    assert_nil @@sv.stb(lot), 'STB must fail'
    assert_equal 11210, @@sv.rc, 'wrong error msg, expected: Invalid UserData attribute'
  end

  def test61_missing_lotformatdetail_for_sourcelot
    assert srclot = @@svtest.new_srclot(product: @@bump_product)
    @@testlots << srclot
    assert_equal 0, @@sv.user_data_update(:lot, srclot, {'LOT_LABEL'=>"MyOwnLotLabel##{srclot}"})
    # schedule lot and try to STB
    assert lot = @@sv.new_lot_release(product: @@bump_product, customer: @@customer,
      lotGenerationType: 'By Source Lot', srclot: srclot)
    assert_nil @@sv.stb(lot), 'STB must fail'
    assert_equal 11210, @@sv.rc, 'wrong error msg, expected: Invalid UserData attribute'
  end


  # aux methods

  # create new BSM lot and verify the lot label
  def new_bsmlayer_lot(lot, product, mfg_layer, fab_lot: lot, is_bumped: true)
    assert newlot = @@svtest.new_lot(product: product, customer: @@customer, bysrclot: lot)  # lot may be nil
    @@testlots << newlot if newlot != lot
    assert check_custom_lot_number(newlot, mfg_layer, lot_family: fab_lot || newlot, is_bumped: is_bumped)
    return newlot
  end

  def move_to_start_bank(lot)
    assert_equal 0, @@sv.lot_opelocate(lot, :last), "failed to bankin lot"
    assert_equal 0, @@sv.lot_bankin(lot), "failed to bankin lot" if @@sv.lot_info(lot).status != 'COMPLETED'
    assert_equal 0, @@sv.lot_bank_move(lot, @@start_bank), "failed to move lot"
  end

  def check_custom_lot_number(lot, mfg_layer, lot_family: lot, is_bumped: true)
    $log.info "check_custom_lot_number #{lot}, #{mfg_layer}, #{lot_family}, #{is_bumped}"
    assert lot_label = @@sv.lot_label(lot), "failed to get lot label for #{lot}"
    assert @@sv.user_data(:lot, lot)['CustomLIG_LotIdFormatDetail'], "failed to get lot format details for #{lot}"
    assert_match @@bsm_data.reg_pattern(mfg_layer, lot_family, is_bumped: is_bumped), lot_label, "lot label does not match"
    return lot_label
  end

  def split_actions(lot)
    children = []
    # Normal split
    children << [@@sv.lot_split(lot, 1), @@sv.tx_id]
    # Onhold split
    assert_equal 0, @@sv.lot_hold(lot, @@hold_reason), "Hold failed"
    # MSR717973: SplitWithoutLotHoldRelease
    children << [@@sv.lot_split(lot, 1, onhold: true), @@sv.tx_id]
    # split with hold release
    children << [@@sv.lot_split(lot, 1, onhold: true, release: true), @@sv.tx_id]
    # Auto split
    children <<  [@@sv.AMD_WaferSorterAutoSplitReq(lot, 1), @@sv.tx_id]
    # PSM incl reset
    assert_equal 0, @@sv.lot_experiment(lot, 1, @@psm_route, @@psm_split_opNo, @@psm_return_opNo), "Failed to setup PSM"
    2.times {|i|
      assert_equal 0, @@sv.lot_experiment_reset(lot, @@psm_split_opNo), "failed to reset PSM" if i > 0
      assert_equal 0, @@sv.lot_opelocate(lot, @@psm_split_opNo), "Failed to do PSM split"
      sublot = @@sv.lot_family(lot)[-1]
      children << [sublot, 'PSM']
      # merge
      [lot, sublot].each {|lot| assert_equal 0, @@sv.lot_opelocate(lot, @@psm_return_opNo), "Failed to locate to return"}
      if @@sv.f7
        assert_equal 'EMPTIED', @@sv.lot_info(sublot).status, "#{sublot} should have been merged with parent automatically"
      else
        assert_equal 0, @@sv.lot_merge(lot, sublot), "failed to merge #{sublot} to #{lot}"
      end
    }
    assert @@sv.lot_experiment_delete(lot), "Failed to clear PSM"
    # Partial Rework
    assert_equal 0, @@sv.lot_opelocate(lot, @@rework_opNo), "Failed to locate to rework"
    assert child = @@sv.lot_partial_rework(lot, 1, @@rework_route)
    children << [child, @@sv.tx_id]
    assert_equal 0, @@sv.lot_partial_rework_cancel(lot, child), "Failed to merge after rework"
    # Partial Rework w/o hold release
    assert_equal 0, @@sv.lot_hold(lot, @@hold_reason), "Hold failed"
    assert child = @@sv.lot_partial_rework(lot, 1, @@rework_route, onhold: true)
    children << [child, @@sv.tx_id]
    assert_equal 0, @@sv.lot_partial_rework_cancel(lot, child), "Failed to merge after rework"
    # Partial Rework with hold release
    assert child = @@sv.lot_partial_rework(lot, 1, @@rework_route, onhold: true, release: true)
    children << [child, @@sv.tx_id]
    assert_equal 0, @@sv.lot_partial_rework_cancel(lot, child), "Failed to merge after rework"
    #
    return children
  end

  def split_merge_all(lot, max_seq: 33)
    @@split_loops.times.collect {|i|
      if i < max_seq
        assert child = @@sv.lot_split(lot, 2), "failed to split #{lot}"
        assert_equal 0, @@sv.lot_merge(lot, child), "failed to merge #{child} into #{lot}"
        child
      else
        assert_nil @@sv.lot_split(lot, 2), "split #{lot} should fail"
        assert_equal 11215, @@sv.rc, "error expected: Sequence number overflow [#{@@sv.msg_text}]"
        nil
      end
    }.compact
  end

  # verify merge is rejected for _any_ LOT_LABEL other than the correct one
  def try_merge_wrong_labels(lot, child, layer, &blk)
    lot_label = @@sv.lot_label(lot)
    child_label = @@bsm_data.next_split(lot, lot_label, layer)
    assert_equal child_label, @@sv.lot_label(child), "wrong lot label for #{lot}"
    @@bsm_data.enum_lot_labels(lot, child_label, layer).each_with_index {|label, idx|
      $log.info "-- ##{idx}: LOT_LABEL #{label.inspect}"
      assert_equal 0, @@sv.user_data_update(:lot, child, {'LOT_LABEL'=>label})
      blk.call(lot, child)
      assert_equal 159, @@sv.rc, "wrong error code"
      assert @@sv.msg_text.include?('0001430E:Some attribute value is different between parent lot and child lot'), "wrong msg text"
    }
  end

end
