=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: Daniel Steger     2013-07-04

History:
  2015-06-25 ssteidte, cleaned up and adapted for R15
  2016-06-28 dsteger,  added MES-2913
  2018-03-06 sfrieske, replaced user_parameter_set_verify by user_paramer_change
  2019-12-09 sfrieske, minor cleanup, renamed from Test150_CarrierXferStateEI
  2020-08-03 sfrieske, use product_list raw results
  2021-01-15 sfrieske, moved SiView::MM#new_srclot_for_product to SiView::Test#new_srclot
  2021-08-27 sfrieske, split verify_equal off of misc.rb
=end

require 'util/verifyequal'  # TODO: remove
require 'SiViewTestCase'


# Tests various tx for carrier xfer state EI (MSR297548, MSR544211, MSR202453, MSR202990, MSR576673, MSR806964,
#   MSR111430, implicitly test MSR329203), MES-2913
class MM_Lot_CarrierXferStateEI < SiViewTestCase
  @@eqp = 'UTC001'
  @@opno = '1000.700'
  @@merge_opno = '1000.800'
  @@tw_opno = '100.200'
  @@product2 = 'UT-PRODUCT002.01'
  @@route_2 = 'UTRT001.01'
  @@rework_route = 'UTRW002.01'
  @@sub_route = 'UTBR001.01'
  @@hold_reason = 'ENG'
  @@skip_opno = '1000.900'
  @@skip_opno_next = '1000.930'
  @@non_probank = 'UT-NONPROD'
  @@locate_check = '' # not set in Fab1/8
  @@ei_check = '0'

  @@testlots = []


  def self.after_all_passed
    @@sv.eqp_cleanup(@@eqp)
    @@testlots.each {|lot| @@sv.delete_lot_family(lot)}
  end

  def setup
    assert_equal 0, @@sv.eqp_cleanup(@@eqp)
  end


  def test00_setup
    $setup_ok = false
    #
    assert_equal @@ei_check, @@sv.environment_variable[1]['SP_LOT_OPERATION_EI_CHECK'], "wrong server setting"
    assert @@svtest.cleanup_eqp(@@eqp)
    assert @@svtest.get_empty_carrier(n: 5), 'not enough empty carriers'
    assert @@lot = @@svtest.new_lot, 'error creating test lot'
    @@testlots << @@lot
    #
    $setup_ok = true
  end

  # Should work after operation was completed, before unload
  def test10_after_processing
    prepare(@@lot, @@opno)
    assert @@sv.claim_process_lot(@@lot, eqp: @@eqp, nounload: true), "failed to process"
    # check tx calls
    assert _lot_operations(@@lot), "failed tx calls while carrier EI"
  end

  def test11_before_operation_start
    prepare(@@lot, @@opno)
    assert @@sv.claim_process_prepare(@@lot, eqp: @@eqp), "failed to resrv/load"
    # check tx calls
    assert _lot_operations(@@lot, true), "failed tx calls while carrier EI"
  end

  def test12_after_operation_start
    prepare(@@lot, @@opno)
    assert @@sv.claim_process_lot(@@lot, eqp: @@eqp, noopecomp: true, nounload: true), "failed to process"
    # check tx calls
    assert _lot_operations(@@lot, true, true), "failed tx calls while carrier EI"
  end

  def test13_merge_after_processing  # MSR806964: allow merge
    prepare(@@lot, @@opno)
    lc = @@sv.lot_split(@@lot, 10, mergeop: @@merge_opno)
    assert_equal 0, @@sv.lot_opelocate(lc, 1)
    assert @@sv.claim_process_lot(@@lot, eqp: @@eqp, nounload: true), "failed to process"
    assert_equal 0, @@sv.lot_merge(@@lot, lc)
    assert_equal 0, @@sv.eqp_unload(@@eqp, nil, @@sv.lot_info(@@lot).carrier)
    # check tx calls
    assert _lot_operations(@@lot), "failed tx calls while carrier EI"
  end

  def test14_merge_lc_after_processing
    prepare(@@lot, @@opno)
    lc = @@sv.lot_split(@@lot, 10, mergeop: @@merge_opno)
    @@sv.lot_opelocate(@@lot, 1)
    assert @@sv.claim_process_lot(lc, eqp: @@eqp, nounload: true), "failed to process"
    assert_equal 0, @@sv.lot_merge(@@lot, lc)
    assert_equal 0, @@sv.eqp_unload(@@eqp, nil, @@sv.lot_info(@@lot).carrier)
    # check tx calls
    assert _lot_operations(@@lot), "failed tx calls while carrier EI"
  end

  def test15_merge_both_after_processing
    prepare(@@lot, @@opno)
    lc = @@sv.lot_split(@@lot, 10, mergeop: @@merge_opno)
    assert @@sv.claim_process_lot([@@lot, lc], eqp: @@eqp, nounload: true), "failed to process"
    assert_equal 0, @@sv.lot_merge(@@lot, lc)
    assert_equal 0, @@sv.eqp_unload(@@eqp, nil, @@sv.lot_info(@@lot).carrier)
    # check tx calls
    assert _lot_operations(@@lot), "failed tx calls while carrier EI"
  end

  def test20_tw_processing
    assert lot = @@svtest.new_controllot('Equipment Monitor'), "error creating test lot"
    @@testlots << lot
    prepare(lot, @@tw_opno)
    lc = @@sv.lot_split(lot, 10)
    assert lc, "error splitting lot #{lot}"
    assert @@sv.claim_process_lot(lc, eqp: @@eqp, noopecomp: true, nounload: true), "failed to process"
    # check tx calls
    assert _tw_lot_operations(lot, true), "failed tx calls while carrier EI"
  end

  def test21_tw_after_processing
    assert lot = @@svtest.new_controllot('Equipment Monitor'), "error creating test lot"
    @@testlots << lot
    prepare(lot, @@tw_opno)
    lc = @@sv.lot_split(lot, 10)
    assert lc, "error splitting lot #{lot}"
    assert @@sv.claim_process_lot(lc, eqp: @@eqp, nounload: true), "failed to process"
    # check tx calls
    assert _tw_lot_operations(lot, false), "failed tx calls while carrier EI"
  end

  def test30_vendor_lot_processing
    plot, vlot = _prepare_vendorlot
    assert @@sv.claim_process_lot(plot, eqp: @@eqp, noopecomp: true, nounload: true), "failed to process"
    # check tx calls
    assert _vendor_lot_operations(vlot, true), "failed tx calls while carrier EI"
  end

  def test31_vendor_lot_after_processing
    plot, vlot = _prepare_vendorlot
    assert @@sv.claim_process_lot(plot, eqp: @@eqp, nounload: true), "failed to process"
    # check tx calls
    assert _vendor_lot_operations(vlot, false), "failed tx calls while carrier EI"
  end


  # Should do some concurrent multi lot tests

  def _prepare_vendorlot
    # need one prod lot and one vendor lot
    assert vlot = @@svtest.new_srclot, 'error creating srclot'
    @@testlots << vlot
    assert plot = @@svtest.new_lot(srclot: vlot, nwafers: 5)
    @@testlots << plot
    prepare(plot, @@opno)
    return [plot, vlot]
  end

  def _vendor_lot_operations(lot, processing)
    _rc = @@ei_check == '0' ? 0 : (processing ? 2045 : 905)
    res = true
    li = @@sv.lot_info(lot)
    assert_equal 'COMPLETED', li.status, "failed to prepare lot for STB"
    startbank = @@sv.AMD_product_details(@@svtest.product).startBankID.identifier
    assert_equal 0, @@sv.lot_bank_move(lot, startbank) unless li.bank == startbank
    assert new_lot = @@sv.new_lot_release(product: @@svtest.product, srclot: lot)
    @@testlots << new_lot
    stb_lot = @@sv.stb(new_lot)
    assert_equal _rc, @@sv.rc, "TxSTBReleasedLotReq"
    if stb_lot
      @@testlots << stb_lot
      assert cl_lot = @@sv.stb_cancel(new_lot), "TxSTBCancelReq"
      @@testlots << cl_lot
      child = @@sv.lot_split_notonroute(cl_lot, 5) # with no hold
      assert_equal _rc, @@sv.rc, "TxSplitWaferLotNotOnRouteReq"
      assert_equal _rc, @@sv.lot_merge_notonroute(cl_lot, child), "TxMergeWaferLotNotOnRouteReq" if child
    elsif new_lot
      assert_equal 0, @@sv.new_lot_release_cancel(new_lot), "cannot cleanup lot #{new_lot}"
    end
    return res
  end

  def _tw_lot_operations(lot, processing)
    _rc = @@ei_check == '0' ? 0 : (processing ? 2045 : 905)
    res = true
    @@sv.lot_opelocate(lot, :last)
    @@sv.lot_bankin(lot) unless @@sv.lot_info(lot).status == 'COMPLETED'
    linfo = @@sv.lot_info(lot)
    assert_equal 'COMPLETED', linfo.status, "failed to prepare lot for STB"
    ## startbank = @@sv.product_list(product: @@svtest.product_eqpmonitor, lottype: 'Equipment Monitor').first.startBankID.identifier
    # assert pl = @@sv.product_list('Equipment Monitor')
    # assert pattrs = pl.find {|p| p.productID.identifier == @@svtest.product_eqpmonitor}
    # startbank = pattrs.startBankID.identifier
    startbank = @@sv.AMD_product_details(@@svtest.product_eqpmonitor).startBankID.identifier
    @@sv.lot_bank_move(lot, startbank)
    stb_lot = @@svtest.new_controllot('Equipment Monitor', srclot: lot, nwafers: linfo.nwafers)
    assert_equal _rc, @@sv.rc, "TxCtrlLotSTBReq"
    if stb_lot
      @@testlots << stb_lot
      assert cl_lot = @@sv.stb_cancel(stb_lot), "TxSTBCancelReq"
      @@testlots << cl_lot
      child = @@sv.lot_split_notonroute(cl_lot, 5) # without hold
      res &= verify_equal(_rc, @@sv.rc, "TxSplitWaferLotNotOnRouteReq")
      res &= verify_equal(_rc, @@sv.lot_merge_notonroute(cl_lot, child), "TxMergeWaferLotNotOnRouteReq") if child
    end
    return res
  end

  def _lot_operations(lot, has_cj=false, processing=false, loaded=true)
    if @@ei_check == '1'
      _rc = 905
      _rc2 = 938 # other reason code for partial rework
      _rc5 = 905
    elsif has_cj
      _rc = 1229
      _rc2 = 1229
      _rc5 = 329
    else # no check
      _rc = 0
      _rc2 = 0
      _rc5 = 0
    end
    _rc3 = has_cj ? 1229 : (@@ei_check == '1' ? 1738 : 0) # other reason priority + postprocessing
    _rc4 = has_cj ? 1229 : _rc # other reason priority
    _rc_locate = (@@locate_check == '1' && loaded) ? 905 : _rc

    res = true
    # according to MSR111430, MSR466909 priority class change always succeeds if env is configured correctly
    # replaced by MES-2913
    assert_equal 0, @@sv.schdl_change(lot, priorityclass: 4), "TxLotSchdlChangeReq(prio class)"
    assert_equal 0, @@sv.schdl_change(lot, starttime: @@sv.siview_timestamp, finishtime: @@sv.siview_timestamp(Time.now + 72*3600)), "TxLotSchdlChangeReq(timestamp)"
    return res if processing

    assert_equal _rc5, @@sv.carrier_usage_reset(@@sv.lot_info(lot).carrier), "TxCassetteUsageResetReq"
    # MSR576673, replaced by MES-2913
    assert_equal 0, @@sv.lot_mfgorder_change(lot, sublottype: "QX"), "TxLotMfgOrderChangeReq(SLT)"
    assert_equal _rc, @@sv.lot_requeue(lot), "TxLotRequeueReq"
    #
    child = @@sv.lot_split(lot, 5) # with no hold
    assert_equal _rc4, @@sv.rc, "TxSplitWaferLotReq"
    assert_equal(_rc, @@sv.lot_merge(lot, child), "TxMergeWaferLotReq") if child
    #
    assert_equal 0, @@sv.lot_hold(lot, @@hold_reason)
    child = @@sv.lot_split(lot, 5, onhold: true) # with hold inherit
    assert_equal _rc4, @@sv.rc, "TxSplitWaferLotWithoutHoldReleaseReq"
    if @@sv.f7 && @@sv.rc == 0 # TODO: wait for MES-2279
      assert_equal 0, @@sv.lot_hold_release(lot, nil)
      assert_equal 0, @@sv.lot_hold_release(child, nil)
    end
    assert_equal(_rc, @@sv.lot_merge(lot, child), "TxMergeWaferLotReq") if child
    if @@sv.f7 && @@sv.rc == 0 # TODO: wait for MES-2279
      assert_equal 0, @@sv.lot_hold(lot, @@hold_reason)
    end
    #
    child = @@sv.lot_split(lot, 5, onhold: true, release: true) # with hold release
    assert_equal _rc4, @@sv.rc, "TxSplitWaferLotWithHoldReleaseReq"
    assert_equal(_rc, @@sv.lot_merge(lot, child), "TxMergeWaferLotReq") if child

    if _rc != 0
      assert_equal 0, @@sv.lot_hold_release(lot, nil), "release hold"
    end

    child = @@sv.lot_partial_rework(lot, 5, @@rework_route) # with hold inherit | hold release | no hold
    assert_equal _rc2, @@sv.rc, "TxPartialReworkReq"
    assert_equal(_rc, @@sv.lot_partial_rework_cancel(lot, child), "TxPartialReworkCancelReq") if child

    # lot hold is required for following actions
    assert_equal 0, @@sv.lot_hold(lot, @@hold_reason)

    child = @@sv.lot_partial_rework(lot, 5, @@rework_route, onhold: true) # with hold inherit
    assert_equal _rc4, @@sv.rc, "TxPartialReworkWithoutHoldReleaseReq"
    assert_equal(_rc, @@sv.lot_partial_rework_cancel(lot, child), "TxPartialReworkCancelReq") if child

    child = @@sv.lot_partial_rework(lot, 5, @@rework_route, onhold: true, release: true) # with hold inherit | hold release | no hold
    assert_equal _rc2, @@sv.rc, "TxPartialReworkWithHoldReleaseReq"
    assert_equal(_rc, @@sv.lot_partial_rework_cancel(lot, child), "TxPartialReworkCancelReq") if child

    # no lot hold required for following actions
    assert @@sv.lot_hold_release(lot, nil)

    assert_equal _rc, @@sv.lot_rework(lot, @@rework_route), "TxReworkReq"
    assert_equal(_rc, @@sv.lot_rework_cancel(lot), "TxReworkWholeLotCancelReq") if _rc == 0 # no need to check if rework was not executed

    assert_equal _rc4, @@sv.lot_branch(lot, @@sub_route), "TxBranchReq"
    assert_equal _rc2, @@sv.lot_branch_cancel(lot), "TxSubRouteBranchCancelReq"

    #### Post processing related errors ####
    assert_equal 0, @@sv.lot_futurehold(lot, @@skip_opno, @@hold_reason)
    assert_equal _rc, @@sv.lot_gatepass(lot), "TxGatePassReq"

    if _rc == 0
      assert_equal(_rc, @@sv.lot_hold_release(lot, nil), "Hold release triggers Pre1") # with Pre1-script using locate or skip
    else
      # need to move lot to skip operation ????
      # cancel remaining future holds
      @@sv.lot_futurehold_cancel(lot, nil)
    end

    assert_equal _rc_locate, @@sv.lot_opelocate(lot, @@skip_opno, force: false), "TxOpeLocateReq"

    # This Tx is already covered by MSR466909, but not the post processing
    # replaced by MES-2913
    assert_equal _rc3, @@sv.schdl_change(lot, product: @@product2, route: @@svtest.route, opNo: @@skip_opno), "TxLotSchdlChangeReq"
    if _rc3 == 0
      opno = @@skip_opno_next
    elsif _rc3 == 1229
      opno = @@opno
    else
      opno = @@skip_opno
      assert_equal 0, @@sv.pp_force_delete("", lot), "can't remove post processing"
    end
    assert_equal opno, @@sv.lot_info(lot).opNo, "lot at wrong operation, expected #{opno}"

    # put lot onhold and force locate to previous operation
    assert_equal 0, @@sv.lot_hold(lot, @@hold_reason), "failed to put lot onhold"

    # choose different op according to current position
    if opno == @@skip_opno
      opno = @@skip_opno_next
    else
      opno = @@skip_opno
    end
    assert_equal _rc_locate, @@sv.lot_opelocate(lot, opno, force: true), "TxForceOpeLocateReq"

    assert_equal (has_cj ? 1229 : 0), @@sv.lot_nonprobankin(lot, @@non_probank), "faild to do nonpro bankin"
    if _rc == 0 #no need to check if bankin failed
      assert_equal 0, @@sv.lot_hold_release(lot, @@hold_reason), "failed to release hold"
      assert_equal 0, @@sv.lot_nonprobankout(lot), "TxNonProBankOutReq"
    end

    # move lot to measurement operation
    rc_loc = @@sv.lot_opelocate(lot, @@opno)
    if rc_loc == 0
      assert_equal _rc, @@sv.lot_remeasure(lot, force: true), "CS_TxRemeasureLocateReq"
    else
      ### can't move to metrology operation - skip
      $log.warn "CS_TxRemeasureLocateReq failed"
    end
    return res
  end


  def prepare(lot, opno)
    # cleanup lot
    assert_equal 0, @@sv.lot_cleanup(lot)
    assert @@sv.merge_lot_family(lot, nosort: true)
    assert_equal 0, @@sv.lot_opelocate(lot, opno), "failed to locate #{lot} to operation"
    if @@sv.lot_info(lot).lottype == 'Production'
      assert_equal 0, @@sv.schdl_change(lot, product: @@svtest.product, route: @@svtest.route, priorityclass: 1), "failed to set route/product"
      assert_equal 0, @@sv.sublottype_change(lot, 'PO'), "failed to set sublottype"
    end
    assert_equal 0, @@sv.user_parameter_change('Lot', lot, 'UTSkip', 'YES')
  end

end
