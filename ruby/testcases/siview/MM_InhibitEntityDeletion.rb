=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: ssteidte, 2014-05-21

History:
  2019-11-18 sfrieske, minor cleanup
  2019-12-05 sfrieske, renamed from Test135_InhibitEntityDeletion
=end

require 'SiViewTestCase'


# Test the deletion of inhibits when the object is deleted in SM, MSR 822650
class MM_InhibitEntityDeletion < SiViewTestCase
  @@productgroup = '2191A'
  @@productgroupnew = 'XXUT01'
  # for combined inhibits
  @@eqp = 'UTF001'
  @@mrecipe = 'P.UTMR005.01'
  @@pd = 'UTPD200.01'

  def setup
    @@sm.delete_production(:productgroup, @@productgroupnew)
  end


  def test11_productgroup
    do_test(:productgroup, @@productgroup, @@productgroupnew, :productgroup, @@productgroupnew)
  end

  def test12_productgroup_eqp
    do_test(:productgroup, @@productgroup, @@productgroupnew, :productgroup_eqp, [@@productgroupnew, @@eqp])
  end

  def test13_productgroup_eqp_recipe
    do_test(:productgroup, @@productgroup, @@productgroupnew, :productgroup_eqp_recipe, [@@productgroupnew, @@eqp, @@mrecipe])
  end

  def test14_productgroup_pd
    do_test(:productgroup, @@productgroup, @@productgroupnew, :productgroup_pd, [@@productgroupnew, @@pd])
  end

  def test15_productgroup_pd_eqp
    do_test(:productgroup, @@productgroup, @@productgroupnew, :productgroup_pd, [@@productgroupnew, @@pd, @@eqp])
  end

  def test16_productgroup_pd_recipe
    do_test(:productgroup, @@productgroup, @@productgroupnew, :productgroup_pd, [@@productgroupnew, @@pd, @@mrecipe])
  end


  def do_test(smclass, template, obj, inhclass, inhobjs)
    assert_empty @@sm.object_info(smclass, obj), "#{obj} already exists"
    assert @@sm.copy_object(smclass, template, obj), 'error creating new product group'
    assert_empty @@sv.inhibit_list(inhclass, inhobjs), "#{inhobjs.inspect} already has inhibits"
    assert @@sv.inhibit_entity(inhclass, inhobjs)
    # refute_empty @@sv.inhibit_list(inhclass, inhobjs), "error setting inhibit for #{inhobjs.inspect}"
    #
    assert @@sm.delete_production(smclass, obj), 'error creating new product group'
    assert_empty @@sv.inhibit_list(inhclass, inhobjs), "#{inhobjs.inspect} still has inhibits"
  end

end
