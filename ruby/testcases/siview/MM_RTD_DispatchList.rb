=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: ssteidte, 2014-05-21

History:
  2017-01-05 sfrieske, use new RTD::EmuRemote
  2017-04-04 sfrieske, create test lot for @@eqp on demand
  2019-12-09 sfrieske, minor cleanup, renamed from Test170_RTD
  2021-01-20 sschoen3, change ToolID for Test12 from UTS007 to UTNODISPATCH
=end

require 'rtdserver/rtdemu'
require 'rtdserver/rtdemuremote'
require 'SiViewTestCase'


# Test SiView calls to RTD, MSR644011
class MM_RTD_DispatchList < SiViewTestCase
  @@eqp = 'UTF001'
  @@eqp_nolots = 'UTNODISPATCH'
  @@testlots = []


  def self.after_all_passed
    @@testlots.each {|lot| @@sv.delete_lot_family(lot)}
  end

  def teardown
    [@@eqp, @@eqp_nolots].each {|eqp| @@rtdremote.delete_emustation(eqp)}
  end


  def test00_setup
    $setup_ok = false
    #
    @@rtdremote = RTD::EmuRemote.new($env)
    #
    $setup_ok = true
  end

  def test11_rtd_nolotlist
    # verify RTD is called without lot list
    eqp = @@eqp
    assert dlist = @@sv.what_next(eqp)
    if dlist.empty?
      # create a test lot and locate make it available to @@eqp
      assert lot = @@svtest.new_lot, 'error creating test lot'
      @@testlots << lot
      assert opNo = @@sv.lot_eqplist_in_route(@@lot, eqp: eqp).keys.first, "no opNo for #{eqp}"
      assert_equal 0, @@sv.lot_opelocate(@@lot, opNo)
      dlist = @@sv.what_next(eqp)
    end
    refute_empty dlist, "#{eqp} has no candidate lots. Pick a different eqp."
    lotlist = 'wrong'
    @@rtdremote.add_emustation(eqp) {|station, rule, params|
      $log.info "RTD call params:\n#{params.inspect}"
      lotlist = params['lotlist']
      RTD::Emulator.empty_reply('', '')
    }
    assert @@sv.rtd_dispatch_list(eqp)
    assert_nil lotlist, "lotlist must not be sent"
  end

  def test12_rtd_nocandidates
    # verify RTD is called even if there are no SiView WhatNext candidates, MSR644011
    eqp = @@eqp_nolots
    dlist = @@sv.what_next(eqp)
    assert_empty dlist, "#{eqp} has must have no candidate lots. Pick a different eqp."
    called = false
    @@rtdremote.add_emustation(eqp) {|station, rule, params|
      $log.info "RTD call params:\n#{params.inspect}"
      called = true
      RTD::Emulator.empty_reply('', '')
    }
    assert @@sv.rtd_dispatch_list(eqp)
    assert called, "RTD not called"
  end

end
