=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: Steffen Steidten, 2010-05-03
        Beate Kochinka    2010-08-13

History:
  2014-09-03 ssteidte, removed tests for obsolete AMD_TxLotReticleGroupIDInq
  2017-02-04 sfrieske, minor cleanup
  2019-12-09 sfrieske, minor cleanup, renamed from Test112_LotReticleGroup
=end

require 'SiViewTestCase'


# Test the correct function of reticlesets and the transaction AMD_TxLotReticleGroupIDListInq
#
# The tests in group 1 (test1...) at photolayer 02 verify equipment overrides work correctly.
# The tests in group 2 (test2...) at photolayer 06 verify equipment and product overrides work correctly, including combined overrides.
class MM_Eqp_LotReticleGroup < SiViewTestCase
  @@lot_default = nil       # will be set by test
  @@lot_override_prod = nil # will be set by test
  @@equipment1 = 'UTR001'   # equipment for which override is defined in photo layer 02 and 06
  @@equipment2 = 'UTR002'   # equipment for which override is defined in photo layer 02 only
  @@openum1 = '1000.950'    # operation with photo layer 02
  @@openum2 = '2000.950'    # operation with photo layer 06
  @@prod_default = 'UT-PRODUCT001.01'
  @@prod_override = 'UT-PRODUCT000.01'
  # default reticle groups in both photo layer 02 and 06
  @@rg_default = %w(UTRETICLEGROUP10 UTRETICLEGROUP11 UTRETICLEGROUP12 UTRETICLEGROUP13 UTRETICLEGROUP14 UTRETICLEGROUP15 UTRETICLEGROUP16 UTRETICLEGROUP17 UTRETICLEGROUP18 UTRETICLEGROUP19)
  # override reticle groups in photo layer 02 for equipment 1
  @@rg_override_02_eqp1 = %w(UTRETICLEGROUP3 UTRETICLEGROUP13 UTRETICLEGROUP23)
  # override reticle groups in photo layer 02 for equipment 2
  @@rg_override_02_eqp2 = %w(UTRETICLEGROUP4 UTRETICLEGROUP14 UTRETICLEGROUP24)
  # override reticle groups in photo layer 06 for product
  @@rg_override_06_prod = %w(UTRETICLEGROUP4 UTRETICLEGROUP14 UTRETICLEGROUP24)
  # override reticle groups in photo layer 06 for equipment 1
  @@rg_override_06_eqp1 = %w(UTRETICLEGROUP5 UTRETICLEGROUP15 UTRETICLEGROUP25)
  @@loops = 20

  @@testlots = []


  def self.after_all_passed
    @@testlots.each {|lot| @@sv.delete_lot_family(lot)}
  end


  def test00_setup
    $setup_ok = false
    #
    assert @@loops > 1, "loops must be > 1"
    assert @@lot_default = @@svtest.new_lot(product: @@prod_default)
    @@testlots << @@lot_default
    assert @@lot_override_prod = @@svtest.new_lot(product: @@prod_override)
    @@testlots << @@lot_override_prod
    $log.info "using lots #{@@testlots}"
    #
    $setup_ok = true
  end

  def test01_invalid_parameters
    $log.info "invalid lot at #{@@equipment2}"
    assert_nil @@sv.AMD_lot_reticlegrouplist('', @@equipment2)
    assert_equal 1450, @@sv.rc, "wrong reason code"
    #
    $log.info "invalid lot and equipment"
    assert_nil @@sv.AMD_lot_reticlegrouplist('', '')
    assert_equal 10503, @@sv.rc, "wrong reason code"
  end


  # test at operation with photo layer 02, two different equipment overrides

  def test110_setup
    $setup_ok = false
    #
    assert_equal 0, @@sv.lot_opelocate(@@lot_default, @@openum1)
    assert_equal 0, @@sv.lot_opelocate(@@lot_override_prod, @@openum1)
    #
    $setup_ok = true
  end

  def test112_op1_lot1_eqp1_rglist
    check_rglist(@@lot_default, @@equipment1, @@rg_override_02_eqp1)
  end

  def test122_op1_lot2_eqp1_rglist
    check_rglist(@@lot_override_prod, @@equipment1, @@rg_override_02_eqp1)
  end

  def test132_op1_lot1_eqp2_rglist
    check_rglist(@@lot_default, @@equipment2, @@rg_override_02_eqp2)
  end

  def test142_op1_lot2_eqp2_rglist
    check_rglist(@@lot_override_prod, @@equipment2, @@rg_override_02_eqp2)
  end


  # test at operation with photo layer 06, equipment and product overrides

  def test210_setup
    $setup_ok = false
    assert_equal 0, @@sv.lot_opelocate(@@lot_default, @@openum2)
    assert_equal 0, @@sv.lot_opelocate(@@lot_override_prod, @@openum2)
    $setup_ok = true
  end

  def test212_op2_lot1_eqp1_rglist
    # default product (lot), eqp1 with override, eqp2 default
    check_rglist(@@lot_default, @@equipment1, @@rg_override_06_eqp1)
  end

  def test232_op2_lot1_eqp2_rglist
    # default product (lot), eqp1 with override, eqp2 default
    check_rglist(@@lot_default, @@equipment2, @@rg_default)
  end

  def test242_op2_lot2_eqp2_rglist
    # override product (lot), eqp1 with override -> eqp1 effective, eqp2 default -> product override effective
    check_rglist(@@lot_override_prod, @@equipment2, @@rg_override_06_prod)
  end


  # aux methods

  def check_rglist(lot, eqp, rgs)
    $log.info "verifying rg list for lot #{lot}, product #{@@sv.lot_info(lot).product} on eqp #{eqp}"
    @@loops.times {|i| assert_equal rgs, @@sv.AMD_lot_reticlegrouplist(lot, eqp), "wrong rg list"}
  end
end
