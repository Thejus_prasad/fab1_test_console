=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Authors: Daniel Steger (Java), Beate Kochinka (Ruby port)

History:
  2013-01-13 ssteidte, started code cleanup for better maintenance and error analysis
  2014-11-06 ssteidte, rewrite of setup and test combinations
  2016-04-22 sfrieske, added testcase for MES-2992
  2016-08-18 sfrieske, added test for eqp_chamber_productgroup (MSR1123959)
  2018-09-26 sfrieske, fixed use of inhibit_entity for "all sublottypes", more cleanup
  2020-08-27 sfrieske, minor cleanup
  2022-01-07 sfrieske, fixed use of object_info(:equipmentstate)
=end

require 'SiViewTestCase'

# was: Test076_ChambersAvailability
class MM_Eqp_ChamberAvailability < SiViewTestCase
  @@nchildren = 2
  @@eqp = 'UTC003'
  @@chamberstates = %w(PRD.1PRD SBY.2WPR ENG.2NDP SDT.4QUL)
  @@product = 'UT-PRODUCT000.01'
  @@product2 = 'UT-PRODUCT001.01'
  @@pg2 = 'QAPG2'  # any unused product group
  # monitor products for UTC003
  @@product_dummy = 'UT-DN-UTF002.01'
  @@product_eqpmonitor = 'UT-MN-UTC001.01'
  @@product_procmonitor = 'UT-PR-UTC001.01'
  #
  @@opNo = '2000.700'
  @@opNo_dummy = '100.200'
  @@opNo_eqpmonitor = '100.200'
  @@opNo_procmonitor = '100.200'
  @@recipe = 'P.UTMR000.01'
  @@recipe2 = 'P.UTMR001.01'

  @@testlots = []


  def self.after_all_passed
    @@sv.eqp_cleanup(@@eqp)
    @@testlots.each {|lot| @@sv.delete_lot_family(lot)}
  end

  def setup
    assert_equal 0, @@sv.eqp_cleanup(@@eqp)
  end

  def test00_setup
    $setup_ok = false
    #
    # get Equipment State availability from SM
    @@eqpstates = {}
    @@chamberstates.each {|s|
      assert @@eqpstates[s] = @@sm.object_info(:equipmentstate, s).first, "Equipment State #{s} not configured in SM"
    }
    assert_equal ['Dummy', 'Equipment Monitor', 'Process Monitor'], @@eqpstates['SDT.4QUL'].specific[:sublottypes].sort, 'wrong SM setup for 4QUL'
    assert @@eqpstates['SDT.4QUL'].specific[:conditional], 'wrong SM setup for SDT.4QUL "Conditional Available"'
    #
    assert @@svtest.cleanup_eqp(@@eqp, mode: 'Off-Line-1')
    eqpi = @@sv.eqp_info(@@eqp)
    @@port = eqpi.ports.first.port
    @@chambers = eqpi.chambers.collect {|c| c.chamber}
    assert @@chambers.size > 2, "wrong eqp #{@@eqp}, not enough chambers"
    #
    # prepare Production lot
    assert @@lot = @@svtest.new_lot(product: @@product), 'error creating test lot'
    @@testlots << @@lot
    assert_equal 0, @@sv.lot_opelocate(@@lot, @@opNo)
    @@nchildren.times {assert @@sv.lot_split(@@lot, 1), 'error splitting lot'}
    @@pg = @@sv.lot_info(@@lot).productgroup
    #
    # prepare monitor lots
    assert @@lot_dummy = @@svtest.new_controllot('Dummy', product: @@product_dummy)
    @@testlots << @@lot_dummy
    assert_equal 0, @@sv.lot_opelocate(@@lot_dummy, @@opNo_dummy)
    @@nchildren.times {assert @@sv.lot_split(@@lot_dummy, 1), 'error splitting lot'}
    #
    assert @@lot_procmonitor = @@svtest.new_controllot('Process Monitor', product: @@product_procmonitor)
    @@testlots << @@lot_procmonitor
    assert_equal 0, @@sv.lot_opelocate(@@lot_procmonitor, @@opNo_procmonitor)
    @@nchildren.times {assert @@sv.lot_split(@@lot_procmonitor, 1), 'error splitting lot'}
    #
    assert @@lot_eqpmonitor = @@svtest.new_controllot('Equipment Monitor', product: @@product_eqpmonitor)
    @@testlots << @@lot_eqpmonitor
    assert_equal 0, @@sv.lot_opelocate(@@lot_eqpmonitor, @@opNo_eqpmonitor)
    @@nchildren.times {assert @@sv.lot_split(@@lot_eqpmonitor, 1), 'error splitting lot'}
    #
    $setup_ok = true
  end

  def test01_ControlJobNotFound
    assert_nil @@sv.AMD_chambers_availability('NotExistentCJ'), 'must fail for not existing cj'
    assert_equal 2801, @@sv.rc, 'wrong RC'
    assert @@sv.msg_text.start_with?('0000630E:ControlJob is not found'), 'wrong message text'
  end

  def test11_Production_EqpChamber
    $setup_ok = false
    #
    @pd_condavailable = true
    @lotID = @@lot
    idx = 0  ##rand(2)
    chamber = @@chambers[idx]
    doInhibits(idx, [:eqp_chamber, @@eqp, attrib: chamber], true)
    #
    $setup_ok = true
  end

  def test12_Production_EqpChamberProduct
    @pd_condavailable = true
    @lotID = @@lot
    idx = 0  ##rand(2)
    chamber = @@chambers[idx]
    doInhibits(idx, [:eqp_chamber_product, [@@eqp, @@product], attrib: chamber], true)
    doInhibits(idx, [:eqp_chamber_product, [@@eqp, @@product2], attrib: chamber], false)
  end

  def test13_Production_EqpChamberRecipe
    @pd_condavailable = true
    @lotID = @@lot
    idx = 0  ##rand(2)
    chamber = @@chambers[idx]
    doInhibits(idx, [:eqp_chamber_recipe, [@@eqp, @@recipe], attrib: chamber], true)
    doInhibits(idx, [:eqp_chamber_recipe, [@@eqp, @@recipe2], attrib: chamber], false)
  end

  def test14_Production_EqpChamberProductgroup
    @pd_condavailable = true
    @lotID = @@lot
    idx = 0  ##rand(2)
    chamber = @@chambers[idx]
    doInhibits(idx, [:eqp_chamber_productgroup, [@@eqp, @@pg], attrib: chamber], true)
    doInhibits(idx, [:eqp_chamber_productgroup, [@@eqp, @@pg2], attrib: chamber], false)
  end

  # tests chamber state combinations with dummy lot
  def test21_Dummy
    @pd_condavailable = true
    @lotID = @@lot_dummy
    op = @@sv.lot_info(@lotID).op
    assert @@sm.update_udata(:pd, op, {'E10ConditionalAvailable'=>'Yes'}), 'error writing SM UDATA'
    doChamberStateLotCombinations(nil)
  end

  # tests chamber state combinations with process moniotr lot
  def test31_ProcessMonitor
    @pd_condavailable = true
    @lotID = @@lot_procmonitor
    op = @@sv.lot_info(@lotID).op
    assert @@sm.update_udata(:pd, op, {'E10ConditionalAvailable'=>'Yes'}), 'error writing SM UDATA'
    doChamberStateLotCombinations(nil)
  end

  # tests chamber state combinations with equipment monitor lot for available PD
  def test41_EquipmentMonitor_conditional
    @pd_condavailable = true
    @lotID = @@lot_eqpmonitor
    op = @@sv.lot_info(@lotID).op
    assert @@sm.update_udata(:pd, op, {'E10ConditionalAvailable'=>'Yes'}), 'error writing SM UDATA'
    doChamberStateLotCombinations(nil)
  end

  # tests chamber state combinations with equipment monitor lot for not available PD
  def test42_EquipmentMonitor_conditional_notavailable
    @pd_condavailable = false
    @lotID = @@lot_eqpmonitor
    op = @@sv.lot_info(@lotID).op
    assert @@sm.update_udata(:pd, op, {'E10ConditionalAvailable'=>''}), 'error writing SM UDATA'
    doChamberStateLotCombinations(nil)
  end

  # special test cases

  def test51_2inhibits_sublottype_recipe  # MES-2992, ref MSR675209
    assert @@sv.eqp_cleanup(@@eqp, mode: 'Auto-1')
    chamber = @@sv.eqp_info(@@eqp).chambers.collect {|c| c.chamber}[0]
    assert lot = @@svtest.new_lot, 'error creating test lot'
    @@testlots << lot
    # inhibit eqp+chamber+recipe related to the tool but not lot -> available
    mrcp2 = 'P.UTMR000.02'
    assert @@sm.object_info(:recipe, mrcp2).first.specific[:eqps].member?(@@eqp), "wrong setup, #{@@eqp} not in #{mrcp2}"
    assert @@sv.inhibit_entity(:eqp_chamber_recipe, [@@eqp, mrcp2], attrib: chamber)
    assert_equal 0, @@sv.lot_opelocate(lot, @@opNo)
    lotcas = nil
    assert @@sv.claim_process_lot(lot, eqp: @@eqp) {|cj| lotcas = @@sv.AMD_chambers_availability(cj)}
    lotcas.first.chamberAvailability.each {|ca|assert_equal '1', ca.AMD_ChamberStatus, 'wrong chamber status'}
    # additionally inhibit eqp+chamber for sublottype other than lot's one -> available
    slt2 = 'EO'
    refute_equal slt2, @@sv.lot_info(lot).sublottype, 'wrong sublottype'
    assert @@sv.inhibit_entity(:eqp_chamber, @@eqp, attrib: chamber, sublottypes: slt2)
    assert_equal 0, @@sv.lot_opelocate(lot, @@opNo)
    lotcas = nil
    assert @@sv.claim_process_lot(lot, eqp: @@eqp) {|cj| lotcas = @@sv.AMD_chambers_availability(cj)}
    lotcas.first.chamberAvailability.each {|ca|assert_equal '1', ca.AMD_ChamberStatus, 'wrong chamber status'}
  end

  def test52_2inhibits_sublottype_pd  # MES-2992
    assert @@sv.eqp_cleanup(@@eqp, mode: 'Auto-1')
    chamber = @@sv.eqp_info(@@eqp).chambers.collect {|c| c.chamber}[0]
    assert lot = @@svtest.new_lot, 'error creating test lot'
    @@testlots << lot
    # inhibit eqp+chamber+pd related to the tool but not lot -> available
    pd2 = 'UTPD000.02'
    lrcp2 = @@sm.object_info(:pd, pd2).first.specific[:lrcps][nil]
    mrcp2 = @@sm.object_info(:lrcp, lrcp2).first.specific[:recipe].keys.first
    assert @@sm.object_info(:recipe, mrcp2).first.specific[:eqps].member?(@@eqp), "wrong setup, #{@@eqp} not in #{mrcp2}"
    assert @@sv.inhibit_entity(:eqp_chamber_pd, [@@eqp, pd2], attrib: chamber)
    assert_equal 0, @@sv.lot_opelocate(lot, @@opNo)
    lotcas = nil
    assert @@sv.claim_process_lot(lot, eqp: @@eqp) {|cj| lotcas = @@sv.AMD_chambers_availability(cj)}
    lotcas.first.chamberAvailability.each {|ca|assert_equal '1', ca.AMD_ChamberStatus, 'wrong chamber status'}
    # additionally inhibit eqp+chamber for sublottype other than lot's one -> available
    slt2 = 'EO'
    refute_equal slt2, @@sv.lot_info(lot).sublottype, 'wrong sublottype'
    assert @@sv.inhibit_entity(:eqp_chamber, @@eqp, attrib: chamber, sublottypes: slt2)
    assert_equal 0, @@sv.lot_opelocate(lot, @@opNo)
    lotcas = nil
    assert @@sv.claim_process_lot(lot, eqp: @@eqp) {|cj| lotcas = @@sv.AMD_chambers_availability(cj)}
    lotcas.first.chamberAvailability.each {|ca|assert_equal '1', ca.AMD_ChamberStatus, 'wrong chamber status'}
  end

  def test53_2inhibits_sublottype_product  # MES-2992
    assert @@sv.eqp_cleanup(@@eqp, mode: 'Auto-1')
    chamber = @@sv.eqp_info(@@eqp).chambers.collect {|c| c.chamber}[0]
    assert lot = @@svtest.new_lot, 'error creating test lot'
    @@testlots << lot
    # inhibit eqp+chamber+product related to the tool but not lot -> available
    refute_equal @@product2, @@sv.lot_info(lot).product, 'wrong product'
    assert @@sv.inhibit_entity(:eqp_chamber_product, [@@eqp, @@product2], attrib: chamber)
    assert_equal 0, @@sv.lot_opelocate(lot, @@opNo)
    lotcas = nil
    assert @@sv.claim_process_lot(lot, eqp: @@eqp) {|cj| lotcas = @@sv.AMD_chambers_availability(cj)}
    lotcas.first.chamberAvailability.each {|ca|assert_equal '1', ca.AMD_ChamberStatus, 'wrong chamber status'}
    # additionally inhibit eqp+chamber for sublottype other than lot's one -> available
    slt2 = 'EO'
    refute_equal slt2, @@sv.lot_info(lot).sublottype, 'wrong sublottype'
    assert @@sv.inhibit_entity(:eqp_chamber, @@eqp, attrib: chamber, sublottypes: slt2)
    assert_equal 0, @@sv.lot_opelocate(lot, @@opNo)
    lotcas = nil
    assert @@sv.claim_process_lot(lot, eqp: @@eqp) {|cj| lotcas = @@sv.AMD_chambers_availability(cj)}
    lotcas.first.chamberAvailability.each {|ca|assert_equal '1', ca.AMD_ChamberStatus, 'wrong chamber status'}
  end


  # aux methods

  def cancel_inhibits
    SiView::MM::InhibitClasses.each_pair {|iclass, entities|
      assert_equal 0, @@sv.inhibit_cancel(iclass, @@eqp, silent: true) if entities.include?('Chamber') || entities.include?('Equipment')
    }
  end

  def doInhibits(chamberIndex, inhdata, inhibitedForLot)
    $log.info ">>> doInhibits chamberIndex=#{chamberIndex.inspect} entities=#{inhdata.inspect} inhibitedForLot=#{inhibitedForLot}"
    inhibited = Array.new(@@chambers.size, false)
    # no inhibit
    $log.info '--- no inhibit'
    cancel_inhibits
    doChamberStateLotCombinations(inhibited)
    $log.info "--- inhibit all sublot types"
    cancel_inhibits
    assert @@sv.inhibit_entity(inhdata[0], inhdata[1], inhdata[2])
    inhibited[chamberIndex] = inhibitedForLot
    doChamberStateLotCombinations(inhibited)
    # inhibit lot's sublot type only
    $log.info '--- inhibit sublottype of the lot only'
    cancel_inhibits
    slt = @@sv.lot_info(@lotID).sublottype
    assert @@sv.inhibit_entity(inhdata[0], inhdata[1], inhdata[2].merge(sublottypes: slt))
    inhibited[chamberIndex] = inhibitedForLot
    doChamberStateLotCombinations(inhibited)
    # inhibit all but lot's sublot type
    $log.info '--- inhibit all but sublottype of the lot'
    cancel_inhibits
    assert @@sv.inhibit_entity(inhdata[0], inhdata[1], inhdata[2].merge(sublottypes: @@sv.sublot_types('Production')-[slt]))
    inhibited[chamberIndex] = false
    doChamberStateLotCombinations(inhibited)
    # clean up
    cancel_inhibits
  end

  # basic test case for state combination test
  def doChamberStateLotCombinations(inhibited)
    inhibited ||= Array.new(@@chambers.size, false)
    $log.info ">>> doChamberStateLotCombinations lot: #{@lotID.inspect}"
    li = @@sv.lot_info(@lotID)
    assert li.nwafers > @@nchildren, "not enough wafers in lot #{@lotID}"
    carrier = li.carrier
    assert_equal 0, @@sv.carrier_xfer_status_change(carrier, 'EO', @@eqp)
    # SLR
    assert cj = @@sv.slr(@@eqp, carrier: carrier, lot: @lotID, port: @@port), 'error in SLR'
    # create list of E10 state combinations
    combinations = @@chamberstates.product(@@chamberstates)
    combinations.each_with_index {|sc, j|
      # state change for the other chambers
      otherstate = @@chamberstates[j % @@chamberstates.size]
      (@@chambers.size - 2).times {sc << otherstate}
    }
    # test
    combinations.each {|states|
      $log.info "-- state combination: #{states.inspect}"
      eqpi = @@sv.eqp_info(@@eqp)
      states.each_with_index {|s, j|
        next if eqpi.chambers[j].status == s  # .end_with?(s)
        assert_equal 0, @@sv.chamber_status_change(@@eqp, @@chambers[j], s.split('.')[1]) # 'PRD.1PRD' -> '1PRD'
      }
      verify_chamberstates(cj, states, inhibited)
    }
    # clean up
    assert_equal 0, @@sv.slr_cancel(@@eqp, cj)
  end


  def verify_chamberstates(cj, states, inhibited)
    assert lotcas = @@sv.AMD_chambers_availability(cj), "error executing AMD_chambers_availability for cj #{cj.inspect}"
    lotcas.each {|lotca|
      slt = @@sv.lot_info(lotca.lotId).sublottype
      $log.info "availability: #{lotca.chamberAvailability.collect {|c| [c.AMD_ChamberName, c.AMD_ChamberStatus]}}"
      states.each_with_index {|state, i|
        # get calculated (expected) availability
        assert entry = @@eqpstates[state], "unknown eqpstate #{state}"
        expected = !inhibited[i]
        if expected
          # apply conditional availability settings
          if entry.specific[:available]
            if entry.specific[:conditional]
              expected = @pd_condavailable && entry.specific[:sublottypes].member?(slt)
            end
          else
            expected = false
          end
        end
        # get actual availability
        available = lotca.chamberAvailability[i].AMD_ChamberStatus == '1'
        assert_equal expected, available, 'wrong chamber availability reported'
      }
    }
  end

end
