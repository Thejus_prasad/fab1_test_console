=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: Daniel Steger, 2014-08-26

History:
  2015-01-16 dsteger,  alias and lot priority check improved
  2015-03-12 dsteger,  new alias formats
  2015-10-28 sfrieske, added test for lots with empty slots
  2019-12-05 sfrieske, minor cleanup, renamed from Test004_WaferAlias
  2021-08-27 sfrieske, removed verify_equal

Notes:
  Test lot creation with alias (MSR176088) and priority check
  dropped customization MSR335442 "499999" => "0"; MES-2638
=end

require 'SiViewTestCase'


class MM_Lot_WaferAlias < SiViewTestCase
  # CS_SP_WAFER_ALIAS_NAME_FORMAT should be <##> for Fab1/8: 01, 02, 03
  # CS_SP_WAFER_ALIAS_NAME_FORMAT should be <LOT_FAMILY_ID_PFX>.<##> or "" for Fab7: Lot.01, Lot.02, Lot.03, ...
  # future format: CS_SP_WAFER_ALIAS_NAME_FORMAT is WAFERALIAS.<##>
  @@wafer_alias = '<##>'  # Fab7: "<LOT_FAMILY_ID_PFX>.<##>"
  @@epriority = '20000'
  @@priority = '1'

  @@testlots = []


  def self.after_all_passed
    @@testlots.each {|lot| @@sv.delete_lot_family(lot)}
  end


  def test00_setup
    $setup_ok = false
    #
    assert_equal @@wafer_alias, @@sv.environment_variable[1]['CS_SP_WAFER_ALIAS_NAME_FORMAT'], 'unexpected wafer alias format'
    #
    $setup_ok = true
  end

  def test01_production_lot
    assert lot = @@svtest.new_lot(priorityclass: @@priority, epriority: @@epriority)
    @@testlots << lot
    check_alias_by_slot(lot)
    check_priority(lot, @@priority, @@epriority)

    _walias = @@sv.lot_info(lot, wafers: true).wafers.sort {|w1, w2| w1.slot <=> w2.slot}.map {|w| w.alias}
    $log.info "old alias: #{_walias.join(' ')}"
    new_alias = _walias.shuffle
    # update the alias
    @@sv.wafer_alias_set(lot, aliases: new_alias)
    new_walias_set = @@sv.lot_info(lot, wafers: true).wafers.sort {|w1, w2| w1.slot <=> w2.slot}.map {|w| w.alias}
    $log.info "new alias: #{new_walias_set.join(' ')}"
    assert_equal new_alias, new_walias_set
    #TODO: add test to check alias set in FHWLTHS (MSR413458)
  end

  def test02_monitor_lots
    priority = @@sv.environment_variable(var: 'SP_CTRLLOT_PRTYCLASS')
    ['Equipment Monitor', 'Process Monitor', 'Dummy'].each {|t|
      assert lot = @@svtest.new_controllot(t), 'error creating test lot'
      @@testlots << lot
      check_alias_by_slot(lot)
      check_priority(lot, priority, (@@sv.release >= 15 ? '0' : '499999'))
    }
  end

  # fails in C7.1.1
  def test03_production_empty_slots
    # stb production lot from srclot with empty slots
    slots = [11, 12, 13, 17, 25]
    assert lot = @@svtest.new_lot(ondemand: false, nwafers: 5, slots: slots)
    @@testlots << lot
    # check aliases
    @@sv.lot_info(lot, wafers: true).wafers.each_with_index {|w, i| assert_equal slots[i], w.slot}
    check_alias_by_slot(lot)
  end

  # fails in C7.1.1
  def test04_monitor_empty_slots
    # stb monitor lot from srclot with empty slots
    slots = [11, 12, 13, 17, 25]
    assert lot = @@svtest.new_controllot('Process Monitor', ondemand: false, nwafers: 5, slots: slots)
    @@testlots << lot
    # check aliases
    @@sv.lot_info(lot, wafers: true).wafers.each_with_index {|w, i| assert_equal slots[i], w.slot}
    check_alias_by_slot(lot)
  end


  # aux methods

  # verify aliases are correctly set, starting with '01' in any case, MSR52807, MES-96
  def check_alias_by_slot(lot)
    # get wafers sorted by slot
    wafers = @@sv.lot_info(lot, wafers: true).wafers.sort {|w1, w2| w1.slot <=> w2.slot}
    alias_pattern = @@wafer_alias.sub('<##>', '%0.2d')
    alias_pattern = alias_pattern.sub('<LOT_FAMILY_ID_PFX>', lot.split('.').first) # lot family
    res = true
    wafers.each_with_index  {|w, i|
      # res &= verify_equal(alias_pattern % (i+1), w.alias, "wrong alias for #{w.wafer}")
      exp = alias_pattern % (i+1)
      $log.warn "wrong alias for #{w.wafer}, expected: #{exp}, got: #{w.alias}" if w.alias != exp
      w.alias == exp
    }
    assert res, "wrong alias for lot #{lot}"
  end

  def check_priority(lot, priority, epriority)
    linfo = @@sv.lot_info(lot)
    assert_equal epriority, linfo.epriority, " wrong external priority for #{lot}"
    assert_equal priority, linfo.priority, " wrong class for #{lot}"
  end


end