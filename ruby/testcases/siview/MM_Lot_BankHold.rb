=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: Daniel Steger, 2013-03-06

History:
  2019-12-05 sfrieske, minor cleanup, renamed from Test095_BankHold
  2020-08-10 sfrieske, improved use of lot_operation_history
  2022-01-07 sfrieske, fixed use of object_info(:reasoncode)
=end

require 'SiViewTestCase'


# Test BankHold/Release, incl vendor lots (MSR 563287) and secured reasoncodes (MSR 801120)
class MM_Lot_BankHold < SiViewTestCase
  # unsecured reason codes
  @@reason_code = 'LONG'
  @@reason_code2 = 'INT'
  # secured reason, X-UNITTST has privileges
  @@privilege = 'QualityHold'
  @@reason_secured = 'QA2'
  # restricted users
  @@user_nopriv = 'X-UNITHLDPRV'  # user w/o privileges for hold reason @@reason_secured
  @@user_nohold = 'X-UNITNOHOLD'  # user w/o privilege for category HOLD

  # TODO: test lot creation, @@testlots = []

  # def self.after_all_passed
  #   @@sv.eqp_cleanup(@@eqp)
  #   @@testlots.each {|lot| @@sv.delete_lot_family(lot)}
  # end


  def test00_setup
    $setup_ok = false
    #
    # Lot Types: Production, Vendor, Equipment Monitor, Process Monitor, Dummy
    # State: Completed
    #   Vendor lots: with wafer ids, without wafer ids
    #   Equipment monitor: with route, without route (STB Canceled)
    #   Process monitor: with route, without route (STB Canceled)
    #
    # verify privilege setup
    # hold privilege
    refute_empty oinfos = @@sm.object_info(:reasoncode, "BankHold.#{@@reason_secured}"), "undefined reason code #{@@reason_secured}"
    assert_equal @@privilege, oinfos.first.udata['PrivilegeID'], 'wrong privilege'
    # user w/o privileges for hold reason @@reason_secured
    @@svnopriv = SiView::MM.new($env, user: @@user_nopriv, password: :default)
    assert_equal 0, @@svnopriv.logon_check(subsystem: 'MM')
    assert priv = @@svnopriv._res.subSystemFuncLists.first.strFuncIDs.find {|e| e.functionID == @@privilege}
    assert_equal 'X', priv.permission, "user #{@@user_nopriv} must have no permission for #{@@privilege}"
    # user w/o privilege category HOLD
    @@svnohold = SiView::MM.new($env, user: @@user_nohold, password: :default)
    assert_equal 0, @@svnohold.logon_check(subsystem: 'MM')
    assert_nil @@svnopriv._res.subSystemFuncLists.first.strFuncIDs.find {|e| e.functionID == 'HOLDS'}
    #
    # get test lots, TODO: create!
    @@lots = []
    @@lots << @@sv.lot_list(lottype: 'Production', status: 'COMPLETED').first
    emonitors = @@sv.lot_list(lottype: 'Equipment Monitor', status: 'COMPLETED', lotinfo: true)
    @@lots << emonitors.select {|l| l.route.empty?}.first.lot
    @@lots << emonitors.select {|l| !l.route.empty?}.first.lot
    pmonitor = @@sv.lot_list(lottype: 'Process Monitor', status: 'COMPLETED', lotinfo: true)
    @@lots << pmonitor.select {|l| l.route.empty?}.first.lot
    @@lots << pmonitor.select {|l| !l.route.empty?}.first.lot
    dummy = @@sv.lot_list(lottype: 'Dummy', status: 'COMPLETED', lotinfo: true)
    @@lots << dummy.select {|l| !l.route.empty?}.first.lot
    vendor = @@sv.lot_list(lottype: 'Vendor', status: 'COMPLETED', lotinfo: true)
    @@lots << vendor.select {|l| l.nwafers > 25}.first.lot #likely to have no waferids
    vendor.select {|l| l.nwafers <= 25}.each do |vlot|
      if @@sv.lot_info(vlot.lot, wafers: true).wafers.inject(true) {|res, w| res && !w.wafer.empty?}
        @@lots << vlot.lot
        break
      end
    end
    $log.info "using lots #{@@lots.inspect}"
    assert_equal 8, @@lots.size, "error finding all test lots"
    #
    # cleanup test lots
    @@lots.each {|lot|
      @@sv.lot_hold_list(lot, type: 'BankHold').each {|h| @@sv.lot_bankhold_release(lot)}
    }
    #
    $setup_ok = true
  end

  def test01_bankhold
    tstart = Time.now
    sleep 3
    @@lots.each {|lot|
      # (unsecured) hold
      assert_equal 0, @@sv.lot_bankhold(lot, reason: @@reason_code), "error setting bank hold for lot #{lot}"
      assert_equal 'ONHOLD', @@sv.lot_info(lot).status, 'hold expected'
      # 2nd hold rejected
      assert_equal 206, @@sv.lot_bankhold(lot, reason: @@reason_code2), "wrong 2nd bank hold for lot #{lot}"
      assert @@sv.reason_text.include?('is already held')
      # release
      assert_equal 0, @@sv.lot_bankhold_release(lot), "error releasing bank hold for lot #{lot}"
      assert_equal 'NOTONHOLD', @@sv.lot_info(lot).states['Lot Hold State'], 'no hold expected'
    }
    $log.info "waiting 60 s for history watchdog"; sleep 60
    # check history
    @@lots.each {|lot|
      history = @@sv.lot_operation_history(lot, history: false, raw: true).select {|e|
        @@sv.siview_time(e.reportTimeStamp) > tstart && e.operationCategory == 'BankHold'
      }
      assert_equal 1, history.count, "wrong BankHold history for #{lot}"
    }
  end

  def test02_bankhold_privileges
    lot = @@lots[0]
    #
    # set secured hold, user w/o privilege for hold reason
    assert_equal 206, @@svnopriv.lot_bankhold(lot, reason: @@reason_secured), "wrong bank hold for lot #{lot}"
    assert @@svnopriv.reason_text.include?('does not have the Privilege')
    # set secured hold, user w/o priv category HOLD
    assert_equal 206, @@svnohold.lot_bankhold(lot, reason: @@reason_secured), "wrong bank hold for lot #{lot}"
    assert @@svnohold.reason_text.include?('does not have the Privilege')
    # set secured hold, user with privilege
    assert_equal 0, @@sv.lot_bankhold(lot, reason: @@reason_secured), "wrong bank hold for lot #{lot}"
    assert_equal 'ONHOLD', @@sv.lot_info(lot).states['Lot Hold State'], 'no hold expected'
    #
    # release secured hold, user w/o privilege for hold reason
    assert_equal 2056, @@svnopriv.lot_bankhold_release(lot), "wrong bankhold release for lot #{lot} by #{@@user_nopriv}"
    assert @@svnopriv.reason_text.include?('does not have the Privilege')
    # try to release secured hold, user w/o priv category HOLD
    assert_equal 2056, @@svnohold.lot_bankhold_release(lot), "wrong bankhold release for lot #{lot} by #{@@user_nohold}"
    assert @@svnohold.reason_text.include?('does not have the Privilege')
    # release secured hold, user with privilege
    assert_equal 0, @@sv.lot_bankhold_release(lot), "error releasing bank hold for lot #{lot}"
    assert_equal 'NOTONHOLD', @@sv.lot_info(lot).states['Lot Hold State'], 'no hold expected'
  end

end
