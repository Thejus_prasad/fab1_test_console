=begin 
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.
    
Author: Paul Peschel, 2013-01-10 migrate from Test050_NPW_PortStatusChange.java

History:
  2013-07-17 ssteidte, (re-)start dummy EI
  2019-12-09 sfrieske, minor cleanup, renamed from Test050_NPWPortStatusChange
  2020-09-07 sfrieske, minor cleanup
=end

require 'SiViewTestCase'
require 'dummytools'


# Test NPW reservation and port status changes
class MM_Eqp_NPWPortStatusChange < SiViewTestCase
  @@eqp = 'UTC001'
  
  
  def self.after_all_passed
    @@sv.delete_lot_family(@@lot)
  end


  def test00_setup
    $setup_ok = false
    #
    assert @@svtest.cleanup_eqp(@@eqp)
    @@port = @@sv.eqp_info(@@eqp).ports.first.port
    #
    assert @@lot = @@svtest.new_lot
    @@carrier = @@sv.lot_info(@@lot).carrier
    assert @@svtest.cleanup_carrier(@@carrier)
    #
    $setup_ok = true
  end
    
  def test10
    # set eqp mode Auto-2, port status LoadReq
    assert_equal 0, @@sv.eqp_mode_change(@@eqp, 'Auto-2')
    assert_equal 0, @@sv.eqp_port_status_change(@@eqp, @@port, 'LoadReq')
    # do NPW reserve
    assert_equal 0, @@sv.npw_reserve(@@eqp, @@port, @@carrier)
    assert check_reservation(true, 'Dispatched')
    # change port status to LoadComp
    assert_equal 0, @@sv.eqp_port_status_change(@@eqp, @@port, 'LoadComp')
    assert check_reservation(true, 'Dispatched')
    # load carrier and change port status to UnloadReq, reservation is deleted
    assert_equal 0, @@sv.carrier_xfer_status_change(@@carrier, 'EO',  @@eqp)
    assert_equal 0, @@sv.eqp_load(@@eqp, @@port, @@carrier)
    assert_equal 0, @@sv.eqp_port_status_change(@@eqp, @@port, 'UnloadReq')
    assert check_reservation(false, 'Required')
    # unload carrier and change port status to UnloadComp, reservation is still deleted
    assert_equal 0, @@sv.eqp_unload(@@eqp, @@port, @@carrier)
    assert_equal 0, @@sv.eqp_port_status_change(@@eqp, @@port, 'UnloadComp')
    assert check_reservation(false, 'Required')
     # change port status to LoadReq, still no reservation
    assert_equal 0, @@sv.eqp_port_status_change(@@eqp, @@port, 'LoadReq')
    assert check_reservation(false, 'Required')
    # clean up
    assert_equal 0, @@sv.eqp_cleanup(@@eqp)
    assert_equal 0, @@sv.eqp_mode_change(@@eqp, 'Off-Line-1')
  end


  def check_reservation(reserved, state)
    res = true
    pi = @@sv.eqp_info(@@eqp).ports.find {|p| p.port == @@port}
    res &= pi.disp_load_carrier == @@carrier if reserved
    res &= pi.disp_state == state
    return res
  end
end