=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: Daniel Steger, 2012-01-04

History:
  2015-10-26 sfrieske, minor cleanup
  2019-12-09 sfrieske, minor cleanup, renamed from Test410_Remeasure
  2021-08-27 sfrieske, removed verify_equal
=end

require 'SiViewTestCase'


# Test remeasure TX, MSRs 799186, 328785
class MM_Lot_Remeasure < SiViewTestCase
  @@product = 'UT-PROD-REMEASURE.01'
  @@route = 'UTRT-REMEASURE.01'

  @@op_process = '1000.1500'
  @@op_other = '1000.5000'
  @@op_measure = '1000.2000'
  @@op_measure_2 = '1000.3000'

  @@branch_route1 = 'UTBR001.01'
  @@psm_split_op1 = '1000.1500'
  @@psm_return_op1 = '1000.3000'
  @@psm_merge_op1 = '1000.4000'

  @@branch_route2 = 'UTBR002.01'
  @@psm_split_op2 = '1000.3000'
  @@psm_merge_op2 = '1000.4000'

  @@rework_route = 'UTRW-REMEASURE.01'
  @@rework_op = '1000.2000'
  @@rework_lastop = 'RW.2000'

  @@testlots = []


  def self.after_all_passed
    @@testlots.each {|lot| @@sv.delete_lot_family(lot)}
  end


  # create a new lot for each test since lot records need to be cleared
  def setup
    super
    assert @@lot = @@svtest.new_lot(product: @@product, route: @@route), "failed to create new lot"
    @@testlots << @@lot
  end


  def test01_locate_to_metrology
    assert_equal 0, @@sv.lot_opelocate(@@lot, @@op_measure)
    assert_pdtype(@@lot)
    assert_equal 0, @@sv.lot_opelocate(@@lot, 1), "failed to locate"
    op_after = @@sv.lot_info(@@lot).opNo
    #
    assert_equal 1468, @@sv.lot_remeasure(@@lot), "remeasurement is not allowed (since no previous metro)"
    assert_equal 0, @@sv.lot_remeasure(@@lot, force: true), "failed to do forced remeasurement"
    assert verify_remeasured(@@lot, op_after)
  end

  def test02_gatepass_metrology
    assert_equal 0, @@sv.lot_opelocate(@@lot, @@op_measure)
    assert_pdtype(@@lot)
    assert_equal 0, @@sv.lot_gatepass(@@lot), "failed to gatepass"
    op_after = @@sv.lot_info(@@lot).opNo
    #
    assert_equal 0, @@sv.lot_remeasure(@@lot), "failed to do remeasurement"
    assert verify_remeasured(@@lot, op_after)
    #
    assert_equal 0, @@sv.lot_opelocate(@@lot, 2), "failed to locate after 2nd measurement"
    assert_equal 1468, @@sv.lot_remeasure(@@lot), "remeasurement is not allowed (since no previous metro)"
    #
    assert_equal 0, @@sv.lot_opelocate(@@lot, -1), "failed to locate to 1st measurement"
    assert_equal 0, @@sv.lot_remeasure(@@lot), "failed to do remeasurement"
    assert verify_remeasured(@@lot, op_after)
  end

  def test03_process_metrology
    assert_equal 0, @@sv.lot_opelocate(@@lot, @@op_measure_2)
    assert_pdtype(@@lot)
    assert @@sv.claim_process_lot(@@lot), "failed to gatepass"
    op_after = @@sv.lot_info(@@lot).opNo
    #
    assert_equal 0, @@sv.lot_remeasure(@@lot), "failed to do remeasurement"
    assert verify_remeasured(@@lot, op_after)
  end

  def test04_chain_metrology
    assert_equal 0, @@sv.lot_opelocate(@@lot, @@op_measure)
    op_after = 3.times.collect {
      assert_pdtype(@@lot)
      assert @@sv.claim_process_lot(@@lot), "failed to process lot"
      @@sv.lot_info(@@lot).opNo
    }
    3.times {|i|
      assert_equal 0, @@sv.lot_remeasure(@@lot), "failed to do remeasurement"
      assert verify_remeasured(@@lot, op_after[2-i])
    }
  end

  def test05_psm_split_join_merge
    assert_equal 0, @@sv.lot_experiment(@@lot, 2, @@branch_route1, @@psm_split_op1, @@psm_return_op1, :merge=>@@psm_merge_op1)
    assert_equal 0, @@sv.lot_opelocate(@@lot, @@psm_split_op1), "failed to locate to branch op"
    assert child1 = @@sv.lot_family(@@lot)[-1], "failed to execute experiment"

    assert_equal 0, @@sv.lot_opelocate(@@lot, @@psm_return_op1), "failed to locate to merge operation"
    assert_equal 0, @@sv.lot_gatepass(@@lot), "failed to gatepass"
    assert_equal 0, @@sv.lot_opelocate(child1, @@psm_return_op1), "failed to locate to join operation"
    assert_equal 1468, @@sv.lot_remeasure(child1), "remeasurement is not allowed (since lot came from branch)"
    #
    assert_equal 0, @@sv.lot_opelocate(child1, @@psm_merge_op1), "failed to locate to merge operation"
    check_automerge(@@lot, child1)
    op_after = @@sv.lot_info(@@lot).opNo
    assert_equal 0, @@sv.lot_remeasure(@@lot), "failed to do remeasurement"
    assert verify_remeasured(@@lot, op_after)
  end

  def test06_psm_join_equal_merge
    assert_equal 0, @@sv.lot_experiment(@@lot, 2, @@branch_route1, @@psm_split_op1, @@psm_return_op1)
    assert_equal 0, @@sv.lot_opelocate(@@lot, @@psm_split_op1), "failed to locate to branch op"
    assert child1 = @@sv.lot_family(@@lot)[-1], "failed to execute experiment"

    # gate pass parent to join operation
    assert_equal 0, @@sv.lot_gatepass(@@lot), "failed to gatepass"
    assert_equal 0, @@sv.lot_gatepass(@@lot), "failed to gatepass"
    #
    assert_equal 0, @@sv.lot_opelocate(child1, @@psm_return_op1), "failed to locate to join operation"
    check_automerge(@@lot, child1)
    op_after = @@sv.lot_info(@@lot).opNo
    assert_equal 0, @@sv.lot_remeasure(@@lot), "failed to do remeasurement" # might actually be a gap here!
    assert verify_remeasured(@@lot, op_after)
  end

  def test07_branch_all
    nwafers = @@sv.lot_info(@@lot).nwafers
    assert_equal 0, @@sv.lot_experiment(@@lot, nwafers, @@branch_route2, @@psm_split_op2, @@psm_split_op2, merge: @@psm_merge_op2)
    assert_equal 0, @@sv.lot_opelocate(@@lot, @@op_measure), "failed to locate to branch op"
    assert_pdtype(@@lot)
    assert_equal 0, @@sv.lot_gatepass(@@lot), "failed to gatepass to branch op"
    #
    op_after = @@sv.lot_info(@@lot).opNo
    assert_equal 0, @@sv.lot_opelocate(@@lot, @@psm_split_op2), "failed to locate to join operation"
    assert_equal 0, @@sv.lot_remeasure(@@lot), "remeasurement is allowed (although lot came from branch)"
    #
    #assert_ok @@sv.lot_gatepass(@@lot), "failed to gate pass"
    #op_after = @@sv.lot_info(@@lot).opNo
    #assert_equal 0, @@sv.lot_remeasure(@@lot), "failed to do remeasurement"
    assert verify_remeasured(@@lot, @@psm_split_op2)
  end

  # Rework is not mentioned in the MSR / same as PSM
  def test08_rework
    assert_equal 0, @@sv.lot_opelocate(@@lot, @@rework_op)
    assert_pdtype(@@lot)
    assert_equal 0, @@sv.lot_gatepass(@@lot), "failed to gatepass"
    op_after = @@sv.lot_info(@@lot).opNo
    #
    assert_equal 0, @@sv.lot_rework(@@lot, @@rework_route, return_opNo: op_after), "failed to start rework"
    assert_equal 0, @@sv.lot_opelocate(@@lot, @@rework_lastop), "failed to locate to last operation of rework"
    assert_equal 0, @@sv.lot_gatepass(@@lot), "failed to gatepass"
    assert_equal 0, @@sv.lot_remeasure(@@lot), "remeasurement is not allowed (since lot came from branch)"
  end

  def test10_other_pdtypes
    assert_equal 0, @@sv.lot_opelocate(@@lot, @@op_process)
    assert_pdtype(@@lot, "Process")
    assert_equal 0, @@sv.lot_gatepass(@@lot), "failed to gatepass"
    assert_equal 11601, @@sv.lot_remeasure(@@lot), "Remeasurement is not allowed for process!"
    #
    assert_equal 0, @@sv.lot_opelocate(@@lot, @@op_other)
    assert_pdtype(@@lot, "Other")
    assert_equal 0, @@sv.lot_gatepass(@@lot), "failed to gatepass"
    assert_equal 11601, @@sv.lot_remeasure(@@lot), "Remeasurement is not allowed for process!"
  end


  # check if current PD is measurment PD
  def assert_pdtype(lot, pdtype='Measurement')
    pd, version = @@sv.lot_info(lot).op.split('.')
    refute_empty @@sv.module_list(id: pd, version: version, type: pdtype, level: 'Operation'), "#{pd} type is not #{pdtype}"
  end

  def verify_remeasured(lot, op_after)
    $log.info "verify_remeasured #{lot.inspect}, #{op_after.inspect}"
    li = @@sv.lot_info(lot)
    ops = @@sv.lot_operation_list(lot)
    index = ops.find_index {|i| i.opNo == op_after}
    exp_ope_no = ops[index-1].opNo
    # verify_equal exp_ope_no, li.opNo, "Lot should be at operation #{exp_ope_no}"
    $log.warn "wrong opNo, expected: #{exp_ope_no}, got: #{li.opNo}" if li.opNo != exp_ope_no
    return (li.opNo == exp_ope_no)
  end

  # merge check for different fabs used by multiple tests
  def check_automerge(lot, child)
    if @@sv.f7 && !child.nil? # check for automerge
      assert_equal 'EMPTIED', @@sv.lot_info(child).status, "#{child} should have been merged with parent automatically"
    else
      # merge any remaining child (even if nil given)
      assert_equal 0, @@sv.lot_merge(lot, child), "failed to merge #{child} to #{lot}"
    end
  end

end
