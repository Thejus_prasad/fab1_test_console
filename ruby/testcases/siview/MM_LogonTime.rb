=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: ssteidte, 2011-11-18

History:
  2014-10-28 ssteidte, separated from LogonCheck
  2015-05-22 ssteidte, cleanup, verified with R15
  2018-03-06 sfrieske, replaced user_parameter_set_verify by user_paramer_change
  2019-12-05 sfrieske, minor cleanup, renamed from Test001_LogonTime
  2021-08-31 sfrieske, requiring 'time' for #iso8601
=end

require 'time'
require 'RubyTestCase'


# Verify TxLogonCheck with OJT, MSR 589373
class MM_LogonTime < RubyTestCase
  @@user_ojt = 'OJT-UNITTEST'
  @@count = 1000


  def self.shutdown
    @@sv.user_parameter_delete('User', @@user_ojt, 'DisabledByOJTFlag', check: true)
  end


  def test04_last_logon_time_logon
    assert_equal 0, @@sv.user_parameter_delete('User', @@sv.user, 'LastLogOnTime', check: true), 'error deleting logon time'
    lastlogon = Time.now - 10   # allow small differences between computer times
    $log.info "running #{@@count} times..."
    @@count.times {
      assert_equal 0, @@sv.logon_check(subsystem: 'MM'), 'logon failed'
      ts = @@sv.user_parameter('User', @@sv.user, name: 'LastLogOnTime')
      assert ts.valueflag, "LastLogOnTime parameter is not set"
      logontime = @@sv.siview_time(ts.value)
      assert lastlogon <= logontime, "LastLogOnTime should have been updated after #{lastlogon.utc.iso8601(3)}, but is #{logontime.utc.iso8601(3)}"
      lastlogon = logontime
      sleep 0.1
    }
  end

  def test05_ojt_logon
    assert_equal '1', @@sv.environment_variable[1]['CS_SP_OJT_FUNC_ENABLED'], 'OJT logon is not enabled'
    #
    assert_equal 0, @@sv.user_parameter_delete('User', @@user_ojt, 'DisabledByOJTFlag', check: true), 'OJT should be disabled'
    assert_equal 0, @@sv.logon_check(user: @@user_ojt, password: :default), 'user should be able to logon'
    #
    assert_equal 0, @@sv.user_parameter_change('User', @@user_ojt, 'DisabledByOJTFlag', 0, 1, datatype: 'INTEGER'), 'OJT should be disabled'
    assert_equal 0, @@sv.logon_check(user: @@user_ojt, password: :default), 'user should be able to logon'
    #
    assert_equal 0, @@sv.user_parameter_change('User', @@user_ojt, 'DisabledByOJTFlag', 1, 1, datatype: 'INTEGER'), 'OJT should be disabled'
    assert_equal 12203, @@sv.logon_check(user: @@user_ojt, password: :default), "user should not be able to logon"
    #
    assert_equal 0, @@sv.user_parameter_change('User', @@user_ojt, 'DisabledByOJTFlag', 0, 1, datatype: 'INTEGER'), 'OJT should be disabled'
    assert_equal 0, @@sv.logon_check(user: @@user_ojt, password: :default), 'user should be able to logon'
  end

  def testmanual06_ojt_logon_CS_SP_OJT_FUNC_ENABLED0
    # This test is normally not executed! It needs to be started separately after setting CS_SP_OJT_FUNC_ENABLED to 0.
    #
    assert_equal '0', @@sv.environment_variable[1]['CS_SP_OJT_FUNC_ENABLED'], "OJT logon is not disabled"
    #
    assert_equal 0, @@sv.user_parameter_delete('User', @@user_ojt, 'DisabledByOJTFlag', check: true), 'OJT should be disabled'
    assert_equal 0, @@sv.logon_check(user: @@user_ojt, password: :default), 'user should be able to logon'
    #
    assert_equal 0, @@sv.user_parameter_change('User', @@user_ojt, 'DisabledByOJTFlag', 0, 1, datatype: 'INTEGER'), 'OJT should be disabled'
    assert_equal 0, @@sv.logon_check(user: @@user_ojt, password: :default), 'user should be able to logon'
    #
    assert_equal 0, @@sv.user_parameter_change('User', @@user_ojt, 'DisabledByOJTFlag', 1, 1, datatype: 'INTEGER'), 'OJT should be disabled'
    assert_equal 0, @@sv.logon_check(user: @@user_ojt, password: :default), "user should not be able to logon"
    #
    assert_equal 0, @@sv.user_parameter_change('User', @@user_ojt, 'DisabledByOJTFlag', 0, 1, datatype: 'INTEGER'), 'OJT should be disabled'
    assert_equal 0, @@sv.logon_check(user: @@user_ojt, password: :default), 'user should be able to logon'
  end

end
