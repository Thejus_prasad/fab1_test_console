=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: Beate Kochinka

History:
  2015-08-12 dsteger,  merged and added Fab7 config
  2016-02-16 dsteger,  memo check for expired qtimes
  2016-04-06 sfrieske, separate checks for qtime action and action execution; added tests for MSR977934
  2017-01-05 sfrieske, use new RTD::EmuRemote
  2019-12-09 sfrieske, minor cleanup, renamed from Test601_QTime
  2020-08-13 sfrieske, cleanup
  2021-08-27 sfrieske, require uniquestring instead of misc
=end

require 'util/uniquestring'
require 'SiViewTestCase'


# Q-time related tests, MSR535439, MSR502340, MSR757239, MSR768261, MSR977934 (LimitOnly)
class MM_Lot_QTime < SiViewTestCase
  @@sv_defaults = {product: 'UT-PROD-QTIME.01', route: 'UTRT-QTIME.01'}
  @@op_trigger = '1000.1200'      # operation that triggers Q-time
  @@op_target = '2000.1500'       # QT target operation
  @@sleeptime = 20
  @@route2 = 'UTRT-QTIME.02'      # new route
  @@product2 = 'UT-PROD-QTIME.02' # new product
  @@product3 = 'UT-PROD-QTIME.03' # new product with same route as default product
  @@qtime_hold = 'QTOV'
  @@epsilon = 20   # allowed difference for qtime calculations
  @@qt_expiration = 315
  @@stop_outof_qzone = 'ON'
  @@inactive_qtime = '1'
  # Q-times as set up in SM
  @@qtimes = {'1000.1000'=>'1000.1100', '1000.1200'=>'2000.1500',
    '2000.2000'=>['2000.2500', '2100.1000'], '2500.1000'=>'2500.1100'}
  @@eqp = 'UTC003'

  @@testlots = []


  def self.shutdown
    if $env == 'itdc'
      @@rtdremote.delete_emustation('QT_LOTS_WITH_QT')
      @@rtdremote.delete_emustation('QT_LOTS_WITHOUT_QT')
    end
  end

  def self.after_all_passed
    @@testlots.each {|lot| @@sv.delete_lot_family(lot)}
  end


  def test00_setup
    $setup_ok = false
    #
    if $env == 'itdc'
      # prevent the QT JCAP job from taking the action
      require 'rtdserver/rtdemuremote'
      @@rtdremote = RTD::EmuRemote.new($env)
      @@rtdremote.add_emustation('QT_LOTS_WITH_QT', method: 'empty_reply')
      @@rtdremote.add_emustation('QT_LOTS_WITHOUT_QT', method: 'empty_reply')
    end
    # correct mm config
    env_vars = @@sv.environment_variable
    assert_equal '1', env_vars[1]['SP_KEEP_QTIME_ON_SCHCHANGE'], 'wrong MM env'
    assert_equal '0', env_vars[1]['SP_KEEP_QTIME_ACTION_ON_CLEAR'], 'wrong MM env'
    assert_equal '1', env_vars[1]['SP_SHORTEST_QTIME_USE_FLAG'], 'wrong MM env'
    assert_equal '2', env_vars[1]['SP_QTIMEINFO_MERGE_RULE'], 'wrong MM env'
    assert_equal '1', env_vars[1]['SP_POSTPROC_FOR_LOT_FLAG'], 'wrong MM env'
    assert_equal @@inactive_qtime, env_vars[1]['SP_INACTIVE_QTIME_LIST'], 'wrong MM env'
    assert_equal @@stop_outof_qzone, env_vars[1]['CS_SP_STOP_QACTION_OUTOF_QZONE'], 'wrong MM env'
    #
    assert @@svtest.cleanup_eqp(@@eqp, mode: 'Off-Line-1')
    #
    $setup_ok = true
  end

  def test01_qtime_expires_processing
    assert lot = @@svtest.new_lot, 'error creating test lot'
    @@testlots << lot
    qtime_expires(lot, '1000.1200', @@qtimes['1000.1200'], true, [:hold])
    assert @@sv.lot_operation_in_qtime(lot, @@sv.lot_info(lot).opNo).inQTimeZoneFlag, 'wrong QT indication'
  end

  def test02_qtime_expires_actions
    skip "action not appliable to Fab7" if @@sv.f7
    assert lot = @@svtest.new_lot, 'error creating test lot'
    @@testlots << lot
    qtime_expires(lot, '1000.1000', @@qtimes['1000.1000'], false, [:hold, :futurehold, :rework])
  end

  def test03_qtime_expires_limitonly
    assert lot = @@svtest.new_lot, 'error creating test lot'
    @@testlots << lot
    qtime_expires(lot, '2500.1000', @@qtimes['2500.1000'], false, [:limitonly])
    assert_empty @@sv.lot_hold_list(lot)
    assert_empty @@sv.lot_futurehold_list(lot)
  end

  def test04_qtime_expires_gatepass_actions
    skip "action not appliable to Fab7" if @@sv.f7
    assert lot = @@svtest.new_lot, 'error creating test lot'
    @@testlots << lot
    qtime_expires(lot, '1000.1000', @@qtimes['1000.1000'], false, [:hold, :futurehold, :rework])
    # release hold and process outside qtime
    assert_equal 0, @@sv.lot_hold_release(lot, @@qtime_hold)
    assert_equal 0, @@sv.lot_opelocate(lot, 3)
    assert_equal 0, @@sv.lot_gatepass(lot), "gatepass failed"
    #
    assert_empty @@sv.lot_futurehold_list(lot, reason: @@qtime_hold)
    assert_empty @@sv.lot_future_rework_list(lot)
  end

  def test05_qtime_expires_future_hold_rework_cleared
    skip "action not appliable to Fab7" if @@sv.f7
    assert lot = @@svtest.new_lot, 'error creating test lot'
    @@testlots << lot
    qtime_expires(lot, '1000.1000', @@qtimes['1000.1000'], false, [:hold])
    # process outside qtime
    assert_equal 0, @@sv.lot_opelocate(lot, 2)
    assert @@sv.claim_process_lot(lot), 'error processing lot'
    #
    assert_empty @@sv.lot_futurehold_list(lot, reason: @@qtime_hold)
    assert_empty @@sv.lot_future_rework_list(lot)
  end

  def test10_qtimelookup
    assert lot = @@svtest.new_lot, 'error creating test lot'
    @@testlots << lot
    @@qtimes.each_pair {|trigger, targets|
      refute @@sv.lot_operation_in_qtime(lot, trigger).inQTimeZoneFlag, "trigger PRE should not have qtime, #{trigger}"
      assert @@sv.lot_operation_in_qtime(lot, trigger,
        hold_type: 'FutureHold', ope_info: 'POST').inQTimeZoneFlag, "trigger POST should have qtime, #{trigger}"
      targets = [targets] unless targets.kind_of?(Array)
      targets.each {|target|
        assert @@sv.lot_operation_in_qtime(lot, target).inQTimeZoneFlag, "target not in qtime, #{target}"
      }
    }
  end

  # min. Q-time will be used if lots are merged (process parent before child lot)
  def test11_parent_first
    assert lot = @@svtest.new_lot, 'error creating test lot'
    @@testlots << lot
    assert child = @@sv.lot_split(lot, 1)
    [lot, child].each {|l| assert_equal 0, @@sv.lot_opelocate(l, @@op_trigger)}
    assert_equal 0, @@sv.lot_opelocate(lot, @@op_trigger)
    do_sequence_test(lot, child, true)
  end

  # min. Q-time will be used if lots are merged (process child before parent lot)
  def test12_child_first
    assert lot = @@svtest.new_lot, 'error creating test lot'
    @@testlots << lot
    assert child = @@sv.lot_split(lot, 1)
    [lot, child].each {|l| assert_equal 0, @@sv.lot_opelocate(l, @@op_trigger)}
    do_sequence_test(lot, child, false)
  end

  def test13_qtime_inherited
    assert lot = @@svtest.new_lot, 'error creating test lot'
    @@testlots << lot
    assert_equal 0, @@sv.lot_opelocate(lot, @@op_trigger)
    # process lot at trigger operation
    assert @@sv.claim_process_lot(lot)
    # check Q-time
    qtFirst = get_qtime(lot, target_opNo: @@op_target, trigger_opNo: @@op_trigger)
    assert_equal 1, qtFirst.size, "no Q-time information found for #{lot}"

    # split the lot
    assert @@child = @@sv.lot_split(lot, 1), "no child lot created"
    qtSecond = get_qtime(@@child, target_opNo: @@op_target, trigger_opNo: @@op_trigger)
    assert_equal 1, qtSecond.size, "no Q-time information found for #{@@child}"
    qtFirst = time_delta(qtFirst.first.qrestrictionRemainTime)
    qtSecond = time_delta(qtSecond.first.qrestrictionRemainTime)

    assert_in_delta qtFirst, qtSecond, @@epsilon, "wrong remaining Q-time"
    # merge lots and check Q-times again
    assert_equal 0, @@sv.lot_merge(lot, @@child)
    qtParentAfter = get_qtime(lot, target_opNo: @@op_target, trigger_opNo: @@op_trigger)
    qtChildAfter = get_qtime(@@child, target_opNo: @@op_target, trigger_opNo: @@op_trigger)
    assert_equal 1, qtParentAfter.size, "no Q-time information found for #{lot}"
    qtParentAfter = time_delta(qtParentAfter.first.qrestrictionRemainTime)
    assert qtParentAfter <= qtFirst, "remaining Q-time of lot #{lot} (#{qtParentAfter}) is greater than value before merge (#{qtFirst})"

    # difference should not be > epsilon
    assert_in_delta qtFirst, qtParentAfter, @@epsilon, "wrong remaining Q-time"
    assert_equal 0, qtChildAfter.size, "no Q-time information expected for #{@@child}"
  end

  # changing lot's priority keeps Q-time (MSR535439)
  def test21_change_priority
    assert lot = @@svtest.new_lot, 'error creating test lot'
    @@testlots << lot
    assert_equal 0, @@sv.lot_opelocate(lot, @@op_trigger)
    assert @@sv.claim_process_lot(lot)
    qt1 = get_qtime(lot, target_opNo: @@op_target, trigger_opNo: @@op_trigger)
    time1 = Time.now
    assert_equal 1, qt1.size, "no Q-time information for #{lot}"
    oldPrio = @@sv.lot_info(lot).priority
    newPrio = (oldPrio.to_i + 1) % 5
    $log.info "  changing priority for #{lot} from #{oldPrio} to #{newPrio}"
    @@sv.schdl_change(lot, priority: newPrio)
    refute_equal @@sv.lot_info(lot).priority, oldPrio, "priority of #{lot} was not changed"
    qt2 = get_qtime(lot, target_opNo: @@op_target, trigger_opNo: @@op_trigger)
    assert_equal 1, qt2.size, "missing Q-time information for #{lot} after prio change"
    verify_qtdiff(qt1.first, qt2.first, Time.now - time1)
  end

  # Q-Time is not removed if route is the same (MSR535439)
  def test22_change_product
    assert lot = @@svtest.new_lot, 'error creating test lot'
    @@testlots << lot
    assert_equal 0, @@sv.lot_opelocate(lot, @@op_trigger)
    assert @@sv.claim_process_lot(lot)
    qt1 = get_qtime(lot, target_opNo: @@op_target, trigger_opNo: @@op_trigger)
    assert_equal 1, qt1.size, "missing Q-time information for #{lot}"
    @@sv.schdl_change(lot, route: @@svtest.route, product: @@product3)
    assert_equal @@product3, @@sv.lot_info(lot).product, "schedule change failed"
    qt2 = get_qtime(lot, target_opNo: @@op_target, trigger_opNo: @@op_trigger)
    assert_equal 1, qt2.size, "found no Q-time information for #{lot} after product change"
  end

  # changing lot's route (and product) removes Q-time (MSR535439)
  def test23_change_route
    assert lot = @@svtest.new_lot, 'error creating test lot'
    @@testlots << lot
    assert_equal 0, @@sv.lot_opelocate(lot, @@op_trigger)
    assert @@sv.claim_process_lot(lot)
    qt1 = get_qtime(lot, target_opNo: @@op_target, trigger_opNo: @@op_trigger)
    assert_equal 1, qt1.size, "missing Q-time information for #{lot}"
    @@sv.schdl_change(lot, route: @@route2, product: @@product2)
    assert_equal @@route2, @@sv.lot_info(lot).route, "schedule change failed"
    qt2 = get_qtime(lot, target_opNo: @@op_target, trigger_opNo: @@op_trigger)
    assert_equal 0, qt2.size, "found Q-time information for #{lot} after route change"
  end

  # requeuing a lot keeps its Q-time information
  def test31_requeue
    assert lot = @@svtest.new_lot, 'error creating test lot'
    @@testlots << lot
    assert_equal 0, @@sv.lot_opelocate(lot, @@op_trigger)
    assert @@sv.claim_process_lot(lot)
    qt1 = get_qtime(lot, target_opNo: @@op_target, trigger_opNo: @@op_trigger)
    assert_equal 1, qt1.size, "missing Q-time information for #{lot}"
    time1 = Time.now
    assert_equal 0, @@sv.lot_requeue(lot), "requeue failed"
    qt2 = get_qtime(lot, target_opNo: @@op_target, trigger_opNo: @@op_trigger)
    assert_equal 1, qt2.size, "missing Q-time information for #{lot} after requeue"
    verify_qtdiff(qt1.first, qt2.first, Time.now - time1)
  end

  # changing a lot's order keeps its Q-time information
  def test32_mfgorder_change
    assert lot = @@svtest.new_lot, 'error creating test lot'
    @@testlots << lot
    assert_equal 0, @@sv.lot_opelocate(lot, @@op_trigger)
    assert @@sv.claim_process_lot(lot)
    qt1 = get_qtime(lot, target_opNo: @@op_target, trigger_opNo: @@op_trigger)
    assert_equal 1, qt1.size, "missing Q-time information for #{lot}"
    time1 = Time.now
    assert_equal 0, @@sv.lot_mfgorder_change(lot, sublottype: 'RD', customer: 'ibm',
      order: unique_string, owner: 'gmstest01', epriority: '334455'), "changing lot order failed"
    assert_equal 'RD', @@sv.lot_info(lot).sublottype
    qt2 = get_qtime(lot, target_opNo: @@op_target, trigger_opNo: @@op_trigger)
    assert_equal 1, qt2.size, "missing Q-time information for #{lot} after mfg order change"
    verify_qtdiff(qt1.first, qt2.first, Time.now - time1)
  end

  def test40_module_change_qtimes
    assert lot = @@svtest.new_lot, 'error creating test lot'
    @@testlots << lot
    process_qtimes(lot)
  end

  def test50_gatepass_qtimes
    assert lot = @@svtest.new_lot, 'error creating test lot'
    @@testlots << lot
    process_qtimes(lot, true)
  end

  def test51_locate_outside
    assert lot = @@svtest.new_lot, 'error creating test lot'
    @@testlots << lot
    locate_qtime(lot, false)
  end

  def test52_reactivate_locate
    assert lot = @@svtest.new_lot, 'error creating test lot'
    @@testlots << lot
    locate_qtime(lot, true)
  end


  # aux methods

  def locate_qtime(lot, qtime_action)
    trigger = '1000.1200'
    target = @@qtimes[trigger]
    assert_equal 0, @@sv.lot_opelocate(lot, trigger), "cannot move lot to trigger operation"
    assert @@sv.claim_process_lot(lot)
    assert_equal 1, get_qtime(lot, trigger_opNo: trigger, target_opNo: target).size, "Q-time should be set"

    assert_equal 0, @@sv.lot_opelocate(lot, :first), "failed to locate the lot"
    assert_equal 1, get_qtime(lot, trigger_opNo: trigger, target_opNo: target).size, "Q-time should be set"
    assert_equal @@stop_outof_qzone == 'ON',  !@@sv.lot_info(lot).qtime, "Q-time flag should be #{@@stop_outof_qzone=='ON' ? 'No' : 'Yes'}"

    if qtime_action
      $log.info "wait #{@@epsilon} s outside before moving back..."; sleep @@epsilon
      assert_equal 0, @@sv.lot_opelocate(lot, target), "failed to locate the lot to target operation"
      assert_equal 1, get_qtime(lot, trigger_opNo: trigger, target_opNo: target).size, "Q-time should be set"
      $log.info "waiting up to #{@@qt_expiration} s for the Q-time to expire"
      assert wait_for(timeout: @@qt_expiration) {
        qtime = get_qtime(lot, trigger_opNo: trigger)
        assert_equal 1, qtime.size, "Q-time should be cleared"
        !@@sv.lot_info(lot).qtime && qtime.first.qrestrictionRemainTime.start_with?('-')
      }, "Q-time for #{lot} not expired after #{@@qt_expiration} s"
    else
      $log.info "waiting #{@@qt_expiration} s for the Q-time to expire"; sleep @@qt_expiration
    end
    # qtime action
    assert_equal qtime_action ? 1 : 0, @@sv.lot_hold_list(lot, reason: @@qtime_hold).count, "expected hold"
  end

  # # parses qrestrictionRemainTime of form "+/-HH:MM:SS"
  def time_delta(s)
    time = 0
    m = s.match(/(\+|-)?(\d\d):(\d\d):(\d\d)/)
    if m
      time = m[2].to_i * 3600 + m[3].to_i * 60 + m[4].to_i
      time = -time if m[1] == '-'
    end
    return time
  end

  def qtime_expires(lot, trigger, target, in_processing, action_type)
    assert @@sv.lot_opelocate(lot, trigger), "Cannot move lot to trigger operation"
    assert @@sv.claim_process_lot(lot)
    assert_equal 1, get_qtime(lot, trigger_opNo: trigger, target_opNo: target).size, "Qtime should be set"
    if in_processing
      assert cj = @@sv.claim_process_lot(lot, eqp: @@eqp, nounload: true, noopecomp: true), "cannot process lot"
      assert_equal 1, get_qtime(lot, trigger_opNo: trigger, target_opNo: target).size, "Qtime should be set"
    end

    qtinfo = nil
    $log.info "waiting #{@@qt_expiration} s for the QT to expire"; sleep @@qt_expiration
    assert wait_for(timeout: 2 * @@epsilon) {
      res = get_qtime(lot, trigger_opNo: trigger)
      assert_equal 1, res.size , "Qtime should be set"
      qtinfo = res.first
      qtinfo.qrestrictionRemainTime.start_with?('-')   # < 0
    }, "QT for #{lot} not expired after #{@@qt_expiration + 2 * @@epsilon} s"

    if in_processing
      # complete operation
      assert_equal 0, @@sv.eqp_data_speccheck(@@eqp, cj), "failed to do spec check"
      assert_equal 0, @@sv.eqp_opecomp(@@eqp, cj), "failed to complete operation"
      assert_equal 0, @@sv.eqp_unload(@@eqp, nil, @@sv.lot_info(lot).carrier)
    end
    duration = @@sv.lot_operation_in_qtime(lot, @@sv.lot_info(lot).opNo).expiredTimeDuration
    sleep 30 # for the watchdog to act

    # verify QT actions are set, with memo (MSR757239, MES-1949)
    if action_type.member?(:hold)
      assert hold = @@sv.lot_hold_list(lot, reason: @@qtime_hold).first, "expected hold"
      assert_equal expected_memo(lot, duration, qtinfo, :hold), hold.memo, "wrong claim memo"
    end
    if action_type.member?(:futurehold)
      assert fhold = @@sv.lot_futurehold_list(lot, reason: @@qtime_hold).first, "expected hold"
      assert_equal expected_memo(lot, duration, qtinfo, :futurehold), fhold.memo, "wrong claim memo"
    end
    if action_type.member?(:rework)
      assert rw = @@sv.lot_future_rework_list(lot).first, "expected future rework"
    end

    # verify QT action execution, with memo
    if action_type.member?(:hold)
      assert_equal 0, @@sv.lot_hold_release(lot, nil)
    end
    if action_type.member?(:futurehold)
      assert_equal 0, @@sv.lot_opelocate(lot, fhold.rsp_op)
      assert hold = @@sv.lot_hold_list(lot, reason: @@qtime_hold).first, "expected hold"
      assert_equal expected_memo(lot, duration, qtinfo, :futurehold), hold.memo, "wrong claim memo"
      assert_equal 0, @@sv.lot_hold_release(lot, nil)
    end
    if action_type.member?(:rework)
      assert_equal 0, @@sv.lot_opelocate(lot, rw.operationNumber), "failed to locate to rework operation"
      rwk = nil
      assert wait_for {
        rwk = @@sv.lot_operation_history(lot, route: @@svtest.route, opNo: trigger, category: 'Rework').first
      }
      assert_equal expected_memo(lot, duration, qtinfo, :rework), rwk.memo, "wrong claim memo"
    end
  end

  def expected_memo(lot, duration, qtinfo, action_type)
    $log.info "expected_memo #{lot}, #{duration.inspect}  (#{action_type.inspect})"
    assert qtaction = qtinfo.strQtimeActionInfo.find {|qt|
      if action_type == :hold
        qt.qrestrictionAction == 'ImmediateHold'
      elsif action_type == :futurehold
        qt.qrestrictionAction == 'FutureHold'
      elsif action_type == :rework
        qt.qrestrictionAction == 'FutureRework'
      else
        true # any
      end
    }, "no action found for #{lot} and #{action_type}"
    return "QTime exceed; Criticality=#{qtaction.customField}; Limit=#{duration} min; " +
      "TriggerTime=#{qtinfo.qrestrictionTriggerTimeStamp}, " +
      "Trigger=#{qtinfo.qrestrictionTriggerRouteID.identifier}.#{qtinfo.qrestrictionTriggerOperationNumber}, " +
      "Target=#{qtinfo.qrestrictionTargetRouteID.identifier}.#{qtinfo.qrestrictionTargetOperationNumber}"
  end

  def get_qtime(lot, params={})
    li = @@sv.lot_info(lot)
    qinfo = @@sv.lot_qtime_list(lot)
    qtime = qinfo.find {|q| q.routeID.identifier == li.route && q.operationNumber == li.opNo}
    return [] unless qtime
    qdetails = qtime.strQtimeInfo
    qdetails = qdetails.select {|d| d.qrestrictionTriggerOperationNumber == params[:trigger_opNo]} if params[:trigger_opNo]
    qdetails = qdetails.select {|d| d.qrestrictionTargetOperationNumber == params[:target_opNo]} if params[:target_opNo]
    return qdetails
  end

  def process_qtimes(lot, gatepass=false)
    @@qtimes.each_pair {|trigger, targets|
      assert @@sv.lot_opelocate(lot, trigger), "Cannot move lot to trigger operation"
      if gatepass
        assert_equal 0, @@sv.lot_gatepass(lot)
      else
        assert @@sv.claim_process_lot(lot)
      end
      targets = [targets] unless targets.kind_of?(Array)
      targets.each {|target|
        assert_equal 1, get_qtime(lot, trigger_opNo: trigger, target_opNo: target).size, "Qtime should be set for #{trigger} to #{target}"
        li = @@sv.lot_info(lot)
        $log.info "--- #{li.opNo}"
        while li.opNo != target
          assert @@sv.claim_process_lot(lot), "Cannot process lot"
          assert_equal 1, get_qtime(lot, trigger_opNo: trigger, target_opNo: target).size, "Qtime should be set for #{trigger} to #{target}"
          li = @@sv.lot_info(lot)
          assert li.qtime, "Qtime should be set"
        end
      }
      if gatepass
        assert_equal 0, @@sv.lot_gatepass(lot)
      else
        assert @@sv.claim_process_lot(lot), "Cannot process lot"
      end
      assert_empty get_qtime(lot, trigger_opNo: trigger), "Qtime should be cleared"
      refute @@sv.lot_info(lot).qtime, "Qtime should be cleared"
    }
  end

  # do processing, merge and checks
  # parameter defines if to process parent or child lot first
  def do_sequence_test(lot, child, parentFirst=true)
    first, second = parentFirst ? [lot, child] : [child, lot]
    $log.info "do_sequence_test #{first} - #{second}"
    # process lots at trigger operation: first - sleep - second
    assert @@sv.claim_process_lot(first)
    $log.info "... sleeping #{@@sleeptime} s"; sleep(@@sleeptime)
    assert @@sv.claim_process_lot(second)
    # check Q-times
    qtFirst = get_qtime(first, target_opNo: @@op_target, trigger_opNo: @@op_trigger)
    qtSecond = get_qtime(second, target_opNo: @@op_target, trigger_opNo: @@op_trigger)
    assert_equal 1, qtFirst.size, "no Q-time information found for #{first}"
    assert_equal 1, qtSecond.size, "no Q-time information found for #{second}"
    qtFirst = time_delta(qtFirst.first.qrestrictionRemainTime)
    qtSecond = time_delta(qtSecond.first.qrestrictionRemainTime)
    assert qtFirst < qtSecond - @@sleeptime, 'remaining QT of first lot too high'
    # merge lots and check Q-times again
    assert_equal 0, @@sv.lot_merge(lot, child)
    qtParentAfter = get_qtime(lot, target_opNo: @@op_target, trigger_opNo: @@op_trigger)
    qtChildAfter = get_qtime(child, target_opNo: @@op_target, trigger_opNo: @@op_trigger)
    assert_equal 1, qtParentAfter.size, "no Q-time information found for #{first}"
    qtParentAfter = time_delta(qtParentAfter.first.qrestrictionRemainTime)
    assert qtParentAfter <= qtFirst, 'remaining QT of parent lot too high'
    # differences should not be > 1s
    assert_in_delta qtFirst, qtParentAfter, @@epsilon, 'remaining QT of first lot too high'
    assert_equal 0, qtChildAfter.size, "no Q-time information expected for #{second}"
  end

  # check if Q-times qt1 and qt2 difference (in seconds)
  # is in the interval of the time stamps qt1 and qt2 difference +- epsilon
  # for "correct" results, measure as follows:
  # ---+----+--------+----+---------> time
  #   qt1 time1     qt2 time2
  def verify_qtdiff(qtinfo1, qtinfo2, timeDiff)
    qtDiff = time_delta(qtinfo1.qrestrictionRemainTime) - time_delta(qtinfo2.qrestrictionRemainTime)
    assert (timeDiff - @@epsilon <= qtDiff and qtDiff <= timeDiff + @@epsilon), "wrong Q-time difference"
    $log.info "ok: Q-time difference #{qtDiff}s is in the interval of #{timeDiff} +- #{@@epsilon}"
  end

end
