=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: Daniel Steger, 2015-08-13

History
  2016-02-10 dsteger,  remove copy on vendor lot receive MES-2121
  2017-12-08 sfrieske, cleanup
  2019-05-02 sfrieske, minor cleanup
  2019-12-05 sfrieske, minor cleanup, renamed from Test082_LotLabel
  2020-09-15 sfrieske, minor cleanup
  2021-01-15 sfrieske, moved SiView::MM#new_srclot_for_product to SiView::Test#new_srclot
  2021-08-30 sfrieske, moved siviewmmdb.rb to siview/mmdb.rb

Notes
  TODO: merge with MM_Lot_Udata
=end

require 'siview/mmdb'
require 'SiViewTestCase'


class MM_Lot_Label < SiViewTestCase
  @@sequence = '0000'
  @@invalid_label = 'NOLABEL'
  @@sublottype = nil     # "IBM" # for the vendor lot, needs numeric split logic
  @@plantcode = 'T'
  @@fab_plantcode = 'J'  # K for Fab8, J for Fab1, C for Fab7, T for IBM
  @@split_loops = 20
  @@customer = 'ibm'     # needs numeric split logic
  @@rework_route = 'UTRW001.01'
  @@rework_opNo = '1000.400'
  @@rework_return_opNo = '1000.400'
  @@psm_route = 'UTBR001.01'
  @@psm_split_opNo = '1000.200'
  @@psm_return_opNo = '1000.300'
  @@hold_reason = 'AEHL'
  @@fpc_product = 'UT-PROD-FPC.01'
  @@fpc_eqp = 'UTR001'
  @@fpc_mrecipe = 'P.UTMR000.01'
  @@fpc_opNo = '1010.1100'
  @@fpc_mgr_opNo = '1010.1200'

  @@duplicate_check = false
  @@testlots = []

  def self.startup
    super
    # @@vendorlot = nil
    @@own_fab = @@plantcode == @@fab_plantcode
    @@first_splitcode = @@own_fab ? '01' : 'ZZ'
    @@last_splitcode = nil   # start uninitialized
  end

  def self.after_all_passed
    @@testlots.each {|lot| @@sv.delete_lot_family(lot)}
  end


  def test00_setup
    $setup_ok = false
    #
    assert_equal 'IBMULL', @@sv.user_data(:customer, @@customer)['CustomLIG_LotIdFormat'], 'wrong customer lot format'
    assert_equal @@fab_plantcode, @@sv.environment_variable[1]['CS_SP_ULL_FAB_PLANTCODE_CHAR'], 'wrong fab_plantcode'
    unless @@own_fab
      assert @@sv.environment_variable[1]['CS_SP_ULL_INHERIT_FAB_PLANTCODE_LIST'].include?(@@plantcode), 'wrong MM env setup'
    end
    # get next free ULL family
    @@mmdb = SiView::MMDB.new($env)
    set_next_sequence
    #
    $setup_ok = true
  end


  def test10_vendorlotcreation_split
    $setup_ok = false
    #
    lotlabel = @@own_fab ? '' : get_lotlabel(split_code: '00')
    assert ec = @@svtest.get_empty_carrier, 'no empty carrier'
    assert srclot = @@svtest.new_srclot(lotlabel: lotlabel, sublottype: @@sublottype, carrier: ec), 'error creating vendorlot'
    @@testlots << srclot
    assert_equal lotlabel, @@sv.lot_label(srclot), "failed to verify lot label creation" if @@sv.f7
    assert @@vendorlot = @@sv.vendorlot_prepare(srclot, ec), "failed to prepare vendor lot"
    @@testlots << @@vendorlot
    unless @@own_fab
      # Prepare will not copy over the lot label, need to set it (accepted behaviour)
      assert_equal 0, @@sv.user_data_update(:lot, @@vendorlot, {'LOT_LABEL'=>lotlabel}), "failed to set lot label"
      #
      children = @@split_loops.times.collect {
        assert child = @@sv.lot_split_notonroute(@@vendorlot, 2), "failed to split #{@@vendorlot}"
        assert_equal 0, @@sv.lot_merge_notonroute(@@vendorlot, child), "failed to merge #{child} into #{@@vendorlot}"
        child
      }
      children.each {|child|
        assert_equal get_lotlabel(next_split: true), @@sv.lot_label(child), 'wrong lot label'
      }
    end
    #
    $setup_ok = true
  end

  def test11_copy_on_stb
    $setup_ok = false
    #
    assert @@vendorlot, 'need to execute test10 first'
    assert @@lot = @@svtest.new_lot(product: @@svtest.product, srclot: @@vendorlot, customer: @@customer), 'error creating lot'
    @@testlots << @@lot
    assert_equal get_lotlabel, @@sv.lot_label(@@lot), 'wrong lot label'
    #
    $setup_ok = true
  end

  def test12_split_actions
    assert lot = @@lot, 'need to execute tests 10 and 11 first'
    children = []
    # Normal split
    children << [@@sv.lot_split(lot, 1), @@sv.tx_id]
    # Onhold split
    assert_equal 0, @@sv.lot_hold(lot, @@hold_reason), "Hold failed"
    # MSR717973: SplitWithoutLotHoldRelease
    children << [@@sv.lot_split(lot, 1, onhold: true), @@sv.tx_id]
    children << [@@sv.lot_split(lot, 1, onhold: true, release: true), @@sv.tx_id]
    # Auto split
    children <<  [@@sv.AMD_WaferSorterAutoSplitReq(lot, 1), @@sv.tx_id]
    assert_equal 0, @@sv.lot_experiment(lot, 1, @@psm_route, @@psm_split_opNo, @@psm_return_opNo), "Failed to setup PSM"
    2.times {|i|
      assert_equal 0, @@sv.lot_experiment_reset(lot, @@psm_split_opNo), "Failed to reset PSM" if i > 0
      assert_equal 0, @@sv.lot_opelocate(lot, @@psm_split_opNo), "Failed to do PSM split"
      sublot = @@sv.lot_family(lot)[-1]
      children << [sublot, 'PSM']
      [lot, sublot].each {|lot| assert_equal 0, @@sv.lot_opelocate(lot, @@psm_return_opNo), "Failed to locate to return" }
      check_automerge(lot, sublot)
    }
    assert @@sv.lot_experiment_delete(lot), "Failed to clear PSM"
    # Partial Rework
    assert_equal 0, @@sv.lot_opelocate(lot, @@rework_opNo), "Failed to locate to rework"
    child = @@sv.lot_partial_rework(lot, 1, @@rework_route, return_opNo: @@rework_return_opNo)
    children << [child, @@sv.tx_id]
    assert_equal 0, @@sv.lot_partial_rework_cancel(lot, child), "Failed to merge after rework"
    # Partial Rework w/o hold release
    assert_equal 0, @@sv.lot_hold(lot, @@hold_reason), "Hold failed"
    child = @@sv.lot_partial_rework(lot, 1, @@rework_route, return_opNo: @@rework_return_opNo, onhold: true)
    children << [child, @@sv.tx_id]
    assert_equal 0, @@sv.lot_partial_rework_cancel(lot, child), "Failed to merge after rework"
    # Partial Rework with hold release
    child = @@sv.lot_partial_rework(lot, 1, @@rework_route, return_opNo: @@rework_return_opNo, onhold: true, release: true)
    children << [child, @@sv.tx_id]
    assert_equal 0, @@sv.lot_partial_rework_cancel(lot, child), "Failed to merge after rework"
    #
    # verify created childrens' lot labels
    children.each {|child, tx|
      assert_equal get_lotlabel(next_split: true), @@sv.lot_label(child), "wrong lot label for #{tx}"
    }
    # Merge
    assert @@sv.merge_lot_family(lot, nosort: true)
  end

  def test13_receive_child
    assert @@last_splitcode, 'need to execute split test12 first'
    lotlabel = get_lotlabel(split_code: '02')
    assert ec = @@svtest.get_empty_carrier, 'no empty carrier'
    srclot = @@svtest.new_srclot(lotlabel: lotlabel, sublottype: @@sublottype, carrier: ec)
    if @@own_fab
      assert_equal 10938, @@sv.rc, "no lot should have been received #{@@sv.msg_text}"
      return
    end
    assert srclot, "failed to create vendor lot"
    @@testlots << srclot
    assert_equal lotlabel, @@sv.lot_label(srclot), 'wrong lot label' if @@sv.f7
    assert vendor_lot = @@sv.vendorlot_prepare(srclot, ec), "failed to prepare vendor lot"
    @@testlots << vendor_lot
    ## Prepare will not copy over the lot label - need to set it again
    assert_equal 0, @@sv.user_data_update(:lot, vendor_lot, {'LOT_LABEL'=>lotlabel}), "failed to set lot label"
    # another vendor lot split
    assert child = @@sv.lot_split_notonroute(vendor_lot, 2), "failed to split #{vendor_lot}"
    assert_equal 0, @@sv.lot_merge_notonroute(vendor_lot, child), "failed to merge #{child} into #{vendor_lot}"
    lot_label2 = get_lotlabel(next_split: true)
    assert_equal lot_label2, @@sv.lot_label(child), 'wrong lot label'
    # another split after STB
    assert lot = @@sv.new_lot_release(product: @@svtest.product, srclot: vendor_lot, customer: @@customer), "failed to create new lot"
    assert @@sv.stb(lot)
    assert_equal lotlabel, @@sv.lot_label(lot), 'wrong lot label'
    assert child2 = @@sv.lot_split(lot, 2), "failed to split #{lot}"
    lot_label3 = get_lotlabel(next_split: true)
    assert_equal lot_label3, @@sv.lot_label(child2), 'wrong lot label'
    @@testlots << lot
  end

  def test14_split_loop
    assert lot = @@lot, "need to run setup first"
    children = (@@sv.lot_info(@@lot).nwafers-1).downto(1).collect {|i|
      assert lot = @@sv.lot_split(lot, i), "failed to split #{@@lot}"
      lot
    }
    children.each {|child|
      assert_equal get_lotlabel(next_split: true), @@sv.lot_label(child), 'wrong lot label'
    }
    # Merge
    assert @@sv.merge_lot_family(@@lot, nosort: true)
  end

  def test15_vendorlot_prepare_cancel
    set_next_sequence(2)  # 2 is only effective if @@own_fab
    assert vlot = @@svtest.new_srclot(sublottype: @@sublottype), 'error creating vendor lot'
    @@testlots << vlot
    # prepare does not copy the lot label - need to set it new
    assert label = get_lotlabel(split_code: '00')
    assert_equal 0, @@sv.user_data_update(:lot, vlot, {'LOT_LABEL'=>label}), 'error setting lot label'
    assert_equal 0, @@sv.wafer_sort(vlot, ''), 'error removing lot from carrier'
    assert res = @@sv.vendorlot_preparation_cancel(vlot), 'vendorlot_preparation_cancel failed'
    res.strReceivedLotInfoSeq.each {|e|
      lot = e.newLotID.identifier
      @@testlots << lot
      # TODO: label preserved since C7.17 ??
      #assert_empty @@sv.lot_label(lot), "lot label not removed for #{lot}"
    }
  end

  def test20_invalid_label_on_vendor
    skip 'only relevant in Fab7' unless @@sv.f7
    do_invalid_actions(true) {|lotlabel|
      @@svtest.new_srclot(lotlabel: lotlabel, sublottype: @@sublottype)
    }
  end

  def test22_invalid_label_on_prod_split
    assert lot = @@svtest.new_lot, 'error creating test lot'
    @@testlots << lot
    do_invalid_actions {|lotlabel|
      assert_equal 0, @@sv.user_data_update(:lot, lot, {'LOT_LABEL'=>lotlabel}), "failed to set lot label"
      @@sv.lot_split(lot, 2)
    }
  end

  def test23_invalid_label_on_vendor_split
    do_invalid_actions {|lotlabel|
      assert vlot = @@svtest.new_srclot(sublottype: @@sublottype), "failed to create vendor lot"
      assert_equal 0, @@sv.user_data_update(:lot, vlot, {'LOT_LABEL'=>lotlabel}), "failed to set lot label to #{vlot}"
      @@sv.lot_split_notonroute(vlot, 2)
    }
  end

  def test30_split_received_child_vendor
    skip "not applicable in own fab" if @@own_fab
    vlot, lotlabel = create_new_vlot(split_code: '99')
    #
    assert child = @@sv.lot_split_notonroute(vlot, 2), "failed to split child for #{vlot}"
    assert_equal get_lotlabel(split_code: @@first_splitcode), @@sv.lot_label(child), 'wrong lot label'
  end

  def test31_split_received_child
    set_next_sequence
    assert lot = @@svtest.new_lot, 'error creating test lot'
    @@testlots << lot
    lotlabel = get_lotlabel(split_code: '99')
    assert_equal 0, @@sv.user_data_update(:lot, lot, {'LOT_LABEL'=>lotlabel}), "failed to set lot label"
    assert child = @@sv.lot_split(lot, 2), "failed to split child for #{lot}"
    assert_equal get_lotlabel(split_code: @@first_splitcode), @@sv.lot_label(child), 'wrong lot label'
  end

  def test32_copy_on_stb_child
    skip "not applicable in own fab" if @@own_fab
    vlot, lotlabel = create_new_vlot(split_code: '97')
    assert lot = @@sv.new_lot_release(product: @@svtest.product, srclot: vlot, customer: @@customer, nwafers: 1), "failed to create new lot"
    @@testlots << lot
    assert @@sv.stb(lot)
    assert_equal get_lotlabel(split_code: @@first_splitcode), @@sv.lot_label(lot), 'wrong lot label'
    assert lot = @@sv.new_lot_release(product: @@svtest.product, srclot: vlot, customer: @@customer, nwafers: 24), "failed to create new lot"
    @@testlots << lot
    assert @@sv.stb(lot)
    assert_equal lotlabel, @@sv.lot_label(lot), 'wrong lot label'
  end

  def test33_nocopy_on_stb_cancel
    skip "not applicable in own fab" if @@own_fab
    vlot, lotlabel = create_new_vlot(split_code: '97')
    assert lot = @@sv.new_lot_release(product: @@svtest.product, srclot: vlot, customer: @@customer), "failed to create new lot"
    @@testlots << lot
    assert @@sv.stb(lot)
    assert_equal lotlabel, @@sv.lot_label(lot), 'wrong lot label'
    assert lot = @@sv.stb_cancel(lot), "failed to cancel STB"
    @@testlots << lot
    if @@sv.f7
      assert_equal lotlabel, @@sv.lot_label(lot), 'wrong lot label'
    else
      assert_empty @@sv.lot_label(lot), 'wrong lot label'
    end
  end

  def test40_stb_from_multiple_sourcelots
    skip "not applicable in own fab" if @@own_fab
    vlots = [create_new_vlot(next_split: true).first]
    2.times do
      assert child = @@sv.lot_split_notonroute(vlots[0], 2), "failed to split child for #{vlots[0]}"
      vlots << child
    end
    assert plot = @@sv.new_lot_release(product: @@svtest.product, customer: @@customer, srclot: vlots), "new lot request expected"
    vlots.each {|lot|
      assert_nil @@sv.stb(plot), "STB for #{plot} should have failed"
      assert_equal 10318, @@sv.rc, "error expected: cannot inherit multiple lot labels [#{@@sv.msg_text}]"
      assert_equal 0, @@sv.user_data_delete(:lot, lot, 'LOT_LABEL')
    }
    assert @@sv.stb(plot), "STB for #{plot} should have succeeded"
    @@testlots << plot
    lotlabel = @@sv.lot_label(plot)
    assert_match /\A[0-9A-Z]{4}#{@@fab_plantcode}0{5}\z/, lotlabel, "wrong lot label generated"
    assert clots = @@sv.stb_cancel(plot), "failed to do STB cancel"
    @@testlots += clots
    clots.each_with_index {|l, i|
      if @@sv.f7
        assert_equal i == 0 ? lotlabel : 'Undefined', @@sv.lot_label(l), "wrong lot label after STB cancel for #{l}"
      else
        assert_empty @@sv.lot_label(l), "wrong lot label after STB cancel for #{l}"
      end
    }
  end

  def xxxtest50_fpc_by_wafer
    skip "FPC not enabled" if @@sv.environment_variable[1]['SP_FPC_ADAPTATION_FLAG'] != '1'
    assert fpc_lot = @@svtest.new_lot(product: @@fpc_product), "failed to get FPC lot"
    @@testlots << fpc_lot
    assert fpc_id = @@sv.fpc_update(fpc_lot, @@fpc_opNo, wafers: 2, eqp: @@fpc_eqp, mrecipe: @@fpc_mrecipe), "failed to create FPC"
    set_next_sequence
    lot_label = get_lotlabel(split_code: '97')
    assert_equal 0, @@sv.user_data_update(:lot, fpc_lot, {'LOT_LABEL'=>lot_label})
    assert_equal 0, @@sv.lot_opelocate(fpc_lot, @@fpc_opNo), "failed to locate to FPC operation"
    child = @@sv.lot_family(fpc_lot).last
    assert_equal get_lotlabel(split_code: @@first_splitcode), @@sv.lot_label(child), 'wrong lot label'
  end

  def test60_stb_monitor
    assert mlot = @@svtest.new_controllot('Dummy'), "failed to create monitor lot"
    @@testlots << mlot
    if @@sv.f7
      lot_label = @@sv.lot_label(mlot)
      assert_match /\A[0-9A-Z]{4}#{@@fab_plantcode}0{5}\z/, lot_label, "wrong lot label generated"
    else
      assert_empty @@sv.lot_label(mlot), 'wrong lot label'
    end
  end


  # IBM rule to decrement counter on split
  def next_seq_desc(inp)
    seq = String.new(inp)
    (seq.length - 1).downto(0) {|i|
      if seq[i] == '0'
        seq[i] = 'Z'
      elsif seq[i] == 'A'
        seq[i] = '9'
        return seq
      else
        a = seq[i].ord
        begin
          a -= 1
        end while (a.chr =~ /[IO]/)
        seq[i] = a.chr
        return seq
      end
    }
    return seq
  end

  # generated rule to increment on split
  def next_seq(inp)
    seq = String.new(inp)
    (seq.length - 1).downto(0) {|i|
      if seq[i] == 'Z'
        seq[i] = '0'
      elsif seq[i] == '9'
        seq[i] = 'A'
        return seq
      else
        a = seq[i].ord
        begin
          a += 1
        end while (a.chr =~ /[IO]/)
        seq[i] = a.chr
        return seq
      end
    }
    return seq
  end

  # Generate a valid lot label for IBM
  # ULL : SSSSFWWBBM
  # SSSS: Sequence Number. Alphanumeric except I and O. "0000 - ZZZZ”
  # F: Fab Plant Code
  # WW: Child from a Wafer Lot.
  # BB: BAT Plant Code. “00”
  # M: Module Code ”0”
  def get_lotlabel(params={})
    if params.has_key?(:split_code)
      split_code = params[:split_code]
    elsif params.has_key?(:next_split)
      if @@last_splitcode.nil?
        @@last_splitcode = @@first_splitcode.clone
      else
        @@last_splitcode = @@own_fab ? next_seq(@@last_splitcode) : next_seq_desc(@@last_splitcode)
      end
      split_code = @@last_splitcode
    else
      # no split, get latest ull sequence for lot
      if @@own_fab
        @@sequence = base34(@@mmdb.ull_sequence)
        @@last_splitcode = nil
      end
      split_code = '00'
    end
    return String.new("#{@@sequence}#{@@plantcode}#{split_code}000")
  end

  def set_next_sequence(inc=1)
    @@sequence = @@own_fab ? base34(@@mmdb.ull_sequence + inc) : @@mmdb.free_ull_sequence(plantcode: @@plantcode, sequence: @@sequence)
    $log.info "  new sequence: #{@@sequence}"
  end

  # convert decimal number to base 34 for ULL
  def base34(n)
    ullCharacters = '0123456789ABCDEFGHJKLMNPQRSTUVWXYZ'
    out = ''
    while n > 34
      r = n % 34
      n = n / 34
      out = ullCharacters[r] + out
    end
    $log.info "  code: #{out}"
    return '0' * (3 - out.length) + ullCharacters[n] + out
  end

  def create_new_vlot(params)
    set_next_sequence
    lotlabel = get_lotlabel(params)
    assert vlot = @@svtest.new_srclot(lotlabel: lotlabel, sublottype: @@sublottype)
    @@testlots << vlot
    assert_equal 0, @@sv.user_data_update(:lot, vlot, {'LOT_LABEL'=>lotlabel})
    return vlot, lotlabel
  end

  # call block to provoke a lot label error and verify
  def do_invalid_actions(receive=false, stb=false, &blk)
    # too long ULL
    assert_nil blk.call('AXFDSFD' * 5), "error expected instead of success [#{@@sv.msg_text}]"
    _rc = receive || stb ? 10937 : 10926
    assert_equal _rc, @@sv.rc, "expected: Lot Label should be in ten character length [#{@@sv.msg_text}]"
    # duplicate ULL
    if @@duplicate_check
      assert @@last_splitcode, "need to run test1x first"
      assert_nil blk.call("#{@@sequence}#{@@plantcode}#{@@last_splitcode}000"), "error expected"
      assert_equal 11235, @@sv.rc, "expected: Lot Label should not be duplicate [#{@@sv.msg_text}]"
    end
    # wrong plant code
    if receive || stb
      saved_split_code = @@last_splitcode
      lotlabel = get_lotlabel(next_split: true)
      lotlabel[4] = 'x'
      assert_nil blk.call(lotlabel), 'error expected'
      assert_equal 10938, @@sv.rc, "expected: Fab Plant Code is not found [#{@@sv.msg_text}]"
      @@last_splitcode = saved_split_code  # restore split id since the split is supposed to fail
    end
    # empty ULL
    assert lot = blk.call(''), "no error expected"
    @@testlots << lot
    if stb
      # lot label is generated with Fab code (on vendor lot receive)
      assert_match /\A[0-9A-Z]{4}#{@@fab_plantcode}0{5}\z/, @@sv.lot_label(lot), 'wrong lot label'
    else
      assert_equal @@sv.f7 ? 'Undefined' : '', @@sv.lot_label(lot), 'lot label not removed'
    end
  end

  # merge check for different fabs used by multiple tests
  def check_automerge(lot, child)
    if @@sv.f7 && !child.nil? # check for automerge
      assert_equal 'EMPTIED', @@sv.lot_info(child).status, "#{child} should have been merged with parent automatically"
    else
      # merge any remaining child (even if nil given)
      assert_equal 0, @@sv.lot_merge(lot, child), "failed to merge #{child} to #{lot}"
    end
  end

end
