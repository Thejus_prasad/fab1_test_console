=begin 
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.
    
Author: sfrieske, 2016-05-01

History:
  2019-12-05 sfrieske, minor cleanup
  2022-01-04 sfrieske, require 'waitfor'
=end

require 'util/waitfor'
require 'SiViewTestCase'

# batch route release returns 'Released' early, MES-2999
class SM_RouteRelease < SiViewTestCase
  @@rte0 = 'F1QA-ECM-A-RTE0000.0000'
  @@nroutes = 2
  @@thread = nil
  
  def self.shutdown
    unless @@thread.nil?
      @@thread.kill
      @@thread.join
    end
  end


  def test11
    rte = @@rte0
    routes = @@nroutes.times.collect {|i| rte = rte.next}
    @@sm.delete_objects(:mainpd, routes)
    # create new routes
    assert @@sm.copy_object(:mainpd, @@rte0, routes, release: false)
    # release the routes
    @@thread = Thread.new {assert @@sm.release_objects(:mainpd, routes), 'error releasing routes'}
    # try to delete routes individually as soon as they are released
    routes.each {|route|
      assert wait_for(sleeptime: 1) {@@sm.object_info(:mainpd, route).first.state == 'Released'}
      assert @@sm.delete_production_consistency_check(:mainpd, route)
      assert @@sm.delete_production(:mainpd, route)
    }
    #
    @@thread.join
  end
  
end
