=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: Daniel Steger, 2012-11-07

History:
        added script parameter history
  2015-06-23 ssteidte, minor fixes for robustness, verified with R15
  2018-03-06 sfrieske, replaced user_parameter_delete_verify by user_paramer_delete
  2018-03-16 sfrieske, cleaned up calls to claim_process_lot
  2019-12-09 sfrieske, minor cleanup, renamed from Test450_CSScript
  2021-08-27 sfrieske, split verify_equal off of misc.rb
=end

require 'siview/mmdb'
require 'util/verifyequal'
require 'SiViewTestCase'


# Test CSS scripting. Requires SM setup.
# MSR277601, MSR612416, MSR222668 (R10.1.1 core), MSR431208 (R14 core)
class MM_Lot_CSScript < SiViewTestCase
  @@product = 'UT-PROD-SCRIPT.01'
  @@route = 'UTRT-SCRIPT.01'
  @@nonprobank = 'UT-NONPROD'
  @@opnum = '1000.2000'
  @@opnum_skip = '1000.4000'
  @@reasoncode = 'ENG'
  @@pd = 'BANKIN.01' # PD for wich UDATA is queried in script
  @@eqp = 'UTC003'
  @@branch_route1 = 'e2403-UT-SCRIPT-A0.01'
  @@psm_split_op1 = '1000.2000'
  @@psm_return_op1 = '1000.8000'
  @@psm_merge_op1 = '1000.9000'

  @@branch_route2 = 'e2403-UT-SCRIPT-A1.01'
  @@psm_split_op2 = 'e1000.2000'
  @@psm_return_op2 = 'e1000.3000'

  @@rework_route = 'UTRW-SCRIPT.01'
  @@rework_op = '1000.3000'

  @@ooc_op = '2000.1000'
  @@loc_skip_op = '2000.3000'
  @@loc_skip_op2 = '2000.5000'

  @@source = 'MDS' # MDS or SiView
  @@timeout = 90
  @@spread = 3     # account for minimal time differences between test console and DB server


  def self.after_all_passed
    @@sv.delete_lot_family(@@lot)
  end


  def test00_setup
    $setup_ok = false
    #
    assert_equal '1', @@sv.environment_variable[1]['SP_POSTPROC_CHAINED_MODE'], "chain mode required to test MSR431208"
    assert @@sv.environment_variable[1]['SP_POSTPROC_MAX_CHAIN_EXEC'].to_i >= 3, "chain exec required to test MSR431208"
    @@historydb = SiView::MMDB.new($env, use_mds: @@source == 'MDS')
    # eqp and lot cleanup
    assert @@lot = @@svtest.new_lot(product: @@product, route: @@route), "failed to create new lot"
    assert_equal 0, @@sv.eqp_cleanup(@@eqp)
    #
    $setup_ok = true
  end

  def test02_script_process
    assert_equal 0, @@sv.lot_cleanup(@@lot, opNo: @@opnum)
    reset_scriptparameters(@@lot)
    li = @@sv.lot_info(@@lot)

    # trigger pre-1 by lotholdrelease
    assert_equal 0, @@sv.lot_hold(@@lot, @@reasoncode), "failed to set lot hold"
    assert_equal 0, @@sv.lot_hold_release(@@lot, nil), "failed to release lot"
    assert verify_scriptparameters(@@lot, li), "failed to verify scripts"

    # trigger pre-2 by opestart
    reset_scriptparameters(@@lot)
    cj, cjinfo, eqpiraw, pstatus, mode = @@sv.claim_process_prepare(@@lot, eqp: @@eqp)
    assert cj = @@sv.eqp_opestart(@@eqp, li.carrier), "failed to start operation"
    assert verify_scriptparameters(@@lot, li), "failed to verify scripts"

    # trigger post by opecomplete, $CurrentOperation is the new (i.e. next) operation
    reset_scriptparameters(@@lot)
    sleep 10 # to separate TX
    assert_equal 0, @@sv.eqp_tempdata(@@eqp, pstatus.portGroup, cj), "failed to complete operation"
    txtime = Time.now
    assert_equal 0, @@sv.eqp_opecomp(@@eqp, cj), "failed to complete operation"
    assert_equal 0, @@sv.eqp_unload(@@eqp, pstatus.portID, li.carrier), "failed to unload"
    assert verify_scriptparameters(@@lot, li, false), "failed to verify scripts"

    # history
    assert verify_scriptparameter_history(@@lot, txtime, 4), "no event found in #{@@source} history table since #{txtime.strftime('%F_%T')}"
  end

  def test03_locate
    assert_equal 0, @@sv.lot_cleanup(@@lot, opNo: :first)
    reset_scriptparameters(@@lot)

    assert_equal 0, @@sv.lot_opelocate(@@lot, @@opnum)
    li = @@sv.lot_info(@@lot)
    assert verify_scriptparameters(@@lot, li), "failed to verify scripts"
  end

  def test04_gatepass
    assert_equal 0, @@sv.lot_cleanup(@@lot, opNo: :first)
    reset_scriptparameters(@@lot)

    assert_equal 0, @@sv.lot_gatepass(@@lot)
    assert verify_scriptparameters(@@lot, @@sv.lot_info(@@lot)), "failed to verify scripts"
  end

  def test05_script_skip_chain
    assert_equal 0, @@sv.lot_cleanup(@@lot, opNo: :first)
    reset_scriptparameters(@@lot)

    assert_equal 0, @@sv.lot_opelocate(@@lot, @@opnum_skip)
    assert verify_scriptparameters(@@lot, @@sv.lot_info(@@lot)), "failed to verify scripts"
  end

  def test06_return_from_erf
    assert_equal 0, @@sv.lot_cleanup(@@lot, opNo: :first)
    assert_equal 0, @@sv.lot_experiment(@@lot, 2, @@branch_route1, @@psm_split_op1, @@psm_return_op1, merge: @@psm_merge_op1)
    assert_equal 0, @@sv.lot_experiment(@@lot, 1, @@branch_route2, @@psm_split_op2, @@psm_return_op2, split_route: @@branch_route1, original_opNo: @@psm_split_op1)
    reset_scriptparameters(@@lot)

    assert_equal 0, @@sv.lot_opelocate(@@lot, @@psm_split_op1), "failed to locate to branch op"
    sublot1 = @@sv.lot_family(@@lot)[-1]
    assert sublot1, "failed to execute experiment"

    assert verify_scriptparameters(@@lot, @@sv.lot_info(@@lot)), "failed to verify scripts for #{@@lot}"
    #This is a known bug in R10, no script execution for child lot on branch
    assert verify_scriptparameters(sublot1, @@sv.lot_info(sublot1)), "failed to verify scripts for #{sublot1}"
    assert_equal 0, @@sv.lot_opelocate(sublot1, @@psm_split_op2), "failed to locate to branch-to-branch op"
    sublot2 = @@sv.lot_family(@@lot)[-1]
    refute_equal sublot1, sublot2, "failed to execute experiment"

    assert verify_scriptparameters(sublot1, @@sv.lot_info(sublot1)), "failed to verify scripts for #{sublot1}"
    if @@sv.release > 13
      #This is a known bug in R10, no script execution for child lot on branch (MSR431208)
      assert verify_scriptparameters(sublot2, @@sv.lot_info(sublot2)), "failed to verify scripts for #{sublot2}"
    end

    assert_equal 0, @@sv.lot_gatepass(sublot2), "failed to gatepass to return op"
    #assert verify_scriptparameters(sublot2, @@sv.lot_info(sublot2)), "failed to verify scripts for #{sublot2}"

    assert_equal 0, @@sv.lot_opelocate(sublot1, @@psm_return_op2), "failed to locate to merge ope"
    reset_scriptparameters(sublot1)
    assert_equal 0, @@sv.lot_merge(sublot1, sublot2), "failed to merge the lot"
    assert verify_scriptparameters(sublot1, @@sv.lot_info(sublot1)), "failed to verify scripts for #{sublot1}"

    assert_equal 0, @@sv.lot_opelocate(@@lot, @@psm_merge_op1), "faild to locate to merge operation"
    assert_equal 0, @@sv.lot_opelocate(sublot1, @@psm_merge_op1), "faild to locate to merge operation"
    assert_equal 0, @@sv.lot_merge(@@lot, sublot1), "failed to merge the lot"
  end

  def test07_return_from_branch
    assert_equal 0, @@sv.lot_cleanup(@@lot, opNo: :first)
    wafers = @@sv.lot_info(@@lot).nwafers
    assert_equal 0, @@sv.lot_experiment(@@lot, wafers, @@branch_route1, @@psm_split_op1, @@psm_return_op1, merge: @@psm_merge_op1)
    assert_equal 0, @@sv.lot_experiment(@@lot, wafers, @@branch_route2, @@psm_split_op2, @@psm_return_op2, split_route: @@branch_route1, original_opNo: @@psm_split_op1)
    reset_scriptparameters(@@lot)
    assert_equal 0, @@sv.lot_opelocate(@@lot, @@psm_split_op1), "failed to locate to branch op"

    #script parameters should show subroute/PD values (MSR431208)
    verify_scriptparameters(@@lot, @@sv.lot_info(@@lot))

    reset_scriptparameters(@@lot)
    assert_equal 0, @@sv.lot_opelocate(@@lot, @@psm_split_op2), "failed to locate to branch-to-branch op"

    #script parameters should show subroute/PD values (MSR431208)
    verify_scriptparameters(@@lot, @@sv.lot_info(@@lot))

    reset_scriptparameters(@@lot)
    assert_equal 0, @@sv.lot_gatepass(@@lot), "failed to gatepass to return op"
    assert verify_scriptparameters(@@lot, @@sv.lot_info(@@lot)), "failed to verify script parameter"
  end

  def test08_return_from_rework_via_locate
    assert_equal 0, @@sv.lot_cleanup(@@lot, opNo: @@rework_op), "failed to locate lot"
    reset_scriptparameters(@@lot)

    assert_equal 0, @@sv.lot_rework(@@lot, @@rework_route, return_opNo: @@opnum), "failed to start rework"
    assert verify_scriptparameters(@@lot, @@sv.lot_info(@@lot)), "failed to verify scripts"

    reset_scriptparameters(@@lot)
    assert_equal 0, @@sv.lot_opelocate(@@lot, @@opnum), "faild to locate to return operation"
    assert verify_scriptparameters(@@lot, @@sv.lot_info(@@lot)), "failed to verify scripts"
  end

  def test09_return_from_rework_via_gatepass
    assert_equal 0, @@sv.lot_cleanup(@@lot, opNo: @@rework_op), "failed to locate lot"
    reset_scriptparameters(@@lot)

    assert_equal 0, @@sv.lot_rework(@@lot, @@rework_route, return_opNo: @@opnum), "failed to start rework"
    assert verify_scriptparameters(@@lot, @@sv.lot_info(@@lot)), "failed to verify scripts"

    reset_scriptparameters(@@lot)
    assert_equal 0, @@sv.lot_gatepass(@@lot), "faild to locate to return operation"
    assert verify_scriptparameters(@@lot, @@sv.lot_info(@@lot)), "failed to verify scripts"
  end

  def test10_return_from_partial_rework
    assert_equal 0, @@sv.lot_cleanup(@@lot, opNo: @@rework_op), "failed to locate lot"
    reset_scriptparameters(@@lot)

    assert sublot = @@sv.lot_partial_rework(@@lot, 1, @@rework_route, return_opNo: @@opnum), "failed to do partial rework"
    assert verify_scriptparameters(sublot, @@sv.lot_info(sublot)), "failed to verify scripts"

    reset_scriptparameters(sublot)
    assert_equal 0, @@sv.lot_opelocate(sublot, @@opnum), "faild to locate to return operation"
    assert verify_scriptparameters(sublot, @@sv.lot_info(sublot)), "failed to verify scripts"

    @@sv.lot_opelocate(sublot, @@rework_op)
    assert_equal 0, @@sv.lot_merge(@@lot, sublot), "failed to merge the lot"
  end

  def test11_nonprobank
    assert_equal 0, @@sv.lot_cleanup(@@lot, opNo: @@opnum), "failed to locate lot"
    assert_equal 0, @@sv.lot_nonprobankin(@@lot, @@nonprobank)
    reset_scriptparameters(@@lot)

    assert_equal 0, @@sv.lot_nonprobankout(@@lot)
    assert verify_scriptparameters(@@lot, @@sv.lot_info(@@lot)), "failed to verify scripts for #{@@lot}"
  end

  def test12_force_complete
    assert_equal 0, @@sv.lot_cleanup(@@lot, opNo: @@opnum), "failed to locate lot"
    reset_scriptparameters(@@lot)
    li = @@sv.lot_info(@@lot)

    #trigger pre-1 by lotholdrelease
    assert_equal 0, @@sv.lot_hold(@@lot, @@reasoncode), "failed to set lot hold"
    assert_equal 0, @@sv.lot_hold_release(@@lot, nil), "failed to release lot"
    assert verify_scriptparameters(@@lot, li), "failed to verify scripts"

    #trigger pre-2 by opestart
    reset_scriptparameters(@@lot)
    cj, cjinfo, eqpiraw, pstatus, mode = @@sv.claim_process_prepare(@@lot, eqp: @@eqp)
    assert cj = @@sv.eqp_opestart(@@eqp, li.carrier), "failed to start operation"
    assert verify_scriptparameters(@@lot, li), "failed to verify scripts"

    #trigger post by opecomplete
    reset_scriptparameters(@@lot)
    assert_equal 0, @@sv.running_hold(@@lot, @@eqp, cj), "failed to set runnning hold"
    assert_equal 0, @@sv.eqp_opecomp(@@eqp, cj, force: true), "failed to force operation"
    assert_equal 0, @@sv.eqp_unload(@@eqp, pstatus.portID, li.carrier), "failed to unload"
    assert verify_scriptparameters(@@lot, li, false), "failed to verify scripts"
  end

  def test20_scriptparameter_history
    assert_equal 0, @@sv.lot_cleanup(@@lot, opNo: @@ooc_op), "failed to locate lot"
    assert_equal 0, @@sv.user_parameter_delete("Lot", @@lot, "OOC", check: true)
    assert_equal 0, @@sv.user_parameter_delete("Lot", @@lot, "FabDPML", check: true)
    assert_equal 0, @@sv.user_parameter_delete("Lot", @@lot, "PassCount", check: true)
    li = @@sv.lot_info(@@lot)
    sleep 10 # separate events
    txtime = Time.now
    #trigger pre-1 by lotholdrelease
    assert_equal 0, @@sv.lot_hold(@@lot, @@reasoncode), "failed to set lot hold"
    assert_equal 0, @@sv.lot_hold_release(@@lot, nil), "failed to release lot"
    #trigger pre-2 by opestart
    cj, cjinfo, eqpiraw, pstatus, mode = @@sv.claim_process_prepare(@@lot, eqp: @@eqp)
    assert cj = @@sv.eqp_opestart(@@eqp, li.carrier), "failed to start operation"
    #trigger post by opecomplete, $CurrentOperation is the new (i.e. next) operation
    assert_equal 0, @@sv.eqp_tempdata(@@eqp, pstatus.portGroup, cj), "failed to complete operation"
    assert_equal 0, @@sv.eqp_opecomp(@@eqp, cj), "failed to complete operation"
    assert_equal 0, @@sv.eqp_unload(@@eqp, pstatus.portID, li.carrier), "failed to unload"

    # verify history
    assert verify_scriptparameter_history(@@lot, txtime, 3*3), "no event found in #{@@source} history table since #{txtime.strftime('%F_%T')}"
  end

  # MES-1918: Data inconsistency in FRPFX_POLIST after lot is located then gatepassed in BRScript
  def test30_locate_skip
    # TODO: new lot?
    assert_equal 0, @@sv.lot_cleanup(@@lot, opNo: @@loc_skip_op), 'failed to locate lot'
    # lot_info fails with the bug in R15 (FP9)
    assert_equal @@loc_skip_op2, @@sv.lot_info(@@lot).opNo, 'lot at wrong operation'
    # check the history
    pre1_hist = nil
    assert wait_for {
      hist = @@sv.lot_operation_history(@@lot, opNo: @@loc_skip_op, raw: true)
      pre1_hist = hist.select {|e| e.userID == 'Pre1'} if hist
      pre1_hist && pre1_hist.count >= 2
    }, 'failed to get history'
    assert_equal 2, pre1_hist.count, 'wrong number of history entries'
    assert_equal ['LocateForward', 'GatePass'], pre1_hist.map {|h| h.operationCategory}, 'wrong history entries'
  end


  def reset_scriptparameters(lot)
    assert_equal 0, @@sv.user_parameter_delete('Lot', lot, 'UTContextKeys', check: true)
    assert_equal 0, @@sv.user_parameter_delete('Lot', lot, 'UTCurrentPD', check: true)
    assert_equal 0, @@sv.user_parameter_delete('Lot', lot, 'UTPassCount', check: true)
    assert_equal 0, @@sv.user_parameter_delete('Lot', lot, 'UTUData', check: true)
  end

  def _get_user_parameter(params, name)
    _context = params.select {|p| p.name == name}
    assert _context.count > 0, "script parameter #{name} not found"
    return _context[0].value
  end

  def verify_scriptparameters(lot, linfo, current=true)
    $log.info "verify_scriptparameters #{lot.inspect}, #{linfo.route.inspect}, #{linfo.opNo.inspect}, #{current}"
    _params = @@sv.user_parameter('Lot', lot)
    res = verify_equal("#{linfo.opNo};#{linfo.op};#{linfo.route}", _get_user_parameter(_params, "UTContextKeys"), "UTContextKeys")
    # parameters are set from a post-script;
    #  <tt>@obsolete:</tt> CS_CurrentPD_ID returns the operation AFTER the OpeComplete
    #  Core method $CurrentProcess returns operation BEFORE the OpeComplete
    res &= verify_equal(linfo.op, _get_user_parameter(_params, "UTCurrentPD"), "UTCurrentPD")
    entry = @@sv.lot_operation_list(lot, forward: false, history: false, searchcount: 1, current: current)
    pass_count = entry[0].pass
    res &= verify_equal(pass_count, _get_user_parameter(_params, "UTPassCount"), "UTPassCount")
    udata1 = @@sv.user_data(:pd, [@@pd, 'Operation'])['ProcessLayer']
    udata2 = (@@sv.user_data(:route_opNo, [linfo.opNo, linfo.route, 'Main']) || [])['RouteOpSafeOp'] || 'SQLERROR=100'
    # udata2 = (@@sv.user_data_route_operation(linfo.route, linfo.opNo) || [])["RouteOpSafeOp"] || 'SQLERROR=100'
    res &= verify_equal("#{udata1};#{udata2};SQLERROR=100;SQLERROR=100", _get_user_parameter(_params, "UTUData"), "UTUData")
    return res
  end

  def verify_scriptparameter_history(lot, txtime, count)
    $log.info "verify_scriptparameter_history #{lot.inspect}, #{txtime}, #{count}"
    return wait_for(timeout: @@timeout) {
      evs = @@historydb.user_parameter_history(id: lot, tstart: txtime - @@spread)
      evs && evs.size == count
    }
  end

end
