=begin 
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.
    
Author: Daniel Steger, 2015-10-13

History:
  2019-12-05 sfrieske, cleanup, renamed from Test093_LotMergeHold
  2020-02-12 sfrieske, removed use of _guess_op_route
=end

require 'SiViewTestCase'


# Register merge hold manually, MES2565
class MM_Lot_MGHL < SiViewTestCase
  @@reason = 'INT'


  def self.after_all_passed
    @@sv.delete_lot_family(@@lot)
  end

  
  def test00_setup
    $setup_ok = false
    #
    assert @@lot = @@svtest.new_lot, 'error creating test lot'
    #
    $setup_ok = true
  end
  
  def test10_split_merge_current
    assert @@sv.merge_lot_family(@@lot)
    assert_equal 0, @@sv.lot_cleanup(@@lot)
    assert child = @@sv.lot_split(@@lot, 5), 'split failed'
    mghl_current_op(@@lot, child)
  end

  def test11_split_merge_next
    assert @@sv.merge_lot_family(@@lot)
    assert_equal 0, @@sv.lot_cleanup(@@lot)
    assert child = @@sv.lot_split(@@lot, 5), 'split failed'
    mghl_nexp_op(@@lot, child)
  end
  
  def test12_split_merge_grandchild
    assert @@sv.merge_lot_family(@@lot)
    assert_equal 0, @@sv.lot_cleanup(@@lot)
    assert child = @@sv.lot_split(@@lot, 5), 'split failed'
    assert child2 = @@sv.lot_split(child, 4), 'child split failed'
    assert_equal 0, @@sv.lot_merge(@@lot, child), 'merge failed'
    mghl_current_op(@@lot, child2)
  end
  
  def test13_split_merge_next_grandchild
    assert @@sv.merge_lot_family(@@lot)
    assert_equal 0, @@sv.lot_cleanup(@@lot)
    assert child = @@sv.lot_split(@@lot, 5), 'split failed'
    assert child2 = @@sv.lot_split(child, 4), 'child split failed'
    assert_equal 0, @@sv.lot_merge(@@lot, child), 'merge failed'
    mghl_nexp_op(@@lot, child2)
  end


  # aux methods
  
  def mghl_current_op(lot, child)
    assert_equal 'Waiting', @@sv.lot_info(child).status
    li = @@sv.lot_info(lot)
    assert_equal 'Waiting', li.status
    assert_equal 0, @@sv.lot_hold(lot, 'MGHL', related_lot: child, holdtype: 'MergeHold'), 'failed to set MGHL hold'
    assert_equal 0, @@sv.lot_futurehold(lot, li.opNo, 'MGHL', related_lot: child, holdtype: 'MergeHold'), 'failed to set MGHL FutureHold'
    assert_equal 0, @@sv.lot_hold(child, 'MGHL', related_lot: lot, holdtype: 'MergeHold'), 'failed to set MGHL hold'
    assert_equal 0, @@sv.lot_futurehold(child, li.opNo, 'MGHL', related_lot: lot, holdtype: 'MergeHold'), 'failed to set MGHL FutureHold'
    # verify MGHL holds and future holds
    [lot, child].each {|l|
      hh = @@sv.lot_hold_list(l, reason: 'MGHL')
      assert_equal 1, hh.count, 'wrong LotHold'
      assert_equal 'MergeHold', hh.first.type, 'invalid hold type'
      fhs = @@sv.lot_futurehold_list(l, reason: 'MGHL', opno: li.opNo)
      assert_equal 1, fhs.count, 'wrong FutureHold'
      assert_equal 'MergeHold', fhs.first.type, 'invalid hold type'
    }
    # verify child can be merged
    assert_equal 0, @@sv.lot_merge(lot, child), 'merge failed'
    assert_equal 'Waiting', @@sv.lot_info(lot).status    
  end

  def mghl_nexp_op(lot, child)
    # assert res = @@sv._guess_op_route(lot, 1).first, "next operation not found for #{lot}"
    # opNo_next = res.opNo
    assert opNo_next = @@sv.lot_operation_list(lot, raw: true)[1].operationNumber, "next operation not found for #{lot}"
    assert_equal 'Waiting', @@sv.lot_info(child).status
    assert_equal 'Waiting',  @@sv.lot_info(lot).status
    assert_equal 0, @@sv.lot_futurehold(lot, opNo_next, 'MGHL', related_lot: child, holdtype: 'MergeHold'), 'failed to set MGHL FutureHold'
    assert_equal 0, @@sv.lot_futurehold(child, opNo_next, 'MGHL', related_lot: lot, holdtype: 'MergeHold'), 'failed to set MGHL FutureHold'
    # verify MGHL future holds
    [lot, child].each {|l|
      fhs = @@sv.lot_futurehold_list(l, reason: 'MGHL', opno: opNo_next)
      assert_equal 1, fhs.count, 'wrong FutureHold'
      assert_equal 'MergeHold', fhs.first.type, 'invalid hold type'
    }
    # locate lots to merge op and verify child can be merged
    [lot, child].each {|lot| assert_equal 0, @@sv.lot_opelocate(lot, opNo_next)}
    if @@sv.f7 
      # check for automerge
      assert_equal 'EMPTIED', @@sv.lot_info(child).status, 'automerge failed'
    else
      assert_equal 0, @@sv.lot_merge(lot, child), 'merge failed'
    end
    assert_equal 'Waiting', @@sv.lot_info(lot).status
  end

end
