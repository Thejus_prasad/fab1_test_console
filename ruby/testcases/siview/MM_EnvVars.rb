=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: sfrieske, 2017-04-25

History:
  2021-08-27 sfrieske, moved verify_envvars here
=end

require 'pp'
require 'yaml'
# require 'util/verifyhash' # TODO: remove
require 'RubyTestCase'


class MM_EnvVars < RubyTestCase
  @@exclude = ['BRS_SERVER_NAME', 'BRS_HOST_NAME', 'EVENT_IPCKEY_MM',
    'CS_MM_CATALOG_FILE', 'CS_SP_DISPATCH_CATEGORY_ID', 'MM_CATALOG_FILE', 'SP_E10PRIORITY_FILE', 'SP_ZONE_TYPE_INI']


  # def test10
  #   verify_envvars.each_pair {|k, diff|
  #     assert_empty diff, 'unexpected MM env vars'
  #   }
  # end

  def test11_svc
    ref = YAML.load_file(File.join('./testparameters', $env, 'siview_envvars.yaml'))[0]
    vars = @@sv.environment_variable[0]
    assert_equal vars.size, ref.size, 'additional or missing SvcEnvVariables'
    diff = {}
    ref.each_pair {|k, v|
      actual = vars[k]
      diff[k] = [v, actual] if actual != v
    }
    assert_empty diff, 'unexpected MM SvcEnvVariables'
  end

  def test12_ppt
    ref = YAML.load_file(File.join('./testparameters', $env, 'siview_envvars.yaml'))[1]
    vars = @@sv.environment_variable[1]
    # assert_equal vars.size, ref.size, 'additional or missing PptEnvVariables'
    diff = {}
    ref.each_pair {|k, v|
      next if @@exclude.include?(k)
      actual = vars[k]
      diff[k] = [v, actual] if actual != v
    }
    vars.each_pair {|k, v|
      next if @@exclude.include?(k)
      actual = ref[k]
      diff[k] = [actual, v] if actual != v
    }
    $log.info "\n#{diff.pretty_inspect}" unless diff.empty?
    assert_empty diff, 'unexpected MM PptEnvVariables'
  end

  # # compare current MM server env vars with a reference file, return {SvcEnvVariable: diff, PptEnvVariable: diff}
  # def verify_envvars(params={})
  #   $log.info 'verify_envvars'
  #   exclude = params[:exclude] || ['BRS_SERVER_NAME', 'BRS_HOST_NAME', 'EVENT_IPCKEY_MM',
  #     'CS_MM_CATALOG_FILE', 'MM_CATALOG_FILE', 'SP_E10PRIORITY_FILE', 'SP_ZONE_TYPE_INI']
  #   ref = YAML.load_file(File.join('./testparameters', $env, 'siview_envvars.yaml'))
  #   envvars = @@sv.environment_variable
  #   ret = {}
  #   $log.info ' SvcEnvVariable'
  #   ret['SvcEnvVariable'] = verify_hash(ref[0], envvars[0], nil, return_diff: true)
  #   $log.info ' PptEnvVariable'
  #   ret['PptEnvVariable'] = verify_hash(ref[1], envvars[1], nil, exclude: exclude, return_diff: true)
  #   return ret
  # end

end
