=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: Paul Peschel, 2013-01-18 migrate from Test045_LoadPurposeTypesInternalBuffer.java

History
  2015-06-24 ssteidte, merged with junit_testing repository and verified with R15
  2019-12-09 sfrieske, minor cleanup, renamed from Test045_LoadPurposeTypesInternalBuffer
  2020-09-07 sfrieske, minor cleanup
  2021-08-27 sfrieske, split verify_equal off of misc.rb
=end

require 'util/verifyequal'
require 'SiViewTestCase'


# Test carrier loading of internal buffer tool for load purposes
class MM_Eqp_LoadPurposesIB < SiViewTestCase
  @@eqp = 'UTI002'
  @@opNo = '1000.200'
  @@opNo_eqpmonitor = '100.400'

  CassetteLoadPurposes = ['Process Lot', 'Process Monitor Lot', 'Empty Cassette', 'Filler Dummy Lot', 'Side Dummy Lot', 'Waiting Monitor Lot', 'Other']

  RC_OK = 0
  RC_CAST_NOT_EMPTY = 310
  RC_INVALID_PROCMONITOR_COUNT = 2913
  RC_LOT_BANK_DIFFERENT = 1237
  RC_LOT_NOT_IN_BANK = 1214
  RC_NEED_TO_SPECIFY_ALL_LOT_IN_CAST = 2819
  RC_NOT_CANDIDATE_LOT_FOR_OPE_START = 2800
  RC_NOT_FOUND_MATERIAL_LOCATION = 2854
  RC_INVALID_LOT_INVENTORYSTAT = 932
  RC_INVALID_PURPOSE_TYPE = 954
  RC_INVALID_PORT_LOADPURPOSE = 2966

  # /* Process Lot */   /* Process Monitor */   /* Empty Cass */    /* Filler Dummy */    /* Side Dummy */    /* Waiting Monitor */   /* Other */    Internal Buf
  RC_PROCESS = [RC_OK, RC_INVALID_PROCMONITOR_COUNT, RC_CAST_NOT_EMPTY, RC_LOT_NOT_IN_BANK, RC_LOT_NOT_IN_BANK, RC_LOT_NOT_IN_BANK, RC_NOT_FOUND_MATERIAL_LOCATION]
  RC_PROCESS_NPW = [RC_INVALID_PURPOSE_TYPE, RC_INVALID_PURPOSE_TYPE, RC_INVALID_PURPOSE_TYPE, RC_INVALID_LOT_INVENTORYSTAT, RC_INVALID_LOT_INVENTORYSTAT, RC_INVALID_LOT_INVENTORYSTAT, RC_NOT_FOUND_MATERIAL_LOCATION]

  RC_EMPTY = [RC_NOT_CANDIDATE_LOT_FOR_OPE_START, RC_NOT_CANDIDATE_LOT_FOR_OPE_START, RC_OK, RC_INVALID_PORT_LOADPURPOSE, RC_INVALID_PORT_LOADPURPOSE, RC_INVALID_PORT_LOADPURPOSE, RC_INVALID_PORT_LOADPURPOSE]
  RC_EMPTY_NPW = [RC_INVALID_PURPOSE_TYPE, RC_INVALID_PURPOSE_TYPE, RC_INVALID_PURPOSE_TYPE, RC_INVALID_PORT_LOADPURPOSE, RC_INVALID_PORT_LOADPURPOSE, RC_INVALID_PORT_LOADPURPOSE, RC_INVALID_PORT_LOADPURPOSE]

  RC_DUMMY = [RC_NEED_TO_SPECIFY_ALL_LOT_IN_CAST, RC_NEED_TO_SPECIFY_ALL_LOT_IN_CAST, RC_CAST_NOT_EMPTY, RC_OK, RC_OK, RC_LOT_BANK_DIFFERENT, RC_INVALID_PORT_LOADPURPOSE]
  RC_DUMMY_NPW = [RC_INVALID_PURPOSE_TYPE, RC_INVALID_PURPOSE_TYPE, RC_INVALID_PURPOSE_TYPE, RC_OK, RC_OK, RC_LOT_BANK_DIFFERENT, RC_INVALID_PORT_LOADPURPOSE]

  RC_MONITOR = RC_PROCESS
  RC_MONITOR_NPW = RC_PROCESS_NPW

  RC_WAITINGMONITOR = [RC_NEED_TO_SPECIFY_ALL_LOT_IN_CAST, RC_NEED_TO_SPECIFY_ALL_LOT_IN_CAST, RC_CAST_NOT_EMPTY, RC_LOT_BANK_DIFFERENT, RC_LOT_BANK_DIFFERENT, RC_OK, RC_INVALID_PORT_LOADPURPOSE]
  RC_WAITINGMONITOR_NPW = [RC_INVALID_PURPOSE_TYPE, RC_INVALID_PURPOSE_TYPE, RC_INVALID_PURPOSE_TYPE, RC_LOT_BANK_DIFFERENT, RC_LOT_BANK_DIFFERENT, RC_OK, RC_INVALID_PORT_LOADPURPOSE]

  @@testlots = []


  def self.after_all_passed
    @@testlots.each {|lot| @@sv.delete_lot_family(lot)}
  end

  def teardown
    @@sv.eqp_cleanup(@@eqp)
  end


  def test00_setup
    $setup_ok = false
    #
    if @@sv.f7
      RC_PROCESS[-1] = RC_OK
      RC_PROCESS_NPW[-1] = RC_OK
      RC_EMPTY[-1] = RC_OK
      RC_EMPTY_NPW[-1] = RC_OK
      RC_DUMMY[-1] = RC_OK
      RC_DUMMY_NPW[-1] = RC_OK
      RC_WAITINGMONITOR[-1] = RC_OK
      RC_WAITINGMONITOR_NPW[-1] = RC_OK
    end
    # ensure eqp has all load types configured and clean it up
    eqpi = @@sv.eqp_info(@@eqp)
    CassetteLoadPurposes.each {|purpose|
      next if purpose == 'Other'
      assert eqpi.ib.buffers.keys.find {|b| b == purpose}, "eqp #{@@eqp} has no buffer of category #{purpose.inspect}"
    }
    assert @@svtest.cleanup_eqp(@@eqp, mode: 'Off-Line-1')
    @@port = eqpi.ports.first.port
    #
    # get a process lot
    assert lot = @@svtest.new_lot, 'error creating test lot'
    @@testlots << lot
    assert_equal 0, @@sv.lot_cleanup(lot, opNo: @@opNo)
    @@processLotCarrier = @@sv.lot_info(lot).carrier
    $log.info "process lot: #{lot}, carrier: #{@@processLotCarrier}"
    #
    # get monitor lots
    assert lot = @@svtest.new_controllot('Dummy', bankin: true)
    @@testlots << lot
    @@dummyLotCarrier = @@sv.lot_info(lot).carrier
    assert lot = @@svtest.new_controllot('Process Monitor', bankin: true)
    @@testlots << lot
    @@waitingMonitorLotCarrier = @@sv.lot_info(lot).carrier
    assert lot = @@svtest.new_controllot('Equipment Monitor')
    @@testlots << lot
    assert_equal 0, @@sv.lot_cleanup(lot, opNo: @@opNo_eqpmonitor)
    @@monitorLotCarrier = @@sv.lot_info(lot).carrier
    #
    # find empty carrier
    assert @@emptyCarrier = @@svtest.get_empty_carrier, "no empty carrier found"
    #
    $setup_ok = true
  end

  def test11_ProcessLot
    assert runtest_load(@@processLotCarrier, RC_PROCESS)
  end

  def test12_ProcessLot_NPW
    assert runtest_npw(@@processLotCarrier, RC_PROCESS_NPW, RC_PROCESS)
  end

  def test21_EmptyCarrier
    assert runtest_load(@@emptyCarrier, RC_EMPTY)
  end

  def test22_EmptyCarrier_NPW
    assert runtest_npw(@@emptyCarrier, RC_EMPTY_NPW, RC_EMPTY)
  end

  def test31_DummyLotCarrier
    assert runtest_load(@@dummyLotCarrier, RC_DUMMY)
  end

  def test32_DummyLotCarrier_NPW
    assert runtest_npw(@@dummyLotCarrier, RC_DUMMY_NPW, RC_DUMMY)
  end

  def test41_EquipmentMonitorLotCarrier
    assert runtest_load(@@monitorLotCarrier, RC_MONITOR)
  end

  def test42_EquipmentMonitorLotCarrier_NPW
    assert runtest_npw(@@monitorLotCarrier, RC_MONITOR_NPW, RC_MONITOR)
  end

  def test51_WaitingMonitorLotCarrier
    assert runtest_load(@@waitingMonitorLotCarrier, RC_WAITINGMONITOR)
  end

  def test52_WaitingMonitorLotCarrier_NPW
    assert runtest_npw(@@waitingMonitorLotCarrier, RC_WAITINGMONITOR_NPW, RC_WAITINGMONITOR)
  end


  # aux methods

  def runtest_load(carrier, rc_list_load)
    ok = true
    assert_equal 0, @@sv.eqp_cleanup(@@eqp)
    assert @@svtest.cleanup_carrier(carrier)
    assert_equal(0, @@sv.carrier_xfer_status_change(carrier, 'EO', @@eqp))
    #
    CassetteLoadPurposes.each_with_index {|purpose, i|
      $log.info "-- carrier load purpose #{purpose.inspect}"
      # load carrier and verify return
      rc_load = @@sv.eqp_load(@@eqp, @@port, carrier, ib: true, purpose: purpose)
      ok &= verify_equal(rc_list_load[i], rc_load, "wrong rc for loading with purpose #{purpose} [#{@@sv.msg_text}]")
      # clean up
      @@sv.eqp_unload(@@eqp, @@port, carrier, ib: true) if rc_load == 0
    }
    return ok
  end

  def runtest_npw(carrier, rc_list_npw, rc_list_load)
    ok = true
    assert_equal 0, @@sv.eqp_cleanup(@@eqp)
    assert @@svtest.cleanup_carrier(carrier)
    assert_equal 0, @@sv.carrier_xfer_status_change(carrier, 'EO', @@eqp)
    #
    CassetteLoadPurposes.each_with_index {|purpose, i|
      $log.info "-- carrier load purpose #{purpose.inspect}"
      # reserve carrier and verify return code
      rc_npw = @@sv.npw_reserve(@@eqp, @@port, carrier, purpose: purpose)
      ok &= verify_equal(rc_list_npw[i], rc_npw, "wrong rc for NPW with purpose #{purpose} [#{@@sv.msg_text}]")
      next unless rc_npw == 0
      # load carrier and verify return code
      rc_load = @@sv.eqp_load(@@eqp, @@port, carrier, ib: true, purpose: purpose)
      ok &= verify_equal(rc_list_load[i], rc_load, "wrong rc for loading with purpose #{purpose} [#{@@sv.msg_text}]")
      # clean up
      if rc_load == 0
        @@sv.eqp_unload(@@eqp, @@port, carrier, ib: true)
      else
        @@sv.npw_reserve_cancel(@@eqp, carrier)
      end
    }
    return ok
  end

end
