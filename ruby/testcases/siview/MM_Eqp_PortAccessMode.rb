=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: Steffen Steidten, 2012-02-22

History:
  2015-10-16 dsteger,  additional tests for Auto-3 to Off-Line-1/2 (MSRs 264697, 139183)
  2015-12-11 dsteger,  added test for MSR 653928
  2018-03-20 sfrieske, better eqp cleanup
  2019-05-13 sfrieske, minor code cleanup
  2019-12-09 sfrieske, minor cleanup, renamed from Test510_PortAccessMode
  2020-08-24 sfrieske, minor cleanup
  2020-10-23 sschoen3, Test18 changed, because new SiView behaviour in C7.17
=end

require 'SiViewTestCase'


# Test Port Access Mode Change, MSR 482828
class MM_Eqp_PortAccessMode < SiViewTestCase
  @@eqp = 'UTF001'    # must have at least 2 port groups
  @@eqp_ib = 'UTI001'
  @@ope_fb = '1000.600'
  @@ope_ib = '1000.300'
  @@dummy_mode_change = '0'    # mode change supported if dummy carrier loaded
  @@port_check = '0'

  @@testlots = []


  def self.after_all_passed
    @@testlots.each {|lot| @@sv.delete_lot_family(lot)}
    [@@eqp, @@eqp_ib].each {|eqp| @@sv.eqp_cleanup(eqp)}
  end


  def test00_setup
    $setup_ok = false
    #
    assert_equal @@port_check, @@sv.environment_variable[1]['SP_LOT_CASSETTE_ON_PORT_CHECK_FLAG'], 'wrong setting for port check'
    assert_equal @@dummy_mode_change, @@sv.environment_variable[1]['SP_EQP_MODE_CHANGE_OFFLINE_TO_ONLINE_POSSIBLE'], 'npw check should be enforced'
    assert @@svtest.cleanup_eqp(@@eqp)
    assert @@svtest.cleanup_eqp(@@eqp_ib)
    # refute_empty @@monitor_bank, "no monitor bank defined for #{@@eqp_ib}"
    refute_empty @@monitor_bank = @@sv.eqp_info_raw(@@eqp_ib, ib: true).equipmentBRInfoForInternalBuffer.controlBanks.find {|e| 
      e.controlLotType == 'Waiting Monitor Lot'
    }.controlBankID.identifier, "no monitor bank defined for #{@@eqp_ib}"
    assert @@lot = @@svtest.new_lot, 'error creating test lot'
    @@testlots << @@lot
    #
    $setup_ok = true
  end

  def test01_fb_with_foup_to_auto_mode
    do_switch_access_mode_test(@@eqp, @@ope_fb, 'Auto-1', 'Auto-2')
  end

  def test02_fb_with_foup_to_manual_mode
    do_switch_access_mode_test(@@eqp, @@ope_fb, 'Auto-3', 'Auto-1')
  end

  def test03_fb_offline_online
    rc = @@port_check == '0' ? 0 : 1242
    do_switch_online_test(@@eqp, @@ope_fb, 'Off-Line-1', 'Auto-3', rc)
    do_switch_online_test(@@eqp, @@ope_fb, 'Off-Line-2', 'Auto-3', rc)
  end

  def test04_fb_online_offline
    do_switch_online_test(@@eqp, @@ope_fb, 'Auto-3', 'Off-Line-1', 0)
    do_switch_online_test(@@eqp, @@ope_fb, 'Auto-3', 'Off-Line-2', 0)
  end

  # MSR 264697
  def test05_fb_with_unloaddisp_no_foup_to_auto3
    assert_equal 0, @@sv.eqp_cleanup(@@eqp, mode: 'Off-Line-2')
    assert_equal 0, @@sv.lot_cleanup(@@lot, opNo: @@ope_fb)
    carrier = @@sv.lot_info(@@lot).carrier
    assert @@svtest.cleanup_carrier(carrier)
    ports = @@sv.eqp_info(@@eqp).ports
    port = ports[0].port # per default take first one
    assert @@sv.carrier_xfer(carrier, @@eqp, port, @@svtest.stocker, ''), 'carrier stockin error'
    check_reservation(carrier, true, 'Dispatched', @@eqp, port)
    # to Auto-3
    assert_equal 0, @@sv.eqp_mode_change(@@eqp, 'Auto-3'), 'failed to change from Off-Line-2 to Auto-3'
    check_reservation(carrier, false, @@port_check == '1' ? 'Dispatched' : 'Required', @@eqp, port) #port_check change from 0 to 1 - Env Variable was changed in SiView 7.19
  end

  # MSR 309991 (MES-2295)
  def test06_fb_with_unloaddisp_foup_to_auto3
    skip "MSR309991 not fixed yet"
    assert_equal 0, @@sv.eqp_cleanup(@@eqp, mode: 'Auto-2')
    assert_equal 0, @@sv.lot_cleanup(@@lot, opNo: @@ope_fb)
    carrier = @@sv.lot_info(@@lot).carrier
    assert @@svtest.cleanup_carrier(carrier)
    #
    ports = @@sv.eqp_info(@@eqp).ports
    port = ports[0].port # per default take first one
    assert cj = @@sv.claim_process_lot(@@lot, eqp: @@eqp, port: port, mode: 'Auto-2', nounload: true), 'error processing lot'
    assert_equal 0, @@sv.eqp_port_status_change(@@eqp, port, 'UnloadReq'), "failed to request unload"
    assert_equal 0, @@sv.carrier_xfer(carrier, @@eqp, port, @@svtest.stocker, ''), 'carrier stockin error'
    check_reservation(carrier, true, 'Dispatched', @@eqp, port)
    assert_equal 0, @@sv.carrier_xferjob_delete(carrier)
    # to Auto-1
    assert_equal 0, @@sv.eqp_mode_change(@@eqp, 'Auto-1')
    # to Auto-3
    assert_equal 0, @@sv.eqp_mode_change(@@eqp, 'Auto-3'), 'failed to change to Auto-3'
    check_reservation(carrier, false, @@port_check == '0' ? 'Dispatched' : 'Required', @@eqp, port)
  end

  def test10_ib_with_slr_auto_mode
    # rc_exp = mode_tmpl == 'Off-Line' ? 1242 : 0
    do_switch_online_test(@@eqp_ib, @@ope_ib, 'Auto-1', 'Auto-3', 0)
  end

  def test11_ib_with_slr_manual_mode
    # rc_exp = mode_tmpl == 'Off-Line' ? 1242 : 0
    do_switch_online_test(@@eqp_ib, @@ope_ib, 'Auto-3', 'Auto-1', 0)
  end

  def test12_ib_with_slr_offline_online
    rc = @@port_check == '0' ? 0 : 1242
    do_switch_online_test(@@eqp_ib, @@ope_ib, 'Off-Line-1', 'Auto-3', rc)
    do_switch_online_test(@@eqp_ib, @@ope_ib, 'Off-Line-2', 'Auto-3', rc)
  end

  def test13_ib_with_slr_online_offline
    do_switch_online_test(@@eqp_ib, @@ope_ib, 'Auto-3', 'Off-Line-1', 0)
    do_switch_online_test(@@eqp_ib, @@ope_ib, 'Auto-3', 'Off-Line-2', 0)
  end

  # MSR 169143
  def test14_offline_auto3_with_dummy
    assert_equal 0, @@sv.eqp_cleanup(@@eqp_ib, mode: 'Off-Line-1')
    assert lot = @@svtest.new_controllot('Process Monitor', bankin: true)
    @@testlots << lot
    carrier = @@sv.lot_info(lot).carrier
    ports = @@sv.eqp_info(@@eqp_ib).ports
    port = ports.first.port # per default take first one
    assert_equal 0, @@sv.carrier_xfer_status_change(carrier, 'EO', @@eqp_ib)
    assert_equal 0, @@sv.eqp_load(@@eqp_ib, port, carrier, purpose: 'Waiting Monitor Lot'), 'error loading carrier'
    # to Auto-3
    rc_exp = (@@dummy_mode_change == '1' || @@port_check == '0') ? 0 : 1242
    assert_equal rc_exp, @@sv.eqp_mode_change(@@eqp_ib, 'Auto-3'), 'wrong rc when switching from Off-Line-1 to Auto-3'
  end

  # MSR 264697
  def test15_ib_with_unloaddisp_to_auto3
    assert_equal 0, @@sv.eqp_cleanup(@@eqp_ib, mode: 'Off-Line-2')
    assert_equal 0, @@sv.lot_cleanup(@@lot, opNo: @@ope_ib)
    carrier = @@sv.lot_info(@@lot).carrier
    assert @@svtest.cleanup_carrier(carrier)
    ports = @@sv.eqp_info(@@eqp_ib).ports
    port = ports.first.port # per default take first one
    assert @@sv.carrier_xfer(carrier, @@eqp_ib, port, @@svtest.stocker, ''), "failed to create carrier move"
    check_reservation(carrier, true, 'Dispatched', @@eqp_ib, port)
    # to Auto-3
    assert_equal 0, @@sv.eqp_mode_change(@@eqp_ib, 'Auto-3'), "failed to change from Off-Line-2 to Auto-3"
    check_reservation(carrier, false, 'Required', @@eqp_ib, port)
  end

  def test16_ib_with_foup_to_auto_mode
    do_switch_access_mode_test(@@eqp_ib, @@ope_ib, 'Auto-1', 'Auto-2')
  end

  def test17_ib_with_foup_to_manual_mode
    do_switch_access_mode_test(@@eqp_ib, @@ope_ib, 'Auto-3', 'Auto-1')
  end

  def test18_no_mixedmode_batch_reservation
    assert_equal 0, @@sv.eqp_cleanup(@@eqp_ib, mode: 'Auto-1')
    assert lots = @@svtest.new_lots(2), 'error creating test lots'
    @@testlots += lots
    lots.each {|lot| assert_equal 0, @@sv.lot_cleanup(lot, opNo: @@ope_ib)}
    #not needed anymore - changed C7.17
	#$log.info "using lots #{lots}"
    assert ports = @@sv.eqp_info(@@eqp_ib).ports, 'failed to get ports'
    port = ports.last.port
    assert_equal 0, @@sv.eqp_mode_change(@@eqp_ib, 'Auto-3', port: port), 'error switching individual port'
    portids = ports.collect {|p| p.port}
    pgids = ports.collect {|p| p.pg}
	#changed row 183-186 in C7.17#
	#assert_nil @@sv.slr(@@eqp_ib, lot: lots, port: portids, pg: pgids, cycle_port: true), 'reservation should be rejected'
	#assert_nil @@sv.slr(@@eqp_ib, lot: lots, port: portids, pg: pgids, cycle_port: true), 'reservation should be rejected'
    #assert_equal 911, @@sv.rc, 'wrong rc'
    #assert cj = @@sv.claim_process_lot(lots, eqp: @@eqp_ib, port: port, mode: 'Auto-3'), 'error processing on single port'
  end


  def do_switch_access_mode_test(eqp, opNo, mode1, mode2)
    assert_equal 0, @@sv.eqp_cleanup(eqp, mode: mode1)
    assert_equal 0, @@sv.lot_cleanup(@@lot, opNo: opNo)
    carrier = @@sv.lot_info(@@lot).carrier
    assert @@svtest.cleanup_carrier(carrier)
    ports = @@sv.eqp_info(eqp).ports
    port = ports.first.port # per default take first one
    assert @@sv.claim_process_prepare(@@lot, eqp: eqp, port: port, mode: mode1), 'reservation failed'
    # switch other ports to Auto Access
    ports[1..-1].each {|port|
      _p = ports.select {|p| p.pg == port.pg}.map {|p| p.port}
      assert_equal 0, @@sv.eqp_mode_change(eqp, mode2, port: _p), "#{mode1} -> #{mode2}"
      assert_equal 0, @@sv.eqp_mode_change(eqp, mode1, port: _p), "#{mode1} -> #{mode2}"
    }
    # P1 to Auto Access fails
    # if mode_tmpl == 'Off-Line'
    #   assert_equal 1242, @@sv.eqp_mode_change(eqp, mode2, port: port), "#{mode1} -> #{mode2}"
    # else
    assert_equal 0, @@sv.eqp_mode_change(eqp, mode2, port: port), "#{mode1} -> #{mode2}"
    assert_equal 0, @@sv.eqp_mode_change(eqp, mode1, port: port)
    assert_equal 0, @@sv.eqp_mode_change(eqp, 'Auto-3', port: port), "#{mode1} -> #{mode2}" ##if mode_tmpl == 'Auto'
    # end
  end

  def do_switch_online_test(eqp, opNo, mode1, mode2, rc_exp)
    assert_equal 0, @@sv.lot_cleanup(@@lot, opNo: opNo)
    carrier = @@sv.lot_info(@@lot).carrier
    assert @@svtest.cleanup_carrier(carrier)
    assert_equal 0, @@sv.eqp_cleanup(eqp, mode: mode1)
    # port = @@sv.eqp_info(eqp).ports.first.port
    assert @@sv.claim_process_prepare(@@lot, eqp: eqp, mode: mode1)
    assert_equal rc_exp, @@sv.eqp_mode_change(eqp, mode2), "#{eqp}: #{mode1} -> #{mode2}"
  end

  def check_reservation(carrier, reserved, state, eqp, port)
    $log.info "check_reservation #{carrier} #{port}"
    port = @@sv.eqp_info(eqp).ports.find {|p| p.port == port}
    assert_equal carrier, port.disp_unload_carrier, 'wrong disp_unload_carrier' if reserved
    assert_equal state, port.disp_state, 'wrong dispatch state'
  end

end
