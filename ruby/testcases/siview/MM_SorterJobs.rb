=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.


Author: Steffen Steidten, 2010-06-09

History:
  2012-07-31 ssteidte, added tests for jobs with wildcards
  2014-03-25 ssteidte, ensure AutoProc serves the sorter
  2014-11-12 ssteidte, removed dependency on AutoProc (test61)
  2015-08-25 ssteidte, cleanup
  2016-06-23 dsteger,  added MSR 755262 test (??) , added MSR 815910 (test70, 71)
  2018-10-09 sfrieske, minor cleanup, renamed from Test_SorterJobs to MM_SorterJobs
  2019-01-29 sfrieske, added test73 for MES-3965
  2019-12-05 sfrieske, merged with Test091_LotSplit_WithoutHoldRelease (MSR 742801)
  2020-08-27 sfrieske, minor cleanup
  2021-01-15 sfrieske, moved SiView::MM#new_srclot_for_product to SiView::Test#new_srclot
  2021-01-20 sschoen3, renamed Test73 to xxTest73, because not relevant anymore
  2021-04-21 sfrieske, moved @sorter and @pg form SiView::Test to SJC::Test
=end

require 'SiViewTestCase'


# Test R10 Sorter Jobs, including MSR 496889 (no sorter specified), 653154, 691923, 815910
class MM_SorterJobs < SiViewTestCase
  @@sorter = 'UTS001'
  @@testlots = []

  def self.after_all_passed
    @@sv.eqp_cleanup(@@sorter)
    @@testlots.each {|lot| @@sv.delete_lot_family(lot)}
  end

  def teardown
    # super
    @@svtest.eis.set_variable(@@sorter, 'sjnotificationwaittime', 2000)
    ##@@svtest.eis.set_variable(nil, 'defaultsjnotificationwaittime', 2000)   # for sandbox (??)
  end


  def test00_setup
    $setup_ok = false
    #
    @@svnox = SiView::MM.new($env, user: 'NOX-UNITTEST', password: :default)
    # set sorter port variables
    @@sorter = @@sorter
    eqpi = @@sv.eqp_info(@@sorter)
    @@port1 = eqpi.ports[0].port
    @@port2 = eqpi.ports[1].port
    @@pg1 = eqpi.ports[0].pg
    assert_equal @@pg1, eqpi.ports[1].pg, 'ports 1 and 2 must be in the same pg'
    @@port3 = eqpi.ports[2].port
    @@port4 = eqpi.ports[3].port
    @@pg2 = eqpi.ports[2].pg
    assert_equal @@pg2, eqpi.ports[3].pg, 'ports 3 and 4 must be in the same pg'
    refute_equal @@pg1, @@pg2, 'port groups must be different'
    # create 2 test lots
    assert lots = @@svtest.new_lots(2), 'error creating test lots'
    @@testlots += lots
    @@lot, @@lot2 = lots
    #
    assert @@svtest.get_empty_carrier(n: 2), 'not enough empty carriers'
    $log.info "sorter #{@@sorter}, lots: #{lots.inspect}"
    #
    $setup_ok = true
  end

  def test11_unspecified_sorter  # a.k.a. wildcard sorter job
    assert @@svtest.cleanup_eqp(@@sorter, mode: 'Auto-2')
    assert @@sv.merge_lot_family(@@lot)
    assert @@svtest.cleanup_carrier(@@sv.lot_info(@@lot).carrier)
    # get an empty carrier
    assert carrier = @@svtest.get_empty_carrier, 'no empty carriers found'
    @@sv.carrier_xfer_status_change(carrier, '', @@svtest.stocker, manualin: true)
    # create sort job with no sorter specified (MSR 496889)
    assert sjid = @@sv.sj_create('*', '*', '*', @@lot, carrier), 'error creating unspecified sorter job'
    # list wildcard sorter job
    assert_equal 1, @@sv.sj_list(sjid: sjid).size, 'error listing wildcard sorter job'
    # cancel wildcard sorter job
    assert_equal 0, @@sv.sj_cancel(sjid), 'error canceling unspecified sorter job'
    # create new wildcard sorter job
    assert sjid = @@sv.sj_create('*', '*', '*', @@lot, carrier), 'error creating unspecified sorter job'
    # update
    assert_equal 0, @@sv.sj_update(sjid, @@sorter, @@port1, @@port2, @@pg1), 'error updating sorter job'
    # cancel updated job
    assert_equal 0, @@sv.sj_cancel(sjid), 'error canceling updated sorter job'
  end

  def test21_SortJobCreateReq
    $setup_ok = false
    #
    assert @@svtest.cleanup_eqp(@@sorter, mode: 'Auto-1')
    assert @@sv.merge_lot_family(@@lot)
    # get an empty carrier
    assert carrier = @@svtest.get_empty_carrier, 'no empty carriers found'
    ### customization removed in C7.18 create regular sort job with auto-split, select 4 wafers to move
    assert sj = @@sv.sj_create(@@sorter, @@port1, @@port2, @@lot, carrier), 'error creating sorter job'
    # check the carrier flags
    sji = @@sv.sj_status(sj)
    ci = @@sv.carrier_status(sji.strSorterComponentJobListSequence.first.originalCassetteID.identifier)
    assert ci.sjexists, 'SJ Exists flag not set for src carrier'
    ci = @@sv.carrier_status(sji.strSorterComponentJobListSequence.first.destinationCassetteID.identifier)
    assert ci.sjexists, 'SJ Exists flag not set for tgt carrier'
    # check the lot has been split and 4 wafers are in the sort job
    assert_equal [@@lot], @@sv.lot_family(@@lot), 'wrong split (old customized SiView?'
    # lc = @@sv.lot_family(@@lot).last
    # assert_equal 4, @@sv.lot_info(lc).nwafers, "error in the created child lot #{lc}"
    @@sj = sj
    #
    $setup_ok = true
  end

  def test31_SortJobList
    assert_equal 0, @@sv.sj_list(sjid: 'NotExSjXXWW').size, 'error in SortJobList for non-existing sj'
    assert_equal 1, @@sv.sj_list(sjid: @@sj).size, "error in SortJobList for sj #{@@sj.inspect}"
    refute_nil @@sv.sj_status(@@sj), "error in SortJobStatus for sj #{@@sj.inspect}"
  end

  # stress test for TxSortJobListInq
  def test32_SortJobList_stress
    100.times {|i| assert @@sv.sj_list}
  end

  def test41_SortJobStatusChange
    assert_equal 0, @@sv.sj_status_change(@@sorter, @@sj, 'Completed'), "error in SortJobStatusChange for sj #{@@sj.inspect}"
  end

  def test51_SortJobCancel
    assert_equal 0, @@sv.sj_cancel(@@sj), "error in SortJobcancel for sj #{@@sj.inspect}"
    assert_empty @@sv.sj_list(sjid: @@sj), "error deleting sj #{@@sj.inspect}"
  end

  def test61_Separate2
    assert @@svtest.cleanup_eqp(@@sorter, mode: 'Auto-2')
    assert @@sv.merge_lot_family(@@lot)
    assert @@svtest.cleanup_carrier(@@sv.lot_info(@@lot).carrier)
    [@@port1, @@port2].each {|p| assert_equal 0, @@sv.eqp_port_status_change(@@sorter, p, 'LoadReq')}
    # create 2 child lots
    assert lc1 = @@sv.lot_split(@@lot, 2)
    assert lc2 = @@sv.lot_split(@@lot, 2)
    # get 2 empty carriers and stock them in
    assert carriers = @@svtest.get_empty_carrier(n: 2, stockin: true), 'not enough empty carriers'
    # create sorter job, user NOX-UNITTEST to prevent SJC from cleaning up the result
    assert sj = @@svnox.sj_create(@@sorter, @@port1, @@port2, [lc1, lc2], carriers)
    assert wait_for(timeout: 600) {
      # avoid dependency on AutoProc
      p = @@sv.eqp_info(@@sorter).ports.find {|p| p.port == @@port2}
      if p.state == 'UnloadReq'
        @@sv.eqp_unload(@@sorter, @@port2, p.loaded_carrier) unless p.loaded_carrier.empty?
        @@sv.eqp_port_status_change(@@sorter, @@port2, 'LoadReq')  # work around an intermittend bug in JavaEI in conjunction with AutoProc
      end
      # wait for job completion
      @@sv.sj_status(sj).strSorterComponentJobListSequence.empty?
    }, "error in sorter job #{sj.inspect}"
    @@sv.eqp_cleanup(@@sorter)
  end

  # TODO: Disabled for now
  def XXtest62_2SorterJobs
    assert @@svtest.cleanup_eqp(@@sorter, mode: 'Auto-2')
    @@sv.merge_lot_family(@@lot)
    @@sv.merge_lot_family(@@lot2)
    # stock in carriers
    li = @@sv.lot_info(@@lot)
    assert_equal 0, @@sv.carrier_xfer_status_change(li.carrier, '', @@svtest.stocker, manualin: true)
    li = @@sv.lot_info(@@lot2)
    assert_equal 0, @@sv.carrier_xfer_status_change(li.carrier, '', @@svtest.stocker, manualin: true)
    # get 2 empty carriers and stock them in
    assert carriers = @@svtest.get_empty_carrier(n: 2, stockin: true), 'no empty carriers found'
    # create 2 sorter jobs using the same sorter ports
    assert sj1 = @@sv.sj_create(@@sorter, @@port1, @@port2, @@lot, carriers[0]), 'error creating sorter job1'
    assert sj2 = @@sv.sj_create(@@sorter, @@port1, @@port2, @@lot2, carriers[1]), 'error creating sorter job2'
  end

  def test63_concurrent_update_cancel
    # test with wildcard sorter jobs, mode Auto-2
    assert @@svtest.cleanup_eqp(@@sorter, mode: 'Auto-2')
    # stock in carriers
    assert @@svtest.cleanup_carrier(@@sv.lot_info(@@lot).carrier)
    assert @@svtest.cleanup_carrier(@@sv.lot_info(@@lot2).carrier)
    # get 2 empty carriers and stock them in
    assert carriers = @@svtest.get_empty_carrier(n: 2, stockin: true), 'no empty carriers found'
    #
    # create 2 wildcard sorter jobs
    assert sj1 = @@svnox.sj_create('*', '*', '*', @@lot, carriers[0])
    assert sj2 = @@svnox.sj_create('*', '*', '*', @@lot2, carriers[1])
    # set EI's sjnotification wait time to ensure concurrency and SiView timeout
    assert @@svtest.eis.set_variable(@@sorter, 'sjnotificationwaittime', 130000), 'error setting JavaEI variable'
    # concurrently update 1st job and cancel 2nd job
    res1 = nil
    t1 = Thread.new {res1 = SiView::MM.new($env).sj_update(sj1, @@sorter, @@port1, @@port2, @@pg1)}
    sleep 5  # sj_update must have started
    res = @@sv.sj_cancel(sj1)
    assert [0, 324].member?(res), 'wrong return code from sj_cancel'
    t1.join
    assert_equal 2104, res1 # no response from TCS
    # will fail without MSR 653154
    assert_equal 0, @@sv.sj_cancel(sj2)
  end

  def test64_concurrent_update_create   # MSR 653154
    # test with wildcard sorter jobs, mode Auto-2
    assert @@svtest.cleanup_eqp(@@sorter, mode: 'Auto-2')
    # stock in carriers
    assert @@svtest.cleanup_carrier(@@sv.lot_info(@@lot).carrier)
    assert @@svtest.cleanup_carrier(@@sv.lot_info(@@lot2).carrier)
    # get 2 empty carriers and stock them in
    assert carriers = @@svtest.get_empty_carrier(n: 2, stockin: true), 'no empty carriers found'
    #
    # create 1 wildcard sorter job
    sj1 = @@svnox.sj_create('*', '*', '*', @@lot, carriers[0])
    assert sj1, "error creating sorter job1"
    # set EI's sjnotification wait time to ensure concurrency and SiView timeout
    @@svtest.eis.set_variable(@@sorter, 'sjnotificationwaittime', 130000)
    ##@@svtest.eis.set_variable(nil, "defaultsjnotificationwaittime", 130000)   # for sandbox
    # concurrently update 1st job and create a 2nd job
    res1 = nil
    t1 = Thread.new {res1 = SiView::MM.new($env).sj_update(sj1, @@sorter, @@port1, @@port2, @@pg1)}
    sleep 5
    # create 2nd wildcard sorter job, sj_update must be running
    assert sj2 = @@svnox.sj_create('*', '*', '*', @@lot2, carriers[1])
    t1.join
    assert_equal 2104, res1 # no response from TCS
    # fails without MSR 653154
    assert [0, 3212].member?(@@sv.sj_cancel(sj2)), 'failed to cancel SJ or jCAP CaptureWildCard job failed'
    assert [0, 3212].member?(@@sv.sj_cancel(sj1)), 'failed to cancel SJ or jCAP CaptureWildCard job failed'
  end

  def test65_concurrent_cancel_create
    # test with regular sorter jobs, fixed with MSR 691923
    assert @@svtest.cleanup_eqp(@@sorter, mode: 'Auto-2')
    # stock in carriers
    assert @@svtest.cleanup_carrier(@@sv.lot_info(@@lot).carrier)
    assert @@svtest.cleanup_carrier(@@sv.lot_info(@@lot2).carrier)
    # get 2 empty carriers and stock them in
    assert carriers = @@svtest.get_empty_carrier(n: 2, stockin: true), 'no empty carriers found'
    #
    # create 1 regular sorter job and set it to Error
    assert sj1 = @@sv.sj_create(@@sorter, @@port1, @@port2, @@lot, carriers[0])
    assert_equal 0, @@sv.sj_status_change(@@sorter, sj1, 'Error')
    # set EI's sjnotification wait time to ensure concurrency and NO SiView timeout
    @@svtest.eis.set_variable(@@sorter, 'sjnotificationwaittime', 20000)
    # concurrently create a 2nd job and delete first one
    sj2 = nil
    t1 = Thread.new {sj2 = SiView::MM.new($env).sj_create(@@sorter, @@port3, @@port4, @@lot2, carriers[1])}
    # cancel 1st sorter job, currently fails
    @@svtest.eis.set_variable(@@sorter, 'sjnotificationwaittime', 2000)
    $log.info "waiting 5 s before canceling the 2nd concurrent sorter job.."; sleep 5
    assert_equal 0, @@sv.sj_cancel(sj1), 'error canceling 1st SJ, check if SJC is interfering'
    t1.join
    assert sj2
    assert_equal 0, @@sv.sj_cancel(sj2), "error canceling 2nd SJ #{sj2}"
  end

  # MSR 815910
  def test71_sorterjob_stbcontrollot
    assert @@svtest.cleanup_eqp(@@sorter, mode: 'Auto-2')
    assert tlot = @@svtest.new_srclot(lottype: 'Equipment Monitor'), 'error creating srclot'
    carrier = @@sv.lot_info(tlot).carrier
    @@testlots << tlot
    assert_equal 0, @@sv.carrier_xfer_status_change(carrier, '', @@svtest.stocker)
    assert child = @@sv.lot_split_notonroute(tlot, 2), 'error splitting lot'
    assert tgt_carrier = @@svtest.get_empty_carrier, 'no empty carrier found'
    assert sj = @@sv.sj_create(@@sorter, @@port1, @@port2, tlot, tgt_carrier), 'error creating SJ'
    # test with 1 srclot
    assert_nil @@sv.stb_controllot('Equipment Monitor', @@svtest.product_eqpmonitor, tlot), 'STB should fail'
    assert_equal 1752, @@sv.rc, 'wrong return code'
    # test with 2 srclots
    assert_nil @@sv.stb_controllot('Equipment Monitor', @@svtest.product_eqpmonitor, [tlot, child]), 'STB should fail'
    assert_equal 1752, @@sv.rc, 'wrong return code'
  end

  #MSR815910
  def test72_sorterjob_newlot
    assert @@svtest.cleanup_eqp(@@sorter, mode: 'Auto-2')
    # schedule a lot and create a srclot (with carrier)
    assert vlot = @@svtest.new_srclot, 'error creating srclot'
    @@testlots << vlot
    carrier = @@sv.lot_info(vlot).carrier
    assert_equal 0, @@sv.carrier_xfer_status_change(carrier, '', @@svtest.stocker, manualin: true)
    assert lot = @@sv.new_lot_release(product: @@svtest.product, srclot: vlot), 'new lot release failed'
    @@testlots << lot
    assert tgt_carrier = @@svtest.get_empty_carrier, 'no empty carriers found'
    # create SJ and try to STB
    assert @@sv.sj_create(@@sorter, @@port1, @@port2, vlot, tgt_carrier), 'error creating SJ'
    #
    assert_nil @@sv.stb(lot)
    assert_equal 1752, @@sv.rc, 'wrong return code'
  end

  # MES-3965,
  ### sf 2021-01-18: should fail because customization was removed in C7.18
  def xxtest73_sj_split #rename, because customization was removed in C7.18 and test not needed anymore
    assert @@svtest.cleanup_eqp(@@sorter)
    assert lot = @@svtest.new_lot, 'error creating test lot'
    @@testlots << lot
    assert_equal 0, @@sv.lot_hold(lot, 'X-M1', memo: 'QA 1')
    assert_equal 0, @@sv.lot_hold(lot, 'X-M2', memo: 'QA 2')
    assert ec = @@svtest.get_empty_carrier, 'no empty carrier'
    assert_equal 0, @@sv.carrier_xfer_status_change(ec, '', @@svtest.stocker, manualin: true)
    assert sjid = @@sv.sj_create(@@sorter, @@port1, @@port2, lot, ec, select: 2), 'error creating sorter job'
    assert @@sv.claim_sj_execute(sjid: sjid, report: true), 'error executing sorter job'
    $log.info 'verifying child lot holds'
    lc = @@sv.lot_family(lot).last
    refute_equal lot, lc, 'lot has not been split'
    assert lh1 = @@sv.lot_hold_list(lc, reason: 'X-M1').first, 'missing child lot hold'
    assert_equal 'QA 1', lh1.memo, 'wrong memo'
    assert lh2 = @@sv.lot_hold_list(lc, reason: 'X-M2').first, 'missing child lot hold'
    assert_equal 'QA 2', lh2.memo, 'wrong memo'
    # merge
    assert_equal 0, @@sv.wafer_sort(lc, @@sv.lot_info(lot).carrier)
    assert_equal 0, @@sv.lot_merge(lot, lc)
  end

  # Test Omit of code merge for split wafer lot without Hold Release MSR 742801
  def test74_sj_split_onhold
    assert_equal 0, @@sv.lot_cleanup(@@lot)
    assert_equal 0, @@sv.lot_hold(@@lot, nil)
    carrier = @@svtest.get_empty_carrier
    assert sj = @@sv.sj_create('*', '*', '*', @@lot, carrier)
    cl = @@sv.lot_split(@@lot, 5)
    assert_equal 1752, @@sv.rc
    assert_equal 0, @@sv.sj_cancel(sj)
    assert_equal 0, @@sv.lot_hold_release(@@lot, nil)
  end

end
