=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: Steffen Steidten, 2012-04-16

History:
  2014-11-18 ssteidte, added test04..7 for FCHL and ProcessHold splits
  2015-10-08 sfrieske, added checks for sublottype (MSR351504) from Test098
  2016-06-02 dsteger,  added script parameter history check (MES-3050, C7.6)
  2017-10-06 sfrieske, added tests for MSRs 534346 and 1229417 (C7.7)
  2019-12-05 sfrieske, minor cleanup, renamed from Test090_LotSplit
  2021-01-15 sfrieske, moved SiView::MM#new_srclot_for_product to SiView::Test#new_srclot
  2021-08-30 sfrieske, moved siviewmmdb.rb to siview/mmdb.rb
=end

require 'siview/mmdb'
require 'SiViewTestCase'


# Test TxLotSplit et al. including MSRs 57546 (and 641207), 801115, 529507, 436293, 351455, 351504, 534346, 1229417
class MM_Lot_Split < SiViewTestCase
  @@user2 = 'NOX-UNITTEST'
  @@sparam = 'UTstring'
  @@sparam_mergecheck = 'UTmergecheck' # with inheritance and mergecheck (in the parameter description)
  @@rework_route = 'UTRW001.01'
  @@rework_opNo = '1000.400'
  @@rework_ret = '1000.400'
  @@psm_route = 'UTBR001.01'
  @@psm_opNo = '1000.200'
  @@psm_ret = '1000.300'
  @@reason = 'AEHL'
  @@slt2 = 'ES'   # for test21
  @@check_parameterhistory = true   # MES-3050
  @@parameterhistory_timeout = 180  # 120
  @@use_mds = false

  @@testlots = []


  def self.after_all_passed
    @@testlots.each {|lot| @@sv.delete_lot_family(lot)}
  end


  def test00_setup
    $setup_ok = false
    #
    assert @@mmdb = SiView::MMDB.new($env, use_mds: @@use_mds) if  @@check_parameterhistory
    assert @@sv_user = SiView::MM.new("#{$env}_gms")
    assert @@lot = @@svtest.new_lot
    @@testlots << @@lot
    #
    $setup_ok = true
  end

  def test01_split_merge_multiple_times
    assert_equal 0, @@sv.lot_cleanup(@@lot)
    3.times {|i|
      $log.info "pass #{i}"
      assert_equal 0, @@sv.user_parameter_change('Lot', @@lot, @@sparam, "QA#{i}")
      assert lc = @@sv.lot_split(@@lot, 2)
      assert_equal "QA#{i}", @@sv.user_parameter('Lot', lc, name: @@sparam).value
      assert_equal 0, @@sv.lot_merge(@@lot, lc)
    }
  end

  def test02_split_merge_lot_onhold
    assert_equal 0, @@sv.lot_cleanup(@@lot)
    assert_equal 0, @@sv.lot_hold(@@lot, @@reason)
    3.times {|i|
      $log.info "pass #{i}"
      assert_equal 0, @@sv.user_parameter_change('Lot', @@lot, @@sparam, "QA#{i}")
      assert lc = @@sv.lot_split(@@lot, 2, onhold: true)
      assert_equal "QA#{i}", @@sv.user_parameter('Lot', lc, name: @@sparam).value
      assert_equal 0, @@sv.lot_merge(@@lot, lc)
    }
  end

  def test03_split_actions
    assert_equal 0, @@sv.lot_cleanup(@@lot)
    assert_equal 0, @@sv.user_parameter_change('Lot', @@lot, @@sparam, 'VALUE')
    tstart = Time.now
    lot = @@lot
    children = []
    # Normal split
    children << @@sv.lot_split(lot, 1)
    # Onhold split
    assert_equal 0, @@sv.lot_hold(lot, @@reason), "hold failed"
    # SplitWithoutLotHoldRelease
    children << @@sv.lot_split(lot, 1, onhold: true)
    # SplitWithHoldRelease
    children << @@sv.lot_split(lot, 1, onhold: true, release: true)
    # Auto split
    children << @@sv.AMD_WaferSorterAutoSplitReq(lot, 1)
    # PSM
    assert_equal 0, @@sv.lot_experiment(lot, 1, @@psm_route, @@psm_opNo, @@psm_ret, merge: @@psm_ret), "failed to setup PSM"
    2.times {|i|
      assert_equal 0, @@sv.lot_experiment_reset(lot, @@psm_opNo), "failed to reset PSM" if i > 0
      assert_equal 0, @@sv.lot_opelocate(lot, @@psm_opNo), "failed to do PSM split"
      lc = @@sv.lot_family(lot).last
      children << lc
      [lot, lc].each {|l| assert_equal 0, @@sv.lot_opelocate(l, @@psm_ret), "Failed to locate to return op"}
      assert_equal 0, @@sv.lot_merge(lot, lc), "failed to merge lots"
    }
    assert @@sv.lot_experiment_delete(lot), "failed to clear PSM"
    # Partial Rework
    assert_equal 0, @@sv.lot_opelocate(lot, @@rework_opNo), 'error locating lot'
    assert lc = @@sv.lot_partial_rework(lot, 1, @@rework_route, return_opNo: @@rework_ret)
    children << lc
    assert_equal 0, @@sv.lot_partial_rework_cancel(lot, lc), 'error merging after rework'
    # Partial Rework w/o hold release
    assert_equal 0, @@sv.lot_hold(lot, @@reason), "hold failed"
    assert lc = @@sv.lot_partial_rework(lot, 1, @@rework_route, return_opNo: @@rework_ret, onhold: true)
    children << lc
    assert_equal 0, @@sv.lot_partial_rework_cancel(lot, lc), 'error merging after rework'
    # Partial Rework with hold release
    assert lc = @@sv.lot_partial_rework(lot, 1, @@rework_route, return_opNo: @@rework_ret, onhold: true, release: true)
    children << lc
    assert_equal 0, @@sv.lot_partial_rework_cancel(lot, lc), 'error merging after rework'
    # check all children for script parameter
    $log.info "#{children.inspect}"
    children.each {|lc|
      assert_equal 'VALUE', @@sv.user_parameter('Lot', lc, name: @@sparam).value, "wrong value for #{lc}"
      if @@check_parameterhistory
        $log.info "getting history for #{lc} starting at #{tstart.utc.strftime('%F_%TZ')}"
        evs = nil
        assert wait_for(timeout: @@parameterhistory_timeout) {
          evs = @@mmdb.user_parameter_history(id: lc, tstart: tstart, name: @@sparam)
          evs && !evs.empty?
        }, 'no history record in DB'
        assert_equal 1, evs.size, "wrong number of entries for #{lc}"
      end
    }
    # Merge
    assert @@sv.merge_lot_family(@@lot, nosort: true), 'error merging lot family'
  end

  # MSR436293
  def test04_FCHL_split
    assert_equal 0, @@sv.lot_cleanup(@@lot)
    assert @@sv.claim_process_lot(@@lot, running_hold: true)
    assert @@sv.lot_hold_list(@@lot, reason: 'FCHL').first
    assert lc = @@sv.lot_split(@@lot, 2, onhold: true)
    assert_equal 0, @@sv.lot_merge(@@lot, lc)
  end

  # MSR436293
  def test05_FCHL_partial_rework
    assert_equal 0, @@sv.lot_cleanup(@@lot, opNo: @@rework_opNo, offset: -1)
    assert @@sv.claim_process_lot(@@lot, running_hold: true)
    assert @@sv.lot_hold_list(@@lot, reason: 'FCHL').first
    # partial rework without hold release
    assert lc = @@sv.lot_partial_rework(@@lot, 1, @@rework_route, return_opNo: @@rework_ret, onhold: true, inheritholds: true)
    assert @@sv.lot_hold_list(lc, reason: 'FCHL').first
    assert_equal 0, @@sv.lot_partial_rework_cancel(@@lot, lc)
    # partial rework with hold release
    assert lc = @@sv.lot_partial_rework(@@lot, 1, @@rework_route, return_opNo: @@rework_ret, onhold: true, inheritholds: false)
    assert_empty @@sv.lot_hold_list(lc, reason: 'FCHL')
    assert_equal 0, @@sv.lot_partial_rework_cancel(@@lot, lc)
  end

  def test10_split_mergecheck
    assert_equal 0, @@sv.lot_cleanup(@@lot)
    do_action(@@lot,
      lambda {|lot| @@sv.lot_split(lot, 1)},
      lambda {|lot, child| @@sv.lot_merge(lot, child)}
    )
  end

  def test11_partial_rework
    assert_equal 0, @@sv.lot_cleanup(@@lot, opNo: @@rework_opNo)
    do_action(@@lot,
      lambda {|lot| @@sv.lot_partial_rework(lot, 1, @@rework_route, return_opNo: @@rework_ret)},
      lambda {|lot, child| @@sv.lot_partial_rework_cancel(lot, child)}
    )
  end

  def test12_split_merge_notonroute
    assert srclot = @@svtest.new_srclot, 'error creating srclot'
    @@testlots << srclot
    do_action(srclot,
      lambda {|lot| @@sv.lot_split_notonroute(lot, 1)},
      lambda {|lot, child| @@sv.lot_merge_notonroute(lot, child)}
    )
  end

  def test13_split_notonroute_timestamps
    # prepare a vendor lot (creates wafer ids for Split to appear in history) and split
    assert srclot = @@svtest.new_srclot, 'error creating srclot'
    @@testlots << srclot
    assert lc = @@sv.lot_split_notonroute(srclot, 2)
    $log.info 'verifying timestamps'
    sleep 20
    # verify reportTimeStamp and waferHistoryTimeStamp for split match (DB: CLAIM_TIME and WFRHS_TIME, MSR 534346)
    [srclot, lc].each {|l|
      assert hist = @@sv.lot_operation_history(l, history: false, raw: true), 'no lot history'
      assert r = hist.find {|e| e.operationCategory == 'Split'}, 'no split record'
      assert_equal r.reportTimeStamp, r.waferHistoryTimeStamp, 'wrong timestamp'
    }
  end

  # MSR 1229417, bankhold user ID inheritance on split
  def test14_split_bankhold
    # bank in a lot and set a bank hold
    assert lot = @@svtest.new_lot(bankin: true), 'error creating lot'
    assert_equal 0, @@sv.lot_bankhold(lot), 'error setting bank hold'
    # split as different user and verify hold user
    assert lc = @@sv_user.lot_split(lot, 2, onhold: true), 'error splitting lot'
    assert_equal @@sv.user, @@sv.lot_hold_list(lc).first.user, 'wrong hold record user'
    assert_equal 0, @@sv.lot_merge(lot, lc)
  end

  # MSR 351504
  def test21_different_sublottypes
    assert_equal 0, @@sv.lot_cleanup(@@lot)
    # split lot, change child sublottype and merge
    assert lc = @@sv.lot_split(@@lot, 5, mergeop: nil), 'error splitting lot'
    assert_equal 0, @@sv.sublottype_change(lc, @@slt2), 'error changing sublottype'
    assert_equal 159, @@sv.lot_merge(@@lot, lc), 'merge must fail'
    assert_equal 0, @@sv.sublottype_change(lc, 'PO'), 'error changing sublottype'
    assert_equal 0, @@sv.lot_merge(@@lot, lc), 'merge failed'
  end



  # aux methods

  # split/merge with changed script parameters
  def do_action(lot, split_action, merge_action)
    # set parameter and execute split
    assert_equal 0, @@sv.user_parameter_change('Lot', lot, @@sparam_mergecheck, 'VALUE1')
    assert child = split_action.call(lot), "failed to split lot"
    assert_equal 'VALUE1', @@sv.user_parameter('Lot', child, name: @@sparam_mergecheck).value
    # modify value and try to execute merge
    assert_equal 0, @@sv.user_parameter_change('Lot', child, @@sparam_mergecheck, 'VALUE2')
    assert_equal 159, merge_action.call(lot, child)
    assert @@sv.msg_text.include?("[#{@@sparam_mergecheck}], Parent: [VALUE1], Child: [VALUE2]")
    # empty value and try to execute merge
    assert_equal 0, @@sv.user_parameter_change('Lot', lot, @@sparam_mergecheck, '')
    assert_equal 159, merge_action.call(lot, child)
    assert @@sv.msg_text.include?("[#{@@sparam_mergecheck}], Parent: [], Child: [VALUE2]")
    # set to equal values and merge
    assert_equal 0, @@sv.user_parameter_change('Lot', lot, @@sparam_mergecheck, 'VALUE1')
    assert_equal 0, @@sv.user_parameter_change('Lot', child, @@sparam_mergecheck, 'VALUE1')
    assert_equal 0, merge_action.call(lot, child)
  end

end
