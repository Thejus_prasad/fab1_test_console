=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: Paul Peschel, 2013-01-28 migrate from Test069_TxGetRecipeName.java

History:
  2013-07-17 ssteidte, (re-)start dummy EI
  2014-11-27 ssteidte, cleanup
  2019-12-09 sfrieske, minor cleanup, renamed from Test069_TxGetRecipeName
=end

require 'SiViewTestCase'

class MM_Eqp_TxGetRecipeName < SiViewTestCase
  @@eqp = 'UTC001'
  @@port = 'P2'
  @@opNo = '1000.100'
  @@mrecipe = 'P.UTMR000.01'
  @@precipe = 'UTMR000'

  def self.after_all_passed
    @@sv.eqp_cleanup(@@eqp)
    @@sv.delete_lot_family(@@lot)
  end


  def test00_setup
    $setup_ok = false
    #
    # set eqp mode and status
    assert @@svtest.cleanup_eqp(@@eqp, mode: 'Off-Line-1')
    # get lot and locate it
    assert @@lot = @@svtest.new_lot
    assert_equal 0, @@sv.lot_opelocate(@@lot, @@opNo)
    #
    $setup_ok = true
  end

  def test10_getrecipename
    assert_equal 0, @@sv.carrier_xfer_status_change(@@sv.lot_info(@@lot).carrier, 'EO', @@eqp)
    assert cj = @@sv.slr(@@eqp, port: @@port, lot: @@lot), "error in SLR for lot #{@@lot} on eqp #{@@eqp}"
    assert rcp = @@sv.eqp_recipename(cj)
    assert_equal @@lot, rcp.first.lotId
    assert_equal @@mrecipe, rcp.first.machineRecipe
    assert_equal @@precipe, rcp.first.physicalRecipe
  end
  
end
