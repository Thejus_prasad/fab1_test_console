=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: sfrieske, 2017-11-01

History:
  2019-12-10 sfrieske, minor cleanup, renamed from MM_VendorLotReceive
  2021-08-27 sfrieske, require uniquestring instead of misc
=end

require 'util/uniquestring'
require 'RubyTestCase'


# Verify TxVendLotReceiveReq, MES-3505 and MES-3695 (vendorlot has spaces)
class MM_Lot_VendLotReceive < RubyTestCase
  @@bank = 'UT-RAW'
  @@srcproduct = 'UT-RAW.00'

  @@testlots = []


  def self.after_all_passed
    @@testlots.each {|lot| @@sv.delete_lot_family(lot)}
  end


  def test11_happy
    assert vlot = @@sv.vendorlot_receive(@@bank, @@srcproduct, 25)
    @@testlots << vlot
  end

  def test12_vendorlot_notempty
    assert vlot = @@sv.vendorlot_receive(@@bank, @@srcproduct, 25, vendorlot: 'XXX')
    @@testlots << vlot
  end

  def test13_vendorlot_spaces
    assert vlot = @@sv.vendorlot_receive(@@bank, @@srcproduct, 25, vendorlot: 'XX X')
    @@testlots << vlot
  end

  def test22_lot_notempty
    assert vlot = @@sv.vendorlot_receive(@@bank, @@srcproduct, 25, lot: unique_string)
    @@testlots << vlot
  end

  # MSR1351072, fails in C7.7.13 after MES-3695 has been fixed
  def testdev23_lot_spaces
    vlot = @@sv.vendorlot_receive(@@bank, @@srcproduct, 25, lot: unique_string + ' X')
    if vlot
      @@testlots << vlot
      fail 'lot with spaces in the lot ID must not be received'
    end
  end

end
