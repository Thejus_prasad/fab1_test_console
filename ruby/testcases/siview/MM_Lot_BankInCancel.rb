=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: Daniel Steger, 2015-11-16

History:
  2019-12-09 sfrieske, minor cleanup, renamed from Test311_BankInCancel
=end

require 'SiViewTestCase'


# MSR 935833 TxBankInCancelReq to return to last MainPD version of lot, 
# instead of unrecoverable error if lot product MainPD Version is different from last MainPD Version of that lot
class MM_Lot_BankInCancel < SiViewTestCase
  @@product1 = 'UT-PROD-BANKIN.01'
  @@route1 = 'UTRT-BANKIN.01'
  @@route2 = 'UTRT-BANKIN.02'

  @@product_check = '0'
  

  def self.after_all_passed
    @@sm.change_product_route(@@product1, @@route1)
    @@sv.delete_lot_family(@@lot)
  end


  def setup
    return unless self.class.class_variable_defined?(:@@lot)
    assert @@sm.change_product_route(@@product1, @@route1), "failed to release #{@@product1}"
    assert_equal 0, @@sv.lot_opelocate(@@lot, :last), "failed to locate lot"
    assert_equal 0, @@sv.lot_bankin(@@lot) if @@sv.lot_info(@@lot).bank.empty?
    assert_equal 'COMPLETED', @@sv.lot_info(@@lot).status
  end
  

  def test00_setup
    $setup_ok = false
    #
    assert_equal @@product_check, @@sv.environment_variable[1]['SP_BANKINCANCEL_PRODUCT_CHECK'], "product check is not correctly set"
    if active_route = @@sm.object_info(:mainpd, @@route1.split('.').first + '.##').first
      assert @@sm.stop_active_version(:mainpd, active_route.name), "failed to stop active versioning"
    end
    # assign product route
    assert @@sm.change_product_route(@@product1, @@route1), "failed to release #{@@product1}"
    assert @@lot = @@svtest.new_lot(product: @@product1), "failed to create new lot"
    #
    $setup_ok = true
  end
  
  def test01_bankincancel # smoke test
    assert_equal 0, @@sv.lot_bankin_cancel(@@lot)
  end
  
  def test02_other_route
    assert @@sm.change_product_route(@@product1, @@route2), "failed to release #{@@product1}"
    check_bankin_cancel(@@lot)
  end
  
  def test03_obsolete_prod
    skip "not applicaple since product_check is 0" if @@product_check == '0'
    assert @@sm.change_product_route(@@product1, '', state: 'Obsolete'), "failed to release #{@@product1}"
    assert_equal 0, @@sv.lot_bankin_cancel(@@lot)
    assert_equal @@route1, @@sv.lot_info(@@lot).route
  end
  
  def test04_new_version
    assert @@sm.reserve_active(:mainpd, @@route2), "failed to activate #{@@route2}"
    assert @@sm.release_objects(:mainpd, @@route2), "failed to release #{@@route2}"
    assert @@sm.change_product_route(@@product1, @@route2.sub(/\.\w\w\z/, '.##')), "failed to release #{@@product1}"    
    check_bankin_cancel(@@lot)
  end
  
  
  def check_bankin_cancel(lot)
    if @@product_check == '0'
      assert_equal 0, @@sv.lot_bankin_cancel(lot)
      assert_equal @@route1, @@sv.lot_info(lot).route
    else
      assert_equal 2939, @@sv.lot_bankin_cancel(lot)
    end
  end
  
end
