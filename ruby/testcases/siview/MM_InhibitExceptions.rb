=begin
(c) Copyright 2012 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES and the GLOBALFOUNDRIES logo are trademarks of GLOBALFOUNDRIES Inc.

Author: Ulrich Koerner (Saxonia Systems AG), 2012-11-09

History:
  2018-05-09 sfrieske, minor cleanup incl removal of R10 code, added test091 for MSR 1147819
  2019-11-19 sfrieske, minor cleanup
  2019-12-17 sfrieske, cleanup, renamed from Test133_InhibitExceptions
  2020-08-26 sfrieske, minor cleanup
  2020-03-25 sfrieske, minor cleanup
=end

require 'siview'  # to get SiView::MM::InhibitClasses early enough for dynamic testcase creation
require 'SiViewTestCase'


# Test Inhibit Exceptions, incl MSR579564, MSR777042
class MM_InhibitExceptions < SiViewTestCase
  @@sv_defaults = {product: 'UT-INHIBIT.01', route: 'UTRT-INHIBIT.01'}
  @@opNo_fb = '1000.2000'
  @@opNo_ib = '1000.2000'
  @@eqp_fb = 'UTR003'
  @@eqp_ib = 'UTI003RTCL'
  @@reticle_fb = 'UTRETICLEFBRTCL'
  @@reticle_ib = 'UTRETICLEIBRTCL'

  @@fixture = 'UTFIXTURE0001'
  @@eqp_fixture = 'UTF003'
  @@opNo_fixture = '1100.1000'
  @@test_single = true  # only single application of inhibit exception
  @@test_fixtures = false

  @@inhibit_entities = {}
  @@testlots = []


  def self.after_all_passed
    @@testlots.each {|lot| @@sv.delete_lot_family(lot)}
    @@inhibit_entities.keys.each {|k| @@sv.inhibit_cancel(k, @@inhibit_entities[k][0], silent: true)}
  end


  def test000_setup
    $setup_ok = false
    #
    assert @@lots = @@svtest.new_lots(2), 'error creating test lots'
    @@testlots += @@lots
    #
    $setup_ok = true
  end

  # inhibit exception deleted when lot is deleted (MSR 777042)
  def test090_lot_deleted
    assert lot = @@svtest.new_lot, 'error creating test lot'
    @@testlots << lot
    # set inhibit and inhibit exception
    assert inh = @@sv.inhibit_entity(:eqp, @@eqp_fb), 'failed to register inhibit'
    assert_equal 0, @@sv.inhibit_exception_register(lot, inh), 'failed to register inhibit exception'
    refute_empty @@sv.inhibit_exception_list(lot, inh), 'missing inhibit exception'
    # delete lot family and verify inhibit exception is deleted - MSR777042
    assert_equal 0, @@sv.delete_lot_family(lot, delete: true)
    assert_empty @@sv.inhibit_exception_list(lot, inh), "wrong inhibit exception for #{lot}"
  end

  # inhibit exception inherited (MSR 1147819)
  def test091_exception_inherited
    lot = @@lots.first
    assert @@sv.merge_lot_family(lot)
    assert_equal 0, @@sv.lot_cleanup(lot, opNo: @@opNo_fb)
    assert_equal 0, @@sv.eqp_cleanup(@@eqp_fb)
    # set inhibit and inhibit exception
    assert inh = @@sv.inhibit_entity(:eqp, @@eqp_fb), 'failed to register inhibit'
    assert_equal 0, @@sv.inhibit_exception_register(lot, inh), 'failed to register inhibit exception'
    assert_equal 1, @@sv.inhibit_exception_list(lot, inh).size, 'missing inhibit exception'
    # split lot and verify child lot is also excepted
    assert lc = @@sv.lot_split(lot, 3), 'error splitting lot'
    assert_equal 1, @@sv.inhibit_exception_list(lc, inh).size, 'missing inhibit exception'
  end

  def test100_setup_fb
    $setup_ok = false
    #
    assert_equal 0, @@sv.eqp_cleanup(@@eqp_fb), "failed to cleanup #{@@eqp_fb}"
    @@lots.each {|lot|
      assert @@sv.merge_lot_family(lot)
      assert_equal 0, @@sv.lot_cleanup(lot, opNo: @@opNo_fb)
    }
    get_inhibit_parameters(@@opNo_fb, @@eqp_fb, @@reticle_fb)
    # clear all inhibits
    @@inhibit_entities.keys.each {|k| @@sv.inhibit_cancel(k, @@inhibit_entities[k][0], silent: true)}
    #
    $setup_ok = true
  end

  # genereate lot tests (test1xx, except fixtures)
  SiView::MM::InhibitClasses.keys.reject {|k| k.to_s.start_with?('f')}.each_with_index {|k, i|
    # single lot tests
    send(:define_method, "test#{i + 101}_fb_singlelot_#{k}") {
      $setup_ok = false
      check_inhibit_exception(k, @@eqp_fb, @@opNo_fb)
      $setup_ok = true
    }
  }

  def test181_fixture
    skip 'skipped fixture tests' unless @@test_fixtures
    assert_equal 0, @@sv.eqp_cleanup(@@eqp_fixture), 'error cleaning up eqp'
    check_inhibit_exception(:fixture, @@eqp_fixture, @@opNo_fixture)
  end

  def test182_fixture_eqp
    skip 'skipped fixture tests' unless @@test_fixtures
    assert_equal 0, @@sv.eqp_cleanup(@@eqp_fixture), 'error cleaning up eqp'
    check_inhibit_exception(:fixture_eqp, @@eqp_fixture, @@opNo_fixture)
  end

  def test183_fg
    skip 'skipped fixture tests' unless @@test_fixtures
    assert_equal 0, @@sv.eqp_cleanup(@@eqp_fixture), 'error cleaning up eqp'
    check_inhibit_exception(:fg, @@eqp_fixture, @@opNo_fixture)
  end

  def test184_fg_eqp
    skip 'skipped fixture tests' unless @@test_fixtures
    assert_equal 0, @@sv.eqp_cleanup(@@eqp_fixture), 'error cleaning up eqp'
    check_inhibit_exception(:fg_eqp, @@eqp_fixture, @@opNo_fixture)
  end

  def test191_inhibit_all
    # get_inhibit_parameters(@@opNo_fb, @@eqp_fb, @@reticle_fb)
    lot = @@lots.first
    assert_equal 0, @@sv.lot_cleanup(lot, opNo: @@opNo)
    # cancel all inhibits
    @@inhibit_entities.keys.each {|k| assert_equal 0, @@sv.inhibit_cancel(k, @@inhibit_entities[k][0], silent: true)}
    #
    # set inhibits and exceptions
    inhs = {}
    inh_chamber = nil
    iclasses = @@inhibit_entities.reject {|k, v| k.to_s.start_with?('f')}
    iclasses.each_pair {|iclass, value|
      # set inhibit
      objectids, attrib = value
      assert inh = @@sv.inhibit_entity(iclass, objectids, attrib: attrib)
      inhs[iclass] = inh
      # store eqp_chamber inhibit for later test
      inh_chamber = inh if iclass == :eqp_chamber
      # register exception
      assert_equal 0, @@sv.inhibit_exception_register(lot, inh), 'error registering inhibit exception'
    }
    #
    # test with eqp_chamber to verify inhibit and chamber availability
    verify_process(inh_chamber, lot, @@eqp, @@opNo, true)
    #
    # verify all exceptions are gone
    inhs.each_pair {|iclass, inh|
      assert_empty @@sv.inhibit_exception_list(lot, inh), "wrong exception for #{iclass.inspect}"
    }
  end

  # MSR 789357, no fixtures
  def test192_inhibit_exc_cancel
    lot = @@lots.first
    assert_equal 0, @@sv.lot_cleanup(lot)
    # cancel all inhibits
    @@inhibit_entities.keys.each {|k| assert_equal 0, @@sv.inhibit_cancel(k, @@inhibit_entities[k][0], silent: true)}
    #
    # set inhibits with some exceptions
    inhs = []
    exceptions = {}
    SiView::MM::InhibitClasses.keys.reject {|k| k.to_s.start_with?('f')}.each_with_index {|iclass, i|
      iclasses = iclass
      objectids, attrib = @@inhibit_entities[iclass]
      refute_nil objectids, "unknown inhibit class #{iclass.inspect}"
      assert inh = @@sv.inhibit_entity(iclass, objectids, attrib: attrib), "error inhibiting entity #{iclass}"
      inhs << inh
      if i.even?
        # exception every 2nd class
        assert_equal 0, @@sv.inhibit_exception_register(lot, inh), "error registering #{iclass} exception"
        exceptions[iclass] = inh
      end
    }
    # cancel all inhibits at once
    assert_equal 0, @@sv.inhibit_cancel(nil, nil, inhibits: inhs), 'failed to cancel inhibits'
    # verify all exceptions are gone
    exceptions.each_pair {|iclass, inh|
      assert_empty @@sv.inhibit_exception_list(lot, inh), "wrong exception for #{iclass.inspect}"
    }
  end

  # generate multilot tests (test2xx, except fixtures)
  SiView::MM::InhibitClasses.keys.reject {|k| k.to_s.start_with?('f')}.each_with_index {|k, i|
    send(:define_method, "test#{i + 201}_fb_multilot_#{k}") {
      check_inhibit_exception_mlots(k, @@eqp, @@opNo)
    }
  }

  def test400_setup_ib
    $setup_ok = false
    #
    assert_equal 0, @@sv.eqp_cleanup(@@eqp_ib), "failed to cleanup #{@@eqp_ib}"
    @@lots.each {|lot|
      assert @@sv.merge_lot_family(lot)
      assert_equal 0, @@sv.lot_cleanup(lot, opNo: @@opNo_ib)
    }
    get_inhibit_parameters(@@opNo_ib, @@eqp_ib, @@reticle_ib)
    @@inhibit_entities.keys.each {|k| assert_equal 0, @@sv.inhibit_cancel(k, @@inhibit_entities[k][0], silent: true)}
    #
    $setup_ok = true
  end

  # generate internal buffer tests (test4xx, single lot, except fixtures)
  SiView::MM::InhibitClasses.reject {|k| k.to_s.start_with?('f')}.keys.each_with_index {|k, i|
    send(:define_method, "test#{i+401}_ib_#{k}") {
      check_inhibit_exception(k, @@eqp_ib, @@opNo_ib)
    }
  }


  # aux methods

  def get_inhibit_parameters(opNo, eqp, reticle)
    # cancel current entities' inhibits
    @@inhibit_entities.keys.each {|k| @@sv.inhibit_cancel(k, @@inhibit_entities[k][0], silent: true)}
    #
    lot = @@lots[0]
    @@opNo = opNo
    @@eqp = eqp
    @@reticle = reticle
    # get inbit entity values
    eqpi = @@sv.eqp_info(@@eqp)
    @@chamber = eqpi.chambers[0].chamber
    li = @@sv.lot_info(lot)
    @@productgroup = li.productgroup
    @@technology = li.technology
    @@customer = li.customer
    @@stage = li.stage
    @@module = @@sv.AMD_modulepd(lot)
    @@op = li.op
    assert ui = @@sv.lot_upcoming_info(lot, opNo, eqp), "lot #{lot} is no candicate for #{@@eqp}"
    @@lrecipe = ui.logicalRecipeID.identifier
    @@mrecipe = ui.machineRecipeID.identifier
    assert r = @@sv.eqp_info_raw(@@eqp).strEquipmentAdditionalReticleAttribute.strStoredReticles.find {|r|
      r.reticleID.identifier == @@reticle
    }, "reticle #{@@reticle} not loaded at #{@@eqp}"
    reticle_group = r.reticleGroupID.identifier
    if @@test_fixtures
      # f = @@sv.fixture_list(eqp: @@eqp_fixture, fixtureinfo: true).find {|f| f.fid == @@fixture}
      # assert f, "fixture #{@@fixture} not loaded at #{@@eqp_fixture}"
      # fixture_group = f.group
      assert fixture_group = @@sv.fixture_list(eqp: @@fixture_eqp).find {|e|
        e.fixtureStatusInfo.fixtureID.identifier == @@fixture
      }.fixtureGroupID.identifier
    end
    #
    @@inhibit_entities = {}
    SiView::MM::InhibitClasses.keys.each {|iclass|
      attrib = []
      objectids = SiView::MM::InhibitClasses[iclass].collect {|e|
        case e
        when 'Chamber'; attrib << @@chamber; @@eqp
        when 'Operation'; attrib << @@opNo; @@svtest.route
        when 'Equipment'; attrib << ''; iclass.to_s.start_with?('f') ? @@eqp_fixture : @@eqp
        when 'Fixture'; attrib << ''; @@fixture
        when 'Fixture Group'; attrib << ''; fixture_group
        when 'Logical Recipe'; attrib << ''; @@lrecipe
        when 'Machine Recipe'; attrib << ''; @@mrecipe
        when 'Module Process Definition'; attrib << ''; @@module
        when 'Route'; attrib << '*'; @@svtest.route
        when 'Process Definition'; attrib << ''; @@op
        when 'Product Specification'; attrib << ''; @@svtest.product
        when 'Product Group'; attrib << ''; @@productgroup
        when 'Reticle'; attrib << ''; @@reticle
        when 'Reticle Group'; attrib << ''; reticle_group
        when 'Stage'; attrib << ''; @@stage
        when 'Technology'; attrib << ''; @@technology
        when 'Customer'; attrib << ''; @@customer
        else
          flunk "unknown class #{e}"
        end
      }
      @@inhibit_entities[iclass] = [objectids, attrib]
    }
  end

  def check_inhibit_exception_mlots(iclass, eqp, opNo)
    $log.info "---- check_inhibit_exception_mlots #{iclass.inspect})"
    # cleanup lots
    @@lots.each {|lot| assert_equal 0, @@sv.lot_cleanup(lot, opNo: opNo)}
    lot1 = @@lots.first
     # multi lot case, both lots are in the same carrier
    assert lot2 = @@sv.lot_split(lot1, 1), 'error splitting lot'
    # cancel all inhibits
    @@inhibit_entities.keys.each {|k| assert_equal 0, @@sv.inhibit_cancel(k, @@inhibit_entities[k][0], silent: true)}
    #
    # set inhibit
    $log.info "-- inhibit set -> no processing"
    objectids, attrib = @@inhibit_entities[iclass]
    refute_nil objectids, "unknown inhibit class #{iclass.inspect}"
    assert inh = @@sv.inhibit_entity(iclass, objectids, attrib: attrib), 'error registering inhibit'
    verify_process(inh, [lot1, lot2], eqp, opNo, false)
    #
    # set exception for lot1
    $log.info "-- inhibit and exception for lot1 set -> no processing"
    assert_equal 1, @@sv.inhibit_list(iclass, objectids).count, 'inhibit lost'
    assert_equal 0, @@sv.inhibit_exception_register(lot1, inh), 'error registering inhibit exception'
    verify_process(inh, [lot1, lot2], eqp, opNo, false)
    #
    # set exception for lot2
    $log.info "-- inhibit and exception for lot2 set -> processing"
    assert_equal 0, @@sv.inhibit_exception_register(lot2, inh), 'error registering inhibit exception'
    verify_process(inh, [lot1, lot2], eqp, opNo, true)
    $log.info "-- exception consumed -> no processing"
    verify_process(inh, [lot1, lot2], eqp, opNo, false)
    #
    # cancel inhibit to clean up
    @@sv.inhibit_cancel(iclass, objectids)
    @@sv.lot_merge(lot1, lot2)
  end

  def check_inhibit_exception(iclass, eqp, opNo)
    $log.info "---- check_inhibit_exception #{iclass.inspect}"
    # cleanup lots
    @@lots.each {|lot| assert_equal 0, @@sv.lot_cleanup(lot, opNo: opNo)}
    lot1, lot2 = @@lots
    # cancel all inhibits
    @@inhibit_entities.keys.each {|k|assert_equal 0, @@sv.inhibit_cancel(k, @@inhibit_entities[k][0], silent: true)}
    #
    # set inhibit
    $log.info "-- inhibit set -> no processing"
    objectids, attrib = @@inhibit_entities[iclass]
    refute_nil objectids, "unknown inhibit class #{iclass.inspect}"
    assert inh = @@sv.inhibit_entity(iclass, objectids, attrib: attrib), 'error registering inhibit'
    verify_process(inh, lot1, eqp, opNo, false)
    #
    # set exception
    $log.info "-- inhibit and exception set -> processing"
    assert_equal 0, @@sv.inhibit_exception_register(lot1, inh), 'error registering inhibit exception'
    verify_process(inh, lot1, eqp, opNo, true)
    $log.info "-- exception consumed -> no processing"
    verify_process(inh, lot1, eqp, opNo, false)
    #
    # set exception and cancel it
    $log.info "-- exception set and canceled -> no processing"
    assert_equal 0, @@sv.inhibit_exception_register(lot2, inh), 'error registering inhibit exception'
    assert_equal 0, @@sv.inhibit_exception_cancel(lot2, inh), 'error canceling inhibit exception'
    verify_process(inh, lot2, eqp, opNo, false)
    #
    # set exception, cancel inhibit set a new inhibit
    $log.info "-- exception set, inhibit canceled and set new -> no processing"
    assert_equal 0, @@sv.inhibit_exception_register(lot1, inh), 'error registering inhibit exception'
    assert_equal 0, @@sv.inhibit_cancel(nil, nil, inhibits: [inh]), 'error canceling inhibit'
    assert inh2 = @@sv.inhibit_entity(iclass, objectids, attrib: attrib), 'error registering inhibit'
    verify_process(inh2, lot1, eqp, opNo, false)
    #
    # cancel inhibit to clean up
    @@sv.inhibit_cancel(iclass, objectids)
  end

  # verify capability to process the lot, lots must be in the same carrier if more than one
  def verify_process(inh, lots, eqp, opNo, should_process)
    lots = [lots] if lots.instance_of?(String)
    $log.info "verify_process #{lots}, #{eqp.inspect}, #{opNo.inspect}, #{should_process}"
    #
    chamber_inhibit = inh && inh.entityInhibitAttributes.entities.find {|e| e.className == 'Chamber'}
    if chamber_inhibit
      @@sv.chamber_status_change(@@eqp, @@chamber, '2NDP')
      assert_equal 0, @@sv.chamber_status_change(@@eqp, @@chamber, '2WPR'), "#{@@eqp} #{@@chamber} status is not 2WPR"
    end
    sleep 5 if $env.include?('f8stag')   # =~ /f8stag/ #workaround!
    nexc = lots.inject(0) {|s, lot| s + @@sv.inhibit_exception_list(lot, inh).count}
    expected_result = inh.nil? || (nexc >= lots.count)
    $log.info "  #{expected_result}, #{should_process}, #{nexc}"
    assert_equal should_process, expected_result, 'wrong inhibit or inhibit exception'
    lots.each {|lot| assert_equal 0, @@sv.lot_opelocate(lot, opNo)}
    # try to reserve and load
    loaded = @@sv.claim_process_prepare(lots, eqp: eqp)
    unless chamber_inhibit
      assert_equal !!loaded, should_process, "wrong reserving or carrier loading for lot #{lots}"
      return true unless should_process
    end
    #
    # opestart (should_process or chamber_inhibit)
    cj, cjinfo, eqpiraw, pstatus, mode = loaded
    carriers = @@sv.lots_info(lots).collect {|l| l.carrier}.uniq
    assert cj = @@sv.eqp_opestart(eqp, carriers), 'opestart failed'
    if chamber_inhibit
      assert_equal should_process, @@sv.AMD_chambers_availability(cj).inject(true) {|ok, lotca|
        ok && lotca.chamberAvailability.find {|ca| ca.AMD_ChamberName == @@chamber}.AMD_ChamberStatus == '1'
      }, 'wrong chamber availability'
      unless should_process
        @@sv.eqp_opestart_cancel(eqp, cj)
        carriers.each {|carrier| @@sv.eqp_unload(eqpiraw.equipmentID, pstatus.portID, carrier)}
        $log.info "-- as expected lot #{lots} cannot be processed"
        return true
      end
    else
      curexc = lots.inject(0) {|s, lot| s + @@sv.inhibit_exception_list(lot, inh).count}
      assert_equal nexc, curexc, 'exception not found after opestart'
    end
    #
    # opestart cancel, exceptions are restored even for single
    assert_equal 0, @@sv.eqp_opestart_cancel(eqpiraw.equipmentID, cj), 'error canceling cj'
    curexc = lots.inject(0) {|s, lot| s + @@sv.inhibit_exception_list(lot, inh).count}
    assert_equal nexc, curexc, 'exception not found after opestart cancel'
    #
    # opestart again, must succeed regardless of chamber_inhibit
    assert cj = @@sv.eqp_opestart(eqpiraw.equipmentID, carriers), 'opestart failed'
    if chamber_inhibit
      assert @@sv.AMD_chambers_availability(cj).inject(true) {|ok, lotca|
        ok && lotca.chamberAvailability.find {|ca| ca.AMD_ChamberName == @@chamber}.AMD_ChamberStatus == '1'
      }, 'chamber is not available'
    else
      curexc = lots.inject(0) {|s, lot| s + @@sv.inhibit_exception_list(lot, inh).count}
      assert_equal nexc, curexc, 'exception not found after opestart'
    end
    #
    # opecomp, exceptions go away
    @@sv.eqp_data_speccheck(eqpiraw.equipmentID, cj)
    #sleep 1
    assert_equal 0, @@sv.eqp_opecomp(eqpiraw.equipmentID, cj), 'opecomp failed'
    curexc = lots.inject(0) {|s, lot| s + @@sv.inhibit_exception_list(lot, inh).count}
    if @@test_single
      assert chamber_inhibit || curexc.zero?, 'exception still registered after opecomp'
    else
      assert_equal nexc, curexc, 'exception not found after opecomp'
    end
    carriers.each {|carrier| @@sv.eqp_unload(eqpiraw.equipmentID, pstatus.portID, carrier)}
  end

end
