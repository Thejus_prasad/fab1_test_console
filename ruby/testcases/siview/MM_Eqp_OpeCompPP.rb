=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: Daniel Steger     2013-03-25

History:
  2015-10-26 sfrieske, rewritten, added test for MSR713567
  2015-11-02 dsteger,  removed dependency on EI check
  2015-11-04 sfrieske, leveraging claim_process_lot with a block to simplify code
  2017-09-13 sfrieske, fixed use of user_parameter_change
  2019-12-09 sfrieske, minor cleanup, renamed from Test460_OperationComplete
=end

require 'SiViewTestCase'


# Test that operation complete is supported while lot is in post processing, incl. MSRs 544211 and 713567
class MM_Eqp_OpeCompPP < SiViewTestCase
  @@eqp = 'UTF001'
  @@opNo = '1000.600'    # with Pre-1 script, triggered by lot hold release


  def self.after_all_passed
    @@sv.delete_lot_family(@@lot)
  end


  def setup
    return unless self.class.class_variable_defined?(:@@lot)
    # cleanup lots and eqp
    assert_equal 0, @@sv.eqp_cleanup(@@eqp)
    assert_equal 0, @@sv.user_parameter_delete('Lot', @@lot2, 'UTSkip', check: true), 'error deleting script parameter'
    assert_equal 0, @@sv.user_parameter_delete('Lot', @@lot2, 'UTSleep', check: true), 'error deleting script parameter'
    assert_equal 0, @@sv.lot_cleanup(@@lot, opNo: @@opNo)
    assert_equal 0, @@sv.lot_cleanup(@@lot2, opNo: @@opNo)
    # set script parameters again (must not be set during lot cleanup!)
    assert_equal 0, @@sv.user_parameter_change('Lot', @@lot2, 'UTSkip', 'YES'), 'error setting script parameter'
    assert_equal 0, @@sv.user_parameter_change('Lot', @@lot2, 'UTSleep', '15'), 'error setting script parameter'
  end

  def teardown
    # ensure thread is dead if tests passed
    @@thread.join if self.class.class_variable_defined?(:@@thread)
  end


  def test00_setup
    $setup_ok = false
    #
    assert @@lot = @@svtest.new_lot
    assert @@lot2 = @@sv.lot_split(@@lot, 1), "error splitting lot #{@@lot}"
    #
    $setup_ok = true
  end

  def test01_opecomplete
    assert @@sv.claim_process_lot(@@lot, eqp: @@eqp) {pp_carrier}
  end

  def test02_forceopecomplete
    assert @@sv.claim_process_lot(@@lot, eqp: @@eqp, running_hold: true) {pp_carrier}
  end

  def test03_opestartcancel
    assert @@sv.claim_process_lot(@@lot, eqp: @@eqp, opestart_cancel: true) {pp_carrier}
  end


  def pp_carrier
    # trigger Pre1 script for lot2 by hold release
    assert_equal 0, @@sv.lot_hold(@@lot2, nil), 'failed to set hold'
    @@thread = Thread.new {@@sv.lot_hold_release(@@lot2, nil)}
    # check post processing flags
    sleep 5
    li = @@sv.lot_info(@@lot2)
    assert li.pp_lot, 'lot should be in post processing'
    assert li.pp_carrier, 'carrier should be in post processing'
  end

end
