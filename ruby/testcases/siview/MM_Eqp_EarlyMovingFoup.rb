=begin 
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.
    
Author: Steffen Steidten, 2012-01-13

History:
  2015-05-26 ssteidte, cleaned up and verified with R15
  2015-09-18 dsteger,  extended coverage and added partial operation complete
  2018-03-16 sfrieske, cleaned up calls to claim_process_lot
  2018-05-23 sfrieske, added tests for MSR 1345414
  2019-12-05 sfrieske, minor cleanup, renamed from Test_EarlyMovingFoup
  2020-02-21 sfrieske, adapted for CS_SP_CHECK_EARLYMOVINGFOUP_ON_UNLOAD OFF (C7.14)
=end

require 'SiViewTestCase'


# Test Early Foup Removal, MSRs 240489, 494105, 1345414; todo: MSR72530 Stockin
class MM_Eqp_EarlyMovingFoup < SiViewTestCase
  @@eqp_fb = 'UTF002'
  @@eqp_ib = 'UTI002'
  @@opNo_fb = '1000.500'
  @@opNo_ib = '1000.200'
  @@partial_complete = false
  
  @@testlots = []
  

  def self.after_all_passed
    @@testlots.each {|lot| @@sv.delete_lot_family(lot)}
  end


  def test00_setup
    $setup_ok = false
    #
    # OFF: always early moving foup
    assert_equal 'OFF', @@sv.environment_variable[1]['CS_SP_CHECK_EARLYMOVINGFOUP_ON_UNLOAD'], 'wrong SiView env var'
    #
    [@@eqp_fb, @@eqp_ib].each {|eqp|
      assert @@svtest.cleanup_eqp(eqp)
      assert @@sm.update_udata(:eqp, eqp, {'EarlyMovingFOUP'=>''}) if @@sv.user_data(:eqp, eqp)['EarlyMovingFOUP']
      assert @@sv.eqp_info(eqp).pjctrl, "PJ Level Control not enabled for #{eqp}" if @@partial_complete
    }
    #
    # create test lots
    assert @@testlots = @@svtest.new_lots(3), 'error creating test lots'
    @@carriers = @@sv.lots_info(@@testlots).collect {|li| li.carrier}
    #
    $setup_ok = true
  end


  def test111_fb_auto1
    # Udata or env var setting shouldn't matter in Fab1/8
    run_scenario(@@eqp_fb, @@opNo_fb, 'Auto-1', @@carriers.take(1), false, :ope_comp)
    run_scenario(@@eqp_fb, @@opNo_fb, 'Auto-1', @@carriers.take(1), false, :force_comp)
    if @@partial_complete   # MES-2448 and MES-2451
      run_scenario(@@eqp_fb, @@opNo_fb, 'Auto-1', @@carriers.take(1), false, :partial_comp)
    end
  end

  def test112_fb_auto3
    # Udata or env var setting shouldn't matter in Fab1/8
    run_scenario(@@eqp_fb, @@opNo_fb, 'Auto-3', @@carriers.take(1), false, :ope_comp)
    run_scenario(@@eqp_fb, @@opNo_fb, 'Auto-3', @@carriers.take(1), false, :force_comp)
    if @@partial_complete   # MES-2448 and MES-2451
      run_scenario(@@eqp_fb, @@opNo_fb, 'Auto-3', @@carriers.take(1), false, :partial_comp)
    end
  end
  
  def test151_fb_cancel_auto1
    run_scenario_cancel(@@eqp_fb, @@opNo_fb, 'Auto-1', @@carriers.take(1), true)
    run_scenario_cancel(@@eqp_fb, @@opNo_fb, 'Auto-1', @@carriers.take(1), false)
  end
  
  def test152_fb_cancel_auto2   # MSR 1345414 (C7.9)
    run_scenario_cancel(@@eqp_fb, @@opNo_fb, 'Auto-2', @@carriers.take(1), true)
    run_scenario_cancel(@@eqp_fb, @@opNo_fb, 'Auto-2', @@carriers.take(1), false)
  end

  def test153_fb_cancel_auto3   # MSR 1345414 (C7.9)
    run_scenario_cancel(@@eqp_fb, @@opNo_fb, 'Auto-3', @@carriers.take(1), true)
    run_scenario_cancel(@@eqp_fb, @@opNo_fb, 'Auto-3', @@carriers.take(1), false)
  end

  def test154_fb_cancel_semi1
    run_scenario_cancel(@@eqp_fb, @@opNo_fb, 'Semi-1', @@carriers.take(1), true)
    run_scenario_cancel(@@eqp_fb, @@opNo_fb, 'Semi-1', @@carriers.take(1), false)
  end

  def test155_fb_cancel_semi2   # MSR 1345414 (C7.9)
    run_scenario_cancel(@@eqp_fb, @@opNo_fb, 'Semi-2', @@carriers.take(1), true)
    run_scenario_cancel(@@eqp_fb, @@opNo_fb, 'Semi-2', @@carriers.take(1), false)
  end


  def test211_ib_auto1
    run_scenario(@@eqp_ib, @@opNo_ib, 'Auto-1', @@carriers.take(1), true, :ope_comp)
    run_scenario(@@eqp_ib, @@opNo_ib, 'Auto-1', @@carriers.take(1), false, :ope_comp)
    if @@partial_complete
      run_scenario(@@eqp_ib, @@opNo_ib, 'Auto-1', @@carriers, true, :partial_comp)
      run_scenario(@@eqp_ib, @@opNo_ib, 'Auto-1', @@carriers, false, :partial_comp)
    end
  end

  def test212_ib_auto2
    run_scenario(@@eqp_ib, @@opNo_ib, 'Auto-2', @@carriers.take(1), true, :ope_comp)
    run_scenario(@@eqp_ib, @@opNo_ib, 'Auto-2', @@carriers.take(1), false, :ope_comp)
    # no partial_complete
  end

  def test213_ib_auto3
    run_scenario(@@eqp_ib, @@opNo_ib, 'Auto-3', @@carriers.take(1), true, :ope_comp)
    run_scenario(@@eqp_ib, @@opNo_ib, 'Auto-3', @@carriers.take(1), false, :ope_comp)
    if @@partial_complete
      run_scenario(@@eqp_ib, @@opNo_ib, 'Auto-1', @@carriers, true, :partial_comp)
      run_scenario(@@eqp_ib, @@opNo_ib, 'Auto-1', @@carriers, false, :partial_comp)
    end
  end
  
  def test221_ib_batch_auto1
    run_scenario(@@eqp_ib, @@opNo_ib, 'Auto-1', @@carriers, true, :ope_comp)
    run_scenario(@@eqp_ib, @@opNo_ib, 'Auto-1', @@carriers, false, :ope_comp)
    run_scenario(@@eqp_ib, @@opNo_ib, 'Auto-1', @@carriers, true, :force_comp)
    run_scenario(@@eqp_ib, @@opNo_ib, 'Auto-1', @@carriers, false, :force_comp)
  end

  def test222_ib_batch_auto2
    run_scenario(@@eqp_ib, @@opNo_ib, 'Auto-2', @@carriers, true, :ope_comp)
    run_scenario(@@eqp_ib, @@opNo_ib, 'Auto-2', @@carriers, false, :ope_comp)
    # no force_comp
  end

  def test223_ib_batch_auto3
    run_scenario(@@eqp_ib, @@opNo_ib, 'Auto-3', @@carriers, true, :ope_comp)
    run_scenario(@@eqp_ib, @@opNo_ib, 'Auto-3', @@carriers, false, :ope_comp)
    run_scenario(@@eqp_ib, @@opNo_ib, 'Auto-3', @@carriers, true, :force_comp)
    run_scenario(@@eqp_ib, @@opNo_ib, 'Auto-3', @@carriers, false, :force_comp)
  end

  def test251_ib_batch_cancel_auto1
    run_scenario_cancel(@@eqp_ib, @@opNo_ib, 'Auto-1', @@carriers, true)
    run_scenario_cancel(@@eqp_ib, @@opNo_ib, 'Auto-1', @@carriers, false)
  end

  def test252_ib_batch_cancel_auto2
    run_scenario_cancel(@@eqp_ib, @@opNo_ib, 'Auto-2', @@carriers, true)
    run_scenario_cancel(@@eqp_ib, @@opNo_ib, 'Auto-2', @@carriers, false)
  end

  def test253_ib_batch_cancel_auto3
    run_scenario_cancel(@@eqp_ib, @@opNo_ib, 'Auto-3', @@carriers, true)
    run_scenario_cancel(@@eqp_ib, @@opNo_ib, 'Auto-3', @@carriers, false)
  end

  def test254_ib_batch_cancel_semi1
    run_scenario_cancel(@@eqp_ib, @@opNo_ib, 'Semi-1', @@carriers, true)
    run_scenario_cancel(@@eqp_ib, @@opNo_ib, 'Semi-1', @@carriers, false)
  end

  def test255_ib_batch_cancel_semi2
    run_scenario_cancel(@@eqp_ib, @@opNo_ib, 'Semi-2', @@carriers, true)
    run_scenario_cancel(@@eqp_ib, @@opNo_ib, 'Semi-2', @@carriers, false)
  end

  def test261_ib_unloadrsv_auto1
    run_scenario_unloadrsv(@@eqp_ib, @@opNo_ib, 'Auto-1', @@carriers)
  end

  def test262_ib_unloadrsv_auto3
    run_scenario_unloadrsv(@@eqp_ib, @@opNo_ib, 'Auto-1', @@carriers)
  end


  # aux methods

  def run_scenario(eqp, opNo, mode, carriers, earlyfoup, scenario)
    $log.info "run_scenario #{eqp.inspect}, #{opNo.inspect}, #{mode.inspect}, #{carriers}, #{earlyfoup}, #{scenario.inspect}"
    lots = carriers.collect {|c| @@sv.carrier_status(c).lots}.flatten
    # setup
    assert_equal 0, @@sv.eqp_cleanup(eqp, mode: mode)
    lots.each {|lot| assert_equal 0, @@sv.lot_cleanup(lot, opNo: opNo), 'error preparing lot'}
    carriers.each {|carrier| assert @@svtest.cleanup_carrier(carrier)}
    # process lot with optional Early Foup Removal (noopecomp: true)
    walias1, walias2 = (1..25).map {|i| '%02d' % i}.partition {|s| s < '11'}
    assert cj = @@sv.claim_process_lot(lots, eqp: eqp, cycle_port: true, mode: mode, noopecomp: earlyfoup,
      running_hold: scenario == :force_comp, partial_comp: scenario == :partial_comp, opecomp_hold: walias1, opecancel_hold: walias2
    ), 'error processing lot'
    #
    lots.each {|lot|
      li = @@sv.lot_info(lot)
      assert_equal 'EO', li.xfer_status, 'wrong lot xfer status'
      assert_equal scenario == :force_comp ? 'ONHOLD' : 'Processing', li.status, 'wrong lot status' if earlyfoup
    }
    # carrier stockin
    carriers.each {|carrier|
      assert_equal 0, @@sv.carrier_xfer_status_change(carrier, 'SI', @@svtest.stocker), 'stockin failed'
    }
    # opecomp if earlyfoup
    if earlyfoup
      assert_equal 0, @@sv.eqp_tempdata(eqp, @@sv.eqp_info(eqp).ports.first.pg, cj)
      case scenario
      when :ope_comp
        assert_equal 0, @@sv.eqp_opecomp(eqp, cj), 'opecomp failed'
      when :force_comp
        assert_equal 0, @@sv.eqp_opecomp(eqp, cj, force: true), 'force opecomp failed'
      when :partial_comp                
        assert plots = @@sv.eqp_partial_opecomp(eqp, cj, opecomp_hold: walias1, opecancel_hold: walias2), 'partial opecomp failed'
        assert plots.count > lots.count, 'more lots expected'
        lots = plots
      else
        raise "unknown scenario: #{scenario}"
      end      
    end
    status = scenario == :ope_comp ? 'Waiting' : 'ONHOLD'
    lots.each {|lot| 
      assert_equal status, @@sv.lot_info(lot).status, 'wrong lot status'
      @@sv.merge_lot_family(lot, nosort: true) if scenario == :partial_comp
    }
  end

  def run_scenario_cancel(eqp, opNo, mode, carriers, earlyfoup)
    $log.info "run_scenario_cancel #{mode.inspect}, #{carriers}, #{earlyfoup}"
    lots = carriers.collect {|c| @@sv.carrier_status(c).lots}.flatten
    # setup
    assert_equal 0, @@sv.eqp_cleanup(eqp)
    lots.each {|lot| assert_equal 0, @@sv.lot_cleanup(lot, opNo: opNo, stocker: @@svtest.stocker), "error preparing lot #{lot}"}
    # start processing lots, opestart cancel if not earlyfoup, unload carriers
    assert cj = @@sv.claim_process_lot(lots, eqp: eqp, mode: mode, noopecomp: true, opestart_cancel: !earlyfoup)
    carriers.each {|carrier|
      assert_equal 0, @@sv.carrier_xfer_status_change(carrier, 'SI', @@svtest.stocker), 'stockin failed'
    }
    # opestart cancel if earlyfoup, fails except in Auto-1 and Semi-1 w/o MSR 1345414 (C7.9)
    assert_equal 0, @@sv.eqp_opestart_cancel(eqp, cj), 'opestart cancel failed' if earlyfoup
    lots.each {|lot|
      li = @@sv.lot_info(lot)
      assert_equal 'SI', li.xfer_status, 'wrong lot xfer status'
      assert_equal 'Waiting', li.status, 'wrong lot status'
    }
  end

  def run_scenario_unloadrsv(eqp, opNo, mode, carriers)
    assert_equal 0, @@sv.eqp_cleanup(eqp, mode: mode)
    lots = carriers.collect {|c| @@sv.carrier_status(c).lots}.flatten
    lots.each {|lot| assert_equal 0, @@sv.lot_cleanup(lot, opNo: opNo), 'error preparing lot'}
    carriers.each {|carrier| assert @@svtest.cleanup_carrier(carrier)}
    #
    assert cj = @@sv.claim_process_lot(lots, eqp: eqp, mode: mode, cycle_port: true, noopecomp: true, nounload: true), 'error processing lot'
    #
    ports = @@sv.eqp_info(eqp).ports.collect {|p| p.port}
    carriers.each_with_index {|carrier, i|
      port = ports[i % ports.size]
      assert_equal 0, @@sv.eqp_carrierfromib_reserve(eqp, port, carrier), 'unload reservation failed'
      assert_equal 0, @@sv.eqp_carrierfromib_reserve_cancel(eqp, carrier), 'unload reservation cancel failed'
      assert_equal 0, @@sv.eqp_carrierfromib_reserve(eqp, port, carrier), 'unload reservation failed'
      assert_equal 0, @@sv.eqp_carrierfromib(eqp, port, carrier), 'move to port failed'
      assert_equal 0, @@sv.eqp_port_status_change(eqp, port, 'UnloadReq')
      assert_equal 0, @@sv.eqp_unload(eqp, port, carrier, ib: true, autoib: false), 'unload failed'
      assert_equal 0, @@sv.eqp_port_status_change(eqp, port, 'UnloadComp')
      assert_equal 0, @@sv.eqp_port_status_change(eqp, port, 'LoadReq')
      assert_equal 0, @@sv.carrier_xfer_status_change(carrier, '', @@svtest.stocker, manualin: true), 'stockin failed'
    }
    assert_equal 0, @@sv.eqp_opecomp(eqp, cj), 'operation complete failed'
  end
   
end
