=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: Steffen Steidten, 2012-06-2

History:
  2015-05-28 adarwais, cleaned up and verified test cases with R15
  2017-10-04 sfrieske, clean up eqp before testing
  2019-12-09 sfrieske, minor cleanup, renamed from Test210_AMD_PM_Tx
=end

require 'RubyTestCase'

# Test that the SAP support transactions are accepted by SiView
# note: since C7.9 (MSR 1198714) the tests would fail with CS_SP_ENABLE_SAPPM_REPORTING=ON
class MM_Eqp_PMEvents < RubyTestCase
  @@eqp = 'CVD400'
  @@chamber = 'CHA'

  def test00_setup
    $setup_ok = false
    #
    assert_equal 0, @@sv.eqp_cleanup(@@eqp)
    #
    $setup_ok = true
  end

  def test_tx1
    assert_equal 0, @@sv.eqp_pm_event(@@eqp, state: '2NDP')
    assert_equal 0, @@sv.eqp_pm_event(@@eqp, state: '2WPR')
  end

  def test_tx2
    assert_equal 0, @@sv.chamber_pm_event(@@eqp, @@chamber, state: '2NDP')
    assert_equal 0, @@sv.chamber_pm_event(@@eqp, @@chamber, state: '2WPR')
  end

end
