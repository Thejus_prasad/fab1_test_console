=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author:  Ulrich Koerner, Steffen Steidten, 2011-08-16

History:
  2014-11-24 ssteidte, clean up, added tests for MSR 480894
  2015-05-19 ssteidte, added PSM setup to @@svtest's default route, verified tests with R15
  2015-08-07 dsteger,  skip merge and back-to-back functionality for Fab7
  2015-11-24 sfrieske, merge with junit_testing, added test61 and 62 for MSR 874664, use wafer slots instead of aliases
  2016-06-10 sfrieske, add TC15 for reset after branch delete
  2016-12-13 dsteger,  added test for MSR 758546
  2017-01-04 sfriesek, minor cleanup
  2017-05-08 sfrieske, added test18 for MSR 1170014
  2018-05-14 sfrieske, added test23 for MES-3689
  2019-06-13 sfrieske, added test91 for MSR 1457417
  2021-02-05 sfrieske, minor cleanup
  2021-08-30 sfrieske, moved siviewmmdb.rb to siview/mmdb.rb
  2021-12-16 sfrieske, fiexed use of mainpd_add_branch in TC15
=end

require 'siview/mmdb'
require 'SiViewTestCase'


# Test planned split/merge experiments, MSRs 495041, 98983, 480894, 874664, 58123
class MM_Lot_PSM < SiViewTestCase
  @@nonprobank = 'UT-NONPROD'
  # PSM on main route, format: branch_route split_opno merge_opno
  @@eroute1 = 'e6795-RECIPE-CHECK-A1.01'
  @@split_opno1 = '1000.400'
  @@join_opno1 = '1000.600'
  # normal PSM, but route may not be attached in SM (for route delete tests only)
  @@erouteSM = 'UTBR002.01'
  @@split_opnoSM = '1000.400'
  @@join_opnoSM = '1000.500'
  # PSM nested into PSM1
  @@eroute2 = 'e6889-SAMPLING-A2.01'
  @@split_opno2 = '1000.500'
  @@join_opno2 = '1000.600'
  # PSM following PSM1 back-to-back
  @@eroute3 = 'e6889-SAMPLING-A3.01'
  @@split_opno3 = '1000.600'
  @@join_opno3 = '1000.700'
  # PSM branching from PSM1 (branch-to-branch)
  @@eroute4 = 'eA226-SIVIEW-R10-A1.01'
  @@split_opno4 = '1000.1100'  # on @@eroute1
  @@join_opno4 = '1000.1300'    # on @@eroute1
  # PSM with split = join = merge
  @@eroute5 = 'UTBR001.01'
  @@split_opno5 = '1000.700'
  # PSM branching from PSM1 first operation (branch-to-branch)
  @@eroute6 = 'eA226-SIVIEW-R10-A2.01'
  @@split_opno6 = '1000.1000'
  @@join_opno6 = '1000.1300'
  # wafer aliases for splits
  @@wafersets = [[1, 2, 3, 4], [6, 7, 8, 9], [11, 12, 13, 14], [16, 17, 18, 19], [21, 22, 23, 24]]

  @@testlots = []

  def self.after_all_passed
    @@testlots.each {|lot| @@sv.delete_lot_family(lot)}
  end


  def test00_setup
    $setup_ok = false
    #
    @@mmdb = SiView::MMDB.new($env)  # for test18
    #
    assert lot = @@svtest.new_lot, 'error creating test lot'
    @@testlots << lot
    @@route = @@svtest.route
    #
    $setup_ok = true
  end

  # simple PSM, incl MSR 480894 (carrier multi-lot/multi-recipe status)
  def test11_psm_simple
    $setup_ok = false
    #
    wafers1 = @@wafersets[0]
    lot = @@testlots.first
    psm_and_split(lot, wafers1)
    psm_and_split(lot, wafers1, nil, erf_route: @@eroute3, split_op: @@split_opno3, join_opNo: @@join_opno3, mltype: 'ML-MR')
    #
    $setup_ok = true
  end

  def test12_psm_and_split
    wafers1 = @@wafersets[0]
    wafers2 = @@wafersets[2]
    assert_empty (wafers1 & wafers2), 'wrong choice of wafers'
    lot = @@testlots.first
    psm_and_split(lot, wafers1, wafers2)
  end

  def test13_psm_and_split_overlap
    wafers1 = @@wafersets[0] | @@wafersets[1]
    wafers2 = @@wafersets[1] | @@wafersets[2]
    refute_empty (wafers1 & wafers2), 'wrong choice of wafers'
    lot = @@testlots.first
    psm_and_split(lot, wafers1, wafers2)
  end

  # MSR 58123
  def test14_psm_reset
    wafers1 = @@wafersets[1]
    lot = @@testlots.first
    psm_and_split(lot, wafers1, nil, reset: true)
  end

  def test15_reset_after_delete  # MES-3065
    # prepare branch route
    brinfo = {mainpd: @@erouteSM, joining_opNo: @@join_opnoSM}
    opinfo = @@sm.object_info(:mainpd, @@route, details: true).first[:specific][:operations].find {|o| o.opNo == @@split_opnoSM}
    assert @@sm.mainpd_add_branch(@@svtest.route, @@split_opnoSM, brinfo) unless opinfo.branches.include?(brinfo)
    # prepare new lot (hard to cleanup if this test fails)
    assert lot = @@svtest.new_lot, 'error creating test lot'
    @@testlots << lot
    # attach PSM and execute
    assert_equal 0, @@sv.lot_experiment(lot, 5, @@erouteSM, @@split_opnoSM, @@join_opnoSM)
    assert_equal 0, @@sv.lot_opelocate(lot, @@split_opnoSM)
    # remove 2nd branch route (SM)
    assert @@sm.mainpd_delete_branch(@@svtest.route, @@split_opnoSM, brinfo)
    # try to reset experiment, must fail     either not return 0 or reset the exec flag in "details"
    assert_equal 2874, @@sv.lot_experiment_reset(lot, @@split_opnoSM), 'wrong rc'
    assert @@sv.lot_experiment_list(lot).first.execFlag, 'PSM execFlag has been reset'
  end

  # MSR 1170014/ MES-3206
  def test18_psm_claim_time
    # simple PSM
    lot = @@testlots.first
    assert @@sv.merge_lot_family(lot)
    assert_equal 0, @@sv.lot_cleanup(lot), 'wrong lot status'
    assert_equal 0, @@sv.lot_experiment(lot, 2, @@eroute1, @@split_opno1, @@join_opno1)
    txtime = Time.now
    assert_equal 0, @@sv.lot_opelocate(lot, @@split_opno1)
    lots = @@sv.lot_family(lot)
    assert_equal 2, lots.size, 'PSM failed'
    # history
    $log.info "waiting 90 s for lot history"; sleep 90
    l = lot.split('.').first + '%'
    t = txtime.localtime(@@mmdb.db.dbtimezone)  # for DB2
    res = @@mmdb.db.select('select CLAIM_TIME from FHOPEHS where LOT_ID LIKE ? and CLAIM_TIME > ?', l, t).flatten
    assert_equal 6, res.size, 'wrong number of records'
    assert_equal 1, res.uniq.size, 'CLAIM_TIME is not the same for all records'
  end

  # MSR 98983
  def test19_psm_with_pre1_script
    wafers1 = @@wafersets[0]
    lot = @@testlots.first
    psm_and_split(lot, wafers1, nil, hold: true)
  end

  def test21_nested_psms
    wafers1 = @@wafersets[0]
    wafers2 = @@wafersets[1]
    assert_empty (wafers1 & wafers2), "no suited wafer sets"
    lot = @@testlots.first
    nested_psms(lot, wafers1, wafers2)
  end

  def test22_nested_psms_overlap
    wafers1 = @@wafersets[0] | @@wafersets[1]
    wafers2 = @@wafersets[1] | @@wafersets[2]
    refute_empty (wafers1 & wafers2), 'wrong choice of wafers'
    lot = @@testlots.first
    nested_psms(lot, wafers1, wafers2)
  end

  def test23_execute_try_overlap  # MES-3689, seems to be a Fab7 specific issue
    wafers1 = @@wafersets[0] | @@wafersets[1]
    wafers2 = @@wafersets[1] | @@wafersets[2]
    refute_empty (wafers1 & wafers2), 'wrong choice of wafers'
    lot = @@testlots.first
    assert @@sv.merge_lot_family(lot)
    assert_equal 0, @@sv.lot_cleanup(lot), 'wrong lot status'
    # attach first PSM and execute it
    assert_equal 0, @@sv.lot_experiment(lot, wafers1, @@eroute1, @@split_opno1, @@join_opno1)
    assert_equal 0, @@sv.lot_opelocate(lot, @@split_opno1)
    return
    @@sv.lot_experiment(lot, wafers2, @@eroute1, @@split_opno1, @@join_opno1)
    assert_equal 554, @@sv.rc, 'wrong rc'
  end

  # the 3x testcases do a split and an experiment parallel and directly at the
  # merge operation of the next experiment, the lots arrive in different sequences
  def test31_parent_splitchild_experimentchildren
    wafers1 = @@wafersets[0] | @@wafersets[1]
    wafers2 = @@wafersets[1] | @@wafersets[2] | @@wafersets[3]
    wafers3 = @@wafersets[3] | @@wafersets[4]
    refute_empty (wafers1 & wafers2), 'wrong choice of wafers'
    assert_empty (wafers1 & wafers3), 'wrong choice of wafers'
    refute_empty (wafers2 & wafers3), 'wrong choice of wafers'
    lot = @@testlots.first
    consecutive_psms(lot, wafers1, wafers2, wafers3, 0)
  end

  def test32_parent_experimentchildren_splitchild
    wafers1 = @@wafersets[0] | @@wafersets[1]
    wafers2 = @@wafersets[1] | @@wafersets[2] | @@wafersets[3]
    wafers3 = @@wafersets[3] | @@wafersets[4]
    refute_empty (wafers1 & wafers2), 'wrong choice of wafers'
    assert_empty (wafers1 & wafers3), 'wrong choice of wafers'
    refute_empty (wafers2 & wafers3), 'wrong choice of wafers'
    lot = @@testlots.first
    consecutive_psms(lot, wafers1, wafers2, wafers3, 1)
  end

  def test33_splitchild_parent_experimentchildren
    wafers1 = @@wafersets[0] | @@wafersets[1]
    wafers2 = @@wafersets[1] | @@wafersets[2] | @@wafersets[3]
    wafers3 = @@wafersets[3] | @@wafersets[4]
    refute_empty (wafers1 & wafers2), 'wrong choice of wafers'
    assert_empty (wafers1 & wafers3), 'wrong choice of wafers'
    refute_empty (wafers2 & wafers3), 'wrong choice of wafers'
    lot = @@testlots.first
    consecutive_psms(lot, wafers1, wafers2, wafers3, 2)
  end

  def test34_splitchild_experimentchildren_parent
    wafers1 = @@wafersets[0] | @@wafersets[1]
    wafers2 = @@wafersets[1] | @@wafersets[2] | @@wafersets[3]
    wafers3 = @@wafersets[3] | @@wafersets[4]
    refute_empty (wafers1 & wafers2), 'wrong choice of wafers'
    assert_empty (wafers1 & wafers3), 'wrong choice of wafers'
    refute_empty (wafers2 & wafers3), 'wrong choice of wafers'
    lot = @@testlots.first
    consecutive_psms(lot, wafers1, wafers2, wafers3, 3)
  end

  def test35_experimentchildren_parent_splitchild
    wafers1 = @@wafersets[0] | @@wafersets[1]
    wafers2 = @@wafersets[1] | @@wafersets[2] | @@wafersets[3]
    wafers3 = @@wafersets[3] | @@wafersets[4]
    refute_empty (wafers1 & wafers2), 'wrong choice of wafers'
    assert_empty (wafers1 & wafers3), 'wrong choice of wafers'
    refute_empty (wafers2 & wafers3), 'wrong choice of wafers'
    lot = @@testlots.first
    consecutive_psms(lot, wafers1, wafers2, wafers3, 4)
  end

  def test36_experimentchildren_splitchild_parent
    wafers1 = @@wafersets[0] | @@wafersets[1]
    wafers2 = @@wafersets[1] | @@wafersets[2] | @@wafersets[3]
    wafers3 = @@wafersets[3] | @@wafersets[4]
    refute_empty (wafers1 & wafers2), 'wrong choice of wafers'
    assert_empty (wafers1 & wafers3), 'wrong choice of wafers'
    refute_empty (wafers2 & wafers3), 'wrong choice of wafers'
    lot = @@testlots.first
    consecutive_psms(lot, wafers1, wafers2, wafers3, 5)
  end

  def test37_consecutive_psms
    wafers1 = @@wafersets[0] | @@wafersets[1] | @@wafersets[2] | @@wafersets[3]
    wafers2 = @@wafersets[2] | @@wafersets[3]
    wafers3 = @@wafersets[0]
    refute_empty (wafers2 & wafers1), 'wrong choice of wafers'
    assert_equal wafers3, (wafers1 & wafers3), 'wrong choice of wafers'
    assert_empty (wafers2 & wafers3), 'wrong choice of wafers'
    lot = @@testlots.first
    consecutive_psms(lot, wafers1, wafers2, wafers3, 9, move_child: false)
  end

  def test38_consecutive_psms_b2b
    wafers1 = @@wafersets[0] | @@wafersets[1] | @@wafersets[2] | @@wafersets[3]
    wafers2 = @@wafersets[2] | @@wafersets[3]
    wafers3 = @@wafersets[0]
    refute_empty (wafers2 & wafers1), 'wrong choice of wafers'
    assert_equal wafers3, (wafers1 & wafers3), 'wrong choice of wafers'
    assert_empty (wafers2 & wafers3), 'wrong choice of wafers'
    lot = @@testlots.first
    consecutive_psms(lot, wafers1, wafers2, wafers3, 9, move_child: false, branch_to_branch: true)
  end

  def test39_consecutive_psms_b2b
    wafers1 = @@wafersets[0] | @@wafersets[1] | @@wafersets[2] | @@wafersets[3]
    wafers2 = @@wafersets[2] | @@wafersets[3]
    wafers3 = @@wafersets[3]
    refute_empty (wafers2 & wafers1), 'wrong choice of wafers'
    assert_equal wafers3, (wafers1 & wafers3), 'wrong choice of wafers'
    assert_equal wafers3, (wafers2 & wafers3), 'wrong choice of wafers'
    lot = @@testlots.first
    consecutive_psms(lot, wafers1, wafers2, wafers3, 9, move_child: false, branch_to_branch: true)
  end

  def test41_branch_to_branch
    wafers1 = @@wafersets[3] | @@wafersets[0]
    wafers2 = @@wafersets[3]
    assert_equal wafers2, (wafers1 & wafers2), 'wrong choice of wafers'
    lot = @@testlots.first
    branch_to_branch(lot, wafers1, wafers2)
  end

  def test42_branch_to_branch_at_split
    wafers1 = @@wafersets[4] | @@wafersets[1]
    wafers2 = @@wafersets[4]
    assert_equal wafers2, (wafers1 & wafers2), 'wrong choice of wafers'
    lot = @@testlots.first
    branch_to_branch_at_split(lot, wafers1, wafers2)
  end

  def test43_branch_to_branch_at_split_nomergepoint # MSR758546
    wafers1 = @@wafersets[4] | @@wafersets[1]
    wafers2 = @@wafersets[4]
    assert_equal wafers2, (wafers1 & wafers2), 'wrong choice of wafers'
    lot = @@testlots.first
    branch_to_branch_at_split(lot, wafers1, wafers2, mergepoint: false)
  end

  def test51_multi_psm_none_in_split
    wafers1 = @@wafersets[0]
    wafers2 = @@wafersets[4]
    wafers3 = @@wafersets[1] | @@wafersets[2] | @@wafersets[3]
    assert_empty (wafers1 & wafers2), 'wrong choice of wafers'
    assert_empty (wafers2 & wafers3), 'wrong choice of wafers'
    assert_empty (wafers1 & wafers3), 'wrong choice of wafers'
    lot = @@testlots.first
    multi_psms_and_split(lot, wafers1, wafers2, wafers3)
  end

  def test52_multi_psm_partial_in_split
    wafers1 = @@wafersets[0] | @@wafersets[1]
    wafers2 = @@wafersets[3] | @@wafersets[4]
    wafers3 = @@wafersets[1] | @@wafersets[2] | @@wafersets[3]
    assert_empty (wafers1 & wafers2), 'wrong choice of wafers'
    refute_empty (wafers2 & wafers3), 'wrong choice of wafers'
    refute_empty (wafers1 & wafers3), 'wrong choice of wafers'
    lot = @@testlots.first
    multi_psms_and_split(lot, wafers1, wafers2, wafers3)
  end

  def test53_multi_psm_mixed
    wafers1 = @@wafersets[0] | @@wafersets[1]
    wafers2 = @@wafersets[4]
    wafers3 = @@wafersets[1] | @@wafersets[2] | @@wafersets[3]
    assert_empty (wafers1 & wafers2), 'wrong choice of wafers'
    assert_empty (wafers2 & wafers3), 'wrong choice of wafers'
    refute_empty (wafers1 & wafers3), 'wrong choice of wafers'
    lot = @@testlots.first
    multi_psms_and_split(lot, wafers1, wafers2, wafers3)
  end

  # MSR573881 (since R15)
  def test60_psm_at_nonprobankout
    wafers1 = @@wafersets[0]
    lot = @@testlots.first
    psm_and_split(lot, wafers1, nil, trigger: :nonprobank)
  end

  # MSR874664, merge op same as split (since R15)
  def test61_merge_at_split
    # attach and execute PSM
    lot = @@testlots.first
    assert_equal 0, @@sv.lot_experiment(lot, 5, @@eroute5, @@split_opno5, @@split_opno5)
    assert_equal 0, @@sv.lot_opelocate(lot, @@split_opno5)
    lc = @@sv.lot_family(lot).last
    refute_equal lot, lc, "error splitting lot at PSM split opNo"
    assert_equal 'ONHOLD', @@sv.lot_info(lot).status, "parent should be onhold"
    # locate child lot to merge op (parent is still/already there) and merge
    assert_equal 0, @@sv.lot_opelocate(lc, @@split_opno5, route: @@svtest.route)
    check_automerge(lot, lc)
  end

  def test62_merge_at_split_multi
    lot = @@testlots.first
    assert @@sv.merge_lot_family(lot)
    assert_equal 0, @@sv.lot_cleanup(lot), 'wrong lot status'
    # attach multiple PSMs
    eroutes = Array.new(3, @@eroute5)
    wafers = eroutes.count.times.collect {|i| [i+1]}
    assert_equal 0, @@sv.lot_experiment(lot, wafers, eroutes, @@split_opno5, @@split_opno5), "failed to setup experiment"
    # execute PSM
    assert_equal 0, @@sv.lot_opelocate(lot, @@split_opno5)
    children = @@sv.lot_family(lot) - [lot]
    assert_equal eroutes.count, children.count, "not enough lots created, found #{children.inspect}"
    assert_equal 'ONHOLD', @@sv.lot_info(lot).status, "parent should be onhold"
    # locate child lot to merge op (parent is still/already there) and merge
    children.each {|child|
      assert_equal 0, @@sv.lot_opelocate(child, @@split_opno5), "failed to locate lot #{child}"
      check_automerge(lot, child)
    }
  end

  def test80_psm_overlap_same_wafers
    skip "overlap check is only active in Fab7" unless @@sv.f7
    lot = @@testlots.first
    wafers1 = @@wafersets[1]
    wafers2 = @@wafersets[1][1..-1] + @@wafersets[2]
    assert_equal 0, @@sv.lot_experiment(lot, wafers1, @@eroute1, @@split_opno1, @@join_opno1), "failed to setup experiment 1"
    assert_equal 2973, @@sv.lot_experiment(lot, wafers2, @@eroute2, @@split_opno2, @@join_opno2), "wrong error to setup experiment 2"
    assert @@sv.lot_experiment_delete(lot), "Failed to clear PSM"
    #
    assert_equal 0, @@sv.lot_experiment(lot, wafers1, @@eroute1, @@split_opno1, @@join_opno3), "failed to setup experiment 1"
    assert_equal 2973, @@sv.lot_experiment(lot, wafers2, @@eroute3, @@split_opno3, @@join_opno3), "wrong error to setup experiment 2"
    assert @@sv.lot_experiment_delete(lot), "Failed to clear PSM"
  end

  def test90_psm_check_operation
    lot = @@testlots.first
    @@sv.lot_intermediate_psm(lot, '', '')
    assert_equal 2962, @@sv.rc, 'wrong return code'
    assert_equal "0000792E:A parameter isn't normal. ", @@sv.msg_text, 'wrong message'
  end

  # MSR 1457417
  def test91_branchcancel
    lot = @@testlots.first
    assert_equal 0, @@sv.lot_cleanup(lot)
    # create an experiment and start it
    assert_equal 0, @@sv.lot_experiment(lot, 25, @@eroute1, @@split_opno1, @@join_opno1), 'error attaching experiment'
    assert_equal 0, @@sv.lot_opelocate(lot, @@split_opno1), 'error locating lot'
    assert_equal @@eroute1, @@sv.lot_info(lot).route, 'wrong route'
    # call branch cancel with reset
    assert_equal 0, @@sv.lot_experiment_branchcancel(lot, reset: true), 'wrong branchcancel RC'
    assert_equal @@svtest.route, @@sv.lot_info(lot).route, 'wrong route'
    assert lh = @@sv.lot_hold_list(lot, reason: 'PSMC').first, 'missing lot hold'
    assert lh.memo.include?('PSM was reset'), 'wrong hold claim memo'
    # release hold to re-execute the experiment
    assert_equal 0, @@sv.lot_hold_release(lot, 'PSMC')
    assert_equal @@eroute1, @@sv.lot_info(lot).route, 'wrong route'
    # call branch cancel w/o reset
    assert_equal 0, @@sv.lot_experiment_branchcancel(lot), 'wrong branchcancel RC'
    assert_equal @@svtest.route, @@sv.lot_info(lot).route, 'wrong route'
    assert_empty @@sv.lot_hold_list(lot, reason: 'PSMC'), 'wrong lot hold'
    assert @@sv.lot_experiment_list(lot).first.execFlag, 'PSM execFlag has been reset'
  end


  # aux methods

  # single PSM, incl MSR 480894 (carrier multi-lot/multi-recipe status)
  def psm_and_split(lot, wafers1, wafers2=nil, params={})
    assert @@sv.merge_lot_family(lot)
    assert_equal 0, @@sv.lot_cleanup(lot), 'wrong lot status'
    carrier = @@sv.lot_info(lot).carrier
    assert_equal 'SL-SR', @@sv.carrier_status(carrier).mltype
    if params[:trigger] == :nonprobank
      # bankin lot at split opno to prepare for bankout
      assert_equal 0, @@sv.lot_opelocate(lot, @@split_opno1)
      assert_equal 0, @@sv.lot_nonprobankin(lot, @@nonprobank), "nonpro bankin failed for lot #{lot}"
    end
    # attach PSM
    assert_equal 0, @@sv.lot_experiment(lot, wafers1, @@eroute1, @@split_opno1, @@join_opno1, hold: params[:hold])
    #
    # repeat the test after lot_experiment_reset if reset: true
    (params[:reset] ? 2 : 1).times {
      # split if wafers2 is provided
      nlots = 1 # parent
      if wafers2
        assert lcsplit = @@sv.lot_split(lot, wafers2), "error splitting #{lot}, wafers #{wafers2}"
        nlots += 1 # split child
        assert_equal nlots, @@sv.lot_family(lot).size, "lot split error"
      end
      unless params[:reset]
        route = @@sv.lot_info(lot).route
        assert_equal 0, @@sv.lot_intermediate_psm(lot, route, @@split_opno1).size, "#{lot} check of intermediate PSMs failed"
        assert_equal 1, @@sv.lot_intermediate_psm(lot, route, @@join_opno1).size, "#{lot} check of intermediate PSMs failed"
      end
      # move parent to PSM split opno or trigger by other TX
      if params[:trigger] == :nonprobank
        assert_equal 0, @@sv.lot_nonprobankout(lot), "non-pro bankout failed for #{lot}"
      else
        assert_equal 0, @@sv.lot_opelocate(lot, @@split_opno1)
      end
      nlots += 1 # exp child
      lots = @@sv.lot_family(lot)
      assert_equal nlots, @@sv.lot_family(lot).size, "PSM split error"
      assert_equal 'ML-MR', @@sv.carrier_status(carrier).mltype
      unless params[:reset]
        route = @@sv.lot_info(lot).route
        assert_equal 0, @@sv.lot_intermediate_psm(lot, route, @@split_opno1).size, "#{lot} check of intermediate PSMs failed"
        assert_equal 0, @@sv.lot_intermediate_psm(lot, route, @@join_opno1).size, "#{lot} check of intermediate PSMs failed"
      end
      #
      if wafers2
        # move also split child to split op of experiment
        assert_equal 0, @@sv.lot_opelocate(lcsplit, @@split_opno1)
        nlots += 1 unless (wafers1 & wafers2).empty?  # exp child from split child
        lots = @@sv.lot_family(lot)
        assert_equal nlots, lots.size, "PSM split error"
      end
      # move lots to the merge op and merge
      lots.each {|l|
        assert_equal 0, @@sv.lot_hold_release(l, nil) if params[:hold]
        assert_equal 0, @@sv.lot_opelocate(l, @@join_opno1, route: @@route)
      }
      check_automerge(lots[1], lots[-1]) if (nlots == 4)
      assert_equal 0, @@sv.lot_experiment_reset(lot, @@split_opno1) if params[:reset]
      check_automerge(lot, nil)
      assert_equal 'SL-SR', @@sv.carrier_status(carrier).mltype
    }
  end

  def nested_psms(lot, wafers1, wafers2)
    assert @@sv.merge_lot_family(lot)
    assert_equal 0, @@sv.lot_cleanup(lot), 'wrong lot status'
    # attach PSMs
    assert_equal 0, @@sv.lot_experiment(lot, wafers1, @@eroute1, @@split_opno1, @@join_opno1)
    assert_equal 0, @@sv.lot_experiment(lot, wafers2, @@eroute2, @@split_opno2, @@join_opno2)
    # locate lot to first split_op
    assert_equal 0, @@sv.lot_opelocate(lot, @@split_opno1)
    lots = @@sv.lot_family(lot)
    assert_equal 2, lots.size, 'PSM1 split error'
    # locate lot to second split_op
    assert_equal 0, @@sv.lot_opelocate(lot, @@split_opno2)
    lots = @@sv.lot_family(lot)
    assert_equal 3, lots.size, "PSM split error"
    # locate to merge_opno and merge
    lots.each {|l| assert_equal 0, @@sv.lot_opelocate(l, @@join_opno1, route: @@route)}
    check_automerge(lot, nil)
  end

  def consecutive_psms(lot, wafers1, wafers2, wafers3, sequence, params={})
    assert @@sv.merge_lot_family(lot)
    assert_equal 0, @@sv.lot_cleanup(lot), 'wrong lot status'
    # attach PSMs
    assert_equal 0, @@sv.lot_experiment(lot, wafers1, @@eroute1, @@split_opno1, @@join_opno1)
    assert_equal 0, @@sv.lot_experiment(lot, wafers2, @@eroute3, @@split_opno3, @@join_opno3)
    assert_equal 0, @@sv.lot_experiment(lot, wafers2, @@eroute4, @@split_opno4, @@join_opno4,
      original_route: @@route, split_route: @@eroute1, original_opNo: @@split_opno1) if params[:branch_to_branch]
    # split
    assert lc1 = @@sv.lot_split(lot, wafers3), "error splitting #{lot}, wafers #{wafers2}"
    # locate parent lot and split child lot to first split_op
    assert_equal 0, @@sv.lot_opelocate(lot, @@split_opno1)
    assert_equal 0, @@sv.lot_opelocate(lc1, @@split_opno1) unless params[:move_child] == false
    lots = @@sv.lot_family(lot)
    assert_equal 3, lots.size, 'PSM1 split error'
    lc2 = lots[-1]  # PSM1 child
    $log.info "split child: #{lc1}, PSM1 child: #{lc2}"
    # locate to merge_opno1 in specified order
    lots = case sequence
    when 0; [lot, lc1, lc2]; # parent - splitchild - experimentchildren
    when 1; [lot, lc2, lc1]; # parent - experimentchildren - splitchild
    when 2; [lc1, lot, lc2]; # splitchild - parent - experimentchildren
    when 3; [lc1, lc2, lot]; # splitchild - experimentchildren - parent
    when 4; [lc2, lot, lc1]; # experimentchildren - parent - splitchild
    when 5; [lc2, lc1, lot]; # experimentchildren - splitchild - parent
    when 9; [lot, lc2]; # Stefanie's case
    end
    lots.each {|l| assert_equal 0, @@sv.lot_opelocate(l, @@join_opno1, route: @@route)}
    # merge parent and (remaining) PSM1 child
    check_automerge(lot, lc2)
    if @@sv.f7 # back-to-back is not supported
      assert_equal 0, @@sv.lot_opelocate(lot, @@split_opno3, route: @@route)
    end
    # merge experiment 2 lots
    lots = @@sv.lot_family(lot)
    lots.each {|l| assert_equal 0, @@sv.lot_opelocate(l, @@join_opno3, route: @@route)}
    check_automerge(lot, nil)
  end

  def branch_to_branch(lot, wafers1, wafers2)
    assert @@sv.merge_lot_family(lot)
    assert_equal 0, @@sv.lot_cleanup(lot), 'wrong lot status'
    # attach PSMs
    assert_equal 0, @@sv.lot_experiment(lot, wafers1, @@eroute1, @@split_opno1, @@join_opno1)
    assert_equal 0, @@sv.lot_experiment(lot, wafers2, @@eroute4, @@split_opno4, @@join_opno4,
      original_route: @@route, split_route: @@eroute1, original_opNo: @@split_opno1)
    # locate lot to first split_op
    assert_equal 0, @@sv.lot_opelocate(lot, @@split_opno1)
    lots = @@sv.lot_family(lot)
    assert_equal 2, lots.size, 'PSM1 split error'
    lc1 = lots[-1]
    # locate lot to second split_op
    assert_equal 0, @@sv.lot_opelocate(lc1, @@split_opno4)
    lots = @@sv.lot_family(lot)
    assert_equal 3, lots.size, 'PSM2 split error'
    lc2 = lots[-1]
    # merge child and grandchild at mrg_opno2
    [lc1, lc2].each {|l| assert_equal 0, @@sv.lot_opelocate(l, @@join_opno4, route: @@eroute1)}
    check_automerge(lc1, lc2)
    # merge lot and child at mrg_opno1
    [lot, lc1].each {|l| assert_equal 0, @@sv.lot_opelocate(l, @@join_opno1, route: @@route)}
    check_automerge(lot, nil)
  end

  def branch_to_branch_at_split(lot, wafers1, wafers2, params={})
    assert @@sv.merge_lot_family(lot)
    assert_equal 0, @@sv.lot_cleanup(lot), 'wrong lot status'
    # attach PSMs
    merge = params[:mergepoint] == false ? '' : nil # default is join_opNo
    assert_equal 0, @@sv.lot_experiment(lot, wafers1, @@eroute1, @@split_opno1, @@join_opno1, merge: merge)
    assert_equal 0, @@sv.lot_experiment(lot, wafers2, @@eroute6, @@split_opno6, @@join_opno6, merge: merge,
      original_route: @@route, split_route: @@eroute1, original_opNo: @@split_opno1)
    ##check_intermediate_psms(lot, [[@@split_opno1, 0], [@@join_opno1, 1]])
    # locate lot to first split_op
    assert_equal 0, @@sv.lot_opelocate(lot, @@split_opno1)
    lots = @@sv.lot_family(lot)
    assert_equal 3, lots.size, 'PSM1 split error'
    # grand-child already created
    lots = @@sv.lot_family(lot)
    assert_equal 3, lots.size, 'PSM2 split error'
    lc1 = lots[-2]
    assert_equal @@eroute1, @@sv.lot_info(lc1, operation_info: true).route, "grand child #{lc1} should be on branch"
    lc2 = lots[-1]
    assert_equal @@eroute6, @@sv.lot_info(lc2, operation_info: true).route, "grand child #{lc2} should be on branch-to-branch"
    # merge child and grandchild at mrg_opno2
    [lc1, lc2].each {|l| assert_equal 0, @@sv.lot_opelocate(l, @@join_opno6, route: @@eroute1)}
    check_automerge(lc1, lc2)
    # merge lot and child at mrg_opno1
    [lot, lc1].each {|l| assert_equal 0, @@sv.lot_opelocate(l, @@join_opno1, route: @@route)}
    check_automerge(lot, nil)
  end

  def multi_psms_and_split(lot, wafers1, wafers2, wafers3=nil)
    assert @@sv.merge_lot_family(lot)
    assert_equal 0, @@sv.lot_cleanup(lot), 'wrong lot status'
    nlots = 3 # parent + 2 experiments
    ndetails = 2 # as defined
    # define multi-PSM
    assert_equal 0, @@sv.lot_experiment(lot, [wafers1, wafers2], [@@eroute1, @@eroute1], @@split_opno1, @@join_opno1)
    assert_equal ndetails, @@sv.lot_experiment_info(lot, @@split_opno1).strExperimentalLotDetailInfoSeq.size, 'wrong number of experiments'
    # split if wafers3 is provided
    if wafers3
      assert @@sv.lot_split(lot, wafers3), "error splitting #{lot}, wafers #{wafers3}"
      nlots += 1
      nlots += 1 if !(wafers1 & wafers3).empty? && !(wafers1 - wafers3).empty?
      nlots += 1 if !(wafers2 & wafers3).empty? && !(wafers2 - wafers3).empty?
      ndetails += 1 if !(wafers1 & wafers3).empty?
      ndetails += 1 if !(wafers2 & wafers3).empty?
    end
    # locate parent and split child to split_op
    @@sv.lot_family(lot).each {|l| assert_equal 0, @@sv.lot_opelocate(l, @@split_opno1)}
    lots = @@sv.lot_family(lot)
    assert_equal nlots, lots.size, "PSM split error"
    assert_equal ndetails, @@sv.lot_experiment_info(lot, @@split_opno1).strExperimentalLotDetailInfoSeq.size, 'wrong number of experiments'
    # locate to merge_opno and merge
    lots.each {|l| assert_equal 0, @@sv.lot_opelocate(l, @@join_opno1, route: @@route)}
    check_automerge(lot, nil)
  end

  # merge check for different fabs used by multiple tests
  def check_automerge(lot, child)
    if @@sv.f7 && !child.nil? # check for automerge
      assert_equal 'EMPTIED', @@sv.lot_info(child).status, "#{child} should have been merged with parent automatically"
    else
      # merge any remaining child (even if nil given)
      if child.nil?
        # merge all child lots, no assert
        li = @@sv.lot_info(lot)
        children = @@sv.lot_family(lot) - [lot, li.parent]
        children.sort.reverse.collect {|lc|
          lci = @@sv.lot_info(lc)
          mlot = children.member?(lci.parent) ? lci.parent : lot
          @@sv.lot_merge(lot, lc)
        }
      else
        assert_equal 0, @@sv.lot_merge(lot, child), "failed to merge #{child} to #{lot}"
      end
    end
  end

end
