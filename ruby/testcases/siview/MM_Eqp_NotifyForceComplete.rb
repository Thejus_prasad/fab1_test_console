=begin 
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.
    
Author: Paul Peschel, 2013-08-02

History:
  2013-08-06 dsteger,  added EI notification
  2015-05-07 ssteidte, minor adjustments for R15, still compatible with R10
  2018-05-08 sfrieske, minor cleanup
  2019-12-09 sfrieske, minor cleanup, renamed from Test530_NotifyForceComplete
=end

require 'SiViewTestCase'


# Test MSR 272950 MM should inform EI in case of ForceOpeComplete
class MM_Eqp_NotifyForceComplete < SiViewTestCase
  @@opNo = '1000.600'
  @@opNoib = '1000.800'
  @@eqp_fb = 'UTF001'
  @@eqp_ib = 'UTI001'
  @@forcecomp_notify = 'ON' #OFF
  @@eichecked = '0'
  @@opemodes = ['Off-Line-1', 'Semi-Start-1', 'Semi-1', 'Semi-Comp-1', 'Auto-1', 'Auto-3']
  

  def self.after_all_passed
    @@testlots.each {|lot| @@sv.delete_lot_family(lot)}
  end


  def test00_setup
    $setup_ok = false
    #
    assert_equal @@forcecomp_notify, @@sv.environment_variable[1]['CS_SP_NOTIFY_EI_ON_FORCECOMP'], 'incorrect server setting'
    assert_equal '0', @@sv.environment_variable[1]['SP_PJCTRL_FUNC_ENABLED'], 'incorrect server setting'
    assert @@testlots = @@svtest.new_lots(2), 'error creating test lots'
    [@@eqp_fb, @@eqp_ib].each {|eqp| assert @@svtest.cleanup_eqp(eqp)} 
    #
    $setup_ok = true
  end
    
  def test10_fb_force_opecomplete
    lot = @@testlots.first
    eqp = @@eqp_fb
    @@opemodes.each {|mode|
      $log.info "-- mode #{mode.inspect}"
      assert_equal 0, @@sv.lot_cleanup(lot, opNo: @@opNo, stocker: @@svtest.stocker)
      assert_equal 0, @@sv.eqp_cleanup(eqp)
      @@svtest.eis.reset_forcecomp_jobs(eqp) if @@eichecked == '1'
      assert cj = @@sv.claim_process_lot(lot, mode: mode, eqp: eqp, running_hold: true), 'error processing lot'
      verify_forcecomp(eqp, cj, lot)
    }
  end
  
  def test11_ib_force_opecomplete
    lot = @@testlots.first
    eqp = @@eqp_ib
    @@opemodes.each {|mode|
      $log.info "-- mode #{mode.inspect}"
      assert_equal 0, @@sv.lot_cleanup(lot, opNo: @@opNoib, stocker: @@svtest.stocker)
      assert_equal 0, @@sv.eqp_cleanup(eqp)
      @@svtest.eis.reset_forcecomp_jobs(eqp) if @@eichecked == '1'
      assert cj = @@sv.claim_process_lot(lot, mode: mode, eqp: eqp, running_hold: true), 'error processing lot'
      verify_forcecomp(eqp, cj, lot)
    }
  end
  
  def test12_ib_ForceOpeComplete_multi
    @@testlots.each {|lot| assert_equal 0, @@sv.lot_cleanup(lot, opNo: @@opNoib)}
    eqp = @@eqp_ib
    assert_equal 0, @@sv.eqp_cleanup(eqp)
    assert cj = @@sv.claim_process_lot(@@testlots, mode: 'Off-Line-1', eqp: eqp, running_hold: true), 'error processing lot'
    verify_forcecomp(eqp, cj, @@testlots)
  end
  
  
  # aux methods
  
  def verify_forcecomp(eqp, cj, lots)
    if @@eichecked == '1'
      if @@forcecomp_notify == 'ON'
        assert_equal [cj], @@svtest.eis.forcecomp_jobs(eqp), 'failed to verify EI notification'
      elsif @@forcecomp_notify == 'OFF'
        assert_empty @@svtest.eis.forcecomp_jobs(eqp), 'no EI notification expected'
      end
    end
    lots = [lots] unless lots.kind_of?(Array)
    lots.each {|lot| assert @@sv.lot_hold_list(lot, reason: 'FCHL').first, "no FCHL hold for #{lot}"}
    return true
  end

end
