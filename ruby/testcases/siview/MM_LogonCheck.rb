=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: ssteidte, 2011-11-18

History:
  2014-10-28 ssteidte, separated login time and OJT tests
  2017-09-12 sfrieske, minor changes to password handling, renamed to MM_LogonCheck
  2019-12-10 sfrieske, minor cleanup
=end

require 'RubyTestCase'


# Verify TxLogonCheck, including subsystem authentication and acceptance of strange user names (MSR527203)
class MM_LogonCheck < RubyTestCase
  @@subsystem = 'MM'
  @@category = 'Report'

  def test01_logon_check
    assert_equal 0, @@sv.logon_check, 'failed to login with SiView pw'
    svnt = SiView::MM.new("#{$env}_gms")
    assert_equal 0, svnt.logon_check, 'failed to login with NT pw'
  end

  def test02_privileges
    assert_equal 0, @@sv.logon_check(subsystem: @@subsystem, category: @@category)
    priv1 = Hash[@@sv._res.subSystemFuncLists.collect {|ss|
      [ss.subSystemID, ss.strFuncIDs.collect {|e| [e.functionID, e.permission]}.sort]
    }]
    priv2 = @@sv.user_privileges(@@sv.user, @@subsystem, @@category)
    assert_equal priv1, priv2, 'wrong privileges'
  end

  def test03_strange_names
    users = ['Dummy', '.', ':', "ID: XXX - ; \t \n \r \r\n , "]
    users.each {|user| assert_equal 1466, @@sv.logon_check(user: user, password: SiView.nt_pw(''))}
  end

end
