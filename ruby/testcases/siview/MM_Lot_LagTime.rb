=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: Daniel Steger

History:
  2016-10-24 dsteger, added MES-3213
  2016-12-13 dsteger, MSRs 1058499 and 805556
  2018-03-06 sfrieske, replaced user_parameter_set_verify by user_paramer_change
  2019-12-09 sfrieske, minor cleanup, renamed from Test600_LagTime
=end

require 'SiViewTestCase'


# Lag time related tests
class MM_Lot_LagTime < SiViewTestCase
  @@route = 'UTRT-LAGTIME.01'
  @@product = 'UT-PROD-LAGTIME.01'
  @@proc_opNo = '1000.1100'
  @@proc2_opNo = '1000.1200'
  @@skip_opNo = '1000.1400'
  @@ieqp = 'UTI002'
  @@feqp = 'UTF002'
  @@lagtime = 120
  @@reworkroute = 'UTRW001.01'


  def self.after_all_passed
    @@testlots.each {|lot| @@sv.delete_lot_family(lot)}
  end


  def teardown
    assert @@sv.merge_lot_family(@@lot, nosort: true), "failed to merge lots"
  end


  def test00_setup
    $setup_ok = false
    #
    @@testlots = []
    assert @@lot = @@svtest.new_lot(product: @@product, route: @@route), "failed to create new lot"
    @@testlots << @@lot
    [@@ieqp, @@feqp].each {|eqp| assert @@svtest.cleanup_eqp(eqp, mode: 'Off-Line-1')}
    #
    $setup_ok = true
  end

  def test01_lagtime_ib
    assert_equal 0, @@sv.lot_cleanup(@@lot, opNo: @@proc_opNo), "failed to cleanup lot"
    assert @@sv.claim_process_lot(@@lot, eqp: @@ieqp), "failed to process lot"
    assert_equal "ONHOLD", @@sv.lot_info(@@lot).status, "lot should be onhold"
    assert_equal 1, @@sv.lot_hold_list(@@lot, reason: 'PLTH').size, "expected lagtime hold"
    $log.info "waiting #{@@lagtime}s for the lagtime to expire"; sleep @@lagtime
    assert wait_for(timeout: 60) {@@sv.lot_info(@@lot).status == 'Waiting'}, "lot should have been released"
  end

  def test02_lagtime_gatepass
    assert_equal 0, @@sv.lot_cleanup(@@lot, opNo: @@proc2_opNo), "failed to cleanup lot"
    assert_equal 0, @@sv.lot_gatepass(@@lot), "failed to gatepass lot"
    assert_equal "ONHOLD", @@sv.lot_info(@@lot).status, "lot should be onhold"
    assert_equal 1, @@sv.lot_hold_list(@@lot, reason: 'PLTH').size, "expected lagtime hold"
    $log.info "waiting #{@@lagtime}s for the lagtime to expire"; sleep @@lagtime
    assert wait_for(timeout: 60) {@@sv.lot_info(@@lot).status == 'Waiting'}, "lot should have been released"
  end

  def test03_lagtime_multilot
    assert_equal 0, @@sv.lot_cleanup(@@lot, opNo: @@proc_opNo), "failed to cleanup lot"
    clot = @@sv.lot_split(@@lot, 2)
    lots = [@@lot, clot]
    assert @@sv.claim_process_lot(lots, eqp: @@ieqp), "failed to process lot"
    lots.each do |lot|
      assert_equal "ONHOLD", @@sv.lot_info(lot).status, "#{lot} should be onhold"
      assert_equal 1, @@sv.lot_hold_list(lot, reason: 'PLTH').size, "expected lagtime hold"
    end
    $log.info "waiting #{@@lagtime} s for the lagtime to expire"; sleep @@lagtime
    assert wait_for(timeout: 60) {
      lots.inject(true) {|res, lot| res && @@sv.lot_info(lot).status == 'Waiting'}
    }, "lots should have been released"
  end

  def test04_lagtime_pre1  # MES-2987 no history written
    assert_equal 0, @@sv.lot_cleanup(@@lot, opNo: @@proc2_opNo), "failed to cleanup lot"
    assert_equal 0, @@sv.user_parameter_change('Lot', @@lot, 'UTSkip', 'YES'), 'failed to set script parameter'
    assert @@sv.claim_process_lot(@@lot, eqp: @@feqp), "failed to process lot"
    assert_equal 'ONHOLD', @@sv.lot_info(@@lot).status, "lot should be onhold"
    assert_equal 1, @@sv.lot_hold_list(@@lot, reason: 'PLTH').size, "expected lagtime hold"
    $log.info "waiting #{@@lagtime}s for the lagtime to expire"; sleep @@lagtime
    assert wait_for(timeout: 60) {@@sv.lot_info(@@lot).status == 'Waiting'}, "lot should have been released"
    assert_equal @@skip_opNo, @@sv.lot_info(@@lot).opNo, "unexpected current operation"
    assert_equal 0, @@sv.user_parameter_delete('Lot', @@lot, 'UTSkip', check: true), 'failed to remove skript parameter'
  end

  # MSR 1058499 / MES-2783
  def test05_lagtime_split
    assert_equal 0, @@sv.lot_cleanup(@@lot, opNo: @@proc2_opNo), "failed to locate lot"
    assert @@sv.claim_process_lot(@@lot, eqp: @@feqp), "failed to process lot"
    assert_equal 'ONHOLD', @@sv.lot_info(@@lot).status, "lot should be onhold"
    assert_equal 1, @@sv.lot_hold_list(@@lot, reason: 'PLTH').size, "expected lagtime hold"
    assert child = @@sv.lot_split(@@lot, 2, onhold: true, inheritholds: true), "failed to split lot with lag time"
    assert_equal 'ONHOLD', @@sv.lot_info(child).status, "#{child} should be onhold"
    assert_equal 1, @@sv.lot_hold_list(child, reason: 'PLTH').size, "expected lagtime hold"
    $log.info "waiting #{@@lagtime} s for the lagtime to expire"; sleep @@lagtime
    assert wait_for(timeout: 60) {
      [@@lot, child].inject(true) {|res, lot| res && @@sv.lot_info(lot).status == 'Waiting'}
    }, "lots should have been released"
  end

  # MSR 805556
  def test06_lagtime_partialrework
    assert_equal 0, @@sv.lot_cleanup(@@lot, opNo: @@proc2_opNo), "failed to locate lot"
    assert @@sv.claim_process_lot(@@lot, eqp: @@feqp), "failed to process lot"
    assert_equal 'ONHOLD', @@sv.lot_info(@@lot).status, "lot should be onhold"
    assert_equal 1, @@sv.lot_hold_list(@@lot, reason: 'PLTH').size, "expected lagtime hold"
    assert child = @@sv.lot_partial_rework(@@lot, 2, @@reworkroute, onhold: true, inheritholds: true), "failed to split lot with lag time"
    assert_equal 'ONHOLD', @@sv.lot_info(child).status, "#{child} should be onhold"
    assert_equal 1, @@sv.lot_hold_list(child, reason: 'PLTH').size, "expected lagtime hold"
    $log.info "waiting #{@@lagtime} s for the lagtime to expire"; sleep @@lagtime
    assert wait_for(timeout: 60) {
      [@@lot, child].inject(true) {|res, lot| res && @@sv.lot_hold_list(lot, reason: 'PLTH').empty?}
    }, "lots should have been released"
  end


  # MSR805559 / MES-1362
  def test10_lagtime_merge
    skip "requires R15 or MSR805559" if @@sv.release < 15
    assert_equal '1', @@sv.environment_variable[1]['SP_LAGTIMEINFO_MERGE_RULE'], " wrong config"
    assert_equal 0, @@sv.lot_cleanup(@@lot, opNo: @@proc2_opNo), "failed to cleanup lot"
    clot = @@sv.lot_split(@@lot, 2)
    # start lagtime for parent and wait half of the lagtime
    assert @@sv.claim_process_lot(@@lot, eqp: @@feqp), "failed to process lot"
    assert_equal 1, @@sv.lot_hold_list(@@lot, reason: 'PLTH').size, "expected lagtime hold"
    $log.info "waiting half of the lagtime"; sleep @@lagtime/2
    # start lagtime for child and merge
    assert @@sv.claim_process_lot(clot, eqp: @@feqp), "failed to process lot"
    tstart = Time.now
    assert_equal 1, @@sv.lot_hold_list(clot, reason: 'PLTH').size, "expected lagtime hold"
    assert_equal 0, @@sv.lot_merge(@@lot, clot), "failed to merge lots"
    # verify the hold is released after the whole lagtime (from child)
    $log.info "waiting #{@@lagtime + 60} sfor the lagtime to expire"
    assert wait_for(timeout: @@lagtime + 60) {@@sv.lot_info(@@lot).status == 'Waiting'}, "lot should have been released"
    duration = Time.now - tstart
    assert duration > @@lagtime - 5, "lot has been released early after #{duration} s"
  end

end
