=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: Steffen Steidten, 2010-08-27

History:
  2015-05-28 adarwais,	cleaned up and verified test cases with R15
  2019-12-09 sfrieske, minor cleanup, renamed from Test122_ReticleInPodSLR
  2020-08-13 sfrieske, minor cleanup
=end

require 'SiViewTestCase'


# Test SLR and OpeStart work with a Reticle in a pod loaded into the Tool, MSR 458356
class MM_Eqp_SLRReticleInPod < SiViewTestCase
  @@eqp = 'UTR002'
  @@port = 'P1'
  @@opNo = '100.200'
  @@reticle = 'UTRETICLE0003'
  @@pod = 'R29N'
  @@route = 'UT-MN-UTR001.01'
  @@product = 'UT-MN-UTR001.01'
  @@lot = nil  # for setup to work


  def self.after_all_passed
    @@sv.lot_cleanup(@@lot, opNo: @@opNo)
    @@sv.eqp_cleanup(@@eqp)
    self.cleanup_reticle_and_pod
    @@sv.delete_lot_family(@@lot)
  end


  def setup
    @@sv.lot_cleanup(@@lot, opNo: @@opNo) if @@lot
    @@sv.eqp_cleanup(@@eqp)
    self.class.cleanup_reticle_and_pod
  end


  def test00_setup
    $setup_ok = false
    #
    assert_equal 0, @@sv.eqp_mode_change(@@eqp, 'Off-Line-1')
    assert @@lot = @@svtest.new_controllot('Equipment Monitor', product: @@product, route: @@route), "error creating test lot"
    # stockout carrier for tests 21 and 22
    li = @@sv.lot_info(@@lot)
    assert_equal 0, @@sv.carrier_xfer_status_change(li.carrier, 'MO', @@svtest.stocker), "error changing xfer status of #{li.carrier} to MO"
    #
    $setup_ok = true
  end

  def test11_slr_reticle_only
    # load reticle into eqp
    @@sv.reticle_xfer_status_change(@@reticle, 'EI', eqp: @@eqp)
    # check status
    ri = @@sv.reticle_status(@@reticle).reticleStatusInfo
    assert_equal 'EI', ri.transferStatus
    assert_equal '', ri.reticlePodID.identifier
    # SLR
    assert cj = @@sv.slr(@@eqp, lot: @@lot), "SLR failed"
  end

  def test12_slr_reticle_and_pod
    # load reticle into pod
    @@sv.reticle_justinout(@@reticle, @@pod, 1, 'in')
    # load pod into eqp
    @@sv.reticle_pod_xfer_status_change(@@pod, 'EI', eqp: @@eqp)
    # check status
    ri = @@sv.reticle_status(@@reticle).reticleStatusInfo
    assert_equal 'EI', ri.transferStatus
    assert_equal @@pod, ri.reticlePodID.identifier
    pi = @@sv.reticle_pod_list(pod: @@pod).first.reticlePodStatusInfo
    assert_equal 'EI', pi.transferStatus
    assert pi.strContainedReticleInfo.find {|r| r.reticleID.identifier == @@reticle}
    # SLR
    assert cj = @@sv.slr(@@eqp, lot: @@lot), "SLR failed"
  end

  def test21_opestart_reticle_only
    # load reticle into eqp
    @@sv.reticle_xfer_status_change(@@reticle, 'EI', eqp: @@eqp)
    # check status
    ri = @@sv.reticle_status(@@reticle).reticleStatusInfo
    assert_equal 'EI', ri.transferStatus
    assert_equal '', ri.reticlePodID.identifier
    # load carrier
    sleep 10
    li = @@sv.lot_info(@@lot)
    assert_equal 0, @@sv.eqp_load(@@eqp, @@port, li.carrier), "error loading carrier #{li.carrier} on #{@@eqp}"
    # OpeStart
    assert cj = @@sv.eqp_opestart(@@eqp, li.carrier), "error issuing opestart for #{li.carrier} on #{@@eqp}"
  end

  def test22_opestart_reticle_and_pod
    # load reticle into pod
    @@sv.reticle_justinout(@@reticle, @@pod, 1, 'in')
    # load pod into eqp
    @@sv.reticle_pod_xfer_status_change(@@pod, 'EI', eqp: @@eqp)
    # check status
    ri = @@sv.reticle_status(@@reticle).reticleStatusInfo
    assert_equal 'EI', ri.transferStatus
    assert_equal @@pod, ri.reticlePodID.identifier
    pi = @@sv.reticle_pod_list(pod: @@pod).first.reticlePodStatusInfo
    assert_equal 'EI', pi.transferStatus
    assert pi.strContainedReticleInfo.find {|r| r.reticleID.identifier == @@reticle}
    # load carrier
    li = @@sv.lot_info(@@lot)
    assert_equal 0, @@sv.eqp_load(@@eqp, @@port, li.carrier), "error loading carrier #{li.carrier} on #{@@eqp}"
    # OpeStart
    assert cj = @@sv.eqp_opestart(@@eqp, li.carrier), "error issuing opestart for #{li.carrier} on #{@@eqp}"
  end


  # aux methods

  def self.cleanup_reticle_and_pod
    # set reticle and pod "EO" and remove the reticle from the pod
    # return true on success
    $log.info "cleanup_reticle_and_pod"
    @@sv.reticle_pod_xfer_status_change(@@pod, 'EO', eqp: @@eqp)
    pod = @@sv.reticle_status(@@reticle).reticleStatusInfo.reticlePodID.identifier
    @@sv.reticle_justinout(@@reticle, pod, 1, 'out') unless pod.empty?
    @@sv.reticle_xfer_status_change(@@reticle, 'EO', eqp: @@eqp)
    ri = @@sv.reticle_status(@@reticle).reticleStatusInfo
    ret = (ri.transferStatus == 'EO') && (ri.reticlePodID.identifier == '')
    pi = @@sv.reticle_pod_list(pod: @@pod).first.reticlePodStatusInfo
    ret &= pi.transferStatus == 'EO'
    $log.warn "  wrong status: #{pi.inspect} #{ri.inspect}" unless ret
    @@sv.reticle_status_change(@@reticle, 'AVAILABLE', check: true)

    # Regarding jira issue "MES-1602" till it is resolved: always set the reticle pod to "AVAILABLE" in order to successfully
    # JustIn reticle in the pod for further testing. This can be removed when Jira ticket is resolved and Siview allows to JustIn
    # Reticle while Pod status = "NOTAVAILABLE"
    @@sv.reticle_pod_multistatus_change(@@pod, 'AVAILABLE') unless pi.reticlePodStatus != 'NOTAVAILABLE'

    return ret
  end

end
