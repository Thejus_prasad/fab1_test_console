=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: sfrieske, 2017-01-17

History:
  2019-12-09 sfrieske, minor cleanup, renamed from Test312_NonProBankIn
=end

require 'SiViewTestCase'


# MSR 732754
class MM_Lot_NonProBankIn < SiViewTestCase
  @@nonprobank = 'W-BACKUPOPER'


  def self.after_all_passed
    @@sv.delete_lot_family(@@lot)
  end


  def test10
    assert @@lot = @@svtest.new_lot
    assert_equal 0, @@sv.wafer_sort(@@lot, '')
    assert_equal 0, @@sv.lot_nonprobankin(@@lot, @@nonprobank)
    assert_equal 0, @@sv.lot_nonprobankout(@@lot)
  end

end
