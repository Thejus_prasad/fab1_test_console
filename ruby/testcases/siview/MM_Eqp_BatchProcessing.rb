=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.
    
Author: Paul Peschel, 2013-02-19 migrate from TestLotProcessingInternalBuffer.java

History:
  2014-02-24 dsteger,  upgraded test case
  2018-05-31 sfrieske, added asserts for better analysis of preparation errors
  2019-12-05 sfrieske, minor cleanup, renamed from Test_LotProcessingBatch
=end

require 'SiViewTestCase'


# Test lot processing at a batch tool (internal buffer), MM must not crash in case of 12+ batch lots
class MM_Eqp_BatchProcessing < SiViewTestCase
  @@eqp = 'UTI005'
  @@mode = 'Auto-3' #Off-Line-1"
  @@opNo = '1000.200'

  @@testlots = []


  def self.after_all_passed
    @@testlots.each {|lot| @@sv.delete_lot_family(lot)}
  end


  def test00_setup
    $setup_ok = false
    #
    # prepare eqp
    @@capacity = @@sv.eqp_info(@@eqp).ib.buffers['Process Lot']
    assert @@capacity >= 12, "eqp #{@@eqp} must have a 'Process Lot' capacity at least of 12"
    assert @@svtest.cleanup_eqp(@@eqp)
    # create test lots
    assert @@lots = @@svtest.new_lots(@@capacity + 1), 'error creating test lots'
    @@testlots = @@lots
    #
    $setup_ok = true
  end

  def test11_moderate_capacity_test
    assert_equal 0, @@sv.eqp_cleanup(@@eqp, mode: @@mode), "failed to clean up #{@@eqp}"
    lots = @@lots.take(@@capacity - 1)
    lots.each {|lot| assert_equal 0, @@sv.lot_cleanup(lot, opNo: @@opNo, stocker: @@svtest.stocker)}
    # assert lots = prepare_resources(@@capacity - 1)
    assert @@sv.claim_process_lot(lots, eqp: @@eqp, mode: @@mode), 'error processing lots'
  end
  
  def test12_full_capacity_test
    assert_equal 0, @@sv.eqp_cleanup(@@eqp, mode: @@mode), "failed to clean up #{@@eqp}"
    lots = @@lots.take(@@capacity)
    lots.each {|lot| assert_equal 0, @@sv.lot_cleanup(lot, opNo: @@opNo, stocker: @@svtest.stocker)}
    # assert lots = prepare_resources(@@capacity)
    assert @@sv.claim_process_lot(lots, eqp: @@eqp, mode: @@mode), 'error processing lots'
  end
  
  def test13_too_many_lots_in_batch
    assert_equal 0, @@sv.eqp_cleanup(@@eqp, mode: @@mode), "failed to clean up #{@@eqp}"
    lots = @@lots.take(@@capacity + 1)
    lots.each {|lot| assert_equal 0, @@sv.lot_cleanup(lot, opNo: @@opNo, stocker: @@svtest.stocker)}
    # assert lots = prepare_resources(@@capacity + 1)
    @@sv.slr(@@eqp, lot: lots)
    assert_equal 2855, @@sv.rc
  end
  
  def test14_process_monitor_batch
    assert_equal 0, @@sv.eqp_cleanup(@@eqp, mode: @@mode), "failed to clean up #{@@eqp}"
    lots = @@lots.take(@@capacity - 1)
    lots.each {|lot| assert_equal 0, @@sv.lot_cleanup(lot, opNo: @@opNo, stocker: @@svtest.stocker)}
    # add a monitor lot
    assert monitor = @@svtest.new_controllot('Process Monitor')
    @@testlots << monitor
    # process lots
    # assert lots = prepare_resources(@@capacity - 1)
    assert @@sv.claim_process_lot(lots+[monitor], monitor: monitor, eqp: @@eqp, mode: @@mode), 'error processing lots'
    # check monitor relation
    assert_equal lots.sort, @@sv.monitorlot_groups(monitor).sort, 'monitor relation is wrong'
    # process monitor to release group
    assert @@sv.claim_process_lot(monitor, mode: 'Off-Line-1'), 'failed to process monitor lot'
    assert_empty @@sv.monitorlot_groups(monitor), 'monitor lot relation should have been released'
    # @@sv.delete_lot_family(monitor)
  end

end
