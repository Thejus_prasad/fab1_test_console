=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: Steffen Steidten, 2012-06-27

History:
  2019-02-05 sfrieske, minor cleanup
  2019-12-05 sfrieske, renamed from Test132_InhibitReticleEquipment
  2020-08-26 sfrieske, minor cleanup
=end

require 'SiViewTestCase'

# Test Reticle+Equipment inhibits, MSR 514486, implicitly test IB tools work with reticles, MSR 285056
class MM_InhibitReticleEquipment < SiViewTestCase
  @@sv_defaults = {product: 'UT-IB-RTCL.01', route: 'UTRT-IB-RTCL.01'}  # route must have photo layers defined
  @@eqp_fb = 'UTR003'
  @@eqp_ib = 'UTI004'  #'UTI003RTCL'
  @@reticle_ib = 'UTRETICLEIB02' # 'UTRETICLEIBRTCL'
  @@reticle_fb = 'UTRETICLEFBRTCL'


  def self.after_all_passed
    @@sv.delete_lot_family(@@lot)
    @@sv.eqp_cleanup(@@eqp_fb)
    @@sv.eqp_cleanup(@@eqp_ib)
  end


  def test00_setup
    $setup_ok = false
    #
    assert @@lot = @@svtest.new_lot, 'error creating testlot'
    assert el = @@sv.lot_eqplist_in_route(@@lot)
    assert @@opNo_fb = el.find {|opno, eqps| eqps.include?(@@eqp_fb)}.first, "no opNo for eqp #{@@eqp_fb} on route"
    assert @@opNo_ib = el.find {|opno, eqps| eqps.include?(@@eqp_ib)}.first, "no opNo for eqp #{@@eqp_ib} on route"
    #
    set_reticle_availability(@@eqp_fb, @@reticle_fb)
    set_reticle_availability(@@eqp_ib, @@reticle_ib)
    #
    $setup_ok = true
  end

  def test11_inhibits
    $setup_ok = false
    #
    cleanup_inhibits
    # set inhibits
    assert @@sv.inhibit_entity(:reticle_eqp, [@@reticle_fb, @@eqp_fb]), 'error setting inhibit'
    assert_equal 1, @@sv.inhibit_list(:reticle_eqp, [@@reticle_fb, @@eqp_fb]).size, 'wrong inhibit'
    # verify the inhibits don't show up as reticle or eqp inhibits
    assert_empty @@sv.inhibit_list(:reticle, @@reticle_fb)
    assert_empty @@sv.inhibit_list(:eqp, @@eqp_fb)
    #
    $setup_ok = true
  end

  def test12_fb_what_next
    $setup_ok = false
    #
    assert_equal 0, @@sv.lot_opelocate(@@lot, @@opNo_fb)
    assert res = @@sv.what_next(@@eqp_fb).find {|e| e.lot == @@lot}, "setup error, lot #{@@lot} not in WhatNext list"
    assert_equal 1, res.inhibits.size , "wrong inhibits: #{res.inhibits.inspect}"
    #
    $setup_ok = true
  end

  def test13_fb_slr
    $setup_ok = false
    #
    cleanup_inhibits
    assert prepare_processing(@@lot, @@opNo_fb, @@eqp_fb, @@reticle_fb), 'error preparing lot, eqp and reticle'
    [[:reticle, [@@reticle_fb]], [:eqp, [@@eqp_fb]], [:reticle_eqp, [@@reticle_fb, @@eqp_fb]]].each {|args|
      assert try_slr(@@lot, @@eqp_fb, args), 'SLR must fail'
    }
    #
    $setup_ok = true
  end

  def test14_fb_opestart
    cleanup_inhibits
    assert prepare_processing(@@lot, @@opNo_fb, @@eqp_fb, @@reticle_fb), 'error preparing lot, eqp and reticle'
    li = @@sv.lot_info(@@lot)
    $log.info "loading carrier #{li.carrier} on Equipment #{@@eqp_fb}"
    assert_equal 0, @@sv.eqp_load(@@eqp_fb, nil, li.carrier), 'error loading carrier'
    [[:reticle, [@@reticle_fb]], [:eqp, [@@eqp_fb]], [:reticle_eqp, [@@reticle_fb,@@eqp_fb]]].each {|args|
      assert try_opestart(@@lot, @@eqp_fb, nil, args), 'OpeStart must fail'
    }
  end


  def test21_ib_inhibits
    $setup_ok = false
    #
    set_reticle_availability(@@eqp_ib, @@reticle_ib)
    cleanup_inhibits
    # set inhibits
    assert @@sv.inhibit_entity(:reticle_eqp, [@@reticle_ib, @@eqp_ib]), 'error setting inhibit'
    assert_equal 1, @@sv.inhibit_list(:reticle_eqp, [@@reticle_ib, @@eqp_ib]).size, 'wrong inhibit'
    # verify the inhibits don't show up as reticle or eqp inhibits
    assert_empty @@sv.inhibit_list(:reticle, @@reticle_ib)
    assert_empty @@sv.inhibit_list(:eqp, @@eqp_ib)
    #
    $setup_ok = true
  end

  def test22_ib_what_next
    $setup_ok = false
    #
    assert_equal 0, @@sv.lot_opelocate(@@lot, @@opNo_ib)
    assert res = @@sv.what_next(@@eqp_ib).find {|e| e.lot == @@lot}, "setup error, lot #{@@lot} not in WhatNext list"
    assert_equal 1, res.inhibits.size , "wrong inhibits: #{res.inhibits.inspect}"
    #
    $setup_ok = true
  end

  def test23_ib_slr
    cleanup_inhibits
    assert prepare_processing(@@lot, @@opNo_ib, @@eqp_ib, @@reticle_ib), 'error preparing lot, eqp and reticle'
    [[:reticle, [@@reticle_ib]], [:eqp, [@@eqp_ib]], [:reticle_eqp, [@@reticle_ib,@@eqp_ib]]].each {|args|
      assert try_slr(@@lot, @@eqp_ib, args), 'SLR must fail'
    }
  end

  def test24_ib_opestart
    cleanup_inhibits
    assert prepare_processing(@@lot, @@opNo_ib, @@eqp_ib, @@reticle_ib), 'error preparing lot, eqp and reticle'
    li = @@sv.lot_info(@@lot)
    $log.info "loading carrier #{li.carrier} on Equipment #{@@eqp_ib}"
    assert_equal 0, @@sv.eqp_load(@@eqp_ib, nil, li.carrier), 'error loading carrier'
    [[:reticle, [@@reticle_ib]], [:eqp, [@@eqp_ib]], [:reticle_eqp, [@@reticle_ib, @@eqp_ib]]].each {|args|
      assert try_opestart(@@lot, @@eqp_ib, nil, args), 'OpeStart must fail'
    }
  end


  def try_slr(lot, eqp, inhibitargs)
    $log.info "\n\ntrying SLR with inhibit #{inhibitargs.inspect}"
    # try without inhibit
    cj = @@sv.slr(eqp, lot: lot)
    ($log.warn "error calling slr without inhibit"; return) unless cj
    res = @@sv.slr_cancel(eqp, cj)
    ($log.warn "error calling slr_cancel without inhibit"; return) if res != 0
    # set inhibit
    @@sv.inhibit_entity(*inhibitargs) || ($log.warn 'error setting inhibit'; return)
    # try again
    cj = @@sv.slr(eqp, lot: lot)
    ($log.warn "SLR must fail"; return false) if cj
    ($log.warn "incorrect SLR return code, wrong setup (inhibit/eqp/reticle)"; return false) if @@sv.rc != 979
    res = @@sv.inhibit_cancel(*inhibitargs)
    ($log.warn "error removing inhibit"; return) if res != 0
    return true
  end

  def try_opestart(lot, eqp, port, inhibitargs)
    li = @@sv.lot_info(lot)
    $log.info "\n\ntrying OpeStart with inhibit #{inhibitargs.inspect}"
    # try without inhibit
    cj = @@sv.eqp_opestart(eqp, li.carrier)
    ($log.warn "error calling opestart without inhibit"; return) unless cj
    res = @@sv.eqp_opestart_cancel(eqp, cj)
    ($log.warn "error calling opestart_cancel without inhibit"; return) if res != 0
    # set inhibit
    @@sv.inhibit_entity(*inhibitargs) || ($log.warn 'error setting inhibit'; return)
    # try again
    cj = @@sv.eqp_opestart(eqp, li.carrier)
    ($log.warn "OpeStart must fail"; return) if cj
    ($log.warn "incorrect SLR return code, wrong setup (inhibit/eqp/reticle)"; return false) if @@sv.rc != 979
    res = @@sv.inhibit_cancel(*inhibitargs)
    ($log.warn "error removing inhibit"; return) if res != 0
    return true
  end

  def prepare_processing(lot, opNo, eqp, reticle)
    $log.info "prepare_processing #{lot}, #{opNo}, #{eqp}, #{reticle}"
    assert_equal 0, @@sv.lot_cleanup(lot, opNo: opNo)
    li = @@sv.lot_info(lot)
    ($log.warn "  lot #{lot} is not at an op for #{eqp}"; ret=false) unless li.opEqps.member?(eqp)
    assert_equal 0, @@sv.carrier_xfer_status_change(li.carrier, 'EO', eqp)
    assert_equal 0, @@sv.eqp_mode_change(eqp, 'Off-Line-1', notify: false)
    # ensure reticle is loaded
    @@sv.reticle_xfer_status_change(reticle, 'EI', eqp: eqp)
    ri = @@sv.reticle_status(reticle).reticleStatusInfo
    unless ri.equipmentID.identifier == eqp && ri.transferStatus == 'EI'
      $log.warn "reticle #{reticle} not loaded on #{eqp}"
      ret=false
    end
    # disable all other reticles in eqp
    set_reticle_availability(eqp, reticle)
    return true
  end

  def set_reticle_availability(eqp, reticle)
    @@sv.eqp_info_raw(eqp).strEquipmentAdditionalReticleAttribute.strStoredReticles.each {|r|
      status = (r.reticleID.identifier == reticle) ? 'AVAILABLE' : 'NOTAVAILABLE'
      assert_equal 0, @@sv.reticle_status_change(r.reticleID.identifier, status) if r.status != status
    }
  end

  def cleanup_inhibits
    $log.info 'cleanup_inhibits'
    assert_equal 0, @@sv.inhibit_cancel(:reticle, [@@reticle_fb])
    assert_equal 0, @@sv.inhibit_cancel(:reticle_eqp, [@@reticle_fb, @@eqp_fb])
    assert_equal 0, @@sv.inhibit_cancel(:reticle, [@@reticle_ib])
    assert_equal 0, @@sv.inhibit_cancel(:reticle_eqp, [@@reticle_ib, @@eqp_ib])
    assert_equal 0, @@sv.eqp_cleanup(@@eqp_fb)
    assert_equal 0, @@sv.eqp_cleanup(@@eqp_ib)
  end

end
