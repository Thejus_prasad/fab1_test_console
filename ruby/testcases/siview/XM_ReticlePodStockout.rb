=begin 
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.
    
Author: Steffen Steidten, 2012-04-10

History:
  2013-09-06 dsteger,  fixed reticle in pod logic
  2019-12-03 sfrieske, minor cleanup
  2020-08-13 sfrieske, minor cleanup
=end

require 'SiViewTestCase'

# Test XM reticle pod stockout, MSRs 564312, 356882 and 595740
class XM_ReticlePodStockout < SiViewTestCase
  @@stocker = 'UTSTO11'
  @@port = 'P4'
  
  @@rpstocker = 'UTRPSTO110'
  @@rpstocker2 = 'UTRPSTO120'
  @@rpport = 'P4'
  @@pod = 'R263'
  @@pod2 = 'R204'  
  @@reticle = '4048AUGPA1'
  
  @@sleeptime = 10
  
  
  def test00_setup
    $setup_ok = false
    #
    assert @@xm = SiView::XM.new($env)
    #
    # smoke test to enusre (Dummy) AMHS works
    assert carrier = @@svtest.get_empty_carrier
    @@sv.carrier_xfer_status_change(carrier, '', @@stocker, manualin: true)
    cs = @@sv.carrier_status(carrier)
    assert cs.xfer_status == 'MI' && cs.stocker == @@stocker, "wrong carrier status: #{cs.inspect}"
    # XM stockout
    assert_equal 0, @@xm.transport_job_create(carrier, @@stocker, '', @@stocker, @@port), 'error creating transport job'
    sleep @@sleeptime
    cs = @@sv.carrier_status(carrier)
    assert cs.xfer_status == 'MO' && cs.stocker == @@stocker, "wrong carrier status: #{cs.inspect}"
    #
    $setup_ok = true
  end
  
  def test11_reticle_pod_empty
    # assert_equal 0, @@sv.reticle_pod_xfer_status_change_new(@@pod, 'SI', @@rpstocker), 'error stocking in pod'
    assert_equal 0, @@sv.reticle_pod_xfer_status_change(@@pod, 'SI', stocker: @@rpstocker), 'error stocking in pod'
    # rs = @@sv.reticle_pod_status(@@pod)
    # assert rs.xfer_status == 'SI' && rs.stocker == @@rpstocker, "wrong carrier status: #{rs.inspect}"
    verify_rpod_status(@@pod, 'SI', @@rpstocker)
    # XM stockout
    assert_equal 0, @@xm.transport_job_create(@@pod, @@rpstocker, '', @@rpstocker, @@rpport), 'error creating transport job'
    sleep @@sleeptime
    # rs = @@sv.reticle_pod_status(@@pod)
    # assert rs.xfer_status == "MO" && rs.stocker == @@rpstocker, "wrong pod status: #{rs.inspect}"
    verify_rpod_status(@@pod, 'MO', @@rpstocker)
  end
  
  def test12_reticle_in_pod
    prepare_reticle_in_pod(@@pod2, @@reticle, @@rpstocker)    
    # XM stockout
    assert_equal 0, @@xm.transport_job_create(@@pod2, @@rpstocker, '', @@rpstocker, @@rpport), 'error creating transport job'
    sleep @@sleeptime
    verify_rpod_status(@@pod2, 'MO', @@rpstocker)
    verify_reticle_status(@@reticle, 'MO', @@rpstocker)
  end
  
  # disabled since this is not a "actual" stocker
  def XXtest13_bare_reticle
    rs = @@sv.reticle_status(@@reticle).reticleStatusInfo
    unless ['SI', 'MI'].member?(rs.transferStatus)
      @@sv.reticle_pod_xfer_status_change_new(@@pod, "MO", @@brstocker)
      @@sv.reticle_pod_offline_load(@@brstocker, @@port, @@pod)
      @@sv.reticle_offline_store(@@brstocker, @@port, @@pod, @@reticle)
    end
    verify_reticle_status(@@reticle, 'SI', @@brstocker)
    # XM stockout
    assert_equal 0, @@xm.transport_job_create(@@pod, @@brstocker, '', @@brstocker, @@brport), 'error creating transport job'
    sleep @@sleeptime
    verify_reticle_status(@@reticle, 'MO', @@brstocker)
  end
  
  def test21_reticle_pod_empty_unconfigured_port
    @@sv.reticle_pod_xfer_status_change_new(@@pod, 'SI', @@rpstocker)
    # rs = @@sv.reticle_pod_status(@@pod)
    # assert (rs.xfer_status == 'SI' and rs.stocker == @@rpstocker), "wrong carrier status: #{rs.inspect}"
    verify_rpod_status(@@pod, 'SI', @@rpstocker)
    # XM "stockout" to unconfigured port
    assert_equal 0, @@xm.transport_job_create(@@pod, @@rpstocker, '', @@rpstocker, "unconfigured"), 'error creating transport job'
    assert @@sv.wait_carrier_xfer(@@pod)
    sleep @@sleeptime
    # rs = @@sv.reticle_pod_status(@@pod)
    # assert (rs.xfer_status == 'SI' and rs.stocker == @@rpstocker), "wrong pod status: #{rs.inspect}"
    verify_rpod_status(@@pod, 'SI', @@rpstocker)
    # XM "stockout" to unconfigured port on second stocker
    assert_equal 0, @@xm.transport_job_create(@@pod, @@rpstocker, '', @@rpstocker2, "P1"), 'error creating transport job'
    assert @@sv.wait_carrier_xfer(@@pod)
    # rs = @@sv.reticle_pod_status(@@pod)
    # assert (rs.xfer_status == 'SI' and rs.stocker == @@rpstocker2), "wrong pod status: #{rs.inspect}"
    verify_rpod_status(@@pod, 'SI', @@rpstocker2)
  end
  
  def test22_reticle_in_pod_unconfigured_port
    prepare_reticle_in_pod(@@pod2, @@reticle, @@rpstocker)
    # XM "stockout" to unconfigured port
    assert_equal 0, @@xm.transport_job_create(@@pod2, @@rpstocker, '', @@rpstocker, "unconfigured"), 'error creating transport job'
    sleep @@sleeptime
    verify_rpod_status(@@pod2, 'SI', @@rpstocker)
    verify_reticle_status(@@reticle, 'SI', @@rpstocker)
    # XM "stockout" to unconfigured port on second stocker
    assert_equal 0, @@xm.transport_job_create(@@pod2, @@rpstocker, '', @@rpstocker2, "unconfigured"), 'error creating transport job'
    assert @@sv.wait_carrier_xfer(@@pod2)
    verify_rpod_status(@@pod2, 'SI', @@rpstocker2)
    verify_reticle_status(@@reticle, 'SI', @@rpstocker2)
  end

  
  # aux methods

  # prepare: ensure reticle is in the pod
  def prepare_reticle_in_pod(pod, reticle, rpstocker)
    # reticle is already in pod?
    unless @@sv.reticle_pod_status(pod).reticlePodStatusInfo.strContainedReticleInfo.find {|e| 
                            e.reticleID.identifier == reticle}
      _oldpod = @@sv.reticle_status(reticle).reticleStatusInfo.reticlePodID.identifier
      unless _oldpod.empty?
        assert_equal 0, @@sv.reticle_justinout(reticle, _oldpod, 1, 'out'), "#{reticle} cannot be removed from #{_oldpod}"
      end
      assert_equal 0, @@sv.reticle_justinout(reticle, pod, 1, 'in'), "#{reticle} needs to be in #{pod}"
    end
    # force pod/reticle to stocker    
    @@sv.reticle_pod_xfer_status_change(pod, 'SI', stocker: rpstocker)
    # precondition check
    assert rps = @@sv.reticle_pod_status(pod).reticlePodStatusInfo
    assert rps.transferStatus == 'SI' && rps.stockerID.identifier == rpstocker, "wrong pod status: #{rps.inspect}"
    assert_equal reticle, rps.strContainedReticleInfo.first.reticleID.identifier, "wrong reticle in pod: #{rps.inspect}"
    verify_reticle_status(reticle, 'SI', rpstocker)
  end

  def verify_reticle_status(reticle, expstatus, expstocker)
    rs = @@sv.reticle_status(reticle).reticleStatusInfo
    assert_equal expstatus, rs.transferStatus, 'wrong xfer status'
    assert_equal expstocker, rs.stockerID.identifier, 'wrong stocker'
  end

  def verify_rpod_status(rpod, expstatus, expstocker)
    rps = @@sv.reticle_pod_status(rpod).reticlePodStatusInfo
    assert_equal expstatus, rps.transferStatus, 'wrong xfer status'
    assert_equal expstocker, rps.stockerID.identifier, 'wrong stocker'
  end

end
