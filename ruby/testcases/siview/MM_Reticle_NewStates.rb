=begin 
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.
    
Author: Paul Peschel, 2013-02-01 migrate from Test030_NewReticleStates.java

History:
  2013-07-17 ssteidte, fixed runtests
  2015-06-01 adarwais, cleanup
  2019-12-09 sfrieske, minor cleanup, renamed from Test030_NewReticleStates
=end

require 'RubyTestCase'


class MM_Reticle_NewStates < RubyTestCase    
    @@rpod1 = 'R222'
    @@rpods2 = %w(R223 R224 R225)
    @@states = %w(NOTAVAILABLE AVAILABLE INUSE SCRAPPED)
    @@substates = %w(DIRTY CLEANING REPAIR)
    @@substatesfor = %w(NOTAVAILABLE)


    def test00_setup
      $setup_ok = false
      ([@@rpod1] + @@rpods2).each {|r|
        assert_equal 0, @@sv.reticle_pod_multistatus_change(r, 'AVAILABLE', check: true)
      }
      $setup_ok = true
    end

    def test11_one_pod
      runtest([@@rpod1])
    end
    
    def test12_multiple_pods
      assert @@rpods2.size > 1, "must be more than one reticle pod"
      runtest(@@rpods2)
    end
    
    # test the new ReticlePodStatus method for a reticle array
    def runtest(rpods)
      @@states.each {|s|
        if @@substatesfor.member?(s)
          @@substates.each {|su|
            # 1st time ok
            assert_equal 0, @@sv.reticle_pod_multistatus_change(rpods, s, substate: su)
            # 2nd time must fail
            assert_equal 11902, @@sv.reticle_pod_multistatus_change(rpods, s, substate: su)
          }
          res3 = @@sv.reticle_pod_multistatus_change(rpods, s)
        else
          assert_equal 0, @@sv.reticle_pod_multistatus_change(rpods, s)
          assert_equal 2004, @@sv.reticle_pod_multistatus_change(rpods, s)
          @@substates.each {|su|
            assert_equal 11901, @@sv.reticle_pod_multistatus_change(rpods, s, substate: su)
          }
        end
      }
    end
end
