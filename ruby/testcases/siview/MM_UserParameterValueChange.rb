=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: Steffen Steidten, 2011-01-12

History:
  2017-07-18 sfrieske, fixed call of user_parameter_change_multi
  2019-12-09 sfrieske, removed tests for unused CS_TxUserParameterValueForMultipleObjectReq
  2019-12-10 sfrieske, renamed from Test084_UserParameterValueChange
=end

require 'SiViewTestCase'

# Test TxUserParameterValueChangeReq and CS_TxUserParameterValueForMultipleObjectReq including MSR 339580
class MM_UserParameterValueChange < SiViewTestCase
  @@userparameters = {'STRING'=>'UTstring', 'INTEGER'=>'UTinteger', 'REAL'=>'UTreal'}


  def self.after_all_passed
    @@sv.delete_lot_family(@@lot)
  end


  def test00_setup
    assert @@lot = @@svtest.new_lot
    @@wafer = @@sv.lot_info(@@lot, wafers: true).wafers.first.wafer
  end

  def test11_correct_datatypes
    @@userparameters.each_pair {|datatype, pname|
      assert check_userparams('Lot', @@lot, pname, datatype)
      assert check_userparams('Wafer', @@wafer, pname, datatype)
    }
  end

  def test12_wrong_type_for_string
    datatype = 'STRING'
    pname = @@userparameters[datatype]
      assert check_wrong_datatypes('Lot', @@lot, pname, datatype)
      assert check_wrong_datatypes('Wafer', @@wafer, pname, datatype)
  end

  def test13_wrong_type_for_integer
    datatype = 'INTEGER'
    pname = @@userparameters[datatype]
    assert check_wrong_datatypes('Lot', @@lot, pname, datatype)
    assert check_wrong_datatypes('Wafer', @@wafer, pname, datatype)
  end

  def test14_wrong_type_for_real
    datatype = 'REAL'
    pname = @@userparameters[datatype]
    assert check_wrong_datatypes('Lot', @@lot, pname, datatype)
    assert check_wrong_datatypes('Wafer', @@wafer, pname, datatype)
  end


  # aux methods

  def check_userparams(classname, pid, pname, datatype)
    $log.info "\ntesting #{classname} datatype #{datatype.inspect}"
    # cleanup
    if @@sv.user_parameter(classname, pid, name: pname).valueflag  # avoid errors
      @@sv.user_parameter_change(classname, pid, pname, '', 3, datatype: datatype)
    end
    refute @@sv.user_parameter(classname, pid, name: pname).valueflag, "parameter #{pname} not deleted"
    # pick values
    if datatype == 'STRING'
      values = ['TestXX1', 'TestXX2']
    elsif datatype == 'INTEGER'
      values = [11, -22, 45678]
    elsif datatype == 'REAL'
      values = [11, -22, 45678.789, -33.66]
    else
      refute "datatype #{datatype} cannot be tested"
    end
    # test
    [1, 2].each {|mode|
      values.each {|value|
        assert_equal 0, @@sv.user_parameter_change(classname, pid, pname, value, mode, datatype: datatype)
        p = @@sv.user_parameter(classname, pid, name: pname)
        assert_equal value, value_of(p), "error changing parameter #{p.inspect}"
      }
    }
  end

  def check_wrong_datatypes(classname, pid, pname, pdatatype)
    typevalues = {'STRING'=>'NA', 'INTEGER'=>11, 'REAL'=>-11.1}  # valid values per type
    @@userparameters.each_pair {|datatype, name|
      next if pdatatype == datatype
      $log.info "\ntesting wrong #{classname} datatype #{datatype} for #{pdatatype}"
      # cleanup, parameter is deleted (valueflag=false)
      if @@sv.user_parameter(classname, pid, name: pname).valueflag  # avoid errors
        @@sv.user_parameter_change(classname, pid, pname, '', 3, datatype: datatype)
      end
      refute @@sv.user_parameter(classname, pid, name: pname).valueflag, "parameter #{pname} not deleted"
      # try to add a currently deleted parameter
      value = typevalues[datatype]  # pick a valid value for the wrong datatype
      assert_equal 909, @@sv.user_parameter_change(classname, pid, pname, value, 1, datatype: datatype)
      refute @@sv.user_parameter(classname, pid, name: pname).valueflag, "false add of #{p.inspect}"
      # set a valid value in preparation for the update test
      value = typevalues[pdatatype]  # a valid value for the tested datatype
      assert_equal 0, @@sv.user_parameter_change(classname, pid, pname, value, 1, datatype: pdatatype)
      # try to update parameter
      assert_equal 909, @@sv.user_parameter_change(classname, pid, pname, value, 2, datatype: datatype)
      p = @@sv.user_parameter(classname, pid, name: pname)
      assert_equal value, value_of(p), "false change of #{p.inspect}"
    }
  end

  # return parameter value with correct type, not just as string
  def value_of(p)
    if p.datatype == 'STRING'
      v = p.value
    elsif p.datatype == 'INTEGER'
      v = p.value.to_i
    elsif p.datatype == 'REAL'
      v = p.value.to_f
    end
  end

end
