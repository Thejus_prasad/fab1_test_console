=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: Steffen Steidten, 2013-01-10

History:
  2014-10-30 ssteidte, query for the value set instead of event time
  2018-03-06 sfrieske, replaced user_parameter_set_verify by user_paramer_change
  2019-12-09 sfrieske, minor cleanup, renamed from Test086_UserParameterHistory
  2021-08-30 sfrieske, moved siviewmmdb.rb to siview/mmdb.rb
  2021-12-09 sfrieske, added missing require 'util/waitfor'

  TODO: merge with MM_UserParameterValueChange?
=end

require 'siview/mmdb'
require 'util/waitfor'
require 'RubyTestCase'


# Test SiView history is written for TxUserParameterValueChangeReq and AMD_TxUserParameterValueChangeReq
#
# Ensure replication is enabled if using the MDS."
class MM_UserParameterHistory < RubyTestCase
  @@eqp = 'UTF001'
  @@parameter = 'MaxProcessWaferCnt'  #"IdleTime"
  @@use_mds = true


  def test00_setup
    $setup_ok = false
    #
    assert @@mmdb = SiView::MMDB.new($env, use_mds: @@use_mds)
    # enusere entity is defined
    assert @@sv.eqp_info(@@eqp), "eqp #{@@eqp} not defined in #{@@sv.env}"
    # ensure user parameter is defined
    p = @@sv.user_parameter("Eqp", @@eqp, :name=>@@parameter)
    assert p, "user parameter #{@@parameter} not defined for #{@@eqp}"
    #
    $setup_ok = true
  end

  def test11_core
    execute(false)
  end

  def test12_customized
    execute(true)
  end

  def execute(customized)
    tstart = Time.now
    v = tstart.to_i # "QA_#{tstart}"
    sleep 1
    assert_equal 0, @@sv.user_parameter_change('Eqp', @@eqp, @@parameter, v, 1, tx_customized: customized), "error setting #{@@parameter}"
    evs = nil
    $log.info 'getting history'
    assert wait_for(timeout: 90) {
      # time based query fails if working remotely in a different time zone
      evs = @@mmdb.user_parameter_history(id: @@eqp, value: v.to_s)
      !evs.empty?
    }, 'no event found in history table'
    assert_equal 1, evs.size, 'more than one event in the history'
    assert_equal v.to_s, evs.first[:value], "wrong parameter value in #{evs.first.inspect}"
  end
end
