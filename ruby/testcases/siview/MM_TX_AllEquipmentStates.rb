=begin 
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.
    
Author: Paul Peschel, 2013-02-01 migrate from Test071_TxAllEquipmentStatesInq.java

History:
  2013-03-27 dsteger,  removed all but test20. The rest is obsolete.
  2013-07-31 ssteidte, added call to AMD_TxAllEquipmentStatesInq to ruby/lib
  2014-03-28 dsteger,  dual CMMS support via userid (MSR 841753)
  2019-12-09 sfrieske, minor cleanup, renamed from Test071_TxAllEquipmentStatesInq
=end

require 'RubyTestCase'


class MM_TX_AllEquipmentStates < RubyTestCase
    @@loops = 10
    @@user = 'X-UNITTEST' # X-SAP, X-XSITE require standard password schema
    
    def test10_AMD_TxAllEquipmentStatesInq
      # Tx called by SAPPM, returns nil (calls SAP-PM internally). Just check if there are no exceptions
      @@loops.times {assert_nil @@sv.AMD_TxAllEquipmentStatesInq(user: @@user)}
    end
    
end
