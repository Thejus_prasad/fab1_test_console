=begin 
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.
    
Author: ssteidte, 2012-05-31

History:
  2015-10-07 sfrieske, cleaned up, SM based verification
  2019-12-05 sfrieske, minor cleanup, renamed from Test020_GetUDATA
  2022-01-07 sfrieske, minor cleanup
=end

require 'SiViewTestCase'


# Test the AMD_TxGetUDATAInq, MSRs 81234, 114870 and 116510
class MM_Udata_AMD_TxGetUDATA < SiViewTestCase
  @@udata_classes = %w(AREA AREAGRP BANK CALENDAR CAST CTRLJOB CUSTOMER CUSTPROD DCDEF DCSPEC 
    DRBL DRBLGRP E10STATE EQP EQPST LOT LOTFAMILY LOTNOTE LOTSCHE LOTTYPE LRCP MRCP MSGDEF 
    PLSPLIT PRODGRP PRODREQ PRODSPEC RAWEQPST RTCLPOD RTCLSET SCRIPT STAGE STAGEGRP 
    STK SYSMESSAGE TECH TESTSPEC TESTTYPE USER WAFER)
    
  @@udata_code_objects = ['Action Code:', 'AdjustChip:', 'AdjustDie:', 'BankHold:', 'BankHoldRelease:', 
    'Carrier Category:', 'ChipScrap:', 'ChipScrapCancel:', 'Department:', 'E-REWORK:', 'EntityInhibit:', 
    'EntityInhibitCancel:', 'Equipment Category:', 'Equipment Status:', 'Equipment Type:', 'FutureHold:', 
    'FutureHoldCancel:', 'Inspection Type:', 'Level:', 'Load Purpose Type:', 'Lot Control Use State:', 
    'LotHold:', 'LotHoldRelease:', 'Main Process Definition Typ:', 'Message Distribution Type:', 
    'Message Media:', 'Mfg Layer:', 'Operation Mode:', 'PPLineHold:', 'Photo Layer:', 
    'Priority Class Type:', 'ProcessHold:', 'ProcessHoldCancel:', 'ProcessPending:', 'ProcessResume:', 
    'Product Type:', 'ReticlePod Category:', 'Rework:', 'ReworkCancel:']
    
  @@udata_objects = [
    # if the object ends with ':'' the class is CODE and the object e.g. 'Carrier Category:BEOL'
    [:carriercategory, 'Carrier Category:', 'BEOL'],
    [:department, 'Department:', 'ETC',],
    [:eqptype, 'Equipment Type:', 'CVD16X'],
    [:reticlepodcategory, 'ReticlePod Category:', 'BMP'],
    [:pd, 'PD', 'FOUT-CERT.01'],
    [:mainpd, 'PD', 'C02-1550.01'],
    [:user, 'USER', 'X-UNITTEST']
  ]
  @@udata_wrong_class = ['USER1', 'X-UNITTEST']
  @@udata_wrong_object = ['PRIVGRP', 'SC10']
  
  
  def test11_classes
    test_ok = true
    @@udata_classes.each {|c|
      res = @@sv.AMDGetUDATA(c, '')
      ($log.warn "error getting UDATA for class #{c.inspect}"; test_ok=false) unless res
    }
    assert test_ok
  end
  
  def test12_code_objects
    test_ok = true
    @@udata_code_objects.each {|o|
      res = @@sv.AMDGetUDATA('CODE', o)
      ($log.warn "error getting UDATA for class 'CODE', object #{o.inspect}"; test_ok=false) unless res
    }
    assert test_ok
  end
  
  def test13_fields
    test_ok = true
    @@udata_objects.each {|smclass, getclass, objid|
      $log.info "-- class #{smclass}"
      smudata = @@sm.object_info(smclass, objid).first.udata
      if getclass.end_with?(':')
        objid = getclass + objid
        getclass = 'CODE'
      end
      mmudata = @@sv.AMDGetUDATA(getclass, objid)
      ($log.warn "mismatch: #{smudata}, got #{mmudata}"; test_ok=false) if smudata != mmudata
    }
    assert test_ok
  end

  def test14_notexisting_object
    assert_equal({},  @@sv.AMDGetUDATA('USER', 'xxnotexist'))
  end
  
  def test21_wrong_class
    $log.info "#{@@udata_wrong_class}"
    @@sv.AMDGetUDATA(*@@udata_wrong_class)
    assert_equal 10902, @@sv.rc, 'wrong RC'
  end
  
  def test22_wrong_object
    $log.info "#{@@udata_wrong_object}"
    @@sv.AMDGetUDATA(*@@udata_wrong_object)
    assert_equal 10904, @@sv.rc, 'wrong RC'
  end

end
