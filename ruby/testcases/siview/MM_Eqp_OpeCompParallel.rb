=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.
    
Author: Thomas Klose, 2014-03-04

History:
  2015-11-04 sfrieske, leveraging claim_process_lot with a block to simplify code
  2018-03-06 sfrieske, replaced user_parameter_set_verify by user_paramer_change
  2019-12-09 sfrieske, minor cleanup, renamed from Test461_OperationCompleteParallel
=end

require 'SiViewTestCase'


# parallel execution of TxOpeComplete, MSRs 296657 and 713567
class MM_Eqp_OpeCompParallel < SiViewTestCase
  @@eqp = 'UTF002'
  @@opNo = '1000.500'     # Pre-1 script at next op
  @@eqp_ib = 'UTI002'
  @@opNo_ib = '1000.200'  # Pre-1 script at next op
  @@portstates = %w(LoadReq Down LoadComp UnloadReq Unknown)
  

  def self.shutdown
    [@@eqp, @@eqp_ib].each {|eqp| @@sv.eqp_cleanup(eqp)}
    @@testlots.each {|lot| @@sv.delete_lot_family(lot)}
  end


  def teardown
    # thread is dead if tests passed
    @@thread.join if self.class.class_variable_defined?(:@@thread)
  end
  

  def test00_setup
    $setup_ok = false
    #
    assert @@testlots = @@svtest.new_lots(2)
    assert_equal 0, @@sv.user_parameter_change('Lot', @@testlots[0], 'UTSleep', '15')
    #
    $setup_ok = true
  end


  # parallel opecomp etc.

  def test01_opecomp_fb
    [false, true].each {|force|
      assert_equal 0, @@sv.eqp_cleanup(@@eqp)
      @@testlots.each {|lot| assert_equal 0, @@sv.lot_cleanup(lot, opNo: @@opNo)}
      @@thread = nil
      tend2 = nil
      assert @@sv.claim_process_lot(@@testlots[0], eqp: @@eqp) {
        @@thread = Thread.new {
          @@sv.claim_process_lot(@@testlots[1], eqp: @@eqp, running_hold: force)
          tend2 = Time.now
        }
      }, "error processing 1st lot"
      assert @@thread.value, "error processing 2nd lot"
      assert tend2 < Time.now, "no parallel operation, wrong setup?"
      @@thread.join
    }
  end

  def test02_opecomp_ib
    [false, true].each {|force|
      assert_equal 0, @@sv.eqp_cleanup(@@eqp_ib)
      @@testlots.each {|lot| assert_equal 0, @@sv.lot_cleanup(lot, opNo: @@opNo_ib)}
      @@thread = nil
      tend2 = nil
      assert @@sv.claim_process_lot(@@testlots[0], eqp: @@eqp_ib) {
        @@thread = Thread.new {
          @@sv.claim_process_lot(@@testlots[1], eqp: @@eqp_ib, running_hold: force)
          tend2 = Time.now
        }
      }, "error processing 1st lot"
      assert @@thread.value, "error processing 2nd lot"
      assert tend2 < Time.now, "no parallel operation, wrong setup?"
      @@thread.join
    }
  end

  def test03_port_status_change
    assert @@svtest.cleanup_eqp(@@eqp, mode: 'Auto-1')
    @@testlots.each {|lot| assert_equal 0, @@sv.lot_cleanup(lot, opNo: @@opNo)}
    @@thread = nil
    done = false
    assert @@sv.claim_process_lot(@@testlots[0], eqp: @@eqp, mode: 'Auto-1') {
      @@thread = Thread.new {
        sleep 3
        @@sv.eqp_info(@@eqp).ports.each {|p| 
          next unless p.loaded_carrier.empty?
          @@portstates.each {|state| assert_equal 0, @@sv.eqp_port_status_change(@@eqp, p.port, state)}
        }
        done = true
      }
    }, 'error processing lot'
    assert done, 'error changing port states'
  end

  def test04_carrier
    assert @@svtest.cleanup_eqp(@@eqp, mode: 'Auto-1')
    @@testlots.each {|lot| assert_equal 0, @@sv.lot_cleanup(lot, opNo: @@opNo)}
    carrier = @@sv.lot_info(@@testlots[1]).carrier
    @@thread = nil
    done = false
    assert @@sv.claim_process_lot(@@testlots[0], eqp: @@eqp, mode: 'Auto-1') {
      @@thread = Thread.new {
        sleep 3
        p = @@sv.eqp_info(@@eqp).ports.find {|p| p.loaded_carrier.empty?}
        assert_equal 0, @@sv.eqp_port_status_change(@@eqp, p.port, 'LoadComp')
        assert_equal 0, @@sv.carrier_xfer_status_change(carrier, 'EO', @@eqp)
        assert_equal 0, @@sv.eqp_load(@@eqp, p.port, carrier, ib: false)
        assert_equal 0, @@sv.eqp_unload(@@eqp, p.port, carrier, ib: false)
        assert_equal 0, @@sv.carrier_reserve(carrier)
        assert_equal 0, @@sv.carrier_reserve_cancel(carrier)
        done = true
      }
    }, 'error processing lot'
    assert done, 'error changing port states'
  end

  # lot hold while lot is in post processing (MSR713567)

  def testman11_hold
    assert_equal 'UT-PRIV', @@sv.environment_variable[1]['SP_EXTERNAL_POSTPROC_USERGRP']
    assert_equal 0, @@sv.lot_cleanup(@@lot, opNo: @@opNo)
    assert_equal 0, @@sv.user_parameter_change('Lot', @@lot, 'UTSleep', '15')
    assert_equal 0, @@sv.eqp_cleanup(@@eqp)
    res = nil
    assert cj = @@sv.claim_process_lot(@@lot, eqp: @@eqp) {
      Thread.new {
        sleep 5
        res = @@sv.lot_hold(@@lot, nil)
      }
    }
    assert_equal 0, res, 'error executing TxHoldLot'
  end


end
