=begin 
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.
    
Author: Daniel Steger, 2014-08-28

History:
  2015-06-24 ssteidte, cleaned up
  2019-12-05 sfrieske, minor cleanup, renamed from Test010_SM
  2022-01-03 sfrieske, minor cleanup, renamed tests from test0xx to testxx
=end

require 'SiViewTestCase'

# Test TX fixes in MSR 898010
class SM_EditComplete < SiViewTestCase
  @@template = 'UTPD000.01'
  @@classname = :pd
  
  
  def test30_save_edit_complete_one_tx
    oinfos = @@sm.object_info(@@classname, @@template, raw: true)
    new_obj = "PD-#{Time.now.to_i}.01"
    oinfos[0].objectId = @@sm.oid(new_obj)
    assert @@sm.save_edit_complete(@@classname, oinfos)
    assert @@sm.release_objects(@@classname, new_obj), "failed to release #{new_obj}"
    assert @@sm.delete_production(@@classname, new_obj), "failed to delete #{new_obj}"
  end
    
  def test31_save_edit_complete_multi_tx
    oinfos = @@sm.object_info(@@classname, @@template, raw: true)
    new_obj = "PD-#{Time.now.to_i}.01"
    oinfos[0].objectId = @@sm.oid(new_obj)
    assert @@sm.save_objects(@@classname, oinfos), "failed to save #{new_obj}"
    assert @@sm.edit_complete(@@classname, new_obj), "failed to edit & complete #{new_obj}"
    assert @@sm.release_objects(@@classname, new_obj), "failed to release #{new_obj}"
    assert @@sm.delete_production(@@classname, new_obj), "failed to delete #{new_obj}"
  end
  
  def test32_copy_edit_complete_cn
    oinfos = @@sm.object_info(@@classname, @@template, raw: true)
    new_obj = "PD-#{Time.now.to_i}.01"
    oinfos.first.objectId = @@sm.oid(new_obj)
    assert cn = @@sm.create_changenotice("CN-#{Time.now.to_i}").first.objectId, 'failed to create CN'
    oinfos.first.propertyInfo.changeNotice = cn # add change notice
    assert @@sm.save_edit_complete(@@classname, oinfos), "failed to save #{new_obj}"
    assert @@sm.edit_complete(:changenotice, cn), "failed to save #{cn.identifier}"
    assert @@sm.release_objects(:changenotice, cn), "failed to release #{new_obj} with #{cn.identifier}"
    assert @@sm.delete_production(@@classname, new_obj), "failed to delete #{new_obj}"
    assert @@sm.delete_production(:changenotice, cn), "failed to delete #{cn.identifier}"
  end

end
