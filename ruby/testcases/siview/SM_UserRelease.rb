=begin 
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.
    
Author: ssteidte, 2014-05-22

History:
  2015-06-24 ssteidte, cleaned up and renamed
  2019-12-05 sfrieske, minor cleanup, renamed from Test011_SMUserRelease
=end

require 'SiViewTestCase'

# Test user releases in SM are not blocking MM for over a minute, MSRs 498805, 723386
class SM_UserRelease < SiViewTestCase
  @@eqp = 'UTF001'
  @@usernames = 'UTUSER%03d'
  @@nusers = 10
  
  
  def test11_users
    users = @@nusers.times.collect {|i| @@usernames % i}
    @@sm.delete_production(:user, users)
    tstart = Time.now
    # create new users, don't yet release
    assert @@sm.copy_object(:user, @@sm._userinfo.userId, users, release: false), 'error creating users'
    # count number of eqp_info calls during the SM release
    ncalls = 0
    nerrs = 0
    t = Thread.new {
      while true
        res = @@sv.eqp_info(@@eqp)
        if res
          ncalls += 1
        else
          nerrs += 1
        end
      end
    }
    # release users
    assert @@sm.release_objects(:user, users), 'error releasing users'
    t.kill
    duration = Time.now - tstart
    # delete users to clean up
    assert @@sm.delete_production(:user, users), 'error deleting users'
    # report
    $log.info "nusers: #{@@nusers}, duration: #{duration}, eqp_info calls succeeding: #{ncalls}, errors: #{nerrs}"
    assert_equal 0, nerrs, "some MM calls failed"
    # compare to duration without SM
    tstart = Time.now
    ncalls.times {@@sv.eqp_info(@@eqp)}
    ref = Time.now - tstart
    $log.info "duration without SM activities: #{ref} s"
    $log.info "duration with SM activities: #{duration} s"
    # verify
    assert duration < ref * 2, 'MM calls during SM release are slow'
  end
  
end
