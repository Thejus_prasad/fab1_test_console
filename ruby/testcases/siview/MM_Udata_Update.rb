=begin 
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.
    
Author: dsteger, 2014-08-08

History:
  2015-10-08 sfrieske, updated for R15
  2019-12-05 sfrieske, minor cleanup, renamed from Test021_UserData
=end

require 'SiViewTestCase'


# Test the TxUserDataUpdateReq customization, MSR 81234 and MSR 116510
class MM_Udata_Update < SiViewTestCase
  @@eqp = 'UTF002'
  @@udata = 'QAUserData'
    
  
  def self.after_all_passed
    @@sv.inhibit_cancel(:eqp, @@eqp)
  end


  def test11_update_wrong_class
    assert_equal 3203, @@sv.user_data_update(:qainvalid, 'NOTEX', {@@udata=>'123'})
  end
  
  def test12_update_wrong_object
    assert_equal 1462, @@sv.user_data_update(:eqp, 'NOTEX', {@@udata=>'123'})
  end
  
  # fails, UserDataUpdate does not perform type checks - accepted
  def testman13_update_wrong_type
    refute_equal 0, @@sv.user_data_update(:eqp, @@eqp, {'QAUserDataInt'=>'123'}, datatype: 'Integer'), 'must not accept wrong type'
    assert_nil @@sv.user_data(:inhibit, iid)['QAUserDataInt'], 'wrong udata'
  end

  def test14_update_objectid_mm
    # test with an inhibit
    assert_equal 0, @@sv.inhibit_cancel(:eqp, @@eqp)
    assert inh = @@sv.inhibit_entity(:eqp, @@eqp), 'failed to register inhibit'
    iid = inh.entityInhibitID.identifier
    # set
    assert_equal 0, @@sv.user_data_update(:inhibit, iid, {@@udata=>'123'}), 'failed to update udata'
    assert_equal '123', @@sv.user_data(:inhibit, iid)[@@udata], 'wrong udata'
    # update
    assert_equal 0, @@sv.user_data_update(:inhibit, iid, {@@udata=>'987'}), 'failed to update udata'
    assert_equal '987', @@sv.user_data(:inhibit, iid)[@@udata], 'wrong udata'
    # delete
    assert_equal 0, @@sv.user_data_update(:inhibit, iid, {@@udata=>''}, action: 'Delete'), 'failed to update udata'
    assert_nil @@sv.user_data(:inhibit, iid)[@@udata], 'wrong udata'
  end
  

  # updata UDATA already defined in SM

  def test31_wrong_originator
    assert_equal 388, @@sv.user_data_update(:eqp, @@eqp, {'CMMS'=>'SAP'}, originator: 'SM'), 'SM udata update must fail'
    assert_equal 388, @@sv.user_data_update(:eqp, @@eqp, {@@udata=>'SAP'}, originator: 'SM'), 'SM udata update must fail'
  end

  def test32_update_sm_eqp
    assert_equal 2106, @@sv.user_data_update(:eqp, @@eqp, {'CMMS'=>'SAP'}), 'SM udata update must fail'
  end

  # fails in R15 except for eqp: http://gfjira/browse/MES-2081 - accepted
  def testman33_update_sm_op
    assert_equal 2106, @@sv.user_data_update(:pd, ['UTPD000.01', 'Operation'], {'ProcessLayer'=>'No'}), 'SM udata update must fail'
  end

end
