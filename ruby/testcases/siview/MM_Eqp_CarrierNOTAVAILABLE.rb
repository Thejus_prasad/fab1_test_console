=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: sfrieske, 2017-04-07

History:
  2019-12-09 sfrieske, minor cleanup, renamed from Test152_CarrierNOTAVAILABLE
  2021-04-21 sfrieske, moved @sorter and @pg form SiView::Test to SJC::Test
=end

require 'SiViewTestCase'

# Test carrier can be loaded with purpose "Other" when NOTAVAILABLE, MES-3486
class MM_Eqp_CarrierNOTAVAILABLE < SiViewTestCase
  @@sorter = 'UTS001'

  def test10
    $setup_ok = false
    #
    assert @@sv.eqp_cleanup(@@sorter, mode: 'Auto-1')
    assert ec = @@svtest.get_empty_carrier, 'not enough empty carriers'
    assert_equal 0, @@sv.carrier_xfer_status_change(ec, 'EO', @@sorter)
    assert_equal 0, @@sv.carrier_status_change(ec, 'NOTAVAILABLE')
    assert_equal 0, @@sv.eqp_port_status_change(@@sorter, 'P1', 'LoadComp')
    assert_equal 0, @@sv.eqp_load(@@sorter, 'P1', ec, purpose: 'Other')
    #
    $setup_ok = true
  end

end
