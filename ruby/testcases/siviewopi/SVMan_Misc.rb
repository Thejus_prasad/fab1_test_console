=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author:  Steffen Frieske, 2016-09-01

Version: C7.4.0.6

History:
=end

require 'SiViewTestCase'


# Preparation for manual SiView Tests, section Misc
class SVMan_Misc < SiViewTestCase
  @@eqp_fb = 'UTC001'
  @@eqp_ib = 'UTI002'

  @@testlots = []


  def self.after_all_passed
    @@testlots.each {|lot| @@sv.delete_lot_family(lot)}
  end


  def test01_rsv
    eqp = @@eqp_fb
    assert lots = @@svtest.new_lots(2), 'error creating test lots'
    @@testlots += lots
    [@@eqp_fb, @@eqp_ib].each_with_index {|eqp, i|
      lot = lots[i]
      refute_empty res = @@sv.lot_eqplist_in_route(lot, eqp: eqp), "eqp #{eqp} not used on the lot's route"
      assert_equal 0, @@sv.lot_opelocate(lot, res.keys.first), 'error locating lot'
      assert @@svtest.cleanup_eqp(eqp, mode: 'Auto-3')
    }
    puts "\nLots #{lots} are prepared for #{[@@eqp_fb, @@eqp_ib]} resp.\nExecute the test, then press Enter to continue!"
    gets
  end

end
