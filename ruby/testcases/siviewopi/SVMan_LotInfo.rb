=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author:  Steffen Frieske, 2016-09-01

Version: C7.4.0.6

History:
=end

require 'SiViewTestCase'


# Preparation for manual SiView Tests, section LotInfo
class SVMan_LotInfo < SiViewTestCase
  @@backup_opNo = '1000.200'
  @@nprobank = 'O-BACKUPOPER'
  # F8 is the source fab!
  @@svsrc_defaults = {product: 'UT-PRODUCT-FAB1.01', route: 'UTRT101.01', customer: 'qgt', carriers: '%'}
  @@src_env = 'f8stag'
  @@nprobank_return = 'W-BACKUPOPER'

  @@testlots_bop = []


  def self.after_all_passed
    @@testlots_bop.each {|lot| @@sv_src.backup_delete_lot_family(lot, @@sv)}
  end


  def test00_setup
    @@svtest_src = SiView::Test.new(@@src_env, @@svsrc_defaults)
    @@sv_src = @@svtest_src.sv
    # @@params = {return_bank: @@nprobank_return, backup_bank: @@nprobank, backup_opNo: @@backup_opNo}
  end

  def test105
    assert lot = @@svtest_src.new_lot, 'error creating test lot'
    @@testlots_bop << lot
    # send lot to backup and return it
    assert @@sv_src.backup_prepare_send_receive(lot, @@sv, backup_bank: @@nprobank, backup_opNo: @@backup_opNo)
    assert @@sv.backup_prepare_return_receive(lot, @@sv_src, return_bank: @@nprobank_return)
    # assert @@sv_src.do_at_backup_site(lot, @@sv, @@params) {true}
    assert_equal 0, @@sv_src.lot_opelocate(lot, :last)
    assert_equal 'COMPLETED', @@sv_src.lot_info(lot).status
    puts "\nLot #{lot} is prepared.\nPress Enter to continue!"
    gets
  end
end
