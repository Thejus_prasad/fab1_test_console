=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author:  Steffen Frieske, 2016-09-01

Version: C7.4.0.6

History:
  2021-09-15 sfrieske, finally replaced $svtest by @@svtest, $sv by @@sv and $sm by @@sm
=end

require 'SiViewTestCase'


# Preparation for manual SiView Tests, section Backup Operation
class SVMan_Backup < SiViewTestCase
  # Fab1 is the source Fab
  @@svsrc_defaults = {product: 'UT-PRODUCT-FAB8.01', route: 'UTRT101.01', customer: 'qgt'}
  @@backup_env = 'let'  #'f8stag'
  @@nprobank = 'W-BACKUPOPER'
  @@nprobank_return = 'O-BACKUPOPER'
  @@comment = 'QA Testing'
  @@backup_opNo = '1000.200'


  def self.startup
    ##super
    @@testlots = []
    @@svtest_src = SiView::Test.new($env, @@svsrc_defaults)
    @@sv_src = @@svtest_src.sv
    ##@@svtest_bak = SiView::Test.new(@@backup_env)
    @@sv_back = SiView::MM.new(@@backup_env)
  end

  def self.after_all_passed
    @@testlots.each {|lot| @@sv.delete_lot_family(lot)}
    @@testlots.each {|lot| @@sv_src.backup_delete_lot_family(lot, @@sv_back)}
  end

  def test01
    assert_equal '0', @@sv_back.environment_variable[1]['CS_SP_INVALID_BKVER'], "wrong env"
    assert lot = @@svtest_src.new_lot, "error creating test lot"
    @@testlots << lot
    # prepare lot for backup operation
    assert_equal 0, @@sv_src.lot_opelocate(lot, @@backup_opNo)
    assert_equal 0, @@sv_src.lot_nonprobankin(lot, @@nprobank)
    assert_equal 0, @@sv_src.wafer_sort(lot, '')
    puts "\nTC01:\nLot #{lot} is prepared for Backup Send tests.\nPress Enter to prepare the lot for Split at Backup Site!"
    gets
    assert lc = @@sv_back.lot_split(lot, 2)
    puts "\nTC02:\nLot #{lot} is split.\nPress Enter to merge and prepare lot for Backup Return tests!"
    gets


    assert @@sv.merge_lot_family(lot, nosort: true), 'error merging lot'
    # assert_equal 0, @@sv_back.lot_merge(lot, nil), 'error merging lot'
    ##assert_equal 0, @@sv_back.wafer_sort(lc, '')
    assert @@sv_back.claim_process_lot(lot), 'error processing lot'
    assert_equal 0, @@sv_back.lot_nonprobankin(lot, @@nprobank_return)
    assert_equal 0, @@sv_back.wafer_sort(lot, '')
    puts "\nTC03\nLot #{lot} is ready for Return tests.\nPress Enter to continue with the preparation for Lot Completion tests!."
    gets
    assert @@sv_src.backup_prepare_send_receive(lot, @@sv_back, backup_opNo: @@backup_opNo, backup_bank: @@nprobank)
    assert_equal 0, @@sv_back.lot_opelocate(lot, :last)
    puts "\nLot #{lot} is ready for Completion tests.\nPress ENTER to continue with cleanup!"
    gets
  end

end
