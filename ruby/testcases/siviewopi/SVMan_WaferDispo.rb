=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author:  Steffen Frieske, 2016-09-01

Version: C7.5.0.6

History:
  2019-04-08 sfrieske, added EIProcessState
=end

require 'SiViewTestCase'


# Preparation for manual SiView Tests, section Wafer Dispo
class SVMan_WaferDispo < SiViewTestCase
  @@testlots = []


  def self.after_all_passed
    @@testlots.each {|lot| @@sv.delete_lot_family(lot)}
  end

  def test01
    assert_equal '0', @@sv.environment_variable(var: 'CS_SP_FCHL_DELETE_SP'), 'wrong MM env setting'
    assert lot = @@svtest.new_lot, 'error creating test lot'
    @@testlots << lot
    # get value to append to process state
    li = @@sv.lot_info(lot, wafers: true)
    pass = @@sv.lot_operation_list(lot).first.pass  # passcount is 1 for a fresh lot
    v = "#{li.op}_#{li.route}_#{li.opNo}_#{pass}"
    # process lot with running hold (FCHL)
    assert @@sv.claim_process_lot(lot, running_hold: true)
    # set wafer script params
    li.wafers.each_with_index {|w, i|
      if i < 9
        assert_equal 0, @@sv.user_parameter_change('Wafer', w.wafer, 'EIProcessState', "Processed_#{v}")
      elsif i < 17
        assert_equal 0, @@sv.user_parameter_change('Wafer', w.wafer, 'EIProcessState', "InProcessing_#{v}")
      else
        assert_equal 0, @@sv.user_parameter_change('Wafer', w.wafer, 'EIProcessState', "Unprocessed_#{v}")
      end
    }
    #
    puts "\n-> Lot #{lot} is prepared for the Wafer Dispo Screen test.\nPress Enter to continue!"
    gets
    #
    $log.info "verifying wafer script parameters have not been changed"
    li.wafers.each_with_index {|w, i|
      if i < 9
        assert_equal "Processed_#{v}", @@sv.user_parameter('Wafer', w.wafer, name: 'EIProcessState').value, 'wrong wafer script param'
      elsif i < 17
        assert_equal "InProcessing_#{v}", @@sv.user_parameter('Wafer', w.wafer, name: 'EIProcessState').value, 'wrong wafer script param'
      else
        assert_equal "Unprocessed_#{v}", @@sv.user_parameter('Wafer', w.wafer, name: 'EIProcessState').value, 'wrong wafer script param'
      end
    }
  end

end
