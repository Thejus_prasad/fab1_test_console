=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: Conny Stenzel, 2019-01-10

History:

Notes:
  Resp. PD setup see http://....
=end

require 'SiViewTestCase'
# require 'rtdserver/rtdemuqedata'
require 'sil/siltest'


class TDG_Inhibits < SiViewTestCase
  @@mpd = 'QA-MFC-MEAS.01'
  #@@sv_defaults = {route: 'MFC-0001.01', product: 'MFCPROD.01', carriers: 'UM%'}
  @@siltest_params = {
    sil_timeout: 900, ca_timeout: 240, notification_timeout: 120,
    parameter_template: '_Template_QA_PARAMS_900',
    mpd: 'QA-MFC-MEAS.01',
    mtool: 'UTFMFC01',
    ptool: 'UTCMFC01',
    route: 'MFC-0001.01',
    product: 'MFCPROD.01',
    carriers: 'UM%',
    carrier: 'UM%',
    department: 'MSS'
  }
  @@templates_folder = '_TEMPLATES'
  @@default_template = '_DEFAULT_TEMPLATE'
  @@default_template_qa = 'DEFAULT_QA_TEMPLATE'
  @@autocreated_qa = 'AutoCreated_QAMFC'
  @@testlots = []

  def self.startup
    super
    # @@tdg = RTD::TDG.new($env, sv: @@sv)
    @@siltest = SIL::Test.new($env, @@siltest_params.merge(sv: @@sv))
    @@lds = @@siltest.lds_inline
    @@lds_setup = @@siltest.lds_setup
    # often used variables
    @@params_template = @@siltest.parameter_template
  end

  def self.shutdown
    @@testlots.each {|lot| @@sv.delete_lot_family(lot)}
  end


  def test00_setup
    $setup_ok = false
    #
    assert lots = @@svtest.new_lots(1, @@siltest_params), 'error creating lots'
    @@testlots += lots
    #
    parameters = ['QA_MFCPAR_900', 'QA_MFCPAR_901', 'QA_MFCPAR_902', 'QA_MFCPAR_903', 'QA_MFCPAR_904']
    assert @@siltest.set_defaults(@@siltest_params.merge(parameters: parameters, lot: lots[0])), "no PPD for MPD #{@@siltest.mpd} ?"
    #
    $setup_ok = true
  end

  def test11_inhibit_eqp_spci
    assert_equal 0, @@sv.eqp_cleanup(@@siltest.ptool)
    assert channels = @@siltest.setup_channels(cas: ['AutoEquipmentInhibit'])
    assert @@siltest.submit_process_dcr
    # send meas DCR with OOC values and verify inhibit is set
    assert @@siltest.submit_meas_dcr(ooc: true)
    refute_empty @@sv.inhibit_list(:eqp, @@siltest.ptool)
    puts "\n--> Please check for data in MFC now and press Enter to continue.\n"
    gets
    assert_equal 0, @@sv.eqp_cleanup(@@siltest.ptool)
  end

  def test12_inhibit_eqp_spcs
    assert_equal 0, @@sv.eqp_cleanup(@@siltest.ptool)
    channels = @@siltest.setup_channels(cas: ['AutoEquipmentInhibit'], technology: 'TEST')
    dcrp = @@siltest.submit_process_dcr(technology: 'TEST')
    # send meas DCR with OOC values and verify inhibit is set
    dcr = @@siltest.submit_meas_dcr(ooc: true, technology: 'TEST')
    refute_empty @@sv.inhibit_list(:eqp, @@siltest.ptool)
    puts "\n--> Please check for data in MFC now and press Enter to continue.\n"
    gets
    assert_equal 0, @@sv.eqp_cleanup(@@siltest.ptool)
  end

  def test20_inhibit_eqp_fdc
    eqp = 'UTCMFC03'
    reason = 'XFDC'
    memo = "QA #{reason} FDCInhibit for equipment #{eqp}"
    owner = 'X-CATALYST'
    assert_equal 0, @@sv.eqp_cleanup(eqp)
    assert_equal 0, @@sv.inhibit_entity(:eqp, eqp, reason: reason, memo: memo, owner: owner), 'error setting entity inhibit'
    puts "\n--> Please check for data in MFC now and press Enter to continue.\n"
    gets
    assert_equal 0, @@sv.eqp_cleanup(eqp)
  end

  def test21_inhibit_eqpchamber_fdc
    eqp = 'UTCMFC03'
    ch = 'PM1'
    reason = 'XFDC'
    memo = "QA #{reason} FDCInhibit for equipment #{eqp} and chamber #{ch}"
    owner = 'X-CATALYST'
    assert_equal 0, @@sv.eqp_cleanup(eqp)
    assert_equal 0, @@sv.inhibit_entity(:eqp_chamber, eqp, attrib: ch, reason: reason, memo: memo, owner: owner), 'error setting entity inhibit'
    puts "\n--> Please check for data in MFC now and press Enter to continue.\n"
    gets
    assert_equal 0, @@sv.eqp_cleanup(eqp)
  end

  def test30_inhibit_eqp_otherx_apc
    eqp = 'UTCMFC04'
    reason = 'XAPC'
    memo = "QA #{reason} OtherXInhibit for equipment #{eqp}"
    owner = 'X-CATALYST'
    assert_equal 0, @@sv.eqp_cleanup(eqp)
    assert_equal 0, @@sv.inhibit_entity(:eqp, eqp, reason: reason, memo: memo, owner: owner), 'error setting entity inhibit'
    puts "\n--> Please check for data in MFC now and press Enter to continue.\n"
    gets
    assert_equal 0, @@sv.eqp_cleanup(eqp)
  end

  def test31_inhibit_eqpchamber_otherx_apc
    eqp = 'UTCMFC04'
    ch = 'PM1'
    reason = 'XAPC'
    memo = "QA #{reason} OtherXInhibit for equipment #{eqp} and chamber #{ch}"
    owner = 'X-CATALYST'
    assert_equal 0, @@sv.eqp_cleanup(eqp)
    assert_equal 0, @@sv.inhibit_entity(:eqp_chamber, eqp, attrib: ch, reason: reason, memo: memo, owner: owner), 'error setting entity inhibit'
    puts "\n--> Please check for data in MFC now and press Enter to continue.\n"
    gets
    assert_equal 0, @@sv.eqp_cleanup(eqp)
  end

  def test32_inhibit_eqp_otherx_lg
    eqp = 'UTCMFC04'
    reason = 'X-LG'
    memo = "QA #{reason} OtherXInhibit for equipment #{eqp}"
    owner = 'X-CATALYST'
    assert_equal 0, @@sv.eqp_cleanup(eqp)
    assert_equal 0, @@sv.inhibit_entity(:eqp, eqp, reason: reason, memo: memo, owner: owner), 'error setting entity inhibit'
    puts "\n--> Please check for data in MFC now and press Enter to continue.\n"
    gets
    assert_equal 0, @@sv.eqp_cleanup(eqp)
  end

  def test33_inhibit_eqpchamber_otherx_lg
    eqp = 'UTCMFC04'
    ch = 'PM1'
    reason = 'X-LG'
    memo = "QA #{reason} OtherXInhibit for equipment #{eqp} and chamber #{ch}"
    owner = 'X-CATALYST'
    assert_equal 0, @@sv.eqp_cleanup(eqp)
    assert_equal 0, @@sv.inhibit_entity(:eqp_chamber, eqp, attrib: ch, reason: reason, memo: memo, owner: owner), 'error setting entity inhibit'
    puts "\n--> Please check for data in MFC now and press Enter to continue.\n"
    gets
    assert_equal 0, @@sv.eqp_cleanup(eqp)
  end

  def test34_inhibit_reticle_otherx_rd
    reticle = 'QAMFCReticle0001'
    reason = 'X-RD'
    memo = "QA #{reason} OtherXInhibit for reticle #{reticle}"
    owner = 'X-CATALYST'
    assert_equal 0, @@sv.inhibit_cancel(:reticle, reticle)
    assert_equal 0, @@sv.inhibit_entity(:reticle, reticle, reason: reason, memo: memo, owner: owner), 'error setting entity inhibit'
    puts "\n--> Please check for data in MFC now and press Enter to continue.\n"
    gets
    assert_equal 0, @@sv.inhibit_cancel(:reticle, reticle)
  end

  def test35_inhibit_eqpmachinerecipe_otherx_rs
    eqp = 'UTCMFC04'
    assert recipe = @@sv.machine_recipes.sample
    reason = 'X-RS'
    memo = "QA #{reason} OtherXInhibit for equipment #{eqp} and recipe #{recipe}"
    owner = 'X-CATALYST'
    assert_equal 0, @@sv.inhibit_cancel(:eqp_recipe, [eqp, recipe])
    assert_equal 0, @@sv.inhibit_entity(:eqp_recipe, [eqp, recipe], reason: reason, memo: memo, owner: owner), 'error setting entity inhibit'
    puts "\n--> Please check for data in MFC now and press Enter to continue.\n"
    gets
    assert_equal 0, @@sv.inhibit_cancel(:eqp_recipe, [eqp, recipe])
  end

  def test38_inhibit_eqp_otherx_sec
    eqp = 'UTCMFC04'
    reason = 'XSEC'
    memo = "QA #{reason} OtherXInhibit for equipment #{eqp}"
    owner = 'X-CATALYST'
    assert_equal 0, @@sv.eqp_cleanup(eqp)
    assert_equal 0, @@sv.inhibit_entity(:eqp, eqp, reason: reason, memo: memo, owner: owner), 'error setting entity inhibit'
    puts "\n--> Please check for data in MFC now and press Enter to continue.\n"
    gets
  end

  def test39_inhibit_eqpchamber_otherx_sec
    eqp = 'UTCMFC04'
    ch = 'PM1'
    reason = 'XSEC'
    memo = "QA #{reason} OtherXInhibit for equipment #{eqp} and chamber #{ch}"
    owner = 'X-CATALYST'
    assert_equal 0, @@sv.eqp_cleanup(eqp)
    assert_equal 0, @@sv.inhibit_entity(:eqp_chamber, eqp, attrib: ch, reason: reason, memo: memo, owner: owner), 'error setting entity inhibit'
    puts "\n--> Please check for data in MFC now and press Enter to continue.\n"
    gets
  end

end
