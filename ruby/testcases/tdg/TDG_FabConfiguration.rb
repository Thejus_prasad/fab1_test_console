=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: Conny Stenzel, 2019-09-06

History:
  2019-09-06 cstenzel initial development
=end

require 'SiViewTestCase'
require 'rtdserver/rtdemuqedata'


class TDG_FabConfiguration < SiViewTestCase
  @@dryrun = false
    
  def self.startup
    super
    @@tdg = RTD::TDG.new($env)
  end

  def teardown
    @@tdg.remove_data()
  end

  def self.shutdown
    @@tdg.remove_data()
  end

  
  def test11_fabconfiguration_tasks
    tasktypeshort = 'FCG'
    tasks = []
    @@tdg.taskcategories[tasktypeshort].each {|tt|
      @@tdg.tasktypespecs[tt].each {|tts|
        task = @@tdg.prepare_fabconfiguration_task(tt, tts, tasktypeshort)
        tasks << task
      }
    }
    manual_mfc_check(tasks, tasktypeshort, dryrun: @@dryrun, method: 'file')
  end

  def manual_mfc_check(tasks, tasktypeshort, params={})
    $log.info "manual_mfc_check params: #{params}"
    method = params[:method] || 'once'
    dryrun = params[:dryrun] || @@dryrun
    params.merge!({method: method, dryrun: dryrun})
    @@tdg.push_data(tasks, tasktypeshort, params)
    puts "\n--> Please check for data in MFC now and press Enter to continue.\n"
    gets
  end

end
