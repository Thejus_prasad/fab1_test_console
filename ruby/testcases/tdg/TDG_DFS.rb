=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: Conny Stenzel, 2019-09-06

History:
  2019-09-06 cstenzel initial development
=end

require 'SiViewTestCase'
require 'rtdserver/rtdemuqedata'


class TDG_DFS < SiViewTestCase
  @@dryrun = false
    
  def self.startup
    super
    @@tdg = RTD::TDG.new($env)
  end

  def teardown
    @@tdg.remove_data()
  end

  def self.shutdown
    @@tdg.remove_data()
  end

  
  def test11_dfs_tasks
    tasktypeshort = 'DFS'
    tasks = []
    dfscomments = []
    dfswafers = []
    @@tdg.taskcategories[tasktypeshort].each {|tt|
      @@tdg.tasktypespecs[tt].each {|tts|
        task = @@tdg.prepare_dfs_task(tt, tts, tasktypeshort)
        tasks << task
        dfscomment = "DFS_COMMENTS|#{task[:task_id]}|QA DFS Comment UR25574.000|#{task[:start_time]}|qauser\n"  # 2020-08-06 - aschmid3: Added lot ID (MSR1542222)
        dfscomments << dfscomment
        dfswafer = "DFS_WAFERS|#{task[:task_id]}|UR25574.000|PHUBO015MX|38399054|0|#{task[:start_time]}\n"
        dfswafers << dfswafer
      }
    }
    manual_mfc_check(tasks, tasktypeshort, dryrun: @@dryrun, method: 'file', dfscomments: dfscomments, dfswafers: dfswafers)
  end

  def manual_mfc_check(tasks, tasktypeshort, params={})
    $log.info "manual_mfc_check params: #{params}"
    method = params[:method] || 'once'
    dryrun = params[:dryrun] || @@dryrun
    params.merge!({method: method, dryrun: dryrun})
    @@tdg.push_data(tasks, tasktypeshort, params)
    puts "\n--> Please check for data in MFC now and press Enter to continue.\n"
    gets
  end

end
