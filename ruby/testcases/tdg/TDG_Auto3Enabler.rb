=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: Conny Stenzel, 2019-09-06

History:
  2019-09-06 cstenzel initial development
  2020-07-14 aschmid3 added 'test12'
=end

require 'SiViewTestCase'
require 'rtdserver/rtdemuqedata'


class TDG_Auto3Enabler < SiViewTestCase
  @@dryrun = false
  @@testlots = []
    
  def self.startup
    super
    @@tdg = RTD::TDG.new($env)
  end

  def teardown
    @@tdg.remove_data()
    @@testlots.each {|lot| @@sv.delete_lot_family(lot)}
  end

  def self.shutdown
    @@tdg.remove_data()
    @@testlots.each {|lot| @@sv.delete_lot_family(lot)}
  end

  
  def test11_auto3enabler_tasks
    tasktypeshort = 'A3E'
    tasks = []
    eqps = ['UTCMFC01']
    lotcount = 0
    @@tdg.taskcategories[tasktypeshort].each {|tt| lotcount = lotcount + @@tdg.tasktypespecs[tt].size}
    assert lots = @@svtest.new_lots(lotcount, carrier: 'UM%')
    @@testlots += lots
    @@tdg.taskcategories[tasktypeshort].each {|tt|
      @@tdg.tasktypespecs[tt].each_with_index {|tts, idx|
        eqps.each {|eqp|
          eqpi = @@sv.eqp_info(eqp)
          task = @@tdg.prepare_auto3enabler_task([lots[idx]], eqpi, tt, tts, tasktypeshort)
          tasks << task
        }
      }
    }
    #
    manual_mfc_check(tasks, tasktypeshort, dryrun: @@dryrun, method: 'file')
  end

  def test12_auto3enabler_multiple_lots   # 2020-07-30 - aschmid3/sfrieske (MSR1520316 - LON)
    tasktypeshort = 'A3E'
    tt = 'Auto-3 Enabler'
    assert tts = @@tdg.tasktypespecs[tt].first, 'wrong task type definition'
    eqp = 'UTCMFC01'
    eqpi = @@sv.eqp_info(eqp)
    assert lots = @@svtest.new_lots(2, carrier: 'UM%')
    @@testlots += lots
    assert task = @@tdg.prepare_auto3enabler_task(lots, eqpi, tt, tts, tasktypeshort)
    #
    lldat = lots.collect {|lot|
      li = @@sv.lot_info(lot)
      "LOT_LIST_DATA|#{task[:task_id]}|#{lot}|#{li.op}|#{li.route}|#{li.opNo}|1\n"
    }
    manual_mfc_check([task], tasktypeshort, lotlistdata: lldat, dryrun: @@dryrun, method: 'file')
  end

  # derived from test11
  def test13_auto3enabler_tasks_multiple_EqpDisplay   # 2020-08-06 - aschmid3 (MSR1559316/MSR1650641)
    tasktypeshort = 'A3E'
    tasks = []
    eqps = ['UTCMFC01', 'UTCMFC02']
    lotcount = 0
    @@tdg.taskcategories[tasktypeshort].each {|tt| lotcount = lotcount + @@tdg.tasktypespecs[tt].size}
    assert lots = @@svtest.new_lots(lotcount, carrier: 'UM%')
    @@testlots += lots
    @@tdg.taskcategories[tasktypeshort].each {|tt|
      @@tdg.tasktypespecs[tt].each_with_index {|tts, idx|
        eqps.each {|eqp|
          eqpi = @@sv.eqp_info(eqp)
          task = @@tdg.prepare_auto3enabler_task([lots[idx]], eqpi, tt, tts, tasktypeshort)
          task.merge!({eqp_id: "#{eqp}",})  # new row compared to test11
          tasks << task
        }
      }
    }
    #
    manual_mfc_check(tasks, tasktypeshort, dryrun: @@dryrun, method: 'file')
  end

  def manual_mfc_check(tasks, tasktypeshort, params={})
    $log.info "manual_mfc_check params: #{params}"
    method = params[:method] || 'once'
    dryrun = params[:dryrun] || @@dryrun
    params.merge!({method: method, dryrun: dryrun})
    @@tdg.push_data(tasks, tasktypeshort, params)
    puts "\n--> Please check for data in MFC now and press Enter to continue.\n"
    gets
  end

end
