=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: Conny Stenzel, 2019-09-04

History:
  2019-09-04 cstenzel initial development
=end

require 'SiViewTestCase'
require 'rtdserver/rtdemuqedata'


class TDG_BlockedLots < SiViewTestCase
  @@dryrun = false
  @@testlots = []
    
  def self.startup
    super
    @@tdg = RTD::TDG.new($env)
  end

  def teardown
    @@tdg.remove_data()
    @@testlots.each {|lot| @@sv.delete_lot_family(lot)}
  end

  def self.shutdown
    @@tdg.remove_data()
    @@testlots.each {|lot| @@sv.delete_lot_family(lot)}
  end
  
  def test11_blockedlot_tasks
    tasktypeshort = 'BLOC'
    tasks = []
    lotcount = 0
    @@tdg.taskcategories[tasktypeshort].each {|tt| lotcount = lotcount + @@tdg.tasktypespecs[tt].size}
    assert lots = @@svtest.new_lots(lotcount, carrier: 'UM%')
    @@testlots += lots
    assert lis = @@sv.lots_info(lots)
    @@tdg.taskcategories[tasktypeshort].each {|tt|
      @@tdg.tasktypespecs[tt].each_with_index {|tts, idx|
        task = @@tdg.prepare_blockedlot_task(lis[idx], tt, tts, tasktypeshort)
        tasks << task
      }
    }
    manual_mfc_check(tasks, tasktypeshort, dryrun: @@dryrun, method: 'file')
  end

  def test12_blockedlot_tasks_withTargetEQP   # 2020-08-06 - aschmid3
    tasktypeshort = 'BLOC'
    tasks = []
    lotcount = 0
    @@tdg.taskcategories[tasktypeshort].each {|tt| lotcount = lotcount + @@tdg.tasktypespecs[tt].size}
    assert lots = @@svtest.new_lots(lotcount, carrier: 'UM%')
    @@testlots += lots
    assert lis = @@sv.lots_info(lots)
    @@tdg.taskcategories[tasktypeshort].each {|tt|
      @@tdg.tasktypespecs[tt].each_with_index {|tts, idx|
        task = @@tdg.prepare_blockedlot_task(lis[idx], tt, tts, tasktypeshort)
        # task.merge!({lot_block_target_eqp_list: "#{lis[idx].opEqps}",})  # Display is available but with '[] and "" at each tool'
        task.merge!({
          lot_block_target_eqp_list: 'UTC001, UTC003',  # Eqp display directly per ID (MSR1559316/MSR1650641)
          task_add_info: "QA test with Add. Info display of lot ID #{lis[idx].lot}",  # Click-to-Copy fct (MSR1542222)
          gantt_eqp_list: 'UTC001',   # Display of Gantt Chart
        })  # Eqp display directly per ID
        tasks << task
      }
    }
    manual_mfc_check(tasks, tasktypeshort, dryrun: @@dryrun, method: 'file')
  end

  def manual_mfc_check(tasks, tasktypeshort, params={})
    $log.info "manual_mfc_check params: #{params}"
    method = params[:method] || 'once'
    dryrun = params[:dryrun] || @@dryrun
    params.merge!({method: method, dryrun: dryrun})
    @@tdg.push_data(tasks, tasktypeshort, params)
    puts "\n--> Please check for data in MFC now and press Enter to continue.\n"
    gets
  end

end
