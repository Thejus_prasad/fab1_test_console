=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: Conny Stenzel, 2019-09-06

History:
  2019-09-06 cstenzel initial development
  2020-11-03 aschmid3 added 'test12', test13
=end

require 'SiViewTestCase'
require 'rtdserver/rtdemuqedata'


class TDG_ToolAlarms < SiViewTestCase
  @@dryrun = false
    
  def self.startup
    super
    @@tdg = RTD::TDG.new($env)
  end

  def teardown
    @@tdg.remove_data()
  end

  def self.shutdown
    @@tdg.remove_data()
  end

  
  def test11_toolalarm_tasks
    tasktypeshort = 'Alarm'
    tasks = []
    eqps = ['UTCMFC01']
    chambers = ['']
    @@tdg.taskcategories[tasktypeshort].each {|tt|
      @@tdg.tasktypespecs[tt].each {|tts|
        eqps.each {|eqp|
          eqpi = @@sv.eqp_info(eqp)
          chambers.each {|ch|
            task = @@tdg.prepare_toolalarm_task(eqpi, ch, tt, tts, tasktypeshort)
            $log.info "Task: #{task}"
            tasks << task
          }
        }
      }
    }
    manual_mfc_check(tasks, tasktypeshort, dryrun: @@dryrun, method: 'file')
  end

  # derived from test11
  # row 'Description' + row 'Additional Information' with short text passage
  def test12_toolalarm_tasks_shortAI  
    tasktypeshort = 'Alarm'
    tasks = []
    eqps = ['UTCMFC01']
    chambers = ['']
    lot = 'UXYV14150.006'
    addinfo = ["Fab1QA test - Short text passage for testing of different topics: <br>
                - MSR...    : Valid spec ID named - hyperlink to setup.FC C08-00001052 <br>
                - MSR1542222: Click-to-copy-functionality for lot ID #{lot} at row 'Description' <br>
                - MSR1661399: Possibility for using of links for text display"]                
    @@tdg.taskcategories[tasktypeshort].each {|tt|
      @@tdg.tasktypespecs[tt].each {|tts|
        eqps.each {|eqp|
          eqpi = @@sv.eqp_info(eqp)
          chambers.each {|ch|
            task = @@tdg.prepare_toolalarm_task(eqpi, ch, tt, tts, tasktypeshort)
            # new 'task.merge'-rows compared to test11
            task.merge!({
              ta_alarm_description: "Sequencer has detected a deadlock - affected lot ID #{lot}",
              task_add_info: "#{addinfo}"
            })  
            $log.info "Task: #{task}"
            tasks << task
          }
        }
      }
    }
    manual_mfc_check(tasks, tasktypeshort, dryrun: @@dryrun, method: 'file')
  end

  # derived from test12
  # row 'Description' + row 'Additional Information' with long text passage
  def test13_toolalarm_tasks_longAI  
    tasktypeshort = 'Alarm'
    tasks = []
    eqps = ['UTCMFC01']
    chambers = ['']
    lot = 'UXYV14150.006'
    addinfo = ["Fab1QA test - Large text passage for testing of different topics: <br>
                - MSR...    : Valid spec ID named - hyperlink to setup.FC C08-00001052 <br>
                xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx <br>
                - MSR1542222: Click-to-copy-functionality for lot ID #{lot} at row 'Description' <br>
                xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx <br>
                - MSR1661399: Possibility for using of links for text display <br>
                xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx <br>
                - MSR...    : test test test test test test"]
    @@tdg.taskcategories[tasktypeshort].each {|tt|
      @@tdg.tasktypespecs[tt].each {|tts|
        eqps.each {|eqp|
          eqpi = @@sv.eqp_info(eqp)
          chambers.each {|ch|
            task = @@tdg.prepare_toolalarm_task(eqpi, ch, tt, tts, tasktypeshort)
            # new 'task.merge'-rows compared to test11
            task.merge!({
              ta_alarm_description: "Sequencer has detected a deadlock - affected lot ID #{lot}",
              task_add_info: "#{addinfo}"
            })  
            $log.info "Task: #{task}"
            tasks << task
          }
        }
      }
    }
    manual_mfc_check(tasks, tasktypeshort, dryrun: @@dryrun, method: 'file')
  end  

  def manual_mfc_check(tasks, tasktypeshort, params={})
    $log.info "manual_mfc_check params: #{params}"
    method = params[:method] || 'once'
    dryrun = params[:dryrun] || @@dryrun
    params.merge!({method: method, dryrun: dryrun})
    @@tdg.push_data(tasks, tasktypeshort, params)
    puts "\n--> Please check for data in MFC now and press Enter to continue.\n"
    gets
  end

end
