=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: Conny Stenzel, 2019-01-10

History:
  2019-01-10 cstenzel initial development
=end

require 'SiViewTestCase'
require 'rtdserver/rtdemuqedata'
require 'sil/siltest'


# Manufactoring Cockpit tests - all tasks
class MFC_All < SiViewTestCase
  @@mpd = 'QA-MFC-MEAS.01'
  #@@sv_defaults = {route: 'MFC-0001.01', product: 'MFCPROD.01', carriers: 'UM%'}
  @@siltest_params = {
    sil_timeout: 900, ca_timeout: 240, notification_timeout: 120,
    parameter_template: '_Template_QA_PARAMS_900',
    mpd: 'QA-MFC-MEAS.01',
    mtool: 'UTFMFC01',
    ptool: 'UTCMFC01',
    route: 'MFC-0001.01',
    product: 'MFCPROD.01',
    carriers: 'UM%',
    carrier: 'UM%',
    department: 'MSS'
  }
  @@templates_folder = '_TEMPLATES'
  @@default_template = '_DEFAULT_TEMPLATE'
  @@default_template_qa = 'DEFAULT_QA_TEMPLATE'
  @@autocreated_qa = 'AutoCreated_QAMFC'
  @@testlots = []

  @@dryrun = false

  def self.startup
    super
    @@tdg = RTD::TDG.new($env)
    @@siltest = SIL::Test.new($env, @@siltest_params.merge(sv: @@sv))
    @@lds = @@siltest.lds_inline
    @@lds_setup = @@siltest.lds_setup
    # often used variables
    @@params_template = @@siltest.parameter_template
  end

  def self.shutdown
    @@testlots.each {|lot| @@sv.delete_lot_family(lot)}
  end


  def test00_setup
    $setup_ok = false
    #
    assert lots = @@svtest.new_lots(1, @@siltest_params), 'error creating lots'
    @@testlots += lots
    #
    parameters = ['QA_MFCPAR_900', 'QA_MFCPAR_901', 'QA_MFCPAR_902', 'QA_MFCPAR_903', 'QA_MFCPAR_904']
    assert @@siltest.set_defaults(@@siltest_params.merge(parameters: parameters, lot: lots[0])), "no PPD for MPD #{@@siltest.mpd} ?"
    #
    $setup_ok = true
  end

  ### Lot Hold tasks ###
  def test11_lothold_tasks
    tasktypeshort = 'HLD'
    tasks = []
    @@tdg.taskcategories[tasktypeshort].each {|tt|
      @@tdg.tasktypespecs[tt].each {|tts|
        reason = tts[:reason]
        lots = []
        if tt.include?('TW')
          2.times {lots << @@svtest.new_controllot('Equipment Monitor', carrier: 'UM%')}
        else
          lots = @@svtest.new_lots(2, carrier: 'UM%')
        end
        @@testlots += lots
        lots.each_with_index {|lot, i|
          memo = "QA hold for MFC Test TDG_01:test11; tasktype: #{tt}, taskdescription: #{tts[:task_description]}"
          if i == 0
            assert_equal 0, @@sv.lot_hold(lot, reason, memo: memo, rsp_mark: 'C'), 'error setting lot hold'
            li = @@sv.lot_info(lot)
            lhi = @@sv.lot_hold_list(lot, reason: reason).first
          else
            if tt.include?('TW')
              assert_equal 0, @@sv.lot_opelocate(lot, '100.300')
              assert_equal 0, @@sv.lot_gatepass(lot)
            else
              2.times {assert_equal 0, @@sv.lot_gatepass(lot)}
            end
            assert_equal 0, @@sv.lot_hold(lot, reason, memo: memo, rsp_mark: 'P'), 'error setting lot hold'
            li = @@sv.lot_info(lot)
            lhi = @@sv.lot_hold_list(lot, reason: reason).first
          end
          task = @@tdg.prepare_lothold_task(lot, reason, tt, tts, tasktypeshort, memo, li, lhi, @@sv)
          $log.info "Task: #{task}"
          tasks << task
        }
      }
    }
    @@tdg.push_data(tasks, tasktypeshort: tasktypeshort, dryrun: @@dryrun)
    $log.info "Used lots: #{@@testlots}"
  end

  def testman19_lothold_tasks_load
    576.times {|count|
      tasktypeshort = 'HLD'
      tasks = []
      @@tdg.taskcategories[tasktypeshort].each {|tt|
        @@tdg.tasktypespecs[tt].each {|tts|
          reason = tts[:reason]
          lots = []
          memo = ''
          if tt.include?('TW')
            2.times {lots << @@svtest.new_controllot('Equipment Monitor', carrier: 'UM%')}
          else
            lots = @@svtest.new_lots(2, carrier: 'UM%')
          end
          @@testlots += lots
          lots.each_with_index {|lot, i|
            if i == 0
              rsp_mark = 'C'
              memo = "QA hold for MFC Test TDG_01:test20:run #{count}; responsible mark: #{rsp_mark} tasktype: #{tt}, taskdescription: #{tts[:task_description]}"
              assert_equal 0, @@sv.lot_hold(lot, reason, memo: memo, rsp_mark: rsp_mark), 'error setting lot hold'
              li = @@sv.lot_info(lot)
              lhi = @@sv.lot_hold_list(lot, reason: reason).first
            else
              rsp_mark = 'P'
              memo = "QA hold for MFC Test TDG_01:test20:run #{count}; responsible mark: #{rsp_mark} tasktype: #{tt}, taskdescription: #{tts[:task_description]}"
              if tt.include?('TW')
                assert_equal 0, @@sv.lot_opelocate(lot, '100.300')
                assert_equal 0, @@sv.lot_gatepass(lot)
              else
                2.times {assert_equal 0, @@sv.lot_gatepass(lot)}
              end
              assert_equal 0, @@sv.lot_hold(lot, reason, memo: memo, rsp_mark: rsp_mark), 'error setting lot hold'
              li = @@sv.lot_info(lot)
              lhi = @@sv.lot_hold_list(lot, reason: reason).first
            end
            task = @@tdg.prepare_lothold_task(lot, reason, tt, tts, tasktypeshort, memo, li, lhi, @@sv)
            $log.info "Task: #{task}"
            tasks << task
          }
        }
      }
      @@tdg.push_data(tasks, tasktypeshort: tasktypeshort, dryrun: @@dryrun)
      $log.info "Used lots: #{@@testlots}"

      sleep 600

      @@testlots.each {|lot| @@sv.delete_lot_family(lot)}
      @@testlots = []
    }
  end

  ### Inhibit tasks ###
  def test20_inhibit_eqp_spci
    assert_equal 0, @@sv.eqp_cleanup(@@siltest.ptool)
    channels = @@siltest.setup_channels(cas: ['AutoEquipmentInhibit'])
    dcrp = @@siltest.submit_process_dcr
    # send meas DCR with OOC values
    dcr = @@siltest.submit_meas_dcr(ooc: true)
    # verify inhibit and comment
    inhs = @@sv.inhibit_list(:eqp, @@siltest.ptool, details: true)
    assert @@siltest.verify_ecomment(channels, "TOOL_HELD: #{@@siltest.ptool}: reason={X-S")
  end

  def test21_inhibit_eqp_spcs
    ptool = 'UTCMFC02'
    mtool = 'UTFMFC02'
    assert_equal 0, @@sv.eqp_cleanup(ptool)
    channels = @@siltest.setup_channels(cas: ['AutoEquipmentInhibit'], technology: 'TEST', ptool: ptool, mtool: mtool)
    dcrp = @@siltest.submit_process_dcr(technology: 'TEST', ptool: ptool)
    # send meas DCR with OOC values
    dcr = @@siltest.submit_meas_dcr(ooc: true, technology: 'TEST', mtool: mtool)
    # verify inhibit and comment
    inhs = @@sv.inhibit_list(:eqp, ptool, details: true)
    assert @@siltest.verify_ecomment(channels, "TOOL_HELD: #{ptool}: reason={X-S")
  end

  def test22_inhibit_eqp_fdc
    eqp = 'UTCMFC03'
    reason = 'XFDC'
    memo = "QA #{reason} FDCInhibit for equipment #{eqp}"
    owner = 'X-CATALYST'
    assert_equal(0, @@sv.inhibit_entity(:eqp, eqp, reason: reason, memo: memo, owner: owner), 'error setting entity inhibit')
  end

  def test23_inhibit_eqpchamber_fdc
    eqp = 'UTCMFC03'
    ch = 'PM1'
    reason = 'XFDC'
    memo = "QA #{reason} FDCInhibit for equipment #{eqp} and chamber #{ch}"
    owner = 'X-CATALYST'
    assert_equal(0, @@sv.inhibit_entity(:eqp_chamber, eqp, attrib: ch, reason: reason, memo: memo, owner: owner), 'error setting entity inhibit')
  end

  def test24_inhibit_eqp_otherx
    eqp = 'UTCMFC04'
    reason = 'XAPC'
    memo = "QA #{reason} OtherXInhibit for equipment #{eqp}"
    owner = 'X-CATALYST'
    assert_equal(0, @@sv.inhibit_entity(:eqp, eqp, reason: reason, memo: memo, owner: owner), 'error setting entity inhibit')
  end

  def test25_inhibit_eqpchamber_otherx
    eqp = 'UTCMFC04'
    ch = 'PM1'
    reason = 'XAPC'
    memo = "QA #{reason} OtherXInhibit for equipment #{eqp} and chamber #{ch}"
    owner = 'X-CATALYST'
    assert_equal(0, @@sv.inhibit_entity(:eqp_chamber, eqp, attrib: ch, reason: reason, memo: memo, owner: owner), 'error setting entity inhibit')
  end

  ### Maintenance tasks
  def test30_maintenance_tasks
    tasktypeshort = 'MTC'
    tasks = []
    @@tdg.taskcategories[tasktypeshort].each {|tt|
      @@tdg.tasktypespecs[tt].each {|tts|
        $log.info tts
        timestamp = Time.now.to_i
        date = Time.now.utc.strftime("%FT%T%:z")
        runkey = '8612'
        taskcontext = 'UTCMFC01'
        task = {
          fs_table_id: 'G_TASKLIST',
          task_context: taskcontext,
          task_id: "#{tasktypeshort}#{tts[:task_sub_type]}#{taskcontext}xXxSAPPMNOTIFICATIONIDxXx",
          task_type: tt,
          task_type_short: tasktypeshort,
          task_sub_type: tts[:task_sub_type],
          task_description: tts[:task_description],
          task_rank_class: '8',
          task_rank_reason: '8: Blk/Hld Lots Task',
          task_add_info: 'UTFMFC01.QA: TEST',
          task_run_key: runkey,
          department_list: 'MSS',
          eqp_id: taskcontext,
          chamber_id: 'PM1',
          e10_eqp_state: 'SBY.2WPR',
          e10_chamber_state: 'SBY.2WPR',
          gantt_eqp_list: 'UTFMFC01',
          rank_no: '8612',
          expected_duration: '600',
          toolgroup_list: 'O-ALL',
          ctrljob_id: "ITDC-#{runkey}-2019"
        }
        $log.info "Task: #{task}"
        tasks << task
      }
    }
    @@tdg.push_data(tasks, tasktypeshort: tasktypeshort, dryrun: @@dryrun)
  end

end
