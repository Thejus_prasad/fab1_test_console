=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: Conny Stenzel, 2019-01-10

History:
  2021-01-25 sfrieske, use sv_defaults for standard options
=end

require 'SiViewTestCase'
require 'rtdserver/rtdemuqedata'


class TDG_LotHolds < SiViewTestCase
  @@sv_defaults = {carriers: 'UM%'}
  @@tasktypeshort = 'HLD'

  @@dryrun = false
  @@testlots = []


  def self.startup
    super
    @@tdg = RTD::TDG.new($env)
    @@spcholds = @@tdg.taskcategories[@@tasktypeshort].select {|hld| hld.include?('SPCLotHold')}
    @@twholds = @@tdg.taskcategories[@@tasktypeshort].select {|hld| hld.include?('TW')}
    @@fdcholds = @@tdg.taskcategories[@@tasktypeshort].select {|hld| hld.include?('FDC')}
    @@otherholds = @@tdg.taskcategories[@@tasktypeshort].select {|hld| hld.include?('Other')}
  end

  def teardown
    @@tdg.remove_data()
    @@testlots.each {|lot| @@sv.delete_lot_family(lot)}
  end

  def self.shutdown
    @@tdg.remove_data()
    @@testlots.each {|lot| @@sv.delete_lot_family(lot)}
  end


  def testdev01_smoke_mtqa8
    tasks = []
    tt = "OtherLotHold: PROD"
    tts = {reason: 'TENG', task_sub_type: 'TENG', task_description: 'QA Test: Test hold for MFC tests'}
    memo = "QA hold for MFC Test TDG_LotHolds:test11; tasktype: #{tt}, taskdescription: #{tts[:task_description]}"
    reason = 'HENG'  #tts[:reason]
    assert lot = @@svtest.new_lot, 'error creating test lot'
    @@testlots << lot
    assert_equal 0, @@sv.lot_hold(lot, reason, memo: memo, rsp_mark: 'C'), 'error setting lot hold'
    li = @@sv.lot_info(lot)
    lhi = @@sv.lot_hold_list(lot, reason: reason).first
    task = @@tdg.prepare_lothold_task(li, reason, tt, tts, @@tasktypeshort, memo, lhi)
    tasks << task
    # manual_mfc_check(tasks, @@tasktypeshort, dryrun: @@dryrun, method: 'file')
    @@tdg.push_data(tasks, @@tasktypeshort, {dryrun: @@dryrun, method: 'file'})
    puts "\n--> Please check for data in MFC now and press Enter to continue.\n"
    gets
  end

  def test11_otherlothold_tasks
    tasks = []
    @@otherholds.each {|tt|
      @@tdg.tasktypespecs[tt].each {|tts|
        reason = tts[:reason]
        lots = @@svtest.new_lots(2)
        @@testlots += lots
        lots.each_with_index {|lot, idx|
          memo = "QA hold for MFC Test TDG_LotHolds:test11; tasktype: #{tt}, taskdescription: #{tts[:task_description]}"
          task = String.new
          if idx == 0
            assert_equal 0, @@sv.lot_hold(lot, reason, memo: memo, rsp_mark: 'C'), 'error setting lot hold'
            li = @@sv.lot_info(lot)
            lhi = @@sv.lot_hold_list(lot, reason: reason).first
            # starttime = Time.now.utc.strftime('%m/%d/%Y %H:%M:%S')  # new to 2020-08-06
            task = @@tdg.prepare_lothold_task(li, reason, tt, tts, @@tasktypeshort, memo, lhi)
            # NEW - 2020-08-06 aschmid3
            # task.merge!({start_time: "#{li.claim_time}",})   # Hold timestamp for row 'Start Time' display
            # task.merge!({start_time: "#{starttime}",})
          else
            holdrespop = '1000.150'
            holdrespopname = 'PD for Chamber Tool UTC001'
            holdrespopid = 'UTPD000.02'
            assert_equal 0, @@sv.lot_opelocate(lot, holdrespop)
            assert_equal 0, @@sv.lot_futurehold(lot, holdrespop, reason, memo: memo, post: true), 'error setting future hold'
            assert_equal 0, @@sv.lot_gatepass(lot)
            li = @@sv.lot_info(lot)
            lhi = @@sv.lot_hold_list(lot, reason: reason).first
            # starttime = Time.now.utc.strftime('%m/%d/%Y %H:%M:%S')  # new to 2020-08-06
            task = @@tdg.prepare_lothold_task(li, reason, tt, tts, @@tasktypeshort, memo, lhi, holdrespopid: holdrespopid, holdrespop: holdrespop, holdrespopname: holdrespopname)
            # NEW - 2020-08-06 aschmid3
            # task.merge!({start_time: "#{li.claim_time}",})   # Hold timestamp for row 'Start Time' display
            # task.merge!({start_time: "#{starttime}",})
          end
          $log.info "Task: #{task}"
          tasks << task
        }
      }
    }
    manual_mfc_check(tasks, @@tasktypeshort, dryrun: @@dryrun, method: 'file')
  end

  def test20_spclothold_tasks
    tasks = []
    @@spcholds.each {|tt|
      @@tdg.tasktypespecs[tt].each {|tts|
        reason = tts[:reason]
        lots = @@svtest.new_lots(2)
        @@testlots += lots
        lots.each_with_index {|lot, idx|
          memo = "QA hold for MFC Test TDG_LotHolds:test20; tasktype: #{tt}, taskdescription: #{tts[:task_description]}"
          task = String.new
          if idx == 0
            assert_equal 0, @@sv.lot_hold(lot, reason, memo: memo, rsp_mark: 'C'), 'error setting lot hold'
            li = @@sv.lot_info(lot)
            lhi = @@sv.lot_hold_list(lot, reason: reason).first
            task = @@tdg.prepare_spacelothold_task(li, reason, tt, tts, @@tasktypeshort, memo, lhi)
          else
            holdrespop = '1000.150'
            holdrespopname = 'PD for Chamber Tool UTC001'
            holdrespopid = 'UTPD000.02'
            assert_equal 0, @@sv.lot_opelocate(lot, holdrespop)
            assert_equal 0, @@sv.lot_futurehold(lot, holdrespop, reason, memo: memo, post: true), 'error setting future hold'
            assert_equal 0, @@sv.lot_gatepass(lot)
            li = @@sv.lot_info(lot)
            lhi = @@sv.lot_hold_list(lot, reason: reason).first
            task = @@tdg.prepare_spacelothold_task(li, reason, tt, tts, @@tasktypeshort, memo, lhi, holdrespopid: holdrespopid, holdrespop: holdrespop, holdrespopname: holdrespopname)
          end
          $log.info "Task: #{task}"
          tasks << task
        }
      }
    }
    manual_mfc_check(tasks, @@tasktypeshort, dryrun: @@dryrun, method: 'file')
  end

  def test21_spclothold_spcresppd_tasks
    tasks = []
    @@spcholds.each {|tt|
      @@tdg.tasktypespecs[tt].each {|tts|
        reason = tts[:reason]
        lots = @@svtest.new_lots(2)
        @@testlots += lots
        lots.each_with_index {|lot, idx|
          memo = "QA hold for MFC Test TDG_LotHolds:test21; tasktype: #{tt}, taskdescription: #{tts[:task_description]}"
          task = String.new
          if idx == 0
            assert_equal 0, @@sv.lot_hold(lot, reason, memo: memo, rsp_mark: 'C'), 'error setting lot hold'
            li = @@sv.lot_info(lot)
            lhi = @@sv.lot_hold_list(lot, reason: reason).first
            task = @@tdg.prepare_spacelothold_task(li, reason, tt, tts, @@tasktypeshort, memo, lhi, space_responsible_pd: '1001.0815')
          else
            holdrespop = '1000.150'
            holdrespopname = 'PD for Chamber Tool UTC001'
            holdrespopid = 'UTPD000.02'
            assert_equal 0, @@sv.lot_opelocate(lot, holdrespop)
            assert_equal 0, @@sv.lot_futurehold(lot, holdrespop, reason, memo: memo, post: true), 'error setting future hold'
            assert_equal 0, @@sv.lot_gatepass(lot)
            li = @@sv.lot_info(lot)
            lhi = @@sv.lot_hold_list(lot, reason: reason).first
            task = @@tdg.prepare_spacelothold_task(li, reason, tt, tts, @@tasktypeshort, memo, lhi, space_responsible_pd: '1001.0815', holdrespopid: holdrespopid, holdrespop: holdrespop, holdrespopname: holdrespopname)
          end
          $log.info "Task: #{task}"
          tasks << task
        }
      }
    }
    manual_mfc_check(tasks, @@tasktypeshort, dryrun: @@dryrun, method: 'file')
  end

  def test30_fdclothold_tasks
    tasks = []
    @@fdcholds.each {|tt|
      @@tdg.tasktypespecs[tt].each {|tts|
        reason = tts[:reason]
        lots = @@svtest.new_lots(2)
        @@testlots += lots
        lots.each_with_index {|lot, idx|
          memo = "QA hold for MFC Test TDG_LotHolds:test30; tasktype: #{tt}, taskdescription: #{tts[:task_description]}"
          task = String.new
          if idx == 0
            assert_equal 0, @@sv.lot_hold(lot, reason, memo: memo, rsp_mark: 'C'), 'error setting lot hold'
            li = @@sv.lot_info(lot)
            lhi = @@sv.lot_hold_list(lot, reason: reason).first
            task = @@tdg.prepare_lothold_task(li, reason, tt, tts, @@tasktypeshort, memo, lhi)
          else
            holdrespop = '1000.150'
            holdrespopname = 'PD for Chamber Tool UTC001'
            holdrespopid = 'UTPD000.02'
            assert_equal 0, @@sv.lot_opelocate(lot, holdrespop)
            assert_equal 0, @@sv.lot_futurehold(lot, holdrespop, reason, memo: memo, post: true), 'error setting future hold'
            assert_equal 0, @@sv.lot_gatepass(lot)
            li = @@sv.lot_info(lot)
            lhi = @@sv.lot_hold_list(lot, reason: reason).first
            task = @@tdg.prepare_lothold_task(li, reason, tt, tts, @@tasktypeshort, memo, lhi, holdrespopid: holdrespopid, holdrespop: holdrespop, holdrespopname: holdrespopname)
          end
          $log.info "Task: #{task}"
          tasks << task
        }
      }
    }
    manual_mfc_check(tasks, @@tasktypeshort, dryrun: @@dryrun, method: 'file')
  end

  def test40_twlothold_tasks
    tasks = []
    @@twholds.each {|tt|
      @@tdg.tasktypespecs[tt].each {|tts|
        reason = tts[:reason]
        lots = []
        2.times {lots << @@svtest.new_controllot('Equipment Monitor')}
        @@testlots += lots
        lots.each_with_index {|lot, idx|
          memo = "QA hold for MFC Test TDG_LotHolds:test40; tasktype: #{tt}, taskdescription: #{tts[:task_description]}"
          task = String.new
          if idx == 0
            assert_equal 0, @@sv.lot_hold(lot, reason, memo: memo, rsp_mark: 'C'), 'error setting lot hold'
            li = @@sv.lot_info(lot)
            lhi = @@sv.lot_hold_list(lot, reason: reason).first
            task = @@tdg.prepare_lothold_task(li, reason, tt, tts, @@tasktypeshort, memo, lhi)
          else
            holdrespop = '100.300'
            holdrespopname = ''
            holdrespopid = 'UT-UTC001-PST-MEAS.01'
            assert_equal 0, @@sv.lot_opelocate(lot, holdrespop)
            assert_equal 0, @@sv.lot_futurehold(lot, holdrespop, reason, memo: memo, post: true), 'error setting future hold'
            assert_equal 0, @@sv.lot_gatepass(lot)
            li = @@sv.lot_info(lot)
            lhi = @@sv.lot_hold_list(lot, reason: reason).first
            task = @@tdg.prepare_lothold_task(li, reason, tt, tts, @@tasktypeshort, memo, lhi, holdrespop: holdrespop, holdrespopname: holdrespopname)
          end
          $log.info "Task: #{task}"
          tasks << task
        }
      }
    }
    manual_mfc_check(tasks, @@tasktypeshort, dryrun: @@dryrun, method: 'file')
  end

  def testman90_lothold_tasks_load
    576.times {|count|
      tasks = []
      @@otherholds.each {|tt|
        @@tdg.tasktypespecs[tt].each {|tts|
          reason = tts[:reason]
          lots = @@svtest.new_lots(2)
          @@testlots += lots
          lots.each_with_index {|lot, idx|
            memo = "QA hold for MFC Test TDG_LotHolds:test90; tasktype: #{tt}, taskdescription: #{tts[:task_description]}"
            holdlot(lot, reason, memo, idx)
            li = @@sv.lot_info(lot)
            lhi = @@sv.lot_hold_list(lot, reason: reason).first
            task = @@tdg.prepare_lothold_task(li, reason, tt, tts, @@tasktypeshort, memo, lhi)
            $log.info "Task: #{task}"
            tasks << task
          }
        }
      }
      @@tdg.push_data(tasks, @@tasktypeshort, dryrun: @@dryrun)

      sleep 600

      @@testlots.each {|lot| @@sv.delete_lot_family(lot)}
      @@testlots = []
    }
  end

  def holdlot(lot, reason, memo, idx)
    if idx == 0
      assert_equal 0, @@sv.lot_hold(lot, reason, memo: memo, rsp_mark: 'C'), 'error setting lot hold'
    else
      2.times {assert_equal 0, @@sv.lot_gatepass(lot)}
      assert_equal 0, @@sv.lot_hold(lot, reason, memo: memo, rsp_mark: 'P'), 'error setting lot hold'
    end
  end

  def manual_mfc_check(tasks, tasktypeshort, params={})
    $log.info "manual_mfc_check params: #{params}"
    method = params[:method] || 'once'
    dryrun = params[:dryrun] || @@dryrun
    params.merge!({method: method, dryrun: dryrun})
    @@tdg.push_data(tasks, tasktypeshort, params)
    puts "\n--> Please check for data in MFC now and press Enter to continue.\n"
    gets
  end

end
