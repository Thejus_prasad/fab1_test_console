=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: Conny Stenzel, 2019-09-06

History:
  2019-09-06 cstenzel initial development
=end

require 'SiViewTestCase'
require 'rtdserver/rtdemuqedata'


class TDG_LADF < SiViewTestCase
  @@dryrun = false
  @@testlots = []
    
  def self.startup
    super
    @@tdg = RTD::TDG.new($env)
  end

  def teardown
    @@tdg.remove_data()
    @@testlots.each {|lot| @@sv.delete_lot_family(lot)}
  end

  def self.shutdown
    @@tdg.remove_data()
    @@testlots.each {|lot| @@sv.delete_lot_family(lot)}
  end

  
  def test11_ladf_tasks
    tasktypeshort = 'LADF'
    tasks = []
    eqps = ['UTCMFC01']
    lotcount = 0
    @@tdg.taskcategories[tasktypeshort].each {|tt| lotcount = lotcount + @@tdg.tasktypespecs[tt].size}
    assert lots = @@svtest.new_lots(lotcount, carrier: 'UM%')
    @@testlots += lots
    assert lis = @@sv.lots_info(lots)
    @@tdg.taskcategories[tasktypeshort].each {|tt|
      @@tdg.tasktypespecs[tt].each_with_index {|tts, idx|
        eqps.each {|eqp|
          eqpi = @@sv.eqp_info(eqp)
          task = @@tdg.prepare_ladf_task(lis[idx], eqpi, tt, tts, tasktypeshort)
          tasks << task
        }
      }
    }
    manual_mfc_check(tasks, tasktypeshort, dryrun: @@dryrun, method: 'file')
  end

  def manual_mfc_check(tasks, tasktypeshort, params={})
    $log.info "manual_mfc_check params: #{params}"
    method = params[:method] || 'once'
    dryrun = params[:dryrun] || @@dryrun
    params.merge!({method: method, dryrun: dryrun})
    @@tdg.push_data(tasks, tasktypeshort, params)
    puts "\n--> Please check for data in MFC now and press Enter to continue.\n"
    gets
  end

end
