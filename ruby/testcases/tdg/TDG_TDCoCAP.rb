=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: Conny Stenzel, 2019-01-10

History:
  2019-01-10 cstenzel initial development
=end

require 'SiViewTestCase'
require 'rtdserver/rtdemuqedata'
require 'sil/siltest'


class TDG_TDCoCAP < SiViewTestCase
  @@dryrun = false
  @@testlots = []
  @@tasktypeshort = 'HLD'
  @@siltest_params = {
    sil_timeout: 900, ca_timeout: 240, notification_timeout: 120,
    parameter_template: '_Template_QA_PARAMS_900',
    mpd: 'QA-MFC-MEAS.01',
    mtool: 'UTFMFC01',
    ptool: 'UTCMFC01',
    route: 'MFC-0001.01',
    product: 'MFCPROD.01',
    carriers: 'UM%',
    carrier: 'UM%',
    department: 'MSS'
  }
  @@templates_folder = '_TEMPLATES'
  @@default_template = '_DEFAULT_TEMPLATE'
  @@default_template_qa = 'DEFAULT_QA_TEMPLATE'
  @@autocreated_qa = 'AutoCreated_QAMFC'

  def self.startup
    super
    @@siltest = SIL::Test.new($env, @@siltest_params.merge(sv: @@sv))
    @@lds = @@siltest.lds_inline
    @@lds_setup = @@siltest.lds_setup
    # often used variables
    @@params_template = @@siltest.parameter_template
    @@tdg = RTD::TDG.new($env)
    @@spcholds = @@tdg.taskcategories[@@tasktypeshort].select {|hld| hld.include?('SPCLotHold')}
  end

  def self.shutdown
    @@tdg.remove_data()
    @@testlots.each {|lot| @@sv.delete_lot_family(lot)}
  end

  def test00_setup
    $setup_ok = false
    #
    assert lots = @@svtest.new_lots(1, @@siltest_params), 'error creating lots'
    @@testlots += lots
    #
    parameters = ['QA_MFCPAR_900', 'QA_MFCPAR_901', 'QA_MFCPAR_902', 'QA_MFCPAR_903', 'QA_MFCPAR_904']
    assert @@siltest.set_defaults(@@siltest_params.merge(parameters: parameters, lot: lots[0])), "no PPD for MPD #{@@siltest.mpd} ?"
    #
    $setup_ok = true
  end

  def test11_spclothold_tasks
    assert channels = @@siltest.setup_channels(cas: ['LotHold', 'AutoLotHold', 'ReleaseLotHold', 'CancelLotHold'])
    assert @@siltest.submit_process_dcr
    # send meas DCR with OOC values
    assert dcr = @@siltest.submit_meas_dcr(ooc: true), 'error submitting DCR'

    space_ch_id = channels.first.chid
    space_sample_id = channels.first.ckc_samples.last.sid
    lot = @@testlots[0]
    assert_equal 0, @@sv.lot_gatepass(lot)

    tasks = []
    @@spcholds.each {|tt|
      @@tdg.tasktypespecs[tt].each {|tts|
        reason = tts[:reason]
        li = @@sv.lot_info(lot)
        lhi = @@sv.lot_hold_list(lot, reason: reason).first
        memo = "QA hold for MFC Test TDG_TDCoCAP:test11; tasktype: #{tt}, taskdescription: #{tts[:task_description]}"
        task = @@tdg.prepare_spacelothold_task(li, reason, tt, tts, @@tasktypeshort, memo, lhi,
          space_ch_id: space_ch_id, space_sample_id: space_sample_id,
          mtool: @@siltest_params[:mtool], ptool: @@siltest_params[:ptool])
        $log.info "Task: #{task}"
        tasks << task
      }
    }
    manual_mfc_check(tasks, @@tasktypeshort, dryrun: @@dryrun, method: 'file')
  end

  def test12_cleanup
    @@tdg.remove_data()
  end

  def manual_mfc_check(tasks, tasktypeshort, params={})
    $log.info "manual_mfc_check params: #{params}"
    method = params[:method] || 'once'
    dryrun = params[:dryrun] || @@dryrun
    params.merge!({method: method, dryrun: dryrun})
    @@tdg.push_data(tasks, tasktypeshort, params)
    puts "\n--> Please check for data in MFC now and press Enter to continue.\n"
    gets
  end

end
