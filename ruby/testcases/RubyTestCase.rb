=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: Steffen Steidten, 2010-05-03

History:
  2010-08-06 ssteidte, added self.set_parameters for bulk parameter setting
  2012-06-05 ssteidte, added $setup_ok check to setup
  2013-08-14 ssteidte, removed Ruby1.8 support
  2013-11-06 dsteger,  added test-unit support
  2014-10-09 ssteidte, support for manual tests, starting with 'testmanual' or 'testdev'
  2014-11-04 ssteidte, move load_test and run_test from RubyTestCase to a separate module
  2015-01-26 ssteidte, prepare for use with Ruby 2.2
  2015-03-06 ssteidte, create RubyTestCase with setup parameters allowing env or test specific overrides
  2015-06-22 ssteidte, don't set timezone for ENV, this is handled in DB and SiView instances
  2015-10-14 sfrieske, switched to Ruby 2.2 (JRuby9)
  2016-02-29 sfrieske, added _get_class_variables for easier debgging of testcases, use with care!
  2016-10-14 sfrieske, _get_class_variables includes assertions in the console's scope
  2016-12-13 sfrieske, removed custom assertions assert_nothing_raised, assert_not_nil, assert_not_equal
  2018-09-13 sfrieske, prefer the use of @@sv over $sv
  2019-04-11 sfrieske, renamed run_test to runtest
  2019-11-06 sfrieske, set Minitest::Assertions.diff instead of opening Minitest::Assertions definition
  2021-02-05 sfrieske, use instance_of instead of kind_of
  2021-09-15 sfrieske, finally replaced $svtest by @@svtest, $sv by @@sv and $sm by @@sm
=end

require 'fileutils'
require 'pp'
require 'minitest'
require 'yaml'

ENV['MT_NO_SKIP_MSG'] = '1'  # suppress 'You have skipped tests' message in the summary

Minitest::Assertions.diff = nil   # no external diffs for assert_equal with arrays and hashes


module TestCases
  [:TcDir, :ParameterDir].each {|c| remove_const(c) if const_defined?(c)} # for clean re-load

  TcDir = File.dirname(__FILE__)
  ParameterDir = File.join(Dir.pwd, 'testparameters')

  # cleanup and (re-)load testcase code and parameters, used by TCRunner and Console, return test class or nil on error
  def self.load_test(classname, env, params={})
    $log.info "TestCases.load_test #{classname.inspect}, #{env.inspect}, #{params}"
    # remove old test classes and remove loaded feature for require and require_relative to work
    cnames = Minitest::Runnable.runnables.collect {|c| c.name unless c.name.start_with?('Minitest')}.compact
    Minitest::Runnable.reset  # remove test suites
    cnames.reverse.each {|cname|
      $log.debug {"  undefining old test class #{cname}"}
      Object.send(:remove_const, cname) if Object.const_defined?(cname)
      feature = $LOADED_FEATURES.find {|e| e.end_with?("#{cname}.rb")}
      $LOADED_FEATURES.delete(feature) if feature
    }
    # load test class
    classname = File.basename(classname, '.rb')  # allows to pass TestCase or TestCase.rb as classname
    fname = Dir["#{TcDir}/**/#{classname}.rb"].first || return
    $log.info "  loading test file #{fname.sub(Dir.pwd, '.')}"
    load(fname)
    return Object.const_get(classname) unless env  # suppress loading of env specific testparameters if no env
    #
    # remove test classes again and do NOT remove loaded feature again for require_relative to work
    cnames = Minitest::Runnable.runnables.collect {|c| c.name unless c.name.start_with?('Minitest')}.compact
    Minitest::Runnable.reset
    cnames.reverse.each {|cname| Object.send(:remove_const, cname)}
    # load env specific test parameters class by class
    cnames.each {|cname|
      fname = Dir["#{TcDir}/**/#{cname}.rb"].first || return
      load(fname)
      pfile = Dir["#{ParameterDir}/#{env}/**/#{cname}.par"].first
      if pfile && File.file?(pfile)
        $log.info "  loading test parameters from #{pfile.sub(Dir.pwd, '.')}"
        parameters = YAML::load_file(pfile)
        if parameters.instance_of?(Hash)
          tcclass = Object.const_get(cname)
          parameters.keep_if {|k, v| tcclass.class_variables.member?(:"@@#{k}")}
          $log.info "  setting parameters: #{parameters.inspect}"
          parameters.each_pair {|k, v|
            var = :"@@#{k}"
            oldv = tcclass.class_variable_get(var)
            ($log.info "    changing #{k}: #{oldv.inspect} -> #{v.inspect}"; tcclass.class_variable_set(var, v)) if oldv != v
          }
        else
          $log.debug {"  empty parameter file, got #{parameters.inspect}"} unless parameters.instance_of?(Hash)
        end
      end
    }
    tcclass = Minitest::Runnable.runnables.last # runnables has also the superclasses
    # apply overrides
    if params[:parameters].instance_of?(Hash)
      parameters = params[:parameters]
      parameters.keep_if {|k, v| tcclass.class_variables.member?(:"@@#{k}")}
      $log.info "  setting override parameters #{parameters.inspect}"
      parameters.each_pair {|k, v|
        var = :"@@#{k}"
        oldv = tcclass.class_variable_get(var)
        ($log.info "    changing #{k}: #{oldv.inspect} -> #{v.inspect}"; tcclass.class_variable_set(var, v)) if oldv != v
      }
    end
    return tcclass
  end

  # store parameters of a clean testcase object (including loaded parameters if any), separated by superclasses
  def self.store_builtin_parameters(classname, env, params={})
    $log.info "TestCases.store_builtin_parameters #{classname.inspect}, #{env.inspect}, #{params}"
    # load test class to get the class hierarchy
    load_test(classname, nil) || return
    cnames = Minitest::Runnable.runnables.collect {|c| c.name unless c.name.start_with?('Minitest')}.compact
    # remove class variables and methods
    cnames.reverse.each {|cname| Object.send(:remove_const, cname)}
    # re-load test class hierarchy bottom up, separating parameters
    parameters = {}
    cnames.each {|cname|
      cfile = Dir["#{TcDir}/**/#{cname}.rb"].first
      load(cfile)
      classobj = Object.const_get(cname)
      # get new parameters
      newparameters = {}
      classobj.class_variables.each {|var|
        oldval = parameters[var]
        newval = classobj.class_variable_get(var)
        newparameters[var] = newval if oldval != newval
      }
      if env && classobj != RubyTestCase
        # load parameters if env is given
        pfile = Dir["#{ParameterDir}/#{env}/**/#{cname}.par"].first
        if pfile && File.file?(pfile)
          $log.info "  loading test parameters from #{pfile.split(ParameterDir).last.inspect}"
          fparameters = YAML::load_file(pfile)
        else
          fparameters = {}
        end
        # apply overrides and remove (old) parameters not defined in the class
        fparameters.merge!(params[:parameters]) if params[:parameters].instance_of?(Hash)
        fparameters.each_pair {|k, v|
          k = :"@@#{k}"
          newparameters[k] = v if classobj.class_variables.member?(k)
        }
        $log.info "  setting test parameters #{newparameters.keys.inspect}"
        newparameters.each_pair {|k, v| classobj.class_variable_set(k, v)}
        # store parameters, sorted
        pars = {}
        newparameters.keys.sort.each {|k| pars[k[2..-1]] = newparameters[k]}
        fname = "#{ParameterDir}/#{env}/#{cfile.split(TcDir).last.split('.rb').first}.par"
        $log.info "  writing test parameters to #{fname.inspect}"
        FileUtils.mkdir_p(File.dirname(fname))
        File.binwrite(fname, pars.to_yaml)
      end
      parameters.merge!(newparameters)
    }
    return Object.const_get(classname)
  end

  # run all or selected tests in the specified environment; used by TCRunner and Console
  #
  # optional params:
  #   tp: /testxx|testyy/ to select test cases,
  #   notp: /testa|testb/ to deselect test cases,
  #   fail_abort: true to stop at first failed tc,
  #   nosummary: true to suppress the list of not executed tcs
  #   parameters: {hash of class variables defined in the testcase and their override values}
  #
  # return Reporter instance or nil on general errors
  def self.runtest(tcclass, env, params={}, &blk)
    $log.debug {"TestCases.runtest #{tcclass.inspect}, #{env.inspect} #{params}"}
    tcclass = load_test(tcclass, env, params) if tcclass.instance_of?(String)
    ($log.warn 'no testcase'; return) unless tcclass
    # set $env and $setup_ok
    ($log.warn 'test environment is not set'; return) unless env
    $env = env
    $setup_ok = true
    # get test methods
    reporter = Minitest::CompositeReporter.new
    reporter << Minitest::SummaryReporter.new($log) # for Skips in the summary: verbose: true)
    reporter << Minitest::ProgressReporter.new($log)
    reporter.start
    executed = []
    notexecuted = []
    $log.puts "\n#{tcclass.name}.startup"
    tcclass.startup
    # main loop to execute the collected test methods
    tp = params[:tp]
    notp = params[:notp]
    tcclass.instance_methods(true).grep(/^test/).sort.each {|m|
      # test case pattern filter
      if (tp && m !~ tp) || (tp.nil? && m.to_s.start_with?('testdev', 'testman')) || (notp && m =~ notp)
        notexecuted << m
        next
      end
      # run the testcase method
      $log.puts "\n\n#{tcclass}##{m}"
      inst = tcclass.new(m)
      blk.call(inst) if blk  # e.g. to notify tcrunner, argument is the tcclass instance
      result = inst.run
      # register method as executed or notexecuted, log assertion and location immediately
      if f = inst.failures.first
        Minitest::Skip === f ? notexecuted << m : executed << m
        $log.puts "line #{f.location.split(':').last}: #{f.message}"
      else
        executed << m
      end
      # handle $setup_ok
      if $setup_ok == false
        ass = Minitest::Assertion.new("\n--> Setup error in #{m}, test aborted!")
        ass.set_backtrace('')
        inst.failures << ass
      end
      # record the result
      reporter.record(result)
      break if ($setup_ok == false) || (!reporter.passed? && params[:fail_abort])
    }
    # final cleanup
    unless params[:cleanup] == false
      $log.puts "\n\n#{tcclass.name}.shutdown"
      tcclass.shutdown
      if reporter.passed?
        $log.puts "\n\n#{tcclass.name}.after_all_passed"
        tcclass.after_all_passed
      end
    end
    # report
    $log.puts "\n\n"
    reporter.report
    unless params[:summary] == false
      if notexecuted.empty?
        $log.puts "\nall tests have been executed\n" if !params[:fail_abort] || reporter.passed?
      else
        $log.puts "\ntests executed:\n#{executed}\n\ntests not executed:\n#{notexecuted}\n"
      end
      $log.puts "#{tcclass.name} done at #{Time.now.utc}\n"
    end
    return reporter
  end

end


class RubyTestCase < Minitest::Test

  # called before the test runs, create a SiView::MM instance per default
  def self.startup(params={})
    return if params[:nosiview]
    require 'siview'
    @@sv = SiView::MM.new($env, params)
  end

  # called after the last test run
  def self.shutdown
  end

  # called after the last test method if all executed test cases have passed
  def self.after_all_passed
  end

  # called before a test method starts, defined in Minitest::Test
  ##def setup
  ##end

  # called after a test method ended, defined in Minitest::Test
  ##def teardown
  ##end


  # include assertions and pull all class variables into the console's scope
  # use with care, for debugging testcases only, and verify the test afterwards in a new console!
  #
  # return list of the imported class variables
  def self._get_class_variables
    main = IRB.conf[:MAIN_CONTEXT].workspace.main
    Minitest::Assertions.attr_accessor(:assertions)
    main.__send__(:include, Minitest::Assertions)
    main.assertions = 0   # makes assert etc. work on the console
    class_variables.each {|v| main.class.class_variable_set(v, class_variable_get(v))}
  end

end
