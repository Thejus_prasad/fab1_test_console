=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: Steffen Steidten, 2014-10-29

History:
  2014-10-29 ssteidte, extracted SiView testing related stuff from RubyTestCase
  2015-03-06 ssteidte, create SiViewTestCase with setup parameters allowing env or test specific overrides
  2018-05-03 sfrieske, added @@svtest etc to enable the migration away from global vars
  2021-09-15 sfrieske, finally replaced $svtest by @@svtest, $sv by @@sv and $sm by @@sm
=end

require 'siviewtest'
require 'RubyTestCase'


# Class for SiView tests that need to interact with JavaEIs or SM in addition to MM
class SiViewTestCase < RubyTestCase
  @@sv_defaults = {}  # default user, route, sorter, etc.

  # called before the test runs, creates a SiView::Test instance
  def self.startup(params={})
    @@svtest = SiView::Test.new($env, @@sv_defaults.merge(params))
    @@sv = @@svtest.sv
    @@sm = @@svtest.sm
  end

end
