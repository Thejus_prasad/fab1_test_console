=begin
(c) Copyright 2016 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.


Author: Steffen Frieske, 2018-07-19

Version: 1.0

History:
=end

require 'util/verifyhash'
require 'remote'
require 'SiViewTestCase'


# Test basic requirements for EMS tests
class EMS_Basic < SiViewTestCase
  @@propsfile = '/amdapps/ems/EMS_160205/configuration/Fab1.ITDC/ems.site.properties'
  @@reffile = "./testparameters/#{$env}/ems/envvars.yaml"
  @@exclude = []


  def test00_setup
    $setup_ok = false
    #
    @@rhost = RemoteHost.new("ems.#{$env}")
    #
    $setup_ok = true
  end

  def test01_verify_env
    $setup_ok = false
    #
    assert envvars = @@rhost.read_propsfile(@@propsfile), "error reading properties from #{@@propsfile.inspect}"
    assert ref = YAML.load_file(@@reffile), "error loading ref data from #{@@reffile.inspect}"
    assert verify_hash(ref, envvars, nil, exclude: @@exclude), "EMS server env vars are not as expected"
    #
    $setup_ok = true
  end

end
