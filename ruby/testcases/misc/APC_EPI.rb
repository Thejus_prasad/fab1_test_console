=begin
(c) Copyright 2016 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.


Author: Steffen Frieske, 2020-02-21

Version: 0.0.0

History:

Notes:
  http://f1onewiki:21080/display/PCLP/Contexts#Contexts-SiViewContexts
=end

# require 'misc'
# require 'catalyst/apcbaseline'
# require 'catalyst/apcdb'
require 'SiViewTestCase'

# UNDER CONSTRUCTION


# Test prepartion for EPI APC Lotmanager tests
class APC_EPI < SiViewTestCase
  @@sv_defaults = {route: 'UTRT-CASCADING.01', product: 'UT-PRODUCT-CASCADING.01'}
  @@eqp = 'EPI4201'
  @@pd = 'UTPD-APCEPI.01'  # prod: 'TJ-DEP-106860.01'
  @@lr = 'D-P-EPI-106860.01'
  @@recipe = 'D-P-EPI.P-106860-APC-CHAB.01'

  @@testlots = []

  @@use_http = false


  def self.after_all_passed
    @@testlots.each {|lot| @@sv.delete_lot_family(lot)}
  end


  def test00_setup
    $setup_ok = false
    #
    # @@apcbl = APC::BaselineTest.new($env, use_http: @@use_http)
    # @@apcdb = APC::DB.new($env)
    # assert @@apcbl.apc.catalyst.ping, "#{@@apcbl.apc.catalyst.inspect} is not working"
    #
    $setup_ok = true
  end

  def test11_xxx
    assert @@lot = @@svtest.new_lot, 'error creating test lot'
    @@testlots << @@lot
    assert_equal 0, @@sv.lot_opelocate(lot, nil, op: @@pd), 'error locating lot'
  end

end
