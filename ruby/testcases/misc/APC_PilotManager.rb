=begin
(c) Copyright 2016 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: Steffen Frieske, 2017-11-01

Version: 1.2.0

History:
  2019-06-26 sfrieske, added TCs for meas types DI, DR, FI, FDR

Notes:
  ITDC scenario specs: C07-00003517 and C07-00003523 (LIT)
  also required:
    resppd spec C05-00001757
    PCP spec C05-00001759 (for pilot completion, entries for all Meas PDs with 'Frequency Lot' set to S,I,C,M *)
    DCP spec C05-00001773 for meas types DI, DR, FI, FDR
=end

require 'catalyst/pilotmgrtest'
require 'RubyTestCase'


class APC_PilotManager < RubyTestCase
  @@pm_defaults = {
    route: 'UTRT-PILOTMGR.01', product: 'UT-PRODUCT-PILOTMGR.01', spec: 'C07-00003517',
    sorter: 'UTS001APC', eqp: 'UTPILOTMGR1', ppd: 'UTPDPILOT01.01', last_mpd: 'UTPDM03.01'
  }
  @@spec_lit = 'C07-00003523'
  @@user_hold = 'ENG'
  @@route_di = 'UTRT-PILOTMGR.02'
  @@op_di = 'UTPDMDI.01'
  @@op_dr = 'UTPDMDR.01'
  @@op_fi = 'UTPDMFI.01'
  @@op_fdr = 'UTPDMFDR.01'
  @@nonprobank = 'UT-NONPROD'

  @@use_http = false
  @@testlots = []


  def self.shutdown
    @@pmtest.rtdremote.delete_emustation('EMPTY')
  end

  def self.after_all_passed
    @@testlots.each {|lot| @@sv.delete_lot_family(lot)}
  end

  def teardown
    @@pmtest.rtdremote.delete_emustation('EMPTY')
  end


  def test00_setup
    $setup_ok = false
    #
    assert @@pmtest = APC::PilotMgrTest.new($env, @@pm_defaults.merge(sv: @@sv, use_http: @@use_http))
    assert @@pmtest_lit = APC::PilotMgrTest.new($env, @@pm_defaults.merge(sv: @@sv, use_http: @@use_http, spec: @@spec_lit))
    assert @@pmtest.pmgr.catalyst.ping, "#{@@pmtest.pmgr.inspect} is not connected"
    assert lot = @@pmtest.new_lot, 'error creating test lot'
    @@testlots << lot
    #
    $setup_ok = true
  end

  def testman01_dev
    lot = @@testlots[0]
    assert lc = @@pmtest.verify_scenario('QA-000', lot, lot_cleanup: true), 'test failed'
    assert @@pmtest.complete_pilot(lc), 'error completing pilot'
  end

  # prepare a lot for manual tests
  def testman02_prepare_lot
    assert lot = @@pmtest.new_lot, 'error creating test lot'
    assert_equal 0, @@sv.lot_opelocate(lot, nil, op: @@pmtest.ppd)
  end

  def test11_happy
    $setup_ok = false
    #
    lot = @@testlots[0]
    %w(QA-000 QA-001 QA-010 QA-011).each {|scenario|
      $log.info "\n-- scenario #{scenario.inspect}"
      assert lc = @@pmtest.verify_scenario(scenario, lot, lot_cleanup: true), "scenario #{scenario} failed"
      assert @@pmtest.complete_pilot(lc), 'error completing pilot'
    }
    #
    $setup_ok = true
  end

  def test12_happyseparate
    %w(QA-100 QA-101 QA-110 QA-111).each {|scenario|
      $log.info "\n-- scenario #{scenario.inspect}"
      assert lc = @@pmtest.verify_scenario(scenario, @@testlots[0], lot_cleanup: true), "scenario #{scenario} failed"
      assert @@pmtest.complete_pilot(lc), 'error completing pilot'
    }
  end

  def test13_happylitho
    lot = @@testlots[0]
    assert @@pmtest_lit.sjctest.cleanup_sort_requests(@@sv.lot_info(lot).carrier)
    assert @@sv.merge_lot_family(lot)
    assert_equal 0, @@sv.lot_cleanup(lot, op: @@pmtest.ppd)
    # create_pilots checks the message to the EI that completes the test. no pilot lot created by PilotManager
    assert @@pmtest_lit.create_pilots('QA-LIT-000', lot)
  end

  def test14_happyonhold
    lot = @@testlots[0]
    %w(QA-000 QA-001 QA-010 QA-011).each {|scenario|
      $log.info "\n-- scenario #{scenario.inspect}"
      assert @@sv.merge_lot_family(lot)
      assert_equal 0, @@sv.lot_cleanup(lot, op: @@pmtest.ppd)
      assert_equal 0, @@sv.lot_hold(lot, @@user_hold)
      assert lc = @@pmtest.verify_scenario(scenario, lot), "scenario #{scenario} failed"
      refute_empty @@sv.lot_hold_list(lot, reason: @user_hold), 'srclot missing user hold'
      refute_empty @@sv.lot_hold_list(lc, reason: @user_hold), 'pilot missing user hold'
      ##assert @@pmtest.complete_pilot(lc), 'error completing pilot'
    }
  end

  def test15_before_ppd
    lot = @@testlots[0]
    assert @@sv.merge_lot_family(lot)
    assert_equal 0,  @@sv.lot_cleanup(lot, opNo: :first)
    assert_equal 0, @@sv.lot_opelocate(lot, nil, op: @@pmtest.ppd, offset: -1)
    assert lc = @@pmtest.verify_scenario('QA-000P', lot, checktoolwish: false), 'scenario test failed'
    assert @@pmtest.complete_pilot(lc), 'error completing pilot'
  end

  def test16_same_toolwish
    assert lot = @@pmtest.new_lot, 'error creating test lot'
    @@testlots << lot
    assert @@pmtest.verify_scenario('QA-200', lot, lot_cleanup: true), 'test failed'
  end

  def test17_same_chamberwish
    assert lot = @@pmtest.new_lot, 'error creating test lot'
    @@testlots << lot
    assert @@pmtest.verify_scenario('QA-210', lot, chambers: ['CH1'], lot_cleanup: true), 'test failed'
  end

  def test19_same_sampling
    lot = @@testlots[0]
    assert @@pmtest.verify_scenario('QA-220', lot, lot_cleanup: true), 'test failed'
  end

  def test21_2dialogs
    scenario = 'QA-000'
    lot = @@testlots[0]
    assert @@sv.merge_lot_family(lot)
    assert_equal 0,  @@sv.lot_cleanup(lot, op: @@pmtest.ppd)
    assert lcs = [scenario, scenario].collect {|sc| @@pmtest.verify_scenario(sc, lot)}, 'scenario failed'
    lcs.each {|lc| assert @@pmtest.complete_pilot(lc), 'error completing pilot'}
  end

  def test22_2main_happy
    lot = @@testlots[0]
    [ ['QA-000', 'QA-000'], ['QA-001', 'QA-000'], ['QA-001', 'QA-001'],
      ['QA-010', 'QA-000'], ['QA-011', 'QA-000'], ## may fail if 1st pilot picks the wafers for 2nd ['QA-000', 'QA-011'],
      # with separate
      ['QA-100', 'QA-000'], ['QA-000', 'QA-100'], ## fails because 2nd SJC task cannot be created ['QA-100', 'QA-100'],
    ].each_with_index {|scenarios, idx|
      $log.info "\n- testing scenario combination ##{idx + 1}: #{scenarios}"
      assert @@pmtest.verify_scenario(scenarios, lot, checktoolwish: false, maindialog: true, lot_cleanup: true), "scenarios #{scenarios} failed"
    }
  end

  def test23_2main_fail
    lot = @@testlots[0]
    [ ['QA-010', 'QA-010'] # 2x use_aliases
    ].each_with_index {|scenarios, idx|
      $log.info "\n- testing scenario combination ##{idx + 1}: #{scenarios}"
      refute @@pmtest.verify_scenario(scenarios, lot, maindialog: true, lot_cleanup: true), 'test failed'
      assert @@pmtest.pmgr.extsvcqamsg.include?('Selected Scenario is not allowed'), 'wrong dialog message'
    }
  end

  def test24_2add
    lot = @@testlots[0]
    idxstart = 0
    idx = 0
    bscenarios = ['QA-000', 'QA-001', 'QA-010', 'QA-011', 'QA-100', 'QA-101', 'QA-110', 'QA-111']
    bscenarios.each {|scenario1|
      bscenarios.each {|scenario2|
        scenarios = [scenario1, scenario2]
        $log.info "\n\n- testing scenario combination ##{idx += 1}: #{scenarios}"
        ($log.info "\n- skipping known combination"; next) if idx < idxstart
        scs = scenarios.collect {|scenario| @@pmtest.spec_scenario(scenario)}
        expected = scs[0][:srclot][:hold] == scs[1][:srclot][:hold] &&
                   ((scs[0][:pilot][:aliases] || []) & (scs[1][:pilot][:aliases] || [])).empty?
        assert_equal expected, !!@@pmtest.verify_scenario(scenarios, lot, checktoolwish: false, lot_cleanup: true), "test ##{idx}  #{scenarios} failed"
      }
    }
  end

  def testdev24
    lot = @@testlots[0]
    scenarios = ['QA-000', 'QA-110']
    assert !!@@pmtest.verify_scenario(scenarios, lot, checktoolwish: false, lot_cleanup: true)
  end

  def test25_separate_desc
    lot = @@testlots[0]
    assert lcs = @@pmtest.verify_scenario(['QA-100CDesc', 'QA-100CDesc'], lot, lot_cleanup: true), 'test failed'
    assert_equal 1, lcs.collect {|lc| @@sv.lot_info(lc).carrier}.uniq.size, 'wrong pilot carriers'
  end

  def test26_separate_desc_notallowed
    lot = @@testlots[0]
    [['QA-100CNo', 'QA-100CDesc'], ['QA-100CDesc', 'QA-100CNo']].each {|scenarios|
      assert lcs = @@pmtest.verify_scenario(['QA-100CDesc', 'QA-100CNo'], lot, lot_cleanup: true), 'test failed'
      assert_equal 2, lcs.collect {|lc| @@sv.lot_info(lc).carrier}.uniq.size, 'wrong pilot carriers'
    }
  end

  def test31_golden
    # 4 child lots, children 3+4 go into 1 carrier; note: pilots are _not_ created in the order added in the dialog!
    lot = @@testlots[0]
    scenarios = ['QA-100NO', 'QA-100NO', 'QA-100', 'QA-100']
    assert lcs = @@pmtest.verify_scenario(scenarios, lot, lot_cleanup: true), 'test failed'
    lcs.each_with_index {|lc, i| assert @@pmtest.complete_pilot(lc), "error completing pilot ##{i}"}
  end

  def test32_6chambers
    # 6 child lots in 1 separate carrier, 6 different chambers
    lot = @@testlots[0]
    chambers = @@sv.eqp_info(@@pmtest.eqp).chambers.collect {|e| e.chamber}.take(6)
    assert_equal 6, chambers.size, 'setup error: not enough eqp chambers'
    scenarios = Array.new(6, 'QA-101E')
    assert lcs = @@pmtest.verify_scenario(scenarios, lot, chambers: chambers, lot_cleanup: true), 'test failed'
    lcs.each_with_index {|lc, i| assert @@pmtest.complete_pilot(lc), "error completing pilot ##{i}"}
  end

  def test33_pilot1complete_pilot2complete
    # 1 pilot incl complete, 2nd pilot (inheriting X-CAT hold) complete
    lot = @@testlots[0]
    # pilot 1
    assert lc1 = @@pmtest.verify_scenario('QA-101E', lot, lot_cleanup: true), 'test failed'
    assert_equal 'ONHOLD', @@sv.lot_info(lot).status, 'missing src lot hold'
    assert_equal 'Waiting', @@sv.lot_info(lc1).status, 'missing src lot hold'
    assert @@pmtest.complete_pilot(lc1), 'error completing pilot1'
    # pilot 2, no srclot cleanup!
    assert lc2 = @@pmtest.verify_scenario('QA-101E', lot), 'test failed'
    assert_equal 'ONHOLD', @@sv.lot_info(lot).status, 'missing src lot hold'
    assert_equal 'ONHOLD', @@sv.lot_info(lc2).status, 'missing pilot lot hold'
    assert_equal 0, @@sv.lot_hold_release(lc2, nil), 'error releasing lc2 hold'
    assert @@pmtest.complete_pilot(lc2), 'error completing pilot1'
    assert_equal 'ONHOLD', @@sv.lot_info(lot).status, 'missing src lot hold'
    assert_equal 'ONHOLD', @@sv.lot_info(lc2).status, 'missing pilot lot hold'
  end

  def test41_internal
    lot = @@testlots[0]
    assert lc = @@pmtest.verify_scenario('QA-000INTERNAL', lot, last_mpd: @@pmtest.ppd, lot_cleanup: true), 'test failed'
    assert @@pmtest.complete_pilot(lc, last_mpd: @@pmtest.ppd), 'error completing pilot'
  end

  def test42_di
    lot = @@testlots[0]
    # route w/o DI, must fail
    refute @@pmtest.verify_scenario('QA-000DI', lot, lot_cleanup: true), 'pilot creation must fail'
    assert @@pmtest.pmgr.extsvcqamsg.include?('No valid merge PD'), 'wrong err msg, wrong setup?'
    # route with DI/DR, merge PD DR must fail
    assert lot = @@pmtest.new_lot, 'error creating test lot'
    @@testlots << lot
    assert_equal 0, @@sv.schdl_change(lot, route: @@route_di)  # with DI/DR PDs
    refute @@pmtest.verify_scenario('QA-000DI', lot, last_mpd: @@op_dr, lot_cleanup: true), 'pilot creation must fail'
    #                   merge PD DI must pass
    assert lc = @@pmtest.verify_scenario('QA-000DI', lot, last_mpd: @@op_di, lot_cleanup: true), 'test failed'
    assert @@pmtest.complete_pilot(lc, last_mpd: @@op_di), 'error completing pilot'
  end

  def test43_dr
    lot = @@testlots[0]
    # route w/o DI, must fail
    refute @@pmtest.verify_scenario('QA-000DR', lot, lot_cleanup: true), 'pilot creation must fail'
    assert @@pmtest.pmgr.extsvcqamsg.include?('No valid merge PD'), 'wrong err msg, wrong setup?'
    # route with DI/DR, merge PD DI must fail
    assert lot = @@pmtest.new_lot, 'error creating test lot'
    @@testlots << lot
    assert_equal 0, @@sv.schdl_change(lot, route: @@route_di)  # with DI/DR PDs
    refute @@pmtest.verify_scenario('QA-000DR', lot, last_mpd: @@op_di, lot_cleanup: true), 'pilot creation must fail'
    #                   merge PD DR must pass
    assert lc = @@pmtest.verify_scenario('QA-000DR', lot, last_mpd: @@op_dr, lot_cleanup: true), 'test failed'
    assert @@pmtest.complete_pilot(lc, last_mpd: @@op_dr), 'error completing pilot'
  end

  def test44_fi
    lot = @@testlots[0]
    # route w/o FI, must fail
    refute @@pmtest.verify_scenario('QA-000FI', lot, lot_cleanup: true), 'pilot creation must fail'
    assert @@pmtest.pmgr.extsvcqamsg.include?('No valid merge PD'), 'wrong err msg, wrong setup?'
    # route with FI, must pass
    assert lot = @@pmtest.new_lot, 'error creating test lot'
    @@testlots << lot
    assert_equal 0, @@sv.schdl_change(lot, route: @@route_di)  # with FI/FDR PDs
    assert lc = @@pmtest.verify_scenario('QA-000FI', lot, last_mpd: @@op_fi, lot_cleanup: true), 'test failed'
    assert @@pmtest.complete_pilot(lc, last_mpd: @@op_fi), 'error completing pilot'
  end

  def test45_fdr
    lot = @@testlots[0]
    # route w/o FI, must fail
    refute @@pmtest.verify_scenario('QA-000FI', lot, lot_cleanup: true), 'pilot creation must fail'
    assert @@pmtest.pmgr.extsvcqamsg.include?('No valid merge PD'), 'wrong err msg, wrong setup?'
    # route with FDR, must pass
    assert lot = @@pmtest.new_lot, 'error creating test lot'
    @@testlots << lot
    assert_equal 0, @@sv.schdl_change(lot, route: @@route_di)  # with FI/FDR PDs
    assert lc = @@pmtest.verify_scenario('QA-000FDR', lot, last_mpd: @@op_fdr, lot_cleanup: true), 'test failed'
    assert @@pmtest.complete_pilot(lc, last_mpd: @@op_fdr), 'error completing pilot'
  end

  def test48_qtimes
    assert lot = @@pmtest.new_lot, 'error creating test lot'
    @@testlots << lot
    # QT ends at last MPD -> merge after last MPD
    assert_equal 0, @@sv.schdl_change(lot, route: 'UTRT-PILOTMGR-QT.00')
    assert @@pmtest.verify_scenario('QA-000', lot, merge_offset: 1, lot_cleanup: true), 'test failed'
    # QT ends 1 PD after last MPD -> merge 2 PDs after last MPD
    assert_equal 0, @@sv.schdl_change(lot, route: 'UTRT-PILOTMGR-QT.01')
    assert @@pmtest.verify_scenario('QA-000', lot, merge_offset: 2, lot_cleanup: true), 'test failed'
    # QT ends 1 PD after last MPD, 2nd QT starts -> merge 4 PDs after last MPD
    assert_equal 0, @@sv.schdl_change(lot, route: 'UTRT-PILOTMGR-QT.02')
    assert @@pmtest.verify_scenario('QA-000', lot, merge_offset: 4, lot_cleanup: true), 'test failed'
    # same as above, QTs' criticality is C4
    assert_equal 0, @@sv.schdl_change(lot, route: 'UTRT-PILOTMGR-QT.02C4')
    assert @@pmtest.verify_scenario('QA-000', lot, merge_offset: 1, lot_cleanup: true), 'test failed'
  end


  # srclot conditions

  def test61_nonprobank
    lot = @@testlots[0]
    assert @@sv.merge_lot_family(lot)
    assert_equal 0,  @@sv.lot_cleanup(lot, op: @@pmtest.ppd)
    assert_equal 0, @@sv.lot_nonprobankin(lot, @@nonprobank)
    refute @@pmtest.create_pilots('QA-000', lot), 'pilot creation must fail'
    assert @@pmtest.pmgr.extsvcqamsg.include?('NonProBank'), 'wrong err msg, wrong setup?'
  end

  def test62_pilot_as_srclot
    lot = @@testlots[0]
    assert @@sv.merge_lot_family(lot)
    assert_equal 0,  @@sv.lot_cleanup(lot, op: @@pmtest.ppd)
    assert lc = @@sv.lot_split(lot, 5)
    assert_equal 0, @@sv.user_parameter_change('Lot', lc, 'PilotEquipment', 'QAQA')
    assert_equal 0, @@sv.user_parameter_change('Lot', lc, 'PilotOperation', 'QAQA')
    assert_equal 0, @@sv.user_parameter_change('Lot', lc, 'PilotScenario', 'QAQA')
    refute @@pmtest.create_pilots('QA-000', lc), 'pilot creation must fail'
    assert @@pmtest.pmgr.extsvcqamsg.include?('Lot is already a pilot'), 'wrong err msg, wrong setup?'
  end

  def test63_eqpmonitor
    assert lot = @@pmtest.new_lot, 'error creating test lot'
    @@testlots << lot
    assert_equal 0, @@sv.sublottype_change(lot, 'Equipment Monitor')
    assert_equal 0, @@sv.lot_opelocate(lot, nil, op: @@pmtest.ppd)
    refute @@pmtest.create_pilots('QA-000', lot), 'pilot creation must fail'
    assert @@pmtest.pmgr.extsvcqamsg.include?('wrong lot type'), 'wrong err msg, wrong setup?'
  end

  def test65_toolwish_NEVER
    lot = @@testlots[0]
    assert @@sv.merge_lot_family(lot)
    assert_equal 0,  @@sv.lot_cleanup(lot, op: @@pmtest.ppd)
    assert_equal 0, @@sv.sublottype_change(lot, 'PO')
    assert @@pmtest.apc.set_toolwishes(@@sv.lot_info(lot), @@pmtest.eqp, 'NEVER')
    refute @@pmtest.create_pilots('QA-000', lot), 'pilot creation must fail'
    assert @@pmtest.pmgr.extsvcqamsg.include?('ToolWish'), 'wrong err msg, wrong setup?'
  end

  def test66_chamberwish_NEVER
    lot = @@testlots[0]
    assert @@sv.merge_lot_family(lot)
    assert_equal 0,  @@sv.lot_cleanup(lot, op: @@pmtest.ppd)
    assert_equal 0, @@sv.sublottype_change(lot, 'PO')
    assert @@pmtest.apc.set_toolwishes(@@sv.lot_info(lot), @@pmtest.eqp, 'not set', chambers: {'CH2'=>'NEVER'})
    refute @@pmtest.create_pilots('QA-000', lot, chambers: ['CH2']), 'pilot creation must fail'
    assert @@pmtest.pmgr.extsvcqamsg.include?('ChamberWish'), 'wrong err msg, wrong setup?'
  end

  def XXtest67_srclot_qt_at  # same as test68
    assert lot = @@pmtest.new_lot, 'error creating test lot'
    @@testlots << lot
    # srclot with QT at PPD
    assert_equal 0, @@sv.schdl_change(lot, route: 'UTRT-PILOTMGR-QT.PPD')
    assert_equal 0, @@sv.lot_opelocate(lot, nil, op: @@pmtest.ppd, offset: -2)
    assert_equal 0, @@sv.lot_gatepass(lot)
    assert_equal 0, @@sv.lot_opelocate(lot, 1)
    assert @@sv.lot_info(lot).qtime, 'lot has no qtime'
    # scenarios w/o srclot ONHOLD must pass
    assert @@pmtest.verify_scenario('QA-000', lot), 'test failed'
    # scenarios with srclot ONHOLD must fail
    refute @@pmtest.verify_scenario('QA-001', lot), 'test failed'
  end

  def test68_srclot_qt_before
    assert lot = @@pmtest.new_lot, 'error creating test lot'
    @@testlots << lot
    # srclot with QT before PPD
    assert_equal 0, @@sv.schdl_change(lot, route: 'UTRT-PILOTMGR-QT.PPD')
    assert_equal 0, @@sv.lot_opelocate(lot, nil, op: @@pmtest.ppd, offset: -2)
    assert_equal 0, @@sv.lot_gatepass(lot)
    assert @@sv.lot_info(lot).qtime, 'lot has no qtime'
    # scenario w/o srclot ONHOLD passes
    assert @@pmtest.verify_scenario('QA-000P', lot, checktoolwish: false), 'test failed'
    # scenario with srclot ONHOLD fails
    refute @@pmtest.verify_scenario('QA-001P', lot, checktoolwish: false), 'pilot creation must fail'
  end

end
