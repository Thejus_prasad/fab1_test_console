=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.


Author: sfrieske, 2022-01-18

Version: 6.0

=end

require 'misc/ammo'
require 'RubyTestCase'


# Compare AMMO responses between old and new versions
class AMMO_Compare < RubyTestCase
  # ProcessType/ProcessLayer (PD UDATA) combination
  @@pt = 'DISPO'
  @@pl = 'LOTSTART'
  # env to compare with
  @@env_ref = 'let'


  def self.startup
    super(nosiview: true)
  end

  def self.shutdown
    [@@ammo, @@ammo_ref].each {|a|
      ['APCUSER', 'GUIUSER', 'SUPERUSER'].each {|system|
        a.remove_wafer_selection(@@wafers, @@pt, @@pl, system: system)
      }
    }
  end


  def test00_setup
    $setup_ok = false
    #
    @@ammo = AMMO::Client.new($env, logdir: "log/ammo/#{$env}")
    assert @@ammo.echo, 'no AMMO connection'
    @@ammo_ref = AMMO::Client.new(@@env_ref, logdir: "log/ammo/#{@@env_ref}")
    assert @@ammo_ref.echo, 'no AMMO ref connection'
    #
    # s = 'QAQAQAQA'
    # @@wafers = (1..3).collect {|i| "#{s}%02dQA" % i}
    @@wafers = ['QAQAQAQA09QA']
    #
    $setup_ok = true
  end

  def test11_must
    [@@ammo, @@ammo_ref].each {|a|
      ['APCUSER', 'GUIUSER', 'SUPERUSER'].each {|system|
        a.remove_wafer_selection(@@wafers, @@pt, @@pl, system: system)
      }
    }
    #
    # ensure the wafers are not registered in AMMO
    assert_nil @@ammo.get_selected_wafers(@@wafers, @@pt, @@pl), 'wafers are already known in AMMO'
    assert @@ammo.faultstring.include?('did not have an active selection'), 'wrong error message'
    #
    # set wafer selection, MUST (1)
    fname = 'set_1.xml'
    params = {export: fname, ts: Time.now.strftime('%F %T.%L')}
    assert @@ammo.set_wafer_selection_bymap(@@wafers, @@pt, @@pl, 1, params)
    assert @@ammo_ref.set_wafer_selection_bymap(@@wafers, @@pt, @@pl, 1, params)
    assert self.class.compare_response(fname)
    #
    # get_selected_wafers
    fname = 'get_1.xml'
    assert res = @@ammo.get_selected_wafers(@@wafers, @@pt, @@pl, export: fname)
    assert_equal @@wafers, res.keys, 'wrong data'
    assert resref = @@ammo_ref.get_selected_wafers(@@wafers, @@pt, @@pl, export: fname)
    assert_equal @@wafers, resref.keys, 'wrong data'
    assert self.class.compare_response_lines(fname)
    #
    # get_only_selected_wafers
    fname = 'getonly_1.xml'
    assert res = @@ammo.get_only_selected_wafers(@@wafers, @@pt, @@pl, export: fname)
    assert_equal @@wafers, res, 'wrong data'
    assert resref = @@ammo_ref.get_only_selected_wafers(@@wafers, @@pt, @@pl, export: fname)
    assert_equal @@wafers, resref, 'wrong data'
    assert self.class.compare_response(fname)
    #
    # get_all_selected_wafers_noconflict
    fname = 'getnoconflict_1.xml'
    assert res = @@ammo.get_all_selected_wafers_noconflict(@@wafers, @@pt, @@pl, export: fname)
    assert_equal @@wafers, res.keys, 'wrong data'
    assert resref = @@ammo_ref.get_all_selected_wafers_noconflict(@@wafers, @@pt, @@pl, export: fname)
    assert_equal @@wafers, resref.keys, 'wrong data'
    assert self.class.compare_response_lines(fname)
  end

  def test21_donotcare
    [@@ammo, @@ammo_ref].each {|a|
      ['APCUSER', 'GUIUSER', 'SUPERUSER'].each {|system|
        a.remove_wafer_selection(@@wafers, @@pt, @@pl, system: system)
      }
    }
    #
    # ensure the wafers are not registered in AMMO
    assert_nil @@ammo.get_selected_wafers(@@wafers, @@pt, @@pl), 'wafers are already known in AMMO'
    assert @@ammo.faultstring.include?('did not have an active selection'), 'wrong error message'
    #
    # set wafer selection, DONOTCARE (3)
    fname = 'set_3.xml'
    params = {export: fname, ts: Time.now.strftime('%F %T.%L')}
    assert @@ammo.set_wafer_selection_bymap(@@wafers, @@pt, @@pl, 3, params)
    assert @@ammo_ref.set_wafer_selection_bymap(@@wafers, @@pt, @@pl, 3, params)
    assert self.class.compare_response(fname)
    #
    # get_selected_wafers
    fname = 'get_3.xml'
    assert res = @@ammo.get_selected_wafers(@@wafers, @@pt, @@pl, export: fname)
    assert_equal @@wafers, res.keys, 'wrong data'
    assert resref = @@ammo_ref.get_selected_wafers(@@wafers, @@pt, @@pl, export: fname)
    assert_equal @@wafers, res.keys, 'wrong data'
    assert self.class.compare_response_lines(fname)
    #
    # get_only_selected_wafers
    fname = 'getonly_3.xml'
    assert res = @@ammo.get_only_selected_wafers(@@wafers, @@pt, @@pl, export: fname)
    assert_equal [], res, 'wrong data'
    assert resref = @@ammo_ref.get_only_selected_wafers(@@wafers, @@pt, @@pl, export: fname)
    assert_equal [], resref, 'wrong data'
    assert self.class.compare_response(fname)
    #
    # get_all_selected_wafers_noconflict
    fname = 'getnoconflict_3.xml'
    assert res = @@ammo.get_all_selected_wafers_noconflict(@@wafers, @@pt, @@pl, export: fname)
    assert_equal @@wafers, res.keys, 'wrong data'
    assert resref = @@ammo_ref.get_all_selected_wafers_noconflict(@@wafers, @@pt, @@pl, export: fname)
    assert_equal @@wafers, resref.keys, 'wrong data'
    assert self.class.compare_response_lines(fname)
  end


  # aux methods

  def self.compare_response(fname)
    s = File.binread(File.join(@@ammo.logdir, fname))
    sref = File.binread(File.join(@@ammo_ref.logdir, fname))
    if s != sref
      $log.warn "#{fname} not matching"
      return
    end
    return true
  end

  def self.compare_response_lines(fname)
    lines = File.binread(File.join(@@ammo.logdir, fname)).split("\n")
    linesref = File.binread(File.join(@@ammo_ref.logdir, fname)).split("\n")
    linesref.each_with_index {|line, i|
      next if line.include?('<timestamp')   # skip timestamps, TODO: parse
      if line != lines[i]
        return
      end
    }
    return true
  end

end
