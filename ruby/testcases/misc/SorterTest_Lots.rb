=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: sfrieske, 2021-09-02

History:

Notes:
  for recovery/re-rceation of ITDC SorterTest lots
=end

require 'util/waitfor'
require 'SiViewTestCase'


class SorterTest_Lots < SiViewTestCase
  @@sv_defaults = {product: 'ITDC-0003.01', route: 'ITDC-0003.01'}
  @@carriers = {
    'SAT1'=>{
      lotfamily: 'U8001.00',
      waferids: %w(SAT01-0001 SAT01-0002 SAT01-0003 SAT01-0004 SAT01-0005
                   SAT01-0006 SAT01-0007 SAT01-0008 SAT01-0009 SAT01-0010
                   SAT01-0011 SAT01-0012 SAT01-0013 SAT01-0014 SAT01-0015
                   SAT01-0016 SAT01-0017 SAT01-0018 SAT01-0019 SAT01-0020 SAT01-0021 SAT01-0022 SAT01-0023 SAT01-0024 SAT01-0025),
      opNo: 'CEI01.1000'
    },
    'SAT5'=>{
      lotfamily: 'U8003.00',
      waferids: %w(WSR-SAT03-21 WSR-SAT03-22 WSR-SAT03-23 SAT03-0024 SAT03-0025),
      slots: [21, 22, 23, 24, 25],
      opNo: 'CEI03.1000'
    },
    'SAT6'=>{
      lotfamily: 'U8004.00',
      waferids: %w(SAT04-0001 SAT04-0002 SAT04-0003 SAT04-0004 SAT04-0005 SAT04-0006 SAT04-0007 SAT04-0008 SAT04-0009 SAT04-0010
                   SAT04-0011 SAT04-0012 SAT04-0013 SAT04-0014 SAT04-0015 SAT04-0016 SAT04-0017 SAT04-0018 SAT04-0019 SAT04-0020
                   SAT04-0021 SAT04-0022 SAT04-0023 SAT04-0024 SAT04-0025),
      opNo: 'CEI04.1000'
    },
    'SATF'=>{
      lotfamily: 'U8006.00',
      waferids: %w(SAT06-0001 SAT06-0002 SAT06-0003 SAT06-0004 SAT06-0005 SAT06-0006 SAT06-0007 SAT06-0008 SAT06-0009 SAT06-0010
                   SAT06-0011 SAT06-0012 SAT06-0013 SAT06-0014 SAT06-0015),
      opNo: 'CEI06.1000'
    },
    'SATJ'=>{
      lotfamily: 'U8007.00',
      waferids: %w(SAT07-0001B7 SAT07-0002B2 SAT07-0003A5 SAT07-0004A0 SAT07-0005G6
                   SAT07-0006G1 SAT07-0007F4 SAT07-0008E7 SAT07-0009E2 SAT07-0010E7
                   SAT07-0011E2 SAT07-0012D5 SAT07-0013D0 SAT07-0014C3 SAT07-0015B6
                   SAT07-0016B1 SAT07-0017A4 SAT07-0018H2 SAT07-0019G5 SAT07-0020H2),
      opNo: 'CEI07.1000'
    },
    'SATM'=>{
      lotfamily: 'U8009.00',
      waferids: %w(WSR-SAT09-01 WSR-SAT09-02 WSR-SAT09-03 WSR-SAT09-04 WSR-SAT09-05),
      opNo: 'CEI09.1000'
    },
    'SATQ'=>{
      lotfamily: 'U8010.00',
      waferids: %w(SAT10-GR01 SAT10-GR02 SAT10-GR03 SAT10-GR04 SAT10-GR05
                   SAT10-GR06 SAT10-GR07 SAT10-GR08 SAT10-GR09 SAT10-GR10
                   SAT10-GR11 SAT10-GR12 SAT10-GR13 SAT10-GR14 SAT10-GR15),
      opNo: 'CEI10.1000'
    }
  }
  @@splits = {
    @@carriers['SAT1'][:lotfamily]=>[
      'U8001.01'=>{
        waferids: %w(SAT01-0006 SAT01-0007 SAT01-0008 SAT01-0009 SAT01-0010)
      },
      'U8001.02'=>{
        waferids: %w(SAT01-0001 SAT01-0002 SAT01-0003 SAT01-0004 SAT01-0005)
      }
    ],
    @@carriers['SATF'][:lotfamily]=>[
      'U8006.01'=>{
        waferids: %w(SAT06-0011 SAT06-0012 SAT06-0013 SAT06-0014 SAT06-0015)
      }
    ]
  }
  # for testing
  @@lot_replace = ['U80', 'QA6']
  @@wafer_replace = ['SAT', 'QA6']
  @@testing = true

  @@lots = []


  def self.after_all_passed
    # do nothing
  end


  def test00
  end

  def testman11
    $log.info "\ncreating lots in the test carriers"
    @@carriers.each_pair {|carrier, data|
      $log.info "-- #{carrier}"
      # create lot
      lot = data[:lotfamily]
      lot = lot.gsub(*@@lot_replace) if @@testing != false
      waferids = data[:waferids]
      waferids = waferids.collect {|e| e.gsub(*@@wafer_replace)} if @@testing != false
      assert @@svtest.cleanup_carrier(carrier)
      assert @@svtest.new_lot(carrier: carrier, lot: lot, waferids: waferids, slots: data[:slots])
      @@lots << lot
      assert_equal 0, @@sv.lot_opelocate(lot, data[:opNo])
      # optionally split lot
      if splits = @@splits[data[:lotfamily]]
        splits.each {|split|
          split.values.each {|v|
            waferids = v[:waferids]
            waferids = waferids.collect {|e| e.gsub(*@@wafer_replace)} if @@testing != false
            assert @@sv.lot_split(lot, waferids)
          }
        }
      end
    }
  end

end
