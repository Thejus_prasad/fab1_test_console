=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.


Author: Steffen Frieske, 2021-08-31

History:

Notes:
=end

require_relative 'QT_Scenarios'


class QT_Scenarios4 < QT_Scenarios
  # 2 consecutive QTs, 2nd QT starting at opNo_tgt1
  @@sv_defaults = {product: 'UT-QT-SCENARIOS-2.01', route: 'UT-QT-SCENARIOS-2.01', nwafers: 5, carriers: 'QT%'}


  def test401_qt2_locateback_into1
    assert lot = @@svtest.new_lot, 'error creating test lot'
    @@testlots << lot
    # process/gatepass at trigger QT
    assert_equal 0, @@sv.lot_opelocate(lot, @@opNo_trg1), 'error locating lot'
    assert_equal 0, @@sv.lot_gatepass(lot), 'error gatepassing lot'
    refute_empty qtlist_ref1 = @@sv.lot_qtime_list(lot), 'lot is not in QT1'
    # process/gatepass at target QT/trigger QT2
    assert_equal 0, @@sv.lot_opelocate(lot, @@opNo_tgt1), 'error locating lot'
    assert_equal 0, @@sv.lot_gatepass(lot), 'error gatepassing lot'
    assert self.class.verify_qt_removed(lot, qtlist_ref1), 'QT1 not removed'
    refute_empty qtlist_ref2 = @@sv.lot_qtime_list(lot), 'lot is not in QT2'
    #
    # process/gatepass again after trg2 PD then locate back into QT1
    tref = Time.now
    #assert_equal 0, @@sv.lot_gatepass(lot), 'error gatepassing lot'
    assert_equal 0, @@sv.lot_opelocate(lot, @@opNo_tgt1), 'error locating lot'
    # lot has QT1 again, QT2 removed
    assert self.class.verify_qt_unchanged(lot, qtlist_ref1)  # currently fails
    assert self.class.verify_qt_removed(lot, qtlist_ref2), 'QT2 not removed'
  end

  def test411_qt2_rework_replace
    assert lot = @@svtest.new_lot, 'error creating test lot'
    @@testlots << lot
    # gatepass at trigger QT
    assert_equal 0, @@sv.lot_opelocate(lot, @@opNo_trg1), 'error locating lot'
    assert_equal 0, @@sv.lot_gatepass(lot), 'error gatepassing lot'
    refute_empty qtlist_ref1 = @@sv.lot_qtime_list(lot), 'lot is not in QT1'
    # process/gatepass at target QT/trigger QT2
    assert_equal 0, @@sv.lot_opelocate(lot, @@opNo_tgt1), 'error locating lot'
    assert_equal 0, @@sv.lot_gatepass(lot), 'error gatepassing lot'
    assert self.class.verify_qt_removed(lot, qtlist_ref1), 'QT1 not removed'
    refute_empty qtlist_ref2 = @@sv.lot_qtime_list(lot), 'lot is not in QT2'
    #
    # rework with replacement and gatepass back to main route, lot has QT1 retriggered
    tref = Time.now
    assert_equal 0, @@sv.lot_rework(lot, @@rte_rwk_replace)
    while @@sv.lot_info(lot).route != @@svtest.route
      assert_equal 0, @@sv.lot_gatepass(lot), 'error gatepassing lot'
      assert self.class.verify_qt_retriggered(lot, qtlist_ref1, tref)
      assert self.class.verify_qt_removed(lot, qtlist_ref2), 'QT2 not removed'
    end
  end

  def test412_qt2_rework_noreplace
    assert lot = @@svtest.new_lot, 'error creating test lot'
    @@testlots << lot
    # gatepass at trigger QT
    assert_equal 0, @@sv.lot_opelocate(lot, @@opNo_trg1), 'error locating lot'
    assert_equal 0, @@sv.lot_gatepass(lot), 'error gatepassing lot'
    refute_empty qtlist_ref1 = @@sv.lot_qtime_list(lot), 'lot is not in QT1'
    # process/gatepass at target QT/trigger QT2
    assert_equal 0, @@sv.lot_opelocate(lot, @@opNo_tgt1), 'error locating lot'
    assert_equal 0, @@sv.lot_gatepass(lot), 'error gatepassing lot'
    assert self.class.verify_qt_removed(lot, qtlist_ref1), 'QT1 not removed'
    refute_empty qtlist_ref2 = @@sv.lot_qtime_list(lot), 'lot is not in QT2'
    #
    # rework with replacement and gatepass back to main route, lot has original QT1
    assert_equal 0, @@sv.lot_rework(lot, @@rte_rwk_noreplace)
    while @@sv.lot_info(lot).route != @@svtest.route
      #assert_equal 0, @@sv.lot_gatepass(lot), 'error gatepassing lot'
      assert @@sv.claim_process_lot(lot), 'error processing lot'
      assert self.class.verify_qt_unchanged(lot, qtlist_ref1)
      assert self.class.verify_qt_removed(lot, qtlist_ref2), 'QT2 not removed'
    end
  end

end
