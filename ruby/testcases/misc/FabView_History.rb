=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: aschmid3, 2019-11-11

Version: 4.3.3

History:
  2020-05-07 sfrieske, changed wafer_history_report call
  2021-05-05 aschmid3, added different scenarios for display of FabView link: "Future Hold History"

=end

require 'SiViewTestCase'
require 'jcap/toolevents'


class FabView_History < SiViewTestCase

  @@testlots = []

  
  def self.after_all_passed
    @@testlots.each {|lot| @@sv.delete_lot_family(lot)}
  end
  
  
  def test00_setup
    $setup_ok = false
    #
    assert @@wh = ToolEvents::WaferHistory.new($env, sv: @@sv)
    #
    $setup_ok = true
  end
  
  def test11_Lot_processing
    ### Create lot with std UT-route and -product
    assert lot = @@svtest.new_lot(), 'error creating test lot'
    @@testlots << lot
    #
    puts "\n\nLot #{lot} is created."
    gets
    #
    ### Lot processing
    # don't use tool "UTS001" and 
    # if possible also not tools "...1" for other tool groups
    assert @@sv.claim_process_lot(lot, eqp: 'UTC003') # PD processing of 1st PD: UTPD000.01 - 'Process tool'
    assert @@sv.lot_hold(lot, 'TENG', memo: '1st QA test - FabView - UserHold History')
    assert_equal 0, @@sv.lot_gatepass(lot, force: true)  # GatePass of 2nd PD: UTPD000.02 incl. hold
    assert @@sv.lot_hold_release(lot, 'TENG')
    assert @@sv.claim_process_lot(lot, eqp: 'UTI002') # PD processing of 3rd PD: UTPD001.01 - 'Batch tool'
    assert_equal 0, @@sv.lot_gatepass(lot)             # GatePass of 4th PD: UTPD002.01
    assert @@sv.claim_process_lot(lot, eqp: 'UTS002') # PD processing of 5th PD: UTPD003.02 - 'Wafer Sorter tool'
    assert @@sv.lot_hold(lot, 'E-LA', memo: '2nd QA test - FabView - UserHold History')
    assert_equal 0, @@sv.lot_opelocate(lot, nil, op: 'UTPD005.02', force: true) # Locate incl. hold
    assert @@sv.lot_hold_release(lot, 'E-LA')
    assert @@sv.claim_process_lot(lot, eqp: 'UTF001') # PD processing of 8th PD: UTPD005.02 - 'Measurement tool' (only this tool allowed)
    assert_equal 0, @@sv.lot_opelocate(lot, nil, op: 'UTPD010.01')
    #TODO: Ensure that reticles are loaded
    assert @@sv.claim_process_lot(lot, eqp: 'UTR002') # PD processing of 13th PD: UTPD010.01 - 'Reticle tool'
    assert_equal 0, @@sv.lot_gatepass(lot)             # GatePass of 14th PD: UTPD011.01
    # PD processing of 15th PD - UTPD015.01 - with wafer history entries
    assert cj = @@sv.claim_process_lot(lot, eqp: 'UTF003') {|cj, cjinfo, eqpiraw, pstatus, mode, lis|
      li = @@sv.lot_info(lot, wafers: true)
      @@wh.wafer_history_report(cj, li, eqpiraw.equipmentID.identifier, pstatus.portID.identifier)
    }
    ### Different scenarios for display of FabView link: "Future Hold History"
    ## Put FHReg for 17th PD - UTPD000.01 (opNo 2000.100) with following locate
    # Scenario 1: Same lot ID is used for 'EventType' entries FUTUREHOLD and HOLD
    assert clot1 = @@sv.lot_split(lot, 1, memo: 'Split for scenario 1')
    assert @@sv.lot_futurehold(clot1, '2000.100', 'TENG', memo: 'Scenario 1: Same lot ID is used for EventType entries FUTUREHOLD and HOLD')
    assert_equal 0, @@sv.lot_opelocate(clot1, nil, op: 'UTPD000.01')
    # Scenario 2: Different lot IDs are used for 'EventType' entries FUTUREHOLD and HOLD
    assert clot2 = @@sv.lot_split(lot, 2, memo: 'Split for scenario 2')
    assert @@sv.lot_futurehold(clot2, '2000.100', 'TENG', memo: 'Scenario 2: Different lot IDs are used for EventType entries FUTUREHOLD and HOLD')
    assert gclot1 = @@sv.lot_split(clot2, 1, memo: 'Further split action for scenario 2')
    assert_equal 0, @@sv.lot_opelocate(gclot1, nil, op: 'UTPD000.01')
    #
    puts "\n\nLot #{lot} is processed (different histories available)."
    puts "\n\nSpecial history entry of link Future Hold History"
    puts "\n - Scenario 1: same lot ID for EventTypes FUTUREHOLD and HOLD - #{clot1}"
    puts "\n - Scenario 2: different lot IDs for EventTypes FUTUREHOLD and HOLD - #{clot2} and #{gclot1}"
    gets
    #
  end
  
  
end