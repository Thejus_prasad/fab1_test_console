=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.


Author: Steffen Frieske, 2021-08-06


History:

Notes:
  see https://gfoundries.sharepoint.com/:x:/r/sites/Fab1QTControlerReplacement/Shared%20Documents/General/QT_scenarios_RTD_vs_SiViewWD.xlsx
  lots are kept in the T_QTIME table only for ~ 24 hrs
=end

#require 'dbsequel'
require 'SiViewTestCase'


class QT_Scenarios < SiViewTestCase
  @@sv_defaults = {product: 'UT-QT-SCENARIOS.01', route: 'UT-QT-SCENARIOS.01', nwafers: 5, carriers: 'QT%'}
  @@opNo_trg1 = '1000.1200'
  @@opNo_tgt1 = '1000.1500'
  # RWK routes
  @@rwk_opNo_replace = '1000.2000'                # 1st opNo on rework route, with replace trigger
  @@rte_rwk_replace = 'UT-QT-SCENARIOS-RWK.01'    # with QT replcaement
  @@rte_rwk_noreplace = 'UT-QT-SCENARIOS-RWK.02'  # no QT replcaement
  @@rte_rwk_retrigger = 'UT-QT-SCENARIOS-RWK.03'  # RETRIGGER
  @@rte_rwk_delete = 'UT-QT-SCENARIOS-RWK.04'     # DELETE

  #@@mds_table = 'T_QTIME'
  #@@job_interval = 100        # runs every 60 s, plus slack, plus apf_sync!
  #@@skip_qtcontroller = true  # QT checks in SiView only

  @@testlots = []


  #def self.startup
  #  super
  #  @@mds = DB.new("mds.#{$env}")
  #end

  def self.after_all_passed
    @@testlots.each {|lot| @@sv.delete_lot_family(lot)}
  end


  # aux methods

  # ensure lot has a SiView QT, wait until the lot has an entry in T_QTIME; return T_QTIME entry
  def XXwait_lot_qtime(lot, params={})
    $log.info "verifying lot #{lot} has an active SiView QT"
    # assert res = @@sv.lot_qtime_list(lot)
    # refute_empty qtinfo = res.first.strQtimeInfo, 'lot has no SiView QT'
    refute_empty @@sv.lot_qtime_list(lot), 'lot has no SiView QT'
    if @@skip_qtcontroller
      $log.info '  skipping QT Controller check'
      return true
    else
      $log.info "waiting up to #{@@job_interval} s for lot #{lot} to become active in T_QTIME"
      entry = nil
      tend = Time.now + (params[:timeout] || @@job_interval)
      loop {
        entry = self.class.mds_qtime(lot: lot, trg_opNo: params[:trg_opNo]).first
        return entry if entry && entry[:active] == 1
        return nil if Time.now >= tend
        sleep 10
      }
    end
  end

  # ensure lot has a SiView QT, wait until the lot has an entry in T_QTIME; return true on success
  def XXwait_lot_noqtime(lot)
    $log.info "verifying lot #{lot} has no active SiView QT"
    assert_empty @@sv.lot_qtime_list(lot), 'lot has a SiView QT'
    if @@skip_qtcontroller
      $log.info '  skipping QT Controller check'
      return true
    else
      $log.info "waiting up to #{@@job_interval} s for lot #{lot} to become inactive in T_QTIME"
      entry = nil
      tend = Time.now + @@job_interval
      loop {
        entry = self.class.mds_qtime(lot: lot).first
        return true if entry.nil? || entry[:active] == 0
        return nil if Time.now >= tend
        sleep 10
      }
    end
  end

  # def self.mds_qtime(params={})
  #   table = params[:history] ? 'T_QTIME_HIST' : 'T_QTIME'
  #   qargs = []
  #   q = "SELECT LOT_ID, START_PD, END_PD, QTIME, TRIGGER_OPE_NO, TARGET_OPE_NO,
  #     TARGET_TIME, HOLD_ACTION, QT_ACTIVE from #{table}"
  #   q, qargs = @@mds._q_restriction(q, qargs, 'LOT_ID', params[:lot])
  #   q, qargs = @@mds._q_restriction(q, qargs, 'TRIGGER_OPE_NO', params[:trg_opNo])
  #   res = @@mds.select(q, *qargs) || return
  #   return res.collect {|r|
  #     Hash[[:lot, :startpd, :endpd, :qtime, :trg_opNo, :tgt_opNo, :tgt_time, :holdaction, :active].zip(r)]
  #   }
  # end

  # return true on success
  def self.verify_qt_unchanged(lot, qtlist_ref)
    $log.info "verify_qt_unchanged #{lot}"
    qtlist = @@sv.lot_qtime_list(lot)
    ($log.warn "  found #{qtlist.size} QTs"; return) if qtlist.size != 1
    qtinfo = qtlist.first.strQtimeInfo.first
    ($log.warn 'lot has no SiView QT'; return) if qtinfo.nil?
    qtinfo_ref = qtlist_ref.first.strQtimeInfo.first
    opNo_trg_ref = qtinfo_ref.qrestrictionTriggerOperationNumber
    opNo_trg = qtinfo.qrestrictionTriggerOperationNumber
    ($log.warn "wrong QT, trigger opNo: #{opNo_trg}, expected: #{opNo_trg_ref}"; return) if opNo_trg != opNo_trg_ref
    ttrigger_ref = qtinfo_ref.qrestrictionTriggerTimeStamp
    ttrigger = qtinfo.qrestrictionTriggerTimeStamp
    ($log.warn "wrong trigger time: #{ttrigger}, expected: #{ttrigger_ref}"; return) if ttrigger != ttrigger_ref
    return true
  end

  # return true on success
  def self.verify_qt_retriggered(lot, qtlist_ref, tref_new)
    $log.info "verify_qt_retriggered #{lot}, #{tref_new}"
    qtlist = @@sv.lot_qtime_list(lot)
    ($log.warn "  found #{qtlist.size} QTs"; return) if qtlist.size != 1
    qtinfo = qtlist.first.strQtimeInfo.first
    ($log.warn 'lot has no SiView QT'; return) if qtinfo.nil?
    qtinfo_ref = qtlist_ref.first.strQtimeInfo.first
    opNo_tgt_ref = qtinfo_ref.qrestrictionTargetOperationNumber
    opNo_tgt = qtinfo.qrestrictionTargetOperationNumber
    ($log.warn "wrong target opNo: #{opNo_tgt}, expected: #{opNo_tgt_ref}"; return) if opNo_tgt != opNo_tgt_ref
    ttrigger = @@sv.siview_time(qtinfo.qrestrictionTriggerTimeStamp)
    ($log.warn "wrong trigger time: #{ttrigger}, expected: #{tref_new}"; return) if ttrigger < tref_new
    return true
  end

  # return true on success (for multiple QT tests)
  def self.verify_qt_removed(lot, qtlist_ref)
    qtinfo_ref = qtlist_ref.first.strQtimeInfo.first
    opNo_trg_ref = qtinfo_ref.qrestrictionTriggerOperationNumber
    $log.info "verify_qt_removed #{lot}  #{opNo_trg_ref}"
    qtinfos = @@sv.lot_qtime_list(lot).first.strQtimeInfo
    #$log.info "  found #{qtinfos.size} QTs"
    res = qtinfos.find {|e| e.qrestrictionTriggerOperationNumber == opNo_trg_ref}
    ($log.warn "QT not removed"; return) unless res.nil?
    return true
  end

end
