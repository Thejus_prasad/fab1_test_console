=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.


Author: Steffen Frieske, 2018-01-08

Version: 2.0.0

History
  2018-05-08 sfrieske, use copy_object with ondemand: true
  2018-10-18 sfrieske, improved error messages on missing UDATA and reticleset
  2021-04-06 sfrieske, changed oid([]) to oids([])
  2022-01-05 sfrieske, use create_xxx instead of copy_object, create missing UDATA values

Notes:
  Copy a product from Prod into ITDC, creating reticles for MRA department's tests.
  must set PG (UDATA DeviceID, WaferLayout), Tech, Reticle Set: only RGs
  For final object creation w/o QAMRA1 prefix call
    runtest 'MRA_ProductImport', 'itdc', parameters: {tgtprefix: '', product: 'a product in srcenv'}
=end

require 'siview'
require 'RubyTestCase'


class MRA_ProductImport < RubyTestCase
  @@srcenv = 'f1prodqaview'   # set to 'itdc' in parameters for dev
  @@product = nil             # must be passed in parameters, see above
  @@tgtprefix = 'QAMRA1-'     # must be set to '' in parameters for real imports, see above


  def self.startup
    @@sm = SiView::SM.new($env)
    @@sm_src = SiView::SM.new(@@srcenv)
  end


  def test00_setup
    $setup_ok = false
    #
    if @@tgtprefix.start_with?('QAMRA')  # protection from empty tgtprefix (which would be normal after tc dev!)
      assert self.class.delete_all_testobjects
    end
    assert @@product, 'no product passed at the command line'
    refute_empty @@product, 'empty product name'
    assert @@prodinfo_src = @@sm_src.object_info(:product, @@product).first
    assert @@rsinfo_src = @@sm_src.object_info(:reticleset, @@prodinfo_src.specific[:reticleset]).first
    #
    $setup_ok = true
  end

  def test11_technology_productgroup
    $setup_ok = false
    #
    # get src env productgroup and technology data
    assert pginfo_src = @@sm_src.object_info(:productgroup, @@prodinfo_src.specific[:productgroup]).first
    #
    # create tech on demand
    assert techinfo_src = @@sm_src.object_info(:technology, pginfo_src.specific[:technology]).first
    techname = @@tgtprefix + pginfo_src.specific[:technology]
    assert @@sm.create_technology(techname, description: techinfo_src.description, ondemand: true)
    # TODO: create tech UDATA values on demand?
    # copy tech udata
    assert @@sm.update_udata(:technology, techname, techinfo_src.udata), "error copying UDATA for Tech #{techname}"
    #
    # create pg on demand
    pgname = @@tgtprefix + @@prodinfo_src.specific[:productgroup]
    assert @@sm.create_productgroup(pgname, techname, description: pginfo_src.description, ondemand: true)
    #   create UDATA value definitions, currently for ProductGroupProgramID only
    prgid = pginfo_src.udata['ProductGroupProgramID']
    desc = @@sm_src.object_info(:userdefinedcode, "ProgramID.#{prgid}").first.description
    assert @@sm.create_userdefinedcode("ProgramID.#{prgid}", description: desc, ondemand: true)
    # copy pg UDATA
    assert @@sm.update_udata(:productgroup, pgname, pginfo_src.udata), "error copying UDATA for PG #{pgname}"
    #
    $setup_ok = true
  end

  def test12_photolayers
    $setup_ok = false
    #
    @@rsinfo_src.specific[:rgs].collect {|e| e[:photolayer]}.uniq.each {|name|
      if @@sm.object_ids(:photolayer, @@tgtprefix + name).empty?
        desc = @@sm_src.object_info(:photolayer, name).first.description
        assert @@sm.create_photolayer(@@tgtprefix + name, description: desc)
        # skipped: UDATA
      end
    }
    #
    $setup_ok = true
  end

  def test13_reticlegroups
    $setup_ok = false
    #
    @@rsinfo_src.specific[:rgs].collect {|e| e[:rgs]}.flatten.uniq.each {|name|
      if @@sm.object_ids(:reticlegroup, @@tgtprefix + name).empty?
        desc = @@sm_src.object_info(:reticlegroup, name).first.description
        assert @@sm.create_reticlegroup(@@tgtprefix + name, description: desc)
      end
    }
    #
    $setup_ok = true
  end

  def test14_reticleset
    $setup_ok = false
    #
    name = @@prodinfo_src.specific[:reticleset]
    if @@sm.object_ids(:reticleset, @@tgtprefix + name).empty?
      data = @@rsinfo_src.specific[:rgs].collect {|e|
        {photolayer: @@tgtprefix + e[:photolayer], rgs: e[:rgs].collect {|rg| @@tgtprefix + rg}, override: e[:override]}
      }
      desc = @@sm_src.object_info(:reticleset, name).first.description
      assert @@sm.create_reticleset(@@tgtprefix + name, data, description: desc)
    end
    #
    $setup_ok = true
  end

  def test15_reticles
    $setup_ok = false
    #
    rnames = @@rsinfo_src.specific[:rgs].collect {|e| e[:rgs]}.flatten.uniq.collect {|rg| 
      @@sm_src.object_ids(:reticle, :all, search: {reticlegroup: rg}).collect {|e| e.identifier}
    }.flatten
    rnames.each {|name|
      if @@sm.object_ids(:reticle, @@tgtprefix + name).empty?
        rinfo = @@sm_src.object_info(:reticle, name).first
        assert @@sm.create_reticle(@@tgtprefix + name, @@tgtprefix + rinfo.specific[:rg], description: rinfo.description)
        assert @@sm.update_udata(:reticle, @@tgtprefix + name, rinfo.udata), "error copying UDATA for reticle #{@@tgtprefix + name}"
      end
    }
    #
    $setup_ok = true
  end

  def test19_product
    name = @@product
    if @@sm.object_ids(:product, @@tgtprefix + name).empty?
      desc = "Production copy of #{name} for MRA tests, do not touch!"
      assert @@sm.create_product(@@tgtprefix + name, @@tgtprefix + @@prodinfo_src.specific[:productgroup], 
        reticleset: @@tgtprefix + @@prodinfo_src.specific[:reticleset], description: desc)
      assert @@sm.update_udata(:product, @@tgtprefix + name, @@prodinfo_src.udata), "error copying UDATA for product #{@@tgtprefix + name}"
    end
  end

  # aux methods

  # delete all test objects matching the tgtprefix (from previous tests); return true on success
  def self.delete_all_testobjects
    $log.info "delete_all_testobjects"
    ($log.warn 'tgtprefix is empty, not deleting any objects'; return) if @@tgtprefix.empty?
    ($log.warn "tgtprefix #{@@tgtprefix.inspect} is not specific enough"; return) unless @@tgtprefix.start_with?('QAMRA')
    ret = true
    [:product, :productgroup, :technology, :reticleset, :reticle, :reticlegroup, :photolayer].each {|classname|
      $log.info "-- #{classname}"
      @@sm.object_ids(classname, @@tgtprefix + '*').each {|oid| 
        ret &= @@sm.delete_production(classname, oid) # if o.identifier != otemplate
      }
    }
    return ret
  end

  # create missing UDATA value definitions
  def self.create_udata_values
  end

end
