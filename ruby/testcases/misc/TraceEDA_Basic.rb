=begin
(c) Copyright 2016 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.


Author: Steffen Frieske, 2016-09-08

Version: 0.0.2

History:
  2020-01-03 sfrieske, renamed from Test_TraceEDA
  2021-08-27 sfrieske, require uniquestring instead of misc
=end

require 'traceeda/traceedatest'
require 'util/uniquestring'
require 'RubyTestCase'


# Test TraceEDA (WonderWare)
class TraceEDA_Basic < RubyTestCase
  @@eqp = 'LETCL1050'


  def self.startup
    super(nosiview: true)
  end

  def self.after_all_passed
  end

  def test00_setup
    $setup_ok = false
    #
    assert @@eas = TraceEDA::EAS.new($env)
    assert @@eas.connect
    assert @@eas.store_plan
    assert @@etc = TraceEDA::ETC.new(@@eqp, @@eas)
    assert @@hist = TraceEDA::WWHWS.new($env, etc: @@etc)
    assert @@eas.close
    #
    $setup_ok = true
  end

  def test01_happy
    $setup_ok = false
    #
    @@etc.collections.clear
    @@etc.report_times = []
    #
    # define plan
    assert @@etc.unresolve_plan
    assert @@etc.plan_mapping
    #
    # send report
    cj = "#{@@eqp}-#{Time.now.to_i}"
    recipesteps = 1
    lot = "QA#{unique_string}.00"
    waferbase = lot.split('.').first
    nwafers = 3
    nwafers.times {|nwafer|
      wafer = "WW#{waferbase}%2.2d" % nwafer
      @@etc.chambers.each {|ch|
        $log.info "-- report for wafer #{wafer}, chamber #{ch}"
        @@etc.report_event("PA:#{ch}/ProcessingStarted_Coronus",
          chamber: ch, cj: cj, lot: lot, wafer: wafer, slot: nwafer+1)
        recipesteps.times {|rstep|
          sleep 1
          # send a data collection
          assert colls = @@etc.create_collections
          @@etc.collections << colls
          assert @@etc.report(colls)
          @@etc.report_times << Time.now
        }
        @@etc.report_event("PA:#{ch}/ProcessingCompleted_Coronus",
          chamber: ch, cj: cj, lot: lot, wafer: wafer, slot: nwafer+1)
      }
    }
    #
    # retrieve data
    assert colls = @@etc.collections.shift
    assert_equal 0, @@hist.retrieve_compare(colls)
    #
    $setup_ok = true
  end

  def test11_plan_noevents
    assert @@etc.unresolve_plan
    # define plan with events only
    assert @@etc.plan_mapping(event_parameters: false)
  end

  def test12_plan_notrace
    assert @@etc.unresolve_plan
    # define plan with events only
    assert @@etc.plan_mapping(trace_parameters: false)
  end
end
