=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.


Author: Thomas Klose, 2016-12-02

History:
  2018-04-09 sfrieske, minor cleanup
  2020-01-03 sfrieske, cleaned up and renamed from Test_AMMO_Stress
  2021-07-20 sfrieske, remove unused parameters @@eqp and @@lot
  2021-08-27 sfrieske, require uniquestring instead of misc

Notes:
  as of V6 (2021) following methods are used:
    setWaferSelectionByMap
    getSelectedWafers
    getOnlySelectedWafers
    getAllSelectedWafersNoConflict
    removeWaferSelection
    maybe removeWaferSelectionForContext

  stats: http://f36wwmonp1:3638/cgi-bin/directDBAccessWithLegend/start.pl?app=AMMO&env=Prod
  as of 21-08-05: total msgs ~ 21k per h, up to 32k peak
    this test in LET: > 120k (18 threads), > 90k (4 threads), > 50k (2 threads), > 35k (1 thread)
    2 threads with 1000 loops each take < 30 minutes
    18 threads with 1000 loops each take < 90 minutes
=end

require 'misc/ammo'
require 'util/statistics'
require 'util/uniquestring'
require 'RubyTestCase'


# Stress test AMMO
class AMMO_Stress < RubyTestCase
  # ProcessType/ProcessLayer (PD UDATA) combinations
  @@combinations = [
    ['CFMWafer', 'LOTSTART'], ['DISPO', 'LOTSTART'], ['SectionWafer', 'LOTSTART'],
    ['CFMWafer', 'T-SRT'], ['DISPO', 'T-SRT'], ['SectionWafer', 'T-SRT'],
    ['CFMWafer', 'FIN'], ['DISPO', 'FIN'], ['SectionWafer', 'FIN'],
    ['CFMWafer', 'M1'], ['DISPO', 'M1'], ['SectionWafer', 'M1'],
    ['CFMWafer', 'C3'], ['DISPO', 'C3'], ['SectionWafer', 'C3'],
    ['CFMWafer', 'INIT'], ['DISPO', 'INIT'], ['SectionWafer', 'INIT'],
  ]

  @@nthreads = @@combinations.length
  @@loops = 10  # with 9 threads ~ 30 s, with 18 threads ~ 55 s


  def self.startup
    super(nosiview: true)
  end


  def test00_setup
    $setup_ok = false
    #
    @@clients = @@combinations.take(@@nthreads).collect {|e| AMMO::Client.new($env)}
    @@ammo = @@clients.first
    assert @@ammo.echo, 'no AMMO connection'
    #
    $setup_ok = true
  end

  def test11_stress
    assert @@ammo.echo, 'no AMMO connection'
    create_logger(file: "log/ammo_stress_#{Time.now.strftime('%F_%T').gsub(':', '-')}.log")
    res = []
    threads = []
    tstart = Time.now
    ###s = unique_string
    @@clients.each_with_index {|cl, i|
      pt, pl = @@combinations[i]
      threads << Thread.new {
        @@loops.times {|n|
          $log.info {"\n-- loop #{n+1}/#{@@loops}, client ##{i}"} if n % 10 == 0
          s = unique_string
          wafers = (1..25).collect {|i| "#{s}%02dAA" % i}
          ok = cl.set_wafer_selection_bymap(wafers, pt, pl, 3, silent: true)
          res << (ok ? cl.mq.mq_response.duration : nil)
          ok = cl.get_selected_wafers(wafers, pt, pl, parse: false)
          res << (ok ? cl.mq.mq_response.duration : nil)
          ok = cl.set_wafer_selection_bymap(wafers, pt, pl, 2, silent: true)
          res << (ok ? cl.mq.mq_response.duration : nil)
          ok = cl.get_selected_wafers(wafers, pt, pl, parse: false)
          res << (ok ? cl.mq.mq_response.duration : nil)
          ok = cl.set_wafer_selection_bymap(wafers, pt, pl, 0, silent: true)
          res << (ok ? cl.mq.mq_response.duration : nil)
          ok = cl.get_selected_wafers(wafers, pt, pl, parse: false)
          res << (ok ? cl.mq.mq_response.duration : nil)
          #
          ok = cl.set_wafer_selection_bymap(wafers, pt, pl, 1, silent: true)
          res << (ok ? cl.mq.mq_response.duration : nil)
          ok = cl.get_selected_wafers(wafers, pt, pl, parse: false)
          res << (ok ? cl.mq.mq_response.duration : nil)
          ok = cl.get_only_selected_wafers(wafers, pt, pl, parse: false)
          res << (ok ? cl.mq.mq_response.duration : nil)
          ok = cl.get_all_selected_wafers_noconflict(wafers, pt, pl, parse: false)
          res << (ok ? cl.mq.mq_response.duration : nil)
          #
          ['APCUSER', 'GUIUSER', 'SUPERUSER'].each {|system|
            # never fails, even if a wafer has no selection, as long as selecting system is valid
            ok = cl.remove_wafer_selection(wafers, pt, pl, system: system, silent: true)
            res << (ok ? cl.mq.mq_response.duration : nil)
          }
        }
      }
    }
    threads.each_with_index {|t, i|
      $log.info "joining thread #{i}"
      t.join
    }
    @@stats = stats = {tstart: tstart.clone, duration: Time.now - tstart, loops: @@loops,
      threads: threads.size, responsetime: res.clone, ok: !res.member?(nil)}
    show_stats(stats)
  end

end


def show_stats(stats)
  # display the results of a stress test
  res = stats[:responsetime]
  r = res.compact
  sdata = Statistics.calc(r)
  s = "\n\nAMMO Stress Test from #{stats[:tstart].strftime('%F %T')}\n"
  s += '=' * 47
  s += "\ntest duration:         #{stats[:duration]} s"
  s += "\nnumber of threads:     #{stats[:threads]}, #{stats[:loops]} loops"
  s += "\nnumber of calls:       %d  (%d per hour)" % [r.size, r.size / stats[:duration] * 3600]
  s += "\naverage call duration: %.1f s +- %.1f s (%d%%)\n" % [sdata[:average], sdata[:stddev], sdata[:stddev_pct]]
  $log.info s
end

