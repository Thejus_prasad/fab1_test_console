=begin
(c) Copyright 2016 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.


Author: Steffen Frieske, 2016-09-09

Version: 1.18

History:
  2017-11-17 sfrieske, renamed from Test_APC to APC_Baseline and added MQ communication as default
  2018-04-05 sfrieske, add new APC Eqp directly to workarea in test00_setup
  2019-08-19 sfrieske, added testcases for LotHoldWithReasonCode and InhibitReticleWithDuration
  2021-08-27 sfrieske, require uniquestring instead of misc
  2022-01-04 sfrieske, remove dedicated test eqp from workarea
=end

require 'catalyst/apcbaseline'
require 'catalyst/apcdb'
require 'util/uniquestring'
require 'SiViewTestCase'


# Test APC Baseline
class APC_Baseline < SiViewTestCase
  @@eqp_template = 'UTC003'
  @@eqp = 'UTC003APC'
  @@chamber = 'PM3'
  @@workarea = 'UT'
  @@eqp2 = 'UTF001'  # must have an MQ queue CEI.eqp_REQUEST01
  @@reticle = 'UTRETICLE0003'
  @@reason = 'XFDC'

  @@use_http = false
  @@testlots = []


  def self.shutdown
    @@sv.eqp_cleanup(@@eqp2)
    @@sv.inhibit_cancel(:reticle, @@reticle)
  end

  def self.after_all_passed
    @@testlots.each {|lot| @@sv.delete_lot_family(lot)}
    @@sm.edit_objects(:workarea, @@workarea) {|oinfos|
      ci = @@sm.extract_si(oinfos.first.classInfo)
      ci.equipments = (ci.equipments.to_a.delete_if {|e| e.identifier == @@eqp}).to_java(@@sm.jcscode.objectIdentifier_struct)
      oinfos.first.classInfo = @@sm.insert_si(ci)
    }
    @@sm.delete_objects(:eqp, @@eqp)
  end


  def test00_setup
    $setup_ok = false
    #
    @@apcbl = APC::BaselineTest.new($env, use_http: @@use_http)
    @@apcdb = APC::DB.new($env)
    @@apcei = APC::EIMQ.new($env, @@eqp2)
    assert @@apcbl.apc.catalyst.ping, "#{@@apcbl.apc.catalyst.inspect} is not working"
    #
    # create fresh test eqp to throw away after eqp note tests
    assert @@sm.delete_objects(:eqp, @@eqp)
    assert eqpinfos = @@sm.copy_object(:eqp, @@eqp_template, @@eqp)
    assert @@sm.edit_objects(:workarea, @@workarea) {|oinfos|
      ci = @@sm.extract_si(oinfos.first.classInfo)
      ci.equipments = (ci.equipments.to_a << eqpinfos.first.objectId).to_java(@@sm.jcscode.objectIdentifier_struct)
      oinfos.first.classInfo = @@sm.insert_si(ci)
    }
    #
    assert @@lot = @@svtest.new_lot, 'error creating test lot'
    @@testlots << @@lot
    #
    $setup_ok = true
  end

  def test11_lothold
    assert_equal 0, @@sv.lot_futurehold_cancel(@@lot, nil)
    cj = "QA-DUMMYCJ-#{unique_string}"
    data = {'LotProperties'=>{@@lot=>nil}}
    assert @@apcbl.testaction('LotHold', cj: cj, data: data)
    # verify FutureHold and memo
    assert fh = @@sv.lot_futurehold_list(@@lot, reason: @@reason).first, 'no future hold'
    assert fh.memo.include?("|CJID=#{cj},ResultID=ITDC-Integration-Test-X#{cj}X"), 'wrong memo'
  end

  def test12_lotnote
    data = {'LotProperties'=>{@@lot=>nil}}
    assert @@apcbl.testaction('LotNote', data: data)
    # skip the notes set by new_lot and STBMfgAttributes (ASN=Y)
    assert note = @@sv.lot_notes(@@lot).first
    assert_equal 'Fehlerklasse FaultFor_LotNote', note.title
  end

  def test13_lotopenote
    li = @@sv.lot_info(@@lot)
    data = {'LotProperties'=>{@@lot=>nil}}
    assert @@apcbl.testaction('LotOperationNote', op: li.op, data: data)
    assert notes = @@sv.lot_openotes(@@lot)
    assert_equal 1, notes.size
  end

  def test14_waferscriptparameter
    li = @@sv.lot_info(@@lot, wafers: true)
    wafer = li.wafers.first.wafer
    data = {'LotProperties'=>{@@lot=>{
      'Context'=>{'logicalRecipeName'=>'LR-QADUMMY.01'},
      'WaferProperties'=>{wafer=>{
        'history'=>'4 TransferRobotLowerEE LTRobotUpperEE CP10',
        'locationEnter'=>'09/04/2017 06:14:58 09/04/2017 06:20:17 09/04/2017 06:20:26 09/04/2017 06:20:35',
        'locationExit'=>'09/04/2017 06:20:17 09/04/2017 06:20:20 09/04/2017 06:20:35 NA',
        'slot'=>li.wafers.first.slot
      }}
    }}}
    assert @@apcbl.testaction('SetScriptParameter', data: data), 'APC test action error'
    assert_equal 'APC Baseline QA', @@sv.user_parameter('Wafer', wafer, name: 'UTstring').value, 'wrong script parameter'
  end

  # MSR 1114378 (http://f1onewiki:21080/display/AB/MSR1114378+-+MarkSplit)
  def test15_waferscriptparameter_marksplit
    li = @@sv.lot_info(@@lot, wafers: true)
    wafer = li.wafers.first.wafer
    data = {'LotProperties'=>{@@lot=>{
      'Context'=>{'logicalRecipeName'=>'LR-QADUMMY.01'},
      'WaferProperties'=>{wafer=>{
        'history'=>'4 TransferRobotLowerEE LTRobotUpperEE CP10',
        'locationEnter'=>'09/04/2017 06:14:58 09/04/2017 06:20:17 09/04/2017 06:20:26 09/04/2017 06:20:35',
        'locationExit'=>'09/04/2017 06:20:17 09/04/2017 06:20:20 09/04/2017 06:20:35 NA',
        'slot'=>li.wafers.first.slot
      }}
    }}}
    assert @@apcbl.testaction('SetScriptParameterMarkSplit', data: data), 'APC test action error'
    assert_equal "#{li.route}_#{li.opNo}_1_#{@@lot}_X-RW", @@sv.user_parameter('Wafer', wafer, name: 'MarkSplit').value, 'wrong script parameter'
  end

  def test16_lothold_with_reasoncode
    assert_equal 0, @@sv.lot_futurehold_cancel(@@lot, nil)
    cj = "QA-DUMMYCJ-#{unique_string}"
    reason = 'E-ENG'
    data = {'LotProperties'=>{@@lot=>nil}}
    assert @@apcbl.testaction('LotHoldWithReasonCode', cj: cj, reticle: reason, data: data)
    # verify FutureHold and memo
    assert fh = @@sv.lot_futurehold_list(@@lot, reason: reason).first, "no future hold with reason #{reason}"
    assert fh.memo.include?("|CJID=#{cj},ResultID=ITDC-Integration-Test-X#{cj}X"), 'wrong memo'
  end

  def test21_inhibit_eqp
    assert_equal 0, @@sv.eqp_cleanup(@@eqp)
    assert @@apcbl.testaction('InhibitEquipment', eqp: @@eqp)
    assert_equal 1, @@sv.inhibit_list(:eqp, @@eqp, reason: @@reason).size, 'missing eqp inhibit'
  end

  def test22_inhibit_eqp_group
    assert_equal 0, @@sv.eqp_cleanup(@@eqp)
    assert @@apcbl.testaction('InhibitEquipmentWithGroup', eqp: @@eqp)
    assert_equal 2, @@sv.inhibit_list(:eqp, @@eqp, reason: @@reason).size, 'missing eqp inhibit'
  end

  def test31_inhibit_chamber
    assert_equal 0, @@sv.eqp_cleanup(@@eqp)
    assert @@apcbl.testaction('InhibitChamber', eqp: @@eqp, chamber: @@chamber)
    assert_equal 1, @@sv.inhibit_list(:eqp_chamber, @@eqp, reason: @@reason).size, 'missing chamber inhibit'
  end

  def test32_inhibit_chamber_group
    assert_equal 0, @@sv.eqp_cleanup(@@eqp)
    assert @@apcbl.testaction('InhibitChamberWithGroup', eqp: @@eqp, chamber: @@chamber)
    assert_equal 2, @@sv.inhibit_list(:eqp_chamber, @@eqp, reason: @@reason).size, 'missing chamber inhibit'
  end

  def test33_inhibit_reticle
    assert_equal 0, @@sv.inhibit_cancel(:reticle, @@reticle)
    assert @@apcbl.testaction('InhibitReticle', reticle: @@reticle)
    assert_equal 1, @@sv.inhibit_list(:reticle, @@reticle, reason: @@reason).size, 'missing or wrong inhibit'
  end

  def test34_inhibit_reticle_duration
    assert_equal 0, @@sv.inhibit_cancel(:reticle, @@reticle)
    assert @@apcbl.testaction('InhibitReticleWithDuration', reticle: @@reticle)
    tstart = Time.now
    assert_equal 1, @@sv.inhibit_list(:reticle, @@reticle, reason: @@reason).size, 'missing or wrong inhibit'
    inh = @@sv.inhibit_list(:reticle, @@reticle, reason: @@reason).first
    duration = @@sv.siview_time(inh.tend) - tstart
    assert duration > 55 && duration < 65, "wrong inhibit duration: #{duration}"
  end

  def test35_inhibit_reticle_eqp
    assert_equal 0, @@sv.eqp_cleanup(@@eqp)
    assert_equal 0, @@sv.inhibit_cancel(:reticle, @@reticle)
    assert @@apcbl.testaction('InhibitEquipmentAndReticle', reticle: @@reticle, eqp: @@eqp)
    assert_equal 1, @@sv.inhibit_list(:reticle_eqp, [@@reticle, @@eqp], reason: @@reason).size, 'missing or wrong inhibit'
  end

  def test41_eqpnote
    assert_equal 0, @@sv.eqp_cleanup(@@eqp)
    assert @@apcbl.testaction('EquipmentNote', eqp: @@eqp)
    assert notes = @@sv.eqp_notes(@@eqp)
    assert_equal 1, notes.size, 'wrong number of eqp notes'
  end

  def test42_cancel_cj
    refute_nil @@apcei.mq.delete_msgs(nil)
    assert @@apcbl.testaction('CancelQueuedControlJobs', eqp: @@eqp2)
    assert msg = @@apcei.receive_msg
    assert_equal @@eqp2, msg[:cancelAllQueuedJobsWithReason][:requestList][:requestEntry][:toolID][nil]
  end

  def test43_pause_cj
    cj = unique_string
    refute_nil @@apcei.mq.delete_msgs(nil)
    assert @@apcbl.testaction('PauseControlJob', eqp: @@eqp2, cj: cj)
    assert msg = @@apcei.receive_msg, "no msg to EI"
    assert_equal cj, msg[:pauseJobs][:jobIDList][:jobID][nil], 'wrong msg to EI'
  end

  def test44_disable_chamber
    assert_equal 0, @@sv.eqp_cleanup(@@eqp2)
    chambers = @@sv.eqp_info(@@eqp2).chambers.collect {|ch| ch.chamber}
    refute_nil @@apcei.mq.delete_msgs(nil)
    # no response
    assert res = @@apcbl.testaction('DisableChamberOnTool', eqp: @@eqp2, chamber: chambers.first)
    assert entry = res.values.find {|v| v.keys.member?('testResultData,errorActionExecution')}
    assert entry['testResultData,errorActionExecution'].start_with?('{DisableChamberOnTool=Unable'), 'wrong response'
    @@apcei.receive_msg  # need to consume the msg
    # response OK
    res = nil
    th = Thread.new {res = @@apcbl.testaction('DisableChamberOnTool', eqp: @@eqp2, chamber: chambers.first)}
    assert msg = @@apcei.receive_msg, 'no msg to EI'
    assert @@apcei.send_response_ok, 'error sending reply'
    assert th.join(30)
    assert entry = res.values.find {|v| v.keys.member?('testResultData,errorActionExecution')}
    assert_equal '{}', entry['testResultData,errorActionExecution'], 'wrong response'
    # response Fault
    res = nil
    th = Thread.new {res = @@apcbl.testaction('DisableChamberOnTool', eqp: @@eqp2, chamber: chambers.first)}
    assert msg = @@apcei.receive_msg, 'no msg to EI'
    assert @@apcei.send_response_fault, 'error sending reply'
    assert th.join(30)
    assert entry = res.values.find {|v| v.keys.member?('testResultData,errorActionExecution')}
    refute_equal '{}', entry['testResultData,errorActionExecution'], 'wrong response (MSR 1239978)'
  end

  def test51_sampling
    li = @@sv.lot_info(@@lot, wafers: true)
    w = li.wafers[0]
    data = {'LotProperties'=>{@@lot=>{'WaferProperties'=>{
      w.wafer=>{'slot'=>w.slot, 'history'=>' ', 'locationEnter'=>' ', 'locationExit'=>' '}
    }}}}
    udata = @@sv.user_data_pd(li.op)
    assert @@apcbl.testaction('SamplingClass1', ProcessLayer: udata['ProcessLayer'], ProcessType: udata['ProcessTypeWfrSelection'], data: data)
    # verify DB entry for lot/null and lot/wafer
    ['IS NULL', w.wafer].each {|wafer|
      assert props = @@apcdb.rdb_material_props(lot: @@lot, wafer: wafer).first, "no DB entry for lot #{@@lot}, wafer #{wafer}"
      assert_equal udata['ProcessLayer'], props.processlayer, 'wrong processlayer'
      assert_equal udata['ProcessTypeWfrSelection'], props.processtype, 'wrong processtype'
    }
  end

  def testdev81_mail
    assert @@apcbl.testaction('ImmediateMailToFDCEngineer')
  end

end
