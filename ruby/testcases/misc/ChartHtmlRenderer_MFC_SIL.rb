=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: aschmid3, 2020-09-16

History:
  2020-09-16 aschmid3, 1) Copy of 'FabGUI_SC_MHLHR_Inhibit_SIL' and then required adjustments

=end

require 'sil/siltest'
require 'SiViewTestCase'


# Create data to test Chart-Html-Renderer display at corresponding MFC task entries
# via Space chart entries (SIL)
class ChartHtmlRenderer_MFC_SIL < SiViewTestCase
  @@sv_defaults = {route: 'SIL-0001.01', product: 'SILPROD.01', carriers: 'SIL%'}
  @@mpd = 'QA-MB-FTDEPM-LIS-CAEX.01'
  @@testlots = []

  def self.after_all_passed
    @@sv.eqp_cleanup(@@siltest.ptool)
    @@testlots.each {|lot| @@sv.delete_lot_family(lot)}
  end

  def test00_setup
    assert lots = @@svtest.new_lots(2), 'error creating testlots'
    @@testlots += lots
    @@siltest = SIL::Test.new($env, sv: @@sv, lot: lots)
    @@lds = @@siltest.lds_inline  # new for test12
    @@lds_setup = @@siltest.lds_setup # new for test12
    assert @@siltest.set_defaults(lot: lots, mpd: @@mpd), "no PPD for MPD #{@@mpd} ?"
  end

  # Create entries for MFC - TaskCategories of 'SPC...: INLINE' and 'SPC...: SETUP'
  def test11_SIL_InlineSetup
    # Tool cleanup
    assert_equal 0, @@sv.eqp_cleanup(@@siltest.ptool), "error cleaning up eqp #{@@siltest.ptool.inspect}"
    #
    # Create entries for MFC - TaskCategories - 'SPCInhibit: INLINE' and 'SPCLotHold: INLINE'
    lot1 = @@testlots[0]
    assert channels = @@siltest.setup_channels(lot: lot1, cas: ['EquipmentInhibit', 'AutoEquipmentInhibit', 'ReleaseEquipmentInhibit', 'AutoLotHold', 'CancelLotHold'])
    ## SpaceNavigator under:
    #   'Inline_Fab1' -> 'AUTOCHART' -> 'MODULE' -> 'AutoCreated_QA' -> 'SPC Channels' -> "<shown entry>"
    #     -> right mouse click -> 'Properties...' -> tab 'Valuations & Events' -> table of 'Valuations of Apply' 
    #     -> contained entries should show follow entries at table 'Events Assigned to Selected Valuations' below
    #        - for Inhibits: 'EquipmentInhibit', 'AutoEquipmentInhibit', 'ReleaseEquipmentInhibit' (these are 'Corrective Actions')
    #        - for Holds: 'AutoLotHold', 'CancelLotHold' (these are 'Corrective Actions')
    #        (possible CAs to find under: SpaceNavigator -> 'Events' -> 'DEFAULT_DIVISION' -> 'Corrective Actions')
    assert @@siltest.submit_process_dcr(lot: lot1), 'error sending DCR'
    assert dcr = @@siltest.submit_meas_dcr(ooc: true, nooc: 'all*2', lot: lot1), 'error submitting DCR'
    assert @@siltest.wait_inhibit(:eqp, @@siltest.ptool, '', n: channels.size), 'equipment must still have all inhibits'
    # Gate Pass necessary for LotHold activation due to HoldTiming = Post
    assert_equal 0, @@sv.lot_gatepass(lot1)
    #
    # Create entries for MFC - TaskCategories - 'SPCInhibit: SETUP' and 'SPCLotHold: SETUP'
    # indicator is 'Technology = TEST' or 'Technology = <empty>'
    lot2 = @@testlots[1]
    assert channels = @@siltest.setup_channels(lot: lot2, cas: ['EquipmentInhibit', 'AutoEquipmentInhibit', 'ReleaseEquipmentInhibit', 'AutoLotHold', 'CancelLotHold'], technology: 'TEST')
    ## SpaceNavigator under:
    #   'Setup_Fab1' -> 'AUTOCHART' -> 'MODULE' -> 'AutoCreated_QA' -> 'SPC Channels' -> "<shown entry>"
    #     -> right mouse click -> 'Properties...' -> tab 'Valuations & Events' -> table of 'Valuations of Apply' 
    #     -> contained entries should show follow entries at table 'Events Assigned to Selected Valuations' below
    #        - for Inhibits: 'EquipmentInhibit', 'AutoEquipmentInhibit', 'ReleaseEquipmentInhibit' (these are 'Corrective Actions')
    #        - for Holds: 'AutoLotHold', 'CancelLotHold' (these are 'Corrective Actions')
    #        (possible CAs to find under: SpaceNavigator -> 'Events' -> 'DEFAULT_DIVISION' -> 'Corrective Actions')
    assert @@siltest.submit_process_dcr(lot: lot2, technology: 'TEST'), 'error sending DCR'
    assert dcr = @@siltest.submit_meas_dcr(ooc: true, nooc: 'all*2', lot: lot2, technology: 'TEST'), 'error submitting DCR'
    assert @@siltest.wait_inhibit(:eqp, @@siltest.ptool, '', n: channels.size*2), 'equipment must still have all inhibits'
    # --> n: channels.size replaced by 10 (5*2 because tool has still previous inhibit entries from INLINE part)
    # Gate Pass necessary for LotHold activation due to HoldTiming = Post
    assert_equal 0, @@sv.lot_gatepass(lot2)
    #
    # HoldMemoUpdate only necessary for MFC task display due to introduction of MSR1696869 - Hold Code UDATA Change (new value "CustomDept")
    @@testlots.each {|lot|
      holds = @@sv.lot_hold_list(lot)
      holds.each {|hold|
        parts = hold.memo.split(',')
        parts[0] = 'DEPT->DIF'        # original value: DEPT->QA
        @@sv.lot_holdmemo_update(lot, parts.join(','), reason: hold.reason)   # TODO: exclude reason XSPC-P from holdmemo_update
      }
    }
    #
    # Summary
    puts "\n\n--> SIL Inhibit and LotHold test data are prepared for follow topics."
    puts "\n      - INLINE: with eqp #{@@siltest.ptool} and lot #{lot1}"
    puts "\n      - SETUP:  with eqp #{@@siltest.ptool} and lot #{lot2}"
    puts "\n\n--> Execute the tests and press Enter to continue with cleanup."
    gets
  end

  # Derived from test11 - with additional action of channel deletion for creating error at Chart-Renderer
  def test12_SIL_InlineSetup_withChDel
    # Tool cleanup
    assert_equal 0, @@sv.eqp_cleanup(@@siltest.ptool), "error cleaning up eqp #{@@siltest.ptool.inspect}"
    #
    # Create entries for MFC - TaskCategories - 'SPCInhibit: INLINE' and 'SPCLotHold: INLINE'
    lot1 = @@testlots[0]
    assert channels = @@siltest.setup_channels(lot: lot1, cas: ['EquipmentInhibit', 'AutoEquipmentInhibit', 'ReleaseEquipmentInhibit', 'AutoLotHold', 'CancelLotHold'])
    ## SpaceNavigator under:
    #   'Inline_Fab1' -> 'AUTOCHART' -> 'MODULE' -> 'AutoCreated_QA' -> 'SPC Channels' -> "<shown entry>"
    #     -> right mouse click -> 'Properties...' -> tab 'Valuations & Events' -> table of 'Valuations of Apply' 
    #     -> contained entries should show follow entries at table 'Events Assigned to Selected Valuations' below
    #        - for Inhibits: 'EquipmentInhibit', 'AutoEquipmentInhibit', 'ReleaseEquipmentInhibit' (these are 'Corrective Actions')
    #        - for Holds: 'AutoLotHold', 'CancelLotHold' (these are 'Corrective Actions')
    #        (possible CAs to find under: SpaceNavigator -> 'Events' -> 'DEFAULT_DIVISION' -> 'Corrective Actions')
    assert @@siltest.submit_process_dcr(lot: lot1), 'error sending DCR'
    assert dcr = @@siltest.submit_meas_dcr(ooc: true, nooc: 'all*2', lot: lot1), 'error submitting DCR'
    assert @@siltest.wait_inhibit(:eqp, @@siltest.ptool, '', n: channels.size), 'equipment must still have all inhibits'
    # Gate Pass necessary for LotHold activation due to HoldTiming = Post
    assert_equal 0, @@sv.lot_gatepass(lot1)
    #
    # Create entries for MFC - TaskCategories - 'SPCInhibit: SETUP' and 'SPCLotHold: SETUP'
    # indicator is 'Technology = TEST' or 'Technology = <empty>'
    lot2 = @@testlots[1]
    assert channels = @@siltest.setup_channels(lot: lot2, cas: ['EquipmentInhibit', 'AutoEquipmentInhibit', 'ReleaseEquipmentInhibit', 'AutoLotHold', 'CancelLotHold'], technology: 'TEST')
    ## SpaceNavigator under:
    #   'Setup_Fab1' -> 'AUTOCHART' -> 'MODULE' -> 'AutoCreated_QA' -> 'SPC Channels' -> "<shown entry>"
    #     -> right mouse click -> 'Properties...' -> tab 'Valuations & Events' -> table of 'Valuations of Apply' 
    #     -> contained entries should show follow entries at table 'Events Assigned to Selected Valuations' below
    #        - for Inhibits: 'EquipmentInhibit', 'AutoEquipmentInhibit', 'ReleaseEquipmentInhibit' (these are 'Corrective Actions')
    #        - for Holds: 'AutoLotHold', 'CancelLotHold' (these are 'Corrective Actions')
    #        (possible CAs to find under: SpaceNavigator -> 'Events' -> 'DEFAULT_DIVISION' -> 'Corrective Actions')
    assert @@siltest.submit_process_dcr(lot: lot2, technology: 'TEST'), 'error sending DCR'
    assert dcr = @@siltest.submit_meas_dcr(ooc: true, nooc: 'all*2', lot: lot2, technology: 'TEST'), 'error submitting DCR'
    assert @@siltest.wait_inhibit(:eqp, @@siltest.ptool, '', n: channels.size*2), 'equipment must still have all inhibits'
    # --> n: channels.size replaced by 10 (5*2 because tool has still previous inhibit entries from INLINE part)
    # Gate Pass necessary for LotHold activation due to HoldTiming = Post
    assert_equal 0, @@sv.lot_gatepass(lot2)
    #
    # Summary
    puts "\n\n--> SIL Inhibit and LotHold test data are prepared for follow topics."
    puts "\n      - INLINE: with eqp #{@@siltest.ptool} and lot #{lot1}"
    puts "\n      - SETUP:  with eqp #{@@siltest.ptool} and lot #{lot2}"
    puts "\n\n--> Verify shown entries at SPACE and MFC (incl. correct OCAP display)"
    puts "\n\n--> Press Enter to continue with special channel deletion"
    gets
    #
    # Channel deletion to activate an interruption of the 'Chart Renderer'
    # channels.spc_channels(name: 'QA_PARAM_902*').each {|ch| ch.delete}
    # assert_equal 0, @@siltest.spc_channels(name: 'QA_PARAM_902*').each {|ch| ch.delete}
    $log.info "Channel deletion has started. Wait up to 5 minutes for completion."  # msg incl. for user visibility
    @@siltest.setup_channels
    #[@@lds, @@lds_setup].each {|lds|
    #  lds.spc_channels(name: 'QA_PARAM_902*').each {|ch| ch.delete}
    #}
    #
    puts "\n\n--> Verify correct execution of channel deletion for SPACE and MFC entries" 
    puts "\n\n--> Execute the tests and press Enter to continue with cleanup."
    gets
  end

end
