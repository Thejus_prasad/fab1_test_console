=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.


Author: Steffen Frieske, 2016-05-11

History:
  2019-02-21 sfrieske, minor cleanup
  2021-02-05 sfrieske, use instance_of instead of kind_of
  2021-08-26 sfrieske, removed dependency on misc.rb (duration_in_seconds, duration_string)

Notes:
  typical test: runtest 'SAP_E10StatusChanges', 'let', tp: /test12/
=end

require 'util/durationstring'
require 'util/uniquestring'
require 'RubyTestCase'


# Stress test for MDS/SAP, replacing the SAP-to-SiView adaptor
class SAP_E10StatusChanges < RubyTestCase
  @@eqp1 = 'CVD1900'  #'ATC101'
  @@eqp2 = 'ETX1100'  #'UVC1500'
  @@ch2 = 'PM3'  #'CHA'
  @@e10states = ['1PRD', '1SUP', '1PRD', '2WPR']  # ['2WPR', '1PRD']

  @@reticle = '2196AK0PCA1'
  @@rparam = 'ParallelPrint'
  @@rstates = ['AVAILABLE', 'NOTAVAILABLE']

  @@results = {}
  @@e10rate = 10200    # changes per hour
  @@rrate = 5700       # changes per hour
  @@duration = 3600    # 1h


  def self.startup
    super
    # actions
    @@blk_reticle = proc {
      @@rstates.each {|state|
        @@sv.reticle_status_change(@@reticle, state)
        @@sv.user_parameter_change('Reticle', @@reticle, @@rparam, unique_string, 2, datatype: 'STRING')
      }
    }
    @@blk_e10 = proc {
      @@e10states.each {|state|
        @@sv.eqp_status_change(@@eqp1, state)
        @@sv.chamber_status_change(@@eqp2, @@ch2, state)
      }
    }
  end

  def self.shutdown
    $log.info "Summary:\n\n" + @@results.to_a.join("\n\n")
  end


  def setup
    assert_equal 0, @@sv.eqp_cleanup(@@eqp1, notify: false, mode: 'Off-Line-1')
    assert_equal 0, @@sv.eqp_cleanup(@@eqp2, notify: false, mode: 'Off-Line-1')
    assert_equal 0, @@sv.reticle_status_change(@@reticle, @@rstates.last, check: true)
  end


  def test01_verify_states
    @@e10states.each {|state|
      assert_equal 0, @@sv.eqp_status_change(@@eqp1, state)
    }
    @@e10states.each {|state|
      assert_equal 0, @@sv.chamber_status_change(@@eqp2, @@ch2, state)
    }
  end

  def test11_reticle
    a1 = actions_per_hour(@@duration, rate: @@rrate, nactions: @@rstates.length * 2, name: 'RTCL', &@@blk_reticle)
    res = @@results['Reticle'] = actions_result(a1)
    $log.info "\n" + res
  end

  def test12_reticle_e10
    a1 = actions_per_hour(@@duration, rate: @@rrate, nactions: @@rstates.length * 2, name: 'RTCL', &@@blk_reticle)
    a2 = actions_per_hour(@@duration, rate: @@e10rate, nactions: @@e10states.length * 2, name: 'E10', &@@blk_e10)
    res = @@results['Reticle+E10'] = actions_result([a1, a2])
    $log.info "\n" + res
  end

  def test13_reticle_e10_125
    a1 = actions_per_hour(@@duration, rate: @@rrate * 1.25, nactions: @@rstates.length * 2, name: 'RTCL125', &@@blk_reticle)
    a2 = actions_per_hour(@@duration, rate: @@e10rate * 1.25, nactions: @@e10states.length * 2, name: 'E10125', &@@blk_e10)
    res = @@results['Reticle+E10_125'] = actions_result([a1, a2])
    $log.info "\n" + res
  end

  def test14_reticle_e10_200
    a1 = actions_per_hour(@@duration, rate: @@rrate * 2, nactions: @@rstates.length * 2, name: 'RTCL200', &@@blk_reticle)
    a2 = actions_per_hour(@@duration, rate: @@e10rate * 2, nactions: @@e10states.length * 2, name: 'E10200', &@@blk_e10)
    res = @@results['Reticle+E10_200'] = actions_result([a1, a2])
    $log.info "\n" + res
  end

  def test15_reticle_e10_300
    a1 = actions_per_hour(@@duration, rate: @@rrate * 3, nactions: @@rstates.length * 2, name: 'RTCL300', &@@blk_reticle)
    a2 = actions_per_hour(@@duration, rate: @@e10rate * 3, nactions: @@e10states.length * 2, name: 'E10300', &@@blk_e10)
    res = @@results['Reticle+E10_300'] = actions_result([a1, a2])
    $log.info "\n" + res
  end



  # aux methods

  # calculate the action sleep time and return a hash containing thread and result fields
  def actions_per_hour(duration, params={}, &blk)
    duration = duration.to_f  # duration_in_seconds(duration).to_f
    rate = params[:rate] || 0  # per hour
    n = rate.to_f * duration / 3600.0
    nactions = params[:nactions] || 1  # number of counting actions in the block
    tdiff = duration * nactions / n    # *2 because combined changes are measured
    tnext = Time.now
    tend = Time.now + duration
    $log.info "-- actions_per_hour, tend: #{tend.strftime('%F_%T')}"
    ret = {name: params[:name] || 'worker', count: 0, duration: duration}
    t = Thread.new {
      Thread.current['name'] = ret[:name]
      ok = true
      loop {
        if rate > 0
          tsleep = (tnext - Time.now).round(1)
          tnext += tdiff
          ($log.info "sleeping #{tsleep}s"; sleep tsleep) if tsleep > 0
        end
        break if Time.now >= tend
        blk.call
        ret[:count] += nactions
      }
    }
    ret[:thread] = t
    return ret
  end

  # wait for action threads and print the result
  def actions_result(ares)
    ares = [ares] unless ares.instance_of?(Array)
    ares.each {|a| a[:thread].join}
    #
    ret = "Test from #{Time.now.utc.strftime('%F_%T')} (ending)"
    ret += "\n" + "=" * ret.length
    ares.each {|a|
      r = (a[:count].to_f * 3600.0 / a[:duration].to_f).round
      ret += "\n#{a[:name]}\tduration: #{duration_string(a[:duration])}, count: #{a[:count]}, events/h: #{r}"
    }
    return ret + "\n"
  end

end
