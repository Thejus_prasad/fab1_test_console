=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: sfrieske, 2018-12-10

History:
  2020-05-15 sfrieske, added wafer selection to prevent Pre1 script holds and skips

Note:
  on request by Thomas Kreutzmann, Janine Gierth
=end

require 'catalyst/waferselection'
require 'sil/siltest'
require 'SiViewTestCase'


# Create SiView lot and SIL process DCR to test MIC eCAP
class MICeCAP1 < SiViewTestCase
  @@sv_defaults = {route: 'C02-1522.P001', product: 'SHELBY21AJ.QA01', carriers: '%', carrier_category: 'MOL'}
  # test11
  @@op11 = 'CA-BARRDEPBSI.01'  #'CA-MSKAV013EM93X05-28BK.01'
  @@ptool11 = 'AMI4800'  # 'ALC1360'
  # test12
  @@op12DI = 'CA-BARRDEPDI.01'
  @@ptool12DI = 'DFS1614'
  @@op12DR = 'CA-BARRDEPDR.01'
  @@ptool12DR = 'SEM100'
  # test13
  @@op13pre = 'CA-BARRDEPDI.01'
  @@op13 = 'CA-BARRDEPF2DR.01'
  @@ptool13 = 'SEM100'

  @@testlots = []


  def self.after_all_passed
    @@sv.eqp_cleanup(@@siltest.ptool)
    $log.warn "\n\ntestlots: #{@@testlots}"
  end

  def self.manual_cleanup
    @@testlots.each {|lot| @@sv.delete_lot_family(lot)}
  end


  def test00_setup
    $setup_ok = false
    #
    assert @@wafersel = Catalyst::WaferSelection.new($env)
    assert @@siltest = SIL::Test.new($env, sv: @@sv)
    #
    $setup_ok = true
  end

  def test11_ptool
    assert lot = @@svtest.new_lot(nwafers: 10), 'error creating test lot'
    @@testlots << lot
    op, ptool = @@op11, @@ptool11
    assert_equal 0, @@sv.eqp_cleanup(ptool), "error cleaning up #{ptool}"
    udata = @@sv.user_data_pd(op)
    assert ptype = udata['ProcessTypeWfrSelection'], 'missing PD UDATA ProcessTypeWfrSelection'
    assert player = udata['ProcessLayer'], 'missing PD UDATA ProcessLayer'
    li = @@sv.lot_info(lot, wafers: true)
    @@wafersel.set_selection(li, ptype, player)
    assert_equal 0, @@sv.lot_opelocate(lot, nil, op: op), 'error locating lot'
    li = @@sv.lot_info(lot, wafers: true)
    assert wn = @@sv.what_next(ptool).find {|e| e.lot == lot}, 'lot is no candidate for processing'
    assert cj = @@sv.claim_process_lot(lot, eqp: ptool), 'error processing lot'
    assert chs = @@siltest.setup_channels(cas: ['EquipmentInhibit', 'AutoEquipmentInhibit', 'ReleaseEquipmentInhibit'])
    readings = Hash[li.wafers.collect {|w| [w.wafer, 45]}]
    lparms = {
      ptool: ptool, cj: cj, lot: lot, ppd: li.op, opNo: li.opNo, opName: li.opName, route: li.route,
      product: li.product, productgroup: li.productgroup, technology: li.technology,
      lrecipe: wn.lrecipe, mrecipe: wn.mrecipe, readings: readings, export: "log/sil/#{lot}.xml"
    }
    chambers = @@sv.eqp_info(ptool).chambers
    lparms[:chambers] = [chambers.first.chamber] unless chambers.empty?
    assert @@siltest.submit_process_dcr(lparms), 'error submitting process DCR'
  end

  def test12_DiDr
    assert lot = @@svtest.new_lot(nwafers: 3), 'error creating test lot'
    @@testlots << lot
    assert @@siltest.setup_channels(cas: ['EquipmentInhibit', 'AutoEquipmentInhibit', 'ReleaseEquipmentInhibit'])
    [[@@op12DI, @@ptool12DI], [@@op12DR, @@ptool12DR]].each {|op, ptool|
      $log.info "\n-- creating setup for #{op} / #{ptool}"
      assert_equal 0, @@sv.eqp_cleanup(ptool), "error cleaning up #{ptool}"
      assert_equal 0, @@sv.lot_opelocate(lot, nil, op: op), 'error locating lot'
      li = @@sv.lot_info(lot, wafers: true)
      assert wn = @@sv.what_next(ptool).find {|e| e.lot == lot}, 'lot is no candidate for processing'
      assert cj = @@sv.claim_process_lot(lot, eqp: ptool), 'error processing lot'
      # assert @@siltest.setup_channels(cas: ['EquipmentInhibit', 'AutoEquipmentInhibit', 'ReleaseEquipmentInhibit'])  # once?
      readings = Hash[li.wafers.collect {|w| [w.wafer, 45]}]
      chs = @@sv.eqp_info(ptool).chambers
      ch = chs.empty? ? nil : [chs.first.chamber]  # SEM100 has no chamber
      lparms = {
        ptool: ptool, chambers: ch, cj: cj,
        lot: lot, ppd: li.op, opNo: li.opNo, opName: li.opName, route: li.route,
        product: li.product, productgroup: li.productgroup, technology: li.technology,
        lrecipe: wn.lrecipe, mrecipe: wn.mrecipe, readings: readings, export: "log/sil/#{lot}.xml"
      }
      assert @@siltest.submit_process_dcr(lparms), 'error submitting process DCR'
    }
  end

  def test13_DR
    @@siltest.setup_channels(cas: ['EquipmentInhibit', 'AutoEquipmentInhibit', 'ReleaseEquipmentInhibit'])
    assert lot = @@svtest.new_lot(nwafers: 10), 'error creating test lot'
    @@testlots << lot
    op, ptool = @@op13, @@ptool13
    $log.info "\n-- gatepassing lot from #{@@op13pre} .. #{op}"
    assert_equal 0, @@sv.lot_opelocate(lot, nil, op: @@op13pre), 'error locating lot'
    while @@sv.lot_info(lot).op != op
      assert_equal 0, @@sv.lot_gatepass(lot, mandatory: true), 'error gatepassing lot'
    end
    $log.info "\n-- creating setup for #{op} / #{ptool}"
    assert_equal 0, @@sv.eqp_cleanup(ptool), "error cleaning up #{ptool}"
    ### assert_equal 0, @@sv.lot_opelocate(lot, nil, op: op), 'error locating lot'
    li = @@sv.lot_info(lot, wafers: true)
    assert wn = @@sv.what_next(ptool).find {|e| e.lot == lot}, 'lot is no candidate for processing'
    assert cj = @@sv.claim_process_lot(lot, eqp: ptool), 'error processing lot'
    readings = Hash[li.wafers.collect {|w| [w.wafer, 45]}]
    chs = @@sv.eqp_info(ptool).chambers
    ch = chs.empty? ? nil : [chs.first.chamber]  # SEM100 has no chamber
    lparms = {
      ptool: ptool, chambers: ch, cj: cj,
      lot: lot, ppd: li.op, opNo: li.opNo, opName: li.opName, route: li.route,
      product: li.product, productgroup: li.productgroup, technology: li.technology,
      lrecipe: wn.lrecipe, mrecipe: wn.mrecipe, readings: readings, export: "log/sil/#{lot}.xml"
    }
    assert @@siltest.submit_process_dcr(lparms), 'error submitting process DCR'
  end

  def test22_BARRDEPDR
    # Rsp chamber spec (ITDC): C07-00001314
    assert lot = @@svtest.new_lot(nwafers: 10), 'error creating test lot'
    # assert lot = @@svtest.new_lot(nwafers: 1), 'error creating test lot'
    @@testlots << lot
    #
    assert chs = @@siltest.setup_channels(cas: ['EquipmentInhibit', 'AutoEquipmentInhibit', 'ReleaseEquipmentInhibit'])
    opspre = %w(CA-MSKAV013EM93X05-28BK.01 CA-BARRDEP-8821.01 CA-BARRDEPSCBPSTCLN-10770.01)
    opspre.each_with_index {|op, idx|
      assert_equal 0, @@sv.lot_opelocate(lot, nil, op: op, force: :ondemand), 'error locating lot'
      li = @@sv.lot_info(lot, wafers: true)
      # assert lop = lops.find {|e| e.op == op}, "no such operation: #{op}"
      ptool = li.opEqps.first
      readings = Hash[li.wafers.collect {|w| [w.wafer, 45]}]
      lparms = {
        ptool: ptool, cj: "CJQA-#{ptool}",
        lot: lot, ppd: li.op, opNo: li.opNo, opName: li.opName, route: li.route,
        product: li.product, productgroup: li.productgroup, technology: li.technology,
        lrecipe: "LRQA-#{ptool}", mrecipe: "MRQA-#{ptool}", readings: readings,
        export: "log/sil/#{lot}-#{idx}-#{ptool}-#{op}.xml"
      }
      chambers = @@sv.eqp_info(ptool).chambers
      lparms[:chambers] = [chambers.last.chamber] unless chambers.empty?
      assert @@siltest.submit_process_dcr(lparms), "error submitting process DCR for #{ptool}"
    }

    # opspre.each {|op|
    #   assert_equal 0, @@sv.lot_opelocate(lot, nil, op: op, force: :ondemand), 'error locating lot'
    #   assert_equal 0, @@sv.lot_hold_release(lot, nil)
    #   eqp = @@sv.lot_info(lot).opEqps.first
    #   assert_equal 0, @@sv.eqp_cleanup(eqp), "error cleaning up #{eqp}"
    #   assert @@sv.claim_process_lot(lot, eqp: eqp), 'error processing lot'
    # }

    op, ptool = @@op12DR, @@ptool12DR
    assert_equal 0, @@sv.eqp_cleanup(ptool), "error cleaning up #{ptool}"
    udata = @@sv.user_data_pd(op)
    assert ptype = udata['ProcessTypeWfrSelection'], 'missing PD UDATA ProcessTypeWfrSelection'
    assert player = udata['ProcessLayer'], 'missing PD UDATA ProcessLayer'
    li = @@sv.lot_info(lot, wafers: true)
    @@wafersel.set_selection(li, ptype, player)
    assert_equal 0, @@sv.lot_opelocate(lot, nil, op: op, force: :ondemand), 'error locating lot'
    assert_equal 0, @@sv.lot_hold_release(lot, nil)
    li = @@sv.lot_info(lot, wafers: true)
    assert wn = @@sv.what_next(ptool).find {|e| e.lot == lot}, 'lot is no candidate for processing'
    assert cj = @@sv.claim_process_lot(lot, eqp: ptool), 'error processing lot'
    # assert chs = @@siltest.setup_channels(cas: ['EquipmentInhibit', 'AutoEquipmentInhibit', 'ReleaseEquipmentInhibit'])
    readings = Hash[li.wafers.collect {|w| [w.wafer, 45]}]
    lparms = {
      ptool: ptool, cj: cj,
      lot: lot, ppd: li.op, opNo: li.opNo, opName: li.opName, route: li.route,
      product: li.product, productgroup: li.productgroup, technology: li.technology,
      lrecipe: wn.lrecipe, mrecipe: wn.mrecipe, readings: readings, export: "log/sil/#{lot}.xml"
    }
    chambers = @@sv.eqp_info(ptool).chambers
    lparms[:chambers] = [chambers.first.chamber] unless chambers.empty?
    assert @@siltest.submit_process_dcr(lparms), 'error submitting process DCR'
  end

end
