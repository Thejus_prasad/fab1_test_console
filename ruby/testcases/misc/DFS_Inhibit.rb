=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: sfrieske, 20120-02-17

Note:
=end

# require 'ammo'
require 'sil/siltest'
require 'SiViewTestCase'


# Create SiView lot and SIL process DCR to test MIC eCAP
class DFS_Inhibit < SiViewTestCase
  @@sv_defaults = {route: 'C02-1522.P001', product: 'SHELBY21AJ.QA01', carriers: '%', carrier_category: 'FEOL'}
  # test11
  # @@op11 = 'M1-MSKAR010TI33Y1D-28BK.01'   # reticle SHELBY30M1AYLA, carrier_category 'BEOL'
  # @@ptool11 = 'ALC1362'  # 'ALC1360'
  @@op11 = 'DPN-GOXPRECLN-8752.01'
  @@ptool11 = 'SNK1311'
  # # test12
  # @@op12DI = 'CA-BARRDEPDI.01'
  # @@ptool12DI = 'DFS1614'
  # @@op12DR = 'CA-BARRDEPDR.01'
  # @@ptool12DR = 'SEM100'
  # # test13
  # @@op13pre = 'CA-BARRDEPDI.01'
  # @@op13 = 'CA-BARRDEPF2DR.01'
  # @@ptool13 = 'SEM100'

  @@waferselections = []
  @@testlots = []


  def self.after_all_passed
    @@sv.eqp_cleanup(@@siltest.ptool)
    # @@waferselections.each {|args| @@ammo.remove_wafer_selection(*args)}
    $log.warn "\n\ntestlot: #{@@testlots}"
  end

  def self.manual_cleanup
    @@testlots.each {|lot| @@sv.delete_lot_family(lot)}
  end


  def test00_setup
    $setup_ok = false
    #
    # assert @@ammo = AMMO::Client.new($env)  # to prevent skips by the SiView PRE1 script
    # assert @@ammo.echo, 'no AMMO connection'
    assert @@siltest = SIL::Test.new($env, sv: @@sv)
    #
    $setup_ok = true
  end

  def test11_ptool
    assert lot = @@svtest.new_lot(nwafers: 10), 'error creating test lot'
    @@testlots << lot
    op, ptool = @@op11, @@ptool11
    assert_equal 0, @@sv.eqp_cleanup(ptool), "error cleaning up #{ptool}"

    udata = @@sv.user_data_pd(op)
    assert process_type = udata['ProcessTypeWfrSelection'], 'missing PD UDATA ProcessTypeWfrSelection'
    assert process_layer = udata['ProcessLayer'], 'missing PD UDATA ProcessLayer'
    wafers = @@sv.lot_info(lot, wafers: true).wafers.collect {|w| w.wafer}
    @@waferselections << [wafers, process_type, process_layer]
    # assert @@ammo.set_wafer_selection_bymap([wafers.first], process_type, process_layer, selectflag: 1)
    # sleep 120
    assert_equal 0, @@sv.lot_opelocate(lot, nil, op: op), 'error locating lot'
    li = @@sv.lot_info(lot, wafers: true)
    assert wn = @@sv.what_next(ptool).find {|e| e.lot == lot}, 'lot is no candidate for processing'
    assert cj = @@sv.claim_process_lot(lot, eqp: ptool), 'error processing lot'
    assert chs = @@siltest.setup_channels(cas: ['EquipmentInhibit', 'AutoEquipmentInhibit', 'ReleaseEquipmentInhibit'])
    # readings = Hash[li.wafers.collect {|w| [w.wafer, 45]}]
    readings = Hash[li.wafers.collect {|w| [w.wafer, 145]}]  # OOC
    lparms = {
      ptool: ptool, cj: cj, lot: lot, ppd: li.op, opNo: li.opNo, opName: li.opName, route: li.route, 
      product: li.product, productgroup: li.productgroup, technology: li.technology,
      lrecipe: wn.lrecipe, mrecipe: wn.mrecipe, readings: readings, export: "log/sil/#{lot}.xml"
    }
    chambers = @@sv.eqp_info(ptool).chambers
    lparms[:chambers] = [chambers.first.chamber] unless chambers.empty?
    assert @@siltest.submit_process_dcr(lparms), 'error submitting process DCR'
  end

end
