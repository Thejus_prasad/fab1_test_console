=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: sfrieske, 2019-06-21

Version: 1.0.0

History:

Notes:
  TCs: https://docs.google.com/spreadsheets/d/18YOEwbFoMCUO2G7hjwRdes3bALIz8M13qz947X-L38Q/edit?ts=5e2ac131#gid=623037288
  WebMRB: http://sdrsqrad01.gfoundries.com/WMRB/Fabgui.aspx
  ICase:  http://172.20.5.110/F1ICASE/task.aspx
=end

require 'misc/fabguimsg'
require 'SiViewTestCase'


class ICASE1 < SiViewTestCase
  @@sv_defaults = {route: 'UTRT-TKBTF.01', product: 'UT-PRODUCT-ICASE.ATV', carriers: '%'}

  @@lots_webmrb = {
    # 'UR9028G.000'=>{'1M2020-00002'=>true},  # broken
    # 'UR9028G00.000'=>{'1M2020-00019'=>false},
    'UR9028K.000'=>{'1M2020-00005'=>false},
    'UR9028L.000'=>{'1S2020-00002'=>false},
    'UR9028N.000'=>{'1M2020-00009'=>false},
    'UR90293.000'=>{'1M2020-00011'=>true},
    'UR90294.000'=>{'1M2020-00011'=>false},
    'UR90295.000'=>{'1S2020-00004'=>false},
    'UR90296.000'=>{'1M2020-00012'=>false},
    'UXYV05133.000'=>{'1S2020-00008'=>false},
    'UXYV05138.000'=>{'1M2020-00019'=>true},
    'UXYV05139.000'=>{'1M2020-00019'=>false},
    'UXYV05140.000'=>{'1M2020-00016'=>false, '1M2020-00025'=>false},
    'UXYV05141.000'=>{'1M2020-00016'=>true},
    'UXYV05146.000'=>{'1M2020-00034'=>true, '1M2020-00035'=>false},
    'UXYV23034.000'=>{'1M2020-00067'=>true, '1M2020-00068'=>false},
    'UXYV23036.000'=>{'1M2020-00069'=>false},
    'UXYV23036.001'=>{'1M2020-00069'=>false},
  }

  # may have ICASE MRB
  @@lots_nowebmrb = [
    'UR9028G00.000',
  ]

  @@lots_icase = {
    'UR90285.000'=>{'1M19-000151'=>false},
    'UR90286.000'=>{'1M20-000006'=>false},
    'UR90287.000'=>{'1M19-000152'=>false},
    'UR90288.000'=>{'1M19-000153'=>true},
    'UR90289.000'=>{'1M19-000153'=>true, '1SCR19-000111'=>false},
    'UR9028A.000'=>{'1M19-000154'=>true, '1M19-000155'=>false},
    'UXYV23033.000'=>{'1M20-000280'=>true, '1M20-000281'=>true},
    'UXYV05144.001'=>{'1M20-000112'=>true, '1SCR20-000175'=>false}, # known issue, wrong false for closed MRB 112
    # 'UR90290.000'=>{'1M19-000162'=>false, '1M19-000163'=>true, '1M20-000051'=>true}, lot spoiled
    'UR9028E.000'=>{'1M19-000159'=>true},     # Q release
    'UR9028F.000'=>{'1M19-000159'=>false},
    'UXYV23035.000'=>{'1M20-000282'=>true, '1M20-000283'=>false},   # Q release, 2 MRBs
    'UR9028P.000'=>{'1M20-000284'=>false},
    'UR9028R.000'=>{'1SCR19-000112'=>false},
    'UR9028S.000'=>{'1SCR19-000113'=>false},
    'UR9028U.000'=>{'1SCR19-000115'=>false},
    'UR9028V.000'=>{'1SCR19-000115'=>true},
    'UR9028X.000'=>{'1SCR19-000117'=>false},
    'UR9028Y.001'=>{'1SCR19-000124'=>false},
    # 'UR9028Z.000'=>{''=>false},   # failed, not shippable (TC 44)
    'UR90291.000'=>{'1M20-000004'=>true, '1SCR19-000012'=>false},
    'UR90292.000'=>{'1SCR19-000123'=>false},
    # 'UR90297.001'=>{''=>false},   # failed, child not shippable (TC 54)
    # 'UR90299.001'=>{''=>false},   # failed, child not shippable (TC 55)
    'UXYV05129.000'=>{'1M20-000011'=>true},  # add WebMRB -> false
    'UXYV05130.000'=>{'1SCR20-000021'=>false},  # add WebMRB -> true
    'UXYV05131.000'=>{'1M20-000012'=>true},  # add WebMRB -> false
    'UXYV05132.000'=>{'1SCR20-000023'=>false},
    'UXYV05134.000'=>{'1M20-000013'=>true, '1SCR20-000025'=>false},
    'UXYV05135.000'=>{'1M20-000014'=>false},
    'UXYV05136.000'=>{'1M20-000014'=>false},
    'UXYV05137.000'=>{'1M20-000015'=>true},
    'UXYV05142.001'=>{'1SCR20-000167'=>false},
    'UXYV05147.000'=>{'1SCR20-000241'=>false},
  }
  xxx = {
    'UR90299.000'=>{''=>false},
    'UXYV05124.000'=>{''=>false},
    'UXYV05125.000'=>{''=>false},
    'UXYV05126.000'=>{''=>false},
    'UXYV05127.000'=>{''=>false},
    'UXYV05128.000'=>{''=>false},

  }
  # # intermediate: wrong shippability 'false'
  # True_wrong = false  # must be true

  # may have WebMRB
  @@lots_noicase = [
    'UR90284.000',    # incident ICASE-IN-F1-19-000192 not shown
    'UR9028B.000',    # MRB voided
    'UR9028Q.000',    # removed from MRB
    'UR9028W.000',    # voided scrap case
    'UR9028Y.001',    # scrapped wafer split off
    'UXYV05144.000',  # scrapped wafer split off
  ]

  @@testlots = []


  def self.after_all_passed
    @@testlots.each {|lot| @@sv.delete_lot_family(lot)}
  end

  def self.new_lots(n=1, params={})
    return @@svtest.new_lots(n, params)
  end


  def test00_setup
    $setup_ok = false
    #
    assert @@ic = FabGUI::ICase.new($env)
    #
    $setup_ok = true
  end


  def test01_smoke
    lot = 'UR9028K.000'
    assert ref = @@lots_webmrb[lot], "no WebMRB reference data for lot #{lot.inspect}"
    assert verify_lot_webmrb(lot, ref), 'wrong reference data?'
  end

  def testdev02
    lot = 'UXYV23034.000'
    assert ref = @@lots_webmrb[lot], "no WebMRB reference data for lot #{lot.inspect}"
    assert verify_lot_webmrb(lot, ref), 'wrong ICase report'
  end

  def testdev03
    lot = 'UXYV05133.000'
    assert ref = @@lots_icase[lot], "no ICase reference data for lot #{lot.inspect}"
    assert verify_lot_icase(lot, ref), 'wrong ICase report'
  end


  def test11_notex
    assert verify_lot_nomrb('NOTEX.00'), 'test failed for lot NOTEX.00'
  end

  def test12_fresh
    assert lot = @@svtest.new_lot, 'error creating test lot'
    @@testlots << lot
    assert verify_lot_nomrb(lot), "test failed for lot #{lot.inspect}"
  end

  def test13_nowebmrb
    @@lots_nowebmrb.each {|lot|
      res = @@ic.shipping_availability(lot) || ($log.warn 'wrong response from ICase'; return)
      assert_empty res.select {|e| e[:ContainingURL] == false}, 'unexpected MRB'
    }
  end

  def test14_noicase
    @@lots_noicase.each {|lot|
      res = @@ic.shipping_availability(lot) || ($log.warn 'wrong response from ICase'; return)
      assert_empty res.select {|e| e[:ContainingURL] == true}, 'unexpected MRB'
    }
  end

  def test21_webmrb
    failed = []
    @@lots_webmrb.each_pair {|lot, ref|
      failed << lot unless verify_lot_webmrb(lot, ref)
    }
    assert_empty failed, "failed lots: #{failed}"
  end

  def test31_icase
    failed = []
    @@lots_icase.each_pair {|lot, ref|
      failed << lot unless verify_lot_icase(lot, ref)
    }
    assert_empty failed, "failed lots: #{failed}"
  end

  def test41_mrb_split # works
    lot = 'UXYV23020.000'
  end

  def test42_scrap_split_merge
    lot = 'UXYV23021.000'
    # ICase with wafers 01..04 scrapped, repeatable!
  end

  def test43_merge_webmrb_icase  # fails
    lot = 'UXYV23022.000'
    lot = 'UXYV23023.000'
  end

  def test43_merge_icase_icase  # fails
    lot = 'UXYV23024.000'
  end


  def testman95_branch
    rte_branch = 'e0B493-BRANCH-A0.01'
    opNo_branch = '4600.1300'
    # opNo_return = '4600.1400'
    assert lot = @@svtest.new_lot, 'error creating test lot'
    assert_equal 0, @@sv.lot_gatepass(lot)
    3.times {
      assert @@sv.claim_process_lot(lot)
    }
    assert ecmol = @@svtest.get_empty_carrier(carrier_category: 'MOL')
    assert_equal 0, @@sv.wafer_sort(lot, ecmol)
    assert_equal 0, @@sv.lot_opelocate(lot, opNo_branch), 'error locating lot'
    assert_equal 0, @@sv.lot_branch(lot, rte_branch), 'error branching lot'
    while @@sv.lot_info(lot).route == rte_branch
      assert @@sv.claim_process_lot(lot)
    end
    assert @@sv.claim_process_lot(lot)
    $log.warn "\n\ttest lot: #{lot}"
  end


  # aux methods

  def verify_lot_nomrb(lot)
    $log.info "verify_lot_nomrb #{lot.inspect}"
    res = @@ic.shipping_availability(lot) || ($log.warn 'wrong response from ICase'; return)
    ($log.warn "ambigous or empty response"; return) if res.size != 1
    res = res.first
    ($log.warn "wrong LotID: #{res[:LotID]}";return) if res[:LotID] != lot
    ($log.warn "wrong IsShippable: #{res[:IsShippable]}";return) if res[:IsShippable] != 'true'
    ($log.warn "wrong MRBNo: #{res[:MRBNo]}";return) if res[:MRBNo] != 'No MRB Found'
    return true
  end

  def verify_lot_webmrb(lot, ref)
    $log.info "verify_lot_webmrb #{lot.inspect}"
    res = @@ic.shipping_availability(lot) || ($log.warn 'wrong response from ICase'; return)
    ok = true
    ref.each_pair {|mrbno, shippable|
      $log.info "  checking MRBNo #{mrbno} (#{shippable})"
      data = res.find {|e| e[:MRBNo] == mrbno}
      if data
        if data[:LotID] != lot
          $log.warn "  wrong LotID: #{data[:LotID]}"
          return
        elsif data[:IsShippable] != shippable.to_s
          $log.warn "    wrong IsShippable: #{data[:IsShippable]} insted of #{shippable}"
          ok = false
        end
        ($log.warn "  wrong ContainingURL: #{data[:ContainingURL]}"; ok=false) if data[:ContainingURL] != 'false'
        ($log.warn "  wrong URL: #{data[:URL]}"; ok=false) if data[:URL] != nil
      else
        $log.warn "    no entry for MRBNo #{mrbno}"
        ok = false
      end
    }
    # mrbs_ref = ref.keys.sort
    # mrbs_res = res.collect {|e| e[:MRBNo]}.sort
    # ($log.warn "  wrong MRBs reported: #{mrbs_res}, expected: #{mrbs_ref}"; ok=false) if mrbs_ref != mrbs_res
    return ok
  end

  def verify_lot_icase(lot, ref)
    $log.info "verify_lot_icase #{lot.inspect}"
    res = @@ic.shipping_availability(lot) || ($log.warn 'wrong response from ICase'; return)
    ok = true
    ref.each_pair {|mrbno, shippable|
      $log.info "  checking MRBNo #{mrbno} (#{shippable})"
      data = res.find {|e| e[:MRBNo] == mrbno}
      if data
        if data[:LotID] != lot
          $log.warn "  wrong LotID: #{data[:LotID]}"
          return
        elsif data[:IsShippable] != shippable.to_s
          $log.warn "    wrong IsShippable: #{data[:IsShippable]} insted of #{shippable}"
          ok = false
        end
        ($log.warn "  wrong ContainingURL: #{data[:ContainingURL]}"; ok=false) if data[:ContainingURL] != 'true'
        ($log.warn "  wrong URL: #{data[:URL]}"; ok=false) if data[:URL].nil?
      else
        $log.warn "    no entry for MRBNo #{mrbno}"
        ok = false
      end
    }
    # mrbs_ref = ref.keys.sort
    # mrbs_res = res.collect {|e| e[:MRBNo]}.sort
    # ($log.warn "  wrong MRBs reported: #{mrbs_res}, expected: #{mrbs_ref}"; ok=false) if mrbs_ref != mrbs_res
    return ok
  end

end
