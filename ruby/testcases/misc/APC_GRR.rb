=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: sfrieske, 2020-12-07

Version: 21.04

History:

Notes:
  https://drive.google.com/file/d/1M0qg-y6i8wvTzCvYqYig6OiVBbD5IvhU/view?usp=sharing
  PCP spec http://vf1sfcd01:10080/setupfc-rest/v1/specs/C05-00001945/content?format=html
  CATADMIN AH_GRR_STUDY
  CATADMIN AH_GRR_STUDY_VALUES
  Catalyst logs ITDC: http://drsapc/apcsetup/cajallog/log_fab1_dev.html
=end

require 'SiViewTestCase'
require 'catalyst/grrstudy'


class APC_GRR < SiViewTestCase
  @@sv_defaults = {product: 'UT-PRODUCT-GRR-PFL.01', route: 'UTRT-GRR-ORIG.01', nwafers: 3, carriers: '%'}
  @@original_pds = %w(UTPDM-GRR-ORIG-1T.01 UTPDM-GRR-ORIG-2T.01 UTPDM-GRR-ORIG-3T.01 UTPDM-GRR-ORIG-NT.01)

  @@testlots = []

  
  def self.startup
    super
    @@wafersel = Catalyst::WaferSelection.new($env)
    @@apc = APC::Client.new($env)
    @@cat2 = APC::DBCat2.new($env)
    @@grr_studies = []
    @@original_pds.each {|pd|
      @@grr_studies << APC::GRRStudy.new($env, @@svtest.route, pd, sv: @@sv, apc: @@apc, cat2: @@cat2, wafersel: @@wafersel)
    }
  end

  def self.after_all_passed
    @@testlots.each {|lot| @@sv.delete_lot_family(lot)}
  end


  def testdev01_fpc
    assert lot = @@svtest.new_lot, 'error creating test lot'
    @@testlots << lot
    assert @@grr_studies[1].apc_prepare_lot(lot)
    assert fpcids = @@sv.lot_fpc_create(lot)
    assert @@mrecipe = @@sv.fpc_mrecipes(pd: @@original_pds[1], lot: lot).first.machineRecipeID
    eqp = 'THK1514'
    assert @@sv.lot_fpc_update(fpcids: fpcids, mrecipe: @@mrecipe, eqp: eqp, hold: false)
#    assert_equal 0, @@sv.lot_hold_release(lot, nil)
#    assert cj = @@sv.claim_process_lot(lot, eqp: eqp, noopecomp: true, nounload: true)
    # assert res = @@sv.claim_process_prepare(lot, eqp: eqp)
    # cj, cjinfo, eqpiraw, pstatus, mode, lisraw = res
    # carriers = lisraw.strLotInfo.collect {|e| e.strLotLocationInfo.cassetteID.identifier}.uniq
    # assert @@sv.eqp_opestart(eqp, carriers)
  end

  # not yet implemented
  def testman11_1T
    assert lot = @@svtest.new_lot, 'error creating test lot'
    @@testlots << lot
    assert @@grr_studies[0].apc_runtest(lot, true, parameters: ['MyParameterA'])
  end

  def test12_1T_0
    assert lot = @@svtest.new_lot, 'error creating test lot'
    @@testlots << lot
    assert @@grr_studies[0].apc_runtest(lot, false, npds: 0, parameters: ['MyParameterA'], warning: 'No_stored_data_found')
  end

  def test21_2T
    assert lot = @@svtest.new_lot, 'error creating test lot'
    @@testlots << lot
    assert @@grr_studies[1].apc_runtest(lot, true)
  end

  def test22_2T_1
    assert lot = @@svtest.new_lot, 'error creating test lot'
    @@testlots << lot
    assert @@grr_studies[1].apc_runtest(lot, false, npds: 1)
  end

  def test31_3T
    assert lot = @@svtest.new_lot, 'error creating test lot'
    @@testlots << lot
    assert @@grr_studies[2].apc_runtest(lot, true)
  end

  def test32_3T_2
    assert lot = @@svtest.new_lot, 'error creating test lot'
    @@testlots << lot
    assert @@grr_studies[2].apc_runtest(lot, true, npds: 2)
  end

  def test33_3T_1
    assert lot = @@svtest.new_lot, 'error creating test lot'
    @@testlots << lot
    assert @@grr_studies[2].apc_runtest(lot, false, npds: 1)
  end

  def test34_NT_3
    assert lot = @@svtest.new_lot, 'error creating test lot'
    @@testlots << lot
    assert @@grr_studies[3].apc_runtest(lot, true, npds: 3)
  end

  def test35_NT_2
    assert lot = @@svtest.new_lot, 'error creating test lot'
    @@testlots << lot
    assert @@grr_studies[3].apc_runtest(lot, false, npds: 2)
  end

  def test41_2T_2meas
    assert lot = @@svtest.new_lot, 'error creating test lot'
    @@testlots << lot
    study = @@grr_studies[1]
    assert study.apc_prepare_lot(lot)
    assert study.apc_measroute(lot, repeats: [3, 2])
    assert study.apc_verify_study(lot, true)
  end

  def test42_2T_1meas
    assert lot = @@svtest.new_lot, 'error creating test lot'
    @@testlots << lot
    assert @@grr_studies[1].apc_runtest(lot, false, repeats: [3, 1], warning: 'meas trials')
  end


  def test51_2T_2params
    assert lot = @@svtest.new_lot, 'error creating test lot'
    @@testlots << lot
    parameters = ['MyParameterA', 'MyParameterB']
    assert @@grr_studies[1].apc_runtest(lot, true, parameters: parameters)
    parameters.each {|p|
      assert @@grr_studies[1].apc_verify_study(lot, true, parameter: p), "wrong study for parameter #{p}"
    }
  end

end
