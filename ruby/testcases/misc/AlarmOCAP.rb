=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: sfrieske, 2019-01-17

History:
  2021-08-24 sfrieske, extended tests, including Derdack

Notes:
  Eqp must exist in SiView and its reply queue (CEI.<EQP>_REPLY01) must exist
=end

require 'derdack'
require 'misc/ocap'
require 'util/uniquestring'
require 'util/waitfor'
require 'SiViewTestCase'


class AlarmOCAP < SiViewTestCase
  @@eqp = 'ETX101'  # 'HYB171'
  @@eqp2 = 'UTF001'
  @@opNo2 = '1000.600'

  @@testlots = []


  def self.after_all_passed
    @@sv.eqp_cleanup(@@eqp)
    @@sv.eqp_cleanup(@@eqp2)
    @@testlots.each {|lot| @@sv.delete_lot_family(lot)}
  end


  def test00
    @@client = OCAP::Alarm.new($env, @@eqp)
    @@client2 = OCAP::Alarm.new($env, @@eqp2)
    @@notifications = DerdackEvents.new($env)
    assert_equal 0, @@sv.inhibit_cancel(:eqp, @@eqp)
  end

  def test11_unconfigured
    tstart = Time.now - 10  # allow some time differences between systems
    alid = 9  # not configured, default OCAP settings
    altext = unique_string
    assert @@client.submit(alid, text: altext)
    $log.info 'waiting for the alarm to show up in SiView'
    assert wait_for(timeout: 90) {
      @@sv.eqp_alarm_history(@@eqp, tstart: tstart).find {|e| e.alarmText == altext}
    }, 'alarm not found in SiView'
    # no Derdack notification, TODO: verify?
  end

  def test12_OCAP_SIVIEW  # same behaviour as unconfigured
    tstart = Time.now - 10  # allow some time differences between systems
    alid = 57  # OCAP_DEFAULT (send to SiView)
    altext = unique_string
    assert @@client.submit(alid, text: altext)
    $log.info 'waiting for the alarm to show up in SiView'
    assert wait_for(timeout: 90) {
      @@sv.eqp_alarm_history(@@eqp, tstart: tstart).find {|e| e.alarmText == altext}
    }, 'alarm not found in SiView'
    # no Derdack notification, TODO: verify?
  end

  def test13_OCAP_NOTIFY
    tstart = Time.now - 10  # allow some time differences between systems
    alid = 58  # OCAP_NOTIFY
    altext = unique_string
    assert @@client.submit(alid, text: altext)
    $log.info 'waiting for the alarm to show up in SiView'
    assert wait_for(timeout: 90) {
      @@sv.eqp_alarm_history(@@eqp, tstart: tstart).find {|e| e.alarmText == altext}
    }, 'alarm not found in SiView'
    # verify Derdack notification
    assert @@notifications.wait_for(tstart: tstart, timeout: 120,
      service: '%GFSoapTemplate%', filter: {'Application'=>'AlarmOCAP'}) {|msgs|
        msgs.find {|m| m['Message'].include?(altext)}
    }, 'missing or wrong notification'
  end

  def test21_OCAP_INH
    assert_equal 0, @@sv.inhibit_cancel(:eqp, @@eqp)
    tstart = Time.now - 10  # allow some time differences between systems
    alid = 55  # OCAP_DEFAULT, OCAP_INH, OCAP_NOTIFY
    altext = unique_string
    assert @@client.submit(alid, text: altext)
    $log.info 'waiting for the alarm to show up in SiView'
    assert wait_for(timeout: 90) {
      @@sv.eqp_alarm_history(@@eqp, tstart: tstart).find {|e| e.alarmText == altext}
    }, 'alarm not found in SiView'
    # verify Derdack notification
    assert @@notifications.wait_for(tstart: tstart, timeout: 120,
      service: '%GFSoapTemplate%', filter: {'Application'=>'AlarmOCAP'}) {|msgs|
        msgs.find {|m| m['Message'].include?(altext)}
    }, 'missing or wrong notification'
    # verify inhibit
    assert inh = @@sv.inhibit_list(:eqp, @@eqp, owner: 'X-OCAP', reason: 'X-EA').first, 'missing inhibit'
  end

  def test31_OCAP_RHL
    assert lot = @@svtest.new_lot, 'error creating lot'
    @@testlots << lot
    assert_equal 0, @@sv.lot_opelocate(lot, @@opNo2), 'error locating lot'
    assert_equal 0, @@sv.eqp_cleanup(@@eqp2)
    assert @@sv.claim_process_prepare(lot, eqp: @@eqp2), 'error preparing lot'
    assert @@sv.eqp_opestart(@@eqp2, [@@sv.lot_info(lot).carrier]), 'error starting lot processing'
    tstart = Time.now - 10  # allow some time differences between systems
    alid = 56  # OCAP_DEFAULT, OCAP_RHL (includes notify)
    altext = unique_string
    assert @@client2.submit(alid, text: altext)   # ? e10: 'PRD', e10sub: '1PRD'
    $log.info 'waiting for the alarm to show up in SiView'
    assert wait_for(timeout: 90) {
      @@sv.eqp_alarm_history(@@eqp2, tstart: tstart).find {|e| e.alarmText == altext}
    }, 'alarm not found in SiView'
    # verify Derdack notification
    assert @@notifications.wait_for(tstart: tstart, timeout: 120,
      service: '%GFSoapTemplate%', filter: {'Application'=>'AlarmOCAP'}) {|msgs|
        msgs.find {|m| m['Message'].include?(altext)}
    }, 'missing or wrong notification'
    # verify RNHL
    refute_empty @@sv.lot_hold_list(lot, reason: 'RNHL'), 'no RNHL hold'
  end

end
