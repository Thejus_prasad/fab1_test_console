=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.


Author: Steffen Frieske, 2018-01-08

Version: 1.0.2

History
  2018-05-08 sfrieske, use copy_object with ondemand: true
  2018-10-18 sfrieske, improved error messages on missing UDATA and reticleset
  2021-04-06 sfrieske, changed oid([]) to oids([])
=end

require 'siview'
require 'RubyTestCase'


# Copy a product from Prod into ITDC, creating data for MRA department's tests
# For final object creation w/o QAMRA1 prefix call
#   runtest 'MRA_ProductImport', 'itdc', parameters: {tgtprefix: '', product: 'any product in Prod'}
class MRA_ProductImport < RubyTestCase
  @@product = nil   # see above, 'ATLANTB1AA.B1'
  @@srcenv = 'f1prodqaview'
  @@tgtprefix = 'QAMRA1-'  # for new target objects like product, reticle, etc.
  @@templates = {
    technology: 'QAMRA-TECH0',
    productgroup: 'QAMRA-PRODGRP0',
    product: 'QAMRA-PRODUCT0.00',
    photolayer: 'QAMRA0',
    reticleset: 'QAMRARS0',
    rg: 'QAMRARG0',
    reticle: 'QAMRARETICLE0',
    userdefinedcode: 'ProgramID.generic'
  }
  # must set PG (UDATA DeviceID, WaferLayout), Tech, Reticle Set: only RGs

  def self.startup
    @@sm = SiView::SM.new($env)
    @@sm_src = SiView::SM.new(@@srcenv)
  end

  # remove all test objects matching the object prefix except the templates; return true on success
  def self.delete_all_testobjects
    $log.info "delete_all_testobjects"
    ($log.warn "object prefix #{@@tgtprefix.inspect} is not specific enough"; return) unless @@tgtprefix.start_with?('QAMRA')
    ret = true
    @@templates.each_pair {|oclass, otemplate|
      $log.info "-- #{oclass}"
      @@sm.object_ids(oclass, @@tgtprefix + '*').each {|o| 
        ret &= @@sm.delete_objects(oclass, o) if o.identifier != otemplate
      }
    }
    return ret
  end


  def test00_setup
    $setup_ok = false
    #
    if @@tgtprefix.start_with?('QAMRA')  # protection from empty tgtprefix (which would be normal after tc dev!)
      assert self.class.delete_all_testobjects
    end
    assert @@product, 'no product passed at the command line'
    refute_empty @@product, 'empty product name'
    assert @@prodinfo_src = @@sm_src.object_info(:product, @@product).first, "error getting product info for #{@@product.inspect}"
    #
    $setup_ok = true
  end

  def test11_tech_pg
    # get src env productgroup and technology data
    assert pginfo_src = @@sm_src.object_info(:productgroup, @@prodinfo_src.specific[:productgroup]).first
    # create tech on demand
    techname = @@tgtprefix + pginfo_src.specific[:technology]
    assert @@sm.copy_object(:technology, @@templates[:technology], techname, ondemand: true)
    # create pg on demand
    pgname = @@tgtprefix + @@prodinfo_src.specific[:productgroup]
    assert @@sm.copy_object(:productgroup, @@templates[:productgroup], pgname, ondemand: true) {|oinfos|
      assert ci = @@sm.extract_si(oinfos.first.classInfo)
      ci.technology = @@sm.object_ids(:technology, techname).first
      oinfos.first.classInfo = @@sm.insert_si(ci)
    }
    # optionally create UDATA value definitions
    prgid = pginfo_src.udata['ProductGroupProgramID']
    assert @@sm.copy_object(:userdefinedcode, @@templates[:userdefinedcode], "ProgramID.#{prgid}", ondemand: true) {|oinfos|
      assert ci = @@sm.extract_si(oinfos.first.classInfo)
      ci.description = @@sm_src.object_info(:userdefinedcode, "ProgramID.#{prgid}").first.description
      oinfos.first.classInfo = @@sm.insert_si(ci)
    }
    # copy pg UDATA
    assert @@sm.update_udata(:productgroup, pgname, pginfo_src.udata), "error copying UDATA for PG #{pgname}"
  end

  def test13_reticleset
    # get src env product and reticleset data
    refute_empty @@prodinfo_src.specific[:reticleset], 'reticleset not defined'
    assert rsinfo_src = @@sm_src.object_info(:reticleset, @@prodinfo_src.specific[:reticleset]).first
    # create photolayers and reticlegroups on demand
    rsinfo_src.specific[:rgs].each {|e|
      # photolayer
      oname = @@tgtprefix + e[:photolayer]
      assert @@sm.copy_object(:photolayer, @@templates[:photolayer], oname, ondemand: true)
      # rg, assuming only 1 rg per photolayer
      oname = @@tgtprefix + e[:rgs].first
      assert @@sm.copy_object(:rg, @@templates[:rg], oname, ondemand: true)
    }
    # create reticleset on demand
    oname = @@tgtprefix + @@prodinfo_src.specific[:reticleset]
    assert @@sm.copy_object(:reticleset, @@templates[:reticleset], oname, ondemand: true) {|oinfos|
      assert oinfo_ci = @@sm.extract_si(oinfos.first.classInfo)
      oinfo_ci.defaultReticleGroups = rsinfo_src.specific[:rgs].collect {|e|
        assert plid = @@sm.object_ids(:photolayer, @@tgtprefix + e[:photolayer]).first
        assert rgids = @@sm.object_ids(:rg, @@tgtprefix + e[:rgs].first)
        @@sm.jcscode.brDefaultReticleGroupData_struct.new(plid, rgids, false)
      }.to_java(@@sm.jcscode.brDefaultReticleGroupData_struct)
      oinfos.first.classInfo = @@sm.insert_si(oinfo_ci)
    }
  end

  def test14_reticles
    # get src env product and reticleset data
    refute_empty @@prodinfo_src.specific[:reticleset], 'reticleset not defined'
    assert rsinfo_src = @@sm_src.object_info(:reticleset, @@prodinfo_src.specific[:reticleset]).first
    # create reticles on demand
    rsinfo_src.specific[:rgs].each {|e|
      # src reticle by rg
      rnames = @@sm_src.object_ids(:reticle, :all, search: {rg: e[:rgs].first}).collect {|e| e.identifier}
      rnames.each {|rname|
        oname = @@tgtprefix + rname
        ## assert rgid = @@sm.object_ids(:rg, @@tgtprefix + e[:rgs].first).first
        assert @@sm.copy_object(:reticle, @@templates[:reticle], oname, ondemand: true) {|oinfos|
          assert oinfo_ci = @@sm.extract_si(oinfos.first.classInfo)
          oinfo_ci.reticleGroup = @@sm.object_ids(:rg, @@tgtprefix + e[:rgs].first).first
          oinfos.first.classInfo = @@sm.insert_si(oinfo_ci)
        }
      }
    }
  end

  def test19_product
    # create product on demand
    oname = @@tgtprefix + @@product
    assert @@sm.copy_object(:product, @@templates[:product], oname, ondemand: true) {|oinfos|
      oinfo_ci = @@sm.extract_si(oinfos.first.classInfo)
      oinfo_ci.description = "Production copy of #{@@product} for MRA tests, do not touch!"
      oinfo_ci.mainProcessDefinition = @@sm.oid('')
      oinfo_ci.mainProcessDefinitionId = ''
      oinfo_ci.mainProcessDefinitionVersion = ''
      oinfo_ci.mainProcessDefinitions = [].to_java(@@sm.jcscode.brMainProcessDefinitionData_struct)
      oinfo_ci.sourceProducts = @@sm.oids([])
      oinfo_ci.state = 'Draft'  # allows to check in w/o route
      oinfo_ci.productGroup = @@sm.object_ids(:productgroup, @@tgtprefix + @@prodinfo_src.specific[:productgroup]).first
      oinfo_ci.reticleSet = @@sm.object_ids(:reticleset, @@tgtprefix + @@prodinfo_src.specific[:reticleset]).first
      oinfos.first.classInfo = @@sm.insert_si(oinfo_ci)
    }
  end

end
