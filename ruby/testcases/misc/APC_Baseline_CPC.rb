=begin
(c) Copyright 2016 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.


Author: Steffen Frieske, 2018-06-04

Version: 1.0.2

History:
  2018-10-16 sfrieske, added testcases for spec rev 21
Notes:
  see http://vf1sfcd01:10080/setupfc-rest/v1/specs/C05-00001809/content?format=html
=end

require 'SiViewTestCase'
require 'catalyst/apcbaseline'


# Test APC Baseline Configurable Parameter Calculation
class APC_Baseline_CPC < SiViewTestCase
  @@eqp = 'BLITM001'  # must be configured in the APC test adapter
  @@slots1 = [1, 3]
  @@slots2 = [6, 8]
  @@readings = {
    'INP-P1'   =>{@@slots1=>[1, -2]}, 
    'INP-P2-S1'=>{@@slots1=>[[1, 2, -1, -4], [0, 1, -2, -3]]},          # simple site level parameter
    'INP-P3-S1'=>{@@slots1=>[[0, 0, 0, 0], [1, 0, 0, 0]]},              # simple site level parameter
    'INP-P4-S1'=>{@@slots1=>[[1, 1, 1, 0], [1, 1, 1, 1]]},              # simple site level parameter
    'INP-P5-INV-DIEXY-S1'=>{@@slots1=>[[1, 2, 3, 4], [1, 2, 3, 4]],
      dieX: [[1.1, 2.2, 3.3, 4], [1, 2, 3, 4]],
      dieY: [[1, 2, 3, 4], [1, 2, 3, 4]]
    },
    'INP-P5-VALID-DIEXY-S1'=>{@@slots1=>[[1, 2, 3, 4], [1, 2, 3, 4]],
      dieX: [[1, 2, 3, 4], [1, 2, 3, 4]],
      dieY: [[1, 2, 3, 4], [1, 2, 3, 4]]
    },
    'INP-P6-S1'=>{@@slots2=>[[11, 12, -11, -14], [10, 11, -12, -13]]},  # like INP-P4-S1 with different wafers
  }
  @@resref = {
    # 5.2.1. Simple Operators and Expressions
    'TEST-CONST-42'         =>{var: 'APC-DC-FLOAT', level: :lot, value: 42},
    'TEST-WAFER-RATIO'      =>{var: 'APC-DC-FLOAT', level: :lot, value: 50.0},
    'TEST-ADD'              =>{var: 'APC-DC-FLOAT', level: :lot, value: 42},
    'TEST-SUB'              =>{var: 'APC-DC-FLOAT', level: :lot, value: 42},
    'TEST-MULT'             =>{var: 'APC-DC-FLOAT', level: :lot, value: 42},
    'TEST-DIV'              =>{var: 'APC-DC-FLOAT', level: :lot, value: 42.0},                             # fixed value
    'TEST-ADD-P-CONST'      =>{var: 'APC-DC-FLOAT', level: :wafers, slots: @@slots1, value: [4, 1]},       # adds 3
    'TEST-SUB-P-CONST'      =>{var: 'APC-DC-FLOAT', level: :wafers, slots: @@slots1, value: [-2, -5]},     # subs 3
    'TEST-MUL-P-CONST'      =>{var: 'APC-DC-FLOAT', level: :wafers, slots: @@slots1, value: [3, -6]},      # * 3
    'TEST-DIV-P-CONST'      =>{var: 'APC-DC-FLOAT', level: :wafers, slots: @@slots1, value: [0.1, -0.2]},  # / 10
    'TEST-P-P'              =>{var: 'APC-DC-FLOAT', level: :wafers, slots: @@slots1, value: [1.0, -2.0]},  # sent values
    'TEST-CONST-L-1'        =>{var: 'APC-DC-FLOAT', level: :lot, value: 1},
    'TEST-CONST-L-10'       =>{var: 'APC-DC-FLOAT', level: :lot, value: 10},
    'TEST-CONST-L-100'      =>{var: 'APC-DC-FLOAT', level: :lot, value: 100},
    'TEST-CONST-L-1000'     =>{var: 'APC-DC-FLOAT', level: :lot, value: 1000},
    'TEST-COMPLEX-ADD-SUB'  =>{var: 'APC-DC-FLOAT', level: :lot, value: -909},    # L1-L10+L100-L1000
    'TEST-COMPLEX-ADD-SUB-1'=>{var: 'APC-DC-FLOAT', level: :lot, value: -909},    # (L1-L10)+(L100-L1000)
    # 5.2.2. General Functions
    'TEST-MATH-ABS-1'       =>{var: 'APC-DC-FLOAT', level: :wafers, slots: @@slots1, value: [1.0, 2.0]},
    # 5.2.3. Context access
    'TEST-CTX-LOT'          =>{var: 'APC-DC-STRING', level: :lot, value: :lot},
    # 5.2.4. Constants
    'TEST-CONST-PI'         =>{var: 'APC-DC-FLOAT', level: :lot, value: '3.141592653589793'},
    'TEST-CONST-E'          =>{var: 'APC-DC-FLOAT', level: :lot, value: '2.718281828459045'},
    # 5.2.5. Control Structures
    'TEST-IF-TRUE'          =>{var: 'APC-DC-STRING', level: :lot, value: 'PASS'},
    'TEST-IF-FALSE'         =>{var: 'APC-DC-STRING', level: :lot, value: 'PASS'},
    'TEST-OPT-ARGS'         =>{var: 'APC-DC-FLOAT', level: :lot, value: 141},
    'TEST-IF_EXISTS'        =>{var: 'APC-DC-FLOAT', level: :lot, value: 141},
    # 5.2.6. Statistical Functions
    'TEST-STATS-MEAN-LOT'   =>{var: 'APC-DC-FLOAT', level: :lot, value: -0.75},                         # avg over all 'INP-P2-S1'
    'TEST-STATS-STDDEV-LOT' =>{var: 'APC-DC-FLOAT', level: :lot, value: 2.1213203435596424},
    # 5.2.7. Aggregations
    'TEST-LOT-MEAN'         =>{var: 'APC-DC-FLOAT', level: :lot, value: -0.5},                          # avg over all 'INP-P1' 
    'TEST-COUNT-W'          =>{var: 'APC-DC-FLOAT', level: :wafers, slots: @@slots1, value: [4, 4]},    # number of INP-P2-S1 
    'TEST-COUNT-L'          =>{var: 'APC-DC-FLOAT', level: :lot, value: 8},                             # number of all INP-P2-S1 
    # 5.2.8. Coordinate Funtions
    'TEST-FILTER-BELOW-ZERO'=>{var: 'APC-DC-FLOAT', level: :sites, value: [-1, -4, -2, -3],
      "siteY"=>"-47577.299 -47577.299 -47577.299 -47577.299", 
      "siteX"=>"-125856.3521 -125856.3521 -125856.3521 -125856.3521", 
      "site"=>"3 4 3 4", 
    },
    'TEST-FILTER-ABOVE-ZERO'=>{var: 'APC-DC-FLOAT', level: :sites, value: [1, 2, 0, 1],
      "siteY"=>"78694.701 78694.701 78694.701 78694.701", 
      "siteX"=>"-75453.3521 -75453.3521 -75453.3521 -75453.3521", 
      "site"=>"1 2 1 2", 
    },
    'TEST-INV-DIEXY-VALUES' =>{var: 'APC-DC-STRING', level: :lot, value: 'PASS'},
    # 5.2.9. Site Level Selection Functions
    'TEST-SITE-SELECT'      =>{var: 'APC-DC-FLOAT', level: :wafers, slots: @@slots1, value: [2, 1]},
    # 5.2.10. Select and Filter
    'TEST-FILTER-WAFER'     =>{var: 'APC-DC-FLOAT', level: :sites, value: [1, 2, -1, -4],
      "siteY"=>"78694.701 78694.701 -47577.299 -47577.299",
      "siteX"=>"-75453.3521 -75453.3521 -125856.3521 -125856.3521",
      "site"=>"1 2 3 4"
    },
    'TEST-MERGE-Wafer'      =>{var: 'APC-DC-FLOAT', level: :sites, value: [1, 2, -1, -4, 0, 1, -2, -3, 11, 12, -11, -14, 10, 11, -12, -13],
      "siteY"=>"78694.701 78694.701 -47577.299 -47577.299 78694.701 78694.701 -47577.299 -47577.299 78694.701 78694.701 -47577.299 -47577.299 78694.701 78694.701 -47577.299 -47577.299",
      "siteX"=>"-75453.3521 -75453.3521 -125856.3521 -125856.3521 -75453.3521 -75453.3521 -125856.3521 -125856.3521 -75453.3521 -75453.3521 -125856.3521 -125856.3521 -75453.3521 -75453.3521 -125856.3521 -125856.3521",
      "site"=>"1 2 3 4 1 2 3 4 1 2 3 4 1 2 3 4"
    },
    # 5.2.11. Time and Duration
    'TEST-CURRENT-TIME'        =>{var: 'APC-DC-STRING', level: :lot, value: :tstart},
    'TEST-CURRENT-TIME_ISO8601'=>{var: 'APC-DC-STRING', level: :lot, value: :tstart},
    # # 5.2.12. RespPD 
    # TODO: no data
    # 'TEST-CONST-42-FROM-PREPD' =>{},
    # 'TEST-PREPD'               =>{},
    # 'TEST-PRE-POST'            =>{},
    # # 5.2.13. Metrology Input
    'TEST-INP-CHK-INP-P5-INV-DIEXY-S1'         =>{var: 'APC-DC-FLOAT', level: :sites, value: [1, 2, 3, 4, 1, 2, 3, 4],
      "siteY"=>"78694.701 78694.701 -47577.299 -47577.299 78694.701 78694.701 -47577.299 -47577.299",
      "siteX"=>"-75453.3521 -75453.3521 -125856.3521 -125856.3521 -75453.3521 -75453.3521 -125856.3521 -125856.3521",
      "site"=>"1 2 3 4 1 2 3 4"
    },
    'TEST-INP-CHK-INP-P5-INV-DIEXY-S1-CHK'     =>{var: 'APC-DC-STRING', level: :lot, value: 'FAIL'},
    'TEST-INP-CHK-INP-P5-VALID-DIEXY-S1'       =>{var: 'APC-DC-FLOAT', level: :sites, value: [1, 2, 3, 4, 1, 2, 3, 4],
      "siteY"=>"78694.701 78694.701 -47577.299 -47577.299 78694.701 78694.701 -47577.299 -47577.299",
      "siteX"=>"-75453.3521 -75453.3521 -125856.3521 -125856.3521 -75453.3521 -75453.3521 -125856.3521 -125856.3521",
      "site"=>"1 2 3 4 1 2 3 4",
      "dieY"=>"1 2 3 4 1 2 3 4",
      "dieX"=>"1 2 3 4 1 2 3 4"
    },
    'TEST-INP-CHK-INP-P5-VALID-DIEXY-S1-CHK'   =>{var: 'APC-DC-STRING', level: :lot, value: 'PASS'},
    # TODO: No data
    # 'TEST-INP-CHK-INP-P5-SITE-AS-DOUBLE-S1'    =>{var: 'APC-DC-FLOAT', level: :sites, value: [1, 2, 3, 4, 1, 2, 3, 4],
    # }
    'TEST-INP-CHK-INP-P5-SITE-AS-DOUBLE-S1-CHK'=>{var: 'APC-DC-STRING', level: :lot, value: 'FAIL'},
    # # 5.2.14. Litho Spec Access
    # TODO: No data
    # 'TEST-SCANNER-OFFSET',
    # 'TEST-RETICLE-OFFSET',
    # # 5.2.15. String Functions
    # 'TEST_WFR_SUPPLIER_CODE',
  }

  @@testlots = []
  @@use_http = false


  def self.after_all_passed
    @@testlots.each {|lot| @@sv.delete_lot_family(lot)}
  end


  def test00_setup
    $setup_ok = false
    #
    @@apcbl = APC::BaselineTest.new($env, sv: @@sv, use_http: @@use_http)
    assert @@apcbl.apc.catalyst.ping, "#{@@apcbl.apc.catalyst.inspect} not working"
    #
    assert @@lot = @@svtest.new_lot(nwafers: 8), 'error creating test lot'
    @@testlots << @@lot
    #
    $setup_ok = true
  end

  def test11_happy
    $setup_ok = false
    #
    assert @@apcbl.submit_cpcdcr(@@lot, @@eqp, slots: @@slots1 + @@slots2, readings: @@readings), 'error in APC run'
    failed = []
    @@resref.each_pair {|pname, ref|
      puts  # separate log lines
      @@apcbl.verify_cpcentry(pname, @@resref[pname]) || failed << pname
    }
    puts
    assert_empty failed, "#{failed.size} out of #{@@resref.size} tests failed"
    #
    $setup_ok = true
  end

  def test21_no_inv_diexy
    readings = @@readings.select {|k, v| k != 'INP-P5-INV-DIEXY-S1'}
    assert @@apcbl.submit_cpcdcr(@@lot, @@eqp, slots: @@slots1 + @@slots2, readings: readings), 'error in APC run'
    pname = 'TEST-INV-DIEXY-VALUES'
    ref = {var: 'APC-DC-STRING', level: :lot, value: 'FAILED'}
    assert @@apcbl.verify_cpcentry(pname, ref), "wrong result for #{pname.inspect}"
  end

end
