=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.


Author: Steffen Frieske, 2021-08-06


History:

Notes:
  see https://gfoundries.sharepoint.com/:x:/r/sites/Fab1QTControlerReplacement/Shared%20Documents/General/QT_scenarios_RTD_vs_SiViewWD.xlsx
  lots are kept in T_QTIME only for ~ 24 hrs
=end

require_relative 'QT_Scenarios'

class QT_Scenarios2 < QT_Scenarios
  @@sv_defaults = {product: 'UT-QT-SCENARIOS.01', route: 'UT-QT-SCENARIOS.01', nwafers: 5, carriers: 'QT%'}
  #@@sv_defaults = {product: 'A-QT-PRODUCT.01', route: 'A-QT-MAIN.01', nwafers: 5, carriers: 'QT%'}
  #@@opNo_trg1 = '1000.1200'
  #@@opNo_tgt1 = '1000.1500'

  def test201_Locate_into_QT_no_QTControl
    assert lot = @@svtest.new_lot, 'error creating test lot'
    @@testlots << lot
    # locate into QT - not on Trigger or Target PD
    assert_equal 0, @@sv.lot_opelocate(lot, "1000.1300"), 'error locating lot'
    assert_empty @@sv.lot_qtime_list(lot), 'lot has a SiView QT'  
  end

  def test202_Locate_into_QT_from_Target
    assert lot = @@svtest.new_lot, 'error creating test lot'
    @@testlots << lot
    # Initial trigger QT
    assert_equal 0, @@sv.lot_opelocate(lot, @@opNo_trg1), 'error locating lot'
    assert_equal 0, @@sv.lot_gatepass(lot), 'error gatepassing lot'
    refute_empty qtlist_ref = @@sv.lot_qtime_list(lot), 'lot has no QT'
    # End of QT
    assert_equal 0, @@sv.lot_opelocate(lot, @@opNo_tgt1), 'error locating lot'
    assert_equal 0, @@sv.lot_gatepass(lot), 'error gatepassing lot'
    assert_empty @@sv.lot_qtime_list(lot), 'lot has a SiView QT'
    # Locate from Target back to QT Area (Trigger PD)
    assert_equal 0, @@sv.lot_opelocate(lot, @@opNo_trg1), 'error locating lot'
    assert self.class.verify_qt_unchanged(lot, qtlist_ref)
  end

  def test203_Locate_OutOfQT
    assert lot = @@svtest.new_lot, 'error creating test lot'
    @@testlots << lot
    # Initial trigger QT
    assert_equal 0, @@sv.lot_opelocate(lot, @@opNo_trg1), 'error locating lot'
    assert_equal 0, @@sv.lot_gatepass(lot), 'error gatepassing lot'
    refute_empty qtlist_ref = @@sv.lot_qtime_list(lot), 'lot has no QT'
    # Locate out of QT
    assert_equal 0, @@sv.lot_opelocate(lot, "1000.1600"), 'error locating lot'
    assert_empty @@sv.lot_qtime_list(lot), 'lot has a SiView QT'  
  end
end