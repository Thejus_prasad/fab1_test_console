=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.


Author: Steffen Frieske, 2021-08-31

History:

Notes:
=end

require_relative 'QT_Scenarios'


class QT_Scenarios1 < QT_Scenarios
  @@sv_defaults = {product: 'UT-QT-SCENARIOS.01', route: 'UT-QT-SCENARIOS.01', nwafers: 5, carriers: 'QT%'}
  @@opNo_rwk_inqt = '1000.1300'     # start rework _after_ this op
  @@opNo_rwk_outofqt = '1000.1400'  # start rework _after_ this op


  def test101_happy
    assert lot = @@svtest.new_lot, 'error creating test lot'
    @@testlots << lot
    # gatepass at trigger QT
    assert_equal 0, @@sv.lot_opelocate(lot, @@opNo_trg1), 'error locating lot'
    assert_equal 0, @@sv.lot_gatepass(lot), 'error gatepassing lot'
    refute_empty @@sv.lot_qtime_list(lot), 'lot has no QT'
    # process/gatepass at target QT
    assert_equal 0, @@sv.lot_opelocate(lot, @@opNo_tgt1), 'error locating lot'
    assert_equal 0, @@sv.lot_gatepass(lot), 'error gatepassing lot'
    assert_empty @@sv.lot_qtime_list(lot), 'lot has no QT'
  end

  def test111_rwkwithin_replace
    assert lot = @@svtest.new_lot, 'error creating test lot'
    @@testlots << lot
    # gatepass at trigger QT
    assert_equal 0, @@sv.lot_opelocate(lot, @@opNo_trg1), 'error locating lot'
    assert_equal 0, @@sv.lot_gatepass(lot), 'error gatepassing lot'
    qtlist_ref = @@sv.lot_qtime_list(lot)
    refute_empty qtlist_ref, 'lot has no QT'
    # rework within QT
    assert_equal 0, @@sv.lot_opelocate(lot, @@opNo_rwk_inqt, offset: 1)
    assert_equal 0, @@sv.lot_rework(lot, @@rte_rwk_replace)
    assert self.class.verify_qt_unchanged(lot, qtlist_ref)
    # rework cancel
    assert_equal 0, @@sv.lot_rework_cancel(lot)
    assert self.class.verify_qt_unchanged(lot, qtlist_ref)
    # rework with replacement and gatepass back to main route
    assert_equal 0, @@sv.lot_rework(lot, @@rte_rwk_replace)
    assert self.class.verify_qt_unchanged(lot, qtlist_ref)
    while @@sv.lot_info(lot).route != @@svtest.route
      assert_equal 0, @@sv.lot_gatepass(lot), 'error gatepassing lot'
      assert self.class.verify_qt_unchanged(lot, qtlist_ref)
    end
  end

  def test112_rwkwithin_retrigger
    assert lot = @@svtest.new_lot, 'error creating test lot'
    @@testlots << lot
    # gatepass at trigger QT
    assert_equal 0, @@sv.lot_opelocate(lot, @@opNo_trg1), 'error locating lot'
    assert_equal 0, @@sv.lot_gatepass(lot), 'error gatepassing lot'
    qtlist_ref = @@sv.lot_qtime_list(lot)
    refute_empty qtlist_ref, 'lot has no QT'
    # rework within QT
    assert_equal 0, @@sv.lot_opelocate(lot, @@opNo_rwk_inqt, offset: 1)
    assert_equal 0, @@sv.lot_rework(lot, @@rte_rwk_retrigger)
    assert self.class.verify_qt_unchanged(lot, qtlist_ref)
    # rework cancel
    assert_equal 0, @@sv.lot_rework_cancel(lot)
    assert self.class.verify_qt_unchanged(lot, qtlist_ref)
    # rework and gatepass, QT retriggered
    assert_equal 0, @@sv.lot_rework(lot, @@rte_rwk_retrigger)
    assert self.class.verify_qt_unchanged(lot, qtlist_ref)
    tref = Time.now
    while @@sv.lot_info(lot).route != @@svtest.route
      assert_equal 0, @@sv.lot_gatepass(lot), 'error gatepassing lot'
      assert self.class.verify_qt_retriggered(lot, qtlist_ref, tref)
    end
  end

  def test113_rwkwithin_delete
    assert lot = @@svtest.new_lot, 'error creating test lot'
    @@testlots << lot
    # gatepass at trigger QT
    assert_equal 0, @@sv.lot_opelocate(lot, @@opNo_trg1), 'error locating lot'
    assert_equal 0, @@sv.lot_gatepass(lot), 'error gatepassing lot'
    qtlist_ref = @@sv.lot_qtime_list(lot)
    refute_empty qtlist_ref, 'lot has no QT'
    # rework within QT
    assert_equal 0, @@sv.lot_opelocate(lot, @@opNo_rwk_inqt, offset: 1)
    assert_equal 0, @@sv.lot_rework(lot, @@rte_rwk_delete)
    assert self.class.verify_qt_unchanged(lot, qtlist_ref)
    # rework cancel
    assert_equal 0, @@sv.lot_rework_cancel(lot)
    assert self.class.verify_qt_unchanged(lot, qtlist_ref)
    # rework and gatepass, QT deleted by 1st processing/gatepass
    assert_equal 0, @@sv.lot_rework(lot, @@rte_rwk_delete)
    assert self.class.verify_qt_unchanged(lot, qtlist_ref)
    while @@sv.lot_info(lot).route != @@svtest.route
      assert_equal 0, @@sv.lot_gatepass(lot), 'error gatepassing lot'
      assert_empty @@sv.lot_qtime_list(lot), 'lot has a QT'
    end
  end

  def test121_rwkoutof_replace
    assert lot = @@svtest.new_lot, 'error creating test lot'
    @@testlots << lot
    # gatepass at trigger QT
    assert_equal 0, @@sv.lot_opelocate(lot, @@opNo_trg1), 'error locating lot'
    assert_equal 0, @@sv.lot_gatepass(lot), 'error gatepassing lot'
    qtlist_ref = @@sv.lot_qtime_list(lot)
    refute_empty qtlist_ref, 'lot has no QT'
    # rework outof QT, QT unchanged
    assert_equal 0, @@sv.lot_opelocate(lot, @@opNo_rwk_outofqt, offset: 1)
    assert_equal 0, @@sv.lot_rework(lot, @@rte_rwk_replace), 'error starting rework'
    assert self.class.verify_qt_unchanged(lot, qtlist_ref)
    # rework cancel
    assert_equal 0, @@sv.lot_rework_cancel(lot)
    assert self.class.verify_qt_unchanged(lot, qtlist_ref)
    # rework with replacement, QT re-established
    assert_equal 0, @@sv.lot_rework(lot, @@rte_rwk_replace), 'error starting rework'
    assert self.class.verify_qt_unchanged(lot, qtlist_ref)
    # process/gatepass at replacement op, QT removed
    assert_equal 0, @@sv.lot_opelocate(lot, @@rwk_opNo_replace), 'error locating lot'
    assert_equal 0, @@sv.lot_gatepass(lot), 'error gatepassing lot'
    ## assert @@sv.claim_process_lot(lot)
    assert_empty @@sv.lot_qtime_list(lot), 'lot has a QT'
  end

  def test122_rwkoutof_noreplace
    assert lot = @@svtest.new_lot, 'error creating test lot'
    @@testlots << lot
    # gatepass at trigger QT
    assert_equal 0, @@sv.lot_opelocate(lot, @@opNo_trg1), 'error locating lot'
    assert_equal 0, @@sv.lot_gatepass(lot), 'error gatepassing lot'
    qtlist_ref = @@sv.lot_qtime_list(lot)
    refute_empty qtlist_ref, 'lot has no QT'
    # rework out of QT, QT removed
    assert_equal 0, @@sv.lot_opelocate(lot, @@opNo_rwk_outofqt, offset: 1)
    assert_equal 0, @@sv.lot_rework(lot, @@rte_rwk_noreplace), 'error starting rework'
    # QT remains unless processed:  assert_empty @@sv.lot_qtime_list(lot), 'lot has a QT'
    # rework cancel, QT re-established
    assert_equal 0, @@sv.lot_rework_cancel(lot)
    assert self.class.verify_qt_unchanged(lot, qtlist_ref)
    # rework with replacement, QT removed after 1st processing/gatepass
    assert_equal 0, @@sv.lot_rework(lot, @@rte_rwk_noreplace), 'error starting rework'
    #assert_empty @@sv.lot_qtime_list(lot), 'lot has a QT'
    # process/gatepass, QT removed
    assert_equal 0, @@sv.lot_gatepass(lot), 'error gatepassing lot'
    ## assert @@sv.claim_process_lot(lot)
    assert_empty @@sv.lot_qtime_list(lot), 'lot has a QT'
  end

end
