=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.


Author: Steffen Frieske, 2021-08-31

History:

Notes:
=end

require_relative 'QT_Scenarios'


class QT_Scenarios3 < QT_Scenarios
  @@sv_defaults = {product: 'UT-QT-SCENARIOS.01', route: 'UT-QT-SCENARIOS.01', nwafers: 5, carriers: 'QT%'}


  def test301_rwk_into_1off_replace
    assert lot = @@svtest.new_lot, 'error creating test lot'
    @@testlots << lot
    # process/gatepass at trigger QT
    assert_equal 0, @@sv.lot_opelocate(lot, @@opNo_trg1), 'error locating lot'
    assert_equal 0, @@sv.lot_gatepass(lot), 'error gatepassing lot'
    qtlist_ref = @@sv.lot_qtime_list(lot)
    refute_empty qtlist_ref, 'lot has no QT'
    # process/gatepass at target QT
    assert_equal 0, @@sv.lot_opelocate(lot, @@opNo_tgt1), 'error locating lot'
    assert_equal 0, @@sv.lot_gatepass(lot), 'error gatepassing lot'
    #assert @@sv.claim_process_lot(lot), 'error gatepassing lot'
    assert_empty @@sv.lot_qtime_list(lot), 'lot has a QT'
    # additional processing
    assert @@sv.claim_process_lot(lot), 'error gatepassing lot'
    #
    # rwk after tgt PD, no QT
    assert_equal 0, @@sv.lot_rework(lot, @@rte_rwk_replace), 'error starting rework'
    assert_empty @@sv.lot_qtime_list(lot), 'lot has a QT'
    # rework cancel
    assert_equal 0, @@sv.lot_rework_cancel(lot)
    assert_empty @@sv.lot_qtime_list(lot), 'lot has a QT'
    # rework with replacement, QT retriggerd after processing/gatepass
    assert_equal 0, @@sv.lot_rework(lot, @@rte_rwk_replace), 'error starting rework'
    assert_empty @@sv.lot_qtime_list(lot), 'lot has a QT'
    assert_equal 0, @@sv.lot_opelocate(lot, @@rwk_opNo_replace), 'error locating lot'
    tref = Time.now
    #assert_equal 0, @@sv.lot_gatepass(lot), 'error gatepassing lot'
    assert @@sv.claim_process_lot(lot)
    assert self.class.verify_qt_retriggered(lot, qtlist_ref, tref)  # currently fails
  end

  def test302_rwk_into_1off_noreplace
    assert lot = @@svtest.new_lot, 'error creating test lot'
    @@testlots << lot
    # process/gatepass at trigger QT
    assert_equal 0, @@sv.lot_opelocate(lot, @@opNo_trg1), 'error locating lot'
    assert_equal 0, @@sv.lot_gatepass(lot), 'error gatepassing lot'
    qtlist_ref = @@sv.lot_qtime_list(lot)
    refute_empty qtlist_ref, 'lot has no QT'
    # process/gatepass at target QT
    assert_equal 0, @@sv.lot_opelocate(lot, @@opNo_tgt1), 'error locating lot'
    assert_equal 0, @@sv.lot_gatepass(lot), 'error gatepassing lot'
    assert_empty @@sv.lot_qtime_list(lot), 'lot has a QT'
    # additional processing
    assert @@sv.claim_process_lot(lot), 'error gatepassing lot'
    #
    # rwk after tgt PD, no QT replacement
    assert_equal 0, @@sv.lot_rework(lot, @@rte_rwk_noreplace), 'error starting rework'
    while @@sv.lot_info(lot).route != @@svtest.route
      assert_equal 0, @@sv.lot_gatepass(lot), 'error gatepassing lot'
      assert_empty @@sv.lot_qtime_list(lot), 'lot has a QT'
    end
  end

  def test311_rwk_into_replace
    assert lot = @@svtest.new_lot, 'error creating test lot'
    @@testlots << lot
    # process/gatepass at trigger QT
    assert_equal 0, @@sv.lot_opelocate(lot, @@opNo_trg1), 'error locating lot'
    assert_equal 0, @@sv.lot_gatepass(lot), 'error gatepassing lot'
    qtlist_ref = @@sv.lot_qtime_list(lot)
    refute_empty qtlist_ref, 'lot has no QT'
    # process/gatepass at target QT
    assert_equal 0, @@sv.lot_opelocate(lot, @@opNo_tgt1), 'error locating lot'
    assert_equal 0, @@sv.lot_gatepass(lot), 'error gatepassing lot'
    #assert @@sv.claim_process_lot(lot), 'error gatepassing lot'
    assert_empty @@sv.lot_qtime_list(lot), 'lot has a QT'
    #
    # start rwk after tgt PD, no QT
    assert_equal 0, @@sv.lot_rework(lot, @@rte_rwk_replace), 'error starting rework'
    assert_empty @@sv.lot_qtime_list(lot), 'lot has a QT'
    # rework cancel
    assert_equal 0, @@sv.lot_rework_cancel(lot)
    assert_empty @@sv.lot_qtime_list(lot), 'lot has a QT'
    #
    # rework with replacement, QT retriggerd
    assert_equal 0, @@sv.lot_rework(lot, @@rte_rwk_replace), 'error starting rework'
    assert_empty @@sv.lot_qtime_list(lot), 'lot has a QT'
    assert_equal 0, @@sv.lot_opelocate(lot, @@rwk_opNo_replace), 'error locating lot'
    tref = Time.now
    #assert_equal 0, @@sv.lot_gatepass(lot), 'error gatepassing lot'
    assert @@sv.claim_process_lot(lot)
    assert self.class.verify_qt_retriggered(lot, qtlist_ref, tref)
  end

  def test312_rwk_into_noreplace
    assert lot = @@svtest.new_lot, 'error creating test lot'
    @@testlots << lot
    # process/gatepass at trigger QT
    assert_equal 0, @@sv.lot_opelocate(lot, @@opNo_trg1), 'error locating lot'
    assert_equal 0, @@sv.lot_gatepass(lot), 'error gatepassing lot'
    qtlist_ref = @@sv.lot_qtime_list(lot)
    refute_empty qtlist_ref, 'lot has no QT'
    # process/gatepass at target QT
    assert_equal 0, @@sv.lot_opelocate(lot, @@opNo_tgt1), 'error locating lot'
    assert_equal 0, @@sv.lot_gatepass(lot), 'error gatepassing lot'
    assert_empty @@sv.lot_qtime_list(lot), 'lot has a QT'
    #
    # start rwk after tgt PD, no QT
    assert_equal 0, @@sv.lot_rework(lot, @@rte_rwk_noreplace), 'error starting rework'
    assert_empty @@sv.lot_qtime_list(lot), 'lot has a QT'
    # rework cancel
    assert_equal 0, @@sv.lot_rework_cancel(lot)
    assert_empty @@sv.lot_qtime_list(lot), 'lot has a QT'
    #
    # rwk after tgt PD, original QT when on main route
    assert_equal 0, @@sv.lot_rework(lot, @@rte_rwk_noreplace), 'error starting rework'
    while @@sv.lot_info(lot).route != @@svtest.route
      assert_equal 0, @@sv.lot_gatepass(lot), 'error gatepassing lot'
      #assert_empty @@sv.lot_qtime_list(lot), 'lot has a QT'  # ??
      assert self.class.verify_qt_unchanged(lot, qtlist_ref)  # currently fails
    end
    assert self.class.verify_qt_unchanged(lot, qtlist_ref)  # currently fails
  end



  def XXtest303_rwk_into_replace_opestartcancel
    assert lot = @@svtest.new_lot, 'error creating test lot'
    @@testlots << lot
    # process/gatepass at trigger QT
    assert_equal 0, @@sv.lot_opelocate(lot, @@opNo_trg1), 'error locating lot'
    assert_equal 0, @@sv.lot_gatepass(lot), 'error gatepassing lot'
    assert entry = wait_lot_qtime(lot), 'lot is not in T_QTIME'
    # process/gatepass at target QT
    assert_equal 0, @@sv.lot_opelocate(lot, @@opNo_tgt1), 'error locating lot'
    assert_equal 0, @@sv.lot_gatepass(lot), 'error gatepassing lot'
    assert wait_lot_noqtime(lot), 'lot is still active in T_QTIME'
    #
    # rwk after tgt PD, with QT replacement
    assert_equal 0, @@sv.lot_rework(lot, @@rte_rwk_replace), 'error starting rework'
    # opestart and cancel at replacement PD
    assert @@sv.claim_process_lot(lot, opestart_cancel: true), 'error processing lot'
    # lot has neither SV nor RTD QT
    assert_empty @@sv.lot_qtime_list(lot), 'lot has a QT'
  end

  def XXtest_old_311_locateback_into
    assert lot = @@svtest.new_lot, 'error creating test lot'
    @@testlots << lot
    # process/gatepass at trigger QT
    assert_equal 0, @@sv.lot_opelocate(lot, @@opNo_trg1), 'error locating lot'
    assert_equal 0, @@sv.lot_gatepass(lot), 'error gatepassing lot'
    assert entry = wait_lot_qtime(lot), 'lot is not in T_QTIME'
    # process/gatepass at target QT
    assert_equal 0, @@sv.lot_opelocate(lot, @@opNo_tgt1), 'error locating lot'
    # assert_equal 0, @@sv.lot_gatepass(lot), 'error gatepassing lot'
    assert @@sv.claim_process_lot(lot), 'error gatepassing lot'
    assert wait_lot_noqtime(lot), 'lot is still active in T_QTIME'
    #
    # process/gatepass again after target PD then locate back
    tref = Time.now
    assert_equal 0, @@sv.lot_gatepass(lot), 'error gatepassing lot'
    assert_equal 0, @@sv.lot_opelocate(lot, @@opNo_tgt1), 'error locating lot'
    # lot has a SV QT and RTD QT  # C7.19: no SV QT
    # assert qtlist = @@sv.lot_qtime_list(lot).first, 'lot has no SiView QT'
  end

  def XXtest_old_312_locateback_into_immediately
    assert lot = @@svtest.new_lot, 'error creating test lot'
    @@testlots << lot
    # process/gatepass at trigger QT
    assert_equal 0, @@sv.lot_opelocate(lot, @@opNo_trg1), 'error locating lot'
    assert_equal 0, @@sv.lot_gatepass(lot), 'error gatepassing lot'
    assert entry = wait_lot_qtime(lot), 'lot is not in T_QTIME'
    # process/gatepass at target QT
    assert_equal 0, @@sv.lot_opelocate(lot, @@opNo_tgt1), 'error locating lot'
    # assert_equal 0, @@sv.lot_gatepass(lot), 'error gatepassing lot' # gatepass does not work!
    assert @@sv.claim_process_lot(lot), 'error gatepassing lot'
    assert wait_lot_noqtime(lot), 'lot is still active in T_QTIME'
    #
    # locate back from PD after target PD
    tref = Time.now
    assert_equal 0, @@sv.lot_opelocate(lot, @@opNo_tgt1), 'error locating lot'
    # lot has a SV QT and RTD QT  # C7.19: no SV QT
    # assert qtlist = @@sv.lot_qtime_list(lot).first, 'lot has no SiView QT'
  end

end
