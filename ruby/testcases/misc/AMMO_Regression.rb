=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.


Author: sfrieske, 2022-01-11

History:

Notes:
  as of V6 (2021) following methods are used:
    setWaferSelectionByMap
    getSelectedWafers
    getOnlySelectedWafers
    getAllSelectedWafersNoConflict
    removeWaferSelection
    maybe removeWaferSelectionForContext
=end

require 'misc/ammo'
require 'util/statistics'
require 'util/uniquestring'
require 'RubyTestCase'


# Stress test AMMO
class AMMO_Regression < RubyTestCase
  # ProcessType/ProcessLayer (PD UDATA) combination
  @@pt = 'DISPO'
  @@pl = 'LOTSTART'
  # for parallel query tests
  @@combinations = [['CFMWafer', 'LOTSTART'], ['DISPO', 'LOTSTART']]
  @@loops = 2

  @@waferids = []


  def self.startup
    super(nosiview: true)
  end

  def self.shutdown
    @@waferids.each {|wafers|
      ['APCUSER', 'GUIUSER', 'SUPERUSER'].each {|system|
        # never fails, even if a wafer has no selection, as long as selecting system is valid
        @@ammo.remove_wafer_selection(wafers, @@pt, @@pl, system: system)
      }
    }
  end


  def test00_setup
    $setup_ok = false
    #
    @@ammo = AMMO::Client.new($env)
    assert @@ammo.echo, 'no AMMO connection'
    #
    $setup_ok = true
  end

  def test11_must
    #
    # create new waferids
    s = unique_string
    wafers = (1..3).collect {|i| "#{s}%02dAM" % i}
    @@waferids << wafers
    # ensure the wafers are not registered in AMMO
    assert_nil @@ammo.get_selected_wafers(wafers, @@pt, @@pl), 'wafers are already known in AMMO'
    assert @@ammo.faultstring.include?('did not have an active selection'), 'wrong error message'
    #
    # set wafer selection, MUST (1)
    assert @@ammo.set_wafer_selection_bymap(wafers, @@pt, @@pl, 1)
    #
    # get_selected_wafers
    assert res = @@ammo.get_selected_wafers(wafers, @@pt, @@pl)
    assert_equal wafers, res.keys, 'wrong data'
    assert self.class.verify_selection(res, 1)
    #
    # get_only_selected_wafers
    assert res = @@ammo.get_only_selected_wafers(wafers, @@pt, @@pl)
    assert_equal wafers, res, 'wrong data'
    #
    # get_all_selected_wafers_noconflict
    assert res = @@ammo.get_all_selected_wafers_noconflict(wafers, @@pt, @@pl)
    assert_equal wafers, res.keys, 'wrong data'
    assert self.class.verify_selection(res, 1)
  end

  def test21_donotcare
    #
    # create new waferids
    s = unique_string
    wafers = (1..3).collect {|i| "#{s}%02dAM" % i}
    @@waferids << wafers
    # ensure the wafers are not registered in AMMO
    assert_nil @@ammo.get_selected_wafers(wafers, @@pt, @@pl), 'wafers are already known in AMMO'
    assert @@ammo.faultstring.include?('did not have an active selection'), 'wrong error message'
    #
    # set wafer selection, DONOTCARE (3)
    assert @@ammo.set_wafer_selection_bymap(wafers, @@pt, @@pl, 3)
    #
    # get_selected_wafers
    assert res = @@ammo.get_selected_wafers(wafers, @@pt, @@pl)
    assert_equal wafers, res.keys, 'wrong data'
    assert self.class.verify_selection(res, 3)
    #
    # get_only_selected_wafers
    assert res = @@ammo.get_only_selected_wafers(wafers, @@pt, @@pl)
    assert_equal [], res, 'wrong data'
    #
    # get_all_selected_wafers_noconflict
    assert res = @@ammo.get_all_selected_wafers_noconflict(wafers, @@pt, @@pl)
    assert_equal wafers, res.keys, 'wrong data'
    assert self.class.verify_selection(res, 3)
  end

  # will be developed on demand
  def test31_excluded
  end


  def test91_parallel
    @@ammo1 = AMMO::Client.new($env, logdir: 'log/ammo_parallel')
    assert @@ammo1.echo, 'no AMMO1 connection'
    @@ammo2 = AMMO::Client.new($env, logdir: 'log/ammo_parallel')
    assert @@ammo2.echo, 'no AMMO2 connection'
    @@res = []
    threads = []
    tstart = Time.now
    nwafers = 25
    [@@ammo1, @@ammo2].each_with_index {|cl, i|
      pt, pl = @@combinations[i]
      threads << Thread.new {
        @@loops.times {|n|
          $log.info {"\n-- loop #{n+1}/#{@@loops}, client ##{i}"} #if n % 10 == 0
          s = unique_string
          wafers = (1..nwafers).collect {|i| "#{s}%02dAA" % i}
          ok = cl.set_wafer_selection_bymap(wafers, pt, pl, 3, silent: true, export_fault: "set3_#{i}_#{n}.xml")
          @@res << (ok ? cl.mq.mq_response.duration : nil)
          ok = cl.get_selected_wafers(wafers, pt, pl, export_fault: "get3_#{i}_#{n}.xml")
          @@res << (ok ? cl.mq.mq_response.duration : nil)
          ok = cl.set_wafer_selection_bymap(wafers, pt, pl, 2, silent: true, export_fault: "set2_#{i}_#{n}.xml")
          @@res << (ok ? cl.mq.mq_response.duration : nil)
          ok = cl.get_selected_wafers(wafers, pt, pl, export_fault: "get2_#{i}_#{n}.xml")
          @@res << (ok ? cl.mq.mq_response.duration : nil)
          ok = cl.set_wafer_selection_bymap(wafers, pt, pl, 0, silent: true, export_fault: "set0_#{i}_#{n}.xml")
          @@res << (ok ? cl.mq.mq_response.duration : nil)
          ok = cl.get_selected_wafers(wafers, pt, pl, export_fault: "get0_#{i}_#{n}.xml")
          @@res << (ok ? cl.mq.mq_response.duration : nil)
          #
          ok = cl.set_wafer_selection_bymap(wafers, pt, pl, 1, silent: true, export_fault: "set1_#{i}_#{n}.xml")
          @@res << (ok ? cl.mq.mq_response.duration : nil)
          ok = cl.get_selected_wafers(wafers, pt, pl, export_fault: "get1_#{i}_#{n}.xml")
          @@res << (ok ? cl.mq.mq_response.duration : nil)
          ok = cl.get_only_selected_wafers(wafers, pt, pl, export_fault: "getonly_#{i}_#{n}.xml")
          @@res << (ok ? cl.mq.mq_response.duration : nil)
          ok = cl.get_all_selected_wafers_noconflict(wafers, pt, pl, export_fault: "getall_#{i}_#{n}.xml")
          @@res << (ok ? cl.mq.mq_response.duration : nil)
          #
          ['APCUSER', 'GUIUSER', 'SUPERUSER'].each {|system|
            ok = cl.remove_wafer_selection(wafers, pt, pl, system: system, silent: true)
            @@res << (ok ? cl.mq.mq_response.duration : nil)
          }
        }
      }
    }
    threads.each_with_index {|t, i|
      $log.info "joining thread #{i}"
      t.join
    }
    $log.info "res: #{@@res}"
  end


  # aux methods

  def self.verify_selection(res, selectflag)
    ok = true
    res.each_pair {|wafer, data|
      if data[:selectFlag] != selectflag
        $log.warn "wrong selectflag for wafer #{wafer}: expected #{selectflag}, got #{data[:selectFlag]}"
        ok = false
      end
    }
    return ok
  end

end
