=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: sfrieske, 2018-07-27

History:
  2020-08-04 aschmid3, 1) delete/deactivate setup_channels call, 2) add topic of LotHolds for Space charts

=end

require 'sil/siltest'
require 'SiViewTestCase'


# Create data to test FabGUI SiView Control - Multi Hold (-> Lot Hold Release) and Inhibit (-> InhibitList)
# for Space chart entries (SIL)
class FabGUI_SC_MHLHR_Inhibit_SIL < SiViewTestCase
  @@sv_defaults = {route: 'SIL-0001.01', product: 'SILPROD.01', carriers: 'SIL%'}
  @@testlots = []

  def self.after_all_passed
    @@sv.eqp_cleanup(@@siltest.ptool)
    @@testlots.each {|lot| @@sv.delete_lot_family(lot)}
  end


  def test00_setup
    assert lot = @@svtest.new_lot, 'error creating testlot'
    @@testlots << lot
    @@siltest = SIL::Test.new($env, sv: @@sv, lot: lot)
  end

  def test11_SIL
    assert_equal 0, @@sv.eqp_cleanup(@@siltest.ptool), "error cleaning up eqp #{@@siltest.ptool.inspect}"
    assert channels = @@siltest.setup_channels(cas: ['EquipmentInhibit', 'AutoEquipmentInhibit', 'ReleaseEquipmentInhibit', 'AutoLotHold', 'CancelLotHold'])
    # Rolled back 17.09.2020 --> row for 'channels' deactivated due to issues after last PROD-input-data to ITDC (06-07?/2020)
    ## substituted for this still existing issue --> used template adjusted at SpaceNavigator under:
    #   'Inline_Fab1' -> '_TEMPLATES' -> 'SPC Channels' -> "_Template_QA_PARAMS_900"
    #     -> right mouse click -> 'Properties...' -> tab 'Valuations & Events' -> table of 'Valuations of Apply' 
    #     -> contained entries should show follow entries at table 'Events Assigned to Selected Valuations' below
    #        - for Inhibits: 'EquipmentInhibit', 'AutoEquipmentInhibit', 'ReleaseEquipmentInhibit' (these are 'Corrective Actions')
    #        - for Holds: 'AutoLotHold', 'CancelLotHold' (these are 'Corrective Actions')
    #        (possible CAs to find under: SpaceNavigator -> 'Events' -> 'DEFAULT_DIVISION' -> 'Corrective Actions')
    assert @@siltest.submit_process_dcr, 'error sending DCR'
    assert dcr = @@siltest.submit_meas_dcr(ooc: true, nooc: 'all*2'), 'error submitting DCR'
    assert @@siltest.wait_inhibit(:eqp, @@siltest.ptool, '', n: channels.size), 'equipment must still have all inhibits'
    # Rolled back 17.09.2020 --> row replacement necessary due to issue for setup_channels above
    # assert @@siltest.wait_inhibit(:eqp, @@siltest.ptool, '', n: 5), 'equipment must still have all inhibits'
    #
    # Gate Pass necessary for LotHold activation due to HoldTiming = Post
    assert_equal 0, @@sv.lot_gatepass(@@testlots)
    #
    puts "\n\n--> SIL LotHold and Inhibit test data are prepared with eqp #{@@siltest.ptool}. Execute the tests and press Enter to continue."
    gets
  end

end
