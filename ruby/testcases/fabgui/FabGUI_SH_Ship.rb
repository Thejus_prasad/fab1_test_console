=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author:   aschmid3 and sfrieske, 2019-05-24

Version: 26.0.6 with 'MoveEvent' fct. (FabGUI config "SHIPPING.AsmView.TurnkeyB2B.Disabled=false")

History:
  2020-01-22 aschmid3 and sfrieske, verification and adjustment of existing tests due to new behaviour 'MoveEvent' (lot complete fct.)
  2021-09-15 sfrieske, finally replaced $svtest by @@svtest, $sv by @@sv and $sm by @@sm

Notes:
  2019-12, as first v26.0.6 was rollout with FabGUI config "SHIPPING.AsmView.TurnkeyB2B.Disabled=true" where shipping regression tests are executed as confirmed working in v24.0.6 (F1 Prod)

=end

require 'asmview'
require 'jcap/turnkeybtf'  # for TestDB
require 'jcap/turnkeybtftest'
require 'SiViewTestCase'


# Create lots to test FabGUI Shipping
class FabGUI_SH_Ship < SiViewTestCase
  @@sv_defaults = {carrier_category: nil, carriers: '%', customer: 'mtk'} # not 'gf' or 'amd'
  @@newlot_params = {
    #
    # final shipment
    # no AsmView tracking
    noav_u: {product: 'UT-TKBTF.01', route: 'UTRT-TKBTF.01', user_parameters: {}},
    noav_u_avprod: {product: 'SHELBY-TKBTF.QA', route: 'UTRT-TKBTF.01', user_parameters: {}},
    # TkType N redundant, same as J and R
    noav_r: {product: 'UT-TKBTF.01', route: 'UTRT-TKBTF.01',
      user_parameters: {'TurnkeyType'=>'R', 'TurnkeySubconIDBump'=>'GG', 'TurnkeySubconIDSort'=>'GG', 'TurnkeySubconIDReflow'=>'GG'}
    },
    noav_s: {product: 'UT-TKBTF.01', route: 'UTRT-TKBTF.01',
      user_parameters: {'TurnkeyType'=>'S', 'TurnkeySubconIDSort'=>'GG'}
    },
    noav_l: {product: 'UT-TKBTF.01', route: 'UTRT-TKBTF.01',
      user_parameters: {'TurnkeyType'=>'L', 'TurnkeySubconIDBump'=>'GG'}
    },
    # with AsmView tracking
    #   also working (?): {product: 'CN78002AC.D1', route: 'C02-1431.01', erpitem: 'CN78002AC-JD0', customer: 'mtk'}
    withav_j: {product: 'SHELBY-TKBTF.QA', route: 'UTRT-TKBTF.01', erpitem: '*SHELBY21AE-M00-JB4',
      user_parameters: {'TurnkeyType'=>'J', 'TurnkeySubconIDBump'=>'GG', 'TurnkeySubconIDSort'=>'GG', 'ERPSalesOrder'=>'1234567'}
    },
    withav_s: {product: 'SHELBY-TKBTF.QA', route: 'UTRT-TKBTF.01', erpitem: '*SHELBY21AE-M00-JB4',
      user_parameters: {'TurnkeyType'=>'S', 'TurnkeySubconIDSort'=>'GG'}
    },
    withav_r: {product: 'UT-TKBTF-R.01', route: 'UTRT-TKBTF-R.01', erpitem: '*SHELBY21AE-M00-JB4',
      user_parameters: {'TurnkeyType'=>'R', 'TurnkeySubconIDBump'=>'GG', 'TurnkeySubconIDSort'=>'GG', 'TurnkeySubconIDReflow'=>'GG'}
    },
    withav_l: {product: 'SHELBY-TKBTF-L.QA', route: 'UTRT-TKBTF-L.01', erpitem: '*SHELBY21AE-M00-JB4',
      user_parameters: {'TurnkeyType'=>'L', 'TurnkeySubconIDBump'=>'GG'}
    },
    # with AsmView tracking, BackupOperation
    withav_j_f8: {product: 'SHELBY3CC.01', route: 'FF-28LPQ-45M.01', erpitem: '*SHELBY3CC-J01',
      user_parameters: {'TurnkeyType'=>'J', 'TurnkeySubconIDBump'=>'GG', 'TurnkeySubconIDSort'=>'GG'}
    },
    #
    # subcon (trans) shipment
    subcon_j_bump: {product: 'UT-TKBTF-SUBCON-BUMP.01', route: 'UTRT-TKBTF-SUBCON-BUMP.01', erpitem: '*SHELBY21AE-M00-JB4',
      user_parameters: {'TurnkeyType'=>'J', 'TurnkeySubconIDBump'=>'ST', 'TurnkeySubconIDSort'=>'AK'}
                                            # Use value "ST" for TkSubconIDBump per Tony Genenncher
    },
    subcon_j_sort: {product: 'UT-TKBTF-SUBCON-SORT.01', route: 'UTRT-TKBTF-SUBCON-SORT.01', erpitem: '*SHELBY21AE-M00-JB4',
      user_parameters: {'TurnkeyType'=>'J', 'TurnkeySubconIDBump'=>'GG', 'TurnkeySubconIDSort'=>'6T'}
                                                                        # Used values at Fab1 "ST", "6T", "JC" per Tony Genenncher
    },
    noav_uu_fab: {product: 'UT-TKFAB.01', route: 'UTRT-TKFAB.01', customer: 'everspin', erpitem: 'MCKINL11AA-UD0',
                                                                    # customer and erpitem have to match
      user_parameters: {'TurnkeyType'=>'UU', 'TurnkeySubconIDBump'=>'G7'}
                                            # Normally TkSubconIDFab required but it's unusable per Tony Genenncher at the moment
                                            # therefore this workaround is using also in PROD
    },
    noav_uu_tranfab: {product: 'UT-TKTRANFAB.01', route: 'UTRT-TKTRANFAB.01', customer: 'everspin', erpitem: 'MCKINL11AA-UD0',
                                                                    # customer and erpitem have to match
      user_parameters: {'TurnkeyType'=>'UU', 'TurnkeySubconIDBump'=>'G7'}
                                            # Normally TkSubconIDFab required but it's unusable per Tony Genenncher at the moment
                                            # therefore this workaround is using also in PROD
    },

    # this is like Fab10 lots arrive (test prep Arthur)
    withav_restb_bump10: {product: 'WARP352-A1FABB.AA01', route: '000-BUMP-01-PEB2NCG.0001',
      user_parameters: {'TurnkeyType'=>'R', 'TurnkeySubconIDBump'=>'GG', 'TurnkeySubconIDSort'=>'GB', 'TurnkeySubconIDReflow'=>'GB',
                        'CountryOfOrigin'=>'US', 'SourceFabCode'=>'F10'} # necessary due to jcap-job (X-JSTBWA) modified this during execution
    },
    #
    # transbumpFab: {product: 'WARP352-A1FABU.AA01', route: '000-FAB-00-BTFRCVX.0001', erpitem: '0000001VN343', customer: 'ericab',
    #   user_parameters: {'TurnkeyType'=>'R', 'TurnkeySubconIDBump'=>'GG', 'TurnkeySubconIDSort'=>'GB'}
    # },
    transbump: {product: 'WARP352-A1FABB.AA01', route: '000-BUMP-01-PEB2NCG.0001', erpitem: '0000001VN343', customer: 'ericab',
      user_parameters: {'TurnkeyType'=>'R', 'TurnkeySubconIDBump'=>'GG', 'TurnkeySubconIDSort'=>'GB', 'CountryOfOrigin'=>'US', 'SourceFabCode'=>'F10'}
    },
    # nonstandard shipment
    noav_u_amd: {product: 'UT-TKBTF.01', route: 'UTRT-TKBTF.01', customer: 'amd', user_parameters: {}},
    # These items belong together: customer, ERPItem, PurchaseOrder [and CustomerItem (= ERPItem value)]
    # Matching values get from Tony Genenncher
    # Product have to be listed together with ERPItem at report 'ERP Data Mirror'
    noav_u_nxptw: {product: '2SN100B-A1FAAU.AA01', route: 'C02-1431.01', customer: 'nxptw', erpitem: '2SN100B-A1FAAU-UAA01',
      user_parameters: {'PurchaseOrder'=>'FOC:1582072'}
    },
    withav_j_amd: {product: 'SHELBY-TKBTF.QA', route: 'UTRT-TKBTF.01', customer: 'amd', erpitem: '*SHELBY21AE-M00-JB4',
      user_parameters: {'TurnkeyType'=>'J', 'TurnkeySubconIDBump'=>'GG', 'TurnkeySubconIDSort'=>'GG'}
    },
    noav_tw: {product: 'UT-MN-UTC001-SHIP.01', route: 'UT-MN-UTC001-SHIP.01', customer: '', user_parameters: {}},
    # sales order shipment
    # customer + 'ERPItem' have to match to used 'Order Number' (FabGUI) resp. 'ERPSalesOrder' (Lot.ScriptParameter)
    ordernoav: {product: 'UT-TKBTF.01', route: 'UTRT-TKBTF.01', customer: 'gshuttle',
      # outdated: 'ERPItem'=>'MPW03291AA-UA0'with 'Order Number' = 6010003641:2:1 (EOL = EndOfLife)
      user_parameters: {'ERPItem'=>'MPW0350-A1FAAU-UAA01'} # 'Order Number' = 6010007260:2:1 (FabGUI)
    }
  }
  # stages on the BUMP route
  @@stages_BUMP = {
    'UBM'=>['TKYUBM.1', true],
    'SLD'=>['TKYSLDR.1', true],
    'RFL'=>['TKYRW1.1', true],
    'OQAB'=>['TKYOQAB.1', false],  # 2020-05-07 - aschmid3: changed from "true" to "false" because lot is already at 'TKYTRANS.1' (w/o MoveEvent fct)
    'DSB'=>['TKYOQAB.1', false]  # 2020-05-25 - aschmid3: changed from 'TKYTRANS.1' to 'TKYOQAB.1' because MoveEvent fct execute the moving to 'TKYTRANS.1'
  }
  # src fab F8 / US, with AsmView tracking (backup operations required, ERPItem must match product and customer(?))
  @@env_src = 'f8stag'
  @@sv_defaults_src = {carrier_category: nil, carriers: '%', customer: 'qgt'}  # 'qgt' and 'mtk' not working in AsmView (lot hold before TTS)
  @@newlot_params_f8 = {
    withav_r_f8_BUMP: {product: 'UT-PRODUCT-FAB1.01', route: 'UTRT101.01', erpitem: '0000001VN343', customer: 'ericab',
      user_parameters: {'TurnkeyType'=>'R', 'TurnkeySubconIDBump'=>'GG', 'TurnkeySubconIDSort'=>'GB'}
    },
    withav_j_f8: {product: 'SHELBY3CC.01', route: 'FF-28LPQ-45M.01', erpitem: '*SHELBY3CC-J01',
      user_parameters: {'TurnkeyType'=>'J', 'TurnkeySubconIDBump'=>'GG', 'TurnkeySubconIDSort'=>'GG'}
    },
  #   withav_r_f8: {
  #     product: 'SHELBY3CC.01', route: 'FF-28LPQ-45M.01', erpitem: '*SHELBY3CC-J01',
  #     user_parameters: {'TurnkeyType'=>'R', 'TurnkeySubconIDBump'=>'GG', 'TurnkeySubconIDSort'=>'GG'}
  #   },
  #   withav_s_f8: {
  #     product: 'SHELBY3CC.01', route: 'FF-28LPQ-45M.01', erpitem: '*SHELBY3CC-J01',
  #     user_parameters: {'TurnkeyType'=>'S', 'TurnkeySubconIDSort'=>'GG'}
  #   },
  #   withav_l_f8: {
  #     product: 'ELLSMR11MA.A0', route: 'FF-ELLSMR1MA.01', erpitem: 'ELLSMR11MA-M01-LA0',
  #     user_parameters: {'TurnkeyType'=>'L', 'TurnkeySubconIDBump'=>'GG'}
  #   },
  }
  @@psend_rcv = {backup_op: /TKEYBUMPSHIPINIT/, backup_bank: 'O-BACKUPOPER'}
  @@binning_data = {1=>[628, 0, 618], 2=>[2332, 0, 1234], 4=>[1005, 222, 56]}

  @@testlots = []
  @@testlots_bop = []


  def self.after_all_passed
    @@testlots_bop.each {|lot| @@sv_src.backup_delete_lot_family(lot, @@sv)}
    @@testlots.each {|lot| @@sv.delete_lot_family(lot)}
  end


  def test00_setup
    $setup_ok = false
    #
    @@newlot_params.each_value {|v|
      v[:user_parameters]['ShipStorageExpiration'] = '20300303.235900'
      v[:user_parameters]['QualityGateApproval'] = 'Approved,qauser'
    }
    @@tktest = Turnkey::BTFTest.new($env, sv: @@sv)
    @@av = @@tktest.av
    @@dbbtf = Turnkey::TestDB.new('itdc', sv: @@sv)
    #
    @@svtest_src = SiView::Test.new(@@env_src, @@sv_defaults_src)
    @@sv_src = @@svtest_src.sv
    #
    $setup_ok = true
  end


  # Standard Shipment no AsmView (Fab1 -> Customer)

  def test111_FinalStd_noav_U # FinalStd_noav_processing with UT-* product (not included in FabGUI-AsmView-config)
    # create lot, locate it to LOTSHIPDESIG and set OQAGoodDie
    assert lot = @@svtest.new_lot(@@newlot_params[:noav_u]), 'error creating Fab lot'
    @@testlots << lot
    assert_equal 0, @@sv.lot_opelocate(lot, nil, op: 'LOTSHIPDESIG.01')
    @@sv.lot_info(lot, wafers: true).wafers.each {|w|
      assert_equal 0, @@sv.user_parameter_change('Wafer', w.wafer, 'OQAGoodDie', 100, 1, datatype: 'INTEGER')
    }
    #
    puts "\n\nLot #{lot} is ready for shipment"
    gets
    # verify in sv
    $log.info 'verifying lot script parameter ShipComment'
    assert @@sv.user_parameter('Lot', lot, name: 'ShipComment').valueflag, 'script parameter ShipComment not set'
    #
    puts "\n\nPress Enter to continue with next test."
    gets
  end

  # Skipped for FabP v26.0.6/v28.0.7 + MoveEvent and ignore of "PP AsmView" (CR82880/CR83440)
  def test112_FinalStd_noav_U_avprod  # FinalStd_noav_processing with configured AsmView product (included in FabGUI-AsmView-config)
    # create lot, locate it to LOTSHIPDESIG and set OQAGoodDie
    assert lot = @@svtest.new_lot(@@newlot_params[:noav_u_avprod]), 'error creating Fab lot'
    @@testlots << lot
    assert_equal 0, @@sv.lot_opelocate(lot, nil, op: 'LOTSHIPDESIG.01')
    @@sv.lot_info(lot, wafers: true).wafers.each {|w|
      assert_equal 0, @@sv.user_parameter_change('Wafer', w.wafer, 'OQAGoodDie', 100, 1, datatype: 'INTEGER')
    }
    #
    puts "\n\nLot #{lot} is ready for shipment"
    gets
    # verify in sv
    $log.info 'verifying lot script parameter ShipComment'
    assert @@sv.user_parameter('Lot', lot, name: 'ShipComment').valueflag, 'script parameter ShipComment not set'
    #
    puts "\n\nPress Enter to continue with next test."
    gets
  end

  # Skipped for FabP v26.0.6/v28.0.7 + MoveEvent and ignore of "PP AsmView" (CR82880/CR83440)
  def test131_FinalStd_noav_S # FinalStd_noav_processing for "S = Sort only" with UT-* product
    # create lot, locate it to LOTSHIPDESIG and set DieCountGood
    assert lot = @@svtest.new_lot(@@newlot_params[:noav_s]), 'error creating Fab lot'
    @@testlots << lot
    assert_equal 0, @@sv.lot_opelocate(lot, nil, op: 'LOTSHIPDESIG.01')
    @@sv.lot_info(lot, wafers: true).wafers.each {|w|
      assert_equal 0, @@sv.user_parameter_change('Wafer', w.wafer, 'DieCountGood', 100, 1, datatype: 'INTEGER')
    }
    #
    puts "\n\nLot #{lot} is ready for shipment"
    gets
    # verify in sv
    $log.info 'verifying lot script parameter ShipComment'
    assert @@sv.user_parameter('Lot', lot, name: 'ShipComment').valueflag, 'script parameter ShipComment not set'
    #
    puts "\n\nPress Enter to continue with next test."
    gets
  end

  # Skipped for FabP v26.0.6/v28.0.7 + MoveEvent and ignore of "PP AsmView" (CR82880/CR83440)
  def test132_FinalStd_noav_R # FinalStd_noav_processing
    # create lot, locate it to LOTSHIPDESIG and set DieCountGood
    assert lot = @@svtest.new_lot(@@newlot_params[:noav_r]), 'error creating Fab lot'
    @@testlots << lot
    assert_equal 0, @@sv.lot_opelocate(lot, nil, op: 'LOTSHIPDESIG.01')
    @@sv.lot_info(lot, wafers: true).wafers.each {|w|
      assert_equal 0, @@sv.user_parameter_change('Wafer', w.wafer, 'DieCountGood', 100, 1, datatype: 'INTEGER')
    }
    #
    puts "\n\nLot #{lot} is ready for shipment"
    gets
    # verify in sv
    $log.info 'verifying lot script parameter ShipComment'
    assert @@sv.user_parameter('Lot', lot, name: 'ShipComment').valueflag, 'script parameter ShipComment not set'
    #
    puts "\n\nPress Enter to continue with next test."
    gets
  end

  # Skipped for FabP v26.0.6/v28.0.7 + MoveEvent and ignore of "PP AsmView" (CR82880/CR83440)
  def test133_FinalStd_noav_L # FinalStd_noav_processing for "L = Bump only" with UT-* product
    # create lot, locate it to LOTSHIPDESIG and set OQAGoodDie
    assert lot = @@svtest.new_lot(@@newlot_params[:noav_l]), 'error creating Fab lot'
    @@testlots << lot
    assert_equal 0, @@sv.lot_opelocate(lot, nil, op: 'LOTSHIPDESIG.01')
    @@sv.lot_info(lot, wafers: true).wafers.each {|w|
      assert_equal 0, @@sv.user_parameter_change('Wafer', w.wafer, 'OQAGoodDie', 100, 1, datatype: 'INTEGER')
    }
    #
    puts "\n\nLot #{lot} is ready for shipment"
    gets
    # verify in sv
    $log.info 'verifying lot script parameter ShipComment'
    assert @@sv.user_parameter('Lot', lot, name: 'ShipComment').valueflag, 'script parameter ShipComment not set'
    #
    puts "\n\nPress Enter to continue with next test."
    gets
  end

  # def test134_FinalStd_noav_N # redundant, same as J and R
  #   # create lot, locate it to LOTSHIPDESIG and set DieCountGood
  #   assert lot = @@svtest.new_lot(@@newlot_params[:noav_n]), 'error creating Fab lot'
  #   @@testlots << lot
  #   assert_equal 0, @@sv.lot_opelocate(lot, nil, op: 'LOTSHIPDESIG.01')
  #   @@sv.lot_info(lot, wafers: true).wafers.each {|w|
  #     assert_equal 0, @@sv.user_parameter_change('Wafer', w.wafer, 'DieCountGood', 100, 1, datatype: 'INTEGER')
  #   }
  #   #
  #   puts "\n\nLot #{lot} is ready for shipment"
  #   gets
  #   # verify in sv
  #   $log.info 'verifying lot script parameter ShipComment'
  #   assert @@sv.user_parameter('Lot', lot, name: 'ShipComment').valueflag, 'script parameter ShipComment not set'
  #   #
  #   puts "\n\nPress Enter to continue with next test."
  #   gets
  # end


  # Standard Shipment with AsmView (Fab1 -> Customer)

  def test221_FinalStd_withav_J
    $setup_ok = false
    # create lot and process it through BTF with AsmView tracking
    assert lot = @@svtest.new_lot(@@newlot_params[:withav_j]), 'error creating test lot'
    @@testlots << lot
    assert @@tktest.start_btf(lot), 'error starting test lot'
    # assert @@tktest.process_btf(lot), 'error processing lot'
    assert @@tktest.prepare_shipment(lot, 'J'), 'error preparing lot'
    # lot is at (sv) LOTSHIPDESIG.01, (av) TKYOQAS.1 (out-dated: TKYSORT.1), set TestDB data and DieCountGood
    assert sortdata = @@dbbtf.set_lot_wafer_bins(lot, binning_data: @@binning_data, goodbins: [0, 1])
    $setup_ok = true
    #
    puts "\n\nLot #{lot} is ready for shipment"
    gets
    # verify in sv
    $log.info 'verifying (sv) lot script parameter ShipComment'
    assert @@sv.user_parameter('Lot', lot, name: 'ShipComment').valueflag, '(sv) ShipComment not set'
    # verify in av
    $log.info 'verifying (av) lot script parameters FabQualityCheck and ERPSalesOrder, (av) PD and binning data'
    assert avlot = @@tktest.get_avlot(lot), "(sv) missing LOT_LABEL for #{lot}"
    assert @@av.user_parameter('Lot', avlot, name: 'FabQualityCheck').valueflag, '(av) FabQualityCheck not set'
    assert_equal 'Complete', @@av.user_parameter('Lot', avlot, name: 'FabQualityCheck').value, '(av) FabQualityCheck wrong value'
    assert @@av.user_parameter('Lot', avlot, name: 'ERPSalesOrder').valueflag, '(av) ERPSalesOrder not set' # request of Andreas Pohl
    assert_equal 'LOTSHIPD.1', @@av.lot_info(avlot).op, 'wrong op in AsmView'
    assert @@tktest.verify_diecount(avlot, sortdata), 'binning data mismatch in AsmView'
    # TODO:
    # Check for Oracle-ERP
    # SubProcessComplete = Complete (waiting time = 8 min)
    #
    puts "\n\nPress Enter to continue with next test."
    gets
  end

  def test231_FinalStd_withav_S  # S: Sort only
    $setup_ok = false
    # create lot and process it through BTF SORT with AsmView tracking
    assert lot = @@svtest.new_lot(@@newlot_params[:noav_s]), 'error creating test lot'  # was: withav_s
    @@testlots << lot
    assert @@tktest.start_btf(lot), 'error starting test lot'
    # assert @@tktest.process_btf(lot), 'error processing lot'
    assert @@tktest.prepare_shipment(lot, 'S'), 'error preparing lot'
    # lot is at (sv) LOTSHIPDESIG.01, (av) TKYOQAS.1 (out-dated: TKYSORT.1), set TestDB data and DieCountGood (defaults to 100 for all wafers)
    assert sortdata = @@dbbtf.set_lot_wafer_bins(lot), 'error setting binning data'
    $setup_ok = true
    #
    puts "\n\nLot #{lot} is ready for shipment"
    gets
    # verify in sv
    $log.info 'verifying (sv) lot script parameter ShipComment'
    assert @@sv.user_parameter('Lot', lot, name: 'ShipComment').valueflag, '(sv) ShipComment not set'
    # verify in av
    $log.info 'verifying (av) lot script parameter FabQualityCheck, (av) PD and binning data'
    assert avlot = @@tktest.get_avlot(lot), "(sv) missing LOT_LABEL for #{lot}"
    assert @@av.user_parameter('Lot', avlot, name: 'FabQualityCheck').valueflag, '(av) FabQualityCheck not set'
    assert_equal 'Complete', @@av.user_parameter('Lot', avlot, name: 'FabQualityCheck').value, '(av) FabQualityCheck wrong value'
    assert_equal 'LOTSHIPD.1', @@av.lot_info(avlot).op, 'wrong op in AsmView'
    assert @@tktest.verify_diecount(avlot, sortdata), 'binning data mismatch in AsmView'
    #
    puts "\n\nPress Enter to continue with next test."
    gets
  end

  ## Additional test for TkT = R because additional stage btw sort and ship --> bisher waren TkT=R Produkte vom PilotMode ausgenommen
  def test232_FinalStd_withav_R
    $setup_ok = false
    # create lot and process it through BTF with AsmView tracking
    assert lot = @@svtest.new_lot(@@newlot_params[:withav_r]), 'error creating test lot'
    @@testlots << lot
    assert @@tktest.start_btf(lot), 'error starting test lot'
    # assert @@tktest.process_btf(lot), 'error processing lot'
    assert @@tktest.prepare_shipment(lot, 'R'), 'error preparing lot'
    # lot is at (sv) LOTSHIPDESIG.01, (av) TKYOQAR.1, set TestDB data and DieCountGood (defaults to 100 for all wafers)
    # assert @@dbbtf.set_lot_wafer_bins(lot), 'error setting binning data'
    assert sortdata = @@dbbtf.set_lot_wafer_bins(lot, binning_data: @@binning_data, goodbins: [0, 1])
    $setup_ok = true
    #
    puts "\n\nLot #{lot} is ready for shipment"
    gets
    # verify in sv
    $log.info 'verifying (sv) lot script parameter ShipComment'
    assert @@sv.user_parameter('Lot', lot, name: 'ShipComment').valueflag, '(sv) ShipComment not set'
    # verify in av
    $log.info 'verifying (av) lot script parameter FabQualityCheck, (av) PD and binning data'
    assert avlot = @@tktest.get_avlot(lot), "(sv) missing LOT_LABEL for #{lot}"
    assert @@av.user_parameter('Lot', avlot, name: 'FabQualityCheck').valueflag, '(av) FabQualityCheck not set'
    assert_equal 'Complete', @@av.user_parameter('Lot', avlot, name: 'FabQualityCheck').value, '(av) FabQualityCheck wrong value'
    assert_equal 'LOTSHIPD.1', @@av.lot_info(avlot).op, 'wrong op in AsmView'
    assert @@tktest.verify_diecount(avlot, sortdata), 'binning data mismatch in AsmView'
    #
    puts "\n\nPress Enter to continue with next test."
    gets
  end

  def test233_FinalStd_withav_L  # L: Bump only
    $setup_ok = false
    # create lot and process it through BTF with AsmView tracking, final PDs are (sv) LOTSHIPDESIG.01, (av) LOTSHIPD.1
    assert lot = @@svtest.new_lot(@@newlot_params[:withav_l]), 'error creating test lot'  # TODO: use product from noav_l (?)
    @@testlots << lot
    assert @@tktest.start_btf(lot), 'error starting test lot'
    # assert @@tktest.process_btf(lot), 'error processing lot'
    assert @@tktest.prepare_shipment(lot, 'L'), 'error preparing lot'
    @@sv.lot_info(lot, wafers: true).wafers.each {|w|
      assert_equal 0, @@sv.user_parameter_change('Wafer', w.wafer, 'OQAGoodDie', 100, 1, datatype: 'INTEGER')
    }
    $setup_ok = true
    #
    puts "\n\nLot #{lot} is ready for shipment"
    gets
    # verify in sv
    $log.info 'verifying (sv) lot script parameter ShipComment'
    assert @@sv.user_parameter('Lot', lot, name: 'ShipComment').valueflag, '(sv) ShipComment not set'
    # verify in av
    $log.info 'verifying (av) lot script parameter FabQualityCheck and (av) PD'
    assert avlot = @@tktest.get_avlot(lot), "(sv) missing LOT_LABEL for #{lot}"
    assert @@av.user_parameter('Lot', avlot, name: 'FabQualityCheck').valueflag, '(av) FabQualityCheck not set'
    assert_equal 'Complete', @@av.user_parameter('Lot', avlot, name: 'FabQualityCheck').value, '(av) FabQualityCheck wrong value'
    assert_equal 'LOTSHIPD.1', @@av.lot_info(avlot).op, 'wrong op in AsmView'
    #
    puts "\n\nPress Enter to continue with next test."
    gets
  end

  ## UNDER DEVELOPMENT
  ## Additional test - wish of Andreas Pohl --> test sheet has no test251 --> TODO: Verify if this old test prep. try can be used for this
  def test241_FinalStd_withav_J_F8_BOP  # same as test221_FinalStd_withav_J, except lot is received via BOP from F8
    # create test lot at src site and send it to backup (incl. FabGUI receiving, set lot script params)
    assert lot = @@svtest_src.new_lot(@@newlot_params_f8[:withav_j_f8]), 'error creating src fab lot'
    @@testlots_bop << lot
    # set lot script parameters at backup site (as FabGUI does on receive)
    # assert_equal 0, @@sv.schdl_change(lot, product: 'SHELBY-TKBTF.QA', route: 'UTRT-TKBTF.01')
    uparams = {
      'ShipStorageExpiration'=>'20300303.235900',
      'QualityGateApproval'=>'Approved,qauser',
      'CountryOfOrigin'=>'US',
      'SourceFabCode'=>'F8',  # TODO: AsmView HOLD when set, AsmTurnkey tries F1 AsnReceiver!
      'ERPItem'=>@@sv_src.user_parameter('Lot', lot, name: 'ERPItem').value
    }.merge(@@newlot_params_f8[:withav_j_f8][:user_parameters])
    assert_equal 0, @@sv.user_parameter_change('Lot', lot, uparams), 'error setting lot script params'
    # process lot through BTF with AsmView tracking
    assert @@tktest.start_btf(lot), 'error starting test lot, MQ issues?'
    assert @@tktest.process_btf(lot), 'error processing lot, setup error?'
    # lot is at (sv) LOTSHIPDESIG.01 or LOTSHIP-F8TK.01, (av) TKYSORT.1, set TestDB data and DieCountGood (defaults to 100 for all wafers)
    assert @@dbbtf.set_lot_wafer_bins(lot), 'error setting binning data'
    #
    puts "\n\nLot #{lot} is ready for shipment"
    gets
    # verify in sv and av
    $log.info 'verifying lot script parameters ShipComment (sv) and FabQualityCheck (av)'
    assert @@sv.user_parameter('Lot', lot, name: 'ShipComment').valueflag, '(sv) ShipComment not set'
    assert @@av.user_parameter('Lot', lot, name: 'FabQualityCheck').valueflag, '(av) FabQualityCheck not set'
  end

  ## TEST is Out-of-Scope (according to mail of Andreas Pohl on the 27th of May 2020)
  ## --> ImplicitSTB doesn't work anymore. Detected at TC setup in May 2020.
  ## Additional test - wish of Andreas Pohl for exception case (based on #test221 but w/o BtfTurnkey processing)
  def test281_FinalStd_EC_impSTB_J
    $setup_ok = false
    # create lot with dev sublottype
    assert lot = @@svtest.new_lot(@@newlot_params[:withav_j]), 'error creating test lot'
    @@testlots << lot
    @@sv.lot_mfgorder_change(lot, sublottype: 'EM')
    assert_equal 'EM', @@sv.lot_info(lot).sublottype, 'wrong sublottype'
    ## NONO - BtfTurnkey-Processing
    # lot processing needed for available operation history
    assert_equal 0, @@sv.lot_gatepass(lot) # GatePass of 1st PD: LOTSTART.01
    assert @@sv.claim_process_lot(lot) # PD processing of 2nd PD: INITPCL.01
    assert @@sv.claim_process_lot(lot) # PD processing of 3rd PD: NICPRECLNDISPO.01
    assert @@sv.claim_process_lot(lot) # PD processing of 4th PD: NICPRECLN.02
    assert_equal 0, @@sv.lot_opelocate(lot, nil, op: 'LOTSHIPDESIG.01')
    # lot is at (sv) LOTSHIPDESIG.01, (av) doesn't exist here at this time
    $setup_ok = true
    #
    puts "\n\nLot #{lot} is availble as DEV-lot"
    gets
    #
    $setup_ok = false
    # change SLT to "PO" via script - because "FabGUI->LotInfoChg" needs exact match data
    @@sv.lot_mfgorder_change(lot, sublottype: 'PO')
    assert_equal 'PO', @@sv.lot_info(lot).sublottype, 'wrong sublottype'
    # set TestDB data and DieCountGood
    assert sortdata = @@dbbtf.set_lot_wafer_bins(lot), 'error setting binning data'
    $setup_ok = true
    #
    puts "\n\nLot #{lot} is ready for shipment"
    gets
    # verify in sv
    $log.info 'verifying (sv) lot script parameter ShipComment'
    assert @@sv.user_parameter('Lot', lot, name: 'ShipComment').valueflag, '(sv) ShipComment not set'
    # verify in av
    $log.info 'verifying (av) lot script parameters FabQualityCheck and ERPSalesOrder, (av) PD and binning data'
    assert avlot = @@tktest.get_avlot(lot), "(sv) missing LOT_LABEL for #{lot}"
    assert @@av.user_parameter('Lot', avlot, name: 'FabQualityCheck').valueflag, '(av) FabQualityCheck not set'
    assert_equal 'Complete', @@av.user_parameter('Lot', avlot, name: 'FabQualityCheck').value, '(av) FabQualityCheck wrong value'
    assert @@av.user_parameter('Lot', avlot, name: 'ERPSalesOrder').valueflag, '(av) ERPSalesOrder not set' # request of Andreas Pohl
    assert_equal 'LOTSHIPD.1', @@av.lot_info(avlot).op, 'wrong op in AsmView'
    assert @@tktest.verify_diecount(avlot, sortdata), 'binning data mismatch in AsmView'
    # TODO:
    # Check for Oracle-ERP
    # SubProcessComplete = Complete (waiting time = 8 min)
    #
    puts "\n\nPress Enter to continue with next test."
    gets
  end

  # Subcon Shipment

  def test311_Subcon_withav_BUMP_R_F10 # BUMP layer route with AsmView tracking (SrcFab -> Fab1 -> OSAT)
    $setup_ok = false
    av_gid = 'LOT_LABEL'
    lot = "D0ZRT.00000" # lot ID gets from Arthur via hangout (08.06.2020, 12:45 PM)
    avlot = @@tktest.get_avlot(lot, av_gid: av_gid)
    assert @@av.lot_exists?(avlot), "(av) lot #{avlot.inspect} does not exist"
    # create lot with the BUMP product and process it through BTF
    assert @@svtest.restb_lot(lot, @@newlot_params[:withav_restb_bump10]), 'error creating lot'
    ## NONO  @@testlots << lot
    assert @@tktest.process_btf(lot, stages: @@stages_BUMP, av_gid: av_gid), 'error processing lot'
    # lot is at (sv) LOTSHIP-BUMP.01, (av) TKYOQAB.1 (after Shipment exectuion in FabGUI - it's on TKYTRANS.1)
    assert_equal 0, @@sv.lot_opelocate(lot, nil, op: 'LOTSHIP-BUMP.01'), 'error locating lot'
    # During 'process_btf' Lot.SP 'SourceFabCode' is changed to "F1" per jCab-job user X-JSTBWA
    assert_equal 0, @@sv.user_parameter_change('Lot', lot, {'SourceFabCode'=>'F10'})
    @@sv.lot_info(lot, wafers: true).wafers.each {|w|
      assert_equal 0, @@sv.user_parameter_change('Wafer', w.wafer, 'DieCountGood', 100, 1, datatype: 'INTEGER')
    }
    $setup_ok = true
    #
    puts "\n\nLot #{lot} is ready for subcon shipment"
    gets
    # verify in sv
    $log.info 'verifying sv lot script parameters ShipComment and ShipToFirm'
    assert @@sv.user_parameter('Lot', lot, name: 'ShipComment').valueflag, '(sv) script parameter ShipComment not set'
    subcon = @@newlot_params[:withav_restb_bump10][:user_parameters]['TurnkeySubconIDSort']
    assert_equal subcon, @@sv.user_parameter('Lot', lot, name: 'ShipToFirm').value, 'wrong script parameter'
    # verify in av
    $log.info 'verifying av lot script parameters Interface7b5Status, TurnkeySubconIDBump, TurnkeySubconIDSort, SubProcessComplete and MesCarrierId'
    uparams = @@av.user_parameter('Lot', avlot)
    # Value target from customer incorrect --> mail to customer but no feedback until now:
    # assert_equal 'ASN', uparams.find {|e| e.name == 'Interface7b5Status'}.value, '(av) Interface7b5Status wrong value'
    # query changed to parameter include string 'ASN':
    assert uparams.find {|e| e.name == 'Interface7b5Status'}.value.include?('ASN'), '(av) Interface7b5Status wrong value'
    assert_equal 'GG', uparams.find {|e| e.name == 'TurnkeySubconIDBump'}.value, '(av) TurnkeySubconIDBump wrong value'
    assert_equal 'GB', uparams.find {|e| e.name == 'TurnkeySubconIDSort'}.value, '(av) TurnkeySubconIDSort wrong value'
    assert_equal 'Complete', uparams.find {|e| e.name == 'SubProcessComplete'}.value, '(av) SubProcessComplete wrong value'
    # Value target from customer unclear --> mail to customer but no feedback until now:
    # assert_equal 'FOSBID', uparams.find {|e| e.name == 'MesCarrierId'}.value, '(av) MesCarrierId wrong value'
    # query changed to compare this lot.SP with (sv) carrierID:
    assert_equal @@sv.lot_info(lot).carrier, uparams.find {|e| e.name == 'MesCarrierId'}.value, '(av) MesCarrierId wrong value'
    #
    puts "\n\nPress Enter to continue with next test."
    gets
  end

  def Xtest311_Subcon_trans_bump_R_F8 # Subcon_trans_bump via AsmView tracking (SourceFabCode = F8)
    $setup_ok = false
    # create test lot at src site
    assert lot = @@svtest_src.new_lot(@@newlot_params_f8[:withav_r_f8_BUMP]), 'error creating src fab lot'
    @@testlots_bop << lot
    # create lot with the BUMP product and process it through BTF
    wids = @@sv_src.lot_info(lot, wafers: true).wafers.collect {|w| w.wafer}
    assert @@svtest.new_lot(@@newlot_params[:transbump].merge(lot: lot, waferids: wids)), 'error creating lot'
    @@testlots << lot
    av_gid = 'LOT_LABEL'
    avlot = @@tktest.get_avlot(lot, av_gid: av_gid)
    assert @@tktest.start_btf(lot, stages: @@tktest.stages['L'], av_gid: av_gid), 'error starting test lot'
    assert @@tktest.process_btf(lot, stages:  @@tktest.stages['L'], av_gid: av_gid), 'error processing lot'
    # TODO: fails 'Specified wafer WWJY5NJOI601 already exists' if SourceFabCode is not F1
    # TODO: test preparation
    $setup_ok = true
    #
    puts "\n\nPress Enter to continue with next test"
    gets

  end

  def test331_Subcon_noav_J_bump  # Fab1 -> OSAT
    # create lot, locate it to TKEYBUMPSHIPDISPO and set OQAGoodDie
    assert lot = @@svtest.new_lot(@@newlot_params[:subcon_j_bump]), 'error creating Fab lot'
    @@testlots << lot
    assert_equal 0, @@sv.lot_opelocate(lot, nil, op: 'TKEYBUMPSHIPDISPO.01')
    @@sv.lot_info(lot, wafers: true).wafers.each {|w|
      assert_equal 0, @@sv.user_parameter_change('Wafer', w.wafer, 'OQAGoodDie', 100, 1, datatype: 'INTEGER')
    }
    #
    puts "\n\nLot #{lot} is ready for subcon shipment"
    gets
    # verify in sv
    $log.info 'verifying lot script parameters ShipComment and ShipToFirm'
    assert @@sv.user_parameter('Lot', lot, name: 'ShipComment').valueflag, 'script parameter ShipComment not set'
    subcon = @@newlot_params[:subcon_j_bump][:user_parameters]['TurnkeySubconIDBump']
    assert_equal subcon, @@sv.user_parameter('Lot', lot, name: 'ShipToFirm').value, 'wrong script parameter'
    #
    # TODO: Check of lot creation in AsmView needs special user activity (Shipped) in ERP Oracle
    #       (After this action: Lot will be created at AsmView through ERP Oracle action)
    #
    puts "\n\nPress Enter to continue with next test."
    gets
  end

  def test341_Subcon_withav_J_sort  # Fab1 -> OSAT
    # create lot, locate it to (av) TKYOQAB.1, (sv) TKEYSORTSHIPDISPO.01 and set DieCountGood
    assert lot = @@svtest.new_lot(@@newlot_params[:subcon_j_sort]), 'error creating Fab lot'
    @@testlots << lot
    assert @@tktest.start_btf(lot), 'error starting test lot'
    assert avlot = @@tktest.get_avlot(lot), "(sv) missing LOT_LABEL for #{lot}"
    assert_equal 0, @@av.lot_opelocate(avlot, nil, op: 'TKYOQAB.1')  ## instead of op: TKYTRANS.1 due to new mapping with CR83227
    assert_equal 0, @@sv.lot_opelocate(lot, nil, op: 'TKEYSORTSHIPDISPO.01')
    @@sv.lot_info(lot, wafers: true).wafers.each {|w|
      assert_equal 0, @@sv.user_parameter_change('Wafer', w.wafer, 'DieCountGood', 100, 1, datatype: 'INTEGER')
    }
    #
    puts "\n\nLot #{lot} is ready for subcon shipment"
    gets
    # verify in sv
    $log.info 'verifying sv lot script parameters ShipComment and ShipToFirm'
    assert @@sv.user_parameter('Lot', lot, name: 'ShipComment').valueflag, 'script parameter ShipComment not set'
    subcon = @@newlot_params[:subcon_j_sort][:user_parameters]['TurnkeySubconIDSort']
    assert_equal subcon, @@sv.user_parameter('Lot', lot, name: 'ShipToFirm').value, 'wrong script parameter'
    # verify in av
    $log.info 'verifying av lot operation'
    assert_equal 'TKYTRANS.1', @@av.lot_info(avlot).op, 'wrong op in AsmView'
    #
    puts "\n\nPress Enter to continue with next test."
    gets
  end

  def test351_Subcon_noav_UU_fab  # Fab1 -> Fab7
    # create lot, locate it to LOTSHIP-FAB and set OQAGoodDie
    assert lot = @@svtest.new_lot(@@newlot_params[:noav_uu_fab]), 'error creating Fab lot'
    @@testlots << lot
    assert_equal 0, @@sv.lot_opelocate(lot, nil, op: 'LOTSHIP-FAB.01')
    @@sv.lot_info(lot, wafers: true).wafers.each {|w|
      assert_equal 0, @@sv.user_parameter_change('Wafer', w.wafer, 'OQAGoodDie', 100, 1, datatype: 'INTEGER')
    }
    #
    puts "\n\nLot #{lot} is ready for subcon shipment"
    gets
    # verify in sv
    $log.info 'verifying lot script parameters ShipComment and ShipToFirm'
    assert @@sv.user_parameter('Lot', lot, name: 'ShipComment').valueflag, 'script parameter ShipComment not set'
    subcon = @@newlot_params[:noav_uu_fab][:user_parameters]['TurnkeySubconIDBump']
    assert_equal subcon, @@sv.user_parameter('Lot', lot, name: 'ShipToFirm').value, 'wrong script parameter'
    #
    # TODO: Check of lot creation in AsmView needs special user activity (Shipped) in ERP Oracle
    #       (After this action: Lot will be created at AsmView through ERP Oracle action)
    #
    puts "\n\nPress Enter to continue with next test."
    gets
  end

  # Additional test for TransFab testing of (sv) StageID "3ML" (with UDATA 'RouteEndBBStage'= TRANFAB-M3)
  def test361_Subcon_noav_UU_tranfab  # Fab1 -> Fab7 per TransFab-Shipment
    # create lot, locate it to LOTSHIP-FAB and set OQAGoodDie
    assert lot = @@svtest.new_lot(@@newlot_params[:noav_uu_tranfab]), 'error creating Fab lot'
    @@testlots << lot
    assert_equal 0, @@sv.lot_opelocate(lot, nil, op: 'LOTSHIP-FAB.01')
    @@sv.lot_info(lot, wafers: true).wafers.each {|w|
      assert_equal 0, @@sv.user_parameter_change('Wafer', w.wafer, 'OQAGoodDie', 100, 1, datatype: 'INTEGER')
    }
    #
    puts "\n\nLot #{lot} is ready for subcon shipment"
    gets
    # verify in sv
    $log.info 'verifying lot script parameters ShipComment and ShipToFirm'
    assert @@sv.user_parameter('Lot', lot, name: 'ShipComment').valueflag, 'script parameter ShipComment not set'
    subcon = @@newlot_params[:noav_uu_tranfab][:user_parameters]['TurnkeySubconIDBump']
    assert_equal subcon, @@sv.user_parameter('Lot', lot, name: 'ShipToFirm').value, 'wrong script parameter'
    #
    puts "\n\nPress Enter to continue with next test."
    gets
  end

  # NonStandard Shipment no AsmView

  def test411_NonStd_noav_U_amd # NonStd_noav_processing with 'amd' as customer because 'mtk' has special fct.
    # create lots, locate it to PD NICETAPI.01 (any PD) and set OQAGoodDie
    assert lots = @@svtest.new_lots(2, @@newlot_params[:noav_u_amd]), 'error creating test lots'
    @@testlots += lots
    lots.each {|lot|
      assert_equal 0, @@sv.lot_opelocate(lot, nil, op: 'NICETAPI.01')
      @@sv.lot_info(lot, wafers: true).wafers.each {|w|
        assert_equal 0, @@sv.user_parameter_change('Wafer', w.wafer, 'OQAGoodDie', 100, 1, datatype: 'INTEGER')
      }
    }
    #
    puts "\n\nLots #{lots} are ready for NonStandard shipment"
    gets
    # verify in sv
    $log.info 'verifying lot script parameters ShipComment and (FabGUI mandatory fields) ShipToFirm + ShipToEngineer'
    lots.each {|lot|
      assert @@sv.user_parameter('Lot', lot, name: 'ShipComment').valueflag, 'script parameter ShipComment not set'
      assert @@sv.user_parameter('Lot', lot, name: 'ShipToFirm').valueflag, 'script parameter ShipToFirm not set'
      assert @@sv.user_parameter('Lot', lot, name: 'ShipToEngineer').valueflag, 'script parameter ShipToEngineer not set'
    }
    #
    puts "\n\nPress Enter to continue with next test."
    gets
  end

  def test412_NonStd_noav_U_nxptw # NonStd_noav_processing with 'nxptw' as special customer (fct.: B2B Interface)
    # create lot, locate it to PD NICETAPI.01 (any PD) and set OQAGoodDie
    assert lot = @@svtest.new_lot(@@newlot_params[:noav_u_nxptw]), 'error creating Fab lot'
    @@testlots << lot
    assert_equal 0, @@sv.lot_opelocate(lot, nil, op: 'NICETAPI.01')
    @@sv.lot_info(lot, wafers: true).wafers.each {|w|
      assert_equal 0, @@sv.user_parameter_change('Wafer', w.wafer, 'OQAGoodDie', 100, 1, datatype: 'INTEGER')
    }
    #
    puts "\n\nLot #{lot} is ready for NonStandard shipment"
    gets
    # verify in sv
    $log.info 'verifying lot script parameters ShipComment and (FabGUI mandatory fields) ShipToFirm + ShipToEngineer'
    assert @@sv.user_parameter('Lot', lot, name: 'ShipComment').valueflag, 'script parameter ShipComment not set'
    assert @@sv.user_parameter('Lot', lot, name: 'ShipToFirm').valueflag, 'script parameter ShipToFirm not set'
    assert @@sv.user_parameter('Lot', lot, name: 'ShipToEngineer').valueflag, 'script parameter ShipToEngineer not set'
    #
    puts "\n\nPress Enter to continue with next test."
    gets
  end


  # NonStandard Shipment with AsmView

  def test421_NonStd_withav_J_amd # Customer 'amd' because 'mtk' has special fct.
    $setup_ok = false
    # create lot and start it at BTF with AsmView tracking
    assert lot = @@svtest.new_lot(@@newlot_params[:withav_j_amd]), 'error creating test lot'
    @@testlots << lot
    assert @@tktest.start_btf(lot), 'error starting test lot'
    # process_bft isn't necessary because lot should be at any PD on route for NonStdShipment
    # lot is at (sv) TR-TO-C4B.01, (av) xxx, set TestDB data and DieCountGood (defaults to 100 for all wafers)
    assert @@dbbtf.set_lot_wafer_bins(lot), 'error setting binning data'
    $setup_ok = true
    #
    puts "\n\nLot #{lot} is ready for NonStandard shipment"
    gets
    # verify in sv
    $log.info 'verifying lot script parameters ShipComment and (FabGUI mandatory fields) ShipToFirm + ShipToEngineer'
    assert @@sv.user_parameter('Lot', lot, name: 'ShipComment').valueflag, 'script parameter ShipComment not set'
    assert @@sv.user_parameter('Lot', lot, name: 'ShipToFirm').valueflag, 'script parameter ShipToFirm not set'
    assert @@sv.user_parameter('Lot', lot, name: 'ShipToEngineer').valueflag, 'script parameter ShipToEngineer not set'
    #
    puts "\n\nPress Enter to continue with next test."
    gets
  end


  # NonStandard Shipment for Testwafer

  def test431_NonStd_noav_TW  # NonStd_noav_processing for Testwafer
    # create testwafer lot, locate it to PD UTPD001.01 (any PD)
    assert lot = @@svtest.new_controllot('Equipment Monitor', @@newlot_params[:noav_tw]), 'error creating Testwafer lot'
    @@testlots << lot
    assert_equal 0, @@sv.lot_opelocate(lot, nil, op: 'UTPD001.01')
    #
    puts "\n\nLot #{lot} is ready for NonStandard shipment"
    gets
    # verify in sv
    $log.info 'verifying lot script parameters ShipComment and (FabGUI mandatory fields) ShipToFirm + ShipToEngineer'
    assert @@sv.user_parameter('Lot', lot, name: 'ShipComment').valueflag, 'script parameter ShipComment not set'
    assert @@sv.user_parameter('Lot', lot, name: 'ShipToFirm').valueflag, 'script parameter ShipToFirm not set'
    assert @@sv.user_parameter('Lot', lot, name: 'ShipToEngineer').valueflag, 'script parameter ShipToEngineer not set'
    #
    puts "\n\nPress Enter to continue with next test."
    gets
  end


  # Sales Order Shipment

  def test511_SalesOrder_noav_U # SalesOrder_noav_processing with UT-* product
    # create lots, locate it to LOTSHIPDESIG and set OQAGoodDie, customer must match ERPSalesOrder
    assert lots = @@svtest.new_lots(3, @@newlot_params[:ordernoav]), 'error creating test lots'
    @@testlots += lots
    @@sv.lot_mfgorder_change(lots[1], customer: 'amd')
    assert_equal 'amd', @@sv.lot_info(lots[1]).customer, 'wrong customer'
    lots.each {|lot|
      assert_equal 0, @@sv.lot_opelocate(lot, nil, op: 'LOTSHIPDESIG.01')
      # wafer script parameter only for warning pop-up to'DieCount' check with confirm="yes"
      @@sv.lot_info(lot, wafers: true).wafers.each {|w|
        assert_equal 0, @@sv.user_parameter_change('Wafer', w.wafer, 'OQAGoodDie', 100, 1, datatype: 'INTEGER')
      }
    }
    #
    puts "\n\nLots #{lots} are ready for 'Sales Order Shipment'"
    gets
    # verify in sv
    $log.info 'verifying lot script parameters ERPSalesOrder and ShipComment'
    lots.each_with_index {|lot, idx|
      if idx == 1   # lot with changed customer must not be shipped
        refute @@sv.user_parameter('Lot', lot, name: 'ERPSalesOrder').valueflag, 'wrong parameter ERPSalesOrder'
      else          # regular lots, must be shipped
        assert @@sv.user_parameter('Lot', lot, name: 'ERPSalesOrder').valueflag, 'script parameter ERPSalesOrder not set'
        refute @@sv.user_parameter('Lot', lot, name: 'ShipComment').valueflag, 'script parameter ShipComment is set'
      end
    }
    #
    puts "\n\nPress Enter to continue."
    gets
  end


  # Lot cleanup for all

  def test999
    puts "\n\nTesting done. Press Enter to clean up all test lots!"
    gets
  end

end