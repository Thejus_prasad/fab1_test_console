=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: sfrieske, 2019-02-11
=end

require 'SiViewTestCase'


# Create lots to test FabGUI Shipping -> QualityGate
class FabGUI_SH_QualityGate < SiViewTestCase
  ## @@sv_defaults = {route: 'C02-TKEY-ACTIVE.01', product: 'SHELBY21BTF.QA', customer: 'qgt'}
  @@sv_defaults = {product: 'UT-TKBTF.01', route: 'UTRT-TKBTF.01', customer: 'qgt'} # or customer: 'mkt'
  @@tkparams_J = {'TurnkeyType'=>'J', 'TurnkeySubconIDBump'=>'GG', 'TurnkeySubconIDSort'=>'GG'}
  #
  @@newlot_params = {
    prod_atv: {product: 'UT-PRODUCT-UD-ATV.01', route: 'UTRT-TKBTF.01'},
    prod_sec: {product: 'UT-PRODUCT-UD-SEC.01', route: 'UTRT-TKBTF.01'},
    prod_hrp: {product: 'UT-PRODUCT-UD-HRP.01', route: 'UTRT-TKBTF.01'},
    prod_mpl: {product: 'UT-PRODUCT-UD-MPL.01', route: 'UTRT-TKBTF.01'},
    prod_wo: {product: 'UT-PRODUCT-UD-WO.01', route: 'UTRT-TKBTF.01'},
  }

  @@testlots = []


  def self.after_all_passed
    @@testlots.each {|lot| @@sv.delete_lot_family(lot)}
  end


  # OUT-DATED --> MSR1358038 is rolled back btw. FabP-v23/-v24 --> and till this day not implemented in any form again
  def test00_setup
    $setup_ok = false
    #
    assert lots = @@svtest.new_lots(6, user_parameters: {'ShipStorageExpiration'=>'20300303.235900'}), 'error creating test lots'
    @@testlots += lots
    lis = @@sv.lots_info(lots, wafers: true)
    #
    # lot0: TKType U, no wafer script params
    # lot1: TKType U, OQAGoodDie (good)
    lis[1].wafers.each {|w| assert_equal 0, @@sv.user_parameter_change('Wafer', w.wafer, 'OQAGoodDie', 100, 1, datatype: 'INTEGER')}
    # lot2: TKType U, DieCountGood(wrong)
    lis[2].wafers.each {|w| assert_equal 0, @@sv.user_parameter_change('Wafer', w.wafer, 'DieCountGood', 100, 1, datatype: 'INTEGER')}
    #
    # lot3: TKType J, no wafer script params
    assert_equal 0, @@sv.user_parameter_change('Lot', lots[3], @@tkparams_J)
    # lot4: TKType J, OQAGoodDie (wrong)
    lis[4].wafers.each {|w| assert_equal 0, @@sv.user_parameter_change('Wafer', w.wafer, 'OQAGoodDie', 100, 1, datatype: 'INTEGER')}
    assert_equal 0, @@sv.user_parameter_change('Lot', lots[4], @@tkparams_J)
    # lot5: TKType J, DieCountGood(good)
    lis[5].wafers.each {|w| assert_equal 0, @@sv.user_parameter_change('Wafer', w.wafer, 'DieCountGood', 100, 1, datatype: 'INTEGER')}
    assert_equal 0, @@sv.user_parameter_change('Lot', lots[5], @@tkparams_J)
    #
    lots.each {|lot| assert_equal 0, @@sv.lot_opelocate(lot, nil, op: 'FOUT-GATE.01')}
    #
    puts "\n\ntest lots:\n#{lots}"
    puts "0: U, nothing; 1: U, OQAGoodDie (good);  2: U, DieCountGood (wrong)"
    puts "3: J, nothing; 4: J, OQAGoodDie (wrong); 5: J, DieCountGood (good)"
    puts "\n-> Execute the tests and press Enter to continue."
    gets
    #
    # note: approved lots advance to next PD (e.g. SORTPPSHIP.01) 
    #       and lot script parameter QualityGateApproval is set to "Approved,sfrieske,Tue Jul 09 09:00:49 UTC 2019,QAMemo"
    #
    $setup_ok = true
  end

  #
  def test111_column_Restricted
    # column 'Restricted' based on product UDATAs
    assert alot = @@svtest.new_lot(@@newlot_params[:prod_atv]), 'error creating AutomotiveProduct test lot'
    @@testlots << alot
    assert slot = @@svtest.new_lot(@@newlot_params[:prod_sec]), 'error creating SecurityProduct test lot'
    @@testlots << slot
    assert hlot = @@svtest.new_lot(@@newlot_params[:prod_hrp]), 'error creating HighReliabilityProduct test lot'
    @@testlots << hlot
    assert mlot = @@svtest.new_lot(@@newlot_params[:prod_mpl]), 'error creating Multiple UDATA choices test lot'
    @@testlots << mlot
    assert wolot = @@svtest.new_lot(@@newlot_params[:prod_wo]), 'error creating Without UDATA choices test lot'
    @@testlots << wolot
    # using of @@testlots only possible if test00 is unused!!!
    @@testlots.each {|lot|
      assert_equal 0, @@sv.lot_opelocate(lot, nil, op: 'FOUT-GATE.01')
    }
    #
    puts "\n\nLots are available:"
    puts "\n  AutomotiveProduct: #{alot}\n  SecurityProduct: #{slot}\n  HighReliabilityProduct: #{hlot}"
    puts "\n  Multiple UDATA choices: #{mlot}\n  Without UDATA choices: #{wolot}"
    gets
    #
  end

end
