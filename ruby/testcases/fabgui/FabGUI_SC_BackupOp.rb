=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: sfrieske, 2018-10-10

Notes:
  cf. JCAP_BtfTurnkey#test35
=end

require 'SiViewTestCase'


# Create lots to test FabGUI SiView Control->Lot Start->Backup Operation
class FabGUI_SC_BackupOp < SiViewTestCase
  @@sv_defaults = {product: 'UT-PRODUCT-FAB8.01', route: 'UTRT101.01', carriers: '%'}
  @@psend = {backup_opNo: '1000.200', backup_bank: 'W-BACKUPOPER'}
  #
  @@env_other = 'f8stag'
  @@sv_defaults_other = {product: 'UT-PRODUCT-FAB1.01', route: 'UTRT101.01', carriers: '%'}
  @@psend_other = {backup_opNo: '1000.200', backup_bank: 'O-BACKUPOPER'}
  @@preturn_other = {return_bank: 'O-BACKUPOPER'}
  #
  @@tkparams = {'TurnkeyType'=>'J', 'TurnkeySubconIDBump'=>'GG', 'TurnkeySubconIDSort'=>'GG'}
  @@scriptparams = {'CountryOfOrigin'=>'US', 'ERPItem'=>'ENGINEERING-U00', 'SourceFabCode'=>'F8'}

  @@testlots_bop = []
  @@testlots_bop_other = []


  def self.after_all_passed
    @@testlots_bop.each {|lot| @@sv.backup_delete_lot_family(lot, @@sv_other, @@psend)}
    @@testlots_bop_other.each {|lot| @@sv_other.backup_delete_lot_family(lot, @@sv, @@psend_other)}
  end


  def test00_setup
    $setup_ok = false
    #
    assert @@svtest_other = SiView::Test.new(@@env_other, @@sv_defaults_other)
    @@sv_other = @@svtest_other.sv
    #
    $setup_ok = true
  end

  def test11
    # lots_other: create 2 test lots at other site and send them (ready for send receive)
    assert lots_other = @@svtest_other.new_lots(2, user_parameters: @@tkparams), "error creating test lots in #{@@env_other}"
    @@testlots_bop_other += lots_other
    lots_other.each {|lot|
      assert @@sv_other.prepare_for_backup(lot, @@psend_other)
      assert_equal 0, @@sv_other.backupop_lot_send(lot, @@sv), 'error sending lot'
    }
    #
    # lot1: create test lot here, send/receive and return it (ready for return receive)
    assert lot1 = @@svtest.new_lot, "error creating test lot in #{$env}"
    @@testlots_bop << lot1
    assert @@sv.backup_prepare_send_receive(lot1, @@sv_other, @@psend), 'error sending and receiving lot'
    assert_equal 0, @@sv_other.user_parameter_change('Lot', lot1, @@tkparams), 'error setting lot script params'
    assert @@sv_other.prepare_for_return(lot1, @@preturn_other)
    assert_equal 0, @@sv_other.backupop_lot_return(lot1, @@sv), 'error returning lot'
    #
    lots = lots_other + [lot1]
    assert ecs = @@svtest.get_empty_carrier(carrier: 'F%', n: 3)
    puts "\n\nLots #{lots} are ready for receive, empty carriers: #{ecs}"
    puts "Press ENTER to start lot script parameter comparison!"
    gets
    #
    # script parameter verification
    lots.each {|lot|
      @@scriptparams.merge(@@tkparams).each_pair {|name, value|
        $log.info "verifying #{lot}: #{name.inspect}"
        # TODO: it is wrong to have US/F8 set for the ITDC lot !
        #       --> related to config property STB.BackupOperation.ScriptParameter.Defaults and JCAP remote service
        #       --> see Jira ticket FABPORTAL-2324 - comments of Prithviraj Patil from Dec 2019
        assert_equal value, @@sv.user_parameter('Lot', lot, name: name).value, "#{lot}: wrong script parameter #{name}"
      }
    }
    $log.info "verified #{@@scriptparams.merge(@@tkparams).keys}"
    puts "\n\nPress ENTER to start lot clean up!"
    gets
  end

end
