=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: aschmid3, 2019-11-05
=end

require 'SiViewTestCase'


# Create data to test MFC --> Task Categories of 'All Inhibits' --> MTR equipment/recipe tasks with 'Related lot'
class MFC_Inhibits_MTR < SiViewTestCase
  ### Data for Inhibit
  # Reason codes dependent on 'Task Categories'
  @@reason1 = 'X-S1'  # possible reason code for SPC-I and SPC-S in connection with memo
  @@reason2 = 'X-S2'  # possible reason code for SPC-I and SPC-S in connection with memo
  @@reason3 = 'XFDC'  # reason code for FDC
  @@reason4 = 'X-ER'  # possible reason code for OtherXInhibit
  # possible Metrology tools dependent on equipment recipe
  @@tools = ['THK117', 'THK513']
  @@recipe = 'D-P-THKTWI1-PROD+BULK+.PADNITM50.01'
  ### Data for 'Inhbibit Related Lot'
  # possible lots for test using
  # note: different variants of lot data - navigate per pointer to position of item
  @@lots = ['UXYU13357.000', 'UXYW36015.000',   # product: SHELBY31AM.B5 (PG: SHELBY2-2F), product: SHELBY31AK.B1 (PG: SHELBY3-1F)
            'UXYU45032.000']                    # product: ZA10699-A1FADU.AC01 (PG: ZA10699-A1F)
  @@routes = ['C02-1550.01', 
              '028LPS-00F04-ZA10699.0001']
  @@openo = '1100.1800'
  @@pd = 'PADN-NITM-0088.01'
  
  @@testinhibits = []
  
  
  def self.after_all_passed
    $log.info "canceling #{@@testinhibits.size} inhibits"
    @@testinhibits.each {|inh| @@sv.inhibit_cancel(nil, nil, inhibits: [inh], silent: true)}
  end
  
  
  ### Create inhibit tasks with 'Inhibit Related Lot' entry/ies
  # task entry/ies with valid/existing 'SpecBook' ProductGroup  
  def test11_inh_one_lot
    # SPCInhibit: INLINE
    assert inh = @@sv.inhibit_entity(:eqp_recipe, [@@tools[0], @@recipe], 
      reason: @@reason1, memo: 'MSR1494357 - MTR INH w/ single lot - LDS->INLINE', sublottypes: 'EO',
      reason_details: {lot: @@lots[0], route: @@routes[0], pd: @@pd, opNo: @@openo}
    )
    @@testinhibits << inh
    # SPCInhibit: SETUP
    assert inh = @@sv.inhibit_entity(:eqp_recipe, [@@tools[0], @@recipe], 
      reason: @@reason2, memo: 'MSR1494357 - MTR INH w/ single lot - LDS->SETUP', sublottypes: 'EO',
      reason_details: {lot: @@lots[0], route: @@routes[0], pd: @@pd, opNo: @@openo}
    )
    @@testinhibits << inh
    # FDCInhibit
    assert inh = @@sv.inhibit_entity(:eqp_recipe, [@@tools[0], @@recipe], 
      reason: @@reason3, memo: 'MSR1494357 - MTR INH w/ single lot - FDC', sublottypes: 'EO',
      reason_details: {lot: @@lots[0], route: @@routes[0], pd: @@pd, opNo: @@openo}
    )
    @@testinhibits << inh
    # OtherXInhibit
    assert inh = @@sv.inhibit_entity(:eqp_recipe, [@@tools[0], @@recipe], 
      reason: @@reason4, memo: 'MSR1494357 - MTR INH w/ single lot - OtherXInhibit', sublottypes: 'EO',
      reason_details: {lot: @@lots[0], route: @@routes[0], pd: @@pd, opNo: @@openo}
    )
    @@testinhibits << inh
    #
    puts "\n-> Execute the tests at MFC and press Enter to continue."
    gets
  end
  
  # task entry/ies with invalid/non-existing 'SpecBook' ProductGroup
  def test12_inh_one_lot_unvalid  
    # as example used 'SPCInhibit: INLINE'
    assert inh = @@sv.inhibit_entity(:eqp_recipe, [@@tools[0], @@recipe], 
      reason: @@reason1, memo: 'MSR1494357 - MTR INH with invalid SpecBook PG - LDS->INLINE', sublottypes: 'EO',
      reason_details: {lot: @@lots[1], route: @@routes[0], pd: @@pd, opNo: @@openo}
    )
    @@testinhibits << inh
    #
    puts "\n-> Execute the tests at MFC and press Enter to continue."
    gets
  end
  
  def test21_inh_two_lots
    # SPCInhibit: INLINE
    assert inh = @@sv.inhibit_entity(:eqp_recipe, [@@tools[1], @@recipe], 
      reason: @@reason1, memo: 'MSR1494357 - MTR INH w/ multiple lots - LDS->INLINE', sublottypes: 'ES',
      reason_details: [{lot: @@lots[0], route: @@routes[0], pd: @@pd, opNo: @@openo}, 
                       {lot: @@lots[2], route: @@routes[1], pd: @@pd, opNo: @@openo}]
    )
    @@testinhibits << inh
    # SPCInhibit: SETUP
    assert inh = @@sv.inhibit_entity(:eqp_recipe, [@@tools[1], @@recipe], 
      reason: @@reason2, memo: 'MSR1494357 - MTR INH w/ multiple lots - LDS->SETUP', sublottypes: 'ES',
      reason_details: [{lot: @@lots[0], route: @@routes[0], pd: @@pd, opNo: @@openo}, 
                       {lot: @@lots[2], route: @@routes[1], pd: @@pd, opNo: @@openo}]
    )
    @@testinhibits << inh
    # FDCInhibit
    assert inh = @@sv.inhibit_entity(:eqp_recipe, [@@tools[1], @@recipe], 
      reason: @@reason3, memo: 'MSR1494357 - MTR INH w/ multiple lots - FDC', sublottypes: 'ES',
      reason_details: [{lot: @@lots[0], route: @@routes[0], pd: @@pd, opNo: @@openo}, 
                       {lot: @@lots[2], route: @@routes[1], pd: @@pd, opNo: @@openo}]
    )
    @@testinhibits << inh
    # OtherXInhibit                                    
    assert inh = @@sv.inhibit_entity(:eqp_recipe, [@@tools[1], @@recipe], 
      reason: @@reason4, memo: 'MSR1494357 - MTR INH w/ multiple lots - OtherXInhibit', sublottypes: 'ES',
      reason_details: [{lot: @@lots[0], route: @@routes[0], pd: @@pd, opNo: @@openo}, 
                       {lot: @@lots[2], route: @@routes[1], pd: @@pd, opNo: @@openo}]
    )
    @@testinhibits << inh
    #
    puts "\n-> Execute the tests at MFC."
    puts "\n-> Press Enter to continue with cleanup of these created inhibits."
    gets
  end

end