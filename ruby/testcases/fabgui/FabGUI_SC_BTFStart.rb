=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: sfrieske, 2018-10-10

History:
  2022-01-04 sfrieske, speed up candidate product search 
=end

require 'jcap/erpmdm'
require 'SiViewTestCase'


# Create data to test FabGUI SiView Control Lotstart->BTF-Start
class FabGUI_SC_BTFStart < SiViewTestCase
  @@products = {
    regular: 'UT-PRODUCT000.01', atv: 'UT-PRODUCT000.ATV', sec: 'UT-PRODUCT000.SEC',  # Fab layer
    bump: 'UT-TKBTF-BUMP-QT.01'   # BUMP -> SORT with QT, no BUMP ERPitem required
  }
  @@endbank = 'OX-ENDFG'          # configured in FabGUI for search candidates
  @@tktypes = %w(J L U)           # configured in FabGUI for start product candidates
  ### for BTF Startsplan
  ##@@stbparams_plan = {customer: 'qgt', product: 'LEPTON21U.AA00', erpitem: 'LEPTON21U-UAA00'}
  @@bump_qtstart = '98601.1100'
  @@bump_startbank = 'G-BSTB'

  @@testlots = []


  def self.after_all_passed
    @@testlots.each {|lot| @@sv.delete_lot_family(lot)}
  end


  def test00_setup
    $setup_ok = false
    #
    @@mdm = ERPMDM.new($env)
    @@products_cand = {}
    @@products.each_pair {|k, srcproduct|
      @@products_cand[k] = @@sm.object_ids(:product, :all, search: {mfglayer: %w(BUMP SORT), srcproducts: srcproduct}).collect {|e| e.identifier}
    }
    ok = true
    @@products_cand.each_pair {|k, products|
      ($log.warn "no BSM start candidate product for #{k.inspect}"; ok = false) if products.empty?
    }
    assert ok, "missing SM products for BSM start, #{@@products_cand}"
    # filter products with ERPItems
    @@products_bsm = {}
    @@products_cand.each_pair {|k, products|
      @@products_bsm[k] = []
      products.each {|product|
        @@products_bsm[k] += @@mdm.mdm_entries(tktype: @@tktypes, product: product).collect {|e| e.product}.uniq.sort
      }
    }
    puts "\n\nBSM candidate products with ERPItem:\n#{@@products_bsm.pretty_inspect}"
    #
    $setup_ok = true
  end

  def test11_bsmstart_sec_atv
    # create fab layer lots, complete and move them to the configured end bank
    assert lotsr = @@svtest.new_lots(4, product: @@products[:regular], bankin: true), 'error creating regular testlots'
    @@testlots += lotsr
    assert lotsa = @@svtest.new_lots(2, product: @@products[:atv], bankin: true), 'error creating ATV testlots'
    @@testlots += lotsa
    assert lotss = @@svtest.new_lots(2, product: @@products[:sec], bankin: true), 'error creating SEC testlots'
    @@testlots += lotss
    @@testlots.each {|lot| assert_equal 0, @@sv.lot_bank_move(lot, @@endbank)}
    #
    puts "\n\n--> BTF Start test data are prepared"
    puts "  regular lots   : #{lotsr}"
    puts "  automotive lots: #{lotsa}"
    puts "  security lots  : #{lotss}"
    puts "\n-> Execute the tests and press Enter to continue."
    gets
  end

  def test12_bsmstart_qt
    # create Fab layer lots
    assert lotsb = @@svtest.new_lots(4, product: @@products[:regular], bankin: true), 'error creating regular testlots'
    @@testlots += lotsb
    # create BUMP layer lots, acquire Q-Time, complete and move them to the configured end bank
    lotsb.each_with_index {|lot, idx|
      assert_equal 0, @@sv.lot_bank_move(lot, @@bump_startbank)
      assert @@svtest.new_lot(bysrclot: lot, product: @@products[:bump], layer: 'BUMP'), 'error creating BUMP testlot'
      if idx != 3     # skip lot3 (no QT at all)
        assert_equal 0, @@sv.lot_opelocate(lot, @@bump_qtstart)
        assert_equal 0, @@sv.lot_gatepass(lot, mandatory: true)
        if idx == 2   # expire lot2's QT
          assert_equal 0, @@sv.lot_qtime_update(lot, 'Update', expire: true)
          sleep 5
        end
      end
      assert_equal 0, @@sv.lot_opelocate(lot, :last)
      assert_equal 0, @@sv.lot_bank_move(lot, @@endbank)
    }
    #
    puts "\n\nBTF BUMP->SORT lots\n  with QT: #{lotsb[0]}, #{lotsb[1]}\n  expired QT: #{lotsb[2]}\n  no QT: #{lotsb[3]}"
    puts "\n-> Execute the tests and press Enter to continue."
    gets
  end

  def test21_startsplan
    ### # create lots with customer, product and ERPItem according to BTF starts plan, complete and move to configured end bank
    ### assert lots = @@svtest.new_lots(4, @@stbparams_plan.merge(bankin: true))
    ###
    # create srclots with customer, product and ERPItem according to BTF starts plan, complete and move to configured end bank
    # note: 1) lot's product must be a srcproduct for target, 2) BTFStartsPlan must be matching
    assert lots = @@svtest.new_lots(4, bankin: true)
    @@testlots += lots
    lots.each {|lot| assert_equal 0, @@sv.lot_bank_move(lot, @@endbank)}
    #
    puts "\n\n--> BTF Startsplan test lots: #{lots}"
    puts "\n-> Execute the tests and press Enter to continue."
    gets
  end

end
