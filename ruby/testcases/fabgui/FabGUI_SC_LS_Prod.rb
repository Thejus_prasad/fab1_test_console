=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author:   aschmid3, 2020-02-28

Version: 26.0.6 

History:
  2020-02-28 aschmid3, init version
  2020-06 aschmid3, additional tests with new introduced setup.FC spec 'C14-BOM-SOURCE-PRODUCTS'

Notes:
  2020-02-28, 1) needs spec "C14-Source-Product-Assignment" --> Attention: with FabP-v29 new spec!!!
              2) used product and source/vendor needs a connection inside this spec
  2020-06, to point 1) conversion of responsible C14-spec with FabP-v29
                        (old: 'C14-Source-Product-Assignment' to new: 'C14-BOM-SOURCE-PRODUCTS')
           to point 2) no changes

=end

require 'SiViewTestCase'


# Create lots to test FabGUI - SiView Control - Lot Start - Production
class FabGUI_SC_LS_Prod < SiViewTestCase

  # settings for 'Lot Comment' - necessary for STB in FabGUI otherwise LotHold X-JSTBWA (jCAP: STBMfgWIPAttributes)
  erpitem = "ENGINEERING-U00"
  dpml = 1.2
  fsd = Time.now.strftime('%F')
  comment = "ERPITEM=#{erpitem},DPML=#{dpml},PLANNED_FSD=#{fsd}"
  # lot settings
  # standard customer = "gf" 
  @@newlotrel_params = {
    ## only for lot scheduling
    # tests with check of mandatory lot script parameter display in FabGUI
    # check based on SLT definition at separate csv-file 'route-sublottype-matrix' of FabGUI-config
    # --> column 'LOTSTART-PROD': "1" = No (for mandatory Lot.SP); "0" = Yes (for mandatory Lot.SP)
    por_sched: {product: 'SHELBY31AK.B1', route: 'C02-1550.01', sublottype: "PO", 
                comment: comment
    },
    nonp_sched: {product: 'SHELBY31AK.B1', route: 'C02-1550.01', sublottype: "EJ", # chg from "EB" to "EJ" due to new C14-spec
                comment: comment
    },
    #
    # 2020-06: additional tests with new introduced setup.FC spec 'C14-BOM-SOURCE-PRODUCTS'
    # Using of POR and non-POR according to StartType (table: 5.1.1. BOM Source Product Table) and table '5.3.1. Allowed SubLotTypes per StartType'
    tech_wsm_por: {product: 'UT-LSPROD-03.01', route: 'UTRT-LSPROD-AA.01', sublottype: "PO", customer: "qgt", 
                  comment: comment
    },
    tech_wsm_por_nonp: {product: 'UT-LSPROD-00.01', route: 'UTRT-LSPROD-AA.01', sublottype: "EJ", 
                          comment: comment
    },
    tech_prodexc_por: {product: 'UT-LSPROD-01.01', route: 'UTRT-LSPROD-AA.01', sublottype: "PO", 
                      comment: comment
    },
    tech_custexc_por: {product: 'UT-LSPROD-02.01', route: 'UTRT-LSPROD-AA.01', sublottype: "PO", customer: "amd", 
                      comment: comment
    },
    cc_wsmpor_sltnonp: {product: 'UT-LSPROD-00.01', route: 'UTRT-LSPROD-AA.01', sublottype: "RD", 
                      comment: comment
    },
    cc_wsmnonp_sltpor: {product: 'UT-LSPROD-00.01', route: 'UTRT-LSPROD-AA.01', sublottype: "EO", 
                      comment: comment
    },
    lm_check: {product: 'UT-LSPROD-00.01', route: 'UTRT-LSPROD-AA.01', sublottype: "PR", 
                      comment: comment
    },
    scr_other_p1: {product: 'UT-LSPROD-03.01', route: 'UTRT-LSPROD-AA.01', sublottype: "PR", customer: "qgt", 
                  comment: comment
    },
    scr_other_p2: {product: 'SHELBY31AK.B1', route: 'C02-1550.01', sublottype: "PR", 
                  comment: comment
    },
    src_unused_sched: {product: 'UT-LSPROD-04.01', route: 'UTRT-LSPROD-AA.01', sublottype: "PR", 
                      comment: comment
    },
    src_empty_sched: {product: 'UT-LSPROD-05.01', route: 'UTRT-LSPROD-AA.01', sublottype: "PR", 
                      comment: comment
    },
  }
  @@newlot_params = {
    rma_plot: {product: 'SHELBY31AK.B1', route: 'C02-1550.01', sublottype: "PO", bankin: true, user_parameters: {}},
  }
  # vendor-/sourcelot settings
  @@startbank = 'ON-RAW'
  @@srcproduct1 = '2ERWF-12537.ALL'
  @@srcproduct2 = '2ERWF-11532.ALL'
  @@srcproduct3 = '2ERWF-11002.ALL'
  @@srcproduct4 = '2ERWF-13789.ALL'
  @@srcproduct5 = 'OTHER.ALL'   # special source - can use for all - independent of C14-spec entries
  @@count = 25
  # Not necessary anymore because new introduced LaserMark definition does it now.
  # @@vendlot_params = { 
  #   happy: {vendorlot: 'SILTRONIC - 999999/QHRM2SSD1 - 31007610'}   
  #   # SiVieMM - Lot Info Menu - tab: 'Detail' - Vendor Lot ID --> will verify by FabGUI - SC - LS - Production - STB Details - Source Lot: column 'Vendor'
  # }
  # FabGUI - SC - LS - Production - STB Details - Source Lot: column 'Vendor' will be filled-out by LaserMark definition

  @@testlots = []


  def self.after_all_passed
    @@testlots.each {|lot| @@sv.delete_lot_family(lot)}
  end


  ## Normal Lot STB for check of mandatory lot script parameter display in FabGUI
  # Mandatory Lot.SP check based on SLT definition at separate csv-file 'route-sublottype-matrix' of FabGUI-config
  # --> column 'LOTSTART-PROD': "1" = No (for mandatory Lot.SP); "0" = Yes (for mandatory Lot.SP)
  
  def test111_POR_Lot   # POR - Plan on Record
    # schedule lot
    assert lot = @@sv.new_lot_release(@@newlotrel_params[:por_sched]), 'error scheduling test lot'
    @@testlots << lot
    #
    # search empty carrier
    assert ec = @@svtest.get_empty_carrier, 'error getting empty carrier'
    assert_equal 0, @@sv.carrier_counter_reset(ec)
    #
    # create vendorlot + sourcelot
    # assert vlot = @@sv.vendorlot_receive(@@startbank, @@srcproduct1, @@count, @@vendlot_params[:happy]), 'error creating vendorlot'
    # --> @@vendlot_params[:happy] not necessary anymore because new introduced LaserMark definition does it now.
    # --> FabGUI - SC - LS - Production - STB Details - Source Lot: column 'Vendor' will be filled-out by LaserMark definition
    assert vlot = @@sv.vendorlot_receive(@@startbank, @@srcproduct1, @@count), 'error creating vendorlot'
    @@testlots << vlot
    assert srclot = @@sv.vendorlot_prepare(vlot, ec, lasermark: 'PL'), 'error creating source lot'
    @@testlots << srclot
    #
    puts "\n\nscheduled lot: #{lot}, prepared vendor lot: #{srclot}"
    puts 'Press ENTER to continue when done with the tests.'
    gets
  end

  def test112_nonPOR_Lot    # non-POR - no Plan on Record
    # schedule lot
    assert lot = @@sv.new_lot_release(@@newlotrel_params[:nonp_sched]), 'error scheduling test lot'
    @@testlots << lot
    #
    # search empty carrier
    assert ec = @@svtest.get_empty_carrier, 'error getting empty carrier'
    assert_equal 0, @@sv.carrier_counter_reset(ec)
    #
    # create vendorlot + sourcelot
    # assert vlot = @@sv.vendorlot_receive(@@startbank, @@srcproduct1, @@count ,@@vendlot_params[:happy]), 'error creating vendorlot'
    # --> @@vendlot_params[:happy] not necessary anymore because new introduced LaserMark definition does it now.
    # --> FabGUI - SC - LS - Production - STB Details - Source Lot: column 'Vendor' will be filled-out by LaserMark definition
    assert vlot = @@sv.vendorlot_receive(@@startbank, @@srcproduct1, @@count), 'error creating vendorlot'
    @@testlots << vlot
    assert srclot = @@sv.vendorlot_prepare(vlot, ec, lasermark: 'MM'), 'error creating source lot'
    @@testlots << srclot
    #
    puts "\n\nscheduled lot: #{lot}, prepared vendor lot: #{srclot}"
    puts 'Press ENTER to continue when done with the tests.'
    gets
  end

  # Lot STB for RMA - Returned Material from Customer

  def test211_RMA_POR_Lot
    # create wafer IDs
    widprefix = "#{unique_string}PL"  # laser mark PL
    waferids = (1..25).collect {|i| "#{widprefix}%02d" % i}
    #
    # create lot, ship and delete
    assert lot = @@svtest.new_lot(@@newlot_params[:rma_plot].merge(waferids: waferids)), 'error creating Fab lot'
    @@testlots << lot
    assert_equal 0, @@sv.lot_ship(lot, 'OX-ENDFG', memo: "QA test - RMA POR lot")
    assert_equal 0, @@sv.delete_lot_family(lot, delete: true)  # delete: true --> final lot deletion
    refute @@sv.lot_exists?(lot), 'lot still exists in SiView'
    #
    # schedule lot with previously used lot ID
    assert lot2 = @@sv.new_lot_release(@@newlotrel_params[:por_sched].merge(lot: lot)), 'error scheduling with previous used lot ID for RMA'
    assert_equal lot, lot2, 'wrong lot ID'
    assert ec = @@svtest.get_empty_carrier, 'no empty carrier'
    assert_equal 0, @@sv.carrier_counter_reset(ec)  # require for FabGUI
    # assert vlot = @@sv.vendorlot_receive(@@startbank, @@srcproduct1, 25 ,@@vendlot_params[:happy]), 'error creating vendorlot'
    # --> @@vendlot_params[:happy] not necessary anymore because new introduced LaserMark definition does it now.
    # --> FabGUI - SC - LS - Production - STB Details - Source Lot: column 'Vendor' will be filled-out by LaserMark definition
    assert vlot = @@sv.vendorlot_receive(@@startbank, @@srcproduct1, 25), 'error receiving vendorlot'
    @@testlots << vlot
    assert srclot = @@sv.vendorlot_prepare(vlot, ec, waferids: waferids.reverse), 'error creating source lot with wafer IDs of previous used lot ID'
    @@testlots << srclot
    #
    puts "\n\nscheduled lot: #{lot}, prepared vendor lot: #{srclot} (wafers in reversed order)"
    puts 'Press ENTER to continue when done with the tests.'
    gets
  end

  # 2 lots as in test211
  def test212_2RMA_POR_Lots
    lots = []
    srclots = []
    2.times {|i|
      # create wafer IDs
      widprefix = "#{unique_string}PL"  # laser mark PL
      waferids = (1..25).collect {|i| "#{widprefix}%02d" % i}
      #
      # create lot, ship and delete
      assert lot = @@svtest.new_lot(@@newlot_params[:rma_plot].merge(waferids: waferids)), 'error creating Fab lot'
      @@testlots << lot
      lots << lot
      assert_equal 0, @@sv.lot_ship(lot, 'OX-ENDFG', memo: "QA test - RMA POR lot")
      assert_equal 0, @@sv.delete_lot_family(lot, delete: true)
      refute @@sv.lot_exists?(lot), 'lot still exists in SiView'
      #
      # schedule lot with previously used lot ID
      assert lot2 = @@sv.new_lot_release(@@newlotrel_params[:por_sched].merge(lot: lot)), 'error scheduling with previous used lot ID for RMA'
      assert_equal lot, lot2, 'wrong lot ID'
      assert ec = @@svtest.get_empty_carrier, 'no empty carrier'
      assert_equal 0, @@sv.carrier_counter_reset(ec)  # required for FabGUI
      assert vlot = @@sv.vendorlot_receive(@@startbank, @@srcproduct1, 25), 'error receiving vendorlot'
      @@testlots << vlot
      assert srclot = @@sv.vendorlot_prepare(vlot, ec, waferids: waferids.reverse), 'error creating srclot'
      @@testlots << srclot
      srclots << srclot
    }
    #
    puts "\n\nscheduled lots: #{lots}, prepared vendor lot: #{srclots} (wafers in reversed order)"
    puts 'Press ENTER to continue when done with the tests.'
    gets
  end

  ## 2020-06: additional tests with newly introduced setup.FC spec 'C14-BOM-SOURCE-PRODUCTS'
  # Technology entry based

  # Tech + diff. WaferStartMaterial (Sources) for only StartType = POR (used SLT must be also defined only for POR)
  def test311_Tech_WSM_POR   # POR - Plan on Record
    # schedule lot
    assert lot1 = @@sv.new_lot_release(@@newlotrel_params[:tech_wsm_por]), 'error scheduling 1st lot'
    @@testlots << lot1
    assert lot2 = @@sv.new_lot_release(@@newlotrel_params[:tech_wsm_por]), 'error scheduling 2nd lot'
    @@testlots << lot2
    #
    # search empty carriers
    assert ecs = @@svtest.get_empty_carrier(n: 2), 'error getting empty carriers'
    ecs.each {|ec| assert_equal 0, @@sv.carrier_counter_reset(ec)}
    #
    # create vendorlots + sourcelots
    assert vlot1 = @@sv.vendorlot_receive(@@startbank, @@srcproduct1, @@count), 'error creating 1st vendorlot'
    @@testlots << vlot1
    assert vlot2 = @@sv.vendorlot_receive(@@startbank, @@srcproduct2, @@count), 'error creating 2nd vendorlot'
    @@testlots << vlot2
    assert srclot1 = @@sv.vendorlot_prepare(vlot1, ecs[0], lasermark: 'WF'), 'error creating 1st source lot'
    @@testlots << srclot1
    assert srclot2 = @@sv.vendorlot_prepare(vlot2, ecs[1], lasermark: 'MM'), 'error creating 2nd source lot'
    @@testlots << srclot2
    #
    puts "\n\nscheduled lots: #{lot1}, #{lot2}, prepared vendor lots: #{srclot1}, #{srclot2}"
    puts 'Press ENTER to continue when done with the tests.'
    gets
  end

  # Tech + diff. WaferStartMaterial (Sources) for StartType = POR & non-POR (used SLT is defined for POR & non-POR)
  def test321_Tech_WSM_POR_nonPOR   # POR - Plan on Record & non-POR - no Plan on Record
    # schedule lot
    assert lot = @@sv.new_lot_release(@@newlotrel_params[:tech_wsm_por_nonp]), 'error scheduling test lot'
    @@testlots << lot
    #
    # search empty carriers
    assert ecs = @@svtest.get_empty_carrier(n: 2), 'error getting empty carriers'
    ecs.each {|ec| assert_equal 0, @@sv.carrier_counter_reset(ec)}
    #
    # create vendorlots + sourcelots with same LaserMark
    assert vlot1 = @@sv.vendorlot_receive(@@startbank, @@srcproduct3, @@count), 'error creating 1st vendorlot'
    @@testlots << vlot1
    assert vlot2 = @@sv.vendorlot_receive(@@startbank, @@srcproduct4, @@count), 'error creating 2nd vendorlot'
    @@testlots << vlot2
    assert srclot1 = @@sv.vendorlot_prepare(vlot1, ecs[0], lasermark: 'PL'), 'error creating 1st source lot'
    @@testlots << srclot1
    assert srclot2 = @@sv.vendorlot_prepare(vlot2, ecs[1], lasermark: 'PL'), 'error creating 2nd source lot'
    @@testlots << srclot2
    #
    puts "\n\nscheduled lot: #{lot}, prepared vendor lot: #{srclot1}, #{srclot2}"
    gets
  end

  # Tech + Product Exception for only StartType = POR (used SLT must be also defined only for POR)
  def test331_Tech_ProdExc_POR   # POR - Plan on Record
    # schedule lot
    assert lot = @@sv.new_lot_release(@@newlotrel_params[:tech_prodexc_por]), 'error scheduling test lot'
    @@testlots << lot
    #
    # search empty carriers
    assert ecs = @@svtest.get_empty_carrier(n: 2), 'error getting empty carriers'
    ecs.each {|ec| assert_equal 0, @@sv.carrier_counter_reset(ec)}
    #
    # create vendorlots + sourcelots with one valid and one invalid LaserMark
    assert vlot1 = @@sv.vendorlot_receive(@@startbank, @@srcproduct2, @@count), 'error creating 1st vendorlot'
    @@testlots << vlot1
    assert vlot2 = @@sv.vendorlot_receive(@@startbank, @@srcproduct2, @@count), 'error creating 2nd vendorlot'
    @@testlots << vlot2
    assert srclot1 = @@sv.vendorlot_prepare(vlot1, ecs[0], lasermark: 'WF'), 'error creating 1st source lot'
    @@testlots << srclot1
    assert srclot2 = @@sv.vendorlot_prepare(vlot2, ecs[1], lasermark: 'PL'), 'error creating 2nd source lot'
    @@testlots << srclot2
    #
    puts "\n\nscheduled lot: #{lot}"
    puts "prepared vendor lot with valid LaserMark (WF): #{srclot1}, with invalid LaserMark (PL): #{srclot2}"
    gets
  end

  # Tech + Customer Exception for only StartType = POR (used SLT must be also defined only for POR)
  def test341_Tech_CustExc_POR   # POR - Plan on Record
    # schedule lot
    assert lot = @@sv.new_lot_release(@@newlotrel_params[:tech_custexc_por]), 'error scheduling test lot'
    @@testlots << lot
    #
    # search empty carriers
    assert ecs = @@svtest.get_empty_carrier(n: 2), 'error getting empty carriers'
    ecs.each {|ec| assert_equal 0, @@sv.carrier_counter_reset(ec)}
    #
    # create vendorlots + sourcelots
    assert vlot1 = @@sv.vendorlot_receive(@@startbank, @@srcproduct2, @@count), 'error creating 1st vendorlot'
    @@testlots << vlot1
    assert vlot2 = @@sv.vendorlot_receive(@@startbank, @@srcproduct1, @@count), 'error creating 2nd vendorlot'
    @@testlots << vlot2
    assert srclot1 = @@sv.vendorlot_prepare(vlot1, ecs[0], lasermark: 'PL'), 'error creating 1st source lot'
    @@testlots << srclot1
    assert srclot2 = @@sv.vendorlot_prepare(vlot2, ecs[1], lasermark: 'MM'), 'error creating 2nd source lot'
    @@testlots << srclot2
    #
    puts "\n\nscheduled lot: #{lot}, prepared vendor lots: #{srclot1}, #{srclot2}"
    gets
  end

  # Tech + Customer Exception + only 1 allowed LaserMark for only StartType = POR (used SLT must be also defined only for POR)
  def test342_Tech_CustExc_Only1LM_POR   # POR - Plan on Record
    # schedule lot
    assert lot = @@sv.new_lot_release(@@newlotrel_params[:tech_custexc_por]), 'error scheduling test lot'
    @@testlots << lot
    #
    # search empty carriers
    assert ecs = @@svtest.get_empty_carrier(n: 2), 'error getting empty carriers'
    ecs.each {|ec| assert_equal 0, @@sv.carrier_counter_reset(ec)}
    #
    # create vendorlots + sourcelots with one valid and one invalid LaserMark
    assert vlot1 = @@sv.vendorlot_receive(@@startbank, @@srcproduct1, @@count), 'error creating 1st vendorlot'
    @@testlots << vlot1
    assert vlot2 = @@sv.vendorlot_receive(@@startbank, @@srcproduct1, @@count), 'error creating 2nd vendorlot'
    @@testlots << vlot2
    assert srclot1 = @@sv.vendorlot_prepare(vlot1, ecs[0], lasermark: 'MM'), 'error creating 1st source lot'
    @@testlots << srclot1
    assert srclot2 = @@sv.vendorlot_prepare(vlot2, ecs[1], lasermark: 'WF'), 'error creating 2nd source lot'
    @@testlots << srclot2
    #
    puts "\n\nscheduled lot: #{lot}"
    puts "prepared vendor lot with valid LaserMark (MM): #{srclot1}, with invalid LaserMark (WF): #{srclot2}"
    gets
  end

  # Cross connection

  # WaferStartMaterial (Source) only for StartType = POR + used SLT must be defined only for non-POR
  def test411_CC_WSMPOR_SLTnonPOR   # POR - Plan on Record & non-POR - no Plan on Record
    # schedule lot
    assert lot = @@sv.new_lot_release(@@newlotrel_params[:cc_wsmpor_sltnonp]), 'error scheduling test lot'
    @@testlots << lot
    #
    # search empty carrier
    assert ec = @@svtest.get_empty_carrier, 'error getting empty carrier'
    assert_equal 0, @@sv.carrier_counter_reset(ec)
    #
    # create vendorlot + sourcelot
    assert vlot = @@sv.vendorlot_receive(@@startbank, @@srcproduct3, @@count), 'error creating vendorlot'
    @@testlots << vlot
    assert srclot = @@sv.vendorlot_prepare(vlot, ec, lasermark: 'PL'), 'error creating source lot'
    @@testlots << srclot
    #
    puts "\n\nscheduled lot: #{lot}, prepared vendor lot: #{srclot}"
    gets
  end

  # WaferStartMaterial (Source) only for StartType = non-POR + used SLT must be defined only for POR
  def test421_CC_WSMnonPOR_SLTPOR   # non-POR - no Plan on Record & POR - Plan on Record
    # schedule lot
    assert lot = @@sv.new_lot_release(@@newlotrel_params[:cc_wsmnonp_sltpor]), 'error scheduling test lot'
    @@testlots << lot
    #
    # search empty carrier
    assert ec = @@svtest.get_empty_carrier, 'error getting empty carrier'
    assert_equal 0, @@sv.carrier_counter_reset(ec)
    #
    # create vendorlot + sourcelot
    assert vlot = @@sv.vendorlot_receive(@@startbank, @@srcproduct4, @@count), 'error creating vendorlot'
    @@testlots << vlot
    assert srclot = @@sv.vendorlot_prepare(vlot, ec, lasermark: 'MM'), 'error creating source lot'
    @@testlots << srclot
    #
    puts "\n\nscheduled lot: #{lot}, prepared vendor lot: #{srclot}"
    gets
  end

  # Checks

  # LaserMark Check
  def test511_LM_Check   # independent of StartType
    # schedule lot
    assert lot = @@sv.new_lot_release(@@newlotrel_params[:lm_check]), 'error scheduling test lot'
    @@testlots << lot
    #
    # search empty carriers
    assert ecs = @@svtest.get_empty_carrier(n: 2), 'error getting empty carriers'
    ecs.each {|ec| assert_equal 0, @@sv.carrier_counter_reset(ec)}
    #
    # create vendorlots + sourcelots with one valid and one invalid LaserMark
    assert vlot1 = @@sv.vendorlot_receive(@@startbank, @@srcproduct3, @@count), 'error creating 1st vendorlot'
    @@testlots << vlot1
    assert vlot2 = @@sv.vendorlot_receive(@@startbank, @@srcproduct3, @@count), 'error creating 2nd vendorlot'
    @@testlots << vlot2
    assert srclot1 = @@sv.vendorlot_prepare(vlot1, ecs[0], lasermark: 'PL'), 'error creating 1st source lot'
    @@testlots << srclot1
    # w/o lasermark data --> using standard lasermark = "QA"
    assert srclot2 = @@sv.vendorlot_prepare(vlot2, ecs[1]), 'error creating 2nd source lot'
    @@testlots << srclot2
    #
    puts "\n\nscheduled lot: #{lot}"
    puts "prepared vendor lot with valid LaserMark (PL): #{srclot1}, with invalid LaserMark (QA): #{srclot2}"
    gets
  end

  # Special cases

  # Special source "OTHER.ALL"
  def test611_Src_Other   # independent of StartType
    # schedule lot
    assert lot1 = @@sv.new_lot_release(@@newlotrel_params[:scr_other_p1]), 'error scheduling test lot'
    @@testlots << lot1
    assert lot2 = @@sv.new_lot_release(@@newlotrel_params[:scr_other_p1]), 'error scheduling test lot'
    @@testlots << lot2
    assert lot3 = @@sv.new_lot_release(@@newlotrel_params[:scr_other_p2]), 'error scheduling test lot'
    @@testlots << lot3
    #
    # search empty carriers
    assert ecs = @@svtest.get_empty_carrier(n: 3), 'error empty carriers'
    ecs.each {|ec| assert_equal 0, @@sv.carrier_counter_reset(ec)}
    #
    # create vendorlots + sourcelots
    assert vlot1 = @@sv.vendorlot_receive(@@startbank, @@srcproduct5, @@count), 'error creating 1st vendorlot'
    @@testlots << vlot1
    assert vlot2 = @@sv.vendorlot_receive(@@startbank, @@srcproduct5, @@count), 'error creating 2nd vendorlot'
    @@testlots << vlot2
    assert vlot3 = @@sv.vendorlot_receive(@@startbank, @@srcproduct5, @@count), 'error creating 3rd vendorlot'
    @@testlots << vlot3
    assert srclot1 = @@sv.vendorlot_prepare(vlot1, ecs[0], lasermark: 'PL'), 'error creating 1st source lot'
    @@testlots << srclot1
    # w/o lasermark data --> using standard lasermark = "QA"
    assert srclot2 = @@sv.vendorlot_prepare(vlot2, ecs[1]), 'error creating 2nd source lot'
    @@testlots << srclot2
    assert srclot3 = @@sv.vendorlot_prepare(vlot3, ecs[2]), 'error creating 3rd source lot'
    @@testlots << srclot3
    #
    puts "\n\nscheduled lots with UT-product: #{lot1}, #{lot2}, with SHELBY-product: #{lot3}"
    puts "prepared vendor lot with spec-known LaserMark: #{srclot1}, spec-unknown LaserMark: #{srclot2}, #{srclot3}"
    gets
  end

  # Verification of non-availability for Source Lot    
  
  # Product with one linked SourceProductID has no existing Vendor-/SourceLot display
  def test711_nonSrcLot_Display_unusedSrcProd
    # schedule lot
    assert lot = @@sv.new_lot_release(@@newlotrel_params[:src_unused_sched]), 'error scheduling test lot'
    @@testlots << lot
    #
    # NODO: Creation of vendorlot + sourcelot is not required for this TC
    #
    puts "\n\nscheduled lot: #{lot}"
    gets
  end

  # Product with no linked SourceProductID
  def test712_nonSrcLot_Display_emptySrcProd
    # schedule lot
    assert lot = @@sv.new_lot_release(@@newlotrel_params[:src_empty_sched]), 'error scheduling test lot'
    @@testlots << lot
    #
    # NODO: Creation of vendorlot + sourcelot is not required for this TC
    #
    puts "\n\nscheduled lot: #{lot}"
    gets
  end

end
