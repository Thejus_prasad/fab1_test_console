=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: aschmid3, 2019-04-03

History:
  2021-08-31 sfrieske, removed Time#iso8601
=end

require 'SiViewTestCase'


# Create data to test FabGUI --> SiView Control --> Inhibit --> Matrixes
class FabGUI_SC_Inhibits_Matrix < SiViewTestCase
  ### possible/available tools and tool chambers:
  # short list - pointer to position of item (e.g. ALC1360 = [0], ALC1361 = [1], ...)
  @@tools = ['ALC1360', 'ALC1361', 'ALC1362', 'ALC1363', 'ALC1364', 'ALC1365',
              'THK503', 'THK610', 'THK612',
              'ETX200', 'ETX201', 'ETX202', 'ETX203', 'ETX204']
  # short list - same like @@tools above
  @@chambers = ['IRIS', 'STP', 'TRK', 'XC1', 'XC2',
                'ODP',
                'MM1', 'PM1', 'PM2', 'PM3']
  ### possible/available Equipment Recipes:
  @@recipes = %w(L-ALC.L-AEM93X.01 L-ALC.L-BEM93X.01 L-ALC.L-KEM93X.01 L-ALC.L-RJ329Y.01 L-ALC.L-RJI59X.01
                 L-ALC.L-VEM93X.01 L-ALC.L-VEM93Y.01)
  # another short form as list with wide space (%w)
  # another spelling style is - @@recipes = "L-ALC.L-AEM93X.01 L-ALC.L-BEM93X.01 L-ALC.L-KEM93X.01".split(' ') - or also w/o (' ')
  ### ProductGroups:
  @@pgs = ['3260C',
            'MPW0328-4F', 'QCTTV2-1S', 'SHELBY2-1F', 'TQV28H-3S']
  ### PDs:
  @@pds = %w(B1-CUPLTM.01 B2-CUPLTM.01 B3-CUPLTM.01 B4-CUPLTM.01)
  ### Technologies:
  @@techs = ['022FSOI-01', '022FSOI-A0', '22FSOI']
  ### Reason codes:
  @@reason1 = 'EMMO'
  @@reason2 = 'DEDI'
  @@reason3 = 'CREW'


  def self.after_all_passed
    @@tools.each {|tool| @@sv.eqp_cleanup(tool)}
  end


  def test00_setup
    # test preparation - in case of test abort all items will be removed which are still available as hangover
    @@tools.each {|tool| assert_equal 0, @@sv.eqp_cleanup(tool), 'error cleaning up eqp'}
  end

  # Create inhibits at RecipeMatrix
  # Setup available: tools [0]-[5], chambers [0]-[4], recipes [0]-[6]
  def test11_recipematrix
    # var memos created for prefix - otherwise row to long
    eqpmemo = "#{@@sv.user}_#{Time.now.utc.strftime('%F_%T').gsub(':', '-')}/RecMatrix - Eqp -- "
    eqpchmemo = "#{@@sv.user}_#{Time.now.utc.strftime('%F_%T').gsub(':', '-')}/RecMatrix - EqpCh -- "
    # !!! time definition must specified exactly because STRING - T replaced by space and Z replaced by nothing

    ### Equipment-Recipe
    [@@reason1, @@reason2].each {|rc1|   # .each do - replaced by - .each { }
      # simple INH - with default of manual creation (empty memo, all SLTs, w/o inh owner)
      @@sv.inhibit_entity(:eqp_recipe, [@@tools[0], @@recipes[1]], reason: rc1)
      # simple INH - memo (manually) + one SLT
      @@sv.inhibit_entity(:eqp_recipe, [@@tools[0], @@recipes[2]], reason: rc1,
                            memo: 'RecMatrix - only Eqp --> one SLT', sublottypes: 'EC')
      # prefix memo (var eqpmemo + add. text) + one SLT
      @@sv.inhibit_entity(:eqp_recipe, [@@tools[2], @@recipes[0]], reason: rc1,
                            memo: eqpmemo + 'prefix memo + one SLT', sublottypes: 'EJ')
      # few SLTs
      @@sv.inhibit_entity(:eqp_recipe, [@@tools[1], @@recipes[0]], reason: rc1,
                            memo: eqpmemo + 'few SLTs', sublottypes: ['EM', 'ES', 'QE', 'PO'])
      # one SLT + Start TS (+ 3600 sec = 1 h)
      @@sv.inhibit_entity(:eqp_recipe, [@@tools[3], @@recipes[6]], reason: rc1,
                            memo: eqpmemo + 'one SLT + Start TS', sublottypes: 'EJ', tstart: (Time.now + 3600))
      # inh owner change is required to execute in two steps (FabGUI shows an additional popup for inh owner input)
      assert inh = @@sv.inhibit_entity(:eqp_recipe, [@@tools[0], @@recipes[3]], reason: rc1)
      assert_equal 0, @@sv.inhibit_update(inh, inhibit_owner: 'gmstest01')
      ####@@sv.inhibit_update(:eqp_recipe, [@@tools[0], @@recipes[3]], reason: rc1, inhibit_owner: 'gmstest01')
      # ERT (ExpectedReleaseTime) change is required to execute in two steps (FabGUI shows an additional popup for ERT input)
      assert inh = @@sv.inhibit_entity(:eqp_recipe, [@@tools[2], @@recipes[1]], reason: rc1)
      assert_equal 0, @@sv.inhibit_update(inh, release_time: (Time.now + 7200))  # 7200 sec = 2 h
      # INH with all possible settings
      assert inh = @@sv.inhibit_entity(:eqp_recipe, [@@tools[5], @@recipes[5]], reason: rc1,
                                        memo: eqpmemo + 'inh with all actions', sublottypes: ['RD', 'PX'], tstart: (Time.now))
      # parameter memo = comment at inh-update
      assert_equal 0, @@sv.inhibit_update(inh,
                            comment: 'Updated memo - all actions!!!', inhibit_owner: 'gmstest01', release_time: (Time.now + 14400))
    }
    ### Equipment-Chamber-Recipe
    [@@reason1, @@reason3].each {|rc2|   # .each do - replaced by - .each { }
      [@@chambers[0], @@chambers[3]].each {|ch1|
        # simple INH - with default of manual creation (empty memo, all SLTs, w/o inh owner)
        @@sv.inhibit_entity(:eqp_chamber_recipe, [@@tools[1], @@recipes[2]], attrib: [ch1], reason: rc2)
        # prefix memo (var eqpchmemo + add. text) + one SLT
        @@sv.inhibit_entity(:eqp_chamber_recipe, [@@tools[2], @@recipes[0]], attrib: [ch1],
                              reason: rc2, memo: eqpchmemo + 'prefix memo + one SLT', sublottypes: 'EJ')
      }
      [@@chambers[1], @@chambers[2]].each {|ch2|
        # few SLTs
        @@sv.inhibit_entity(:eqp_chamber_recipe, [@@tools[1], @@recipes[0]], attrib: [ch2],
                              reason: rc2, memo: eqpchmemo + 'few SLTs', sublottypes: ['EM', 'ES', 'QE', 'PO'])
        # one SLT + Start TS (+ 3600 sec = 1 h)
        @@sv.inhibit_entity(:eqp_chamber_recipe, [@@tools[3], @@recipes[6]], attrib: [ch2],
                              reason: rc2, memo: eqpchmemo + 'one SLT + Start TS', sublottypes: 'EJ', tstart: (Time.now + 3600))
      }
      [@@chambers[2], @@chambers[4]].each {|ch3|
        # inh owner change is required to execute in two steps (FabGUI shows an additional popup for inh owner input)
        assert inh = @@sv.inhibit_entity(:eqp_chamber_recipe, [@@tools[0], @@recipes[3]], attrib: [ch3], reason: rc2)
        assert_equal 0, @@sv.inhibit_update(inh, inhibit_owner: 'gmstest01')
        # ERT (ExpectedReleaseTime) change is required to execute in two steps (FabGUI shows an additional popup for ERT input)
        assert inh = @@sv.inhibit_entity(:eqp_chamber_recipe, [@@tools[2], @@recipes[1]], attrib: [ch3], reason: rc2)
        assert_equal 0, @@sv.inhibit_update(inh, release_time: (Time.now + 7200))  # 7200 sec = 2 h
      }
    }
    #
    puts "\n-> Execute the tests at 'RecipeMatrix' and press Enter to continue."
    gets
  end

  # Create inhibits at ProductGroupPDMatrix
  # Setup available: tools [6]-[8], chambers [5], pgs [0], pds [0]-[3]
  def test21_pgpdmatrix
    # var memo for prefix
    eqpmemo = "#{@@sv.user}_#{Time.now.utc.strftime('%F_%T').gsub(':', '-')}/PGPDMatrix - Eqp -- "
    eqpchmemo = "#{@@sv.user}_#{Time.now.utc.strftime('%F_%T').gsub(':', '-')}/PGPDMatrix - EqpCh -- "
    # !!! time definition must specified exactly because STRING - T replaced by space and Z replaced by nothing

    ### Equipment-ProductGroup-PD - !!! classname other order
    [@@reason1, @@reason2].each {|rc3|
      # n-PDs : 1-Equipment - with memo, SLTs, Start TS and update to inh owner and ERT
      [@@pds[0], @@pds[1]].each {|pd|
        assert inh = @@sv.inhibit_entity(:productgroup_pd_eqp, [@@pgs[0], pd, @@tools[6]], reason: rc3,
                                          memo: eqpmemo + 'all inh actions', sublottypes: ['EO', 'QD'], tstart: (Time.now))
        assert_equal 0, @@sv.inhibit_update(inh,
                                            inhibit_owner: 'gmstest01', release_time: (Time.now + 7200))
      }
      # 1-PD : n-Equipments - with memo
      [@@tools[7], @@tools[8]].each {|tool|
        @@sv.inhibit_entity(:productgroup_pd_eqp, [@@pgs[0], @@pds[0], tool], reason: rc3, memo: eqpmemo + 'test')
      }
    }
    ### Equipment-Chamber-ProductGroup-PD
    assert @@sv.inhibit_entity(:eqp_chamber_productgroup_pd, [@@tools[6], @@pgs[0], @@pds[1]], attrib: @@chambers[5],
                                            reason: @@reason1, memo: eqpchmemo + 'qa test!$%')
    #
    puts "\n-> Execute the tests at 'ProductGroupPDMatrix' and press Enter to continue."
    gets
  end

  # Create inhibits at RecipeProductGroupMatrix
  # Setup available: tools [0]-[5], chambers [0]-[4], recipes [2]/[4]/[5], pgs [1]-[4]
  def test31_recipepgmatrix
    # var memo for prefix
    eqpmemo = "#{@@sv.user}_#{Time.now.utc.strftime('%F_%T').gsub(':', '-')}/RecPGMatrix - Eqp -- "
    eqpchmemo = "#{@@sv.user}_#{Time.now.utc.strftime('%F_%T').gsub(':', '-')}/RecPGMatrix - EqpCh -- "
    # !!! time definition must specified exactly because STRING - T replaced by space and Z replaced by nothing

    ### Equipment-ProductGroup-Recipe - !!! classname other order
    # all corresponding inhibits will created and shown at 'FabGUI InhibitList'...
    # ...as long as the defined objects are available at SiView
    # !!! independent of the visibility at matrix view !!!
    # create inhibit matrix: inhibits[REASON][PG][EQP][RECIPE]
    inhmatrix = {}
    [@@reason1, @@reason3].each {|reason|
      inhpgs = {}
      [@@pgs[1], @@pgs[3], @@pgs[4]].each {|pg|
        inheqps = {}
        [@@tools[0], @@tools[2], @@tools[4]].each {|eqp|
          inhrcps = {}
          [@@recipes[4], @@recipes[5]].each {|recipe|
            inhrcps[recipe] = @@sv.inhibit_entity(:productgroup_eqp_recipe, [pg, eqp, recipe], reason: reason, memo: eqpmemo + 'QA test !$!')
          }
          inheqps[eqp] = inhrcps.clone
        }
        inhpgs[pg] = inheqps.clone
      }
      inhmatrix[reason] = inhpgs.clone
    }
    #
    puts "\n-> Verify created inhibits at 'InhibitList' and 'RecipeProductGroupMatrix'."
    puts "\n-> Press Enter to continue with update of few inhibits."
    gets
    # selective update of only few inhibts (same precondition with SiView objects like above)
    # Example for created inhibit but invisibles at matrix view
    assert_equal 0, @@sv.inhibit_update(inhmatrix[@@reason3][@@pgs[3]][@@tools[2]][@@recipes[5]],
                                        comment: 'update', inhibit_owner: 'gmstest01')
    # inhibit changes which can verify also at matrix view
    [@@tools[2], @@tools[4]].each {|tool|
      assert_equal 0, @@sv.inhibit_update(inhmatrix[@@reason3][@@pgs[4]][tool][@@recipes[4]],
                                          comment: "#{Time.now.utc} / update for inh owner", inhibit_owner: 'gmstest01')
      assert_equal 0, @@sv.inhibit_update(inhmatrix[@@reason3][@@pgs[1]][tool][@@recipes[5]],
                                          comment: "#{Time.now.utc} / update for ERT", release_time: (Time.now + 3600))
    }
    #
    puts "\n-> Verify updated inhibits at 'InhibitList' and 'RecipeProductGroupMatrix'."
    puts "\n-> Press Enter to continue with EquipmentChamber inhibits."
    gets
    ### Equipment-Chamber-ProductGroup-Recipe
    [@@tools[1], @@tools[2]].each {|tool|
      [@@chambers[1], @@chambers[2]].each {|ch|
        @@sv.inhibit_entity(:eqp_chamber_productgroup_recipe, [tool, @@pgs[3], @@recipes[2]], attrib: ch,
                              reason: @@reason2, memo: eqpchmemo + 'test QA /%&')
      }
    }
    #
    puts "\n-> Execute the tests at 'RecipeProductGroupMatrix' and press Enter to continue."
    gets
  end

  # Create inhibits at TechnologyMatrix
  # Setup available: tools [9]-[13], chambers [6]-[9], techs [0]-[2]
  def test41_technologymatrix
    # var memo for prefix
    eqpmemo = "#{@@sv.user}_#{Time.now.utc.strftime('%F_%T').gsub(':', '-')}/TechMatrix - Eqp -- "
    eqpchmemo = "#{@@sv.user}_#{Time.now.utc.strftime('%F_%T').gsub(':', '-')}/TechMatrix - EqpCh -- "
    # !!! time definition must specified exactly because STRING - T replaced by space and Z replaced by nothing

    ### Equipment-Technology - !!! classname other order
    # create inhibit matrix: inhibits[EQP][TECH]
    inhmatrix = {}
    [@@tools[9], @@tools[10], @@tools[12]].each {|eqp|
      inhtechs = {}
      [@@techs[0], @@techs[2]].each {|tech|
        inhtechs[tech] = @@sv.inhibit_entity(:technology_eqp, [tech, eqp], reason: @@reason2, memo: eqpmemo + 'tested by #QA')
      }
      inhmatrix[eqp] = inhtechs.clone
    }
    # inh with all possible settings at creation
    assert inhall = @@sv.inhibit_entity(:technology_eqp, [@@techs[1], @@tools[11]], reason: @@reason2,
                                            memo: eqpmemo + 'tested by #QA !inh-all!', sublottypes: ['RD', 'ES'], tstart: (Time.now))
    #
    puts "\n-> Verify created inhibits at 'InhibitList' and 'TechnologyMatrix'."
    puts "\n-> Press Enter to continue with update of few inhibits."
    gets
    # update of few inhibits
    ["#{Time.now.utc} / update for inh owner + ERT"].each {|comment|
      ['gmstest01'].each {|inhowner|
        [(Time.now + 5400)].each {|ert|
          assert_equal 0, @@sv.inhibit_update(inhmatrix[@@tools[9]][@@techs[0]],
                                              comment: comment, inhibit_owner: inhowner, release_time: ert)
          assert_equal 0, @@sv.inhibit_update(inhall,
                                              comment: comment + ' inh-all', inhibit_owner: inhowner, release_time: ert)
          assert_equal 0, @@sv.inhibit_update(inhmatrix[@@tools[12]][@@techs[2]],
                                              comment: comment, inhibit_owner: inhowner, release_time: ert)
        }
      }
    }
    #
    puts "\n-> Verify updated inhibits at 'InhibitList' and 'TechnologyMatrix'."
    puts "\n-> Press Enter to continue with EquipmentChamber inhibits."
    gets
    ### Equipment-Chamber-Technology - !!! classname other order
    # !!! attention: chamber attrib needs two values as array because eqp isn't the first of classname
    [@@tools[10], @@tools[11], @@tools[12]].each {|tool|
      [['EJ', 'QD'], 'PX'].each {|slt|
        @@sv.inhibit_entity(:technology_eqp_chamber, [@@techs[2], tool], attrib: [" ", @@chambers[6]],
                              reason: @@reason2, memo: eqpchmemo + 'with SLT(s)', sublottypes: slt)
      }
      [@@techs[0], @@techs[1]].each {|tech|
        @@sv.inhibit_entity(:technology_eqp_chamber, [tech, tool], attrib: [" ", @@chambers[8]],
                              reason: @@reason2, memo: eqpchmemo + 'with SLT + Start TS',
                              sublottypes: 'EO', tstart: (Time.now + 1800))
      }
    }
    #
    puts "\n-> Execute the tests at 'TechnologyMatrix' and press Enter to continue (with cleanup of all created inhibits)."
    gets
  end

end