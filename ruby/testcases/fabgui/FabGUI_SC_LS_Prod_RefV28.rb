=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author:   aschmid3, 2020-02-28

Version: 26.0.6 

History:
  2020-02-28 aschmid3, init version

Notes:
  2020-02-28, 1) needs spec "C14-Source-Product-Assignment" --> Attention: with FabP-v29 new spec!!!
              2) used product and source/vendor needs a connection inside this spec

=end

require 'SiViewTestCase'


# Create lots to test FabGUI - SiView Control - Lot Start - Production
class FabGUI_SC_LS_Prod < SiViewTestCase

  # settings for 'Lot Comment' - necessary for STB in FabGUI otherwise LotHold X-JSTBWA (jCAP: STBMfgWIPAttributes)
  erpitem = "ENGINEERING-U00"
  dpml = 1.2
  fsd = Time.now.strftime('%F')
  comment = "ERPITEM=#{erpitem},DPML=#{dpml},PLANNED_FSD=#{fsd}"
  # lot settings 
  @@newlotrel_params = {
    # only for lot scheduling - TODO: check the possibility to insert test setup (product: 'UT-TKBTF.01', route: 'UTRT-TKBTF.01') into required spec "C14-Source-Product-Assignment"
    por_sched: {product: 'SHELBY31AK.B1', route: 'C02-1550.01', sublottype: "PO", order: "2ERWF-12537",  # "order:" instead of erpitem: 'ENGINEERING-U00',
                comment: comment
    },
    nonp_sched: {product: 'SHELBY31AK.B1', route: 'C02-1550.01', sublottype: "EB", order: "2ERWF-12537",  # "order:" instead of erpitem: 'ENGINEERING-U00',
                comment: comment
    },
  }
  @@newlot_params = {
    rma_plot: {product: 'SHELBY31AK.B1', route: 'C02-1550.01', sublottype: "PO", bankin: true, user_parameters: {}},
  }
  # vendor-/sourcelot settings
  @@startbank = 'ON-RAW'
  @@srcproduct = '2ERWF-12537.ALL'
  @@count = 25
  @@vendlot_params = { 
    happy: {vendorlot: 'SILTRONIC - 999999/QHRM2SSD1 - 31007610'}   
    # SiVieMM - Lot Info Menu - tab: 'Detail' - Vendor Lot ID --> will verify by FabGUI - SC - LS - Production - STB Details - Source Lot: column 'Vendor'
  }

  @@testlots = []


  def self.after_all_passed
    @@testlots.each {|lot| @@sv.delete_lot_family(lot)}
  end


  # Normal Lot STB
  
  def test111_POR_Lot   # POR - Plan on Record
    # schedule lot
    assert lot = @@sv.new_lot_release(@@newlotrel_params[:por_sched]), 'error scheduling of test lot for the route and product'
    @@testlots << lot
    #
    puts "\n\nScheduled lot: #{lot}"
    gets
    #
    # search empty carrier
    assert ec = @@svtest.get_empty_carrier, 'error empty carrier'
    assert_equal 0, @@sv.carrier_counter_reset(ec)
    #
    # create vendorlot + sourcelot
    assert vlot = @@sv.vendorlot_receive(@@startbank, @@srcproduct, @@count, @@vendlot_params[:happy]), 'error creating vendorlot'
    @@testlots << vlot
    assert srclot = @@sv.vendorlot_prepare(vlot, ec, lasermark: 'PL'), 'error creating source lot'
    @@testlots << srclot
    #
    puts "\n\nPrepared vendor lot: #{srclot}"
    gets
    #
    # TODO: --> DONE - 03.03.2020 !!!
    # - Vendorlot is empty if all 25 wafers are taken over to lot
    # - Lot is available at SiViewMM on first PD of used route but it has a X-JSTBWA hold with comment
    #    "Unable to Set SP(s) [ERPItem, ERPSalesOrder, GateCD_Target_PLN, TurnkeyType, FabDPML, FSD, CustomerQualityCode, FOD_INIT, FOD_COMMIT, CountryOfOrigin, SourceFabCode]; 
    #     reason: oracle item ID not found in lot comment"
    # --> example lot: UXYU45032.000 - lot comment: "ERPITEM=ENGINEERING-U00,DPML=1.2,PLANNED_FSD=2019-11-05" - lot was created per "svtest.new_lot"
  end

  def test112_nonPOR_Lot    # non-POR - no Plan on Record
    # schedule lot
    assert lot = @@sv.new_lot_release(@@newlotrel_params[:nonp_sched]), 'error scheduling of test lot for the route and product'
    @@testlots << lot
    #
    puts "\n\nLot #{lot} is scheduled."
    gets
    #
    # search empty carrier
    assert ec = @@svtest.get_empty_carrier, 'error empty carrier'
    assert_equal 0, @@sv.carrier_counter_reset(ec)
    #
    # create vendorlot + sourcelot
    assert vlot = @@sv.vendorlot_receive(@@startbank, @@srcproduct, @@count ,@@vendlot_params[:happy]), 'error creating vendorlot'
    @@testlots << vlot
    assert srclot = @@sv.vendorlot_prepare(vlot, ec), 'error creating source lot'
    @@testlots << srclot
    #
    puts "\n\nVendor-/Sourcelot #{srclot} is created."
    gets
    #
  end

  # Lot STB for RMA - Returned Material from Customer

  def test211_RMA_POR_Lot
    # lot circle for ship
    assert lot = @@svtest.new_lot(@@newlot_params[:rma_plot]), 'error creating Fab lot'
    @@testlots << lot
    #
    assert wfrs = @@sv.lot_info(lot, wafers: true).wafers    # saved wafer IDs and order for future comparison
    #
    puts "\n\nSaved WaferIDs: #{wfrs} \n\n Only for visibility and comparison later."
    gets
    #
    assert = @@sv.lot_ship(lot, 'OX-ENDFG', memo: "QA test - RMA POR lot")
    assert = @@sv.delete_lot_family(lot, delete: true)  # delete: true --> final lot deletion
    #
    # NODO anymore because job 'LotMaintenance' hang once in a while
    # $log.info 'Waiting up to 1 h for lot deletion'
    # assert wait_for(timeout: 3600) {!@@sv.lot_exists?(lot)}, 'lot has not been deleted in SiView' # Should be lot findable, check LotMaintenance at sv-SM
    # 
    puts "\n\nLot #{lot} is deleted finally in SiViewMM."   # No manual work necessary in SiViewMM
    gets
    #
    # schedule lot with previous used lot ID
    assert = @@sv.new_lot_release(@@newlotrel_params[:por_sched].merge(lot: lot)), 'error scheduling with previous used lot ID for RMA'
    assert ec = @@svtest.get_empty_carrier, 'error empty carrier'
    assert_equal 0, @@sv.carrier_counter_reset(ec)
    assert vlot = @@sv.vendorlot_receive(@@startbank, @@srcproduct, 25 ,@@vendlot_params[:happy]), 'error creating vendorlot'
    @@testlots << vlot
    # assert srclot = @@sv.vendorlot_prepare(vlot, ec), 'error creating source lot'
    assert srclot = @@sv.vendorlot_prepare(vlot, ec, waferids: wfrs.map(&:wafer).reverse), 'error creating source lot with wafer IDs of previous used lot ID'
    @@testlots << srclot
    #
    puts "\n\nVendor-/Sourcelot #{srclot} is created and wafers are in reversed order."
    gets
    #
  end

end