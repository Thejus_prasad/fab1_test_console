=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: aschmid3 and sfrieske, 2019-05-24

=end

require 'asmview'
require 'jcap/turnkeybtf'  # for TestDB
require 'jcap/turnkeybtftest'
require 'SiViewTestCase'

# UNDER DEVELOPMENT

# Create BackupOperation lots to test FabGUI Shipping - UNUSED
class FabGUI_SH_Ship_BOP < SiViewTestCase
  @@sv_defaults = {carrier_category: nil, carriers: '%', customer: 'mtk'} # not 'gf' or 'amd'
  @@newlot_params = {
    # no AsmView tracking
    noav_u: {product: 'UT-TKBTF.01', route: 'UTRT-TKBTF.01', user_parameters: {}},
    noav_u_avprod: {product: 'SHELBY-TKBTF.QA', route: 'UTRT-TKBTF.01', user_parameters: {}},
    noav_n: {product: 'UT-TKBTF.01', route: 'UTRT-TKBTF.01',
      user_parameters: {'TurnkeyType'=>'N', 'TurnkeySubconIDBump'=>'GG', 'TurnkeySubconIDSort'=>'GG'}
    },
    noav_r: {product: 'UT-TKBTF.01', route: 'UTRT-TKBTF.01',
      user_parameters: {'TurnkeyType'=>'R', 'TurnkeySubconIDBump'=>'GG', 'TurnkeySubconIDSort'=>'GG'}
    },
    noav_s: {product: 'UT-TKBTF.01', route: 'UTRT-TKBTF.01', 
      user_parameters: {'TurnkeyType'=>'S', 'TurnkeySubconIDSort'=>'GG'} 
    },
    noav_l: {product: 'UT-TKBTF.01', route: 'UTRT-TKBTF.01',
      user_parameters: {'TurnkeyType'=>'L', 'TurnkeySubconIDBump'=>'GG'}
    },
    # with AsmView tracking
    #   also working (?): {product: 'CN78002AC.D1', route: 'C02-1431.01', erpitem: 'CN78002AC-JD0', customer: 'mtk'}
    withav_j: {product: 'SHELBY-TKBTF.QA', route: 'UTRT-TKBTF.01', erpitem: '*SHELBY21AE-M00-JB4',
      user_parameters: {'TurnkeyType'=>'J', 'TurnkeySubconIDBump'=>'GG', 'TurnkeySubconIDSort'=>'GG'}
    },
    withav_s: {product: 'SHELBY-TKBTF.QA', route: 'UTRT-TKBTF.01', erpitem: '*SHELBY21AE-M00-JB4',
      user_parameters: {'TurnkeyType'=>'S', 'TurnkeySubconIDSort'=>'GG'}
    },
    withav_l: {product: 'SHELBY-TKBTF-L.QA', route: 'UTRT-TKBTF-L.01', erpitem: '*SHELBY21AE-M00-JB4',
      user_parameters: {'TurnkeyType'=>'L', 'TurnkeySubconIDBump'=>'GG'}
    },
    # subcon (trans) shipment
    noav_j_sort: {product: 'TKEY01.A0', route: 'C02-TKEY.01', erpitem: '*SHELBY21AE-M00-JB4',
      user_parameters: {'TurnkeyType'=>'J', 'TurnkeySubconIDBump'=>'GG', 'TurnkeySubconIDSort'=>'AK'}
    },
    noav_j_bump: {product: 'TKEY01.A0', route: 'C02-TKEY.01', erpitem: '*SHELBY21AE-M00-JB4',
      user_parameters: {'TurnkeyType'=>'J', 'TurnkeySubconIDBump'=>'CT', 'TurnkeySubconIDSort'=>'AK'}
    },

    # this is like Fab10 lots arrive (test prep Arthur)
    # transbumpFab: {product: 'WARP352-A1FABU.AA01', route: '000-FAB-00-BTFRCVX.0001', erpitem: '0000001VN343', customer: 'ericab',
    #   user_parameters: {'TurnkeyType'=>'R', 'TurnkeySubconIDBump'=>'GG', 'TurnkeySubconIDSort'=>'GB'}
    # },
    transbump: {product: 'WARP352-A1FABB.AA01', route: '000-BUMP-01-PEB2NCG.0001', erpitem: '0000001VN343', customer: 'ericab',
      user_parameters: {'TurnkeyType'=>'R', 'TurnkeySubconIDBump'=>'GG', 'TurnkeySubconIDSort'=>'GB', 'CountryOfOrigin'=>'US', 'SourceFabCode'=>'F8'}
    },
    
    # TODO: under development
    ordernoav: {product: 'UT-TKBTF.01', route: 'UTRT-TKBTF.01',
      user_parameters: {'ERPSalesOrder'=>'6010003641:2:1'}
    }
  }

  # # src fab F8 / US, with AsmView tracking (backup operations required, ERPItem must match product and customer(?))
  @@env_src = 'f8stag'
  @@sv_defaults_src = {carrier_category: nil, carriers: '%', customer: 'qgt'}  # 'qgt' and 'mtk' not working in AsmView (lot hold before TTS)
  @@newlot_params_f8 = {
    withav_r_f8_BUMP: {product: 'UT-PRODUCT-FAB1.01', route: 'UTRT101.01', erpitem: '0000001VN343', customer: 'ericab',
      user_parameters: {'TurnkeyType'=>'R', 'TurnkeySubconIDBump'=>'GG', 'TurnkeySubconIDSort'=>'GB'}
    }
  }
  #   withav_j_f8: {
  #     product: 'SHELBY3CC.01', route: 'FF-28LPQ-45M.01', erpitem: '*SHELBY3CC-J01',
  #     user_parameters: {'TurnkeyType'=>'J', 'TurnkeySubconIDBump'=>'GG', 'TurnkeySubconIDSort'=>'GG'}
  #   },
  #   withav_r_f8: {
  #     product: 'SHELBY3CC.01', route: 'FF-28LPQ-45M.01', erpitem: '*SHELBY3CC-J01',
  #     user_parameters: {'TurnkeyType'=>'R', 'TurnkeySubconIDBump'=>'GG', 'TurnkeySubconIDSort'=>'GG'}
  #   },
  #   withav_s_f8: {
  #     product: 'SHELBY3CC.01', route: 'FF-28LPQ-45M.01', erpitem: '*SHELBY3CC-J01',
  #     user_parameters: {'TurnkeyType'=>'S', 'TurnkeySubconIDSort'=>'GG'}
  #   },
  #   withav_l_f8: {
  #     product: 'ELLSMR11MA.A0', route: 'FF-ELLSMR1MA.01', erpitem: 'ELLSMR11MA-M01-LA0',
  #     user_parameters: {'TurnkeyType'=>'L', 'TurnkeySubconIDBump'=>'GG'}
  #   },
  # }
  # @@psend_rcv = {backup_op: /TKEYBUMPSHIPINIT/, backup_bank: 'O-BACKUPOPER'}

  #
  # @@sv_bump = {product: 'WARP352-A1FABB.AA01', route: '000-BUMP-01-PEB2NCG.0001', erpitem: '0000001VN343'}
  
  @@testlots = []
  @@testlots_bop = []
  
  
  def self.after_all_passed
    @@testlots_bop.each {|lot| @@sv_src.backup_delete_lot_family(lot, @@sv)}
    @@testlots.each {|lot| @@sv.delete_lot_family(lot)}
  end
  
  
  def test00_setup
    $setup_ok = false
    #
    @@newlot_params.each_value {|v|
      v[:user_parameters]['ShipStorageExpiration'] = '20300303.235900'
      v[:user_parameters]['QualityGateApproval'] = 'Approved,qauser'
    }
    @@tktest = Turnkey::BTFTest.new($env, sv: @@sv)
    @@av = @@tktest.av
    @@dbbtf = Turnkey::TestDB.new('itdc', sv: @@sv)
    #
    @@svtest_src = SiView::Test.new(@@env_src, @@sv_defaults_src)
    @@sv_src = @@svtest_src.sv
    #
    $setup_ok = true
  end
  
  ### Standard Shipment (Fab1 -> Customer)
  
  def test111_FinalStd_noav_U # FinalStd_noav_processing with UT-* product (not included in FabGUI-AsmView-config)
    # create lot, locate it to LOTSHIPDESIG and set OQAGoodDie
    assert lot = @@svtest.new_lot(@@newlot_params[:noav_u]), 'error creating Fab lot'
    @@testlots << lot
    assert_equal 0, @@sv.lot_opelocate(lot, nil, op: 'LOTSHIPDESIG.01')
    @@sv.lot_info(lot, wafers: true).wafers.each {|w| 
      assert_equal 0, @@sv.user_parameter_change('Wafer', w.wafer, 'OQAGoodDie', 100, 1, datatype: 'INTEGER')
    }
    #
    puts "\n\nLot #{lot} is ready for shipment"
    gets
    # verify in sv
    $log.info 'verifying lot script parameter ShipComment'
    assert @@sv.user_parameter('Lot', lot, name: 'ShipComment').valueflag, 'script parameter ShipComment not set'
  end
  
  def test112_FinalStd_noav_U_avprod  # FinalStd_noav_processing with configured AsmView product (included in FabGUI-AsmView-config)
    # create lot, locate it to LOTSHIPDESIG and set OQAGoodDie
    assert lot = @@svtest.new_lot(@@newlot_params[:noav_u_avprod]), 'error creating Fab lot'
    @@testlots << lot
    assert_equal 0, @@sv.lot_opelocate(lot, nil, op: 'LOTSHIPDESIG.01')
    @@sv.lot_info(lot, wafers: true).wafers.each {|w| 
      assert_equal 0, @@sv.user_parameter_change('Wafer', w.wafer, 'OQAGoodDie', 100, 1, datatype: 'INTEGER')
    }
    #
    puts "\n\nLot #{lot} is ready for shipment"
    gets
    # verify in sv
    $log.info 'verifying lot script parameter ShipComment'
    assert @@sv.user_parameter('Lot', lot, name: 'ShipComment').valueflag, 'script parameter ShipComment not set'
  end
  
  def test131_FinalStd_noav_S # FinalStd_noav_processing for "S = Sort only" with UT-* product
    # create lot, locate it to LOTSHIPDESIG and set DieCountGood
    assert lot = @@svtest.new_lot(@@newlot_params[:noav_s]), 'error creating Fab lot'
    @@testlots << lot
    assert_equal 0, @@sv.lot_opelocate(lot, nil, op: 'LOTSHIPDESIG.01')
    @@sv.lot_info(lot, wafers: true).wafers.each {|w| 
      assert_equal 0, @@sv.user_parameter_change('Wafer', w.wafer, 'DieCountGood', 100, 1, datatype: 'INTEGER')
    }
    #
    puts "\n\nLot #{lot} is ready for shipment"
    gets
    # verify in sv
    $log.info 'verifying lot script parameter ShipComment'
    assert @@sv.user_parameter('Lot', lot, name: 'ShipComment').valueflag, 'script parameter ShipComment not set'
  end
  
  def test132_FinalStd_noav_R # FinalStd_noav_processing --> TODO: error msg after ERP transmission
    # create lot, locate it to LOTSHIPDESIG and set DieCountGood
    assert lot = @@svtest.new_lot(@@newlot_params[:noav_r]), 'error creating Fab lot'
    @@testlots << lot
    assert_equal 0, @@sv.lot_opelocate(lot, nil, op: 'LOTSHIPDESIG.01')
    @@sv.lot_info(lot, wafers: true).wafers.each {|w| 
      assert_equal 0, @@sv.user_parameter_change('Wafer', w.wafer, 'DieCountGood', 100, 1, datatype: 'INTEGER')
    }
    #
    puts "\n\nLot #{lot} is ready for shipment"
    gets
    # verify in sv
    $log.info 'verifying lot script parameter ShipComment'
    assert @@sv.user_parameter('Lot', lot, name: 'ShipComment').valueflag, 'script parameter ShipComment not set'
  end
  
  def test133_FinalStd_noav_L # FinalStd_noav_processing for "L = Bump only" with UT-* product
    # create lot, locate it to LOTSHIPDESIG and set OQAGoodDie
    assert lot = @@svtest.new_lot(@@newlot_params[:noav_l]), 'error creating Fab lot'
    @@testlots << lot
    assert_equal 0, @@sv.lot_opelocate(lot, nil, op: 'LOTSHIPDESIG.01')
    @@sv.lot_info(lot, wafers: true).wafers.each {|w| 
      assert_equal 0, @@sv.user_parameter_change('Wafer', w.wafer, 'OQAGoodDie', 100, 1, datatype: 'INTEGER')
    }
    #
    puts "\n\nLot #{lot} is ready for shipment"
    gets
    # verify in sv
    $log.info 'verifying lot script parameter ShipComment'
    assert @@sv.user_parameter('Lot', lot, name: 'ShipComment').valueflag, 'script parameter ShipComment not set'
  end
  
  # def test134_FinalStd_noav_N # redundant, same as J and R
  #   # create lot, locate it to LOTSHIPDESIG and set DieCountGood
  #   assert lot = @@svtest.new_lot(@@newlot_params[:noav_n]), 'error creating Fab lot'
  #   @@testlots << lot
  #   assert_equal 0, @@sv.lot_opelocate(lot, nil, op: 'LOTSHIPDESIG.01')
  #   @@sv.lot_info(lot, wafers: true).wafers.each {|w| 
  #     assert_equal 0, @@sv.user_parameter_change('Wafer', w.wafer, 'DieCountGood', 100, 1, datatype: 'INTEGER')
  #   }
  #   #
  #   puts "\n\nLot #{lot} is ready for shipment"
  #   gets
  #   # verify in sv
  #   $log.info 'verifying lot script parameter ShipComment'
  #   assert @@sv.user_parameter('Lot', lot, name: 'ShipComment').valueflag, 'script parameter ShipComment not set'
  # end
  
  
  ### Standard Shipment (Fab1 -> Customer)
 
  def test221_FinalStd_withav_J
    # create lot and process it through BTF with AsmView tracking
    assert lot = @@svtest.new_lot(@@newlot_params[:withav_j]), 'error creating test lot'
    @@testlots << lot
    assert @@tktest.start_btf(lot), 'error starting test lot'
    assert @@tktest.process_btf(lot), 'error processing lot'
    # lot is at (sv) LOTSHIPDESIG.01, (av) TKYSORT.1, set TestDB data and DieCountGood (defaults to 100 for all wafers)
    assert @@dbbtf.set_lot_wafer_bins(lot), 'error setting binning data'
    #
    puts "\n\nLot #{lot} is ready for shipment"
    gets
    # verify in sv and av
    $log.info 'verifying lot script parameters ShipComment (sv) and FabQualityCheck (av)'
    assert @@sv.user_parameter('Lot', lot, name: 'ShipComment').valueflag, '(sv) ShipComment not set'
    assert @@av.user_parameter('Lot', lot, name: 'FabQualityCheck').valueflag, '(av) FabQualityCheck not set'
  end
  
  def test231_FinalStd_withav_S # S: Sort only
    # create lot and process it through BTF SORT with AsmView tracking
    assert lot = @@svtest.new_lot(@@newlot_params[:withav_s]), 'error creating test lot'
    @@testlots << lot
    assert @@tktest.start_btf(lot), 'error starting test lot'
    assert @@tktest.process_btf(lot), 'error processing lot'
    # lot is at (sv) LOTSHIPDESIG.01, (av) TKYSORT.1, set TestDB data and DieCountGood (defaults to 100 for all wafers)
    assert @@dbbtf.set_lot_wafer_bins(lot), 'error setting binning data'
    #
    puts "\n\nLot #{lot} is ready for shipment"
    gets
    # verify in sv and av
    $log.info 'verifying lot script parameters ShipComment (sv) and FabQualityCheck (av)'
    assert @@sv.user_parameter('Lot', lot, name: 'ShipComment').valueflag, '(sv) ShipComment not set'
    assert @@av.user_parameter('Lot', lot, name: 'FabQualityCheck').valueflag, '(av) FabQualityCheck not set'
  end
  
  def test233_FinalStd_withav_L  # L: Bump only
    # create lot and process it through BTF with AsmView tracking, final PDs are (sv) LOTSHIPDESIG.01, (av) LOTSHIPD.1
    assert lot = @@svtest.new_lot(@@newlot_params[:withav_l]), 'error creating test lot'
    @@testlots << lot
    assert @@tktest.start_btf(lot), 'error starting test lot'
    assert @@tktest.process_btf(lot), 'error processing lot'
    @@sv.lot_info(lot, wafers: true).wafers.each {|w| 
      assert_equal 0, @@sv.user_parameter_change('Wafer', w.wafer, 'OQAGoodDie', 100, 1, datatype: 'INTEGER')
    }
    #
    puts "\n\nLot #{lot} is ready for shipment"
    gets
    # verify in sv and av
    $log.info 'verifying lot script parameters ShipComment (sv) and FabQualityCheck (av)'
    assert @@sv.user_parameter('Lot', lot, name: 'ShipComment').valueflag, '(sv) ShipComment not set'
    assert @@av.user_parameter('Lot', lot, name: 'FabQualityCheck').valueflag, '(av) FabQualityCheck not set'
  end
  

  # # F8 lots (with BackupOperation and ERP->AsmView), redundant

  # def test241_FinalStd_withav_J_F8_BOP  # same as test221_FinalStd_withav_J, except lot is received via BOP from F8
  #   # create test lot at src site and send it to backup (incl. FabGUI receiving, set lot script params)
  #   assert lot = @@svtest_src.new_lot(@@newlot_params_f8[:withav_j_f8]), 'error creating src fab lot'
  #   @@testlots_bop << lot
  #   assert @@sv_src.backup_prepare_send_receive(lot, @@sv, @@psend_rcv), 'error receiving backup lot'
  #   uparams = {
  #     'ShipStorageExpiration'=>'20300303.235900',
  #     'QualityGateApproval'=>'Approved,qauser',
  #     'CountryOfOrigin'=>'US', 
  #     # 'SourceFabCode'=>'F8',  # TODO: AsmView HOLD when set, AsmTurnkey tries F1 AsnReceiver!
  #     'ERPItem'=>@@sv_src.user_parameter('Lot', lot, name: 'ERPItem').value
  #   }.merge(@@newlot_params_f8[:withav_j_f8][:user_parameters])
  #   assert_equal 0, @@sv.user_parameter_change('Lot', lot, uparams), 'error setting lot script params'
  #   # process lot through BTF with AsmView tracking
  #   assert @@tktest.start_btf(lot), 'error starting test lot, MQ issues?'
  #   assert @@tktest.process_btf(lot), 'error processing lot, setup error?'
  #   # lot is at (sv) LOTSHIPDESIG.01 or LOTSHIP-F8TK.01, (av) TKYSORT.1, set TestDB data and DieCountGood (defaults to 100 for all wafers)
  #   assert @@dbbtf.set_lot_wafer_bins(lot), 'error setting binning data'
  #   #
  #   puts "\n\nLot #{lot} is ready for shipment"
  #   gets
  #   # verify in sv and av
  #   $log.info 'verifying lot script parameters ShipComment (sv) and FabQualityCheck (av)'
  #   assert @@sv.user_parameter('Lot', lot, name: 'ShipComment').valueflag, '(sv) ShipComment not set'
  #   assert @@av.user_parameter('Lot', lot, name: 'FabQualityCheck').valueflag, '(av) FabQualityCheck not set'
  # end
  
  # def test242_FinalStd_withav_S_F8  # same as test231_FinalStd_withav_S, except lot is received via BOP from F8
  #   # create test lot at src site and send it to backup (incl. FabGUI receiving, set lot script params)
  #   assert lot = @@svtest_src.new_lot(@@newlot_params_f8[:withav_s_f8]), 'error creating src fab lot'
  #   @@testlots_bop << lot
  #   assert @@sv_src.backup_prepare_send_receive(lot, @@sv, @@psend_rcv), 'error receiving backup lot'
  #   uparams = {
  #     'ShipStorageExpiration'=>'20300303.235900',
  #     'QualityGateApproval'=>'Approved,qauser',
  #     'CountryOfOrigin'=>'US', 
  #     # 'SourceFabCode'=>'F8',  # TODO: AsmView HOLD when set, AsmTurnkey tries F1 AsnReceiver!
  #     'ERPItem'=>@@sv_src.user_parameter('Lot', lot, name: 'ERPItem').value
  #   }.merge(@@newlot_params_f8[:withav_s_f8][:user_parameters])
  #   assert_equal 0, @@sv.user_parameter_change('Lot', lot, uparams), 'error setting lot script params'
  #   # process lot through BTF with AsmView tracking
  #   assert @@tktest.start_btf(lot), 'error starting test lot, MQ issues?'
  #   assert @@tktest.process_btf(lot), 'error processing lot, setup error?'
  #   # lot is at (sv) LOTSHIPDESIG.01 or LOTSHIP-F8TK.01, (av) TKYSORT.1, set TestDB data and DieCountGood (defaults to 100 for all wafers)
  #   assert @@dbbtf.set_lot_wafer_bins(lot), 'error setting binning data'
  #   #
  #   puts "\n\nLot #{lot} is ready for shipment"
  #   gets
  #   # verify in sv and av
  #   $log.info 'verifying lot script parameters ShipComment (sv) and FabQualityCheck (av)'
  #   assert @@sv.user_parameter('Lot', lot, name: 'ShipComment').valueflag, '(sv) ShipComment not set'
  #   assert @@av.user_parameter('Lot', lot, name: 'FabQualityCheck').valueflag, '(av) FabQualityCheck not set'
  # end
  
  # def test243_FinalStd_withav_L_F8  # same as test233_FinalStd_withav_L, except lot is received via BOP from F8
  #   # create test lot at src site and send it to backup (incl. FabGUI receiving, set lot script params)
  #   assert lot = @@svtest_src.new_lot(@@newlot_params_f8[:withav_l_f8]), 'error creating src fab lot'
  #   @@testlots_bop << lot
  #   assert @@sv_src.backup_prepare_send_receive(lot, @@sv, @@psend_rcv), 'error receiving backup lot'
  #   uparams = {
  #     'ShipStorageExpiration'=>'20300303.235900',
  #     'QualityGateApproval'=>'Approved,qauser',
  #     'CountryOfOrigin'=>'US', 
  #     # 'SourceFabCode'=>'F8',  # TODO: AsmView HOLD when set, AsmTurnkey tries F1 AsnReceiver!
  #     'ERPItem'=>@@sv_src.user_parameter('Lot', lot, name: 'ERPItem').value
  #   }.merge(@@newlot_params_f8[:withav_l_f8][:user_parameters])
  #   assert_equal 0, @@sv.user_parameter_change('Lot', lot, uparams), 'error setting lot script params'
  #   # process lot through BTF with AsmView tracking
  #   assert @@tktest.start_btf(lot), 'error starting test lot, MQ issues?'
  #   assert @@tktest.process_btf(lot), 'error processing lot, setup error?'  # TODO: AsmView move errors (route cache?)
  #   # lot is at (sv) LOTSHIPDESIG.01, (av) LOTSHIPD.1
  #   @@sv.lot_info(lot, wafers: true).wafers.each {|w| 
  #     assert_equal 0, @@sv.user_parameter_change('Wafer', w.wafer, 'OQAGoodDie', 100, 1, datatype: 'INTEGER')
  #   }
  #   #
  #   puts "\n\nLot #{lot} is ready for shipment"
  #   gets
  #   # verify in sv and av
  #   $log.info 'verifying lot script parameters ShipComment (sv) and FabQualityCheck (av)'
  #   assert @@sv.user_parameter('Lot', lot, name: 'ShipComment').valueflag, '(sv) ShipComment not set'
  #   assert @@av.user_parameter('Lot', lot, name: 'FabQualityCheck').valueflag, '(av) FabQualityCheck not set'
  # end
  
  # def test244_FinalStd_withav_R_F8  # like J_F8
  #   # create test lot at src site and send it to backup (incl. FabGUI receiving, set lot script params)
  #   assert lot = @@svtest_src.new_lot(@@newlot_params_f8[:withav_r_f8]), 'error creating src fab lot'
  #   @@testlots_bop << lot
  #   assert @@sv_src.backup_prepare_send_receive(lot, @@sv, @@psend_rcv), 'error receiving backup lot'
  #   uparams = {
  #     'ShipStorageExpiration'=>'20300303.235900',
  #     'QualityGateApproval'=>'Approved,qauser',
  #     'CountryOfOrigin'=>'US', 
  #     # 'SourceFabCode'=>'F8',  # TODO: AsmView HOLD when set, AsmTurnkey tries F1 AsnReceiver!
  #     'ERPItem'=>@@sv_src.user_parameter('Lot', lot, name: 'ERPItem').value
  #   }.merge(@@newlot_params_f8[:withav_r_f8][:user_parameters])
  #   assert_equal 0, @@sv.user_parameter_change('Lot', lot, uparams), 'error setting lot script params'
  #   # process lot through BTF with AsmView tracking
  #   assert @@tktest.start_btf(lot), 'error starting test lot, MQ issues?'
  #   assert @@tktest.process_btf(lot), 'error processing lot, setup error?'
  #   # lot is at (sv) LOTSHIPDESIG.01 or LOTSHIP-F8TK.01, (av) TKYSORT.1, set TestDB data and DieCountGood (defaults to 100 for all wafers)
  #   assert @@dbbtf.set_lot_wafer_bins(lot), 'error setting binning data'
  #   #
  #   puts "\n\nLot #{lot} is ready for shipment"
  #   gets
  #   # verify in sv and av
  #   $log.info 'verifying lot script parameters ShipComment (sv) and FabQualityCheck (av)'
  #   assert @@sv.user_parameter('Lot', lot, name: 'ShipComment').valueflag, '(sv) ShipComment not set'
  #   assert @@av.user_parameter('Lot', lot, name: 'FabQualityCheck').valueflag, '(av) FabQualityCheck not set'
  # end
  
  
  ### Subcon Shipment (x -> Fab1 -> OSAT)

  def test311_Subcon_trans_bump_R_F8 # Subcon_trans_bump via AsmView tracking (SourceFabCode = F8)
    # create test lot at src site
    assert lot = @@svtest_src.new_lot(@@newlot_params_f8[:withav_r_f8_BUMP]), 'error creating src fab lot'
    @@testlots_bop << lot
    # create lot with the BUMP product and process it through BTF
    wids = @@sv_src.lot_info(lot, wafers: true).wafers.collect {|w| w.wafer}
    assert @@svtest.new_lot(@@newlot_params[:transbump].merge(lot: lot, waferids: wids)), 'error creating lot'
    @@testlots << lot
    av_gid = 'LOT_LABEL'
    assert @@tktest.start_btf(lot, stages: @@tktest.stages['L'], av_gid: av_gid), 'error starting test lot'
    assert @@tktest.process_btf(lot, stages:  @@tktest.stages['L'], av_gid: av_gid), 'error processing lot'  
    # TODO: currently fails 'Specified wafer WWJY5NJOI601 already exists in the lot VU00L14.00.'
    # TODO: test preparation

  end
  
  def XXtest311_Subcon_trans_bump_R_F8 # Subcon_trans_bump via AsmView tracking (SourceFabCode = F8)
    # # create Fab lot
    # assert lot = @@svtest.new_lot(@@newlot_params[:transbump]), 'error creating Fab lot'
    # @@testlots << lot
    # # create BUMP lot and ensure it exists in AsmView
    # assert @@svtest.restb_lot(lot, @@sv_bump)
    #
    # create lot with the BUMP product and process it through BTF with AmView tracking and set DieCountGood
    assert @@svtest.new_lot(@@newlot_params[:transbump].merge(lot: lot)), 'error creating lot'
    @@testlots << lot
    av_gid = 'LOT_LABEL'
    assert @@tktest.start_btf(lot, stages: @@tktest.stages['L'], av_gid: av_gid), 'error starting test lot'
    assert @@tktest.process_btf(lot, stages:  @@tktest.stages['L'], av_gid: av_gid), 'error processing lot'  
    # lot is before (sv) LOTSHIP-BUMP.01, (av) TKYTRANS.1
    assert_equal 0, @@sv.lot_opelocate(lot, nil, op: 'LOTSHIP-BUMP.01'), 'error locating lot'
    @@sv.lot_info(lot, wafers: true).wafers.each {|w| 
      assert_equal 0, @@sv.user_parameter_change('Wafer', w.wafer, 'DieCountGood', 100)
    }
    #
    puts "\n\nLot #{lot} is ready for subcon shipment"
    gets
    # verify in sv and av
    $log.info 'verifying lot script parameters ShipComment (sv)'
    assert @@sv.user_parameter('Lot', lot, name: 'ShipComment').valueflag, '(sv) script parameter ShipComment not set'
    avlot = av_gid == 'LOT_LABEL' ? @@sv.lot_label(lot) : lot
    assert_equal 'TKYTRANS.1',  @@av.lot_info(avlot).op, '(av) wrong op'
  end
  
  def test331_Subcon_noav_J_bump
    # create lot, locate it to TKEYBUMPSHIPDISPO and set OQAGoodDie
    assert lot = @@svtest.new_lot(@@newlot_params[:noav_j_bump]), 'error creating Fab lot'
    @@testlots << lot
    assert_equal 0, @@sv.lot_opelocate(lot, nil, op: 'TKEYBUMPSHIPDISPO.01')
    @@sv.lot_info(lot, wafers: true).wafers.each {|w| 
      assert_equal 0, @@sv.user_parameter_change('Wafer', w.wafer, 'OQAGoodDie', 100, 1, datatype: 'INTEGER')
    }
    #
    puts "\n\nLot #{lot} is ready for shipment"
    gets
    # verify in sv
    $log.info 'verifying lot script parameters ShipComment and ShipToFirm'
    assert @@sv.user_parameter('Lot', lot, name: 'ShipComment').valueflag, 'script parameter ShipComment not set'
    subcon = @@newlot_params[:noav_j_bump][:user_parameters]['TurnkeySubconIDBump']
    assert_equal subcon, @@sv.user_parameter('Lot', lot, name: 'ShipToFirm').value, 'wrong script parameter'
  end
  
  def test341_Subcon_noav_J_sort
    # create lot, locate it to TKEYSORTSHIPDISPO and set DieCountGood
    assert lot = @@svtest.new_lot(@@newlot_params[:noav_j_sort]), 'error creating Fab lot'
    @@testlots << lot
    assert_equal 0, @@sv.lot_opelocate(lot, nil, op: 'TKEYSORTSHIPDISPO.01')
    @@sv.lot_info(lot, wafers: true).wafers.each {|w| 
      assert_equal 0, @@sv.user_parameter_change('Wafer', w.wafer, 'DieCountGood', 100, 1, datatype: 'INTEGER')
    }
    #
    puts "\n\nLot #{lot} is ready for shipment"
    gets
    # verify in sv
    $log.info 'verifying lot script parameters ShipComment and ShipToFirm'
    assert @@sv.user_parameter('Lot', lot, name: 'ShipComment').valueflag, 'script parameter ShipComment not set'
    subcon = @@newlot_params[:noav_j_sort][:user_parameters]['TurnkeySubconIDSort']
    assert_equal subcon, @@sv.user_parameter('Lot', lot, name: 'ShipToFirm').value, 'wrong script parameter'
  end
  
  
  ### Sales Order Shipment
  
  def testx41_salesorder_noav # --> TODO: under development
    # create lots, locate it to LOTSHIPDESIG and set OQAGoodDie, customer must match ERPSalesOrder
    assert lotsso = @@svtest.new_lots(2, @@newlot_params[:ordernoav].merge(customer: 'gshuttle')), 'error creating Fab lots'
    @@testlots += lotsso
    # assert_equal 0, @@sv.lot_mfgorder_change(lotsso, customer: 'gshuttle') # Customer have to match to used ERPSalesOrder
    lotsso.each {|lot| 
      assert_equal 0, @@sv.lot_opelocate(lot, nil, op: 'LOTSHIPDESIG.01')
      # wafer script parameter only for warning pop-up to'DieCount' check with confirm="yes"      
      @@sv.lot_info(lot, wafers: true).wafers.each {|w| 
        assert_equal 0, @@sv.user_parameter_change('Wafer', w.wafer, 'OQAGoodDie', 100, 1, datatype: 'INTEGER')
      }
    }
    #
    puts "\n\nLot #{lotsso} is ready for 'Sales Order Shipment'"
    gets
  end


  # aux methods  TODO: useful?

  # def av_moveto_TKYSORT(lot)
  #   # branch cancel
  #   # on route TURNKEY.2 locate to op SORTDISP.1 and branch to SORT-A.2, locate to op TKYSORT.1   # TKtype J
  #   # wait and remove av and sv holds
  # end

end