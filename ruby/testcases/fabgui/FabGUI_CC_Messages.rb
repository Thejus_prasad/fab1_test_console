=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: Steffen Steidten, 2011-07-21

History:
  2014-07-25 dsteger,  added test24 for FabGUI 11.7.5 bug  (now testdev94)
  2018-07-18 sfrieske, cleanup and minor fixes
  2019-07-02 sfrieske, added option to run with the 'new' interface
  2020-06-08 sfrieske, added test33 for number of ToolLog messages shown
  2021-08-26 sfrieske, removed dependency on YAML and misc
  2021-10-20 aschmid3, removed/deactivated test33 due to outdated FabGUI interface 'processLogToolLog' (w/ FabP-V32.0.4)
  2021-10-28 aschmid3, adjusted test23 and test33 to new FabGUI interface 'handleMessage'

Notes:
  test new interface: runtest 'FabGUI_CC_Messages', 'itdc', parameters: {newinterface: true}
=end

require 'misc/fabguimsg'
require 'util/statistics'
require 'util/uniquestring'
require 'RubyTestCase'


class FabGUI_CC_Messages < RubyTestCase
  @@tools = %w(LETCL1000 LETCL1001 LETCL1002 LETCL1003 LETCL1004)
  @@session = 'All'  #'SDRSXAD02-3'   #"QATEST-#{Time.now.to_i}"
  @@msg1 = 'QA: 60 s timeout test, no special chars. Do not press any button!'

  @@loops = 10  # 20 gives stuck messages in test92
  @@newinterface = false
  @@results = {}

  def self.startup
    super(nosiview: true)
  end


  def test00_setup
    $setup_ok = false
    #
    @@eqp = @@tools.first  # default tool for msg tests
    @@clients = @@tools.collect {|t| @@newinterface ? FabGUI::Client2.new($env) : FabGUI::Client.new($env)}
    @@client = @@clients.first
    $log.info "using new interface: #{@@newinterface}"
    # clean up the queue
    refute_nil @@client.mq.mq_response.delete_msgs(nil)
    #
    $setup_ok = true
  end

  def test21_view_simple
    assert_equal 'TOOL.TIMEOUT', @@client.simple_query(@@eqp, @@msg1), 'no response'
    $log.info "Check Status screen for #{@@eqp} and hit OK within 60 s!"
    msg = "QATest #{Time.now.utc.strftime('%F_%T')} \xc3\xa4 QAQA"
    assert @@client.simple_query(@@eqp, msg), 'wrong response'
  end

  # Checking special characters in FabGUI messages
  def test22_view_extended
    assert @@client.extended_query(@@eqp, @@msg1), 'no response'
    $log.info "Check Status screen for #{@@eqp} and hit OK within 60 s!"
    msg = "QATest #{Time.now.utc.strftime('%F_%T')} \xc3\xa4 QAQA"
    assert @@client.extended_query(@@eqp, msg), 'wrong response'
  end

  def test23_tool_log
    skip "not applicable for new interface" if @@newinterface
    assert @@client.tool_log_handle_message(@@eqp, @@msg1), 'no response'
    $log.info "Check Screen EI Status -> ToolLog for #{@@eqp}!"
    msg = "QATest #{Time.now.utc.strftime('%F_%T')} äös QAQA"
    assert @@client.tool_log_handle_message(@@eqp, msg), 'wrong response'
  end

  def test24_long_msg
    $log.info "Check status screen for #{@@eqp} and hit OK within 60 s!"
    msg = "QATest #{Time.now.utc.strftime('%F_%T')} QA " + "0123456789\n" * 199 + "'abc'cbs'asdv'"
    assert @@client.simple_query(@@eqp, msg), 'long msg error'
  end

  def test31_msg_timeout
    $log.info "Check status screen for #{@@eqp}. Verify there is exacly 1 tab open during the next 12 minutes and that it's closed automatically."
    msg = "QATest #{Time.now.utc.strftime('%F_%T')}, 1 tab for 12 minutes!"
    @@client.extended_query(@@eqp, msg, msgtimeout: 12 * 60, timeout: 1)
    $log.info "--> Press Enter to continue after 13 minutes!"
    gets
  end

  def test33_tool_log_limit
    skip "not applicable for new interface" if @@newinterface
    # 1 message, must go away when all others are sent
    assert @@client.tool_log_handle_message(@@eqp, @@msg1), 'no response'
    # another 200 messages, must stay  (per CR85660: increased from 50 to 200)
    prefix = unique_string
    n = 200            # per CR85660: increased from 50 to 200
    n.times {|i|
      msg = "#{prefix} - #{i}: QATest #{Time.now.utc.strftime('%F_%T')}"
      assert @@client.tool_log_handle_message(@@eqp, msg), 'wrong response'
    }
    $log.info "Check EI Status -> ToolLog for #{@@eqp}. Exactly #{n} messages must appear."
  end

  # Perf tests

  def test91_stress_eiping
    @@results = dcmsg_stress(@@clients, @@tools, loops: @@loops) {|cl, tool|
      cl.eiping(tool)
    }
  end

  def test92_stress_handle_message
    skip "not applicable for new interface" if @@newinterface
    @@results = dcmsg_stress(@@clients, @@tools, loops: @@loops) {|cl, tool|
      cl.handle_message(tool, session: @@session, msgtimeout: 10, popup: false)
    }
    assert @@results[:ok], 'FabGUI comm failure'
  end

  def testdev94_msg_queuing
    loops = 2 * 250 + 3  # 2 server a mq limit 250 and 3
    msg = "QA" + Time.now.to_i.to_s.reverse
    # $log.info("grep for #{msg}")
    threads = loops.times.collect {
      Thread.new {
        @@client.extended_query(@@tools.sample, msg, timeout: 190, msgtimeout: 180)
      }
    }
    30.times {
      $log.info("Q size: #{fg.mq_request.qdepth}")
      sleep 60
    }
    threads.each{|t| t.join}
    $log.info "Search for #{msg.inspect}!"
  end

end


def dcmsg_stress(clients, tools, params={}, &blk)
  # run the stress test and return performance stats
  loops = params[:loops] || 1
  res = []
  threads = []
  tstart = Time.now
  clients.each_with_index {|cl, i|
    #sleep 0.2 if threads.size > 0
    threads << Thread.new {
      loops.to_i.times {|r|
        sleep 0.1 if r > 0
        ok = blk.call(cl, tools[i])
        res << (ok ? cl.mq.mq_response.duration : nil)
      }
    }
  }
  threads.each_with_index {|t, i|
    $log.info "joining thread #{i}"
    t.join
  }
  stats = {tstart: tstart.clone, duration: Time.now - tstart, loops: loops,
    ntools: clients.size, responsetime: res.clone, ok: !res.member?(nil)}
  #
  show_dcmsg_stats(stats)
  #
  return stats
end

def show_dcmsg_stats(stats)
  # display the results of a stress test
  res = stats[:responsetime]
  r = res.compact
  sdata = Statistics.calc(r)
  puts "FabGUI DC Message Stress Test from #{stats[:tstart].strftime('%F_%T')}"
  puts '=' * 60
  puts "test duration:         #{stats[:duration]} s"
  puts "number of tools:       #{stats[:ntools]}, #{stats[:loops]} loops"
  puts 'number of FabGUI calls: %d  (%.1f per hour)' % [r.size, r.size / stats[:duration] * 3600]
  puts 'average call duration : %.1f s +- %.1f s (%d%%)' % [sdata[:average], sdata[:stddev], sdata[:stddev_pct]]
  puts
  puts
end
