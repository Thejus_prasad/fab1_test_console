=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: sfrieske, 2018-04-27
=end

require 'misc/ocap'
require 'RubyTestCase'


# Create data to test FabGUI ControlCenter Alarms
class FabGUI_CC_Alarms < RubyTestCase
  @@eqps = ['HYB171', 'HYB172']


  def test00
    @@clients = @@eqps.collect {|eqp| OCAP::Alarm.new($env, eqp)}
  end

  def test01
    @@clients.each {|client|
      5.times {|i| assert client.submit(i + 1)}
    }
  end

end
