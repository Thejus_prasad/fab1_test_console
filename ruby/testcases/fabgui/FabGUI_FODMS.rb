=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: sfrieske, 2018-04-27
=end

require 'misc/fodms'
require 'SiViewTestCase'


# Test FabGUI FODMS server, create test data for FOD-MS GUI tests
class FabGUI_FODMS < SiViewTestCase
  @@sv_defaults = {product: 'TKEY01.A0', route: 'C02-TKEY.01'}  # route must have a PD FAB1OUT.01

  @@testlots = []


  def self.after_all_passed
    @@testlots.each {|lot| @@sv.delete_lot_family(lot)}
  end


  def test00_setup
    @@fodtest = FODMS::Test.new($env, sv: @@sv)
  end

  def test11
    assert lot = @@svtest.new_lot, 'error creating test lot'
    @@testlots << lot
    assert @@fodtest.verify_lot_fod(lot), 'FOD not set correctly'
    #
    puts "Lot #{lot} is ready for the manual tests. Press ENTER to clean up!"
    gets
  end

end
