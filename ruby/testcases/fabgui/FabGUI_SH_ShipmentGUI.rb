=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author:   aschmid3, 2021-07-27

Version: 32.0.1 

History:
  2021-07-27 aschmid3, init version

Notes:
  xxx

=end

require 'SiViewTestCase'


# Create lots to test FabGUI - Shipping - GUIs of 'Shipment' and 'Sales Order Shipment'
class FabGUI_SH_ShipmentGUI < SiViewTestCase

  # lot settings
  # standard customer = "gf" 
  @@sv_defaults = {carrier_category: nil, carriers: '%'}
  @@newlot_params = {
    ## Ship Exp(iration) Date
    #
    ship_exp_date: {product: 'SHELBY-TKBTF.QA', route: 'UTRT-TKBTF.01', sublottype: "PW", customer: 'gshuttle', 
                                                                        # SLT only used through carrier check
      user_parameters: {'ERPItem'=>'*SHELBY21AE-M00-JB4', 'ERPSalesOrder'=>'6010007260:2:1'}  # 'Order Number' = 6010007260:2:1 (FabGUI)
    },
    #
    #
    ## Check of 'Multiple lots in one carrier' - MSR1778290
    #
    # no defined SLT at config
    nond_slt: {product: 'UT-TKBTF.01', route: 'UTRT-TKBTF.01', sublottype: "PW", user_parameters: {}},
    #
    # defined SLT at config
    def_slt: {product: 'UT-TKBTF.01', route: 'UTRT-TKBTF.01', sublottype: "PO", user_parameters: {}},
    
    # Subcon (trans) shipment
    # --> successful shipment is depending on put Lot.SP TkSubconIDBump and known ERPItem at ERP system otherwise error in FabGUI
    #
    # no defined SLT at config
    nond_slt_subcon: {product: 'UT-TKFAB.01', route: 'UTRT-TKFAB.01', sublottype: "PW", 
      user_parameters: {'ERPItem'=>'*SHELBY21AE-M00-JB4', 'TurnkeySubconIDBump'=>'ST'}
    },
    #
    # defined SLT at config
    def_slt_subcon: {product: 'UT-TKFAB.01', route: 'UTRT-TKFAB.01', sublottype: "PO", 
      user_parameters: {'ERPItem'=>'*SHELBY21AE-M00-JB4', 'TurnkeySubconIDBump'=>'ST'}
    },
    
    # NonStandard shipment
    # defined SLT at config
    def_slt_nonstd: {product: 'UT-TKBTF.01', route: 'UTRT-TKBTF.01', sublottype: "PO", user_parameters: {}},
    
    # Sales Order shipment
    # customer + 'ERPItem' have to match to used 'Order Number' (FabGUI) resp. 'ERPSalesOrder' (Lot.ScriptParameter)
    # no defined SLT at config
    nond_slt_order: {product: 'UT-TKBTF.01', route: 'UTRT-TKBTF.01', sublottype: "PW", customer: 'gshuttle',
      user_parameters: {'ERPItem'=>'MPW0350-A1FAAU-UAA01'}  # 'Order Number' = 6010007260:2:1 (FabGUI)
    },
    #
    # defined SLT at config
    def_slt_order: {product: 'UT-TKBTF.01', route: 'UTRT-TKBTF.01', sublottype: "PO", customer: 'gshuttle', 
      user_parameters: {'ERPItem'=>'MPW0350-A1FAAU-UAA01'}  # 'Order Number' = 6010007260:2:1 (FabGUI)
    },
  }

  @@testlots = []


  def self.after_all_passed
    @@testlots.each {|lot| @@sv.delete_lot_family(lot)}
  end

  def test00_setup
    $setup_ok = false
    #
    # Lot.SP only necessary for shipment screen otherwise error msg appears
    @@newlot_params.each_value {|v|
      v[:user_parameters]['ShipStorageExpiration'] = '20300303.235900'
      v[:user_parameters]['QualityGateApproval'] = 'Approved,qauser'
    }
    #
    $setup_ok = true
  end

  ## Ship Exp(iration) Date
  #
  # Valid for screens of Shipment and Sales Order Shipment
  
  def testman111_Ship_Exp_Date
    # create a lot family
    assert lot = @@svtest.new_lot(@@newlot_params[:ship_exp_date]), 'error creating Fab lot'
    @@testlots << lot
    #
    # child lot with future date
    assert cl1 = @@sv.lot_split(lot, 1)
    @@testlots << cl1
    assert_equal 0, @@sv.lot_opelocate(cl1, nil, op: 'LOTSHIPDESIG.01')
    # child lot with date in the past
    assert cl2 = @@sv.lot_split(lot, 1)
    @@testlots << cl2
    assert_equal 0, @@sv.lot_opelocate(cl2, nil, op: 'LOTSHIPDESIG.01')
    assert_equal 0, @@sv.user_parameter_change('Lot', cl2, {'ShipStorageExpiration'=>'20200411.235900'})
    # child lots without put date value --> empty and ScriptParamter deleted
    assert cl3 = @@sv.lot_split(lot, 1)
    @@testlots << cl3
    assert_equal 0, @@sv.lot_opelocate(cl3, nil, op: 'LOTSHIPDESIG.01')
    assert_equal 0, @@sv.user_parameter_change('Lot', cl3, {'ShipStorageExpiration'=>''})
    assert cl4 = @@sv.lot_split(lot, 1)
    @@testlots << cl4
    assert_equal 0, @@sv.lot_opelocate(cl4, nil, op: 'LOTSHIPDESIG.01')
    assert_equal 0, @@sv.user_parameter_delete('Lot', cl4, 'ShipStorageExpiration')
    # child lot with an invalid (date) value
    assert cl5 = @@sv.lot_split(lot, 1)
    @@testlots << cl5
    assert_equal 0, @@sv.lot_opelocate(cl5, nil, op: 'LOTSHIPDESIG.01')
    assert_equal 0, @@sv.user_parameter_change('Lot', cl5, {'ShipStorageExpiration'=>'20300303'})
    #
    puts "\n\nLots are ready for verification:"
    puts "\n- Child lot #{cl1} --> with future date"
    puts "\n- Child lot #{cl2} --> with date in the past"
    puts "\n- Child lot #{cl3} --> without put date value"
    puts "\n- Child lot #{cl4} --> without existing script parameter 'ShipStorageExpiration'"
    puts "\n- Child lot #{cl5} --> with an invalid (date) value"
    #
    puts "\nPress ENTER to continue when done with the tests."
    gets
  end

  ## Check of 'Multiple lots in one carrier' - MSR1778290
  #
  # Shipment - Shipping Overview screen

  def test211_non_def_SLT_nLots    # Used no defined sublottype from config
    # create a lot, locate it to LOTSHIPDESIG and set OQAGoodDie (Wafer.SP only necessary that no error msg appears)
    assert lot = @@svtest.new_lot(@@newlot_params[:nond_slt]), 'error creating Fab lot'
    @@testlots << lot
    assert_equal 0, @@sv.lot_opelocate(lot, nil, op: 'LOTSHIPDESIG.01')
    @@sv.lot_info(lot, wafers: true).wafers.each {|w| 
      assert_equal 0, @@sv.user_parameter_change('Wafer', w.wafer, 'OQAGoodDie', 100, 1, datatype: 'INTEGER')
    }
    # split out a child lot for multiple lot IDs in one carrier
    assert cl1 = @@sv.lot_split(lot, 1)
    @@testlots << cl1
    #
    puts "\n\nLots #{lot} and #{cl1} are ready for verification to 'Lot Count Carrier Validation'."
    #
    puts "\n\nPress ENTER to continue when done with the test."
    gets
  end

  def test212_def_SLT_1Lot  # Used defined sublottype from config
    # create a lot, locate it to LOTSHIPDESIG and set OQAGoodDie (Wafer.SP only necessary that no error msg appears)
    assert lot = @@svtest.new_lot(@@newlot_params[:def_slt]), 'error creating Fab lot'
    @@testlots << lot
    assert_equal 0, @@sv.lot_opelocate(lot, nil, op: 'LOTSHIPDESIG.01')
    @@sv.lot_info(lot, wafers: true).wafers.each {|w| 
      assert_equal 0, @@sv.user_parameter_change('Wafer', w.wafer, 'OQAGoodDie', 100, 1, datatype: 'INTEGER')
    }
    #
    puts "\n\nLot #{lot} is ready for verification to 'Lot Count Carrier Validation'."
    #
    puts "\n\nPress ENTER to continue when done with the test."
    gets
  end
  
  def test213_def_SLT_nLots    # Used defined sublottype from config
    # create a lot, locate it to LOTSHIPDESIG and set OQAGoodDie (Wafer.SP only necessary that no error msg appears)
    assert lot = @@svtest.new_lot(@@newlot_params[:def_slt]), 'error creating Fab lot'
    @@testlots << lot
    assert_equal 0, @@sv.lot_opelocate(lot, nil, op: 'LOTSHIPDESIG.01')
    @@sv.lot_info(lot, wafers: true).wafers.each {|w| 
      assert_equal 0, @@sv.user_parameter_change('Wafer', w.wafer, 'OQAGoodDie', 100, 1, datatype: 'INTEGER')
    }
    # split out a child lot for multiple lot IDs in one carrier
    assert cl1 = @@sv.lot_split(lot, 1)
    @@testlots << cl1
    #
    puts "\n\nLots #{lot} and #{cl1} are ready for verification to 'Lot Count Carrier Validation'."
    #
    puts "\n\nPress ENTER to continue when done with the test."
    gets
  end

  # Standard Shipment - popup Save Lot Data
  
  def test221_Std_non_def_SLT_nLots    # Used no defined sublottype from config
    # create a lot, locate it to LOTSHIPDESIG and set OQAGoodDie (Wafer.SP only necessary that no error msg appears)
    assert lot = @@svtest.new_lot(@@newlot_params[:nond_slt]), 'error creating Fab lot'
    @@testlots << lot
    assert_equal 0, @@sv.lot_opelocate(lot, nil, op: 'LOTSHIPDESIG.01')
    @@sv.lot_info(lot, wafers: true).wafers.each {|w| 
      assert_equal 0, @@sv.user_parameter_change('Wafer', w.wafer, 'OQAGoodDie', 100, 1, datatype: 'INTEGER')
    }
    # split out a child lot for multiple lot IDs in one carrier
    assert cl1 = @@sv.lot_split(lot, 1)
    @@testlots << cl1
    #
    puts "\n\nLots #{lot} and #{cl1} are ready for verification to 'Lot Count Carrier Validation'."
    #
    puts "\n\nPress ENTER to continue when done with the test."
    gets
  end
  
  def test222_Std_def_SLT_1Lot  # Used defined sublottype from config
    # create a lot, locate it to LOTSHIPDESIG and set OQAGoodDie (Wafer.SP only necessary that no error msg appears)
    assert lot = @@svtest.new_lot(@@newlot_params[:def_slt]), 'error creating Fab lot'
    @@testlots << lot
    assert_equal 0, @@sv.lot_opelocate(lot, nil, op: 'LOTSHIPDESIG.01')
    @@sv.lot_info(lot, wafers: true).wafers.each {|w| 
      assert_equal 0, @@sv.user_parameter_change('Wafer', w.wafer, 'OQAGoodDie', 100, 1, datatype: 'INTEGER')
    }
    #
    puts "\n\nLot #{lot} is ready for verification to 'Lot Count Carrier Validation'."
    #
    puts "\n\nPress ENTER to continue when done with the test."
    gets
  end

  def test223_Std_def_SLT_nLots    # Used defined sublottype from config
    # create a lot, locate it to LOTSHIPDESIG and set OQAGoodDie (Wafer.SP only necessary that no error msg appears)
    assert lot = @@svtest.new_lot(@@newlot_params[:def_slt]), 'error creating Fab lot'
    @@testlots << lot
    assert_equal 0, @@sv.lot_opelocate(lot, nil, op: 'LOTSHIPDESIG.01')
    @@sv.lot_info(lot, wafers: true).wafers.each {|w| 
      assert_equal 0, @@sv.user_parameter_change('Wafer', w.wafer, 'OQAGoodDie', 100, 1, datatype: 'INTEGER')
    }
    # split out a child lot for multiple lot IDs in one carrier
    assert cl1 = @@sv.lot_split(lot, 1)
    @@testlots << cl1
    #
    puts "\n\nLots #{lot} and #{cl1} are ready for verification to 'Lot Count Carrier Validation'."
    #
    puts "\n\nPress ENTER to continue when done with the test."
    gets
  end

  # Subcon (trans) Shipment - popup Save Lot Data

  def test231_Subcon_non_def_SLT_nLots    # Used no defined sublottype from config
    # create a lot, locate it to LOTSHIP-FAB and set OQAGoodDie (Wafer.SP only necessary that no error msg appears)
    assert lot = @@svtest.new_lot(@@newlot_params[:nond_slt_subcon]), 'error creating Fab lot'
    @@testlots << lot
    assert_equal 0, @@sv.lot_opelocate(lot, nil, op: 'LOTSHIP-FAB.01')
    @@sv.lot_info(lot, wafers: true).wafers.each {|w| 
      assert_equal 0, @@sv.user_parameter_change('Wafer', w.wafer, 'OQAGoodDie', 100, 1, datatype: 'INTEGER')
    }
    # split out a child lot for multiple lot IDs in one carrier
    assert cl1 = @@sv.lot_split(lot, 1)
    @@testlots << cl1
    #
    puts "\n\nLots #{lot} and #{cl1} are ready for verification to 'Lot Count Carrier Validation'."
    #
    puts "\n\nPress ENTER to continue when done with the test."
    gets
  end

  def test232_Subcon_def_SLT_1Lot  # Used defined sublottype from config
    # create a lot, locate it to LOTSHIP-FAB and set OQAGoodDie (Wafer.SP only necessary that no error msg appears)
    assert lot = @@svtest.new_lot(@@newlot_params[:def_slt_subcon]), 'error creating Fab lot'
    @@testlots << lot
    assert_equal 0, @@sv.lot_opelocate(lot, nil, op: 'LOTSHIP-FAB.01')
    @@sv.lot_info(lot, wafers: true).wafers.each {|w| 
      assert_equal 0, @@sv.user_parameter_change('Wafer', w.wafer, 'OQAGoodDie', 100, 1, datatype: 'INTEGER')
    }
    #
    puts "\n\nLot #{lot} is ready for verification to 'Lot Count Carrier Validation'."
    #
    puts "\n\nPress ENTER to continue when done with the test."
    gets
  end

  def test233_Subcon_def_SLT_nLots    # Used defined sublottype from config
    # create a lot, locate it to LOTSHIP-FAB and set OQAGoodDie (Wafer.SP only necessary that no error msg appears)
    assert lot = @@svtest.new_lot(@@newlot_params[:def_slt_subcon]), 'error creating Fab lot'
    @@testlots << lot
    assert_equal 0, @@sv.lot_opelocate(lot, nil, op: 'LOTSHIP-FAB.01')
    @@sv.lot_info(lot, wafers: true).wafers.each {|w| 
      assert_equal 0, @@sv.user_parameter_change('Wafer', w.wafer, 'OQAGoodDie', 100, 1, datatype: 'INTEGER')
    }
    # split out a child lot for multiple lot IDs in one carrier
    assert cl1 = @@sv.lot_split(lot, 1)
    @@testlots << cl1
    #
    puts "\n\nLots #{lot} and #{cl1} are ready for verification to 'Lot Count Carrier Validation'."
    #
    puts "\n\nPress ENTER to continue when done with the test."
    gets
  end

  # NonStandard Shipment - popup Save Lot Data

  def test241_NonStd_def_SLT_1Lot    # Used defined sublottype from config
    # create a lot, locate it to NICETAPI.01 and set OQAGoodDie (Wafer.SP only necessary that no error msg appears)
    assert lot = @@svtest.new_lot(@@newlot_params[:def_slt_nonstd]), 'error creating Fab lot'
    @@testlots << lot
    assert_equal 0, @@sv.lot_opelocate(lot, nil, op: 'NICETAPI.01')
    @@sv.lot_info(lot, wafers: true).wafers.each {|w| 
      assert_equal 0, @@sv.user_parameter_change('Wafer', w.wafer, 'OQAGoodDie', 100, 1, datatype: 'INTEGER')
    }
    #
    puts "\n\nLots #{lot} is ready for verification to 'Lot Count Carrier Validation'."
    #
    puts "\n\nPress ENTER to continue when done with the test."
    gets
  end

  # Sales Order Shipment - Sales Order Shipment Details screen and popup Save Lot Data

  def test251_SalesOrder_non_def_SLT_nLots    # Used no defined sublottype from config
    # create lots, locate they to LOTSHIPDESIG and set OQAGoodDie (Wafer.SP only necessary that no error msg appears), 
    # customer must match ERPSalesOrder
    assert lots = @@svtest.new_lots(3, @@newlot_params[:nond_slt_order]), 'error creating Fab lots'
    @@testlots += lots
    lots.each {|lot| 
      assert_equal 0, @@sv.lot_opelocate(lot, nil, op: 'LOTSHIPDESIG.01')
      # wafer script parameter only for warning pop-up to'DieCount' check with confirm="yes"      
      @@sv.lot_info(lot, wafers: true).wafers.each {|w| 
        assert_equal 0, @@sv.user_parameter_change('Wafer', w.wafer, 'OQAGoodDie', 100, 1, datatype: 'INTEGER')
      }
    }
    # split out child lots for multiple lot IDs in one carrier
    assert cl1 = @@sv.lot_split(lots[0], 1)
    @@testlots << cl1
    assert cl2 = @@sv.lot_split(lots[1], 1)
    @@testlots << cl2
    #
    puts "\n\nLots #{lots}, #{cl1} and #{cl2} are ready for verification to 'Lot Count Carrier Validation'."
    #
    puts "\n\nPress ENTER to continue when done with the test."
    gets
  end

  def test252_SalesOrder_def_SLT_1Lot  # Used defined sublottype from config
    # create lots, locate they to LOTSHIPDESIG and set OQAGoodDie (Wafer.SP only necessary that no error msg appears)
    # customer must match ERPSalesOrder
    assert lots = @@svtest.new_lots(3, @@newlot_params[:def_slt_order]), 'error creating Fab lots'
    @@testlots += lots
    lots.each {|lot| 
      assert_equal 0, @@sv.lot_opelocate(lot, nil, op: 'LOTSHIPDESIG.01')
      # wafer script parameter only for warning pop-up to'DieCount' check with confirm="yes"      
      @@sv.lot_info(lot, wafers: true).wafers.each {|w| 
        assert_equal 0, @@sv.user_parameter_change('Wafer', w.wafer, 'OQAGoodDie', 100, 1, datatype: 'INTEGER')
      }
    }
    #
    puts "\n\nLots #{lots} are ready for verification to 'Lot Count Carrier Validation'."
    #
    puts "\n\nPress ENTER to continue when done with the test."
    gets
  end

  def test253_SalesOrder_def_SLT_nLots    # Used defined sublottype from config
    # create lots, locate they to LOTSHIPDESIG and set OQAGoodDie (Wafer.SP only necessary that no error msg appears), 
    # customer must match ERPSalesOrder
    assert lots = @@svtest.new_lots(3, @@newlot_params[:def_slt_order]), 'error creating Fab lots'
    @@testlots += lots
    lots.each {|lot| 
      assert_equal 0, @@sv.lot_opelocate(lot, nil, op: 'LOTSHIPDESIG.01')
      # wafer script parameter only for warning pop-up to'DieCount' check with confirm="yes"      
      @@sv.lot_info(lot, wafers: true).wafers.each {|w| 
        assert_equal 0, @@sv.user_parameter_change('Wafer', w.wafer, 'OQAGoodDie', 100, 1, datatype: 'INTEGER')
      }
    }
    # split out child lots for multiple lot IDs in one carrier
    assert cl1 = @@sv.lot_split(lots[0], 1)
    @@testlots << cl1
    assert cl2 = @@sv.lot_split(lots[1], 1)
    @@testlots << cl2
    #
    puts "\n\nLots #{lots}, #{cl1} and #{cl2} are ready for verification to 'Lot Count Carrier Validation'."
    #
    puts "\n\nPress ENTER to continue when done with the test."
    gets
  end

  def test999
    puts "\n\nTesting done. Press ENTER to clean up all test lots!"
    gets
  end

end
