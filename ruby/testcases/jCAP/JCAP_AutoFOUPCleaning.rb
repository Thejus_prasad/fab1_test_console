=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: Steffen Steidten, 2013-04-29

Version: 18.09

History:
  2015-04-14 ssteidte, cleaned up and verified test cases
  2016-09-12 sfrieske, adapted for v16.09
  2017-01-26 sfrieske, added test for removal of UDATA FILTER_OPE_COUNT (MSR 1041115)
  2019-01-15 sfrieske, minor cleanup
  2019-04-18 sfrieske, replaced carrier_status.lots by lot_list_incassette for (hopefully) better reliability
  2021-08-27 sfrieske, require waitfor instead of misc
=end

require 'derdack'
require 'util/waitfor'
require 'SiViewTestCase'


class JCAP_AutoFOUPCleaning < SiViewTestCase
  @@category = 'UTCLEAN1'
  @@category2 = 'UTCLEAN2'
  @@product = 'UT-FOUP-CLEANING.UTCLEAN1'   #'ME-OC-FOUP-CLEANING.UTCLEAN1'
  @@product2 = 'UT-FOUP-CLEANING.UTCLEAN2'  #'ME-OC-FOUP-CLEANING.UTCLEAN2'
  @@route = 'UT-FOUP-CLEANING.01'  # 'ME-OX-U-FOUP-CLEANING.01'
  @@opNo_start = '1000.1000'       # 'ME-OX-CLEANSTART.01'
  @@opNo_premeas = '1000.1100'     # 'OX-FOUP-MEASUREMENT-PRE.01'
  @@opNo_cleaning = '1000.1200'    # 'ME-OX-FOUP-CLEANING.01'
  @@opNo_postmeas = '1000.1400'    # 'OX-FOUP-MEASUREMENT-POST.01', gatepass from conditioning PD to this one after cleaning

  @@lot_owner = 'X-JFPCLN'
  @@customer = 'gf'
  @@sublottype = 'VRT'

  @@max_carriers = 5  # see job properties
  @@lagtime = 60                # configured in SiView at the PD "ME-OX-FOUP-CONDITIONING.01"

  @@job_interval = 360          # lotprovider timer: 5+ min, include some slack time
  @@job_lotorphanremover = 220  # 3+ min
  @@job_lotremover = 120
  @@notification_params = {service: '%jCAP_AsyncNotification%', filter: {'Category'=>'AutoFoupCleaning'}}


  def self.after_all_passed
    @@sv.inhibit_entity(:route_opNo_product, [@@route, @@product], attrib: @@opNo_start)
    @@sv.inhibit_entity(:route_opNo_product, [@@route, @@product2], attrib: @@opNo_start)
    @@sv.released_lots('Z-FOUP-CLN').each {|e| @@sv.new_lot_release_cancel(e.lotID)}
    lots = @@sv.lot_list(lot: 'CLEAN-%')
    return if lots.empty?
    lots.each {|lot| @@sv.delete_lot_family(lot)}
    $log.info "trying to delete lots #{lots} in 40 s"; sleep 40
    @@sv.lot_delete(lots)
  end

  def setup
    # set inhibits to prevent execution until setup is complete
    assert @@sv.inhibit_entity(:route_opNo_product, [@@route, @@product], attrib: @@opNo_start)
    assert @@sv.inhibit_entity(:route_opNo_product, [@@route, @@product2], attrib: @@opNo_start)
    @@sv.released_lots('Z-FOUP-CLN').each {|e| assert_equal 0, @@sv.new_lot_release_cancel(e.lotID)}
    lots = @@sv.lot_list(lot: 'CLEAN-%')
    return if lots.empty?
    lots.each {|lot| @@sv.delete_lot_family(lot)}
    $log.info "trying to delete lots #{lots} in 40 s"; sleep 40
    @@sv.lot_delete(lots)
    # @@sv.lot_list(lot: 'CLEAN-%').each {|lot| assert_equal 0, @@sv.delete_lot_family(lot, delete: true)}
    assert_empty @@sv.lot_list(lot: 'CLEAN-%'), 'error deleting old CLEAN-% lots'
  end


  def test00_setup
    $setup_ok = false
    #
    assert @@sm.mainpd_change_lagtime(@@route, @@opNo_cleaning, @@lagtime / 60), 'SM error'
    @@notifications = DerdackEvents.new($env)
    #
    $setup_ok = true
  end

  def test11_lot_creation
    $setup_ok = false
    # prepare 3 carriers per category with status INUSE
    assert cc1 = prepare_carriers(@@category, 3), 'error preparing carriers'
    assert cc2 = prepare_carriers(@@category2, 3), 'error preparing carriers'
    $log.info "using carriers #{cc1} and #{cc2}"
    #
    # cancel inhibits and verify lot creation
    assert_equal 0, @@sv.inhibit_cancel(:route_opNo_product, [@@route, @@product])
    assert_equal 0, @@sv.inhibit_cancel(:route_opNo_product, [@@route, @@product2])
    $log.info "waiting up to #{@@job_interval} s for the jCAP job to run"
    carriers = cc1 + cc2
    assert wait_for(timeout: @@job_interval) {
      carriers.inject(true) {|ok, c| ok && !@@sv.lot_list_incassette(c).empty?}
      # (cc1 + cc2).inject(true) {|ok, c| ok && !@@sv.carrier_status(c).lots.empty?}
    }, 'error creating lots'
    #
    # verify lots
    ok = true
    cc1.each {|c| ok &= verify_lot_creation(c, @@product)}
    cc2.each {|c| ok &= verify_lot_creation(c, @@product2)}
    assert ok, 'error creating lots'
    #
    $setup_ok = true
  end

  def test12_throttling
    # set more than max_carriers to status INUSE
    assert cc1 = prepare_carriers(@@category, @@max_carriers + 1)
    $log.info "carriers INUSE: #{cc1}"
    #
    # cancel inhibits and verify creation of @@max_carriers lots
    assert_equal 0, @@sv.inhibit_cancel(:route_opNo_product, [@@route, @@product])
    $log.info "waiting up to #{@@job_interval} s for the jCAP job to run"
    assert wait_for(timeout: @@job_interval) {
      cc1.collect {|c|
        @@sv.lot_list_incassette(c)
        # cstat = @@sv.carrier_status(c)
        # cstat ? cstat.lots : []
      }.flatten.size == @@max_carriers
    }, 'error creating lots'
    #
    # locate the first lot to the cleaning PD and verify the process is continued
    # lot = @@sv.carrier_status(cc1.first).lots.first
    lot = @@sv.lot_list_incassette(cc1.first).first
    assert_equal 0, @@sv.lot_opelocate(lot, @@opNo_cleaning)
    $log.info "waiting up to #{@@job_interval} s for the jCAP job to run a 2nd time"
    assert wait_for(timeout: @@job_interval) {
      # get the number of all lots on the first 3 PDs of the carrier category UTCLEAN1
      lots = [@@opNo_start, @@opNo_premeas, @@opNo_cleaning].collect {|opNo| @@sv.lot_list(opNo: opNo, route: @@route)}.flatten
      lots2 = []
      lots.each {|lot|
        c = @@sv.lot_info(lot).carrier
        lots2 << lot if !c.empty? && @@sv.carrier_status(c).category == @@category
      }
      @@max_carriers == lots2.size
    }, 'wrong number of created lots'
  end

  def test13_category_control
    # prepare carriers with status INUSE
    assert cc1 = prepare_carriers(@@category, 2)
    $log.info "carriers INUSE: #{cc1}"
    # wait for the jCAP job and verify no lot has been created because the inhibit is still active
    $log.info "waiting #{@@job_interval} s for the jCAP job to run"; sleep @@job_interval
    lots = cc1.collect {|c|
      @@sv.lot_list_incassette(c)
      # @@sv.carrier_status(c).lots
    }.flatten
    assert_empty lots, "lots have been created despite of the inhibit: #{lots}"
  end

  def test14_happy  # happy scenario, incl. bankin and lot removal
    # set one carrier INUSE, add FILTER_OPE_COUNT to verify removal (MSR 1041115)
    assert carrier = prepare_carriers(@@category, 1).first
    assert_equal 0, @@sv.user_data_update(:carrier, carrier, {'FILTER_OPE_COUNT'=>'0'})
    # cancel inhibit, wait for the jCAP job and verify the lot creation
    assert_equal 0, @@sv.inhibit_cancel(:route_opNo_product, [@@route, @@product])
    $log.info "waiting up to #{@@job_interval} s for the jCAP job to run"
    assert wait_for(timeout: @@job_interval) {
      !@@sv.lot_list_incassette(carrier).empty?
      # !@@sv.carrier_status(carrier).lots.empty?
    }, 'no lots are created for carrier'
    assert verify_lot_creation(carrier, @@product)
    # process lot at the CLEAN pd and wait for cooldown (lagtime)
    # lot = @@sv.carrier_status(carrier).lots.first
    lot = @@sv.lot_list_incassette(carrier).first
    assert_equal 0, @@sv.lot_opelocate(lot, @@opNo_cleaning)
    assert @@sv.claim_process_lot(lot)
    $log.info "waiting #{@@lagtime} s for cooldown"; sleep @@lagtime
    assert wait_for(timeout: 60) {@@sv.lot_info(lot).status != 'ONHOLD'}, 'lot is still ONHOLD'
    # bankin lot
    sleep 10  # avoid "lot is in postprocess" errors
    assert_equal 0, @@sv.lot_opelocate(lot, :last)
    @@sv.lot_bankin(lot) # no assertion here, AutoBankin job might interfere
    # wait for lot deletion
    $log.info "waiting up to #{@@job_interval} s for the jCAP job to run"
    assert wait_for(timeout: @@job_interval) {
      li = @@sv.lot_info(lot)
      li.status == 'SHIPPED' && li.carrier.empty?
    }, "wrong lot status #{@@sv.lot_info(lot).status}, carrier #{@@sv.lot_info(lot).carrier}"
    # verify carrier status, note: FilterOpeCount in carrier status is still reported as 0
    assert_equal 'AVAILABLE',  @@sv.carrier_status(carrier).status, 'wrong carrier status'
    assert_equal 'Cleaned', @@sv.user_data(:carrier, carrier)['CARRIER_CONDITION'], 'wrong carrier condition'
    assert_nil @@sv.user_data(:carrier, carrier)['FILTER_OPE_COUNT'], "#{carrier} UDATA FILTER_OPE_COUNT not deleted"
  end

  def test15_old_lot_exists
    # set one carrier INUSE
    assert carrier = prepare_carriers(@@category, 1).first
    # create a stale lot for this carrier, cancel inhibit and wait for the jCAP job
    tstart = Time.now
    lot0 = "CLEAN-#{carrier}.00"
    assert_equal 0, @@sv.delete_lot_family(lot0, delete: true)
    assert @@svtest.new_lot(lot: lot0, product: @@product, route: @@route, waferids: ['XXYY001.00'], nwafers: 1, customer: @@customer)
    @@sv.wafer_sort(lot0, '')
    assert_equal 0, @@sv.inhibit_cancel(:route_opNo_product, [@@route, @@product])
    $log.info "waiting #{@@job_interval} s for the jCAP job to run"; sleep @@job_interval
    #
    # verify no new lot creation
    # assert_empty @@sv.carrier_status(carrier).lots, "wrong new lot created for #{carrier}"
    assert_empty @@sv.lot_list_incassette(carrier), "wrong new lot created for #{carrier}"
    # verify notification
    assert @@notifications.wait_for(@@notification_params.merge(tstart: tstart)) {|msgs|
      msgs.find {|m| m['Message'].include?("Existing virtual lot(s) for carrier '#{carrier}'")}
    }, 'invalid notification'
    # clean up lot
    assert_equal 0, @@sv.delete_lot_family(lot0, delete: true)
  end

  def test16_old_lot_scheduled
    # inhibit is still active
    $log.warn 'waiting 5 min to ensure notification is not skipped by the jCAP job'; sleep 300
    #
    # set one carrier INUSE
    assert carrier = prepare_carriers(@@category, 1).first, 'error preparing carrier'
    # create a stale lot request for this carrier, cancel inhibit and wait for the jCAP job
    tstart = Time.now
    lot0 = "CLEAN-#{carrier}.00"
    assert @@sv.new_lot_release(lot: lot0, product: @@product, route: @@route), "error scheduling lot #{lot0}"
    assert_equal 0, @@sv.inhibit_cancel(:route_opNo_product, [@@route, @@product])
    $log.info "waiting #{@@job_interval} s for the jCAP job to run"; sleep @@job_interval
    #
    # verify no new lot creation
    # assert_empty @@sv.carrier_status(carrier).lots, "wrong new lot created for #{carrier}"
    assert_empty @@sv.lot_list_incassette(carrier), "wrong new lot created for #{carrier}"
    # verify notification
    assert @@notifications.wait_for(@@notification_params.merge(tstart: tstart)) {|msgs|
      msgs.find {|m| m['Message'].include?("Existing virtual lot(s) for carrier '#{carrier}'")}
    }, 'invalid notification'
  end

  def test17_wrong_lot_in_carrier
    # set one carrier INUSE
    assert carrier = prepare_carriers(@@category, 1).first
    # create a wrong lot for this carrier, cancel inhibit and wait for the jCAP job
    assert lot0 = @@svtest.new_lot(product: @@product, route: @@route, nwafers: 1), 'error creating lot'
    assert_equal 0, @@sv.wafer_sort(lot0, carrier)
    assert_equal 0, @@sv.inhibit_cancel(:route_opNo_product, [@@route, @@product])
    $log.info "waiting #{@@job_interval} s for the jCAP job to run"; sleep @@job_interval
    #
    # verify no new lot creation
    # assert_equal [lot0], @@sv.carrier_status(carrier).lots, "wrong lot in #{carrier}"
    assert_equal [lot0], @@sv.lot_list_incassette(carrier), "wrong new lot created for #{carrier}"
    #
    # clean up lot
    @@sv.delete_lot_family(lot0)
  end

  def test18_orphaned_lot
    # set one carrier to status INUSE, cancel inhibit and wait for the jCAP job
    assert carrier = prepare_carriers(@@category, 1).first
    assert_equal 0, @@sv.inhibit_cancel(:route_opNo_product, [@@route, @@product])
    $log.info "waiting up to #{@@job_interval} s for the jCAP job to run"
    assert wait_for(timeout: @@job_interval) {
      !@@sv.lot_list_incassette(carrier).empty?
      # !@@sv.carrier_status(carrier).lots.empty?
    }, 'no lot created'
    #
    # verify creation of 1 lot and remove it from the carrier
    assert verify_lot_creation(carrier, @@product)
    # lot = @@sv.carrier_status(carrier).lots.first
    lot = @@sv.lot_list_incassette(carrier).first
    assert_equal 0, @@sv.wafer_sort(lot, '')
    #
    # verify lot is scrapped and a notification sent
    tstart = Time.now
    $log.info "waiting up to #{@@job_lotorphanremover} s for the jCAP orphanremover job to run"
    assert wait_for(timeout: @@job_lotorphanremover + 60) {
      @@sv.lot_info(lot).status == 'SCRAPPED'
    }, 'wrong lot status'
    assert @@notifications.wait_for(@@notification_params.merge(tstart: tstart)) {|msgs|
      msgs.find {|m| m['Message'].include?("Existing virtual lot(s) for carrier '#{carrier}'")}
    }, 'invalid notification'
  end

  def test19_lot_bankin_carrier_EI
    # set one carrier to status INUSE, cancel inhibit and wait for the jCAP job
    assert carrier = prepare_carriers(@@category, 1).first
    assert_equal 0, @@sv.inhibit_cancel(:route_opNo_product, [@@route, @@product])
    $log.info "waiting up to #{@@job_interval} s for the jCAP job to run"
    assert wait_for(timeout: @@job_interval) {
      !@@sv.lot_list_incassette(carrier).empty?
      # !@@sv.carrier_status(carrier).lots.empty?
    }, 'no lot created'
    #
    # verify lot creation
    assert verify_lot_creation(carrier, @@product)
    # process lot at the post meas. pd and leave the carrier in the eqp with xfer state EI
    # lot = @@sv.carrier_status(carrier).lots.first
    lot = @@sv.lot_list_incassette(carrier).first
    assert_equal 0, @@sv.lot_opelocate(lot, @@opNo_postmeas)
    assert @@sv.claim_process_lot(lot, nounload: true)
    assert_equal 0, @@sv.lot_gatepass(lot)
    #
    # bankin lot
    @@sv.lot_bankin(lot) # no assertion here, AutoBankin might interfere
    refute_empty @@sv.lot_info(lot).bank, 'lot is not banked in'
    #
    # verify lot is not touched by the job (still COMPLETED, xfer status EI)
    $log.info "waiting #{@@job_lotremover} s for the jCAP lotremover job to run"; sleep @@job_lotremover
    li = @@sv.lot_info(lot)
    assert_equal 'COMPLETED', li.status, 'wrong lot status'
    assert_equal 'EI', li.xfer_status, 'wrong xfer_status'
  end


  # aux methods

  # change status of all carriers to NOTAVAILABLE, n carriers to INUSE, return INUSE carriers on success or nil
  def prepare_carriers(cat, n=0, params={})
    $log.info "prepare_carriers #{cat.inspect}, #{n}"
    ret = true
    cc = @@sv.carrier_list(category: cat)
    # remove lots, CLEAN-* lots are removed separately
    cc.each {|c|
      @@sv.lot_list_incassette(c).each {|l| ret &= @@sv.delete_lot_family(l).zero?}
      ret &= @@svtest.cleanup_carrier(c)
    }
    return unless ret
    # set carrier status
    ccinuse = cc.take(n)
    (cc - ccinuse).each {|c|
      cs = @@sv.carrier_status(c)
      next if cs.status == 'NOTAVAILABLE'
      @@sv.carrier_status_change(c, 'AVAILABLE') unless cs.status == 'AVAILABLE'
      ret &= @@sv.carrier_status_change(c, 'NOTAVAILABLE').zero?
    }
    return unless ret
    ccinuse.each {|c|
      cs = @@sv.carrier_status(c)
      next if cs.status == 'INUSE'
      @@sv.carrier_status_change(c, 'AVAILABLE') unless cs.status == 'AVAILABLE'
      ret &= @@sv.carrier_status_change(c, 'INUSE').zero?
      ret &= @@sv.carrier_condition_change(c, 'Dirty')
    }
    return ret ? ccinuse : nil
  end

  # return true on success
  def verify_lot_creation(carrier, product)
    $log.info "verify_lot_creation #{carrier.inspect}, #{product.inspect}"
    ret = true
    # cs = @@sv.carrier_status(carrier)
    lots = @@sv.lot_list_incassette(carrier)
    ($log.warn "  wrong lots in carrier: #{lots.inspect}"; return) if lots.size != 1
    lot = lots.first
    li = @@sv.lot_info(lot, wafers: true)
    ($log.warn "  wrong wafer number for lot #{lot}: #{li.nwafers}"; ret=false) if li.nwafers != 1
    ($log.warn "  lot #{lot} is ONHOLD"; ret=false) if li.states['Lot Hold State'] == 'ONHOLD'
    ($log.warn "  wrong lot name: #{li.lot}"; ret=false) unless li.lot.start_with?("CLEAN-#{carrier}.")
    w = li.wafers[0].wafer
    ($log.warn "  lot #{lot} has a wrong wafer id #{w}"; ret=false) unless w.start_with?("WSRC-#{carrier}")
    ($log.warn "  lot #{lot} has the wrong lottype #{li.lottype}"; ret=false) if li.lottype != 'Engineering'
    ($log.warn "  lot #{lot} has the wrong sublottype #{li.sublottype}"; ret=false) if li.sublottype != @@sublottype
    ($log.warn "  lot #{lot} has the wrong product #{li.product}"; ret=false) if li.product != product
    ($log.warn "  lot #{lot} is on the wrong route #{li.route}"; ret=false) if li.route != @@route
    ($log.warn "  lot #{lot} is at the wrong opNo #{li.opNo}"; ret=false) if li.opNo != @@opNo_premeas
    return ret
  end

end
