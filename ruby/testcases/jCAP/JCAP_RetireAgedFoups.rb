=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: sfrieske, 2018-03-21

Version: 18.09

History:
  2018-11-07 sfrieske, minor cleanup
  2019-02-19 sfrieske, improved EmuRemote usage

Notes:
  This job periodically calls RTD and just acts on the response, no logic contained.
  see: http://f1onewiki:21080/display/JCAP/RetireAgedFoups+Design+Document 
=end

require 'rtdserver/rtdemuremote'
require 'SiViewTestCase'


class JCAP_RetireAgedFoups < SiViewTestCase
  @@sv_defaults = {carriers: 'JC%'}
  @@rtd_rule = 'JCAP_RETIRE_AGED_FOUPS'
  # @@rtd_category = 'DISPATCHER/JCAP'
  @@columns = ['cast_id']

  @@job_interval = 190   # 3+ min, from properties


  def self.shutdown
    @@rtdremote.delete_emustation('', rule: @@rtd_rule)
  end


  def test11
    @@rtdremote = RTD::EmuRemote.new($env)
    @@rtdremote.add_emustation('', rule: @@rtd_rule, method: 'empty_reply')
    #
    assert cc = @@svtest.get_empty_carrier(n: 4), 'not enough empty carriers'
    # all carriers are AVAILABLE, scrap one of them
    assert_equal 0, @@sv.carrier_status_change(cc[0], 'SCRAPPED')
    # prepare RTD rule response
    assert @@rtdremote.add_emustation_wait('', rule: @@rtd_rule, timeout: @@job_interval) {
      @@rtdremote.create_response(@@columns, {'rows'=>[[cc[0]], [cc[1]], ['NOTEX'], [cc[2]], [cc[3]]]})
    }, 'rule has not been called'
    # allow the job to work and verify carriers status
    sleep 10
    cc.each {|c| assert_equal 'SCRAPPED', @@sv.carrier_status(c).status, "wrong status of carrier #{c}"}
  end

end
