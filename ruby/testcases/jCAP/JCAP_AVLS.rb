=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: Conny Stenzel, 2011-01-18
        Steffen Steidten, 2012-01-09

Version: 19.05.08

History:
  2015-08-25 ssteidte, minor cleanup
  2016-08-30 sfrieske, minor cleanup
  2017-05-19 sfrieske, added TCs for 17.04
  2018-11-07 sfrieske, removed QATestAdapter, minor cleanup
  2019-02-19 sfrieske, improved EmuRemote usage
  2020-02-21 sfrieske, added SORT_TYPE check in avlstest
  2021-04-21 sfrieske, moved @sorter and @pg form SiView::Test to SJC::Test
  2021-08-27 sfrieske, require uniquestring instead of misc

Notes:
  change properties (admin):
  automatedvendorlotstart.transfer.request.time.frames=["00:00-24:00"]
=end

require 'jcap/avlstest'
require 'rtdserver/rtdemuremote'
require 'util/uniquestring'
require 'util/waitfor'
require 'SiViewTestCase'


class JCAP_AVLS < SiViewTestCase
  @@sorter = 'UTS001'
  @@pg = 'PG1'
  @@location = 'Fab 1'
  @@part = '31006550'       # SiView product is xxx.ALL
  @@part2 = '31006551'      # for product change
  @@part3 = '31006565'      # for property 'xferEnabled: false'
  @@cond3 = 'Due'           # for property 'condition'
  @@type3 = 'QA'            # for property 'type' (UDATA)
  @@partmol = 'UTAVLSMOL'
  @@part_version = '.ALL'
  @@bank_raw = 'ON-RAW'
  @@bank_rawmol = 'OS-RAW'
  #
  @@empty_rule = 'EMPTY'  # as configured in sjc.properties for sjc.rtd.empty.rule
  @@columns = %w(cast_id cast_category stk_id)  # SJC uses the first column w/o checking the name

  @@use_notifications = true
  @@job_interval = 330    # 5 min, include sufficient slack time
  @@vcarriers = []
  @@testlots = []


  def self.startup
    super
    @@avlst = AVLS::Test.new($env, @@sv_defaults.merge(sv: @@sv, job_interval: @@job_interval,
    sorter: @@sorter, pg: @@pg, notifications: @@use_notifications, bank_raw: @@bank_raw, location: @@location, part: @@part))
    @@rtdremote = RTD::EmuRemote.new($env)
    @@rtdremote.add_emustation('JCAP_RETIRE_AGED_FOUPS', rule: 'JCAP_RETIRE_AGED_FOUPS', method: 'empty_reply')
  end

  def self.shutdown
    @@rtdremote.delete_emustation(@@empty_rule)
    @@rtdremote.delete_emustation('JCAP_RETIRE_AGED_FOUPS', rule: 'JCAP_RETIRE_AGED_FOUPS')
  end

  def self.after_all_passed
    @@vcarriers.each {|carrier|
      next if carrier.nil? || carrier.empty?
      next if @@sv.carrier_list(carrier: carrier).empty?
      lots = @@sv.lot_list_incassette(carrier)
      lots.each {|lot| @@sv.delete_lot_family(lot)}
      @@svtest.delete_registered_carrier(carrier)
    }
    @@testlots.each {|lot| @@sv.delete_lot_family(lot)}
  end

  def setup
    @@rtdremote.delete_emustation(@@empty_rule)
  end

  def teardown
    @@rtdremote.delete_emustation(@@empty_rule)
  end


  def test10_happy
    $setup_ok = false
    #
    vcarrier = unique_string('FQ')
    assert_empty @@sv.carrier_list(carrier: vcarrier), 'carrier already exists'
    @@vcarriers << vcarrier
    vlot = unique_string('Q')
    @@testlots << vlot
    # send vendor message and verify vendor lot creation incl. script parameters (MSR 1511029)
    order = unique_string
    assert @@avlst.send_vmessage_verify_carrier_lot(vlot, vcarrier, order: order)
    # stock in vendor carrier and verify a SR is created
    assert srid = @@avlst.carrier_stockin_verify_sr(vcarrier), 'wrong SR'
    # verify script parameters (available shortly after vlot and vcarrier creation, definitely now)
    $log.info 'verifying script parameters'
    assert lot = @@sv.lot_list_incassette(vcarrier).first, 'empty vendor lot carrier?'
    assert li = @@sv.lot_info(lot, wafers: true)
    assert_equal 'Y', @@sv.user_parameter('Lot', lot, name: 'AutoSTBAllowed').value, 'wrong lot script parameter AutoSTBAllowed'
    li.wafers.each {|w|
      assert_equal order, @@sv.user_parameter('Wafer', w.wafer, name: 'OracleLotID').value, 'wrong wafer script parameter OracleLotID'
    }
    # verify slotmap in SR, MSR 882657
## not working in 19.05.x (2020-02-24), accepted by user  assert @@avlst.verify_sr_slots(vcarrier, srid), 'wrong SR slotmap'
    # prepare sorter and an empty, stocked in carrier and execute the SR
    assert ec = @@svtest.get_empty_carrier, 'no empty carrier'
    assert @@svtest.cleanup_carrier(ec)
    assert @@svtest.cleanup_eqp(@@sorter, mode: 'Auto-3', portstatus: 'UnloadReq'), 'error cleaning up sorter'
    sleep 10
    # set RTD rule, execute SR and verify vlot bank move
    @@rtdremote.add_emustation(@@empty_rule) {@@rtdremote.create_response(@@columns, {'rows'=>[[ec, '', '']]})}
    assert @@avlst.execute_sr_verify(vcarrier), 'error executing SR/banking in lot'
    # verify slotmap, MSR 882657
## not working in 19.05.x (2020-02-24), accepted by user  assert @@avlst.verify_carrier_slots(vcarrier, ec), 'wrong slots in carrier'
    # scrap carrier and verify deletion in SiView
    assert_equal 0, @@sv.carrier_status_change(vcarrier, 'SCRAPPED')
    assert_equal 0, @@sv.carrier_xfer_status_change(vcarrier, 'MO', @@avlst.stocker)
    $log.info "waiting up to #{@@job_interval} s for vendor carrier deletion"
    assert wait_for(timeout: @@job_interval) {
      @@sv.carrier_list(carrier: vcarrier).empty?
    }, "error deleting carrier #{vcarrier}"
    #
    $setup_ok = true
  end

  def test11_fosb_trailing_whitespace
    # same test as happy but FOSB sent in the vendor msg has a trailing whitespace
    vcarrier = unique_string('FQ')
    assert_empty @@sv.carrier_list(carrier: vcarrier), "carrier #{vcarrier} already exists"
    @@vcarriers << vcarrier
    fosb = vcarrier + ' '
    assert_empty @@sv.carrier_list(carrier: fosb), "carrier #{vcarrier} already exists"
    @@vcarriers << fosb
    vlot = unique_string('Q')
    @@testlots << vlot
    # send vendor message and verify vendor lot creation
    assert @@avlst.send_vmessage_verify_carrier_lot(vlot, fosb)
    # stock in vendor carrier and verify a SR is created
    assert @@avlst.carrier_stockin_verify_sr(vcarrier)
    # prepare sorter and an empty, stocked in carrier and execute the SR
    assert ec = @@svtest.get_empty_carrier, "no empty carrier"
    assert @@svtest.cleanup_carrier(ec)
    assert @@svtest.cleanup_eqp(@@sorter, mode: 'Auto-3', portstatus: 'UnloadReq'), 'error cleaning up sorter'
    sleep 10
    # set RTD rule, execute SR and verify vlot bank move
    @@rtdremote.add_emustation(@@empty_rule) {@@rtdremote.create_response(@@columns, {'rows'=>[[ec, '', '']]})}
    assert @@avlst.execute_sr_verify(vcarrier), 'error executing SR/banking in lot'
    # scrap carrier and verify deletion in SiView
    assert_equal 0, @@sv.carrier_status_change(vcarrier, 'SCRAPPED')
    assert_equal 0, @@sv.carrier_xfer_status_change(vcarrier, 'MO', @@avlst.stocker)
    assert wait_for(timeout: @@job_interval) {
      @@sv.carrier_list(carrier: vcarrier).empty?
    }, "error deleting carrier #{vcarrier}"
  end

  def test12_duplicate_carrierid
    # carrier name
    vcarrier = unique_string('FQ')
    assert_empty @@sv.carrier_list(carrier: vcarrier), "carrier #{vcarrier} already exists"
    @@vcarriers << vcarrier
    # 1st message
    vlot = unique_string('Q')
    @@testlots << vlot
    assert @@avlst.send_vmessage_verify_carrier_lot(vlot, vcarrier)
    # 2nd message: same carrier, other wafers
    vlot2 = unique_string('Q')
    @@testlots << vlot
    assert @@avlst.send_wrong_vmessage_verify_notification(vlot2, vcarrier, 'Duplicate carrier id'), 'wrong response'
  end

  def test13_empty_carrierid
    assert @@avlst.send_wrong_vmessage_verify_notification(nil, '', 'Carrier id is not set')
  end

  def test14_wrong_carrierid
    assert @@avlst.send_wrong_vmessage_verify_notification(nil, 'XX', 'Carrier id must match pattern')
  end

  def test15_empty_lotid
    assert @@avlst.send_wrong_vmessage_verify_notification('', nil, 'Vendorlot id is not set')
  end

  def test16_empty_partid
    assert @@avlst.send_wrong_vmessage_verify_notification(nil, nil, 'Part id is not set', part: '')
  end

  def test17_empty_order
    assert @@avlst.send_wrong_vmessage_verify_notification(nil, nil, 'Delivery number is not set', order: '')
  end

  def test18_empty_supplier
    assert @@avlst.send_wrong_vmessage_verify_notification(nil, nil, 'Vendor name is not set', supplier: '')
  end

  def test19_empty_location
    assert @@avlst.send_wrong_vmessage_verify_notification(nil, nil, 'Location is not set', location: '')
  end

  def test20_no_wafers
    assert @@avlst.send_wrong_vmessage_verify_notification(nil, nil, 'No wafer information found', err_nowafers: true)
  end

  def test21_too_many_wafers
    assert @@avlst.send_wrong_vmessage_verify_notification(nil, nil, 'More than 25 wafers found', nwafers: 26)
  end

  def test22_empty_waferid
    assert @@avlst.send_wrong_vmessage_verify_notification(nil, nil, 'At least one wafer id is not set', err_waferid: '')
  end

  def test23_waferid_short
    assert @@avlst.send_wrong_vmessage_verify_notification(nil, nil, 'shorter than allowed', err_waferid: 'Y'*10)
  end

  def test24_wrong_slot
    assert @@avlst.send_wrong_vmessage_verify_notification(nil, nil, 'wafer position is incorrect', first_slot: 0)
  end

  def test26_wafer_exists_sv
    vlot = unique_string('Q')
    @@testlots << vlot
    vcarrier = 'F' + vlot
    waferid = vlot + '01QA'
    @@vcarriers << vcarrier
    # create a lot in SiView with the same wafer ID
    assert svlot = @@svtest.new_lot(waferids: [waferid]), 'error creating lot'
    @@testlots << svlot
    # send message
    tstart = Time.now - 30
    assert res = @@avlst.vmessage.create_fosb(vlot, vcarrier, err_waferid: waferid), 'error sending msg'
    $log.info 'verifying response has no carrier'
    assert_nil res[:carrierId], "wrong response: #{res}"
    # verify notification
    if @@avlst.notifications
      assert @@avlst.notifications.wait_for(tstart: tstart, timeout: 180,
        service: '%jCAP_AsyncNotification%', filter: {'Application'=>'AutoVenLotStart'}) {|msgs|
          msgs.find {|m| m['Message'].include?("Wafer with wafer id '#{waferid}' already exists")}
      }
    end
  end

  def test27_wafer_exists_mds
    # vlot
    vlot = unique_string('Q')
    @@testlots << vlot
    # 1st message
    vcarrier = unique_string('FQ')
    assert_empty @@sv.carrier_list(carrier: vcarrier), "carrier #{vcarrier} already exists"
    @@vcarriers << vcarrier
    assert @@avlst.send_vmessage_verify_carrier_lot(vlot, vcarrier)
    # 2nd message: same wafers, other carrier
    vcarrier2 = unique_string('FQ')
    assert_empty @@sv.carrier_list(carrier: vcarrier2), "carrier #{vcarrier} already exists"
    @@vcarriers << vcarrier2
    assert @@avlst.send_wrong_vmessage_verify_notification(vlot, vcarrier2, 'Wafer with wafer id'), 'wrong notification'
  end

  def test28_wrong_carrier_in_the_middle
    # create lot and carrier ids
    lots = 3.times.collect {unique_string[0..8]}
    @@testlots += lots
    carriers = [unique_string('F'), '', unique_string('F')]
    @@vcarriers += carriers
    # send message
    tstart = Time.now - 30
    assert res = @@avlst.vmessage.create_fosb(lots, carriers), 'no response'
    $log.info 'verifying response'
    assert_equal 3, res.size, "wrong response: #{res.inspect}"
    assert_equal carriers[0], res[0][:carrierId], "wrong response for 1st carrier: #{res.inspect}"
    assert_nil res[1][:carrierId], "wrong response for empty 2nd carrier: #{res.inspect}"
    assert_equal carriers[2], res[2][:carrierId], "wrong response for 3rd carrier: #{res.inspect}"
    # verify notification for missing 2nd carrier
    if @@avlst.notifications
      assert @@avlst.notifications.wait_for(tstart: tstart, timeout: 180,
        service: '%jCAP_AsyncNotification%', filter: {'Application'=>'AutoVenLotStart'}) {|msgs|
          msgs.find {|m| m['Message'].include?('Carrier id is not set')}
      }
    end
  end


  def test31_lot_terminated
    vcarrier = unique_string('FQ')
    @@vcarriers << vcarrier
    vlot = unique_string('Q')
    @@testlots << vlot
    assert_empty @@sv.carrier_list(carrier: vcarrier), "carrier #{vcarrier} already exists"
    assert @@avlst.send_vmessage_verify_carrier_lot(vlot, vcarrier)
    # mark received lot as terminated and verify removal of the lot and FOSB
    rlot = @@sv.carrier_status(vcarrier).lots.first
    @@testlots << rlot
    assert_equal 0, @@sv.lot_note_register(rlot, 'QA Test', 'AVLSTermination')
    assert wait_for(timeout: @@job_interval) {@@sv.lot_info(rlot).status == 'EMPTIED'}, "error removing lot #{rlot}"
    assert wait_for(timeout: @@job_interval) {@@sv.carrier_list(carrier: vcarrier).empty?}, "error deleting carrier #{vcarrier}"
  end

  def test32_product_change
    vcarrier = unique_string('FQ')
    assert_empty @@sv.carrier_list(carrier: vcarrier), "carrier #{vcarrier} already exists"
    @@vcarriers << vcarrier
    vlot = unique_string('Q')
    @@testlots << vlot
    assert @@avlst.send_vmessage_verify_carrier_lot(vlot, vcarrier)
    # mark received lot for product change
    rlot = @@sv.carrier_status(vcarrier).lots.first
    @@testlots << rlot
    assert_equal 0, @@sv.lot_note_register(rlot, 'QA Test', "AVLSProductChange:#{@@part3}")
    assert_equal 0, @@sv.lot_note_register(rlot, 'QA Test', "AVLSProductChange:#{@@part2}")
    assert wait_for(timeout: @@job_interval) {@@sv.lot_info(rlot).status == 'EMPTIED'}, "error removing lot #{rlot}"
    sleep 20
    rlot2 = @@sv.carrier_status(vcarrier).lots.first
    @@testlots << rlot2
    assert rlot2, 'error creating new vendor lot'
    refute_equal rlot, rlot2, 'no new vendor lot created'
    li = @@sv.lot_info(rlot2)
    assert_equal "#{@@part2}#{@@part_version}", li.product
  end


  def test41_xferenabled_type_condition
    vcarrier = unique_string('FQ')
    assert_empty @@sv.carrier_list(carrier: vcarrier), "carrier #{vcarrier} already exists"
    @@vcarriers << vcarrier
    vlot = unique_string('Q')
    @@testlots << vlot
    $log.info "using vendor FOSB #{vcarrier.inspect}, lot #{vlot.inspect}"
    # send vendor message and verify vendor lot creation
    assert @@avlst.send_vmessage_verify_carrier_lot(vlot, vcarrier, part: @@part3)
    # verify carrier MaterialType and condition
    assert_equal @@cond3, @@sv.user_data(:carrier, vcarrier)['CARRIER_CONDITION'], 'wrong carrier condition'
    assert_equal @@type3, @@sv.user_data(:carrier, vcarrier)['MaterialType'], 'wrong MaterialType'
    # stock in vendor carrier and verify no SR was created
    assert @@avlst.carrier_stockin_verify_sr(vcarrier, state: 'SORTJOBCOMPLETED')
    refute_empty @@sv.carrier_status(vcarrier).lots, "wrong SR for carrier #{vcarrier}"
  end

  def test51_SBMO
    @@avlstmol = AVLS::Test.new($env, @@sv_defaults.merge(sv: @@sv, sm: @@avlst.sm, job_interval: @@job_interval,
    sorter: @@sorter, pg: @@pg, bank_raw: @@bank_rawmol, location: @@location, part: @@partmol, vcarrier_category: 'SBMO'))
    vcarrier = unique_string('FQ')
    assert_empty @@sv.carrier_list(carrier: vcarrier), "carrier #{vcarrier} already exists"
    @@vcarriers << vcarrier
    vlot = unique_string('Q')
    @@testlots << vlot
    # send vendor message and verify vendor lot creation
    assert @@avlstmol.send_vmessage_verify_carrier_lot(vlot, vcarrier)
    # stock in vendor carrier and verify a SR is created
    assert @@avlstmol.carrier_stockin_verify_sr(vcarrier)
    @@testlots += @@sv.lot_list_incassette(vcarrier)
    # prepare sorter and an empty, stocked in carrier and execute the SR
    assert @@svtest.cleanup_eqp(@@sorter, mode: 'Auto-3', portstatus: 'UnloadReq'), 'error cleaning up sorter'
    assert ec = @@svtest.get_empty_carrier(carrier_category: 'MOL', carrier: '%'), 'no empty carrier'
    assert @@svtest.cleanup_carrier(ec)
    sleep 10
    # set RTD rule, execute SR and verify vlot bank move
    @@rtdremote.add_emustation(@@empty_rule) {@@rtdremote.create_response(@@columns, {'rows'=>[[ec, '', '']]})}
    assert @@avlstmol.execute_sr_verify(vcarrier), 'error executing SR/banking in lot'
    #
    # scrap carrier and verify deletion in SiView
    assert_equal 0, @@sv.carrier_status_change(vcarrier, 'SCRAPPED')
    assert_equal 0, @@sv.carrier_xfer_status_change(vcarrier, 'MO', @@avlstmol.stocker)
    $log.info "waiting up to #{@@job_interval} s for vendor carrier deletion"
    assert wait_for(timeout: @@job_interval) {@@sv.carrier_list(carrier: vcarrier).empty?}, "error deleting carrier #{vcarrier}"
  end

end
