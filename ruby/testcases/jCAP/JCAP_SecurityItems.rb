=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.


Author: Steffen Frieske, 2019-02-06

Version: 18.08

History:

Notes:
  This job periodically calls RTD and just acts on the response, no logic contained.
  actions: lot hold, reticle inhibit
=end

require 'derdack'
require 'rtdserver/rtdemuremote'
require 'SiViewTestCase'


class JCAP_SecurityItems < SiViewTestCase
  @@reticle = 'UTRETICLE9999'
  @@rule_lot = 'JCAP_MISSING_SECURITY_LOTS'
  @@columns_lot = %w(lot_id last_known_location carrier_station_id transit_time hold_time)
  @@rule_rtcl = 'JCAP_MISSING_SECURITY_RETICLES'
  @@columns_rtcl = %w(reticle_id last_known_location transit_time inhibit_time)
  # @@rtd_category = 'DISPATCHER/JCAP' or DISPATCHER/QA
  @@notification_params = {service: '%jCAP_AsyncNotification%', filter: {'Application'=>'SecurityItems'}}

  @@testlots = []
  @@job_interval = 330  # 5 minutes


  def self.startup
    super
    @@notifications = DerdackEvents.new($env)
    @@rtdremote = RTD::EmuRemote.new($env)
    @@rtdremote.delete_emustation('', rule: @@rule_lot)
    @@rtdremote.delete_emustation('', rule: @@rule_rtcl)
  end

  def self.shutdown
    @@rtdremote.delete_emustation('', rule: @@rule_lot)
    @@rtdremote.delete_emustation('', rule: @@rule_rtcl)
  end

  def self.after_all_passed
    @@sv.inhibit_cancel(:reticle, @@reticle)
    @@testlots.each {|lot| @@sv.delete_lot_family(lot)}
  end


  def test11_lot
    assert lot = @@svtest.new_lot, 'error creating test lot'
    @@testlots << lot
    #
    # set RTDEmu data and wait for the JCAP job to run
    tstart = Time.now
    t = (Time.now.utc - 7200).strftime('%d/%m/%y %H:%M:%S')  # transit_time e.g.  30/11/18 16:01:17
    assert @@rtdremote.add_emustation_wait('', rule: @@rule_lot, timeout: @@job_interval) {
      @@rtdremote.create_response(@@columns_lot, {'rows'=>[[lot, 'QALotLastLoc', '', t, '']]})
    }, 'rule not called'
    # verify notification
    assert msg = @@notifications.wait_for(@@notification_params.merge(tstart: tstart)) {|msgs| 
      msgs.find {|m| m['LotID'] == lot}
    }, 'missing notification'
    assert_equal 'XSEC', msg['ReasonCode'], 'notification: wrong reason code'
    # verify hold
    assert lh = @@sv.lot_hold_list(lot, reason: 'XSEC').first, 'missing lot hold'
  end

  def test12_lot_repeat
    lot = @@testlots[0]
    # set RTDEmu data and wait for the JCAP job to run
    tstart = Time.now
    t = (Time.now.utc - 7200).strftime('%d/%m/%y %H:%M:%S')  # transit_time e.g.  30/11/18 16:01:17
    assert @@rtdremote.add_emustation_wait('', rule: @@rule_lot, timeout: @@job_interval) {
      @@rtdremote.create_response(@@columns_lot, {'rows'=>[[lot, 'QALotLastLoc', '', t, '']]})
    }, 'rule not called'
    # verify new notification
    assert msg = @@notifications.wait_for(@@notification_params.merge(tstart: tstart)) {|msgs| 
      msgs.find {|m| m['LotID'] == lot}
    }, 'missing 2nd notification'
    assert_equal 'XSEC', msg['ReasonCode'], '2nd notification: wrong reason code'
    # verify no new hold
    assert_equal 1, @@sv.lot_hold_list(lot, reason: 'XSEC').size, 'wrong lot hold'
  end

  def test13_arbitrary_columns
    lot = @@testlots[0]
    # set RTDEmu data and wait for the JCAP job to run
    tstart = Time.now
    t = (Time.now.utc - 7200).strftime('%d/%m/%y %H:%M:%S')  # transit_time e.g.  30/11/18 16:01:17
    data = [[lot, 'QALotLastLoc', '', t, '', 'QAval1']]
    assert @@rtdremote.add_emustation_wait('', rule: @@rule_lot, timeout: @@job_interval) {
      @@rtdremote.create_response(@@columns_lot + ['QAcol1'], {'rows'=>data})
    }, 'rule not called'
    # verify new notification
    assert msg = @@notifications.wait_for(@@notification_params.merge(tstart: tstart)) {|msgs| 
      msgs.find {|m| m['LotID'] == lot}
    }, 'missing 3rd notification'
    assert_equal 'XSEC', msg['ReasonCode'], '3rd notification: wrong reason code'
    # verify no new hold
    assert_equal 1, @@sv.lot_hold_list(lot, reason: 'XSEC').size, 'wrong lot hold'
    # verify message text includes all RTD headers
    msgtxt = msg['Message']
    (@@columns_lot + ['QAcol1']).each {|e| assert msgtxt.include?(e), "missing header #{e.inspect}"}
    data.first.each {|e| assert msgtxt.include?(e), "missing value #{e.inspect}"}
  end

  def testdev19_wrong_rule  # error log line ' DispatchList result rule', no notification 
    assert lot = @@svtest.new_lot, 'error creating test lot'
    @@testlots << lot
    #
    # set RTDEmu data and wait for the JCAP job to run
    tstart = Time.now
    t = (Time.now.utc - 7200).strftime('%d/%m/%y %H:%M:%S')  # transit_time e.g.  30/11/18 16:01:17
    assert @@rtdremote.add_emustation_wait('', rule: @@rule_lot, timeout: @@job_interval) {
      @@rtdremote.create_response(@@columns_lot, {'rows'=>[[lot, 'QALotLastLoc', '', t, '']], 'replyrule'=>'QARule'})
    }, 'rule not called'
    # # verify new notification
    # assert msg = @@notifications.wait_for(@@notification_params.merge(tstart: tstart)) {|msgs| 
    #   msgs.find {|m| m['LotID'] == lot}
    # }, 'missing error notification'
    # verify no hold
    sleep 60
    assert_equal 0, @@sv.lot_hold_list(lot, reason: 'XSEC').size, 'wrong lot hold'
  end


  def test21_reticle
    assert_equal 0, @@sv.inhibit_cancel(:reticle, @@reticle), 'error canceling reticle inhibit'
    #
    # set RTDEmu data and wait for the JCAP job to run
    tstart = Time.now
    t = (Time.now.utc - 7200).strftime('%d/%m/%y %H:%M:%S')  # transit_time e.g.  30/11/18 16:01:17
    assert @@rtdremote.add_emustation_wait('', rule: @@rule_rtcl, timeout: @@job_interval) {
      @@rtdremote.create_response(@@columns_rtcl, {'rows'=>[[@@reticle, 'QARtclLastLoc', t, '']]})
    }, 'rule not called'
    sleep 10
    # verify inhibit
    assert lh = @@sv.inhibit_list(:reticle, @@reticle, reason: 'XSEC').first, 'missing reticle inhibit'
    # verify notification
    assert msg = @@notifications.wait_for(@@notification_params.merge(tstart: tstart)) {|msgs| 
      msgs.find {|m| m['ReticleID'] == @@reticle}
    }, 'missing notification'
    assert_equal 'XSEC', msg['ReasonCode'], 'notification: wrong reason code'
    # assert ...
  end

  def test22_reticle_repeat
    # set RTDEmu data and wait for the JCAP job to run
    tstart = Time.now
    t = (Time.now.utc - 7200).strftime('%d/%m/%y %H:%M:%S')  # transit_time e.g.  30/11/18 16:01:17
    assert @@rtdremote.add_emustation_wait('', rule: @@rule_rtcl, timeout: @@job_interval) {
      @@rtdremote.create_response(@@columns_rtcl, {'rows'=>[[@@reticle, 'QARtclLastLoc', t, '']]})
    }, 'rule not called'
    # verify new notification
    assert msg = @@notifications.wait_for(@@notification_params.merge(tstart: tstart)) {|msgs| 
      msgs.find {|m| m['ReticleID'] == @@reticle}
    }, 'missing notification'
    assert_equal 'XSEC', msg['ReasonCode'], 'notification: wrong reason code'
    # verify inhibit
    assert lh = @@sv.inhibit_list(:reticle, @@reticle, reason: 'XSEC').first, 'missing reticle inhibit'
  end

end
