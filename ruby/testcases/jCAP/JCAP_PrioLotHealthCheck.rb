=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.


Author: sfrieske, 2016-11-08

Version: 18.09

History:
  2018-11-07 sfrieske, minor cleanup
  2019-02-19 sfrieske, improved EmuRemote usage
  2021-08-27 sfrieske, require uniquestring instead of misc
=end

require 'rtdserver/rtdemuremote'
require 'util/uniquestring'
require 'SiViewTestCase'


class JCAP_PrioLotHealthCheck < SiViewTestCase
  @@rtd_rule = 'JCAP_PRIO_LOT_HEALTH_CHECK'
  ##@@rtd_category = 'DISPATCHER/JCAP'
  @@columns = %w(LOT_ID CLAIM_TIME DELAY_REASON_CODE DELAY_REASON MAIN_PD_ID PD_ID OPE_NO task_type task_id task_sub_type
    tcl_qt_criticality tcl_department tcl_too_long_processing tcl_too_long_waiting TCT lot_process_state task_add_info TAO)

  @@job_interval = 330  # 5 minutes
  @@testlots = []


  def self.shutdown
    @@rtdremote.delete_emustation('', rule: @@rtd_rule)
  end

  def self.after_all_passed
    @@testlots.each {|lot| @@sv.delete_lot_family(lot)}
  end


  def test10_test
    @@rtdremote = RTD::EmuRemote.new($env)
    # create test lot
    assert lot = @@svtest.new_lot, 'error creating test lot'
    @@testlots << lot
    li = @@sv.lot_info(lot)
    # configure RTD answer
    data = [['LOT_ID', '2017-02-13 13:15:17', '1', 'QAQA..512', 'MAIN_PD_ID', 'PD_ID', 'OPE_NO',
      'TimeCriticalLot', 'UMN231.007-TimeCriticalLot-C02-1889.000-6850.1450', 'SRKT', 'C1', 'TST',
      '0', '1', '4260', 'ONHOLD', 'Addtl info (QAQA)', '76:23']]
    reason = unique_string
    params = {'LOT_ID'=>lot, 'DELAY_REASON'=>reason, 'MAIN_PD_ID'=>li.route, 'PD_ID'=>li.op, 'OPE_NO'=>li.opNo}
    params.each_pair {|k, v| data[0][@@columns.index(k)] = v}
    assert @@rtdremote.add_emustation_wait('', rule: @@rtd_rule, timeout: @@job_interval) {
      @@rtdremote.create_response(@@columns, {'rows'=>data})
    }, 'rule has not been called'
    # verify note
    sleep 10
    notes = @@sv.lot_openotes(lot)
    assert_equal 1, notes.size, 'wrong number of notes'
    note = notes.first
    assert_equal 'LotHlthChk', note.title, 'wrong openote title'
    assert_equal reason, note.note, 'wrong openote text'
    #
    # remove lot from RTD response
    assert @@rtdremote.add_emustation_wait('', rule: @@rtd_rule, timeout: @@job_interval) {
      @@rtdremote.create_response(@@columns, {'rows'=>[[]]})
    }, 'rule has not been called'
    # verify new note
    sleep 10
    notes = @@sv.lot_openotes(lot)
    assert_equal 2, notes.size, 'wrong number of notes'
    note = notes.first
    assert_equal 'LotHlthChk', note.title, 'wrong openote title'
    assert_equal 'RESOLVED', note.note, 'wrong openote text'
  end

end
