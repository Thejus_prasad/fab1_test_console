=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.


Author: Steffen Frieske, 2020-10-30

Version: 20.09

History:
  2021-08-27 sfrieske, require uniquestring instead of misc

Notes:
  see http://f1onewiki:21080/display/JCAP/UpdateIntermediateTarget
=end

require 'time'
require 'misc/fodms'
require 'rtdserver/rtdemuremote'
require 'util/uniquestring'
require 'SiViewTestCase'


class JCAP_UpdateIntermediateTarget < SiViewTestCase
  @@sv_defaults = {product: 'TKEY01.A0', route: 'C02-TKEY.01'}  # route must have a PD like FAB1OUT.01
  @@tgt_op = 'FAB1OUT.01'
  @@rtd_rule = 'JCAP_ADJUST_FOD_COMMIT'
  @@rtd_category = 'DISPATCHER/JCAP'
  @@columns = ['LotID', 'OpeNo', 'PdID', 'MainPDID', 'DueDate', 'Category', 'ClaimMemo']

  @@job_interval = 330
  @@testlots = []


  def self.after_all_passed
    @@testlots.each {|lot| @@sv.delete_lot_family(lot)}
  end

  def self.shutdown
    @@rtdremote.delete_emustation('', rule: @@rtd_rule)
  end

  def setup
    return unless self.class.class_variable_defined?(:@@rtdremote)
    # set empty_reply for the time of test preparation
    assert @@rtdremote.add_emustation('', rule: @@rtd_rule, method: 'empty_reply')
  end


  def test00_setup
    $setup_ok = false
    #
    @@fodtest = FODMS::Test.new($env, sv: @@sv, tgt_op: @@tgt_op)
    @@rtdremote = RTD::EmuRemote.new($env)
    # create 3 test lots in T_XF_LOT_TARGET
    assert lots = @@svtest.new_lots(3), 'error creating test lots'
    @@testlots += lots
    tstart = Time.now
    lots.each {|lot|
      assert @@fodtest.verify_lot_fod(lot, tstart: tstart), 'FOD not set correctly'
    }
    #
    $setup_ok = true
  end


  def test11
    $setup_ok = false
    #
    # prepare RTDEmu answer for the test lots
    data = []
    duedate = Time.at(Time.now.to_i + 86400 * 3)  # 3 days from now
    due_ts = duedate.utc.iso8601
    memo = unique_string('QA')
    # lot0: full information
    assert tgt0 = @@fodtest.mds.lot_target(@@testlots[0], category: 'FOD').first, 'missing FOD data'
    data << [tgt0.lot, tgt0.opNo, tgt0.op, tgt0.route, due_ts, tgt0.category, memo]
    # lot1: empty opeNo and MainPDID
    assert tgt1 = @@fodtest.mds.lot_target(@@testlots[1], category: 'FOD').first, 'missing FOD data'
    data << [tgt1.lot, '', tgt1.op, '', due_ts, tgt1.category, memo]
    # lot2: wrong PdID
    assert tgt2 = @@fodtest.mds.lot_target(@@testlots[2], category: 'FOD').first, 'missing FOD data'
    data << [tgt2.lot, tgt2.opNo, 'QAQA.01', tgt2.route, due_ts, tgt2.category, memo]
    #
    # set RTDEmu answer and wait for the rule call
    assert @@rtdremote.add_emustation_wait('', rule: @@rtd_rule, timeout: @@job_interval) {
      @@rtdremote.create_response(@@columns, {'rows'=>data})
    }, 'rule has not been called'
    sleep 5
    #
    # verify lots 0 and 1 have been changed in the DB, lot2 was not touched
    uparam_ok = true
    @@testlots[0 .. 1].each {|lot|
      assert tgt_new = @@fodtest.mds.lot_target(lot, category: 'FOD').first, 'missing FOD data'
      assert_equal duedate, tgt_new.fod_commit, 'wrong due date'
      assert_equal memo, tgt_new.memo, 'wrong memo'
      uparam = @@sv.user_parameter('Lot', lot, name: 'FOD_COMMIT').value
      uparam_ok &= (uparam == due_ts[0 .. 9])
    }
    # just for information:
    $log.info "lot script parameters FOD_COMMIT have been updated: #{uparam_ok}"
    #
    assert tgt_new = @@fodtest.mds.lot_target(@@testlots[2], category: 'FOD').first, 'missing FOD data'
    assert_equal tgt2.claim_time, tgt_new.claim_time, 'lot has been touched'
    #
    $setup_ok = true
  end

end
