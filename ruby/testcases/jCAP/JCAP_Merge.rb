=begin
(c) Copyright 2011 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: Adel Darwaish, 2012-06-7

Version: 21.03

History:
  2015-04-02 ssteidte, cleanup
  2017-10-18 sfrieske, cleaner tests for extra X-MA holds
  2018-12-06 sfrieske, minor cleanup
  2021-02-23 sfrieske, added tests for v21.03
  2021-08-27 sfrieske, require waitfor instead of misc

Notes:
  The MergeJob looks for parent / child Lot pairs in the MDS with a merge hold.
  If SiView returns an error child lots may get an extra X-MA hold to draw attention,
  see property: siview.merge.return.codes.leading.to.hold
=end

require 'util/waitfor'
require 'SiViewTestCase'


class JCAP_Merge < SiViewTestCase
  @@sv_defaults = {carriers: 'JC%', nwafers: 10}
  @@sorter = 'UTS001'
  @@reason_ENG = 'ENG'
  @@holdreason2 = 'DQE'
  @@rework_route = 'UTRW001.01'
  @@rework_opNo = '1000.400'
  @@product2 = 'UT-PRODUCT002.01'
  @@route2 = 'UTRT001.02'
  @@slt2 = 'ES'
  @@split_nwafers = 2  # number of wafers to split (arbitrary number)

  @@reason_extrahold = 'X-MA'
  @@lots_extrahold = []   # lots with extra X-MA hold
  @@lots_merged = []      # completely merged lots

  @@job_interval = 330
  @@testlots = []


  def self.after_all_passed
    @@sv.eqp_cleanup(@@sorter)
    @@testlots.each {|lot| @@sv.delete_lot_family(lot)}
  end


  def test00_setup
    $setup_ok = false
    #
    assert @@svtest.cleanup_eqp(@@sorter, mode: 'Auto-1'), 'error cleaning up sorter'
    assert @@svtest.get_empty_carrier(n: 25), 'not enough empty carriers'
    #
    # lot setup
    assert lots = @@svtest.new_lots(23), 'error creating test lots'
    @@testlots = lots
    oplist = @@sv.lot_operation_list(@@testlots.last, raw: true)
    assert merge_opNo = oplist[1].operationNumber
    assert future_opNo = oplist[2].operationNumber
    #
    # 1 child: merge (at current PD, same code in the jCAP job as 'future PD')
    lot = @@testlots[0]
    assert_equal 0, @@sv.lot_opelocate(lot, merge_opNo)
    assert @@sv.lot_split(lot, @@split_nwafers, mergeop: merge_opNo), 'error splitting lot'
    @@lots_merged << lot
    #
    # 1 child, parent and child with same additional future hold: merge
    lot = @@testlots[1]
    assert_equal 0, @@sv.lot_opelocate(lot, merge_opNo)
    assert_equal 0, @@sv.lot_futurehold(lot, future_opNo, @@reason_ENG), 'error setting future hold'
    assert lc = @@sv.lot_split(lot, @@split_nwafers, mergeop: merge_opNo), 'error splitting lot'
    @@lots_merged << lot
    #
    # 1 child, different additional future holds: RC 301, additional X-MA hold
    lot = @@testlots[2]
    assert_equal 0, @@sv.lot_opelocate(lot, merge_opNo)
    assert child = @@sv.lot_split(lot, @@split_nwafers, mergeop: merge_opNo), 'error splitting lot'
    assert_equal 0, @@sv.lot_futurehold(lot, future_opNo, @@reason_ENG), "error setting future hold"
    assert_equal 0, @@sv.lot_futurehold(child, future_opNo, @@holdreason2), "error setting future hold"
    # assert_equal 301, @@sv.lot_merge(lot, child), 'wrong RC'
    @@lots_extrahold << child
    #
    # 1 child, parent ENG hold: no merge (note: SiView merge would succeed)
    lot = @@testlots[3]
    assert_equal 0, @@sv.lot_opelocate(lot, merge_opNo)
    assert @@sv.lot_split(lot, @@split_nwafers, mergeop: merge_opNo), 'error splitting lot'
    assert_equal 0, @@sv.lot_hold(lot, @@reason_ENG)
    ## extra hold is impossible, lot is not returned as candidate by DB query @@lots_extrahold << lot
    #
    # 1 child, child ENG hold: no merge
    lot = @@testlots[4]
    assert_equal 0, @@sv.lot_opelocate(lot, merge_opNo)
    assert lc = @@sv.lot_split(lot, @@split_nwafers, mergeop: merge_opNo), 'error splitting lot'
    assert_equal 0, @@sv.lot_hold(lc, @@reason_ENG)
    # assert_equal 802, @@sv.lot_merge(lot, lc), 'wrong RC'
    ## extra hold is impossible, lot is not returned as candidate by DB query @@lots_extrahold << child
    #
    # 1 child by partial rework: merge
    lot = @@testlots[5]
    assert_equal 0, @@sv.lot_opelocate(lot, @@rework_opNo)
    assert lc = @@sv.lot_partial_rework(lot, 2, @@rework_route)
    assert_equal 0, @@sv.lot_opelocate(lc, @@rework_opNo)
    @@lots_merged << lot
    #
    # 1 child by partial rework, parent ENG hold: no merge, no extra hold
    lot = @@testlots[6]
    assert_equal 0, @@sv.lot_opelocate(lot, @@rework_opNo)
    assert lc = @@sv.lot_partial_rework(lot, 2, @@rework_route)
    assert_equal 0, @@sv.lot_opelocate(lc, @@rework_opNo)
    assert_equal 0, @@sv.lot_hold(lot, @@reason_ENG)
    #
    # 2 children: merge 2 child lots
    lot = @@testlots[7]
    assert lc1 = @@sv.lot_split(lot, @@split_nwafers, mergeop: merge_opNo), 'error splitting lot'
    assert lc2 = @@sv.lot_split(lot, @@split_nwafers, mergeop: merge_opNo), 'error splitting lot'
    [lot, lc1, lc2].each {|l| assert_equal 0, @@sv.lot_opelocate(l, merge_opNo)}
    @@lots_merged << lot
    #
    # 2 children, parent ENG hold: no merge
    lot = @@testlots[8]
    assert lc1 = @@sv.lot_split(lot, @@split_nwafers, mergeop: merge_opNo), 'error splitting lot'
    assert lc2 = @@sv.lot_split(lot, @@split_nwafers, mergeop: merge_opNo), 'error splitting lot'
    assert_equal 0, @@sv.lot_hold(lot, @@reason_ENG)
    [lot, lc1, lc2].each {|l| assert_equal 0, @@sv.lot_opelocate(l, merge_opNo, force: :ondemand)}
    #
    # 2 children, one child ENG hold: partial merge
    lot = @@testlots[9]
    assert lc1 = @@sv.lot_split(lot, @@split_nwafers, mergeop: merge_opNo), 'error splitting lot'
    assert @@lot9_child2 = lc2 = @@sv.lot_split(lot, @@split_nwafers, mergeop: merge_opNo), 'error splitting lot'
    assert_equal 0, @@sv.lot_hold(@@lot9_child2, @@reason_ENG)
    [lot, lc1, lc2].each {|l| assert_equal 0, @@sv.lot_opelocate(l, merge_opNo, force: :ondemand)}
    #
    # child and grandchild: merge all
    lot = @@testlots[10]
    assert lc1 = @@sv.lot_split(lot, @@split_nwafers * 2, mergeop: merge_opNo), 'error splitting lot'
    assert @@grand_child16 = lgc11 = @@sv.lot_split(lc1, @@split_nwafers, mergeop: merge_opNo), 'error splitting lot'
    [lot, lc1, lgc11].each {|l| assert_equal 0, @@sv.lot_opelocate(l, merge_opNo)}
    @@lots_merged << lot
    #
    # 2 children and 2 grandchildren with future holds: merge child1 (special verification), extra hold for grandchildren RC 301
    lot = @@testlots[11]
    assert_equal 0, @@sv.lot_opelocate(lot, merge_opNo)
    assert @@lot11_child1 = lc1 = @@sv.lot_split(lot, @@split_nwafers, mergeop: merge_opNo), 'error splitting lot'
    assert lc2 = @@sv.lot_split(lot, @@split_nwafers * 3, mergeop: merge_opNo, onhold: true, inheritholds: false)
    assert lgc21 = @@sv.lot_split(lc2, @@split_nwafers, mergeop: future_opNo, onhold: true, inheritholds: false)
    assert lgc22 = @@sv.lot_split(lc2, @@split_nwafers, mergeop: future_opNo, onhold: true, inheritholds: false)
    @@lots_extrahold << lc2
    #
    # child and grandchild, parent ENG hold: merge child and grandchild
    lot = @@testlots[12]
    assert lc1 = @@sv.lot_split(lot, @@split_nwafers * 2, mergeop: merge_opNo), 'error splitting lot'
    assert @@lot12_grandchild = lgc11 = @@sv.lot_split(lc1, @@split_nwafers, mergeop: merge_opNo), 'error splitting lot'
    assert_equal 0, @@sv.lot_hold(lot, @@reason_ENG)
    [lot, lc1, lgc11].each {|l| assert_equal 0, @@sv.lot_opelocate(l, merge_opNo, force: :ondemand)}
    #
    # carrier xfer status "EI": merge (MSR 737488)
    lot = @@testlots[13]
    assert_equal 0, @@sv.lot_opelocate(lot, merge_opNo)
    assert @@sv.claim_process_prepare(lot, slr: false), 'error loading lot'
    assert @@sv.lot_split(lot, @@split_nwafers, mergeop: merge_opNo), 'error splitting lot'
    @@lots_merged << lot
    #
    # parent and child are in different carriers: RC 335, no extra hold
    lot = @@testlots[14]
    assert_equal 0, @@sv.lot_opelocate(lot, merge_opNo)
    assert lc = @@sv.lot_split(lot, @@split_nwafers, mergeop: merge_opNo), 'error splitting lot'
    assert ec = @@svtest.get_empty_carrier, 'no empty carrier'
    assert_equal 0, @@sv.wafer_sort(lc, ec)
    #
    # sorter job for parent: RC 1752, no extra hold
    lot = @@testlots[15]
    assert_equal 0, @@sv.lot_opelocate(lot, merge_opNo)
    assert lc = @@sv.lot_split(lot, @@split_nwafers, mergeop: merge_opNo)
    assert ec = @@svtest.get_empty_carrier, 'no empty carrier'
    assert @@sv.sj_create(@@sorter, 'P1', 'P2', lot, ec), 'error creating sorter job'
    # assert_equal 1752, @@sv.lot_merge(lot, lc), 'wrong RC'
    #
    # different products, RC 362, no X-MA hold possible!
    lot = @@testlots[16]
    assert_equal 0, @@sv.lot_opelocate(lot, merge_opNo)
    assert lc = @@sv.lot_split(lot, @@split_nwafers, mergeop: merge_opNo), 'error splitting lot'
    assert_equal 0, @@sv.schdl_change(lc, product: @@product2, route: @@route2), 'error changing product'
    # assert_equal 362, @@sv.lot_merge(lot, lc)
    ##@@lots_extrahold << lc  # will fail  TODO: why?
    #
    # different sublottypes, RC 159, X-MA hold
    lot = @@testlots[17]
    assert_equal 0, @@sv.lot_opelocate(lot, merge_opNo)
    assert lc = @@sv.lot_split(lot, @@split_nwafers, mergeop: merge_opNo)
    assert_equal 0, @@sv.sublottype_change(lc, @@slt2)
    # assert_equal 159, @@sv.lot_merge(lot, lc), 'wrong RC'
    @@lots_extrahold << lc
    #
    # same future hold but different claim memos, X-MA hold
    lot = @@testlots[18]
    assert_equal 0, @@sv.lot_opelocate(lot, merge_opNo)
    assert lc = @@sv.lot_split(lot, @@split_nwafers, mergeop: merge_opNo)
    assert_equal 0, @@sv.lot_futurehold(lc, future_opNo, @@holdreason2, memo: 'QA1')
    assert_equal 0, @@sv.lot_futurehold(lot, future_opNo, @@holdreason2, memo: 'QA2')
    # assert_equal 10323, @@sv.lot_merge(lot, lc), 'wrong RC'
    @@lots_extrahold << lc
    #
    # same merge holds but not at merge op, jCAP job tries to merge, RC 1302, X-MA hold
    lot = @@testlots[19]
    assert_equal 0, @@sv.lot_opelocate(lot, merge_opNo)
    assert lc = @@sv.lot_split(lot, @@split_nwafers, mergeop: merge_opNo)
    assert_equal 0, @@sv.lot_opelocate(lot, -1, force: true)
    assert_equal 0, @@sv.lot_opelocate(lc, -1, force: true)
    # assert_equal 1302, @@sv.lot_merge(lot, lc), 'wrong RC'
    @@lots_extrahold << lc
    #
    # parent and grandchild only (with manually set merge holds)
    lot = @@testlots[20]
    assert lc = @@sv.lot_split(lot, @@split_nwafers + 1), 'error splitting lot'
    assert lgc = @@sv.lot_split(lc, @@split_nwafers), 'error splitting lot'
    assert_equal 0, @@sv.lot_merge(lot, lc)
    assert_equal 0, @@sv.lot_futurehold(lot, future_opNo, 'MGHL', holdtype: 'MergeHold', related_lot: lgc)
    assert_equal 0, @@sv.lot_futurehold(lgc, future_opNo, 'MGHL', holdtype: 'MergeHold', related_lot: lot)
    assert_equal 0, @@sv.lot_opelocate(lot, future_opNo)
    assert_equal 0, @@sv.lot_opelocate(lgc, future_opNo)
    @@lots_merged << lot
    #
    # brothers with manually set merge holds, new in 18.04
    lot = @@testlots[21]
    assert_equal 0, @@sv.lot_opelocate(lot, merge_opNo)
    assert_equal 0, @@sv.sublottype_change(lot, 'RD')   # prevent AutoScrap from doing a ScrapCancel
    assert lcs = 2.times.collect {@@sv.lot_split(lot, @@split_nwafers)}
    # 1 great grandchild each that gets scrapped
    lcs.each {|l|
      assert lgc = @@sv.lot_split(l, 1, mergeop: future_opNo)
      assert_equal 0, @@sv.scrap_wafers(lgc)
    }
    # scrap parent and set merge holds between the children (!, but useless because of the other merge holds)
    assert_equal 0, @@sv.scrap_wafers(lot)
    assert_equal 0, @@sv.lot_futurehold(lcs[0], future_opNo, 'MGHL', holdtype: 'MergeHold', related_lot: lcs[1])
    assert_equal 0, @@sv.lot_futurehold(lcs[1], future_opNo, 'MGHL', holdtype: 'MergeHold', related_lot: lcs[0])
    lcs.each {|l| assert_equal 0, @@sv.lot_opelocate(l, future_opNo)}
    # cannot be merged because of the extra mergehold with parent, must not crash the job
    @@lots_extrahold += [lcs[0], lcs[1]]  # since 21.03
    #
    # different products with same route, RC 362, no X-MA hold possible!
    lot = @@testlots[22]
    assert_equal 0, @@sv.lot_opelocate(lot, merge_opNo)
    assert lc = @@sv.lot_split(lot, @@split_nwafers, mergeop: merge_opNo), 'error splitting lot'
    assert_equal 0, @@sv.schdl_change(lc, product: @@product2, route: 'UTRT001.01'), 'error changing product'
    @@lots_extrahold << lc
    #
    # consecutive multi-merge with 24 great.. grandchild lots
    assert lot = @@svtest.new_lot(nwafers: 25), 'error creating test lot'
    @@testlots << lot
    plot = lot
    lots = [lot]
    24.times {|i|
      assert plot = @@sv.lot_split(plot, 24 - i, mergeop: merge_opNo), 'error splitting lot'
      lots << plot
    }
    # locate all lots but last grand..child to merge PD
    lots[0..-2].each {|l| assert_equal 0, @@sv.lot_opelocate(l, merge_opNo)}
    #
    $setup_ok = true
  end

  def test01_general
    # wait for jCAP job
    $log.info "waiting #{@@job_interval} s for the jCAP job to run"; sleep @@job_interval
    #
    # verify lots are merged or not and have or don't have the extra X-MA hold
    ok = true
    @@testlots.each_with_index {|lot, i|
      $log.info "verifying lot ##{i} (#{lot})"
      # check merge
      lf = @@sv.lot_family(lot)
      if @@lots_merged.member?(lot)
        ($log.warn "  not merged: #{lf}"; ok = false) if lf.size > 1
      else
        ($log.warn '  wrong merge'; ok = false) if lf.size == 1
      end
      # check parent and child lots for extra holds
      lf.each {|lc|
        hh = @@sv.lot_hold_list(lc, reason: @@reason_extrahold)
        if @@lots_extrahold.member?(lc)
          ($log.warn "  missing X-MA hold for #{lc}"; ok=false) if hh.empty?
        else
          ($log.warn "  wrong X-MA hold for #{lc}"; ok=false) unless hh.empty?
        end
      }
    }
    assert ok, 'some tests failed, see log'
  end


  # verify partial merges

  def test11_partially_merge_child_onhold
    lot = @@testlots[9]
    lf = @@sv.lot_family(lot)
    # child with ENG hold not merged, the other one is
    assert_equal 2, lf.size, 'merge error'
    assert lf.member?(@@lot9_child2), "wrong child remains after jCAP merge, expected: #{@@lot9_child2}"
  end

  def test12_partial_merge_child_grandchild
    lot = @@testlots[11]
    lf = @@sv.lot_family(lot)
    # only the child without grandchild lots is merged to the parent
    assert_equal 4, lf.size, 'merge error'
    refute lf.member?(@@lot11_child1), "wrong merge of child #{@@lot11_child1}"
  end

  def test13_partial_merge_grandchild
    lot = @@testlots[12]
    lf = @@sv.lot_family(lot)
    # because parent has an ENG Hold, only child & its grand child will be merged
    assert_equal 2, lf.size, "lots were merged: #{lf.inspect}"
    refute lf.member?(@@lot12_grandchild), "grand child not merged correctly: #{lf.inspect}"
  end

  # verify merge of deeply nested grandchildren

  def test21_nested_grandchildren
    # no merge because last grand..child is not yet mergeable
    lot = @@testlots.last
    lf = @@sv.lot_family(lot)
    assert_equal 25, lf.size, 'wrong merge'
    # locate last grand..child to merge pd and wait for the jCAP job to merge it
    assert_equal 0, @@sv.lot_opelocate(lf.last, 1), 'error locating lot'
    # must be merged in 1 job run, v21.03 (works, but only in the 2nd run, why?)
    ##assert_equal 1, @@sv.lot_family(lot).size, 'lots not completely merged'
    assert wait_for(timeout: @@job_interval, sleeptime: 10) {
      nlots = @@sv.lot_family(lot).size
      $log.info {"waiting for #{nlots - 1} lots to get merged"} if nlots > 1
      nlots == 1
    }, 'lots not completely merged'
  end

end
