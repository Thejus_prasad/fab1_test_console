=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.


Author: Steffen Frieske, 2021-09-14


History:

Notes:
  see http://f1onewiki:21080/display/JCAP/ReworkRouter
      https://app.diagrams.net/#G17uIimWkJrArnaWA_XiHrNymv9jpJaWSS
  MSR 1827597 (reason codes)

  properties maxreworklimit (<, per lot)  Prod: 1, ITDC: 3,
             reducereworklimit (Prod and ITDC): 1
=end

require 'catalyst/waferselection'
require 'SiViewTestCase'


class JCAP_ReworkRouter < SiViewTestCase
  @@sv_defaults = {product: 'UT-PRODUCT-RWKROUTER.01', route: 'UTRT-RWKROUTER.01', nwafers: 5, carriers: 'JC%'}
  # generic script parameter setup
  @@uparam_Module_Max_Rework_Limit = '4'
  @@uparam_Module_Undefine_Rework_Limit = '0'   # 0 for LIT, builtin default is 3
  # for wafer selection, op is the operation where lot rework is started from
  @@op = 'UTPDRWKROUTER-LIT.01'
  @@ptype = 'X-ReworkLit-Auto'  # virtual, from property, PD has 'QA-PTYPE'
  @@player = 'QA-PLAYER'
  # operations where Rework is defined on in SM, 1 step before op(!)
  @@opNos_rwk = [
    '1100.0200',  # 1 valid rework route
    '1200.0200',  # 1 valid rework route + 1 unvalid (no UDATA)
    '1300.0200',  # 2 valid rework routes
    '1400.0200',  # 1 invalid rework route
  ]
  @@opNo_norework = '1500.0200'   # no rework route but rework limit, limit is 0
  @@opNo_lowlimit = '2100.0200'   # valid rework route with low rework limit (2)
  @@opNo_nolimit  = '3100.0200'   # valid rework route at non-LIT PD, rework limit is 9999
  @@rwkuname = 'SubRouteScenario'
  @@rwkuvalue = 'SPACECA'
  @@rwkroute1 = 'UT-RWKRTE.01'
  # OOC FutureHolds and related Rework reasons, MSR 1827597
  @@futureholds = {'X-RWCD'=>'L-CD', 'X-RWCB'=>'L-CB', 'X-RWOV'=>'L-OV', 'X-RWAP'=>'L-AP', 'X-RWHS'=>'L-HS'}
  @@holdreason_trigger = 'X-MR'
  @@holdreason_error = 'X-MRE'
  @@holdreason_extra = 'ENG'
  # error hold claim memos (compared with include?())
  @@error_memos = {
    nowafersmarked:       'No rework-marked wafers',
    # missinglimit:         'Rework limit is not set',  # not testable
    limitreached:         'limit already reached',
    noreworkroute:        'route not found',
    norouteUDATA:         'No rework route has udata',
    multipleroutes:       'More than one rework route',
    # noammoreply:          'message from AMMO is missing',   # not testable
    oocholdmissing:       'OOC hold is missing',
    extrahold:            'Not supported hold',
    # reworkfailed:         'rework failed ',           # not testable
    # partialreworkfailed:  'Partial rework failed',    # not testable
  }
  @@rwkholduser = 'X-CAT'

  @@job_interval = 320  #620
  @@testlots = []


  def self.after_all_passed
    @@testlots.each {|lot| @@sv.delete_lot_family(lot)}
    nil
  end


  def test00_setup
    $setup_ok = false
    #
    assert @@wafersel = Catalyst::WaferSelection.new($env)
    #
    assert_equal 'ON', @@sv.environment_variable[1]['CS_INFINITE_MODULE_REWORK_LIMIT'], 'wrong MM server env variable'
    #
    # Factory script parameters, any object id works
    assert uparams = @@sv.user_parameter('Factory', 'X'), 'no Factory script parameters'
    assert uparam = uparams.find {|e| e.name == 'Module_Max_Rework_Limit'}, 'no Module_Max_Rework_Limit'
    assert_equal @@uparam_Module_Max_Rework_Limit, uparam.value, 'wrong Module_Max_Rework_Limit'
    assert uparam = uparams.find {|e| e.name == 'Module_Undefine_Rework_Limit'}, 'no Module_Undefine_Rework_Limit'
    assert_equal @@uparam_Module_Undefine_Rework_Limit, uparam.value, 'wrong Module_Undefine_Rework_Limit'
    assert uraw = @@sv.user_parameter('Factory', 'X', raw: true)
    assert uraw.parameters.find {|e|
      e.parameterName == 'G_LithoRwkCheckDept' && e.value == 'LIT'
    }, 'LIT not configured as G_LithoRwkCheckDept'
    #
    # UDATA checks
    udata = @@sv.user_data_pd(@@op)
    refute_equal @@ptype, udata['ProcessTypeWfrSelection'], 'wrong PD UDATA ProcessTypeWfrSelection'
    assert_equal @@player, udata['ProcessLayer'], 'wrong PD UDATA ProcessLayer'
    #
    # module rework limits and PD departments
    rops = @@sv.route_operations(@@svtest.route)
    @@opNos_rwk.each {|opNo|
      assert_equal 'LIT', rops.find {|e| e.operationNumber == opNo}.departmentNumber, 'wrong department'
      modNo = opNo.split('.').first
      assert_equal 4, @@sv.module_rework_limits(@@svtest.route, modNo).moduleReworkLimit, "wrong reworklimit for #{modNo}"
    }
    #
    assert_equal 'LIT', rops.find {|e| e.operationNumber == @@opNo_lowlimit}.departmentNumber, 'wrong department'
    modNo = @@opNo_lowlimit.split('.').first
    assert_equal 2, @@sv.module_rework_limits(@@svtest.route, modNo).moduleReworkLimit, "wrong reworklimit for #{modNo}"
    #
    assert_equal 'LIT', rops.find {|e| e.operationNumber == @@opNo_nolimit}.departmentNumber, 'wrong department'
    modNo = @@opNo_nolimit.split('.').first
    assert_equal 9999, @@sv.module_rework_limits(@@svtest.route, modNo).moduleReworkLimit, "wrong reworklimit for #{modNo}"
    #
    assert_equal 'LIT', rops.find {|e| e.operationNumber == @@opNo_norework}.departmentNumber, 'wrong department'
    modNo = @@opNo_norework.split('.').first
    assert_equal 0, @@sv.module_rework_limits(@@svtest.route, modNo).moduleReworkLimit, "wrong reworklimit for #{modNo}"
    #
    $setup_ok = true
  end

  def test01_prepare_testlots
    $setup_ok = false
    #
    assert @@svtest.get_empty_carrier(n: 18), 'not enough empty carriers'
    @@lots_err = []                     # no rework, error hold
    @@lots_triggerhold_preserved = []   # error hold, trigger hold preserved
    @@lots_full = []      # full rework
    @@lots_partial = []   # partial rework
    @@lots_ignored = []   # not touched TODO: more specific
    #
    # lot0: happy, FutureHold and LotHold
    assert lot = @@svtest.new_lot, 'error creating test lot'
    @@testlots << lot
    prepare_lot(lot, @@opNos_rwk[0], 'X-RWOV')
    @@lots_full << [lot, {route: @@svtest.route, opNo: @@opNos_rwk[0], pass: 1}]
    #
    # lot1: happy, FutureHold and LotHold, partial rework
    assert lot = @@svtest.new_lot, 'error creating test lot'
    @@testlots << lot
    prepare_lot(lot, @@opNos_rwk[0], 'X-RWOV', [1, 2])
    @@lots_partial << [lot, {route: @@svtest.route, opNo: @@opNos_rwk[0], pass: 1}]
    #
    # lot2: happy, FutureHold and LotHold, 1 valid and 1 invalid rework route (by UDATA)
    assert lot = @@svtest.new_lot, 'error creating test lot'
    @@testlots << lot
    prepare_lot(lot, @@opNos_rwk[1], 'X-RWOV')
    @@lots_full << [lot, {route: @@svtest.route, opNo: @@opNos_rwk[1], pass: 1}]
    #
    # lot3: FutureHold and LotHold, 2 valid rework routes (by UDATA) -> error
    assert lot = @@svtest.new_lot, 'error creating test lot'
    @@testlots << lot
    prepare_lot(lot, @@opNos_rwk[2], 'X-RWOV')
    @@lots_err << [lot, :multipleroutes]
    @@lots_triggerhold_preserved << lot
    #
    # lot4: FutureHold and LotHold, invalid rework route (no UDATA) -> error
    assert lot = @@svtest.new_lot, 'error creating test lot'
    @@testlots << lot
    prepare_lot(lot, @@opNos_rwk[3], 'X-RWOV')
    @@lots_err << [lot, :norouteUDATA]
    @@lots_triggerhold_preserved << lot
    #
    # lot5: FutureHold and LotHold, no rework route -> error, release trigger
    assert lot = @@svtest.new_lot, 'error creating test lot'
    @@testlots << lot
    prepare_lot(lot, @@opNo_norework, 'X-RWOV')
    @@lots_err << [lot, :limitreached]  # limit is reported as 0 although set
    #
    # lot6: nolimit never occurs   FutureHold and LotHold, no rework limit -> error, release trigger
    assert lot = @@svtest.new_lot, 'error creating test lot'
    @@testlots << lot
    # prepare_lot(lot, @@opNo_nolimit, 'X-RWOV')
    # @@lots_err << [lot, :missinglimit]  # limit is reported as -1
    #
    # lot7: FutureHold and LotHold, no wafer selection -> error, preserve trigger
    assert lot = @@svtest.new_lot, 'error creating test lot'
    @@testlots << lot
    assert_equal 0, @@sv.lot_futurehold(lot, @@opNos_rwk[0], 'X-RWOV', post: true), 'error setting FutureHold'
    assert_equal 0, @@sv.lot_opelocate(lot, @@opNos_rwk[0]), 'error locating lot'
    assert_equal 0, @@sv.lot_gatepass(lot), 'error gatepassing lot'
    @@lots_err << [lot, :nowafersmarked]
    @@lots_triggerhold_preserved << lot
    #
    # lot8: No FutureHold -> error
    assert lot = @@svtest.new_lot, 'error creating test lot'
    @@testlots << lot
    li = @@sv.lot_info(lot, wafers: true)
    assert @@wafersel.set_selection(li, @@ptype, @@player)
    assert_equal 0, @@sv.lot_opelocate(lot, @@opNos_rwk[0]), 'error locating lot'
    assert_equal 0, @@sv.lot_gatepass(lot), 'error gatepassing lot'
    @@lots_err << [lot, :oocholdmissing]
    @@lots_triggerhold_preserved << lot
    #
    # lot9: Extra Hold -> error, preserve trigger
    assert lot = @@svtest.new_lot, 'error creating test lot'
    @@testlots << lot
    prepare_lot(lot, @@opNos_rwk[0], 'X-RWOV')
    assert @@sv.lot_hold(lot, @@holdreason_extra), 'error setting extra hold'
    @@lots_err << [lot, :extrahold]
    @@lots_triggerhold_preserved << lot
    #
    # lot10: Error Hold -> ignored
    assert lot = @@svtest.new_lot, 'error creating test lot'
    @@testlots << lot
    prepare_lot(lot, @@opNos_rwk[0], 'X-RWOV')
    assert @@sv.lot_hold(lot, @@holdreason_error), 'error setting error hold'
    @@lots_ignored << [lot, :extrahold]
    @@lots_triggerhold_preserved << lot
    #
    # lot11: No FutureHold but no trigger Hold -> ignored
    assert lot = @@svtest.new_lot, 'error creating test lot'
    @@testlots << lot
    prepare_lot(lot, @@opNos_rwk[0], 'X-RWOV')
    @@lots_ignored << [lot, :extrahold]
    @@lot_notrigger = lot
    #
    # lot12: happy but rework limit exceeded -> error, release trigger
    assert lot = @@svtest.new_lot, 'error creating test lot'
    @@testlots << lot
    rework_lot(lot, @@opNo_lowlimit, 2)
    assert_equal 0, @@sv.lot_opelocate(lot, :first)
    prepare_lot(lot, @@opNo_lowlimit, 'X-RWOV')
    @@lots_err << [lot, :limitreached]
    #
    # lot13: happy but rework limit exceeded for 1 wafer -> error
    assert lot = @@svtest.new_lot, 'error creating test lot'
    @@testlots << lot
    assert lc = @@sv.lot_split(lot, [3]), 'error splitting lot'
    rework_lot(lc, @@opNo_lowlimit, 2)
    assert_equal 0, @@sv.lot_opelocate(lc, :first)
    assert_equal 0, @@sv.lot_merge(lot, lc), 'error merging lot'
    prepare_lot(lot, @@opNo_lowlimit, 'X-RWOV')
    @@lots_err << [lot, :limitreached]
    #
    # lot14: partial rework while limit exceeded for 1 wafer -> partial rework
    assert lot = @@svtest.new_lot, 'error creating test lot'
    @@testlots << lot
    assert lc = @@sv.lot_split(lot, [3]), 'error splitting lot'
    rework_lot(lc, @@opNo_lowlimit, 2)
    assert_equal 0, @@sv.lot_opelocate(lc, :first)
    assert_equal 0, @@sv.lot_merge(lot, lc), 'error merging lot'
    prepare_lot(lot, @@opNo_lowlimit, 'X-RWOV', [1, 2])
    @@lots_partial << [lot, {route: @@svtest.route, opNo: @@opNo_lowlimit, pass: 1}]
    #
    # lot15: rework in other module -> full rework
    assert lot = @@svtest.new_lot, 'error creating test lot'
    @@testlots << lot
    rework_lot(lot,  @@opNos_rwk[0], 2)
    prepare_lot(lot, @@opNo_lowlimit, 'X-RWOV')
    @@lots_full << [lot, {route: @@svtest.route, opNo: @@opNo_lowlimit, pass: 1}]
    #
    # lot16: reduce rework limit -> error
    assert lot = @@svtest.new_lot, 'error creating test lot'
    @@testlots << lot
    rework_lot(lot, @@opNo_lowlimit, 1)
    assert_equal 0, @@sv.lot_opelocate(lot, :first)
    prepare_lot(lot, @@opNo_lowlimit, 'X-RWOV')
    @@lots_err << [lot, :limitreached]
    #
    # lot17: multiple OOC holds -> full rework
    assert lot = @@svtest.new_lot, 'error creating test lot'
    @@testlots << lot
    assert_equal 0, @@sv.lot_futurehold(lot, @@opNos_rwk[0], 'X-RWCD', post: true), 'error setting FutureHold'
    prepare_lot(lot, @@opNos_rwk[0], 'X-RWOV')
    @@lots_full << [lot, {route: @@svtest.route, opNo: @@opNos_rwk[0], pass: 1}]
    #
    # set the trigger holds and wait for the job to run
    @@tstart = Time.now
    @@testlots.each {|lot|
      next if lot == @@lot_notrigger
      assert @@sv.lot_hold(lot, @@holdreason_trigger, user: @@rwkholduser), 'error setting trigger hold'
    }
    $log.info "waiting #{@@job_interval} s for the jCAP job to run"; sleep @@job_interval
    #
    $setup_ok = true
  end

  def test81_full_rwk
    failed = Set.new
    @@lots_full.each {|entry|
      lot, hparams = entry
      $log.info "#{lot}"
      li = @@sv.lot_info(lot)
      if li.route != @@rwkroute1
        $log.warn '  wrong route'
        failed << lot
      end
      if @@sv.lot_family(lot).size != 1
        $log.warn '  wrong split'
        failed << lot
      end
      if !@@sv.lot_hold_list(lot).empty?
        $log.warn '  wrong hold'
        failed << lot
      end
      next if failed.include?(lot)
      hops = @@sv.lot_operation_history(lot, hparams.merge(opCategory: 'Rework', raw: true))
      if hops.empty?
        $log.warn "  no such history entry: #{hparams}"
        failed << lot
      elsif hops.first.reasonCodeID != 'L-OV'
        $log.warn "  wrong rework reason: #{hops.first.reasonCodeID}"
        failed << lot
      end
    }
    assert_empty failed
  end

  def test82_partial_rwk
    failed = Set.new
    @@lots_partial.each {|entry|
      lot, hparams = entry
      $log.info "#{lot}"
      lf = @@sv.lot_family(lot)
      if lf.size != 2
        $log.warn '  no partial rework'
        failed << lot
      end
      li = @@sv.lot_info(lot)
      if li.route != @@svtest.route
        $log.warn '  wrong route for parent'
        failed << lot
      end
      lhs = @@sv.lot_hold_list(lot)
      if lhs.size != 1 && lhs.first.type != 'ReworkHold'
        $log.warn '  wrong hold for parent'
        failed << lot
      end
      lc = lf.last
      lci = @@sv.lot_info(lc)
      if lci.route != @@rwkroute1
        $log.warn '  wrong route for child'
        failed << lc
      end
      if !@@sv.lot_hold_list(lc).empty?
        $log.warn '  wrong hold for child'
        failed << lc
      end
      if !failed.include?(lc)
        lhistop = @@sv.lot_operation_history(lc, hparams.merge(opCategory: 'Rework', raw: true)).first
        if lhistop.reasonCodeID != 'L-OV'
          $log.warn "  wrong rework reason: #{lhistop.reasonCodeID}"
          failed << lc
        end
      end
    }
    assert_empty failed
  end

  def test83_errorholds
    failed = Set.new
    @@lots_err.each {|entry|
      lot, reason = entry
      $log.info "#{lot}, #{reason}"
      li = @@sv.lot_info(lot)
      if li.route != @@svtest.route
        $log.warn "  wrong route: #{li.route}"
        failed << entry
        next
      end
      lh = @@sv.lot_hold_list(lot, reason: @@holdreason_error).first
      if lh.nil?
        $log.warn '  missing error hold'
        failed << entry
      else
        s = @@error_memos[reason]
        if !lh.memo.include?(s)
          $log.warn "  wrong error hold memo, expected: #{s}, got: #{lh.memo}"
          failed << entry
        end
      end
      lhs = @@sv.lot_hold_list(lot, reason: @@holdreason_trigger)
      if @@lots_triggerhold_preserved.include?(lot)
        if lhs.empty?
          $log.warn '  missing trigger hold'
          failed << entry
        end
      else
        if !lhs.empty?
          $log.warn '  unexpected trigger hold'
          failed << entry
        end
      end
    }
    assert_empty failed
  end

  def test84_untouched
    failed = Set.new
    @@lots_ignored.each {|entry|
    lot, desc = entry
    $log.info "#{lot}, #{desc}"
      li = @@sv.lot_info(lot)
      if li.user != @@sv.user
        $log.warn "wrong claim user for #{lot}"
        failed << lot
      end
    }
    assert_empty failed
  end

  def test99_testlots
    $log.info "testlots:\n#{@@testlots}"
  end


  # aux methods

  def prepare_lot(lot, opNo, fhreason, slots=nil)
    li = @@sv.lot_info(lot, wafers: true)
    assert @@wafersel.set_selection(li, @@ptype, @@player, slots: slots)
    assert_equal 0, @@sv.lot_futurehold(lot, opNo, fhreason, post: true), 'error setting FutureHold'
    assert_equal 0, @@sv.lot_opelocate(lot, opNo), 'error locating lot'
    assert_equal 0, @@sv.lot_gatepass(lot), 'error gatepassing lot'
  end

  def rework_lot(lot, opNo, n)
    n.times {
      assert_equal 0, @@sv.lot_opelocate(lot, opNo, offset: 1)
      assert_equal 0, @@sv.lot_rework(lot, @@rwkroute1)
      while @@sv.lot_info(lot).route == @@rwkroute1
        assert_equal 0, @@sv.lot_gatepass(lot)
      end
    }
  end

end
