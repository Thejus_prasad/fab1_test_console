=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author:  Steffen Steidten, 2013-11-22

Version: 20.04

History:
  2015-09-08 ssteidte, cleanup
  2016-01-15 sfrieske, adapted for v16.01
  2016-08-24 sfrieske, use lib rqrsp_mq instead of rqrsp
  2017-06-01 sfrieske, minor fixes
  2019-01-24 sfrieske, minor cleanup
  2020-04-24 sfrieske, added tests for v20.04
=end

require 'RubyTestCase'
require 'jcap/lmtest'


# Test LotManager2RTD Transformer, API v1.1
class JCAP_LMTrans < RubyTestCase
  @@orig_lots = ['QALot11', 'QALot21', 'QALot31']
  @@add_lots = ['QALot97', 'QALot98', 'QALot99']

  @@orig_ops = ['QAPD11', 'QAPD21', 'QAPD31']
  @@add_ops = ['QAPD97', 'QAPD98', 'QAPD99']

  @@orig_eqps = ['QAEQP11', 'QAEQP21', 'QAEQP31']
  @@add_eqps = ['QAEQP97', 'QAEQP98', 'QAEQP99']


  def self.startup  
    # no SiView::MM required
    @@lmtest = LotManager::Test.new($env)
    @@lmdb = @@lmtest.db
    @@lmtrans = @@lmtest.transformer
  end


  def setup
    @@lmtrans.set_default_data
  end


  def test00_setup
    $setup_ok = false
    #
    assert @@lmtrans.get_version =~ /\d\.\d/, 'no connection to LmTransformer'
    #
    $setup_ok = true
  end

  # LM_REQUEST

  def test11_valid_data
    $setup_ok = false
    #
    assert @@lmtrans.update_lmrequests
    assert @@lmtest.verify_lmrequest_data
    sleep 1
    assert @@lmtrans.change_default_data
    assert @@lmtrans.update_lmrequests
    assert @@lmtest.verify_lmrequest_data
    #
    $setup_ok = true
  end

  def test12_wrong_data
    data = @@lmtrans.lmrequest_data[1]
    data.expiration = 'qaqaqa'  # timestamp required
    assert !@@lmtrans.update_lmrequests(data: data), 'data must be rejected'
    assert_equal 'error', @@lmtrans.mq.service_response[:GenericResponse11][:Result]['Message']
  end

  def test13_mixed
    # good reference data
    data = @@lmtrans.lmrequest_data.take(3)
    assert @@lmtrans.update_lmrequests
    assert @@lmtest.verify_lmrequest_data
    sleep 2
    # new data with wrong timestamp in 2nd record
    @@lmtrans.set_default_data
    data += @@lmtrans.lmrequest_data.take(3)
    data[4].expiration = 'qaqaqa'  # wrong timestamp in 1 record, others are valid
    refute @@lmtrans.update_lmrequests
    assert_equal 'partial', @@lmtrans.mq.service_response[:GenericResponse11][:Result]['Message']
    # make sure that all data is still correct, the wrong data went not into the db
    assert @@lmtest.verify_lmrequest_data(data: data[3]), 'wrong data stored'
    refute @@lmtest.verify_lmrequest_data(data: data[4]), 'wrong data stored'  # not updated
    assert @@lmtest.verify_lmrequest_data(data: data[1]), 'wrong data stored'
    assert @@lmtest.verify_lmrequest_data(data: data[5]), 'wrong data stored'
  end

  def test14_long_memo
    memo = 'QA' * 256
    @@lmtrans.lmrequest_data[1].claim_memo = memo
    assert @@lmtrans.update_lmrequests
    assert record = @@lmdb.lmrequest(@@lmtrans.lmrequest_data[0].rqid).first
    assert record = @@lmdb.lmrequest(@@lmtrans.lmrequest_data[1].rqid).first
    assert_equal "#{memo[0..251]} ...", record.claim_memo, 'wrong memo stored'
  end

  def OBStest15_special_chars
    memo = "abc\xE4\xF6\xFC\xDF#+\\:/&%$\xA7\xFE xyz"
    @@lmtrans.lmrequest_data[1].claim_memo = memo
    assert @@lmtrans.update_lmrequests
    @@lmtrans.lmrequest_data[1].claim_memo = memo.force_encoding('iso-8859-1').encode('UTF-8')
    assert @@lmtest.verify_lmrequest_data
  end

  def test16_delete_request
    rqid = @@lmtrans.lmrequest_data[1].rqid
    assert @@lmtrans.delete_lmrequests(rqid)
    assert_equal nil, @@lmdb.lmrequest(rqid).first, "record #{rqid} was not deleted"
  end

  def test17_lot_withcategory
    clots = {'PLTMGR.Cat1'=>['QAPLT11', 'QAPLT12'], 'PLTMGR.Cat2'=>['QAPLT21']}
    @@lmtrans.lmrequest_data[1].lots_withcategory = clots
    assert @@lmtrans.update_lmrequests
    assert @@lmtest.verify_lmrequest_data
  end

  # LotSummary

  def test21_valid_data
    $setup_ok = false
    #
    assert @@lmtrans.update_lotsummary
    assert @@lmtest.verify_lotsummary_data
    sleep 1
    assert @@lmtrans.change_default_data
    assert @@lmtrans.update_lotsummary
    assert @@lmtest.verify_lotsummary_data
    #
    $setup_ok = true
  end

  def test22_wrong_data
    assert @@lmtrans.update_lotsummary
    assert @@lmtest.verify_lotsummary_data
    sleep 5
    data = @@lmtrans.lotsummary_data[0].clone
    data.pclbase = data.pclbase.clone
    data.pclbase.selwafers_time = 'qaqaqa'
    assert !@@lmtrans.update_lotsummary(data: data)
    assert_equal 'error', @@lmtrans.mq.service_response[:GenericResponse11][:Result]['Message']
    # original data are kept
    assert @@lmtest.verify_lotsummary_data
  end

  def test23_mixed
    assert @@lmtrans.update_lotsummary
    assert @@lmtest.verify_lotsummary_data
    oldtoolwish = @@lmtrans.lotsummary_data[2].pclbase.toolwish
    @@lmtrans.lotsummary_data[0].pclbase.toolwish = 'QAEQP11-PARTIAL'
    @@lmtrans.lotsummary_data[1].pclbase.toolwish = 'QAEQP21-PARTIAL'
    @@lmtrans.lotsummary_data[2].pclbase.toolwish = 'QAEQP31-PARTIAL'
    oldtime = @@lmtrans.lotsummary_data[2].pclbase.selwafers_time
    @@lmtrans.lotsummary_data[2].pclbase.selwafers_time = 'qaqaqa'  # timestamp required
    assert !@@lmtrans.update_lotsummary
    assert_equal 'partial', @@lmtrans.mq.service_response[:GenericResponse11][:Result]['Message']
    # verify the 3rd record has not be saved
    @@lmtrans.lotsummary_data[2].pclbase.toolwish = oldtoolwish
    @@lmtrans.lotsummary_data[2].pclbase.selwafers_time = oldtime
    assert @@lmtest.verify_lotsummary_data
  end

  def test24_long_memo
    memo = 'QA' * 256
    @@lmtrans.lotsummary_data[0].pclbase.toolwish_memo = memo
    assert @@lmtrans.update_lotsummary
    assert record = @@lmdb.lotsummary(context: @@lmtrans.lotsummary_data[0].context).first
    assert_equal "#{memo[0..251]} ...", record.pclbase.toolwish_memo, 'wrong memo stored'
  end

  def OBStest25_special_characters
    memo = "abc\xE4\xF6\xFC\xDF#+\\:/&%$\xA7\xFE xyz"   #'abcäöüß#+\/&%$§\xfe xyz', ANSI characters
    @@lmtrans.lotsummary_data[0].pclbase.toolwish_memo = memo
    assert @@lmtrans.update_lotsummary
    @@lmtrans.lotsummary_data[0].pclbase.toolwish_memo = memo.force_encoding('iso-8859-1').encode('UTF-8')
    assert @@lmtest.verify_lotsummary_data
  end

  def test26_qtskip_no_tgttime
    assert @@lmtrans.update_lotsummary
    assert @@lmtest.verify_lotsummary_data
    sleep 5
    data = @@lmtrans.lotsummary_data[0].clone
    data.pclqtskip = data.pclqtskip.clone
    data.pclqtskip.target_time = ''
    assert !@@lmtrans.update_lotsummary(data: data)
    assert_equal 'error', @@lmtrans.mq.service_response[:GenericResponse11][:Result]['Message']
    # original data are kept
    assert @@lmtest.verify_lotsummary_data
  end

  def test27_no_sitesampling
    assert @@lmtrans.update_lotsummary
    assert @@lmtest.verify_lotsummary_data
    sleep 5
    @@lmtrans.lotsummary_data[0].pclsitesamplings = []
    assert @@lmtrans.update_lotsummary(export: "log/#{__method__}.xml")
    assert @@lmtest.verify_lotsummary_data
  end

  def test28_minimal_request
    # cleanup
    assert @@lmtrans.delete_lotsummary
    @@lmtrans.lotsummary_data.each {|dat| assert_empty @@lmtest.db.lotsummary(context: dat.context)}
    # send minimal request
    data = 3.times.collect {|i|
      LotManager::LotSummary.new(
        LotManager::LmContext.new(nil, "QALot#{i+1}1", "QAPD#{i+1}1", "QAEQP#{i+1}1"), nil, nil, [], [], [], [], [], [], []
      )
    }
    assert @@lmtrans.update_lotsummary(data: data)
    assert @@lmtest.verify_lotsummary_data(data: data)
    # update
    @@lmtrans.set_default_data
    assert @@lmtrans.update_lotsummary
    assert @@lmtest.verify_lotsummary_data
  end

  # DeleteLotSummary

  def test51_delete_lotsummary
    assert @@lmtrans.update_lotsummary
    assert @@lmtest.verify_lotsummary_data
    assert @@lmtrans.delete_lotsummary
    assert !@@lmtest.verify_lotsummary_data, 'wrong, the request record was not deleted'
  end

  def test52_delete_lotsummary_wildcard
    [true, false].repeated_permutation(3).each {|p|
      next if p == [true, true, true]
      lot, op, eqp = p
      $log.info "-- wildcards: #{lot}, #{op}, #{eqp}"
      # set clean data
      assert @@lmtrans.set_default_data
      assert @@lmtrans.update_lotsummary
      assert @@lmtest.verify_lotsummary_data
      # change data
      dataorig = @@lmtrans.lotsummary_data.collect {|data|
        d = data.clone
        d.context = d.context.clone
        d
      }
      @@add_lots.each_with_index {|e, i| @@lmtrans.lotsummary_data[i].context.lot = e} if lot
      @@add_ops.each_with_index {|e, i| @@lmtrans.lotsummary_data[i].context.pd = e} if op
      @@add_eqps.each_with_index {|e, i| @@lmtrans.lotsummary_data[i].context.eqp = e} if eqp
      assert @@lmtrans.update_lotsummary
      assert @@lmtest.verify_lotsummary_data
      # wildcard delete
      data = @@lmtrans.lotsummary_data[0].clone
      data.context = data.context.clone
      data.context.lot = '*' if lot
      data.context.pd = '*' if op
      data.context.eqp = '*' if eqp
      assert @@lmtrans.delete_lotsummary(data: data)
      # verify that the selected old and new contexts have been deleted
      assert !@@lmtest.verify_lotsummary_data(data: dataorig[0])
      assert !@@lmtest.verify_lotsummary_data(data: @@lmtrans.lotsummary_data[0]), 'record was not deleted'
      # verify the other old contexts have not been deleted
      dataorig[1..-1].each {|data| assert @@lmtest.verify_lotsummary_data(data: data)}
      # verify the other new contexts have not been deleted
      @@lmtrans.lotsummary_data[1..-1].each {|data| assert @@lmtest.verify_lotsummary_data(data: data)}
    }
  end

  def test53_delete_lotsummary_3wildcards_declined
    # 3 wildcards
    data = @@lmtrans.lotsummary_data.first
    data.context.eqp = '*'
    data.context.pd = '*'
    data.context.lot = '*'
    assert !@@lmtrans.delete_lotsummary(data: data)
    assert_equal 'error', @@lmtrans.mq.service_response[:GenericResponse11][:Result]['Message']
    assert @@lmtrans.mq.service_response[:GenericResponse11][:Result][:Detail]['Data'].include?('Maximum two wildcards allowed')
  end


  # LmRecipe

  def test61_update_recipe_no_deleteflag
    assert @@lmtrans.update_lotsummary
    assert @@lmtest.verify_lotsummary_data
    #
    sleep 1
    now = Time.at(Time.now.to_i)
    @@lmtrans.lotsummary_data[0].recipes << LotManager::LmRecipe.new('QA_TYPE1', 'QA-RECIPE-1-CHG', now, 'QA CH memo')
    assert @@lmtrans.update_recipes
    assert @@lmtest.verify_lotsummary_data
    # verify claim_time is updated
    assert lsum = @@lmtest.db.lotsummary(context: @@lmtrans.lotsummary_data.first.context).first, 'no data'
  end

  def test62_update_recipe_deleteflag
    assert @@lmtrans.update_lotsummary
    assert @@lmtest.verify_lotsummary_data
    @@lmtrans.lotsummary_data[0].recipes = []
    # send deleteflag for dataset #0
    assert @@lmtrans.update_recipes(delete: [0])
    assert @@lmtest.verify_lotsummary_data
  end

  def test63_update_recipe_deleteflag_add
    assert @@lmtrans.update_lotsummary
    assert @@lmtest.verify_lotsummary_data
    # add recipes
    sleep 1
    now = Time.at(Time.now.to_i)
    @@lmtrans.lotsummary_data[0].recipes << LotManager::LmRecipe.new('QA_TYPE1', 'QA-RECIPE-1-CHG', now, 'QA CH memo')
    @@lmtrans.lotsummary_data[1].recipes << LotManager::LmRecipe.new('QA_TYPE2', 'QA-RECIPE-2-CHG', now, 'QA CH memo')
    @@lmtrans.lotsummary_data[2].recipes << LotManager::LmRecipe.new('QA_TYPE3', 'QA-RECIPE-3-CHG', now, 'QA CH memo')
    # new recipes are added although the delete flag is set
    assert @@lmtrans.update_recipes(delete: [0, 1, 2])
    assert @@lmtest.verify_lotsummary_data
    # delete recently added recipe for dataset #1
    @@lmtrans.lotsummary_data[1].recipes.pop
    # send deleteflag for all datasets
    assert @@lmtrans.update_recipes(delete: [0, 1, 2])
    assert @@lmtest.verify_lotsummary_data
  end


  # LmWaferSelection

  def test71_update_wafer_selection
    assert @@lmtrans.update_lotsummary
    assert @@lmtest.verify_lotsummary_data
    sleep 1
    @@lmtrans.lotsummary_data[0].pclbase.selwafers = 5
    @@lmtrans.lotsummary_data[0].pclbase.selwafers_src = 'CARRIER'
    @@lmtrans.lotsummary_data[0].pclsitesamplings[0].ptype = 'QA97'
    @@lmtrans.lotsummary_data[1].pclbase.selwafers = 6
    @@lmtrans.lotsummary_data[1].pclbase.selwafers_src = 'FAB'
    @@lmtrans.lotsummary_data[1].pclsitesamplings[0].stype = 'QA98'
    @@lmtrans.lotsummary_data[2].pclbase.selwafers = 7
    @@lmtrans.lotsummary_data[2].pclbase.selwafers_src = 'LOT'
    @@lmtrans.lotsummary_data[2].pclsitesamplings[0].wafer = 'QA99'
    assert @@lmtrans.update_wafer_selection
    assert @@lmtest.verify_lotsummary_data
  end

  def test72_update_wafer_selection_without_sitesamplings
    assert @@lmtrans.update_lotsummary
    assert @@lmtest.verify_lotsummary_data
    sleep 1
    @@lmtrans.lotsummary_data[0].pclsitesamplings = []
    assert @@lmtrans.update_wafer_selection
    assert @@lmtest.verify_lotsummary_data
  end

  # LmWishes

  def test81_update_wishes_no_deleteflag
    assert @@lmtrans.update_lotsummary
    assert @@lmtest.verify_lotsummary_data
    sleep 1
    now = Time.at(Time.now.to_i)
    @@lmtrans.lotsummary_data[0].pclbase.toolwish = 'QAEQP11-CHG'
    @@lmtrans.lotsummary_data[0].pclbase.toolwish_memo = 'Changed QA ToolWish memo'
    @@lmtrans.lotsummary_data[0].pclbase.toolwish_time = now
    @@lmtrans.lotsummary_data[0].pclbase.toolscore = 333
    @@lmtrans.lotsummary_data[0].pclbase.toolscore_memo = 'Changed QA ToolScore memo'
    @@lmtrans.lotsummary_data[0].pclbase.toolscore_time = now
    @@lmtrans.lotsummary_data[0].pclchambers << LotManager::LmPclChamber.new('QACH11', 'QAChamberWishCHG', now, 'Changed QA ChamberWish memo')
    @@lmtrans.lotsummary_data[1].pclchambers << LotManager::LmPclChamber.new('QACH21', 'QAChamberWishCHG', now, 'Changed QA ChamberWish memo')
    @@lmtrans.lotsummary_data[2].pclchambers << LotManager::LmPclChamber.new('QACH31', 'QAChamberWishCHG', now, 'Changed QA ChamberWish memo')
    assert @@lmtrans.update_wishes
    assert @@lmtest.verify_lotsummary_data
  end

  def test82_update_wishes_deleteflag
    assert @@lmtrans.update_lotsummary
    assert @@lmtest.verify_lotsummary_data
    sleep 1
    # delete one of toolwish, toolscore and chambers in each dataset
    @@lmtrans.lotsummary_data[0].pclbase.toolwish = nil
    @@lmtrans.lotsummary_data[1].pclbase.toolscore = nil
    @@lmtrans.lotsummary_data[2].pclchambers = []
    # send deleteflag for all datasets to cover toolwish, toolscore and chambers
    assert @@lmtrans.update_wishes(delete: [0, 1, 2])
    # verify datasets
    #0
    @@lmtrans.lotsummary_data[0].pclbase.toolwish_memo = nil
    assert @@lmtest.verify_lotsummary_data(data: @@lmtrans.lotsummary_data[0], exclude: :toolwish_time)
    #1
    @@lmtrans.lotsummary_data[1].pclbase.toolscore = 0
    @@lmtrans.lotsummary_data[1].pclbase.toolscore_memo = nil
    assert @@lmtest.verify_lotsummary_data(data: @@lmtrans.lotsummary_data[1], exclude: :toolscore_time)
    #2
    assert @@lmtest.verify_lotsummary_data(data: @@lmtrans.lotsummary_data[2])
  end


  def test83_update_wishes_deleteflag_new
    assert @@lmtrans.update_lotsummary
    assert @@lmtest.verify_lotsummary_data
    sleep 1
    now = Time.at(Time.now.to_i)
    @@lmtrans.lotsummary_data[0].pclbase.toolwish = 'QAEQP11-CHG'
    @@lmtrans.lotsummary_data[0].pclbase.toolwish_memo = 'Changed QA ToolWish memo'
    @@lmtrans.lotsummary_data[0].pclbase.toolwish_time = now
    @@lmtrans.lotsummary_data[0].pclbase.toolscore = 333
    @@lmtrans.lotsummary_data[0].pclbase.toolscore_memo = 'Changed QA ToolScore memo'
    @@lmtrans.lotsummary_data[0].pclbase.toolscore_time = now
    @@lmtrans.lotsummary_data[0].pclchambers << LotManager::LmPclChamber.new('QACH11', 'QAChamberWishCHG', now, 'Changed QA ChamberWish memo')
    @@lmtrans.lotsummary_data[1].pclchambers << LotManager::LmPclChamber.new('QACH21', 'QAChamberWishCHG', now, 'Changed QA ChamberWish memo')
    @@lmtrans.lotsummary_data[2].pclchambers << LotManager::LmPclChamber.new('QACH31', 'QAChamberWishCHG', now, 'Changed QA ChamberWish memo')
    # update wishes with deleteflag on for all contexts
    assert @@lmtrans.update_wishes(delete: [0, 1, 2])
    assert @@lmtest.verify_lotsummary_data
  end

  def test84_update_wishes_deleteflag_second
    # only 2nd dataset has the deleteflag, only 2nd set's chamberwhishes get deleted
    assert @@lmtrans.update_lotsummary
    assert @@lmtest.verify_lotsummary_data
    @@lmtrans.lotsummary_data[1].pclchambers = []
    assert @@lmtrans.update_wishes(delete: [1])
    assert @@lmtest.verify_lotsummary_data
  end

  def test85_update_wishes_delete_second_by_entry
    assert @@lmtrans.update_lotsummary
    assert @@lmtest.verify_lotsummary_data
    @@lmtrans.lotsummary_data[1].pclchambers = []
    assert @@lmtrans.update_wishes(delete: [0, 1, 2])
    assert @@lmtest.verify_lotsummary_data
  end

  # optional tests: data updates for APF

  def testman91_apf_update_all
    assert @@lmtrans.delete_lotsummary
    @@lmtrans.set_default_data
    5.times {|i|
      sleep 10
      assert @@lmtrans.update_lotsummary
      assert @@lmtrans.update_lmrequests
      assert @@lmtest.verify_lmrequest_data
      @@lmtrans.lmrequest_data.each {|d| d.maxlots += 1}
    }
  end

  def testman92_apf_update_lmrequests
    assert @@lmtrans.delete_lotsummary
    sleep 10
    @@lmtrans.set_default_data
    assert @@lmtrans.update_lotsummary
    5.times {|i|
      sleep 10
      assert @@lmtrans.update_lmrequests
      assert @@lmtest.verify_lmrequest_data
      @@lmtrans.lmrequest_data.each {|d| d.maxlots += 1}
    }
  end

end
