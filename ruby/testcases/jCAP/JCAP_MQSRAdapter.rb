=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author:  sfrieske, 2017-10-24

Version: 21.04

History:
  2019-01-30 sfrieske, minor cleanup
  2019-02-19 sfrieske, improved EmuRemote usage
  2021-04-21 sfrieske, moved @sorter and @pg from SiView::Test to SJC::Test
  2021-07-30 sfrieske, inherit from SiViewTestCase

Notes:
  Job used by APC GenericPilot
=end

require 'jcap/apcsra'
require 'jcap/sjctest'
require 'rtdserver/rtdemuremote'
require 'SiViewTestCase'


class JCAP_MQSRAdapter < SiViewTestCase
  @@sv_defaults = {carriers: 'JC%'}
  @@sorter = 'UTS001'
  @@empty_rule = 'EMPTY'  # as configured in sjc.properties for sjc.rtd.empty.rule
  @@columns = %w(cast_id cast_category stk_id)  # SJC uses the first column w/o checking the name
  @@testlots = []


  def self.shutdown
    @@svtest.cleanup_eqp(@@sorter)
    @@rtdremote.delete_emustation(@@empty_rule)
  end

  def self.after_all_passed
    @@sjctest.cleanup_sort_requests(creator: 'MQSRA')
	  @@testlots.each {|lot| @@sv.delete_lot_family(lot)}
  end


  def test00_setup
    $setup_ok = false
    #
    @@sjctest = SJC::Test.new($env, @@sv_defaults.merge(sv: @@sv, sorter: @@sorter))
    @@rtdremote = RTD::EmuRemote.new($env)
    @@mqsrclient = APC::MQSRClient.new($env, sv: @@sv)
    assert @@svtest.get_empty_carrier(n: 4), 'not enough empty carriers'
    #
    $setup_ok = true
  end

  def test11_happy1
    $setup_ok = false
    #
    run_scenario(nchildren: 1)
    #
    $setup_ok = true
  end

  def test12_happy3
    run_scenario(nchildren: 3)
  end

  def test13_golden
    run_scenario(nchildren: 3, extrachild: true)
  end

  def test21_error
    assert lot = @@testlots.last, 'no test lot'
    # send a wrong request (missing carrier), other info doesn't matter
    winfos = @@sv.lot_info(lot, wafers: true).wafers.take(1)
    assert res = @@mqsrclient.sjseparate([winfos], srccarrier: '', srccarrier_cat: ''), 'error submitting job'
    assert_equal 'SORT_REQUEST_CREATION_FAILED', res[:StatusID], 'wrong status'
    assert_equal '0', res[:SortRequestID], 'wrong SRID'
  end


  def run_scenario(params={})
    refute_nil @@mqsrclient.mq.mq_response.delete_msgs(nil)
    refute_nil @@mqsrclient.mq_status.delete_msgs(nil)
    assert @@svtest.cleanup_eqp(@@sorter, mode: 'Auto-3', portstatus: 'UnloadReq'), 'error cleaning up sorter'
    assert lot = @@svtest.new_lot, 'error creating lot'
    @@testlots << lot
    nchildren = params[:nchildren] || 1
    assert lcs = nchildren.times.collect {@@sv.lot_split(lot, 2)}, 'error splitting lot'
    if params[:extrachild]  # 'golden scenario' with another child going into the same carrier as the one of the previous children
      assert lc = @@sv.lot_split(lot, 2), 'error splitting lot'
      lcs << lc
    end
    lcis = @@sv.lots_info(lcs, wafers: true)
    # submit message, target carriers: c1: 1st child, etc.
    winfos = lcis.collect {|lci| lci.wafers}
    if params[:extrachild]
      # last child goes into the same carrier as sencond to last one
      winfos[-2] = winfos[-2] + winfos[-1]
      winfos.pop()
    end
    assert res = @@mqsrclient.sjseparate(winfos), 'error submitting job'
    assert_equal 'SORT_REQUEST_CREATED', res[:StatusID], 'wrong status'
    assert srid = res[:SortRequestID], 'missing SRID'
    # verify SR creation
    assert sts = @@sjctest.mds.sjc_tasks(srid: srid), 'MDS conn error'
    assert_equal lcis.size, sts.size, 'wrong or missing SJC sort tasks'
    # get empty carriers and ensure they are stocked in
    assert ecs = @@svtest.get_empty_carrier(n: nchildren), 'not enough empty carriers'
    ecs.each {|c| assert @@svtest.cleanup_carrier(c)}
    # prepare RTD rule response
    @@rtdremote.add_emustation(@@empty_rule) {@@rtdremote.create_response(@@columns, {'rows'=>ecs.collect {|c| [c, '', '']}})}
    sleep 3
    # execute SR and SJ
    assert sjid = @@sjctest.execute_verify(srid: srid), 'error executing SR'
    assert @@sv.claim_sj_execute(sjid: sjid, report: false)
    # verify response msg on status queue
    assert msg = @@mqsrclient.receive_statusmsg, 'no status msg'
    assert_equal srid, msg['SortRequestID'], 'wrong SRID'
    assert_equal 'SORT_REQUEST_COMPLETE', msg['StatusID'], 'wrong status'
    # verify child lot carriers
    carriers_seen = [@@sv.lot_info(lot).carrier]
    lcs.take(nchildren).each {|lc|
      c = @@sv.lot_info(lc).carrier
      refute carriers_seen.member?(c), "wrong carrier #{c} for lot #{lc}"
      carriers_seen << c
    }
    if params[:extrachild]
      assert_equal carriers_seen.last, @@sv.lot_info(lcs.last).carrier, 'extra child in wrong carrier'
    end
  end

end
