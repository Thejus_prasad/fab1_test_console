=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: Adel Darwaish, 2013-11-28

Version: 20.07

History:
  2014-01-14 dsteger,  refactored
  2015-04-15 ssteidte, cleaned up and verified test cases
  2017-02-17 sfrieske, simplified test data setup, added CONDI PDs
  2018-03-06 sfrieske, replaced user_parameter_delete_verify by user_parameter_delete
  2019-01-23 sfrieske, minor cleanup
  2020-08-10 sfrieske, added tests for MSR 1590127, v 20.07
  2021-08-27 sfrieske, require waitfor instead of misc

Notes:
  see http://f1onewiki:21080/display/JCAP/BankInUnusedKits+Design+Document
  timeout formats: HH, HH:MM, HH:MM:SS, set to 00:00:01
=end

require 'util/waitfor'
require 'SiViewTestCase'


class JCAP_BankInUnusedKits < SiViewTestCase
  # @@sv_defaults = {carriers: '%', nwafers: 5}
  @@p_eqpmon = 'UT-UNUSED-KITS1.01'
  @@p_procmon = 'UT-UNUSED-KITS2.01'
  @@p_dummy = 'UT-UNUSED-KITS3.01'
  @@p_udata_custom = 'UT-UNUSED-KITS4.01'
  @@p_udata_empty = 'UT-UNUSED-KITS5.01'
  @@p_udata_invalid = 'UT-UNUSED-KITS6.01'
  @@p_lottype_invalid = 'UT-UNUSED-KITS7.01'
  @@p_udata_broken_regular = 'UT-UNUSED-KITS8.01'   # MSR 1590127, v 20.07
  #
  @@udata = 'XferUnusedProd'
  @@wafergrade = '123'
  @@route = 'UT-UNUSEDKITS-BANKIN.01'
  @@op_qual = 'UT-UNUSEDKITS-QUAL.01'
  @@op_condi = 'UT-UNUSEDKITS-CONDI.01'
  @@op_dummy = 'UT-UNUSEDKITS-DUMMY.01'

  @@job_interval = 330
  @@testlots = []


  def self.shutdown
    @@testlots.each {|lot| @@sv.delete_lot_family(lot)}
  end


  def test00_setup
    $setup_ok = false
    #
    # set product UDATA XferUnusedProd
    assert @@sm.update_udata(:product, @@p_eqpmon, {@@udata=>@@wafergrade})
    assert @@sm.update_udata(:product, @@p_procmon, {@@udata=>@@wafergrade + '|00:09:59'})
    assert @@sm.update_udata(:product, @@p_dummy, {@@udata=>'DEFAULT'})
    assert @@sm.update_udata(:product, @@p_udata_custom, {@@udata=>'DEFAULT|00:10'})
    assert @@sm.update_udata(:product, @@p_udata_broken_regular, {@@udata=>'DEFAULT|00:00:01|00:10'})
    assert @@sm.update_udata(:product, @@p_udata_empty, {@@udata=>''})
    assert @@sm.update_udata(:product, @@p_udata_invalid, {@@udata=>'|00:00:01'})
    assert @@sm.update_udata(:product, @@p_lottype_invalid, {@@udata=>@@wafergrade})
    # verify PD UDATA OperationReportingType
    assert @@sv.user_data_pd(@@op_qual)['OperationReportingType'].include?('-QUAL'), 'wrong PD UDATA'
    assert @@sv.user_data_pd(@@op_condi)['OperationReportingType'].include?('-CONDI'), 'wrong PD UDATA'
    assert @@sv.user_data_pd(@@op_dummy)['OperationReportingType'].include?('-DUMMY'), 'wrong PD UDATA'
    # opNos for further reference
    rops = @@sv.route_operations(@@route)
    assert @@opNo_qual = rops.find {|e| e.operationID.identifier == @@op_qual}.operationNumber
    assert @@opNo_condi = rops.find {|e| e.operationID.identifier == @@op_condi}.operationNumber
    assert @@opNo_dummy = rops.find {|e| e.operationID.identifier == @@op_dummy}.operationNumber
    assert @@opNo_last = rops.last.operationNumber
    #
    # create test lots
    @@svtest.carriers = '%'
    assert @@lot1 = @@svtest.new_controllot('Equipment Monitor', product: @@p_eqpmon)
    @@testlots << @@lot1
    assert @@lot2 = @@svtest.new_controllot('Process Monitor', product: @@p_procmon)
    @@testlots << @@lot2
    assert @@lot3 = @@svtest.new_controllot('Dummy', product: @@p_dummy)
    @@testlots << @@lot3
    assert @@lot4 = @@svtest.new_controllot('Dummy', product: @@p_udata_custom)
    @@testlots << @@lot4
    assert @@lot5 = @@svtest.new_controllot('Dummy', product: @@p_udata_empty)
    @@testlots << @@lot5
    assert @@lot6 = @@svtest.new_controllot('Dummy', product: @@p_udata_invalid)
    @@testlots << @@lot6
    assert @@lot7 = @@svtest.new_controllot('Recycle', product: @@p_lottype_invalid)
    @@testlots << @@lot7
    assert @@lot8 = @@svtest.new_controllot('Dummy', product: @@p_dummy)
    @@testlots << @@lot8
    assert @@lot9 = @@svtest.new_controllot('Dummy', product: @@p_dummy)
    @@testlots << @@lot9
    assert @@lot10 = @@svtest.new_controllot('Dummy', product: @@p_dummy)
    @@testlots << @@lot10
    assert @@lot11 = @@svtest.new_controllot('Dummy', product: @@p_dummy)
    @@testlots << @@lot11
    assert @@lot12 = @@svtest.new_controllot('Dummy', product: @@p_dummy)
    @@testlots << @@lot12
    assert @@lot13 = @@svtest.new_controllot('Dummy', product: @@p_dummy)
    @@testlots << @@lot13
    # broken kit
    assert @@lot21 = @@svtest.new_controllot('Dummy', product: @@p_dummy)
    @@testlots << @@lot21
    assert_equal 0, @@sv.user_parameter_change('Lot', @@lot21, 'SAPOrderNumber', Time.now.to_i)
    assert @@lot22 = @@sv.lot_split(@@lot21, 1)
    @@testlots << @@lot22
    assert @@lot23 = @@sv.lot_split(@@lot21, 1)
    @@testlots << @@lot23
    # broken kit, separate custom timeouts
    assert @@lot31 = @@svtest.new_controllot('Dummy', product: @@p_udata_broken_regular)
    @@testlots << @@lot31
    assert_equal 0, @@sv.user_parameter_change('Lot', @@lot31, 'SAPOrderNumber', Time.now.to_i)
    assert @@lot32 = @@sv.lot_split(@@lot31, 1)
    @@testlots << @@lot32
    assert @@lot33 = @@sv.lot_split(@@lot31, 1)
    @@testlots << @@lot33
    # not broken kit, separate custom timeouts
    assert @@lot41 = @@svtest.new_controllot('Dummy', product: @@p_udata_broken_regular)
    @@testlots << @@lot41
    assert_equal 0, @@sv.user_parameter_change('Lot', @@lot41, 'SAPOrderNumber', Time.now.to_i)
    assert @@lot42 = @@sv.lot_split(@@lot41, 1)
    @@testlots << @@lot42
    assert @@lot43 = @@sv.lot_split(@@lot41, 1)
    @@testlots << @@lot43
    #
    # register a FutureHold for all lots (no Hold) to prevent early job runs to handle them
    @@testlots.each {|lot|
      next if lot == @@lot10  # except lot10 (not located)
      assert_equal 0, @@sv.lot_futurehold(lot, @@opNo_last, nil, post: true)
      if lot == @@lot2
        assert_equal 0, @@sv.lot_opelocate(lot, @@opNo_condi)
      elsif lot == @@lot3
        assert_equal 0, @@sv.lot_opelocate(lot, @@opNo_dummy)
      else
        assert_equal 0, @@sv.lot_opelocate(lot, @@opNo_qual)
      end
    }
    # set SAPOrderNumber except for lot9 (and lots 2x, 3x, 4x that are handled separately)
    @@testlots.each {|lot|
      next if lot == @@lot9
      next if [@@lot21, @@lot22, @@lot23, @@lot31, @@lot32, @@lot33, @@lot41, @@lot42, @@lot43].member?(lot)
      assert_equal 0, @@sv.user_parameter_change('Lot', lot, 'SAPOrderNumber', '12345')
    }
    # lot8: carrier EI
    assert @@sv.claim_process_prepare(@@lot8, notify: false, purpose: 'Other')
    # lot9: no SAPOrderNumber, not set above
    # lot10: wrong PD, not located above
    udata = @@sv.user_data_pd(@@sv.lot_info(@@lot10).op)['OperationReportingType']
    %w(-CONDI -DUMMY -QUAL).each {|v| assert !udata.include(v), 'wrong PD UDATA for lot10'} if udata
    # lot11: scrapped
    assert_equal 0, @@sv.scrap_wafers(@@lot11)
    # lot12: ONHOLD
    assert_equal 0, @@sv.lot_hold(@@lot12, nil)
    # Lot13: FutureHold, not released below
    # lots 21, 22, 23: broken kit because 2nd lot is banked in
    assert_equal 0, @@sv.lot_opelocate(@@lot22, :last), 'error locating lot'
    assert_equal 'InBank', @@sv.lot_info(@@lot22).states['Lot Inventory State'], "#{@@lot22} must be banked in"
    # lots 31, 32, 33: broken kit because 2nd lot is banked in
    assert_equal 0, @@sv.lot_opelocate(@@lot32, :last), 'error locating lot'
    assert_equal 'InBank', @@sv.lot_info(@@lot32).states['Lot Inventory State'], "#{@@lot32} must be banked in"
    # lots 41, 42, 43: not broken kit
    #
    # for waittime calculations
    @@tstart = Time.now
    # wait for MDS synch
    sleep 45
    # cancel the FutureHold, except for lot13
    @@testlots.each {|lot|
      next if lot == @@lot13
      assert_equal 0, @@sv.lot_futurehold_cancel(lot, nil)
    }
    #
    # lots by waittime
    @@lots_default = [@@lot1, @@lot3, @@lot6, @@lot21, @@lot22, @@lot23, @@lot31, @@lot32, @@lot33]
    @@lots_custom = @@lots_default + [@@lot2, @@lot4, @@lot41, @@lot42, @@lot43]
    #
    $setup_ok = true
  end

  def test11_happy_default_waittime
    $log.info "waiting up to #{@@job_interval} s for the jCAP job to run"
    assert wait_for(timeout: @@job_interval) {@@sv.lot_info(@@lot1).opNo == @@opNo_last}, 'job did not run?'
    @@timeshift = Time.now - @@tstart  # for custom waittime calculation below
    sleep 15
    # all happy lots with default (very short) waittime have been located and have the correct WaferGrade
    verify_lots(@@lots_default)
  end

  def test12_happy_custom_waittime
    # adjust custom waittime
    assert @@timeshift, 'test11 must be completed before this test can run'
    t = @@timeshift + @@job_interval - 90
    v = "#{@@wafergrade}|00:%02d:%02d" % [(t / 60).to_i, (t % 60).to_i]
    assert @@sm.update_udata(:product, @@p_procmon, {@@udata=>v})
    v = "DEFAULT|00:%02d" % (t / 60).to_i
    assert @@sm.update_udata(:product, @@p_udata_custom, {@@udata=>v})
    v = "DEFAULT|00:00:01|00:%02d" % (t / 60).to_i
    assert @@sm.update_udata(:product, @@p_udata_broken_regular, {@@udata=>v})
    #
    $log.info "waiting up to #{@@job_interval} s for the jCAP job to run"
    assert wait_for(timeout: @@job_interval) {@@sv.lot_info(@@lot2).opNo == @@opNo_last}, 'job did not run?'
    sleep 15
    # all happy lots with custom long waittime have also been located and the correct WaferGrade
    verify_lots(@@lots_custom)
  end

  def test21_memo
    [@@lot1, @@lot2, @@lot3, @@lot4, @@lot6, @@lot21, @@lot23, @@lot31, @@lot33, @@lot41, @@lot42, @@lot43].each {|lot|
      lno = @@testlots.index(lot) + 1
      # assert h = @@sv.lot_operation_history(lot, category: 'LocateForward').last
      # assert h.memo.include?('BankInUnusedKits'), "wrong claim memo for lot#{lno} (#{lot.inspect})"
      assert h = @@sv.lot_operation_history(lot, opCategory: 'LocateForward',
        opNo: @@opNo_last, route: @@sv.lot_info(lot).route, pass: 1, raw: true).last
      assert h.claimMemo.include?('BankInUnusedKits'), "wrong claim memo for lot#{lno} (#{lot.inspect})"
    }
  end

  # verify log entries are related to existing slr (cj)
  def testman31_slr
    # pick a happy lot and prepare it
    lot = @@lot1
    assert_equal 0, @@sv.lot_cleanup(lot)
    assert_equal 0, @@sv.lot_futurehold(lot, @@opNo_last, nil, post: true)
    assert_equal 0, @@sv.lot_opelocate(lot, @@opNo_qual)
    # do a SLR and release the future hold to make the lot a candidate for the job
    eqp = @@sv.lot_info(lot).opEqps.first
    assert_equal 0, @@sv.eqp_cleanup(eqp, mode: 'Off-Line-1', notify: false)
    assert_equal 0, @@sv.slr(eqp, lot: lot)
    assert_equal 0, @@sv.lot_futurehold_cancel(lot, nil)
    # wait for the jCAP job to run and verify lot has not been moved
    $log.info "waiting #{@@job_interval} s for the jCAP job to run"; sleep @@job_interval
    assert_equal @@opNo_qual, @@sv.lot_info(lot.opNo), "lot has been touched"
  end


  # verify lot and wafer properties
  def verify_lots(goods)
    $log.info "verify_lots #{goods}"
    @@testlots.each {|lot|
      li = @@sv.lot_info(lot, wafers: true)
      lno = @@testlots.index(lot) + 1
      if goods.include?(lot)
        assert_equal @@opNo_last, li.opNo, "wrong opNo for lot#{lno} (#{lot.inspect})"
        # assert_equal 'InBank', li.states['Lot Inventory State'], 'lot is not banked in'
        value = @@sv.user_data(:product, li.product)[@@udata].start_with?(@@wafergrade) ? @@wafergrade : ''
        li.wafers.each {|w|
          assert_equal value, @@sv.user_parameter('Wafer', w.wafer, name: 'WfrGrade').value, "wrong wafergrade for lot#{lno} (#{lot.inspect})"
        }
      else
        refute_equal @@opNo_last, li.opNo, "wrong opNo for lot#{lno} (#{lot.inspect})"
        li.wafers.each {|w| assert_empty @@sv.user_parameter('Wafer', w.wafer, name: 'WfrGrade').value}
      end
    }
  end

end
