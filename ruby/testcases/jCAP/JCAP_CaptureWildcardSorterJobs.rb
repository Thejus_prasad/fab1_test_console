=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.


Author: Steffen Steidten, 2013-03-01

Version: 21.05

History:
  2015-03-31 ssteidte, cleaned up
  2017-06-01 sfrieske, minor fixes
  2018-11-07 sfrieske, minor cleanup
  2019-11-08 sfrieske, changed TC11 (different lot openote, MSR 822533)
  2020-10-05 sfrieske, changed TC13 because of SJC 20.07 changes
  2021-04-21 sfrieske, moved @sorter and @pg form SiView::Test to SJC::Test
  2021-07-28 sfrieske, adjusted notification check (changed with SJC 21.04)
  2021-08-27 sfrieske, require waitfor instead of misc
=end

require 'jcap/sjctest'
require 'util/waitfor'
require 'SiViewTestCase'


class JCAP_CaptureWildcardSorterJobs < SiViewTestCase
  @@carriers = 'JC%'  # with disabled usage check
  @@sorter = 'UTS001'
  @@pg = 'PG1'
  # maximum age of not handled sort request (MDS status UNPROCESSED) before they get automatically deleted
  @@expiration_unhandled_sr = 1800
  # maximum age of not completed (MDS status Pending, SiView status Executing) SiView sorter jobs before deletion
  @@expiration_executing_sj = 1800
  # time interval of lifecycle check
  @@lifecyclecheck_interval = 300

  @@use_notifications = true
  @@mds_sync = 45


  def self.after_all_passed
    @@sjctest.cleanup_eqp(@@sorter)
    @@sv.delete_lot_family(@@lot)
  end


  def test00_setup
    $setup_ok = false
    #
    assert @@svnox = SiView::MM.new($env, user: 'NOX-UNITTEST', password: :default)
    @@sjctest = SJC::Test.new($env, @@sv_defaults.merge(sv: @@sv, carriers: @@carriers,
      sorter: @@sorter, pg: @@pg, notifications: @@use_notifications))
    # create a lot with 3 children
    assert @@lot = @@sjctest.new_lot, 'error creating test lot'
    3.times {assert @@sv.lot_split(@@lot, 2)}
    @@carrier = @@sv.lot_info(@@lot).carrier
    assert @@carrier1 = @@sjctest.get_empty_carrier, 'no empty carrier'
    #
    $setup_ok = true
  end

  def test11_wildcard_sj_happy
    $setup_ok = false
    #
    assert @@sjctest.cleanup_eqp(@@sorter, mode: 'Auto-3'), 'error cleaning up sorter'
    assert @@sjctest.prepare_carrier(@@carrier1, 'empty')
    assert @@sjctest.prepare_carrier(@@carrier, @@lot)
    li_old = @@sv.lot_info(@@lot, wafers: true)
    #
    # create sorter job with wildcards, verify sr creation and sj update
    assert sjid = @@sjctest.create_wildcard_verify(@@lot, @@carrier1, slots: 6), 'error creating SJ or SR'
    assert sj2 = @@sjctest.execute_verify, 'no SiView SJ'
    assert_equal sjid.identifier, sj2, 'wrong SJ'
    #
    # complete the sorter job and verify lot is in the new carrier and has an operation note
    $log.info 'waiting up to 180 s for SR status PROCESSED'
    tstart = Time.now
    ok = wait_for(timeout: 180) {
      sr = @@sjctest.mds.sjc_requests(srid: @@sjctest.sr.srid, history: true).first
      sr && sr.status == 'PROCESSED'
    }
    assert @@sv.claim_sj_execute(eqp: @@sorter) unless ok
    assert @@sjctest.verify_lot_openote('MOVE', @@lot, @@carrier, @@carrier1, tstart: tstart, user: 'SIVIEW'), 'wrong lot openote'
    li = @@sv.lot_info(@@lot, wafers: true)
    assert_equal @@carrier1, li.carrier, "wrong carrier for lot #{@@lot}"
    #
    # verify wafer positions
    $log.info 'verifying wafer positions'
    ok = true
    li.wafers.each_with_index {|w, i| ok &= (w.slot == 6 + i + 1)}
    assert ok, "wrong slotmap, old: #{li_old.wafers.pretty_inspect}\nnew: #{li.wafers.pretty_inspect}"
    #
    $setup_ok = true
  end

  def test12_wildcard_sj_error
    assert @@sjctest.cleanup_eqp(@@sorter, mode: 'Auto-3', portstatus: 'UnloadReq'), 'error cleaning up sorter'
    assert @@sjctest.prepare_carrier(@@carrier1, 'empty')
    assert @@sjctest.prepare_carrier(@@carrier, @@lot)
    #
    # create sorter job with wildcards, verify sr creation and sj update
    assert sjid = @@sjctest.create_wildcard_verify(@@lot, @@carrier1), 'error creating SJ or SR'
    assert sj2 = @@sjctest.execute_verify, 'no SiView SJ'
    assert_equal sjid.identifier, sj2, 'wrong SJ'
    #
    # set sj on error and verify deletion of the sort request
    assert_equal 0, @@sv.sj_status_change(@@sorter, sjid, 'Error')
    assert @@sjctest.verify_sr_removed, 'SR not removed from MDS table'
    # verify deletion of the sorter job
    assert_empty @@sv.sj_list(sjid: sjid), "SiView sj #{sjid} not deleted"
  end

  def test13_wildcard_reject
    assert @@sjctest.cleanup_eqp(@@sorter), 'error cleaning up sorter'
    assert @@sjctest.prepare_carrier(@@carrier1, 'empty')
    assert @@sjctest.prepare_carrier(@@carrier, 'empty')
    # ensure there is a lot with less than 13 wafers in carrier
    lot = nil
    @@sv.lot_family(@@lot).each {|l| (lot = l; break) if @@sv.lot_info(l).nwafers < 13}
    assert lot, "no lot with less than 13 wafers in #{@@carrier}"
    assert_equal 0, @@sv.wafer_sort(lot, @@carrier)
    #
    # create a SR with @carrier, don't execute
    $log.info "waiting #{@@mds_sync} s for the MDS to sync"; sleep @@mds_sync
    assert @@sjctest.create_combine_verify(@@carrier, @@carrier1), 'error in COMBINE request'
    #
    # create sorter job with wildcards with the same carriers as above (refused by SJC)
    assert sjid = @@svnox.sj_create('*', '*', '*', lot, @@carrier1, slots: 13), 'error creating SJ'
    #
    # wait for Capture job to delete the job
    $log.info 'waiting for the SJ to be deleted'
    assert wait_for(timeout: 90) {@@sv.sj_list(sjid: sjid).empty?}, "SJ #{sjid.identifier} not deleted"
    assert wait_for {!@@sv.lot_info(lot).sjexists}, "lot #{lot} still has an SJ"
  end

  def test14_wildcard_sj_timeout
    assert @@sjctest.cleanup_eqp(@@sorter, portstatus: 'UnloadReq'), 'error cleaning up sorter'
    assert @@sjctest.prepare_carrier(@@carrier1, 'empty')
    assert @@sjctest.prepare_carrier(@@carrier, @@lot)
    #
    # create a wildcard SJ, don't execute
    assert sjid = @@sjctest.create_wildcard_verify(@@lot, @@carrier1), 'error creating SJ or SR'
    tstart = Time.now
    #
    # wait for SR removal after expiration time, verify SiView SJ got deleted
    assert @@sjctest.verify_sr_removed(timeout: @@lifecyclecheck_interval + @@expiration_unhandled_sr + 90)
    assert_empty @@sv.sj_list(sjid: sjid), "SiView SJ #{sjid.identifier} not deleted"
    #
    if @@use_notifications
      sleep 60
      # verify notification, sometimes fails because of stale SJ cleanup interfering (?)
      filter = {'Category'=>'CanceledSiViewSystemSelectedSorterJob'}
      assert msg = @@sjctest.notifications.query(tstart: tstart, service: '%jCAP_AsyncNotification%', filter: filter).first
      assert msg['Reason'].include?(@@carrier), "srccarrier #{@@carrier} missing in msg reason"
      assert msg['Reason'].include?(@@carrier1), "tgtcarrier #{@@carrier1} missing in msg reason"
    end

  end

end
