=begin
(c) Copyright 2011 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.


Author: Steffen Steidten, 2012-11-28

Version: 18.09

History:
  2015-04-15 ssteidte, cleaned up and verified test cases
  2018-03-06 sfrieske, replaced user_parameter_delete_verify by user_parameter_delete
  2019-01-15 sfrieske, minor cleanup
  2021-08-30 sfrieske, moved svlog.rb to siview/

Notes:
  2 lots need to be set up in jdbc:mysql://neo2.gfoundries.com/Lotgrading_dev !!
=end

require 'dbsequel'
require 'siview/svlog'
require 'SiViewTestCase'


class JCAP_AutoScriptParamUpload < SiViewTestCase
  @@parameters = ['CustomerLotGrade', 'IntegrationLotGrade']
  @@svlogdelay = 660
  @@jobinterval = 310  # check with the admin to change the default of 1 day


  def test00_setup
    $setup_ok = false
    #
    @@svlog = SvLog.new($env)
    #
    # ensure there are at least 2 lots int the lotgrading DB
    @@lgdb = DB.new("lotgrading.#{$env}")
    q = 'SELECT i.Lot_id, c.CustomerLotGrade, i.IntegrationLotGrade
      FROM CustomerLotGrade2SiView c, IntegrationLotGrade2SiView i WHERE BINARY(c.lot) = BINARY(i.lot_id) ORDER BY c.Lot'
    lotdata = @@lgdb.select(q)
    assert lotdata.size > 1, "not enough test lots: #{lotdata}"
    @@lotdata = lotdata.take(2)
    # create the test lots if necessary
    @@lotdata.each {|e| assert @@svtest.new_lot(lot: e.first) unless @@sv.lot_exists?(e.first)}
    $log.info "using test lots\n#{@@lotdata.pretty_inspect}"
    #
    $setup_ok = true
  end

  def test11_set_params_after_delete
    $setup_ok = false
    #
    # delete current paramters and wait for the jCAP job
    @@lotdata.each {|e|
      @@parameters.each {|p|
        assert_equal 0, @@sv.user_parameter_delete('Lot', e.first, p, check: true)
      }
    }
    $log.info "waiting #{@@jobinterval} s for the jCAP job"; sleep @@jobinterval
    # verify the SiView parameters are set again
    check_lots
    #
    $setup_ok = true
  end

  def test12_set_params_after_change
    # change the SiView values and wait for the jCAP job
    @@tstart = Time.now
    @@lotdata.each {|e|
      @@parameters.each {|p| assert_equal 0, @@sv.user_parameter_change('Lot', e.first, p, "QA#{Time.now.utc.strftime('%F_%T').gsub(':', '-')}")}
    }
    $log.info "waiting #{@@jobinterval} s for the jCAP job"; sleep @@jobinterval
    # verify the SiView parameters are set again
    check_lots
  end

  def test13_verify_params_not_set  # need to run test12 before this one!
    # verify parameters are correct
    check_lots
    qparams = {txname: 'TxUserParameterValueChangeReq', txuser: 'X-JASPU', tstart: @@tstart}
    # get last time from log
    assert t1 = @@svlog.query(qparams.merge(detail: @@lotdata[0].first, tend: Time.now)).last
    assert t2 = @@svlog.query(qparams.merge(detail: @@lotdata[1].first, tend: Time.now)).last
    t = @@jobinterval + @@svlogdelay
    $log.info "waiting #{t} s for the jCAP job and svlog"; sleep t
    #
    # verify parameters are still correct
    check_lots
    # verify in svlog that the Tx has not been called
    assert t3 = @@svlog.query(qparams.merge(detail: @@lotdata[0].first, tend: Time.now)).last
    assert t4 = @@svlog.query(qparams.merge(detail: @@lotdata[1].first, tend: Time.now)).last
    assert_equal t1.first, t3.first
    assert_equal t2.first, t4.first
  end

  # aux method

  # assert the lots have the SiView script paramters set as in the lotgrading DB
  def check_lots
    @@lotdata.each {|e|
      @@parameters.each_with_index {|p, pi|
        assert_equal e[pi + 1], @@sv.user_parameter('Lot', e.first, name: p).value, "lot #{e.first}: wrong #{p.inspect}"
      }
    }
  end

end
