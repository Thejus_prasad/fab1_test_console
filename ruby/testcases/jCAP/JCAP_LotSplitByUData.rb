=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: Steffen Steidten, 2010-12-17

Version: 18.09

History:
  2013-05-15 dsteger,  cleanup routine
  2014-10-17 ssteidte, expanded tests, 1 tc per lot
  2014-10-20 ssteidte, testcases for carrier NOTAVAILABLE and INUSE (v14.10)
  2015-04-09 ssteidte, cleanup
  2018-12-17 sfrieske, minor cleanup
  2020-08-17 aschmid3, minor cleanup
  2021-07-30 sfrieske, major cleanup
=end

require 'derdack'
require 'SiViewTestCase'


# The test sometimes fails due to splits that happen while preparing the lots (gone with new_lots?)
class JCAP_LotSplitByUData < SiViewTestCase
  @@sv_defaults = {product: 'ITDC-JCAP-TEST.01', route: 'ITDC-JCAP-TEST.01', carriers: 'JC%'}
  @@nonprobank = 'UT-NONPROD'
  @@opNo_split1 = '2000.1300'  # PD 'ITDC-JCAP-TEST-SPLITSEPERATE.01'
  @@opNo_split2 = '1000.1300'  # PD 'ITDC-JCAP-TEST-SPLITSEPERATE.02'
  @@opNo_split3 = '3000.1300'  # PD 'ITDC-JCAP-TEST-SPLITSEPERATE.03'
  @@psm_split_op = '1000.1400'
  @@psm_return_op = '1000.1600'
  @@merge_op_child1 = '1000.1400'
  @@branch_route = 'C-ERS-DFU.01'
  @@branch_route2 = 'e2405-GET-AB.01'
  @@opNo_split_erf = 'e3600.1200'  # PD 'ITDC-JCAP-TEST-SPLITSEPERATE.03'
  @@eqp = 'UTF001'
  @@hold_reason = 'ENG'
  @@hold_error = 'E-TS'
  #
  @@memo = 'Done by jCAP application LotSplitByUData. Failed splitting Lot'
  #
  # PD UDATA
  @@automation_type1 = 'COMBINE:NO,SEPARATE:NO,LOTSPLIT(7),NoInterFabXfer'
  @@automation_type2 = 'COMBINE:NO,SEPARATE:NO,NoInterFabXfer,LOTSPLIT(7:ITDC-JCAP-TEST-MERGE.02)'
  @@automation_type3 = 'COMBINE:NO,SEPARATE:NO,NoInterFabXfer,LOTSPLIT(7:ITDC-JCAP-TEST-MERGE-WRONG.02)'

  @@slot_or_alias = :alias  # Fab1 :alias, Fab8 :slot
  @@job_interval = 310


  def self.after_all_passed
    @@testlots.each {|lot| @@sv.delete_lot_family(lot)}
  end


  def test00_setup
    $setup_ok = false
    #
    assert @@notifications = DerdackEvents.new($env)
    assert @@svtest.cleanup_eqp(@@eqp, mode: 'Off-Line-1')
    #
    # check PD UDATA
    # on main route
    assert rops = @@sv.route_operations(@@svtest.route)
    assert e = rops.find {|e| e.operationNumber == @@opNo_split1}, 'missing opNo'
    assert_equal @@automation_type1, @@sv.user_data_pd(e.operationID.identifier)['AutomationType'], 'wrong UDATA'
    assert e = rops.find {|e| e.operationNumber == @@opNo_split2}, 'missing opNo'
    assert_equal @@automation_type2, @@sv.user_data_pd(e.operationID.identifier)['AutomationType'], 'wrong UDATA'
    assert e = rops.find {|e| e.operationNumber == @@opNo_split3}, 'missing opNo'
    assert_equal @@automation_type3, @@sv.user_data_pd(e.operationID.identifier)['AutomationType'], 'wrong UDATA'
    # on branch route
    assert ropsb = @@sv.route_operations(@@branch_route2)
    assert e = ropsb.find {|e| e.operationNumber == @@opNo_split_erf}, 'missing opNo'
    assert_equal @@automation_type3, @@sv.user_data_pd(e.operationID.identifier)['AutomationType'], 'wrong UDATA'
    #
    # prepare test lots and carriers
    assert @@testlots = @@svtest.new_lots(20), 'error creating test lots'
    slots = (1..25).to_a
    @@lot_wafer_map = {}
    @@testlots.each {|lot|
      li = @@sv.lot_info(lot, wafers: true)
      @@lot_wafer_map[lot] = li.wafers.collect {|w| w.send(@@slot_or_alias)}.sort
      assert_equal 0, @@sv.wafer_sort(lot, li.carrier, slots: slots.shuffle!)
    }
    assert cc = @@svtest.get_empty_carrier(n: 2), 'not enough empty carriers'
    cc.each {|c| assert @@svtest.cleanup_carrier(c)}
    @@empty_carrier1, @@empty_carrier2 = cc
    #
    @@notification_time = Time.now
    @@lots_notification = []
    @@lots_nosplit = []
    @@lots_split = []
    #
    # lot holds
    lot = @@testlots[0]
    @@lots_nosplit << lot
    assert_equal 0, @@sv.lot_hold(lot, @@hold_reason)
    assert_equal 0, @@sv.lot_opelocate(lot, @@opNo_split1, force: true)
    lot = @@testlots[1]
    @@lots_nosplit << lot
    assert_equal 0, @@sv.lot_hold(lot, @@hold_reason)
    assert_equal 0, @@sv.lot_opelocate(lot, @@opNo_split2, force: true)
    #
    # nonpro bank
    lot = @@testlots[2]
    @@lots_nosplit << lot
    assert_equal 0, @@sv.lot_opelocate(lot, @@opNo_split1)
    assert_equal 0, @@sv.lot_nonprobankin(lot, @@nonprobank)
    lot = @@testlots[3]
    @@lots_nosplit << lot
    assert_equal 0, @@sv.lot_opelocate(lot, @@opNo_split2)
    assert_equal 0, @@sv.lot_nonprobankin(lot, @@nonprobank)
    #
    # carrier EI
    lot = @@testlots[4]
    @@lots_split << lot
    li = @@sv.lot_info(lot)
    assert_equal 0, @@sv.lot_opelocate(lot, @@opNo_split1)
    assert_equal 0, @@sv.carrier_xfer_status_change(li.carrier, 'EO', @@eqp)
    assert_equal 0, @@sv.eqp_load(@@eqp, nil, li.carrier, purpose: 'Other'), "failed to load carrier #{li.carrier}"
    lot = @@testlots[5]
    @@lots_split << lot
    li = @@sv.lot_info(lot)
    assert_equal 0, @@sv.lot_opelocate(lot, @@opNo_split2)
    assert_equal 0, @@sv.carrier_xfer_status_change(li.carrier, 'EO', @@eqp)
    assert_equal 0, @@sv.eqp_load(@@eqp, nil, li.carrier, purpose: 'Other'), "failed to load carrier #{li.carrier}"
    #
    # carrier AO
    lot = @@testlots[6]
    @@lots_nosplit << lot
    li = @@sv.lot_info(lot)
    assert_equal 0, @@sv.lot_opelocate(lot, @@opNo_split1)
    assert_equal 0, @@sv.carrier_xfer_status_change(li.carrier, 'AO', @@svtest.stocker)
    lot = @@testlots[7]
    @@lots_nosplit << lot
    li = @@sv.lot_info(lot)
    assert_equal 0, @@sv.lot_opelocate(lot, @@opNo_split2)
    assert_equal 0, @@sv.carrier_xfer_status_change(li.carrier, 'AO', @@svtest.stocker)
    #
    # carrier NOTAVAILABLE
    lot = @@testlots[8]
    @@lots_nosplit << lot
    li = @@sv.lot_info(lot)
    assert_equal 0, @@sv.lot_opelocate(lot, @@opNo_split1)
    assert_equal 0, @@sv.carrier_status_change(li.carrier, 'NOTAVAILABLE')
    lot = @@testlots[9]
    @@lots_nosplit << lot
    li = @@sv.lot_info(lot)
    assert_equal 0, @@sv.lot_opelocate(lot, @@opNo_split2)
    assert_equal 0, @@sv.carrier_status_change(li.carrier, 'NOTAVAILABLE')
    #
    # carrier INUSE
    lot = @@testlots[10]
    @@lots_nosplit << lot
    li = @@sv.lot_info(lot)
    assert_equal 0, @@sv.lot_opelocate(lot, @@opNo_split1)
    assert_equal 0, @@sv.carrier_status_change(li.carrier, 'INUSE')
    lot = @@testlots[11]
    @@lots_nosplit << lot
    li = @@sv.lot_info(lot)
    assert_equal 0, @@sv.lot_opelocate(lot, @@opNo_split2)
    assert_equal 0, @@sv.carrier_status_change(li.carrier, 'INUSE')
    #
    # 2 child lots in one carrier
    lot = @@testlots[12]
    @@lots_split << lot
    assert lc = @@sv.lot_split(lot, 12), 'error splitting lot'
    @@lots_split << lc
    assert_equal 0, @@sv.wafer_sort(lc, @@empty_carrier1, slots: (1..12).to_a)
    [lot, lc].each {|lot|
      @@lot_wafer_map[lot] = @@sv.lot_info(lot, wafers: true).wafers.collect {|w| w.send(@@slot_or_alias)}.sort
    }
    assert_equal 0, @@sv.lot_opelocate(lc, @@opNo_split1)
    assert_equal 0, @@sv.lot_opelocate(lot, @@opNo_split1)
    lot = @@testlots[13]
    @@lots_split << lot
    assert lc = @@sv.lot_split(lot, 12), 'error splitting lot'
    @@lots_split << lc
    assert_equal 0, @@sv.wafer_sort(lc, @@empty_carrier1, slots: (13..24).to_a)
    [lot, lc].each {|lot|
      @@lot_wafer_map[lot] = @@sv.lot_info(lot, wafers: true).wafers.collect {|w| w.send(@@slot_or_alias)}.sort
    }
    assert_equal 0, @@sv.lot_opelocate(lc, @@opNo_split2)
    assert_equal 0, @@sv.lot_opelocate(lot, @@opNo_split2)
    #
    # 2 child lots in one carrier, with merge op and PSM, resp.
    lot = @@testlots[14]
    # nono @@lots_nosplit << lot
    assert lc = @@sv.lot_split(lot, 10, mergeop: @@merge_op_child1), 'error splitting lot'
    assert_equal 0, @@sv.wafer_sort(lc, @@empty_carrier2, slots: (1..19).step(2).to_a)
    [lot, lc].each {|lot|
      @@lot_wafer_map[lot] = @@sv.lot_info(lot, wafers: true).wafers.collect {|w| w.send(@@slot_or_alias)}.sort
    }
    assert_equal 0, @@sv.lot_opelocate(lc, @@opNo_split2)
    @@lots_notification << lc
    @@lots_nosplit << lc
    lot = @@testlots[15]
    # nono @@lots_nosplit << lot
    assert lc = @@sv.lot_split(lot, 10), 'error splitting lot'
    assert_equal 0, @@sv.lot_experiment(lc, 8, @@branch_route, @@psm_split_op, @@psm_return_op, dynamic: true)
    assert_equal 0, @@sv.wafer_sort(lc, @@empty_carrier2, slots: (2..20).step(2).to_a)
    [lot, lc].each {|lot|
      @@lot_wafer_map[lot] = @@sv.lot_info(lot, wafers: true).wafers.collect {|w| w.send(@@slot_or_alias)}.sort
    }
    assert_equal 0, @@sv.lot_opelocate(lc, @@opNo_split2)
    @@lots_split << lc
    #
    # happy cases
    lot = @@testlots[16]
    @@lots_split << lot
    assert_equal 0, @@sv.lot_opelocate(lot, @@opNo_split1)
    lot = @@testlots[17]
    @@lots_split << lot
    assert_equal 0, @@sv.lot_opelocate(lot, @@opNo_split2)
    #
    # wrong merge PD
    lot = @@testlots[18]
    @@lots_nosplit << lot
    assert_equal 0, @@sv.lot_opelocate(lot, @@opNo_split3)
    #
    # lot on ERF route with wrong merge PD
    lot = @@testlots[19]
    @@lots_nosplit << lot
    assert_equal 0, @@sv.lot_experiment(lot, 25, @@branch_route2, @@psm_split_op, @@psm_return_op, dynamic: false)
    assert_equal 0, @@sv.lot_opelocate(lot, @@psm_split_op)
    assert_equal 0, @@sv.lot_opelocate(lot, @@opNo_split_erf)
    #
    $setup_ok = true
  end


  def test11_wait_nosplit
    $log.info "waiting #{@@job_interval} s for the jCAP job to run"; sleep @@job_interval
    #
    wrong = []
    @@lots_nosplit.each {|lot|
      li = @@sv.lot_info(lot)
      lf = @@sv.lot_family(lot) - [li.parent]
      wrong << lot if lf.size != 1
    }
    assert_empty wrong, 'wrong lot split'
  end

  def test12_verify_split
    wrong = []
    @@lots_split.each {|lot|
      $log.info "-- verify_split #{lot}"
      # assert the lot has children
      lf = @@sv.lot_family(lot)
      # consider only own children
      children = lf.select {|l| @@sv.lot_info(l).parent == lot && !@@lot_wafer_map.keys.include?(l)}
      ($log.warn "no split for #{lot}"; wrong << lot) if children.empty?
      #
      # check lots, beginning with parent
      wmap = @@lot_wafer_map[lot]
      ([lot] + children).each_with_index {|lot, i|
        ids = wmap[i * 7, 7]  #  7 wafers each are split
        if ids != @@sv.lot_info(lot, wafers: true).wafers.collect {|w| w.send(@@slot_or_alias)}.sort
          $log.warn "wrong alias for lot #{lot}"
          wrong << lot
        end
      }
      #
      # check mergehold, depending on PD UDATA
      li = @@sv.lot_info(lot)
      if li.opNo == @@opNo_split2
        $log.info 'verify merge holds are set'
        mghls = @@sv.lot_hold_list(lot, type: 'MergeHold') + @@sv.lot_futurehold_list(lot, type: 'MergeHold')
        ($log.warn "no merge (future) hold for #{lot}"; wrong << lot) if mghls.empty?
      else
        $log.info 'verify no merge holds are set'
        @@sv.lot_hold_list(lot, type: 'MergeHold').each {|h|
          ($log.warn "wrong merge hold for #{lot}"; wrong << lot) if h.related_lot == li.parent
        }
        @@sv.lot_futurehold_list(lot, type: 'MergeHold').each {|h|
          ($log.warn "wrong merge future hold for #{lot}"; wrong << lot) if h.related_lot == li.parent
        }
      end
    }
    assert_empty wrong, 'wrong lot split'
  end

  def test18_mergepd_not_available
    lot = @@testlots[18]
    hh = @@sv.lot_hold_list(lot, reason: @@hold_reason)
    assert_equal 1, hh.size, "lot #{lot} has no #{@@hold_reason} hold"
    assert hh.first.memo.start_with?(@@memo), "wrong claim memo at lot #{lot} hold"
  end

  def test19_erf_mergepd_not_available
    lot = @@testlots[19]
    hh = @@sv.lot_hold_list(lot, reason: @@hold_error)
    assert_equal 1, hh.size, "lot #{lot} has no #{@@hold_error} hold"
    assert hh.first.memo.start_with?(@@memo), "wrong claim memo at lot #{lot} hold"
  end

  def test81_notification
    $log.info "notification_time #{@@notification_time.inspect}"
    @@lots_notification.each {|lot|
      $log.info "checking notification for lot #{lot}"
      evs = @@notifications.query(tstart: @@notification_time, service: '%jCAP_AsyncNotification%', filter: {'LotID'=>lot})
      refute_empty evs, "no notification for lot #{lot}"
    }
  end


  def testman91_transaction_timeout
    # test lots (all @@testlots) that cannot be handled within one cycle are treated in the next one
    @@testlots.each {|lot|
      @@sv.lot_family(lot).each {|l| @@sv.lot_cleanup(l)}
      assert @@sv.merge_lot_family(lot)
      li = @@sv.lot_info(lot)
      assert_equal 0, @@sv.carrier_xfer_status_change(li.carrier, 'MI', @@svtest.stocker)
      assert_equal 0, @@sv.lot_opelocate(lot, @@opNo_split2), "error locating lot #{lot.inspect}"
    }
    #
    done = []
    previous_done_size = 0
    while true
      # wait for jCAP job
      $log.info "waiting #{@@job_interval} s for the jCAP job to run"; sleep @@job_interval
      $log.info "check split lots with merge hold:"
      # for not already done lots => check split & merge hold lots
      done += @@testlots.select {|lot|
        !done.include?(lot) && (@@sv.lot_family(lot).size == 4)
      }
      $log.info "done: #{done.inspect}, count: #{done.size}, previous count: #{previous_done_size}"
      # break loop if no remaining lots have been split during the last jCAP job run
      assert done.size > previous_done_size, "no split with merge hold found for #{(@@testlots - done).inspect}"
      # save last done lots count for verification
      break if done.size == @@testlots.size
      previous_done_size = done.size
      $log.info 'check again for remaining lots'
    end
    #
    refute_equal 0, previous_done_size, 'test not relevant because all lots have been treated in the first run'
  end

end
