=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.


Author: Steffen Steidten, 2010-11-03

Version: 18.09

History:
  2012-01-27 ssteidte, removed JMX console actions
  2015-04-07 ssteidte, use rtdemuext, check jCAP only with a test rule
  2017-01-05 sfrieske, use new RTD::EmuRemote, minor cleanup
  2018-11-07 sfrieske, minor cleanup
  2019-02-19 sfrieske, improved EmuRemote usage

Notes:
  Note: This job starts at initial.starting.time=09:00
  This job just acts on the RTD response, no logic contained.
=end

require 'rtdserver/rtdemuremote'
require 'SiViewTestCase'


class JCAP_MoveLotsOnHoldToBank < SiViewTestCase
  @@nonprobank = 'O-TOWING'
  @@rtd_rule = 'JCAP_MOVE_LOTS_ONHOLD_TO_BANK'  # default rule, was: QA_...
  @@columns = %w(LotId HoldCode HoldUser LotOwner SubLotType DestinationBank LotOwnerEmail)

  @@job_interval = 630  # 10 minutes


  def self.shutdown
    @@rtdremote.delete_emustation('', rule: @@rtd_rule)
  end

  def self.after_all_passed
    @@testlots.each {|lot| @@sv.delete_lot_family(lot)}
  end


  def test10_test
    @@rtdremote = RTD::EmuRemote.new($env)
    #
    assert @@testlots = @@svtest.new_lots(2), 'error creating test lots'
    #
    assert @@rtdremote.add_emustation_wait('', rule: @@rtd_rule, timeout: @@job_interval) {
      @@rtdremote.create_response(@@columns, {'rows'=>[[@@testlots[0], 'ENG', 'XXX-USER', 'XXX-OWNER', 'XSLT', @@nonprobank, '']]})
    }, 'rule has not been called'
    # verify lot1 is banked in, lot2 is not
    sleep 10
    assert_equal @@nonprobank, @@sv.lot_info(@@testlots[0]).bank, "#{@@testlots[0]} not banked in"
    assert_empty @@sv.lot_info(@@testlots[1]).bank, "#{@@testlots[1]} is banked in"
  end

end
