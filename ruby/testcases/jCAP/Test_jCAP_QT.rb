=begin
Test the QT controller.

additionaly check qt logs and qt controller website.
f36asd1//local/logs/jCAP/qt-controller.log
http://f36iesd1:8000/reporting/lot_qt.jsp

Start PD: A-QT-PD2 OpNo: 1000.1200
End PD:   A-QT-PD5 OpNo: 1000.1500
Meas PD:  A-QT-PDMEAS    1000.1600
Start PD: A-QT-PD21 OpNo: 2000.1200 for future holds
End PD:   A-QT-PD51 OpNo: 2000.1500
Tools: UTQT01... 06
ERF MainPDs:
RW MainPDs: A-QT-RWIG.01, A-QT-RWDL.01, A-QT-RWDA.01, A-QT-RWUD.01

load_run_testcase "Test_jCAP_QT", "itdc"

(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: Steffen Steidten, Thomas Klose 2011-05-11
        Adel Darwaish June 2012

todo:
    -check testcase for tc341, add proper assertions
    -check testcase 220, no assertions defined

QHTL claim memo example: Queue time of 10 min exceeded at 07/10/12 09:22:58 (hold action: HLD, target operation 1000.1500, DLIS comment: Add), criticality = {not defined}
=end

require "RubyTestCase"
#require "sjc"
require "thread"

class Test_jCAP_QT < RubyTestCase
  Description = "Test jCAP Q-Time job"

  include MonitorMixin

  @@tc100 = false  #process at start pd, process at end pd. No hold
  @@tc110 = true  #process at start pd, qt expires. Lot on hold
  @@tc120 = false  #opestart cancel at end pd after qt expires. Lot on hold
  @@tc130 = false  #process at start pd, locate to meas pd after end pd. Lot on hold.
  @@tc140 = false  #process at start pd, locate to first process pd after end pd. No hold.
  @@tc150 = false  #opecomp at end pd after qt expired, No hold.
  @@tc160 = false  #split lot in qt area, parent and child on hold.
  @@tc170 = false  #split lot in qt area, process child lot at end pd. Parent on hold. Childlot no hold.
  @@tc180 = false  #split before startpd, parent processed at startpd. Parent on hold.
  @@tc190 = false  #split before startpd, child processed at startpd with delay, merge in qt area. Hold at qt from parent lot
  @@tc191 = false  #split before startpd, parent processed at startpd with delay, merge in qt area. Hold at qt from child lot
  @@tc200 = false  #split before startpd, parent processed at startpd with delay, scrap child lot. Hold at qt from parent lot
  @@tc210 = false  #split lot in qt area, process both at end pd. No hold for both lots.
  @@tc220 = false  #see email...
  @@tc230 = false  #gate pass at end PD, no hold expected
  @@tc240 = false  #gate pass at end PD, process at measurement PD, no hold expected

  @@tc300 = false  #PSM before qt start pd, join in qt area
  @@tc310 = false  #PSM before start pd, join in qt area
  @@tc311 = false  #PSM before start pd, parent and child enter qt area, lots on hold.
  @@tc320 = false  #PSM in qt area, join in qt area
  @@tc330 = false  #PSM in qt area, join after qt area
  @@tc340 = false  #PSM in qt area, join pd after qt area without corresponding pd on main route, child enter erf pd only
  @@tc341 = false  #!!PSM in qt area, join after qt area without corresponding pd on main route, process child through erf
  @@tc350 = false  #PSM happy lot without holds
  @@tc360 = false  #PSM scenario, PSM ends inside QT area. Lot should go on hold.
  @@tc370 = false  #PSM scenario, PSM ends inside QT area. new partial PSM starts in QT area - not requiered!
  @@tc380 = false  #PSM scenario, PSM ends inside QT area. new partial PSM starts immediately in QT area and ends in qt area
  @@tc390 = false  #PSM scenario, PSM ends inside QT area. new PSM starts in QT area and ends in qt area
  @@tc391 = false  #PSM scenario, delete QT in PSM
  @@tc392 = false  #PSM scenario, retrigger PSM in QT
  @@tc393 = false  #PSM scenario, retrigger PSM in QT, process child lot to PSM END PD an Cancel Process after QT limit


  @@tc400 = false  #PSM outside QT area with own QT area. lot should go on hold.
  @@tc420 = false  #PSM accross module PDs, qt expires while lot is in erf area
  @@tc430 = false  #!PSM accross module PDs, qt expires while lot outside erf area
  @@tc440 = false  #!PSM across module PDs, partial psm
  @@tc450 = false  #!PSM accross module PDs, qt expires while lot is in erf area - ref tc420 (different erf split & merge pds)

  @@tc500 = false  #Lot on Rework route, ReworkType NULL
  @@tc501 = false  #Lot on Rework route, ReworkType NULL, child completes rework, check qt history for repeating updates
  @@tc510 = false  #Lot on Rework route, ReworkType IG
  @@tc520 = false  #Lot on Rework route, ReworkType DA
  @@tc530 = false  #Lot on Rework route, ReworkType DL
  @@tc540 = false  #Lot on Rework with Joining Operation after EndPD - ReworkType IG
  @@tc550 = false  #Lot on Rework with Joining Operation after EndPD - ReworkType DA
  @@tc560 = false  #Lot on Rework with Joining Operation after EndPD - ReworkType DL
  @@tc570 = false  #Lot on Rework route, ReworkType UD, scenario #1
  @@tc580 = false  #!Lot on Rework route, ReworkType UD, scenario #2
  #
  @@tc581 = false  #!Lot on Rework route, ReworkType UD, scenario #2 (futurehold WITH MAIN PD = A-QT-MAIN-ENH)
  #
  @@tc590 = false  #Lot on Rework route, ReworkType UD, scenario #3
  @@tc600 = false  #Lot on Rework route, ReworkType UD, scenario #4

  @@tc700 = false  #process at 2nd startpd (A-QT-PD21), qt expires, check future hold after A-QT-PD51
  @@tc710 = false  #process at 3nd startpd (A-QT-PD22), process lot at A-QT-PD3 (has sleep in post script) and check future hold
  @@tc720 = true   #!process at startpd and end pd, force oper complete at end pd, branch to PD A-QT-PROCESSABORT.01 back to qt area, lot on hold.
  @@tc730 = false  #split lot after startpd, move child through qt area, check hold. no hold expected
  @@tc740 = false  #process at startpd, process at endpd, split lot and rework child at endpd. RCR518439

  @@tc810 = false   #process at start pd, qt expires. Lot on hold- C2 criticality memo
  @@tc820 = false   #process at start pd, qt expires. Lot on hold- C4 criticality memo

  @@tc900 = false   #process at start pd (5000.1200). lot is not on hold after qt limit. QT action is LimitOnly
  @@tc910 = false   #process at start pd (5000.1200), locate to end pd. wait for qt and process at end pd. lot is not on hold after qt limit. QT action is LimitOnly

  @@tc999 = true # delete all created lots when testing is finished

  @@continue_test = false

  @@Hold_Code1 = "QTHL"   #WD test aktiv!
  #@@Hold_Code1 = "QTOV"
  @@Hold_Code2 = "QTOV"

  @@C1 = "C1"
  @@C2 = "C2"
  @@C4 = "C4"
  @@QT_time = 15 # in minutes. Configured in SiView for the related PD. default 10 min for ITDC

  def siview_time(ts)
    return($sv.siview_time(ts))
  end

  def test010_prepare
    @@qtstart = {}
    @@qthold = {}
    @@holds = {}
    @@lots = {}
    @@threads = []

    mutex = Mutex.new

    #process at start pd, process at end pd. No hold
    @@threads << Thread.new {
      sv = SiView::MM.new($env)
      @@lots['tc100'] = l = start_lot(sv)
      if l
        sv.lot_opelocate(l, "1000.1200")
        sv.claim_process_lot(l, :freeport=>true, :synchronize=>mutex)
        @@qtstart[l] = siview_time(sv.lot_info(l).claim_time)
        sleep 60
        sv.lot_opelocate(l, "1000.1500")
        sv.claim_process_lot(l, :freeport=>true, :synchronize=>mutex)
        $log.info("-- tc100 (lot #{l}) sleeps for 600 seconds")
        sleep 600
      end
    } if @@tc100


    #process at start pd, qt expires. Lot on hold
    @@threads << Thread.new {
      sv = SiView::MM.new($env)
      @@lots['tc110'] = l = start_lot(sv)
      if l
        sv.lot_opelocate(l, "1000.1200")
        sv.claim_process_lot(l, :freeport=>true, :synchronize=>mutex)
        @@qtstart[l] = siview_time(sv.lot_info(l).claim_time)
        $log.info("-- tc110 (lot #{l}) sleeps for 600 seconds")
        sleep 600
      end
    } if @@tc110

    #opestart cancel at end pd after qt expires. Lot on hold
    @@threads << Thread.new {
      sv = SiView::MM.new($env)
      @@lots['tc120'] = l = start_lot(sv)
      if l
        sv.lot_opelocate(l, "1000.1200")
        sv.claim_process_lot(l, :freeport=>true, :synchronize=>mutex)
        @@qtstart[l] = siview_time(sv.lot_info(l).claim_time)
        sleep 60
        sv.lot_opelocate(l, "1000.1500")
        sv.claim_process_lot(l, :freeport=>true, :synchronize=>mutex, :noopecomp=>true, :nounload=>true)
        $log.info("-- tc120 (lot #{l}) sleeps for 600 seconds")
        sleep 600
        li = sv.lot_info(l)
        mutex.synchronize {
          sv.eqp_opestart_cancel(li.eqp, li.cj)
          sv.eqp_unload(li.eqp, nil, li.carrier)
        }
        sleep 60    # wait for qt ctrl
      end
    } if @@tc120

    #process at start pd, locate to meas pd after end pd. Lot on hold.
    @@threads << Thread.new {
      sv = SiView::MM.new($env)
      @@lots['tc130'] = l = start_lot(sv)
      if l
        sv.lot_opelocate(l, "1000.1200")
        sv.claim_process_lot(l, :freeport=>true, :synchronize=>mutex)
        @@qtstart[l] = siview_time(sv.lot_info(l).claim_time)
        sleep 60
        sv.lot_opelocate(l, "1000.1600")
        $log.info("-- tc130 (lot #{l}) sleeps for 600 seconds")
        sleep 600
      end
    } if @@tc130

    #process at start pd, locate to first process pd after end pd. No hold.
    @@threads << Thread.new {
      sv = SiView::MM.new($env)
      @@lots['tc140'] = l = start_lot(sv)
      if l
        sv.lot_opelocate(l, "1000.1200")
        sv.claim_process_lot(l, :freeport=>true, :synchronize=>mutex)
        @@qtstart[l] = siview_time(sv.lot_info(l).claim_time)
        sleep 60
        sv.lot_opelocate(l, "1000.1700")
        $log.info("-- tc140 (lot #{l}) sleeps for 600 seconds")
        sleep 600
      end
    } if @@tc140

    #opecomp at end pd after qt expired, No hold.
    @@threads << Thread.new {
      sv = SiView::MM.new($env)
      @@lots['tc150'] = l = start_lot(sv)
      if l
        sv.lot_opelocate(l, "1000.1200")
        sv.claim_process_lot(l, :freeport=>true, :synchronize=>mutex)
        @@qtstart[l] = siview_time(sv.lot_info(l).claim_time)
        sleep 60
        sv.lot_opelocate(l, "1000.1500")
        $log.info("-- tc150 (lot #{l}) sleeps for 600 seconds")
        sv.claim_process_lot(l, :freeport=>true, :synchronize=>mutex, :proctime=>600)
      end
    } if @@tc150

    #split lot in qt area, parent and child on hold.
    @@threads << Thread.new {
      sv = SiView::MM.new($env)
      @@lots['tc160'] = l = start_lot(sv)
      if l
        sv.lot_opelocate(l, "1000.1200")
        sv.claim_process_lot(l, :freeport=>true, :synchronize=>mutex)
        @@qtstart[l] = siview_time(sv.lot_info(l).claim_time)
        sleep 60
        lc = sv.lot_split(l, 10)
        sv.claim_process_lot(l, :freeport=>true, :synchronize=>mutex)
        sv.claim_process_lot(lc, :freeport=>true, :synchronize=>mutex)
        sleep 600
      end
    } if @@tc160

    #split lot in qt area, process child lot at end pd. Parent on hold. Childlot no hold.
    @@threads << Thread.new {
      sv = SiView::MM.new($env)
      @@lots['tc170'] = l = start_lot(sv)
      if l
        sv.lot_opelocate(l, "1000.1200")
        sv.claim_process_lot(l, :freeport=>true, :synchronize=>mutex)
        @@qtstart[l] = siview_time(sv.lot_info(l).claim_time)
        sleep 60
        lc = sv.lot_split(l, 10)
        sv.claim_process_lot(l, :freeport=>true, :synchronize=>mutex)
        sv.lot_opelocate(lc, "1000.1500")
        sv.claim_process_lot(lc, :freeport=>true, :synchronize=>mutex)
        sleep 600
      end
    } if @@tc170

    #split before startpd, parent processed at startpd. Parent on hold.
    @@threads << Thread.new {
      sv = SiView::MM.new($env)
      @@lots['tc180'] = l = start_lot(sv)
      if l
        lc = sv.lot_split(l, 10)
        sv.lot_opelocate(l, "1000.1200")
        sv.claim_process_lot(l, :freeport=>true, :synchronize=>mutex)
        @@qtstart[l] = siview_time(sv.lot_info(l).claim_time)
        sleep 660
      end
    } if @@tc180

    #split before startpd, parent processed at startpd with delay, merge in qt area. Hold at qt from child lot
    @@threads << Thread.new {
      sv = SiView::MM.new($env)
      @@lots['tc190'] = l = start_lot(sv)
      if l
        lc = sv.lot_split(l, 10)
        sv.lot_opelocate(l, "1000.1200")
        sv.claim_process_lot(l, :freeport=>true, :synchronize=>mutex)
        sleep 60
        sv.lot_opelocate(lc, "1000.1200")
        sv.claim_process_lot(lc, :freeport=>true, :synchronize=>mutex)
        @@qtstart[l] = siview_time(sv.lot_info(l).claim_time)
        sleep 60
        sv.lot_merge(l, lc)
        sleep 500
        @@qthold[l] = siview_time(sv.lot_info(l).claim_time)
      end
    } if @@tc190

    #split before startpd, parent processed at startpd with delay, merge in qt area. Hold at qt from child lot
    @@threads << Thread.new {
      sv = SiView::MM.new($env)
      @@lots['tc191'] = l = start_lot(sv)
      if l
        lc = sv.lot_split(l, 10)
        sv.lot_opelocate(lc, "1000.1200")
        sv.claim_process_lot(lc, :freeport=>true, :synchronize=>mutex)
        sleep 60
        sv.lot_opelocate(l, "1000.1200")
        sv.claim_process_lot(l, :freeport=>true, :synchronize=>mutex)
        @@qtstart[l] = siview_time(sv.lot_info(lc).claim_time)
        sleep 60
        sv.lot_merge(l, lc)
        sleep 500
        @@qthold[l] = siview_time(sv.lot_info(l).claim_time)
      end
    } if @@tc191

    #split before startpd, parent processed at startpd with delay, scrap child lot. Hold at qt from parent lot
    @@threads << Thread.new {
      sv = SiView::MM.new($env)
      @@lots['tc200'] = l = start_lot(sv)
      if l
        lc = sv.lot_split(l, 10)
        sv.lot_opelocate(l, "1000.1200")
        sv.claim_process_lot(l, :freeport=>true, :synchronize=>mutex)
        @@qtstart[l] = siview_time(sv.lot_info(l).claim_time)
        sleep 60
        sv.scrap_wafers(lc)
        sleep 600
        @@qthold[l] = siview_time(sv.lot_info(l).claim_time)
      end
    } if @@tc200

    #split lot in qt area, process both at end pd. No hold for both lots.
    @@threads << Thread.new {
      sv = SiView::MM.new($env)
      @@lots['tc210'] = l = start_lot(sv)
      if l
        sv.lot_opelocate(l, "1000.1200")
        sv.claim_process_lot(l, :freeport=>true, :synchronize=>mutex)
        @@qtstart[l] = siview_time(sv.lot_info(l).claim_time)
        sleep 60
        lc = sv.lot_split(l, 10)
        sleep 60
        sv.lot_opelocate(l, "1000.1500")
        sv.claim_process_lot(l, :freeport=>true, :synchronize=>mutex)
        sleep 60
        sv.lot_opelocate(lc, "1000.1500")
        sv.claim_process_lot(lc, :freeport=>true, :synchronize=>mutex)
        sleep 450
      end
    } if @@tc210

    @@threads << Thread.new {
      sv = SiView::MM.new($env)
      @@lots['tc220'] = l = start_lot(sv)
      if l
        cl = sv.lot_split(l, 5)
        sv.lot_opelocate(cl, "1000.1200")
        sv.claim_process_lot(cl, :freeport=>true, :synchronize=>mutex)
        sleep 60
        lc = sv.lot_split(l, 10)
        sleep 60
        sv.lot_opelocate(l, "1000.1500")
        sv.claim_process_lot(l, :freeport=>true, :synchronize=>mutex)
        sleep 60
        sv.lot_opelocate(lc, "1000.1500")
        sv.claim_process_lot(lc, :freeport=>true, :synchronize=>mutex)
        sleep 450
        @@qtstart[l] = siview_time(sv.lot_info(l).claim_time)
      end
    } if @@tc220

    #process at start pd, gate pass end pd. No hold
    @@threads << Thread.new {
      sv = SiView::MM.new($env)
      @@lots['tc230'] = l = start_lot(sv)
      if l
        sv.lot_opelocate(l, "1000.1200")
        sv.claim_process_lot(l, :freeport=>true, :synchronize=>mutex)
        @@qtstart[l] = siview_time(sv.lot_info(l).claim_time)
        sleep 60
        sv.lot_opelocate(l, "1000.1500")
        sv.lot_gatepass(l)
        $log.info("-- tc230 (lot #{l}) sleeps for 600 seconds")
        sleep 600
      end
    } if @@tc230

    #process at start pd, gatepass at end pd, process at measurement pd. No hold and DELETE event in QT History
    @@threads << Thread.new {
      sv = SiView::MM.new($env)
      @@lots['tc240'] = l = start_lot(sv)
      if l
        sv.lot_opelocate(l, "1000.1200")
        sv.claim_process_lot(l, :freeport=>true, :synchronize=>mutex)
        @@qtstart[l] = siview_time(sv.lot_info(l).claim_time)
        sleep 60
        sv.lot_opelocate(l, "1000.1500")
        sv.lot_gatepass(l)
        sleep 60
        sv.claim_process_lot(l, :freeport=>true, :synchronize=>mutex)
        $log.info("-- tc240 (lot #{l}) sleeps for 600 seconds")
        sleep 540
      end
    } if @@tc240

    #PSM before start pd, join after qt area. parent on hold
    @@threads << Thread.new {
      sv = SiView::MM.new($env)
      @@lots['tc300'] = l = start_lot(sv)
      if l
        sv.lot_experiment(l, 10, "eA-QT-ERF-A0.01", "1000.1100", "1000.1600")
        sv.lot_opelocate(l, "1000.1100")
        lc = sv.lot_family(l)[-1]
        sv.lot_hold(lc, "AEHL")
        sv.lot_opelocate(l, "1000.1200")
        sv.claim_process_lot(l, :freeport=>true, :synchronize=>mutex)
        @@qtstart[l] = siview_time(sv.lot_info(l).claim_time)
        $log.info("parent #{sv.lot_info(l).opNo}")
        sleep 60
        sv.lot_hold_release(lc, nil)
        sv.claim_process_lot(lc, :freeport=>true, :synchronize=>mutex)
        @@qtstart[lc] = siview_time(sv.lot_info(lc).claim_time)
        $log.info("child #{sv.lot_info(lc).opNo}")
        sleep 720
        @@qthold[l] = siview_time(sv.lot_info(l).claim_time)
        @@qthold[lc] = siview_time(sv.lot_info(lc).claim_time)
      end
    } if @@tc300

    #PSM before start pd, join in qt area, lot on hold.
    @@threads << Thread.new {
      sv = SiView::MM.new($env)
      @@lots['tc310'] = l = start_lot(sv)
      if l
        sv.lot_experiment(l, 10, "eA-QT-ERF-A1.01", "1000.1100", "1000.1300")
        sv.lot_opelocate(l, "1000.1100")
        lc = sv.lot_family(l)[-1]
        sv.lot_hold(lc, "AEHL")
        sv.lot_opelocate(l, +1)
        sv.claim_process_lot(l, :freeport=>true, :synchronize=>mutex)
        @@qtstart[l] = siview_time(sv.lot_info(l).claim_time)
        sleep 60
        sv.lot_hold_release(lc, nil)
        2.times{sv.claim_process_lot(lc, :freeport=>true, :synchronize=>mutex)}
        sv.claim_process_lot(l, :freeport=>true, :synchronize=>mutex)
        sv.lot_merge(l, lc)
        sleep 600
        @@qthold[l] = siview_time(sv.lot_info(l).claim_time)
      end
    } if @@tc310

    #PSM before start pd, parent and child enter qt area, lots on hold.
    @@threads << Thread.new {
      sv = SiView::MM.new($env)
      @@lots['tc311'] = l = start_lot(sv)
      if l
        sv.lot_experiment(l, 10, "eA-QT-ERF-A1.01", "1000.1100", "1000.1300")
        sv.lot_opelocate(l, "1000.1100")
        lc = sv.lot_family(l)[-1]
        sv.lot_hold(lc, "AEHL")
        sv.lot_opelocate(l, +1)
        sv.claim_process_lot(l, :freeport=>true, :synchronize=>mutex)
        @@qtstart[l] = siview_time(sv.lot_info(l).claim_time)
        sleep 60
        sv.lot_hold_release(lc, nil)
        sv.claim_process_lot(lc, :freeport=>true, :synchronize=>mutex)
        sleep 600
        @@qthold[l] = siview_time(sv.lot_info(l).claim_time)
      end
    } if @@tc311

    #PSM in qt area, join in qt area
    @@threads << Thread.new {
      sv = SiView::MM.new($env)
      @@lots['tc320'] = l = start_lot(sv)
      if l
        sv.lot_experiment(l, 10, "eA-QT-ERF-A2.01", "1000.1300", "1000.1400")
        sv.lot_opelocate(l, "1000.1200")
        sv.claim_process_lot(l, :freeport=>true, :synchronize=>mutex)
        @@qtstart[l] = siview_time(sv.lot_info(l).claim_time)
        sv.lot_opelocate(l, +1)
        lc = sv.lot_family(l)[-1]
        sv.lot_hold(lc, "AEHL")
        sleep 60
        sv.lot_hold_release(lc, nil)
        2.times{sv.claim_process_lot(lc, :freeport=>true, :synchronize=>mutex)}
        sv.lot_merge(l, lc)
        sleep 600
        @@qthold[l] = siview_time(sv.lot_info(l).claim_time)
      end
    } if @@tc320

    #PSM in qt area, join after qt area
    @@threads << Thread.new {
      sv = SiView::MM.new($env)
      @@lots['tc330'] = l = start_lot(sv)
      if l
        sv.lot_opelocate(l, "1000.1200")
        sv.lot_experiment(l, 10, "eA-QT-ERF-A3.01", "1000.1400", "1000.1600")
        sv.claim_process_lot(l, :freeport=>true, :synchronize=>mutex)
        sv.lot_opelocate(l, "1000.1400")
        lc = sv.lot_family(l)[-1]
        sv.lot_hold(lc, "AEHL")
        sv.claim_process_lot(l, :freeport=>true, :synchronize=>mutex)
        @@qtstart[l] = siview_time(sv.lot_info(l).claim_time)
        sleep 60
        sv.lot_hold_release(lc, nil)
        2.times{sv.claim_process_lot(lc, :freeport=>true, :synchronize=>mutex)}
        sv.claim_process_lot(l, :freeport=>true, :synchronize=>mutex)
        sv.lot_merge(l, lc)
        2.times{sv.claim_process_lot(l, :freeport=>true, :synchronize=>mutex)}
        sleep 600
        @@qthold[l] = siview_time(sv.lot_info(l).claim_time)
      end
    } if @@tc330

    #PSM in qt area, join after qt area without corresponding pd on main route
    @@threads << Thread.new {
      sv = SiView::MM.new($env)
      @@lots['tc340'] = l = start_lot(sv)
      if l
        sv.lot_experiment(l, 10, "eA-QT-ERF-A4.01", "1000.1400", "1000.1600", :mrgOpNo=>"1000.1800")
        sv.lot_opelocate(l, "1000.1200")
        sv.claim_process_lot(l, :freeport=>true, :synchronize=>mutex)
        @@qtstart[l] = siview_time(sv.lot_info(l).claim_time)
        sv.lot_opelocate(l, "1000.1400")
        lc = sv.lot_family(l)[-1]
        @@qtstart[lc] = @@qtstart[l]
        sv.claim_process_lot(l, :freeport=>true, :synchronize=>mutex)
        sleep 720
        @@qthold[l] = siview_time(sv.lot_info(l).claim_time)
        @@qthold[lc] = siview_time(sv.lot_info(lc).claim_time)
      end
    } if @@tc340

    #PSM in qt area, join after qt area without corresponding pd on main route
    @@threads << Thread.new {
      sv = SiView::MM.new($env)
      @@lots['tc341'] = l = start_lot(sv)
      if l
        sv.lot_experiment(l, 10, "eA-QT-ERF-A4.01", "1000.1400", "1000.1600", :mrgOpNo=>"1000.1800")
        sv.lot_opelocate(l, "1000.1200")
        sv.claim_process_lot(l, :freeport=>true, :synchronize=>mutex)
        @@qtstart[l] = siview_time(sv.lot_info(l).claim_time)
        sv.lot_opelocate(l, "1000.1400")
        lc = sv.lot_family(l)[-1]
        @@qtstart[lc] = @@qtstart[l]
        sv.claim_process_lot(l, :freeport=>true, :synchronize=>mutex)
        sleep 60
        sv.claim_process_lot(lc, :freeport=>true, :synchronize=>mutex)
        sleep 600
        sv.claim_process_lot(lc, :freeport=>true, :synchronize=>mutex)
        sleep 540
        @@qthold[l] = siview_time(sv.lot_info(l).claim_time)
        @@qthold[lc] = siview_time(sv.lot_info(lc).claim_time)
      end
    } if @@tc341


    #PSM happy lot without holds
    @@threads << Thread.new {
      sv = SiView::MM.new($env)
      @@lots['tc350'] = l = start_lot(sv)
      if l
        sv.lot_experiment(l, 10, "eA-QT-ERF-A1.01", "1000.1100", "1000.1300")
        sv.lot_experiment(l, 10, "eA-QT-ERF-A3.01", "1000.1400", "1000.1600")
        sv.lot_opelocate(l, "1000.1100")
        lc = sv.lot_family(l)[-1]
        sv.claim_process_lot(l, :freeport=>true, :synchronize=>mutex)
        sv.lot_opelocate(lc, +1)
        sv.lot_opelocate(l, +1)
        sleep 10
        sv.lot_merge(l, lc)
        sleep 30
        sv.lot_opelocate(l, +1)
        lc = sv.lot_family(l)[-1]
        sv.lot_opelocate(lc, +1)
        sv.claim_process_lot(lc, :freeport=>true, :synchronize=>mutex)
        sv.lot_opelocate(l, +1)
        sv.claim_process_lot(l, :freeport=>true, :synchronize=>mutex)
        sleep 10
        sv.lot_merge(l, lc)
        sleep 30
        sv.claim_process_lot(l, :freeport=>true, :synchronize=>mutex)
        sleep 550
      end
    } if @@tc350

    #PSM scenario, PSM ends inside QT area. Lot should go on hold.
    @@threads << Thread.new {
      sv = SiView::MM.new($env)
      @@lots['tc360'] = l = start_lot(sv)
      if l
        sv.lot_experiment(l, 25, "eA-QT-ERF-A1.01", "1000.1100", "1000.1300")
        sv.lot_opelocate(l, "1000.1100")
		sleep 60
        2.times{sv.claim_process_lot(l, :freeport=>true, :synchronize=>mutex)}
        @@qtstart[l] = siview_time(sv.lot_info(l).claim_time)
        $log.info("-- tc360 (lot #{l}) sleeps for 660 seconds")
        sleep 660
      end
    } if @@tc360

    #PSM scenario, PSM ends inside QT area. new partial PSM starts in QT area
    @@threads << Thread.new {
      sv = SiView::MM.new($env)
      @@lots['tc370'] = l = start_lot(sv)
      if l
        sv.lot_experiment(l, 25, "eA-QT-ERF-A1.01", "1000.1100", "1000.1300")
        sv.lot_experiment(l, 10, "eA-QT-ERF-A2.01", "1000.1300", "1000.1400")
        sv.lot_opelocate(l, "1000.1100")
        3.times{sv.claim_process_lot(l, :freeport=>true, :synchronize=>mutex)}
        @@qtstart[l] = siview_time(sv.lot_info(l).claim_time)
        $log.info("-- tc370 (lot #{l}) sleeps for 600 seconds")
        sleep 660
      end
    } if @@tc370

    #process at start pd, process at end pd. No hold
    @@threads << Thread.new {
      sv = SiView::MM.new($env)
      @@lots['tc380'] = l = start_lot(sv)
      if l
        sv.lot_experiment(l, 25, "eA-QT-ERF-A1.01", "1000.1100", "1000.1300")
        sv.lot_experiment(l, 10, "eA-QT-ERF-A2.01", "1000.1300", "1000.1400")
        sv.lot_opelocate(l, "1000.1100")
        sv.lot_opelocate(l, "1000.2100")
        sv.claim_process_lot(l, :freeport=>true, :synchronize=>mutex)
        lc = sv.lot_family(l)[-1]
        sv.claim_process_lot(l, :freeport=>true, :synchronize=>mutex)
        2.times{sv.claim_process_lot(lc, :freeport=>true, :synchronize=>mutex)}
        sv.lot_merge(l, lc)
        @@qtstart[l] = siview_time(sv.lot_info(l).claim_time)
        $log.info("-- tc380 (lot #{l}) sleeps for 600 seconds")
        sleep 660
      end
    } if @@tc380

    #process at start pd, process at end pd. No hold
    @@threads << Thread.new {
      sv = SiView::MM.new($env)
      @@lots['tc390'] = l = start_lot(sv)
      if l
        sv.lot_experiment(l, 25, "eA-QT-ERF-A2.01", "1000.1300", "1000.1400")
        sv.lot_experiment(l, 25, "eA-QT-ERF-A3.01", "1000.1400", "1000.1600")
        sv.lot_opelocate(l, "1000.1200")
        sv.claim_process_lot(l, :freeport=>true, :synchronize=>mutex)
        @@qtstart[l] = siview_time(sv.lot_info(l).claim_time)
        sleep 60
        3.times{sv.claim_process_lot(l, :freeport=>true, :synchronize=>mutex)}
        $log.info("-- tc390 (lot #{l}) sleeps for 600 seconds")
        sleep 660
      end
    } if @@tc390

    #PSM scenario, delete QT in PSM
    @@threads << Thread.new {
      sv = SiView::MM.new($env)
      @@lots['tc391'] = l = start_lot(sv)
      if l
        sv.lot_experiment(l, 25, "eA-QT-ERF-A5.01", "1000.1300", "1000.1400")
        sv.lot_opelocate(l, "1000.1200")
        sv.claim_process_lot(l, :freeport=>true, :synchronize=>mutex)
        @@qtstart[l] = siview_time(sv.lot_info(l).claim_time)
        sleep 60
        sv.claim_process_lot(l, :freeport=>true, :synchronize=>mutex)
        $log.info("-- tc391 (lot #{l}) sleeps for 600 seconds")
        sleep 660
      end
    } if @@tc391

    #PSM scenario, retrigger QT in PSM
    @@threads << Thread.new {
      sv = SiView::MM.new($env)
      @@lots['tc392'] = l = start_lot(sv)
      if l
        sv.lot_experiment(l, 25, "eA-QT-ERF-A6.01", "1000.1300", "1000.1400")
        sv.lot_opelocate(l, "1000.1200")
        sv.claim_process_lot(l, :freeport=>true, :synchronize=>mutex)
        sleep 60
        sv.claim_process_lot(l, :freeport=>true, :synchronize=>mutex)
        @@qtstart[l] = siview_time(sv.lot_info(l).claim_time)
        $log.info("-- tc392 (lot #{l}) sleeps for 660 seconds")
        sleep 660
        @@qthold[l] = siview_time(sv.lot_info(l).claim_time)
      end
    } if @@tc392

    #PSM scenario, retrigger PSM in QT, process child lot to PSM END PD an Cancel Process after QT limit
    @@threads << Thread.new {
      sv = SiView::MM.new($env)
      @@lots['tc393'] = l = start_lot(sv)
      if l
        sv.lot_experiment(l, 10, "eA-QT-ERF-A3.01", "1000.1400", "1000.1600")
        sv.lot_opelocate(l, "1000.1200")
        sv.claim_process_lot(l, :freeport=>true, :synchronize=>mutex)
        @@qtstart[l] = siview_time(sv.lot_info(l).claim_time)
        sleep 60
        sv.lot_opelocate(l, "1000.1400")
        lc = sv.lot_family(l).last
        sv.claim_process_lot(lc, :freeport=>true, :synchronize=>mutex)
        sv.claim_process_lot(lc, :freeport=>true, :synchronize=>mutex, :nounload=>true, :noopecomp=>true)
        @@qtstart[lc] = siview_time(sv.lot_info(l).claim_time)
        $log.info("-- tc393 (lot #{l}) sleeps for 660 seconds")
        sleep 660
        li = sv.lot_info(lc)
        sv.eqp_opestart_cancel(li.eqp, li.cj)
        sleep 60
        @@qthold[l] = siview_time(sv.lot_info(l).claim_time)
      end
    } if @@tc393

    #PSM outside QT area with own QT area. lot should go on hold. (A-QT-ERFwithQT-A1)
    @@threads << Thread.new {
      sv = SiView::MM.new($env)
      @@lots['tc400'] = l = start_lot(sv)
      if l
#        sv.lot_experiment(l, 25, "A-QT-ERF-A1.01", "1000.1100", "1000.1300")
        sv.lot_experiment(l, 25, "eA-QT-ERFwithQT-A1.01", "1000.1100", "1000.1600")
        2.times {sv.claim_process_lot(l, :freeport=>true, :synchronize=>mutex)
            sleep 60}
        $log.info("-- tc400 (lot #{l}) sleeps for 600 seconds")
        sleep 660
      end
    } if @@tc400

    #PSM accross module PDs, qt expires while lot is in erf area
    @@threads << Thread.new {
      sv = SiView::MM.new($env)
      @@lots['tc420'] = l = start_lot(sv, "A-QT-PRODUCT.03", "A-QT-MAIN.03")
      if l
        sv.lot_experiment(l, 25, "A-QT-ERF-A2.02", "1000.1300", "2000.1400")

        sv.lot_opelocate(l, "1000.1200")
        sv.claim_process_lot(l, :freeport=>true, :synchronize=>mutex)
        @@qtstart[l] = siview_time(sv.lot_info(l).claim_time)
        sleep 60
        # sv.claim_process_lot(l, :freeport=>true, :synchronize=>mutex)
        $log.info("-- tc420 (lot #{l}) sleeps for 600 seconds")
        sleep 660
      end
    } if @@tc420

    #PSM accross module PDs, qt expires while lot is outside erf area
    @@threads << Thread.new {
      sv = SiView::MM.new($env)
      @@lots['tc430'] = l = start_lot(sv, "A-QT-PRODUCT.03", "A-QT-MAIN.03")
      if l
        sv.lot_experiment(l, 25, "A-QT-ERF-A2.01", "1000.1300", "2000.1400")
        sv.lot_opelocate(l, "1000.1200")
        sv.claim_process_lot(l, :freeport=>true, :synchronize=>mutex)
        @@qtstart[l] = siview_time(sv.lot_info(l).claim_time)
        sleep 60
        3.times{sv.claim_process_lot(l, :freeport=>true, :synchronize=>mutex)}
        $log.info("-- tc430 (lot #{l}) sleeps for 660 seconds")
        sleep 660
      end
    } if @@tc430

    #PSM accross module PDs, qt expires while lot is outside erf area
    @@threads << Thread.new {
      sv = SiView::MM.new($env)
      @@lots['tc440'] = l = start_lot(sv, "A-QT-PRODUCT.03", "A-QT-MAIN.03")
      if l
        sv.lot_experiment(l, 10, "eA-QT-ERF-A2.01", "1000.1300", "2000.1400")
        sv.lot_opelocate(l, "1000.1200")
        sv.claim_process_lot(l, :freeport=>true, :synchronize=>mutex)
        @@qtstart[l] = siview_time(sv.lot_info(l).claim_time)
        lc = sv.lot_family(l)[-1]
        sleep 60
        sv.claim_process_lot(lc, :freeport=>true, :synchronize=>mutex)
        5.times{sv.claim_process_lot(l, :freeport=>true, :synchronize=>mutex)}
        $log.info("-- tc440 (lot #{l}) sleeps for 600 seconds")
        sleep 660
      end
    } if @@tc440

    #PSM in the same module PDs, qt expires while lot is in erf area. erf starts on QT EndPD
    @@threads << Thread.new {
      sv = SiView::MM.new($env)
      @@lots['tc450'] = l = start_lot(sv, "A-QT-PRODUCT.03", "A-QT-MAIN.03")
      $log.info "wait 10s"
      sleep 10
      if l
        sv.lot_experiment(l, 25, "eA-QT-ERF-A2.03", "1000.1700", "1000.1800")
        $log.info "wait 10s"
        sleep 10
        sv.lot_opelocate(l, "1000.1600")
        $log.info "wait 10s"
        sleep 10
        sv.claim_process_lot(l, :freeport=>true, :synchronize=>mutex)
        $log.info "wait 60s"
        sleep 60
        # set lot on the ERF automatically.
#        sv.lot_opelocate(l, "1000.1700")
        @@qtstart[l] = siview_time(sv.lot_info(l).claim_time)
        $log.info("-- tc450 (lot #{l}) sleeps for 180 seconds")
        sleep 180
      end
    } if @@tc450

    #@@tc500 Lot on Rework route, ReworkType NULL
    @@threads << Thread.new {
      sv = SiView::MM.new($env)
      @@lots['tc500'] = l = start_lot(sv)
      if l
        sv.lot_opelocate(l, "1000.1200")
        sv.claim_process_lot(l, :freeport=>true, :synchronize=>mutex)
        @@qtstart[l] = siview_time(sv.lot_info(l).claim_time)
        sleep 30
        sv.lot_opelocate(l, "1000.1400")
        sv.lot_rework(l, "A-QT-RWNULL.01")
        sleep 30
        sv.lot_rework_cancel(l)
        sv.lot_partial_rework(l, 10, "A-QT-RWNULL.01")
        lc = sv.lot_family(l)[-1]
        @@qtstart[lc] = @@qtstart[l]
        sleep 600
        @@qthold[l] = siview_time(sv.lot_info(l).claim_time)
        @@qthold[lc] = siview_time(sv.lot_info(lc).claim_time)
      end
    } if @@tc500

    #@@tc501 Lot on Rework route, ReworkType NULL, child completes rework, check qt history for repeating updates
    @@threads << Thread.new {
      sv = SiView::MM.new($env)
      @@lots['tc501'] = l = start_lot(sv)
      if l
        sv.lot_opelocate(l, "1000.1200")
        sv.claim_process_lot(l, :freeport=>true, :synchronize=>mutex)
        @@qtstart[l] = siview_time(sv.lot_info(l).claim_time)
        sleep 60
        sv.lot_opelocate(l, "1000.1400")
        4.times {|i|
          lc = sv.lot_partial_rework(l, i+1, "A-QT-RWNULL.01")
          $log.info("child lot #{i} with lot id #{lc}")
          sleep 55
          2.times {sv.claim_process_lot(lc, :freeport=>true, :synchronize=>mutex)}
          sleep 5
          sv.lot_merge(l, lc)
          sleep 5
        }
        sleep 420
        @@qthold[l] = siview_time(sv.lot_info(l).claim_time)
      end
    } if @@tc501

    #@@tc510 Lot on Rework route, ReworkType IG
    @@threads << Thread.new {
      sv = SiView::MM.new($env)
      @@lots['tc510'] = l = start_lot(sv)
      if l
        sv.lot_opelocate(l, "1000.1200")
        sv.claim_process_lot(l, :freeport=>true, :synchronize=>mutex)
        @@qtstart[l] = siview_time(sv.lot_info(l).claim_time)
        sleep 30
        sv.lot_opelocate(l, "1000.1400")
        sv.lot_rework(l, "A-QT-RWIG.01")
        sleep 30
        sv.lot_rework_cancel(l)
        sv.lot_partial_rework(l, 10, "A-QT-RWIG.01")
        lc = sv.lot_family(l)[-1]
        @@qtstart[lc] = @@qtstart[l]
        sleep 600
        @@qthold[l] = siview_time(sv.lot_info(l).claim_time)
        @@qthold[lc] = siview_time(sv.lot_info(lc).claim_time)
      end
    } if @@tc510

    #@@tc520 Lot on Rework route, ReworkType DA
    @@threads << Thread.new {
      sv = SiView::MM.new($env)
      @@lots['tc520'] = l = start_lot(sv)
      if l
        sv.lot_opelocate(l, "1000.1200")
        sv.claim_process_lot(l, :freeport=>true, :synchronize=>mutex)
        @@qtstart[l] = siview_time(sv.lot_info(l).claim_time)
        sleep 30
        sv.lot_opelocate(l, "1000.1400")
        sv.lot_rework(l, "A-QT-RWDA.01", :return_opNo=>"1000.1100")
        sleep 30
        sv.lot_rework_cancel(l)
        sv.lot_partial_rework(l, 10, "A-QT-RWDA.01", :return_opNo=>"1000.1100")
        lc = sv.lot_family(l)[-1]
        sleep 600
        @@qthold[l] = siview_time(sv.lot_info(l).claim_time)
      end
    } if @@tc520

    #@@tc530 Lot on Rework route, ReworkType DL
    @@threads << Thread.new {
      sv = SiView::MM.new($env)
      @@lots['tc530'] = l = start_lot(sv)
      if l
        sv.lot_opelocate(l, "1000.1200")
        sv.claim_process_lot(l, :freeport=>true, :synchronize=>mutex)
        @@qtstart[l] = siview_time(sv.lot_info(l).claim_time)
        sleep 30
        sv.lot_opelocate(l, "1000.1400")
        sv.lot_rework(l, "A-QT-RWDL.01", :return_opNo=>"1000.1100")
        sleep 30
        sv.lot_rework_cancel(l)
        sv.lot_partial_rework(l, 10, "A-QT-RWDL.01", :return_opNo=>"1000.1100")
        lc = sv.lot_family(l)[-1]
        @@qtstart[lc] = @@qtstart[l]
        sleep 600
        @@qthold[l] = siview_time(sv.lot_info(l).claim_time)
        @@qthold[lc] = siview_time(sv.lot_info(lc).claim_time)
      end
    } if @@tc530

    #@@tc540 Lot on Rework with Joining Operation after EndPD - ReworkType IG
    @@threads << Thread.new {
      sv = SiView::MM.new($env)
      @@lots['tc540'] = l = start_lot(sv)
      if l
        sv.lot_opelocate(l, "1000.1200")
        sv.claim_process_lot(l, :freeport=>true, :synchronize=>mutex)
        sleep 30
        sv.lot_opelocate(l, "1000.1500")
        sv.lot_rework(l, "A-QT-RWIG.01", :return_opNo=>"1000.1600")
        sleep 30
        sv.lot_opelocate(l, +3)
        sleep 600
      end
    } if @@tc540

    #@@tc550 Lot on Rework with Joining Operation after EndPD - ReworkType DA
    @@threads << Thread.new {
      sv = SiView::MM.new($env)
      @@lots['tc550'] = l = start_lot(sv)
      if l
        sv.lot_opelocate(l, "1000.1200")
        sv.claim_process_lot(l, :freeport=>true, :synchronize=>mutex)
        sleep 30
        sv.lot_opelocate(l, "1000.1500")
        sv.lot_rework(l, "A-QT-RWDA.01", :return_opNo=>"1000.1600")
        sleep 30
        sv.lot_opelocate(l, +3)
        sleep 530
      end
    } if @@tc550

    #@@tc560 Lot on Rework with Joining Operation after EndPD - ReworkType DL
    @@threads << Thread.new {
      sv = SiView::MM.new($env)
      @@lots['tc560'] = l = start_lot(sv)
      if l
        sv.lot_opelocate(l, "1000.1200")
        sv.claim_process_lot(l, :freeport=>true, :synchronize=>mutex)
        sleep 30
        sv.lot_opelocate(l, "1000.1500")
        sv.lot_rework(l, "A-QT-RWDL.01", :return_opNo=>"1000.1600")
        sleep 30
        sv.lot_opelocate(l, +3)
        sleep 530
      end
    } if @@tc560

    #@@tc570 Lot on Rework route, ReworkType UD, scenario #1
    @@threads << Thread.new {
      sv = SiView::MM.new($env)
      @@lots['tc570'] = l = start_lot(sv)
      if l
        sv.lot_opelocate(l, "1000.1200")
        sv.claim_process_lot(l, :freeport=>true, :synchronize=>mutex)
        @@qtstart[l] = siview_time(sv.lot_info(l).claim_time)
        sleep 30
        sv.lot_opelocate(l, "1000.1400")
        sv.lot_rework(l, "A-QT-RWUD.01")
        sleep 30
        sv.lot_rework_cancel(l)
        sv.lot_rework(l, "A-QT-RWUD.01")
        3.times{sv.claim_process_lot(l, :freeport=>true, :synchronize=>mutex)}
        sv.lot_opelocate(l, +1)
        sv.claim_process_lot(l, :freeport=>true, :synchronize=>mutex)
        sleep 530
        @@qthold[l] = siview_time(sv.lot_info(l).claim_time)
      end
    } if @@tc570

    #@@tc580 Lot on Rework route, ReworkType UD, scenario #2
    @@threads << Thread.new {
      sv = SiView::MM.new($env)
      @@lots['tc580'] = l = start_lot(sv)
      if l
        sv.lot_opelocate(l, "1000.1200")
        sv.claim_process_lot(l, :freeport=>true, :synchronize=>mutex)
        sleep 30
        sv.lot_opelocate(l, "1000.1400")
        sv.lot_rework(l, "A-QT-RWUD.01")
        @@holds[l] = checkhold(l)
        sleep 600
        # check hold status for this lot
        2.times{sv.claim_process_lot(l, :freeport=>true, :synchronize=>mutex)}
        @@qtstart[l] = siview_time(sv.lot_info(l).claim_time)
        sv.claim_process_lot(l, :freeport=>true, :synchronize=>mutex)
        sleep 610
        @@qthold[l] = siview_time(sv.lot_info(l).claim_time)
      end
    } if @@tc580

    #@@tc581 Lot on Rework route, ReworkType UD, scenario #2-1 special test ((QT futureHold on 1000.1600)^
    @@threads << Thread.new {
      sv = SiView::MM.new($env)
      @@lots['tc581'] = l = start_lot(sv, "A-QT-PRODUCT-ENH.01", "A-QT-MAIN-ENH.01")
      if l
        sv.lot_opelocate(l, "1000.1200")
        sv.claim_process_lot(l, :freeport=>true, :synchronize=>mutex)
        sleep 65
        sv.lot_opelocate(l, "1000.1400")
        sv.lot_rework(l, "A-QT-RWUD.01")
		# check hold status for this lot
        @@holds[l] = checkhold(l)
		$log.info "wait 600s ..."
        sleep 600
        # peocess first two PDs on the rework route
        2.times{sv.claim_process_lot(l, :freeport=>true, :synchronize=>mutex)}
        @@qtstart[l] = siview_time(sv.lot_info(l).claim_time)
		# process the last PD on the rework route
        sv.claim_process_lot(l, :freeport=>true, :synchronize=>mutex)
		# lot is on 1000.1400
		$log.info "wait 610s ..."
        sleep 610
		# after second QT has expired process claim the next OP (1000.1500)
		2.times{sv.claim_process_lot(l, :freeport=>true, :synchronize=>mutex)}
		# lot is on 1000.1600, the lot get hold because of the configured QT FurtureHold
        @@qthold[l] = siview_time(sv.lot_info(l).claim_time)
      end
    } if @@tc581

    #@@tc590 Lot on Rework route, ReworkType UD, scenario #3
    @@threads << Thread.new {
      sv = SiView::MM.new($env)
      @@lots['tc590'] = l = start_lot(sv)
      if l
        sv.lot_opelocate(l, "1000.1200")
        sv.claim_process_lot(l, :freeport=>true, :synchronize=>mutex)
        sleep 60
        sv.lot_opelocate(l, "1000.1400")
        sv.lot_rework(l, "A-QT-RWUD.01")
        sleep 60
        2.times{sv.claim_process_lot(l, :freeport=>true, :synchronize=>mutex)}
        @@qtstart[l] = siview_time(sv.lot_info(l).claim_time)
        sleep 630
        @@qthold[l] = siview_time(sv.lot_info(l).claim_time)
      end
    } if @@tc590

    #@@tc600 Lot on Rework route, ReworkType UD, scenario #4
    @@threads << Thread.new {
      sv = SiView::MM.new($env)
      @@lots['tc600'] = l = start_lot(sv)
      if l
        sv.lot_opelocate(l, "1000.1200")
        sv.claim_process_lot(l, :freeport=>true, :synchronize=>mutex)
        @@qtstart[l] = siview_time(sv.lot_info(l).claim_time)
        sleep 30
        sv.lot_opelocate(l, "1000.1400")
        sv.lot_partial_rework(l, 10, "A-QT-RWUD.01")
        lc = sv.lot_family(l)[-1]
        sleep 60
        3.times{sv.claim_process_lot(lc, :freeport=>true, :synchronize=>mutex)}
        @@qtstart[lc] = siview_time(sv.lot_info(lc).claim_time)
        sleep 630
        @@qthold[l] = siview_time(sv.lot_info(l).claim_time)
        @@qthold[lc] = siview_time(sv.lot_info(lc).claim_time)
      end
    } if @@tc600

    #@@tc700 locate lot to 2000.1200, process and wait. check future hold at 2000.1500
    @@threads << Thread.new {
      sv = SiView::MM.new($env)
      @@lots['tc700'] = l = start_lot(sv)
      if l
        sv.lot_opelocate(l, "2000.1200")
        sv.claim_process_lot(l, :freeport=>true, :synchronize=>mutex)
        sleep 660
      end
    } if @@tc700

    #process at 3nd startpd (A-QT-PD22), process lot at A-QT-PD3 (has sleep in post script) and check future hold
    @@threads << Thread.new {
      sv = SiView::MM.new($env)
      @@lots['tc710'] = l = start_lot(sv)
      if l
        sv.lot_opelocate(l, "3000.1200")
        sv.claim_process_lot(l, :freeport=>true, :synchronize=>mutex)
        @@qtstart[l] = siview_time(sv.lot_info(l).claim_time)
        sleep 580
        sv.claim_process_lot(l, :freeport=>true, :synchronize=>mutex, :nounload=>true)
        sleep 60
        res = sv.lot_futurehold_list(l)
        r = res[0]
        @@holds['tc710'] = r['reason'] if r
        sv.eqp_unload("UTQT03", nil, sv.lot_info(l).carrier)
        sv.pp_force_delete(sv.lot_info(l).carrier, '')
      end
    } if @@tc710

    #process at startpd and end pd, force oper complete at end pd, branch to PD A-QT-PROCESSABORT.01 back to qt area, lot on hold.
    @@threads << Thread.new {
      sv = SiView::MM.new($env)
      @@lots['tc720'] = l = start_lot(sv)
      if l
        sv.lot_opelocate(l, "1000.1200")
        sv.claim_process_lot(l, :freeport=>true, :synchronize=>mutex)
        @@qtstart[l] = siview_time(sv.lot_info(l).claim_time)
        $log.info "(1) wait 60 s ...."
        sleep 60
        sv.lot_opelocate(l, "1000.1500")
        $log.info "(2) wait 300 s ...."
        sleep 300
        sv.claim_process_lot(l, :freeport=>true, :synchronize=>mutex, :noopecomp=>true)
        $log.info "(3) wait 90 s ...."
        sleep 90

        li = sv.lot_info(l)
        sv.running_hold(l, li.eqp, li.cj)
        $log.info "(4) wait 10 s ...."
        sleep 10
        sv.eqp_opecomp(li.eqp, li.cj, :force=>true)
        $log.info "(5) wait 10 s ...."
        sleep 10
        port = nil
        eqpi = sv.eqp_info(li.eqp)
        eqpi.ports.each {|p| port = p.port if p.loaded_carrier == li.carrier}
        sv.eqp_unload(li.eqp, port, li.carrier)
        $log.info "(6) wait 10 s ...."
        sleep 10
        sv.lot_hold_release(l, nil)
        $log.info "(7) wait 60 s ...."
        sleep 60
        sv.lot_branch(l, "eA-QT-PROCESSABORT.01", :retop=>"1000.1500")
        $log.info "(8) wait 60 s ...."
        sleep 60
        sv.claim_process_lot(l, :freeport=>true, :synchronize=>mutex)
        $log.info "(9) wait 240 s ...."
        sleep 240
      end
    } if @@tc720

    @@threads << Thread.new {
      sv = SiView::MM.new($env)
      @@lots['tc730'] = l = start_lot(sv)
      if l
        sv.lot_opelocate(l, "1000.1200")
        sv.claim_process_lot(l, :freeport=>true, :synchronize=>mutex)
        @@qtstart[l] = siview_time(sv.lot_info(l).claim_time)
        sleep 120
        lc = sv.lot_split(l, 10)
        @@qtstart[lc] = @@qtstart[l]
        sleep 120
        sv.lot_hold(l, "ENG")
        sv.lot_opelocate(lc, "1000.1500")
        sleep 330
        sv.claim_process_lot(lc, :freeport=>true, :synchronize=>mutex, :proctime=>60)
        sleep 20
        sv.claim_process_lot(lc, :freeport=>true, :synchronize=>mutex)
        #$sv.lot_opelocate(lc, "9900.1000")
        #$sv.lot_bankin(lc)
      end
    } if @@tc730

    @@threads << Thread.new {
      sv = SiView::MM.new($env)
      @@lots['tc740'] = l = start_lot(sv)
      if l
        sv.lot_opelocate(l, "1000.1200")
        sv.claim_process_lot(l, :freeport=>true, :synchronize=>mutex)
        sleep 240
        sv.lot_opelocate(l, "1000.1500")
        sv.claim_process_lot(l, :freeport=>true, :synchronize=>mutex)
        sleep 60
        lc = sv.lot_split(l, 10)
        sv.lot_opelocate(lc, "1000.1500")
        sleep 60
        sv.claim_process_lot(lc, :freeport=>true, :synchronize=>mutex)
        sv.lot_merge(l, lc)
        sleep 60
        sv.claim_process_lot(l, :freeport=>true, :synchronize=>mutex)
      end
    } if @@tc740

    #process at start pd, qt expires. Lot on hold with memo criticalit = C2
    @@threads << Thread.new {
      sv = SiView::MM.new($env)
      @@lots['tc810'] = l = start_lot(sv)
      if l
        sv.lot_opelocate(l, "4000.1200")
        sv.claim_process_lot(l, :freeport=>true, :synchronize=>mutex)
        @@qtstart[l] = siview_time(sv.lot_info(l).claim_time)
        $log.info("-- tc810 (lot #{l}) sleeps for 600 seconds")
        sleep 600
      end
    } if @@tc810

    #process at start pd, qt expires. Lot on hold with memo criticalit = C4
    @@threads << Thread.new {
      sv = SiView::MM.new($env)
      @@lots['tc820'] = l = start_lot(sv)
      if l
        sv.lot_opelocate(l, "4000.1400")
        sv.claim_process_lot(l, :freeport=>true, :synchronize=>mutex)
        @@qtstart[l] = siview_time(sv.lot_info(l).claim_time)
        $log.info("-- tc820 (lot #{l}) sleeps for 600 seconds")
        sleep 600
      end
    } if @@tc820

    #process at start pd (5000.1200). lot is not on hold after qt limit. QT action is LimitOnly
    @@threads << Thread.new {
      sv = SiView::MM.new($env)
      @@lots['tc900'] = l = start_lot(sv)
      if l
        sv.lot_opelocate(l, "5000.1200")
        sv.claim_process_lot(l, :freeport=>true, :synchronize=>mutex)
        $log.info("-- tc900 (lot #{l}) sleeps for 660 seconds")
        sleep 660
      end
    } if @@tc900

    #process at start pd (5000.1200), locate to end pd. wait for qt and process at end pd. lot is not on hold after qt limit. QT action is LimitOnly
    @@threads << Thread.new {
      sv = SiView::MM.new($env)
      @@lots['tc910'] = l = start_lot(sv)
      if l
        sv.lot_opelocate(l, "5000.1200")
        sv.claim_process_lot(l, :freeport=>true, :synchronize=>mutex)
        sleep 60
        sv.lot_opelocate(l, "5000.1400")
        $log.info("-- tc900 (lot #{l}) sleeps for 660 seconds")
        sleep 660
        sv.claim_process_lot(l, :freeport=>true, :synchronize=>mutex)
        sleep 60
      end
    } if @@tc910
  end

  def test020_wait_for_threads
    # wait for all threads to complete
    @@threads.each {|t| t.join}
    sleep 60   # for qt ctrl
    $log.info "******* created following lots: #{@@lots.inspect}"
  end

  def test100_checklots
    return if @@tc100 == false
    l = @@lots['tc100']
    $log.info("checking tc100 with lot #{l}")
    assert_equal nil, checkhold(l), "no hold expected"
  end

  def test110_checklots
    return if @@tc110 == false
    l = @@lots['tc110']
    $log.info("checking tc110 with lot #{l}")
    $log.info "found lot hold with reason: #{checkhold(l)}"
    assert_equal @@Hold_Code1, checkhold(l), "hold expected"
    assert checkhold_criticality_memo(l,@@C1),"wrong criticality claim memo"
  end

  def test120_checklots
    return if @@tc120 == false
    l = @@lots['tc120']
    $log.info("checking tc120 with lot #{l}")
    $log.info "found lot hold with reason: #{checkhold(l)}"
    assert_equal @@Hold_Code1, checkhold(l),  "hold expected"
  end

  def test130_checklots
    return if @@tc130 == false
    l = @@lots['tc130']
    $log.info("checking tc130 with lot #{l}")
    $log.info "found lot hold with reason: #{checkhold(l)}"
    assert_equal @@Hold_Code1, checkhold(l),  "hold expected"
  end

  def test140_checklots
    return if @@tc140 == false
    l = @@lots['tc140']
    $log.info("checking tc140 with lot #{l}")
    $log.info "found lot hold with reason: #{checkhold(l)}"
    assert_equal checkhold(l), nil, "no hold expected"
  end

  def test150_checklots
    return if @@tc150 == false
    l = @@lots['tc150']
    $log.info("checking tc150 with lot #{l}")
    $log.info "found lot hold with reason: #{checkhold(l)}"
    assert_equal checkhold(l), nil, "no hold expected"
  end

  def test160_checklots
    return if @@tc160 == false
    l = @@lots['tc160']
    $log.info("checking tc160 with lot #{l}")
    $log.info "found lot hold with reason: #{checkhold(l)}"
    assert_equal checkhold(l), @@Hold_Code1, "hold expected"
    child_lot = $sv.lot_family(l)[-1]
    assert_equal checkhold(child_lot), @@Hold_Code1, "hold expected"
  end

  def test170_checklots
    return if @@tc170 == false
    l = @@lots['tc170']
    $log.info("checking tc170 with lot #{l}")
    $log.info "found lot hold with reason: #{checkhold(l)}"
    assert_equal checkhold(l), @@Hold_Code1, "hold expected"
    child_lot = $sv.lot_family(l)[-1]
    assert_equal checkhold(child_lot), nil, "no hold expected"
  end

  def test180_checklots
    return if @@tc180 == false
    l = @@lots['tc180']
    $log.info("checking tc180 with lot #{l}")
    $log.info "found lot hold with reason: #{checkhold(l)}"
    assert_equal checkhold(l), @@Hold_Code1, "hold expected"
    child_lot = $sv.lot_family(l)[-1]
    assert_equal checkhold(child_lot), nil, "no hold expected"
  end

  def test190_checklots
    return if @@tc190 == false
    l = @@lots['tc190']
    $log.info "checking tc190 with lot #{l}"
    $log.info "found lot hold with reason: #{checkhold(l)}"
    assert_equal checkhold(l), @@Hold_Code1, "hold expected"
    qt = calc_qt(l)
    check = true
    check = qt > 599.100 && qt < 600.400
    #assert_equal qt, 600, "QT = 600s expected"
    assert check, "QT  #{qt}s should be in range 599.500 -> 600.400"
  end

  def test191_checklots
    return if @@tc191 == false
    l = @@lots['tc191']
    $log.info "checking tc190 with lot #{l}"
    $log.info "found lot hold with reason: #{checkhold(l)}"
    assert_equal checkhold(l), @@Hold_Code1, "hold expected"
    qt = calc_qt(l)
    check = true
    check = qt > 599.100 && qt < 600.400
    #assert_equal qt, 600, "QT = 600s expected"
    assert check, "QT  #{qt}s should be in range 599.500 -> 600.400"
  end

  def test200_checklots
    return if @@tc200 == false
    l = @@lots['tc200']
    $log.info("checking tc200 with lot #{l}")
    $log.info "found lot hold with reason: #{checkhold(l)}"
    assert_equal checkhold(l), @@Hold_Code1, "hold expected"
    qt = calc_qt(l)
    check = true
    check = qt > 599.100 && qt < 600.400
    #assert_equal qt, 600, "QT = 600s expected"
    assert check, "QT  #{qt}s should be in range 599.500 -> 600.400"
  end

  def test210_checklots
    return if @@tc210 == false
    l = @@lots['tc210']
    $log.info("checking tc210 with lot #{l}")
    $log.info "found lot hold with reason: #{checkhold(l)}"
    assert_equal checkhold(l), nil, "no hold expected"
    lc = $sv.lot_family(l)[-1]
    assert_equal checkhold(lc), nil, "no hold expected for child lot"
  end

  def test230_checklots
    return if @@tc230 == false
    l = @@lots['tc230']
    $log.info("checking tc230 with lot #{l}")
    assert_equal nil, checkhold(l), "no hold expected"
  end

  def test240_checklots
    return if @@tc240 == false
    l = @@lots['tc240']
    $log.info("checking tc240 with lot #{l}")
    assert_equal nil, checkhold(l), "no hold expected"
  end

  def test300_checklots
    return if @@tc300 == false
    l = @@lots['tc300']
    $log.info("checking tc300 with lot #{l}")
    $log.info "found lot hold with reason: #{checkhold(l)}"
    assert_equal @@Hold_Code1, checkhold(l), "hold expected"
    lc = $sv.lot_family(l)[-1]
    # note that the following check depends on the config of the start PD on the ERF route. Check in SM client the config.
    # ExperimentOriginalPD	String	PD Experiments Original PD	A-QT-PD112.01			0 .
    # If no reference to the main route QT PDs then no Hold for child.
    $log.info("checking tc300 with lot #{lc}")
    assert_equal nil, checkhold(lc), "no hold expected for child lot"

    qt = calc_qt(l)
    check = true
    #check = qt > 599.100 && qt < 600.400
    #assert_equal qt, 600, "QT = 600s expected"
    assert check, "QT  #{qt}s should be in range 599.500 -> 600.400"
  end

  def test310_checklots
    return if @@tc310 == false
    l = @@lots['tc310']
    $log.info("checking tc310 with lot #{l}")
    assert_equal @@Hold_Code1, checkhold(l), "hold expected"
    qt = calc_qt(l)
    check = true
    #check = qt > 599.100 && qt < 600.400
    #assert_equal qt, 600, "QT = 600s expected"
    assert check, "QT  #{qt}s should be in range 599.500 -> 600.400"
  end

  def test311_checklots
    return if @@tc311 == false
    l = @@lots['tc311']
    lc = $sv.lot_family(l)[-1]
    $log.info("checking tc310 with lot #{l}")
    assert_equal @@Hold_Code1, checkhold(l), "hold expected"
    assert_equal @@Hold_Code1, checkhold(lc), "hold expected"
    qt = calc_qt(l)
    check = true
    check = qt > 599.100 && qt < 600.400
    #assert_equal qt, 600, "QT = 600s expected"
    assert check, "QT  #{qt}s should be in range 599.500 -> 600.400"
  end

  def test320_checklots
    return if @@tc320 == false
    l = @@lots['tc320']
    $log.info("checking tc320 with lot #{l}")
    assert_equal @@Hold_Code1, checkhold(l), "hold expected"
    qt = calc_qt(l)
    check = true
    check = qt > 595.0 && qt < 605.0
    #assert_equal qt, 600, "QT = 600s expected"
    assert check, "QT  #{qt}s should be in range 599.500 -> 600.400"
  end

  def test330_checklots
    return if @@tc330 == false
    l = @@lots['tc330']
    $log.info("checking tc330 with lot #{l}")
    assert_equal checkhold(l), nil, "no hold expected"
  end

  def test340_checklots
    return if @@tc340 == false
    l = @@lots['tc340']
    $log.info("checking tc340 with lot #{l}")
    assert_equal @@Hold_Code1, checkhold(l), "hold expected"
    lc = $sv.lot_family(l)[-1]
    assert_equal @@Hold_Code1, checkhold(lc), "hold expected for child lot"
    $log.warn("QT for lot #{l} was #{calc_qt(l)} seconds. Should be 600 seconds.")
    $log.warn("QT for lot #{lc} was #{calc_qt(lc)} seconds. Should be 600 seconds.")

    qt = calc_qt(l)
    check = true
    check = qt > 595.1 && qt < 605.0
    assert check, "QT  #{qt}s should be in range 599.500 -> 600.400 for lot #{l}"

    # assert_equal 600, calc_qt(l), "QT = 600s expected for lot #{l}"
    qt = calc_qt(lc)
    check = true
    check = 595.100 < qt && qt < 605.400
    assert check, "QT  #{qt}s should be in range 599.500 -> 600.400 for lot #{lc}"
  end

  def test341_checklots
    return if @@tc341 == false
    l = @@lots['tc341']
    lc = $sv.lot_family(l)[-1]
    $log.info("checking tc341 with lot #{l}")
    $log.info("checkhold for lot #{l} returns #{checkhold(l)}, qt was #{calc_qt(l)}")
    $log.info("checkhold for lot #{lc} returns #{checkhold(lc)}, qt was #{calc_qt(lc)}")
    assert_equal @@Hold_Code1, checkhold(l), "hold expected"
    assert_equal @@Hold_Code1, checkhold(lc), "hold expected for child lot"
    # assert_equal 600, calc_qt(l), "QT = 600s expected for lot #{l}"
    # assert_equal 600, calc_qt(lc), "QT = 600s expected for lot #{lc}"
    qt = calc_qt(l)
    check = true
    check = qt > 599.100 && qt < 600.400
    #assert_equal qt, 600, "QT = 600s expected"
    assert check, "QT  #{qt}s should be in range 599.500 -> 600.400 for lot #{l}"

    # assert_equal 600, calc_qt(l), "QT = 600s expected for lot #{l}"
    qt = calc_qt(lc)
    check = true
    check = qt > 599.100 && qt < 600.400
    #assert_equal qt, 600, "QT = 600s expected"
    assert check, "QT  #{qt}s should be in range 599.500 -> 600.400 for lot #{lc}"
    # assert_equal 600, calc_qt(lc), "QT = 600s expected for lot #{lc}"

  end

  def test350_checklots
    return if @@tc350 == false
    l = @@lots['tc350']
    $log.info("checking tc350 with lot #{l}")
    assert_equal nil, checkhold(l), "no hold expected"
  end

  def test360_checklots
    return if @@tc360 == false
    l = @@lots['tc360']
    $log.info("checking tc360 with lot #{l}")
    $log.info "found lot hold with reason: #{checkhold(l)}"
    assert_equal @@Hold_Code1, checkhold(l),  "hold expected"
  end

  def test370_checklots
    return if @@tc370 == false
    l = @@lots['tc370']
    lc = $sv.lot_family(l)[-1]
    $log.info("checking tc370 with lot #{l} and child #{lc}")
    $log.info "found lot hold with reason: #{checkhold(l)}"
    assert_equal nil, checkhold(l),  "no hold expected for #{l}. This test case is Not required due to discussion with RTD resposibles. 11/12/2013 adarwais & Meinhard Schulze (RTD)"
    assert_equal nil, checkhold(lc),  "no hold expected for #{lc}. This test case is Not required due to discussion with RTD resposibles. 11/12/2013 adarwais & Meinhard Schulze (RTD)"
  end

  def test380_checklots
    return if @@tc380 == false
    l = @@lots['tc380']
    $log.info("checking tc380 with lot #{l}")
    assert_equal nil, checkhold(l),  "hold expected"
  end

  def test390_checklots
    return if @@tc390 == false
    l = @@lots['tc390']
    $log.info("checking tc390 with lot #{l}")
    assert_equal @@Hold_Code1, checkhold(l),  "hold expected"
  end

  def test391_checklots
    return if @@tc391 == false
    l = @@lots['tc391']
    $log.info("checking tc391 with lot #{l}")
    assert_equal nil, checkhold(l),  "hold expected"
  end

  def test392_checklots
    return if @@tc392 == false
    l = @@lots['tc392']
    $log.info("checking tc392 with lot #{l}")
    assert_equal @@Hold_Code1, checkhold(l),  "hold expected"
    qt = calc_qt(l)
    check = true
    check = qt > 590.100 && qt < 610.400
    assert check, "QT  #{qt}s should be in range 599.500 -> 600.400 for lot #{l}"
  end

  def test393_checklots
    return if @@tc393 == false
    l = @@lots['tc393']
    lc = $sv.lot_family(l).last
    $log.info("checking tc393 with lot #{l}")
    assert_equal @@Hold_Code1, checkhold(l),  "hold expected"
    assert_equal @@Hold_Code1, checkhold(lc),  "hold expected"
    qt = calc_qt(l)
    check = true
    check = qt > 590.100 && qt < 610.400
    assert check, "QT  #{qt}s should be in range 599.500 -> 600.400 for lot #{l}"
    # qt = calc_qt(lc)
    # check = true
    # check = qt > 590.100 && qt < 610.400
    # assert check, "QT  #{qt}s should be in range 599.500 -> 600.400 for lot #{lc}"
  end

  def test400_checklots
    return if @@tc400 == false
    l = @@lots['tc400']
    $log.info("checking tc400 with lot #{l}")
    assert_equal @@Hold_Code1, checkhold(l),  "hold expected"
  end

  def test420_checklots
    return if @@tc420 == false
    l = @@lots['tc420']
    $log.info("checking tc420 with lot #{l}")
    assert_equal @@Hold_Code1, checkhold(l), "hold expected"
  end

  def test430_checklots
    return if @@tc430 == false
    l = @@lots['tc430']
    $log.info("checking tc430 with lot #{l}")
    assert_equal nil, checkhold(l), "no hold expected"
  end

  def test440_checklots
    return if @@tc440 == false
    l = @@lots['tc440']
    lc = $sv.lot_family(l)[-1]
    $log.info("checking tc440 with lot #{l}")
    assert_equal @@Hold_Code1, checkhold(l), "no hold expected"
    assert_equal @@Hold_Code1, checkhold(lc), "no hold expected"
  end

  def test450_checklots
    return if @@tc450 == false
    l = @@lots['tc450']
    $log.info("checking tc450 with lot #{l}")
    assert_equal @@Hold_Code1, checkhold(l), "hold expected"
  end

  def test500_checklots
    return if @@tc500 == false
    l = @@lots['tc500']
    $log.info("checking tc500 with lot #{l}")
    assert_equal @@Hold_Code1, checkhold(l), "hold expected"
    lc = $sv.lot_family(l)[-1]
    assert_equal @@Hold_Code1, checkhold(lc), "hold expected for child lot"
    # assert_equal 600, calc_qt(l), "QT = 600s expected for lot #{l}"
    # assert_equal 600, calc_qt(lc), "QT = 600s expected for lot #{l}"

    qt = calc_qt(l)
    check = true
    check = qt > 599.100 && qt < 600.400
    #assert_equal qt, 600, "QT = 600s expected"
    assert check, "QT  #{qt}s should be in range 599.500 -> 600.400 for lot #{l}"

    # assert_equal 600, calc_qt(l), "QT = 600s expected for lot #{l}"
    qt = calc_qt(lc)
    check = true
    check = qt > 599.100 && qt < 600.400
    #assert_equal qt, 600, "QT = 600s expected"
    assert check, "QT  #{qt}s should be in range 599.500 -> 600.400 for lot #{lc}"
    # assert_equal 600, calc_qt(lc), "QT = 600s expected for lot #{lc}"
  end

  def test501_checklots
    return if @@tc501 == false
    l = @@lots['tc501']
    $log.info("checking tc501 with lot #{l}")
    assert_equal @@Hold_Code1, checkhold(l), "hold expected"

    qt = calc_qt(l)
    check = true
    check = qt > 599.100 && qt < 600.400
    #assert_equal qt, 600, "QT = 600s expected"
    assert check, "QT  #{qt}s should be in range 599.500 -> 600.400 for lot #{l}"
  end

  def test510_checklots
    return if @@tc510 == false
    l = @@lots['tc510']
    $log.info("checking tc510 with lot #{l}")
    assert_equal @@Hold_Code1, checkhold(l), "hold expected"
    lc = $sv.lot_family(l)[-1]
    assert_equal @@Hold_Code1, checkhold(lc), "hold expected for child lot"
    # assert_equal 600, calc_qt(l), "QT = 600s expected for lot #{l}"
    # assert_equal 600, calc_qt(lc), "QT = 600s expected for lot #{l}"

    qt = calc_qt(l)
    check = true
    check = qt > 599.100 && qt < 600.400
    #assert_equal qt, 600, "QT = 600s expected"
    assert check, "QT  #{qt}s should be in range 599.500 -> 600.400 for lot #{l}"

    # assert_equal 600, calc_qt(l), "QT = 600s expected for lot #{l}"
    qt = calc_qt(lc)
    check = true
    check = qt > 599.100 && qt < 600.400
    #assert_equal qt, 600, "QT = 600s expected"
    assert check, "QT  #{qt}s should be in range 599.500 -> 600.400 for lot #{lc}"
    # assert_equal 600, calc_qt(lc), "QT = 600s expected for lot #{lc}"

  end

  def test520_checklots
    return if @@tc520 == false
    l = @@lots['tc520']
    $log.info("checking tc520 with lot #{l}")
    assert_equal @@Hold_Code1, checkhold(l), "hold expected"
    lc = $sv.lot_family(l)[-1]
    assert_equal nil, checkhold(lc), "no hold expected for child lot"
    # assert_equal 600, calc_qt(l), "QT = 600s expected for lot #{l}"

    qt = calc_qt(l)
    check = true
    check = qt > 599.100 && qt < 600.400
    #assert_equal qt, 600, "QT = 600s expected"
    assert check, "QT  #{qt}s should be in range 599.500 -> 600.400 for lot #{l}"

  end

  def test530_checklots
    return if @@tc530 == false
    l = @@lots['tc530']
    $log.info("checking tc530 with lot #{l}")
    assert_equal @@Hold_Code1, checkhold(l), "hold expected"
    lc = $sv.lot_family(l)[-1]
    assert_equal @@Hold_Code1, checkhold(lc), "hold expected for child lot"
    $log.warn("QT for lot #{l} was #{calc_qt(l)} seconds. Should be 600 seconds.")
    $log.warn("QT for lot #{lc} was #{calc_qt(lc)} seconds. Should be 600 seconds.")
    #assert_equal 600, calc_qt(l), "QT = 600s expected for lot #{l}"
    #assert_equal 600, calc_qt(lc), "QT = 600s expected for lot #{l}"
  end

  def test540_checklots
    return if @@tc540 == false
    l = @@lots['tc540']
    $log.info("checking tc540 with lot #{l}")
    assert_equal checkhold(l), nil, "no hold expected"
  end

  def test550_checklots
    return if @@tc550 == false
    l = @@lots['tc550']
    $log.info("checking tc550 with lot #{l}")
    assert_equal checkhold(l), nil, "no hold expected"
  end

  def test560_checklots
    return if @@tc560 == false
    l = @@lots['tc560']
    $log.info("checking tc560 with lot #{l}")
    assert_equal checkhold(l), nil, "no hold expected"
  end

  def test570_checklots
    return if @@tc570 == false
    l = @@lots['tc570']
    $log.info("checking tc570 with lot #{l}")
    assert_equal checkhold(l), nil, "no hold expected"
  end

  def test580_checklots
    return if @@tc580 == false
    l = @@lots['tc580']
    $log.info("checking tc580 with lot #{l}")
    $log.info("checking tc580: 1st hold  #{@@holds[l]} - should be nil")
    $log.info("checking tc580: 1st hold #{checkhold(l)} - should be QTHL")
    assert_equal nil, @@holds[l], "no hold in RW area expected"
    assert_equal @@Hold_Code1, checkhold(l), "hold expected"
    $log.warn("QT for lot #{l} was #{calc_qt(l)} seconds. Should be 600 seconds.")
    #assert_equal 600, calc_qt(l), "QT = 600s expected for lot #{l}"
  end

  def test581_checklots
    return if @@tc581 == false
    l = @@lots['tc581']
    $log.info("checking tc581 with lot #{l}")
    $log.info("checking tc581: 1st hold  #{@@holds[l]} - should be nil")
    $log.info("checking tc581: 1st hold #{checkhold(l)} - should be QTHL")
    assert_equal nil, @@holds[l], "no hold in RW area expected"
    assert_equal @@Hold_Code1, checkhold(l), "hold expected"
    $log.warn("QT for lot #{l} was #{calc_qt(l)} seconds. Should be 600 seconds.")
    #assert_equal 600, calc_qt(l), "QT = 600s expected for lot #{l}"
  end

  def test590_checklots
    return if @@tc590 == false
    l = @@lots['tc590']
    $log.info("checking tc590 with lot #{l}")
    assert_equal @@Hold_Code1, checkhold(l), "hold expected"
    # assert_equal 600, calc_qt(l), "QT = 600s expected"

    qt = calc_qt(l)
    check = true
    check = qt > 599.100 && qt < 600.100
    #assert_equal qt, 600, "QT = 600s expected"
    assert check, "QT  #{qt}s should be in range 599.500 -> 600.400 for lot #{l}"

  end

  def test600_checklots
    return if @@tc600 == false
    l = @@lots['tc600']
    $log.info("checking tc600 with lot #{l}")
    assert_equal @@Hold_Code1, checkhold(l), "hold expected"
    lc = $sv.lot_family(l)[-1]
    assert_equal @@Hold_Code1, checkhold(lc), "hold expected for child lot"
    $log.warn("QT for lot #{l} was #{calc_qt(l)} seconds. Should be 600 seconds.")
    $log.warn("QT for lot #{lc} was #{calc_qt(lc)} seconds. Should be 600 seconds.")
    # assert_equal 600, calc_qt(l), "QT = 600s expected for lot #{l}"
    # assert_equal 600, calc_qt(lc), "QT = 600s expected for lot #{l}"

    qt = calc_qt(l)
    check = true
    check = qt > 599.100 && qt < 600.400
    #assert_equal qt, 600, "QT = 600s expected"
    assert check, "QT  #{qt}s should be in range 599.500 -> 600.400 for lot #{l}"
    qt = calc_qt(lc)
    check = true
    check = qt > 599.100 && qt < 600.400
    #assert_equal qt, 600, "QT = 600s expected"
    assert check, "QT  #{qt}s should be in range 599.500 -> 600.400 for lot #{lc}"

  end

  def test700_checklots
    return if @@tc700 == false
    l = @@lots['tc700']
    $log.info("checking tc700 with lot #{l}")
    res = $sv.lot_futurehold_list(l)
    r = res[0]
    $log.info("res 700 #{res.inspect}")
    assert res.size > 0, "not future hold found for lot"
    assert_equal @@Hold_Code1, r['reason'], "reason code not correct for lot."
    assert_equal "2000.1700", r['rsp_op'], "responsible operation should be 2000.1500"
  end

  def test710_checklots
    return if @@tc710 == false
    l = @@lots['tc710']
    $log.info("checking tc710 with lot #{l}")
    res = @@holds['tc710']
    assert_equal @@Hold_Code1, res, "future hold expected for lot #{l}."
  end

  def test720_checklots
    return if @@tc720 == false
    l = @@lots['tc720']
    $log.info("checking tc720 with lot #{l}")
    $log.info("... disabled")
    #assert_equal @@Hold_Code1, checkhold(l), "hold expected for lot #{l}"
  end

  def test730_checklots
    return if @@tc730 == false
    l = @@lots['tc730']
    $log.info("checking tc730 with lot #{l}")
    assert_equal @@Hold_Code1, checkhold(l), "hold expected for lot #{l}"
  end

  def test740_checklots
    return if @@tc740 == false
    l = @@lots['tc740']
    $log.info("checking tc740 with lot #{l}")
    $log.info("check qt history for parent and child, details in test plan")
  end

  def test810_checklots
    return if @@tc810 == false
    l = @@lots['tc810']
    $log.info("checking tc810 with lot #{l}")
    $log.info "found lot hold with reason: #{checkhold(l)}"
    assert_equal @@Hold_Code1, checkhold(l),  "hold expected"
    assert checkhold_criticality_memo(l,@@C2), "wrong cirticality claim memo"
  end

  def test820_checklots
    return if @@tc820 == false
    l = @@lots['tc820']
    $log.info("checking tc820 with lot #{l}")
    $log.info "found lot hold with reason: #{checkhold(l)}"
    assert_equal checkhold(l), @@Hold_Code1, "hold expected"
    assert checkhold_criticality_memo(l,@@C4), "wrong cirticality claim memo"
  end

  def test900_checklots
    return if @@tc900 == false
    l = @@lots['tc900']
    $log.info("checking tc900 with lot #{l}")
    assert_equal checkhold(l), nil, "no hold expected for tc900"
  end

  def test910_checklots
    return if @@tc910 == false
    l = @@lots['tc910']
    $log.info("checking tc900 with lot #{l}")
    assert_equal checkhold(l), nil, "no hold expected for tc910"
  end

  def test1999_attention
	puts "\n Do not forget to run the extended test program Test_jCAP_QT_EXT for the semi automatic test caes, which requires setup changes during testing."
  end

  def test999_cleanup
    return unless @@tc999
    puts "\ndelete all lots: #{@@lots.inspect} (j/n)?"
    answer = STDIN.gets
    if "jy".index(answer.chomp.downcase)
      @@lots.keys.each{|tc| $sv.delete_lot_family(@@lots[tc], :sleeptime=>2)}
    end
  end

  def get_child_lot(lot)
    _clot = []
    _clot = $sv.lot_family(lot)
    _clot.delete(lot)
    return _clot[0]
  end

  def checkhold(lot)
    #$log.info "method checkhold sleep 10s"
    #sleep 10
    ret=$sv.lot_hold_list(lot, :reason=>@@Hold_Code1)
    if ret.size == 0
        return nil
    else
        r = ret[0]
        #refute_equal "", r['memo'], "hold memo for QTHL hold is empty"   # WatchDog doesn�t provide a memo
        return r['reason']
    end
  end

  def checkfuturehold(lot)
    #$log.info "method checkfuturehold sleep 10s"
    #sleep 10
    ret=$sv.lot_futurehold_list(lot, :reason=>@@Hold_Code1)
    if ret.size == 0
        return nil
    else
        r = ret[0]
        refute_equal "", r['memo'], "hold memo for QTHL hold is empty"
        return r['reason']
    end
  end
  def checkhold_criticality_memo(lot, criticality, params={})
    # e.g.: @@claim_memo_criticality = "time of %s min exceeded at %s (hold action: %s, target operation %s, DLIS comment: %s), Criticality =%s"
    #memo = @@claim_memo_criticality %[@@QT_time,@@carrier0,@@carrier5,@@requestor,@@sorter,@@pg]
    #$log.info "method checkhold_criticality_memo sleep 30s"
    #sleep 30
	  hold = (params[:hold] or "")

	if hold == "future"
		ret=$sv.lot_futurehold_list(lot, :reason=>@@Hold_Code1)
	else
		ret=$sv.lot_hold_list(lot, :reason=>@@Hold_Code1)
	end

    if ret.size == 0
        return false
    else
        r = ret[0]
        end_memo = "criticality = #{criticality}"
        refute_equal "", r['reason'], "hold memo for QTHL hold is empty"
        assert r['memo'].start_with?("Queue time of #{@@QT_time} min"), "wrong start of claim memo for QTHL hold"
        assert r['memo'].end_with?(end_memo), "wrong end of claim memo for QTHL hold"
        return true
    end
  end

  def start_lot(sv, product="A-QT-PRODUCT.01", route="A-QT-MAIN.01")
    product="A-QT-PRODUCT-WD.01"; route="A-QT-MAINPD-WD.01"  # WD!
    lot = nil
    synchronize do
      lot = sv.new_lot_release(:product=>product, :route=>route, :stb=>true, :sublottype=>"PO", :carrier=>"QT%")
    end
    sleep 60
    sv.lot_hold_release(lot, nil)
    li = sv.lot_info(lot)
    sv.carrier_xfer_status_change(li.carrier, "EO", li.opEqps[0]) if li.xfer_status != "EO"
    return lot
  end

  def calc_qt(lot)
    return @@qthold[lot] - @@qtstart[lot]
  end
end
