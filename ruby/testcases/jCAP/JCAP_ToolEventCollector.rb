=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: Steffen Steidten, 2012-09-20

Version: 18.05

History:
  2015-05-26 adarwais, cleaned up and verified test cases with R15
  2017-02-24 sfrieske, more cleanup
  2018-03-14 sfrieske, changed usage of wafer_history_report during claim_process_lot
  2019-02-27 sfrieske, use eqp mode Off-Line-1
  2020-05-07 sfrieske, changed wafer_history_report call
  2021-04-23 aschmid3, minor cleanup in test11
  2021-04-27 aschmid3, added testmanual91 and testmanual92 (MQ failover)
  2021-08-27 sfrieske, require uniquestring instead of misc
=end

require 'SiViewTestCase'
require 'jcap/toolevents'
require 'util/uniquestring'
require 'util/waitfor'


class JCAP_ToolEventCollector < SiViewTestCase
  @@opNo = '1000.700'
  @@eqp = 'UTC001'

  @@jobdelay = 30
  @@testlots = []


  def self.startup
    super
    @@wh = ToolEvents::WaferHistory.new($env, sv: @@sv)
    @@ph = ToolEvents::Parameter.new($env, eqp: @@eqp, mq: @@wh.mq)
    @@al = ToolEvents::Alarm.new($env, mq: @@wh.mq)
    @@mds = ToolEvents::MDS.new($env)
  end

  def self.after_all_passed
    @@testlots.each {|lot| @@sv.delete_lot_family(lot)}
  end


  def test11_waferevents
    assert lot = @@svtest.new_lot, 'error creating test lot'
    @@testlots << lot
    assert_equal 0, @@sv.lot_opelocate(lot, @@opNo), 'error locating lot'
    assert_equal 0, @@sv.eqp_cleanup(@@eqp, mode: 'Off-Line-1')
    assert cj = @@sv.claim_process_lot(lot, eqp: @@eqp, wafer_history: @@wh) {|cj, cjinfo, eqpi, port|
      li = @@sv.lot_info(lot, wafers: true)
      @@wh.wafer_history_report(cj, li, @@eqp, port)
    }
    $log.info "waiting #{@@jobdelay} s for the jCAP job"; sleep @@jobdelay
    li = @@sv.lot_info(lot, wafers: true)
    $log.info "verifying WAFERCOMPLETED events for cj #{cj.inspect}"
    li.wafers.each {|w|
      assert_equal 1, @@mds.history_view_wafer(event: 'WAFERCOMPLETED', cj: cj, wafer: w.wafer).size, "no event for wafer #{w.wafer}"
    }
    $log.info "verifying WAFERCHAMBER events for cj #{cj.inspect}"
    li.wafers.each {|w|
      assert_equal 1, @@mds.history_view_wafer(event: 'WAFERCHAMBER', cj: cj, wafer: w.wafer).size, "no event for wafer #{w.wafer}"
    }
  end

  def test21_tool_parameter
    parameter = 'QA_PARAM01'
    value = 10.times.inject(10.0) {|v, i|
      v += 1.0
      txtime = Time.now
      assert @@ph.submit(eqp: @@eqp, parameter: {parameter=>[v]}, reading_timestamp: Time.now)
      sleep 1
      v
    }
    assert wait_for(timeout: @@jobdelay) {
      @@mds.tool_para(eqp: @@eqp, name: parameter, value: value).size == 1
    }, "failed to get parameter #{parameter}"
  end

  def test22_tool_parameter_desc_time
    parameter = 'QA_PARAM02'
    txtime = Time.now
    10.times.inject(10.0) {|v, i|
      assert @@ph.submit(eqp: @@eqp, parameter: {parameter=>[v]}, reading_timestamp: txtime - i)
      sleep 1
      v + 1.0
    }
    assert wait_for(timeout: @@jobdelay) {
      @@mds.tool_para(eqp: @@eqp, name: parameter, value: 10.0).size == 1
    }, "failed to get parameter #{parameter}"
    sleep 10  # wait some extra time and check again
    assert_equal 1, @@mds.tool_para(eqp: @@eqp, name: parameter, value: 10.0).size
  end

  def test23_parameters_multiple
    parameter = 'QA_PARAM04'
    @@ph.parameters = []
    @@ph.add_parameter(parameter, [1])
    @@ph.add_readings([2.2], rading_valid: false)
    v = unique_string
    @@ph.add_readings([v])
    assert @@ph.submit
    $log.info "waiting #{@@jobdelay} s for the jCAP job"; sleep @@jobdelay
    # last value is valid
    assert_equal v, @@mds.tool_para(eqp: @@ph.eqp, name: parameter, value: v).first[3], 'wrong value'
  end

  def test31_tool_alarm
    text = "QA Test #{Time.now.utc.strftime('%F_%T')}"
    assert @@al.submit(eqp: @@eqp, text: text)
    assert wait_for(timeout: @@jobdelay) {
      @@mds.history_view_eqp(eqp: @@eqp, text: text).size == 1
    }, 'failed to get alarm'
  end

  def test32_tool_alarm_repeated
    count = 10
    count.times.each {|i|
      text = "QA Test #{i} #{Time.now.utc.strftime('%F_%T')}"
      assert @@al.submit(timestamp: Time.now, eqp: @@eqp, text: text)
      $log.info "test #{i+1}/#{count}"
      assert wait_for(timeout: @@jobdelay) {@@mds.history_view_eqp(eqp: @@eqp, text: text).size == 1}
    }
  end

  def testmanual81_tool_alarm_stress # on demand only
    count = 2000
    count.times.each {|i|
      @@al.submit(timestamp: Time.now, eqp: @@eqp, text: "QA stress test, msg no #{i}")
      sleep 0.5
      $log.info "sent msg #{i+1}/#{count}" if i % 50 == 0
    }
  end

  # MQ failover tests

  def testmanual91_tool_alarm_w_MQHA_via_reconnect
    count = 3  # put to (minimum) 50 for MQ failover test
    $n = 0
    # Set alarm
    while $n < count
      $n += 1
      now = Time.now
      text = "QA Test #{$n}/#{count} for MQHA via reconnect #{now.utc.strftime('%F_%T')}"
      $log.info "MQHA test run #{$n}"
      begin
        assert @@wh.mq.reconnect
        assert @@al.submit(timestamp: now, eqp: @@eqp, text: text)
      rescue Exception => err
        $log.warn "Assertion error (rescued): #{err}"
      end
      sleep 10  # waiting time till next run
    end
    #
    puts "Check messages manually (via FabView or testconsole)" # console: @@mds.history_view_eqp(eqp: @@eqp, text: text).size == 1)
  end

  def testmanual92_tool_alarm_w_MQHA_via_new_mqenv
    count = 3  # put to (minimum) 50 for MQ failover test
    $n = 0
    # Set alarm
    while $n < count
      $n += 1
      now = Time.now
      text = "QA Test #{$n}/#{count} for MQHA via new MQ env #{now.utc.strftime('%F_%T')}"
      $log.info "MQHA test run #{$n}"
      begin
        al = ToolEvents::Alarm.new($env)
        assert al.submit(timestamp: now, eqp: @@eqp, text: text)
      rescue Exception => err
        $log.warn "Assertion error (rescued): #{err}"
      end
      sleep 10  # waiting time till next run
    end
    #
    puts "Check messages manually (via FabView or testconsole)" # console: @@mds.history_view_eqp(eqp: @@eqp, text: text).size == 1)
  end

end
