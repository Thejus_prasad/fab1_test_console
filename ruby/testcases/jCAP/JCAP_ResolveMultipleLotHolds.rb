=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: sfrieske, 2017-03-21

Version: 19.01

History:
  2019-01-31 sfrieske, minor cleanup
  2021-08-27 sfrieske, require waitfor instead of misc
=end

require 'util/waitfor'
require 'SiViewTestCase'


class JCAP_ResolveMultipleLotHolds < SiViewTestCase
  @@hold1 = 'XSPC-P'
  @@hold_stay = 'X-DC'

  @@job_interval = 330


  def self.after_all_passed
    @@testlots.each {|lot| @@sv.delete_lot_family(lot)}
  end


  def test00_setup
    $setup_ok = false
    #
    assert @@lot = @@svtest.new_lot, 'error creating test lot'
    @@testlots = [@@lot]
    #
    $setup_ok = true
  end

  def test11_happy
    lot = @@lot
    assert_equal 0, @@sv.lot_cleanup(lot)
    # set both holds and verify hold1 is removed and hold_stay stays
    assert_equal 0, @@sv.lot_hold(lot, @@hold_stay)
    assert_equal 0, @@sv.lot_hold(lot, @@hold1)
    $log.info "waiting up to #{@@job_interval} s for the job to run"
    assert wait_for(timeout: @@job_interval) {@@sv.lot_hold_list(lot, reason: @@hold1).empty?}, 'hold not released'
    refute_empty @@sv.lot_hold_list(lot, reason: @@hold_stay)
  end

  def test12_hold1_only
    lot = @@lot
    assert_equal 0, @@sv.lot_cleanup(lot)
    # set hold1 only and ensure it stays
    assert_equal 0, @@sv.lot_hold(lot, @@hold1)
    $log.info "waiting #{@@job_interval} s for the job to run"; sleep @@job_interval
    refute_empty @@sv.lot_hold_list(lot, reason: @@hold1)
  end

  def test13_multiple_holds
    lot = @@lot
    assert_equal 0, @@sv.lot_cleanup(lot)
    # set 2 XSPC hods (e.g. with different users) and the XDC hold and verify hold1 is removed and hold_stay stays
    assert_equal 0, @@sv.lot_hold(lot, @@hold_stay)
    assert_equal 0, @@sv.lot_hold(lot, @@hold1)
    assert_equal 0, @@sv.lot_hold(lot, @@hold1, user: 'NOX-UNITTEST')
    $log.info "waiting up to #{@@job_interval} s for the job to run"
    res = wait_for(timeout: @@job_interval) {@@sv.lot_hold_list(lot, reason: @@hold1).empty?}
    if !res
      $log.warn "not all #{@@hold1} holds released after 1st run, waiting for 2nd run"
      assert wait_for(timeout: @@job_interval) {@@sv.lot_hold_list(lot, reason: @@hold1).empty?}, 'hold not released'
    end
    refute_empty @@sv.lot_hold_list(lot, reason: @@hold_stay)
  end

  def testman91_perf  # on demand only
    # prepare many test lots, look at the log file for analysis
    assert lots = @@svtest.new_lots(10), 'error creating test lots'
    @@testlots += lots
    lcs = lots.collect {|lot|
      assert_equal 0, @@sv.lot_hold(lot, @@hold_stay)
      assert_equal 0, @@sv.lot_hold(lot, @@hold1)
      24.times.collect {@@sv.lot_split(lot, 1, onhold: true)}
    }.flatten
    assert_equal lots.size * 24, lcs.size
  end

end
