=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: Steffen Steidten, 2012-07-23

Version: 20.11.2

History:
  2016-08-29 sfrieske, small maintenance due to changes in erpmes
  2017-02-17 sfrieske, moved tests for ext_lot_info from Test_jCAP_GlobalCoreSiView here
  2018-03-12 sfrieske, removed use of lot_tkey_prepare which revealed a hidden setup issue in test22 ('S')
  2019-08-06 sfrieske, wait 10 s after lot script parameter changes for MDS sync
  2020-06-17 sfrieske, added test03 for ext_lotfamily_info LOT_START_TIME
  2021-01-20 sfrieske, minor cleanup, use QA defaults where possible
=end

require 'SiViewTestCase'
require 'jcap/turnkey'


# Test Fab Turnkey B2B interface
class JCAP_Turnkey < SiViewTestCase
  @@sv_defaults = {nwafers: 7, erpitem: '*SHELBY21AE-M00-JB4',  customer: "qgt"}
  @@product_ship = 'TKEY01.A0'
  @@route_ship = 'C02-TKEY.01'  # for test2x, must have a configured PD like LOTSHIPDESIG
  @@tkbump = 'CT'
  @@tksort = 'ST'
  @@holdcode = 'PPD'
  @@holdcode2 = 'PCD'
  @@scrapcode = 'CCC'
  @@repeats = 20  # for ext_lotinfo stress test
  @@mds_delay = 15
  @@gid_customer = 'qaeric'                 # a customer with UDATA GID_SOURCE 'LOT_LABEL'

  @@testlots = []


  def self.after_all_passed
   @@testlots.each {|lot| @@sv.wafer_sort(lot, "")}

  #  @@testlots.each {|lot| @@sv.delete_lot_family(lot)}
  end


  def test00_setup
    $setup_ok = false
    
    assert @@b2b = Turnkey::B2B.new($env, sv: @@sv)
    assert @@b2b.get_version, 'no MQ communication with Turnkey B2B'
    
    uparams = {'TurnkeyType'=>'L', 'TurnkeySubconIDBump'=>@@tkbump}
	assert lot = @@svtest.new_lot(user_parameters: uparams), 'error creating test lot'
    @@testlots << lot
    assert_equal 0, @@sv.lot_gatepass(lot)  # lot must have a history
    
    $setup_ok = true
  end

  # special perf tests, moved from GlobalCoreSiViewAdapter here, optional

  def test01_notex
    assert_nil @@b2b.ext_lot_info('NOTEX.QA')
    assert res = @@b2b.mq.service_response
    assert res = res[:getExtLotInfoResponse][:return][:reason][nil], 'wrong response message format'
    assert res.include?('MesOperationException'), 'wrong setup?'
  end

  def test02_extlotinfo_stress
    lot = @@testlots.first
    opNo1 = @@sv.lot_info(lot).opNo
    assert_equal 0, @@sv.lot_opelocate(lot, 1)
    opNo2 = @@sv.lot_info(lot).opNo
    goods = 0
    failed = false
    running = true
    tlocate = Thread.new {
      Thread.current['name'] = 'opelocate'
      while running
        [opNo1, opNo2].each {|opNo| @@sv.lot_opelocate(lot, opNo)}
      end
    }
    @@repeats.times {|i|
      failed = @@b2b.ext_lot_info(lot).nil?
      (running = false; break) if failed
      goods += 1
    }
    running = false
    tlocate.join(15)
    tlocate.kill
    $log.info "failed: #{failed}, goods: #{goods}"
    refute failed
  end

  def test03_extlotfamilyinfo
    lot = @@testlots.first
    assert lc = @@sv.lot_split(lot, 2), 'error splitting lot'
    assert lc2 = @@sv.lot_split(lot, 2), 'error splitting lot'
    assert_equal 0, @@sv.wafer_sort(lc2, ''), 'wafer sort error'
    $log.info "waiting #{@@mds_delay} s for the MDS to pick up the new child lot incl. lot script params"; sleep @@mds_delay
    #
    # get 1st child lot's attributes and compare with SiView history
    assert eli = @@b2b.ext_lotfamily_info(lc), 'wrong response message'
    assert_equal 3, eli.size, 'wrong ext_lotfamily_info data'
    assert a1 = eli[1][:additionalAttributes].find {|e| e[:name] == 'LOT_START_TIME'}, "no LOT_START_TIME"
    assert oph = @@sv.lot_operation_history(lot, first: true, category: 'STB').first, 'no op history'
    assert (@@sv.siview_time(oph.timestamp) - @@sv.siview_time(a1[:strValue])).abs < 2, 'wrong timestamp?'
    # compare child and parent timestamps
    assert a0 = eli[0][:additionalAttributes].find {|e| e[:name] == 'LOT_START_TIME'}, "no LOT_START_TIME"
    assert_equal a0[:strValue], a1[:strValue], 'LOT_START_TIMEs of child and parent do not match'
    #
    # get 2nd child lot's attributes and verify last carrier (i.e. the parent lot's carrier) is reported, MSR 1714963
    assert eli = @@b2b.ext_lotfamily_info(lc2), 'wrong response message'
    assert r = eli.find {|e| e[:lotId] == lc2}, 'child lot record not found'
    assert_equal @@sv.lot_info(lot).carrier, r[:carrierId], 'wrong carrier'
  end

  def test09_update_extlotfamily
    # for Fab8 only, but seems to work in Fab1
    uparams = {'TurnkeyType'=>'J', 'TurnkeySubconIDBump'=>@@tkbump, 'TurnkeySubconIDSort'=>@@tksort}
    assert lot = @@svtest.new_lot(user_parameters: uparams), 'error creating test lot'
    @@testlots << lot
    $log.info "waiting #{@@mds_delay} s for the MDS to pick up the lot script params"; sleep @@mds_delay
    refute_nil @@b2b.update_ext_lot_info(lot, 'TurnkeySubconIDBump'=>'X1', 'TurnkeySubconIDSort'=>'X2', sendattribs: true)
    assert_equal 'X1', @@sv.user_parameter('Lot', lot, name: 'TurnkeySubconIDBump').value
    assert_equal 'X2', @@sv.user_parameter('Lot', lot, name: 'TurnkeySubconIDSort').value
    refute_nil @@b2b.update_ext_lot_info(lot, 'TurnkeySubconIDBump'=>'GG', 'TurnkeySubconIDSort'=>'GG')
    assert_equal 'GG', @@sv.user_parameter('Lot', lot, name: 'TurnkeySubconIDBump').value
    assert_equal 'GG', @@sv.user_parameter('Lot', lot, name: 'TurnkeySubconIDSort').value
  end


  # lot actions

  def test11_hold_release
    $setup_ok = false
    #
    lot = @@testlots[0]
    # send 2 holds
    et1 = Time.now
    assert @@b2b.hold(lot, @@holdcode, eventtime: et1), 'error in hold action'
    assert @@b2b.verify_lot_hold(lot, true, eventtime: et1), 'wrong hold parameters'
    et2 = Time.now
    assert @@b2b.hold(lot, @@holdcode2, eventtime: et2), 'error in hold action'
    assert @@b2b.verify_lot_hold(lot, true, eventtime: et2), 'wrong hold parameters'
    # release 2 holds
    assert @@b2b.release_verify(lot, holdtime: et2, onhold: true), 'error releasing lot'
    assert @@b2b.release_verify(lot, holdtime: et1), 'error releasing lot'
    #
   $setup_ok = true
  end    

  def test12_split_merge
    lot = @@testlots[0]
	@@sv.wafer_sort(lot, "tkey_carrier2")
	sleep 100
    assert lc = @@b2b.split(lot, 2), 'split errror, no child'
    assert_equal 2, @@sv.lot_info(lc).nwafers, 'wrong nwafers for child'
    $log.info "waiting #{@@mds_delay} s for the MDS to pick up the new child lot incl. lot script params"; sleep @@mds_delay	
    @@sv.wafer_sort(lc, @@sv.lot_info(lot).carrier)
	refute_nil @@b2b.merge(lot, lc), 'merge error'	
	$log.info "waiting #{@@mds_delay} s for the MDS to pick up the child lot merge"; sleep 30
    assert_equal 'EMPTIED', @@sv.lot_info(lc).status, 'child lot is not EMPTIED'	
  end

  def test13_scrap
    lot = @@testlots[0]
    assert @@b2b.scrap(lot, @@scrapcode)
    li = @@sv.lot_info(lot)
    assert_equal 'SCRAPPED', li.status
    assert_empty li.carrier
  end
  
  	# MSR 1808703    
  def test14_verify_siview_regular_carrier
    $setup_ok = false
    #
	uparams = {'TurnkeyType'=>'L', 'TurnkeySubconIDBump'=>@@tkbump}
    assert lot = @@svtest.new_lot(user_parameters: uparams, carrier: 'regular_carrier'), 'error creating test lot'
    @@testlots << lot
    assert_equal 0, @@sv.lot_gatepass(lot)	
    # send 2 holds
    et1 = Time.now		
	sleep 100
    assert @@b2b.hold(lot, @@holdcode, eventtime: et1), 'error in hold action' 
	assert_equal 'regular_carrier', @@b2b.mq.service_response[:hold2Response][:return][:mesLotInfo][:carrierId]
    assert @@b2b.release_verify(lot, holdtime: et1), 'error releasing lot'
    #
   $setup_ok = true
  end
  
  	# MSR 1808703    
  def test14a_verify_tkey_carrier
    $setup_ok = false
    #
	uparams = {'TurnkeyType'=>'L', 'TurnkeySubconIDBump'=>@@tkbump}
    assert lot = @@svtest.new_lot(user_parameters: uparams, carrier: 'regular_carrier1'), 'error creating test lot'
    @@testlots << lot
    assert_equal 0, @@sv.lot_gatepass(lot)
	sleep 100	
	@@sv.wafer_sort(lot, "tkey_carrier")
    assert response = @@b2b.ext_lotfamily_info(lot)
	assert_equal 'regular_carrier1', @@b2b.mq.service_response[:getExtLotFamilyInfoResponse][:return][:lotInfo][:carrierId]
	@@sv.wafer_sort(lot, "regular_carrier1")
	assert response = @@b2b.ext_lotfamily_info(lot)
	assert_equal 'regular_carrier1', @@b2b.mq.service_response[:getExtLotFamilyInfoResponse][:return][:lotInfo][:carrierId]
    #
   $setup_ok = true
  end
  
   def test14b_verify_multiple_tkey_carrier
    $setup_ok = false
    #
	uparams = {'TurnkeyType'=>'L', 'TurnkeySubconIDBump'=>@@tkbump}
    assert lot = @@svtest.new_lot(user_parameters: uparams, carrier: 'tkey_carrier1'), 'error creating test lot'
    @@testlots << lot
    assert_equal 0, @@sv.lot_gatepass(lot)
	sleep 100
	@@sv.wafer_sort(lot, "tkey_carrier2")
    assert response = @@b2b.ext_lotfamily_info(lot)
	assert_equal 'tkey_carrier2', @@b2b.mq.service_response[:getExtLotFamilyInfoResponse][:return][:lotInfo][:carrierId]
    et2 = Time.now
	@@sv.wafer_sort(lot, "tkey_carrier3")
     assert response = @@b2b.ext_lotfamily_info(lot)
	assert_equal 'tkey_carrier3', @@b2b.mq.service_response[:getExtLotFamilyInfoResponse][:return][:lotInfo][:carrierId]
	#
   $setup_ok = true
  end
  
   def test14c_verify_multiple_regular_siview_carrier
    $setup_ok = false
    #
	uparams = {'TurnkeyType'=>'L', 'TurnkeySubconIDBump'=>@@tkbump}
    assert lot = @@svtest.new_lot(user_parameters: uparams, carrier: 'regular_carrier2'), 'error creating test lot'
    @@testlots << lot
    assert_equal 0, @@sv.lot_gatepass(lot)
    # send 2 holds
    et1 = Time.now
	sleep 100	
	@@sv.wafer_sort(lot, "regular_carrier3")
    assert @@b2b.hold(lot, @@holdcode, eventtime: et1), 'error in hold action'
    assert @@b2b.verify_lot_hold(lot, true, eventtime: et1), 'wrong hold parameters'
	assert_equal 'regular_carrier3', @@b2b.mq.service_response[:hold2Response][:return][:mesLotInfo][:carrierId]
    et2 = Time.now
	@@sv.wafer_sort(lot, "regular_carrier4")
    assert response = @@b2b.ext_lotfamily_info(lot)
	assert_equal 'regular_carrier4', @@b2b.mq.service_response[:getExtLotFamilyInfoResponse][:return][:lotInfo][:carrierId]
	@@sv.wafer_sort(lot, " ")
    #
   $setup_ok = true
  end
  
  def test14d_verify_empty_carrier_and_non_tkey
    $setup_ok = false
    #
	uparams = {'TurnkeyType'=>'L', 'TurnkeySubconIDBump'=>@@tkbump}
    assert lot = @@svtest.new_lot(user_parameters: uparams, carrier: 'regular_carrier10'), 'error creating test lot'
    @@testlots << lot
    assert_equal 0, @@sv.lot_gatepass(lot)
	sleep 100	
	@@sv.wafer_sort(lot, "")
    assert response = @@b2b.ext_lotfamily_info(lot)
	assert_equal 'regular_carrier10', @@b2b.mq.service_response[:getExtLotFamilyInfoResponse][:return][:lotInfo][:carrierId]
	@@sv.wafer_sort(lot, " ")
   $setup_ok = true
  end  
  
  def test14e_verify_empty_carrier_and_tkey
    $setup_ok = false
    #
	uparams = {'TurnkeyType'=>'L', 'TurnkeySubconIDBump'=>@@tkbump}
    assert lot = @@svtest.new_lot(user_parameters: uparams, carrier: 'tkey_carrier4'), 'error creating test lot'
    @@testlots << lot
    assert_equal 0, @@sv.lot_gatepass(lot)
    et1 = Time.now
	sleep 100	
	@@sv.wafer_sort(lot, "")
    assert response = @@b2b.ext_lotfamily_info(lot)
	assert_nil @@b2b.mq.service_response[:getExtLotFamilyInfoResponse][:return][:lotInfo][:carrierId]
	#
   $setup_ok = true
  end
  
  def test15_verify_child_carrier
	$setup_ok = false
    #
	uparams = {'TurnkeyType'=>'L', 'TurnkeySubconIDBump'=>@@tkbump}
    assert lot = @@svtest.new_lot(user_parameters: uparams, carrier: 'tkey_carrier5'), 'error creating test lot'
    @@testlots << lot
    assert_equal 0, @@sv.lot_gatepass(lot)   
	#sleep 300	
	assert lc = @@sv.lot_split(lot, 2), 'split errror, no child'
    assert_equal 2, @@sv.lot_info(lc).nwafers, 'wrong nwafers for child'
    $log.info "waiting #{@@mds_delay} s for the MDS to pick up the new child lot incl. lot script params"; sleep @@mds_delay
	sleep 100	
	@@sv.wafer_sort(lot, "regular_carrier11")	
    assert response = @@b2b.ext_lotfamily_info(lot)
	assert_equal 'regular_carrier11', @@b2b.mq.service_response[:getExtLotFamilyInfoResponse][:return][:lotInfo][:carrierId]
	assert response = @@b2b.ext_lotfamily_info(lc)
	assert_equal 'regular_carrier11', @@b2b.mq.service_response[:getExtLotFamilyInfoResponse][:return][:lotInfo][:carrierId]
  end
  
  def test15a_verify_child_carrier
	$setup_ok = false
    #
	uparams = {'TurnkeyType'=>'L', 'TurnkeySubconIDBump'=>@@tkbump}
    assert lot = @@svtest.new_lot(user_parameters: uparams, carrier: 'regular_carrier12'), 'error creating test lot'
    @@testlots << lot
    assert_equal 0, @@sv.lot_gatepass(lot)  
	@@sv.wafer_sort(lot, "tkey_carrier6")
	assert lc = @@sv.lot_split(lot, 2), 'split errror, no child'
    assert_equal 2, @@sv.lot_info(lc).nwafers, 'wrong nwafers for child'
    $log.info "waiting #{@@mds_delay} s for the MDS to pick up the new child lot incl. lot script params"; sleep @@mds_delay	
	sleep 100	
    assert response = @@b2b.ext_lotfamily_info(lot)
	assert_equal 'regular_carrier12', @@b2b.mq.service_response[:getExtLotFamilyInfoResponse][:return][:lotInfo][:carrierId]
	assert response = @@b2b.ext_lotfamily_info(lc)
	assert_equal 'regular_carrier12', @@b2b.mq.service_response[:getExtLotFamilyInfoResponse][:return][:lotInfo][:carrierId]
	@@sv.wafer_sort(lot, "")
  end
  
  def test16_verify_valid_carrier_xxx
    $setup_ok = false
    #
	uparams = {'TurnkeyType'=>'L', 'TurnkeySubconIDBump'=>@@tkbump}
    assert lot = @@svtest.new_lot(user_parameters: uparams, carrier: 'xxx_regular_carrier'), 'error creating test lot'
    @@testlots << lot
    assert_equal 0, @@sv.lot_gatepass(lot)
    # send 2 holds
    et1 = Time.now
	$log.info "waiting #{@@mds_delay} s for the MDS to pick up the new child lot incl. lot script params"; sleep @@mds_delay	
	sleep 100
    assert response = @@b2b.ext_lotfamily_info(lot)
	assert_equal 'xxx_regular_carrier', @@b2b.mq.service_response[:getExtLotFamilyInfoResponse][:return][:lotInfo][:carrierId]
	@@sv.wafer_sort(lot,'tkey_carrier6')
    assert response = @@b2b.ext_lotfamily_info(lot)
	assert_equal 'xxx_regular_carrier', @@b2b.mq.service_response[:getExtLotFamilyInfoResponse][:return][:lotInfo][:carrierId]
	#
   $setup_ok = true
  end
  
  def test16a_verify_invalid_carrier_xxx
    $setup_ok = false
    #
	uparams = {'TurnkeyType'=>'L', 'TurnkeySubconIDBump'=>@@tkbump}
    assert lot = @@svtest.new_lot(user_parameters: uparams, carrier: 'xxx_TKEY_carrier'), 'error creating test lot'
    @@testlots << lot
    assert_equal 0, @@sv.lot_gatepass(lot)
    # send 2 holds
    et1 = Time.now
	#sleep 300	
	$log.info "waiting #{@@mds_delay} s for the MDS to pick up the new child lot incl. lot script params"; sleep @@mds_delay	
	assert response = @@b2b.ext_lotfamily_info(lot)
	assert_equal 'xxx_TKEY_carrier', @@b2b.mq.service_response[:getExtLotFamilyInfoResponse][:return][:lotInfo][:carrierId]
	@@sv.wafer_sort(lot, 'xxx_regular_carrier1' )
	sleep 100
    assert response = @@b2b.ext_lotfamily_info(lot)
	assert_equal 'xxx_regular_carrier1', @@b2b.mq.service_response[:getExtLotFamilyInfoResponse][:return][:lotInfo][:carrierId]
	#
   $setup_ok = true
  end
  
   def test17_verify_turnkey_type_u8
    $setup_ok = false
    #
	uparams = {'TurnkeyType'=>'U8', 'TurnkeySubconIDBump'=>@@tkbump}
    assert lot = @@svtest.new_lot(user_parameters: uparams), 'error creating test lot'
    @@testlots << lot
    assert_equal 0, @@sv.lot_gatepass(lot)
    # send 2 holds
    et1 = Time.now
	sleep 100	
    assert @@b2b.hold(lot, @@holdcode, eventtime: et1), 'error in hold action'
	assert_equal 'U8', @@b2b.mq.service_response[:hold2Response][:return][:mesLotInfo][:turnkeyType]
	assert response = @@b2b.ext_lotfamily_info(lot)
	assert_equal 'U8', @@b2b.mq.service_response[:getExtLotFamilyInfoResponse][:return][:lotInfo][:turnkeyType]
	#
	end
	
	def test17a_verify_turnkey_type_r3u
    $setup_ok = false
    #
	uparams = {'TurnkeyType'=>'R3u', 'TurnkeySubconIDBump'=>@@tkbump}
    assert lot = @@svtest.new_lot(user_parameters: uparams), 'error creating test lot'
    @@testlots << lot
    assert_equal 0, @@sv.lot_gatepass(lot)
    # send 2 holds
    et1 = Time.now
	sleep 100	
    assert @@b2b.hold(lot, @@holdcode, eventtime: et1), 'error in hold action'
	assert_equal 'R3u', @@b2b.mq.service_response[:hold2Response][:return][:mesLotInfo][:turnkeyType]
	assert response = @@b2b.ext_lotfamily_info(lot)
	assert_equal 'R3', @@b2b.mq.service_response[:getExtLotFamilyInfoResponse][:return][:lotInfo][:turnkeyType]
	end
	
	def test17b_verify_turnkey_type_ju
    $setup_ok = false
    #
	uparams = {'TurnkeyType'=>'Ju', 'TurnkeySubconIDBump'=>@@tkbump}
    assert lot = @@svtest.new_lot(user_parameters: uparams), 'error creating test lot'
    @@testlots << lot
    assert_equal 0, @@sv.lot_gatepass(lot)
    # send 2 holds
    et1 = Time.now
	sleep 100	
    assert @@b2b.hold(lot, @@holdcode, eventtime: et1), 'error in hold action'
	assert_equal 'Ju', @@b2b.mq.service_response[:hold2Response][:return][:mesLotInfo][:turnkeyType]
	assert response = @@b2b.ext_lotfamily_info(lot)
	assert_equal 'J', @@b2b.mq.service_response[:getExtLotFamilyInfoResponse][:return][:lotInfo][:turnkeyType]
	end
	
	def test17c_verify_turnkey_type_qusu
    $setup_ok = false
    #
	uparams = {'TurnkeyType'=>'QU-Su', 'TurnkeySubconIDBump'=>@@tkbump}
    assert lot = @@svtest.new_lot(user_parameters: uparams), 'error creating test lot'
    @@testlots << lot
    assert_equal 0, @@sv.lot_gatepass(lot)
    # send 2 holds
    et1 = Time.now
	sleep 100	
    assert @@b2b.hold(lot, @@holdcode, eventtime: et1), 'error in hold action'
	assert_equal 'QU-Su', @@b2b.mq.service_response[:hold2Response][:return][:mesLotInfo][:turnkeyType]
	assert response = @@b2b.ext_lotfamily_info(lot)
	assert_equal 'QU-S', @@b2b.mq.service_response[:getExtLotFamilyInfoResponse][:return][:lotInfo][:turnkeyType]
	end
	
	def test17d_verify_turnkey_type_qusu_child_lot
    $setup_ok = false
    #
	uparams = {'TurnkeyType'=>'QU-Su', 'TurnkeySubconIDBump'=>@@tkbump}
    assert lot = @@svtest.new_lot(user_parameters: uparams), 'error creating test lot'
    @@testlots << lot
    assert_equal 0, @@sv.lot_gatepass(lot)
    # send 2 holds
    et1 = Time.now
	sleep 100	
	assert lc = @@sv.lot_split(lot, 2), 'split errror, no child'
    assert_equal 2, @@sv.lot_info(lc).nwafers, 'wrong nwafers for child'
    assert @@b2b.hold(lot, @@holdcode, eventtime: et1), 'error in hold action'
	assert_equal 'QU-Su', @@b2b.mq.service_response[:hold2Response][:return][:mesLotInfo][:turnkeyType]
	assert @@b2b.hold(lc, @@holdcode, eventtime: et1), 'error in hold action'
	assert_equal 'QU-Su', @@b2b.mq.service_response[:hold2Response][:return][:mesLotInfo][:turnkeyType]
	assert response = @@b2b.ext_lotfamily_info(lot)
	#assert_equal 'QU-S', @@b2b.mq.service_response[:getExtLotFamilyInfoResponse][:return][:lotInfo][:turnkeyType]
	assert response = @@b2b.ext_lotfamily_info(lc)
	assert_equal 'QU-S', @@b2b.mq.service_response[:getExtLotFamilyInfoResponse][:return][:lotInfo][:turnkeyType]
	end
  
  
    def test18_siview_Split_b2b_merge   
	uparams = {'TurnkeyType'=>'QU-Su', 'TurnkeySubconIDBump'=>@@tkbump}
    assert lot = @@svtest.new_lot(user_parameters: uparams, nwafers: 25), 'error creating test lot'
    @@testlots << lot
    assert_equal 0, @@sv.lot_gatepass(lot)
    # send 2 holds
    et1 = Time.now
	sleep 200
    $log.info "waiting"	
	assert lc = @@sv.lot_split(lot, 2), 'split errror, no child'
		sleep 200
    assert_equal 2, @@sv.lot_info(lc).nwafers, 'wrong nwafers for child'
		refute_nil @@b2b.merge(lot, lc), 'merge error'	
	$log.info "waiting #{@@mds_delay} s for the MDS to pick up the child lot merge"; sleep 30
    assert_equal 'EMPTIED', @@sv.lot_info(lc).status, 'child lot is not EMPTIED'		
  end
  
  def test18a_siview_merge_b2b_split   
	uparams = {'TurnkeyType'=>'QU-Su', 'TurnkeySubconIDBump'=>@@tkbump}
    assert lot = @@svtest.new_lot(user_parameters: uparams, nwafers: 25), 'error creating test lot'
    @@testlots << lot
    assert_equal 0, @@sv.lot_gatepass(lot)
    # send 2 holds
    et1 = Time.now
	sleep 200	
	assert lc = @@b2b.split(lot, 2), 'split errror, no child'
    assert_equal 2, @@sv.lot_info(lc).nwafers, 'wrong nwafers for child'
	refute_nil @@sv.lot_merge(lot, lc), 'merge error'	
	$log.info "waiting #{@@mds_delay} s for the MDS to pick up the child lot merge"; sleep 30
    assert_equal 'EMPTIED', @@sv.lot_info(lc).status, 'child lot is not EMPTIED'		
  end
  
   #INC1080456: 1. Split in FEOL, 2. Sort child into different FEOL carrier and different slot, 3. sort child into FOSB different slot, 4. sort child into 9* TKEY same slot, 5. sort child into unmanaged carrier same slot. Get FamilyInfo for child lot: slot# returned is for step #1 (ITDC old rev), after fix is returned for step #3 (F8 staging new rev). 
	
  def test19_verify_updated_slot_numbers  
	uparams = {'TurnkeyType'=>'QU-Su', 'TurnkeySubconIDBump'=>@@tkbump}
    assert lot = @@svtest.new_lot(user_parameters: uparams, nwafers: 25), 'error creating test lot'
    @@testlots << lot
    assert_equal 0, @@sv.lot_gatepass(lot)
    # send 2 holds
    et1 = Time.now
	sleep 200	
	assert lc = @@sv.lot_split(lot, 2), 'split errror, no child'
    assert_equal 2, @@sv.lot_info(lc).nwafers, 'wrong nwafers for child'
	
	 $sv.wafer_sort_rpt child,'99A01', slot:(10..15).step(2).to_a
    $sv.wafer_sort_rpt child, "3UT001", slot:(15..20).step(2).to_a
    $sv.wafer_sort_rpt child, "99B08"
    $sv.wafer_sort_rpt child, child, :tgt_managed=> false
    assert response = $tkb2b.ext_lot_family_info(child), 'failed'
	end

  # lot moves

  def test21_moves_L
    uparams = {'TurnkeyType'=>'L', 'TurnkeySubconIDBump'=>@@tkbump}
    assert lot = @@svtest.new_lot(product: @@product_ship, route: @@route_ship, user_parameters: uparams), 'error creating test lot'
    @@testlots << lot
    assert_equal 0, @@sv.lot_nonprobankin(lot, 'O-TKYWBANK')
    $log.info "waiting #{@@mds_delay} s for the MDS to pick up the lot script params"; sleep @@mds_delay
    # start and move must do nothing
    li = @@sv.lot_info(lot)
    assert @@b2b.move_verify('start', lot, li.op, 'NonProBank', type: 'BUMP'), 'error in start'
    assert @@b2b.move_verify('move', lot, li.op, 'NonProBank', type: 'BUMP'), 'error in move'
    assert @@b2b.move_verify('move', lot, li.op, 'NonProBank', type: 'SORT'), 'error in move'
    # complete, move on to LOTSHIPDESIG
    assert @@b2b.move_verify('complete', lot, 'LOTSHIPDESIG.01', 'Waiting', type: 'COMPLETE'), 'error in complete'
  end

  def test22_moves_S
    uparams = {'TurnkeyType'=>'S', 'TurnkeySubconIDSort'=>@@tksort}
    assert lot = @@svtest.new_lot(product: @@product_ship, route: @@route_ship, user_parameters: uparams), 'error creating test lot'
    @@testlots << lot
    assert_equal 0, @@sv.lot_nonprobankin(lot, 'O-TKYWBANK')
    $log.info "waiting #{@@mds_delay} s for the MDS to pick up the lot script params"; sleep @@mds_delay
    # start and move must do nothing
    li = @@sv.lot_info(lot)
    assert @@b2b.move_verify('start', lot, li.op, 'NonProBank', type: 'BUMP'), 'error in start'
    assert @@b2b.move_verify('move', lot, li.op, 'NonProBank', type: 'BUMP'), 'error in move'
    assert @@b2b.move_verify('move', lot, li.op, 'NonProBank', type: 'SORT'), 'error in move'
    # complete, move on to LOTSHIPDESIG
    assert @@b2b.move_verify('complete', lot, 'LOTSHIPDESIG.01', 'Waiting', type: 'COMPLETE'), 'error in complete'
  end

  def test23_moves_J
    uparams = {'TurnkeyType'=>'J', 'TurnkeySubconIDBump'=>@@tkbump, 'TurnkeySubconIDSort'=>@@tksort}
    assert lot = @@svtest.new_lot(product: @@product_ship, route: @@route_ship, user_parameters: uparams), 'error creating test lot'
    @@testlots << lot
    assert_equal 0, @@sv.lot_nonprobankin(lot, 'O-TKYWBANK')
    $log.info "waiting #{@@mds_delay} s for the MDS to pick up the lot script params"; sleep @@mds_delay
    # start and move must do nothing
    li = @@sv.lot_info(lot)
    assert @@b2b.move_verify('start', lot, li.op, 'NonProBank', type: 'BUMP'), 'error in start'
    assert @@b2b.move_verify('move', lot, li.op, 'NonProBank', type: 'BUMP'), 'error in move'
    assert @@b2b.move_verify('move', lot, li.op, 'NonProBank', type: 'SORT'), 'error in move'
    # complete, move on to LOTSHIPDESIG
    assert @@b2b.move_verify('complete', lot, 'LOTSHIPDESIG.01', 'Waiting', type: 'COMPLETE'), 'error in complete'
  end
      

  #report Lot ID as CustomerLotID when GID_SOURCE=LOT_LABEL
  def test31_lot_lable_check_country_of_origin
    assert lot = @@svtest.new_lot(customer: @@gid_customer), 'error creating test lot'
    @@testlots << lot
    refute_empty label = @@sv.lot_label(lot), 'lot has no LOT_LABEL'
    assert eli = @@b2b.ext_lotfamily_info(label), 'wrong response message' 
    assert data = eli[:additionalAttributes].find {|e| e[:name] == "COUNTRY_OF_ORIGIN"}, 'Failed to fetch the data'
    assert_equal 'DE', data[:strValue], 'wrong CountryOfOrigin reported'
    assert_equal 'DE', @@sv.user_parameter('Lot', lot, name: 'CountryOfOrigin').value, 'wrong CountryOfOrigin reported'
  end
  
end
