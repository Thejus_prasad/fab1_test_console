=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: sfrieske, 2022-01-10

Version: 22.01

History

Notes:
  http://f1onewiki:21080/display/F1Disp/DokuJCAPMOVELOTS
=end

require 'rtdserver/rtdemuremote'
require 'SiViewTestCase'

# Under Construction!

class JCAP_MoveLots < SiViewTestCase
  @@rtd_rule = 'JCAP_MOVE_LOTS'
  # @@rtd_category = 'DISPATCHER/JCAP'  # will be moved to 'DISPATCHER/RULES'
  @@columns = %w(lot_id dest_bank_id mainpd_id ope_no)

  @@job_interval = 330
  @@testlots = []


  def self.shutdown
    @@testlots.each {|lot| @@sv.delete_lot_family(lot)}
  end


  def test00_setup
    $setup_ok = false
    #
    @@rtdremote = RTD::EmuRemote.new($env)
    #
    assert @@testlots = @@svtest.new_lots(2), 'error creating test lots'
    @@lot1, @@lot2 = @@testlots
    # set lot1 ONHOLD
    assert_equal 0, @@sv.lot_hold(@@lot1, nil)
    # move lots to the skip operation
    assert odata = @@sv.lot_operation_list(@@lot1)[1..-1].find {|e| !e.mandatory}
    @@opNo_skip = odata.opNo
    assert_equal 0, @@sv.lot_opelocate(@@lot1, @@opNo_skip, force: true), 'failed to locate lot'
    assert_equal 0, @@sv.lot_opelocate(@@lot2, @@opNo_skip), 'failed to locate lot'
    #
    # carrier for lot2 is EI
    assert @@svtest.cleanup_eqp(@@eqp, mode: 'Off-Line-1')
    c = @@sv.lot_info(@@lot2).carrier
    @@sv.carrier_xfer_status_change(c, 'EO', @@eqp)
    assert_equal 0, @@sv.eqp_load(@@eqp, nil, c, purpose: 'Other')
    #
    # prepare RTD rule reply
    assert @@rtdremote.add_emustation_wait('', rule: @@rtd_rule, timeout: @@job_interval) {
      @@rtdremote.create_response(@@columns, {'rows'=> @@testlots.collect {|lot| [lot, @@svtest.route, @@opNo_skip]}})
    }, 'rule has not been called'
    sleep 30
    #
    $setup_ok = true
  end

  def test11_happy_lot
    # lot not ONHOLD but carrier EI
    refute_equal @@opNo_skip, @@sv.lot_info(@@lot2).opNo, "wrong operation for lot #{@@lot2}"
    # verify GatePass claim memo
    oph = @@sv.lot_operation_history(@@lot2, category: 'GatePass', opNo: @@opNo_skip)
    assert oph.size > 0, "no GatePass history for lot #{@@lot2}"
    assert oph.last.memo.include?(@@memo), 'wrong claim memo'
  end

  def test12_lot_on_hold
    assert_equal @@opNo_skip, @@sv.lot_info(@@lot1).opNo, "wrong operation for lot #{@@lot1}"
  end

end
