=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: Steffen Steidten, 2011-01-06

Version: 19.01

History:
  2015-04-07 ssteidte, cleanup
  2017-06-06 sfrieske, cleaned up again
  2019-01-30 sfrieske, minor cleanup
  2020-02-12 sfrieske, removed use of _guess_op_route
=end

require 'SiViewTestCase'


class JCAP_NonProBankInQual < SiViewTestCase
  @@sv_defaults = {product: 'ITDC-JCAP-NONPROBANKINFORQUAL.01', route: 'ITDC-JCAP-NONPROBANKINFORQUAL.01'}
  @@slt_dev = 'ES'
  @@slt_qual = 'QD'
  @@slt_tqual = 'EQ'
  @@slt_gatepass = 'PO'
  @@bank_dev = 'T-END'
  @@bank_tqual = 'XY-PLN-HOLD'
  @@opno = '1100.1200'  # PD CHKPLNHLD.01

  @@job_interval = 90 # 1+ min


  def self.after_all_passed
    @@testlots.each {|lot| @@sv.delete_lot_family(lot)}
  end


  def test00_setup
    $setup_ok = false
    #
    assert_equal 'CHKPLNHOLD', @@sv.user_data_pd('CHKPLNHLD.01')['AutomationType'], 'wrong PD UDATA AutomationType'
    #
    @@slt_banks = {@@slt_dev=>@@bank_dev, @@slt_tqual=>@@bank_tqual}
    # create test lots
    @@testlots = []
    assert @@dev_lot = @@svtest.new_lot(sublottype: @@slt_dev)
    @@testlots << @@dev_lot
    assert @@qual_lot = @@svtest.new_lot(sublottype: @@slt_qual)
    @@testlots << @@qual_lot
    assert @@tqual_lot = @@svtest.new_lot(sublottype: @@slt_tqual)
    @@testlots << @@tqual_lot
    assert @@gatepass_lot = @@svtest.new_lot(sublottype: @@slt_gatepass)
    @@testlots << @@gatepass_lot
    #
    ops = @@sv.lot_operation_list(@@gatepass_lot, raw: true)
    opno_idx = ops.find_index {|e| e.operationNumber == @@opno}
    assert_equal 'CHKPLNHLD.01', ops[opno_idx].operationID.identifier, 'wrong PD name'
    @@opno_prev = ops[opno_idx - 1].operationNumber
    @@opno_next = ops[opno_idx + 1].operationNumber
    #
    $setup_ok = true
  end

  def test11_nonprobankin
    $setup_ok = false
    #
    # move lots to the magic operation
    @@testlots.each {|lot| assert_equal 0, @@sv.lot_opelocate(lot, @@opno), 'error locating lot'}
    assert check_status(true)
    #
    $setup_ok = true
  end

  def test12_nonprobankout
    $setup_ok = false
    # nonprobankout, lots must stay banked out
    [@@dev_lot, @@tqual_lot].each {|lot| assert_equal 0, @@sv.lot_nonprobankout(lot)}
    assert check_status(false)
    # verify bankout is permanent
    assert check_status(false)
    #
    $setup_ok = true
  end

  def test14_nonprobankin_again
    $setup_ok = false
    # move lots to the previous and then the magic operation
    @@testlots.each {|lot|
      assert_equal 0, @@sv.lot_opelocate(lot, @@opno_prev)
      assert_equal 0, @@sv.lot_opelocate(lot, @@opno)
    }
    assert check_status(true)
    #
    $setup_ok = true
  end

  def test15_nonprobankout_again
    $setup_ok = false
    # nonprobankout again, lots must stay banked out
    [@@dev_lot, @@tqual_lot].each {|lot| assert_equal 0, @@sv.lot_nonprobankout(lot)}
    assert check_status(false)
    #
    $setup_ok = true
  end

  def test21_prio1
    # set prio != 4, move lots to the previous and then the magic operation
    @@testlots.each {|lot|
      @@sv.schdl_change(lot, priority: 1)
      assert_equal '1', @@sv.lot_info(lot).priority, 'wrong priority'
      assert_equal 0, @@sv.lot_opelocate(lot, @@opno_prev)
      assert_equal 0, @@sv.lot_opelocate(lot, @@opno)
    }
    assert check_status(false)
  end


  # aux methods

  # return true on success
  def check_status(bankedin)
    # wait for the jCAP job to complete
    $log.info "waiting #{@@job_interval} s for the jCAP job to complete"; sleep @@job_interval
    # check lot status
    summary = []
    test_ok = true
    @@testlots.each {|lot|
      li = @@sv.lot_info(lot)
      if [@@slt_dev, @@slt_tqual].member?(li.sublottype)
        if bankedin
          ok = (li.status == 'NonProBank' && li.bank == @@slt_banks[li.sublottype])
        else
          ok = (li.status != 'NonProBank' && li.opNo == @@opno)
        end
      else
        ok = (li.opNo == @@opno_next)
      end
      summary << [lot, li.opNo, li.status, li.bank, li.customer, li.sublottype, ok]
      test_ok &= ok
    }
    $log.warn("-- test summary: #{test_ok}\n#{summary.pretty_inspect}") unless test_ok
    return test_ok
  end

end
