=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: Conny Stenzel, 2011-01-18

Version: 20.08

History:
  2012-01-09 ssteidte, refactored
  2015-04-09 ssteidte, cleanup
  2015-11-23 sfrieske, changed tests21 and 22 to use future holds (with reason X-IP)
  2017-10-13 sfrieske, replaced user_parameter_set_verify with user_parameter_change
  2018-12-06 sfrieske, minor cleanup
  2019-02-21 sfrieske, improved notification verification
  2020-04-02 sfrieske, extended test21 by 'Unknown_...', changed test26, added test27
  2020-05-25 sfrieske, added test22, 23 for MSR 1668319
  2020-10-02 sfrieske, added claim memo for testing MSR 1713059
  2020-11-10 sfrieske, added use_notifications to bypass notification checks
=end

require 'derdack'
require 'SiViewTestCase'


class JCAP_AutoSplitLotsOnHold < SiViewTestCase
  @@sv_defaults = {product: 'ITDC-JCAP-TEST.01', route: 'ITDC-JCAP-TEST.01', carriers: 'JC%'}
  @@opNo_split_default = '1000.1200'  # 'ITDC-JCAP-TEST-DUMMY.01'
  @@opNo_split_marked = '1000.1400'   # 'ITDC-JCAP-TEST-DUMMY.01'
  # merge PD configuration "look.ahead.fail.over.merge.pd.distance":
  #   0: current, 1: next (default), 2: next but 1
  # using the current PD on the route for merge
  @@opNo_merge_default= '1000.1200'
  @@opNo_merge_marked = '3000.1500'
  # additional future holds set by a user by property (v20.5)
  @@reason_allowed = 'ENG'
  @@reason_excluded = 'X-S1'  # property autosplitlotsonhold.split.exclusion.holds.xip=["X-S*", ...
  # future holds set by APC or EI resp.
  @@reason_apc = 'X-RW'
  @@reason_ei = 'X-IP'
  @@memo_ei = 'QAMemo Orig'
  # holds set by the JCAP job
  @@reason_complete = 'X-MC'
  @@reason_error = 'X-ME'

  @@use_futurehold = false
  @@use_notifications = true

  @@job_interval = 310
  @@testlots = []


  def self.after_all_passed
    @@testlots.each {|lot| @@sv.delete_lot_family(lot)}
  end


  def test00_setup
    $setup_ok = false
    #
    if @@use_notifications
      @@notifications = DerdackEvents.new($env)
      @@notification_params = {timeout: @@job_interval + 60,
        service: '%jCAP_AsyncNotification%', filter: {'Application'=>'AutoSplitLotsOnHold'}}
    end
    # set holdtype, must match look.ahead.fail.over.merge.pd.distance setting
    @@holdtype_normal = @@use_futurehold ? 'FutureHold' : 'LotHold'
    @@rsp_mark = @@use_futurehold ? 'P' : 'C'
    # check PD UDATA
    rops = @@sv.route_operations(@@svtest.route)
    [@@opNo_split_default, @@opNo_split_marked].each {|opNo|
      assert e = rops.find {|e| e.operationNumber == opNo}, "opNo #{opNo} not on test route"
      udata = @@sv.user_data_pd(e.operationID.identifier)['AutomationType']
      # Fab8 has a camel case value
      assert_equal 'AUTOSPLIT', udata.upcase, 'wrong PD UDATA AutomationType'
    }
    #
    $setup_ok = true
  end

  # APC Rework

  def test11_default_pd
    $setup_ok = false
    #
    assert lot = @@svtest.new_lot, 'error creating test lot'
    @@testlots << lot
    # move lot to split PD, set script params, hold and wait for the jCAP job
    assert_equal 0, @@sv.lot_opelocate(lot, @@opNo_split_default), 'error locating lot'
    tstart = Time.now
    set_scriptparameters_apc(lot)
    set_hold_apc(lot)
    $log.info "waiting #{@@job_interval} s for the jCAP job"; sleep @@job_interval
    #
    assert verify_lot_split(lot, @@opNo_merge_default), 'error splitting lot'
    if (@@opNo_split_default == @@opNo_merge_default)  # || @@use_futurehold)
      assert_equal 0, @@sv.lot_hold_list(lot, type: @@holdtype_normal).size, 'wrong lot holds'
      assert_equal 1, @@sv.lot_hold_list(lot, type: 'MergeHold').size, 'missing merge hold'
    else
      assert_equal 0, @@sv.lot_hold_list(lot).size, 'lot holds are not released'
    end
    child = @@sv.lot_family(lot).last
    refute_equal lot, child, 'lot has not been split'
    verify_lot_hold(child)
    verify_user_params_deleted(lot)
    if @@use_notifications
      assert @@notifications.wait_for(@@notification_params.merge(tstart: tstart)) {|msgs|
        msgs.find {|m| m['LotID'] == lot}
      }, 'missing Derdack notification'
    end
    #
    $setup_ok = true
  end

  def test13_marked_pd
    assert lot = @@svtest.new_lot, 'error creating test lot'
    @@testlots << lot
    # move lot to split PD, set script params, NO hold and wait for the jCAP job
    assert_equal 0, @@sv.lot_opelocate(lot, @@opNo_split_marked)
    set_scriptparameters_apc(lot)
    $log.info "waiting #{@@job_interval} s for the jCAP job"; sleep @@job_interval
    #
    assert_equal 1, @@sv.lot_family(lot).size, 'lot has been split'
    #
    # set hold and wait for the jCAP job
    set_hold_apc(lot)
    $log.info "waiting #{@@job_interval} s for the jCAP job"; sleep @@job_interval
    #
    assert verify_lot_split(lot, @@opNo_merge_marked), 'error splitting lot'
    assert_empty @@sv.lot_hold_list(lot), 'lot holds are not released'
    child = @@sv.lot_family(lot).last
    verify_lot_hold(child)
    verify_user_params_deleted(lot)
  end

  def test14_wrong_passcount
    assert lot = @@svtest.new_lot, 'error creating test lot'
    @@testlots << lot
    # move lot to split PD, set script params with WRONG passcount, hold and wait for the jCAP job
    assert_equal 0, @@sv.lot_opelocate(lot, @@opNo_split_marked)
    set_scriptparameters_apc(lot, pass: 'wrong')
    set_hold_apc(lot)
    $log.info "waiting #{@@job_interval} s for the jCAP job"; sleep @@job_interval
    #
    assert_equal 1, @@sv.lot_family(lot).size, 'lot has been split'
    verify_lot_hold(lot, reason: @@reason_error)
    # not deleted: verify_user_params_deleted(lot)
  end

  def test15_all_wafers
    assert lot = @@svtest.new_lot, 'error creating test lot'
    @@testlots << lot
    # move lot to split PD, set script params for ALL wafers, hold and wait for the jCAP job
    assert_equal 0, @@sv.lot_opelocate(lot, @@opNo_split_default)
    set_scriptparameters_apc(lot, nwafers: 25)
    set_hold_apc(lot)
    $log.info "waiting #{@@job_interval} s for the jCAP job"; sleep @@job_interval
    #
    assert_equal 1, @@sv.lot_family(lot).size, 'lot has been split'
    verify_lot_hold(lot)
    verify_user_params_deleted(lot)
    # no notification
  end

  def test16_carrier_EI
    assert_equal @@opNo_split_default, @@opNo_merge_default, 'wrong merge PD' unless @@use_futurehold
    assert lot = @@svtest.new_lot, 'error creating test lot'
    @@testlots << lot
    # load carrier
    li = @@sv.lot_info(lot)
    eqp = li.opEqps.first
    @@sv.carrier_xfer_status_change(li.carrier, "EO", li.opEqps[0])
    assert_equal 0, @@sv.eqp_load(eqp, nil, li.carrier, purpose: 'Other'), 'error loading carrier'
    # move lot to split PD, set script params, hold and wait for the jCAP job
    assert_equal 0, @@sv.lot_opelocate(lot, @@opNo_split_default)
    tstart = Time.now
    set_scriptparameters_apc(lot)
    set_hold_apc(lot)
    $log.info "waiting #{@@job_interval} s for the jCAP job"; sleep @@job_interval
    #
    assert verify_lot_split(lot, @@opNo_merge_default), 'error splitting lot'
    assert_equal 0, @@sv.lot_hold_list(lot, type: @@holdtype_normal).size, 'wrong lot hold'
    if @@use_futurehold
      assert_equal 1, @@sv.lot_futurehold_list(lot, type: 'MergeHold').size, 'missing future MergeHold'
    else
      assert_equal 1, @@sv.lot_hold_list(lot, type: 'MergeHold').size, 'missing MergeHold'
    end
    child = @@sv.lot_family(lot).last
    verify_lot_hold(child)
    verify_user_params_deleted(lot)
    if @@use_notifications
      assert @@notifications.wait_for(@@notification_params.merge(tstart: tstart)) {|msgs|
        msgs.find {|m| m['LotID'] == lot}
      }, 'missing Derdack notification'
    end
  end

  def test17_lot_without_carrier
    assert lots = @@svtest.new_lots(3), 'error creating test lots'
    @@testlots += lots
    # move lots to to split pd, set script params and hold
    lots.each {|lot|
      assert_equal 0, @@sv.lot_opelocate(lot, @@opNo_split_default)
      set_scriptparameters_apc(lot)
      set_hold_apc(lot)  # use only in case the configured merge PD is the next one
    }
    # move 1 lot out of its carrier and wait for the jCAP job
    lotnc = lots[1]
    assert_equal 0, @@sv.wafer_sort(lotnc, '')
    $log.info "waiting #{@@job_interval} s for the jCAP job"; sleep @@job_interval
    #
    # check all lots that are in a carrier
    # nmghl = (@@opNo_split_default == @@opNo_merge_default || @@use_futurehold) ? 1 : 0
    (lots - [lotnc]).each {|lot|
      assert verify_lot_split(lot, @@opNo_merge_default), 'error splitting lot'
      # filter on
      assert_equal 0, @@sv.lot_hold_list(lot, type: @@holdtype_normal).size, 'wrong lot holds'
      # assert_equal nmghl, @@sv.lot_hold_list(lot, type: 'MergeHold').size, 'wrong/missing merge hold'
      if @@use_futurehold
        assert_equal 1, @@sv.lot_futurehold_list(lot, type: 'MergeHold').size, 'missing future MergeHold'
      else
        assert_equal 1, @@sv.lot_hold_list(lot, type: 'MergeHold').size, 'missing MergeHold'
      end

      child = @@sv.lot_family(lot).last
      verify_lot_hold(child)
      verify_user_params_deleted(lot)
    }
    #
    # verify the lot without carrier is not split
    assert_equal 1, @@sv.lot_family(lotnc).size, 'lot has been split'
    # filter on
    assert_equal 1, @@sv.lot_hold_list(lotnc, type: @@holdtype_normal).size, 'wrong lot holds'
    assert_equal 0, @@sv.lot_hold_list(lotnc, type: 'MergeHold').size, 'wrong merge hold'
  end

  # EI Processing, always use future hold

  def test21_incomplete_processing
    assert lot = @@svtest.new_lot, 'error creating test lot'
    @@testlots << lot
    # move lot to split PD, set different values for "EIProcessState"
    assert_equal 0, @@sv.lot_opelocate(lot, @@opNo_split_default)
    li = @@sv.lot_info(lot, wafers: true)
    v = "#{li.op}_#{li.route}_#{li.opNo}_1"  # passcount is 1 for a fresh lot
    li.wafers.each_with_index {|w, i|
      if i < 3
        value = "Processed_#{v}"
      elsif i < 6
        value = "InProcessing_#{v}"
      elsif i < 9
        value = "Unknown_#{v}"
      else
        value = "Unprocessed_#{v}"
      end
      assert_equal 0, @@sv.user_parameter_change('Wafer', w.wafer,
        'EIProcessState', value, 1, datatype: 'STRING')
    }
    tstart = Time.now
    # set X-EI future hold, gatepass and wait for jCAP job
    assert_equal 0, @@sv.lot_futurehold(lot, @@opNo_split_default, @@reason_ei, post: true, memo: @@memo_ei)
    assert_equal 0, @@sv.lot_gatepass(lot), 'error gatepassing lot'
    $log.info "waiting #{@@job_interval} s for the jCAP job"; sleep @@job_interval
    #
    mrgopNo = @@use_futurehold ? @@opNo_merge_default : @@sv.lot_info(lot).opNo # TODO ??
    assert verify_lot_split(lot, mrgopNo, 3), 'error splitting lot'
    assert_empty @@sv.lot_hold_list(lot, reason: @@reason_ei), 'X-IP hold is not released'
    children = @@sv.lot_family(lot) - [lot]
    children.each {|child|
      verify_lot_hold(child, rsp_mark: 'P')
      verify_user_params_deleted(child)
    }
    verify_user_params_deleted(lot)
    if @@use_notifications
      assert @@notifications.wait_for(@@notification_params.merge(tstart: tstart)) {|msgs|
        msgs.find {|m| m['LotID'] == lot}
      }, 'missing Derdack notification'
    end
  end

  def test22_ei_hold_allowed
    assert lot = @@svtest.new_lot, 'error creating test lot'
    @@testlots << lot
    # move lot to split PD, set different values for "EIProcessState"
    assert_equal 0, @@sv.lot_opelocate(lot, @@opNo_split_default)
    li = @@sv.lot_info(lot, wafers: true)
    v = "#{li.op}_#{li.route}_#{li.opNo}_1"  # passcount is 1 for a fresh lot
    li.wafers.each_with_index {|w, i|
      if i < 3
        value = "Processed_#{v}"
      elsif i < 6
        value = "InProcessing_#{v}"
      elsif i < 9
        value = "Unknown_#{v}"
      else
        value = "Unprocessed_#{v}"
      end
      assert_equal 0, @@sv.user_parameter_change('Wafer', w.wafer,
        'EIProcessState', value, 1, datatype: 'STRING')
    }
    tstart = Time.now
    # set additional future hold and X-EI, gatepass and wait for jCAP job
    assert_equal 0, @@sv.lot_futurehold(lot, @@opNo_split_default, @@reason_allowed, post: true)
    assert_equal 0, @@sv.lot_futurehold(lot, @@opNo_split_default, @@reason_ei, post: true, memo: @@memo_ei)
    assert_equal 0, @@sv.lot_gatepass(lot), 'error gatepassing lot'
    $log.info "waiting #{@@job_interval} s for the jCAP job"; sleep @@job_interval
    #
    mrgopNo = @@use_futurehold ? @@opNo_merge_default : @@sv.lot_info(lot).opNo # TODO ??
    assert verify_lot_split(lot, mrgopNo, 3), 'error splitting lot'
    # assert verify_lot_split(lot, @@sv.lot_info(lot).opNo, 3), 'error splitting lot'
    assert_empty @@sv.lot_hold_list(lot, reason: @@reason_ei), 'X-IP hold is not released'
    children = @@sv.lot_family(lot) - [lot]
    children.each {|child|
      verify_lot_hold(child, rsp_mark: 'P')
      verify_user_params_deleted(child)
    }
    verify_user_params_deleted(lot)
    if @@use_notifications
      assert @@notifications.wait_for(@@notification_params.merge(tstart: tstart)) {|msgs|
        msgs.find {|m| m['LotID'] == lot}
      }, 'missing Derdack notification'
    end
  end

  def test23_ei_hold_excluded
    assert lot = @@svtest.new_lot, 'error creating test lot'
    @@testlots << lot
    # move lot to split PD, set different values for "EIProcessState"
    assert_equal 0, @@sv.lot_opelocate(lot, @@opNo_split_default)
    li = @@sv.lot_info(lot, wafers: true)
    v = "#{li.op}_#{li.route}_#{li.opNo}_1"  # passcount is 1 for a fresh lot
    li.wafers.each_with_index {|w, i|
      if i < 3
        value = "Processed_#{v}"
      elsif i < 6
        value = "InProcessing_#{v}"
      elsif i < 9
        value = "Unknown_#{v}"
      else
        value = "Unprocessed_#{v}"
      end
      assert_equal 0, @@sv.user_parameter_change('Wafer', w.wafer,
        'EIProcessState', value, 1, datatype: 'STRING')
    }
    tstart = Time.now
    # set additional future hold and X-EI, gatepass and wait for jCAP job
    assert_equal 0, @@sv.lot_futurehold(lot, @@opNo_split_default, @@reason_excluded, post: true)
    assert_equal 0, @@sv.lot_futurehold(lot, @@opNo_split_default, @@reason_ei, post: true, memo: @@memo_ei)
    assert_equal 0, @@sv.lot_gatepass(lot), 'error gatepassing lot'
    $log.info "waiting #{@@job_interval} s for the jCAP job"; sleep @@job_interval
    #
    # lot is not touched by the job
    assert_equal [lot], @@sv.lot_family(lot)
    refute_empty @@sv.lot_hold_list(lot, reason: @@reason_ei), 'X-IP hold removed'
    refute_empty @@sv.lot_hold_list(lot, reason: @@reason_excluded), 'excluded hold removed'
  end

  def test26_ei_inprocessing_all_error
    assert lot = @@svtest.new_lot, 'error creating test lot'
    @@testlots << lot
    # move lot to split PD and set script params for all 25 wafers
    assert_equal 0, @@sv.lot_opelocate(lot, @@opNo_split_default, offset: -1)
    li = @@sv.lot_info(lot, wafers: true)
    v = "#{li.op}_#{li.route}_#{li.opNo}_1"  # passcount is 1 for a fresh lot
    li.wafers.each {|w|
      assert_equal 0, @@sv.user_parameter_change('Wafer', w.wafer, 'EIProcessState', "InProcessing_#{v}")
    }
    # set future hold and wait for jCAP job
    assert_equal 0, @@sv.lot_futurehold(lot, @@opNo_split_default, @@reason_ei, memo: @@memo_ei)
    assert_equal 0, @@sv.lot_opelocate(lot, 1)
    $log.info "waiting #{@@job_interval} s for the jCAP job"; sleep @@job_interval
    #
    assert_equal 1, @@sv.lot_family(lot).size, 'lot has been split'
    # TODO: always C? rsp_mark = @@use_futurehold ? 'C' : @@rsp_mark
    verify_lot_hold(lot, rsp_mark: 'C', reason: @@reason_error)  # expectation changed with v20.02
    ## not deleted, no notification: verify_user_params_deleted(lot)
  end

  def test27_ei_inprocessing_all_happy
    assert lot = @@svtest.new_lot, 'error creating test lot'
    @@testlots << lot
    # move lot to process PD (with UDATA set) and set script params for all 25 wafers
    assert_equal 0, @@sv.lot_opelocate(lot, @@opNo_split_default)
    li = @@sv.lot_info(lot, wafers: true)
    v = "#{li.op}_#{li.route}_#{li.opNo}_1"  # passcount is 1 for a fresh lot
    li.wafers.each {|w|
      assert_equal 0, @@sv.user_parameter_change('Wafer', w.wafer, 'EIProcessState', "InProcessing_#{v}")
    }
    # set future hold Post at process PD, process lot and wait for jCAP job
    assert_equal 0, @@sv.lot_futurehold(lot, li.opNo, @@reason_ei, post: true, memo: @@memo_ei)
    # need to process, locate is rejected by SiView because of the FutureHold Post
    assert @@sv.claim_process_lot(lot), 'error processing lot'
    # note: the FutureHold has become a Hold with type 'FutureHold' and resp_mark 'P'
    $log.info "waiting #{@@job_interval} s for the jCAP job"; sleep @@job_interval
    #
    assert_equal 1, @@sv.lot_family(lot).size, 'lot has been split'
    verify_lot_hold(lot, rsp_mark: 'P')
    # not deleted, no notification: verify_user_params_deleted(lot)
  end


  # aux methods

  def set_hold_apc(lot)
    if @@use_futurehold
      li = @@sv.lot_info(lot)
      assert_equal 0, @@sv.lot_futurehold(lot, li.opNo, @@reason_apc, post: true, memo: @@memo_ei)
      assert_equal 0, @@sv.lot_gatepass(lot)
    else
      assert_equal 0, @@sv.lot_hold(lot, @@reason_apc, memo: @@memo_ei), 'failed to set lot hold'
    end
  end

  def set_scriptparameters_apc(lot, params={})
    li = @@sv.lot_info(lot, wafers: true)
    pass = @@sv.lot_operation_list(lot).first.pass
    pass.next! if params[:pass] == 'wrong'  # pass is a String
    nwafers = params[:nwafers] || 5
    value = "#{li.route}_#{li.opNo}_#{pass}_#{lot}_#{@@reason_apc}"
    li.wafers.each_with_index {|w, i|
      assert_equal 0, @@sv.user_parameter_delete('Wafer', w.wafer, 'EIProcessState', check: true)
      if i < nwafers
        assert_equal 0, @@sv.user_parameter_change('Wafer', w.wafer, 'MarkSplit', value, 1, datatype: 'STRING')
      else
        assert_equal 0, @@sv.user_parameter_delete('Wafer', w.wafer, 'MarkSplit', check: true)
      end
    }
    return true
  end

  def verify_lot_split(lot, opNo_merge, count=1)
    $log.info "verify_lot_split #{lot.inspect}, #{opNo_merge.inspect}, #{count}"
    children = @@sv.lot_family(lot) - [lot]
    if children.size != count
      $log.warn "  wrong number of children: #{children.size}"
      return false
    end
    # check parent lot
    hls = @@sv.lot_futurehold_list(lot, reason: 'MGHL')
    if hls.size != count
      $log.warn "  wrong number of merge holds: #{hls.size}"
      return false
    end
    ret = true
    hls.each {|hl|
      ($log.warn "  wrong lot merge op: #{hl.inspect}"; ret=false) if hl.rsp_op != opNo_merge
    }
    # check child lots
    children.each {|child|
      hls = @@sv.lot_futurehold_list(child, reason: 'MGHL')
      if hls.size != 1
        $log.warn '  wrong or missing child merge hold'
        ret = false
      elsif hls.first.rsp_op != opNo_merge
        $log.warn "  wrong merge op: #{hls[0].inspect}"
        ret = false
      end
    }
    return ret
  end

  def verify_lot_hold(lot, rsp_mark: @@rsp_mark, reason: @@reason_complete)
    $log.info "verify_lot_hold #{lot.inspect}"
    assert hold = @@sv.lot_hold_list(lot, reason: reason).first, 'missing lot hold'
    assert_equal rsp_mark, hold.rsp_mark, 'wrong operation mark'
    if reason == @@reason_complete || reason == @@reason_apc
      assert hold.memo.end_with?(@@memo_ei), 'missing original memo'
    end
  end

  def verify_user_params_deleted(lot)
    $log.info "verify_user_params_deleted #{lot}"
    ret = true
    @@sv.lot_info(lot, wafers: true).wafers.each {|w|
      ret &= !@@sv.user_parameter('Wafer', w.wafer, name: 'MarkSplit').valueflag
      ret &= !@@sv.user_parameter('Wafer', w.wafer, name: 'EIProcessState').valueflag
    }
    assert ret, 'wafer script parameters not deleted'
  end

end
