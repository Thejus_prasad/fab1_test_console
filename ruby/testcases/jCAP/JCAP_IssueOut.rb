=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.


Author: Steffen Frieske, 2019-11-27

Version: 20.11

History:
  2020-07-08 sfrieske, removed newlot_delay
  2020-12-03 sfrieske, added test15
  2021-02-11 sfrieske, removed unused parameter :srclot_ondemand
  2021-08-27 sfrieske, require uniquestring instead of misc
  2021-12-14 sfrieske, unified srcproduct handling

Notes:
  job properties must be set to have QA.ANYUSE_REQUEST01 as MQ output queue!
  see http://f1onewiki:21080/display/JCAP/IssueOutProcess
  see https://docs.google.com/document/d/1GrODO2D3h0MNL7PYjrqqC3U0DmmH5xS9xo0uEvlsNmE/edit
    jCAP Req: Issue Out Process (MSR 1511033)

  prerequisite:
  - PD with UDATA AutomationType: OracleIssueOut
  - Wafer script Parameter: OracleLotID is set (normally by AVLS)
  - Lot Script Parameter OracleIssueOut is not DONE
=end

require 'derdack'
require 'mqjms'
require 'util/uniquestring'
require 'util/waitfor'
require 'util/xmlhash'
require 'SiViewTestCase'


class JCAP_IssueOut < SiViewTestCase
  @@sv_defaults = {
    product: 'UT-PRODUCT-ISSUEOUT.01', route: 'UTRT001-ISSUEOUT.01', nwafers: 5, product_eqpmonitor: 'UT-PRODUCT-ISSUEOUT.02'
  }
  @@srcproduct_prime = 'UT-SRC-PRIME.01'        # Udata RawWaferType PRIME_QA
  @@srcproduct_nonprime = 'UT-SRC-NONPRIME.01'  # Udata RawWaferType NONPRIME_QA
  @@srcproduct_empty = 'UT-SRC-EMPTY.01'        # Udata RawWaferType not set
  @@op = 'UTPD-ISSUEOUT.01'
  @@spname = 'OracleIssueOut'             # empty, DONE, SKIP are valid values
  @@reason_err = 'X-MG'
  @@slt2 = 'EM'
  @@costcenter_prime1 = '000000-116113'
  @@costcenter_prime2 = '101014-551199'   # for SLT2
  @@acct_nonprime = '551116'
  @@product_costcenter = '114010'         # SM UData for testwafers, must exist in SM
  # msg values
  @@orgcode_prime = 'MS1'
  @@orgcode_nonprime = 'MA1'
  @@subinvcode = 'PSL-PCO01'
  # backup operation
  @@env_src = 'f8stag'
  @@sv_defaults_src = {product: 'UT-PRODUCT-FAB1.01', route: 'UTRT101.01', carriers: '%'}
  @@psend_src = {backup_opNo: '1000.200', backup_bank: 'O-BACKUPOPER'}
  @@preturn_src = {return_bank: 'O-BACKUPOPER'}

  @@job_interval = 620  #530  # 5 minutes + slack
  @@mds_sync = 45
  @@testlots_bop = []
  @@testlots = []


  def self.after_all_passed
    @@testlots_bop.each {|lot| @@sv_src.backup_delete_lot_family(lot, @@sv)}
    @@testlots.each {|lot| @@sv.delete_lot_family(lot)}
  end

  def setup
    return unless self.class.class_variable_defined?(:@@mq)
    refute_nil @@mq.delete_msgs(nil), 'MQ issues'
  end


  def test00_setup
    $setup_ok = false
    #
    assert @@sv.user_data(:product, @@srcproduct_prime)['RawWaferType'].start_with?('PRIME'), 'wrong srcproduct UData'
    assert @@sv.user_data(:product, @@srcproduct_nonprime)['RawWaferType'].start_with?('NONPRIME'), 'wrong srcproduct UData'
    assert_nil @@sv.user_data(:product, @@srcproduct_empty)['RawWaferType'], 'wrong srcproduct UData'
    #
    assert @@notifications = DerdackEvents.new($env)
    assert @@mq = MQ::JMS.new("issueout.#{$env}")
    refute_nil @@mq.delete_msgs(nil), 'MQ issues'
    #
    $setup_ok = true
  end

  def test11_prime
    $setup_ok = false
    #
    assert lot = @@svtest.new_lot(srcproduct: @@srcproduct_prime), 'error creating test lot'
    @@testlots << lot
    #
    # locate lot to PD with Udata AutomationType 'OracleIssueOut' but no script parameters set
    assert_equal 0, @@sv.lot_opelocate(lot, nil, op: @@op), 'error locating lot'
    verify_failure(lot, 'OracleLotID is empty', "Script parameter 'OracleLotID' is empty or invalid")
    #
    # set wafer script parameters and release lot hold
    olotid = unique_string
    li = @@sv.lot_info(lot, wafers: true)
    li.wafers.each {|w| assert_equal 0, @@sv.user_parameter_change('Wafer', w.wafer, 'OracleLotID', olotid)}
    assert_equal 0, @@sv.lot_hold_release(lot, @@reason_err)
    verify_success(lot, olotid, @@costcenter_prime1, @@orgcode_prime, @@srcproduct_prime)
    #
    $setup_ok = true
  end

  def test12_nonprime
    assert lot = @@svtest.new_lot(srcproduct: @@srcproduct_nonprime), 'error creating test lot'
    @@testlots << lot
    li = @@sv.lot_info(lot, wafers: true)
    olotid = unique_string
    li.wafers.each {|w| assert_equal 0, @@sv.user_parameter_change('Wafer', w.wafer, 'OracleLotID', olotid)}
    #
    # no OracleCostCenterForWaferStart, lot must go ONHOLD
    assert_equal 0, @@sv.lot_opelocate(lot, nil, op: @@op), 'error locating lot'
    verify_failure(lot, 'OracleCostCenterForWaferStart is empty',
                        "Script parameter 'OracleCostCenterForWaferStart' is empty or invalid")
    #
    # set OracleCostCenterForWaferStart, release hold
    oraclecc = 'QANONPRIME1'
    assert_equal 0, @@sv.user_parameter_change('Lot', lot, 'OracleCostCenterForWaferStart', oraclecc)
    assert_equal 0, @@sv.lot_hold_release(lot, nil)
    verify_success(lot, olotid, "#{oraclecc}-#{@@acct_nonprime}", @@orgcode_nonprime, @@srcproduct_nonprime)
  end

  def test13_empty
    assert lot = @@svtest.new_lot(srcproduct: @@srcproduct_empty), 'error creating test lot'
    @@testlots << lot
    li = @@sv.lot_info(lot, wafers: true)
    olotid = unique_string
    li.wafers.each {|w| assert_equal 0, @@sv.user_parameter_change('Wafer', w.wafer, 'OracleLotID', olotid)}
    #
    # no RawWaferType, lot must go ONHOLD
    assert_equal 0, @@sv.lot_opelocate(lot, nil, op: @@op), 'error locating lot'
    verify_failure(lot, 'RawWaferType is empty', "Empty product udata 'RawWaferType'")
  end

  def test14_nonprime_monitor
    assert @@sm.update_udata(:product, @@svtest.product_eqpmonitor, {'ProductCostCenter'=>''})
    $log.info "waiting #{@@mds_sync} s for the MDS to sync"; sleep @@mds_sync
    assert lot = @@svtest.new_controllot('Equipment Monitor', srcproduct: @@srcproduct_nonprime), 'error creating test lot'
    @@testlots << lot
    li = @@sv.lot_info(lot, wafers: true)
    olotid = unique_string
    li.wafers.each {|w| assert_equal 0, @@sv.user_parameter_change('Wafer', w.wafer, 'OracleLotID', olotid)}
    #
    # no ProductCostCenter, lot must go ONHOLD
    assert_equal 0, @@sv.lot_opelocate(lot, nil, op: @@op), 'error locating lot'
    verify_failure(lot, 'ProductCostCenter is empty', "Empty product udata 'ProductCostCenter'")
    #
    # set ProductCostCenter as lot script parameter (20.11), release hold
    # assert @@sm.update_udata(:product, @@svtest.product_eqpmonitor, {'ProductCostCenter'=>@@product_costcenter})
    assert_equal 0, @@sv.user_parameter_change('Lot', lot, 'OracleCostCenterForWaferStart', @@product_costcenter)
    # $log.info "waiting #{@@mds_sync} s for the MDS to sync"; sleep @@mds_sync
    assert_equal 0, @@sv.lot_hold_release(lot, nil)
    verify_success(lot, olotid, "#{@@product_costcenter}-#{@@acct_nonprime}", @@orgcode_nonprime, @@srcproduct_nonprime)
  end

  def test15_mixed  # prime, nonprime and invalid wafers in 1 carrier, v20.11
    # prepare 3 differnt lots in 1 carrier
    # prime
    assert lot0 = @@svtest.new_lot(srcproduct: @@srcproduct_prime), 'error creating test lot'
    @@testlots << lot0
    li0 = @@sv.lot_info(lot0, wafers: true)
    olotid0 = unique_string
    li0.wafers.each {|w| assert_equal 0, @@sv.user_parameter_change('Wafer', w.wafer, 'OracleLotID', olotid0)}
    # noneprime
    assert lot1 = @@svtest.new_lot(srcproduct: @@srcproduct_nonprime), 'error creating test lot'
    @@testlots << lot1
    li1 = @@sv.lot_info(lot1, wafers: true)
    olotid1 = unique_string
    li1.wafers.each {|w| assert_equal 0, @@sv.user_parameter_change('Wafer', w.wafer, 'OracleLotID', olotid1)}
    assert_equal 0, @@sv.wafer_sort(lot1, li0.carrier, slots: 'empty')
    oraclecc1 = 'QANONPRIME1'
    assert_equal 0, @@sv.user_parameter_change('Lot', lot1, 'OracleCostCenterForWaferStart', oraclecc1)
    # no src product UDATA
    assert lot2 = @@svtest.new_lot(srcproduct: @@srcproduct_empty), 'error creating test lot'
    @@testlots << lot2
    li2 = @@sv.lot_info(lot2, wafers: true)
    olotid2 = unique_string
    li2.wafers.each {|w| assert_equal 0, @@sv.user_parameter_change('Wafer', w.wafer, 'OracleLotID', olotid2)}
    assert_equal 0, @@sv.wafer_sort(lot2, li0.carrier, slots: 'empty')
    #
    # locate the lots and wait for the lot script parameter
    tstart = Time.now
    [lot0, lot1, lot2].each {|lot|
      assert_equal 0, @@sv.lot_opelocate(lot, nil, op: @@op), 'error locating lot'
    }
    assert wait_for(timeout: @@job_interval) {@@mq.qdepth == 2}
    assert msgs = @@mq.read_msgs, 'missing MQ msg'
    # verify lot0
    assert msg = msgs.find {|m| m.include?(lot0)}, 'msg not found'
    verify_success(lot0, olotid0, @@costcenter_prime1, @@orgcode_prime, @@srcproduct_prime, msg)
    # verify lot1
    assert msg = msgs.find {|m| m.include?(lot1)}, 'msg not found'
    verify_success(lot1, olotid1, "#{oraclecc1}-#{@@acct_nonprime}", @@orgcode_nonprime, @@srcproduct_nonprime, msg)
    # verify lot2
    @@mq.delete_msgs(nil)
    verify_failure(lot2, 'RawWaferType is empty', "Empty product udata 'RawWaferType'", tstart)
  end

  def test21_prime_slt2   # like test11 with different SLT and costcenter
    assert lot = @@svtest.new_lot(srcproduct: @@srcproduct_prime, sublottype: @@slt2), 'error creating test lot'
    @@testlots << lot
    #
    # locate lot to PD with Udata AutomationType 'OracleIssueOut' but no script parameters set
    assert_equal 0, @@sv.lot_opelocate(lot, nil, op: @@op), 'error locating lot'
    verify_failure(lot, 'OracleLotID is empty', "Script parameter 'OracleLotID' is empty or invalid")
    #
    # set wafer script parameters and release lot hold
    olotid = unique_string
    li = @@sv.lot_info(lot, wafers: true)
    li.wafers.each {|w| assert_equal 0, @@sv.user_parameter_change('Wafer', w.wafer, 'OracleLotID', olotid)}
    assert_equal 0, @@sv.lot_hold_release(lot, @@reason_err)
    verify_success(lot, olotid, @@costcenter_prime2, @@orgcode_prime, @@srcproduct_prime)
  end

  def test31_oracleissueout_done
    assert lot = @@svtest.new_lot(srcproduct: @@srcproduct_prime), 'error creating test lot'
    @@testlots << lot
    #
    # set lot script param OracleIssueOut DONE and locate lot to the PD with Udata AutomationType 'OracleIssueOut'
    assert_equal 0, @@sv.user_parameter_change('Lot', lot, 'OracleIssueOut', 'DONE')
    assert_equal 0, @@sv.lot_opelocate(lot, nil, op: @@op), 'error locating lot'
    $log.info "waiting up to #{@@job_interval} s for the job"
    assert wait_for(timeout: @@job_interval) {
      @@sv.lot_info(lot).op != @@op
    }, 'no gatepass'
    assert_empty @@sv.lot_hold_list(lot), 'unexpected lot hold'
    refute @@mq.delete_msgs(nil), 'unexpected MQ msg'
  end

  def test32_oracleissueout_skip
    assert lot = @@svtest.new_lot(srcproduct: @@srcproduct_prime), 'error creating test lot'
    @@testlots << lot
    #
    # set lot script param OracleIssueOut DONE and locate lot to the PD with Udata AutomationType 'OracleIssueOut'
    assert_equal 0, @@sv.user_parameter_change('Lot', lot, 'OracleIssueOut', 'SKIP')
    assert_equal 0, @@sv.lot_opelocate(lot, nil, op: @@op), 'error locating lot'
    $log.info "waiting up to #{@@job_interval} s for the job"
    assert wait_for(timeout: @@job_interval) {
      @@sv.lot_info(lot).op != @@op
    }, 'no gatepass'
    assert_empty @@sv.lot_hold_list(lot), 'unexpected lot hold'
    refute @@mq.delete_msgs(nil), 'unexpected MQ msg'
  end

  def test33_onhold
    assert lot = @@svtest.new_lot(srcproduct: @@srcproduct_prime), 'error creating test lot'
    @@testlots << lot
    #
    # locate lot to PD with Udata AutomationType 'OracleIssueOut' script parameters set, lot ONHOLD
    olotid = unique_string
    li = @@sv.lot_info(lot, wafers: true)
    li.wafers.each {|w| assert_equal 0, @@sv.user_parameter_change('Wafer', w.wafer, 'OracleLotID', olotid)}
    assert_equal 0, @@sv.lot_hold(lot, 'ENG')
    assert_equal 0, @@sv.lot_opelocate(lot, nil, op: @@op, force: true), 'error locating lot'
    $log.info "waiting #{@@job_interval} s for the job"; sleep @@job_interval
    assert_equal @@op, @@sv.lot_info(lot).op, 'wrong GatePass'
    assert_empty @@sv.lot_hold_list(lot, reason: @@reason_err), 'unexpected lot hold'
    refute @@mq.delete_msgs(nil), 'unexpected MQ msg'
  end

  def test34_many
    assert lot = @@svtest.new_lot(srcproduct: @@srcproduct_prime, nwafers: 25), 'error creating test lot'
    @@testlots << lot
    #
    # locate lot to PD with Udata AutomationType 'OracleIssueOut' but no script parameters set
    assert_equal 0, @@sv.lot_opelocate(lot, nil, op: @@op), 'error locating lot'
    verify_failure(lot, 'OracleLotID is empty', "Script parameter 'OracleLotID' is empty or invalid")
    #
    # now there is enough time to prepare the child lots
    olotid = unique_string
    li = @@sv.lot_info(lot, wafers: true)
    li.wafers.each {|w| assert_equal 0, @@sv.user_parameter_change('Wafer', w.wafer, 'OracleLotID', olotid)}
    assert_equal 0, @@sv.lot_hold_release(lot, @@reason_err)
    assert_equal 0, @@sv.lot_opelocate(lot, nil, op: @@op), 'error locating lot'
    assert lcs = 24.times.collect {@@sv.lot_split(lot, 1)}, 'error splitting lot'
    $log.info "waiting up to #{@@job_interval} s for the job"
    assert wait_for(timeout: @@job_interval) {
      @@sv.user_parameter('Lot', lot, name: 'OracleIssueOut').value == 'DONE'
    }, 'lot script parameter OracleIssueOut not set'
    sleep 10
    lcs.each {|lc|
      @@sv.user_parameter('Lot', lc, name: 'OracleIssueOut').value == 'DONE'
    }
  end

  def test41_bop
    assert @@svtest_src = SiView::Test.new(@@env_src, @@sv_defaults_src)
    @@sv_src = @@svtest_src.sv
    assert lot = @@svtest_src.new_lot(srcproduct: @@srcproduct_prime), 'error creating test lot'
    @@testlots_bop << lot
    assert @@sv_src.backup_prepare_send_receive(lot, @@sv, @@psend_src), 'error receiving backup lot'
    params = {product: @@svtest.product, route: @@svtest.route,
      opNo: @@sv.route_operations(@@svtest.route, count: 1).first.operationNumber}
    assert_equal 0, @@sv.schdl_change(lot, params)
    #
    # locate lot to the PD with Udata AutomationType 'OracleIssueOut', no script parameters set
    assert_equal 0, @@sv.lot_opelocate(lot, nil, op: @@op), 'error locating lot'
    $log.info "waiting up to #{@@job_interval} s for the job"
    assert wait_for(timeout: @@job_interval) {
      @@sv.lot_info(lot).op != @@op
    }, 'no gatepass'
    assert_empty @@sv.lot_hold_list(lot), 'unexpected lot hold'
    refute @@mq.delete_msgs(nil), 'unexpected MQ msg'
  end


  # aux methods

  def verify_failure(lot, holdmemo, evmsg, tstart= Time.now)
    $log.info "waiting up to #{@@job_interval} s for the job"
    # wait for lot hold
    hold = nil
    assert wait_for(timeout: @@job_interval) {
      hold = @@sv.lot_hold_list(lot, reason: @@reason_err).first
    }, 'lot is not ONHOLD'
    $log.info "verifying hold claim memo: #{holdmemo.inspect}"
    assert_equal holdmemo, hold.memo, 'wrong hold claim memo'
    ev = nil
    # wait for notification
    assert wait_for(timeout: 180) {
      ev = @@notifications.query(tstart: tstart, service: '%jCAP_AsyncNotification%',
        filter: {'Application'=>'IssueOutProcess', 'LotID'=>lot}
      ).first
    }, 'no notification'
    $log.info "verifying notification: #{evmsg.inspect}"
    assert_equal evmsg, ev['Message'], 'wrong notification'
    # verify no GatePass
    $log.info 'verifying no GatePass'
    assert_equal @@op, @@sv.lot_info(lot).op, 'wrong GatePass'
    # verify no MQ message is sent
    $log.info 'verifying no MQ message was sent'
    refute @@mq.delete_msgs(nil), 'unexpected MQ msg'
    $log.info 'verifying OracleIssueOut is not set'
    refute_equal 'DONE', @@sv.user_parameter('Lot', lot, name: 'OracleIssueOut').value, 'wrong value for OracleIssueOut'
  end

  def verify_success(lot, olotid, costcenter, orgcode, itemcode, msg=nil)
    $log.info "waiting up to #{@@job_interval} s for the job"
    assert wait_for(timeout: @@job_interval) {
      @@sv.user_parameter('Lot', lot, name: 'OracleIssueOut').value == 'DONE'
    }, 'lot script parameter OracleIssueOut not set'
    $log.info 'verifying OracleIssueOut is DONE'
    # verify GatePass
    $log.info 'verifying GatePass'
    refute_equal @@op, @@sv.lot_info(lot).op, 'no GatePass'
    # verify MQ msg
    if msg.nil?
      $log.info 'verifying MQ message'
      assert_equal 1, @@mq.qdepth, 'wrong number of MQ messages'
      assert msg = @@mq.read_msg, 'missing MQ msg'
    end
    li = @@sv.lot_info(lot)
    h = XMLHash.xml_to_hash(msg)
    assert ldata = h[:AWCA_MSG][:LOT_MO_MAPS][:LOT_MO_MAP]
    assert_equal olotid, ldata[:ORCL_LOT_NUM], 'wrong ORCL_LOT_NUM'
    assert_equal li.nwafers.to_s, ldata[:WAFER_QTY], 'wrong WAFER_QTY'
    assert_equal "#{lot}_#{li.product}", ldata[:DEST_PROD],'wrong DEST_PROD'
    assert_equal costcenter, ldata[:COST_CENTER], 'wrong COST_CENTER'
    assert_equal orgcode, ldata[:ORG_CODE], 'wrong ORG_CODE'
    assert_equal itemcode.split('.').first, ldata[:ORCL_ITEM_CODE], 'wrong ORCL_ITEM_CODE'
    assert_equal @@subinvcode, ldata[:ORCL_SUB_INV_CODE], 'wrong ORCL_SUB_INV_CODE'
  end

end
