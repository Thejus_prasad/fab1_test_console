=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.


Author: Steffen Frieske, 2015-12-10

Version: 18.09

History:
  2017-01-05 sfrieske, use new  RTD::EmuRemote, minor cleanup
  2018-11-07 sfrieske, minor cleanup
  2019-02-19 sfrieske, improved EmuRemote usage

Notes:
  This job periodically calls RTD and just acts on the response, no logic contained.
  actions: nonpro bankin, bankout, bankmove, gatepass
=end

require 'rtdserver/rtdemuremote'
require 'SiViewTestCase'


class JCAP_GatePdBTF < SiViewTestCase
  @@nonprobank1 = 'O-TOWING'
  @@nonprobank2 = 'UT-NONPROD'
  @@rtd_rule = 'JCAP_GatePD_BTF'
  @@columns = %w(lot_id pd_id prodspec_id mainpd_id jCAP_scenario jCAP_LotOpeNote jCAP_src_BankID jCAP_dest_BankID jCAP_HoldReason)
  # @@rtd_category = 'DISPATCHER/JCAP'
  @@job_interval = 330  # 5 minutes


  def self.shutdown
    @@rtdremote.delete_emustation('', rule: @@rtd_rule)
  end

  def self.after_all_passed
    @@testlots.each {|lot| @@sv.delete_lot_family(lot)}
  end


  def test10
    @@rtdremote = RTD::EmuRemote.new($env)
    @@rtdremote.delete_emustation('', rule: @@rtd_rule)
    # prepare lots for bankout and bank move
    assert @@testlots = @@svtest.new_lots(4), 'error creating test lots'
    tstart = Time.now
    @@testlots[2..3].each {|lot| assert_equal 0, @@sv.lot_nonprobankin(lot, @@nonprobank1)}
    #
    # set RTDEmu data
    rtdnote = "QA Test #{Time.now.utc}"
    lis = @@sv.lots_info(@@testlots)
    data = []
    li = lis[0]
    data << [li.lot, li.op, li.product, li.route, 'GatePass', rtdnote, '', '', '']
    li = lis[1]
    data << [li.lot, li.op, li.product, li.route, 'NonProdBankIn', rtdnote, '', @@nonprobank1, '']
    li = lis[2]
    data << [li.lot, li.op, li.product, li.route, 'NonProdBankOut', rtdnote, @@nonprobank1, '', '']
    li = lis[3]
    data << [li.lot, li.op, li.product, li.route, 'NonProdBankMove', rtdnote, @@nonprobank1, @@nonprobank2, '']
    assert @@rtdremote.add_emustation_wait('', rule: @@rtd_rule, timeout: @@job_interval) {
      @@rtdremote.create_response(@@columns, {'rows'=>data})
    }, 'rule has not been called'
    # verify lot status
    sleep 10
    refute_equal lis[0].opNo, @@sv.lot_info(@@testlots[0]).opNo, "lot0 has not been gatepassed"
    assert_equal lis[1].opNo, @@sv.lot_info(@@testlots[1]).opNo, "lot1 has been gatepassed"
    assert_equal @@nonprobank1, @@sv.lot_info(@@testlots[1]).bank, "lot1 is not banked in"
    assert_equal lis[2].opNo, @@sv.lot_info(@@testlots[2]).opNo, "lot2 has been gatepassed"
    assert_equal '', @@sv.lot_info(@@testlots[2]).bank, "lot2 is not banked out"
    assert_equal lis[3].opNo, @@sv.lot_info(@@testlots[3]).opNo, "lot3 has been gatepassed"
    assert_equal @@nonprobank2, @@sv.lot_info(@@testlots[3]).bank, "lot3 has not been moved to other bank"
    #
    # verify lot note
    @@testlots.each {|lot|
      assert_equal rtdnote, @@sv.lot_openotes(lot).first.note, "wrong note for lot #{lot}"
    }
  end

end
