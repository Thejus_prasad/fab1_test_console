=begin 
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.
    
Author: Steffen Steidten, 2011-05-13

Version: 19.01

History:
  2015-03-26 ssteidte, cleanup test preparation
  2019-01-23 sfrieske, minor cleanup
=end

require 'SiViewTestCase'


class JCAP_AutoBankIn < SiViewTestCase
  @@sv_defaults = {product: 'ITDC-JCAP-TEST.01', route: 'ITDC-JCAP-TEST.01'}
  @@route_branch = 'ITDC-JCAP-AUTOBANKIN-DYNAMICBRANCH.01'
  @@route_erf = 'ITDC-JCAP-AUTOBANKIN-ERFBRANCH.01'
  @@opno_split = '4000.1000'
  @@opno_merge = '4000.1100'
  
  @@holdreason = 'X-M5'
  @@holdmemo = 'No bank defined. Lot on hold by jCAP AutoBankIn.'
  
  @@testlots = []
  @@job_interval = 310
  
  
  def self.after_all_passed
    @@testlots.each {|lot| @@sv.delete_lot_family(lot)}
  end

  def test00_setup
    $setup_ok = false
    #
    assert @@testlots = @@svtest.new_lots(5), 'error creating test lots'
    # prepare lot1 for bankin
    lot = @@testlots[0]
    assert_equal 0, @@sv.lot_opelocate(lot, 'last')
    assert_equal 0, @@sv.lot_bankin_cancel(lot)
    # set future hold for lot2, locate to next to last op and gatepass
    lot = @@testlots[1]
    ops = @@sv.lot_operation_list(lot)
    opNo = ops[-2].opNo
    assert_equal 0, @@sv.lot_futurehold(lot, opNo, nil, post: true)
    assert_equal 0, @@sv.lot_opelocate(lot, opNo)
    assert_equal 0, @@sv.lot_gatepass(lot)
    assert_equal 0, @@sv.lot_hold_release(lot, nil)
    # locate lot3 to the dynamic branch route
    lot = @@testlots[2]
    assert_equal 0, @@sv.lot_branch(lot, @@route_branch, dynamic: true)
    # locate lot4 to the ERF route
    lot = @@testlots[3]
    li = @@sv.lot_info(lot)
    assert_equal 0, @@sv.lot_experiment(lot, 2, @@route_erf, @@opno_split, @@opno_split, merge: @@opno_merge)
    assert_equal 0, @@sv.lot_opelocate(lot, @@opno_split)
    # move lot5 to a FOSB and locate to bankin pd
    lot = @@testlots[4]
    assert_equal 0, @@sv.lot_opelocate(lot, :last)
    assert_equal 0, @@sv.lot_bankin_cancel(lot)
    assert_equal 0, @@sv.wafer_sort(lot, '')
    #
    $log.info "waiting #{@@job_interval} s for the jCAP job to run"
    sleep @@job_interval
    #
    $setup_ok = true
  end
  
  
  def test11_bankin
    lot = @@testlots[0]
    li = @@sv.lot_info(lot)
    assert_equal 'COMPLETED', li.status, "wrong status of lot #{lot}"
    assert_equal 'InBank', li.states['Lot Inventory State'], "wrong status of lot #{lot}"
    assert_equal 'NOTONHOLD', li.states['Lot Hold State'], "wrong status of lot #{lot}"
  end
  
  def test12_futurehold_bankin
    lot = @@testlots[1]
    li = @@sv.lot_info(lot)
    assert_equal 'COMPLETED', li.status, "wrong status of lot #{lot}"
    assert_equal 'InBank', li.states['Lot Inventory State'], "wrong status of lot #{lot}"
    assert_equal 'NOTONHOLD', li.states['Lot Hold State'], "wrong status of lot #{lot}"
  end
  
  def test13_dynamic_branch
    lot = @@testlots[2]
    # get holds
    lh = @@sv.lot_hold_list(lot, reason: @@holdreason)
    assert_equal 1, lh.size, "child lot #{lot} not ONHOLD"
    assert_equal @@holdmemo, lh[0].memo
  end
  
  def test14_erf
    # get child lot
    lots = @@sv.lot_family(@@testlots[3])
    assert_equal 2, lots.size, 'ERF execution failed'
    lot = lots.last
    # get holds
    lh = @@sv.lot_hold_list(lot, reason: @@holdreason)
    assert_equal 1, lh.size, "child lot #{lot} not ONHOLD"
    assert_equal @@holdmemo, lh[0].memo
  end
  
  def test15_nocarrier
    lot = @@testlots[4]
    li = @@sv.lot_info(lot)
    assert_equal 'Waiting', li.status, "wrong status of lot #{lot}"
    assert_equal 'NOTONHOLD', li.states["Lot Hold State"], "wrong status of lot #{lot}"
  end
end
