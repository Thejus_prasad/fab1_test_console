=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: Steffen Steidten, 2011-10-20

Version: 20.07

History:
  2013-12-11 ssteidte, added automated tests for LEADINGORDER
  2014-08-26 ssteidte, simplified setup by using Setup.FC
  2015-01-26 ssteidte, maintenance for manual tests 30x
  2015-07-06 ssteidte, updated testcases for v15.06
  2015-07-06 sfrieske, updated testcases for v15.10 (test18x)
  2015-10-22 sfrieske, added TC147
  2017-01-30 sfrieske, changed expected messages only (v16.11)
  2017-04-07 sfrieske, added job timeout tests 201 and 202
  2017-11-08 sfrieske, added grouping tests
  2018-11-07 sfrieske, minor cleanup
  2019-02-19 sfrieske, improved EmuRemote usage
  2019-08-07 sfrieske, added TC108 Engineering kit 'Cancel'
  2020-04-27 sfrieske, added TCs 113 and 144 for No LeadingOrder
  2020-08-02 sfrieske, added tests 2xx for Single Wafer Chamber Qual (SWC)
  2021-04-21 sfrieske, moved @sorter and @pg form SiView::Test to SJC::Test
  2021-08-27 sfrieske, require uniquestring instead of misc

Notes:
  Requirements:
  * setup.FC up and running, SPECs created to match the test cases
    http://vf1sfcd01:10080/setupfc-rest/v1/specs/C07-00000766/content?format=html
    http://vf1sfcd01:10080/setupfc-rest/v1/specs/C07-00003716/content?format=html
  * Carriers TQ* with wafers at correct positions, see test sheet
  * SJC up and running
  * WBTQ needs to be reconfigured to write SAP messages to a QA test queue
  Monitoring: http://f36iesd1:8000/reporting/wbtq_monitor_status.jsp
=end

# require 'jcap/mdswritesvc'
require 'jcap/wbtqtest'
require 'rtdserver/rtdemuremote'
require 'util/uniquestring'
require 'RubyTestCase'


class JCAP_WBTQ < RubyTestCase
  @@sv_defaults = {}
  @@sorter = 'UTS001'
  @@pg = 'PG1'
  @@order = '000013370815'
  @@spec = 'C07-00000766'
  @@rawbank = 'WBTQ-DISPO'
  # regular build product 2nd source
  @@product1_2nd_source = 'B-400-CLUSTER.01'
  # build product for a use product with a route having a start bank not allowing STB
  @@product_wrong_bank = 'B-600-CLUSTER.01'   # would build 'U-600-CLUSTER-QASAPCLUSTER.01'
  # no build product (for all kits except srcprio 21-24), but exists in SiView
  @@product_wrong = 'B-200-CLUSTER.01'
  # unused but valid product for creating other kits
  @@product_unused = 'U-200-CLUSTER-QASAPCLUSTERCHBR.01' #'U-300-CLUSTER-QASAPCLUSTER.01'
  # eqp Module2 (location precedence and manual tests)
  @@eqp2 = 'QACLUST02'
  @@chamber2 = 'CH1'
  @@stocker2 = 'QASTOM2'
  # eqp ANNEX
  @@eqp_anx = 'QACLUST03'
  @@chamber_anx = 'CH1'
  @@stocker_anx = 'QASTOANX'
  # eqp for special test cases
  @@eqp_for_slr = 'A-DUMMY'
  # eqp and stocker Location UDATA
  @@loc1 = {'Location'=>'QAM1-X1-Y2'}
  @@loc2 = {'Location'=>'QAM2-X3-Y4'}
  @@loc3 = {'Location'=>'ANX'}
  # carriers
  # full carrier, default
  @@carrier = 'TQ01'
  # 5 wafers in correct slots for most kits
  @@carrier_prebuilt = 'TQ02'
  # empty carrier for executing SRs, needs to be cleaned up!
  @@carrier_sr = 'TQ04'
  # 5 empty carriers, will be emptied as part of test preparation
  @@carriers_empty = ['TQ05', 'TQ06', 'TQ07', 'TQ08', 'TQ09']
  # RTD rule
  @@empty_rule = 'EMPTY'  # as configured in sjc.properties for sjc.rtd.empty.rule
  @@columns = %w(cast_id cast_category stk_id)  # SJC uses the first column w/o checking the name
  # Single Wafer Chamber Qual (SWC)
  @@spec_swc = 'C07-00003716'
  @@eqp_swc = 'QAC003'

  @@timeout = 120           # MQ response timeout
  @@mds_sync = 35
  @@use_notifications = true
  @@jobtimeout = 600        # from wbtq.properties transaction.timeout
  @@testlots = []


  def self.startup
    super
    @@rtdremote = RTD::EmuRemote.new($env)
    @@wbtqtest = WBTQ::Test.new($env, @@sv_defaults.merge(sv: @@sv, carriers: 'TQ%', carrier_category: 'BEOL', 
      sorter: @@sorter, pg: @@pg, notifications: @@use_notifications,
      order: @@order, spec: @@spec, rawbank: @@rawbank, mds_sync: @@mds_sync, timeout: @@timeout))
    ##@@wbtqtest.kits.each_key {|k| @@wbtqtest.kits[k].delete('QA-KIT16')}  # temporarily disable a kit, issue 2017-01-25 09:28:25
  end

  def self.shutdown
    @@rtdremote.delete_emustation(@@empty_rule)
    @@wbtqtest.cleanup_eqp(@@sorter, mode: 'Off-Line-1')
    @@wbtqtest.prepare_resources  # make all carriers NOTAVAILABLE, remove routes' pre-1 sleep
    @@wbtqtest.cancel_kit_verify('QA%')
    @@testlots.each {|lot| @@sv.delete_lot_family(lot)}
  end


  def setup
    # cleanup queues
    @@wbtqtest.client.mq_response.delete_msgs(nil)
    # move sorted wafers back into the default carrier
    unless @@sv.carrier_status(@@carrier_sr).lots.empty?
      @@sv.carrier_status_change(@@carrier, 'AVAILABLE', check: true)
      @@sv.lot_list_incassette(@@carrier_sr).each {|lot| @@sv.wafer_sort(lot, @@carrier, slots: 'empty')}
      @@sv.carrier_status_change(@@carrier, 'NOTAVAILABLE')
    end
  end

  def teardown
    @@rtdremote.delete_emustation(@@empty_rule)
  end


  def test00_setup
    $setup_ok = false
    #
    # ensure WBTQ is configured to use the test (not SAP) response queue
    res = @@wbtqtest.client.stb_trigger('QAQA', timeout: 60)
    assert @@wbtqtest.verify_response(res, '200', 'No kit context'), 'wrong WBTQ response queue configured?'
    # prepare full carrier
    @@sv.carrier_status(@@carrier).lots.each {|lot| assert @@sv.delete_lot_family(lot)}
    assert @@wbtqtest.prepare_src_carriers(@@carrier, {@@wbtqtest.srcproduct=>(1..25).to_a})
    # prepare prebuilt carrier
    @@sv.carrier_status(@@carrier_prebuilt).lots.each {|lot| assert @@sv.delete_lot_family(lot)}
    assert @@wbtqtest.prepare_src_carriers(@@carrier_prebuilt, @@wbtqtest.kits[:stbonly_prebuilt].first, no_other_wafers: true)
    # empty the SR target carrier
    @@sv.carrier_status(@@carrier_sr).lots.each {|lot| assert @@sv.delete_lot_family(lot)}
    # ensure locations are set correctly
    assert @@wbtqtest.stocker_location(@@wbtqtest.stocker, @@loc1), 'error setting UDATA'
    assert @@wbtqtest.eqp_location(@@wbtqtest.eqp, @@loc1), 'error setting UDATA'
    assert @@wbtqtest.stocker_location(@@stocker2, @@loc2), 'error setting UDATA'
    assert @@wbtqtest.eqp_location(@@eqp2, @@loc2), 'error setting UDATA'
    assert @@wbtqtest.stocker_location(@@stocker_anx, @@loc3), 'error setting UDATA'
    assert @@wbtqtest.eqp_location(@@eqp_anx, @@loc3), 'error setting UDATA'
    #
    [@@wbtqtest.eqp, @@eqp2, @@eqp_anx, @@eqp_swc].each {|eqp| @@sv.eqp_cleanup(eqp)}
    #
    # assert @@mdswritesvc = MdsWriteService.new($env)
    #
    $setup_ok = true
  end

  def testman011
    # don't stop further testing if the next test with SR execution fails
    kit = "QA-KIT13"
    assert @@wbtqtest.cleanup_eqp(@@sorter, mode: 'Auto-3', portstatus: 'UnloadReq'), 'error cleaning up sorter'
    # set RTD rule and build the kit
    @@rtdremote.add_emustation(@@empty_rule) {@@rtdremote.create_response(@@columns, {'rows'=>[[@@carrier_sr, '', '']]})}
    assert @@wbtqtest.build_kit_verify(kit, @@carrier, new_carrier: @@carrier_sr), 'error building kit with SR execution'
  end

  # for test case development only
  def testman010_smoke
     kit = 'QA-KIT03'  # @@wbtqtest.kits[:stbonly_other][0]
     assert @@wbtqtest.build_kit_verify(kit, @@carrier)
  end


  # stb_only, happy scenario
  def test101_stb_pass
    $setup_ok = false
    #
    kits = @@wbtqtest.kits[:stbonly_other]
    kits.each_with_index {|kit, i|
      $log.info "\n-- kit #{kit}  #{i+1}/#{kits.size}"
      assert @@wbtqtest.stb_only_verify(kit, @@carrier)
    }
    #
    $setup_ok = true
  end

  # stb_only, disturbing carrier
  # issue in 15.10 for a kit with fixed slots, other wafers allowed -> does not return
  #   1st carrier with 1 src wafer + others, 2nd carrier with enough wafers
  def test102_stb_pass_disturbing_carrier
    assert lots = @@wbtqtest.prepare_src_carriers(@@carriers_empty.first, [{'B-100-CLUSTER.01'=>1}])
    @@testlots += lots
    failed = []
    kits = @@wbtqtest.kits[:stbonly_other]
    kits.each_with_index {|kit, i|
      $log.info "\n-- kit #{kit}  #{i+1}/#{kits.size}"
      ok = @@wbtqtest.stb_only_verify(kit, @@carrier, available_carriers: @@carriers_empty.first)
      failed << kit unless ok
    }
    assert_empty failed, 'failed kits'
  end

  # build_kit and stb_trigger, happy scenario
  def test103_build_kit_nosr
    $setup_ok = false
    #
    kits = @@wbtqtest.kits[:build_nosr]
    kits.each_with_index {|kit, i|
      $log.info "\n-- kit #{kit}  #{i+1}/#{kits.size}"
      assert @@wbtqtest.build_kit_verify(kit, @@carrier)
    }
    #
    $setup_ok = true
  end

  # build_kit and stb_trigger, disturbing carrier
  def test104_build_kit_nosr_disturbing_carrier
    assert lots = @@wbtqtest.prepare_src_carriers(@@carriers_empty.first, [{'B-100-CLUSTER.01'=>1}])
    @@testlots += lots
    failed = []
    kits = @@wbtqtest.kits[:build_nosr]
    kits.each_with_index {|kit, i|
      $log.info "\n-- kit #{kit}  #{i+1}/#{kits.size}"
      ok = @@wbtqtest.build_kit_verify(kit, @@carrier, available_carriers: @@carriers_empty.first)
      failed << kit unless ok
    }
    assert_empty failed, 'failed kits'
  end

  # build_kit, happy scenario; SR execution and stb_trigger are tested only once
  def test105_build_kit_sr
    $setup_ok = false
    #
    kits = @@wbtqtest.kits[:build_sr]
    kits[0..-2].each_with_index {|kit, i|
      $log.info "\n-- kit #{kit}  #{i+1}/#{kits.size}"
      assert @@wbtqtest.build_kit_verify(kit, @@carrier, stbtrigger: false)
    }
    #
    $setup_ok = true
    #
    # don't stop further testing if the next test with SR execution fails
    kit = kits.last
    $log.info "\n-- kit #{kit} #{kits.size}/#{kits.size} with SR execution"
    assert @@wbtqtest.cleanup_eqp(@@sorter, mode: 'Auto-3', portstatus: 'UnloadReq'), 'error cleaning up sorter'
    # set RTD rule and build the kit
    @@rtdremote.add_emustation(@@empty_rule) {@@rtdremote.create_response(@@columns, {'rows'=>[[@@carrier_sr, '', '']]})}
    assert @@wbtqtest.build_kit_verify(kit, @@carrier, new_carrier: @@carrier_sr), 'error building kit with SR execution'
  end

  # build_kit, disturbing carrier; SR execution and stb_trigger are not tested
  def test106_build_kit_sr_disturbing_carrier
    assert lots = @@wbtqtest.prepare_src_carriers(@@carriers_empty.first, [{'B-100-CLUSTER.01'=>1}])
    @@testlots += lots
    failed = []
    kits = @@wbtqtest.kits[:build_sr]
    kits.each_with_index {|kit, i|
      $log.info "\n-- kit #{kit}  #{i+1}/#{kits.size}"
      ok = @@wbtqtest.build_kit_verify(kit, @@carrier, stbtrigger: false, available_carriers: @@carriers_empty.first)
      failed << kit unless ok
    }
    assert_empty failed, 'failed kits'
  end

  # build_kit from a source carrier where wafers are not in the final positions -> SR
  # only available carrier has the src wafers in the wrong slots
  def test107_build_kit_srcwafers_not_matching_slots
    carrier = @@carriers_empty.first
    assert lots = @@wbtqtest.prepare_src_carriers(carrier, {@@wbtqtest.srcproduct=>[21, 22, 23, 24, 25]})
    @@testlots += lots
    # build kits requiring a SR with @@carrier but no new carrier, carrier is filled with other wafers (QA-KIT02, 05)
    refute_empty kits = @@wbtqtest.sfc.select_kits(eqp: @@wbtqtest.eqp,
      match_slots: true, new_carrier: false, allow_other_wafers: false) - @@wbtqtest.specialkits, 'not kits found'
    kits.each_with_index {|kit, i|
      $log.info "\n-- kit #{kit}  #{i+1}/#{kits.size} (with other wafers in carrier)"
      assert @@wbtqtest.build_kit_verify(kit, carrier, stbtrigger: false)
    }
    # build kits again with no other wafers in the carrier (issue in v15.06)
    assert_equal 0, @@sv.delete_lot_family(lots.first)
    kits.each_with_index {|kit, i|
      $log.info "\n-- kit #{kit}  #{i+1}/#{kits.size} (no other wafers in carrier)"
      assert @@wbtqtest.build_kit_verify(kit, carrier, stbtrigger: false)
    }
  end

  # verify cancel_kit cleans up the lots' bank holds
  def test108_cancel_kit_bankholds
    carrier = @@carrier_prebuilt
    assert kit = @@wbtqtest.kits[:build_nosr].first   # 'QA-KIT03'
    assert guid = @@wbtqtest.build_kit_verify(kit, carrier, stbtrigger: false), 'build_kit failed'
    assert @@wbtqtest.cancel_kit_verify(guid), "error canceling kit #{guid}"
    @@wbtqtest.kitcarrier_info(carrier).each {|e| assert_equal 'COMPLETED', e.status}
  end

  # verify cancel_kit cleans up the carriers' xfer user
  def test109_xfer_reservation
    carrier = @@carrier_prebuilt
    assert kit = @@wbtqtest.kits[:build_nosr].first   # 'QA-KIT03'
    assert guid = @@wbtqtest.build_kit_verify(kit, carrier, stbtrigger: false), 'build_kit failed'
    # remove one lot's bank hold and send stb_trigger (failing)
    kci = @@wbtqtest.kitcarrier_info(carrier)
    assert_equal 0, @@sv.lot_bankhold_release(kci.first.lot)
    res = @@wbtqtest.client.stb_trigger(guid)
    assert @@wbtqtest.verify_response(res, '888', 'Could not release bank hold for lot')
    # verify xfer_user is not set
    assert_empty @@sv.carrier_status(carrier).xfer_user
  end

  # WBTQ fix pending
  def testdevel110_parallel
    kit = @@wbtqtest.kits[:stbonly_other].first
    guids = []
    2.times {|i|
      Thread.new {guids[i] = @@wbtqtest.stb_only(kit, nil, nil)}
    }
  end

  def test111_stb_notonroute
    kit = @@wbtqtest.kits[:stbonly_other].first   # 'QA-KIT03'
    # prepare notonroute srclot
    # uproduct = @@wbtqtest.sfc.kit_slots(name: kit).keys.first
    uproduct = @@wbtqtest.sfc.kit_info(kit).lot_definitions.first.product
    assert @@wbtqtest.stb_only_verify(kit, @@carrier) {
      @@wbtqtest.rework_monitor_lots(@@carrier, product: uproduct)
      !@@sv.stb_cancel(@@sv.lot_list_incassette(@@carrier).first).nil?
    }
  end

  def test112_build_kit_notonroute
    kit = @@wbtqtest.kits[:build_nosr].first   # 'QA-KIT03'
    # prepare notonroute srclot
    # uproduct = @@wbtqtest.sfc.kit_slots(name: kit).keys.first
    uproduct = @@wbtqtest.sfc.kit_info(kit).lot_definitions.first.product
    assert @@wbtqtest.build_kit_verify(kit, @@carrier) {
      @@wbtqtest.rework_monitor_lots(@@carrier, product: uproduct)
      !@@sv.stb_cancel(@@sv.lot_list_incassette(@@carrier).first).nil?
    }
  end

  # like stb_only_verify, but no leading order
  def test113_stb_no_leadingorder
    kit = @@wbtqtest.kits[:stbonly_other].first   # 'QA-KIT03'
    assert @@wbtqtest.stb_only_verify(kit, @@carrier, leadingorder: nil)
  end

  def test114_build_kit_no_leading_order
    kit = @@wbtqtest.kits[:build_nosr].first   # 'QA-KIT03'
    assert @@wbtqtest.build_kit_verify(kit, @@carrier, leadingorder: nil)
  end

  # wrong parameters

  # try stb_only and build_kit with an invalid spec
  def test121_invalid_spec
    assert kit = @@wbtqtest.kits[:invalid_spec].first, 'no invalid kit found'
    assert guid = @@wbtqtest.stb_only(kit, '204', '2 matching kit definitions')
    assert @@wbtqtest.verify_kit(guid, 'READ_SPEC', 'NO_KIT')
    assert guid = @@wbtqtest.build_kit(kit, '204', '2 matching kit definitions')
    assert @@wbtqtest.verify_kit(guid, 'READ_SPEC', 'NO_KIT')
  end

  # build_kit, sorting required, wrong chamber requested
  def test122_wrong_chamber
    assert kit = @@wbtqtest.kits[:build_nosr].first
    assert guid = @@wbtqtest.stb_only(kit, '204', "chamberId='CHNOTEX'] not valid", chamber: 'CHNOTEX')
    assert @@wbtqtest.verify_kit(guid, 'READ_SPEC', 'NO_KIT')
    assert guid = @@wbtqtest.build_kit(kit, '204', "chamberId='CHNOTEX'] not valid", chamber: 'CHNOTEX')
    assert @@wbtqtest.verify_kit(guid, 'READ_SPEC', 'NO_KIT')
  end

  # stb_only, kit spec requires a new carrier
  def test123_stb_new_carrier
    assert kit = @@wbtqtest.kits[:new_carrier].first, 'no new_carrier kit found'
    assert guid = @@wbtqtest.stb_only(kit, '204', 'Having a specification requiring an empty carrier is senseless in STB only')
    assert @@wbtqtest.verify_kit(guid, 'READ_SPEC', 'NO_KIT')
  end

  # application crashed in v 15.06
  def test124_1wrong_source
    assert @@wbtqtest.prepare_resources
    kit = 'QA-KIT06' #@@wbtqtest.kits[:build_nosr][0]
    # pslots = @@wbtqtest.sfc.kit_slots(name: kit, eqp: @@wbtqtest.eqp, srcproduct: 0)
    # pslots[@@product_wrong] = [pslots[pslots.keys.first].pop]  # one wafer with wrong srcproduct
    kinfo = @@wbtqtest.sfc.kit_info(kit)
    slots = kinfo.kit_slots.values.flatten.sort
    pslots = {}
    pslots[kinfo.lot_definitions.first.src_pattern.first] = slots[0..-2]
    pslots[@@product_wrong] = slots[-1..-1]    # one wafer with wrong srcproduct
    assert lots = @@wbtqtest.prepare_src_carriers(@@carriers_empty.first, pslots)
    @@testlots += lots
    #
    $log.info "waiting #{@@mds_sync} s for the MDS to sync"; sleep @@mds_sync
    assert guid = @@wbtqtest.build_kit(kit, '208', 'Not enough source wafer available.')
  end


  # carrier in use

  # build_kit, sorting required, carrier has NPW reservation
  def test131_build_kit_npw_reserve
    assert @@wbtqtest.prepare_resources(@@carrier)
    # make an NPW reservation with one wafer/lot
    kci = @@wbtqtest.kitcarrier_info(@@carrier)
    assert_equal 0, @@sv.eqp_status_change(@@wbtqtest.eqp, '2WPR')
    assert_equal 0, @@sv.npw_reserve(@@wbtqtest.eqp, nil, @@carrier, lot: kci.first.lot)
    $log.info "waiting #{@@mds_sync} s for the MDS to sync"; sleep @@mds_sync
    #
    # build_kit request
    assert guid = @@wbtqtest.build_kit(@@wbtqtest.kits[:build_sr].first, '208', 'Not enough source wafer available.')
    assert @@wbtqtest.verify_kit(guid, 'BUILD_KIT', 'NOT_ENOUGH_WAFER')
  end

  # build_kit, sorting required, carrier has SLR reservation
  def test132_build_kit_slr   ## sst: use other wafer, not part of the kit
    assert @@wbtqtest.prepare_resources(@@carrier)
    # create a use-product lot to make an SLR with
    kci = @@wbtqtest.kitcarrier_info(@@carrier)
    assert kit = @@wbtqtest.kits[:build_sr].first
    kslots = @@wbtqtest.sfc.kit_info(kit, @@wbtqtest.eqp).kit_slots
    assert lot = @@sv.stb_controllot('Equipment Monitor', kslots.keys.first, kci.last.lot, nwafers: 1), 'error creating lot for SLR'
    # make an SLR
    assert_equal 0, @@sv.eqp_cleanup(@@eqp_for_slr)
    assert @@sv.slr(@@eqp_for_slr, lot: lot), "error making SLR"
    $log.info "waiting #{@@mds_sync} s for the MDS to sync"; sleep @@mds_sync
    #
    # build_kit request
    assert guid = @@wbtqtest.build_kit(kit, '208', 'Not enough source wafer available.')
    assert @@wbtqtest.verify_kit(guid, 'BUILD_KIT', 'NOT_ENOUGH_WAFER')
    #
    # clean up
    @@sv.eqp_cleanup(@@eqp_for_slr)
  end

  # build_kit, sorting required, carrier is in a sorter job
  def test133_build_kit_sj   ## sst: use other wafer, not part of the kit
    assert @@wbtqtest.cleanup_eqp(@@sorter, mode: 'Auto-3', portstatus: 'UnloadReq'), 'error cleaning up sorter'
    assert @@wbtqtest.prepare_resources([@@carrier, @@carrier_sr])
    # create a sorter job
    kci = @@wbtqtest.kitcarrier_info(@@carrier)
    lot = kci.first.lot
    assert @@sv.sj_create(@@sorter, 'P1', 'P2', lot, @@carrier_sr), 'error creating sorter job'
    $log.info "waiting #{@@mds_sync} s for the MDS to sync"; sleep @@mds_sync
    #
    # build_kit request
    assert guid = @@wbtqtest.build_kit(@@wbtqtest.kits[:build_sr].first, '208', 'Not enough source wafer available.')
    assert @@wbtqtest.verify_kit(guid, 'BUILD_KIT', 'NOT_ENOUGH_WAFER')
  end

  # build_kit, sorting required, carrier is in a sort request
  def test134_build_kit_sr
    assert @@wbtqtest.cleanup_eqp(@@sorter, mode: 'Auto-3', portstatus: 'UnloadReq'), 'error cleaning up sorter'
    assert @@wbtqtest.prepare_resources([@@carrier, @@carrier_sr])
    $log.info "waiting #{@@mds_sync} s for the MDS to sync"; sleep @@mds_sync
    # create a sort request
    assert @@wbtqtest.create_combine_verify(@@carrier, @@carrier_sr), 'error creating COMBINE request'
    $log.info "waiting #{@@mds_sync} s for the MDS to sync"; sleep @@mds_sync
    #
    # build_kit request
    assert guid = @@wbtqtest.build_kit(@@wbtqtest.kits[:build_sr].first, '208', 'Not enough source wafer available.')
    assert @@wbtqtest.verify_kit(guid, 'BUILD_KIT', 'NOT_ENOUGH_WAFER')
  end

  # build_kit, stb only, carrier state is EO, eqp is in an Auto-3 area
  def test135_build_kit_EO
    assert @@wbtqtest.prepare_resources(@@carrier)
    $log.info "waiting #{@@mds_sync} s for the MDS to sync"; sleep @@mds_sync
    # change carrier status to EO
    assert_equal 0, @@sv.carrier_xfer_status_change(@@carrier, 'EO', @@wbtqtest.eqp)
    $log.info "waiting #{@@mds_sync} s for the MDS to sync"; sleep @@mds_sync
    #
    # build_kit request
    assert guid = @@wbtqtest.build_kit(@@wbtqtest.kits[:build_sr].first, '208', 'Not enough source wafer available.')
    assert @@wbtqtest.verify_kit(guid, 'BUILD_KIT', 'NOT_ENOUGH_WAFER')
  end

  # preferences

  def test141_stb_only_2nd_source
    assert @@wbtqtest.stb_only_verify(@@wbtqtest.kits[:stbonly_other].first, @@carrier, product: @@product1_2nd_source)
  end

  def test142_build_kit_2nd_source
    assert @@wbtqtest.build_kit_verify(@@wbtqtest.kits[:build_nosr].first, @@carrier,
      product: @@product1_2nd_source, stbtrigger: false)
  end

  def test143_stb_only_prebuilt
    #assert @@wbtqtest.prepare_resources(@@carrier), "error cleaning up required carriers"
    assert kit = @@wbtqtest.kits[:stbonly_prebuilt].first   # 'QA-KIT02'
    assert @@wbtqtest.stb_only_verify(kit, @@carrier_prebuilt, available_carriers: @@carrier)
  end

  # use kit normally requiring a SR with a prebuilt carrier: no SR
  def test144_build_kit_prebuilt
    assert @@wbtqtest.prepare_resources(@@carrier), 'error cleaning up required carriers'
    # pick kit requiring an SR with @@carrier but no new carrier
    assert kit = (@@wbtqtest.kits[:build_sr] - @@wbtqtest.kits[:new_carrier]).first
    assert @@wbtqtest.build_kit_verify(kit, @@carrier_prebuilt, available_carriers: @@carrier, nosr: true)
  end

  # must not use prebuilt carrier with second source, preferred is any first source carrier
  def test145_stb_only_2nd_source_prebuilt
    assert @@wbtqtest.prepare_resources(@@carrier_prebuilt, product: @@product1_2nd_source), "error cleaning up"
    kit = @@wbtqtest.kits[:stbonly_other].first
    assert @@wbtqtest.stb_only_verify(kit, @@carrier, available_carriers: @@carrier_prebuilt)
  end

  # must use prebuilt carrier with second source, although first source carrier requiring SR is available
  def test146_build_kit_2nd_source_prebuilt
    assert @@wbtqtest.prepare_resources(@@carrier_prebuilt, product: @@product1_2nd_source), 'error cleaning up'
    # pick kit requiring a SR with @@carrier (bot not a new carrier)
    assert kit = (@@wbtqtest.kits[:build_sr] - @@wbtqtest.kits[:new_carrier]).first
    assert @@wbtqtest.build_kit_verify(kit, @@carrier_prebuilt, available_carriers: @@carrier, nosr: true, stbtrigger: false)
  end

  # carrier location

  # stb_only, carrier in other module
  def test151_stb_only_other_module
    skip 'not applicable in Fab8' if $env == 'f8stag'
    assert kit = @@wbtqtest.kits[:stbonly_other].first
    assert @@wbtqtest.stb_only_verify(kit, @@carrier, stocker: @@stocker2)
  end

  # build_kit, sorting required, carrier in other module
  def test152_build_kit_other_module
    skip 'not applicable in Fab8' if $env == 'f8stag'
    assert kit = @@wbtqtest.kits[:build_sr].first
    assert @@wbtqtest.build_kit_verify(kit, @@carrier, stocker: @@stocker2, stbtrigger: false)
  end

  # stb_only, carriers in other module and Annex available
  def test153_stb_only_other_module_and_annex
    skip 'not applicable in Fab8' if $env == 'f8stag'
    assert kit = @@wbtqtest.kits[:stbonly_other].first
    carrier2 = @@carriers_empty.first
    assert lots = @@wbtqtest.prepare_src_carriers(carrier2, kit, no_other_wafers: true, stocker: @@stocker2)
    @@testlots += lots
    assert @@wbtqtest.stb_only_verify(kit, @@carrier, stocker: @@stocker_anx, available_carriers: carrier2)
  end

  # build_kit, sorting required, tgtcarriers in other module and Annex available
  def test154_build_kit_other_module_and_annex
    skip 'not applicable in Fab8' if $env == 'f8stag'
    assert kit = @@wbtqtest.kits[:build_sr].first
    carrier2 = @@carriers_empty.first
    assert lots = @@wbtqtest.prepare_src_carriers(carrier2, kit, no_other_wafers: true, stocker: @@stocker2)
    @@testlots += lots
    assert @@wbtqtest.build_kit_verify(kit, @@carrier, stocker: @@stocker_anx, available_carriers: carrier2, stbtrigger: false)
  end

  # stb_only, eqp in ANX, carrier in other module  BROKEN
  def test155_stb_only_ANX
    skip 'not applicable in Fab8' if $env == 'f8stag'
    assert kit = @@wbtqtest.sfc.select_kits(eqp: @@eqp_anx).first, 'no kit found'
    assert @@wbtqtest.stb_only_verify(kit, @@carrier, eqp: @@eqp_anx, chamber: @@chamber_anx, stocker: @@stocker2)
  end

  # stb_only, kit from multiple srccarriers
  # requires a kit with 1) no wafer order, 2) no matching slot for src products and 3 wafers
  def test156_stb_only_multicarrier
    skip 'not applicable in Fab8' if $env == 'f8stag'
    # create 3 lots with 1 wafer each in separate carriers
    carriers = @@carriers_empty.take(3)
    pslot = {@@wbtqtest.srcproduct=>[25]}
    # stb_only (QA-KIT14), 3 srclots with 1 wafer each in separate carriers
    assert kit = @@wbtqtest.sfc.select_kits(eqp: @@eqp_anx).first, 'no kit found'
    assert guid = @@wbtqtest.stb_only_verify(kit, nil, eqp: @@eqp_anx, chamber: @@chamber_anx, carrier_check: false) {
      assert lots = @@wbtqtest.prepare_src_carriers(carriers, [pslot, pslot, pslot], stocker: @@stocker_anx, nwafers: 1)
      @@testlots += lots
    }
    @@testlots += carriers.collect {|c| @@sv.carrier_status(c).lots}.flatten
    # ensure the correct src carriers have been used
    assert_equal carriers.sort, @@wbtqtest.mds.context_carriers(guid), 'wrong carriers'
  end

  # stb_only, kit from multiple srccarriers at different locations
  # requires a kit with 1) no wafer order, 2) no matching slot for src products and 3 wafers
  def test157_stb_only_multicarrier_multilocation
    skip 'not applicable in Fab8' if $env == 'f8stag'
    # create 3 lots with 1 wafer each in separate carriers and different locations
    carriers = @@carriers_empty.take(3)
    pslot = {@@wbtqtest.srcproduct=>[25]}
    # stb_only (QA-KIT14), 3 srclots with 1 wafer each in separate carriers
    assert kit = @@wbtqtest.sfc.select_kits(eqp: @@eqp_anx).first, 'no kit found'
    assert guid = @@wbtqtest.stb_only_verify(kit, nil, eqp: @@eqp_anx, chamber: @@chamber_anx, carrier_check: false) {
      assert lots = @@wbtqtest.prepare_src_carriers(carriers[0], pslot, stocker: @@stocker2, nwafers: 1)
      @@testlots += lots
      assert lots = @@wbtqtest.prepare_src_carriers(carriers[1], pslot, stocker: @@stocker2, nwafers: 1)
      @@testlots += lots
      assert lots = @@wbtqtest.prepare_src_carriers(carriers[2], pslot, stocker: @@stocker_anx, nwafers: 1)
      @@testlots += lots
    }
    @@testlots += carriers.collect {|c| @@sv.carrier_status(c).lots}.flatten
    # ensure the correct src carriers have been used
    assert_equal carriers.sort, @@wbtqtest.mds.context_carriers(guid), 'wrong carriers'
  end

  # TODO: derived from test153, not working (TargetLocation not clarified)
  def XXtestdev158_stb_only_lotTargetArea
    skip 'not applicable in Fab8' if $env == 'f8stag'
    assert kit = @@wbtqtest.kits[:stbonly_other].first
    cc = @@carriers_empty.take(2)
    assert @@wbtqtest.prepare_resources(cc, stocker: @@stocker2, rework_lots: false)
    lots = cc.collect {|c|
      assert ll = @@wbtqtest.prepare_src_carriers(c, kit)
      @@testlots += ll
      ll.last
    }
    # mark 1st lot as Module2 and 2nd lot as Module1
    assert @@mdswritesvc.write_lottgtarea(lots[0], 'QA', tgtloc: 'QAM2')
    assert @@mdswritesvc.write_lottgtarea(lots[1], 'QA', tgtloc: 'QAM1')
    $log.info "waiting #{@@mds_sync} s for the MDS to sync"; sleep @@mds_sync
    # verify the lot in Module1, 2nd carrier is picked
    assert @@wbtqtest.stb_only(kit, '100', "Target carrier: #{cc[1]}")
  end

  # spec allowes other kit in carrier, stbonly
  def test161_other_kit_allowed_stbonly
    # stb_only (QA-KIT03) with qual lot in the carrier (must not disturb kit slots)
    assert kit = (@@wbtqtest.kits[:stbonly_other] & @@wbtqtest.sfc.select_kits(eqp: @@wbtqtest.eqp, no_other_kits: false)).first
    assert @@wbtqtest.stb_only_verify(kit, @@carrier) {
      # create 1 qual lot in the carrier (must not disturb kit slots)
      srclot = @@wbtqtest.kitcarrier_info(@@carrier).last.lot
      @@wbtqtest.create_quallot(srclot, @@product_unused)
    }
  end

  # spec allowes other kit in carrier, build_kit must not use this carrier
  def test162_other_kit_allowed_build_kit
    assert @@wbtqtest.prepare_resources(@@carrier)
    # create 1 qual lot in the carrier
    srclot = @@wbtqtest.kitcarrier_info(@@carrier).last.lot
    assert @@wbtqtest.create_quallot(srclot, @@product_unused), 'error creating dummy qual lot'
    $log.info "waiting #{@@mds_sync} s for the MDS to sync"; sleep @@mds_sync
    #
    # build_kit request
    assert kit = (@@wbtqtest.kits[:build_nosr] & @@wbtqtest.sfc.select_kits(eqp: @@wbtqtest.eqp, no_other_kits: false)).first
    assert guid = @@wbtqtest.build_kit(kit, '208', 'Not enough source wafer')
    assert @@wbtqtest.verify_kit(guid, 'BUILD_KIT', 'NOT_ENOUGH_WAFER')
  end

  # spec allowes no other kit in carrier, stbonly
  def test163_other_kit_not_allowed_stbonly
    assert @@wbtqtest.prepare_resources(@@carrier)
    # create 1 qual lot in the carrier
    srclot = @@wbtqtest.kitcarrier_info(@@carrier).first.lot
    assert @@wbtqtest.create_quallot(srclot, @@product_unused), 'error creating dummy qual lot'
    $log.info "waiting #{@@mds_sync} s for the MDS to sync"; sleep @@mds_sync
    #
    # stb_only request
    assert kit = (@@wbtqtest.kits[:stbonly_other] & @@wbtqtest.sfc.select_kits(eqp: @@wbtqtest.eqp, no_other_kits: true)).first
    assert guid = @@wbtqtest.stb_only(kit, '208', 'Not enough source wafer')
    assert @@wbtqtest.verify_kit(guid, 'BUILD_KIT', 'NOT_ENOUGH_WAFER')
  end

  # spec allowes no other kit in carrier, build_kit
  def test164_other_kit_not_allowed_build_kit
    assert @@wbtqtest.prepare_resources(@@carrier)
    # create 1 qual lot in the carrier
    srclot = @@wbtqtest.kitcarrier_info(@@carrier).first.lot
    assert @@wbtqtest.create_quallot(srclot, @@product_unused), 'error creating dummy qual lot'
    $log.info "waiting #{@@mds_sync} s for the MDS to sync"; sleep @@mds_sync
    #
    # build_kit request
    assert kit = (@@wbtqtest.kits[:build_nosr] & @@wbtqtest.sfc.select_kits(eqp: @@wbtqtest.eqp, no_other_kits: true)).first
    assert guid = @@wbtqtest.build_kit(kit, '208', 'Not enough source wafer')
    assert @@wbtqtest.verify_kit(guid, 'BUILD_KIT', 'NOT_ENOUGH_WAFER')
  end

  # spec allowes no other kit in carrier, but other kit is alredy FINISHED, stb_only
  def test165_other_kit_not_allowed_finished_stb_only
    # stb_only (QA-KIT08) with finished qual lot in the carrier (must not disturb kit slots)
    assert kit = (@@wbtqtest.kits[:stbonly_other] & @@wbtqtest.sfc.select_kits(eqp: @@wbtqtest.eqp, no_other_kits: true, match_order: false)).first
    assert @@wbtqtest.stb_only_verify(kit, @@carrier) {
      # create 1 qual lot in the carrier (must not disturb kit slots)
      srclot = @@wbtqtest.kitcarrier_info(@@carrier).last.lot
      assert qlot = @@wbtqtest.create_quallot(srclot, @@product_unused)
      # finish qual lot
      assert_equal 0, @@sv.lot_opelocate(qlot, :last)
      assert_equal 'InBank', @@sv.lot_info(qlot).states['Lot Inventory State'], 'qual lot must be banked in'
    }
  end

  # spec allowes no other kit in carrier, but other kit is alredy FINISHED, build_kit
  def test166_other_kit_not_allowed_finished_build_kit
    # build_kit (QA-KIT08) with finished qual lot in the carrier (must not disturb kit slots)
    assert kit = (@@wbtqtest.kits[:build_nosr] & @@wbtqtest.sfc.select_kits(eqp: @@wbtqtest.eqp, no_other_kits: true, match_order: false)).first
    assert @@wbtqtest.build_kit_verify(kit, @@carrier) {
      # create 1 qual lot in the carrier (must not disturb kit slots)
      srclot = @@wbtqtest.kitcarrier_info(@@carrier).last.lot
      assert qlot = @@wbtqtest.create_quallot(srclot, @@product_unused)
      # finish qual lot
      assert_equal 0, @@sv.lot_opelocate(qlot, :last)
      assert_equal 'InBank', @@sv.lot_info(qlot).states['Lot Inventory State'], 'qual lot must be banked in'
    }
  end

  # spec allows to build kit but the route's start bank does not allow STB
  def test167_startbank_stb_not_allowed
    assert @@wbtqtest.prepare_resources(@@carrier, product: @@product_wrong_bank)
    $log.info "waiting #{@@mds_sync} s for the MDS to sync"; sleep @@mds_sync
    #
    assert kit = @@wbtqtest.kits[:wrong_bank].first
    # verify STBOnly and build_kit are refused
    msg = 'No valid start bank found for destination product [id='
    assert guid = @@wbtqtest.stb_only(kit, '203', msg), 'wrong response message'
    assert guid = @@wbtqtest.build_kit(kit, '203', msg), 'wrong response message'
  end

  def test168_stb_only_multiple_retries
    carrier = @@carrier_prebuilt
    assert @@wbtqtest.prepare_resources(carrier)
    # disable carrier to let the stb only request fail
    assert_equal 0, @@sv.carrier_status_change(carrier, 'NOTAVAILABLE')
    $log.info "waiting #{@@mds_sync} s for the MDS to sync"; sleep @@mds_sync
    #
    # stb_only request, must fail
    guid = unique_string('QA168_')
    assert kit = @@wbtqtest.kits[:stbonly_other].first
    3.times {assert @@wbtqtest.stb_only(kit, '208', 'Not enough source wafer', guid: guid)}
    # enable the carrier, stb_only must pass
    assert_equal 0, @@sv.carrier_status_change(carrier, 'AVAILABLE')
    $log.info "waiting #{@@mds_sync} s for the MDS to synch"; sleep @@mds_sync
    #
    assert @@wbtqtest.stb_only(kit, '100', "Target carrier: #{carrier}", guid: guid)
  end

  def test169_build_kit_multiple_retries
    carrier = @@carrier_prebuilt
    assert @@wbtqtest.prepare_resources(carrier)
    # disable carrier to let the build kit request fail
    assert_equal 0, @@sv.carrier_status_change(carrier, 'NOTAVAILABLE')
    $log.info "waiting #{@@mds_sync} s for the MDS to sync"; sleep @@mds_sync
    #
    # build_kit request, must fail
    guid = unique_string('QA169_')
    assert kit = @@wbtqtest.kits[:build_nosr].first
    3.times {assert @@wbtqtest.build_kit(kit, '208', 'Not enough source wafer', guid: guid)}
    # enable the carrier, stb_only must pass
    assert_equal 0, @@sv.carrier_status_change(carrier, 'AVAILABLE')
    $log.info "waiting #{@@mds_sync} s for the MDS to synch"; sleep @@mds_sync
    #
    assert @@wbtqtest.build_kit(kit, '100', "Target carrier: #{carrier}", guid: guid)
  end

  # verify kits are found in spec if defined chambers are CH1 and CH11, optional
  def test171_ch1_ch11
    kit = @@wbtqtest.kits[:wrong_chamber].first   # QA-KIT17
    assert @@wbtqtest.prepare_resources  # no carriers available
    #
    assert res = @@wbtqtest.client.stb_only(unique_string('QA171_'), @@wbtqtest.sfc.spec, kit, @@eqp2, @@chamber2,
      order: '0' + Time.now.to_i.to_s, leadingorder: unique_string)
    # it is ok to fail because of not enough source wafers, because the kit definition has been found
    assert ['302', '208'].member?(res['RETURNCODE']), 'wrong return code'
    # assert @@wbtqtest.stb_only(kit, '302', 'STB of needed lots failed', eqp: @@eqp2, chamber: @@chamber2)
    #
    assert_equal 0, @@sv.carrier_status_change(@@carrier_prebuilt, 'AVAILABLE')
    sleep 10
    assert @@wbtqtest.stb_only(kit, '208', 'Not enough source wafer', eqp: @@eqp2, chamber: @@chamber2 + '1')
  end

  # wrong spec
  def test172_wrong_spec
    assert @@wbtqtest.prepare_resources(@@carrier_prebuilt)
    assert res = @@wbtqtest.client.stb_only(unique_string('QA172_'), 'C07-NOTEX', 'QA-KIT03', @@eqp2, @@chamber2,
      order: '0' + Time.now.to_i.to_s, leadingorder: unique_string)
    assert_equal '203', res['RETURNCODE'], 'wrong return code'
    assert_equal 'Specification not valid.', res['MESSAGE'][0], 'wrong message'
    assert res = @@wbtqtest.client.build_kit(unique_string('QA172_'), 'C07-NOTEX', 'QA-KIT03', @@eqp2, @@chamber2,
      order: '0' + Time.now.to_i.to_s, leadingorder: unique_string)
    assert_equal '203', res['RETURNCODE'], 'wrong return code'
    assert_equal 'Specification not valid.', res['MESSAGE'][0], 'wrong message'
  end

  # verify kits are built with restricted source lots, 'Steffen Volt Case 1'
  def test180
    assert @@wbtqtest.prepare_resources
    kit = @@wbtqtest.kits[:srcprio][0]
    productwafers = [{'B-100-CLUSTER.01'=>4}, {'B-200-CLUSTER.01'=>2}]
    assert lots = @@wbtqtest.prepare_src_carriers(@@carriers_empty.take(2), productwafers)
    @@testlots += lots
    #
    $log.info "waiting #{@@mds_sync} s for the MDS to sync"; sleep @@mds_sync
    assert guid = @@wbtqtest.build_kit(kit, '903', 'SortJobController sort request was created.')
  end

  # verify kits are built with restricted source lots, 'Steffen Volt Case 1b'
  def test181
    assert @@wbtqtest.prepare_resources
    kit = @@wbtqtest.kits[:srcprio][1]
    productwafers = [{'B-100-CLUSTER.01'=>4}, {'B-200-CLUSTER.01'=>2}]
    assert lots = @@wbtqtest.prepare_src_carriers(@@carriers_empty.take(2), productwafers)
    @@testlots += lots
    #
    $log.info "waiting #{@@mds_sync} s for the MDS to sync"; sleep @@mds_sync
    assert guid = @@wbtqtest.build_kit(kit, '903', 'SortJobController sort request was created.')
  end

  # verify kits are built with restricted source lots, 'Steffen Volt Case 2'
  def test182
    assert @@wbtqtest.prepare_resources
    kit = @@wbtqtest.kits[:srcprio][2]
    productwafers = [{'B-100-CLUSTER.01'=>4}, {'B-200-CLUSTER.01'=>(3..10).to_a}]
    assert lots = @@wbtqtest.prepare_src_carriers(@@carriers_empty.take(2), productwafers, no_other_wafers: true)
    @@testlots += lots
    #
    $log.info "waiting #{@@mds_sync} s for the MDS to sync"; sleep @@mds_sync
    assert guid = @@wbtqtest.build_kit(kit, '903', 'SortJobController sort request was created.')
    #
    # verfy carriers
    assert ctx = @@wbtqtest.mds.contexts(guid: guid).first
    srclots = @@wbtqtest.mds.sjc_tasks(srid: ctx.srid).collect {|st| st.srclot}
    expectedlots = @@sv.carrier_status(@@carriers_empty.first).lots + [@@sv.carrier_status(@@carriers_empty[1]).lots.last]
    $log.info "srclots: #{srclots}"
    assert_equal expectedlots.sort, srclots.sort, 'WBTQ picked the wrong lots'
  end

  # verify kits are built with restricted source lots, 'Steffen Volt Case 3'
  def test183_lot_age
    assert @@wbtqtest.prepare_resources
    kit = @@wbtqtest.kits[:srcprio][3]
    productwafers = [{'B-100-CLUSTER.01'=>2}, {'B-200-CLUSTER.01'=>2}, {'B-300-CLUSTER.01'=>2},
      {'B-100-CLUSTER.01'=>2, 'B-200-CLUSTER.01'=>2}, {'B-300-CLUSTER.01'=>2}]
    assert lots = @@wbtqtest.prepare_src_carriers(@@carriers_empty.take(5), productwafers, no_other_wafers: true)
    @@testlots += lots
    #
    $log.info "waiting #{@@mds_sync} s for the MDS to sync"; sleep @@mds_sync
    assert guid = @@wbtqtest.build_kit(kit, '903', 'SortJobController sort request was created.')
    #
    # verfy carriers
    assert ctx = @@wbtqtest.mds.contexts(guid: guid).first
    srclots = @@wbtqtest.mds.sjc_tasks(srid: ctx.srid).collect {|st| st.srclot}
    expectedlots = @@carriers_empty[2..3].collect {|c| @@sv.carrier_status(c).lots}.flatten
    ##expectedlots = @@sv.carrier_status(@@carriers_empty[2]).lots + @@sv.carrier_status(@@carriers_empty[3]).lots
    assert_equal expectedlots.sort, srclots.sort, 'WBTQ picked the wrong lots'
  end


  # Engineering Kits

  def test211_engineeringkit_cancel
    # send engingeering_flag
    guid = unique_string('QA109_')
    res = @@wbtqtest.client.engineering_flag(guid, @@wbtqtest.eqp)
    assert @@wbtqtest.verify_response(res, '100', 'OK')
    assert @@wbtqtest.verify_kit(guid, 'ENGINEERING_KIT', 'ALL_OK')
    #
    # send stb_only
    assert kit = @@wbtqtest.kits[:stbonly_other].first   # 'QA-KIT03'
    res = @@wbtqtest.stb_only(kit, '902', 'Engineering kit selection send to FabGUI.', guid: guid)
    assert @@wbtqtest.verify_kit(guid, 'ENGINEERING_SELECTION', 'ALL_OK')
    #
    # send FabGUI response msg 'Cancel'
    kslots = @@wbtqtest.sfc.kit_info(kit, @@wbtqtest.eqp).kit_slots
    sel = kslots.values.flatten
    assert @@wbtqtest.fabgui.engineering_kit_response(guid, sel, kslots, response: 'Cancel'), 'error sending FabGUI response'
    #
    # verify msg to SAP
    assert res = @@wbtqtest.client.receive_msg(guid), 'no msg to SAP'
    assert @@wbtqtest.verify_response(res, '402', 'Engineering kit canceled'), 'wrong msg tp SAP'
  end

  def test212_engineeringkit_stbonly
    carrier = @@carrier_prebuilt
    assert @@wbtqtest.prepare_resources(carrier), "error preparing resources"
    $log.info "waiting #{@@mds_sync} s for the MDS to sync"; sleep @@mds_sync
    #
    # send engingeering_flag
    guid = unique_string('QA109_')
    res = @@wbtqtest.client.engineering_flag(guid, @@wbtqtest.eqp)
    assert @@wbtqtest.verify_response(res, '100', 'OK')
    assert @@wbtqtest.verify_kit(guid, 'ENGINEERING_KIT', 'ALL_OK')
    #
    # send stb_only
    assert kit = @@wbtqtest.kits[:stbonly_other].first   # 'QA-KIT03'
    lorder = unique_string
    res = @@wbtqtest.stb_only(kit, '902', 'Engineering kit selection send to FabGUI.', guid: guid, leadingorder: lorder)
    assert @@wbtqtest.verify_kit(guid, 'ENGINEERING_SELECTION', 'ALL_OK', leadingorder: lorder)
    #
    # send FabGUI response msg, selecting only the first 2 slots
    kslots = @@wbtqtest.sfc.kit_info(kit, @@wbtqtest.eqp).kit_slots
    sel = kslots.values.flatten.take(2)  # the first 2 only
    assert @@wbtqtest.fabgui.engineering_kit_response(guid, sel, kslots), 'error sending FabGUI response'
    # verify WBTQ response to SAP, kit wafers are only the selected slots
    res = @@wbtqtest.client.receive_msg(guid)
    assert @@wbtqtest.verify_response(res, '100', "Target carrier: #{@@carrier_prebuilt}")
    ekslots = @@wbtqtest.engineering_kit_slots(sel, kit_slots: kslots)
    assert @@wbtqtest.verify_kit(guid, 'FINISHED', 'ALL_OK', leadingorder: lorder, kit_slots: ekslots, match_order: false)
  end

  # AutoWaferSelection

  def test221_autowafersel_engineering
    # send auto_waferselection_flag
    guid = unique_string('QA221_')
    assert res = @@wbtqtest.client.auto_waferselection_flag(guid, @@eqp_swc)
    assert @@wbtqtest.verify_response(res, '100', 'OK')
    assert @@wbtqtest.verify_kit(guid, 'ENGINEERING_KIT', 'ALL_OK')
    #
    # send engineering_flag, must be refused
    assert res = @@wbtqtest.client.engineering_flag(guid, @@eqp_swc)
    assert @@wbtqtest.verify_response(res, '888', 'already marked as automated wafer selection'), 'wrong response'
    assert @@wbtqtest.verify_kit(guid, 'ENGINEERING_KIT', 'INTERNAL_ERROR')
  end

  def test222_autowafersel_wrong_eqp
    # send auto_waferselection_flag for a wrong eqp
    guid = unique_string('QA222_')
    assert res = @@wbtqtest.client.auto_waferselection_flag(guid, @@wbtqtest.eqp)
    # verify responses: eqp is not in QUALSELECTION spec, eqp is not of allowed model (properties)
    assert @@wbtqtest.verify_response(res, '203', 'Specification not valid')
    assert @@wbtqtest.verify_response(res, '203', 'Equipment model is not allowed')
  end

  def test231_all_up
    assert @@wbtqtest.auto_waferselection_stbonly_verify('QA-KIT31-SWC', @@eqp_swc, @@carrier) {
      true
    }
  end

  def test232_eqp_down
    assert @@wbtqtest.auto_waferselection_stbonly_verify('QA-KIT31-SWC', @@eqp_swc, @@carrier) {
      @@sv.eqp_status_change(@@eqp_swc, '2NDP') == 0
    }
  end

  def test233_1ch_down
    assert @@wbtqtest.auto_waferselection_stbonly_verify('QA-KIT31-SWC', @@eqp_swc, @@carrier) {
      @@sv.chamber_status_change(@@eqp_swc, 'CHA', '2NDP') == 0
    }
  end

  def test234_all_chambers_down
    assert @@wbtqtest.auto_waferselection_stbonly_verify('QA-KIT31-SWC', @@eqp_swc, @@carrier) {
      ok = true
      @@sv.eqp_info(@@eqp_swc).chambers.each {|ch|
        ok &= @@sv.chamber_status_change(@@eqp_swc, ch.chamber, '2NDP') == 0
      }
      ok
    }
  end

  def test235_eqp_and_chambers_down
    assert @@wbtqtest.auto_waferselection_stbonly_verify('QA-KIT31-SWC', @@eqp_swc, @@carrier) {
      ok = @@sv.eqp_status_change(@@eqp_swc, '2NDP') == 0
      @@sv.eqp_info(@@eqp_swc).chambers.each {|ch|
        ok &= @@sv.chamber_status_change(@@eqp_swc, ch.chamber, '2NDP') == 0
      }
      ok
    }
  end

  def test236_1ch_down_build_kit
    assert @@wbtqtest.auto_waferselection_buildkit_verify('QA-KIT31-SWC', @@eqp_swc, @@carrier) {
      @@sv.chamber_status_change(@@eqp_swc, 'CHA', '2NDP') == 0
    }
  end


  # test job specific transaction timeout is used

  def test301_notimeout_stbonly
    assert kit = @@wbtqtest.kits[:stbonly_prebuilt].first   # 'QA-KIT02'
    carrier = @@carrier_prebuilt
    assert @@wbtqtest.prepare_resources(carrier)
    # prepare use route such that the STB duration is slightly less than transaction.timeout
    assert routes = @@wbtqtest.kit_routes(kit)
    stbsleep = 300 / routes.size + 20
    assert stbsleep * routes.size < @@jobtimeout, "jobtimeout too short for a good test: #{@@jobtimeout}"
    routes.uniq.each {|route|
      assert_equal 0, @@sv.user_parameter_change('Route', route, @@wbtqtest.stb_routeparameter, stbsleep)
    }
    $log.info "waiting #{@@mds_sync} s for the MDS to sync"; sleep @@mds_sync
    # stb_only request
    tstart = Time.now
    assert guid = @@wbtqtest.stb_only(kit, '100', "Target carrier: #{carrier}", timeout: @@jobtimeout)
    assert Time.now - tstart > 300, 'STB time too short, wrong route config in SM?'
  end

  def test302_notimeout_stbtrigger
    assert kit = @@wbtqtest.kits[:stbonly_prebuilt].first   # 'QA-KIT02'
    carrier = @@carrier_prebuilt
    assert @@wbtqtest.prepare_resources(carrier)
    # prepare use route such that the STB duration is slightly less than transaction.timeout
    assert routes = @@wbtqtest.kit_routes(kit)
    stbsleep = 300 / routes.size + 20
    assert stbsleep * routes.size < @@jobtimeout, "jobtimeout too short for a good test: #{@@jobtimeout}"
    routes.uniq.each {|route|
      assert_equal 0, @@sv.user_parameter_change('Route', route, @@wbtqtest.stb_routeparameter, stbsleep)
    }
    $log.info "waiting #{@@mds_sync} s for the MDS to sync"; sleep @@mds_sync
    # build_kit
    assert guid = @@wbtqtest.build_kit(kit, '100', "Target carrier: #{carrier}", stbtrigger: false)  #, timeout: 600)
    # stb_trigger, actually the jobtimeout for stbtrigger is the configured value minus 60 s
    tstart = Time.now
    res = @@wbtqtest.client.stb_trigger(guid, timeout: @@jobtimeout)
    assert @@wbtqtest.verify_response(res, '100', carrier)
    assert Time.now - tstart > 300, 'STB time too short, wrong route config in SM?'
  end

  def testmanual311_timeout
    # no regular test, requires special SM setup on route ITDC-WBQT-U.01, only on demand
    #
    kit = @@wbtqtest.kits[:stbonly_prebuilt].first # QA-KIT02
    carrier = @@carrier_prebuilt
    assert @@wbtqtest.prepare_resources(carrier)
    $log.info "waiting #{@@mds_sync} s for the MDS to sync"; sleep @@mds_sync
    # stb_only request
    assert_nil @@wbtqtest.stb_only(kit, nil, nil)
    # build kit request
    assert guid = @@wbtqtest.build_kit(kit, '100', "Target carrier: #{carrier}")
    assert_nil @@wbtqtest.client.stb_trigger(guid)
  end

end
