=begin 
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.
    
Author: Steffen Steidten, 2011-03-30

Version: 18.06

History:
  2015-04-02 ssteidte, cleanup
  2018-11-07 sfrieske, minor cleanup
=end

require 'SiViewTestCase'


class JCAP_LotHoldRelease < SiViewTestCase
  @@sv_defaults = {product: 'ITDC-JCAP-TEST.01', route: 'ITDC-JCAP-TEST.01'}  # TODO: standard route ?
  @@holdcode = 'X-PR'
  @@holdcode_norelease = 'FAT'
  @@eqp = 'UTF001'
  
  @@job_interval = 330
  
  
  def self.after_all_passed
    @@lots.each {|lot| @@sv.delete_lot_family(lot)}
  end

  
  def test00_setup
    $setup_ok = false
    #
    assert @@lots = @@svtest.new_lots(6), 'error creating test lots'
    # set lot holds: #0 holdcode, #1 holdcode_norelease, #2 both
    assert_equal 0, @@sv.lot_hold(@@lots[0], @@holdcode)
    assert_equal 0, @@sv.lot_hold(@@lots[1], @@holdcode_norelease)
    assert_equal 0, @@sv.lot_hold(@@lots[2], @@holdcode)
    assert_equal 0, @@sv.lot_hold(@@lots[2], @@holdcode_norelease)
    # set future holds: #3 future hold + hold, #4 future hold only; all with holdcode
    lot = @@lots[3]
    ops = @@sv.lot_operation_list(lot)
    assert_equal 0, @@sv.lot_futurehold(lot, ops[-1].opNo, @@holdcode)
    assert_equal 0, @@sv.lot_hold(lot, @@holdcode)
    lot = @@lots[4]
    ops = @@sv.lot_operation_list(lot)
    assert_equal 0, @@sv.lot_futurehold(lot, ops[-1].opNo, @@holdcode)
    # lot #5 with hold and carrier EI
    assert_equal 0, @@sv.lot_hold(@@lots[5], @@holdcode)
    carrier = @@sv.lot_info(@@lots[5]).carrier
    assert_equal 0, @@sv.eqp_cleanup(@@eqp, mode: 'Off-Line-1')
    assert_equal 0, @@sv.carrier_xfer_status_change(carrier, 'EO', @@eqp)
    assert_equal 0, @@sv.eqp_load(@@eqp, nil, carrier, purpose: 'Other'), "error loading carrier #{carrier}"
    # verify all holds
    assert_equal 1, @@sv.lot_hold_list(@@lots[0], reason: @@holdcode).size
    assert_equal 1, @@sv.lot_hold_list(@@lots[1], reason: @@holdcode_norelease).size
    assert_equal 1, @@sv.lot_hold_list(@@lots[2], reason: @@holdcode).size
    assert_equal 1, @@sv.lot_hold_list(@@lots[2], reason: @@holdcode_norelease).size
    assert_equal 1, @@sv.lot_hold_list(@@lots[3], reason: @@holdcode).size
    assert_equal 1, @@sv.lot_futurehold_list(@@lots[3], reason: @@holdcode).size
    assert_equal 1, @@sv.lot_futurehold_list(@@lots[4], reason: @@holdcode).size
    assert_equal 1, @@sv.lot_hold_list(@@lots[5], reason: @@holdcode).size
    #    
    # wait for jCAP job
    $log.info "waiting #{@@job_interval} s for the jCAP job to run"; sleep @@job_interval
    #
    $setup_ok = true
  end
  
  def test11_hold_to_release
    assert_equal 0, @@sv.lot_hold_list(@@lots[0], reason: @@holdcode).size
    assert_equal 0, @@sv.lot_hold_list(@@lots[0], reason: @@holdcode_norelease).size
  end

  def test12_hold_not_to_release
    assert_equal 0, @@sv.lot_hold_list(@@lots[1], reason: @@holdcode).size
    assert_equal 1, @@sv.lot_hold_list(@@lots[1], reason: @@holdcode_norelease).size
  end
  
  def test13_both_holds
    assert_equal 0, @@sv.lot_hold_list(@@lots[2], reason: @@holdcode).size
    assert_equal 1, @@sv.lot_hold_list(@@lots[2], reason: @@holdcode_norelease).size
  end
  
  def test14_hold_futurehold
    assert_equal 0, @@sv.lot_hold_list(@@lots[3], reason: @@holdcode).size
    assert_equal 1, @@sv.lot_futurehold_list(@@lots[3], reason: @@holdcode).size
  end
  
  def test15_futurehold_only  
    assert_equal 1, @@sv.lot_futurehold_list(@@lots[4], reason: @@holdcode).size
  end

  def test16_hold_to_release_carrier_status_EI
    assert_equal 0, @@sv.lot_hold_list(@@lots[5], reason: @@holdcode).size, "holds exist for lot #{@@lots[5]}: #{@@sv.lot_hold_list(@@lots[5], reason: @@holdcode)}"
    assert_equal 0, @@sv.lot_hold_list(@@lots[5], reason: @@holdcode_norelease).size, "holds exist for lot #{@@lots[5]}: #{@@sv.lot_hold_list(@@lots[5], reason: @@holdcode_norelease)}"
  end

end
