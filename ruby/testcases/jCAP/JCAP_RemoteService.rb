=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author:  Steffen Steidten, 2012-04-24

Version: 21.08

History:
  2015-05-26 adarwais, cleaned up and verified test cases with R15
  2016-08-24 sfrieske, use lib rqrsp_mq instead of rqrsp
  2017-11-20 sfrieske, use struct_to_hash instead of Struct.to_hash
  2018-03-06 sfrieske, replaced user_parameter_delete_verify by user_parameter_delete
  2019-07-01 sfrieske, added tests for new iCASE interfaces (v 19.06)
  2020-03-31 sfrieske, added test for getProcessedLotOperationHistory3
  2021-07-05 sfrieske, added test for user_parameter_update
  2021-08-04 sfrieske, do not wait for MDS sync before calling get_lot_info4
  2021-08-26 sfrieske, removed dependency on misc.rb (duration_in_seconds)

Notes:
  v20.08 see https://docs.google.com/document/d/1rj3o0PpPbMLliVAEY8zxlf4DarglUog8SbvyOjoT-8Y/edit#heading=h.yvgkbgjc4cz5
=end

require 'SiViewTestCase'
require 'jcap/remotesvc'
require 'util/verifyhash'
require 'util/waitfor'


# Test the jCAP Remote Service job, used by WebMRB, ePRF, OJT, etc.
class JCAP_RemoteService < SiViewTestCase
  @@product_atv = 'UT-PRODUCT000.ATV'
  @@product_sec = 'UT-PRODUCT000.SEC'
  @@holdreason = 'ENG'
  @@holdrelease_reason = 'ALL'
  # for backup lots
  @@bak_env = 'f8stag'
  @@newlot_params_bop = {product: 'UT-PRODUCT-FAB8.01', route: 'UTRT101.01'}
  @@bop_params = {return_bank: 'O-BACKUPOPER', backup_bank: 'W-BACKUPOPER', backup_opNo: '1000.200'}
  #
  @@customer_gid = 'qaeric'      # a customer with UDATA GID_SOURCE 'LOT_LABEL'
  @@customer_lotid = 'qaeric3'   # a customer with UDATA GID_SOURCE 'LOT_ID'

  @@mds_sync = 35
  @@testlots = []


  def self.after_all_passed
	  @@testlots.each {|lot| @@sv.delete_lot_family(lot)}
  end


  def test00_setup
    $setup_ok = false
    #
    @@rsvc = JCAP::RemoteService.new($env, sv: @@sv)
    assert @@rsvc.get_version, 'error executing get_version'
    #
    # verify automotive and security products and GID_SOURCE customer are setup
    assert_equal 'Y', @@sv.user_data(:product, @@product_atv)['AutomotiveProduct'], 'wrong product UDATA'
    assert_equal 'Y', @@sv.user_data(:product, @@product_sec)['SecurityProduct'], 'wrong product UDATA'
    assert_nil @@sv.user_data(:customer, @@svtest.customer)['GID_SOURCE'], 'wrong customer setup'
    assert_equal 'LOT_LABEL', @@sv.user_data(:customer, @@customer_gid)['GID_SOURCE'], 'wrong customer setup'
    assert_equal 'LOT_ID', @@sv.user_data(:customer, @@customer_lotid)['GID_SOURCE'], 'wrong customer setup'
    #
    # for backup lot sync
    assert @@svbak = SiView::MM.new(@@bak_env)
    # #
    assert @@lot = @@svtest.new_lot, 'error creating test lot'
    @@testlots << @@lot
    $log.info "waiting #{@@mds_sync} s for the MDS to sync"; sleep @@mds_sync
    #
    $setup_ok = true
  end

  def test01_get_lot_info_wrong_lot
    assert_nil @@rsvc.get_lot_info('XXDUMMYNOTEX.00'), 'error executing get_lot_info for wrong lot'
    assert_equal 'PROCESSING_ERROR', @@rsvc.mq.service_response[:getLotInfoResponse][:return][:code], 'wrong service response?'
  end

  def test02_get_lot_info
    assert data = @@rsvc.get_lot_info(@@lot), 'error executing get_lot_info'
    verify_lotinfo(@@lot, data, 1)
  end

  def test03_get_lot_info2
    # regular lot
    assert data = @@rsvc.get_lot_info2(@@lot), 'error executing get_lot_info'
    verify_lotinfo(@@lot, data, 2)
    # automotive lot
    assert lot = @@svtest.new_lot(product: @@product_atv), 'error creating test lot'
    @@testlots << lot
    assert_equal 0, @@sv.lot_gatepass(lot), 'error gatepassing lot'  # to pick up the lot script params
    assert data = @@rsvc.get_lot_info2(lot), 'error executing get_lot_info'
    verify_lotinfo(lot, data, 2)
    # security lot
    assert lot = @@svtest.new_lot(product: @@product_sec), 'error creating test lot'
    @@testlots << lot
    assert_equal 0, @@sv.lot_gatepass(lot), 'error gatepassing lot'  # to pick up the lot script params
    assert data = @@rsvc.get_lot_info2(lot), 'error executing get_lot_info'
    verify_lotinfo(lot, data, 2)
  end

  # https://docs.google.com/document/d/1sxfx0x_X5mpQzoKM3uKikMGiBaDzad5VYei9hcnTR-U/edit#heading=h.8vp3k66lb3lh
  def test04_get_lot_info3
    # regular lot
    assert data = @@rsvc.get_lot_info3(@@lot), 'error calling get_lot_info3'
    verify_lotinfo(@@lot, data, 3)
    # automotive lot
    assert lot = @@svtest.new_lot(product: @@product_atv), 'error creating test lot'
    @@testlots << lot
    assert_equal 0, @@sv.lot_gatepass(lot), 'error gatepassing lot'  # to pick up the lot script params
    assert data = @@rsvc.get_lot_info3(lot), 'error calling get_lot_info3'
    verify_lotinfo(lot, data, 3)
    # security lot
    assert lot = @@svtest.new_lot(product: @@product_sec), 'error creating test lot'
    @@testlots << lot
    assert_equal 0, @@sv.lot_gatepass(lot), 'error gatepassing lot'  # to pick up the lot script params
    assert data = @@rsvc.get_lot_info3(lot), 'error calling get_lot_info3'
    verify_lotinfo(lot, data, 3)
  end


  def test05_get_lot_operation_list
    assert data = @@rsvc.get_lot_operation_list(@@lot), 'error calling service'
    lops = @@sv.lot_operation_list(@@lot)
    lops.each_with_index {|e, i|
      assert_equal e.op, data[i][:opId]
      assert_equal e.opNo, data[i][:opNumber]
      assert_equal e.route, data[i][:routeId]
    }
  end

  def test06_get_processed_lot_operation_history
    refute_nil @@rsvc.get_processed_lot_operation_history(@@lot), 'error calling service'
  end

  def test07_get_processed_lot_operation_history2
    refute_nil @@rsvc.get_processed_lot_operation_history2(@@lot), 'error calling service'
  end

  def test08_get_processed_lot_operation_history3
    liold = @@sv.lot_info(@@lot)
    assert @@sv.claim_process_lot(@@lot), 'error processing lot'
    sleep 20  # for SiView history
    assert res = @@rsvc.get_processed_lot_operation_history3(@@lot), 'error calling service'
    assert record = res.last, 'missing history entry'
    assert_equal liold.opNo, record[:operation][:opNumber], 'wrong opNo'
    assert_equal liold.carrier, record[:carrierId], 'wrong carrier'
    assert_equal liold.stage, record[:stageId], 'wrong stage'
  end

  def test11_lot_hold_and_release
    # hold
    assert res = @@rsvc.lot_hold(@@lot, reason: @@holdreason), 'error calling service'
    hh = @@sv.lot_hold_list(@@lot, reason: @@holdreason)
    assert_equal 1, hh.size, 'lot has not been set ONHOLD in SiView'
    # release
    assert res = @@rsvc.release_lot_hold(@@lot, @@holdreason, reason: @@holdrelease_reason), 'error calling service'
    hh = @@sv.lot_hold_list(@@lot, reason: @@holdreason)
    assert_equal 0, hh.size, "lot has not been set released in SiView"
  end

  def test12_lot_future_hold
    #
    # set future hold
    assert @@rsvc.future_hold(@@lot, nil, post: true, reason: @@holdreason), 'error calling service'
    refute_empty @@sv.lot_futurehold_list(@@lot, reason: @@holdreason), 'future hold has not been set in SiView'
    # send request again, must pass
    assert @@rsvc.future_hold(@@lot, nil, post: true, reason: @@holdreason), 'error calling service'
    # send request again with errorIfAlreadyExists false, must pass
    assert @@rsvc.future_hold(@@lot, nil, post: true, reason: @@holdreason, exists_error: false), 'error calling service'
    # send request again with errorIfAlreadyExists true, must fail
    assert !@@rsvc.future_hold(@@lot, nil, post: true, reason: @@holdreason, exists_error: true), 'error calling service'
    #
    # remove future hold and send request again with errorIfAlreadyExists false, must pass and set future hold
    assert_equal 0, @@sv.lot_futurehold_cancel(@@lot, nil)
    assert @@rsvc.future_hold(@@lot, nil, post: true, reason: @@holdreason, exists_error: false), 'error calling service'
    refute_empty @@sv.lot_futurehold_list(@@lot, reason: @@holdreason), 'future hold has not been set in SiView'
    #
    # remove future hold and send request again with errorIfAlreadyExists true, must pass and set future hold
    assert_equal 0, @@sv.lot_futurehold_cancel(@@lot, nil)
    assert @@rsvc.future_hold(@@lot, nil, post: true, reason: @@holdreason, exists_error: true), 'error calling service'
    refute_empty @@sv.lot_futurehold_list(@@lot, reason: @@holdreason), 'future hold has not been set in SiView'
  end

  def test13_get_mes_user_report
    since = Time.now - 8640000  # duration_in_seconds('100d')
    assert @@rsvc.get_mes_user_report(since), 'error calling service'
  end

  def test14_set_mes_user_certification
    # wrong user
    assert_nil @@rsvc.set_mes_user_certification('doesnotexist', true), 'must be rejected'
    assert_nil @@rsvc.set_mes_user_certification('doesnotexist', false), 'must be rejected'
    #
    testuser = 'OJT-UNITTEST'
    # disable
    assert @@rsvc.set_mes_user_certification(testuser, true), 'error calling service'
    assert_equal '1', @@sv.user_parameter('User', testuser, name: 'DisabledByOJTFlag').value, 'wrong script parameter'
    # enable
    assert @@rsvc.set_mes_user_certification(testuser, false), 'error calling service'
    assert_equal '0', @@sv.user_parameter('User', testuser, name: 'DisabledByOJTFlag').value, 'wrong script parameter'
  end

  def test15_update_user_parameters
    # special user required
    @@sv_xips = SiView::MM.new($env, user: 'X-IPS', password: :default)
    @@rsvc_xips = JCAP::RemoteService.new($env, sv: @@sv_xips)
    pname = 'ERPSalesOrder'
    pvalue = 'QAQA'
    uparams = [{lot: @@lot, name: pname, type: 'STRING', strValue: pvalue}]
    assert @@rsvc_xips.update_user_parameters(uparams), 'error calling service'
    assert_equal pvalue, @@sv.user_parameter('Lot', @@lot, name: pname).value, "user parameter #{pname} not updated"
  end

  # sync lots in backup operation
  def test21_sync_backup_lot
    # create lot and send it to backup site
    assert lot = @@svtest.new_lot(@@newlot_params_bop), 'error creating test lot'
    @@testlots << lot
    assert @@sv.backup_prepare_send_receive(lot, @@svbak, @@bop_params)
    #
    # split lot at backup site, send sync request and ensure the child lot is picked up
    assert lc = @@svbak.lot_split(lot, 10)
    assert !@@sv.lot_family(lot).member?(lc)
    @@rsvc.sync_backup_lot(lot)
    assert wait_for(timeout: 60) {@@sv.lot_family(lot).member?(lc)}, 'error syncing child lot'
    #
    # set lot script parameters at src site, send sync request and ensure child is picked up and parameters set
    assert_equal 0, @@sv.user_parameter_change('Lot', lot, 'TurnkeySubconIDBump', 'QAQA')
    assert lc = @@svbak.lot_split(lot, 2)
    @@rsvc.sync_backup_lot(lot, syncchild: lc)
    assert wait_for(timeout: 60) {@@sv.lot_family(lot).member?(lc)}, 'error syncing child lot'
    assert_equal 'QAQA', @@sv.user_parameter('Lot', lc, name: 'TurnkeySubconIDBump').value
    #
    # return from backup operation
    @@sv.lot_family(lot).each {|l| @@svbak.backup_prepare_return_receive(l, @@sv, @@bop_params)}
  end

  # gen4 interfaces (GID_SOURCE)

  def test41_gen4_nogidsource
    # regular, automotive and security lots
    [@@svtest.product, @@product_atv, @@product_sec].each {|product|
      $log.info "-- test product: #{product}"
      assert lot = @@svtest.new_lot(product: product), 'error creating test lot'
      @@testlots << lot
      liref = @@sv.lot_info(lot)
      assert @@sv.claim_process_lot(lot), 'error processing lot'  # gatepass or opecomp to pick up the lot script params
      # $log.info "waiting 20 s for MDS sync and SiView history"; sleep 20
      assert data = @@rsvc.get_lot_info4(lot), 'error calling get_lot_info4'
      verify_lotinfo4(lot, data)
      $log.info "waiting 20 s for SiView history"; sleep 20
      assert hdata = @@rsvc.get_processed_lot_operation_history4(lot), 'error calling service'
      verify_opehistory4(liref, hdata.last)
    }
  end

  def test42_gen4_nogidsource_lotlabel
    # arbitrarily pick an automotive product; customer gf with GID_SOURCE not set
    assert lot = @@svtest.new_lot(product: @@product_atv), 'error creating test lot'
    @@testlots << lot
    liref = @@sv.lot_info(lot)
    label = Time.now.to_i.hash.to_s
    assert_equal 0, @@sv.user_data_update(:lot, lot, {'LOT_LABEL'=>label})
    assert @@sv.claim_process_lot(lot), 'error processing lot'  # gatepass or opecomp to pick up the lot script params
    # $log.info "waiting 20 s for MDS sync and SiView history"; sleep 20
    assert data = @@rsvc.get_lot_info4(lot), 'error calling get_lot_info4'
    verify_lotinfo4(lot, data)
    $log.info "waiting 20 s for SiView history"; sleep 20
    assert hdata = @@rsvc.get_processed_lot_operation_history4(lot), 'error calling service'
    verify_opehistory4(liref, hdata.last)
    # get data by LOT_LABEL although GID_SOURCE is empty (!)
    assert data = @@rsvc.get_lot_info4(label), 'error calling get_lot_info4'
    verify_lotinfo4(lot, data)              # still the lot ID reported as globalLotId
    assert hdata = @@rsvc.get_processed_lot_operation_history4(label), 'error calling service'
    verify_opehistory4(liref, hdata.last)   # still the lot ID reported as globalLotId
  end

  def test43_gen4_gidsourcelotid_lotlabel
    # like above but customer with GID_SOURCE set to LOT_ID
    assert lot = @@svtest.new_lot(customer: @@customer_lotid), 'error creating test lot'
    @@testlots << lot
    liref = @@sv.lot_info(lot)
    refute_empty label = @@sv.lot_label(lot), 'missing LOT_LABEL'
    refute_equal lot, label, 'wrong LOT_LABEL'
    assert @@sv.claim_process_lot(lot), 'error processing lot'  # gatepass or opecomp to pick up the lot script params
    # $log.info "waiting 20 s for MDS sync and SiView history"; sleep 20
    assert data = @@rsvc.get_lot_info4(lot), 'error calling get_lot_info4'
    verify_lotinfo4(lot, data)
    $log.info "waiting 20 s for SiView history"; sleep 20
    assert hdata = @@rsvc.get_processed_lot_operation_history4(lot), 'error calling service'
    verify_opehistory4(liref, hdata.last)
    # get data by LOT_LABEL although GID_SOURCE is empty (!)
    assert data = @@rsvc.get_lot_info4(label), 'error calling get_lot_info4'
    verify_lotinfo4(lot, data)              # still the lot ID reported as globalLotId
    assert hdata = @@rsvc.get_processed_lot_operation_history4(label), 'error calling service'
    verify_opehistory4(liref, hdata.last)   # still the lot ID reported as globalLotId
  end

  def test44_gen4_gidsource_lotlabel
    # arbitrarily pick an automotive product; customer with GID_SOURCE LOT_LABEL
    assert lot = @@svtest.new_lot(product: @@product_atv, customer: @@customer_gid), 'error creating test lot'
    @@testlots << lot
    liref = @@sv.lot_info(lot)
    refute_empty label = @@sv.lot_label(lot), 'lot has no LOT_LABEL'
    assert @@sv.claim_process_lot(lot), 'error processing lot'  # gatepass or opecomp to pick up the lot script params
    # $log.info "waiting 20 s for MDS sync and SiView history"; sleep 20
    assert data = @@rsvc.get_lot_info4(lot), 'error calling get_lot_info4'
    verify_lotinfo4(lot, data, gid_lot: label)
    $log.info "waiting 20 s for SiView history"; sleep 20
    assert hdata = @@rsvc.get_processed_lot_operation_history4(lot), 'error calling service'
    verify_opehistory4(liref, hdata.last, gid_lot: label)
    # get data by LOT_LABEL
    assert data = @@rsvc.get_lot_info4(label), 'error calling get_lot_info4'
    verify_lotinfo4(lot, data, gid_lot: label)
    assert hdata = @@rsvc.get_processed_lot_operation_history4(label), 'error calling service'
    verify_opehistory4(liref, hdata.last, gid_lot: label)
  end

  def test45_gen4_gidsource_nolotlabel
    # arbitrarily pick an automotive product; customer with GID_SOURCE LOT_LABEL
    assert lot = @@svtest.new_lot(product: @@product_atv, customer: @@customer_gid), 'error creating test lot'
    @@testlots << lot
    assert_equal 0, @@sv.user_data_delete(:lot, lot, 'LOT_LABEL')
    sleep 15
    assert_empty @@sv.lot_label(lot), 'lot has a LOT_LABEL'
    liref = @@sv.lot_info(lot)
    assert @@sv.claim_process_lot(lot), 'error processing lot'  # gatepass or opecomp to pick up the lot script params
    # $log.info "waiting 20 s for MDS sync and SiView history"; sleep 20
    assert data = @@rsvc.get_lot_info4(lot), 'error calling get_lot_info4'
    verify_lotinfo4(lot, data, gid_lot: nil)
    $log.info "waiting 20 s for SiView history"; sleep 20
    assert hdata = @@rsvc.get_processed_lot_operation_history4(lot), 'error calling service'
    verify_opehistory4(liref, hdata.last, gid_lot: nil)
  end

  def testmanual91_MQ_failover
    $nholds = 0
    $nreleases = 0
    $errors = {}
    $n = 0
    # hold
    while $n < 1
      assert lot = @@svtest.new_lot, 'error creating test lot'
      @@testlots << lot
      $n += 1
      $log.info "-- test #{$n}, lot #{lot}"
      begin
        rsvc = JCAP::RemoteService.new($env, sv: @@sv)
        assert rsvc.get_version, 'error executing get_version'
        assert res = rsvc.lot_hold(lot, reason: @@holdreason), 'error calling service'
        $nholds += 1
        hh = @@sv.lot_hold_list(lot, reason: @@holdreason)
        assert_equal 1, hh.size, 'lot has not been set ONHOLD in SiView'
        # release
        assert res = rsvc.release_lot_hold(lot, @@holdreason, reason: @@holdrelease_reason), 'error calling service'
        $nreleases += 1
        hh = @@sv.lot_hold_list(lot, reason: @@holdreason)
        assert_equal 0, hh.size, 'lot has not been released in SiView'
      # rescue Minitest::Assertion => err
      rescue Exception => err
        $log.warn "Assertion error (rescued): #{err}"
        $errors[lot] = [Time.now, err]
      ensure
        @@sv.delete_lot_family(lot)
        sleep 10
      end
    end
  end

  def testmanual99
    rsvc = JCAP::RemoteService.new($env, sv: @@sv)
    assert rsvc.get_version, 'error executing get_version'
  end


  # aux methods

  # verify get_lot_info or get_lot_info2/3 response data
  def verify_lotinfo(lot, data, type=1)
    data[:waferInfo].sort! {|a, b| a[:waferAlias] <=> b[:waferAlias]}
    li = @@sv.lot_info(lot, wafers: true)
    ref = {
      currOperation: {opId: li.op, opName: li.opName, opNumber: li.opNo, routeId: li.route},
      lotId: li.lot, lotType: li.lottype, subLotType: li.sublottype,
      productId: li.product, technology: li.technology, customerCode: li.customer, lotWaferQty: li.nwafers.to_s,
      waferInfo: li.wafers.collect {|w| {slotNumber: w.slot.to_s, waferAlias: w.alias, waferId: w.wafer}}
    }
    if type == 2 || type == 3
      data.delete('xsi:type')
      ref[:automotiveLot] = (@@sv.user_data(:product, li.product)['AutomotiveProduct'] == 'Y').to_s
      ref[:carrierId] = li.carrier
      ref[:lotHoldState] = li.states['Lot Hold State']
      ref[:lotInventoryState] = li.states['Lot Inventory State']
      ref[:lotProcessState] = li.states['Lot Process State']
      ref[:lotProductionState] = li.states['Lot Production State']
      ref[:lotState] = li.states['Lot State']
      ref[:lotStatus] = li.status
      ref[:securityLot] = (@@sv.user_data(:product, li.product)['SecurityProduct'] == 'Y').to_s
      ref[:sourceFabCode] = @@sv.user_parameter('Lot', @@lot, name: 'SourceFabCode').value
      ref[:stageId] = li.stage
    end
    if type == 3
      ref[:carrierCategory] = @@sv.carrier_status(li.carrier).category
    end
    assert verify_hash(ref, data)
  end

  def verify_lotinfo4(lot, data, params={})
    data.delete('xsi:type')
    data[:waferInfo].sort! {|a, b| a[:waferAlias] <=> b[:waferAlias]}
    li = @@sv.lot_info(lot, wafers: true)
    udata_prod = @@sv.user_data(:product, li.product)
    ref = {
      fabLotId: li.lot, globalLotId: params.has_key?(:gid_lot) ? params[:gid_lot] : li.lot,
      waferInfo: li.wafers.collect {|w|
        {slotNumber: w.slot.to_s, waferAlias: "#{lot.split('.').first}.#{w.alias}", waferId: w.wafer}
      },
      currOperation: {opId: li.op, opName: li.opName, opNumber: li.opNo, routeId: li.route},
      lotType: li.lottype, subLotType: li.sublottype, stageId: li.stage,
      productId: li.product, technology: li.technology, customerCode: li.customer, lotWaferQty: li.nwafers.to_s,
      automotiveLot: (udata_prod['AutomotiveProduct'] == 'Y').to_s,
      securityLot: (udata_prod['SecurityProduct'] == 'Y').to_s,
      carrierId: li.carrier, carrierCategory: @@sv.carrier_status(li.carrier).category,
      lotHoldState: li.states['Lot Hold State'],
      lotInventoryState: li.states['Lot Inventory State'],
      lotProcessState: li.states['Lot Process State'],
      lotProductionState: li.states['Lot Production State'],
      lotState: li.states['Lot State'],
      lotStatus: li.status,
      sourceFabCode: @@sv.user_parameter('Lot', lot, name: 'SourceFabCode').value,
    }
    assert verify_hash(ref, data)
  end

  def verify_opehistory4(liref, record, params={})
    assert_equal liref.opNo, record[:operation][:opNumber], 'wrong opNo'
    assert_equal liref.carrier, record[:carrierId], 'wrong carrier'
    assert_equal liref.stage, record[:stageId], 'wrong stage'
    assert_equal liref.lot, record[:fabLotId], 'wrong fabLotId'
    gidlot = params.has_key?(:gid_lot) ? params[:gid_lot] : liref.lot
    if gidlot.nil?
      assert_nil record[:globalLotId], 'wrong globalLotId'
    else
      assert_equal gidlot, record[:globalLotId], 'wrong globalLotId'
    end
  end

end
