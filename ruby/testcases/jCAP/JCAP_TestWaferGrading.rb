=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: Steffen Steidten, 2012-09-20

Version: 18.09

History
  2013-04-16 dsteger,  added some more checks, modified setup
  2013-04-23 ssteidte, added check for RTD rule, parameters need to be set separately for each fab
  2014-09-08 ssteidte, use remote RTDEmulator rules
  2015-01-08 ssteidte, use @@svtest to create test lots
  2015-04-07 ssteidte, cleanup
  2017-01-05 sfrieske, use new RTD::EmuRemote
  2018-03-06 sfrieske, replaced user_parameter_delete_verify by user_parameter_delete
  2018-11-07 sfrieske, minor cleanup
  2019-02-19 sfrieske, improved EmuRemote usage
=end

require 'rtdserver/rtdemu'
require 'rtdserver/rtdemuremote'
require 'SiViewTestCase'


class JCAP_TestWaferGrading < SiViewTestCase
  @@grades = ['UT-MN-GRADING1.01', 'UT-MN-GRADING2.01', 'UT-MN-GRADING3.01']  # STB target product
  @@grade_source = 'QA test'  # logging and STB claim memo
  @@rtd_rule = 'QA_JCAP_TW_STB'
  @@columns = %w(seq cast_id lot_id wafer_id prodspec_id grade grade_source grade_reset position prod_category_id)

  @@job_interval = 310
  @@testlots = []


  def self.after_all_passed
    @@testlots.each {|lot| @@sv.delete_lot_family(lot)}
  end


  def self.shutdown
    @@rtdremote.delete_emustation('', rule: @@rtd_rule)
  end


  def test00_setup
    $setup_ok = false
    #
    @@rtdremote = RTD::EmuRemote.new($env)
    #
    $setup_ok = true
  end

  def test11_single_lot
    $setup_ok = false
    #
    @@rtdremote.add_emustation('', rule: @@rtd_rule, method: 'empty_reply')
    assert lot = @@svtest.new_controllot('Equipment Monitor', bankin: true), 'error creating lot'
    li = @@sv.lot_info(lot, wafers: true)
    grade = @@grades.first
    li.wafers.each {|w| assert_equal 0, @@sv.user_parameter_change('Wafer', w.wafer, 'WfrGrade', grade)}
    #
    # create RTD rule
    data = li.wafers.collect {|w|
      [1, li.carrier, lot, w.wafer, li.product, grade, @@grade_source, 'false', w.slot, li.lottype]
    }
    assert @@rtdremote.add_emustation_wait('', rule: @@rtd_rule, timeout: @@job_interval) {
      @@rtdremote.create_response(@@columns, {'rows'=>data})
    }, 'rule has not been called'
    # verify new lot creation
    sleep 30
    lots = @@sv.carrier_status(li.carrier).lots
    assert_equal 1, lots.size, 'wrong number of lots'
    refute_equal lot, lot_new = lots.first, 'no new lot'
    # verify new lot's parameters
    @@testlots += @@sv.carrier_status(li.carrier).lots
    $log.info "new lot: #{lot_new}"
    $log.info 'waiting 30 s for SiView history'; sleep 30
    # check wafers
    li_new = @@sv.lot_info(lot_new, wafers: true)
    assert_equal grade, li_new.product, 'wrong destination product'
    li_new.wafers.each_with_index {|w, i|
      assert_equal w.wafer, li.wafers[i].wafer, "wrong wafer in lot #{w.lot}"
    }
    # check claim memo
    ##assert verify_claim_memo(lot_new, grade), "wrong claim memo for #{lot_new}"
    memo = @@sv.lot_operation_history(lot_new, first: true, category: 'STB').first.memo
    assert memo =~ /(#{grade}).*(#{@@grade_source})/, 'wrong claim memo'
    # wafer grades not reset
    li_new.wafers.each {|w|
      assert_equal grade, @@sv.user_parameter('Wafer', w.wafer, name: 'WfrGrade').value, 'wrong wafer grade'
    }
    #
    $setup_ok = true
  end

  def test12_multiple_target_lots
    @@rtdremote.add_emustation('', rule: @@rtd_rule, method: 'empty_reply')
    assert lot = @@svtest.new_controllot('Equipment Monitor', bankin: true), 'error creating lot'
    li = @@sv.lot_info(lot, wafers: true)
    carrier = li.carrier
    lots_old = @@sv.carrier_status(carrier).lots
    $log.info "original lot in carrier #{carrier}: #{lots_old}"
    #
    # create RTD rule, 3 new lots with 5 wafers each
    data = []
    @@grades.each_with_index {|grade, i|
      wafers = li.wafers[i*5, 5]
      wafers.each {|w| assert_equal 0, @@sv.user_parameter_change('Wafer', w.wafer, 'WfrGrade', grade)}
      grade_reset = i.odd? ? 'true' : 'false'
      data += wafers.collect {|w|
        [i+1, carrier, lot, w.wafer, li.product, grade, @@grade_source, grade_reset, w.slot, li.lottype]
      }
    }
    assert @@rtdremote.add_emustation_wait('', rule: @@rtd_rule, timeout: @@job_interval) {
      @@rtdremote.create_response(@@columns, {'rows'=>data})
    }, 'rule has not been called'
    #
    # verify STB of the lots
    sleep 30
    @@testlots += @@sv.carrier_status(carrier).lots
    lots_new = @@sv.carrier_status(carrier).lots - lots_old
    $log.info "new lots: #{lots_new.inspect}"
    $log.info "waiting 30s for SiView history"; sleep 30
    @@grades.each_with_index {|grade, i|
      wafers = li.wafers[i*5, 5]
      wlots = wafers.map {|w| @@sv.lot_by_waferid(w.wafer)}.uniq
      assert_equal 1, wlots.count, "expected 1 lot, but got #{wlots.inspect}"
      lot_new = wlots.first
      $log.info "checking lot #{lot_new}"
      # check claim memo
      memo = @@sv.lot_operation_history(lot_new, first: true, category: 'STB').first.memo
      assert memo =~ /(#{grade}).*(#@@grade_source)/, 'wrong claim memo'
      # wafer grades reset?
      exp_grade = i.odd? ? 'NA' : grade
      wafers.each {|w|
        assert_equal exp_grade, @@sv.user_parameter('Wafer', w.wafer, name: 'WfrGrade').value, 'wrong wafer grade'
      }
    }
  end

  def test13_multiple_source_lots
    @@rtdremote.add_emustation('', rule: @@rtd_rule, method: 'empty_reply')
    # create a lot and split off 5 wafers into a 2nd lot
    assert lot1 = @@svtest.new_controllot('Equipment Monitor', bankin: true), 'error creating lot'
    lot2 = @@sv.lot_split(lot1, 5)
    li1 = @@sv.lot_info(lot1, wafers: true)
    li2 = @@sv.lot_info(lot2, wafers: true)
    grade = @@grades.first
    li1.wafers.each {|w| assert_equal 0, @@sv.user_parameter_change('Wafer', w.wafer, 'WfrGrade', grade)}
    li2.wafers.each {|w| assert_equal 0, @@sv.user_parameter_change('Wafer', w.wafer, 'WfrGrade', grade)}
    carrier = li1.carrier
    lots_old = @@sv.carrier_status(carrier).lots
    $log.info "original lots in carrier #{carrier}: #{lots_old.inspect}"
    #
    # create RTD rule, 1 new lot with 1 wafer from each source lot
    w1 = li1.wafers[-1]
    w2 = li2.wafers[-1]
    data = [
      [1, carrier, lot1, w1.wafer, li1.product, grade, @@grade_source, 'true', w1.slot, li1.lottype],
      [1, carrier, lot2, w2.wafer, li2.product, grade, @@grade_source, 'true', w2.slot, li2.lottype]
    ]
    assert @@rtdremote.add_emustation_wait('', rule: @@rtd_rule, timeout: @@job_interval) {
      @@rtdremote.create_response(@@columns, {'rows'=>data})
    }, 'rule has not been called'
    #
    # verify the new lot's parameters
    sleep 30
    lots = @@sv.carrier_status(carrier).lots
    @@testlots += lots
    assert_equal 1 + lots_old.size, lots.size, "wrong lots in carrier #{carrier}: #{lots.inspect}"
    lot_new = (lots - lots_old)[0]
    $log.info "new lot: #{lot_new}"
    $log.info "waiting 30 s for SiView history"; sleep 30
    li_new = @@sv.lot_info(lot_new, wafers: true)
    assert_equal 2, li_new.nwafers
    [w1, w2].sort {|a, b| a.slot <=> b.slot}.each_with_index {|w, i|
      assert_equal w.wafer, li_new.wafers[i].wafer, "wrong wafer in lot #{lot_new}"
      assert_equal w.slot, li_new.wafers[i].slot, "wrong wafer position in lot #{lot_new}"
    }
    assert_equal grade, li_new.product, 'wrong destination product'
    #assert verify_claim_memo(lot_new, grade), "wrong claim memo for #{lot_new}"
    memo = @@sv.lot_operation_history(lot_new, first: true, category: 'STB').first.memo
    assert memo =~ /(#{grade}).*(#{@@grade_source})/, 'wrong claim memo'
    li_new.wafers.each {|w|
      assert_equal 'NA', @@sv.user_parameter('Wafer', w.wafer, name: 'WfrGrade').value, 'wrong wafer grade'
    }
  end

end
