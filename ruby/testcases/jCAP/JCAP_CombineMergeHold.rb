=begin
(c) Copyright 2011 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: Daniel Steger, 2011-03-17
Version: 21.01

History:
  2012-08-03 adarwais, added testcases
  2014-10-14 ssteidte, reworked preparation and verification routines, added test127 for v14.10
  2015-04-08 ssteidte, use SJC::Test and cleanup
  2017-06-01 sfrieske, minor fixes
  2018-02-20 sfrieske, rewritten for speedup, setting up all test cases at once
  2018-11-07 sfrieske, minor cleanup
  2020-02-12 sfrieske, removed use of _guess_op_route
  2021-01-25 sfrieske, cleanup, added TCs for v21.01
=end

require 'SiViewTestCase'
require 'jcap/sjctest'


class JCAP_CombineMergeHold < SiViewTestCase
  @@sv_defaults = {carriers: 'JC%'}
  @@other_carrier_category = 'BEOL'
  @@rework_route = 'UTRW001.01'
  @@rework_opNo = '1000.400'
  @@hold_extra = 'ENG'
  @@job_interval = 220    # 3 min + slack time


  def self.after_all_passed
    @@testlots.each {|lot| @@sv.delete_lot_family(lot)}
    @@sjctest.cleanup_sort_requests(creator: 'COMBINELOTSMGHL')
  end


  def test00_setup
    $setup_ok = false
    #
    # @@sjctest = SJC::Test.new($env, @@sv_defaults.merge(carriers: 'JC%', sv: @@sv, sm: @@svtest.sm))
    @@sjctest = SJC::Test.new($env, sv: @@sv, sm: @@svtest.sm)
    @@mds = @@sjctest.mds
    assert @@sjctest.cleanup_sort_requests(creator: 'COMBINELOTSMGHL'), 'pending SRs'
    #
    assert @@svtest.get_empty_carrier(n: 53), 'not enough empty carriers'
    assert @@svtest.get_empty_carrier(n: 2, carrier_category: @@other_carrier_category), 'not enough empty BEOL carriers'
    #
    # lot setup, sublottype RD prevents AutoScrap job to interfere with scrapped lots
    assert @@testlots = @@svtest.new_lots(20, sublottype: 'RD'), 'error creating test lots'
    fillers = 8.times.collect {@@sv.lot_split(@@testlots.last, 3)}
    fidx = -1
    assert mergeop = @@sv.lot_operation_list(@@testlots.last, raw: true)[1].operationNumber
    @@lots = []   # collect all lots for final locate
    @@carriers = []
    @@testlots[0..-2].each_with_index {|lot, i|
      next if [2, 13, 15].member?(i)  # specific split setup
      cat = i == 16 ? @@other_carrier_category : @@svtest.carrier_category
      @@lots[i] = [lot]
      @@carriers[i] = [@@sv.lot_info(lot).carrier]
      # split off 1st child lot with 5 wafers
      assert ec1 = @@svtest.get_empty_carrier(carrier_category: cat, stockin: true), 'no empty carrier'
      assert lc1 = @@sv.lot_split(lot, 5, mergeop: mergeop), 'error splitting lot'
      assert_equal 0, @@sv.wafer_sort(lc1, ec1)
      @@lots[i] << lc1
      @@carriers[i] << ec1
      # split off 2nd child lot with 6 wafers (preferred over lc1 as sort target)
      assert ec2 = @@svtest.get_empty_carrier(carrier_category: cat, stockin: true), 'no empty carrier'
      assert lc2 = @@sv.lot_split(lot, 6, mergeop: mergeop), 'error splitting lot'
      assert_equal 0, @@sv.wafer_sort(lc2, ec2)
      @@lots[i] << lc2
      @@carriers[i] << ec2
    }
    @@vargs = []
    @@nosrcarriers = []
    #
    # 2 children (happy)
    i = 0
    @@vargs[i] = [@@lots[i][1..2], @@carriers[i].first]
    #
    # not enough space for all lots of lot family in parent carrier, combine into child2 carrier
    i = 1
    assert_equal 0, @@sv.wafer_sort(fillers[fidx += 1], @@carriers[i][0], slots: 'empty')
    @@vargs[i] = [@@lots[i][0..1], @@carriers[i][2]]
    #
    # REHL after PartialRework
    i = 2
    lot = @@testlots[i]
    assert_equal 0, @@sv.lot_opelocate(lot, @@rework_opNo)
    # start partial rework with a few wafers, child lot will get the sort task
    assert lc = @@sv.lot_partial_rework(lot, 2, @@rework_route)
    # move lot into a separate carrier and conplete partial rework (returning to original PD)
    assert ec = @@svtest.get_empty_carrier(stockin: true), 'no empty carrier'
    assert_equal 0, @@sv.wafer_sort(lc, ec)
    assert_equal 0, @@sv.lot_opelocate(lc, @@rework_opNo)
    @@vargs[i] = [[lc], @@sv.lot_info(lot).carrier]
    #
    # not enough space for all lots of lot family in parent & child2 carriers, combine into child1 carrier
    i = 3
    assert_equal 0, @@sv.wafer_sort(fillers[fidx += 1], @@carriers[i][0], slots: 'empty')
    assert_equal 0, @@sv.wafer_sort(fillers[fidx += 1], @@carriers[i][2], slots: 'empty')
    @@vargs[i] = [[@@lots[i].first, @@lots[i][2]], @@carriers[i][1]]
    #
    # not enough space for all lots of lot family in all carriers, combine into an empty carrier
    i = 4
    @@vargs[i] = [@@lots[i], 'EMPTY']
    assert_equal 0, @@sv.wafer_sort(fillers[fidx += 1], @@carriers[i][0], slots: 'empty')
    assert_equal 0, @@sv.wafer_sort(fillers[fidx += 1], @@carriers[i][1], slots: 'empty')
    assert_equal 0, @@sv.wafer_sort(fillers[fidx += 1], @@carriers[i][2], slots: 'empty')
    #
    # parent and child1 in one carrier -> combine child2 into the parent's carrier
    i = 5
    assert_equal 0, @@sv.wafer_sort(@@lots[i][1], @@carriers[i].first)
    @@vargs[i] = [[@@lots[i][2]], @@carriers[i].first]
    #
    # both children in one carrier -> combine children into the parent's carrier
    i = 6
    assert_equal 0, @@sv.wafer_sort(@@lots[i][1], @@carriers[i][2])
    @@vargs[i] = [@@lots[i][1..2], @@carriers[i].first]
    #
    # special lot conditions
    #
    # child 1 is scrapped, combine child2 only
    i = 7
    assert_equal 0, @@sv.scrap_wafers(@@lots[i][1])
    @@vargs[i] = [[@@lots[i][2]], @@carriers[i].first]
    @@nosrcarriers[i] = [@@carriers[i][1]]
    #
    # parent scrapped, no combine
    i = 8
    assert_equal 0, @@sv.scrap_wafers(@@lots[i].first)
    @@nosrcarriers[i] = @@carriers[i]
    #
    # no merge hold for child1, combine child2 only
    i = 9
    assert_equal 0, @@sv.lot_futurehold_cancel(@@lots[i][1], 'MGHL')
    @@vargs[i] = [[@@lots[i][2]], @@carriers[i].first]
    @@nosrcarriers[i] = [@@carriers[i][1]]
    #
    # no merge hold for parent, no combine
    i = 10
    assert_equal 0, @@sv.lot_futurehold_cancel(@@lots[i].first, 'MGHL')
    @@nosrcarriers[i] = @@carriers[i]
    #
    # child1 is scrapped, no merge hold for child2, additional hold for child3, no combine
    i = 11
    assert_equal 0, @@sv.scrap_wafers(@@lots[i][1])
    assert_equal 0, @@sv.lot_futurehold_cancel(@@lots[i][2], 'MGHL')
    @@nosrcarriers[i] = @@carriers[i]
    assert lc3 = @@sv.lot_split(@@lots[i].first, 2, mergeop: mergeop)
    @@lots[i] << lc3
    assert ec3 = @@svtest.get_empty_carrier(stockin: true), 'no empty carrier'
    assert_equal 0, @@sv.wafer_sort(lc3, ec3)
    assert_equal 0, @@sv.lot_hold(lc3, @@hold_extra)
    @@nosrcarriers[i] << ec3
    #
    # children at merge point but parent is not, no combine
    i = 12
    @@nosrcarriers[i] = @@carriers[i]
    #
    # children have different merge opNos, combine child1 with matching opNo only
    i = 13
    lot = @@testlots[i]
    assert ec1 = @@svtest.get_empty_carrier(stockin: true), 'no empty carrier'
    assert lc1 = @@sv.lot_split(lot, 5, mergeop: mergeop), 'error splitting lot'
    assert_equal 0, @@sv.wafer_sort(lc1, ec1)
    assert mergeop2 = @@sv.lot_operation_list(@@testlots.last, raw: true)[2].operationNumber
    assert ec2 = @@svtest.get_empty_carrier(stockin: true), 'no empty carrier'
    assert lc2 = @@sv.lot_split(lot, 5, mergeop: mergeop2), 'error splitting lot'
    assert_equal 0, @@sv.wafer_sort(lc2, ec2)
    @@lots[i] = [lot, lc1, lc2]
    @@vargs[i] = [[lc1], @@sv.lot_info(lot).carrier]
    @@nosrcarriers[i] = [ec2]
    #
    # grandchild with manually set merge holds
    i = 14
    lot, lc1, lc2 = @@lots[i]
    assert lgc = @@sv.lot_split(lc1, 1), 'error splitting lot'
    assert_equal 0, @@sv.wafer_sort(lc1, @@sv.lot_info(lot).carrier)
    assert_equal 0, @@sv.lot_futurehold_cancel(lot, 'MGHL', related_lot: lc1)
    assert_equal 0, @@sv.lot_futurehold_cancel(lc1, 'MGHL', related_lot: lot)
    assert_equal 0, @@sv.lot_merge(lot, lc1)
    assert_equal 0, @@sv.lot_futurehold(lot, mergeop, 'MGHL', holdtype: 'MergeHold', related_lot: lgc)
    assert_equal 0, @@sv.lot_futurehold(lgc, mergeop, 'MGHL', holdtype: 'MergeHold', related_lot: lot)
    @@lots[i] = [lot, lgc, lc2]
    @@vargs[i] = [[lgc, lc2], @@carriers[i].first]
    #
    # brothers with manually set merge holds
    i = 15
    lot = @@testlots[i]
    assert ec1 = @@svtest.get_empty_carrier(stockin: true), 'no empty carrier'
    assert lc1 = @@sv.lot_split(lot, 20, mergeop: mergeop), 'error splitting lot'
    assert_equal 0, @@sv.wafer_sort(lc1, ec1)
    lgcs = []
    2.times.collect {
      assert ec = @@svtest.get_empty_carrier(stockin: true), 'no empty carrier'
      assert lgc = @@sv.lot_split(lc1, 5, mergeop: mergeop), 'error splitting lot'
      assert_equal 0, @@sv.wafer_sort(lgc, ec)
      # 1 great grandchild each that gets scrapped
      assert lggc = @@sv.lot_split(lgc, 2, mergeop: mergeop)
      assert_equal 0, @@sv.scrap_wafers(lggc)
      lgcs << lgc
    }
    # set merge holds between the grandchildren
    assert_equal 0, @@sv.lot_futurehold(lgcs[0], mergeop, 'MGHL', holdtype: 'MergeHold', related_lot: lgcs[1])
    assert_equal 0, @@sv.lot_futurehold(lgcs[1], mergeop, 'MGHL', holdtype: 'MergeHold', related_lot: lgcs[0])
    @@lots[i] = [lot, lc1] + lgcs
    @@vargs[i] = [[lot] + lgcs, @@sv.lot_info(lc1).carrier]  # 20.01: combined into carrier with most wafers (lc1)
    #
    # different carrier category, no combine
    i = 16
    @@nosrcarriers[i] = @@carriers[i]
    #
    # parent's carrier NOTAVAILABLE, no combine
    i = 17
    assert_equal 0, @@sv.carrier_status_change(@@carriers[i].first, 'NOTAVAILABLE')
    @@nosrcarriers[i] = @@carriers[i]
    #
    # different special conditions -> no combine
    i = 18
    @@nosrcarriers[i] = @@carriers[i]
    # prepare for child in a FOSB (set hold, release after locate and wafer_sort)
    lot_no_carrier = @@lots[i][1]
    assert_equal 0, @@sv.lot_hold(lot_no_carrier, @@hold_extra)
    # carrier state not MI, SI, SO
    assert_equal 0, @@sv.carrier_xfer_status_change(@@carriers[i][2], 'EO', 'UTF001')
    # carrier already has an SR
    assert lc3 = @@sv.lot_split(@@lots[i].first, 2, mergeop: mergeop), 'error splitting lot'
    @@lots[i] << lc3
    assert ec3 = @@svtest.get_empty_carrier(stockin: true), 'no empty carrier'
    assert_equal 0, @@sv.wafer_sort(lc3, ec3)
    assert ec4 = @@svtest.get_empty_carrier(stockin: true), 'no empty carrier'
    assert @@sjctest.sjcrequest.create('COMBINE',
      lots: [lc3], srccarriers: [ec3], tgtcarriers: ['EMPTY'], requestor: 'COMBINELOTSMGHL')
    refute_empty @@sjctest.mds.sjc_tasks(lot: lc3), 'no SR created'
    @@nosrcarriers[i] << ec3
    #
    # locate all lots to the mergeop and wait for the jCAP job
    @@lots.each_with_index {|lots, i|
      next if i == 2  # Rework lot
      lots.each_with_index {|lot, lotidx|
        next if i == 12 && lotidx == 0  # specific parent lot setup
        next if @@sv.lot_info(lot).status == 'SCRAPPED'
        assert_equal 0, @@sv.lot_opelocate(lot, mergeop, force: :ondemand), "error locating lots ##{i}, lot ##{lotidx}"
      }
    }
    assert_equal 0, @@sv.wafer_sort(lot_no_carrier, '')
    assert_equal 0, @@sv.lot_hold_release(lot_no_carrier, @@hold_extra)
    #
    @@lotstested = []
    ($log.info "waiting #{@@job_interval} s for the jCAP job"; sleep @@job_interval)
    #
    $setup_ok = true
  end

  def test11_sr
    ok = true
    @@vargs.each_index {|idx|
      lots, tgtcarrier = @@vargs[idx]
      next if lots.nil?   # no SR expected
      @@lotstested[idx] = true
      $log.info "-- verifying lots ##{idx}"
      assert sts = @@mds.sjc_tasks(srclot: lots)
      if sts.size != lots.size
        $log.warn "wrong number of SJC tasks (expected ##{lots.size}:\n#{sts.inspect}"
        ok=false
        next
      end
      # verify sort tasks
      srid = sts.first.srid
      sts.each_with_index {|st, idx|
        ($log.warn "  more than one SJC sort request: #{st.srid}"; ok=false; next) if st.srid != srid
        ($log.warn "  wrong tgtcarrier: #{st.tgtcarrier}"; ok=false; next) if st.tgtcarrier != tgtcarrier
      }
    }
    assert ok, 'not all test lots have the expected SRs'
  end

  def test12_nosr
    ok = true
    @@nosrcarriers.each_with_index {|cc, idx|
      next unless cc
      @@lotstested[idx] = true
      $log.info "-- verifying lots ##{idx}"
      srs = @@mds.sjc_tasks(srccarrier: cc).select {|e| e.tgtcarrier != 'EMPTY'}
      ($log.warn "unexpected SRs: #{srs}"; ok=false) unless srs.empty?
    }
    assert ok, 'some test lots have unexpected SRs'
  end

  def test19_summary
    ok = true
    @@lotstested.each_with_index {|v, i|
      next if v
      $log.warn "no verification for lots ##{i}"
      ok = false
    }
    assert ok, 'not all test lots have been verified'
  end

end
