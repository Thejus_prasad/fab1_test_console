=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: Steffen Steidten, 2010-05-03

Version: 21.03

History:
  2012-01-11 Beate Kochinka, generalization for 1ERP and SAP related tests
  2012-06-18 Adel Darwaish, refactored, use ERP only
  2014-03-17 ssteidte, simplified CountryOfOrigin tests by setting SM UDATA in test preparation
  2017-01-04 sfrieske, use default carriers
  2018-03-12 sfrieske, minor code cleanup after separation of erpmdm.rb from erpmes.rb
  2018-10-11 sfrieske, unified MDM query
  2021-02-01 sfrieske, major cleanup, added tests for lot note (MSR 1106645)
  2021-07-02 sfrieske, added test for ASN=Y
  2021-08-27 sfrieske, require waitfor instead of misc
=end

require 'jcap/erpmdm'
require 'util/waitfor'
require 'SiViewTestCase'


class JCAP_STBMfgWIPAttributes < SiViewTestCase
  @@sv_defaults = {carriers: 'JC%'}
  @@sourcefabcode = 'F1'
  @@coo_default = 'DE'
  @@coo_alt = 'QA'  # any name is accepted
  @@erporder = '1234567:5:05'
  @@targets = ['HOT', 'COLD', 'MEDIUM']
  @@dpml = 1.5
  @@fsd = Time.now
  @@customer1 = 'ibm'
  @@erpitem1 = '3006AA-UA0'
  @@customer2 = 'qgt'
  @@erpitem2 = '*HALCYONV21BF1-QE-JB2'
  @@jobdelay = 60
  @@srcproducts = []

  @@testlots = []


  def self.after_all_passed
    @@testlots.each {|lot| @@sv.delete_lot_family(lot)}
  end

  def self.shutdown
    @@srcproducts.each {|p| @@sm.update_udata(:product, p, {'CountryOfOrigin'=>''})}
  end


  def test00_setup
    $setup_ok = false
    #
    @@mdm = ERPMDM.new($env)
    # ensure DefaultCustomerQualityCode is set for IBM customer only
    assert @@sv.user_data(:customer, @@customer1)['DefaultCustomerQualityCode'], "no DefaultCustomerQualityCode for #{@@customer1}"
    assert_nil @@sv.user_data(:customer, @@customer2)['DefaultCustomerQualityCode'], "wrong DefaultCustomerQualityCode for #{@@customer2}"
    #
    # data for customer1 lots
    assert @@mdmentry1 = @@mdm.mdm_entries(erpitem: @@erpitem1).first, "no MDM data for erp item #{@@erpitem1}"
    $log.info "MDM entry #1: #{@@mdmentry1.inspect}"
    assert @@targets.member?(@@mdmentry1.target), "wrong target: #{@@mdmentry1.target}"
    # set UDATA for srcproduct1 to empty
    assert product1 = @@mdmentry1.product
    assert srcprod1 = @@sm.object_info(:product, product1).first.specific[:srcproducts].first
    @@srcproducts << srcprod1
    assert @@sm.update_udata(:product, srcprod1, {'CountryOfOrigin'=>''})
    #
    # data for customer2 lots
    assert @@mdmentry2 = @@mdm.mdm_entries(erpitem: @@erpitem2).first, "no MDM data for erp item #{@@erpitem2}"
    $log.info "MDM entry #2: #{@@mdmentry2.inspect}"
    assert @@targets.member?(@@mdmentry2.target), "wrong target: #{@@mdmentry2.target}"
    # set UDATA for srcproduct2 to alternate CountryOfOrigin ('QA')
    product2 = @@mdmentry2.product
    assert srcprod2 = @@sm.object_info(:product, product2).first.specific[:srcproducts].first
    @@srcproducts << srcprod2
    refute_equal srcprod1, srcprod2, 'srcproducts must be different'
    assert @@sm.update_udata(:product, srcprod2, {'CountryOfOrigin'=>@@coo_alt})
    #
    # create test lots
    params_c1 = {customer: @@customer1, product: product1, srproduct: srcprod1, dpml: @@dpml, fsd: @@fsd, asn: false}
    params_c2 = {customer: @@customer2, product: product2, srproduct: srcprod2, dpml: @@dpml, fsd: @@fsd, asn: false}
    # valid  ERPItem, check with different COOs
    assert @@lot1_c1 = @@svtest.new_lot(params_c1.merge(erpitem: @@erpitem1))
    @@testlots << @@lot1_c1
    assert @@lot1_c2 = @@svtest.new_lot(params_c2.merge(erpitem: @@erpitem2, erporder: @@erporder))
    @@testlots << @@lot1_c2
    # invalid ERPItem
    assert @@lot2_c1 = @@svtest.new_lot(params_c1.merge(erpitem: 'XXXXXX'))
    @@testlots << @@lot2_c1
    assert @@lot2_c2 = @@svtest.new_lot(params_c2.merge(erpitem: 'XXXXXX'))
    @@testlots << @@lot2_c2
    # missing ERPItem
    assert @@lot3_c1 = @@svtest.new_lot(params_c1.merge(erpitem: ''))
    @@testlots << @@lot3_c1
    assert @@lot3_c2 = @@svtest.new_lot(params_c2.merge(erpitem: ''))
    @@testlots << @@lot3_c2
    # ASN=Y
    assert @@lot4_c1 = @@svtest.new_lot(customer: @@customer1, product: product1, srproduct: srcprod1, asn: true)
    @@testlots << @@lot4_c1
    assert @@lot4_c2 = @@svtest.new_lot(customer: @@customer2, product: product2, srproduct: srcprod2, asn: true)
    @@testlots << @@lot4_c2
    #
    $log.info "waiting #{@@jobdelay} s for the jCAP job to run"; sleep @@jobdelay
    #
    $setup_ok = true
  end

  def test11_lot1_c1
    # lot note (MSR 1106645)
    note = nil
    assert wait_for(timeout: 60) {note = @@sv.lot_notes(@@lot1_c1).first}, 'no lot note'
    assert_equal 'STBMfgWIPAttributes', note.title, 'wrong lot note title'
    assert_equal 'STBMfgWIPAttributes successfully completed', note.desc, 'wrong note desc'
    # no lot hold
    assert_empty @@sv.lot_hold_list(@@lot1_c1)
    # lot script params
    assert uparams = @@sv.user_parameter('Lot', @@lot1_c1)
    # ERPItem
    assert_equal @@erpitem1, uparams.find {|e| e.name == 'ERPItem'}.value, 'wrong ERPItem'
    # no ERPSalesOrder
    refute uparams.find {|e| e.name == 'ERPSalesOrder'}, 'ERPSalesOrder must not be set'
    # GateCD_Target_PLN
    assert_equal @@mdmentry1.target, uparams.find {|e| e.name == 'GateCD_Target_PLN'}.value, 'wrong GateCD_Target_PLN'
    # TurnkeyType
    assert_equal @@mdmentry1.tktype, uparams.find {|e| e.name == 'TurnkeyType'}.value, 'wrong TurnkeyType'
    # CustomerQualityCode
    assert defcode = @@sv.user_data(:customer, @@customer1)['DefaultCustomerQualityCode']
    assert_equal defcode, uparams.find {|e| e.name == 'CustomerQualityCode'}.value, 'wrong CustomerQualityCode'
    # FODs
    verify_FODs(@@lot1_c1, uparams)
    # SourceFabCode
    assert_equal @@sourcefabcode, uparams.find {|e| e.name == 'SourceFabCode'}.value, 'wrong SourceFabCode'
    # CountryOfOrigin
    assert_equal @@coo_default, uparams.find {|e| e.name == 'CountryOfOrigin'}.value, 'wrong CountryOfOrigin'
  end

  def test21_lot1_c2
    # lot note (MSR 1106645)
    note = nil
    assert wait_for(timeout: 60) {note = @@sv.lot_notes(@@lot1_c2).first}, 'no lot note'
    assert_equal 'STBMfgWIPAttributes', note.title, 'wrong lot note title'
    assert_equal 'STBMfgWIPAttributes successfully completed', note.desc, 'wrong note desc'
    # no lot hold
    assert_empty @@sv.lot_hold_list(@@lot1_c2)
    # lot script params
    assert uparams = @@sv.user_parameter('Lot', @@lot1_c2)
    # ERPItem
    assert_equal @@erpitem2, uparams.find {|e| e.name == 'ERPItem'}.value, 'wrong ERPItem'
    # ERPSalesOrder
    assert_equal @@erporder, uparams.find {|e| e.name == 'ERPSalesOrder'}.value, 'wrong ERPSalesOrder'
    # GateCD_Target_PLN
    assert_equal @@mdmentry2.target, uparams.find {|e| e.name == 'GateCD_Target_PLN'}.value, 'wrong GateCD_Target_PLN'
    # TurnkeyType
    assert_equal @@mdmentry2.tktype, uparams.find {|e| e.name == 'TurnkeyType'}.value, 'wrong TurnkeyType'
    # no CustomerQualityCode
    refute uparams.find {|e| e.name == 'CustomerQualityCode'}, 'wrong CustomerQualityCode'
    # FODs
    verify_FODs(@@lot1_c2, uparams)
    # SourceFabCode
    assert_equal @@sourcefabcode, uparams.find {|e| e.name == 'SourceFabCode'}.value, 'wrong SourceFabCode'
    # CountryOfOrigin
    assert_equal @@coo_alt, uparams.find {|e| e.name == 'CountryOfOrigin'}.value, 'wrong CountryOfOrigin'
  end

  # check lots2 and 3 of customers 1 and 2 (ERPITEM wrong or not set), nothing set except COO

  def test31_onhold
    keywords = %w(ERPItem GateCD_Target_PLN TurnkeyType FOD_INIT FOD_COMMIT FabDPML FSD CustomerQualityCode)
    [@@lot2_c1, @@lot3_c1, @@lot2_c2, @@lot3_c2].each {|lot|
      $log.info "verify hold #{lot.inspect}"
      assert hold = @@sv.lot_hold_list(lot, reason: 'X-M2').first, 'missing X-M2 hold'
      keywords.each {|k| assert hold.memo.include?(k), "missing text #{k.inspect} in claim memo"}
      assert_empty @@sv.lot_notes(lot), 'wrong lot note'
    }
  end

  def test32_noparams
    # no lot script params except CountryOfOrigin
    [@@lot2_c2, @@lot3_c2].each {|lot|
      lparams = @@sv.user_parameter('Lot', lot).select {|p| p.name != 'CountryOfOrigin'}
      assert_empty lparams, "wrong user parameter: #{lparams.inspect}"
    }
  end

  # # COO must be set even if no EREPITEM provided: BROKEN in code long since

  # def XXtest33_COO_c1 # BROKEN in code
  #   [@@lot2_c1, @@lot3_c1].each {|lot|
  #     assert_equal @@coo_default, @@sv.user_parameter('Lot', lot, name: 'CountryOfOrigin').value, 'wrong CountryOfOrigin'
  #   }
  # end

  # def XXtest33_COO_c2 # BROKEN in code
  #   [@@lot2_c2, @@lot3_c2].each {|lot|
  #     assert_equal @@coo_alt, @@sv.user_parameter('Lot', lot, name: 'CountryOfOrigin').value, 'wrong CountryOfOrigin'
  #   }
  # end

  def test41_asn
    # no lot script params at all
    [@@lot4_c1, @@lot4_c2].each {|lot|
      assert_empty @@sv.user_parameter('Lot', lot), 'wrong user parameter'
      assert note = @@sv.lot_notes(lot).first, 'no lot note'
      assert_equal 'STBMfgWIPAttributes', note.title
      assert_equal 'STBMfgWIPAttributes successfully completed for ASN=Y', note.desc
    }
  end


  # aux methods

  def verify_FODs(lot, uparams)
    assert_equal @@dpml, uparams.find {|e| e.name == 'FabDPML'}.value.to_f, 'wrong FabDPML'
    assert_equal @@fsd.strftime('%F'), uparams.find {|e| e.name == 'FSD'}.value, 'wrong FSD'
    due = @@sv.lots_info_raw(lot).strLotInfo.first.strLotBasicInfo.dueTimeStamp[0..9] # year-month-day
    assert_equal due, uparams.find {|e| e.name == 'FOD_INIT'}.value, 'wrong FOD_INIT'
    assert_equal due, uparams.find {|e| e.name == 'FOD_COMMIT'}.value, 'wrong FOD_COMMIT'
  end

end
