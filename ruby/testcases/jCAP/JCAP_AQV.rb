=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: Steffen Steidten, Conny Stenzel 2014-05-13

Version: 20.08

History:
  2015-09-10 ssteidte, added TC0400b for SPC fail after an allowed gatepass
  2016-01-20 sfrieske, added TC0103 for long pre-1 scripts after STB
  2016-04-08 sfrieske, added verification of final message to SAP
  2016-05-05 sfrieske, added TC0104 for 25 parallel bankin events
  2016-05-30 sfrieske, changed kit evaluation details to better log eval status (MDS, AQV)
  2017-03-10 sfrieske, added test case for EI VPDCompletionNotification, use earlycompletion to speed up most tests
  2017-11-20 sfrieske, use struct_to_hash instead of Struct.to_hash
  2018-02-06 sfrieske, added TCs 07xx with WBTQ kit grouping (WBTQ 18.2.2)
  2018-11-07 sfrieske, minor cleanup, remove kit grouping tests
  2020-09-14 sfrieske, minor fixes
  2021-08-27 sfrieske, require waitfor instead of misc
=end

require 'jcap/aqvtest'
require 'util/waitfor'
require 'RubyTestCase'


class JCAP_AQV < RubyTestCase
  @@srccarrier = 'TQ01'
  @@loops = 5               # for parallel tests in test104
  @@eqpwrong = 'QACLUST02'  # for test309
  @@evaldelay = 160         # bankin.event.handling.delay.seconds=150 + slack


  def self.shutdown
    @@aqvtest.wbtqtest.prepare_resources([])  # make all carriers NOTAVAILABLE, remove routes' pre-1 sleep
  end

  def setup
    return unless self.class.class_variable_defined?(:@@aqvtest)
    @@aqvtest.sap.mq.delete_msgs(nil)
    @@sv.eqp_status_change(@@aqvtest.wbtqtest.eqp, '2WPR')
  end


  def test00_setup
    $setup_ok = false
    #
    @@aqvtest = AQV::Test.new($env, sv: @@sv, srccarrier: @@srccarrier, evaldelay: @@evaldelay)
    # ensure AQV listens on the proper queue, the same one SIL writes to
    assert @@aqvtest.spc.mq.send_msg('QA')
    sleep 3
    assert_equal 0, @@aqvtest.spc.mq.qdepth, "wrong AQV_SIL queue: #{@@aqvtest.spc.mq.inspect}, check with the AQV admin"
    #
    $setup_ok = true
  end

  def test100_spcevent_success
    $setup_ok = false
    #
    # happy scenario
    assert lorder = @@aqvtest.create_aqv_kit
    assert @@aqvtest.complete_kit_verify('ACCEPT', leadingorder: lorder)  # only for trouble shooting: wait_aqv_lot: true, wait_aqv_spc: true)
    # bankout, bankin, split since v16.01 (MDS changes)
    assert aqvlots = @@aqvtest.mds.aqv_lot(leadingorder: lorder)
    splitlots = []
    aqvlots.each {|aqvlot|
      assert_equal 0, @@sv.lot_bankin_cancel(aqvlot.lot)
      sleep 3
      assert_equal 0, @@sv.lot_bankin(aqvlot.lot)
      sleep 3
      if @@sv.lot_info(aqvlot.lot).nwafers > 1
        assert @@sv.lot_split(aqvlot.lot, 1)
        splitlots << aqvlot.lot
      end
    }
    refute_empty splitlots, 'no lots split'
    $log.info 'waiting 3 min for the MDS'; sleep 180
    assert aqvlots2 = @@aqvtest.mds.aqv_lot(leadingorder: lorder)
    aqvlots2.each_index {|i|
      aqvlot1 = aqvlots[i]
      aqvlot2 = aqvlots[i]
      [:lot, :bankin_time, :split_flag, :status].each {|member|
        assert_equal aqvlot1[member], aqvlot2[member], "wrong update in AQV_LOTS\nexpected: #{aqvlot1}\ngot: #{aqvlot2}"
      }
    }
    #
    $setup_ok = true
  end

  def test101_spcevent_fail
    # basic fail scenario
    $setup_ok = false
    assert @@aqvtest.complete_kit_verify('REJECT', spcresult: false)  # only for trouble shooting: wait_aqv_lot: true, wait_aqv_spc: true)
    $setup_ok = true
  end

  def test103_wbtq_long
    # happy scenario, WBTQ STB of all lots takes very long (whole kit > 3min)
    assert @@aqvtest.wbtqtest.prepare_resources([@@aqvtest.srccarrier]), 'error preparing resources'
    assert routes = @@aqvtest.wbtqtest.kit_routes(@@aqvtest.kit)
    refute_empty routes, 'wrong setup'
    t = 100   # short enough for STB to pass, long enough for MDS-AQV to fail before 16.01
    assert t * routes.size > 200, 'STB sleep must be short enough for STB to pass, long enough for MDS-AQV to fail before 16.01'
    routes.uniq.each {|r|
      assert_equal 0, @@sv.user_parameter_change('Route', r, @@aqvtest.wbtqtest.stb_routeparameter, t)
    }
    tstart = Time.now
    lorder = @@aqvtest.create_aqv_kit(prepare_resources: false, wbtq_timeout: t * routes.size + 150)
    assert Time.now - tstart > t * routes.size, 'was too quick, wrong setup'
    assert lorder, 'not all kit lots are in AQV_LOT'
  end

  def test104_parallel_bankin
    @@loops.times {|i|
      $log.info "\n-- loop #{i+1}/#{@@loops}"
      assert @@aqvtest.complete_kit_verify('ACCEPT', kit: 'QA-KIT25', evaldelay: 300, earlycompletion: true, process_lots_parallel: true), "failed in loop #{i+1}"
    }
  end

  def test105_vpd_bankin
    assert lorder = @@aqvtest.create_aqv_kit
    assert aqvlots = @@aqvtest.mds.aqv_lot(leadingorder: lorder)
    aqvlots.each {|aqvlot|
      lot = aqvlot.lot
      rops = @@sv.lot_operation_list(lot)
      # process lots until last PD before bankin and send SPC events
      rops[0..-3].each {|routeop|
        li = @@sv.lot_info(lot)
        pparams = {proctime: 0.1}
        pparams[:eqp] = @@aqvtest.wbtqtest.eqp if li.opEqps.member?(@@aqvtest.wbtqtest.eqp)
        assert @@sv.claim_process_lot(lot, pparams)
        assert @@aqvtest.spc.send_msg(lot, routeop, result: true)
      }
      # send trigger_spcevent for NOTEX op,  a) accept, b) reject
      ##rops[-2].op = 'QANOTEX.01'
      ##rops[-2].opNo = nil
      ##assert @@aqvtest.spc.send_msg(lot, rops[-2], result: true)
      # lot is at last PD before bankin ##, send SPC event and MQ notification
      ## DO NOT SEND - NOT MANDATORY  @@aqvtest.trigger_spcevent(lot, rops[-2])
      assert @@aqvtest.ei.send_msg(lot)
    }
    # wait for AQV kit evaluation
    $log.info "waiting up to #{@@aqvtest.evaldelay} s for AQV lot evaluation"
    aqvlots = nil
    wait_for(timeout: @@aqvtest.evaldelay) {
      aqvlots = @@aqvtest.mds.aqv_lot(leadingorder: lorder)
      aqvlots.inject(true) {|ok, aqvlot| ok && [0, 1].member?(aqvlot.status)}
    } || ($log.warn 'missing aqvlot status update'; return)
    res = true
    aqvlots.each {|aqvlot| ($log.warn "wrong aqvlot status: #{aqvlot}"; res=false) if aqvlot.status != 0}
    assert res, 'wrong AQVLOT status'
    sleep 5
    # verify final message to SAP
    assert kitresult = @@aqvtest.sap.receive_msg, 'no msg on SAP queue'
    assert_equal aqvlots.first.order, kitresult.order, 'wrong SAP order number in kit result msg'
    assert_equal 'ACCEPT', kitresult.result, 'wrong kit result'
  end

  def testman110_sildcr_success
    # happy scenario with SIL not emulated
    # needs helper to transfer msgs from the SIL QA test queue to the AQV queue
    assert @@aqvtest.complete_kit_verify('ACCEPT', sil: true)  # only for trouble shooting: wait_aqv_lot: true, wait_aqv_spc: true)
  end

  def testman111_sildcr_fail
    # fail scenario with SIL not emulated
    # needs helper to transfer msgs from the SIL QA test queue to the AQV queue
    assert @@aqvtest.complete_kit_verify('REJECT', result: false, sil: true)  # only for trouble shooting: wait_aqv_lot: true, wait_aqv_spc: true)
  end

  def test200_spcevent_onespcbefore_opercomp
    # happy scenario, sending SPC message before opecomp
    assert @@aqvtest.complete_kit_verify('ACCEPT', spcfirst: true)  # debugging only, takes long: wait_aqv_spc: true)
  end

  def test201_spcevent_allspcbefore_opercomp
    # happy scenario, sending all SPC messages before lot processing starts
    assert @@aqvtest.complete_kit_verify('ACCEPT', allspcfirst: true)
  end

  def test300_noncriticalhold
    # whitelist holdcodes ("M-ENG") >> success
    assert @@aqvtest.complete_kit_verify('ACCEPT', earlycompletion: true, lothold: 0, holdreason: @@aqvtest.holdreason_noncritical)
  end

  def test301_criticalhold
    # other holdcodes >> fail
    assert @@aqvtest.complete_kit_verify('REJECT', earlycompletion: true, lothold: 0)
  end

  def test302_criticalfuturehold
    # other holdcodes >> fail
    assert @@aqvtest.complete_kit_verify('REJECT', earlycompletion: true, futurehold: 1)
  end

  def test303_bankhold
    # bankhold after complete >> success
    assert @@aqvtest.complete_kit_verify('ACCEPT', earlycompletion: true, bankhold: 0)
  end

  def test304_nonprobankin
    # nonprobankin (bankhold) >> success
    assert @@aqvtest.complete_kit_verify('ACCEPT', earlycompletion: true, nonprobankin: 0)
  end

  def test305_running_hold
    # force ope comp (running_hold: true) >> fail
    assert @@aqvtest.complete_kit_verify('REJECT', earlycompletion: true, running_hold: true)
  end

  def test309_process_at_wrong_eqp
    # process lots at QACLUST02 instead of QACLUST01 which is not the T_AQV_LOT.QUAL_EQP_ID
    assert @@aqvtest.complete_kit_verify('REJECT', earlycompletion: true, eqpwrong: @@eqpwrong)
  end

  def test400a_skipop_gatepass_success
    # gatepass, pd Tool01,02,03,06 not skipable, 04..05 skipable
    # allowed users >> properties ("X-") -> RegEx
    assert @@aqvtest.complete_kit_verify('ACCEPT', earlycompletion: true, gatepass: 3)
  end

  def test400b_skippable_gatepass_before_spcfail  # INCIDENT 2015-09-07
    # legal gatepass before a fail must yield REJECT
    assert @@aqvtest.complete_kit_verify('REJECT', earlycompletion: true, gatepass: 0, spcfailat: 3)
  end

  def test401a_skipop_opelocate_success
    # opelocateforward, pd Tool01,02,03,06 not skipable, 04..05 skipable
    # allowed users >> properties ("X-")
    assert @@aqvtest.complete_kit_verify('ACCEPT', earlycompletion: true, opelocate: 3)
  end

  def test401b_skippable_opelocate_before_spcfail  # INCIDENT 2015-09-07
    # legal gatepass before a fail must yield REJECT
    assert @@aqvtest.complete_kit_verify('REJECT', earlycompletion: true, opelocate: 0, spcfailat: 3)
  end

  def test402_skipop_gatepass_fail
    # gatepass, pd Tool01,02,03,06 not skipable per spec, 04..05 skipable
    # allowed users >> properties ("X-")
    assert @@aqvtest.complete_kit_verify('REJECT', earlycompletion: true, gatepass: 1)
  end

  def test403_skipop_opelocate_fail
    # opelocateforward, pd Tool01,02,03,06 not skipable, 04..05 skipable
    # allowed users >> properties ("X-")
    assert @@aqvtest.complete_kit_verify('REJECT', earlycompletion: true, opelocate: 1)
  end

  def test405_skipop_opelocate_two_ops_success
    # opelocateforward 2 steps, pd Tool01,02,03,06 not skipable, 04..05 skipable
    # allowed users >> properties ("X-")
    assert @@aqvtest.complete_kit_verify('ACCEPT', earlycompletion: true, opelocate: 3, opelocatesteps: 2)
  end

  def test406_missing_spc_at_skipop_success
    # ops Tool04..05 skipable
    assert @@aqvtest.complete_kit_verify('ACCEPT', earlycompletion: true, nospcat: 3)
  end

  def test407_missing_spc_at_nonskipop_fail
    # opelocateforward, pd Tool01,02,03,06 not skipable, 04..05 skipable
    assert @@aqvtest.complete_kit_verify('REJECT', earlycompletion: true, nospcat: 2)
  end

  def test408_locate_backward_success
    # opelocatebackward
    assert @@aqvtest.complete_kit_verify('ACCEPT', earlycompletion: true, opelocateback: 2)
  end

  def test500_spcsuccess_spcinvalid
    # "Suxxezz" and "Fehl" instead of Success and Fail
    assert @@aqvtest.complete_kit_verify('REJECT', earlycompletion: true, invalidres: true)
  end

  def test501_spcfail_spcinvalid
    # "Suxxezz" and "Fehl" instead of Success and Fail
    assert @@aqvtest.complete_kit_verify('REJECT', earlycompletion: true, invalidres: true, spcresult: false)
  end

  def test502_spcinvalid_but_skippable
    # "Suxxezz" and "Fehl" instead of Success and Fail
    assert @@aqvtest.complete_kit_verify('REJECT', earlycompletion: true, invalidresat: 3)
  end

  def test503_spcfail_skippable_op
    # fail scenario, sending SPC messages 1 by 1, SPC  'fail' at skippable PD Tool03.01
    assert @@aqvtest.complete_kit_verify('REJECT', earlycompletion: true, spcfailat: 3)
  end

  def test504_spcsuccess_send_long_comment
    # Send long message content (more then 1024 chars), AQV to cut off/exchange the comment
    assert @@aqvtest.complete_kit_verify('ACCEPT', earlycompletion: true, comment: 'QA long message:' + ' 0123456789' * 200)
  end

  def test505_spcfail_send_long_comment
    # Send long message content (more then 1024 chars), AQV to cut off/exchange the comment
    assert @@aqvtest.complete_kit_verify('REJECT', earlycompletion: true, comment: 'QA long message:' + ' 0123456789' * 200, spcresult: false)
  end

  def test506_spcsuccess_spcinvalid_lot1
    # "Suxxezz" and "Fehl" instead of Success and Fail
    assert @@aqvtest.complete_kit_verify('REJECT', earlycompletion: true, invalidresidx: 0)
  end

  def test507_spcsuccess_spcinvalid_lot2
    # "Suxxezz" and "Fehl" instead of Success and Fail
    assert @@aqvtest.complete_kit_verify('REJECT', earlycompletion: true, invalidresidx: 1)
  end

  def test508_spcsuccess_spcinvalid_lot3
    # "Suxxezz" and "Fehl" instead of Success and Fail
    assert @@aqvtest.complete_kit_verify('REJECT', earlycompletion: true, invalidresidx: 2)
  end

  def test601_combined_reject
    # multiple reject reasons, all must be reported in the final message to SAP
    assert @@aqvtest.complete_kit_verify('REJECT', earlycompletion: true, futurehold: 0, lothold: 1, spcfailat: 2, lotsplit: 3)
  end

end
