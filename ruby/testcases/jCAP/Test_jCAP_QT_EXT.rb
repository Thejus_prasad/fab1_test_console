=begin
Test the QT controller.

additionaly check qt logs and qt controller website.
f36asd1//local/logs/jCAP/qt-controller.log
http://f36iesd1:8000/reporting/lot_qt.jsp

Start PD: A-QT-PD2 OpNo: 1000.1200
End PD:   A-QT-PD5 OpNo: 1000.1500
Meas PD:  A-QT-PDMEAS    1000.1600
Start PD: A-QT-PD21 OpNo: 2000.1200 for future holds
End PD:   A-QT-PD51 OpNo: 2000.1500
Tools: UTQT01... 06
ERF MainPDs:
RW MainPDs: A-QT-RWIG.01, A-QT-RWDL.01, A-QT-RWDA.01, A-QT-RWUD.01

load_run_testcase "Test_jCAP_QT", "itdc"

(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

This test program contains semi automatic tests that require setup changes during run by the tester
QHTL claim memo example: Queue time of 10 min exceeded at 07/10/12 09:22:58 (hold action: HLD, target operation 1000.1500, DLIS comment: Add), criticality = {not defined}

Author: Adel Darwaish Jan 2014

=end

require "RubyTestCase"
require "sjc"
require "thread"

class Test_jCAP_QT_EXT < RubyTestCase
  Description = "Test jCAP Q-Time job"

  include MonitorMixin

  @@continue_test = false

  @@Hold_Code1 = "QTHL"
  @@Hold_Code2 = "QTOV"

  @@C1 = "C1"
  @@C2 = "C2"
  @@C4 = "C4"
  @@QT_time = 10 # in minutes. Configured in SiView for the related PD. default 10 min for ITDC
  @@Rdat_setup_time = 300

  def test00_setup
	@@qtstart = {}
    @@qthold = {}
    @@holds = {}
    @@lots = {}
    @@threads = []
	puts "\n The following test cases are semi automatic test cases that requires SiView setup changes during testing. Continue?"
	answer = STDIN.gets
	if !"jy".index(answer.chomp.downcase)
	  $log.info "invalid input. Abort test case"
	  @@continue_test = false
	  return
	end
	@@continue_test = true
  end

  #############################################################################################
  # Semi automatic test cases. 															      #
  # the following test cases require setup changes during run								  #
  #############################################################################################

  def test10_lot_in_same_related_Module_before_QT_area_change_setup_StartPD
	  return if @@continue_test == false
	  @@lots['tc10'] = l = start_lot($sv, "A-QT-PRODUCT-EXT.01", "A-QT-MAIN-EXT.01")
      if l
        $sv.lot_opelocate(l, "1000.1100")
		puts "\n replace the old 'A-QT-MODULE1.01' by PD Module 'A-QT-MODULE-EXT1-SPD.01' on SiView then type j or y when done to continue. Done?\n"
		answer = STDIN.gets
		if !"jy".index(answer.chomp.downcase)
		  $log.info "invalid input. Abort test case"
		  return
		end
		$log.info "Wait #{@@Rdat_setup_time / 60} min for the RDAT setup scripts to run..."
		sleep @@Rdat_setup_time
		$sv.lot_opelocate(l, "1000.1200")
        $sv.claim_process_lot(l, :freeport=>true)
        @@qtstart[l] = siview_time($sv.lot_info(l).claim_time)
		$log.info "wait 11 min (1 min for the jCAP to run and 10 min for the qt to expire)"
        sleep 660
		l = @@lots['tc10']
		hold_res = checkhold(l)
		criticality = checkhold_criticality_memo(l,@@C1)
		$log.info("checking tc110 with lot #{l}")
		$log.info "found lot hold with reason: #{hold_res}"
		$log.info "Test is finished. Revert setup changes:"
		puts "\n replace  Module 'A-QT-MODULE-EXT1-SPD.01' on SiView back to the old Module 'A-QT-MODULE1.01', then type j or y when done to continue. Done?\n"
		answer = STDIN.gets
		if !"jy".index(answer.chomp.downcase)
		  $log.info "invalid input. Abort test case"
		  return
		end
		# check results
		assert_equal @@Hold_Code1, hold_res, "hold with hold code = #{@@Hold_Code1} expected"
		assert criticality,"wrong criticality claim memo"
		#
		$log.info "Wait #{@@Rdat_setup_time / 60} min for the RDAT setup scripts to run..."
		sleep @@Rdat_setup_time
      end
  end

  def test11_lot_in_same_related_Module_before_QT_area_change_setup_EndPD
	  return if @@continue_test == false
	  @@lots['tc11'] = l = start_lot($sv, "A-QT-PRODUCT-EXT.01", "A-QT-MAIN-EXT.01")
      if l
        $sv.lot_opelocate(l, "1000.1100")
		puts "\n replace the old 'A-QT-MODULE1.01' by PD Module 'A-QT-MODULE-EXT1-EPD.01' on SiView then type j or y when done to continue. Done?\n"
		answer = STDIN.gets
		if !"jy".index(answer.chomp.downcase)
		  $log.info "invalid input. Abort test case"
		  return
		end
		$log.info "Wait #{@@Rdat_setup_time / 60} min for the RDAT setup scripts to run..."
		sleep @@Rdat_setup_time
		$sv.lot_opelocate(l, "1000.1200")
        $sv.claim_process_lot(l, :freeport=>true)
        @@qtstart[l] = siview_time($sv.lot_info(l).claim_time)
		$log.info "wait 11 min (1 min for the jCAP to run and 10 min for the qt to expire)"
        sleep 660
		l = @@lots['tc11']
		hold_res = checkhold(l)
		criticality = checkhold_criticality_memo(l,@@C1)
		$log.info("checking tc110 with lot #{l}")
		$log.info "found lot hold with reason: #{hold_res}"
		$log.info "Test is finished. Revert setup changes:"
		puts "\n replace the new A-QT-MODULE-EXT1-SPD.01 by old PD Module 'A-QT-MODULE1.01' back on SiView then type j or y when done to continue. Done?\n"
		answer = STDIN.gets
		if !"jy".index(answer.chomp.downcase)
		  $log.info "invalid input. Abort test case"
		  return
		end
		# check results
		assert_equal @@Hold_Code1, hold_res, "hold with hold code = #{@@Hold_Code1} expected"
		assert criticality,"wrong criticality claim memo"
		#
		$log.info "Wait #{@@Rdat_setup_time / 60} min for the RDAT setup scripts to run..."
		sleep @@Rdat_setup_time
      end
  end

 def test12_lot_in_previous_module_to_related_Module_QT_area_change_setup_StartPD
	  return if @@continue_test == false
	  @@lots['tc12'] = l = start_lot($sv, "A-QT-PRODUCT-EXT.01", "A-QT-MAIN-EXT.01")
      if l
        $sv.lot_opelocate(l, "1000.1100")
		puts "\n replace the old 'A-QT-MODULE2.01' by PD Module 'A-QT-MODULE-EXT2-SPD.01' on SiView then type j or y when done to continue. Done?\n"
		answer = STDIN.gets
		if !"jy".index(answer.chomp.downcase)
		  $log.info "invalid input. Abort test case"
		  return
		end
		$log.info "Wait #{@@Rdat_setup_time / 60} min for the RDAT setup scripts to run..."
		sleep @@Rdat_setup_time
		$sv.lot_opelocate(l, "2000.1200")
        $sv.claim_process_lot(l, :freeport=>true)
        @@qtstart[l] = siview_time($sv.lot_info(l).claim_time)
		$log.info "wait 11 min (1 min for the jCAP to run and 10 min for the qt to expire)"
        sleep 660
		l = @@lots['tc12']
		hold_res = checkfuturehold(l)
		criticality = checkhold_criticality_memo(l,@@C1, :hold=> "future")
		$log.info("checking tc110 with lot #{l}")
		$log.info "found lot futurehold with reason: #{hold_res}"
		$log.info "Test is finished. Revert setup changes:"
		puts "\n replace the new A-QT-MODULE-EXT2-SPD.01 by old PD Module 'A-QT-MODULE2.01' back on SiView then type j or y when done to continue. Done?\n"

		answer = STDIN.gets
		if !"jy".index(answer.chomp.downcase)
		  $log.info "invalid input. Abort test case"
		  return
		end
		# check results
		assert_equal @@Hold_Code1, hold_res, "futurehold with hold code = #{@@Hold_Code1} expected"
		assert criticality,"wrong criticality claim memo"
		#
		$log.info "Wait #{@@Rdat_setup_time / 60} min for the RDAT setup scripts to run..."
		sleep @@Rdat_setup_time
      end
  end

  def test13_lot_in_previous_module_to_related_Module_QT_area_change_setup_EndPD
	  return if @@continue_test == false
	  @@lots['tc13'] = l = start_lot($sv, "A-QT-PRODUCT-EXT.01", "A-QT-MAIN-EXT.01")
      if l
        $sv.lot_opelocate(l, "1000.1100")
		puts "\n replace the old 'A-QT-MODULE2.01' by PD Module 'A-QT-MODULE-EXT2-EPD.01' on SiView then type j or y when done to continue. Done?\n"
		answer = STDIN.gets
		if !"jy".index(answer.chomp.downcase)
		  $log.info "invalid input. Abort test case"
		  return
		end
		$log.info "Wait #{@@Rdat_setup_time / 60} min for the RDAT setup scripts to run..."
		sleep @@Rdat_setup_time
		$sv.lot_opelocate(l, "2000.1200")
        $sv.claim_process_lot(l, :freeport=>true)
        @@qtstart[l] = siview_time($sv.lot_info(l).claim_time)
		$log.info "wait 11 min (1 min for the jCAP to run and 10 min for the qt to expire)"
        sleep 660
		l = @@lots['tc13']
		hold_res = checkfuturehold(l)
		criticality = checkhold_criticality_memo(l,@@C1, :hold=> "future")
		$log.info("checking tc110 with lot #{l}")
		$log.info "found lot futurehold with reason: #{checkhold(l)}"
		$log.info "Test is finished. Revert setup changes:"
		puts "\n replace the new A-QT-MODULE-EXT2-EPD.01 by old PD Module 'A-QT-MODULE2.01' back on SiView then type j or y when done to continue. Done?\n"

		answer = STDIN.gets
		if !"jy".index(answer.chomp.downcase)
		  $log.info "invalid input. Abort test case"
		  return
		end
		# check results
		assert_equal @@Hold_Code1, hold_res, "futurehold with hold code = #{@@Hold_Code1} expected"
		assert criticality,"wrong criticality claim memo"
		#
		$log.info "Wait #{@@Rdat_setup_time / 60} min for the RDAT setup scripts to run..."
		sleep @@Rdat_setup_time
      end
  end

  def test14_lot_in_same_related_Module_within_QT_area_change_setup_EndPD
	  return if @@continue_test == false
	  @@lots['tc14'] = l = start_lot($sv, "A-QT-PRODUCT-EXT.01", "A-QT-MAIN-EXT.01")
      if l
        $sv.lot_opelocate(l, "1000.1100")
		$sv.lot_opelocate(l, "1000.1200")
        $sv.claim_process_lot(l, :freeport=>true)
        @@qtstart[l] = siview_time($sv.lot_info(l).claim_time)
		#
		puts "\n replace the old 'A-QT-MODULE1.01' by PD Module 'A-QT-MODULE-EXT1-EPD.01' on SiView then type j or y when done to continue. Done?\n"

		answer = STDIN.gets
		if !"jy".index(answer.chomp.downcase)
		  $log.info "invalid input. Abort test case"
		  return
		end
		$log.info "Wait 12 min: 10 min for the RDAT setup scripts and for QT and 2 min for the jCAP job  ..."
		sleep 700
		l = @@lots['tc14']
		hold_res = checkhold(l)
		criticality = checkhold_criticality_memo(l,@@C1)
		$log.info("checking tc110 with lot #{l}")
		$log.info "found lot hold with reason: #{checkhold(l)}"
		$log.info "Test is finished. Revert setup changes:"
		puts "\n replace the new A-QT-MODULE-EXT1-EPD.01 by old PD Module 'A-QT-MODULE2.01' back on SiView then type j or y when done to continue. Done?\n"

		answer = STDIN.gets
		if !"jy".index(answer.chomp.downcase)
		  $log.info "invalid input. Abort test case"
		  return
		end
		# check results
		assert_equal @@Hold_Code1, hold_res, "hold with hold code = #{@@Hold_Code1} expected"
		assert criticality,"wrong criticality claim memo"
		#
		$log.info "Wait #{@@Rdat_setup_time / 60} min for the RDAT setup scripts to run..."
		sleep @@Rdat_setup_time
      end
  end

   def test15_lot_in_same_related_Module_within_QT_area_change_setup_EndPD_No_QT_expires_NoHold
	  return if @@continue_test == false
	  @@lots['tc15'] = l = start_lot($sv, "A-QT-PRODUCT-EXT.01", "A-QT-MAIN-EXT.01")
      if l
        $sv.lot_opelocate(l, "1000.1100")
		$sv.lot_opelocate(l, "1000.1200")
        $sv.claim_process_lot(l, :freeport=>true)
        @@qtstart[l] = siview_time($sv.lot_info(l).claim_time)
		#
		puts "\n replace the old 'A-QT-MODULE1.01' by PD Module 'A-QT-MODULE-EXT1-EPD.01' on SiView then type j or y when done to continue. Done?\n"
		answer = STDIN.gets
		if !"jy".index(answer.chomp.downcase)
		  $log.info "invalid input. Abort test case"
		  return
		end
		$log.info "Wait 8 min for the RDAT setup scripts, for the jCAP job  and for shortly before QT..."
		sleep 480
		$sv.claim_process_lot(l, :freeport=>true)
        @@qtstart[l] = siview_time($sv.lot_info(l).claim_time)
		l = @@lots['tc15']
		hold_res = checkhold(l)
		criticality = checkhold_criticality_memo(l,@@C1)
		$log.info("checking tc110 with lot #{l}")
		$log.info "found lot hold with reason: #{checkhold(l)}"
		$log.info "Test is finished. Revert setup changes:"
		puts "\n replace the new A-QT-MODULE-EXT1-EPD.01 by old PD Module 'A-QT-MODULE2.01' back on SiView then type j or y when done to continue. Done?\n"

		answer = STDIN.gets
		if !"jy".index(answer.chomp.downcase)
		  $log.info "invalid input. Abort test case"
		  return
		end
		# check results
		assert_equal nil, hold_res, "found hold with hold code = #{@@Hold_Code1} instead of nil"
		#
		$log.info "Wait #{@@Rdat_setup_time / 60} min for the RDAT setup scripts to run..."
		sleep @@Rdat_setup_time
      end
  end

  def test99_cleanup
    puts "\ndelete all lots: #{@@lots.inspect} (j/n)?"
    answer = STDIN.gets
    if "jy".index(answer.chomp.downcase)
      @@lots.keys.each{|tc| $sv.delete_lot_family(@@lots[tc], :sleeptime=>2)}
    end
  end

  def get_child_lot(lot)
    _clot = []
    _clot = $sv.lot_family(lot)
    _clot.delete(lot)
    return _clot[0]
  end

  def checkhold(lot)
    #$log.info "method checkhold sleep 10s"
    #sleep 10
    ret=$sv.lot_hold_list(lot, :reason=>@@Hold_Code1)
    if ret.size == 0
        return nil
    else
        r = ret[0]
        refute_equal "", r['memo'], "hold memo for QTHL hold is empty"
        return r['reason']
    end
  end

  def checkfuturehold(lot)
    #$log.info "method checkfuturehold sleep 10s"
    #sleep 10
    ret=$sv.lot_futurehold_list(lot, :reason=>@@Hold_Code1)
    if ret.size == 0
        return nil
    else
        r = ret[0]
        refute_equal "", r['memo'], "hold memo for QTHL hold is empty"
        return r['reason']
    end
  end
  def checkhold_criticality_memo(lot, criticality, params={})
    # e.g.: @@claim_memo_criticality = "time of %s min exceeded at %s (hold action: %s, target operation %s, DLIS comment: %s), Criticality =%s"
    #memo = @@claim_memo_criticality %[@@QT_time,@@carrier0,@@carrier5,@@requestor,@@sorter,@@pg]
    #$log.info "method checkhold_criticality_memo sleep 30s"
    #sleep 30
	hold = (params[:hold] or "")

	if hold == "future"
		ret=$sv.lot_futurehold_list(lot, :reason=>@@Hold_Code1)
	else
		ret=$sv.lot_hold_list(lot, :reason=>@@Hold_Code1)
	end

    if ret.size == 0
        return false
    else
        r = ret[0]
        end_memo = "criticality = #{criticality}"
        refute_equal "", r['reason'], "hold memo for QTHL hold is empty"
        assert r['memo'].start_with?("Queue time of #{@@QT_time} min"), "wrong start of claim memo for QTHL hold"
        assert r['memo'].end_with?(end_memo), "wrong end of claim memo for QTHL hold"
        return true
    end
  end

  def start_lot(sv, product="A-QT-PRODUCT.01", route="A-QT-MAIN.01")
    lot = nil
    synchronize do
      lot = sv.new_lot_release(:product=>product, :route=>route, :stb=>true, :sublottype=>"PROD-PO", :carrier=>"QT%")
    end
    sleep 60
    sv.lot_hold_release(lot, nil)
    li = sv.lot_info(lot)
    sv.carrier_xfer_status_change(li.carrier, "EO", li.opEqps[0]) if li.xfer_status != "EO"
    return lot
  end

  def calc_qt(lot)
    return @@qthold[lot] - @@qtstart[lot]
  end
end
