=begin 
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.
    
Author:   ssteidte, 2016-07-19
Version:  19.07

History:
  2018-03-06 sfrieske, replaced user_parameter_delete_verify by user_parameter_delete
  2019-01-22 sfrieske, minor cleanup
  2019-09-23 sfrieske, modified tests for v 19.07

Notes:
  lots on NonProBank, based on slt, PD not owned by C4B, carrier category FEOL, MOL, BEOL
=end

require 'SiViewTestCase'


class JCAP_RecycleUsedJumper < SiViewTestCase
  @@sublottype = 'EJ'
  @@sublottype2 = 'EC'
  @@priority = '2'
  @@sparams = {'ImportantLot'=>'QA', 'JumperDistance'=>'QA', 'LotDistance'=>'QA', 'ImportantLotPEM'=>'QA'}
  @@jparam = 'Jumper'
  @@jprefix = 'QATest-XY:'
  @@jposts = ['J3']
  @@nonprobank = 'UT-NONPROD'
  @@lots_cleaned = []
  @@lots_ignored = []

  @@job_interval = 330
  @@testlots = []


  def self.after_all_passed
    @@testlots.each {|lot| @@sv.delete_lot_family(lot)}
  end


  def test00_setup
    $setup_ok = false
    #
    # prepare test lots
    assert lots = @@svtest.new_lots(9, sublottype: @@sublottype, priority: @@priority), 'error creating test lots'
    @@testlots += lots
    lots.each_with_index {|lot, idx|
      # @@sparams.each {|p| assert_equal 0, @@sv.user_parameter_change('Lot', lot, p, 'QATest')}
      # assert_equal 0, @@sv.user_parameter_change('Lot', lot, @@jparam, @@jprefix + @@jposts[idx % @@jposts.size])
      assert_equal 0, @@sv.user_parameter_change('Lot', lot, 
        @@sparams.merge({@@jparam=>@@jprefix + @@jposts[idx % @@jposts.size]}) )
    }
    #
    # lot0 has Jumper set to J3 -> clean
    @@lots_cleaned << lots[0]
    #
    # lots1..4 have a script parameter missing -> clean
    assert_equal 0, @@sv.user_parameter_delete('Lot', lots[1], 'ImportantLot', check: true)
    @@lots_cleaned << lots[1]
    assert_equal 0, @@sv.user_parameter_delete('Lot', lots[2], 'JumperDistance', check: true)
    @@lots_cleaned << lots[2]
    assert_equal 0, @@sv.user_parameter_delete('Lot', lots[3], 'LotDistance', check: true)
    @@lots_cleaned << lots[3]
    assert_equal 0, @@sv.user_parameter_delete('Lot', lots[4], 'ImportantLotPEM', check: true)
    @@lots_cleaned << lots[4]
    #
    # lot5 has Jumper set to ..J1 -> ignore
    assert_equal 0, @@sv.user_parameter_change('Lot', lots[5], @@jparam, @@jprefix + 'J1')
    @@lots_ignored << lots[5]
    #
    # lot6 has Jumper set to ..J2 -> ignore
    assert_equal 0, @@sv.user_parameter_change('Lot', lots[6], @@jparam, @@jprefix + 'J2')
    @@lots_ignored << lots[6]
    #
    # lot7 has Jumper empty -> ignore
    assert_equal 0, @@sv.user_parameter_change('Lot', lots[7], @@jparam, '')
    @@lots_ignored << lots[7]
    #
    # lot8 with other sublottype -> ignore
    assert_equal 0, @@sv.sublottype_change(lots[8], @@sublottype2)
    @@lots_ignored << lots[8]
    #
    # finally bank the lots in to make them eligible for the job
    @@liorigs = @@sv.lots_info(@@testlots)
    lots.each {|lot| assert_equal 0, @@sv.lot_nonprobankin(lot, @@nonprobank)}
    $log.info "waiting #{@@job_interval} s for the jCAP job to run"; sleep @@job_interval
    #
    $setup_ok = true
  end

  def test11_clean
    @@lots_cleaned.each {|lot|
      $log.info "verifying lot #{lot} has been cleaned up"
      li = @@sv.lot_info(lot)
      assert_equal '4', li.priority, 'wrong prio class'
      lparams = @@sv.user_parameter('Lot', lot)
      @@sparams.each {|sp| 
        refute lparams.find {|e| e.name == sp}, "script param #{sp} not deleted"
      }
      assert jp = lparams.find {|e| e.name == @@jparam}, "missing script param #{@@jparam}"
      assert_equal @@jprefix + 'NS', jp.value, "wrong value for script param #{@@jparam}"
    }
  end

  def test12_ignore
    @@lots_ignored.each {|lot|
      $log.info "verifying lot #{lot} has been ignored"
      liorig = @@liorigs.find {|li| li.lot == lot}
      li = @@sv.lot_info(lot)
      assert_equal liorig.priority, li.priority, 'wrong prio class'
      assert_equal liorig.sublottype, li.sublottype, 'wrong sublottype'
    }
  end

end
