=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.


Author: Steffen Frieske, 2018-01-11

Version: 20.06

History:
  2019-01-30 sfrieske, minor cleanup

Notes:
  see http://f1onewiki:21080/display/JCAP/AutoScrapProcessHandler,
  sublottype spec http://vf1sfcp01:10080/setupfc/pages/restricted/viewSpec.seam?SpecId=C08-00000047&cid=622547
=end

require 'SiViewTestCase'


class JCAP_AutoScrap < SiViewTestCase
  @@sv_defaults = {customer: 'amd', carriers: 'JC%'}
  @@endbank = 'T-END'  # must be both End and NonPro bank for the job to work
  @@nonprobank = 'UT-NONPROD'
  @@slt2 = 'QD'
  @@sltskip = 'RD'
  @@lotnotetitle = 'ScrapExcursion'
  #
  # from properties
  ##@@scrapcancel_reason = 'ALL'
  @@tgtbank = 'XY-PEN-HOLD'
  @@tgtcustomer = 'gf'
  @@tgtslt = 'SCRAP'

  @@job_interval = 190   # 3+ min
  @@testlots = []


  def self.after_all_passed
    @@testlots.each {|lot| @@sv.delete_lot_family(lot)}
  end

  def test10
    @@lis_skip = []
    # prepare test lots
    assert @@testlots = @@svtest.new_lots(10, user_parameters: {'ERPSalesOrder'=>'QA'}), 'error creating test lots'
    # lot 0: on route
    # lot 1: on NonProBank
    lot = @@testlots[1]
    assert @@sv.lot_nonprobankin(lot, @@nonprobank)
    # lot 2: on bank (special bank with "For Production" no and "Bank In" yes -> both End and NonPro bank)
    lot = @@testlots[2]
    assert_equal 0, @@sv.lot_opelocate(lot, :last)
    assert_equal 0, @@sv.lot_bank_move(lot, @@endbank)
    # lot 3: slt QD
    lot = @@testlots[3]
    assert_equal 0, @@sv.sublottype_change(lot, @@slt2)
    # lot 4: lot note NOT magic
    lot = @@testlots[4]
    assert_equal 0, @@sv.lot_note_register(lot, 'No ScrapExcursion', 'QA Test')
    # lot 5: missing ERPSalesOrder
    lot = @@testlots[5]
    assert_equal 0, @@sv.user_parameter_delete('Lot', lot, 'ERPSalesOrder')
    #
    # lot 6: virtual carrier 1%
    lot = @@testlots[6]
    assert ec = @@svtest.get_empty_carrier(carrier: '1%', carrier_category: nil), 'no empty carrier 1%'
    assert_equal 0, @@sv.wafer_sort(lot, ec), 'error moving lot to empty carrier'
    # lot 7: virtual carrier 9%
    lot = @@testlots[7]
    assert ec = @@svtest.get_empty_carrier(carrier: '9%', carrier_category: nil), 'no empty carrier 9%'
    assert_equal 0, @@sv.wafer_sort(lot, ec), 'error moving lot to empty carrier'
    # lot 8: slt other then PROD must be skipped
    lot = @@testlots[8]
    assert_equal 0, @@sv.sublottype_change(lot, @@sltskip)
    # lot 9: lot note (must be skipped)
    lot = @@testlots[9]
    assert_equal 0, @@sv.lot_note_register(lot, @@lotnotetitle, 'QA Test')
    #
    #
    @@testlots.each {|lot| assert_equal 0, @@sv.scrap_wafers(lot)}
    (6..9).each {|i| @@lis_skip << @@sv.lot_info(@@testlots[i])}
    $log.info "waiting #{@@job_interval} s for the jCAP job to run"; sleep @@job_interval
  end

  def test11_verify_handled
    @@testlots[0..5].each_with_index {|lot, idx|
      $log.info "\n -- verifying lot ##{idx}: #{lot}"
      assert li = @@sv.lot_info(lot)
      ##assert_equal 'NonProBank', li.status, 'wrong status'  # lot2's status is COMPLETED
      assert_equal @@tgtbank, li.bank, 'wrong bank'
      assert_equal @@tgtcustomer, li.customer, 'wrong customer'
      assert_equal @@tgtslt, li.sublottype, 'wrong sublottype'
      ['ERPItem', 'ERPSalesOrder'].each {|name|
        refute @@sv.user_parameter('Lot', lot, name: name).valueflag, "#{name} not deleted"
      }
    }
  end

  def test12_verify_skipped
    @@lis_skip.each {|li|
      $log.info "\n -- verifying lot #{li.lot}"
      ok = true
      linew = @@sv.lot_info(li.lot)
      linew.members.each {|m|
        next if [:ipriority].member?(m)
        if linew[m] != li[m]
         $log.warn "wrong field #{m}: #{linew[m].inspect} instead of #{li[m].inspect}"
         ok = false
        end
      }
      assert ok, "lot #{li.lot} has been touched"
    }
  end

end
