=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: Steffen Steidten, 2011-10-20

Version: 20.07

History:
  2013-12-11 ssteidte, added automated tests for LEADINGORDER
  2014-08-26 ssteidte, simplified setup by using Setup.FC
  2015-01-26 ssteidte, maintenance for manual tests 30x
  2015-07-06 ssteidte, updated testcases for v15.06
  2015-07-06 sfrieske, updated testcases for v15.10 (test18x)
  2015-10-22 sfrieske, added TC147
  2017-01-30 sfrieske, changed expected messages only (v16.11)
  2017-04-07 sfrieske, added job timeout tests 201 and 202
  2017-11-08 sfrieske, added grouping tests
  2018-11-07 sfrieske, minor cleanup
  2019-02-19 sfrieske, improved EmuRemote usage
  2019-08-07 sfrieske, added TC108 Engineering kit 'Cancel'
  2020-04-27 sfrieske, added TCs 113 and 144 for No LeadingOrder
  2020-08-05 sfrieske, separated tests involving SAP (tests are broken)
  2021-04-21 sfrieske, moved @sorter and @pg form SiView::Test to SJC::Test
  2021-08-27 sfrieske, require uniquestring instead of misc

Notes:
  Requirements:
  * setup.FC up and running, SPECs created to match the test cases
    http://vf1sfcd01:10080/setupfc-rest/v1/specs/C07-00000766/content?format=html
    http://vf1sfcd01:10080/setupfc-rest/v1/specs/C07-00003716/content?format=html
  * Carriers TQ* with wafers at correct positions, see test sheet
  * SJC up and running
  * WBTQ needs to be reconfigured to write SAP messages to a QA test queue
  ## broken (sf 2020-07-21)  Monitoring: http://f36iesd1:8000/reporting/wbtq_monitor_status.jsp
=end

require 'jcap/wbtqtest'
require 'util/waitfor'
require 'RubyTestCase'


# assistance for manual tests of WBTQ with SAP, depending on SAP setup, BROKEN
class JCAP_WBTQ_SAP < RubyTestCase
  @@sorter = 'UTS001'
  @@order = '000013370815'
  @@spec = 'C07-00000766'
  # regular build product 2nd source
  @@product1_2nd_source = 'B-400-CLUSTER.01'
  # eqp Module2 (location precedence and manual tests)
  @@eqp2 = 'QACLUST02'
  @@stocker2 = 'QASTOM2'
  # eqp and stocker Location UDATA
  @@loc1 = {'Location'=>'QAM1-X1-Y2'}
  @@loc2 = {'Location'=>'QAM2-X3-Y4'}
  # carriers
  # full carrier, default
  @@carrier = 'TQ01'
  # empty carrier for executing SRs, needs to be cleaned up!
  @@carrier_sr = 'TQ04'
  # 5 empty carriers, will be emptied as part of test preparation
  @@carriers_empty = ['TQ05', 'TQ06', 'TQ07', 'TQ08', 'TQ09']

  @@timeout = 120           # MQ response timeout
  @@mds_sync = 35
  @@testlots = []


  def self.startup
    super
    @@rtdremote = RTD::EmuRemote.new($env)
    @@wbtqtest = WBTQ::Test.new($env, @@sv_defaults.merge(sv: @@sv, carriers: 'TQ%', carrier_category: 'BEOL',
      sorter: @@sorter, order: @@order, spec: @@spec, mds_sync: @@mds_sync, timeout: @@timeout))
    ##@@wbtqtest.kits.each_key {|k| @@wbtqtest.kits[k].delete('QA-KIT16')}  # temporarily disable a kit, issue 2017-01-25 09:28:25
  end

  def self.shutdown
    @@wbtqtest.cleanup_eqp(@@sorter, mode: 'Off-Line-1')
    @@wbtqtest.prepare_resources  # make all carriers NOTAVAILABLE, remove routes' pre-1 sleep
    @@wbtqtest.cancel_kit_verify('QA%')
    @@testlots.each {|lot| @@sv.delete_lot_family(lot)}
  end


  def setup
    # cleanup queues
    @@wbtqtest.client.mq_response.delete_msgs(nil)
    # move sorted wafers back into the default carrier
    unless @@sv.carrier_status(@@carrier_sr).lots.empty?
      @@sv.carrier_status_change(@@carrier, 'AVAILABLE', check: true)
      @@sv.lot_list_incassette(@@carrier_sr).each {|lot| @@sv.wafer_sort(lot, @@carrier, slots: 'empty')}
      @@sv.carrier_status_change(@@carrier, 'NOTAVAILABLE')
    end
  end


  def test00_setup
    $setup_ok = false
    #
    # ensure WBTQ is configured to use the test (not SAP) response queue
    res = @@wbtqtest.client.stb_trigger('QAQA', timeout: 60)
    assert @@wbtqtest.verify_response(res, '200', 'No kit context'), 'wrong WBTQ response queue configured?'
    # prepare full carrier
    @@sv.carrier_status(@@carrier).lots.each {|lot| assert @@sv.delete_lot_family(lot)}
    assert @@wbtqtest.prepare_src_carriers(@@carrier, {@@wbtqtest.srcproduct=>(1..25).to_a})
    # empty the SR target carrier
    @@sv.carrier_status(@@carrier_sr).lots.each {|lot| assert @@sv.delete_lot_family(lot)}
    # ensure locations are set correctly
    assert @@wbtqtest.stocker_location(@@wbtqtest.stocker, @@loc1), 'error setting UDATA'
    assert @@wbtqtest.eqp_location(@@wbtqtest.eqp, @@loc1), 'error setting UDATA'
    assert @@wbtqtest.stocker_location(@@stocker2, @@loc2), 'error setting UDATA'
    assert @@wbtqtest.eqp_location(@@eqp2, @@loc2), 'error setting UDATA'
    #
    [@@wbtqtest.eqp, @@eqp2].each {|eqp| @@sv.eqp_cleanup(eqp)}
    #
    $setup_ok = true
  end


  # for test case development only
  def testman010_smoke
     kit = 'QA-KIT04'  # @@wbtqtest.kits[:stbonly_other][0]
     assert @@wbtqtest.build_kit_verify(kit, @@carrier)
  end


  def testmanual800_prepare_manual
    $setup_ok = false
    #
    # ensure WBTQ is configured to use the SAP (not test) response queue
    res = @@wbtqtest.client.stb_trigger('QAQA', timeout: 60)
    assert !@@wbtqtest.verify_response(res, '200', 'No kit context'), "wrong WBTQ response queue configured"
    [@@wbtqtest.eqp, @@eqp2].each {|eqp| @@sv.eqp_cleanup(eqp)}
    #
    $setup_ok = true
  end

  def testmanual801_stb_only
    assert @@wbtqtest.prepare_resources(@@carrier, stocker: @@stocker2, product: @@product1_2nd_source)
    $log.info "waiting #{@@mds_sync} s for the MDS to sync"; sleep @@mds_sync
    # execute manual steps
    tstart = Time.now
    puts "\n\nExecute the manual test steps now, when ready press ENTER to start the verification!"
    gets
    # verify kit using WBTQ history data
    assert guid = @@wbtqtest.mds.carrier_contexts(@@carrier, history: true, tstart: tstart).first, "no kit context found"
    assert @@wbtqtest.verify_kit(guid, 'FINISHED', 'ALL_OK', history: true)
  end

  def testmanual802_sj_timeout
    assert @@wbtqtest.prepare_resources(@@carrier)
    $log.info "waiting #{@@mds_sync} s for the MDS to sync"; sleep @@mds_sync
    # create a disturbing SR for the carrier
    assert lot = @@sv.carrier_status(@@carrier).lots.last, "no lot in carrier #{@@carrier}"
    assert @@wbtqtest.sjccreate.create('SEPARATE', srccarriers: @@carrier, lots: lot, tgtcarriers: 'EMPTY')
    assert st = @@wbtqtest.mds.sjc_tasks(srccarrier: @@carrier), 'SR has not been created'
    $log.info "waiting #{@@mds_sync} s for the MDS to sync"; sleep @@mds_sync
    #
    # execute first set of manual steps
    puts "\n\nExecute the first set of manual test steps now, press ENTER to cancel the interfering SR!"
    gets
    # cancel SR
    assert @@wbtqtest.sjcdelete.delete(sr.srid)
    assert_empty @@wbtqtest.mds.sjc_requests(srid: st.srid), 'SR not canceled'
    #
    puts "\n\nRestart KitBuilding in SAP including wafer selection in FabGUI, press ENTER when ready!"
    gets
    # wait for the new SR created by SAP to expire
    $log.info "waiting 30m for the new SR created by SAP to expire"
    sleep 1800
    assert wait_for(timeout: 300) {@@wbtqtest.mds.contexts(eqp: @@eqp2).first.errorstate == 'WAFER_SORTING_FAILED'}
    #
    puts "\n\nCancel the KitBuilding Order in SAP and press ENTER when ready!"
    gets
    assert wait_for(timeout: 180) {@@wbtqtest.mds.contexts(eqp: @@eqp2) == []}
    assert_equal [], @@wbtqtest.kitcarrier_info(@@carrier).select {|e| e.status == 'ONHOLD'}
  end

  def testmanual803_build_kit
    assert @@wbtqtest.cleanup_eqp(@@sorter, mode: 'Auto-3', portstatus: 'UnloadReq'), "error cleaning up sorter"
    assert @@wbtqtest.prepare_resources([@@carrier, @@carrier_sr])
    $log.info "waiting #{@@mds_sync} s for the MDS to sync"; sleep @@mds_sync
    # execute manual steps
    puts "\n\nExecute the first set of manual test steps now, when SR has been created press ENTER to start the verification!"
    gets
    # verify kit
    assert ctx = @@wbtqtest.mds.contexts(eqp: @@eqp2).first, "no kit context found"
    ekslots = @@wbtqtest.engineering_kit_slots([3, 5, 6, 10], name: 'QA-KIT01', eqp: @@eqp2) # according to test plan
    assert @@wbtqtest.verify_kit(ctx.guid, 'SORT_REQUEST_CREATED', 'ALL_OK', kit_slots: ekslots, sr_created: true)
    assert_equal 4, @@wbtqtest.mds.sjc_wafers(srid: ctx.srid).size
    # assign empty carrier, execute SR and execute SJ
    assert @@wbtqtest.sjcqa.replace(ctx.srid, @@carrier_sr, 'BEOL')
    assert @@wbtqtest.sjcexec.execute(ctx.srid, @@sorter, @@wbtqtest.pg)
    sleep 5
    assert @@sv.claim_sj_execute(eqp: @@sorter)
    # ensure both carriers are stocked in
    assert_equal 0, @@sv.carrier_xfer_status_change(@@carrier, '', @@wbtqtest.stocker, manualin: true)
    assert_equal 0, @@sv.carrier_xfer_status_change(@@carrier_sr, '', @@wbtqtest.stocker, manualin: true)
    # execute manual steps
    puts "\n\nComplete the manual test steps now, when ready press ENTER to start the verification!"
    gets
    # verify kit using WBTQ history data
    assert @@wbtqtest.verify_kit(ctx.guid, 'FINISHED', 'ALL_OK', kit_slots: ekslots, history: true, carrier: @@carrier_sr)
    # lots have no holds
    assert_equal [], @@wbtqtest.kitcarrier_info(@@carrier).select {|e| e.status == 'ONHOLD'}
    assert_equal [], @@wbtqtest.kitcarrier_info(@@carrier_sr).select {|e| e.status == 'ONHOLD'}
  end

  def testmanual806_upqual_stb_trigger
    assert @@wbtqtest.cleanup_eqp(@@sorter, mode: 'Auto-3', portstatus: 'UnloadReq'), "error cleaning up sorter"
    assert @@wbtqtest.prepare_resources([@@carrier, @@carrier_sr])
    $log.info "waiting #{@@mds_sync} s for the MDS to sync"; sleep @@mds_sync
    # execute manual steps
    puts "\n\nExecute the first set of manual test steps now, when ready press ENTER to start the verification!"
    gets
    # verify kit
    assert ctx = @@wbtqtest.mds.contexts(eqp: @@eqp2).last, "no kit context found"
    ekslots = @@wbtqtest.engineering_kit_slots([3, 5, 10], name: 'QA-KIT01', eqp: @@eqp2) # according to test plan
    assert @@wbtqtest.verify_kit(ctx.guid, 'SORT_REQUEST_CREATED', 'ALL_OK', kit_slots: ekslots, sr_created: true)
    assert_equal 3, @@wbtqtest.mds.sjc_wafers(srid: ctx.srid).size
    # assign empty carrier, execute SR and execute SJ
    assert @@wbtqtest.sjcqa.replace(ctx.srid, @@carrier_sr, 'BEOL')
    assert @@wbtqtest.sjcexec.execute(ctx.srid, @@sorter, @@wbtqtest.pg)
    sleep 5
    assert @@sv.claim_sj_execute(eqp: @@sorter)
    # ensure both carriers are stocked in
    assert_equal 0, @@sv.carrier_xfer_status_change(@@carrier, '', @@wbtqtest.stocker, manualin: true)
    assert_equal 0, @@sv.carrier_xfer_status_change(@@carrier_sr, '', @@wbtqtest.stocker, manualin: true)
    # execute manual steps
    puts "\n\nComplete the manual test steps now, when ready press ENTER to start the verification!"
    gets
    # verify kit using WBTQ history data
    assert @@wbtqtest.verify_kit(ctx.guid, 'FINISHED', 'ALL_OK', kit_slots: ekslots, history: true, carrier: @@carrier_sr)
    # lots have no holds
    assert_equal [], @@wbtqtest.kitcarrier_info(@@carrier).select {|e| e.status == 'ONHOLD'}
    assert_equal [], @@wbtqtest.kitcarrier_info(@@carrier_sr).select {|e| e.status == 'ONHOLD'}
  end

  def testmanual807_upqual_stb_only
    assert @@wbtqtest.cleanup_eqp(@@sorter, mode: 'Auto-3', portstatus: 'UnloadReq'), "error cleaning up sorter"
    assert @@wbtqtest.prepare_resources([@@carrier, @@carrier_sr])
    $log.info "waiting #{@@mds_sync} s for the MDS to sync"; sleep @@mds_sync
    # execute manual steps
    puts "\n\nExecute the manual test steps now, when ready press ENTER to start the verification!"
    gets
    # verify kit using WBTQ history data
    assert ctx = @@wbtqtest.mds.contexts(eqp: @@wbtqtest.eqp, history: true).last, "no kit context found"
    ekslots = @@wbtqtest.engineering_kit_slots([3, 6, 10], name: 'QA-KIT03', eqp: @@wbtqtest.eqp) # according to test plan
    assert @@wbtqtest.verify_kit(ctx.guid, 'FINISHED', 'ALL_OK', kit_slots: ekslots, history: true, carrier: @@carrier)
    # lots have no holds
    assert_equal [], @@wbtqtest.kitcarrier_info(@@carrier).select {|e| e.status == 'ONHOLD'}
  end

end
