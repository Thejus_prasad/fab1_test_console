=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: Steffen Frieske, 2016-10-15

Version: 18.06

History:
  2017-06-07 sfrieske, changed test31 for v17.06
  2018-11-07 sfrieske, minor cleanup
  2021-04-21 sfrieske, moved @sorter and @pg from SiView::Test to SJC::Test

Notes:
  Limits are set to 4 min_available and max_inuse for UTFEOL, UTMOL and UTBEOL each.
  The lots in the UTBEOL carriers must be of product "UT-DIRTY_FOUP.01" on the
  route "UTRT-DIRTY-FOUP.01" with Required Carrier Category UTBEOL at the first operation.
  The second operation must have the UDATA "AutomationType" NOFOUPOPEN

=end

require 'jcap/sjctest'
require 'RubyTestCase'


class JCAP_DirtyFoupTransfer < RubyTestCase
  @@sjc_defaults = {product: 'UT-DIRTY-FOUP.01', route: 'UTRT-DIRTY-FOUP.01'}
  @@sorter = 'UTS001'
  @@product2 = 'UT-DIRTY-FOUP.02'   # UDATA SorterAutomation -CLEAN
  @@nonprobank = 'UT-NONPROD'
  @@bank2 = 'UT-USE'                # UDATA CarrierCategory ..
  @@bank_fvx = 'FILLDU'             # filler dummy bank for FVX scenario
  @@stocker2 = 'STO811'             # UDATA AmhsArea is stocker name (standalone stocker)
  @@route2 = 'UTRT-DURTY-FOUP.01'   # 'U' at 7th position like TW use routes
  # OpNos for carrier aging
  @@opNo_age = {'UTFEOL'=>'1000.100', 'UTMOL'=>'2000.1000', 'UTBEOL'=>'1000.500'}
  @@eqp_UTBEOL = 'UTF002' # for processing and reservations
  # carrier categories, 12 carriers each
  @@cat_UTFEOL = 'UTFEOL'
  @@cat_UTBEOL = 'UTBEOL'
  @@cat_UTMOL = 'UTMOL'
  @@carriers_stress = 'AY'
  #
  @@eqp = 'UTC001'        # for processing and reservations
  @@slt = 'PO'
  @@slt_qtexc = 'EO'
  @@sltscrap = 'SCRAPQA'
  @@hold = 'ENG'
  @@hold_special = 'X-JC'
  # job properties
  @@trigger_pm = 2880     # in minutes before SM pm_max is reached
  @@trigger_starts = 49   # opestarts before SM starts_max is reached
  ## max.cassettes.pz.in.use=UTFEOL=4,UTMOL=4,UTBEOL=12
  @@min_avail = 4         # min.cassettes.available=UTFEOL=4,UTMOL=4,UTBEOL=4
  @@max_srs = 2           # max.cassettes.pz.sort.request=UTMOL=2

  @@mds_sync = 45
  @@job_interval = 330    # protocol.zone.timer.start.interval + job duration
  @@testlots = []


  def self.shutdown
    @@sv.eqp_cleanup(@@eqp)
    @@testlots.each {|lot| @@sv.delete_lot_family(lot)}
    while !(cc = @@sv.carrier_list(category: @@cat_UTFEOL, carrier: @@carriers_stress + '%')).empty?
      cc.each {|c| @@sjctest.delete_registered_carrier(c)}
    end
  end


  def setup
    return unless self.class.class_variable_defined?(:@@testlots)
    assert_equal 0, @@sv.eqp_cleanup(@@eqp)        # remove NPW reservations
    @@testlots.each {|lot| @@sv.lot_cleanup(lot)}  # to avoid issues when moving between carrier categories TODO: ??
  end


  def test00_setup
    $setup_ok = false
    #
    @@sjctest = SJC::Test.new($env, @@sjc_defaults.merge(sv: @@sv, sorter: @@sorter))
    @@sm = @@sjctest.sm
    @@mds = @@sjctest.mds
    #
    @@ccats = [@@cat_UTFEOL, @@cat_UTMOL, @@cat_UTBEOL]
    # disable watchdog for the test categories
    @@ccats.each {|cat| @@sv.carrier_category_change(cat, filter_time_warn: 0, op_warning: 0)}
    #
    # delete leftovers from previous stress tests
    while !(cc = @@sv.carrier_list(category: @@cat_UTFEOL, carrier: @@carriers_stress + '%')).empty?
      cc.each {|c| assert @@sjctest.delete_registered_carrier(c)}
    end
    #
    # get operations with or w/o specific PD UDATA or QT
    assert ops = @@sm.object_info(:mainpd, @@sjctest.route, details: true).first.specific[:operations]
    assert rop = ops.find {|e| @@sv.user_data_pd(e.op)['AutomationType'].nil?}
    @@opNo = rop.opNo
    assert rop = ops.find {|e| @@sv.user_data_pd(e.op)['AutomationType'] == 'NOFOUPOPEN'}
    @@opNo_nofoupopen = rop.opNo
    assert rop = ops.find {|e| !e.qtimes.empty?}, "no QT on route #{@@sjctest.route}"
    @@opNo_qt = rop.opNo
    assert rop = ops.find{|e| e.carrier_category.empty?}
    @@opNo_nocat = rop.opNo
    assert rop = ops.find {|e| e.carrier_category == @@cat_UTMOL}, 'no UTMOL operation'
    @@opNo_utmol = rop.opNo
    assert rop = ops.find {|e| !e.reworks.empty? && e.carrier_category == @@cat_UTBEOL}, 'no UTBEOL rework operation'
    @@opNo_rwk = rop.opNo
    @@rte_rwk = rop.reworks.first[:mainpd]
    $log.info "standard opNo: #{@@opNo}, no carrier cat: #{@@opNo_nocat}, NOFOUPOPEN: #{@@opNo_nofoupopen}, QT: #{@@opNo_qt}"
    #
    # UDATA for special objects
    assert_equal '-CLEAN', @@sv.user_data(:product, @@product2)['SorterAutomation '], 'wrong UDATA'
    assert_equal @@stocker2, @@sv.user_data(:stocker, @@stocker2)['AmhsArea'], 'wrong UDATA'
    assert !@@sv.user_data(:bank, @@bank2)['CarrierCategory'].include?(@@cat_UTBEOL), 'wrong UDATA'
    #
    # verify carrier counts, remove leftover lots
    @@ccats.each {|cat|
      assert cc = @@sv.carrier_list(category: cat)
      cc.each {|c| @@sv.lot_list_incassette(c).each {|lot| @@sv.delete_lot_family(lot)}}
      assert_equal 12, cc.size, "wrong number of #{cat} carriers"
    }
    #
    # test lots for SJ classification + 1 'ager' lot
    assert @@testlots = @@sjctest.new_lots(9)
    @@agerlot = @@testlots.last
    #
    $setup_ok = true
  end

  def test11_forced
    #
    # UTFEOL: reset all carriers, eligible by opestarts (2, SM starts_max = @@trigger_starts)
    afols = reset_carriers(@@cat_UTFEOL, starts_max: @@trigger_starts + 2)
    # set 1 carrier's opestart count to 0 (and starts_max to trigger_starts)
    assert_equal 0, @@sv.carrier_counter_reset(afols.first)
    assert_equal 0, @@sv.carrier_status_change(afols.first, 'AVAILABLE', check: true)
    assert @@sm.change_carriers(afols.first, starts_max: @@trigger_starts)
    #
    # UTMOL: reset all carriers, eligible by time passed (SM pm_max = @@trigger_pm, carriers clean)
    amols = reset_carriers(@@cat_UTMOL, pm_max: @@trigger_pm, times_used: 0)
    #
    # UTBEOL: prepare carriers with exclusion conditions
    abols = reset_carriers(@@cat_UTBEOL, times_used: 0)  # prevent job actions during setup
    c_cj, c_ei, c_npw = abols.take(3)
    # CJ
    lot = @@testlots[0]
    assert_equal 0, @@sv.wafer_sort(lot, c_cj)
    assert_equal 0, @@sv.lot_opelocate(lot, @@opNo_age[@@cat_UTBEOL])
    assert @@sv.slr(@@eqp_UTBEOL, lot: lot, carrier: c_cj)
    # EI
    lot = @@testlots[1]
    assert_equal 0, @@sv.wafer_sort(lot, c_ei)
    assert_equal 0, @@sv.lot_opelocate(lot, @@opNo_age[@@cat_UTBEOL])
    assert @@sv.claim_process_lot(lot, nounload: true)
    # NPW
    assert_equal 0, @@sv.npw_reserve(@@eqp, nil, c_npw)
    # make all UTBEOL carriers eligible by age (time)
    sleep 10
    assert @@sm.change_carriers(abols, pm_max: @@trigger_pm), 'error changing carrier pm_max in SM'
    #
    # verify all carriers except special UTBEOL ones are Dirty and INUSE, no limits apply
    $log.info "waiting #{@@job_interval} s for the jCAP job"; sleep @@job_interval
    (afols + amols + abols).each {|c|
      $log.info "verifying carrier #{c}"
      cstat = @@sv.carrier_status(c)
      ccond = @@sv.user_data(:carrier, c)['CARRIER_CONDITION']
      crpt = @@mds.dirty_foup_report(carrier: c).first
      if c == afols.first  # carrier is already clean
        assert_equal 'AVAILABLE', cstat.status, "wrong carrier status"
        assert_equal 'Cleaned', ccond, "wrong carrier condition"
        assert_nil crpt, "wrong carrier report #{crpt}"
      elsif [c_cj, c_ei, c_npw].include?(c)
        assert_equal 'AVAILABLE', cstat.status, "wrong carrier status"
        assert_equal 'Dirty', ccond, "wrong carrier condition"
        refute_nil crpt, "wrong carrier report #{crpt}"
      else
        assert_equal 'INUSE', cstat.status, "wrong carrier status"
        assert_equal 'Dirty', ccond, "wrong carrier status"
        refute_nil crpt, "no classification data for carrier #{c}"
        assert_equal 'INUSE', crpt.status, "wrong carrier status #{cstat}"
        assert_equal 'EMP', crpt.classification, "wrong carrier status #{cstat}"
      end
    }
  end

  def test12_sync
    amols = reset_carriers(@@cat_UTMOL)  # prevent job actions during setup
    # prepare carriers with not in sync states
    cc = amols.take(4)
    c_due, c_dirty, c_used_inuse, c_cleaned_inuse = cc
    assert_equal 0, @@sv.carrier_condition_change(c_due, 'Due')
    assert_equal 0, @@sv.carrier_condition_change(c_dirty, 'Dirty')
    assert_equal 0, @@sv.carrier_condition_change(c_used_inuse, 'Used')
    assert_equal 0, @@sv.carrier_status_change(c_used_inuse, 'INUSE')
    assert_equal 0, @@sv.carrier_status_change(c_cleaned_inuse, 'INUSE')
    #
    $log.info "waiting #{@@job_interval} s for the jCAP job"; sleep @@job_interval
    cc.each {|c|
      cstat = @@sv.carrier_status(c)
      ccond = @@sv.user_data(:carrier, c)['CARRIER_CONDITION']
      assert_equal 'Dirty', ccond, "INUSE carrier #{c} is not Dirty"
      assert_equal 'INUSE', cstat.status, "Due carrier #{c} is not INUSE"
      verify_report(c, 'INUSE', 'EMP', 'Empty Foup')
    }
  end

  def test13_class2
    amols = reset_carriers(@@cat_UTMOL)  # prevent job actions during setup TODO: ??? verify prep ??? times_used: 1, starts_max: @@trigger_starts + 1
    #
    $log.info "waiting #{@@job_interval} s for the jCAP job"; sleep @@job_interval
    cc = @@sv.carrier_list(category: @@cat_UTMOL, status: 'INUSE')
    assert_equal 4, cc.size, "wrong number of INUSE carriers: #{cc}"
    cc.each {|c|
      cstat = @@sv.carrier_status(c)
      ccond = @@sv.user_data(:carrier, c)['CARRIER_CONDITION']
      assert_equal 'Dirty', ccond, "#{c.inspect}: wrong condition"
      verify_report(c, 'INUSE', 'EMP', 'Empty Foup')
    }
  end

  def test14_order
    amols = reset_carriers(@@cat_UTMOL)  # prevent job actions during setup
    # prepare carriers
    cc = amols.take(4)
    c_dirty_lot, c_due, c_warn, c_due_lot = cc
    # carrier Dirty with lot
    assert_equal 0, @@sv.carrier_condition_change(c_dirty_lot, 'Dirty')
    assert_equal 0, @@sv.lot_cleanup(@@testlots[0], opNo: @@opNo_utmol, carrier: c_dirty_lot)
    # carrier Due and empty
    assert_equal 0, @@sv.carrier_condition_change(c_due, 'Due')
    # carrier warn (LIMIT_REACHED)
    assert_equal 0, @@sv.user_data_update(:carrier, c_warn, {'LIMIT_REACHED'=>'1'})
    # carrier Due with lot
    assert_equal 0, @@sv.carrier_condition_change(c_due_lot, 'Due')
    assert_equal 0, @@sv.lot_cleanup(@@testlots[1], opNo: @@opNo_utmol, carrier: c_due_lot)
    #
    $log.info "waiting #{@@job_interval} s for the jCAP job"; sleep @@job_interval
    cc.each {|c| assert_equal 'Dirty', @@sv.user_data(:carrier, c)['CARRIER_CONDITION'], "carrier #{c} is not Dirty"}
    verify_report(c_dirty_lot, 'AVAILABLE', 'SJ_REQUESTED', nil)
    verify_report(c_due, 'INUSE', 'EMP', nil)
    verify_report(c_warn, 'INUSE', 'EMP', nil)
    verify_report(c_due_lot, 'AVAILABLE', 'SJ_REQUESTED', nil)
  end

  def test15_SJR
    amols = reset_carriers(@@cat_UTMOL)  # prevent job actions during setup
    # prepare carriers
    cc = amols.take(2)
    c_lot, c_sjr = cc
    # empty carrier Dirty, part of SJR, must not be touched by the job
    #   prepare SJR, lot is in c_lot, target carrier c_sjr
    assert @@sjctest.cleanup_eqp(@@sorter, mode: 'Auto-3'), "error cleaning up sorter"
    assert @@sjctest.prepare_carrier(c_lot, @@testlots[0])
    assert @@sjctest.prepare_carrier(c_sjr, 'empty')    # makes carrier AVAILABLE
    #   create COMBINE request and make the target carrier Dirty
    $log.info "waiting #{@@mds_sync} s for the MDS to sync"; sleep @@mds_sync
    assert @@sjctest.create_combine_verify(c_lot, c_sjr), "error in COMBINE request"
    assert_equal 0, @@sv.carrier_condition_change(c_sjr, 'Dirty')
    #
    $log.info "waiting #{@@job_interval} s for the jCAP job"; sleep @@job_interval
    assert_equal 'AVAILABLE', @@sv.carrier_status(c_sjr).status, "carrier status changed"
  end

  def test21_limit_reached_maxinuse
    dotest_maxinuse(limit_reached: true)
  end

  def test22_due_maxinuse
    dotest_maxinuse(due: true)
  end

  def test23_limit_reached_minavailable
    dotest_minavailable(limit_reached: true)
  end

  def test24_due_minavailable
    dotest_minavailable(due: true)
  end

  def test25_maxsrs
    # UTMOL with minavailable 4 and max_srs = 2
    amols = reset_carriers(@@cat_UTMOL)  # prevent job actions during setup
    # prepare 3 carriers with lots at an UTMOL operation
    cc_sr = amols.take(@@max_srs + 1)
    cc_sr.each_with_index {|c, i| assert_equal 0, @@sv.lot_cleanup(@@testlots[i], opNo: @@opNo_age[@@cat_UTMOL], carrier: c)}
    # mark the 3 carriers Due, leaving enough empty ones for SJ creation
    cc_sr.each {|c| assert_equal 0, @@sv.carrier_condition_change(c, 'Due')}
    #
    $log.info "waiting #{@@job_interval} s for the jCAP job"; sleep @@job_interval
    umols = @@mds.dirty_foup_report(carrier_category: @@cat_UTMOL, classification: 'SJ%')
    assert_equal cc_sr, umols.collect {|e| e.carrier}.sort, 'wrong carriers'
    assert_equal @@max_srs, @@mds.dirty_foup_report(carrier_category: @@cat_UTMOL, classification: 'SJ_REQUESTED').size
    # # all 3 carriers are marked SJ* but only 2 actually get an SR
    # nsj = 0
    # umols.each {|e|
    #   assert e.classification.start_with?('SJ'), "wrong classification for #{e}"
    #   nsj += 1 if e.classification == 'SJ_REQUESTED'
    # }
    # assert_equal @@max_srs, nsj, "wrong classification, #{@@max_srs} carriers must get an SR"
  end

  def test26_maxsrs_FVX_mixed
    # UTMOL, minavailable 4, maxinuse 4, max_srs 2
    amols = reset_carriers(@@cat_UTMOL)  # prevent job actions during setup
    # fill 5 carriers with lots (for 2 SRs)
    cc_lots = amols.take(5)
    cc_lots.each_with_index {|c, i| assert_equal 0, @@sv.lot_cleanup(@@testlots[i], opNo: @@opNo_utmol, carrier: c)}
    # move 1 lot to FILLDU bank (FVX dummy lot bank)
    lot_fvx = @@testlots[3]
    c_fvx = cc_lots[3]
    assert_equal 0, @@sv.lot_nonprobankin(lot_fvx, @@bank_fvx)
    #
    # mark the carriers Dirty except the FVX carrier (Due) and wait for report
    cc_lots.each {|c| assert_equal 0, @@sv.carrier_condition_change(c, (c == c_fvx) ? 'Due' : 'Dirty')}
    #
    $log.info "waiting #{@@job_interval} s for the jCAP job"; sleep @@job_interval
    # verify_report(c_fvx, 'AVAILABLE', 'SJ_REQUESTED', nil)  # FVX carrier has an SR
    verify_report(c_fvx, 'AVAILABLE', 'SJ%', nil)  # FVX carrier has an SR
    assert_equal @@max_srs, @@mds.dirty_foup_report(carrier_category: @@cat_UTMOL, classification: 'SJ_REQUESTED').size
    assert_equal 5, @@mds.dirty_foup_report(carrier_category: @@cat_UTMOL, classification: 'SJ%').size
  end

  def test27_many_FVX
    # UTMOL, minavailable 4, maxinuse 4, max_srs 2
    amols = reset_carriers(@@cat_UTMOL)  # prevent job actions during setup
    # fill 5 carriers with lots on a FILLDU bank (FVX dummy lot bank)
    cc_lots = amols.take(5)
    cc_lots.each_with_index {|c, i|
      assert_equal 0, @@sv.lot_cleanup(@@testlots[i], opNo: @@opNo_utmol, carrier: c)
      assert_equal 0, @@sv.lot_nonprobankin(@@testlots[i], @@bank_fvx)
    }
    #
    # mark the carriers Due and wait for report (expect _all_ carriers to get an SR)
    cc_lots.each {|c| assert_equal 0, @@sv.carrier_condition_change(c, 'Due')}
    #
    $log.info "waiting #{@@job_interval} s for the jCAP job"; sleep @@job_interval
    # assert_equal 5, @@mds.dirty_foup_report(carrier_category: @@cat_UTMOL, classification: 'SJ_REQUESTED').size
    assert_equal 5, @@mds.dirty_foup_report(carrier_category: @@cat_UTMOL, classification: 'SJ%').size
  end

  # classification tests using UTBEOL because maxinuse limit is 12 and does not become effective
  # max 8 test carriers to not hit the minavail limit
  # note: all operations must have carrier category set for correct classification

  # all test lots are re-usable after this test
  def test31_classification1
    abols = reset_carriers(@@cat_UTBEOL)  # prevent job actions during setup
    ##($log.info "waiting 45 s for the MDS to sync"); sleep 45
    cc_special = abols.take(8)
    c_lot, c_qt, c_qtexc, c_qtb, c_qtexcb, c_qtexcbhold, c_hold, c_holdspecial = cc_special
    # normal lot
    lot = @@testlots[0]
    assert_equal 0, @@sv.lot_cleanup(lot, opNo: @@opNo, carrier: c_lot)
    assert_equal 0, @@sv.sublottype_change(lot, @@slt)
    # QT lot
    lot = @@testlots[1]
    assert_equal 0, @@sv.lot_cleanup(lot, opNo: @@opNo_qt, carrier: c_qt)
    assert_equal 0, @@sv.sublottype_change(lot, @@slt)
    assert_equal 0, @@sv.lot_gatepass(lot)
    # QT lot with excepted sublottype
    lot = @@testlots[2]
    assert_equal 0, @@sv.lot_cleanup(lot, opNo: @@opNo_qt, carrier: c_qtexc)
    assert_equal 0, @@sv.sublottype_change(lot, @@slt_qtexc)
    assert_equal 0, @@sv.lot_gatepass(lot)
    # QT lot on NonProBank
    lot = @@testlots[3]
    assert_equal 0, @@sv.lot_cleanup(lot, opNo: @@opNo_qt, carrier: c_qtb)
    assert_equal 0, @@sv.sublottype_change(lot, @@slt)
    assert_equal 0, @@sv.lot_gatepass(lot)
    assert_equal 0, @@sv.lot_nonprobankin(lot, @@nonprobank)
    # QT lot on NonProBank with excepted sublottype
    lot = @@testlots[4]
    assert_equal 0, @@sv.lot_cleanup(lot, opNo: @@opNo_qt, carrier: c_qtexcb)
    assert_equal 0, @@sv.sublottype_change(lot, @@slt_qtexc)
    assert_equal 0, @@sv.lot_gatepass(lot)
    assert_equal 0, @@sv.lot_nonprobankin(lot, @@nonprobank)
    # QT lot on NonProBank with excepted sublottype, ONHOLD
    lot = @@testlots[5]
    assert_equal 0, @@sv.lot_cleanup(lot, opNo: @@opNo_qt, carrier: c_qtexcbhold)
    assert_equal 0, @@sv.sublottype_change(lot, @@slt_qtexc)
    assert_equal 0, @@sv.lot_gatepass(lot)
    assert_equal 0, @@sv.lot_hold(lot, @@hold)
    assert_equal 0, @@sv.lot_nonprobankin(lot, @@nonprobank)
    # normal lot hold
    lot = @@testlots[6]
    assert_equal 0, @@sv.lot_cleanup(lot, opNo: @@opNo, carrier: c_hold)
    assert_equal 0, @@sv.sublottype_change(lot, @@slt)
    assert_equal 0, @@sv.lot_hold(lot, @@hold)
    # special lot hold
    lot = @@testlots[7]
    assert_equal 0, @@sv.lot_cleanup(lot, opNo: @@opNo, carrier: c_holdspecial)
    assert_equal 0, @@sv.sublottype_change(lot, @@slt)
    assert_equal 0, @@sv.lot_hold(lot, @@hold_special)
    #
    # mark the special carriers Due and wait for the report
    cc_special.each {|c| assert_equal 0, @@sv.carrier_condition_change(c, 'Due')}
    $log.info "waiting #{ @@job_interval} s for the jCAP job"; sleep @@job_interval
    # because there are not enough empty AVAILABLE carriers no SRs are created
    verify_report(c_lot, 'AVAILABLE', 'SJ%', nil)
    verify_report(c_qt, 'AVAILABLE', 'Q', 'QT')
    verify_report(c_qtexc, 'AVAILABLE', 'Q', 'QT')
    verify_report(c_qtb, 'AVAILABLE', 'Q', 'QT')
    verify_report(c_qtexcb, 'AVAILABLE', 'SJ%', nil)
    verify_report(c_qtexcbhold, 'AVAILABLE', 'SJ%', nil)
    verify_report(c_hold, 'AVAILABLE', 'SJ%', nil)
    verify_report(c_holdspecial, 'AVAILABLE', 'NO', "reason code #{@@hold_special}")
  end

  # this test changes @@testlots[5..-1] such that they cannot be recovered by lot_cleanup
  def test32_classification2
    abols = reset_carriers(@@cat_UTBEOL)  # prevent job actions during setup
    cc_special = abols.take(8)
    c_nocat, c_bank, c_banknocat, c_rwk, c_scrap, c_rocket, c_product, c_wsr = cc_special
    # lot at operation w/o carrier category
    lot = @@testlots[0]
    assert_equal 0, @@sv.lot_cleanup(lot, opNo: @@opNo_nocat, carrier: c_nocat)
    assert_equal 0, @@sv.sublottype_change(lot, @@slt)
    # normal lot on end bank with carrier category UDATA
    lot = @@testlots[1]
    assert_equal 0, @@sv.lot_cleanup(lot, opNo: :last, carrier: c_bank)
    assert_equal 0, @@sv.sublottype_change(lot, @@slt)
    @@sv.lot_bankin(lot) if @@sv.lot_info(lot).bank.empty?
    assert @@sv.user_data(:bank, @@sv.lot_info(lot).bank)['CarrierCategory'].include?(@@cat_UTBEOL)
    # normal lot on end bank without carrier category UDATA
    lot = @@testlots[2]
    assert_equal 0, @@sv.lot_cleanup(lot, opNo: :last, carrier: c_banknocat)
    assert_equal 0, @@sv.sublottype_change(lot, @@slt)
    @@sv.lot_bankin(lot) if @@sv.lot_info(lot).bank.empty?
    assert_equal 0, @@sv.lot_bank_move(lot, @@bank2)
    refute @@sv.user_data(:bank, @@sv.lot_info(lot).bank)['CarrierCategory'].include?(@@cat_UTBEOL)
    # rework
    lot = @@testlots[3]
    assert_equal 0, @@sv.lot_cleanup(lot, opNo: @@opNo_rwk, carrier: c_rwk)
    assert_equal 0, @@sv.sublottype_change(lot, @@slt)
    assert_equal 0, @@sv.lot_opelocate(lot, 1)
    assert_equal 0, @@sv.lot_rework(lot, @@rte_rwk)
    # scrapped wafer
    lot = @@testlots[5]
    assert_equal 0, @@sv.lot_cleanup(lot, opNo: @@opNo, carrier: c_scrap)
    assert_equal 0, @@sv.sublottype_change(lot, @@slt)
    assert lc1 = @@sv.lot_split(lot, 2)
    assert lc2 = @@sv.lot_split(lot, 2)
    assert_equal 0, @@sv.sublottype_change(lc2, @@sltscrap)
    assert_equal 0, @@sv.scrap_wafers(lc2)
    # rocket lot
    lot = @@testlots[6]
    assert_equal 0, @@sv.lot_cleanup(lot, opNo: @@opNo, carrier: c_rocket)
    assert_equal 0, @@sv.schdl_change(lot, priority: 1)
    # lot's product UDATA SorterAutomation -CLEAN
    lot = @@testlots[7]
    assert_equal 0, @@sv.lot_cleanup(lot, opNo: @@opNo, carrier: c_product)
    assert_equal 0, @@sv.schdl_change(lot, product: @@product2, route: @@sjctest.route)
    # wafer ID WSR
    s = unique_string
    assert lot = @@sjctest.new_lot(waferids: ['WWW1', 'WSR2', 'WWW3'].collect {|w| w + s})
    @@testlots << lot
    assert_equal 0, @@sv.lot_cleanup(lot, opNo: @@opNo, carrier: c_wsr)
    #
    # mark the special carriers (including an empty one) Due and wait for the report
    cc_special.each {|c| assert_equal 0, @@sv.carrier_condition_change(c, 'Due')}
    $log.info "waiting #{@@job_interval} s for the jCAP job"; sleep @@job_interval
    # because there are not enough empty AVAILABLE carriers no SRs are created
    verify_report(c_nocat, 'AVAILABLE', 'NO', 'Missing CarrierCategory')
    verify_report(c_bank, 'AVAILABLE', 'SJ%', nil)
    verify_report(c_banknocat, 'AVAILABLE', 'NO', 'Missing CarrierCategory')
    verify_report(c_rwk, 'AVAILABLE', 'Q', 'REWORK')
    verify_report(c_scrap, 'AVAILABLE', 'NO', 'scrap wafer')
    verify_report(c_rocket, 'AVAILABLE', 'Q', 'Rocket')
    verify_report(c_product, 'AVAILABLE', 'NO', 'No FOUP allowed for this Product')
    verify_report(c_wsr, 'AVAILABLE', 'WSR', 'Unreadable wafer')
  end

  # this test changes @@testlots[2..] such that they cannot be recovered by lot_cleanup
  def test33_classification3
    abols = reset_carriers(@@cat_UTBEOL)  # prevent job actions during setup
    cc_special = abols.take(8)
    c_sto, c_cj, c_tw, c_nofoupopen, c_notavail, c_emp = cc_special
    # standalone stocker
    lot = @@testlots[0]
    assert_equal 0, @@sv.lot_cleanup(lot, opNo: @@opNo, carrier: c_sto)
    assert_equal 0, @@sv.sublottype_change(lot, @@slt)
    assert_equal 0, @@sv.carrier_xfer_status_change(c_sto, 'MI', @@stocker2)
    # lot is processing (but carrier not EI), need to set Due before lot has a cj
    lot = @@testlots[1]
    assert_equal 0, @@sv.lot_cleanup(lot, opNo: @@opNo, carrier: c_cj)
    assert_equal 0, @@sv.sublottype_change(lot, @@slt)
    assert_equal 0, @@sv.carrier_condition_change(c_cj, 'Due')
    assert @@sv.claim_process_lot(lot, mode: 'Auto-1', noopecomp: true)  # earlyfoup
    assert_equal 0, @@sv.carrier_xfer_status_change(c_cj, 'MI', @@sjctest.stocker)
    # lot on test wafer use route
    lot = @@testlots[2]
    assert_equal 0, @@sv.lot_cleanup(lot, opNo: @@opNo, carrier: c_tw)
    assert_equal 0, @@sv.sublottype_change(lot, @@slt)
    assert_equal 0, @@sv.schdl_change(lot, route: @@route2)
    # lot at NOFOUPOPEN PD
    lot = @@testlots[3]
    assert_equal 0, @@sv.lot_cleanup(lot, opNo: @@opNo_nofoupopen, carrier: c_nofoupopen)
    assert_equal 0, @@sv.sublottype_change(lot, @@slt)
    # carrier NOTAVAILABLE (wafer ID read error scenario)
    assert_equal 0, @@sv.carrier_status_change(c_notavail, 'NOTAVAILABLE')
    #
    # mark the special carriers Due (c_sj is already Due) and wait for the report
    (cc_special - [c_cj]).each {|c| assert_equal 0, @@sv.carrier_condition_change(c, 'Due')}
    $log.info "waiting #{@@job_interval} s for the jCAP job"; sleep @@job_interval
    verify_report(c_sto, 'AVAILABLE', 'NO', 'Stand alone stocker')
    verify_report(c_cj, 'AVAILABLE', 'NO', 'Processing')
    verify_report(c_tw, 'AVAILABLE', 'Q', 'TW Lot')
    verify_report(c_nofoupopen, 'AVAILABLE', 'Q', 'FOUP open')
    ## disabled in 17.02  verify_report(c_notavail, 'NOTAVAILABLE', 'EMP', 'Empty Foup')
    verify_report(c_emp, 'INUSE', 'EMP', 'Empty Foup')
  end

  # pre-version Jan-23: 177 s
  def testman91_stress
    # delete existing carriers
    while !(cc = @@sv.carrier_list(category: @@cat_UTFEOL, carrier: @@carriers_stress + '%')).empty?
      cc.each {|c| assert @@sjctest.delete_registered_carrier(c)}
    end
    # register carriers
    n = 5000
    cc = n.times.collect {|i| @@carriers_stress + '%4.4d' % i}
    assert @@sv.durable_register('Cassette', cc,  @@cat_UTFEOL)
    # set them INUSE
    assert_equal 0, @@sv.carrier_status_change(cc, 'AVAILABLE')
    $log.info "Press ENTER to continue"
    gets
    assert_equal 0, @@sv.carrier_status_change(cc, 'INUSE')
    # delete the carriers
    $log.info "Press ENTER to clean up"
    gets
    while !(cc = @@sv.carrier_list(category: @@cat_UTFEOL, carrier: @@carriers_stress + '%')).empty?
      cc.each {|c| assert @@sjctest.delete_registered_carrier(c)}
    end
  end


  # aux methods

  # list carrier status and condition (dev aide)
  def list_carriers(cat)
    @@sv.carrier_list(category: cat).each {|c|
      cstat = @@sv.carrier_status(c)
      ccond = @@sv.user_data(:carrier, c)['CARRIER_CONDITION']
      puts "#{c}  #{ccond}  #{cstat.status}  #{@@sv.user_data(:carrier, c)['LIMIT_REACHED']}  #{cstat.xfer_status} #{cstat.lots}"
    }
  end

  # reset carrier category data, SJRs, carriers to clean, empty, stocked in, AVAILABLE; return all carriers of the category on success
  def reset_carriers(cat, params={})
    pm_max = params[:pm_max] || @@trigger_pm + 720            # SM time for forced cleaning (default: safe, above current value)
    starts_max = params[:starts_max] || @@trigger_starts + 3  # SM opestarts for forced cleaning (default: safe, above current value)
    return @@sv.carrier_list(category: cat).each {|c|
      # reset LIMIT_REACHED and usage counts
      assert_equal 0, @@sv.user_data_update(:carrier, c, {'LIMIT_REACHED'=>'0'})
      assert_equal 0, @@sv.carrier_counter_reset(c)           # resets also FRCAST.TIMES_USED
      # set SM data (for forced cleaning)
      # cstat = @@sv.carrier_status(c)
      # if cstat.pm_max != pm_max || cstat.starts_max != starts_max
      pminfo = @@sv.carrier_status(c, raw: true).cassettePMInfo
      if pminfo.intervalBetweenPM != pm_max || pminfo.maximumOperationStartCount != starts_max
        assert @@sm.change_carriers(c, pm_max: pm_max, starts_max: starts_max)
      end
      # age the carriers
      assert_equal 0, @@sv.carrier_status_change(c, 'AVAILABLE', check: true), 'error making carrier AVAILABLE'
      @@sv.lot_list_incassette(c).each {|lot| assert_equal 0, @@sv.wafer_sort(lot, '')}
      assert_equal 0, @@sv.wafer_sort(@@agerlot, c)
      (params[:times_used] || 2).times {                      # default: 2 (still below starts_max but not 'Clean')
        assert_equal 0, @@sv.lot_opelocate(@@agerlot, @@opNo_age[cat])
        assert @@sv.claim_process_lot(@@agerlot)
      }
      # delete stale SRs, (set carrier AVAILABLE), stock it in and remove lots
      assert @@sjctest.cleanup_sort_requests(c)
      assert @@sjctest.cleanup_carrier(c, make_empty: true)   # does not reset FRCAST.TIMES_USED
    }
  end

  def verify_report(carrier, status, cl, reason)
    $log.info "verify_report #{carrier.inspect}, #{status.inspect}, #{cl.inspect}, #{reason.inspect}"
    assert crpt = @@mds.dirty_foup_report(carrier: carrier).first, "no report for carrier #{carrier}"
    assert @@mds.dirty_foup_report(carrier: carrier, classification: cl).first , "wrong classification, expected #{cl}, got #{crpt}"
    assert crpt.reason.include?(reason), "wrong classification reason #{crpt}" if reason
    assert_equal status, crpt.status, "wrong carrier status #{crpt}"
  end

  def dotest_maxinuse(params={})
    # prepare carriers: all clean in UTFEOL, 5 LIMIT_REACHED in UTMOL, 5 LIMIT_REACHED in UTBEOL
    reset_carriers(@@cat_UTFEOL)
    amols = reset_carriers(@@cat_UTMOL)
    abols = reset_carriers(@@cat_UTBEOL)
    $lmols = lmols = amols.take(5)
    $lbols = lbols = abols.take(5)
    if params[:limit_reached]
      (lmols + lbols).each {|c| @@sv.user_data_update(:carrier, c, {'LIMIT_REACHED'=>'1'})}
    end
    if params[:due]
      (lmols + lbols).each {|c| @@sv.carrier_condition_change(c, 'Due')}
    end
    #
    $log.info "waiting #{@@job_interval} s for the jCAP job"; sleep @@job_interval
    # UTFEOL: no carrier INUSE (reference)
    ufols = @@sv.carrier_list(category: @@cat_UTFEOL, status: 'INUSE')
    assert_empty ufols, "wrong UTFEOL carriers INUSE: #{ufols} - bad category 2 scenario?"
    # UTMOL: 4 out of 5 INUSE because maxinuse = 4
    umols = @@sv.carrier_list(category: @@cat_UTMOL, status: 'INUSE')
    umols.each {|c| assert_equal 'Dirty', @@sv.user_data(:carrier, c)['CARRIER_CONDITION']}
    assert_equal 4, umols.size, 'wrong UTMOL carriers INUSE'
    ### TODO: ???
    assert_equal 1, (lmols - umols).size, 'wrong UTMOL carriers INUSE'
    # UTBEOL: all 5 INUSE because maxinuse = 12
    ubols = @@sv.carrier_list(category: @@cat_UTBEOL, status: 'INUSE')
    ubols.each {|c| assert_equal 'Dirty', @@sv.user_data(:carrier, c)['CARRIER_CONDITION']}
    assert_equal lbols, ubols, 'wrong UTBEOL carriers INUSE'
  end

  def dotest_minavailable(params={})
    abols = reset_carriers(@@cat_UTBEOL)
    # one of the carriers has a higher opestarts count
    c_op = abols[-2]
    assert_equal 0, @@sv.wafer_sort(@@agerlot, c_op)
    assert_equal 0, @@sv.lot_opelocate(@@agerlot, @@opNo_age[@@cat_UTBEOL])
    assert @@sv.claim_process_lot(@@agerlot)
    assert @@sjctest.cleanup_carrier(c_op, make_empty: true)
    #
    if params[:limit_reached]
      abols.each {|c| @@sv.user_data_update(:carrier, c, {'LIMIT_REACHED'=>'1'})}
    end
    if params[:due]
      abols.each {|c| @@sv.carrier_condition_change(c, 'Due')}
    end
    $log.info "waiting #{@@job_interval} s for the jCAP job"; sleep @@job_interval
    # verify 8 carriers are INUSE because minavailable = 4, incl the the one with highest opecount
    ubols = @@sv.carrier_list(category: @@cat_UTBEOL, status: 'INUSE')
    assert_equal abols.size - @@min_avail, ubols.size, "wrong UTBEOL carriers INUSE: #{ubols}"
    assert ubols.include?(c_op), "wrong UTBEOL carriers INUSE: #{ubols}"
    ubols.each {|c| assert_equal 'Dirty', @@sv.user_data(:carrier, c)['CARRIER_CONDITION']}
  end

end
