=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.


Author: Steffen Frieske, 2016-11-21

Version: 18.09

History:
  2017-01-05 sfrieske, use new  RTD::EmuRemote, minor cleanup
  2018-11-07 sfrieske, minor cleanup
  2019-02-19 sfrieske, improved EmuRemote usage
  2021-08-30 sfrieske, moved xmlhash.rb to util

Notes:
  This job periodically calls RTD and just acts on the response, no logic contained.
  actions: nonpro bankin, bankout, bankmove, gatepass
=end

require 'mqjms'
require 'util/xmlhash'
require 'rtdserver/rtdemuremote'
require 'RubyTestCase'


class JCAP_CondiKitBuild < RubyTestCase
  @@rtd_rule = 'JCAP_AUTOCOND_KITBUILD'
  @@columns = ['virt_eqp_id', 'kit_part_id', 'info_message']
  @@job_interval = 330  # 5 minutes


  def self.startup
    ##super(nosiview: true)
    @@mq = MQ::JMS.new("ackb.#{$env}")
    @@rtdremote = RTD::EmuRemote.new($env)
  end

  def self.shutdown
    @@rtdremote.delete_emustation('', rule: @@rtd_rule)
  end

  def setup
    refute_nil @@mq.delete_msgs(nil), 'MQ issues'
  end

  def teardown
    assert @@rtdremote.delete_emustation('', rule: @@rtd_rule)
  end


  def test11_happy
    $setup_ok = false
    #
    # RTDEmu answer data
    data = [
      ['QAeqp0', 'QApart0', 'QAmessage0'],
      ['', 'QApart1', 'QAmessage1'],
      ['QAeqp2', '', 'QAmessage2'],
      ['QAeqp3', 'QApart3', ''],
      ['QAeqp4', 'QApart4', 'QAmessage4']
    ]
    assert @@rtdremote.add_emustation_wait('', rule: @@rtd_rule, timeout: @@job_interval) {
      @@rtdremote.create_response(@@columns, {'rows'=>data})
    }, 'rule has not been called'
    # allow the job to write the msgs
    sleep 10
    msgs = @@mq.read_msgs
    assert_equal 3, msgs.size, 'job not running or using a wrong MQ queue'
    # verify msgs for data 0, 3, 4 created?
    [data[0], data[3], data[4]].each_with_index {|d, idx|
      expected = {:EXT_EVENT=>{
        :HEADER=>
          {:EVENT=>'AUTOCONDIKIT_BEGIN', :OBJECT_TYPE=>'IE', :OBJECT=>d[0]},  # eqp
        :AUTOCONDIKIT=>
          {:UID=>d[1], :BAUTL=>d[1], :INFOTEXT=>(d[2].empty? ? nil : d[2])},  # uid, part and info
      }}
      assert_equal expected, XMLHash.xml_to_hash(msgs[idx]), "wrong msg\n#{msgs[idx]}"
    }
    #
    $setup_ok = true
  end

  def test12_wrong_headers
    # set RTDEmu data with wrong headers
    assert @@rtdremote.add_emustation_wait('', rule: @@rtd_rule, timeout: @@job_interval) {
      @@rtdremote.create_response(['x', 'y', 'z'], {'rows'=>[['QAeqp0', 'QApart0', 'QAmessage0']]})
    }, 'rule has not been called'
    # allow the job to write msgs
    sleep 10
    assert_equal 0, @@mq.qdepth, 'unexpected MQ msg'
  end

end
