=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: sfrieske, 2020-12-07

Version: 22.01

History:
  2021-08-27 sfrieske, require waitfor instead of misc

Notes:
  see http://f1onewiki:21080/display/MGA/JCAP%3A+GRR+Measurement+Controller
=end

require 'catalyst/grrstudy'
require 'util/waitfor'
require 'SiViewTestCase'


class JCAP_GRRMC < SiViewTestCase
  @@sv_defaults = {product: 'UT-PRODUCT-GRR-PFL.01', route: 'UTRT-GRR-ORIG.01', nwafers: 3, carriers: 'JC%'}
  @@original_pds = %w(UTPDM-GRR-ORIG-1T.01 UTPDM-GRR-ORIG-2T.01 UTPDM-GRR-ORIG-3T.01 UTPDM-GRR-ORIG-NT.01)
  @@op_eqps = {
    'UTPDM-GRR-ORIG-1T.01'=>[],
    'UTPDM-GRR-ORIG-2T.01'=>[],
    'UTPDM-GRR-ORIG-3T.01'=>[],
    'UTPDM-GRR-ORIG-NT.01'=>[]
  }
  @@subroute = 'M-GAGE-RR.01'   # 9 meas PDs
  @@sub_op1 = 'M-GAGE-RR-MEASUREMENT-T1.01'
  @@sub_oplast = 'M-GAGE-RR-RETURN.01'
  @@holdcode = 'M-GRR'
  @@holdcode_error = 'X-MH'
  @@holdcode_extra = 'FAT'
  # for wafer selection (taken from first PD's UDATA on @@subroute)
  @@ptype = 'TX'
  @@player = 'GRR'

  @@rtd_rule = 'JCAP_GRRMC'
  # @@rtd_category = 'DISPATCHER/JCAP'

  @@job_interval = 120
  @@testlots = []


  def self.startup
    super
    @@wafersel = Catalyst::WaferSelection.new($env)
    @@apc = APC::Client.new($env)
    @@cat2 = APC::DBCat2.new($env)
    @@rtdremote = RTD::EmuRemote.new($env)
    @@grr_mcs = []
    @@original_pds.each {|pd|
      @@grr_mcs << JCAP::GRRMC.new($env, @@svtest.route, pd, sv: @@sv,
        apc: @@apc, cat2: @@cat2, wafersel: @@wafersel, rtdremote: @@rtdremote)
    }
    @@subroute_ops = @@sv.route_operations(@@subroute)
  end

  def self.after_all_passed
    @@rtdremote.delete_emustation('', rule: @@rtd_rule)
    @@testlots.each {|lot| @@sv.delete_lot_family(lot)}
  end


  # single happy lot, for dev
  def testdev01
    assert lot = @@svtest.new_lot, 'error creating test lot'
    @@testlots << lot
    assert @@grr_mcs[1].jcap_runtest(lot, true)
    # grrmc = @@grr_mcs[1]  # 2T
    # assert grrmc.jcap_prepare_lot(lot)
    # assert grrmc.jcap_branch_lot(lot)
    # assert grrmc.jcap_measroute(lot)
    # assert grrmc.apc_verify_study(lot, true)
  end

  # create and prepare a lot for manual tests
  def testman09_prepare_lot
    assert lot = @@svtest.new_lot, 'error creating test lot'
    @@testlots << lot
    assert @@grr_mcs[1].jcap_prepare_lot(lot)
  end


  def test10_prepare_nocandidates
    $setup_ok = false
    #
    # verify PD UDATA
    @@subroute_ops[0 .. -2].each {|e|
      op = e.operationID.identifier
      udata = @@sv.user_data_pd(op)
      assert_equal 'useLotScriptParam.GRROriginalPD', udata['ExperimentOriginalPD'], "wrong UDATA for PD #{op}"
      assert_equal @@ptype, udata['ProcessTypeWfrSelection'], "wrong UDATA for PD #{op}"
      assert_equal @@player, udata['ProcessLayer'], "wrong UDATA for PD #{op}"
    }
    #
    assert lots = @@svtest.new_lots(6), 'error creating test lots'
    @@testlots += lots
    #
    # prepare all lots and RTD answers
    @@rtd_data = {}
    #
    # lot0: extra hold
    lot = lots[0]
    grrmc = @@grr_mcs[0]
    assert grrmc.jcap_prepare_lot(lot)
    @@rtd_data[lot] = grrmc.rtd_data.clone
    assert_equal 0, @@sv.lot_hold(lot, @@holdcode_extra), 'error setting lot hold'
    #
    # lot1: 0 out of 3 tools
    lot = lots[1]
    grrmc = @@grr_mcs[2]
    assert grrmc.jcap_prepare_lot(lot)
    @@rtd_data[lot] = grrmc.rtd_data.take(0).clone
    #
    # lot2: 1 out of 2 tools
    lot = lots[2]
    grrmc = @@grr_mcs[1]
    assert grrmc.jcap_prepare_lot(lot)
    @@rtd_data[lot] = grrmc.rtd_data.take(1).clone
    #
    # lot3: 1 out of 3 tools
    lot = lots[3]
    grrmc = @@grr_mcs[2]
    assert grrmc.jcap_prepare_lot(lot)
    @@rtd_data[lot] = grrmc.rtd_data.take(1).clone
    #
    # lot4: 2 out of n tools
    lot = lots[4]
    grrmc = @@grr_mcs[3]
    assert grrmc.jcap_prepare_lot(lot)
    @@rtd_data[lot] = grrmc.rtd_data.take(2).clone
    #
    # lot5: wrong carrier category
    lot = lots[5]
    assert ec = @@svtest.get_empty_carrier(carrier_category: 'C4', carrier: '%')
    assert_equal 0, @@sv.wafer_sort(lot, ec), 'error moving lot into carrier'
    grrmc = @@grr_mcs[0]
    assert grrmc.jcap_prepare_lot(lot)
    @@rtd_data[lot] = grrmc.rtd_data.clone
    #
    # final preparation
    @@rtd_lot_called = {}
    lots.each {|lot| @@rtd_lot_called[lot] = false}
    @@rtd_method = proc {|stn, rule, rparams|
      lot = rparams['lot_id']
      @@rtd_lot_called[lot] = true
      $log.info "EmuRemote response for #{lot}: #{@@rtd_data[lot]}"
      @@rtdremote.create_response(['eqp_id'], {'rows'=>@@rtd_data[lot]})
    }
    assert @@rtdremote.add_emustation('', rule: @@rtd_rule, method: @@rtd_method), 'error adding RTD emustation'
    lots.each {|lot|
      # gatepass (a.k.a. opecomp at GRR original meas PD)
      assert_equal 0, @@sv.lot_gatepass(lot, force: :ondemand), 'error gatepassing lot'
    }
    #
    # wait for the JCAP job to run
    $log.info "waiting up to #{@@job_interval + 60} s for the JCAP job to call RTD"
    # sleep @@job_interval + 60
    reflot = lots.last
    assert wait_for(timeout: @@job_interval + 60) {@@rtd_lot_called[reflot]}, 'RTD was not called'
    $log.info 'RTD was called, waiting another 60 s'
    sleep 60
    #
    $setup_ok = true
  end

  def test11_extrahold
    # lot0: extra hold
    lot = @@testlots[0]
    refute_empty @@sv.lot_hold_list(lot, reason: @@holdcode), 'wrong GRR hold'
    refute_empty @@sv.lot_hold_list(lot, reason: @@holdcode_extra), 'missing additional hold'
    li = @@sv.lot_info(lot)
    assert_equal @@svtest.route, li.route, 'lot not on main route'
    assert_empty @@sv.user_parameter('Lot', lot, name: 'GRROriginalPD').value, 'wrong GRROriginalPD value'
  end

  def test12_0_3
    # lot1: no tools in Rtd response
    lot = @@testlots[1]
    assert_empty @@sv.lot_hold_list(lot, reason: @@holdcode), 'wrong GRR hold'
    li = @@sv.lot_info(lot)
    assert_equal @@svtest.route, li.route, 'lot not on main route'
    assert_empty @@sv.user_parameter('Lot', lot, name: 'GRROriginalPD').value, 'wrong GRROriginalPD value'
  end

  def test13_1_2
    # lot2: 1 out of 2 tools
    lot = @@testlots[2]
    assert_empty @@sv.lot_hold_list(lot, reason: @@holdcode), 'wrong GRR hold'
    li = @@sv.lot_info(lot)
    assert_equal @@svtest.route, li.route, 'lot not on main route'
    assert_empty @@sv.user_parameter('Lot', lot, name: 'GRROriginalPD').value, 'wrong GRROriginalPD value'
  end

  def test14_1_3
    # lot3: 1 out of 3 tools
    lot = @@testlots[3]
    assert_empty @@sv.lot_hold_list(lot, reason: @@holdcode), 'wrong GRR hold'
    li = @@sv.lot_info(lot)
    assert_equal @@svtest.route, li.route, 'lot not on main route'
    assert_empty @@sv.user_parameter('Lot', lot, name: 'GRROriginalPD').value, 'wrong GRROriginalPD value'
  end

  def test15_2_n
    # lot4: 2 out of n tools
    lot = @@testlots[4]
    assert_empty @@sv.lot_hold_list(lot, reason: @@holdcode), 'wrong GRR hold'
    li = @@sv.lot_info(lot)
    assert_equal @@svtest.route, li.route, 'lot not on main route'
    assert_empty @@sv.user_parameter('Lot', lot, name: 'GRROriginalPD').value, 'wrong GRROriginalPD value'
  end

  def test16_wrong_carrier_category
    # lot5: wrong carrier category
    lot = @@testlots[5]
    assert_empty @@sv.lot_hold_list(lot, reason: @@holdcode), 'wrong GRR hold'
    li = @@sv.lot_info(lot)
    assert_equal @@svtest.route, li.route, 'lot not on main route'
    assert_empty @@sv.user_parameter('Lot', lot, name: 'GRROriginalPD').value, 'wrong GRROriginalPD value'
    assert_empty @@sv.lot_fpc_infos(lot: lot), 'wrong FPC definition'
  end


  def test21_1T_happy  # APC study for 1T not yet working
    assert lot = @@svtest.new_lot, 'error creating test lot'
    @@testlots << lot
    assert @@grr_mcs[0].jcap_runtest(lot, true, parameters: ['MyParameterA'])
  end

  def test22_2T_happy
    assert lot = @@svtest.new_lot, 'error creating test lot'
    @@testlots << lot
    assert @@grr_mcs[1].jcap_runtest(lot, true)
  end

  def test23_3T_happy
    assert lot = @@svtest.new_lot, 'error creating test lot'
    @@testlots << lot
    assert @@grr_mcs[2].jcap_runtest(lot, true)
  end

  def test24_3T_2branching
    assert lot = @@svtest.new_lot, 'error creating test lot'
    @@testlots << lot
    assert @@grr_mcs[2].jcap_runtest(lot, true, neqps_branching: 2)
  end

  def test25_NT_3branching
    assert lot = @@svtest.new_lot, 'error creating test lot'
    @@testlots << lot
    assert @@grr_mcs[3].jcap_runtest(lot, true, neqps_branching: 3)
  end

  def test26_3T_1onroute
    assert lot = @@svtest.new_lot, 'error creating test lot'
    @@testlots << lot
    assert @@grr_mcs[2].jcap_runtest(lot, false, neqps_measroute: 1)
  end

  def test27_has_child
    # happy lot but with a child lot
    assert lot = @@svtest.new_lot, 'error creating test lot'
    @@testlots << lot
    assert lc = @@sv.lot_split(lot, [2]), 'error splitting lot'
    assert @@grr_mcs[1].jcap_runtest(lot, true)
  end

  def test31_filter_measurement_tools
    assert lot = @@svtest.new_lot, 'error creating test lot'
    @@testlots << lot
    grrmc = @@grr_mcs[0]  # 1T
    assert grrmc.jcap_prepare_lot(lot)
    grrmc.rtd_data += [['A-DUMMY'], ['UTC003']]
    assert grrmc.jcap_branch_lot(lot)
    assert grrmc.jcap_measroute(lot, parameters: ['MyParameterA'])
    # assert grrmc.apc_verify_study(lot, true)
  end

end
