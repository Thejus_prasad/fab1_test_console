=begin 
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.
    
Author: Steffen Frieske, 2016-04-21

Version: 18.05

History:
  2018-11-07 sfrieske, minor cleanup
=end

require 'SiViewTestCase'


class JCAP_SkipLagTime < SiViewTestCase
  @@sv_defaults = {product: 'ITDC-JCAP-TEST.01', route: 'ITDC-JCAP-TEST.01', carriers: '%'}
  @@pd_excluded = 'ITDC-JCAP-TEST-GATEPASS.01'  # must be at another opNo
  @@carrier_category_wrong = 'TKEY'
  @@holdreason2 = 'C-ENG'
  @@sublottype = 'EJ'
  @@sublottype2 = 'PO'

  @@job_interval = 330


  def self.after_all_passed
    @@testlots.each {|lot| @@sv.delete_lot_family(lot)}
  end
  

  def test00_setup
    $setup_ok = false
    #
    assert cwrong = @@svtest.get_empty_carrier(carrier_category: 'TKEY', carrier: '9%'), 'no empty 9 carrier'
    #
    # create test lots
    assert @@testlots = @@svtest.new_lots(6, sublottype: @@sublottype)
    # lot0 happy
    # lot1 multiple holds
    assert_equal 0, @@sv.lot_hold(@@testlots[1], 'PLTH', user: 'NOX-UNITTEST')
    assert_equal 0, @@sv.lot_hold(@@testlots[1], @@holdreason2)
    # lot2 wrong sublottype
    assert_equal 0, @@sv.sublottype_change(@@testlots[2], @@sublottype2)
    # lot3 excluded PD
    refute_equal @@pd_excluded, @@sv.lot_info(@@testlots[3]).op, "setup error, wrong ecluded PD #{@@pd_excluded}"
    assert_equal 0, @@sv.lot_opelocate(@@testlots[3], nil, op: @@pd_excluded)
    # lot4 wrong carrier category
    assert_equal 0, @@sv.wafer_sort(@@testlots[4], cwrong)
    #
    # set holds
    @@testlots.each {|lot| assert_equal 0, @@sv.lot_hold(lot, 'PLTH')}
    #
    # lot5 wrong lot state
    assert_equal 0, @@sv.scrap_wafers(@@testlots[5])
    #
    $setup_ok = true
  end

  def test01_job
    $log.info "waiting #{@@job_interval} s for the job to run"
    sleep @@job_interval
  end

  def test10_happy
    assert_empty @@sv.lot_hold_list(@@testlots[0]), 'lagtime hold not removed'
  end

  def test11_multiple_holds
    lhs = @@sv.lot_hold_list(@@testlots[1])
    assert_equal 1, lhs.size, 'lagtime hold not removed'
    assert_equal @@holdreason2, lhs.first.reason, 'unrelated hold removed'
  end

  def test12_sublottype
    refute_empty @@sv.lot_hold_list(@@testlots[2]), 'lagtime hold removed'
  end

  def test13_pd
    refute_empty @@sv.lot_hold_list(@@testlots[3]), 'lagtime hold removed'
  end

  def test14_carrier
    refute_empty @@sv.lot_hold_list(@@testlots[4]), 'lagtime hold removed'
  end

  def test15_scrapped
    refute_empty @@sv.lot_hold_list(@@testlots[4]), 'lagtime hold removed'
  end

end
