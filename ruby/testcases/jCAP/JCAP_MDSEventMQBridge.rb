=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.


Author: Steffen Frieske, 2019-02-22

Version: 19.02

History:
=end

require 'dbsequel'
require 'mqjms'
require 'RubyTestCase'


class JCAP_MDSEventMQBridge < RubyTestCase
  @@nmsgs = 5000  # MQ limit
  @@mqtimeout = 10

  def self.startup
    # no SiView
  end

  def test00_setup
    $setup_ok = false
    #
    @@db = ::DB.new("mds.#{$env}")
    #
    @@mq = MQ::JMS.new("qaany.#{$env}")
    refute_nil @@mq.delete_msgs(nil)
    s = Time.now.utc.strftime('%F_%T').gsub(':', '-')
    assert @@mq.send_msg(s), 'error sending msg'
    assert_equal s, @@mq.receive_msg, 'error receiving msg'
    #
    $setup_ok = true
  end

  def test11
    refute_nil @@mq.delete_msgs(nil)
    # call the MDS proc once and receive the MQ message
    s = Time.now.utc.strftime('%F_%T').gsub(':', '-')
    @@db.execute("CALL P_SIGNAL_MDS_EVENT('MSG_TO_MQ', '1', '#{@@mq.queue.name}', '#{s}')")
    assert_equal s, @@mq.receive_msg(timeout: @@mqtimeout), 'error receiving msg'
    #
    tstart = Time.now
    # call the MDS proc nmsgs times
    $log.info "calling the MDS proc #{@@nmsgs} times"
    @@nmsgs.times {|i| @@db.execute("CALL P_SIGNAL_MDS_EVENT('MSG_TO_MQ', '1', '#{@@mq.queue.name}', '#{i}')")}
    # receive the MQ msgs and verify order
    $log.info "verifying #{@@nmsgs} ordered msgs"
    @@nmsgs.times {|i| assert_equal i.to_s, @@mq.receive_msg(timeout: @@mqtimeout), 'wrong msg'}
    #
    duration = Time.now - tstart
    $log.info "\n\n--> msgs: #{@@nmsgs}, duration: #{duration} s, rate: #{(@@nmsgs / duration).round} msgs/s\n"
  end

end
