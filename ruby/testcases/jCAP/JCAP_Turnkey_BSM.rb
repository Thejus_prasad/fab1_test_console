=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author:  sfrieske, 2018-11-14
Version: 21.01

History:
  2021-08-27 sfrieske, require uniquestring instead of misc

Notes:
  see https://docs.google.com/document/d/1qvC8aw5Fe3jNS40x2Cb9v3ljUW2xmPkgPpqRpGeTeHA/edit#heading=h.dc32t6dysrql
=end

require 'apfam'
require 'util/waitfor'
require 'SiViewTestCase'


class JCAP_Turnkey_BSM < SiViewTestCase
  #@@sv_defaults = {product: 'SHANTI1-A1FAAU.AA01', route: 'C02-TKEY.01', customer: 'gf'}
  @@sv_defaults = {product: 'SHANTI1-A1FAAB.AA01', route: '014LPP-00-BUMP-01-SHANTI1.0001', customer: 'qgt', erpitem: 'ENGINEERING-U00'}
  #  @@sv_defaults = {product: 'SHANTI1-A1FAAS.AA01', route: 'UTRT-TKBTF-SORT-QT.01', customer: 'qgt', erpitem: '*SHANTI1-A1FAAU-JAA01'}
   # @@sv_defaults = {product: 'RHEAB1MA.A4', route: 'UTRT-TKBTF-SUBCON-BUMP.01',customer: 'qgt', erpitem: 'RHEAB1MA-LA4'}


  #@@slt = 'EC'  # or PR
  @@slt = 'PO'  # or PR
  #@@fab_qtstart = '6400.1000'
  @@fab_qtstart = '98400.1500'
    @@fab_qtstart1 = '98601.1100'

  @@endbanks = ['G-WEND', 'G-BEND']            # property, for search candidates
  @@bank = 'G-BEND'
  @@tkparams = {'ERPItem'=>'SHANTI11MA-JA0', 'TurnkeyType'=>'J', 'TurnkeySubconIDBump'=>'GG', 'TurnkeySubconIDSort'=>'GG'}

  @@amjob_handle = 'Job-99'    # http://f36apfd1:21241/jobs/details/Job-99
  @@amjob_name = '/Reporting/DATABASE/MDS_T_START_PLAN_Every_30_Min_04'

  @@job_interval = 330  # JCAP job interval

  @@testlots = []


  def self.after_all_passed
    @@testlots.each {|lot| @@sv.delete_lot_family(lot)}
  end


  def test00_setup
    $setup_ok = false
    #
    # @@mdm = ERPMDM.new($env)
    #
    # AM client, ensure AM job can be controlled
    @@apfam = APF::AM.new("bsm_#{$env}")
    assert details = @@apfam.job_details(@@amjob_handle)
    assert_equal @@amjob_name, File.join(details[:folder], details[:name]), 'wrong job handle'
    # ensure current BSM startsplan gets loaded
    assert @@apfam.wait_run_job(@@amjob_handle, action: 'ManualStart_FabGUI'), 'error running T_START_PLAN update job'
    #
    # remove lots from endbanks
    @@endbanks.each {|b| @@sv.lot_list(bank: b).each {|l| @@sv.delete_lot_family(l)}}
    #
    $setup_ok = true
  end


  # BTF starts plan:
  # gf,  PO, SHANTI11MA-JA0, SHANTI1-A1FAAU.AA01 -> qgt, EB, SHANTI11MA-JA0, 'SHANTI1-A1FAAB.AA01'
  # qgt, EC, SHANTI11MA-JA0, SHANTI1-A1FAAU.AA01 -> qgt, EB, SHANTI11MA-JA0, 'SHANTI1-A1FAAB.AA01'
  def testman11
    # create fab layer lots, complete and move them to the configured end bank
    assert lots = @@svtest.new_lots(4, nwafers: 2, sublottype: 'PO', bankin: true), 'error creating regular testlots'
    @@testlots += lots
    # lot0: gf, PO, ENGINEERING-U00 -> not started
    # nothing changed
    # lot1: gf, PO, SHANTI11MA-JA0 -> qgt, EB, 'SHANTI1-A1FAAB.AA01', SHANTI11MA-JA0
    lot = lots[1]
    assert_equal 0, @@sv.user_parameter_change('Lot', lot, @@tkparams)
    # lot2: qgt, PO, ENGINEERING-U00 -> not started
    lot = lots[2]
    assert_equal 0, @@sv.lot_mfgorder_change(lot, customer: 'qgt')
    assert_equal 0, @@sv.user_parameter_change('Lot', lot, @@tkparams)
    # lot3: qgt, EC, SHANTI11MA-JA0  -> 'SHANTI1-A1FAAB.AA01', SHANTI11MA-JA0
    lot = lots[3]
    assert_equal 0, @@sv.sublottype_change(lot, 'EC')
    assert_equal 0, @@sv.lot_mfgorder_change(lot, customer: 'qgt')
    assert_equal 0, @@sv.user_parameter_change('Lot', lot, @@tkparams)
    #
    lots.each {|lot| assert_equal 0, @@sv.lot_bank_move(lot, @@endbanks.last)}
    #
    # TODO: automatic verification
    $log.info "\nLots #{lots} are ready.\nLots #{lots[1]}, #{lots[3]} are expected to be STBed.\nExecute the tests and press ENTER to clean up!"
    gets

  end

  def test12_qt

    assert lot = @@svtest.new_lot(nwafers: 2, sublottype: @@slt)
    @@testlots << lot
    assert_equal 0, @@sv.user_parameter_change('Lot', lot, {'TurnkeyType'=>'J' , 'TurnkeySubconIDBump'=>'GG'})
    assert_equal 0, @@sv.lot_opelocate(lot, @@fab_qtstart)
	    assert_equal 0, @@sv.lot_opelocate(lot, @@fab_qtstart1)

    assert_equal 0, @@sv.lot_gatepass(lot, mandatory: true)
    assert_equal 0, @@sv.lot_opelocate(lot, :last)
    refute_empty qtold = @@sv.lot_qtime_list(lot), 'lot has no QTime'
    liold = @@sv.lot_info(lot)
    assert_equal 0, @@sv.lot_bank_move(lot, @@bank)
    #
    # wait for STB
    #assert lot = @@svtest.new_lot(bysrclot: lot, product: "SHANTI1-A1FAAS.AA01", customer: 'qgt', erpitem: '*SHANTI1-A1FAAU-JAA01')
    ##comment = "ERPITEM=*SHANTI1-A1FAAU-JAA01,DPML=1.5,PLANNED_FSD=#{Time.now.strftime('%F')}"
    ##relparams = {lot: lot, nwafers: 2, lotGenerationType: 'By Source Lot', customer: 'qgt', product: "SHANTI1-A1FAAS.AA01", srclot: lot, comment: comment}
    #doesn't seems to work
    ##lot = @@sv.new_lot_release(relparams)
    #job is supposed todo @@svtest.new_lot(bysrclot: lot, product: "SHANTI1-A1FAAB.AA01", customer: "gf",erpitem: @@svtest.erpitem)
    assert wait_for(timeout: @@job_interval) {@@sv.lot_info(lot).product != liold.product}, 'no ReSTB'
    sleep 5
    assert note = @@sv.lot_notes(lot).first, 'no lot note'
    assert_equal 'STBMfgWIPAttributes', note.title, 'wrong note title(StbBySourceLot - Active Q-Time transfered)'
    refute_empty qt = @@sv.lot_qtime_list(lot), 'lot QTime has not been set'
    # qtdiff = (@@sv.siview_time(qt.qt_details.first.target_timestamp) - @@sv.siview_time(qtold.qt_details.first.target_timestamp))
    qtdiff = @@sv.siview_time(qt.first.strQtimeInfo.first.qrestrictionTargetTimeStamp) - @@sv.siview_time(qtold.first.strQtimeInfo.first.qrestrictionTargetTimeStamp)
    assert qtdiff.abs < 0.3, 'wrong QT'
    $log.info "\nLot #{lot} moved from Bump-Sort layer.\n with Active Q-Time transfered with qtime difference #{qtdiff}.\n"
  end

def test13_qt

    assert lot = @@svtest.new_lot(nwafers: 2, sublottype: @@slt)
    @@testlots << lot
    assert_equal 0, @@sv.user_parameter_change('Lot', lot, {'TurnkeyType'=>'J' , 'TurnkeySubconIDBump'=>'GG'})
    assert_equal 0, @@sv.lot_opelocate(lot, 1)
	    assert_equal 0, @@sv.lot_opelocate(lot, "99400.1100")

    assert_equal 0, @@sv.lot_gatepass(lot, mandatory: true)
    assert_equal 0, @@sv.lot_opelocate(lot, :last)
    refute_empty qtold = @@sv.lot_qtime_list(lot), 'lot has no QTime'
    liold = @@sv.lot_info(lot)
    assert_equal 0, @@sv.lot_bank_move(lot, @@bank)
    #
    # wait for STB
    #assert lot = @@svtest.new_lot(bysrclot: lot, product: "SHANTI1-A1FAAS.AA01", customer: 'qgt', erpitem: '*SHANTI1-A1FAAU-JAA01')
    ##comment = "ERPITEM=*SHANTI1-A1FAAU-JAA01,DPML=1.5,PLANNED_FSD=#{Time.now.strftime('%F')}"
    ##relparams = {lot: lot, nwafers: 2, lotGenerationType: 'By Source Lot', customer: 'qgt', product: "SHANTI1-A1FAAS.AA01", srclot: lot, comment: comment}
    #doesn't seems to work
    ##lot = @@sv.new_lot_release(relparams)
    #job is supposed todo @@svtest.new_lot(bysrclot: lot, product: "SHANTI1-A1FAAB.AA01", customer: "gf",erpitem: @@svtest.erpitem)
    assert wait_for(timeout: @@job_interval) {@@sv.lot_info(lot).product != liold.product}, 'no ReSTB'
    sleep 5
    assert note = @@sv.lot_notes(lot).first, 'no lot note'
    assert_equal 'STBMfgWIPAttributes', note.title, 'wrong note title(StbBySourceLot - Active Q-Time transfered)'
    refute_empty qt = @@sv.lot_qtime_list(lot), 'lot QTime has not been set'
    # qtdiff = (@@sv.siview_time(qt.qt_details.first.target_timestamp) - @@sv.siview_time(qtold.qt_details.first.target_timestamp))
    qtdiff = @@sv.siview_time(qt.first.strQtimeInfo.first.qrestrictionTargetTimeStamp) - @@sv.siview_time(qtold.first.strQtimeInfo.first.qrestrictionTargetTimeStamp)
    assert qtdiff.abs < 0.3, 'wrong QT'
    $log.info "\nLot #{lot} moved from Bump-Sort layer.\n with Active Q-Time transfered with qtime difference #{qtdiff}.\n"
  end
end
