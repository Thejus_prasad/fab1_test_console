=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author:   Conny Stenzel, 2011-03-09 and Steffen Steidten, 2012-03-12

Version:  19.01

History:
  2015-04-01 ssteidte, cleanup
  2017-06-01 sfrieske, minor fixes
  2019-01-22 sfrieske, minor cleanup
=end

require 'SiViewTestCase'


# test as non-x-user: runtest 'JCAP_DetectCassetteReservation', 'itdc_nox'
class JCAP_DetectCassetteReservation < SiViewTestCase
  @@sv_defaults = {carriers: 'JC%'}  # TODO: ??
  @@sublottype = 'RD'   # for scapped lots to be left alone by AutoScrapHandler
  @@sorter1 = 'UTS001'
  @@sorter2 = 'UTS002'
  @@eqp = 'UTC001'
  @@sorter_operation = '1000.400'
  @@bankin_operation = '3000.100'
  @@nonprobank = 'S-NONPROD'
  @@hold_reason = 'ENG'

  @@slack = 90
  @@job_interval = 180        #timer.interval.min=*/3  # 3 min, original: */15 min
  @@jcap_nox_interval = 600   #time.reservation.general.min=30  # 10
  @@jcap_x_interval = 1800    #time.reservation.x-user.min=90   # 30

  @@testlots = []
  @@testcarriers = []

  def self.after_all_passed
    [@@sorter1, @@sorter2, @@eqp].each {|eqp| @@sv.eqp_cleanup(eqp)}
    @@testcarriers.each {|c| @@sv.carrier_reserve_cancel(c)}
    @@testlots.each {|lot| @@sv.delete_lot_family(lot)}
  end


  def test00_setup
    $setup_ok = false
    #
    [@@sorter1, @@sorter2, @@eqp].each {|eqp| assert @@svtest.cleanup_eqp(eqp)}
    #
    assert @@testlots = @@svtest.new_lots(11, sublottype: @@sublottype), 'error creating test lots'
    @@testcarriers += @@sv.lots_info(@@testlots).collect {|li| li.carrier}
    #
    # carriers
    assert ecs = @@svtest.get_empty_carrier(n: 2), 'not enough empty carriers'
    @@testcarriers += ecs
    @@empty_carrier1, @@empty_carrier2 = ecs
    assert_equal 0, @@sv.carrier_reserve_cancel(ecs, check: true)
    #
    # sleep to let jCAP pick up any canceled reservation
    t = @@job_interval + @@slack
    $log.info "waiting #{t} s for the jCAP job to pick up carrier status changes"; sleep t
    #
    # prepare test cases
    #
    # test 0, empty_carrier
    assert_equal 0, @@sv.carrier_reserve(@@empty_carrier1), 'error reserving carrier'
    #
    # test 1, onelot_lotwaiting
    lot = @@testlots[0]
    li = @@sv.lot_info(lot)
    assert_equal 0, @@sv.carrier_reserve(li.carrier), 'error reserving carrier'
    #
    # test 2, onelot_waiting_sorternpw
    lot = @@testlots[1]
    assert_equal 0, @@sv.lot_opelocate(lot, @@sorter_operation)
    @@sv.eqp_mode_change(@@sorter1, 'Off-Line-1')
    li = @@sv.lot_info(lot)
    assert_equal 0, @@sv.carrier_reserve(li.carrier), 'error reserving carrier'
    assert_equal 0, @@sv.npw_reserve(@@sorter1, nil, li.carrier)
    #
    # test 3, onelot_lothold, verify that no extra hold was set
    lot = @@testlots[2]
    assert_equal 0, @@sv.lot_hold(lot, @@hold_reason)
    li = @@sv.lot_info(lot)
    assert_equal 'ONHOLD', li.status, "Lot #{lot} process state is '#{li.status}' instead of ONHOLD"
    assert_equal 0, @@sv.carrier_reserve(li.carrier), 'error reserving carrier'
    #
    # test 4, onelot_lotbank, verify that the lot is set on bankhold
    lot = @@testlots[3]
    assert_equal 0, @@sv.lot_opelocate(lot, :last)
    li = @@sv.lot_info(lot)
    refute_empty li.bank, 'lot is not banked in'
    assert_equal 0, @@sv.carrier_reserve(li.carrier), 'error reserving carrier'
    #
    # test 5, onelot_lotbankhold, verify that no extra hold was set
    lot = @@testlots[4]
    assert_equal 0, @@sv.lot_opelocate(lot, :last)
    li = @@sv.lot_info(lot)
    refute_empty li.bank, 'lot is not banked in'
    assert_equal 0, @@sv.carrier_reserve(li.carrier), 'error reserving carrier'
    assert_equal 0, @@sv.lot_bankhold(lot)
    li = @@sv.lot_info(lot)
    assert_equal 'ONHOLD', li.status, 'wrong lot status'
    #
    # test 6, onelot_lotnonprobank, verify that no extra hold was set
    lot = @@testlots[5]
    li = @@sv.lot_info(lot)
    assert_equal 0, @@sv.lot_nonprobankin(lot, @@nonprobank)
    assert_equal 0, @@sv.carrier_reserve(li.carrier), 'error reserving carrier'
    li = @@sv.lot_info(lot)
    assert_equal 'NonProBank', li.status, 'wrong lot status'
    #
    # test 7, onelot_lotscrapped
    lot = @@testlots[6]
    li = @@sv.lot_info(lot)
    assert_equal 0, @@sv.scrap_wafers(lot)
    assert_equal 0, @@sv.carrier_reserve(li.carrier), 'error reserving carrier'
    li = @@sv.lot_info(lot)
    assert_equal 'SCRAPPED', li.status, 'wrong lot status'
    #
    # test 8, twolots_onelothold_onelotwaiting
    lot = @@testlots[7]
    clot = @@sv.lot_split(lot, 5)
    li = @@sv.lot_info(lot)
    assert_equal 0, @@sv.carrier_reserve(li.carrier), 'error reserving carrier'
    assert_equal 0, @@sv.lot_hold(clot, @@hold_reason)
    li = @@sv.lot_info(clot)
    assert_equal 'ONHOLD', li.status, 'wrong lot status'
    #
    # test 9, threelots_onelotscrapped_onelothold_onelotwaiting
    lot = @@testlots[8]
    clot1 = @@sv.lot_split(lot, 5)
    clot2 = @@sv.lot_split(lot, 5)
    assert_equal 0, @@sv.lot_hold(clot1, @@hold_reason)
    assert_equal 0, @@sv.scrap_wafers(clot2)
    li = @@sv.lot_info(lot)
    assert_equal 0, @@sv.carrier_reserve(li.carrier), 'error reserving carrier'
    #
    # test 10, onelot_lotwaiting, sj exists
    lot = @@testlots[9]
    li = @@sv.lot_info(lot)
    assert_equal 0, @@sv.carrier_reserve(li.carrier), 'error reserving carrier'
    assert_equal 0, @@sv.carrier_reserve(@@empty_carrier2), 'error reserving carrier'
    assert_equal 0, @@sv.eqp_mode_change(@@sorter2, 'Auto-1'),  "#{@@sorter2} could not be switched to Auto-1"
    assert @@sv.sj_create(@@sorter2, 'P1', 'P2', lot, @@empty_carrier2), 'error creating sj'
    #
    # test 11, onelot_lotwaiting, cj exists
    lot = @@testlots[10]
    li = @@sv.lot_info(lot)
    assert_equal 0, @@sv.carrier_xfer_status_change(li.carrier, 'MI', @@svtest.stocker)
    assert_equal 0, @@sv.carrier_reserve(li.carrier), 'error reserving carrier'
    @@sv.eqp_mode_change(@@eqp, 'Off-Line-1', notify: false)
    assert @@sv.slr(@@eqp, lot: lot), 'slr error'
    @@sv.eqp_status_change(@@eqp, '2NDP')
    #
    # wait for the jCAP job and verify xfer users are still set
    t = @@job_interval + @@slack
    $log.info "waiting #{t} s for the jCAP job"; sleep t
    (@@testlots.collect {|l| @@sv.lot_info(l).carrier} + [@@empty_carrier1, @@empty_carrier2]).each {|c|
      refute_empty @@sv.carrier_status(c).xfer_user, "missing transfer user for #{c}"
    }
    #
    $setup_ok = true
  end

  # X users only: sleep jcap_nox_interval
  def test099_jcap_nox_interval_sleep
    return if @@sv.user == 'NOX-UNITTEST'
    # verify that the jCAP job recognizes the x-user and does not use the non x-interval
    t = @@jcap_nox_interval + @@job_interval + @@slack
    $log.info "waiting #{t} s for non-X user timeout"; sleep t
    $log.info 'first job interval done'
  end

  # X user only: verify setup is still correct

  def test100_empty_carrier
    return if @@sv.user == 'NOX-UNITTEST'
    # test 0, empty_carrier, verify that the reservation is canceled
    refute_empty @@sv.carrier_status(@@empty_carrier1).xfer_user, 'missing transfer user'
  end

  def test101_onelot_lotwaiting
    return if @@sv.user == 'NOX-UNITTEST'
    # test 1, onelot_lotwaiting, verify that the lot is set on hold
    lot = @@testlots[0]
    li = @@sv.lot_info(lot)
    assert_empty @@sv.lot_hold_list(lot, reason: 'X-M1'), "wrong X-M1 hold for #{lot}"
    refute_empty @@sv.carrier_status(li.carrier).xfer_user, 'missing transfer user'
  end

  def test102_onelot_lotwaiting_sorternpw
    return if @@sv.user == 'NOX-UNITTEST'
    # test 2, onelot_lotwaiting_sorternpw, verify that the reservation is not canceled
    lot = @@testlots[1]
    li = @@sv.lot_info(lot)
    assert_empty @@sv.lot_hold_list(lot, reason: 'X-M1'), "wrong X-M1 hold for #{lot}"
    refute_empty @@sv.carrier_status(li.carrier).xfer_user, 'missing transfer user'
  end

  def test103_onelot_lothold
    return if @@sv.user == 'NOX-UNITTEST'
    # test 3, onelot_lothold, verify that no extra hold was set
    lot = @@testlots[2]
    li = @@sv.lot_info(lot)
    assert_empty @@sv.lot_hold_list(lot, reason: 'X-M1'), "wrong X-M1 hold for #{lot}"
    refute_empty @@sv.carrier_status(li.carrier).xfer_user, 'missing transfer user'
  end

  def test104_onelot_lotbank
    return if @@sv.user == 'NOX-UNITTEST'
    # test 4, onelot_lotbank, verify that the lot is set on bankhold
    lot = @@testlots[3]
    li = @@sv.lot_info(lot)
    assert_empty @@sv.lot_hold_list(lot), "wrong lot hold for #{lot}"
    refute_empty @@sv.carrier_status(li.carrier).xfer_user, 'missing transfer user'
  end

  def test105_onelot_lotbankhold
    return if @@sv.user == 'NOX-UNITTEST'
    # test 5, onelot_lotbankhold, verify that no extra hold was set
    lot = @@testlots[4]
    li = @@sv.lot_info(lot)
    assert_empty @@sv.lot_hold_list(lot, reason: 'X-M1'), "wrong X-M1 hold for #{lot}"
    refute_empty @@sv.carrier_status(li.carrier).xfer_user, 'missing transfer user'
  end

  def test106_onelot_lotnonprobank
    return if @@sv.user == 'NOX-UNITTEST'
    # test 6, onelot_lotnonprobank, verify that no extra hold was set
    lot = @@testlots[5]
    li = @@sv.lot_info(lot)
    assert_empty @@sv.lot_hold_list(lot, reason: 'X-M1'), "wrong X-M1 hold for #{lot}"
    refute_empty @@sv.carrier_status(li.carrier).xfer_user, 'missing transfer user'
  end

  def test107_onelot_lotscrapped
    return if @@sv.user == 'NOX-UNITTEST'
    # test 7, onelot_lotscrapped, verify that no hold is set
    lot = @@testlots[6]
    li = @@sv.lot_info(lot)
    assert_empty @@sv.lot_hold_list(lot), "wrong lot hold for #{lot}"
    refute_empty @@sv.carrier_status(li.carrier).xfer_user, 'missing transfer user'
  end

  def test108_twolots_onelothold_onelotwaiting
    return if @@sv.user == 'NOX-UNITTEST'
    # test 8, twolots_onelothold_onelotwaiting, verify that no extra hold is set for the ONHOLD lot and that the waiting lot is set onhold
    lot = @@testlots[7]
    li = @@sv.lot_info(lot)
    lots = @@sv.carrier_status(li.carrier).lots
    assert_equal 2, lots.length, "Carrier #{li.carrier} contains #{lots.length} lots instead of 2 lots"
    clot = lots[1]
    assert_empty @@sv.lot_hold_list(lot, reason: 'X-M1'), "wrong X-M1 hold for #{lot}"
    assert_empty @@sv.lot_hold_list(clot, reason: 'X-M1'), "wrong X-M1 hold for #{clot}"
    refute_empty @@sv.carrier_status(li.carrier).xfer_user, 'missing transfer user'
  end

  def test109_threelots_onelotscrapped_onelothold_onelotwaiting
    return if @@sv.user == 'NOX-UNITTEST'
    # test 9, threelots_onelotscrapped_onelothold_onelotwaiting, verify that no extra hold is set for the ONHOLD lot, no hold is set for the scrapped lot and that the waiting lot is set onhold
    lot = @@testlots[8]
    li = @@sv.lot_info(lot)
    lots = @@sv.carrier_status(li.carrier).lots
    assert_equal 3, lots.length, "Carrier #{li.carrier} contains #{lots.length} lots instead of 3 lots"
    clot1 = lots[1]
    clot2 = lots[2]
    assert_empty @@sv.lot_hold_list(lot, reason: 'X-M1'), "wrong X-M1 hold for #{lot}"
    assert_empty @@sv.lot_hold_list(clot1, reason: 'X-M1'), "wrong X-M1 hold for #{clot1}"
    assert_empty @@sv.lot_hold_list(clot2), "wrong lot hold for #{clot2}"
    refute_empty @@sv.carrier_status(li.carrier).xfer_user, 'missing transfer user'
  end

  def test110_onelot_lotwaiting_sj_exists
    return if @@sv.user == 'NOX-UNITTEST'
    # test 10, onelot_lotwaiting, sj exists
    lot = @@testlots[9]
    li = @@sv.lot_info(lot)
    assert_empty @@sv.lot_hold_list(lot), "wrong lot hold for #{lot}"
    refute_empty @@sv.carrier_status(li.carrier).xfer_user, 'missing transfer user'
  end

  def test111_onelot_lotwaiting_cj_exists
    return unless @@sv.user.start_with?('X')
    return if @@sv.user == 'NOX-UNITTEST'
    # test 11, onelot_lotwaiting, cj exists
    lot = @@testlots[10]
    li = @@sv.lot_info(lot)
    assert_empty @@sv.lot_hold_list(lot), "wrong lot hold for #{lot}"
    refute_empty @@sv.carrier_status(li.carrier).xfer_user, 'missing transfer user'
  end

  # sleep the remaining time
  def test199_jcap_x_interval_sleep
    t = @@sv.user.start_with?('X') ? @@jcap_x_interval - @@jcap_nox_interval : @@jcap_nox_interval
    t += @@job_interval + @@slack
    $log.info "waiting #{t} s for timeout"; sleep t
  end

  # verify expectations for X and non-X users

  def test200_empty_carrier
    # test 0, empty_carrier, verify that the reservation is canceled
    assert_empty @@sv.carrier_status(@@empty_carrier1).xfer_user, "reservation of #{@@empty_carrier1} not canceled"
  end

  def test201_onelot_lotwaiting
    # test 1, onelot_lotwaiting, verify that the lot is set on hold
    lot = @@testlots[0]
    li = @@sv.lot_info(lot)
    refute_empty @@sv.lot_hold_list(lot, reason: 'X-M1'), "missing X-M1 hold for #{lot}"
    refute_empty @@sv.carrier_status(li.carrier).xfer_user, 'missing transfer user'
  end

  def test202_onelot_lotwaiting_sorternpw
    # test 2, onelot_lotwaiting_sorternpw, verify that the reservation is not canceled
    lot = @@testlots[1]
    li = @@sv.lot_info(lot)
    refute_empty @@sv.lot_hold_list(lot, reason: 'X-M1'), "missing X-M1 hold for #{lot}"
    refute_empty @@sv.carrier_status(li.carrier).xfer_user, 'missing transfer user'
  end

  def test203_onelot_lothold
    # test 3, onelot_lothold, verify that no extra hold was set
    lot = @@testlots[2]
    li = @@sv.lot_info(lot)
    assert_equal 1, @@sv.lot_hold_list(lot).length, "An extra hold was set for lot #{lot} unexpectedly"
    refute_empty @@sv.carrier_status(li.carrier).xfer_user, 'missing transfer user'
  end

  def test204_onelot_lotbank
    # test 4, onelot_lotbank, verify that the lot is set on bankhold
    lot = @@testlots[3]
    li = @@sv.lot_info(lot)
    assert_equal 1, @@sv.lot_hold_list(lot, type: 'BankHold').length, "nissing BankHold of lot #{lot}"
    refute_empty @@sv.carrier_status(li.carrier).xfer_user, 'missing transfer user'
  end

  def test205_onelot_lotbankhold
    # test 5, onelot_lotbankhold, verify that no extra hold was set
    lot = @@testlots[4]
    li = @@sv.lot_info(lot)
    assert_equal 1, @@sv.lot_hold_list(lot).length, "An extra hold was set for lot #{lot} unexpectedly"
    refute_empty @@sv.carrier_status(li.carrier).xfer_user, 'missing transfer user'
  end

  def test206_onelot_lotnonprobank
    # test 6, onelot_lotnonprobank, verify that no extra hold was set
    lot = @@testlots[5]
    li = @@sv.lot_info(lot)
    assert_equal 1, @@sv.lot_hold_list(lot).length, "An extra hold was set for lot #{lot} unexpectedly"
    refute_empty @@sv.carrier_status(li.carrier).xfer_user, 'missing transfer user'
  end

  def test207_onelot_lotscrapped
    # test 7, onelot_lotscrapped, verify that no hold is set but the carrier has to be put into the job cache
    lot = @@testlots[6]
    li = @@sv.lot_info(lot)
    assert_empty @@sv.lot_hold_list(lot), "wrong lot hold for #{lot}"
    refute_empty @@sv.carrier_status(li.carrier).xfer_user, 'missing transfer user'
  end

  def test208_twolots_onelothold_onelotwaiting
    # test 8, twolots_onelothold_onelotwaiting, verify that no extra hold is set for the ONHOLD lot and that the waiting lot is set onhold
    lot = @@testlots[7]
    li = @@sv.lot_info(lot)
    lots = @@sv.carrier_status(li.carrier).lots
    assert_equal 2, lots.length, "Carrier #{li.carrier} contains #{lots.length} lots instead of 2 lots"
    clot = lots[1]
    refute_empty @@sv.lot_hold_list(lot, reason: 'X-M1'), "missing X-M1 hold"
    assert_equal 1, @@sv.lot_hold_list(clot).length, "An extra hold was set for lot #{clot} unexpectedly"
    refute_empty @@sv.carrier_status(li.carrier).xfer_user, 'missing transfer user'
  end

  def test209_threelots_onelotscrapped_onelothold_onelotwaiting
    # test 9, threelots_onelotscrapped_onelothold_onelotwaiting, verify that no extra hold is set for the ONHOLD lot, no hold is set for the scrapped lot and that the waiting lot is set onhold
    lot = @@testlots[8]
    li = @@sv.lot_info(lot)
    lots = @@sv.carrier_status(li.carrier).lots
    assert_equal 3, lots.length, "Carrier #{li.carrier} contains #{lots.length} lots instead of 3 lots"
    clot1 = lots[1]
    clot2 = lots[2]
    refute_empty @@sv.lot_hold_list(lot, reason: 'X-M1'), "missing X-M1 hold"
    assert_equal 1, @@sv.lot_hold_list(clot1).length, "An extra hold was set for lot #{clot1} unexpectedly"
    assert_empty @@sv.lot_hold_list(clot2), "wrong lot hold for #{clot2}"
    refute_empty @@sv.carrier_status(li.carrier).xfer_user, 'missing transfer user'
  end

  def test210_onelot_lotwaiting_sj_exists
    # test 10, onelot_lotwaiting, sj exists
    lot = @@testlots[9]
    li = @@sv.lot_info(lot)
    assert_empty @@sv.lot_hold_list(lot), "wrong lot hold for #{lot}"
    refute_empty @@sv.carrier_status(li.carrier).xfer_user, 'missing transfer user'
  end

  def test211_onelot_lotwaiting_cj_exists
    # test 11, onelot_lotwaiting, cj exists
    lot = @@testlots[10]
    li = @@sv.lot_info(lot)
    assert_empty @@sv.lot_hold_list(lot), "wrong lot hold for #{lot}"
    refute_empty @@sv.carrier_status(li.carrier).xfer_user, 'missing transfer user'
  end

end
