=begin
QT test NG

load_run_testcase "Test_jCAP_QT", "itdc"

(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: Thomas Klose 2016-04-21

=end

require "RubyTestCase"
require "siviewtest"
require "thread"

class Test_QTNG < RubyTestCase
  Description = "Test jCAP Q-Time job"

  include MonitorMixin

  @@tc200 = true
  @@tc201 = true
  @@tc202 = true
  @@tc203 = true
  @@tc204 = true
  @@tc205 = true
  @@tc206 = true  # Retrigger
  @@tc207 = true
  @@tc208 = true
  @@tc209 = true
  @@tc210 = true
  @@tc211 = true
  @@tc212 = true
  @@tc213 = true
  @@tc214 = true
  @@tc215 = true
  @@tc216 = true
  @@tc217 = true
  @@tc218 = true
  @@tc219 = true
  @@tc220 = true
  @@tc221 = true
  @@tc222 = true  # RW cancel
  @@tc223 = true
  @@tc224 = true
  @@tc225 = true

  def test010_prepare
    $svtest = SiView::Test.new($env)
    @@result = {}
    @@threads = []

    mutex = Mutex.new

    $log.info "STARTING THREADS"
    @@threads << Thread.new { @@result["tc200"] = tc200 } if @@tc200
    @@threads << Thread.new { @@result["tc201"] = tc201 } if @@tc201
    @@threads << Thread.new { @@result["tc202"] = tc202 } if @@tc202
    @@threads << Thread.new { @@result["tc203"] = tc203 } if @@tc203
    @@threads << Thread.new { @@result["tc204"] = tc204 } if @@tc204
    @@threads << Thread.new { @@result["tc205"] = tc205 } if @@tc205
    @@threads << Thread.new { @@result["tc206"] = tc206 } if @@tc206
    @@threads << Thread.new { @@result["tc207"] = tc207 } if @@tc207
    @@threads << Thread.new { @@result["tc208"] = tc208 } if @@tc208
    @@threads << Thread.new { @@result["tc209"] = tc209 } if @@tc209
    @@threads << Thread.new { @@result["tc210"] = tc210 } if @@tc210
    @@threads << Thread.new { @@result["tc211"] = tc211 } if @@tc211
    @@threads << Thread.new { @@result["tc212"] = tc212 } if @@tc212
    @@threads << Thread.new { @@result["tc213"] = tc213 } if @@tc213
    @@threads << Thread.new { @@result["tc214"] = tc214 } if @@tc214
    @@threads << Thread.new { @@result["tc215"] = tc215 } if @@tc215
    @@threads << Thread.new { @@result["tc216"] = tc216 } if @@tc216
    @@threads << Thread.new { @@result["tc217"] = tc217 } if @@tc217
    @@threads << Thread.new { @@result["tc218"] = tc218 } if @@tc218
    @@threads << Thread.new { @@result["tc219"] = tc219 } if @@tc219
    @@threads << Thread.new { @@result["tc220"] = tc220 } if @@tc220
    @@threads << Thread.new { @@result["tc221"] = tc221 } if @@tc221
    @@threads << Thread.new { @@result["tc222"] = tc222 } if @@tc222
    @@threads << Thread.new { @@result["tc223"] = tc223 } if @@tc223
    @@threads << Thread.new { @@result["tc224"] = tc224 } if @@tc224
    @@threads << Thread.new { @@result["tc225"] = tc225 } if @@tc225

    @@threads.each {|t| t.join}
    sleep 10   # for qt ctrl
    $log.info "*"*60
    @@result.keys.sort.each {|k|
      $log.info("#{k}: #{@@result[k].join(", ")}")
      }
  end

  # template for testcases
  def tcXXX
    res = []
    sv = SiView::MM.new($env)
    l = new_lot(sv, "testroute")
    return "no lot" if not l

    # do the work
    qt.lot_opelocate(l, "1000.1200")

    # wait here
    sleep 600

    # check holds
    res << checkholdqtctrl(sv, l)
    res << checkholdwd(sv, l)

    # return the result
    return res
  end

  # test section starts here
  def tc200
    res = []
    sv = SiView::MM.new($env)
    l = new_lot(sv, "A-QTNG-TC200.01")
    return "no lot" if not l
    assert sv.lot_opelocate(l, "3.20"), "#{__method__.to_s}"
    assert sv.claim_process_lot(l, :freeport=>true), "#{__method__.to_s}"
    assert sv.lot_opelocate(l, "3.32"), "#{__method__.to_s}"
    sleep 60
    assert sv.lot_rework(l, "A-QTNG-RWTC200.01", return_opNo: "2.10"), "#{__method__.to_s}"
    assert sv.claim_process_lot(l, :freeport=>true), "#{__method__.to_s}"
    assert sv.lot_opelocate(l, "2.13"), "#{__method__.to_s}"
    assert sv.claim_process_lot(l, :freeport=>true), "#{__method__.to_s}"

    sleep 600

    res << l
    res << checkholdctrl(sv, l)
    res << checkholdwd(sv, l)
    return res
  end

  def tc201
    res = []
    sv = SiView::MM.new($env)
    l = new_lot(sv, "A-QTNG-TC200.01")
    return "no lot" if not l
    assert sv.lot_opelocate(l, "3.20"), "#{__method__.to_s}"
    assert sv.claim_process_lot(l, :freeport=>true), "#{__method__.to_s}"
    assert sv.lot_opelocate(l, "3.32"), "#{__method__.to_s}"
    sleep 60
    assert sv.lot_rework(l, "A-QTNG-RWTC200.01", return_opNo: "2.10"), "#{__method__.to_s}"
    assert sv.claim_process_lot(l, :freeport=>true), "#{__method__.to_s}"

    sleep 600
    res << l
    res << checkholdctrl(sv, l)
    res << checkholdwd(sv, l)
    return res
  end

  def tc202
    res = []
    sv = SiView::MM.new($env)
    l = new_lot(sv, "A-QTNG-TC202.01")
    return "no lot" if not l
    assert sv.lot_opelocate(l, "3.20"), "#{__method__.to_s}"
    assert sv.claim_process_lot(l, :freeport=>true), "#{__method__.to_s}"
    assert sv.lot_opelocate(l, "3.32"), "#{__method__.to_s}"
    sleep 60
    assert sv.lot_rework(l, "A-QTNG-RWTC200.01", return_opNo: "4.30"), "#{__method__.to_s}"
    assert sv.claim_process_lot(l, :freeport=>true), "#{__method__.to_s}"
    assert sv.lot_opelocate(l, "4.33"), "#{__method__.to_s}"
    assert sv.claim_process_lot(l, :freeport=>true), "#{__method__.to_s}"

    sleep 600

    res << l
    res << checkholdctrl(sv, l)
    res << checkholdwd(sv, l)
    return res
  end

  def tc203
    res = []
    sv = SiView::MM.new($env)
    l = new_lot(sv, "A-QTNG-TC202.01")
    return "no lot" if not l
    assert sv.lot_opelocate(l, "3.20"), "#{__method__.to_s}"
    assert sv.claim_process_lot(l, :freeport=>true), "#{__method__.to_s}"
    assert sv.lot_opelocate(l, "3.32"), "#{__method__.to_s}"
    sleep 60
    assert sv.lot_rework(l, "A-QTNG-RWTC200.01", return_opNo: "4.30"), "#{__method__.to_s}"
    assert sv.claim_process_lot(l, :freeport=>true), "#{__method__.to_s}"

    sleep 600

    res << l
    res << checkholdctrl(sv, l)
    res << checkholdwd(sv, l)
    return res
  end

  def tc204
    res = []
    sv = SiView::MM.new($env)
    l = new_lot(sv, "A-QTNG-TC204.01")
    return "no lot" if not l
    assert sv.lot_opelocate(l, "3.20"), "#{__method__.to_s}"
    assert sv.claim_process_lot(l, :freeport=>true), "#{__method__.to_s}"
    assert sv.lot_opelocate(l, "3.31"), "#{__method__.to_s}"
    sleep 60
    assert sv.lot_rework(l, "A-QTNG-RWTC200.01", return_opNo: "3.32"), "#{__method__.to_s}"
    assert sv.claim_process_lot(l, :freeport=>true), "#{__method__.to_s}"
    sleep 60
    assert sv.lot_opelocate(l, "2.13"), "#{__method__.to_s}"
    assert sv.claim_process_lot(l, :freeport=>true), "#{__method__.to_s}"
    sleep 600

    res << l
    res << checkholdctrl(sv, l)
    res << checkholdwd(sv, l)
    return res
  end

  def tc205
    res = []
    sv = SiView::MM.new($env)
    l = new_lot(sv, "A-QTNG-TC204.01")
    return "no lot" if not l
    assert sv.lot_opelocate(l, "3.20"), "#{__method__.to_s}"
    assert sv.claim_process_lot(l, :freeport=>true), "#{__method__.to_s}"
    assert sv.lot_opelocate(l, "3.31"), "#{__method__.to_s}"
    sleep 60
    assert sv.lot_rework(l, "A-QTNG-RWTC200.01", return_opNo: "3.32"), "#{__method__.to_s}"
    assert sv.claim_process_lot(l, :freeport=>true), "#{__method__.to_s}"
    sleep 600

    res << l
    res << checkholdctrl(sv, l)
    res << checkholdwd(sv, l)
    return res
  end

  def tc206
    res = []
    sv = SiView::MM.new($env)
    l = new_lot(sv, "A-QTNG-TC206.01")
    return "no lot" if not l
    assert sv.lot_opelocate(l, "3.20"), "#{__method__.to_s}"
    assert sv.claim_process_lot(l, :freeport=>true), "#{__method__.to_s}"
    assert sv.lot_opelocate(l, "3.31"), "#{__method__.to_s}"
    sleep 60
    assert sv.lot_rework(l, "A-QTNG-RWTC206.01", return_opNo: "3.32"), "#{__method__.to_s}"
    assert sv.claim_process_lot(l, :freeport=>true), "#{__method__.to_s}"
    qt = last_ts(sv, l)
    sleep 60
    assert sv.lot_opelocate(l, "2.13"), "#{__method__.to_s}"
    assert sv.claim_process_lot(l, :freeport=>true), "#{__method__.to_s}"
    sleep 600

    res << l
    res << checkholdctrl(sv, l, qt)
    res << checkholdwd(sv, l, qt)
    return res
  end

  def tc207
    res = []
    sv = SiView::MM.new($env)
    l = new_lot(sv, "A-QTNG-TC206.01")
    return "no lot" if not l
    assert sv.lot_opelocate(l, "3.20"), "#{__method__.to_s}"
    assert sv.claim_process_lot(l, :freeport=>true), "#{__method__.to_s}"
    assert sv.lot_opelocate(l, "3.31"), "#{__method__.to_s}"
    sleep 60
    assert sv.lot_rework(l, "A-QTNG-RWTC206.01", return_opNo: "3.32"), "#{__method__.to_s}"
    assert sv.claim_process_lot(l, :freeport=>true), "#{__method__.to_s}"
    qt = last_ts(sv, l)
    sleep 660

    res << l
    res << checkholdctrl(sv, l, qt)
    res << checkholdwd(sv, l, qt)
    return res
  end

  def tc208
    res = []
    sv = SiView::MM.new($env)
    l = new_lot(sv, "A-QTNG-TC208.01")
    return "no lot" if not l
    assert sv.lot_opelocate(l, "3.20"), "#{__method__.to_s}"
    assert sv.lot_rework(l, "A-QTNG-RWTC200.01", return_opNo: "3.30"), "#{__method__.to_s}"
    assert sv.claim_process_lot(l, :freeport=>true), "#{__method__.to_s}"
    qt = last_ts(sv, l)
    sleep 660

    res << l
    res << checkholdctrl(sv, l, qt)
    res << checkholdwd(sv, l, qt)
    return res
  end

  def tc209
    res = []
    sv = SiView::MM.new($env)
    l = new_lot(sv, "A-QTNG-TC208.01")
    return "no lot" if not l
    assert sv.lot_opelocate(l, "3.20"), "#{__method__.to_s}"
    assert sv.lot_rework(l, "A-QTNG-RWTC200.01", return_opNo: "3.30"), "#{__method__.to_s}"
    assert sv.claim_process_lot(l, :freeport=>true), "#{__method__.to_s}"
    qt = last_ts(sv, l)
    sleep 60
    assert sv.lot_opelocate(l, "2.13"), "#{__method__.to_s}"
    assert sv.claim_process_lot(l, :freeport=>true), "#{__method__.to_s}"
    sleep 600

    res << l
    res << checkholdctrl(sv, l, qt)
    res << checkholdwd(sv, l, qt)
    return res
  end

  def tc210
    res = []
    sv = SiView::MM.new($env)
    l = new_lot(sv, "A-QTNG-TC208.01")
    return "no lot" if not l
    assert sv.lot_opelocate(l, "4.30"), "#{__method__.to_s}"
    assert sv.claim_process_lot(l, :freeport=>true), "#{__method__.to_s}"
    assert sv.lot_rework(l, "A-QTNG-RWTC200.01", return_opNo: "3.30"), "#{__method__.to_s}"
    assert sv.claim_process_lot(l, :freeport=>true), "#{__method__.to_s}"
    qt = last_ts(sv, l)
    sleep 60
    assert sv.lot_opelocate(l, "2.13"), "#{__method__.to_s}"
    assert sv.claim_process_lot(l, :freeport=>true), "#{__method__.to_s}"
    sleep 600

    res << l
    res << checkholdctrl(sv, l, qt)
    res << checkholdwd(sv, l, qt)
    return res
  end

  def tc211
    res = []
    sv = SiView::MM.new($env)
    l = new_lot(sv, "A-QTNG-TC208.01")
    return "no lot" if not l
    assert sv.lot_opelocate(l, "4.30"), "#{__method__.to_s}"
    assert sv.claim_process_lot(l, :freeport=>true), "#{__method__.to_s}"
    assert sv.lot_rework(l, "A-QTNG-RWTC200.01", return_opNo: "3.30"), "#{__method__.to_s}"
    assert sv.claim_process_lot(l, :freeport=>true), "#{__method__.to_s}"
    qt = last_ts(sv, l)
    sleep 660

    res << l
    res << checkholdctrl(sv, l, qt)
    res << checkholdwd(sv, l, qt)
    return res
  end

  def tc212
    res = []
    sv = SiView::MM.new($env)
    l = new_lot(sv, "A-QTNG-TC208.01")
    return "no lot" if not l
    assert sv.lot_opelocate(l, "3.20"), "#{__method__.to_s}"
    assert sv.lot_rework(l, "A-QTNG-RWTC206.01", return_opNo: "3.30"), "#{__method__.to_s}"
    assert sv.claim_process_lot(l, :freeport=>true), "#{__method__.to_s}"
    qt = last_ts(sv, l)
    sleep 660

    res << l
    res << checkholdctrl(sv, l, qt)
    res << checkholdwd(sv, l, qt)
    return res
  end

  def tc213
    res = []
    sv = SiView::MM.new($env)
    l = new_lot(sv, "A-QTNG-TC208.01")
    return "no lot" if not l
    assert sv.lot_opelocate(l, "3.20"), "#{__method__.to_s}"
    assert sv.lot_rework(l, "A-QTNG-RWTC206.01", return_opNo: "3.30"), "#{__method__.to_s}"
    assert sv.claim_process_lot(l, :freeport=>true), "#{__method__.to_s}"
    qt = last_ts(sv, l)
    sleep 60
    assert sv.lot_opelocate(l, "2.13"), "#{__method__.to_s}"
    assert sv.claim_process_lot(l, :freeport=>true), "#{__method__.to_s}"
    sleep 600

    res << l
    res << checkholdctrl(sv, l, qt)
    res << checkholdwd(sv, l, qt)
    return res
  end

  def tc214
    res = []
    sv = SiView::MM.new($env)
    l = new_lot(sv, "A-QTNG-TC208.01")
    return "no lot" if not l
    assert sv.lot_opelocate(l, "4.30"), "#{__method__.to_s}"
    assert sv.claim_process_lot(l, :freeport=>true), "#{__method__.to_s}"
    assert sv.lot_rework(l, "A-QTNG-RWTC206.01", return_opNo: "3.30"), "#{__method__.to_s}"
    assert sv.claim_process_lot(l, :freeport=>true), "#{__method__.to_s}"
    qt = last_ts(sv, l)
    sleep 60
    assert sv.lot_opelocate(l, "2.13"), "#{__method__.to_s}"
    assert sv.claim_process_lot(l, :freeport=>true), "#{__method__.to_s}"
    sleep 600

    res << l
    res << checkholdctrl(sv, l, qt)
    res << checkholdwd(sv, l, qt)
    return res
  end

  def tc215
    res = []
    sv = SiView::MM.new($env)
    l = new_lot(sv, "A-QTNG-TC208.01")
    return "no lot" if not l
    assert sv.lot_opelocate(l, "4.30"), "#{__method__.to_s}"
    assert sv.claim_process_lot(l, :freeport=>true), "#{__method__.to_s}"
    assert sv.lot_rework(l, "A-QTNG-RWTC206.01", return_opNo: "3.30"), "#{__method__.to_s}"
    assert sv.claim_process_lot(l, :freeport=>true), "#{__method__.to_s}"
    qt = last_ts(sv, l)
    sleep 660

    res << l
    res << checkholdctrl(sv, l, qt)
    res << checkholdwd(sv, l, qt)
    return res
  end

  def tc216
    res = []
    sv = SiView::MM.new($env)
    l = new_lot(sv, "A-QTNG-TC216.01")
    return "no lot" if not l
    assert sv.lot_opelocate(l, "3.20"), "#{__method__.to_s}"
    assert sv.claim_process_lot(l, :freeport=>true), "#{__method__.to_s}"
    assert sv.lot_opelocate(l, "3.32"), "#{__method__.to_s}"
    sleep 60
    assert sv.lot_rework(l, "A-QTNG-RWTC200.01", return_opNo: "2.10"), "#{__method__.to_s}"
    assert sv.claim_process_lot(l, :freeport=>true), "#{__method__.to_s}"
    qt = last_ts(sv, l)
    assert sv.lot_opelocate(l, "2.13"), "#{__method__.to_s}"
    assert sv.claim_process_lot(l, :freeport=>true), "#{__method__.to_s}"

    sleep 600

    res << l
    res << checkholdctrl(sv, l, qt)
    res << checkholdwd(sv, l, qt)
    return res
  end

  def tc217
    res = []
    sv = SiView::MM.new($env)
    l = new_lot(sv, "A-QTNG-TC216.01")
    return "no lot" if not l
    assert sv.lot_opelocate(l, "3.20"), "#{__method__.to_s}"
    assert sv.claim_process_lot(l, :freeport=>true), "#{__method__.to_s}"
    assert sv.lot_opelocate(l, "3.32"), "#{__method__.to_s}"
    sleep 60
    assert sv.lot_rework(l, "A-QTNG-RWTC200.01", return_opNo: "2.10"), "#{__method__.to_s}"
    assert sv.claim_process_lot(l, :freeport=>true), "#{__method__.to_s}"
    qt = last_ts(sv, l)

    sleep 600
    res << l
    res << checkholdctrl(sv, l, qt)
    res << checkholdwd(sv, l, qt)
    return res
  end

  def tc218
    res = []
    sv = SiView::MM.new($env)
    l = new_lot(sv, "A-QTNG-TC218.01")
    return "no lot" if not l
    assert sv.lot_opelocate(l, "3.20"), "#{__method__.to_s}"
    assert sv.claim_process_lot(l, :freeport=>true), "#{__method__.to_s}"
    assert sv.lot_opelocate(l, "3.32"), "#{__method__.to_s}"
    sleep 60
    assert sv.lot_rework(l, "A-QTNG-RWTC200.01", return_opNo: "4.30"), "#{__method__.to_s}"
    assert sv.claim_process_lot(l, :freeport=>true), "#{__method__.to_s}"
    qt = last_ts(sv, l)
    assert sv.lot_opelocate(l, "4.33"), "#{__method__.to_s}"
    assert sv.claim_process_lot(l, :freeport=>true), "#{__method__.to_s}"

    sleep 600

    res << l
    res << checkholdctrl(sv, l, qt)
    res << checkholdwd(sv, l, qt)
    return res
  end

  def tc219
    res = []
    sv = SiView::MM.new($env)
    l = new_lot(sv, "A-QTNG-TC218.01")
    return "no lot" if not l
    assert sv.lot_opelocate(l, "3.20"), "#{__method__.to_s}"
    assert sv.claim_process_lot(l, :freeport=>true), "#{__method__.to_s}"
    assert sv.lot_opelocate(l, "3.32"), "#{__method__.to_s}"
    sleep 60
    assert sv.lot_rework(l, "A-QTNG-RWTC200.01", return_opNo: "4.30"), "#{__method__.to_s}"
    assert sv.claim_process_lot(l, :freeport=>true), "#{__method__.to_s}"
    qt = last_ts(sv, l)

    sleep 600

    res << l
    res << checkholdctrl(sv, l, qt)
    res << checkholdwd(sv, l, qt)
    return res
  end

  def tc220
    res = []
    sv = SiView::MM.new($env)
    l = new_lot(sv, "A-QTNG-TC220.01")
    return "no lot" if not l
    assert sv.lot_opelocate(l, "3.20"), "#{__method__.to_s}"
    assert sv.claim_process_lot(l, :freeport=>true), "#{__method__.to_s}"
    assert sv.lot_opelocate(l, "3.31"), "#{__method__.to_s}"
    sleep 60
    assert sv.lot_rework(l, "A-QTNG-RWTC220.01", return_opNo: "3.32"), "#{__method__.to_s}"
    assert sv.claim_process_lot(l, :freeport=>true), "#{__method__.to_s}"
    qt = last_ts(sv, l)
    sleep 60
    assert sv.lot_opelocate(l, "2.13"), "#{__method__.to_s}"
    assert sv.claim_process_lot(l, :freeport=>true), "#{__method__.to_s}"
    sleep 600

    res << l
    res << checkholdctrl(sv, l, qt)
    res << checkholdwd(sv, l, qt)
    return res
  end

  def tc221
    res = []
    sv = SiView::MM.new($env)
    l = new_lot(sv, "A-QTNG-TC220.01")
    return "no lot" if not l
    assert sv.lot_opelocate(l, "3.20"), "#{__method__.to_s}"
    assert sv.claim_process_lot(l, :freeport=>true), "#{__method__.to_s}"
    assert sv.lot_opelocate(l, "3.31"), "#{__method__.to_s}"
    sleep 60
    assert sv.lot_rework(l, "A-QTNG-RWTC220.01", return_opNo: "3.32"), "#{__method__.to_s}"
    assert sv.claim_process_lot(l, :freeport=>true), "#{__method__.to_s}"
    qt = last_ts(sv, l)
    sleep 600

    res << l
    res << checkholdctrl(sv, l, qt)
    res << checkholdwd(sv, l, qt)
    return res
  end

  def tc222
    res = []
    sv = SiView::MM.new($env)
    l = new_lot(sv, "A-QTNG-TC200.01")
    return "no lot" if not l
    assert sv.lot_opelocate(l, "3.20"), "#{__method__.to_s}"
    assert sv.claim_process_lot(l, :freeport=>true), "#{__method__.to_s}"
    qt = last_ts(sv, l)
    assert sv.lot_opelocate(l, "3.32"), "#{__method__.to_s}"
    sleep 60
    assert sv.lot_rework(l, "A-QTNG-RWTC200.01", return_opNo: "2.10"), "#{__method__.to_s}"
    sleep 60
    assert sv.lot_rework_cancel(l)

    sleep 600

    res << l
    res << checkholdctrl(sv, l, qt)
    res << checkholdwd(sv, l, qt)
    return res
  end

  def tc223
    res = []
    sv = SiView::MM.new($env)
    l = new_lot(sv, "A-QTNG-TC200.01")
    return "no lot" if not l
    assert sv.lot_opelocate(l, "3.20"), "#{__method__.to_s}"
    assert sv.claim_process_lot(l, :freeport=>true), "#{__method__.to_s}"
    assert sv.lot_opelocate(l, "3.32"), "#{__method__.to_s}"
    qt = last_ts(sv, l)
    sleep 60
    assert sv.lot_rework(l, "A-QTNG-RWTC206.01", return_opNo: "2.10"), "#{__method__.to_s}"
    sleep 60
    assert sv.lot_rework_cancel(l)

    sleep 600

    res << l
    res << checkholdctrl(sv, l, qt)
    res << checkholdwd(sv, l, qt)
    return res
  end

  def tc224
    res = []
    sv = SiView::MM.new($env)
    l = new_lot(sv, "A-QTNG-TC202.01")
    return "no lot" if not l
    assert sv.lot_opelocate(l, "3.20"), "#{__method__.to_s}"
    assert sv.claim_process_lot(l, :freeport=>true), "#{__method__.to_s}"
    qt = last_ts(sv, l)
    assert sv.lot_opelocate(l, "3.32"), "#{__method__.to_s}"
    sleep 60
    assert sv.lot_rework(l, "A-QTNG-RWTC200.01", return_opNo: "4.30"), "#{__method__.to_s}"
    sleep 60
    assert sv.lot_rework_cancel(l)

    sleep 600

    res << l
    res << checkholdctrl(sv, l, qt)
    res << checkholdwd(sv, l, qt)
    return res
  end

  def tc225
    res = []
    sv = SiView::MM.new($env)
    l = new_lot(sv, "A-QTNG-TC202.01")
    return "no lot" if not l
    assert sv.lot_opelocate(l, "3.20"), "#{__method__.to_s}"
    assert sv.claim_process_lot(l, :freeport=>true), "#{__method__.to_s}"
    qt = last_ts(sv, l)
    assert sv.lot_opelocate(l, "3.32"), "#{__method__.to_s}"
    sleep 60
    assert sv.lot_rework(l, "A-QTNG-RWTC206.01", return_opNo: "4.30"), "#{__method__.to_s}"
    sleep 60
    assert sv.lot_rework_cancel(l)

    sleep 600

    res << l
    res << checkholdctrl(sv, l, qt)
    res << checkholdwd(sv, l, qt)
    return res
  end


  # ****************************************************************************************

  def last_ts(sv, lot)
    return(sv.siview_time(sv.lot_info(lot).claim_time))
  end

  def get_child_lot(sv, lot)
    _clot = []
    _clot = sv.lot_family(lot)
    _clot.delete(lot)
    return _clot[0]
  end

  def checkholdctrl(sv, lot, ts=false)
    ret=sv.lot_hold_list(lot, :reason=>"QTHL")
    if ret.size == 0
        return "Ctrl: No Hold"
    else
        r = ret.first
        qt = ''
        qt = (sv.siview_time(r['timestamp']) - ts).round if ts
        return "Ctrl: #{r['reason']} #{qt}".strip
    end
  end

  def checkholdwd(sv, lot, ts=false)
    ret=sv.lot_hold_list(lot, :reason=>"QTOV")
    if ret.size == 0
        return "WD: No Hold"
    else
        r = ret.first
        qt = ''
        qt = (sv.siview_time(r['timestamp']) - ts).round if ts
        return "WD: #{r['reason']} #{qt}".strip
    end
  end

  def checkfuturehold(lot)
    #$log.info "method checkfuturehold sleep 10s"
    #sleep 10
    ret=$sv.lot_futurehold_list(lot, :reason=>@@Hold_Code1)
    if ret.size == 0
        return nil
    else
        r = ret[0]
        refute_equal "", r['memo'], "hold memo for QTHL hold is empty"
        return r['reason']
    end
  end

  def checkhold_criticality_memo(lot, criticality, params={})
    # e.g.: @@claim_memo_criticality = "time of %s min exceeded at %s (hold action: %s, target operation %s, DLIS comment: %s), Criticality =%s"
    #memo = @@claim_memo_criticality %[@@QT_time,@@carrier0,@@carrier5,@@requestor,@@sorter,@@pg]
    #$log.info "method checkhold_criticality_memo sleep 30s"
    #sleep 30
	  hold = (params[:hold] or "")

    if hold == "future"
      ret=$sv.lot_futurehold_list(lot, :reason=>@@Hold_Code1)
    else
      ret=$sv.lot_hold_list(lot, :reason=>@@Hold_Code1)
    end

    if ret.size == 0
        return false
    else
        r = ret[0]
        end_memo = "criticality = #{criticality}"
        refute_equal "", r['reason'], "hold memo for QTHL hold is empty"
        assert r['memo'].start_with?("Queue time of #{@@QT_time} min"), "wrong start of claim memo for QTHL hold"
        assert r['memo'].end_with?(end_memo), "wrong end of claim memo for QTHL hold"
        return true
    end
  end

  def cleanqtcarrier
    $sv.carrier_list(carrier: "QT%").each{|c| $sv.delete_lot_family($sv.carrier_status(c).lots.first) unless $sv.carrier_status(c).lots.empty?}
  end

  def new_lot(sv, route)
    $log.info "new lot"
    params = {product: 'A-QTNG-TEMPLATE.01', route: 'A-QTNG-TEMPLATE.01'}
    lot = nil
    synchronize do
      lot = $svtest.new_lot({:sublottype=>"PO", :carrier=>"QT%"}.merge(params))
      sleep @@testmode
    end
    li = sv.lot_info(lot)
    sv.carrier_xfer_status_change(li.carrier, "EO", li.opEqps[0]) if li.xfer_status != "EO"
    sv.schdl_change(lot, route: route)
    return lot
  end
end
