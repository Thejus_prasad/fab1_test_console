=begin 
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.
    
Author: Steffen Steidten, Thomas Klose 2011-05-11

Version:

History:
  2015-03-27 ssteidte,  separated jCAP job tests from QT rule tests
=end

require 'SiViewTestCase'

# Test the jCAP QTController job. QT rules are tested separetely.
class Test_jCAP_QTController < SiViewTestCase
  @@product = 'A-QT-PRODUCT.01'
  @@route = 'A-QT-MAIN.01'
  @@opNo_start = '1000.1200'
  @@opNo_end = '1000.1500'
  @@qthold = 'QTHL'

  @@qtime = 600 # 10 minutes, configured in SiView for the related PD


  def self.startup
    super
    @@testlots = []
  end

  def self.after_all_passed
    @@testlots.each {|lot| $sv.delete_lot_family(lot)}
  end


  def test10_happy
    $setup_ok = false
    #
    assert lot = $svtest.new_lot(route: @@route, product: @@product)
    @@testlots << lot
    # lot1: process at start pd, process at end pd. No hold
    assert_equal 0, $sv.lot_opelocate(lot, @@opNo_start)
    assert $sv.claim_process_lot(lot)
    ##qtstart = siview_time($sv.lot_info(lot).claim_time)
    sleep 60
    assert_equal 0, $sv.lot_opelocate(lot, @@opNo_end)
    assert $sv.claim_process_lot(lot)
    #
    $log.info "waiting #{@@qtime/60} min for the qtime to expire"; sleep @@qtime
    assert_equal 0, $sv.lot_hold_list(lot).size, "lot is ONHOLD"
    #
    $setup_ok = true
  end

  def test20_timeout
    assert lot = $svtest.new_lot(route: @@route, product: @@product)
    @@testlots << lot
    # lot2: process at start pd, qt expires. Lot on hold
    assert_equal 0, $sv.lot_opelocate(lot, @@opNo_start)
    assert $sv.claim_process_lot(lot)
    ##qtstart = siview_time($sv.lot_info(lot).claim_time)
    #
    $log.info "waiting #{@@qtime/60} min for the qtime to expire"; sleep @@qtime
    assert lh = $sv.lot_hold_list(lot, reason: @@qthold).first, "lot is not ONHOLD"
    assert lh.memo.include?('criticality = C1'), "wrong hold claim memo"
  end
end
