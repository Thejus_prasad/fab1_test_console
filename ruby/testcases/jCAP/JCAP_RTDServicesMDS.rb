=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.


Author: Steffen Frieske, 2019-02-22

Version: 19.02

History:
  2021-08-27 sfrieske, require waitfor instead of misc
  2021-12-08 sfrieske, simplified MDS queries
=end

require 'misc/rtddb'
require 'jcap/mdswritesvc'
require 'util/waitfor'
require 'SiViewTestCase'


class JCAP_RTDServicesMDS < SiViewTestCase
  @@sv_defaults = {carrier_category: 'BEOL', carriers: '%', route: 'A-AP-MAINPD.02', product: 'A-AP-PRODUCT.02'}
  @@eqp = 'UTA007'
  @@stockers = ['UTSTO12', 'UTSTO13']

  @@testlots = []


  def self.after_all_passed
    @@testlots.each {|lot| @@sv.delete_lot_family(lot)}
  end


  def test00_setup
    $setup_ok = false
    #
    @@mdswrite = MdsWriteService.new($env)
    @@mds = RTD::MDS.new($env)
    assert @@chamber = @@sv.eqp_info(@@eqp).chambers.first.chamber, 'no chamber tool'
    assert_equal 0, @@sv.eqp_cleanup(@@eqp, mode: 'Off-Line-1'), 'error cleaning up eqp'  # SBY.2WPR ?
    # prepare test lot
    assert lot = @@svtest.new_lot, 'error creating test lot'
    @@testlots << lot
    rops = @@sv.lot_eqplist_in_route(lot, eqp: @@eqp)
    assert @@opNo = rops.keys.first, "eqp #{@@eqp} not used on route #{@@svtest.route}"
    assert_equal 0, @@sv.lot_opelocate(lot, @@opNo), 'error locating lot'
    #
    $setup_ok = true
  end

  def test11_write_eqpwip
    (11..19).each {|n|
      assert @@mdswrite.write_eqpwip(@@eqp, n), 'error sending msg'
      assert wait_for {@@mds.tool(@@eqp).first[:wip_lot_a3] == n}, "wrong MDS entry for n: #{n}"
    }
  end

  def test12_write_eqpsubstates  # TODO: not reliable (2nd retry fails)
    lot = @@testlots[0]
    assert wait_for {@@mds.tool(@@eqp).first[:wip_siview] > 0}, "no WIP in T_TOOL for #{@@eqp}"
    [1, 0].each {|n|
      assert @@mdswrite.write_eqpsubstates(@@eqp, 'n/a', wip_lot_a3: n), 'error sending msg'
      assert wait_for {@@mds.tool(@@eqp).first[:wip_lot_a3] == n}, "wrong WIP_LOT_A3 in T_TOOL for #{@@eqp}"
    }
    # T_TOOL
    %w(2INH 2WMR 2WCB).each {|s|
      assert @@mdswrite.write_eqpsubstates(@@eqp, s), 'error sending msg'
      assert wait_for {@@mds.tool(@@eqp).first[:substate] == s}, "wrong MDS entry for substate #{s.inspect}"
    }
    # T_SUBTOOL
    %w(2INH 2WMR 2WCB).each {|s|
      assert @@mdswrite.write_eqpsubstates(@@eqp, s, chamber: @@chamber), 'error sending msg'
      assert wait_for {@@mds.subtool(@@eqp, @@chamber).first[:substate] == s}, "wrong MDS entry for substate #{s.inspect}"
    }
  end

  def test13_write_eqpnexttime
    assert @@mdswrite.write_eqpnexttime(@@eqp, 1000), 'error sending msg'
    # NEXT_TIME set to something
    assert wait_for {!@@mds.tool(@@eqp).first[:nexttime].nil?}, 'wrong MDS entry for nexttime'
    # write -1 -> NEXT_TIME is null
    assert @@mdswrite.write_eqpnexttime(@@eqp, -1), 'error sending msg'
    assert wait_for {@@mds.tool(@@eqp).first[:nexttime].nil?}, 'wrong MDS entry for nexttime'
  end

  # note: AutoProc may create transfer jobs!
  def test14_write_fouptransfer
    assert carrier = @@svtest.get_empty_carrier
    assert @@svtest.cleanup_carrier(carrier)
    sleep 10
    @@stockers.each {|sto|
      assert @@mdswrite.write_fouptransfer(carrier, sto), 'error sending msg'
      assert wait_for {
        xj = @@mds.trans_job(carrier).first
        xj && xj[:tgtloc] == sto
      }, "wrong MDS entry for fouptransfer to #{sto}"
    }
  end

  def test15_write_lottgtarea
    lot = @@testlots[0]
    assert @@mdswrite.write_lottgtarea(lot, 'QAArea', memo: 'QA memo')
    e = nil
    assert wait_for {e = @@mds.lot_target_area(lot).first}, 'wrong MDS entry for lottgtarea'
    assert_equal 'QAArea', e[:tgtarea], 'wrong tgtarea'
    assert_equal 'QA memo', e[:memo], 'wrong claim memo'
  end

end
