=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: Adel Darwaish, 2013-02-15

Version: 21.05

History
  2013-05-30 dsteger,  added test case
  2015-01-08 ssteidte, use remote RTDEmulator rules
  2017-01-05 sfrieske, use new RTD::EmuRemote, cleanup
  2018-11-07 sfrieske, minor cleanup
  2019-02-19 sfrieske, improved EmuRemote usage
  2021-05-20 sfrieske, changed rule name
=end

require 'rtdserver/rtdemuremote'
require 'SiViewTestCase'


class JCAP_SkipProcessOperation < SiViewTestCase
  @@eqp = 'UTF001'    # any eqp to load lot2
  @@memo = 'Skip process operation by jCAP'
  @@rtd_rule = 'JCAP_SKIP_CHKYLHLD'
  # @@rtd_category = 'DISPATCHER/QA'
  @@columns = ['lot_id', 'mainpd_id', 'ope_no']
  @@job_interval = 330  # 930


  def self.shutdown
    @@sv.eqp_cleanup(@@eqp)
    @@testlots.each {|lot| @@sv.delete_lot_family(lot)}
  end


  def test00_setup
    $setup_ok = false
    #
    @@rtdremote = RTD::EmuRemote.new($env)
    #
    assert @@testlots = @@svtest.new_lots(2), 'error creating test lots'
    @@lot1, @@lot2 = @@testlots
    # set lot1 ONHOLD
    assert_equal 0, @@sv.lot_hold(@@lot1, nil)
    # move lots to the skip operation
    assert odata = @@sv.lot_operation_list(@@lot1)[1..-1].find {|e| !e.mandatory}
    @@opNo_skip = odata.opNo
    assert_equal 0, @@sv.lot_opelocate(@@lot1, @@opNo_skip, force: true), 'failed to locate lot'
    assert_equal 0, @@sv.lot_opelocate(@@lot2, @@opNo_skip), 'failed to locate lot'
    #
    # carrier for lot2 is EI
    assert @@svtest.cleanup_eqp(@@eqp, mode: 'Off-Line-1')
    c = @@sv.lot_info(@@lot2).carrier
    @@sv.carrier_xfer_status_change(c, 'EO', @@eqp)
    assert_equal 0, @@sv.eqp_load(@@eqp, nil, c, purpose: 'Other')
    #
    # prepare RTD rule reply
    assert @@rtdremote.add_emustation_wait('', rule: @@rtd_rule, timeout: @@job_interval) {
      @@rtdremote.create_response(@@columns, {'rows'=> @@testlots.collect {|lot| [lot, @@svtest.route, @@opNo_skip]}})
    }, 'rule has not been called'
    sleep 30
    #
    $setup_ok = true
  end

  def test11_happy_lot
    # lot not ONHOLD but carrier EI
    refute_equal @@opNo_skip, @@sv.lot_info(@@lot2).opNo, "wrong operation for lot #{@@lot2}"
    # verify GatePass claim memo
    oph = @@sv.lot_operation_history(@@lot2, category: 'GatePass', opNo: @@opNo_skip)
    assert oph.size > 0, "no GatePass history for lot #{@@lot2}"
    assert oph.last.memo.include?(@@memo), 'wrong claim memo'
  end

  def test12_lot_on_hold
    assert_equal @@opNo_skip, @@sv.lot_info(@@lot1).opNo, "wrong operation for lot #{@@lot1}"
  end

end
