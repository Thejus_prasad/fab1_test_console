=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.


Author: Steffen Frieske, 2016-12-06

Version: 18.08

History:
  2017-01-05 sfrieske, use new RTD::EmuRemote, minor cleanup
  2017-04-10 sfrieske, added formal rule test (optional)
  2018-11-07 sfrieske, minor cleanup
  2019-02-19 sfrieske, improved EmuRemote usage
  2021-08-26 sfrieske, removed dependency on misc.rb

Notes:
  This job periodically calls RTD and just acts on the response, no logic contained.
  see https://docs.google.com/document/d/1wD5ThpDyehJXuIH7WsirWTxzS7goAxc6cKNQXTiJrlE/edit?ts=583bf0d7
=end

require 'rtdserver/rtdclient'
require 'rtdserver/rtdemuremote'
require 'SiViewTestCase'


class JCAP_CarrierDistribution < SiViewTestCase
  @@carriers = ['UTCD01', 'UTCD02', 'UTCD03']
  @@categories = ['MOL', 'NOTEX', 'BEOL']
  @@category0 = 'FEOL'
  @@description = 'QA Test'

  @@rtd_rule = 'JCAP_CARRIER_DISTRIBUTION'
  @@rtd_category = 'DISPATCHER/JCAP'
  @@columns = ['cast_id', 'cast_category']

  @@job_interval = 330  # 5 minutes


  def self.shutdown
    @@rtdremote.delete_emustation('', rule: @@rtd_rule)
  end

  def setup
    # set empty_reply for the time of test preparation
    assert @@rtdremote = RTD::EmuRemote.new($env) unless self.class.class_variable_defined?(:@@rtdremote)
    assert @@rtdremote.add_emustation('', rule: @@rtd_rule, method: 'empty_reply')
    # clean up and optionally create carriers, set description
    @@carriers.each {|c|
      @@sv.durable_register('Cassette', c, @@category0) if @@sv.carrier_list(carrier: c).empty?
      assert @@svtest.cleanup_carrier(c, make_empty: true)
      assert_equal 0, @@sv.durable_register('Cassette', c, @@category0, description: @@description, update: true)
      # assert_equal @@category0, @@sv.carrier_status(c).category, 'wrong carrier category'
    }
  end


  def test11_happy
    $setup_ok = false
    #
    # prepare RTDEmu answer
    data = @@carriers.each_index.collect {|i| [@@carriers[i], @@categories[i]]}
    assert @@rtdremote.add_emustation_wait('', rule: @@rtd_rule, timeout: @@job_interval) {
      @@rtdremote.create_response(@@columns, {'rows'=>data})
    }, 'rule has not been called'
    # allow the job to work
    sleep 60
    assert_equal @@categories[0], @@sv.carrier_status(@@carriers[0]).category, 'wrong carrier category'
    assert_equal @@category0, @@sv.carrier_status(@@carriers[1]).category, 'wrong carrier category'
    assert_equal @@categories[2], @@sv.carrier_status(@@carriers[2]).category, 'wrong carrier category'
    @@carriers.each {|c|
      assert_equal @@description, @@sv.carrier_status(c, raw: true).cassetteBRInfo.description
    }
    #
    $setup_ok = true
  end

  def test12_wrong_columns
    # prepare RTDEmu answer
    data = @@carriers.each_index.collect {|i| [@@carriers[i], @@categories[i]]}
    assert @@rtdremote.add_emustation_wait('', rule: @@rtd_rule, timeout: @@job_interval) {
      @@rtdremote.create_response(['cast_idQA', 'QAcast_category'], {'rows'=>data})
    }, 'rule has not been called'
    # allow the job to work
    sleep 60
    @@carriers.each {|c|
      csraw= @@sv.carrier_status(c, raw: true).cassetteBRInfo
      assert_equal @@category0, csraw.cassetteCategory, 'wrong carrier category'
      assert_equal @@description, csraw.description, 'wrong carrier description'
    }
  end

  # manually verify RTD rule output
  def testman91_RTD_Rule
    assert @@rtdclient = RTD::Client.new($env)
    assert res = @@rtdclient.dispatch_list(nil, report: @@rtd_rule, category: @@rtd_category)
    assert_equal @@rtd_rule, res.rule, 'wrong rule called'
    assert_equal @@columns, res.headers, 'wrong headers returned'
  end

end
