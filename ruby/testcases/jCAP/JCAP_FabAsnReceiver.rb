=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author:  sfrieske, 2019-03-13

Version: 21.08

History:
  2019-04-09 sfrieske, verify notifications
  2019-05-21 sfrieske, send subcon IDs
  2019-07-11 sfrieske, added test for STBtoLotFamily
  2021-05-25 aschmid3, added tests 44, 51 and 53
  2021-08-24 aschmid3, adjusted tests 11, 12 and 13 due to new config via CR85975 (no SLT differentiation)
                       added tests 21, 22 and 23 for StageGroup 'SORT'
  2021-08-27 sfrieske, require waitfor instead of misc
  2021-10-15 sfrieske, added test52 (deletion of incomplete lot script parameters)
  2021-11-26 sfrieske, added lot owner check for v21.08, merged ASN=Y checks into test11
=end

require 'derdack'
require 'jcap/erpmdm'
require 'jcap/fabasnreceiver'
require 'util/waitfor'
require 'SiViewTestCase'


class JCAP_FabAsnReceiver < SiViewTestCase
  @@customer = 'ericab'
  @@udata_splitseq = '700'
  @@udata_splitseq_amd = '99'
  @@udata_splitseq_sort = '800'
  @@udata_splitseq_sort_amd = 'C9'
  @@erpitem = '0000001VN343'  # must have a B layer product associated in MDM
  @@tktype = 'R'
  @@scriptparams = {'CountryOfOrigin'=>'US', 'ERPItem'=>@@erpitem}
  @@scriptparams_delete = %w(ShipComment ShipLabelNote ShipToFirm TurnkeySubconIDBump TurnkeySubconIDSort)
  #
  # for lot creation in update tests
  @@sv_defaults = {
    product: 'WARP352-A1FABU.AA01', route: '000-FAB-00-BTFRCVX.0001', customer: @@customer, erpitem: @@erpitem
  }
  #
  # F8 is the source fab for Backup Operation lots (special tests)
  @@env_src = 'f8stag'
  @@svsrc_defaults = {
    product: 'UT-PRODUCT-FAB1.01', route: 'UTRT101.01', customer: @@customer, carriers: '%'
  }
  @@psend = {backup_opNo: '1000.200', backup_bank: 'O-BACKUPOPER'}
  @@erpitem_bopchannel = 'SHELBY3CC-J01'  # valid item with backup channel defined in source fab

  @@vcarriers = []
  @@testlots = []
  @@testlots_src = []


  def self.after_all_passed
    @@testlots.each {|lot| @@sv.delete_lot_family(lot)}
    @@vcarriers.each {|vcarrier| @@svtest.delete_registered_carrier(vcarrier)}
    @@testlots_src.each {|lot| @@sv_src.backup_delete_lot_family(lot, @@sv)}
    nil
  end


  def test00_setup
    $setup_ok = false
    #
    @@mdm = ERPMDM.new($env)
    found = false
    @@mdm.mdm_entries(erpitem: @@erpitem).each {|e|
      if @@sv.CS_product_list(product: e.product).first.manufacturingLayerID == 'B'
        found = true
        break
      end
    }
    assert found, "no suitable product for ERPItem #{@@erpitem}"
    #
    @@fabasn = FabAsnReceiver.new($env, erpitem: @@erpitem, tktype: @@tktype)
    assert_match /\d\.\d/, @@fabasn.get_version, 'no MQ connectivity'
    @@notifications = DerdackEvents.new($env)
    #
    assert_equal 'LOTID', @@sv.user_data(:customer, @@customer)['CustomLIG_LotIdFormat'], 'wrong customer setup'
    #
    @@svtest_src = SiView::Test.new(@@env_src, @@svsrc_defaults)
    @@sv_src = @@svtest_src.sv
    #
    $setup_ok = true
  end

  def test11_happy_newcarrier_qaeric   # incl ASN=Y checks from MSR1777553
    $setup_ok = false
    #
    # pick a 'valid' new FOSB ID
    vcarrier = unique_string('F1AQ')  # matching ^F[189ABX][ABCDEFX].........$ (12 digits)
    assert_empty @@sv.carrier_list(carrier: vcarrier), 'carrier already exists'
    @@vcarriers << vcarrier
    # generate a valid new lot ID
    #   like 0000Q.00000 --> NEW due to CR85975; 'qaeric' due to LOT_LABEL properties
    assert lot = @@sv.generate_new_lotids('qaeric').first
    @@testlots << lot
    # send notification
    order = "QA00#{unique_string}"
    assert @@fabasn.send_msg(lot, vcarrier, customer: 'qaeric', shipno: order)
    #
    # verify lot data
    assert_equal lot.gsub('.', ''), @@sv.lot_label(lot), 'wrong LOT_LABEL (customer setup?)'
    assert liraw = @@sv.lots_info_raw(lot, wafers: true).strLotInfo.first
    assert_equal order, liraw.strLotOrderInfo.orderNumber, 'wrong orderNumber'
    assert_equal 'X-JASNRC', @@sv.extract_si(liraw.siInfo).lotOwner, 'wrong lot owner'
    liraw.strLotWaferAttributes.each {|w| assert_match /^\d\d$/, w.aliasWaferName}
    assert_equal @@udata_splitseq, @@sv.user_data(:lotfamily, lot)['LastSplitSeq'], 'wrong LastSplitSeq'
    # verify lot comment
    $log.info 'verifying lot comment'
    assert comment = @@sv.lot_comments(lot).last
    assert_equal 'ASN=Y', comment.desc, 'wrong comment'
    assert_equal 'X-JASNRC', comment.user, 'wrong report user ID'
    # verify lot script parameters
    $log.info 'verifying lot script parameters'
    now = Time.now
    tstart = now.strftime('%F')           # YYYY-MM-dd
    offset = now.utc.hour >= 12 ? 5 : 4   # after 12:00 UTC plus 5 days
    ftime = (now + offset * 24 * 3600).strftime('%F')
    assert_equal '2.000000', @@sv.user_parameter('Lot', lot, name: 'FabDPML').value, 'wrong FabDPML'
    assert_equal tstart, @@sv.user_parameter('Lot', lot, name: 'FSD').value, 'wrong FabStartDate'
    assert_equal ftime, @@sv.user_parameter('Lot', lot, name: 'FOD_INIT').value, 'wrong Initial Fab Out Date'
    assert_equal ftime, @@sv.user_parameter('Lot', lot, name: 'FOD_COMMIT').value, 'wrong Committed Fab Out Date'
    @@scriptparams.each_pair {|name, value|
      assert_equal value, @@sv.user_parameter('Lot', lot, name: name).value, "wrong script parameter #{name}"
    }
    assert_equal 'MEDIUM', @@sv.user_parameter('Lot', lot, name: 'GateCD_Target_PLN').value, 'wrong Gate Target'
    assert_equal @@fabasn.srcfab, @@sv.user_parameter('Lot', lot, name: 'SourceFabCode').value, 'wrong SourceFabCode'
    assert_equal @@tktype, @@sv.user_parameter('Lot', lot, name: 'TurnkeyType').value, 'wrong TurnkeyType'
    #
    # verify carrier data
    assert_equal vcarrier, liraw.strLotLocationInfo.cassetteID.identifier, 'wrong carrier'
    assert cstat = @@sv.carrier_status(vcarrier), 'carrier has not been created'
    assert_equal "SB#{liraw.strLotBasicInfo.requiredCassetteCategory[0..1]}", cstat.category, 'wrong carrier category'
    assert_equal "#{vcarrier[0..1]}-FOSB", @@sv.user_data(:carrier, vcarrier)['MaterialType'], 'wrong carrier Udata'
    #
    $setup_ok = true
  end

  def test12_happy_9carrier_amd
    # pick an 'invalid' new FOSB ID, lot goes to an existing 9% carrier
    vcarrier = unique_string('QA')  # not matching ^F[189ABX][ABCDEFX].........$ (12 digits)
    assert_empty @@sv.carrier_list(carrier: vcarrier), 'carrier already exists'
    @@vcarriers << vcarrier
    # generate a valid new lot ID
    assert lot = @@sv.generate_new_lotids('amd').first
    @@testlots << lot
    # send notification
    order = "QA00#{unique_string}"
    assert @@fabasn.send_msg(lot, vcarrier, customer: 'amd', keep_lotid: true, shipno: order)
    # used stageGroup: BUMP
    #
    # verify lot data, incl 9% carrier (note: no LOT_LABEL for customer 'amd')
    assert li = @@sv.lot_info(lot, wafers: true)
    assert_equal order, @@sv._res.strLotInfo[0].strLotOrderInfo.orderNumber, 'wrong orderNumber'
    @@scriptparams.each_pair {|name, value|
      assert_equal value, @@sv.user_parameter('Lot', lot, name: name).value, "wrong script parameter #{name}"
    }
    assert_equal @@tktype, @@sv.user_parameter('Lot', lot, name: 'TurnkeyType').value
    li.wafers.each {|w| assert_match /^\d\d$/, w.alias}
    assert_equal @@udata_splitseq_amd, @@sv.user_data(:lotfamily, lot)['LastSplitSeq'], 'wrong LastSplitSeq'
    assert li.carrier.start_with?('9'), 'wrong carrier (not 9%)'
  end

  def test13_empty_9carrier
    # send notification with an empty carrier ID, lot goes to an existing 9% carrier
    assert lot = @@sv.generate_new_lotids(@@customer).first
    @@testlots << lot
    # send notification
    order = "QA00#{unique_string}"
    assert @@fabasn.send_msg(lot, '', shipno: order)
    #
    # verify lot data, incl 9% carrier
    assert_equal lot.gsub('.', ''), @@sv.lot_label(lot), 'wrong LOT_LABEL (customer setup?)'
    assert li = @@sv.lot_info(lot, wafers: true)
    assert_equal order, @@sv._res.strLotInfo[0].strLotOrderInfo.orderNumber, 'wrong orderNumber'
    @@scriptparams.each_pair {|name, value|
      assert_equal value, @@sv.user_parameter('Lot', lot, name: name).value, "wrong script parameter #{name}"
    }
    assert_equal @@tktype, @@sv.user_parameter('Lot', lot, name: 'TurnkeyType').value
    li.wafers.each {|w| assert_match /^\d\d$/, w.alias}
    assert_empty @@sv.user_data(:lotfamily, lot)['LastSplitSeq'], 'wrong LastSplitSeq'
    assert li.carrier.start_with?('9'), 'wrong carrier (not 9%)'
  end

  # test2x derives from test1x --> with using of StageGroup 'SORT' (instead of 'BUMP')
  #
  def test21_happy_newcarrier_qaeric_SORT
    $setup_ok = false
    #
    # pick a 'valid' new FOSB ID
    vcarrier = unique_string('F1AQ')  # matching ^F[189ABX][ABCDEFX].........$ (12 digits)
    assert_empty @@sv.carrier_list(carrier: vcarrier), 'carrier already exists'
    @@vcarriers << vcarrier
    # generate a valid new lot ID
    #   like 0000Q.00000 --> NEW due to CR85975; 'qaeric' due to LOT_LABEL properties
    assert lot = @@sv.generate_new_lotids('qaeric').first
    @@testlots << lot
    # send notification
    order = "QA00#{unique_string}"
    assert @@fabasn.send_msg(lot, vcarrier, customer: 'qaeric', shipno: order, stagegroup: 'SORT')
    #
    # verify lot data
    assert_equal lot.gsub('.', ''), @@sv.lot_label(lot), 'wrong LOT_LABEL (customer setup?)'
    assert li = @@sv.lot_info(lot, wafers: true)
    assert_equal order, @@sv._res.strLotInfo[0].strLotOrderInfo.orderNumber, 'wrong orderNumber'
    @@scriptparams.each_pair {|name, value|
      assert_equal value, @@sv.user_parameter('Lot', lot, name: name).value, "wrong script parameter #{name}"
    }
    assert_equal @@tktype, @@sv.user_parameter('Lot', lot, name: 'TurnkeyType').value
    li.wafers.each {|w| assert_match /^\d\d$/, w.alias}
    assert_equal @@udata_splitseq_sort, @@sv.user_data(:lotfamily, lot)['LastSplitSeq'], 'wrong LastSplitSeq'
    # verify carrier data
    assert_equal vcarrier, li.carrier, 'wrong carrier'
    assert cstat = @@sv.carrier_status(vcarrier), 'carrier has not been created'
    assert_equal "SB#{li.cast_cat[0..1]}", cstat.category, 'wrong carrier category'
    assert_equal "#{vcarrier[0..1]}-FOSB", @@sv.user_data(:carrier, vcarrier)['MaterialType'], 'wrong carrier Udata'
    #
    $setup_ok = true
  end

  def test22_happy_9carrier_amd_SORT
    # pick an 'invalid' new FOSB ID, lot goes to an existing 9% carrier
    vcarrier = unique_string('QA')  # not matching ^F[189ABX][ABCDEFX].........$ (12 digits)
    assert_empty @@sv.carrier_list(carrier: vcarrier), 'carrier already exists'
    @@vcarriers << vcarrier
    # generate a valid new lot ID
    assert lot = @@sv.generate_new_lotids('amd').first
    @@testlots << lot
    # send notification
    order = "QA00#{unique_string}"
    assert @@fabasn.send_msg(lot, vcarrier, customer: 'amd', keep_lotid: true, shipno: order, stagegroup: 'SORT')
    #
    # verify lot data, incl 9% carrier (note: no LOT_LABEL for customer 'amd')
    assert li = @@sv.lot_info(lot, wafers: true)
    assert_equal order, @@sv._res.strLotInfo[0].strLotOrderInfo.orderNumber, 'wrong orderNumber'
    @@scriptparams.each_pair {|name, value|
      assert_equal value, @@sv.user_parameter('Lot', lot, name: name).value, "wrong script parameter #{name}"
    }
    assert_equal @@tktype, @@sv.user_parameter('Lot', lot, name: 'TurnkeyType').value
    li.wafers.each {|w| assert_match /^\d\d$/, w.alias}
    assert_equal @@udata_splitseq_sort_amd, @@sv.user_data(:lotfamily, lot)['LastSplitSeq'], 'wrong LastSplitSeq'
    assert li.carrier.start_with?('9'), 'wrong carrier (not 9%)'
  end

  def test23_empty_9carrier_SORT
    # send notification with an empty carrier ID, lot goes to an existing 9% carrier
    assert lot = @@sv.generate_new_lotids(@@customer).first
    @@testlots << lot
    # send notification
    order = "QA00#{unique_string}"
    assert @@fabasn.send_msg(lot, '', shipno: order, stagegroup: 'SORT')
    #
    # verify lot data, incl 9% carrier
    assert_equal lot.gsub('.', ''), @@sv.lot_label(lot), 'wrong LOT_LABEL (customer setup?)'
    assert li = @@sv.lot_info(lot, wafers: true)
    assert_equal order, @@sv._res.strLotInfo[0].strLotOrderInfo.orderNumber, 'wrong orderNumber'
    @@scriptparams.each_pair {|name, value|
      assert_equal value, @@sv.user_parameter('Lot', lot, name: name).value, "wrong script parameter #{name}"
    }
    assert_equal @@tktype, @@sv.user_parameter('Lot', lot, name: 'TurnkeyType').value
    li.wafers.each {|w| assert_match /^\d\d$/, w.alias}
    assert_empty @@sv.user_data(:lotfamily, lot)['LastSplitSeq'], 'wrong LastSplitSeq'
    assert li.carrier.start_with?('9'), 'wrong carrier (not 9%)'
  end


  def test31_duplicate_slots
    # send notification with a duplicate slot
    assert lot = @@sv.generate_new_lotids(@@customer).first
    @@testlots << lot
    tstart = Time.now - 10
    refute @@fabasn.send_msg(lot, unique_string('QA'), slots: [1, 2, 2])
    assert ret = @@fabasn.mq.service_response[:receiveAdvanceShipmentNotification2Response][:return][:shipment]
    assert_equal 'ADVANCE_SHIPPING_NOTICE_SERVICE_VALIDATION_ERROR', ret[:code], 'wrong return code'
    assert ret[:msg].include?('Slot 2'), 'wrong return msg'
    refute @@sv.lot_exists?(lot), 'lot must not be created'
    # Derdack notification
    assert @@notifications.wait_for(tstart: tstart, timeout: 120,
      service: '%jCAP_AsyncNotification%', filter: {'Application'=>'FabAsnReceiver'}) {|msgs|
        msgs.find {|m| m['Message'].include?('Slot 2')}
    }, 'missing or wrong notification'
  end

  def test32_duplicate_lot
    # send notification with a duplicate lot, must run after test11 or test12 have run
    assert lot = @@testlots.first, 'run test1x before this one to have a valid lot'
    tstart = Time.now - 10
    refute @@fabasn.send_msg(lot, unique_string('QA')), 'notification must be rejected'
    assert ret = @@fabasn.mq.service_response[:receiveAdvanceShipmentNotification2Response][:return][:shipment]
    assert_equal 'ADVANCE_SHIPPING_NOTICE_SERVICE_POST_PROCESSING_ERROR', ret[:code], 'wrong return code'
    # assert ret[:details][:msg].include?("#{lot} already exists"), 'wrong msg'
    assert ret[:details][:msg].include?('INVALID_LOT_RESTART'), 'wrong msg'
    # Derdack notification
    filter = {'Application'=>'FabAsnReceiver', 'LotID'=>lot.sub('.', '')}
    assert @@notifications.wait_for(tstart: tstart, timeout: 120,
      service: '%jCAP_AsyncNotification%', filter: filter) {|msgs|
        msgs.find {|m| m['Message'].include?('INVALID_LOT_RESTART')}
      }, 'missing or wrong notification'
  end

  def test33_duplicate_wafers
    # send notification with a duplicate wafer
    assert lot = @@sv.generate_new_lotids(@@customer).first
    @@testlots << lot
    tstart = Time.now - 10
    refute @@fabasn.send_msg(lot, unique_string('QA'), wafers: Array.new(2, unique_string('WW')))
    assert ret = @@fabasn.mq.service_response[:receiveAdvanceShipmentNotification2Response][:return][:shipment]
    assert_equal 'ADVANCE_SHIPPING_NOTICE_SERVICE_VALIDATION_ERROR', ret[:code], 'wrong return code'
    assert ret[:msg].include?('Duplicate wafer'), 'wrong return msg'
    refute @@sv.lot_exists?(lot), 'lot must not be created'
    # Derdack notification
    assert @@notifications.wait_for(tstart: tstart, timeout: 120,
      service: '%jCAP_AsyncNotification%', filter: {'Application'=>'FabAsnReceiver'}) {|msgs|
        msgs.find {|m| m['Message'].include?('Duplicate wafer')}
    }, 'missing or wrong notification'
  end

  def test41_backup_lot
    # get a lotid that does not exist at backup site
    lot = nil
    100.times {
      lot = @@sv_src.generate_new_lotids(@@customer).last
      if @@sv.lot_exists?(lot)
        lot = nil
      else
        break
      end
    }
    assert lot, 'no valid lot ID found'
    # create a lot at the source site and backup-send it
    assert lot = @@svtest_src.new_lot, 'error creating lot'
    @@testlots_src << lot
    assert @@sv_src.prepare_for_backup(lot,  @@psend), 'error receiving backup lot'
    assert_equal 0, @@sv_src.backupop_lot_send(lot, @@sv)
    #
    # send notification with this lot ID
    vcarrier = unique_string('F1AQ')  # matching ^F[189ABX][ABCDEFX].........$ (12 digits)
    assert_empty @@sv.carrier_list(carrier: vcarrier), 'carrier already exists'
    @@vcarriers << vcarrier
    # no error message, lot must just be ignored
    assert @@fabasn.send_msg(lot, vcarrier), 'wrong msg rejection'
    refute @@sv.lot_exists?(lot), 'lot must not be created'
    assert_empty @@sv.carrier_list(carrier: vcarrier), 'carrier must not be created'
    #
    # send notification again with empty carrier and slots, same behaviour
    assert @@fabasn.send_msg(lot, '', noslots: true), 'wrong msg rejection'
    refute @@sv.lot_exists?(lot), 'lot must not be created'
    assert_empty @@sv.carrier_list(carrier: vcarrier), 'carrier must not be created'
  end

  def test42_backupchannel_nolot
    # get lotid at the source site but no lot (yet)
    assert lot = @@sv_src.generate_new_lotids('qgt').last
    @@testlots << lot
    #
    # send notification with this lot ID: no error message, lot must just be ignored
    assert @@fabasn.send_msg(lot, '', erpitem: @@erpitem_bopchannel), 'wrong msg rejection'
    refute @@sv.lot_exists?(lot), 'lot must not be created'
  end

  def test43_stbtolotfamily
    # send notification with an empty carrier ID, lot goes to an existing 9% carrier
    assert lot = @@sv.generate_new_lotids(@@customer).first
    @@testlots << lot
    # send notification (3 wafers)
    assert @@fabasn.send_msg(lot, '')
    # send notification for a child lot, 3 wafers
    lc = lot.split('.').first + '.09000'
    @@testlots << lc
    assert @@fabasn.send_msg(lc, '')
    # sleep 3
    # @@fabasn.send_msg(lc, '')
    # verify lot family and merge (proving UDATA are correct)
    assert_equal lot, @@sv.lot_info(lc).family, 'wrong lot family'
    assert_equal 0, @@sv.wafer_sort(lc, @@sv.lot_info(lot).carrier, slots: 'empty')
    assert_equal 0, @@sv.lot_merge(lot, lc), 'error merging lot'
  end

  def test44_backup_childlot  # MSR1767485
    # get a lotid that does not exist at backup site
    lot = nil
    100.times {
      lot = @@sv_src.generate_new_lotids(@@customer).last
      if @@sv.lot_exists?(lot)
        lot = nil
      else
        break
      end
    }
    assert lot, 'no valid lot ID found'
    # create a lot at the source site, split a child lot and backup-send it
    assert lot = @@svtest_src.new_lot, 'error creating lot'
    @@testlots_src << lot
    assert lc1 = @@sv_src.lot_split(lot, 2)
    @@testlots_src << lc1
    assert @@sv_src.prepare_for_backup(lc1,  @@psend), 'error receiving backup lot'
    assert_equal 0, @@sv_src.backupop_lot_send(lc1, @@sv)
    assert_equal 0, @@sv.backupop_lot_send_receive(lc1, @@sv_src)
    #
    # send notification with a second child lot ID
    assert lc2 = @@sv_src.lot_split(lot, 2)
    @@testlots_src << lc2
    vcarrier = unique_string('F1AQ')  # matching ^F[189ABX][ABCDEFX].........$ (12 digits)
    assert_empty @@sv.carrier_list(carrier: vcarrier), 'carrier already exists'
    @@vcarriers << vcarrier
    # error message, no creation of second child lot, wafers and carrier
    wafers = 2.times.collect {unique_string('WW')}
    refute @@fabasn.send_msg(lc2, vcarrier, wafers: wafers), 'lot must be rejected'
    refute @@sv.lot_exists?(lc2), 'lot must not be created'
    wafers.each {|w| assert_nil @@sv.lot_by_waferid(w)}
    # carrier is created, declared as expected
    ##assert_empty @@sv.carrier_list(carrier: vcarrier), 'carrier must not be created'
    #
    ## send notification again with empty carrier and slots, same behaviour
    #refute @@fabasn.send_msg(lc2, '', noslots: true, wafers: wafers), 'lot must be rejected'
    #refute @@sv.lot_exists?(lc2), 'lot must not be created'
    #wafers.each {|w| assert_nil @@sv.lot_by_waferid(w)}
    ## carrier is created, declared as expected
    #assert_empty @@sv.carrier_list(carrier: vcarrier), 'carrier must not be created'
  end

  def test51_lot_scriptparameter_update   # MSR1770446
    # create Fab lot in a 9% TKEY carrier
    assert ec = @@svtest.get_empty_carrier(carrier_category: 'TKEY', carrier: '9%'), 'no empty 9% carrier'
    wafers = 3.times.collect {unique_string('WW')}
    lprops = {
      carrier: ec, waferids: wafers, user_parameters: {
        'TurnkeyType'=>'QA', 'TurnkeySubconIDBump'=>'GG', 'TurnkeySubconIDSort'=>'GB',
        'ShipComment'=>'Fab1QA', 'ShipLabelNote'=>'QAQA-Fab1', 'ShipToFirm'=>'GB',
        'CountryOfOrigin'=>'US', 'SourceFabCode'=>'F8'
      }
    }
    assert lot = @@svtest.new_lot(lprops), 'error creating Fab lot'
    @@testlots << lot
    # lot has to be in a NonProBank with additional hold code XOHL
    assert_equal 0, @@sv.lot_nonprobankin(lot, 'OX-ENDFAB')
    assert @@sv.lot_hold(lot, 'XOHL')
    # send notification
    assert @@fabasn.send_msg(lot, '', wafers: wafers)
    # verify lot data
    $log.info 'verifying lot data'
    assert li = @@sv.lot_info(lot, wafers: true)
    assert_equal ec, li.carrier, 'wrong carrier'
    li.wafers.each {|w| assert_match /^\d\d$/, w.alias}
    # verify lot script parameters
    $log.info 'verifying lot script parameters'
    @@scriptparams.each_pair {|name, value|
      assert_equal value, @@sv.user_parameter('Lot', lot, name: name).value, "wrong script parameter #{name}"
    }
    assert_equal @@fabasn.srcfab, @@sv.user_parameter('Lot', lot, name: 'SourceFabCode').value, 'wrong SourceFabCode'
    assert_equal @@tktype, @@sv.user_parameter('Lot', lot, name: 'TurnkeyType').value, 'wrong TurnkeyType'
    # deletion required
    ok = true
    @@scriptparams_delete.each {|pname|
      ($log.warn "not deleted: #{pname}"; ok=false) if @@sv.user_parameter('Lot', lot, name: pname).valueflag
    }
    assert ok, 'lot script parameters not deleted correctly'
  end

  def test52_lot_scriptparameters_incomplete # JCORP-2897, incomplete script parameters
    # create Fab lot in a 9% TKEY carrier
    assert ec = @@svtest.get_empty_carrier(carrier_category: 'TKEY', carrier: '9%'), 'no empty 9% carrier'
    wafers = 3.times.collect {unique_string('WW')}
    lprops = {
      carrier: ec, waferids: wafers, user_parameters: {
        'TurnkeyType'=>'QA', 'TurnkeySubconIDBump'=>'GG',   # removed: 'TurnkeySubconIDSort'=>'GB',
        'ShipComment'=>'Fab1QA', 'ShipLabelNote'=>'QAQA-Fab1', 'ShipToFirm'=>'GB',
        'CountryOfOrigin'=>'US', 'SourceFabCode'=>'F8'
      }
    }
    assert lot = @@svtest.new_lot(lprops), 'error creating Fab lot'
    @@testlots << lot
    # lot has to be in a NonProBank with additional hold code XOHL
    assert_equal 0, @@sv.lot_nonprobankin(lot, 'OX-ENDFAB')
    assert @@sv.lot_hold(lot, 'XOHL')
    # send notification
    assert @@fabasn.send_msg(lot, '', wafers: wafers)
    # verify lot data
    $log.info 'verifying lot data'
    assert li = @@sv.lot_info(lot, wafers: true)
    assert_equal ec, li.carrier, 'wrong carrier'
    li.wafers.each {|w| assert_match /^\d\d$/, w.alias}
    # verify lot script parameters
    $log.info 'verifying lot script parameters'
    @@scriptparams.each_pair {|name, value|
      assert_equal value, @@sv.user_parameter('Lot', lot, name: name).value, "wrong script parameter #{name}"
    }
    assert_equal @@fabasn.srcfab, @@sv.user_parameter('Lot', lot, name: 'SourceFabCode').value, 'wrong SourceFabCode'
    assert_equal @@tktype, @@sv.user_parameter('Lot', lot, name: 'TurnkeyType').value, 'wrong TurnkeyType'
    # deletion required according to MSR comment of Tony Genenncher
    ok = true
    @@scriptparams_delete.each {|pname|
      ($log.warn "not deleted: #{pname}"; ok=false) if @@sv.user_parameter('Lot', lot, name: pname).valueflag
    }
    assert ok, 'lot script parameters not deleted correctly'
  end

  # aux methods

  # for dev only
  # def bop_channels
  #   return @@sv_src.backup_channels(level: 'Product').collect {|e| e.strBackupUniqueChannel.categoryID}
  # end

end
