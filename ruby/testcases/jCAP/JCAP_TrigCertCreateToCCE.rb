=begin
(c) Copyright 2011 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: Daniel Steger, 2011-04-21

Version: 21.01

History:
  2012-01-24 ssteidte, use builder for XML doc generation
  2015-03-25 ssteidte, cleaned up testcases, create test lots on the fly
  2016-08-24 sfrieske, use lib rqrsp_mq instead of rqrsp
  2017-12-13 sfrieske, minor cleanup
  2019-01-24 sfrieske, minor cleanup
  2021-02-12 vbhojara, added test61 for v21.01
  2021-07-27 sfrieske, renamed test61 to testman61
=end

require 'SiViewTestCase'
require 'jcap/trigcce'


class JCAP_TrigCertCreateToCCE < SiViewTestCase
  @@sv_defaults = {product: 'UT-PRODUCT-TRIGCCE.01', route: 'UTRT-TRIGCCE.01', nwafers: 10}
  @@nonprobank = 'UT-NONPROD'
  @@jcapuser = 'X-JTRCCE'
  @@triggerpd = 'FOUT-CERT.01'
  @@triggerhold = 'X-M4'
  @@errorhold = 'ENG'
  @@rejecthold = 'DQE'
  @@reworkhold = 'RWK'
  # for CECEILE EI
  @@product2 = 'UT-PRODUCT-TRIGCCE.02'
  @@route2 = 'UTRT-TRIGCCE.02'
  @@triggerpd2 = 'FOUTCERT.01'

  @@gg_repl_timeout = 20
  @@wait_timeout = 20
  @@testlots = []


  def self.after_all_passed
    @@testlots.each {|lot| @@sv.delete_lot_family(lot)}
  end

  def setup
    @@tcc.mq_request.delete_msgs(nil) if self.class.class_variable_defined?(:@@tcc)
  end


  def test00_setup
    $setup_ok = false
    #
    @@tcc = CCE::TrigCCE.new($env, sv: @@sv)
    assert_equal 0, @@tcc.mq_response.qdepth, "jCAP job not listening on queue #{@@tcc.mq_response.inspect}"
    #
    $setup_ok = true
  end

  def test11_single_lot_at_PD
    $setup_ok = false
    #
    assert lots = @@svtest.new_lots(2), 'error creating test lots'
    @@testlots += lots
    lots.each {|lot|
      # find op after trigger pd
      ops = @@sv.lot_operation_list(lot)
      idx = ops.index {|e| e.op == @@triggerpd}
      nextop = ops[idx + 1].op
      # move to trigger PD and read CCE request
      assert_equal 0, @@sv.lot_opelocate(lot, nil, op: @@triggerpd)
      assert @@tcc.verify_trigger(lot)
      # verify hold
      assert_equal [@@triggerhold], @@sv.lot_hold_list(lot).collect {|h| h.reason}, 'wrong hold'
      # send response
      sleep @@gg_repl_timeout
      assert @@tcc.send_cce_response(lot, 'Success')
      # lot has moved on
      $log.info 'verify lot has moved on'
      sleep @@wait_timeout
      li = @@sv.lot_info(lot, wafers: true)
      assert_equal nextop, li.op
      assert_equal 'Waiting', li.status
      $log.info 'verify wafer script parameters'  # 800 + slot is hard coded in @@tcc
      li.wafers.each {|w|
        assert_equal 800 + w.slot, @@sv.user_parameter('Wafer', w.wafer, name: 'OQAGoodDie').value.to_i, 'wrong OQAGoodDie'
      }
    }
    #
    $setup_ok = true
  end

  def test12_lot_already_onhold_at_PD
    assert lot = @@svtest.new_lot, 'error creating test lot'
    @@testlots << lot
    assert_equal 0, @@sv.lot_hold(lot, @@errorhold)
    assert_equal 0, @@sv.lot_opelocate(lot, nil, op: @@triggerpd, force: true)
    assert_nil @@tcc.mq_request.receive_msg(timeout: @@tcc.timeout), 'unexpected message'
    # verify hold
    assert_equal [@@errorhold], @@sv.lot_hold_list(lot).collect {|h| h.reason}, 'wrong hold'
  end

  def test13_two_lots_at_PD
    assert lots = @@svtest.new_lots(2), 'error creating test lots'
    @@testlots += lots
    lots.each {|lot| assert_equal 0, @@sv.lot_opelocate(lot, nil, op: @@triggerpd)}
    #
    msgs = @@tcc.mq_request.receive_msgs(nmax: 2, timeout: @@tcc.timeout)
    assert_equal 2, msgs.size
    lots.each {|lot|
      mm = msgs.select {|m| m.index(lot)}.compact
      assert_equal 1, mm.size, "no unique message for lot #{lot}"
      assert @@tcc.verify_trigger(lot, mm.first)
      # verify hold
      assert_equal [@@triggerhold], @@sv.lot_hold_list(lot).collect {|h| h.reason}, 'wrong hold'
    }
  end

  def test15_lot_not_at_PD
    assert lot = @@svtest.new_lot, 'error creating test lot'
    @@testlots << lot
    old_opNo = @@sv.lot_info(lot).opNo
    # send CCE response, lot not at trigger PD
    sleep @@gg_repl_timeout
    assert @@tcc.send_cce_response(lot, 'Success')
    # lot has not moved on and is not ONHOLD
    sleep @@wait_timeout
    li = @@sv.lot_info(lot)
    assert_equal old_opNo, li.opNo
    assert_equal 'Waiting', li.status
  end

  def test16_lot_not_at_PD_onhold_XM4
    assert lot = @@svtest.new_lot, 'error creating test lot'
    @@testlots << lot
    old_opNo = @@sv.lot_info(lot).opNo
    # set trigger hold, not at trigger PD
    assert_equal 0, @@sv.lot_hold(lot, @@triggerhold, user: @@jcapuser)
    # send CCE response
    sleep @@gg_repl_timeout
    assert @@tcc.send_cce_response(lot, 'Success')
    # lot has not moved on and is ONHOLD
    sleep @@wait_timeout
    li = @@sv.lot_info(lot)
    assert_equal old_opNo, li.opNo
    refute_empty @@sv.lot_hold_list(lot, reason: @@triggerhold), 'lot is not ONHOLD'
  end

  def test17_lot_not_at_PD_onhold_ENG
    assert lot = @@svtest.new_lot, 'error creating test lot'
    @@testlots << lot
    old_opNo = @@sv.lot_info(lot).opNo
    # set CCE hold, not at trigger PD
    assert_equal 0, @@sv.lot_hold(lot, @@errorhold)
    # send CCE response
    sleep @@gg_repl_timeout
    assert @@tcc.send_cce_response(lot, 'Success')
    # lot has not moved on and is ONHOLD with error reason
    sleep @@wait_timeout
    assert_equal old_opNo, @@sv.lot_info(lot).opNo
    assert_equal [@@errorhold], @@sv.lot_hold_list(lot).collect {|h| h.reason}, 'wrong hold'
  end

  def test18_lot_nonprobankin
    assert lot = @@svtest.new_lot, 'error creating test lot'
    @@testlots << lot
    old_opNo = @@sv.lot_info(lot).opNo
    # NonProBankIn
    assert_equal 0, @@sv.lot_nonprobankin(lot, @@nonprobank)
    # send CCE response
    sleep @@gg_repl_timeout
    assert @@tcc.send_cce_response(lot, 'Success')
    # lot has not moved on and is not ONHOLD
    $log.info "verify lot status"
    sleep @@wait_timeout
    li = @@sv.lot_info(lot)
    assert_equal old_opNo, li.opNo
    assert_equal 'NonProBank', li.status
    assert_empty @@sv.lot_hold_list(lot, type: 'LotHold'), 'wrong hold'
  end

  def test21_response_unrelated_then_reject
    assert lot = @@svtest.new_lot, 'error creating test lot'
    @@testlots << lot
    # move to trigger PD and read CCE request
    assert_equal 0, @@sv.lot_opelocate(lot, nil, op: @@triggerpd)
    assert @@tcc.verify_trigger(lot)
    # verify hold
    assert_equal [@@triggerhold], @@sv.lot_hold_list(lot).collect {|h| h.reason}, 'wrong hold'
    # send CCE response for an unrelated lot
    sleep @@gg_repl_timeout
    assert @@tcc.send_cce_response('XXXX.00', 'Success', nowafers: true)
    # lot has not moved and is still on hold
    sleep @@wait_timeout * 2
    assert_equal [@@triggerhold], @@sv.lot_hold_list(lot).collect {|h| h.reason}, 'wrong hold'
    assert_equal @@triggerpd, @@sv.lot_info(lot).op
    # send CCE reject response
    assert @@tcc.send_cce_response(lot, 'Reject')
    # lot has not moved and a DQE hold
    sleep @@wait_timeout
    assert_equal [@@rejecthold], @@sv.lot_hold_list(lot).collect {|h| h.reason}, 'wrong hold'
    assert_equal @@triggerpd, @@sv.lot_info(lot).op
  end

  def test22_response_rework
    assert lot = @@svtest.new_lot, 'error creating test lot'
    @@testlots << lot
    # move to trigger PD and read CCE request
    assert_equal 0, @@sv.lot_opelocate(lot, nil, op: @@triggerpd)
    assert @@tcc.verify_trigger(lot)
    # verify hold
    assert_equal [@@triggerhold], @@sv.lot_hold_list(lot).collect {|h| h.reason}, 'wrong hold'
    # send CCE rework response
    sleep @@gg_repl_timeout
    assert @@tcc.send_cce_response(lot, 'Rework')
    # lot has not moved and RWK hold
    sleep @@wait_timeout
    assert_equal [@@reworkhold], @@sv.lot_hold_list(lot).collect {|h| h.reason}, 'wrong hold'
    assert_equal @@triggerpd, @@sv.lot_info(lot).op
  end

  def test23_response_failed
    assert lot = @@svtest.new_lot, 'error creating test lot'
    @@testlots << lot
    # move to trigger PD and read CCE request
    assert_equal 0, @@sv.lot_opelocate(lot, nil, op: @@triggerpd)
    assert @@tcc.verify_trigger(lot)
    # verify hold
    assert_equal [@@triggerhold], @@sv.lot_hold_list(lot).collect {|h| h.reason}, 'wrong hold'
    # send CCE Failed response
    sleep @@gg_repl_timeout
    assert @@tcc.send_cce_response(lot, 'Failed')
    # lot has not moved and ENG hold
    sleep @@wait_timeout
    assert_equal [@@errorhold], @@sv.lot_hold_list(lot).collect {|h| h.reason}, 'wrong hold'
    assert_equal @@triggerpd, @@sv.lot_info(lot).op
  end

  # EI not yet working as of 2021-07-27, sf
  def testman61_lot_for_CECEILE
    assert lots = @@svtest.new_lots(5, route: @@route2, product: @@product2)
    lots.each {|lot|
      assert_equal 0, @@sv.lot_opelocate(lot, nil, op: @@triggerpd2)  # move to trigger PD
    }
    $log.info "\n--> test lots: #{lots}"
  end

end
