=begin 
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.
    
Author: Steffen Steidten, 2011-07-06

Version: 19.01

History:
  2015-04-02 ssteidte, cleanup
  2018-03-06 sfrieske, replaced user_parameter_delete_verify by user_parameter_delete
  2019-01-02 sfrieske, minor cleanup
  2020-08-13 sfrieske, minor cleanup
=end

require 'RubyTestCase'


class JCAP_InhibitReticle < RubyTestCase
  @@reticle = '7280AV041A1'
  @@pod_ok = 'R249'
  @@pod_bad = 'R2D9'
  @@eqp = 'ALC300'    # for reticle transfer status, not used directly
  @@reason = 'X-RD'
  @@owner = 'X-JRETIH'

  @@job_delay = 1830
  
  
  def setup
    # reticle cleanup
    @@sv.inhibit_cancel(:reticle, @@reticle)
    @@sv.inhibit_cancel(:reticle_eqp, @@reticle)
    pod = @@sv.reticle_status(@@reticle).reticleStatusInfo.reticlePodID.identifier
    @@sv.reticle_justinout(@@reticle, pod, 1, 'out') unless pod.empty?
  end


  def test00_setup
    $setup_ok = false
    #
    # reticle pod setup
    [@@pod_ok, @@pod_bad].each {|pod|
      pi = @@sv.reticle_pod_list(pod: pod).first.reticlePodStatusInfo
      assert_equal 0, @@sv.reticle_pod_xfer_status_change(pod, 'EO', eqp: @@eqp) if pi.transferStatus != 'EO'
      # empty the pods
      pi.strContainedReticleInfo.each {|r| @@sv.reticle_justinout(r.reticleID.identifier, pod, 1, 'out')}
    }
    #
    $setup_ok = true
  end
  
  def test11_reticle_in_wrong_pod
    $setup_ok = false
    #
    assert_empty @@sv.inhibit_list(:reticle, @@reticle)
    assert_empty @@sv.inhibit_list(:reticle_eqp, @@reticle)    
    #
    # put the reticle in the wrong pod
    assert_equal 0, @@sv.user_parameter_change('Reticle', @@reticle, 'PodID', @@pod_ok)
    assert_equal 0, @@sv.reticle_justinout(@@reticle, @@pod_bad, 1, 'in')
    #
    # wait for the jCAP job and check the inhibit
    $log.info "waiting #{@@job_delay} s for the jCAP job to run"; sleep @@job_delay
    check_inhibit
    #
    # wait for the jCAP job to run again and check that there is only 1 inhibit
    $log.info "waiting #{@@job_delay} s for the jCAP job to run again"; sleep @@job_delay
    # only one inhibit from jCAP with same conditions and properties is allowed
    inhs = @@sv.inhibit_list(:reticle, @@reticle)
    assert_equal 1, inhs.size, "multiple inhibits on reticle #{@@reticle}"
    check_inhibit
    #
    $setup_ok = true
  end
  
  # MSR 631347: handling of reticle_eqp inhibits
  def test12_wrong_reticle_in_pod_inhibit_with_existing_reticle_and_EQ
    assert_empty @@sv.inhibit_list(:reticle, @@reticle)
    assert_empty @@sv.inhibit_list(:reticle_eqp, @@reticle)    
    #
    # put the reticle in the wrong pod and set a reticle_eqp inhibit
    assert_equal 0, @@sv.user_parameter_change('Reticle', @@reticle, 'PodID', @@pod_bad)
    assert_equal 0, @@sv.reticle_justinout(@@reticle, @@pod_ok, 1, 'in')
    assert @@sv.inhibit_entity(:reticle_eqp, [@@reticle, @@eqp]), 'error setting inhibit'
    #
    # wait for the jCAP job and check the inhibit
    $log.info "waiting #{@@job_delay} s for the jCAP job to run"; sleep @@job_delay
    check_inhibit
  end
  
  
  def check_inhibit
    assert inh = @@sv.inhibit_list(:reticle, @@reticle).first, "reticle #{@@reticle} must be inhibited"
    assert_equal @@reason, inh.reason, 'wrong inhibit reason'
    assert_equal @@owner, inh.owner, "wrong inhibit owner on reticle #{@@reticle}"
    assert inh.memo.start_with?('Dedicated Pod:'), 'wrong claim memo'
  end

end
