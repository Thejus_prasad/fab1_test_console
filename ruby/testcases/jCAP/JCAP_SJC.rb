=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.


Author:  Steffen Steidten, 2012-03-19

Version: 21.04.2

History:
  2013-01-14 ssteidte, adapted to changes in rtdqarules.rb
  2014-09-08 ssteidte, use RTDEmuExt instead of RTD QA Rule files
  2015-04-07 ssteidte, cleanup
  2017-01-05 sfrieske, use new RTD::EmuRemote, minor cleanup
  2017-04-27 sfrieske, minor fixes and validation of changes to sjc.rb and sjctest.rb
  2018-09-25 sfrieske, removed QATestAdapter
  2018-11-07 sfrieske, minor cleanup
  2019-02-19 sfrieske, improved EmuRemote usage
  2019-09-23 sfrieske, added tests for version 19.08
  2020-01-07 sfrieske, removed MDS#sjc_requests_carrier, added verify_sr_removed
  2021-04-21 sfrieske, moved @sorter and @pg form SiView::Test to SJC::Test
  2021-08-27 sfrieske, require uniquestring instead of misc

Notes:
  see http://f1onewiki:21080/display/JCAP/SortJobController
=end

require 'jcap/sjctest'
require 'rtdserver/rtdemuremote'
require 'util/uniquestring'
require 'util/waitfor'
require 'RubyTestCase'


# Test the jCAP Sort Job Controller
class JCAP_SJC < RubyTestCase
  @@sv_defaults = {}
  @@carriers = 'JC%'  # with disabled usage check
  @@sorter = 'UTS001'
  @@pg = 'PG1'
  @@pg2 = 'PG2'
  @@sorter_cats = 'UTS005'
  @@pg_cats = 'PG1'
  @@requestor = 'RTD_QA'  # creator of the RTD sort requests
  @@empty_rule = 'EMPTY'  # as configured in sjc.properties for sjc.rtd.empty.rule
  @@columns = %w(cast_id cast_category stk_id)  # SJC uses the first column w/o checking the name
  # age of not handled sort request (MDS status UNPROCESSED) before they get automatically deleted
  @@expiration_unhandled_sr = 1800
  # age of not completed (MDS status Pending, SiView status Executing) SiView sorter jobs before deletion
  @@expiration_executing_sj = 1800
  # time interval for starting of lifecyclecheck
  @@lifecyclecheck_interval = 300

  @@use_notifications = true
  @@mds_sync = 45


  def self.shutdown
    @@sjctest.cleanup_eqp(@@sorter)
    @@rtdremote.delete_emustation(@@empty_rule)
  end

  def self.after_all_passed
    @@testlots.each {|lot| @@sv.delete_lot_family(lot)}
  end

  def teardown
    @@rtdremote.delete_emustation(@@empty_rule)
  end


  def test00_setup
    $setup_ok = false
    #
    @@sjctest = SJC::Test.new($env, @@sv_defaults.merge(sv: @@sv, carriers: @@carriers,
      sorter: @@sorter, pg: @@pg, notifications: @@use_notifications))
    @@sjcrequest = @@sjctest.sjcrequest
    @@mds = @@sjctest.mds
    @@rtdremote = RTD::EmuRemote.new($env)
    #
    # prepare 3 lots, ensure lot0 has 3 children
    assert @@testlots = @@sjctest.new_lots(2), 'error creating test lots'
    @@lot0, @@lot1 = @@testlots
    3.times {assert @@sv.lot_split(@@lot0, 2)}
    #
    # get 6 empty carriers
    assert ecs = @@sjctest.get_empty_carrier(n: 6), 'not enough empty carriers'
    @@carrier0, @@carrier1, @@carrier2, @@carrier3, @@carrier4, @@carrier5 = ecs
    $log.info "using carriers #{ecs}"
    #
    $setup_ok = true
  end

  def test11_combine_1_1
    $setup_ok = false
    #
    assert @@sjctest.prepare_carrier(@@carrier1, 'empty')
    assert @@sjctest.prepare_carrier(@@carrier0, @@lot0)
    assert @@sjctest.cleanup_eqp(@@sorter, mode: 'Auto-3'), 'error cleaning up sorter'
    # create COMBINE request
    $log.info "waiting #{@@mds_sync} s for the MDS to sync"; sleep @@mds_sync
    assert @@sjctest.create_combine_verify(@@carrier0, @@carrier1), 'error in COMBINE request'
    # send create message again to verify no 2nd sort request is created
    assert @@sjctest.create_combine_verify(@@carrier0, @@carrier1), 'there is a 2nd COMBINE request'
    assert sjid = @@sjctest.execute_verify, 'error executing SR'
    # execute the sorter job
    tstart = Time.now
    assert @@sv.claim_sj_execute(sjid: sjid)
    assert_equal @@carrier1, @@sv.lot_info(@@lot0).carrier, "wrong carrier for lot #{@@lot0}"
    # verify lots are in the new carriers and have an operation note
    assert @@sjctest.verify_lot_openote('COMBINE', @@lot0, @@carrier0, @@carrier1, tstart: tstart)
    # verify SR entry is removed from T_SJC_SORT_REQUEST
    assert @@sjctest.verify_sr_removed, 'SR not removed from MDS table'
    #
    $setup_ok = true
  end

  def test12_combine_N_1
    assert @@sjctest.cleanup_eqp(@@sorter, mode: 'Auto-3'), 'error cleaning up sorter'
    # prepare 4 lots in 4 carriers, will be combined into 1
    assert @@sjctest.prepare_carrier(@@carrier0, @@lot0)
    lots = @@sv.lot_family(@@lot0).take(3)
    srccarriers = [@@carrier1, @@carrier2, @@carrier3]
    srccarriers.each_with_index {|c, i|
      assert @@sjctest.prepare_carrier(c, 'empty')
      assert_equal 0, @@sv.wafer_sort(lots[i], c)
    }
    # create COMBINE request
    $log.info "waiting #{@@mds_sync} s for the MDS to sync"; sleep @@mds_sync
    assert @@sjctest.create_combine_verify(srccarriers, @@carrier0), 'error in COMBINE request'
    # send create message again to verify no 2nd sort request is created
    assert @@sjctest.create_combine_verify(srccarriers, @@carrier0), 'there is a 2nd COMBINE request'
    assert sjid = @@sjctest.execute_verify, 'error executing SR'
    # execute the sorter job
    tstart = Time.now
    assert @@sv.claim_sj_execute(sjid: sjid)
    # verify lots are in the new carriers and have an operation note
    lots.each_with_index {|lot, i|
      li = @@sv.lot_info(lot)
      assert_equal @@carrier0, li.carrier, "wrong carrier #{li.carrier} for lot #{lot}"
      assert @@sjctest.verify_lot_openote('COMBINE', lot, srccarriers[i], @@carrier0, tstart: tstart)
    }
    # verify SR entry is removed from T_SJC_SORT_REQUEST
    assert @@sjctest.verify_sr_removed, 'SR not removed from MDS table'
  end

  def test13_combine_N_1_EMPTY
    assert @@sjctest.cleanup_eqp(@@sorter, mode: 'Auto-3'), 'error cleaning up sorter'
    assert @@sv.lot_family(@@lot0).size > 1
    assert @@sjctest.prepare_carrier(@@carrier0, @@lot0)
    assert @@sjctest.prepare_carrier(@@carrier1, 'empty')
    assert_equal 0, @@sv.wafer_sort(@@lot0, @@carrier1)
    # accept tgt carrier EMPTY
    assert @@sjctest.cleanup_sort_requests(@@carrier0)
    assert @@sjctest.create_combine_verify([@@carrier0, @@carrier1], 'EMPTY')
    # accept tgt carrier EMPTY.1
    assert @@sjctest.cleanup_sort_requests(@@carrier0)
    assert @@sjctest.create_combine_verify([@@carrier0, @@carrier1], 'EMPTY.1')
  end

  def test14_2lots_tgtslots
    assert @@sjctest.cleanup_eqp(@@sorter, mode: 'Auto-3'), 'error cleaning up sorter'
    assert @@sjctest.prepare_carrier(@@carrier1, 'empty')
    assert @@sjctest.prepare_carrier(@@carrier0, @@lot0)
    lf = @@sv.lot_family(@@lot0)
    assert lf.size > 2, 'not enough lots'
    # move 1 lot in the target carrier
    assert_equal 0, @@sv.wafer_sort(lf[2], @@carrier1, slots: [1, 3]), 'wafer_sort error'
    # create COMBINE request for 2 lots
    $log.info "waiting #{@@mds_sync} s for the MDS to sync"; sleep @@mds_sync
    assert @@sjctest.create_combine_verify([@@carrier0, @@carrier0], @@carrier1, lots: lf.take(2)), 'error in COMBINE request'
    assert sjid = @@sjctest.execute_verify, 'error executing SR'
    # execute the sorter job
    tstart = Time.now
    assert @@sv.claim_sj_execute(sjid: sjid)
    # verify lots are in the new carriers and have an operation note
    lf.take(2).each_with_index {|lot, i|
      li = @@sv.lot_info(lot)
      assert_equal @@carrier1, li.carrier, "wrong carrier #{li.carrier} for lot #{lot}"
      assert @@sjctest.verify_lot_openote('COMBINE', lot, @@carrier0, @@carrier1, tstart: tstart)
    }
    # verify SR entry is removed from T_SJC_SORT_REQUEST
    assert @@sjctest.verify_sr_removed, 'SR not removed from MDS table'
  end

  def test16_combine_1lot_byslotmap   # like test11 with specified slot map, MSR 1379721
    assert @@sjctest.cleanup_eqp(@@sorter, mode: 'Auto-3'), 'error cleaning up sorter'
    assert @@sjctest.prepare_carrier(@@carrier1, 'empty')
    assert @@sjctest.prepare_carrier(@@carrier0, @@lot0)
    # create slotmap for 1 lot
    li = @@sv.lot_info(@@lot0, wafers: true)
    tgtslots = (1..25).to_a.shuffle
    slotmap = li.wafers.each_with_index.collect {|w, idx|
      {lot: w.lot, wafer: w.wafer, srccarrier: @@carrier0, tgtcarrier: @@carrier1, tgtslot: tgtslots[idx]}
    }
    $log.info "waiting #{@@mds_sync} s for the MDS to sync"; sleep @@mds_sync
    assert @@sjctest.create_byslotmap_verify('COMBINE', slotmap)
  end

  def test17_combine_2lots_byslotmap  # like test 16 with 2 lots
    assert @@sjctest.cleanup_eqp(@@sorter, mode: 'Auto-3'), 'error cleaning up sorter'
    assert @@sjctest.prepare_carrier(@@carrier1, 'empty')
    assert @@sjctest.prepare_carrier(@@carrier0, @@lot0)
    lf = @@sv.lot_family(@@lot0)
    assert lf.size > 3, 'not enough lots'
    lots = lf[1, 2]
    assert @@sjctest.prepare_carrier(@@carrier2, 'empty')
    assert_equal 0, @@sv.wafer_sort(lots[1], @@carrier2)
    # create slotmap for 2 lots
    lis = @@sv.lots_info(lots, wafers: true)
    tgtslots = (1..25).to_a.shuffle
    slotmap = []
    idx = 0
    lis.each {|li|
      li.wafers.each {|w|
        slotmap << {lot: w.lot, wafer: w.wafer, srccarrier: li.carrier, tgtcarrier: @@carrier1, tgtslot: tgtslots[idx]}
        idx += 1
      }
    }
    $log.info "waiting #{@@mds_sync} s for the MDS to sync"; sleep @@mds_sync
    assert @@sjctest.create_byslotmap_verify('COMBINE', slotmap)
  end

  def test18_combine_N_1_cats  # like test12 but different srccarrier categories, MSR 1304010
    # ensure sorter setup is correct
    assert_equal 'FEOL', @@sv.carrier_status(@@carrier1).category, 'wrong src carrier category'
    ports = @@sv.eqp_info(@@sorter_cats).ports.select {|p| p.pg == @@pg_cats}
    assert_equal 2, ports.size, 'wrong number of ports in PG1'
    # TODO: more checks on port categories ??
    assert @@sjctest.cleanup_eqp(@@sorter_cats, mode: 'Auto-3'), 'error cleaning up sorter'
    # prepare lots and carriers
    assert @@sjctest.prepare_carrier(@@carrier0, @@lot0)
    assert carrier2 = @@sjctest.get_empty_carrier(carrier_category: 'BEOL'), 'no empty BEOL carrier'
    assert @@sjctest.prepare_carrier(carrier2, 'empty')
    assert_equal 0, @@sv.wafer_sort(@@lot0, carrier2)
    assert carrier3 = @@sjctest.get_empty_carrier(carrier_category: 'MOL'), 'no empty MOL carrier'
    assert @@sjctest.prepare_carrier(carrier3, 'empty')
    srccarriers = [@@carrier0, carrier2]
    lots = srccarriers.collect {|c| @@sv.lot_list_incassette(c).first}
    # send create COMBINE and execute requests
    $log.info "waiting #{@@mds_sync} s for the MDS to sync"; sleep @@mds_sync
    assert @@sjctest.create_combine_verify([@@carrier0, carrier2], carrier3), 'error in COMBINE request'
    assert sjid = @@sjctest.execute_verify(sorter: @@sorter_cats), 'error executing SR'
    # execute the sorter job
    tstart = Time.now
    assert @@sv.claim_sj_execute(sjid: sjid)
    # verify lots are in the new carrier and have an operation note
    lots.each_with_index {|lot, i|
      li = @@sv.lot_info(lot)
      assert_equal carrier3, li.carrier, "wrong carrier #{li.carrier} for lot #{lot}"
      assert @@sjctest.verify_lot_openote('COMBINE', lot, srccarriers[i], carrier3, sorter: @@sorter_cats, tstart: tstart)
    }
  end

  def test21_separate_1_1
    assert @@sjctest.cleanup_eqp(@@sorter, mode: 'Auto-3'), 'error cleaning up sorter'
    assert @@sjctest.prepare_carrier(@@carrier1, 'empty')
    assert @@sjctest.prepare_carrier(@@carrier0, @@lot0)
    lot = @@lot0
    # create SEPARATE request
    $log.info "waiting #{@@mds_sync} s for the MDS to sync"; sleep @@mds_sync
    assert @@sjctest.create_separate_verify(@@carrier0, @@carrier1), 'error in SEPARATE request'
    # send create message again to verify no 2nd sort request is created
    assert @@sjctest.create_separate_verify(@@carrier0, @@carrier1), 'error in SEPARATE request'
    # send execute message
    assert sjid = @@sjctest.execute_verify, 'error executing SR'
    # execute the sorter job
    tstart = Time.now
    assert @@sv.claim_sj_execute(sjid: sjid)
    # verify lot is in the new carrier and has a correct openote
    li = @@sv.lot_info(lot)
    assert_equal @@carrier1, li.carrier, "wrong carrier #{li.carrier} for lot #{lot}"
    assert @@sjctest.verify_lot_openote('SEPARATE', lot, @@carrier0, @@carrier1, tstart: tstart)
    # verify SR entry is removed from T_SJC_SORT_REQUEST
    assert @@sjctest.verify_sr_removed, 'SR not removed from MDS table'
  end

  def test22_separate_1_N
    assert @@sjctest.cleanup_eqp(@@sorter, mode: 'Auto-3'), 'error cleaning up sorter'
    tgtcarriers = [@@carrier1, @@carrier2, @@carrier3]
    tgtcarriers.each {|c| assert @@sjctest.prepare_carrier(c, 'empty')}
    assert @@sjctest.prepare_carrier(@@carrier0, @@lot0)
    lots = @@sv.lot_list_incassette(@@carrier0).take(tgtcarriers.size)
    # create SEPARATE and execute requests
    $log.info "waiting #{@@mds_sync} s for the MDS to sync"; sleep @@mds_sync
    assert @@sjctest.create_separate_verify(@@carrier0, tgtcarriers), 'error in SEPARATE request'
    assert sjid = @@sjctest.execute_verify, 'error executing SR'
    # execute the sorter job
    tstart = Time.now
    assert @@sv.claim_sj_execute(sjid: sjid)
    # verify lots are in the new carriers and have an operation note
    lots.each_with_index {|lot, i|
      li = @@sv.lot_info(lot)
      assert_equal tgtcarriers[i], li.carrier, "wrong carrier #{li.carrier} for lot #{lot}"
      assert @@sjctest.verify_lot_openote('SEPARATE', lot, @@carrier0, tgtcarriers[i], tstart: tstart)
    }
    # verify SR entry is removed from T_SJC_SORT_REQUEST
    assert @@sjctest.verify_sr_removed, 'SR not removed from MDS table'
  end

  def test23_separate_1_N_EMPTY
    assert @@sjctest.cleanup_eqp(@@sorter, mode: 'Auto-3'), 'error cleaning up sorter'
    assert @@sjctest.prepare_carrier(@@carrier0, @@lot0)
    # individual EMPTY.x carriers are accepted
    assert @@sjctest.create_separate_verify(@@carrier0, ['EMPTY.1', 'EMPTY.2', 'EMPTY.3'])
    # multiple EMPTY.1 carriers are unchanged (like transfer without order)
    assert @@sjctest.cleanup_sort_requests(@@carrier0)
    assert @@sjctest.create_separate_verify(@@carrier0, ['EMPTY.1', 'EMPTY.1', 'EMPTY.1'])
    # multiple EMPTY carriers are converted into EMPTY.1, EMPTY.2 and EMPTY.3
    assert @@sjctest.cleanup_sort_requests(@@carrier0)
    assert @@sjctest.create_separate_verify(@@carrier0, ['EMPTY', 'EMPTY', 'EMPTY'], st_carriers: ['EMPTY.1', 'EMPTY.2', 'EMPTY.3'])
  end

  def test26_separate_1lot_byslotmap # like test21 with specified slot map, MSR 1379721
    assert @@sjctest.cleanup_eqp(@@sorter, mode: 'Auto-3'), 'error cleaning up sorter'
    assert @@sjctest.prepare_carrier(@@carrier1, 'empty')
    assert @@sjctest.prepare_carrier(@@carrier0, @@lot0)
    # create slotmap for 1 lot
    li = @@sv.lot_info(@@lot0, wafers: true)
    tgtslots = (1..25).to_a.shuffle
    slotmap = li.wafers.each_with_index.collect {|w, idx|
      {lot: w.lot, wafer: w.wafer, srccarrier: @@carrier0, tgtcarrier: @@carrier1, tgtslot: tgtslots[idx]}
    }
    $log.info "waiting #{@@mds_sync} s for the MDS to sync"; sleep @@mds_sync
    assert @@sjctest.create_byslotmap_verify('SEPARATE', slotmap)
  end

  def test27_separate_2lots_byslotmap
    assert @@sjctest.cleanup_eqp(@@sorter, mode: 'Auto-3'), 'error cleaning up sorter'
    tgtcarriers = [@@carrier1, @@carrier2]
    tgtcarriers.each {|carrier|
      assert @@sjctest.prepare_carrier(carrier, 'empty')
    }
    assert @@sjctest.prepare_carrier(@@carrier0, @@lot0)
    lf = @@sv.lot_family(@@lot0)
    assert lf.size > 3, 'not enough lots'
    lots = lf[1, 2]
    # create slotmap for 2 lots
    lis = @@sv.lots_info(lots, wafers: true)
    tgtslots = (1..25).to_a.shuffle
    slotmap = []
    idx = 0
    lis.each_with_index {|li, i|
      li.wafers.each {|w|
        slotmap << {lot: w.lot, wafer: w.wafer, srccarrier: @@carrier0, tgtcarrier: tgtcarriers[i], tgtslot: tgtslots[idx]}
        idx += 1
      }
    }
    $log.info "waiting #{@@mds_sync} s for the MDS to sync"; sleep @@mds_sync
    assert @@sjctest.create_byslotmap_verify('SEPARATE', slotmap)
  end

  def test28_separate_1_N_cats    # like test22 but different tgtcarrier categories, MSR 1304010
    # ensure sorter setup is correct
    assert_equal 'FEOL', @@sv.carrier_status(@@carrier0).category, 'wrong src carrier category'
    ports = @@sv.eqp_info(@@sorter_cats).ports.select {|p| p.pg == @@pg_cats}
    assert_equal 2, ports.size, 'wrong number of ports in PG1'
    # TODO: more checks on port categories ??
    assert @@sjctest.cleanup_eqp(@@sorter_cats, mode: 'Auto-3'), 'error cleaning up sorter'
    # prepare lots and carriers
    assert carrier2 = @@sjctest.get_empty_carrier(carrier_category: 'BEOL'), 'no empty BEOL carrier'
    assert carrier3 = @@sjctest.get_empty_carrier(carrier_category: 'MOL'), 'no empty MOL carrier'
    tgtcarriers = [@@carrier0, carrier2]
    tgtcarriers.each {|c| assert @@sjctest.prepare_carrier(c, 'empty')}
    assert @@sjctest.prepare_carrier(carrier3, @@lot0)
    lots = @@sv.lot_list_incassette(carrier3).take(tgtcarriers.size)
    # send create SEPARATE and execute requests
    $log.info "waiting #{@@mds_sync} s for the MDS to sync"; sleep @@mds_sync
    assert @@sjctest.create_separate_verify(carrier3, tgtcarriers), 'error in SEPARATE request'
    assert sjid = @@sjctest.execute_verify(sorter: @@sorter_cats), 'error executing SR'
    # execute the sorter job
    tstart = Time.now
    assert @@sv.claim_sj_execute(sjid: sjid)
    # verify lots are in the new carrier and have an operation note
    lots.each_with_index {|lot, i|
      li = @@sv.lot_info(lot)
      assert_equal tgtcarriers[i], li.carrier, "wrong carrier #{li.carrier} for lot #{lot}"
      assert @@sjctest.verify_lot_openote('SEPARATE', lot, carrier3, tgtcarriers[i], sorter: @@sorter_cats, tstart: tstart)
    }
  end


  def test31_transfer_full_lot
    assert @@sjctest.cleanup_eqp(@@sorter, mode: 'Auto-3', portstatus: 'UnloadReq'), 'error cleaning up sorter'
    assert @@sjctest.prepare_carrier(@@carrier1, 'empty')
    assert @@sjctest.prepare_carrier(@@carrier0,  @@lot1)  # use the full lot
    li_old = @@sv.lot_info(@@lot1, wafers: true)
    assert_equal 25, li_old.wafers.size, 'lot must have 25 wafers'
    # send create TRANSFER and execute requests
    $log.info "waiting #{@@mds_sync} s for the MDS to sync"; sleep @@mds_sync
    assert @@sjctest.create_transfer_verify(@@carrier0, @@carrier1), 'error in TRANSFER request'
    9.times {
      assert @@sjcrequest.execute(@@sjctest.sr.srid, @@sorter), 'error sending execute message'
      sleep 10
    }
    assert sjid = @@sjctest.execute_verify, 'error executing SR'
    # execute the sorter job
    tstart = Time.now
    assert @@sv.claim_sj_execute(sjid: sjid, report: false)
    # verify lots are in the new carriers and have an operation note
    li = @@sv.lot_info(@@lot1, wafers: true)
    assert_equal li.wafers, li_old.wafers, "error in wafer slot map\nold: #{li_old.wafers.inspect}\nnew:#{li.wafers.inspect}"
    assert_equal @@carrier1, li.carrier, "wrong carrier #{li.carrier} for lot #{@@lot1}"
    assert @@sjctest.verify_lot_openote('TRANSFER', @@lot1, @@carrier0, @@carrier1, tstart: tstart)
    # verify SR entry is removed from T_SJC_SORT_REQUEST
    assert @@sjctest.verify_sr_removed, 'SR not removed from MDS table'
  end

  def test32_transfer_multiple_lots
    assert @@sjctest.cleanup_eqp(@@sorter, mode: 'Auto-3', portstatus: 'UnloadReq'), 'error cleaning up sorter'
    assert @@sjctest.prepare_carrier(@@carrier1, 'empty')
    assert @@sjctest.prepare_carrier(@@carrier0, @@lot0)
    li_olds = @@sv.lot_list_incassette(@@carrier0).collect {|l| @@sv.lot_info(l, wafers: true)}
    assert li_olds.size > 2, "not enough lots in carrier #{@@carrier0}"
    $log.info "waiting #{@@mds_sync} s for the MDS to sync"; sleep @@mds_sync
    # send TRANSFER and execute messages
    assert @@sjctest.create_transfer_verify(@@carrier0, @@carrier1), 'error in TRANSFER request'
    10.times {
      assert @@sjcrequest.execute(@@sjctest.sr.srid, @@sorter), 'error sending execute message'
      sleep 10
    }
    assert sjid = @@sjctest.execute_verify, 'error executing SR'
    # execute the sorter job
    tstart = Time.now
    assert @@sv.claim_sj_execute(sjid: sjid, report: false)
    # verify lots are in the new carrier and have an operation note
    li_olds.each {|li_old|
      lot = li_old.lot
      li = @@sv.lot_info(lot, wafers: true)
      assert li.wafers == li_old.wafers, "error in wafer slot map\nold: #{li_old.wafers.inspect}\nnew:#{li.wafers.inspect}"
      assert_equal @@carrier1, li.carrier, "wrong carrier #{li.carrier} for lot #{lot}"
      assert @@sjctest.verify_lot_openote('TRANSFER', lot, @@carrier0, @@carrier1, tstart: tstart)
    }
    # verify SR entry is removed from T_SJC_SORT_REQUEST
    assert @@sjctest.verify_sr_removed, 'SR not removed from MDS table'
  end

  def test33_execute_multiple_srs
    assert @@sjctest.cleanup_eqp(@@sorter, mode: 'Auto-3', portstatus: 'UnloadReq'), 'error cleaning up sorter'
    # prepare test lots
    assert @@sjctest.prepare_carrier(@@carrier0, @@lot0)
    assert @@sjctest.prepare_carrier(@@carrier1, @@lot1)
    # prepare 2 empty carriers
    ecs = [@@carrier2, @@carrier3]
    ecs.each {|c| assert @@sjctest.prepare_carrier(c, 'empty')}
    $log.info "waiting #{@@mds_sync} s for the MDS to sync"; sleep @@mds_sync
    # create multiple SRs
    assert @@sjctest.create_transfer_verify(@@carrier0, 'EMPTY'), 'error in TRANSFER request'
    srid1 = @@sjctest.sr.srid
    assert @@sjctest.create_transfer_verify(@@carrier1, 'EMPTY'), 'error in TRANSFER request'
    srid2 = @@sjctest.sr.srid
    # set RTD rule
    @@rtdremote.add_emustation(@@empty_rule) {@@rtdremote.create_response(@@columns, {'rows'=>ecs.collect {|c| [c, '', '']}})}
    # send 1 execute request for both SRs
    assert @@sjcrequest.execute([srid1, srid2], [@@sorter, @@sorter], [@@pg, @@pg2])
    # verify 2 sorter jobs have been created
    assert wait_for {@@sv.sj_list(eqp: @@sorter).size == 2}, 'wrong number of sorter jobs'
    [srid1, srid2].each {|srid| assert @@sjctest.mds.sjc_requests(srid: srid).first.sjid}
  end

  def test34_transfer_EMPTY
    assert @@sjctest.cleanup_eqp(@@sorter, mode: 'Auto-3', portstatus: 'UnloadReq'), 'error cleaning up sorter'
    assert @@sjctest.prepare_carrier(@@carrier0, @@lot0), "error cleaning up carrier #{@@carrier0}"
    assert @@sv.lot_list_incassette(@@carrier0).size > 2, "not enough lots in #{@@carrier0}"
    assert @@sjctest.create_transfer_verify(@@carrier0, 'EMPTY')
    assert @@sjctest.cleanup_sort_requests(@@carrier0)
    assert @@sjctest.create_transfer_verify(@@carrier0, 'EMPTY.1')
  end

  def test35_transfer_notcontiguous
    # similar to test32, with shuffled wafer slots
    assert @@sjctest.cleanup_eqp(@@sorter, mode: 'Auto-3', portstatus: 'UnloadReq'), 'error cleaning up sorter'
    assert @@sjctest.prepare_carrier(@@carrier1, 'empty')
    assert @@sjctest.prepare_carrier(@@carrier0, 'empty')
    assert lot = @@sjctest.new_lot(carrier: @@carrier0, nwafers: 9), 'error creating test lot'
    @@testlots << lot
    slots = (1..25).to_a.shuffle.take(9)
    assert_equal 0, @@sv.wafer_sort(lot, @@carrier0, slots: slots)
    2.times {assert @@sv.lot_split(lot, 3), 'error splitting lot'}
    li_olds = @@sv.lot_list_incassette(@@carrier0).collect {|lot| @@sv.lot_info(lot, wafers: true)}
    assert_equal 3, li_olds.size, "not enough lots in carrier #{@@carrier0}"
    $log.info "waiting #{@@mds_sync} s for the MDS to sync"; sleep @@mds_sync
    # send TRANSFER and execute messages
    assert @@sjctest.create_transfer_verify(@@carrier0, @@carrier1), 'error in TRANSFER request'
    assert sjid = @@sjctest.execute_verify, 'error executing SR'
    # execute the sorter job
    tstart = Time.now
    assert @@sv.claim_sj_execute(sjid: sjid, report: false)
    # verify lots are in the new carrier, in the original slot and have an operation note
    li_olds.each {|li_old|
      lot = li_old.lot
      li = @@sv.lot_info(lot, wafers: true)
      assert li.wafers == li_old.wafers, "error in wafer slot map\nold: #{li_old.wafers.inspect}\nnew:#{li.wafers.inspect}"
      assert_equal @@carrier1, li.carrier, "wrong carrier #{li.carrier} for lot #{lot}"
      assert @@sjctest.verify_lot_openote('TRANSFER', lot, @@carrier0, @@carrier1, tstart: tstart)
    }
    # verify SR entry is removed from T_SJC_SORT_REQUEST
    assert @@sjctest.verify_sr_removed, 'SR not removed from MDS table'
  end

  def test36_transfer_1lot_byslotmap # like test31 with specified slot map, MSR 1379721
    assert @@sjctest.cleanup_eqp(@@sorter, mode: 'Auto-3'), 'error cleaning up sorter'
    assert @@sjctest.prepare_carrier(@@carrier1, 'empty')
    assert @@sjctest.prepare_carrier(@@carrier0, @@lot1)  # use the full lot
    # create slotmap for 1 lot
    li = @@sv.lot_info(@@lot1, wafers: true)
    assert_equal 25, li.wafers.size, 'lot must have 25 wafers'
    tgtslots = (1..25).to_a.shuffle
    slotmap = li.wafers.each_with_index.collect {|w, idx|
      {lot: w.lot, wafer: w.wafer, srccarrier: @@carrier0, tgtcarrier: @@carrier1, tgtslot: tgtslots[idx]}
    }
    $log.info "waiting #{@@mds_sync} s for the MDS to sync"; sleep @@mds_sync
    assert @@sjctest.create_byslotmap_verify('TRANSFER', slotmap)
  end

  def test37_transfer_2lots_byslotmap
    assert @@sjctest.cleanup_eqp(@@sorter, mode: 'Auto-3'), 'error cleaning up sorter'
    assert @@sjctest.prepare_carrier(@@carrier1, 'empty')
    assert @@sjctest.prepare_carrier(@@carrier0, @@lot0)
    lf = @@sv.lot_family(@@lot0)
    assert lf.size > 3, 'not enough lots'
    lots = lf[1, 2]
    # create slotmap for 2 lots
    lis = @@sv.lots_info(lots, wafers: true)
    tgtslots = (1..25).to_a.shuffle
    slotmap = []
    idx = 0
    lis.each {|li|
      li.wafers.each {|w|
        slotmap << {lot: w.lot, wafer: w.wafer, srccarrier: @@carrier0, tgtcarrier: @@carrier1, tgtslot: tgtslots[idx]}
        idx += 1
      }
    }
    $log.info "waiting #{@@mds_sync} s for the MDS to sync"; sleep @@mds_sync
    assert @@sjctest.create_byslotmap_verify('TRANSFER', slotmap)
  end


  def test41_separate_reserved_tgtcarrier
    assert @@sjctest.cleanup_eqp(@@sorter, mode: 'Auto-3', portstatus: 'UnloadReq'), 'error cleaning up sorter'
    assert @@sjctest.prepare_carrier(@@carrier1, 'empty')
    assert @@sjctest.prepare_carrier(@@carrier0, @@lot0)
    $log.info "waiting #{@@mds_sync} s for the MDS to sync"; sleep @@mds_sync
    #
    # create and verify a sort request
    assert @@sjctest.create_separate_verify(@@carrier0, @@carrier1, requestor: @@requestor)
    srid = @@sjctest.sr.srid
    #
    # create a NPW reservation for target carrier to let execute requests fail
    assert_equal 0, @@sv.eqp_port_status_change(@@sorter, 'P1', 'LoadReq')
    assert_equal 0, @@sv.npw_reserve(@@sorter, 'P1', @@carrier1), 'error creating NPW reservation'
    sleep 10
    #
    # send execute message and verify status is still UNPROCESSED, retry count 1
    assert @@sjcrequest.execute(srid, @@sorter, @@pg)
    sleep 20
    assert sr = @@mds.sjc_requests(srid: srid).first, 'SR is gone'
    assert_equal 'UNPROCESSED', sr.status, 'wrong SR status'
    assert_equal 1, sr.retrycount, 'wrong SR retrycount'
    assert_nil sr.sjid, 'no SJ expected'
    # send 2 more execute messages so the retries are exhausted  # 20.7 unnecessary, LifeCycleCheck does it anyway ?
    2.times {
      assert @@sjcrequest.execute(srid, @@sorter, @@pg), 'error sending execute message'
      sleep 10
    }
    #
    # verify sort request has been moved to history and lot is ONHOLD with correct claim memo
    assert_empty @@mds.sjc_requests(srid: srid), "sr #{srid} not removed from sort request table"
    assert @@mds.sjc_requests(srid: srid, history: true).first, "sr #{srid} not found in sort request history"
    assert h = @@sv.lot_hold_list(@@lot0, reason: 'X-SE').first, "lot #{@@lot0} must be ONHOLD with reason X-SE"
    assert h.memo.start_with?("Could not separate lots: #{@@lot0}(#{@@carrier0}->#{@@carrier1})"), "wrong claim memo: #{h.memo}"
    assert h.memo.index("Requestor: #{@@requestor}"), "wrong claim memo (requestor): #{h.memo}"
    assert h.memo.index("Cassette dispatch state has already been reserved"), "wrong claim memo (reservation): #{h.memo}"
  end

  def test42_siview_sj_error
    assert @@sjctest.cleanup_eqp(@@sorter, mode: 'Auto-3', portstatus: 'UnloadReq'), 'error cleaning up sorter'
    assert @@sjctest.prepare_carrier(@@carrier1, 'empty')
    assert @@sjctest.prepare_carrier(@@carrier0, @@lot0)
    $log.info "waiting #{@@mds_sync} s for the MDS to sync"; sleep @@mds_sync
    #
    # create and verify a sort request
    tstart = Time.now
    assert @@sjctest.create_separate_verify(@@carrier0, @@carrier1, requestor: @@requestor)
    srid = @@sjctest.sr.srid
    3.times {
      # need to clean up JavaEI after each error
      assert @@sjctest.cleanup_eqp(@@sorter, mode: 'Auto-3', portstatus: 'UnloadReq'), 'error cleaning up sorter'
      # execute sr
      sleep 10
      assert @@sjcrequest.execute(srid, @@sorter), 'error sending execute message'
      # abort the SiView sorter job
      sleep 10
      assert sj = @@sv.sj_list(lot: @@lot0).first, 'no sorter job'
      assert sj.sorterJobStatus.index('Executing'), 'sj status must be (Wait To) Executing'
      assert_equal 0, @@sv.sj_status_change(@@sorter, sj.sorterJobID, 'Error'), 'error changing SJ status'
      # wait until the sorter job has been deleted by the SJC (on MDS event SJM_SJ_CANCEL)
      assert wait_for(timeout: 180) {
        @@sv.sj_list(lot: @@lot0).empty?
      }, 'sorter job not canceled'
    }
    #
    # verify sort request has been moved to history and lot is ONHOLD with correct claim memo
    $log.info "waiting #{@@mds_sync} s for the MDS to sync"; sleep @@mds_sync
    assert_empty @@mds.sjc_requests(srid: srid), "sr #{srid} not removed from sort request table"
    assert sr = @@mds.sjc_requests(srid: srid, history: true).first, "sr #{srid} not found in sort request history"
    assert_equal 'ERROR', sr.status, 'wrong sr status'
    h = @@sv.lot_hold_list(@@lot0, reason: 'X-SE').first
    assert h, "lot #{@@lot0} must be ONHOLD with reason X-SE"
    memo = "Could not separate lots: #{@@lot0}(#{@@carrier0}->#{@@carrier1}), Requestor: #{@@requestor}, " +
           "Sorter: #{@@sorter}[#{@@pg}]"
    assert h.memo.start_with?(memo), "wrong claim memo: #{h.memo}"
    #
    # verify notification
    return if @@use_notifications == false
    sleep 60
    assert @@sjctest.verify_sjc_notification(tstart, @@lot0, @@carrier0, @@carrier1), 'wrong notification'
  end

  def test43_combine_too_many_wafers
    assert @@sjctest.cleanup_eqp(@@sorter, mode: 'Auto-3', portstatus: 'UnloadReq'), 'error cleaning up sorter'
    assert @@sjctest.prepare_carrier(@@carrier0, @@lot0)
    assert @@sjctest.prepare_carrier(@@carrier1, @@lot1)
    assert lot = @@sv.lot_list_incassette(@@carrier0).first, 'no lot in carrier'
    $log.info "waiting #{@@mds_sync} s for the MDS to sync"; sleep @@mds_sync
    #
    # create combine request
    tstart = Time.now
    assert @@sjctest.create_combine_verify(@@carrier0, @@carrier1, requestor: @@requestor), 'error creating COMBINE request'
    srid = @@sjctest.sr.srid
    # send execute message 3 times to exhaust the retry counter
    3.times {
      assert @@sjcrequest.execute(srid, @@sorter), 'error in execute request'
      sleep 10
    }
    #
    # verify sort request has been moved to history and lot is ONHOLD with correct claim memo
    assert_empty @@mds.sjc_requests(srid: srid), "sr #{srid} not removed from sort request table"
    assert sr = @@mds.sjc_requests(srid: srid, history: true).first, "sr #{srid} not found in sort request history"
    assert_equal 'ERROR', sr.status, 'wrong sr status'
    assert h = @@sv.lot_hold_list(@@lot0, reason: 'X-SE').first, "lot #{@@lot0} has no X-SE hold"
    assert h.memo.start_with?("Could not combine lots: #{lot}(#{@@carrier0}->#{@@carrier1})"), "wrong claim memo: #{h.memo}"
    assert h.memo.include?('Reason: no more target slot available'), "wrong claim memo: #{h.memo}"
    #
    # verify notification
    return if @@use_notifications == false
    sleep 60
    assert @@sjctest.verify_sjc_notification(tstart, lot, @@carrier0, @@carrier1)
  end

  def test44_ignore_EI_errors
    assert @@sjctest.cleanup_eqp(@@sorter, mode: 'Auto-3', portstatus: 'UnloadReq'), 'error cleaning up sorter'
    assert @@sjctest.prepare_carrier(@@carrier0, @@lot0)
    assert @@sjctest.prepare_carrier(@@carrier1, @@lot1)
    [@@carrier2, @@carrier3, @@carrier4].each {|c| assert @@sjctest.prepare_carrier(c, 'empty')}
    $log.info "waiting #{@@mds_sync} s for the MDS to sync"; sleep @@mds_sync
    #
    # create two sort requests, the 2nd one to ensure there are no side effects
    assert @@sjctest.create_transfer_verify(@@carrier0, @@carrier2), 'error creating sr1'
    srid1 = @@sjctest.sr.srid
    assert @@sjctest.create_transfer_verify(@@carrier1, @@carrier3), 'error creating sr2'
    srid2 = @@sjctest.sr.srid
    #
    # NPW reservation for another carrier for PG1, execute request for PG1 fails with SiView rc 999 and reasonText=CEI120569
    assert_equal 0, @@sv.eqp_port_status_change(@@sorter, 'P2', 'LoadReq')
    assert_equal 0, @@sv.npw_reserve(@@sorter, 'P2', @@carrier4), "error creating NPW reservation for #{@@carrier4}"
    #
    # send execute message for sort request 1, verify status is UNPROCESSED
    sleep 10
    assert @@sjcrequest.execute(srid1, @@sorter, @@pg)
    sleep 20
    assert sr = @@mds.sjc_requests(srid: srid1).first, 'SR is gone'
    assert_equal 'UNPROCESSED', sr.status, 'wrong SR status'
    assert_nil sr.sjid, 'no SJ expected'
    # verify sr #2 is still unchanged
    assert sr = @@mds.sjc_requests(srid: srid2).first
    assert_equal 'UNPROCESSED', sr.status, 'wrong SR status'
    #
    # cancel NPW reservation and execute sr #1
    assert @@sv.npw_reserve_cancel(@@sorter, @@carrier4), "error canceling NPW reservation for #{@@carrier4}"
    sleep 10
    assert @@sjctest.execute_verify(srid: srid1), 'error executing SR'
    #
    # verify sr #2 is still unchanged
    assert sr = @@mds.sjc_requests(srid: srid2).first
    assert_equal 'UNPROCESSED', sr.status, 'wrong SR status'
  end

  def test45_same_carrier
    assert @@sjctest.cleanup_eqp(@@sorter, mode: 'Auto-3', portstatus: 'UnloadReq'), 'error cleaning up sorter'
    assert @@sjctest.prepare_carrier(@@carrier0, @@lot0)
    $log.info "waiting #{@@mds_sync} s for the MDS to sync"; sleep @@mds_sync
    #
    # send Create COMBINE request
    params = {lots: @@lot0, srccarriers: @@carrier0, srccarrier_cat: @@sjctest.carrier_category,
                        tgtcarriers: @@carrier0, tgtcarrier_cat: @@sjctest.carrier_category}
    assert @@sjcrequest.create('COMBINE', params), 'no response'
    #
    # verify no sort request is created
    sleep 10
    assert_empty @@mds.sjc_tasks(srccarrier: @@carrier0), "false sort request for carrier #{@@carrier0}"
  end

  def test46_transfer_invalid_srccarrier
    srccarrier = unique_string
    assert_empty @@sv.carrier_list(carrier: srccarrier), "dummy carrier #{srccarrier} exists in SiView"
    tgtcarrier = "#{srccarrier}T"
    assert_empty @@sv.carrier_list(carrier: tgtcarrier), "dummy carrier #{tgtcarrier} exists in SiView"
    # send TRANSFER request with invalid src carrier, a response _is_ returned
    assert @@sjcrequest.create('TRANSFER', srccarriers: srccarrier, tgtcarriers: tgtcarrier), 'no response'
    # verify no sort request is created
    sleep 10
    [false, true].each {|history|
      assert_empty @@mds.sjc_tasks(srccarrier: srccarrier, history: history), 'wrong SR for srccarrier'
      assert_empty @@mds.sjc_tasks(tgtcarrier: tgtcarrier, history: history), 'wrong SR for tgtcarrier'
    }
  end

  def test47_combine_invalid_srccarrier
    srccarrier = unique_string
    assert_empty @@sv.carrier_list(carrier: srccarrier), "dummy carrier #{srccarrier} exists in SiView"
    tgtcarrier = "#{srccarrier}T"
    assert_empty @@sv.carrier_list(carrier: tgtcarrier), "dummy carrier #{tgtcarrier} exists in SiView"
    # send COMBINE request with invalid src carrier, a response _is_ returned
    assert @@sjcrequest.create('COMBINE', lots: 'QAQA.00', srccarriers: srccarrier, tgtcarriers: tgtcarrier), 'no response'
    # verify no sort request is created
    sleep 10
    [false, true].each {|history|
      assert_empty @@mds.sjc_tasks(srccarrier: srccarrier, history: history), 'wrong SR for srccarrier'
      assert_empty @@mds.sjc_tasks(tgtcarrier: tgtcarrier, history: history), 'wrong SR for tgtcarrier'
    }
  end

  def test48_multiple_separates_not_enough_empty_foups  # requires RTDEmu
    assert @@sjctest.cleanup_eqp(@@sorter, mode: 'Auto-3', portstatus: 'UnloadReq'), 'error cleaning up sorter'
    #
    # prepare lots for 2 1:N separate requests
    assert @@sv.lot_family(@@lot0).size > 3, "lot #{@@lot0} must have at least 3 children"
    assert @@sjctest.prepare_carrier(@@carrier0, @@lot0)
    assert @@sjctest.prepare_carrier(@@carrier1, 'empty')
    assert lot2 = @@sjctest.new_lot(carrier: @@carrier1), 'error creating test lot'
    @@testlots << lot2
    3.times {assert @@sv.lot_split(lot2, 2)}
    # prepare 4 empty carriers
    ecs = [@@carrier2, @@carrier3, @@carrier4, @@carrier5]
    ecs.each {|c| assert @@sjctest.prepare_carrier(c, 'empty')}
    $log.info "waiting #{@@mds_sync} s for the MDS to sync"; sleep @@mds_sync
    # set RTD rule
    @@rtdremote.add_emustation(@@empty_rule) {@@rtdremote.create_response(@@columns, {'rows'=>ecs.collect {|c| [c, '', '']}})}
    # create 2 separate requests with 3 empty target carriers each
    tgtcarriers = ['EMPTY.1', 'EMPTY.2', 'EMPTY.3']
    assert @@sjctest.create_separate_verify(@@carrier0, tgtcarriers, srccarrier_cat: @@sjctest.carrier_category)
    srid1 = @@sjctest.sr.srid
    assert @@sjctest.create_separate_verify(@@carrier1, tgtcarriers, srccarrier_cat: @@sjctest.carrier_category)
    srid2 = @@sjctest.sr.srid
    #
    # send execute messages for both sort requests (on behalf of RTD)
    assert @@sjcrequest.execute(srid1, @@sorter, @@pg), "error executing sr #{srid1}"
    assert @@sjcrequest.execute(srid2, @@sorter, @@pg2), "error executing sr #{srid2}"
    #
    # check the sort request status: one must be PENDING the other one UNPROCESSED
    sleep 20
    assert sr1 = @@mds.sjc_requests(srid: srid1).first, "sort request #{srid1} no longer exists"
    assert sr2 = @@mds.sjc_requests(srid: srid2).first, "sort request #{srid2} no longer exists"
    assert_equal ['PENDING', 'UNPROCESSED'], [sr1.status, sr2.status].sort, "wrong sort request status: #{[sr1, sr2].inspect}"
  end

  # DB field lenghts

  def test51_long_hint
    # send SEPARATE request with Hint 64 chars long
    assert @@sjctest.prepare_carrier(@@carrier0, @@lot0)
    assert @@sjctest.prepare_carrier(@@carrier1, 'empty')
    $log.info "waiting #{@@mds_sync} s for the MDS to sync"; sleep @@mds_sync
    params = {lots: @@lot0, srccarriers: @@carrier0, tgtcarriers: @@carrier1, hint: 'Q' * 64}
    assert @@sjcrequest.create('SEPARATE', params), 'no response'
    assert wait_for(timeout: 60) {
      !@@mds.sjc_tasks(srccarrier: @@carrier0).empty?
    }, 'no SR created, strange!'
    # send Create request with Hint > 64 chars long
    assert @@sjctest.prepare_carrier(@@carrier0, @@lot0)
    assert @@sjctest.prepare_carrier(@@carrier1, 'empty')
    $log.info "waiting #{@@mds_sync} s for the MDS to sync"; sleep @@mds_sync
    params[:hint] = 'Q' * 65
    assert @@sjcrequest.create('SEPARATE', params), 'no response'
    assert wait_for(timeout: 60) {
      !@@mds.sjc_tasks(srccarrier: @@carrier0).empty?
    }, 'no SR with long hint created'
  end

  def test52_long_requestor
    # send Create request with Requestor 64 chars long
    assert @@sjctest.prepare_carrier(@@carrier0, @@lot0)
    assert @@sjctest.prepare_carrier(@@carrier1, 'empty')
    $log.info "waiting #{@@mds_sync} s for the MDS to sync"; sleep @@mds_sync
    params = {lots: @@lot0, srccarriers: @@carrier0, tgtcarriers: @@carrier1, requestor: 'Q' * 16}
    assert @@sjcrequest.create('SEPARATE', params), 'no response'
    assert wait_for(timeout: 60) {
      !@@mds.sjc_tasks(srccarrier: @@carrier0).empty?
    }, 'no SR created, strange!'
    # send Create request with Requestor > 64 chars long
    assert @@sjctest.prepare_carrier(@@carrier0, @@lot0)
    assert @@sjctest.prepare_carrier(@@carrier1, 'empty')
    $log.info "waiting #{@@mds_sync} s for the MDS to sync"; sleep @@mds_sync
    params[:requestor] = 'Q' * 17
    assert @@sjcrequest.create('SEPARATE', params), 'no response'
    assert wait_for(timeout: 60) {
      !@@mds.sjc_tasks(srccarrier: @@carrier0).empty?
    }, 'no SR with long requestor created'
  end

  # life cycle handler

  def test61_timeout1_unprocessed
    assert @@sjctest.cleanup_eqp(@@sorter, mode: 'Auto-3', portstatus: 'UnloadReq'), 'error cleaning up sorter'
    assert @@sjctest.prepare_carrier(@@carrier0, @@lot0)
    # lots = @@sv.lot_list_incassette(@@carrier0).take(2)
    #
    # create SR, but don't execute
    tgtcarriers = ['EMPTY.1', 'EMPTY.2']
    assert @@sjctest.create_separate_verify(@@carrier0, tgtcarriers, requestor: @@requestor), 'wrong or no SR'
    #
    # wait for expiration_unhandled_sr
    sleeptime = @@lifecyclecheck_interval + @@expiration_unhandled_sr + 30
    $log.info "waiting #{(sleeptime/60).to_i} m for timeout1"; sleep sleeptime
    #
    # verify if the sort request has been deleted from MDS when it expires
    assert_empty @@mds.sjc_requests(srid: @@sjctest.sr.srid), 'SR still exists'
  end

  def test62_timeout2_pending
    # change sorter port status to 'UnloadReq' to prevent SJ execution by the watchdog
    assert @@sjctest.cleanup_eqp(@@sorter, mode: 'Auto-3', portstatus: 'UnloadReq'), 'error cleaning up sorter'
    # prepare src and target carriers
    assert @@sjctest.prepare_carrier(@@carrier0, @@lot0)
    ecs = [@@carrier2, @@carrier3, @@carrier4, @@carrier5]
    ecs.each {|c| assert @@sjctest.prepare_carrier(c, 'empty')}
    #
    # create SR, but don't execute yet
    tgtcarriers = ['EMPTY.1', 'EMPTY.2']
    assert @@sjctest.create_separate_verify(@@carrier0, tgtcarriers, requestor: @@requestor), 'wrong or no SR'
    srid = @@sjctest.sr.srid
    tstart1 = Time.now
    #
    # wait part of expiration_unhandled_sr, with big slick due to lifecyclecheck intervals (5 m)
    sleeptime = 600 + @@expiration_unhandled_sr - @@expiration_executing_sj
    $log.info "sleeping #{sleeptime/60} min as part of timeout1"; sleep sleeptime
    #
    # check sr status again: UNPROCESSED
    assert sr = @@mds.sjc_requests(srid: srid).first, "no sort request with id #{srid}"
    assert_equal 'UNPROCESSED', sr.status, "wrong status of sort request #{sr.inspect}"
    #
    # set RTD rule to replace EMPTY.x carriers
    assert @@rtdremote.add_emustation(@@empty_rule) {
      @@rtdremote.create_response(@@columns, {'rows'=>ecs.collect {|c| [c, @@sjctest.carrier_category, @@sjctest.stocker]}})
    }
    # send execute message (SJC calls the RTD empty foup rule)
    assert @@sjctest.execute_verify, 'error in execute request'
    refute_empty @@sv.sj_list(eqp: @@sorter), 'no SiView sorter job'
    assert sr = @@mds.sjc_requests(srid: srid).first
    assert_equal 'PENDING', sr.status, "wrong status of sort request #{sr.inspect}"
    tstart2 = Time.now
    #
    # wait for completion of expiration_unhandled_sr
    sleeptime = tstart1 - Time.now + @@lifecyclecheck_interval + @@expiration_unhandled_sr + 20
    $log.info "waiting #{(sleeptime/60).to_i} m for timeout1 (sort request)"; sleep sleeptime
    #
    # verify sort request and sorter job are unchanged
    assert sr = @@mds.sjc_requests(srid: srid).first, "no sort request with id #{srid}"
    assert_equal 'PENDING', sr.status, "wrong status of sort request #{sr.inspect}"
    assert sj = @@sv.sj_list(eqp: @@sorter).first, 'no SiView sorter job'
    assert_equal 'Wait To Executing', sj.sorterJobStatus, 'wrong sj status'
    #
    # wait for SJC expiration_executing_sj
    sleeptime = tstart2 - Time.now + @@lifecyclecheck_interval + @@expiration_executing_sj + 20
    $log.info "waiting #{(sleeptime/60).to_i} m for timeout2 (sorter job)"; sleep sleeptime
    #
    # verify sort request and sorter job are unchanged
    assert sr = @@mds.sjc_requests(srid: srid).first, "no sort request with id #{srid}"
    assert_equal 'PENDING', sr.status, "wrong status of sort request #{sr.inspect}"
    assert sj = @@sv.sj_list(eqp: @@sorter).first, 'no SiView sorter job'
    assert_equal 'Wait To Executing', sj.sorterJobStatus, 'wrong sj status'
    #
    # complete sorter job and wait for deletion
    assert_equal 0, @@sv.sj_status_change(@@sorter, sj.sorterJobID, 'Error'), 'error changing SJ status'
    @@sv.sj_cancel(sj.sorterJobID)
    assert wait_for(timeout: 120) {@@sv.sj_list(eqp: @@sorter).empty?}, 'SJ must be deleted'
    #
    # verify SR deletion
    $log.info "waiting up to #{@@lifecyclecheck_interval} s for the SR life cycle check"
    assert wait_for(timeout: @@lifecyclecheck_interval + 70) {@@mds.sjc_requests(srid: srid).empty?}, "SR #{srid} not terminated"
    #
    # verify SR status in history
    sr = @@mds.sjc_requests(srid: srid, history: true).first
    assert_equal 'EXPIRED', sr.status, "wrong SR status in history: #{sr.inspect}"
  end

  def test63_timeout_sjupdate   # MSR 1148183
    skip 'not applicable in Fab8' if $env == 'f8stag'
    # code is similar to JCAP_SJC#test44, P2 down do make execute fail
    assert @@sjctest.cleanup_eqp(@@sorter, mode: 'Auto-3', portstatus: 'UnloadReq', available_ports: ['P1', 'P2']), 'error cleaning up sorter'
    assert @@sjctest.prepare_carrier(@@carrier0, @@lot0)
    assert @@sjctest.prepare_carrier(@@carrier1, 'empty')
    assert @@sjctest.prepare_carrier(@@carrier4, 'empty')
    #
    # create a SiView wildcard SJ and wait for CaptureWildcardSorterJobs to create an SR
    assert sjid = @@sjctest.create_wildcard_verify(@@lot0, @@carrier1), 'error creating SJ or SR'
    #
    # call sj_update to generate an SJC error on the next execute call
    assert_equal 0, @@sv.sj_update(sjid, @@sorter, 'P1', 'P2', @@pg), 'sj_update error'
    # NPW reservation for another carrier for PG1, execute request for PG1 fails with SiView rc 999 and reasonText=CEI120569
    assert_equal 0, @@sv.eqp_port_status_change(@@sorter, 'P2', 'LoadReq')
    assert_equal 0, @@sv.npw_reserve(@@sorter, 'P2', @@carrier4), "error creating NPW reservation for #{@@carrier4}"
    # assert_equal 0, @@sv.eqp_port_status_change(@@sorter, 'P1', 'Down')
    srid = @@sjctest.sr.srid
    assert @@sjcrequest.execute(srid, @@sorter, @@pg), 'error submitting execute request'
    #
    # wait for SJ and SRcancelation
    assert wait_for(timeout: 120) {
      !@@mds.sjc_requests(srid: srid, status: 'CANCELED', history: true).empty?
    }, 'SR not canceled?'
    assert_empty @@sv.sj_list(sjid: sjid), 'SiView SJ not canceled'
  end


  def testman91_memory
    # test data generation to allow admins to monitor memory consumption, on demand only
    params = {lots: 'QA0001', srccarriers: 'QAC0001', srccarrier_cat: @@sjctest.carrier_category,
                          tgtcarriers: 'QB0001', tgtcarrier_cat: @@sjctest.carrier_category}
    30000.times {@@sjcrequest.create('COMBINE', params)}
  end

end
