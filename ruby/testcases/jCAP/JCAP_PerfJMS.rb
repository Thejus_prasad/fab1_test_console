=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.


Author: Steffen Frieske, 2019-02-22

Version: 19.02

History:

Notes:
  Tests the performance/endurance of the JBOSS JMS system under sustained high load
=end

require 'mqjms'             # to take resulting msgs from the Queue
require 'dbsequel'          # for MDSEventMQBridge

require 'jcap/lmtrans'
require 'jcap/apcsra'
require 'jcap/avls'
require 'jcap/sjctest'
require 'jcap/mdswritesvc'  # for RTDServicesMDS (MdsWriteService)
require 'RubyTestCase'


class JCAP_PerfJMS < RubyTestCase
  @@eqp = 'DFU522'  # any eqp existing in SiView
  @@rscs = {}
  @@_run = nil
  @@duration = 30

  def self.shutdown
    @@sjctest.cleanup_sort_requests(creator: 'MQSRA')
    while @@mq.delete_msgs(nil); sleep 10; end
  end


  def test00_setup
    $setup_ok = false
    #
    # for cleanup
    @@sjctest = SJC::Test.new($env, sv: @@sv)
    @@mq = MQ::JMS.new("qaany.#{$env}")
    while @@mq.delete_msgs(nil); sleep 3; end
    #
    #
    @@rscs['MQSnk'] = 2.times.collect {|i|
      o = MQ::JMS.new("qaany.#{$env}")
      Proc.new {o.receive_msg(timeout: 20, raw: true)}
    }
    @@rscs['SJCCl'] = 1.times.collect {|i| 
      o = SJC::Test.new($env, sv: @@sv)
      Proc.new {o.cleanup_sort_requests(creator: 'MQSRA')}
    }
    @@rscs['MDSMQ'] = 1.times.collect {|i| 
      o = ::DB.new("mds.#{$env}")
      q = "CALL P_SIGNAL_MDS_EVENT('MSG_TO_MQ', '1', '#{@@mq.queue.name}', 'QAPerf#{i}')"
      Proc.new {o.execute(q); sleep 1}
    }
    @@rscs['RTDWR'] = 15.times.collect {|i| 
      o = MdsWriteService.new($env, silent: true)
      Proc.new {o.write_eqpnexttime(@@eqp, 1000)}
    }
    @@rscs['LMTRA'] = 15.times.collect {|i| 
      o = LotManager::Transformer.new($env)
      o.set_default_data
      Proc.new {o.update_lmrequests(silent: true)}
    }
    @@rscs['MQSRA'] = 15.times.collect {|i| 
      o = APC::MQSRClient.new($env, sv: @@sv)
      winfos = [[SiView::MM::Wafer.new('QAWAFER', 1, '01', 'QALOT')]]
      Proc.new {o.sjseparate(winfos, srccarrier: "QAC#{i}", srccarrier_cat: 'QACCAT', silent: true)}
    }
    @@rscs['AVLSt'] = 15.times.collect {|i| 
      o = AVLS::VendorMessage.new($env)
      Proc.new {o.create_fosb(["QAQA#{i}", "QAQA#{i}"], ["QAQA#{i}", "QAQA#{i}"], silent: true)}
    }
    #
    $setup_ok = true
  end

  def test11
    @@mq.delete_msgs(nil)
    #
    $log.info "running test for #{@@duration} s"
    @@_run = true
    tt = []
    @@rscs.keys.each {|k| tt += self.class.start_gen(k)}
    #
    # set stop timer
    Thread.new {
      sleep @@duration
      $log.info "stopping test"
      @@_run = false
    }
    $tt = tt
    tt.reverse.each {|t| t.join}
  end

  # aux methods

  def self.start_gen(name)
    return @@rscs[name].each_with_index.collect {|prc, i|
      sleep 0.1
      Thread.new {
        tname = "#{name}%02d" % i
        Thread.current['name'] = tname
        $log.info "start #{tname}"
        sleep 0.1
        o = @@rscs[name][i]
        while @@_run
          prc.call(i)
        end
        $log.info "stopped #{tname}"
      }
    }
  end

  ##           @@fgui.engineering_kit_response('QAPerf', [2, 3], {k: [1, 2, 3, 4, 5]})

end
