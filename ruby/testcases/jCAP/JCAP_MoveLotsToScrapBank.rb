=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.


Author: Steffen Steidten, 2010-11-08

Version: 19.10

History:
  2015-04-07 ssteidte, use rtdemuext, check jCAP only with a test rule
  2017-01-05 sfrieske, use new RTD::EmuRemote, minor cleanup
  2017-01-26 tklose,   extended for version 17.1
  2017-06-06 sfrieske, minor fixes
  2018-11-07 sfrieske, minor cleanup
  2019-02-19 sfrieske, improved EmuRemote usage
  2019-09-23 sfrieske, added notification tests for version 19.10
  2020-07-14 sfrieske, added non-ASCII char tests

Notes:
  http://f1onewiki:21080/display/JCAP/MoveLotsToScrapBank/
  This job just acts on the RTD response, no logic contained.
=end

require 'derdack'
require 'rtdserver/rtdemuremote'
require 'SiViewTestCase'


class JCAP_MoveLotsToScrapBank < SiViewTestCase
  @@nonprobank1 = 'O-TOWING'
  @@nonprobank2 = 'OX-SCRAPDISPO'
  @@rtd_rule = 'JCAP_MOVE_LOTS_TO_SCRAP_BANK'
  @@columns = %w(LotId LotOwner SubLotType CurrentBank DestinationBank ActionType LotOwnerEmail 
    MoveDurationSeconds MoveDurationDays PD Product LotPurpose)
  @@owner = 'steffen.frieske'  # email address
  @@notification_params = {service: '%jCAP_AsyncNotification%'}

  @@job_interval = 210
  @@testlots = []
  @@tstart = nil


  def self.shutdown
    @@rtdremote.delete_emustation('', rule: @@rtd_rule)
  end

  def self.after_all_passed
    @@testlots.each {|lot| @@sv.delete_lot_family(lot)}
    $log.info 'check your mail box for emails from job'
  end

  def test00_setup
    $setup_ok = false
    #
    @@rtdremote = RTD::EmuRemote.new($env)
    @@notifications = DerdackEvents.new($env)
    @@notification_params[:filter] = {'Application'=>'MoveLotsToScrapBank', 'Owner'=>@@owner}
    #
    $setup_ok = true
  end


  def test11
    @@tstart = Time.now
    # prepare test lots
    assert lots = @@svtest.new_lots(3), 'error creating test lots'
    @@testlots += lots
    lot1, lot2, lot3 = lots
    sleep 3 # 120
    assert_equal 0, @@sv.lot_nonprobankin(lot1, @@nonprobank1)
    assert_equal 0, @@sv.lot_hold(lot2, 'B-ENG')
    assert_equal 0, @@sv.lot_nonprobankin(lot3, @@nonprobank1)
    #
    # create RTD answer
    li = @@sv.lot_info(lot1)
    data = [
      # static data for upcoming moves
      ['SENT-LOT1', 'XXX-OWNER', 'PO', 'CURBANK', 'DESTBANK1', 'WARN', @@owner, 3, 1, 'PD', 'PROD', 'WARNING1'],
      ['SENT-LOT2', 'XXX-OWNER', 'PZ', 'CURBANK', 'DESTBANK1', 'WARN', @@owner, 3, 1, 'PD', 'PROD', 'WARNING2'],
      ['SENT-LOT3', 'XXX-OWNER', 'PY', 'CURBANK', 'DESTBANK1', 'WARN', @@owner, 3, 1, 'PD', 'PROD', "WARNING\u00DF"],
      ['SENT-LOT4', 'XXX-OWNER', 'PA', 'CURBANK', 'DESTBANK2', 'WARN', @@owner, 3, 1, 'PD', 'PROD', 'WARNING4'],
      ['NOTSENT-1', 'XXX-OWNER', 'PB', 'CURBANK', 'DESTBANK1', 'WARN', @@owner, 3, 5, 'PD', 'PROD', 'WARNING5'],
      ['NOTSENT-2', 'XXX-OWNER', 'PC', 'CURBANK', 'DESTBANK1', 'WARN', @@owner, 3, 5, 'PD', 'PROD', 'WARNING6'],
      # empty email, for logging only
      ['NOMAIL', 'XXX-OWNER', 'PC', 'CURBANK', 'DESTBANK1', 'WARN', '', 3, 1, 'PD', 'PROD', 'WARNING'],
      # dynamic data
      [lot1, li.user, li.sublottype, @@nonprobank1, @@nonprobank2, 'MOVE', @@owner, 3, 1, li.op, li.product, 'SUCCESS'],
      [lot2, li.user, li.sublottype, @@nonprobank1, @@nonprobank2, 'MOVE', @@owner, 3, 1, li.op, li.product, 'FAIL'],
      [lot3, li.user, li.sublottype, @@nonprobank1, @@nonprobank2, 'MOVE', @@owner, 3, 1, li.op, li.product, 'SUCCESS']
    ]
    assert @@rtdremote.add_emustation_wait('', rule: @@rtd_rule, timeout: @@job_interval) {
      @@rtdremote.create_response(@@columns, {'rows'=>data})
    }, 'rule has not been called'
    #
    # verify lot1 and lot3 are banked in, lot2 is not
    sleep 15
    assert_equal @@nonprobank2, @@sv.lot_info(lot1).bank, "lot1 #{lot1} not banked in"
    assert_equal @@nonprobank2, @@sv.lot_info(lot3).bank, "lot3 #{lot3} not banked in"
    refute_equal @@nonprobank2, @@sv.lot_info(lot2).bank, "lot2 #{lot2} wrong bankin"
    #
    # verify notifications
    assert msgs = @@notifications.query(@@notification_params.merge(tstart: @@tstart))
    assert msgs.size >= 4, 'missing notification'
    # summary
    assert msg = msgs.find {|m| m['Event'] == 'MoveLotsToScrapBank Warning'}
    %w(DESTBANK1 DESTBANK2 SENT-LOT1 SENT-LOT2 SENT-LOT3 SENT-LOT4).each {|s|
      assert msg['Message'].include?(s), "missing #{s}"
    }
    %w(NOTSENT-1 NOTSENT-2).each {|s|
      refute msg['Message'].include?(s), "wrong #{s}"
    }
    # moved lots
    [lot1, lot3].each {|lot|
      assert m = msgs.find {|m| m['Message'].include?(lot)}, "missing message for lot #{lot}"
      assert m['Message'].include?('LotPurpose: SUCCESS')
      assert m['Message'].include?(@@nonprobank2)
    }
    # failed lots
    [lot2].each {|lot|
      assert m = msgs.find {|m| m['Message'].include?(lot)}, "missing message for lot #{lot}"
      assert m['Message'].include?('LotPurpose: FAIL')
    }
  end

end
