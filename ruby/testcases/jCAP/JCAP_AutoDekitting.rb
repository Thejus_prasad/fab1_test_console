=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: Daniel Steger, 2013-04-16

Version: 21.05

History:
  2015-04-10 ssteidte, use rtdemuext and sjctest
  2015-08-21 ssteidte, changed RTD rule name, cleaned up
  2017-01-05 sfrieske, use new RTD::EmuRemote, minor cleanup
  2018-11-07 sfrieske, minor cleanup
  2019-02-19 sfrieske, improved EmuRemote usage
  2021-07-13 sfrieske, removed use of prepare_separate_child
  2021-07-30 sfrieske, inherit from SiViewTestCase
=end

require 'jcap/sjctest'
require 'rtdserver/rtdemuremote'
require 'SiViewTestCase'


class JCAP_AutoDekitting < SiViewTestCase
  @@sv_defaults = {carriers: 'JC%'}
  @@sorter = 'UTS001'
  @@columns = %w(cast_id lot_id new_cast wafer_id info seq)
  @@rtd_rule = 'JCAP_SJM_DEKIT'

  @@job_interval = 330   # 5+ min, check in properties
  @@testlots = []


  def self.shutdown
    @@rtdremote.delete_emustation('', rule: @@rtd_rule)
  end

  def self.after_all_passed
    @@testlots.each {|lot| @@sv.delete_lot_family(lot)}
  end

  def setup
    return unless self.class.class_variable_defined?(:@@rtdremote)
    @@rtdremote.add_emustation('', rule: @@rtd_rule, method: 'empty_reply')
    @@sjctest.cleanup_sort_requests(creator: 'DEKIT')
  end


  def test00_setup
    $setup_ok = false
    #
    @@sjctest = SJC::Test.new($env, @@sv_defaults.merge(sorter: @@sorter, sv: @@sv))
    @@rtdremote = RTD::EmuRemote.new($env)
    assert @@svtest.get_empty_carrier(n: 10), 'not enough empty carriers'
    #
    $setup_ok = true
  end

  def test11_multiple_1_1
    $setup_ok = false
    #
    # prepare 4 lots in 4 carriers
    assert lot = @@svtest.new_lot, 'error creating testlot'
    @@testlots << lot
    carrier = @@sv.lot_info(lot).carrier
    assert @@sjctest.cleanup_sort_requests(carrier)
    lcs = 3.times.collect {
      assert lc = @@sv.lot_split(lot, 2), 'error splitting lot'
      assert c = @@svtest.get_empty_carrier, 'no empty carrier found'
      assert @@sjctest.cleanup_sort_requests(c)
      assert_equal 0, @@sv.wafer_sort(lc, c)
      assert_equal 0, @@sv.carrier_xfer_status_change(c, '', @@sjctest.stocker)
      lc
    }
    #
    # get empty carrier
    assert ec = @@svtest.get_empty_carrier
    assert @@sjctest.cleanup_sort_requests(ec)
    #
    # create RTD rule
    l0wafers = @@sv.lot_info(lot, wafers: true).wafers.collect {|w| w.wafer}.join(',')
    c1 = @@sv.lot_info(lcs[1]).carrier
    lc2wafers = @@sv.lot_info(lcs[2], wafers: true).wafers.collect {|w| w.wafer}.join(',')
    data = [
      [@@sv.lot_info(lot).carrier, lot, ec, l0wafers, 'QA Test', 1],        # parent lot into empty carrier
      [@@sv.lot_info(lcs[2]).carrier, lcs[2], c1, lc2wafers, 'QA Test', 2]  # lc2 to lc1
    ]
    tstart = Time.now
    assert @@rtdremote.add_emustation_wait('', rule: @@rtd_rule, timeout: @@job_interval) {
      @@rtdremote.create_response(@@columns, {'rows'=>data})
    }, 'rule has not been called'
    #
    # verify sort requests have been created
    assert @@sjctest.verify_sr_combine(lot, ec, timeout: @@job_interval, tstart: tstart)
    assert_equal 'DEKIT', @@sjctest.sr.creator, 'wrong SR creator'
    assert @@sjctest.verify_sr_combine(lcs[2], c1, timeout: @@job_interval, tstart: tstart)
    assert_equal 'DEKIT', @@sjctest.sr.creator, 'wrong SR creator'
    #
    $setup_ok = true
  end

  def test12_multiple_1_1_errors
    # prepare 4 lots in 4 carriers
    assert lot = @@svtest.new_lot, 'error creating test lot'
    @@testlots << lot
    carrier = @@sv.lot_info(lot).carrier
    assert @@sjctest.cleanup_sort_requests(carrier)
    lcs = 3.times.collect {
      assert lc = @@sv.lot_split(lot, 2), 'error splitting lot'
      assert c = @@svtest.get_empty_carrier, 'no empty carrier found'
      assert @@sjctest.cleanup_sort_requests(c)
      assert_equal 0, @@sv.wafer_sort(lc, c)
      assert_equal 0, @@sv.carrier_xfer_status_change(c, '', @@sjctest.stocker)
      lc
    }
    #
    # create RTD rule
    l0wafers = @@sv.lot_info(lot, wafers: true).wafers.collect {|w| w.wafer}.join(',')
    c1 = @@sv.lot_info(lcs[1]).carrier
    lc2wafers = @@sv.lot_info(lcs[2], wafers: true).wafers.collect {|w| w.wafer}.join(',')
    data = [
      [@@sv.lot_info(lot).carrier, lot, 'NOTEX', l0wafers, 'QA Test', 1],    # parent lot into not existing carrier
      [@@sv.lot_info(lcs[2]).carrier, lcs[2], c1, lc2wafers, 'QA Test', 2]   # lc2 to lc1
    ]
    tstart = Time.now
    assert @@rtdremote.add_emustation('', rule: @@rtd_rule, timeout: @@job_interval) {
      @@rtdremote.create_response(@@columns, {'rows'=>data})
    }, 'rule has not been called'
    #
    # no SR for parent lot, SR for lc2
    assert @@sjctest.verify_sr_combine(lcs[2], c1, timeout: @@job_interval, tstart: tstart)
    assert_equal 'DEKIT', @@sjctest.sr.creator, 'wrong SR creator'
    assert_empty @@sjctest.mds.sjc_tasks(srccarrier: carrier)
    assert_empty @@sjctest.mds.sjc_tasks(tgtcarrier: carrier)
  end

  def test13_N_1
    # prepare 4 lots in 4 carriers
    assert lot = @@svtest.new_lot, 'error creating test lot'
    @@testlots << lot
    carrier = @@sv.lot_info(lot).carrier
    assert @@sjctest.cleanup_sort_requests(carrier)
    lcs = 3.times.collect {
      assert lc = @@sv.lot_split(lot, 2), 'error splitting lot'
      assert c = @@svtest.get_empty_carrier, 'no empty carrier found'
      assert @@sjctest.cleanup_sort_requests(c)
      assert_equal 0, @@sv.wafer_sort(lc, c)
      assert_equal 0, @@sv.carrier_xfer_status_change(c, '', @@sjctest.stocker)
      lc
    }
    #
    # create RTD rule to move 3 chlid lots in the parent lot's carrier
    data = lcs.collect {|lc|
      lwafers = @@sv.lot_info(lc, wafers: true).wafers.collect {|w| w.wafer}.join(',')
      [@@sv.lot_info(lc).carrier, lc, carrier, lwafers, 'QA Test03', 4]
    }
    tstart = Time.now
    assert @@rtdremote.add_emustation('', rule: @@rtd_rule, timeout: @@job_interval) {
      @@rtdremote.create_response(@@columns, {'rows'=>data})
    }, 'rule has not been called'
    #
    # verify SR to move all child lots is created
    assert @@sjctest.verify_sr_combine(lcs, carrier, timeout: @@job_interval, tstart: tstart)
    assert_equal 'DEKIT', @@sjctest.sr.creator, 'wrong SR creator'
  end

  # scenario not supported, no SR created
  def XXtest14_1_N
    # prepare 4 lots in 1 carrier
    assert lot = @@svtest.new_lot, 'error creating test lot'
    @@testlots << lot
    carrier = @@sv.lot_info(lot).carrier
    assert @@sjctest.cleanup_sort_requests(carrier)
    assert lcs = 3.times.collect {@@sv.lot_split(lot, 2)}
    # get 3 empty carriers
    assert ecs = @@svtest.get_empty_carrier(n: 3)
    ecs.each {|ec| assert @@sjctest.cleanup_sort_requests(ec)}
    #
    # create RTD rule to move 3 chlid lots in separate carriers
    data = lcs.each_with_index.collect {|lc, i|
      lwafers = @@sv.lot_info(lc, wafers: true).wafers.collect {|w| w.wafer}.join(',')
      [carrier, lc, ecs[i], lwafers, 'QA Test02', 3]
    }
    @@rtdremote.add_emustation(@@rtd_rule) {|*args| @@headers.merge('rows'=>data)}
    #
    # verify no SR is created for the src carrier
    $log.info "waiting #{@@job_interval} s for the jCAP job to run"; sleep @@job_interval
    assert_empty @@sjctest.mds.sjc_tasks(srccarrier: carrier)
  end

end
