=begin 
Dummy Test to verify the test framework works.

(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.
    
Author: Steffen Steidten, 2011-08-02
=end

require "Test_0"

# Dummy Test to verify the test framework works.
class Test_10 < Test_0
  @@param1 = "subclass override"

  def test01
    # override setting from Test_0 in Test_0.par and Test_10
    assert_equal "subclass override", @@param1, "override not working"
  end
  
  def test02
    # override setting from Test_0 in Test_0.par only
    assert_equal 100, @@param2, "override not working"
  end
  
  def test12
    $log.info "new tc on top of the inherited ones"
  end
  
  def test42
    $log.info "new tc on top of the inherited ones"
  end

  def self.after_all_passed
    puts "Cleanup"
  end
end
