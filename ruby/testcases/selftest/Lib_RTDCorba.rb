=begin
(c) Copyright 2011 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: sfrieske, 2018-04-25

Version: 1.0.0

History:
=end

require 'RubyTestCase'

# Test RTD server call via SiView
class Lib_RTDCorba < RubyTestCase
  @@eqp_emu = 'UTR002'
  @@eqp_real = 'AWS001'
  @@stn1 = 'INSP'
  @@parameters1 = 'TYPE=URSA,PRINT,RDI|Report=L_rdi_icada|Category=DISPATCHER/RULES|requestingEntity=iCADA'


  def test1
    assert res = @@sv.rtd_dispatch_list(@@eqp_emu)
    assert res.columnHeaders.include?('Machine Recipe ID')
    assert res = @@sv.rtd_dispatch_list(@@eqp_real)
    assert res.columnHeaders.include?('Machine Recipe ID')
    assert res = @@sv.rtd_dispatch_list(@@stn1, function: 'dummy', parameters: @@parameters1)
    assert res.columnHeaders.include?('RDI')
  end
  
end
