=begin 
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.
    
Author: Steffen Frieske, 2015-10-02
=end

require 'RubyTestCase'


# Test $log rotation
class Test_Logger < RubyTestCase

  def self.startup
  end

  def test11_rotate
    $setup_ok = false
    flog = Logger.new('log/testlogger.log', 3, 1024)
    10.times {flog.info "X" * 512}
    $setup_ok = true
  end

  def test12_rotate_threads
    flog = Logger.new('log/testlogger.log', 3, 1024)
    10.times {
      Thread.new {
        100.times {flog.info "X" * 512}
      }
    }
  end
end
