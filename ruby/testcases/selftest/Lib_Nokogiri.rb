=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: Steffen Frieske, 2016-02-10

Version: 1.0.0

History:
=end

require 'RubyTestCase'
require 'apfam'


## Test HTML access vio Nokogiri, fails if there are failures in JRUBY Java gem access like in JRuby 9.0.5
class Lib_Nokogiri < RubyTestCase
  @@job = 'Job-99'

  def self.startup
    # no siview
  end

  def test01
    $setup_ok = false
    #
    @@apfam = APF::AM.new($env)
    assert res = @@apfam.job_details(@@job)
    #
    $setup_ok = true
  end

end
