=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: Steffen Frieske, 2016-02-10

Version: 1.0.0

History:
=end

require 'RubyTestCase'
require 'rubyXL'
#require 'poi'


## Test Excel file access vio POI, fails if there are failures in JRUBY Java gem access like in JRuby 9.0.5
# Test Excel file access, requires a Ruby gem loaded from gems.jar
class Lib_Excel < RubyTestCase
  @@xls_template = 'testcasedata/rva/specs/C02-QA-Load/C02-QL00v1.xlsx'

  def self.startup
    # no siview
  end

  def test01
    $setup_ok = false
    #
    data = File.binread(@@xls_template)
    assert wb1 = RubyXL::Parser.parse_buffer(StringIO.new(data))
    assert sheet1 = wb1[0]
    assert wb2 = RubyXL::Parser.parse(@@xls_template)
    assert sheet2 = wb2[0]
    #
    $setup_ok = true
  end

end
