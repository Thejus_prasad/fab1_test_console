=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: Steffen Frieske, 2016-01-01

Version: 1.0.1

History:
=end

require 'SiViewTestCase'
# require 'dbsequel'
require 'siview/mmdb'
require 'util/uniquestring'
require 'util/waitfor'


# Test db access, especially time zone awareness
class Lib_db < SiViewTestCase
  @@sorter = 'UTS001'
  @@uparam = 'Pull'


  def self.shutdown
    @@sv.eqp_cleanup(@@sorter)
  end

  def test00_setup
    $setup_ok = false
    #
    assert_nil ENV['TZ']
    # MDS
    assert @@mmdb1 = SiView::MMDB.new($env, use_mds: true)
    assert @@mmdb1.db.dsn.include?('oracle'), 'no Oracle database'
    # SiView
    assert @@mmdb2 = SiView::MMDB.new($env)
    assert @@mmdb2.db.dsn.include?('db2'), 'no DB2 database'
    #
    $setup_ok = true
  end

  def test11_siview
    #
    # change a user parameter
    tstart = Time.now - 10
    v = unique_string
    assert_equal 0, @@sv.user_parameter_change('Eqp', @@sorter, @@uparam, v)
    #
    # Oracle MDS
    res1 = nil
    assert wait_for {
      res1 = @@mmdb1.user_parameter_history(id: @@sorter, name: @@uparam, value: v, tstart: tstart).first
    }, 'no matching MDS parameter history entry'
    $log.info "tstart: #{tstart}, event_time: #{res1[:event_time]}"
    assert tstart < res1[:event_time]
    assert tstart + 60 > res1[:event_time]
    #
    # SiView DB2
    ($log.warn 'DB2 not tested'; return) if ['f8stag', 'mtdc'].member?($env)  # not kept in SiView!!
    res2 = nil
    assert wait_for {
      res2 = @@mmdb2.user_parameter_history(id: @@sorter, name: @@uparam, value: v, tstart: tstart).first
    }, 'no matching MMDB parameter history entry'
    $log.info "tstart: #{tstart}, event_time: #{res2[:event_time]}"
    assert_equal res1[:event_time], res2[:event_time]
  end

end
