=begin 
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.
    
Author: Steffen Steidten, 2011-08-02
=end

require 'RubyTestCase'

# Dummy Test to verify the test framework works.
class Test_1 < RubyTestCase
  @@sleeptime = 10
  
  def self.startup
    $log.info "Startup called"
  end

  def self.after_all_passed
    $log.info "called after_all_passed"
  end


  def test00_setup
    #$setup_ok = false
    #assert false
  end

  def test01_raise
    raise "MyException"
  end

  def test10_class_variables
    # set a class variable to see if it gets deleted
    assert !self.class.class_variables.member?(:@@testvar)
    @@testvar = 'SET'
  end

  def test11    
    $log.info "happy test case, sleeping #{@@sleeptime}s"
    sleep @@sleeptime
    #$setup_ok = false
  end
  
  def test12
    assert_equal ["x"], ["test"]
  end

end
