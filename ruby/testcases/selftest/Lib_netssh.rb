=begin
(c) Copyright 2016 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.


Author: Steffen Frieske, 2021-11-04

Version: 1.0

History:
=end

require 'remote'
require 'RubyTestCase'


class Lib_netssh < RubyTestCase
  @@host = 'f38asq1'
  @@fname = '/tmp/qa_lib_netssh.txt'


  def self.startup
    super(nosiview: true)
  end


  def test00_setup
    $setup_ok = false
    #
    @@rhost = RemoteHost.new("cei.#{$env}", host: @@host)
    #
    $setup_ok = true
  end

  def test11_exec
    assert_equal "/users/service/cei\n", @@rhost.exec!('pwd')
  end

  def test21_scp
    s = Time.now.utc.to_s
    assert @@rhost.write_file(s, @@fname), 'error in scp write_file'
    assert s2 = @@rhost.read_file(@@fname), 'error in scp read_file'
    assert_equal s, s2, 'wrong file content?'
    @@rhost.exec!("rm #{@@fname}")
  end

end
