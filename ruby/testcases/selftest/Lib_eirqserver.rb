=begin
(c) Copyright 2011 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: sfrieske, 2018-03-07

Version: 1.0.0

History:

Notes:
  login to f36asd27 as cei
  cd qa/junit_testing 
  ./bin/start_eirqserver_itdc.sh (start the ALC2460 EI with mappings to TRK2460 STP2460)
  (single start with: start_ei ALC2460 TRK2460 STP2460 on f36asd27 (as cei) telnet 22224)
=end

##require 'misc'
require 'rmstest'
require 'RubyTestCase'

# Test eirqserver, used in Test_RMS_Autolinks
class Lib_eirqserver < RubyTestCase
  @@eqp = 'ALC400'
  @@department = 'MSS'
  @@upload_tool = 'TRK2460'

  def self.startup
    @@rmstest = RmsTest.new($env, @@eqp, @@department)
    @@rms = @@rmstest.rms
  end
  
  def test1
    keys = {'Department'=>'MSS', 'ToolPrefix'=>'ALC', 'ToolVendor'=>'SOKUDO', 'state'=>'ACTIVE', 'SoftRev'=>'TC01-1'}
    assert robdef = @@rms.find_object_with_data_exp(keys, registration: 'RCP_DEF_PROD')
    assert rob = @@rms.trigger_recipe_upload(@@upload_tool, robdef, 'WaferFlow\\PRD\\OBAN\\PUVII950----001')
    assert @@rms.get_data(rob)
  end
  
end
