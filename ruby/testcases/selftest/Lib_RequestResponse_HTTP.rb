=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: Steffen Steidten, 2015-01-26

Version: 1.0.0

History:
=end

require 'RubyTestCase'
require 'misc/sfc'


# Test SetupFC connectivity
class Lib_RequestResponse_HTTP < RubyTestCase

  def self.startup
    # no siview
  end

  def test01
    $setup_ok = false
    #
    @@sfc = SetupFC::Client.new($env)
    assert res = @@sfc.getSpecification('XXX'), "error in SetupFC connection to the standard SOAP service"
    assert res[:getSpecificationResponse]
    #
    $setup_ok = true
  end

end
