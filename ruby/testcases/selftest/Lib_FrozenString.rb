=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author:  sfrieske, 2019-11-04

Version: 1.0.0

History:
=end

require 'RubyTestCase'


# Test frozen-string-literal: true works
class Lib_FrozenString < RubyTestCase

  def self.startup
    # no siview
  end

  def test00
    $setup_ok = false
    #
    assert_equal '--enable=frozen-string-literal', ENV['RUBYOPT'], 'missing RUBYOPT'
    #
    $setup_ok = true
  end

  def test101
  end

end
