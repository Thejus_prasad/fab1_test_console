=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: Steffen Steidten, 2011-08-02
=end

require 'yaml'
require 'RubyTestCase'

# Dummy Test to verify the test framework works.
class Test_0 < RubyTestCase
  @@param1 = 'dummy1'
  @@param2 = 0
  @@param3 = ['xyz']
  @@param4 = true
  @@pass_setup = true


  def self.startup
    $log.info "startup, called before all tests run"
    super(nosiview: true)
  end

  def self.shutdown
    $log.info "shutdown, called after all tests have been completed"
  end

  def self.after_all_passed
    $log.info "after_all_passed, called last if all tests have passed"
  end


  def test00_setup
    $setup_ok = false
    $log.info "pass_setup: #{@@pass_setup}"
    assert @@pass_setup, "QA Test Test"
    $setup_ok = true
  end

  def test10
    $log.warn "XXXX param1: #{@@param1.inspect}"
    $log.warn "YYYY param2: #{@@param2.inspect}"
  end

  def test11
    assert true, "Testing failures"
  end

  def test21
    assert true
  end

  def test22_skip
    skip "skipped"
  end

  def testdev
    $log.info "a manual test (under development)"
  end

  def testmanual
    $log.info "a manual test"
  end

end
