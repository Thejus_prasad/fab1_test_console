=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author:  sfrieske, 2019-11-19

History:
=end

require 'RubyTestCase'


# Test mm_inhibit
class Lib_SiView_inhibit < RubyTestCase
  @@inhclass = :eqp_chamber_productgroup_recipe
  @@objects1 = [['UTR003', 'TEST', 'P.UTMR-IB-RTCL.01'], ['PM1', '', '']]
  @@objects2 = [['UTR003', 'TEST', 'P.UTMR-IB-RTCL.01'], ['PM2']]
  @@reason1 = 'PENG'
  @@reason2 = 'DEDI'


  def test00_setup
    $setup_ok = false
    #
    # clean up
    [@@objects1, @@objects2].each {|objs|
      objectids, attrib = objs
      assert_equal 0, @@sv.inhibit_cancel(@@inhclass, objectids), 'error cleaning up inhibits'
      assert_empty @@sv.inhibit_list(@@inhclass, objectids), 'wrong inhibit'
    }
    #
    # set 2 inhibits
    objectids, attrib = @@objects1
    assert @@sv.inhibit_entity(@@inhclass, objectids, attrib: attrib, reason: @@reason1), 'error setting inhibit'
    objectids, attrib = @@objects2
    assert @@sv.inhibit_entity(@@inhclass, objectids, attrib: attrib, reason: @@reason2), 'error setting inhibit'
    # unfiltered inhibit_list to verify
    assert inhs = @@sv.inhibit_list(@@inhclass, objectids), 'inhibit_list error'
    assert_equal 2, inhs.size, 'wrong number of inhibits'
    assert_equal [@@reason1, @@reason2].sort, inhs.collect {|e| e.reason}.sort, 'wrong reasons'
    #
    $setup_ok = true
  end

  def test11_inhibit_list_raw
    # test reason filtering by TX parameter
    objectids, attrib = @@objects1
    [@@reason1, @@reason2].each {|reason|
      assert @@sv.inhibit_list(@@inhclass, objectids, reason: reason, raw: true), 'inhibit_list error'
      assert_equal 1, @@sv._res.strEntityInhibitions.size, 'wrong number of inhibits'
      assert_equal reason, @@sv._res.strEntityInhibitions.first.entityInhibitAttributes.reasonCode, 'wrong inhibit selected'
    }
    assert_empty @@sv.inhibit_list(@@inhclass, objectids, reason: 'NOTEX'), 'inhibit_list error'
  end

  def XXtest21_inhibit_list_attrib
    [@@objects1, @@objects2].each {|objs|
      objectids, attrib = objs
      assert inhs = @@sv.inhibit_list(@@inhclass, objectids, attrib: attrib), 'inhibit_list error'
      assert_equal 1, inhs.size, 'wrong number of inhibits'
      assert_equal attrib.first, inhs.first.entities.first.attrib, 'wrong inhibit selected'
      assert_empty @@sv.inhibit_list(@@inhclass, objectids, attrib: ['NOTEX']), 'inhibit_list error'
      assert_empty @@sv.inhibit_list(@@inhclass, objectids, attrib: [attrib.first, 'NOTEX']), 'inhibit_list error'
      assert_empty @@sv.inhibit_list(@@inhclass, objectids, attrib: ['', 'NOTEX']), 'inhibit_list error'
    }
  end

  def XXtest31_inhibit_list_rawinhibits_attrib
    [@@objects1, @@objects2].each {|objs|
      objectids, attrib = objs
      assert rawinhs = @@sv.inhibit_list(@@inhclass, objectids, attrib: attrib, raw: true), 'inhibit_list error'
      assert_equal 1, rawinhs.size, 'wrong number of inhibits'
      assert_equal attrib.first, rawinhs.first.entityInhibitAttributes.entities.first.attrib, 'wrong inhibit selected'
      assert_empty @@sv.inhibit_list(@@inhclass, objectids, attrib: ['NOTEX']), 'inhibit_list error'
      assert_empty @@sv.inhibit_list(@@inhclass, objectids, attrib: [attrib.first, 'NOTEX']), 'inhibit_list error'
      assert_empty @@sv.inhibit_list(@@inhclass, objectids, attrib: ['', 'NOTEX']), 'inhibit_list error'
    }
  end

end
