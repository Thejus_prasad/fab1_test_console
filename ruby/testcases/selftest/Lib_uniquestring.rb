=begin
(c) Copyright 2011 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: sfrieske, 2021-10-28

Version: 1.0.0

=end

require 'util/uniquestring'
require 'RubyTestCase'


# Test util/uniquestring works correctly
class Lib_uniquestring < RubyTestCase
  @@res = []
  @@nthreads = 20
  @@loops = 10000  # 10000: 400 s, 100_000: 4000 s


  def self.startup
    super(nosiview: true)
  end


  def test1
    threads = []
    tstart = Time.now
    @@nthreads.times {|i|
      threads << Thread.new {
        @@loops.times {|n|
          $log.info {"-- loop #{n+1}/#{@@loops}, thread ##{i}"} if n % 100 == 0
          @@res << unique_id
        }
      }
    }
    threads.each_with_index {|t, i|
      $log.info "joining thread #{i}"
      t.join
    }
    assert_equal @@res.uniq.size, @@res.size, 'duplicate UUIDs'
  end

end
