=begin 
Dummy Test to verify the test framework works.

(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.
    
Author: Steffen Steidten, 2011-08-02
=end

require "Test_1"

# Dummy Test to verify the test framework works.
class Test_01 < Test_1
  @@param1 = "subclass override"

  def test12
    $log.info "new tc on top of the inherited ones"
  end
  
  def test42
    $log.info "new tc on top of the inherited ones"
  end

  def self.after_all_passed
    puts "Cleanup"
  end
end
