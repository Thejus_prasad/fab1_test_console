=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author:  sfrieske, 2019-07-26

Version: 1.0.0

History:
=end

require 'RubyTestCase'
require 'rexml/document'
load 'xmlhash.rb'
load 'xmlhash2.rb'


# Test new xml_to_hash based on REXML::Document instead of Builder
class Lib_hash2xml < RubyTestCase
  @@xml = <<EOF
<r>
  <t1>
    <st1>1</st1>
    <st2>2</st2>
  </t1>
  <t2>s1</t2>
  <t2>s2</t2>
  <t3 a1='1' a2='2' a3='x'>s3</t3>
</r>  
EOF
  @@href = XMLHash.xml_to_hash(@@xml)

  def self.startup
    # no siview
  end

  def test01
    $setup_ok = false
    #
    $xmlold = XMLHash.hash_to_xml(@@href)
    $xml = XMLHash2.pretty_format(XMLHash2.hash_to_xml(@@href), true)
    assert_equal $xmlold, $xml
    #
    $setup_ok = true
  end

end

# "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<r>\n  <t1>\n    <a1>1</a1>\n    <a2>2</a2>\n  </t1>\n  <t2>s1</t2>\n  <t2>s2</t2>\n</r>\n"
# "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<r>\n  <t1>\n    <a1>1</a1>\n    <a2>2</a2>\n  </t1>\n  <t2>s1</t2>\n  <t2>s2</t2>\n</r>"
