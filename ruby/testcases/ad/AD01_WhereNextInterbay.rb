=begin 
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.
    
Author: Paul Peschel, 2013-02-01 migrate from Test101_WhereNextInterbay.java

History:
  2019-06-11 sfrieske, minor cleanup
  2020-08-13 sfrieske, minor cleanup
=end

require 'RubyTestCase'

class AD01_WhereNextInterbay < RubyTestCase
    @@ProductionBankWithTransportEqp = 'OY-SHIP'      # production bank with transport equipment
    @@BankShip = 'OY-SHIP'                            # production bank with transport equipment
    @@BankScrap = 'TX-SCRAP'                          # production bank without transport equipment
    @@BankOnRaw = 'ON-RAW'                            # vendor lot bank
    @@ProductionBankWithOutTransportEqp = 'OY-SHIP'   #'TX-SCRAP'  # bank without transport equipment
    @@bank_completed = 'OX-ENDFG'
    @@bank_shipped = 'OY-SHIP-RETURN'
    @@bank_nonpro = 'O-PACK'
    
    @@min_carriers = 1
    @@max_carriers = 10
    
    # Tests where next on a certain lot state
    def XXtest10_WhereNextVendorNoTpEqp
      # no bank in prod to find lots reliably
      runtest('V%', '', '', @@ProductionBankWithOutTransportEqp, false)
    end
    
    # Tests where next on a certain lot state
    def XXtest11_WhereNextVendorHasTpEqp
      # no bank in prod to find lots reliably
      runtest('V%', '', '', @@ProductionBankWithTransportEqp, false)
    end
    
    # Tests where next on a certain lot state
    def XXtest12_WhereNextLotCOMPLETED
      runtest('%', 'COMPLETED', '', @@bank_completed, false)
    end
	
    # Tests where next on a certain lot state
    def XXtest13_WhereNextLotSHIPPED
      # no bank in prod to find lots reliably
      runtest('%', 'SHIPPED', '', @@bank_shipped, false)
    end
	
    # Tests where next on a certain lot state  scrapped lots are not in carriers any more
    def XXtest14_WhereNextLotSCRAPPED
      runtest('%', 'SCRAPPED', '', @@BankScrap, false)
    end
  
    # Tests where next lots in a non productive bank
    def test15_WhereNextLotNonProBank
      runtest('%', '', '', @@bank_nonpro, false)
    end
  
    # Tests where next lots in a production bank
    def test16_WhereNextNoTransportEquipment
      runtest('%', '', '', @@ProductionBankWithOutTransportEqp, false)
    end

    # Tests where next for lots that are on hold
    def test17_WhereNextLotONHOLD
      runtest('%', 'ONHOLD', '', '', false)
    end
    
    # Tests where next for lots of a certain type
    def test18_WhereNextStandard
      runtest('%', '', '', @@ProductionBankWithTransportEqp, false)
    end

    # Tests where next for lots of a certain type
    def test20_WhereNextProductionLot
      runtest('%', 'Waiting', 'Production', '', false)
    end
    
    # Tests where next for lots of a certain type
    def test21_WhereNextEqpMonitorLot
      runtest('%', 'Waiting', 'Equipment Monitor', '', false)
    end

    # Tests where next for lots of a certain type
    def test22_WhereNextProcessMonitorLot
      runtest('%', 'Waiting', 'Process Monitor', '', false)
    end
	
    # Tests where next for lots of a certain type
    def test23_WhereOnRawBank
      runtest('%', '', '', @@BankOnRaw, false)
    end
	
    # Tests where next for lots of a certain type
    def test24_WhereNextNoCarrier
      runtest('%', '', '', @@BankOnRaw, true)
    end
    
    # test where_next_interbay for lots matching the search criteria
    def runtest(lotPattern, lotState, lotType, bank, empty_carrier)
      $log.info "runtest #{lotPattern.inspect}, #{lotState.inspect}, #{lotType.inspect}, #{bank.inspect}, #{empty_carrier.inspect}"
      list = @@sv.lot_list(lot: lotPattern, status: lotState, lottype: lotType, bank: bank, lotinfo: true)
      list = list.select {|l| (l.carrier.empty? && empty_carrier) || (!l.carrier.empty? && !empty_carrier)}
      assert list.count >= @@min_carriers, "at least #{@@min_carriers} carrier must be available"
      _max = [list.count, @@max_carriers].min
      $log.info "#{list.count}, #{@@max_carriers}, #{_max}"
      list.take(max).each {|li|
        $log.info "where_next_interbay #{li.lot.inspect} carrier: #{li.carrier.inspect}"
        @@sv.where_next_interbay(li.lot, carrier: li.carrier)
        assert [0, 1429].member?(@@sv.rc), 'wrong return code'
      }
    end
end
