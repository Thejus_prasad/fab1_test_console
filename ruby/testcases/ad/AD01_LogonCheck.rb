=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: ssteidte, 2011-11-18

History:
  2019-06-11 sfrieske, minor cleanup
  2019-12-11 sfrieske, cleanup use of logon_check
=end

require 'RubyTestCase'

class AD01_LogonCheck < RubyTestCase
  @@subsystem = 'MM'
  @@category = 'Report'
  @@logon_count = 10


  def test02_privileges
    assert_equal 0, @@sv.logon_check(subsystem: @@subsystem, category: @@category)
    assert_equal @@subsystem, @@sv._res.subSystemFuncLists.first.subSystemID
#    priv2 = @@sv.user_privileges(@@sv.user, @@subsystem, @@category) # no privileges for this Tx
#    assert_equal priv1, priv2, "privileges mismatch"
  end

  def test03_strange_names
    users = ["Dummy", ".", ":", "ID: XXX - ; \t \n \r \r\n , "]
    users.each {|user| assert_equal 1466, @@sv.logon_check(user: user, password: SiView.nt_pw(''))}
  end

  def test04_last_logon_time_logon
    lastlogon = Time.now - 10   # allow small differences between computer times
    $log.info "running #{@@logon_count} times..."
    @@logon_count.times {
      assert_equal 0, @@sv.logon_check(subsystem: @@subsystem, category: @@category), 'logon failed'
      logon_timestamp = @@sv.user_parameter('User', @@sv.user, name: 'LastLogOnTime')
      assert logon_timestamp.valueflag, 'LastLogOnTime parameter is not set'
      logontime = @@sv.siview_time(logon_timestamp.value)
      assert lastlogon <= logontime, "LastLogOnTime should have been updated after #{lastlogon.utc}, but is #{logontime.utc}"
      lastlogon = logontime
      sleep 0.1
  }
  end

end
