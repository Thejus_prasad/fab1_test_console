=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.
    
Author: Paul Peschel, 2013-02-25 migrate from StressEqpAndStockerInfoInq.java

History:
  2019-06-11 sfrieske, minor cleanup
=end

require 'RubyTestCase'


class ADStress_EqpAndStockerInfoInq < RubyTestCase
  @@loops = 1000
  @@allowed_areas = %w(A-TKEY UT 1-STUTT A-SKIH A-SRTB A-SSJB A-SSJF C-BEOL C-FEOL C-HK C-MOL D-FEOL E-BEOL E1-BOL E1-FOL E1-HK E1-MOL F-FEOL F-METR L-MASK L-METR L-SPRT O-LSTR O-WPCK)
  
  def test1_stress
    areas = @@sv.workarea_list
    eqps = []
    stockers = []
    (areas & @@allowed_areas).each {|a|
      # eqplist = @@sv.workarea_eqp_list(a, stocker_type: /^Shelf|Auto/)
      # eqps += eqplist.eqp
      # stockers += eqplist.stockers
      eqplist = @@sv.workarea_eqp_list(a)
      eqps += eqplist.strAreaEqp.collect {|e| e.equipmentID.identifier}
      stockers += eqplist.strAreaStocker.collect {|e| 
        e.stockerID.identifier if e.stockerType.start_with?('Shelf') || e.stockerType.include?('Auto')
      }.compact
    }
    #
    $log.info "found #{eqps.size} equipments"
    @@loops.times {|l|
      assert @@sv.eqp_info_raw(eqps.sample)
      $log.info("-- #{l}/#{@@loops}") if (l % 100 == 0)
    }
    #
    $log.info "found #{stockers.size} stockers/shelfs"
    @@loops.times {|l|
      assert @@sv.stocker_info(stockers.sample, raw: true)
      $log.info("-- #{l}/#{@@loops}") if (l % 100 == 0)
    }
  end
end