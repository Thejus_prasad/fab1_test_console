=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.
    
Author: Paul Peschel, 2013-02-22 migrate from StressTxLotInfoInq.java

History:
  2019-06-11 sfrieske, minor cleanup
=end

require 'RubyTestCase'

class ADStress_LotInfoInq < RubyTestCase
  @@max_size = 1600
  
  def test1_stress
    # create a large list of lots
    lots = @@sv.lot_list(status: 'Waiting')
    $log.info "found #{lots.size} lots"
    lotids = lots
    lotids += lots while lotids.size < @@max_size
    # increase list of lots passed to TxLotInfo from 100 up to @@max_size an see where it fails
    (100..@@max_size).step(500) {|i|
      $log.info "-- testing with #{i} lots"
      assert @@sv.lots_info_raw(lotids.take(i))
    }
  end

end
