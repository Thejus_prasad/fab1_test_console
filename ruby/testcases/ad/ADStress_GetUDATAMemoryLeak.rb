=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.
    
Author: Paul Peschel, 2013-02-22 migrate from StressGetUDATAMemoryLeak.java

History:
  2019-06-11 sfrieske, minor cleanup
=end

require 'RubyTestCase'

class ADStress_GetUDATAMemoryLeak < RubyTestCase
  @@pds = %w(10METETSTM.01 1METPAT1.01 1METPAT2.01 2BP-MSKD-28BK.01 2G-MSKD-40BK.01 
             2NB-ST.01 2NB-UDI.01 2NB-UDR.01 3C-DEPDI.01 3C-DEPDR.01)
  @@loops = 1000
   
  def test1_stress
    $log.info "testing with #{@@loops} tx calls"
    @@loops.times {|n|
      $log.info "-- loop #{n}/#{@@loops}" if n % 100 == 0
      assert @@sv.AMDGetUDATA('PD', @@pds.sample, raw: true)
    }
  end

end
