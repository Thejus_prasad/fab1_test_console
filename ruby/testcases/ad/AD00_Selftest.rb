=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: Steffen Steidten, 2011-08-02

History:
  2019-06-11 sfrieske, minor cleanup
=end

require 'RubyTestCase'


# Dummy Test to verify the test framework works.
class AD00_Selftest < RubyTestCase

  def self.startup
    super(nosiview: true)
  end

  def test01_setup
    $setup_ok = false
    $setup_ok = true
  end

  def test02_siview
    require 'siview'
    assert sv = SiView::MM.new($env), 'no MM connection'
    assert_equal 0, sv.logon_check, 'wrong username or password'
  end

  def test12
    assert_nil nil
    refute_nil false
  end

  def test11
    assert true
  end

  def test21
    assert true
  end

end
