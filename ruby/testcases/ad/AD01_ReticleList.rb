=begin 
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.
    
Author: ssteidte, 2012-05-31

History:
  2013-04-08 dsteger,  fixed query if less then max number of reticles
  2019-06-11 sfrieske, minor cleanup
=end

require 'RubyTestCase'

class AD01_ReticleList < RubyTestCase
  @@rg = '2470GQ2NBB'
  @@eqp = 'STP906'
  @@max_reticles = 2200   # as configured in MM env settings
  
  @@status = ['AVAILABLE', 'NOTAVAILABLE', 'SCRAPPED', 'INUSE']
  
  
  def test00_setup
    $setup_ok = false
    assert_equal @@max_reticles.to_s, @@sv.environment_variable[1]['SP_RETICLELIST_SEQLEN_MAX_FOR_RETICLE_LIST_INQ'], "wrong mm server env"
    $setup_ok = true
  end

  def test01_all_customized
    rl = @@sv.reticle_list(customized: true)
    assert_equal @@max_reticles, [rl.count, @@max_reticles].min, "wrong number of reticles returned, #{rl.size} instead of #{@@max_reticles}"
    assert check_reticles(rl)
  end
  
  def test02_reticle_group
    rlcore = @@sv.reticle_list(customized: false, rg: @@rg)
    rl = @@sv.reticle_list(customized: true, rg: @@rg)
    assert_equal rlcore.size, rl.size, 'different list size'
    assert check_reticles(rl)
  end
  
  def test03_eqp
    rlcore = @@sv.reticle_list(customized: false, eqp: @@eqp)
    rl = @@sv.reticle_list(customized: true, eqp: @@eqp)
    assert_equal rlcore.size, rl.size, 'different list size'
    assert check_reticles(rl)
  end
  
  def test04_status
    @@status.each {|s|
      $log.info "verify for status #{s}"
      rlcore = @@sv.reticle_list(customized: false, status: s)
      rl = (@@sv.reticle_list(customized: true, status: s) || [])
      assert_equal rlcore.size, rl.size, 'different list size'
      assert check_reticles(rl)
    }
  end
  
  
  def check_reticles(rl)
    # compare obtained list rl with reference list
    # due to size limitations, only reticle parameters with matching ids are compared,
    # there is no complaint if a reticle is not found in rl
    $log.info "check_reticles (#{rl.size})"
    test_ok = true
    rl.each_with_index {|ri, i|
      $log.info "  #{i}/#{rl.size}" if i % 100 == 0
      r = ri.reticleID.identifier
      # skip reticles with strange characters, not handled correctly
      next unless r.ascii_only?
      # find record, because reticle_list returns all reticles _starting_ with r, not just r 
      ref = @@sv.reticle_list(customized: false, reticle: r, reticleinfo: true).find {|e| e.reticle == r}
      if ref.nil?
        $log.warn "  no core reticle info for #{r.inspect}"
        test_ok = false
        next
      end
      # ref.members.each {|m|
      #   ($log.warn "  wrong info: #{ri.inspect} instead of #{ref.inspect}"; test_ok = false) if ri[m] != ref[m]
      # }
    }
    return test_ok
  end

end
