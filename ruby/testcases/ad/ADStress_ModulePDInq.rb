=begin 
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.
    
Author: Paul Peschel, 2013-02-20 migrate from StressAMD_TxModulePDInq.java

History:
  2019-06-11 sfrieske, minor cleanup
=end

require 'RubyTestCase'


class ADStress_ModulePDInq < RubyTestCase

  def test1_stress
    types = ['Production', 'Equipment Monitor', 'Process Monitor']
    types.each {|t|
      $log.info "lottype #{t.inspect}"
      lotlist = @@sv.lot_list(status: 'Waiting', lottype: t)
      lotlist.each_with_index {|lot, n|
        $log.info "-- lot #{n}/#{lotlist.size}" if n % 100 == 0
        @@sv.AMD_modulepd(lot)
        assert [0, 1450].member?(@@sv.rc), "wrong return code from AMD_TxModulePDInq for lot #{lot}"
      }
    }
  end

end
