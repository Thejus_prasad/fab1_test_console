=begin 
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.
    
Author: Paul Peschel, 2013-02-20 migrate from StressTxRouteOperationListInq.java

History:
  2019-06-11 sfrieske, minor cleanup
=end

require 'RubyTestCase'


class ADStress_RouteOperationListInq < RubyTestCase
  @@nroutes = 100
  
  def test1_stress
    routes = @@sv.lot_list(status: 'Waiting', lotinfo: true).collect {|li| li.route}.uniq.sort
    routes.take(@@nroutes).each_with_index {|route, i|
      tstart = Time.now
      assert res = @@sv.route_operations(route), "error executing TxRouteOperationListInq for #{route}"
      $log.info "-- #{i+1}/#{@@nroutes}  #{route}  duration: #{(Time.now - tstart).round(1)} s"
    }
  end
  
end
