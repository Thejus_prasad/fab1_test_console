=begin 
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.
    
Author: Daniel Steger 2005, Douglas Kaip 2010; Steffen Steidten 2013 (Ruby port)

History:
  2019-06-11 sfrieske, minor cleanup
=end

require 'RubyTestCase'


class ADStress_EqpListInq < RubyTestCase
  @@loops = 10
  
  def test1_stress
    @@loops.times {|i|
      $log.info "-- loop #{i+1}/#{@@loops}"
      was = @@sv.workarea_list
      $log.info "found work areas #{was.inspect}"
      assert was.size > 0, 'error getting work areas'
      was.each {|wa|
        assert @@sv.workarea_eqp_list(wa), "error retrieving equipment list for work area #{wa.inspect}" 
      }
    }
  end

end
