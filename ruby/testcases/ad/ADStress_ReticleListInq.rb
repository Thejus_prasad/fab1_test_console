=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.
    
Author: Paul Peschel, 2013-02-22 migrate from StressAMD_AllReticleListInq.java

History:
  2019-06-11 sfrieske, minor cleanup
=end

require 'RubyTestCase'


class ADStress_ReticleListInq < RubyTestCase
  
  def test1_stress
    'ABCDEFGHIKLMNPQRSTUVWXYZ0123456789'.chars.each {|c|
      assert res = @@sv.reticle_list(reticle: "#{c}%")
      ##assert [0, 1551, 1488].member?(@@sv.rc), "return code must be one of 0, 1551, 1488"
    }
  end

end
