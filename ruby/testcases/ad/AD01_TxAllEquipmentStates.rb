=begin 
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.
    
Author: Paul Peschel, 2013-02-01 migrate from Test071_TxAllEquipmentStatesInq.java

History:
  2013-03-27 dsteger,  removed all but test20. The rest is obsolete.
  2019-06-11 sfrieske, minor cleanup
=end

require 'RubyTestCase'

class AD01_TxAllEquipmentStates < RubyTestCase
    @@loops = 2

    def test10_AMD_TxAllEquipmentStatesInq
      # Tx returns nil (calls SAP-PM internally). Just check if there are no exceptions
      @@loops.times {assert_nil @@sv.AMD_TxAllEquipmentStatesInq}
    end
    
end
