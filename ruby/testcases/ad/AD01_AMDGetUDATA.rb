=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: ssteidte, 2012-05-31

History:
  2019-06-11 sfrieske, minor cleanup
=end

require 'RubyTestCase'

class AD01_AMDGetUDATA < RubyTestCase
  @@udata_classes = 'AREA,AREAGRP,BANK,CALENDAR,CAST,CTRLJOB,CUSTOMER,CUSTPROD,DCDEF,DCSPEC,
    DRBL,DRBLGRP,E10STATE,EQP,EQPST,LOT,LOTFAMILY,LOTNOTE,LOTSCHE,LOTTYPE,LRCP,MRCP,MSGDEF,
    PLSPLIT,PRODGRP,PRODREQ,PRODSPEC,RAWEQPST,RTCLPOD,RTCLSET,SCRIPT,STAGE,STAGEGRP,
    STK,SYSMESSAGE,TECH,TESTSPEC,TESTTYPE,USER,WAFER'

  @@udata_code_objects = 'Action Code:,AdjustChip:,AdjustDie:,BankHold:,BankHoldRelease:,
    Carrier Category:,ChipScrap:,ChipScrapCancel:,Department:,E-REWORK:,EntityInhibit:,EntityInhibitCancel:,
    Equipment Category:,Equipment Status:,Equipment Type:,FutureHold:,FutureHoldCancel:,Inspection Type:,
    Level:,Load Purpose Type:,Lot Control Use State:,LotHold:,LotHoldRelease:,Main Process Definition Typ:,
    Message Distribution Type:,Message Media:,Mfg Layer:,Operation Mode:,PPLineHold:,Photo Layer:,
    Priority Class Type:,ProcessHold:,ProcessHoldCancel:,ProcessPending:,ProcessResume:,Product Type:,
    ReticlePod Category:,Rework:,ReworkCance:'

  @@udata_fields = 'AREA,ATV,WorkAreaAlias,test1
    AREA,test,BRLocationAlias,test
    CODE,Carrier Category:FH-CC1,CarrierCatAlias,Test Category
    CODE,Department:u_test,DepartmentLetter,A,DeltaDCDefAlias,test
    CODE,Equipment Type:Test01,SchedulingQueueSize,1,BREquipTypeAlias,alias1,ABDSiViewControl,control1,SchedulingTriggerEvent,event1,SchedulingQueueSize,2,NearCompleteTrigger,trigger1,NearCompleteTriggerValue,3,ModelType,model1,Cascades,4,LimitUtilization,1.2340000
    CODE,Mfg Layer:j,MfgLayerAlias,Test Layer
    CODE,Photo Layer:14,BRPhotoLayer,Test
    CODE,PPLineHold:LineHold,UserCodeAlias,User defined code
    CODE,Priority Class Type:1,PriorityClassAlias,Test
    CODE,ReticlePod Category:RETICLE,RetPicCadAlias,Test Result
    CODE,User Group:14,UserGroupAlias,Test Result
    DCDEF,udata_test.00,DeltaDCDefAlias,udata_test
    DCSPEC,udata_spec.00,DeltaDCSpecAlias,udata_spec
    DRBLGRP,KKRETICLES,ReticleGropuAlias,test reticle set
    PD,Test_DUMMY_ROUTE.01,PC03.UsrDef1,1,BRMainProcDefAlias,2
    PD,AZ_tcm.01,ProcessTypeWfrSelection,Trial1,ProcessLayer,Trial2,SamplingReference,Trial3,ProcessTypeDispatching,Trial4,SpecificationID,Trial5
    POS,TEST_PROD_ROUTE_01.1:100.100,Note 1,myUDATA
    PRIVGRP,ARMOR.ADMIN.All,PrivGroupAlias,test_123
    RAWEQPST,UP20,RawEquipStateSetAlias,test
    SYSMESSAGE,MM.EQPOVER,SysMessageCodeAlias,test code
    USER,noexisting
    USER,tklose,FirstName,Thomas,LastName,Klose,BadgeNumber,201111,Type,Human'

  @@udata_wrong_class = 'USER1,tklose'

  @@udata_wrong_object = 'PRIVGRP,SC10'


  def test1_classes
    test_ok = true
    @@udata_classes.split(',').each {|c|
      c.strip!
      res = @@sv.AMDGetUDATA(c, '')
      ($log.warn "error getting UDATA for class #{c.inspect}"; test_ok=false) unless res
    }
    assert test_ok
  end

  def test2_code_objects
    test_ok = true
    @@udata_code_objects.split(',').each {|o|
      o.strip!
      res = @@sv.AMDGetUDATA('CODE', o)
      ($log.warn "error getting UDATA for class 'CODE', object #{o.inspect}"; test_ok=false) unless res
    }
    assert test_ok
  end

  def test3_fields
    test_ok = true
    @@udata_fields.split("\n").each {|line|
      ff = line.strip.split(',')
      $log.info "testing #{ff.inspect}"
      res = @@sv.AMDGetUDATA(ff[0], ff[1])
      ($log.warn "  error getting UDATA for #{ff[0..1].inspect}"; test_ok=false; next) unless res
      next if res == {} # from old Java test
      ($log.warn "  data mismatch for #{res.inspect}"; test_ok=false) unless ff[2..-1] == res.to_a.flatten
    }
    assert test_ok
  end

  def test8_wrong_class
    test_ok = true
    @@udata_wrong_class.split("\n").each {|line|
      $log.info "#{line.inspect}"
      ff = line.split(",")
      res = @@sv.AMDGetUDATA(*line.split(','))
      if res
        $log.warn "test must fail for UDATA #{line.inspect}"
        test_ok = false
      else
        test_ok = false unless @@sv.rc == 10902
      end
    }
    assert test_ok
  end

  def test9_wrong_object
    test_ok = true
    @@udata_wrong_object.split("\n").each {|line|
      $log.info "#{line.inspect}"
      res = @@sv.AMDGetUDATA(*line.split(','))
      if res
        $log.warn "test must fail for UDATA #{line.inspect}"
        test_ok = false
      else
        test_ok = false unless @@sv.rc == 10904
      end
    }
    assert test_ok
  end

end
