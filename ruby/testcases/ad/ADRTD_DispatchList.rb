=begin 
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.
    
Author: Steffen Steidten, 2013-07-31

History:
  2019-06-11 sfrieske, minor cleanup
=end

require 'RubyTestCase'

class ADRTD_DispatchList < RubyTestCase
    @@eqps = %w(ALC300 ALC600 ALC1365 ALC1370 ALC1402 BFI102 CDS1125 CVD322 CVD402 DFS610 
      EPI104 EPI1123 EPI4209 ETX103 ETX651 ETX1580 ETX1615 ETX1804 FVX100 FVX400 
      FVX450 FVX501 FVX605 FVX904 FVX1604 FVX4150 FVX4423 FVX4520 HLS100 IMP100 
      IMP1103 IMP1120 INS900 LSA100 MDX1808 MDX4802 MES500 MPX615 OPI100 OVL100 
      OVL300 OVN510 POL402 POL713 POL1400 PSX100 PSX213 PSX600 PSX1152 PSX4152 
      RTA101 SCB1301 SCB4401 SEM1521 SNK310 SNK383 SNK621 SNK926 SNK952 SNK1550 
      SNK4312 STP900 THK509 THK1622 TRK900 UVC1550 VDS100 WDT1100 VAMOS_NOWIP)
    @@loops = 3

    def test1
      tstart = Time.now
      # perf = {tstart: tstart, durations: []}
      tt = @@loops.times.collect {|i|
        $log.info "-- loop #{i+1}/#{@@loops}"
        tstart = Time.now
        @@eqps.each_with_index {|eqp, j|
          $log.info "eqp #{j+1}/#{@@eqps.size}"
          assert @@sv.rtd_dispatch_list(eqp)
        }
        (Time.now - tstart).round(1)
      }
      $log.info "durations per round: #{tt.join('|')}"
    end
end
