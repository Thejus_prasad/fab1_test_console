=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.
    
Author: Paul Peschel, 2013-02-22 migrate from StressAMD_TxProductListInq.java

History:
  2014-09-03 ssteidte,  replaced AMD by CS Tx
  2019-06-11 sfrieske, minor cleanup
=end

require 'RubyTestCase'


class ADStress_ProductListInq < RubyTestCase
  
  def test1_stress
    lottypes = ['Production', 'Dummy', 'Equipment Monitor', 'Process Monitor', 'Raw', 'Recycle']
    lottypes.each {|lottype|
      $log.info "lottype #{lottype.inspect}"
      tstart = Time.now
      products = @@sv.CS_product_list(lottype: lottype)
      $log.info "#{lottype} #{products.size} #{Time.now - tstart} s"
    }
  end

end
