=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: Steffen Steidten, GLOBALFOUNDRIES INC., 2011

Version:    1.4.1

History:
  2020-01-03 sfrieske, moved TraceEDA lib files into subdirectory traceeda
=end


load './ruby/set_paths.rb'
require 'optparse'
require 'traceeda/traceedatest'

def main(argv)
  # set defaults
  env = 'let'
  logfile = 'log/traceeda_exerciser.log'
  d = '1h'
  ntools = 25
  tool0 = 'LETCL1050'
  chambers = ['CHA', 'CHB', 'CHC']
  bsize = 5         # number of tools reporting at the same time
  bshift = 2.0      # time shift between batches
  ncoll = 25        # samples per report and sensor
  sinterval = 0.5   # time between samples
  timeshift = 10    # accomodate for time differences to the ETCs
  cmpratio = 4      # compare every 4th report
  cmpmaxwait = 40   # how long to wait for successful retrieval
  hist2 = true
  verbose = false
  noconsole = true
  # parse cmdline options
  optparser = OptionParser.new {|o|
    o.banner = "Usage: #{File.basename(__FILE__)} [options]"
    o.on('-e', '--env [ENV]', "environment, default is #{env}") {|s| env = s}
    o.on('-d', '--duration [DURATION]', "duration as seconds or 10m or 12h, default is #{d}") {|s| d = s}
    o.on('-c', '--coll [COLLECTIONS]', Integer, "collections per DCR, default is #{ncoll}") {|n| ncoll = n}
    o.on('-i', '--interval [INTERVAL]', Float, "sample interval, default is #{sinterval}s") {|n| sinterval = n}
    o.on('-s', '--shift [SHIFT]', Integer, "time shift to ETC, default is #{timeshift}s") {|n| timeshift = n}
    o.on('-n', '--ntools [TOOLS]', Integer, "number of tools, default is #{ntools}") {|n| ntools = n}
    o.on('-t', '--tool0 [TOOL0]', "first tool, default is #{tool0}") {|s| tool0 = s}
    o.on('-b', '--batch [SIZE,SHIFT]', "batchsize[,shift], default is #{bsize},#{bshift}") {|s|
      x,y = s.split(',')
      bsize = x.to_i
      bshift = y.to_f if y
    }
    o.on('-r', '--ratio [RATIO]', Integer, "compare every RATIO report, default is #{cmpratio}") {|n| cmpratio = n}
    o.on('-w', '--wait [SECONDS]', Integer, "time to wait for correct retrieval, default is #{cmpmaxwait}") {|n| cmpmaxwait = n}
    o.on('-2', '--nohist2', 'no comparison with 2nd historian instance') {hist2 = false}
    o.on('-v', '--verbose', 'verbose logging') {verbose = true}
    o.on('--console', 'log to console in addition to log file') {noconsole = false}
    o.on_tail('-h', '--help', 'diplay this screen') {puts o; return}
  }
  optparser.parse(*argv)
  create_logger(file: logfile, file_verbose: verbose, verbose: verbose, noconsole: noconsole)
  STDOUT.puts "logging to #{logfile}, console log: #{!noconsole}"
  # get parameters from options and run the test
  $log.info "\n\n\nTraceEDA load test #{env.inspect}, #{ntools} tools, historian2 comparison: #{hist2}, max retrieval time: #{cmpmaxwait}s\n"
  $wwtest = TraceEDA::Test.new(env, tool0: tool0, ntools: ntools, chambers: chambers, hist2: hist2, cmpmaxwait: cmpmaxwait)
  params = {duration: d, sinterval: sinterval, ncollections: ncoll, timeshift: timeshift, cmpratio: cmpratio, bsize: bsize, bshift: bshift}
  $wwtest.load_test(params)
end

main(ARGV) if __FILE__ == $0
