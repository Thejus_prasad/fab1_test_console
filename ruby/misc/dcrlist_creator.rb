=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: Steffen Steidten, GLOBALFOUNDRIES INC., 2011
=end

load './ruby/set_paths.rb'
require 'optparse'
require 'dcr_exerciser'


# create DCR lists for the dcr_exerciser
def main(argv)
  # set defaults
  listfile = 'log/dcrlist.txt'
  # parse cmdline options
  optparser = OptionParser.new {|o|
    o.banner = "Usage: #{File.basename(__FILE__)} [options] <playlist or DCRdirectory or 'dir1 dir2'>"
    o.on('-f', '--filename [filename]', 'listfile name') {|f| listfile = f}
    o.on('-v', '--verbose', 'verbose logging') {verbose=true}
    o.on_tail('-h', '--help', 'diplay this screen') {puts o; exit}
  }
  optparser.parse(*argv)
  create_logger
  #
  fnames = argv[2 .. -1]
  DCR.create_dcrlist(fnames, listfile)
end

main(ARGV) if __FILE__ == $0
