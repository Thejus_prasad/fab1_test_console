=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: sfrieske, 2019-07-08

Version: 1.0.0

=end

require 'siview'


$env = 'f1prod' #'f1prod'  # 'itdc'
$fname = 'log/vlots.txt'

#
# ------------- no changes below this line required ------------------------
#

$sv = SiView::MM.new($env, ntauth: true, user: nil, password: nil)
$entries_ok = []
$entries_fail = []

# return array of arrays of [vlot, OracleLotID, AutoSTBallowed]
def get_lots
  ret = []
  File.readlines($fname).each {|line|
    next unless line.start_with?('V')  # skip headers
    ret << line.strip.split
  }
  return ret
end

def set_params(entries)
  entries.each {|e|
    lot, olotid, _unused = e
    if olotid.nil? || olotid.empty?
      $log.warn "missing olotid"
      $entries_fail << lot
      next
    end
    $log.info "-- #{e}"
    li = $sv.lot_info(lot, wafers: true)
    if li.nil?
      $entries_fail << lot
      next
    end
    ok = true
    li.wafers.each {|w|
      res = $sv.user_parameter_change('Wafer', w.wafer, 'OracleLotID', olotid, 1, datatype: 'STRING')
      ok = false if res != 0
    }
    res = $sv.user_parameter_change('Lot', lot, 'AutoSTBallowed', 'Y', 1, datatype: 'STRING')
    ok = false if res != 0
    if ok
      $entries_ok << lot
    else
      $entries_fail << lot
      $log.info "lot failed: #{lot}"
    end
  }
  return $entries_fail
end
