=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.


Author: Steffen Steidten, 2011-05-13

History:
  2012-01-06 ssteidte, refactored to use the new Space::DCR class for creation of DCRs
  2012-11-19 ssteidte, fixed errors in runId determination (due to previous changes in xml_to_hash)
  2016-12-07 sfrieske, use lib rqrsp_mq instead of rqrsp
  2017-07-11 sfrieske, separated from space.rb
  2021-02-05 sfrieske, use instance_of instead of kind_of
=end

load './ruby/set_paths.rb'
require 'find'
require 'optparse'
require 'thread'
require 'zlib'
require 'rqrsp_mq'
require 'sil/sildb'
require 'util/charts'

Thread.abort_on_exception = false  # false is correct! true only for debugging


module DCR
  # Create load by sending previously collected (Space) DCRs
  class Exerciser
    attr_accessor :env, :mutex, :clients, :available, :clientthreads, :perflog, :dcrs_accepted, :dcrs_rejected, :dcrs_timeout

    def initialize(env, params={})
      @env = env
      nthreads = params[:nthreads] || params[:n] || 10
      @mutex = Mutex.new
      @clients = nthreads.times.collect {RequestResponse::MQXML.new("space.#{@env}")}
      @available = []
    end

    def inspect
      "#<#{self.class.name}  #{env}>"
    end

    # fnames is a dir with DCRs or a list of DCR files or any combination as array
    #   Space specific only because of response evaluation
    #
    # return array of counters
    def load_test(fnames, params={})
      $log.info "load_test #{fnames.inspect}, #{params}"
      # get list of DCR files
      $log.info "  getting DCRs from #{fnames.inspect}"
      dcrfiles = DCR.create_dcrlist(fnames)
      # initialize performance log
      @perflog = params[:perflog]
      @perflog = "log/space_exerciser_#{Time.now.strftime('%Y-%m-%d_%H-%M-%S')}.csv" unless @perflog or params[:nolog]
      if @perflog
        @perflog = File.expand_path(@perflog)
        File.binwrite(@perflog, "#Sendtime;#Filename;#Duration(ms)\n")
        $log.info "  performance log: #{@perflog}"
      end
      # calculate time to sleep between consecutive DCR submittals
      frequency = params[:frequency] || params[:f]  # DCRs per hour if set else as fast as possible
      duration = params[:duration] || params[:d]    # max time to run
      tmax = duration ? Time.now + duration : nil
      timestep = frequency ? 3600.0/frequency : 0
      timestep_noclient = [timestep/3.0, 1].max
      # start scheduler
      delete = params[:delete]
      @dcrs_accepted = @dcrs_rejected = @dcrs_timeout = 0
      @clientthreads = []
      @available = (0...@clients.size).to_a
      tnext = Time.now + timestep
      $log.info "  starting #{@clients.size} clients"
      $log.info "  sending one DCR every #{timestep}s" if timestep > 0
      $log.info "  max duration #{duration}s until #{tmax}" if duration
      $log.info "  going to send #{dcrfiles.size} DCRs"
      dcrfiles.each_with_index {|f, i|
        $log.debug {"going to send file ##{i}/#{dcrfiles.size}"}
        # get available client
        ci = nil
        loop {
          @mutex.synchronize {ci = @available.shift}
          break if ci
          $log.info 'no client available'
          sleep timestep_noclient
        }
        # submit DCR
        @clientthreads[ci] = Thread.new {
          Thread.current['name'] = "T-DCR-#{i}"
          cl = @clients[ci]
          s = open(f, 'rb') {|fh| f.end_with?('gz') ? Zlib::GzipReader.new(fh).read : fh.read}
          resp = cl.send_receive(s)
          # collect statistics
          duration = -1
          @mutex.synchronize {
            if resp
              runID = resp[:submitRunResponse][:submitRunReturn][:runID]
              $log.debug {"  runID: #{runID}"}
              if runID and runID != '0'
                $log.info "  accepted: #{f.inspect}"
                @dcrs_accepted += 1
                duration = (cl.duration * 1000).to_i
                ## not used anymore  File.delete(f) if delete
              else
                $log.warn "  rejected: #{f.inspect}"
                @dcrs_rejected += 1
              end
            else
              $log.warn "  timeout: #{f.inspect}"
              @dcrs_timeout += 1
            end
            if @perflog
              s = "#{cl.txtime.utc.iso8601(3)};#{File.basename(f)};#{duration}\n"
              open(@perflog, 'a') {|fh| fh.write(s)}
            end
            @available << ci
          }
        }
        # wait for next time slot
        break if tmax and Time.now > tmax
        tsleep = tnext - Time.now
        sleep tsleep if tsleep > 0
        tnext += timestep
      }
      @clientthreads.each {|t| t.join if t}
      DCR.performance_charts(@perflog) unless params[:nolog]
      DCR.sil_performance_charts(@env)
      return [@dcrs_accepted, @dcrs_rejected, @dcrs_timeout]
    end

  end


  # create a list of DCRs sorted by timestamp (through filename) from other lists and/or dirs of DCRs
  def self.create_dcrlist(fnames, listfile=nil)
    dcrfiles = []
    fnames = fnames.split if fnames.instance_of?(String)
    fnames.each {|fname|
      if File.directory?(fname)
        Find.find(fname) {|f| dcrfiles << File.expand_path(f) unless File.directory?(f)}
      else
        dcrfiles += File.readlines(fname).collect {|l| l.chomp}
      end
    }
    dcrfiles.sort!
    File.binwrite(listfile, dcrfiles.join("\n")) if listfile
    return dcrfiles
  end

  # create performance charts from a performance log file
  def self.performance_charts(fname)
    bname = fname[0...-File.extname(fname).size]
    lines = File.readlines(fname)
    return unless lines and lines.size > 1
    # first column: time, 3rd column: duration
    data = lines[1..-1].collect {|l|
      ff = l.split(';')
      [Time.parse(ff[0]), ff[2].to_i]
    }
    # DCR response time charts
    ch = Chart::Time.new(data)
    ch.create_chart(y: 'duration', dots: true, export: "#{bname}_chart1.png")
    ##ch.create_chart(y: 'duration', dots: true, lg: true, export: "#{bname}_chart2.png")
    ch = Chart::Histogram.new(data.collect {|e| e.last})
    ch.create_chart(title: File.basename(bname), export: "#{bname}_hist.png")
  end

  # create performance charts from SILDB data
  def self.sil_performance_charts(env, params={})
    sildb = SIL::DB.new(env)
    component_methods = %w(run getGenericKeys getResponsibleRuns getSpecLimit doAutoCharting) # dont work: "createSpaceRununits",
    component_methods.each {|m|
      $log.info "creating chart #{m}"
      data = sildb.perfdata(m)
      ch = Chart::Time.new(data)
      ch.create_chart(title: m, y: '', dots: true, export: "log/sil_performance_#{m}.png")
    }
  end

end


# starter, moved from misc/space_exerciser here
def main(argv)
  # set defaults
  env = nil
  perflog = nil
  nthreads = 10
  frequency = nil
  duration = nil
  verbose = false
  noconsole = true
  # parse cmdline options
  optparser = OptionParser.new {|o|
    o.banner = "Usage: #{File.basename(__FILE__)} [options] DCRdirectory"
    o.on('-e', '--env [ENV]', 'environment (e.g. "let", "itdc")') {|s| env = s}
    o.on('-p', '--perflog [FILE]', 'performance log file') {|f| perflog = f}
    o.on('-d', '--duration [DURATION]', Integer, 'max test duration in seconds, default is as long as data exist') {|n| duration = n}
    o.on('-f', '--frequency [FREQ]', Integer, 'number of DCRs per hour, default is as fast as possible') {|n| frequency = n}
    o.on('-n', '--nthreads [THREADS]', Integer, "number of threads, default is #{nthreads}") {|n| nthreads = n}
    o.on('-v', '--verbose', 'verbose logging') {verbose = true}
    o.on('--console', 'log to console in addition to log file') {noconsole = false}
    o.on_tail('-h', '--help', 'diplay this screen') {puts o; exit}
  }
  optparser.parse(*argv)
  # create logger
  STDOUT.puts "logging to log/space_exerciser.log"
  create_logger(file: 'log/space_exerciser.log', verbose: verbose, noconsole: noconsole)
  # get parameters from options
  params = {perflog: perflog, f: frequency, d: duration}
  # run the test
  dir = argv[-1]
  ($log.warn "missing DCRdirectory, run #{File.basename(__FILE__)} -h"; exit) unless dir and !dir.start_with?('-')
  res = DCR::Exerciser.new(env, nthreads: nthreads).load_test(dir, perflog: perflog, f: frequency, d: duration)
  $log.info "\ncompleted:\n#{res}"
end

main(ARGV) if __FILE__ == $0
