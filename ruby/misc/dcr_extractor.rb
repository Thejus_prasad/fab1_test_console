=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: Steffen Steidten, 2011

History:
  2013-10-23 dsteger,  separated from space.rb
  2015-06-22 ssteidte, removed siview_timestamp
  2017-06-30 sfrieske, remove Aspect
  2021-02-05 sfrieske, use instance_of instead of kind_of
=end

load './ruby/set_paths.rb'
require 'optparse'
require 'remote'
require 'zlib'
require 'fileutils'


# EI DCR Extractor
module DCR
  class EIServer
    attr_accessor :rhost, :logdir, :logfile, :marker, :starttag, :endtag

    def initialize(rhinstance, params={})
      @rhost = RemoteHost.new(rhinstance, params)
      @logdir = params[:logdir] || '/local/logs/eilogs'
      @logfile = params[:logfile] || 'space'
      @starttag = params[:starttag] || '<?xml version'
      @endtag = params[:endtag] || '</SOAP-ENV:Envelope>'
      @marker = params[:marker] || 'reportingTimeStamp'
    end

    # return array of directories matching the tool naming convention
    def list_tool_folders(params={})
      $log.info "list_tool_folders"
      entries = @rhost.exec!("cd #{@logdir}; find . -maxdepth 1 -type d").split || ($log.warn "no entries found"; return)
      entries.collect {|e|
        e = e.chomp[2..-1]                       # remove leading "./" from find output
        e =~ /^[A-Z]{3,6}+[0-9]{0,4}/ ? e : nil  # tool naming convention
      }.compact.sort
    end

    # get logfiles for the specified tools, detect tools if nil
    # write logfiles to the :download_to location if specified
    # age is specified by :mtime, defaulting to 10 days back
    #
    # return hash with a list of files per tool
    def get_logfiles(params={})
      ret = {}
      tools = params[:tools] || list_tool_folders
      tools = [tools] if tools.instance_of?(String)
      download_to = params[:download_to]  # only for dubugging
      mtime = params[:mtime] || 10
      $log.info "get_logfiles mtime: #{mtime}, download_to: #{download_to.inspect}, tools: #{tools}"
      tools.each {|tool|
        res = @rhost.exec!("cd #{@logdir}/#{tool}; find . -maxdepth 1 -type f -mtime -#{mtime}")
        if res
          files = res.split.collect {|e|
            e = e.chomp[2..-1]   # remove leading "./" from find output
            e.include?(@logfile) ? e : nil
          }.compact.sort
          ret[tool] = files
          if download_to
            tgtdir = File.join(download_to, tool)
            FileUtils.mkdir_p(tgtdir)
            files.each {|f| @rhost.download!(File.join(@logdir, tool, f), tgtdir)}
          end
        else
          ret[tool] = []
        end
      }
      ret
    end

    # extract DCRs from a string or file and write them to the directory specified by :write_to
    #
    # return number of DCRs read or (on return_dcrs: true) array of DCRs as string
    def get_dcrs(tool, file, params={})
      $log.info "get_dcrs #{tool.inspect}, #{file.inspect}, #{params}"
      if params[:data]
        s = params[:data]  # pass string directly
      else
        # ignore tool and file, get read from the specified local or remote file
        if fname = params[:localfile]
          s = open(fname, 'rb') {|fh| fname.end_with?('gz') ? Zlib::GzipReader.new(fh).read : fh.read}
        else
          s = @rhost.read_file(File.join(@logdir, tool, file))
        end
      end
      return_dcrs = params[:return_dcrs]
      ndcrs = 0
      dcrs = [] if return_dcrs
      i = 0
      while i
        imarker = s.index(@marker, i) || break
        istart = i
        while true
          _i = s.index(@starttag, istart + 1)
          break unless _i and _i < imarker
          istart = _i
        end
        iend = s.index(@endtag, istart)
        ##$log.debug {"#{endtag}, #{iend}, #{s[istart, 100]}"}
        ($log.warn "  istart: #{istart}: end tag not found"; break) unless iend
        dcr = s[istart..iend + @endtag.size - 1]
        ndcrs += 1
        dcrs << dcr if return_dcrs
        i = iend + @endtag.size + 1
        # write to DCR directory, by date and hour
        if write_to = params[:write_to]
          t = Time.parse(s[imarker+20..imarker+43])
          subdir = "#{write_to}/#{t.strftime('%F')}/#{t.strftime('%H')}"
          fname = "#{subdir}/#{t.strftime('%F-%H.%M.%S.%6N')}_#{tool}.xml.gz"
          $log.debug {"  istart: #{istart}, iend: #{iend}, writing #{fname}"}
          begin
            FileUtils.mkdir_p(subdir)
            Zlib::GzipWriter.open(fname) {|fh| fh.write(dcr)}
          rescue => e
            $log.warn $!
          end
        end
      end
      return return_dcrs ? dcrs : ndcrs
    end

  end


  # e.g. DCR.extract_dcrs("cei.itdc", hosts: ["f36eid1, "f36eid2"], write_to: "space_dcrs", mtime: 10)
  # or DCR.extract_dcrs(..., write_to: "apc_dcrs", logfile: "apc", marker: 'requestSentTimestamp')
  #
  # return Hash of tools and respective logfiles per server
  def self.extract_dcrs(rhinstance, hosts, params)
    $log.info "extract_dcrs #{rhinstance.inspect}, #{hosts}, #{params}"
    ret = {}
    hosts.each {|h|
      $log.info "connecting to host #{h.inspect}"
      srv = EIServer.new(rhinstance, host: h, logfile: params[:logfile])
      unless srv.rhost.ssh
        $log.warn "error connecting to host #{h.inspect}"
        ret[h] = nil
        next
      end
      $logfiles = logfiles = srv.get_logfiles(mtime: params[:mtime])
      ret[h] = logfiles
      logfiles.each_pair {|tool, files|
        files.each {|file| srv.get_dcrs(tool, file, write_to: params[:write_to])}
      }
    }
    return ret
  end

end


# starter
def main(argv)  # e.g. --console -v -e f36eip1 -t 3 ../dcrs
  # set defaults
  rhinstance = 'ceieng36.prod'
  hosts = %w(f36eip1 f36eip2 f36eip3 f36eip4 f36eip5 f36eip6 f36eip7 f36eip8 f36eip9
    f36eip10 f36eip11 f36eip12 f36eip13 f36eip14 f36eip15 f36eip16 f36eip17 f36eip18 f36eip19
    f36eip20 f36eip21 f36eip22 f36eip23 f36eip24 f36eip25 f36eip26 f36eip27 f36eip28 f36eip29
  )
  dcrfile = 'space'
  mtime = 10
  verbose = false
  noconsole = true
  # parse cmdline options
  optparser = OptionParser.new {|o|
    o.banner = "Usage: #{File.basename(__FILE__)} [options] DCRdirectory"
    o.on('-d', '--dcrfile [DCRFILE]', "EI log file with DCRs (#{dcrfile})") {|d| dcrfile = d}
    o.on('-e', '--hosts [EISrv1+EISrv2]', "EI servers") {|e| hosts = e.split('+')}
    o.on('-t', '--mtime [MTIME]', Integer, 'number of days back to look for DCR files') {|n| mtime = n}
    o.on('-u', '--user [USER]', "remote user instance, e.g. #{rhinstance}") {|s| rhinstance = s}
    o.on('-v', '--verbose', 'verbose logging') {verbose = true}
    o.on('--console', 'log to console in addition to log file') {noconsole = false}
    o.on_tail('-h', '--help', 'diplay this screen') {puts o; exit}
  }
  # get DCR directory
  args = optparser.parse(*argv)
  dir = args[0]
  (STDOUT.puts "missing DCRdirectory, run #{File.basename(__FILE__)} -h"; exit) unless dir && !dir.start_with?('-')
  # create logger
  STDOUT.puts "logging to log/space_extractor.log"
  create_logger(file: 'log/space_extractor.log', verbose: verbose, noconsole: noconsole)
  # get the files
  DCR.extract_dcrs(rhinstance, hosts, {logfile: dcrfile, mtime: mtime, write_to: dir})
end

main(ARGV) if __FILE__ == $0
