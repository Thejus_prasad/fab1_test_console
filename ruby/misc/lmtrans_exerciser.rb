=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: Steffen Steidten, Conny Stenzel, GLOBALFOUNDRIES INC., 2013

Example:
./bin/lmtrans_exerciser.sh -e 'dev' -d 120 -i 1
./bin/lmtrans_exerciser.sh -e 'let' -d 120 -i 30 -j 0.5 -n 1 -m 10

=end


load './ruby/set_paths.rb'
require 'optparse'
require 'lmtest' # old: lm.rb

def main(argv)
  # set defaults
  env = "dev"
  #env = "let"
  logfile = "log/lmtrans_exerciser.log"
  d = "60m"
  lotsuminterval = 600   # time between samples
  lmreqinterval = 1.15   # time between samples 1.15 is ~100000/Day
  nlotsumthreads = 6
  nlmreqthreads = 1
  lotsummessagesize = 1000
  lotsummessages = 12
  verbose = false
  noconsole = true
  # parse cmdline options
  optparser = OptionParser.new {|o|
    o.banner = "Usage: #{File.basename(__FILE__)} [options]"
    o.on('-e', '--env [ENV]', "environment, default is #{env}") {|s| env = s}
    o.on('-d', '--duration [DURATION]', "duration as seconds or 10m or 12h, default is #{d}") {|s| d = s}
    o.on('-i', '--lsuminterval [LSINTERVAL]', Float, "sample interval, default is #{lotsuminterval}s") {|n| lotsuminterval = n}
    o.on('-j', '--lreqinterval [LMINTERVAL]', Float, "sample interval, default is #{lmreqinterval}s") {|n| lmreqinterval = n}
    o.on('-n', '--nlotsumthreads [LSTHREADS]', Integer, "number of lotsummary threads, default is #{nlotsumthreads}") {|n| nlotsumthreads = n}
    o.on('-m', '--nlmreqthreads [LMTHREADS]', Integer, "number of lmrequest threads, default is #{nlmreqthreads}") {|n| nlmreqthreads = n}
    o.on('-s', '--lotsummessagesize [LSSIZE]', Integer, "number of lotsummary entries in a message, default is #{lotsummessagesize}") {|n| lotsummessagesize = n}
    o.on('-c', '--lotsummessages [LSCOUNT]', Integer, "number of lotsummary messages to be send, default is #{lotsummessages}") {|n| lotsummessages = n}
    o.on('-v', '--verbose', 'verbose logging') {verbose = true}
    o.on('--console', 'log to console in addition to log file') {noconsole = false}
    o.on_tail('-h', '--help', 'diplay this screen') {puts o; exit}
  }
  optparser.parse(*argv)
  create_logger(:file=>logfile, :file_verbose=>verbose, :verbose=>verbose, :noconsole=>noconsole)
  STDOUT.puts "logging to #{logfile}, console log: #{!noconsole}"
  # get parameters from options
  params = {:duration=>d, :lotsuminterval=>lotsuminterval, :lmreqinterval=>lmreqinterval, :nlotsumthreads=>nlotsumthreads, :nlmreqthreads=>nlmreqthreads, :lotsummessagesize=>lotsummessagesize, :lotsummessages=>lotsummessages}
  # run the test
  $log.info "\n\n\nLotManager load test #{env.inspect}, #{params.inspect}"
  LotManager::Exerciser.new(env, params).load_test(params)
end

main(ARGV) if __FILE__ == $0
