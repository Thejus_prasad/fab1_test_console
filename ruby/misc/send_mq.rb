=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: Steffen Steidten, GLOBALFOUNDRIES INC., 2011
=end

load './ruby/set_paths.rb'
require 'optparse'
require 'rqrsp_mq'


# create DCR lists for the dcr_exerciser
def main(argv)
  instance = 'space.let'
  optparser = OptionParser.new {|o|
    o.banner = "Usage: #{File.basename(__FILE__)} [-e ENV] filename"
    o.on('-i', '--instance [MQ Instance]', "MQ instance, default: #{instance}") {|s| instance = s}
    # o.on('-v', '--verbose', 'verbose logging') {verbose=true}
    o.on_tail('-h', '--help', 'diplay this screen') {puts o; exit}
  }
  optparser.parse(*argv)
  create_logger
  #
  fname = argv[-1]
  $log.info "#{File.basename(__FILE__)} #{instance.inspect} #{fname.inspect}"
  s = File.binread(fname)
  mq = RequestResponse::MQ.new(instance)
  res = mq.send_receive(s)
  File.binwrite(fname + 'response.txt', res)
end

main(ARGV) if __FILE__ == $0
