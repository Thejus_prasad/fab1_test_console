=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: sfrieske, 2019-11-18

Version: 1.0.0

Notes:
  based on FSDR report: http://vfc1qlikp01/QvAJAXZfc/opendoc.htm?document=fsdr%5Cfab1_fsdr_inhibits.qvw&lang=en-US&host=QVS%40FC1Cluster
=end

require 'csv'
require 'fileutils'
require 'time'
require 'siview'


class InhibitsAddSublottype
  # maps report 'Type' to SiView::MM::InhibitClasses
  IClasses = {
    'Chamber'                                             => :eqp_chamber,
    'Chamber/Machine Recipe'                              => :eqp_chamber_recipe,
    'Chamber/Machine Recipe/Product Group'                => :eqp_chamber_productgroup_recipe,
    'Chamber/Process Definition'                          => :eqp_chamber_pd,
    'Chamber/Process Definition/Product Group'            => :eqp_chamber_productgroup_pd,
    'Chamber/Product Group'                               => :eqp_chamber_productgroup,
    'Chamber/Product Specification'                       => :eqp_chamber_product,
    'Chamber/Technology'                                  => :technology_eqp_chamber,
    'Equipment'                                           => :eqp,
    'Equipment/Machine Recipe'                            => :eqp_recipe,
    'Equipment/Machine Recipe/Product Group'              => :productgroup_eqp_recipe,
    'Equipment/Process Definition'                        => :pd_eqp,
    'Equipment/Process Definition/Product Group'          => :productgroup_pd_eqp,
    'Equipment/Product Group'                             => :productgroup_eqp,
    'Equipment/Product Specification/Route'               => :route_opNo_product_eqp, # ??
    'Equipment/Reticle'                                   => :reticle_eqp,
    'Equipment/Technology'                                => :technology_eqp,
    'Machine Recipe'                                      => :recipe,
    'Machine Recipe/Process Definition/Product Group'     => :productgroup_pd_recipe,
    'Process Definition/Product Group'                    => :productgroup_pd,
    'Product Specification/Route'                         => :route_opNo_product,     # ??
    'Reticle'                                             => :reticle
  }

  # maps InhibitClasses' object class to report column name
  IClassColnames = {
    "Chamber"                 => 'Eqp',             # attrib 'Chamber'
    "Equipment"               => 'Eqp',
    "Machine Recipe"          => 'Machine Recipe',
    "Operation"               => 'Route',           # attrib 'OperNo'
    "Process Definition"      => 'PD',
    "Product Group"           => 'ProdGrp',
    "Product Specification"   => 'Product',
    "Reticle"                 => 'Reticle',
    "Technology"              => 'Technology'
  }

  # maps InhibitClasses object class attribs to report column name
  IClassAttribColnames = {
    'Chamber'   => 'Chamber',
    'Operation' => 'OperNo'
  }

  # maps other columns to inhibit variables
  OtherColnames = {
    classname:    'Type',
    owner:        'Requestor',
    reason:       'Reason',
    sublottypes:  'Sub Lot Type',
    tstart:       'Start',
    tend:         'End',
    memo:         'Memo'
  }

  attr_accessor :logprefix, :sv, :slt_add, :slt_marker, :reasons_exclude, :tend, :memoprefix, :reportfile, :colindexes, :_rowsf, :_inhs,
    :owner_fallback, :owners_valid, :owners_invalid, :inhs_fail, :inhs_cancel_fail, :inhs_update_fail


  def initialize(sv, slt_add, slt_marker, params={})
    @sv = sv
    @owner_fallback = params[:owner_fallback] || @sv.user
    @owners_valid = []
    @owners_invalid = []
    @slt_add = slt_add
    @slt_marker = slt_marker
    @reasons_exclude = params[:reasons_exclude]
    @tend = String.new(params[:tend] || '2037-12-31-00.00.00.000101')
    @memoprefix = params[:memoprefix] || "Added SLT #{@slt_add}"
    @logprefix = "log/inhibit_add_slt/#{Time.now.utc.strftime('%F_%T').gsub(':', '-')}_#{@slt_add}/#{@slt_add}"
    FileUtils.mkdir_p(File.dirname(@logprefix))
    @inhs_fail = []
    @inhs_cancel_fail = []
    @inhs_update_fail = []
  end

  def inspect
    "#<InhibitsAddSublottype #{@slt_add} (marker: #{@slt_marker})>"
  end

  # all in one, return array of inhibits selected by report file and filtered
  def get_inhibits_from_report(fname, col_sep=',')
    $log.info "get_inhibits_from_report #{fname.inspect}"
    rows = read_report(fname) || return
    rowsf = filter_report(rows)
    return get_inhibits(rowsf)
  end

  # return the CSV table as array of arrays
  def read_report(fname, col_sep=',')
    $log.info "read_report #{fname.inspect}"
    @reportfile = fname
    rows = CSV.read(fname, col_sep: col_sep, encoding: 'binary')
    # remove BOM
    rows[0][0] = 'Type' if rows[0][0].end_with?('Type')
    # encoding error: rows[0][0] = c00.slice(3) if c00.start_with?("\xEF\xBB\xBF")
    # resolve columns
    ret = true
    @colindexes = Hash[(IClassColnames.values + IClassAttribColnames.values + OtherColnames.values).uniq.sort.collect {|n|
      idx = rows.first.find_index {|c| c == n}
      if idx.nil?
        $log.warn "no column '#{n}'"
        ret = false
      end
      [n, idx]
    }]
    return ret ? rows : nil
  end

  def filter_report(rows)
    $log.info "filter_report  (#{rows.size} rows)"
    idxr = @colindexes[OtherColnames[:reason]]
    idxs = @colindexes[OtherColnames[:sublottypes]]
    return @_rowsf = rows[1 .. -1].select {|row|
      # skip reasons_exclude
      next if !@reasons_exclude.nil? && @reasons_exclude.include?(row[idxr])
      # select entries with makrker SLT and not new SLT
      cells = row[idxs]
      next if cells.nil?
      slts = cells.split(',')
      slts.include?(@slt_marker) && !slts.include?(@slt_add)
    }
  end

  def get_inhibits(rows)
    $log.info "get_inhibits  (#{rows.size} rows)"
    ret = []
    rows.each_with_index {|row, idx|
      classname, objectids, params, filterattribs = create_inhibitlist_parameters(row)
      inhs = @sv.inhibit_list(classname, objectids, params)
      sel = 0
      inhs.each {|inh|
        # need to filter again for sublottypes!
        slts = inh.entityInhibitAttributes.subLotTypes.to_a
        next unless slts.include?(@slt_marker) && !slts.include?(@slt_add)
        # need to filter for attribs
        aok = true
        filterattribs.each_pair {|oname, attrib|
          entity = inh.entityInhibitAttributes.entities.find {|e| e.className == oname}
          aok = !entity.nil? && entity.attrib == attrib
        }
        next unless aok
        # found a matching inhibit
        sel += 1
        ret << inh
      }
      $log.warn "row #{idx}: selected #{sel} inhibits from #{inhs.size}" if sel > 1
    }
    @_inhs = ret
    if ret.size != rows.size
      $log.warn "inhibit count mismatch: expected #{rows.size}, got #{ret.size}"
      return
    else
      return ret
    end
  end

  # internally used, separated for dev
  def create_inhibitlist_parameters(row)
    classname = IClasses[row[@colindexes[OtherColnames[:classname]]]]
    objectids = SiView::MM::InhibitClasses[classname].collect {|oname|
      row[@colindexes[IClassColnames[oname]]]
    }
    params = {
      owner:    row[@colindexes[OtherColnames[:owner]]],
      reason:   row[@colindexes[OtherColnames[:reason]]],
      raw: true
    }
    filterattribs = []
    SiView::MM::InhibitClasses[classname].each {|oname|
      attribcolname = IClassAttribColnames[oname]
      filterattribs << [oname, row[@colindexes[attribcolname]]] if attribcolname
    }
    return [classname, objectids, params, Hash[filterattribs]]
  end

  # internally used, separated for dev
  def validate_owners(inhs)
    $log.info "validate_owners"
    inhs.each {|inh|
      iinfo = @sv._extract_inhibit_info(inh)
      [iinfo.owner, iinfo.inhibit_owner].each {|u|
        next if u.nil? || @owners_valid.member?(u) || @owners_invalid.member?(u)
        if @sv.user_desc(u)
          @owners_valid << u
        else
          @owners_invalid << u
        end
      }
    }
    return @owners_invalid
  end

  def add_slt(inhs)
    puts "add_slt  (#{inhs.size} inhibits)"
    validate_owners(inhs)
    logfile = "#{@logprefix}_#{Time.now.utc.strftime('%F_%T').gsub(':', '-')}.log"
    fh = File.new(logfile, 'wb')
    fh.write("treating #{inhs.size} inhibits\n")
    $log.info "treating #{inhs.size} inhibits\n"
    inhs.each {|inh|
      iinfo = @sv._extract_inhibit_info(inh)
      fh.write("\niid: #{iinfo.iid}, tend: #{iinfo.tend}")
      #
      # prepare memo and valid owner, set new inhibit with slt added
      memo = iinfo.memo
      memo = memo[1...-1] if memo.start_with?('"') && memo.end_with?('"')
      memo = memo.start_with?('MSR') ? memo.sub(/MSR.+;/, @memoprefix) : @memoprefix + memo
      newparams = {
        inhibit: inh,
        tend: @tend == iinfo.tend ? @tend.next : @tend,   # this is part of SiView's inhibit key and must be changed
        sublottypes: iinfo.sublottypes.reject {|e| e == 'Correlation'} << @slt_add,
        memo: memo,
        raw: true
      }
      if @owners_invalid.member?(iinfo.owner)
        newparams[:owner] = @owner_fallback
        newparams[:memo] = "OrigOwner:#{iinfo.owner};#{memo}"
      end
      inhnew = @sv.inhibit_entity(nil, nil, newparams)
      if inhnew.nil?
        $log.warn "setting new inhibit failed, iid: #{iinfo.iid}"
        fh.write(', setting new inhibit failed')
        @inhs_fail << inh
        next
      end
      fh.write(", iidnew: #{inhnew.entityInhibitID.identifier}")
      #
      # update tend, inhibit_owner and release_time
      iparams = {
        comment: newparams[:memo],
        inhibit_owner: iinfo.inhibit_owner,
        release_time: iinfo.release_time
      }
      if !iinfo.inhibit_owner.nil? && @owners_invalid.member?(iinfo.inhibit_owner)
        iparams[:inhibit_owner] = @owner_fallback
      end
      if !iinfo.release_time.nil? && iinfo.release_time < @sv.siview_timestamp
        iparams[:release_time] = @tend
      end
      iparams[:tend] = iinfo.tend if iinfo.tend != '1901-01-01-00.00.00.000000' # SiView accepts but does not honor 1901
      rc = @sv.inhibit_update(inhnew, iparams)
      if rc != 0
        $log.warn ', updating new inhibit failed'
        fh.write(', error: update failed')
        @inhs_update_fail << inhnew
        next
      end
      #
      # cancel old inhibit
      rc = @sv.inhibit_cancel(nil, nil, inhibits: [inh])
      if rc != 0
        $log.warn ', canceling old inhibit failed'
        fh.write(', error: cancel failed')
        @inhs_cancel_fail << inh
        next
      end
    }
    fh.close
    return {inhibit_failed: @inhs_fail.size, cancel_failed: @inhs_cancel_fail.size, update_failed: @inhs_update_fail.size}
  end

end
