=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.


Author:     sfrieske, 2015-12-07

Version:    1.0.0

History:
=end

load './ruby/set_paths.rb'
set_paths

require 'optparse'
require 'ecm'


# get EC summaries
def main(argv)
  # defaults
  envs = ['mtdc1', 'itdc', 'f1prod']
  host = ''
  port = 43210
  reportinterval = 300
  # options
  OptionParser.new do |opts|
    opts.banner = "Usage: #{__FILE__} [options]"
    opts.on("-a", "--addr HOST", String, "server address, default: #{host}") {|v| host = v}
    opts.on("-p", "--port PORT", Integer, "set port, default: #{port}") {|v| port = v}
    opts.on('-r', '--reportinterval INTERVAL', Integer, "report interval (s), default is #{reportinterval}") {|e| reportinterval = e}
    opts.separator ''
    opts.on("-h", "--help", "Show this help message.") {puts opts; exit}
  end.parse!

  create_logger(file: "log/ecsummary.log", verbose: false)
  # start reporters
  clients = {}
  threads = envs.collect {|env|
    clients[env] = ECM::Session.new(env, instance: "ecm_reader.#{env}")
    Thread.new(clients[env]) {|client|
      Thread.current['name'] = client.env
      loop {
        $log.info "getting summary"
        client.ecroute_summary(export: true)
        $log.info "writing summary"
        sleep reportinterval
      }
    }
  }

  # start report server
  require 'rack'
  app = Rack::Builder.new {
    envs.each {|env|
      map("/ecsummary/#{env}") {run Rack::File.new("#{Dir.pwd}/log/ecsummary_#{env}.xml")}
    }
  }.to_app
  Rack::Server.start(Host: host, Port: port, server: 'webrick', environment: 'production', quiet: true, app: app)
end

main(ARGV) if __FILE__ == $0
