=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author:   Steffen Steidten, 2012-11-19

Version:  2.0.3

History:
  2015-05-28 ssteidte, moved Armor::Exerciser from armor.rb to armor_exerciser.rb
  2019-01-22 sfrieske, minor cleanup
  2021-02-05 sfrieske, use instance_of instead of kind_of

Notes:
  ARMOR Exerciser. Load recipes from an ARMOR system, simulating EI recipe requests.
  Inspired by getArmorRecipeTreestats.sh by Jörg Nothnagel.
  example calls:
  ./bin/armor_exerciser.sh -e 'let' -d 1800 -i 1 -s 1 ../data/armor/payload\-treestats.csv
  ./bin/armor_exerciser.sh -e 'let' -r '/system/plugins/renderers/xmlrendererold' -d 120 -i 1000 -s 1 ../data/armor/payload\-treestats\-renderer.csv
=end


load './ruby/set_paths.rb'
require 'optparse'
require 'thread'
require 'armor'
require 'charts'
require 'remote'
require 'ril'
require 'xmlhash'


module Armor

  class Hosts < RemoteHostGroup
    attr_accessor :payloadfiles

    def initialize(name, params={})
      super
      @armorlogdir = params[:armorlogdir] || '/local/logs/armor'
    end

    # get_payloadfiles(payloadfilepattern: "payload-treestats.csv.*"), return true
    def get_payloadfiles(params={})
      @payloadfiles = []
      save_at = params[:save_at] || 'log/armor/data'
      FileUtils.mkdir_p(save_at)
      # get payload log files
      payloadfilepattern = params[:payloadfilepattern] || "payload-treestats.csv.*"
      $log.info "get_payloadfiles payloadfilepattern: #{payloadfilepattern.inspect}, save_at: #{save_at.inspect}"
      @hosts.each {|host|
        res = host.exec!("cd #{@armorlogdir}; find #{payloadfilepattern} -maxdepth 0 -type f") || next
        res.split.each {|f|
          local = "#{save_at}/#{host.hostname}_#{f}"  #
          host.download!(File.join(@armorlogdir, f), local)
          @payloadfiles << local
        }
      }
      $log.info "  retrieved #{@payloadfiles}"
      # copy into one file
      payloadfile = params[:payloadfile] || "#{save_at}/payload-treestats.csv"
      File.open(payloadfile, 'wb') {|fh|
        @payloadfiles.each {|local| fh.write(File.read(local))}
      }
      $log.info "  concatenated to #{payloadfile.inspect}"
      true
    end

  end


  PayloadEntry = Struct.new(:timestamp, :installid, :startpath, :otherpath, :resolvedpath, :context, :rilcontext)

  # Create load by sending request collected from Production logs to the test system
  class Exerciser
    attr_accessor :env, :clients, :clientthreads, :perflog, :rcps_ok, :rcps_error, :rcps_timeout, :payload_entries

    def initialize(env, params={})
      @env = env
    end

    def inspect
      "#<#{self.class.name}, env: #{env}>"
    end

    # read the local payload file, return true on success
    def read_payloadfile(fname)
      $log.info "read_payloadfile #{fname.inspect}"
      # parse payload file
      @payload_entries = File.readlines(fname).collect {|line|
        next if line.start_with?('requesttime')  # skip header
        ff = line.split(';')
        next unless ff.size == 6
        ff[0] = Time.parse(ff[0])
        PayloadEntry.new(*ff)
      }.compact.sort {|a, b| a.timestamp <=> b.timestamp}
      $log.info "  loaded payload file"
      return true
    end

    # create RIL contexts from @payload_entries' contexts
    def create_ril_contexts
      $log.info "create_ril_contexts"
      @payload_entries.each_with_index {|entry, i|
        $log.info "  #{i}/#{@payload_entries.size}" if i % 1000 == 0
        nvpairs = XMLHash.xml_to_hash(entry.context)[:fab36_context][:nvpair]
        nvpairs = [nvpairs] unless nvpairs.instance_of?(Array)
        kvs = ['RecipeName', 'RecipeNameSpaceID', 'Equipment'].collect {|k|
          nv = nvpairs.find {|e| e[:name] == k}
          [k, nv[:value]] if nv
        }
        entry.rilcontext = Hash[kvs]
      }
      return true
    end

    def load_test(fname, params={})
      $log.info "load_test #{fname.inspect}"
      read_payloadfile(fname) || return if fname
      create_ril_contexts || return if fname && params[:rmsarmor]
      # initialize performance log
      @perflog = params[:perflog]
      @perflog = "log/armor_perf_#{Time.now.utc.iso8601.gsub(':', '-')}.csv" unless @perflog || params[:nolog]
      if @perflog
        open(File.expand_path(@perflog), 'wb') {|fh| fh.write("#Sendtime;#Requestnumber;#Duration(s)\n")}
        $log.info "  performance log: #{@perflog}"
      end
      nthreads = params[:nthreads] || 10
      @clients = Queue.new
      if params[:rmsarmor]
        nthreads.times {@clients << RMS::RIL.new(@env, 'ETX100', params)}
        @armorclients = Queue.new
        (@clients.size * 3).times {@armorclients << Armor::EI.new(@env, nil, {timeout: 100}.merge(params))}
      else
        nthreads.times {@clients << Armor::EI.new(@env, nil, {timeout: 100}.merge(params))}
      end
      #
      iterations = params[:iterations] || 1
      speed = (params[:speed] || 1).to_f
      duration = params[:duration]    # max time to run
      tmax = duration ? Time.now + duration : nil
      $log.info "\n\n  starting test with #{@payload_entries.size} requests, #{iterations} iterations, timefactor: #{speed}"
      $log.info "\n    max duration #{duration.to_s}s until #{tmax}" if duration
      @rcps_ok = @rcps_error = @rcps_timeout = 0
      tstart = Time.now
      i = 0
      while i < iterations
        i += 1
        # start scheduler
        @clientthreads = {}
        $log.info "------------------------------------------------------------------------------------"
        $log.info "  starting run ##{i} of #{iterations}"
        $log.info "------------------------------------------------------------------------------------"
        @payload_entries.each_with_index {|entry, rq_number|
          break if tmax && (Time.now > tmax)
          if rq_number > 0 && speed > 0
            # wait for next time slot
            tsleep = (@payload_entries[rq_number].timestamp - @payload_entries[rq_number-1].timestamp) / speed
            $log.info "sleeping #{tsleep}s"
            sleep tsleep if tsleep > 0
          end
          # get available client and submit the request
          client = @clients.pop
          @clientthreads[client] = Thread.new(client, entry, rq_number) {|client, entry, rq_number|
            if params[:rmsarmor]
              begin
                ##res = client.get_recipe_payload_request('RecipeName'=>entry.rilcontext['RecipeName'], 'RecipeNameSpaceID'=>entry.rilcontext['RecipeNameSpaceID'])
                rqstart = Time.now
                resp = client.get_recipe_payload_request(entry.rilcontext)
              rescue
                resp = nil
              end
              if resp
                armorei = @armorclients.pop
                startpaths = resp.dcppaths
                startpaths = [startpaths] unless startpaths.instance_of?(Array)
                startpaths += resp.linkedrecipes
                resp = armorei.dcps_ecsv(startpaths, eqp: entry.rilcontext['Equipment'], check: true)
                @armorclients.push(armorei)
              end
              responsetime = Time.now - rqstart
              if resp.nil?
                $log.warn "  timeout: #{rq_number.inspect}"
                @rcps_timeout += 1
                responsetime = -1
              elsif resp == false
                $log.info "  accepted but wrong context #{rq_number}"
                @rcps_error += 1
                responsetime = -2
              else
                $log.info "  accepted: #{rq_number.inspect}"
                @rcps_ok += 1
              end
            else
              ##resp = client.rendered_payload_context(nil, nil, context: entry.context, unpack: false)
              resp = client.get_payload(:bycontext, context: entry.context)
              responsetime = client.responsetime
              if resp.nil?
                $log.warn "  timeout: #{rq_number.inspect}"
                @rcps_timeout += 1
                responsetime = -1
              elsif resp.has_key?(:Fault)
                $log.info "  accepted but wrong context #{rq_number}"
                @rcps_error += 1
                responsetime = -2
              else #resp.instance_of?(String)
                $log.info "  accepted: #{rq_number.inspect}"
                @rcps_ok += 1
              end
            end
            if @perflog
              Thread.exclusive {
                open(File.expand_path(@perflog), 'a') {|fh|
                  fh.write("#{client.txtime.utc.iso8601(3)};#{rq_number.inspect};#{responsetime}\n")
                }
              }
            end
            @clients.push(client)
          }
        }
        @clientthreads.values.each {|t| t.join if t}
      end
      $log.info "-" * 65
      $log.info "Test completed after #{Time.now - tstart} seconds, good messages: #{@rcps_ok}, wrong contexts: #{@rcps_error}, timeouts: #{@rcps_timeout}"
      $log.info "-" * 65
      performance_charts(@perflog) unless params[:nolog]
      return @rcps_timeout.zero?
    end

    # create performance charts from a performance log file
    def performance_charts(fname)
      $log.info "performance_charts #{fname.inspect}"
      bname = fname[0...-File.extname(fname).size]
      lines = File.readlines(fname) || return
      # first column: time, 3rd column: duration
      data = lines[1..-1].collect {|l|
        ff = l.split(';')
        [Time.parse(ff[0]), ff[2].to_f]
      }
      # DCR response time charts
      ch = Chart::Time.new(data)
      ch.create_chart(y: 'duration (s)', dots: true, export: "#{bname}_chart1.png")
      ch = Chart::Histogram.new(data.collect {|e| e.last})
      ch.create_chart(x: 'duration (s)', title: File.basename(bname), export: "#{bname}_hist.png")
      true
    end
  end

end


def main(argv)
  # set defaults
  env = 'let'
  rendererpath = '/system/plugins/renderers/idxmlrenderer-DCP_2_2_9-TDM_2_2_9-DCC_2_2_9'
  logfile = 'log/armor_exerciser.log'
  perflog = "log/armor_perf_#{Time.now.utc.iso8601.gsub(':', '-')}.csv"
  nthreads = 10
  speed = 1
  iterations = 1
  duration = nil
  timeout = 1000
  verbose = false
  noconsole = true
  getpayload = false
  rmsarmor = false
  # parse cmdline options
  optparser = OptionParser.new {|o|
    o.banner = "Usage: #{File.basename(__FILE__)} [options] payloadfile"
    o.on('-e', '--env [ENV]', "environment, default #{env.inspect}") {|s| env = s}
    o.on('--rendererpath [RENDERERPATH]', 'renderer path') {|s| rendererpath = s}
    o.on('-d', '--duration [DURATION]', Integer, 'max test duration in seconds, default is as long as data exist') {|n| duration = n}
    o.on('-i', '--iterations [ITERATION]', Integer, "number of iterations, default is #{iterations}") {|n| iterations = n}
    o.on('-n', '--nthreads [THREADS]', Integer, "number of threads, default is #{nthreads}") {|n| nthreads = n}
    o.on('-p', '--perflog [FILE]', 'performance log file') {|f| perflog = f}
    o.on('-r', '--rmsarmor', 'test with RMS/Armor calls') {rmsarmor = true}
    o.on('-s', '--speed [SPEED]', Float, '0: as fast as possible, 0.5: half speed, 1: as in payloadfile, 2: double speed, ..') {|n| speed = n}
    o.on('-t', '--timeout [TIMEOUT]', Integer, "request timeout, default #{timeout}s") {|n| timeout = n}
    o.on('-g', '--getpayload', 'download payloadfile from hosts - no test') {getpayload = true; noconsole = false}
    o.on('-v', '--verbose', 'verbose logging') {verbose = true}
    o.on('--console', 'log to console and file') {noconsole = false}
    o.on_tail('-h', '--help', 'diplay this screen') {puts o; exit}
  }
  optparser.parse(*argv)
  # create logger
  create_logger(file: logfile, file_verbose: verbose, verbose: verbose, noconsole: noconsole)
  STDOUT.puts "logging to #{logfile}, console log: #{!noconsole}"
  # get parameters from options
  params = {rmsarmor: rmsarmor, rendererpath: rendererpath, perflog: perflog, nthreads: nthreads,
    timeout: timeout, duration: duration, speed: speed, iterations: iterations}
  if getpayload
    # get payload files from Prod servers only
    Armor::Hosts.new('armor.prod').get_payloadfiles(payloadfilepattern: 'payload-treestats.csv.*', save_at: 'log/armor/data')
  else
    # run the test
    payloadfile = argv[-1]
    res = Armor::Exerciser.new(env, params).load_test(payloadfile, params)
    $log.info "\ncompleted: #{res.inspect}"
  end
end

main(ARGV) if __FILE__ == $0
