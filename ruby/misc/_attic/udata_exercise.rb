=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: Daniel Steger, 2013-12-23

History:
  2016-02-18 sfrieske, use user_data instead of AMDGetUDATA
  2017-11-20 sfrieske, moved LoopThread from misc.rb here
=end

require 'siview'
require 'misc'
require 'svlog'


class UDataExercise
  @@Products = (1..1000).collect {|i| "LET-PROD%04d.01" % i }
  @@PDs = (1..5000).collect {|i| "LET-PD%04d.01" % i }
  @@LRs = (1..5000).collect {|i| "LET-LR%04d.01" % i }
  @@nthreads = 20
  
  def initialize(env='let', params={})
    $sv = SiView::MM.new(env.to_s, params) unless $sv
    $sm = SiView::SM.new(env.to_s, params) unless $sm
    $svstats = SvLog.new(env.to_s) unless $svstats
  end
  
  def update_udatas()
    udatas = Hash[*[(1..100).collect {|i| ["UDATA_TEST_%03d" % i, "VALUE-%03d" % i]}]]
    batch = (@@PDs.count / 100).ceil
    batch.times do |i|
      $sm.update_udata(:pd, @@PDs.slice(i*100, 100), udatas, release: false)
      $log.info "Batch #{i}"
    end
    batch = (@@LRs.count / 100).ceil
    batch.times do |i|
      $sm.update_udata(:lrcp, @@LRs.slice(i*100, 100), udatas, release: false)
      $log.info "LR Batch #{i}"
    end
    batch = (@@Products.count / 100).ceil
    batch.times do |i|
      $sm.update_udata(:product, @@Products.slice(i*100, 100), udatas, release: false)
      $log.info "Prod Batch #{i}"
    end
  end
  
  def start()
    pdslice = (1.0 * @@PDs.count / @@nthreads).ceil
    lrslice = (1.0 * @@LRs.count / @@nthreads).ceil
    pslice = (1.0 * @@Products.count / @@nthreads).ceil

    threads = []
    @@nthreads.times do |t|
      #threads << LocateThread.new(t, lots.slice(t*lslice, lslice), @@eqp)
      #threads << RunBRScriptThread.new(t, lots.slice(t*lslice, lslice), @@eqp)
      threads << UDataThread.new(t, :pd, @@PDs.slice(t*pdslice, pdslice))
      threads << UDataThread.new(t, :lrecipe, @@LRs.slice(t*lrslice, lrslice))
      threads << UDataThread.new(t, :product, @@Products.slice(t*pslice, pslice))
      threads << UserDataThread.new(t, @@PDs.slice(t*pdslice, pdslice))
    end
    threads.each {|t| t.run}
    time = 30*60
    $log.warn "Waiting #{time/60} min to complete"
    sleep time
    threads.each {|t| t.stop}
    threads.each {|t| t.join}
  end

  
  def XXstats    
    #start = "2013-12-23 09:22:00"
    #dend = "2013-12-23 09:53:00"
    #start = "2013-12-23 14:11:00"
    #dend = "2013-12-23 14:42:00"
    start = "2013-12-23 18:13:00"
    dend = "2013-12-23 18:44:00"
    i = 3
    $svstats.export_tx_details "stats_udata_pd_userdata_#{i}.csv", start, :dmax=>dend, :txids=>"TXOTQ002", :details=>"%PosProcessDefinition%"
    $svstats.export_tx_details "stats_udata_pd_getudata_#{i}.csv", start, :dmax=>dend, :txids=>"TXPLQ502", :details=>"%CLASS_NAME=PD%"
    $svstats.export_tx_details "stats_udata_pr_getudata_#{i}.csv", start, :dmax=>dend, :txids=>"TXPLQ502", :details=>"%CLASS_NAME=PRODSPEC%"
    $svstats.export_tx_details "stats_udata_lr_getudata_#{i}.csv", start, :dmax=>dend, :txids=>"TXPLQ502", :details=>"%CLASS_NAME=LRCP%"
  end
  

  # Looping thread with the ability to be stopped
  class LoopThread < Thread
    attr_accessor :stopped, :paused
    def initialize
      @stopped = false
      @paused = false
      super do
        before_loop
        until @stopped
          yield
          Thread.stop if @paused
        end
        after_loop
      end
    end

    def before_loop; end

    def after_loop; end

    def stop
      @stopped = true
    end

    def paused=(paused)
      @paused = paused
      run if !paused
    end
  end


  class LocateThread < LoopThread
    def initialize(t, lots, eqp)
      $log.info "#{t}"
      @lots = (lots or [])
      super() {     
        @lots.each {|lot|
          $sv.lot_opelocate(lot, +1)
          sleep 1
          $sv.user_parameter("Lot", lot)
          sleep 1
        }
      }
    end

    def before_loop()
      $log.warn "Start locate for #{@lots.inspect}"
    end

    def after_loop()
      $log.warn "Stopping thread"
    end
  end

  class RunBRScriptThread < LoopThread
    def initialize(t, lots, eqp)
      $log.info "#{t}"
      @lots = (lots or [])
      super() {
        @lots.each {|lot|
          #$sv.lot_opelocate(lot, +1)
          $sv.run_script(lot, eqp)
          sleep 1
        }
      }
    end

    def before_loop()
      $log.warn "Start script runs for #{@lots.inspect}"
    end

    def after_loop()
      $log.warn "Stopping thread"
    end
  end


  class UDataThread < LoopThread
    def initialize(t, classid, pids)
      $log.info "#{t}"
      @pids = pids
      super() {
        ##@pids.each {|pid|
        pid = @pids.sample
        $sv.user_data(classid, pid)
        sleep 1
        ##}
      }
    end

    def before_loop()
      $log.warn "Start user_data queries for #{@pids.inspect}"
    end

    def after_loop()
      $log.warn "Stopping thread"
    end
  end
  
  class UserDataThread < LoopThread
    def initialize(t, pids)
      $log.info "#{t}"
      @pids = pids
      super() {
        ##@pids.each {|pid|
        pid = @pids.sample
        $sv.user_data_pd(pid)
        sleep 1
        ##}
      }
    end

    def before_loop()
      $log.warn "Start User_data queries for #{@pids.inspect}"
    end

    def after_loop()
      $log.warn "Stopping thread"
    end
  end
end  
  
#$test = UDataExercise.new()
#.start()
