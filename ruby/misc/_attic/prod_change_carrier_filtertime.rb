=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: sfrieske, 2016-08-18

Version: 1.0.0

History:
  2016-08-18 sfrieske, created and verified in ITDC


Usage:
preparation:
1) Create a file containing the carriers, one per line, no headers
2) Edit $env, $fname, $username and $ntauth (must be true for real persons and false for X-UNITTEST)
3) start a test console, e.g. bin\console_R15_custom.bat

on the console:
4) load this file:
    load 'ruby/misc/prod_change_carrier_filtertime.rb'
5) read carriers:
    get_carriers
6) verify the carriers have been read correctly:
    $_carriers.size (compare with the number of carriers in the file ($fname)
7) set the filter time: set_filtertime($_carriers)
8) verify result, e.g.: pp $failed
    the carriers not changed are shown
9) optionally re-run with error carriers: set_filtertime($failed)
    pp $error

=end

require 'siview'
require 'misc'

$env = 'f1prod'
$fname = 'log/carriers1.txt'
$username = 'tgenennc'
$ntauth = true

#
# ------------- no changes below this line required ------------------------
#

$sv = SiView::MM.new($env, ntauth: $ntauth, user: $username)
$carriers = []

# return array of carriers names
def get_carriers
  ret = []
  open($fname) {|fh|
    fh.readlines.each {|line|
      #next if ret.empty?  # skip header
      ret << line.strip
    }
  }
  $carriers = ret
end

# set FILTER_START_TIME Udata from PM reset time
def set_filtertime(carriers, params={})
  res = $sv.logon_check
  ($log.warn "invalid username/password"; return) if res != 0
  #
  tstart = Time.now
  $_carriers = carriers.clone
  $passed = []
  $failed = []
  $_carriers.each_with_index {|carrier, i|
    $log.info "#{i+1}/#{carriers.size}" if i % 100 == 0
    #
    cs = $sv.carrier_status(carrier, raw: true)
    if $sv.rc != 0
      $log.warn "SiView error"
      $failed << carrier
      next
    end
    ts = cs.cassettePMInfo.lastMaintenanceTimeStamp
    res = $sv.user_data_update(:carrier, carrier, 'FILTER_START_TIME'=>ts)
    if $sv.rc != 0
      $log.warn "SiView error"
      $failed << carrier
      next
    end
    $passed << carrier
    #break if i >= 3
  }
  $log.info "duration: #{duration_string(Time.now - tstart)}, passed: #{$passed.size}, failed: #{$failed.size}"
  return $failed.empty?
end
