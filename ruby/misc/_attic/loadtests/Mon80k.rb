# ------------------------------------------------------------------------------
# monitoring for 80 k test
# author Beate Kochinka
# 2011-01-20
# ------------------------------------------------------------------------------
# $Rev: 14722 $
# $LastChangedDate: 2012-08-30 14:09:31 +0200 (Do, 30 Aug 2012) $
# $LastChangedBy: ppeschel $

$:.unshift File.dirname(__FILE__) unless $:[0] == File.dirname(__FILE__)
$:.unshift("ruby")

require 'Common80k'
require 'svlog'

# monitoring for 80 k test
class Mon80k < Common80k

  # ------------------------------------------------------------------------------
  # check consistency between lots waiting at operation and tool
  # (may be indicator for SiView or test script setup error)
  # ------------------------------------------------------------------------------
  def mon_check_waiting
      errors = 0
      @ops2tools.each{|op2tools|
        op = op2tools[0]
        waiting = (@sv.lot_list :route=>@route, :opNo=>op, :status=>"Waiting").length
        tool_arr = op2tools[1]
        tool_arr.each{|tool|
          t_waiting = 0
          (@sv.what_next tool).each{|el|
            if el.route == @route
              t_waiting += 1
            end
          }
          if t_waiting == waiting
            $log.info "#lots ok: #{waiting} at #{op} - #{t_waiting} at #{tool}"
          else
            $log.error "#lots ERROR: #{waiting} at #{op} - #{t_waiting} at #{tool}"
            errors += 1
          end
        }
      }
      $log.info "found #{errors} inconsistencies"
  end
  
  # ------------------------------------------------------------------------------
  # monitor processing tools
  # ------------------------------------------------------------------------------
  def mon_tools_processing (loops=20, sleeptime=20)
    numTools = tools.length
    $log.info "... checking tool states (max. #{loops} loops)"
    nonProcessing = []
    @tools.each{|tool|
      if (@sv.eqp_info tool).status != "PRD.1PRD"
        nonProcessing.push tool
      end
    }
    processing = numTools - nonProcessing.length
    processingOld = nil
    noChange = 0
    i = 0
    loops.times{
      i += 1
      processing = numTools - nonProcessing.length
      info = "##{"%03d" % i}: processing #{processing}/#{numTools}"
      info = info + " not: #{nonProcessing.sort.join(" ")}" if nonProcessing.length > 0
      $log.info info
      # note if number of processing tools changed
      noChange = processingOld == processing ? noChange + 1 : 0
      processingOld = processing
      # break if no changes after n checks
      break if nonProcessing.length == 0
      if noChange == 20
        $log.info "no change within #{noChange} checks"
        break
      end
      # check again after some time
      sleep sleeptime
      nonProcessing.each{|tool|
        if (@sv.eqp_info tool).status == "PRD.1PRD"
          nonProcessing.delete tool
        end
      }
      if nonProcessing.length == 0
        $log.info "all #{numTools} tools observed as processing"
        break
      end
    }
    $log.info "not processing: #{nonProcessing.sort.join(",")}" if nonProcessing.length > 0
  end
  
  # ------------------------------------------------------------------------------
  # monitor processing lots per operation
  # ------------------------------------------------------------------------------
  def mon_tools_detail
    puts "---------------------------------------------------------------------------------------------------------------------------------------"
    puts "tool (proc.)| ports - state        | loaded         | disp_load      | disp_unload    | control jobs"
    puts "---------------------------------------------------------------------------------------------------------------------------------------"
    @tools.each{|tool|
      info = @sv.eqp_info tool
      t_state = info.status == "PRD.1PRD" ? "*" : " "
      cj = []
      state = []
      loaded = []
      disp_load = []
      disp_unload = []
      info.ports.each{|p|
        cj.push "#{"%-24s" % p.cj}" # "LETCLxxxx-20110102-001"
        state.push "#{"%-10s" % p.state}" # "UnloadReq"
        loaded.push "#{"%-7s" % p.loaded_carrier}" # "LSxxxx"
        disp_load.push "#{"%-7s" % p.disp_load_carrier}"
        disp_unload.push "#{"%-7s" % p.disp_unload_carrier}"
      }
      puts "#{tool} #{t_state} | #{state.join ""} | #{loaded.join ""} | #{disp_load.join ""} | #{disp_unload.join ""} | #{cj.join ""}"
    }
    nil
  end

  # ------------------------------------------------------------------------------
  # monitor tools (output) - one line for both tools for operation
  # output of (per operation) tools, their state and port state,
  # number of processing, waiting lots and lots in RTD dispatch list
  # :loops - number of loops for output (default: 1)
  # :last - output for last n operations only (default: all)
  # ------------------------------------------------------------------------------
  def mon_tools2(params={})
    oldLogLevel = $log.level
    # change log level to avoid disturbing output
    $log.level = Logger::ERROR
    loops = (params[:loops] or 1)
    last = (params[:last])
    ops2tools = (last ? @ops2tools[@ops2tools.length-last..-1] : @ops2tools)
    if last and last < @ops2tools.length
      ops2tools ={}
      @ops2tools.to_a[@ops2tools.length-last..-1].each{|e|
        ops2tools.store e[0], e[1]
      }
    else
      ops2tools = @ops2tools
    end
    #pp ops2tools
    #return
    until loops == 0
      # header
      puts ""
      puts "         tools.............. state lots................... ports......................................"
      puts "op.no.   t1        t2        s1 s2 proc RTDdis wait ONHOLD t1|P1      t1|P2      t2|P1      t2|P2"
      ops2tools.each{|op2tools|
        op = op2tools[0]
        waiting = (@sv.lot_list :route=>@route, :opNo=>op, :status=>"Waiting").length
        processing = (@sv.lot_list :route=>@route, :opNo=>op, :status=>"Processing").length
        onhold = (@sv.lot_list :route=>@route, :opNo=>op, :status=>"ONHOLD").length
        tool1 = op2tools[1][0]
        tool2 = op2tools[1][1]
        toolInfo1 = eqp_mon_info tool1
        toolInfo2 = eqp_mon_info tool2
        rtdDispatch = @sv.rtd_dispatch_list(tool1)[1].length
        puts "#{op} #{tool1} #{tool2} #{toolInfo1[0]}  #{toolInfo2[0]}  #{"%4s"%processing}   #{"%4s"%rtdDispatch} #{"%4s"%waiting}  #{"%5s"%onhold} #{"%-10s"%toolInfo1[2]} #{"%-10s"%toolInfo1[3]} #{"%-10s"%toolInfo2[2]} #{"%-10s"%toolInfo2[3]}"
      }
    loops -= 1
    end
    # reset log level
    $log.level = oldLogLevel
    nil
  end
  
  # ------------------------------------------------------------------------------
  # check if tools are alive
  # ------------------------------------------------------------------------------
  def mon_tools_alive(tools = @tools)
    succeeded = 0
    failed = []
    @tools.each{|tool|
      if @sv.eqp_signaltower_off(tool) == 0
        succeeded += 1
      else
        failed << tool
      end
    }
    if failed.length == 0
      $log.info "#{succeeded} tools alive"
    else
      $log.error "#{succeeded} tools alive, dead: #{failed.join(', ')}"
    end
  end
  
  # ------------------------------------------------------------------------------
  # show tools' port modes
  # ------------------------------------------------------------------------------
  def mon_tools_portmodes(tools = @tools)
    tools.each{|tool|
      portInfo = @sv.eqp_info(tool).ports
      puts("#{tool} #{"%-11s"%portInfo[0].mode} #{"%-11s"%portInfo[1].mode}")
    }
    nil
  end
  
  # ------------------------------------------------------------------------------
  # monitor tools (output) - one line per tool and operation
  # ------------------------------------------------------------------------------
  def mon_tools1(params={})
    loops = (params[:loops] or 1)
    until loops == 0
      header_mon1
      @ops2tools.each{|op2tools|
        op = op2tools[0]
        waiting = (@sv.lot_list :route=>@route, :opNo=>op, :status=>"Waiting").length
        op2tools[1].each{|tool|
        toolInfo1 = eqp_mon_info tool
        #     op.no. tool   p1               r1                         wait             t1|P1                   t1|P2
        puts "#{op} #{tool} #{toolInfo1[0]}  #{"%-11s"%toolInfo1[1]}    #{"%4s"%waiting} #{"%-10s"%toolInfo1[2]} #{"%-10s"%toolInfo1[3]}"
        }
    }
      loops -= 1
    end
    nil
  end
  
  # ------------------------------------------------------------------------------
  # monitor tools' what's next (output) periodically with warnings only
  # ------------------------------------------------------------------------------
  def mon_whats_next_min
    mon_whats_next -1, num_lots_per_op_min, true
  end
    
  # ------------------------------------------------------------------------------
  # monitor tools' what's next (output)
  # ------------------------------------------------------------------------------
  def mon_whats_next(loops=1, numMinLots=6, warnOnly=false)
    until loops == 0
      puts "tool        # what's next"
      @tools.each{|tool|
        i = 0
        li = @sv.what_next tool
        li.each{|el|
          if el.route == @route
            i += 1
          end
        }
        if numMinLots != nil and i < numMinLots
          # below minimum number of lots - warn
          puts "! #{tool} #{i}"
        else
          if not warnOnly
            puts "  #{tool} #{i}"
          end
        end
      }
      loops -= 1
    end
    nil
  end
  
  # ------------------------------------------------------------------------------
  # monitor selected tools (output)
  # tools - comma separated string for tools' selection
  # based on tool's What's next, filtered for test route
  # ------------------------------------------------------------------------------
  def mon_tool(tools, params={})
    tool_arr = tools.split(",")
    loops = (params[:loops] or 1)
    delay = (params[:delay] or 0)
    until loops == 0
      header_mon_single_tool
      tool_arr.each{|tool|
        waiting = 0
        (@sv.what_next tool).each{|el|
          if el.route == @route
            waiting += 1
          end
        }
        toolInfo1 = eqp_mon_info tool
        #     tool/state               reserved                  wait             P1                      P2
        puts "#{tool} #{toolInfo1[0]}  #{"%-11s"%toolInfo1[1]}   #{"%4s"%waiting} #{"%-10s"%toolInfo1[2]} #{"%-10s"%toolInfo1[3]}"
      }
      sleep delay
      loops -= 1
    end
    nil
  end
  
  # ------------------------------------------------------------------------------
  # show non-processing tools
  # ------------------------------------------------------------------------------
  def show_non_processing_tools(params={})
    non_processing = []
    delay = (params[:delay] or 1)
    tools = (params[:tools] or @tools)
    puts "... checking #{tools.length} tools"
    tools.each{|tool|
      if (@sv.eqp_info tool).status != "PRD.1PRD"
        non_processing.push tool
      end
    }
    if non_processing.length > 0
      sleep delay
    end
    non_processing.each{|tool|
      if (@sv.eqp_info tool).status = "PRD.1PRD"
        non_processing.delete tool
      end
    }
    if non_processing.length > 0
      puts "#{non_processing.length} non-processing tools: #{non_processing.join(",")}"
    else
      puts "all tools processing"
    end
  end
  
  # ------------------------------------------------------------------------------
  # show reasons for ONHOLD lots
  # ------------------------------------------------------------------------------
  def show_hold_reasons(memo_length_max=80)
    reasons = []
    summary = {}
    i = 0
    @ops.each{|op|
      @sv.lot_list(:opNo=>op, :route=>@route, :status=>"ONHOLD").each{|lot|
        firstHold = @sv.lot_hold_list(lot)[0]
        reason = firstHold.reason
        if not summary[reason]
          summary[reason] = 1
        else
          summary[reason] += 1
        end
        reasons << [reason, firstHold.memo[0..memo_length_max-1]]
      }
      i += 1
    }
    pp reasons.uniq
    pp summary
  end
  
  # ------------------------------------------------------------------------------
  # show carriers in 'EO'
  # ------------------------------------------------------------------------------
  def show_carriers_EO(carriers=@carriers)
    pp(info_carriers_EO(carriers))
  end
  
  def info_carriers_EO(carriers=@carriers)
    carrInfoList = []
    i = 0
    carriers.each{|carrier|
      stat = @sv.carrier_status(carrier)
      if stat.xfer_status == 'EO'
        lot = stat.lots[0]
        if lot != nil
        end
        carrInfoList << [carrier, stat.user, stat.claim_time, lot]
      end
      i += 1
      if i%500 == 0
        $log.info "... #{i}/#{@carriers.length} carriers checked"
      end
    }
    $log.info "... #{i}/#{@carriers.length} carriers checked"
    if carrInfoList.length > 0
      puts "#{carrInfoList.length} of #{@carriers.length} carriers in 'EO':"
#      carrInfoList.each{|c|
#        puts "#{c.inspect}"
#      }
    else
      puts "none of #{i} carriers in 'EO'"
    end
    return carrInfoList
  end
  
  def mon_carrier_states(checked_states)
    checkedStates = checked_states.split(',')
    info = {}
    i = 0
    checkedStates.each{|state|
      info.store(state, 0)
    }
    @carriers.each{|carrier|
      state = @sv.carrier_status(carrier).xfer_status
      if checkedStates.include?(state)
        info.store(state, info[state]+1)
      end
      i += 1
      if i%500 == 0
        $log.info "... #{i}/#{@carriers.length} carriers checked"
      end
    }
    puts "#{info.inspect}"
  end

  # ------------------------------------------------------------------------------
  # monitor tools ports (output)
  # precondition: tools with 2 ports
  # ------------------------------------------------------------------------------
  def mon_tool_ports(params={})
    loops = (params[:loops] or 1)
    until loops == 0
      header_tools_port_mode
      @tools.each{|tool|
        portInfo = port_info tool
        puts "#{"%-10s"%portInfo[0]} #{"%-10s"%portInfo[1][0]} #{"%-10s"%portInfo[1][1]} #{"%-10s"%portInfo[2][0]} #{"%-10s"%portInfo[2][1]} #{"%-10s"%portInfo[3][0]} #{"%-10s"%portInfo[3][1]}"
      }
      loops -= 1
    end
    nil
  end

  # ------------------------------------------------------------------------------
  # check tool EI availibility (signal tower off)
  # ------------------------------------------------------------------------------
  def mon_ei(params={})
    loops = (params[:loops] or 1)
    until loops == 0
      failed = []
      @tools.each{|tool|
        $log.info "#{tool}..."
        if (@sv.eqp_signaltower_off tool) != 0
          failed.push tool
        end
      }
      loops -= 1
      if failed.length > 0
        $log.error "no response from: #{failed.join(" ")}"
      end
    end
    nil
  end
  
  # ------------------------------------------------------------------------------
  # monitor waiting lots (output)
  # precondition: tools with 2 ports
  # ------------------------------------------------------------------------------
  def mon_lots(params={})
    loops = (params[:loops] or -1)
    sleepTime = (params[:sleep] or 10)
    info = (params[:info] or false)
    lwm = (params[:lwm] or 4)
    status = (params[:status])# or "Waiting")
    if not info
      puts "... monitoring waiting lots < #{lwm} every #{sleepTime} seconds"
    end
    _ops = []
    @ops.each{|op| _ops << op}
    if params[:all]
      # include non-processing operations
      _ops << @exit_op
      _ops << @bankin_op
      _ops << @lotstart_op
    end
    until loops == 0
      header_waiting_lots
      _ops.each{|op|
        if status == nil
          waiting = (@sv.lot_list :route=>@route, :opNo=>op).length
        else
          waiting = (@sv.lot_list :route=>@route, :opNo=>op, :status=>status).length
        end
        if info
          puts "#{"%-10s"%op} #{"%-10s"%waiting}"
        else
          puts "#{"%-10s"%op} #{"%-10s"%waiting}" if waiting < lwm
        end
      }
      loops -= 1
      sleep sleepTime
    end
  end
  
  # ------------------------------------------------------------------------------
  # get monitoring data for a tool:
  # [state, reserved_carriers, port1_state, port2_state, mode]
  # state as P(rocessing) or W(aiting)
  # ------------------------------------------------------------------------------
  def eqp_mon_info (tool)
    eqpInfo = @sv.eqp_info tool
    state = eqpInfo.status == "PRD.1PRD" ? "P" : "W" # (not necessarily SBY.2WPR)
    reserved = eqpInfo.rsv_carriers
    return [state, reserved.join("|"), eqpInfo.ports[0].state, eqpInfo.ports[1].state]
  end
  
  # ------------------------------------------------------------------------------
  # get port mode info for a tool:
  # [tool, [port1_mode, port2_mode], [port1_state, port2_state], [port1_cass_loaded, port2_cass_loaded]
  # ------------------------------------------------------------------------------
  def port_info (tool)
    portModes = []
    portStats = []
    carriersLoaded = []
    (@sv.eqp_info tool).ports.each{|port|
      portModes.push port.mode
      carriersLoaded.push port.loaded_carrier
      portStats.push port.state
    }
    return [tool, portModes, portStats, carriersLoaded]
  end
  
  # ------------------------------------------------------------------------------
  # monitoring headers (output)
  # ------------------------------------------------------------------------------
  def header_mon_single_tool
    puts ""
    puts "tool/state   reserved      wait P1         P2"
  end
  
  def header_mon1
    puts ""
    puts "op.no.   tool/state   reserved       wait P1         P2"
  end
  
  def header_tools_port_mode
    puts ""
    puts "#{"%-10s"%"tool"} #{"%-10s"%"P1 mode"} #{"%-10s"%"P2 mode"} #{"%-10s"%"P1 state"} #{"%-10s"%"P2 state"} #{"%-10s"%"P1 cass"} #{"%-10s"%"P2 cass"}"
  end
  
  def header_waiting_lots
    puts ""
    puts "#{"%-10s"%"op."} #{"%-10s"%"waiting"}"
  end

  def get_lot_loops (status="SHIPPED")
    return status == nil ? \
    @sv.lot_list(:lot=>"LET2%").collect{|l|@sv.user_parameter("Lot",l,:name=>"TestRouteLoops").value.to_i} \
    : @sv.lot_list(:lot=>"LET2%", :status=>status).collect{|l|@sv.user_parameter("Lot",l,:name=>"TestRouteLoops").value.to_i}
  end
  
  # get avg. number of loops that shipped lots did
  def get_avg_lot_loops
    ops = get_lot_loops
    retung ops.sum / ops.length.to_f
  end
  
  # ------------------------------------------------------------------------------
  # count successful operation starts using direct database access
  # (includes internal buffer tools)
  # ------------------------------------------------------------------------------
  def count_opstarts(min=nil, max=nil, interval=60, out_file=nil, svlog=SvLog.new(@env))
    if interval <= 0
      $log.error "invalid interval: #{interval}"
      return
    end
    out = open_out_file(out_file)
    maxTime = max == nil ? Time.now : Time.parse(max)
    minTime = min == nil ? maxTime - (60*60) : Time.parse(min)
    fromTime = minTime
    toTime = fromTime + interval*60
    $log.debug "from|to|opStarts|WOPM"
    out.puts "from\tto\topStarts\tWOPM"
    until toTime > maxTime
      res = svlog.get_opestarts(:tmin=>fromTime,:tmax=>toTime,:txresult=>'0')
      # wafer out per month: 1000 op. starts per hour = 30 k
      # (normalize for used interval
      wopm = (60/interval.to_f * res/100*3).to_i
      $log.debug "#{fromTime.strftime('%Y-%m-%d %H:%M:%S')}|#{toTime.strftime('%Y-%m-%d %H:%M:%S')}|#{res}\t#{wopm}"
      out.puts "#{fromTime.strftime('%Y-%m-%d %H:%M:%S')}\t#{toTime.strftime('%Y-%m-%d %H:%M:%S')}\t#{res}\t#{wopm}"
      fromTime += interval*60
      toTime = fromTime + interval*60
      out.flush
    end
    close_out_file(out)
  end
  def open_out_file(out_file)
    if out_file != nil
      out = open(out_file, "w")
      $log.info "...writing output to #{out_file}"
    else
      out = $stdout
    end
    return out
  end
  
  def close_out_file(out)
    if out != $stdout
      out.close
    end
  end
  
  # ------------------------------------------------------------------------------
  # generate shell script for output of operation start counts to file
  # parameters:
  # min and max - timespan; default: actual time (- 1 hour)
  # sh_file - output file for shell script
  # res_file - output of shell script execution
  # per hour counts for TXTRC002 only (no internal buffer tool!) 
  # for all tools and filtered for LETCL tools - output like
  # all|2011-04-12.23.00|2011-04-13.00.00|3181
  # LETCL|2011-04-12.23.00|2011-04-13.00.00|3181
  # ------------------------------------------------------------------------------
  def gen_sh_opstarts(min=nil, max=nil, sh_file="getOpStarts.sh", res_file="opStart.txt")
    maxTime = max == nil ? Time.now : Time.parse(max)
    minTime = min == nil ? maxTime - (60*60) : Time.parse(min)
    fromTime = minTime
    toTime = fromTime + (60*60)
    cmd = "printf \"%s|%s|%s|$(svlog -env let -dmin %s -dmax %s -tx TXTRC002 -rc 0 | grep TXTRC002 %s | wc -l)\\r\\n\" >> %s"
    out = open(sh_file, "w")
    puts "writing output to #{sh_file}"
    out.puts "# successful op. starts #{minTime.strftime('%Y-%m-%d %H:%M')} - #{maxTime.strftime('%Y-%m-%d %H:%M')}"
    until toTime > maxTime
      from = fromTime.strftime('%Y-%m-%d.%H.%M')
      to = (fromTime + (60*60)).strftime('%Y-%m-%d.%H.%M')
      out.puts "#{cmd % ["all", from, to, from, to, "", res_file]}"
      out.puts "#{cmd % ["LETCL", from, to, from, to, "| grep LETCL", res_file]}"
      fromTime += 60*60
      toTime = fromTime + (60*60)
    end
    out.close
  end
  
end
  
#$log.level = Logger::INFO
