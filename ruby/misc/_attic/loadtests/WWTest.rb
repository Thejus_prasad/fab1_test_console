# ------------------------------------------------------------------------------
# common source for 80 k test
# author Beate Kochinka
# ------------------------------------------------------------------------------
# $Rev: 16400 $
# $LastChangedDate: 2013-12-18 17:21:57 +0100 (Mi, 18 Dez 2013) $
# $LastChangedBy: ssteidte $

# common source for 80 k test
class WWTest
  attr_accessor :env, :sv,
    :product, :route, 
    :carriers, :lots, 
    :tools, :stocker

  def initialize(params={})
    # use defaults for 80 k test
    @env = (params[:env] or "let")
    @sv = (params[:sv] or SiView::MM.new(env))
    @product = (params[:product] or "LET-PROD0047.01")
    @route = (params[:route] or "LET-MN0047.01")
    @tools = init_tools params[:tools]
    @carriers = (params[:carriers] or init_carriers)
    @lots = (params[:lots] or init_lots)
    @stocker = (params[:stocker] or "STO110")
    nil
  end
  
  # initialize carriers
  def init_carriers
    carriers = []
    200.times{|i| carriers.push("WW%03d" % i)}
    return carriers
  end
  
  # initialize lots
  def init_lots
    lots = []
    @carriers.each{|carrier| lots << (@sv.carrier_status carrier).lots.collect}
    return lots.flatten
  end
  
  # initialize tools
  def init_tools (tools)
    tools = []
    # LETCL3000 - LETCL3099
    100.times{|i| tools.push("LETCL3%03d" % i)}
    return tools
  end
  
  # get number of processing lots
  def num_processing_lots
    return (@sv.lot_list :product=>@product, :status=>"Processing").length
  end
  
  # carrier states
  def carrier_states
    @carriers.each{|carrier| puts "#{carrier} #{@sv.carrier_status(carrier).xfer_status}"}
  end
  
  # carriers with lots
  def show_lots
    @lots.each{|lot| puts "#{lot} #{@sv.lot_info(lot).carrier}"}
  end
  
  # release holds
  def release_holds
    @lots.each{|lot|
      @sv.lot_hold_release lot, nil
      @sv.lot_futurehold_cancel lot, nil
    }
  end
  
  # tools info
  def mon_tools
    @tools.each{|tool|
      rtdDisp = @sv.rtd_dispatch_list(tool).rows.length
      eqpInfo = @sv.eqp_info tool
      state = eqpInfo.status == "PRD.1PRD" ? "P" : "W" # (not necessarily SBY.2WPR)
      reserved = eqpInfo.rsv_carriers
      puts "#{tool} rtdDisp=#{rtdDisp} state=#{state} reserved=#{reserved}"
    }
    nil
  end
  
  # ------------------------------------------------------------------------------
  # move carriers to destination stocker
  # ------------------------------------------------------------------------------
  def stock_in_carriers
    $log.info "... stocking in carriers"
    carriersEI = []
    @carriers.each{|carrier|
      status = @sv.carrier_status(carrier).xfer_status
      if status != "MI" and status != "SI" and status != "EI"
        @sv.carrier_xfer_status_change carrier, "MI", @stocker, ""
      end
      if status == "EI"
        carriersEI.push carrier
      end
    }
    puts "carriers in state EI: #{carriersEI.join ' '}" if carriersEI.length > 0
    nil
  end
end
