# ------------------------------------------------------------------------------
# common source for 80 k test
# author Beate Kochinka
# ------------------------------------------------------------------------------
# $Rev: 16400 $
# $LastChangedDate: 2013-12-18 17:21:57 +0100 (Mi, 18 Dez 2013) $
# $LastChangedBy: ssteidte $

$:.unshift File.dirname(__FILE__) unless $:[0] == File.dirname(__FILE__)
require 'set_paths'
set_paths
create_logger unless $log

require 'siview'

ENV.delete 'http_proxy'   # always get fresh IOR files

# common source for 80 k test
class Common80k
  attr_accessor :env, :sv,
    :product, :route, :ops, :bankin_op, :carriers, :tools, 
    :interbays, :stocker, :sleep_time, 
    :start_bank, :ship_bank, :src_product,
    :num_lots_per_op,
    :customer, :sublottype,
    # renewing lots:
    :num_lots_min, :num_lots_max, # min/max number of lots for product
    :num_lots_per_op_min, :num_lots_per_op_max, # min/max number of lots per op.
    :renew_interval, # interval in seconds
    :lwm, # low water mark for waiting lots per operation
    :hwm, # high water mark for waiting lots per operation
    # ---------------
    :lotstart_op, # value of script parameter TestRouteStartOp
    :start_op, # value of script parameter TestRouteStartOp
    :exit_op, # value of script parameter TestRouteExitOp
    :bankin_op, # bank in operation
    :max_loops, # value of script parameter TestRouteStartOp
    :ops2tools, # operations to tools mapping
    :carrier_filters, # filter for empty carriers, comma separated
    :stress_user_par_threads, # number of threads for user parameter changes
    :stress_user_par_sleep, # sleep time for user parameter changes
    :stress_user_par_loops # loops per lot for user parameter changes

  def initialize(params={})
    # use defaults for 80 k test
    @env = (params[:env] or "let")
    @sv = (params[:sv] or SiView::MM.new(env))
    @tools = init_tools params[:tools]
    @customer = (params[:customer] or "LET2")
    @product = (params[:product] or "LET2-PRODUCT01.01")
    @sublottype = (params[:sublottype] or "PO")
    @route = (params[:route] or "LET2-ROUTE01.01")
    @max_loops = (params[:max_loops] or 10)
    @lotstart_op = (params[:lotstart_op] or "100.100")
    @ops = init_ops params[:ops]
    @exit_op = (params[:exit_op] or "8000.1000")
    init_route(@ops.length > 0 ? @ops[0] : nil, @max_loops)
    @carriers = (params[:carriers] or init_carriers)
    @carrier_filters = (params[:carrier_filters] or "8K%")
    @src_product = (params[:src_product] or "LET2-SRCPRODUCT.ALL")
    @start_bank = (params[:start_bank] or "UT-RAW")
    @ship_bank = (params[:ship_bank] or "UT-SHIP")
    @bankin_op = (params[:last_op] or "9000.100")
    @renew_interval = (params[:renew_interval] or 1*60)
    @lwm = (params[:lwm] or 8)
    @hwm = (params[:lwm] or 15)
    @num_lots_min = (params[:num_lots_min] or 3000)
    @num_lots_max = (params[:num_lots_max] or 3000)
    @num_lots_per_op_min = (params[:num_lots_per_op_min] or @num_lots_min/ops.length)
    @num_lots_per_op_max = (params[:num_lots_per_op_max] or @num_lots_max/ops.length)
    @stocker = (params[:stocker] or "STO110")
    @interbays = (params[:interbays] or "INTER1,INTER2,INTER3,INTER4,INTER5".split(","))
    @sleep_time = (params[:sleep_time] or 15*60) # sleep time for interbay cleanup
    @ii = 100 # log progress after each ... element
    @num_lots_per_op = nil # number of lots per operation to be located - will be computed
    init_ops2tools
    @stress_user_par_threads = 4
    @stress_user_par_sleep = 4 
    @stress_user_par_loops = 10
    nil
  end
  
  # initialize carriers
  # carriers set up but not available: LS.. DS W X Y Z BK AX AW AV AU AR AS AQ AP AO AN AM
  def init_carriers
    cl = []
    cl << @sv.carrier_list(:carrier=>"8K0%")
    cl << @sv.carrier_list(:carrier=>"8K1%")
    cl << @sv.carrier_list(:carrier=>"8K2%")
    cl << @sv.carrier_list(:carrier=>"8K3%")
    cl << @sv.carrier_list(:carrier=>"8K4%")
    cl << @sv.carrier_list(:carrier=>"8K5%")
    cl << @sv.carrier_list(:carrier=>"8K6%")
    cl << @sv.carrier_list(:carrier=>"8K7%")
    cl << @sv.carrier_list(:carrier=>"8K8%")
    cl << @sv.carrier_list(:carrier=>"8K9%")
    return cl.flatten.uniq
  end
  
  # initialize operations
  # 1000.000 .. 1000.900 1100.000 .. .. 2400.900
  def init_ops (ops)
    if ops == nil
      ops = []
      modI = 1000
      while modI <= 2400 do
        i = 0
        while i <= 900 do
          ops.push "#{modI}.#{"%03d" % i}"
          i += 100
        end
        modI += 100
      end
    end
    return ops
  end

  # initialize the route's script parameters
  def init_route(startOp, maxLoops)
    @max_loops = maxLoops
    @start_op = startOp
    @sv.user_parameter_change "Route", @route, "TestRouteStartOp", @start_op
    @sv.user_parameter_change "Route", @route, "MaxTestRouteLoops", @max_loops
    @sv.user_parameter_change "Route", @route, "TestRouteExitOp", @exit_op
  end
  
  # initialize tools
  def init_tools (tools)
    if tools == nil
      # default tool list for 80 k test
      tools = []
      # LETCL1000 - LETCL1099
      i = 0
      while i < 100 do
        tools.push("LETCL1%03d" % i)
        i +=1
      end
      # LETCL1100 - LETCL1149
      i = 0
      while i < 50 do
        tools.push("LETCL11%02d" % i)
        i +=1
      end
      # LETCL2000 - LETCL2099
      i = 0
      while i < 100 do
        tools.push("LETCL20%02d" % i)
        i +=1
      end
      # LETCL2100 - 2149
      i = 0
      while i < 50 do
        tools.push("LETCL21%02d" % i)
        i +=1
      end
    end
    return tools
  end
  
  # get number of processing lots
  def num_processing_lots
    return (@sv.lot_list :product=>@product, :status=>"Processing").length
  end

  # ------------------------------------------------------------------------------
  # initialize the operations to tool mapping
  # ------------------------------------------------------------------------------
  # Warning: keep in sync with SiView route setup!
  def init_ops2tools
    @ops2tools = {}
    i = 0
    @ops.each{|op|
      # 2 tools per operation
      @ops2tools.store op, [@tools[i], @tools[i+1]]
      i += 2
    }
  end
  
  def ping
    $log.info "ping to #{self.class} succeeded"
  end
  
end
