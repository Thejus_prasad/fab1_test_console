# ------------------------------------------------------------------------------
# main class for 80 k test
# author Beate Kochinka
# ------------------------------------------------------------------------------
# $Rev: 16952 $
# $LastChangedDate: 2014-08-21 12:59:31 +0200 (Do, 21 Aug 2014) $
# $LastChangedBy: ssteidte $

$:.unshift File.dirname(__FILE__) unless $:[0] == File.dirname(__FILE__)
$:.unshift("ruby")

require 'Common80k'

# main class for 80 k test
class Test80k < Common80k
  attr_accessor :opinfo, # maps operation numbers to number of lots at corresponding operations
    :opinfo_few, # ... with too few lots
    :opinfo_many # ... with too many lots

  def initialize(params={})
    super
  end
  # ------------------------------------------------------------------------------
  # main procedure for starting the route
  # ------------------------------------------------------------------------------
  def start(stock_in_pending_carriers=false)
    if stock_in_pending_carriers
      stock_in_pending_carriers 0
    end
    $log.info "... starting"
    inhibit_route_cancel
    # show processing lots for a while
    $log.info "... show processing lots - stop with CTRL-C"
    watch_num_processing_lots :loops=>-1
  end

  # ------------------------------------------------------------------------------
  # main procedure for stopping the route
  # num_ops number of route's operations (from beginning) for lot redistribution
  # ------------------------------------------------------------------------------
  def stop(sleep_time=@sleep_time, force_tool_status_adjust=false, num_ops=@ops.length)
    $log.info "***** stopping *****"
    $log.info "      sleep_time = #{sleep_time} (seconds)"
    $log.info "      force_tool_status_adjust = #{force_tool_status_adjust}"
    $log.info "      num_ops = #{num_ops} (for lot redistribution)"
    # inhibit route
    inhibit_route
    # wait until processing will have been finished
    wait_until_processing_finish
    # wait additional time to get transports finished
    $log.info "... waiting #{sleep_time} seconds to get transports finished"
    sleep sleep_time.to_i
    # cancel reservations
    cancel_reservations
    # cleanup interbays -> should be obsolete if SP_REROUTE_XFER_FLAG=1 and SiView bug fixed
    #cleanup_interbays 0 # already waited - no sleeping needed
    # cleanup tools
    cleanup_tools force_tool_status_adjust # mode change for tools only if port state != LoadReq ?
    # stock in carriers
    stock_in_pending_carriers
    # release holds
    release_holds
    # distribute lots over operations
    distribute_lots num_ops
    $log.info "***** stopping finished *****"
  end

  # ------------------------------------------------------------------------------
  # distribute lots over operations
  # numOps number of operations from beginning of route
  # if nil then lots will not be redistributed
  # ------------------------------------------------------------------------------
  def distribute_lots(numOps=@ops.length)
    if numOps != nil and numOps > 1
      $log.info "... distributing lots over #{numOps} operations"
      find_lots_at_ops
      store_diffs(numOps)
      redistribute
    else
      $log.info "-> no lots will be redistributed"
    end
  end

  # ------------------------------------------------------------------------------
  # distribute lots over operations:
  # surplus lots > hwm will be located to lot start operation
  # ------------------------------------------------------------------------------
  def distribute_lots1(hwm=10)
    surplusLots = 0
    @opinfo_many = {}
    @opinfo_few = {}
    @ops.each{|op|
      plusLots = @sv.lot_list(:route=>@route, :opNo=>op, :status=>"Waiting").length - hwm
      if plusLots > 0
        @opinfo_many.store(op, plusLots)
        surplusLots += plusLots
      end
    }
    @opinfo_few.store(@lotstart_op, surplusLots)
    redistribute
  end

  # ------------------------------------------------------------------------------
  # watch number of processing lots
  # ------------------------------------------------------------------------------
  # :duration in min
  # :sleeptime in s
  # :loops as count
  # sample calls:
  # test.watch_num_processing_lots :duration=>1
  # test.watch_num_processing_lots :loops=>3
  # test.watch_num_processing_lots :loops=>-1 # infinitely
  def watch_num_processing_lots(params={})
    sleeptime = (params[:sleeptime] or 10)
    duration = (params[:duration] or 0)
    loops = (params[:loops] or (duration > 0 ? duration*60/sleeptime : 3))
    $log.info "... watching number of processing lots: [#{duration}] minutes - [#{loops}] loops - [#{sleeptime}] seconds sleep"
    $log.info "#{num_processing_lots} processing lots"
    until loops == 0
      $log.info "#{num_processing_lots} processing lots"
      loops -= 1
      sleep sleeptime
    end
  end

  # ------------------------------------------------------------------------------
  # unload carriers from tools and stock in
  # cleanup_tools [true]
  # use parameter value true
  # to reset online mode and adjust status independently of port status
  # ------------------------------------------------------------------------------
  # to be used after stopping route or transport system crash
  # sets all tools to Offline-1 and then to Auto-3 to ensure Auto-3 capability
  # starting point is a list of tools
  def cleanup_tools(force=false)
    $log.info "... cleaning up tools"
    failedTools = []
    @tools.each{|tool|
      # unload tool
      $log.info "... #{tool}"
      toUnload = {}
      toAdjust = false
      (@sv.eqp_info tool).ports.each{|port|
        if port.state != "LoadReq"
          toAdjust = true
          if port.loaded_carrier != ""
            # carrier on port - to be unloaded
            toUnload.store port.port, port.loaded_carrier
          end
        end
      }
      if toAdjust or force
        # set tool to offline
        res = @sv.eqp_mode_change tool, "Off-Line-1"
        if res != 0 # EI not running
          failedTools.push tool
        else # EI running
          # unload carrier and transfer to stocker
          toUnload.each{|todo|
            port = todo[0]
            cass = todo[1]
            $log.info "... unloading #{cass} from #{tool}/#{port}"
            # unload may fail because processing hangs - then it will be handled below
            @sv.eqp_unload tool, port, cass
            @sv.carrier_xfer_status_change cass, "MI", @stocker, ""
          }
          # force operation complete, if any hanging
          (@sv.eqp_info tool).ports.each{|p|
            cj = p.cj
            carrier = p.loaded_carrier
            port = p.port
            if carrier != "" # carrier reserved on port
              lot = (@sv.carrier_status carrier).lots[0]
              @sv.running_hold lot, tool, cj  # running hold
              @sv.eqp_opecomp tool, cj, :force=>true # force operation complete
              @sv.eqp_unload tool, port, carrier # unload cassette
              @sv.carrier_xfer_status_change carrier, "MI", @stocker, "" # cassette to stocker
            end
          }
          # set tool to Auto-3
          @sv.eqp_mode_change(tool, 'Auto-3')
          sleep 5 # wait for sync.
          # adjust status
          #@sv.eqp_status_adjust tool
        end # EI running
      end # to adjust
    }
    if failedTools.length > 0 # failure
      failed = failedTools.join(",")
      $log.error "failed (EI not running): #{failed}"
    end
    nil
  end

  # ------------------------------------------------------------------------------
  # cancel lots reservations
  # ------------------------------------------------------------------------------
  def cancel_reservations
    $log.info "... cancel lot reservations"
    i = 0
    canceled = 0
    failed = 0;
    @tools.each{|tool|
      (@sv.eqp_info tool).rsv_cjs.each{|cj|
        res = @sv.slr_cancel tool, cj
        if res == 0
          canceled += 1
        else
          failed +=1
        end
      }
      i += 1
      if i%50 == 0
        $log.info "... #{i} tools checked"
      end
    }
    $log.info "... #{i} tools checked"
    $log.info "... #{canceled} control jobs canceled, #{failed} cancels failed"
  end

  # ------------------------------------------------------------------------------
  # release holds
  # ------------------------------------------------------------------------------
  def release_holds
    $log.info "... releasing holds"
    lotList = @sv.lot_list :route=>@route, :status=>"ONHOLD"
    released = 0
    failed = 0
    until lotList.length == 0
      lotList.each{|lot|
        res = @sv.lot_hold_release lot, nil
        if res == 0
          released +=1
        else
          failed += 1
        end
      }
      $log.info "#{released} lots released, #{failed} failed"
      lotList = @sv.lot_list :route=>@route, :status=>"ONHOLD"
    end
  end

  # ------------------------------------------------------------------------------
  # requeue lots
  # ------------------------------------------------------------------------------
  def requeue_lots
    $log.info "... requeue lots"
    i = 0
    @ops.each{|op|
      #$log.info "... #{op}"
      lotList = @sv.lot_list :route=>@route, :opNo=>op
      lotList.each{|lot|
        @sv.lot_requeue lot
        i += 1
        if i%@ii == 0
          $log.info "... #{i} lots requeued"
        end
      }
    }
    $log.info "... #{i} lots requeued"
  end

  # find number of lots at operations
  def find_lots_at_ops
    $log.info "... analyze #{@ops.length} operations"
    i = 0
    # initialise map: each operations has 0 lots
    @opinfo = {}
    @ops.each{|op|
      @opinfo.store(op, (@sv.lot_list :route=>@route, :opNo=>op).length)
    }
    $log.info @opinfo
  end

  # store lot number differences for operations with too few and too many lots
  def store_diffs(numOps)
    $log.info "... store differences"
    i = 1 # i. operation of route
    numLots = 0
    @opinfo.each{|opi|
      numLots += opi[1]
    }
    $log.info "#{numLots} lots on route"
    @num_lots_per_op = numLots / numOps
    $log.info "#{@num_lots_per_op} required per operation"
    @opinfo_few = {}
    @opinfo_many = {}
    @opinfo.each{|opi|
      op = opi[0]
      numLots = opi[1]
      if i > numOps
        # locate all lots of this operation to other operations
        @opinfo_many.store op, numLots
      else
        # store additional needed lots
        if numLots < @num_lots_per_op
          @opinfo_few.store op, @num_lots_per_op - numLots
        end
        # store surplus lots
        if numLots > @num_lots_per_op
          @opinfo_many.store op, numLots - @num_lots_per_op
        end
      end
      i += 1
    }
    $log.debug "missing lots at #{@opinfo_few.length} operations"
    $log.debug @opinfo_few
    $log.debug "surplus lots at #{@opinfo_many.length} operations"
    $log.debug @opinfo_many
  end

  # redistribute lots
  def redistribute
    $log.info "...try to fill up #{@opinfo_few.length} operations"
    while @opinfo_few.length > 0
      if @opinfo_many.length == 0
        # no more lots available - breaking...
        $log.warn "!!! no more lots available - breaking..."
        break
      end
      sourceOp = @opinfo_many.first[0] # source operation
      sourceAvail = @opinfo_many.first[1] # available lots at source operation
      targetOp = @opinfo_few.first[0] # target operation
      toBeLocated = @opinfo_few.first[1] # additional lots needed at target operation
      toLocate = sourceAvail < toBeLocated ? sourceAvail : toBeLocated
      # locate lots to other op.
      $log.info "... try to locate #{toLocate} lots to #{targetOp} from #{sourceOp}"
      located = locate_from_to sourceOp, targetOp, toLocate
      $log.info "... #{located} lots located to #{targetOp} from #{sourceOp}"
      # if number of lots reached for target op., use next target op.
      if located >= toBeLocated  # should really be only =
        @opinfo_few.delete targetOp
      else
        @opinfo_few.store targetOp, toBeLocated - located
      end
      # update remaining lots of source op.
      if located >= sourceAvail # should really be only =
        $log.info "... #{sourceOp} has no more surplus lots"
        @opinfo_many.delete sourceOp
      else
        @opinfo_many.store sourceOp, sourceAvail - located
      end
    end
  end

  # locate 'numLots' lots from operation 'opFrom' to operation 'opTo'
  def locate_from_to (opFrom, opTo, numLots)
    if numLots < 1
      return 0
    end
    no = 0
    lotList = @sv.lot_list :status=>"Waiting", :route=>@route, :opNo=>opFrom
    lotList.each{|lot|
      rc = @sv.lot_opelocate lot, opTo
      no += 1 if rc == 0
      if no == numLots
        break
      end
    }
    return no
  end

  # ------------------------------------------------------------------------------
  # Misc.
  # ------------------------------------------------------------------------------

  # inhibit the route for 1 year
  def inhibit_route
    @sv.inhibit_entity(:route, @route, tend: @sv.siview_timestamp(Time.now + 365 * 24 * 3600))
  end

  def inhibit_route_cancel
    @sv.inhibit_cancel(:route, @route)
  end

  # ------------------------------------------------------------------------------
  # inhibit 'numInhibit' tools
  # ------------------------------------------------------------------------------
  def inhibit_tools (numInhibit)
    if numInhibit > 0 and numInhibit < @tools.length
      $log.info "... inhibiting #{numInhibit} tools"
      tools[0..numInhibit-1].each{|tool|
        @sv.inhibit_entity(:eqp, tool, tend: @sv.siview_timestamp(Time.now + 365 * 24 * 3600))
      }
    end
  end

  # ------------------------------------------------------------------------------
  # cancel tool inhibits
  # ------------------------------------------------------------------------------
  def inhibit_tools_cancel
    @tools.each{|tool|
      @sv.inhibit_cancel "Equipment", tool
    }
  end

  # wait until no more "Processing" lots
  def wait_until_processing_finish
    $log.info "... waiting until processing will have been finished"
    num = num_processing_lots
    $log.info "... #{num} processing lots"
    while num  > 0
      $log.info "... #{num} processing lots"
      sleep 10
      num = num_processing_lots
    end
  end

  # get number of processing lots
  def get_num_processing_lots
    return (@sv.lot_list :product=>@product, :status=>"Processing").length
  end

  # adjust all tools' status
  def tools_status_adjust
    $log.info "... adjusting tool status"
    errTools = []
    @tools.each{|tool|
      $log.info "... #{tool}"
      if (@sv.eqp_status_adjust tool) != 0
        errTools.push tool
      end
    }
    if errTools.length > 0
      $log.error "tools returning error: #{errTools.join(',')}"
    end
  end

  # info for all carriers
  def carrier_info
    @carriers.each{|carrier|
      $log.info(@sv.carrier_status(carrier))
    }
    nil
  end

  # ------------------------------------------------------------------------------
  # stock in carriers from interbays (obsolete)
  # ------------------------------------------------------------------------------
  # to be used after stopping route or transport system crash
  # (carriers may wait forever in interbay for transport)
  # verify that no more reservations are done (-> inhibit)
  # starting point is a list of carriers
  def cleanup_interbays sleeptime
    $log.info "... cleaning up #{interbays.length} interbays - moving carriers to #{@stocker}"
    # initialise map for carriers
    release_map = {} # interbay => possibly locked carriers
    @interbays.each{|inter|
      release_map.store inter, []
    }
    carriers_to_release = []
    release_candidates = []
    # collect release candidates: all carriers in interbays
    i = 0
    $log.info "... checking #{@carriers.length} carriers"
    @carriers.each{|carrier|
      stocker = (@sv.carrier_status carrier).stocker
      if @interbays.index(stocker) != nil
        # carrier is in interbay
        release_candidates.push carrier
        carrs = release_map[stocker]
        carrs.push carrier
        release_map.store stocker, carrs
      end
      i += 1
      if i%@ii == 0
        $log.info "... #{i} carriers checked"
      end
    }
    $log.info "... #{i} carriers checked"
    $log.info "candidate carriers to move: #{release_candidates.join(",")}"
    if sleeptime > 0
    # check if candidate carriers did not move meanwhile
      $log.info "... sleeping #{sleeptime} seconds before checking candidates"
      sleep sleeptime
    end
    release_map.each{|mapel|
      interbay = mapel[0]
      mapel[1].each{|carrier|
        if @sv.carrier_status(carrier).stocker == interbay
          # carrier not yet leaved interbay - has to be stocked in manually
          carriers_to_release.push carrier
        end
      }
    }
    $log.info "checked carriers to move: #{carriers_to_release.join(",")}"
    # move carriers to destination stocker
    stock_in_carriers carriers_to_release
    nil
  end

  # ------------------------------------------------------------------------------
  # move carriers to destination stocker
  # ------------------------------------------------------------------------------
  def stock_in_carriers(carriers)
    $log.info "... stocking in #{carriers.join(",")}"
    carriers.each{|carrier|
      @sv.carrier_xfer_status_change carrier, "MI", @stocker, ""
    }
    nil
  end

  # ------------------------------------------------------------------------------
  # move carriers hanging for some time in 'EO'
  # or with transfer state '-'  (for new carriers released)
  # to destination stocker
  # workaround for problem in unloading scenario
  # min - minimum duration in minutes since carrier's last claim timestamp
  # ------------------------------------------------------------------------------
  def stock_in_pending_carriers(min=15)
    $log.info ">>> stocking in carriers hanging for #{min} minutes"
    i = 0
    ok = 0
    failed = 0
    now = Time.now
    @carriers.each{|carrier|
      status = (@sv.carrier_status carrier)
      # how long carrier was not touched (in minutes)?
      durationNotTouched = (now - Time.parse(status.claim_time))/60
      if status.xfer_status == "EO" and durationNotTouched > min or status.xfer_status == "-"
        rc = @sv.carrier_xfer_status_change carrier, "MI", @stocker, ""
        rc == 0 ? ok += 1 : failed += 1
      end
      i += 1
      if i%500 == 0
        $log.info "... #{i} carriers checked"
      end
    }
    $log.info "... #{i} carriers checked: #{ok} stocked in,  #{failed} failed "
    nil
  end

  # ------------------------------------------------------------------------------
  # move matching carriers to destination stocker
  # pattern - starting carrier ID characters,
  #           e. g. 'LS' will find carriers like 'LS001' etc.
  # ------------------------------------------------------------------------------
  def stock_in_matching_carriers(pattern, emergency = false)
    $log.info "... stocking in carriers starting with '#{pattern}'"
    i = 0
    succeeded = 0
    failed = 0
    @carriers.each{|carrier|
      if carrier.index(pattern) == 0
        $log.info carrier
        cinfo = @sv.carrier_status(carrier)
        # move carrier out of tool in case of emergency
        if emergency and cinfo.xfer_status == "EI"
          @sv.carrier_xfer_status_change(carrier, "EO", cinfo.eqp)
        end
        rc = @sv.carrier_xfer_status_change(carrier, "MI", @stocker, "")
        if rc == 0
          succeeded += 1
        else
          failed += 1
        end
      end
      i += 1
      if i%@ii == 0
        $log.info("... #{i} carriers checked")
      end
    }
    $log.info("... #{i} carriers checked, #{succeeded} succeeded, #{failed} failed")
  end

  # ------------------------------------------------------------------------------
  # get empty carriers
  # ------------------------------------------------------------------------------
  def get_empty_carriers (prefix=@carrier_filters) # prefixs for carrier name - use % as wildcard
    carriers = []
    prefixes = prefix.split(",")
    prefixes.each{|prefix|
      carrList = @sv.carrier_list :carriers=>prefix, :status=>"AVAILABLE", :empty=>true
      carrList.each{|carrier|
        carriers.push carrier}
    }
    return carriers
  end

  # ------------------------------------------------------------------------------
  # renew lots: let lots no more looping get out of the route
  # and create new ones for looping start periodically
  # ------------------------------------------------------------------------------
  def renew (renewInterval=@renew_interval, first_op=false)
    if renew_interval != nil
      while true
        #stock_in_pending_carriers # should be obsolete if scenario problem solved
        renew_exit
        renew_entry @lwm, first_op # relocate to operations with low number of lots
        $log.info "sleeping #{renew_interval}..."
        sleep renew_interval
      end
    end
  end

  # ------------------------------------------------------------------------------
  # renew lots: let lots no more looping get out of the route
  # and create new ones for looping start
  # lwm defines low water mark for minimum number of waiting lots per operation
  # ------------------------------------------------------------------------------
  def renew_exit(hwm=@hwm, sleep_time=@renew_interval, once_only = false, delete=false)
    loop{
      $log.info ">>> removing lots from #{@exit_op}"
      # for case of passing arguments as shell parameter (string)
      hwm = hwm.to_i
      sleep_time = sleep_time.to_i
      toDelete = @sv.lot_list :route=>@route, :opNo=>@exit_op, :status=>"Waiting"
      numEmptiedCarriers = 0
      toDelete.each{|lot|
        numEmptiedCarriers += renew_exit_lot(lot)
      }
      if delete # delete lots
        @sv.lot_list(:route=>@route, :opNo=>@bankin_op, :status=>"SHIPPED", :delete=>true).each {|l| @sv.delete_lot_family(l)}
      end
      # remove surplus lots
      surplus = count_surplus(hwm)
      $log.info ">>> reducing lots per operation (hwm=#{hwm}): #{surplus.inspect}"
      surplus.each{|w|
        num = w[1]
        op = w[0]
        i = num
        @sv.lot_list(:status=>"Waiting", :route=>@route, :opNo=>op).reverse.each{|lot|
          if i == 0
            break
          end
          i -= 1 if renew_exit_lot(lot) > 0
        }
      }
      break if once_only
      sleep sleep_time
    }
    nil
  end

  # ------------------------------------------------------------------------------
  # schedule and location of new lots
  # ------------------------------------------------------------------------------
  # locate lots to operations with low number of operations
  # lwm - low water mark - minimum of lots required for each operation
  # first_op - locate new lots to first operation only
  def renew_entry(lwm=@lwm, sleep_time=@renew_interval, once_only = false, first_op=false)
    # for case of passing arguments as shell parameter (string)
    lwm = lwm.to_i
    sleep_time = sleep_time.to_i
    loop{
      missing = count_missing(lwm, first_op ? @ops[0..0] : @ops)
      $log.info ">>> filling lots (lwm=#{lwm}): #{missing.inspect}"
      missing.each{|w|
        num = w[1]
        op = w[0]
        renew_entry_lots(op, num)
      }
      break if once_only
      sleep sleep_time
    }
    nil
  end

  # ------------------------------------------------------------------------------
  # release new lots, stb, locate
  # will create source lots, if required
  # ------------------------------------------------------------------------------
  def renew_entry_lots(targetOp, num, numWafers=20, numSourclotWafers=5000)
    # locate lots already at lot start
    readyLots = @sv.lot_list(:product=>@product, :opNo=>@lotstart_op, :status=>"Waiting")
    readyLots.each{|lot|
      @sv.lot_opelocate lot, targetOp # locate to operation
      num -= 1
    }
    $log.info "... releasing #{num} lots for #{targetOp}"
    carrier = @carrier_filters.split(",")[0]
    num.times{
      lot = @sv.new_lot_release :customer=>@customer, :product=>product, :route=>route, :nwafers=>numWafers,
        :carrier=>carrier, :sublottype=>@sublottype, :stb=>true
      if lot == nil
        # guess there are not enough source wafers, but possibly this is not right
        # create source lot and try again
        $log.info "... creating source lot with #{numSourclotWafers} wafers"
        @sv.vendorlot_receive @start_bank, @src_product, numSourclotWafers
        lot = @sv.new_lot_release :customer=>@customer, :product=>product, :route=>route, :nwafers=>numWafers,
          :carrier=>carrier, :sublottype=>@sublottype, :stb=>true
      end
      if lot != nil
        @sv.lot_opelocate lot, targetOp # locate to operation
      else
        $log.error "ERROR: lot is nil!"
      end
    }
  end

  # ------------------------------------------------------------------------------
  # scrap lots and move out of carrier for later deletion
  # ------------------------------------------------------------------------------
  def scrap_lots (ops)
    numEmptiedCarriers = 0
    ops.each{|op|
      lotList = @sv.lot_list :route=>@route, :opNo=>op, :status=>"Waiting"
      $log.info "... scrapping #{lotList.length} lots at #{op}"
      lotList.each{|lot|
        @sv.scrap_wafers lot # scrap wafers
        numEmptiedCarriers += (@sv.wafer_sort_req(lot, nil) == 0 ? 1 : 0) # move out of carrier
        # no deletion because of timing restrictions
        #@sv.delete_lot_family lot, :sleep=>0 # delete lot, retry after 0 seconds
      }
    }
    return numEmptiedCarriers
  end

  # ------------------------------------------------------------------------------
  # release lot holds, bank in, move out of carrier and ship
  # returns 1 if carrier was emptied, 0 otherwise
  # ------------------------------------------------------------------------------
  def renew_exit_lot(lot)
    emptied = 0
    carrier = (@sv.lot_info lot).carrier
    if carrier == "" or carrier != "" and @sv.carrier_status(carrier).xfer_status != "EI"
      # lot is in carrier and carrier already unloaded
      $log.info "... #{lot} - prepare to ship"
      rc = @sv.lot_hold_release lot, nil if rc == 0 # release holds
      rc = @sv.lot_opelocate lot, @bankin_op # bank in
      rc = @sv.wafer_sort_req lot, nil if rc == 0 # move out of carrier
      if rc == 0
        emptied += 1
        rc = @sv.lot_ship lot, @ship_bank if rc == 0 # ship
      end
    else
      $log.info "... #{lot} - carrier in '#{@sv.carrier_status(carrier).xfer_status}' - not yet ready"
    end
    return emptied
  end

  # ------------------------------------------------------------------------------
  # set tools' mode
  # ------------------------------------------------------------------------------
  # pattern - start string of equipments' id, e. g. LETCL1
  # mode - e. g. "Off-Line-1", "Auto-3"
  def set_tools(pattern, mode)
    $log.info "... setting tools to #{mode}"
    succeeded = 0
    failed = []
    @tools.each{|tool|
      if (((@sv.eqp_info tool).eqp).index pattern) == 0
        if (@sv.eqp_mode_change tool, mode) == 0
          succeeded += 1
        else
          failed << tool
        end
      end
    }
    if failed.length == 0
      $log.info "#{succeeded} tools set to #{mode}"
    else
      $log.error "#{succeeded} tools set to #{mode}, failed: #{failed.join(',')}"
    end
  end

  # ------------------------------------------------------------------------------
  # set tools "Off-Line-1"
  # ------------------------------------------------------------------------------
  # pattern - start of equipment id, e. g. LETCL1
  def set_tools_offline1(pattern)
    set_tools pattern, "Off-Line-1"
  end

  # ------------------------------------------------------------------------------
  # set tools "Auto-3"
  # ------------------------------------------------------------------------------
  def set_tools_auto3(pattern)
    set_tools pattern, "Auto-3"
  end

  # ------------------------------------------------------------------------------
  # check carrier xfer states of tool's what's next list
  # and changes from "-" to stocked in, if needed
  # ------------------------------------------------------------------------------
  def check_xfer_states (tools=@tools)
    tools.each{|tool|
      $log.info "... checking carrier xfer states of #{tool} what's next list"
      (@sv.what_next tool).each{|lot|
        if lot.route == @route
          if (@sv.carrier_status lot.carrier).status == "-"
            @sv.carrier_xfer_status_change "MI", @stocker, ""
          end
        end
      }
    }
    nil
  end

  # ------------------------------------------------------------------------------
  # count missing waiting lots at operations
  # lwm - the low water mark
  # returns [[operation#, #missing], ...] sorted ascending by #missing
  # ------------------------------------------------------------------------------
  def count_missing(lwm, ops=@ops)
    lmap = {}
    ops.each{|op|
      missing = lwm - (@sv.lot_list :route=>@route, :opNo=>op, :status=>"Waiting").length
      if missing > 0
        lmap.store op, missing
      end
    }
    return lmap.sort{|a,b| b[1]<=>a[1]} # sort by ascending # lots
  end

  # ------------------------------------------------------------------------------
  # count surplus waiting lots at operations
  # hwm - the high water mark
  # returns [[operation#, #surplus], ...]
  # ------------------------------------------------------------------------------
  def count_surplus(hwm)
    lmap = {}
    @ops.each{|op|
      surplus = (@sv.lot_list :route=>@route, :opNo=>op, :status=>"Waiting").length - hwm
      if surplus > 0
        lmap.store op, surplus
      end
    }
    return lmap
  end

  def stress_user_params
    while true
      change_user_params(@stress_user_par_threads, @stress_user_par_sleep, @stress_user_par_loops)
    end
  end

  # set user parameter values of test lots (for mixed load)
  # number_of_threads - number of threads to use
  # n - number of user parameters to change per lot in every cycle
  # sleep_time - sleep time after changed parameters for one lot, to get target throuput
  def change_user_params(number_of_threads=4, sleep_time=4, n=10)
    startTime = Time.now
    threads = []
    numChanges = 0
    numChangesT = []
    opsPerThread = get_ops_per_thread(number_of_threads)
    opsPerThread.each{|ops|
      if number_of_threads == 1 # without threading
        threads << Thread.current
        changeUserParams(ops, n, sleep_time)
      else
        threads << Thread.new{changeUserParams(ops, n, sleep_time)}
      end
    }
    threads.each {|thread|
      thread.join if number_of_threads > 1 # join threads
      numChangesT << thread["numChanges"]
    }
    endTime = Time.now
    diffS = (endTime - startTime).to_i
    numChangesT.each{|c| numChanges += c}
    $log.info "change_user_params: #threads=#{number_of_threads} n=#{n} #ops/thread=#{opsPerThread[0].length}"
    $log.info "change_user_params: #changes: #{numChanges} [#{numChangesT.join(',')}]"
    $log.info "change_user_params: run time: #{diffS}s = #{diffS.to_f/60}min from #{startTime.strftime('%Y-%m-%d %H:%M:%S')} to #{endTime.strftime('%Y-%m-%d %H:%M:%S')}"
  end

  # change user parameter and read it twice
  def changeUserParams(ops, n, sleep_time)
    Thread.current["numChanges"] = 0
    # cofigured script parameters:
    paramPrefixes = ["LET_I", "LET_S", "LET_R"] # script parameter names start with these strings
    paramsPerPrefix = 7 # number of parameters per prefix, maximum: 20
    ops.each{|op|
      lotList = @sv.lot_list(:route=>@route, :opNo=>op)
      lotList.each{|lot|
        n.times{|i|
          # select user parameter to change randomly
          paramName = paramPrefixes[rand(3)] + rand(paramsPerPrefix).to_s
          res = @sv.user_parameter("Lot", lot, :name=>paramName)
          res = @sv.user_parameter_change("Lot", lot, paramName, i, 1)
          res = @sv.user_parameter("Lot", lot, :name=>paramName)
          Thread.current["numChanges"] += 1
        }
      sleep sleep_time
      }
    }
    end

  # return array containing array of operations for each thread
  # number_of_threads - number of threads
  #                     defaults to 2, set to 1 if < 0
  def get_ops_per_thread(number_of_threads = 2)
    number_of_threads = 1 if number_of_threads < 1
    numOpsPerThread = @ops.length / number_of_threads
    ###numOpsPerThread = 1 ############# TEMP. TEST ONLY
    res = []
    number_of_threads.times{|i|
      res << @ops.slice(i * numOpsPerThread, numOpsPerThread)
    }
    return res
  end

end # class

# load 'C:/Users/tklose/sw/f1_qa/ruby/loadtests/Test80k.rb'
# load "ruby/loadtests/Test80k.rb"
# load "ruby/misc/_attic/loadtests/Test80k.rb"  # 2018
# t = Test80k.new()
#$log.level = Logger::INFO
#test.stop 50
#test.start
#t.distribute_lots

def stin
    "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ".split("").each {|char| $let.carrier_list(:carrier=>"LS#{char}%").each {|c| $let.carrier_xfer_status_change(c, "MI", "ZFG0410") if $let.carrier_status(c).xfer_status.end_with?("O")}}
    "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ".split("").each {|char| $let.carrier_list(:carrier=>"LSA#{char}%").each {|c| $let.carrier_xfer_status_change(c, "MI", "ZFG0410") if $let.carrier_status(c).xfer_status.end_with?("O")}}
end

def xmdl
    "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ".split("").each {|char| $let.carrier_list(:carrier=>"LS#{char}%").each {|c| $let.carrier_xfer_jobs_delete(c)}}
    "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ".split("").each {|char| $let.carrier_list(:carrier=>"LSA#{char}%").each {|c| $let.carrier_xfer_jobs_delete(c)}}
end


def lhr
    $let.lot_list(:lot=>"LET%", :status=>"ONHOLD").each {|l| $let.lot_hold_release(l, nil)}
end

def enpr
    $let.lot_list(:lot=>"LET%", :status=>"Processing").each {|l|
      li = $let.lot_info(l)
      $let.running_hold(l, li.eqp, li.cj)
      $let.eqp_opecomp(li.eqp, li.cj, :force=>true)
      port = nil
      #eqpi = $let.eqp_info(li.eqp)
      #eqpi.ports.each {|p| port = p.port if p.loaded_carrier == li.carrier}
      $let.eqp_unload(li.eqp, port, li.carrier)
      }
end
