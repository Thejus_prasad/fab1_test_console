$:.unshift File.dirname(__FILE__) unless $:[0] == File.dirname(__FILE__)
require 'SvLog'

# ------------------------------------------------------------------------------
# 80 k load test utility
# author Beate Kochinka
# created 2011-04
#
# $Rev: 16952 $
# $LastChangedDate: 2014-08-21 12:59:31 +0200 (Do, 21 Aug 2014) $
# $LastChangedBy: ssteidte $
# ------------------------------------------------------------------------------
class MonLoadTest80k
  attr_accessor :env, :sv, :svlog, :svlog_prod,
    :product, :route, :ops, :carriers, :tools, 
    :interbays, :stocker, :sleep_time, 
    :start_bank, :ship_bank, :src_product,
    :num_lots_per_op,
    :customer, :sublottype,
    # renewing lots:
    :num_lots_min, :num_lots_max, # min/max number of lots for product
    :num_lots_per_op_min, :num_lots_per_op_max, # min/max number of lots per op.
    :renew_interval, # interval in seconds
    :lwm, # low water mark for waiting lots per operation
    :hwm, # high water mark for waiting lots per operation
    # ---------------
    :lotstart_op, # value of script parameter TestRouteStartOp
    :start_op, # value of script parameter TestRouteStartOp
    :exit_op, # value of script parameter TestRouteExitOp
    :bankin_op, # bank in operation
    :max_loops, # value of script parameter TestRouteStartOp
    :ops2tools, # operations to tools mapping
    :lot_filter # lot id filter
    :carrier_filter # filter for empty carriers

  def initialize(params={})
    # use defaults for 80 k test
    @env = (params[:env] or "let")
    @sv = (params[:sv] or SiView::MM.new(env))
    @svlog = SvLog.new(@env)
    @svlog_prod = SvLog.new("prod")
    @tools = (params[:tools] or tools = "ACS100,ACS101,A-DISPO,ALC300,ALC301,ALC302,ALC303,ALC304,ALC305,ALC306,ALC307,ALC351,ALC352,ALC354,ALC370,ALC400,ALC403,ALC405,ALC406,ALC407,ALC408,ALC409,ALC410,ALC411,ALC421,ALC422,ALC423,ALC600,ALC601,ALC602,ALC603,AMI100,AMI101,AMI103,AMI104,AMI105,AMI107,AMI910,AMI912,AMI913,AMI914,AMI915,AMI916,AMI917,ATC100,ATC101,ATC1019,ATC102,ATC103,ATC104,ATC105,ATC106,ATC107,ATC108,ATC109,ATC110,ATC1100,ATC1101,ATC1102,ATC1103,ATC1104,ATC1105,ATC1106,ATC1107,ATC111,ATC112,ATC113,ATC114,ATC115,ATC116,ATC117,ATC118,ATC119,ATC120,ATC121,ATC122,ATC123,ATC124,ATC200,ATC201,ATC202,ATC203,ATC204,ATC205,ATC206,ATC207,ATC330,ATC400,ATC401,ATC402,ATC403,ATC404,ATC405,ATC406,ATC407,ATC408,ATC409,ATC410,ATC411,ATC412,ATC413,ATC414,ATC415,ATC416,ATC417,ATC418,ATC419,ATC420,ATC421,ATC422,ATC423,ATC424,ATC425,ATC426,ATC427,ATC428,ATC429,ATC430,ATC431,ATC432,ATC433,ATC434,ATC435,ATC436,ATC437,ATC438,ATC439,ATC440,ATC441,ATC442,ATC443,ATC444,ATC445,ATC446,ATC447,ATC448,ATC449,ATC450,ATC451,ATC452,ATC453,ATC454,AWS110,AWS1101,AWS1103,AWS1104,AWS111,AWS112,AWS120,AWS1201,AWS1204,AWS1302,AWS1305,AWS1401,AWS150,AWS1501,AWS1502,AWS1508,AWS151,AWS152,AWS153,AWS154,AWS155,AWS160,AWS162,AWS163,AWS164,AWS165,AWS166,AWS172,AWS173,AWS180,AWS1803,AWS181,AWS1811,AWS182,AWS183,AWS184,AWS190,AWS191,AWS192,AWS193,AWS200,AWS201,AWS202,AWS203,AWS204,AWS210,AWS700,AWS800,AWS810,AWS811,AWS812,AWS832,AWS833,AWS834,BDI100,BET320,BFI100,BFI101,BFI102,BFI103,BFI104,BFI105,BFI107,BFI108,BFI109,BFI1103,CDS1100,CDS1501,CDS1600,CDS202,CDS203,CDS500,CDS501,CDS502,CDS503,CDS504,CDS506,CDS507,CDS508,CDS509,CDS510,CDS511,CDS512,CDS513,CDS514,CDS515,CDS521,CVD1100,CVD1600,CVD200,CVD201,CVD300,CVD301,CVD320,CVD321,CVD322,CVD323,CVD324,CVD340,CVD400,CVD402,CVD403,CVD420,CVD421,CVD422,CVD600,CVD601,CVD602,CVD603,CVD604,CVD605,CVD606,CVD607,CVD608,CVD700,CVD701,CVD702,CVD703,CVD704,CVD705,CVD706,CVD710,CVD800,CVD801,CVD802,CVD803,CVD804,CVD805,CVD806,CVD807,CVD850,CVD851,CVD852,CVD900,CVD901,CVD902,CVD903,CVD904,CVD905,CVD906,CVD907,CVD908,CVD909,D-DISPO,DFS200,DFS201,DFS202,DFS203,DFS204,DFS600,DFS601,DFS603,DFS605,DFS606,DFS607,DFS608,DFS609,DFS610,DFS611,DFS612,DFS613,DFU120,DFU121,E-DISPO,EEX102,EPI100,EPI101,EPI102,EPI103,EPI104,EPI105,EPI106,EPI107,EPI108,EPI109,ETX100,ETX101,ETX102,ETX103,ETX104,ETX1250,ETX1500,ETX1550,ETX1601,ETX1650,ETX1658,ETX1659,ETX1920,ETX200,ETX201,ETX202,ETX203,ETX204,ETX250,ETX300,ETX301,ETX302,ETX400,ETX401,ETX402,ETX501,ETX502,ETX503,ETX550,ETX551,ETX600,ETX601,ETX602,ETX603,ETX604,ETX605,ETX606,ETX650,ETX651,ETX652,ETX653,ETX654,ETX655,ETX656,ETX657,ETX658,ETX659,ETX700,ETX701,ETX702,ETX703,ETX704,ETX705,ETX706,ETX750,ETX751,ETX752,ETX753,ETX754,ETX755,ETX801,ETX900,ETX901,ETX902,ETX950,ETX951,FVX100,FVX1200,FVX150,FVX1500,FVX201,FVX301,FVX302,FVX303,FVX401,FVX402,FVX403,FVX420,FVX500,FVX501,FVX520,FVX600,FVX601,FVX602,FVX603,FVX604,FVX605,FVX900,FVX901,FVX902,FVX903,HDP200,HDP201,HDP202,IMP100,IMP101,IMP102,IMP103,IMP104,IMP105,IMP106,IMP107,IMP108,IMP1100,IMP1101,IMP1300,IMP200,IMP201,IMP202,IMP203,IMP204,IMP205,IMP206,INS900,INS901,INS902,INS903,INS904,INS905,INS906,LSA100,LSA101,MDX1400,MDX1600,MDX1700,MDX200,MDX202,MDX203,MDX220,MDX221,MDX222,MDX223,MDX250,MDX251,MDX400,MDX450,MDX600,MDX601,MDX700,MDX701,MDX800,MDX801,MDX802,MDX803,MDX804,MDX805,MDX806,MDX810,MDX811,MDX812,MDX900,MDX901,MES101,MES102,MES1501,MES500,MES501,MES503,MES504,MES505,MET1800,MET300,MET800,MET801,MPX200,MPX202,MPX203,MPX251,MPX252,MPX253,MPX254,MPX255,MPX260,MPX261,MPX600,MPX601,MPX602,MPX603,MPX604,MPX605,MPX606,MPX607,MPX608,MPX609,MPX610,MPX710,MPX711,OCD100,OCD700,OCD701,O-LOTSTART,OPI100,OPI101,OPI104,OPI1120,OPI500,OPI501,OPI900,OPI901,OPI902,OPI903,OPI904,OPI905,OPI906,OPI907,OPI908,OPI921,OPI923,OVL100,OVL101,OVL102,OVL103,OVL104,OVL105,OVL200,OVL201,OVL300,OVN500,OVN501,OVN502,OVN503,OVN510,OVN511,OVN512,P-DISPO,POL1200,POL200,POL201,POL202,POL300,POL400,POL401,POL700,POL701,POL702,POL703,POL704,POL705,POL706,POL707,POL708,POL709,POL710,POL711,POL712,POL713,POL714,POL715,POL716,POL717,PSX104,PSX105,PSX106,PSX107,PSX108,PSX130,PSX150,PSX151,PSX152,PSX153,PSX154,PSX155,PSX156,PSX210,PSX211,PSX212,PSX213,PSX500,PSX501,Q-DISPO,QRE1152,QRE150,QRE151,RTA100,RTA101,RTA102,RTA103,RTA104,RTA105,RTA1100,RTA1400,RTA1460,RTA200,RTA201,RTA300,RTA301,RTA302,RTA303,RTA400,RTA401,RTA450,S-DISPO,SDISPOPP,SEI500,SEI502,SEM100,SEM110,SEM111,SEM112,SEM113,SEM114,SEM120,SEM1520,SEM500,SEM501,SEM502,SEM510,SEM511,SEM512,SEM513,SEM514,SEM515,SEM517,SNK1600,SNK311,SNK312,SNK313,SNK314,SNK315,SNK320,SNK321,SNK322,SNK330,SNK331,SNK332,SNK340,SNK341,SNK360,SNK380,SNK500,SNK501,SNK502,SNK510,SNK600,SNK601,SNK602,SNK620,SNK621,SNK630,SNK631,SNK900,SNK902,SNK903,SNK911,SNK913,SNK920,SNK921,SNK922,SNK923,SNK924,SNK925,STP900,STP902,STP903,STP904,THK110,THK1104,THK111,THK112,THK113,THK114,THK115,THK116,THK117,THK118,THK1400,THK1500,THK501,THK502,THK503,THK504,THK505,THK506,THK508,THK509,THK511,THK513,THK521,THK522,THK523,THK610,THK612,TRK900,TRK901,TRK902,TRK910,TRK911,TRK912,UVC500,UVC501,UVC502,UVC550,UVC551,XPS100,XPS1100".split(','))
    @customer = (params[:customer] or "80K")
    @product = (params[:product] or "3150CF.E0")
    @sublottype = (params[:sublottype] or "PO")
    @route = (params[:route] or "C02-1152.01")
    @max_loops = (params[:max_loops] or 1)
    @ops = (params[:ops] or "1000.100,1100.1000,1100.1100,1100.1200,1100.1300,1100.1400,1100.1500,1100.1600,1100.1700,1100.1800,1100.1900,1100.2000,1100.2010,1100.2015,1100.2020,1100.2025,1100.2050,1100.2075,1100.2100,1100.2150,1100.2200,1100.2250,1100.2300,1100.2350,1100.2400,1100.2500,1100.2600,1100.2700,1100.2800,1100.2900,1100.3000,1100.3200,1100.3300,1100.3400,1200.0900,1200.1000,1200.1100,1200.1300,1200.1400,1200.1500,1200.1600,1200.1700,1200.1800,1200.1900,1200.2100,1200.2125,1200.2150,1200.2600,1200.2625,1200.2650,1200.2700,1200.2800,1200.3000,1200.3100,1200.3200,1200.3300,1200.3400,1300.1000,1300.1100,1300.1200,1400.900,1400.1000,1400.1100,1400.1150,1400.1200,1400.1300,1400.1400,1400.1500,1400.1600,1400.1700,1500.1000,1500.1050,1500.1100,1500.1200,1500.1300,1500.1400,1500.1500,1500.1600,1500.1800,1500.1900,1500.2000,1600.900,1600.1000,1600.1050,1600.1100,1600.1200,1600.1300,1600.1400,1600.1500,1600.1600,1600.1700,1700.900,1700.1000,1700.1050,1700.1100,1700.1200,1700.1300,1700.1400,1700.1500,1700.1600,1700.1700,1800.1000,1800.1050,1800.1100,1800.1200,1800.1300,1800.1400,1800.1500,1800.1600,1800.1700,1900.900,1900.1000,1900.1050,1900.1100,1900.1200,1900.1300,1900.1400,1900.1500,1900.1600,1900.1700,2000.1000,2000.1050,2000.1100,2000.1200,2000.1300,2000.1400,2000.1500,2000.1600,2000.1700,2100.1000,2100.1100,2200.1000,2200.1100,2200.1200,2200.1300,2300.1400,2300.1500,2300.1550,2300.1600,2300.1700,2300.1800,2300.1900,2300.2000,2300.2100,2400.1000,2500.1000,2500.1100,2500.1200,2500.1300,2500.1400,2600.1000,2600.1100,2600.1200,2600.1300,2600.1400,2600.1500,2700.900,2700.1000,2700.1050,2700.1100,2700.1200,2700.1300,2700.1400,2700.1500,2700.1600,2700.1700,2700.1800,2700.2100,2800.1000,2800.1200,2800.1300,2800.1400,2900.0800,2900.0900,2900.0950,2900.1000,2900.1100,2900.1150,2900.1200,2900.1250,2900.1300,2900.1350,2900.1400,2900.1500,2900.1600,3000.1000,3000.1100,3000.1200,3000.1300,3000.1320,3000.1330,3000.1340,3000.1350,3000.1400,3000.1500,3000.1700,3000.1800,3000.1900,3000.2000,3100.1000,3100.1100,3100.1200,3200.1000,3200.1100,3200.1200,3200.1300,3300.1350,3300.1375,3300.1400,3300.1450,3300.1500,3300.1600,3300.1700,3300.1800,3300.1900,3300.2000,3300.2100,3300.2200,3300.2300,3300.2400,3300.2500,3300.2600,3300.2650,3300.2700,3300.2800,3300.2900,3400.900,3400.1000,3400.1050,3400.1100,3400.1200,3400.1300,3400.1400,3400.1500,3400.1600,3400.1700,3500.1000,3500.1050,3500.1100,3500.1200,3500.1300,3500.1400,3500.1500,3500.1600,3500.1700,3500.1800,3500.1900,3600.1000,3600.1050,3600.1100,3600.1200,3600.1300,3600.1400,3600.1500,3600.1600,3600.1700,3700.1000,3700.1100,3700.1200,3700.1300,3700.1350,3700.1400,3700.1500,3700.1600,3700.1700,3700.1800,3700.1900,3700.2000,3800.900,3800.1000,3800.1050,3800.1100,3800.1200,3800.1300,3800.1400,3800.1500,3800.1600,3800.1700,3800.1800,3800.1900,3800.2000,3800.2100,3900.900,3900.1000,3900.1050,3900.1100,3900.1200,3900.1300,3900.1325,3900.1350,3900.1400,3900.1500,3900.1600,3900.1700,3900.1800,3900.1900,3900.2000,3900.2100,3900.2200,4000.1000,4000.1100,4000.1200,4000.1300,4000.1350,4000.1400,4000.1450,4000.1500,4000.1525,4000.1550,4000.1700,4000.1750,4000.1800,4000.1900,4000.2000,4000.2100,4000.2200,4000.2300,4000.2400,4000.2500,4000.2600,4000.2700,4000.2800,4000.2900,4000.3000,4100.1000,4100.1100,4100.1150,4100.1200,4100.1300,4100.1400,4100.1500,4100.1600,4100.1700,4200.1000,4200.1050,4200.1075,4200.1100,4200.1200,4200.1300,4200.1400,4200.1500,4200.1600,4200.1700,4200.1800,4200.1900,4300.1000,4300.1050,4300.1100,4300.1200,4300.1300,4300.1400,4300.1500,4300.1600,4300.1700,4300.1800,4300.1900,4300.2000,4300.2100,4400.1100,4400.1200,4400.1300,4400.1350,4400.1400,4400.1500,4400.1600,4400.1725,4400.1735,4400.1750,4400.1775,4400.1800,4400.1900,4500.0500,4500.1000,4500.1050,4500.1100,4500.1200,4500.1300,4500.1350,4500.1375,4500.1390,4500.1400,4500.1450,4500.1500,4500.1600,4500.1700,4500.1800,4500.1900,4500.2000,4500.2050,4500.2100,4500.2200,4500.2300,4500.2400,4500.2500,4600.1000,4600.1100,4600.1200,4600.1300,4600.1400,4600.1500,4600.1550,4600.1600,4600.1700,4600.1800,4600.1900,4600.1950,4600.2000,4600.2100,4600.2200,4600.2300,4600.2400,4700.1025,4700.1030,4700.1050,4700.1075,4700.1200,4700.1300,4700.1400,4700.1500,4700.1600,4700.1700,4700.1900,4700.2000,4700.2010,4700.2015,4700.2020,4700.2040,4700.2060,4700.2080,4700.2100,4700.2150,4700.2200,4700.2300,4700.2350,4700.2400,4700.2500,4700.2600,4700.2700,4700.2720,4700.2730,4700.2750,4700.2800,4700.2900,4700.3000,4700.3100,4700.3200,4700.3300,4700.3400,4700.3500,4700.3600,4700.3700,4700.3800,4700.4050,4700.4100,4700.4150,4700.4200,4700.4300,4700.4310,4700.4325,4700.4350,4700.4400,4800.1000,4800.1050,4800.1100,4800.1150,4800.1200,4800.1225,4800.1250,4800.1275,4800.1300,4800.1400,4800.1450,4800.1500,4800.1600,4800.1700,4800.1800,4800.1900,4800.2000,4800.2100,4800.2200,4800.2225,4800.2250,4800.2300,4800.2500,4800.2600,4800.2650,4800.2700,4800.2800,4800.3100,4800.3200,4800.3300,4800.3400,4800.3500,4800.3600,4800.3700,4800.3800,4800.3900,4800.3950,4800.4000,4800.4100,4800.4150,4800.4200,4800.4300,4800.4400,4800.4420,4800.4440,4800.4500,4800.4600,4900.1000,4900.1020,4900.1040,4900.1100,4900.1200,4900.1300,4900.1400,4900.1500,4900.1600,4900.1610,4900.1620,4900.1640,4900.1700,4900.1800,4900.1900,4900.2000,4900.2200,4900.2250,4900.2300,4900.2400,4900.2500,4900.2600,4900.2700,4900.2800,4900.2900,4900.3000,4900.3100,4900.3200,4900.3300,4900.3500,4900.3600,4900.3700,4900.3800,4900.3820,4900.3840,4900.3880,4900.3900,4900.3950,4900.4000,4900.4100,4900.4200,4900.4300,4900.4400,4900.4500,4900.4600,4900.4700,4900.4870,4900.4900,4900.4920,4900.4950,4900.4975,4900.5000,4900.5050,4900.5100,4900.5200,4900.5300,4900.5400,4900.5500,4900.5600,4900.5700,4900.5800,4900.5900,4900.6000,4900.6100,4900.6200,4900.6250,4900.6300,4900.6325,4900.6350,4900.6370,4900.6385,4900.6400,4900.6500,4900.6550,4900.6600,5000.1000,5000.1100,5000.1200,5000.1300,5000.1310,5000.1320,5000.1340,5000.1400,5000.1500,5000.1550,5000.1600,5000.1700,5000.1800,5000.1900,5000.2000,5000.2100,5000.2200,5000.2300,5000.2600,5000.2700,5000.2800,5000.2900,5000.2925,5000.2950,5000.3000,5000.3050,5000.3100,5000.3200,5000.3300,5000.3400,5000.3500,5000.3600,5000.3650,5000.3700,5000.3800,5000.3850,5000.3900,5000.3950,5000.3975,5000.4000,5000.4100,5000.4300,5000.4315,5000.4325,5000.4350,5000.4400,5000.4500,5000.4600,5000.4700,5000.4800,5000.4900,5000.5000,5000.5100,5000.5150,5000.5200,5000.5250,5000.5330,5000.5360,5000.5390,5000.5400,5000.5450,5000.5500,5100.1000,5100.1050,5100.1100,5100.1200,5100.1300,5100.1350,5100.1400,5100.1500,5100.1600,5100.1700,5100.1800,5100.1900,5100.2000,5100.2100,5100.2200,5100.2300,5100.2400,5100.2500,5100.2600,5100.2700,5100.2800,5100.2850,5100.2900,5100.2950,5100.3000,5100.3100,5100.3200,5100.3300,5100.3400,5100.3500,5100.3550,5100.3600,5100.3700,5100.3900,5100.4000,5100.4050,5100.4100,5100.4150,5100.4175,5100.4200,5100.4250,5100.4300,5100.4400,5100.4600,5100.4700,5100.4800,5100.4900,5100.5000,5100.5100,5100.5200,5100.5300,5100.5400,5100.5425,5100.5450,5100.5500,5100.5600,5100.5650,5100.5700,5100.5800,5100.5900,5200.1000,5200.1100,5200.1150,5200.1200,5200.1220,5200.1300,5200.1350,5200.1400,5200.1500,5200.1600,5200.1700,5200.1800,5200.1900,5200.2000,5200.2100,5200.2200,5200.2300,5200.2400,5200.2500,5200.2600,5200.2650,5200.2700,5200.2750,5200.2800,5200.2900,5200.3000,5200.3100,5200.3200,5200.3300,5200.3350,5200.3400,5200.3500,5200.3700,5200.3900,5200.3950,5200.3975,5200.4000,5200.4050,5200.4100,5200.4115,5200.4125,5200.4150,5200.4200,5200.4300,5200.4400,5200.4500,5200.4600,5200.4700,5200.4800,5200.4900,5200.5000,5200.5025,5200.5050,5200.5075,5200.5085,5200.5100,5200.5200,5200.5250,5200.5300,5300.1000,5300.1100,5300.1110,5300.1120,5300.1140,5300.1200,5300.1300,5300.1600,5300.1700,5300.1750,5300.1800,5300.1900,5300.2000,5300.2100,5300.2200,5300.2300,5300.2400,5300.2500,5300.2600,5300.2700,5300.2720,5300.2740,5300.2780,5300.2800,5300.2850,5300.2900,5300.3000,5300.3100,5300.3200,5300.3250,5300.3300,5300.3400,5300.3500,5300.3550,5300.3600,5300.3650,5300.3700,5300.3750,5300.3800,5300.3900,5300.4000,5300.4100,5300.4200,5300.4300,5300.4400,5300.4500,5300.4600,5300.4700,5300.4750,5300.4800,5300.4825,5300.4900,5300.5000,5300.5050,5300.5100,5400.1000,5400.1100,5400.1150,5400.1200,5400.1220,5400.1300,5400.1400,5400.1450,5400.1500,5400.1600,5400.1700,5400.1800,5400.1900,5400.2000,5400.2100,5400.2200,5400.2300,5400.2400,5400.2500,5400.2600,5400.2650,5400.2700,5400.2750,5400.2800,5400.2900,5400.3000,5400.3100,5400.3150,5400.3200,5400.3300,5400.3400,5400.3500,5400.3600,5400.3650,5400.3700,5400.3750,5400.3800,5400.3850,5400.3900,5400.4000,5400.4100,5400.4200,5400.4300,5400.4400,5400.4500,5400.4600,5400.4700,5400.4725,5400.4740,5400.4780,5400.4800,5400.4900,5400.4950,5400.5000,5400.5100,5400.5200,5500.1000,5500.1100,5500.1150,5500.1200,5500.1220,5500.1300,5500.1400,5500.1450,5500.1500,5500.1600,5500.1700,5500.1800,5500.1900,5500.2000,5500.2100,5500.2200,5500.2300,5500.2400,5500.2425,5500.2450,5500.2500,5500.2550,5500.2600,5500.2700,5500.2800,5500.2900,5500.2950,5500.3000,5500.3100,5500.3200,5500.3250,5500.3300,5500.3350,5500.3400,5500.3410,5500.3420,5500.3440,5500.3500,5500.3600,5500.3700,5500.3800,5500.3900,5500.4000,5500.4100,5500.4200,5500.4300,5500.4325,5500.4400,5500.4500,5500.4550,5500.4600,5600.1000,5600.1100,5600.1150,5600.1200,5600.1220,5600.1300,5600.1400,5600.1450,5600.1500,5600.1600,5600.1700,5600.1800,5600.1900,5600.2000,5600.2100,5600.2200,5600.2300,5600.2400,5600.2500,5600.2600,5600.2650,5600.2700,5600.2750,5600.2800,5600.2900,5600.3000,5600.3100,5600.3150,5600.3200,5600.3300,5600.3600,5600.3650,5600.3700,5600.3730,5600.3760,5600.3780,5600.3800,5600.3850,5600.3900,5600.4000,5600.4100,5600.4200,5600.4300,5600.4400,5600.4500,5600.4600,5600.4700,5600.4750,5600.4800,5600.4900,5600.4950,5600.5000,5700.1000,5700.1100,5700.1125,5700.1150,5700.1175,5700.1200,5700.1220,5700.1300,5700.1400,5700.1450,5700.1500,5700.1600,5700.1700,5700.1800,5700.1900,5700.2000,5700.2025,5700.2050,5700.2100,5700.2200,5700.2300,5700.2400,5700.2500,5700.2550,5700.2600,5700.2700,5700.2800,5700.2900,5700.3000,5700.3100,5700.3200,5700.3250,5700.3300,5700.3350,5700.3400,5700.3450,5700.3500,5700.3600,5700.3700,5700.3800,5700.3900,5700.4000,5700.4100,5700.4200,5700.4300,5700.4400,5700.4450,5700.4500,5700.4525,5700.5000,5700.5100,5700.5150,5700.5200,5800.1000,5800.1100,5800.1110,5800.1120,5800.1140,5800.1200,5800.1300,5800.1400,5800.1500,5800.1600,5800.1650,5800.1700,5800.1800,5800.1900,5800.2000,5800.2100,5800.2200,5800.2300,5800.2400,5800.2500,5800.2600,5800.2700,5800.2750,5800.2775,5800.2800,5800.2850,5800.2900,5800.3000,5800.3100,5800.3200,5800.3300,5800.3420,5800.3440,5800.3600,5800.3650,5800.3675,5800.3700,5800.3710,5800.3720,5800.3740,5800.3800,5800.3900,5800.4000,5800.4100,5800.4200,5800.4300,5800.4400,5800.4500,5800.4600,5800.4650,5800.4700,5800.4800,5800.4850,5800.4900,5900.0200,5900.0300,5900.0350,5900.0400,5900.0500,5900.0600,5900.0700,5900.0800,5900.0900,5900.1000,5900.1100,5900.1200,5900.1300,5900.1400,5900.1500,5900.1600,5900.1800,5900.1830,5900.1850,5900.1875,5900.1900,5900.2000,6000.1000,6000.1100,6000.1200,6000.1300,6000.1400,6000.1500,6000.1600,6000.1700,6000.1800,6000.1900,6000.2000,6000.2100,6000.2200,6000.2250,6000.2430,6000.2460,6000.2500,6000.2700,6000.2800,6000.2950,6000.2975,6000.2990,6000.3000,6000.3100,6000.3300,6000.3370,6000.3375,6000.3377,6000.3378,6000.3379,6000.3380,6000.3400,6000.3500,6000.3600,6100.0900,6100.1000,6100.1100,6100.1150,6100.1175,6100.1180,6100.1200,6100.1300,6100.1400,6100.1500,6100.1520,6100.1540,6100.1550,6100.1600,6100.1700,6100.1800,6100.2100,6100.2200,6100.2300,6100.2400,6100.2500,6100.2600,6100.2700,6200.1000,6200.1100,6300.1000".split(','))
    @lotstart_op = (params[:lotstart_op] or @ops.first)
    @exit_op = (params[:exit_op] or "6300.1000")
    #init_route(@ops.first, @max_loops) # attention - changes user parameters!
    @carriers = (params[:carriers] or init_carriers)
    @carrier_filter = (params[:carrier_filters] or "8K%")
    @carrier_prefix = '8K'
    @lot_filter = (params[:lot_filter] or "80K-%")
    @src_product = (params[:src_product] or "31003762.SEH") # other source products
    @start_bank = (params[:start_bank] or "ON-RAW")
    @ship_bank = (params[:ship_bank] or "OY-SHIP") 
    @bankin_op = (params[:last_op] or ops.last)
    @renew_interval = (params[:renew_interval] or 3*60)
    @lwm = (params[:lwm] or 8)
    @hwm = (params[:lwm] or 15)
    @num_lots_min = (params[:num_lots_min] or 3000)
    @num_lots_max = (params[:num_lots_max] or 3000)
    @num_lots_per_op_min = (params[:num_lots_per_op_min] or @num_lots_min/ops.length)
    @num_lots_per_op_max = (params[:num_lots_per_op_max] or @num_lots_max/ops.length)
    @stocker = (params[:stocker] or "STO110")
    @interbays = (params[:interbays] or "INTER1,INTER2,INTER3,INTER4,INTER5".split(","))
    @sleep_time = (params[:sleep_time] or 15*60) # sleep time for interbay cleanup
    @ii = 100 # log progress after each ... element
    @num_lots_per_op = nil # number of lots per operation to be located - will be computed
    nil
  end
  
  # initialize carriers
  # carriers set up but not available: LS.. DS W X Y Z BK AX AW AV AU AR AS AQ AP AO AN AM
  def init_carriers
    carriers = []
    10.times {|i|
      carriers << @sv.carrier_list(:carrier=>"8K#{i.to_s}%")
    }
    return carriers.flatten.uniq
  end
  
  # initialize the route's script parameters
  def init_route(startOp, maxLoops)
    @max_loops = maxLoops
    @start_op = startOp
    @sv.user_parameter_change "Route", @route, "TestRouteStartOp", @start_op
    @sv.user_parameter_change "Route", @route, "MaxTestRouteLoops", @max_loops
    @sv.user_parameter_change "Route", @route, "TestRouteExitOp", @exit_op
  end
  
  # get number of processing lots
  def num_processing_lots
    return (@sv.lot_list :product=>@product, :status=>"Processing").length
  end

  # tools info
  def mon_tools(params={})
    out = open_out_file(params[:out_file])
    out.puts "#{"%-8s"%"tool"} #{"%4s"%"next"} #{"%7s"%"RTDdisp"} #{"%5s"%"state"} #{"reserved"}"
    @tools.each{|tool|
      rtdDisp = @sv.rtd_dispatch_list(tool).rows.length
      wnext = @sv.what_next(tool).length
      eqpInfo = @sv.eqp_info tool
      state = eqpInfo.status == "PRD.1PRD" ? "P" : "W" # (not necessarily SBY.2WPR)
      reserved = eqpInfo.rsv_carriers
      modes = eqpInfo.ports.collect{|p|p.mode}
      out.puts "#{"%-8s"%tool} #{"%4s"%wnext} #{"%7s"%rtdDisp} #{"%5s"%state} #{"%s"%modes.inspect} #{reserved}"
    }
    close_out_file(out)
    nil
  end
  
  # watch for tools to be alive
  def mon_tools_alive
    numAlive = 0
    numNotAlive = 0
    notAlive = []
    @tools.each{|tool|
      if eqp_signaltower_off(tool) == 0
        numAlive += 1
      else
        numNotAlive += 1
        notAlive << tool
      end
    }
    puts "#{numAlive} tools alive\n#{numNotAlive} tools not responding: #{notAlive.join(' ')}"
  end

  def mon_tools_modes(params={})
    out = open_out_file(params[:out_file])
    out.puts "#{"%-8s"%"tool"} #{"port modes"}"
    @tools.each{|tool|
      rtdDisp = @sv.rtd_dispatch_list(tool).rows.length
      wnext = @sv.what_next(tool).length
      eqpInfo = @sv.eqp_info tool
      state = eqpInfo.status == "PRD.1PRD" ? "P" : "W" # (not necessarily SBY.2WPR)
      reserved = eqpInfo.rsv_carriers
      modes = eqpInfo.ports.collect{|p|p.mode}
      out.puts "#{"%-8s"%tool} #{"%s"%modes.inspect}"
      out.flush
    }
    close_out_file(out)
    nil
  end
  
  # ------------------------------------------------------------------------------
  # shows number of lots at last operation of route and returns the lot list
  # :in_carrier - true -> get lots in carriers only
  #             - false -> get lots not in carriers only
  #             - nil -> get both lots in carriers and not in carriers
  # :deletable - show deletable lots only (defaults to false)
  # :status - lot status (nil = unspecified)
  # ------------------------------------------------------------------------------
  def get_lots_lastop(params={})
    in_carrier =(params[:in_carrier] or nil)
    deletable =(params[:deletable] or false)
    status =(params[:status] or nil)
    if in_carrier == true and deletable == true
      $log.warn "in_carrier == true -> setting deletable to false"
      deletable = false
    end
    ll = []
    lotList = @sv.lot_list(:route=>@route, :opNo=>@ops.last, :sublottype=>@sublottype, :delete=>deletable, :status=>status)
    lotList.each{|lot|
      if in_carrier != nil
        if in_carrier == true # only lots in carrier
          if @sv.lot_info(lot).carrier.index(@carrier_prefix) != nil
            ll << lot
          end
        else # only lots not in carrier
          if @sv.lot_info(lot).carrier == ""
            ll << lot
          end
        end
      else # if in carrier doesn't matter
        ll << lot
      end
    }
    puts "#{ll.length} lots at #{@ops.last} (in_carrier=#{in_carrier}, deletable=#{deletable}, status=#{status})"
    return ll
  end
  
  # ------------------------------------------------------------------------------
  # monitor carriers (output):
  # xfer status, location (tool/stocker), lots
  # :carriers - carrier list (none: use carriers)
  # :out_file - output file (none: write to standard output)
  # ------------------------------------------------------------------------------
  def mon_carriers(params={})
    carriers = (params[:carriers] or @carriers)
    out = open_out_file(params[:out_file])
    carriers.each{|carrier|
      carrStat = @sv.carrier_status(carrier)
      xfer = carrStat.xfer_status
      location = carrStat.eqp + carrStat.stocker
      lots = @sv.carrier_status(carrier).lots
      out.puts "#{carrier}\t#{xfer}\t#{location}\t#{lots}"
    }    
    close_out_file(out)
  end
  
  # ------------------------------------------------------------------------------
  # monitor lots per operation (output)
  # :out_file - output file (none: write to standard output)
  # :lot - lot ID (pattern) to watch for (none: any lot ID)
  # ------------------------------------------------------------------------------
  def mon_lots(params={})
    name = (params[:lot] or "%")
    out = open_out_file(params[:out_file])
    status = (params[:status])# or "Waiting")
    out.puts "\t#{Time.now.strftime('%Y-%m-%d %H:%M')}"
    out.puts ""
    @ops.each{|op|
      numLots = 0
        if status == nil
          lotList = @sv.lot_list(:route=>@route, :opNo=>op, :sublottype=>@sublottype, :lot=>name)
        else
          lotList = @sv.lot_list(:route=>@route, :opNo=>op, :status=>status, :sublottype=>@sublottype, :lot=>name)
        end
        lotList.each{|lot|
          # for last (bank in) operation, carrier does not matter
          # because lot may already been moved out
          lotInfo = @sv.lot_info(lot)
          # lot may be deleted meanwhile by lot maintenance - so check if not null
          numLots = 
            op == @ops.last ? numLots+1 \
          : lotInfo == nil  ? numLots+1 \
          : lotInfo.carrier.index(@carrier_prefix) != nil ? numLots+1 \
          : numLots
        }
        out.puts "#{op}\t#{numLots}"
    }
    out.puts "\n\t#{Time.now.strftime('%Y-%m-%d %H:%M')}"
    close_out_file(out)
  end
  
  # ------------------------------------------------------------------------------
  # get test route lots: iterates operations
  # -> possibly lots will be missing if > 500 lots at an operation
  # :status - lot status (default: no restriction)
  # :lot - lot id pattern (default: lot filter)
  # :in_carrier - get lots in carrier/outside carrier/all
  #               as true/false/nil (defaults to nil -> all)
  # ------------------------------------------------------------------------------
  def get_lots(params={})
    status = params[:status] 
    name = (params[:lot] or @lot_filter)
    in_carrier = params[:in_carrier]
    lots = []
    @ops.each{|op|
      lotList = (status == nil ? \
      @sv.lot_list(:route=>@route, :opNo=>op, :sublottype=>@sublottype, :lot=>name) : \
      @sv.lot_list(:route=>@route, :opNo=>op, :sublottype=>@sublottype, :lot=>name, :status=>status))
      lotList.each{|lot|
        info = @sv.lot_info(lot)
        # ATTENTION! Ruby does not respect boolean operator precedence!!!
        if in_carrier == nil or (in_carrier == true and info.carrier != "") or (in_carrier == false and info.carrier == "")
          lots << lot
        end
      }
    }
    return lots
  end
  
  # show info for lots:
  # lot id, opNo, carrier, status
  def show_lots(params={})
    status = params[:status]
    name = (params[:lot] or @lot_filter)
    in_carrier = params[:in_carrier] 
    out = open_out_file(params[:out_file])
    out.puts "lot\topNo\tcarrier\tstatus\t#{Time.now.strftime('%Y-%m-%d %H:%M:%S')}"
    get_lots(:in_carrier=>in_carrier, :status=>status, :lot=>name).each{|lot|
      info = @sv.lot_info(lot)
      out.puts "#{lot}\t#{@sv.lot_info(lot).opNo}\t#{info.carrier}\t#{info.status}"
    }
    out.puts "lot\topNo\tcarrier\tstatus\t#{Time.now.strftime('%Y-%m-%d %H:%M:%S')}"
    close_out_file(out)
    nil
  end
  
  # show info for lots in carriers
  def show_lots_in_carrier(params={})
    show_lots(params.merge(:in_carrier=>true))
  end
  
  # show info for lots outside carriers
  def show_lots_outside_carrier(params={})
    show_lots(params.merge(:in_carrier=>false))
  end
  
  # put lots into test carriers
  def bag_lots(lot_list)
    needed = lot_list.length
    done = 0
    lot_list.each{|lot|
      carriers = @sv.carrier_list(:empty=>true, :status=>"AVAILABLE", :carrier=>@carrier_filter)
      if carriers.length == 0
        $log.error "no more empty carrier available"
        break
      end
      if @sv.wafer_sort_rpt(lot, carriers[0]) == 0
        done += 1
      end
    }
    $log.info "#{done} of #{needed} lots put into carrier"
  end

  def find_ops2tools(params={})
    puts("params=#{params.inspect}")
    lot = (params[:lot] or "SNOOPY00.00")
    out = open_out_file(params[:out_file])
    lotList = @sv.lot_list(:route=>@route, :status=>'Waiting', :lot=>lot)
    lotFound = false
    i = 0
    until lotFound or i == lotList.length
      lot = lotList[i]
      carrier = @sv.lot_info(lot).carrier
      # use stocked in lot only
      if carrier != "" and ["SI", "MI"].index(@sv.carrier_status(carrier).xfer_status) != nil
        lotFound = true
      else
        i += 1
      end
    end
    if i == lotList.length
      puts("no lot found")
      return
    end
    puts("lot found:#{lot} in carrier #{carrier}")
    @ops2tools = []
    ops.each{|op|
      res =$sv.lot_opelocate(lot, op)
      if res == 0
        lotInfo = $sv.lot_info(lot)
        @ops2tools  << [op, lotInfo.opEqps]
      end
    }
    @ops2tools.each{|x|
      op = x[0]
      tools = x[1]
      out.puts("#{op} #{tools.join(",")}")
    }
    close_out_file(out)
  end
  
  def validate_ops2tools
    @ops2tools.each{|x|
      op = x[0]
      tools = x[1]
      if tools & @tools == []
        $log.warn("no tool for #{op} (#{tools.join(",")})")
      end
    }
    nil
  end

  # ------------------------------------------------------------------------------
  # count successful operation starts using direct database access
  # (includes internal buffer tools)
  # min - start time (defaults to max - 1 hour)
  # max - end time (defaults to current time)
  # interval - interval in minutes for querying (defaults to 60)
  # out_file - file name for output
  # ------------------------------------------------------------------------------
  def count_opstarts(min=nil, max=nil, interval=60, out_file=nil, svlog=@svlog)
    if interval <= 0
      $log.error "invalid interval: #{interval}"
      return
    end
    out = open_out_file(out_file)
    maxTime = max == nil ? Time.now : Time.parse(max)
    minTime = min == nil ? maxTime - (60*60) : Time.parse(min)
    fromTime = minTime
    toTime = fromTime + interval*60
    $log.debug "from|to|opStarts|WOPM"
    out.puts "from\tto\topStarts\tWOPM"
    until toTime > maxTime
      res = svlog.get_opestarts(:tmin=>fromTime,:tmax=>toTime,:txresult=>'0')
      # wafer out per month: 1000 op. starts per hour = 30 k
      # (normalize for used interval
      wopm = (60/interval.to_f * res/100*3).to_i
      $log.debug "#{fromTime.strftime('%Y-%m-%d %H:%M:%S')}|#{toTime.strftime('%Y-%m-%d %H:%M:%S')}|#{res}\t#{wopm}"
      out.puts "#{fromTime.strftime('%Y-%m-%d %H:%M:%S')}\t#{toTime.strftime('%Y-%m-%d %H:%M:%S')}\t#{res}\t#{wopm}"
      fromTime += interval*60
      toTime = fromTime + interval*60
      out.flush
    end
    close_out_file(out)
  end
  
  # show transaction statistics from svlog
  def show_tx_stats(min=nil, max=nil, interval=60, out_file=nil, svlog=@svlog)
    if interval <= 0
      $log.error "invalid interval: #{interval}"
      return
    end
    out = open_out_file(out_file)
    maxTime = max == nil ? Time.now : Time.parse(max)
    minTime = min == nil ? maxTime - (60*60) : Time.parse(min)
    fromTime = minTime
    toTime = fromTime + interval*60
    i = 0
    until toTime > maxTime
      res = svlog.get_statistics(tmin: fromTime, tmax: toTime)
      out.puts "#{"start\tend\t"}#{svlog.db.ds.columns.join("\t")}"
      res.each {|row|
        out.puts "#{fromTime.strftime('%Y-%m-%d %H:%M:%S')}\t#{toTime.strftime('%Y-%m-%d %H:%M:%S')}\t#{row.collect{|col| col + "\t"}}"
        i += 1
      }
      fromTime += interval*60
      toTime = fromTime + interval*60
      out.flush
    end
    close_out_file(out)
  end
  
  # returns [[lot ...][carrier ...]] for special lots
  # details see source (changes as developer's needs)
  def get_special_lots_carriers()
    lotList = []
    carrList = []
    @carriers.each{|carrier|
      lot = @sv.carrier_status(carrier).lots.first
      if lot != nil and lot.index('U') == 0
        lotInfo = @sv.lot_info(lot)
        carrStat = @sv.carrier_status(carrier).xfer_status
        if lotInfo.status == "COMPLETED" #lotInfo.opNo != @ops.last and carrStat == "EI"
          puts lot
          carrList << carrier
          lotList << lot
        end
      end
    }
    return [lotList, carrList]
  end
  
  def get_special_eqps(lot_list)
    eqpList = []
    lot_list.each{|lot|
      carrier = @sv.lot_info(lot).carrier
      if carrier != ""
        carrStat = @sv.carrier_status(carrier)
      if carrStat.xfer_status == "EI"
        eqpList << carrStat.eqp
      end
      end
    }
    return eqpList
  end

  def scrap_at_end(lot_list)
    lot_list.each{|lot|
      # cancel control job, if any
#      cj = @sv.lot_info(lot).cj
#      if cj != ""
#        eqp = cj[0..cj.index("-")-1]
#        @sv.slr_cancel(eqp, cj)
#      end
#      @sv.lot_opelocate(lot, @ops.last)
      @sv.scrap_wafers(lot)
      @sv.wafer_sort_rpt(lot, "")
    }
  end

  # stock in carriers
  # ATTENTION: not for loaded carriers
  def stock_in(carrier_list=@carriers)
    carrier_list.each{|carrier|
      stat = @sv.carrier_status(carrier)
      #@sv.carrier_status_change(carrier, "AVAILABLE")
      if stat.xfer_status == "EI"
        @sv.carrier_xfer_status_change(carrier, "EO", stat.eqp, "", true)
        @sv.carrier_xfer_status_change(carrier, "MI", @stocker, "")
      end
    }
  end
  
  def carrier_out(tool="ALC600", carrier_list=@carriers)
    carrier_list.each{|carrier|
      stat = @sv.carrier_status(carrier)
      if stat.xfer_status == "MI" or stat.xfer_status == "SI"
        @sv.carrier_xfer_status_change(carrier, "EO", tool, "", true)
      end
    }
  end
  
  def tools_info(params={})
    tools = (params[:tools] or @tools)
    out = open_out_file(params[:out_file])
    out.puts "tool\tcjs\trsv_cjs\tloaded_carriers\trsv_carriers"
    tools.each{|tool|
      info = @sv.eqp_info(tool)
      out.puts "#{tool}\t#{info.cjs.inspect}\t#{info.rsv_cjs.inspect}\t#{info.loaded_carriers.inspect}\t#{info.rsv_carriers.inspect}"
    }
    close_out_file(out)
    nil
  end
  
  def open_out_file(out_file)
    if out_file != nil
      out = open(out_file, "w")
      $log.info "...writing output to #{out_file}"
    else
      out = $stdout
    end
    return out
  end
  
  def close_out_file(out)
    if out != $stdout
      out.close
    end
  end

  def show_lot_cycleno(lot)
    cycleno = @sv.user_parameter("Lot", lot, :name=>"TestRouteLoops").value
    puts "#{lot}\t#{cycleno}"
  end

  # lots' operation history statistics
  # :status - for lots of test route with status (default: SCRAPPED)
  # :opNo - for lots at this operation (default: last operation)
  # :nodetails - return nil 
  #            - default: return array containing number of operations for each lot
  def mon_lotophiststat(params={})
    status = (params[:status] or "SCRAPPED")
    nodetails = params[:nodetails]
    opNo = (params[:opNo] or @exit_op)
    lotList = @sv.lot_list(:route=>@route, :status=>status, :opNo=>opNo)
    numOps = lotList.collect{|lot|
      @sv.lot_operation_list_fromhistory(lot).length
    }
    count = numOps.size
    if count > 0
      max = 0
      min = 999999
      avg = (numOps.inject(0.0) {|sum, el| 
        max = el if el > max
        min = el if el < min
        sum + el 
      } / count).to_i
    else
      max = nil
      min = nil
      avg = nil
    end
    puts "op. history length of #{count} #{status} lots at #{opNo}: avg=#{avg} max=#{max} min=#{min}"
    return nil if nodetails
    res = {}
    lotList.length.times{|i|
      res.store(lotList[i-1], numOps[i-1])
    }
    return res
  end

end
