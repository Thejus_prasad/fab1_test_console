# ------------------------------------------------------------------------------
# start maintenance and stress processes for 80 k test
#
# does dynamic invocation of any Test80k method via command line:
# pass the name of the method and required arguments
#
# author Beate Kochinka 2011-08
# ------------------------------------------------------------------------------
# $Rev: 13327 $
# $LastChangedDate: 2011-08-04 16:04:33 +0200 (Do, 04 Aug 2011) $
# $LastChangedBy: bkochink $

$:.unshift File.dirname(__FILE__) unless $:[0] == File.dirname(__FILE__)
require 'Test80k'

# start the process
def start_process()
  numberOfLogFiles = 3
  logFileSize = 50*1024
  if ARGV.size < 1
    puts "usage: ... #{$0} <method> {<argument>}*\n for a Test80k method"
    exit
  end
  puts "... starting #{ARGV.join(' ')}"
  meth = ARGV[0]
  logbase = ""
  # log file name is in <workingDirectory>/log/<baseFilenameWithoutExtension>_<calledMethod>.log
  logfile = "#{Dir.getwd}/log/#{File.basename(__FILE__).split(".")[0]}_#{meth}.log"
  puts "... logging to #{logfile}"
  $log = Logger.new(logfile, numberOfLogFiles, logFileSize)
  $log.datetime_format = "%Y-%m-%d %H:%M:%S "
  $log.level = Logger::INFO
  t = Test80k.new
  # call of test's method with up to 5 arguments supported
  case ARGV.length
  when 1 then t.send(meth)
  when 2 then t.send(meth, ARGV[1])
  when 3 then t.send(meth, ARGV[1], ARGV[2])
  when 4 then t.send(meth, ARGV[1], ARGV[2], ARGV[3])
  when 5 then t.send(meth, ARGV[1], ARGV[2], ARGV[3], ARGV[4])
  when 6 then t.send(meth, ARGV[1], ARGV[2], ARGV[3], ARGV[4], ARGV[5])
  end
end

start_process if $0 == __FILE__
