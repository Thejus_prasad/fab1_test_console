=begin
Locate lots in BTF from LOTSHIPDESIG back to SORTPP for CR49666

This script is meant to be edited before it runs from the JRuby test console.
Change $env, $fname and $username before running it.

$username must be a SiView user with sufficient privileges.

Author: ssteidte, 2013-02-20
Version: 1.0.1

History:
  2013-02-20  ssteidte, created, verified in ITDC and used in Production
  2013-05-27  ssteidte, summary file generation
  
  
Usage:
preparation:
1) create a file containing the lots, one per line, lotID in first column, no headerline
2) edit $env, $fname, $username and $ntauth (must be true for real persons and false for X-UNITTEST)
3) start a test console, e.g. bin\console_R10_custom.bat (from junit_testing)

on the console:
1) load this file:
    load 'ruby/misc/prod_locate_lots_btf.rb' (password input required)
2) read lots:
    get_lots
3) verify the lots have been read correctly:
    $lots.size (compare with the number of lots in the file ($fname)
4) locate lots:
    locate_lots($lots)
    (interactive login required, progress is shown 
5) write summary to console, log file and separate summary file (next to input file):
    show_summary
=end

require 'pp'
require 'siview'

##$fname = 'C:\Temp\BTF\4310_2.txt'
##$fname = 'C:\Temp\BTF\4310_1.txt'
##$fname = 'C:\Temp\BTF\Shelby2.txt'
##$fname = 'C:\Temp\BTF\dd_all_lots_by_module.txt'
##$fname = 'C:\Temp\BTF\btf20130320.txt'
##$fname = 'C:\Temp\BTF\btf20130410.txt'
##$fname = 'C:\Temp\BTF\btf20130522.txt'
$fname = 'C:\Temp\BTF\btf20130527.txt'

$opNo = "99800.1000"
$memo = "erneutes PostProcessing per Carola Gaertner; CR51128"

$sv = SiView::MM.new("production", :ntauth=>true, :user=>"kklein") unless $sv and $sv.env == "production"
##$sv = SiView::MM.new("itdc") unless $sv and $sv.env == "itdc"

#
# ------------- no changes below this line required ------------------------
#

$lots = []
$lots_passed = {}
$lots_failed = {}


def get_lots
  # return array of lot info structs
  $lots = []
  open($fname) {|fh|
    fh.readlines.each_with_index {|line,i|
      # skip header
      #next if i == 0
      next if line.start_with?('#')
      next if line.downcase.start_with?('lot-id')
      next if line.downcase.start_with?('lotid')
      ff = line.chomp.split(';')
      $lots << ff[0] if ff[0]   # skip empty lines
    }
  }
  $log.info "read #{$lots.size} lots"
  nil
end

def get_lot_routes(lots)
  lis = $sv.lots_info(lots)
  $lot_routes = {}
  lis.each {|li| 
    $lot_routes[li.route] ||= []
    $lot_routes[li.route] << li.lot
  }
  return $lot_routes.keys.sort
end

def locate_lots(lots, params={})
  res = $sv.logon_check
  ($log.warn "no valid username/password"; return nil) unless res and res == 0
  #
  lots.each_with_index {|lot, i|
    $log.info "-- lot #{lot.inspect} (#{i+1}/#{lots.size})"
    li = $sv.lot_info(lot)
    # carrier check
    if li.carrier.start_with?("Z")
      # bankout
      res = $sv.lot_nonprobankout(lot, :memo=>$memo)
      if res == 0
        # locate
        li = $sv.lot_info(lot)
        res = $sv.lot_opelocate(lot, $opNo, :force=>(li.status == "ONHOLD"), :memo=>$memo)
        if res == 0
          # success
          $lots_passed[lot] = li.nwafers
          $log.info "success"
        else
          $lots_failed[lot] = "locate #{res}"
        end
      else
        $lots_failed[lot] = "bankout #{res}"
      end
    else
      $lots_failed[lot] = "carrier #{li.carrier.inspect}"
    end
  }
  $log.info "\nfailed lots: #{$lots_failed.pretty_inspect}"
  $lots_failed == {}
end

def show_summary
  s = ["Summary of Lots Moved, #{Time.now.iso8601}"]
  s << "================================================"
  s << "lots file: #{$fname.inspect}"
  s << "lots moved to #{$opNo}: #{$lots_passed.size} (#{$lots_passed.each_value.collect {|v| v}.sum} wafers)"
  s << "\nlots not moved (analysis required): #{$lots_failed.size}"
  s << "failed lots:"
  $lots_failed.each_pair {|l,r| s << "  #{l}\t(#{r})"}
  s << "\npassed lots:"
  $lots_passed.each_pair {|l,n| s << "  #{l}\t(#{n} wafers)"}
  $log.info "\n\n#{s.join("\n")}\n\n"
  f = File.join(File.dirname($fname), "#{File.basename($fname, File.extname($fname))}_summary.txt")
  open(f, "wb") {|fh| fh.write("#{s.join("\n")}\n")}
  nil
end
