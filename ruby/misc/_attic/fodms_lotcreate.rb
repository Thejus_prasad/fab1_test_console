# encoding: UTF-8

=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: Thomas Klose, 2015-07-24

usage:
  load 'ruby/misc/fodms_lotcreate.rb'
=end

require 'siview'
require 'thread'


def create_lots(amount, nwafers=1)
  route = "C02-1138.01"
  product = "3150CC.D1"
  slt = "PO"
  ll = []
  amount.times {ll << $sv.new_lot_release(stb: true, route: route, product: product, sublottype: slt, nwafers: nwafers)}
  return ll
end

def remove_hold(lotlist)
  lotlist.each {|l|
    $sv.lot_hold_release(l, nil)}
end

def distribute_lots(lotlist)
  li = $sv.lot_info(lotlist[0])
  ops = $sv.lot_route_lookahead(li.lot).collect{|rlh| rlh.opNo}
  ops = ops[1...-1]   #remove first and last op
  lotlist.each {|l|
    $sv.lot_opelocate(l, ops.sample)}
end

def create_lots
  $sv = SiView::MM.new('let') unless $sv
  ll = create_lots(1000)
  remove_hold(ll)
  distribute_lots(ll)
end

def delete_lots
  return # think about that piece of code!
  route = "C02-1138.01"
  product = "3150CC.D1"
  slt = "PO"
  ll = $sv.lot_list(route: route, product: product, sublottype: slt)
  threads = []
  10.times {
    threads << Thread.new {
      sv = SiView::MM.new('let')
      sv.delete_lot_family(ll.pop)
      }
    }
  thread.join
end