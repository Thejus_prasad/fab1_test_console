=begin
Locate lots

This script is meant to be edited before it runs from the JRuby test console.
Change $env, $fname and $username before running it.

$username must be a SiView user with sufficient privileges. Interactive login 
(no password shown) is required to run "change_lot_prio" from the console.

Author: ssteidte, 2011-08-10
Version: 1.0.0

History:
  2011-08-10  ssteidte, created, verified in ITDC and used in Production
  
  
Usage:
preparation:
1) Create a file containing the lots, one per line, lotID in first column, one headerline
2) Edit $env, $fname, $username and $ntauth (must be true for real persons and false for X-UNITTEST)
3) edit $opno
4) start a test console, e.g. bin\console_R10_custom.bat (from junit_testing)

on the console:
1) load this file: 
    load 'ruby/misc/prod_locate_lots.rb'
2) read lots:
    get_lots
3) verify the lots have been read correctly:
    $lots.size (compare with the number of lots in the file ($fname)
4) locate lots:
    locate_lots($lots, $opno)
    (interactive login required, progress is shown 
  
=end

require 'siview'

$env = "production"
$fname = 'C:\Temp\2011-08-11-opelocate.csv'
$username = "abecker"
$ntauth = true

$memo = "CR41032, locate backward to new DISPO PD after route change"

#
# ------------- no changes below this line required ------------------------
#

$_ll_info = Struct.new(:lot, :cur_pd, :cur_opno, :new_pd, :new_opno, :result)
$lots = []

$sv = SiView::MM.new($env, :ntauth=>$ntauth, :user=>$username)


def get_lots
  # return array of lot info structs
  $lots = []
  open($fname) {|fh|
    fh.readlines.each_with_index {|line, i|
      # skip header?
      next if i == 0
      ff = line.chomp.split(';')
      $lots << $_ll_info.new(ff[1], ff[3], nil, nil, ff[0], nil) #if ff.size == 12
    }
  }
  nil
end

def locate_lots(lots, params={})
  res = $sv.logon_check
  ($log.warn "no valid username/password"; return nil) unless res and res == 0
  #
  lots.each_with_index {|_l, i|
    li = $sv.lot_info(_l.lot)
    if li.op == _l.cur_pd #and li.opNo == _l.cur_opno
      res = $sv.lot_opelocate(_l.lot, _l.new_opno, :force=>true, :memo=>$memo)
      _l.result = res
      puts "error locating lot #{_l.lot}" if res != 0
    else
      puts "wrong location of lot #{_l.lot}"
    end
  }
  lots
end
