=begin
Support for ERP-AsmView E2E Tests. Create lots suitable for shipment via FabGUI.

Note on ERP stages and TurnKey parameters
stage UWINV: "U", "", ""
      PWINV: "J", "GG", "GG"

usage:
  load 'ruby/misc/erp_asm_e2e.rb'
  # create 3 lots with data from $tc_data['tc13'], in SiView only
  new_lot_tc "tc13", 3  
  # create 3 lots with data from $tc_data['tc13'], in SiView and AsmView
  new_asmlot_tc "tc13", 3  
=end

require 'yaml'
require 'asmviewtest'
require 'fabguictrl'

$env = "erpstage" #"f8stag"
$at = AsmView::Test.new($env, :f7=>false, :erpshipment=>false, :btf=>true) unless $at and $at.env == $env
$av = $at.av
$avb2b = $at.avb2b
$avstb = $at.avstb
$f1b2b = $at.f1b2b
$testdbbtf = $at.dbbtf
$f1sv = $at.f1sv
$f8sv = $at.f8sv
$f8b2b = $at.f8b2b
$fguictrl = FabGUI::ProcessCtrl.new("f8stag") unless $fguictrl and $fguictrl.env = $env

$_tk_u = SiView::MM::LotTkInfo.new("U")
$_tk_j_gggg = SiView::MM::LotTkInfo.new("J", "GG", "GG")
$_tk_j_gg1t = SiView::MM::LotTkInfo.new("J", "GG", "1T")

$tc_data = {
  "tc12"=>["F1", ERPMES::ErpAsmLot.new(nil, $_tk_j_gggg, "HLCYV2-AA0", nil, "TKEY01.A0", "C02-TKEY.01", "ibm", "QE", :lotshipdesig)],
  "tc13"=>["F1", ERPMES::ErpAsmLot.new(nil, $_tk_j_gg1t, "MPW0413SAMPLE-A02", nil, "TKEY01.A0", "C02-TKEY.01", "imc", "QE", :lotshipdesig)],
  "tc13"=>["F1", ERPMES::ErpAsmLot.new(nil, $_tk_j_gggg, "*SHELBY21AE-M00-JB4", "6010000543:02:01", "SHELBY21AE.B4", "C02-1431.01", "qgt", "PB", :lotshipdesig)],
  "tc80"=>["F8", ERPMES::ErpAsmLot.new(nil, $_tk_u, "OBANSK-U02", nil, "OBANSK.02", "FF-OBANSK.01", "ibm", "QE", :lotshipdesig)],
  "tc81"=>["F8", ERPMES::ErpAsmLot.new(nil, $_tk_j_gggg, "GIMLI21MA-JA0", nil, "GIMLI21MA.A0", "FF-GIMLI2.01", "qgt", "PO", :tkbumpshipinit)], #:tkbumpshipdispo)],
  "tc82"=>["F8", ERPMES::ErpAsmLot.new(nil, $_tk_j_stst, "BAGERA11MA-JA0", nil, "BAGERA11MA.A0", "FF-BAGERA1-TN1.01", "qgt", "PO", :tkbumpshipdispo)],
#  "tc83"=>$_erpmeslot.new(nil, "ibmca", "QE", nil, nil, nil, :lotshipdesig, "FF-OBANSK.01", "OBANSK.01", "U", "", "", "OBANSK-U01"),
}

$bins = [[404,150,238],[150,300,340],[508,164,98]]

$tc_lot_number = {
  "tc12"=>1,
  "tc13"=>1,
  "tc80"=>1,
  "tc81"=>1,
}


def init_parameters
  $carrier = "Q0001"  # first carrier, count up automatically
  $lots = {}
end

def create_carriers(c0, n)
  n.times {
    res = $sv.durable_register(c0, "BEOL")
    return false unless res
    res = $sv.carrier_status_change(c0, "AVAILABLE")
    return false unless res == 0
    c0 = c0.siview_next
  }
  true
end

def release_lots
  $lots.each_pair {|p, ll| ll.each {|l| $sv.lot_hold_release(l)} }
end

def show_lots(params={})
  $lots.each_pair {|p, ll| puts "#{p}:\n  #{ll.join(" ")}"}
  fname = params[:file]
  open(fname, "wb") {|fh| fh.write($lots.to_yaml)} if fname
  nil
end

def all_new_lots
  $lots = {}
  $tc_lot_number.each_pair {|tc, n| new_asmlot_tc(tc, n)}
end

# create lots for SiView tests (no AsmView)
# tc is something like "tc23", n the number of lots to create
# return true on success
def new_lot_tc(tc, n=1, params={})
  $log.info "new_lot_tc #{tc.inspect}, #{n}"
  env, lc = $tc_data[tc]
  ($log.warn "no such test case"; return nil) unless lc  
  sv = (env == "F1") ? $f1sv : $f8sv
  b2b = (env == "F1") ? $f1b2b : $f8b2b
  $lots ||= {}
  $lots[tc] ||= []
  n.times {|i|
    $log.info "\n creating lot #{i+1}/#{n}"
    params[:carrier] ||= "Q%"
    params.merge!(:carrier_category=>nil)
    lot = b2b.new_lot(lc, params)
    $lots[tc] << lot
    break unless lot
    b2b.lot_locate(lot, lc.op) unless params[:nolocate]
  }
  return $lots[tc].compact.size == n
end

# create lots for AsmView tests
# tc is something like "tc23", n the number of lots to create
# return true on success
def new_asmlot_tc(tc, n=1, params={})
  $log.info "new_lot_tc #{tc.inspect}, #{n}"
  env, lc = $tc_data[tc]
  ($log.warn "no such test case"; return nil) unless lc
  sv = (env == "F1") ? $f1sv : $f8sv
  b2b = (env == "F1") ? $f1b2b : $f8b2b
  $lots ||= {}
  $lots[tc] ||= []
  n.times {|i|
    $log.info "\n creating lot #{i+1}/#{n}"
    params[:carrier] ||= "Q%"
    params.merge!(:carrier_category=>nil)
    lot = b2b.new_lot(lc, params)
    sv.user_parameter_set_verify("Lot", lot, "SourceFabCode", env)
    $lots[tc] << lot
    break unless lot
    # Fab setup
    b2b.lot_locate(lot, lc.op) unless params[:nolocate]
    if env == "F8" and not params[:nobackup]
      # send to backup, receive and set script parameters      
      if params[:noreceive]
        $f8sv.prepare_for_backup(lot, :backup_bank=>"O-BACKUPOPER", :bak_carrier=>"Q%")
        $f8sv.backupop_lot_send(lot, $f1sv, :backup_bank=>"O-BACKUPOPER", :bak_carrier=>"Q%")
      else
        $f8sv.backup_prepare_send_receive(lot, $f1sv, :backup_bank=>"O-BACKUPOPER", :bak_carrier=>"Q%")
      end      
      $f8sv.user_parameter("Lot", lot).each {|p| $f1sv.user_parameter_set_verify "Lot", lot, p.name, p.value}
    end
    next if params[:noasmview]
    # AsmView setup
    res = $avstb.stb(lot)
    break unless res
    $avstb.lot_cleanup(lot, :op=>:lotshipdesig)
    $av.asm_diecount_req(lot, $bins)
    # SiView LotShipDesignation
    b2b = (env == "F1") ? $f8b2b : $f1b2b
    b2b.lot_locate(lot, :lotshipdesig) unless params[:nolocate]
  }
  return $lots[tc].compact.size == n
end

