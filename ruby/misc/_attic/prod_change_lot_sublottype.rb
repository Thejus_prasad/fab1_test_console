=begin
Change lot priority class

This script is meant to be edited before it runs from the JRuby test console.
Change $env, $fname and $username before running it.

$username must be a SiView user with sufficient privileges. Interactive login 
(no password shown) is required to run  from the console.

Author: ssteidte, 2012-06-07
Version: 1.0.0

Usage in Production:
1) change $env ("production"), $username, $ntauth (true)
2) edit $sublottypes
3) load 'ruby/misc/prod_change_lot_sublottype.rb'
4) run_loops [n] # converts lots for all defined sublottypes, looping n times (default: 1 billion)

=end

require 'siview'

$env = "itdc"               #"production"
$username = "abecker"       #"X-UNITTEST"  #nil or "abecker"
$ntauth = true              #true for personal accounts,  nil else

$memo = "SubLotType change to remove obsolete Sublottypes from SiView. (PQUL-, PROD-; TQUL, MW, PZ)"

$sublottypes = {
  "MW"=>"EO",
  "PQUL-MW"=>"EO",
  "PQUL-QD"=>"QD",
  "PQUL-QE"=>"QE",
  "PROD-PO"=>"PO",
  "PROD-PR"=>"PR",
  "PROD-PX"=>"PX",
  "TQUL"=>"EQ",
  "PZ"=>"PX",
}
#
# ------------- no changes below this line required ------------------------
#

$sv = SiView::MM.new($env) #, :ntauth=>$ntauth, :user=>$username)
$lots_old ||= {}
$lots_converted ||= {}
$lots_notconverted ||= {}

def get_lots
  # return array of lot names per old sublottype
  $sublottypes.keys.each {|c|
    $lots_old[c] = []
    $lots_old[c].concat $sv.lot_list(:sublottype=>c)
    $lots_old[c].uniq!
    $lots_old[c].sort!
  }
  nil
end

def convert_sublottype(sublottypes, params={})
  # change sublottype, all sublottypes defined in $sublottypes  if sublottypes is nil
  res = $sv.logon_check
  ($log.warn "no valid username/password"; return nil) unless res and res == 0
  #
  sublottypes = sublottypes.split if sublottypes.kind_of?(String)
  sublottypes ||= $lots_old.keys.sort
  ret = true
  sublottypes.each {|c|
    lots = $lots_old[c].clone
    $log.info "converting sublottype #{c}: #{lots.size} lots"
    newc = $sublottypes[c]
    ($log.warn "no new sublottypes defined"; ret = false; next) unless newc
    $lots_converted[c] ||= []
    $lots_notconverted[c] ||= []
    lots.each_with_index {|lot, i|
#      $log.info "#{i+1}/#{lots.size}"
      res = $sv.lot_mfgorder_change(lot, :sublottype=>newc, :memo=>$memo)
      break unless res
      if res[0] == 0
        $lots_converted[c] << lot
        $lots_notconverted[c].delete(lot)
        $lots_old[c].delete(lot)
      else
        $lots_notconverted[c] << lot
        ret = false
      end
    }
    $lots_converted[c].uniq!
    $lots_notconverted[c].uniq!
  }
  return ret
end

def run_loops(nloops=1000000000)
  nloops.times {|loop|
    $log.info "\n\n loop #{loop}"
    get_lots
    convert_sublottype(nil)
    nb = stats
    break if nb == 0
    sleep 120
  }
end

def clean
  $lots_old = {}
  $lots_converted = {}
  $lots_notconverted = {}
end

def stats
  $log.info "\n\n"
#  $log.info "lots_converted: #{$lots_converted.inspect}"
  $log.info "lots_notconverted: #{$lots_notconverted.inspect}"
  nc = nb = 0
  $lots_converted.each_pair {|c, lots| nc += lots.size if lots}
  $lots_notconverted.each_pair {|c, lots| nb += lots.size if lots}
  $log.info "summary: converted lots: #{nc}, blocked_lots: #{nb}"
  return nb
end
