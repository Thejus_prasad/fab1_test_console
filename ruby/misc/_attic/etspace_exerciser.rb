=begin
ETest SPACE Exerciser. Simulating EI ETest Space package send.

(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author:   Conny Stenzel, 2013-04-24
Version:  1.0.0

Example:
./bin/etspace_exerciser.sh -e 'itdc' -d 1800 -i 1 -s 1
./bin/etspace_exerciser.sh -e 'itdc' -d 60 -i 1 -s 1

=end

load './ruby/set_paths.rb'
require 'optparse'
require 'etspaceload'

def main(argv)
  # set defaults
  env = "itdc"
  logfile = "log/etspace_exerciser.log"
  perflog = "log/etspace_perf_#{Time.now.utc.iso8601.gsub(':', '-')}.csv"
  speed = 1
  iterations = 0
  duration = nil
  timeout = 1000
	packages = 1
	wafer = 25
	parameters = 1
	violations = 0
  verbose = false
  noconsole = true
	getpayload = false
	appname = "etspace"
  # parse cmdline options
  optparser = OptionParser.new {|o|
    o.banner = "Usage: #{File.basename(__FILE__)} [options] payloadfile"
    o.on('-e', '--env [ENV]', 'environment (e.g. "let", "itdc")') {|s| env = s}
    o.on('-l', '--perflog [FILE]', 'performance log file') {|f| perflog = f}
    o.on('-d', '--duration [DURATION]', Integer, 'max test duration in seconds, default is as long as data exist') {|n| duration = n}
    o.on('-i', '--iterations [ITERATION]', Integer, "number of iterations, default is #{iterations}") {|n| iterations = n}
    o.on('-s', '--speed [SPEED]', Float, '0: as fast as possible, 0.5: half speed, 1: standard, 2: double speed, ..') {|n| speed = n}
    o.on('-t', '--timeout [TIMEOUT]', Integer, "request timeout, default #{timeout}s") {|n| timeout = n}
		
		o.on('-p', '--packages [PACKAGES]', Integer, "amount of packages, 1...unlimited default #{packages}") {|n| packages = n}
		o.on('-w', '--wafer [WAFER]', Integer, "amount of wafers/package, 1...25 default #{wafer}") {|n| wafer = n}
		o.on('-a', '--parameters [PARAMETERS]', Integer, "amount of parameters/package, 1...unlimited default #{parameters}") {|n| parameters = n}
		o.on('-o', '--violations [VIOLATIONS]', Integer, "amount of violations, 0...100 (%) default #{violations}") {|n| violations = n}
    o.on('-r', '--recipefine', 'recipe fine parameters on') {recipefine = true}
		o.on('-y', '--nctype', 'nctype (e.g. "mnc", "nc")') {|s| nctype = s.to_sym}
		
    o.on('-v', '--verbose', 'verbose logging') {verbose = true}
    o.on('--console', 'verbose logging') {noconsole = false}
    o.on_tail('-h', '--help', 'diplay this screen') {puts o; exit}
  }
  optparser.parse(*argv)
  # create logger
  create_logger(:file=>logfile, :file_verbose=>verbose, :verbose=>verbose, :noconsole=>noconsole)
	#create_logger(:file=>"log/etspace_exerciser.log", :file_verbose=>false, :verbose=>false, :noconsole=>true)
  STDOUT.puts "logging to #{logfile}, console log: #{!noconsole}"
  # get parameters from options
  params = {:perflog=>perflog, :timeout=>timeout, :speed=>speed, :iterations=>iterations, :duration=>duration, :packages=>packages, :wafer=>wafer, :parameters=>parameters, :violations=>violations, :recipefine=>recipefine, :nctype=>nctype}
	#params = {:perflog=>"log/etspace_perf_#{Time.now.utc.iso8601.gsub(':', '-')}.csv", :timeout=>1000, :speed=>1, :iterations=>0, :duration=>60, :packages=>1, :wafer=>25, :parameters=>1, :violations=>0, :recipefine=>true, :nctype=>:mnc}
	# run the test
	$log.info "ETSpace::Exerciser #{env.inspect}, #{params.inspect}"
	$etspace = ETSpace::Exerciser.new(env, params)
	res = $etspace.load_test(params)
	$log.info "\ncompleted: #{res.inspect}"
end

main(ARGV) if __FILE__ == $0
