=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: Steffen Frieske, 2016-09-02

Version: 0.0.1

History:
=end

require 'spaceapi'


class Space::Screenshot
  attr_accessor :session
  def initialize(env, params={})
    @session = ::Space::SpcApi::Session.new(env, params)
  end
end


def test
  $sshot = Space::Screenshot.new('let')
  $sess = $sshot.session
  $chid = 1538572  #855437
  $ch = $sess.spc_channel($chid)
  $ch.capture_image('xxx0.png', tend: Time.parse("2016-11-26"))
  $ckc = $ch.ckcs[0]
  $ckc.capture_image('xxx1.png', tend: Time.parse("2016-11-26"))
end
