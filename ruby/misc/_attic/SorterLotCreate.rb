=begin 
create lots for Sorter EI Tests

(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.
    
Author: Thomas Klose 2010-10-05
=end

## preparation: delete old lots

## SAT5
waferids=["WSR-SAT03-21", "WSR-SAT03-22", "WSR-SAT03-23", "SAT03-0024D2", "SAT03-0025C5"]
nwafers=waferids.length
lotid="U8003.00"
route="ITDC-0003.01"
operno="CEI03.1000"
carrier="SAT5"
$sv.new_lot_release( :lot=>lotid, :waferids=>waferids, :nwafers=>nwafers, :product=>route, :route=>route, :carrier=>carrier, :stb=>true)
$sv.lot_opelocate(lotid, operno)


## SAT6
waferids=["SAT04-0001A6", "SAT04-0002A1", "SAT04-0003G7", "SAT04-0004G2", "SAT04-0005F5", "SAT04-0006F0", "SAT04-0007E3", "SAT04-0008D6", "SAT04-0009D1", "SAT04-0010D6", "SAT04-0011D1", "SAT04-0012C4", "SAT04-0013B7", "SAT04-0014B2", "SAT04-0015A5", "SAT04-0016A0", "SAT04-0017G6", "SAT04-0018G1", "SAT04-0019F4", "SAT04-0020G1", "SAT04-0021F4", "SAT04-0022E7", "SAT04-0023E2", "SAT04-0024D5", "SAT04-0025D0"]
nwafers=waferids.length
lotid="U8004.00"
route="ITDC-0003.01"
operno="CEI04.1000"
carrier="SAT6"
$sv.new_lot_release( :lot=>lotid, :waferids=>waferids, :nwafers=>nwafers, :product=>route, :route=>route, :carrier=>carrier, :stb=>true)
$sv.lot_opelocate(lotid, operno)


## SATC
waferids=["WSR-SAT04-11", "WSR-SAT04-12", "WSR-SAT04-13", "WSR-SAT04-14", "WSR-SAT04-15", "WSR-SAT04-16", "WSR-SAT04-17", "WSR-SAT04-18", "SAT04-0026C3", "SAT04-0027B6"]
nwafers=waferids.length
lotid="U7004.00"
route="ITDC-0003.01"
operno="CEI04.1000"
carrier="SATC"
$sv.new_lot_release( :lot=>lotid, :waferids=>waferids, :nwafers=>nwafers, :product=>route, :route=>route, :carrier=>carrier, :stb=>true)
$sv.lot_opelocate(lotid, operno)


## SATE
waferids=["SAT04-0028B1","SAT04-0029A4"]
nwafers=waferids.length
lotid="U6004.00"
route="ITDC-0003.01"
carrier="SATE"
$sv.new_lot_release( :lot=>lotid, :waferids=>waferids, :nwafers=>nwafers, :product=>route, :route=>route, :carrier=>carrier, :stb=>true)

## SATF
waferids=["SAT06-0001B4", "SAT06-0002A7", "SAT06-0003A2", "SAT06-0004H0", "SAT06-0005G3", "SAT06-0006F6", "SAT06-0007F1", "SAT06-0008E4", "SAT06-0009D7", "SAT06-0010E4", "SAT06-0011D7", "SAT06-0012D2", "SAT06-0013C5", "SAT06-0014C0", "SAT06-0015B3"]
nwafers=waferids.length
lotid="U8006.00"
route="ITDC-0003.01"
operno="CEI06.1000"
carrier="SATF"
$sv.new_lot_release( :lot=>lotid, :waferids=>waferids, :nwafers=>nwafers, :product=>route, :route=>route, :carrier=>carrier, :stb=>true)
$sv.lot_opelocate(lotid, operno)

## SATJ
waferids=["SAT07-0001B7", "SAT07-0002B2", "SAT07-0003A5", "SAT07-0004A0", "SAT07-0005G6", "SAT07-0006G1", "SAT07-0007F4", "SAT07-0008E7", "SAT07-0009E2", "SAT07-0010E7", "SAT07-0011E2", "SAT07-0012D5", "SAT07-0013D0", "SAT07-0014C3", "SAT07-0015B6", "SAT07-0016B1", "SAT07-0017A4", "SAT07-0018H2", "SAT07-0019G5", "SAT07-0020H2"]
nwafers=waferids.length
lotid="U8007.00"
route="ITDC-0003.01"
operno="CEI07.1000"
carrier="SATJ"
$sv.new_lot_release( :lot=>lotid, :waferids=>waferids, :nwafers=>nwafers, :product=>route, :route=>route, :carrier=>carrier, :stb=>true)
$sv.lot_opelocate(lotid, operno)

## SATM
waferids=["WSR-SAT09-01", "WSR-SAT09-02", "WSR-SAT09-03", "WSR-SAT09-04", "WSR-SAT09-05"]
nwafers=waferids.length
lotid="U8009.00"
route="ITDC-0003.01"
operno="CEI07.1000"
carrier="SATM"
$sv.new_lot_release( :lot=>lotid, :waferids=>waferids, :nwafers=>nwafers, :product=>route, :route=>route, :carrier=>carrier, :stb=>true)
$sv.lot_opelocate(lotid, operno)

## SATN
waferids=["SAT09-0007G2", "SAT09-0006G7", "SAT09-0008F5", "SAT09-0009F0", "SAT09-0010F5", "SAT09-0011F0", "SAT09-0012E3", "SAT09-0013D6", "SAT09-0014D1", "SAT09-0015C4", "SAT09-0016B7", "SAT09-0017B2", "SAT09-0018A5", "SAT09-0019A0", "SAT09-0020A5"]
nwafers=waferids.length
lotid="U7009.00"
route="ITDC-0003.01"
operno="CEI09.1000"
carrier="SATN"
$sv.new_lot_release( :lot=>lotid, :waferids=>waferids, :nwafers=>nwafers, :product=>route, :route=>route, :carrier=>carrier, :stb=>true)
$sv.lot_opelocate(lotid, operno)

## SATO
waferids=["SAT09-0021A0", "SAT09-0022G6", "SAT09-0023G1", "SAT09-0024F4", "SAT09-0025E7"]
nwafers=waferids.length
lotid="U5009.00"
route="ITDC-0003.01"
operno="CEI09.1000"
carrier="SATO"
$sv.new_lot_release( :lot=>lotid, :waferids=>waferids, :nwafers=>nwafers, :product=>route, :route=>route, :carrier=>carrier, :stb=>true)
$sv.lot_opelocate(lotid, operno)

## SATQ
waferids=["SAT10-GR01G7", "SAT10-GR02G2", "SAT10-GR03F5", "SAT10-GR04F0", "SAT10-GR05E3", "SAT10-GR06D6", "SAT10-GR07D1", "SAT10-GR08C4", "SAT10-GR09B7", "SAT10-GR10C4", "SAT10-GR11B7", "SAT10-GR12B2", "SAT10-GR13A5", "SAT10-GR14A0", "SAT10-GR15G6"]
nwafers=waferids.length
lotid="U8010.00"
route="ITDC-0003.01"
operno="CEI10.1000"
carrier="SATQ"
$sv.new_lot_release( :lot=>lotid, :waferids=>waferids, :nwafers=>nwafers, :product=>route, :route=>route, :carrier=>carrier, :stb=>true)
$sv.lot_opelocate(lotid, operno)



