=begin
(c) Copyright 2011 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.
=end
    
# Sample script that can be used for FabView history testing 


########### ITDC ############
source_prd = 'OTHER.X'
source_bnk = 'ON-RAW'
product = '3260CH.J0'
route = 'C02-1083.01'
layer = 'B'
carrier = 'A56N'
eqp1 = ''
ship_bnk = 'OY-SHIP'

######### F8STAGING #########
source_prd = 'O-RECEIVED-FROM-OTHER.FABS'
source_bnk = 'OF-START'
product = 'OBAN-TEST.01'
route = 'OBANSK-TEST.01'
layer = 'PROD'
eqp1 = 'DFU2100'
eqp2 = 'THK2100'
ship_bnk = 'O-SHIP'
other_bnk = 'O-SHIP-NS'

console 'itdc'
vlot1 = $itdc.vendorlot_receive source_bnk, source_prd, 25, :memo=>'Vendor lot receive memo'
vlot2 = $itdc.vendorlot_prepare vlot1, carrier, :memo=>"Vendor lot prepare memo"
#TODO: vendorlot cancel

$itdc.new_lot_release :product=>product, :route=>route, :layer=>layer, :stb=>true, :comment=>"lot comment for FabView"
$itdc.lot_hold_release lot, nil, :memo=> "Lot is released"

$itdc.lot_gatepass lot, :memo=> "Lot has skipped operation"
$itdc.claim_process_lot lot, :eqp=>eqp1, :mode=>'Auto-1'
$itdc.eqp_status_rpt
$itdc.eqp_status_change_rpt
$itdc.lot_remeasure lot
$itdc.claim_process_lot lot, :eqp=>eqp1, :mode=>'Auto-1'

$itdc.lot_info lot, :wafers=>true
clot = $itdc.lot_split lot, 5, :memo=> "Split for FabView"
$itdc.lot_merge lot, clot, :memo=> "Merge the lot"

$itdc.lot_opelocate lot, "1000.4900", :memo=>"locate forward"
$itdc.lot_opelocate lot, "1000.1400", :memo=>"locate backward"

$itdc.eqp_note_register eqp1, "Test note", "This is a note for FabView."
$itdc.eqp_alarm eqp1, :memo=>"Test alarm! Alarm! FabView!"

$itdc.lot_opelocate lot, "1000.4900"
$itdc.lot_rework
$itdc.lot_rework lot, "REWORK-TEST.01", :memo=>"Rework test for FabView"
$itdc.lot_rework lot, "REWORK-TEST.01", :memo=>"Rework test for FabView", :return_opNo=>'1000.4000'
$itdc.lot_rework_cancel lot, :memo=>"Rework test for FabView"
$itdc.lot_opelocate lot, "1000.4900"
$itdc.lot_partial_rework 
clot = $itdc.lot_partial_rework lot, 3, "REWORK-TEST.01", :memo=>"Partial Rework test for FabView", :return_opNo=>'1000.4000'
$itdc.lot_partial_rework_cancel lot, clot, :memo=>"Partial rework cancel"

$itdc.scrap_wafers lot, :memo=>"FabView scrap wafer test", :wafers=>["04","07","10"]
$itdc.scrap_wafers_cancel lot, :memo=>"FabView test scrap wafer cancel",:wafers=>["04","07","10"]
$itdc.lot_openote_register lot, "Test note", "My very FabView 3 test"
$itdc.lot_note_register
$itdc.lot_note_register lot, "Test lot note", "My very FabView 3 test"
$itdc.carrier_xfer_status_change carrier, "STK02E001"
$itdc.carrier_xfer_status_change carrier, "STK02E001", "SI"
$itdc.carrier_xfer_status_change carrier, "SI", "STK02E001"
$itdc.carrier_xfer_status_change carrier, "SO", "STK02E001"
$itdc.carrier_xfer_status_change carrier, "MO", "STK02E001"

$itdc.lot_requeue  lot, :memo=>"Test requeue"
$itdc.lot_nonprobank lot,"D-ARMOR"

$itdc.lot_nonprobankin lot,"D-AMOR",:memo=>"Test for nonpro bankin"
$itdc.lot_nonprobankout lot,:memo=>"Test for nonpro bankout"

$itdc.lot_futurehold lot, "1000.5400", "AUT", :memo=>"Future Hold registered"
$itdc.lot_futurehold_cancel lot, "1000.5400", "AUT", :memo=>"Future Hold canceled"
$itdc.lot_futurehold_cancel lot, "AUT", :memo=>"Future Hold canceled"


$itdc.lot_externalpriority_change lot, "7777", :memo=>"Priority change"

cj = $itdc.claim_process_lot lot, :noopecomp=>true, :nounload=>true, :notify=>false,:mode=>"Off-Line-1"

$itdc.running_hold lot, eqp2, cj, :memo=>"Runnning hold"
$itdc.eqp_tempdata eqp2, "PG1", cj
$itdc.eqp_opecomp eqp2, cj, :force=>true, :memo=>"Force complete"
$itdc.eqp_unload eqp2, "P1", carrier
$itdc.lot_hold_release lot, "FCHL"

$itdc.lot_opelocate lot,"6999.9999"
$itdc.lot_bankin lot, :memo=> "Bankin"

clot = $itdc.lot_split lot, 4, :memo=>"Split on bank"
$itdc.lot_merge lot, clot, :memo=>"Merge on bank"

$itdc.scrap_wafers lot, :memo=>"FabView scrap wafer test in bank", :splitwafers=>5
$itdc.scrap_wafers_cancel lot, :memo=>"FabView test scrap wafer cancel in bank", :wafers=>["01","02","03","04","05"]

$itdc.user_parameter_set_or_delete "Lot", lot,"OOC", "Error for FabView"
$itdc.user_parameter_set_or_delete "Lot", lot,"OOC"


$itdc.user_parameter_set_or_delete "Wafer", "132126774704","DieCountGood", "23"
$itdc.user_parameter_set_or_delete "Wafer", "132126774704","DieCountGood"

$itdc.lot_ship lot, ship_bnk, :memo=>"Ship the lot"
$itdc.lot_ship_cancel lot, ship_bnk,:memo=>"Ship cancel  the lot"

$itdc.lot_bank_move lot,  other_bnk
$itdc.lot_bank_move lot, ship_bnk
$itdc.lot_bankin_cancel lot

$itdc.inhibit_entity "Equipment", eqp2, :memo=>"Register equipment inhibit"
$itdc.inhibit_cancel "Equipment", eqp2, :memo=>"Cancel inhibit"



