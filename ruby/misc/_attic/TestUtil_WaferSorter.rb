=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: Beate Kochinka, 2011-07

$Rev: 16952 $
$LastChangedDate: 2014-08-21 12:59:31 +0200 (Do, 21 Aug 2014) $
$LastChangedBy: ssteidte $
=end

require 'siview'

# generate scribe map file with parameters:
# - (list of) carriers (passed as array or space separated string)
# - output file - default: c:\temp\scribes.txt
# - SiView environment - default: "itdc"
def gen_scribe_file(carriers=[], out_file='c:\temp\scribes.txt', env="itdc")
  carriers = carriers.split if carriers.kind_of?(String)
  out = open(out_file, "w")
  $log.info "...writing output to #{out_file}"
  out.puts "// scribe map file - generated #{Time.now.strftime('%Y-%m-%d %H:%M')}"
  carriers.each{|carrier|
    $sv = SiView::MM.new(env)
    slotMap = {}
    map = ""
    cassInfo = $sv.carrier_status(carrier)
    if cassInfo != nil
      out.puts "\n// --- carrier #{carrier} ---"
      $sv.lot_info(cassInfo.lots, :wafers=>true).each{|x|
        x.wafers.each{|w|
          slotMap.store(w.slot, w.wafer)
        }
      }
      25.times{|i|
        slot = i+1
        wafer = slotMap[slot]
        if wafer != nil # wafer in slot
          out.puts "#{carrier}.#{"%02d" % slot}, #{wafer}"
          map += "3"
        else # empty slot
          out.puts "// slot #{"%02d" % slot}"
          map += "1"
        end
      }
      $log.info "\ncarrier map for #{carrier}:\n#{map}"
    else
      $log.error "unknown carrier: #{carrier}"
    end
  }
  out.flush
  out.close
end
