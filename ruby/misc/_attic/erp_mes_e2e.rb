=begin
Support for ERP-MES E2E Tests. Create lots suitable for shipment via FabGUI.

test cases: http://myteamsgf/sites/ERPTeam/MES/Shared%20Documents/TO-BE%20Documents%20200mm_300mm/Testing/E2E2%20testing/MES%20Tests.xlsx

Note on ERP stages and TurnKey parameters
stage UWINV: "U", "", ""
      PWINV: "J", "GG", "GG"

usage:
  load 'ruby/misc/erp_mes_e2e.rb'
  new_lot_tc "tc13", 3  # create 3 lots with data from $tc_data['tc13']
=end

require 'yaml'
require 'erpmes'

$env = "f8stag" #"itdc"
$sv = SiView::MM.new($env)
$erpmes = ERPMES.new($env, :sv=>$sv)

$sublottypes = ["EB", "EC", "EM", "EO", "EQ", "ES", "ET", "MW", "PB", "PC", "PO", "QD", "QE", "QF", "QX", "RD", "RQ", "TQUL", "PR", "PX", "PZ", "SCRAP"]

$_tk_u = SiView::MM::LotTkInfo.new("U")
$_tk_j_gggg = SiView::MM::LotTkInfo.new("J", "GG", "GG")
$_tk_j_gg1t = SiView::MM::LotTkInfo.new("J", "GG", "1T")

$tc_data = {
  "tc12"=>ERPMES::ErpAsmLot.new(nil, $_tk_j_gggg, "HLCYV2-AA0", nil, "TKEY01.A0", "C02-TKEY.01", "ibm", "QE", :lotshipdesig),
  "tc13"=>ERPMES::ErpAsmLot.new(nil, $_tk_j_gg1t, "MPW0413SAMPLE-A02", nil, "TKEY01.A0", "C02-TKEY.01", "imc", "QE", :lotshipdesig),
  "tc13"=>ERPMES::ErpAsmLot.new(nil, $_tk_j_gggg, "*SHELBY21AE-M00-JB4", "6010000548:01:01", "SHELBY21AE.B4", "C02-1431.01", "qgt", "PB", :lotshipdesig),
  "tc80"=>ERPMES::ErpAsmLot.new(nil, $_tk_u, "OBANSK-U02", nil, "OBANSK.02", "FF-OBANSK.01", "ibm", "QE", :lotshipdesig),
  "tc81"=>ERPMES::ErpAsmLot.new(nil, $_tk_u, "OBANSK-U02", nil, "OBANSK.02", "FF-OBANSK.01", "ibm", "QE", 'FOUT-GATE.01'),
  "tc82"=>ERPMES::ErpAsmLot.new(nil, $_tk_u, "OBANSK-U02", nil, "OBANSK.02", "FF-OBANSK.01", "ibm", "PO", 'FOUT-GATE.01'),
#  "tc83"=>$_erpmeslot.new(nil, "ibmca", "QE", nil, nil, nil, :lotshipdesig, "FF-OBANSK.01", "OBANSK.01", "U", "", "", "OBANSK-U01"),
}

$tc_lot_number = {
  "tc12"=>1,
  "tc13"=>1,
  "tc80"=>1,
}

if $env == "f8stag"
  $source_product = "ALL-8547.01"
  $source_bank = "OF-START"
else
  $source_product = "31006550.SOITEC"
  $source_bank = "ON-RAW"
end

def init_parameters
  $carrier = "Q0001"  # first carrier, count up automatically
  $lots = {}
end


def new_vendor_lot
  $sv.vendorlot_receive($source_bank, $source_product, 100000)
end

def create_carriers(c0, n)
  n.times {
    res = $sv.durable_register(c0, "BEOL")
    return false unless res
    res = $sv.carrier_status_change(c0, "AVAILABLE")
    return false unless res == 0
    c0 = c0.siview_next
  }
  true
end

def release_lots
  $lots.each_pair {|p, ll| ll.each {|l| $sv.lot_hold_release(l)} }
end

def show_lots(params={})
  $lots.each_pair {|p, ll| puts "#{p}:\n  #{ll.join(" ")}"}
  fname = params[:file]
  open(fname, "wb") {|fh| fh.write($lots.to_yaml)} if fname
  nil
end


def all_new_lots
  $lots = {}
  $tc_lot_number.each_pair {|tc, n| new_lot_tc(tc, n)}
end


def new_lot_slts(tc="tc16", params={})
  $lots ||= {}
  $lots["slt"] ||= []
  lc = $tc_data[tc]
  params[:carrier] ||= "Q%"
  $sublottypes.each {|slt|
    $log.info "\n creating lot type #{slt}"
    lc.sublottype = slt
    lot = $erpmes.new_lot(lc, params.merge(:carrier_category=>""))
    $lots["slt"] << lot
    break unless lot
    $erpmes.lot_locate(lot, lc.op) unless params[:nolocate]
  }
end

def new_lot_tc(tc, n=1, params={})
  # tc is something like "tc23", n the number of lots to create
  # return true on success
  $log.info "new_lot_tc #{tc.inspect}, #{n}"
  lc = $tc_data[tc]
  ($log.warn "no such test case"; return nil) unless lc
  $lots ||= {}
  $lots[tc] ||= []
  n.times {|i|
    $log.info "\n creating lot #{i+1}/#{n}"
    params[:carrier] ||= "Q%"
    params.merge!(:carrier_category=>nil) # do not get default carrier category FEOL
    lot = $erpmes.new_lot(lc, params)
    $lots[tc] << lot
    break unless lot
    $erpmes.lot_locate(lot, lc.op) unless params[:nolocate]
  }
  return $lots[tc].compact.size == n
end

