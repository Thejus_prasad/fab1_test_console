=begin
Change lot priority class

This script is meant to be edited before it runs from the JRuby test console.
Change $env, $fname and $username before running it.

$username must be a SiView user with sufficient privileges. Interactive login 
(no password shown) is required to run "change_lot_prio" from the console.

Author: ssteidte, 2011-06-06
Version: 1.0.1

History:
  2011-06-06  ssteidte, created, verified in ITDC and used in Production
  2011-06-09  ssteidte, modified for easier use, checked in to SVN
  
  
Usage:
preparation:
1) Create a file containing the lots, one per line, no headers
2) Edit $env, $fname, $username and $ntauth (must be true for real persons and false for X-UNITTEST)
3) start a test console, e.g. bin\console_R10_custom.bat (from junit_testing)

on the console:
4) load this file: 
    load 'ruby/misc/change_lot_prio.rb'
5) read lots:
    get_lots
6) verify the lots have been read correctly:
    $lots.size (compare with the number of lots in the file ($fname)
7) change lot priority:
    increase_lot_prio($lots) or revert_lot_prio($lots)
    (interactive login required, progress is shown 
     The lots that have been changed, have not been changed because of lower prio
     and those that could not be changed because of error conditions are returned.)
8) verify result, e.g.:
    pp $ret
9) optional: re-run with error lots:
    $ret_orig = $ret.clone
    increase_lot_prio($ret[:errors]) or revert_lot_prio($ret[:errors])
    pp $ret
  
=end

require 'siview'

$env = "itdc" #"production"
$fname = 'C:\Temp\2011-06-07-lotprio.csv'
$username = nil  #"abecker"
$ntauth = nil

#
# ------------- no changes below this line required ------------------------
#

$sv = SiView::MM.new($env, :ntauth=>$ntauth, :user=>$username)
#$prios = {'STD'=>4, 'HOT'=>2}
$lots = []

def get_lots
  # return array of lot names
  $lots = []
  open($fname) {|fh|
    fh.readlines.each_with_index {|line, i|
      # skip header?
      #next if i == 0
      lot, dummy = line.split(';', 2)
      $lots << lot
    }
  }
  nil
end

def increase_lot_prio(lots, params={})
  # increase lot prio (change if current prio is lower than newpriority)
  # note: increasing prio means decreasing the value of priority class
  res = $sv.logon_check
  ($log.warn "no valid username/password"; return nil) unless res and res == 0
  #
  newprio = (params[:newpriority] or 2)
  force = params[:force]
  changed = []
  unchanged = []
  errors = []
  lots.each_with_index {|lot, i|
    $log.info "#{i+1}/#{lots.size}"
    #
    li = $sv.lot_info(lot)
    $log.warn "SiView error? Aborting" unless li
    # skip lots with higher prio
    curprio = li.priority.to_i
    if curprio <= newprio and !force
      unchanged << lot
      next
    end
    #
    res = $sv.schdl_change(lot, :priority=>newprio)
    if res == 0
      li = $sv.lot_info(lot)
      if li.priority.to_i != newprio
        $log.warn "priority change failed for lot #{lot} (prio #{li.priority})"
        errors << lot
      else
        changed << lot
      end
    else
      errors << lot
    end
    #break if i >= 3
  }
  $ret = {:changed=>changed, :unchanged=>unchanged, :errors=>errors}
  return $ret
end
  
def revert_lot_prio(lots, params={})
  # decrese lot prio (change to new lower prio if current prio is not higher than old prio)
  #   and not higher than oldpriority)
  # note: decreasing prio means increasing the value of priority class
  res = $sv.logon_check
  ($log.warn "no valid username/password"; return nil) unless res and res == 0
  #
  newprio = (params[:newpriority] or 4)
  oldprio = (params[:oldpriority] or 2)
  ($log.warn "newpriority must be higher than oldpriority"; return nil) if newprio <= oldprio
  force = params[:force]
  changed = []
  unchanged = []
  errors = []
  lots.each_with_index {|lot, i|
    $log.info "#{i+1}/#{lots.size}"
    #
    li = $sv.lot_info(lot)
    $log.warn "SiView error? Aborting" unless li
    # skip lots with higher prio
    curprio = li.priority.to_i
    if curprio < oldprio and !force
      unchanged << lot
      next
    end
    #
    res = $sv.schdl_change(lot, :priority=>newprio)
    if res == 0
      li = $sv.lot_info(lot)
      if li.priority.to_i != newprio
        $log.warn "priority change failed for lot #{lot} (prio #{li.priority})"
        errors << lot
      else
        changed << lot
      end
    else
      errors << lot
    end
    #break if i >= 3
  }
  $ret = {:changed=>changed, :unchanged=>unchanged, :errors=>errors}
  return $ret
end
  
