=begin
Change lot priority class

This script is meant to be edited before it runs from the JRuby test console.
Change $env, $fname and $username before running it.

$username must be a SiView user with sufficient privileges. Interactive login 
(no password shown) is required to run "change_lot_prio" from the console.

Author: ssteidte, 2012-05-04
Version: 1.0.0

Usage in Production:
1) change $env ("production"), $username, $ntauth (true)
2) edit $customers
3) load 'ruby/misc/prod_change_lot_customer.rb'
4) run_loops [n] # converts lots for all defined customers, looping n times (default: 1 billion)

=end

require 'siview'

$env = "production"
$username = nil #"X-UNITTEST"            #"X-UNITTEST"  #nil or "abecker"
$ntauth = true             #true for personal accounts,  nil else

$customers = {
  "5000"=>"amd",
  "5010"=>"amd",
  "5015"=>"amd",
  "300001"=>"stmicro",
  "300005"=>"ibm",
  "300010"=>"qgt",
  "300011"=>"broadcom",
  "300012"=>"infinap",
  "300013"=>"infin",
  "300014"=>"infinatagvi",
  "300020"=>"renesas",
  "300021"=>"samsung",
  "300025"=>"ibm",
  "300026"=>"qgt",
  "300040"=>"imc",
  "300045"=>"gf",
  "300050"=>"st-ericsson",
  "300060"=>"samsung",
  "600015"=>"gf",
  "600075"=>"gshuttle",
  "GF Fab 8"=>"gf8",
#  "OPS"=>"gf",
#  "INT"=>"gf",
#  "VRT"=>"gf",
#  "QUALCOMM"=>"qgt",
}
#
# ------------- no changes below this line required ------------------------
#

$sv = SiView::MM.new($env, :ntauth=>$ntauth, :user=>$username)
$lots_old ||= {}
$lots_converted ||= {}
$lots_notconverted ||= {}

def get_lots
  # return array of lot names per old customer
  $customers.keys.each {|c|
    $lots_old[c] = []
    $lots_old[c].concat $sv.lot_list(:customer=>c)
    $lots_old[c].uniq!
    $lots_old[c].sort!
  }
  nil
end

def convert_customer(customers, params={})
  # change customer code, all customers defined in $customers  if customers is nil
  res = $sv.logon_check
  ($log.warn "no valid username/password"; return nil) unless res and res == 0
  #
  customers = customers.split if customers.kind_of?(String)
  customers ||= $lots_old.keys.sort
  ret = true
  customers.each {|c|
    lots = $lots_old[c].clone
    $log.info "converting customer #{c}: #{lots.size} lots"
    newc = $customers[c]
    ($log.warn "no new customer defined"; ret = false; next) unless newc
    $lots_converted[c] ||= []
    $lots_notconverted[c] ||= []
    lots.each_with_index {|lot, i|
#      $log.info "#{i+1}/#{lots.size}"
      res = $sv.lot_mfgorder_change(lot, :customer=>newc)
      break unless res
      if res[0] == 0
        $lots_converted[c] << lot
        $lots_notconverted[c].delete(lot)
        $lots_old[c].delete(lot)
      else
        $lots_notconverted[c] << lot
        ret = false
      end
    }
    $lots_converted[c].uniq!
    $lots_notconverted[c].uniq!
  }
  return ret
end

def run_loops(nloops=1000000000)
  nloops.times {|loop|
    $log.info "\n\n loop #{loop}"
    get_lots
    convert_customer(nil)
    stats
    sleep 120
  }
end

def clean
  $lots_old = {}
  $lots_converted = {}
  $lots_notconverted = {}
end

def stats
  $log.info "\n\n"
#  $log.info "lots_converted: #{$lots_converted.inspect}"
  $log.info "lots_notconverted: #{$lots_notconverted.inspect}"
  nc = nb = 0
  $lots_converted.each_pair {|c, lots| nc += lots.size if lots}
  $lots_notconverted.each_pair {|c, lots| nb += lots.size if lots}
  $log.info "summary: converted lots: #{nc}, blocked_lots: #{nb}"
  return nb
end
