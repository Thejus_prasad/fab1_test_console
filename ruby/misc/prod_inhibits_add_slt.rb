=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: sfrieske, 2019-11-18

Version: 1.0.0

Note:
  based on FSDR report: http://vfc1qlikp01/QvAJAXZfc/opendoc.htm?document=fsdr%5Cfab1_fsdr_inhibits.qvw&lang=en-US&host=QVS%40FC1Cluster
=end

require 'siview'
require 'ruby/misc/inhibits_add_slt'


$env = 'f1prod'  # 'itdc'

$slt_add = 'QB'
$slt_marker = 'QE'
$tend = String.new('2037-12-31-00.00.00.000106')  # change if slt_add is changed
$memoprefix = 'MSR1594866;'  #  PD/QM/QN/QB: 'MSR1594866;', PE/POC: 'MSR1605606;', RV: 'MSR1373397;'

$reasons_exclude = ['XAPC', 'XFDC']

$reportfile = 'inhibits_slt_2020-01-06.csv'

#
# ------------- no changes below this line required ------------------------
#

authparams = $env == 'itdc' ? {} : {ntauth: true, user: nil, password: nil}
$sv = SiView::MM.new($env, authparams)

aparams = {tend: $tend, memoprefix: $memoprefix, reasons_exclude: $reasons_exclude}
$adder = InhibitsAddSublottype.new($sv, $slt_add, $slt_marker, aparams)

# $inhs = $adder.get_inhibits_from_report($reportfile)
# in case of an acceptable error (not all inhibits found): $inhs = $adder._inhs 
# $res = $adder.add_slt($inhs)
