=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: Steffen Steidten, GLOBALFOUNDRIES INC., 2009

History:
  2013-08-14 ssteidte, removed Ruby1.8 support
  2013-12-18 ssteidte, use new SiView::MM class in console()
  2014-01-14 ssteidte, remove obsolete load_file() and load_lib() (use load 'xxx.rb' instead)
  2014-06-16 ssteidte, added run_sequence to leverage the new tcrunner
  2014-07-14 dsteger,  added command line options to support loading from different dir or gems
  2014-10-10 ssteidte, improved logging to files; include assertions
  2014-10-22 ssteidte, start with irb -r ./ruby/console.rb
  2015-01-23 ssteidte, added back explicit loading of irb because of issues in many JRUBY releases
  2015-08-19 ssteidte, added svtest convenience method
  2017-01-02 sfrieske, removed DRb remoting support
  2017-11-10 sfrieske, set external_encoding BINARY
  2019-04-11 sfrieske, improved setting of LOAD_PATH and GEM_PATH
=end

# console for QA integration testing

require_relative 'set_paths'
$LOAD_PATH.push(File.expand_path('ruby/testcases'))
create_logger

# module DidYouMean
#   module Correctable
#     prepend_features NameError
#     def to_s
#       super
#     end
#   end
# end

# # define IRB#print to see all output in the log file
# require 'irb'
# class IRB::Irb
#   def print(*args); $log.print(*args); end
# end

# create a SiView::MM client named like $itdc
def console(env, params={})
  require 'siview'
  # create a SiView::MM client named like env ('itdc' -> $itdc)
  mm = SiView::MM.new(env.to_s, params)
  _env = env.to_s #.gsub('.', '')
  eval "$#{_env} = mm" if mm
  _env
end

# create a SiView::Test instance named like $itdctest
def svtest(env, params={})
  require 'siviewtest'
  # create a SiView::MM client named like env ('itdc' -> $itdc)
  svtest = SiView::Test.new(env.to_s, {carriers: '%'}.merge(params))
  _env = env.to_s #.gsub('.', '')
  eval "$#{_env}test = svtest" if svtest
  _env + 'test'
end


# for runtest and assertXX (for copy-and-paste)
def include_assertions
  require 'RubyTestCase'
  include Minitest::Assertions
  self.assertions = 0
end


# run a test case including properties
def runtest(tcname, env, params={})
  require 'RubyTestCase'
  TestCases.runtest(tcname, env, params)
  nil
end

# configure IRB
require 'pp'
require 'irb'
#require 'irb/completion'
# ENV['HOME'] ||= "#{ENV['HOMEDRIVE']}#{ENV['HOMEPATH']}"
IRB.setup nil
IRB.conf[:AUTO_INDENT] = true
IRB.conf[:PROMPT_MODE] = :SIMPLE
IRB.conf[:SAVE_HISTORY] = 200
IRB.conf[:HISTORY_FILE] = "#{Dir.pwd}/.irb-history"
# IRB.conf[:USE_READLINE] = false
# IRB.conf[:VERBOSE] = true   # for logging
irb = IRB::Irb.new
IRB.conf[:MAIN_CONTEXT] = irb.context
# start IRB
trap('SIGINT') {
  $setup_ok = false
  irb.signal_handle
}
begin
  # load a program in IRB (e.g. TCRunner)
  irb.__send__(:load, $PROGRAM_NAME) unless $PROGRAM_NAME.end_with?(__FILE__)
  catch(:IRB_EXIT) {irb.eval_input}
ensure
  IRB.irb_at_exit
end
