=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: Steffen Steidten, GLOBALFOUNDRIES INC., 2010

History:
  2015-01-29 ssteidte, moved ORB settings to corba.rb
  2018-03-05 sfrieske, logging uses logger directly if there is only one
  2019-03-12 sfrieske, use gem dir instead of gems.jar file
  2019-04-11 sfrieske, improved setting of LOAD_PATH and GEM_PATH
  2020-02-25 sfrieske, removed Encoding.default_external = Encoding::BINARY (use string.force_encoding|encode!(Encoding::BINARY))
=end

puts "RUBY_VERSION #{RUBY_VERSION}"
Thread.current['name'] = 'main'
$LOAD_PATH.push(Dir.pwd)
$LOAD_PATH.push(File.expand_path('ruby/lib'))
ENV['GEM_PATH'] = File.expand_path('ruby/gems')
##Gem.use_paths(Gem.path.first) # for using of old minitest-5.10.3

require 'fileutils'
require 'logger'


# create $log object with console log and rotating file log
def create_logger(params={})
  ll = []
  # console logger
  unless params[:noconsole]
    clog = Logger.new(STDERR)
    clog.level = params[:verbose] ? Logger::DEBUG : Logger::INFO
    clog.formatter = proc {|sev, t, name, msg| "#{t.strftime('%F %T')} #{Thread.current['name']} #{sev}: #{msg}\n"}
    ll << clog
  end
  # file logger
  fname = params[:file]
  fname = "log/console_#{Time.now.strftime('%F_%T').gsub(':', '-')}.log" if fname == true
  if fname
    FileUtils.mkdir_p(File.dirname(fname))
    flog = Logger.new(fname, (params[:maxbackups] || 11), (params[:maxsize] || 1024000)) # 10 files, 1MB each
    flog.level = (params[:file_verbose] || params[:verbose]) ? Logger::DEBUG : Logger::INFO
    flog.formatter = proc {|sev, t, name, msg| "#{t.strftime('%F %T')} #{Thread.current['name']} #{sev}: #{msg}\n"}
    ll << flog
  end
  $log = ll.size > 1 ? MultiLogger.new(*ll) : ll.first
  puts "created #{$log.inspect}"
end


class Logger

  def inspect
    "#<#{self.class.name} #{@logdev.dev.inspect}>"
  end

  # methods for generic use as STDOUT replacement, e.g.for IRB and TestRunner
  def print(*args)
    args.each {|a| @logdev.write(a)}
    nil
  end

  def puts(*args)
    args.each {|a| @logdev.write(a); @logdev.write("\n")}
    nil
  end

  # for Ruby2.2 minitest
  def sync; true; end

  def sync=(x); x; end

end


# fix error when rotating logfiles on Windows
class Logger::LogDevice
  def shift_log_age
    (@shift_age-3).downto(0) do |i|
      if FileTest.exist?("#{@filename}.#{i}")
        File.rename("#{@filename}.#{i}", "#{@filename}.#{i+1}")
      end
    end
    # @dev.close rescue nil
    # File.rename("#{@filename}", "#{@filename}.0")
    # @dev = create_logfile(@filename)
    FileUtils.cp(@filename, "#{@filename}.0")
    @dev.truncate(0)
    return true
  end
end


class MultiLogger
  attr_accessor :targets, :logdevs

  def initialize(*targets)
    @targets = targets
    @logdevs = targets.collect {|t| t.instance_variable_get(:@logdev)}
  end

  def inspect
    "#<#{self.class.name} #{@logdevs.collect {|l| l.dev}}>"
  end

  def method_missing(meth, *args, &block)
    @targets.collect {|t| t.send(meth, *args, &block)}
  end

  # # send log output to both targets
  # %w(level level= log debug info warn error fatal print puts sync sync=).each {|m|
  #   define_method(m) {|*args, &block| @targets.map {|t| t.send(m, *args, &block)}}
  # }

  # search in logfiles (in correct order if rotated)
  def grep_for(msg=nil)
    flog = @logdevs.find {|d| d.filename} || (puts 'no file logger found'; return)
    logfiles = (0 .. flog.instance_variable_get(:@shift_age)-2).collect {|i| "#{flog.filename}.#{i}"}
    logfiles.unshift(flog.filename)
    lines = logfiles.reverse.collect {|f| File.readable?(f) && File.readlines(f) || []}.flatten
    lines.select! {|line| line =~ msg} if msg
    lines
  end

end
