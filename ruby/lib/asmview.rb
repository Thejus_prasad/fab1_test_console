=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: Steffen Steidten, 2009-05-03, 2012-04-25

History:
  2012-04-25 ssteidte, derived from easysiview.rb
  2013-12-18 ssteidte, refactored EasySiViewR, SMFacade, XM, AsmViewR and Fab7 SiView
  2016-08-04 sfrieske, added lot_characteristics (no need to use AsmView::JCAP for this)
  2017-04-18 sfrieske, remove R9 tweaks
  2017-07-20 sfrieske, dropped support for old etc/connections/<env>.properties
  2019-07-09 sfrieske, removed AsmDieCount struct
  2021-02-05 sfrieske, use instance_of instead of kind_of
=end

require 'siview'


# Wrapper around the AsmView JCS, implemented as extension of SiView::MM
module AsmView
  include SiView

  class MM < ::SiView::MM
    attr_accessor :asm_manager

    def initialize(env, params={})
      super("asmview.#{env}", {flavour: 'asmview'}.merge(params))
    end

    def connect
      @jcscode = com.globalfoundries.jcs.asmview.code
      @customized = false
      super
      # create additional ASM_PPTServiceManager for AsmView specific TXs
      $log.info 'creating ASM_PPTServiceManager'
      obj = @orb.string_to_object(@iorparser.ior)
      @asm_manager = @jcscode.ASM_PPTServiceManagerHelper.narrow(obj)
    end

    # return created lot ID or nil on error
    def asm_vendorlot_receive(bank, product, count, params={})
      lot = params[:lot] || ''
      vendorlot = params[:vendorlot] || ''
      vendor = params[:vendor] || ''
      sublottype = params[:sublottype] || 'Vendor'
      content = params[:content] || 'Wafer'
      memo = params[:memo] || ''
      $log.info "asm_vendorlot_receive #{bank.inspect}, #{product.inspect}, #{count}, #{params.inspect}"
      #
      res = @asm_manager.TxASM_VendLotReceiveReq(@_user, oid(bank), oid(product), lot, sublottype, count, vendorlot, vendor, content, memo)
      return nil if rc(res) != 0
      return res if params[:raw]
      return res.createdLotID.identifier unless params[:prepare]
      # optionally call vendorlot_prepare
      return vendorlot_prepare(res.createdLotID.identifier, params[:carrier], params)
    end

    # return lot ID on success
    def asm_new_lot_release(params={})
      product = params[:product]
      route = params[:route] || 'TURNKEY.1'
      lottype = params[:lottype]
      sublottype = params[:sublottype]
      customer = params[:customer]
      template = params[:template]
      #
      if template
        tli = lot_info(template)
        product ||= tli.product
        route ||= tli.route
        lottype ||= tli.lottype
        sublottype ||= tli.sublottype
        customer ||= tli.customer
      end
      ($log.warn "product is not specified (correctly)"; return) unless product.instance_of?(String)
      lottype ||= 'Production'  # a.k.a. lotType
      sublottype ||= 'PO'
      customer ||= 'gf'
      order = params[:order] || ''
      lotOwner = params[:lotowner] || @_user.userID.identifier
      lotGenerationType = params[:lotGenerationType] || 'By Volume' # "By Source Lot"
      schedulingMode = params[:schedulingMode] || 'Forward'
      lotIDGenerationMode = params[:lotIDGenerationMode] || 'Automatic'
      productDefinitionMode = params[:productDefinitionMode] || 'By Product Quantity'
      priority = params[:priorityclass] || params[:priority] || '4'
      epriority = params[:epriority] || '499999'
      startTime = siview_timestamp(params[:starttime] || Time.now)
      finishTime = siview_timestamp(params[:finishtime] || Time.now + 86400)
      memo = params[:release_memo] || params[:memo] || ''
      comment = params[:comment] || ''
      #
      lot = params[:lot] || generate_new_lotids(customer, starttime: startTime).first
      $log.info "asm_new_lot_release lot: #{lot.inspect}"
      #
      nwafers = params[:nwafers] || 25
      srclot = params[:srclot]  # mandatory
      ($log.warn "  no srclot"; return) if srclot.empty?
      sli = lot_info(srclot, wafers: true)
      ($log.warn "  required srclot #{srclot.inspect} has no wafers"; return) if sli.wafers.empty?
      dswafers = sli.wafers.take(nwafers).collect {|w|
        @jcscode.pptSourceProduct_struct.new(w.wafer, any) if w.wafer != ''
      }.compact.to_java(@jcscode.pptSourceProduct_struct)
      dslot = [@jcscode.pptDirectedSourceLot_struct.new(oid(srclot), dswafers, any)].to_java(@jcscode.pptDirectedSourceLot_struct)
      sched = [@jcscode.pptReleaseLotSchedule_struct.new('0.0',
        oid(''), startTime, startTime, any)].to_java(@jcscode.pptReleaseLotSchedule_struct)
      ordertype = ''
      rattrs = @jcscode.asm_pptReleaseLotAttributes_struct.new(oid(lot), oid(product), customer, order,
        lotOwner, lottype, sublottype, oid(route), lotGenerationType, schedulingMode, lotIDGenerationMode,
        productDefinitionMode, priority, epriority, startTime, finishTime, comment, nwafers, dslot, sched, ordertype, any)
      #
      res = @asm_manager.TxASM_NewLotReleaseReq(@_user, rattrs, memo)
      return nil if rc(res) != 0
      # return scheduled lot id or STB
      return res.strReleasedLotReturn.lotID.identifier unless params[:stb]
      return asm_stb_released_lot(lot, srclot)
    end

    # return 0 on success
    def asm_new_lot_release_cancel(lot, params={})
      rq = product_request(lot)
      res = @asm_manager.TxASM_NewLotReleaseCancelReq(@_user, rq.lotID, params[:memo] || '')
      return rc(res)
    end

    # return lot on success
    def asm_stb_released_lot(lot, srclot, params={})
      $log.info "asm_stb_released_lot #{lot.inspect}, #{srclot.inspect}"
      $log.debug "  #{params.inspect}"
      memo = params[:stb_memo] || params[:memo] || ''
      carrier = lot
      sli = lot_info(srclot, wafers: true)   # returns max 25 wafers even if more than 25 are in the lot
      # set wafer and lot attributes
      waferids = params[:waferids] || params[:waferid]
      widprefix = unique_string # for generated wids
      nwafers = product_request(lot).productCount
      wattrs = sli.wafers.take(nwafers).collect {|w|
        # use explicitly passed wafer ids, else source wafer ids
        wid = waferids.instance_of?(Array) ? waferids.shift : w.wafer
        # generate missing wafer ids
        if wid.nil? or (wid == '')
          # T7M12 standard
          wid = ("%10.10s%2.2d" % [widprefix, w.slot]).gsub(' ', 'W')
        end
        @jcscode.asm_pptNewWaferAttributes_struct.new(oid(lot), oid(wid), w.slot, oid(srclot), oid(w.wafer), 1)
      }.to_java(@jcscode.asm_pptNewWaferAttributes_struct)
      lattrs = @jcscode.asm_pptNewLotAttributes_struct.new(oid(''), 'TURNKEY', true, wattrs)
      sattrs = [].to_java(@jcscode.asm_pptModuleSerialAttributes_struct)
      #
      res = @asm_manager.TxASM_STBReleasedLotReq(@_user, oid(lot), lattrs, sattrs, memo)
      return nil if rc(res) != 0
      ##return res if params[:raw]
      return lot
    end

    # params countains the paramters to change
    #
    # empty default values needed for external priority test
    #
    # return 0 on success BUT YOU NEED TO CHECK WITH LOTINFO
    def asm_schdl_change(lot, params={})
      $log.info "asm_schdl_change #{lot.inspect}, #{params.inspect}"
      ($log.warn "parameters must be passed as hash"; return) unless params.instance_of?(Hash)
      li = lot_info(lot) || return
      # get new parameters, using empty values as defaults
      product = params[:product] || ''
      route = params[:route] || ''
      opNo = params[:opNo]
      opNo ||= ((product.empty? && route.empty?) ? '' : li.opNo)
      schedulingMode = params[:schedulingMode] || '' #"Forward"
      priority = (params[:priorityclass] || params[:priority] || '').to_s
      startTime = params[:starttime] || ''
      finishTime = params[:finishtime] || '' #(Time.new + 86400).strftime("%Y-%m-%d-%H.%M.%S.000000")
      memo = params[:memo] || ''
      #
      res = @jcscode.pptLotSchdlChangeReqResult_structHolder.new
      # leave the schedule empty
      chg_schdl = [].to_java(@jcscode.pptChangedLotSchedule_struct)
      lattrs = [@jcscode.asm_pptRescheduledLotAttributes_struct.new(oid(lot), oid(product), li.route,
        oid(route), li.opNo, opNo, schedulingMode, priority, startTime, finishTime, chg_schdl, "", any)]
      #
      @asm_manager.TxASM_LotSchdlChangeReq(res, @_user, lattrs, memo)
      return res if params[:raw]
      return rc(res.value)
    end

    def asm_lot_split_with_id(lot, child, splitwafers, params={})
      mrte = params[:mergeroute] || ''
      mop = params[:mergeop] || ''
      bflag = false
      brte = params[:broute] || ''
      rop = params[:returnop] || ''
      memo = params[:memo] || ''
      #
      $log.info "asm_lot_split_with_id #{lot.inspect}, #{child.inspect}, #{splitwafers.inspect}, #{params.inspect}"
      _wafers = lot_info(lot, wafers: true, select: splitwafers).wafers
      ($log.warn "no valid wafers found"; return) unless _wafers && _wafers.size > 0
      wafers = _wafers.collect {|w| w.wafer}
      # optional mergepoint calculation, mop must be set explicitly to nil!
      unless mop
        # merge at current op if mop is set to nil
        li = lot_info(lot)
        mop, mrte = li.opNo, li.route
      end
      if mrte == ''
        li = lot_info(lot)
        mrte = li.route
      end
      mflag = (mop != '')
      #
      $log.info "  splitting wafers #{wafers.inspect}"
      $log.info "  merge route '#{mrte}', op '#{mop}'" if mop != ''
      res = @jcscode.asm_pptSplitWaferLotWithIDReqResult_struct.new
      #
      res = @asm_manager.TxASM_SplitWaferLotWithIDReq(@_user, oid(lot), oid(child), oids(wafers),
        mflag, oid(mrte), mop, bflag, oid(brte), rop, memo)
      return nil if rc(res) != 0
      ret = res.childLotID.identifier
      $log.info "  created child lot #{ret}"
      return ret
    end

    def asm_chiplot_split(lot, count, params={})
      mrte = params[:mergeroute] || ''
      mop = params[:mergeop] || ''
      bflag = false
      brte = params[:broute] || ''
      rop = params[:returnop] || ''
      memo = params[:memo] || ''
      #
      mflag = (mop != "")
      $log.info "asm_chiplot_split #{lot.inspect}, #{count.inspect}, #{params.inspect}"
      sattrs = [@jcscode.asm_pptModuleSerialAttributes_struct.new].to_java(@jcscode.asm_pptModuleSerialAttributes_struct)
      #
      res = @asm_manager.TxASM_SplitChipLotReq(@_user, oid(lot), count,
        mflag, oid(mrte), mop, bflag, oid(brte), rop, sattrs, memo)
      return nil if rc(res) != 0
      ret = res.childLotID.identifier
      $log.info "  created child lot #{ret}"
      return ret
    end

    # return child lot id on success, else nil
    def asm_chiplot_split_with_id(lot, child, count, params={})
      mrte = params[:mergeroute] || ''
      mop = params[:mergeop] || ''
      bflag = false
      brte = params[:broute] || ''
      rop = params[:returnop] || ''
      memo = params[:memo] || ''
      mflag = (mop != "")
      $log.info "asm_chiplot_split_with_id #{lot.inspect}, #{child.inspect}, #{count.inspect}, #{params.inspect}"
      sattrs = [@jcscode.asm_pptModuleSerialAttributes_struct.new].to_java(@jcscode.asm_pptModuleSerialAttributes_struct)
      #
      res = @asm_manager.TxASM_SplitChipLotWithIDReq(@_user, oid(lot), oid(child), count,
        mflag, oid(mrte), mop, bflag, oid(brte), rop, sattrs, memo)
      return nil if rc(res) != 0
      ret = res.childLotID.identifier
      $log.info "  created child lot #{ret}"
      return ret
    end

    def asm_lot_merge(lot, child, params={})
      memo = params[:memo] || ''
      $log.info "asm_lot_merge #{lot.inspect}, #{child.inspect}"
      li = lot_info(lot, :wafers=>true)
      slots = params[:slots]
      slots ||= (1..25).collect {|i| i unless li.wafers.find {|w| w.slot == i}}.compact
      if child == nil
        # merge all child lots
        children = lot_family(lot) - [lot]
        children -= [li.parent] unless li.parent.empty?
      else
        children = [child]
      end
      ($log.info "  lot #{lot} has no children to merge"; return 0) unless children and children.size > 0
      rets = children.sort.reverse.collect {|c|
        lci = lot_info(c, wafers: true)
        $log.info "  merging lot #{lot} with child #{c}"
        wattrs = lci.wafers.collect {|w| @jcscode.pptMoveWaferAttributes_struct.new(oid(w.wafer), slots.shift, any)}
        #
        res = @asm_manager.TxASM_MergeWaferLotReq(@_user, oid(lot), oid(c), wattrs, memo)
        rc(res)
      }
      return rets.max # return 0 only if no errors occurred
    end

    # note: lot MUST be in a carrier else rc 1922 is returned; return 0 on success
    def asm_scrap_wafers_cancel(lot, params={})
      reason = params[:reason]
      category = params[:category] || 'TURNKEY'
      cinfo = code_list('WaferScrapCancel').find {|e|
        cid = e.code.identifier
        (cid == reason) || (reason.nil? && %w(ALL MISC ENDE EVAL).include?(cid))
      }
      ($log.warn 'wrong reason code'; return) unless cinfo
      reasonID = cinfo.code
      reasonopNo = params[:reasonopNo] || ''
      memo = params[:memo] || ''
      #
      li = lot_info(lot, wafers: true, select: params[:wafers]) || return
      carrier = params[:carrier] || ''
      ww = li.wafers.collect {|w|
        @jcscode.pptScrapCancelWafers_struct.new(oid(w.wafer), w.slot, reasonID, oid(lot), reasonopNo, any)
      }
      $log.info "asm_scrap_wafers_cancel #{lot.inspect}, reason: #{reasonID.identifier.inspect}"
      #
      res = @asm_manager.TxASM_ScrapWaferCancelReq(@_user, oid(lot), oid(carrier), category, ww, memo)
      return rc(res)
    end

    # convert wafer lot to chip lot, return 0 on success
    def asm_change_lotcontent(lot, qty, params={})
      content = params[:content] || 'Chip'
      carrier = params[:carrier] || ''
      category = params[:category] || 'TURNKEY'
      dummyflag = (carrier == '')
      memo = params[:memo] || ''
      $log.info "asm_change_lotcontent #{lot.inspect}, #{qty.inspect}, #{params.inspect}"
      #
      res = @asm_manager.TxASM_ChangeLotContentReq(@_user, content, oid(lot), dummyflag, oid(carrier), category, qty, memo)
      return rc(res)
    end

    # return 0 on success
    def asm_adjust_chipcount(lot, qty, params={})
      reason = params[:reason] || 'R0001'
      memo = params[:memo] || ''
      $log.info "asm_adjust_chipcount #{lot.inspect}, #{qty.inspect}, #{params.inspect}"
      #
      res = @asm_manager.TxASM_AdjustChipCountReq(@_user, oid(lot), oid(reason), qty, memo)
      return rc(res)
    end

    # return a hash of binning data by wafer
    def asm_diecount(lot)
      res = @asm_manager.TxASM_DieCountInq(@_user, oid(lot))
      return nil if rc(res) != 0
      return Hash[res.asm_strBinCountWafers.collect {|w|
        [w.waferID.identifier, w.asm_strBinCounts.collect {|b| {binnumber: b.binNo.to_i, dieqty: b.qty, product: b.productID}}]
      }]
    end

    # # set sort result - UNUSED
    # #
    # # diecounts is either a hash with [AsmDieCount] arrays per wafer or [diecount] integer arrays for each wafer
    # #
    # # diecounts is converted to default product and bin values if only integers are provided
    # #
    # # return 0 on success
    # def asm_diecount_req(lot, diecounts, params={})
    #   $log.info "asm_diecount_req #{lot.inspect}"
    #   $log.debug "  diecounts: #{diecounts.inspect}"
    #   dcs = @asm_manager.TxASM_DieCountInq(@_user, oid(lot))
    #   bincountwafers = dcs.asm_strBinCountWafers
    #   if bincountwafers.size == 0
    #     # chip lot, create dummy wafer entry
    #     bincountwafers = [@jcscode.asm_pptBinCountWafer_struct.new(oid(''), nil)].to_java(@jcscode.asm_pptBinCountWafer_struct)
    #   end
    #   bincountwafers.each_with_index {|b, i|
    #     bins = diecounts.instance_of?(Hash) ? diecounts[b.waferID.identifier] : diecounts[i]
    #     bins ||= []
    #     $log.debug "  #{b.waferID.identifier}: #{bins.inspect}"
    #     _bincounts = []
    #     bins.each_with_index {|bin, j|
    #       if bin.kind_of?(Numeric)
    #         next if bin == 0  # values of 0 are not allowed
    #         bin = ["P#{j+1}", "#{j+1}", bin]
    #       else
    #         next if bin.qty == 0
    #       end
    #       _bincounts << @jcscode.asm_pptBinCount_struct.new(*bin.to_a)
    #     }
    #     b.asm_strBinCounts = _bincounts.to_java(@jcscode.asm_pptBinCount_struct)
    #   }
    #   res = @asm_manager.TxASM_DieCountReq(@_user, oid(lot), bincountwafers)
    #   return rc(res)
    # end

  end


  class SM < ::SiView::SM
    def initialize(env, params={})
      super("asmview.#{env}", {flavour: 'asmview'}.merge(params))
    end

    def connect(params)
      super({jcscode: com.ibm.asmview.sm}.merge(params))
    end
  end

end
