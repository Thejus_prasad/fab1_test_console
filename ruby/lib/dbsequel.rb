=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.


Author:   Steffen Steidten, 2005-03-01

Version:  4.4.0

History:
  2009-05-07 ssteidte, added support for jtds
  2010-03-10 ssteidte, fixed issues reading config files
  2010-10-27 ssteidte, catch exception at disconnect
  2010-12-17 ssteidte, added support for microsoft.sqlserver
  2011-10-28 ssteidte, added _q_restriction to support the query building
  2012-01-23 ssteidte, migrated from DBI to Sequel
  2012-07-11 ssteidte, allow wildcards in WHERE clauses, automatically compare using LIKE
  2012-08-20 ssteidte, fix timestamp handling for Oracle JDBC connections
  2012-08-23 ssteidte, added module AutoDB
  2012-09-27 ssteidte, added support for INSERT and DELETE
  2013-08-14 ssteidte, removed Ruby1.8 support
  2015-04-20 ssteidte, added MySQL support (for Lotgrading DB, AutoScriptParamUpload job)
  2015-06-29 ssteidte, added session timezone and t.localtime(@dbtimezone) in _q_restriction; removed AutoDB
  2015-09-08 ssteidte, ENV['TZ'] workaround for Oracle SQL/JDBC Timestamps
  2016-01-22 sfrieske, added IS NULL handling to _q_restriction
  2016-03-01 sfrieske, removed TZ workaround
  2016-05-04 sfrieske, added update and update_table
  2017-01-16 sfrieske, load JDBC drivers from lib/jdbc
  2017-05-08 sfrieske, removed select_one and renamed select_all to select, select_all is an alias for now
  2017-11-21 sfrieske, removed open and close, use connect and disconnect; selectively require JDBC jar files
  2018-02-15 sfrieske, removed select_all alias
  2019-01-07 sfrieske, removed reconnect
  2019-11-05 sfrieske, use Java 8's java.time.ZoneId instead of Ruby tzinfo
  2021-02-05 sfrieske, use instance_of instead of kind_of
  2021-08-26 sfrieske, use read_config instead of _config_from_file
  2021-12-07 sfrieske, _q_restriction supports ORed queries, fixed dbtimezone calc for fractions of an hour
=end

require 'rubygems'
require 'sequel'
require 'util/readconfig'


# Holds DB connections, ''./etc/databases.conf' be a collection of database connection data
# with 'mydb.env' one of the entries you can do:
#
#   db = DB.new('mydb.env')
#   res = db.select('select count(*) from TABLE where TIME > ?'', Time.localtime(2006,12,1))
#   and db.ds to see the query
class DB
  attr_accessor :dsn, :user, :password, :cleartext, :dbh, :ds, :schema, :dbtimezone

  def initialize(instance, overrides={})
    params = read_config(instance, 'etc/databases.conf', overrides)
    @dsn = params[:dsn]
    ($log.info "no such DB: #{instance.inspect}, #{params}"; return) if @dsn.nil?
    if @dsn.start_with?('jdbc:db2:')
      Dir['lib/jdbc/db2jcc*jar'].each {|f| require f}
    elsif @dsn.start_with?('jdbc:jtds:')
      Dir['lib/jdbc/jtds*jar'].each {|f| require f}
    elsif @dsn.start_with?('jdbc:mysql:')
      Dir['lib/jdbc/mysql*jar'].each {|f| require f}
    elsif @dsn.start_with?('jdbc:oracle:thin:')
      Dir['lib/jdbc/ojdbc*jar'].each {|f| require f}
    end
    @schema = params[:schema]
    @user = params[:user] || params[:username]
    @password = params[:password]
    @cleartext = !!params[:cleartext]
    @dbtimezone = params[:dbtimezone] || (instance && instance.split('.').last.start_with?('f8', 'mtqa') ? '-05:00' : '+00:00')
    unless @dbtimezone.start_with?('+', '-')
      # for DBs in local timezones with summer time change, e.g. Dresden YMS DB with 'Europe/Berlin'
      offsecs = java.time.ZoneId.of(@dbtimezone).getRules().getOffset(java.time.Instant.now()).getTotalSeconds()
      offdiv = offsecs.divmod(3600)
      @dbtimezone = '%+2.2d:%02d' % [offdiv[0], offdiv[1] / 60]
    end
    connect(params) unless params[:connect] == false
  end

  def inspect
    "#<#{self.class.name} dsn: #{@dsn.inspect} user: #{@user.inspect}>"
  end

  # return dbh object
  def connect(params={})
    return unless @dsn
    disconnect
    ac = nil
    if @dsn.include?(':oracle')
      ac = proc {|c|
        c.createStatement.execute("ALTER SESSION SET CURRENT_SCHEMA = #{@schema}") if @schema
        c.createStatement.execute("ALTER SESSION SET TIME_ZONE = '#{@dbtimezone}'")
      }
    elsif @dsn.include?(':db2')
      ac = proc {|c|
        c.createStatement.execute("SET CURRENT SCHEMA = #{@schema}") if @schema
        # not working in LUW
        ##c.createStatement.execute("SET CURRENT TIME ZONE = '#{@dbtimezone}'")
      }
    end
    begin
      pw = @cleartext ? @password : (@password || '').unpack('m').first
      @dbh = Sequel.connect(@dsn, params.merge(user: @user, password: pw, after_connect: ac))
      @dbh.instance_variable_set(:@dbtimezone, @dbtimezone)  # timezone is used otherwise
      # handle server timezones correctly
      def @dbh.to_application_timestamp(v)
        v[-2] += v[-1].to_f / 1000000000.0 # ns
        v[-1] = @dbtimezone
        Time.new(*v)
        #t = now
        #local(t.year, t.month, t.day, hour, minute, second, usec)
      end
      true
    rescue => e
      $log.warn {"error opening DB\n#{$!}"}
      $log.debug {e.backtrace.join("\n")}
      nil
    end
  end

  def disconnect
    @dbh.disconnect if @dbh
    @dbh = nil
  end

  def connected?
    !!@dbh
  end

  def select(*args)
    begin
      @ds = @dbh[*args]
      @ds.all.collect {|e| e.values}
    rescue => e
      $log.warn "error:\n#{$!}"
      $log.debug e.backtrace.join("\n")
      nil
    end
  end

  def insert(*args)
    begin
      @ds = @dbh[*args]
      @ds.insert
      true
    rescue => e
      $log.warn "error:\n#{$!}"
      $log.debug e.backtrace.join("\n")
      nil
    end
  end

  # values is a hash with column names as keys
  def insert_into(table, values)
    cols = values.keys.collect {|k| k.to_s.upcase}.join(',')
    vals = values.keys.collect {|k| '?'}.join(',')
    insert("INSERT INTO #{table} (#{cols}) VALUES(#{vals})", *values.values)
  end

  def delete(*args)
    begin
      @ds = @dbh[*args]
      @ds.delete
      true
    rescue => e
      $log.warn "error:\n#{$!}"
      $log.debug e.backtrace.join("\n")
      nil
    end
  end

  # delete from table, params is a hash suitable for _q_restriction
  def delete_from(table, params)
    q, qargs = _q_restriction("DELETE FROM #{table}", [], params)
    qargs.unshift(q)
    delete(*qargs)
  end

  def update(*args)
    begin
      @ds = @dbh[*args]
      @ds.update
      true
    rescue => e
      $log.warn "error:\n#{$!}"
      $log.debug e.backtrace.join("\n")
      nil
    end
  end

  # update table, stmt is the update statement like "set lotid='XXX'", params is a hash suitable for _q_restriction
  def update_table(table, stmt, params)
    q, qargs = _q_restriction("UPDATE #{table} #{stmt}", [], params)
    qargs.unshift(q)
    update(*qargs)
  end

  # run generic statements
  def execute(s)
    @dbh.execute(s)
  end

  # query construction helper, adds WHERE clauses concatenated by AND
  def _q_restriction(q, qargs, col, p=nil)
    if p
      unless q.end_with?(' OR')
        q += q.include?(' WHERE ') ? ' AND' : ' WHERE'
      end
      if p.instance_of?(Array)
        q += " #{col.to_s} IN(?"
        (p.size - 1).times {q += ', ?'}
        q += ')'
        qargs += p
      elsif p == 'IS NULL'
        q += " #{col.to_s} IS NULL"
      else
        q += " #{col.to_s}"
        if p.instance_of?(String) && (p.include?('%') || p.include?('?'))
          # use "col LIKE p" if p has wildcards
          q += ' LIKE ?'
        elsif col.instance_of?(String) && (col.include?('<') || col.include?('>'))
          # allow to pass "col >=" p
          q += ' ?'
        else
          q += '=?'
        end
        if p.instance_of?(Time)  
          # conversion is for DB2 and SQLServer, does not matter for Oracle
          qargs << p.clone.localtime(@dbtimezone)
        else
          qargs << p
        end
      end
    elsif col.instance_of?(Hash)
      col.each_pair {|c, p| q, qargs = _q_restriction(q, qargs, c, p)}
    end
    return [q, qargs]
  end

  # misc information
  def tables(params={})
    return unless @dbh
    if @dbh.database_type == :oracle
      s = params[:schema] || @schema
      ret = []
      @dbh.send('metadata', :getTables, nil, s, params[:name], ['TABLE'].to_java(:string)) {|h| ret << h[:table_name]}
      return ret
    else
      return @dbh.tables
    end
  end

  # # not working, UNUSED
  # def columns(table, params={})
  #   return unless @dbh
  #   s = params[:schema] || @schema
  #   q = s ? "#{s}__" : ''
  #   q += table.to_s
  #   Hash[@dbh.schema(q.intern)]
  # end

  # UNUSED
  # def rowcount(table)
  #   q = "select count(*) from #{table}"
  #   q += ' with UR' if @dsn.include?(':db2')
  #   res = select(q).first || return
  #   res.size == 1 ? res.first : nil
  # end

end
