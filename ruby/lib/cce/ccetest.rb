=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: Steffen Steidten, 2015-09-02

Version: 1.1.0

History:
  2018-09-18 sfrieske, move YMS and CCE db access to cceloader
  2020-10-19 sfrieske, CCE::Test does not inherit from SiView::Test
=end

require 'siview'
require_relative 'cceapp'
require_relative 'ccedb'
require_relative 'cceloader'


module CCE

  # CCE testing, app: http://f1oqacced1:8080/cce/#approve
  class Test
    attr_accessor :sv, :app, :yms, :steps, :dataset, :triggerpd

    def initialize(env, params={})
      @sv = params[:sv] || SiView::MM.new(env)
      @app = CCE::App.new(env, sv: @sv)
      @yms = CCE::YMSLoader.new(env, sv: @sv)
      @steps = params[:steps] || ['PMIDEDI', 'BUMPDEVDI', 'BUMPEPLTNICDI', 'BUMPUETDI', 'FOUT-DI', 'FOUT-DI-BS']
      #@steps = params[:steps] || ['PMIDEDI', 'BUMPDEVDI', 'FOUT-DI', 'FOUT-DI-BS']
      # points to a set of directories in testcasedata/cce, e.g. ..../101/default
      @dataset = params[:dataset] || '101'
      @triggerpd = params[:triggerpd] || 'FOUT-CERT.01'
    end

    def inspect
      "#<#{self.class.name} #{@sv.env.inspect}, dataset: #{@dataset}>"
    end

    # create lot data, send it to the YM loader and send the trigger signal, return true on success
    def create_lot_data(lot, params={})
      dataset = params[:dataset] || @dataset
      steps = params.delete(:step) || @steps
      steps = [steps] unless steps.kind_of?(Array)
      steps.each {|step|
        op = step.sub('-BS', '')  # override SiView op for 'FOUT-DI-BS'
        @sv.lot_hold_release(lot, nil)
        @sv.lot_opelocate(lot, nil, op: /#{op}/).zero? || return
        @yms.send_inspection_data(lot, dataset, params.merge(step: step)) || return
      }
      ($log.warn "dryrun, not sending CCE trigger msg"; return) if params[:dryrun]
      #
      tsleep = 5 + @sv.lot_info(lot).nwafers * 7 * steps.size
      $log.info "waiting #{tsleep} s for YM processing"; sleep tsleep
      (@sv.lot_opelocate(lot, nil, op: @triggerpd) == 0) || return
      @app.mq.mq_response.delete_msgs(nil).nil? && ($log.warn 'MQ issue'; return)
      @app.send_trigger(lot)
      return @app.cce_response(lot).nil?  # on success there is no(!) response
    end

  end

end
