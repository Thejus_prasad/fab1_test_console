=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author:  ssteidte, 2015-09-02

Version: 1.3.2

History:
  2017-09-15 sfrieske, added Importer
  2018-09-05 dsteger,  new Importer file format
  2019-12-04 sfrieske, new KLA file format
  2020-10-19 sfrieske, configurable timestamps
  2021-08-31 sfrieske, requiring 'time' for #iso8601
=end

require 'fileutils'
require 'time'
require 'remote'
require 'siview'
require_relative 'ccedb'


# CCE test support
module CCE

  # Prepare lot data and feed the loader. ITDC loader logs: f1dmsd1:/YM/data/log/SCAN/upload_SCAN.log
  class YMSLoader
    attr_accessor :sv, :ymsdb, :ymssrv, :datadir, :imagedir, :templatedir #, :diepitch

    def initialize(env, params={})
      @sv = params[:sv] || SiView::MM.new(env)
      # @ymsdb = params[:ymsdb] || CCE::YMSDB.new(env)
      @datadir = params[:datadir] || '/YM/data/load/SCAN'
      # @imagedir = params[:imagedir] || '/YM/data/tool_image'
      @templatedir = params[:templatedir] || 'testcasedata/cce'
      # @diepitch = {  # SiView ProductGroup (YM: Product) as key
      #   'PANTHR1-1F'=>'7796.000 6866.000',
      #   'BAGERA1-5F'=>'2.5098180000e+003 2.1098530000e+003',
      # }
      @ymssrv = RemoteHost.new("yms.#{env}")
      # cleanup: /ym/data/tool_image
      #          /YM/data/transfer_to_archive
    end

    def inspect
      "#<#{self.class.name}  #{@ymssrv.inspect}>"
    end

    # return free disk space in GB
    def disk_free
      res = @ymssrv.exec!("df -k /YM/data/load/SCAN | grep -v Filesys | awk '{print $4}'") || return
      return (res.to_i / 1048576).round
    end

    # send inspection data to the loader directories, select specific wafers by select: ["alias1", ..]
    # dataset is a subdirectory in etc/testdata/cce that contains KLA and corresponding image files
    #
    # return true on success
    def send_inspection_data(lot, dataset, params={})
      li = @sv.lot_info(lot, wafers: true, select: params[:select])
      step = params[:step] || li.op.split('.').first
      $log.info "send_inspection_data #{lot.inspect}, #{dataset.inspect}, step: #{step.inspect}"
      dsdir = File.join(@templatedir, dataset, step)
      dsdir = File.join(@templatedir, dataset, 'default') unless File.exists?(dsdir)
      $log.info "  searching files in #{dsdir.inspect}"
      klarfnames = Dir["#{dsdir}/*.kla*"].sort
      ($log.warn '  no KLA file found'; return) if klarfnames.nil? || klarfnames.empty?
      imagenames = Dir["#{dsdir}/*.tif"].sort
      #
      @ymssrv.close  # fresh connection (??)
      totalsize = 0
      now = params[:timestamp] || Time.now
      ts = now.strftime('%m-%d-%Y %H:%M:%S')
      mests = "\"#{now.strftime('%Y%m%d_%H%M%S')}\""
      li.wafers.each_with_index {|w, widx|
        fileprefix = "#{lot}_#{w.alias}_#{step}"
        # replace klarfile data with current data
        tgtimgfiles = {}
        # reading_imagenames = false
        # klarftype = 0
        klarfname = klarfnames[widx % klarfnames.size]
        klarflines = File.readlines(klarfname) || return
        data = klarflines.each_with_index.collect {|line, lineidx|
          if line.start_with?('ResultTimestamp ')
            "ResultTimestamp #{ts};\n"
          elsif line.start_with?('FileTimestamp ')
            "FileTimestamp #{ts};\n"
          elsif line.start_with?('MESSubstitution ')
            "MESSubstitution #{mests};\n"
          elsif line.start_with?('LotID ')
            "LotID \"#{lot}\";\n"
          elsif line.start_with?('ResultsID ')
            "ResultsID \"#{lot}\";\n"
          elsif line.start_with?('DeviceID ')
            "DeviceID \"#{li.product.split('.').first}\";\n"
          elsif line.start_with?('Product ')
            "Product \"#{li.productgroup}\";\n"
          elsif line.start_with?('StepID ')
            "StepID \"#{step}\";\n"
          # elsif line.start_with?('DiePitch ')
          #   if dp = @diepitch[li.productgroup]
          #     "DiePitch #{dp};\n"
          #   else
          #     line
          #   end
          elsif line.start_with?('WaferID ')
            "WaferID \"#{w.alias}\";\n"
          elsif line.start_with?('WaferScribe ')
            "WaferScribe \"#{w.wafer}\";\n"
          elsif line.start_with?('Slot ')
            "Slot #{w.slot};\n"
          elsif line.start_with?('TiffFilename ')
            # klarftype = 1
            tfile = line.split[1].split(';')[0]
            iname = imagenames.find {|e| e.end_with?(tfile)} || ($log.warn "  no such image: #{tfile}"; return)
            tgtimgfile = "#{fileprefix}#{File.extname(iname)}"
            tgtimgfiles[tgtimgfile] = iname
            "TiffFilename #{tgtimgfile};\n"
          # elsif line.start_with?('VarsList')
          #   klarftype = 2
          #   reading_imagenames = true
          #   line
          # elsif reading_imagenames
          #   reading_imagenames = false if line.end_with?(";\n")
          #   ff = line.split
          #   tfile = ff[1]
          #   iname = imagenames.find {|e| e.end_with?(tfile)} || ($log.warn "  no such image: #{tfile}"; return)
          #   tgtimgfile = "#{fileprefix}-#{tgtimgfiles.size+1}#{File.extname(iname)}"
          #   tgtimgfiles[tgtimgfile] = iname
          #   ff[1] = tgtimgfile
          #   ' ' + ff.join(' ') + "\n"
          else
            line
          end
        }
        #
        export = params[:export]
        exportdir = "log/CCE/#{lot}/#{step}"
        FileUtils.mkdir_p(exportdir)
        dryrun = params[:dryrun]
        # send image data
        tgtimgfiles.each_pair {|tgtimgfile, iname|
          imagedata = File.binread(iname) || ($log.warn "  missing imagefile: #{iname}"; return)
          totalsize += imagedata.bytesize
          $log.info "  #{'not ' if dryrun}sending #{tgtimgfile} (from #{File.basename(iname)})"
          if export
            iexdir = File.join(exportdir, 'images')
            FileUtils.mkdir_p(iexdir)
            File.binwrite(File.join(iexdir, tgtimgfile), imagedata)
          end
          unless dryrun
            # imagedir = case klarftype
            # when 2; @datadir
            # else @imagedir
            # end
            # p = File.join(imagedir, tgtimgfile)
            p = File.join(@datadir, tgtimgfile)
            @ymssrv.write_file(imagedata, p)
            @ymssrv.exec!("chmod a+r #{p}")
            # res = @ymssrv.exec!("ls -l #{p}")
            # $log.info "file written: #{res}"
          end
        }
        # send KLA file
        klarfdata = data.join
        totalsize += klarfdata.bytesize
        tgtklarfile = fileprefix + '.klarf'
        $log.info "  #{'not ' if dryrun}sending #{tgtklarfile} (from #{File.basename(klarfname)})"
        if export
          File.binwrite(File.join(exportdir, tgtklarfile), klarfdata)
        end
        unless dryrun
          p = File.join(@datadir, tgtklarfile)
          @ymssrv.write_file(klarfdata, p)
          @ymssrv.exec!("chmod a+r #{p}")
          # res = @ymssrv.exec!("ls -l #{p}")
          # $log.info "file written: #{res}"
        end
      }
      $log.info "total bytes written: #{(totalsize / 1048576).round} MB, free disk space: #{disk_free} GB"
      true
    end

  end

  # prepare data for the Backup Operation lot importer
  class Importer
    attr_accessor :sv, :appdb, :ccesrv, :datadir

    def initialize(env, params={})
      @sv = params[:sv] || SiView::MM.new(env)
      @appdb = params[:appdb] || CCE::AppDB.new(env)
      @datadir = params[:datadir] || '/local/work/cce_import/source'
      @ccesrv = RemoteHost.new("cce.#{env}")
    end

    def inspect
      "#<#{self.class.name}  #{@ccesrv.inspect}>"
    end

    # send lot information file to CCE importer, return remote file name on success
    def send_import_data(lot, params = {})
      li = @sv.lot_info(lot, wafers: true, select: params[:select])
      maskset = params[:maskset] || @appdb.maskset(li.product) || return
      dieset = params[:dieset] || @appdb.die_coords_sort(li.product) || return
      $log.info "send_import_data #{lot.inspect}"
      ts = (params[:timestamp] || Time.now).utc.iso8601(3).tr('-:', '')
      data = ['Spec Version: 1.0',
        "Lot ID : #{lot}", "Product Group: #{li.productgroup}", "Product: #{li.product}",
        'Certification Mode: SEMI', "Certification Time: #{ts}", "Layout: #{maskset.values.join(';')}", 'Dies:'
      ]
      # die coords are not required by the importer (are validated if sent)
      # dieset.each {|die|
      #   ucs = @appdb.die_coords_ucs(die) || ($log.warn "no UCS coords for #{ds}"; return)
      #   data << "#{die.x}:#{die.y};#{ucs[0]}:#{ucs[1]}"
      # }
      data << 'DieList wafer;waferAlias;inspection;inspectionTime;sX;sY;uX;uY;type;code'
      reasoncode = params[:reasoncode] || 955
      li.wafers.each_with_index {|w, i|
        die = params[:die] || dieset.sample # take a random die per default
        ucs = @appdb.die_coords_ucs(die) || ($log.warn "no UCS coords for #{ds}"; return)
        data << "#{w.wafer};\t#{w.alias};\tBUMPBSMZDI;\t#{ts};\t#{die.x};\t#{die.y};\t#{ucs[0]};\t#{ucs[1]};\tMAN_OPT;\t#{reasoncode}"
      }
      data = data.join("\n")
      tgtfile = "#{lot}.mask.#{Time.now.strftime('%Y%m%d%H%M%S')}"
      exportdir = 'log/cce_import'
      if params[:export]
        FileUtils.mkdir_p(exportdir)
        File.binwrite(File.join(exportdir, tgtfile), data)
      end
      fname = File.join(@datadir, tgtfile)
      ($log.info "  dry run only, not sending file #{fname}"; return fname) if params[:dryrun]
      $log.info "  writing #{fname.inspect}"
      @ccesrv.write_file(data, fname) || return
      $log.info "  chmod a+r #{fname}"
      @ccesrv.exec!("chmod a+r #{fname}") || return
      return fname
    end

  end

end
