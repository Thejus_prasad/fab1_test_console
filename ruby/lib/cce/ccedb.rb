=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author:     ssteidte, 2015-09-02
Version:    1.0.4

History:
  2017-05-08 sfrieske, replaced select_one by select
  2018-09-05 dsteger,  added maskset and dieset queries
  2019-03-22 sfrieske, increased precision for most maskset values
=end

require 'dbsequel'


module CCE
  # YmEqp = Struct.new(:eqp, :model, :vendor, :eqpid)
  # YmProd = Struct.new(:product, :productid)  # actually product group!
  # YmStep = Struct.new(:op, :stepid, :productid)
  # YmLot = Struct.new(:lot, :lotid, :productid, :timestamp, :tend)
  # YmWafer = Struct.new(:wafer, :slot, :waferid, :lotid, :productid)
  MaskSet = Struct.new(:num_die_x, :num_die_y, :rtcl_orgin_off_x, :rtcl_orgin_off_y, :wafer_diameter, 
    :edge_exclusion_dist, :die_size_x, :die_size_y, :flash_size_x, :flash_size_y)
  DieSort = Struct.new(:x, :y, :maskset_sk, :ucslayout_sk)

  # CCE application database
  class AppDB
    attr_accessor :db

    def initialize(env, params={})
      @db = params[:db] || ::DB.new("cce.#{env}")
    end

    def lotimportstate(lot)
      res = @db.select('select count(*) from LOTIMPORTSTATE where LOTID = ?', lot).first
      return res ? res.first : nil
    end

    def fileimporthist(lot)
      res = @db.select('select count(*) from FILEIMPORTHISTORY where LOTID = ?', lot).first
      return res ? res.first : nil
    end

    # retrieves the mask set information of a product exactly tailored for import files
    def maskset(product)
      q = 'select NUM_DIE_IN_X, NUM_DIE_IN_Y, RTCL_ORIGIN_OFFSET_X, RTCL_ORIGIN_OFFSET_Y, 
              WAFER_DIAMETER_IN_MM, EDGE_EXCLUSION_DIST, DIE_SIZE_X, DIE_SIZE_Y, FLASH_SIZE_X, FLASH_SIZE_Y
            from MASKSET, V_PRODUCT_TO_MASKSET 
            where MASKSET.MASKSET_SK = V_PRODUCT_TO_MASKSET.MASKSET_SK
            and V_PRODUCT_TO_MASKSET.PRODUCT_ID = ?'
      res = @db.select(q, product) || return
      ($log.warn "AppDB#maskset: no unique data for product #{product.inspect}"; return) if res.size != 1
      r = res.first
      # [2, 3, 5, 6, 7, 8, 9].each {|i| r[i] = r[i].to_i}
      return MaskSet.new(*r)
    end

    # retrieves the sort die coordinates of a product (for import files)
    def die_coords_sort(product)
      # TODO: and TEST_COORD_SK = 
      q = 'select SORT_DIE_X, SORT_DIE_Y, SORT_DIE_WAFER_LAYOUT.MASKSET_SK, UCS_DIE_WAFER_LAYOUT_SK
            from SORT_DIE_WAFER_LAYOUT, V_PRODUCT_TO_MASKSET 
            where SORT_DIE_WAFER_LAYOUT.MASKSET_SK = V_PRODUCT_TO_MASKSET.MASKSET_SK
            and V_PRODUCT_TO_MASKSET.PRODUCT_ID = ?'
      res = @db.select(q, product) || return
      return res.collect {|r|
        DieSort.new(* r.collect {|e| e.to_i})
      }
    end

    # return UCS [x, y] coords from DieSort data
    def die_coords_ucs(diesort)
      q = 'select DIE_X, DIE_Y from UCS_LAYOUT_DIE_WAFER where MASKSET_SK = ? and DIE_WAFER_LAYOUT_SK = ?'
      res = @db.select(q, diesort.maskset_sk, diesort.ucslayout_sk) || return
      ($log.warn "AppDB#die_coords_ucs: no unique data for #{diesort}"; return) if res.size != 1
      return res.first
    end

  end


  # YM DMS tables, currently not used
  class OBS_YMSDB
    attr_accessor :db

    def initialize(env, params={})
      @db = params[:db] || ::DB.new("yms.#{env}")
    end

    def ym_eqp(params={})
      qargs = []
      q = 'SELECT EQUIPMENTID, MODELID, VENDOR, EQUIPMENT_ID FROM YM_EQUIPMENT'
      q, qargs = @db._q_restriction(q, qargs, 'EQUIPMENTID', params[:eqp])
      q += ' ORDER BY EQUIPMENTID'
      res = @db.select(q, *qargs) || return
      return res.collect {|r| YmEqp.new(*r)}
    end

    # return array of YmProd structs
    def ym_prod(params={})
      qargs = []
      q = 'SELECT NAME, PRODUCT_ID FROM YM_PRODUCT'
      if product = params[:product]
        q, qargs = @db._q_restriction(q, qargs, 'NAME', "#{product}.%")
      end
      q, qargs = @db._q_restriction(q, qargs, 'PRODUCT_ID', params[:productid])
      q += ' ORDER BY NAME'
      res = @db.select(q, *qargs) || return
      return res.collect {|r| YmStep.new(*r)}
    end

    # return array of YmStep structs
    def ym_step(params={})
      qargs = []
      q = 'SELECT NAME, STEP_ID, PRODUCT_ID FROM YM_STEP'
      if op = params[:op]
        q, qargs = @db._q_restriction(q, qargs, 'NAME', "#{op}.%")
      end
      q, qargs = @db._q_restriction(q, qargs, 'PRODUCT_ID', params[:productid])
      q += ' ORDER BY NAME'
      res = @db.select(q, *qargs) || return
      return res.collect {|r| YmStep.new(*r)}
    end

    # return array of YmLot structs
    def ym_lot(params={})
      qargs = []
      q = 'SELECT LOTID, LOT_ID, PRODUCT_ID, DATETIME, END_TIME FROM YM_LOT'
      q, qargs = @db._q_restriction(q, qargs, 'LOTID', params[:lot])
      q += ' ORDER BY LOTID'
      res = @db.select(q, *qargs) || return
      return res.collect {|r| YmLot.new(*r)}
    end

    # return array of YmWafer structs
    def ym_wafer(params={})
      qargs = []
      q = 'SELECT WAFERID, SLOTNUM, WAFER_ID, LOT_ID, PRODUCT_ID FROM YM_WAFER'
      q, qargs = @db._q_restriction(q, qargs, 'LOT_ID', params[:lotid])
      q, qargs = @db._q_restriction(q, qargs, 'SLOTNUM', params[:slot])
      q, qargs = @db._q_restriction(q, qargs, 'WAFERID', params[:wafer])
      if lot = params[:lot]
        q, qargs = @db._q_restriction(q, qargs, 'WAFERID',  "#{lot}/%")
      end
      q += ' ORDER BY WAFERID'
      res = @db.select(q, *qargs) || return
      return res.collect {|r| YmWafer.new(*r)}
    end

    # return number of defects
    def ym_defects(params={})
      qargs = []
      q = 'SELECT count(SEQUENTIALNO) FROM YM_DEFECT_TYPE_DTL'
      q, qargs = @db._q_restriction(q, qargs, 'STEP_ID', params[:stepid])
      q, qargs = @db._q_restriction(q, qargs, 'LOT_ID', params[:lotid])
      q, qargs = @db._q_restriction(q, qargs, 'WAFER_ID', params[:waferid])
      q, qargs = @db._q_restriction(q, qargs, 'PK_DATE', params[:timestamp])
      res = @db.select(q, *qargs) || return
      res.first.first
    end

  end

end
