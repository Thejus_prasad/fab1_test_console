=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author:     ssteidte, 2015-04-27

Version:    1.2.2

History:
  2016-08-24 sfrieske, use lib rqrsp_mq instead of rqrsp
  2017-12-13 sfrieske, separate from TrigCCE jCAP test lib
  2019-07-31 sfrieske, remove dependency on Builder
  2021-08-31 sfrieske, requiring 'time' for #iso8601
=end

require 'time'
require 'rexml/document'
require 'siview'
require 'rqrsp_mq'


# CCE test support
module CCE

  # CCE app testing
  class App
    attr_accessor :sv, :mq, :_doc

    def initialize(env, params={})
      @sv = params[:sv] || SiView::MM.new(env)
      # the TrigCCE job must not be connected to the request queue; there is no correlation
      @mq = RequestResponse::MQXML.new("cce.#{env}", timeout: 30)
    end

    # send lot data to CCE when lot has reached the trigger PD, act on behalf of the TrigCCE job
    #
    # no response, the msg and correlation IDs are returned on sucess
    def send_trigger(lot, params={})
      $log.info "send_trigger #{lot.inspect}"
      li = @sv.lot_info(lot) unless params[:route] && params[:opNo] && params[:op]
      @_doc = doc = REXML::Document.new('<?xml version="1.0" encoding="UTF-8"?>')
      tag = doc.add_element('soapenv:Envelope', {
        'xmlns:ns1'=>'http://www.globalfoundries.com/jcap/trigcertcreatetocce'
      }).add_element('soapenv:Body').add_element('ns1:sendTrigger')
      tag.add_element('ns1:Lot_Id').text = lot
      tag.add_element('ns1:MainPd_Id').text = params[:route] || li.route
      tag.add_element('ns1:Op_No').text = params[:opNo] || li.opNo
      tag.add_element('ns1:Pd_Id').text = params[:op] || li.op
      xml = String.new
      doc.write(xml)
      return params[:nosend] ? xml : @mq.mq_request.send_msg(xml)
    end

    # receive a CCE response msg, sent from CCE to the TrigCertCE jCAP job and validate it
    #
    # return true on success, false on 'failed' and nil in case of no response
    def cce_response(lot)
      $log.info "receive_cce_response #{lot.inspect}"
      @response = @mq.mq_response.receive_msg(timeout: 5) || ($log.warn "  no message received"; return)
      resp = XMLHash.xml_to_hash(@response)
      m = resp[:cceResponse] || ($log.warn "wromg msg:\n#{resp.inspect}"; return)
      ($log.warn "  wrong lot: #{m[:Lot_Id]}"; return false) if m[:Lot_Id] != lot
      if m[:Return_Code] != 'Success'
        $log.warn "  failed: #{m.pretty_inspect}"
        $log.warn "  CCE is probably not connected to SiView" if m[:Comment].start_with?('Invalid lot id in SiView')
        return false
      else
        return true
      end
    end

  end

  # sends LotEvents on backup lot arrival, as MDSEventHandler
  class LotEvent
    attr_accessor :mq

    def initialize(env)
      @mq = MQ::JMS.new("ccelotevent.#{env}")
    end

    def lot_event(lot, params={})
      h = {cceLotEvent: {lotId: lot, claimTime: Time.now.utc.iso8601(3), eventId: 'ReceiveForSend'}}
      return @mq.send_msg(XMLHash.hash_to_xml(h))
    end
  end

end
