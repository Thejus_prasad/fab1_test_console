# encoding: UTF-8

=begin
(c) Copyright 2012 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: Steffen Steidten, 2013-01-31

History:
  2015-06-29 ssteidte, removed unused pdwhdb
  2015-10-05 sfrieske, moved claim_process_lot_to from siview here (only use case)
  2015-11-02 sfrieske, inherit from SiView::Test
  2015-12-08 sfrieske, added back pdwhdb for regression tests
  2018-03-06 sfrieske, minor cleanup
  2021-02-11 sfrieske, removed unused parameter :srclot_ondemand
  2021-08-27 sfrieske, require waitfor instead of misc
=end

require 'siviewtest'
require 'util/waitfor'
require_relative 'pdwhdb'


# Create/Run PDWH Scenarios
module PDWH

  class Test < SiView::Test
    ClaimProcessStep = Struct.new(:route, :module, :op, :opNo)  # for claim_process_lot_to, deprecated

    attr_accessor :lldb, :loader_timeout, :opNo_pre_erf, :opNo_erf, :opNo_erf_return,
      :subroutes, :hold_reason, :release_reason, :scenario_flags, :scenario_lots

    def initialize(env, params={})
      super(env, {carriers: 'PDWH%', product: 'PDWHPROD5.01', route: 'P-PDWH-MAINPD.01', newlot_delay: 5, nonprobank: 'O-PACK'}.merge(params))
      @lldb = PDWH::LLDB.new(params[:dbenv] || env, params)
      @loader_timeout = params[:loader_timeout] || 1800  # for 'stress' DB typically ~ 10min
      @hold_reason = 'ENG'
      @release_reason = 'ALL'
      # GatePass PD
      @opNo_gatepass = '1000.1000'
      # bankin and ship
      @bank_ship = 'OY-SHIP'
      # ERF / branch PDs
      @opNo_pre_erf = '2000.1100'
      @opNo_erf = '2000.1200'
      @opNo_erf_return = '4000.1000'
      # rework PDs
      @opNo_rwk = '3000.1000'
      @opNo_rwk_return = '2000.1300'
      @opNo_erf_rwk = '1000.1100'
      @opNo_erf_rwk_return = '1000.1000'
      # subroutes
      @subroutes = {
        branch: {
          static: {layer: 'e2PDWH-STAGEID.01', nolayer: 'e1PDWH-NOSTAGEID.01'},
          dynamic: {layer: 'e2PDWH-DSTAGEID.01', nolayer: 'e1PDWH-DNOSTAGEID.01'}
        },
        rework: {
          static: {layer: 'P-PDWH-RWK-STAGEID.01', nolayer: 'P-PDWH-RWK-NOSTAGEID.01'},
          dynamic: {layer: 'P-PDWH-DRWK-STAGEID.01', nolayer: 'P-PDWH-DRWK-NOSTAGEID.01'}
        }
      }
      @scenario_flags = {
        erf: [:erf_partial, :erf_dynamic, :erf_layer],
        rework: [:rwk_partial, :rwk_dynamic, :rwk_layer],
        erf_rework: [:erf_partial, :erf_dynamic, :erf_layer, :rwk_partial, :rwk_dynamic, :rwk_layer]
      }
      @memos = ['ÄÖÜäöü', 'szß', '§para', '$dollar', '%', 'and&', '\'"\'"', '[](){}', '.,:;-_#']
    end

    def new_lot(params={})
      ts = Time.now.utc.strftime('%F_%T').gsub(':', '-')
      return super({user_parameters: {'DevProjectID'=>"QADEV#{ts}", 'ProgramID'=>"QAPID#{ts}"}}.merge(params))
    end

    # do all typical lot activities, incl GatePass, OpeComplete and Ship
    #
    # return true on success
    def lot_activities(lot, params={})
      $log.info "lot_activities #{lot.inspect}"
      gatepass_done = false
      opecomp_done = false
      forceopecomp_done = false
      ship_done = false
      @sv.lot_operation_list(lot, forward: true, history: false, current: true).each {|rop|
        $log.info "-- #{rop.opNo}  #{rop.op}"
        if rop.opNo == @opNo_gatepass
          # gatepass
          res = @sv.lot_gatepass(lot)
          return false if res != 0
          gatepass_done = true
        elsif rop.op.start_with?('LOTSHIP')
          if params[:noship]
            break
          elsif params[:scrap]
            res = @sv.scrap_wafers(lot)
            return nil if res != 0
            res = @sv.wafer_sort(lot, '')
            return nil if res != 0
            break
          else
            res = @sv.lot_gatepass(lot)
            return false if res != 0
            gatepass_done = true
          end
        elsif rop.op.start_with?('BANKIN')
          # bankin and ship
          @sv.lot_bankin(lot) # may occur automatically -> don't check
          res = @sv.lot_bank_move(lot, @bank_ship)
          return nil if res != 0
          res = @sv.lot_ship(lot, @bank_ship)
          return nil if res != 0
        elsif !forceopecomp_done
          # foceopecomp
          @sv.claim_process_lot(lot, running_hold: true) || return
          forceopecomp_done = true
          sleep 1
          @sv.lot_hold_release(lot, 'FCHL')
        else
          # opecomp
          @sv.claim_process_lot(lot) || return
          opecomp_done = true
        end
      }
      $log.warn "no gatepass executed" unless gatepass_done
      $log.warn "no opecomp executed" unless opecomp_done
      $log.warn "no forceopecomp executed" unless forceopecomp_done
      return gatepass_done && opecomp_done && forceopecomp_done
    end

    # execute actions, typically on an ERF or rework route
    #
    # return true on success
    def lot_action(lot, params={})
      if params[:holdrelease]
        res = @sv.lot_hold(lot, @hold_reason)
        return nil if res != 0
        res = @sv.lot_hold_release(lot, @hold_reason, memo: params[:memo], reason: @release_reason)
        return nil if res != 0
      end
      if params[:bankinout]
        res = @sv.lot_nonprobankin(lot, @nonprobank, memo: params[:memo])
        return nil if res != 0
        sleep 1
        res = @sv.lot_nonprobankout(lot, memo: params[:memo])
        return nil if res != 0
      end
      if params[:interaction]
        puts "\n\nProzessing of lot #{lot.inspect} paused. Press ENTER to continue."
        gets
      end
      action = params[:action]
      if action
        res = action.call || return
      end
      return true
    end

    # branch
    #
    # select route by params :dynamic and :layer from erf routes
    #
    # return lot id on success, else nil
    def branch(lot, params={})
      $log.info "\n-- branch #{lot.inspect}, #{params.inspect}"
      res = @sv.lot_opelocate(lot, @opNo_pre_erf, memo: params[:memo])
      return nil if res != 0
      # process lot at pre-branch PD
      cj = @sv.claim_process_lot(lot, params)
      return nil unless cj
      # select the branch route
      rte = @subroutes[:branch][params[:branch_dynamic] ? :dynamic : :static][params[:branch_layer] ? :layer : :nolayer]
      branch_return = (params[:branch_return] or @opNo_erf_return)
      res = @sv.lot_branch(lot, rte, :retop=>branch_return, :dynamic=>params[:branch_dynamic], memo: params[:memo])
      return nil if res != 0
      return true if params[:branch_stay]
      sleep 5 # to separate events
      if params[:branch_cancel]
        res = @sv.lot_branch_cancel(lot, memo: params[:memo])
        return nil if res != 0
        return lot
      else
        return _branch_complete(lot, params)
      end
    end

    def _branch_complete(lot, params={})
      branch_return = params[:branch_return] || @opNo_erf_return
      # process lot to the return position
      claim_process_lot_to(lot, {opNo: branch_return}.merge(params)) || return
      @sv.claim_process_lot(lot) || return
      return [lot, lot]
    end

    # ERF, the complete lot is moved to the ERF route unless :erf_partial=>true
    #
    # select route by params :dynamic and :layer
    #
    # return ERF (child) lot on success else nil
    def erf(lot, params={})
      $log.info "\n-- erf #{lot.inspect}, #{params.inspect}"
      res = @sv.lot_opelocate(lot, @opNo_pre_erf, memo: params[:memo])
      return nil if res != 0
      # attach PSM
      splitwafers = params[:erf_splitwafers] || 5
      splitwafers = @sv.lot_info(lot).nwafers unless params[:erf_partial]
      rte = @subroutes[:branch][params[:erf_dynamic] ? :dynamic : :static][params[:erf_layer] ? :layer : :nolayer]
      res = @sv.lot_experiment(lot, splitwafers, rte, @opNo_erf, @opNo_erf_return, dynamic: params[:erf_dynamic], memo: params[:memo])
      return nil if res != 0
      # process lot at pre-erf PD
      cj = @sv.claim_process_lot(lot, params) || return
      # verify lot is on ERF route
      lc = (params[:splitwafers] == :all) ? lot : @sv.lot_family(lot)[-1]
      ($log.warn "error executing PSM"; return) if @sv.lot_info(lc).route != rte
      # lot_action on ERF route
      pp = params.merge(action: params[:erf_action], interaction: params[:erf_interaction],
        holdrelease: params[:erf_holdrelease], bankinout: params[:erf_bankinout])
      lot_action(lc, pp) || return
      #
      # process ERF (child) lot to the return PD and merge back to parent
      return lc if params[:erf_stay]
      sleep 5 # to separate events
      if params[:erf_cancel]
        res = @sv.lot_branch_cancel(lc, memo: params[:memo])
        return nil if res != 0
        if params[:erf_partial]
          res = @sv.lot_futurehold_cancel(lc, nil, memo: params[:memo])
          return nil if res != 0
          res = @sv.lot_futurehold_cancel(lot, nil, memo: params[:memo])
          return nil if res != 0
          res = @sv.lot_merge(lot, lc, memo: params[:memo])
          return nil if res != 0
        end
        @sv.lot_experiment_delete(lot, memo: params[:memo])
        return lc
      else
        return _erf_complete(lot, lc, params)
      end
    end

    # complete ERF and merge lots
    #
    # return ERF (child) lot on success else nil
    def _erf_complete(lot, lc, params={})
      erf_return = params[:erf_return] || @opNo_erf_return
      # process ERF (child) lot to the return PD
      claim_process_lot_to(lc, {route: @route}.merge(params)) || return
      # remove PSM
      @sv.lot_experiment_delete(lot)
      # process parent lot to merge PD
      claim_process_lot_to(lot, {opNo: erf_return}.merge(params)) || return
      # merge
      unless params[:erf_merge] == false
        if lot != lc
          res = @sv.lot_merge(lot, lc)
          return nil if res != 0
        end
      end
      return [lot, lc]
    end

    # rework, the complete lot is moved to the rework route unless :rwk_partial=>true
    #
    # select route by params :dynamic and :layer
    #
    # return child lot on success else nil
    def rework(lot, params={})
      $log.info "\n-- rework #{lot.inspect}, #{params.inspect}"
      rwk_start = params[:rwk_start] || @opNo_rwk
      rwk_return = params[:rwk_return] || @opNo_rwk_return
      res = @sv.lot_opelocate(lot, rwk_start, memo: params[:memo])
      return nil if res != 0
      # process lot at rwk PD
      cj = @sv.claim_process_lot(lot, params) || return
      # set lot ONHOLD if :rwk_onhold (for partial reworks only)
      res = @sv.lot_hold(lot, nil, memo: params[:memo]) if params[:rwk_onhold]
      # select the rework route
      rte = @subroutes[:rework][params[:rwk_dynamic] ? :dynamic : :static][params[:rwk_layer] ? :layer : :nolayer]
      pp = {return_opNo: rwk_return, dynamic: params[:rwk_dynamic], onhold: params[:rwk_onhold], memo: params[:memo]}
      if params[:rwk_partial]
        # partial rework
        splitwafers = params[:rwk_splitwafers] || 5
        lc = @sv.lot_partial_rework(lot, splitwafers, rte, pp) || return
      else
        # full rework
        res = @sv.lot_rework(lot, rte, pp)
        return nil if res != 0
        lc = lot
      end
      ($log.warn "error executing rework"; return) if @sv.lot_info(lc).route != rte
      # lot_action on rework route
      pp = params.merge(action: params[:rwk_action], interaction: params[:rwk_interaction],
        holdrelease: params[:rwk_holdrelease], bankinout: params[:rwk_bankinout])
      lot_action(lc, pp) || return
      #
      # process child lot to the join position and merge back to parent
      return lc if params[:rwk_stay]
      sleep 5 # to separate events
      if params[:rwk_cancel]
        res = params[:rwk_partial] ? @sv.lot_partial_rework_cancel(lot, lc, memo: params[:memo]) :
                                     @sv.lot_rework_cancel(lc, memo: params[:memo])
        return nil if res != 0
        res = @sv.lot_hold_release(lot, nil, memo: params[:memo]) if params[:rwk_onhold]
        return nil if res != 0
        return lc
      else
        return _rework_complete(lot, lc, params)
      end
    end


    # complete rework and merge lots
    #
    # return reworked (child) lot on success else nil
    def _rework_complete(lot, lc, params={})
      [lot, lc].each {|l|
        res = @sv.lot_hold_release(l, nil, memo: params[:memo])
        return nil if res != 0
      } if params[:rwk_onhold]
      rwk_start = params[:rwk_start] || @opNo_rwk
      # process child lot to the parent's position
      claim_process_lot_to(lc, {opNo: rwk_start}.merge(params)) || return
      claim_process_lot(lc) || return
      # merge
      unless params[:rwk_merge] == false
        if lot != lc
          res = @sv.lot_merge(lot, lc)
          return nil if res != 0
        end
      end
      return [lot, lc]
    end

    # combined scenarios

    def erf_rework(lot, params={})
      $log.info "\n-- erf_rework lot #{lot.inspect}, #{params.inspect}"
      lc = erf(lot, params.merge(erf_stay: true))
      lci = @sv.lot_info(lc)
      rwk_params = {rwk_start: @opNo_erf_rwk, rwk_return: @opNo_erf_rwk_return, route_opNo: [lci.route, @opNo_erf_rwk]}
      rwk_params[:rwk_splitwafers] ||= (lci.nwafers / 2).to_i
      lcc = rework(lc, rwk_params.merge(params)) || return
      # return with lc still on the ERF route if params[:erf_stay]
      return [lot, lc, lcc] if params[:erf_stay]
      # complete ERF
      res = _erf_complete(lot, lc, params)
      return res ? [lot, lc, lcc] : nil
    end

    def erf_rework_scrap_parent(lot, params={})
      $log.info "\n-- erf_rework_scrap_parent lot #{lot.inspect}, #{params.inspect}"
      scrap_p = proc {
        res = @sv.scrap_wafers(lot)
        return nil unless res == 0
        res = @sv.wafer_sort(lot, '')
        return (res == 0)
      }
      pp = {rwk_action: scrap_p, erf_partial: true, erf_bankinout: true, erf_holdrelease: true,
        rwk_partial: true, rwk_bankinout: true, rwk_holdrelease: true, erf_merge: false}
      erf_rework(lot, pp.merge(params))
    end

    # create lots for each scenario combination
    #
    # return hash of scenarios with lots and parameters
    def run_scenarios(scenario, params={})
      if scenario == :all
        # run all scenarios
        @scenario_lots = {}
        @scenario_flags.keys.each {|s| scenario_lots[s] = run_scenarios(s, params)}
        return scenario_lots
      end
      #
      args = @scenario_flags[scenario]
      $log.info "---- running scenarios #{scenario.inspect} #{args.inspect}"
      # expand args combinations
      h0 = Hash[args.collect {|e| [e, false]}]
      aa = []
      (args.size+1).times {|i| aa += args.combination(i).to_a}
      args_expanded = aa.collect {|a|
        h = h0.clone
        a.each {|e| h[e] = true}
        h
      }
      # run all combinations of a scenario
      ret = {}
      args_expanded.each_with_index {|flags, i|
        ret[flags] = send(scenario, params[:lot] || new_lot, flags.merge(memo: @memos[i % @memos.size]))
      }
      show_scenarios(params) if params[:show]
      return ret
    end

    # visualization of scenarios
    def show_scenarios(params={})
      results = (params[:data] or @scenario_lots)
      ret = "PDWH Scenarios\n"
      ret += "==============\n\n"
      results.each_pair {|scenario, sdata|
        if scenario == :erf
          ret += "ERF\n---\n"
          ret += "Partial\tDynamic\tLayer\n"
        elsif scenario == :rework
          ret += "Rework\n------\n"
          ret += "Partial\tDynamic\tLayer\n"
        else
          ret += "ERF and Rework\n--------------\n"
          ret += "Partial\tDynamic\tLayer\tPartial\tDynamic\tLayer\n"
        end
        flags = @scenario_flags[scenario]
        sdata.each_pair {|sflags, lots|
          ret += flags.collect {|f| sflags[f] ? "x" : "-"}.join("\t")
          ret += "\t#{lots.inspect}\n"
        }
        ret += "\n\n"
      }
      #
      fname = params[:export]
      File.binwrite(fname, ret) if fname
      return ret
    end

    # claim processing a lot along a route until a specific operation or operation number is reached
    # end conditions are passed as parameter :module, :op or :opNo
    # note: the lot is not processed at the end operation/opNo
    # correct setup, equipment availability etc. must be ensured before calling this method!
    #
    # return array of processed operations plus the current (end) operation or nil on error
    def claim_process_lot_to(lot, params)
      $log.info "-- claim_process_lot_to #{lot.inspect}, #{params.inspect}"
      ret = []
      loop {
        # current step data
        li = lot_info(lot)
        mod = AMD_modulepd(lot)
        ret << ClaimProcessStep.new(li.route, mod, li.op, li.opNo)
        # check end condition
        if params.has_key?(:route_opNo)
          return ret if li.route == params[:route_opNo][0] and li.opNo == params[:route_opNo][1]
        else
          return ret if params.has_key?(:route) and li.route == params[:route]
          return ret if params.has_key?(:module) and mod == params[:module]
          return ret if params.has_key?(:op) and li.op == params[:op]
          return ret if params.has_key?(:opNo) and li.opNo == params[:opNo]
        end
        # next process step
        res = params[:gatepass] ? lot_gatepass(lot, params).zero? : claim_process_lot(lot, params)
        return unless res
      }
    end

    # wraps wait_for with default parameters and a log, a block must be given
    def wait_loader(params={}, &blk)
      loader_timeout = params[:loader_timeout] || @loader_timeout
      $log.info "waiting up to #{loader_timeout / 60} min for the data to get loaded"
      wait_for(sleeptime: (params[:sleeptime] || 60), timeout: loader_timeout, tstart: params[:tstart]) {blk.call}
    end

    # wait for the current batch run to complete if running and for the next one to complete
    def wait_next_batch_run(batch, params={})
      loader_timeout = params[:loader_timeout] || @loader_timeout
      $log.info "waiting up to #{loader_timeout / 60} min for the #{batch} loader"
      run = nil
      tstart = params[:tstart] || (Time.now + 20)  # the batches cut off the last 10s of the GG replication
      wait_for(sleeptime: (params[:sleeptime] || 60), timeout: loader_timeout, tstart: params[:tstart]) {
        $log.info {@lldb.etl_batch_run(batch, tstart: tstart).last} if params[:verbose]
        run = @lldb.etl_batch_run(batch, tstart: tstart, status: ['FINISHED', 'NO_NEW_DATA']).last
      }
      run
    end

  end

end
