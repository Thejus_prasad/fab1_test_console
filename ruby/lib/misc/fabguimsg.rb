=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author:   ssteidte, 2009-02-26
Version:  2.1.2

History:
  2013-11-13 ssteidte, fixed ei_ping after transition to XMLHash
  2016-08-24 sfrieske, use lib rqrsp_mq and _http instead of rqrsp
  2018-07-17 sfrieske, stripped down with focus on FabGui testing
  2019-07-02 sfrieske, added 'new' interface
  2020-01-16 sfrieske, added ICase interface (FabGUI acting as client)
  2021-02-05 sfrieske, use instance_of instead of kind_of
  2021-08-03 sfrieske, start to use rexml/document directly
  2021-08-27 sfrieske, require uniquestring instead of misc
  2021-10-28 aschmid3, added 'tool_log_handle_message' and deactivated 'tool_log' at class Client and Client2
  2022-01-12 sfrieske, ICase uses RequestResponse::MQ instead of RequestResponse::MQXML
=end

require 'time'  # for iso8601
require 'rexml/document'
require 'rqrsp_mq'
require 'util/uniquestring'
require 'util/xmlhash'


# FabGUI Testing
module FabGUI

  # Control Center messages
  class Client
    attr_accessor :mq, :_doc

    def initialize(env, params={})
      @mq = RequestResponse::MQXML.new("fabgui.#{env}", params)
    end

    def inspect
      "#<#{self.class.name}  #{@mq.inspect}>"
    end

    # send EI ping, return true on sucess
    def eiping(eqp, params={})
      runID = params[:runID] || "#{eqp}@#{Time.now.iso8601}"
      $log.info "eiping #{eqp.inspect}, runID: #{runID.inspect}"
      @_doc = doc = REXML::Document.new('<?xml version="1.0" encoding="UTF-8"?>')
      tag = doc.add_element('soapenv:Envelope', {
        'xmlns:soapenv'=>'http://schemas.xmlsoap.org/soap/envelope/',
        'xmlns:ns'=>'http://services.soap.amd.com'
      })
      .add_element('soapenv:Body', {'soapenv:encodingStyle'=>'http://schemas.xmlsoap.org/soap/encoding/'})
      .add_element('ns:eiPing')
      tag.add_element('theEntity').text = eqp
      tag.add_element('theRunID').text = runID
      res = @mq.send_receive(tag.document) || return
      ($log.warn res.inspect; return) unless res[:eiPingResponse] && res[:eiPingResponse][:eiPingReturn][nil] == 'OK'
      return true
    end

    # sends manual DC input form
    def handle_message(eqp, params={})
      session = params[:session] || 'All'
      msgtimeout = params[:msgtimeout] || 50
      taid = params[:taid] || unique_id  # Long
      text = params[:text] || 'Entry screen for manual data collection'
      $log.info "handle_message #{eqp.inspect}, session: #{session.inspect}"
      data = [
        {key: 'Source', value: {'xsi:type'=>'ns0:string', nil=>params[:source] || 'QA Test'}},  # EI sends EI/MQ
        {key: 'TAID', value: {'xsi:type'=>'ns0:long', nil=>taid}},
        {key: 'Timestamp', value: {'xsi:type'=>'ns0:string', nil=>Time.now.utc.iso8601}},
        {key: 'Handler', value: {'xsi:type'=>'ns0:string', nil=>'Query'}},
        {key: 'Timeout', value: {'xsi:type'=>'ns0:string', nil=>msgtimeout * 1000}},  # ms
        {key: 'Display', value: {'xsi:type'=>'ns0:string', nil=>session}},
        {key: 'Text', value: {'xsi:type'=>'ns0:string', nil=>text}},
        {key: 'Layout', value: {'xsi:type'=>'ns0:string', nil=>'S'}},
        {key: 'Popup', value: {'xsi:type'=>'ns0:string', nil=>params[:popup] ? 'Y' : 'N'}},
        {key: 'Parameter', value: {
          'xsi:type'=>'ns1:Vector', item: {'xsi:type'=>'ns1:Map', item: [
            {key: 'Value', value: ''},
            {key: 'Description', value: ''},
            {key: 'Label', value: 'Input'},
            {key: 'Range', value: ''},
            {key: 'Class', value: 'TextField'},
            {key: 'Size', value: 22},
            {key: 'Type', value: ''},
            {key: 'ReadingID', value: 3},
            {key: 'Unit', value: 'string'},
            {key: 'Default', value: params[:input] || "QA#{taid}"},
            {key: 'LabelAlign', value: 'L2'}
          ]}
        }}
      ]
      h = {'soapenv:Envelope': {
        'xmlns:soapenv'=>'http://schemas.xmlsoap.org/soap/envelope/',
        'xmlns:ns0'=>'http://www.w3.org/2001/XMLSchema',
        'xmlns:ns1'=>'http://xml.apache.org/xml-soap',
        'xmlns:xsi'=>'http://www.w3.org/2001/XMLSchema-instance',
        'soapenv:Body': {handleMessage: {
          'soapenv:encodingStyle'=>'http://schemas.xmlsoap.org/soap/encoding/',
          theTarget: eqp, theTAID: taid, theData: {properties: {'xsi:type'=>'SOAP-ENC:Array', item: data}}
        }}
      }}
      res = @mq.send_receive(h, timeout: 120) || return
      ret = (res[:handleMessageResponse][:handleMessageReturn][nil] == 'NOWAIT')
      $log.warn res.inspect unless ret
      return ret
    end

    def simple_query(eqp, msg, params={})
      session = params[:session] || 'All'
      $log.info "simple_query #{eqp.inspect}, #{msg.inspect}, session: #{session.inspect}"
      h = {'soapenv:Envelope': {
        'xmlns:soapenv'=>'http://schemas.xmlsoap.org/soap/envelope/',
        'soapenv:Body': {viewSimpleQuery: {
          conDesc: {
            displayGroup: params[:displaygroup] || 'All',    # 'true' ? no influence
            originatorID: params[:originator] || 'QA Test',
            sessionID: session,
            timeout: (params[:msgtimeout] || 50) * 1000,
            toolID: eqp,
            userID: params[:user] || 'QATest'
          },
          message: msg
        }}
      }}
      res = @mq.send_receive(h, timeout: params[:timeout] || 60) || return
      ret = res[:viewSimpleQueryResponse][:viewSimpleQueryReturn][nil]
      $log.warn res.inspect unless ret
      return ret  # may return TOOL.TIMEOUT
    end

    ## Used FabGUI interface 'processLogToolLog' is outdated. PROD is using FabGUI interface 'handleMessage' (status: 2021-10-20)
    ## Contact: Uwe Liebold, Stephan Melzig (EI-Baseline and EI-Models)
    # like simple_query but logs only
    def XXXtool_log(eqp, msg, params={})
      session = params[:session] || 'All'
      $log.info "tool_log #{eqp.inspect}, #{msg.inspect}, session: #{session.inspect}"
      h = {'soapenv:Envelope': {
        'xmlns:soapenv'=>'http://schemas.xmlsoap.org/soap/envelope/',
        'soapenv:Body': {processLogToolLog: {
          conDesc: {
            displayGroup: params[:displaygroup] || 'All',    # 'true' ? no influence
            originatorID: params[:originator] || 'QA Test',
            sessionID: session,
            timeout: (params[:msgtimeout] || 50) * 1000,
            toolID: eqp,
            userID: params[:user] || 'QATest'
          },
          message: msg
        }}
      }}
      res = @mq.send_receive(h, timeout: params[:timeout] || 30) || return
      ret = (res[:processLogToolLogResponse][:processLogToolLogReturn][nil] == 'OK')
      $log.warn res.inspect unless ret
      return ret
    end

    # new 'tool_log' with using of FabGUI interface 'handleMessage' (status: 2021-10-20)
    def tool_log_handle_message(eqp, msg, params={})
      taid = params[:taid] || unique_id  # separat definition required - used 2times and must be unique
      $log.info "tool_log_handle_message #{eqp.inspect}, #{msg.inspect}"
      @_doc = doc = REXML::Document.new('<?xml version="1.0" encoding="UTF-8"?>')
      btag = doc.add_element('soapenv:Envelope', {
        'xmlns:soapenv'=>'http://schemas.xmlsoap.org/soap/envelope/',
        'xmlns:SOAP-ENC'=>'http://schemas.xmlsoap.org/soap/encoding/',
        'xmlns:xsd'=>'http://www.w3.org/2001/XMLSchema',
        'xmlns:ns'=>'http://services.soap.amd.com',
        'xmlns:ns1'=>'de.systemagmbh.ei.soapmq',
        'xmlns:xsi'=>'http://www.w3.org/2001/XMLSchema-instance',
      })
      .add_element('soapenv:Body')
      mtag = btag.add_element('ns:handleMessage', {'soapenv:encodingStyle'=>'http://schemas.xmlsoap.org/soap/encoding/'})
      mtag.add_element('theTarget').text = eqp
      mtag.add_element('theTAID').text = taid
      dtag = mtag.add_element('theData')
      ptag = dtag.add_element('properties', {'SOAP-ENC:arrayType'=>'ns1:MsgAttribute[6]', 'xsi:type'=>'SOAP-ENC:Array'})
      #
      itag0 = ptag.add_element('item')
      itag0.add_element('key').text = 'Source'
      itag0.add_element('value', {'xsi:type'=>'xsd:string'}).text = params[:source] || 'QA Test'   # EI sends EI/MQ
      itag1 = ptag.add_element('item')
      itag1.add_element('key').text = 'Timestamp'
      itag1.add_element('value', {'xsi:type'=>'xsd:string'}).text = Time.now.utc.strftime('%d-%b-%Y %H:%M:%S')
      itag2 = ptag.add_element('item')
      itag2.add_element('key').text = 'Handler'
      itag2.add_element('value', {'xsi:type'=>'xsd:string'}).text = 'ToolLog'
      itag3 = ptag.add_element('item')
      itag3.add_element('key').text = 'Category'
      itag3.add_element('value', {'xsi:type'=>'xsd:string'}).text = params[:session] || 'undefined'
      itag4 = ptag.add_element('item')
      itag4.add_element('key').text = 'Text'
      itag4.add_element('value', {'xsi:type'=>'xsd:string'}).text = msg
      itag5 = ptag.add_element('item')
      itag5.add_element('key').text = 'TAID'
      itag5.add_element('value', {'xsi:type'=>'xsd:long'}).text = taid
      #
      res = @mq.send_receive(btag.document, timeout: 30) || return
      ($log.warn res.inspect; return) unless res[:handleMessageResponse][:handleMessageReturn][nil] == 'OK'
      return true
    end

    def extended_query(eqp, msg, params={})
      session = params[:session] || 'All'
      $log.info "extended_query #{eqp.inspect}, #{msg.inspect}, session: #{session.inspect}"
      h = {'soapenv:Envelope': {
        'xmlns:soapenv'=>'http://schemas.xmlsoap.org/soap/envelope/',
        'xmlns:xsd'=>'http://www.w3.org/2001/XMLSchema',
        'xmlns:xsi'=>'http://www.w3.org/2001/XMLSchema-instance',
        'soapenv:Body': {
          viewExtendedQuery: {
            conDesc: {
              displayGroup: true,
              originatorID: params[:originator] || 'QA Test',
              sessionID: params[:session] || 'All',
              timeout: (params[:msgtimeout] || 50) * 1000,
              toolID: eqp,
              userID: params[:user] || 'QATest'
            },
            message: msg,
            guiElements: {item: [
              {ID: 0, parameters: {'href'=>'#id1'}},
              {ID: 1, parameters: {'href'=>'#id2'}},
              {ID: 2, parameters: {'href'=>'#id3'}},
            ]},
            buttons: {item: [
              {default: false, label: 'OK'},
              {default: true, label: 'Cancel'}
            ]}
          },
          multiRef: [
            _soap_map(10, 1, {'Value'=>msg, 'Color'=>'red', 'Class'=>'TextField', 'Label'=>'Input'}),
            _soap_map(10, 2, {'Value'=>'true', 'Class'=>'CheckBox', 'Label'=>'Check'}),
            _soap_map(10, 3, {'Value'=>'Debug', 'List'=>'Trace|Debug|Verbose', 'Class'=>'ComboBox', 'Label'=>'Select'})
          ]
        }
      }}
      #
      res = @mq.send_receive(h, timeout: params[:timeout] || 60) || return
      $log.info res.inspect
      ret = res[:viewExtendedQueryResponse][:viewExtendedQueryReturn]
      $log.warn res.inspect unless ret
      return ret
    end

    # return a SOAP multiRef element for extended_query
    def _soap_map(ns, id, kvhash, params={})
      return {
        'id'=>id,
        'soapenc:root'=>0,
        'soapenv:encodingStyle'=>'http://schemas.xmlsoap.org/soap/encoding/',
        'xsi:type'=>"ns#{ns}:Map",
        "xmlns:ns#{ns}"=>'http://xml.apache.org/xml-soap',
        'xmlns:soapenc'=>'http://schemas.xmlsoap.org/soap/encoding/',
        item:  kvhash.each_pair.collect {|k, v|
          {key: {'xsi:type'=>'soapenc:string', nil=>k}, value: {'xsi:type'=>'soapenc:string', nil=>v}}
        }
      }
    end

  end


  # Control Center message new interface, different MQ msg structure
  class Client2
    attr_accessor :mq, :_doc

    def initialize(env, params={})
      @mq = RequestResponse::MQXML.new("fabgui2.#{env}", params)
    end

    def inspect
      "#<#{self.class.name}  #{@mq.inspect}>"
    end

    # send EI ping, return true on sucess
    def eiping(eqp, params={})
      runID = params[:runID] || "#{eqp}@#{Time.now.iso8601}"
      $log.info "eiping #{eqp.inspect}, runID: #{runID.inspect}"
      @_doc = doc = REXML::Document.new('<?xml version="1.0" encoding="UTF-8"?>')
      tag = doc.add_element('soapenv:Envelope', {
        'xmlns:soapenv'=>'http://schemas.xmlsoap.org/soap/envelope/', 'xmlns:ns'=>'urn:FabGUI'
      })
      .add_element('soapenv:Body')
      .add_element('ns:eiPingRequest', {'xmlns:ns'=>'urn:FabGUI'})
      tag.add_element('entity').text = eqp
      tag.add_element('runId').text = runID
      res = @mq.send_receive(tag.document, timeout: params[:timeout] || 20) || return

      ($log.warn res.inspect; return) unless res[:eiPingResponse] && res[:eiPingResponse][nil] == 'OK'
      return true
    end

    # sends manual DC input form - NOT implemented in FabGUI'S new interface
    def handle_message(eqp, params={})
      $log.warn "handle_message is not implemented in the new FabGUI msg interface"
      return false
    end

    # return the query response, e.g. OK, Cancel, TOOL.TIMEOUT or nil on error
    def simple_query(eqp, msg, params={})
      session = params[:session] || 'All'
      $log.info "simple_query #{eqp.inspect}, #{msg.inspect}, session: #{session.inspect}"
      h = {'soapenv:Envelope': {
        'xmlns:soapenv'=>'http://schemas.xmlsoap.org/soap/envelope/', 'xmlns:ns'=>'urn:FabGUI',
        'soapenv:Body': {'ns:viewSimpleQueryRequest': {
          conDesc: {
            displayGroup: params[:displaygroup] || 'All',    # 'true' ? no influence
            originatorID: params[:originator] || 'QA Test',
            sessionID: session,
            timeout: (params[:msgtimeout] || 50) * 1000,
            toolID: eqp,
            userID: params[:user] || 'QATest'
          },
          message: msg
        }}
      }}
      res = @mq.send_receive(h, timeout: params[:timeout] || 60) || return
      ($log.warn res.inspect; return) unless res[:viewSimpleQueryResponse] && res[:viewSimpleQueryResponse][nil]
      return res[:viewSimpleQueryResponse][nil]
    end

    ## Used FabGUI interface 'processLogToolLog' is outdated. PROD is using FabGUI interface 'handleMessage' (status: 2021-10-20)
    def XXXtool_log(eqp, msg, params={})
      session = params[:session] || 'All'
      $log.info "tool_log #{eqp.inspect}, #{msg.inspect}, session: #{session.inspect}"
      h = {'soapenv:Envelope': {
        'xmlns:soapenv'=>'http://schemas.xmlsoap.org/soap/envelope/', 'xmlns:ns'=>'urn:FabGUI',
        'soapenv:Body': {'ns:processLogToolLogRequest': {
          conDesc: {
            displayGroup: params[:displaygroup] || 'All',    # 'true' ? no influence
            originatorID: params[:originator] || 'QA Test',
            sessionID: session,
            timeout: (params[:msgtimeout] || 50) * 1000,
            toolID: eqp,
            userID: params[:user] || 'QATest'
          },
          message: msg
        }}
      }}
      res = @mq.send_receive(h, timeout: params[:timeout] || 30) || return
      ($log.warn res.inspect; return) unless res[:processLogToolLogResponse] && res[:processLogToolLogResponse][nil]
      return res[:processLogToolLogResponse][nil]
    end

    # FabGUI interface 'handleMessage' is not implemented in FabGUI's new interface
    def tool_log_handle_message(eqp, msg, params={})
      $log.warn "tool_log_handle_message is not implemented in the new FabGUI msg interface"
      return false
    end

    def extended_query(eqp, msg, params={})
      session = params[:session] || 'All'  # Denis: nil
      guiElements = params[:guiElements] || [
        {'Value'=>'QA Message', 'Color'=>'red', 'Class'=>'TextField', 'Label'=>'Input'},
        {'Value'=>'true', 'Class'=>'CheckBox', 'Label'=>'Check'},
        {'Value'=>'Debug', 'List'=>'Trace|Debug|Verbose', 'Class'=>'ComboBox', 'Label'=>'Select'}
      ]
      $log.info "extended_query #{eqp.inspect}, #{msg.inspect}, session: #{session.inspect}"
      h = {'soapenv:Envelope': {
        'xmlns:soapenv'=>'http://schemas.xmlsoap.org/soap/envelope/', 'xmlns:ns2'=>'urn:FabGUI',
        'soapenv:Body': {
          'ns2:viewExtendedQueryRequest': {
            'xmlns:ns2'=>'urn:FabGUI',
            conDesc: {
              displayGroup: true,
              # displayGroup: {'xmlns:xsi'=>'http://www.w3.org/2001/XMLSchema-instance', 'xsi:nil'=>'true'},
              originatorID: params[:originator] || 'QA Test',
              sessionID: session,
              timeout: (params[:msgtimeout] || 50) * 1000,
              toolID: eqp,
              userID: params[:user] || 'QATest'               # Denis: 'All'
            },
            message: msg,         # Denis: HTML formatted
            guiElements: guiElements.each_with_index.collect {|e, i|
              {ID: i, parameters: e.each_pair.collect {|k, v| {name: k, value: v}}}
            },
            buttons: [
              {default: false, label: 'OK'},
              {default: true, label: 'Cancel'}
            ]
          },
        }
      }}
      #
      res = @mq.send_receive(h, timeout: params[:timeout] || 60) || return
      ($log.warn res.inspect; return) unless res[:viewExtendedQueryResponse] && res[:viewExtendedQueryResponse][:response]
      return res[:viewExtendedQueryResponse][:response]
    end

  end


  # Send a message to WebMRB/ICase as FabGUI does
  class ICase
    attr_accessor :mq

    def initialize(env, params={})
      @mq = RequestResponse::MQ.new("icase.#{env}", params)
    end

    def inspect
      "#<#{self.class.name} #{@mq.inspect}>"
    end

    def shipping_availability(lot)
      $log.info "shipping_availability #{lot.inspect}"
      # TODO: use rexml/document !
      h = {'soapenv:Envelope': {
        'xmlns:soapenv'=>'http://schemas.xmlsoap.org/soap/envelope/',
        'xmlns:tns'=>'http://tempuri.org/',
        'soapenv:Body': {'tns:GetLotShippingAvailability': {'tns:lots': {
          'xmlns:tns'=>'http://tempuri.org/',
          'ns1:string': {'xmlns:ns1'=>'http://schemas.microsoft.com/2003/10/Serialization/Arrays', nil=>lot}
        }}}
      }}
      begin
        resp = @mq.send_receive(XMLHash.hash_to_xml(h), timeout: 40) || ($log.warn 'no response'; return)
        res = XMLHash.xml_to_hash(resp)
      rescue NoMethodError
        $log.warn "wrong response:\n-> #{@mq.response}\n"
        return
      end
      res1 = res[:Envelope][:Body][:GetLotShippingAvailabilityResponse][:GetLotShippingAvailabilityResult]
      k1 = res1.keys.find {|k| k.to_s.start_with?('ArrayOfKeyValueOfstringArrayOfMRBLotReport')}
      ($log.warn "wrong key: #{k1.inspect}"; return) if k1 != :ArrayOfKeyValueOfstringArrayOfMRBLotReportSyc7bmur
      res2 = res1[k1] || ($log.warn "value of #{k1} is empty"; return)
      k2 = k1.to_s.sub('ArrayOfKeyValueOfstringArrayOfMRBLotReport', 'KeyValueOfstringArrayOfMRBLotReport').to_sym
      ret = res2[k2][:Value][:MRBLotReport]
      ($log.warn "wrong response: #{res1}"; return) if ret.nil?
      return ret.instance_of?(Array) ? ret : [ret]
    end

  end

end
