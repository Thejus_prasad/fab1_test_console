=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.


Author:  sfrieske, 2018-04-27

Version: 1.0.2

History:
  2019-01-17 sfrieske, removed dependency on builder
  2021-08-24 sfrieske, use rexml/document directly

Notes:
  Alarm ids must be configured; Alarm code is a flag: < 128: off, >= 128: on

  Alarm ids (configured actions in OCAP) TODO: still valid?
  default: 456, downtool: 457, runninghold: 458, down_hold: 459, futurehold: 460,
  disabled: 461, chamberdown2: 500, chamberdown3: 501
=end

require 'time'  # for iso8601
require 'rexml/document'
require 'mqjms'


# Test OCAP Alarm Reporting
module OCAP

  # Client sending MQ messages to OCAP like EIs do, MQ queues and OCAP config must exist
  class Alarm
    attr_accessor :eqp, :mq, :_doc

    def initialize(env, eqp)
      @eqp = eqp
      @mq = MQ::JMS.new("ocap.#{env}", reply_queue: "CEI.#{eqp}_REPLY01")
      # note: msgs _to_ EI (e.g. CANCEL_QUED_JOBS) go to "CEI.<EQP>_REQUEST01"
      $log.info self.inspect
    end

    # send msg, return true on sucessful send
    def submit(alid, params={})
      $log.info "submit #{alid}, #{params}"
      @_doc = doc = REXML::Document.new('<?xml version="1.0" encoding="UTF-8"?>')
      tag = doc.add_element('SOAP-ENV:Envelope', {
        'xmlns:OCAPServices'=>'http://www.globalfoundries.com/OCAPServices',
        'xmlns:SOAP-ENV'=>'http://schemas.xmlsoap.org/soap/envelope/'
      })
      .add_element('SOAP-ENV:Body')
      .add_element('OCAPServices:SendAlarmInfo', {
        'SOAP-ENV:encodingStyle'=>'http://schemas.xmlsoap.org/soap/encoding/'
      })
      .add_element('AlarmInfo')
      tag.add_element('EquipID').text = @eqp
      tag.add_element('ALCode').text = params[:code] || 128
      tag.add_element('ALID').text = alid   # not actually used
      tag.add_element('ALText').text = params[:text] || "QA Test Alarm #{alid}"
      tag.add_element('ALTime').text = Time.now.utc.iso8601(6)
      tag.add_element('ALIDSymbol').text = alid
      tag.add_element('EquipE10').text = params[:e10] || 'SBY'    # other than SBY and PRD are ignored
      tag.add_element('EquipE10Sub').text = params[:e10sub] || '2WPR'
      #
      # note: OCAP cannot parse XML with newlines and spaces
      s = String.new
      tag.document.write(s)
      res = @mq.send_msg(s, bytes_msg: true)
      return !res.nil?
    end

  end

end
