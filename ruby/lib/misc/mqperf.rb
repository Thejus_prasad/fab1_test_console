=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.


Author:  sfrieske, 2019-03-06 (migrated from FICS MQ tests (Perl/C))

Version: 1.0.0

History:
  2021-08-26 sfrieske, split duration_string off of misc

Note:
  must be run on the MQ test server, typically f38asq18
  usage:
    require 'mqperf'
    perf = MQPerf.new
    perf.runtest
=end

ENV.delete('MQSERVER')

require 'fileutils'
require 'time'
require 'zlib'
require 'zip'
require 'util/charts'
require 'util/durationstring'

Thread.abort_on_exception = true   # set true for debugging only


class MQPerf
  attr_accessor :logdir, :datadir, :exedir, :ctrlfiles, :perflogs, :randomfile,
    :qprefix, :tuningfile, :mqchltab, :qmgr, :tstart, :tend, :duration,
    :waiters, :waitsperhour, :reconnectors, :reconnsperhour

  def initialize(params={})
    @logdir = 'log/mqperf'
    FileUtils.mkdir_p(@logdir)
    #
    @datadir = params[:datadir] || '/local/tmp/qa/mqperf'   # holds input and intermediate log data
    FileUtils.mkdir_p(@datadir)
    @tracefiles = "#{datadir}/tracefiles/MQPUT_RHSHRP3*gz"
    @ctrlfiles = "#{datadir}/ctrl"                          # holds per-queue msg control files
    @perflogs = "#{datadir}/perflog"
    @randomfile = "#{datadir}/randomContentFile.txt"
    @qprefix = params[:qprefix] || 'TEST.'
    @exedir = params[:exedir] || File.absolute_path('lbin/mqperf')
    @mqchltab = params[:mqchltab] || 'AMQCLCHL_f38asq12.TAB'
    @tuningfile = params[:tuningfile] || File.absolute_path('etc/mqperf/messageTuning.cfg')
    @qmgr = params[:qmgr] || 'RHSHRL3'
    @waiters = params[:waiters] || 3544
    @waitsperhour = params[:waits] || 2207
    @reconnectors = params[:reconnectors] || 15
    @reconnsperhour = params[:reconns] || 247
  end


  # test preparation

  # from scripts/currentLoad/prepareMQMsgSizeFiles.pl
  # converts MQM trace logs to per-queue control files (timing, size, flags)
  def prepare(params={})
    $log.info "MQPerf.prepare #{params}"
    tstart = Time.now
    # tracefiles
    files = Dir[@tracefiles].sort
    ($log.warn "No tracefiles (#{@tracefiles.inspect}). Wrong machine?"; return) if files.empty?
    # random msg content file
    if !File.exists?(@randomfile)
      $log.info "creating randomfile #{@randomfile}"
      chars = ['_', *'A'..'Z', *'a'..'z', *'0'..'9']
      flen = 200e6.to_i  # 200 MB
      File.open(@randomfile, 'wb') {|fh| flen.times {fh.write(chars[rand(chars.length)])} }
    end
    # fresh perflog dir
    if File.exist?(@perflogs)
      $log.info "removing old perflogs #{@perflogs}"
      FileUtils.rm_r(@perflogs)
    end
    $log.info "creating new perflogs #{@perflogs}"
    FileUtils.mkdir_p(@perflogs)
    # fresh or existing ctrl files
    ($log.info "using existing ctrlfiles"; return true) if params[:use_existing_ctrl]
    if File.exist?(@ctrlfiles)
      $log.info "removing old ctrlfiles #{@ctrlfiles}"
      FileUtils.rm_r(@ctrlfiles)
    end
    $log.info "creating new ctrlfiles #{@ctrlfiles}"
    FileUtils.mkdir_p(@ctrlfiles)
    tgtfhs = {}
    msgids = {}
    t0 = nil  # get start time in Unix seconds from first file
    Zlib::GzipReader.open(files.first) {|gz| t0 = Time.parse(gz.readline.split('|').first).to_i}
    # parse src files and write per-queue control files
    t = nil
    files.each {|pname|
      # get all msgs read (timestamp and count)
      gname = pname.sub('PUT', 'GET')
      $log.info "processing #{File.basename(gname)}"
      Zlib::GzipReader.open(gname).each_line {|line|
        ff = line.split('|')  # tstamp persistence ...
        emid = msgids[ff[13]]
        msgids[ff[13]] = emid = [Time.parse(ff.first).to_i, 0] unless emid
        emid[1] += 1  # msg read count
      }
      # get msgs put
      $log.info "processing #{File.basename(pname)}"
      Zlib::GzipReader.open(pname).each_line {|line|
        ff = line.split('|')  # tstamp persistence ...
        t = Time.parse(ff.first).to_i
        mid = ff[13]
        if emid = msgids[mid]
          mqtime = emid[0] - t
          gcount = emid[1]
          msgids.delete(mid)
        else
          mqtime = 0
          gcount = 0
        end
        qname = ff[4].upcase
        qname = 'AMQ' if qname.start_with?('AMQ')
        qname = 'COM.IBM.MQ.PCF' if qname.start_with?('COM.IBM.MQ.PCF')
        qname = 'NASTEL.EVENT' if qname.start_with?('NASTEL.EVENT')
        qname = 'NASTEL.REPLY' if qname.start_with?('NASTEL.REPLY')
        qname = 'PCFGETQSTAT' if qname.start_with?('PCFGETQSTAT')
        qname = 'PCFRESETQSTATISTIC' if qname.start_with?('PCFRESETQSTATISTIC')
        qname = 'SAVEQMGR' if qname.start_with?('SAVEQMGR')
        tgtf = File.join(@ctrlfiles, (@qprefix + qname)[0...48])
        tgtfhs[tgtf] = File.open(tgtf, 'wb') unless tgtfhs[tgtf]
        fh = tgtfhs[tgtf]
        # format: relts persistence msgsize mqtime getcount msgid
        fh.write("#{t - t0}|#{ff[1]}|#{ff[5]}|#{mqtime}|#{gcount}|#{mid}\n")
      }
    }
    $log.info "closing #{tgtfhs.values.size} file handles"
    tgtfhs.each_value {|fh| fh.close}
    $log.info "prepared within #{(Time.now - tstart).to_i} s"
    @duration = t - t0  # per iteration!
    $log.info "anticipated test duration: #{duration_string(@duration)} per iteration"
    return true
  end


  # test run

  # from scripts/startupCurrentLoad.sh
  def start_currents(params={})
    iterations = params[:iterations] || 1
    drtn = @duration * iterations
    sleepinterval = 60
    tuning = {}
    File.readlines(@tuningfile).each {|line|
      line.chomp!
      next if line.start_with?('#')
      ff = line.split('|')
      tuning[ff[0]] = [ff[1], ff[2]]  # speedup and inflation per queue
    }
    @tstart = Time.now
    exe = File.join(@exedir, 'currentLoad')
    env = {'MQCHLLIB'=>File.absolute_path('etc/mqperf/mqchllib'), 'MQCHLTAB'=>@mqchltab}
    out = "#{@logdir}/output_currentload.log"
    inputfiles = Dir["#{@ctrlfiles}/#{@qprefix}*"]
    inputfiles.each_with_index {|fname, idx|
      qname = File.basename(fname)
      if t = tuning[qname]
        speedup = t[0]
        inflation = t[1]
      else
        speedup = params[:speedup] || 1
        inflation = params[:inflation] || 1
      end
      args = "#{fname} #{@perflogs}/#{qname} #{@randomfile} #{@qmgr} #{qname} "
      args += "#{speedup} #{inflation} #{iterations} #{@tstart.to_i} #{drtn} #{sleepinterval}"
      exe_start(env, exe, args, out, params[:dryrun])
      $log.info "started worker ##{idx + 1}/#{inputfiles.size}" if idx % 100 == 0
    }
    @tend = Time.now + drtn
    $log.info "started #{inputfiles.size} currentLoad processes"
    $log.info "currentLoad processes are running until #{@tend} (#{duration_string(drtn)})"
    return true
  end

  def start_waiters(params={})
    exe = File.join(@exedir, 'amqsget0WaitForMessage')
    env = {'MQSERVER'=>'POLL.TO.RHSHRL3/TCP/f38asq12(1614)'}
    out = "#{@logdir}/output_waiters.log"
    @waiters.times {|i|
      args = "WAITER_#{i}.REQUEST01 #{@qmgr} #{@waitsperhour}"
      exe_start(env, exe, args, out, params[:dryrun])
      $log.info "started waiter ##{i + 1}/#{@waiters}" if i % 100 == 0
    }
    $log.info "started #{@waiters} waiters"
    return true
  end

  def start_reconnectors(params={})
    exe = File.join(@exedir, 'amqsget0Reconnect')
    env = {'MQSERVER'=>'RECON.TO.RHSHRL3/TCP/f38asq12(1614)'}
    out = "#{@logdir}/output_reconnectors.log"
    @reconnectors.times {|i|
      args = "WAITER_#{i}.REQUEST01 #{@qmgr} #{@reconnsperhour}"
      exe_start(env, exe, args, out, params[:dryrun])
    }
    $log.info "started #{@reconnectors} reconnectors"
    return true
  end

  # helper to get a process started incl permissions, logging, dryrun
  def exe_start(env, exe, args, out, dryrun)
    File.chmod(0555, exe) unless File.executable?(exe)
    File.new(out, 'w') if out != '/dev/null' && (!File.exists?(out) || File.mtime(out) < @tstart)
    if dryrun
      $log.warn "Dry run only, would start:\n#{exe} #{args}"
    else
      system(env, "#{exe} #{args} >> #{out} 2>&1 &")
    end
    sleep 0.1
  end

  # stop (kill) all executables, return true on success
  def exe_stop(exe)
    $log.info "exe_stop #{exe.inspect}"
    n = %x(ps -ef|grep #{exe}|grep -v grep|wc -l).to_i
    if n != 0
      $log.info "  stopping #{n} processes"
      %x(killall #{exe})
      n = %x(ps -ef|grep #{exe}|grep -v grep|wc -l).to_i
    end
    ($log.warn "#{n} remaining processes"; return false) if n != 0
    return true
  end

  # stop all running processes because of test end or abort
  def stop_processes
    %w(currentLoad amqsget0WaitForMessage amqsget0Reconnect).inject(true) {|res, exe| exe_stop(exe)}
  end


  # test data evaluation

  # from scripts/currentLoad/evalScript.pl
  # analyzes per-queue worker (currentLoad exe) logs, creates a csv result file and charts (in a zip file)
  def report(params={})
    reszipname = params[:results] || "#{@logdir}/mqperf_#{Time.now.utc.strftime('%F_%T').gsub(':', '-')}.zip"
    $log.info "MQPerf.report perflogs: #{@perflogs}, results: #{reszipname}"
    #
    tshash = {}
    files = Dir["#{@perflogs}/#{qprefix}*"]
    $log.info "  analyzing #{files.size} files in #{@perflogs}"
    files.each {|fname|
      timestamp = nil
      putMsgs = 0
      putMsgSize = 0
      lateMsgs = 0
      lateTime = 0
      maxLateTime = 0
      getMsgs = 0
      getMsgSize = 0
      roundTripTime = 0
      maxRoundTripTime = 0
      discardCount = 0
      errorCount = 0
      corruptCount = 0
      #
      File.readlines(fname).each {|rawline|
        ff0 = rawline.split(' ', 2)
        next if ff0.length < 2
        ctime, cmin, csec = ff0.first.split(':')
        currentTS = "#{ctime}:#{cmin}"
        if currentTS != timestamp
          if timestamp
            tshash[timestamp] ||= Array.new(12, 0)
            a = tshash[timestamp]
            a[0] += putMsgs
            a[1] += putMsgSize
            a[2] += lateMsgs
            a[3] += lateTime
            a[4] = maxLateTime if a[4] < maxLateTime
            a[5] += getMsgs
            a[6] += getMsgSize
            a[7] += roundTripTime
            a[8] = maxRoundTripTime if a[8] < maxRoundTripTime
            a[9] += discardCount
            a[10] += errorCount
            a[11] += corruptCount
            putMsgs = 0
            putMsgSize = 0
            lateMsgs = 0
            lateTime = 0
            maxLateTime = 0
            getMsgs = 0
            getMsgSize = 0
            roundTripTime = 0
            maxRoundTripTime = 0
            discardCount = 0
            errorCount = 0
            corruptCount = 0
          end
          timestamp = currentTS
        end
        line = ff0[1]
        if line.include?('Message put')
          putMsgs += 1
          putMsgSize += line.split('Size:')[1].to_i
        elsif line.include?('Oops')
          t = line.match(/m (.*?) seconds/)[1].to_i
          if t > 1
            lateMsgs += 1
            lateTime += t
            maxLateTime = t if maxLateTime < t
          end
        elsif line.include?('received')
          getMsgs += 1
          m = line.match(/:(.*?) ms .*Size:(.*)/)
          rtt = m[1].to_i
          roundTripTime += rtt
          maxRoundTripTime = rtt if maxRoundTripTime < rtt
          getMsgSize += m[2].to_i
        elsif line.include?('discard')
          discardCount += 1
        elsif line.include?('ERROR')
          errorCount += 1
        elsif line.include?('corrupt')
          corruptCount += 1
        end
      }
      if timestamp
        tshash[timestamp] ||= Array.new(12, 0)
        a = tshash[timestamp]
        a[0] += putMsgs
        a[1] += putMsgSize
        a[2] += lateMsgs
        a[3] += lateTime
        a[4] = maxLateTime if a[4] < maxLateTime
        a[5] += getMsgs
        a[6] += getMsgSize
        a[7] += roundTripTime
        a[8] = maxRoundTripTime if a[8] < maxRoundTripTime
        a[9] += discardCount
        a[10] += errorCount
        a[11] += corruptCount
      end
    }
    # create a zip file with results and charts
    Zip::File.open(reszipname, Zip::File::CREATE) {|zipfile|
      # write CSV result file
      zipfile.get_output_stream('mqperf_result.csv') {|fh|
        fh.write('timestamp|')
        fh.write('putMsgs|putMsgSize|lateMsgs|sumlateTime|maxLateTime|getMsgs|getMsgSize|')
        fh.write("sumRoundTripTime|maxRoundTripTime|discardCount|errorCount|corruptCount\n")
        tshash.keys.sort.each {|ts| fh.write("#{ts}|#{tshash[ts].join('|')}\n")}
      }
      # create charts
      # 1
      ch1 = Chart::Time.new
      ch1.add_series(tshash.keys.sort.collect {|ts| [Time.parse(ts), tshash[ts][0] / 60]}, name: 'put')
      ch1.add_series(tshash.keys.sort.collect {|ts| [Time.parse(ts), tshash[ts][5] / 60]}, name: 'get')
      ch1.add_series(tshash.keys.sort.collect {|ts| [Time.parse(ts), tshash[ts][2] / 60]}, name: 'late')
      ch1.create_chart(title: 'Messages put/got', x: 'timestamp', y: 'msgs/s')
      zipfile.get_output_stream('1_msgCount.png') {|fh| fh.write(ch1.encode_png)}
      # 2
      ch2 = Chart::Time.new
      ch2.add_series(tshash.keys.sort.collect {|ts| [Time.parse(ts), tshash[ts][8]]}, name: 'max RTT')
      ch2.add_series(tshash.keys.sort.collect {|ts|
          msgs = tshash[ts][0]
          [Time.parse(ts), msgs > 0 ? tshash[ts][7] / msgs : 0]
        }, name: 'avg RTT')
      ch2.create_chart(title: 'Message Run Times', x: 'timestamp', y: 'round trip time/ms', ymax: 5000)
      zipfile.get_output_stream('2_runTimes.png') {|fh| fh.write(ch2.encode_png)}
      # 3
      ch3 = Chart::Time.new
      ch3.add_series(tshash.keys.sort.collect {|ts|
          msgs = tshash[ts][2]
          [Time.parse(ts), msgs > 0 ? msgs / tshash[ts][3] : 0]
        }, name: 'avgLateTime')
      ch3.add_series(tshash.keys.sort.collect {|ts| [Time.parse(ts), tshash[ts][4]]}, name: 'maxLateTime')
      ch3.add_series(tshash.keys.sort.collect {|ts| [Time.parse(ts), tshash[ts][2]]}, name: 'lateMsgs')
      ch3.create_chart(title: 'Late Times', x: 'timestamp', y: 'late time in seconds', ymax: 10)
      zipfile.get_output_stream('3_lateTimes.png') {|fh| fh.write(ch3.encode_png)}
      # 4
      ch4 = Chart::Time.new
      ch4.add_series(tshash.keys.sort.collect {|ts| [Time.parse(ts), tshash[ts][6] / 1024 / 1024]}, name: 'getMsgSize')
      ch4.create_chart(title: 'Message Volume per Minute', x: 'timestamp', y: 'data volume/GiB')
      zipfile.get_output_stream('4_dataVolume.png') {|fh| fh.write(ch4.encode_png)}
    }
    $log.info "results have been written to #{File.absolute_path(reszipname)}"
    return true
  end


  # run a complete test
  def runtest(params={})
    # prepare ctrl files and start processes
    prepare(params) || return
    start_currents(params) || return
    start_waiters || (stop_processes; return)
    start_reconnectors || (stop_processes; return)
    # waiting for test end
    sleeptime = @tend - Time.now
    $log.info "waiting #{duration_string(sleeptime)} for test end at #{@tend}"
    sleep sleeptime
    # stop processes and create report
    stop_processes
    report(results: params[:results])
  end

end
