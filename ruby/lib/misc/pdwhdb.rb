=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.


Author: Steffen Steidten, 2012-08-07

Version:  1.2.3

History:
  2015-06-29 ssteidte, removed dependency on the obsolete AutoDB
  2016-01-13 sfrieske, started adding LL queries for regression tests
  2018-02-15 sfrieske, added lot_wafer
=end

require 'dbsequel'


# PDWH Testing Support
module PDWH

  # PDWH DB queries for (mostly) LL
  class LLDB
    constants.each {|c| remove_const(c)} # for clean re-load

    BatchRun = Struct.new(:run_id, :name, :status, :tstart, :tend)

    Entry = Struct.new(:sk, :name, :systemkey, :deleted, :cdc_date, :update_date, :valid_from, :valid_to) # generic
    Cast = Struct.new(:sk, :name, :systemkey, :deleted, :cdc_date, :claim_time, :valid_from, :valid_to)
    LotWafer = Struct.new(:sk, :lot, :wafergroupsk, :nwafers, :cdc_date, :valid_from, :valid_to)
    WaferLot = Struct.new(:sk, :wafer, :lot, :cdc_date, :insert_date, :valid_from, :valid_to)
    SrcProduct = Struct.new(:sk, :name, :srcproductsk, :productsk, :cdc_date, :insert_date, :valid_from, :valid_to)
    Udata = Struct.new(:parentsk, :srctable, :name, :value, :deleted, :cdc_date, :update_date, :valid_from, :valid_to)
    Xfer = Struct.new(:sk, :carrier, :cj, :jobid, :status, :type, :zone, :cjobid, :cjobstatus, :frommachine_type,
      :frommachine, :fromport, :tomachine_type, :tomachine, :toport, :insert_date, :cdc_date, :valid_from, :valid_to)
    XferHistory = Struct.new(:sk, :carrier, :cj, :jobid, :jobid_last, :cjobid, :frommachine_type, :frommachine_location,
      :frommachine, :fromport, :tomachine_type, :tomachine_location, :tomachine, :toport, :ncarriers, :nreroutes,
      :interarea, :stockout, :rerouted, :arrived, :cjstart, :cjpickup, :cjend, :insert_date, :cdc_date)
    Cj = Struct.new(:sk, :cj, :state, :nlots, :lot, :carrierhistsk, :eqphistsk, :pg, :loadport, :unloadport, :mode,
      :insert_date, :cdc_date)
    Sj = Struct.new(:sk, :sj, :state, :eqp, :eqphistsk, :pg, :sj_created, :sj_started, :sj_finished, :sj_deleted,
      :sj_error, :insert_date, :cdc_date)

    LotOp = Struct.new(:lot, :route, :module, :op, :opNo, :category, :claim_time)
    RouteModule = Struct.new(:main_sk, :detail_sk, :valid_from, :valid_to, :route, :module, :module_seqno, :op_seqno,
      :op, :opNo, :mainpd_type, :mandatory, :pd_type)

    attr_accessor :db


    def initialize(env, params={})
      @db = ::DB.new("pdwh.#{env}", {schema: 'PDWH_LL'}.merge(params))
    end

    # return intervals for lot and lothold snapshots in seconds
    def snapshot_interval(params={})
      q = "select param_value from PDWH_LL_ETL.ETL_CONFIG where batch_id='SIVIEW_SNS' and param_name='SNAPSHOT_INTERVAL'
        and (process_id='siview_lothold_snapshot_load' or process_id='siview_lot_snapshot_load')"
      res = @db.select(q) || return
      return res if params[:raw]
      res.flatten.collect {|e| e.to_i * 60}
    end

    # batch is one of BACKLOG, SIVIEW_DIM, SIVIEW_TRA, SIVIEW_SNS, etc.
    #   see https://docs.google.com/drawings/d/1S5Rslntccdv52dujzst7MaRLjKEl9i0kAmCwU-Wbwdk/edit
    def etl_batch_run(batch, params={})
      qargs = []
      q = 'select BATCH_RUN_ID, BATCH_ID, STATUS, START_TS, END_TS from PDWH_LL_ETL.ETL_BATCH_RUN'
      q, qargs = @db._q_restriction(q, qargs, 'BATCH_ID', batch)
      q, qargs = @db._q_restriction(q, qargs, 'BATCH_RUN_ID', params[:run_id])
      q, qargs = @db._q_restriction(q, qargs, 'STATUS', params[:status])
      q, qargs = @db._q_restriction(q, qargs, 'START_TS >=', params[:tstart])
      q, qargs = @db._q_restriction(q, qargs, 'END_TS <=', params[:tend])
      q += ' ORDER BY BATCH_RUN_ID'
      res = @db.select(q, *qargs) || return
      return res if params[:raw]
      return res.collect {|r|
        r[0] = r[0].to_i
        BatchRun.new(*r)
      }
    end

    # generic query for LL TABLE or TABLE_HIST entries
    def entries(table, namecolumn, params={})
      ($log.warn 'no selector for LLDB query, aborting'; return) if params.values.compact.empty?
      qargs = []
      nosystemkey = params[:nosystemkey] ? 'null as ' : ''
      if params[:hist]
        skname = "#{table}_HIST_SK"
        q = "select #{skname}, #{namecolumn}, #{nosystemkey}D_THESYSTEMKEY, null as DELETED_FLAG, CDC_DATE,
          null as UPDATE_DATE, VALID_FROM, VALID_TO from #{table}_HIST"
        q, qargs = @db._q_restriction(q, qargs, 'VALID_TO <=', params[:valid_to])
      else
        skname = "#{table}_SK"
        q = "select #{skname}, #{namecolumn}, #{nosystemkey}D_THESYSTEMKEY, DELETED_FLAG, CDC_DATE, UPDATE_DATE from #{table}"
        q, qargs = @db._q_restriction(q, qargs, 'DELETED_FLAG', params[:deleted])
      end
      q, qargs = @db._q_restriction(q, qargs, skname, params[:sk])
      q, qargs = @db._q_restriction(q, qargs, namecolumn, params[:name] || params[namecolumn.downcase.to_sym])
      q, qargs = @db._q_restriction(q, qargs, 'CDC_DATE >=', params[:tstart])
      q, qargs = @db._q_restriction(q, qargs, 'CDC_DATE <=', params[:tend])
      q += ' ORDER BY CDC_DATE'
      res = @db.select(q, *qargs) || return
      return res.collect {|r|
        r[0] = r[0].to_i
        Entry.new(*r)
      }
    end

    # uses CLAIM_TIME instead of UPDATE_DATE
    def cast(params={})
      ($log.warn 'no selector for LLDB query, aborting'; return) if params.empty?
      qargs = []
      if params[:hist]
        q = 'select CAST_HIST_SK, CAST_ID, D_THESYSTEMKEY, null as DELETED_FLAG, CDC_DATE,null as CLAIM_TIME, 
          VALID_FROM, VALID_TO from CAST_HIST'
        q, qargs = @db._q_restriction(q, qargs, 'VALID_TO <=', params[:valid_to])
      else
        q = 'select CAST_SK, CAST_ID, D_THESYSTEMKEY, DELETED_FLAG, CDC_DATE, CLAIM_TIME from CAST'
        q, qargs = @db._q_restriction(q, qargs, 'DELETED_FLAG', params[:deleted])
      end
      q, qargs = @db._q_restriction(q, qargs, 'CAST_ID', params[:carrier])
      q, qargs = @db._q_restriction(q, qargs, 'CDC_DATE >=', params[:tstart])
      q, qargs = @db._q_restriction(q, qargs, 'CDC_DATE <=', params[:tend])
      q += ' ORDER BY CDC_DATE'
      res = @db.select(q, *qargs) || return
      return res.collect {|r|
        r[0] = r[0].to_i
        Cast.new(*r)
      }
    end

    # nothing special
    def eqp(params={})
      entries('EQUIPMENT', 'EQP_ID', {name: params[:eqp]}.merge(params))
    end

    # nothing special
    def inhibit(params={})
      entries('INHIBIT', 'INHIBIT_ID', {name: params[:iid]}.merge(params))
    end

    # no systemkey
    def product(params={})
      entries('PRODUCT', 'PRODSPEC_ID', {nosystemkey: true, name: params[:product]}.merge(params))
    end

    # hist only meaningful
    def lot_wafer(lot, params={})
      qargs = []
      q = 'select LOT_TO_WAFER_HIST_SK, LOT_ID, WAFER_GROUP_SK, WAFER_COUNT, CDC_DATE, VALID_FROM, VALID_TO from LOT_TO_WAFER_HIST'
      q, qargs = @db._q_restriction(q, qargs, 'LOT_ID', lot)
      q, qargs = @db._q_restriction(q, qargs, 'WAFER_COUNT', params[:nwafers])
      q, qargs = @db._q_restriction(q, qargs, 'CDC_DATE >=', params[:tstart])
      q, qargs = @db._q_restriction(q, qargs, 'CDC_DATE <=', params[:tend])
      q, qargs = @db._q_restriction(q, qargs, 'VALID_FROM >=', params[:valid_from])
      q, qargs = @db._q_restriction(q, qargs, 'VALID_TO <=', params[:valid_to])
      q += ' ORDER BY CDC_DATE'
      res = @db.select(q, *qargs) || return
      return res.collect {|r|
        [0, 2, 3].each {|i| r[i] = r[i].to_i}
        LotWafer.new(*r)
      }
    end

    # hist only
    def wafer_lot(wafer, params={})
      # ($log.warn 'no selector for LLDB query, aborting'; return) if params.values.compact.empty?
      qargs = []
      q = 'select WAFER_TO_LOT_HIST_SK, WAFER_ID, LOT_ID, CDC_DATE, INSERT_DATE, VALID_FROM, VALID_TO from WAFER_TO_LOT_HIST'
      q, qargs = @db._q_restriction(q, qargs, 'WAFER_ID', wafer)
      q, qargs = @db._q_restriction(q, qargs, 'LOT_ID', params[:lot])
      q, qargs = @db._q_restriction(q, qargs, 'CDC_DATE >=', params[:tstart])
      q, qargs = @db._q_restriction(q, qargs, 'CDC_DATE <=', params[:tend])
      q, qargs = @db._q_restriction(q, qargs, 'VALID_FROM >=', params[:valid_from])
      q, qargs = @db._q_restriction(q, qargs, 'VALID_TO <=', params[:valid_to])
      q += ' ORDER BY CDC_DATE'
      res = @db.select(q, *qargs) || return
      return res.collect {|r|
        r[0] = r[0].to_i
        WaferLot.new(*r)
      }
    end
  
    # hist only
    def srcproduct_hist(params={})
      ($log.warn 'no selector for LLDB query, aborting'; return) if params.empty?
      qargs = []
      q = 'select PRODUCT_SOURCE_HIST_SK, SRC_PRODSPEC_ID, SRC_PRODUCT_SK, PRODUCT_SK,
        CDC_DATE, INSERT_DATE, VALID_FROM, VALID_TO from PRODUCT_SOURCE_HIST'
      q, qargs = @db._q_restriction(q, qargs, 'SRC_PRODSPEC_ID', params[:srcproduct] || params[:name])
      q, qargs = @db._q_restriction(q, qargs, 'PRODUCT_SK', params[:productsk])
      if params[:product]
        prods = product(name: params[:product])
        q, qargs = @db._q_restriction(q, qargs, 'PRODUCT_SK', prods.collect {|e| e.sk})
      end
      q, qargs = @db._q_restriction(q, qargs, 'VALID_TO <=', params[:valid_to])
      q, qargs = @db._q_restriction(q, qargs, 'CDC_DATE >=', params[:tstart])
      q, qargs = @db._q_restriction(q, qargs, 'CDC_DATE <=', params[:tend])
      q += ' ORDER BY CDC_DATE'
      res = @db.select(q, *qargs) || return
      return res.collect {|r|
        r[0] = r[0].to_i
        r[2] = r[2].to_i
        r[3] = r[3].to_i
        SrcProduct.new(*r)
      }
    end

    # hist only
    def trnreq_hist(params={})
      ($log.warn 'no selector for LLDB query, aborting'; return) if params.empty?
      qargs = []
      q = 'select TRN_REQ_HIST_SK, CARRIER_ID, CARRIER_CTRL_JOB_ID, JOB_ID, JOB_STATUS, TRANSPORT_TYPE, ZONE_TYPE,
        CARRIER_JOB_ID, CARRIER_JOB_STATUS, FROM_MACHINE_TYPE, FROM_MACHINE_ID, FROM_PORT_ID,
        TO_MACHINE_TYPE, TO_MACHINE_ID, TO_PORT_ID, INSERT_DATE, CDC_DATE, VALID_FROM, VALID_TO from TRN_REQ_HIST'
      q, qargs = @db._q_restriction(q, qargs, 'JOB_ID', params[:jobid])
      q, qargs = @db._q_restriction(q, qargs, 'CARRIER_JOB_ID', params[:cjobid])
      q, qargs = @db._q_restriction(q, qargs, 'CARRIER_CTRL_JOB_ID', params[:cj])
      q, qargs = @db._q_restriction(q, qargs, 'CARRIER_ID', params[:carrier])
      q, qargs = @db._q_restriction(q, qargs, 'VALID_TO <=', params[:valid_to])
      q, qargs = @db._q_restriction(q, qargs, 'CDC_DATE >=', params[:tstart])
      q, qargs = @db._q_restriction(q, qargs, 'CDC_DATE <=', params[:tend])
      q += ' ORDER BY CDC_DATE'
      res = @db.select(q, *qargs) || return
      return res.collect {|r|
        r[0] = r[0].to_i
        Xfer.new(*r)
      }
    end

    def trnreq_history(params={})
      ($log.warn 'no selector for LLDB query, aborting'; return) if params.empty?
      qargs = []
      q = 'select TRN_REQ_HISTORY_SK, CARRIER_ID, CARRIER_CTRL_JOB_ID, FIRST_JOB_ID, LAST_JOB_ID, CARRIER_JOB_ID, 
        SOURCE_LEVEL, SOURCE_LOCATION, SOURCE_MACHINE_ID, SOURCE_PORT_ID, DESTINATION_LEVEL, DESTINATION_LOCATION, 
        DESTINATION_MACHINE_ID, DESTINATION_PORT_ID, NUMBER_INVOLVED_CARRIER, NUMBER_REROUTES, IS_INTERAREA_TRANSPORT, 
        IS_STOCKOUT, IS_REROUTED_CARRIER, IS_ARRIVED_AT_DEST, CARRIER_JOB_BEGIN_TS, CARRIER_JOB_PICK_UP_TS, 
        CARRIER_JOB_END_TS, INSERT_DATE, CDC_DATE from TRN_REQ_HISTORY'
      q, qargs = @db._q_restriction(q, qargs, 'FIRST_JOB_ID', params[:jobid])
      q, qargs = @db._q_restriction(q, qargs, 'CARRIER_JOB_ID', params[:cjobid])
      q, qargs = @db._q_restriction(q, qargs, 'CARRIER_CTRL_JOB_ID', params[:cj])
      q, qargs = @db._q_restriction(q, qargs, 'CARRIER_ID', params[:carrier])
      q, qargs = @db._q_restriction(q, qargs, 'CARRIER_JOB_BEGIN_TS >=', params[:cjstart])
      q, qargs = @db._q_restriction(q, qargs, 'CARRIER_JOB_END_TS <=', params[:cjend])
      q, qargs = @db._q_restriction(q, qargs, 'CDC_DATE >=', params[:tstart])
      q, qargs = @db._q_restriction(q, qargs, 'CDC_DATE <=', params[:tend])
      q += ' ORDER BY CDC_DATE'
      res = @db.select(q, *qargs) || return
      return res.collect {|r|
        [0, 14, 15].each {|i| r[i] = r[i].to_i}
        [16, 17, 18, 19].each {|i|
          if r[i].include?('TRUE')
            r[i] = true
          elsif r[i].include?('FALSE')
            r[i] = false
          else
            r[i] = nil
          end
        }
        XferHistory.new(*r)
      }
    end

    # hist only
    def cj_hist(params={})
      ($log.warn 'no selector for LLDB query, aborting'; return) if params.empty?
      qargs = []
      q = 'select CTRLJOB_HIST_DETAILS_SK, CTRLJOB_ID, LAST_STATE, TOTAL_LOTS_OF_CTRLJOB, LOT_ID, CAST_HIST_SK,
        EQUIPMENT_HIST_SK, PORT_GRP, LOAD_PORT_ID, UNLOAD_PORT_ID, AUTOMATION_MODE, INSERT_DATE, CDC_DATE 
        from CTRLJOB_HIST_DETAILS'
      q, qargs = @db._q_restriction(q, qargs, 'CTRLJOB_ID', params[:cj])
      q, qargs = @db._q_restriction(q, qargs, 'LOT_ID', params[:lot])
      q, qargs = @db._q_restriction(q, qargs, 'CDC_DATE >=', params[:tstart])
      q, qargs = @db._q_restriction(q, qargs, 'CDC_DATE <=', params[:tend])
      q += ' ORDER BY CDC_DATE'
      res = @db.select(q, *qargs) || return
      return res.collect {|r|
        [0, 3, 5, 6].each {|i| r[i] = r[i].to_i}
        Cj.new(*r)
      }
    end

    def sj_hist(params={})
      ($log.warn 'no selector for LLDB query, aborting'; return) if params.empty?
      qargs = []
      q = 'select SORTJOB_HIST_DETAILS_SK, SORTER_JOB_ID, LAST_STATE, EQP_ID, EQUIPMENT_HIST_SK, PORT_GROUP_ID,
        SORTJOB_CREATED_TS, SORTJOB_STARTED_TS, SORTJOB_FINISHED_TS, SORTJOB_DELETED_TS, SORTJOB_ERROR_TS,
        INSERT_DATE, CDC_DATE from SORTJOB_HIST_DETAILS'
      q, qargs = @db._q_restriction(q, qargs, 'SORTER_JOB_ID', params[:sjid])
      q, qargs = @db._q_restriction(q, qargs, 'CDC_DATE >=', params[:tstart])
      q, qargs = @db._q_restriction(q, qargs, 'CDC_DATE <=', params[:tend])
      q += ' ORDER BY CDC_DATE'
      res = @db.select(q, *qargs) || return
      return res.collect {|r|
        [0, 4].each {|i| r[i] = r[i].to_i}
        Sj.new(*r)
      }
    end

    def udata(params={})
      ($log.warn 'no selector for LLDB query, aborting'; return) if params.empty?
      qargs = []
      if params[:hist]
        q = 'select PARENT_SK, SOURCE_TABLE, NAME, VALUE, null as DELETED_FLAG, CDC_DATE, null as UPDATE_DATE,
          VALID_FROM, VALID_TO from UDATA_HIST'
        q, qargs = @db._q_restriction(q, qargs, 'VALID_TO <=', params[:valid_to])
      else
        q = 'select PARENT_SK, SOURCE_TABLE, NAME, VALUE, DELETED_FLAG, CDC_DATE, UPDATE_DATE from UDATA'
        q, qargs = @db._q_restriction(q, qargs, 'DELETED_FLAG', params[:deleted])
      end
      q, qargs = @db._q_restriction(q, qargs, 'PARENT_SK', params[:parentsk])
      q, qargs = @db._q_restriction(q, qargs, 'SOURCE_TABLE', params[:srctable])
      q, qargs = @db._q_restriction(q, qargs, 'NAME', params[:name])
      q, qargs = @db._q_restriction(q, qargs, 'VALUE', params[:value])
      q, qargs = @db._q_restriction(q, qargs, 'CDC_DATE >=', params[:tstart])
      q, qargs = @db._q_restriction(q, qargs, 'CDC_DATE <=', params[:tend])
      q += ' ORDER BY CDC_DATE'
      res = @db.select(q, *qargs) || return
      return res.collect {|r|
        r[0] = r[0].to_i
        Udata.new(*r)
      }
    end


    def XXXlot_opecomp(lot, params={})
      q = "select lot_id, mainpd_id_mainpd, modulepd_id, pd_id, ope_no, ope_category, claim_time
        from PDWH_REPO.v_lot_trc where lot_id = ? and ope_category = 'OperationComplete' order by claim_time desc"
      qargs = [lot]
      res = @db.select(q, *qargs) || return
      return res if params[:raw]
      return res.collect {|r| LotOp.new(*r)}
    end

    def XXXroute_modules(params={})
      route = params[:route] || 'P-PDWH-MAINPD.01'
      mod = params[:module]
      q = "select main.mainpd_flow_hist_sk main_sk, detail.mainpd_flow_det_hist_sk det_sk,
        main.valid_from, main.valid_to, main.mainpd_id, detail.modulepd_id, detail.module_seqno, detail.operation_seqno,
        detail.ope_no, detail.pd_id, main.mainpd_type, detail.mandatory_flag, detail.pd_type
        from mainpd_flow_hist main, mainpd_flow_det_hist detail
        where mainpd_id = ? and
        main.mainpd_flow_hist_sk=detail.mainpd_flow_hist_sk and valid_from < sysdate and valid_to > sysdate
        order by valid_from desc, valid_to desc, module_seqno,operation_seqno"
      qargs = [route]
      res = @db.select(q, *qargs) || return
      return res if params[:raw]
      return res.collect {|r|
        r = RouteModule.new(*r)
        next if mod and r.module != mod
        r
      }.compact
    end

  end
end
