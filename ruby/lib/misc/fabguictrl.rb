=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author:     ssteidte, 2012-03-21
Version:    2.0.3

History
  2012-12-07 ssteidte, work on all instances unless explicitly told
  2013-02-04 ssteidte, support new service ports
  2014-06-04 ssteidte, FabGUI 11.7: remove RMI, service specific URLs
  2014-07-14 dsteger,  fixed service specific urls
  2016-08-24 sfrieske, use lib rqrsp_http instead of rqrsp
  2021-02-05 sfrieske, use instance_of instead of kind_of
  2021-08-26 sfrieske, use read_config instead of _config_from_file
=end

require 'util/readconfig'
require 'rqrsp_http'


# FabGUI control via HTTP
module FabGUI
  RestPorts = {fabgui: 8088, ccenter:	8180, lotctrl: 8280, processctrl:	8380, service: 8480}


  # FabGUI Control via HTTP
  class HttpControl
    attr_accessor :env, :params, :hosts, :instances

    def initialize(env, service, params={})
      @env = env
      @params = read_config("fabguictrl.#{env}", 'etc/urls.conf', params)
      @hosts = @params[:hosts] || @params[:host]
      @hosts = @hosts.split if @hosts.instance_of?(String)
      raise "no hosts defined for env #{env.inspect}" unless @hosts
      port = RestPorts[service.to_sym]
      raise "service #{service} is not defined" unless port
      # create control instances
      @instances = @hosts.collect {|host|
        RequestResponse::HttpJSON.new(nil, {url: "http://#{host}:#{port}/#{service.to_s}"}.merge(@params))
      }
    end

    def inspect
      "#<#{self.class.name} service: #{@service}, instances: #{@instances.inspect}>"
    end

    # return hash of property settings, optionally filtered by section, or nil on error
    def runtime_values(params={})
      # instance is either passed as number (0, 1, ..) or instance object, default is first instance
      instance = params[:instance] || @instances.first
      instance = @instances[instance] if instance.kind_of?(Numeric)
      res = instance.get(resource: '/get-all') || return
      section = params[:section]
      ret = {}
      res.each {|e|
        k = e['key']
        next if section and !k.start_with?(section)
        ret[k] = e['value']
      }
      return ret
    end

    # compare a hash of expected settings with the current runtime settings
    def verify_runtime_values(expvals)
      @instances.inject(true) {|ok, i|
        rvals = runtime_values(instance: i) || return
        expvals.each_pair {|k, v|
          ($log.warn "  wrong property #{k.inspect}: #{rvals[k].inspect} instead of #{v.inspect}"; ok=false) if rvals[k] != v
        }
        ok
      }
    end

    def set_runtime_value(pname, value, params={})
      @instances.inject(true) {|s, i|
        s && i.put([{key: pname, value: value}])
      }
    end

    def set_runtime_values(hashmap, params={})
      body = hashmap.map {|k, v| {key: k, value: v}}
      @instances.inject(true) {|s, i| s && i.put(body)}
    end
  end


  class ProcessCtrl < HttpControl
    def initialize(env, params={})
      super(env, :processctrl, params)
    end
  end


  class LotCtrl < HttpControl
    def initialize(env, params={})
      super(env, :lotctrl, params)
    end
  end

#  class EMSCtrl < HttpControl
#    def initialize(env, params={})
#      super(env, :ems, params)
#    end
#  end

end
