=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author:     sfrieske, 2017-03-31

Version:    1.2.0

History:
  2018-02-08 sfrieske, fixed typo in tool()
  2019-02-25 sfrieske, consolidated RTD related queries (for MdsWriteService)
  2021-12-08 sfrieske, simplified RTDServicesMDS queries
=end

require 'dbsequel'


# RTD/APF specific MDS data
module RTD

  class MDS
    attr_accessor :db

    def initialize(env, params={})
      @db = params[:db] || DB.new("mds.#{env}")
    end

    def inspect
      return "<#{self.class.name} #{@db.inspect}>"
    end

    def subtool(eqp, chamber)
      q = 'select EQP_ID, SUBTOOL_ID, E10, E10_ID, E10_SUBSTATE, USER_ID, IS_LONG_DOWN, TIME from T_SUBTOOL
        WHERE EQP_ID=? AND SUBTOOL_ID=?'
      res = @db.select(q, eqp, chamber) || return
      return res.collect {|r| {state: r[3], substate: r[4]}}
    end

    def tool(eqp)
      q = 'select EQP_ID, E10, E10_ID, E10_SUBSTATE, USER_ID, IS_LONG_DOWN, WIP_SIVIEW, WIP_LOT_AUTO3, 
        LAST_TIME_DISP_RTD, NEXT_TIME_DISP_RTD, TIME from T_TOOL WHERE EQP_ID=?'
      res = @db.select(q, eqp) || return
      return res.collect {|r| {state: r[2], substate: r[3], wip_siview: r[6], wip_lot_a3: r[7], nexttime: r[9]}}
    end

    # contains LRANK and GRANK, used in VAMOS_AutoProc06
    def tool_dlis_hist(params={})
      qargs = []
      q = 'select EQP_ID, DLIS_CALL_ID, SEQ_NO, CLAIM_TIME, SCENARIO, CAST_ID, LOT_ID, SELECT_FLAG, 
        RANK_LOCAL, RANK_GLOBAL from T_TOOL_DLIS_HIST'
      q, qargs = @db._q_restriction(q, qargs, 'EQP_ID', params[:eqp])
      q, qargs = @db._q_restriction(q, qargs, 'CAST_ID', params[:carrier])
      q, qargs = @db._q_restriction(q, qargs, 'LOT_ID', params[:lot])
      q, qargs = @db._q_restriction(q, qargs, 'CLAIM_TIME >=', params[:tstart])
      q, qargs = @db._q_restriction(q, qargs, 'CLAIM_TIME <=', params[:tend])
      q += ' ORDER BY CLAIM_TIME'
      res = @db.select(q, *qargs) || return
      return res.collect {|r| {eqp: r[0], carrier: r[5], lot: r[6], lrank: r[8], grank: r[9]}}
    end

    # ExternalMove jobs
    def trans_job(carrier)
      q = 'select CAST_ID, STATE, CURR_LOC, TARGET_LOC from T_TRANS_JOB WHERE CAST_ID=?'
      res = @db.select(q, carrier) || return
      return res.collect {|r| {state: r[1], currloc: r[2], tgtloc: r[3]}}
    end

    # returns exactly 1 value for the script parameter like 'Lot', 'Uxxx', 'TargetLocation'
    def script_para(objtype, objid, pname)
      q, qargs = @db._q_restriction('select S_VALUE from T_SCRIPT_PARA', [], 'OBJ_TYPE', objtype)
      q, qargs = @db._q_restriction(q, qargs, 'OBJ_ID', objid)
      q, qargs = @db._q_restriction(q, qargs, 'SP_NAME', pname)
      res = @db.select(q, *qargs) || return
      ret = res.flatten
      ($log.warn "no unique T_SCRIPT_PARA entry for #{qargs}: #{ret}"; return) if ret.size != 1
      return ret.first
    end

    def lot_target_area(lot)
      q = 'select LOT_ID, TARGET_LOCATION_ID, TARGET_AREA_ID, UPDATE_TIME, CLAIM_MEMO from T_LOT_TARGET_AREA WHERE LOT_ID=?'
      res = @db.select(q, lot) || return
      return res.collect {|r| {tgtarea: r[2], memo: r[4], update_time: r[3]}}
    end

    # get specs for a specified topic, e.g. CARRIER_CLASS_DEFINITION
    def specs_bytopic(topic)
      q = 'select unique SPEC_ID, SPEC_VERSION, TOPIC_ID from V_RTD_SPEC_DISP_CONFIG 
        WHERE TOPIC_ID=? ORDER BY TOPIC_ID, SPEC_ID, SPEC_VERSION'
      res = @db.select(q, topic) || return
      return res.collect {|r| {topic: r[2], spec_id: r[0], spec_version: r[1].to_i} }
    end

    # assuming specs_bytopic('CARRIER_CLASS_DEFINITION') is unique
    def carrier_class_definition(category)
      q = "select DATASET_INDEX, PARAM_NAME, PARAM_VALUE_NUM, PARAM_VALUE_STR 
        from V_RTD_SPEC_DISP_CONFIG where TOPIC_ID='CARRIER_CLASS_DEFINITION' and DATASET_INDEX IN 
          (select DATASET_INDEX from V_RTD_SPEC_DISP_CONFIG where PARAM_NAME='pz' and PARAM_VALUE_STR=?)
        ORDER BY DATASET_INDEX, PARAM_NAME"
      res = @db.select(q, category) || return
      ret = []
      reti = {}
      res.each {|r|
        idx = r[0].to_i
        if reti['idx'] != idx
          ret << reti unless reti.empty?
          reti = {'idx'=>idx}
        end
        value = r[2]
        reti[r[1]] = value.nil? ? r[3] : r[2].to_i
      }
      return ret
    end

  end

end
