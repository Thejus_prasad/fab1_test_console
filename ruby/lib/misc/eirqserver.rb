=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author:     dsteger, 2013-01-29

Version:    1.0.3

History:
  2013-04-24 dsteger,  added chamber support
  2015-01-08 ssteidte, fixed startup
  2015-05-04 ssteidte, based on WEBRick::GenericServer instead of GServer (for Ruby 2.2)
  2017-02-01 sfrieske, use MQ::JMS
  2017-11-20 sfrieske, moved LoopThread from misc.rb here
  2018-03-07 sfrieske, improved logging in case of missing recipes, merged LoopThread into EventLoop
  2021-08-26 sfrieske, use read_config instead of _config_from_file
=end

load './ruby/set_paths.rb'
require 'base64'
require 'cgi'
require 'optparse'
require 'pp'
require 'pathname'
require 'thread'
require 'webrick/server'
require 'securerandom'
# require 'misc'
require 'mqjms'
require 'util/readconfig'
require 'util/xmlhash'


module EI

  # Starts a new EI recipe servers.
  #
  # Configuration file contains the tool names and options.
  #
  # example for MTQA environment:
  #
  # mtqa:
  #  :tools: ALC2401 ETX2100 ETX2101 LTK2900 POL2300 SCB2200 CDS2101 MDX2900
  #  :LTK2900: TRK2900 STP2900
  #  :ALC2401: TRK2401 STP2401
  #  :synthetic: true
  #  :count: 1000
  #  :size: 10
  #
  class RecipeServer < WEBrick::GenericServer
    attr_accessor :env, :eis

    def initialize(env, port=22224, addr='', cfgfile='etc/eirqserver.conf')
      super(Port: port, BindAddress: addr)
      $log.info "EI RecipeServer listening on #{addr.inspect}:#{port}"
      @eis = {}
      @env = env
      params = read_config(env, cfgfile)
      params[:tools].split.each {|tool|
        mappings = params[tool.to_sym].split if params.has_key?(tool.to_sym)
        if params[:synthetic]
          rcount = params[:count]
          rsize = params[:size]
        end
        start_ei(tool, eqp_mappings: mappings, recipe_count: rcount, recipe_size: rsize)
      }
    end

    def run(io)
      io.puts('Welcome to EI server')
      loop do
        line = io.readline
        case line
        when /^exitserver/
          io.puts('OK') if stop_server
        when /^tools/
          io.puts(@eis.keys.inspect)
          io.puts('OK')
        when /^start_ei ([A-Z]+[0-9]+)( ([A-Z]+[0-9]+)){0,2}/
          mappings = $3 ? [$3, $5].compact : nil
          if start_ei($1, eqp_mappings: mappings)
            io.puts('OK')
          else
            io.puts("NOK - Failed to start #{$1}. Maybe queue does not exist.")
          end
        when /^stop_ei ([A-Z]+[0-9]+)/
          io.puts('OK') if stop_ei($1)
        when /^update_recipes ([A-Z]+[0-9]+)/
          io.puts('OK') if update_recipes($1)
        when /^help/
          io.puts('start_ei <entity> [mapping1 [mapping2]]  start the MQ listener for EI')
          io.puts('stop_ei <entity>                         stop the MQ listener for EI')
          io.puts('update_recipes <entity>                  read recipes from filesystem for EI')
          io.puts('exitserver                               shutdown the server')
          io.puts
        when /^exit/
          io.puts('Bye')
          break
        else
          io.puts("Unknown command #{line.inspect}")
        end
      end
    end

    # Connect to the EI queue
    def start_ei(entity, params={})
      $log.info "start_ei #{entity}, #{params.inspect}"
      # stop EI if running
      if @eis.has_key?(entity)
        $log.info "EI #{entity} already running"
        return true
        # t = @eis[entity]
        # t.stop
        # t.thread.join # need to wait for MQ closing
      end
      begin
        @eis[entity] = EventLoop.new(entity, @env, params)
        ##@eis[entity].run
        return true
      rescue => e
        $log.error e
        @eis.delete(entity)
        return false
      end
    end

    def update_recipes(entity)
      @eis[entity].read_recipes
    end

    # Disconnect and unregister
    def stop_ei(entity)
      if @eis.has_key?(entity)
        t = @eis[entity]
        t.stop
        t.thread.join
        @eis.delete(entity)
      end
    end

    def stop_server
      @eis.each_key {|ei| stop_ei(ei)}
      shutdown
    end

  end


  class Recipes
    attr_accessor :recipes, :eqp

    # Store tool prefix for recipe name extraction
    # default is :use_recipe
    @@tooltypes = { "BST" => :use_recipe, "ETX" => :use_recipe, "MDX28" => :use_recipe }
    # Filter to detect chamber subrecipes
    @@filter = /^PM[1-9][:#{File::SEPARATOR}]|^CH[1-9A-Z]?[:#{File::SEPARATOR}]|^LDG|^RDG/

    def initialize(eqp, params={})
      @recipes = {}
      @eqp = eqp
      @folder = Pathname.new(params[:folder] || 'testcasedata/recipes')
      @folder = @folder.join(eqp)
      if params[:recipe_count]
        generate_recipes(params)
      else
        read_recipes(params)
      end
    end

    def generate_recipes(params={})
      rsize = params[:recipe_size] || 1 # in MB
      recipe = SecureRandom.base64(rsize*1024*1024)
      params[:recipe_count].times do |i|
        @recipes["QA-RECIPE-%02dMB-%04d" % [rsize,i]] = recipe
      end
    end

    def read_recipes(params={})
      _key = @@tooltypes.keys.find {|tt| @eqp.start_with?(tt)}
      _type = @@tooltypes[_key] || :use_filename
      # read recipes
      _folder = @folder
      _folder = _folder.join('**') unless params[:nosubfolders]
      _folder = _folder.join('*')
      begin
        Dir[_folder].each {|filename| file_to_body(filename, @folder, _type) if File.file?(filename)}
      rescue => e
        $log.error "Can't read recipes from #{_folder}"
        $log.info e.backtrace.join("\n")
      end
    end

    def each_filtered_recipename(chamber, &block)
      @recipes.each_key.select {|r| r =~ /^#{chamber}/}.map {|r| r.sub("#{chamber}:","") }.each(&block)
    end

    def decode_name(toolrecipename)
      _str = toolrecipename
      repl_map = { "%22" => '"',
        "%2A" => "*",
        "%2F" => "/",
        "%3A" => ":",
        "%3C" => "<",
        "%3E" => ">",
        "%3F" => "?",
        "%5C" => "\\",
        "%7C" => "|" }
      repl_map.each {|k,v| _str = _str.gsub(k,v)}
      return _str
    end

    def each_recipename(&block)
      @recipes.each_key.reject {|r| r =~ @@filter}.each(&block)
    end

    def file_to_body(filename, basename, type)
      body = open(filename, "rb") {|io| io.read }
      path = Pathname.new(filename).relative_path_from(basename).to_s
      rcp_name = ""
      # Detect chamber folder and mask recipe name
      if path =~ @@filter
        cha, path = path.split(File::SEPARATOR, 2)
        rcp_name = "#{cha}:"
      end
      case type
      when :use_filename
        rcp_name += path
        rcp_name = decode_name(rcp_name)  # CGI.unescape(rcp_name)?
      when :use_recipe
        rcp_name += _extract_toolrecipename(body)
      else
        $log.error "Don't know recipename"
      end
      @recipes[rcp_name] = Base64.encode64(body)
    end

    # Extract the tool recipe name, works for LAM
    def _extract_toolrecipename(recipebody)
      _iend = recipebody.index("A\006")   # some magic bytes in the LAM recipe
      ($log.warn "tool recipe name not found in body"; return) unless _iend
      return recipebody.slice(4.._iend-1)
    end

  end


  class EventLoop
    attr_accessor :thread, :stopped, :mq, :recipes

    def initialize(entity, env, params={})
      @stopped = false
      @eqp = entity
      @recipes = {}
      if params[:eqp_mappings]
        params[:eqp_mappings].each {|eqp| @recipes[eqp] = Recipes.new(eqp, params)}
      else
        @recipes[entity] = Recipes.new(entity, params)
      end
      @mq = MQ::JMS.new(env, qname: params[:qname] || "CEI.#{@eqp}_REQUEST01", use_jms: false)
      @thread = Thread.new {
        until @stopped
          msg = @mq.receive_msg(timeout: 10)
          begin
            if msg
              xml = XMLHash.xml_to_hash(msg, '*//*Body')
              $log.info "#{xml.inspect}"
              if xml[:listRecipes]
                eqp, cha = xml[:listRecipes][:equipmentID][nil].split('.')
                rsp = list_recipes(eqp, cha)
              elsif xml[:uploadBody]
                eqp, cha = xml[:uploadBody][:equipmentID][nil].split('.')
                rcpname = xml[:uploadBody][:recipeName][nil]
                rsp = upload_recipe(rcpname, eqp, cha)
              elsif xml[:compareBody]
                eqp, cha = xml[:compareBody][:equipmentID][nil].split('.')
                rcpname = xml[:compareBody][:recipeName][nil]
                body = xml[:compareBody][:recipeBody][nil]
                rsp = compare_recipe(rcpname, body, eqp, cha)
              elsif xml[:downloadBody]
                eqp, cha = xml[:downloadBody][:equipmentID][nil].split('.')
                rcpname = xml[:downloadBody][:recipeName][nil]
                body = xml[:downloadBody][:recipeBody][nil]
                key_name = cha ? "#{cha}:#{rcpname}" : rcpname
                @recipes[eqp].recipes[key_name] = body
                rsp = download_recipe
              elsif xml[:visualizationFromToolRecipe]
                eqp, cha = xml[:visualizationFromToolRecipe][:equipmentID][nil].split('.')
                rcpname = xml[:visualizationFromToolRecipe][:recipeName][nil]
                rsp = visualize_recipe(rcpname, eqp, cha)
              elsif xml[:deleteBody]
                eqp, cha = xml[:deleteBody][:equipmentID][nil].split('.')
                rcpname = xml[:deleteBody][:recipeName][nil]
                key_name = cha ? "#{cha}:#{rcpname}" : rcpname
                @recipes[eqp].recipes.delete(key_name) if @recipes[eqp].recipes.has_key?(key_name)
                rsp = delete_recipe
              elsif xml[:reloadToolDCPs]
                dcp_path = xml[:reloadToolDCPs][:dcpPath][nil]
                rsp = reload_dcp(dcp_path)
              else
                $log.warn "invalid request: #{xml.pretty_inspect}"
                rsp = nil
              end
              queue = @mq.qopen(@mq.msg_properties[:reply_to], use_jms: false)
              @mq.send_msg(rsp, bytes_msg: true, correlation: @mq.msg.jms_message_id, queue: queue)
            else
              $log.debug {'no message, waiting 10 s'}
              sleep 10
            end
          rescue => error
            $log.warn $!
            $log.warn error.backtrace.join("\n")
          end
        end
        @mq.close
      }
    end

    def inspect
      "#<#{self.class.name}  #{@mq.inspect}>"
    end

    def stop
      @stopped = true
    end

    def read_recipes
      @recipes.values.each {|v| v.read_recipes}
    end

    def list_recipes(eqp, chamber=nil)
      $log.info "list_recipes #{eqp.inspect}, #{chamber.inspect}"
      xml = Builder::XmlMarkup.new()
      xml.instruct!
      xml.env :Envelope, "xmlns:env"=>"http://schemas.xmlsoap.org/soap/envelope/", "xmlns:ns"=>"http://www.amd.com/schemas/baselineservices" do |enve|
        enve.env :Body, "xmlns:xsi"=>"http://www.w3.org/2001/XMLSchema-instance", "xmlns:xsd"=>"http://www.w3.org/2001/XMLSchema" do |body|
          if @recipes.has_key?(eqp)
            body.ns :listRecipesResponse do |blsrv|
              blsrv.recipeList do |rcp|
                _enum = chamber ? @recipes[eqp].each_filtered_recipename(chamber) : @recipes[eqp].each_recipename
                _enum.each do |r|
                  rcp.recipeName r
                end
              end
            end
          else
            $log.warn "no recipes for #{eqp} in #{@folder}.to_s"
            body.env :Fault do |fault|
              fault.faultcode "CEIServiceException"
              fault.faultstring "CEI120155: Unknown equipment: #{eqp}."
            end
          end
        end
      end
      xml.target!
    end

    def upload_recipe(recipename, eqp, chamber=nil)
      $log.info "upload_recipe #{recipename}, #{eqp.inspect}, #{chamber.inspect}"
      xml = Builder::XmlMarkup.new()
      xml.instruct!
      xml.env :Envelope, "xmlns:env"=>"http://schemas.xmlsoap.org/soap/envelope/", "xmlns:ns"=>"http://www.amd.com/schemas/baselineservices" do |enve|
        enve.env :Body, "xmlns:xsi"=>"http://www.w3.org/2001/XMLSchema-instance", "xmlns:xsd"=>"http://www.w3.org/2001/XMLSchema" do |body|
          if @recipes.has_key?(eqp)
            _rcp = recipename
            _rcp = "#{chamber}:" + _rcp if chamber
            $log.info "#{_rcp.inspect}"
            _rcp_body = @recipes[eqp].recipes[_rcp]
            if _rcp_body
              body.ns :uploadBodyResponse  do |blsrv|
                $log.debug "#{_rcp_body.inspect}"
                blsrv.recipeBody CGI.escapeHTML(_rcp_body)
              end
            else
              body.env :Fault do |fault|
                fault.faultcode "ns:RecipeNotFoundException"
                fault.faultstring "CEI40030: Recipe Does Not Exist on the Tool. Recipe name '#{_rcp}'."
              end
            end
          else
            $log.warn "no recipes for #{eqp} in #{@folder}.to_s"
            body.env :Fault do |fault|
              fault.faultcode "CEIServiceException"
              fault.faultstring "CEI120155: Unknown equipment: #{eqp}."
            end
          end
        end
      end
      xml.target!
    end

    def compare_recipe(recipename, armor_body, eqp, chamber=nil)
      xml = Builder::XmlMarkup.new()
      xml.instruct!
      xml.env :Envelope, "xmlns:env"=>"http://schemas.xmlsoap.org/soap/envelope/", "xmlns:ns"=>"http://www.amd.com/schemas/baselineservices" do |enve|
        enve.env :Body, "xmlns:xsi"=>"http://www.w3.org/2001/XMLSchema-instance", "xmlns:xsd"=>"http://www.w3.org/2001/XMLSchema" do |body|
          if @recipes.has_key?(eqp)
            _rcp = recipename
            _rcp = "#{chamber}:" + _rcp if chamber
            $log.info "#{_rcp.inspect}"
            _rcp_body = @recipes[eqp].recipes[_rcp]
            if _rcp_body
              body.ns :compareBodyResponse  do |blsrv|
                _rcp_body_no_w = _rcp_body.gsub(/\s/,'')
                $log.info("#{_rcp_body_no_w.inspect} === #{armor_body.inspect}")
                blsrv.result (_rcp_body_no_w == armor_body).to_s
              end
            else
              body.env :Fault do |fault|
                fault.faultcode "ns:RecipeNotFoundException"
                fault.faultstring "CEI40030: Recipe Does Not Exist on the Tool. Recipe name '#{_rcp}'."
              end
            end
          else
            $log.warn "no recipes for #{eqp} in #{@folder}.to_s"
            body.env :Fault do |fault|
              fault.faultcode "CEIServiceException"
              fault.faultstring "CEI120155: Unknown equipment: #{eqp}."
            end
          end
        end
      end
      xml.target!
    end

    def download_recipe
      xml = Builder::XmlMarkup.new()
      xml.instruct!
      xml.env :Envelope, "xmlns:env"=>"http://schemas.xmlsoap.org/soap/envelope/", "xmlns:ns"=>"http://www.amd.com/schemas/baselineservices" do |enve|
        enve.env :Body, "xmlns:xsi"=>"http://www.w3.org/2001/XMLSchema-instance", "xmlns:xsd"=>"http://www.w3.org/2001/XMLSchema" do |body|
          body.ns :downloadBodyResponse
        end
      end
      xml.target!
    end

    def delete_recipe
      xml = Builder::XmlMarkup.new()
      xml.instruct!
      xml.env :Envelope, "xmlns:env"=>"http://schemas.xmlsoap.org/soap/envelope/", "xmlns:ns"=>"http://www.amd.com/schemas/baselineservices" do |enve|
        enve.env :Body, "xmlns:xsi"=>"http://www.w3.org/2001/XMLSchema-instance", "xmlns:xsd"=>"http://www.w3.org/2001/XMLSchema" do |body|
          body.ns :deleteBodyResponse
        end
      end
      xml.target!
    end

    def visualize_recipe(recipename, eqp, chamber=nil)
      xml = Builder::XmlMarkup.new()
      xml.instruct!
      xml.env :Envelope, "xmlns:env"=>"http://schemas.xmlsoap.org/soap/envelope/", "xmlns:ns"=>"http://www.amd.com/schemas/baselineservices" do |enve|
        enve.env :Body, "xmlns:xsi"=>"http://www.w3.org/2001/XMLSchema-instance", "xmlns:xsd"=>"http://www.w3.org/2001/XMLSchema" do |body|
          body.ns :visualizationFromToolRecipeResponse  do |blsrv|
            blsrv.content CGI.escapeHTML("<HTML><BODY>Not supported!</BODY></HTML>")
          end
        end
      end
      xml.target!
    end

    def reload_dcp(dcp_path)
      $log.info "Reload DCPs"
      xml = Builder::XmlMarkup.new()
      xml.instruct!
      xml.env :Envelope, "xmlns:env"=>"http://schemas.xmlsoap.org/soap/envelope/", "xmlns:ns"=>"http://www.amd.com/schemas/baselineservices" do |enve|
        enve.env :Body, "xmlns:xsi"=>"http://www.w3.org/2001/XMLSchema-instance", "xmlns:xsd"=>"http://www.w3.org/2001/XMLSchema" do |body|
          $log.info "#{dcp_path}..."
          body.ns :reloadToolDCPsResponse
        end
      end
      xml.target!
    end

  end

end


def main(argv)
  # set defaults
  env = nil
  logfile = "log/eirqserver.log"
  cfgfile = "etc/eirqserver.conf"
  verbose = false
  noconsole = true
  port = 22224
  # parse cmdline options
  optparser = OptionParser.new {|o|
    o.banner = "Usage: #{File.basename(__FILE__)} [options]"
    o.on('-e', '--env [ENV]', 'environment (e.g. "mtqa", "f8stag")') {|s| env = s}
    o.on('-p', '--port [PORT]', 'port (default is 22224)') {|s| port = s}
    o.on('-l', '--logfile [FILE]', 'log to FILE instead of STDOUT') {|f| logfile = f}
    o.on('-c', '--config [FILE]', 'configuration for specific environment') {|f| cfgfile = f}
  }
  optparser.parse(*argv)
  # create logger
  create_logger(file: logfile, file_verbose: verbose, verbose: verbose, noconsole: noconsole)
  STDOUT.puts "logging to #{logfile}, console log: #{!noconsole}"
  srv = EI::RecipeServer.new(env, port, '', cfgfile)
  $log.info "started EI server in #{env} on #{port}"
  srv.start
  srv.join
end

main(ARGV) if __FILE__ == $0
