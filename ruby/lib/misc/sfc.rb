=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author:  Ulrich Koerner, 2011-08-01

History:
  2012-01-27 ssteidte, copied BaseSoap from separate file here
  2014-07-11 dsteger,  converted to xmlhash function
  2016-08-30 sfrieske, split setupfc.rb in separate files, use lib rqrsp_http instead of rqrsp
  2017-05-29 sfrieske, use hash_to_xml instead of Builder
  2021-08-30 sfrieske, moved xmlhash.rb to util
=end

require 'base64'
require 'rqrsp_http'
require 'util/xmlhash'

ENV.delete('http_proxy')


module SetupFC

  # SetupFC SOAP Client
  class Client
    attr_accessor :env, :http

    def initialize(env, params={})
      @env = env
      @http = RequestResponse::HttpXML.new("sfcservice.#{env}", params)
    end

    def post_request(fname, data={})
      h = {:'soapenv:Envelope'=>{
        'xmlns:soapenv'=>'http://schemas.xmlsoap.org/soap/envelope/', 'xmlns:ws'=>'http://ws.setupfc.globalfoundries.com/',
        :'soapenv:Body'=>{:"ws:#{fname}"=>data}
      }}
      return @http.post(XMLHash.hash_to_xml(h))
    end

    # convenience method to extract the spec content as XML and version, optionally XML converted to hash
    def spec_content(spec, params={})
      res = getSpecification(spec, params[:version]) || return
      res = res[:getSpecificationResponse][:return][:result] || ($log.warn "no such spec: #{spec}"; return)
      specxml = Base64.decode64(res[:content])
      return params[:parsed] ? XMLHash.xml_to_hash(specxml) : [specxml, res[:version].to_i]
    end

    def createAttachment(params={})
      post_request('createAttachment')
    end

    def createSpecificationReference(params={})
      post_request('createSpecificationReference')
    end

    def deleteAttachment(params = {})
      post_request('deleteAttachment')
    end

    def deleteSpecificationReference(params={})
      post_request('deleteSpecificationReference')
    end

    def findSpecificationsByKeywords(params={})
      soap_params = {key: params[:key] || '', value: params[:value] || ''}
      soapBody = {'ws:specificationMetadata': {}, 'ws:keywords': soap_params}
      post_request('findSpecificationsByKeywords', soapBody)
    end

    def fSBK # not really find... but getall
      post_request('findSpecificationsByKeywords', {specificationMetadata: {}})
    end

    def findSpecificationsByMetadata(soap_params, params={})
      post_request('findSpecificationsByMetadata',  {'ws:specificationMetadata': soap_params})
    end

    def getAttachment(params={}) #"ws:`specificationId" => "", "ws:specificationVersion" => "", "ws:fileName" => ""})
      post_request('getAttachment')
    end

    def getDepartments
      post_request('getDepartments')
    end

    def getKeywordTypes
      post_request('getKeywordTypes')
    end

    def getKeywordValues(key)
      post_request('getKeywordValues', {'ws:keywordType': key})
    end

    def getSpecification(spec, version=nil)
      data = {'ws:specificationId': spec}
      data[:'ws:specificationVersion'] = version.to_s if version
      post_request('getSpecification', data)
    end

    def getSpecificationClasses
      post_request('getSpecificationClasses')
    end

    def getSpecificationHistory(spec_id, version=nil)
      version ||= '0'
      post_request('getSpecificationHistory', {'ws:specificationId': spec_id, 'ws:specificationVersion': version})
    end

    def getSpecificationSubClasses(class_id)
      post_request('getSpecificationSubClasses', {'ws:specificationClassId': class_id})
    end

    def getSpecificationMetadata(spec_id, version=nil)
      params = {'ws:specificationId': spec_id}
      params[:"ws:specificationVersion"] = version.to_s if version
      post_request('getSpecificationMetadata')
    end

    def getStackVersion(params={})  # ping
      post_request('getStackVersion')
    end

    # don't be worried about results - only class 07 specs
    def hasOpenAcknowledgements(user, params={})
      post_request('hasOpenAcknowledgements', {'ws:userName': user})
    end

    def submitSpecification(spec_id, content, params={})
      shortapproval = (params[:shortapproval] or false)
      post_request('submitSpecification', {'ws:specificationId': spec_id,
        'ws:specificationContent': content, 'ws:submitParameters': {'ws:useShortWorkflow': shortapproval}})
    end

    def updateSpecification(spec_id, content, params={})
      post_request('updateSpecification', {'ws:specificationId': spec_id, 'ws:specificationContent': content})
    end

    def getSpecificationWithContentTransformation(spec_id, version, stylesheet, params={})
      data = {'ws:specificationId': spec_id}
      data[:'ws:specificationVersion'] = version unless version.nil?
      data[:'ws:stylesheetName'] = stylesheet unless stylesheet.nil?
      post_request('getSpecificationWithContentTransformation', data)
    end

  end


  # SetupFC SOAP Client for QA test
  class QAClient
    attr_accessor :env, :http

    def initialize(env,params={})
      @env = env
      @http = RequestResponse::HttpXML.new("sfcsupport.#{env}", params)
    end

    def post_request(fname, data={})
      h = {:'soapenv:Envelope'=>{
        'xmlns:soapenv'=>'http://schemas.xmlsoap.org/soap/envelope/', 'xmlns:ws'=>'http://ws.setupfc.globalfoundries.com/',
        'xmlns:wor'=>'http://setupfc.globalfoundries.com/workflowconfig', :'soapenv:Body'=>{:"ws:#{fname}"=>data}
      }}
      return @http.post(XMLHash.hash_to_xml(h))
    end

    def submitSpecification(spec, task, params={})
      args = {arg0: spec, arg1: task, arg2: {useShortWorkflow: !!params[:shortapproval]}}
      post_request('submitSpecification', args)
    end

    def cancelSignoff(spec, requestor, params={})
      post_request('cancelSignoff', {arg0: spec, arg1: requestor, arg2: params[:comment] || 'Test'})
    end

    def canCancelSignoff(workflow_id, params)
      post_request('canCancelSignoff', {arg0: workflow_id})
    end

    def completeTask(task_id, transition, comment, props, params={})
      data = {arg0: task_id}
      data[:arg1] = transition if transition
      data[:arg2] = comment
      data[:arg3] = props
      post_request('completeTask', data)
    end

    def getApprovalStatus(task_id, params={})
      post_request('getApprovalStatus', {arg0: task_id})
    end

    # retrieve all active tasks, If spec_content is true the spec content will be retrieved
    def getAllActiveTasks(with_content, params={})
      post_request('getAllActiveTasks', {arg0: with_content})
    end

    # retrieve all active workflows, If spec_content is true the spec content will be retrieved
    def getAllActiveWorkflows(with_content, params={})
      post_request('getAllActiveWorkflows', {arg0: with_content})
    end

    def getCurrentWorkflowNode(workflow_id, params={})
      post_request('getCurrentWorkflowNode', {arg0: workflow_id})
    end

    def createSpecification(params={arg0: ''})
      post_request('createSpecification', params)
    end

    def createWorkingCopy(params={})
      post_request('createWorkingCopy', params)
    end

    def deleteSpecification(args, params={})
      post_request('deleteSpecification', args)
    end

    def deleteWorkingCopy(params={})
      post_request('deleteWorkingCopy', params)
    end

    def getActiveWorkflowsForSpec(spec_id)
      post_request('getActiveWorkflowsForSpec', {arg0: spec_id})
    end

    def getUserTaskList(params={})
      post_request('getUserTaskList')
    end

    def getPooledActorsForTask(task_id, params={})
      post_request('getPooledActorsForTask', {arg0: task_id})
    end

    def workOnSpecification(params={})
      post_request('workOnSpecification', params)
    end

    def transferTaskToUser(params)
      post_request('transferTaskToUser', params)
    end

    def createWorkflowConfig(wf_cfg, params={})
      post_request('createWorkflowConfig', {:arg0 => wf_cfg})
    end

    def updateWorkflowConfig(wf_cfg, params={})
      post_request('updateWorkflowConfig', {:arg0 => wf_cfg})
    end

    # delete workflow config workflowConfigId
    def deleteWorkflowConfig(wf_cfg_id, wf_set, params={})
      args = {'wor:metaInformation': {'wor:workflowConfigId': wf_cfg_id, 'wor:workflowSetId': wf_set}}
      post_request('deleteWorkflowConfig', {arg0: args})
    end

    # returns an array of workflow configurations for class/subclass
    def findWorkflowConfigs(wf_class, wf_subclass, params={})
      post_request('findWorkflowConfigs', params.merge(arg0: wf_class, arg1: wf_subclass))
    end

    def deleteKeywords(sp_id)
      # delete keywords for a given spec
      post_request('deleteKeywords', arg0: sp_id)
    end

    def getApprovalStatusFromSpecification(spec_id, params={})
        post_request('getApprovalStatusFromSpecification', {arg0: spec_id})
    end

    def getUsersForGroup(group, params={})
      post_request('getUsersForGroup', {arg0: group})
    end

  end

end
