=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author:  sfrieske, 2019-07-23

Version: 1.0.0

History:
  2021-08-27 sfrieske, require waitfor instead of misc

Notes:
  see http://f36wwd2:8000/data/IE/ie_dev_duedate_updater.xml
=end

require 'dbsequel'
require 'siview'
require 'util/waitfor'  # TODO: remove


module FODMS
  Target = Struct.new(:lot, :category, :route, :op, :opNo, :fod_commit, :fod_calc, :claim_time, :memo)

  class MDS
    attr_accessor :db

    def initialize(env, params={})
      @db = ::DB.new("mds.#{env}", params)
    end

    def inspect
      "#<FODMS::MDS #{@db.inspect}>"
    end

    def lot_target(lot, params={})
      qargs = []
      q = 'select LOT_ID, TARGET_CATEGORY, TARGET_MAINPD_ID, TARGET_PD_ID, TARGET_OPE_NO,
        OPE_END_TIME_COMMIT, OPE_END_TIME_CALC, CLAIM_TIME, CLAIM_MEMO from T_XF_LOT_TARGET'
      q, qargs = @db._q_restriction(q, qargs, 'LOT_ID', lot)
      q, qargs = @db._q_restriction(q, qargs, 'TARGET_CATEGORY', params[:category])
      q, qargs = @db._q_restriction(q, qargs, 'TARGET_PD_ID', params[:tgt_op])
      res = @db.select(q, *qargs) || return
      return res.collect {|r| Target.new(*r)}
    end

  end


  class Test
    attr_accessor :sv, :mds, :tgt_op, :jobdelay

    def initialize(env, params={})
      @sv = params[:sv] || SiView::MM.new(env, params)
      @mds = MDS.new(env)
      @tgt_op = params[:tgt_op] || 'FAB1OUT.01'  # target PD
      @jobdelay = params[:jobdelay] || 330
    end

    def inspect
      "#<FODMS::Test #{@sv.inspect}>"
    end

    # verify the job has worked
    def verify_lot_fod(lot, params={})
      $log.info "verify_lot_fod #{lot.inspect}"
      li = @sv.lot_info(lot)
      return wait_for(timeout: @jobdelay, tstart: params[:tstart]) {
        res = @mds.lot_target(lot).first
        res && res.category == 'FOD' && res.route == li.route && res.op == @tgt_op
      }
    end

  end

end
