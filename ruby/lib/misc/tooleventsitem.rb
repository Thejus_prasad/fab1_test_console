=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.


Author: Steffen Frieske, 2018-03-16

Version: 2.1.1

History:
  2021-08-30 sfrieske, moved xmlhash.rb to util
=end

require 'fileutils'
require 'time'
require 'mqjms'
require 'siview'
require 'util/xmlhash'


module ToolEvents

  # reads pre-defined messages from a zip file, adapts them (e.g. timestamp) and submits them to MQ
  class Item
    attr_accessor :sv, :mq, :nosend, :eqp, :tsformat, :events, :eventkeys, :defaultvalues, :msgno, :msgdelay, :exportdir, :_msgh, :_subst,
                  :cj, :cjinfo, :pstatus


    def initialize(env, eqp, params={})
      @sv = params[:sv] || SiView::MM.new(env)
      @mq = MQ::JMS.new("pdwhitem.#{params[:dbenv] || env}")  # dbenv is typically 'stress' if env is 'itdc'
      @eqp = eqp
      @tsformat = params[:tsformat] || 6  # ITEM 1.0/ATC 2.0 with ms
      @events = {
        # for claim_process_lot
        onslr: ['EIStarted', 'JobSubmitted'],
        onload: ['FOUPPlaced', 'CarrierAccepted', 'LotMapped', 'RecipeLoadStarted', 'RecipeLoadCompleted'], ##  'JobPreparationCompleted',
        onopestart1: ['E10StateChanged', 'JobStarted'], #  during job: 'BatchStarted', ProcessStarted/Completed... 'BatchCompleted'
        onopestart9: ['JobCompleted'],
        onopecomp: ['E10StateChanged', 'DataCollectionStarted'],
        onunload: ['FOUPRemoved', 'DataCollectionCompleted', 'LotDismapped']
        # E10StateChanged not used
      }
      @eventkeys = {
        #
        # regular processing
        'EIStarted'=>['SiViewEntity', 'EIVersion', 'BaselineVersion', 'ToolSWVersion'],
        'JobSubmitted'=>['Job', 'Wfr_Job', 'LogicalRecipe', 'PhysicalRecipe'],
        'FOUPPlaced'=>['Port'],
        'CarrierAccepted'=>['Foup', 'Port', 'Wfr_Foup'],
        'LotMapped'=>['Lot', 'Foup', 'Job', 'Wfr_Lot', 'LotType', 'SubLotType', 'Tech', 'MainPD', 'ProdGrp', 'ProdSpec', 'PD',
                      'LogicalRecipe', 'PhysicalRecipe'],
        'RecipeLoadStarted'=>['Job', 'LogicalRecipe', 'PhysicalRecipe'],
        'RecipeLoadCompleted'=>['Job', 'LogicalRecipe', 'PhysicalRecipe'],
        'E10StateChanged'=>['SiViewEntity', 'E10State'],  # E10State for SiViewEntity and optionally 'SubEquipment' (chambers)
        'JobStarted'=>['Job', 'Wfr_Job'],
        # ...
        'JobCompleted'=>['Job', 'Wfr_Job'],
        # E10StateChanged to '2WPR'
        'DataCollectionStarted'=>['Job'],
        'FOUPRemoved'=>['Foup', 'Port'],
        'DataCollectionCompleted'=>['Job'],
        'LotDismapped'=>['Lot', 'Job', 'Wfr_Lot'],
        #
        # RDI reticle events
        'ReticleReserved'=>['ReticleId', 'Job', 'VisitID', 'DedicatedToolGroup', 'DedicatedPixelSize', 'PortOperationMode',
          'EquipmentOnlineMode', 'ReticleRequestor', 'Rotation'],
        'ReticlePodAt'=>['Port', 'Foup', 'Reticle'],
        'ReticlePodFrom'=>['Port', 'Foup', 'Reticle'],
        'ReticleAt'=>['ReticleId', 'SubEquipment'],
        'ReticleFrom'=>['ReticleId', 'SubEquipment'],
        'ReticleStarted'=>['Port', 'Foup', 'ReticleId', 'Job', 'VisitID', 'EquipmentOnlineMode', 'PortOperationMode'],
        'InspectionStepStarted'=>['ReticleId', 'Job', 'VisitID', 'ProcessStep', 'InspectionID'],
        'ReticleProcessStarted'=>['ReticleId', 'Job', 'VisitID', 'InspectionID', 'MainProcessRecipeName',
          'PixelSize', 'DedicatedPixelSize'],
        'ReticleProcessCompleted'=>['ReticleId', 'Job', 'VisitID', 'InspectionID'],
        'ReticleCompleted'=>['Port', 'Foup', 'ReticleId', 'Job', 'VisitID', 'EquipmentOnlineMode', 'PortOperationMode'],
        'ReticleLeftEquipment'=>['ReticleId', 'Job', 'VisitID', 'InspectionID']
      }
      @defaultvalues = {
        # regular
        'SiViewEntity'=>@eqp, 'EIVersion'=>'10.1.00', 'BaselineVersion'=>'f36cei10.1. - patch 2', 'ToolSWVersion'=>'.9_144',
        'Job'=>'QAXXX-01-03', 'Wfr_Job'=>25, 'LogicalRecipe'=>'QA', 'PhysicalRecipe'=>'QARCP',
        'Port'=>'P1', 'Foup'=>'QAFOUP', 'Wfr_Foup'=>25, 'Lot'=>'QAQA000.000', 'Wfr_Lot'=>25,
        'LotType'=>'Production', 'SubLotType'=>'PO', 'Tech'=>'28NM', 'MainPD'=>'QA-ROUTE01.00', 'ProdGrp'=>'QAPG',
        'ProdSpec'=>'QA-PRODUCT01.00', 'PD'=>'QAPD01.01',
        'E10State'=>'1PRD',
        # RDI unless defined above
        'ReticleId'=>'QARETICLE', 'Reticle'=>'QARETICLE', 'MainProcessRecipeName'=>'QARETICLE-Full-Standard-P090-X5', 'ProcessStep'=>'QASTEP1',
        'VisitID'=>"#{@eqp}_#{Time.now.utc.to_s.gsub(' ', '_')}", 'DedicatedToolGroup'=>'KLA_XR',
        'SubEquipment'=>'QAFOUP.1', 'EquipmentOnlineMode'=>'On-Line Remote', 'PortOperationMode'=>'Auto-3',
        'DedicatedPixelSize'=>'P090', 'PixelSize'=>'P090', 'ReticleRequestor'=>'Auto', 'Rotation'=>'Library.Right',
        'InspectionID'=>''
      }
      @msgdelay = params[:msgdelay] || 1
      @msgno = -1
    end

    def inspect
      "#<#{self.class.name} #{@sv.inspect}>"
    end

    def export(exp)
      if exp
        @exportdir = exp.kind_of?(String) ? exp : "log/item/#{@eqp}"
        FileUtils.mkdir_p(@exportdir)
      else
        @exportdir = nil
      end
    end

    def send_msgs(evnames)
      evnames = @events[evnames] if evnames.kind_of?(Symbol)
      ##$log.info "send_msgs #{evnames}"
      evnames.each {|evname| send_msg(evname)}
    end

    def send_msg(evname, params={})
      $log.info "send_msg #{evname.inspect}"
      sleep @msgdelay
      ts = Time.now.utc.iso8601(@tsformat)  # Windows fills only 3 digits (ms)
      no = params[:msgno] || (@msgno += 1)
      data = params[:data] || {}
      state = @eventkeys[evname].collect {|k| {'Name'=>k, 'Content'=>data[k] || @defaultvalues[k], nil=>''}}
      state << {'Name'=>'EventNumber', 'Content'=>no, nil=>''}
      h = {EquipmentEvents: {EquipmentEvent: {Date: ts, Equipment: @eqp, Event: {EventName: evname, State: state}}}}
      @_msgh = h
      s = XMLHash.hash_to_xml(h)
      @_msgx = s
      File.binwrite("#{@exportdir}/%3.3d_#{evname}.xml" % @msgno, s) if @exportdir
      return s if @nosend
      @mq.send_msg(s) || ($log.warn '  error sending msg'; return)
    end

    def claim_process_lot(lot, params={})
      @msgno = -1
      parms = {eqp: @eqp, xfer: params[:xfer],
        onslr: proc {|cj, cjinfo, eqpiraw, pstatus|
          @cj = cj
          @cjinfo = cjinfo
          @pstatus = pstatus
          li = @sv.lot_info(lot)
          @defaultvalues['Job'] = cj
          @defaultvalues['Wfr_Job'] = li.nwafers
          @defaultvalues['Port'] = pstatus.portID.identifier
          @defaultvalues['Foup'] = li.carrier
          @defaultvalues['Wfr_Foup'] = li.nwafers
          @defaultvalues['Lot'] = li.lot
          @defaultvalues['Wfr_Lot'] = li.nwafers
          @defaultvalues['LotType'] = li.lottype
          @defaultvalues['SubLotType'] = li.sublottype
          @defaultvalues['Tech'] = li.technology
          @defaultvalues['MainPD'] = li.route
          @defaultvalues['ProdGrp'] = li.productgroup
          @defaultvalues['ProdSpec'] = li.product
          @defaultvalues['PD'] = li.op
          @defaultvalues['E10State'] = '1PRD'
          # get recipes
          lislr, pg, ib, eqpiraw = @sv.lots_info_slr(@eqp, lot, port: pstatus.portID.identifier)
          rcpstruct = lislr.strStartCassette[0].strLotInCassette[0].strStartRecipe
          @defaultvalues['LogicalRecipe'] = rcpstruct.logicalRecipeID.identifier
          @defaultvalues['PhysicalRecipe'] = rcpstruct.physicalRecipeID
          #
          send_msgs(:onslr)
        },
        onload: proc {|*args| send_msgs(:onload)},
        onopestart: proc {|*args|
          send_msgs(:onopestart1)
          # wafer events ...
          send_msgs(:onopestart9)
        },
        onopecomp: proc {|*args|
          @defaultvalues['E10State'] = '2WPR'
          send_msgs(:onopecomp)
        },
        onunload: proc {|*args| send_msgs(:onunload)},
      }
      return @sv.claim_process_lot(lot, parms)
    end

    # send msgs for a reticle move from one location via intermediate ones to the final location, return true on success
    def reticle_move(subeqps, params={}, &blk)
      ($log.warn "reticle_move: subeqps must be an array"; return) unless subeqps.kind_of?(Array) && subeqps.size > 1
      send_msg('ReticleFrom', data: {'SubEquipment'=>subeqps.first}) || return
      (blk.call(subeqps.first, 0) || return) if blk
      subeqps[1..-2].each_with_index {|subeqp, idx|
        send_msg('ReticleAt', data: {'SubEquipment'=>subeqp}) || return
        send_msg('ReticleFrom', data: {'SubEquipment'=>subeqp}) || return
        (blk.call(subeqp, idx + 1) || return) if blk
      }
      send_msg('ReticleAt', data: {'SubEquipment'=>subeqps.last}) || return
    end

  end

end
