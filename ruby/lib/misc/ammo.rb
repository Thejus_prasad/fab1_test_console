=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.


Author:     ssteidte, 2011-08-26; dsteger, 2013-05-08
Version:    1.2.2

History:
  2013-12-10 ssteidte, added echo, get_selected_wafers
  2016-08-24 sfrieske, use lib rqrsp_mq instead of rqrsp
  2016-12-08 tklose,   added set_wafer_selection_by_map
  2018-04-09 sfrieske, use hash_to_xml instead of builder
  2021-02-05 sfrieske, use instance_of instead of kind_of
  2021-07-20 sfrieske, use rexml/document directly

Notes:
  cf https:/gfoundries-my.sharepoint.com/:f:/p/jnothnag/Ek7LKlipnG5Op0L2tCI99ycBBwYhCYJdyiRNxD0vbdOkPg?e=hiwTbW
  setWaferSelection disabled in V6 (2021)
  Standard usage:
  - APC or FabGUI system user calls set_wafer_selection_bymap, APC calls set_wafer_selection_by_map_sub
  - EI and APC call get_wafer_selection
  - APC or FabGUI call remove_wafer_selection

  logs ITDC: f36asd9:/local/logs/ammo/
=end


require 'fileutils'       # export response for dev/comparison
require 'rexml/document'
require 'util/xmlhash'    # well formatted output for dev/comparison
require 'rqrsp_mq'


module AMMO

  # AMMO MQ interface
  class Client
    attr_accessor :mq, :logdir, :faultstring, :_doc

    def initialize(env, params={})
      @mq = RequestResponse::MQXML.new("ammo.#{env}")
      @logdir = params[:logdir]
      FileUtils.mkdir_p(@logdir) if @logdir
    end

    def inspect
      "#<#{self.class.name} #{mq.inspect}>"
    end

    def check_result(res, params)
      if fname = params[:export]
        s = XMLHash.pretty_format(@mq.response)
        File.binwrite(File.join(@logdir, fname), s)
      end
      if res[:Fault]
        @faultstring = res[:Fault][:faultstring]
        $log.warn @faultstring
        if fname_fault = params[:export_fault]
          s = XMLHash.pretty_format(@mq.response)
          File.binwrite(File.join(@logdir, fname_fault), s)
        end
        return
      end
      return true
    end

    def new_doc
      @_doc = doc = REXML::Document.new('<?xml version="1.0" encoding="UTF-8"?>')
      return doc.add_element('soapenv:Envelope', {
        'xmlns:soapenv'=>'http://schemas.xmlsoap.org/soap/envelope/',
        'xmlns:soapenc'=>'http://schemas.xmlsoap.org/soap/encoding/',
        'xmlns:xsd'=>'http://www.w3.org/2001/XMLSchema',
        'xmlns:xsi'=>'http://www.w3.org/2001/XMLSchema-instance',
        'xmlns:ns'=>'http://www.amd.com/ammo',
      }).add_element('soapenv:Body')
    end

    # return true if the passed string is returned
    def echo(params={})
      s = params[:msg] || 'QA'
      tag = new_doc.add_element('ns:echoString')
      tag.add_element('input').text = s
      res = @mq.send_receive(tag.document) || return
      ($log.warn "wrong response:\n#{res}"; return) if res[:echoStringResponse][:echoStringReturn][nil] != "Received XML message: #{s}"
      return true
    end

    # wafers: wafer ids, ptype: PD UDATA 'ProcessTypeWfrSelection' (e.g. DISPO), player: PD UDATA 'ProcessLayer' (e.g. INIT)
    # return Hash of wafers with selection data or nil if Fault returned
    def get_selected_wafers(wafers, ptype, player, params={})
      # $log.info "get_selected_wafers (#{wafers.size})  #{ptype.inspect}, #{player.inspect}, #{params}"
      @faultstring = nil
      tag = new_doc.add_element('ns:getSelectedWafers')
      tagw = tag.add_element('waferIDs')
      wafers.each {|w| tagw.add_element('item').text = w}
      tag.add_element('operationType').text = ptype
      tag.add_element('photoLayer').text = player
      res = @mq.send_receive(tag.document) || return
      check_result(res, params) || return
      return true if params[:parse] = false
      # parse the multiref response
      h = XMLHash.xml_to_hash(@mq.response, '/*/*Body').values.first
      h2 = h[:getSelectedWafersResponse][:getSelectedWafersReturn][:getSelectedWafersReturn]
      hrefs = h2.instance_of?(Array) ? h2.collect {|e| e['href']} : [h2['href']]
      ret = {}
      hrefs.each {|href|
        waferdata = {}
        #
        id = href[1..-1]  # "#id0" -> 'id0'
        record = h[:multiRef].find {|e| e['id'] == id}
        waferid = record[:waferID][nil]
        waferdata = {operationType: record[:operationType][nil], photoLayer: record[:photoLayer][nil]}
        #
        idsel = record[:selectionData]['href'][1..-1]
        recordsel = h[:multiRef].find {|e| e['id'] == idsel}
        waferdata[:reason] = recordsel[:reason][nil]
        waferdata[:selectingSystem] = recordsel[:selectingSystem][nil]
        waferdata[:timestamp] = recordsel[:timestamp][nil]
        waferdata[:userID] = recordsel[:userID][nil]
        #
        idselflag = recordsel[:selectFlag]['href'][1..-1]
        recordselflag = h[:multiRef].find {|e| e['id'] == idselflag}
        waferdata[:selectFlag] = recordselflag[nil].to_i
        ret[waferid] = waferdata
      }
      return ret
    end

    # wafers: wafer ids, ptype: PD UDATA 'ProcessTypeWfrSelection' (e.g. DISPO), player: PD UDATA 'ProcessLayer' (e.g. INIT)
    # return (potentially empty) array of actually selected wafers
    def get_only_selected_wafers(wafers, ptype, player, params={})
      @faultstring = nil
      tag = new_doc.add_element('ns:getOnlySelectedWafers')
      tagw = tag.add_element('waferIDs', {'soapenc:arrayType'=>"xsd:string[#{wafers.size}]"})
      wafers.each {|w| tagw.add_element('item').text = w}
      tag.add_element('operationType').text = ptype
      tag.add_element('photoLayer').text = player
      res = @mq.send_receive(tag.document) || return
      check_result(res, params) || return
      return true if params[:parse] = false
      res2 = res[:getOnlySelectedWafersResponse][:getOnlySelectedWafersReturn][:getOnlySelectedWafersReturn]
      return [] if res2.nil?
      return res2.instance_of?(Array) ? res2.collect {|e| e[nil]} : [res2[nil]]
    end

    # wafers: wafer ids, ptype: PD UDATA 'ProcessTypeWfrSelection' (e.g. DISPO), player: PD UDATA 'ProcessLayer' (e.g. INIT)
    # return Hash of wafers with selection data or nil if Fault returned
    # note: selectFlag 4 means wafer unknown
    def get_all_selected_wafers_noconflict(wafers, ptype, player, params={})
      @faultstring = nil
      tag = new_doc.add_element('ns:getAllSelectedWafersNoConflict')
      tagw = tag.add_element('waferIDs', {'soapenc:arrayType'=>"xsd:string[#{wafers.size}]"})
      wafers.each {|w| tagw.add_element('item').text = w}
      tag.add_element('operationType').text = ptype
      tag.add_element('photoLayer').text = player
      res = @mq.send_receive(tag.document) || return
      check_result(res, params) || return
      return true if params[:parse] = false
      # parse the multiref response
      h = XMLHash.xml_to_hash(@mq.response, '/*/*Body').values.first
      h2 = h[:getAllSelectedWafersNoConflictResponse][:getAllSelectedWafersNoConflictReturn][:getAllSelectedWafersNoConflictReturn]
      hrefs = h2.instance_of?(Array) ? h2.collect {|e| e['href']} : [h2['href']]
      ret = {}
      hrefs.each {|href|
        waferdata = {}
        #
        id = href[1..-1]  # "#id0" -> 'id0'
        record = h[:multiRef].find {|e| e['id'] == id}
        waferid = record[:waferID][nil]
        waferdata = {operationType: record[:operationType][nil], photoLayer: record[:photoLayer][nil]}
        seldata = record[:selectionData][:selectionData]
        if seldata.instance_of?(Array)
          idsel = seldata.first['href'][1..-1]
        else
          idsel = seldata['href'][1..-1]
        end
        #
        recordsel = h[:multiRef].find {|e| e['id'] == idsel}
        waferdata[:reason] = recordsel[:reason][nil]
        waferdata[:selectingSystem] = recordsel[:selectingSystem][nil]
        waferdata[:timestamp] = recordsel[:timestamp][nil]
        waferdata[:userID] = recordsel[:userID][nil]
        #
        idselflag = recordsel[:selectFlag]['href'][1..-1]
        recordselflag = h[:multiRef].find {|e| e['id'] == idselflag}
        waferdata[:selectFlag] = recordselflag[nil].to_i
        ret[waferid] = waferdata
      }
      return ret
    end

    # pass selectflag: 1 (must), 2 (excluded), 3: (do not care), anything else: delete (destructive!!, broken?)
    # return true on success (nothing useful in the response)
    def set_wafer_selection_bymap(wafers, ptype, player, selectflag, params={})
      $log.info {"set_wafer_selection_bymap (#{wafers.size} wafers)  #{ptype.inspect}, #{player.inspect}, #{selectflag}, #{params}"} unless params[:silent]
      @faultstring = nil
      tag = new_doc.add_element('ns:setWaferSelectionByMap', {
        'soapenv:encodingStyle'=>'http://schemas.xmlsoap.org/soap/encoding/'
      })
      tagd = tag.add_element('waferSelectionDataMap', {
        'xsi:type'=>'ns:ArrayOf_tns2_WaferSelectionDataMap',
        'soapenc:arrayType'=>'waf:WaferSelectionDataMap[]',
        'xmlns:waf'=>'http://waferselection.ammo.amd.com'
      })
      tagi = tagd.add_element('item', {'xsi:type'=>'waf:WaferSelectionDataMap'})
      tagw = tagi.add_element('waferIDs', {
        'xsi:type'=>'ns:ArrayOf_tns2_String',
        'soapenc:arrayType'=>'waf:ArrayOfString[]',
        'xmlns:waf'=>'http://waferselection.ammo.amd.com'
      })
      wafers.each {|w| tagw.add_element('item').text = w}
      tagi.add_element('operationType').text = ptype
      tagi.add_element('photoLayer').text = player
      tagd = tagi.add_element('selectionData', {
        'xsi:type'=>'waf:WaferSelectionData',
        'xmlns:waf'=>'http://waferselection.ammo.amd.com'
      })
      tagd.add_element('reason').text = 'QA Test'
      tagd.add_element('selectFlag', {'type'=>'xsd:int'}).text = selectflag
      tagd.add_element('selectingSystem').text = params[:system] || 'APCUSER'     # 'GUIUSER', 'SUPERUSER'
      tagd.add_element('timestamp').text = params[:timestamp] || Time.now.strftime('%F %T.%L')
      tagd.add_element('userID').text = 'QATester'
      res = @mq.send_receive(tag.document) || return
      check_result(res, params) || return
      return true
    end

    # unknown data structure, used by Catalyst?
    def XXset_wafer_selection_by_map_sub(wafers, ptype, player, params={})
      $log.info "set_wafer_selection_bymap_sub (#{wafers.size})  #{ptype.inspect}, #{player.inspect}, #{params}"
    end

    # never fails, even if a wafer has no selection, as long as selectingSystem is valid
    def remove_wafer_selection(wafers, ptype, player, params={})
      $log.info {"remove_wafer_selection (#{wafers.size} wafers)  #{ptype.inspect}, #{player.inspect}, #{params}"} unless params[:silent]
      @faultstring = nil
      tag = new_doc.add_element('ns:removeWaferSelectionForContext')
      tagw = tag.add_element('waferIDs', {'soapenc:arrayType'=>"xsd:string[#{wafers.size}]"})
      wafers.each {|w| tagw.add_element('item').text = w}
      tag.add_element('operationType').text = ptype
      tag.add_element('photoLayer').text = player
      tag.add_element('selectingSystem').text = params[:system] || 'APCUSER'     # 'GUIUSER', 'SUPERUSER'
      res = @mq.send_receive(tag.document) || return
      check_result(res, params) || return
      return true
    end

  end

end
