=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: Steffen Steidten, 2010-05-07

Version: 1.45.3

History:
  2010-08-05 ssteidte, made get_reports generic, only msg_types needs to be updated to add a new feed
  2010-08-05 ssteidte, ReportMsg.report_time is now a Time object instead of String
  2010-08-31 ssteidte, added support for more feeds
  2010-09-08 ssteidte, @product, @route and @sapcustomer are not overwritten by new_lot
  2010-10-04 ssteidte, added ULL support through MDS
  2010-11-12 tklose,   added support for MES-Oracle Interface
  2010-11-23 ssteidte, added STAGE_NO support
  2011-03-10 tklose,   fixed new_lot, add parameter datatypes
  2011-08-16 ssteidte, added cp_verify_date_format
  2012-01-24 ssteidte, use Sequel instead of DBI for databases
  2012-05-04 dsteger,  changed to erpitem and customer
  2012-06-15 ssteidte, removed SAP related tests
  2012-07-19 ssteidte, use XMLHash
  2013-06-25 ssteidte, added wafer_history to lot_history
  2013-10-23 ssteidte, use HDS instead of MDS for MDM and SDM access
  2014-12-03 ssteidte, removed cs_verify_date_format and cs_verify_timestamp_format
  2014-12-05 ssteidte, moved MDS queries from CP to CPTest
  2015-09-24 sfrieske, call m_rollback before starting a loader
  2016-10-20 sfrieske, read data from MQ msgs instead of XML files
  2017-07-17 sfrieske, remove @lserver.reconnect, handled by @lserver.exec!
  2017-08-30 tklose,   adding partudata
  2021-02-05 sfrieske, use instance_of instead of kind_of
  2021-08-30 sfrieske, moved xmlhash.rb to util
=end

require 'mqjms'
require 'remote'
require 'util/xmlhash'


# Test Common Platform data feeds.
class CP
  attr_accessor :mq, :lserver, :_loader_env, :loader_run_durations, :reports, :record_counts, :msg_types

  def initialize(env, params={})
    @mq = MQ::JMS.new("cp.#{env}")
    @lserver = RemoteHost.new("wipetl.#{env}")
    @_loader_env = params[:loaderenv] || 'bin/prodenv.ksh'
    @loader_run_durations = {}
    # reports are the messages read from a feed
    @reports = {}
    @record_counts = {}
    # msg_types are the msg_type attributes for each feed,
    # feeds are the names of the files created by the loaders
    @msg_types = {
      'wip_status'=>:WIPStatusMsg,
      'wip_wafer'=>:WIPWaferMsg,
      'reticle_runsheet'=>:RunSheetVsReticleMsg,
      'part'=>:PartMsg,
      'part_udata'=>:PartUDataMsg,
      'lot_start'=>:StartMsg,
      'lot_wfr_start'=>:StartwfrMsg,
      'wip_attribute'=>:WipAttribMsg,
      'fab_out'=>:FaboutMsg,
      'etest_event'=>:ETestEventMsg,
      'lot_hold_release'=>:LotHoldReleaseMsg,
      'lot_split_wafer'=>:LotSplitWaferMsg,
      'lot_merge_wafer'=>:LotMergeWaferMsg,
      'lot_stage_move'=>:LotStageMoveMsg,
      'lot_complete'=>:LotCompleteMsg,
      'lot_attribute_change'=>:LotAttribChangeMsg,
      'lot_ship_details'=>:LotShipDetailsMsg,
      'tk_subcon_alert'=>:TKSubconAlertMsg,
      'runsheet'=>:RunsheetMsg,
      'lot_ship_goods'=>:LotShipGoodsMsg,
      'equipment_current_info'=>:EquipmentCurrentInfoMsg,
      'plan_start'=>:PlanStartMsg,
      'scrap'=>:ScrapMsg,
      'rework'=>:LotReworkMsg,
      'source_products'=>:SourceProductsMsg,
      'lot_history'=>:LotHistoryMsg,
      'wafer_history'=>:LotHistoryWaferMsg,
      'recipeidlist'=>:RecipeidlistMsg
    }
    $log.info self.inspect
  end

  def inspect
    "#<#{self.class.name} #{@lserver.inspect}>"
  end

  # load the current report and count messages, return true on success
  def get_reports(feed, params={})
    $log.info "get_reports #{feed.inspect}"
    @reports.delete(feed)
    @record_counts.delete(feed)
    msgs = @mq.read_msgs || return
    ($log.warn 'no messages in the queue'; return) if msgs.empty?
    msgs.each {|data|
      s = data[0..128]
      if s.empty?
        $log.warn "empty msg"
        return
      elsif s.include?('<DataFeedRecordCountMsg')
        h = XMLHash.xml_to_hash(data)
        @record_counts[feed] = h[:DataFeedRecordCountMsg][:RecordCountMsgRec][:DataFeedRecordCount].to_i
      elsif s.include?("<#{@msg_types[feed]}")
        File.binwrite("log/#{feed}.xml", data) if params[:export]
        @reports[feed] = msg = ReportMsg.new(data)
        mtype = params[:msg_type] || @msg_types[feed]
        ($log.warn "wrong message type: #{msg.msg_type} instead of #{mtype.inspect}"; return) if msg.msg_type != mtype
      end
    }
    return !!@reports[feed]
  end

  # compare number of data records with record_count, return true on success
  def compare_record_count(feed)
    r = @reports[feed] || return
    rr = r.records || return
    rc = @record_counts[feed] || return
    ret = (rr.size == rc)
    $log.warn "wrong record count report: #{rc}, got #{rr.size} messages" unless ret
    return ret
  end

  # start the loader and wait for completion, return true on success
  def run_loader(loader, runargs=nil)
    @mq.delete_msgs(nil)
    cmd = "$AI_BIN/run_#{loader}.ksh"
    cmd += " " + runargs if runargs   # e.g. '-LOT_ID XXX'
    $log.info "running loader #{cmd.inspect}"
    ## not necessary  @lserver.reconnect
    sleep 1
    tstart = Time.now
    res = @lserver.exec!(". #{@_loader_env}; test -f #{loader}.rec && m_rollback -d -k #{loader}.rec; #{cmd}")
    @loader_run_durations[loader] = duration = (Time.now - tstart).round(1)
    $log.info "loader run duration: #{duration} s"
    res
  end

  # wait for MDS sync, run loader and get the reports, return true on success
  def update_reports(sleeptime, loader, params={})
    $log.info "waiting #{sleeptime} s before running the #{loader} loader"
    sleep sleeptime
    run_loader(loader, params.delete(:runargs))
    get_reports(loader, params)
  end
end

# report contents
class ReportMsg
  attr_accessor :msg_type, :report_time, :records

  # initialize from String, s is interpreted as file name if it ends with ".xml"
  def initialize(s)
    ($log.warn "message missing: nil"; return) unless s
    ($log.info "reading file #{s}"; s = File.read(s)) if s.end_with?('.xml')
    res = XMLHash.xml_to_hash(s)
    @msg_type = res.keys[0]
    @report_time = Time.parse(res[@msg_type]['ReportTimeStamp'])
    $log.info "loaded #{@msg_type} from #{@report_time.utc.strftime('%F_%T')}"
    k = res[@msg_type].keys[1]
    @records = res[@msg_type][k]
    @records = [@records] unless @records.instance_of?(Array)
  end

  def inspect
    "#<#{self.class.name} #{@msg_type.inspect}>"
  end

  # return an array of records with key field matching value, e.g. LOT_ID="..."
  def get_records(k, v, params={})
    tfield = params[:tfield] || :CLAIM_TIME
    t = params[:timestamp] || params[:time] || params[:t]
    t = Time.parse(t) if t.instance_of?(String)
    spread = params[:spread] || 15    # 15 seconds default
    @records.collect {|r|
      # check main selection, e.g. LOT_ID
      next if k && v && r[k] != v
      # check the optional time field
      if t
        s = r[tfield]
        next if s && (Time.parse(s) - t).abs > spread
      end
      r
    }.compact
  end

  # return an array of records with LOT_ID field matching value
  def get_records_lot(v, params={})
    get_records(:LOT_ID, v, params)
  end

end
