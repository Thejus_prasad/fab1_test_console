=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: Steffen Steidten, 2011-12-12

Version: 1.47

History:
  2014-12-05 ssteidte, moved MDS queries from CP to CPTest
  2015-04-30 tklose,   removed lot script parameter RFB
  2015-07-07 ssteidte, fixed siview_time calls
  2016-04-14 sfrieske, adapted to SiView R15 LOT_LABEL
  2016-10-18 sfrieske, calculate LAST_STAGE_NUMBER and LAST_MASK_COUNT from SM data
  2017-05-08 sfrieske, replaced select_one by select
  2017-07-17 sfrieske, added check for LOT_LOCATION in WipStatus
  2017-08-30 tklose,   added verify_part_udata_data
  2018-05-09 sfrieske, minor adjustments for scrapped lots with AutoScrap jCAP job interfering
  2019-01-08 sfrieske, use erpmdm for TK_SUBCON_ALERT
  2019-02-12 sfrieske, dont inherit from SiView::Test
  2020-02-12 sfrieske, clean up use of lot_operation_list_fromhistory
  2021-02-05 sfrieske, use instance_of instead of kind_of
  2021-08-30 sfrieske, moved cp.rb and cptest.rb to misc
=end

require 'time'
require 'siview'
require 'jcap/erpmdm'
require_relative 'cp'


# Test Common Platform (CPDI Mfg) data feeds.
class CPTest
  attr_accessor :sv, :sm, :cp, :mdm, :skip_lot_out_tests

  def initialize(env, params={})
    @sv = params[:sv] || SiView::MM.new(env, params)
    @sm = params[:sm] || SiView::SM.new(env, params)
    @cp = CP.new(env, params)
    @mdm = ERPMDM.new(env, params)
    @skip_lot_out_tests = params[:skip_lot_out_tests] != false
  end

  # MDS and SiView data extraction

  # return OSD as Date object or nil
  def get_OSD(lot)
    res = @mdm.db.select('select OSD from t_lot_out_dates where LOT_ID = ?', lot).first
    return res ? res.first : res
  end

  # return CSD as Date object or nil
  def get_CSD(lot)
    res = @mdm.db.select('select CSD from t_lot_out_dates where LOT_ID = ?', lot).first
    return res ? res.first : res
  end

  def total_stage_count(route)
    oinfo = @sm.object_info(:mainpd, route, details: true).first
    return oinfo.specific[:modules].map {|e| e[:stage] unless e[:stage].empty?}.compact.uniq.size
  end

  def total_mask_count(route)
    oinfo = @sm.object_info(:mainpd, route, details: true).first
    return oinfo.specific[:operations].map {|e| e.photolayer unless e.photolayer.empty?}.compact.uniq.size
  end

  def curr_mask_count(lot)
    rops = @sv.lot_operation_list(lot, forward: false, history: true) || return
    ret = {}
    count = 0
    mask = ''
    rops.reverse.each {|rop|
      opmask = rop.masklevel
      if opmask != mask && !opmask.empty?
        mask = opmask
        count += 1
      end
      ret[rop.opNo] = count
    }
    return ret
  end

  # return Y or N
  def get_expediteorder(lot)
    ret = @sv.user_parameter('Lot', lot, name: 'ExpediteOrder').value
    ret = 'N' if [nil, ''].member?(ret)
    return ret
  end

  # field checks
  def field_exists(r, fieldname, msg='')
    # return true on success
    ($log.warn "no field #{fieldname.inspect} #{msg}"; return) if r[fieldname].nil?
    return true
  end

  # return true on success
  def field_not_empty(r, fieldname, msg='')
    if field_exists(r, fieldname, msg)
      ($log.warn "empty field #{fieldname.inspect} #{msg}"; return) if r[fieldname].strip == ''
      return true
    end
    return
  end

  # return true on success
  def cmp_timestamp_format(r, fieldname, msg='')
    return unless field_exists(r, fieldname, msg)
    s = r[fieldname]
    begin
      Time.parse(s)
      # test significant elements, format is "2010-09-08T09:58:36.507000Z"
      ret = s.size == 27 and s[13].chr == ':' and s[16].chr == ':' and s[19].chr == '.' and s[26].chr == 'Z'
    rescue
      ret = false
    end
    #
    $log.warn "wrong timestamp format: #{fieldname.inspect}: #{r[fieldname].inspect} #{msg}" unless ret
    return ret
  end

  # return true on success
  def cmp_timestamp(r, fieldname, params={})
    cmp_timestamp_format(r, fieldname, params[:msg]) || return
    spread = params[:spread] || 20*60
    tref = params[:tref] || Time.now
    tref = Time.parse if tref.instance_of?(String)
    if params[:utc_fix] # for old run_sheet data
      tref -= 7200 if tref < Time.parse('2015-05-11T10:00:00Z')
    end
    ret = (tref - Time.parse(r[fieldname])).abs < spread
    $log.warn "wrong timestamp #{fieldname}: #{r[fieldname]} instead of #{tref.utc.iso8601}" unless ret
    return ret
  end

  # return true on success
  def cmp_date_format(r, fieldname, msg='')
    field_exists(r, fieldname, msg) || return
    s = r[fieldname]
    begin
      t = Time.parse(s)
      # test significant elements, format: "2010-09-08"
      ret = s.size == 10 and s[4].chr == '-' and s[7].chr == '-' and t.hour == 0 and t.min == 0 and t.sec == 0
    rescue
      ret = false
    end
    $log.warn "wrong date format: #{fieldname.inspect}: #{r[fieldname].inspect} #{msg}" unless ret
    return ret
  end

  # return true on success
  def cmp_field(v, r, fieldname, msg='')
    if v != nil
      field_exists(r, fieldname, msg) || return
      if r[fieldname] != v
        $log.warn "wrong #{fieldname.inspect}: expected #{v.inspect}, got #{r[fieldname].inspect} #{msg}"
        return
      end
    end
    return true
  end

  # return true on success
  def cmp_user_param(lot, parameter, r, fieldname, msg='')
    up = @sv.user_parameter('Lot', lot, name: parameter)
    ($log.warn "no SiView user parameter #{parameter.inspect}"; return true) unless up and up.valueflag
    v = up.value
    res = cmp_field(v, r, fieldname, msg)
    return v != '' ? res : true
  end

  # return true on success
  def cmp_LOT_LABEL(lot, r, fieldname, msg='')
    ull = @sv.lot_label(lot)
    if ull.empty?
      $log.info "  no SiView LOT_LABEL"
      ($log.warn "  wrong field #{fieldname}"; return) if r[fieldname] && !r[fieldname].empty?
      return true
    else
      return cmp_field(ull, r, fieldname, msg)
    end
  end

  # return true on success
  def cmp_SALES_ORDER_NUMBER(lot, r, fieldname, msg='')
    return true if @skip_lot_out_tests
    order = @sv.user_parameter('Lot', lot, name: 'ERPOrderNumber').value
    ($log.warn "no SiView ERPOrderNumber for lot #{lot}"; return) if order.empty?
    cmp_field(order, r, fieldname, msg)
  end

  # return true on success
  def cmp_ORACLE_ITEM_ID(lot, r, fieldname, msg='')
    oid = @sv.user_parameter('Lot', lot, name: 'ERPItem').value
    ($log.warn "ERPItem for lot #{lot} not set"; return) if oid.empty?
    cmp_field(oid, r, fieldname, msg)
  end

  # feed record checks

  def verify_wip_status_data(lot, params={})
    $log.info "verify_wip_status_data #{lot.inspect}, #{params}"
    rr = @cp.reports['wip_status'].get_records_lot(lot)
    ($log.warn "  wrong number of records: #{rr.size} instead of 1"; return) if rr.size != 1
    r = rr[0]
    li = params[:li] || @sv.lot_info(lot, backup: true, wafers: true)
    test_ok = true
    test_ok &= cmp_field(li.lot, r, :LOT_ID)
    test_ok &= cmp_field(li.customer, r, :CUSTOMER_NAME)
    test_ok &= cmp_field(li.sublottype, r, :LOT_TYPE)
    test_ok &= cmp_field(li.states['Lot State'], r, :LOT_STATE)
    test_ok &= cmp_field(li.states['Lot Process State'], r, :LOT_PROCESS_STATE)
    test_ok &= cmp_field(li.product, r, :PART_ID)
    test_ok &= cmp_field("FAB", r, :STAGEGRP_ID)
    test_ok &= cmp_user_param(lot, 'CustomerPriority', r, :LOT_PRIORITY)
    test_ok &= cmp_field(li.priority, r, :INTERNAL_PRIORITY_CLASS)
    test_ok &= cmp_user_param(lot, 'SourceFabCode', r, :SOURCE_FAB)
    test_ok &= cmp_user_param(lot, 'DueDate', r, :DUE_DATE)   #CP1.51
    test_ok &= field_exists(r, :OSD) unless @skip_lot_out_tests
    test_ok &= field_exists(r, :CSD) unless @skip_lot_out_tests
    # stage ID: use parent lot information if child and ID is NA
    stage = params['stageid'] || li.stage
    stage = @sv.lot_info(li.parent).stage if li.stage == 'NA' and li.parent != ''
    test_ok &= cmp_field(stage, r, :STAGE_ID)
    #
    test_ok &= cmp_field(li.department, r, :LOCATION)
    test_ok &= cmp_field(li.backup.current_loc ? 'This Site' : 'Other Site', r, :LOT_LOCATION)
    test_ok &= cmp_field((params['nwafers'] || li.nwafers).to_s, r, :CURRENT_LOT_QTY)
    _rt, _rv = li.route.split('.')
    test_ok &= cmp_field(_rt, r, :PRCD_NAME)
    test_ok &= cmp_field(_rv, r, :PRCD_VERSION)
    if li.backup.current_loc && !li.backup.backup_proc
      # lot start date from history, use parent for children
      ###lf = @sv.lot_info(lot).family
      opentry = @sv.lot_operation_history(li.family, first: true, category: 'STB').first ||
        @sv.lot_operation_history(li.family, category: 'STB').first # after schdl_change
      if opentry
        test_ok &= cmp_timestamp(r, :LOT_START_DATE, tref: @sv.siview_time(opentry.timestamp))
      else
        ($log.warn "no STB entry in operation history"; test_ok = false)
      end
    else
      test_ok &= cmp_timestamp_format(r, :LOT_START_DATE) # exact LOT_START_DATE value for backup op lots?
    end
    #
    test_ok &= cmp_SALES_ORDER_NUMBER(lot, r, :SALES_ORDER_NUMBER) if lot == li.family
    test_ok &= cmp_timestamp_format(r, :CLAIM_TIME)
    test_ok &= cmp_field('25', r, :START_LOT_QTY)
    test_ok &= field_not_empty(r, :CURR_STAGE_NUMBER)
    test_ok &= cmp_field(total_stage_count(li.route).to_s, r, :LAST_STAGE_NUMBER)
    test_ok &= cmp_field(total_mask_count(li.route).to_s, r, :LAST_MASK_COUNT)
    test_ok &= cmp_timestamp_format(r, :STEP_BEGIN_TIME)
    _bops = @sv.lot_operation_list(lot, forward: false, history: true)
    test_ok &= cmp_field(_bops.size.to_s, r, :CURR_STEP_NUMBER, "on route #{li.route}")
    _fops = @sv.lot_operation_list(lot, forward: true, current: false)
    test_ok &= cmp_field((_bops + _fops).size.to_s, r, :LAST_STEP_NUMBER)
    test_ok &= cmp_field(li.states['Lot Hold State'], r, :HOLD_STATE)
    test_ok &= field_not_empty(r, :PRODUCTION_AREA)
    test_ok &= cmp_field(@sv.AMD_modulepd(lot), r, :STAGE_PROCEDURE_ID)
    test_ok &= cmp_field(li.route, r, :FLOW_PROCEDURE_ID)
    test_ok &= cmp_field(li.op, r, :PD_ID)
    test_ok &= cmp_field(li.opName, r, :OPE_NAME)
    test_ok &= cmp_field(li.opNo, r, :OPE_NO)
    test_ok &= field_not_empty(r, :COMPLETION_CLASS)
    test_ok &= cmp_date_format(r, :PROJ_CSD) unless @skip_lot_out_tests
    test_ok &= cmp_date_format(r, :PROJ_FABOUT) if r[:PROJ_FABOUT]
    test_ok &= cmp_user_param(lot, 'Purpose', r, :LOT_PURPOSE)
    #
    # conditional parameters
    # CURR_MASK_COUNT, MATERIAL_NUMBER
    if li.states['Lot Finished State'].empty?
      test_ok &= cmp_field(curr_mask_count(lot)[li.opNo].to_s, r, :CURR_MASK_COUNT)
    end
    test_ok &= cmp_ORACLE_ITEM_ID(lot, r, :MATERIAL_NUMBER)
    # HOLD_STATE
    if li.status == 'ONHOLD'
      reasons = @sv.lot_hold_list(lot).collect {|h| h.reason}
      ($log.warn "  wrong HOLD_REASON"; test_ok = false) unless reasons.member?(r[:HOLD_REASON])
    end
    test_ok &= cmp_field(li.parent, r, :PARENT_LOTID) if li.parent != ''
    test_ok &= cmp_field(li.bank, r, :BANK_ID) if li.bank != ''
    stageno = @sm.object_info(:stage, li.stage).first[:specific][:stagenumber]
    test_ok &= cmp_field(stageno, r, :STAGE_NO, "wrong STAGE_NO for #{lot} #{li.stage}")
    # new fields for OMM
    test_ok &= cmp_field(li.family, r, :LOT_FAMILY_ID)
    test_ok &= cmp_field(li.states["Lot Inventory State"], r, :LOT_INV_STATE)
    test_ok &= cmp_field(li.states["Lot Finished State"], r, :LOT_FINISHED_STATE) if li.states['Lot Finished State'] != ''
    dcg = li.wafers.inject(0) {|s, w| s += @sv.user_parameter('Wafer', w.wafer, name: 'DieCountGood').value.to_i}
    test_ok &= cmp_field(dcg.to_s, r, :CURRENT_SUBLOT_QTY) if dcg > 0
    test_ok &= cmp_field(li.masklevel, r, :PHOTO_LAYER) if li.states['Lot Finished State'] == ''
    # INV_AGE
    s = r[:INV_AGE]
    ($log.warn "wrong INV_AGE"; test_ok = false) unless (s.index('d') && s.index('h'))
    test_ok &= cmp_field(params[:erfid], r, :ERF_ID) if params[:erfid]
    # often not filled in ITDC
    test_ok &= cmp_date_format(r, :EST_DATE_1SWET) if r[:EST_DATE_1SWET]
    test_ok &= cmp_date_format(r, :EST_DATE_4SWET) if r[:EST_DATE_4SWET]
    test_ok &= cmp_date_format(r, :FAB_CSD) if r[:FAB_CSD]
    test_ok &= cmp_date_format(r, :BUM_CSD) if r[:BUM_CSD]
    return test_ok
  end

  def verify_wip_wafer_data(lot, params={})
    $log.info "verify_wip_wafer_data #{lot.inspect}"
    li = @sv.lot_info(lot, wafers: true)
    ($log.info '  lot is SHIPPED, no wip_wafer data'; return true) if li.status == 'SHIPPED'
    totallotwafers = 0
    totalrptwafers = 0
    test_ok = true
    nrecords = @cp.reports['wip_wafer'].get_records_lot(lot).size
    # check number of entries in wip_wafer matches lot size
    ($log.warn "  wrong number of wip_wafer records for #{lot}: #{nrecords}"; test_ok = false) if li.nwafers != nrecords
    # check wip_wafer parameters for each wafer
    li.wafers.each {|w|
      $log.debug {"  checking wafer=#{w.wafer} lot=#{li.lot} slot=#{w.slot} alias=#{w.alias}"}
      rr = @cp.reports['wip_wafer'].get_records(:WAFER_ID, w.wafer)
      ($log.warn "  wrong number of records for wafer #{w.wafer}"; test_ok = false; next) if rr.size != 1
      r = rr[0]
      test_ok &= cmp_field(li.lot, r, :LOT_ID, "invalid lot for wafer #{w.wafer}")
      test_ok &= cmp_field(w.alias, r, :WAFER_NUMBER, "invalid alias for wafer #{w.wafer}")
    }
    return test_ok
  end

  def verify_reticle_runsheet_data(index, fab, product, customer, retdata)
    # retdata is array of :PARMNAME, :PARMVAL and :RTCL_ID (a.k.a. PhotoLayer, ReticleGroup, Reticle)
    $log.info "verify_reticle_runsheet_data #{index}, '#{fab}', '#{product}', '#{customer}', '#{retdata.join("', '")}'"
    rr = @cp.reports['reticle_runsheet'].get_records(:PINDEX, index.to_s)
    ($log.warn "  wrong number of records: #{rr.size}"; return) if rr.size != 1
    r = rr.first
    test_ok = true
    test_ok &= cmp_field(fab, r, :FAB)
    test_ok &= cmp_field(product, r, :PART_ID)
    test_ok &= cmp_field(retdata[0], r, :PARMNAME, 'wrong PARMNAME/PhotoLayer')
    test_ok &= cmp_field(retdata[1], r, :PARMVAL, 'wrong PARMVAL/ReticleGroup')
    test_ok &= cmp_field(retdata[2], r, :RTCL_ID, 'wrong RTCL_ID')
    test_ok &= cmp_field(customer, r, :CUSTOMER_NAME)
    return test_ok
  end

  def verify_part_data(product, customer)
    $log.info "verify_part_data #{product.inspect}, #{customer.inspect}"
    # product and pg info from SM
    pinfo = @sm.object_info(:product, product).first || ($log.warn "product #{product.inspect} not found"; return)
    pg = pinfo.specific[:productgroup]
    pginfo = @sm.object_info(:productgroup, pg).first || ($log.warn "pg #{pg} not found"; return)
    # find record(s) by customer
    rr = @cp.reports['part'].get_records(:PART_ID, product)
    r = rr.find {|e| e[:CUSTOMER_NAME] == customer} || ($log.warn "  no part record found"; return)
    # check fields of one record
    test_ok = true
    _rt, _rv = pinfo.specific[:route].split('.')
    test_ok &= cmp_field(_rt, r, :PRCD_NAME)
    test_ok &= cmp_field(_rv, r, :PRCD_VERSION)
    test_ok &= cmp_field(customer, r, :CUSTOMER_NAME)
    test_ok &= cmp_field(product, r, :PART_ID)
    ##test_ok &= cmp_field(pinfo.owner, r, :PART_OWNER)
    test_ok &= cmp_field(pinfo.specific[:productowner], r, :PART_OWNER)
    test_ok &= cmp_field(pg, r, :SHORT_PART_NAME)
    test_ok &= cmp_field(pginfo.specific[:technology], r, :TECH_GEOMETRY)
    test_ok &= cmp_field(pginfo.specific[:diecount].to_s, r, :DIE_QTY)
    test_ok &= cmp_field(pinfo.description.tr('<>', ' ').strip, r, :PART_TITLE)
    test_ok &= cmp_field(pinfo.specific[:productcategory], r, :PROD_CATEGORY_ID)
    test_ok &= cmp_field(pinfo.specific[:producttype], r, :PROD_TYPE)
    test_ok &= cmp_field(pinfo.specific[:state], r, :STATE)
    test_ok &= cmp_field(pinfo.specific[:yield].to_int.to_s, r, :PLAN_YIELD)
    return test_ok
  end

  def verify_part_udata_data(product)
    $log.info "verify_part_udata_data #{product.inspect}"
    rr = @cp.reports['part_udata'].get_records(:PART_ID, product)
    ud = @sm.object_info(:product, product).first.udata

    test_ok &= rr.size == ud.size || $log.warn('size of report data und udata does not match')
    rr.each {|r|
      test_ok &= ud.member?r[:NAME] || $log.warn('part udata not found')
      test_ok &= ud[r[:NAME]] == r[:VALUE] || $log.warn('part udata not correct')
    }
  end

  def verify_lot_start_data(lot, params={})
    $log.info "verify_lot_start_data #{lot}, #{params}"
    li = @sv.lot_info(lot)
    rr = @cp.reports['lot_start'].get_records_lot(lot)
    ($log.warn "  wrong number of records: #{rr.size} instead of 1"; return) if rr.size != 1
    r = rr[0]
    test_ok = true
    test_ok &= cmp_field(lot, r, :LOT_ID)
    test_ok &= cmp_LOT_LABEL(lot, r, :LOT_LABEL)
    test_ok &= cmp_timestamp_format(r, :CLAIM_TIME)
    test_ok &= cmp_field(li.customer, r, :CUSTOMER_ID)
    test_ok &= cmp_field(li.product, r, :PRODSPEC_ID)
    test_ok &= cmp_field(li.sublottype, r, :SUB_LOT_TYPE)
    test_ok &= cmp_user_param(lot, 'CustomerPriority', r, :PRIORITY_CLASS)
    test_ok &= cmp_field(li.stage, r, :STAGE_ID)
    test_ok &= field_exists(r, :STAGE_NO)
    test_ok &= cmp_field(li.nwafers.to_s, r, :CUR_WAFER_QTY)
    test_ok &= cmp_field(li.route, r, :MAINPD_ID)
    test_ok &= cmp_field(li.op, r, :PD_ID)
    test_ok &= cmp_field(li.opNo, r, :OPE_NO)
    test_ok &= cmp_timestamp_format(r, :WFRHS_TIME)
    test_ok &= cmp_timestamp_format(r, :EVENT_CREATE_TIME)
    test_ok &= cmp_field(li.carrier, r, :CAST_ID)
    test_ok &= field_exists(r, :OPE_CATEGORY)
    test_ok &= cmp_SALES_ORDER_NUMBER(lot, r, :ORDER_NO)
    ## untestable unless we want to wait 1 hour for RTD:
    $log.info "  not verified CSD: #{get_CSD(lot)}, OSD: #{get_OSD(lot)}"
    $log.info "  not verified CURR_SCH_DATE, ORIG_SCH_DATE"
    ##test_ok &= cmp_date_format(r, "CURR_SCH_DATE")
    ##test_ok &= cmp_date_format(r, "ORIG_SCH_DATE")
    return test_ok
  end

  def verify_lot_wfr_start_data(lot, params={})
    $log.info "verify_lot_wfr_start_data #{lot.inspect}, #{params}"
    li = @sv.lot_info(lot, wafers: true)
    rr = @cp.reports['lot_wfr_start'].get_records_lot(lot)
    ($log.warn "  wrong number of records: #{rr.size} instead of #{li.nwafers}"; return) if rr.size != li.nwafers
    test_ok = true
    rr.each {|r|
      w = li.wafers.find {|w| w.wafer == r[:WAFER_ID]} || ($log.warn "no entry for wafer #{r[:WAFER_ID]}"; return)
      test_ok &= cmp_field(w.alias, r, :ALIAS_WAFER_NAME)
      test_ok &= cmp_field(lot, r, :LOT_ID)
      test_ok &= cmp_field('0', r, :GOOD_UNIT_COUNT)
      test_ok &= cmp_field(w.slot.to_s, r, :CUR_CAST_SLOT_NO)
    }
    return test_ok
  end

  def verify_fabout_data(lot, params={})
    $log.info "verify_fabout_data #{lot.inspect}, #{params}"
    li = @sv.lot_info(lot)
    rr = @cp.reports['fab_out'].get_records_lot(lot)
    ($log.warn "  wrong number of records: #{rr.size} instead of 1"; return) unless rr.size == 1
    r = rr[0]
    test_ok = true
    test_ok &= cmp_field(lot, r, :LOT_ID)
    test_ok &= cmp_timestamp_format(r, :CLAIM_TIME)
    test_ok &= cmp_field(li.nwafers.to_s, r, :CUR_WAFER_QTY)
    test_ok &= cmp_ORACLE_ITEM_ID(lot, r, :ORACLE_ITEM_ID)
    test_ok &= cmp_field(get_expediteorder(lot), r, :EXPEDITELOTFLAG)
    test_ok &= cmp_user_param(lot, 'SourceFabCode', r, :SOURCE_FAB)
    return test_ok
  end

  def verify_wip_attribute_data(lot, params={})
    $log.info "verify_wip_attribute_data #{lot.inspect}, #{params}"
    rr = @cp.reports['wip_attribute'].get_records_lot(lot)
    ($log.warn "  wrong number of records: #{rr.size} instead of 1"; return) if rr.size != 1
    r = rr[0]
    #
    test_ok = true
    test_ok &= cmp_field(lot, r, :LOT_ID)
    test_ok &= cmp_LOT_LABEL(lot, r, :LOT_LABEL)
    test_ok &= cmp_user_param(lot, "TurnkeyType", r, :TURNKEY_TYPE)
    test_ok &= cmp_user_param(lot, 'TurnkeySubconIDBump', r, :TURNKEY_SUBCON_BUMP)
    test_ok &= cmp_user_param(lot, 'TurnkeySubconIDSort', r, :TURNKEY_SUBCON_SORT)
    test_ok &= cmp_user_param(lot, 'GateCD_Target_PLN', r, :GATE_CD_TARGET)
    test_ok &= cmp_user_param(lot, 'CustomerLotGrade', r, :LOT_GRADE)
    test_ok &= cmp_user_param(lot, 'CustomerQualityCode', r, :QUALITY_CODE)
    test_ok &= cmp_user_param(lot, 'ShipLabelNote', r, :SHIP_LABEL_NOTE)
    test_ok &= cmp_ORACLE_ITEM_ID(lot, r, :ORACLE_ITEM_ID)
    test_ok &= cmp_user_param(lot, 'CustomerNCSLPrintFlag', r, :CUSTOMER_NCSL_PRINTFLAG)
    test_ok &= cmp_user_param(lot, 'CustomerNCSLInformation', r, :CUSTOMER_NCSL_INFORMATION)
    test_ok &= cmp_user_param(lot, 'PPCD', r, :PPCD_NR)
    test_ok &= cmp_field(get_expediteorder(lot), r, :EXPEDITELOTFLAG)
    return test_ok
  end

  def verify_lot_hold_release_data(lot, category, holdreason, params={})
    $log.info "verify_lot_hold_release_data #{lot.inspect}, '#{category}', '#{holdreason}', #{params}"
    # get SiView data
    li = params[:lotinfo] || @sv.lot_info(lot, wafers: true)
    hfirst = params[:hfirst] || !(li.parent == '')
    hops = @sv.lot_operation_history(lot, category: category)#, first: hfirst)
    # get feed data, filtered for OPE_CATEGORY
    records = @cp.reports['lot_hold_release'].get_records_lot(lot, timestamp: @sv.siview_time(hops.last.timestamp))
    records = records.select {|r| r[:OPE_CATEGORY] == category}
    ($log.warn "  #{records.size} records found for lot #{lot}"; return) if records.size != 1
    r = records.first
    #
    test_ok = true
    test_ok &= cmp_field(lot, r, :LOT_ID)
    test_ok &= cmp_LOT_LABEL(lot, r, :LOT_LABEL)
    test_ok &= cmp_timestamp_format(r, :HOLD_TIME)
    test_ok &= cmp_timestamp_format(r, :CLAIM_TIME)
    test_ok &= cmp_field(category, r, :OPE_CATEGORY)
    test_ok &= cmp_field(li.customer,  r, :CUSTOMER_ID)
    test_ok &= cmp_user_param(lot, 'CustomerPriority', r, :PRIORITY_CLASS)
    test_ok &= cmp_field((holdreason == 'MGHL') ? 'MergeHold' : 'LotHold', r, :HOLD_TYPE)
    test_ok &= cmp_field(holdreason, r, :HOLD_REASON_CODE)
    test_ok &= cmp_field((li.stage), r, :STAGE_ID)
    test_ok &= field_exists(r, :STAGE_NO)
    test_ok &= field_exists(r, :HOLD_REASON_DESC)
    test_ok &= cmp_field(li.route, r, :MAINPD_ID)
    test_ok &= cmp_field(li.op, r, :PD_ID)
    test_ok &= cmp_field(li.opNo, r, :OPE_NO)
    test_ok &= cmp_field((params[:nwafers] || li.nwafers).to_s, r, :CUR_WAFER_QTY)
    test_ok &= cmp_SALES_ORDER_NUMBER(lot, r, :SALES_ORDER_NUMBER) if lot == li.family
    test_ok &= cmp_field(li.product, r, :PRODSPEC_ID)
    test_ok &= cmp_field(li.sublottype, r, :SUB_LOT_TYPE)
    test_ok &= cmp_date_format(r, :ORIG_SCH_DATE) unless @skip_lot_out_tests
    test_ok &= cmp_date_format(r, :CURR_SCH_DATE) unless @skip_lot_out_tests
    test_ok &= cmp_timestamp_format(r, :EVENT_CREATE_TIME)
    test_ok &= cmp_ORACLE_ITEM_ID(lot, r, :ORACLE_ITEM_ID)
    test_ok &= cmp_user_param(lot, 'SourceFabCode', r, :SOURCE_FAB)
    return test_ok
  end

  def verify_etest_event_data(lot, t, params={})
    $log.info "verify_etest_event_data #{lot.inspect}, '#{t.iso8601}'"
    li = @sv.lot_info(lot, wafers: true)
    rr = @cp.reports['etest_event'].get_records_lot(lot, tfield: :EVENT_TIME, timestamp: t, spread: 3)
    ($log.warn "  wrong number of records: #{rr.size} instead of #{li.nwafers}"; return) if rr.size != li.nwafers
    test_ok = true
    rr.each {|r|
      w = li.wafers.find {|w| w.wafer == r[:VENDOR_WAFER_ID]} || ($log.warn "no entry for wafer #{r[:VENDOR_WAFER_ID]}"; return)
      test_ok &= cmp_field(lot, r, :LOT_ID)
      test_ok &= cmp_field(li.customer,  r, :CUSTOMER_NAME)
      test_ok &= cmp_field(li.sublottype, r, :SUB_LOT_TYPE)
      test_ok &= cmp_field(li.nwafers.to_s, r, :CURRENT_LOT_QTY)
      test_ok &= cmp_field("#{lot.split('.')[0]}.#{w.alias}", r, :ALIAS_WAFER_ID)
      test_ok &= cmp_user_param(lot, 'SourceFabCode', r, :SOURCE_FAB)
    }
    return test_ok
  end

  def verify_lot_split_data(lot, lot2, t, params={})
    # lot is parent and lot2 child, vice versa when :checking_child is true
    $log.info "verify_lot_split_data #{lot.inspect}, #{lot2.inspect}, '#{t.iso8601}', #{params}"
    li = @sv.lot_info(lot)
    li2 = @sv.lot_info(lot2)
    $log.info "checking lot_split_wafer for lot #{lot.inspect} and lot2 #{lot2.inspect}"
    $log.info "  time #{t.iso8601}"
    rr = @cp.reports['lot_split_wafer'].get_records_lot(lot, timestamp: t)
    ($log.warn "  wrong number of records: #{rr.size} instead of #{li.nwafers}"; return) if rr.size != li.nwafers
    test_ok = true
    rr.each {|r|
      $log.debug "  checking wafer #{r[:WAFER_ID]}"
      test_ok &= cmp_field(lot, r, :LOT_ID)
      test_ok &= cmp_LOT_LABEL(lot, r, :LOT_LABEL)
      test_ok &= cmp_field(lot2, r, :RELATIVE_LOT_ID)
      test_ok &= cmp_timestamp_format(r, :CLAIM_TIME)
      test_ok &= cmp_field(li.nwafers.to_s, r, :CUR_WAFER_QTY)
      test_ok &= cmp_field('Split', r, :OPE_CATEGORY)
      test_ok &= cmp_field(li.sublottype, r, :SUB_LOT_TYPE)
      test_ok &= cmp_field(li.product, r, :PRODSPEC_ID)
      if li.parent == li2.lot # checking child
        test_ok &= cmp_field(li.nwafers.to_s, r, :CLAIM_PROD_QTY) # child's wafer count
        test_ok &= cmp_field(li2.customer, r, :CUSTOMER_ID) # parent's customer
        $log.info '  ORACLE_ITEM_ID and PRIORITY_CLASS are not reported for child lots'
      else
        test_ok &= cmp_field(li2.nwafers.to_s, r, :CLAIM_PROD_QTY) # child's wafer count
        test_ok &= cmp_field(li.customer, r, :CUSTOMER_ID)
        test_ok &= cmp_ORACLE_ITEM_ID(lot, r, :ORACLE_ITEM_ID)
        test_ok &= cmp_user_param(lot, 'CustomerPriority', r, :PRIORITY_CLASS)
      end
      test_ok &= cmp_field(li.stage, r, :STAGE_ID)
      test_ok &= field_exists(r, :STAGE_NO)
      test_ok &= cmp_SALES_ORDER_NUMBER(lot, r, :ORDER_NO) if lot == li.family
      test_ok &= cmp_field(li.route, r, :MAINPD_ID)
      test_ok &= cmp_field(li.op, r, :PD_ID)
      test_ok &= cmp_field(li.opNo, r, :OPE_NO)
      test_ok &= field_exists(r, :CLAIM_MEMO)
      test_ok &= field_exists(r, :ALIAS_WAFER_NAME)
      test_ok &= field_exists(r, :WAFER_ID)
      test_ok &= field_exists(r, :SLOT_NO)
      test_ok &= field_exists(r, :GOOD_UNIT_COUNT)
      test_ok &= cmp_date_format(r, :ORIG_SCH_DATE) unless @skip_lot_out_tests
      test_ok &= cmp_date_format(r, :CURR_SCH_DATE) unless @skip_lot_out_tests
      test_ok &= cmp_field(li.carrier, r, :CAST_ID)
      test_ok &= cmp_timestamp_format(r, :EVENT_CREATE_TIME)
      test_ok &= cmp_user_param(lot, 'SourceFabCode', r, :SOURCE_FAB)
    }
    return test_ok
  end

  def verify_lot_merge_data(lot, lci, t, params={})
    $log.info "verify_lot_merge_data #{lot.inspect}, #{lci.lot}, '#{t.iso8601}', #{params}"
    li = @sv.lot_info(lot, wafers: true)
    rr = @cp.reports['lot_merge_wafer'].get_records_lot(lot, timestamp: t)
    ($log.warn "  wrong number of records: #{rr.size} instead of #{lci.wafers.size}"; return) if rr.size != lci.wafers.size
    test_ok = true
    rr.collect {|r|
      w = li.wafers.find {|w| w.wafer == r[:WAFER_ID]} || ($log.warn "no entry for wafer #{r[:WAFER_ID]}"; return)
      # compare fields
      test_ok &= cmp_field(lot, r, :LOT_ID)
      test_ok &= cmp_field(lci.lot, r, :RELATIVE_LOT_ID)
      test_ok &= cmp_timestamp_format(r, :CLAIM_TIME)
      test_ok &= field_exists(r, :CUR_WAFER_QTY)
      test_ok &= field_exists(r, :CLAIM_PROD_QTY)
      test_ok &= cmp_field('Merge', r, :OPE_CATEGORY)
      test_ok &= cmp_field(li.customer, r, :CUSTOMER_ID)
      test_ok &= cmp_field(li.sublottype, r, :SUB_LOT_TYPE)
      test_ok &= cmp_field(li.product, r, :PRODSPEC_ID)
      test_ok &= cmp_user_param(lot, 'CustomerPriority', r, :PRIORITY_CLASS)
      test_ok &= cmp_field(li.stage, r, :STAGE_ID)
      test_ok &= field_exists(r, :STAGE_NO)
      test_ok &= cmp_SALES_ORDER_NUMBER(lot, r, :ORDER_NO)
      test_ok &= cmp_field(li.route, r, :MAINPD_ID)
      test_ok &= cmp_field(li.op, r, :PD_ID)
      test_ok &= cmp_field(li.opNo, r, :OPE_NO)
      test_ok &= field_exists(r, :MERGE_QTY)
      test_ok &= cmp_date_format(r, :CURR_SCH_DATE) unless @skip_lot_out_tests
      test_ok &= cmp_date_format(r, :ORIG_SCH_DATE) unless @skip_lot_out_tests
      test_ok &= cmp_field(li.carrier, r, :CAST_ID)
      test_ok &= cmp_timestamp_format(r, :EVENT_CREATE_TIME)
      test_ok &= field_exists(r, :ALIAS_WAFER_NAME)
      test_ok &= field_exists(r, :SLOT_NO)
      test_ok &= field_exists(r, :GOOD_UNIT_COUNT)
    }
    return test_ok
  end

  # compare loader and SiView data
  def verify_lot_ship_details_data(lot, t, params={})
    test_ok = true
    $log.info "verifylot_ship_details #{lot.inspect}, '#{t.iso8601}', #{params}"
    li = @sv.lot_info(lot, wafers: true)
    records = @cp.reports['lot_ship_details'].get_records_lot(lot, :time=>t)
    ($log.warn "  wrong number of records: #{records.size} instead of #{li.nwafers}"; return) if records.size != li.nwafers
    # compare fields
    records.each {|r|
      w = li.wafers.find {|w| w.wafer == r[:WAFER_ID]} || ($log.warn "no entry for wafer #{r[:WAFER_ID]}"; return)
      test_ok &= cmp_field(li.lot, r, :LOT_ID)
      test_ok &= cmp_LOT_LABEL(lot, r, :LOT_LABEL)
      test_ok &= cmp_field(li.customer, r, :CUSTOMER_ID)
      test_ok &= cmp_field(li.sublottype, r, :SUB_LOT_TYPE)
      test_ok &= cmp_field(li.product, r, :PRODSPEC_ID)
      test_ok &= cmp_user_param(lot, 'CustomerPriority', r, :PRIORITY_CLASS)
      test_ok &= cmp_field(li.nwafers.to_s, r, :CUR_WAFER_QTY)
      test_ok &= cmp_timestamp_format(r, :CLAIM_TIME)
      test_ok &= field_exists(r, :CLAIM_MEMO)
      test_ok &= cmp_SALES_ORDER_NUMBER(lot, r, :ORDER_NO)
      test_ok &= cmp_user_param(lot, 'CustomerLotGrade', r, :LOT_GRADE)
      test_ok &= cmp_user_param(lot, 'CustomerQualityCode', r, :QUALITY_CODE)
      test_ok &= cmp_field(li.carrier, r, :CAST_ID)
      test_ok &= cmp_timestamp_format(r, :WFRHS_TIME)
      test_ok &= cmp_field(w.alias, r, :ALIAS_WAFER_NAME)
      test_ok &= cmp_field(w.slot.to_s, r, :CUR_CAST_SLOT_NO)
  	  dgc = @sv.user_parameter('Wafer', w.wafer, name: 'DieCountGood').value
      test_ok &= cmp_field(dgc, r, :GOOD_UNIT_COUNT)
      test_ok &= cmp_ORACLE_ITEM_ID(lot, r, :ORACLE_ITEM_ID)
      test_ok &= cmp_timestamp_format(r, :EVENT_CREATE_TIME)
      test_ok &= cmp_user_param(lot, 'TurnkeySubconIDBump', r, :TURNKEY_SUBCON_BUMP)
      test_ok &= cmp_user_param(lot, 'TurnkeySubconIDSort', r, :TURNKEY_SUBCON_SORT)
      test_ok &= cmp_user_param(lot, 'SourceFabCode', r, :SOURCE_FAB)
      test_ok &= cmp_field(get_expediteorder(lot), r, :EXPEDITELOTFLAG)
    }
    return test_ok
  end

  # compare feed and SiView data
  def verify_tk_subcon_alert_data(lot, t, params={})
    test_ok = true
    etype = params[:etype]
    $log.info "verify_tk_subcon_alert #{lot.inspect}, '#{t.iso8601}', #{params}"
    li = @sv.lot_info(lot, wafers: true)
    records = @cp.reports['tk_subcon_alert'].get_records_lot(lot, tfield: :EVENT_TIME, timestamp: t)
    ($log.warn "  wrong number of records: #{records.size} instead of #{li.nwafers}"; return) if records.size != li.nwafers
    mdme = @mdm.mdm_entries(customer: li.customer, erpitem: @sv.user_parameter('Lot', lot, name: 'ERPItem').value).first
    ($log.warn "no CUST_REF_ITEM for lot #{lot} in the MDM"; return) unless mdme
    records.each {|r|
      w = li.wafers.find {|w| w.wafer == r[:WAFER_ID]} || ($log.warn "no entry for wafer #{r[:WAFER_ID]}"; return)
      # compare fields
      test_ok &= cmp_field(lot, r, :LOT_ID)
      test_ok &= cmp_timestamp_format(r, :EVENT_TIME)
      test_ok &= cmp_timestamp_format(r, :STORE_TIME)
      test_ok &= cmp_field(etype, r, :EVENT_TYPE)
      test_ok &= cmp_field(li.customer, r, :CUSTOMER_ID)
      test_ok &= cmp_field(li.sublottype, r, :LOT_TYPE)
      test_ok &= cmp_field(li.product, r, :PART_ID)
      test_ok &= cmp_field('F1', r, :SOURCE_FAB)
      test_ok &= cmp_SALES_ORDER_NUMBER(lot, r, :SALES_ORDER_NUMBER)
      test_ok &= cmp_user_param(lot, 'TurnkeySubconIDBump', r, :SUBCON_ID_BUMP)
      test_ok &= cmp_user_param(lot, 'TurnkeySubconIDSort', r, :SUBCON_ID_SORT)
      test_ok &= cmp_user_param(lot, 'TurnkeyType', r, :TURNKEY_TYPE)
      test_ok &= cmp_field(li.nwafers.to_s, r, :LOT_QTY)
      test_ok &= field_exists(r, :SUBCONPROCESS)   # BUMP or SORT, calculated
      test_ok &= cmp_field(w.alias, r, :WAFER_NUMBER)
      test_ok &= cmp_ORACLE_ITEM_ID(lot, r, :ORACLE_ITEM_ID)
      test_ok &= cmp_field(mdme.custrefitem, r, :CUSTPARTNO)
    }
    return test_ok
  end

  def verify_lot_attribute_change_data(lot, t, attrib, value, params={})
    test_ok = true
    $log.info "verify_lot_attribute_change_data #{lot.inspect}, '#{t.iso8601}', #{attrib.inspect}, #{value.inspect}"
    spread = params[:spread] || 180 # this is not to wait for MDS, but to account for slight timestamp uncertainties
    li = @sv.lot_info(lot)
    records = @cp.reports['lot_attribute_change'].get_records_lot(lot, spread: spread, tfield: :ATTRIBUTE_CHANGE_TIME, timestamp: t)
    ($log.warn "  wrong number of records: #{records.size} instead of 1"; return) if records.size != 1
    r = records.first
    test_ok &= cmp_field(lot, r, :LOT_ID)
    test_ok &= cmp_LOT_LABEL(lot, r, :LOT_LABEL)
    test_ok &= cmp_field(li.customer, r, :CUSTOMER_ID)
    test_ok &= cmp_user_param(lot, 'CustomerPriority', r, :PRIORITY_CLASS)
    test_ok &= cmp_field(li.stage, r, :STAGE_ID)
    test_ok &= field_exists(r, :STAGE_NO)
    test_ok &= cmp_user_param(lot, 'SourceFabCode', r, :SOURCE_FAB)
    test_ok &= cmp_field(get_expediteorder(lot), r, :EXPEDITELOTFLAG)
    test_ok &= cmp_field(li.nwafers.to_s, r, :CUR_WAFER_QTY)
    test_ok &= cmp_field(li.sublottype, r, :CUR_SUB_LOT_TYPE)
    test_ok &= cmp_field(li.product, r, :PRODSPEC_ID)
    test_ok &= cmp_SALES_ORDER_NUMBER(lot, r, :ORDER_NO)
    test_ok &= field_exists(r, :OPE_CATEGORY)
    test_ok &= cmp_field(li.route, r, :MAINPD_ID)
    test_ok &= cmp_field(li.opNo, r, :OPE_NO)
    test_ok &= cmp_field(li.op, r, :PD_ID)
    test_ok &= cmp_field(li.carrier, r, :CAST_ID)
    test_ok &= cmp_timestamp_format(r, :EVENT_CREATE_TIME)
    test_ok &= cmp_ORACLE_ITEM_ID(lot, r, :ORACLE_ITEM_ID)
    # attribute changes
    test_ok &= field_exists(r, :LOT_ATTRIB_NAME)
    test_ok &= cmp_field(value, r, :CUR_LOT_ATTRIB)
    test_ok &= cmp_field(value, r, attrib) if attrib && attrib != :SUB_LOT_TYPE
    return test_ok
  end

  def verify_runsheet_data(route, product, params={})
    $log.info "verify_runsheet_data #{route.inspect}, #{product.inspect}"
    # get SM reference data
    rinfo = @sm.object_info(:mainpd, route, details: true, log: true).first
    rops = rinfo.specific[:operations]
    pinfo = @sm.object_info(:product, product).first
    pg = pinfo.specific[:productgroup]
    pginfo = @sm.object_info(:productgroup, pg).first
    # get report data, filtered by PRODUCT_ID
    records = @cp.reports['runsheet'].records.select {|r| r[:PRODUCT_ID] == product}
    ($log.warn "  wrong number of records: #{records.size} instead of #{rops.size}"; return) unless records.size == rops.size
    #
    test_ok = true
    nmasks = 0
    masklevel = nil
    nstages = 0
    stage = nil
    rops.each_with_index {|rop, i|
      $log.info "  #{i}: #{rop.opNo.inspect}, #{rop.op.inspect}"
      r = records.find {|rr| rr[:OPE_NO] == rop.opNo}
      test_ok &= cmp_field((i+1).to_s, r, :INST_NUM)
      test_ok &= cmp_field(product, r, :PRODUCT_ID)
      test_ok &= cmp_field(route, r, :FLOW_PRCD_ID)
      test_ok &= cmp_field(route.sub(/.-/, '').split('.').first, r, :SHORT_FLOW_PRCDNAME)
      test_ok &= cmp_field(rinfo.description, r, :FLOW_TITLE)
      test_ok &= cmp_timestamp(r, :FLOW_ACTIVE_DATE, tref: rinfo.log[:created], utc_fix: true)
      unless rop.stage.empty?
        test_ok &= cmp_field(rop.stage, r, :FLOW_STAGE)
        if rop.stage != stage
          nstages += 1
          stage = rop.stage
        end
        test_ok &= cmp_field(nstages.to_s, r, :STAGE_ORDER)
      end
      unless rop.photolayer.empty?
        test_ok &= cmp_field(rop.photolayer, r, :MASK_LAYER)
        if rop.photolayer != masklevel
          nmasks += 1
          masklevel = rop.photolayer
        end
        test_ok &= cmp_field(nmasks.to_s, r, :MASK_COUNT)
      end
      technology = pginfo.specific[:technology]
      test_ok &= cmp_field(technology, r, :TECH_GEOMETRY)
      test_ok &= cmp_field(rop.op, r, :STEP_ID)
      test_ok &= cmp_field(rop.opNo, r, :OPE_NO)
      test_ok &= cmp_field(rop.udata['RouteOpSafeOp'], r, :ROUTE_OP_SAFE_OP)
      pdinfo = @sm.object_info(:pd, rop.op).first
      test_ok &= cmp_field(pdinfo.specific[:pdname], r, :OPE_NAME)
      test_ok &= cmp_field(pdinfo.specific[:pdtype], r, :RECIPE_TYPE)
      test_ok &= cmp_field(rop.mname, r, :STAGE_PRCD_ID)
      minfo = @sm.object_info(:modulepd, rop.mname, log: true).first
      ## currently wrong date or unknown source:
      test_ok &= cmp_timestamp(r, :STAGE_ACTIVE_DATE, tref: minfo.log[:created], utc_fix: true)
      lrcps = pdinfo.specific[:lrcps]
      lrcp = lrcps[:product][product] || lrcps[:productgroup][pg] || lrcps[:technology][technology] || lrcps[nil]
      test_ok &= cmp_field(lrcp, r, :RECP_ID)
      lrcpinfo = @sm.object_info(:lrcp, lrcp).first
      unless lrcpinfo.description.empty?
        test_ok &= cmp_field(lrcpinfo.description.gsub(/[^[:ascii:]]/, ' '), r, :RECP_TITLE)
      end
      test_ok &= cmp_field(pdinfo.specific[:pdtype], r, :RECIPE_TYPE)
    }
    return test_ok
  end

  def verify_scrap_data(lot, params={})
    $log.info "verify_scrap_data #{lot.inspect}, #{params}"
    category = params[:scrap_cancel] ? 'WaferScrapCancel' : 'WaferScrap'
    # opentry = @sv.lot_operation_history(lot, category: category).first || return
    hist = @sv.lot_operation_history(lot, raw: true)
    opentry = hist.find {|e| e.operationCategory == category} || return
    li = params[:lotinfo] || @sv.lot_info(lot, wafers: true) # wafers are not visible if not in a carrier
    scraptype = category == 'WaferScrap' ? 'WS' : 'WC'
    records = @cp.reports['scrap'].get_records_lot(lot)
    records.select! {|r| r[:SCRAP_TYPE] == 'WC'} if scraptype == 'WC' # filter out scrap records for scrap cancel
    nwafers = opentry.claimedProductWaferQuantity
    ($log.warn "  wrong number of records: #{records.size} instead of #{nwafers}"; return) if records.size != nwafers
    test_ok = true
    records.each {|r|
      w = li.wafers.find {|w| w.wafer == r[:WAFER_ID]} || ($log.warn "no entry for wafer #{r[:WAFER_ID]}"; return)
      # compare fields
      test_ok &= cmp_field(w.alias, r, :ALIAS_WAFER_NAME)
      test_ok &= cmp_field(w.slot.to_s, r, :CUR_CAST_SLOT_NO)
  	  dgc = @sv.user_parameter('Wafer', w.wafer, name: 'DieCountGood').value
      test_ok &= cmp_field(dgc, r, :GOOD_UNIT_COUNT)
      test_ok &= cmp_field(lot, r, :LOT_ID)
      test_ok &= cmp_LOT_LABEL(lot, r, :LOT_LABEL)
      test_ok &= cmp_timestamp_format(r, :CLAIM_TIME)
      test_ok &= cmp_field(li.customer, r, :CUSTOMER_ID)
      test_ok &= cmp_field(li.product, r, :PRODSPEC_ID)
      test_ok &= field_exists(r, :CLAIM_STAGE_ID)
      test_ok &= field_exists(r, :CLAIM_STAGE_NO)
      test_ok &= field_exists(r, :REASON_DEPARTMENT)
      test_ok &= cmp_field(opentry.claimMemo, r, :CLAIM_MEMO)
      test_ok &= cmp_field(@sv.user, r, :CLAIM_USER_ID)
      test_ok &= cmp_field(li.route, r, :CLAIM_MAINPD_ID)
      test_ok &= cmp_field(li.op, r, :CLAIM_PD_ID)
      test_ok &= cmp_field(li.op.split('.')[0], r, :CLAIM_OPE_NAME)
      test_ok &= cmp_field(li.opNo, r, :CLAIM_OPE_NO)
      test_ok &= cmp_field(scraptype, r, :SCRAP_TYPE)
      test_ok &= field_exists(r, :REASON_CODE)
      test_ok &= field_exists(r, :REASON_DESCRIPTION) # reason code not in history
      test_ok &= cmp_field(li.sublottype, r, :SUB_LOT_TYPE)
      test_ok &= cmp_SALES_ORDER_NUMBER(lot, r, :ORDER_NO)
      test_ok &= cmp_user_param(lot, 'CustomerPriority', r, :PRIORITY_CLASS)
      test_ok &= cmp_user_param(lot, 'CustomerQualityCode', r, :QUALITY_CODE)
      test_ok &= cmp_user_param(lot, 'CustomerLotGrade', r, :LOT_GRADE)
      ##CURR_SCH_DATE and ORIG_SCH_DATE always empty
      #test_ok &= cmp_ORACLE_ITEM_ID(lot, r, :ORACLE_ITEM_ID)
      test_ok &= cmp_user_param(lot, 'TurnkeySubconIDBump', r, :SUBCON_ID_BUMP)
      test_ok &= cmp_user_param(lot, 'TurnkeySubconIDSort', r, :SUBCON_ID_SORT)
      test_ok &= cmp_timestamp_format(r, :EVENT_CREATE_TIME)
      test_ok &= cmp_field(li.op, r, :REASON_PD_ID)
      test_ok &= cmp_field(get_expediteorder(lot), r, :EXPEDITELOTFLAG)
      test_ok &= cmp_user_param(lot, 'SourceFabCode', r, :SOURCE_FAB)
    }
    return test_ok
  end

  def verify_recipeidlist(lrcp, lrcptype)
    $log.info "verify_recipeidlist #{lrcp.inspect}, #{lrcptype.inspect}"
    # get recipetype
    lrcp_info = @sm.object_info(:lrcp, lrcp).first
    _t = lrcp_info.specific[:lrcptype]
    ($log.warn "  wrong lrcptype #{_t.inspect}") if _t != lrcptype
    # get eqptype via default recipe -> eqp
    rcp = lrcp_info.specific[:recipe].keys.first
    namespace, rcpname = rcp.split('.', 2)
    rcp_info = @sm.object_info(:recipe, rcpname).first
    eqp_info = @sm.object_info(:eqp, rcp_info.specific[:eqps].first).first  # take first eqp
    eqptype = eqp_info.specific[:eqptype]
    # find record(s) by lcrecipe and eqptype
    rr = @cp.reports['recipeidlist'].get_records(:LCRECIPE_ID, lrcp)
    ($log.warn "  no records for lrecipe #{lrcp.inspect}"; return) if rr.empty?
    r = rr.find {|e| e[:EQP_TYPE] == eqptype}
    ($log.warn "  no records for eqptype #{eqptype}"; return) unless rr
    # check fields of record
    test_ok = true
    test_ok &= cmp_field(lrcp, r, :LCRECIPE_ID)
    test_ok &= cmp_field(lrcptype, r, :RECIPE_TYPE)
    test_ok &= cmp_field(eqptype, r, :EQP_TYPE)
    return test_ok
  end

  def verify_source_product(product, params={})
    $log.info "verify_source_products #{product.inspect}, #{params}"
    # get product data from SM
    details = @sv.AMD_product_details(product) || ($log.warn "undefined product #{product}"; return)
    ($log.warn "no srcproduct"; return) if details.sourceProductID.empty?
    test_ok = true
    details.sourceProductID.each {|srcp|
      srcproduct = srcp.identifier
      # find record by product
      res = @cp.reports['source_products'].get_records(:PRODSPEC_ID, product)
      r = res.find {|l| l[:SRC_PRODSPEC_ID] == srcproduct}
      (test_ok = false; next) unless r
      srctechid = @sv.AMD_product_details(srcproduct).technologyID.identifier || ($log.warn "undefined technology for source product #{srcproduct}"; return)
      # check fields of record
      test_ok = true
      test_ok &= cmp_field(product, r, :PRODSPEC_ID)
      test_ok &= cmp_field(details.routeID.identifier, r, :MAINPD_ID)
      test_ok &= cmp_field(details.technologyID.identifier, r, :TECH_ID)
      test_ok &= cmp_field(details.productIDDescription.tr('<>', ' '), r, :DESCRIPTION)
      test_ok &= cmp_field(srcproduct, r, :SRC_PRODSPEC_ID)
      test_ok &= cmp_field(srctechid, r, :SRC_TECH_ID)
      $log.info "   tested with srcproduct #{srcproduct} and srctechid #{srctechid}\n"
    }
    return test_ok
  end

  def verify_lot_history(lot, category, params={})
    $log.info "verify_lot_history #{lot.inspect}, #{category.inspect}, #{params}"
    # get lot history record(s) for this category, TransferTitle and UnTransferTitle are virtual cats
    hist = @sv.lot_operation_history(lot, first: true, raw: true)
    if ['TransferTitle', 'UnTransferTitle'].include?(category)
      # lh = @sv.lot_operation_history(lot, first: true, category: 'OrderChange', tstart: params[:tstart], tend: params[:tend])
      if params[:tstart]
        hist.select! {|e|
          ts =siview_time(e.reportTimeStamp)
          (ts >= params[:tstart]) && (ts <= params[tend])
        }
      end
      lhr = hist.find {|e| e.operationCategory == 'OrderChange'}
    else
      # lh = @sv.lot_operation_history(lot, first: true, category: category)
      lhr = hist.find {|e| e.operationCategory == category}
    end
    ($log.warn "  no unique history entry (#{lh.size})"; return) unless lhr  #lh && lh.size == 1
    # lhr = lh.first
    # find report record
    if ['TransferTitle', 'UnTransferTitle'].include?(category)
      records = @cp.reports['lot_history'].get_records_lot(lot, timestamp: @sv.siview_time(lhr.timestamp), spread: 1)
      # 2 records returned: OrderChanged + virtual
      ($log.warn "  wrong number of records: #{records.size}"; return) if records.size != 2
      records = records.select {|r| r[:OPE_CATEGORY] == category}
    else
      records = @cp.reports['lot_history'].get_records_lot(lot, timestamp: @sv.siview_time(lhr.timestamp), spread: 0)
    end
    ($log.warn "  wrong number of records: #{records.size}"; return) if records.size != 1
    r = records.first
    return unless cmp_field(category, r, :OPE_CATEGORY)
    li = @sv.lot_info(lot, wafers: true)
    #
    test_ok = true
    test_ok &= cmp_timestamp_format(r, :EVENT_CREATE_TIME)
    test_ok &= cmp_timestamp_format(r, :WFRHS_TIME)
    test_ok &= cmp_field(lot, r, :LOT_ID)
    test_ok &= cmp_field(params[:sublottype] || li.sublottype, r, :SUB_LOT_TYPE)
    test_ok &= cmp_field(lhr.operationPass, r, :OPE_PASS_COUNT)
    test_ok &= cmp_timestamp_format(r, :CLAIM_TIME)
    test_ok &= cmp_timestamp_format(r, :STORE_TIME)
    test_ok &= cmp_field('0', r, :CLAIM_CNTL_QTY)
    test_ok &= field_exists(r, :CLAIM_USER_ID)
    test_ok &= cmp_user_param(lot, 'SourceFabCode', r, :SOURCE_FAB)
    movetype = case category
      when 'STB'; 'STB'
      when 'STBCancelled'; 'STBCancel'
      when 'OperationComplete'; 'ForwardOperation'
      when 'ForceComp'; 'ForwardStage'
      when 'GatePass'; 'ForwardOperation'
      else 'NonMove'
    end
    test_ok &= cmp_field(movetype, r, :MOVE_TYPE)
    #
    return test_ok if ['TransferTitle', 'UnTransferTitle'].include?(category)
    test_ok &= cmp_field(lhr.currentWaferQuantity.to_s, r, :CUR_WAFER_QTY)
    test_ok &= cmp_field(lhr.claimedProductWaferQuantity.to_s, r, :CLAIM_PROD_QTY)
    return test_ok if category == 'OrderChange'
    #
    test_ok &= cmp_field(lhr.productGroupID, r, :PRODGRP_ID)
    test_ok &= cmp_field(lhr.productID, r, :PRODSPEC_ID)
    test_ok &= cmp_field(lhr.customerCode, r, :CUSTOMER_ID)
    test_ok &= cmp_field(li.lottype, r, :LOT_TYPE)
    #
    if category != 'Ship'
      if ['OperationComplete', 'ForceComp'].member?(category)
        # find previous opNo
        ops = @sv.lot_operation_list(lot, forward: false, history: true)
        idx = nil
        ops.each_with_index {|o, i| idx = i if o.opNo == lhr.operationNumber}
        o = ops[idx - 1]
        lhr.routeID = o.route
        lhr.stageID = o.stage
        lhr.maskLevel = o.masklevel
        lhr.operationNumber = o.opNo
        lhr.operationID = o.op
      end
      test_ok &= cmp_field(lhr.routeID, r, :MAINPD_ID)
      test_ok &= cmp_field(lhr.stageID, r, :STAGE_ID)
      test_ok &= cmp_field(lhr.maskLevel, r, :PHOTO_LAYER) unless lhr.maskLevel.empty?
      if category != 'GatePass'   # exclude OPE_NO and PD_ID for GatePass, since CP 1.39, CR57053
        test_ok &= cmp_field(lhr.operationNumber, r, :OPE_NO)
        test_ok &= cmp_field(lhr.operationID, r, :PD_ID)
      end
    end
    field_exists(r, :RELATIVE_LOT_ID) if ['STB', 'Merge'].member?(category) # does not work reliably, not regarded a failure
    test_ok &= cmp_user_param(lot, 'ERPSalesOrder', r, :ORDER_NO)
    test_ok &= cmp_field(get_expediteorder(lot), r, :EXPEDITELOTFLAG)
    return unless test_ok
    #
    # verify wafer history
    ($log.info "  skipped wafer check due to known issues"; return test_ok) if ['Ship', 'OperationComplete', 'ForceComp'].member?(category)
    wrecords = @cp.reports['wafer_history'].get_records_lot(lot, spread: 0, tfield: :EVENT_CREATE_TIME, timestamp: r[:EVENT_CREATE_TIME])
    nwafers = (category == 'WaferScrap') ? lhr.claimedProductWaferQuantity : lhr.currentWaferQuantity
    if nwafers != wrecords.size
      $log.warn "  wrong number of records in wafer_history for #{lot} #{category}: #{wrecords.size} instead of #{nwafers}"
      return
    end
    if category == 'Split'
      $log.info "  number of wafer records is correct, field validation skipped"
    else
      li.wafers.each {|w|
        wr = wrecords.find {|e| e[:WAFER_SCRIBE] == w.wafer}
        test_ok &= cmp_field(r[:EVENT_CREATE_TIME], wr, :EVENT_CREATE_TIME)
        test_ok &= cmp_field(r[:WFRHS_TIME], wr, :CLAIM_TIME)
        test_ok &= cmp_field(w.alias, wr, :WAFER_ALIAS)
      }
    end
    #
    return test_ok
  end

  def verify_plan_start_data(lot, sublottype, erpitem, params={})
    $log.info "verify_plan_start_data #{lot.inspect}, #{params}"
    rq = @sv.product_request(lot)
    records = @cp.reports['plan_start'].get_records_lot(lot)
    ($log.warn "  no unique record found for lot #{lot}"; return) if records.size != 1
    r = records[0]
    test_ok = true
    test_ok &= cmp_field(lot, r, :LOT_ID)
    test_ok &= cmp_field(rq.customerCode, r, :CUSTOMER_ID)
    test_ok &= cmp_SALES_ORDER_NUMBER(lot, r, :ORDER_NO)
    test_ok &= cmp_field(sublottype, r, :SUB_LOT_TYPE)
    test_ok &= cmp_field(rq.routeID.identifier, r, :MAINPD_ID)
    test_ok &= cmp_field(rq.productCount.to_s, r, :QTY)
    test_ok &= cmp_timestamp_format(r, :PLAN_START_TIME)
    test_ok &= cmp_field('0', r, :PRIORITY_CLASS)  # fixed value for scheduled lots
    test_ok &= cmp_field(rq.productID.identifier, r, :PRODSPEC_ID)
    test_ok &= cmp_field(erpitem, r, :ORACLE_ITEM_ID)
    test_ok &= cmp_field('N', r, :EXPEDITELOTFLAG)  # always N because the lot does not exist yet
    return test_ok
  end

  def verify_equipment_current_info_data(eqp, fab)
    $log.info "verify_equipment_current_info_data #{eqp.inspect}, #{fab.inspect}"
    eqpi = @sv.eqp_info(eqp)
    eqp_info = @sm.object_info(:eqp, eqp).first
    eqptype = eqp_info.specific[:eqptype]
    oinfo = @sm.object_info(:eqptype, eqptype).first
    records = @cp.reports['equipment_current_info'].get_records(:EQPID, eqp)
    ($log.warn "  no unique record found for equipmemnt #{eqp}"; return) if records.size != 1
    r = records.first
    #
    test_ok = true
    test_ok &= cmp_field(fab, r, :FAB)
    test_ok &= cmp_field(eqp, r, :EQPID)
    test_ok &= cmp_field(eqpi.status.split('.').first, r, :E10_STATE)
    test_ok &= cmp_field(eqp_info.specific[:eqpowner], r, :MODULE)
    test_ok &= cmp_field(eqptype, r, :EQPTYPE)
    test_ok &= cmp_field(eqpi.category, r, :EQP_CATEGORY)
    test_ok &= cmp_field(oinfo.description, r, :STN_FAMILY)
    test_ok &= cmp_field(oinfo.udata['ReportFamily'], r, :REPORT_FAMILY)
    test_ok &= cmp_field(oinfo.udata['EquipmentClass'], r, :EQUIPMENT_CLASS)
    test_ok &= cmp_field(eqp_info.description, r, :EquipDesc) #unless eqpi.name.empty?
    return test_ok
  end

  def verify_lot_ship(lot, t, params={})
    $log.info "verify_lot_ship #{lot.inspect}, '#{t.iso8601}', #{params}"
    # compare loader and SiView data
    records = @cp.reports['lot_ship'].get_records_lot(lot, tfield: 'SHIP_DATE', time: t)
    ($log.warn "  no unique record found for lot #{lot}"; return) if records.size != 1
    r = records[0]
    # compare fields
    li = @sv.lot_info(lot)
    test_ok = true
    test_ok &= cmp_field(li.customer, r, 'CUSTOMER_NAME')
    test_ok &= cmp_field(li.product, r, 'PART_ID')
    test_ok &= cmp_field(li.nwafers.to_s, r, 'SHIP_WFR_QTY')
    test_ok &= cmp_timestamp_format(r, 'SHIP_DATE')
    test_ok &= cmp_ORACLE_ITEM_ID(lot, r, 'ORACLE_ITEM_ID')
    return test_ok
  end

  def verify_lot_complete_data(lot, params={})
    $log.info "verify_lot_complete_data #{lot.inspect}, #{params}"
    test_ok = true
    # compare feed and SiView data
    li = @sv.lot_info(lot, wafers: true)
    records = @cp.reports['lot_complete'].get_records_lot(lot)
    ($log.warn "  wrong number of records: #{records.size} instead of #{li.nwafers}"; return) if records.size != li.nwafers
    records.each {|r|
      w = li.wafers.find {|w| w.wafer == r[:WAFER_ID]} || ($log.warn "no entry for wafer #{r[:WAFER_ID]}"; return)
      # compare fields
      test_ok &= cmp_field(lot, r, :LOT_ID)
      test_ok &= cmp_LOT_LABEL(lot, r, :LOT_LABEL)
      test_ok &= cmp_field(li.customer, r, :CUSTOMER_ID)
      test_ok &= cmp_field(li.sublottype, r, :SUB_LOT_TYPE)
      test_ok &= cmp_field(li.product, r, :PRODSPEC_ID)
      test_ok &= cmp_user_param(lot, 'CustomerPriority', r, :PRIORITY_CLASS)
      test_ok &= cmp_field(li.stage, r, :STAGE_ID)
      test_ok &= field_exists(r, :STAGE_NO)
      test_ok &= cmp_field(li.nwafers.to_s, r, :CUR_WAFER_QTY)
      test_ok &= cmp_timestamp_format(r, :CLAIM_TIME)
      test_ok &= cmp_field(li.route, r, :MAINPD_ID)
      test_ok &= cmp_field(li.op, r, :PD_ID)
      test_ok &= cmp_field(li.opNo, r, :OPE_NO)
      test_ok &= cmp_field(li.carrier, r, :CAST_ID)
      test_ok &= cmp_timestamp_format(r, :WFRHS_TIME)
      test_ok &= cmp_field(w.wafer, r, :WAFER_ID)
      test_ok &= cmp_field(w.alias, r, :ALIAS_WAFER_NAME)
      test_ok &= cmp_field(w.slot.to_s, r, :CUR_CAST_SLOT_NO)
  	  dgc = @sv.user_parameter('Wafer', w.wafer, name: 'DieCountGood').value
      test_ok &= cmp_field(dgc, r, :GOOD_UNIT_COUNT)
      test_ok &= cmp_SALES_ORDER_NUMBER( lot, r, :ORDER_NO) ## untestable unless want to wait 1 hour for RTD:
      ##test_ok &= cmp_date_format(r, "CURR_SCH_DATE")
      ##test_ok &= cmp_date_format(r, "ORIG_SCH_DATE")
      test_ok &= cmp_timestamp_format(r, :EVENT_CREATE_TIME)
      test_ok &= field_exists(r, :OPE_CATEGORY)
      test_ok &= cmp_field('BankIn', r, :OPE_CATEGORY)
      test_ok &= cmp_user_param(lot, 'SourceFabCode', r, :SOURCE_FAB)
    }
    return test_ok
  end

  def verify_stage_move_data(lot, category, oldstage, params={})
    $log.info "verify_stage_move_data #{lot.inspect}, #{category.inspect}, #{params}"
    # get operation timestamp
    li = params[:lotinfo] || @sv.lot_info(lot, wafers: true)
    ts = @sv.lot_operation_history(lot, first: true).last.timestamp
    # get feed data, filtered for OPE_CATEGORY
    records = @cp.reports['lot_stage_move'].get_records_lot(lot, {time: @sv.siview_time(ts)}.merge(params))
    records = records.select {|r| r[:OPE_CATEGORY] == category}
    ($log.warn "  #{records.size} records found for lot #{lot}"; return) if records.size != 1
    r = records.first
    test_ok = true
    test_ok &= cmp_field(lot, r, :LOT_ID)
    test_ok &= cmp_LOT_LABEL(lot, r, :LOT_LABEL)
    test_ok &= cmp_field(li.nwafers.to_s, r, :CUR_WAFER_QTY)
    test_ok &= cmp_field(li.sublottype, r, :SUB_LOT_TYPE)
    test_ok &= cmp_timestamp_format(r, :STAGE_START_TIME)
    test_ok &= cmp_timestamp_format(r, :STAGE_END_TIME)
    test_ok &= cmp_field(li.product, r, :PRODSPEC_ID)
    test_ok &= cmp_field(li.customer, r, :CUSTOMER_ID)
    test_ok &= cmp_user_param(lot, 'CustomerPriority', r, :PRIORITY_CLASS)
    test_ok &= cmp_SALES_ORDER_NUMBER(lot, r, :ORDER_NO)  ## untestable unless we want to wait 1 hour for RTD:
    ##test_ok &= cmp_SALES_ORDER_NUMBER(lot, r, :ORDER_NO) if lot == li.family ## untestable unless we want to wait 1 hour for RTD:
    # $log.warn "SALES_ORDER_NUMBER: "
    test_ok &= cmp_field(li.route, r, :MAINPD_ID)
    test_ok &= cmp_field((params[:stageid] || li.stage), r, :CURR_STAGE_ID)
    test_ok &= cmp_field(oldstage, r, :STAGE_ID)
    test_ok &= field_exists(r, :STAGE_NO)
    test_ok &= field_exists(r, :MOVE_TYPE)
    test_ok &= field_exists(r, :OPE_CATEGORY)
    # $log.warn "CSD: #{get_CSD(lot)}, OSD: #{get_OSD(lot)}"
    ## untestable unless we want to wait 1 hour for RTD:
    $log.info "  not verified CSD: #{get_CSD(lot)}, OSD: #{get_OSD(lot)}"
    test_ok &= cmp_date_format(r, :CURR_SCH_DATE) unless @skip_lot_out_tests
    test_ok &= cmp_date_format(r, :ORIG_SCH_DATE) unless @skip_lot_out_tests
    test_ok &= cmp_field(li.opNo, r, :OPE_NO)
    test_ok &= cmp_field(li.op, r, :PD_ID)
    test_ok &= cmp_timestamp_format(r, :EVENT_CREATE_TIME)
    test_ok &= cmp_field(li.carrier, r, :CAST_ID)
    test_ok &= cmp_timestamp_format(r, :CLAIM_TIME)
    dcg = li.wafers.inject(0) {|s, w| s + @sv.user_parameter('Wafer', w.wafer, name: 'DieCountGood').value.to_i}
    test_ok &= cmp_field(dcg.to_s, r, :GOOD_DIE_COUNT)
    test_ok &= cmp_user_param(lot, 'SourceFabCode', r, :SOURCE_FAB)
    return test_ok
  end

  def verify_rework(lot, fabmodule, hist, rework_time, params={})
    $log.info "verify_rework #{lot.inspect}, #{fabmodule.inspect}, '#{rework_time.iso8601}', #{params}"
    test_ok = true
    # compare feed and SiView data
    li = params[:lotinfo] || @sv.lot_info(lot, wafers: true)
    qty = params[:qty] || li.nwafers.to_s
    records = @cp.reports['rework'].get_records_lot(lot, tfield: :CLAIM_MEMO, timestamp: rework_time)
    ($log.warn "  no records found for lot #{lot}"; return) if records.empty?
    ($log.warn "  wrong number of records found for lot #{lot}"; return) if hist.size != records.size
    records.each {|r|
      lpd = r[:PD_ID]
      if lpd
        found = false
        hist.each {|h|
          if h.operationID.identifier == lpd
            found = true
            test_ok &= cmp_field(lot, r, :LOT_ID)
            test_ok &= cmp_timestamp_format(r, :REWORK_OPERATION_COMPLETION_TIME)
            test_ok &= cmp_timestamp_format(r, :OPE_COMP_CLAIM_TIME)
            test_ok &= cmp_field(rework_time.iso8601, r, :CLAIM_MEMO)
            test_ok &= cmp_field(li.customer, r, :CUSTOMER_ID)
            test_ok &= cmp_field(li.product, r, :PRODSPEC_ID)
            test_ok &= cmp_field(h.operationID.identifier, r, :PD_ID)
            test_ok &= cmp_field(h.routeID.identifier, r, :RWK_ROUTE_ID)
            test_ok &= cmp_field(qty, r, :CUR_WAFER_QTY) if qty != 'nocheck'
            test_ok &= cmp_field(fabmodule, r, :MODULE)
            test_ok &= cmp_field(li.sublottype, r, :LOT_TYPE)
            test_ok &= cmp_field('RWK', r, :REASON_CODE)
            test_ok &= cmp_field(get_expediteorder(lot), r, :EXPEDITELOTFLAG)
            test_ok &= cmp_user_param(lot, 'SourceFabCode', r, :SOURCE_FAB)
         end
        }
        ($log.warn "no record found for #{lpd}"; test_ok = false) unless found
      else
        $log.warn "found an invalid record without PD_ID: #{r}"
      end
    }
    return test_ok
  end

end
