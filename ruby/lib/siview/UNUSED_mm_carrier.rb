=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.
=end


class SiView::MM

  XferJob = Struct.new(:jobid, :status, :type, :carrierjobs)
  CarrierXferJob = Struct.new(:carrier, :cjobid, :cjobstatus, :zone, :frommachine, :fromport, :tomachine, :toport, :estart, :eend, :xstart, :xend)
  CarrierBasicInfo = Struct.new(:carrier, :category, :description, :usagecheck, :pm_max, :capacity, :contents, :udata)
  CarrierCategoryInfo = Struct.new(:carrier_op_warn, :carrier_op_due, :carrier_op_inhibit,
    :filter_op_warn, :filter_op_due, :filter_op_inhibit, :filter_time_warn, :filter_time_due, :filter_time_inhibit,
    :gasket_op_warn, :gasket_op_due, :gasket_op_inhibit,:gasket_time_warn, :gasket_time_due, :gasket_time_inhibit)


  def carrier_basicinfo(carriers, params={})
    carriers = carriers.split if carriers.instance_of?(String)
    cstr = @jcscode.pptCassetteBaseInfoListInqInParm_struct.new(oid(carriers), any)
    #
    res = @manager.TxCassetteBaseInfoListInq(@_user, cstr)
    return nil if rc(res) != 0
    return res if params[:raw]
    res.strDurableAttributeSeq.collect {|a|
      udata = Hash[a.strUserDataSeq.collect {|u| [u.name, u.value]}]
      CarrierBasicInfo.new(a.durableID.identifier, a.category, a.description, a.usageCheckFlag, a.intervalBetweenPM, a.capacity, a.contents, udata)
    }
  end

  # send a xfer job complete report, return 0 on success
  def carrier_xfer_complete(carrier, state, eqp, port, params={})
    memo = params[:memo] || ''
    xfers = [@jcscode.pptXferJobComp_struct.new(oid(carrier), oid(eqp), oid(port), state, any)]
    #
    res = @manager.TxLotCassetteXferJobCompRpt(@_user, xfers, memo)
    return rc(res)
  end

  # move carriers and wait for completion, return true on complete success
  def carrier_move(carriers, frommachine, fromport, tomachine, toport, params={})
    carriers = [carriers] if carriers.instance_of?(String)
    # reserve carriers unless suppressed by :noreservation
    unless params[:noreservation]
      res = carrier_reserve(carriers)
      if res != 0
        carrier_reserve_cancel(carriers) if res != 113  # already reserved
        return false
      end
    end
    # create xfer jobs
    res = carrier_xfer(carriers, frommachine, fromport, tomachine, toport, params)
    (carrier_reserve_cancel(carriers); return) if res != 0
    # wait for jobs to complete
    return wait_carrier_xfer(carriers, timeout: params[:timeout] || 900)
  end

  # carrier maintenance counter limits per carrier category, currently not used
  def carrier_category_info(category, params={})
    res = @manager.CS_TxCarrierCategoryInfoInq(@_user, oid(category))
    return nil if rc(res) != 0
    cci = res.strCS_CarrierCatergoryInfo
    CarrierCategoryInfo.new(cci.CarrierOpeCountWarning, cci.CarrierOpeCountDue, cci.CarrierOpeCountInhibit,
      cci.FilterOpeCountWarning, cci.FilterOpeCountDue, cci.FilterOpeCountInhibit, cci.FilterTimeWarning, cci.FilterTimeDue,
      cci.FilterTimeInhibit, cci.GasketOpeCountWarning, cci.GasketOpeCountDue, cci.GasketOpeCountInhibit, cci.GasketTimeWarning,
      cci.GasketTimeDue, cci.GasketTimeInhibit)
  end

  # TODO: remove!
  def carrier_xfer_jobs(carrier, params={})
    inquiryType = params[:inquiryType] || 'C'
    detailFlag = (params[:detailFlag] != false)
    if params[:mcs]
      res = @manager.TxLotCassetteXferJobDetailInq(@_user, detailFlag, inquiryType, oid(carrier), 'INQUIRY')
    else
      res = @manager.TxLotCassetteXferJobInq(@_user, detailFlag, inquiryType, oid(carrier))
    end
    return nil if rc(res) != 0
    return res.strJobResult.collect {|j|
      cjobs = j.strCarrierJobResult.collect {|cji|
        CarrierXferJob.new(cji.carrierID.identifier, cji.carrierJobID, cji.carrierJobStatus,
        cji.zoneType, cji.fromMachineID.identifier, cji.fromPortID.identifier,
        cji.toMachine.identifier, cji.toPortID.identifier, cji.estimatedStartTime,
        cji.estimatedEndTime, cji.expectedStartTime, cji.expectedEndTime)
      }
      XferJob.new(j.jobID, j.jobStatus, j.transportType, cjobs)
    }
  end

  # delete all xfer jobs for a carrier, return 0 on success, TODO: remove!
  def carrier_xfer_jobs_delete(carrier, params={})
    $log.info "carrier_xfer_jobs_delete #{carrier.inspect}"
    memo = params[:memo] || ''
    jobs = carrier_xfer_jobs(carrier) || return
    return 0 if jobs.empty?
    # there is only one
    $log.info "  deleting #{jobs.length} jobs"
    jobid = jobs.first.jobid
    delcjobs = jobs.first.carrierjobs.collect {|cjid| @jcscode.pptDelCarrierJob_struct.new(cjid.cjobid, oid(carrier), any)}
    #
    res = @manager.TxLotCassetteXferJobDeleteReq(@_user, jobid, delcjobs, memo)
    return rc(res)
  end

end
