=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.
=end


class SiView::MM

  # currently UNUSED
  def lot_hold_release_with_action(lot, holdreason, action, params={}) #actions: split, hold, futurehold, gatepass
    release_reason = params[:release_reason] || params[:reason]
    release_reason ||= @f7 ? 'GCIM' : 'ALL'
    memo = params[:memo] || ''
    # get all holds, selected by hold reason code if given
    user = params[:user]
    _holdreason = holdreason.instance_of?(String) ? [holdreason] : holdreason
    _keepreasons = params[:keep] || []
    _keepreasons = _keepreasons.split if _keepreasons.instance_of?(String)
    _releasing = [] # for logging only
    hlist = lot_hold_list(lot, raw: true) || return
    fholdActions = [].to_java(@jcscode.pptCS_FutureHoldAction_struct)
    holdAction = [].to_java(@jcscode.pptCS_HoldAction_struct)
    waferIDs = [].to_java(@jcscode.objectIdentifier_struct)
    skipFlg = !!params[:skip]
    splitFlg = !!params[:split]
    forceSkip = !!params[:forceskip]
    mergeRouteId = params[:mergerouteid] || params[:mergert] || params[:mergeRtId] || '' #used when split action
    mergeOpeNo = params[:mergeopeno] || params[:mergeOpeNo] || '' #used when split action
    case action
    when 'futurehold'
      holdreason = params[:holdreason] || 'CEB'
      routeID = params[:route]
      opeNo = params[:openo]
      postFlag = !!params[:postflag]
      singleTriggerFlag  = !!params[:singletrigger]
      action_claimmemo = params[:actionmemo] || 'FutureHold Action memo'
      fholdAction = [@jcscode.pptCS_FutureHoldAction_struct.new(oid(holdreason), oid(routeID),
        opeNo, postFlag, singleTriggerFlag, action_claimmemo, any)].to_java(@jcscode.pptCS_FutureHoldAction_struct)
    when 'hold'
      holdreason = params[:holdreason] || 'CEB'
      action_claimmemo = params[:actionmemo] || 'Hold Action memo'
      holdAction = [@jcscode.pptCS_HoldAction_struct.new(oid(holdreason), action_claimmemo, any)].to_java(@jcscode.pptCS_HoldAction_struct)
    when 'split'
      wafers=params[:wafers] || 2
      waferIDs = oid(lot_info(lot, wafers: true, select: wafers).wafers)
    end
    holds = hlist.collect {|h|
      next if user && h.userID.identifier != user
      hreason = h.reasonCodeID.identifier
      next if _holdreason && !_holdreason.member?(hreason)
      next if _keepreasons.member?(hreason)
      _releasing << hreason
      @jcscode.pptHoldReq_struct.new(h.holdType, h.reasonCodeID, h.userID, h.responsibleOperationMark,
        h.responsibleRouteID, h.responsibleOperationNumber, h.relatedLotID, memo, any)
    }.compact.to_java(@jcscode.pptHoldReq_struct)
    ($log.info "lot #{lot} has no#{' '+_holdreason.inspect if holdreason} holds to release"; return 0) if holds.empty?
    $log.info "lot_hold_release_with_action #{lot.inspect}, #{_releasing.inspect}, release_reason: #{release_reason.inspect}"
    #
    holdReleaseInfo = [@jcscode.pptCS_LotHoldRelease_struct.new(oid(release_reason), holds, any)].to_java(@jcscode.pptCS_LotHoldRelease_struct)
    waferActions=[@jcscode.pptCS_WaferReleaseAction_struct.new(splitFlg, skipFlg, forceSkip, waferIDs, holdAction,
      fholdAction, 'splitClaimMemo', 'skipClaimMemo', oid(mergeRouteId), mergeOpeNo, any)].to_java(@jcscode.pptCS_WaferReleaseAction_struct)
    #
    res = @manager.CS_TxHoldReleaseWithActionReq(@_user, oid(lot), holdReleaseInfo, waferActions)
    return rc(res)
  end

end
