=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.
=end


class SiView::MM

  def _user_parameter(parameters, name, params)
    ret = parameters.collect {|p|
      next unless params[:all] || !name && p.valueFlag || name && (p.parameterName == name)
      UParam.new(p.parameterName, p.value, p.dataType, p.keyValue, p.valueFlag, p.description)
    }.compact
    ret0 = ret[0]
    return (name && !params[:istable] && !(ret0 && ret0.datatype.start_with?('TABLE'))) ? ret0 : ret
  end

  # get parameters for multiple objects at once
  # for Test083_MultiUserParameterValueChange only
  #
  # return hash "classname::objectid"=>[Uparams]
  def user_parameter_multi(classnames, pids, params={})
    pids = [pids] unless pids.instance_of?(Array)
    classnames = [classnames] * pids.count unless classnames.instance_of?(Array)
    #
    res = @manager.CS_TxUserParameterValueForMultipleObjectInq(@_user, classnames, oid(pids))
    return nil if rc(res) != 0
    ##return res if params[:raw]
    ret = {}
    res.strCS_ObjectUserParameterValueSeq.each {|res|
      ret["#{res.className}::#{res.objectID.identifier}"] = _user_parameter(res.parameters, params[:name], params)
    }
    return ret
  end

  # set paramters for multiple objects by hash: 'classname::pid'=>{:names=>..., :values=>..., :datatype=>...}
  # for Test083_MultiUserParameterValueChange and Test084_UserParameterValueChange only
  #
  # return 0 on success
  def user_parameter_change_multi(uparam_hash, changetype=1, params={})
    $log.info "user_parameter_change_multi #{uparam_hash.keys} ..."
    cs_uparams = uparam_hash.each_pair.collect {|k, v|
      classname, pid = k.split('::', 2)
      _uparams = _build_uparams(classname, pid, v[:names], v[:values], changetype, v[:datatype], params)
      @jcscode.pptCS_ObjectUserParameterValue_struct.new(classname, oid(pid), _uparams, any) ##if _uparams
    }
    #
    res = @manager.CS_TxUserParameterValueForMultipleObjectReq(@_user, cs_uparams, params[:memo] || '')
    return rc(res)
  end

end
