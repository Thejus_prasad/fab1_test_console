=begin
(c) Copyright 2013 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.
=end


class SiView::MM

  # returns available fixture groups, UNUSED
  def fixture_groups(params={})
    res = @manager.TxFixtureGroupIDListInq(@_user)
    return nil if rc(res) != 0
    return res if params[:raw]
    return res.objectIdentifiers.collect {|f| f.identifier}
  end

  # returns available fixtures, UNUSED
  def fixture_ids(params={})
    res = @manager.TxFixtureIDListInq(@_user)
    return nil if rc(res) != 0
    return res if params[:raw]
    return res.objectIdentifiers.collect {|f| f.identifier}
  end

  # change fixture status, UNUSED
  def fixture_status_change(fixture, status, params={})
    memo = params[:memo] || ''
    $log.info "fixture_status_change #{fixture.inspect} #{status.inspect}"
    if params[:check]
      fs = fixture_status(fixture)
      ($log.debug {"  fixture is already #{status}, no change"}; return 0) if fs.fixtureStatusInfo.fixtureStatus == status
    end
    #
    res = @manager.TxFixtureStatusChangeRpt(@_user, oid(fixture), status, memo)
    return rc(res)
  end

  # fixture information, UNUSED
  def fixture_status(fixture)
    res = @manager.TxFixtureStatusInq(@_user, oid(fixture))
    return nil if rc(res) != 0
    return res
  end

  # transfer status change of one or more fixtures, UNUSED
  def fixture_xfer_status_change(fixtures, xfer_status, params={})
    fix = [fixtures] if fixtures.instance_of?(String)
    $log.info "fixture_xfer_status_change #{fixtures}, #{xfer_status.inspect}, #{params}"
    memo = params[:memo] || ''
    eqp = params[:eqp] || ''
    stocker = params[:stocker] || ''
    fstruct = fix.map {|f| @jcscode.pptXferFixture_struct.new(oid(f), xfer_status, '', '', '', siview_timestamp(Time.now), any)}
    #
    res = @manager.TxFixtureXferStatusChangeRpt(@_user, oid(stocker), oid(eqp), fstruct, memo)
    return rc(res)
  end

  # multiple status change for fixtures, UNUSED
  def fixtures_multistatus_change(fixtures, status, params={})
    fixtures = [fixtures] if fixtures.instance_of?(String)
    $log.info "fixtures_multistatus_change #{fixtures}, #{status.inspect}, #{params.inspect}"
    memo = params[:memo] || ''
    if params[:check] && fixtures.size == 1
      fs = fixture_status(fixtures.first)
      ($log.debug {"  already #{status}, no change"}; return 0) if fs.fixtureStatusInfo.fixtureStatus == status
    end
    #
    res = @manager.TxFixtureMultiStatusChangeRpt(@_user, state, oid(fixtures), memo)
    return rc(res)
  end

  # return list of fixtures in stocker, UNUSED
  def fixture_stocker_info(stocker, params={})
    res = @manager.TxFixtureStockerInfoInq(@_user, oid(stocker))
    return nil if rc(res) != 0
    return res if params[:raw]
    return res.strInventoryFixtureInfo.map {|c| c.fixtureID.identifier}
  end

  # usage count reset, UNUSeD
  def fixture_usage_reset(fixture, params={})
    memo = params[:memo] || ''
    #
    res = @manager.TxFixtureUsageCountResetReq(@_user, oid(fixture), memo)
    return rc(res)
  end

end
