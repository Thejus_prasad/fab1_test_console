=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.
=end


class SiView::MM
  LotHold = Struct.new(:timestamp, :type, :reason, :rsp_mark, :rsp_route, :rsp_op, :related_lot, :user, :post, :single, :memo)

  # return 0 on success
  def lot_hold(lot, reason, params={})
    lotID = lot.instance_of?(String) ? oid(lot) : lot
    opNo = params[:opNo]
    route = params[:route]
    if opNo.nil? || route.nil?
      liraw = lots_info_raw(lotID)
      lopinfo = liraw.strLotInfo.first.strLotOperationInfo
      opNo ||= lopinfo.operationNumber
      routeID = route ? oid(route) : lopinfo.routeID
    else
      routeID = oid(route)
    end
    related_lot = params[:related_lot] || ''
    rsp_mark = params[:rsp_mark] || 'C'  # or 'P'
    memo = params[:memo] || ''
    holdtype = params[:holdtype] || 'LotHold'
    if reason.nil?
      reasonID = code_list(holdtype).first.code
    elsif reason.instance_of?(@jcscode.objectIdentifier_struct)
      reasonID = reason
    # elsif params[:wrong_reason]
    #   reasonID = oid(reason)
    else
      reasonID = nil
      cinfo = code_list(holdtype).find {|e| e.code.identifier == reason}
      reasonID = cinfo.code unless cinfo.nil?
    end
    ($log.warn "lot_hold: no hold reason #{reason.inspect}, type #{holdtype.inspect}"; return) if reasonID.nil?
    $log.info "lot_hold #{lotID.identifier.inspect}, #{reasonID.identifier.inspect}, holdtype: #{holdtype.inspect}"
    user = params[:user]
    userID = user ? oid(user) : @_user.userID
    rq = [@jcscode.pptHoldReq_struct.new(holdtype, reasonID, userID, rsp_mark, routeID, opNo, oid(related_lot), memo, any)]
    #
    res = @manager.TxHoldLotReq(@_user, lotID, rq)
    return rc(res)
  end

  # return an array of LotHold structs (strLotHoldListAttributes if :raw) on success, else nil
  def lot_hold_list(lot, params={})
    lotID = lot.instance_of?(String) ? oid(lot) : lot
    res = @manager.TxLotHoldListInq(@_user, lotID)
    return nil if rc(res) != 0
    return res.strLotHoldListAttributes if params[:raw]
    #
    reason = params[:reason]
    # reason = [reason] if reason.instance_of?(String)
    htype = params[:type]
    user = params[:user]  # used in pilotmgrtest only, TODO: remove
    # related_lot = params[:related_lot]
    memo = params[:memo]  # used in siltest only, TODO: remove
    ret = []
    res.strLotHoldListAttributes.each {|h|
      # next if reason && !reason.include?(h.reasonCodeID.identifier)
      next if reason && h.reasonCodeID.identifier != reason
      next if htype && h.holdType != htype
      next if user && h.userID.identifier != user
      # next if related_lot && h.relatedLotID.identifier != related_lot
      next if memo && !h.claimMemo.include?(memo)
      ret << LotHold.new(h.holdTimeStamp, h.holdType, h.reasonCodeID.identifier,
        h.responsibleOperationMark, h.responsibleRouteID.identifier, h.responsibleOperationNumber,
        h.relatedLotID.identifier, h.userID.identifier, nil, nil, h.claimMemo)
    }
    return ret
  end

  # release holds with reason e.g. 'QTHL', nil for all, return 0 on succes
  def lot_hold_release(lot, holdreason, params={})
    lotID = lot.instance_of?(String) ? oid(lot) : lot
    release_reason = params[:release_reason] || params[:reason]
    release_reason ||= @f7 ? 'GCIM' : 'ALL'
    htype = params[:type]
    memo = params[:memo] || ''
    # get all holds, selected by hold reason code if given
    user = params[:user]
    holdreason = [holdreason] if holdreason.instance_of?(String)
    keep = params[:keep]
    keep = [keep] if keep.instance_of?(String)
    releasing = [] # for logging only
    holds = []
    hlist = lot_hold_list(lotID, raw: true) || return
    hlist.each {|h|
      hreason = h.reasonCodeID.identifier
      next if htype && h.holdType != htype
      next if user && h.userID.identifier != user
      next if holdreason && !holdreason.member?(hreason)
      next if keep && keep.member?(hreason)
      if holdreason.nil? && ['NPBH', 'BOHL'].member?(hreason)
        $log.info "lot_hold_release: skipping reason #{hreason}" unless params[:silent]
        next
      end
      releasing << hreason
      holds << @jcscode.pptHoldReq_struct.new(h.holdType, h.reasonCodeID, h.userID, h.responsibleOperationMark,
        h.responsibleRouteID, h.responsibleOperationNumber, h.relatedLotID, memo, any)
    }
    if holds.empty?
      $log.info "lot #{lotID.identifier} has no#{' ' + holdreason.inspect if holdreason} holds to release" unless params[:silent]
      return 0
    end
    $log.info "lot_hold_release #{lotID.identifier.inspect}, #{releasing}, release_reason: #{release_reason.inspect}"
    cinfo = code_list('LotHoldRelease').find {|e| e.code.identifier == release_reason}
    ($log.warn 'wrong release reason code'; return) unless cinfo
    #
    res = @manager.TxHoldLotReleaseReq(@_user, lotID, cinfo.code, holds)
    return rc(res)
  end

  # return an array of LotHolds or strFutureHoldListAttributes
  def lot_futurehold_list(lot, params={})
    lotID = lot.instance_of?(String) ? oid(lot) : lot
    htype = params[:type] || ''
    reason = params[:reason] || ''
    user = params[:user] || ''
    route = params[:route] || ''
    opno = params[:opno] || ''
    related_lot = params[:related_lot] || ''
    search_key = @jcscode.pptFutureHoldSearchKey_struct.new(lotID, htype,
      oid(reason), oid(user), oid(route), opno, oid(related_lot), '', '', any)
    #
    res = @manager.TxFutureHoldListByKeyInq(@_user, search_key, 9999)
    return nil if rc(res) != 0
    return res.strFutureHoldListAttributes if params[:raw]
    reason = params[:reason]
    reason = [reason] if reason.instance_of?(String)
    memo = params[:memo]
    ret = []
    res.strFutureHoldListAttributes.each {|h|
      next if memo && !h.claimMemo.include?(memo)
      ret << LotHold.new(h.reportTimeStamp, h.holdType, h.reasonCodeID.identifier,
        nil, h.routeID.identifier, h.operationNumber,
        h.relatedLotID.identifier, h.userID.identifier, h.postFlag, h.singleTriggerFlag, h.claimMemo)
    }
    return ret
  end

  # register a future hold, if reason is nil an arbitrary reason code will be selected, return 0 on success
  def lot_futurehold(lot, opNo, reason, params={})
    $log.info "lot_futurehold #{lot.inspect}, #{opNo.inspect}, #{reason.inspect}, #{params}"
    route = params[:route]
    if opNo.nil? || route.nil?
      liraw = lots_info_raw(lot)
      lopinfo = liraw.strLotInfo.first.strLotOperationInfo
      opNo ||= lopinfo.operationNumber
      routeID = route ? oid(route) : lopinfo.routeID
    else
      routeID = oid(route)
    end
    relatedLot = params[:related_lot] || ''
    postFlag = !!params[:post]
    singleTriggerFlag = params[:single]
    singleTriggerFlag = true if singleTriggerFlag.nil? && postFlag
    singleTriggerFlag = false if singleTriggerFlag.nil?
    # MES-711: allow future hold on attached branch routes for privileged users, defaults to true in F1/F8
    privileged = params.has_key?(:privileged) ? params[:privileged] : !@f7
    u = privileged ? user_struct(@_user.userID.identifier, @_user.password, function: 'TXPCC542') : @_user
    memo = params[:memo] || ''
    #
    holdtype = params[:holdtype] || 'FutureHold' # ReworkHold, MergeHold, FutureHold
    if reason.nil?
      reasonID = code_list(holdtype).first.code
    else
      cinfo = code_list(holdtype).find {|e| e.code.identifier == reason}
      ($log.warn 'lot_futurehold: wrong reason code'; return) unless cinfo
      reasonID = cinfo.code
    end
    #
    res = @manager.TxEnhancedFutureHoldReq(u, holdtype, oid(lot), routeID,
      opNo, reasonID, oid(relatedLot), postFlag, singleTriggerFlag, memo)
    return rc(res)
  end

  # release future hold indicated by hold reason (may be nil or ''), return 0 on success
  def lot_futurehold_cancel(lot, hreason, params={})
    lotID = lot.instance_of?(String) ? oid(lot) : lot
    hlist = lot_futurehold_list(lotID, params.merge(reason: hreason, raw: true)) || return
    # keep = params[:keep]
    # keep = [keep] if keep.instance_of?(String)
    holds = []
    hlist.each {|h|
      # next if keep && keep.member?(h.reasonCodeID.identifier)
      # next if hreason && hreason != h.reasonCodeID.identifier
      holds << @jcscode.pptHoldReq_struct.new(h.holdType, h.reasonCodeID, h.userID, h.operationName,
        h.routeID, h.operationNumber, h.relatedLotID, h.claimMemo, h.siInfo)
    }
    if holds.empty?
      $log.info "lot #{lotID.identifier} has no futureholds to cancel" unless params[:silent]
      return 0
    end
    $log.info "lot_futurehold_cancel #{lotID.identifier.inspect}, #{hreason.inspect}, #{params}"
    cancel_reason = params[:reason] || 'ALL'
    cinfo = code_list('FutureHoldCancel').find {|e| e.code.identifier == cancel_reason}
    ($log.warn '  wrong cancel reason code'; return) unless cinfo
    #
    res = @manager.TxFutureHoldCancelReq(@_user, lotID, cinfo.code, 'Cancel', holds)
    return rc(res)
  end

  # update claim memo, specify hold criteria by params, return 0 on success
  def lot_futureholdmemo_update(lot, memo, params={})
    $log.info "lot_futureholdmemo_update #{lot.inspect}, #{memo.inspect}, #{params}"
    hlist = lot_futurehold_list(lot, params.merge(raw: true))
    ($log.warn '  no such futurehold'; return) if hlist.empty?
    ($log.warn '  multiple futureholds found'; return) if hlist.count > 1
    h = hlist.first
    #
    res = @manager.CS_TxFutureHoldClaimMemoUpdateReq(@_user, h.holdType, oid(lot), h.routeID, h.operationNumber,
      h.reasonCodeID, h.userID, h.relatedLotID, h.postFlag, h.singleTriggerFlag, memo)
    return rc(res)
  end

  # set a running hold, return 0 on success
  def running_hold(lot, eqp, cj, params={})
    eqpID = eqp.instance_of?(String) ? oid(eqp) : eqp
    cjID = cj.instance_of?(String) ? oid(cj) : cj
    memo = params[:memo] || ''
    $log.info "running_hold #{lot.inspect}, #{eqpID.identifier.inspect}, #{cjID.identifier.inspect}"
    $log.info "  #{params}" unless params.empty?
    reason = params[:reason] || 'RNHL'
    cinfo = code_list('LotHold').find {|e| e.code.identifier == reason}
    ($log.warn '  wrong reason code'; return) unless cinfo
    #
    res = @manager.TxRunningHoldReq(@_user, eqpID, cjID, cinfo.code, memo)
    return rc(res)
  end

  # return 0 on success
  def lot_bankhold(lots, params={})
    lots = [lots] if lots.instance_of?(String)
    memo = params[:memo] || ''
    reason = params[:reason] || 'EGHL'
    $log.info "lot_bankhold #{lots}, #{params}"
    cinfo = code_list('BankHold').find {|e| e.code.identifier == reason}
    ($log.warn '  wrong reason code'; return) unless cinfo
    #
    res = @manager.TxHoldBankLotReq(@_user, oids(lots), cinfo.code, memo)
    return rc(res)
  end

  # return 0 on success
  def lot_bankhold_release(lots, params={})
    memo = params[:memo] || ''
    reason = params[:reason] || 'EGRL'
    lotIDs = _as_objectIDs(lots)
    $log.info "lot_bankhold_release #{lotIDs.collect {|e| e.identifier}}, #{params}"
    cinfo = code_list('BankHoldRelease').find {|e| e.code.identifier == reason}
    ($log.warn '  wrong reason code'; return) unless cinfo
    #
    res = @manager.TxHoldReleaseBankLotReq(@_user, lotIDs, cinfo.code, memo)
    return rc(res)
  end

  # return Array of LotHold for claim memo history (empty for old lots), used in MM_LotHoldClaimMemo only
  def lot_holdmemo_history(lot, params={})
    reason = params[:reason]
    user = params[:user]
    hlist = lot_hold_list(lot, raw: true).collect {|h|
      next if user && h.userID.identifier != user
      next if reason && h.reasonCodeID.identifier != reason
      h
    }.compact
    ($log.warn "no such hold for lot #{lot}"; return) if hlist.empty?
    ($log.warn "multiple holds found for lot #{lot}"; return) if hlist.count > 1
    h = hlist.first
    hreq = @jcscode.pptHoldReq_struct.new(h.holdType, h.reasonCodeID, h.userID, h.responsibleOperationMark,
      h.responsibleRouteID, h.responsibleOperationNumber, h.relatedLotID, h.claimMemo, h.siInfo)
    #
    res = @manager.CS_TxHoldClaimMemoHistoryListInq(@_user, oid(lot), hreq, h.holdTimeStamp)
    return nil if rc(res) != 0
    return res.strLotHoldListAttributes.collect {|h|
      LotHold.new(h.holdTimeStamp, h.holdType, h.reasonCodeID.identifier,
        h.responsibleOperationMark, h.responsibleRouteID.identifier, h.responsibleOperationNumber,
        h.relatedLotID.identifier, h.userID.identifier, nil, nil, h.claimMemo)
    }
  end

  # update claim memo, specify hold criteria by params, return 0 on success
  def lot_holdmemo_update(lot, memo, params={})
    $log.info "lot_holdmemo_update #{lot.inspect}, #{memo.inspect}, #{params}"
    # optional filter
    reason = params[:reason]
    user = params[:user]
    hlist = lot_hold_list(lot, raw: true).collect {|h|
      next if user && h.userID.identifier != user
      next if reason && h.reasonCodeID.identifier != reason
      h
    }.compact
    ($log.warn 'no such lot hold'; return) if hlist.empty?
    ($log.warn 'multiple holds found'; return) if hlist.count > 1
    h = hlist.first
    hold = @jcscode.pptHoldReq_struct.new(h.holdType, h.reasonCodeID, h.userID, h.responsibleOperationMark,
        h.responsibleRouteID, h.responsibleOperationNumber, h.relatedLotID, memo, h.siInfo)
    #
    res = @manager.CS_TxHoldClaimMemoUpdateReq(@_user, oid(lot), hold, h.holdTimeStamp)
    return rc(res)
  end

end
