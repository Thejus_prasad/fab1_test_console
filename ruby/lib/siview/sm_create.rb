=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: Steffen Steidten, 2009-05-03; Daniel Steger, 2014-10-30

History:
  2021-12-16 sfrieske, cleaned up update_udata
  2022-01-03 sfrieske, added create_userdefinedcode
=end


class SiView::SM

  # create a new brObjectInfo_struct, internally used
  def _create_oinfo(name, cinfo)
    return @jcscode.brObjectInfo_struct.new(
      oid(name),
      @jcscode.brPropertyInfo__090.new(
        oid(''), '', oid(''), '' ,oid(''), '', oid(''), '', oid(''), '', oid(''), '' ,oid(''), '', oid(''), '',
        oid(''), oid(''), '', '', '', '', '', 'Public', 'Public', 'Public', oid(''), oid(''), oid(''), oid(@user), false),
      @jcscode.brDocumentEntityInfo_struct.new('', false, false, false,
        oid(''), '', oid(''), '', oid(''), '', oid(''), '', oid(''), '', oid(''), ''),
      cinfo.nil? ? nil : insert_si(cinfo)
    )
  end

  # create a new SM object, return sequence of brObjectInfo_struct, internally used, use with care!!
  def _create_object(classname, oname, cinfo, params={})
    $log.info "create_object #{classname.inspect}, #{oname.inspect}"
    if params[:ondemand]
      ($log.info '  already exists, skipping'; return true) unless object_ids(classname, oname).empty?
    end
    if desc = params[:description]
      cinfo.description = desc
    end
    oinfo = _create_oinfo(oname, cinfo)
    if params[:editcomplete] == false
      # currently for :changenotice only
      return save_objects(classname, [oinfo].to_java(@jcscode.brObjectInfo_struct))
    end
    oinfos = save_edit_complete(classname, [oinfo].to_java(@jcscode.brObjectInfo_struct)) || return
    return oinfos if params[:release] == false   # currently unused
    return release_objects(classname, oinfos)
  end

  # create a new change notice, state InEdit (cannot be checked in as long as it is empty)
  def create_changenotice(name, params={})
    cinfo = @jcscode.brChangeNoticeInfo__110.new(name, name, '', [].to_java(:string),
      [].to_java(@jcscode.brChangeItemData_struct), [].to_java(@jcscode.brChangeItemData_struct), [].to_java(@jcscode.userDataSet_struct), any)
    return _create_object(:changenotice, name, cinfo, params.merge(editcomplete: false))
  end

  # create a new user defined code like 'ProgramID.28', return true on success
  def create_userdefinedcode(name, params={})
    category, code = name.split('.')
    cinfo = @jcscode.brUserDefinedCodeInfo_struct.new(name, oid(category), code, '', [].to_java(@jcscode.userDataSet_struct), any)
    return _create_object(:userdefinedcode, name, cinfo, params)
  end

  def create_technology(name, params={})
    ownerid = object_ids(:user, params[:owner] || 'G-FAT').first
    cinfo = @jcscode.brTechnologyInfo_struct.new(name, name, '', ownerid, [].to_java(@jcscode.userDataSet_struct), any)
    return _create_object(:technology, name, cinfo, params)
  end

  def create_productgroup(name, technology, params={})
    techid = object_ids(:technology, technology).first
    ownerid = object_ids(:user, params[:owner] || 'G-FAT').first
    cinfo = @jcscode.brProductGroupInfo_struct.new(name, name, '', techid, ownerid, 
      @jcscode.coordinate2D_struct.new(0, 0), 0, 100, 153, oids([]), [].to_java(@jcscode.userDataSet_struct), any)
    return _create_object(:productgroup, name, cinfo, params)
  end

  def create_product(name, productgroup, params={})
    prdprefix, ec = name.split('.')
    state = params[:state] || 'Draft'  # allows to check in w/o route, also 'Complete', 'Obsolete'
    pgid = object_ids(:productgroup, productgroup).first || ($log.warn '  wrong productgroup'; return)
    prdcatid = object_ids(:productcategory, 'Production').first
    ownerid = object_ids(:user, params[:owner] || 'G-FAT').first
    if srcproducts = params[:srcproducts]
      srcprids = object_ids(:product, srcproducts) || ($log.warn '  wrong srcproducts'; return)
    else
      srcprids = oids([])
    end
    mfglayerid = object_ids(:mfglayer, params[:mfglayer] || 'B').first || ($log.warn '  wrong mfglayer'; return)
    if mainpd = params[:mainpd]
      mainpdid = object_ids(:mainpd, mainpd).first || ($log.warn '  wrong mainpd'; return)
      mainprefix, mainversion = mainpd.split('.')
      maindata = [
        @jcscode.brMainProcessDefinitionData_struct.new('*', mfglayerid, mainpdid, mainprefix, mainversion, oid(''))
      ].to_java(@jcscode.brMainProcessDefinitionData_struct)
    else
      mainpdid = oid('')
      mainprefix = ''
      mainversion = ''
      maindata = [].to_java(@jcscode.brMainProcessDefinitionData_struct)
    end
    if reticleset = params[:reticleset]
      rsid = object_ids(:reticleset, reticleset).first || ($log.warn '  wrong reticleset'; return)
    else
      rsid = oid('')
    end
    schdids = object_ids(:usergroup, 'SH-NONE')
    cinfo = @jcscode.brProductInfo__101.new(name, prdprefix, ec, '', state, false, false, pgid, 'Wafer', prdcatid,
      ownerid, srcprids, mainpdid, mainprefix, mainversion, maindata, rsid, 'By Volume', 2, 0, 100, 0, false, 
      mfglayerid, oid(''), '', schdids.first, schdids, [].to_java(@jcscode.userDataSet_struct), any)
    return _create_object(:product, name, cinfo, params)
  end

  def create_photolayer(name, params={})
    cinfo = @jcscode.brPhotoLayerInfo_struct.new(name, name, '', [].to_java(@jcscode.userDataSet_struct), any)
    return _create_object(:photolayer, name, cinfo, params)
  end

  def create_reticlegroup(name, params={})
    cinfo = @jcscode.brReticleGroupInfo_struct.new(name, name, '', [].to_java(@jcscode.userDataSet_struct), any)
    return _create_object(:reticlegroup, name, cinfo, params)
  end

  # data is an array of {:photolayer=>"RX", :rgs=>["G9040B00RXBZLA"], :override=>false}
  def create_reticleset(name, data, params={})
    rgdef = data.collect {|e|
      @jcscode.brDefaultReticleGroupData_struct.new(
        object_ids(:photolayer, e[:photolayer]).first, object_ids(:reticlegroup, e[:rgs]), !!e[:override])
    }.to_java(@jcscode.brDefaultReticleGroupData_struct)
    rgspec = [].to_java(@jcscode.brSpecificReticleGroupData_struct)
    cinfo = @jcscode.brReticleSetInfo_struct.new(name, name, '', rgdef, rgspec, [].to_java(@jcscode.userDataSet_struct), any)
    return _create_object(:reticleset, name, cinfo, params)
  end

  def create_reticle(name, rg, params={})
    rgid = object_ids(:reticlegroup, rg).first
    cinfo = @jcscode.brReticleInfo_struct.new(name, name, '', '', false, 0, 0, '', 'B1', 'TPI, DR', 
      rgid, 0, oid(''), false, [].to_java(@jcscode.userDataSet_struct), any)
    return _create_object(:reticle, name, cinfo, params)
  end

end
