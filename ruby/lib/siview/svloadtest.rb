=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: Steffen Steidten, 2010-12-09

History:
  2017-05-08 sfrieske, replaced select_one by select
  2017-11-13 sfrieske, cleaned up
=end

require 'pp'
# require 'thread'
require 'dummytools'
require 'remote'
require 'siview'
require_relative 'svlog'

# Autoproc viewer: http://f38asq10:8580/autoproc-war/index.jsf

# Monitor Load Test Performance Parameters
class SvLoadTest
  attr_accessor :env, :mutex, :sv, :routes, :mmdb, :mds, :svlog, :svlog_prod, :tools, :dummyamhs, :dummyamhs_svrs, :autoproc_svrs,
    :carriers, :stocker

  def initialize(env, params={})
    @env = env
    @mutex = Mutex.new
    @routes = params[:routes] || ['LET2-ROUTE01.01']
    @tools = params[:tools] || ((1000..1149).to_a + (2000..2149).to_a).collect {|e| 'LETCL%4.4d' % e}
    @sv = SiView::MM.new(@env)
    @svlog = SvLog.new(@env)
    @svlog_prod = SvLog.new('f1prod')
    @dummyamhs = Dummy::AMHS.new(@env)
    hosts = @dummyamhs.host
    hosts = [hosts] if hosts.kind_of?(String)
    @dummyamhs_svrs = hosts.uniq.collect {|e| RemoteHost.new("cei.#{@env}", host: e)}
    @autoproc_svrs = RemoteHostGroup.new("fabgui.#{@env}").hosts
    @mmdb = DB.new("siviewmm.#{@env}")
    @mds = DB.new("mds.#{@env}")
    # for minitoring and repair
    @carriers = 'LS%'
    @stocker = 'ZFG0410'
  end

  def start_test
    ok = true
    @routes.each {|r|
      ok &= @sv.inhibit_cancel(:route, r, memo: 'load test start').zero?
    }
    return ok
  end

  def stop_test
    ok = true
    @routes.each {|r|
      ok &= !@sv.inhibit_entity(:route, r, memo: 'load test stop', tend: '').nil?
    }
    return ok
  end

  def is_started
    return @routes.inject(true) {|ok, r| ok && @sv.inhibit_list(:route, r).empty?}
  end

  # monitoring and repair

  def held_lots(params={})
    lots = params[:lots]
    unless lots
      carriers = params[:carriers] || @sv.carrier_list(carriers: @carriers)
      lots = carriers.collect {|c| @sv.carrier_status(c).lots}.flatten
    end
    ret = []
    lots.each {|lot|
      unless @sv.lot_hold_list(lot, raw: true).empty?
        ret << lot
        @sv.lot_hold_release(lot, nil) if params[:cleanup]
      end
    }
    return ret
  end

  # get carriers stuch in EO or SO, pass cleanup: true to clean up
  def stuck_carriers(params={})
    tcut = params[:tcut]|| (Time.now - 600)  # 10 m
    carriers = params[:carriers] || @sv.carrier_list(carriers: @carriers)
    cis = carriers.collect {|c| @sv.carrier_status(c)}
    cis.select! {|ci| ['EO', 'SO', 'MO', 'AO', 'HO'].member?(ci.xfer_status) && @sv.siview_time(ci.claim_time) < tcut}
    ret = []
    cis.each {|ci|
      ret << ci.carrier
      @sv.carrier_xfer_status_change(ci.carrier, '', @stocker, manualin: true) if params[:cleanup]
    }
    return ret
  end

  # get stale reservations or control jobs, pass cleanup: true to clean up
  def stale_jobs(params={})
    tcut = params[:tcut]|| (Time.now - 900)
    eqpis = eqp_infos
    rsvs, cjs = [], []
    eqpis.each_pair {|eqp, eqpi|
      eqpi.cjs.each {|cj|
        # TODO ??
      }
      eqpi.ports.each {|p|
        if !p.cj.empty?
          cjs << p.cj if @sv.siview_time(p.disp_time) < tcut
        elsif !p.rsv_carrier.empty?
          li = @sv.eqp_lotsinfo_for_opestart(eqp, p.rsv_carrier)
          ($log.warn "-> mismatching data for eqp #{eqp.inspect}, carrier #{p.rsv_carrier}"; next) if li.nil?
          if @sv.siview_time(p.disp_time) < tcut  # this seems to be wrong!!
            rsvs << li.controlJobID.identifier
            @sv.slr_cancel(eqp, li.controlJobID.identifier) if params[:cleanup]
          end
        end
        # TODO: p.disp_load_carrier, p.disp_unload_carrier ?
      }
    }
    return [rsvs.sort, cjs.sort]
  end


  # tools

  def eqp_infos(params={})
    # get eqp_info for all configured tools, whatnext is size in EqpInfo.chambers
    return unless @tools
    tcut = params[:tcut]|| (Time.now - 1800)
    wnmax = params[:distribute]
    wnmax = 60 if wnmax == true
    eqpis = {}
    gplots = []
    rsvs = []
    threads = @tools.collect {|eqp|
      sleep 1 if params[:distribute]
      sleep 0.1 if params[:cleanup] || params[:whatnext]
      Thread.new {
        eqpi = @sv.eqp_info(eqp)
        if params[:whatnext]
          wns = @sv.what_next(eqp)
          eqpi.chambers = wns.length if eqpi # misusing chambers to report what next list length
          if wnmax && wns.length > wnmax
            wns[wnmax..-1].each {|wn|
              lot = wn.lot
              next if gplots.include?(lot)
              gplots << lot
              @sv.lot_gatepass(lot)
            }
          end
        end
        if params[:cleanup]
          eqpi.ports.each {|p|
            if !p.cj.empty?
              ##cjs << p.cj if @sv.siview_time(p.disp_time) < tcut
            elsif !p.rsv_carrier.empty?
              li = @sv.eqp_lotsinfo_for_opestart(eqp, p.rsv_carrier)
              ($log.warn "-> mismatching data for eqp #{eqp.inspect}, carrier #{p.rsv_carrier}"; next) if li.nil?
              if @sv.siview_time(p.disp_time) < tcut  # this seems to be wrong!!
                rsvs << li.controlJobID.identifier
                @sv.slr_cancel(eqp, li.controlJobID.identifier) if params[:cleanup]
              end
            end
            # TODO: p.disp_load_carrier, p.disp_unload_carrier ?
          }
        end
        eqpis[eqp] = eqpi
      }
    }
    threads.each {|t| t.join}
    $log.info "re-distributed #{gplots.size} lots" if params[:distribute]
    $log.info "canceled #{rsvs.size} reservations" if params[:cleanup]
    return eqpis
  end

  # show number of running jobs, per tool if verbose: true
  def show_eqp_status(params={})
    rsvs = cjs = []
    eqpis = eqp_infos(params) || return
    @tools.each {|eqp|
      eqpi = eqpis[eqp]
      if eqpi
        pss = eqpi.ports.collect {|p| '%-10s' % p.state}.join(' ')
        n = eqpi.chambers.kind_of?(Numeric) ? eqpi.chambers : -1  # whatnext length
        puts("#{eqp} %4d  #{eqpi.status} #{pss}   #{eqpi.cjs.size}  #{eqpi.rsv_cjs.size}" % n) if params[:verbose]
        cjs += eqpi.cjs
        rsvs += eqpi.rsv_cjs
      else
        puts "#{eqp} error"
      end
    }
    puts "tools: #{@tools.size}, running jobs: #{cjs.size}, reservations: #{rsvs.size}"
    (puts "control jobs:"; pp cjs; puts "reservations:"; pp rsvs) if params[:jobs]
  end

  # # based on cj or rsv id, cancel jobs and reservations older than ~1day, attn: cleans the whole eqp!, useful ????
  # def _cancel_stale_jobs
  #   eqps = []
  #   tcut = Time.now - 86400
  #   (@rsvs + @cjs).each {|job|
  #     eqp, tstring, _ = job.split('-')
  #     t = Time.parse(tstring)
  #     if t < tcut
  #       eqps << eqp
  #     else
  #     end
  #   }
  #   eqps.uniq.each {|eqp| @sv.eqp_cleanup(eqp)}
  #   return show_eqp_status(jobs: true)
  # end

  # server machines

  def server_status
    res = (@autoproc_svrs + @dummyamhs_svrs).collect {|h|
      [h.host, (h.close; h.open;  h.exec!('uptime').gsub(',', ' ').split[-3..-1])]
    }
    return Hash[res]
  end

  # databases

  # get the size of critical MM event tables
  def mm_eventtables_count
    # E10 status, lot hold, ??, system messages
    ret = ['FREVCMBS_FIFO', 'FREVLHLD_FIFO', 'FREVPRST_FIFO', 'FREVSYSM_FIFO'].collect {|t|
      res = @mmdb.select("select count(*) from #{t}").first
      [t, res ? res.first : nil]
    }
    return Hash[ret]
  end

  # get the number MDS event table entries
  def mds_event_count
    res = @mds.select("select count(*) from T_MDS_EVENT where TARGET_APPLICATION = 'DISP_UNLOAD_A'")
    return res ? res.first.first : nil
  end

  # 30 k wafer out per month correspond to ~ 1000 OpeStarts per hour
  def opestarts_prod
    puts "\nNumber of OpeStarts per hour in Production"  # note: very slow, takes ~ 5 minutes
    puts '=' * 42
    t = Time.now - 86400 * 14
    while t < Time.now
      osp = @svlog_prod.get_opestarts(tmin: t, interval: 86400) / 24
      puts "#{t.strftime('%F')}  #{osp}"
      t += 86400
    end
  end

  # summary

  def status(params={})
    sleeptime = params[:sleep]
    loop {
      nstarts = 0
      tstarts = Thread.new {nstarts = @svlog.get_opestarts(interval: 900) * 4}
      puts "\nLoadTest Status, #{Time.now.strftime('%F %T')}"
      puts '=' * 36
      puts "Test started: #{is_started}"
      puts 'SiView event table sizes:'
      puts mm_eventtables_count.pretty_inspect
      puts "MDS AutoProc event table size: #{mds_event_count}"
      puts "DummyAMHS job queues: #{@dummyamhs.queue.collect {|k, v| v}.inspect}"
      puts "Server Load:"
      pp server_status
      show_eqp_status(params)
      tstarts.join()
      puts "OpeStarts per hour: #{nstarts}"
      sleeptime ? sleep(sleeptime) : break
    }
    true
  end

end
