=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.
=end

class SiView::MM
  LotOperation = Struct.new(:route, :opNo, :op, :opName, :pass, :stage, :masklevel, :mandatory, :timestamp, :category, :memo)
  LotOpeNote = Struct.new(:time, :user, :route, :op, :opNo, :title, :note)


  # return current ModulePD
  def AMD_modulepd(lot, params={})
    res = @manager.AMD_TxModulePDInq(@_user, oid(lot))
    return nil if rc(res) != 0
    return res.modulePD
  end

  # lot operation history with missing parameters taken from lot_operation_list_from_history.
  # return array of LotOperation structs, with the most current op at last element
  def lot_operation_history(lot, params={})
    op = params[:op] || ''
    opCategory = params[:opCategory] || ''  # can only be used if route, opNo and pass are not empty and matching
    pinpoint = !!params[:pinpoint]          # not used, defaults to false
    route = params[:route] || ''
    opNo = params[:opNo] || ''
    pass = (params[:pass] || '').to_s
    lotID = lot.instance_of?(String) ? oid(lot) : lot
    # try to get missing parameters, does not work if no operation is found (vendor lots)
    if (params[:history] != false) && (route.empty? || opNo.empty? || pass.empty?)
      hops = lot_operation_list_fromhistory(lotID) || ($log.warn "no operation history for lot #{lotID.identifier}"; return)
      # hindex = params[:hindex]  # -1 .. first (incl STB), 0 .. last, 1 .. before last
      hindex = params[:first] ? hops.size - 1 : 0  # operations are newest first
      ophi = hops[hindex] || ($log.warn "no operation history for lot #{lotID.identifier}, hindex: #{hindex.inspect}"; return)
      route = ophi.routeID.identifier if route.empty?
      opNo = ophi.operationNumber if opNo.empty?
      pass = ophi.operationPass if pass.empty?
    end
    # filtering by opCategory works only for matching route, opNo and pass!
    $log.debug {"lot_operation_history #{lotID.identifier.inspect}, route: #{route.inspect}, opNo: #{opNo.inspect}, pass: #{pass.inspect}"}
    #
    res = @manager.TxOperationHistoryInq__101(@_user, lotID, oid(route), oid(op), opNo, pass, opCategory, pinpoint)
    return nil if rc(res) != 0
    return res.strOperationHisInfo if params[:raw]
    #
    category = params[:category]  # filtering all entries by category, TODO: use opCategory instead
    return res.strOperationHisInfo.collect {|e|
      next if category && e.operationCategory != category
      LotOperation.new(e.routeID, e.operationNumber, e.operationID, e.operationName,
        e.operationPass, e.stageID, e.maskLevel, nil, e.reportTimeStamp, e.operationCategory, e.claimMemo)
    }.compact
  end

  # brief operation history of the lot
  def lot_operation_list_fromhistory(lot, params={})
    lotID = lot.instance_of?(String) ? oid(lot) : lot
    res = @manager.TxLotOperationListFromHistoryInq(@_user, 10000, lotID)
    return nil if rc(res) != 0
    return res.strOperationNameAttributes
  end

  # for Route Lookahead as in MM OPI: forward: true, history: false, current: false
  def lot_operation_list(lot, params={})
    forward = (params[:forward] != false)
    history = !!params[:history]  # normally false if forward, true if backward
    searchcount = params[:searchcount] || 10000
    currentFlag = (params[:current] != false)
    lotID = lot.instance_of?(String) ? oid(lot) : lot
    #
    res = @manager.TxLotOperationListInq(@_user, forward, history, searchcount, currentFlag, lotID)
    return nil if rc(res) != 0
    return res.strOperationNameAttributes if params[:raw]
    return res.strOperationNameAttributes.collect {|e|
      LotOperation.new(e.routeID.identifier, e.operationNumber, e.operationID.identifier, e.operationName,
        e.operationPass, e.stageID.identifier, e.maskLevel, e.mandatoryOperationFlag)
    }
  end

  # guess target opNo and route for opelocate from a set of symbolic parameters, used by lot_opelocate only
  def _guess_op_route(lot, opNo, params)
    lotID = lot.instance_of?(String) ? oid(lot) : lot
    offset = params[:offset] || 0  # might not work if the target opNo is close to the current one
    op = params[:op] # can be a regular expression
    if opNo.nil?
      stage = params[:stage]
      ops_fwd = lot_operation_list(lotID, forward: true, history: false, current: params[:current], raw: true)
      if stage
        idx = ops_fwd.find_index {|e| e.stageID.identifier == stage}
      elsif op.instance_of?(Regexp)
        idx = ops_fwd.find_index {|e| e.operationID.identifier =~ op}
      else
        idx = ops_fwd.find_index {|e| e.operationID.identifier == op}
      end
      return [ops_fwd[idx + offset], true] if idx && (idx + offset >= 0) && ops_fwd[idx + offset]
      #
      ops_bck = lot_operation_list(lotID, forward: false, history: true, current: params[:current], raw: true)
      if stage
        idx = ops_bck.find_index {|e| e.stageID.identifier == stage}
      elsif op.instance_of?(Regexp)
        idx = ops_bck.find_index {|e| e.operationID.identifier =~ op}
      else
        idx = ops_bck.find_index {|e| e.operationID.identifier == op}
      end
      return [ops_bck[idx + offset], false] if idx && (idx + offset >= 0) && ops_bck[idx + offset]
    elsif opNo.instance_of?(Integer)
      # locate number of ops forward or backward if negative
      forward = (opNo >= 0)
      i = opNo.abs
      ops = forward ? lot_operation_list(lotID, forward: true, history: false, current: params[:current], raw: true)
                    : lot_operation_list(lotID, forward: false, history: true, current: params[:current], raw: true)
      ($log.warn "error: not enough ops on route to locate #{lotID.identifier}"; return) unless ops.size > i
      return [ops[i], forward]
    elsif opNo.to_sym == :first
      ops_bck = lot_operation_list(lotID, forward: false, history: true, current: params[:current], raw: true)
      ops = params[:route] ? ops_bck.select {|o| o.routeID.identifier == params[:route]} : ops_bck
      return [ops[ops.size - 1 - offset], false]
    elsif opNo.to_sym == :last
      ops_fwd = lot_operation_list(lotID, forward: true, history: false, current: params[:current], raw: true)
      ops = params[:route] ? ops_fwd.select {|o| o.routeID.identifier == params[:route]} : ops_fwd
      return [ops[ops.size - 1 + offset], true]
    else
      # decide on forward or backward location
      ops = lot_operation_list(lotID, forward: true, history: false, current: params[:current], raw: true)
      ops = ops.select {|o| o.routeID.identifier == params[:route]} if params[:route]
      idx = ops.find_index {|o| o.operationNumber == opNo}
      return [ops[idx + offset], true] if idx && (idx + offset >= 0) && ops[idx + offset]
      ops = lot_operation_list(lotID, forward: false, history: true, current: params[:current], raw: true)
      ops = ops.select {|o| o.routeID.identifier == params[:route]} if params[:route]
      idx = ops.find_index {|o| o.operationNumber == opNo}
      return [ops[idx + offset], false] if idx && (idx + offset >= 0) && ops[idx + offset]
    end
    if opNo.nil?
      $log.warn "  op #{op.inspect}#{', offset ' + offset.to_s unless offset.zero?} not found on route for lot #{lotID.identifier}"
    else
      $log.warn "  opNo #{opNo.inspect}#{', offset ' + offset.to_s unless offset.zero?} not found on route for lot #{lotID.identifier}"
    end
    return
  end

  # locates a lot to a given operation number along a route
  #   if opNo is Numeric, a number operations forward or backward
  #   use TxForceOpeLocateReq if parameter :force is set
  # return 0 on success
  def lot_opelocate(lot, opNo, params={})
    # newRoute = params[:route]
    forward = params[:forward]
    memo = params[:memo] || ''
    lotID = lot.instance_of?(String) ? oid(lot) : lot
    p = params.keep_if {|k, v| !v.nil?}
    $log.info "lot_opelocate #{lotID.identifier.inspect}, #{opNo.inspect}#{(', ' + p.inspect) unless p.empty?}"
    res = _guess_op_route(lotID, opNo, params) || return
    opdata, forward = res
    newRoute = opdata.routeID
    newOp = opdata.operationID
    newOpNo = opdata.operationNumber
    processref = opdata.processRef
    liraw = lots_info_raw(lotID) || return
    lotID = liraw.strLotInfo.first.strLotBasicInfo.lotID
    curRoute = liraw.strLotInfo.first.strLotOperationInfo.routeID
    curOpNo = liraw.strLotInfo.first.strLotOperationInfo.operationNumber
    if curOpNo == newOpNo && curRoute.identifier == newRoute.identifier
      $log.info "  already at #{newRoute.identifier} #{newOpNo}"
      return 0
    end
    $log.info "  locating #{forward ? 'forward' : 'backward'} to #{newRoute.identifier} #{newOpNo}"
    #
    force = params[:force]
    if force == :ondemand
      hstat = liraw.strLotInfo.first.strLotBasicInfo.strLotStatusList.find {|e| e.stateName == 'Lot Hold State'}
      force = hstat.stateValue == 'ONHOLD'
    end
    if force
      lparm = @jcscode.pptForceOpeLocateReqInParm_struct.new(forward, lotID,
      curRoute, curOpNo, newRoute, newOp, newOpNo, processref, 0, any)
      res = @manager.TxForceOpeLocateReq(@_user, lparm, memo)
    else
      res = @manager.TxOpeLocateReq(@_user, forward, lotID, curRoute, curOpNo,
        newRoute, newOp, newOpNo, processref, 0, memo)
    end
    return rc(res)
  end

  # requeue lot to activate changed setup, return 0 on success
  def lot_requeue(lots, params={})
    memo = params[:memo] || ''
    lots = [lots] if lots.instance_of?(String)
    opNo = params[:opNo]
    route = params[:route]
    product = params[:product]
    attrs = lots.collect {|lot|
      unless opNo && route && product
        li = lot_info(lot)
        opNo ||= li.opNo
        route ||= li.route
        product ||= li.product
      end
      @jcscode.pptLotReQueueAttribute_struct.new(oid(lot), oid(product), oid(route), opNo, any)
    }
    $log.info "lot_requeue #{lots.inspect}, #{params.inspect}"
    #
    res = @manager.TxLotReQueueReq(@_user, attrs, memo)
    return rc(res)
    ## There is a bug in the return if the request failed
    #return res.strLotReQueueReturn.collect {|ret|
    #  ret.returnCode != '' ? ret.returnCode.to_i : _rc
    #}
  end

  def lot_remeasure(lot, params={})
    $log.info "lot_remeasure #{lot.inspect}, #{params}"
    memo = params[:memo] || ''
    li = lot_info(lot) || return
    # previous op
    opdata = lot_operation_list(lot, forward: false, history: true, raw: true)[1]
    # force remeasure?
    opdata.processRef.siInfo = insert_si('CheckPreviousPO') unless params[:force]
    # TODO: use opdata for oids
    res = @manager.CS_TxRemeasureLocateReq(@_user, oid(lot), oid(li.route),
      li.opNo, opdata.routeID, opdata.operationID, opdata.operationNumber, opdata.processRef, -1, memo)
    return rc(res)
  end

  # advance lots to the next op without claiming Operation Start/Complete, return 0 for each lot on success
  def lot_gatepass(lots, params={})
    lots = [lots] if lots.instance_of?(String)
    $log.info "lot_gatepass #{lots}, #{params}"
    route = params[:route]
    opNo = params[:opNo]
    memo = params[:memo] || ''
    liraw = lots_info_raw(lots) || return
    gpi = liraw.strLotInfo.collect {|li|
      r = route.nil? ? li.strLotOperationInfo.routeID : oid(route)
      o = opNo || li.strLotOperationInfo.operationNumber
      skip = params[:mandatory] ? insert_si(@jcscode.pptCS_GatePass_siInfo_struct.new('ON', any)) : any
      @jcscode.pptGatePassLotInfo_struct.new(li.strLotBasicInfo.lotID, r, o, skip)
    }
    $log.info "  skipping #{gpi.first.currentRouteID.identifier} #{gpi.first.currentOperationNumber}"
    force = params[:force]
    if force == :ondemand
      hstat = liraw.strLotInfo.first.strLotBasicInfo.strLotStatusList.find {|e| e.stateName == 'Lot Hold State'}
      force = hstat.stateValue == 'ONHOLD'
    end
    if force # returns only one result
      # TODO: 	@manager.CS_TxForceGatePassWithoutHoldReleaseReq(@_user, gpi, memo)
      res = @manager.CS_TxGatePassWithoutHoldReleaseReq(@_user, gpi, memo)
      return rc(res)
    else
      res = @manager.TxGatePassReq(@_user, gpi, memo)
      return rc(res, res.strGatePassLotsResult)
    end
  end

  # call TxWIPLotResetReq, return 0 for each lot on success, currently not used
  def wiplot_reset(lots, params={})
    lots = [lots] if lots.instance_of?(String)
    $log.info "wiplot_reset #{lots}, #{params}"
    route = params[:route]
    opNo = params[:opNo]
    level = params[:level] || 0
    memo = params[:memo] || ''
    wli = lots.collect {|lot|
      if route.nil? || opNo.nil?
        li = lot_info(lot)
        route, opNo = li.route, li.opNo
      end
      @jcscode.pptWIPLotResetAttribute_struct.new(oid(lot), oid(route), opNo, any)
    }.to_java(@jcscode.pptWIPLotResetAttribute_struct)
    inparms = @jcscode.pptWIPLotResetReqInParm_struct.new(level, wli, any)
    #
    res = @manager.TxWIPLotResetReq(@_user, inparms, memo)
    return rc(res)  # additionally com.amd.jcs.siview.code.pptLotInfo__160_struct returned
  end

  # return an array of all notes registered for this lot, except the note itself
  def lot_openote_list(lot, params={})
    res = @manager.TxLotOperationNoteListInq(@_user, oid(lot))
    return nil if rc(res) != 0
    return res if params[:raw]
    return res.strLotOperationNoteList.collect {|e|
      LotOpeNote.new(e.reportTimeStamp, e.reportUserID.identifier,
        e.routeID.identifier, e.operationID.identifier, e.operationNumber, e.noteTitle, nil)
    }
  end

  # return an array of all notes registered for this lot
  def lot_openotes(lot, params={})
    ons = lot_openote_list(lot) || return
    notes = []
    route, op, opNo = nil, nil, nil
    ons.each {|on|
      next if [route, op, opNo] == [on.route, on.op, on.opNo]
      res = @manager.TxLotOperationNoteInfoInq(@_user, oid(lot), oid(on.route), oid(on.op), on.opNo)
      return nil if rc(res) != 0
      return res if params[:raw]
      res.strLotOperationNoteInfo.each {|n| notes << LotOpeNote.new(
        n.reportTimeStamp, n.reportUserID.identifier, on.route, on.op, on.opNo, n.noteTitle, n.lotOperationNoteDescription)
      }
      route, op, opNo = on.route, on.op, on.opNo
    }
    # sort by time, most recent first
    ret = []
    notes.each {|n|
      done = false
      ret.each_with_index {|n1, i|
        if n.time > n1.time
          ret.insert(i, n)
          done = true
          break
        end
      }
      ret << n unless done
    }
    return ret
  end

  # register a lot operation note, return 0 on success
  def lot_openote_register(lot, title, note, params={})
    $log.info "lot_openote_register #{lot.inspect}, #{title.inspect}, #{note.inspect}"
    route, op, opNo = params[:route], params[:op], params[:opNo]
    unless route && op && opNo
      # fill with current parameters
      li = lot_info(lot) || return
      route ||= li.route
      op ||= li.op
      opNo ||= li.opNo
    end
    #
    res = @manager.TxLotOperationNoteInfoRegistReq(@_user, oid(lot), oid(route), oid(op), opNo, title, note)
    return rc(res)
  end

  # return array of arrays [route, opNo] of a lot, ending with the current one, used in MM_Lot_BranchOpe only
  # (used by APC)
  def lot_branch_ope(lot, params={})
    res = @manager.CS_TxBranchOperationForLotInq(@_user, @jcscode.pptCS_BranchOperationForLotInqInParm_struct.new(oid(lot), any))
    return nil if rc(res) != 0
    return res if params[:raw]
    return res.strOriginalOperationInfoSeq.collect {|oinfo|
      [oinfo.routeID.identifier, oinfo.opeNo]
    } + [[res.currentRouteID.identifier, res.currentOperationNumber]]
  end

end
