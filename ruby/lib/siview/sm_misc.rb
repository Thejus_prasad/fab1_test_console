=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: Steffen Steidten, 2009-05-03; Daniel Steger, 2014-10-30

History:
  2021-12-16 sfrieske, cleaned up update_udata
  2022-01-03 sfrieske, added create_userdefinedcode
=end


class SiView::SM

  # checkout, edit, checkin and release objects
  #
  # editing is executed externally within a block that takes the checked out objects as argument
  # and must return anything truthy to allow checkin and release
  #
  # return sequence of brObjectInfo_struct on success or nil
  def edit_objects(classname, objs, params={}, &blk)
    $log.debug {"edit_objects #{classname.inspect}, #{objs.inspect}, #{params}"}
    oinfos = edit_checkout(classname, objs)
    # call block with oinfos to edit the new objects, blk must return a truthy value
    blk.call(oinfos) || ($log.warn '  editing failed'; return) if blk
    # save and release
    oinfos2 = save_edit_complete(classname, oinfos) || return
    # return (params[:release] == false) ? ret : release_objects(classname, objs, params)
    return release_objects(classname, oinfos2)
  end

  # # make a main object obsolete, return true on success, UNUSED
  # def obsolete_mainobject(classname, objs, params={})
  #   $log.info "obsolete_mainobject #{classname.inspect}, #{objs.inspect}, #{params}"
  #   oids = object_ids(classname, objs, params)
  #   noreturn = params[:noreturn] != false
  #   tablename, fieldname = SMClasses[classname]
  #   res = @_res = @manager.TxObsoleteObjectForMainObject(tablename, oids, noreturn, @_userinfo)
  #   oinfos = object_info(classname, objs)
  #   return oinfos.inject(true) {|res, e| res && e.obsolete}
  # end

  # # cancel obsolete state of a main object, return true on success, UNUSED
  # def cancel_obsolete_mainobject(classname, objs, params={})
  #   $log.info "cancel_obsolete_mainobject #{classname.inspect}, #{objs.inspect}, #{params}"
  #   oids = object_ids(classname, objs, params)
  #   noreturn = params[:noreturn] != false
  #   tablename, fieldname = SMClasses[classname]
  #   res = @_res = @manager.TxCancelObsoleteObjectForMainObject(tablename, oids, noreturn, @_userinfo)
  #   oinfos = object_info(classname, objs)
  #   return oinfos.inject(true) {|res, e| res && !e.obsolete}
  # end

  # change carrier data, e.g. contents, return sequence of brObjectInfo_struct on success, used in JCAP_DirtyFoupTransfer
  def change_carriers(cc, params={})
    cc = [cc] if cc.instance_of?(String)
    $log.info "change_carriers #{cc.inspect}, #{params}"
    category = params[:category]
    catid = object_ids(:carriercategory, category).first if category
    return edit_objects(:carrier, cc, params) {|oinfos|
      oinfos.each {|e|
        # ($log.warn "  no classInfo value for object class #{classname}"; return) unless e.classInfo.value
        if e.classInfo.type.kind == Java::OrgOmgCORBA::TCKind.tk_null
          $log.warn "  no classInfo value for object class #{classname}"
          return
        end
        ci = extract_si(e.classInfo)
        # change contents
        ci.contents = params[:contents] if params.has_key?(:contents)
        # change category
        ci.carrierCategory = catid if params.has_key?(:category)
        # change interval between PM
        ci.intervalBetweenPM = params[:pm_max] if params.has_key?(:pm_max)
        # change maximum start count
        ci.maximumStartCount = params[:starts_max] if params.has_key?(:starts_max)
        #
        e.classInfo = insert_si(ci)
      }
    }
  end

  # # change equipment (basic) data, data is a Hash of parameters to change, return true on success, UNUSED
  # def change_eqp(eqps, data)
  #   eqps = [eqps] if eqps.instance_of?(String)
  #   $log.info "change_eqp #{eqps}, #{data}"
  #   return edit_objects(:eqp, eqps) {|oinfos|
  #     oinfos.each {|e|
  #       ci = extract_si(e.classInfo)
  #       if !(v = data[:e2e]).nil?
  #         ci.eqpToEqpTransferFlag = v
  #       end
  #       if !(v = data[:takeoutin]).nil?
  #         ci.takeOutInTransferFlag = v
  #       end
  #       if !(v = data[:max_batchsize]).nil?
  #         ci.maximumProcessBatchSize = v
  #       end
  #       if !(v = data[:min_batchsize]).nil?
  #         ci.minimumProcessBatchSize = v
  #       end
  #       if !(v = data[:pjctrl]).nil?
  #         ci.processJobControlFlag = v
  #       end
  #       e.classInfo = insert_si(ci)
  #     }
  #   }
  # end

  # change recipe parameter, return sequence of brObjectInfo_struct on success, for RTD_Rule_xxx tests
  def change_recipeparameter(lrcp, parameter, value, params={})
    $log.info "change_recipeparameter #{lrcp.inspect}, #{parameter.inspect}, #{value.inspect}"
    return edit_objects(:lrcp, lrcp, params) {|oinfos|
      oinfos.each {|e|
        ci = extract_si(e.classInfo)
        rp = ci.defaultRecipeSettings[0].recipeParameters.find {|p| p.name == parameter} || ($log.warn "  no such parameter: #{parameter}"; return)
        rp.defaultValue = value
        e.classInfo = insert_si(ci)
      }
    }
  end

  # change the passed udata for the objects specified by classname and objs, return sequence of brObjectInfo_struct on success
  #   no UDATA type check implemented yet
  #   specifying e.g. {'AutomationType'=>''} deletes the UDATA AutomationType
  def update_udata(classname, objs, udatas, params={})
    $log.info "update_udata #{classname.inspect}, #{objs.inspect}, #{udatas}, #{params}"
    uas = @manager.TxGetAttributeForUserDataSets(@_userinfo)
    tablename = SMClasses[classname].first
    # check if UDATA is defined
    udatas.each_key {|k|
      uas.find {|u| u.name == k && u.classId == tablename} || ($log.warn "  undefined UDATA #{k.inspect}"; return)
    }
    # check if an update is needed
    if params[:force] != true     # currently not used, for stress tests
      needed = false
      object_info(classname, objs).each {|o|
        udatas.each_pair {|k, v|
          next if o.udata[k] == v || o.udata[k].nil? && v == ''
          needed = true
          break
        }
        break if needed
      }
      ($log.info '  no update needed'; return true) unless needed
    end
    # edit the objects
    return edit_objects(classname, objs) {|oinfos|
      oinfos.each {|o|
        ci = extract_si(o.classInfo)
        uds = ci.userDataSets.to_a
        udatas.each_pair {|k, v|
          ($log.warn "  illegal value 'nil' for #{k.inspect}"; return) if v.nil?
          ud = uds.find {|u| u.name == k}
          if ud
            # overwrite, deletes it if v is ''
            ud.value = v
          elsif v != ''
            # not set, create new entry
            ua = uas.find {|u| u.name == k && u.classId == tablename}
            uds << @jcscode.userDataSet_struct.new(k, ua.type, v, '')
          else
            # not set but '' passed
            $log.info "  #{k.inspect} currently not set and '' (delete) passed, skipping"
          end
        }
        ci.userDataSets = uds.to_java(@jcscode.userDataSet_struct)
        o.classInfo = insert_si(ci)
      }
    }
  end

end
