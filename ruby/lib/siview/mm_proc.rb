=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.
=end


class SiView::MM

  # return an array of pptProcHoldListAttributes_struct structs or nil
  def process_hold_list(params={})
    count = params[:count] || 100
    route = params[:route] || ''
    opNo = params[:opNo] || ''
    product = params[:product] || ''
    holdtype = params[:holdtype] || ''
    user = params[:user] || ''
    srchkey = @jcscode.pptProcessHoldSearchKey_struct.new(oid(route), opNo,
        oid(product), holdtype, oid(user), any)
    res = @manager.TxProcessHoldListInq(@_user, srchkey, count)
    return nil if rc(res) != 0
    return res.strProcHoldListAttributes
  end

  # return 0 on success
  def process_hold(reason, params={})
    $log.info "process_hold #{reason.inspect}, #{params.inspect}"
    route = params[:route] || ''
    opNo = params[:opNo] || ''
    product = params[:product] || ''
    holdtype = params[:holdtype] || 'ProcessHold'
    exechold = (params[:exechold] != false)
    memo = params[:memo] || ''
    if reason.nil?
      reasonID = code_list(holdtype).first.code
    else
      reasonID = nil
      cinfo = code_list(holdtype).find {|e| e.code.identifier == reason}
      reasonID = cinfo.code unless cinfo.nil?
    end
    ($log.warn "  hold reason #{reason.inspect}, type #{holdtype.inspect} not found"; return) if reasonID.nil?
    #
    res = @manager.TxProcessHoldReq(@_user, holdtype, oid(route), opNo, oid(product), reasonID, exechold, memo)
    return rc(res)
  end

  # cancel process holds, pass individual parameters, or a pptProcHoldListAttributes_struct, return 0 on succes
  def process_hold_cancel(params={})
    hr = params[:holdrecord]
    if hr
      # pptProcHoldListAttributes_struct
      holdtype = hr.holdType
      route = hr.routeID
      opNo = hr.operationNumber
      product = hr.productID
      hreason = hr.reasonCodeID
    else
      holdtype = params[:holdtype] || 'ProcessHold'
      route = oid(params[:route] || '')
      opNo = params[:opNo] || ''
      product = oid(params[:product] || '')
      hreason = oid(params[:holdreason] || '')
    end
    execholdrelease = !!params[:exechold]
    release_reason = params[:release_reason] || params[:reason] || 'PHLC'
    memo = params[:memo] || ''
    $log.info "process_hold_cancel #{params}"
    #
    res = @manager.TxProcessHoldCancelReq(@_user, holdtype, route, opNo, product,
      hreason, oid(release_reason), execholdrelease, memo)
    return rc(res)
  end

  # Register a process hold exception
  #   the TX allows to register exceptions for multiple process holds
  #   required parameters are :route, :product or :reason
  # return 0 on success
  def process_hold_exception(lot, params)
    $log.info "process_hold_exception #{lot.inspect}, #{params}"
    single = !!params[:single]  # defaults to multiple trigger
    memo = params[:memo] || ''
    pholds = process_hold_list(params)
    #
    res = @manager.CS_TxProcessHoldExceptionLotRegistrationReq(@_user, oid(lot), single, pholds, memo)
    return rc(res)
  end

  # Find existing process hold exceptions
  #   wildcards (note: * not %) are supported by :lots, :route and :product
  # return list of process hold exceptions
  def process_hold_exception_list(params={})
    lots = params[:lot] || '*'
    lots = [lots] if lots.instance_of?(String)
    route = params[:route] || '*'
    opNo = params[:opNo] || ''
    product = params[:product] || '*'
    reason = params[:reason] || ''
    user = params[:user] || ''
    count = params[:count] || 100
    #
    res = @manager.CS_TxProcessHoldExceptionLotListInq(@_user, oids(lots), oid(product), oid(route), opNo, oid(reason), oid(user), count)
    return nil if rc(res) != 0
    return res.strCS_ProcessHoldExceptionLotList
  end

  # Cancel existing process hold exceptions, return 0 on succes
  def process_hold_exception_cancel(params)
    $log.info "process_hold_exception_cancel #{params}"
    memo = params[:memo] || ''
    elist = process_hold_exception_list(params)  #.merge(raw: true)).strCS_ProcessHoldExceptionLotList
    #
    res = @manager.CS_TxProcessHoldExceptionLotCancelReq(@_user, elist, memo)
    return rc(res)
  end


  # Returns the lot / cassette info for the lots in a given list of control jobs
  def lot_list_incontroljob(cj)
    cjIDs = _as_objectIDs(cj)
    #
    res = @manager.TxLotListInControlJobInq(@_user, cjIDs)
    return nil if rc(res) != 0
    return res.strControlJobInfo
  end

  # EI to SiView
  #
  # return pptLotInCassette struct for eqp_data_speccheck and eqp_tempdata
  def eqp_data_items(eqp, cj)
    eqpID = eqp.instance_of?(String) ? oid(eqp) : eqp
    cjID = cj.instance_of?(String) ? oid(cj) : cj
    res = @manager.TxDataItemWithTempDataInq(@_user, eqpID, cjID)
    return nil if rc(res) != 0
    return res
  end

  # used in Off-Line mode to send DC data items, see TRK4
  #
  # fills the DC structure with dummy values that serve as Strings or Integers; return 0 on success
  def eqp_data_speccheck(eqp, cj, params={})
    eqpID = eqp.instance_of?(String) ? oid(eqp) : eqp
    cjID = cj.instance_of?(String) ? oid(cj) : cj
    $log.info "eqp_data_speccheck #{eqpID.identifier.inspect}, #{cjID.identifier.inspect}"
    di = eqp_data_items(eqpID, cjID) || return
    data = params[:data] || {}
    str = di.strStartCassette[0]
    str.strLotInCassette.each {|l|
      l.strStartRecipe.strDCDef.each {|dc|
        dc.strDCItem.each {|i| i.dataValue = (data[i.dataCollectionItemName] || '1').to_s}
      }
    }
    #
    res = @manager.TxDataSpecCheckReq(@_user, eqpID, cjID, [str])
    return rc(res)
  end

  # used in On-Line mode to send DC data items, see TRK4
  #
  # fills the DC structure with dummy values that serve as Strings or Integers; return 0 on success
  def eqp_tempdata(eqp, pg, cj, params={})
    eqpID = eqp.instance_of?(String) ? oid(eqp) : eqp
    cjID = cj.instance_of?(String) ? oid(cj) : cj
    $log.info "eqp_tempdata #{eqpID.identifier.inspect}, #{pg.inspect}, #{cjID.identifier.inspect}"
    memo = params[:memo] || ''
    di = eqp_data_items(eqpID, cjID) || return
    data = params[:data] || {}
    str = di.strStartCassette[0]
    str.strLotInCassette.each {|l|
      l.strStartRecipe.strDCDef.each {|dc|
        dc.strDCItem.each {|i| i.dataValue = (data[i.dataCollectionItemName] || '1').to_s}
      }
    }
    #
    res = @manager.TxTempDataRpt(@_user, eqpID, pg, cjID, [str], memo)
    return rc(res)
  end

  # query the DC values, UNUSED
  def lot_dc_history(lot, route, opNo, pass, params={})
    specflag = !!params[:specflag]
    #
    res = @manager.TxCollectedDataHistoryInq__120(@_user, oid(lot), oid(route), opNo, pass.to_s, specflag)
    return nil if rc(res) != 0
    return res
    # return res if params[:raw]
    # ret = res.strControlJobInfoResult.collect {|e|
    #   results = Hash[e.strDCItemResult.collect {|r| [r.dataCollectionItemName, r.dataValue]}]
    #   [e.dataCollectionDefinitionID.identifier, results]
    # }
    # return Hash[ret]
  end

  # report process status to SiView, typically used by EIs (be careful!)
  #
  # actions are ProcessStart, ProcessEnd; return 0 on success
  def process_status(eqp, cj, lots, action)
    eqpID = eqp.instance_of?(String) ? oid(eqp) : eqp
    cjID = cj.instance_of?(String) ? oid(cj) : cj
    lots = [lots] if lots.instance_of?(String)
    $log.info "process_status #{eqpID.identifier.inspect}, #{cjID.identifier.inspect}, #{lots}, #{action.inspect}"
    #
    res = @manager.TxProcessStatusRpt(@_user, eqpID, cjID, oids(lots), action)
    return rc(res)
  end

  # returns lotRecipeInfo_struct, used in 1 SiView regression test only
  def eqp_recipename(cj, params={})
    res = @manager.AMD_TxGetRecipeNameInq(@_user, cj)
    return nil if rc(res) != 0
    return res.lotRecipeInfo
  end

  # returns the monitored lots by a given process monitor, used in 1 SiView regression test only
  def monitorlot_groups(lot, params={})
    res = @manager.TxMonitorProdLotsRelationInq(@_user, oid(lot), oid(lot_info(lot).carrier))
    return nil if rc(res) != 0
    groups = res.strMonitorGroups.collect {|g| g.strMonitoredLots.collect {|p| p.lotID.identifier}}
    return groups.count == 1 ? groups.first : groups
  end

  # cancels all relations of a given process monitor, UNUSED
  def monitorlot_cancel_relations(lot, params={})
    res = @manager.TxCancelRelationMonitorProdLotsReq(@_user, oid(lot))
    return nil if rc(res) != 0
    return res.strMonRelatedProdLots.collect {|p| p.productLotID.identifier}
  end

end
