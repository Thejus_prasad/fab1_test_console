=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.
=end


class SiView::MM

  # return array of dynamic routes for pdtype Branch or Rework, currently UNUSED
  def dynamic_routes(pdtype, params={})
    route = params[:route] || '%'
    active = !!params[:active]
    req = @jcscode.pptDynamicRouteListInq_struct.new(route, pdtype, active, any)
    #
    res = @manager.TxDynamicRouteListInq(@_user, req)
    return nil if rc(res) != 0
    return res.strDynamicRouteLists.collect {|e| e.routeID.identifier}.sort
  end

  # return original route of lot or the list of routes connected if connected_routes: true
  def lot_processlist_in_route(lot, params={})
    lotfamily = params[:lotfamily] || ''
    originalroute = params[:originalroute] || ''
    #
    if !@customized || @release < 15
      res = @manager.TxProcessListInRouteInq(@_user, oid(lot), oid(lotfamily), oid(originalroute))
    else
      inparam = @jcscode.pptCS_ProcessListInRouteInqInParm_struct.new(oid(lot), oid(lotfamily), oid(originalroute), any)
      res = @manager.CS_TxProcessListInRouteInq(@_user, inparam)
    end
    return nil if rc(res) != 0
    #
    infos = res.respond_to?(:strExperimentalSplitInfoSeq) ? res.strExperimentalSplitInfoSeq : res.strCS_ExperimentalSplitInfoSeq
    rimethod = infos.first.respond_to?(:strConnectedRouteInfoList) ? :strConnectedRouteInfoList : :strCS_ConnectedRouteInfoList
    if params[:raw]
      return infos
    elsif params[:connected_routes]
      # array of connected routes, TODO: by opNo ?
      return infos.collect {|info|
        info.send(rimethod).collect {|cr| cr.routeID.identifier}
      }.flatten.uniq
    else
      return Hash[infos.collect {|info| [info.operationNumber, info.mainPDID]}]
    end
  end

  # move to branch route, return 0 on success
  def lot_branch(lot, broute, params={})
    lotID = lot.instance_of?(String) ? oid(lot) : lot
    eventTxId = ''
    dynamic = !!params[:dynamic]
    retopNo = params[:return_opNo] || ''  # default: returns to same operation (next op configured in SM!)
    $log.info "lot_branch #{lotID.identifier.inspect}, #{broute.inspect}, return_opNo: #{retopNo.inspect}"
    breq = @jcscode.pptBranchReq_struct.new(lotID, oid(''), '', oid(broute), retopNo, eventTxId, dynamic, any)
    #
    res = @manager.TxBranchReq(@_user, breq, params[:memo] || '')
    return rc(res)
  end

  # return 0 on success
  def lot_branch_cancel(lot, params={})
    lotID = lot.instance_of?(String) ? oid(lot) : lot
    opNo = params[:opNo]
    route = params[:route]
    unless opNo && route
      lopinfo = lots_info_raw(lotID).strLotInfo.first.strLotOperationInfo
      opNo ||= lopinfo.operationNumber
      routeID = route ? oid(route) : lopinfo.routeID
    end
    $log.info "lot_branch_cancel #{lotID.identifier.inspect}"
    #
    res = @manager.TxSubRouteBranchCancelReq(@_user, lotID, routeID, opNo, params[:memo] || '')
    return rc(res)
  end

  # rework or rework with hold release, return 0 on success
  def lot_rework(lot, subroute, params={})
    ropno = params[:return_opNo] || ''
    reason = params[:reason] || 'RWK'
    release_reason = params[:release_reason] || 'ALL'
    event = params[:event] || ''
    dynamic = !!params[:dynamic]
    force = !!params[:force]      # override SM defined max rework count
    # TXIDs: dynamic false / force false: TXTRC068,
    #        dynamic false / force true:  TXTRC086,
    #        dynamic true  / force false: TXEWC017,
    #        dynamic true  / force true:  TXEWC018
    memo = params[:memo] || ''
    liraw = lots_info_raw(lot) || return
    strLotInfo = liraw.strLotInfo.first
    lotID = strLotInfo.strLotBasicInfo.lotID
    $log.info "lot_rework #{lotID.identifier.inspect}, #{subroute.inspect}, #{params.inspect}"
    cinfo = code_list('Rework').find {|e| e.code.identifier == reason}
    ($log.warn '  wrong rework reason code'; return) unless cinfo
    req = @jcscode.pptReworkReq_struct.new(lotID, strLotInfo.strLotOperationInfo.routeID,
      strLotInfo.strLotOperationInfo.operationNumber, oid(subroute), ropno, cinfo.code, event, force, dynamic, any)
    if params[:onhold]
      hreqs = lot_hold_list(lot, raw: true).collect {|h|
          @jcscode.pptHoldReq_struct.new(h.holdType, h.reasonCodeID, h.userID, h.responsibleOperationMark,
            h.responsibleRouteID, h.responsibleOperationNumber, h.relatedLotID, memo, any)
      }
      $log.info '  release holds'
      rcinfo = code_list('LotHoldRelease').find {|e| e.code.identifier == release_reason}
      ($log.warn '  wrong release reason code'; return) unless rcinfo
      res = @manager.TxReworkWithHoldReleaseReq(@_user, req, memo, rcinfo.code, hreqs)
    else
      res = @manager.TxReworkReq(@_user, req, memo)
    end
    return rc(res)
  end

  # return 0 on success
  def lot_rework_cancel(lot, params={})
    reason = params[:reason] || 'ALL'
    memo = params[:memo] || ''
    liraw = lots_info_raw(lot) || return
    strLotInfo = liraw.strLotInfo.first
    lotID = strLotInfo.strLotBasicInfo.lotID
    $log.info "lot_rework_cancel #{lotID.identifier}"
    cinfo = code_list('ReworkCancel').find {|e| e.code.identifier == reason}
    ($log.warn '  wrong cancel reason code'; return) unless cinfo
    #
    res = @manager.TxReworkWholeLotCancelReq(@_user, lotID, strLotInfo.strLotOperationInfo.routeID,
      strLotInfo.strLotOperationInfo.operationNumber, cinfo.code, memo)
    return rc(res)
  end

  # partial rework for a lot w/o hold, return created child lot on success
  def lot_partial_rework(lot, splitwafers, subroute, params={})
    liraw = lots_info_raw(lot, wafers: true) || return
    strLotInfo = liraw.strLotInfo.first
    lotID = strLotInfo.strLotBasicInfo.lotID
    if splitwafers.instance_of?(Array)
      wantedwafers = splitwafers
      n = splitwafers.size
    else
      wantedwafers = nil
      n = splitwafers
    end
    waferIDs = []
    strLotInfo.strLotWaferAttributes.each {|w|
      next if wantedwafers && !wantedwafers.include?(w.slotNumber)
      waferIDs << w.waferID
      break if waferIDs.size == n
    }
    ($log.warn '  not enough wafers found'; return) if waferIDs.size != n
    retOpNo = params[:return_opNo] || strLotInfo.strLotOperationInfo.operationNumber
    reason = params[:reason] || 'RWK'
    release_reason = params[:release_reason] || 'ALL'
    event = params[:event] || ''
    force = !!params[:force]
    dynamic = !!params[:dynamic]
    memo = params[:memo] || ''
    cinfo = code_list('Rework').find {|e| e.code.identifier == reason}
    ($log.warn '  wrong rework reason code'; return) unless cinfo
    req = @jcscode.pptPartialReworkReq_struct.new(lotID, waferIDs.to_java(@jcscode.objectIdentifier_struct),
      oid(subroute), retOpNo, cinfo.code, event, force, dynamic, any)
    $log.info "lot_partial_rework #{lotID.identifier.inspect} (#{waferIDs.size} wafers) #{subroute.inspect} return_opNo: #{retOpNo.inspect}"
    if onhold = params[:onhold]
      if (params[:inheritholds] != false) || params[:release]
        hreqs = lot_hold_list(lotID, raw: true).collect {|h|
            @jcscode.pptHoldReq_struct.new(h.holdType, h.reasonCodeID, h.userID, h.responsibleOperationMark,
              h.responsibleRouteID, h.responsibleOperationNumber, h.relatedLotID, memo, any)
        }
      else
        hreqs = []
      end
      if params[:release]
        $log.info '  release holds'
        rcinfo = code_list('LotHoldRelease').find {|e| e.code.identifier == release_reason}
        ($log.warn '  wrong release reason code'; return) unless rcinfo
        res = @manager.TxPartialReworkWithHoldReleaseReq(@_user, req, memo, rcinfo.code, hreqs)
      else
        $log.info '  inherit holds' if params[:inheritholds] != false
        res = @manager.TxPartialReworkWithoutHoldReleaseReq(@_user, req, memo, hreqs)
      end
    else
      res = @manager.TxPartialReworkReq(@_user, req, memo)
    end
    #
    return nil if rc(res) != 0
    return onhold ? res.childLotID.identifier : res.createdLotID.identifier
  end

  # return 0 on success
  def lot_partial_rework_cancel(lot, child, params={})
    reason = params[:reason] || 'ALL'
    memo = params[:memo] || ''
    lotID = lot.instance_of?(String) ? oid(lot) : lot
    childID = child.instance_of?(String) ? oid(child) : child
    $log.info "lot_partial_rework_cancel #{lotID.identifier.inspect} #{childID.identifier.inspect}"
    cinfo = code_list('ReworkCancel').find {|e| e.code.identifier == reason}
    ($log.warn '  wrong cancel reason code'; return) unless cinfo
    #
    res = @manager.TxReworkPartialWaferLotCancelReq(@_user, lotID, childID, cinfo.code, memo)
    return rc(res)
  end

  # list registered future rework for lot, used in MM_Lot_QTime
  def lot_future_rework_list(lot)
    res = @manager.TxFutureReworkListInq(@_user, oid(lot))
    return nil if rc(res) != 0
    return res.strFutureReworkInfoList
  end

  # return array of WaferReworkInfo struct per wafer or nil, used in MM_Lot_ReworkCounter
  def lot_wafer_rework_count(lot)
    liraw = lots_info_raw(lot)
    strLotInfo = liraw.strLotInfo.first
    lotID = strLotInfo.strLotBasicInfo.lotID
    routeID = strLotInfo.strLotOperationInfo.routeID
    opNo = strLotInfo.strLotOperationInfo.operationNumber
    #
    res = @manager.CS_TxCumulativeReworkCountbyWaferInq(@_user, lotID, routeID, opNo)
    return nil if rc(res) != 0
    return res.strWafersCumulativeReworkInfo
  end

  # register a lot for a PSM (a.k.a. experiment), return 0 on success
  #   for multi split give splitwafers and broute and optionally retOpNo as arrays
  def lot_experiment(lot, splitwafers, broute, splitOpNo, retOpNo, params={})
    li = lot_info(lot)
    splitroute = params[:split_route] || li.route       # route where split is executed, default main
    originalroute = params[:original_route] || li.route # main route
    originalOpNo = params[:original_opNo] || splitOpNo  # opNo of first split on main route
    mrgOpNo = params[:merge] || retOpNo
    memo = params[:memo] || ''
    testmemo = params[:testmemo] || ''
    email = !!params[:email]
    hold = !!params[:hold]
    exe = !!params[:exec]
    dynamic = !!params[:dynamic]
    actiontime = ''
    modify_user = ''
    $log.info "lot_experiment #{lot.inspect}, #{splitwafers}, #{broute.inspect}, #{splitOpNo.inspect}, #{retOpNo.inspect}"
    broutes = broute.instance_of?(Array) ? broute : broute.split
    splitwafers = [splitwafers] if broutes.size == 1
    retOpNos = retOpNo.instance_of?(Array)? retOpNo : [retOpNo]
    mrgOpNos = mrgOpNo.instance_of?(Array)? mrgOpNo : [mrgOpNo]
    # build experiment details structs
    lots = lot_family(lot)
    details = broutes.collect.with_index {|broute, i|
      sw = splitwafers[i]
      sw = sw.split if sw.instance_of?(String)
      wafers = lots.collect {|l|
        lot_info(l, wafers: true, select: sw).wafers.collect {|w| w.wafer}
      }.flatten
      r = retOpNos[i] || retOpNos[0]
      m = mrgOpNos[i] || mrgOpNos[0]
      $log.info "  on #{broute} from #{splitroute} #{splitOpNo} .. #{r}, (#{wafers.size} wafers)"
      @jcscode.pptExperimentalLotDetailInfo_struct.new(oid(broute), r, m, oids(wafers), memo, dynamic, exe, actiontime, any)
    }
    #
    if params[:customized] || @f7
      owners = params[:owners] || @user
      owners = [owners] unless owners.instance_of?(Array)
      ohash = owners.collect.with_index {|o, i|
        @jcscode.pptHashedInfo_struct.new("ownerID#{i}", o, any)
      }.to_java(@jcscode.pptHashedInfo_struct)
      inparam = @jcscode.pptCS_ExperimentalLotUpdateReqInParm_struct.new(
        oid(li.family), oid(splitroute), splitOpNo, oid(originalroute), originalOpNo, email, hold, ohash, testmemo,
        exe, actiontime, oid(modify_user), details.to_java(@jcscode.pptExperimentalLotDetailInfo_struct), any)
      res = @manager.CS_TxExperimentalLotUpdateReq(@_user, inparam, memo)
    else
      res = @manager.TxExperimentalLotUpdateReq(@_user, oid(li.family), oid(splitroute), splitOpNo,
        oid(originalroute), originalOpNo, email, hold, testmemo, exe, actiontime, oid(modify_user), details, memo)
    end
    return rc(res)
  end

  # return list of experiments
  def lot_experiment_list(lot)
    lotID = lot.instance_of?(String) ? oid(lot) : lot
    res = @manager.TxExperimentalLotListInq(@_user, lotID)
    return nil if rc(res) != 0
    return res.strExperimentalLotList
  end

  # return experiment info (incl details), used in MM_Lot_PSM only
  def lot_experiment_info(lot, splitOpNo, params={})
    li = lot_info(lot)
    splitroute = params[:split_route] || li.route # main route
    originalroute = params[:original_route] || li.route # main route
    originalOpNo = params[:original_opNo] || splitOpNo  # opNo of first split on main route
    #
    if @customized
      inparam = @jcscode.pptCS_ExperimentalLotInfoInqInParm_struct.new(
        oid(li.family), oid(splitroute), splitOpNo, oid(originalroute), originalOpNo, any)
      res = @manager.CS_TxExperimentalLotInfoInq(@_user, inparam)
      ret = res.strCS_ExperimentalLotInfo
    else
      res = @manager.TxExperimentalLotInfoInq(
        @_user, oid(li.family), oid(splitroute), splitOpNo, oid(originalroute), originalOpNo)
      ret = res.strExperimentalLotInfo
    end
    return nil if rc(res) != 0
    return ret
  end

  # delete all experiments (PSM, ERF), return true on success
  # TODO: separate into lot_experiments_cleanup?
  def lot_experiment_delete(lot, params={})
    lotID = lot.instance_of?(String) ? oid(lot) : lot
    exps = lot_experiment_list(lotID)
    return true if exps.empty? && params[:silent]
    $log.info "lot_experiment_delete #{lotID.identifier}, #{params}"
    ($log.info "  lot #{lotID.identifier} has no experiments to delete"; return true) if exps.empty?
    ret = true
    exps.each {|e|
      $log.info "  deleting experiment #{e.splitRouteID.identifier} #{e.splitOperationNumber}"
      res = @manager.TxExperimentalLotDeleteReq(@_user, e.lotFamilyID, e.splitRouteID,
        e.splitOperationNumber, e.originalRouteID, e.originalOperationNumber, params[:memo] || '')
      ret &= (rc(res) == 0)
    }
    return ret
  end

  # reset PSM execution flag, return 0 on success
  def lot_experiment_reset(lot, splitOpNo, params={})
    $log.info "lot_experiment_reset #{lot.inspect}, #{splitOpNo.inspect}, #{params}"
    li = lot_info(lot)
    splitroute = params[:split_route] || li.route
    originalroute = params[:original_route] || splitroute
    originalOpNo = params[:original_opNo] || splitOpNo
    memo = params[:memo] || ''
    #
    res = @manager.AMD_TxExperimentalLotResetCompFlagReq(
      @_user, oid(li.family), oid(splitroute), splitOpNo, oid(originalroute), originalOpNo, false, [], memo)
    return rc(res)
  end

  # new in C7.12, canceling a whole lot PSM, used in MM_Lot_PSM only
  def lot_experiment_branchcancel(lot, params={})
    $log.info "lot_experiment_branchcancel #{lot.inspect}, #{params}"
    li = lot_info(lot)
    route = params[:route] || li.route
    opNo = params[:opNo] || li.opNo
    reset = !!params[:reset]
    memo = params[:memo] || ''
    inparm = @jcscode.pptCS_ExperimentalLotBranchCancelReqInParm_struct.new(oid(lot), oid(route), opNo, reset, memo, any)
    #
    res = @manager.CS_TxExperimentalLotBranchCancelReq(@_user, inparm)
    return rc(res)
  end

  # check if an operation is crossing any PSMs, return list of PSMs crossed (MM_Lot_PSM only)
  def lot_intermediate_psm(lot, route, opNo, params={})
    inparam = @jcscode.pptCS_IntermediatePSMCheckInqInParm_struct.new(oid(lot), oid(route), opNo, any)
    res = @manager.CS_TxIntermediatePSMCheckInq(@_user, inparam)
    return nil if rc(res) != 0
    return res.strExperimentalLotInfoList
  end

  # return strRouteInfo at opNo or nil, used in MM_LotUpcomingInfoForEqp
  def route_nested_operations(route, opNo, params={})
    level = params[:level] || 2
    srteinfo = params[:subroute_info] == true
    inparm = @jcscode.pptCS_RouteOperationNestListInqInParam_struct.new(oid(route), opNo, level, srteinfo, any)
    #
    res = @manager.CS_TxRouteOperationNestListInq__C74(@_user, inparm)
    return nil if rc(res) != 0
    return res.strRouteInfo
  end

end
