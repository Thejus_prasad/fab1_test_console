=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.
=end


class SiView::MM

  # return connected routes for the lot at its current operation, currently UNUSED
  # route types are Rework, Recycle, Production, Engineering, Dummy, Branch, "Process Monitor" and "Equipment Monitor"
  def lot_connected_routes(lot, type, params={})
    res = @manager.TxConnectedRouteListInq(@_user, oid(lot), oid(params[:carrier] || ''), type)
    return nil if rc(res) != 0
    return res.strConnectedRouteList.collect {|e| e.routeID.identifier}.sort
  end

  # get available route list, UNUSED
  #
  # route_type = Backup, Branch, Dummy, Engineering, Equipment Monitor, Process Monitor, Production, Rework, Recycle
  #
  # return array of routes
  def route_index_list(route_type, params={})
    rtype = params[:rtype] || ''
    lot = params[:lot] || ''
    active = !!params[:active]
    #
    res = @manager.TxRouteIndexListInq(@_user, rtype, oid(lot), route_type, active)
    return nil if rc(res) != 0
    return res if params[:raw]
    return res.strRouteIndexList.collect {|r| r.routeID.identifier}
  end

  # check if products's route is different from lot's route, used for non-pro bankout warning message
  #
  # return true/false (isDifferentFlag Y or N) or nil on error, UNUSED
  def lot_cmp_cur_route(lot, params={})
    res = @manager.CS_TxCmpCurRouteInq(@_user, oid(lot))
    return nil if rc(res) != 0
    return res if params[:raw]
    return res.isDifferentFlag == 'Y'
  end


  # from mm_lot_rwk

  # register a future rework request, return 0 on success, UNUSED
  def lot_future_rework(lot, rework_route, rework_opno, return_opno, params={})
    $log.info "lot_future_rework #{lot.inspect}, #{rework_route.inspect}"
    memo = params[:memo] || ''
    reason = params[:reason] || 'RWK'
    details = @jcscode.pptFutureReworkDetailInfo_struct.new("LotID.RouteID.TriggerOpeNo.TargetOpeNo",
      oid(rework_route), return_opno, oid(reason), any)
    li = lot_info(lot)
    #
    res = @manager.TxFutureReworkReq(@_user, oid(lot), oid(li.route), rework_opno, details, memo)
    return rc(res)
  end

  # cancel registered future reworks (pass reason nil for all), return 0 on success, UNUSED
  def lot_future_rework_cancel(lot, rework_opno, reason, params={})
    memo = params[:memo] || ''
    $log.info "lot_future_rework_cancel #{lot.inspect}, #{rework_opno.inspect}, #{reason.inspect}"
    reworks = lot_future_rework_list(lot) || return
    rw_entry = reworks.find {|rw| rw.operationNumber == rework_opno}
    ($log.warn 'no entry found'; return) unless rw_entry
    details = rw_entry.strFutureReworkDetailInfoSeq.select {|d| d.reasonCodeID.identifier == reason || reason.nil?}
    #
    res = @manager.TxFutureReworkCancelReq(@_user, oid(lot), rw_entry.routeID, rework_opno, details, memo)
    return rc(res)
  end

end
