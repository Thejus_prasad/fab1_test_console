=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.
=end


class SiView::MM

  # return 0 on success else rc
  def logon_check(params={})
    subsystem = params[:subsystem] || ''  # e.g. MM
    category = params[:category] || ''    # e.g. Report
    productRequest = !!params[:productRequest]
    recipeRequest = !!params[:recipeRequest]
    _u = params[:user] ? user_struct(params[:user], (params[:password] || '')) : @_user
    $log.info "logon_check for user #{_u.userID.identifier.inspect} (#{@env})"
    #
    res = @manager.TxLogOnCheckReq(_u, subsystem, category, productRequest, recipeRequest)
    return rc(res)
  end

  # e.g. subsystem 'MM', category 'Report', return array of privileges, used in 1 SiView regression test
  def user_privileges(user, subsystem, category, params={})
    res = @manager.CS_TxUserPrivilegesInq(@_user, oid(user), subsystem, category)
    return nil if rc(res) != 0
    Hash[res.subSystemFuncLists.collect {|ss|
      [ss.subSystemID, ss.strFuncIDs.collect {|e| [e.functionID, e.permission]}.sort]
    }]
  end

  # return pptUserDescInqResult_struct if user exists else nil
  def user_desc(user)
    res = @manager.TxUserDescInq(@_user, oid(user))
    return nil if rc(res) != 0
    return res
  end

  # return [strSvcEnvVariable, strPptEnvVariable] or the value of the variable passed as parameter
  def environment_variable(params={})
    res = @manager.TxEnvironmentVariableInfoInq(@_user)
    return nil if rc(res) != 0
    return res if params[:raw]
    svc = Hash[res.strSvcEnvVariable.collect {|e| [e.envName, e.envValue]}.sort]
    ppt = Hash[res.strPptEnvVariable.collect {|e| [e.envName, e.envValue]}.sort]
    var = params[:var]
    return var ? (svc[var] || ppt[var]) : [svc, ppt]
  end

  # category is e.g. BankHold, LotHoldRelease, Rework, WaferScrap ..., return strCodeInfoList_struct
  def code_list(category)
    if cached = @reasoncodes[category]
      return cached
    else
      res = @manager.TxCodeListInq(@_user, oid(category))
      return nil if rc(res) != 0
      @reasoncodes[category] = res.strCodeInfoList
      return res.strCodeInfoList
    end
  end

  # lottype is one of Production, Engineering, etc., return array of sublot types or nil on error
  def sublot_types(lottype, params={})
    res = @manager.TxSubLotTypeIDListInq(@_user, lottype)
    return nil if rc(res) != 0
    return res if params[:raw]
    res.strLotTypes[0].strSubLotTypes.collect {|s| s.subLotType}.sort
  end

  def machine_recipes(params={})
    res = @manager.TxMachineRecipeIDListInq(@_user)
    return nil if rc(res) != 0
    return res if params[:raw]
    res.objectIdentifiers.collect {|s| s.identifier}.sort
  end

  # register or update durables of a category, classnames are Cassette, Fixture, ReticlePod, return 0 on success
  #   for classnames Fixture and ReticlePod pass {contents: '', capacity: 10}
  def durable_register(classname, names, category, params={})
    update = !!params[:update]
    description = params[:description] || ''
    usagecheck = !!params[:usagecheck]
    run_max = (params[:run_max] || 0).to_s
    use_max = params[:use_max] || 0
    pm_max = params[:pm_max] || 0   # 1 hour between PMs
    capacity = params[:capacity] || 25
    nominalsize = params[:nominalsize] || 12
    contents = params[:contents] #|| 'Wafer'
    if contents.nil?
      contents = 'Wafer' if classname == 'Cassette'
      contents = '' if classname == 'Fixture'
      contents = '' if classname == 'ReticlePod'
    end
    instance = params[:instance] || ''
    memo = params[:memo] || ''
    #
    names = [names] if names.instance_of?(String)
    $log.info "durable_register #{classname}, #{names}, #{category.inspect}, #{params}"
    #
    # if udata is defined fill it out
    udd = {}
    udd = user_defined_data('DR06') if classname == 'Cassette'
    udd = user_defined_data('DR08') if classname == 'ReticlePod'
    udata = udd.collect {|n, t|
      value = 'Test' if t == 'String'
      value = '12345' if t == 'Integer'
      @jcscode.pptUserData_struct.new(n, t, value, @user, any)
    }.to_java(@jcscode.pptUserData_struct)
    #
    attrs = names.collect {|name|
      @jcscode.pptDurableAttribute__090_struct.new(oid(name), description, category, usagecheck,
        run_max, use_max, pm_max, capacity, nominalsize, contents, instance, udata, any)
    }.to_java(@jcscode.pptDurableAttribute__090_struct)
    dreg = @jcscode.pptDurableRegistInfo__090_struct.new(update, classname, attrs, any)
    #
    res = @manager.TxDurableRegistReq__090(@_user, dreg, memo)
    return rc(res)
  end

  # delete durables created by MM, classnames are Cassette, Fixture, ReticlePod, return 0 on success
  def durable_delete(classname, names, params={})
    memo = params[:memo] || ''
    names = [names] if names.instance_of?(String)
    $log.info "durable_delete #{classname.inspect}, #{names}, #{params}"
    ddel = @jcscode.pptDurableDeleteInfo_struct.new(classname, oids(names), any)
    #
    res = @manager.TxDurableDeleteReq(@_user, ddel, memo)
    return rc(res)
  end

  # Inquire PDs (level: Operation | Module | Main), return strProcessDefinitionIndexLists or array of PD names
  # has a bug: MSR719043 start and end bank of Main PDs are object references only and not usable
  def module_list(params={})
    pdid = params[:id] || ''
    pdtype = params[:type] || ''
    level = params[:level] || ''
    version = params[:version] || ''
    inparm = @jcscode.pptModuleProcessDefinitionIDListInq_struct.new(pdid, pdtype, level, version, any)
    #
    res = @manager.TxModuleProcessDefinitionIDListInq(@_user, inparm)
    return nil if rc(res) != 0
    return res.strProcessDefinitionIndexLists.map {|pd| pd.processDefinitionID.identifier}
  end

  def module_rework_limits(route, modulenum)
    routeID = route.instance_of?(String) ? oid(route) : route
    #
    res = @manager.CS_TxModuleReworkLimitInq(@_user, routeID, modulenum)
    return nil if rc(res) != 0
    return res
  end

  # type: B..all banks, N..non prod banks, P..production banks, return strBankAttributes on success or nil
  def bank_list(params={})
    res = @manager.TxBankListInq(@_user, params[:type] || 'B')
    return nil if rc(res) != 0
    return res.strBankAttributes
  end

  def route_operations(route, params={})
    op = params[:op] || ''
    opNo = params[:opNo] || ''
    pdtype = params[:pdtype] || ''
    count = params[:count] || 0
    #
    if @release >= 15
      res = @manager.TxRouteOperationListInq__160(@_user, oid(route), oid(op), opNo, pdtype, count)
    else
      res = @manager.TxRouteOperationListInq(@_user, oid(route), oid(op), opNo, count)
    end
    return nil if rc(res) != 0
    return res.strOperationNameAttributes
  end

  # delete any active post processing of lot/cassette ('' is allowed), return 0 on success
  def pp_force_delete(carrier, lot, params={})
    carrierID = carrier.instance_of?(String) ? oid(carrier) : carrier
    lotID = lot.instance_of?(String) ? oid(lot) : lot
    inparm = @jcscode.pptLotCassettePostProcessForceDeleteReqInParm_struct.new(carrierID, lotID, any)
    #
    res = @manager.TxLotCassettePostProcessForceDeleteReq(@_user, inparm, params[:memo] || '')
    return rc(res)
  end

  # Generic inquiry of SiView objects,
  # examples of classnames are PosTechnology, PosLogicalRecipe, PosProductSpecification
  #
  # additional search conditions can be given, e.g. ProductCategory
  def object_list(classname, object, params={})
    conditions = params[:conditions] || {}
    search_conds = conditions.each_pair.collect {|k, v|
      @jcscode.pptAdditionalSearchCondition_struct.new(classname, k, v, any)
    }.to_java(@jcscode.pptAdditionalSearchCondition_struct)
    inparm = @jcscode.pptObjectIDListInqInParm_struct.new(classname, oid(object), search_conds, any)
    #
    res = @manager.TxObjectIDListInq(@_user, inparm)
    return nil if rc(res) != 0
    return res.strObjectIDListSeq.collect {|o| o.objectID.identifier}
  end

end
