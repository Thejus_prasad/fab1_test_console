=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: sfrieske, 2018-04-18

History:
=end

require 'siview'


module EI

  # client connecting to an EI's CS_TCSServiceManager like SiView
  class TCSClient < SiView::Client
    attr_accessor :_oideqp

    # general settings are taken from mm.<env> instance, 
    #   additionally either eqp: <eqp>, ior: <string> or iorfile: <url> must be passed
    def initialize(env, params)
      super(env, params.merge(instance: "mm.#{env}"))
    end

    def connect
      eqp = @_props[:eqp]
      if @_props[:iorfile]  # overwrite the MM server filename with eqp file name
        @_props[:iorfile] = "#{File.dirname(@_props[:iorfile])}/#{eqp}.IOR"
        $log.info "  using iorfile #{@_props[:iorfile].inspect}"
      end
      super
      $log.info '  creating CS_TCSServiceManager'
      @jcscode = com.amd.jcs.siview.code
      @manager = @jcscode.CS_TCSServiceManagerHelper.narrow(@orb.string_to_object(@iorparser.ior))
      @_user = user_struct(@user, @password, @_props)
      @_oideqp = oid(eqp || '')
      $log.info self.inspect
    end

    def signaltower_off(params={})
      res = @manager.TxSignalTowerOffReq(@_user, @_oideqp, params[:memo] || '')
      return rc(res)
    end

    def detail_info(params={})
      res = @manager.TxEqpDetailInfoInq(@_user, @_oideqp)
      return rc(res)
    end

    def cjpj_info(cj, pj, params={})
      parm = @jcscode.pptCJPJInfoInqInParm_struct.new(@_oideqp, oid(cj), pj, !!params[:recipeparms], any)
      res = @manager.TxCJPJInfoInq(@_user, parm)
      return rc(res)
    end

  end

end
