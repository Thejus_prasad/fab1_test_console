=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.
=end


class SiView::MM

  # used in slr and npw_reserve, return array [cassetteID, pptLotInCassette_struct] or nil on error
  def _ppt_lotsincassette(carrier, lots, params)
    monitors = params[:monitor] # only 1 monitor currently allowed by SiView
    monitors = [monitors] if monitors.instance_of?(String)
    lisraw = lots_info_raw(lot_list_incassette(carrier), wafers: true) || return
    res = lisraw.strLotInfo.collect {|li|
      lotID = li.strLotBasicInfo.lotID
      startrecipe = @jcscode.pptStartRecipe_struct.new(oid(''), oid(''), '', [].to_java(@jcscode.pptStartReticle_struct),
        [].to_java(@jcscode.pptStartFixture_struct), false, [].to_java(@jcscode.pptDCDef_struct), any)
      #
      wafers = []
      lisraw.strWaferMapInCassetteInfo.each {|w|
        next unless (lotID.identifier == w.lotID.identifier) ##|| params[:wrong_wafermap]
        wafers << @jcscode.pptLotWafer_struct.new(w.waferID, w.slotNumber, false, false, false, '',
          [].to_java(@jcscode.pptStartRecipeParameter_struct), any)
      }
      #
      o = li.strLotOperationInfo
      startop = @jcscode.pptStartOperationInfo_struct.new(o.routeID, o.operationID, o.operationNumber, 0, o.siInfo)
      startflag = lots.member?(lotID.identifier)  # lot selection
      monitorflag = !monitors.nil? && monitors.include?(lotID.identifier)
      $log.info "  carrier #{carrier}: #{lotID.identifier}, start: true#{' (monitor)' if monitorflag}" if startflag
      $log.debug {"  carrier #{carrier}: #{lotID.identifier}, start: false#{' (monitor)' if monitorflag}"} unless startflag
      # $log.info '  volontarily using wrong wafermap!' if params[:wrong_wafermap]
      @jcscode.pptLotInCassette_struct.new(startflag, monitorflag, lotID, '', '', startrecipe, '',
        wafers.to_java(@jcscode.pptLotWafer_struct), oid(''), startop, any)
    }.to_java(@jcscode.pptLotInCassette_struct)
    #
    return [lisraw.strLotInfo.first.strLotLocationInfo.cassetteID, res]
  end

  # return result of TxLotsInfoForStartReservationInq, portgroup, ib in an array; used for slr
  def lots_info_slr(eqp, lots, params)
    lots = [lots] if lots.instance_of?(String)
    carriers = params[:carrier] || lots_info_raw(lots).strLotInfo.collect {|e| e.strLotLocationInfo.cassetteID.identifier}.uniq
    carriers = [carriers] if carriers.instance_of?(String)
    eqpiraw = params[:eqpinfo] || eqp_info_raw(eqp, ib: params[:ib]) || return
    if port = params[:loadport] || params[:port]
      # if port is given find portgroup
      lp = eqpiraw.equipmentPortInfo.strEqpPortStatus.find {|p| p.portID.identifier == port}
    else  # TODO: use case ??
      category = params[:carrier_category]
      eqpiraw.equipmentPortInfo.strEqpPortStatus.each {|p|
        # next if pg && pg != p.portGroup
        next if p.portUsage != 'INPUT_OUTPUT'
        next if p.loadSequenceNumber > 1  # ib: 0, fb: first
        next unless p.loadedCassetteID.identifier.empty?
        next unless p.dispatchLoadCassetteID.identifier.empty?
        next unless p.dispatchUnloadCassetteID.identifier.empty?
        next unless p.loadResrvedCassetteID.identifier.empty?
        next if p.portState == 'Down' && !p.operationModeID.identifier.start_with?('Off-Line')
        next if category && !p.cassetteCategoryCapability.empty? && !p.cassetteCategoryCapability.member?(category)
        lp = p
        break
      }
    end
    ($log.warn "  #{eqp}: no suitable equipment ports available"; return) unless lp
    if unloadport = params[:unloadport]
      up = eqpiraw.equipmentPortInfo.strEqpPortStatus.find {|p| p.portID.identifier == unloadport}
    else
      up = lp
    end
    purpose = params[:purpose] || 'Process Lot'
    ib = eqpiraw.respond_to?(:equipmentBRInfoForInternalBuffer)
    #
    # prepare pptStartCassette_struct with original oids (e.g. for VAMOS)
    cycleport = params[:cycle_port]
    if cycleport
      # load each lot from another port, take all ports
      loadports = eqpiraw.equipmentPortInfo.strEqpPortStatus
      loadports = loadports.select {|p| p.portGroup == lp.portGroup} unless ib
    end
    startCass = carriers.each_with_index.collect {|carrier, index|
      if cycleport
        lp = up = loadports[index % loadports.count]
      end
      carrierID, lotsincass = _ppt_lotsincassette(carrier, lots, params)
      # need to override load purpose for proc monitors
      purpose = 'Process Monitor Lot' if params[:monitor] && lotsincass.find {|l| l.monitorLotFlag}
      $log.info "  loadport #{lp.portID.identifier}, unloadport #{up.portID.identifier}, #{purpose.inspect}"
      @jcscode.pptStartCassette_struct.new(lp.loadSequenceNumber, carrierID, purpose, lp.portID, up.portID, lotsincass, any)
    }
    #
    if ib
      res = @manager.TxLotsInfoForStartReservationForInternalBufferInq(@_user, eqpiraw.equipmentID, startCass)
    else
      res = @manager.TxLotsInfoForStartReservationInq(@_user, eqpiraw.equipmentID, startCass)
    end
    return nil if rc(res) != 0
    # for special tests, currently not used
    # if wrongop = params[:set_wrong_op]
    #   $log.warn {"  setting wrong operationID: #{wrongop.inspect}"}
    #   res.strStartCassette.each {|c|
    #     c.strLotInCassette.each {|l| l.strStartOperationInfo.operationID.identifier = wrongop}
    #   }
    # end
    # override the machine recipe (same machineRecipeID for all lots)
    mrecipe = params[:mrecipe]
    if mrecipe && (matchdata = mrecipe.match(/(.*)\.(.*)\.(.*)/))
        $log.info "  setting mrecipe: #{mrecipe.inspect}"
        res.strStartCassette.each {|c|
          c.strLotInCassette.each {|l|
            l.strStartRecipe.machineRecipeID = oid(mrecipe)
            l.strStartRecipe.physicalRecipeID = matchdata[1]  # precipe
          }
        }
    end
    # optional target_chambers: {lot1=>[chambers], ..}
    if tgtchambers = params[:target_chambers]
      # $log.info "  target_chambers: #{tgtchambers}"
      chamberIDs = eqpiraw.equipmentChamberInfo.strEqpChamberStatus.collect {|e| e.chamberID}
      res.strStartCassette.each {|c|
        c.strLotInCassette.each {|l|
          chs = tgtchambers[l.lotID.identifier]
          $log.info "  tgt_chambers[#{l.lotID.identifier.inspect}]: #{chs.inspect}"
          chids = chamberIDs.select {|e| chs.member?(e.identifier)}.to_java(@jcscode.objectIdentifier_struct)
          l.siInfo = insert_si(@jcscode.pptCS_TargetChambers_siInfo_struct.new(chids, any))
        }
      }
    end
    #
    return res, lp.portGroup, ib, eqpiraw
  end

  # StartLotsReservation for lot(s), loadport and unloadport can be detected automatically; return cj on success
  def slr(eqp, params)
    memo = params[:memo] || ''
    $log.info "slr #{eqp.inspect}"  #, #{params}"
    lislr, pg, ib, eqpiraw = lots_info_slr(eqp, params[:lot], params) || return
    retries = params[:slr_retries] || 2
    res = nil
    retries.times {|i|
      sleep 3 if i > 0
      if ib
        res = @manager.TxStartLotsReservationForInternalBufferReq(@_user, eqpiraw.equipmentID, oid(''), lislr.strStartCassette, memo)
      else
        res = @manager.TxStartLotsReservationReq(@_user, eqpiraw.equipmentID, pg, oid(''), lislr.strStartCassette, memo)
      end
      break unless [-1].member?(rc(res))  # error code for retries needed!
    }
    return nil if rc(res) != 0
    $log.info "  cj='#{res.controlJobID.identifier}'"
    return res.controlJobID.identifier
  end

  # StartLotsReservation for TakeOutIn for lot(s); return cj on success
  def slr_takeoutin(eqp, params)
    memo = params[:memo] || ''
    $log.info "slr_takeoutin #{eqp.inspect}, #{params}"
    lislr, pg, ib, eqpiraw = lots_info_slr(eqp, params[:lot], params) || return
    retries = params[:slr_retries] || 2
    res = nil
    inparm = @jcscode.pptStartLotsReservationForTakeOutInReqInParm_struct.new(
      eqpiraw.equipmentID, pg, oid(''), lislr.strStartCassette, any)
    retries.times {|i|
      sleep 3 if i > 0
      #
      res = @manager.TxStartLotsReservationForTakeOutInReq(@_user, inparm, memo)
      break if rc(res) == 0
    }
    return nil if rc(res) != 0
    $log.info "  cj='#{res.controlJobID.identifier}'"
    return res.controlJobID.identifier
  end

  # cancel a StartLotsReservation, return 0 on success
  def slr_cancel(eqp, cj, params={})
    eqpID = eqp.instance_of?(String) ? oid(eqp) : eqp
    cjID = cj.instance_of?(String) ? oid(cj) : cj
    memo = params[:memo] || ''
    $log.info "slr_cancel #{eqpID.identifier.inspect}, #{cjID.identifier.inspect}"
    ib = params.has_key?(:ib) ? params[:ib] : eqp_info_raw(eqpID).respond_to?(:equipmentBRInfoForInternalBuffer)
    if ib
      res = @manager.TxStartLotsReservationCancelForInternalBufferReq(@_user, eqpID, cjID, memo)
    else
      res = @manager.TxStartLotsReservationCancelReq(@_user, eqpID, cjID, memo)
    end
    return rc(res)
  end

  # NPW reservation for carrier(s), port can be nil, lot is optional; return 0 on success
  def npw_reserve(eqp, port, carriers, params={})
    carriers = [carriers] if carriers.instance_of?(String)
    lots = params[:lot] || carriers.collect {|c| lot_list_incassette(c)}.flatten
    lots = [lots] if lots.instance_of?(String)  # optionally reserve only 1 lot (used by WBTQ)
    eqpiraw = params[:eqpinfo] || eqp_info_raw(eqp, ib: params[:ib]) || return
    if port
      # if port is given find portgroup
      lp = eqpiraw.equipmentPortInfo.strEqpPortStatus.find {|p| p.portID.identifier == port}
    else
      # if portgroup is given find port or any free port
      # category = params[:carrier_category]
      eqpiraw.equipmentPortInfo.strEqpPortStatus.each {|p|
        # next if p.portUsage != 'INPUT_OUTPUT'
        # next if p.loadSequenceNumber > 1  # ib: 0, fb: first
        next unless p.loadedCassetteID.identifier.empty?
        next unless p.dispatchLoadCassetteID.identifier.empty?
        next unless p.dispatchUnloadCassetteID.identifier.empty?
        next unless p.loadResrvedCassetteID.identifier.empty?
        next if p.portState == 'Down' && !p.operationModeID.identifier.start_with?('Off-Line')
        # next if category && !p.cassetteCategoryCapability.empty? && !p.cassetteCategoryCapability.member?(category)
        lp = p
        break
      }
    end
    ($log.warn "  #{eqp}: no suitable equipment ports available"; return) unless lp
    if unloadport = params[:unloadport]
      up = eqpiraw.equipmentPortInfo.strEqpPortStatus.find {|p| p.portID.identifier == unloadport}
    else
      up = lp
    end
    purpose = params[:purpose] || 'Other'
    loadseq = params[:loadseq] || lp.loadSequenceNumber
    cj = params[:cj] || ''
    memo = params[:memo] || ''
    $log.info "npw_reserve #{eqpiraw.equipmentID.identifier.inspect}, #{lp.portID.identifier.inspect}, #{carriers}, lots: #{lots}, purpose: #{purpose.inspect}"
    #
    startcas = carriers.collect {|carrier|
      if lots.empty?
        carrierID, lotsincass = oid(carrier), [].to_java(@jcscode.pptLotInCassette_struct)
      else
        carrierID, lotsincass = _ppt_lotsincassette(carrier, lots, params)
      end
      @jcscode.pptStartCassette_struct.new(loadseq, carrierID, purpose, lp.portID, up.portID, lotsincass, any)
    }
    ib = eqpiraw.respond_to?(:equipmentBRInfoForInternalBuffer)
    if ib
      res = @manager.TxArrivalCarrierNotificationForInternalBufferReq(@_user, eqpiraw.equipmentID, lp.portGroup, oid(cj), startcas, memo)
    else
      res = @manager.TxArrivalCarrierNotificationReq(@_user, eqpiraw.equipmentID, lp.portGroup, oid(cj), startcas, memo)
    end
    return rc(res)
  end

  # cancel a NPW reservation for carrier(s), port is detected automatically; return 0 on success
  def npw_reserve_cancel(eqp, carriers, params={})
    carriers = [carriers] if carriers.instance_of?(String)
    notify = params[:notify] != false
    memo = params[:memo] || ''
    #
    if eqp == '' && carriers
      $log.info "npw_reserve_cancel #{eqp.inspect}, #{carriers.inspect}"
      npw = carriers.each_with_index.collect {|c, i|
        @jcscode.pptNPWXferCassette_struct.new(i + 1, oid(c), '', oid(''), oid(''), any)
      }
      res = @manager.TxArrivalCarrierCancelReq__090(@_user, oid(''), '', npw, false, memo)
    else
      eqpiraw = params[:eqpinfo] ||eqp_info_raw(eqp, ib: params[:ib]) || return
      $log.info "npw_reserve_cancel #{eqpiraw.equipmentID.identifier.inspect}, #{carriers.inspect}"
      ib = params[:ib] || eqpiraw.respond_to?(:equipmentBRInfoForInternalBuffer)
      if ib
        p = eqpiraw.equipmentPortInfo.strEqpPortStatus.first   # take first port
        npw = []
        eqpiraw.equipmentInternalBufferInfo.each {|b|
          b.strShelfInBuffer.each {|s|
            next unless carriers && carriers.member?(s.reservedCarrierID.identifier)
            npw << @jcscode.pptNPWXferCassette_struct.new(
              p.loadSequenceNumber, s.reservedCarrierID, p.loadPurposeType, p.portID, p.portID, any)
            break if npw.size == carriers.size
          }
          break if npw.size == carriers.size
        }
        if npw.empty?
          $log.info '  no npw found' unless params[:silent]
          return 0
        end
        res = @manager.TxArrivalCarrierCancelForInternalBufferReq__090(@_user, eqpiraw.equipmentID, p.portGroup, npw, notify, memo)
      else
        pg = nil
        npw = []
        eqpiraw.equipmentPortInfo.strEqpPortStatus.each {|p|
          ($log.warn "  #{c} has a different port group, skip"; next) if pg && pg != p.portGroup
          next unless carriers && carriers.member?(p.dispatchLoadCassetteID.identifier)
          pg ||= p.portGroup
          npw << @jcscode.pptNPWXferCassette_struct.new(
            p.loadSequenceNumber, p.dispatchLoadCassetteID, p.loadPurposeType, p.portID, p.portID, any)
          break if npw.size == carriers.size
        }
        if npw.empty?
          $log.info '  no npw found' unless params[:silent]
          return 0
        end
        res = @manager.TxArrivalCarrierCancelReq__090(@_user, eqpiraw.equipmentID, pg, npw, notify, memo)
      end
    end
    return rc(res)
  end

end
