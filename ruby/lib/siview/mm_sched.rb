=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.
=end

class SiView::MM

  # return a list of released lots (waiting for STB) for a layer (used in JCAP_AutoFOUPCleaning and delete_lot_family)
  def released_lots(layer)
    res = @manager.TxReleasedLotListInq(@_user, oid(layer))
    return nil if rc(res) != 0
    return res.strProdReqListAttributes
  end

  # return strProdReqInq struct for a scheduled lot
  def product_request(lot)
    lotID = lot.instance_of?(String) ? oid(lot) : lot
    res = @manager.TxProductRequestInq(@_user, lotID)
    return nil if rc(res) != 0
    return res.strProdReqInq
  end

  # return a list of new lotIDs following the customer lot ID schema defined in the UDATA fields
  def generate_new_lotids(customer, params={})
    n = params[:n] || 1   # number of lot IDs to return
    starttime = params[:starttime] || ''
    $log.info "generate_new_lotids #{customer.inspect}, n: #{n.inspect}, starttime: #{starttime.inspect}"
    #
    if @f7 || params[:customized]  # Fab7
      sublottype = params[:sublottype] || ''
      res = @manager.CS_TxGenerateNewLotIdsReq__R15(@_user, customer, sublottype, starttime, n)
    else
      res = @manager.CS_TxGenerateNewLotIdsReq(@_user, customer, starttime, n)
    end
    return nil if rc(res) != 0
    return res if params[:raw]
    return res.lotIDSeq.to_a
  end

  # schedule a new lot for the route and product, return the scheduled lot's name on success
  def new_lot_release(params={})
    product = params[:product]
    route = params[:route]
    lottype = params[:lottype]
    sublottype = params[:sublottype]
    customer = params[:customer]
    template = params[:template]
    # parameters for Fab7
    tktype = params[:tktype] || (params[:tkinfo] && params[:tkinfo].tktype)
    subcons = params[:subcons] || params[:tkinfo]
    qualitytag = params[:qualitytag]
    _lot_info = nil
    #
    if template
      tliraw = lots_info_raw(template)
      tli = _extract_lots_info(tliraw).first
      product ||= tli.product
      route ||= tli.route
      lottype ||= tli.lottype
      sublottype ||= tli.sublottype
      customer ||= tli.customer
      if @f7
        # tktype ||= tli.extended.tktype  # broken
        # subcons ||= tli.extended        # broken
        _lot_info = extract_si(tliraw.strLotInfo[0].siInfo)
      end
    end
    ($log.warn 'product is not specified (correctly)'; return) unless product.instance_of?(String)
    unless route
      if @customized
        prd = AMD_product_details(product)
      else
        prd = product_list(lottype).find {|p| p.productID.identifier == product}
      end
      ($log.warn "product #{product.inspect} not found"; return) unless prd
      route = prd.routeID.identifier
    end
    lottype ||= 'Production'
    sublottype ||= 'PO'
    customer ||= 'gf'
    order = params[:order] || ''
    lotOwner = params[:lotowner] || @_user.userID.identifier
    lotGenerationType = params[:lotGenerationType] || 'By Volume' # "By Source Lot"
    schedulingMode = params[:schedulingMode] || 'Forward'
    lotIDGenerationMode = params[:lotIDGenerationMode] || 'Automatic'
    productDefinitionMode = params[:productDefinitionMode] || 'By Product Quantity'
    priority = params[:priorityclass] || params[:priority] || 4
    epriority = params[:epriority] || '499999'
    startTime = siview_timestamp(params[:starttime] || Time.now)
    finishTime = siview_timestamp(params[:finishtime] || (siview_time(startTime) + 86400))
    memo = params[:release_memo] || params[:memo] || ''
    comment = params[:comment] || ''
    #
    lot = params[:lot] || generate_new_lotids(customer, starttime: startTime).first
		$log.info "new_lot_release: #{lot.inspect}, route: #{route.inspect}, product: #{product.inspect}, customer: #{customer.inspect}"
    $log.info {"  lotGenerationType: #{lotGenerationType.inspect}"} if lotGenerationType != 'By Volume'
    # set Fab7 lot attributes (any object)
    if @f7
      _lot_info ||= @jcscode.pptCS_LotInfo_siInfo_struct.new
      # lot attributes
      _lot_info.assetteOwner = params[:assetteOwner] || @_user.userID.identifier
      _lot_info.edgePublishFlag = params[:edgePublishFlag] || 'true'
      _lot_info.expSplits = params[:expSplits] || '10'
      _lot_info.inlineTestCode = params[:inlineTestCode] || ''
      _lot_info.jobClass = params[:jobClass] || ''
      _lot_info.lineCode = params[:lineCode] || ''
      _lot_info.lotLabel = params[:lotLabel] || ''
      _lot_info.lotGrade = params[:lotGrade] || ''
      _lot_info.POI = params[:POI] || ''
      _lot_info.profitAssetteFlag = params[:profitAssetteFlag] || ''
      _lot_info.qualityCode = params[:qualityCode] || ''
      _lot_info.lotProductType = params[:lotProductType] || ''
      _lot_info.facilityID = params[:facilityID] || ''
      _lot_info.famCode = params[:famCode] || ''
      _lot_info.supplierLocation = params[:supplierLocation] || ''
      _lot_info.year = params[:year] || ''
      _lot_info.week = params[:week] || ''
      _lot_info.pendShipFlag = params[:pendShipFlag] || ''
      _lot_info.disbCompState = params[:disbCompState] || ''
      _lot_info.lotExperimentName = params[:lotExperimentName] || ''
      _lot_info.demandClass = params[:demandClass] || ''
      _lot_info.wafFinDueDate = params[:wafFinDueDate] || ''
      _lot_info.dieStkDueDate = params[:dieStkDueDate] || ''
      _lot_info.financeUpdateFlag = params[:financeUpdateFlag] || ''
      _lot_info.featureCode = params[:featureCode] || ''
      _lot_info.dateOfMfg = params[:dateOfMfg] || siview_timestamp
      _lot_info.expediteFlag = params[:expediteFlag] || ''
      _lot_info.wcDateIn = params[:wcDateIn] || siview_timestamp
      _lot_info.wcModule = params[:wcModule] || ''
      _lot_info.SPFlag = params[:SPFlag] || ''
      _lot_info.lotOwner = params[:lotOwner] || @_user.userID.identifier
      _lot_info.lotComment = params[:lotComment] || 'Auto generated lot'
      _lot_info.shipDefinition = params[:shipDefinition] || ''
      _lot_info.activeRouteFlag = params[:activeRouteFlag] || ''
      _lot_info.activeModuleFlag = params[:activeModuleFlag] || ''
      #
      _wafer_proc = @jcscode.pptCS_waferProcessStatus_struct.new
      _wafer_proc.siInfo = any
      _lot_info.strCS_waferProcessStatus = [_wafer_proc]
      _wafer_disp = @jcscode.pptCS_waferProductDisposition_struct.new
      _wafer_disp.siInfo = any
      _lot_info.strCS_waferProductDisposition = [_wafer_disp]
      _ts = siview_timestamp
      subcons = SiView.tkinfo_to_erp(subcons) if subcons && !subcons.instance_of?(String)
      _lot_attr = @jcscode.pptCS_lotAdditionAttrib_siInfo_struct.new(_ts, _ts, _ts,
        subcons || '', params[:attr2] || '', tktype || '', params[:attr4] || '', qualitytag || '', params[:lineflag] || '', any)
      _lot_info.siInfo = insert_si(_lot_attr)
      anyobj = insert_si(_lot_info)
    else
      anyobj = any
    end
    #
    srclots = params[:srclot]
    srccarrier = params[:srccarrier]
    srclots ||= lot_list_incassette(srccarrier) if srccarrier
    if srclots
      slisraw = lots_info_raw(srclots, wafers: true)
      nwafers = params[:nwafers] || slisraw.strLotInfo.inject(0) {|n, sli| n + sli.strLotWaferAttributes.size}
      n = 0
      dslots = []
      slisraw.strLotInfo.each {|sli|
        dswafers = []
        sli.strLotWaferAttributes.each {|w|
          next if w.waferID.identifier.empty?   # TODO: can this happen?
          dswafers << @jcscode.pptSourceProduct_struct.new(w.waferID.identifier, any)
          n += 1
          break if n == nwafers
        }
        dslots << @jcscode.pptDirectedSourceLot_struct.new(
          sli.strLotBasicInfo.lotID, dswafers.to_java(@jcscode.pptSourceProduct_struct), any
        )
        $log.info "  added #{sli.strLotBasicInfo.lotID.identifier}, #{dswafers.size} wafers"
        break if n == nwafers
      }
    else
      nwafers = params[:nwafers] || 25
      dslots = []
    end
    lotID = oid(lot)
    llattr = lot_list(lot: lotID, raw: true).first  # for re-stb
    rattrs = [@jcscode.pptReleaseLotAttributes_struct.new(llattr ? llattr.lotID : lotID, oid(product), customer, order,
      lotOwner, lottype, sublottype, oid(route), lotGenerationType, schedulingMode, lotIDGenerationMode,
      productDefinitionMode, priority.to_s, epriority, startTime, finishTime, comment, nwafers,
      dslots.to_java(@jcscode.pptDirectedSourceLot_struct),
      [@jcscode.pptReleaseLotSchedule_struct.new('0.0', oid(''), startTime, startTime, any)].to_java(@jcscode.pptReleaseLotSchedule_struct),
      anyobj
    )]
    #
    res = @jcscode.pptNewLotReleaseReqResult_structHolder.new
    @manager.TxNewLotReleaseReq(res, @_user, rattrs, memo)
    return nil if rc(res.value) != 0
    return res.value.strReleasedLotReturn[0].lotID if params[:raw]
		return res.value.strReleasedLotReturn[0].lotID.identifier
  end

  # return 0 on success
  def new_lot_release_cancel(lots, params={})
    lotIDs = _as_objectIDs(lots)
    $log.info "new_lot_release_cancel #{lotIDs.collect {|e| e.identifier}}"
    #
		res = @jcscode.pptNewLotReleaseCancelReqResult_structHolder.new
    @manager.TxNewLotReleaseCancelReq(res, @_user, lotIDs, params[:memo] || '')
    return rc(res.value)
  end

  # update the data of a released lot, return 0 on success (used in MM_Lot_Release only)
  def new_lot_release_update(lot, params={})
    $log.info "new_lot_release_update #{lot.inspect}, #{params}"
    prq = product_request(lot)
    product = params[:product] || prq.productID.identifier
    route = params[:route] || prq.routeID.identifier
    lottype = params[:lottype] || prq.lotType
    sublottype = params[:sublottype] || 'PO'
    customer = params[:customer] || prq.customerCode
    # parameters for Fab7, ignored in regular SiView
    tktype = params[:tktype] || (params[:tkinfo] && params[:tkinfo].tktype)
    subcons = params[:subcons] || params[:tkinfo]
    qualitytag = params[:qualitytag]
    _lot_info = nil
    ($log.warn "product is not specified (correctly)"; return) unless product.instance_of?(String)
    order = params[:order] || ''
    lotOwner = params[:lotowner] || @_user.userID.identifier
    lotGenerationType = params[:lotGenerationType] || 'By Volume' # "By Source Lot"
    schedulingMode = params[:schedulingMode] || 'Forward'
    lotIDGenerationMode = params[:lotIDGenerationMode] || 'Automatic'
    productDefinitionMode = params[:productDefinitionMode] || 'By Product Quantity'
    priority = params[:priorityclass] || params[:priority] || 4
    epriority = params[:epriority] || '499999'
    startTime = siview_timestamp(params[:starttime] || Time.now)
    finishTime = siview_timestamp(params[:finishtime] || Time.now + 86400)
    memo = params[:release_memo] || params[:memo] || ''
    comment = params[:comment] || ''
    # end
    nwafers = 0
    prq = product_request(lot)
    dslots = prq.strSourceLotsAttributes.collect {|e|
      dswafers = e.strSourceWafersAttributes.collect {|w|
        @jcscode.pptSourceProduct_struct.new(w.waferID.identifier, any)
      }.to_java(@jcscode.pptSourceProduct_struct)
      nwafers += dswafers.size
      @jcscode.pptDirectedSourceLot_struct.new(e.lotID, dswafers, any)
    }.to_java(@jcscode.pptDirectedSourceLot_struct)
    sched = [@jcscode.pptReleaseLotSchedule_struct.new('0.0',
      oid(''), startTime, startTime, any)].to_java(@jcscode.pptReleaseLotSchedule_struct)
    lotAttributes = [@jcscode.pptUpdateLotAttributes_struct.new(oid(lot),
      oid(product), customer, order, lotOwner, sublottype, oid(route), lotGenerationType, schedulingMode,
      productDefinitionMode, priority.to_s, epriority, startTime, finishTime, comment, nwafers, dslots, sched, any)
    ].to_java(@jcscode.pptUpdateLotAttributes_struct)
    if @f7
      _lot_info ||= @jcscode.pptCS_LotInfo_siInfo_struct.new
      # lot attributes
      _lot_info.assetteOwner = params[:assetteOwner] || @_user.userID.identifier
      _lot_info.edgePublishFlag = params[:edgePublishFlag] || 'true'
      _lot_info.expSplits = params[:expSplits] || '10'
      _lot_info.inlineTestCode = params[:inlineTestCode] || ''
      _lot_info.jobClass = params[:jobClass] || ''
      _lot_info.lineCode = params[:lineCode] || ''
      _lot_info.lotLabel = params[:lotLabel] || ''
      _lot_info.lotGrade = params[:lotGrade] || ''
      _lot_info.POI = params[:POI] || ''
      _lot_info.profitAssetteFlag = params[:profitAssetteFlag] || ''
      _lot_info.qualityCode = params[:qualityCode] || ''
      _lot_info.lotProductType = params[:lotProductType] || ''
      _lot_info.facilityID = params[:facilityID] || ''
      _lot_info.famCode = params[:famCode] || ''
      _lot_info.supplierLocation = params[:supplierLocation] || ''
      _lot_info.year = params[:year] || ''
      _lot_info.week = params[:week] || ''
      _lot_info.pendShipFlag = params[:pendShipFlag] || ''
      _lot_info.disbCompState = params[:disbCompState] || ''
      _lot_info.lotExperimentName = params[:lotExperimentName] || ''
      _lot_info.demandClass = params[:demandClass] || ''
      _lot_info.wafFinDueDate = params[:wafFinDueDate] || ''
      _lot_info.dieStkDueDate = params[:dieStkDueDate] || ''
      _lot_info.financeUpdateFlag = params[:financeUpdateFlag] || ''
      _lot_info.featureCode = params[:featureCode] || ''
      _lot_info.dateOfMfg = params[:dateOfMfg] || siview_timestamp
      _lot_info.expediteFlag = params[:expediteFlag] || ''
      _lot_info.wcDateIn = params[:wcDateIn] || siview_timestamp
      _lot_info.wcModule = params[:wcModule] || ''
      _lot_info.SPFlag = params[:SPFlag] || ''
      _lot_info.lotOwner = params[:lotOwner] || @_user.userID.identifier
      _lot_info.lotComment = params[:lotComment] || 'Auto generated lot'
      _lot_info.shipDefinition = params[:shipDefinition] || ''
      _lot_info.activeRouteFlag = params[:activeRouteFlag] || ''
      _lot_info.activeModuleFlag = params[:activeModuleFlag] || ''
      #
      _wafer_proc = @jcscode.pptCS_waferProcessStatus_struct.new
      _wafer_proc.siInfo = any
      _lot_info.strCS_waferProcessStatus = [_wafer_proc]
      _wafer_disp = @jcscode.pptCS_waferProductDisposition_struct.new
      _wafer_disp.siInfo = any
      _lot_info.strCS_waferProductDisposition = [_wafer_disp]
      _ts = siview_timestamp
      subcons = SiView.tkinfo_to_erp(subcons) if subcons && !subcons.instance_of?(String)
      _lot_attr = @jcscode.pptCS_lotAdditionAttrib_siInfo_struct.new(_ts, _ts, _ts,
        subcons || '', params[:attr2] || '', tktype || '', params[:attr4] || '', qualitytag || '', params[:lineflag] || '', any)
      _lot_info.siInfo = insert_si(_lot_attr)
      anyobj = insert_si(_lot_info)
    else
      anyobj = any
    end
    upd = @jcscode.pptNewLotUpdateReqInParm_struct.new(lotAttributes, anyobj)
    #
    res = @jcscode.pptNewLotUpdateReqResult_structHolder.new
    @manager.TxNewLotUpdateReq(res, @_user, upd, memo)
    return rc(res.value)
  end

  # change parameters of an existing lot, return 0 on success BUT YOU NEED TO CHECK WITH lot_info!
  def schdl_change(lot, params={})
    lotID = lot.instance_of?(String) ? oid(lot) : lot
    $log.info "schdl_change #{lotID.identifier.inspect}, #{params}"
    ($log.warn '  parameters must be passed as hash'; return) unless params.instance_of?(Hash)
    li = lot_info(lotID) || return
    # get new parameters  #, using empty values as defaults except for route changes
    route = params[:route] || ''
    product = params[:product]
    product ||= (route.empty? ? '' : li.product)
    opNo = params[:opNo]
    opNo ||= ((product.empty? && route.empty?) ? '' : li.opNo)
    schedulingMode = params[:schedulingMode] || '' #"Forward"
    priority = (params[:priorityclass] || params[:priority] || '').to_s
    startTime = params[:starttime] || '' #Time.new.strftime("%Y-%m-%d-%H.%M.%S.000000")
    finishTime = params[:finishtime] || '' #(Time.new + 86400).strftime("%Y-%m-%d-%H.%M.%S.000000")
    sublottype = params[:sublottype] || ''
    memo = params[:memo] || ''
    #
    res = @jcscode.pptLotSchdlChangeReqResult_structHolder.new
    # leave the schedule empty
    chgschdl = [].to_java(@jcscode.pptChangedLotSchedule_struct)
    lattrs = [@jcscode.pptRescheduledLotAttributes__110_struct.new(lotID, oid(product), li.route,
      oid(route), li.opNo, opNo, sublottype, schedulingMode, priority, startTime, finishTime, chgschdl, any)]
    #
    @manager.TxLotSchdlChangeReq__110(res, @_user, lattrs, memo)
    return rc(res.value)
  end

  # return struct contains res.startBankID and res.strSourceLot (list), used in siviewtest for :ondemand only
  def srclot_inq(product, lot, params={})
    productID = product.instance_of?(String) ? oid(product) : product
    lotID = lot.instance_of?(String) ? oid(lot) : lot
    $log.info "srclot_inq #{productID.identifier.inspect}, #{lotID.identifier.inspect}"
    #
    res = @manager.TxSourceLotInq(@_user, productID, lotID)
    return nil if rc(res) != 0
    return res  # contains res.startBankID and res.strSourceLot (list)
  end

  # STB a released (scheduled) lot, return the lotID on success
  # note: while it is possible to set lot attributes here it should be done in new_lot_release
  def stb(lot, params={})
    lotID = lot.instance_of?(String) ? oid(lot) : lot
    $log.info "stb #{lotID.identifier.inspect}#{(', ' + params.inspect) unless params.empty?}"
    memo = params[:memo] || ''
    prq = product_request(lotID)
    # srclotIDs = prq.strSourceLotsAttributes.collect {|e| e.lotID}
    # all information except slotNumbers is in the product request
    carrierID = nil
    wattrs = []
    prq.strSourceLotsAttributes.each {|sla|
      $log.info "  using srclot #{sla.lotID.identifier}"
      sliraw = lots_info_raw(sla.lotID, wafers: true).strLotInfo.first # TODO: a cheaper method?
      carrierID = sla.cassetteID if carrierID.nil?
      sla.strSourceWafersAttributes.each {|w|
        srcw = sliraw.strLotWaferAttributes.find {|e| e.waferID.identifier == w.waferID.identifier}
        wattrs << @jcscode.pptNewWaferAttributes_struct.new(
          prq.lotID, w.waferID, srcw.slotNumber, sla.lotID, w.waferID, any)
      }
    }
    $log.info "  using srclot carrier #{carrierID.identifier.inspect}"
    lattrs = @jcscode.pptNewLotAttributes_struct.new(
      carrierID, wattrs.to_java(@jcscode.pptNewWaferAttributes_struct), any)
    #
    res = @manager.TxSTBReleasedLotReq(@_user, prq.lotID, lattrs, memo)
    return nil if rc(res) != 0
    return params[:raw] ? res.lotID : res.lotID.identifier
  end

  # STB an RMA lot to an existing lot family, reset the last split id if newlot: true, handle with care!
  # returns the target lotid
  def stb_lotfamily(lotfamily, srclot, tgtlot, params={})
    $log.info "stb_lotfamily #{lotfamily.inspect}, #{srclot.inspect}, #{tgtlot.inspect}, #{params}"
    sli = lot_info(srclot, select: params[:select], wafers: true)
    carrier = sli.carrier
    ($log.warn "  srclot must be in a carrier"; return) if carrier.empty?
    wafers = params[:wafers] || sli.wafers.collect {|w| w.wafer}  # default: all srclot wafers
    fli = lot_info(lotfamily)
    product = params[:product] || fli.product
    route = params[:route] || fli.route
    sublottype = params[:sublottype] || fli.sublottype
    owner = params[:lotowner] || fli.user
    priority = params[:priorityclass] || params[:priority] || fli.priority
    newlot = !!params[:newlot]
    historycheck = !!params[:historycheck]
    inparm = @jcscode.pptCS_STBToLotFamilyReqInParm_struct.new(oid(lotfamily), oid(carrier),
      oid(srclot), oid(tgtlot), oid(product), oid(route), priority, sublottype,
      oid(owner), oids(wafers), newlot, historycheck, any)
    #
    res = @manager.CS_TxSTBToLotFamilyReq(@_user, inparm, params[:memo] || '')
    return nil if rc(res) != 0
    res.lotID.identifier
  end

  # TODO: CS_TxAdjustLotFamilyReq, C7.15
  def adjust_lotfamily(lot, params={})
    res = @manager.CS_TxAdjustLotFamilyReq(@_user, oid(lot), params[:memo] || '')
    return nil if rc(res) != 0
    return res
  end

  # return lotid or array of lotids of the new vendor lot containing the former lot's wafers or nil
  def stb_cancel(lot, params={})
    $log.info "stb_cancel #{lot.inspect}"
    ci = stb_cancel_info(lot) || return
    wattrs = ci.strSTBCancelWaferInfoSeq.collect {|w|
      @jcscode.pptNewWaferAttributes_struct.new(
        w.currentLotID, w.waferID, w.slotNo, oid(w.STBSourceLotID), oid(''), any)
    }.to_java(@jcscode.pptNewWaferAttributes_struct)
    lattrs = @jcscode.pptNewLotAttributes_struct.new(ci.strSTBCancelledLotInfo.cassetteID, wattrs, any)
    cancel_parms = @jcscode.pptSTBCancelReqInParm_struct.new(
      ci.strSTBCancelledLotInfo.lotID, ci.strNewPreparedLotInfoSeq, lattrs, any)
    #
    res = @manager.TxSTBCancelReq(@_user, cancel_parms, params[:memo] || '')
    return nil if rc(res) != 0
    return res if params[:raw]
    vlots = res.strPreparedLotInfoSeq.collect {|l| l.lotID.identifier}
    $log.info "  new vendor lots: #{vlots}"
    # if @f7
    #   # reset lot udata for Fab7
    #   vlots.each do |vlot|
    #     lot_quality_code_change(vlot, quality_code_list.first)
    #     lot_financial_update_flag_change(vlot, 'N')
    #     lot_feature_code_change(vlot, '')
    #   end
    # end
    return vlots.size == 1 ? vlots.first : vlots
  end

  # used internally, returns info for stb_cancel
  def stb_cancel_info(lot)
    res = @manager.TxSTBCancelInfoInq(@_user, @jcscode.pptSTBCancelInfoInqInParm_struct.new(oid(lot), any))
    return nil if rc(res) != 0
    return res
  end

  # lottype is one of 'Dummy', Equipment Monitor', 'Process Monitor', return lot id or nil on error
  def stb_controllot(lottype, product, srclots, params={})
    productID = product.instance_of?(String) ? oid(product) : product
    srclotIDs = _as_objectIDs(srclots)
    $log.info "stb_controllot #{lottype.inspect}, #{productID.identifier.inspect}, #{srclotIDs.collect {|e| e.identifier}}#{(', ' + params.inspect) unless params.empty?}"
    sublottype = params[:sublottype] || lottype
    memo = params[:memo] || ''
    lot = params[:lot] || ''  # autogenerated
    lotID = lot.instance_of?(String) ? oid(lot) : lot
    nwafers = params.has_key?(:waferids) && params[:waferids].size || params[:nwafers]
    wattrs = []
    carrierID = nil
    lots_info_raw(srclotIDs, wafers: true).strLotInfo.each {|sli|
      if carrierID.nil?
        srclotcarrierID = sli.strLotLocationInfo.cassetteID
        carrierID = srclotcarrierID unless srclotcarrierID.identifier.empty?
      end
      srclotID = sli.strLotBasicInfo.lotID
      sli.strLotWaferAttributes.each {|w|
        wattrs << @jcscode.pptNewWaferAttributes_struct.new(
          lotID, w.waferID, w.slotNumber, srclotID, w.waferID, any)
        break if nwafers && wattrs.size == nwafers
      }
      $log.info "  using srclot #{srclotID.identifier}"
      break if nwafers && wattrs.size == nwafers
    }
    ($log.warn '  no carrier specified'; return) if carrierID.nil?
    $log.info "  using srclot carrier #{carrierID.identifier.inspect}"
    lattrs = @jcscode.pptNewLotAttributes_struct.new(
      carrierID, wattrs.to_java(@jcscode.pptNewWaferAttributes_struct), any)
    #
    if @f7
      res = @manager.CS_TxCtrlLotSTBReqForLotNaming(
        @_user, productID, wattrs.size, lottype, sublottype, lattrs, params[:lineflag] || '', memo)
    else
      res = @manager.TxCtrlLotSTBReq(@_user, productID, wattrs.size, lottype, sublottype, lattrs, memo)
    end
    return if rc(res) != 0
    $log.info "  created lot #{res.controlLotID.identifier.inspect} with #{wattrs.size} wafers"
    return params[:raw] ? res.controlLotID : res.controlLotID.identifier
  end

end
