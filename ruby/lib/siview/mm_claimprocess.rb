=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.
=end

class SiView::MM

  # Prepare processing of lot(s) including SLR, mode change (if requested) and carrier load.
  #
  # return array [cj, cjinfo, eqpiraw, portstatus, mode, lisraw] on success, else nil
  def claim_process_prepare(lots, params={})
    lots = [lots] if lots.instance_of?(String)
    lisraw = lots_info_raw(lots)
    lopi0 = lisraw.strLotInfo.first.strLotOperationInfo
    $log.info "claim_process_prepare #{lots.inspect} at #{lopi0.operationNumber} (#{lopi0.routeID.identifier} #{lopi0.operationID.identifier})"
    ##$log.debug {params.inspect}
    carriers = lisraw.strLotInfo.collect {|e| e.strLotLocationInfo.cassetteID.identifier}.uniq
    ccat = carrier_status(carriers.first, raw: true).cassetteBRInfo.cassetteCategory
    # get equipment, port and portgroup
    eqps = params[:eqp]
    eqps = [eqps] if eqps.instance_of?(String)
    if eqps.nil?
      eqps = lopi0.strLotEquipmentList.collect {|e| e.equipmentID.identifier}.sort
    end
    ($log.warn '  no equipment at operation'; return) if eqps.empty?
    pstatus = nil
    eqpiraw = nil
    eqps.each {|eqp|
      eqpiraw = eqp_info_raw(eqp, ib: params[:ib]) || return
      if port = params[:port]
        # if port is given find portgroup
        pstatus = eqpiraw.equipmentPortInfo.strEqpPortStatus.find {|p| p.portID.identifier == port}
      else
        # find a suitable free port
        eqpiraw.equipmentPortInfo.strEqpPortStatus.each {|p|
          next if p.portUsage != 'INPUT_OUTPUT'
          next if p.loadSequenceNumber > 1  # ib: 0, fb: first
          next unless p.loadedCassetteID.identifier.empty?
          next unless p.dispatchLoadCassetteID.identifier.empty?
          next unless p.dispatchUnloadCassetteID.identifier.empty?
          next unless p.loadResrvedCassetteID.identifier.empty?
          next if !p.cassetteCategoryCapability.empty? && !p.cassetteCategoryCapability == ccat
          # next if p.portState == 'Down' && !p.operationModeID.identifier.start_with?('Off-Line')
          next if !p.operationModeID.identifier.start_with?('Off-Line') && ['Down', '-'].include?(p.portState)
          pstatus = p
          break
        }
      end
      break if !pstatus.nil?
    }
    ($log.warn "no suitable port found on #{eqps} (wrong carrier category?)"; return) if pstatus.nil?
    eqpID = eqpiraw.equipmentID
    ib = eqpiraw.respond_to?(:equipmentBRInfoForInternalBuffer)
    #
    # prepare eqp
    if params[:eqp_status] != false
      res = 0
      estat = eqpiraw.equipmentStatusInfo.equipmentStatusCode.identifier
      res = eqp_status_change(eqpID, '2NDP') unless estat.end_with?('1PRD', '2NDP', '2WPR')
      res = eqp_status_change(eqpID, '2WPR') unless estat == '2WPR'
      return if res != 0
    end
    # if mode = params[:mode]
    mode = params[:mode] || 'Off-Line-1'
      # try to switch eqp to the desired mode, fails if eqp has a carrier loaded or a reservation
      if pstatus.operationMode != mode
        res = eqp_mode_change(eqpID, mode, {notify: false, eqpinfo: eqpiraw}.merge(params))
        return if res != 0
      end
    # else
    #  # pass mode: nil to use the current mode, currently not used
    #  mode = pstatus.operationMode
    # end
    # slr
    cj = nil
    cjinfo = nil
    if params[:slr] != false
      # note: slr collects all eqp ports if :cycle_port is true
      cj = slr(eqpID.identifier, ib: ib, eqpinfo: eqpiraw,
               port: pstatus.portID.identifier, cycle_port: params[:cycle_port],
               carrier: carriers, lot: lots, monitor: params[:monitor],
               target_chambers: params[:target_chambers]) || return
      cjinfo = lot_list_incontroljob(cj)  # need to load foups according to reservation
      if params[:xfer]
        cjinfo.first.strControlJobCassette.each {|e|
          carrier_xfer(e.cassetteID.identifier, '', '', eqpID.identifier, e.loadPortID.identifier)
        }
      end
      if cbslr = params[:onslr]
        cbslr.call(cj, cjinfo, eqpiraw, pstatus) || return
      end
    end
    #
    # load
    i = 0
    carriers.each {|carrier|
      if cjinfo
        cjcarrier = cjinfo.first.strControlJobCassette.find {|e|
          e.cassetteID.identifier == carrier
        } || ($log.warn "  carrier not in control job: #{carrier}"; return)
        loadpurpose = cjcarrier.loadPurposeType
        loadportID = cjcarrier.loadPortID
      else
        loadpurpose = params[:purpose] || 'Process Lot'
        loadportID = pstatus.portID
      end
      # get carrier 'close', note that SLR in Auto modes requires the carrier to be not EO, but e.g. SI
      if params[:xfer]
        return unless wait_carrier_xfer(carrier)  # note: DummyAMHS and JavaEIs must be running for correct locations
      elsif params[:claim_carrier] != false
        return if carrier_xfer_status_change(carrier, 'EO', eqpID) != 0
      end
      return if mode.start_with?('Auto', 'Semi') && eqp_port_status_change(eqpID, loadportID, 'LoadComp') != 0
      # end
      return if eqp_load(eqpID, loadportID, carrier, purpose: loadpurpose, ib: ib) != 0
      if cbload = params[:onload]
        # lpstatus = eqpiraw.equipmentPortInfo.strEqpPortStatus.find {|p| p.portID.identifier == loadportID.identifier}
        # cbload.call(cj, cjinfo, eqpiraw, lpstatus) || return
        cbload.call() || return
      end
    }
    # attention! eqpiraw is for eqpID and eqpi.ib only, pstatus for portID and portGroup only
    return [cj, cjinfo, eqpiraw, pstatus, mode, lisraw]
  end

  # Processes lot(s), eqp defaults to the first one at the current op, at the first available port.
  #
  # proctime defaults to ~1 second.
  # If a block is passed it is called with the controljobID as 1st parameter; no checks done on the return value
  #
  # return control job on success or nil
  def claim_process_lot(lots, params={})
    lots = [lots] if lots.instance_of?(String)
    $log.info "claim_process_lot #{lots}, #{params}"
    # optional synchronization for all eqp operations (except processing)
    synchronize = params[:synchronize]  # a Mutex object
    ($log.info '  obtaining lock'; synchronize.lock) if synchronize
    #
    # select and prepare equipment and load carrier
    res = claim_process_prepare(lots, params)
    unless res
      ($log.info '  releasing lock'; synchronize.unlock) if synchronize
      return
    end
    cj, cjinfo, eqpiraw, pstatus, mode, lisraw = res
    eqpID = eqpiraw.equipmentID
    ib = eqpiraw.respond_to?(:equipmentBRInfoForInternalBuffer)
    carriers = lisraw.strLotInfo.collect {|e| e.strLotLocationInfo.cassetteID.identifier}.uniq
    #
    # opestart
    # nodcitems = params[:nodcitems]
    cjID = eqp_opestart(eqpID, carriers, raw: true, ib: ib)  #, nodcitems: nodcitems)
    unless cjID && !cjID.identifier.empty?
      ($log.info '  releasing lock'; synchronize.unlock) if synchronize
      return
    end
    if cbstart = params[:onopestart]
      # cbstart.call(cj, cjinfo, eqpiraw, pstatus, mode) || return
      cbstart.call() || return
    end
    #
    # E10 status change in Auto mode
    if mode.start_with?('Auto') && eqpiraw.equipmentStatusInfo.equipmentStatusCode.identifier != '1PRD'
      eqp_status_change_rpt(eqpID, '1PRD')
    end
    #
    # start processing lots
    process_status(eqpID, cjID, lots, 'ProcessStart')
    ($log.info '  releasing lock'; synchronize.unlock) if synchronize
    #
    # set running hold?
    rh = params[:running_hold]
    lots.each {|lot| running_hold(lot, eqpID, cjID)} if rh
    #
    # external actions (default: sleep ~1 s) while the lot is being processed
    if block_given?
      yield cj, cjinfo, eqpiraw, pstatus  #, mode, _extract_lots_info(lisraw)  # TODO: use raw data
    else
      tproc = params[:proctime] || 0.8
      $log.info "  sleeping #{tproc} s (process time)"; sleep(tproc)
    end
    ($log.info '  obtaining lock'; synchronize.lock) if synchronize
    #
    # end processing lots
    process_status(eqpID, cjID, lots, 'ProcessEnd') unless params[:partial_comp] || rh
    # E10 status change in Auto mode if no other job is running
    if mode.start_with?('Auto') && eqp_info_raw(eqpID, ib: ib).equipmentInprocessingControlJob.collect {|e|
                                                          e.controlJobID.identifier}.length <= 1
      eqp_status_change_rpt(eqpID, '2WPR')
    end
    #
    if params[:opestart_cancel]
      res = eqp_opestart_cancel(eqpID, cjID, ib: ib)
      if res != 0
        ($log.info '  releasing lock'; synchronize.unlock) if synchronize
        return false
      end
    elsif !params[:noopecomp]  # opecomp unless earlyfoup (noopecomp: true)
      #unless nodcitems
        nwafers = lisraw.strLotInfo.first.strLotBasicInfo.totalWaferCount
        if mode.start_with?('Auto')
          eqp_tempdata(eqpID, pstatus.portGroup, cjID, data: {'WaferCount'=>nwafers})
        elsif params[:speccheck] != false
          eqp_data_speccheck(eqpID, cjID, data: {'WaferCount'=>nwafers})
        end
      #end
      if params[:partial_comp] && !rh
        # set additional params to choose wafer action, default: OperationCompletion
        eqp_partial_opecomp(eqpID, cjID, params.merge(ib: ib))
      else
        eqp_opecomp(eqpID, cjID, ib: ib, force: rh)
      end
      if rc != 0
        ($log.info '  releasing lock'; synchronize.unlock) if synchronize
        return
      end
      if cbopecomp = params[:onopecomp]
        # cbopecomp.call(cj, cjinfo, eqpiraw, pstatus) || return
        cbopecomp.call() || return
      end
    end
    #
    # unload
    unless params[:nounload]
      carriers.each {|carrier|
        if cjinfo
          cjcarrier = cjinfo.first.strControlJobCassette.find {|e|
            e.cassetteID.identifier == carrier
          } || ($log.warn "  carrier not in control job: #{carrier}"; return)
          unloadportID = cjcarrier.unloadPortID
        else
          unloadportID = pstatus.portID
        end
        if ib
          eqp_carrierfromib_reserve(eqpID, unloadportID, carrier)
          eqp_carrierfromib(eqpID, unloadportID, carrier)
        end
        if mode.start_with?('Auto', 'Semi')
          res = eqp_port_status_change(eqpID, unloadportID, 'UnloadReq')
        end
        res = eqp_unload(eqpID, unloadportID, carrier, ib: ib, autoib: false)
        if res != 0
          ($log.info '  releasing lock'; synchronize.unlock) if synchronize
          return
        end
        if cbunload = params[:onunload]
          # upstatus = eqpiraw.equipmentPortInfo.strEqpPortStatus.find {|p| p.portID.identifier == unloadportID.identifier}
          # cbunload.call(cj, cjinfo, eqpiraw, upstatus, mode) || return
          cbunload.call() || return
        end
        if mode.start_with?('Auto', 'Semi')
          res = eqp_port_status_change(eqpID, unloadportID, 'UnloadComp')
          sleep 0.1
          res = eqp_port_status_change(eqpID, unloadportID, 'LoadReq')
          if res != 0
            ($log.info '  releasing lock'; synchronize.unlock) if synchronize
            return
          end
        end
      }
    end
    ($log.info '  releasing lock'; synchronize.unlock) if synchronize
    #
    return cj
  end

end
