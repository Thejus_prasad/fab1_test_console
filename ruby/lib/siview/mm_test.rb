=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.
=end


class SiView::MM

  # # initiate real processing of carrier(s) on an equipment, currently not used
  # #
  # # issue slr and carrier_xfer (in Auto-2 and -3 modes) and wait for processing completion
  # #
  # # return true on success
  # def auto_process_carriers(eqp, ports, carriers, params={})
  #   carriers = [carriers] if carriers.instance_of?(String)
  #   ports = [ports] if ports.instance_of?(String)
  #   $log.info "auto_process_carriers #{eqp} #{ports.inspect} #{carriers.inspect} #{params.inspect}"
  #   ($log.warn "the numbers of carriers and ports must match"; return) if carriers.size != ports.size
  #   # reservation and transport
  #   eqpi = eqp_info(eqp, ib: params[:ib])
  #   cjs = []
  #   carriers.each_with_index {|carrier, i|
  #     port = ports[i]
  #     # slr, retry to cover ARMOR performance issues
  #     cj = slr(eqp, port: port, carrier: carrier, ib: !!eqpi.ib, slr_retries: 3) || return
  #     cjs << cj
  #     # xfer job for ports in Auto-2,-3
  #     $log.info "creating xfer job for #{carrier} to #{eqp}:#{port}"
  #     eqpi.ports.each {|p|
  #       if p.port == port && ['-2', '-3'].member?(p.mode[-2..-1])
  #         res = carrier_xfer(carrier, '', '', eqp, port)
  #         return nil if res != 0
  #       end
  #     }
  #   }
  #   # wait for transfers to complete
  #   wait_carrier_xfer(carriers) || return
  #   # wait for processing to complete
  #   ##return wait_cj(eqp, cjs)
  #   return wait_for {
  #     eqpi = eqp_info(eqp, ib: params[:ib])
  #     done = cjs.inject(true) {|r, cj| r && !(eqpi.cjs + eqpi.rsv_cjs).member?(cj)}
  #   }
  # end

  # clean up the equipment, all control jobs are canceled, return 0 on success
  def eqp_cleanup(eqp, params={})
    eqpID = eqp.instance_of?(String) ? oid(eqp) : eqp
    $log.info "eqp_cleanup #{eqpID.identifier.inspect}#{(', ' + params.inspect) unless params.empty?}"
    unless params[:scriptparams] == false
      # skip this during AutoProc test preparation
      uparams = user_parameter('Eqp', eqpID.identifier)
      ['Pull', 'Push'].each {|name|
        uparam = uparams.find {|p| p.name == name}
        user_parameter_change('Eqp', eqpID.identifier, name, '', 3, datatype: 'STRING') if uparam && uparam.valueflag
      }
    end
    mmudata = user_data(:eqp, eqpID.identifier, originator: 'MM').keys
    user_data_delete(:eqp, eqpID.identifier, mmudata) unless mmudata.empty?
    res = 0 # sum of all rc (should be 0)
    eqpiraw = eqp_info_raw(eqpID, ib: params[:ib]) || return
    eqpID = eqpiraw.equipmentID
    ib = eqpiraw.respond_to?(:equipmentBRInfoForInternalBuffer)
    eqpiraw.equipmentReservedControlJobInfo.strStartReservedControlJob.each {|rsvcj|
      res += slr_cancel(eqpID, rsvcj.controlJobID, ib: ib)
    }
    eqpiraw.equipmentInprocessingControlJob.each {|inprocessingcj|
      cjID = inprocessingcj.controlJobID
      cjinfo = lot_list_incontroljob(cjID)
      # remove running holds
      cjinfo.first.strControlJobCassette.each {|cjcarrier|
        cjcarrier.strControlJobLot.each {|cjlot| lot_hold_release(cjlot.lotID, nil, silent: true)}
      }
      # try opestart cancel
      if eqp_opestart_cancel(eqpID, cjID, ib: ib) != 0
        # cannot cancel, try opecomp
        cjinfo.first.strControlJobCassette.each {|cjcarrier|
          upstatus = eqpiraw.equipmentPortInfo.strEqpPortStatus.find {|p|
            p.portID.identifier == cjcarrier.unloadportID.identifier
          }
          upstatus.operationMode.start_with?('Auto') ? eqp_tempdata(eqpID, upstatus.portGroup, cjID) : eqp_data_speccheck(eqpID, cjID)
        }
        res += eqp_opecomp(eqpID, cjID, ib: ib)
      end
    }
    return res if res != 0
    eqpiraw.equipmentPortInfo.strEqpPortStatus.each {|p|
      carrier = p.loadedCassetteID.identifier
      res += eqp_unload(eqpID, p.portID, carrier, ib: ib, eqpinfo: eqpiraw) unless carrier.empty?
      carrier = p.dispatchLoadCassetteID.identifier
      res += npw_reserve_cancel(eqpID, carrier, ib: ib, eqpinfo: eqpiraw) unless carrier.empty?
    }
    InhibitClasses.each_pair {|classname, entities|
      if idx = entities.index('Equipment') || entities.index('Chamber')
        res += inhibit_cancel(classname, Array.new(idx, '') << eqpID.identifier, silent: true)
      end
    }
    # special handling depending on eqp category
    if ib
      eqpiraw.equipmentInternalBufferInfo.each {|b|
        b.strShelfInBuffer.each {|s|
          c = s.loadedCarrierID.identifier
          eqp_unload(eqpID, nil, c, ib: ib, eqpinfo: eqpiraw) unless c.empty?
        }
      }
      # TODO: ibreserve_cancel ?
      # eqpi.ib.rsv_carriers.each {|c| res += npw_reserve_cancel(eqpID, c, ib: true, eqpinfo: eqpiraw, silent: true)}
    elsif eqpiraw.equipmentBRInfo.equipmentCategory == 'Wafer Sorter'
      sj_list(eqp: eqpID).each {|sj|
        sj_status_change(eqpID, sj.sorterJobID, 'Error') unless sj.sorterJobStatus == 'Completed'
        sj_cancel(sj.sorterJobID)
      }
    end
    # switch E10 status (not considering CMMS)
    if params[:status_change] != false
      res += cleanup_e10state(eqpID, state: eqpiraw.equipmentStatusInfo.equipmentStatusCode.identifier)
      eqpiraw.equipmentChamberInfo.strEqpChamberStatus.each {|ch|
        res += cleanup_e10state(eqpID, chamber: ch.chamberID.identifier, state: ch.chamberStatusCode.identifier)
      }
    end
    # optionally set the mode
    if mode = params[:mode]
      res += eqp_mode_change(eqpID, mode, eqpinfo: eqpiraw, notify: params[:notify])
    end
    # set port status for Auto and Semi modes
    if params[:portstatus_change] != false
      portstatus = params[:portstatus] || 'LoadReq' # for Auto and Semi modes
      aports = params[:available_ports]             # for tests requiring only selected ports being available
      eqp_info_raw(eqpID, ib: ib).equipmentPortInfo.strEqpPortStatus.each {|p|
        if aports.nil? || !aports.member?(p.portID.identifier)
          if (p.operationModeID.identifier.start_with?('Semi', 'Auto') || ['Up', 'Down'].member?(portstatus)) &&
              p.portState != portstatus  # Up not defined
            # direct transition to LoadReq not always possible
            unless ['-', 'UnloadComp', portstatus].member?(p.portState)
              eqp_port_status_change(eqpID, p.portID, 'UnloadComp', silent: true)
            end
            res += eqp_port_status_change(eqpID, p.portID, portstatus)
          end
        end
        if !aports.nil? && !aports.member?(p.portID.identifier)
          res += eqp_port_status_change(eqpID, p.portID, 'Down')
        end
      }
    end
    #
    return res
  end

  # sets the E10 state to STANDBY.WAIT-PRODUCT considering given state transitions, return 0 on success
  def cleanup_e10state(eqp, params={})
    eqpID = eqp.instance_of?(String) ? oid(eqp) : eqp
    chamber = params[:chamber]
    if chamber
      cur_status = params[:state] || eqp_info(eqpID).chambers.find {|cha| chamber == cha.chamber}.status
      e10_proc = proc {|stat| chamber_status_change(eqpID, chamber, stat)}
    else
      cur_status = params[:state] || eqp_info(eqpID).status
      e10_proc = proc {|stat| eqp_status_change(eqpID, stat)}
    end
    if @f7
      cur_sub_state = cur_status[-4..-1]
      unless cur_sub_state == '2000'
        if cur_sub_state == '4000'
          e10_proc.call('4300')
        end
        if cur_sub_state >= '4000'
          e10_proc.call('5000')
          e10_proc.call('5300')
          e10_proc.call('5340')
        end
        return e10_proc.call('2000')
      end
    else
      e10_proc.call('2NDP') unless cur_status.end_with?('1PRD', '1SUP', '2SUP', '2NDP', '2WPR')
      return e10_proc.call('2WPR') unless cur_status.end_with?('2WPR')
    end
    return 0
  end

  # cleanup lot putting it in a carrier and locating it to the specified opNo (default is first), return 0 on success
  def lot_cleanup(lot, params={})
    lotID = lot.instance_of?(String) ? oid(lot) : lot
    $log.info "lot_cleanup #{lotID.identifier.inspect}, #{params}"
    res = 0
    liraw = lots_info_raw(lotID).strLotInfo.first
    lotID = liraw.strLotBasicInfo.lotID
    carrierID = liraw.strLotLocationInfo.cassetteID
    unless carrierID.identifier.empty?
      if params[:xjs]
        xjs = nil
        # wait_for(timeout: 180) {(xjs = carrier_xferjob_detail(carrierID)).empty?}
        tend = Time.now + 180
        loop {
          break if (xjs = carrier_xferjob_detail(carrierID)).empty?
          break if Time.now >= tend
          sleep 10
        }
        carrier_xferjob_delete(carrierID) unless xjs.empty?
      end
      pp_force_delete(carrierID, '')
    end
    pp_force_delete('', lotID)
    # cancel sorter jobs
    sj_list(lot: lotID).each {|sj|
      sj_status_change(sj.equipmentID, sj.sorterJobID, 'Error') unless sj.sorterJobStatus == 'Completed'
      sj_cancel(sj.sorterJobID)
    }
    # cancel control jobs
    liraw = lots_info_raw(lotID).strLotInfo.first
    cjID = liraw.strLotControlJobInfo.controlJobID
    unless cjID.identifier.empty?
      eqpID = oid(cjID.identifier.split('-')[0..-3].join('-'))
      if liraw.strLotBasicInfo.lotStatus == 'Processing'
        eqp_opecomp(eqpID, cjID) if eqp_opestart_cancel(eqpID, cjID) != 0
        return rc if rc != 0
      else
        return rc if slr_cancel(eqpID, cjID) != 0
      end
    end
    # ensure lot is in a proper carrier
    carrierID = liraw.strLotLocationInfo.cassetteID
    if carrierID.identifier.empty?
      c = _get_empty_carrier(params[:carrier], params)
      carrier_reserve_cancel(c, check: true, silent: true)
      res += wafer_sort(lotID, c, slots: 'empty')
    else
      if (c = params[:carrier]) || (ccat = params[:carrier_category])
        if (c && !c.end_with?('%') && carrierID.identifier != c) ||
           (c && c.end_with?('%') && !carrierID.identifier.start_with?(c.gsub('%', ''))) ||
           (ccat && carrier_status(carrierID).category != ccat)
          c = _get_empty_carrier(params[:carrier], params)
          # carrier_usage_reset(c)
          carrier_reserve_cancel(c, check: true, silent: true)
          res += wafer_sort(lotID, c, slots: 'empty')
        end
      else
        carrier_reserve_cancel(carrierID, check: true, silent: true)
        ## NONO: carrier_usage_reset(li.carrier)
        res += carrier_status_change(carrierID, 'AVAILABLE', check: true)
      end
    end
    status = liraw.strLotBasicInfo.lotStatus
    res += scrap_wafers_cancel(lotID) if status == 'SCRAPPED'  # must be in a carrier, else 'already marked for deletion' error
    return res if res != 0
    res += lot_ship_cancel(lotID) if status == 'SHIPPED'
    res += lot_nonprobankout(lotID) if status == 'NonProBank'
    res += lot_bankhold_release(lotID) unless lot_hold_list(lotID, type: 'BankHold').empty?
    res += lot_hold_release(lotID, nil, params.merge(silent: true))
    res += lot_futurehold_cancel(lotID, nil, params.merge(silent: true))
    res += lot_qtime_update(lotID, 'Delete', silent: true) if liraw.strLotBasicInfo.qtimeFlag
    liraw = lots_info_raw(lotID).strLotInfo.first
    carrierID = liraw.strLotLocationInfo.cassetteID
    if liraw.strLotLocationInfo.transferStatus == 'EI'
      res += eqp_unload(liraw.strLotLocationInfo.equipmentID, nil, carrierID)
      return res if res != 0
    end
    res += carrier_xfer_status_change(carrierID, '', params[:stocker]) if params[:stocker]
    return res if params[:move] == false || liraw.strLotBasicInfo.lotType == 'Vendor'
    #
    # bankin cancel
    if liraw.strLotBasicInfo.strLotStatusList.find {|e| e.stateName == 'Lot Inventory State'}.stateValue == 'InBank'
      if params.has_key?(:endbank)
        lot_bank_move(lotID, params[:endbank])
      else
        sleep 30  # wait for history  # TODO: replace with direct route info
        hist = lot_operation_history(lotID, raw: true)
        r = hist.select {|e| e.operationCategory == 'BankIn'}.last
        lot_bank_move(lotID, r.bankID) if r && r.bankID != liraw.strLotBasicInfo.bankID.identifier
      end
      res += lot_bankin_cancel(lotID)
    end
    res += lot_experiment_delete(lotID, silent: true) ? 0 : 10000
    # locate to specified opNo or op, defaulting to first opNo, unless :current is passed
    op = params[:op]
    return res if op == :current
    opNo = params[:opNo]
    opNo = :first if op.nil? && opNo.nil?
    res = lot_opelocate(lotID, opNo,
      op: op, force: :ondemand, route: params[:route], forward: params[:forward], offset: params[:offset])
    lot_futurehold_cancel(lotID, nil, silent: true)
    lot_hold_release(lotID, nil, silent: true)
    return res
  end

  def merge_lot_family(lot, params={})
    lotID = lot.instance_of?(String) ? oid(lot) : lot
    $log.info "merge_lot_family #{lotID.identifier.inspect}" unless params[:silent]
    lotIDs = []
    lot_family(lotID, raw: true).each {|a|
      fstate = a.strLotStatusList.find {|e| e.stateName == 'Lot Finished State'}
      lotIDs << a.lotID unless ['EMPTIED', 'SCRAPPED'].member?(fstate.stateValue)
    }
    return true if lotIDs.size == 1
    lisraw = lots_info_raw(lotIDs)  # lot_family does not return carriers
    liraw0 = lisraw.strLotInfo.first
    carrierID0 = lisraw.strLotInfo.first.strLotLocationInfo.cassetteID
    carriers = lisraw.strLotInfo.collect {|e| e.strLotLocationInfo.cassetteID.identifier}
    if carriers.size > 1 || carriers.first != carrierID0.identifier   # TODO: wrong carriers.first (?)
      carrierlotIDs = lot_list_incassette(carrierID0, raw: true).strLotListInCassetteInfo.lotID
      # remove foreign lots from parent lot carrier
      lots_info_raw(carrierlotIDs).strLotInfo.each {|strLotInfo|
        cjID = strLotInfo.strLotControlJobInfo.controlJobID
        unless cjID.identifier.empty?
          eqpID = oid(cjID.identifier.split('-')[0..-3].join('-'))
          if strLotInfo.strLotBasicInfo.lotStatus == 'Processing'
            eqp_opestart_cancel(eqpID, cjID)
          else
            slr_cancel(eqpID, cjID)
          end
          carrierlotID = strLotInfo.strLotBasicInfo.lotID
          wafer_sort(carrierlotID, '') unless lotIDs.find {|e| e.identifier == carrierlotID.identifier}
        end
      }
    end
    uparams = user_parameter('Lot', lot)
    slt0 = liraw0.strLotBasicInfo.subLotType
    product0 = liraw0.strLotProductInfo.productID.identifier
    route0 = liraw0.strLotOperationInfo.routeID.identifier
    opNo0 = liraw0.strLotOperationInfo.operationNumber
    lisraw.strLotInfo.each {|strLotInfo|
      lcID = strLotInfo.strLotBasicInfo.lotID
      lot_hold_release(lcID, nil)
      lot_futurehold_cancel(lcID, nil)
      next if lcID.identifier == lot
      next if strLotInfo.strLotBasicInfo.strLotStatusList.find {|e|
        e.stateName == 'Lot State'
      }.stateValue == 'FINISHED'
      lot_experiment_delete(lcID) unless @f7
      if strLotInfo.strLotLocationInfo.cassetteID.identifier != carrierID0.identifier
        wafer_sort(lcID, carrierID0, slots: 'empty').zero? || return
      end
      slt = strLotInfo.strLotBasicInfo.subLotType
      sublottype_change(lcID, slt) if slt != slt0
      uparams.each {|p|
        user_parameter_change('Lot', lcID.identifier, p.name, p.value) if p.description.include?('<MERGECHECK>')
      }
      product = strLotInfo.strLotProductInfo.productID.identifier
      route = strLotInfo.strLotOperationInfo.routeID.identifier
      unless product == product0 && route == route0
        schdl_change(lcID.identifier, product: product0, route: route0, opNo: opNo0)
      end
      lot_futurehold_cancel(lcID, nil)
      lot_opelocate(lcID, opNo0)
      lot_hold_release(lcID, nil)   # process holds
      lot_merge(liraw0.strLotBasicInfo.lotID, lcID)
    }
    return lot_family(lot) == [lot]
  end

  # prepare the lot and all family members for deletion, optionally delete (use with care!), return 0 on success
  def delete_lot_family(lot, params={})
    lotID = lot.instance_of?(String) ? oid(lot) : lot
    $log.info "delete_lot_family #{lotID.identifier.inspect}"
    # lot is released but not STB'd
    if rel = released_lots(params[:layer] || 'B').find {|e| e.lotID.identifier == lotID.identifier}
      return new_lot_release_cancel(rel.lotID)
    end
    # lot might be finished and does not show up in lot_family
    lf = params[:lotfamily] == false ? [lotID.identifier] : lot_family(lotID, raw: true).collect {|e| e.lotID.identifier}
    lf.each {|lc|
      lci = lot_list(lot: lc, raw: true).first  # can handle not existing lots
      next if lci.nil?
      next if lci.strLotStatusList.find {|e| e.stateName == 'Lot Finished State'}.stateValue == 'EMPTIED'
      lcID = lci.lotID
      if !lci.carrierID.identifier.empty?
        # protect real ITDC lots
        desc = carrier_status(lci.carrierID, raw: true).cassetteBRInfo.description
        if desc.start_with?('REAL', 'ITDC Sorter Test') || desc.end_with?('Automation') || desc.include?('Barbarine')
          $log.warn "  NOT deleting lot #{lcID.identifier} in real carrier #{lci.carrierID.identifier}"
          return
        end
        # call sj_list and pp_force_delete for carrier only if lci.carrier is not empty!
        sj_list(carrier: lci.carrierID).each {|sj| sj_cancel(sj.sorterJobID)}
        pp_force_delete(lci.carrierID, '')
      end
      pp_force_delete('', lcID)
      if lci.strLotStatusList.find {|e| e.stateName == 'Lot Inventory State'}.stateValue == 'NonProBank' &&
         lci.strLotStatusList.find {|e| e.stateName == 'Lot State'}.stateValue != 'FINISHED'
        lot_nonprobankout(lcID)
      end
      lci = lots_info_raw(lcID).strLotInfo.first
      cjID = lci.strLotControlJobInfo.controlJobID
      lcstats = lci.strLotBasicInfo.strLotStatusList
      unless cjID.identifier.empty?
        eqpID = oid(cjID.identifier.split('-')[0..-3].join('-'))
        if lcstats.find {|e| e.stateName == 'Lot Process State'}.stateValue == 'Processing'
          lot_hold_release(lcID, nil) if lcstats.find {|e| e.stateName == 'Lot Hold State'}.stateValue == 'ONHOLD'
          eqp_opecomp(eqpID, cjID) if eqp_opestart_cancel(eqpID, cjID) != 0
          return rc if rc != 0
        else
          return rc if slr_cancel(eqpID, cjID) != 0
        end
      end
      lci = lots_info_raw(lcID, wafers: true).strLotInfo.first
      lloci = lci.strLotLocationInfo
      eqp_unload(lloci.equipmentID, nil, lloci.cassetteID) if lloci.transferStatus == 'EI'
      w = lci.strLotWaferAttributes.first
      if lci.strLotBasicInfo.lotType == 'Vendor' && w && w.waferID.identifier.empty?
        vendorlot_return(lcID) # vendor lot without wafer ids
      else
        scrap_wafers(lcID) unless ['SCRAPPED', 'SHIPPED'].member?(lci.strLotBasicInfo.lotStatus)
        sublottype_change(lcID, 'SCRAPQA') if lci.strLotBasicInfo.subLotType != 'SCRAPQA'
      end
    }
    lf.each {|lc|
      lci = lot_list(lot: lc, raw: true).first  # can handle not existing lots
      next unless lci
      lcID = lci.lotID
      if lci.sorterJobExistFlag
        sj = sj_list(lot: lcID).first
        if sj
          sj_status_change(sj.equipmentID, sj.sorterJobID, 'Error') if sj.sorterJobStatus != 'Completed'
          sj_cancel(sj.sorterJobID)
        end
      end
      wafer_sort(lcID, '')  unless lci.carrierID.identifier.empty?
    }
    return 0 unless params[:delete]  # leave actual deletion to LotMaintenance
    sleeptime = params[:sleeptime] || 40
    children = lf[0..-2]
    if children.size > 0
      $log.info "trying to delete children #{children} in #{sleeptime} s"; sleep sleeptime
      res = lot_delete(children)
      return res if res != 0
    end
    return 0 if lot_list(lot: lotID).empty?  # lot does not exist (any more)
    $log.info "trying to delete #{lotID.identifier.inspect} in #{sleeptime} s"; sleep sleeptime
    return lot_delete(lotID)
  end

  # return hash {opNo: eqps} for a lot, optionally filtered by eqp, nil on error
  def lot_eqplist_in_route(lot, params={})
    eqp = params[:eqp]
    product = lot_info(lot).product
    rops = lot_processlist_in_route(lot)
    ret = {}
    res = rops.each_pair {|opNo, op|
      eqps = eqp_list_byop(product, op).collect {|eqp| eqp.equipmentID.identifier}
      next if eqp && !eqps.member?(eqp)
      ret[opNo] = eqps
    }
    return Hash[ret]
  end

  # DEPRECATED, use SiView::Test.get_empty_carrier
  #
  # Get an empty carrier if not provided explicitly. Wildcard % is supported, return carrier ID or nil
  def _get_empty_carrier(carrier, params={})
    return carrier unless carrier.nil? || carrier.index('%')  # no search if carrier is explicitly given
    cparams = {carrier_category: 'FEOL', carrier: carrier, status: 'AVAILABLE', empty: true, sjcheck: true}.merge(params)
    carriers = carrier_list(cparams)
    ($log.warn "no empty carriers found, aborting"; return) if carriers.nil? || carriers.empty?
    exclude = params[:exclude] || []
    exclude = [exclude] if exclude.instance_of?(String)
    carriers.each {|c|
      next if exclude.member?(c)
      next if (carrier.nil? || !carrier.start_with?('9')) && c.start_with?('9') # skip Turnkey carriers if not requested explicitly
      csraw = carrier_status(c, raw: true)
      # next if csraw.nil?
      next if csraw.cassetteStatusInfo.cassetteStatus == 'SCRAPPED'
      next if csraw.cassetteStatusInfo.transferStatus == 'EI'
      next if !csraw.cassetteStatusInfo.transferReserveUserID.identifier.empty?
      desc = csraw.cassetteBRInfo.description
      next if desc.start_with?('REAL', 'ITDC Sorter Test') || desc.end_with?('Automation')
      return c
    }
    return nil
  end

end
