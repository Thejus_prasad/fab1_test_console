=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.
=end


class SiView::MM
  UParam = Struct.new(:name, :value, :datatype, :keyvalue, :valueflag, :description)

  # get script parameters, e.g. for class Eqp, return array of Uparams on success
  def user_parameter(classname, pid, params={})
    res = @manager.TxUserParameterValueInq(@_user, classname, pid)
    return nil if rc(res) != 0
    return res if params[:raw]
    name = params[:name]
    ret = res.parameters.collect {|p|
      next unless params[:all] || !name && p.valueFlag || name && (p.parameterName == name)
      UParam.new(p.parameterName, p.value, p.dataType, p.keyValue, p.valueFlag, p.description)
    }.compact
    ret0 = ret[0]
    return (name && !params[:istable] && !(ret0 && ret0.datatype.start_with?('TABLE'))) ? ret0 : ret
  end

  # set script parameters, e.g. for class Eqp
  # changetype 1..add 2..update 3..delete (wrong doc!!)
  # get default parameters from current settings, at least data type has to be specified to add new parameter
  #
  # pass as ...('Lot', lot, 'name', 'value') or ('Lot', lot, {'name1'=>'value1', 'name2'=>'value2'})
  #
  # return 0 on success (note that user_parameter_change may return 0 even if the change failed)
  def user_parameter_change(classname, pid, names, values=nil, changetype=1, params={})
    if values.nil? && changetype == 1
      $log.info "user_parameter_change #{classname.inspect}, #{pid.inspect}, #{names.inspect}" unless params[:silent]
      pvalue = _build_uparams(classname, pid, names.keys, names.values, changetype, params[:datatype], params) || return
    else
      unless params[:silent]
        $log.info "user_parameter_change #{classname.inspect}, #{pid.inspect}, #{names.inspect}, #{values.inspect}, #{changetype}"
      end
      pvalue = _build_uparams(classname, pid, names, values, changetype, params[:datatype], params) || return
    end
    #
    res = @manager.TxUserParameterValueChangeReq(@_user, classname, pid, pvalue)
    return rc(res)
  end

  # TODO: handle directly in user_parameter_change
  def _build_uparams(classname, pid, names, values, changetype, datatype, params)
    values = [values] unless values.instance_of?(Array)
    names = [names] unless names.instance_of?(Array)
    keyvalue = params[:keyvalue] || ''
    description = params[:description] || ''
    valueflag = params[:valueflag] || (changetype != 3)  # true if setting/updating, false if deleting
    unless datatype
      currs = user_parameter(classname, pid, raw: true) || return
    end
    return names.each_with_index.collect {|name, i|
      if datatype
        dtype = datatype.instance_of?(Array) ? datatype[i] : datatype
      else
        curr = currs.parameters.find {|e| e.parameterName == name} || ($log.warn "  no such parameter: #{name.inspect}"; return)
        dtype = curr.dataType
      end
      @jcscode.pptUserParameterValue_struct.new(changetype, name, dtype, keyvalue, values[i].to_s, valueflag, description, any)
    }.to_java(@jcscode.pptUserParameterValue_struct)
  end

  # delete a user parameter, many if passed as Array or Hash, return 0 on success
  def user_parameter_delete(classname, pid, name, params={})
    # for easy cleanup: delete the named parameter if set
    return 0 if params[:check] && !user_parameter(classname, pid, name: name).valueflag
    # if name.instance_of?(String)
    #   return 0 unless user_parameter(classname, pid, name: name).valueflag
    # end
    # #
    return user_parameter_change(classname, pid, name, nil, 3, silent: params[:silent])
  end

end
