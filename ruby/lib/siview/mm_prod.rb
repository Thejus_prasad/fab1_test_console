=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.
=end

class SiView::MM

  # return array of product information for a product category (a.k.a. lottype), defaulting to 'Production'
  def product_list(lottype)
    res = @manager.TxProductIDListInq(@_user, lottype || 'Production')
    ($log.warn "error in TxProductIDListInq for lottype #{lottype}"; return) if rc(res) != 0
    return res.strProductIDListAttributes
  end

  # return array of Product or nil on error
  def CS_product_list(params={})
    pcat = params[:lottype] || ''  # ALL or '', Production, etc.; actually the product category
    product = params[:product] || ''
    description = params[:description] || ''
    productgroup = params[:productgroup] || ''
    mfgLayer = params[:layer] || ''
    technology = params[:technology] || ''
    owner = params[:owner] || ''
    route = params[:route] || ''
    #
    res = @manager.CS_TxProductIDListInq(@_user, pcat, product, description, productgroup, mfgLayer, technology, owner, route)
    return nil if rc(res) != 0
    return res.strProductIDListAttributes
  end

  # return details for a specific product, for SIL, R15 ready
  def AMD_product_details(product)
    res = @manager.AMD_TxProductDetailsInq(@_user, product)
    return nil if rc(res) != 0
    return res.strProductIDListAttributes
  end

  # return the created lot ID or nil on error
  def vendorlot_receive(bank, product, count, params={})
    bankID = bank.instance_of?(String) ? oid(bank) : bank
    productID = product.instance_of?(String) ? oid(product) : product
    lot = params[:lot] || ''
    vendorlot = params[:vendorlot] || ''
    vendor = params[:vendor] || ''
    sublottype = params[:sublottype] || 'VendorOPSPPL'
    memo = params[:memo] || ''
    $log.info "vendorlot_receive #{bankID.identifier.inspect}, #{productID.identifier.inspect}, #{count}#{(', ' + params.inspect) unless params.empty?}"
    #
    if @f7 || params[:customized]
      lotlabel = params[:lotlabel] || ''  # for customized Tx, used in Fab7
      res = @manager.CS_TxVendLotReceiveReq(@_user, bankID, productID, lot, sublottype, count, vendorlot, vendor, lotlabel, memo)
    else
      res = @manager.TxVendLotReceiveReq(@_user, bankID, productID, lot, sublottype, count, vendorlot, vendor, memo)
    end
    return nil if rc(res) != 0
    lotID = res.createdLotID
    $log.info "  received vendorlot #{lotID.identifier.inspect}"
    return params[:raw] ? lotID : lotID.identifier
  end

  # return created lot ID or nil on error
  def vendorlot_prepare(lot, carrier, params={})
    lotID = lot.instance_of?(String) ? oid(lot) : lot
    carrierID = carrier.instance_of?(String) ? oid(carrier) : carrier
    $log.info "vendorlot_prepare #{lotID.identifier.inspect}, #{carrierID.identifier.inspect}, #{params}"
    ($log.warn '  no carrier passed'; return) if carrierID.identifier.empty?
    srclot = params[:srclot] || lotID
    srclotID = srclot.instance_of?(String) ? oid(srclot) : srclot
    sliraw = lots_info_raw(srclotID, wafers: true).strLotInfo.first
    lottype = params[:lottype] || sliraw.strLotBasicInfo.lotType || 'Vendor'
    sublottype = params[:sublottype] || sliraw.strLotBasicInfo.subLotType || 'VendorOPSPPL'
    memo = params[:memo] || ''
    waferids = params[:waferids]
    slots = params[:slots]
    nwafers = params[:nwafers] || !waferids.nil? && waferids.size || sliraw.strLotWaferAttributes.size
    widprefix = "#{unique_string}#{params[:lasermark] || 'QA'}"  # for generated wids, 10 digits
    wattrs = sliraw.strLotWaferAttributes.take(nwafers).each_with_index.collect {|w, i|
      slot = slots.instance_of?(Array) ? slots[i] : w.slotNumber
      # use explicitly passed wafer ids or source wafer ids, generate missing wafer ids
      if waferids.instance_of?(Array)
        wid = oid(waferids[i])  # || "#{widprefix}%02d" % slot)
      else
        wid = w.waferID.identifier.empty? ? oid("#{widprefix}%02d" % slot) : w.waferID
      end
      @jcscode.pptNewWaferAttributes_struct.new(lotID, wid, slot, srclotID, w.waferID, any)
    }.to_java(@jcscode.pptNewWaferAttributes_struct)
    attrs = @jcscode.pptNewLotAttributes_struct.new(carrierID, wattrs, any)
    #
    res = @manager.TxVendorLotPreparationReq(@_user, sliraw.strLotBasicInfo.bankID, lottype, sublottype, attrs, memo)
    return nil if rc(res) != 0
    lotID = res.lotID
    $log.info "  prepared vendorlot #{lotID.identifier}"
    return params[:raw] ? lotID : lotID.identifier
  end

  # used by vendor lot preparation cancel
  def vendorlot_preparation_cancel_info(lot)
    lotID = lot.instance_of?(String) ? oid(lot) : lot
    #
    res = @manager.TxLotPreparationCancelInfoInq(@_user,
      @jcscode.pptLotPreparationCancelInfoInqInParm_struct.new(lotID, any))
    return nil if rc(res) != 0
    return res
  end

  # cancel prepared vendor lot (lot must not be in a carrier), return result struct or nil
  def vendorlot_preparation_cancel(lot, params={})
    lotID = lot.instance_of?(String) ? oid(lot) : lot
    $log.info "vendorlot_preparation_cancel #{lotID.identifier.inspect}"
    # gather lot and wafer info
    vi = vendorlot_preparation_cancel_info(lotID) || return
    carrier = vi.strPreparationCancelledLotInfo.cassetteID.identifier
    ($log.warn '  lot must not be in a carrier'; return) unless carrier.empty?
    cparms = @jcscode.pptLotPreparationCancelReqInParm_struct.new(
      vi.strPreparationCancelledLotInfo.lotID, vi.strNewVendorLotInfoSeq, any)
    #
    res = @manager.TxLotPreparationCancelReq(@_user, cparms, params[:memo] || '')
    return nil if rc(res) != 0
    return res
#    return res.strReceivedLotInfoSeq.map {|lot| lot.newLotID.identifier}
  end

  # return a vendor lot, or a number of wafers to the vendor, return 0 on success
  def vendorlot_return(lot, params={})
    lotID = lot.instance_of?(String) ? oid(lot) : lot
    $log.info "vendorlot_return #{lotID.identifier.inspect}"
    nwafers = params[:nwafers] || lots_info_raw(lotID).strLotInfo.first.strLotBasicInfo.totalWaferCount
    memo = params[:memo] || ''
    #
    res = @manager.TxVendLotReturnReq(@_user, lotID, nwafers, memo)
    return rc(res)
  end

end
