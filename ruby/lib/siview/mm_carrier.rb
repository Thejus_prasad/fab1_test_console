=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.
=end


class SiView::MM

  CarrierStatus = Struct.new(:carrier, :category, :status, :user, :isempty, :sjexists, :mltype,
    :eqp, :stocker, :xfer_status, :xfer_user, :lots, :claim_time)

  # return array of cassette names, optionally restricted to the value passed as :count parameter
  def carrier_list(params={})
    category = params[:carrier_category] || params[:category] || ''
    empty = !!params[:empty]
    stocker = params[:stocker] || ''
    carrier = params[:carriers] || params[:carrier] || ''
    status = params[:status] || ''
    sjcheck = !!params[:sjcheck]
    interfab_state = params[:interfab_state] || ''
    count = params[:count] || 9999
    #
    if params[:customized] # used from OPI only?
      inparm = @jcscode.pptCS_CassetteListInq__150InParm_struct.new(category, empty, oid(stocker), oid(carrier),
        status, count, sjcheck, interfab_state, -1, -1, false, any)
      res = @manager.CS_TxCassetteListInq__150(@_user, inparm)
      return if rc(res) != 0
      return res.strFoundCassetteSeq.collect {|e| e.strFoundCassette.cassetteID.identifier}
    else
      res = @manager.TxCassetteListInq__100(@_user, category, empty, oid(stocker), oid(carrier), status, count, sjcheck, interfab_state)
      return if rc(res) != 0
      return res.strFoundCassette.collect {|e| e.cassetteID.identifier}
    end
  end

  # return CarrierStatus struct
  def carrier_status(carrier, params={})
    carrierID = carrier.instance_of?(String) ? oid(carrier) : carrier
    res = @manager.TxCassetteStatusInq__120(@_user, carrierID)
    ($log.warn "error in carrier_status #{carrierID.identifier.inspect}"; return) if rc(res) != 0
    return res if params[:raw]
    #
    si = res.cassetteStatusInfo
    lots = si.strContainedLotInfo.collect {|l| l.lotID.identifier}.sort
    # UNUSED, preserve!
    # pmi = extract_si(res.cassettePMInfo.siInfo)
    # if pmi
    #   counter = CarrierPMCounter.new(pmi.CarrierOpeCount, pmi.GasketOpeCount, pmi.FilterOpeCount, pmi.GasketCurrTime,
    #     pmi.FilterCurrTime, pmi.GasketResetTime, pmi.FilterResetTime)
    # else
    #   counter = CarrierPMCounter.new(res.cassettePMInfo.operationStartCount)
    # end
    # sinfo = extract_si(res.siInfo)
    # condition = sinfo ? sinfo.carrierCondition : ''
    return CarrierStatus.new(carrier, res.cassetteBRInfo.cassetteCategory,
      si.cassetteStatus, si.lastClaimedPerson.identifier, si.emptyFlag, si.sorterJobExistFlag,
      si.multiLotType, si.equipmentID.identifier, si.stockerID.identifier, si.transferStatus,
      si.transferReserveUserID.identifier, lots, si.lastClaimedTimeStamp)
  end

  # change carrier status, e.g. "AVAILABLE", return 0 on success, TODO: separate _multi
  def carrier_status_change(carrier, status, params={})
    memo = params[:memo] || ''
    # if carrier.instance_of?(Array)
    #   $log.info "carrier_status_change #{carrier.inspect}, #{status.inspect}"
    #   res = @manager.TxCassetteStatusMultiChangeRpt(@_user, status, oids(carrier), memo)
    #   return rc(res)
    # end
    # # single carrier
    carrierID = carrier.instance_of?(String) ? oid(carrier) : carrier
    $log.info "carrier_status_change #{carrierID.identifier.inspect}, #{status.inspect}" unless params[:silent]
    if params[:check]
      csraw = carrier_status(carrierID, raw: true) || return
      if csraw.cassetteStatusInfo.cassetteStatus == status
        $log.debug {"  carrier is already #{status}, no change"}
        return 0
      end
    end
    #
    res = @manager.TxCassetteStatusChangeRpt(@_user, carrierID, status, memo)
    return rc(res)
  end

  # reserve carriers, return 0 on success
  def carrier_reserve(carriers, params={})
    memo = params[:memo] || ''
    lot = params[:lot] || ''
    eqp = params[:eqp] || ''
    carrierIDs = _as_objectIDs(carriers)
    $log.info "carrier_reserve #{carrierIDs.collect {|e| e.identifier}}"
    eqpID = eqp.instance_of?(String) ? oid(eqp) : eqp
    lotID = lot.instance_of?(String) ? oid(lot) : lot
    cc = carrierIDs.collect {|cID| @jcscode.pptRsvLotCarrier_struct.new(lotID, cID, eqpID, any)}
    #
    res = @manager.TxLotCassetteReserveReq(@_user, cc, memo)
    return rc(res)
  end

  # cancel carrier reservations, return 0 on success
  def carrier_reserve_cancel(carriers, params={})
    memo = params[:memo] || ''
    lot = params[:lot] || ''
    eqp = params[:eqp] || ''
    carrierIDs = _as_objectIDs(carriers)
    $log.info "carrier_reserve_cancel #{carrierIDs.collect {|e| e.identifier}}" unless params[:silent]
    eqpID = eqp.instance_of?(String) ? oid(eqp) : eqp
    lotID = lot.instance_of?(String) ? oid(lot) : lot
    cc = carrierIDs.collect {|cID|
      ($log.debug {"  carrier #{cID.identifier} has no reservation"}; next) if params[:check] && carrier_status(cID, params).xfer_user.empty?
      @jcscode.pptRsvCanLotCarrier_struct.new(lotID, cID, eqpID, any)
    }.compact
    #
    res = @manager.TxLotCassetteReserveCancelReq(@_user, cc, memo)
    return rc(res)
  end

  # change transfer status of a carrier, e.g. "EO", return 0 on success
  def carrier_xfer_status_change(carrier, state, eqp, params={})
    carrierID = carrier.instance_of?(String) ? oid(carrier) : carrier
    eqpID = eqp.instance_of?(String) ? oid(eqp) : eqp
    $log.info "carrier_xfer_status_change #{carrierID.identifier.inspect}, #{state.inspect}, #{eqpID.identifier.inspect}"
    $log.info {"  #{params}"} unless params.empty?
    ($log.warn '  xfer_state EI is not allowed'; return) if state == 'EI' && !params[:forceEI]
    manualin = !!params[:manualin]
    port = params[:port] || ''
    portID = port.instance_of?(String) ? oid(port) : port
    #
    res = @manager.TxLotCassetteXferStatusChangeRpt(@_user, carrierID, state, manualin, eqpID, portID, '', '', '')
    return rc(res)
  end

  # return strCarrierJobResult for a carrier (or all if '*') or nil on error
  def carrier_xferjob(carrier, params={})
    carrierID = carrier.instance_of?(String) ? oid(carrier) : carrier
    inquiryType = params[:inquiryType] || 'C'
    detailFlag = (params[:detailFlag] != false)
    #
    res = @manager.TxLotCassetteXferJobInq(@_user, detailFlag, inquiryType, carrierID)
    return if rc(res) != 0
    return res.strJobResult
  end

  # return strCarrierJobResult for a carrier (or all if '*'), was: {mcs: true} or nil on error
  def carrier_xferjob_detail(carrier, params={})
    carrierID = carrier.instance_of?(String) ? oid(carrier) : carrier
    inquiryType = params[:inquiryType] || 'C'
    detailFlag = (params[:detailFlag] != false)
    #
    res = @manager.TxLotCassetteXferJobDetailInq(@_user, detailFlag, inquiryType, carrierID, 'INQUIRY')
    return if rc(res) != 0
    return res.strJobResult
  end

  # delete xfer job for a carrier, return 0 on success
  def carrier_xferjob_delete(carrier, params={})
    carrierID = carrier.instance_of?(String) ? oid(carrier) : carrier
    $log.info "carrier_xferjob_delete #{carrierID.identifier.inspect}"
    memo = params[:memo] || ''
    jobs = carrier_xferjob(carrierID) || return
    return 0 if jobs.empty?
    jobid = jobs.first.jobID  # there is only one
    delcjobs = jobs.first.strCarrierJobResult.collect {|cj|
      @jcscode.pptDelCarrierJob_struct.new(cj.carrierJobID, cj.carrierID, any)
    }
    #
    res = @manager.TxLotCassetteXferJobDeleteReq(@_user, jobid, delcjobs, memo)
    return rc(res)
  end

  # transfer cassettes from source eqp to dest eqp, both optionally
  #
  # if one carrier is given TxSingleCarrierXferReq is used, else TxMultiCarrierXferReq
  #
  #   with a port (machine:port). params include reroute, and multicarrier: true to enforce
  #   the use of TxMultiCarrierXferReq for single carriers
  #
  # TxSingle.. is used for eqp->stocker, TxMulti.. for stocker->eqp transfer
  #
  # TxSingle.. returns 4155 if called for transfers _to_ eqp, TxMulti.. checks for AccessMode Auto
  #
  # return 0 on success
  def carrier_xfer(carriers, frommachine, fromport, tomachine, toport, params={})
		transportType = params[:transport] || 'B' # S=Single, B=Batch, L=Load/Unload, see MRM LGS1-4
    starttime = params[:starttime] || ''
    endtime = params[:endtime] || ''
		mandatory = !!params[:mandatory]
    priority = params[:priority] || ''
    n2purgeflag = !!params[:n2purgeflag]
		stockerGroup = params[:stockergroup] || ''
    reroute = !!params[:reroute]
    multi = params[:multicarrier]
    #
    carriers = [carriers] if carriers.instance_of?(String)
    frommachine = [frommachine] if frommachine.instance_of?(String)
    fromport = [fromport] if fromport.instance_of?(String)
    tomachine = [tomachine] if tomachine.instance_of?(String)
    toport = toport.split if toport.instance_of?(String)
    ($log.warn 'missing tomachine'; return) unless tomachine && tomachine.size > 0
    if multi || (carriers.size > 1 && multi != false)
      $log.info "carrier_xfer (multicarrier)"
      xferrq = carriers.each_with_index.collect {|c, i|
        fm = frommachine[i] || frommachine[0] || ''
        fp = fromport[i] || fromport[0] || ''
        tm = tomachine[i] || tomachine[0] || ''
        tp = toport[i] || toport[0] || ''
        $log.info "  #{c} from #{fm}:#{fp} to #{tm}:#{tp}"
        m = [@jcscode.pptToMachine_struct.new(oid(tm), oid(tp), any)].to_java(@jcscode.pptToMachine_struct)
        @jcscode.pptCarrierXferReq_struct.new(oid(c), oid(''), '', n2purgeflag, oid(fm), oid(fp),
          stockerGroup, m, starttime, endtime, mandatory, priority, any)
      }
      res = @manager.TxMultiCarrierXferReq(@_user, reroute, transportType, xferrq)
    else
      c = carriers[0]
      fm = frommachine[0] || ''
      fp = fromport[0] || ''
      tm = tomachine[0] || ''
      tp = toport[0] || ''
      $log.info "carrier_xfer (single) #{c} from #{fm}:#{fp} to #{tm}:#{tp}"
      m = [@jcscode.pptToMachine_struct.new(oid(tm), oid(tp), any)].to_java(@jcscode.pptToMachine_struct)
      res = @manager.TxSingleCarrierXferReq(@_user, reroute, oid(c), oid(''), '', n2purgeflag,
        oid(fm), oid(fp), stockerGroup, m, starttime, endtime, mandatory, priority)
    end
    return rc(res)
  end

  # wait for transport jobs of the carriers or reticle pods to complete, return true on success or false if timed out
  def wait_carrier_xfer(carriers, params={})
    carrierIDs = _as_objectIDs(carriers)
    $log.info "wait_carrier_xfer #{carrierIDs.collect {|e| e.identifier}}"
    # return wait_for(timeout: params[:timeout]) {
    #   carrierIDs.collect {|cID| carrier_xferjob(cID)}.flatten.empty?
    # }
    tend = Time.now + (params[:timeout] || 300)
    loop {
      return true if carrierIDs.collect {|cID| carrier_xferjob(cID)}.flatten.empty?
      return false if Time.now >= tend
      sleep 10
    }
  end

  # reset the carrier's usage counter, core only, return 0 on success
  def carrier_usage_reset(carrier, params={})
    carrierID = carrier.instance_of?(String) ? oid(carrier) : carrier
    $log.info "carrier_usage_reset #{carrierID.identifier.inspect}"
    res = @manager.TxCassetteUsageCountResetReq(@_user, carrierID, params[:memo] || '')
    return rc(res)
  end

  # reset carrier counters (default: all) except if passed false for :filter, :gasket and/or :opestart
  # also resets FRCAST.TIMES_USED to 0
  # return 0 on success
  def carrier_counter_reset(carrier, params={})
    carrierID = carrier.instance_of?(String) ? oid(carrier) : carrier
    $log.info "carrier_counter_reset #{carrierID.identifier.inspect}, #{params}"
    filter = params[:filter] != false
    gasket = params[:gasket] != false
    opestart = params[:opestart] != false
    #
    res = @manager.CS_TxCarrierCounterResetReq(@_user, carrierID, filter, gasket, opestart, params[:memo] || '')
    return rc(res)
  end

  # change the condition of a carrier to Dirty, Used, Cleaned, Due, return 0 on success
  def carrier_condition_change(carrier, condition, params={})
    carrierID = carrier.instance_of?(String) ? oid(carrier) : carrier
    $log.info "carrier_condition_change #{carrierID.identifier.inspect}, #{condition.inspect}"
    #
    res = @manager.CS_TxCassetteConditionChangeReq(@_user, carrierID, condition, params[:memo] || '')
    return rc(res)
  end

  # list carrier categories with maintenance information, currently used internally only
  def carrier_category_list(params={})
    withinfo = !params[:all]  # default: only carrier categories with maintenance info
    res = @manager.CS_TxCarrierCategoryListInq(@_user, withinfo)
    return nil if rc(res) != 0
    res.strCarrierCategorySequence.map {|cc| cc.CarrierCategoryID}
  end

  # add, change or delete carrier maintenance limits for the carrier category, return 0 on success
  def carrier_category_change(category, params={})
    $log.info "carrier_category_change #{category.inspect}, #{params}"
    # changetype: 1 - create, 2 - modify, 3 - delete
    changetype = params[:changetype] || (carrier_category_list.member?(category) ? 2 : 1)
    # opestart
    cwarn = params[:op_warning] || 0 # per default disabled
    cdue = params[:op_due] || cwarn
    cinhibit = params[:op_inhibit] || cdue
    # filter
    fwarn = params[:filter_op_warning] || 0 # per default disabled
    fdue = params[:filter_op_due] || fwarn
    finhibit = params[:filter_op_inhibit] || fdue
    # gasket
    gwarn = params[:gasket_op_warning] || 0 # per default disabled
    gdue = params[:gasket_op_due] || gwarn
    ginhibit = params[:gasket_op_inhibit] || gdue
    # filter time
    ftwarn = params[:filter_time_warn] || 0
    ftdue = params[:filter_time_due] || ftwarn
    ftinhibit = params[:filter_time_inhibit] || ftdue
    # gasket time
    gtwarn = params[:gasket_time_warn] || 0
    gtdue = params[:gasket_time_due] || gtwarn
    gtinhibit = params[:gasket_time_inhibit] || gtdue
    #
    catinfo = @jcscode.pptCS_CarrierCategoryInfo_struct.new(fwarn, fdue, finhibit, ftwarn, ftdue, ftinhibit,
      gwarn, gdue, ginhibit, gtwarn, gtdue, gtinhibit, cwarn, cdue, cinhibit, oid(''), oid(''), '', any)
    #
    res = @manager.CS_TxCarrierCategoryInfoChgReq(@_user, oid(category), changetype, catinfo)
    return rc(res)
  end

end
