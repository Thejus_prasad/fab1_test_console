=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.


History:
  2016-04-14 sfrieske, fixed SMClasses reticleset
  2021-12-16 sfrieske, fixed SMClasses userdefinedcode
  2022-01-07 sfrieske, fixed SMClasses recipe, scriptparameter etc.
=end


class SiView::SM
  ObjectInfo = Struct.new(:name, :description, :state, :permission, :owner, :udata, :specific, :log)
  MainpdOpInfo = Struct.new(:opNo, :op, :mnumber, :mname, :stage, :photolayer, :carrier_category,
      :pre1, :lagtime, :qtimes, :branches, :reworks, :udata)
  ModulepdOpInfo = Struct.new(:opNo, :op, :mandatory, :pre1, :lagtime, :qtimes, :branches, :reworks)
  QtInfo = Struct.new(:target_opNo, :action_opNo, :duration, :action, :reason, :rework, :timing, :custom)

  # SM classid and fieldid for primary search field (object name), see table BRCLASSFIELDINFO
  SMClasses = {
    entity:             ['EN', 'BR_FIELD_ENTITY_ENTITYID'],
    documententity:     ['DE', 'BR_FIELD_DOCUMENTENTITY_DOCUMENTENTITYID'],
    changenotice:       ['CN', 'BR_FIELD_CHANGENOTICE_CHANGENOTICENUMBER'],
    common:             ['CL', 'BR_FIELD_COMMON_COMMONID'],

    user:               ['SC01', 'BR_FIELD_USER_USERID'],
    areagroup:          ['SC04', 'BR_FIELD_AREAGROUP_AREAGROUPID'],
    usergroup:          ['SC07', 'BR_FIELD_ENTITY_IDENTIFIER'],
    privilegegroup:     ['SC10', 'BR_FIELD_PRIVILEGEGROUP_PRIVILEGEGROUPID'],
    company:            ['SC13', 'BR_FIELD_COMPANY_COMPANYID'],

    location:           ['FA01', 'BR_FIELD_LOCATION_LOCATIONID'],
    workarea:           ['FA02', 'BR_FIELD_WORKAREA_WORKAREAID'],
    department:         ['FA03', 'BR_FIELD_DEPARTMENT_DEPARTMENTID'],
    bank:               ['FA04', 'BR_FIELD_BANK_BANKID'],
    mfglayer:           ['FA05', 'BR_FIELD_MFGLAYER_MFGLAYERID'],
    stagegroup:         ['FA06', 'BR_FIELD_STAGEGROUP_STAGEGROUPID'],
    stage:              ['FA07', 'BR_FIELD_STAGE_STAGEID'],
    calendar:           ['FA08', 'BR_FIELD_CALENDAR_CALENDARID'],

    eqp:                ['EQ01', 'BR_FIELD_EQUIPMENT_EQUIPMENTID'],
    equipmentcategory:  ['EQ02', 'BR_FIELD_EQUIPMENTCATEGORY_EQUIPMENTCATEGORYID'],
    whatsnext:          ['EQ03', 'BR_FIELD_WHATSNEXT_WHATSNEXTID'],
    wherenext:          ['EQ04', 'BR_FIELD_WHERENEXT_WHERENEXTID'],
    loadpurposetype:    ['EQ05', 'BR_FIELD_LOADPURPOSETYPE_LOADPURPOSETYPEID'],
    operationmode:      ['EQ06', 'BR_FIELD_OPERATIONMODE_OPERATIONMODEID'],
    eqptype:            ['EQ07', 'BR_FIELD_EQUIPMENTTYPE_EQUIPMENTTYPEID'],
    stocker:            ['EQ08', 'BR_FIELD_STOCKER_STOCKERID'],
    stockercategory:    ['EQ09', 'BR_FIELD_STOCKERCATEGORY_STOCKERCATEGORYID'],
    lrcp:               ['EQ10', 'BR_FIELD_LOGICALRECIPE_LOGICALRECIPEID'],
    recipe:             ['EQ11', 'BR_FIELD_RECIPE_RECIPEID'],
    dcdefinition:       ['EQ12', 'BR_FIELD_DCDEFINITION_DCDEFINITIONID'],
    dcspecification:    ['EQ13', 'BR_FIELD_DCSPECIFICATION_DCSPECIFICATIONID'],
    calculationtype:    ['EQ14', 'BR_FIELD_CALCULATIONTYPE_CALCULATIONTYPEID'],
    actioncode:         ['EQ15', 'BR_FIELD_ACTIONCODE_ACTIONCODEID'],
    binspecification:   ['EQ16', 'BR_FIELD_BINSPECIFICATION_BINSPECIFICATIONID'],
    bindefinition:      ['EQ17', 'BR_FIELD_BINDEFINITION_BINDEFINITIONID'],
    testspecification:  ['EQ18', 'BR_FIELD_TESTSPECIFICATION_TESTSPECIFICATIONID'],
    testtype:           ['EQ19', 'BR_FIELD_TESTTYPE_TESTTYPEID'],
    samplespec:         ['EQ20', 'BR_FIELD_SAMPLESPEC_SAMPLESPECID'],
    setup:              ['EQ21', 'BR_FIELD_SETUP_SETUPID'],

    technology:         ['PR01', 'BR_FIELD_TECHNOLOGY_TECHNOLOGYID'],
    productgroup:       ['PR02', 'BR_FIELD_PRODUCTGROUP_PRODUCTGROUPID'],
    product:            ['PR03', 'BR_FIELD_PRODUCT_PRODUCTID'],
    producttype:        ['PR04', 'BR_FIELD_PRODUCTTYPE_PRODUCTTYPEID'],
    productcategory:    ['PR05', 'BR_FIELD_PRODUCTCATEGORY_PRODUCTCATEGORYID'],
    lottype:            ['PR06', 'BR_FIELD_LOTTYPE_LOTTYPEID'],
    priorityclasstype:  ['PR07', 'BR_FIELD_PRIORITYCLASSTYPE_PRIORITYCLASSTYPEID'],
    customer:           ['PR10', 'BR_FIELD_CUSTOMER_CUSTOMERID'],
    customerproduct:    ['PR11', 'BR_FIELD_CUSTOMERPRODUCT_CUSTOMERPRODUCTID'],
    bom:                ['PR13', 'BR_FIELD_BOM_BOMID'],

    pd:                   ['PC01', 'BR_FIELD_PROCESSDEFINITION_PROCESSDEFINITIONID'],
    modulepd:             ['PC02', 'BR_FIELD_MODULEPROCESSDEFINITION_MODULEPROCESSDEFINITIONID'],
    mainpd:               ['PC03', 'BR_FIELD_MAINPROCESSDEFINITION_MAINPROCESSDEFINITIONID'],
    mainpddefinitiontype: ['PC04', 'BR_FIELD_MAINPROCESSDEFINITIONTYPE_MAINPROCESSDEFINITIONTYPEID'],
    inspectiontype:       ['PC05', 'BR_FIELD_INSPECTIONTYPE_INSPECTIONTYPEID'],
    deltadcdefinition:    ['PC06', 'BR_FIELD_DELTADCDEFINITION_DELTADCDEFINITIONID'],
    deltadcspecification: ['PC07', 'BR_FIELD_DELTADCSPECIFICATION_DELTADCSPECIFICATIONID'],
    photolayer:           ['PC08', 'BR_FIELD_PHOTOLAYER_PHOTOLAYERID'],

    fixture:              ['DR01', 'BR_FIELD_FIXTURE_FIXTUREID'],
    fixturegroup:         ['DR02', 'BR_FIELD_FIXTUREGROUP_FIXTUREGROUPID'],
    fixturecategory:      ['DR03', 'BR_FIELD_FIXTURECATEGORY_FIXTURECATEGORYID'],
    reticle:              ['DR04', 'BR_FIELD_RETICLE_RETICLEID'],
    reticlegroup:         ['DR05', 'BR_FIELD_RETICLEGROUP_RETICLEGROUPID'],
    carrier:              ['DR06', 'BR_FIELD_CARRIER_CARRIERID'],
    carriercategory:      ['DR07', 'BR_FIELD_CARRIERCATEGORY_CARRIERCATEGORYID'],
    reticlepod:           ['DR08', 'BR_FIELD_RETICLEPOD_RETICLEPODID'],
    reticlepodcategory:   ['DR09', 'BR_FIELD_RETICLEPODCATEGORY_RETICLEPODCATEGORYID'],
    reticleset:           ['DR10', 'BR_FIELD_RETICLESET_FIELD_RETICLESETID'],

    reasoncategory:       ['CS01', 'BR_FIELD_REASONCATEGORY_REASONCATEGORYID'],
    reasoncode:           ['CS02', 'BR_FIELD_REASONCODE_REASONCATEGORY'],
    systemmessagecode:    ['CS03', 'BR_FIELD_SYSTEMMESSAGECODE_SYSTEMMESSAGECODEID'],
    msgdistribution:      ['CS04', 'BR_FIELD_MESSAGEDISTRIBUTION_MESSAGEDISTRIBUTIONID'],
    msgmedia:             ['CS05', 'BR_FIELD_MESSAGEMEDIA_MESSAGEMEDIAID'],
    msgdistributiontype:  ['CS06', 'BR_FIELD_MESSAGEDISTRIBUTIONTYPE_MESSAGEDISTRIBUTIONTYPEID'],
    script:               ['CS07', 'BR_FIELD_SCRIPT_SCRIPTID'],
    scriptparameter:      ['CS08', 'BR_FIELD_SCRIPTPARAMETER_CLASSNAME'],           # e.g. Wafer.DieCountGood
    equipmentstate:       ['CS09', 'BR_FIELD_EQUIPMENTSTATE_E10STATE'],             # e.g. SBY.2WPR
    rawequipmentstateset: ['CS10', 'BR_FIELD_RAWEQUIPMENTSTATESET_RAWEQUIPMENTSTATESETID'],
    e10state:             ['CS11', 'BR_FIELD_ENTITY_IDENTIFIER'],
    userdefinedcategory:  ['CS12', 'BR_FIELD_USERDEFINEDCATEGORY_USERDEFINEDCATEGORYID'],
    userdefinedcode:      ['CS13', 'BR_FIELD_USERDEFINEDCODE_USERDEFINEDCATEGORY'], # e.g. ProgramID.28LS

    # eventlog: ['LG01','BR_FIELD_EVENTLOG_EVENTLOGID'],
    # releaselog: ['LG02','BR_FIELD_RELEASELOG_RELEASELOGID'],
    # setting: ['LG03','BR_FIELD_SETTING_SETTINGID'],
    # logsetting: ['LG04','BR_FIELD_LOGSETTING_LOGSETTINGID'],

    # managerfactory: ['OF01','BR_FIELD_MANAGERFACTORY_MANAGERFACTORYID'],
    # logfactory: ['OF02','BR_FIELD_LOGFACTORY_LOGFACTORYID'],
    # servicemanagerfactory: ['OF03','BR_FIELD_SERVICEMANAGERFACTORY_SERVICEMANAGERFACTORYID'],
    # productionmanagerfactory: ['OF04','BR_FIELD_PRODUCTIONMANAGERFACTORY_PRODUCTIONMANAGERFACTORYID'],
    # objectfactory: ['OF05','BR_FIELD_OBJECTFACTORY_OBJECTFACTORYID'],
    # logmanagerobjectfactory: ['OF06','BR_FIELD_LOGMANAGEROBJECTFACTORY_LOGMANAGEROBJECTFACTORYID'],
    # querymanagerfactory: ['OF07','BR_FIELD_QUERYMANAGERFACTORY_QUERYMANAGERFACTORYID'],

    # manager: ['MG01','BR_FIELD_MANAGER_MANAGERID'],
    # logmanager: ['MG02','BR_FIELD_LOGMANAGER_LOGMANAGERID'],
    # servicemanager: ['MG03','BR_FIELD_SERVICEMANAGER_SERVICEMANAGERID'],
    # productionmanager: ['MG04','BR_FIELD_PRODUCTIONMANAGER_PRODUCTIONMANAGERID'],
    # querymanager: ['MG05','BR_FIELD_QUERYMANAGER_QUERYMANAGERID'],

    # relatedobject: ['RP01','BR_FIELD_RELATEDOBJECT_RELATEDOBJECTID'],
    # processflow: ['RP02','BR_FIELD_PROCESSFLOW_PROCESSFLOWID'],

    # systemfreeze: ['OP01','BR_FIELD_SYSTEMFREEZE_SYSTEMFREEZEID'],
    # productionrelease: ['OP02','BR_FIELD_PRODUCTIONRELEASE_PRODUCTIONRELEASEID'],
  }

  # for search in object_ids, eg. object_ids(:reticle, :all, search: {reticlegroup: 'XXX'}
  # and regular search for objects with special naming conventions like recipe, reasoncode, ..
  SMSearchFields = {
    equipmentstate: {code: 'BR_FIELD_EQUIPMENTSTATE_EQUIPMENTSTATECODE'},
    product: {
      ec: 'BR_FIELD_PRODUCT_EC',
      mfglayer: 'BR_FIELD_PRODUCT_MFGLAYER',
      srcproducts: 'BR_FIELD_PRODUCT_SOURCEPRODUCTS'
    },
    recipe: {namespace: 'BR_FIELD_RECIPE_RECIPENAMESPACE'},
    reticle: {reticlegroup: 'BR_FIELD_RETICLE_RETICLEGROUP'},
    reasoncode: {code: 'BR_FIELD_REASONCODE_REASONCODE'},
    scriptparameter: {name: 'BR_FIELD_SCRIPTPARAMETER_PARAMETERNAME'},
    userdefinedcode: {code: 'BR_FIELD_USERDEFINEDCODE_USERDEFINEDCODE'}
  }

end
