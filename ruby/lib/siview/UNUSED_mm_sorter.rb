=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.
=end


class SiView::MM
  # SjJob = Struct.new(:jobid, :sorter, :pg, :user, :status, :waferid_read, :components)
  # SjComponent = Struct.new(:componentid, :src_port, :tgt_port, :src_carrier, :tgt_carrier, :status, :wafers)

  # check if it's possible to split the lot for separate, return 0 on success, UNUSED
  def sj_check_condition(sorter, srcport, tgtport, lots, tgt_carriers, params={})
    $log.info "sj_check_condition #{sorter} #{params}"
    memo = params[:memo] || ''
    pg = (srcport == '*') ? '*' : eqp_info(sorter).ports.find {|p| p.port == srcport}.pg
    return unless pg
    joblist = _create_sj_joblist(sorter, pg, srcport, tgtport, lots, tgt_carriers, params)
    #
    res = @manager.TxSortJobCheckConditionReq(@_user, joblist, oid(sorter), pg, memo)
    return rc(res)
  end

  #
  # AdHoc WaferSorter Operations
  #

  # execute a WaferSorter action, return 0 on success
  # actions: WaferIDRead, WaferIDMiniRead, PositionChange, JustIn, JustOut, ScrapWafer, AdjustToMM, AdjustToSorter, SP_Sorter_End
  #
  # either :lot=>lot or :carrier=>carrier must be passed as parameter, UNUSED
  def sorter_action(eqp, srcport, tgtport, action, params)
    $log.info "sorter_action '#{eqp}', '#{srcport}', '#{tgtport}', #{action.inspect}, #{params}"
    carrier = params[:carrier] || ''
    lot = params[:lot] || ''
    tgt_carrier = params[:tgt_carrier] || ''
    memo = params[:memo] || ''
    direction = 'MM'
    request_time = ''
    request_user = params[:user] ? oid(params[:user]) : _user.userID
    reply_time = ''
    sorter_status = 'SP_Sorter_Requested'  #"SP_Sorter_Succeeded"
    slotmap_compare_status = ''
    mm_compare_status = ''
    pg = params[:pg] || eqp_info(eqp).ports.find {|p| p.port == srcport}.pg || return
    # prepare slotmap
    if action == 'WaferIDRead'
      map = [@jcscode.pptWaferSorterSlotMap_struct.new(
        pg, oid(eqp), action, request_time, direction, oid(''), oid(lot), 
        oid(carrier), oid(tgtport), !carrier.empty?, 0,
        oid(carrier), oid(srcport), !carrier.empty?, 0, 
        request_user, reply_time, sorter_status, slotmap_compare_status, mm_compare_status, any)
      ]
    else
      slotmap = _create_slotmap(lot, tgt_carrier, params)  # pptWaferTransfer_struct
      map = slotmap.collect {|s|
        @jcscode.pptWaferSorterSlotMap_struct.new(
          pg, oid(eqp), action, request_time, direction, s.waferID, oid(lot), 
          s.destinationCassetteID, oid(tgtport), s.bDestinationCassetteManagedBySiView, s.destinationSlotNumber,
          s.originalCassetteID, oid(srcport), s.bOriginalCassetteManagedBySiView, s.originalSlotNumber, 
          request_user, reply_time, sorter_status, slotmap_compare_status, mm_compare_status, any)
      }
    end
    #
    res = @manager.TxWaferSorterOnEqpReq(@_user, action, oid(eqp), map, pg, action, memo)
    return rc(res)
  end

end
