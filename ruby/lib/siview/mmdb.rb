=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author:     ssteidte, 2013-01-07

Version:    1.1.3

History:
  2013-01-15 ssteidte, added query for MDS SiView history instead of SiView
  2014-07-14 dsteger,  added reticle related queries
  2016-07-06 sfriesek, added free_ull_sequence
  2017-03-20 sfrieske, DB.new uses :schema from databases.conf
  2020-02-05 sfrieske, commented out unused methods and structs
  2021-08-30 sfrieske, moved siviewmmdb.rb to siview/mmdb.rb
  2021-12-09 sfrieske, simplified user_parameter_history query
=end

require 'dbsequel'
require 'siview'  # for ULL sequence calculations in free_ull_sequence


module SiView

  # SiView MM DB queries
  class MMDB
    attr_accessor :db

    def initialize(env, params={})
      @db = params[:db] || ::DB.new("#{params[:use_mds] ? 'mds' : 'siviewmm'}.#{env}")
    end

    # return array of records or nil on error
    def user_parameter_history(params={})
      qargs = []
      q = 'SELECT PARAMETER_CLASS, IDENTIFIER, CHANGE_TYPE, PARAMETER_NAME, DATA_TYPE, KEY_VALUE, VALUE, VALUE_FLAG,
        DESCRIPTION, EVENT_TIME from FHBRPCHS'
      q, qargs = @db._q_restriction(q, qargs, 'EVENT_TIME >=', params[:tstart])
      q, qargs = @db._q_restriction(q, qargs, 'EVENT_TIME <=', params[:tend])
      q, qargs = @db._q_restriction(q, qargs, 'IDENTIFIER', params[:id])
      q, qargs = @db._q_restriction(q, qargs, 'PARAMETER_NAME', params[:name])
      q, qargs = @db._q_restriction(q, qargs, 'VALUE', params[:value])
      q, qargs = @db._q_restriction(q, qargs, 'CLAIM_USER_ID', params[:user])
      q += ' ORDER BY EVENT_TIME DESC'
      res = @db.select(q, *qargs) || return
      return res.collect {|r| {id: r[1], name: r[3], value: r[6], event_time: r[9]}}
    end

    # search for carriers with defective NPW reservation (R15), for troubleshooting
    def carrier_npw(carrier)
      q, qargs = @db._q_restriction("SELECT CAST_ID from FRCAST WHERE NPW_LDPURPOSE_TYPE != ''", [], 'CAST_ID', carrier)
      q += ' ORDER BY CAST_ID'
      res = @db.select(q, *qargs) || return
      return res.flatten
    end

    # get the next unused ULL family, for MM_Lot_Label
    def free_ull_sequence(params={})
      plantcode = params[:plantcode] || 'T'
      sequence = params[:sequence] || '0000'
      q1 = 'SELECT count(*) from FSULLFAMILY_CTRL where ULLFAMILY_ID = ?'
      q2 = "SELECT count(*) from FRLOT_UDATA where NAME='LOT_LABEL' and VALUE like ?"
      while @db.select(q1, "#{sequence}#{plantcode}").first.first > 0 || @db.select(q2, "#{sequence}#{plantcode}%").first.first > 0
        sequence = SiView.siview_next(sequence)
      end
      return sequence
    end

    # next ULL sequence in OwnFab, for MM_Lot_Label
    def ull_sequence
      return @db.select("SELECT SEQ_NO from FSLOTLABEL WHERE LOT_LABEL='ULL'").first.first
    end

    # def fsxregei(params={})
    #   qargs = []
    #   q = 'SELECT EI_NAME, MT_MGR_IOR, FO_MGR_IOR, STATUS, LASTUSED from FSXREGEI_MULTIPLE_MCS'
    #   q, qargs = @db._q_restriction(q, qargs, 'EI_NAME >=', params[:name])
    #   q, qargs = @db._q_restriction(q, qargs, 'STATUS', params[:status])
    #   q += ' ORDER BY MCSZONE, EI_NAME'
    #   res = @db.select(q, *qargs) || return
    #   return res.collect {|r| FSXREGEI.new(*r)}
    # end

    # def fsxregei_multi(params={})  # meanwhile a duplicate of fsxregei ??
    #   qargs = []
    #   q = 'SELECT EI_NAME, MT_MGR_IOR, FO_MGR_IOR, STATUS, MCSZONE, LASTUSED from FSXREGEI_MULTIPLE_MCS'
    #   q, qargs = @db._q_restriction(q, qargs, 'EI_NAME >=', params[:name])
    #   q, qargs = @db._q_restriction(q, qargs, 'MCSZONE <=', params[:zone])
    #   q, qargs = @db._q_restriction(q, qargs, 'STATUS', params[:status])
    #   q += ' ORDER BY MCSZONE, EI_NAME'
    #   res = @db.select(q, *qargs) || return
    #   return res.collect {|r| FSXREGEI_MULTI.new(*r)}
    # end

    # def ligcalendar(params={})
    #   qargs = []
    #   q = 'SELECT CALENDAR_ID, FISCAL_YEAR, FISCAL_YEAR_CODE, YEAR_START_TIME, YEAR_END_TIME from FSLIGCALENDAR'
    #   q, qargs = @db._q_restriction(q, qargs, 'CALENDAR_ID', params[:calendar])
    #   q, qargs = @db._q_restriction(q, qargs, 'FISCAL_YEAR', params[:year])
    #   q += ' ORDER BY CALENDAR_ID, FISCAL_YEAR'
    #   res = @db.select(q, *qargs) || return
    #   return res if params[:raw]
    #   return res.collect {|r| CalendarEntry.new(*r)}
    # end

  end

end
