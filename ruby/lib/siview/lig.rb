=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.


Author:     Conny Stenzel, 2010-12-06, ssteidte, 2012-11-30
Version:    1.3.5

History:
  2012-12-30 ssteidte, created as separate library for better debugging
  2013-07-18 ssteidte, runs with Ruby 1.9
  2013-10-15 ssteidte, use 2013 calendar for special checks
  2014-01-27 dsteger,  updated samsung lot id generation
  2015-05-08 ssteidte, changed vendor lot naming for R15
  2016-02-08 sfrieske, inherit from SiView::Test, cleanup
  2016-03-08 dsteger,  Fab7 sublottypes and AMD SEC lot numbering
  2016-08-03 sfrieske, changed amd-secqa splitseq to Numeric, added :onhold parameters for split tests
  2017-10-05 sfrieske, moved currently unused DB calendar query to siviewmm
  2019-04-08 sfrieske, use customer qaamd instead of amd
  2019-12-18 sfrieske, create all Production lots at once, significant speed up
  2021-02-09 sfrieske, fixed use of stb_controllot
  2021-12-14 sfrieske, removed unused srcproduct and startbank, except in ControlLot
=end

require 'siviewtest'


# verify customer specific lot ID creation
module LIG
  CustomerStruct = Struct.new(
    :calendar, :fabId, :customerPrefix,
    :yearCodeType, :yearCodeLength, :yearCodeExcludedChars,
    :monthCodeType, :monthCodeLength, :monthCodeExcludedChars,
    :workWeekType, :workWeekLength, :workWeekFirst, :workWeekLast,
    :fabPrefix,
    :runSeqType, :runSeqLength, :runSeqExcludedChars,
    :splitSeqType, :splitSeqLength, :splitSeqExcludedChars,
    :regex_runseq, :regex_splitseq_parent, :regex_splitseq_child, :child_offset,
    :special_starttimes, :expected_wws, :expected_months, :expected_years,
    :xsublottypes, :sec_lots
  )

  CustomerSettings = {
    # GF (default):
    #
    'gf'=>CustomerStruct.new('DEFAULT', 'U', 'XY',
      'Alpha', 1, %w(I O),
      '', 0, [],
      'Numeric', 2, 1, 53,
      '',
      'NumericThenAlphanumeric', 3, %w(I O),
      'Numeric', 3, [],
      'UXY[A-HJ-NP-Z](0[1-9]|[1-4][0-9]|5[0-3])[0-9A-HJ-NP-Z]{3}', '000', '[0-9A-HJ-NP-Z]{3}', 0,
      ["2013-01-01-00.00.00.000000", "2013-01-02-10.00.00.000000", "2013-01-03-10.00.00.000000",
       "2013-01-08-10.00.00.000000", "2013-01-09-10.00.00.000000", "2013-12-31-23.59.59.000000"],
      %w(01 01 01 02 02 53), [], %w(K K K K K K)
    ),
    # AMD:
    # Good examples: U0001.00, U9ZZZ.00, U9ZZZ.01
    # Bad examples: U9IZZ.01, U9OZZ.01
    'amd'=>CustomerStruct.new('CUSTOM2', 'U', '',   # calendar was: AMD
      'Numeric', 1, [],
      '', 0, [],
      '', 0, 0, 0,
      '',
      'Alphanumeric', 3, %w(I O),
      'Alphanumeric', 2, %w(I O),
      'U[0-9][0-9A-HJ-NP-Z]{3}', '00', '[0-9A-HJ-NP-Z]{2}', 0,
      ["2013-01-01-00.00.00.000000", "2013-01-02-10.00.00.000000", "2013-01-03-10.00.00.000000",
       "2013-01-08-10.00.00.000000", "2013-01-09-10.00.00.000000", "2013-12-31-23.59.59.000000"],
      [], [], %w(3 3 3 3 3 4)
    ),
    # AMD-SEC old:
    # Good examples: N0001.1, N9ZZZ.1, U9ZZZ.AA
    # Bad examples: U9IZZ.01, U9OZZ.01
    'amd-secqa-old'=>CustomerStruct.new('AMD', 'N', '',
      'Numeric', 1, [],
      '', 0, [],
      '', 0, 0, 0,
      '',
      'Alphanumeric', 3, %w(I O),
      'Alphanumeric', 2, %w(I O),
      'N[A-HJ-NP-Z][0-9A-HJ-NP-Z]{3}', '1', '[0-9A-HJ-NP-Z]{1,2}', 0,
      ['2013-01-01-00.00.00.000000', '2013-01-02-10.00.00.000000', '2013-01-03-10.00.00.000000',
       '2013-01-08-10.00.00.000000', '2013-01-09-10.00.00.000000', '2013-12-31-23.59.59.000000'],
      [], [], %w(K K K K K K),
      [], true
    ),
    # AMD-SEC (numeric children):
    # Good examples: N0001.1, N9ZZZ.1, U9ZZZ.AA
    # Bad examples: U9IZZ.01, U9OZZ.01
    'amd-secqa'=>CustomerStruct.new('DEFAULT', 'N', '',  # calendar was: AMD
      'Numeric', 1, [],
      '', 0, [],
      '', 0, 0, 0,
      '',
      'Alphanumeric', 3, %w(I O),
      'Numeric', 3, [],
      'N[A-HJ-NP-Z][0-9A-HJ-NP-Z]{3}', '1', '[0-9]{1,3}', 0,
      ['2013-01-01-00.00.00.000000', '2013-01-02-10.00.00.000000', '2013-01-03-10.00.00.000000',
       '2013-01-08-10.00.00.000000', '2013-01-09-10.00.00.000000', '2013-12-31-23.59.59.000000'],
      [], [], %w(K K K K K K),
      [], true
    ),
    # Broadcom (same as GF except customerPrefix):
    # Good examples: UBCA01001.000, UBCA01001.001, UBCZ5309Z.059, UBCZ53ZZZ.999
    # Bad examples: UBCA00000.000, UBCZ53ZZZ.ABC
    'broadcom'=>CustomerStruct.new('DEFAULT', 'U', 'BC',
      'Alpha', 1, %w(I O),
      '', 0, [],
      'Numeric', 2, 1, 53,
      '',
      'NumericThenAlphanumeric', 3, %w(I O),
      'Numeric', 3, [],
      'UBC[A-HJ-NP-Z](0[1-9]|[1-4][0-9]|5[0-3])([0-9A-HJ-NP-Z]{3})', '000', '[0-9]{3}', 0,
      [],
      [], [], []
    ),
    # IBM (same as GF except customerPrefix, calendar and splitSeqType):
    # Good examples: UIQA01001.000, UIQA01001.001, UIQZ5309Z.059, UIQZ53ZZZ.999
    # Bad examples: UIQA00000.000, UIQZ53ZZZ.ABC
    'ibm'=>CustomerStruct.new('IBM', 'U', 'IQ',
      'Alpha', 1, %w(I O),
      '', 0, [],
      'Numeric', 2, 1, 53,
      '',
      'NumericThenAlphanumeric', 3, %w(I O),
      'Numeric', 3, %w(I O),
      'UIQ[A-HJ-NP-Z](0[1-9]|[1-4][0-9]|5[0-3])[0-9A-HJ-NP-Z]{3}', '000', '[0-9A-HJ-NP-Z]{3}', 0,
      ['2013-01-01-00.00.00.000000', '2013-01-02-10.00.00.000000', '2013-01-03-10.00.00.000000',
       '2013-01-08-10.00.00.000000', '2013-01-09-10.00.00.000000', '2013-12-31-23.59.59.000000'],
      %w(02 02 02 03 03 02), [], %w(K K K K K L)
    ),
    # Qualcomm (similar to GF, no WW):
    # Good examples: UR00001.000, UR9ZZZZ.000, UR9ZZZZ.001
    # Bad examples: UR9IOIO.000, UR90001.ABC
    'qgt'=>CustomerStruct.new('QCT', 'U', 'R',
      'Numeric', 1, [],
      '', 0, [],
      '', 0, 0, 0,
      '',
      'NumericThenAlphanumeric', 4, %w(I O),
      'Numeric', 3, [],
      'UR[0-9]([0-9A-HJ-NP-Z]{4})', '000', '[0-9]{3}', 0,
      ['2013-01-01-00.00.00.000000', '2013-12-31-23.59.59.000000'],
      [], [], %w(3 3)
    ),
    #  under construction ..
    'mstar'=>CustomerStruct.new('DEFAULT', 'U', 'M',
      'Numeric', 1, [],
      '', 0, [],
      '', 0, 0, 0,
      '',
      'NumericThenAlphanumeric', 4, %w(I O),
      'Numeric', 3, [],
      'UM.*', '000', '.*', 0,
      ['2013-01-01-00.00.00.000000', '2013-12-31-23.59.59.000000'],
      [], [], %w(3 3)
    ),
    # Renesas (month code):
    # Good examples: PU11-000.000, PU11-000.001
    # Bad examples: PU11-IOI.000, PU11-000.ABC
    'renesas'=>CustomerStruct.new('REN', 'U', 'P',
      'Numeric', 1, [],
      'Alphanumeric', 1, '0ABCDEFGHIJKLMNOPQRSTUVW'.chars.to_a,
      '', 0, 0, 0,
      '-',
      'Alphanumeric', 4, %w(I O),
      'Numeric', 3, [],
      'PU[0-9][1-9X-Z]-[0-9A-HJ-NP-Z]{4}', '000', '[0-9]{3}', 0,
      ['2013-01-01-00.00.00.000000', '2013-02-01-00.00.00.000000', '2013-03-01-00.00.00.000000',
       '2013-04-01-00.00.00.000000', '2013-05-01-00.00.00.000000', '2013-06-01-00.00.00.000000',
       '2013-07-01-00.00.00.000000', '2013-08-01-00.00.00.000000', '2013-09-01-00.00.00.000000',
       '2013-10-01-23.59.59.000000', '2013-11-01-23.59.59.000000', '2013-12-31-23.59.59.000000'],
      [], %w(1 2 3 4 5 6 7 8 9 X Y Z), %w(3 3 3 3 3 3 3 3 3 3 3 3)
    ),
    # Samsung:
    # Good examples: G0001.001, G9001.001, GZZZZ.001, GZZZZ.001
    # Bad examples: GIOIO.000, G0001.ABC
    'samsung'=>CustomerStruct.new('NA', '', 'G',
      '', 0, [],
      '', 0, [],
      '', 0, 0, 0,
      '',
      'Alphanumeric', 4, "EIJLMOQSZ".chars.to_a,
      'Numeric', 3, [],
      'G[0-9A-DF-HKNPRT-Y]{4}', '001', '[0-9]{3}', 0,
      [],
      [], [], []
    ),
    # ST Micro:
    # Good examples: FR001001.000, FR952999.000, FR952999.001, FR952123.999
    # Bad examples: FR952AAA.AAA, FR952AAA.000, FR952000.AAA
    'stmicro'=>CustomerStruct.new('STM', '', 'FR',
      'Numeric', 1, [],
      '', 0, [],
      'Numeric', 2, 0, 52,
      '',
      'Numeric', 3, [],
      'Numeric', 3, [],
      'FR[0-9]([0-4][0-9]|5[0-2])[0-9]{3}', '000', '[0-9]{3}', 0,
      ['2013-01-01-00.00.00.000000', '2013-01-02-10.00.00.000000', '2013-01-03-10.00.00.000000',
       '2013-01-08-10.00.00.000000', '2013-01-09-10.00.00.000000', '2013-12-31-23.59.59.000000'],
      %w(01 01 01 02 02 01), [], %w(3 3 3 3 3 4)
    ),
    # Cirrus (Fab7 customer):
    # Good examples: UCX0001.000, UCM0AAA.001, UCMAEAA.023, UCX1435.001
    # Bad examples: UCM00000.000, UDM3ZZZ.ABC
    '7CIRRUS'=>CustomerStruct.new('DEFAULT', 'U', 'C',
      'Alpha', 1, %w(I O),
      '', 0, [],
      '', 0, 0, 0,
      '',
      'NumericThenAlphanumeric3', 4, %w(I O),
      'Numeric', 3, %w(I O),
      'UC[A-HJ-NP-Z][0-9A-DF-HKNPRT-Y]{4}', '000', '[0-9]{3}', 0,
      ['2013-01-01-00.00.00.000000', '2013-01-02-10.00.00.000000', '2013-01-03-10.00.00.000000',
       '2013-01-08-10.00.00.000000', '2013-01-09-10.00.00.000000', '2013-12-31-23.59.59.000000'],
      [], [], %w(K K K K K K),
      %w(EO RD)
    ),
    # Dialog (Fab7 customer):
    # Good examples: UDX001.000, UDMAAA.001, UDMAAA.023, UDXAAA.001
    # Bad examples: UDM00000.000, UDM3ZZZ.ABC
    '7DIALOG'=>CustomerStruct.new('DEFAULT', 'U', 'D',
      'Alpha', 1, %w(I O),
      '', 0, [],
      '', 0, 0, 0,
      '',
      'NumericThenAlphanumeric2', 4, %w(I O),
      'Numeric', 3, %w(I O),
      'UD[A-HJ-NP-Z][0-9A-DF-HKNPRT-Y]{4}', '000', '[0-9]{3}', 0,
      ['2013-01-01-00.00.00.000000', '2013-01-02-10.00.00.000000', '2013-01-03-10.00.00.000000',
       '2013-01-08-10.00.00.000000', '2013-01-09-10.00.00.000000', '2013-12-31-23.59.59.000000'],
      [], [], %w(K K K K K K),
      %w(EO RD)
    ),
  }
  CustomerSettings['qaamd'] = CustomerSettings['amd']  # sfrieske, 2019-04-08

  CustomerSettingsF7 = {
    # Dialog (Fab7 customer):
    # Good examples: UDX001.000, UDMAAA.001, UDMAAA.023, UDXAAA.001
    # Bad examples: UDM00000.000, UDM3ZZZ.ABC
    "7FREESCALE"=>CustomerStruct.new("DEFAULT", "", "QA",
      "Alpha", 1, %w(I O),
      "", 0, [],
      "", 0, 0, 0,
      "",
      "NumericThenAlphanumeric2", 3, %w(I O),
      "Numeric", 3, %w(I O),
      'QA[A-HJ-NP-Z][0-9A-DF-HKNPRT-Y]{3}', '000', '[0-9]{3}', 0,
      ["2013-01-01-00.00.00.000000", "2013-01-02-10.00.00.000000", "2013-01-03-10.00.00.000000",
       "2013-01-08-10.00.00.000000", "2013-01-09-10.00.00.000000", "2013-12-31-23.59.59.000000"],
      [], [], %w(K K K K K K),
      %w(ES EO RD RQ)
    ),
    "7QA"=>CustomerStruct.new("DEFAULT", "7", "CQA",
      "Alpha", 1, %w(I O),
      "", 0, [],
      "", 0, 0, 0,
      "",
      "NumericThenAlphanumeric2", 3, %w(I O),
      "Numeric", 3, %w(I O),
      '7C[A-Z][0-9A-Z]{3}', '000', '[0-9]{3}', 0,
      ["2013-01-01-00.00.00.000000", "2013-01-02-10.00.00.000000", "2013-01-03-10.00.00.000000",
       "2013-01-08-10.00.00.000000", "2013-01-09-10.00.00.000000", "2013-12-31-23.59.59.000000"],
      [], [], %w(K K K K K K),
    ),
    # Cirrus (Fab7 customer):
    # Good examples: UCX0001.000, UCM0AAA.001, UCMAEAA.023, UCX1435.001
    # Bad examples: UCM00000.000, UDM3ZZZ.ABC
    "7CIRRUS"=>CustomerStruct.new("DEFAULT", "7", "C",
      "Alpha", 1, %w(I O),
      "", 0, [],
      "", 0, 0, 0,
      "",
      "NumericThenAlphanumeric3", 4, %w(I O),
      "Numeric", 3, %w(I O),
      '7C[A-HJ-NP-Z][0-9A-DF-HKNPRT-Y]{3}', '000', '[0-9]{3}', 0,
      ["2013-01-01-00.00.00.000000", "2013-01-02-10.00.00.000000", "2013-01-03-10.00.00.000000",
       "2013-01-08-10.00.00.000000", "2013-01-09-10.00.00.000000", "2013-12-31-23.59.59.000000"],
      [], [], %w(K K K K K K),
    ),
  }

  CustomerSettingsF8 = {
    # AMD:
    # Good examples: N0001.1, N9ZZZ.1, U9ZZZ.AA
    # Bad examples: U9IZZ.01, U9OZZ.01
    'amd-sec'=>CustomerStruct.new("AMD", "N", "",
      "Numeric", 1, [],
      "", 0, [],
      "", 0, 0, 0,
      "",
      "Alphanumeric", 3, %w(I O),
      "Alphanumeric", 2, %w(I O),
      'N[A-HJ-NP-Z][0-9A-HJ-NP-Z]{3}', '1', '[0-9A-HJ-NP-Z]{1,2}', 0,
      ["2013-01-01-00.00.00.000000", "2013-01-02-10.00.00.000000", "2013-01-03-10.00.00.000000",
       "2013-01-08-10.00.00.000000", "2013-01-09-10.00.00.000000", "2013-12-31-23.59.59.000000"],
      [], [], %w(K K K K K K),
    ),
    # GF (default):
    #
    'gf'=>CustomerStruct.new('DEFAULT', "8", 'XY',
      'Alpha', 1, %w(I O),
      '', 0, [],
      'Numeric', 2, 1, 53,
      '',
      'NumericThenAlphanumeric', 3, %w(I O),
      'Numeric', 3, [],
      '8XY[A-HJ-NP-Z](0[1-9]|[1-4][0-9]|5[0-3])[0-9A-HJ-NP-Z]{3}', '000', '[0-9A-HJ-NP-Z]{3}', 0,
      ["2013-01-01-00.00.00.000000", "2013-01-02-10.00.00.000000", "2013-01-03-10.00.00.000000",
       "2013-01-08-10.00.00.000000", "2013-01-09-10.00.00.000000", "2013-12-31-23.59.59.000000"],
      %w(01 01 01 02 02 53), [], %w(K K K K K K)
    ),
    # Qualcomm (similar to GF, no WW):
    # Good examples: UR00001.000, UR9ZZZZ.000, UR9ZZZZ.001
    # Bad examples: UR9IOIO.000, UR90001.ABC
    "qgt"=>CustomerStruct.new("QCT", "8", "R",
      'Numeric', 1, [],
      '', 0, [],
      '', 0, 0, 0,
      '',
      'NumericThenAlphanumeric', 4, %w(I O),
      'Numeric', 3, [],
      '8R[0-9]([0-9A-HJ-NP-Z]{4})', '000', '[0-9]{3}', 0,
      ["2013-01-01-00.00.00.000000", "2013-12-31-23.59.59.000000"],
      [], [], %w(3 3)
    ),
    # Qualcomm US (similar to GF, no WW):
    # Good examples: UR00001.000, UR9ZZZZ.000, UR9ZZZZ.001
    # Bad examples: UR9IOIO.000, UR90001.ABC
    "qgt-us"=>CustomerStruct.new("QCT", "8", "R",
      'Numeric', 1, [],
      '', 0, [],
      '', 0, 0, 0,
      '',
      'NumericThenAlphanumeric', 4, %w(I O),
      'Numeric', 3, [],
      '8R[0-9]([0-9A-HJ-NP-Z]{4})', '000', '[0-9]{3}', 0,
      ["2013-01-01-00.00.00.000000", "2013-12-31-23.59.59.000000"],
      [], [], %w(3 3)
    ),
    # Samsung:
    # Good examples: G1001.001, G1001.001, GZZZZ.001, GZZZZ.001
    # Bad examples: GIOIO.000, G0001.ABD
    "samsung"=>CustomerStruct.new("NA", '', "G",
      '', 0, [],
      '', 0, [],
      '', 0, 0, 0,
      '',
      'Alphanumeric', 4, "EIJLMOQSZ".chars.to_a,
      'NumericThenAlphanumeric', 3, %w(D I O),
      'G[0-9A-DF-HKNPRT-Y]{4}', '001', '[0-9A-CE-HJ-NP-Z]{3}', 0,
      [],
      [], [], []
    ),
    # IBM (same as GF except customerPrefix and splitSeqType):
    # Good examples: UIQA01001.000, UIQA01001.001, UIQZ5309Z.059, UIQZ53ZZZ.999
    # Bad examples: UIQA00000.000, UIQZ53ZZZ.ABC
    "ibm"=>CustomerStruct.new("IBM", "8", "IQ",
      'Alpha', 1, %w(I O),
      '', 0, [],
      'Numeric', 2, 1, 53,
      '',
      'NumericThenAlphanumeric', 3, %w(I O),
      'N  umeric', 3, %w(I O),
      '8IQ[A-HJ-NP-Z](0[1-9]|[1-4][0-9]|5[0-3])[0-9A-HJ-NP-Z]{3}', '000', '[0-9A-HJ-NP-Z]{3}', 0,
      ["2013-01-01-00.00.00.000000", "2013-01-02-10.00.00.000000", "2013-01-03-10.00.00.000000",
       "2013-01-08-10.00.00.000000", "2013-01-09-10.00.00.000000", "2013-12-31-23.59.59.000000"],
      %w(01 01 01 02 02 53), [], %w(K K K K K K)
    ),
  }

  # Vendor lot
  # Good examples: V200000.00, V300000.00
  # Bad examples: V 000000.000
  VendorLotSettings = CustomerStruct.new('DEFAULT', 'V', '',
    'Numeric', 1, [],
    '', 0, [],
    '', 0, 0, 0,
    '',
    'Alphanumeric', 5, %w(I O),
    'Alphanumeric', 2, %w(I O),
    'V[A-Z][0-9A-HJ-NP-Z]{5}', '00', '[0-9A-HJ-NP-Z]{2}', 0,
    [],
    [], [], []
  )

  # Control lot
  # Good examples: T200000.00, T300000.00
  # Bad examples: T 000000.000
  ControlLotSettings = CustomerStruct.new('DEFAULT', 'T', '',
    'Numeric', 1, [],
    '', 0, [],
    '', 0, 0, 0,
    '',
    'Alphanumeric', 5, %w(I O),
    'Alphanumeric', 2, %w(I O),
    'T[A-Z][0-9A-HJ-NP-Z]{5}', '00', '[0-9A-HJ-NP-Z]{2}', 0,
    [],
    [], [], []
  )


  class Customer < SiView::Test
    attr_accessor :settings, :nlots, :nchildren, :regex_parent, :regex_child, :lotid_digits,
      :generated_wws, :generated_months, :generated_years,  # for special timestamps
      :generated_lots, :generated_children, :calculated_lots, :calculated_children

    def initialize(env, params={})
      super(env, {carriers: 'LIG%'}.merge(params))
      @nlots = params[:nlots] || 34
      @nchildren = params[:nchildren] || @nlots
      @settings = params[:settings] ||
        (env =~ /f8stag|mtqa/ ? CustomerSettingsF8[@customer].clone :
        (env =~ /f7/ ? CustomerSettingsF7[@customer].clone :
        CustomerSettings[@customer].clone))
      @regex_parent = /^#{@settings.regex_runseq}.#{@settings.regex_splitseq_parent}$/
      @regex_child = /^#{@settings.regex_runseq}.#{@settings.regex_splitseq_child}$/
      @lotid_digits = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ'.chars - @settings.runSeqExcludedChars
    end

    def inspect
      "#<#{self.class.name}  #{@env}, customer: #{@customer.inspect}>"
    end

    # ensure customer UDATA LIG_CalendarID is set as configured, return true on success
    def verify_udata_calendar
      $log.info "verify_udata_calendar"
      ($log.info "skipped for pseudo customer 'Vendor'"; return true) if @customer == 'Vendor'
      calu = @sv.user_data(:customer, @customer)['LIG_CalendarID']
      calcfg = @settings.calendar
      ($log.warn "wrong SM LIG_CalendarID for customer #{@customer}: #{calu} instead of #{calcfg}"; return) if calu != calcfg
      return true
    end

    # call LIG to generate lot IDs as configured, return true on success
    def generate_lotids(params={})
      $log.info "generate_lotids #{params}"
      @generated_children = []
      n = params[:nlots] || @nlots
      # pass sublottype for Fab7
      @generated_lots = @sv.generate_new_lotids(@customer, n: n, sublottype: params[:sublottype])
      @settings.special_starttimes.each {|t|
        @generated_lots += @sv.generate_new_lotids(@customer, starttime: t, sublottype: params[:sublottype])
      }
      @generated_lots.compact!
      # AMD SEC lot numbering is different. Use the generated lots with suffix .1
      @generated_lots.map! {|lot| "#{lot.split('.')[0]}#{params[:parent_suffix] || '.1'}"} if @settings.sec_lots
      ret = (@generated_lots.size == n + @settings.special_starttimes.size)
      $log.warn "  not all requested lot IDs have been generated" unless ret
      return ret
    end

    # verify the lot matches the naming conventions and can be built, create child lots to check child lot rules
    #
    # return true on success
    def verify_lot_creation(lot, params={})
      $log.info "verify_lot_creation #{lot.inspect}, #{params}"
      unless params[:parent_suffix]  # skip test for special lots
        @regex_parent.match(lot) || ($log.warn "wrong parent lot name (#{@regex_parent}"; return)
      end
      if params[:lot] != true
        # lot is not yet created (e.g. to accelerate creation of regular lots)
        res = new_lot(lot: lot, nwafers: 5, sublottype: params[:sublottype])
        ($log.warn 'error creating lot'; return) unless res
      end
      if nch = params[:children]
        splitparams = params[:onhold] ? {onhold: true, release: params[:release], raw: true} : {raw: true}
        li = @sv.lot_info(lot, wafers: true)
        splitwafers = li.wafers.collect {|w| w.wafer}.take(2)
        $log.info "verifying child lot creation, splitparams: #{splitparams}"
        @generated_children ||= []
        nch = @nchildren unless nch.kind_of?(Numeric)  # children: 3 or children: true to use the default
        holdreasonID = @sv.code_list('LotHold').first.code
        nch.times {|i|
          if params[:onhold] && (params[:release] || i.zero?)
            res = @sv.lot_hold(lot, holdreasonID)
            ($log.warn "error setting lot #{lot} ONHOLD"; return) if res != 0
          end
          $log.info "-- creating child lot #{i+1}/#{nch}"
          lcid = @sv.lot_split(lot, splitwafers, splitparams) || ($log.warn 'error splitting lot'; return)
          lc = lcid.identifier
          @generated_children << lc
          @regex_child.match(lc) || ($log.warn 'wrong child lot name'; return)
          res = @sv.lot_merge(lot, lcid)
          ($log.warn 'error merging child lot'; return) if res != 0
        }
      end
      return true
    end

    # verify all lots can be created, child lot creation is verified for the first lot only as default
    #
    # return true on success
    def verify_lots_creation(params={})
      lots = params[:lots] || @generated_lots
      if self.class == LIG::Customer
        lots.each {|lot|
          res = new_lot(lot: lot, nwafers: 5, sublottype: params[:sublottype])
          ($log.warn "\n---- error creating lot"; return) if res.nil?
        }
      else
        $log.info "verify_lots_creation (class #{self.class})"
      end
      lots.each_with_index {|lot, i|
        if params[:children] == 'all'
          vc = true
        elsif params[:children] == false
          vc = false
        else
          vc = (i == 0)
        end
        # verify_lot_creation(lot, sublottype: params[:sublottype], children: vc) || return
        verify_lot_creation(lot, lot: true, children: vc) || return
      }
      return true
    end

    def next_NumericThenAlphanumeric(seq)
      if seq =~ /^9+$/
        '0' * (seq.length-1) + 'A'
      else
        seq.next
      end
    end

    def next_NumericThenAlphanumeric2(seq)
      if seq =~ /^9+$/
        '0' * (seq.length-1) + 'A'
      elsif seq =~ /^(9+)(Z+)$/
        '0' * ($1.length-1) + 'A' * ($2.length+1)
      else
        seq.next
      end
    end

    def next_NumericThenAlphanumeric3(seq)
      if seq =~ /^9+$/
        'A' + '0' * (seq.length-1)
      elsif seq =~ /^(Z+)(9+)$/
        'A' * ($1.length+1) + '0' * ($2.length-1)
      elsif seq =~ /^([A-Z]+)$/
        seq.each_char.map { |c| c.succ}.join
      else
        seq.next
      end
    end

    # predict lot IDs from the settings, starting from the first genereated lot ID
    def predict_lotids(params={})
      $log.info "predict_lotids #{params}"
      @generated_wws = []
      @generated_months = []
      @generated_years = []
      #
      @generated_lots.each_with_index {|lot, j|
        (@calculated_lots = [lot]; next) if j == 0
        #
        lotparts = @generated_lots[j-1].split('.')
        splitSeq = lotparts[1]
        yearStartPos = @settings.fabId.length + @settings.customerPrefix.length
        yearEndPos = yearStartPos + @settings.yearCodeLength
        monthStartPos = yearStartPos + @settings.yearCodeLength
        monthEndPos = monthStartPos + @settings.monthCodeLength
        workWeekStartPos = monthStartPos + @settings.monthCodeLength
        workWeekEndPos = workWeekStartPos + @settings.workWeekLength
        firstSignsEndPos = yearStartPos
        #
        runSeq = lotparts[0].slice(lotparts[0].length-@settings.runSeqLength..lotparts[0].length-1)
        runSeqPre = lotparts[0].slice(0..lotparts[0].length-@settings.runSeqLength-1)
        runSeqSuccessor = case @settings.runSeqType
        when 'Alphanumeric'
          runSeqArray = runSeq.chars.to_a.reverse
          runSeqArray.each_with_index {|char, i|
            lastIndex = @lotid_digits.index(char)
            nextIndex = (char == @lotid_digits.last) ? 0 : lastIndex + 1
            nextSign = @lotid_digits[nextIndex]
            runSeqArray[i] = nextSign
            break if char != @lotid_digits.last
          }
          runSeqArray.reverse.join('')
        when 'Numeric'
          runSeq.succ
        when 'NumericThenAlphanumeric'
          next_NumericThenAlphanumeric(runSeq)
        when 'NumericThenAlphanumeric2'
          next_NumericThenAlphanumeric2(runSeq)
        when 'NumericThenAlphanumeric3'
          next_NumericThenAlphanumeric3(runSeq)
        end
        #
        lotpartsForSpecialTS = lot.split('.')[0]
        firstSigns = lotpartsForSpecialTS[0...firstSignsEndPos]
        #
        expectedMonth = ''
        generatedMonth = lotpartsForSpecialTS[monthStartPos...monthEndPos]
        if @settings.monthCodeLength > 0
          if j > @nlots - 1
            expectedMonth = @settings.expected_months[j - @nlots]
            @generated_months << generatedMonth
          else
            expectedMonth = generatedMonth
          end
        end
        expectedWW = ''
        generatedWW = lotpartsForSpecialTS[workWeekStartPos...workWeekEndPos]
        if @settings.workWeekLength > 0
          if j > @nlots - 1
            expectedWW = @settings.expected_wws[j - @nlots]
            @generated_wws << generatedWW
          else
            expectedWW = generatedWW
          end
        end
        expectedYear = ''
        generatedYear = lotpartsForSpecialTS[yearStartPos...yearEndPos]
        if @settings.yearCodeLength > 0
          if @settings.xsublottypes && @settings.xsublottypes.member?(params[:sublottype])
            expectedYear = 'X'
            @generated_years << generatedYear
          elsif j > @nlots - 1
            expectedYear = @settings.expected_years[j - @nlots]
            @generated_years << generatedYear
          else
            expectedYear = generatedYear
          end
        end
        #
        @calculated_lots << "#{firstSigns}#{expectedYear}#{expectedMonth}#{expectedWW}#{@settings.fabPrefix}#{runSeqSuccessor}.#{splitSeq}"
      }
      return @calculated_lots
    end

    # predict the child ids for the first generated lot (default
    def predict_childids(params={})
      $log.info "predict_childids #{params.inspect}"
      lot = (params[:lot] or @generated_lots[0])
      firstpart, splitSeq = lot.split('.')
      @calculated_children = []
      @nchildren.times {|j|
        targetSplitId = j + 1 + @settings.child_offset
        splitSeqSuccessor = case @settings.splitSeqType
        when 'Alphanumeric'
          output = ""
          carry_over = targetSplitId
          c_i = splitSeq.length - 1
          # calculate the alphanumeric lot split id with leading or no leading zeros
          while carry_over > 0 || c_i >= 0
            if c_i >= 0
              # existing number of digits
              c = splitSeq[c_i]
              i = @lotid_digits.index(c) + carry_over
              c_i -= 1
            else
              # carry over to new digit if allowed
              i = carry_over
            end
            carry_over = i / @lotid_digits.length
            i = i % @lotid_digits.length
            output = @lotid_digits[i] + output
          end
          if output.length > @settings.splitSeqLength
            raise Exception.new "sequence overflow for child lot id"
          end
          output
        when 'Numeric'
          nextSeq = splitSeq
          targetSplitId.times {nextSeq = nextSeq.succ}
          nextSeq
        when 'NumericThenAlphanumeric'
          nextSeq = splitSeq
          targetSplitId.times do
            nextSeq = next_NumericThenAlphanumeric(nextSeq)
            unless @settings.splitSeqExcludedChars.empty?
              while nextSeq =~ Regexp.new("[#{@settings.splitSeqExcludedChars.join}]")
                nextSeq = next_NumericThenAlphanumeric(nextSeq)
              end
            end
          end
          nextSeq
        end
        @calculated_children << "#{firstpart}.#{splitSeqSuccessor}"
      }
      @calculated_children
    end

    # run a complete test, including cleanup, optionally pass sublottype for Fab7
    def verify_lig(params={})
      $log.info "verify_lig  #{params}  # customer: #{@customer.inspect}"
      verify_udata_calendar || return
      generate_lotids(sublottype: params[:sublottype]) || return
      verify_lots_creation(sublottype: params[:sublottype]) || return
      predict_lotids(sublottype: params[:sublottype]) || return
      predict_childids || return
      #
      # verify lot creation
      ret = true
      $log.info 'verifying lotid rules'
      # regular lots
      @calculated_lots.each_with_index {|calc, i|
        gen = @generated_lots[i]
        ($log.warn "wrong lot name: expected #{calc.inspect}, got #{gen.inspect}"; ret = false) if gen != calc
      }
      # lots with special timestamps cannot be verified just by comparing the lot ids,
      # the components year, month and work week must be compared instead
      $log.info 'verifying lot id parts for special timestamps'
      @settings.expected_years.each_with_index {|e, i|
        e = 'X' if @settings.xsublottypes && @settings.xsublottypes.member?(params[:sublottype])
        ($log.warn 'fiscal year error'; ret = false) if e != @generated_years[i]
      }
      @settings.expected_months.each_with_index {|e, i|
        ($log.warn 'month error'; ret = false) if e != @generated_months[i]
      }
      @settings.expected_wws.each_with_index {|e, i|
        ($log.warn 'work week error'; ret = false) if e != @generated_wws[i]
      }
      # child lots
      $log.info 'verifying childid rules'
      ret = true
      @calculated_children.each_with_index {|calc, i|
        gen = @generated_children[i]
        ($log.warn "wrong child name: expected #{calc.inspect}, got #{gen.inspect}"; ret = false) if gen != calc
      }
      #
      delete_lots unless params[:cleanup] == false
      return ret
    end

    # delete created lots
    def delete_lots
      @generated_lots.each {|lot| @sv.delete_lot_family(lot)} if @generated_lots
    end

  end


  class VendorLot < Customer
    attr_accessor :bank

    def initialize(env, bank, params={})
      super(env, {customer: 'Vendor', settings: VendorLotSettings}.merge(params))
      @bank = bank
    end

    # receive vendor lots as configured, return true on success
    def generate_lotids(params={})
      $log.info "generate_lotids  # by receiving vendor lots"
      @generated_children = []
      @generated_lots = @nlots.times.collect {@sv.vendorlot_receive(@bank, @product, 5)}
      ($log.warn "  not all requested lot IDs have been generated"; return) unless @generated_lots.size == @nlots
      return true
    end

    # verify the vendor lot matches the naming conventions and can be received (overrides Customer#verify_lot_creation)
    #
    # return true on success
    def verify_lot_creation(lot, params={})
      $log.info "verify_lot_creation #{lot.inspect}, #{params.inspect}"
      ($log.warn "  wrong parent lot name schema (#{@regex_parent.inspect} for #{lot}"; return) unless @regex_parent.match(lot)
      c = get_empty_carrier || return
      res = @sv.vendorlot_prepare(lot, c) || ($log.warn "  error creating lot"; return)
      ($log.warn "  wrong parent lot name schema (#{@regex_parent.inspect} for #{res}"; return) unless @regex_parent.match(res)
      # update lotid
      @generated_lots[@generated_lots.index(lot)] = res
      lot = res
      if params[:children]
        $log.info "  verifying child lot creation.."
        @generated_children ||= []
        @nchildren.times {|i|
          $log.info "-- creating child #{i+1}/#{@nchildren}"
          clot = @sv.lot_split_notonroute(lot, 2)
          ($log.warn "    error splitting lot #{lot.inspect}"; return false) unless clot
          @generated_children << clot
          ($log.warn '  wrong child lot name'; return false) unless @regex_child.match(clot)
          res = @sv.lot_merge_notonroute(lot, clot)
          ($log.warn "    error merging child lot"; return false) unless res == 0
        }
      end
      return true
    end

  end


  class ControlLot < Customer
    attr_accessor :lottype

    def initialize(env, lottype, params={})
      super(env, {settings: ControlLotSettings}.merge(params))
      @lottype = lottype
      @srcproduct = params[:srcproduct] || 'UT-RAW.00'
      @startbank = params[:startbank] || 'UT-RAW'
    end

    # call LIG to receive vendor lots as configured, return true on success
    def generate_lotids(params={})
      $log.info "generate_lotids #{params}"
      @generated_children = []
      # lot ids are created at stb_controllot
      @generated_lots = @nlots.times.collect {
        ec = get_empty_carrier || return
        vlot = @sv.vendorlot_receive(@startbank, @srcproduct, 5, raw: true) || return
        srclot = @sv.vendorlot_prepare(vlot, ec)
        @sv.stb_controllot(@lottype, @product, srclot)
      }
      ret = (@generated_lots.size == @nlots)
      $log.warn 'not all requested lot IDs have been generated' unless ret
      return ret
    end

    # verify the control lot matches the naming conventions (overrides Customer#verify_lot_creation)
    #
    # return true on success
    def verify_lot_creation(lot, params={})
      $log.info "verify_lot_creation #{lot.inspect}, #{params}"
      @regex_parent.match(lot) || ($log.warn "  wrong parent lot name schema (#{@regex_parent.inspect} for #{lot}"; return)
      if params[:children]
        $log.info "  verifying child lot creation.."
        @generated_children ||= []
        @nchildren.times {|i|
          $log.info "-- creating child #{i+1}/#{@nchildren}"
          clot = @sv.lot_split(lot, 2) || ($log.warn "    error splitting lot #{lot.inspect}"; return)
          @generated_children << clot
          @regex_child.match(clot) || ($log.warn "  not matching child lot name schema"; return)
          res = @sv.lot_merge(lot, clot)
          ($log.warn "  error merging child lot"; return false) unless res == 0
        }
      end
      return true
    end

  end
end
