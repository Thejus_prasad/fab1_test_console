=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: Steffen Steidten, 2009-05-03; Daniel Steger, 2014-10-30

History:
  2021-02-05 sfrieske, use instance_of instead of kind_of
=end


class SiView::SM

    # reserve the given version for active versioning, return true on success
    def reserve_active(classname, objs, params={})
      $log.info "reserve_active #{classname.inspect}, #{objs.inspect}, #{params}"
      objs = [objs] unless objs.instance_of?(Array)
      tablename, fieldname = SMClasses[classname]
      oids = object_ids(classname, objs, params)
      rsv_objs = objs.each_with_index.collect {|obj, i|
        doc, version = obj.split('.') # doesn't work for machine recipes
        @jcscode.brChangeVersionObject_struct.new(tablename, doc, oids[i])
      }.to_java(@jcscode.brChangeVersionObject_struct)
      #
      begin
        if classname == :mainpd
          res = @_res = @manager.TxReserveChangeActiveForMainObject(rsv_objs, !!params[:noreturn], @_userinfo)
        else
          res = @_res = @manager.TxReserveChangeActive(rsv_objs, @_userinfo)
        end
        return true
      rescue
        $log.warn $!
        return false
      end
    end

    # reserve cancel the given version for active versioning, return true on success
    def reserve_active_cancel(classname, objs, params={})
      $log.info "reserve_active_cancel #{classname.inspect}, #{objs.inspect}, #{params.inspect}"
      quick_check = params[:quick_check] != false
      objs = [objs] unless objs.instance_of?(Array)
      tablename, fieldname = SMClasses[classname]
      docs = objs.collect {|obj| obj.split('.').first} # doesn't work for machine recipes
      ccres = @jcscode.brObjectConsistencyCheckResultSequenceHolder.new
      #
      begin
        if classname == :mainpd
          res = @_res = @manager.TxCancelReserveChangeActiveForMainObject(docs, tablename, !!params[:noreturn], quick_check, @_userinfo, ccres)
        else
          res = @_res = @manager.TxCancelReserveChangeActive(docs, tablename, quick_check, @_userinfo, ccres)
        end
        return check_ccres(ccres)
      rescue
        $log.warn $!
        return false
      end
    end

    # return true on success
    def stop_active_version(classname, objs, params={})
      $log.info "stop_active_version #{classname.inspect}, #{objs.inspect}, #{params.inspect}"
      objs = [objs] unless objs.instance_of?(Array)
      tablename, fieldname = SMClasses[classname]
      oids = object_ids(classname, objs.collect {|obj| obj.sub(/\.\d+$/, '.##')}) # get active version objects
      ($log.warn "active versioning is not enabled!"; return) if oids.empty?
      #
      if classname == :mainpd
        res = @_res = @manager.TxStopActiveVersionControlToProductionForMainObject(oids, tablename, !!params[:noreturn], @_userinfo)
      else
        res = @_res = @manager.TxStopActiveVersionControlToProduction(oids, tablename, @_userinfo)
      end
      return true
    end

end
