=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

History
  2014-07-25 ssteidte, converted global struct defs to constants within SiView::MM
  2019-11-27 sfrieske, use pptEntityInhibitDetailInfo_struct to identify inhibits
=end


class SiView::MM

  Inhibit = Struct.new(:iid, :claimed, :tstart, :tend, :entities, :reason, :owner, :memo, :sublottypes, :inhibit_owner,
    :release_time, :reason_details)
  InhibitReasonDetail = Struct.new(:lot, :cj, :fab, :route, :pd, :opNo, :pass, :charts)
  InhibitSpcChart = Struct.new(:dc_type, :chart_group, :chart, :chart_type, :url)
  InhibitEntity = Struct.new(:classname, :objectid, :attrib)
  InhibitException = Struct.new(:lot, :claimed, :user, :memo, :inhibit)
  # used in Fab1: => [
  #  :eqp, :eqp_chamber, :eqp_chamber_pd, :eqp_chamber_product, :eqp_chamber_productgroup, :eqp_chamber_productgroup_pd,
  #  :eqp_chamber_productgroup_recipe, :eqp_chamber_recipe, :eqp_recipe, :pd_eqp, :productgroup_eqp, :productgroup_eqp_recipe,
  #  :productgroup_pd, :productgroup_pd_eqp, :productgroup_pd_recipe, :recipe, :reticle, :reticle_eqp,
  #  :route_opNo_product, :route_opNo_product_eqp, :technology_eqp, :technology_eqp_chamber
  #]
  InhibitClasses = {
    eqp: ['Equipment'],
    # for "Equipment and Chamber" inhibts "Chamber" object is the equipment; chambers are passed as :attrib parameter
    eqp_chamber: ['Chamber'],
    eqp_chamber_productgroup: ['Chamber', 'Product Group'],
    eqp_chamber_pd: ['Chamber', 'Process Definition'],
    eqp_chamber_productgroup_pd: ['Chamber', 'Product Group', 'Process Definition'],
    eqp_chamber_productgroup_recipe: ['Chamber', 'Product Group', 'Machine Recipe'],
    eqp_chamber_product: ['Chamber', 'Product Specification'],
    eqp_chamber_recipe: ['Chamber', 'Machine Recipe'],
    eqp_product: ['Equipment', 'Product Specification'],
    eqp_recipe: ['Equipment', 'Machine Recipe'],
    fixture: ['Fixture'],
    fixture_eqp: ['Fixture', 'Equipment'],
    fg: ['Fixture Group'],
    fg_eqp: ['Fixture Group', 'Equipment'],
    lrecipe: ['Logical Recipe'],
    lrecipe_eqp: ['Logical Recipe', 'Equipment'],
    lrecipe_eqp_chamber: ['Logical Recipe', 'Chamber'],
    module: ['Module Process Definition'],
    pd_eqp: ['Process Definition', 'Equipment'],
    productgroup: ['Product Group'],
    productgroup_eqp: ['Product Group', 'Equipment'],
    productgroup_eqp_recipe: ['Product Group', 'Equipment', 'Machine Recipe'],
    productgroup_pd: ['Product Group', 'Process Definition'],
    productgroup_pd_eqp: ['Product Group', 'Process Definition', 'Equipment'],
    productgroup_pd_recipe: ['Product Group', 'Process Definition', 'Machine Recipe'],
    product: ['Product Specification'],
    recipe: ['Machine Recipe'],
    reticle: ['Reticle'],
    reticle_eqp: ['Reticle', 'Equipment'],
    rg: ['Reticle Group'],
    rg_eqp: ['Reticle Group', 'Equipment'],
    route: ['Route'],
    # for 'RouteOperation' inhibits 'Operation' class is the route; opNo is passed as :attrib parameter
    route_opNo: ['Operation'],
    route_opNo_product: ['Operation', 'Product Specification'],
    route_opNo_product_eqp: ['Operation', 'Product Specification', 'Equipment'],
    route_opNo_product_eqp_chamber: ['Operation', 'Product Specification', 'Chamber'],
    route_opNo_eqp: ['Operation', 'Equipment'],
    route_opNo_eqp_chamber: ['Operation', 'Chamber'],
    stage: ['Stage'], # not shown in MMOPI
    ## disabled per config technology: ['Technology'],
    technology_eqp: ['Technology', 'Equipment'],
    technology_eqp_chamber: ['Technology', 'Chamber'],
    technology_pd: ['Technology', 'Process Definition'],
    technology_pd_eqp: ['Technology', 'Process Definition', 'Equipment'],
    technology_pd_eqp_chamber: ['Technology', 'Process Definition', 'Chamber'],  # see :eqp_chamber
    customer: ['Customer'],
    customer_technology_eqp: ['Customer', 'Technology', 'Equipment'],
    customer_technology_eqp_chamber: ['Customer', 'Technology', 'Chamber']       # see :eqp_chamber
  }

  # classname is :eqp, :eqp_chamber, etc., see InhibitClasses
  #
  # Equipment + Chamber: inhibit_xxx(:eqp_chamber, ['UTC001'], attrib: ['CHA', 'CHB'])
  # Reticle + Equipment: inhibit_xxx(:reticle_eqp, ['UTRETICLE0001', 'UTR001'])
  # Equipment + Chamber + Product: inhibit_entity(:eqp_chamber_product, [eqp, product], attrib: ['PM1', ''])


  # create Inhibit struct from pptEntityInhibitDetailInfo_struct
  def _extract_inhibit_info(inh, details=false)
    aa = inh.entityInhibitAttributes
    # sublottypes = aa.subLotTypes.collect {|s| s}.join(',')
    # sublottypes = '*' if sublottypes == '' # all sublot types, TODO: required?
    owner = nil
    rtime = nil
    if siinfo = extract_si(inh.siInfo)
      udata = siinfo.find {|u| u.name == 'Inhibit Owner'}
      owner = udata.value if udata
      udata = siinfo.find {|u| u.name == 'Expected Release Time'}
      rtime = udata.value if udata
    end
    if details
      # extract reason details
      reasondetails = aa.strEntityInhibitReasonDetailInfos.collect {|r|
        charts = r.strEntityInhibitSpcChartInfos.collect {|c|
          InhibitSpcChart.new(c.relatedSpcDcType, c.relatedSpcChartGroupID,
            c.relatedSpcChartID, c.relatedSpcChartType, c.relatedSpcChartUrl)
        }
        InhibitReasonDetail.new(r.relatedLotID, r.relatedControlJobID, r.relatedFabID, r.relatedRouteID,
          r.relatedProcessDefinitionID, r.relatedOperationNumber, r.relatedOperationPassCount, charts)
      }
    else
      reasondetails = nil
    end
    return Inhibit.new(inh.entityInhibitID.identifier, aa.claimedTimeStamp, aa.startTimeStamp, aa.endTimeStamp,
      aa.entities.collect {|e| InhibitEntity.new(e.className, e.objectID.identifier, e.attrib)},
      aa.reasonCode, aa.ownerID.identifier, aa.memo, aa.subLotTypes.to_a, owner, rtime, reasondetails)
  end

  # list inhibits, classname is :eqp, :eqp_chamber, etc., see InhibitClasses, all inhibits if objectid is nil
  # return array of Inhibit or pptEntityInhibitDetailInfo_struct if raw, nil on error
  def inhibit_list(classname, objectids, params={})
    tstart = params[:tstart] || ''
    tend = params[:tend] || ''
    tend = siview_timestamp(tend) if tend.instance_of?(Time)
    tclaim = params[:claimed] || ''
    reason = params[:reason] || ''
    owner = params[:owner] || ''
    details = !!params[:details]
    sublottypes = [].to_java(:string)   # accepted but not honoured by MM server
    objectids = [objectids] if objectids.instance_of?(String)
    entities = InhibitClasses[classname].each_with_index.collect {|cn, i|
      @jcscode.pptEntityIdentifier_struct.new(cn, oid(objectids[i] || ''), '', any)  # attrib not honoured
    }.to_java(@jcscode.pptEntityIdentifier_struct)
    attrs = @jcscode.pptEntityInhibitDetailAttributes_struct.new(
      entities, sublottypes, tstart, tend, reason, '', '', oid(owner), tclaim,
      [].to_java(@jcscode.pptEntityInhibitReasonDetailInfo_struct), any)
    #
    res = @manager.TxEntityInhibitListInq__101(@_user, attrs, details)
    return nil if rc(res) != 0
    return res.strEntityInhibitions if params[:raw]
    return res.strEntityInhibitions.collect {|inh| _extract_inhibit_info(inh, details)}
  end

  # classname is :eqp, :eqp_chamber, etc., see InhibitClasses; return inhibit id on success or nil
  def inhibit_entity(classname, objectid, params={})
    $log.info "inhibit_entity #{classname.inspect}, #{objectid.inspect}, #{params.reject {|k| k == :inhibit}}"
    attrib = params[:attrib] || []
    attrib = [attrib] if attrib.instance_of?(String)
    tstart = params[:tstart] || ''
    tstart = siview_timestamp(tstart) unless tstart.instance_of?(String)
    tend = params[:tend] || siview_timestamp(Time.now + 43200)  # 12 hours
    ## tend = '1901-01-01-00.00.00.000000' if tend == ''
    tend = siview_timestamp(tend) if tend.instance_of?(Time)
    reason = params[:reason] || 'PENG'
    desc = params[:desc] || ''  # normally reason desc from code data
    memo = params[:memo] || ''
    ownerID = params.has_key?(:owner) ? oid(params[:owner]) : @_user.userID
    sublottypes = params[:sublottypes] || []
    sublottypes = [sublottypes] if sublottypes.instance_of?(String)
    #
    if inh = params[:inhibit]  # pass an inhibit to 'clone'
      $log.info "  (inhibit: #{inh.entityInhibitID.identifier})"
      # use data in a pptEntityInhibitDetailInfo_struct, overriding sublottypes etc.
      inhattrs = inh.entityInhibitAttributes
      entities = inhattrs.entities
      sublottypes = params.has_key?(:sublottypes) ? params[:sublottypes] : inhattrs.subLotTypes.to_a
      tstart = params.has_key?(:tstart) ? tstart : inhattrs.startTimeStamp
      tend = params.has_key?(:tend) ? tend : inhattrs.endTimeStamp
      reason = inhattrs.reasonCode
      desc = inhattrs.reasonDesc
      memo = params.has_key?(:memo) ? memo : inhattrs.memo
      ownerID = params.has_key?(:owner) ? ownerID : inhattrs.ownerID
      rdetails = inhattrs.strEntityInhibitReasonDetailInfos
    else
      classname = InhibitClasses[classname] if classname.instance_of?(Symbol)
      objectid = [objectid] if objectid.instance_of?(String)
      entities = classname.each_with_index.collect {|cn, i|
        @jcscode.pptEntityIdentifier_struct.new(cn, oid(objectid[i]), attrib[i] || '', any)
      }.to_java(@jcscode.pptEntityIdentifier_struct)
      reasondetails = []
      if details = params[:reason_details]
        details = [details] unless details.instance_of?(Array)  # Hash allowed
        details.each {|r|
          chartinfos = []
          if r[:charts].instance_of?(Array)
            r[:charts].each {|c|
              chartinfos << @jcscode.pptEntityInhibitSpcChartInfo_struct.new(
                c[:dc_type] || '', c[:chart_group] || '', c[:chart] || '', c[:chart_type] || '', c[:url] || '', any)
            }
          end
          reasondetails << @jcscode.pptEntityInhibitReasonDetailInfo_struct.new(
            r[:lot] || '', r[:cj] || '', r[:fab] || '', r[:route] || '',
            r[:pd] || '', r[:opNo] || '', r[:pass].to_s || '',
            chartinfos.to_java(@jcscode.pptEntityInhibitSpcChartInfo_struct), any)
        }
      end
      rdetails = reasondetails.to_java(@jcscode.pptEntityInhibitReasonDetailInfo_struct)
    end
    # attrs siInfo is accepted but not honoured, memo MUST be set for correct updates
    attrs = @jcscode.pptEntityInhibitDetailAttributes_struct.new(
      entities, sublottypes.to_java(:string), tstart, tend, reason, desc, memo, ownerID, siview_timestamp, rdetails, any)
    #
    res = @manager.TxEntityInhibitReq__101(@_user, attrs, memo)
    return if rc(res) != 0
    iid = res.entityInhibition.entityInhibitID.identifier
    ($log.warn res.strResult.messageText; return) if iid.empty?  # 0000794E:Requested entity inhibit is already exist.
    return res.entityInhibition
  end

  # cancel all matching inhibits or pass (nil, nil, inhibits: [array of raw inhibits]), return 0 on success
  def inhibit_cancel(classname, objects, params={})
    $log.info "inhibit_cancel #{classname.inspect}, #{objects.inspect}, #{params.reject {|k| k == :inhibits}}}" unless params[:silent]
    # pass a list of inhibits (the way to go), optionally search by parameters
    inhibits = params[:inhibits]
    if inhibits
      $log.info "  (passed #{inhibits.size} inhibits)"
    else
      inhibits = inhibit_list(classname, objects, {raw: true}.merge(params))
    end
    if inhibits.empty?
      $log.info '  no inhibits to cancel' unless params[:silent]
      return 0
    end
    if cancelreason = params[:cancel_reason]
      if cancelreason.instance_of?(String)
        cinfo = code_list('EntityInhibitCancel').find {|e| e.code.identifier == cancelreason}
        ($log.warn '  wrong reason code'; return) unless cinfo
        reasonID = cinfo.code
      else
        reasonID = cancelreason   # special tests with crafted reasonID (oid(reason))
      end
    else
      reasonID = code_list('EntityInhibitCancel').first.code
    end
    $log.info "  canceling #{inhibits.size} #{classname} inhibit(s), cancel_reason: #{reasonID.identifier}"
    #
    res = @manager.TxEntityInhibitCancelReq__101(@_user, inhibits, reasonID, params[:memo] || '')
    return rc(res)
  end

  # update inhibit comment, owner, end time (used from OPI), return 0 on success
  def inhibit_update(inh, params={})
    $log.info "inhibit_update (#{inh.entityInhibitID.identifier}), #{params}}"
    tend = params.delete(:tend)   # must not be passed to inhibit_list, DOES NOT WORK
    tend = siview_timestamp(tend) if tend.instance_of?(Time)
    # ownerID = params.has_key?(:owner) ? oid(params[:owner]) : nil  # accepted but not honoured by the MM server
    # sublottypes = params[:sublottypes]                             # accepted but not honoured by the MM server
    # convert inhibit_list's pptEntityInhibitDetailInfo_struct to pptEntityInhibitInfo_struct
    inhattribs = @jcscode.pptEntityInhibitAttributes_struct.new(
      inh.entityInhibitAttributes.entities, inh.entityInhibitAttributes.subLotTypes,
      inh.entityInhibitAttributes.startTimeStamp, tend || inh.entityInhibitAttributes.endTimeStamp,
      inh.entityInhibitAttributes.reasonCode, inh.entityInhibitAttributes.reasonDesc,
      params[:comment] || inh.entityInhibitAttributes.memo, inh.entityInhibitAttributes.ownerID,
      inh.entityInhibitAttributes.claimedTimeStamp, inh.entityInhibitAttributes.siInfo,
    )
    iparms = @jcscode.pptEntityInhibitInfo_struct.new(inh.entityInhibitID, inhattribs, inh.siInfo)
    # handle UDATA inhibit_owner and release_time
    iinfo = _extract_inhibit_info(inh)
    iowner = params[:inhibit_owner] || iinfo.inhibit_owner || '' # this is not the ownerID
    rtime = params[:release_time]  || iinfo.release_time || ''  # this is not the end time
    rtime = siview_timestamp(rtime) if rtime.instance_of?(Time)
    udatas = []
    udatas << @jcscode.pptUserData_struct.new('Inhibit Owner', 'String', iowner, 'MM', any) if iowner
    udatas << @jcscode.pptUserData_struct.new('Expected Release Time', 'String', rtime, 'MM', any) if rtime
    parms = @jcscode.pptUserDataUpdateReqInParm_struct.new(inh.entityInhibitID.identifier, 'PosEntityInhibit',
      [].to_java(@jcscode.pptHashedInfo_struct), 'Regist', udatas.to_java(@jcscode.pptUserData_struct), any)
    #
    res = @manager.CS_TxEntityInhibitUpdateReq(@_user, iparms, parms, params[:memo] || '')
    return rc(res)
  end

  # return list of inhibit exceptions for the lot and inhibit, used in 1 SiView regression test
  # note that classname and objectid are single arguments, not arrays
  def inhibit_exception_list(lot, inh)
    # workaround for RouteOperation inhibits
    inh.entityInhibitAttributes.entities.each {|e|
      e.className = 'Operation' if e.className == 'Route' && e.attrib != '*'
    }
    inparm = @jcscode.pptEntityInhibitExceptionLotListInqInParm_struct.new(
      inh.entityInhibitID, oid(lot), inh.entityInhibitAttributes.entities,
      inh.entityInhibitAttributes.ownerID, any)
    #
    res = @manager.TxEntityInhibitExceptionLotListInq(@_user, inparm)
    return nil if rc(res) != 0
    return res.strEntityInhibitions.collect {|iex|
      iex.entityInhibitAttributes.strEntityInhibitExceptionLotInfos.collect {|e|
        InhibitException.new(e.lotID.identifier, e.claimTime, e.claimUserID.identifier, e.claimMemo, _extract_inhibit_info(iex))
      }
    }.flatten
  end

  # return 0 on success (used in 1 SiView regression test)
  def inhibit_exception_register(lots, inhs, params={})
    single = params[:single] != false   # pass single: false for reusable inhibit exceptions
    memo = params[:memo] || 'QA Test'
    lots = [lots] if lots.instance_of?(String)
    inhs = [inhs] unless inhs.instance_of?(Array) || inhs.java_class.array?
    $log.info "inhibit_exception_register #{lots}, (passed #{inhs.size} inhibits), #{params}"
    ies = []
    inhs.each {|inh|
      lots.each {|lot| ies << @jcscode.pptEntityInhibitExceptionLot_struct.new(oid(lot), inh.entityInhibitID, single)}
    }
    inparm = @jcscode.pptEntityInhibitExceptionLotReqInParm_struct.new(ies.to_java(@jcscode.pptEntityInhibitExceptionLot_struct), any)
    #
    res = @manager.TxEntityInhibitExceptionLotReq(@_user, inparm, memo)
    return rc(res)
  end

  # cancel an inhibit exception for the lot(s), return 0 on success (used in 1 SiView regression test)
  def inhibit_exception_cancel(lots, inhs, params={})
    single = params[:single] != false   # pass single: false for reusable inhibit exceptions
    memo = params[:memo] || 'QA Test'
    lots = [lots] if lots.instance_of?(String)
    inhs = [inhs] unless inhs.instance_of?(Array) || inhs.java_class.array?
    $log.info "inhibit_exception_cancel #{lots}, (passed #{inhs.size} inhibits), #{params}"
    ies = []
    inhs.each {|inh|
      lots.each {|lot| ies << @jcscode.pptEntityInhibitExceptionLot_struct.new(oid(lot), inh.entityInhibitID, single)}
    }
    inparm = @jcscode.pptEntityInhibitExceptionLotCancelReqInParm_struct.new(ies.to_java(@jcscode.pptEntityInhibitExceptionLot_struct), any)
    #
    res = @manager.TxEntityInhibitExceptionLotCancelReq(@_user, inparm, memo)
    return rc(res)
  end

  # # need to convert the result into our structure, UNUSED
  # def inhibit_config(params={})
  #   res = @manager.CS_TxEntityInhibitConfigurationInq(@_user, !!params[:all])
  #   return if rc(res) != 0
  #   ihash = {}
  #   res.strCombinations.each {|c|
  #     ihash[c.comboId.downcase.to_sym] = c.strEntities.map {|e| e.entityName}
  #   }
  #   return ihash
  # end

end
