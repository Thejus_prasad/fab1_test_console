=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.
=end


class SiView::MM
  PostProcessAction = Struct.new(:key, :seqno, :exec, :exec_seq, :watchdog, :ppId, :sync, :txId, :target_type, :eqp, :cj, :lot,
    :carrier, :commit, :status, :split_count, :error_action, :create_time, :update_time, :user, :claim_time, :claim_memo, :shop_data, :ext_event)

  # return array of manufacturing layers
  def mfg_layers(params={})
    res = @manager.TxMfgLayerListInq(@_user)
    return if rc(res) != 0
    return res if params[:raw]
    return res.strSubMfgLayerAttributes.collect {|e| e.manufacturingLayerID.identifier}
  end

  # get technology list - unused, use object_list('PosTechnology', '%')
  def technology_list(params={})
    res = @manager.TxTechnologyIDListInq(@_user)
    return nil if rc(res) != 0
    return res if params[:raw]
    return res.strTechnologyIDListAttributesSeq.collect {|t| t.technologyID.identifier}
  end

  # get product group list - unused, use object_list('PosProductGroup', '%')
  def productgroup_list(params={})
    res = @manager.TxProductGroupIDListInq(@_user)
    return nil if rc(res) != 0
    return res if params[:raw]
    return res.strProductGroupIDListAttributesSeq.collect {|p| p.productGroupID.identifier}
  end

  # return 0 on success
  def privilege_check(params={})
    user = params[:user]
    password = params[:password] || ''
    _u = user ? user_struct(user, password) : @_user
    eqp = oid(params[:eqp] || '')
    stocker = oid(params[:stocker] || '')
    products = oid((params[:products] || '').split)
    routes = oid((params[:routes] || '').split)
    lots = oid((params[:lots] || '').split)
    mrps = oid((params[:machine_recipes] || '').split)
    #
    res = @manager.TxPrivilegeCheckReq(_u, eqp, stocker, products, routes, lots, mrps)
    return rc(res)
  end

  # change the ppt env variables given in params, return 0 on success
  def environment_variable_update(params={})
    svc_struct = [].to_java(@jcscode.pptEnvVariableList_struct)
    ppt_struct = params.collect {|p, v| @jcscode.pptEnvVariableList_struct.new(p, v, any)}
    #
    res = @manager.TxEnvironmentVariableUpdateReq(@_user, svc_struct, ppt_struct)
    return rc(res)
  end

  # called by SiView watchdogs
  def delivery_req(eqp)
    res = @manager.TxCassetteDeliveryReq(@_user, oid(eqp))
    return rc(res)
  end

  # Postprocessing list for lot/cassette ('' is allowed), UNUSED
  def pp_list(params={})
    lot = params[:lot] || ''
    eqp = params[:eqp] || ''
    cj = params[:cj] || ''
    carrier = params[:carrier] || ''
    object = @jcscode.pptPostProcessTargetObject_struct.new(oid(eqp), oid(cj), oid(lot), oid(carrier), any)
    args = [params[:key] || '', params[:seqno] || -1, params[:watchdog] || '', params[:ppId] || '', params[:sync] || 1,
      params[:txId] || '', params[:target_type] || '', object, params[:status] || '', params[:passed_time] || -1,
      params[:user] || '', params[:start_create_time] || '', params[:end_create_time] || '',
      params[:start_update_time] || '', params[:end_update_time] || '', -1, !!params[:details]
    ]
    #
    res = @manager.TxPostProcessActionListInq__130(@_user, *args)
    return nil if rc(res) != 0
    return res if params[:raw]
    return res.strPostProcessActionInfoSeq.map {|pp|
      PostProcessAction.new(pp.dKey, pp.seqNo, pp.execCondition, pp.execConditionSeq, pp.watchdogName, pp.postProcID,
        pp.syncFlag, pp.txID, pp.targetType, pp.strPostProcessTargetObject.equipmentID.identifier,
        pp.strPostProcessTargetObject.controlJobID.identifier, pp.strPostProcessTargetObject.lotID.identifier,
        pp.strPostProcessTargetObject.cassetteID.identifier, pp.commitFlag, pp.status, pp.splitCount, pp.errorAction,
        pp.createTime, pp.updateTime, pp.claimUserID, pp.claimTime, pp.claimMemo, pp.claimShopDate, pp.extEventID)
    }
  end

  # # unused
  # def takeoutin(eqp, port, load_carrier, frommachine, fromport, params={})
  #   e2e = !!params[:e2e]
  #   cj = params[:cj] || '' # optional
  #   memo = params[:memo] || ''
  #   starttime = params[:tstart] || ''
  #   endtime = params[:tend] || ''
  #   mandatory = !!params[:mandatory]
  #   priority = params[:priority] || ''
  #   n2purgeflag = !!params[:n2purgeflag]
  #   stockerGroup = params[:stockergroup] || ''
  #   #
  #   ports = eqp_info(eqp, :raw=>true).equipmentPortInfo.strEqpPortStatus
  #   _ports = ports.select {|p| p.portID.identifier == port}

  #   tgtport = @jcscode.pptEqpTargetPortInfo_struct.new(oid(eqp),
  #     [@jcscode.pptPortGroup_struct.new(
  #       _ports[0].portGroup,
  #       _ports.collect {|_port|
  #         lot = lot_list_incassette(_port.loadedCassetteID.identifier)[0]
  #         @jcscode.pptPortID_struct.new(
  #           _port.portID,
  #           _port.loadSequenceNumber,
  #           _port.portUsage,
  #           '-',
  #           _port.loadPurposeType,
  #           _port.portState,
  #           _port.loadedCassetteID,
  #           _port.dispatchState,
  #           _port.dispatchState_TimeStamp,
  #           oid(lot),
  #           _port.loadedCassetteID,
  #           oid(''),
  #           oid(''),
  #           [ @jcscode.pptLotInfoOnPort_struct.new(oid(lot), '',any) ].to_java(@jcscode.pptLotInfoOnPort_struct),
  #           _port.cassetteCategoryCapability,
  #           any
  #           )
  #       }.to_java(@jcscode.pptPortID_struct),
  #       any
  #     )].to_java(@jcscode.pptPortGroup_struct),
  #     any
  #   )
  #   m = [@jcscode.pptToMachine_struct.new(oid(eqp), oid(port), any)].to_java(@jcscode.pptToMachine_struct)
  #   xferreq = [
  #     @jcscode.pptCarrierXferReq_struct.new(oid(load_carrier), oid(''), '',
  #         n2purgeflag, oid(frommachine), oid(fromport), stockerGroup, m,
  #         starttime, endtime, mandatory, priority, any)
  #   ].to_java(@jcscode.pptCarrierXferReq_struct)
  #   req_params = @jcscode.pptLotCassetteTakeOutInReqInParm_struct.new(e2e, oid(eqp), oid(cj), tgtport, xferreq, any)
  #   #
  #   res = @manager.TxLotCassetteTakeOutInReq(@_user, req_params, memo)
  #   rc(res)
  # end

  # # get process definition list - unused
  # def pd_list(params={})
  #   res = @manager.TxProcessDefinitionIDListInq(@_user)
  #   return nil if rc(res) != 0
  #   return res if params[:raw]
  #   return res.objectIdentifiers.collect {|p| p.identifier}
  # end

  # # Run a script (Pre1, Pre2, Post) - unused
  # def run_script(lot, eqp, params={})
  #   res = @manager.TxRunBRScriptReq(@_user, params[:phase] || 'Pre1', oid(lot), oid(eqp))
  #   return rc(res)
  # end

  # # Perform object privilege checks - unused
  # #
  # # @category (FPC_Add/FPC_Update/FPC_Delete) and pd id
  # #
  # def cs_functional_check(category, pd, params={})
  #   memo = params[:memo] || ''
  #   in_checks = {category => {'PD_ID' => pd}}.each_pair.collect {|cat, info_hash|
  #     info = info_hash.each_pair.collect {|k,v| @jcscode.pptHashedInfo_struct.new(k,v,any)}.to_java(@jcscode.pptHashedInfo_struct)
  #     @jcscode.pptCS_FunctionalCheck_struct.new(cat, info, any)
  #   }.to_java(@jcscode.pptCS_FunctionalCheck_struct)
  #   inparm = @jcscode.pptCS_FunctionalCheckReqInParm_struct.new(in_checks, any)
  #   #
  #   res = @manager.CS_TxFunctionalCheckReq(@_user, inparm, memo);
  #   return rc(res)
  # end

  # def XXXhistory_info(params={})
  #   tstart = params[:tstart] || ''
  #   tend = params[:tend] || ''
  #   count = params[:count] || 100
  #   table = params[:table] || ''
  #   hinfo = [@jcscode.pptHashedInfo_struct.new(k, v, any)].to_java(@jcscode.pptHashedInfo_struct)
  #   tinfo = @jcscode.pptTargetTableInfo_struct.new(table, hinfo, any)
  #   inparm = @jcscode.pptHistoryInformationInqInParm_struct.new(tstart, tend, category, tinfo, count, any)
  #   #
  #   res = @manager.TxHistoryInformationInq(@_user, inparm)
  #   return rc(res)
  # end

end
