=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: Steffen Steidten, 2009-05-03

History:
  2013-12-18 ssteidte, refactored EasySiViewR, SMFacade, XM, AsmViewR and Fab7 SiView
  2013-12-19 ssteidte, added interface to TxGetObjectInfo
  2014-02-20 ssteidte, separated SMFacade from SM, add SM release; added checkin, release and edit methods
  2014-03-05 cstenzel, added more SMClasses, tbd: verify them by requesting objects
  2014-10-30 dsteger,  added change notice creation
  2014-11-03 ssteidte, remove constants first for a clean re-load
  2015-04-14 ssteidte, move all struct defs out of the methods
  2015-08-31 ssteidte, preferrably use properties from etc/siview.conf
  2016-01-25 sfrieske, moved class specific methods to sm_xx.rb
  2017-04-18 sfrieske, remove R10 tweaks
  2018-04-05 sfrieske, separated CN and Active Versioning methods
  2021-02-05 sfrieske, use instance_of instead of kind_of
  2021-12-08 sfrieske, replaced Profile Struct by a Hash
  2022-01-07 sfrieske, fixed object_ids search for recipe, scriptparameter etc.
=end


module SiView

  class SM < Client
    # constants.each {|c| remove_const(c)} # for clean re-load
    Dir["#{File.dirname(__FILE__)}/sm_*rb"].each {|f| load(f)}

    attr_accessor :_userinfo, :_ccres


    def connect
      super
      @jcscode = @_props[:jcscode] || com.ibm.siview.sm
      profile = @iorparser.profiles.first
      $log.info "SM connection: #{profile[:host]}:#{profile[:port]}"
      begin
        $log.info 'creating BRServiceManager'
        obj = @orb.string_to_object(@iorparser.ior)
        @manager = @jcscode.BRServiceManagerHelper.narrow(obj)
        @_userinfo = logon
        $log.info "connected to SM as #{@_userinfo.userId}"
      rescue => e
        $log.warn "  error creating SM connection\n#{$!}"
        $log.debug e.backtrace.join("\n")
      end
    end

    # return brUserInfo_struct on success, else an Exception is thrown
    def logon(params={})
      privs = @jcscode.brPrivilegeGroupInfoSequenceHolder.new
      return @manager.TxLogon(@user, @password, privs)
    end

    def environment_variables
      Hash[@manager.TxGetAllEnvironmentVariables(@_userinfo).collect {|e| [e.environmentVariableName, e.value]}.sort]
    end

    def show_result(params={})
      params[:data] = @_ccres if params[:data] == :ccres   # show last consistency check result
      super(params)
    end

    # verify consistency check results, optionally display results, return true on success
    def check_ccres(ccresult)
      ok = true
      @_ccres = ccresult
      ccresult.value.each {|e|
        unless e.consistencyCheckResults.empty?
          ok = false
          $log.warn '  consistency check error:'
          $log.warn "  #{e.objectId.identifier}: #{e.consistencyCheckResults.map {|m| m.consistencyCheckMessage}}"
        end
      }
      return ok
    end

    # return sequence of SiView OIDs
    # 1) objs is an (array or sequence of) oid: returned as is 
    # 2) objs is an (array or sequence of) objects with an objectId field: the objectIds are returned
    # 3) objs is an (array of) string: TxGetObjectIds is called
    #     if objs is :all, searchcriteria are specified by the :search parameter, e.g. search: {mfglayer: 'XX'}
    #     pass e.g. n: 1 to get maximum n object ids returned from a search
    def object_ids(classname, objs, params={})
      # try to extract oids from a Ruby or Java array or object (e.g. brObjectInfo_struct)
      if objs.instance_of?(Array) && objs.first.respond_to?(:java_object)
        # return oids(objs)
        if objs.first.class == @jcscode.objectIdentifier_struct
          return objs.to_java(@jcscode.objectIdentifier_struct)
        else
          return objs.collect {|o| o.objectId}.to_java(@jcscode.objectIdentifier_struct)
        end
      elsif objs.respond_to?(:java_object)
        # return objs.java_object.kind_of?(Java::JavaArray) ? objs : [objs].to_java(objs.class)
        if objs.java_object.kind_of?(Java::JavaArray)
          if objs.empty?
            $log.warn "object_ids #{classname.inspect}, #{objs.inspect}: empty array"
            return
          elsif objs.first.class == @jcscode.objectIdentifier_struct
            return objs
          else
            return objs.collect {|o| o.objectId}.to_java(@jcscode.objectIdentifier_struct)
          end
        else
          if objs.class == @jcscode.objectIdentifier_struct
            return [objs].to_java(@jcscode.objectIdentifier_struct)
          else
            return [objs.objectId].to_java(@jcscode.objectIdentifier_struct)
          end
        end
      end
      #
      # search objects by string or array of strings
      ($log.warn "object_ids #{classname.inspect}, #{objs.inspect}: no object name"; return) if objs.empty?
      classid, fieldname = SMClasses[classname]
      fields = []
      if objs != :all
        ostrings = []
        versions = []
        sstrings = objs.instance_of?(String) ? [objs] : objs
        sstrings.each {|o|
          s = o.gsub('%', '*')
          if classname == :recipe && s.count('.') == 2
            # recipe with namespace
            ns, oname, version = s.split('.')
            fields << @jcscode.brFieldSearchCriterias_struct.new(false, SMSearchFields[classname][:namespace], [ns].to_java(:string))
          else
            # split off optional version info, pass object w/o version to get all versions (e.g. 'UTPD000')
            oname, version = s.split('.')
          end
          ostrings << oname
          versions << version if version
        }
        fields << @jcscode.brFieldSearchCriterias_struct.new(false, fieldname, ostrings.to_java(:string))
        # detect additional version/ec/code info
        unless versions.empty?
          if classname == :product
            vfieldname = SMSearchFields[classname][:ec]
          elsif classname == :equipmentstate
            vfieldname = SMSearchFields[classname][:code]
          elsif classname == :reasoncode
            vfieldname = SMSearchFields[classname][:code]
          elsif classname == :scriptparameter
            vfieldname = SMSearchFields[classname][:name]
          elsif classname == :userdefinedcode
            vfieldname = SMSearchFields[classname][:code]
          else
            vfieldname = fieldname.sub(/_[A-Z]*ID$/, '_VERSION')
          end
          fields << @jcscode.brFieldSearchCriterias_struct.new(false, vfieldname, versions.to_java(:string))
        end
      end
      if search = params[:search]
        search.each_pair {|fname, ostrings|
          ostrings = [ostrings] unless ostrings.instance_of?(Array)
          fields << @jcscode.brFieldSearchCriterias_struct.new(false, SMSearchFields[classname][fname], ostrings.to_java(:string))
        }
      end
      udsfields = [].to_java(@jcscode.brFieldSearchCriterias_struct)
      mandfields = [].to_java(@jcscode.brFieldSearchCriterias_struct)
      srch = @jcscode.brSearchCriteria_struct.new(false, false,
        fields.to_java(@jcscode.brFieldSearchCriterias_struct), false, udsfields, mandfields, params[:n] || 0)
      #
      return @_res = @manager.TxGetObjectIds(classid, srch, @_userinfo)
    end

    # return SiView object properties, objs can be passed as String, Array or SiView object
    def object_info(classname, objs, params={})
      oids = object_ids(classname, objs, params) || return
      classid = SMClasses[classname].first
      #
      res = @_res = @manager.TxGetObjectInfo(classid, oids, @_userinfo)
      return res if params[:raw]
      return extract_object_info(classname, res, {details: params[:details], log: params[:log]})
    end

    # return array of Array of ObjectInfo structs, from brObjectInfo_struct including class specific data from Any
    def extract_object_info(classname, oinfos, params={})
      return oinfos.collect {|o|
        desc = nil
        udata = nil
        specific = nil
        # if o.classInfo.value
        if o.classInfo.type.kind != Java::OrgOmgCORBA::TCKind.tk_null
          # extract class specific info from Any
          ci = extract_si(o.classInfo)
          desc = ci.description if ci.respond_to?(:description)
          udata = Hash[ci.userDataSets.collect {|u| [u.name, u.value]}] if ci.respond_to?(:userDataSets)
          specific = case classname
          when :carrier
            {category: ci.carrierCategory.identifier, contents: ci.contents,
              pm_max: ci.intervalBetweenPM, starts_max: ci.maximumStartCount}
          when :eqp
            buffers = {}
            ci.bufferResources.each {|b| buffers[b.bufferCategory] = b.capacity.to_i}
            ports = ci.portResources.map {|p|
              {port: p.portResourceName, pg: p.portGroupName, seq: p.loadSequenceNumber,
                purpose: p.loadPurposeTypeId, carrier_categories: p.carrierCategories.map {|e| e.identifier}}
            }
            {eqpowner: ci.equipmentOwner.identifier, eqptype: ci.equipmentType.identifier, e2e: ci.eqpToEqpTransferFlag,
              takeoutin: ci.takeOutInTransferFlag, slm: ci.slmCapabilityFlag, mrtype: ci.multipleRecipeCapability,
              max_batchsize: ci.maximumProcessBatchSize, min_batchsize: ci.minimumProcessBatchSize, pjctrl: ci.processJobControlFlag,
              ports: ports, ib: buffers != {}, buffers: buffers}
          when :equipmentstate
            {available: ci.equipmentAvailableFlag, conditional: ci.conditionalAvailableFlag, sublottypes: ci.availableSubLotTypes.to_a}
          when :lrcp
            {lrcptype: ci.recipeType,
              recipe: Hash[ci.defaultRecipeSettings.collect {|e|
                parameters = Hash[e.recipeParameters.collect {|p| [p.name, p.defaultValue]}]
                [e.recipe.identifier, parameters]}]
            }
          when :lottype
            ci.subLotTypes.collect {|slt| {sublottype: slt.subLotTypeId, description: slt.description, duration: slt.duration}}
          when :pd
            lrcps = {
              product: Hash[ci.logicalRecipes.collect {|e| [e.product.identifier, e.logicalRecipe.identifier]}],
              productgroup: Hash[ci.logicalRecipesByProductGroup.collect {|e| [e.productGroup.identifier, e.logicalRecipe.identifier]}],
              technology: Hash[ci.logicalRecipesByTechnology.collect {|e| [e.technology.identifier, e.logicalRecipe.identifier]}],
              nil=>ci.defaultLogicalRecipe.identifier
            }
            {pdname: ci.processDefinitionName, pdtype: ci.pdType, department: ci.department.identifier, lrcps: lrcps}
          when :modulepd
            ci.moduleProcessOperations.collect {|o| ModulepdOpInfo.new(
              o.operationNumber, o.processDefinition.identifier, o.mandatoryOperationRequiredFlag,
              o.pre1Script.identifier, o.defaultProcessLagTime,
              o.defaultTimeRestrictions.collect {|e| QtInfo.new(e.targetOperationNumber, e.actionOperationNumber, e.duration, e.action,
                e.reasonCode.identifier, e.reworkMainProcessDefinition.identifier, e.timing, e.customField)},
              o.subProcessDefinitions.collect {|e| {mainpd: e.mainProcessDefinition.identifier, joining_opNo: e.joiningOperationNumber}},
              o.reworkProcessDefinitions.collect {|e| {mainpd: e.mainProcessDefinition.identifier, joining_opNo: e.joiningOperationNumber}}
            )}
          when :mainpd
            if params[:details]
              modules = ci.moduleProcessDefinitions.collect {|m|
                {number: m.moduleNumber, name: m.moduleProcessDefinition.identifier, stage: m.stage.identifier}
              }
              ops = ci.mainProcessOperations.collect {|o| MainpdOpInfo.new(
                o.operationNumber, o.processDefinition.identifier, o.moduleNumber, o.moduleProcessDefinition.identifier,
                o.stage.identifier, o.photoLayer.identifier, o.carrierCategory.identifier, o.pre1Script.identifier, o.defaultProcessLagTime,
                o.defaultTimeRestrictions.collect {|e| QtInfo.new(e.targetOperationNumber, e.actionOperationNumber, e.duration, e.action,
                  e.reasonCode.identifier, e.reworkMainProcessDefinition.identifier, e.timing, e.customField)},
                o.subProcessDefinitions.collect {|e| {mainpd: e.mainProcessDefinition.identifier, joining_opNo: e.joiningOperationNumber}},
                o.reworkProcessDefinitions.collect {|e| {mainpd: e.mainProcessDefinition.identifier, joining_opNo: e.joiningOperationNumber}},
                Hash[o.userDataSets.collect {|u| [u.name, u.value]}]
                )}
            else
              modules = nil
              ops = nil
            end
            {flowtype: ci.processFlowType, dynamic: ci.dynamicRouteFlag, layer: ci.mfgLayer.identifier,
              startbank: ci.startBank.identifier, endbank: ci.endBank.identifier, modules: modules, operations: ops}
          when :product
            {state: ci.state, productcategory: ci.productCategory.identifier, producttype: ci.productType, productowner: ci.productOwner.identifier,
              srcproducts: ci.sourceProducts.collect {|sp| sp.identifier},
              route: ci.mainProcessDefinition.identifier, reticleset: ci.reticleSet.identifier,
              productgroup: ci.productGroup.identifier, yield: ci.plannedYield}
          when :productgroup
            {technology: ci.technology.identifier, yield: ci.plannedYield, diecount: ci.grossDieCount,
              usergroups: ci.userGroups.collect {|ug| ug.identifier} }
          when :recipe
            {eqps: ci.equipments.collect {|e| e.identifier}}
          when :reticle
            {rg: ci.reticleGroup.identifier}
          when :reticleset
            {rgs: ci.defaultReticleGroups.collect {|e|
              {photolayer: e.photoLayer.identifier, override: e.overrideFlag, rgs: e.reticleGroups.collect {|rg| rg.identifier}}
            }}
          when :stage
            {stagenumber: ci.stageNumber}
          when :user
            {areagroups: ci.pptAreaGroups.collect {|ag| ag.identifier}, usergroups: ci.brmUserGroups.collect {|ug| ug.identifier} }
          end
        end
        pi = o.propertyInfo
        if params[:log]   # for cptest
          log = {created: siview_time(pi.createdDateTime), created_by: pi.createdUser.identifier,
            released: siview_time(pi.lastReleasedDateTime), released_by: pi.lastReleasedUser.identifier}
          # cn = pi.changeNotice.identifier
        else
          log = nil
        end
        ObjectInfo.new(o.objectId.identifier, desc, pi.currentSetUpState, pi.permission, pi.owner.identifier, udata, specific, log)
      }
    end

    # edit checkout, return sequence of brObjectInfo_struct on success
    def edit_checkout(classname, objs)
      oids = object_ids(classname, objs)
      $log.info "edit_checkout #{classname.inspect}, #{oids.collect {|o| o.identifier}}"
      #
      res = @_res = @manager.TxEditCheckOut(oids, @_userinfo)
      return res
    end

    # rollback (recover) InEdit objects, return sequence of brObjectInfo_struct on success, UNUSED
    def rollback(classname, objs)
      if objs.instance_of?(String) && (objs.include?('*') || objs.include?('%'))
        $log.warn "rollback #{classname.inspect}, #{objs.inspect} error: wildcards are not allowed to protect ourselves"
        return false
      end
      oids = object_ids(classname, objs, params)
      $log.info "rollback #{classname.inspect}, #{oids.collect {|o| o.identifier}}"
      #
      res = @_res = @manager.TxRollback(oids, @_userinfo)
      return res 
    end

    # delete objects currently being built and verify, return true on success
    def delete_build(classname, objs)
      if objs.instance_of?(String) && (objs.include?('*') || objs.include?('%'))
        $log.warn "delete_build #{classname.inspect}, #{objs.inspect} error: wildcards are not allowed to protect ourselves"
        return false
      end
      oids = object_ids(classname, objs)
      $log.info "delete_build #{classname.inspect}, #{oids.collect {|o| o.identifier}}"
      #
      begin
        @manager.TxDeleteBuild(oids, @_userinfo)
        return true
      rescue
        $log.warn "  #{$!}"
        return false
      end
    end

    # consistency check run by SM client before TxDeleteProduction is called, used in SM_RouteRelease only
    #
    # return sequence of brObjectConsistencyCheckResult_struct
    def delete_production_consistency_check(classname, objs, params={})
      oids = object_ids(classname, objs)
      $log.info "delete_production_consistency_check #{classname.inspect}, #{oids.collect {|o| o.identifier}}"
      #
      res = @_res = @manager.TxDeleteProductionConsistencyCheck(oids, !!params[:quickcheck], @_userinfo)
      return res
    end

    # delete released objects and verify, return true on success or if no objects found
    def delete_production(classname, objs)
      if objs.instance_of?(String) && (objs.include?('*') || objs.include?('%'))
        $log.warn "delete_production #{classname.inspect}, #{objs.inspect} error: wildcards are not allowed to protect ourselves"
        return false
      end
      oids = object_ids(classname, objs) || return
      $log.info "delete_production #{classname.inspect}, #{oids.collect {|o| o.identifier}}"
      #
      begin
        @manager.TxDeleteProduction(oids, @_userinfo)
        return true
      rescue
        $log.warn "  #{$!}"
        return false
      end
    end

    # try to delete an object regardless of its state, return true on success or if no objects found
    def delete_objects(classname, objs)
      oinfos = object_info(classname, objs, raw: true) || return
      onames = oinfos.collect {|o| o.objectId.identifier}
      $log.info "delete_objects #{classname.inspect}, #{onames}"
      ($log.warn '  no objects to delete'; return true) if oinfos.empty?
      ret = true
      oinfos.each {|oinfo|
        unless oinfo.propertyInfo.changeNotice.identifier.empty?
          edit_checkout(classname, oinfo) || return
          oinfo.propertyInfo.changeNotice.identifier = ''
          # edit_checkin(classname, oinfo) || return
        end
        if oinfo.propertyInfo.lastReleasedDateTime.empty?
          ret &= delete_build(classname, oinfo)
        else
          ret &= delete_production(classname, oinfo)
        end
      }
      return ret
    end

    # save objects, return sequence of brObjectInfo_struct on success, used for SM_EditComplete only
    def save_objects(classname, oinfos)
      onames = oinfos.collect {|oinfo| oinfo.objectId.identifier}
      $log.info "save_objects #{classname.inspect}, #{onames}"
      classid = SMClasses[classname].first
      #
      res = @_res = @manager.TxSaveObject(classid, oinfos, @_userinfo)
      return res
    end

    # edit complete, return sequence of brObjectInfo_struct on success, used in SM_EditComplete
    def edit_complete(classname, objs)
      oids = object_ids(classname, objs)
      $log.info "edit_complete #{classname.inspect}, #{oids.collect {|o| o.identifier}}"
      ccres = @jcscode.brObjectConsistencyCheckResultSequenceHolder.new
      #
      res = @_res = @manager.TxEditComplete(oids, @_userinfo, ccres)
      check_ccres(ccres) || return
      return res
    end

    # edit complete and checkin, return sequence of brObjectInfo_struct on success, UNUSED
    def edit_complete_checkin(classname, objs)
      oids = object_ids(classname, objs)
      $log.info "edit_complete_checkin #{classname.inspect}, #{oids.collect {|o| o.identifier}}"
      classid = SMClasses[classname].first
      ccres = @jcscode.brObjectConsistencyCheckResultSequenceHolder.new
      #
      res = @_res = @manager.TxEditCompleteAndCheckIn(classid, oids, @_userinfo, ccres)
      check_ccres(ccres) || return
      return res
    end

    # save, edit complete and checkin in one step, return sequence of brObjectInfo_struct on success
    def save_edit_complete(classname, oinfos)
      onames = oinfos.collect {|oinfo| oinfo.objectId.identifier}
      $log.info "save_edit_complete #{classname.inspect}, #{onames}"
      classid = SMClasses[classname].first
      ccres = @jcscode.brObjectConsistencyCheckResultSequenceHolder.new
      #
      res = @_res = @manager.TxSaveAndEditCompleteAndCheckIn(classid, oinfos, @_userinfo, ccres)
      check_ccres(ccres) || return
      return res
    end

    # release objects, return sequence of brObjectInfo_struct on success
    def release_objects(classname, objs)
      oids = object_ids(classname, objs)
      $log.info "release_objects #{classname.inspect}, #{oids.collect {|o| o.identifier}}"
      classid = SMClasses[classname].first
      #
      coids = oids.collect {|oid| @jcscode.brClassObjectId_struct.new(classid, oid)}
      rmpdd = [].to_java(@jcscode.brRelatedMainProcessDefinitionData_struct)
      rmpdd2 = [].to_java(@jcscode.brRelatedMainProcessDefinitionData_struct)
      ccres = @jcscode.brObjectConsistencyCheckResultSequenceHolder.new
      #
      if classname == :changenotice
        relinfo = @manager.TxGetAllRelatedReleaseReportWithChangeNotice(coids, false, @_userinfo)
        res = @_res = @manager.TxReleaseObjectsWithChangeNotice(oids, relinfo.relatedObjectIds, rmpdd, rmpdd2, false, @_userinfo, ccres)
      else
        relinfo = @manager.TxGetAllRelatedReleaseReportWithoutChangeNotice(coids, false, @_userinfo)
        res = @_res = @manager.TxReleaseObjectsWithoutChangeNotice(oids, relinfo.relatedObjectIds, rmpdd, rmpdd2, false, @_userinfo, ccres)
      end
      check_ccres(ccres) || return
      return res
    end

    # copy an object from a template, return sequence of brObjectInfo_struct on success
    def copy_object(classname, objs, newnames, params={}, &blk)
      newnames = [newnames] if newnames.instance_of?(String)
      $log.info "copy_object #{classname.inspect}, #{objs.inspect}, #{newnames.inspect}"
      origoinfos = object_info(classname, objs, raw: true) || return
      ($log.warn "  no unique src object (#{origoinfos.size} selected)"; return) if origoinfos.size != 1
      #
      if params[:ondemand]  # currently all or nothing
        newnames.select! {|oname| object_ids(classname, oname).empty?}
        ($log.info '  new objects already exist'; return []) if newnames.empty?
      end
      #
      classid = SMClasses[classname].first
      oinfos = @manager.TxCopyObject(classid, origoinfos.first.objectId, newnames.to_java(:string), origoinfos.first.propertyInfo, @_userinfo)
      return if oinfos.size != newnames.size
      # optionally set description and call block with oinfos to edit the new objects, blk must return a truthy value
      if desc = params[:description]
        $log.info "  setting description: #{desc.inspect}"
        oinfos.each {|oinfo|
          ci = extract_si(oinfo.classInfo)
          ci.description = desc
          oinfo.classInfo = insert_si(ci)
        }
      end
      blk.call(oinfos) || ($log.warn '  editing failed'; return) if blk
      # save and release
      oinfos2 = save_edit_complete(classname, oinfos) || return
      return oinfos2 if params[:release] == false   # currently for SM_xxRelease tests only
      return release_objects(classname, oinfos2)
    end

  end

end
