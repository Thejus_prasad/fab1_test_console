=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: Steffen Steidten, 2009-05-03; Daniel Steger, 2014-10-30

History:
=end


class SiView::SM

  # # save objects with change notice, UNUSED, TODO: remove
  # def save_objects_with_cn(classname, oinfos, cn, params={})
  #   oinfos.each {|oid| oid.propertyInfo = oid(cn) } #add Change notice to each object
  #   save_objects(classname, oinfos, params)
  # end

  # # save objects and edit complete with change notice
  # # the cn object needs to be refer to an object reference, UNUSED, TODO: remove
  # def save_edit_complete_with_cn(classname, oinfos, cn, params={})
  #   oinfos.each {|oid| oid.propertyInfo.changeNotice = cn } #add Change notice to each object
  #   save_edit_complete(classname, oinfos, params)
  # end

  # # create a new change notice object, replaced by create_changenotice
  # def create_change_notice(name, params={})
  #   oinfo = @jcscode.brObjectInfo_struct.new
  #   oinfo.objectId = oid(name)
  #   oinfo.propertyInfo = @jcscode.brPropertyInfo__090.new(
  #     oid(''), '', oid(''), '' ,oid(''), '', oid(''), '', oid(''), '', oid(''), '' ,oid(''), '', oid(''), '',
  #     oid(''), oid(''), '', '', '', '', '', 'Public', '', '', oid(''), oid(''), oid(''), oid(@user), false)
  #   oinfo.documentEntityInfo = @jcscode.brDocumentEntityInfo_struct.new('', false, false, false,
  #     oid(''), '', oid(''), '', oid(''), '', oid(''), '', oid(''), '', oid(''), ''
  #   )
  #   cn_info = @jcscode.brChangeNoticeInfo__110.new(name, name, 'QA Test', [].to_java(:string),
  #     [].to_java(@jcscode.brChangeItemData_struct),
  #     [].to_java(@jcscode.brChangeItemData_struct),
  #     [].to_java(@jcscode.userDataSet_struct), any)
  #   oinfo.classInfo = insert_si(cn_info)
  #   res = save_objects(:changenotice, [oinfo].to_java(@jcscode.brObjectInfo_struct), raw: true).first || return
  #   return res if params[:raw]
  #   return res.objectId
  # end

  # # remove the change notice from an object, return true on success, UNUSED, TODO: remove
  # def object_remove_cn(classname, objs, params={})
  #   $log.info "object_remove_cn #{classname.inspect}, #{objs.inspect}, #{params}"
  #   edit_objects(classname, objs, params) {|oinfos|
  #     oinfos.each {|e| e.propertyInfo.changeNotice.identifier = ''}
  #   }
  # end

end
