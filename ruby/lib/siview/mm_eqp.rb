=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.
=end


class SiView::MM
  EqpInfo = Struct.new(:eqp, :category, :pjctrl, :status,
    :loaded_carriers, :disp_load_carriers, :rsv_carriers, :cjs, :rsv_cjs, :ports, :chambers, :ib)
  EqpChamber = Struct.new(:chamber, :status)
  EqpPort = Struct.new(:port, :pg, :category, :seq, :mode, :usage, :state, :rsv_carrier,
    :loaded_carrier, :disp_load_carrier, :disp_unload_carrier, :disp_state, :disp_time, :cj, :purpose)
  EqpIB = Struct.new(:loaded_carriers, :rsv_carriers, :buffers)


  # return equipment list, optionally filtered by name regex or nil
  def eqp_list(params={})
    res = @manager.TxEquipmentIDListInq(@_user)
    return if rc(res) != 0
    return res if params[:raw]
    regex = params[:eqp] ? Regexp.new(params[:eqp]) : nil
    return res.objectIdentifiers.collect {|e|
      next if regex && e.identifier !~ regex
      e.identifier
    }.compact
  end

  # return strEqpInfoList for a product/operation combination or nil
  def eqp_list_byop(product, op)
    # not used: FPC and whiteDef search
    inparm = @jcscode.pptEqpListByOperationInqInParm_struct.new(oid(product), oid(op),
      [].to_java(:string), '', any)
    #
    res = @manager.TxEqpListByOperationInq(@_user, inparm)
    return if rc(res) != 0
    return res.strEqpInfoList
  end

  # return an EqpInfo struct
  def eqp_info(eqp, params={})
    res = eqp_info_raw(eqp, params) || return
    return _extract_eqp_info(res)
  end

  # take eqp_info_raw data and return EqpInfo struct
  def _extract_eqp_info(res, params={})
    rsv_carriers = []
    loaded_carriers = []
    disp_load_carriers = []
    ports = res.equipmentPortInfo.strEqpPortStatus.collect {|p|
      rsvc = p.loadResrvedCassetteID.identifier
      rsv_carriers << rsvc unless rsvc.empty?
      lddc = p.loadedCassetteID.identifier
      loaded_carriers << lddc unless lddc.empty?
      dspc = p.dispatchLoadCassetteID.identifier
      disp_load_carriers << dspc unless dspc.empty?
      EqpPort.new(p.portID.identifier, p.portGroup, p.cassetteCategoryCapability.to_a, p.loadSequenceNumber,
        p.operationModeID.identifier, p.portUsage, p.portState, rsvc, lddc, dspc, p.dispatchUnloadCassetteID.identifier,
        p.dispatchState, p.dispatchState_TimeStamp, p.cassetteControlJobID.identifier, p.loadPurposeType)
    }
    chambers = res.equipmentChamberInfo.strEqpChamberStatus.collect {|c|
      EqpChamber.new(c.chamberID.identifier, "#{c.E10Status.identifier}.#{c.chamberStatusCode.identifier}")
    }
    if res.respond_to?(:equipmentBRInfoForInternalBuffer)
      ib_loaded_carriers = []
      ib_rsv_carriers = []
      buffer = {}
      res.equipmentInternalBufferInfo.each {|b|
        buffer[b.bufferCategory] = b.strShelfInBuffer.size  # b.bufferCapacity.to_i
        b.strShelfInBuffer.each {|s|
          ib_loaded_carriers << s.loadedCarrierID.identifier unless s.loadedCarrierID.identifier.empty?
          ib_rsv_carriers << s.reservedCarrierID.identifier unless s.reservedCarrierID.identifier.empty?
        }
      }
      brinfo = res.equipmentBRInfoForInternalBuffer
      ib = EqpIB.new(ib_loaded_carriers, ib_rsv_carriers, buffer)
    else
      brinfo = res.equipmentBRInfo
      ib = nil
    end
    #
    return EqpInfo.new(res.equipmentID.identifier, brinfo.equipmentCategory, brinfo.processJobLevelCtrl,
      "#{res.equipmentStatusInfo.E10Status}.#{res.equipmentStatusInfo.equipmentStatusCode.identifier}",
      loaded_carriers, disp_load_carriers, rsv_carriers,
      res.equipmentInprocessingControlJob.collect {|e| e.controlJobID.identifier},
      res.equipmentReservedControlJobInfo.strStartReservedControlJob.collect {|e| e.controlJobID.identifier},
      ports, chambers, ib)
  end

  # return pptEqpInfoInqResult or pptEqpInfoForInternalBufferInqResult
  def eqp_info_raw(eqp, params={})
    eqpID = eqp.instance_of?(String) ? oid(eqp) : eqp
    if params[:ib]
      ib = true
    else
      # not an IB tool or not specified, 10 flags
      res = @manager.TxEqpInfoInq__120(@_user, eqpID,
        true, true, true, true, true, true, true, true, true, true)
      ib = res.strResult.returnCode == '548'  # internal buffer tool
    end
    if ib
      # IB tool specified or detected, 10 flags
      res = @manager.TxEqpInfoForInternalBufferInq__120(@_user, eqpID,
        true, true, true, true, true, true, true, true, true, true)
    end
    return if rc(res) != 0
    return res
  end

  # loads a carrier on an equipment load port
  # autoib: automatically transfer carrier to internal buffer on ib tools
  # purpose: load purpose, defaults to 'Process Lot'
  #
  # return 0 on success
  def eqp_load(eqp, port, carrier, params={})
    purpose = params[:purpose] || 'Process Lot'
    autoib = params[:autoib] != false
    memo = params[:memo] || ''
    eqpiraw = params[:eqpinfo]
    if params.has_key?(:ib)
      ib = params[:ib]
    else
      eqpiraw ||= eqp_info_raw(eqp) || return
      ib = eqpiraw.respond_to?(:equipmentBRInfoForInternalBuffer)
    end
    if port.nil?
      eqpiraw ||= eqp_info_raw(eqp, ib: ib) || return
      # autodetect load port if nil
      eqpiraw.equipmentPortInfo.strEqpPortStatus.each {|p|
        next if p.portState == 'Down' && !p.operationModeID.identifier.start_with?('Off-Line')
        next unless p.portUsage.include?('INPUT')
        next unless p.loadedCassetteID.identifier.empty?
        port = p.portID
        break
      }
    end
    eqpID = eqp.instance_of?(String) ? (eqpiraw ? eqpiraw.equipmentID : oid(eqp)) : eqp
    portID = port.instance_of?(String) ? oid(port) : port
    ($log.warn "eqp_load #{eqpID.identifier.inspect}:  no port"; return) if port.nil?
    $log.info "eqp_load #{eqpID.identifier.inspect}, #{portID.identifier.inspect}, #{carrier.inspect}, purpose: #{purpose.inspect}"
    #
    if ib
      res = @manager.TxLoadingLotForInternalBufferRpt(@_user, eqpID, oid(carrier), portID, purpose, memo)
    else
      res = @manager.TxLoadingLotRpt(@_user, eqpID, oid(carrier), portID, purpose, memo)
    end
    ret = rc(res)
    $log.warn {'  ensure eqp status is 2WPR before loading carriers'} if ret == 2800
    return ret unless ret == 0 && autoib && ib
    return eqp_carriertoib(eqpID, portID, carrier)
  end

  # unload a carrier from an equipment load port
  # autoib: automatically transfer carrier from internal buffer on ib tools, defaults to true
  #
  # return 0 on success
  def eqp_unload(eqp, port, carrier, params={})
    carrierID = carrier.instance_of?(String) ? oid(carrier) : carrier
    autoib = params[:autoib] != false
    memo = params[:memo] || ''
    eqpiraw = params[:eqpinfo]
    if params.has_key?(:ib)
      ib = params[:ib]
    else
      eqpiraw ||= eqp_info_raw(eqp) || return
      ib = eqpiraw.respond_to?(:equipmentBRInfoForInternalBuffer)
    end
    if port.nil?
      eqpiraw ||= eqp_info_raw(eqp, ib: ib) || return
      # autodetect unload port if nil
      eqpiraw.equipmentPortInfo.strEqpPortStatus.each {|p|
        if p.loadedCassetteID.identifier == carrierID.identifier
          port = p.portID
          break
        end
        # IB: do not break when a free port has been found, another one might have the carrier loaded
        if ib
          next if p.portState == 'Down'
          next unless p.portUsage.include?('OUTPUT')
          next unless p.loadedCassetteID.identifier.empty?
          port = p.portID
          break
        end
      }
    end
    eqpID = eqp.instance_of?(String) ? (eqpiraw ? eqpiraw.equipmentID : oid(eqp)) : eqp
    ($log.warn "eqp_unload #{eqpID.identifier.inspect}:  no port"; return) if port.nil?
    portID = port.instance_of?(String) ? oid(port) : port
    $log.info "eqp_unload #{eqpID.identifier.inspect}, #{portID.identifier.inspect}, #{carrierID.identifier.inspect}"
    if autoib && ib
      done = false
      eqpiraw ||= eqp_info_raw(eqp, ib: true)
      eqpiraw.equipmentInternalBufferInfo.each {|b|
        b.strShelfInBuffer.each {|s|
          if s.loadedCarrierID.identifier == carrierID.identifier
            eqp_carrierfromib_reserve(eqpID, portID, carrierID).zero? || return
            eqp_carrierfromib(eqpID, portID, carrierID).zero? || return
            done = true
            break
          end
          break if done
        }
      }
    end
    #
    if ib
      res = @manager.TxUnloadingLotForInternalBufferRpt(@_user, eqpID, carrierID, portID, memo)
    else
      res = @manager.TxUnloadingLotRpt(@_user, eqpID, carrierID, portID, memo)
    end
    return rc(res)
  end

  # return 0 on success
  def eqp_carriertoib(eqp, port, carrier, params={})
    memo = params[:memo] || ''
    eqpID = eqp.instance_of?(String) ? oid(eqp) : eqp
    portID = port.instance_of?(String) ? oid(port) : port
    carrierID = carrier.instance_of?(String) ? oid(carrier) : carrier
    $log.info "eqp_carriertoib #{eqpID.identifier.inspect}, #{portID.identifier.inspect}, #{carrierID.identifier.inspect}"
    #
    res = @manager.TxMoveCarrierToInternalBufferRpt(@_user, eqpID, portID, carrierID, memo)
    return rc(res)
  end

  # return 0 on success
  def eqp_carrierfromib(eqp, port, carrier, params={})
    memo = params[:memo] || ''
    eqpID = eqp.instance_of?(String) ? oid(eqp) : eqp
    portID = port.instance_of?(String) ? oid(port) : port
    carrierID = carrier.instance_of?(String) ? oid(carrier) : carrier
    $log.info "eqp_carrierfromib #{eqpID.identifier.inspect}, #{portID.identifier.inspect}, #{carrierID.identifier.inspect}"
    #
    res = @manager.TxMoveCarrierFromInternalBufferRpt(@_user, eqpID, portID, carrierID, memo)
    return rc(res)
  end

  # return 0 on success
  def eqp_carrierfromib_reserve(eqp, port, carrier, params={})
    memo = params[:memo] || ''
    eqpID = eqp.instance_of?(String) ? oid(eqp) : eqp
    portID = port.instance_of?(String) ? oid(port) : port
    carrierID = carrier.instance_of?(String) ? oid(carrier) : carrier
    $log.info "eqp_carrierfromib_reserve #{eqpID.identifier.inspect}, #{portID.identifier.inspect}, #{carrierID.identifier.inspect}"
    #
    res = @manager.TxUnloadingLotsReservationForInternalBufferRpt(@_user, eqpID, carrierID, portID, memo)
    return rc(res)
  end

  # return 0 on success
  def eqp_carrierfromib_reserve_cancel(eqp, carrier, params={})
    eqpID = eqp.instance_of?(String) ? oid(eqp) : eqp
    carrierID = carrier.instance_of?(String) ? oid(carrier) : carrier
    $log.info "eqp_carrierfromib_reserve_cancel #{eqpID.identifier.inspect}, #{carrierID.identifier.inspect}"
    #
    res = @manager.TxUnloadingLotsReservationCancelForInternalBufferReq(@_user, eqpID, carrierID)
    return rc(res)
  end

  # return 0 on success
  def eqp_status_adjust(eqp, params={})
    res = @manager.TxEqpStatusAdjustReq(@_user, oid(eqp), params[:memo] || '')
    return rc(res)
  end

  def eqp_lotsinfo_for_opestart(eqp, carriers, params={})
    # internally used by EI
    eqpID = eqp.instance_of?(String) ? oid(eqp) : eqp
    carriers = [carriers] if carriers.instance_of?(String)
    ib = params.has_key?(:ib) ? params[:ib] : eqp_info_raw(eqpID).respond_to?(:equipmentBRInfoForInternalBuffer)
    if ib
      li = @manager.TxLotsInfoForOpeStartForInternalBufferInq(@_user, eqpID, oids(carriers))
    else
      li = @manager.TxLotsInfoForOpeStartInq(@_user, eqpID, oids(carriers))
    end
    return if rc(li) != 0
    return li
    # # specify reticles, recipes etc., seems to be unused (preserve!)
    # reticles = params[:reticle] || params[:reticles]
    # reticles = [reticles] if reticles.instance_of?(String)
    # li.strStartCassette.each {|c|
    #   c.strLotInCassette.each {|l|
    #     rcp = l.strStartRecipe
    #     if dc = params[:dc]
    #       rcp.dataCollectionFlag = dc
    #     end
    #     if params[:nodcitems]
    #       $log.info '  no dcitems reported to SiView'
    #       rcp.strDCDef = [].to_java(@jcscode.pptDCDef_struct)
    #     end
    #     if lrecipe = params[:lrecipe]
    #       rcp.logicalRecipeID = oid(lrecipe)
    #     end
    #     if mrecipe = params[:mrecipe]
    #       rcp.machineRecipeID = oid(mrecipe)
    #     end
    #     if precipe = params[:precipe]
    #       rcp.physicalRecipeID = precipe
    #     end
    #     if reticles
    #       rcp.strStartReticle = reticles.each_with_index.collect {|r, i|
    #         @jcscode.pptStartReticle_struct.new(i + 1, oid(r), any)
    #       }.to_java(@jcscode.pptStartReticle_struct)
    #     end
    #   }
    # }
    # return li
  end

  # process lots in the carrier(s), return cj on success or nil
  def eqp_opestart(eqp, carriers, params={})
    eqpID = eqp.instance_of?(String) ? oid(eqp) : eqp
    carriers = [carriers] if carriers.instance_of?(String)
    jobpause = !!params[:jobpause]
    memo = params[:memo] || ''
    ib = params.has_key?(:ib) ? params[:ib] : eqp_info_raw(eqpID).respond_to?(:equipmentBRInfoForInternalBuffer)
    #
    $log.info "eqp_opestart #{eqpID.identifier.inspect}, #{carriers}"
    # get lots info struct
    li = eqp_lotsinfo_for_opestart(eqpID, carriers, ib: ib) || return
    #
    if ib
      res = @manager.TxOpeStartForInternalBufferReq(@_user, li.equipmentID, li.controlJobID, li.strStartCassette, jobpause, memo)
    else
      res = @manager.TxOpeStartReq(@_user, li.equipmentID, li.portGroupID, li.controlJobID, li.strStartCassette, jobpause, memo)
    end
    return if rc(res) != 0
    cj = res.controlJobID.identifier
    $log.info "  cj=#{cj.inspect}"
    return params[:raw] ? res.controlJobID : cj
  end

  # return 0 on success
  def eqp_opestart_cancel(eqp, cj, params={})
    eqpID = eqp.instance_of?(String) ? oid(eqp) : eqp
    cjID = cj.instance_of?(String) ? oid(cj) : cj
    memo = params[:memo] || ''
    ib = params.has_key?(:ib) ? params[:ib] : eqp_info_raw(eqpID).respond_to?(:equipmentBRInfoForInternalBuffer)
    $log.info "eqp_opestart_cancel #{eqpID.identifier.inspect}, #{cjID.identifier.inspect}#{', ib: true' if ib}"
    if ib
      res = @manager.TxOpeStartCancelForInternalBufferReq(@_user, eqpID, cjID, memo)
    else
      res = @manager.TxOpeStartCancelReq(@_user, eqpID, cjID, memo)
    end
    return rc(res)
  end

  # return 0 on success
  def eqp_opecomp(eqp, cj, params={})
    cjID = cj.instance_of?(String) ? oid(cj) : cj
    eqpID = eqp.instance_of?(String) ? oid(eqp) : eqp
    spcrequired = !!params[:spcrequired]
    force = params[:force]
    memo = params[:memo] || ''
    ib = params.has_key?(:ib) ? params[:ib] : eqp_info_raw(eqpID).respond_to?(:equipmentBRInfoForInternalBuffer)
    $log.info "eqp_opecomp #{eqpID.identifier.inspect}, #{cjID.identifier.inspect}#{', ib: true' if ib}#{', force: true' if force}"
    if force
      if ib
        res = @manager.TxForceOpeCompForInternalBufferReq(@_user, eqpID, cjID, spcrequired, memo)
      else
        res = @manager.TxForceOpeCompReq(@_user, eqpID, cjID, spcrequired, memo)
      end
    else
      if ib
        res = @manager.TxOpeCompForInternalBufferReq(@_user, eqpID, cjID, spcrequired, memo)
      else
        res = @manager.TxOpeCompWithDataReq(@_user, eqpID, cjID, spcrequired, memo)
      end
    end
    return rc(res)
  end

  # Partial operation complete
  #
  #  per default all wafers go to operation complete action
  #  select wafers for other actions by arrays of slots
  #
  # returns array of lots on success
  def eqp_partial_opecomp(eqp, cj, params={})
    eqpID = eqp.instance_of?(String) ? oid(eqp) : eqp
    cjID = cj.instance_of?(String) ? oid(cj) : cj
    spcrequired = !!params[:spcrequired]
    opecomp_hold = params[:opecomp_hold] || []
    opestart_cancel = params[:opecancel] || []
    opestart_cancel_hold = params[:opecancel_hold] || []
    memo = params[:memo] || ''
    $log.info "eqp_partial_opecomp #{eqpID.identifier.inspect}, #{cjID.identifier.inspect}, #{params}"
    # collect wafer IDs by inteded action, actually no need to call eqp_cjpj_info for this
    wafers = [[], [], [], []]
    pjiraw = eqp_cjpj_info(eqpID, cjID) || ($log.warn 'PJ control not supported'; return)
    pjiraw.strProcessJobInfoSeq.each {|pji|
      pji.strProcessWaferSeq.each {|pwi|
        walias = pwi.aliasWaferName.split('.').last  # Fab7 support
        if opecomp_hold.include?(walias)
          wafers[1] << pwi.waferID
        elsif opestart_cancel.include?(walias)
          wafers[2] << pwi.waferID
        elsif opestart_cancel_hold.include?(walias)
          wafers[3] << pwi.waferID
        else  # all other wafers go to opecomp
          wafers[0] << pwi.waferID
        end
      }
    }
    # create actions array
    actions = []
    actions << @jcscode.pptPartialOpeCompAction_struct.new(
      wafers[0].to_java(@jcscode.objectIdentifier_struct), 'OperationCompletion', any) unless wafers[3].empty?
    actions << @jcscode.pptPartialOpeCompAction_struct.new(
      wafers[1].to_java(@jcscode.objectIdentifier_struct), 'OperationCompletionWithHold', any) unless wafers[0].empty?
    actions << @jcscode.pptPartialOpeCompAction_struct.new(
      wafers[2].to_java(@jcscode.objectIdentifier_struct), 'OperationStartCancel', any) unless wafers[1].empty?
    actions << @jcscode.pptPartialOpeCompAction_struct.new(
      wafers[3].to_java(@jcscode.objectIdentifier_struct), 'OperationStartCancelWithHold', any) unless wafers[2].empty?
    # call Tx
    inparm = @jcscode.pptPartialOpeCompWithDataReqInParm_struct.new(eqpID, cjID,
      actions.to_java(@jcscode.pptPartialOpeCompAction_struct), spcrequired, any)
    ib = params.has_key?(:ib) ? params[:ib] : eqp_info_raw(eqpID).respond_to?(:equipmentBRInfoForInternalBuffer)
    if ib
      res = @manager.TxPartialOpeCompForInternalBufferReq(@_user, inparm, memo)
    else
      res = @manager.TxPartialOpeCompWithDataReq(@_user, inparm, memo)
    end
    return if rc(res) != 0
    ret = res.strPartialOpeCompLotSeq.collect {|l| l.lotID.identifier}
    $log.info "  created lots #{ret}"
    return ret
  end

  # returns pptCJPJInfoInqResult_struct on success, used by eqp_partial_opecomp
  def eqp_cjpj_info(eqp, cj, params={})
    eqpID = eqp.instance_of?(String) ? oid(eqp) : eqp
    cjID = cj.instance_of?(String) ? oid(cj) : cj
    pj = params[:pj] || ''
    inparm = @jcscode.pptCJPJInfoInqInParm_struct.new(eqpID, cjID, pj, false, any)
    res = @manager.TxCJPJInfoInq(@_user, inparm)
    return nil if rc(res) != 0
    return res
  end

  # changetype is one of OnlineModeChange, AccessModeChange or OtherModeChange
  #
  # return string array of candidate modes or nil on error, used in Test520_EqpModeChange
  def eqp_mode_candidates(eqp, params={})
    ports = params[:ports] || eqp_info(eqp, ib: params[:ib]).ports.collect {|p| p.port}
    ret = {}
    ['OnlineModeChange', 'AccessModeChange', 'OtherModeChange'].each {|ct|
      res = @manager.TxCandidateEqpModeInq(@_user, oid(eqp), ct, oids(ports))
      return if rc(res) != 0
      res.strCandidatePortMode.each {|c|
        ret[c.portID.identifier] ||= []
        ret[c.portID.identifier] += c.strOperationMode.collect {|m| m.operationMode.identifier}
      }
    }
    return ret
  end

  # switch the eqp ports (default: all) in the indicated mode ("Off-Line-1", "Auto-1", etc.), return 0 on success
  def eqp_mode_change(eqp, mode, params={})
    eqpID = eqp.instance_of?(String) ? oid(eqp) : eqp
    ports = params[:port] || params[:ports]
    ports = [ports] if ports.instance_of?(String)
    notifyTCS = (params.has_key?(:notifyTCS) ? params[:notifyTCS] : params[:notify]) != false
    notifyEqp = params[:notifyEqp] || notifyTCS
    $log.info "eqp_mode_change #{eqpID.identifier.inspect}, #{mode.inspect}, notify: #{notifyTCS}"
    memo = params[:memo] || ''
    eqpiraw = params[:eqpinfo] || eqp_info_raw(eqpID, ib: params[:ib]) || return
    om = @jcscode.pptOperationMode_struct.new(oid(mode), '', '', '', '', '', '', any)
    pomode = []
    eqpiraw.equipmentPortInfo.strEqpPortStatus.each {|p|
      # need to include all ports for Offline <-> Online change
      next if ports && !ports.member?(p.portID.identifier) && (p.operationModeID.identifier[0, 4] == mode[0, 4])
      pomode << @jcscode.pptPortOperationMode_struct.new(p.portID, p.portGroup, p.portUsage, om, any)
    }
    #
    res = @manager.TxEqpModeChangeReq(@_user, eqpiraw.equipmentID, pomode, notifyEqp, notifyTCS, memo)
    return rc(res)
  end

  # E10 status recover, return 0 on success
  def eqp_status_recover(eqp, params={})
    memo = params[:memo] || ''
    $log.info "eqp_status_recover #{eqp.inspect}"
    #
    res = @manager.TxEqpStatusRecoverReq(@_user, oid(eqp), 'MANU', memo)
    return rc(res)
  end


  # change port status, e.g. to "LoadReq", return 0 on success
  def eqp_port_status_change(eqp, port, status, params={})
    # user = user_struct(params[:user] || @user, params[:user] ? :default : @password, client_node: params[:client_node])
    lot = params[:lot] || ''
    carrier = params[:carrier] || ''
    memo = params[:memo] || ''
    eqpID = eqp.instance_of?(String) ? oid(eqp) : eqp
    portID = port.instance_of?(String) ? oid(port) : port
    evt = [@jcscode.pptEqpPortEventOnTCS_struct.new(portID, status, oid(lot), oid(carrier), any)]
    $log.info "eqp_port_status_change #{eqpID.identifier.inspect}, #{portID.identifier.inspect}, #{status.inspect}" unless params[:silent]
    #
    # customized tx can set the port to state "Down" and "Unknown" for up
    if params[:customized]
      res = @manager.CS_TxEqpPortStatusChangeReq(@_user, eqpID, evt, memo)
    else
      res = @manager.TxEqpPortStatusChangeRpt(@_user, eqpID, evt, memo)
    end
    return rc(res)
  end

  # change reticle port status, e.g. to "LoadReq", the custom version can set the port to state "Down" and "-" for up
  #
  # return 0 on success
  def eqp_rsp_port_status_change(eqp, rport, status, params={})
    pod = params[:pod] || ''
    memo = params[:memo] || ''
    evt = [@jcscode.pptEqpRSPPortEventOnTCS_struct.new(oid(rport), status, oid(pod), any)]
    $log.info "eqp_rsp_port_status_change #{eqp.inspect}, #{(rport.inspect)}, #{status.inspect}"
    #
    # customized tx is for separating OPI privileges only (?)
    if params[:customized]
      res = @manager.CS_TxEqpRSPPortStatusChangeReq(@_user, oid(eqp), evt, memo)
    else
      res = @manager.TxEqpRSPPortStatusChangeRpt(@_user, oid(eqp), evt, memo)
    end
    return rc(res)
  end

  # change reticle port access mode, e.g. to "Auto" or "Manual", return 0 on success
  def eqp_rsp_port_access(eqp, rport, mode, params={})
    notify = (params[:notify] != false)
    memo = params[:memo] || ''
    $log.info "eqp_rsp_port_access_mode #{eqp.inspect}, #{(rport.inspect)}, #{mode.inspect}"
    #
    res = @manager.TxEqpRSPPortAccessModeChangeReq(@_user, oid(eqp), oid(rport), mode, notify, memo)
    return rc(res)
  end

  # sent via MM to the EI
  def eqp_signaltower_off(eqp, params={})
    memo = params[:memo] || ''
    res = @manager.TxSignalTowerOffReq(@_user, oid(eqp), memo)
    return rc(res)
  end


  # E10 status change, e.g. 2WPR , 2NDP, client_node can be "CMMS" or MetaframeID of user; return 0 on success
  def eqp_status_change(eqp, status, params={})
    eqpID = eqp.instance_of?(String) ? oid(eqp) : eqp
    memo = params[:memo] || ''
    # user = user_struct(params[:user] || @user, params[:user] ? :default : @password, client_node: params[:client_node])
    $log.info "eqp_status_change #{eqpID.identifier.inspect}, #{status.inspect}"
    if clientnode = params[:client_node]
      user = user_struct(@user, @password, client_node: clientnode)
      $log.info "  client_node: #{clientnode}"
    else
      user = @_user
    end
    #
    res = @manager.TxEqpStatusChangeReq(user, eqpID, oid(status), memo)
    return rc(res)
  end

  # E10 status change report sent by EI, e.g. to "2WPR", return 0 on success
  def eqp_status_change_rpt(eqp, status, params={})
    eqpID = eqp.instance_of?(String) ? oid(eqp) : eqp
    memo = params[:memo] || ''
    $log.info "eqp_status_change_rpt #{eqpID.identifier.inspect}, #{status.inspect}"
    if clientnode = params[:client_node]
      user = user_struct(@user, @password, client_node: clientnode)
      $log.info "  client_node: #{clientnode}"
    else
      user = @_user
    end
    #
    res = @manager.TxEqpStatusChangeRpt(user, eqpID, oid(status), memo)
    return rc(res)
  end

  # chamber status changes, cf DB table FHCSCHS
  # works in any tool state, but rejects invalid status transitions, see MSR 258760
  # status is e.g. 2WPR, 1PRD, 5DLY, 4QUL, 2NDP, ..
  #
  # return 0 on success
  def chamber_status_change(eqp, chambers, status, params={})
    eqpID = eqp.instance_of?(String) ? oid(eqp) : eqp
    chambers = [chambers] if chambers.instance_of?(String)
    $log.info "chamber_status_change #{eqpID.identifier.inspect}, #{chambers}, #{status.inspect}"
    if clientnode = params[:client_node]
      user = user_struct(@user, @password, client_node: clientnode)
      $log.info "  client_node: #{clientnode}"
    else
      user = @_user
    end
    chstat = chambers.collect {|c| @jcscode.pptEqpChamberStatus_struct.new(oid(c), oid(status), any)}
    #
    res = @manager.TxChamberStatusChangeReq(user, eqpID, chstat, params[:memo] || '')
    return rc(res)
  end

  # comes from the EI and works only if the tool is online-remote, see MSR 258760
  # status is e.g. 2WPR, 1PRD, 5DLY, 4QUL, 2NDP, .., does not check for invalid state transitions
  #
  # return 0 on success
  def chamber_status_change_rpt(eqp, chambers, status, params={})
    eqpID = eqp.instance_of?(String) ? oid(eqp) : eqp
    chambers = [chambers] if chambers.instance_of?(String)
    $log.info "chamber_status_change_rpt #{eqpID.identifier.inspect}, #{chambers}, #{status.inspect}"
    if clientnode = params[:client_node]
      user = user_struct(@user, @password, client_node: clientnode)
      $log.info "  client_node: #{clientnode}"
    else
      user = @_user
    end
    chstat = chambers.collect {|c| @jcscode.pptEqpChamberStatus_struct.new(oid(c), oid(status), any)}
    #
    res = @manager.TxChamberStatusChangeRpt(user, eqpID, chstat, params[:memo] || '')
    return rc(res)
  end

  # # Typically sent from the EI to report the wafers processed in a chamber as event. UNUSED
  # # Works only if the tool is online-remote, see MSR 258760
  # # action is one of "ProcessStart", "ProcessEnd" or "MSR258760"
  # # ?? status is e.g. 2WPR, 1PRD, 4QUL, 2NDP, .., does not check for invalid state transitions
  # #
  # # return 0 on success
  # def chamber_process_wafer_rpt(eqp, chamber, lot, action, params={})
  #   memo = params[:memo] || ''
  #   li = lot_info(lot, wafers: true)
  #   cj = params[:cj] || li.cj
  #   chi = [@jcscode.pptProcessedChamberInfo_struct.new(oid(chamber), siview_timestamp, any)
  #     ].to_java(@jcscode.pptProcessedChamberInfo_struct)
  #   i = 0
  #   chpli = li.wafers.collect {|w|
  #     # support for mixed action codes in MSR258760
  #     if action == 'MSR258760'
  #       action = i < li.wafers.size / 2 ? 'ProcessStart' : 'ProcessEnd'
  #       i += 1
  #     end
  #     wi = [@jcscode.pptChamberProcessWaferInfo_struct.new(oid(w.wafer), chi, any)].to_java(@jcscode.pptChamberProcessWaferInfo_struct)
  #     @jcscode.pptChamberProcessLotInfo_struct.new(oid(lot), action, wi, any)
  #   }
  #   #
  #   res = @manager.TxChamberProcessWaferRpt(@_user, oid(eqp), oid(cj), chpli, memo)
  #   return rc(res)
  # end

  # return arrays of chambers available for the lots associated with a control job or nil on error
  def AMD_chambers_availability(cj, params={})
    res = @manager.AMD_TxGetChambersAvailabilityInq(@_user, cj)
    return if rc(res) != 0
    return res.lotChambersAvailability
  end

  # request a PM (sent from SAP)
  def eqp_pm_event(eqp, params={})
    $log.info "eqp_pm_event #{eqp.inspect}, #{params}"
    chamber = params[:chamber] || ''
    state = params[:state] || '2NDP'
    reasoncode = params[:reasoncode] || 'RQPM'
    memo = params[:memo] || ''
    user = user_struct(params[:user] || @user, params[:user] ? :default : @password, client_node: params[:client_node])
    pmevt = [@jcscode.pptAMD_PMEvent_struct.new(oid(eqp), oid(chamber), oid(state), reasoncode, any)]
    #
    res = @manager.AMD_TxEqpPMEventReq(user, pmevt, memo)
    return rc(res)
  end

  # # sent by EI? return nil;  currently only used for manual test of MSR 1040425
  # def eqp_pm_event_rpt(eqp, params={})
  #   $log.info "eqp_pm_event_rpt #{eqp.inspect}, #{params}"
  #   chamber = params[:chamber] || ''
  #   state = params[:state] || '2NDP'
  #   xsitestate = params[:xsitestate] || ''
  #   user = user_struct(params[:user] || @user, params[:user] ? :default : @password, client_node: params[:client_node])
  #   pmevt = [@jcscode.pptCS_PMEvent_struct.new(oid(eqp), oid(chamber), oid(state), oid(xsitestate), any)]
  #   #
  #   @manager.GF_TxPMEventRpt(user, pmevt, params[:inhibit] || '')
  # end

  # request a chamber PM (sent from SAP)
  def chamber_pm_event(eqp, chamber, params={})
    $log.info "chamber_pm_event #{eqp.inspect}, #{chamber.inspect}, #{params}"
    state = params[:state] || '2NDP'
    reasoncode = params[:reasoncode] || 'RQPM'
    memo = params[:memo] || ''
    user = user_struct(params[:user] || @user, params[:user] ? :default : @password, client_node: params[:client_node])
    pmevt = [@jcscode.pptAMD_PMEvent_struct.new(oid(eqp), oid(chamber), oid(state), reasoncode, any)]
    #
    res = @manager.AMD_TxChamberPMEventReq(user, pmevt, memo)
    return rc(res)
  end

  # cancel PM (sent from SAP)
  def eqp_pm_event_cancel(eqp, params={})
    $log.info "eqp_pm_event_cancel #{eqp.inspect}, #{params}"
    chamber = params[:chamber] || ''
    state = params[:state] || '2NDP'
    reasoncode = params[:reasoncode] || 'RQPM'
    memo = params[:memo] || ''
    user = user_struct(params[:user] || @user, params[:user] ? :default : @password, client_node: params[:client_node])
    pmevt = [@jcscode.pptAMD_PMEvent_struct.new(oid(eqp), oid(chamber), oid(state), reasoncode, any)]
    #
    res = @manager.AMD_TxPMEventCancelReq(user, pmevt, memo)
    return rc(res)
  end

  # report E10 status of all eqps to SAPPM, called by SAPPM; return nil
  #   the result is sent to SAP directly, check SAPPM interface for AMD_SAPPM_TxAllEquipmentStatesRpt
  def AMD_TxAllEquipmentStatesInq(params={})
    $log.info "AMD_TxAllEquipmentStatesInq #{params}"
    user = user_struct(params[:user] || @user, params[:user] ? :default : @password, client_node: params[:client_node])
    @manager.AMD_TxAllEquipmentStatesInq(user)
  end

  # send alarm to SiView, return 0 on success
  def eqp_alarm(eqp, params={})
    acode = params[:code] || 'QAT'
    aid = params[:id] || 'QAT'
    acategory = params[:category] || 'QA Test'
    atext = params[:message] || "QA Test #{Time.now.strftime('%F_%T')}"
    timestamp = siview_timestamp(params[:timestamp])
    stocker = ''
    agvid = ''
    memo = params[:memo] || ''
    alarm = @jcscode.pptEquipmentAlarm_struct.new(acode, aid, acategory, atext, timestamp, any)
    #
    res = @manager.TxEqpAlarmRpt(@_user, oid(eqp), oid(stocker), oid(agvid), alarm, memo)
    return rc(res)
  end

  # inquire alarms, return strEquipmentAlarm on success or nil
  def eqp_alarm_history(eqp, params={})
    type = params[:type] || 'EqpAlarm'
    tstart = siview_timestamp(params[:tstart] || (Time.now - 3600))
    tend = siview_timestamp(params[:tend] || Time.now)
    #
    res = @manager.TxEqpAlarmHistoryInq(@_user, oid(eqp), type, tstart, tend)
    return if rc(res) != 0
    return res.strEquipmentAlarm
  end

  # return strEqpNote on success or nil, used in APC_Baseline
  def eqp_notes(eqp, params={})
    res = @manager.TxEqpNoteInq(@_user, oid(eqp))
    return if rc(res) != 0
    return res.strEqpNote
  end

  # register an equipment note, return 0 on success
  def eqp_note_register(eqp, title, note)
    res = @manager.TxEqpNoteRegistReq(@_user, oid(eqp), title, note)
    return rc(res)
  end

  # return list of recipes for the given eqp or nil on error
  def eqp_related_recipes(eqp, params={})
    res = @manager.TxEqpRelatedRecipeIDListInq(@_user, oid(eqp))
    return if rc(res) != 0
    return res if params[:raw]
    res.strMandPRecipeInfo.collect {|e| e.machineRecipeID.identifier}
  end

end
