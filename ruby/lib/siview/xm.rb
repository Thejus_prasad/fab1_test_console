=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: Steffen Steidten, 2009-05-03

History:
  2013-12-18 ssteidte, refactored EasySiViewR, SMFacade, XM, AsmViewR and Fab7 SiView
  2014-11-03 ssteidte, moved constants to SiView::MM
  2015-08-31 ssteidte, preferrably use properties from etc/siview.conf
  2017-07-20 sfrieske, dropped support for old etc/connections/<env>.properties
  2021-02-05 sfrieske, use instance_of instead of kind_of
  2021-08-31 sfrieske, requiring 'time' for #iso8601
  2021-12-08 sfrieske, replaced Profile Struct by a Hash
=end

require 'time'


module SiView

  class XM < Client

    def connect
      super
      @jcscode = com.amd.jcs.xm.code
      # create XM connection
      profile = @iorparser.profiles.first
      $log.info "XM connection: #{profile[:host]}:#{profile[:port]}"
      begin
        obj = @orb.string_to_object(@iorparser.ior)
        $log.info "creating XMServiceManager"
        @manager = @jcscode.XMServiceManagerHelper.narrow(obj)
        @_user = user_struct(@user, SiView.siview_pw(@password, -1), @_props)
      rescue => e
        $log.warn "  error creating XM connection\n#{$!}"
        $log.debug e.backtrace.join("\n")
      end
      $log.info self.inspect
    end

    # suitable as "ping", return 0 on success
    def date_and_time
      res = @manager.TxDateAndTimeReq(@_user)
      return rc(res)
    end

    # inquire for transport jobs, optionally specify carrier, source or target machine
    #   and optional source and target machine specification, currently UNUSED (XM_Regression_TransportJobs not active)
    #
    # return job list or  nil on error
    def transport_jobs(params={})
      carrier = params[:carrier] || '*' # all carriers
      inqtype = params[:type] || 'C'
      tomachine = params[:tomachine] || ''
      frommachine = params[:frommachine] || ''
      # function: "" .. ask XM, "INQUIRY" .. ask AMHS, "UPDATE" .. ask AMHS and update XM
      function = params[:function] || 'INQUIRY'
      tjinq = @jcscode.amhsTransportJobInq_struct.new(inqtype, oid(carrier), oid(tomachine), oid(frommachine), any)
      #
      res = @manager.TxXmTransportJobInq(@_user, tjinq, function)
      return nil if rc(res) != 0
      return res
    end

    # create a new transport job, return 0 on success
    def transport_job_create(carriers, frommachine, fromport, tomachine, toport, params={})
      carriers = carriers.split if carriers.instance_of?(String)
      job = params[:job] || ''  # blank for new jobs
      reroute = !!params[:reroute]
      transportType = params[:type] || 'S' # S=Single, B=Batch, L=Load/Unload
      zone = params[:zone] || ''
      n2purgeflag = !!params[:n2purgeflag]
      stockerGroup = params[:stockergroup] || ''
      tstart = params[:tstart] || ''
      tend = params[:tend] || ''
      mandatory = !!params[:mandatory]
      priority = params[:priority] || '3' # 1..5
      #
      $log.info "XmTransportJobCreate for #{carriers.inspect} from #{frommachine}:#{fromport} to #{tomachine}:#{toport}"
      ($log.warn "  missing tomachine"; return) unless tomachine
      #
      dest = [@jcscode.amhsToDestination_struct.new(oid(tomachine), oid(toport), any)].to_java(@jcscode.amhsToDestination_struct)
      jobData = carriers.collect {|c| @jcscode.amhsJobCreateArray_struct.new(job, oid(c), zone, n2purgeflag,
        oid(frommachine), oid(fromport), stockerGroup, dest, tstart, tend, mandatory, priority, any)
      }.to_java(@jcscode.amhsJobCreateArray_struct)
      tj = @jcscode.amhsTranJobCreateReq_struct.new(job, reroute, transportType, jobData, any)
      #
      res = @manager.TxXmTransportJobCreateReq(@_user, tj)
      return rc(res)
    end

    # removes the transportjob for the carriers, empty string for all carriers, return 0 on success
    def transport_job_remove(job, carriers)
      carriers = carriers.split if carriers.instance_of?(String)
      cjs = carriers.collect {|c| @jcscode.amhsCarrierJob_struct.new(job, oid(c), any)}.to_java(@jcscode.amhsCarrierJob_struct)
      tjrr = @jcscode.amhsTranJobRemoveReq_struct.new(job, cjs, any)
      #
      res = @manager.TxXmTransportJobRemoveReq(@_user, tjrr)
      return rc(res)
    end

    # return 0 on success
    def carrier_rerouted(job, carrier, machine, tomachine, toport, new_tomachine, new_toport)
      ts = Time.now.iso8601 # formatting??
      rpt = @jcscode.amhsCarrierJobReroutedReport_struct.new(ts, job, carrier, machine,
        tomachine, toport, new_tomachine, new_toport, any)
      #
      res = @manager.AMD_TxCarrierJobReroutedReport(@_user, rpt)
      return rc(res)
    end

    #
    # AMHS EI via XM to SiView
    #

    # sent by stocker EIs at manual stockin, with location optimization via RTD WhereNextInterbay
    #
    # normally $sv.manager.TxLotCassetteXferStatusChangeRpt is sent right before this Tx
    #
    # return 0 on success
    def carrierid_read(carrier, stocker, params={})
      port = params[:port] || ''
      portType = params[:portType] || ''
      inOutType = params[:inOutType] || ''
      jobRequestFlag = !(params[:jobrequest] == false)
      cidrr = @jcscode.pptCarrierIDReadReport_struct.new(oid(carrier), oid(stocker),
        oid(port), oid(portType), inOutType, jobRequestFlag, any)
      #
      res = @manager.TxCarrierIDReadReport(@_user, cidrr)
      return rc(res)
    end

    # sent by AMHS EI to SiView to update the carrier location, UNUSED
    #
    # return 0 on success
    def carrier_location_rpt(job, cjob, carrier, cmachine, cport, status='')
      czone = ''
      cshelftype = ''
      cloc = @jcscode.pptCarrierLocationReport_struct.new(job, cjob,
        oid(carrier), status, oid(cmachine), oid(cport), czone, cshelftype, any)
      #
      res = @manager.TxCarrierLocationReport(@_user, cloc)
      return rc(res)
    end

    # jobstatus 1..completed
    #
    # return 0 on success
    def transportjob_status_rpt(job, jobstatus, originator, removeflag,
      carrier, cjob, cjobstatus, tomachine, toport, cremoveflag)
      cjstat = [@jcscode.pptJobStatusReport_struct.new(cjob, oid(carrier), cjobstatus, oid(tomachine), oid(toport),
        originator, cremoveflag)].to_java(@jcscode.pptJobStatusReport_struct)
      jstat = @jcscode.pptTransportJobStatusReport_struct.new(job, jobstatus, originator, removeflag, cjstat, any)
      #
      res = @manager.TxTransportJobStatusReport(@_user, jstat)
      return rc(res)
    end

    # register the FO and MT managers for the entity, e.g. AMHS01
    #
    # return 0 on success
    def register_EI(entity, foIOR, mtIOR, params={})
      zone = params[:zone]
      res = zone ? @manager.CS_TxRegisterEquipmentInterfaceForMultipleMCSReq(entity, foIOR, mtIOR, zone) :
                   @manager.AMD_TxRegisterEquipmentInterfaceReq(entity, foIOR, mtIOR)
      return rc(res)
    end

    # unregister the FO and MT managers for the entity, e.g. AMHS01
    #
    # return 0 on success
    def unregister_EI(entity, params={})
      zone = params[:zone]
      res = zone ? @manager.CS_TxUnregisterEquipmentInterfaceForMultipleMCSReq(entity, zone) :
                   @manager.AMD_TxUnregisterEquipmentInterfaceReq(entity)
      return rc(res)
    end

    def AMHS_EIs(params={})
      entity = params[:entity] || ''  # not used by XM C5.18
      zone = params[:zone]
      res = zone ? @manager.CS_TxAmhsEquipmentInterfaceForMultipleMCSInq(entity, zone) :
                   @manager.AMD_TxAmhsEquipmentInterfaceInq(entity)
      return nil if rc(res) != 0
      return res if params[:raw]
      return res.strAmhsList.split(',')
    end

  end

end
