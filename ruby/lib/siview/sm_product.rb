=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.
    
Author: Steffen Steidten, 2009-05-03; Daniel Steger, 2014-10-30

=end


class SiView::SM
    
  # return true on success or nil, used in PDWHR110 only
  def add_srcproduct(product, srcproduct)
    $log.info "add_srcproduct #{product.inspect}, #{srcproduct.inspect}"
    srcpid = object_ids(:product, srcproduct).first || ($log.warn "  srcproduct does not exist"; return)
    edit_objects(:product, product) {|oinfos|
      oinfos.each {|e|
        ci = extract_si(e.classInfo)
        ci.sourceProducts = (ci.sourceProducts.to_a << srcpid).to_java(@@sm.jcscode.objectIdentifier_struct)
        e.classInfo = insert_si(ci)
      }
    }
  end

  # return true on success or nil, used in PDWHR110 only
  def remove_srcproduct(product, srcproduct)
    $log.info "remove_srcproduct #{product.inspect}, #{srcproduct.inspect}"
    edit_objects(:product, product) {|oinfos|
      oinfos.each {|e|
        ci = extract_si(e.classInfo)
        ci.sourceProducts = (ci.sourceProducts.to_a.delete_if {|p| p.identifier == srcproduct}).to_java(@@sm.jcscode.objectIdentifier_struct)
        e.classInfo = insert_si(ci)
      }
    }
  end

  # change the product's route, can be empty if state is Draft or Obsolete, return true on success or nil
  #   used in MM_Lot_BankinCancel only
  def change_product_route(product, route, params={})
    state = params[:state] || 'Complete'  # Draft, Obsolete
    $log.info "change_product_route #{product.inspect}, #{route.inspect}, state: #{state.inspect}"
    route_id = route.empty? ? oid('') : object_ids(:mainpd, route).first || ($log.error "  route does not exist"; return)
    edit_objects(:product, product) {|oinfos|
      oinfos.each {|e|
        ci = extract_si(e.classInfo)
        ci.state = state
        ci.mainProcessDefinition = route_id
        mainpd, version = route.split('.')
        ci.mainProcessDefinitionId = mainpd || ''
        ci.mainProcessDefinitionVersion = version || ''
        if route == ''
          ci.mainProcessDefinitions = [].to_java(@jcscode.brMainProcessDefinitionData_struct)
        else
          mpddata = @jcscode.brMainProcessDefinitionData_struct.new
          mpddata.orderType = '*'
          mpddata.mfgLayer = ci.mfgLayer
          mpddata.mainProcessDefinition = route_id
          mpddata.mainProcessDefinitionId = mainpd
          mpddata.mainProcessDefinitionVersion = version || ''
          mpddata.bom = ci.bom
          ci.mainProcessDefinitions = [mpddata].to_java(@jcscode.brMainProcessDefinitionData_struct)
        end
        e.classInfo = insert_si(ci)
      }
    }
  end

end
