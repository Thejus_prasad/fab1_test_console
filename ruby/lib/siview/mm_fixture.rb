=begin
(c) Copyright 2013 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.
=end


class SiView::MM

  # return an array or strFoundFixture structs or nil
  def fixture_list(params={})
    category = params[:category] || ''
    eqp = params[:eqp] || ''
    lot = params[:lot] || '' # can be "%"
    fixture = params[:fixture] || ''
    group = params[:fg] || ''
    partno = params[:fixturePartNumber] || ''
    status = params[:status] || ''
    count = params[:count] || 9999
    #
    res = @manager.TxFixtureListInq(@_user, oid(category), oid(group), partno, oid(fixture), oid(lot), oid(eqp), status, count)
    ($log.warn "error in TxFixtureListInq for #{params.inspect}"; return) if rc(res) != 0
    return res.strFoundFixture
  end

end
