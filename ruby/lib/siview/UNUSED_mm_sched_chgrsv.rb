=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.
=end

class SiView::MM
  SchdlChgRsv = Struct.new(:event, :object, :object_type, :tgt_route, :tgt_opNo, :product, :route, :opNo, :sublottype,
    :startdate, :starttime, :enddate, :endtime, :erase, :max_cnt, :apply_cnt, :status, :action_code, :lotinfo_change)


  # requests a schedule change reservation
  #
  # target route/opNo is the point in current route where schedule change is executed
  #
  # product/route/opNo are the new entries
  #
  # schedule change is executed after operation completion at the target route+opNo
  def schdl_change_rsv_create(objtype, obj, params={})
    $log.info "schdl_change_rsv_create #{objtype.inspect}, #{obj.inspect}, #{params}"
    # get new parameters, using empty values as defaults
    event = params[:event] || 'EVT001'
    tgt_route = params[:tgt_route] || ''
    tgt_opNo = params[:tgt_opNo] || ''
    product = params[:product] || ''
    route = params[:route] || ''
    opNo = params[:opNo] || ''
    startdate = params[:startdate] || Time.now.strftime('%F')
    starttime = params[:starttime] || Time.now.strftime('%H.%M.%S')
    enddate = params[:enddate] || (Time.now + 24*3600).strftime('%F')
    endtime = params[:endtime] || (Time.now + 24*3600).strftime('%H.%M.%S')
    status = params[:status] || '' #"OnGoing" # "Waiting"
    erase = !!params[:erase]
    count = params[:count] || 1
    sublottype = params[:sublottype] || ''
    lotinfo_change = !!params[:lotinfo_change]
    memo = params[:memo] || ''
    rsv = @jcscode.pptSchdlChangeReservation__110_struct.new(event, oid(obj),
      objtype, oid(tgt_route), tgt_opNo, oid(product), oid(route), opNo, sublottype, startdate, starttime,
      enddate, endtime, erase, count, 0, status, 'ScheduleChange', lotinfo_change, any)
    #
    res = @manager.TxSchdlChangeReservationCreateReq__110(@_user, rsv, memo)
    return rc(res)
  end

  # Lists the requested schedule changes
  #
  # allowed object types  are Lot, MainPD, ModulePD or Product
  def schdl_change_rsv_list(objtype, obj, params={})
    # get new parameters, using empty values as defaults
    event = params[:event] || 'EVT001'
    tgt_route = params[:tgt_route] || ''
    tgt_opNo = params[:tgt_opNo] || ''
    product = params[:product] || ''
    route = params[:route] || ''
    opNo = params[:opNo] || ''
    startdate = params[:startdate] || ''
    enddate = params[:enddate] || ''
    status = params[:status] || '' #"OnGoing" # "Waiting"
    sublottype = params[:sublottype] || ''
    lotinfo_change = params[:lotinfo_change] || -1
    #
    res = @manager.TxSchdlChangeReservationListInq__110(@_user,
      event, obj, objtype, tgt_route, tgt_opNo, product, route, opNo, sublottype, startdate, enddate, status, lotinfo_change)
    return nil if rc(res) != 0
    return res if params[:raw]
    return res.strSchdlChangeReservations.collect {|schdl|
      SchdlChgRsv.new(
        schdl.eventID, schdl.objectID.identifier, schdl.objectType, schdl.targetRouteID.identifier, schdl.targetOperationNumber,
        schdl.productID.identifier, schdl.routeID.identifier, schdl.operationNumber, schdl.subLotType,
        schdl.startDate, schdl.startTime, schdl.endDate, schdl.endTime,
        schdl.eraseAfterUsedFlag, schdl.maxLotCnt, schdl.applyLotCnt, schdl.status, schdl.actionCode, schdl.lotInfoChangeFlag)
    }
  end

  # request schedule change cancel
  def schdl_change_rsv_cancel(objtype, obj, params={})
    $log.info "schdl_change_rsv_cancel #{objtype.inspect}, #{obj.inspect}, #{params.inspect}"
    memo = params[:memo] || ''
    #
    schdl_change_rsv_list(objtype, obj, params) || return
    res = @manager.TxSchdlChangeReservationCancelReq__110(@_user, @_res.strSchdlChangeReservations, memo)
    return rc(res)
  end

  # request schedule change execution (normally called by PostProc)
  def schdl_change_rsv_exe(lot, params={})
    $log.info "schdl_change_rsv_exe #{lot.inspect}, #{params.inspect}"
    rsvs = schdl_change_rsv_list('Lot', lot, params)
    memo = params[:memo] || rsvs.first.event
    #
    li = lot_info(lot)
    attrs = rsvs.collect {|s|
      @jcscode.pptRescheduledLotAttributes__110_struct.new(oid(lot), oid(s.product), li.route, oid(s.tgt_route),
        li.opNo, s.tgt_opNo, s.sublottype, '', '', '', '', [].to_java(@jcscode.pptChangedLotSchedule_struct), any)
    }
    #
    res = @manager.TxSchdlChangeReservationExecuteReq__110(@_user, attrs, memo)
    return rc(res)
  end

  # request schedule change change (update)
  #
  # new parameters are required
  def schdl_change_rsv_change(objtype, obj, new_params, params={})
    $log.info "schdl_change_rsv_change #{objtype.inspect}, #{obj.inspect}, #{params.inspect}"
    memo = params[:memo] || ''
    s = schdl_change_rsv_list(objtype, obj, params).first ||($log.warn "no matching schdl change rsv"; return)
    ($log.warn "new parameters must be passed as hash"; return) unless new_params.instance_of?(Hash)
    #
    # get new parameters, using empty values as defaults
    event = new_params[:event] || s.event
    tgt_route = new_params[:tgt_route] || s.tgt_route
    tgt_opNo = new_params[:tgt_opNo] || s.tgt_opNo
    product = new_params[:product] || s.product
    route = new_params[:route] || s.route
    opNo = new_params[:opNo] || s.opNo
    startdate = new_params[:startdate] || s.startdate
    starttime = new_params[:starttime] || s.starttime
    enddate = new_params[:enddate] || s.enddate
    endtime = new_params[:endtime] || s.endtime
    status = new_params[:status] || s.status #"OnGoing" # "Waiting"
    erase = new_params[:erase] || s.erase
    count = new_params[:count] || s.max_cnt
    sublottype = params[:sublottype] || s.sublottype
    lotinfo_change = params[:lotinfo_change] || s.lotinfo_change
    #
    res = @manager.TxSchdlChangeReservationChangeReq__110(@_user, @_res.strSchdlChangeReservations.first,
      @jcscode.pptSchdlChangeReservation__110_struct.new(event, oid(obj),
        objtype, oid(tgt_route), tgt_opNo, oid(product), oid(route), opNo, sublottype,
        startdate, starttime, enddate, endtime,
        erase, count, s.apply_cnt, status, s.action_code, lotinfo_change, any),
      memo)
    return rc(res)
  end

  # STB lot after process, currently not used
  #
  # monitor lot will be build based on sourcelot, empty carrier and monitor product for production lot
  def stb_after_process(eqp, lots, params={})
    sublottype = params[:sublottype] || 'Process Monitor'
    nwafers = params[:nwafers] || 25
    memo = params[:memo] || ''
    lots = [lots] unless lots.instance_of?(Array)
    product = lots_info_raw(lots).collect {|li| li.strLotRecipeInfo.processMonitorProductID.identifier}.sort.last
    ##product = lots.collect {|l| lot_info(l).monitor_product}.uniq.first
    ($log.error "no monitor product definied"; return) if product.empty?
    $log.info "  stb_after_process '#{product}', '#{sublottype}', #{params.inspect}"
    srclots = params[:srclot] || srclot_inq(params.merge(product: product)) || return
    lattrs = _newwafer_attributes(lot, srclots, params) || return
    #
    res = @manager.TxMonitorLotSTBAfterProcessReq(@_user, oid(eqp), sublottype, lattrs, oid(lots), memo)
    return nil if rc(res) != 0
    $log.info "  created lot #{res.monitorLotID.identifier}"
    return res if params[:raw]
    return res.monitorLotID.identifier
  end

end
