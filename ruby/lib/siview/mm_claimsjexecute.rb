=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.
=end


class SiView::MM

  # execute sorter jobs, optionally selected by sjid; return true on success
  def claim_sj_execute(params={})
    $log.info "claim_sj_execute #{params}"
    sjs = sj_list(eqp: params[:eqp], sjid: params[:sjid])
    ($log.warn 'no sj'; return) if sjs.empty?
    report = params[:report]  # report the wafer transfer to SiView
    ret = true
    sjs.each {|sj|
      ($log.info "  skipping completed job #{sj.sorterJobID.identifier}"; next) if sj.sorterJobStatus == 'Completed'
      ($log.info "  skipping errored job #{sj.sorterJobID.identifier}"; next) if sj.sorterJobStatus == 'Error'
      eqpID = sj.equipmentID
      # switch eqp port states so job status can be changed to "Executing"
      sjports = sj.strSorterComponentJobListAttributesSeq.collect {|co|
        [co.originalPortID.identifier, co.destinationPortID.identifier]
      }.flatten.uniq
      eqpiraw = eqp_info_raw(eqpID)
      eqpiraw.equipmentPortInfo.strEqpPortStatus.each {|p|
        next unless p.operationModeID.identifier.start_with?('Auto')
        next unless sjports.member?(p.portID.identifier)
        (eqp_port_status_change(eqpID, p.portID, 'UnloadReq'); sleep 1) unless ['UnloadReq', 'LoadReq'].member?(p.portState)
        (eqp_port_status_change(eqpID, p.portID, 'LoadReq'); sleep 1) unless p.portState == 'LoadReq'
      }
      component = sj_status(sj.sorterJobID).strSorterComponentJobListSequence.first
      if component && !['Xfer', 'Executing'].include?(component.sorterComponentJobStatus)
        sj_status_change(eqpID, sj.sorterJobID, 'Executing')
      end
      job_ok = true
      sj.strSorterComponentJobListAttributesSeq.each_with_index {|co, idx|
        # change status to Xfer
        component = sj_status(sj.sorterJobID).strSorterComponentJobListSequence.find {|e|
          e.componentJobID.identifier == co.sorterComponentJobID.identifier
        } || next
        if component.sorterComponentJobStatus != 'Xfer'
          job_ok &= (sj_status_change(eqpID, co.sorterComponentJobID, 'Xfer', jobtype: 'ComponentJob') == 0)
        end
        if report  # TODO: does not work for multiple component jobs
          $log.info "executing component job ##{idx}"
          srccarrier = co.originalCarrierID.identifier
          tgtcarrier = co.destinationCarrierID.identifier
          srcport = co.originalPortID.identifier
          tgtport = co.destinationPortID.identifier
          # npw rsv (required for Auto-3, -2, ..) and load carriers
          [[srccarrier, srcport], [tgtcarrier, tgtport]].each {|data|
            carrier, port = data
            cstat = carrier_status(carrier, raw: true).cassetteStatusInfo
            if cstat.transferStatus == 'EI'
              $log.info "  carrier #{carrier} is already loaded"  # by previous component job
            else
              if cstat.transferStatus != 'EO' && cstat.transferReserveUserID.identifier.empty?
                unless eqpiraw.equipmentPortInfo.strEqpPortStatus.find {|p|
                  p.portID.identifier == port}.dispatchLoadCassetteID.identifier == carrier
                # unless eqpiraw(eqpID).ports.find {|p| p.port == port}.disp_load_carrier == carrier
                  r = nil
                  3.times {|i|
                    r = npw_reserve(eqpID, port, carrier)
                    break if r != 1752
                    # break if [0, 1752].include?(r)
                    sleep 10 if i > 0
                  }
                  return unless [0, 116].member?(r)
                end
                carrier_xfer_status_change(carrier, 'EO', eqpID)
              end
              eqp_port_status_change(eqpID, port, 'LoadComp')
              (eqp_load(eqpID, port, carrier, ib: false, purpose: 'Other') == 0) || return
            end
          }
          # sorter request and report
          (sorter_on_eqp_req(eqpID, sj.sorterJobID.identifier, co.strWaferSorterSlotMapSequence) == 0) || return
          sleep 1  # processing time
          (sorter_on_eqp_rpt(eqpID, co.strWaferSorterSlotMapSequence) == 0) || return
          # unload carriers unless required in the next component job
          nextcocarriers = []
          if sj.strSorterComponentJobListAttributesSeq.size > idx + 1
            nextco = sj.strSorterComponentJobListAttributesSeq[idx+1]
            nextcocarriers = [nextco.destinationCarrierID.identifier, nextco.originalCarrierID.identifier]
          end
          [[srccarrier, srcport], [tgtcarrier, tgtport]].each {|data|
            carrier, port = data
            if nextcocarriers.include?(carrier)
              $log.info "carrier #{carrier} is needed in next component job"
            else
              eqp_port_status_change(eqpID, port, 'UnloadReq')
              eqp_unload(eqpID, port, carrier, ib: false)
              eqp_port_status_change(eqpID, port, 'LoadReq')
            end
          }
        else  # no reports, change component job status on behalf of the SJ watchdog
          # change status to Executing
          job_ok &= (sj_status_change(eqpID, co.sorterComponentJobID, 'Executing', jobtype: 'ComponentJob') == 0)
          # report wafer moves
          slotmap = co.strWaferSorterSlotMapSequence.collect {|w|
            @jcscode.pptWaferTransfer_struct.new(w.waferID, w.destinationCassetteID,
              w.bDestinationCassetteManagedBySiView, w.destinationSlotNumber,
              w.originalCassetteID, w.bOriginalCassetteManagedBySiView, w.originalSlotNumber, any
            )
          }
          wafer_sort_rpt('', '', slotmap: slotmap)
          # change status to Completed
          job_ok &= (sj_status_change(eqpID, co.sorterComponentJobID, 'Completed', jobtype: 'ComponentJob') == 0)
        end
      }
      if !report
        # change job status to completed and cancel the job
        job_ok = (sj_status_change(eqpID, sj.sorterJobID, 'Completed', jobtype: 'SorterJob') == 0)
        # just to make sure; normally the SiView watchdog does it
        # ?? sleep 1
        sj_cancel(sj.sorterJobID) unless sj_list(sjid: sj.sorterJobID).empty?
      end
      ret &= job_ok
    }
    return ret
  end

end
