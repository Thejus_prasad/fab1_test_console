=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.
=end


class SiView::MM
  A10true = Array.new(10, true)  # for eqp_info
  CandChamber = Struct.new(:chamber, :currentStatus, :currentStatusName, :candCurrentE10, :candOtherE10)
  CandChamberDetail = Struct.new(:E10status, :chamberStatus, :chamberStatusName, :available)
  XsiteWorkOrder = Struct.new(:eqp, :chamber, :requestedStateCode, :cmmsStateCode, :reasoncode, :wotime)

  # # currently not used
  # def eqp_detail_info(eqp, params={})
  #   res = @manager.TxEqpDetailInfoInq(@_user, oid(eqp))
  #   return if rc(res) != 0
  #   return res
  # end

  # # return array of eqpIDs applicable for automatic transfer, not used
  # def all_available_eqp(params={})
  #   res = @manager.TxAllAvailableEqpInq(@_user)
  #   return if rc(res) != 0
  #   return res if params[:raw]
  #   res.equipmentIDs.collect {|e| e.identifier}.sort
  # end

  # # sent by EI? return nil;  currently only used for manual test of MSR 1040425
  # def eqp_pm_event_rpt(eqp, params={})
  #   $log.info "eqp_pm_event_rpt #{eqp.inspect}, #{params}"
  #   chamber = params[:chamber] || ''
  #   state = params[:state] || '2NDP'
  #   xsitestate = params[:xsitestate] || ''
  #   user = user_struct(params[:user] || @user, params[:user] ? :default : @password, client_node: params[:client_node])
  #   pmevt = [@jcscode.pptCS_PMEvent_struct.new(oid(eqp), oid(chamber), oid(state), oid(xsitestate), any)]
  #   #
  #   @manager.GF_TxPMEventRpt(user, pmevt, params[:inhibit] || '')
  # end

  # # return array of CandChamberDetail or nil on error  not used
  # def eqp_cand_chamber_status(eqp, params={})
  #   res = @manager.TxCandidateChamberStatusInq(@_user, oid(eqp), '')
  #   return if rc(res) != 0
  #   return res if params[:raw]
  #   ret = res.strCandidateChamberStatusInfo.collect {|cinfo|
  #     current = cinfo.strCandidateCurrentE10.strCandidateChamberStatus.collect {|x|
  #       CandChamberDetail.new(cinfo.strCandidateCurrentE10.E10Status.identifier,
  #         x.chamberStatusCode.identifier, x.chamberStatusName, x.availableFlag)
  #     }
  #     other = cinfo.strCandidateOtherE10.collect {|x|
  #       x.strCandidateChamberStatus.collect {|y|
  #         CandChamberDetail.new(x.E10Status.identifier, y.chamberStatusCode.identifier, y.chamberStatusName, y.availableFlag)
  #       }
  #     }
  #     CandChamber.new(cinfo.chamberID.identifier, cinfo.currentStatusCode.identifier, cinfo.currentStatusName, current, other)
  #   }
  #   return ret
  # end

  # Create an Xsite work order request
  def CS_work_order_list(eqp, params={})
    res = @manager.CS_TxWorkOrderListInq(@_user, oid(eqp), oid(params[:chamber] || ''))
    return if rc(res) != 0
    return res if params[:raw]
    return res.strRegisteredWorkOrderRequestSequence.collect {|wo|
      XsiteWorkOrder.new(wo.equipmentID.identifier, wo.chamberID.identifier, wo.requestedStateCode.identifier,
        wo.CMMSStateCode.identifier, wo.reasonCode, wo.workOrderRegistrationTime)
    }
  end

  # create an Xsite work order request
  def CS_work_order_create(eqp, params={})
    cha = params[:chamber] || ''
    e10 = params[:e10state] || 'UDT'  #ENG or SDT
    title = params[:title] || 'Work Order Create Test'
    memo = params[:memo] || 'Test'
    #
    res = @manager.CS_TxWorkOrderCreateReq(@_user, oid(eqp), oid(cha), oid(e10), title, memo)
    return if rc(res) != 0
    return res if params[:raw]
    return res.workOrderID
  end

  # # SLM function -- unused
  # def slm_wafer_retrieve(eqp, carrier, lot, params={})
  #   cj = params[:cj] || ''
  #   pj = params[:pj] || ''
  #   slotmap = lot_info(lot, wafers: true).wafers.collect {|w|
  #     @jcscode.pptSLMSlotMap_struct.new(oid(carrier), oid(w.wafer), w.alias.to_i, any)
  #   }.to_java(@jcscode.pptSLMSlotMap_struct)
  #   inparm = @jcscode.pptSLMWaferRetrieveRptInParm_struct.new(oid(eqp), oid(cj), pj, slotmap, any)
  #   #
  #   res = @manager.TxSLMWaferRetrieveRpt(@_user, inparm)
  #   return rc(res)
  # end

end
