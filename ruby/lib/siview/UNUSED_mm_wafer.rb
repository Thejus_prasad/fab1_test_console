=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.
=end


class SiView::MM

  SorterData = Struct.new(:eqp, :action, :user, :request_time, :direction, :sorter_status, :slotmap_compare, :mm_compare, :slotmap)


  # either :pg=>pg or :port=>port must be passed as parameter
  #
  # return data of the last sorter action on the specified port group, currently UNUSED
  def sorter_data(eqp, params)
    pg = params[:pg] || eqp_info(eqp).ports.find {|p| p.port == params[:port]}.pg
    required_data = 'SP_Sorter_SlotMap_AllData'
    sort_pattern = ''
    # $log.debug "sorter_data #{eqp} #{pg.inspect}"
    #
    res = @manager.TxWaferSorterDataInq(@_user, pg, oid(eqp), required_data, sort_pattern)
    return nil if rc(res) != 0
    return res if params[:raw]
    return res.strWaferSorterSlotMapSequence.collect {|e|
      SorterData.new(e.equipmentID.identifier, e.actionCode, e.requestUserID.identifier, e.requestTime,
        e.direction, e.sorterStatus, e.slotMapCompareStatus, e.mmCompareStatus, nil)
    }
  end

  # return a list of WaferSorter actions or nil, currently UNUSED
  def sorter_actions(eqp, params={})
    res = @manager.TxWaferSorterActionListInq(@_user, oid(eqp))
    return nil if rc(res) != 0
    return res if params[:raw]
    return res.strWaferSorterActionListSequence.collect {|a| actionCode}
  end

  # cancel an ad hoc sorter job, either :port or :pg must be passed as parameter, return 0 on success; UNUSED
  def sorter_job_cancel(eqp, params)
    pg = params[:pg] || eqp_info(eqp).ports.find {|p| p.port == params[:port]}.pg
    memo = params[:memo] || ''
    request_time = ''
    $log.info "sorter_job_cancel #{eqp} port group #{pg}"
    #
    res = @manager.TxWaferSorterOnEqpCancelReq(@_user, oid(eqp), pg, request_time, memo)
    return rc(res)
  end

  # execute WaferIDRead and wait for completion
  # multiple jobs can be executed simultaneously with multiple carriers on multiple ports
  #
  # return true on success, currently UNUSED
  # def sorter_action_waferidread(eqp, ports, params)
  #   ports = [ports] if ports.instance_of?(String)
  #   carriers = params[:carrier] || []
  #   carriers = [carriers] if carriers.instance_of?(String)
  #   lots = params[:lots] || []
  #   lots = [lots] if lots.instance_of?(String)
  #   #
  #   ports.each_with_index {|port, i|
  #     sorter_action(eqp, port, 'WaferIDRead', params.merge(carrier: carriers[i], lot: lots[i])).zero? || return
  #   }
  #   wait_for(timeout: params[:timeout]) {
  #     ret = true
  #     ports.each {|port|
  #       data = sorter_data(eqp, port: port) || return
  #       data.each {|e|
  #         if e.sorter_status != 'SP_Sorter_Succeeded'
  #           $log.info "  sorter_status #{e.sorter_status.inspect}"
  #           ret = false
  #           break
  #         elsif !['', 'OK'].member?(e.mm_compare)
  #           $log.warn "  mm_compare #{e.mm_compare.inspect}"
  #           ret = false
  #           break
  #         end
  #       }
  #     }
  #     ret
  #   } || return
  #   return true
  # end

  # compare sorter result with MM
  def wafersorter_compare(sorter, pg)
    res = @manager.TxWaferSorterDataCompareReq(@_user, pg, oid(sorter))
    return if rc(res) != 0
    return res if params[:raw]
    return res.strWaferSorterCompareCassetteSequence.collect {|p| p.strWaferSorterCompareSlotMapSequence}
  end

  # get the alias of the wafers, return 0 on success, currently UNUSED
  def wafer_alias_info(params={})
    lot = params[:lot] || ''
    aliases = params[:aliases] || ''
    aliases = aliases.split if aliases.instance_of?(String)
    #
    aliases = lot_info(lot, params.merge(wafers: true)).wafers.collect {|w| w.wafer} if aliases.empty?
    wafersinfo = @jcscode.pptWaferAliasNameInfoInqInParm_struct.new(oid(aliases), any)
    #
    res = @manager.TxWaferAliasNameInfoInq(@_user, wafersinfo)
    return nil if rc(res) != 0
    return res if params[:raw]
    return res.strAliasWaferNameSeq.collect{|w| w.aliasWaferName}
  end

  # special case of wafer sort where all wafers will be transfered to an empty slot
  # tgt_carrier may be '' to transfer all wafers to a FOSB
  #
  # return 0 on success  UNUSED
  def transfer_all_wafers(src_carrier, tgt_carrier, params={})
    $log.info "transfer_all_wafers #{src_carrier.inspect}, #{tgt_carrier.inspect}"
    $log.debug {"  #{params.inspect}"}
    eqp = params[:eqp] || ''
    notify = !!params[:notify]
    memo = params[:memo] || ''
    src_map = lot_list_incassette(src_carrier, raw: true)
    if tgt_carrier.empty?
      empty_slots = 25.times.collect {|i| jcscode.pptWaferMapInCassetteInfo_struct.new(i+1, oid(''), '', oid(''), '', any)}
      tgt_managed = false
    else
      empty_slots = lot_list_incassette(tgt_carrier, raw: true).strWaferMapInCassetteInfo.select {|w| w.waferID.identifier.empty?}
      tgt_managed = true
    end
    lots = []
    wafer_xfer = src_map.strWaferMapInCassetteInfo.collect {|w|
      next if w.waferID.identifier.empty?
      lots << w.lotID.identifier
      # find empty space in target carrier
      eslot = empty_slots.pop || ($log.warn "not enough empty slots in target carrier"; return)
      @jcscode.pptWaferTransfer_struct.new(w.waferID, oid(tgt_carrier), tgt_managed, eslot.slotNumber, oid(src_carrier), true, w.slotNumber, any)
    }.compact
    ($log.debug {"  nothing to transfer"}; return 0) if wafer_xfer.empty?
    $log.info "  transferring lots #{lots.uniq}"
    #
    res = @manager.TxWaferSortReq(@_user, oid(eqp), wafer_xfer, notify, memo)
    return rc(res)
  end

  # carrier exchange as part of a control job (for tools which have carrier exchange required)
  #
  # return 0 on success, currently UNUSED
  def carrier_exchange(eqp, lot, tgt_carrier, params={})
    $log.info "carrier_exchange #{eqp.inspect}, #{lot.inspect}, #{tgt_carrier.inspect}"
    $log.debug "  #{params.inspect}"
    memo = params[:memo] || ''
    slotmap = params[:slotmap] || _create_slotmap(lot, tgt_carrier, params) || return
    $log.info "  using eqp #{eqp.inspect}"
    #
    res = @manager.TxCassetteExchangeReq(@_user, oid(eqp), slotmap, memo)
    return rc(res)
  end

  # returns all scrapped wafers of a lot, currently UNUSED
  def scrap_history(lot, params={})
    res = @manager.TxScrapHistoryInq(@_user, oid(lot), oid(''))
    return nil if rc(res) != 0
    return res if params[:raw]
    return res.strScrapHistories.collect {|w| w.waferID.identifier}
  end

end
