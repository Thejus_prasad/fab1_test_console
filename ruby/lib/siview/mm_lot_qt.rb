=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.
=end


class SiView::MM

  # return lot QT data for future operation (default: on current route), used in MM_Lot_QTime only
  def lot_operation_in_qtime(lot, opNo, params={})
    product = params[:product] || '' # should be empty if lot is specified
    route = params[:route] || lot_info(lot).route
    ope_info = params[:ope_info] || 'PRE' # or POST, LocateBackward, Current
    hold_type = params[:hold_type] || ''
    res = @manager.CS_TxLotOperationInQTimeZoneInq__C621(@_user, oid(lot), oid(product), oid(route), opNo, ope_info, hold_type)
    return nil if rc(res) != 0
    return res
  end

  # reset lot qtime, used internally only, return 0 on success, UNUSED
  def lot_qtime_reset(lot, params={})
    $log.info "lot_qtime_reset #{lot.inspect}"
    res = @manager.TxQrestTimeActionExecReq(@_user, oid(lot), params[:memo] || '')
    return rc(res)
  end

  # return list of qtime candidates for the given lot or nil, UNUSED
  def lot_qtime_candidate(lot, params={})
    route = params[:route] || ''    # not used
    opNo = params[:opNo] || ''      # not used
    branch_info = params[:branch_info] || ''
    inparm = @jcscode.pptQrestTimeCandidateInqInParm_struct.new(oid(lot), oid(route), opNo, branch_info, any)
    res = @manager.TxQrestTimeCandidateInq(@_user, inparm)
    return nil if rc(res) != 0
    return res
  end

  # return list of lot's Q-time information (empty if none)
  def lot_qtime_list(lot, params={})
    lotID = lot.instance_of?(String) ? oid(lot) : lot
    res = @manager.TxQrestTimeListInq__150(@_user, lotID, !!params[:active])
    return nil if rc(res) != 0
    return res.strQrestLotInfo
  end

  # change a lot's Q-time data, delete if if action is 'Delete', return 0 on success
  def lot_qtime_update(lot, action, params={})
    lotID = lot.instance_of?(String) ? oid(lot) : lot
    $log.info "lot_qtime_update #{lotID.identifier.inspect}, #{action.inspect}" unless params[:silent]
    qtlist = lot_qtime_list(lotID, active: params[:active])
    ($log.info '  lot has no qtime'; return 0) if qtlist.empty? && !params[:silent]
    qtinfo = qtlist.first.strQtimeInfo
    if tend = params[:expire]
      # expire the current QT, action is 'Update'
      tend = Time.now + 5 if tend == true  # must be in the future
      ts = siview_timestamp(tend)
      qtinfo.each {|qi|
        qi.qrestrictionTargetTimeStamp = ts
        qi.strQtimeActionInfo.each {|qai| qai.qrestrictionTargetTimeStamp = ts}
      }
    end
    inparm = @jcscode.pptQrestTimeUpdateReqInParm_struct.new(action, qtlist.first.lotID, qtinfo, any)
    #
    res = @manager.TxQrestTimeUpdateReq(@_user, inparm, params[:memo] || '')
    return rc(res)
  end

end
