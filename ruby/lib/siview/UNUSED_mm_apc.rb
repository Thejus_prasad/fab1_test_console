=begin
(c) Copyright 2015 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

=end

# SiView APC and DC related transactions
class SiView::MM
  # CorrespondingOperation = Struct.new(:seq, :opNo, :pd, :pd_name, :dc_specgroup, :dc_specid, :dcitems)
  # DCSpec = Struct.new(:opNo, :op, :conditions)
  # DCCondition = Struct.new(:technology, :productgroup, :product, :dcitems)
  # DCDeltaDefinition = Struct.new(:preMeas_opNo, :preMeas_pd, :deltadcdef, :deltadcspecid, :ddcitems, :predcdef, :predcspec, :predcitems,
  #   :postdcdef, :postdcspec, :postdcitems)
  # DCSpecItem = Struct.new(:name,
  #   :usl, :target, :lsl, :usl_action_codes, :lsl_action_codes,
  #   :ucl, :lcl, :ucl_action_codes, :lcl_action_codes,
  #   :uscrn, :lscrn, :uscrn_action_codes, :lscrn_action_codes,
  #   :tag, :dc_specgroup)
  # DCItem = Struct.new(:name, :mode, :unit, :data_type, :item_type, :measurement_type,
  #   :wafer, :wafer_pos, :site_pos, :history, :calc_type, :calc_expression, :data_value, :target_value, :spec_check, :action_codes)


  # # unused
  # def apc_runtime_capability(eqp, controljob, params={})
  #   sendtx = params[:sendtx] != false # default is true
  #   lislr, pg, ib = lots_info_slr(eqp, params[:lot], params)
  #   #
  #   res = @manager.TxAPCRunTimeCapabilityInq(@_user, oid(eqp), oid(controljob), lislr, sendtx)
  #   return rc(res)
  # end

  # # Convenience method to query existing DC Spec limits from SiView by current lot info
  # # returns DCSpecs or nil  - unused
  # def lot_apc_speclimit(lot, params={})
  #   li = lot_info(lot)
  #   apc_speclimit(li.route, li.opNo, li.technology, li.productgroup, li.product, params)
  # end

  # # Query existing DC Spec limits from SiView by given information - unused
  # # returns DCSpecs or nil
  # def apc_speclimit(route, opNo, technology, productgroup, product, params={})
  #   lot = params[:lot] || ''
  #   res = @manager.CS_TxAPCSpecLimitInq__C75(@_user, lot, route, opNo, technology, productgroup, product)
  #   return nil if rc(res) != 0
  #   return res if params[:raw]
  #   res.strDCSpecSeq__C75.collect {|dcspec|
  #     conds = dcspec.strDCSpecCondSeq.collect { |cond|
  #       items = cond.strDCSpecItemSeq__C75.collect { |item|
  #         DCSpecItem.new(item.ItemName,
  #           item.UpperSpecLimitRequired ? item.UpperSpecLimit : nil, item.TargetSpecLimit,
  #           item.LowerSpecLimitRequired ? item.LowerSpecLimit : nil, item.ActionCodes_Usl, item.ActionCodes_Lsl,
  #           item.UpperControlLimitRequired ? item.UpperControlLimit : nil,
  #           item.LowerControlLimitRequired ? item.LowerControlLimit : nil, item.ActionCodes_Ucl, item.ActionCodes_Lcl,
  #           item.UpperScreenLimitRequired ? item.UpperScreenLimit : nil,
  #           item.LowerScreenLimitRequired ? item.LowerScreenLimit : nil, item.ActionCodes_Uscrn, item.ActionCodes_Lscrn,
  #           item.Tag, item.DcSpecGroup
  #         )
  #       }
  #       DCCondition.new(cond.Technology, cond.ProductGroupID, cond.ProductID, items)
  #     }
  #     DCSpec.new(dcspec.OpNum, dcspec.OpID, conds)
  #   }
  # end

  # Get Corresponding operations for a given lot or route operation, technology, productgroup, product context
  # returns [<list or corresponding process operations>, <list of corresponding metro operations>] -- unused
  # def lot_corresponding_operations(lot, params={})
  #   route = params[:route] || ''
  #   opNo = params[:opNo] || ''
  #   technology = params[:technology] || ''
  #   productgroup = params[:productgroup] || ''
  #   product = params[:product] || ''
  #   inparm = @jcscode.pptCS_LotCorrespondingOperationInfoInqInParm__C75_struct.new(oid(lot), route, opNo, technology, productgroup, product, any)
  #   res = @manager.CS_TxLotCorrespondingOperationInfoInq__C75(@_user, inparm)
  #   return nil if rc(res) != 0
  #   return res if params[:raw]
  #   [res.strCS_CorrespondingProcessOpeInfoList, res.strCS_CorrespondingMeasurementOpeInfoList].collect {|opinfos|
  #     opinfos.collect {|op|
  #       dc_specs = _extract_dc_items(op.strDCSpecList)
  #       CorrespondingOperation.new(op.seqNo, op.correspondingOpeNo, op.processDefinitionID.identifier,
  #         op.processDefinitionName, op.dcSpecGroup, op.dataCollectionSpecificationID.identifier, dc_specs)
  #     }
  #   }
  # end

  # Get DeltaDC definitions for the given lot - unused
  # def lot_deltadc_definitions(lot, params={})
  #   route = params[:route] || ''
  #   opNo = params[:opNo] || ''
  #   technology = params[:technology] || ''     #optional
  #   productgroup = params[:productgroup] || '' #optional
  #   product = params[:product] || ''           #optional
  #   inparm = @jcscode.pptCS_LotDeltaDCDefinitionInfoInqInParm__C75_struct.new(
  #     oid(lot), oid(route), opNo, oid(technology), oid(productgroup), oid(product), any)
  #   res = @manager.CS_TxLotDeltaDCDefinitionInfoInq__C75(@_user, inparm)
  #   return nil if rc(res) != 0
  #   return res if params[:raw]
  #   [res.strCS_PreDeltaDCDefinitionInfoList__C75, res.strCS_PostDeltaDCDefinitionInfoList__C75].collect {|list|
  #     list.collect {|ddc|
  #       ddc_def = ddc.strDeltaDCDef
  #       ddc_specs = _extract_dc_items(ddc_def.strDeltaDCSpecList)
  #       pre_specs = _extract_dc_items(ddc_def.strPreDCSpecList)
  #       post_specs = _extract_dc_items(ddc_def.strPostDCSpecList)
  #       DCDeltaDefinition.new(ddc.operationNumber, ddc.processDefinitionID.identifier,
  #         ddc_def.deltaDCDefID.identifier, ddc_def.deltaDCSpecID.identifier, ddc_specs,
  #         ddc_def.preDCDefID.identifier, ddc_def.preDCSpecID.identifier, pre_specs,
  #         ddc_def.postDCDefID.identifier, ddc_def.postDCSpecID.identifier, post_specs
  #       )
  #     }
  #   }
  # end

  # Helper to extract dc items
  def _extract_dc_items(dcitem_list)
    dcitem_list.collect {|item|
      DCSpecItem.new(item.dataItemName,
        item.specLimitUpperRequired ? item.specLimitUpper : nil, item.target,
        item.specLimitLowerRequired ? item.specLimitLower : nil, item.actionCodes_usl, item.actionCodes_lsl,
        item.controlLimitUpperRequired ? item.controlLimitUpper : nil,
        item.controlLimitLowerRequired ? item.controlLimitLower : nil, item.actionCodes_ucl, item.actionCodes_lcl,
        item.screenLimitUpperRequired ? item.screenLimitUpper : nil,
        item.screenLimitLowerRequired ? item.screenLimitLower : nil, item.actionCodes_uscrn, item.actionCodes_lscrn,
        item.tag, item.dcSpecGroup
      )
    }
  end

  # # Retrieve existing DCItems for the control job
  # # returns array of DCItems filtered by :lots, :parameters - unused
  # def eqp_dcitems(eqp, cj, params={})
  #   res = @manager.TxDataItemWithTempDataInq(@_user, oid(eqp), oid(cj))
  #   return nil if rc(res) != 0
  #   return res if params[:raw]
  #   dcitems = []
  #   lots = params[:lots]
  #   parameters = params[:parameters]
  #   res.strStartCassette.each {|cas|
  #     cas.strLotInCassette.each {|l|
  #       next if lots && !lots.include?(l.lotID.identifier)
  #       l.strStartRecipe.strDCDef.each {|dc|
  #         dc.strDCItem.each {|i|
  #           next if parameters && !parameters.include?(i.dataCollectionItemName)
  #           dcitems << DCItem.new(i.dataCollectionItemName, i.dataCollectionMode, i.dataCollectionUnit, i.dataType, i.itemType,
  #             i.measurementType, i.waferID.identifier, i.waferPosition, i.sitePosition, i.historyRequiredFlag, i.calculationType,
  #             i.calculationExpression, i.dataValue, i.targetValue, i.specCheckResult, i.actionCode.to_a
  #           )
  #         }
  #       }
  #     }
  #   }
  #   return dcitems
  # end

  # # unused
  # def data_collection_list(params={})
  #   lot = params[:lot] || ''
  #   eqp = params[:eqp] || ''
  #   mrecipe = params[:mrecipe] || '' # supports wildcards
  #   pd = params[:pd] || ''
  #   dc_def = params[:dc_def] || ''
  #   object_id = params[:object_id] || ''
  #   object_type = params[:object_type] || 'DCDefinition' # 'DCSpecification'
  #   dc_type = params[:dc_type] || '' #
  #   search_criteria = params[:search_crit] || 'All' # White, NonWhite
  #   max_count = params[:count] || 0
  #   fpc_cat = params[:fpc_cat] || ''
  #   dc_search_criteria = params[:dc_search_crit] || 'All' # PD
  #   inparm = @jcscode.pptDataCollectionListInqInParm_struct.new(oid(lot), oid(eqp), oid(mrecipe), oid(pd), oid(dc_def),
  #     oid(object_id), object_type, dc_type, search_criteria, max_count, fpc_cat, dc_search_criteria, any)
  #   res = @manager.TxDataCollectionListInq(@_user, inparm)
  #   return nil if rc(res) != 0
  #   return res.strDataCollectionList.collect {|dc| dc.objectID.identifier}
  # end

end
