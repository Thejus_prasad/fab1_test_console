=begin
(c) Copyright 2013 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

History:
  2015-09-04 dsteger, merged code from Fab7QA
=end

class SiView::MM

  # pass e.g. {lot: lot, pd: pd} or {mrecipe: 'M%', search: 'All'}, use case?
  def fpc_mrecipes(params)
    lot = params[:lot] || ''
    eqp = params[:eqp] || ''
    mrecipe = params[:mrecipe] || '%' # supports wildcards
    pd = params[:pd] || ''
    fpc_category = params[:fpc_category] || ''
    fpc_criteria = params[:fpc_crit] || 'All' # White, NonWhite
    search = params[:search] || 'PD'  #'All'
    inparm = @jcscode.pptMachineRecipeListForFPCInqInParm_struct.new(
      oid(lot), oid(eqp), oid(mrecipe), oid(pd), fpc_category, fpc_criteria, search, any)
    #
    res = @manager.TxMachineRecipeListForFPCInq(@_user, inparm)
    return nil if rc(res) != 0
    return res.strMachineRecipeList
  end

  # find all fpc definitions on the lot's route and connected subroutes, used for lot_fpc_create
  def lot_fpc_processlist(lot, params={})
    lotID = lot.instance_of?(String) ? oid(lot) : lot
    inparm = @jcscode.pptFPCProcessListInRouteInqInParm_struct.new(lotID, any)
    #
    res = @manager.TxFPCProcessListInRouteInq__150(@_user, inparm)
    return nil if rc(res) != 0
    return res
  end

  # return list of strFPCInfo, pass e.g. lot or fpcids
  def lot_fpc_infos(params)
    fpcids = params[:fpcids] || []
    fpcids = [fpcids] if fpcids.instance_of?(String)
    lot = params[:lot] || oid('')
    lotID = lot.instance_of?(String) ? oid(lot) : lot
    route = params[:route] || oid('')
    routeID = route.instance_of?(String) ? oid(route) : route
    opNo = params[:opNo] || ''
    org_route = params[:org_route] || oid('')
    org_routeID = org_route.instance_of?(String) ? oid(org_route) : org_route
    org_opNo = params[:org_opNo] || ''
    sub_route = params[:sub_route] || oid('')
    sub_routeID = sub_route.instance_of?(String) ? oid(sub_route) : sub_route
    sub_opNo = params[:sub_opNo] || ''
    inparam = @jcscode.pptFPCDetailInfoInqInParm_struct.new(fpcids.to_java(:String), lotID,
      routeID, opNo, org_routeID, org_opNo, sub_routeID, sub_opNo, true, true, true, true, any)
    if @customized
      res = @manager.CS_TxFPCDetailInfoInq__101(@_user, inparam)
      return nil if rc(res) != 0
      return res.strCS_WholeFPCInfoList
    else
      res = @manager.TxFPCDetailInfoInq__101(@_user, inparam)
      return nil if rc(res) != 0
      return res.strFPCInfoList
    end
  end

  # delete FPCs specified by the FPC IDs for a lot _family_, return 0 on success, UNUSED
  def lot_fpc_delete(lot, fpcids, params={})
    lotID = lot.instance_of?(String) ? oid(lot) : lot
    fpcids = [fpcids] if fpcids.instance_of?(String)
    $log.info "lot_fpc_delete #{lotID.identifier.inspect}, #{fpcids}"
    inparam = @jcscode.pptFPCDeleteReqInParm_struct.new(fpcids.to_java(:String), lotID, any)
    #
    res = @manager.TxFPCDeleteReq(@_user, inparam, params[:memo] || '')
    return rc(res)
  end

  # create an FPC definition for lot at the current opNo on the branch route
  def lot_fpc_create(lot, params={})
    li = lots_info_raw(lot, wafers: true)
    li_basic = li.strLotInfo[0].strLotBasicInfo
    hold = params[:hold] == false ? false : true  # defaults to true, enabling update and delete
    $log.info "lot_fpc_create #{li_basic.lotID.identifier.inspect}, #{{hold: hold}.merge(params)}"
    skip_flag = !!params[:skip]
    force_skip = !!params[:force_skip]
    ($log.info "  setting skip flag since force_skip is set"; skip_flag = true) if force_skip
    reticles = params[:reticle] || []   # TODO: params[:reticles] (in MM_LOT_BackupOperation)
    reticles = [reticles] unless reticles.instance_of?(Array)
    # eqp
    eqp = params[:eqp] || oid('')
    eqpID = eqp.instance_of?(String) ? oid(eqp) : eqp
    # mrecipe
    mrecipe = params[:mrecipe] || oid('')
    mrecipeID = mrecipe.instance_of?(String) ? oid(mrecipe) : mrecipe
    # wafers
    rcpparams = [].to_java(@jcscode.pptRecipeParameterInfo_struct)   # currently not used
    wafers = li.strLotInfo[0].strLotWaferAttributes.collect {|w|
      @jcscode.pptLotWaferInfo_struct.new(w.waferID, rcpparams, any)
    }.to_java(@jcscode.pptLotWaferInfo_struct)
    # reticles and reticle groups
    reticles = reticles.collect.each_with_index {|r, i|
      @jcscode.pptReticleInfo_struct.new(i, oid(r), reticle_status(r).reticleBRInfo.reticleGroupID, any)
    }.to_java(@jcscode.pptReticleInfo_struct)
    # routes and opNos
    rifpc = lot_fpc_processlist(li_basic.lotID).strLotRouteInfoForFPC
    if rifpc.originalMainPDID.identifier.empty?
      # lot is still on the original route, TODO!!!
      pdID = ope_info.operationID
      main_routeID = oid(params[:subroute])
    else
      # lot is on the branch route
      opinfo = li.strLotInfo.first.strLotOperationInfo
      pdID = opinfo.operationID
      pdType = opinfo.pdType
      main_routeID = rifpc.mainPDID             # the sub route
      main_opNo = rifpc.operationNumber         # opNo on the sub route
      org_routeID = rifpc.originalMainPDID
      org_opNo = rifpc.originalOperationNumber
      sub_routeID = oid('')
      sub_opNo = ''
    end
    #
    fpci = @jcscode.pptFPCInfo__101_struct.new
    fpci.FPC_ID = ''  # must be empty
    fpci.lotFamilyID = li_basic.familyLotID
    fpci.mainPDID = main_routeID
    fpci.operationNumber = main_opNo
    fpci.originalMainPDID = org_routeID
    fpci.originalOperationNumber = org_opNo
    fpci.subMainPDID = sub_routeID
    fpci.subOperationNumber = sub_opNo
    fpci.FPCGroupNo = 0
    fpci.FPCType = 'ByLot'  # or ByWafer for 'partial' FPC
    fpci.mergeMainPDID = oid(params[:merge_route] || '')
    fpci.mergeOperationNumber = params[:mrg_opNo] || ''
    fpci.FPCCategory = ''
    fpci.pdID = pdID
    fpci.pdType = pdType
    fpci.skipFlag = skip_flag
    fpci.correspondingOperNo = ''
    fpci.restrictEqpFlag = !!params[:restrict_eqp]
    fpci.equipmentID = eqpID
    fpci.machineRecipeID = mrecipeID
    fpci.dcDefID = oid('')
    fpci.dcSpecID = oid('')
    fpci.sendEmailFlag = false
    fpci.holdLotFlag = hold
    fpci.recipeParameterChangeType = ''
    fpci.strLotWaferInfoList = wafers
    fpci.strReticleInfoList = reticles
    fpci.strDCSpecList = [].to_java(@jcscode.pptDCSpecDetailInfo_struct)
    fpci.description = 'QA Test'
    fpci.createTime = ''  # must be empty
    fpci.updateTime = ''  # must be empty
    fpci.claimUserID = @user
    fpci.strCorrespondingOperationInfoList = [].to_java(@jcscode.pptCorrespondingOperationInfo_struct)
    fpci.siInfo = any
    # only one entry is supported right now
    action = 'Create'
    if @customized
      csfpci = @jcscode.pptCS_CSFPCInfo_struct.new(force_skip, '', oids([]), '', any)
      actionlist = [@jcscode.pptCS_FPCInfoAction__101_struct.new(action, fpci, csfpci, any)]
    else
      actionlist = [@jcscode.pptFPCInfoAction__101_struct.new(action, fpci, any)]
    end
    #
    if @customized
      inparm = @jcscode.pptCS_FPCUpdateReqInParm__101_struct.new(actionlist.to_java(@jcscode.pptCS_FPCInfoAction__101_struct), any)
      res = @manager.CS_TxFPCUpdateReq__101(@_user, inparm, params[:memo] || '')
    else
      inparm = @jcscode.pptFPCUpdateReqInParm__101_struct.new(actionlist.to_java(@jcscode.pptFPCInfoAction__101_struct), any)
      res = @manager.TxFPCUpdateReq__101(@_user, inparm, params[:memo] || '')
    end
    return nil if rc(res) != 0
    return res.FPC_IDs.collect {|e| e}
  end

  # only first found FPC definition gets updated, return fpcids on success
  def lot_fpc_update(params)
    # get FPC definition identified by lot_fpc_infos params
    fpcinfo = lot_fpc_infos(params).first
    fpci = fpcinfo.strFPCInfo
    $log.info "lot_fpc_update lot: #{fpci.lotFamilyID.identifier.inspect}, fpcids: [#{fpci.FPC_ID}]"
    # updates
    if eqp = params[:eqp]
      fpci.equipmentID = eqp.instance_of?(String) ? oid(eqp) : eqp
    end
    if mrecipe = params[:mrecipe]
      fpci.machineRecipeID = mrecipe.instance_of?(String) ? oid(mrecipe) : mrecipe
    end
    hold = params[:hold]
    if !hold.nil?
      fpci.holdLotFlag = hold
      $log.info "  hold: #{hold}"
    end
    action = 'Update'
    if @customized
      csfpci = fpcinfo.strCS_CSFPCInfo
      actionlist = [@jcscode.pptCS_FPCInfoAction__101_struct.new(action, fpci, csfpci, any)]
    else
      actionlist = [@jcscode.pptFPCInfoAction__101_struct.new(action, fpci, any)]
    end
    if @customized
      # fpc_infos.each {|wfpc|
      #   next if wfpc.strFPCInfo.FPC_ID == fpc_id  # ??
      #   actionlist << @jcscode.pptCS_FPCInfoAction__101_struct.new('NoChange', wfpc.strFPCInfo, wfpc.strCS_CSFPCInfo, any)
      # }
      inparm = @jcscode.pptCS_FPCUpdateReqInParm__101_struct.new(actionlist.to_java(@jcscode.pptCS_FPCInfoAction__101_struct), any)
      res = @manager.CS_TxFPCUpdateReq__101(@_user, inparm, params[:memo] || '')
    else
      # fpc_infos.each {|fpc|
      #   next if fpc.FPC_ID == FPC_ID  # ??
      #   actionlist << @jcscode.pptFPCInfoAction__101_struct.new('NoChange', fpc, any)
      # }
      inparm = @jcscode.pptFPCUpdateReqInParm__101_struct.new(actionlist.to_java(@jcscode.pptFPCInfoAction__101_struct), any)
      res = @manager.TxFPCUpdateReq__101(@_user, inparm, params[:memo] || '')
    end
    return nil if rc(res) != 0
    return res.FPC_IDs.collect {|e| e}
  end

end
