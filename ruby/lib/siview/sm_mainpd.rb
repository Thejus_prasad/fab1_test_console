=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: Steffen Steidten, 2009-05-03; Daniel Steger, 2014-10-30

History:
  2021-12-16 sfrieske, cleanup

=end


class SiView::SM

  # change the carrier category at an route opNo, return true on success, for RTD_Rule_GeckoA3
  def mainpd_change_carrier_category(route, opNo, cat, params={})
    $log.info "mainpd_change_carrier_category #{route.inspect}, #{opNo.inspect}, #{cat.inspect}"
    catid = object_ids(:carriercategory, cat).first || ($log.warn '  no such category'; return)
    edit_objects(:mainpd, route, params) {|oinfos|
      oinfos.each {|e|
        ci = extract_si(e.classInfo)
        #
        odata = ci.mainProcessOperations.find {|o| o.operationNumber == opNo} || ($log.warn '  no such opNo'; return)
        odata.carrierCategory = catid
        #
        e.classInfo = insert_si(ci)
      }
    }
  end

  # change a route lagtime (in minutes) at an opNo, pass 0 to remove, for JCAP_AutoFOUPCleaning
  #
  # return true on success
  def mainpd_change_lagtime(route, opNo, t, params={})
    $log.info "mainpd_change_lagtime #{route.inspect}, #{opNo.inspect}, #{t}"
    edit_objects(:mainpd, route, params) {|oinfos|
      oinfos.each {|e|
        ci = extract_si(e.classInfo)
        #
        odata = ci.mainProcessOperations.find {|o| o.operationNumber == opNo} || ($log.warn "no such opNo"; return)
        odata.defaultProcessLagTime = t
        #
        e.classInfo = insert_si(ci)
      }
    }
  end

  # # delete a route qtime at an opNo, pass 1 or more SM::QtInfo to define the qtime to delete, UNUSED
  # #
  # # return true on success
  # def mainpd_delete_qtime(route, opNo, qtinfos, params={})
  #   qtinfos = [qtinfos] unless qtinfos.instance_of?(Array)
  #   $log.info "mainpd_delete_qtime #{route.inspect}, #{opNo.inspect}, #{qtinfos.inspect}"
  #   edit_objects(:mainpd, route, params) {|oinfos|
  #     oinfos.each {|e|
  #       ci = extract_si(e.classInfo)
  #       #
  #       odata = ci.mainProcessOperations.find {|o| o.operationNumber == opNo} || ($log.warn "no such opNo"; return)
  #       qts = odata.defaultTimeRestrictions.to_a
  #       qtinfos.each {|qtinfo|
  #         qts.delete_if {|qt|
  #           qt.targetOperationNumber == qtinfo.target_opNo &&
  #           qt.actionOperationNumber == qtinfo.action_opNo &&
  #           qt.reasonCode.identifier == qtinfo.reason &&
  #           qt.reworkMainProcessDefinition.identifier == qtinfo.rework &&
  #           qt.duration == qtinfo.duration && qt.action == qtinfo.action &&
  #           qt.timing == qtinfo.timing && qt.customField == qtinfo.custom
  #         }
  #       }
  #       odata.defaultTimeRestrictions = qts.to_java(@jcscode.brDefaultTimeRestrictionData_struct)
  #       #
  #       e.classInfo = insert_si(ci)
  #     }
  #   }
  # end

  # add a branch route at an opNo, for MM_Lot_PSM, return true on success
  def mainpd_add_branch(route, opNo, brinfo, params={})
    $log.info "mainpd_add_branch #{route.inspect}, #{opNo.inspect}, #{brinfo.inspect}"
    boid = object_ids(:mainpd, brinfo[:mainpd]).first || ($log.info 'no such branch route'; return)
    newbr = @jcscode.brSubFlowData_struct.new(boid, brinfo[:joining_opNo])
    edit_objects(:mainpd, route, params) {|oinfos|
      oinfos.each {|e|
        ci = extract_si(e.classInfo)
        odata = ci.mainProcessOperations.find {|o| o.operationNumber == opNo} || ($log.warn 'no such opNo'; return)
        odata.subProcessDefinitions = (odata.subProcessDefinitions.to_a << newbr).to_java(@jcscode.brSubFlowData_struct)
        e.classInfo = insert_si(ci)
      }
    }
  end

  # delete a branch route at an opNo, for MM_Lot_PSM
  #
  # return true on success
  def mainpd_delete_branch(route, opNo, brinfo, params={})
    $log.info "mainpd_delete_branch #{route.inspect}, #{opNo.inspect}, #{brinfo.inspect}"
    edit_objects(:mainpd, route, params) {|oinfos|
      oinfos.each {|e|
        ci = extract_si(e.classInfo)
        #
        odata = ci.mainProcessOperations.find {|o| o.operationNumber == opNo} || ($log.warn 'no such opNo'; return)
        branches = odata.subProcessDefinitions.to_a.delete_if {|branch|
          branch.mainProcessDefinition.identifier == brinfo[:mainpd] &&
          branch.joiningOperationNumber == brinfo[:joining_opNo]
        }
        odata.subProcessDefinitions = branches.to_java(@jcscode.brSubFlowData_struct)
        #
        e.classInfo = insert_si(ci)
      }
    }
  end

  # # delete a route  at an opNo, UNUSED
  # #
  # # return true on success
  # def mainpd_delete_rework(route, opNo, rwkinfo, params={})
  #   $log.info "mainpd_delete_rework #{route.inspect}, #{opNo.inspect}, #{rwkinfo.inspect}"
  #   edit_objects(:mainpd, route, params) {|oinfos|
  #     oinfos.each {|e|
  #       ci = extract_si(e.classInfo)
  #       #
  #       odata = ci.mainProcessOperations.find {|o| o.operationNumber == opNo} || ($log.warn "no such opNo"; return)
  #       rwks = odata.reworkProcessDefinitions.to_a.delete_if {|rwk|
  #         rwk.mainProcessDefinition.identifier == rwkinfo[:mainpd] &&
  #         rwk.joiningOperationNumber == rwkinfo[:joining_opNo]
  #       }
  #       odata.reworkProcessDefinitions = rwks.to_java(@jcscode.brSubFlowData_struct)
  #       #
  #       e.classInfo = insert_si(ci)
  #     }
  #   }
  # end

end
