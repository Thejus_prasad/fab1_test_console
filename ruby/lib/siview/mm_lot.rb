=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.
=end


class SiView::MM
  LotInfo = Struct.new(:lot, :parent, :family, :user, :claim_time, :lottype, :sublottype, :content, :vendorlot,
    :priority, :ipriority, :epriority, :sjexists, :pp_carrier, :pp_lot, :qtime, :status, :states, :bank, :cj,
    :carrier, :xfer_status, :cast_cat, :eqp, :stocker, :technology, :productgroup, :product, :layer, :route, :op, :opNo, :pdtype,
    :opName, :mandatory, :opEqps, :stage, :masklevel, :department, :reworkcount, :customer, :nwafers, :wafers, :backup)  #, :extended)
  LotBackupInfo = Struct.new(:backup_proc, :current_loc, :transfering, :born, :src, :dest)
  LotBackupOpData = Struct.new(:state, :servername, :route, :opNo)
  LotNote = Struct.new(:lot, :title, :desc, :user, :timestamp)
  Wafer = Struct.new(:wafer, :slot, :alias, :lot)

  # extract information from TxLotList like results, return LotInfo like array
  def _extract_lotlist_attrs(lotlistattrs, params={})
    return lotlistattrs.collect {|l|
      li = LotInfo.new(l.lotID.identifier, nil, l.lotFamilyID.identifier, l.lotOwnerID.identifier, l.lastClaimedTimeStamp,
        l.lotType, l.subLotType, nil, nil, nil, nil, nil, nil, nil, nil, nil, l.lotStatus,
        Hash[l.strLotStatusList.collect {|e| [e.stateName, e.stateValue]}], l.bankID.identifier, nil,
        l.carrierID.identifier, nil, l.requiredCassetteCategory, nil, nil, nil, nil, l.productID.identifier, nil,
        l.routeID.identifier, nil, l.operationNumber,
        nil, nil, nil, nil, nil, nil, nil, nil, l.customerCode, l.totalWaferCount, nil, nil)
      if params[:wafers]  && l.class.name.end_with?('pptCS_LotListAttributes__100_struct')
        li.wafers = []
        l.waferIDs.each {|w|
          next if w.lotID.identifier != l.lotID.identifier
          li.wafers << Wafer.new(w.waferID.identifier, w.slotNumber, w.aliasWaferName, w.lotID.identifier)
        }
      end
      if params[:backup]
        b = l.strLotBackupInfo
        d = b.strLotBackupSourceDataSeq.last
        srcdata = d.nil? ? nil : LotBackupOpData.new(d.backupState, d.strBackupAddress.serverName,
          d.strBackupProcess.entryRouteID, d.strBackupProcess.entryOperationNumber)
        d = b.strLotBackupDestDataSeq.last
        dstdata = d.nil? ? nil : LotBackupOpData.new(d.backupState, d.strBackupAddress.serverName,
          d.strBackupProcess.entryRouteID, d.strBackupProcess.entryOperationNumber)
        li.backup = LotBackupInfo.new(b.backupProcessingFlag, b.currentLocationFlag, b.transferFlag,
          b.strBornSiteAddress.serverName, srcdata, dstdata)
      end
      li.sjexists = l.sorterJobExistFlag
      li.pp_carrier = l.inPostProcessFlagOfCassette
      li.pp_lot = l.inPostProcessFlagOfLot
      li.pdtype = l.pdType if @release >= 15 && !l.class.name.end_with?('pptCS_LotListAttributes__100_struct')
      li
    }
  end

  # return an array of lots or pptLotListAttributes_struct if raw or nil on error
  def lot_list(params={})
    autoDispatchControlLot = false
    backup_proc = !!params[:backup_proc]
    bankID = oid(params[:bank] || '')
    bankin = !!params[:bankin]
    carriercat = params[:carrier_category] || ''
    customer = params[:customer] || ''
    delete = !!params[:delete]
    holdreason = params[:holdreason] || ''
    interFabXferState = params[:interFabXferState] || ''
    lot = params[:lot] || '%'
    lotID = lot.instance_of?(String) ? oid(lot) : lot
    lotStatus = params[:status] || ''
    lotType = params[:lottype] || ''
    layer = params[:layer] || ''
    opNo = params[:opNo] || ''
    order = params[:order] || ''
    productID = oid(params[:product] || '')
    routeID = oid(params[:route] || '')
    sjcheck = !!params[:sjcheck]
    sublottype = params[:sublottype] || ''
    virtualop = !!params[:virtualop]  # doesn't seem to make any difference in C7.7
    #
    # decide on the Tx to use
    if @release < 14  # AsmView is R12
      res = @manager.TxLotListInq__100(@_user, bankID, lotStatus, lotType, productID, order, customer,
        oid(layer), routeID, opNo, bankin, lotID, carriercat, backup_proc, sublottype, delete,
        oid(holdreason), sjcheck, interFabXferState)
    else
      res = @manager.TxLotListInq__160(@_user, bankID, lotStatus, lotType, productID, order, customer,
        oid(layer), routeID, opNo, bankin, lotID, carriercat, backup_proc, sublottype, delete,
        oid(holdreason), sjcheck, interFabXferState, autoDispatchControlLot, virtualop)
    end
    #
    ($log.warn "error in TxLotListInq for #{params.inspect}"; return) unless rc(res).zero?
    return res.strLotListAttributes if params[:raw]
    return params[:lotinfo] ? _extract_lotlist_attrs(res.strLotListAttributes, params) : res.strLotListAttributes.collect {|l| l.lotID.identifier}
  end

  # return true if lot exists in SiView
  def lot_exists?(lot)
    res = lot_list(lot: lot, raw: true) || return
	  return !res.empty?
  end

  # return sorted array of lots in the carrier
  def lot_list_incassette(carrier, params={})
    carrierID = carrier.instance_of?(String) ? oid(carrier) : carrier
    res = @manager.TxLotListInCassetteInq(@_user, carrierID)
    return if rc(res) != 0
    return res if params[:raw]
    return res.strLotListInCassetteInfo.lotID.collect {|l| l.identifier}.sort
  end

  # extract information from TxLotInfoInq like results, return [LotInfo]
  # select can be a number, an array of slot numbers (int) or array of aliases (string)
  def _extract_lots_info(liraw, params={})
    return liraw.strLotInfo.collect {|li|
      l = LotInfo.new(
        li.strLotBasicInfo.lotID.identifier,
        li.strLotBasicInfo.parentLotID.identifier,
        li.strLotBasicInfo.familyLotID.identifier,
        li.strLotBasicInfo.lastClaimedUserID.identifier,
        li.strLotBasicInfo.lastClaimedTimeStamp,
        li.strLotBasicInfo.lotType,
        li.strLotBasicInfo.subLotType,
        li.strLotBasicInfo.lotContent,
        li.strLotBasicInfo.vendorLotID,
        li.strLotBasicInfo.priorityClass,
        li.strLotBasicInfo.internalPriority,
        li.strLotBasicInfo.externalPriority,
        li.strLotBasicInfo.sorterJobExistFlag,
        li.strLotBasicInfo.inPostProcessFlagOfCassette,
        li.strLotBasicInfo.inPostProcessFlagOfLot,
        li.strLotBasicInfo.qtimeFlag,
        li.strLotBasicInfo.lotStatus,
        Hash[li.strLotBasicInfo.strLotStatusList.collect {|e| [e.stateName, e.stateValue]}],
        li.strLotBasicInfo.bankID.identifier,
        li.strLotControlJobInfo.controlJobID.identifier,
        li.strLotLocationInfo.cassetteID.identifier,
        li.strLotLocationInfo.transferStatus,
        li.strLotBasicInfo.requiredCassetteCategory,
        li.strLotLocationInfo.equipmentID.identifier,
        li.strLotLocationInfo.stockerID.identifier,
        li.strLotProductInfo.technologyCode,
        li.strLotProductInfo.productGroupID.identifier,
        li.strLotProductInfo.productID.identifier,
        li.strLotProductInfo.manufacturingLayer,
        li.strLotOperationInfo.routeID.identifier,
        li.strLotOperationInfo.operationID.identifier,
        li.strLotOperationInfo.operationNumber,
        @release < 15 ? nil : li.strLotOperationInfo.pdType,
        li.strLotOperationInfo.operationName,
        li.strLotOperationInfo.mandatoryOperationFlag,
        li.strLotOperationInfo.strLotEquipmentList.collect {|e|
          e.equipmentID.identifier unless e.equipmentID.identifier.empty?
        }.compact,
        li.strLotOperationInfo.stageID.identifier,
        li.strLotOperationInfo.maskLevel,
        li.strLotOperationInfo.department,
        li.strLotOperationInfo.reworkCount,
        li.strLotOrderInfo.customerCode,
        li.strLotBasicInfo.totalWaferCount, nil, nil
      )
      if params[:select] || params[:wafers]   # TODO: remove
        wanted_wafers = params[:select].instance_of?(Array) ? params[:select] : nil
        n = params[:select].instance_of?(Array) ? params[:select].size : params[:select]
        lot = li.strLotBasicInfo.lotID.identifier
        wafers = []
        li.strLotWaferAttributes.each {|w|
          next if wanted_wafers && !wanted_wafers.include?(w.aliasWaferName) && !wanted_wafers.include?(w.slotNumber)
          wafers << Wafer.new(w.waferID.identifier, w.slotNumber, w.aliasWaferName, lot)
          break if n && wafers.size == n
        }
        if wafers.empty?
          # scrapped lot in carrier
          liraw.strWaferMapInCassetteInfo.each {|w|
            next if wanted_wafers && !wanted_wafers.include?(w.aliasWaferName) && !wanted_wafers.include?(w.slotNumber)
            next if w.lotID.identifier != lot
            wafers << Wafer.new(w.waferID.identifier, w.slotNumber, w.aliasWaferName, lot)
            break if n && wafers.size == n
          }
        end
        l.wafers = wafers
      end
      if params[:backup]
        b = li.strLotBackupInfo
        d = b.strLotBackupSourceDataSeq.last
        srcdata = d.nil? ? nil : LotBackupOpData.new(d.backupState, d.strBackupAddress.serverName,
          d.strBackupProcess.entryRouteID, d.strBackupProcess.entryOperationNumber)
        d = b.strLotBackupDestDataSeq.last
        dstdata = d.nil? ? nil : LotBackupOpData.new(d.backupState, d.strBackupAddress.serverName,
          d.strBackupProcess.entryRouteID, d.strBackupProcess.entryOperationNumber)
        l.backup = LotBackupInfo.new(b.backupProcessingFlag, b.currentLocationFlag, b.transferFlag,
          b.strBornSiteAddress.serverName, srcdata, dstdata)
      end
      l
    }
  end

  # return Array [LotInfo] per lot, including Wafer struct if wafers: true
  def lots_info(lots, params={})
    res = lots_info_raw(lots, params) || return
    return _extract_lots_info(res, params)
  end

  # return LotInfo or pptLotInfoInqResult_struct
  def lot_info(lot, params={})
    res = lots_info(lot, params) || return
    return lot.instance_of?(Array) ? res : res.first
  end

  # call TxLotInfo.., return result struct
  def lots_info_raw(lots, params={})
    lotIDs = _as_objectIDs(lots)
    if params[:flags] == :all
      flags = Array.new(15, true)
    else
      waferinfo = !!params[:wafers] || !!params[:select]
      flags = [
        true,
        !!params[:ctrluse], # strLotControlUseInfo, used in MM_Eqp_UnloadIB only
        false,              # strLotFlowBatchInfo
        false,              # lotNoteFlagInfoFlag
        true,
        true,
        true,
        true,
        !!params[:rgs],     # strLotRecipeInfo, used in MM_Lot_UpcomingInfoForEqp only
        true,
        false,              # lotWipOperationInfoFlag
        waferinfo,          # strLotWaferAttributes,
        waferinfo,          # strLotListInCassetteInfo
        waferinfo,          # strWaferMapInCassetteInfo
        !!params[:backup]   # strLotBackupInfo
      ]
    end
    #
    # AsmView is R12, __160 is backported to R15
    res = nil
    retries = params[:retries] || 2  # retry lots_info on error 1468
    loop {
      if @release < 14  # AsmView is R12, __160 is backported to R15
        res = @manager.TxLotInfoInq__120(@_user, lotIDs, *flags)
      else
        res = @manager.TxLotInfoInq__160(@_user, lotIDs, *flags)
      end
      case rc(res)
      when 0;    break
      when 1211; break  # strange error 1211 for parent lots in different carriers
      when 1468
        retries -= 1
        return if retries < 0
        $log.info 'lots_info failed due to a known SiView bug, retrying'
      else
        $log.warn "error calling lots_info_raw for #{lots}"
        return
      end
    }
    return res
  end

  # extract lotLabel from lotInfo (R15 customized), TODO: remove
  def lot_label(lot)
    # liraw = lots_info_raw(lot) || return
    # extract_si(liraw.strLotInfo.first.siInfo).lotLabel
    ret = user_data(:lot, lot)['LOT_LABEL']
    return ret.nil? ? '' : ret  # for compatibility, TODO: remove
  end

  # return array of not emptied or scapped lots or nil on error
  def lot_family(lot, params={})
    lotID = lot.instance_of?(String) ? oid(lot) : lot
    res = @manager.TxLotFamilyInq(@_user, lotID)
    return if rc(res) != 0
    return res.strLotListAttributes if params[:raw]   # note: does not contain carrierIDs
    #
    ret = []
    res.strLotListAttributes.each {|a|
      fstate = a.strLotStatusList.find {|e| e.stateName == 'Lot Finished State'}
      ret << a.lotID.identifier unless ['EMPTIED', 'SCRAPPED'].member?(fstate.stateValue)
    }
    return ret.sort
  end

  # change use state of a control lot, state is on of WaitUse, InUse, WaitRecycle, InRecycle,  return 0 on success
  def lot_ctrl_status_change(lot, state, params={})
    lotID = lot.instance_of?(String) ? oid(lot) : lot
    count = params[:count]
    memo = params[:memo] || ''
    res = @manager.TxLotCtrlStatusChangeReq(@_user, lotID, state, count, memo)
    return rc(res)
  end

  # merge parent lot with child lot, return 0 on success
  def lot_merge(lot, child, params={})
    lotID = lot.instance_of?(String) ? oid(lot) : lot
    childID = child.instance_of?(String) ? oid(child) : child
    $log.info "lot_merge #{lotID.identifier.inspect}, #{childID.identifier.inspect}"
    res = @manager.TxMergeWaferLotReq(@_user, lotID, childID, params[:memo] || '')
    return rc(res)
  end

  # merge parent lot with child lot, return 0 on success
  def lot_merge_notonroute(lot, child, params={})
    lotID = lot.instance_of?(String) ? oid(lot) : lot
    childID = child.instance_of?(String) ? oid(child) : child
    $log.info "lot_merge_notonroute #{lotID.identifier.inspect}, #{childID.identifier.inspect}"
    res = @manager.TxMergeWaferLotNotOnRouteReq(@_user, lotID, childID, params[:memo] || '')
    return rc(res)
  end

  # split the lot, see lot_info(select: xx) for split wafer selection
  #   (by number of wafers (integer), wafer id (array of strings) or slot (array of integers))
  #   mergeOp is empty by default, pass mergeop: nil to merge at the current op TODO: pass :current ?
  #
  # return child lot id or nil
  def lot_split(lot, splitwafers, params={})
    mergeOpNo = params.has_key?(:mergeop) ? params[:mergeop] : ''
    mrte = params[:mergeroute] || ''
    memo = params[:memo] || ''
    if params.has_key?(:split_route)
      splitroute = params[:split_route]
      retop = params[:retop] || mergeOpNo
      to_subroute = true
    else
      splitroute = ''
      retop = ''
      to_subroute = false
    end
    waferIDs = nil
    if splitwafers.instance_of?(Array)  # array of slots, wafer strings or waferIDs
      waferIDs = oids(splitwafers) if splitwafers.first.instance_of?(String)
    end
    lotID = lot.instance_of?(String) ? oid(lot) : lot
    $log.info "lot_split #{lotID.identifier.inspect}, #{splitwafers.inspect}#{(', ' + params.inspect) unless params.empty?}"
    onhold = params[:onhold]  # split an ONHOLD lot
    if waferIDs.nil? || mergeOpNo.nil? || mrte.nil? || onhold == :ondemand
      liraw = lots_info_raw(lotID, wafers: true) || return
      strLotInfo = liraw.strLotInfo.first
      lotID = strLotInfo.strLotBasicInfo.lotID
      onhold = (strLotInfo.strLotBasicInfo.lotStatus == 'ONHOLD') if onhold == :ondemand
      if mergeOpNo.nil?
        # merge at current op if mergeOpNo is set to nil
        mergeOpNo = strLotInfo.strLotOperationInfo.operationNumber
        mrte = strLotInfo.strLotOperationInfo.routeID.identifier
      elsif mrte.empty?
        mrte = strLotInfo.strLotOperationInfo.routeID.identifier
      end
      if splitwafers.instance_of?(Array)
        wantedwafers = splitwafers
        n = splitwafers.size
      else
        wantedwafers = nil
        n = splitwafers
      end
      waferIDs = []
      strLotInfo.strLotWaferAttributes.each {|w|
        next if wantedwafers && !wantedwafers.include?(w.slotNumber)
        # useful?
        # next if wantedwafers && !wantedwafers.include?(w.waferID.identifier)
        # next if wantedwafers && !wantedwafers.include?(w.aliasWaferName)
        waferIDs << w.waferID
        break if waferIDs.size == n
      }
      ($log.warn '  not enough wafers found'; return) if waferIDs.size != n
    end
    mflag = !mergeOpNo.empty?
    $log.info {"  merge route #{mrte.inspect}, opNo #{mergeOpNo.inspect}"} if mflag
    if onhold
      if (params[:inheritholds] != false) || params[:release]
        hreqs = lot_hold_list(lotID, raw: true).collect {|h|
          @jcscode.pptHoldReq_struct.new(h.holdType, h.reasonCodeID, h.userID, h.responsibleOperationMark,
            h.responsibleRouteID, h.responsibleOperationNumber, h.relatedLotID, h.claimMemo, any)
        }
      else
        hreqs = []
      end
      if params[:release]
        reason = params[:reason] || 'ALL'
        res = @manager.TxSplitWaferLotWithHoldReleaseReq(@_user, lotID, waferIDs,
          mflag, oid(mrte), mergeOpNo, to_subroute, oid(splitroute), retop, memo, oid(reason), hreqs)
      else
        $log.info '  inherit holds' if params[:inheritholds] != false
        res = @manager.TxSplitWaferLotWithoutHoldReleaseReq(@_user, lotID, waferIDs,
          mflag, oid(mrte), mergeOpNo, to_subroute, oid(splitroute), retop, memo, hreqs)
      end
    else
      res = @manager.TxSplitWaferLotReq(@_user, lotID, waferIDs,
        mflag, oid(mrte), mergeOpNo, to_subroute, oid(splitroute), retop, memo)
    end
    return if rc(res) != 0
    childID = res.childLotID
    $log.info "  created child lot #{childID.identifier} with #{waferIDs.size} wafers"
    return params[:raw] ? childID : childID.identifier
  end

  # split a lot that is not on a route (with or without waferids), return child lot on success
  # splitwafers is either a number or an array of slots (e.g. in wbtqtest)
  def lot_split_notonroute(lot, splitwafers, params={})
    lotID = lot.instance_of?(String) ? oid(lot) : lot
    $log.info "lot_split_notonroute #{lotID.identifier.inspect}, #{splitwafers}#{(', ' + params) unless params.empty?}"
    memo = params[:memo] || ''
    waferIDs = []
    liraw = lots_info_raw(lotID, wafers: true).strLotInfo.first
    liraw.strLotWaferAttributes.each {|w|
      next if w.waferID.identifier.empty?   # TODO: ??
      next if splitwafers.instance_of?(Array) && !splitwafers.include?(w.slotNumber)
      waferIDs << w.waferID
      break if waferIDs.size == splitwafers
    }
    # in case no waferids were assigned strLotWaferAttributes is empty
    nrequested = splitwafers.instance_of?(Array) ? splitwafers.size : splitwafers
    nwafers = waferIDs.size
    ($log.warn '  not enough wafers found'; return) if nwafers  < nrequested
    #
    res = @manager.TxSplitWaferLotNotOnRouteReq(@_user, lotID,
      liraw.strLotBasicInfo.totalWaferCount - nwafers, nwafers, waferIDs, memo)
    return if rc(res) != 0
    childID = res.childLotID
    $log.info "  created child lot #{childID.identifier} with #{nwafers} wafers"
    return params[:raw] ? childID : childID.identifier
  end

  # check if the lot can be deleted, return 0 on success  UNUSED
  def lot_deletion_check(lot)
    lotID = lot.instance_of?(String) ? oid(lot) : lot
    res = @manager.TxLotDeletionCheckReq(@_user, lotID)
    return rc(res)
  end

  # delete lots (array or string), return 0 on success
  def lot_delete(lots, params={})
    memo = params[:memo] || ''
    lotIDs = _as_objectIDs(lots)
    $log.info "lot_delete #{lotIDs.collect {|e| e.identifier}}"
    #
    res = @manager.TxLotDeleteReq(@_user, lotIDs, memo)
    res.strLotDeleteResult.each {|l|
      lres = l.strResult
      $log.warn("  #{l.lotID.identifier} #{lres.returnCode}: #{lres.messageText}") if lres.returnCode != '0'
    }
    return rc(res)
  end

  # return 0 on success
  def lot_bankin(lots, params={})
    lotIDs = _as_objectIDs(lots)
    $log.info "lot_bankin #{lotIDs.collect {|e| e.identifier}}"
    res = @manager.TxBankInReq(@_user, lotIDs, params[:memo] || '')
    return rc(res)
  end

  # return 0 on success
  def lot_bankin_cancel(lot, params={})
    lotID = lot.instance_of?(String) ? oid(lot) : lot
    $log.info "lot_bankin_cancel #{lotID.identifier.inspect}"
    res = @manager.TxBankInCancelReq(@_user, lotID, params[:memo] || '')
    return rc(res)
  end

  # return 0 on success
  def lot_bank_move(lots, bank, params={})
    lotIDs = _as_objectIDs(lots)
    bankID = bank.instance_of?(String) ? oid(bank) : bank
    $log.info "lot_bank_move #{lotIDs.collect {|e| e.identifier}}, #{bankID.identifier.inspect}"
    res = @manager.TxBankMoveReq(@_user, lotIDs, bankID, params[:memo] || '')
    return rc(res)
  end

  # return 0 on success
  def lot_nonprobankin(lot, bank, params={})
    lotID = lot.instance_of?(String) ? oid(lot) : lot
    bankID = bank.instance_of?(String) ? oid(bank) : bank
    $log.info "lot_nonprobankin #{lotID.identifier.inspect}, #{bankID.identifier.inspect}"
    res = @manager.TxNonProBankInReq(@_user, lotID, bankID, params[:memo] || '')
    return rc(res)
  end

  # return 0 on success
  def lot_nonprobankout(lot, params={})
    lotID = lot.instance_of?(String) ? oid(lot) : lot
    $log.info "lot_nonprobankout #{lotID.identifier.inspect}"
    res = @manager.TxNonProBankOutReq(@_user, lotID, params[:memo] || '')
    return rc(res)
  end

  # ship lot, pass nil to use the current bank, return 0 on success
  def lot_ship(lots, bank, params={})
    lotIDs = _as_objectIDs(lots)
    # $log.info "lot_ship #{lotIDs.collect {|e| e.identifier}}, #{bank.inspect}"
    if bank.nil?
      bankID = lots_info_raw(lotIDs).strLotInfo.first.strLotBasicInfo.bankID
      $log.info "lot_ship #{lotIDs.collect {|e| e.identifier}}, nil (#{bankID.identifier})"
    else
      bankID = bank.instance_of?(String) ? oid(bank) : bank
      $log.info "lot_ship #{lotIDs.collect {|e| e.identifier}}, #{bankID.identifier.inspect}"
    end
    #
    res = @manager.TxShipReq(@_user, lotIDs, bankID, params[:memo] || '')
    return rc(res)
  end

  # ship cancel, bank defaults to '' (ship bank), return 0 on success
  def lot_ship_cancel(lots, params={})
    bank = params[:bank] || ''
    memo = params[:memo] || ''
    lotIDs = _as_objectIDs(lots)
    bankID = bank.instance_of?(String) ? oid(bank) : bank
    $log.info "lot_ship_cancel #{lotIDs.collect {|e| e.identifier}}, bank: #{bankID.identifier.inspect}"
    #
    res = @manager.TxShipCancelReq(@_user, lotIDs, bankID, memo)
    return rc(res)
  end

  # change lot external priority to the new priority (e.g. 2), return 0 on success
  def lot_externalpriority_change(lots, prio, params={})
    lotIDs = _as_objectIDs(lots)
    $log.info "lot_externalpriority_change #{lotIDs.collect {|e| e.identifier}}, #{prio}"
    commitByLotFlag = !!params[:commit_bylot]
    memo = params[:memo] || ''
    prios = lotIDs.collect {|lotID| @jcscode.pptLotExtPrty_struct.new(lotID, prio.to_i, any)}
    #
    res = @manager.TxLotExternalPriorityChangeReq(@_user, prios, commitByLotFlag, memo)
    return rc(res)
  end

  # change the parameters of lots, like sublot type, order number, customer code, return 0 on succes
  def lot_mfgorder_change(lots, params)
    lotIDs = _as_objectIDs(lots)
    $log.info "lot_mfgorder_change #{lotIDs.collect {|e| e.identifier}}, #{params}"
    customer = params[:customer] || ''
    order = params[:order] || ''
    owner = params[:owner] || ''
    epriority = params[:epriority] || ''
    sublottype = params[:sublottype] || ''
    memo = params[:memo] || ''
    res = @jcscode.pptLotMfgOrderChangeReqResult_structHolder.new
    attrs = lotIDs.collect {|lotID|
      @jcscode.pptChangedLotAttributes_struct.new(lotID, customer, order, oid(owner), epriority, memo, sublottype, any)
    }
    #
    @manager.TxLotMfgOrderChangeReq(res, @_user, attrs, memo)
    return rc(res.value)
  end

  # change sublottype (and automatically lot type if necessary) of a lot or list of lots, return 0 on success
  def sublottype_change(lots, sublottype, params={})
    lotIDs = _as_objectIDs(lots)
    $log.info "sublottype_change #{lotIDs.collect {|e| e.identifier}}, #{sublottype.inspect}"
    res = @jcscode.pptCS_SubLotTypeChangeReqResult_structHolder.new(@jcscode.pptCS_SubLotTypeChangeReqResult_struct.new)
    chparams = lotIDs.collect {|lotID|
      @jcscode.pptCS_ChangedSubLotType_struct.new(lotID, (params[:lotcomment] || ''), sublottype, any)
    }
    #
    @manager.CS_TxSubLotTypeChangeReq(res, @_user, chparams, params[:memo] || '')
    return rc(res.value, res.value.strChangeLotReturn)
  end

  # return list of lot comments or nil on error
  def lot_comments(lot, params={})
    lotID = lot.instance_of?(String) ? oid(lot) : lot
    res = @manager.TxLotCommentInfoInq(@_user, lotID)
    return if rc(res) != 0
    res.strLotCommentInfo.collect {|n|
      LotNote.new(lot, n.lotCommentTitle, n.lotCommentDescription, n.reportUserID.identifier, n.reportTimeStamp)
    }
  end

  # return list of lot notes or nil on error
  def lot_notes(lot, params={})
    lotID = lot.instance_of?(String) ? oid(lot) : lot
    res = @manager.TxLotNoteInfoInq(@_user, lotID)
    return if rc(res) != 0
    res.strLotNoteInfo.collect {|n|
      LotNote.new(lot, n.lotNoteTitle, n.lotNoteDescription, n.reportUserID.identifier, n.reportTimeStamp)
    }
  end

  # register a lot note, return 0 on success
  def lot_note_register(lot, title, desc, params={})
    lotID = lot.instance_of?(String) ? oid(lot) : lot
    $log.info "lot_note_register #{lotID.identifier.inspect}, #{title.inspect}, #{desc.inspect}"
    u = params[:user] ? user_struct(params[:user], params[:password] || '') : @_user
    #
    res = @manager.TxLotNoteInfoRegistReq(u, lotID, title, desc)
    return rc(res)
  end

  # call CS_TxLotUpcomingOperationInfoForEquipmentInq (used by EI and APC), used in 1 SiView regression test
  # return result struct or nil on error
  def lot_upcoming_info(lot, opNo, eqp, params={})
    lotID = lot.instance_of?(String) ? oid(lot) : lot
    eqpID = eqp.instance_of?(String) ? oid(eqp) : eqp
    routeID = oid(params[:route] || '')
    #
    res = @manager.CS_TxLotUpcomingOperationInfoForEquipmentInq(@_user, lotID, routeID, opNo, eqpID)
    return if rc(res) != 0
    return res
  end

  # returns lot families that can be deleted, UNUSED
  def lot_family_for_deletion(sublottype)
    res = @manager.TxLotFamilyListForDeletionInq(@_user, oid(sublottype))
    return if rc(res) != 0
    return res.lotFamilyIDs.collect {|l| l.identifier}
  end

end
