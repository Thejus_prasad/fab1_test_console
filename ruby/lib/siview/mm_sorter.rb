=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.
=end


class SiView::MM

  # return list of sorter jobs
  def sj_list(params={})
    eqp = params[:eqp] || '%'
    eqpID = eqp.instance_of?(String) ? oid(eqp) : eqp
    carrier = params[:carrier] || ''
    carrierID = carrier.instance_of?(String) ? oid(carrier) : carrier
    lot = params[:lot] || ''
    lotID = lot.instance_of?(String) ? oid(lot) : lot
    user = params[:user] || ''
    sjid = params[:sjid] || oid('')
    sjID = sjid.instance_of?(String) ? oid(sjid) : sjid
    #
    res = @manager.TxSortJobListInq(@_user, eqpID, carrierID, lotID, oid(user), sjID)
    return nil if rc(res) != 0
    return res.strSortJobListAttributesSequence
  end

  # return sorter job status
  def sj_status(sjid, params={})
    sjid = oid(sjid) if sjid.instance_of?(String)
    res = @manager.TxSortJobStatusInq(@_user, sjid)
    return nil if rc(res) != 0
    return res
  end

  # change sorter or component job status to Completed, Error, Executing or Xfer (component)
  #
  # return 0 on success
  def sj_status_change(eqpid, sjid, status, params={})
    eqpid = oid(eqpid) if eqpid.instance_of?(String)
    sjid = oid(sjid) if sjid.instance_of?(String)
    pg = params[:pg] || ''
    jobtype = params[:jobtype] || 'SorterJob' # SorterJob or ComponentJob
    memo = params[:memo] || ''
    $log.info "sj_status_change #{eqpid.identifier.inspect}, #{sjid.identifier.inspect}, #{status.inspect}"
    $log.debug {"  #{params.inspect}"}
    #
    res = @manager.TxSortJobStatusChangeRpt(@_user, eqpid, oid(pg), sjid, jobtype, status, memo)
    return rc(res)
  end

  # cancel sj, sj must be in status Completed or Error, return 0 on success
  def sj_cancel(sjid, params={})
    sjid = oid(sjid) if sjid.instance_of?(String)
    $log.info "sj_cancel #{sjid.identifier.inspect}"
    components = params[:components] || []
    notify = (params[:notify] != false)
    memo = params[:memo] || ''
    #
    res = @manager.TxSortJobCancelReq(@_user, sjid, oids(components), notify, memo)
    return rc(res)
  end

  # return pptSorterComponentJobListAttributes_struct for sj_create, :slots is used by create_wildcard_verify
  def _create_sj_joblist(eqpid, pg, srcport, tgtport, lots, tgt_carriers, params={})
    eqpid = oid(eqpid) if eqpid.instance_of?(String)
    action = params[:action] || 'AutoSorting'
    direction = params[:direction] || ''
    tgt_carriers = [tgt_carriers] if tgt_carriers.instance_of?(String)
    tgt_managed = true
    # customization removed in C7.18, TODO: remove
    #select = params[:select] || 25  # for MM_SorterJobs to test customized AutoSplit
    joblist = []
    lots_info_raw(lots, wafers: true).strLotInfo.each_with_index {|liraw, i|
      src_managed = true
      srcportID = oid(srcport)
      tgtcarrierID = oid(tgt_carriers[i])
      tgtportID = oid(tgtport)
      tgtslots = params[:slots]
      slotmap = []
      liraw.strLotWaferAttributes.each {|wattr|
        tgtslot = tgtslots.nil? ? wattr.slotNumber : (tgtslots += 1)
        slotmap << @jcscode.pptWaferSorterSlotMap_struct.new(
          pg, eqpid, action, '', direction, wattr.waferID, liraw.strLotBasicInfo.lotID,
          tgtcarrierID, tgtportID, tgt_managed, tgtslot,
          wattr.cassetteID, srcportID, src_managed, wattr.slotNumber, @_user.userID, '', '', '', '', any)
        #break if slotmap.size == select  # TODO: remove
      }
      joblist << @jcscode.pptSorterComponentJobListAttributes_struct.new(oid(''), '',
        liraw.strLotLocationInfo.cassetteID, srcportID, '', oid(''), oid(''),
        tgtcarrierID, tgtportID, '', oid(''), oid(''), '', oid(''), slotmap.to_java(@jcscode.pptWaferSorterSlotMap_struct), any)
    }
    return joblist
  end

  # create a sorter job, return sjid on success or nil
  def sj_create(sorter, srcport, tgtport, lots, tgt_carriers, params={})
    $log.info "sj_create '#{sorter}', '#{srcport}', '#{tgtport}', #{lots.inspect}, #{tgt_carriers.inspect}, #{params}"
    memo = params[:memo] || ''
    waferIDread = (params[:waferIDread] != false)
    pg = (srcport == '*') ? '*' : eqp_info(sorter).ports.find {|p| p.port == srcport}.pg
    return unless pg
    joblist = _create_sj_joblist(sorter, pg, srcport, tgtport, lots, tgt_carriers, params)
    #
    res = @manager.TxSortJobCreateReq(@_user, joblist, oid(sorter), pg, waferIDread, memo)
    return if rc(res) != 0
    return res.sorterJobID
  end

  # update unspecified sorter jobs with sorter, port and pg information, return 0 on success
  def sj_update(sjid, sorter, srcport, tgtport, pg)
    sjid = oid(sjid) if sjid.instance_of?(String)
    $log.info "sj_update #{sjid.identifier.inspect}, #{sorter.inspect}, #{srcport.inspect}, #{tgtport.inspect}, #{pg.inspect}"
    #
    sjlist = @manager.TxSortJobListInq(@_user, oid('%'), oid(''), oid(''), oid(''), sjid)
    sjl = sjlist.strSortJobListAttributesSequence
    ($log.warn 'wrong sorter job list'; return) if sjl.size != 1
    attrs = sjl.first.strSorterComponentJobListAttributesSeq
    attrs.each {|cj|
      cj.originalPortID = oid(srcport)
      cj.destinationPortID = oid(tgtport)
      cj.strWaferSorterSlotMapSequence.each {|slot|
        slot.equipmentID = oid(sorter)
        slot.originalPortID = oid(srcport)
        slot.destinationPortID = oid(tgtport)
      }
    }
    #
    res = @manager.CS_TxSortJobUpdateReq(@_user, sjl.first.sorterJobID, attrs, oid(sorter), pg, '')
    return rc(res)
  end

  # return lot ID on success
  def AMD_WaferSorterAutoSplitReq(lot, splitwafers, params={})
    $log.info "AMD_WaferSorterAutoSplitReq #{lot.inspect}, #{splitwafers.inspect}, #{params.inspect}"
    wids = lot_info(lot, select: splitwafers).wafers.collect {|w| oid(w.wafer)}
    ($log.warn 'no valid wafers found'; return) unless wids && wids.size > 0
    #
    res = @manager.AMD_TxWaferSorterAutoSplitReq(@_user, oid(lot), wids, params[:memo] || '')
    return if rc(res) != 0
    return res.childLotID.identifier
  end

  # request a sort job action, return 0 on success (used in claim_sj_execute only)
  def sorter_on_eqp_req(eqp, sjid, slotmap, params={})
    eqpid = eqp.instance_of?(String) ? oid(eqp) : eqp
    $log.info "sorter_on_eqp_req #{eqpid.identifier.inspect}, #{sjid.inspect}"
    $log.debug {"  #{params.inspect}"}
    memo = params[:memo] || ''
    action = 'AutoSorting'
    sorter_status = 'SP_Sorter_Requested'
    direction = 'MM'
    slotmap.each {|s|
      s.actionCode = action
      s.direction = direction
      s.requestUserID = @_user.userID
      s.sorterStatus = sorter_status
      s.replyTime = siview_timestamp
    }
    #
    res = @manager.TxWaferSorterOnEqpReq(@_user, action, eqpid, slotmap, slotmap.first.portGroup, sjid, memo)
    return rc(res)
  end

  # report a sort job action result to SiView, return 0 on success (used in claim_sj_execute only)
  def sorter_on_eqp_rpt(eqp, slotmap, params={})
    eqpid = eqp.instance_of?(String) ? oid(eqp) : eqp
    $log.info "sorter_on_eqp_rpt #{eqpid.identifier.inspect}"
    $log.debug {"  #{params.inspect}"}
    rc_tcs = 0
    action = 'AutoSorting'
    sorter_status = 'SP_Sorter_Succeeded'
    direction = 'TCS'
    slotmap.each {|s|
      s.actionCode = action
      s.direction = direction
      s.requestUserID = @_user.userID
      s.sorterStatus = sorter_status
      s.replyTime = siview_timestamp
    }
    #
    res = @manager.TxWaferSorterOnEqpRpt(@_user, eqpid, action, slotmap, rc_tcs)
    return rc(res)
  end

end
