=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.
=end


class SiView::MM
  WhatNext = Struct.new(:carrier, :eqp, :xfer_status, :lot, :route, :op, :product,
    :lrecipe, :mrecipe, :rgs, :mltype, :inhibits)

  # call RTD, parameters are like "requestingEntity=EngineNoWIP"; return strDispatchResult struct or nil
  def rtd_dispatch_list(station, params={})
    rule = params[:rule] || 'WhatNextLotList'  # or WhereNextInterbay, WhereNextEquipment or ARDD
    parameters = params[:parameters] || []
    parameters = parameters.split('|') if parameters.instance_of?(String)  # that is what the OPI does
    $log.info "rtd_dispatch_list #{station.inspect}, rule: #{rule.inspect}, parameters: #{parameters}"
    #
    res = @manager.TxRTDDispatchListInq(@_user, rule, station, parameters)
    return nil if rc(res) != 0
    return res.strDispatchResult
  end


  # equipment's what next list, optionally filtered to get immediately usable carriers/lots
  #
  # return lot list or raw result, nil on error
  def what_next(eqp, params={})
    # SiViewConstants.SP_DP_SelectCriteria_All
    criteria = params[:criteria] || 'SALL'  # SALL, SAVL, Auto3, SHLD
    #
    res = @manager.TxWhatNextLotListInq__140(@_user, oid(eqp), criteria)
    return nil if rc(res) != 0
    return res if params[:raw]
    return res.strWhatNextAttributes.collect {|a|
      mltype = a.multiLotType
      next if params[:singlelot] && mltype != 'SL-SR'
      next if params[:reticle] && a.reticleExistFlag
      inhibits = a.entityInhibitions.collect {|inh|
        Inhibit.new(nil, inh.claimedTimeStamp, inh.startTimeStamp, inh.endTimeStamp,
          inh.entities.collect {|e| InhibitEntity.new(e.className, e.objectID.identifier, e.attrib)},
          inh.reasonCode, inh.ownerID.identifier, inh.memo, nil)
      }
      WhatNext.new(a.cassetteID.identifier, a.equipmentID.identifier, a.transferStatus, a.lotID.identifier,
        a.routeID.identifier, a.operationID.identifier, a.productID.identifier, a.logicalRecipeID.identifier,
        a.machineRecipeID.identifier, a.reticleGroupIDs.collect {|rg| rg.identifier}, mltype, inhibits)
    }.compact
  end

  # return lot list or raw result, nil on error, UNUSED
  def what_next_standby(bank, params={})
    res = @manager.TxWhatNextStandbyLotInq(@_user, oid(bank))
    return nil if rc(res) != 0
    return res if params[:raw]
    return res.strLotListAttributes
  end

  def where_next_interbay(lot, params={})
    carrier = params[:carrier] || lot_info(lot).carrier
    res = @manager.TxWhereNextInterBayInq(@_user, oid(lot), oid(carrier))
    return nil if rc(res) != 0
    return res.strWhereNextEqpStatus
  end

  # returns list of equipment's reticles available to be retrieved from equipment buffer; NOT USED
  def what_reticles(eqp, params={})
    res = @manager.TxWhatReticleRetrieveInq(@_user, oid(eqp))
    return nil if rc(res) != 0
    return res.strDispatchResult
  end

  # returns the reticle pod destination (equipment, bare reticle stocker, reticle pod stocker) ; NOT USED
  def where_next_reticlepod(reticle_pod, params={})
    res = @manager.TxWhereNextForReticlePodInq(@_user, oid(reticle_pod))
    return nil if rc(res) != 0
    return res.strNextMachineForReticlePod
  end

  # RTD returns reticle dispatch job (RDJ) list; NOT USED
  def what_reticle_actionlist(eqp, params={})
    res = @manager.TxWhatReticleActionListInq(@_user, eqp)
    return nil if rc(res) != 0
    return res.strDispatchResult
  end

  # RTD returns reticle dispatch job (RDJ) list; NOT USED
  def what_reticle_actionlist_req(eqp, params={})
    res = @manager.TxWhatReticleActionListReq(@_user, eqp, params[:memo] || '')
    return nil if rc(res) != 0
    return res.strDispatchResult
  end

end
