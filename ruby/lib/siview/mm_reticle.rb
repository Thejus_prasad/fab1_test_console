=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.
=end

class SiView::MM

  # return an array reticles or pptReticleListInqResult_struct or nil on error
  def reticle_list(params={})
    eqp = params[:eqp] ||''
    lot = params[:lot] || ''
    reticle = params[:reticle] || ''
    reticleGroup = params[:rg] || ''
    reticlePartNumber = params[:reticlePartNumber] || ''
    reticleStatus = params[:status] || ''
    maxRetrieveCount = params[:count] || 32767
    reticle_struct = @jcscode.pptReticleListInqInParm_struct.new(oid(reticleGroup), reticlePartNumber,
      oid(reticle), oid(lot), oid(eqp), reticleStatus, maxRetrieveCount, '', 'All', any)
    #
    if params[:customized]  # rarely used, OPI only(?)
      res = @manager.AMD_TxAllReticleListInq(@_user, reticle_struct, [@jcscode.pptAMD_KeyValue_struct.new])
    else
      res = @manager.TxReticleListInq(@_user, reticle_struct)
    end
    return nil if rc(res) != 0
    return res.strFoundReticle
  end

  # return pptReticleStatusInqResult_struct
  def reticle_status(reticle, params={})
    reticle = oid(reticle) if reticle.instance_of?(String)
    res = @manager.TxReticleStatusInq__090(@_user, reticle)
    return nil if rc(res) != 0
    return res
  end

  # return 0 on success
  def reticle_status_change(reticle, status, params={})
    reticle = oid(reticle) if reticle.instance_of?(String)
    memo = params[:memo] || ''
    $log.info "reticle_status_change #{reticle.identifier.inspect}, #{status.inspect}"
    if params[:check]
      rs = reticle_status(reticle)
      ($log.debug {'  no change'}; return 0) if rs.reticleStatusInfo.reticleStatus == status
    end
    #
    res = @manager.TxReticleStatusChangeRpt(@_user, reticle, status, memo)
    return rc(res)
  end

  # return 0 on success
  def reticle_xfer_status_change(reticle, status, params={})
    eqp = params[:eqp] || ''
    stocker = params[:stocker] || ''
    xfers = [@jcscode.pptXferReticle_struct.new(oid(reticle), status, '', any)]
    $log.info "reticle_xfer_status_change #{reticle.inspect}, #{status.inspect}, #{params}"
    #
    res = @manager.TxReticleXferStatusChangeRpt(@_user, oid(stocker), oid(eqp), xfers)
    return rc(res)
  end

  # direction is one of "Just-Out" or "Just-In", return 0 on success
  def reticle_justinout(reticle, pod, slot, direction, params={})
    direction = 'Just-Out' if direction.downcase.index('out')
    direction = 'Just-In' if direction.downcase.index('in')
    $log.info "reticle_justinout #{reticle.inspect}, #{pod.inspect}, #{slot}, #{direction.inspect}"
    inparms = [@jcscode.pptMoveReticles_struct.new(oid(reticle), slot, any)]
    #
    res = @manager.TxReticleJustInOutRpt(@_user, direction, oid(pod), inparms, params[:memo] || '')
    return rc(res)
  end

  # move reticle between pods, default slot is 1, return 0 on success
  def reticle_sort(reticle, tgt_pod, params={})
    memo = params[:memo] || ''
    srcpodID = reticle_status(reticle).reticleStatusInfo.reticlePodID
    src_slot = params[:src_slot] || 1
    tgt_slot = params[:tgt_slot] || src_slot
    rsinfos = [@jcscode.pptReticleSortInfo_struct.new(oid(reticle), oid(tgt_pod), tgt_slot, srcpodID, src_slot, any)]
    $log.info "reticle_sort #{reticle.inspect}, #{tgt_pod.inspect} (from #{srcpodID.identifier})"
    #
    res = @manager.TxReticleSortRpt(@_user, rsinfos, memo)
    return rc(res)
  end

  # Returns a list of reticle groups for a lot reserved at a photo layer on a tool
  #   used in MM_Eqp_LotReticleGroup only
  def AMD_lot_reticlegrouplist(lot, eqp, params={})
    res = @manager.AMD_TxLotReticleGroupIDListInq(@_user, oid(lot), oid(eqp))
    return nil if rc(res) != 0
    return res.rtclGrpIDs.collect {|e| e.identifier}
  end

  # return an array of reticle pods or pptReticlePodListInqResult_struct or nil on error
  def reticle_pod_list(params={})
    eqp = params[:eqp] || ''
    stocker = params[:stocker] || ''
    reticle = params[:reticle] || ''
    rg = params[:rg] || ''
    part = params[:reticlePartNumber] || ''
    pod = params[:pod] || ''
    cat = params[:reticlePodCategory] || ''
    status = params[:status] || ''
    empty = !!params[:empty]
    count = params[:count] || 32767
    #
    tx = params[:customized] ? :AMD_TxReticlePodListInq : :TxReticlePodListInq
    res = @manager.send(tx, @_user, oid(pod), cat, status, oid(eqp), oid(stocker), empty, oid(reticle), oid(rg), part, count)
    return nil if rc(res) != 0
    return res.strReticlePodListInfo
  end

  # pod can contain wildcards
  def reticle_pod_status(pod)
    res = @manager.TxReticlePodStatusInq__090(@_user, oid(pod))
    return nil if rc(res) != 0
    return res
  end

  # return 0 on success
  def reticle_pod_multistatus_change(pods, state, params={})
    pods = [pods] if pods.instance_of?(String)
    substate = params[:substate] || ''
    memo = params[:memo] || ''
    $log.info "reticle_pod_multistatus_change #{pods.inspect}, #{state.inspect}, #{params}"
    if params[:check] && pods.size == 1
      podstatus = reticle_pod_status(pods.first).reticlePodStatusInfo.reticlePodStatus
      ($log.debug "  reticle pod is already #{state}, no change"; return 0) if podstatus == state
    end
    #
    if params[:core]
      $log.info '  TxReticlePodMultiStatusChangeRpt'
      res = @manager.TxReticlePodMultiStatusChangeRpt(@_user, state, oids(pods), memo)
    else
      $log.info '  AMD_TxReticlePodMultiStatusChangeRpt'
      res = @manager.AMD_TxReticlePodMultiStatusChangeRpt(@_user, state, substate, oids(pods), memo)
    end
    return rc(res)
  end

  # return 0 on success
  def reticle_pod_xfer_status_change(pod, status, params={})
    $log.info "reticle_pod_xfer_status_change #{pod.inspect}, #{status.inspect}, #{params}"
    eqp = params[:eqp] || ''
    stocker = params[:stocker] || ''
    struct = [@jcscode.pptXferReticlePod_struct.new(oid(pod), status, '', any)]
    #
    res = @manager.TxReticlePodXferStatusChangeRpt(@_user, oid(stocker), oid(eqp), struct)
    return rc(res)
  end

  # return 0 on success
  def reticle_pod_xfer_status_change_new(pod, state, machine, params={})
    $log.info "reticle_pod_xfer_status_change_new #{pod.inspect}, #{state.inspect}, #{machine.inspect}, #{params}"
    port = params[:port] || ''
    manualin = !!params[:manualin]
    memo = params[:memo] || ''
    #
    res = @manager.TxRSPXferStatusChangeRpt(@_user, oid(pod), state, manualin, oid(machine), oid(port), memo)
    return rc(res)
  end

  # loads a reticle pod on an equipment load port (only in Off-Line), return 0 on success
  def reticle_pod_offline_load(eqp, port, pod, params={})
    memo = params[:memo] || ''
    $log.info "reticle_pod_offline_load #{eqp.inspect}, #{port.inspect}, #{pod.inspect}"
    #
    res = @manager.TxReticlePodOfflineLoadingReq(@_user, oid(eqp), oid(port), oid(pod), !!params[:force], memo)
    return rc(res)
  end

  # unloads a reticle on an equipmen load port (only in Off-Line), return 0 on success
  def reticle_pod_offline_unload(eqp, port, pod, params={})
    memo = params[:memo] || ''
    $log.info "reticle_pod_offline_unload #{eqp.inspect}, #{port.inspect}, #{pod.inspect}"
    #
    res = @manager.TxReticlePodOfflineUnloadingReq(@_user, oid(eqp), oid(port), oid(pod), !!params[:force], memo)
    return rc(res)
  end

  # store all reticles (or :reticles) into an equipment, return 0 on success
  def reticle_offline_store(eqp, port, pod, reticles, params={})
    memo = params[:memo] || ''
    reticles = [reticles] if reticles.instance_of?(String)
    $log.info "reticle_offline_store #{eqp.inspect}, #{port.inspect}, #{pod.inspect}, #{reticles}"
    store = []
    rps = reticle_pod_status(pod) || return
    rps.reticlePodStatusInfo.strContainedReticleInfo.each {|rinfo|
      if reticles.nil? || reticles.include?(rinfo.reticleID.identifier)
        store << @jcscode.pptReticleStoreResult_struct.new(rinfo.reticleID, rinfo.slotNo, true, any)
      end
    }
    #
    res = @manager.TxReticleOfflineStoreReq(@_user, oid(eqp), oid(port), oid(pod), store, memo)
    return rc(res)
  end

  # retrieve reticles from an equipment (only in Off-Line), return 0 on success
  #   used in MM_Reticle_InOutPod only
  def reticle_offline_retrieve(eqp, port, pod, reticles, params={})
    reticles = reticles.split if reticles.instance_of?(String)
    slot = params[:slot] || 1
    memo = params[:memo] || ''
    $log.info "reticle_offline_retrieve #{eqp.inspect}, #{port.inspect}, #{reticles.inspect}"
    slot -= 1
    rstruct = reticles.collect {|r| @jcscode.pptReticleRetrieveResult_struct.new(oid(r), slot += 1, true, any)}
    #
    res = @manager.TxReticleOfflineRetrieveReq(@_user, oid(eqp), oid(port), oid(pod), rstruct, memo)
    return rc(res)
  end

  # loads a reticle on an equipment/bare reticle stocker load port in Online-Remote, return 0 on success
  #   used in MM_Reticle_InOutPod only
  def reticle_pod_load(pod, params={})
    eqp = params[:eqp] || ''
    port = params[:port] || ''
    brs = params[:brs] || params[:barereticlestocker] || ''
    rsc = params[:resource] || params[:rsc] || ''
    memo = params[:memo] || ''
    $log.info "reticle_pod_load #{pod.inspect}, #{params}"
    #
    res = @manager.TxReticlePodLoadingRpt(@_user, oid(eqp), oid(port), oid(brs), oid(rsc), oid(pod), memo)
    return rc(res)
  end

  # unloads a reticle on an equipment/bare reticle stocker load port in Online-Remote, return 0 on success
  #   used in MM_Reticle_InOutPod only
  def reticle_pod_unload(pod, params={})
    eqp = params[:eqp] || ''
    port = params[:port] || ''
    brs = params[:brs] || params[:barereticlestocker] || ''
    rsc = params[:resource] || params[:rsc] || ''
    memo = params[:memo] || ''
    $log.info "reticle_pod_unload #{pod.inspect}, #{params}"
    #
    res = @manager.TxReticlePodUnloadingRpt(@_user, oid(eqp), oid(port), oid(brs), oid(rsc), oid(pod), memo)
    return rc(res)
  end

  # store :reticles into an equipment/bare reticle stocker, return 0 on success
  #   used in MM_Reticle_InOutPod only
  def reticle_store(eqp, port, pod, reticles, params={})
    reticles = [reticles] if reticles.instance_of?(String)
    $log.info "reticle_store #{eqp.inspect}, #{port.inspect}, #{pod.inspect}, #{reticles}"
    brs = params[:brs] || ''
    rsc = params[:resource] || ''
    memo = params[:memo] || ''
    store = []
    rps = reticle_pod_status(pod) || return
    rps.reticlePodStatusInfo.strContainedReticleInfo.each {|rinfo|
      if reticles.nil? || reticles.include?(rinfo.reticleID.identifier)
        store << @jcscode.pptReticleStoreResult_struct.new(rinfo.reticleID, rinfo.slotNo, true, any)
      end
    }
    #
    res = @manager.TxReticleStoreRpt(@_user, oid(eqp), oid(port), oid(brs), oid(rsc), oid(pod), store, memo)
    return rc(res)
  end

  # retrieve reticles into a pod, return 0 on success
  #   used in MM_Reticle_InOutPod only
  def reticle_retrieve(pod, reticles, params={})
    eqp = params[:eqp] || ''
    port = params[:port] || ''
    brs = params[:brs] || ''
    rsc = params[:resource] || ''
    memo = params[:memo] || ''
    reticles = [reticles] unless reticles.instance_of?(Array)
    $log.info "reticle_retrieve #{pod.inspect}, #{reticles}, #{params}"
    retrieve = reticles.each_with_index.collect {|r, i|
      @jcscode.pptReticleRetrieveResult_struct.new(oid(r), i + 1, true, any)
    }
    #
    res = @manager.TxReticleRetrieveRpt(@_user, oid(eqp), oid(port), oid(brs), oid(rsc), oid(pod), retrieve, memo)
    return rc(res)
  end

  # inventory of reticle tool or bare reticle stocker, return 0 on success
  #   used in MM_Reticle_InOutPod only
  def reticle_inventory_upload(params)
    stocker = params[:stocker] || ''
    eqp = params[:eqp] || ''
    memo = params[:memo] || ''
    $log.info "reticle_inventory_upload #{params}"
    res = @manager.TxReticleInventoryReq__090(@_user, oid(stocker), oid(eqp), memo)
    return rc(res)
  end

  # get reticle list, UNUSED
  def reticleid_list(params={})
    res = @manager.TxReticleIDListInq(@_user)
    return nil if rc(res) != 0
    return res if params[:raw]
    return res.objectIdentifiers.collect {|p| p.identifier}
  end

  # get reticle group list, UNUSED
  def reticle_group_list(params={})
    res = @manager.TxReticleGroupIDListInq(@_user)
    return nil if rc(res) != 0
    return res if params[:raw]
    return res.objectIdentifiers.collect {|p| p.identifier}
  end

end
