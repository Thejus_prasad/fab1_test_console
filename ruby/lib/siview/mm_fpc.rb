=begin
(c) Copyright 2013 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

History:
  2015-09-04 dsteger, merged code from Fab7QA
=end

class SiView::MM

  def mrecipe_list(params={})
    lot = params[:lot] || ''
    eqp = params[:eqp] || ''
    mrecipe = params[:mrecipe] || '' # Supports wildcards
    pd = params[:pd] || ''
    fpc_cat = params[:fpc_cat] || ''
    fpc_criteria = params[:fpc_crit] || 'All' # White, NonWhite
    search_criteria = params[:search_crit] || 'All' # PD
    inparm = @jcscode.pptMachineRecipeListForFPCInqInParm_struct.new(
      oid(lot), oid(eqp), oid(mrecipe), oid(pd), fpc_cat, fpc_criteria, search_criteria, any)
    #
    res = @manager.TxMachineRecipeListForFPCInq(@_user, inparm)
    return nil if rc(res) != 0
    return res.strMachineRecipeList
  end

  # find all fpc definitions on the lot's route and connected subroutes, UNUSED
  def fpc_processlist_inroute(lot, params={})
    lotID = lot.instance_of?(String) ? oid(lot) : lot
    inparm = @jcscode.pptFPCProcessListInRouteInqInParm_struct.new(lotID, any)
    #
    res = @manager.TxFPCProcessListInRouteInq__150(@_user, inparm)
    return nil if rc(res) != 0
    return res
    # return res if params[:raw]
    # return res.strMainRouteInfo.strMainRouteOperationInfoList.collect {|f|
    #   subroute = f.strConnectedSubRouteInfoList.collect {|sub|
    #     subroute = sub.routeID.identifier
    #     sub.strConnectedSubRouteOperationInfoList.collect {|s|
    #       sub2route = s.strConnectedSub2RouteInfoList.collect {|sub2|
    #         sub2route = sub2.routeID.identifier
    #         sub2.strConnectedSub2RouteOperationInfoSequence.collect {|s2|
    #           FPCOperation.new(sub2route, s2.operationID.identifier, s2.operationNumber, s2.operationPDType, s2.FPCInfoCount > 0)
    #         }
    #       }
    #       FPCOperation.new(subroute, s.operationID.identifier, s.operationNumber, s.operationPDType, s.FPCInfoCount > 0, sub2route)
    #     }
    #   }
    #   FPCOperation.new(res.strMainRouteInfo.mainRouteID.identifier, f.mainPDID.identifier, f.operationNumber,
    #     f.operationPDType, f.FPCInfoCount > 0, subroute)
    # }
  end

  # return FPC info, optionally pass '' for lot, route, opNo and pass fpcids
  def fpc_info(lot, route, opNo, params={})
    lotID = lot.instance_of?(String) ? oid(lot) : lot
    routeID = route.instance_of?(String) ? oid(route) : route
    fpcids = params[:fpcids] || []
    org_route = params[:org_route] || ''
    org_routeID = org_route.instance_of?(String) ? oid(org_route) : org_route
    org_opNo = params[:org_opNo] || ''
    sub_route = params[:sub_route] || ''
    sub_routeID = sub_route.instance_of?(String) ? oid(sub_route) : sub_route
    sub_opNo = params[:sub_opNo] || ''
    inparam = @jcscode.pptFPCDetailInfoInqInParm_struct.new(fpcids.to_java(:String), lotID,
      routeID, opNo, org_routeID, org_opNo, sub_routeID, sub_opNo, true, true, true, true, any)
    if @customized
      res = @manager.CS_TxFPCDetailInfoInq__101(@_user, inparam)
      return nil if rc(res) != 0
      return res.strCS_WholeFPCInfoList
    else
      res = @manager.TxFPCDetailInfoInq__101(@_user, inparam)
      return nil if rc(res) != 0
      return res.strFPCInfoList
    end
  end

  # delete FPCs specified by the FPC IDs for a lot _family_, return 0 on success, UNUSED
  def fpc_delete(lot, fpcids)
    lotID = lot.instance_of?(String) ? oid(lot) : lot
    $log.info "fpc_delete #{lotID.identifier.inspect}, #{fpcids}"
    inparam = @jcscode.pptFPCDeleteReqInParm_struct.new(fpcids.to_java(:String), lotID, any)
    #
    res = @manager.TxFPCDeleteReq(@_user, inparam, params[:memo] || '')
    return rc(res)
  end

  # Determine operation info by finding the operation on the route, TODO: remove
  def _get_pd_info(lot, op_no, params={})
    original_opno = op_no
    sub_route = oid('')
    sub_opno = ''
    pd_info = self.fpc_process_list_inroute(lot, raw: true).strMainRouteInfo
    org_route = pd_info.mainRouteID
    if params[:sub2route] # branch 2 branch
      sub2_pd_info = nil
      # need to find the operation on the sub-sub route
      pd_info.strMainRouteOperationInfoList.each do |op|
        op.strConnectedSubRouteInfoList.select {|sub| !params.has_key?(:subroute) || sub.routeID.identifier == params[:subroute]}.each do |sub|
          sub.strConnectedSubRouteOperationInfoList.each do |sop|
            sub2_pd_info = sop.strConnectedSub2RouteInfoList.find {|sub2| sub2.routeID.identifier == params[:sub2route]}
            if sub2_pd_info
              sub_route = sub.routeID
              sub_opno = sop.operationNumber
              original_opno = op.operationNumber
              sub_pd_info = sop
              break
            end
          end
        end
      end
      ($log.error "#{params[:subroute]} -> #{params[:sub2route]} not found"; return nil) unless sub2_pd_info
      ope_info = sub2_pd_info.strConnectedSub2RouteOperationInfoSequence.find { |ope| ope.operationNumber == op_no}
      ($log.error "#{op_no} not found on #{params[:sub2route]}"; return nil) unless ope_info
    elsif params[:subroute] # subroute
      sub_pd_info = nil
      # need to find the operation on the sub route
      pd_info.strMainRouteOperationInfoList.each do |r|
        sub_pd_info = r.strConnectedSubRouteInfoList.find {|sub| sub.routeID.identifier == params[:subroute]}
        if sub_pd_info
          original_opno = r.operationNumber
          break
        end
      end
      ($log.error "#{params[:subroute]} not found"; return nil) unless sub_pd_info
      ope_info = sub_pd_info.strConnectedSubRouteOperationInfoList.find { |ope| ope.operationNumber == op_no}
      ($log.error "#{op_no} not found on #{params[:subroute]}"; return nil) unless ope_info
    else
      # find the PD on the main route
      ope_info = pd_info.strMainRouteOperationInfoList.find { |ope| ope.operationNumber == op_no}
      ($log.error "#{op_no} not found on main route"; return nil) unless ope_info
    end
    return ope_info, org_route, original_opno, sub_route, sub_opno
  end

  # get recipe parameters for FPC, UNUSED
  def eqp_recipe_parameters(params)
    lot = params[:lot] || ''
    eqp = params[:eqp] || ''
    mrecipe = params[:mrecipe] || ''
    pd = params[:pd] || ''
    fpc_category = params[:fpc_category] || ''
    search_param = params[:search_param] || 'Equipment'
    inparam = @jcscode.pptEqpRecipeParameterListInqInParm_struct.new(
      oid(lot), oid(eqp), oid(mrecipe), oid(pd), fpc_category, search_param, any)
    #
    res = @manager.TxEqpRecipeParameterListInq(@_user, inparam)
    return if rc(res) != 0
    return res.strRecipeParameterInfoList
  end

  # Register or (update) FPC information
  #  by lot, route, opNo
  #    equipment
  #    mrecipe
  #    reticle
  #   - set parameter :subroute for FPC on branch route
  #   - set parameter :sub2route for FPC on branch2branch route
  #  for update: specify fpc_id and action_type = "Update"
  # TODO: need to refactor
  def fpc_update(lot, op_no, params={})
    memo = params[:memo] || ''
    action_type = params[:action_type] || "Create" #Update #NoChange
    li = lots_info_raw(lot)
    li_basic = li.strLotInfo[0].strLotBasicInfo
    skip_flag = params[:skip] || false
    force_skip = (params[:force_skip] || false)
    ($log.warn "setting skip flag since force_skip is set"; skip_flag = true) if force_skip && !skip_flag
    restrict_eqp = params[:restrict_eqp] || false
    eqp = params[:eqp] || ''
    m_recipe = params[:mrecipe] || ''
    dcdef = params[:dcdef] || ''
    dcspec = params[:dcspec] || ''
    hold_flag = params[:hold] || false
    rcp_type = ''
    reticles = params[:reticle] || ''
    reticles = reticles.split unless reticles.instance_of?(Array)
    fpc_id = params[:fpc_id] || '' # requires action type "Update"
    rcp_params = params[:rcp_params] || {} # waferid/lot: [rcp_param_name, value]
    mrg_route = (params[:merge_route] || '')
    mrg_op_no = (params[:merge_opNo] || '')

    version_const = @release > 10 ? '__101' : ''
    custom_const = @customized ? 'CS_' : ''

    $log.info "fpc_update #{li_basic.familyLotID.identifier}, #{op_no}, #{params.inspect}"

    ope_info, org_route, org_opno, sub_route, sub_opno = _get_pd_info(lot, op_no, params)

    if params[:wafers]
      wparam = {select: params[:wafers]}
      fpc_type = 'ByWafer'
    else
      wparam = {wafers: true}
      fpc_type = 'ByLot'
    end
    if rcp_params == {}
      r_params = [].to_java(@jcscode.pptRecipeParameterInfo_struct)
    else
      r_params = eqp_recipe_parameters(eqp: eqp)

      if rcp_params.has_key?(lot)
        #by lot
        rcp_params[lot].each do |(eqp_param,value)|
          _eparam = r_params.find {|r| r.parameterName == eqp_param}
          if _eparam
            _eparam.parameterValue = value
            _eparam.useCurrentSettingValueFlag = true
          else
            $log.warn "  recipe parameter #{eqp_param} not found"
          end
        end
        rcp_type = 'RecipeParmChangeByLot'
      end
    end

    wafers = lot_info(lot, wparam).wafers.map {|w|
      if rcp_params.has_key?(w.wafer)
        w_r_params = eqp_recipe_parameters(eqp: eqp)
        #by wafer
        rcp_params[w.wafer].each do |(eqp_param,value)|
          _eparam = w_r_params.find {|r| r.parameterName == eqp_param}
          if _eparam
            _eparam.parameterValue = value
            _eparam.useCurrentSettingValueFlag = true
            $log.info "  updated #{_eparam.inspect}"
          else
            $log.warn "  recipe parameter #{eqp_param} not found"
          end
        end
        rcp_type = 'RecipeParmChangeByWafer'
        @jcscode.pptLotWaferInfo_struct.new(oid(w.wafer), w_r_params, any)
      else
        @jcscode.pptLotWaferInfo_struct.new(oid(w.wafer), r_params, any)
      end
    }.to_java(@jcscode.pptLotWaferInfo_struct)
    ($log.error " no wafer found. Invalid FPC setup"; return ) if wafers.empty?

    #need to retrieve reticle groups
    reticles = reticles.collect.each_with_index {|r, i|
      @jcscode.pptReticleInfo_struct.new(i, oid(r), reticle_status(r).reticleBRInfo.reticleGroupID, any)
    }.to_java(@jcscode.pptReticleInfo_struct)

    if params[:subroute] || params[:sub2route]
      pdid = ope_info.operationID
      routeid = oid(params[:subroute]) if params[:subroute]
      routeid = oid(params[:sub2route]) if params[:sub2route]
    else
      pdid = ope_info.mainPDID
      routeid = org_route
      org_route = oid('') # need to be empty
      org_opno = '' # need to be empty
    end

    # if action_type is Update we need to find the existing FPC entry
    if params[:action_type] == "Update" && params[:fpc_id]
      up_fpc_infos = fpc_info(nil, nil, nil, fpcids: fpc_id)
      ($log.error "no FPC found with id #{fpc_id}"; return) if up_fpc_infos.empty?
      up_fpc_info = @customized ? up_fpc_infos.first.strFPCInfo : up_fpc_infos.first
      fpc_group = up_fpc_info.FPCGroupNo
      create_time = up_fpc_info.createTime
      update_time = up_fpc_info.updateTime # set time to latest update
    else
      fpc_group = 0
      create_time = ''
      update_time = ''
    end

    new_fpc_info = @release > 10 ? @jcscode.pptFPCInfo__101_struct.new : @jcscode.pptFPCInfo_struct.new
    new_fpc_info.FPC_ID = fpc_id
    new_fpc_info.lotFamilyID = li_basic.familyLotID
    new_fpc_info.mainPDID = routeid
    new_fpc_info.operationNumber = op_no
    new_fpc_info.originalMainPDID = org_route
    new_fpc_info.originalOperationNumber = org_opno
    new_fpc_info.subMainPDID = sub_route
    new_fpc_info.subOperationNumber = sub_opno
    new_fpc_info.FPCGroupNo = fpc_group
    new_fpc_info.FPCType = fpc_type
    new_fpc_info.mergeMainPDID = oid(mrg_route)
    new_fpc_info.mergeOperationNumber = mrg_op_no
    new_fpc_info.FPCCategory = ''
    new_fpc_info.pdID = pdid
    new_fpc_info.pdType = ope_info.operationPDType
    new_fpc_info.skipFlag = skip_flag
    new_fpc_info.correspondingOperNo = ''
    new_fpc_info.restrictEqpFlag = restrict_eqp
    new_fpc_info.equipmentID = oid(eqp)
    new_fpc_info.machineRecipeID = oid(m_recipe)
    new_fpc_info.dcDefID = oid(dcdef)
    new_fpc_info.dcSpecID = oid(dcspec)
    new_fpc_info.sendEmailFlag = false
    new_fpc_info.holdLotFlag = hold_flag
    new_fpc_info.recipeParameterChangeType = rcp_type
    new_fpc_info.strLotWaferInfoList = wafers
    new_fpc_info.strReticleInfoList = reticles
    new_fpc_info.strDCSpecList = [].to_java(@jcscode.pptDCSpecDetailInfo_struct)
    new_fpc_info.description = 'QA Test'
    new_fpc_info.createTime = create_time
    new_fpc_info.updateTime = update_time
    new_fpc_info.claimUserID = @user
    new_fpc_info.strCorrespondingOperationInfoList = [].to_java(@jcscode.pptCorrespondingOperationInfo_struct)
    new_fpc_info.siInfo = any
    fpc_info_const = @jcscode.const_get("ppt#{custom_const}FPCInfoAction#{version_const}_struct")

    #only one entry is supported right now
    if @customized
      csfpc_info = @jcscode.pptCS_CSFPCInfo_struct.new(force_skip, '', oids([]), '', any)
      action_list = [fpc_info_const.new(action_type, new_fpc_info, csfpc_info, any)]
    else
      action_list = [fpc_info_const.new(action_type, new_fpc_info, any)]
    end

    fpc_infos = fpc_info(lot, routeid, op_no, org_route: org_route.identifier,
      org_opNo: org_opno, sub_route: sub_route.identifier, sub_opNo: sub_opno)
    # Add existing list of fpc infos (skip with same fpc_id)
    if @customized
      fpc_infos.reject {|wfpc| wfpc.strFPCInfo.FPC_ID == fpc_id}.each do |wfpc|
        action_list << fpc_info_const.new("NoChange", wfpc.strFPCInfo, wfpc.strCS_CSFPCInfo, any)
      end
    else
      fpc_infos.reject {|fpc| fpc.FPC_ID == fpc_id }.each do |fpc|
        action_list << fpc_info_const.new("NoChange", fpc, any)
      end
    end

    fpc_update_req_const = @jcscode.const_get("ppt#{custom_const}FPCUpdateReqInParm#{version_const}_struct")
    fpc_inparam = fpc_update_req_const.new(action_list.to_java(fpc_info_const), any)
    $log.debug {" fpc_input: #{show_result(data: fpc_inparam)}"}
    #
    res = @manager.send("#{custom_const}TxFPCUpdateReq#{version_const}", @_user, fpc_inparam, memo)
    return nil if rc(res) != 0
    return res.FPC_IDs.collect {|f| f}
  end

end
