=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.
=end


class SiView::MM
  UdataCategories = {
    qainvalid:    ['QAInvalid', ['OBJECT_ID']],
    area:         ['PosArea', ['AREA_ID']],
    area_group:   ['PosAreaGroup', ['AREAGRP_ID']],
    bank:         ['PosBank', ['BANK_ID']],
    bin:          ['PosBinSpecification', ['BINSPEC_ID']],
    buffer_res:   ['PosBufferResource', ['BUFFRSC_ID', 'EQP_ID']],
    calendar:     ['PosCalendarDate', ['CALENDAR_DATE']],
    carrier:      ['PosCassette', ['CAST_ID']],
    code:         ['PosCode', ['CODE_ID', 'CATEGORY_ID']],  # also for fixture categories
    controljob:   ['PosControlJob', ['CTRLJOB_ID']],
    customer:     ['PosCustomer', ['CUSTOMER_ID']],
    customer_product: ['PosCustomerProduct', ['CUSTOMER_ID', 'PRODSPEC_ID']],
    dcdef:        ['PosDataCollectionDefinition', ['DCDEF_ID']],
    dcspec:       ['PosDataCollectionSpecification', ['DCSPEC_ID']],
    dispatcher:   ['PosDispatcher', ['EQP_ID']],
    eqp:          ['PosMachine', ['EQP_ID']],
    inhibit:      ['PosEntityInhibit', ['INHIBIT_ID']],
    lot:          ['PosLot', ['LOT_ID']],
    lotfamily:    ['PosLotFamily', ['LOTFAMILY_ID']],
    lrecipe:      ['PosLogicalRecipe', ['LCRECIPE_ID']],
    pd:           ['PosProcessDefinition', ['PD_ID', 'PD_LEVEL']],
    product:      ['PosProductSpecification', ['PRODSPEC_ID']],
    productgroup: ['PosProductGroup', ['PRODGRP_ID']],
    reticle:      ['PosReticle', ['DRBL_ID']],
    route_opNo:   ['PosProcessOperationSpecification', ['OPE_NO', 'PD_ID', 'PD_LEVEL']],
    stocker:      ['PosStorageMachine', ['STK_ID']],
    wafer:        ['PosWafer', ['WAFER_ID']],
  }

  # return defined UDATA fields for a class ID (DR06 = Cassette), returns always {} in R15 (?)
  def user_defined_data(classId, params={})
    inq = @jcscode.pptUserDefinedDataAttributeInfoInqInParm_struct.new(classId, any)
    #
    res = @manager.TxUserDefinedDataAttributeInfoInq(@_user, inq)
    return nil if rc(res) != 0
    return res if params[:raw]
    return Hash[res.strUserDefinedDataSeq.collect {|e| [e.name, e.type]}]
  end

  # get UDATA, valid class names are CAST (carrier), EQP, DRBL (reticle), LRCP, ...; return Hash
  def AMDGetUDATA(classID, objectID, params={})
    res = @manager.AMD_TxGetUDATAInq(@_user, classID, objectID)
    return nil if rc(res) != 0
    return res if params[:raw]
    ret = {}
    res.nameValue.each {|e|
      k, v = e.AMD_name_Info, e.AMD_value_Info
      $log.warn "duplicate UDATA entries for #{k.inspect}: #{ret[k].inspect}, #{v.inspect}" if ret[k]
      ret[k] = v
    }
    return ret
  end

  # # get Udata at route operation - only usage was in Test450_CSScript
  # def user_data_route_operation(route, opNo, params={})
  #   if params[:customized]
  #     AMDGetUDATA('POS', "#{route}:#{opNo}", params)
  #   else
  #     # attn, returns 'Object not found' for old routes without any UDATA
  #     user_data(:route_opNo, [opNo, route, 'Main'], params)
  #   end
  # end

  # # get Udata for product group -- unused
  # def user_data_productgroup(productgroup, params={})
  #   if params[:customized] || release < 11
  #     AMDGetUDATA('PRODGRP', productgroup, params)
  #   else
  #     user_data(:productgroup, productgroup, params)
  #   end
  # end

  # # get Udata for product -- unused
  # def user_data_product(product, params={})
  #   if params[:customized] || release < 11
  #     AMDGetUDATA('PRODSPEC', product, params)
  #   else
  #     user_data(:product, product, params)
  #   end
  # end

  # # get Udata for bank -- unused
  # def user_data_bank(bank, params={})
  #   if params[:customized] || release < 11
  #     AMDGetUDATA('BANK', bank, params)
  #   else
  #     user_data(:bank, bank, params)
  #   end
  # end

  def user_data_pd(pd)
    user_data(:pd, [pd, 'Operation'])
  end

  def user_data_route(route)
    user_data(:pd, [route, 'Main'])
  end

  # query UDATA, optionally only one passed as name: <name>,
  #   category is one of the keys in UdataCategories, e.g. :lot or :eqp,
  #   identifier is the (array of) identifiers, e.g. [<PD>, 'Operation']
  #
  # note: look at the raw data (show_result) to detect duplicate keys with different originator
  #
  # return hash of UDATA {field: value} on success or nil
  def user_data(category, identifier, params={})
    ($log.warn "user_data: undefined category #{category}"; return) unless UdataCategories.has_key?(category)
    classname, keys = UdataCategories[category]
    identifier = [identifier] if identifier.instance_of?(String)
    name = params[:name] || ''
    originator = params[:originator] || ''
    memo = params[:memo] || ''
    hinfo = keys.each_with_index.collect {|t, i|
      @jcscode.pptHashedInfo_struct.new(t, identifier[i], any)
    }.to_java(@jcscode.pptHashedInfo_struct)
    inq = @jcscode.pptUserDataInqInParm__101_struct.new('', classname, hinfo, name, originator, any)
    #
    res = @manager.TxUserDataInq__101(@_user, inq, memo)
    return nil if rc(res) != 0
    return res if params[:raw]
    return Hash[res.strUserDataSeq.collect {|e| [e.name, e.value]}]
  end

  # register, change or delete MM UDATA: user_data_update(:lot, lot, 'Udata1'=>'XXX'); return 0 on success
  def user_data_update(category, objectids, udatas, params={})
    action = params[:action] || 'Regist'  # Regist or Delete
    datatype = params[:datatype] || 'String'
    originator = params[:originator] || 'MM'
    objectids = [objectids] if objectids.instance_of?(String)
    $log.info "user_data_update #{category.inspect}, #{objectids}, #{udatas}, action: #{action.inspect}"
    ($log.warn "undefined category #{category}"; return) unless UdataCategories.has_key?(category)
    classname, keys = UdataCategories[category]
    hinfos = keys.each_with_index.collect {|k, i| @jcscode.pptHashedInfo_struct.new(k, objectids[i], any)}
    udatas = udatas.each_pair.collect {|k, v| @jcscode.pptUserData_struct.new(k, datatype, v, originator, any)}
    parms = @jcscode.pptUserDataUpdateReqInParm_struct.new('', classname,
      hinfos.to_java(@jcscode.pptHashedInfo_struct), action, udatas.to_java(@jcscode.pptUserData_struct), any)
    #
    res = @manager.TxUserDataUpdateReq(@_user, parms, params[:memo] || '')
    return rc(res)
  end

  # convenience method to delete UDATA passed as string, array of strings or hash (keys are used); return 0 on success
  def user_data_delete(category, objectid, udatas, params={})
    udatas = udatas.keys if udatas.instance_of?(Hash)
    udatas = [udatas] if udatas.instance_of?(String)
    udatas = Hash[udatas.collect {|k| [k, '']}]
    #
    pparams = {datatype: params[:datatype], originator: params[:originator], memo: params[:memo], action: 'Delete'}
    return user_data_update(category, objectid, udatas, pparams)
  end

end
