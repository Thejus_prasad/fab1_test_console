=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author:   Steffen Steidten, 2010-08-02
Version:  1.1.5

History:
  2012-01-24 ssteidte, use Sequel instead of DBI for databases
  2012-08-23 ssteidte, remove to_char conversion from queries for Date and Time fields
  2015-02-18 pbuddara, extract actually called TxXXX__version from associated SiView::MM object
  2015-08-27 ssteidte, fixed #show()
  2017-05-12 sfrieske, add other queries
  2018-05-16 sfrieske, minor cleanup
  2021-08-26 sfrieske, removed dependency on misc.rb (duration_in_seconds)
=end

# require 'misc'
require 'dbsequel'
require 'yaml'


# Extract information from a SiView log database.
class SvLog
  attr_accessor :env, :db

  def initialize(env, params={})
    @env = env
    @db = DB.new("svlog.#{env}", params)
  end

  def close
    @db.close
  end

  # return TXID for tx name, works in selected environemnts only (e.g. ITDC)
  def txid_byname(txname)
    res = @db.select('select TX_ID from SIVIEW_TXNAME where (TX_SM_NAME=? or TX_METHOD_NAME=?)', txname, txname) || return
    return res.empty? ? nil : res.first.first
  end

  # build svlog query: defaults to "select ... from ... where"
  #
  #   :interval - interval - defaults to 900s, can pass as '1h'
  #
  #   :tmin - start time - defaults to current time - interval
  #
  #   :tmax - end time
  #
  #   :count - if true, use "select count(*) from ..."
  #
  # grouping:
  #
  #   :group - columns to use for "group by ..."
  #
  #   :aggr - aggregate to be used for "select ..." (in combination with :group only)
  #
  #           e. g. "avg(TXDURATION), count(TXDURATION)" - defaults to "count(*)"
  #
  # filters: ("where ..."):
  #
  #   :txname or :tx - transaction(s), e. g. 'TXTRC002,TXTRC054'
  #
  #   :txuser or :user - user
  #
  #   :txresult or :txresult - transaction result
  #
  #   :txdetail or :detail - substring in TXDETAIL column
  #
  # sorting:
  #
  #   :order - colums for ordering - defaults to TXSTART fon non-grouped query
  #
  #   :reverse or :desc - sort result in descending order
  def query(params={})
    # report interval in seconds or as e.g. '30m'
    interval = params[:interval] || params[:i] || 900
    # tmin either as Time object of as number of seconds back from now
    tmin = params[:tmin] || params[:tstart] || (Time.now - interval)
    tmin = Time.parse(tmin) if tmin.kind_of?(String)
    tmin = (Time.now - tmin.abs) if tmin.kind_of?(Numeric)
    # tmax either as Time object or as number of seconds from tmin
    tmax = params[:tmax] || params[:tend] || interval
    tmax = Time.parse(tmax) if tmax.kind_of?(String)
    tmax = tmin + tmax if tmax.kind_of?(Numeric)
    group = params[:group]
    groupfirst = params[:groupfirst]
    aggr = params[:aggr] || (group && 'COUNT(*)')
    order = params[:order] || (group && '1') || 'TXSTART'
    countonly = params[:count]
    # get row count or full content
    if countonly
      q = 'SELECT COUNT(*) FROM SVLOG_ADMIN.SIVIEW_DETAIL WHERE'
    elsif group
      q = "SELECT #{group}, #{aggr} FROM SIVIEW_DETAIL, SIVIEW_TXNAME WHERE TXNAME = TX_ID(+) AND"
    else
      q = 'SELECT TXSTART, TXDURATION as DURATN, HOST, SERVER, TXUSER,
        TXNAME as TXID, TX_SM_NAME as TXNAME, TXRESULT as RC, TXDETAIL, TXERRTXT
        FROM SIVIEW_DETAIL, SVLOG_ADMIN.SIVIEW_TXNAME WHERE TXNAME = TX_ID(+) AND'
      # q = 'SELECT TXSTART, TXDURATION, HOST, SERVER, TXUSER, TXNAME, TXRESULT, TXDETAIL, TXERRTXT FROM SIVIEW_DETAIL WHERE'
    end
    q += ' TXSTART >= ? AND TXSTART < ? '
    qargs = [tmin, tmax]
    # filters
    q, qargs = @db._q_restriction(q, qargs, 'TXNAME', params[:txid])
    q, qargs = @db._q_restriction(q, qargs, 'not TXNAME', params[:notxid])
    q, qargs = @db._q_restriction(q, qargs, 'TX_SM_NAME', params[:txname])
    q, qargs = @db._q_restriction(q, qargs, 'TXRESULT', params[:txresult] || params[:result])
    q, qargs = @db._q_restriction(q, qargs, 'TXUSER', params[:txuser] || params[:user])
    # filter by detail
    vv =  params[:txdetail] || params[:detail]
    q, qargs = @db._q_restriction(q, qargs, 'TXDETAIL', "%#{vv}%") if vv
    # grouping
    q += "\nGROUP BY #{group}" if group
    # sorting
    q += "\nORDER BY #{order}" unless countonly
    # reverse order
    q += ' DESC' if params[:reverse] || params[:desc]
    return @db.select(q, *qargs)
  end

  # format query output for human reading
  #
  # outfile: outputfile, defaults to standard output, has to be opened by client
  #
  # delimiter: - separator between columns - defaults to ' | '
  #
  # header: add colmn names as first and last lines
  def show(params={})
    out = params[:outfile] || $stdout
    delimiter = params[:delimiter] || ' | '
    res = query(params)
    out.puts @db.ds.columns.join(delimiter) if params[:header]
    res.each {|line| out.puts line.join(delimiter)}
    out.puts @db.ds.columns.join(delimiter) if params[:header]
  end


  # special queries

  # get transaction count
  def get_txcount(params={})
    res = query(params.merge(count: true)) || ($log.warn "no result"; return)
    return res[0][0]
  end

  # get number of OpeStarts
  def get_opestarts(params={})
    get_txcount(params.merge(txid: ['TXTRC002', 'TXTRC054']))
  end

  # get tx statistics: tx (ID and NAME), #calls, avg., max. and min. duration, see siview.rb
  def get_statistics(params={})
    $log.info "get_statistics #{params}"
    sv = params.delete(:sv)
    aggr = 'avg(TXDURATION) as avg, stddev(TXDURATION) as std, max(TXDURATION) as max, min(TXDURATION) as min, count(*) as count'
    res = query(params.merge(group: 'TX_SM_NAME, TXNAME', aggr: aggr, desc: true)) || ($log.warn 'no result'; return)
    # convert BigNums to int
    data = res.collect {|row| row.collect {|e| e.kind_of?(Numeric) ? e.to_i : e}}
    ret = {time: Time.now.utc, env: @env, col: [:txname, :txid] + @db.ds.columns[2..-1], data: data}
    svrelease = params[:svrelease]
    release = params[:release]
    if sv
      # special format for perf comparison, see siview.rb
      # add MM release and resolve TX names if a MM instance has been passed
      svrelease ||= sv.release  # MM server release
      release ||= sv.release    # MM client release (max tx__ level)
      ret[:data] = data.collect {|row|
        txname, version = sv.actual_txname(row[0])
        [txname, version, [svrelease] << row[2..-1]]
      }
      ret[:user] = sv.user
      ret[:release] = release
    end
    # export data to the passed file name or generate one if export: true was passed
    if fname = params[:export]
      fname = "log/svlog_perf_#{@env}_#{svrelease}_#{release}_#{Time.now.utc.strftime('%F_%T').gsub(':', '-')}.yaml" unless fname.kind_of?(String)
      $log.info "  writing log to #{fname.inspect}"
      File.binwrite(fname, ret.to_yaml)
    end
    return ret
  end

  # other tables

  def busyratio(params={})
    qargs = []
    q = 'SELECT TIMESTAMP, HOST, DISPATCHER, SERVERGROUP, BUSYRATIO from SVLOG_ADMIN.SIVIEW_DISP_HA_DETAIL'
    tstart = params[:tstart] || (Time.now - 930)
    tend = params[:tend] || Time.now
    dispatcher = params[:dispatcher] || 'MM'  # XM
    svrgroup = params[:svrgroup] || 'All'     # Remaining, RTDServer
    q, qargs = @db._q_restriction(q, qargs, 'TIMESTAMP >=', tstart)
    q, qargs = @db._q_restriction(q, qargs, 'TIMESTAMP <=', tend)
    q, qargs = @db._q_restriction(q, qargs, 'DISPATCHER', dispatcher)
    q, qargs = @db._q_restriction(q, qargs, 'SERVERGROUP', svrgroup)
    q += ' ORDER BY TIMESTAMP DESC'
    res = @db.select(q, *qargs) || return
    sum = 0
    count = 0
    ret = res.collect {|r|
      ratio = r[4].to_f.round(3)
      sum += ratio
      count += 1
      {time: r[0], host: r[1], dispatcher: r[2], svrgroup: r[3], busyratio: ratio}
    }
    return params[:avg] ? (sum / count).round(1) : ret
  end

end
