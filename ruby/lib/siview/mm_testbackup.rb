=begin
(c) Copyright 2012 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.
=end

class SiView::MM

  # Wrapper methods to send/receive and return/receive a lot.
  # The assumption is that sending and receiving lots work.

  # prepare lot for sending, send it to the backup site and receive it there, return true on success
  def backup_prepare_send_receive(lot, backup_sv, params={})
    $log.info "backup_prepare_send_receive #{lot.inspect}, #{params.inspect}"
    prepare_for_backup(lot, params) || return
    #
    # send and receive lot
    $log.info "-- sending lot to the backup site --"
    res = backupop_lot_send(lot, backup_sv, params)
    ($log.warn "error sending lot #{lot.inspect} to #{backup_sv.inspect}"; return) if res != 0
    res = backup_sv.backupop_lot_send_receive(lot, self, params)
    ($log.warn "error receiving lot #{lot.inspect} at #{backup_sv.inspect}"; return) if res != 0
    $log.info "-- lot is at the backup site (#{backup_sv.env}) --"
    return true
  end

  # prepare lot for sending, return true on success
  def prepare_for_backup(lot, params={})
    $log.info "-- preparing lot for sending to the backup site --"
    if backup_opno = params[:backup_opNo]
      res = lot_opelocate(lot, backup_opno, force: :ondemand)
      return if res != 0
    elsif backup_op = params[:backup_op]
      res = lot_opelocate(lot, nil, op: backup_op, force: :ondemand)
      return if res != 0
    end
    if backup_bank = params[:backup_bank]  # NonProBank
      li = lot_info(lot)
      if li.states['Lot Inventory State'] != 'NonProBank'
        # wafer_sort(lot, carrier_list(status: 'AVAILABLE', empty: true).first) if li.carrier.empty?
        res = lot_nonprobankin(lot, backup_bank)
        return if res != 0
      end
    end
    lot_hold_release(lot, nil)  # release XSMP holds
    res = wafer_sort(lot, '')
    return if res != 0
    return true
  end

  # prepare lot for return to the source site (we are backup site)
  def prepare_for_return(lot, params={})
    $log.info "-- preparing lot for return to the source site --"
    if return_bank = params[:return_bank]  # NonProBank
      li = lot_info(lot)
      if li.states['Lot Inventory State'] != 'NonProBank'
        # wafer_sort(lot, carrier_list(status: 'AVAILABLE', empty: true).first) if li.carrier.empty?
        res = lot_nonprobankin(lot, return_bank)
        return if res != 0
      end
    end
    lot_hold_release(lot, nil)  # release XSMP holds
    res = wafer_sort(lot, '')
    return if res != 0
    return true
  end

  # prepare lot and return it to the source site and receive it there (we are backup site)
  #
  # return true on success, nil if preparation fails, false if return/receive fails
  def backup_prepare_return_receive(lot, source_sv, params={})
    $log.info "backup_prepare_return_receive #{lot.inspect}, #{params.inspect}"
    prepare_for_return(lot, params) || return
    # return and receive lot
    $log.info "-- returning lot to the source site --"
    res = backupop_lot_return(lot, source_sv, params)
    ($log.warn "error returning lot #{lot.inspect} to #{source_sv.inspect}"; return false) if res != 0
    res = source_sv.backupop_lot_return_receive(lot, self, params)
    ($log.warn "error receiving returned lot #{lot.inspect} at #{source_sv.inspect}"; return false) if res != 0
    $log.info "-- lot is back at the source site (#{source_sv.env}) --"
    return true
  end

  # Complete the lot at backup, virtual lot at source site is banked out, return 0 on success
  def backup_complete(lot, backup_sv, params={})
    $log.info "backup_complete #{lot.inspect}, #{backup_sv}"
    # backup site: complete lot
    lib = backup_sv.lot_info(lot)
    if lib.states['Lot Inventory State'] == 'NonProBank'
      (backup_sv.lot_nonprobankout(lot) == 0) || return
    end
    unless ['COMPLETED', 'SHIPPED'].member?(lib.status)
      if lib.carrier.empty?
        carrier = backup_sv._get_empty_carrier(params[:bak_carrier] || '%') || ($log.warn 'no empty carrier at backup site'; return)
        (backup_sv.wafer_sort(lot, carrier) == 0) || return
      end
      backup_sv.lot_futurehold_cancel(lot, nil)
      (backup_sv.lot_opelocate(lot, :last, force: :ondemand) == 0) || return
      if backup_sv.lot_info(lot).bank.empty?
        (backup_sv.lot_bankin(lot) == 0) || return
      end
    end
    # set status COMPLETE at source site
    res = backup_lot_status_rpt(lot)  #, lotfamily: params[:lotfamily])
    return res if res != 0
    $log.info '-- lot is COMPLETE at backup and BackupStatus Completed at source sites --'
    # source site: move to (virtual) carrier and bank out
    carrier = _get_empty_carrier(params[:src_carrier] || '%') || ($log.warn 'no empty carrier at source site'; return)
    (wafer_sort(lot, carrier) == 0) || return
    return lot_nonprobankout(lot)
  end

  # trying to clean up lots in backup, UNDER DEVELOPEMENT
  def backup_delete_lot_family(lot, backup_sv, params={})
    $log.info "backup_delete_lot_family #{lot.inspect} (#{@env.inspect})"
    unless lot_exists?(lot)
      if params[:noswitch]
        $log.info '  no lot info'
        return
      else
        $log.info '  no lot info, trying to other site'
        return backup_sv.backup_delete_lot_family(lot, self, noswitch: true)
      end
    end
    li = lot_info(lot, backup: true)
    return delete_lot_family(lot) if li.backup.born.empty?
    if !params[:noswitch] && li.backup.born == backup_sv.servername  # not generally recommended
      $log.info "  switching to source site"
      return backup_sv.backup_delete_lot_family(lot, self, noswitch: true)
    end
    if li.backup.born != @servername
      # try a return cancel (we are at the wrong site because the lot doesnt exist anymore at the source site)
      # if li.status == 'NonProBank' ...
      if li.backup.current_loc && li.backup.transfering
        # returned but not yet received yet
        backupop_lot_return(lot, backup_sv, recover: true)
        sublottype_change(lot, 'SCRAPQA')
        sleep 10
      end
      return true if lot_delete(lot) == 0
      $log.warn "wrong source site, expected: #{@servername}"
      return
    end
    #
    if li.backup.backup_proc
      if li.backup.current_loc
        # lot is at our (the source) site
        if li.backup.transfering
          backupop_lot_send_cancel(lot, backup_sv)
        else
          #??
        end
      else
        # lot is at the backup site
        unless backup_sv.lot_exists?(lot)
          return true if lot_delete(lot) == 0
          ($log.warn 'lot does not exist at backup site'; return)
        end
        lib = backup_sv.lot_info(lot, backup: true) #|| return
        if li.backup.transfering
          if lib.backup.backup_proc
            # cancel the return request, put lot in a carrier and nonprobank out
            (backup_sv.backupop_lot_return(lot, self, recover: true) == 0) #|| return
            backup_complete(lot, backup_sv)
          else
            # if lib.backup.transfering
              # defective lot, other site thinks it's not in backup processing
              backupop_lot_return_receive(lot, backup_sv, ignore_errors: true)
              unless ['COMPLETED', 'SHIPPED'].member?(lib.status)
                lot_cleanup(lot)
                backup_prepare_send_receive(lot, backup_sv, params)  # ensure lot is at the proper op and nonpro bank
                backup_complete(lot, backup_sv)
              end
            # else
            # end
          end
        else
          if (lib.product != li.product) || (lib.route != lib.backup.src.route)
            route = lib.backup.src.route
            if route.end_with?('##')  # fix for (outdated) active routes
              route = ''
              opNo = nil
            else
              opNo = backup_sv.route_operations(route, count: 1).first.operationNumber  # ??
            end
            backup_sv.schdl_change(lot, product: li.product, route: route, opNo: opNo)
          end
          backup_sv.lot_cleanup(lot, opNo: :last) unless ['SCRAPPED', 'SHIPPED'].member?(lib.status)
          backup_lot_status_rpt(lot, lotfamily: lib.family) if lib.status != 'COMPLETED'  # && li.status == 'COMPLETED ?
          # backup_lot_status_rpt(lot) unless (lib.family == lib.lot) && (lib.status == 'COMPLETED')  # && li.status == 'COMPLETED ?
          # backup_lot_status_rpt(lot)
          # backup_sv.delete_lot_family(lot)
        end
      end
    else
      # lib = backup_sv.lot_info(lot, backup: true)
      lib = backup_sv.lot_list(lot: lot, lotinfo: true).first
      unless lib.nil? || ['COMPLETED', 'SHIPPED'].member?(lib.status)
      # if lib.status == 'NonProBank'
        lot_cleanup(lot)
        backup_prepare_send_receive(lot, backup_sv, params)  # ensure lot is at the proper op and in the nonpro bank
        backup_complete(lot, backup_sv)
      end
    end
    if li.family == li.lot  # TODO: needs refinement
      backup_sv.delete_lot_family(lot)
      delete_lot_family(lot)
    end
    slept = false
    if backup_sv.lot_exists?(lot)
      sleep 40
      slept = true
      backup_sv.lot_delete(lot)
    end
    if lot_exists?(lot)
      sleep 40 unless slept
      lot_delete(lot)
    end
  end

end
