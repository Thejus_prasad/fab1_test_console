=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.
=end
    

# area and stocker methods
class SiView::MM

  # return Array of workareas
  def workarea_list(params={})
    res = @manager.TxWorkAreaListInq(@_user)
    return nil if rc(res) != 0
    return res if params[:raw]
    return res.strWorkArea.collect {|w| w.workArea.identifier}.sort
  end

  # return the equipments and stockers in a workarea
  def workarea_eqp_list(workarea)
    p = @jcscode.pptEqpListInqInParm_struct.new(oid(workarea), '', oid(''), [].to_java(:string), '', any)
    #
    res = @manager.TxEqpListInq__100(@_user, p)
    return nil if rc(res) != 0
    return res
  end

  # return stocker_info struct or pptStockerInfoInqResult_struct if raw
  def stocker_info(stocker, params={})
    res = @manager.TxStockerInfoInq__100(@_user, oid(stocker), !!params[:detailFlag])
    return nil if rc(res) != 0
    return res
  end
  
  # return 0 on success
  def stocker_inventory_upload(stocker, params={})
		res = @manager.TxStockerInventoryReq(@_user, oid(stocker), '')
    return rc(res)
  end
  
  # # return 0 on success - unused
  # def stocker_status_change_rpt(stocker, status, params={})
  #   res = @manager.TxStockerStatusChangeRpt(@_user, oid(stocker), oid(status), params[:memo] || '')
  #   return rc(res)
  # end
  
  # # return bare reticle stocker information - unused
  # def brs_info(brs, params={})
  #   res = @manager.TxBareReticleStockerInfoInq(@_user, oid(brs))
  #   return nil if rc(res) != 0
  #   return res if params[:raw]
  #   rports = res.strReticlePodPortIDs.collect {|rport|  
  #     EqpReticlePort.new(rport.reticlePodPortID.identifier, rport.accessMode, rport.portStatus, 
  #       rport.loadedReticlePodID.identifier, rport.reservedReticlePodID.identifier)
  #   }
  #   reticles = res.strStoredReticles.collect {|r| r.reticleID.identifier}
  #   return BRSInfo.new(brs, res.e10Status, res.reticleStoreMaxCount, res.onlineMode, rports, reticles)
  # end

  # # mode: Off-Line, On-Line Remote - unused
  # #
  # # notify: notify TCS, defaults to true
  # #
  # # return 0 on success    
  # def brs_mode_change(brs, mode, params={})    
  #   notifyTCS = ((params[:notifyTCS] || params[:notify]) != false)
  #   memo = params[:memo] || ''
  #   $log.info "brs_mode_change #{brs.inspect}, #{mode.inspect}, notify: #{notifyTCS}"
  #   #
  #   res = @manager.TxBareReticleStockerOnlineModeChangeReq(@_user, oid(brs), mode, notifyTCS, memo)
  #   return rc(res)
  # end

end
