=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.
=end


class SiView::MM

  # helper method to create the wafer map for wafer_sort and wafer_sort_rpt
  # The parameter slot can be
  #   1) an array of target slot numbers for each wafer
  #   2) 'empty' to find empty slots in the tgt carrier
  #   3) nil to use the slot numbers in the source carrier for the target, or
  #      slots starting from 1 if the source carrier is unmanaged
  #
  # return array of pptWaferTransfer_struct or nil on error
  def _create_slotmap(lotID, tgtcarrierID, params={})
    tgt_managed = !['FOSB', ''].member?(tgtcarrierID.identifier)
    #
    liraw = lots_info_raw(lotID, wafers: true) || return
    strLotInfo = liraw.strLotInfo.first
    lotID = strLotInfo.strLotBasicInfo.lotID
    srccarrierID = strLotInfo.strLotLocationInfo.cassetteID
    src_managed = !srccarrierID.identifier.empty?
    srccarrierID.identifier = 'FOSB' unless src_managed   # TODO: necessary?
    _sc = "from #{srccarrierID.identifier.inspect}#{' (unmanaged)' unless src_managed}"
    _tc = "to #{tgtcarrierID.identifier.inspect}#{' (unmanaged)' unless tgt_managed}"
    $log.info "  transfer wafers of lot #{lotID.identifier} #{_sc} #{_tc}"
    #
    # regular lot in carrier or FOSB
    wafers = strLotInfo.strLotWaferAttributes
    # scrapped lot in carrier (all slots listed, select)
    if wafers.empty?
      wafers = liraw.strWaferMapInCassetteInfo.select {|e| e.lotID.identifier == lotID.identifier}
    end
    # scrapped lot in FOSB
    wafers = waferlist_lotfamily(lotID) if wafers.empty?
    #
    tgtslots = params[:slots]
    if tgtslots == 'empty'
      if tgt_managed
        tgtslots = []
        lot_list_incassette(tgtcarrierID, raw: true).strWaferMapInCassetteInfo.each {|w|
          tgtslots << w.slotNumber if w.waferID.identifier.empty?
          break if tgtslots.size == wafers.size
        }
        if tgtslots.size < wafers.size
          $log.warn "not enough target slots, need #{wafers.size}, got #{tgtslots}"
          return
        end
      else
        tgtslots = (1..wafers.size).to_a
      end
    end
    #
    return wafers.to_enum.each_with_index.collect {|w, i|
      if tgtslots.nil?
        tgtslot = src_managed ? w.slotNumber : i + 1
      else
        tgtslot = tgtslots[i]
      end
      srcslot = src_managed ? w.slotNumber : i + 1
      @jcscode.pptWaferTransfer_struct.new(w.waferID,
        tgtcarrierID, tgt_managed, tgtslot, srccarrierID, src_managed, srcslot, any)
    }
  end

  # return strWaferListInLotFamilyInfoSeq of the lot family, currently for _create_slotmap only
  def waferlist_lotfamily(lotID)
    res = @manager.TxWaferListInLotFamilyInq(@_user, lotID)
    return if rc(res) != 0
    return res.strWaferListInLotFamilyInfoSeq
  end

  # move the lot's wafers into a new carrier (FOSB if ''), starting at the given slot, return 0 on success
  def wafer_sort(lot, tgt_carrier, params={})
    lotID = lot.instance_of?(String) ? oid(lot) : lot
    tgtcarrierID = tgt_carrier.instance_of?(String) ? oid(tgt_carrier) : tgt_carrier
    $log.info "wafer_sort #{lotID.identifier.inspect}, #{tgtcarrierID.identifier.inspect}"
    slots = params[:slots]
    eqp = params[:eqp] || ''
    notify = !!params[:notify]
    memo = params[:memo] || ''
    slotmap = params[:slotmap] || _create_slotmap(lotID, tgtcarrierID, slots: slots) || return
    $log.info "  slots: #{slots}" if slots
    $log.info "  using eqp #{eqp.inspect}" unless eqp.empty?
    #
    res = @manager.TxWaferSortReq(@_user, oid(eqp), slotmap, notify, memo)
    return rc(res)
  end

  # report a wafer sort, typically from EI to SiView, return 0 on success
  def wafer_sort_rpt(lot, tgt_carrier, params={})
    lotID = lot.instance_of?(String) ? oid(lot) : lot
    tgtcarrierID = tgt_carrier.instance_of?(String) ? oid(tgt_carrier) : tgt_carrier
    $log.info "wafer_sort_rpt #{lotID.identifier.inspect}, #{tgtcarrierID.identifier.inspect}"
    slots = params[:slots]
    eqp = params[:eqp] || ''
    memo = params[:memo] || ''
    slotmap = params[:slotmap] || _create_slotmap(lotID, tgtcarrierID, slots: slots) || return
    $log.info "  slots: #{slots}" if slots
    $log.info "  using eqp #{eqp.inspect}" unless eqp.empty?
    #
    res = @manager.TxWaferSortRpt(@_user, oid(eqp), slotmap, memo)
    return rc(res)
  end

  # set the wafer aliases, return 0 on success
  def wafer_alias_set(lot, params={})
    aliases = params[:aliases]
    aliases = [aliases] if aliases.instance_of?(String)
    memo = params[:memo] || ''
    #
    i = 0
    wafer_aliases = lot_info(lot, params.merge(wafers: true)).wafers.collect {|w|
      a = aliases ? aliases[i].to_s : "%2.2d" % w.slot
      res = @jcscode.pptAliasWaferName_struct.new(oid(w.wafer), a, any)
      i += 1
      res
    }.to_java(@jcscode.pptAliasWaferName_struct)
    $log.info "wafer_alias_set #{lot}, #{wafer_aliases.count.inspect}"
    #
    res = @manager.TxAliasWaferNameSetReq(@_user, oid(lot), wafer_aliases, memo)
    return rc(res)
  end

  # return 0 on success
  def scrap_wafers(lot, params={})
    lotID = lot.instance_of?(String) ? oid(lot) : lot
    $log.info "scrap_wafers #{lotID.identifier.inspect}"
    $log.debug {params.inspect}
    memo = params[:memo] || ''
    reason = params[:reason]
    cinfo = code_list('WaferScrap').find {|e|
      cid = e.code.identifier
      (cid == reason) || (reason.nil? && %w(ALL MISC ENDE ZCME CAAA).include?(cid))
    } || ($log.warn '  invalid reason code'; return)
    reasonID = cinfo.code
    #
    reasonopNo = params[:reasonopNo]
    if reasonopNo
      oinfo = lot_operation_list(lotID, forward: false, history: true, raw: true).find {|e|
        e.operationNumber == reasonopNo
      } || ($log.warn "  invalid reasonopNo: #{reasonopNo.inspect}"; return)
    else
      oinfo = @jcscode.pptOperationNameAttributes_struct.new
      oinfo.routeID = oid('')
      oinfo.operationID = oid('')
    end
    $log.info "  reason: #{reasonID.identifier.inspect}, reasonopNo: #{oinfo.operationNumber.inspect}"
    liraw = lots_info_raw(lotID, wafers: true) || return
    strLotInfo = liraw.strLotInfo.first
    lotID = strLotInfo.strLotBasicInfo.lotID
    carrierID = strLotInfo.strLotLocationInfo.cassetteID
    ww = strLotInfo.strLotWaferAttributes.collect {|w|
      # TODO: select by w.slotNumber (was for PDWH only, currently unused)
      @jcscode.pptScrapWafers_struct.new(w.waferID, reasonID, any)
    }
    #
    res = @manager.TxScrapWaferReq(@_user, lotID, carrierID,
      oinfo.routeID, oinfo.operationID, oinfo.operationNumber, oinfo.operationPass, oinfo.objrefPO, ww, memo)
    return rc(res)
  end

  # note: lot MUST be in a carrier else rc 1922 is returned; return 0 on success
  def scrap_wafers_cancel(lot, params={})
    reasonopNo = params[:reasonopNo] || ''  # struct field name: scrapTimestamp (??)
    memo = params[:memo] || ''
    reason = params[:reason]
    cinfo = code_list('WaferScrapCancel').find {|e|
      cid = e.code.identifier
      (cid == reason) || reason.nil? && %w(ALL ENDE EVAL MISC).include?(cid)
    }
    ($log.warn 'wrong reason code'; return) unless cinfo
    reasonID = cinfo.code
    liraw = lots_info_raw(lot, wafers: true) || return
    strLotInfo = liraw.strLotInfo.first
    lotID = strLotInfo.strLotBasicInfo.lotID
    carrierID = strLotInfo.strLotLocationInfo.cassetteID
    ww = []
    liraw.strWaferMapInCassetteInfo.collect {|w|
      next if w.lotID.identifier != lotID.identifier
      ww << @jcscode.pptScrapCancelWafers_struct.new(w.waferID, w.slotNumber, reasonID, w.lotID, reasonopNo, any)
    }
    $log.info "scrap_wafers_cancel #{lotID.identifier.inspect}, reason: #{reasonID.identifier.inspect}"
    #
    res = @manager.TxScrapWaferCancelReq(@_user, lotID, carrierID, ww, memo)
    return rc(res)
  end

  # return the lot the wafer belongs to (after an STB), for JCAP_TestWaferGrading
  def lot_by_waferid(wafer)
    res = @manager.TxLotInfoByWaferInq(@_user, oid(wafer))
    return nil if rc(res) != 0
    return res.lotID.identifier
  end

end
