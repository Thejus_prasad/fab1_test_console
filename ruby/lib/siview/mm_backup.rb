=begin
(c) Copyright 2012 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.
=end

class SiView::MM
  # backup operation definitions. the returned list can be filtered by the given values. UNUSED
  def backup_channels(params={})
    level = params[:level] || ''
    category = params[:name] || ''
    route = params[:route] || ''
    opNo = params[:opNo] || ''
    backup_addr = @jcscode.pptBackupAddress_struct.new('', '', '', any)
    backup_proc = @jcscode.pptBackupProcess_struct.new('', '', '', '', any)
    uc = @jcscode.pptBackupUniqueChannel_struct.new(level, category, route, opNo, backup_addr, backup_proc, any)
    #
    res = @manager.TxBackupChannelListInq(@_user, uc)
    return nil if rc(res) != 0
    return res.strBackupChannelSeq
  end

  # return strBackupChannelSeq or nil, used internally
  def backup_destination(lot, params={})
    res = @manager.TxBackupDestinationInfoInq(@_user, oid(lot))
    return nil if rc(res) != 0
    return res.strBackupChannelSeq
  end

  # return strLotTableInfoSeq or nil
  def backup_send_requests
    res = @manager.TxBackupSendRequestListInq(@_user)
    return nil if rc(res) != 0
    return res.strLotTableInfoSeq
  end

  # return strLotTableInfoSeq or nil
  def backup_return_requests
    res = @manager.TxBackupReturnRequestListInq(@_user)
    return nil if rc(res) != 0
    return res.strLotTableInfoSeq
  end

  def backup_lot_info(lot)
    res = @manager.TxBackupLotInfoInq(@_user, oid(lot))
    return nil if rc(res) != 0
    return res
  end

  # # Suffix change for a split, needs to be done on source and backup site, UNUSED
  # def backup_split_suffix_change(lot, suffix, params={})
  #   $log.info "backup_split_suffix_change #{lot.inspect}, #{suffix.inspect} #{params.inspect}"
  #   memo = params[:memo] || ''
  #   #
  #   res = @manager.TxBackupSplitSuffixChangeReq(@_user, oid(lot), suffix, memo)
  #   return rc(res)
  # end

  # can be inquired for source or backup site, return Array of strOperationNameAttributes (for Test700_BackupOperation#test61 only)
  def backup_lot_operation_list(lot, opNo, other_sv, params={})
    searchcount = params[:searchcount] || 9999
    ops = lot_operation_list(lot, forward: false, history: false, raw: true)
    opdata = ops.find {|e| e.operationNumber == opNo} || ($log.error "no operation for #{opNo}"; return)
    ($log.error "no backup operation for #{opNo}"; return) if opdata.strProcessBackupDataSeq.empty?
    srcaddr = @jcscode.pptBackupAddress_struct.new(@hostname, @servername, @port.to_s, any)
    #
    res = other_sv.manager.TxBackupLotOperationListInq(@_user, searchcount, oid(lot), opdata.strProcessBackupDataSeq.first, srcaddr)
    return nil if other_sv.rc(res) != 0
    return res.strOperationNameAttributes
  end


  # co-ordinated actions at source and backup site

  # prepare sending the lot, return 0 on success
  def backupop_lot_send(lot, backup_sv, params={})
    $log.info "backupop_lot_send #{lot.inspect}, #{backup_sv.inspect}, #{params}"
    memo = params[:memo] || ''
    ch = backup_destination(lot).find {|e| e.strBackupUniqueChannel.strBackupAddress.serverName == backup_sv.servername}
    ($log.warn 'no backup channel'; return) unless ch
    uc = ch.strBackupUniqueChannel
    srcproc = @jcscode.pptBackupProcess_struct.new(uc.routeID, uc.operationNumber, uc.routeID, ch.returnOperationNumber, any)
    #
    res = @manager.TxBackupStateChangeReq(@_user, 'Send', oid(lot), uc.strBackupAddress, uc.strBackupProcess, srcproc, memo)
    return nil if rc(res) != 0
    #
    binfo = backup_lot_info(lot) || return
    srcaddr = @jcscode.pptBackupAddress_struct.new(@hostname, @servername, @port.to_s, any)
    #
    res = backup_sv.manager.TxBackupLotSendReq(@_user, oid(lot), binfo.strLotDBInfo, srcaddr, uc.strBackupProcess, memo)
    return backup_sv.rc(res)
  end

  # cancel sending the lot, return 0 on success
  def backupop_lot_send_cancel(lot, backup_sv, params={})
    $log.info "backupop_lot_send_cancel #{lot.inspect}, #{backup_sv.inspect}, #{params}"
    memo = params[:memo] || ''
    liraw = lots_info_raw(lot, backup: true)
    uc = liraw.strLotInfo.first.strLotBackupInfo.strLotBackupDestDataSeq.find {|e| e.backupState == 'Send'}
    ($log.warn 'no backup send data'; return) unless uc
    srcproc = @jcscode.pptBackupProcess_struct.new('', '', '', '', any)
    #
    res = @manager.TxBackupStateChangeReq(@_user, 'SendCancel', oid(lot), uc.strBackupAddress, uc.strBackupProcess, srcproc, memo)
    return nil if rc(res) != 0
    #
    srcaddr = @jcscode.pptBackupAddress_struct.new(@hostname, @servername, @port.to_s, any)
    #
    res = backup_sv.manager.TxBackupLotSendCancelReq(@_user, oid(lot), srcaddr, uc.strBackupProcess, memo)
    return backup_sv.rc(res)
  end

  # receive a lot at the backup site, return 0 on success
  def backupop_lot_send_receive(lot, source_sv, params={})
    $log.info "backupop_lot_send_receive #{lot.inspect} #{source_sv.inspect} #{params}"
    memo = params[:memo] || ''
    action = params[:recover] ? 'ReceiveCancelForSend' : 'ReceiveForSend'
    srq = backup_send_requests.find {|e| e.lotID.identifier == lot}
    ($log.warn 'no send request'; return) unless srq
    uc = srq.strLotBackupDestDataSeq.last
    srcproc = srq.strLotBackupSourceDataSeq.last.strBackupProcess
    #
    res = source_sv.manager.TxBackupStateChangeReq(@_user, action, srq.lotID, uc.strBackupAddress, uc.strBackupProcess, srcproc, memo)
    return nil if source_sv.rc(res) != 0
    return source_sv.rc if params[:recover]
    #
    # map wafers to empty slots
    carrier = _get_empty_carrier(params[:bak_carrier] || '%', params)
    carrierID = carrier.instance_of?(String) ? oid(carrier) : carrier
    $log.info "  using carrier #{carrierID.identifier.inspect}"
    slots = lot_list_incassette(carrierID, raw: true).strWaferMapInCassetteInfo.select {|w| w.waferID.identifier.empty?}
    wafers = @manager.TxBackupSendRequestWaferInq(@_user, oid(lot)).strWaferTableInfoSeq.collect {|w|
      slot = slots.pop || ($log.warn "not enough empty slots in #{carrierID.identifier}"; return)
      @jcscode.pptWafer_struct.new(w.waferID, slot.slotNumber, w.siInfo)
    }.to_java(@jcscode.pptWafer_struct)
    rstruct = @jcscode.pptBackupReceiveRequest_struct.new(srq.lotID, carrierID, wafers, srq.prodSpecID, any)
    #
    res = @manager.TxBackupSendRequestReceiveReq(@_user, rstruct, memo)
    return rc(res)
  end

  # return a lot to the source site or cancel return request (recover: true), return 0 on success
  def backupop_lot_return(lot, source_sv, params={})
    $log.info "backupop_lot_return #{lot.inspect} #{source_sv.inspect} #{params.inspect}"
    memo = params[:memo] || ''
    action = params[:recover] ? 'ReturnCancel' : 'Return'
    liraw = lots_info_raw(lot, backup: true)
    bcki = liraw.strLotInfo.first.strLotBackupInfo
    bckproc = bcki.strLotBackupSourceDataSeq.last.strBackupProcess
    srcproc = @jcscode.pptBackupProcess_struct.new('', '', '', '', any)
    #
    res = @manager.TxBackupStateChangeReq(@_user, action, oid(lot), bcki.strBornSiteAddress, bckproc, srcproc, memo)
    return nil if rc(res) != 0
    return rc if params[:recover]
    #
    binfo = backup_lot_info(lot) || return
    bckaddr = @jcscode.pptBackupAddress_struct.new(@hostname, @servername, @port.to_s, any)
    #
    res = source_sv.manager.TxBackupLotReturnReq(@_user, oid(lot), binfo.strLotDBInfo, bckaddr, srcproc, memo)
    return source_sv.rc(res)
  end

  # receive a lot at the source site, return 0 on success
  def backupop_lot_return_receive(lot, backup_sv, params={})
    $log.info("backupop_lot_return_receive #{lot.inspect}, #{backup_sv.inspect}, #{params.inspect}")
    memo = params[:memo] || ''
    action = params[:recover] ? 'ReceiveCancelForReturn' : 'ReceiveForReturn'
    rrq = backup_return_requests.find {|e| e.lotID.identifier == lot}
    ($log.warn 'no return request'; return) unless rrq
    source_addr = rrq.strBornSiteAddress
    source_proc = rrq.strLotBackupSourceDataSeq.last.strBackupProcess
    backup_proc = rrq.strLotBackupDestDataSeq.last.strBackupProcess
    #
    res = backup_sv.manager.TxBackupStateChangeReq(@_user, action, oid(lot), source_addr, backup_proc, source_proc, memo)
    unless params[:ignore_errors]  # desperately cleaning up
      return nil if backup_sv.rc(res) != 0
      return backup_sv.rc if params[:recover]
    end
    #
    # map wafers to empty slots
    carrier = _get_empty_carrier(params[:src_carrier] || '%', params)
    carrierID = carrier.instance_of?(String) ? oid(carrier) : carrier
    $log.info "  using carrier #{carrierID.identifier.inspect}"
    slots = lot_list_incassette(carrierID, raw: true).strWaferMapInCassetteInfo.select {|w| w.waferID.identifier.empty?}
    wafers = @manager.TxBackupReturnRequestWaferInq(@_user, oid(lot)).strWaferTableInfoSeq.collect {|w|
      slot = slots.pop || ($log.warn "not enough empty slots in #{carrierID.identifier}"; return)
      @jcscode.pptWafer_struct.new(w.waferID, slot.slotNumber, w.siInfo)
    }.to_java(@jcscode.pptWafer_struct)
    rstruct = @jcscode.pptBackupReceiveRequest_struct.new(rrq.lotID, carrierID, wafers, rrq.prodSpecID, any)
    #
    res = @manager.TxBackupReturnRequestReceiveReq(@_user, rstruct, memo)
    return nil if rc(res) != 0
    #
    res = backup_sv.manager.TxBackupLotSendCancelReq(@_user, oid(lot), source_addr, backup_proc, memo)
    return backup_sv.rc(res)
  end

  # # Suffix change for a split, needs to be done on source and backup site. UNUSED
  # def backupop_split_suffix_change(lot, suffix, backup_sv, params={})
  #   memo = params[:memo] || ''
  #   res = @manager.TxBackupSplitSuffixChangeReq(@_user, oid(lot), suffix, memo)
  #   return nil if rc(res) != 0
  #   #
  #   res = backup_sv.manager.TxBackupSplitSuffixChangeReq(@_user, oid(lot), suffix, memo)
  #   return backup_sv.rc(res)
  # end


  # Sync lot family for selected or all sites, site is determined by sv properties, return 0 on success
  def backup_lot_family_sync(lotfamily, backup_sv=nil, params={})
    memo = params[:memo] || ''
    $log.info "backup_lot_family_sync #{lotfamily.inspect}"
    if backup_sv
      backup_addr = @jcscode.pptBackupAddress_struct.new(backup_sv.hostname, backup_sv.servername, backup_sv.port.to_s, any)
      res = @manager.CS_TxBackupLotFamilySyncReq(@_user, oid(lotfamily), backup_addr, memo)
    else
      res = @manager.CS_TxBackupLotFamilySyncAllSitesReq(@_user, oid(lotfamily), memo)
    end
    return rc(res)
  end

  # Report lot status (default: Completed) at source site
  def backup_lot_status_rpt(lot, params={})
    lotfamily = params[:lotfamily] || lot_info(lot).family
    status = params[:status] || 'Completed'
    memo = params[:memo] || ''
    $log.info "backup_lot_status_rpt #{lot.inspect}, lotfamily: #{lotfamily.inspect}, status: #{status.inspect}"
    #
    res = @manager.CS_TxBackupLotStatusRpt(@_user, oid(lotfamily), oid(lot), status, memo)
    return rc(res)
  end

  # # get lot family at all sites, UNUSED
  # def cs_lot_family(lot, params={})
  #   wafer_info = !!params[:wafers]
  #   db_info = !!params[:db_info]
  #   lotfamily = params[:lotfamily] || ''
  #   #
  #   res = @manager.CS_TxLotFamilyInq(@_user, oid(lot), oid(lotfamily), wafer_info, db_info)
  #   return nil if rc(res) != 0
  #   return _extract_lotlist_attrs(res.strLotListAttributes, wafers: params[:wafers], backup: true)
  # end

end
