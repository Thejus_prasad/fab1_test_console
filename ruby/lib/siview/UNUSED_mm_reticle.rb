=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.
=end

class SiView::MM
  WhereNextPod = Struct.new(:reticle_stocker, :bare_reticle_stocker, :resource, :equipment, :port)
  ReticlePodXferJob = Struct.new(:job_id, :status, :pod, :frommachine, :fromport, :tostocker_group, :tomachine, :toport)
  ReticleComponentJob = Struct.new(:job_id, :cjob_id, :status, :rc, :reason)
  ReticleDispatchJob = Struct.new(:station, :req_time, :reticle, :pod, :job_id, :from_eqp, :from_cat, :to_eqp, :to_cat, :priority, :user, :status, :chg_time, :err_info)

  # return stocker_info struct or pptStockerInfoInqResult_struct if raw
  def reticle_stocker_info(stocker, params={})
    res = @manager.TxReticlePodStockerInfoInq(@_user, oid(stocker))
    return nil if rc != 0
    return res if params[:raw]
    cc = res.strReticlePodInfoInStocker.collect {|c| c.reticlePodID.identifier}
    return StockerInfo.new(res.stockerType, cc)
  end

  # create a new reticle transfer, return 0 on success -- unused
  def reticle_pod_xfer_create(pod, from_machine, from_port, to_machine, to_port, params={})    
    memo = params[:memo] || ''
    $log.info "reticle_pod_xfer_create '#{pod}', '#{from_machine}', '#{from_port}', '#{to_machine}', '#{to_port}'"
    #
    res = @manager.TxReticlePodXferJobCreateReq(@_user, oid(pod), oid(from_machine), oid(from_port), oid(to_machine), oid(to_port), memo)
    return rc(res)
  end
  
  # unclamp and create a new reticle transfer, return 0 on success -- unused
  def reticle_pod_unclamp_xfer_create(pod, from_machine, from_port, to_machine, to_port, params={})    
    memo = params[:memo] || ''
    $log.info "reticle_pod_unclamp_xfer_create '#{pod}', '#{from_machine}', '#{from_port}', '#{to_machine}', '#{to_port}'"
    #
    res = @manager.TxReticlePodUnclampAndXferJobCreateReq(@_user, oid(pod), oid(from_machine), oid(from_port), oid(to_machine), oid(to_port), memo)
    return rc(res)
  end

  # transfer reticle to stocker, return 0 on success -- unused
  def async_reticle_xfer_create(reticle, pod, to_machine, params={})
    memo = params[:memo] || ''
    $log.info "async_reticle_xfer_create '#{reticle}', '#{pod}', '#{to_machine}'"
    #
    res = @manager.TxAsyncReticleXferJobCreateReq(@_user, oid(reticle), oid(pod), oid(to_machine), memo)
    return rc(res)
  end

  # return array of transfer job names or nil -- unused
  def reticle_pod_xfer_jobs(params={})
    pod = params[:pod] || ''
    from_eqp = params[:from_eqp] || ''
    from_stocker = params[:from_stocker] || ''
    to_eqp = params[:to_eqp] || ''
    to_stocker = params[:to_stocker] || ''
    detail_flag = (params[:detailFlag] != false)
    #
    res = @manager.TxReticlePodXferJobListInq(@_user, oid(pod), oid(to_eqp), oid(to_stocker), oid(from_eqp), oid(from_stocker), detail_flag)
    return nil if rc(res) != 0
    return res if params[:raw]
    return res.strReticlePodXferJobs.collect {|j|
      rjobs = j.strReticlePodJob.collect {|rji|
        ReticlePodXferJob.new(rji.reticlePodJobID, rji.reticlePodJobStatus, rji.reticlePodID.identifier,
          rji.fromMachineID.identifier, rji.fromPortID.identifier, rji.toStockerGroup, rji.toMachineID.identifier, rji.toPortID.identifier
        )
      }
      XferJob.new(j.jobID, j.jobStatus, j.transportType, rjobs)
    }
  end

  # unused
  def reticle_pod_where_next(pod, params={})
    res = @manager.TxWhereNextForReticlePodInq(@_user, oid(pod))
    return nil if rc(res) != 0
    return res if params[:raw]
    return res.strNextMachineForReticlePod.collect {|r|
      WhereNextPod.new(r.reticlePodStockerID.identifier, r.bareReticleStockerID.identifier,
        r.resourceID.identifier, r.equipmentID.identifier, r.portID.identifier)
    }
  end
  
  # Reticle dispatch job creation (like reticle dispatch watchdog) - unused
  def reticle_dispatch_job_insert(reticle, pod, from, to, params={})
    from_cat = params[:from_cat] || ''
    to_cat = params[:to_cat] || ''
    priority = params[:priority] || 1
    memo = params[:memo] || ''
    time = siview_timestamp(Time.now)
    $log.info "reticle_dispatch_job_insert #{reticle}, port #{pod}, #{from.inspect} -> #{to.inspect}"
    job = @jcscode.pptReticleDispatchJob_struct.new(to, siview_timestamp(Time.now),
        oid(reticle), oid(pod), time, oid(from), from_cat, oid(to), to_cat, priority, @_user.userID, "Created", time, any)
    #
    res = @manager.TxReticleDispatchJobInsertReq(@_user, job, memo)
    return rc(res)
  end

  # unused
  def reticle_dispatch_job_delete(rdj_id, params={})
    memo = params[:memo] || ''
    res = @manager.TxReticleDispatchJobDeleteReq(@_user, rdj_id, memo)
    return rc(res)
  end

  # unused
  def reticle_dispatch_job_cancel(rdj_id, params={})
    memo = params[:memo] || ''
    res = @manager.TxReticleDispatchJobCancelReq(@_user, rdj_id, memo)
    return rc(res)
  end
  
  # all component jobs for a specific dispatch job - unused
  def reticle_component_job_list(job, params={})
    res = @manager.TxReticleComponentJobListInq(@_user, job)
    return nil unless rc(res) == 0
    rcjs = res.strReticleComponentJobSeq.map {|rcj|
      _any = extract_si(rcj.siInfo)
      if _any
        ReticleComponentJob.new(rcj.reticleDispatchJobID, rcj.reticleComponentJobID, rcj.jobStatus, _any.returnCode, _any.reasonText)
      else
        ReticleComponentJob.new(rcj.reticleDispatchJobID, rcj.reticleComponentJobID, rcj.jobStatus)
      end
    }
    return rcjs
  end
  
  # unused
  def reticle_component_job_skip(rdj, rcj, params={})
    memo = params[:memo] || ''
    res = @manager.TxReticleComponentJobSkipReq(@_user, rdj, rcj, memo)
    return nil unless rc(res) == 0
    return rc(res)
  end

  # all reticle dispatch jobs - unused
  # support filtering by :pod, :reticle or :eqp etc.
  def reticle_dispatch_job_list(params={})
    if params[:eqp]
      res = @manager.CS_TxReticleDispatchJobListByEQPInq(@_user, oid(params[:eqp]))
    else
      res = @manager.TxReticleDispatchJobListInq(@_user)
    end
    return nil unless rc(res) == 0
    rdjs = res.strReticleDispatchJobSeq.map {|rdj|
      _any = extract_si(rdj.siInfo)
      ReticleDispatchJob.new(rdj.dispatchStationID, rdj.requestedTimestamp, rdj.reticleID.identifier, rdj.reticlePodID.identifier,
        rdj.reticleDispatchJobID, rdj.fromEquipmentID.identifier, rdj.fromEquipmentCategory, rdj.toEquipmentID.identifier, rdj.toEquipmentCategory,
        rdj.priority, rdj.requestUserID.identifier, rdj.jobStatus, rdj.jobStatusChangeTimestamp, _any)
    }
    rdjs = rdjs.select {|r| r.reticle == params[:reticle]} if params[:reticle]
    rdjs = rdjs.select {|r| r.pod == params[:pod]} if params[:pod]
    rdjs = rdjs.select {|r| r.status == params[:status]} if params[:status]
    return rdjs
  end

  # unused
  def reticle_dispatch_component_job_change(rdj_id, rcj_id, reticle, pod, from_eqp, from_port, to_eqp, to_port, params={})
    job_name = params[:job_name] || ''
    memo = params[:memo] || ''
    res = @manager.TxReticleDispatchAndComponentJobStatusChangeReq(@_user, false, rdj_id, rcj_id, job_name,
      oid(reticle), oid(pod), oid(from_eqp), oid(from_port), oid(to_eqp), oid(to_port), memo)
    return rc(res)
  end

  # Inventory of reticle pod stocker -- unused
  def reticle_pod_inventory_upload(stocker, params={})    
    memo = params[:memo] || ''
    res = @manager.TxReticlePodInventoryReq(@_user, oid(''), oid(stocker), memo)
    return rc(res)
  end
  
  # unused
  def reticle_component_job_retry(rdj, rcj, params={})
    memo = params[:memo] || ''
    res = @manager.TxReticleComponentJobRetryReq(@_user, rdj, rcj, memo)
    return rc(res)
  end
  
  # Retry existing reticle dispatch job (i.e. in error) -- unused
  def reticle_dispatch_job_retry(job, params={})
    memo = params[:memo] || ''
    res = @manager.CS_TxReticleDispatchJobRetryReq(@_user, job, memo)
    return rc(res)
  end
  
end
