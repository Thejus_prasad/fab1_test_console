=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: Steffen Steidten, 2009-05-03

History:
  2009-09-04 ssteidte, changed id() to oid() to avoid a name clash
  2011-08-01 ssteidte, moved auxillary methods to misc.rb
  2011-08-16 ssteidte, moved easysiview_*.rb files to an "easysiview" subdirectory
  2012-05-10 ssteidte, load JCS files during class instantiation
  2012-06-30 ssteidte, support for AsmView and SiView6 for Fab7
  2013-08-20 ssteidte, moved parse_ior to a seaparate class, improved parsing
  2013-12-18 ssteidte, refactored EasySiViewR, SMFacade, XM, AsmViewR and Fab7 SiView
  2014-11-03 ssteidte, remove constants first for a clean re-load
  2015-02-17 ssteidte, added pbuddara's code to resolve the actually used Tx as actual_txname
  2015-03-17 ssteidte, remove cs_manager, all access goes through @manager
  2015-08-31 ssteidte, preferrably use properties from etc/siview.conf
  2017-07-20 sfrieske, dropped support for old etc/connections/<env>.properties
  2020-05-19 sfrieske, dropped support for R8 (minimum is R12 for AsmView)
  2021-12-08 sfrieske, replaced Profile Struct by a Hash
=end


module SiView

  class MM < Client
    # constants.each {|c| remove_const(c)}  # for clean re-load
    Dir["#{File.dirname(__FILE__)}/mm_*rb"].each {|f| load(f)}

    attr_accessor :customized, :release, :f7, :servername, :hostname, :port

    def inspect
      "#<#{self.class.name} #{@env}, user: #{@user}, customized: #{@customized}, R#{@release}>"
    end

    def connect
      super
      # may be set already for AsmView MM, defaults to regular MM client package
      @jcscode ||= com.amd.jcs.siview.code
      # create MM connection
      profile = @iorparser.profiles.first
      $log.info "MM connection: #{profile[:host]}:#{profile[:port]}"
      # for backup operations, can guess hostname and port from IOR
      @servername = @_props[:servername]  # get default value later from environment_variable
      @hostname = @_props[:hostname] || profile[:host]
      @port = @_props[:port] || profile[:port]
      #
      obj = @orb.string_to_object(@iorparser.ior)
      # detect the connection method from the IOR file and connect
      if @iorparser.type_id.include?('PPTServiceManagerObjectFactory')
        $log.info 'creating PPTServiceManagerObjectFactory'
        factory = @jcscode.PPTServiceManagerObjectFactoryHelper.narrow(obj)
        obj = factory.createPPTServiceManager
      end
      # try to create CS_PPTServiceManager, be aware of overrides from AsmView
      @customized = false if @_props[:customized] == false
      if @customized != false
        begin
          $log.info 'creating CS_PPTServiceManager'
          @manager = @jcscode.CS_PPTServiceManagerHelper.narrow(obj)
          @customized = true
        rescue
          @customized = false
          $log.warn '  no CS_PPTServiceManager, customized methods cannot be used'
        end
      end
      unless @customized
        begin
          $log.info 'creating PPTServiceManager'
          @manager = @jcscode.PPTServiceManagerHelper.narrow(obj)
        rescue
          $log.warn "  error creating MM connection\n#{$!}"
        end
      end
      @_user = user_struct(@user, @password, @_props)
      return unless @manager
      # detect MM release from JCS if not passed explicitly
      if @_props.has_key?(:release)
        @release = @_props[:release].to_i
      elsif @manager.respond_to?(:cs_getVersion)
        @release = cs_version.split('.').first.sub('R', '').to_i
      else
        if @manager.respond_to?(:TxLotInfoInq__150)
          @release = 15
        else
          @release = 12
        end
      end
      # get default servername for BackupOperation unless explicitly set
      begin
        @servername ||= environment_variable[1]['CS_MM_SVCMGR_SERVER_NAME']
      rescue
      end
      # set F7 mode
      @f7 = @_props[:f7]
      begin
        @f7 = (environment_variable[1]['CS_SP_FAB_TYPE'] == '2') if @f7.nil?
      rescue
      end
      #
      $log.info self.inspect
    end

    # return Java array of objectIdentifier_struct, TODO: move to SiView (?)
    def _as_objectIDs(objs)
      if objs.instance_of?(Array)
        # Ruby array of strings or Java lotIDs
        return objs.first.instance_of?(String) ? oids(objs) : objs
      elsif objs.instance_of?(@jcscode.objectIdentifier_struct)
        # single objID
        return [objs]
      else
        # single obj either as string or Java array of objIDs
        return objs.instance_of?(String) ? oids([objs]) : objs
      end
    end

    # return the actually used Tx (TxLotInfoInq__xx based on the txname (like TxLotInfoInq)
    def actual_txname(txname, params={})
      txs = []
      vers = []
      cs_txname = '_' + txname
      # find txname in the list of sorted methods, sorting ensures the unversioned tx is found first
      mgr = @manager
      mgr = mgr.manager if mgr.class == SiView::InstrumentedServiceManager
      mgr.methods.sort.each {|m|
        tx = m.to_s
        next unless tx.include?(txname)
        next if tx.include?(cs_txname) && params[:strict] != false  # skip CS_TxName unless strict: false
        txs << tx
        ($log.warn "ambiguous name: #{txname}, found #{txs}"; return) unless tx.include?('__') || tx == txs.first
        vers << tx.split('__').last.to_i  # 0 if unversioned
      }
      # find the highest version that is not bigger than @release
      ver = vers.reverse.find {|v| v <= @release * 10}
      # in special cases when the txname passed was not found in @manager
      ($log.warn "#{txname} not found in JCS"; return txname) if ver.nil?
      return [txs.first, ver.zero? ? '' : '%03d' % ver]
    end

  end

end
