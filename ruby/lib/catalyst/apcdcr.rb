=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.


Author: sfrieske, 2018-06-06

History:
  2021-02-05 sfrieske, use instance_of instead of kind_of
  2021-06-02 sfrieske, set default siteX and siteY per instance
  2021-08-27 sfrieske, require uniquestring instead of misc
=end

require 'time'
require 'util/uniquestring'


module APC

  # Create APC DCRs
  class DCR
    attr_accessor :siteX, :siteY, :li, :context, :wafers, :sites, :wdata, :mdata  # :rdata

    def initialize(li, eqp, params={})
      @siteX = params[:siteX] || %w(-75453.3521 -75453.3521 -125856.3521 -125856.3521
                                    -75453.3521 -75453.3521 -125856.3521 -125856.3521)
      @siteY = params[:siteY] || %w(78694.701 78694.701 -47577.299 -47577.299
                                    78694.701 78694.701 -47577.299 -47577.299)
      @li = li
      @eqp = eqp
      @context = {apcActionType: 'Data', eventType: 'JobDataAvailable',
        eventReceivedTimestamp: Time.now.utc.iso8601, facility: 'Fab36', originatorID: 'CEI',
        controlJobId: li && !li.cj.empty? ? li.cj : unique_string('QA-'),
        equipmentId: eqp, equipmentType: 'Measurement', processingAreaId: params[:chamber] || 'PM1',
        # ceiEquipmentModel: 'XXX', E10state: '2WPR', portOperationMode: 'Auto-1', EIReturnURL: 'http://<host>:3456/dummy',
        # processTypeWfrSelection: 'MS-FM',
      }
      @context.merge!(userID: li.user, carrierID: li.carrier,
        lotID: li.lot, familyLotID: li.family, layer: li.layer, priorityClass: li.priority,
        productID: li.product, productGroupID: li.productgroup, productType: li.content, route: li.route,
        technology: li.technology, maskLevel: li.masklevel,
        operationNumber: li.opNo, operationID: li.op, processDefinition: li.op.split('.').first
      ) if li
      @context.merge!(params[:ctx]) if params.has_key?(:ctx)  # additional context, e.g. {ProcessLayer: @@player}
      @context[:equipmentType] = params[:eqptype] if params.has_key?(:eqptype)  # override 'Measurement'
      ###@context[:reticle] = params[:reticle] if params.has_key?(:reticle)  # need to add reticle section on reticle
      @wafers = []
      @sites = []
      @wdata = {}  # WaferProperties tag
      @mdata = {}  # Metrology tag
      set_wafers(params[:slots]) if params[:slots]
      $log.info self.inspect
    end

    def inspect
      "#<#{self.class.name} eqp: #{@context[:equipmentId]}>"
    end

    # set processed wafers by slot(s)
    def set_wafers(slots)
      slots = [slots] unless slots.instance_of?(Array)
      $log.info "set_wafers #{slots}"
      pa = @context[:processingAreaId]
      @li.wafers.each {|w|
        if slots.member?(w.slot)
          @wafers << w.wafer
          @wdata[w.wafer] = {
            'history'=>"#{w.slot} LTRobotLowerEE #{pa}",
            'locationEnter'=>'10/29/2009 08:49:55 10/29/2009 08:53:30 10/29/2009 08:53:39',
            'locationExit'=>'10/29/2009 08:53:30 10/29/2009 08:53:39 NA',
            'slot'=>w.slot
          }
          @mdata["DataItem#{mdata.length + 1}"] = {
            'logicalName'=>'WaferID', 'processingArea'=>pa,
            'reading'=>1, 'readingsValid'=>true, 'reportingName'=>'WaferID',
            'sequence'=>1, 'valid'=>true, 'value'=>w.wafer, 'waferId'=>w.wafer
          }
        else
          @wdata[w.wafer] = {
            'history'=>"#{w.slot}",
            'locationEnter'=>'10/29/2009 08:49:55',
            'locationExit'=>'NA',
            'slot'=>w.slot
          }
        end
      }
      return @wafers
    end

    # add readings, values are an array of entries per wafer (wafer level if string or number, site level if array)
    def add_readings(readings, params={})
      $log.info "add_readings #{readings}"
      readings.each_pair {|pname, data|
        # data is like {[1,2]=>[[10,11], [12,13]], dieX: [..], dieY: [..]}; 1st key is an array of slots,
        #   following ones are optional symbols for parameters to be passed to newdata like :dieX
        slots = data.keys.first
        values = data[slots]
        ($log.warn "  mismatch: #{values.size} readings for #{slots.size} wafers"; return) if values.size != slots.size
        reading = values.collect {|wv|
          wv.instance_of?(Array) ? (1..wv.size).to_a : 1
        }.flatten
        readingsValid = Array.new(reading.size, true)
        sequence = values.each_with_index.collect {|wv, i|
          wv.instance_of?(Array) ? Array.new(wv.size, i + 1) : i + 1
        }.flatten
        waferId = values.each_with_index.collect {|wv, i|
          wid = @li.wafers[slots[i] - 1].wafer  # slot 1 is at li.wafers' index 0
          wv.instance_of?(Array) ? Array.new(wv.size, wid) : wid
        }.flatten
        newdata = {
          'logicalName'=>"{#{pname}}",
          'reading'=>reading.join(' '), 'readingsValid'=>readingsValid.join(' '),
          'reportingName'=>"{#{pname}}", 'sequence'=>sequence.join(' '),
          'valid'=>true, 'value'=>values.flatten.join(' '),
          'waferId'=>"{#{waferId.join('} {')}}"  # waferId.join(' ')
        }
        if values.first.instance_of?(Array)
          # assuming site level data, default: 2 wafers with 4 sites each
          newdata['dieX'] = data[:dieX].join(' ') if data[:dieX]
          newdata['dieY'] = data[:dieY].join(' ') if data[:dieY]
          newdata['site'] = reading.join(' ')  # same format
          newdata['siteX'] = (data[:siteX] || @siteX).join(' ')
          newdata['siteY'] = (data[:siteY] || @siteY).join(' ')
        end
        @mdata["DataItem#{@mdata.length + 1}"] = newdata
      }
      return @mdata
    end

    # return hash of data ready to be passed to Catalyst#submit
    def data
      # 'Recipe'=>@rdata would go here if required
      return {'Metrology'=>@mdata, 'WaferProperties'=>@wdata}
    end

    # # for verification: get wafers for a parameter reading
    # def pwafers(pname)
    #   dataitem, data = @mdata.find {|k,v| v['reportingName'] == pname}
    #   ($log.warn "DCR has no parameter #{pname.inspect}"; return) unless data
    #   wafers = data['waferId']
    #   ($log.warn "DCR parameter #{pname.inspect} has no waferId part"; return) unless wafers
    #   return wafers.split(' ').uniq.sort
    # end

  end

end
