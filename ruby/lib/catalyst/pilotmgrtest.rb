=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.


Logs ITDC: http://drsapc/apcsetup/cajallog/index.php?fabNumber=36&environment=development
Spec: C07-00003517 (setup.FC: http://vf1sfcd01:10080/setupfc/, view: http://vf1sfcd01:10080/setupfc-rest/v1/specs/C07-00003517/content?format=html)

Author: Steffen Frieske, 2017-11-27

Version: 1.2.3

History:
  2018-07-11 sfrieske, added checks for pilot's lot openotes
  2018-11-29 sfrieske, changed chamberwish expectation for chamber 'empty'
  2021-07-30 sfrieske, moved mq_status_orig from apcsra here
  2021-08-27 sfrieske, split verify_equal off of misc.rb
=end

require 'rqrsp_mq'
require 'jcap/apcsra'
require 'jcap/sjctest'
require 'misc/sfc'
require 'rtdserver/rtdemuremote'
require 'util/verifyequal'
require 'util/waitfor'
require_relative 'pilotmgr'     # FabGUI dialog and EI Data msg
require_relative 'apc'          # for ToolWishes
require_relative 'apcdb'        # for Sampling
require_relative 'apcdcr'       # for pilot completion
require_relative 'apcbaseline'  # for Litho scenarios


module APC

  # PilotManager Testing
  class PilotMgrTest
    attr_accessor :pmgr, :apc, :apcdb, :sfc, :spec, :scenarios, :eqp, :eimq, :ppd, :last_mpd, :holdreason,
      :mqsrclient, :sv, :sjctest, :rtdremote,
      :_dialog  # for dev

    def initialize(env, params={})
      @sjctest = SJC::Test.new(env, params)
      @sv = @sjctest.sv
      @pmgr = APC::PilotManager.new(env, use_http: params[:use_http])
      @apc = APC::Client.new(env, catalyst: @pmgr.catalyst)
      @apcdb = APC::DB.new(env)
      @sfc = SetupFC::Client.new(env)
      @spec = params[:spec] || 'C07-00003517'
      @eqp = params[:eqp] || 'UTPILOTMGR1'
      @eimq = APC::EIMQ.new(env, @eqp)  # for Litho scenarios
      @ppd = 'UTPDPILOT01.01'
      @last_mpd = params[:last_mpd] || 'UTPDM03.01'
      @holdreason = 'XPLT'
      @holduser_tmp = 'X-PM'
      @holduser_cat = 'X-CAT'
      @mqsrclient = APC::MQSRClient.new(env, sv: @sv)
      @mq_status_orig = MQ::JMS.new("srastatus.reply.#{env}")
      @rtdremote = RTD::EmuRemote.new(env)
    end

    def inspect
      "#<#{self.class.name} #{@pmgr.inspect}>"
    end

    def new_lot(params={})
      return @sjctest.new_lot(params)
    end

    def spec_scenarios(params={})
      res = @sfc.spec_content(params[:spec] || @spec, parsed: true) || return
      res = res[:myFields][:Content][:Pilot]
      res = [res] unless res.kind_of?(Array)
      return res if params[:hash]
      #
      return @scenarios = res.collect {|sc|
        rpilot = sc[:PilotLotBehavior]
        mtypes = rpilot[:MeasurementTypes]
        mtypes = [mtypes] unless mtypes.kind_of?(Array)
        aliases = rpilot[:Aliases]
        aliases = aliases[:Alias] if aliases
        slots = rpilot[:Slots]
        if slots
          s = slots[:Slot]
          slots = s.kind_of?(Array) ? s.map {|e| e.to_i} : [s.to_i]
        end
        pilot = {isPilotPD: rpilot[:StartPointEqualPilotPD] == 'true', nwafers: rpilot[:PilotWaferCount].to_i,
          measTypes: mtypes.collect {|e| e[:MeasurementType]},
          useAliases: rpilot[:UseWaferAliases] == 'true', aliases: aliases,
          separate: rpilot[:SeparatePilotLotInEmptyCarrier] == 'true', slots: slots,
          allow_other_pilots: rpilot[:OtherPilotAllowed] == 'true', compress_pilots: rpilot[:CompressPilotLots]
        }
        rsrc = sc[:SourceLotBehavior]
        src = {hold: rsrc[:SetSourceLotOnHold] == 'true', toolwish: rsrc[:SetSameToolWish],
          chamberwish: rsrc[:SetSameChamberWish], sampling: rsrc[:SamplingLikePilotLot] == 'true',
          nwafers_sampling: rsrc[:SamplingWaferCount].to_i
        }
        {id: sc[:ScenarioID], lrcps: sc[:LogicalRecipes].values, apc: sc[:IsAPCcontrolled] == 'true', pilot: pilot, srclot: src}
      }
    end

    def spec_scenario(scenario)
      res = @scenarios || spec_scenarios || return
      return res.find {|e| e[:id] == scenario}
    end

    # # send SJC status message on behalf of the MQSRAdapter jCAP job, returns response on success or nil
    # #   status is SORT_REQUEST_COMPLETE or ...FAILED
    # #   srid must match the srid known by PilotManager from the SJC creation request
    # def XXXsubmit_sjcstatus(srid, status=nil, params={})
    #   context = {apcActionType: 'SortJobControllerResponse', originatorID: 'jCAP:MqSortRequestAdapter', userID: 'jCAP'}
    #   data = {RequestID: params[:rqid] || 'QA', StatusID: status || 'SORT_REQUEST_COMPLETE', SortRequestID: srid}
    #   res = @catalyst.submitjob(context, data) || return
    #   @catalyst.find_entry(res, 'eiCallType', 'apcdone') || ($log.warn res; return)
    #   return res
    # end

    # high level method to create a pilot, verify tmp lot hold, return list of pilots in scenario order on success
    def create_pilots(scenarios, lot, params={})
      chambers = params[:chambers] || Array.new(scenarios.size, 'empty')
      $log.info "create_pilots #{scenarios.inspect}, #{lot.inspect}#{', chambers: ' + chambers.inspect if params[:chambers]}"
      #
      @pmgr.pilotmgr_status  # nothing useful returned
      #
      scenarios = [scenarios] unless scenarios.kind_of?(Array)
      ndialogs = params[:maindialog] ? scenarios.size : 1
      dres = ndialogs.times.collect {|idx|
        $log.info "-- creating pilot #{idx + 1}/#{ndialogs}, scenario: #{scenarios[idx]}" if ndialogs > 1
        dialog = [['PilotManager FABGUI 1/4', {'EquipmentID'=>@eqp, 'Source Lot'=>lot, response: 'Next'}]]
        dialogchambers = ndialogs > 1 ? [chambers[idx]] : chambers
        dialogscenarios = ndialogs > 1 ? [scenarios[idx]] : scenarios
        dialogscenarios.each_with_index {|scenario, scidx|
          $log.info "  adding scenario #{scenario}" if dialogscenarios.size > 1
          dialog << ['PilotManager FABGUI 2/4', {'Add Scenario'=>"#{scenario} {}", 'Add Chamber'=>dialogchambers[scidx], response: 'Add'}]
        }
        dialog << ['PilotManager FABGUI 2/4', {'Add Scenario'=>"#{dialogscenarios.last} {}", 'Add Chamber'=>dialogchambers.last, 'Empty2'=>'', response: 'Next'}]
        dialog << ['PilotManager FABGUI 3/4', {response: 'Submit'}]
        dialog << ['PilotManager FABGUI 4/4', {response: idx == ndialogs - 1 ? 'Exit': 'Main'}]
        @_dialog = dialog  # for dev
        @pmgr.pilotmgr_dialog(dialog) {|idx, msg, response|
          sc = spec_scenario(dialogscenarios.first) || ($log.warn "scenario #{dialogscenarios.first} not found in spec"; return)  # TODO: works only for single scenarios
          if idx == 1
            $log.info 'verifying temporary hold'
            lh = @sv.lot_hold_list(lot, reason: @holdreason, user: @holduser_tmp).first
            ($log.warn '  missing tmp hold'; return) unless lh
          elsif sc[:apc] && idx == dialog.size - 1  # last screen, containing results (FABGUI 4/4)
            $log.info 'verifying APC EI msg'
            @eimq.receive_msg && @eimq.send_response_ok || ($log.warn '  no APC EI msg'; return)
          end
        } || return
      }.flatten
      #
      return @pmgr.verify_dialog_exit && dres
    end

    # verify pilot parameters, return true on success
    def verify_pilot(sc, lc, params={})
      chamber = params[:chamber] || 'empty'
      sc = spec_scenario(sc) if sc.kind_of?(String)
      $log.info "verify_pilot #{sc[:id].inspect}, #{lc.inspect} #{params}"
      lci = @sv.lot_info(lc, wafers: true)
      ok = true
      #
      # parent lot
      lot = lci.family
      li = @sv.lot_info(lot)
      $log.info "verifying source (parent) lot"
      # holds and toolwish
      lhtmp = @sv.lot_hold_list(lot, reason: @holdreason, user: @holduser_tmp).first
      lhcat = @sv.lot_hold_list(lot, reason: @holdreason, user: @holduser_cat).first
      if params[:separated] == false
        $log.info '  separate pending, temporary hold'
        ($log.warn '  missing tmp hold'; ok=false) unless lhtmp
      else
        ($log.warn '  wrong tmp hold'; ok=false) if lhtmp
        if sc[:srclot][:hold] || params[:srclothold]
          $log.info "  #{@holduser_cat} hold"
          ($log.warn '  missing hold'; ok=false) unless lhcat && lhcat.memo.start_with?('PilotManager Hold - Splits:')
          # PilotManager Hold - Splits: Split1 UXYP48044.006, verify memo??
        else
          $log.info "  no #{@holduser_cat} hold"
          ($log.warn '  wrong hold'; ok=false) if lhcat
        end
        # toolwish
        if params[:checktoolwish] == false
          $log.info '  skipping toolwish check'
        else
          ok &= verify_toolwish(li, @eqp, sc[:srclot][:toolwish], chamber, sc[:srclot][:chamberwish])
        end
        # sampling
        ok &= verify_sampling(lot, sc[:srclot][:nwafers_sampling], tstart: params[:tstart])
      end
      # no script parameters
      %w(PilotEquipment PilotOperation PilotScenario).each {|k|
        ($log.warn "  wrong lot script parameter #{k}"; ok=false) if @sv.user_parameter('Lot', lot, name: k).valueflag
      }
      # merge point and hold
      # opNo_merge = @sv._guess_op_route(lc, nil, op: params[:last_mpd] || @last_mpd, offset: params[:merge_offset] || 1).first.opNo
      ops = @sv.lot_operation_list(lc, raw: true)
      opNo_idx = ops.find_index {|e| e.operationID.identifier == params[:last_mpd] || @last_mpd}
      opNo_merge = ops[opNo_idx + params[:merge_offset] || 1].operationNumber
      $log.info "  merge point: #{opNo_merge}"
      fhlot = @sv.lot_futurehold_list(lot, type: 'MergeHold', related_lot: lc).first
      ($log.warn '  missing merge hold'; ok=false) unless fhlot
      ($log.warn "  wrong merge point: #{fhlot.rsp_op}"; ok=false) if fhlot && fhlot.rsp_op != opNo_merge
      #
      # pilot
      $log.info "verifying pilot #{lc}"
      # holds and script params
      lhtmp = @sv.lot_hold_list(lc, reason: @holdreason, user: @holduser_tmp).first
      lhcat = @sv.lot_hold_list(lc, reason: @holdreason, user: @holduser_cat).first
      if params[:separated] == false
        $log.info '  separate pending, temporary hold, no script parameters'
        %w(PilotEquipment PilotOperation PilotScenario).each {|k|
          ($log.warn "  wrong lot script parameter #{k}"; ok=false) if @sv.user_parameter('Lot', lc, name: k).valueflag
        }
        ($log.warn '  missing tmp hold'; ok=false) unless lhtmp
      else
        if params[:pilotcathold]
          # if split from a held srclot
          ($log.warn "  missing inherited #{@holduser_cat} hold"; ok=false) unless lhcat
        else
          ($log.warn "  wrong #{@holduser_cat} hold"; ok=false) if lhcat  # pilot normally has no X-CAT hold
        end
        ($log.warn '  wrong tmp hold'; ok=false) if lhtmp
        $log.info '  script parameters'
        ok &= verify_equal("#{@eqp}:#{chamber}", @sv.user_parameter('Lot', lc, name: 'PilotEquipment').value)
        # opinfo = @sv._guess_op_route(lot, nil, op: @ppd).first
        opNo = @sv.lot_operation_list(lot, raw: true).find {|e| e.operationID.identifier == @ppd}.operationNumber
        ok &= verify_equal("#{lci.route}:#{opNo}:#{@ppd}", @sv.user_parameter('Lot', lc, name: 'PilotOperation').value)
        ok &= verify_equal(sc[:id], @sv.user_parameter('Lot', lc, name: 'PilotScenario').value)
        # separate carrier
        if sc[:pilot][:separate]
          $log.info '  separate pilot'
        else
          ($log.warn '  wrong pilot separate'; ok=false) if lci.carrier != li.carrier
        end
        # other pilots in carrier allowed?
        clots = @sv.lot_list_incassette(lci.carrier)
        unless sc[:pilot][:allow_other_pilots]
          $log.info '  no other pilots in the carrier'
          ($log.warn "  extraneous lots: #{clots}"; ok=false) if clots != [lc]
        end
        # slots
        if slots = sc[:pilot][:slots]
          actuals = lci.wafers.collect {|w| w.slot}
          if sc[:pilot][:allow_other_pilots] && clots != [lc]
            if actuals == slots
              $log.info "  slots: #{slots}"
            else
              if sc[:pilot][:compress_pilots] == 'ascending'
                cok = actuals.min > slots.max
              elsif sc[:pilot][:compress_pilots] == 'descending'
                cok = actuals.max < slots.min
              else  # not allowed
                cok = false
              end
              if cok
                $log.info "  slots: #{slots}, got #{actuals} (accepting #{sc[:pilot][:compress_pilots]} slots)"
              else
                $log.info "  slots: #{slots}, got wrong #{actuals} for #{sc[:pilot][:compress_pilots]}"
                ok = false
              end
            end
          else
            $log.info "  slots: #{slots}"
            ok &= verify_equal(slots, actuals)
          end
        end
        # toolwish
        if params[:checktoolwish] == false
          $log.info '  skipping toolwish check'
        else
          ok &= verify_toolwish(lci, @eqp, 'MUST', chamber, 'MUST')
        end
        # sampling
        ok &= verify_sampling(lc, sc[:pilot][:nwafers], tstart: params[:tstart], last_mpd: params[:last_mpd])
        # lot openote
        ok &= verify_openote(lc, 'used as pilot')
        # $log.info '  openote'
        # note = nil
        # noteok = wait_for(timeout: 60) {
        #   note = @sv.lot_openotes(lc).find {|n| n.title == 'PilotManager: Update Pilot Status'}
        #   note && note.note.include?('used as pilot')
        # }
        # ($log.warn '  no openote'; ok=false) unless note
        # ($log.warn "  wrong note: #{note.note.inspect}"; ok=false) unless noteok
      end
      $log.info "  merge point: #{opNo_merge}"
      fhlc = @sv.lot_futurehold_list(lc, type: 'MergeHold', related_lot: lot).first
      ($log.warn '  missing merge hold'; ok=false) unless fhlc
      ($log.warn "  wrong merge point: #{fhlc.rsp_op}"; ok=false) if fhlc.rsp_op != opNo_merge
      # wafers and aliases
      ok &= verify_equal(sc[:pilot][:nwafers], lci.nwafers)
      if aliases = sc[:pilot][:aliases]
        $log.info "  aliases: #{aliases}"
        ok &= verify_equal(aliases, lci.wafers.collect {|w| w.alias})
      end
      #
      return ok
    end

    # return true on success
    def verify_toolwish(li, eqp, toolwish, chamber, chwish)
      $log.info "  toolwish: #{toolwish}, chamber: #{chamber}, chamberwish: #{chwish}"
      tws = @apc.toolwishes(li) || ($log.warn 'error getting toolwishes'; return)
      tw = tws[@eqp]['ToolWish'] || ($log.warn "  missing toolwish for #{@eqp}"; return)
      ($log.warn "  wrong toolwish: #{tw}"; return) unless tw.include?(toolwish.upcase) || tw.include?(toolwish.downcase)
      twcwish = tw.kind_of?(String) ? nil : tw.find {|e| e.kind_of?(Hash)}
      chwish.upcase!
      if chamber == 'empty' || chwish == 'NOT SET'
        ($log.warn "  unexpected chamberwish: #{twcwish}"; return) if twcwish
      else
        ref = {'ChamberWish'=>{chamber=>chwish}}
        ($log.warn "  wrong chamberwish: #{twcwish} instead of #{ref}"; return) if twcwish != ref
      end
      # if chwish == 'NOT SET'
      #   ($log.warn "  unexpected chamberwish: #{twcwish}"; return) if twcwish
      # else
      #   ref = {'ChamberWish'=>{chamber=>chwish}}
      #   ($log.warn "  wrong chamberwish: #{twcwish} instead of #{ref}"; return) if twcwish != ref
      # end
      return true
    end

    # return true on success
    def verify_sampling(lot, nwafers_sampling, params={})
      tstart = params[:tstart] || (Time.now - 10)
      ##$log.info "  verify_sampling #{lot.inspect}, #{nwafers_sampling}, tstart: #{tstart}, #{params}"
      ok = true
      srecord = @apcdb.misc_dataset(app: 'PilotManager', table: 'Sampling', lot: lot, tstart: tstart)
      if nwafers_sampling && nwafers_sampling > 0
        $log.info '  sampling'
        if srecord
          ($log.warn "  wrong pilot PD: #{srecord['sPilotPD']}"; ok=false) if srecord['sPilotPD'] != @ppd
          ($log.warn "  wrong metro PD: #{srecord['sLastMetroPD']}"; ok=false) if srecord['sLastMetroPD'] != (params[:last_mpd] || @last_mpd)
          ($log.warn "  wrong wafer count: #{srecord['nSampleWafer']}"; ok=false) if srecord['nSampleWafer'].to_i != nwafers_sampling
        else
          ($log.warn '  missing sampling record'; ok=false) unless srecord
        end
      else
        ($log.warn '  wrong sampling record'; ok=false) if srecord
      end
      return ok
    end

    # return true on success
    def verify_openote(lc, s)
      $log.info '  openote'
      note = nil
      noteok = wait_for(timeout: 60) {
        note = @sv.lot_openotes(lc).find {|n| n.title == 'PilotManager: Update Pilot Status'}
        note && note.note.include?(s)
      }
      ($log.warn '  no openote'; return) unless note
      ($log.warn "  wrong note: #{note.note.inspect}"; return) unless noteok
      return true
    end

    # return true on success
    def complete_sr_separate(srccarrier)
      $log.info "complete_sr_separate #{srccarrier.inspect}"
      # prepare sorter
      @sjctest.cleanup_eqp(@sjctest.sorter, mode: 'Auto-2', portstatus: 'UnloadReq')
      # prepare srccarrier
      @sv.carrier_xfer_status_change(srccarrier, '', @sjctest.stocker, manualin: true)
      # get SJC tasks
      sts = @sjctest.mds.sjc_tasks(srccarrier: srccarrier)
      ($log.warn "no SR for srccarrier #{srccarrier}"; return) if sts.nil? || sts.empty?
      srid = sts.first.srid
      sts.inject(true) {|ok, st| ok && st.srid == srid} || ($log.warn "  multiple SRs:\n#{sts}"; return)
      # prepare empty carriers
      tgtcarriers = sts.collect {|st| st.tgtcarrier}.sort.uniq
      ecs = @sjctest.get_empty_carrier(n: tgtcarriers.size, stockin: true) || return
      ecs.each {|ec| @sjctest.cleanup_sort_requests(ec) || return}
      # prepare RTD rule response
      @rtdremote.add_emustation('EMPTY') {
        @rtdremote.create_response(%w(cast_id cast_category stk_id), {'rows'=>ecs.collect {|c| [c, '', '']}})
      }
      # execute SJC SR
      res = @sjctest.execute_verify(srid: srid, timeout: 120)
      @rtdremote.delete_emustation('EMPTY')
      return unless res
      # execute SiView SJ
      @sv.claim_sj_execute(eqp: @sjctest.sorter) || return
      return true
    end

    # run a complete pilot scenario incl completion, return child lot or array of child lots created on success
    def verify_scenario(scenarios, lot, params={})
      li = @sv.lot_info(lot)
      # clean up carrier and optionally lot
      @sjctest.cleanup_sort_requests(li.carrier)
      @sjctest.cleanup_carrier(li.carrier)
      (@sv.merge_lot_family(lot) && @sv.lot_cleanup(lot, op: @ppd).zero? || return) if params[:lot_cleanup]
      # create pilot (FabGUI dialog)
      pilotcathold = !@sv.lot_hold_list(lot, reason: @holdreason, user: @holduser_cat).empty?  # used below for verify_pilot
      tstart = Time.now
      scenarios = [scenarios] unless scenarios.kind_of?(Array)
      chambers = params[:chambers] || Array.new(scenarios.size, 'empty')
      lcs = create_pilots(scenarios, lot, chambers: chambers, maindialog: params[:maindialog]) || return
      ($log.warn "  wrong number of child lot: #{lcs.size} instead of #{scenarios.size}"; return) if lcs.size != scenarios.size
      lf = @sv.lot_family(lot)
      ($log.warn "  missing child lot: #{lcs - lf}"; return) unless (lcs - lf).empty?
      # separate pilot(s) if configured in the scenario(s)
      mustseparate = false
      srclothold = false
      scs = scenarios.collect {|scenario|
        spec_scenario(scenario) || ($log.warn "no scenario definition for #{scenario.inspect}"; return)
      }
      scs.each_with_index {|sc, idx|
        if sc[:pilot][:separate]
          mustseparate = true
          srclothold = true if sc[:srclot][:hold]
          lc = lcs[idx]
          ($log.warn "  missing SJC task for #{lc}"; return) if @sjctest.mds.sjc_tasks(srclot: lc).empty?
          verify_pilot(sc, lc, merge_offset: params[:merge_offset], tstart: tstart,
            checktoolwish: params[:checktoolwish], srclothold: srclothold, separated: false) || return
        end
      }
      if mustseparate
        @mqsrclient.mq_status.delete_msgs(nil) if @mqsrclient.mq_status.qdepth > 0
        complete_sr_separate(li.carrier) || return
        # wait for the status message on the QA queue and forward it to the original CAT queue
        $log.info 'waiting up to 120 s for the MQSRA status msg'
        msg = @mqsrclient.mq_status.receive_msg(raw: true, timeout: 120) || return
        @mq_status_orig.send_msg(msg) || return
        # wait for Pilotmanager to act upon the status msg; tmp hold release is the last action
        wait_for(timeout: 90) {
          @sv.lot_hold_list(lot, reason: @reason, user: @holduser_tmp).empty?
        } || ($log.warn 'srclot tmp hold not released'; return)
      end
      # verify (final) pilot creation
      srclothold = false
      scs.each {|sc| srclothold = true if sc[:srclot][:hold]}
      scs.each_with_index {|sc, idx|
        $log.info "-- verifying (final) pilot ##{idx + 1}/#{scs.size}" if scs.size > 1
        verify_pilot(sc, lcs[idx], chamber: chambers[idx], merge_offset: params[:merge_offset], last_mpd: params[:last_mpd],
          tstart: tstart, checktoolwish: params[:checktoolwish], srclothold: srclothold, pilotcathold: pilotcathold) || return
        # effective for the next pilot and :maindialog (inherits from srclot)
        pilotcathold = true if params[:maindialog] && sc[:srclot][:hold]
      }
      return scenarios.size == 1 ? lcs.first : lcs
    end

    # send Data messages for pilot from all meas PDs (see resppd spec),
    #   verify script params removal and lot OpeNote, return true on success
    def complete_pilot(lc, params={})
      last_mpd = params[:last_mpd] || @last_mpd
      $log.info "complete_pilot #{lc.inspect}"
      done = false
      loop {
        (@sv.lot_gatepass(lc).zero? || return) unless last_mpd == @ppd  # no move for meas type INTERNAL
        lci = @sv.lot_info(lc)
        ($log.info "pilot #{lc} is now at #{lci.opNo} (#{lci.op})"; break) if done
        $log.info "submitting DCR for lot #{lc.inspect}, op: #{lci.op.inspect}, opNo: #{lci.opNo.inspect}"
        @apc.submit_dcr(APC::DCR.new(lci, @eqp)) || return
        done = lci.op == last_mpd
      }
      ok = true
      $log.info 'verifying pilot'
      $log.info '  script parameter removal'
      %w(PilotEquipment PilotOperation PilotScenario).each {|k|
        wait_for(timeout: 60) {
          !@sv.user_parameter('Lot', lc, name: k).valueflag
        } || ($log.warn "  wrong lot script parameter #{k}"; ok=false)
      }
      ok &= verify_openote(lc, 'Pilot Status removed')
      return ok
    end

  end

end
