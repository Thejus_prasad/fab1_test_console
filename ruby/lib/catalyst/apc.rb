=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.


Author: Steffen Frieske, 2016-09-06

Version: 1.0.9

History:
  2016-12-05 sfrieske, improved success search for testaction
  2017-11-16 sfrieske, separated APC application calls from BL tests and Catalyst communication

Notes:
  APC baseline Wiki: http://f1onewiki:21080/display/AB/APC+Baseline+Overview
  Catalyst applog: <@@apc.http.uri.to_s>/logs/applog/
  Catalyst logs ITDC: http://drsapc/apcsetup/cajallog/log_fab1_dev.html
  Per server logs (MQ): \\sfc1catd02\log\system

  Specs: C07-00003517
=end

require_relative 'catalyst'


module APC

  # Interact with often used APC applications: PCL-Pre1, ToolWish via Catalyst (MQ or HTTP)
  class Client
    attr_accessor :catalyst, :_res  # _res for dev only!

    def initialize(env, params={})
      @catalyst = params[:catalyst] || Catalyst.Client(env, params)  # MQ unless use_http: true
    end

    def inspect
      "#<#{self.class.name} #{@catalyst.inspect}>"
    end

    # send PCL Pre1 job data like SiView Pre1 scripts (currently not used)
    def pclpre1(li, prevpd, params={})
      context = {
        apcActionType: 'PCLSpecsPre1', SiViewScript: 'Pre1', SiViewFirstPD: 'no',
        SiViewScriptName: params[:scriptname] || 'PCL.Pre1', completedProcessID: prevpd,
        originatorID: 'SiView',
        lotID: li.lot, CurrentPD: li.op, CurrentPassCount: li.reworkcount, SiViewTimeStamp: li.claim_time,
        product: li.product, productGroup: li.productgroup
      }
      data = params[:data] || {}  # {:''=>''}
      res = @catalyst.submitjob(context, data, params) || return
      return res if params[:raw]
      return res
    end

    # get Catalyst toolwishes (like FabGUI)
    def toolwishes(li, params={})
      masklevel = params[:masklevel] || li.masklevel
      op = params[:op] || li.op
      context = {apcActionType: 'ToolWishRequest', originatorID: 'FabGUI/ToolWish', userClass: '',
        userID: 'X-UNITTEST', ToolWishRequest: 'getToolWishes', LotID: li.lot, MaskLevel: masklevel, PDname: op}
      res = @catalyst.submitjob(context, {}, timeout: params[:timeout]) || return
      ##return res if params[:raw]
      ($log.warn "no result: #{res.inspect}"; return) if res.nil? || res['ToolWishStatus'].nil?
      stat = Hash[res['ToolWishStatus'][:dataEntry].collect {|e| e.values}]
      ($log.warn stat; return) if stat['Code'] != 'OK'
      return @catalyst.extract_data(res['ToolWishData'][:dataEntry])
    end

    # send toolwish to Catalyst like FabGUI, return true on success
    def set_toolwishes(li, eqp, wish, params={})
      $log.info "toolwish  # lot: #{li.lot.inspect}, eqp: #{eqp.inspect}, wish: #{wish.inspect}, #{params}"
      wish = 'not set' if wish.nil? || wish == ''   # or NEVER (or -minutes), MUST (+minutes)
      score = params[:score] || 'not set'           # number of minutes
      masklevel = params[:masklevel] || li.masklevel
      op = params[:op] || li.op
      context = {apcActionType: 'ToolWishRequest', originatorID: 'FabGUI/ToolWish', userClass: '',
        userID: 'X-UNITTEST', ToolWishRequest: 'submitToolWishes', LotID: li.lot, MaskLevel: masklevel, PDname: op}
      wishdata = {'Value'=>wish}
      if chambers = params[:chambers]
        # chambers is a Hash, left out chambers will be treated as 'not set'
        wishdata['ChamberWish'] = chambers  #.each_pair.collect {|ch, cw| @catalyst.expand_data(ch=>cw)}
      end
      data = {'ToolWishData'=>{eqp=>{'Comment'=>'QA', 'ToolScore'=>{'Value'=>score}, 'ToolWish'=>wishdata}}}
      res = @catalyst.submitjob(context, data, timeout: params[:timeout]) || return
      twstat = res['ToolWishStatus'] || ($log.warn "error submitting toolwishes:\n#{res}"; return)
      stat = Hash[twstat[:dataEntry].collect {|e| e.values}]
      ($log.warn stat; return) if stat['Code'] != 'OK'
      return true
    end

    # submit an APC DCR (JobData), verify if eiCallType: apcdone is returned and return JobData Hash
    def submit_dcr(context, data=nil)
      if data.nil?
        # assuming context is an APC::DCR object
        data = context.data
        context = context.context
      end
      @_res = nil
      res = @catalyst.submitjob(context, data) || return
      @_res = res  # for dev only!
      ($log.warn 'no JobData'; return) unless res['JobStatus'] && res['JobData']
      jdata = @catalyst.extract_data(res['JobData'][:dataEntry]) || ($log.warn 'no JobData'; return)
      jdata.values.find {|v| v['eiCallType'] == 'apcdone'} || ($log.warn "missing eiCallType: apcdone\n#{res}"; return)
      return jdata
    end

  end

end

