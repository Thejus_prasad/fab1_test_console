=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.


Author: Steffen Frieske, 2021-03-04

Version: 2.0.2

History:
  2021-08-27 sfrieske, require waitfor instead of misc

Notes:
  APC logs ITDC: http://drsapc/apcsetup/cajallog/index.php?fabNumber=36&environment=development
=end

require 'siview'
require 'catalyst/apcdb'
require 'catalyst/waferselection'
require 'util/waitfor'
require_relative 'apc'
require_relative 'apcdcr'
# for JCAP
require 'rtdserver/rtdemuremote'


module APC

  class GRRStudy
    attr_accessor :sv, :original_route, :original_pd, :wafersel, :ptype, :player,
      :op_eqps, :study_route, :study_op,
      :cat2, :apc, :siteX, :siteY, :spec_target, :study_tools_warning, :openote_title,
      :_dcr  # for dev

    def initialize(env, original_route, original_pd, params={})
      @sv = params[:sv] || SiView::MM.new(env)
      @wafersel = params[:wafersel] || Catalyst::WaferSelection.new(env)
      @apc = params[:apc] || APC::Client.new(env, params)
      @cat2 = params[:cat2] || APC::DBCat2.new(env)
      @original_route = original_route
      @original_pd = original_pd
      @player = params[:processlayer] || 'GRR'
      @ptype = params[:processtype] || 'TX'
      @study_route = 'M-GAGE-RR.01'
      @study_op = 'M-GAGE-RR-RETURN.01'
      @spec_target = 50
      @spec_spread = 2
      @siteX = %w(
        58159.998530755 -81327.997945482 -104575.99735819 -81327.997945482 11663.999705343
        -34831.99912007 104655.99735617 34911.999118049 -34831.99912007 58159.998530755
        104655.99735617 34911.999118049 -11583.999707364)
      @siteY = %w(
        -105270.99744063 -105270.99744063 -42114.999036086 21040.99936846 -10536.999833813
        -10536.999833813 21040.99936846 52618.998570732 84196.997773005 84196.997773005
        -73692.998238358 -42114.999036086 -73692.998238358)
      @study_tools_warning = 'Not_enough_tools_for_GRR'
      @openote_title = 'GageRandR'  # 'GageRandR FAILED', WARNING
    end

    def inspect
      "#<#{self.class.name} #{@sv.env} #{@original_pd}>"
    end

    # send a DCR from original_pd to trigger the TCL monitor, for manual tests
    def send_dcr_orig(lot, eqp, params={})
      $log.info "send_dcr_orig #{lot.inspect}, #{eqp.inspect}, #{params}"
      li = @sv.lot_info(lot, wafers: true)
      li.route = @original_route
      li.op = @original_pd
      li.opNo = '1000.0200'
      # sleep 1
      $log.info "-- submitting DCR for #{eqp}, op: #{li.op}"
      ctx = {ProcessLayer: @player, ProcessType: @ptype, originalProcessDefinition: @original_pd}
      @_dcr = dcr = APC::DCR.new(li, eqp, siteX: @siteX, siteY: @siteY, ctx: ctx, slots: [1])
      readings = {}
      parameters = params[:parameters] || ['MyParameterB']
      parameters.each {|p|
        readings[p] = {[1] => [@siteX.collect {((rand - 0.5) * @spec_spread).round(3) + @spec_target}]}
      }
      dcr.add_readings(readings)
      @apc.submit_dcr(dcr) || ($log.warn 'error submitting DCR'; return)
      return true
    end

    # prepare an existing lot for APC tests, incl. lot branch on the measurement route
    def apc_prepare_lot(lot)
      $log.info "apc_prepare_lot #{lot.inspect}"
      # set wafer selection, 1st wafer only
      li = @sv.lot_info(lot, wafers: true)
      @wafersel.set_selection(li, @ptype, @player, slots: [1]) || ($log.warn 'wafer selection failed'; return)
      # locate to the resp. original PD and set the lot script parameter
      @sv.lot_opelocate(lot, nil, op: @original_pd).zero? || ($log.warn 'error locating lot'; return)
      @op_eqps = []
      @sv.eqp_list_byop(li.product, @original_pd).each {|e|
        eqp = e.equipmentID.identifier
        next if eqp == 'THK111'
        next if eqp == 'A-DUMMY'
        next if eqp == 'UTC003'
        @op_eqps << eqp
      }
      ntools = @op_eqps.size
      if ntools <= 2
        nreq = ntools
      elsif ntools == 3
        nreq = 2
      else
        nreq = 3
      end
      # gatepass (a.k.a. opecomp at GRR original PD), set lot script parameter and branch
      @sv.lot_gatepass(lot).zero? || ($log.warn 'error gatepassing lot'; return)
      v = "{\"PD\":\"#{@original_pd}\",\"route\":\"#{li.route}\",\"num_tools_req\":#{nreq}}"
      @sv.user_parameter_change('Lot', lot, {'GRROriginalPD'=>v}).zero? || ($log.warn 'error setting script param'; return)
      @sv.lot_branch(lot, @study_route, dynamic: true).zero? || ($log.warn 'error branching lot'; return)
      return true
    end

    # process at n PDs, locate to last PD on the meas route and verify lot script param removal
    def apc_measroute(lot, params={})
      eqps = @op_eqps.take(params[:npds] || 9)
      $log.info "apc_measroute #{lot.inspect}, npds: #{eqps.size}  (origOp: #{@original_pd})"
      parameters = params[:parameters] || ['MyParameterB']
      repeats = params[:repeats] || Array.new(eqps.size, 3)   # measurements per tool, 3 is standard
      eqps.each_with_index {|eqp, i|
        li = @sv.lot_info(lot, wafers: true)
        n = repeats[i]
        n.times.each {|j|
          # sleep 1
          $log.info "-- submitting DCR ##{j+1}/#{n} for eqp ##{i+1}/#{eqps.size}, op: #{li.op}"
          ctx = {ProcessLayer: @player, ProcessType: @ptype, originalProcessDefinition: @original_pd}
          @_dcr = dcr = APC::DCR.new(li, eqp, siteX: @siteX, siteY: @siteY, ctx: ctx, slots: [1])
          readings = {}
          parameters.each {|p|
            readings[p] = {[1] => [@siteX.collect {((rand - 0.5) * @spec_spread).round(3) + @spec_target}]}
          }
          dcr.add_readings(readings)
          @apc.submit_dcr(dcr) || ($log.warn 'error submitting DCR'; return)
        }
        # gatepass (a.k.a. opecomp at GRR original PD's eqp)
        @sv.lot_gatepass(lot).zero? || ($log.warn 'error gatepassing lot'; return)
      }
      # may take ~ 4 minutes in case of missing data
      @sv.lot_opelocate(lot, nil, op: @study_op).zero? || ($log.warn 'error locating lot'; return)
      # verify lot has returned and script parameter is removed
      ($log.warn 'lot did not return to main route'; return) if @original_route != @sv.lot_info(lot).route
      ($log.warn 'lot script parameter not deleted'; return) unless @sv.user_parameter('Lot', lot, name: 'GRROriginalPD').value.empty?
      return true
    end

    # verify success or failure and OpeNote, return true on success
    def apc_verify_study(lot, success, params={})
      $log.info "apc_verify_study #{lot.inspect}, #{success}, #{params}"
      wait_for(timeout: 60) {!@cat2.grr_study(lot).empty?} || ($log.warn 'no study in CAT2'; return)
      study = @cat2.grr_study(lot, parameter: params[:parameter]).first || ($log.warn 'no study in CAT2'; return)
      ok = true
      if success
        ($log.warn "wrong tools warning: #{study}"; ok=false) if study.warning.include?(@study_tools_warning)
        ($log.warn "wrong rating: #{study}"; ok=false)  if study.rating != 'POOR'
        ($log.warn 'wrong openote'; ok=false) unless @sv.lot_openotes(lot).empty?
      else
        warning = params[:warning] || @study_tools_warning  # different warning for 0 measurements
        ($log.warn "missing tools warning: #{study}"; ok=false) unless study.warning.include?(warning)
        ($log.warn "wrong rating: #{study}"; ok=false)  unless ['ERROR', 'RETRY'].include?(study.rating)
        note = @sv.lot_openotes(lot).first || ($log.warn 'no openote'; return)
        ($log.warn 'wrong openote title'; ok=false) unless note.title.start_with?(@openote_title)
      end
      return ok
    end

    def apc_runtest(lot, success, params={})
      apc_prepare_lot(lot) || return
      apc_measroute(lot, params) || return
      apc_verify_study(lot, success, params) || return
      return true
    end

  end

end


module JCAP

  class GRRMC < APC::GRRStudy
    attr_accessor :rtdremote, :rtd_rule, :rtd_data, :rtd_data_branching, :rtd_data_measroute,
      :rtd_method, :hold_grr, :opNo_branch, :job_interval

    def initialize(env, original_route, original_pd, params={})
      super
      @rtdremote = params[:rtdremote] || RTD::EmuRemote.new(env)
      @hold_grr = params[hold_grr] || 'M-GRR'
      @rtd_rule = params[:rtd_rule] || 'JCAP_GRRMC'
      @job_interval = params[:job_interval] || 120
    end

    # prepare an existing lot for JCAP tests incl. FutureHold
    def jcap_prepare_lot(lot)
      $log.info "jcap_prepare_lot #{lot.inspect}"
      # set wafer selection, 1st wafer only
      li = @sv.lot_info(lot, wafers: true)
      @wafersel.set_selection(li, @ptype, @player, slots: [1]) || ($log.warn 'wafer selection failed'; return)
      # locate to the original PD
      @sv.lot_opelocate(lot, nil, op: @original_pd).zero? || ($log.warn 'error locating lot'; return)
      @op_eqps = []
      @sv.eqp_list_byop(li.product, @original_pd).each {|e|
        eqp = e.equipmentID.identifier
        next if eqp == 'THK111'
        next if eqp == 'A-DUMMY'
        next if eqp == 'UTC003'
        @op_eqps << eqp
      }
      @rtd_data = @op_eqps.collect {|e| [e]}
      # set the M-GRR future hold (on behalf of PCL)
      @sv.lot_futurehold(lot, nil, @hold_grr, post: true).zero? || ($log.warn 'future hold failed'; return)
      return true
    end

    def jcap_branch_lot(lot, params={})
      $log.info "jcap_branch_lot #{lot.inspect}, #{params}"
      # gatepass (a.k.a. opecomp at GRR original meas PD)
      @sv.lot_gatepass(lot).zero? || ($log.warn 'error gatepassing lot'; return)
      @opNo_branch = @sv.lot_info(lot).opNo   # for FPC verification
      # configure RTD emustation
      @rtd_data_branching = @rtd_data.take(params[:neqps_branching] || 9)
      @rtd_data_measroute = @rtd_data.take(params[:neqps_measroute] || 9)
      @rtd_method = proc {|stn, rule, rparams|
        rtdlot = rparams['lot_id']
        if rtdlot == lot
          li = @sv.lot_info(lot)
          if li.route == @original_route
            data = @rtd_data_branching
          else
            data = @rtd_data_measroute
          end
          $log.info "EmuRemote response for #{lot} (#{li.route} - #{li.op}): #{data}"
          @rtdremote.create_response(['eqp_id'], {'rows'=>data})
        else
          $log.info "empty EmuRemote response for foreign lot #{rtdlot}"
          sleep 3
          @rtdremote.create_response(['eqp_id'], {'rows'=>[]})
        end
      }
      @rtdremote.add_emustation('', rule: @rtd_rule, method: @rtd_method) || ($log.warn 'error adding RTD emustation'; return)
      # wait for the JCAP job to run: lot is branched and FPC definitions set
      $log.info "waiting up to #{@job_interval + 60} s for the JCAP job to branch the lot"
      wait_for(timeout: @job_interval + 60) {
        @sv.lot_info(lot).route == @study_route
        # @sv.lot_hold_list(lot, raw: true).empty?
      } || ($log.warn 'lot is not on the study route'; return)
      !@sv.lot_fpc_infos(lot: lot).empty? || ($log.warn 'FPC data not set'; return)
      $log.info 'lot is ready for the study'
      return true
    end

    def jcap_measroute(lot, params={})
      eqps = (@rtd_data_measroute.flatten & @op_eqps).take(params[:npds] || 9)  # filter out RTD non-meas tools
      $log.info "jcap_measroute #{lot.inspect}, npds: #{eqps.size}  (origOp: #{@original_pd})"
      ctx = {ProcessLayer: @player, ProcessType: @ptype, originalProcessDefinition: @original_pd}
      parameters = params[:parameters] || ['MyParameterB']
      eqps.each_index {|i|
        li = @sv.lot_info(lot, wafers: true)
        # wait for the job to set FPC eqp at the current op
        eqp = ''
        wait_for(timeout: @job_interval + 60) {
          eqp = @sv.lot_fpc_infos(lot: lot, route: @study_route, opNo: li.opNo,
            org_route: @original_route, org_opNo: @opNo_branch).first.strFPCInfo.equipmentID.identifier
          !eqp.empty?
        } || ($log.warn 'no eqp in FPC'; return)
        # process lot, sending APC DCRs (3 is the standard)
        n = 3
        n.times.each {|j|
          $log.info "-- submitting DCR ##{j+1}/#{n} for eqp ##{i+1}/#{eqps.size}, op: #{li.op}"
          @_dcr = dcr = APC::DCR.new(li, eqp, siteX: @siteX, siteY: @siteY, ctx: ctx, slots: [1])
          readings = {}
          parameters.each {|p|
            readings[p] = {[1] => [@siteX.collect {((rand - 0.5) * @spec_spread).round(3) + @spec_target}]}
          }
          dcr.add_readings(readings)
          @apc.submit_dcr(dcr) || ($log.warn 'error submitting DCR'; return)
        }
        @sv.claim_process_lot(lot, eqp: eqp) || ($log.warn 'error processing lot'; return)
      }
      # watch the lot (gatepass by JCAP, final gatepass by APC Pre1) until return to the main route
      $log.info 'waiting for study completion'
      op = nil
      wait_for(timeout: 9 * @job_interval + 60) {
        li = @sv.lot_info(lot)
        ($log.info "  #{li.op}"; op = li.op) if li.op != op
        li.route == @original_route
      } || ($log.warn 'lot did not return to main route'; return)
      $log.info 'verifying lot script parameter GRROriginalPD was removed'
      ($log.warn 'lot script parameter not deleted'; return) unless @sv.user_parameter('Lot', lot, name: 'GRROriginalPD').value.empty?
      return true
    end

    def jcap_runtest(lot, success, params={})
      jcap_prepare_lot(lot) || return
      jcap_branch_lot(lot, params) || return
      jcap_measroute(lot, params) || return
      # apc_verify_study(lot, success, params) || return  # done in APC tests
      return true
    end

  end

end
