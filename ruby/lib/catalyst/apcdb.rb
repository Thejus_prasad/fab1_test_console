=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author:     sfrieske, 2016-09-14
Version:    1.1.0

History:
  2018-01-29 sfrieske, added sampling query (tables MISC and MISC_DATA)
  2021-05-03 sfrieske, added Cat2 access for GRR
=end

require 'dbsequel'


module APC

  # access CAT1 DB for APC Tests
  class DB
    MaterialProps = Struct.new(:id, :updated, :lot, :wafer, :application, :processlayer, :processtype)
    MaterialPropsData = Struct.new(:id, :owner, :updated, :key, :value)
    Misc = Struct.new(:id, :tags, :updated, :extra)

    attr_accessor :db

    def initialize(env, params={})
      @db = params[:db] || ::DB.new("cat1.#{env}", schema: 'CATADMIN')
    end

    # return array of AQV::Lot structs, for Baseline sampling tests
    def rdb_material_props(params={})
      qargs = []
      q = 'SELECT ID, UPDATE_TIME, LOTID, WAFERID, APPLICATIONID, PROCESSLAYER, PROCESSTYPE from RDB_MATERIAL_PROPS'
      q, qargs = @db._q_restriction(q, qargs, 'LOTID', params[:lot])
      q, qargs = @db._q_restriction(q, qargs, 'WAFERID', params[:wafer])
      q += ' ORDER BY ID'
      res = @db.select(q, *qargs) || return
      return res.collect {|r|
        r[0] = r[0].to_i
        MaterialProps.new(*r)
      }
    end

    # UNUSED
    def rdb_material_props_data(params={})
      qargs = []
      q = 'SELECT ID, OWNER_ID, UPDATE_TIME, KEY, VALUE from RDB_MATERIAL_PROPS_DATASET'
      q, qargs = @db._q_restriction(q, qargs, 'OWNER_ID', params[:id])
      q += ' ORDER BY OWNER_ID, ID'
      res = @db.select(q, *qargs) || return
      return res.collect {|r|
        r[0] = r[0].to_i
        r[1] = r[1].to_i
        MaterialPropsData.new(*r)
      }
    end

    # for old PilotManager tests
    def misc(params={})
      qargs = []
      q = 'SELECT ID, TAG, UPDATE_TIME, EXTRA from MISC'
      q, qargs = @db._q_restriction(q, qargs, 'UPDATE_TIME >=', params[:tstart])
      tag = ''
      [:app, :lot, :table].each {|k|   # order matters
        if kvalue = params[k]
          tag += '%' + kvalue
        end
      }
      q, qargs = @db._q_restriction(q, qargs, 'TAG', tag + '%') unless tag.empty?
      q += ' ORDER BY ID'
      res = @db.select(q, *qargs) || return
      return res.collect {|r|
        r[0] = r[0].to_i
        Misc.new(*r)
      }
    end

    # for old PilotManager tests
    def misc_dataset(params)
      ownerid = params[:ownerid]
      unless ownerid
        res = misc(params)
        return unless res && !res.empty?
        ($log.warn "misc has multiple entries for #{params}"; return) if res.size > 1
        ownerid = res.first.id
      end
      res = @db.select('SELECT KEY, VALUE from MISC_DATASET where OWNER_ID = ?', ownerid) || return
      return Hash[res]
    end

  end


  class DBCat2
    Study = Struct.new(:id, :parameter, :rating, :ptratio, :lot, :wafer, :op, :tools, :warning)

    attr_accessor :db

    def initialize(env, params={})
      @db = params[:db] || ::DB.new("cat2.#{env}")
    end

    def grr_study(lot, params={})
      qargs = [lot]
      q = 'SELECT EVENT_ID, PARAMETER, RATING, PTRATIO, LOTID, WAFERID, OPERATIONID, TOOLS, WARNINGLOG
          from AH_GRR_STUDY WHERE LOTID=?'
      q, qargs = @db._q_restriction(q, qargs, 'PARAMETER', params[:parameter])
      res = @db.select(q, *qargs) || return
      return res.collect {|r|
        r[0] = r[0].to_i
        r[3] = r[3].to_f
        Study.new(*r)
      }
    end

    # def grr_study_values, keys LOTID and PARAMETER?, not useful

  end

end
