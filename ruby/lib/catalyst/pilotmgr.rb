=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.


Logs ITDC: http://drsapc/apcsetup/cajallog/index.php?fabNumber=36&environment=development
Specs: C07-00003517

Author: Steffen Frieske, 2017-11-27

Version: 1.0.2

History:
  2018-06-06 sfrieske, removed submit_data
=end

require 'mqjms'
require 'util/xmlhash'
require_relative 'catalyst'


module APC

  # PilotManager Interaction
  class PilotManager
    attr_accessor :_msg, :catalyst, :mqrdialog, :extsvcqa, :extsvcqamsg, :rspprefix

    def initialize(env, params={})
      @catalyst = params[:catalyst] || Catalyst.Client(env, params)
      @extsvcqa = MQ::JMS.new("catqa.#{env}")  # for external service calls (QA testing)
    end

    def inspect
      "#<#{self.class.name} #{@catalyst.inspect}  eqp: #{@eqp}>"
    end

    # convert a key/value Hash to a ViewExtendedQuery hash (to be sent to APC on behalf of FabGUI)
    def data_to_guihash(data)
      ret = {}
      data.each_with_index {|d, i|
        k, v = d
        if k.to_s == 'response'
          ret['response'] = v
        else
          ret["guiElements/guiElement#{i}/guiElementID"] = i.to_s
          ret["guiElements/guiElement#{i}/guiElementParameters/Id"] = i.to_s
          unless k.start_with?('Empty')
            ret["guiElements/guiElement#{i}/guiElementParameters/Label"] = k.to_s
            ret["guiElements/guiElement#{i}/guiElementParameters/Value"] = v.to_s
          end
        end
      }
      return ret
    end

    # start PilotManager communication: userclass: 'QASEMI' for logging, 'QAAUTO' for MQ driven dialogs, '' for FabGUI; returns nil
    def pilotmgr_status(params={})
      $log.info 'pilotmgr_status'
      context = {apcActionType: 'Status', originatorID: 'FabGUI', equipmentId: 'PilotManager',
        equipmentType: 'NA', userClass: params[:userclass] || 'QAAUTO',
        userID: params[:user] || 'X-UNITTEST', sessionID: params[:session] || 'All',
        webAdapterProxy: 'http://f36fabguid1:8088/ccenter/services'
      }
      # answer is sent after dialog completion, see verify_pilotmgr_exit
      return @mqrdialog = @catalyst.submitjob(context, {}, deferred_response: true)
    end

    # receive and verify status code of the response, return true on success
    def verify_dialog_exit(params={})
      $log.info 'verify_dialog_exit'
      msg = @mqrdialog.receive_msg || return
      @mqrdialog.close
      msg = @catalyst.unwrapped_xml(msg) || return
      return !!@catalyst.extract_jobresult(msg)  # verifies statusCode 0
    end

    # execute a FabGUI dialog via external service (APC application support needed, QA queue configured),
    # return result table as array of pilot (child) lots in the order of the scenarios on success or nil
    def pilotmgr_dialog(dialog, &blk)
      $log.info 'pilotmgr_dialog ...'
      @extsvcqa.reconnect
      rspprefix = 'Service=FabGui method=ViewExtendedQuery response='
      ret = nil
      dialog.each_with_index {|step, idx|
        key, response = step
        $log.info "waiting for msg #{key.inspect}"
        # receive APC request
        msg = @extsvcqa.receive_msg(timeout: 30) || ($log.warn 'missing or wrong external service msg'; return)
        @extsvcqamsg = msg
        if erridx = msg.index('Error')
          $log.warn msg[erridx ... msg.index('</font>', erridx)].gsub('<br>', ' ')
          return
        end
        ($log.warn 'wrong msg'; return) unless msg.include?(key)
        # extract result if last one
        if idx == dialog.size - 1
          hres = XMLHash.xml_to_hash(msg[msg.index('<table') .. msg.index('</table>') + 7]) || return
          harr = hres[:table][:tr].collect {|e| e.values.flatten}  # 1st row: headers, 2nd row: srclot
          collot = harr.first.index('Lot')
          ret = harr[2..-1].collect {|e| e[collot]}
        end
        # optional callback
        blk.call(idx, msg, response) if blk
        # send response
        if response.kind_of?(Hash)
          unless response.has_key?('response') && response.has_key?('guiElements/guiElement0/guiElementID')
            response = data_to_guihash(response)
          end
          response = @catalyst.hash_to_dictstring(response)
        end
        @extsvcqa.send_response(rspprefix + response) || return
      }
      return ret
    end

  end

end
