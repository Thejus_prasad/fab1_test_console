=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.


Author: Steffen Frieske, 2020-04-08

Version: 1.0.2

History:
  2021-02-16 sfrieske, use rexml directly

Notes:
  FabGUI log files: /amdapps/fabgui/CURRENT/log/service/f36fabguid1_mqsoap.log

=end

require 'rexml/document'
require 'mqjms'
require 'util/xmlhash'


module Catalyst

  # Send messages via Catalyst like FabGUI WaferSelection
  class WaferSelection
    attr_accessor :mq, :mqr, :_doc   # :mqr and :_doc are for dev only

    def initialize(env)
      @mq = MQ::JMS.new("catsampling.#{env}", {reply_queue: nil})
    end

    def inspect
      "#<#{self.class.name} #{@mq.inspect}>"
    end

    def execute_request(ns1)
      @_doc = doc = REXML::Document.new('<?xml version="1.0" encoding="UTF-8"?>')
      return doc.add_element('soapenv:Envelope', {
        'xmlns:soapenv'=>'http://schemas.xmlsoap.org/soap/envelope/',
        'xmlns:xsd'=>'http://www.w3.org/2001/XMLSchema',
        'xmlns:xsi'=>'http://www.w3.org/2001/XMLSchema-instance',
      })
      .add_element('soapenv:Body')
      .add_element('ns1:executeRequest', {'xmlns:ns1'=>ns1})
    end

    # ex: ptype 'DISPO', player 'INIT'
    def get_selection(li, ptype, player)
      tag1 = execute_request('urn:toolbox:/toolbox/services/WaferSelection/getWaferSelectionData.xml')
      tag1.add_element('PhotoLayer').text = player
      tag1.add_element('ProcessType').text = ptype
      tag1.add_element('FamilyLotID').text = li.family
      tag1.add_element('LotID').text = li.lot
      wtag = tag1.add_element('WaferIDs')
      wtag.add_element('dim').text = li.wafers.size
      li.wafers.each {|w|
        wtag.add_element('val').text = w.wafer
      }
      xml = String.new
      tag1.document.write(xml)
      @mqr = MQ::JMS.new(nil, @mq.params.merge(qname: 'temporary')) || return
      @mq.send_msg(xml, reply_to: @mqr.queue) || return
      res = @mqr.receive_msg(first: true)  ##, timeout: params[:timeout])
      @mqr.close
      ($log.warn "  error:\n#{res}"; return) if res.nil? || !res.include?('executeResponse')
      rhash = XMLHash.xml_to_hash(res, '*//*Body')
      return rhash[:executeResponse]
    end

    # ex: ptype 'POL', player 'CONT-06', default selection is 'MUST'
    def set_selection(li, ptype, player, params={})
      $log.info "set_selection (#{li.lot.inspect})  #{ptype.inspect}, #{player.inspect}, #{params}"
      tag1 = execute_request('urn:toolbox:/toolbox/services/WaferSelection/setWaferSelection.xml')
      tag1.add_element('Comment').text = params[:comment] || 'QA'
      tag1.add_element('PhotoLayer').text = player
      tag1.add_element('ProcessType').text = ptype
      tag1.add_element('FamilyLotID').text = li.family
      tag1.add_element('LotID').text = li.lot
      tag1.add_element('SelectionSystem').text = params[:system] || 'SUPERUSER'  # APCUSER, GUIUSER
      wtag = tag1.add_element('WaferIDs')
      wtag.add_element('dim').text = li.wafers.size
      seltag = tag1.add_element('SelectionInfo')
      seltag.add_element('dim').text = li.wafers.size
      slots = params[:slots]
      selection = params[:selection] || 'MUST'  # MUST, EXCLUDE, DO_NOT_CARE
      li.wafers.each {|w|
        wtag.add_element('val').text = w.wafer
        if slots
          seltag.add_element('val').text = slots.member?(w.slot) ? selection : 'DO_NOT_CARE'
        else
          seltag.add_element('val').text = selection
        end
      }
      xml = String.new
      tag1.document.write(xml)
      @mqr = MQ::JMS.new(nil, @mq.params.merge(qname: 'temporary')) || return
      @mq.send_msg(xml, reply_to: @mqr.queue) || return
      res = @mqr.receive_msg(first: true)  ##, timeout: params[:timeout])
      @mqr.close
      ($log.warn "  error:\n#{res}"; return) if res.nil? || !res.include?('executeResponse')
      return res  # basically empty except the SOAP header
    end

  end

end
