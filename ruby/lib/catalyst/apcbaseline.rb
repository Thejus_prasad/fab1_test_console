=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.


Helpful links:
  APC baseline Wiki: http://f1onewiki:21080/display/AB/APC+Baseline+Overview
  Catalyst applog: <@@apc.http.uri.to_s>/logs/applog/

Author: Steffen Frieske, 2016-09-06

Version: 2.1.3

History:
  2016-12-05 sfrieske, improved success search for testaction
  2017-11-16 sfrieske, separated BL tests from Catalyst communication
  2018-06-07 sfrieske, added CPC result verification
  2018-11-07 sfrieske, allow to send porcess layer and type (PD UDATA) for #testaction
  2021-02-05 sfrieske, use instance_of instead of kind_of
  2021-08-27 sfrieske, require uniquestring instead of misc
=end

require 'pp'
# require 'misc'
require 'siview'
require 'util/uniquestring'
require 'util/xmlhash'
require_relative 'apc'
require_relative 'apcdcr'


module APC

  # APC Baseline Tests
  class BaselineTest
    attr_accessor :sv, :apc, :dcr, :jobresult

    def initialize(env, params={})
      @sv = params[:sv] || SiView::MM.new(env)
      @apc = APC::Client.new(env, params)
    end

    def inspect
      "#<#{self.class.name} #{@apc.inspect}>"
    end

    # submit a job for the integration test controller with a pre-defined test action
    # like InhibitEquipment, InhibitChamber, LotHold, etc.; return response on success
    def testaction(testcase, params={})
      $log.info "testaction #{testcase.inspect}, #{params}"
      ctx = {
        apcActionType: 'ITDC.IntegrationTest', eventType: 'ProcessCompletedForFDC',
        function: 'FaultActionExectionTest', testcase: testcase,
        controlJobId: params[:cj] || unique_string('QA-'), operationID: params[:op] || 'QADUMMYPD.01'
      }
      ctx[:processingAreaId] = params[:chamber] if params.has_key?(:chamber)
      ctx[:reticle] = params[:reticle] if params.has_key?(:reticle)
      # for sampling
      ctx[:ProcessLayer] = params[:ProcessLayer] if params.has_key?(:ProcessLayer)  # PD UDATA ProcessLayer
      ctx[:ProcessType] = params[:ProcessType] if params.has_key?(:ProcessType)     # PD UDATA ProcessTypeWfrSelection
      #
      @dcr = APC::DCR.new(nil, params[:eqp] || 'QADUMMYEQP', ctx: ctx)
      return @jobresult = @apc.submit_dcr(@dcr.context, params[:data] || {})
    end

    # submit a CPC dcr, :slots and :readings are typical parameters; return jobresult
    def submit_cpcdcr(li, eqp, params)
      li = @sv.lot_info(li, wafers: true) if li.instance_of?(String)
      # pass extra context element, for future use
      @dcr = APC::DCR.new(li, eqp, slots: params[:slots], ctx: {CPC_TEST_PARAM_PATTERN: 'TEST-*'})
      @dcr.add_readings(params[:readings])
      return @jobresult = @apc.submit_dcr(@dcr)
    end

    # find entry for a calculated parameter in jobdata as returned above, pass nil to get all
    def find_cpcentry(pname)
      ret = []
      @jobresult.values.each {|resname|  # like 'results5'
        entry = resname['parameter_list']
        if entry && (pname.nil? || entry.values.first['feature'].split.include?(pname))  # wafer level: 'pname pname'
          ret << entry.values.first.merge(var: entry.keys.first)        # flatten structure to a plain Hash
        end
      }
      return (ret.size < 2 ? ret.first : ret)  # returns nil if ret is empty
    end

    # ref is a Hash like {var: 'APC-DC-FLOAT', lot: e} or {var: 'APC-DC-STRING', wafer: e}
    # return true on success
    def verify_cpcentry(entry, ref)
      if entry.instance_of?(String)
        res = find_cpcentry(entry)
        ($log.warn "no result for #{entry.inspect}"; return) unless res
        # currently String is additionally reported as Float, filter
        if res.instance_of?(Array) && ref[:var] == 'APC-DC-STRING'
          res.select! {|e| e[:var] == 'APC-DC-STRING'}
          res = res.first if res.size < 2
        end
        ($log.warn "#{res.size} results for #{entry.inspect}:\n#{res.pretty_inspect}"; return) if res.instance_of?(Array)
        entry = res
      end
      $log.info "verify_cpcentry\n    #{entry}"
      ($log.warn "  wrong variable: #{entry[:var]}"; return) if entry[:var] != ref[:var]
      # levels
      lot = @dcr.context[:lotID]
      slots = ref[:slots] ##|| @dcr.wafers
      sites = @dcr.sites
      ($log.warn '  either lot, slots or sites must be passed'; return) unless lot || slots || sites
      case ref[:level]
      when :lot
        vref = ref[:value]
        if vref == :tstart
          tstamp = entry['value']
          tstamp += 'Z' unless tstamp.end_with?('Z')
          tdiff = (Time.parse(tstamp) - @apc.catalyst.txtime).abs
          ($log.warn "  wrong time value: #{entry['value'].inspect} instead of #{@apc.catalyst.txtime}"; return) if tdiff > @apc.catalyst.duration
        else
          vstring = vref == :lot ? lot : vref.to_s
          ($log.warn "  wrong value: #{entry['value'].inspect} instead of #{vstring.inspect}"; return) if entry['value'] != vstring
        end
        ($log.warn '  missing lotID'; return) unless entry.has_key?('lotID')
        ($log.warn "  wrong lotID: #{entry['lotID']} instead of #{lot}"; return) if entry['lotID'] != lot
      when :wafers
        wstring = slots.collect {|slot| @dcr.li.wafers[slot - 1].wafer}.join(' ')
        # wstring = wafers.join(' ')
        ($log.warn "  wrong waferID: #{entry['waferID']} instead of #{wstring}"; return) if entry['waferID'] != wstring
        vstring = ref[:value] == :wafers ? wstring : ref[:value].join(' ')
        ($log.warn "  wrong value: #{entry['value'].inspect} instead of #{vstring.inspect}"; return) if entry['value'] != vstring
        ($log.warn '  missing waferID'; return) unless entry.has_key?('waferID')
      when :sites
        vstring = ref[:value].join(' ')
        ($log.warn "  wrong value: #{entry['value'].inspect} instead of #{vstring.inspect}"; return) if entry['value'] != vstring
        # match the ref keys' values for keys of type String
        ok = true
        ref.each_pair {|k, v|
          next unless k.instance_of?(String)
          next if entry[k] == v
          $log.warn "  wrong value for #{k.inspect}: #{entry['value'].inspect} instead of #{v.inspect}"
          ok = false
        }
        return unless ok
      end
      #
      return true
    end

  end


  # receive APC msgs to the EI via MQ and send OK or Fault responses
  class EIMQ
    attr_accessor :eqp, :mq

    def initialize(env, eqp, params={})
      @eqp = eqp
      @mq = MQ::JMS.new(env, qname: "CEI.#{eqp}_REQUEST01", use_jms: false)
    end

    def receive_msg(params={})
      msg = @mq.receive_msg(timeout: params[:timeout] || 20) || return  # APC msgtimeout is 10 s, add time for app run
      return XMLHash.xml_to_hash(msg, '*//*Body')
    end

    def send_response_ok
      h = {'env:Envelope': {'xmlns:env'=>'http://schemas.xmlsoap.org/soap/envelope/', 'xmlns:ns'=>'http://www.amd.com/schemas/baselineservices',
        'env:Body': {'ns:genericFunctionResponse': {
          'env:encodingStyle'=>'http://schemas.xmlsoap.org/soap/encoding/', resultList: {nameValuePair: {name: 'STATUS', value: 'OK'}}
        }}
      }}
      return @mq.send_response(XMLHash.hash_to_xml(h))
    end

    def send_response_fault(s="QA Test Fault #{unique_string}")
      h = {'env:Envelope': {'xmlns:env'=>'http://schemas.xmlsoap.org/soap/envelope/', 'xmlns:ns'=>'http://www.amd.com/schemas/baselineservices',
        'env:Body': {'env:Fault': {faultcode: 'env:Server', faultstring: s, detail: {'ns:CEIInternalException': nil}}}
      }}
      return @mq.send_response(XMLHash.hash_to_xml(h))
    end

  end

end
