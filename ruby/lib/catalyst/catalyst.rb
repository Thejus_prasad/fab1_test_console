=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.


Author: Steffen Frieske, 2016-09-06

Version: 2.0.5

History:
  2016-12-05 sfrieske, improved success search for testaction
  2017-11-16 sfrieske, separated BL tests from Catalyst communication
  2021-02-05 sfrieske, use instance_of instead of kind_of
  2021-08-30 sfrieske, mved xmlhash.rb to util

Notes:
  Catalyst applog: <@@apc.http.uri.to_s>/logs/applog/
  Catalyst logs ITDC: http://drsapc/apcsetup/cajallog/log_fab1_dev.html
  Per server logs (MQ): \\sfc1catd02\log\system
=end

ENV.delete('http_proxy')

require 'rqrsp_http'
require 'mqjms'
require 'util/xmlhash'


# Catalyst communication, for APC tests via SubmitJob
module Catalyst

  # process data for submit job requests and responses
  module JobData

    # take a hash of context information and expand it to a hash for hash_to_xml, TODO: use REXML directly
    def expand_context(ctxdata)
      return {item: ctxdata.each_pair.collect {|k, v| {name: k.to_s, value: v.to_s}}}
    end

    # take a hash of data and expand it to a hash for hash_to_xml, TODO: use REXML directly
    def expand_data(entries)
      entries = [entries] unless entries.instance_of?(Array)
      ret = entries.collect {|entry|  # entry is either a hash or nil
        if (entry.nil? || entry.empty?)
          entry
        else
          entry.each_pair.collect {|k, v|
            v.instance_of?(Hash) ? {name: k.to_s, data: expand_data(v)} : {name: k.to_s, value: v.to_s}
          }
        end
      }
      ret = ret.size == 1 ? ret.first : ret
      return ret.nil? || ret.empty? ? ret : {dataEntry: ret}
    end

    # # prepare data to be sent over MQ
    # def wrapped_xml(context, data)
    #   ##s = XMLHash.hash_to_xml({'version'=>'1.0', context: expand_context(context), data: expand_data(data)}, :controlData, xml)
    #   s = XMLHash.hash_to_xml({context: expand_context(context), data: expand_data(data)}, :controlData) #, xml)
    #   h = {'env:Envelope': {'xmlns:env'=>'http://schemas.xmlsoap.org/soap/envelope/', 'xmlns:xsd'=>'http://www.w3.org/2001/XMLSchema',
    #     'xmlns:xsi'=>'http://www.w3.org/2001/XMLSchema-instance', 'env:Body': {'ns1:submitJob': {
    #       'xmlns:ns1'=>'urn:Catalyst', 'env:encodingStyle'=>'http://schemas.xmlsoap.org/soap/encoding/', jobParametersXML: s
    #     }}
    #   }}
    #   return XMLHash.hash_to_xml(h)
    # end

    # unwrap result from a SOAP MQ response (submitJobReturn tag), return Hash for e.g. ControlResults
    def unwrapped_xml(msg)
      res = XMLHash.xml_to_hash(msg, '*//*Body') || return
      res = res[:submitJobResponse][:submitJobReturn][nil] || return
      res = XMLHash.xml_to_hash(res)
    end

    # extract jobResults from a result hash (regardless of MQ or HTTP transport)
    def extract_jobresult(res)
      ($log.warn res; return) unless res[:controlResults][:status][:statusCode] == '0'
      return {} unless res[:controlResults][:jobResults]
      dentries = res[:controlResults][:jobResults][:data][:dataEntry]
      dentries = [dentries] unless dentries.instance_of?(Array)
      return Hash[dentries.collect {|e| e.values}]
    end

    # extract JobData from jobResults hash and return a condensed hash
    def extract_data(dentries)
      ret = {}
      lastname = nil
      dentries = [dentries] unless dentries.instance_of?(Array)
      dentries.each {|dentry|
        name = dentry[:name]
        if name == 'Value'  # last leaf, there is only one
          if ret.instance_of?(Array)  # not empty Array
            ret.push(dentry[:value])
          elsif ret.empty?        # empty Hash
            ret = dentry[:value]
          else    # not empty Hash
            ret = [ret, dentry[:value]]
          end
        else
          if dentry.has_key?(:value)  # nil is allowed
            ret[name] = dentry[:value]
          else
            if data = dentry[:data]
              ret[name] = extract_data(data[:dataEntry])
            else
              $log.warn "unknown entry type: #{dentry.inspect}"
            end
          end
        end
      }
      return ret
    end

    # # find a dataEntry in the response by name and value, return matching data as Hash
    # def find_entry(res, name, value=nil)
    #   return unless res['JobData'][:dataEntry].instance_of?(Array)  # no APC result struct
    #   res['JobData'][:dataEntry].each {|jd|
    #     entry = jd[:data][:dataEntry].find {|e| e[:name] == name && (value.nil? || (e[:value] == value))}
    #     return {entry[:name]=>entry[:value]} if entry
    #   }
    #   return nil
    # end

    # parse a string as Tcl dictionary, return Hash (for PilotManager dev)
    def dictstring_to_hash(s)
      tokens = []
      partial = nil
      pdepth = 0  # number of open brackets
      s.split.each {|e|
        if !partial && e.start_with?('{') && !e.end_with?('}')
          partial = e
          pdepth += 1
        elsif partial
          partial += ' ' + e
          pdepth += e.count('{') - e.count('}')
          if pdepth == 0
            tokens << partial[1..-2]
            partial = nil
          end
        else
          tokens << e
        end
      }
      ($log.warn 'odd size of dictionary tokens'; return) if tokens.size % 2 != 0
      return Hash[(0..tokens.length - 1).step(2).collect {|i| [tokens[i], tokens[i+1]]}.sort]
    end

    # convert a Hash to a string representing a TCL dictionary (for PilotManager)
    def hash_to_dictstring(h)
      return h.each_pair.collect {|k, v|
        k = "{#{k}}" if k.include?(' ')
        v = "{#{v}}" if v.include?(' ')
        "#{k} #{v}"
      }.join(' ')
    end

  end


  # communication via HTTP
  class HTTPClient
    include JobData
    attr_accessor :http, :txtime, :duration

    def initialize(env, params={})
      @http = RequestResponse::Http.new("cat.#{env}", params)
    end

    def inspect
      "#<#{self.class.name} #{@http.inspect}>"
    end

    # submit job with dummy data, return true on success
    def ping
      $log.info "ping  #{@http}"
      submitjob({}, {}, extract_jobresults: false)  # it is way faster to send an invalid context
      return unless @http.response
      return @http.response.body.include?('controlResults')
    end

    # submit a Catalyst job, return hash with jobResults (name value pairs consolidated) or nil on error
    def submitjob(context, data, params={})
      # TODO: create XML directly
      s = XMLHash.hash_to_xml({context: expand_context(context), data: expand_data(data)}, :controlData)
      @duration = nil
      @txtime = Time.now
      res = @http.post("controlData=#{s}", resource: 'submitjob', timeout: params[:timeout]) || return
      @duration = Time.now - @txtime
      return res if @http.nosend || params[:timeout] == 0 || params[:extract_jobresults] == false
      res = XMLHash.xml_to_hash(res)
      return extract_jobresult(res)
    end

  end


  # communication via MQ
  class MQClient
    include JobData
    attr_accessor :mq, :mqr, :txtime, :duration, :_s, :_doc  # _xxx is for dev only

    def initialize(env, params={})
      @mq = MQ::JMS.new("cat.#{env}", {reply_queue: nil})
    end

    def inspect
      "#<#{self.class.name} #{@mq.inspect}>"
    end

    # submit job with dummy data, return true on success
    def ping
      $log.info "ping  #{@mq.inspect}"
      res = submitjob({}, {}, extract_jobresults: false)  # it is way faster to send an invalid context
      return res && res.include?('controlResults')
    end

    # submit a Catalyst job with a tmp reply queue, return hash with jobResults (name value pairs consolidated)
    def submitjob(context, data, params={})
      s = XMLHash.hash_to_xml({context: expand_context(context), data: expand_data(data)}, :controlData)
      @_s = s   # for dev only!
      @_doc = doc = REXML::Document.new('<?xml version="1.0" encoding="UTF-8"?>')
      doc.add_element('env:Envelope', {
        'xmlns:env'=>'http://schemas.xmlsoap.org/soap/envelope/',
        'xmlns:xsd'=>'http://www.w3.org/2001/XMLSchema',
        'xmlns:xsi'=>'http://www.w3.org/2001/XMLSchema-instance',
      }).add_element('env:Body')
      .add_element('ns1:submitJob', {
        'xmlns:ns1'=>'urn:Catalyst',
        'env:encodingStyle'=>'http://schemas.xmlsoap.org/soap/encoding/',
      })
      .add_element('jobParametersXML').text = s
      xml = String.new
      doc.write(xml)
      @mqr = MQ::JMS.new(nil, @mq.params.merge(qname: 'temporary')) || return
      @duration = nil
      @txtime = Time.now
      @mq.send_msg(xml, reply_to: @mqr.queue) || return
      return @mqr if params[:deferred_response]  # for pilotmanager dialog_exit
      msg = @mqr.receive_msg(first: true)  ##, timeout: params[:timeout])
      @duration = Time.now - @txtime
      @mqr.close
      return msg if msg.nil? || (params[:extract_jobresults] == false)  # extract_jobresults mainly for ping
      return extract_jobresult(unwrapped_xml(msg))
    end

  end


  # convenience method to create either an HTTP or an MQ client (default)
  def self.Client(env, params={})
    return params[:use_http] ? HTTPClient.new(env, params) : MQClient.new(env, params)
  end

end
