=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: Steffen Steidten, 2009-05-03

History:
  2009-09-04 ssteidte, changed id() to oid() to avoid a name clash
  2011-08-01 ssteidte, moved auxillary methods to misc.rb
  2011-08-16 ssteidte, moved easysiview_*.rb files to an "easysiview" subdirectory
  2012-05-10 ssteidte, load JCS files during class instantiation
  2012-06-30 ssteidte, support for AsmView and SiView6 for Fab7
  2013-08-20 ssteidte, moved parse_ior to a seaparate class, improved parsing
  2013-12-18 ssteidte, refactored EasySiViewR, SMFacade, XM, AsmViewR and Fab7 SiView
  2015-01-28 ssteidte, exclude src and javadoc files when requiring JCS files
  2015-06-22 ssteidte, added siview_time and siview_timestamp as instance methods
  2015-08-31 ssteidte, preferrably use properties from etc/siview.conf
  2016-06-13 sfrieske, rc() returns with an error if the result is a string like "Incoming"
  2017-07-20 sfrieske, dropped support for old etc/connections/<env>.properties
  2017-08-04 sfrieske, moved nt_pw, siview_pw and siview_next from String to SiView
  2018-04-18 sfrieske, minor cleanup
  2020-09-14 sfrieske, created oids() for arrays of strings
  2020-10-19 sfrieske, added reason code cache
  2021-02-05 sfrieske, use instance_of instead of kind_of
  2021-08-26 sfrieske, moved get_password from misc.rb here, use read_config instead of _config_from_file
=end

require 'corba'
require 'util/readconfig'
require 'util/uniquestring'


# Wrapper around the SiView JCSen
module SiView

  def self.compare_statistics(resultfiles=nil, params={})
    alldata = []
    resultfiles ||= 'log/svlog_perf*.yaml'
    allfiles = Dir[resultfiles]
    $log.info "compare_statistics:\n#{allfiles.pretty_inspect}"
    datacolumns = nil
    allreleases = []
    allfiles.each {|f|
      yaml_content = YAML.load_file(f)
      alldata = alldata | yaml_content[:data]
      datacolumns ||= yaml_content[:col][2..-1]
      yaml_content[:data].each {|d| allreleases << d[2][0] unless allreleases.include?(d[2][0])}
    }
    allreleases = allreleases.sort
    datacolumns = datacolumns.collect {|c| c.to_s.capitalize}  #datacolumns.join(',').capitalize.split(',')
    displaycols = (params[:cols] || 'Avg').gsub(',', ' ').split.collect {|c| c.capitalize}
    indexSeq = displaycols.collect {|dc| datacolumns.index(dc)}

    releaseHeaders = allreleases.collect {|rel| 'R' + rel.to_s + ',' * (indexSeq.size-1)}.join(',')
    pt, pv, pr = '', '', ''
    data = 'TxName, TxVersion, ' + releaseHeaders + "\r\n," + (',' + displaycols.join(',') ) * allreleases.size

    alldata.sort.each {|d|
      displayData = indexSeq.collect {|i| d[2][1][i]}.join(',')
      if d[0] == pt && d[1] == pv
        data +=  ',' * indexSeq.size * (allreleases.index(d[2][0]) - allreleases.index(pr) - 1) + ',' + displayData if  pr != d[2][0]
      else
        data += "\r\n" + d[0..1].join(',="  ') + '"' +  ',' * indexSeq.size * allreleases.index(d[2][0]) + ',' + displayData
      end
      pt = d[0]
      pv = d[1]
      pr = d[2][0]
    }
    separator = params[:separator]  # for temp data only!
    data.gsub!(',', separator) if separator
    env = params[:env] || 'ENV'
    tag = params[:tag] || Time.now.utc.strftime('%F_%T').gsub(':', '-')
    fname = "log/svlog_comp_#{env.upcase}_#{tag}.csv"
    open(fname, 'wb') {|fh| fh.write(data)}
    return fname
  end

  def self.nt_pw(s)
    require 'openssl'
    return '[MD4]' + [OpenSSL::Digest::MD4.digest(s.encode('UTF-16LE'))].pack('m') + '[/MD4]'
  end

  def self.siview_pw(s, offset=1)
    return s.reverse.chars.collect {|c| (c.ord + offset).chr}.join
  end

  # get the Base34 string incremented by the value of carry (defaulting to 1, pass -1 to decrement)
  def self.siview_next(s, carry=1)
    alphabet = '0123456789ABCDEFGHJKLMNPQRSTUVWXYZ'
    digits = s.reverse.chars.to_a
    digits.each_with_index {|d, i|
      pos = alphabet.index(d)
      carry, newd = (pos + carry).divmod(alphabet.size)
      digits[i] = alphabet[newd]
      break if carry.zero?
    }
    digits.join.reverse
  end

  # convert LotTkInfo to an ERP/SiViewF7 string
  def self.tkinfo_to_erp(tkinfo)
    subcons = self._subcon_string(tkinfo.tkbump)
    subcons += self._subcon_string(tkinfo.tksort)
    unless ['', nil].member?(tkinfo.tkreflow)
      subcons += '1' if tkinfo.tkreflow == tkinfo.tkbump
      subcons += '2' if tkinfo.tkreflow == tkinfo.tksort
    end
    subcons += self._subcon_string(tkinfo.tkassy)
    subcons += self._subcon_string(tkinfo.tktest)
    return subcons.gsub(/00+$/, '')
  end

  # aux method for subconid creation
  def self._subcon_string(sc)
    (sc.nil? || sc == '') ? '00' : sc
  end


  class InstrumentedServiceManager
    attr_accessor :sv, :manager, :durations

    def initialize(sv)
      @sv = sv
      @manager = @sv.manager
      @durations = []
    end

    def inspect
      "#<#{self.class.name} #{@sv}>"
    end

    def method_missing(meth, *args, &block)
      tstart = Time.now
      res = @manager.send(meth, *args, &block)
      @durations << [meth, ((Time.now - tstart) * 1000).floor]  # ms as in svlog
      return res
    end

    def respond_to?(meth)
      @manager.public_methods.member?(meth)
    end

    def get_statistics(params={})
      svrelease = params[:svrelease] || @sv.release
      release = params[:release] || @sv.release
      res = {}
      @durations.each {|e|
        tx, duration = e
        res[tx] = [] unless res.has_key?(tx)
        res[tx] << duration
      }
      data = res.each_pair.collect {|tx, durations|
        txname, version = tx.to_s.split('__')
        version ||= ''
        std = durations.standard_deviation
        std = 0 if std.nan?
        [txname, version, [svrelease, [durations.avg.to_i, std.to_i, durations.min.to_i, durations.max.to_i, durations.size]]]
      }
      ret = {time: Time.now.utc, env: @sv.env, col: [:txname, :txid, :avg, :std, :min, :max, :count], data: data}
      ret[:user] = @sv.user
      ret[:release] = release
      # export data to the passed file name or generate one if export: true was passed
      if fname = params[:export]
        fname = "log/svlog_perf_#{@sv.env.upcase}_#{svrelease}_#{release}_#{Time.now.utc.strftime('%F_%T').gsub(':', '-')}.yaml" unless fname.instance_of?(String)
        $log.info "  writing log to #{fname.inspect}"
        File.binwrite(fname, ret.to_yaml)
      end
      return ret
    end
  end


  class Client
    java_import 'org.omg.CORBA.ORB'
    attr_accessor :env, :_flavour, :_props, :_user, :jcscode, :iorparser, :orb, :manager,
      :user, :password, :reasoncodes, :srvtimezone, :_res, :_regexprc

    # read properties from etc/siview.conf and set the SiView connection parameters
    # optionally the user and password can be entered interactively
    def initialize(env, params={})
      @reasoncodes = {} # cache reason codes
      # regexp for returnCode evaluation
      @_regexprc = Regexp.compile(/^\d+$/)
      # get parameters
      @env = env
      instance = params[:instance] || "#{self.class.name.split('::').last.downcase}.#{env}"
      @_props = read_config(instance, 'etc/siview.conf', params)
      $log.info "SiView::Client instance: #{instance}"
      @user = @_props[:user] || @_props[:username]
      @password = @_props[:password]
      @srvtimezone = params[:srvtimezone] || (env.start_with?('f8', 'mtqa') ? '-05:00' : '+00:00')
      # explicitly set, overriding settings from config files
      if @_props.has_key?(:user)
        @user = @_props[:user]
        @password = @_props[:password] # password is nil if not given along with the user
      end
      @password = SiView.siview_pw(@user) if @password == :default
      # read missing username and password from console
      @user ||= (print 'User name: '; gets.chomp)
      unless @password
        # currently not working on Windows   pw = $stdin.noecho(&:gets).chomp
        pw = get_password
        ntauth = params[:ntauth] || (@_props[:ntauth].to_s == 'true')
        @password = ntauth ? SiView.nt_pw(pw) : SiView.siview_pw(pw)
      end
      return if params[:noconnect]
      # connect
      connect
      # optionally collect tx durations
      @manager = InstrumentedServiceManager.new(self) if params[:collect_durations]
    end

    # read a password from the cmdline without displaying it, TODO: $stdin.noecho(&:gets)
    def get_password(msg='Enter Password')
      t = Thread.new {
        loop {
          sleep 0.01
          $stdout.write("\r#{msg}:             \r#{msg}: ")
          $stdout.flush
        }
      }
      inp = $stdin.gets
      t.kill
      return inp.chomp
    end

    # require JCS files from ./lib/_flavour and connect to SiView MM, SM, etc.), override in specific methods
    def connect
      # load JCS files
      @_flavour = @_props[:flavour] || ENV['_flavour'] || 'R15_custom'
      Dir["lib/#{@_flavour}/*jar"].each {|f| require f}
      # get IOR, allowing to override defaults from @_props for SM and XM
      @iorparser = IORParser.new(@_props[:ior] || @_props[:iorfile])
      # create ORB
      @orb = ORB.init([].to_java(:string), nil)
      # @orb = Java::OrgOmgCORBA::ORB.init([].to_java(:string), nil)
      # specific JaCORB ORB, works w/o java_import: @orb = Java::OrgJacorbOrb::ORB.init([].to_java(:string), nil)
    end

    def inspect
      "#<#{self.class.name} #{@env}, user: #{@user}>"
    end

    def user_struct(user, password, params={})
      new_pw = params[:new_pw] || ''
      function = params[:function] || ''
      client_node = params[:client_node] || ''
      password = SiView.siview_pw(user) if password == :default
      @jcscode.pptUser_struct.new(oid(user), password, new_pw, function, client_node, any)
    end

    # return SiView id or list of ids for a string or array of strings, resp.
    def oid(s)
      return @jcscode.objectIdentifier_struct.new(s, '')
    end

    # return java array of objectIdentifier_struct
    def oids(a)
      return a.collect {|e|
        @jcscode.objectIdentifier_struct.new(e, '')
      }.to_java(@jcscode.objectIdentifier_struct)
    end

    def any
      @orb.create_any
    end

    # return SiView timestamp string from a Time object
    def siview_timestamp(t=nil)
      t ||= Time.now
      t = Time.at(t) if t.kind_of?(Numeric)
      t = Time.parse(t) if t.instance_of?(String)
      return t.localtime(@srvtimezone).strftime('%F-%H.%M.%S.%6N')
    end

    # return Time object from a SiView timestamp string
    def siview_time(s)
      return if s.empty?
      Time.strptime("#{s} #{@srvtimezone}", '%Y-%m-%d-%H.%M.%S.%N %Z').localtime(@srvtimezone)
    end

    # log transactionID, returnCode, messageID and messageText on error (returnCode != 0)
    #
    # res is a SiView tx result that is stored as last result in @_res,
    # if not provided the last SiView::Client result is used,
    # rcdetails is an optional array like substructure like strGatePassLotsResult
    #
    # return SiView returnCode as integer or nil on error
    def rc(res=nil, rcdetails=nil)
      # return last return code if no argument given
      return @_res.strResult.returnCode.to_i if res.nil?
      # set last result from res
      @_res = res
      # get return code
      lr = res.strResult
      ($log.error 'invalid returnCode:'; show_result(data: lr); return) unless @_regexprc.match(lr.returnCode)
      ret = lr.returnCode.to_i
      # log errors
      if ret != 0
        unless ret == 1211 && lr.transactionID == 'TXTRQ003'  # LotInfo bug
          $log.warn "#{lr.transactionID} rc: #{ret}, msgID: #{lr.messageID}, msgText: #{lr.messageText}"
          $log.warn "reasonText: #{lr.reasonText.inspect}"
        end
      end
      return ret if rcdetails.nil?
      # optionally calculate ret from rcdetails
      ret = 0
      rcdetails.each {|lr| ret = [ret, lr.returnCode.to_i].max}
      $log.warn show_result(data: rcdetails) unless ret.zero?
      return ret
    end

    # extract the SiView message text without modifying the last result
    def msg_text(res=@_res)
      res.strResult.messageText
    end

    # extract the SiView reason text without modifying the last result
    def reason_text(res=@_res)
      res.strResult.reasonText
    end

    # extract the SiView TX id without modifying the last result
    def tx_id(res=@_res)
      res.strResult.transactionID
    end

    def version
      @manager.getVersion
    end

    def cs_version
      @manager.cs_getVersion
    end

    # return new any object on success or nil
    def insert_si(struct)
      anyobj = @orb.create_any
      begin
        if struct.instance_of?(String)
          anyobj.insert_string(struct)
        elsif struct.kind_of?(Numeric)  # was: Fixnum
          anyobj.insert_ulong(struct)
          ## TODO add other basic types if needed
        else
          jclass = struct.java_class
          if jclass.array?
            cname = jclass.component_type.simple_name.sub('_struct', 'Sequence')
          else
            cname = jclass.simple_name
          end
          helper = @jcscode.const_get("#{cname}Helper")
          helper.insert(anyobj, struct)
        end
        return anyobj
      rescue
        puts $!
        return
      end
    end

    def extract_si(si)
      tk = IORParser.type_kind(si)
      return if tk == :tk_null
      if tk == :tk_string
        return si.extract_string
      elsif tk == :tk_struct || tk == :tk_alias  # SiView struct, :tk_alias e.g. pptUserDataSequence
        begin
          helper = @jcscode.const_get("#{si.type.name}Helper")
          return helper.extract(si)
        rescue NameError
          $log.info "#{si.type.name}Helper not found in JCS"
          return
        end
      elsif tk == :tk_any
        return '(Any)'  # this is insane: having an any obj of kind Any w/o type name
      else
        return "unhandled type (#{tk.inspect})"
      end
    end

    def show_result(params={})
      o = params[:data]# || @_res
      # if params.instance_of?(Hash)
      #   o = params[:data] || @_res
      # else
      #   o = params.clone
      #   params = {}
      # end
      max_depth = params[:max_depth] || 16
      level = params[:level] || -1
      #
      begin
        if level == -1
          o = @_res unless params.has_key?(:data)
          ret = o.respond_to?(:java_class) ? "# #{o.java_class.name} " : '# Ruby object '
          ret += show_result(params.merge(data: o, level: 0))
          fname = params[:export]
          fname ? File.binwrite(fname, ret) : STDOUT.write(ret)
          return
        end
        return "## cut off at max depth #{max_depth}" if level > max_depth
        #
        lindent = '  ' * level
        if o.instance_of?(String) || o.kind_of?(java.lang.String)
          return o.inspect
        elsif o.kind_of?(Java::OrgOmgCORBA::Any)
          tk = IORParser.type_kind(o)
          return 'nil' if tk == :tk_null
          ret = "# CORBA Any, "
          begin
            ret += o.type.name if tk == :tk_struct || tk == :tk_alias
            return ret + show_result(params.merge(data: extract_si(o), level: level + 1))
          rescue => e
            $log.debug {$!}
            $log.debug {e.backtrace.join("\n")}
            ret += " ### extract_si: Bad Type (#{tk.inspect})###"
          end
        elsif o.instance_of?(Array)
          return "## #{o.class}" + o.collect {|e|
            "\n#{lindent}- #{show_result(params.merge(data: e, level: level + 1)).lstrip}"
          }.join
        elsif o.respond_to?(:java_class)
          if o.java_class.array?
            return "## #{o.java_class.name}" + o.collect {|e|
              "\n#{lindent}- #{show_result(params.merge(data: e, level: level + 1)).lstrip}"
            }.join
          else
            # java class, assuming SiView struct
            ff = o.java_class.fields
            return ff.collect {|f|
              next if %w(stringifiedObjectReference).member?(f.name) && !params[:objref]
              "\n#{lindent}#{f.name}: #{show_result(params.merge(data: o.send(f.name), level: level + 1))}"
            }.compact.join
          end
        else
          return o.inspect
        end
      rescue
        $log.debug "show_result error: #{$!.inspect}"
        return "### show_result error ###"
      end
    end

    def result_to_hash(obj=@_res)
      return if obj.nil?  # e.g. empty siInfo
      return result_to_hash(extract_si(obj)) if obj.kind_of?(Java::OrgOmgCORBA::Any)
      return obj.collect {|e| result_to_hash(e)} if obj.respond_to?(:java_class) && obj.java_class.array?
      res = {}
      obj.java_class.fields.each {|field|
        fieldval = obj.send(field.name)
        if fieldval.kind_of?(Java::OrgOmgCORBA::Any)
          fvalue = result_to_hash(extract_si(fieldval))
        elsif fieldval.respond_to?(:java_class)
          fvalue = result_to_hash(fieldval)
        else
          fvalue = fieldval
        end
        res[field.name] = fvalue
      }
      return res
    end

  end


  load 'siview/xm.rb'
  load 'siview/sm.rb'
  load 'siview/mm.rb'
end
