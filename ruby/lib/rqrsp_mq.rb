=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author:     sfrieske, 2016-08-19

Version:    1.1.2

History:
  2016-08-19 sfrieske, re-write of rqrsp.rb to reduce overhead
  2017-01-02 sfrieske, minor cleanup
  2017-07-12 sfrieske, added (back) txtime and duration
  2018-04-09 sfrieske, supported and now preferred to pass a hash to MQXML#send_receive
  2021-02-05 sfrieske, use instance_of instead of kind_of
  2021-08-26 sfrieske, use read_config instead of _config_from_file
  2021-12-02 sfrieske, simplified initialize
  2022-01-12 sfrieske, MQXML has fixed bodytag
=end

require 'rexml/document'
require 'mqjms'
require 'util/readconfig'
require 'util/uniquestring'
require 'util/xmlhash'


module RequestResponse
  # Request - Response Communication over 2 MQ Queues
  class MQ
    attr_accessor :instance, :params, :user, :correlation, :timeout, :bytes_msg, :nosend,
      :mq_request, :mq_response, :request, :response

    def initialize(instance, params={})
      $log.info "RequestResponse::MQ instance: #{instance}"
      @instance = instance
      @params = read_config(instance, 'etc/mq.conf' || params[:config], params)
      use_jms = (@params[:use_jms] != false)
      @correlation = @params[:correlation] || (use_jms ? :corrid : :msgid)  # should go to MQJMS
      @bytes_msg = @params[:bytes_msg]
      @mq_response = ::MQ::JMS.new(nil, @params.merge(qname: @params[:reply_queue], reply_queue: nil))
      @mq_request = ::MQ::JMS.new(nil, @params.merge(reply_queue: @mq_response.queue))
      @timeout = @params.has_key?(:timeout) ? @params[:timeout] : 60
      @nosend = false
      ## $log.info  ## not working well with subclassed self.inspect
    end

    def inspect
      "#<#{self.class.name} #{@instance.inspect}#{' user: ' + @user if @user}>"
    end

    def close
      @mq_request.close
      @mq_response.close
    end

    # return the time the message was sent
    def txtime
      @mq_request.txtime
    end

    # return the time between sending and receiving the response
    def duration
      @mq_response.duration
    end

    # send string s, receive the reply message, return the reply or nil on error
    def send_receive(s, params={})
      File.binwrite(params[:export], s) if params[:export]
      @request = s  # for dev
      return s if @nosend
      # generate corrid unless one is provided or nil explicitly passed to force automatic generation
      if @correlation == :corrid && !params.has_key?(:correlation)
        corrid = "C#{unique_string}#{Thread.current.object_id}"
      else
        corrid = nil
      end
      # send msg
      $log.debug {"send_msg correlation: #{corrid.inspect}"}
      res = @mq_request.send_msg(s, {bytes_msg: @bytes_msg, correlation: corrid}.merge(params))
      ($log.warn 'error sending MQ message'; return) unless res
      msgid, corrid = res
      # set default correlation, can be overridden by params[:correlation]
      if @correlation == :none
        corr = nil
      elsif @correlation == :corrid
        corr = corrid
      elsif @correlation == :msgid
        corr = msgid
      end
      # receive response
      $log.debug {"receive_msg correlation: #{corr.inspect}"}
      return @response = @mq_response.receive_msg({correlation: corr, first: true, timeout: @timeout}.merge(params))
    end

  end

  # Send XML string, receive and parse SOAP response
  class MQXML < MQ
    attr_accessor :bodytag, :service_response

    def initialize(instance, params={})
      super
      @bodytag = '*//*Body'
      # TODO: @bodytag = '/*/*Body'
    end

    # send hash (or XML string, deprecated), receive the reply message and return the parsed reply as Hash or nil on error
    def send_receive(data, params={})
      if data.instance_of?(String)
        s = data
      elsif data.instance_of?(REXML::Document)
        s = String.new
        data.write(s)
      else
        s = XMLHash.hash_to_xml(data)
      end
      res = super(s, params) || return
      return res if @nosend || params[:raw]   # pretty pointless, consider using RequestResponse::MQ instead
      return @service_response = XMLHash.xml_to_hash(res, @bodytag)
      # TODO: return @service_response = XMLHash.xml_to_hash(res, @bodytag).values.first
    end

  end

end
