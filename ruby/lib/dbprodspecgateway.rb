# (c) Copyright 2016 GLOBALFOUNDRIES Inc. All rights reserved.
#     GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
#     combinations thereof are trademarks of GLOBALFOUNDRIES Inc.
# Author: Daniel Steger <daniel.steger@globalfoundries.com>
#         A. Schiebelbein
#
# Version: 0.0.1
#
# History:
#

require 'time'
require 'rexml/document'
require 'rqrsp_mq'
require 'util/uniquestring'

# DBPRODSPEC_GATEWAY interfaces

  class DBProdSpecGateway 
    attr_accessor :mq, :erpitem, :tktype, :srcfab, :_doc, :env
    
    def initialize(env, params = {})
	 @erpitem = params[:erpitem] || '0000001VN343'
     @tktype = params[:tktype] || 'R'
     @srcfab = 'F8'
     @mq = RequestResponse::MQXML.new("dbprodspecgateway.#{env}", {correlation: :msgid}.merge(params))
     @env = env
    end

    # technology info
    def technology_info(tech, params = {})
	 @_doc = doc = REXML::Document.new('<?xml version="1.0" encoding="UTF-8"?>')
    tag1 = doc.add_element('soapenv:Envelope', {
      'xmlns:soapenv'=>'http://www.w3.org/2003/05/soap-envelope',
      'xmlns:urn'=>'urn:FabSpecServiceOperations'
    })
    .add_element('soapenv:Body')
    .add_element('getTechnologyInfo')
    tag1.add_element('urn:technologyId').text = tech
    res = @mq.send_receive(doc) || return
    end
	
end
