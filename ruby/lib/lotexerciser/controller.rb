=begin
(c) Copyright 2012 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: Ulrich Koerner (Saxonia), Steffen Steidten, 2012-08-06

History:
  2015-10-09 sfrieske, cleanup
  2018-07-13 sfrieske, removed DRB RemoteController
  2021-02-05 sfrieske, use instance_of instead of kind_of
  2021-08-26 sfrieske, removed dependency on misc.rb (duration_in_seconds, duration_string)
=end

require 'pp'
require 'util/durationstring'
require 'siview'


module LotExerciser
  class Controller
    attr_accessor :sv, :starting, :running, :done, :duration, :tstart, :tend,
      :reporter, :lotagent, :shipagent, :tgagents, :opnoagents, :tsleep, :threads

    def initialize(env, params={})
      @sv = params[:sv] || SiView::MM.new(env)
      @threads = []
      # lot creation and distribution along the route
      @lotagent = LotAgent.new(env, params.merge(sv: @sv))
      # tools, grouped by PD
      @tgagents = []
      proctime = params[:proctime]
      (params[:tools] || []).each_with_index {|tg, i|
        pt = proctime.instance_of?(Array) ? proctime[i] : proctime
        @tgagents << ToolGroupAgent.new(env, tg, params.merge(proctime: pt))
      }
      # actions at special opNos, resolving "first" and "last"
      rte = @lotagent.lot_defaults[:route]
      rops = rte ? @sv.route_operations(rte).to_a : []
      @opnoagents = []
      if opNos = params[:opNos]
        opNos.each_pair {|opNo, args|
          if args.instance_of?(Hash)
            tgt = args.delete(:target)
          else
            tgt = args
            args = {}
          end
          opNo = rops.first.operationNumber if opNo.to_s == 'first'
          opNo = rops.last.operationNumber if opNo.to_s == 'last'
          tgt = rops.first.operationNumber if tgt.to_s == 'first'
          tgt = rops.last.operationNumber if tgt.to_s == 'last'
          tgt = nil if tgt.to_s == 'gatepass'
          if tgt.to_s == 'ship'
            @shipagent = ShipAgent.new(env, opNo, params.merge(args).merge(lotagent: @lotagent))
          else
            @opnoagents << OpNoAgent.new(env, opNo, tgt, params.merge(args).merge(lotagent: @lotagent))
          end
        }
      end
      # reporting
      @reporter = Reporter.new(self, params)
    end

    def inspect
      "#<#{self.class.name} env: #{@sv.env}>"
    end

    def start(params={})
      @starting = true
      stop
      $log.info "starting exerciser #{params}"
      @duration = params[:duration] || 120
      s = duration < 0 ? 'forever' : "#{@duration} until #{(Time.now + @duration).iso8601}"
      $log.info "\nload test, duration #{s}"
      ($log.info 'dryrun only, not starting'; return) if params[:dryrun]
      @done = false
      @tend = nil
      # prepare lots and eqps
      cleanup_eqp
      if @opnoagents.size + @tgagents.size > 0
        @lotagent.find_lots
        @lotagent.cleanup_lots
        @lotagent.distribute_lots(params) if params[:distribute_lots] != false
      end
      # start the agents
      return if !@starting || @done
      @tstart = Time.now
      @threads = (@tgagents + @opnoagents + [@shipagent, @reporter]).compact.collect {|a| a.start(params)}
      @running = true
      @starting = false
      $log.info 'started exerciser'
      #
      # stop test after :duration seconds, defaulting to 120, -1 sleep forever
      @tsleep = Thread.new {  # make it interruptable
        @duration < 0 ? sleep : sleep(@duration)
        stop(from_thread: true)
        $log.info show_status
      }
      return self if params[:async]
      @tsleep.join
    end

    def stop(params={})
      return if @done && !@running
      $log.info 'stopping exerciser'
      @running = false
      @tend = Time.now
      @tsleep.kill if @tsleep && !params[:from_thread]
      (@tgagents + @opnoagents + [@shipagent, @reporter]).compact.each {|a| a.stop}
      @threads.each {|t| t.join}
      $log.info 'stopped exerciser'
      @done = true
    end


    def cleanup_eqp
      ths = @tgagents.collect {|tga|
        Thread.new {tga.cleanup_eqp}
      }
      ths.each {|t| t.join}
    end

    def lot_distribution(params={})
      @lotagent.lot_distribution(params.merge(sv: @sv))
    end

    def opecomps(params={})
      return if @tgagents.empty?
      ret = [0, 1].collect {|i| @tgagents.collect {|p| p.counter[i]}.inject(:+)}
      return ret unless params[:perhour]
      d = ((@tend || Time.now) - @tstart) / 3600.0
      return (ret[0]/d).round, (ret[1]/d).round
    end

    def shipments(params={})
      return unless @shipagent
      ret = @shipagent.counter
      return ret unless params[:perhour]
      d = ((@tend || Time.now) - @tstart) / 3600.0
      return (ret[0]/d).round, (ret[1]/d).round
    end

    def status(params={})
      ret = {'exerciser'=>{starting: @starting, running: @running, done: @done}}
      (@tgagents + @opnoagents + [@shipagent, @reporter]).compact.each {|a| ret.merge!(a.status(params))}
      return ret
    end

    def show_status
      return 'not yet started' unless @tstart
      etime = @tend || Time.now
      msg = "\nLotExerciser #{@sv.env.upcase} #{@tstart.strftime('%F_%T')} .. #{etime.strftime('%F_%T')} (#{duration_string(etime - @tstart)})"
      msg += @tend ? ', done' : ', running'
      msg += "\n"+ '=' * (msg.size-1) + "\nopecomps: #{opecomps.inspect} (#{opecomps(perhour: true).inspect} per hour)\n"
      msg += "\n#{status.pretty_inspect}\n"
      if @shipagent
        msg += "shipments: #{shipments.inspect} (#{shipments(perhour: true).inspect} per hour)\n"
        ls = @shipagent.shiplist
        msg += "\n#{ls}\n" if ls
      end
      msg
    end

  end

end
