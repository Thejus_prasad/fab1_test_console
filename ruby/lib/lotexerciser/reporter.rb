=begin
(c) Copyright 2012 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: Ulrich Koerner (Saxonia), Steffen Steidten, 2012-08-06

History:
  2015-10-09 sfrieske, cleanup
  2021-08-26 sfrieske, removed dependency on misc.rb (duration_in_seconds)
=end


module LotExerciser

  class Reporter < Agent
    attr_accessor :controller, :reportfile, :reportinterval, :smtpserver, :smtp, :sender, :recipients, :response, :tsleep

    @@smtpservers = {'let'=>'drssmtp', 'itdc'=>'drssmtp'}

    def initialize(controller, params={})
      @name = params[:name] || 'reporter'
      @controller = controller
      @counter = [0, 0]
      @reportfile = params[:reportfile]
      @reportinterval = params[:reportinterval]
      @smtpserver = params[:smtpserver] || @@smtpservers[@controller.sv.env] || 'localhost'
      @recipients = params[:recipients] || params[:recipient] || params[:mailto]
      @sender = params[:sender] || ''
    end

    def execute
      if @reportinterval
        # send report immediately after a start to prove it works
        report_status
        @tsleep = Thread.new {sleep @reportinterval}
        @tsleep.join
      else
        # just sleep for a while (interruptable by stop)
        @tsleep = Thread.new {sleep 60}
        @tsleep.join
      end
    end

    def report_status
      msg = @controller.show_status
      File.binwrite("#{@reportfile}_status_#{Time.now.utc.strftime('%F_%T').gsub(':', '-')}.txt"), msg) if @reportfile
      count_result(sendmail(msg)) if @recipients
      return msg
    end

    def sendmail(msg, params={})
      ($log.info "#{name}.sendmail: no recipients configured"; return) unless @recipients
      subject = params[:subject] || "LotExerciser status report, #{Time.now.strftime('%F_%T')}"
      begin
        @smtp = Net::SMTP.start(@smtpserver)
        $log.info "sending report to #{@recipients.inspect}"
        @response = @smtp.send_message("Subject: #{subject}\r\n\r\n#{msg}\r\n", @sender, @recipients.tr(',;', ' ').split)
        @smtp.finish
        @response.success?
      rescue
        false
      end
    end

    def stop
      @tsleep.kill if @tsleep && @tsleep.alive?
      super
    end

  end

end
