=begin
(c) Copyright 2012 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: Ulrich Koerner (Saxonia), Steffen Steidten, 2012-08-06

History:
  2015-10-09 sfrieske, cleanup
  2021-02-05 sfrieske, use instance_of instead of kind_of
  2021-08-26 sfrieske, removed dependency on misc.rb (duration_in_seconds)
=end


module LotExerciser
  class OpNoAgent < Agent
    attr_accessor :counter, :opNo, :target, :lotagent, :hold, :release, :bankin, :bankout,
      :holdreason, :holdreason2, :holdseparation, :holdtime, :nonprobank, :bankholdtime

    def initialize(env, opNo, target, params={})
      super(env, {name: "#{target ? 'locate' : 'gatepass'}_#{opNo}"}.merge(params))
      @opNo = opNo
      @target = target
      @lotagent = params[:lotagent]
      _h = params[:holdreason]
      _h = [_h] if _h.instance_of?(String)
      @holdreason, @holdreason2 = _h
      @holdseparation = params[:holdseparation] || 30
      @holdtime = params[:holdtime] || 30
      @hold = [0, 0]
      @release = [0, 0]
      @nonprobank = params[:nonprobank]
      @bankholdtime = params[:bankholdtime] || @holdtime
      @bankin = [0, 0]
      @bankout = [0, 0]
      $log.info "created OpNoAgent #{@name}: #{@opNo}->#{@target}"
    end

    def start(params={})
      if params[:reset_counters] != false
        @hold = [0, 0]
        @release = [0, 0]
        @bankin = [0, 0]
        @bankout = [0, 0]
      end
      super
    end

    def execute
      # lot state changes before locate/gatepass, tracked in @lotagent.lots
      select_lots.each {|li|
        lot = li.lot
        status = @lotagent.lots[lot]
        if status == nil
          # set first lot hold, nonprobankin
          hold_set(lot, @holdreason, @holdreason2 ? 'HOLD1' : 'HOLD') if @holdreason
          count_result(@sv.lot_nonprobankin(lot, @nonprobank), counter: @bankin) if @nonprobank
        elsif status == 'HOLD'
          # release of the only hold
          lh = @sv.lot_hold_list(lot, reason: @holdreason).first
          hold_release(lot, @holdreason, nil) if lh && (@sv.siview_time(lh.timestamp) + @holdtime < Time.now)
        elsif status == 'HOLD1'
          # set second hold
          lh = @sv.lot_hold_list(lot, reason: @holdreason).first
          hold_set(lot, @holdreason2, 'HOLD2') if (@sv.siview_time(lh.timestamp) + @holdseparation < Time.now)
        elsif status == 'HOLD2'
          # release the first of 2 holds
          lh = @sv.lot_hold_list(lot, reason: @holdreason).first
          releasetime = (Time.now + @holdseparation).strftime('%F_%T').gsub(':', '-')
          hold_release(lot, @holdreason, "RELEASE1 #{releasetime}") if lh && (@sv.siview_time(lh.timestamp) + @holdtime < Time.now)
        elsif status.start_with?('RELEASE1')
          # release the remaining 1st hold
          lh = @sv.lot_hold_list(lot, reason: @holdreason2).first
          hold_release(lot, @holdreason2, nil) if (Time.parse(status.split.last) < Time.now)
        end
        # nonprobankout
        lh = @sv.lot_hold_list(lot, hold_type: 'BankHold').first
        count_result(@sv.lot_nonprobankout(lot), counter: @bankout) if lh && (@sv.siview_time(lh.timestamp) + @bankholdtime < Time.now)
      }
      #
      # locate or gate pass waiting lots
      lis = select_lots(status: 'Waiting')
      $log.info "#{@target ? 'locating' : 'gatepassing'} #{lis.size} lots" unless lis.empty?
      lis.each {|li|
        # handle sporadic gatepass errors (pp?)
        if @target
          res = @sv.lot_opelocate(li.lot, @target)
        else
          res = nil
          retries = 2
          loop {
            res = @sv.lot_gatepass(li.lot)
            break if res == 0 || res != 1254
            retries -= 1
            break if retries < 0
            sleep 1
          }
        end
        # res = @target ? @sv.lot_opelocate(li.lot, @target) : @sv.lot_gatepass(li.lot)  # , mandatory: true)
        count_result(res)
      }
    end

    def hold_set(lot, reason, status)
      res = @sv.lot_hold(lot, reason)
      @lotagent.lots[lot] = status if count_result(res, counter: @hold)
    end

    def hold_release(lot, reason, status)
      res = @sv.lot_hold_release(lot, reason)
      @lotagent.lots[lot] = status if count_result(res, counter: @release)
    end

    def status(params={})
      if params[:condensed] != false
        data = {}
        data[@target ? :locate : :gatepass] = @counter if @counter != [0, 0]
        data[:hold] = @hold if @hold != [0, 0]
        data[:release] = @release if @release != [0, 0]
        data[:bankin] = @bankin if @bankin != [0, 0]
        data[:bankout] = @bankout if @bankout != [0, 0]
        return data == {} ? {} : {@name=>data}
      else
        {@name=>{(@target ? :locate : :gatepass)=>@counter, hold: @hold, release: @release, bankin: @bankin, bankout: @bankout}}
      end
    end
  end


  class ShipAgent < Agent
    attr_accessor :counter, :opNo, :mutex, :bank_end, :bank_ship, :lotagent, :replace_shipped, :track_shipped, :shiplog

    def initialize(env, opNo, params={})
      super(env, {name: params[:name] || 'shipagent'}.merge(params))
      @mutex = Mutex.new
      @opNo = opNo
      @bank_end = params[:bank_end]
      @bank_ship = params[:bank_ship]
      @lotagent = params[:lotagent]
      @replace_shipped = @lotagent && (params[:replace_shipped] != false)  # default: replace shipped lots if lotagent is set
      @track_shipped = params[:track_shipped]
      @shiplog = params[:shiplog]
      File.binwrite(@shiplog, "shipped lots, starting #{Time.now.strftime('%F_%T')}:\n") if @shiplog
      $log.info "created ShipAgent #{@name}: #{@opNo}"
    end

    def execute
      rlots = []
      select_lots(status: 'Waiting').each {|li|
        lot = li.lot
        @sv.lot_bankin(lot)
        @sv.lot_bank_move(lot, @bank_ship)
        res = @sv.lot_ship(lot, @bank_ship)
        if count_result(res)
          @sv.wafer_sort(lot, '')
          if @lotagent
            @mutex.synchronize {
              @lotagent.lots[lot] = 'SHIPPED'
              @lotagent.lots.delete(lot) unless @track_shipped
              open(@shiplog, 'ab') {|fh| fh.write("#{Time.now.strftime('%F_%T')}\t#{lot}\n")} if @shiplog
            }
            rlots << @lotagent.new_lot(sv: @sv, route: li.route, product: li.product) if @replace_shipped
          end
        end
      }
      # release jCAP HOLDs of replacement lots
      if @replace_shipped && @running
        sleep 40
        rlots.each {|lot| @sv.lot_hold_release(lot, 'X-M2')}
      end
    end

    def shiplist
      return @shiplog ? @mutex.synchronize {File.read(@shiplog)} : nil
    end

    def ship_cancel(lot, params={})
      li = @sv.lot_info(lot)
      if li.carrier == ''
        cID = @sv._get_empty_carrier(params[:carrier] || "#{@carrierprefix}%") || return
        @sv.wafer_sort(lot, cID)
      end
      @sv.lot_ship_cancel(lot)
      @sv.lot_bank_move(lot, @bank_end)
      @sv.lot_bankin_cancel(lot)
    end
  end
end
