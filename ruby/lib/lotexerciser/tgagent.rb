=begin
(c) Copyright 2012 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: Ulrich Koerner (Saxonia), Steffen Steidten, 2012-08-06

History:
  2015-10-09 sfrieske, cleanup
  2021-02-05 sfrieske, use instance_of instead of kind_of
  2021-08-26 sfrieske, removed dependency on misc.rb (duration_in_seconds)
=end

require 'thread'
require 'dummytools'


module LotExerciser

  class ToolGroupAgent < Agent
    attr_accessor :tools, :eis, :mutexes, :cmutex, :svs, :ports, :proctime, :_tsleeps

    def initialize(env, tools, params={})
      super(env, {name: "proc_#{tools[0]}"}.merge(params))
      @proctime = params[:proctime] || 2
      @tools = tools
      @tools = [@tools] if @tools.instance_of?(String)
      @eis = Dummy::EIs.new(env, params)
      @mutexes = Hash[@tools.collect {|t| [t, Mutex.new]}]
      @cmutex = Mutex.new  # for counter update
      @ports = []
      @tools.each {|t|
        $log.info "EQP: #{t}"
        @sv.eqp_info(t).ports.each {|p|
          @ports << [t, p.port, 0, 0]
        }
      }
      @svs = @ports.collect {SiView::MM.new(env)}
      @_tsleeps = []
      $log.info "created ToolGroupAgent #{@name}: #{@tools.inspect}"
    end

    def start(params={})
      @ports.each {|p| p[-2] = p[-1] = 0} if params[:reset_counters] != false
      super
    end

    def execute
      @_tsleeps = []
      threads = []
      lots_carriers = @sv.what_next(@tools.first).collect {|e|
        next unless e.lot.start_with?(@lotprefix) && e.carrier.start_with?(@carrierprefix)
        next unless e.route.start_with?(@routeprefix) && e.product.start_with?(@productprefix)
        next if e.xfer_status == 'EI'
        [e.lot, e.carrier]
      }.compact.shuffle
      lots_carriers.each_with_index {|lot_carrier, i|
        break if i >= @ports.size
        sleep 1
        lot, carrier = lot_carrier
        threads << Thread.new {
          eqp, port, = @ports[i]
          Thread.current['name'] = "_#{eqp}_#{port}"
          # add a random component to the proctime
          sleeptime = @proctime * 0.95 + rand * @proctime / 10
          # process lot, allow to interrupt during long "proctime" sleeps
          s = sleeptime <= 10 ? sleeptime : proc {t = Thread.new {sleep sleeptime}; @_tsleeps << t; t.join}
          res = @svs[i].claim_process_lot(lot, mode: 'Auto-1',
            eqp: eqp, port: port, synchronize: @mutexes[eqp], proctime: s)
          # update counters
          count_result(res, counter: @ports[i])
          @cmutex.synchronize {count_result(res)}
        }
      }
      threads.each {|t| t.join}
    end

    def stop
      @_tsleeps.each {|t| t.kill}
      super
    end

    def status(params={})
      ret = {}
      @ports.each {|eqp, port, c1, c2|
        ret["proc_#{eqp}:#{port}"] = [c1, c2] unless (@counter == [0, 0]) && (params[:condensed] != false)
      }
      ret
    end

    def cleanup_eqp(params={})
      $log.info "cleanup_eqp"
      ths = @tools.collect {|eqp|
        Thread.new {
          @sv.eqp_cleanup(eqp, status_change: false)
          sleep 1
          @eis.restart_tool(eqp)
          sleep 3
          @sv.eqp_cleanup(eqp, mode: 'Auto-1')
        }
      }
      ths.each {|t| t.join}
    end
  end
end
