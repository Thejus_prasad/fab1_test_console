=begin
(c) Copyright 2012 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: Ulrich Koerner (Saxonia), Steffen Steidten, 2012-08-06

History:
  2015-10-09 sfrieske, cleanup
  2018-07-13 sfrieske, removed DRB RemoteController
  2021-02-05 sfrieske, use instance_of instead of kind_of
=end

load './ruby/set_paths.rb'
require 'net/smtp'
require 'pp'
require 'siview'

Thread.abort_on_exception = true   # set to true for debugging only!


module LotExerciser

  class Agent
    attr_accessor :name, :sv, :counter, :running, :done, :lotprefix, :carrierprefix, :routeprefix, :productprefix

    def initialize(env, params={})
      @name = params[:name] || 'agent'
      @sv = SiView::MM.new(env)
      @counter = [0, 0]
      @done = true
      @carrierprefix = params[:carrierprefix]
      @lotprefix = params[:lotprefix]
      @routeprefix = params[:routeprefix]
      @productprefix = params[:productprefix]
    end

    def inspect
      "#<#{self.class.name} #{@name.inspect}, env: #{@sv.env}>"
    end

    def start(params={})
      Thread.new {
        Thread.current['name'] = @name
        $log.info "starting #{@name.inspect} .."
        @counter = [0, 0] if params[:reset_counters] != false
        @done = false
        @running = true
        while @running
          execute
          sleep 1
        end
        @done = true
        $log.info "stopped #{@name.inspect}"
      }
    end

    def execute
      $log.warn "#{self.class.name}#execute is not implemented!"
    end

    def select_lots(params={})
      lis = @sv.lot_list(lot: "#{@lotprefix}%", product: "#{@productprefix}%",
        route: "#{@routeprefix}%", opNo: @opNo, status: params[:status], lotinfo: true)
      return lis if params[:filter] == false
      return lis.select {|li| !li.pp_carrier && !li.pp_lot && li.xfer_status != 'EI'}.shuffle
    end

    def count_result(res, params={})
      c = params[:counter] || @counter
      if (res == 0) || (res == true) || res.instance_of?(String)
        c[-2] += 1
        return true
      else
        c[-1] += 1
        return false
      end
    end

    def stop
      return if @done and !@running
      $log.info "stopping #{@name.inspect} .."
      @running = false
      sleep 1 unless @done
      true
    end

    def status(params={})
      (@counter == [0, 0]) && (params[:condensed] != false) ? {} : {@name=>@counter}
    end

  end


  # pre-defined test scenarios

  def self.pdwh(env, params={})
    ctrlparams = {
      carrierprefix: 'PDWH',
      lotprefix: 'PDWH',
      routeprefix: 'P-PDWHLOAD',
      productprefix: 'PDWHLOAD',
      lot_defaults: {product: 'PDWHLOAD.A1', route: 'P-PDWHLOADA1.01', customer: 'gf', sublottype: 'PO'},
      tools: [['PDWHP10','PDWHP11'], ['PDWHP20','PDWHP21'], ['PDWHP30','PDWHP31'], ['PDWHM40','PDWHM41']],
      proctime: params[:proctime] || ['1m', '20m', '1h', '2h'],
      opNos: {
        first: :gatepass,
        '6400.1000'=>{target: '6400.2000', holdreason: 'ENG', holdtime: '30m', nonprobank: 'UT-HOLD'},
        '6400.2000'=>{target: :last, holdreason: ['DQE', 'INT'], holdtime: '30m', holdseparation: '5m'},
        last: {target: params[:ship] != false ? :ship : :first, bank_ship: 'OY-SHIP'},
      },
      track_shipped: false,
      shiplog: "log/lotexerciser_pdwh_#{env}_shiplist_#{Time.now.utc.strftime('%F_%T').gsub(':', '-')}.txt",
    }.merge(params)
    ##ctrlparams[:mailto] ||= "ie_reporting.drs@globalfoundries.com,heike.urban,rene.hoehne" if env == "itdc"
    #
    # opNo_ignore = params[:opNo_ignore] || /^6400.[3456789].|^65/
    #
    return Controller.new(env, ctrlparams)
  end

  def self.load1(env, params={})
    ctrlparams = {
      carrierprefix: 'PDWH',
      lotprefix: 'PDWH',
      routeprefix: 'P-PDWHLOAD',
      productprefix: 'PDWHLOAD',
      lot_defaults: {product: 'PDWHLOAD.A1', route: 'P-PDWHLOADA1.01', customer: 'gf', sublottype: 'PO'},
      tools: [
        ['PDWHP10', 'PDWHP11', 'PDWHP12', 'PDWHP13'],
        ['PDWHP20', 'PDWHP21', 'PDWHP22', 'PDWHP23'],
        ['PDWHP30', 'PDWHP31', 'PDWHP32', 'PDWHP33'],
        ['PDWHM40', 'PDWHM41', 'PDWHM42', 'PDWHM43'],
        ['PDWHP50', 'PDWHP51', 'PDWHP52', 'PDWHP53'],
        ['PDWHP60', 'PDWHP61', 'PDWHP62', 'PDWHP63'],
        ['PDWHP70', 'PDWHP71', 'PDWHP72', 'PDWHP73']
      ],
      proctime: params[:proctime] || 1,
      opNos: {first: :gatepass, '6400.1000'=>{target: :first}}  #, holdreason: 'ENG', holdtime: 1}},
    }.merge(params)
    #
    return Controller.new(env, ctrlparams)
  end

end


require_relative 'controller'
require_relative 'lotagent'
require_relative 'opnoagent'
require_relative 'reporter'
require_relative 'tgagent'


# command line interface
def main(argv)
  require 'optparse'
  Thread.current['name'] = 'main'
  # set defaults
  env = nil
  testset = 'load1'
  duration = '1h'
  distribute_lots = true
  ship = nil
  mailto = nil
  reportinterval = nil  # '24h'
  verbose = false
  noconsole = true
  async = false
  dryrun = false
  #
  # parse cmdline options
  optparser = OptionParser.new {|o|
    o.banner = "Usage: #{File.basename(__FILE__)} [options]"
    o.on('-e', '--env ENV', "environment, default is #{env}") {|e| env = e}
    o.on('-t', '--testset TEST', "testset, default is #{testset}") {|e| testset = e}
    o.on('-d', '--duration DURATION', "test duration, e.g. '1h' (-1: forever), default is #{duration}") {|d| duration = d}
    o.on('-l', '--nolotdistrib', 'do not distribute lots') {distribute_lots = false}
    o.on('-s', '--noship', 'do not ship lots') {ship = false}
    o.on('-m', '--mailto RECIPIENTS', "report recipients, e.g. 'a.b,c.d'") {|e| mailto = e}
    o.on('-r', '--reportinterval INTERVAL', "report interval, e.g. '24h', default is #{reportinterval}") {|e| reportinterval = e}
    o.on('-c', '--console', 'enable console logging') {noconsole = false}
    o.on('-a', '--async', 'enable async controller operation') {async = true}
    o.on('-v', '--verbose', 'verbose logging') {verbose = true}
    o.on('-y', '--dryrun', 'dry run, no test execution') {dryrun = true}
    o.on_tail('-h', '--help', 'diplay this screen') {puts o; return}
  }
  optparser.parse(*argv)
  ($stderr.puts "environment must be provided (e.g. -e let -t pdwh)"; return) unless env && testset
  # create logger
  logfile = "log/lotexerciser_#{testset}_#{env}_#{Time.now.utc.strftime('%F_%T').gsub(':', '-')}.log"
  $stderr.puts "logging to #{logfile}"
  create_logger(file: logfile, file_verbose: verbose, verbose: verbose, noconsole: noconsole)
  # set the parameters
  params = {
    dryrun: dryrun, async: async, duration: duration,
    ship: ship,
    reportfile: "log/lotexerciser_#{testset}_#{env}", mailto: mailto, reportinterval: reportinterval
  }
  #
  # start load generator
  $log.info "\n\n\LotExerciser.#{env}, #{params.inspect}"
  controller = LotExerciser.send(testset, env, params)  # invoking e.g. LotExerciser.load1("let", params)
  startparams = {
    dryrun: dryrun, async: async, duration: duration,
    opNo_ignore: /^6|^9/,
    distribute_lots: distribute_lots,
    # reset_counters: false,
  }
  controller.start(startparams)
  return controller
end


main(ARGV) if __FILE__ == $0
