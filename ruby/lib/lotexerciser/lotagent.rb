=begin
(c) Copyright 2012 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: Ulrich Koerner (Saxonia), Steffen Steidten, 2012-08-06

History:
  2015-10-09 sfrieske,  cleanup
=end


module LotExerciser
  class LotAgent < Agent
    attr_accessor :lots, :plan, :lot_defaults

    def initialize(env, params={})
      super(env, {name: 'lotagent'}.merge(params))
      @lots = {}
      @lot_defaults = params[:lot_defaults] || {}
      @max_carriers = 100000
      @max_list_count = 1000
      $log.info "created LotAgent #{@name}"
    end

    # find lots that are not scrapped or shipped
    def find_lots
      $log.info "find_lots"
      select_lots(filter: false).each {|li|
        @lots[li.lot] = nil unless ['SHIPPED', 'SCRAPPED'].member?(li.status)
      }
      $log.info "  found #{@lots.count} lots"
      return @lots.keys
    end

    def cleanup_lots
      $log.info 'cleanup_lots'
      @lots.keys.each {|lot| @sv.lot_cleanup(lot, op: :current)}
    end

    def distribute_lots(params={})
      if params[:first]
        @lots.keys.each {|lot| @sv.lot_opelocate(lot, :first)}
      else
        route = params[:route] || @lot_defaults[:route] || @sv.lot_info(@lots.keys.first).route
        rops = @sv.route_operations(route).find_all {|e| e.operationNumber !~ params[:opNo_ignore]}
        @lots.keys.each_with_index {|lot, i|
          @sv.lot_opelocate(lot, rops[i % rops.count].operationNumber)
        }
      end
    end

    # get the lot distribution along the route
    def lot_distribution(params={})
      sv = params[:sv] || @sv
      route = params[:route] || @lot_defaults[:route]
      ret = {}
      sv.route_operations(route).each {|o|
        opNo = o.operationNumber
        lots = sv.lot_list(lot: "#{@lotprefix}%", product: "#{@productprefix}%", opNo: opNo, lotinfo: true).collect {|li|
          next if ['SHIPPED', 'SCRAPPED'].member?(li.status)
          li.lot
        }.compact
        ret[opNo] = params[:lots] ? lots : lots.count
      }
      ret
    end

    # get the number of lots per route/product combination
    def lot_route_product(params={})
      ret = {}
      @lots.keys.each {|lot|
        li = @sv.lot_info(lot)
        next unless li
        ret[li.product] = {} unless ret[li.product]
        ret[li.product][li.route] = [] unless ret[li.product][li.route]
        ret[li.product][li.route] << lot
      }
      return ret if params[:condensed] == false or params[:c] == false
      ret.each_pair {|p, r_lots| r_lots.each_pair {|r, lots| ret[p][r] = lots.count}}
      return ret
    end

    # # create one new lot from params and defaults, return lot id or nil
    # def new_lot(params={})
    #   sv = params[:sv] || @sv
    #   lot = params.has_key?(:lot) ? params[:lot] : "#{@lotprefix}#{unique_string}.000"
    #   carrier = params.has_key?(:carrier) ? params[:carrier] : "#{@carrierprefix}%"
    #   lotparams = @lot_defaults.merge(params).merge(lot: lot, carrier: carrier)  #, stb: true)
    #   lotparams.delete(:route) if params.has_key?(:product) && !params.has_key?(:route)  # allow products with other routes
    #   lotparams[:comment] = "ERPITEM='ENGINEERING-U00',DPML=1.5,PLANNED_FSD=#{Time.now.strftime('%F')}"
    #   if params[:dryrun]
    #     $log.info "dry run, not creating lot #{lotparams.inspect}"
    #   else
    #     ec = sv.carrier_list("#{@carrierprefix}%", empty: true).first || return

    #     lot = sv.new_lot_release(lotparams) || return
    #     sv.stb(lot, lotparams) || return
    #     @lots[lot] = nil
    #   end
    #   return lot
    # end

    # # params can contain keys to create lots with non-std parameters, e.g customer: {"cust1"=>2, "cust2"=>3}
    # #   to create qty-5 lots with default customer, 2 with cust1 and 3 with cust2
    # def new_lots(qty, params={})
    #   $log.info "new_lots #{qty.inspect}"
    #   # plan for lots with exception parameters
    #   @plan = {}
    #   nplan = 0
    #   [:customer, :sublottype, :product, :route, :priority].each {|k|
    #     v = params.delete(k)
    #     if v
    #       @plan[k] = v
    #       nplan += v.values.sum
    #     end
    #   }
    #   qty ||= nplan   # pass nil to create explicitly planned lots only
    #   ($log.warn "cannot satisfy requested #{nplan} variations whith #{qty} lots"; return) if nplan > qty
    #   # create exception lots according to the plan, with exactly one parameter other than default
    #   @plan.each_pair {|k, value_counts|
    #     value_counts.each_pair {|v, c| c.times {break unless new_lot(params.merge(k=>v))} }
    #   }
    #   #
    #   # create lots with default parameters
    #   (qty - nplan).times {|i| break unless new_lot(params)}
    #   $log.info "created #{@lots.size} lots, #{@lots.keys.first} .. #{@lots.keys.last}"
    #   return @lots.keys
    # end

    # def new_lots_pdwh(params={})
    #   startplan = {
    #     product: {'PDWHLOAD.A2'=>2},
    #     sublottype: {'PF'=>2, 'ET'=>2, 'EP'=>2},
    #     customer: {'ibm'=>2},
    #     priority: {3=>2},
    #     dryrun: (params[:dryrun] != false)
    #   }
    #   new_lots(nil, startplan.merge(params))
    #   #$ex.lotagent.new_lots(n, :dryrun=>true, :customer=>{"aaa"=>3, "bbb"=>2}, :sublottype=>{"XX"=>3})
    # end

    # delete the 1000 oldest lots not younger than (days), params are passed to delete_lot_family
    def delete_lots_older_than(days=1, params={})
      t = Time.parse('0:00').to_i - (days * 86400)
      $log.info "deleting lots created before #{Time.at(t)}"
      last = @lotprefix + (t-1).to_s(36).upcase + '.000'
      ll = @sv.lot_list(lot: "#{@lotprefix}%")
      return ll.inject(true) {|ok, lot| @sv.delete_lot_family(lot, params) && ok}
    end

    # carrier_list is faster if no carriers found (100 times in less than 1.5s); gaps are ignored
    def last_carrierid
      res = i = 0
      (@max_carriers/@max_list_count-1).downto(0) {|i|
        res = @sv.carrier_list(carrier: "#{@carrierprefix}%.2d%" % i).count
        break unless res.zero?
      }
      return i * @max_list_count + res
    end

    # # create cnt carriers
    # def create_carriers(cnt, params={})
    #   $log.info "create_carriers #{cnt.inspect}"
    #   ($log.warn " wrong parameter"; return []) unless cnt.instance_of?(Fixnum) and cnt > 0
    #   # generate carrier ids
    #   first_id = last_carrierid + 1
    #   carriers = cnt.times.collect {|i| "#{@carrierprefix}%.6d" % (first_id + i)}
    #   # register the carriers
    #   @sv.durable_register('Cassette', carriers, params[:category] || 'FEOL')
    #   @sv.carrier_status_change(carriers, 'AVAILABLE')
    # end

  end

end
