=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: Steffen Steidten, 2010-12-17

History:
  2011-02-09 ssteidte, added filtering by event parameter key
  2011-12-08 ssteidte, correct parsing of time string
  2012-01-24 ssteidte, use Sequel instead of DBI for database access
  2012-02-15 ssteidte, minor changes due to the switch to Oracle database
  2012-07-25 dsteger,  new interface to derdack
  2012-08-23 ssteidte, remove to_char conversion from queries for Date and Time fields
  2014-10-15 ssteidte, cleaner separation of Fab8 get_events
  2016-01-01 sfrieske. minor cleanup
  2018-02-15 sfrieske, replaced select by select
  2019-01-14 sfrieske, use MS-SQL server for Fab1; new class DerdackEvents obsoletes Derdack::EventDB
  2019-02-21 sfrieske, added wait_notification
  2021-02-05 sfrieske, use instance_of instead of kind_of
  2021-08-25 sfrieske, use rexml/document directly
  2021-12-07 sfrieske, fixed ORDER BY

Notes:
  Useful DerdackEvents#query parameters:
    jCAP on JBOSS 7: service: '%jCAP_AsyncNotification%', # username: 'aa_SOAP_JCAP'
    jCAP on JBOSS 5  service: '%GFSoapTemplate%', username: ''
=end

require 'rexml/document'
require 'dbsequel'


class DerdackEvents
  attr_accessor :db

  def initialize(env)
    @db = ::DB.new("derdack.#{env}")
  end

  def inspect
    return "<#{self.class.name} #{@db.inspect}>"
  end

  # call without query specifications is strongly discoureged
  def query(params)
    $log.debug {"DerdackEvents#query #{params}"}
    tstart = params[:tstart] || Time.now - 7200  # default: 2 h back
    tend = params[:tend] || tstart + 7200        # default: 2 h after tstart
    qargs = []
    q = 'SELECT EventID, Timestamp, ServiceFrom, MsgData FROM EAEvents'
    q, qargs = @db._q_restriction(q, qargs, 'Timestamp >=', tstart)
    q, qargs = @db._q_restriction(q, qargs, 'Timestamp <=', tend)
    q, qargs = @db._q_restriction(q, qargs, 'EventID', params[:eid])
    q, qargs = @db._q_restriction(q, qargs, 'Username', params[:username])
    q, qargs = @db._q_restriction(q, qargs, 'ServiceFrom', params[:service])
    q += ' ORDER BY EventID'
    res = @db.select(q, *qargs) || return
    ret = []
    filter = params[:filter]
    res.each {|r|
      rdata = {eid: r[0], timestamp: r[1], service: r[2]}
      doc = REXML::Document.new(r[3])
      doc.elements.each('mm_message/mm_event/param') {|e|
        v = e.elements['value'].text
        rdata[e['name']] = v unless v.nil?
      }
      fok = true
      unless filter.nil?
        filter.each_pair {|k, v|
          if rdata[k] != v
            fok = false
            break
          end
        }
      end
      ret << rdata if fok
    }
    return ret
  end

  # wait for a specified notification, return the matching msg(s) on success or nil
  def wait_for(params={}, &blk)
    $log.info "DerdackEvents#wait_for #{params}"
    sleeptime = params[:sleeptime] || 10
    tstart = params[:tstart] || Time.now
    tend = tstart + (params[:timeout] || 180)
    loop {
      msgs = query(tstart: tstart, service: params[:service], filter: params[:filter])
      ($log.info 'no response from Derdack DB'; return) unless msgs
      ret = blk ? blk.call(msgs) : msgs   # the evaluating block determines what is returned (find->1 msg, select->array)
      if ret == true || ret.instance_of?(Hash) || ret.instance_of?(Array) && !ret.empty?  # TODO: simplify
        return ret  # one or more messages found
      elsif Time.now >= tend
        return nil
      else
        sleep sleeptime
      end
    }
  end

end
