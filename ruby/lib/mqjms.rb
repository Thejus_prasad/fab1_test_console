=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: Steffen Steidten, 2011-03-25

Version:    1.1.2

History:
  2012-09-17 dsteger,  updated with MQ7 libraries
  2013-11-13 ssteidte, pass :use_jms=>false to connect without JMS header
  2015-01-16 ssteidte, add nonpersistent and expiration flags, temporary queues
  2015-02-25 ssteidte, allow to send raw messages
  2015-05-28 ssteidte, attempt to make send_msg and receive_msg (only!) thread safe
  2016-10-20 sfrieske, check messages for class JMSMessage (return nil)
  2017-02-20 sfrieske, improved read_msg
  2017-03-14 sfrieske, renamed _process_response to process_msg, removed unused delete_msg and send_msgs
  2017-07-21 sfrieske, renamed disconnect to close
  2017-11-17 sfrieske, added send_response for answers on the msg's reply_to queue
  2017-12-15 sfrieske, added reconnect for short living connections
  2018-02-12 sfrieske, allow to pass another MQ::JMS instance to new for cloning, remove java_import
  2018-05-17 sfrieske, send_response sets correlation ID
  2021-02-05 sfrieske, use instance_of instead of kind_of
  2021-03-18 sfrieske, use connection_name_list (for HA connections)
  2021-08-03 sfrieske, updated MQ client lib to v 9.x
  2021-08-26 sfrieske, use read_config instead of _config_from_file
  2021-12-02 sfrieske, simplified initialize
=end

require 'lib/mq/com.ibm.mq.allclient.jar'
require 'lib/mq/javax.jms-api.jar'
require 'util/readconfig'


# Connect to IBM WebSphere MQ
module MQ

  # Connect to IBM WebSphere MQ through javax.jms
  class JMS
    attr_accessor :instance, :params, :cf, :conn, :session, :queue, :rqueue, :msg, :txtime, :duration

    def initialize(instance, params={})
      @instance = instance
      @params = instance.nil? ? params : read_config(instance, 'etc/mq.conf', params)
      connect(@params)
      @queue = qopen(@params[:qname], use_jms: @params[:use_jms])
      # use @rqueue for replyTo only!
      if reply_queue = @params[:reply_queue]
        @rqueue = reply_queue.instance_of?(String) ? qopen(reply_queue, use_jms: @params[:use_jms]) : reply_queue
      end
    end

    def inspect
      "#<#{self.class.name} instance: #{@instance}, qmgr: #{@cf.queue_manager}, queue: #{@queue.name.strip if @queue}>"
    end

    def connect(params=@params)
      @cf = Java::ComIbmMqJms::MQQueueConnectionFactory.new
      @cf.transport_type = Java::ComIbmMsgClientWmq::WMQConstants::WMQ_CM_CLIENT
      if connections = params[:connections]
        @cf.connection_name_list = connections
      else
        @cf.host_name = params[:host]
        @cf.port = params[:port].to_i
      end
      @cf.queue_manager = params[:qmgr]
      @cf.channel = params[:channel]
      @cf.client_reconnect_options = Java::ComIbmMsgClientWmq::WMQConstants::WMQ_CLIENT_RECONNECT  # ANY (0)
      @cf.client_reconnect_timeout = 100000  # default is 1800s; set to something sufficiently large like 100000
      @conn = @cf.create_queue_connection(params[:mquser], params[:mqpassword]) # user and password can be nil
      @conn.start
      @session = @conn.create_queue_session(false, Java::ComIbmMsgClientWmq::WMQConstants::AUTO_ACKNOWLEDGE)
    end

    # may be required if queue connections time out (Catalyst)
    def reconnect
      @session.close if @session
      @conn.close if @conn
      @conn = @cf.create_queue_connection(@params[:mquser], @params[:mqpassword]) # user and password can be nil
      @conn.start
      @session = @conn.create_queue_session(false, Java::ComIbmMsgClientWmq::WMQConstants::AUTO_ACKNOWLEDGE)
    end

    def close
      @session.close if @session
      @conn.close if @conn
    end

    # return a queue object, pass-thru if qname already is a queue object   TODO: simplify
    def qopen(qname, params={})
      # open a queue
      return unless qname
      # ($log.info "using existing queue #{qname.name}"; return qname) unless qname.instance_of?(String)
      begin
        ret = (qname == 'temporary') ? @session.create_temporary_queue : @session.create_queue("queue://#{@cf.queue_manager}/#{qname}")
        ret.target_client = Java::ComIbmMsgClientWmq::WMQConstants::WMQ_TARGET_DEST_MQ if params[:use_jms] == false
        $log.info "  connected to queue #{ret.name.inspect}"
        return ret
      rescue
        $log.warn "error connecting to queue #{qname.inspect}:\n#{$!}"
        return
      end
    end

    # return number of messages on the queue
    def qdepth
      return unless @queue
      begin
        b = @session.create_browser(@queue)
        depth = b.enumeration.to_a.size
        b.close
        return depth
      rescue => e
        $log.info $!
        $log.debug e.backtrace.join("\n")
        return
      end
    end

    # return array of messages (optionally raw) or nil on error
    def read_msgs(params={})
      return unless @session && @queue
      nmax = params[:nmax]
      ret = []
      # create a queue browser and collect messages
      b = @session.create_browser(@queue)
      b.enumeration.each {|msg|
        @msg = msg
        ret << (params[:raw] ? msg : process_msg(msg))
        break if nmax && ret.size >= nmax
      }
      b.close
      return ret
    end

    # read one message, leaving it on the queue and return it
    def read_msg(params={})
      res = read_msgs(params.merge(nmax: 1))
      return res ? res.first : nil
    end

    # receive messages without correlation, removing them from the queue, return array of messages or nil on timeout
    def receive_msgs(params={})
      return unless @session && @queue
      nmax = params[:nmax]
      timeout = (params[:timeout] || 60) * 1000  # default timeout 60 seconds
      rcv = @session.create_receiver(@queue)  # makes no sense(?):, params[:correlation])
      ret = []
      while true
        msg = params[:nowait] || timeout.zero? ? rcv.receive_no_wait : (timeout ? rcv.receive(timeout) : rcv.receive)
        break unless msg
        @msg = msg
        ret << (params[:raw] ? msg : process_msg(msg))
        break if nmax && ret.size >= nmax
      end
      rcv.close
      return ret
    end

    # receive one message, return message or nil on error or timeout
    def receive_msg(params={})
      @duration = nil
      return unless @session and @queue
      tstart = Time.now
      timeout = (params[:timeout] || 60) * 1000  # default timeout 60 seconds
      corr = params[:correlation]
      if corr
        rcv = @session.create_receiver(@queue, "JMSCorrelationID='#{corr}'")
      else
        rcv = @session.create_receiver(@queue)
      end
      @msg = nil
      msg = params[:nowait] || timeout.zero? ? rcv.receive_no_wait : (timeout ? rcv.receive(timeout) : rcv.receive)
      if msg
        @msg = msg
        @duration = Time.now - tstart
        ret = params[:raw] ? msg : process_msg(msg)
      else
        ret = nil
      end
      rcv.close
      return ret
    end

    def process_msg(msg)  #, params={})
      # not used, keep for reference (?)
      # if params[:check_msgtype]
      #   fmt = msg.get_string_property(Java::ComIbmMsgClientWmq::WMQConstants::JMS_IBM_FORMAT)
      #   type = msg.get_int_property(Java::ComIbmMsgClientWmq::WMQConstants::JMS_IBM_MSGTYPE)  # string property ?
      #   $log.debug {"format: #{fmt.inspect}, type: #{type.inspect}"}
      #   if params[:correlation] && type != Java::ComIbmMqConstants::CMQC.MQMT_REPLY
      #     $log.warn "wrong message type #{type}, expected #{Java::ComIbmMqConstants::CMQC.MQMT_REPLY}"
      #   end
      # end
      # empty msgs may be represented as JMSMessage
      msg ||= @msg  # for dev only
      return nil if msg.class == Java::ComIbmJms::JMSMessage
      # text message, retrieve text
      return msg.text if msg.class == Java::ComIbmJms::JMSTextMessage  ##|| fmt == CMQC::MQFMT_STRING
      # bytes message, read bytes and convert them to String
      len = msg.get_body_length
      barr = Java::byte[len].new
      msg.read_bytes(barr, len)
      charset = msg.get_string_property(Java::ComIbmMsgClientWmq::WMQConstants::JMS_IBM_CHARACTER_SET)
      $log.debug {"msg charset: #{charset}"} if charset != 'UTF-8'
      return String.from_java_bytes(barr, charset)
    end

    # delete messages from a queue, return true if msgs have been deleted else false, nil on error
    def delete_msgs(n=1)
      return unless @queue
      begin
        count = 0
        # delete all messages one by one
        while true
          msg = receive_msg(raw: true, nowait: true)
          break unless msg
          count += 1
          break if n and count >= n
        end
        $log.info "#{@queue.name}: deleted #{count} messages"
        return (count > 0)
      rescue => e
        $log.info "error deleting msg\n#{$!}"
        $log.debug e.backtrace.join("\n")
        return
      end
    end

    # put a string or byte message on a queue, return array of message id and correlation id on success or nil
    def send_msg(s, params={})
      que = params[:queue] || @queue || return
      @txtime = nil
      begin
        @msg = nil
        if s.instance_of?(String)
          # create message
          if params[:bytes_msg]
            msg = @session.create_bytes_message
            msg.write_bytes(s.to_java_bytes)
            que.message_body_style = Java::ComIbmMsgClientWmq::WMQConstants::WMQ_MESSAGE_BODY_MQ
          else
            msg = @session.create_text_message(s)
          end
          rq = params[:reply_to] || @rqueue
          msg.jms_reply_to = rq if rq
          corrid = params[:correlation]
          msg.jms_correlation_id = corrid if corrid
        else
          # assuming s is a JMS message
          msg = s
        end
        @msg = msg  # for dev only!
        # send it
        dmode = params[:persistent] ? Java::ComIbmMsgClientWmq::WMQConstants::DELIVERY_PERSISTENT : Java::ComIbmMsgClientWmq::WMQConstants::DELIVERY_NOT_PERSISTENT
        prio = params[:priority] || 4
        ttl = (params[:ttl] || 3600) * 6000  # 6h
        @txtime = Time.now
        # create sender
        sender = @session.create_sender(que)
        sender.send(msg, dmode, prio, ttl)
        res = [msg.jms_message_id, msg.jms_correlation_id]
        sender.close
        return res
      rescue javax.jms.JMSException => e
        $log.info "error sending msg #{msg.inspect}"
        $log.info " #{e.get_localized_message}"
        $log.info " linked exception: #{e.get_linked_exception}"
        $log.debug e.backtrace.join("\n")
        return
      rescue => e
        $log.info "error sending msg #{msg.inspect}\n#{$!}"
        $log.info e.backtrace.join("\n")
        return
      end
    end

    # get reply queue from the msg and send the response
    def send_response(s, msg=@msg)
      msgprops = msg_properties(msg)
      queue = qopen(msgprops[:reply_to], use_jms: msgprops[:use_jms])
      return send_msg(s, queue: queue, correlation: msgprops[:msgid])
    end

    # extract msg delivery properties
    def msg_properties(msg=@msg)
      return nil unless msg
      ret = {
        msgid: msg.jms_message_id,
        timestamp: Time.at(msg.jms_timestamp / 1000.0),
        expiration: Time.at(msg.jms_expiration / 1000.0),
        delivery_mode: msg.jms_delivery_mode,
        type: msg.class.name.split('::').last,
        reply_to: nil, use_jms: nil
      }
      if r = msg.jms_reply_to
        ret[:reply_to] = r['XMSC_DESTINATION_NAME']
        ret[:use_jms] = r['targetClient'] == 0
      end
      return ret
    end

  end

end
