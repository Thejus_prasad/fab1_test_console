=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: Steffen Steidten, GLOBALFOUNDRIES INC., 2015

History:
  2017-01-19 sfrieske, removed import of Log4J
  2018-07-25 sfrieske, removed java_import 'org.omg.CORBA.ORB'
  2019-10-11 sfrieske, added type_kind for better Any object handling (in siview.rb)
  2021-12-08 sfrieske, replaced Profile Struct by a Hash
=end

# load CORBA libs
# Java builtin - not working with SiView structs
# ENV_JAVA['com.sun.CORBA.transport.ORBTCPReadTimeouts'] ||= '200:180000:300:10'  # JBOSS: 10:15000:300:20
# ##ENV_JAVA['com.sun.CORBA.codeset.charsets'] = '0x05010001'
# ##ENV_JAVA['com.sun.CORBA.codeset.wcharsets'] = '0x05010001'
# ENV_JAVA.remove('org.omg.CORBA.ORBClass')
# ENV_JAVA.remove('org.omg.CORBA.ORBSingletonClass')
# JacORB
Dir['lib/jacorb/*jar'].each {|f| require f}
ENV_JAVA['jacorb.home'] ||= '.'
ENV_JAVA['org.omg.CORBA.ORBClass'] = 'org.jacorb.orb.ORB'
ENV_JAVA['org.omg.CORBA.ORBSingletonClass'] = 'org.jacorb.orb.ORBSingleton'

ENV.delete('http_proxy')   # always get fresh IOR files
require 'open-uri'


# IOR string parser with optional file download
class IORParser
  java_import 'org.omg.CORBA.TCKind'
  TCKinds = Java::OrgOmgCORBA::TCKind.methods.select {|m| m.to_s.start_with?('tk_')}

  def self.type_kind(o)
    TCKinds.find {|m| o.type.kind === Java::OrgOmgCORBA::TCKind.send(m)}
  end


  attr_accessor :ior, :hexior, :big_endian, :pos, :type_id, :nprofiles, :profiles

  def initialize(s=nil)
    parse(s) if s
  end

  def inspect
    "#<#{self.class.name} #{@type_id.inspect}>"
  end

  def parse(s)
    # treat iorfile as load URL unless it starts with 'IOR:'
    @ior = s.start_with?('IOR:') ? s.chomp : open(s) {|fh| fh.read}.chomp
    # convert to array of chars, remove leading 'IOR:'
    @hexior = (4..@ior.size).step(2).collect {|i| @ior[i..i+1].hex.chr}
    @big_endian = (@hexior[0] == "\x00")  # 0 -> big endian, currently only supported endianness
    @pos = 4
    #
    @type_id = get_string
    @nprofiles = get_ulong
    @profiles = @nprofiles.times.collect {
      tag = get_ulong
      if tag == 0
        # TAG_INTERNET_IOP, single component profile
        plen = get_ulong  # not used
        @pos += 1
        {major: get_char, minor: get_char, host: get_string, port: get_ushort, object_key: get_string}
      elsif tag == 1
        # TAG_MULTIPLE_COMPONENTS (used by EIs, that also use type 102 in the 2nd profile)
        nil # not yet implemented
      else
        nil # not yet supported
      end
    }
  end

  def get_char
    n = @hexior[@pos].ord
    @pos += 1
    return n
  end

  def get_ushort
    align(2)
    n = @hexior[@pos, 2].join.unpack('n').first
    @pos += 2
    return n
  end

  def get_ulong
    align(4)
    n = @hexior[@pos, 4].join.unpack('N').first
    @pos += 4
    return n
  end

  def get_string
    # read size
    l = get_ulong
    # read string, removing trailing 0
    s = @hexior[@pos, l - 1].join
    @pos += l
    return s
  end

  def align(n)
    rest = @pos % n
    @pos += (n - rest) if rest != 0
  end

end
