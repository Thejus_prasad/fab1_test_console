=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.


Author: Steffen Steidten, 2014-03-04

Version: 1.1.2

History:
  2016-08-24 sfrieske, use lib rqrsp_http instead of rqrsp
  2018-02-15 sfrieske, replaced select_all by select
  2019-02-22 sfrieske, separated MDSWriteService
  2021-08-30 sfrieske, removed dependency on siviewmmdb; removed StockerDspCarrier
=end

require 'dbsequel'


# AutoProc databases
module Vamos

  # MDS VAP tables (used by AutoProc viewer, http://f36asd5:8580/autoproc-war/index.jsf)
  class VAPDB
    attr_accessor :db

    VapScenario = Struct.new(:time, :scenarioid, :scenario, :msg, :carrier, :srctype, :srceqp, :srcport,
      :tgttype, :tgteqp, :tgtport, :txrc, :txmsg)
    # StockerDspCarrier = Struct.new(:carrier, :p_target, :r_target, :port, :portstate, :xfer_status, :owner)

    def initialize(env, params={})
      @db = params[:db] || DB.new("mds.#{env}")
    end

    # MM AutoProc enablement, manually done
    def wtdgid(eqp)
      res = @db.select('SELECT WTDG_ID from FSDRWTDG_CONF WHERE EQP_ID = ?', eqp) || return
      return res.first.first
    end

    def scenariolog(params={})
      table = params[:history] ? 'T_VAP_SCENARIO_LOG' : 'V_RTD_VAP_SCENARIO_LOG'
      qargs = []
      q = "SELECT EVENT_TIME, SCENARIO_ID, SCENARIO, CATEGORY, CAST_ID, SOURCE_TYPE, SOURCE_LOC, SOURCE_PORT,
        TARGET_TYPE, TARGET_LOC, TARGET_PORT, TX_RET_CODE, TX_MESSAGE from #{table}"
      q += " WHERE SCENARIO != 'NoWIP' " unless params[:nowip]
      q, qargs = @db._q_restriction(q, qargs, 'EVENT_TIME >=', params[:tstart])
      q, qargs = @db._q_restriction(q, qargs, 'EVENT_TIME <=', params[:tend])
      q, qargs = @db._q_restriction(q, qargs, 'SCENARIO', params[:scenario])
      q, qargs = @db._q_restriction(q, qargs, 'SOURCE_TYPE', params[:srctype])
      q, qargs = @db._q_restriction(q, qargs, 'SOURCE_LOC', params[:srceqp])
      q, qargs = @db._q_restriction(q, qargs, 'TARGET_TYPE', params[:tgttype])
      q, qargs = @db._q_restriction(q, qargs, 'TARGET_LOC', params[:tgteqp])
      q, qargs = @db._q_restriction(q, qargs, "CAST_ID", "%#{params[:carrier]}%") if params[:carrier]
      q, qargs = @db._q_restriction(q, qargs, 'CATEGORY', params[:msg])
      q += ' ORDER BY EVENT_TIME'
      res = @db.select(q, *qargs) || return
      return res.collect {|r|
        r[1] = r[1].to_i
        VapScenario.new(*r)
      }
    end

    def vapstatus(params={})
      qargs = []
      q = 'SELECT EQP_ID, WTDG_ID, EQP_STATE_ID, EQP_STATE_AVAIL_FLAG, IS_INHIBIT, WIP_SIVIEW, IS_AUTO3_WIP,
        LAST_TIME_DISP_RTD, NEXT_TIME_DISP_RTD, PORT_ID, PORT_MODE, E87_ID, PORT_DISP_MODE, DISP_PULL_FLAG,
        DISP_PUSH_FLAG, AUTO3_PORT_FLAG from V_TOOL_VAP_STATUS'
      q, qargs = @db._q_restriction(q, qargs, 'EQP_ID', params[:eqp])
      q += ' ORDER BY EQP_ID, PORT_ID'
      res = @db.select(q, *qargs) || return
      return res
    end

    # return tools with Auto-3 pull trigger set
    def pull_a(params={})
      qargs = []
      q = 'select EQP_ID from V_PULL_A'
      q, qargs = @db._q_restriction(q, qargs, 'EQP_ID', params[:eqp])
      q += ' ORDER BY EQP_ID'
      res = @db.select(q, *qargs) || return
      return res.collect {|e| e.first}
    end

    # return tools with Auto-1 pull trigger set
    def pull_m(params={})
      qargs = []
      q = 'select EQP_ID from V_PULL_M'
      q, qargs = @db._q_restriction(q, qargs, 'EQP_ID', params[:eqp])
      q += ' ORDER BY EQP_ID'
      res = @db.select(q, *qargs) || return
      return res.collect {|e| e.first}
    end

    # # StockerDisplay carriers
    # def sd_carriers(params={})
    #   qargs = []
    #   q = 'SELECT CC.CAST_ID, P.EQP_ID AS EQP1, R.EQP_ID AS EQP2, P.PORT_ID, P.PORT_STATE, R.TRANS_STATE, C.OWNER_ID FROM
    #     FRCTRLJOB C, FRCTRLJOB_CAST CC, FRPORT P, FRCAST R WHERE
    #     C.D_THESYSTEMKEY = CC.D_THESYSTEMKEY AND CC.LOAD_PORT_OBJ = P.PORT_OBJ AND CC.CAST_ID = R.CAST_ID'
    #   q, qargs = @db._q_restriction(q, qargs, 'CC.CAST_ID', params[:carrier])
    #   q += ' ORDER BY CC.CAST_ID'
    #   qargs.unshift(q)
    #   res = @db.select(*qargs) || return
    #   return res.collect {|r| StockerDspCarrier.new(*r)}
    # end

  end

end
