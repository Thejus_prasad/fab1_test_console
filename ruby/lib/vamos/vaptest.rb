=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: Steffen Steidten, 2014-04-29

History:
  2014-12-17 ssteidte, inherit from SiView::Test, use eqp_cleanup and lot_cleanup
  2015-02-26 ssteidte, added Vap::REST for lock information
  2017-01-05 sfrieske, use new rtdemuremote
  2018-03-06 sfrieske, replaced user_parameter_set_verify by user_parameter_change
  2019-02-22 sfrieske, separated MDSWriteService, moved wtdg to vapdb
  2020-08-27 sfrieske, improved find_operation_eqp
  2021-02-05 sfrieske, use instance_of instead of kind_of
  2021-08-27 sfrieske, require waitfor instead of misc
=end

require 'dummytools'
require 'jcap/mdswritesvc'
require 'rtdserver/rtdemuext'
require 'rtdserver/rtdemuremote'
require 'util/waitfor'
require_relative 'vapdb'
require_relative 'vaprest'
require 'siviewtest'


module Vamos

  # VAMOS AutoProc testing
  class AutoProcTest
    attr_accessor :svtest, :sv, :vapdb, :vaprest, :mdswrite, :rtdremote, :operations

    def initialize(env, params={})
      @svtest = params[:svtest] || SiView::Test.new(env, sv: params[:sv], sm: params[:sm])
      @sv = params[:sv] || @svtest.sv
      @vapdb = Vamos::VAPDB.new(env)
      @vaprest = Vamos::REST.new(env)  # for locks
      @mdswrite = MdsWriteService.new(env)
      @rtdremote = RTD::EmuRemote.new(env)
      @operations = params[:operations] || {
        '1000.1000'=>'UTA001', '1000.2000'=>'UTA002', '1000.3000'=>'UTA003',
        '1000.4000'=>'UTA004', '1000.5000'=>'UTA005', '1000.6000'=>'UTA006', '1000.7000'=>'UTA007' # UTA007 probably not necessary
      }
    end

    # return [opNo, eqp] combination based on the tool's properties passed as params
    def find_operation_eqp(params={})
      $log.info "find_operation_eqp #{params}"
      @operations.each {|e|
        eqpi = @sv.eqp_info_raw(e[1])
        isib = eqpi.respond_to?(:equipmentInternalBufferInfo)
        next if !!params[:ib] != isib   # defaults to false (fixed buffer)
        brinfo = isib ? eqpi.equipmentBRInfoForInternalBuffer : eqpi.equipmentBRInfo
        if (v = params[:max_batchsize]) != nil
          next if v != brinfo.maxBatchSize
        end
        if (v = params[:e2e]) != nil
          next if v != brinfo.eqpToEqpTransferFlag
        end
        if (v = params[:takeoutin]) != nil
          next if v != brinfo.takeInOutTransferFlag
        end
        if (v = params[:mrtype]) != nil
          next if v != brinfo.multiRecipeCapability
        end
        next if isib && params[:procmon] && eqpi.equipmentInternalBufferInfo.find {|e|
          e.bufferCategory == 'Process Monitor Lot'
        }.strShelfInBuffer.empty?
        #
        return e
      }
      return nil
    end

    # ensure tool(s) are configured for AutoProc, return true on success
    def verify_wtdg(eqps)
      eqps ||= @operations.values
      eqps = [eqps] if eqps.instance_of?(String)
      $log.info "verify_wtdg #{eqps}"
      # ensure tools are configured for AutoProc
      ret = true
      eqps.each {|eqp|
        res = @vapdb.wtdgid(eqp)
        ($log.warn "wrong FSDRWTDG_CONF for #{eqp}: #{res.inspect} instead of 'VAP'"; ret=false) if res != 'VAP'
      }
      return ret
    end

    # disable Pull for all ports, wait for carriers unload and delete RTDEmu rules, return true on success
    def stop_test(eqps)
      $log.info "stop_test #{eqps.inspect}"
      eqps ||= @operations.values
      eqps = [eqps] if eqps.instance_of?(String)
      eqps.each {|eqp|
        @rtdremote.delete_emustation(eqp)
        @sv.user_parameter_change('Eqp', eqp, {'Pull'=>'', 'Push'=>''})
      }
      eqpstates = {}
      wait_for(timeout: 180) {
        eqps.each {|eqp|
          eqpi = @sv.eqp_info(eqp)
          eqpstates[eqp] = eqpi.loaded_carriers.empty? && (!eqpi.ib || eqpi.ib.loaded_carriers.empty?)
        }
        $log.debug {eqpstates.pretty_inspect}
        # eqpstates.select {|k, v| !v}.empty? # TODO: better?
        eqpstates.collect {|k, v| k unless v}.compact.empty?
      } || $log.warn("eqps #{eqpstates.collect {|k, v| k unless v}.compact} are not empty")
      return true
    end

    # prepare tools in SiView and restart JavaEIs, return true on success
    def prepare_eqp(eqps, params={})
      eqps ||= @operations.values
      eqps = [eqps] if eqps.instance_of?(String)
      $log.info "prepare_eqp #{eqps}"
      ret = true
      mode = params[:mode] || 'Auto-3'
      eqps.each {|eqp|
        ret &&= @svtest.cleanup_eqp(eqp, scriptparams: false, portstatus: params[:portstatus], mode: mode)
        ret &&= @svtest.eis.set_proctime(eqp, params[:proctime] || 45)  # long enough for carrier locks to expire
        # enable Push and Pull for all ports
        if mode.end_with?('-1')
          s = @sv.eqp_info(eqp).ports.collect {|p| p.port}.join(',')
          ret &&= @sv.user_parameter_change('Eqp', eqp, 'Pull', s).zero? if params[:pull] != false
          ret &&= @sv.user_parameter_change('Eqp', eqp, 'Push', s).zero? if params[:push] != false
        end
      }
      return ret
    end

    # set lot filter rule for tools
    def set_rtdemu_filter(lots, eqps, params={})
      lots = [lots] if lots.instance_of?(String)
      eqps = [eqps] if eqps.instance_of?(String)
      eqps ||= @operations.values
      eqps.uniq!
      $log.info "set_rtdemu_filter #{lots}, #{eqps}"
      wnf = RTD::WhatNextFilter.new(@sv, lots: lots, procmon: params[:procmon])
      eqps.each {|eqp| @rtdremote.add_emustation(eqp, method: wnf)}
      return wnf
    end

  end

end
