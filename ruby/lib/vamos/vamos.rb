=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: Steffen Steidten, 2011-07-29

History:
  2015-01-09 ssteidte, added support for the new browser GUI
  2016-08-29 sfrieske, use MQ::JMS directly for ToolDisplay as there is no reply
  2018-05-03 sfrieske, disabled CORBA displays (browser only)
  2021-02-05 sfrieske, use instance_of instead of kind_of
  2021-07-26 sfrieske, removed StockerDisplay, use rexml/document directly
  2021-08-26 sfrieske, use read_config instead of _config_from_file

Notes:
  # StockerDisplay: http://f36asd3:9180/stockdsp/stockerdsp.jsf
  VamosDisplay:   http://f36asd3:9080/vamos/vamosdsp.jsf
=end

require 'rexml/document'
# require 'misc'
require 'mqjms'
require 'util/readconfig'


# VAMOS Stocker Display and Tool Display Test Clients.
module Vamos

  # MQ/SOAP test client to initiate VAMOS tools display actions, normally sent by the EI
  class ToolDisplay
    attr_accessor :eqp, :port, :mq, :url, :_doc

    def initialize(env, eqp, port, params={})
      @eqp = eqp
      @port = port
      @url = params[:url] || read_config("vamosdsp#{'ib' if params[:ib]}.#{env}", 'etc/urls.conf')[:url]
      @mq = MQ::JMS.new("vamos.#{env}", use_jms: false)
    end

    def inspect
      "#<#{self.class.name} #{@mq.inspect}, #{@eqp}:#{@port}>"
    end

    # create a SOAP message normally sent by the EI, return true on success
    # msgtype is one of materialArrived, materialRemoved, waferStarted, waferCompleted
    def _ei_msg(msgtype, carrier, wafers, params={})
      $log.info "#{msgtype} #{carrier} (#{wafers.size} wafers) #{params}"
      @_doc = doc = REXML::Document.new('<?xml version="1.0" encoding="UTF-8"?>')
      tag = doc.add_element('soapenv:Envelope', {
        'xmlns:soapenv'=>'http://schemas.xmlsoap.org/soap/envelope/',
        'xmlns:soapenc'=>'http://schemas.xmlsoap.org/soap/encoding/',
        # 'xmlns:xsd'=>'http://www.w3.org/2001/XMLSchema',
        'xmlns:xsi'=>'http://www.w3.org/2001/XMLSchema-instance',
        'xmlns:ns'=>'http://soap.vamos.amd.com',
        'xmlns:vamos'=>'http://f36asd6:8088/vamos1/services/VamosSoapService',
        'xmlns:tns1'=>'com.amd.vamos.soap'
      })
      .add_element('soapenv:Body')
      .add_element("ns:#{msgtype}", {'soapenv:encodingStyle'=>'http://schemas.xmlsoap.org/soap/encoding/'})
      .add_element('portData')
      #
      ctag = tag.add_element('carrier')
      ctag.add_element('carrierID').text = carrier
      wtag = ctag.add_element('wafers', {'soapenc:arrayType'=>"tns1:Wafer[#{wafers.size}]", 'xsi:type'=>'soapenc:Array'})
      wafers.each {|w|
        itag = wtag.add_element('item')
        itag.add_element('lotID').text = w.lot
        rcptag = itag.add_element('recipes', {'soapenc:arrayType'=>'tns1:Recipe[1]', 'xsi:type'=>'soapenc:Array'})
        rcptag.add_element('item').add_element('recipeID').text = 'QATestRecipe'
        itag.add_element('selected').text = 'true'
        itag.add_element('waferID').text = w.wafer
        itag.add_element('waferSlot').text = w.slot
        itag.add_element('waferState').text = params[:waferstate] || 'NEEDS PROCESSING'
      }
      #
      tag.add_element('entityID').text = @eqp
      tag.add_element('portID').text = @port
      tag.add_element('siviewJobID').text = params[:cj] || 'qa-test-job'
      #
      s = String.new
      tag.document.write(s)
      res = @mq.send_msg(s, params.merge(bytes_msg: true))
      return !res.nil?
    end

    def material_arrived(carrier, wafers, params={})
      _ei_msg(:materialArrived, carrier, wafers, {waferstate: 'NEEDS PROCESSING'}.merge(params))
    end

    def material_removed(carrier, wafers, params={})
      _ei_msg(:materialRemoved, carrier, wafers, {waferstate: 'PROCESSED'}.merge(params))
    end

    def wafer_started(carrier, wafers, params={})
      _ei_msg(:waferStarted, carrier, wafers, {waferstate: 'IN PROCESS'}.merge(params))
    end

    def wafer_completed(carrier, wafers, params={})
      _ei_msg(:waferCompleted, carrier, wafers, {waferstate: 'PROCESSED'}.merge(params))
    end

    def start_browser(params={})
      cmd = "start #{@url}"
      $log.info "starting vamos display in a browser: #{cmd.inspect}"
      system(cmd)
    end

  end

end
