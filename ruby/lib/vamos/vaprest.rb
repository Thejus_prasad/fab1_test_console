=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.


Author: Steffen Steidten, 2015-02-25

Version: 1.1.0

History:
  2016-08-24 sfrieske,  use lib rqrsp_http instead of rqrsp
=end

#require 'json/pure'
require 'rqrsp_http'


module Vamos

  # VAP REST Interface
  class REST < RequestResponse::HttpJSON
    attr_accessor :env

    def initialize(env, params={})
      @env = env
      super("vaprest.#{@env}", params)
    end

    def locks(params={})
      res = get(resource: 'locks') || return
      if lockid = params[:id]
        return res.select {|e| e['lockId'] == lockid}
      elsif eqp = params[:eqp]
        return res.select {|e| e['lockId'].start_with?("Tool.#{eqp}")}
      elsif carrier = params[:carrier]
        return res.select {|e| e['lockId'].start_with?("Foup.#{carrier}")}
      else
        return res
      end
    end

    def rtdresources(params={})
      get(resource: 'rtd-resources')
    end

    def scenarios(params={})
      if params[:history]
        tstart = (params[:tstart] || Time.now - 7200).utc.strftime('%a, %d %b %Y %H:%M:%S GMT') # 2h back
        tend = (params[:tend] || Time.now).utc.strftime('%a, %d %b %Y %H:%M:%S GMT')
        count = (params[:count] || 1000).to_s
        data = {dateFrom: tstart, dateTo: tend, maxResults: count}
        res = post(data, resource: 'scenario-history') || return
      else
        res = get(resource: 'scenarios') || return
      end
      res.select! {|e| e['engine'] != 'NoWIP'} unless params[:nowip]
      if eqp = params[:eqp]
        return res.select {|e| (e['sourceLocation'] == eqp) || (e['targetLocation'] == eqp)}
      else
        return res
      end
    end

    def triggers(params={})
      res = get(resource: 'triggers') || return
      if eqp = params[:eqp]
        return res.select {|e| e['eqpId'] == eqp}
      else
        return res
      end
    end

  end
end
