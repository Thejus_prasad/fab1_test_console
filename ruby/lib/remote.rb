# frozen_string_literal: false
=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author:   Steffen Steidten, 2010-07-29

Version:  2.1.3

History:
  2010-10-27 ssteidte, catch exception on close
  2011-05-26 ssteidte, added SCP file transfer
  2015-04-17 ssteidte, added support for JCAP jobs on JCAP server clusters
  2015-07-27 ssteidte, raise exception if no connection data is configured or provided
  2017-11-14 sfrieske, removed unused methods connect, reconnect and execute
  2021-02-05 sfrieske, use instance_of instead of kind_of
  2021-08-26 sfrieske, split read_properties off of misc, use read_config instead of _config_from_file
=end

require 'rubygems'
require 'net/ssh'
require 'net/scp'
require 'zlib'
require 'util/readconfig'
require 'util/readproperties'


# Holds SSH connections
class RemoteHost
  attr_accessor :ssh, :scp, :host, :user, :password, :cleartext

  def initialize(instance, overrides={})
    params = read_config(instance, 'etc/remotehosts.conf', overrides) ##if instance
    raise "no connection data for #{instance.inspect}" if params.empty?
    @host = params[:host]
    # take first host if more than one is configured
    @host = @host.split if @host.instance_of?(String)
    @host = @host.first if @host
    @user = params[:user]
    @password = params[:password]
    @cleartext = !!params[:cleartext]
    #open if @host
  end

  def inspect
    "#<#{self.class} #{@user}@#{@host}>"
  end

  # check if ssh connection is closed and (re-)open it
  def open
    # return true on success, nil if already connected
    unless @ssh && !@ssh.closed?   # does not work!
      pw = @cleartext ? @password : @password.unpack('m').first
      begin
        @ssh = Net::SSH.start(@host, @user, password: pw, config: false, keepalive: true, keepalive_interval: 45)
        true
      rescue => e
        $log.warn "error opening connection to host #{@host.inspect}\n#{$!}"
        $log.debug e.backtrace.join("\n")
        throw e
      end
    end
  end

  def close
    begin
      @ssh.close if @ssh and !@ssh.closed?
    rescue
      $log.debug $!
    end
  end

  # deprecated, use @host
  def hostname
    return @ssh.host
  end

  # return nil if there was no output
  def exec!(cmd)
    open #|| return
    return @ssh.exec!(cmd)
  end

  # return the file content or an empty string if the file is empty
  def read_file(fname, params={})
    @scp = nil
    open
    begin
      @scp = Net::SCP.new(@ssh)
      s = @scp.download!(fname)
      s = Zlib::GzipReader.new(StringIO.new(s)).read if fname.end_with?('gz')
      export = params[:export]
      File.binwrite(export, s) if export
      return s
    rescue => e
      $log.warn "error reading #{fname}\n#{$!}"
      $log.debug e.backtrace.join("\n")
      @scp.session.close
      return
    end
  end

  # read the modification time of a file
  def file_mtime(fname)
    Time.at(@ssh.exec!("stat -c %Y #{fname}").to_i)
  end

  # read a properties file, return hash of properties or nil
  def read_propsfile(fname)
    s = read_file(fname) || return
    read_properties(s.force_encoding('binary').split("\n"))  # from misc.rb
  end

  # write String s to the remote file fname, return true on success
  def write_file(s, fname, params={})
    @scp = nil
    export = params[:export]
    File.binwrite(export, s) if export
    open
    begin
      @scp = Net::SCP.new(@ssh)
      @scp.upload!(StringIO.new(s), fname)
      return true
    rescue => e
      $log.warn "error writing #{fname}\n#{$!}"
      $log.debug e.backtrace.join("\n")
      @scp.session.close if @scp
      return false
    end
  end

  def delete_file(fname, params={})
    $log.info "delete_file #{fname.inspect}"
    @ssh.exec!("test -f #{fname} && rm #{fname}")
  end

  # return true on success
  def download_file(fname, local='.')
    $log.info "download_file #{fname.inspect}, #{local.inspect}"
    ok = @ssh.exec!("test -f #{fname} && echo OK")
    ($log.warn "  no such file"; return) unless ok && ok.start_with?('OK')
    @scp = Net::SCP.new(@ssh)
    @scp.download!(remote, local)
  end

  # return true on success
  def download!(remote, local='.')
    $log.info "download! #{remote.inspect}, #{local.inspect}"
    open
    @scp = Net::SCP.new(@ssh)
    @scp.download!(remote, local)
  end

  # return nil
  def upload!(local, remote='.')
    $log.info "upload! #{local.inspect}, #{remote.inspect}"
    open
    @scp = Net::SCP.new(@ssh)
    @scp.upload!(local, remote)
  end
end


# Holds SSH connections to a cluster
class RemoteHostGroup
  attr_accessor :hosts, :sequential

  def initialize(name, params={})
    if name
      configfile = params[:config] || 'etc/remotehosts.conf'
      params = read_config(name, configfile, params)
      # if _params
      #   params = _params.merge(params)
      # else
      #   $log.warn "instance #{name.inspect} not found in config file #{configfile}"
      # end
    end
    hh = params[:host]
    hh = hh.split if hh.instance_of?(String)
    # @hosts = Array.new(hh.size)
    # threads = Array.new(hh.size)
    hostclass = params[:hostclass] || RemoteHost
    # hh.each_with_index {|h, i|
    #   threads[i] = Thread.new {@hosts[i] = hostclass.new(nil, params.merge(host: h))}
    # }
    # threads.each {|t| t.join}
    @hosts = hh.collect {|h| hostclass.new(nil, params.merge(host: h))}
    @sequential = params[:sequential]
  end

  def method_missing(meth, *args, &block)
    ret = Array.new(@hosts.size)
    if @sequential
      @hosts.each_with_index {|h, i| ret[i] = h.send(meth, *args, &block)}
    else
      threads = Array.new(@hosts.size)
      @hosts.each_with_index {|h, i|
        threads[i] = Thread.new {ret[i] = h.send(meth, *args, &block)}
      }
      threads.each {|t| t.join}
    end
    return ret
  end

  def respond_to?(meth)
    @hosts.first.respond_to?(meth)
  end

end


# JCAP server connection, can read property files
class JCAPServer < RemoteHost
  attr_accessor :logdir, :propsdir, :jobprops

  def initialize(env, params={})
    super(env ? "jcap.#{env}" : nil, params)  # no env if called from RemoteHostGroup or JCAPServers
    @logdir = params[:logdir] || '/local/logs/jCAP_5'
    @propsdir = params[:propsdir] || '/amdapps/apf/serverConfig/jCAP_2.0.1/properties'
    @jobprops = {}
  end

  # read a properties file and return a hash of properties or nil
  def read_jobprops(jobname)
    @jobprops[jobname] = read_propsfile("#{File.join(@propsdir, jobname)}.properties")
  end

  def logfile_mtime(jobname)
    file_mtime("#{File.join(@logdir, jobname)}.log")
  end

end


# JCAP job running on a server cluster (currently active/passive implemented)
class JCAPJob < RemoteHostGroup
  attr_accessor :env, :jobname, :jobprops, :srv

  def initialize(env, jobname, params={})
    super("jcap.#{env}", {hostclass: JCAPServer}.merge(params))
    @env = env
    @jobname = jobname
    @jobprops = get_jobprops
    @srv = get_activehost
    $log.info self.inspect
  end

  def inspect
    "#<#{self.class} env: #{@env}, #{@jobname.inspect}, active server: #{@srv.inspect}>"
  end

  def get_activehost
    begin
      mtimes = logfile_mtime(@jobname)
      @hosts[mtimes.index(mtimes.max)]
    rescue
    end
  end

  def get_jobprops
    begin
      props = read_jobprops(@jobname)
      props.instance_of?(Array) ? props.first : props
    rescue
      $log.warn "  error reading properties"
    end
  end

end
