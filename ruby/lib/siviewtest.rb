=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: Steffen Steidten, 2014-10-29

History:
  2015-01-09 ssteidte, added default stocker and sorter
  2015-08-18 ssteidte, add comment when scheduling a lot, should speed up new_lot
  2016-02-03 sfrieske, prevent deletion of lots in real carriers (ITDC)
  2016-10-07 sfrieske, added delete_registered_carrier
  2017-01-04 sfrieske, added ERPORDER to stb comment
  2017-01-26 sfrieske, do not change sublottype duration for SLT SCRAP in delete_testlots
  2017-03-28 sfrieske, disabled obsolete delete_testlots
  2017-04-20 sfrieske, added eqp/stocker location
  2018-03-12 sfrieske, minor improvements
  2018-05-03 sfrieske, removed obsolete delete_testlots
  2019-01-08 sfrieske, added parameter :user_parameters for new_lot(s)
  2019-02-13 sfrieske, added newlot_delay for DB replication after vendorlot prepare
  2020-03-05 sfrieske, cleaned up vendorlot receive and prepare parameters
  2021-01-15 sfrieske, added SRT carriers to default blacklist for get_empty_carrier
  2021-01-15 sfrieske, moved SiView::MM#new_srclot_for_product to SiView::Test#new_srclot
  2021-02-11 sfrieske, moved filters from SiView::MM#srclot_inq to SiView::Test#find_matching_srclot, cleanup
  2021-03-23 sfrieske, removed newlot_delay, call new_srclot separately if required
  2021-04-19 sfrieske, added parameter :asn to skip waiting for the STBMfgAttributes job
  2021-04-21 sfrieske, moved @sorter and @pg from SiView::Test to SJC::Test
  2021-07-13 sfrieske, removed use of prepare_separate_child
  2021-08-27 sfrieske, require waitfor instead of misc
  2021-08-31 aschmid3, added "ABC_" to carrier_blacklist
  2021-08-31 sfrieske, added "SAT" to carrier_blacklist
  2021-12-10 sfrieske, added "MC" to carrier_blacklist, removed instance variables asn, srcproduct and startbank
=end

require 'dummytools'
require 'siview'


module SiView

  # Support for SiView tests
  class Test
    attr_accessor :sv, :sm, :eis, :stocker, :carriers, :carrier_category, :carrier_blacklist, :nwafers,
      :erpitem, :customer, :route, :product, :product_eqpmonitor, :product_procmonitor, :product_dummy

    def initialize(env, params={})
      @sv = params[:sv] || SiView::MM.new(env, params)
      begin
        @sm = params[:sm] || SiView::SM.new(env, params)
      rescue
        $log.warn 'no connection to SM'
      end
      begin
        @eis = Dummy::EIs.new(env, params)
      rescue
        $log.warn 'no connection to the JavaEIs'
      end
      @stocker = params[:stocker] || (env.start_with?('f8', 'mt') ? 'UTSTO110' : 'UTSTO11')
      # without %, used with start_with?
      @carrier_blacklist = params[:carrier_blacklist] || %w(1 9 F1 F8 F9 FA FB FX Z ABC_ ABE BAS CP EB JC MC SAT SIL SRT SZ TC UX MC)
      @carrier_category = params.has_key?(:carrier_category) ? params[:carrier_category] : 'FEOL'
      @carriers = params[:carriers] || 'UX%'
      @erpitem = params[:erpitem] || 'ENGINEERING-U00'
      @customer = params[:customer] || 'gf'
      @route = params[:route] || 'UTRT001.01'
      @product = params[:product] || 'UT-PRODUCT000.01'
      @product_dummy = params[:product_dummy] || 'UT-DN-UTF001.01'
      @product_eqpmonitor = params[:product_eqpmonitor] || 'UT-MN-UTC001.01'
      @product_procmonitor = params[:product_procmonitor] || 'UT-PR-UTI002.01'
      @nwafers = params[:nwafers]
    end

    def inspect
      "#<#{self.class.name} #{@sv.inspect}>"
    end

    # return an empty and available carrier or an array of carriers matching the search filter @carriers
    def get_empty_carrier(params={})
      carrier = params.delete(:carrier) || @carriers
      carriers = @sv.carrier_list(
        {carrier_category: @carrier_category, carrier: carrier, empty: true, sjcheck: true}.merge(params))
      ($log.warn 'no empty carriers found, aborting'; return) if carriers.nil? || carriers.empty?
      exclude = params[:exclude] || []
      exclude = [exclude] if exclude.instance_of?(String) # exclude contains specific carriers
      carrier = carrier.sub('%', '')
      blacklist = @carrier_blacklist.select {|c| carrier.empty? || !carrier.start_with?(c)}
      n = params[:n] || 1
      ret = []
      carriers.each {|c|
        next if exclude.member?(c) || c.start_with?(*blacklist)
        csraw = @sv.carrier_status(c, raw: true)
        next if csraw.nil?
        next if csraw.cassetteStatusInfo.cassetteStatus == 'SCRAPPED'   # RTD rule for RetireAgedFoups does not learn quick enough
        next if csraw.cassetteStatusInfo.transferStatus == 'EI'
        next if !csraw.cassetteStatusInfo.transferReserveUserID.identifier.empty?
        desc = csraw.cassetteBRInfo.description
        next if desc.start_with?('REAL', 'ITDC Sorter Test') || desc.end_with?('Automation')
        cID = csraw.cassetteID
        # if !csraw.cassetteStatusInfo.transferReserveUserID.identifier.empty?
        #   next unless params[:reserve_cancel]   # TODO: use case?
        #   @sv.carrier_reserve_cancel(c)         # allows to get even leftovers from previous tests, use with care!
        # end
        next unless @sv.carrier_xferjob(cID).empty?
        # jCAP job RetireAgedFoups sets carriers to SCRAPPED, preventing VendorLotPrepare to work
        ccond = @sv.user_data(:carrier, c)['CARRIER_CONDITION']
        unless ['Cleaned', 'Used'].include?(ccond)
          @sv.carrier_condition_change(cID, 'Cleaned')
          @sv.carrier_counter_reset(cID)
        end
        @sv.carrier_status_change(cID, 'AVAILABLE') if csraw.cassetteStatusInfo.cassetteStatus != 'AVAILABLE'
        ret << cID
        break if ret.size == n
      }
      ($log.warn "not enough empty carriers, found #{ret.size} instead of #{n}"; return) if ret.size < n
      if params[:stockin]
        ret.each {|cID| @sv.carrier_xfer_status_change(cID, 'MI', params[:stocker] || @stocker) == 0 || return}
      end
      if params[:raw]
        return params.has_key?(:n) ? ret : ret.first
      else
        return params.has_key?(:n) ? ret.collect {|e| e.identifier} : ret.first.identifier
      end
    end

    # remove sort and xfer jobs, make carrier available and stock it in, return true on success
    def cleanup_carrier(c, params={})
      cID = c.instance_of?(String) ? @sv.oid(c) : c
      $log.info "cleanup_carrier #{cID.identifier.inspect}"
      csinfo = @sv.carrier_status(cID, raw: true).cassetteStatusInfo
      @sv.eqp_unload(csinfo.equipmentID, nil, cID) if csinfo.transferStatus == 'EI'
      if params[:xjs]   # TODO: remove?
        tend = Time.now + 180
        loop {
          break if @sv.carrier_xferjob_detail(cID).empty?
          if Time.now > tend
            @sv.carrier_xferjob_delete(cID)
            break
          else
            sleep 10
          end
        }
      end
      ok = (@sv.carrier_xfer_status_change(cID, 'MI', params[:stocker] || @stocker) == 0)
      cs = @sv.carrier_status(cID, raw: true)
      ok &&= (@sv.carrier_reserve_cancel(cID) == 0) unless csinfo.transferReserveUserID.identifier.empty?
      status = params[:status] || 'AVAILABLE'
      ok &&= (@sv.carrier_status_change(cID, status) == 0) if csinfo.cassetteStatus != status
      sjs = @sv.sj_list(carrier: cID)
      sjs.each {|sj| @sv.sj_cancel(sj.sorterJobID)} unless sjs.empty?
      if params[:make_empty]
        csinfo.strContainedLotInfo.each {|cli|
          ok &&= (@sv.wafer_sort(cli.lotID, '') == 0)
        }
      end
      # TODO: npw_reserve_cancel('', cID ...)
      return ok
    end

    # delete a MM registered carrier, return true on success
    def delete_registered_carrier(carrier)
      $log.info "delete_registered_carrier #{carrier.inspect}"
      ($log.info '  skipping'; return) if carrier.nil? || carrier.empty?
      ($log.warn '  no such carrier'; return) if @sv.carrier_list(carrier: carrier).empty?
      ($log.warn '  SM carrier!'; return) unless @sm.object_ids(:carrier, carrier).empty?
      cs = @sv.carrier_status(carrier, raw: true) || return
      cs.cassetteStatusInfo.strContainedLotInfo.each {|e| @sv.delete_lot_family(e.lotID)}
      cstat = cs.cassetteStatusInfo.cassetteStatus
      @sv.carrier_status_change(cs.cassetteID, 'AVAILABLE') if cstat == 'INUSE'
      @sv.carrier_status_change(cs.cassetteID, 'NOTAVAILABLE') if cstat != 'NOTAVAILABLE'
      res = @sv.durable_delete('Cassette', carrier)
      return res == 0
    end

    # clean up eqp, restart JavaEI and clean up again, return true on success
    def cleanup_eqp(eqp, params={})
      eqpID = eqp.instance_of?(String) ? @sv.oid(eqp) : eqp
      @sv.eqp_cleanup(eqpID, status_change: false, scriptparams: false)
      sleep 1
      @eis.restart_tool(eqpID.identifier) || ($log.warn "error (re)starting JavaEI for #{eqpID.identifier}"; return)
      sleep 3
      @sv.eqp_cleanup(eqpID, params).zero?
    end

    # ensure eqp location is set as requested, return true on success
    def eqp_location(eqp, loc, cname=:eqp)
      $log.info "#{cname}_location #{eqp.inspect}, #{loc}"
      udata = @sv.user_data(cname, eqp)
      unew = {}
      ['AmhsArea', 'Location'].each {|u| unew[u] = loc[u] if loc.has_key?(u) && udata[u] != loc[u]}
      unless unew.empty?
        @sv.user_data_delete(cname, eqp, unew.keys).zero? || return  # ensure there is no MM UDATA masking SM UDATA
        @sm.update_udata(cname, eqp, unew) || ($log.warn 'error updating SM UDATA'; return)
      end
      return true
    end

    # ensure stocker location is set as requested, return true on success
    def stocker_location(eqp, loc, cname=:eqp)
      return eqp_location(eqp, loc, :stocker)
    end

    # return a new lot ready to use
    def new_lot(params={})
      $log.info "new_lot #{params}"
      product = params[:product] || @product
      nwafers = params[:nwafers] # nono || 25
      if nwafers.nil?
        nwafers = params.has_key?(:waferids) ? params[:waferids].size : @nwafers  # may still be nil
      end
      if params.has_key?(:bysrclot) # may be nil in special tests!
        srclot = params[:bysrclot]
        lot = srclot
        lgtype = 'By Source Lot'
      else
        srclot = params[:srclot]
        lot = params[:lot]
        lgtype = params[:lotGenerationType]
      end
      carrier_category = params.has_key?(:carrier_category) ? params[:carrier_category] : @carrier_category
      carrier = params[:carrier] || @carriers
      if srclot.nil?
        if carrier.end_with?('%')
          carrier = get_empty_carrier(carrier: carrier, carrier_category: carrier_category, raw: true, stockin: true) || return
        end
        srcparams = {raw: true, product: product, carrier: carrier}
        srcparams[:nwafers] = nwafers unless nwafers.nil?
        srcparams[:bank] = params[:startbank] if params.has_key?(:startbank)
        [:srcproduct, :waferids, :slots, :lasermark, :ondemand].each {|e| srcparams[e] = params[e] if params.has_key?(e)}
        srclot = new_srclot(srcparams) || return
      else
      end
      #
      asn = params[:asn]  # normally not set (nil), for special tests (STBMfgWIPAttributes)
      erpitem = params[:erpitem] || @erpitem
      dpml = params[:dpml] || 1.2
      fsd = (params[:fsd] || Time.now).strftime('%F')
      comment = params[:comment] || "ERPITEM=#{erpitem},DPML=#{dpml},PLANNED_FSD=#{fsd}"
      comment += ",ERPORDER=#{params[:erporder]}" if params[:erporder]
      comment += ',ASN=Y' if asn != false   # regular case
      relparams = {raw: true, lot: lot, nwafers: nwafers, lotGenerationType: lgtype, customer: @customer,
        product: product, srclot: srclot, comment: comment}
      # not: lottype
      [:customer, :route, :sublottype, :lotowner, :priorityclass, :epriority, :order, :starttime, :finishtime].each {|e|
        relparams[e] = params[e] if params.has_key?(e)
      }
      relparams[:route] ||= @route unless params.has_key?(:product)  # allow to auto-detect route for the product
      rlotID = @sv.new_lot_release(relparams) || return
      lotID = @sv.stb(rlotID, raw: true) || return
      # set default and custom (or overriden) lot script parameters as STBWIPAttributes would
      if asn.nil?  # regular case
        due = @sv.lots_info_raw(lotID).strLotInfo.first.strLotBasicInfo.dueTimeStamp[0..9]  # year-month-day
        srcfab = @sv.env.start_with?('f8', 'mt') ? 'F8' : 'F1'
        coo = @sv.env.start_with?('f8', 'mt') ? 'US' : 'DE'
        uparams = {'CountryOfOrigin'=>coo, 'ERPItem'=>erpitem, 'FabDPML'=>dpml, 'FOD_COMMIT'=>due, 'FOD_INIT'=>due,
          'FSD'=>fsd, 'GateCD_Target_PLN'=>'MEDIUM', 'SourceFabCode'=>srcfab, 'TurnkeyType'=>'U'}
        uparams['ERPSalesOrder'] = params[:erporder] if params.has_key?(:erporder)
        uparams.merge!(params[:user_parameters]) if params.has_key?(:user_parameters)
        @sv.user_parameter_change('Lot', lotID.identifier, uparams).zero? || return
        @sv.lot_note_register(lotID, 'STBMfgWIPAttributes', 'Data set by SiViewTest.')
      end
      # optional lot completion
      if params[:bankin]
        @sv.lot_opelocate(lotID, :last)
        @sv.lot_bankin(lotID) if @sv.lots_info_raw(lotID).strLotInfo.first.strLotBasicInfo.bankID.identifier.empty?
      end
      return params[:raw] ? lotID : lotID.identifier
    end

    # return array of lots ready to use, TODO: remove?
    def new_lots(n, params={})
      return n.times.collect {new_lot(params) || return}
    end

    # prepare the lot for re-STB on the product/route passed in params and STB it
    def restb_lot(lot, params)
      $log.info "restb_lot #{lot.inspect}, #{params}"
      # complete Fab lot if necessary and move it to the start bank
      @sv.lot_opelocate(lot, :last) if @sv.lot_info(lot).status != 'COMPLETED'
      startbank = params[:startbank]
      if startbank.nil?
        # get new startbank from SM
        oinfo = @sm.object_info(:mainpd, params[:route]).first
        startbank = oinfo.specific[:startbank]
      end
      (@sv.lot_bank_move(lot, startbank) == 0) || ($log.warn "error moving lot #{lot} to bank #{startbank.inspect}"; return)
      uparams = @sv.user_parameter('Lot', lot)
      erpitem = uparams.find {|e| e.name == 'ERPItem'}.value
      user_parameters = params[:user_parameters] || {}
      uparams.each {|e|
        next if user_parameters.has_key?(e.name)  # do not overwrite explicitly passed user parameters
        next unless e.valueflag
        next unless e.name.start_with?('Turnkey') || ['CountryOfOrigin', 'SourceFabCode'].member?(e.name)
        user_parameters[e.name] = e.value
      }
      asn = params.has_key?(:asn) ? params[:asn] : @asn
      li = @sv.lot_info(lot)
      return new_lot(asn: asn, bysrclot: lot, product: params[:product], route: params[:route],
        customer: li.customer, sublottype: li.sublottype, erpitem: erpitem, user_parameters: user_parameters)
    end

    # create a new monitor lot and bank it in optionally, return lot id or nil
    def new_controllot(lottype, params={})
      $log.info "new_controllot #{lottype.inspect}, #{params}"
      product = params[:product]
      unless product
        product = case lottype
        when 'Equipment Monitor'; @product_eqpmonitor
        when 'Process Monitor'; @product_procmonitor
        when 'Dummy'; @product_dummy
        end
      end
      nwafers = params[:nwafers] || @nwafers  # may still be nil ??
      carrier_category = params.has_key?(:carrier_category) ? params[:carrier_category] : @carrier_category
      carrier = params[:carrier] || @carriers
      srclot = params[:srclot]
      if srclot.nil?
        if carrier.end_with?('%')
          carrier = get_empty_carrier(carrier: carrier, carrier_category: carrier_category, raw: true, stockin: true) || return
        else
          cleanup_carrier(carrier)  ## ??
        end
        srcparams = {raw: true, product: product, carrier: carrier, lottype: lottype, sublottype: lottype} # vlot SLT for STBCancel
        srcparams[:nwafers] = nwafers unless nwafers.nil?
        srcparams[:bank] = params[:startbank] if params.has_key?(:startbank)  # TODO: ever used?
        [:srcproduct, :slots, :lasermark, :ondemand].each {|e| srcparams[e] = params[e] if params.has_key?(e)}
        srclot = new_srclot(srcparams) || return
      end
      stbparams = {raw: true}
      stbparams[:nwafers] = nwafers unless nwafers.nil?
      lotID = @sv.stb_controllot(lottype, product, srclot, stbparams) || return
      # cleanup_carrier(@sv.lot_info(lot).carrier, xjs: params[:xjs], stocker: params[:stocker])
      # cleanup_carrier(carrier)
      # optional lot completion, currently not used
      if params[:bankin]
        @sv.lot_opelocate(lotID, :last)
        @sv.lot_bankin(lotID) if @sv.lots_info_raw(lotID).strLotInfo.first.strLotBasicInfo.bankID.identifier.empty?
      end
      return params[:raw] ? lotID : lotID.identifier
    end

    # find or create a new source lot, return prepared source lot on success
    def new_srclot(params={})
      pparams = {}
      params.each_pair {|k, v|
        pparams[k] = v.instance_of?(@sv.jcscode.objectIdentifier_struct) ? v.identifier : v
      }
      $log.info "new_srclot #{pparams}"
      product = params[:product]  # product to create the srclot for, not the srclot product!
      unless product
        lottype = params[:lottype]
        product = case lottype
        when 'Equipment Monitor'; @product_eqpmonitor
        when 'Process Monitor'; @product_procmonitor
        when 'Dummy'; @product_dummy
        else @product
        end
      end
      srclotID = nil
      if params[:ondemand]
        # do not create a new scrlot if an eligible one already exists
        fparams = {}
        [:nwafers, :srcproduct].each {|e| fparams[e] = params[e] if params.has_key?(e)}
        srclotID = find_matching_srclots(product, fparams).first
        if srclotID
          sliraw = @sv.lots_info_raw(srclotID).strLotInfo.first
          if carrier = params[:carrier]   # TODO: remove (?)
            @sv.wafer_sort(srclotID, carrier) if sliraw.strLotLocationInfo.cassetteID.identifier != carrier
          end
          if sliraw.strLotBasicInfo.lotType != 'Vendor'
            return params[:raw] ? srclotID : srclotID.identifier
          end
        end
      end
      carrier = params[:carrier] || get_empty_carrier(raw: true)   # no other parameters allowed for get_empty_carrier!
      ($log.warn '  no empty carrier found'; return) if carrier.nil?
      if srclotID.nil?  # :ondemand false (default) or no matching srclot found
        srcproduct = params[:srcproduct]
        bank = params[:bank]
        unless bank && srcproduct
          if @sv.customized
            pi = @sv.AMD_product_details(product)
            ($log.warn 'no product definition found'; return) if pi.nil?
            if srcproduct.nil?
              srcprod = pi.sourceProductID.find {|src|
                srcpi = @sv.AMD_product_details(src.identifier)
                srcpi.routeID.identifier.empty? && srcpi.candidateSubLotTypes.empty?
              }
              ($log.warn "  no srcproduct for #{product.inspect}"; return) if srcprod.nil?
              srcproduct = srcprod
            end
            bank = pi.startBankID
          else
            # this can be very slow for Equipment Monitors
            pi = nil
            pl = @sv.product_list(params[:lottype])
            pl.each {|e|
              next if e.productID.identifier != product
              next if e.sourceProductID.empty?
              next if srcproduct && e.sourceProductID.find {|s| s.identifier == srcproduct}.nil?
              pi = e
              break
            }
            ($log.warn '  no product definition found'; return) if pi.nil?
            if srcproduct.nil?
              srcprod = pi.sourceProductID.find {|src|
                srcpi = pl.find {|p| p.productID.identifier == src.identifier}
                (srcpi.nil? || srcpi.routeID.identifier.empty?) && srcpi.candidateSubLotTypes.empty?
              }
              ($log.warn "  no srcproduct for #{product.inspect}"; return) if srcprod.nil?
              srcproduct = srcprod
            end
            bank = pi.startBankID
          end
        end
        rcvparams = {raw: true}
        [:vendorlot, :vendor, :sublottype, :lotlabel].each {|e|
          rcvparams[e] = params[e] if params.has_key?(e)
        }
        vlotID = @sv.vendorlot_receive(bank, srcproduct, params[:nwafers] || 25, rcvparams) || return
      else
        vlotID = srclotID
      end
      prepparams = {}
      [:lottype, :sublottype, :waferids, :slots, :lasermark, :raw].each {|e|
        prepparams[e] = params[e] if params.has_key?(e)
      }
      return @sv.vendorlot_prepare(vlotID, carrier, prepparams)
    end

    # return array of suitable srclotIDs; used if :ondemand in new_srclot only
    def find_matching_srclots(product, params={})
      nwafers = params[:nwafers] || 25
      srcproduct = params[:srcproduct]
      ret = []
      lottypes = ['', 'Vendor']
      lottypes << params[:lottype] if params[:lottype]
      @sv.srclot_inq(product, '').strSourceLot.each {|l|
        next unless lottypes.member?(l.lotType)
        srclotID = l.lotID
        next if nwafers > l.notAllocCount  # too small
        next if srcproduct && srcproduct != l.productID.identifier
        liraw = @sv.lots_info_raw(srclotID, wafers: true)
        # # skip if carrier is not AVAILABLE
        # next if !l.cassetteID.identifier.empty? && @sv.carrier_status(l.cassetteID).status != 'AVAILABLE'
        # skip if wafers are already assigned
        next if liraw.strLotInfo.first.strLotWaferAttributes.inject(false) {|s, w| s || w.STBAllocFlag}
        if @sv.user_data(:lot, srclotID.identifier)['LOT_LABEL']
          $log.warn "  skipping srclot with LOT_LABEL: #{srclotID.identifier}"
          next
        end
        ret << srclotID
      }
      return ret
    end

  end


  # find methods using a given Tx, return array of SiView::XX::methods
  def self.find_txmethods(txname)
    ret = []
    txs = []
    Dir['ruby/lib/siview/**/*rb'].each {|f|
      lines = File.readlines(f)
      mline = nil
      lines.each {|line|
        line.strip!
        if line.start_with?('#')
          next
        elsif line.start_with?('def ')
          mline = line
        elsif pos = line.index(txname)
          dotpos = line[0..pos].rindex('.') || line[0..pos].rindex("'")
          endpos = line[dotpos..-1].index('(')
          endpos = endpos.nil? ? -1 : endpos + dotpos
          txs << line[dotpos+1..endpos-1]
          ret << mline.split[1].split('(').first
        end
      }
    }
    $log.info "txs: #{txs.uniq.sort}"
    ret.uniq.sort
  end

  # find testcases using a given Tx, return array of RubyTestCase classes
  def self.find_txtestcases(txname)
    txmethods = self.find_txmethods(txname)
    ret = []
    Dir['ruby/testcases/**/*rb'].each {|f|
      lines = File.readlines(f)
      cline = nil
      lines.each {|line|
        line.strip!
        if line.start_with?('#')
          next
        elsif line.start_with?('class ')
          cline = line
        else
          txmethods.each {|m|
            if line.include?(m)
              ret << File.basename(f)   ##(cline.split[1].split('<').first || f)
            end
          }
        end
      }
    }
    ret.uniq.sort
  end

end
