=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: Steffen Frieske, GLOBALFOUNDRIES, INC., 2016

Version:  1.2.2

History:
  2017-01-03 sfrieske, new RTD::EmuRemote without DRb
  2017-09-21 sfrieske, added emudelay support
  2018-03-26 sfrieske, separated control interface for new RTD::EmuRemote to rtdemuadmin.rb
  2019-02-18 sfrieske, keep track of rule calls, for one shot tests
  2019-04-04 sfrieske, added support for the QueryExecutor emulator
  2020-07-14 sfrieske, use bytesize for msg length calculation
  2021-02-05 sfrieske, use instance_of instead of kind_of
  2021-08-26 sfrieske, use read_config instead of _config_from_file
=end

require 'json'
require 'socket'
require 'util/readconfig'
require 'util/waitfor'

Thread.abort_on_exception = false   # set to true for debugging only!


module RTD

  # Remote Control of the RTD Emulator via EmuAdmin, including Rule Execution
  class EmuRemote
    KnownReports = {mfgcockpit: 'ie_TASKLIST_all.report'}

    attr_accessor :host, :port, :lastmid, :rprocs, :rsocks, :rthreads, :rcalled

    def initialize(env, overrides={})
      params = read_config("#{env}.emu", 'etc/rtdservers.conf', overrides)
      @host = params[:host] || Socket.gethostname  # local hostname, for Windows
      @port = params[:adminport] || 21212
      @lastmid = 0
      @rprocs = {}
      @rsocks = {}
      @rthreads = {}
      @rcalled = {}
      $log.info self.inspect
    end

    def inspect
      "#<#{self.class.name} for #{@host}:#{@port}>"
    end

    # call methods on the connected RTD::Dispatcher, e.g. emudelay, emudelay=(t), emurandom, emurandom=(f), emustations
    def method_missing(meth, *args, &block)
      return _call_emuadmin(meth, *args)
    end

    # start serving calls for an emulated method, register local, send remote to EmuAdmin
    #  type: 'emustation' or 'emureport'
    #  lmethod: ref to local proc, rparams: remote method (add_emustation) arguments
    def _start_emumethod(mid, type, lmethod, rparams)
      # locally register the method to be called
      @rprocs[mid] = {type: type, rproc: lmethod}.merge(rparams)  # need :station and :rule for deletion
      # call emu from a new socket for emu callbacks
      sock = TCPSocket.new(@host, @port)
      @rsocks[mid] = sock
      # send add_emustation with method id
      msg = {method: "add_#{type}", params: rparams.merge(mid: mid)}.to_json
      sock.write([msg.bytesize].pack('N'))
      sock.write(msg)
      # get the add_emustation answer
      data = sock.read(4)
      mlen = data.unpack('N').first
      ret = JSON.parse(sock.read(mlen))
      # wait for remote method calls
      @rthreads[mid] = Thread.new {
        loop {
          break if @rsocks[mid].nil?
          begin
            data = sock.read(4)
            next if data.nil?
            mlen = data.unpack('N').first
            data = sock.read(mlen)
            $log.info "EmuRemote received #{data}"
            parsed = JSON.parse(data)
            mid = parsed['mid']
            entry = @rprocs[mid]
            ##$log.info "EmuRemote entry #{entry.inspect}"
            ($log.warn {"EmuRemote wrong data or stale mid:\n#{parsed}"}; return) if entry.nil?
            if entry[:type] == 'emustation'
              station = parsed['station']
              rule = parsed['rule']
              params = parsed['params']
              ($log.warn {"EmuRemote incomplete data: #{parsed}"}; return) if station.nil? || rule.nil?
              $log.info "EmuRemote calling mid #{mid}: #{station.inspect}, #{rule.inspect}"
              # TODO: generalized proc call w/o type specifics!
              res = entry[:rproc].call(station, rule, params)
            elsif entry[:type] == 'emureport'
              $log.warn "XXXXXXXXXXXXXXXXXXXXX emureport"
              res = ''
            end
            res = res.to_json
            $log.info 'EmuRemote sending response'
            sock.write([res.bytesize].pack('N'))
            sock.write(res)
            @rcalled[mid] = true  # last
          rescue IOError  # catch Errors due to a race condition on termination
            # $log.warn "XXXXXXXXXXXXXXXXXXX #{$!}"
            sleep 1
          end
        }
      }
    end

    # stop serving calls for an emulated method, internally used
    def _stop_emumethod(mid)
      @rsocks[mid].close
      @rsocks.delete(mid)
      sleep 2
      @rthreads[mid].kill
      @rthreads.delete(mid)
      @rprocs.delete(mid)
      @rcalled.delete(mid)
    end

    # call the admin server to send commands (not for callbacks), internally used
    def _call_emuadmin(method, mparams=nil)
      callparams = {method: method}
      callparams[:params] = mparams unless mparams.nil?
      msg = callparams.to_json
      sock = TCPSocket.new(@host, @port)
      sock.write([msg.bytesize].pack('N'))
      sock.write(msg)
      #
      data = sock.read(4)
      mlen = data.unpack('N').first
      data = sock.read(mlen)
      sock.close
      $log.debug {"emuadmin response: #{data}"}
      res = JSON.parse(data)
      ($log.warn "RTD server error: #{res}"; return) if res['error']
      return res['result']
    end


    # add an emustation with a specific rule, typically executed from this EmuRemote instance, return mid
    def add_emustation(stn, params={}, &blk)
      rule = params[:rule] || 'WhatNextLotList'
      method = params[:method] || blk   # use standard emulator method if nil
      $log.info "add_emustation #{stn.inspect}, rule: #{rule.inspect}, method: #{method.inspect}"
      delete_emustation(stn, rule: rule, local: true)  # remove the current setting, required!
      if method.nil? || method.instance_of?(String)
        # just register the emu's local method with the emu (e.g. 'empty_reply')
        return _call_emuadmin('add_emustation', {station: stn, rule: rule, method: method})
      else
        mid = @lastmid += 1
        # start a callback and register it with the emu
        _start_emumethod(mid, 'emustation', method, {station: stn, rule: rule})
        $log.info "added emustation, mid: #{mid}"
        return mid
      end
    end

    # delete an emustation
    def delete_emustation(stn, params={})
      rule = params[:rule] || 'WhatNextLotList'
      unless params[:local]
        # needed for the deletion of emu locals (nil, 'empty')
        $log.info "delete_emustation #{stn.inspect}, rule: #{rule.inspect}"
        ret = _call_emuadmin('delete_emustation', {station: stn, rule: rule})
      end
      mid = params[:mid]
      unless mid
        # remove the currently configured proc (e.g. if returning to std emulation), required!
        mid, _ = @rprocs.each_pair.find {|mid, mparams| mparams[:station] == stn && mparams[:rule] == rule}
      end
      _stop_emumethod(mid) if mid
      return ret
    end

    # add an emureport, return mid
    def add_emureport(report, params={}, &blk)
      report = KnownReports[report] if report.instance_of?(Symbol)
      method = params[:method] || blk   # use standard emulator method if nil
      $log.info "add_emureport #{report.inspect}, method: #{method.inspect}"
      delete_emureport(report, local: true)  # remove the current setting, required!
      if method.nil?
        $log.info "add_emureport #{report.inspect}: missing method ('file', 'once')"
        return
      elsif method.instance_of?(String)
        # optionally upload data
        if data = params[:data]
          _call_emuadmin('upload_emureport', {report: report, data: data})
        end
        # register the emu's local method with the emu (e.g. 'empty_reply')
        return _call_emuadmin('add_emureport', {report: report, method: method})
      else
        mid = @lastmid += 1
        # start a callback and register it with the emu
        _start_emumethod(mid, 'emureport', method, {report: report})
        $log.info "added emureport, mid: #{mid}"
        return mid
      end
    end

    # delete an emureport
    def delete_emureport(report, params={})
      unless params[:local]
        # needed for the deletion of emu locals ('file', 'once')
        $log.info "delete_emureport #{report.inspect}"
        ret = _call_emuadmin('delete_emureport', {report: report})
      end
      mid = params[:mid]
      unless mid
        # remove the currently configured proc (e.g. if returning to std emulation), required!
        mid, _ = @rprocs.each_pair.find {|mid, mparams| mparams[:report] == report}
      end
      _stop_emumethod(mid) if mid
      return ret
    end


    # response generation: return a dispatcher response object from column names (e.g. for JCAP)
    def create_response(cols, data={})
      return {'headers'=>cols, 'types'=>Array.new(cols.size, 'STRING'), 'widths'=>Array.new(cols.size, 1)}.merge(data)
    end

    # caller helper: add the emustation, wait for a response and delete the emustation; return true if called
    def add_emustation_wait(stn, params={}, &blk)
      mid = add_emustation(stn, {rule: params[:rule], method: params[:method]}, &blk)
      $log.info "waiting up to #{params[:timeout] || 60} s for the rule call"
      ret = wait_for(timeout: params[:timeout], tstart: params[:tstart]) {@rcalled[mid]}
      $log.info "EmuRemote: rule mid #{mid} has not been called" unless ret
      delete_emustation(stn, {mid: mid, rule: params[:rule]})
      return ret
    end

    # # EMPTY rule helper, create a response with the carriers passed and call add_emustation
    # def add_emustation_EMPTY
    # end

  end

end
