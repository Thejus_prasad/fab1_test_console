=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.


Author: Conny Stenzel, 2019-04-01

History:
  2020-07-23 aschmid3, added table LOT_LIST_DATA
  2021-08-27 sfrieske, require uniquestring instead of misc
=end

require 'fileutils'
require 'siview'
require 'util/uniquestring'
require_relative 'rtdemuremote'


module RTD

  # Create Fabscope test data
  class TDG
    attr_accessor :sv, :logdir, :emuremote, :taskcategories, :tasktypespecs, :_tasks  # dev only

    def initialize(env, params={})
      @sv = params[:sv] || SiView::MM.new(env)
      @logdir = 'log/tdg'
      FileUtils.mkdir_p(@logdir)
      @emuremote = RTD::EmuRemote.new("#{env}_qe")
      @tableheaders = {
        'DFS_COMMENTS' => %w(fs_table_id task_id note creation_time user_id),
        'DFS_WAFERS' => %w(fs_table_id task_id lot_id wafer_id wafer_internal_id significant wafer_scan_time),
        'G_TASKLIST' => %w(fs_table_id task_id task_type task_type_short task_sub_type task_context task_description task_rank_class task_rank_reason
          task_add_info task_run_key task_related_run_keys department_list eqp_id chamber_id e10_eqp_state e10_chamber_state ptool mtool pchamber claim_user_id
          gantt_eqp_list fdc_result_id history_context_value hold_claim_memo hold_reason_description hold_reason_id hold_user_id inhibit_claim_memo
          inhibit_context_combination_list inhibit_context_value_list inhibit_id inhibit_reason_desc inhibit_sub_lot_types inhibit_responsible_pd_id_list
          inhibit_register_user inhibit_owner inhibit_exp_release_time lot_cast_id lot_id lot_is_pilot lot_lrecipe_id lot_mainpd_id lot_ope_no lot_ope_no_pass_count
          lot_owner_id lot_pd_id lot_priority lot_priority_desc lot_prodspec_id lot_related_lot_id related_lot_list lot_sublot_type lot_tech_id lot_timelink_criticality
          lot_timelink_timeout lot_wfr_qty lot_status lot_purpose lot_ope_note_flag_exists_in priority_source rank_no lot_pd_id_name lot_current_pd_id
          lot_current_pd_id_name lot_current_ope_no lot_current_ope_no_pass_count lot_prodgrp_id is_AutomotiveProduct lot_at_operation_time lot_responsible_ope_marker
          reticle_list reticle_pod_list space_ch_id space_ckc_id space_lds space_channel_name space_run_id space_sample_id space_type space_violation_type
          space_parameter space_responsible_pd pcl_spec_id pcl_table_name pcl_context_name start_time expected_duration toolgroup_list dfs_case_id dfs_internal_id
          dfs_name dfs_severity dfs_defect_codes dfs_set_module dfs_eqp_list dfs_last_change_time lot_block_reason lot_block_target_eqp_list lot_block_claim_memo
          lot_block_toolwish_reason claim_memo photo_layer ecap_instance_id ecap_lot_id ecap_template_name ecap_node_started ecap_node qv_order_description
          qv_qual_state qv_qual_task_details qv_severity dtc_action qv_lab_link_list dtc_due_time dtc_duration dtc_potential_wip dtc_start_time dtc_stn_family
          dtc_task_category dtc_task_reason dtc_tool_preference dtc_work_instructions tcl_too_long_processing ta_alarm_description ta_alarm_id ta_severity
          ta_sms_info ta_alarm_category a3_code_task_description a3e_apc_info a3e_due_time a3e_expected_duration a3e_lot_list a3e_ovl_toolscore a3e_ps_apc_thread_id
          a3e_reticle_list rba_alert_color rba_alert_description rba_alert_id rba_alert_class rba_creation_time rba_lot_list rba_port rba_severity rba_shelf
          data_source ctrljob_id),
        'G_TASKLIST_PHOTOLAYERS' => %w(fs_table_id photolayer),
        'G_TASKLIST_TASKPRIORITY_CLASSIFICATION' => %w(fs_table_id priority color_code description),
        'G_TASKLIST_TASKTYPES' => %w(fs_table_id task_type task_type_short),
        'G_TASKLIST_TOOLGROUPS' => %w(fs_table_id department toolgroup_id),
        'INHIBIT_RELATED_LOTS' => %w(fs_table_id task_id lot_id current_product_id responsible_route_id responsible_ope_id responsible_pd_id),
        'LOT_LIST_DATA' => %w(fs_table_id task_id lot_id lot_pd_id lot_mainpd_id lot_ope_no lot_ope_no_pass_count)  # for lot ope note (LON)
       }
      @taskcategories = {
        'INH' => ['FDCInhibit', 'OtherXInhibit', 'SPCInhibit: INLINE', 'SPCInhibit: SETUP'],
        'HLD' => ['FDCLotHold', 'SPCLotHold: INLINE', 'SPCLotHold: SETUP', 'OtherLotHold: PROD', 'OtherLotHold: TW'],
        'BLOC' => ['BlockedLot: PROD'], #future tasktype: , 'BlockedLot: TW'
        'DFS' => ['DFSCase'],
        #'2NDP' => ['NoDispatchTool'], #obsolete?
        'eCAP' => ['Other eCAPs'], #'eCAP', obsolete?
        'TCL' => ['TimeCriticalLot'],
        'LongS' => ['LongSitter: PROD'],
        'Qual' => ['Qual Issue'],
        'SAP-E' => ['SAP Event'],
        'RBA' => ['Rule-Based Alert'],
        'Alarm' => ['ToolAlarm'],
        'A3E' => ['Auto-3 Enabler'],
        'DTC' => ['DynamicToolCorridor'],
        'LADF' => ['Lot Abort eCAP'],
        'FCG' => ['Fab Configuration'],
        'MTC' => ['Maintenance Task'],
        'RET' => ['Reticle Transport']
      }
      @tasktypespecs = {
        'FDCLotHold' => [{reason: 'XFDC', task_sub_type: 'FDC', task_description: 'QA Test: Test lot for MFC task type FDCLotHold'}],
        'SPCLotHold: INLINE' => [{reason: 'X-S1', task_sub_type: 'SPC-I', task_description: 'QA Test: LDS->INLINE'}],
        'SPCLotHold: SETUP' => [{reason: 'X-S1', task_sub_type: 'SPC-S', task_description: 'QA Test: LDS->SETUP'}],
        'OtherLotHold: PROD' => [{reason: 'TENG', task_sub_type: 'TENG', task_description: 'QA Test: Test hold for MFC tests'}],
        'OtherLotHold: TW' => [{reason: 'F-ENG', task_sub_type: 'F-ENG', task_description: 'QA Test: APC Fehler (Catalyst finalization timeout)'}],
        'FDCInhibit' => [{reason: 'XFDC', task_sub_type: 'FDC', task_description: 'QA Test: FDC-ALARM - FDC_FAULT_UNCLASSIFIED'}],
        'OtherXInhibit' => [{reason: 'XAPC', task_sub_type: 'XAPC', task_description: 'QA Test: Feedback for chamber x is invalid. Please qualify chamber x.'}],
        'SPCInhibit: INLINE' => [{reason: 'X-INSPACE', task_sub_type: 'SPC-I', task_description: 'QA Test: Mean above control limit'}],
        'SPCInhibit: SETUP' => [{reason: 'X-INSPACE', task_sub_type: 'SPC-S', task_description: 'QA Test: Mean above control limit'}],
        'Maintenance Task' => [
          {task_sub_type: 'M-LTE', task_description: 'QA Test: Maintenance Execution Start Overdue'},
          {task_sub_type: 'M-PSE', task_description: 'QA Test: Long Maintenance Execution Pause'},
        ],
        'BlockedLot: PROD' => [{task_description: 'QA Test: LOTSTART.01:wrongprotocolzone'}],
        'LongSitter: PROD' => [{task_description: 'QA Test: BTF1OUT.01: since 447d 3h'}],
        'TimeCriticalLot' => [{task_sub_type: 'SRKT'}],
        'ToolAlarm' => [{task_description: 'CRITICAL: QA Test- Sequencer has detected a deadlock'}],
        'Lot Abort eCAP' => [{task_description: 'QA Test: C-NDDC2E-MISSING_PATTERN,C-NDDC2E-MISSING_PATTERN'}],
        'DFSCase' => [{task_context: 'BS Kratzer 6uhr nahe Center r=10mm', task_description: 'QA Test: BS Kratzer 6uhr nahe Center r=10mm'}],
        'Rule-Based Alert' => [
          {task_sub_type: 'P', task_description: 'QA Test: nostart11'},
          {task_sub_type: 'T', task_description: 'QA Test: nostart11'},
        ],
        'Auto-3 Enabler' => [
          {task_sub_type: 'CARR', task_description: 'QA Test: Prepare 1 Carrier for ClassX'},
          {task_sub_type: 'GMP', task_description: 'QA Test, Grid-Pilot erstellen'},
          {task_sub_type: 'PS', task_description: 'QA Test: ALC4360=O=ID:O-i-823401177485--V3-MSKAV013EM93X05-28BK--ALC4360--Mainframe--ALC4360--ZA106330V3AZLAM1--1360V1AZ1_4360V1,A=ID:A-i-5545160551661--V3-MSKAV013EM93X05-28BK--ALC4360--Mainframe--ALC4360--ZA106330V3AZLAM1; ZA106330V3AZLAM1=D=ID:D-3689357556371'},
        ],
        'DynamicToolCorridor' => [{task_sub_type: 'PLT', task_description: 'QA Test: Run a lot'}],
        'Fab Configuration' => [
          {task_sub_type: 'EX-GT', task_id: 'QA Test: ERF_e0A597_C_[Not Set]_EX-GT', task_context: 'e0A597', task_description: 'SUBMITTED',
            task_add_info: 'New technology development'},
          {task_sub_type: 'EX-SP', task_id: 'QA Test: ERF_e0B765_A_[Not Set]_EX-SP', task_context: 'Prio:e0B765', task_description: 'WAITING FOR LOTATTACH',
            task_add_info: 'Process Enhancement'},
          {task_sub_type: 'GN-GS', task_id: 'QA Test: AR_512008_GN-GS', task_context: 'AR512008', task_description: 'Approved - System: SIVIEW-FAB1 SubSystem: OPS-MSM (SiV,SAP,-,SPC)',
            task_add_info: '<a href="http://fast/request/MSR/Main/Frame.asp?appId=37&requestId=512008" target="_blank">AR512008</a> ukuehne1 Type: Change existing account'},
          {task_sub_type: 'GN-LP', task_id: 'QA Test: SETUPFC_C05-00001542_2_04/24/201510:07:32_GN-LP', task_context: 'C05-00001542', task_description: 'generic-setup',
            task_add_info: 'Test task filter configuration -- Test task filter configuration - Run2 -- <a href="http://vf1sfcp01:10080/setupfc-rest/v1/specs/C05-00001542/v2/content?format=html" target="_blank">C05-00001542 </a>'},
          {task_sub_type: 'GN-OT', task_id: 'QA Test: WR_TASK0653250_GN-OT', task_context: 'RITM0677800', task_description: 'Setup-Spec - Awaiting Internal Response',
            task_add_info: 'Setup-Spec <a href="https://globalfoundries.service-now.com/nav_to.do?uri=sc_task.do?sys_id=" target="_blank">TASK0653250</a> <br>Requestor: Wunderlich, Jens<br>Phone: 83204<br>Module: MSS<br>Need date: 07.05.2018<br>Type: Setup-Spec<br>Path RTE file (Excel): \\vdrsfgrp1\drs_gs$\RTE\<br>Full description:'},
          {task_sub_type: 'GN-PA', task_id: 'QA Test: RS_20707_GN-PA', task_context: 'Prio:REC.F1.LIT.STP-ASML.20707',
            task_description: 'Buddy Check and Release Stepper Job in ARMOR - SetupPerson: ateschmi',
            task_add_info: '<a href="http://vfc1rsp01:8080/rs/#EditRequest/id=20707" target="_blank">REC.F1.LIT.STP-ASML.20707</a>'},
          {task_sub_type: 'GN-PC', task_id: 'QA Test: SETUPFC_C14-TEST-TASKFILTER_1_04/23/201506:53:43_GN-PC', task_context: 'C14-TEST-TASKFILTER',
            task_description: 'generic-setup-check SetupPerson: qauser01',
            task_add_info: 'Test task filter -- Test run1 -- <a href="http://vf1sfcp01:10080/setupfc-rest/v1/specs/C14-TEST-TASKFILTER/v1/content?format=html" target="_blank">C14-TEST-TASKFILTER </a>'},
          {task_sub_type: 'GN-PO', task_id: 'QA Test: WR_TASK0651299_GN-PO', task_context: 'Prio:RITM0675749', task_description: 'Proto Setup',
            task_add_info: 'Proto Setup <a href="https://globalfoundries.service-now.com/nav_to.do?uri=sc_task.do?sys_id=" target="_blank">TASK0651299</a> Litho Proto Setup fuer  XM28002-A1 und MPWCS10-A1Bitte beide PG zusammen abarbeiten und dazu das MSU File im Ordner MPWCS10-A1 und XM28002-A1 verwenden (enthaelt gleich beide PGs):I:\Factory_Automation\MS-W\Proto-Setup\Litho\Setup Status\MSU\MPWCS10-A1 und XM28002-A1Referenzen:XM28002-A1  ->  ALT1250-A1MPWCS10-A1  ->  PUMA28H-A1Link Handlungsanweisung: http://f1onewiki:21080/pages/viewpage.action?pageId=27116660'},
          {task_sub_type: 'GN-PR', task_id: 'QA Test: SETUPFC_C05-00001332_4_[NotSet]_GN-PR', task_context: 'Prio:C05-00001332', task_description: 'siview-rollback',
            task_add_info: 'ITDC Process Template 2.4.0 UploadTest -- b -- <a href="http://vf1sfcp01:10080/setupfc-rest/v1/specs/C05-00001332/v4/content?format=html" target="_blank">C05-00001332 </a>'},
          {task_sub_type: 'GN-PT', task_id: 'QA Test: SETUPFC_C14-R15STEFFI_1_05/04/201505:41:50_GN-PT', task_context: 'C14-R15STEFFI', task_description: 'generic-setup',
            task_add_info: 'TDC SiView R15 AutoSetup.FC Integration Spec 1 -- TDC SiView R15 AutoSetup.FC Integration Spec 1 -- <a href="http://vf1sfcp01:10080/setupfc-rest/v1/specs/C14-R15STEFFI/v1/content?format=html" target="_blank">C14-R15STEFFI </a>'},
          {task_sub_type: 'GN-SP', task_id: 'QA Test: WR_TASK0653669_GN-SP', task_context: 'Prio:RITM0678143', task_description: 'Source Product attach',
            task_add_info: 'Source Product attach <a href="https://globalfoundries.service-now.com/nav_to.do?uri=sc_task.do?sys_id=" target="_blank">TASK0653669</a> Product -- ME-DN-U-RTA-SPIKE-75K.CHASource Product -- ME-DN-U-RTA-SPIKE-75K.CHBRequestor -- Melcher, TorstenPhone -- 82629Module -- TWMNeed date -- 07.05.2018Priority -- undefinedType -- Change Source ProductFull description -- Effort in hours --'},
          {task_sub_type: 'RT-GT', task_id: 'QA Test: ECM_EC000145_RT-GT', task_context: 'EC000145', task_description: 'PRE_APPROVALS',
            task_add_info: '<a href="http://vfc1ecmp01/#/ec//tasks" target="_blank">EC000145</a> consistency check operation udata vs operation exist'},
          {task_sub_type: 'RT-MF', task_id: 'QA Test: RTE_115276_RT-MF', task_context: 'Prio:RTE115276', task_description: 'module feedback - Rework attachment',
            task_add_info: '<a href="http://myteamsdrs/sites/Fab36_FA/Support/FST/FlowProduct/SetupRequest/RTE115276.xml" target="_blank">RTE115276 </a>55BC - 55BC: Erstellen neuer Reworkrouten (R-LC-55NM-Vx-PSX-SIARC-ETCLN.01/R-LC-55NM-V1-PSX-SIARC-ETCLN.01)'},
          {task_sub_type: 'RT-OP', task_id: 'QA Test: ECM_EC00013N_RT-OP', task_context: 'EC00013N', task_description: 'POST_RELEASE',
            task_add_info: '<a href="http://vfc1ecmp01/#/ec//tasks" target="_blank">EC00013N</a> check connecting operation rule'},
          {task_sub_type: 'RT-PC', task_id: 'QA Test: PCP_pcprs-2017-jcha2-12041411_RT-PC', task_context: 'pcprs-2017-jcha2-12041411', task_description: 'Review - Postponed: C05 - Route move',
            task_add_info: '<a href="http://tecnetnew.gfoundries.com/~pcprs/status/update/" target="_blank"> </a>22MM, Cha, Junghun, Short Loop TYWIN01-A1S PCP move from Product PCPC Spec to DEV PCP Spec.'},
          {task_sub_type: 'RT-PO', task_id: 'QA Test: RTE_114947_RT-PO', task_context: 'RTE114947', task_description: 'module feedback - New Product Group for Proto',
            task_add_info: '<a href="http://myteamsdrs/sites/Fab36_FA/Support/FST/FlowProduct/SetupRequest/RTE114947.xml" target="_blank">RTE114947 </a>28H1 - Context RTE for MPWCS08'},
        ],
        'Other eCAPs' => [
          {task_sub_type: 'ALL-SETUP-PARTICLE-CHECK'},
          {task_sub_type: 'DR-PASSRATE'},
          {task_sub_type: 'Hold'},
          {task_sub_type: 'LotHold'},
          {task_sub_type: 'Sleep'},
          {task_sub_type: 'Test'},
          {task_sub_type: 'Upscan'},
          {task_sub_type: 'V28'},
          {task_sub_type: 'V29'},
          {task_sub_type: 'V30'},
          {task_sub_type: 'WfT'},
        ],
        'Reticle Transport' => [
          {task_sub_type: 'INSP'},
          {task_sub_type: 'MOVE'},
        ],
        'SAP Event' => [
          {task_sub_type: '2NDP', task_description: 'QA Test: SAP Event'},
          {task_sub_type: 'SDQL', task_description: 'QA Test: SAP Event'},
          {task_sub_type: 'UDDly', task_description: 'QA Test: SAP Event'},
          {task_sub_type: 'UPQL', task_description: 'QA Test: SAP Event'},
        ],
        'Qual Issue' => [{task_sub_type: 'Alert'}, task_description: 'KT:STP-CTC_513624924_BLOCKED'],
      }
      @taskrankclasses = [
        ['1', '#FF0000', 'Time Critical Lots'],
        ['2', '#FF7128', 'High Prio Blk / Hld Lots'],
        ['3', '#FF9045', 'LineCtrl Btlnk'],
        ['4', '#FFB062', 'Module Btlnk'],
        ['5', '#FFCF7F', 'TimeBasedPrioTask'],
        ['6', '#FFEF9C', 'Prio Blk/Hld Lots'],
        ['7', '#C5D9F1', 'Non-Prio Blk/Hld Lots'],
        ['8', '#ABC3E7', 'Blk/Hld Lots Task'],
        ['9', '#90ADDC', 'Info Tasks'],
        ['10', '#7597D1', 'TimeBasedWaitTask'],
        ['0', '#FFFFFF', 'Not defined Rank'],
      ]
      @taskphotolayers = ['THJ', 'PVP', 'DE']
      @tasktoolgroups = {
        'C4B' => %w(B-DRY-3 B-DRY-5 B-DRY-6 B-ETX B-LIT-1 B-LIT-2 B-LIT-5 B-LIT-8 B-LS1-1 B-LS1-10 B-LS1-11 B-LS1-12 B-LS1-13 B-LS1-16 B-LS1-17 B-LS1-18 B-LS1-19 B-LS1-2 B-LS1-20 B-LS1-21 B-LS1-22 B-LS1-23 B-LS1-24 B-LS1-25 B-LS1-27 B-LS1-28 B-LS1-29 B-LS1-3 B-LS1-30 B-LS1-31 B-LS1-32 B-LS1-33 B-LS1-34 B-LS1-40 B-LS1-41 B-LS1-42 B-LS1-43 B-LS1-44 B-LS1-45 B-LS1-5 B-LS1-6 B-LS1-7 B-LS1-8 B-LS1-9 B-LS2-1 B-LS2-10 B-LS2-11 B-LS2-12 B-LS2-13 B-LS2-14 B-LS2-15 B-LS2-16 B-LS2-17 B-LS2-18 B-LS2-19 B-LS2-2 B-LS2-20 B-LS2-21 B-LS2-22 B-LS2-23 B-LS2-24 B-LS2-25 B-LS2-26 B-LS2-27 B-LS2-3 B-LS2-4 B-LS2-40 B-LS2-42 B-LS2-43 B-LS2-44 B-LS2-45 B-LS2-46 B-LS2-47 B-LS2-48 B-LS2-49 B-LS2-50 B-LS2-51 B-LS2-6 B-LS2-7 B-LS2-8 B-LS2-9 B-LS3 B-LS3-1 B-LS3-10 B-LS3-11 B-LS3-12 B-LS3-13 B-LS3-14 B-LS3-15 B-LS3-16 B-LS3-17 B-LS3-18 B-LS3-19 B-LS3-2 B-LS3-20 B-LS3-21 B-LS3-22 B-LS3-23 B-LS3-24 B-LS3-3 B-LS3-30 B-LS3-31 B-LS3-32 B-LS3-33 B-LS3-34 B-LS3-35 B-LS3-36 B-LS3-37 B-LS3-4 B-LS3-5 B-LS3-6 B-LS3-7 B-LS3-8 B-LS3-9 B-MPX-NI B-MTR-1 B-MTR-11 B-MTR-12 B-MTR-5 B-MTR-8 B-PLAB B-PSX B-SUPPORT B-WET B-WET-2 B-WET-3 B-WET-4 B-WET-5 B-WET-9 PU:B-DRY PU:B-LITHO PU:B-QUALITY PU:B-WET PU:FVX PU:B-ALL),
        'CFM' => %w(C-ALL C-ANNEX C-ANX C-BTF C-DFU C-F1M1 C-F1M2 C-FAB36 C-FAB38 C-MT-DFS-BFI-AMAT C-MT-DFS-BFI-KLA C-MT-DFU-OPI-AMI C-MT-SEI-SEM-CON),
        'CMP' => %w(I-CLN-ACS I-CLN-ALL I-CLN-ANX I-CLN-BATCH I-CLN-BATCH-MOD2/ANX I-CLN-MOD1 I-CLN-MOD2 I-CLN-SCB I-CLN-SCB-ANX I-CLN-SCB-MOD1 I-CLN-SWC I-CLN-SWC-FEOL/HK I-CLN-SWC-FEOL/HK-ANX I-CLN-SWC-FEOL/HK-MOD1 I-CLN-SWC-FEOL/HK-MOD2 I-CLN-SWC-MOL/BEOL I-CLN-SWC-MOL/BEOL-ANX P-CUPLT P-CUPOL P-FAB38 P-FEOL_MOL P-FVX P-MET-BEOL P-MPX1604 P-MPX600 P-MPX604 P-MPX608 P-PLT-ALL P-PLT-M2 P-PLT-MEAS P-PLT-PROC P-SOW-ALL P-STI PU:FVX PU:BWC PU:SWC PU:NON CU CMP PLT),
        'DIF' => %w(D-DIF D-EPI D-EPI12x D-EPI20x D-FVX D-FVX-MLD D-FVX-NIT-TEOS-ANL D-FVX-OX-POLY D-IMP D-IMX D-LLC-FVX-EPI D-LLC-RTA-EPI D-MET D-RTA D-RTA-DPN-HK D-RTA-RTO-ANL D-SNK D-THK I-IMP-ALL I-IMP-ANX I-IMP-HC I-IMP-HE I-IMP-MC PU:FVX PU:EPI PU:RTA),
        'DQE' => %w(Q-ALL Q-OPI PU:B-QUALITY),
        'ETC' => %w(E-AMAT_Beol E-BST-DCL-WDT E-BeoL E-BeoL2 E-CDS E-FEOL E-HK E-LAM_BeoL_60-70x E-LAM_BeoL_92x E-LAM_Beol E-LAM_FeoL_FET E-LAM_FeoL_SP E-LAM_Feol E-LAM_MoL E-LLC-BEOL E-LLC-FEOL E-LLC_BEOL E-MOL E-MoL E-PSX E-PSX_FEOL E-PSX_MOL E-SNK_Beol E-TEL_BeoL E-TEL_Beol E-TEL_MoL PU:MOL PU:LAM FEOL PU:LAM BEOL),
        'INT' => %w(T-LLC-CVD),
        'IT' => %w(I-LLC-SNK),
        'LIT' => %w(L-ALC L-ANNEX L-ArfIm L-DUV L-INSP L-LLC-ALC L-MET L-RET PU:ARFI PU:ARF DUV RET),
        'MSS' => %w(A-AWS A-AWS;-SJM A-AWS_A3Mon A-AWS_AcrossPG A-AWS_OthMon A-HYB A-LPI A-MEV A-ZIGNORE PU:MSS),
        'MTR' => %w(M-ALL M-CDS M-DFS M-LLC-ALL M-MES M-MTTHK M-NWGCDS M-NWGTHK M-OCD M-OTH M-OVL M-THK),
        'PCO' => %w(O-ALL),
        'PLN' => %w(PU:B-WET),
        'TFM' => %w(F-ALL F-CVD F-CVD-BEOL F-CVD-BEOL-MOL F-CVD-FEOL F-CVD-FEOL-LAM F-CVD-FEOL-MOL F-CVD-Producer F-CVD_1 F-CVD_2 F-FEOL F-LLC-CVD F-LLC-FEOL F-LLC-PVD F-PVD F-PVD-BEOL F-PVD-Endura F-PVD-NoBEOL F-PVD-Non-Endura F-RTA F-ULK-UV PU:FVX PU:CVD PU:PVD PU:ARF DUV RET),
        'TST' => %w(S-AWS S-BTF S-MEM S-PCS S-PDF S-SORT S-SPP S-SRT S-T36 S-TCS S-TST S-WET PU:O-WPCK PU:S-SORT PU:S-TCSTST PU:S-PDF PU:S-WET),
      }
    end

    def inspect
      "#<#{self.class.name}>"
    end

    def mfc_object(tasks, params={})
      $log.info "mfc_object (#{tasks.size} tasks)"
      ret = String.new

      ret += @tableheaders['DFS_COMMENTS'].join('|') + "\n"
      params[:dfscomments].each {|dfscomment| ret += dfscomment} if params[:dfscomments]

      ret += @tableheaders['DFS_WAFERS'].join('|') + "\n"
      params[:dfswafers].each {|dfswafer| ret += dfswafer} if params[:dfswafers]

      ret += @tableheaders['G_TASKLIST'].join('|') + "\n"
      tasks.each {|task|
        contents = []
        @tableheaders['G_TASKLIST'].each {|column| contents << task[column.to_sym] || ''}
        ret += contents.join('|') + "\n"
      }

      ret += @tableheaders['G_TASKLIST_PHOTOLAYERS'].join('|') + "\n"
      @taskphotolayers.each {|taskphotolayer|
        ret += 'G_TASKLIST_PHOTOLAYERS' + '|' + taskphotolayer + "\n"
      }

      ret += @tableheaders['G_TASKLIST_TASKPRIORITY_CLASSIFICATION'].join('|') + "\n"
      @taskrankclasses.each {|taskrankclass|
        ret += 'G_TASKLIST_TASKPRIORITY_CLASSIFICATION' + '|' + taskrankclass.join('|') + "\n"
      }

      ret += @tableheaders['G_TASKLIST_TASKTYPES'].join('|') + "\n"
      @taskcategories.each {|tts, tasktypes|
        tasktypes.each {|tt|
          ret += 'G_TASKLIST_TASKTYPES' + '|' + tt + '|' + tts + "\n"
        }
      }

      ret += @tableheaders['G_TASKLIST_TOOLGROUPS'].join('|') + "\n"
      @tasktoolgroups.each {|department, toolgroups|
        toolgroups.each {|tg|
          ret += 'G_TASKLIST_TOOLGROUPS' + '|' + department + '|' + tg + "\n"
        }
      }

      ret += @tableheaders['INHIBIT_RELATED_LOTS'].join('|') + "\n"

      ret += @tableheaders['LOT_LIST_DATA'].join('|') + "\n"
      params[:lotlistdata].each {|line| ret += line} if params[:lotlistdata]

      return ret
    end

    def push_data(tasks, tasktypeshort, params={})
      content = mfc_object(tasks, params) || return
      method = params[:method] || 'once'
      unless params[:dryrun]
        @emuremote.add_emureport('ie_TASKLIST_all.report', method: method, data: content) || return
      end
      File.binwrite("#{@logdir}/#{Time.now.to_i}_#{tasktypeshort}_ie_TASKLIST_all.report.answer", content)
    end

    def remove_data
      @emuremote.delete_emureport('ie_TASKLIST_all.report')
    end

    #used by prepare_blockedlot_task, prepare_lothold_task, prepare_spacelothold_task, prepare_longsitter_task, prepare_timecriticallot_task
    def prepare_lot_task(li, tt, tts, tasktypeshort, params={})
      passcount = @sv.lot_operation_list(li.lot).find {|e| e.opNo = li.opNo}.pass
      department = li.department
      department = 'MSS' if department == 'FAT'
      task = {
        fs_table_id: 'G_TASKLIST',
        task_type: tt,
        task_type_short: tasktypeshort,
        task_sub_type: tts[:task_sub_type],
        task_context: li.lot,
        task_description: 'QA',
        department_list: department,
        lot_cast_id: li.carrier,
        lot_id: li.lot,
        lot_lrecipe_id: @sv.lot_upcoming_info(li.lot, li.opNo, li.opEqps.first).logicalRecipeID.identifier,
        lot_mainpd_id: li.route,
        lot_ope_no: li.opNo,
        lot_ope_no_pass_count: passcount,
        lot_owner_id: li.user,
        lot_pd_id: li.op,
        lot_priority: li.priority,
        lot_prodspec_id: li.product,
        lot_sublot_type: li.sublottype,
        lot_tech_id: li.technology,
        lot_wfr_qty: li.nwafers,
        lot_status: li.status,
        lot_pd_id_name: li.opName,
        lot_current_pd_id: li.op,
        lot_current_pd_id_name: li.opName,
        lot_current_ope_no: li.opNo,
        lot_current_ope_no_pass_count: passcount,
        lot_prodgrp_id: li.productgroup,
        is_AutomotiveProduct: 'TRUE',
        lot_at_operation_time: @sv.siview_time(li.claim_time).utc.strftime('%FT%T%:z'),   # correct format ?
        start_time: @sv.siview_time(li.due_time).utc.strftime('%FT%T%:z'),   # correct format ?
        toolgroup_list: 'O-ALL',
        photo_layer: li.masklevel,    # old: layer (but this means 'Mfg Layer' in SiView)
        ctrljob_id: li.cj
      }
      return task
    end

    #used by 'FDCLotHold', 'OtherLotHold: PROD', 'OtherLotHold: TW'
    def prepare_lothold_task(li, reason, tt, tts, tasktypeshort, memo, lhi, params={})
      timestamp = Time.now.to_i
      date = Time.now.utc.strftime('%FT%T%:z')
      cinfo = @sv.code_list('LotHold').find {|e| e.code.identifier == reason}
      task = prepare_lot_task(li, tt, tts, tasktypeshort, params)
      task.merge!({
        task_id: "#{li.lot}-LotHold.#{reason}.#{@sv.user}.-#{date}",
        task_description: "QA#{timestamp}_#{memo}",  # uniq_string('QA_')
        task_rank_class: '8',
        task_rank_reason: '8: Blk/Hld Lots Task',
        # task_rank_reason: "8: Blk/Hld Lots Task  by StartTime=#{@sv.siview_time(lhi.timestamp).strftime('%m/%d/%Y %H:%M:%S')}",
        task_run_key: '7605',  # unique_id.to_s
        rank_no: '7605',
        task_related_run_keys: '7603,7604',
        hold_claim_memo: lhi.memo,
        hold_reason_description: cinfo.description,
        hold_reason_id: lhi.reason,
        hold_user_id: lhi.user,
        lot_responsible_ope_marker: lhi.rsp_mark,
        lot_ope_no: params[:holdrespop] || li.opNo,
        lot_pd_id: params[:holdrespopid] || li.op,
        lot_pd_id_name: params[:holdrespopname] || li.opName,
        start_time: @sv.siview_time(lhi.timestamp).utc.strftime('%FT%T%:z')  # strftime('%m/%d/%Y %H:%M:%S')
      })
      return task
    end

    #used by 'SPCLotHold: INLINE', 'SPCLotHold: SETUP'
    def prepare_spacelothold_task(li, reason, tt, tts, tasktypeshort, memo, lhi, params={})
      $log.info params.inspect
      timestamp = Time.now.to_i
      date = Time.now.utc.strftime('%FT%T%:z')
      cinfo = @sv.code_list('LotHold').find {|e| e.code.identifier == reason}
      task = prepare_lot_task(li, tt, tts, tasktypeshort, params)
      task.merge!({
        task_id: "#{li.lot}-LotHold.#{reason}.#{@sv.user}.-#{date}",
        task_description: "QA#{timestamp}_#{memo}",
        task_rank_class: '8',
        task_rank_reason: '8: Blk/Hld Lots Task',
        task_run_key: '7605',
        rank_no: '7605',
        task_related_run_keys: '7603,7604',
        hold_claim_memo: lhi.memo,
        hold_reason_description: cinfo.description,
        hold_reason_id: lhi.reason,
        hold_user_id: lhi.user,
        lot_responsible_ope_marker: lhi.rsp_mark,
        space_ch_id: params[:space_ch_id] || '248734',
        space_ckc_id: '1',
        space_lds: 'INLINE',
        space_run_id: '4498778',
        space_sample_id: params[:space_sample_id] || '7005852413',
        space_type: 'Raw above specification',
        space_violation_type: 'Raw above specification',
        space_parameter: 'QA_PARAM_904',
        space_responsible_pd: params[:space_responsible_pd] || 'empty',
        ptool: params[:ptool] || '',
        mtool: params[:mtool] || '',
        lot_ope_no: params[:holdrespop] || li.opNo,
        lot_pd_id: params[:holdrespopid] || li.op,
        lot_pd_id_name: params[:holdrespopname] || li.opName,
      })
      return task
    end

    #used by'BlockedLot: PROD'
    def prepare_blockedlot_task(li, tt, tts, tasktypeshort, params={})
      timestamp = Time.now.to_i
      passcount = @sv.lot_operation_list(li.lot).find {|e| e.opNo = li.opNo}.pass
      task = prepare_lot_task(li, tt, tts, tasktypeshort, params)
      task.merge!({
        task_id: "#{li.lot}-block-#{li.op}-#{passcount}",
        task_sub_type: '',
        task_description: "QA#{timestamp}_#{li.op}:wrongprotocolzone",
        task_rank_class: '7',
        task_rank_reason: '7: Non-Prio Blk/Hld Lots',
        task_run_key: '7705',
        rank_no: '7705',
        department_list: 'PCO',
        lot_block_reason: 'wrongprotocolzone',
        lot_block_claim_memo: 'wrong protocol zone',
      })
      return task
    end

    #used by 'LongSitter: PROD'
    def prepare_longsitter_task(li, tt, tts, tasktypeshort, params={})
      timestamp = Time.now.to_i
      task = prepare_lot_task(li, tt, tts, tasktypeshort, params)
      task.merge!({
        task_id: "#{li.lot}-#{tt}-#{li.route}-#{li.opNo}",
        task_sub_type: '',
        task_description: "QA#{timestamp}_#{li.op}: since 447d 3h",
        task_rank_class: '2',
        task_rank_reason: '2: High Prio Blk / Hld Lots',
        task_run_key: '2777',
        rank_no: '2777',
        department_list: 'C4B',
      })
      return task
    end

    #used by 'TimeCriticalLot'
    def prepare_timecriticallot_task(li, tt, tts, tasktypeshort, params={})
      timestamp = Time.now.to_i
      task = prepare_lot_task(li, tt, tts, tasktypeshort, params)
      task.merge!({
        task_id: "#{li.lot}-#{tt}-#{li.route}-#{li.opNo}",
        task_description: "QA#{timestamp}_#{li.op}:",
        task_rank_class: '1',
        task_rank_reason: "1: Time Critical Lots!-#{tts[:task_sub_type]}",
        task_run_key: '35',
        rank_no: '35',
        department_list: 'PCO',
      })
      return task
    end

    #used by 'ToolAlarm'
    def prepare_toolalarm_task(eqpi, ch, tt, tts, tasktypeshort, params={})
      runkey = unique_id.to_s
      starttime = Time.now.utc.strftime('%FT%T%:z')
      starttimeid = Time.now.utc.strftime('%m/%d/%Y %H:%M:%S')
      chstatus = ''
      chstatus = eqpi.chambers.select {|c| c[:chamber] == ch}[0][:status] if ch != ''
      task = {
        fs_table_id: 'G_TASKLIST',
        task_id: "#{eqpi.eqp}-#{starttimeid}",
        task_type: tt,
        task_type_short: tasktypeshort,
        task_context: eqpi.eqp,
        task_description: tts[:task_description],
        task_rank_class: '5',
        task_rank_reason: "5: TimeBasedPrioTask by StartTime=#{starttimeid}",
        task_run_key: runkey,
        rank_no: runkey,
        department_list: 'ETC',
        eqp_id: eqpi.eqp,
        chamber: ch,
        e10_eqp_state: eqpi[:status],
        e10_chamber_state: chstatus,
        gantt_eqp_list: eqpi.eqp,
        start_time: starttime,
        ta_alarm_description: 'Sequencer has detected a deadlock',
        ta_alarm_id: '1504640000',
        ta_severity: '1',
        ta_sms_info: 'Configured',
        ta_alarm_category: 'CRITICAL',
        ctrljob_id: "ITDC-#{runkey}-#{Time.now.utc.strftime('%Y')}",
      }
      return task
    end

    #used by 'Maintenance Task'
    def prepare_maintenance_task(eqpi, ch, tt, tts, tasktypeshort, params={})
      runkey = unique_id.to_s
      taskcontext = eqpi.eqp
      chstatus = ''
      chstatus = eqpi.chambers.select {|c| c[:chamber] == ch}[0][:status] if ch != ''
      task = {
        fs_table_id: 'G_TASKLIST',
        task_context: taskcontext,
        task_id: "#{tasktypeshort}#{tts[:task_sub_type]}#{taskcontext}xXxSAPPMNOTIFICATIONIDxXx",
        task_type: tt,
        task_type_short: tasktypeshort,
        task_sub_type: tts[:task_sub_type],
        task_description: tts[:task_description],
        task_rank_class: '8',
        task_rank_reason: '8: Blk/Hld Lots Task',
        task_add_info: "#{eqpi.eqp}.QA: TEST",
        task_run_key: runkey,
        department_list: 'MSS',
        eqp_id: eqpi.eqp,
        chamber_id: ch,
        e10_eqp_state: eqpi[:status],
        e10_chamber_state: chstatus,
        gantt_eqp_list: eqpi.eqp,
        rank_no: runkey,
        expected_duration: '600',
        toolgroup_list: 'O-ALL',
        ctrljob_id: "ITDC-#{runkey}-#{Time.now.utc.strftime('%Y')}",
      }
    end

    #used by 'Lot Abort eCAP'
    def prepare_ladf_task(li, eqpi, tt, tts, tasktypeshort, params={})
      taskid = unique_id.to_s
      runkey = unique_id.to_s
      starttime = Time.now.utc.strftime('%FT%T%:z')
      starttimeid = Time.now.utc.strftime('%m/%d/%Y %H:%M:%S')
      task = {
        fs_table_id: 'G_TASKLIST',
        task_id: "#{tasktypeshort}_#{taskid}",
        task_type: tt,
        task_type_short: tasktypeshort,
        task_context: li.lot,
        task_description: tts[:task_description],
        task_rank_class: '8',
        task_rank_reason: "8: Blk/Hld Lots Task (ConspRF Rk=99 -> Inh Ratio=0 -> Lot RTD=)  by StartTime=#{starttimeid}",
        task_run_key: runkey,
        task_related_run_keys: unique_id.to_s,
        rank_no: runkey,
        department_list: 'CFM',
        mtool: eqpi.eqp,
        lot_id: li.lot,
        related_lot_list: li.lot,
        space_parameter: 'QA-C-NDDC2E-MISSING_PATTERN',
        start_time: starttime,
        photo_layer: li.layer,
        ecap_instance_id: unique_id.to_s,
        ecap_lot_id: li.lot,
        ecap_template_name: 'MIC_SC_Release_NDDP_and_DR-PASSRATE',
        ecap_node_started: starttime,
        ecap_node: 'Display Results',
        ctrljob_id: "ITDC-#{runkey}-#{Time.now.utc.strftime('%Y')}",
      }
      return task
    end

    #used by 'DFSCase'
    def prepare_dfs_task(tt, tts, tasktypeshort, params={})
      taskid = unique_id.to_s
      runkey = unique_id.to_s
      starttime = Time.now.utc.strftime('%FT%T%:z')
      starttimeid = Time.now.utc.strftime('%m/%d/%Y %H:%M:%S')
      task = {
        fs_table_id: 'G_TASKLIST',
        task_id: "DFS_#{taskid}",
        task_type: tt,
        task_type_short: tasktypeshort,
        task_sub_type: '4',
        task_context: tts[:task_context],
        task_description: tts[:task_description],
        task_rank_class: '9',
        task_rank_reason: "9: Info Tasks",
        task_run_key: runkey,
        rank_no: runkey,
        department_list: 'CFM',
        start_time: starttime,
        toolgroup_list: 'QA:MFC:Tools',
        dfs_case_id: '13-02-0538',
        dfs_internal_id: taskid,
        dfs_name: 'BS Kratzer 6uhr nahe Center r=10mm',
        dfs_severity: '4',
        dfs_defect_codes: '1102',
        dfs_set_module: 'CFM',
        dfs_eqp_list: 'UTCMFC01, UTCMFC02',   # 2020-08-05 - aschmid3: Tool UTCMFC02 added for multiple Eqp display (MSR1559316/MSR1650641)
        dfs_last_change_time: starttimeid,
        ctrljob_id: "ITDC-#{runkey}-#{Time.now.utc.strftime('%Y')}",
      }
      return task
    end

    #used by 'Rule-Based Alert'
    def prepare_rulebasedalert_task(li, eqpi, tt, tts, tasktypeshort, params={})
      runkey = unique_id.to_s
      starttime = Time.now.utc.strftime('%FT%T%:z')
      starttimeid = Time.now.utc.strftime('%m/%d/%Y %H:%M:%S')
      task = {
        fs_table_id: 'G_TASKLIST',
        task_id: "#{eqpi.eqp}.P1_#{tts[:task_description]}_-#{starttimeid}",
        task_type: tt,
        task_type_short: tasktypeshort,
        task_sub_type: tts[:task_sub_type],
        task_context: eqpi.eqp + '.P1',
        task_description: tts[:task_description],
        task_rank_class: '5',
        task_rank_reason: "5: TimeBasedPrioTask by StartTime=#{starttimeid}",
        task_add_info: 'Port in `LoadComp` without ControlJob for more than 10 min.',
        task_run_key: runkey,
        rank_no: runkey,
        department_list: 'ETC',
        eqp_id: eqpi.eqp,
        related_lot_list: li.lot,
        e10_eqp_state: eqpi[:status],
        gantt_eqp_list: eqpi.eqp,
        start_time: starttime,
        toolgroup_list: 'QA:MFC:Tools',
        rba_alert_color: '#FF0000',
        rba_alert_description: 'Port in `LoadComp` without ControlJob for more than 10 min.',
        rba_alert_id: tts[:task_description],
        rba_alert_class: 'Processing-Alert ',
        rba_creation_time: starttime,
        rba_lot_list: li.lot,
        rba_port: 'P1',
        rba_severity: '1',
        rba_shelf: 'QA Shelf',
        ctrljob_id: "ITDC-#{runkey}-#{Time.now.utc.strftime('%Y')}",
      }
      return task
    end

    #used by 'eCAP', 'Other eCAPs'
    def prepare_ecap_task(li, eqpi, tt, tts, tasktypeshort, params={})
      taskid = unique_id.to_s
      runkey = unique_id.to_s
      starttime = Time.now.utc.strftime('%FT%T%:z')
      starttimeid = Time.now.utc.strftime('%m/%d/%Y %H:%M:%S')
      task = {
        fs_table_id: 'G_TASKLIST',
        task_id: "#{tasktypeshort}_#{taskid}",
        task_type: tt,
        task_type_short: tasktypeshort,
        task_sub_type: tts[:task_sub_type],
        task_context: li.lot,
        task_description: "#{eqpi.eqp},QA-C-NDDC45E-NEEDLES",
        task_rank_class: '8',
        task_rank_reason: "8: Blk/Hld Lots Task (ConspRF Rk=99 -> Inh Ratio=0 -> Lot RTD=)  by StartTime=#{starttimeid}",
        task_add_info: starttimeid,
        task_run_key: runkey,
        department_list: 'CFM',
        eqp_id: eqpi.eqp,
        ptool: eqpi.eqp,
        mtool: eqpi.eqp,
        gantt_eqp_list: eqpi.eqp,
        lot_id: li.lot,
        related_lot_list: li.lot,
        rank_no: runkey,
        space_parameter: 'QA-C-NDDC45E-NEEDLES',
        start_time: starttime,
        toolgroup_list: 'O-ALL',
        photo_layer: li.layer,
        ecap_instance_id: unique_id.to_s,
        ecap_lot_id: li.lot,
        ecap_template_name: 'MIC_SC_Release_NDDP_and_DR-PASSRATE',
        ecap_node_started: starttime,
        ecap_node: 'Upscan Anleitung NDDC45E-NEEDLES',
        ctrljob_id: "ITDC-#{runkey}-#{Time.now.utc.strftime('%Y')}",
      }
      return task
    end

    #used by 'Qual Issue'
    def prepare_qualissue_task(eqpi, ch, tt, tts, tasktypeshort, params={})
      runkey = unique_id.to_s
      taskcontext = eqpi.eqp
      taskcontext = taskcontext + '.' + ch if ch != ''
      taskcontextshort = taskcontext + ch if ch != ''
      chstatus = ''
      chstatus = eqpi.chambers.select {|c| c[:chamber] == ch}[0][:status] if ch != ''
      task = {
        fs_table_id: 'G_TASKLIST',
        task_context: taskcontext,
        task_id: "#{taskcontextshort}_#{tts[:task_description]}",
        task_type: tt,
        task_type_short: tasktypeshort,
        task_sub_type: tts[:task_sub_type],
        task_description: tts[:task_description],
        task_rank_class: '8',
        task_rank_reason: '8: Blk/Hld Lots Task',
        task_run_key: runkey,
        department_list: 'LIT',
        eqp_id: eqpi.eqp,
        chamber_id: ch,
        e10_eqp_state: eqpi[:status],
        e10_chamber_state: chstatus,
        gantt_eqp_list: eqpi.eqp,
        rank_no: runkey,
        toolgroup_list: 'O-ALL',
        lot_is_pilot: 'FALSE',
        qv_qual_task_details: 'Qual opened: 22.04. 21:44 / due: 22.04. 22:20 / <b>late: 22.04. 22:20</b><BR><b>Lot List (Blocking Reasons)[Product ID]: </b><BR>TU15VM3.00 (Waiting + NoAuto3 / Waiting 31h at PD)[ME-LN-U-STP1363-CTC.01]',
        qv_severity: '2',
        qv_qual_state: 'BLOCKED',
        qv_order_description: 'KT:STP-CTC',
        ctrljob_id: "ITDC-#{runkey}-#{Time.now.utc.strftime('%Y')}",
      }
      return task
    end

    #used by 'SAP Event'
    def prepare_sapevent_task(eqpi, ch, tt, tts, tasktypeshort, params={})
      starttimeid = Time.now.utc.strftime('%m/%d/%Y %H:%M:%S')
      runkey = unique_id.to_s
      taskcontext = eqpi.eqp
      taskcontext = taskcontext + '.' + ch if ch != ''
      taskcontextshort = taskcontext + ch if ch != ''
      chstatus = ''
      chstatus = eqpi.chambers.select {|c| c[:chamber] == ch}[0][:status] if ch != ''
      task = {
        fs_table_id: 'G_TASKLIST',
        task_context: taskcontext,
        task_id: "#{taskcontextshort}-#{starttimeid}",
        task_type: tt,
        task_type_short: tasktypeshort,
        task_sub_type: tts[:task_sub_type],
        task_description: tts[:task_description],
        task_rank_class: '3',
        task_rank_reason: '3: LineCtrl Btlnk',
        task_add_info: "<b> Tool is in No Dispatch State.</b>",
        task_run_key: runkey,
        department_list: 'MSS',
        eqp_id: eqpi.eqp,
        chamber_id: ch,
        e10_eqp_state: eqpi[:status],
        e10_chamber_state: chstatus,
        claim_user_id: 'X-SAP',
        gantt_eqp_list: eqpi.eqp,
        priority_source: 'LC Bottleneck with Rank: 1',
        rank_no: runkey,
        start_time: starttimeid,
        toolgroup_list: 'O-ALL',
        ctrljob_id: "ITDC-#{runkey}-#{Time.now.utc.strftime('%Y')}",
      }
      return task
    end

    # used by 'Auto-3 Enabler'
    def prepare_auto3enabler_task(lots, eqpi, tt, tts, tasktypeshort, params={})
      runkey = unique_id.to_s
      starttime = Time.now.utc.strftime('%FT%T%:z')
      starttimeid = Time.now.utc.strftime('%m/%d/%Y %H:%M:%S')
      task = {
        fs_table_id: 'G_TASKLIST',
        task_id: "#{tts[:task_sub_type]}-#{tts[:task_description]}-#{eqpi.eqp}",
        task_type: tt,
        task_type_short: tasktypeshort,
        task_sub_type: tts[:task_sub_type],
        task_context: "#{tts[:task_description]}-#{eqpi.eqp}",
        task_description: tts[:task_description],
        task_rank_class: '5',
        task_rank_reason: "5: TimeBasedPrioTask by StartTime=#{starttimeid}",
        task_add_info: '<B>APC status: </B>',
        task_run_key: runkey,
        department_list: 'LIT',
        eqp_id: eqpi.eqp,
        gantt_eqp_list: eqpi.eqp,
        related_lot_list: lots.join(','),
        rank_no: runkey,
        start_time: starttime,
        toolgroup_list: 'O-ALL',
        a3_code_task_description: tts[:task_description],
        a3e_apc_info: 'QA Test',
        a3e_due_time: starttime,
        a3e_expected_duration: '21600',
        a3e_lot_list: lots.join(','),
        a3e_ps_apc_thread_id: tts[:task_description],
        a3e_reticle_list: 'ECLIPS20I2AYLAM1',
        data_source: 'ie_litho_a3_sched_tasklist.apf',
        ctrljob_id: "ITDC-#{runkey}-#{Time.now.utc.strftime('%Y')}",
      }
      return task
    end

    #used by 'DynamicToolCorridor'
    def prepare_dynamictoolcorridor_task(eqpi, tt, tts, tasktypeshort, params={})
      time_start = Time.now
      time_due = time_start + 1
      starttime = time_start.utc.strftime('%FT%T%:z')
      duetime = time_due.utc.strftime('%FT%T%:z')
      starttimeid = Time.now.utc.strftime('%m/%d/%Y %H:%M:%S')
      runkey = unique_id.to_s
      task = {
        fs_table_id: 'G_TASKLIST',
        task_context: eqpi.eqp,
        task_id: "#{eqpi.eqp}.#{tts[:task_description]}.#{starttime}",
        task_type: tt,
        task_type_short: tasktypeshort,
        task_sub_type: tts[:task_sub_type],
        task_description: tts[:task_description],
        task_rank_class: '5',
        task_rank_reason: "5: TimeBasedPrioTask by StartTime=#{starttimeid}",
        task_run_key: runkey,
        department_list: 'CMP',
        eqp_id: eqpi.eqp,
        e10_eqp_state: eqpi[:status],
        gantt_eqp_list: eqpi.eqp,
        rank_no: runkey,
        start_time: starttime,
        toolgroup_list: 'O-ALL',
        dtc_action: tts[:task_description],
        dtc_due_time: duetime,
        dtc_duration: '1',
        dtc_potential_wip: '1',
        dtc_start_time: starttime,
        dtc_stn_family: 'P3_AMAT-Reflexion_CuPol_POL70x',
        dtc_task_category: 'Pilot Required',
        dtc_task_reason: 'Tool blocked for processing wafers',
        dtc_tool_preference: 'Toolscore 0',
        dtc_work_instructions: 'Qualify POL402.POLC for ZA10333-1F',
        ctrljob_id: "ITDC-#{runkey}-#{Time.now.utc.strftime('%Y')}",
      }
      return task
    end

    #used by 'Fab Configuration'
    def prepare_fabconfiguration_task(tt, tts, tasktypeshort, params={})
      runkey = unique_id.to_s
      starttime = Time.now.utc.strftime('%FT%T%:z')
      starttimeid = Time.now.utc.strftime('%m/%d/%Y %H:%M:%S')
      task = {
        fs_table_id: 'G_TASKLIST',
        task_id: tts[:task_id],
        task_type: tt,
        task_type_short: tasktypeshort,
        task_sub_type: tts[:task_sub_type],
        task_context: tts[:task_context],
        task_description: tts[:task_description],
        task_rank_class: '8',
        task_rank_reason: "8: Blk/Hld Lots Task (ConspRF Rk=99 -> Inh Ratio=0 -> Lot RTD=)  by StartTime=#{starttimeid}",
        task_add_info: tts[:task_add_info],
        task_run_key: runkey,
        rank_no: runkey,
        department_list: 'MSS',
        is_AutomotiveProduct: 'FALSE',
        start_time: starttime,
        expected_duration: '600',
        ctrljob_id: "ITDC-#{runkey}-#{Time.now.utc.strftime('%Y')}",
      }
      return task
    end

    #used by 'Reticle Transport'
    def prepare_reticletransport_task(li, eqpi, reticle, reticlepod, tt, tts, tasktypeshort, params={})
      runkey = unique_id.to_s
      starttime = Time.now.utc.strftime('%FT%T%:z')
      starttimeid = Time.now.utc.strftime('%m/%d/%Y %H:%M:%S')
      task = {
        fs_table_id: 'G_TASKLIST',
        task_id: "RetTr#{reticle}#{eqpi.eqp}#{starttime}",
        task_type: tt,
        task_type_short: tasktypeshort,
        task_sub_type: tts[:task_sub_type],
        task_context: reticle,
        task_description: "[RBER] BRS1230->#{eqpi.eqp}",
        task_rank_class: '9',
        task_rank_reason: "9: Info Tasks by StartTime=#{starttimeid}",
        task_add_info: '<B>Status: </B>--<BR/><B>N_A: </B>A<BR/><B>RDI: </B>LATE  &gt; 971<BR/><B>URSA: </B>OK  &gt; 300<BR/><B>PRINT: </B>LATE  &gt; 9928',
        task_run_key: runkey,
        department_list: 'LIT',
        gantt_eqp_list: eqpi.eqp,
        related_lot_list: li.lot,
        rank_no: runkey,
        reticle_list: reticle,
        reticle_pod_list: reticlepod,
        start_time: starttime,
        toolgroup_list: 'O-ALL',
        ctrljob_id: "ITDC-#{runkey}-#{Time.now.utc.strftime('%Y')}",
      }
      return task
    end
  end

end
