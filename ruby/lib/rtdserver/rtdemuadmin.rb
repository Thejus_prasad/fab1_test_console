=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: sfrieske, 2018-03-26

Version:  1.1.0

History:
  2019-04-02 sfrieske, added QE Emu support: add_emureport, delete_emureport
=end


require 'json'
require 'socket'

Thread.abort_on_exception = false   # set to true for debugging only!


module RTD

  # RTD Emu admin interface, mainly to register remote method calls
  class EmuAdmin
    attr_accessor :dispatcher, :host, :port, :server, :thsvr

    def initialize(dispatcher, params={})
      @dispatcher = dispatcher
      @host = params[:host] || Socket.gethostname    # local hostname, for Windows
      @port = params[:adminport] || 21212
      start
      $log.info self.inspect
    end

    def inspect
      "#<#{self.class.name}  #{@host}:#{@port}>"
    end

    def start
      @server = TCPServer.new(@port)
      @thsvr = Thread.new {
        loop {
          Thread.start(@server.accept) {|sock| run(sock)}
        }
      }
    end

    # method actually serving the request
    def run(sock)
      begin
        # read msgsize
        data = sock.read(4)
        mlen = data.unpack('N').first
        # read data
        data = JSON.parse(sock.read(mlen))  # e.g. {method: 'emustations', params: []}
        $log.debug {"emuremote: #{data}"}
        keepopen = false
        dmethod, dmargs = data['method'], data['params']
        if dmethod == 'add_emustation'
          # set a specific emustation
          if dmargs.has_key?('method')  # builtin method like 'empty_reply' or nil
            params = {rule: dmargs['rule'], method: dmargs['method']}
          else                          # remote method to be called by method id
            params = {rule: dmargs['rule'], method: [sock, sock.peeraddr(false).last, dmargs['mid']]}
            keepopen = true
          end
          res = @dispatcher.add_emustation(dmargs['station'], params)
        elsif dmethod == 'delete_emustation'
          # delete an emustation
          res = @dispatcher.delete_emustation(dmargs['station'], rule: dmargs['rule'])
        elsif dmethod == 'add_emureport'
          # add an emureport
          if dmargs.has_key?('method')  # builtin method like 'file' or 'once'
            params = dmargs['method']
          else                          # remote method to be called by method id
            params = [sock, sock.peeraddr(false).last, dmargs['mid']]
            keepopen = true
          end
          res = @dispatcher.add_emureport(dmargs['report'], params)
        elsif dmethod == 'upload_emureport'
          # upload data for an emureport answer
          res = @dispatcher.upload_emureport(dmargs['report'], dmargs['data'])
        elsif dmethod == 'delete_emureport'
          # delete an emureport
          res = @dispatcher.delete_emureport(dmargs['report'])
        else
          # calls emustations, emureports or emudelay
          res = @dispatcher.send(dmethod, *dmargs)
        end
        $log.info "result: #{res}"
        reply = {result: res}.to_json
        sock.write([reply.size].pack('N'))
        sock.write(reply)
      rescue => e
        $log.warn "EmuAdmin error:\n#{$!}"
        $log.warn e.backtrace.join("\n")
        reply = {msg: 'EmuAdmin error', error: $!}.to_json
        sock.write([reply.size].pack('N'))
        sock.write(reply)
      ensure
        sock.close unless keepopen
      end
    end

  end

end
