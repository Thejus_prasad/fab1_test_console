=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.


Author:   Steffen Steidten, 2011-05-12

Version:  2.0.17

History:
  2012-10-30 ssteidte, implemented status_tool options
  2013-06-28 ssteidte, added Java User Exit test rule support
  2014-02-26 ssteidte, moved RTD server info to etc/rtdservers.conf
  2014-05-13 ssteidte, added EmptyFoup call
  2015-04-17 ssteidte, minor cleanup
  2015-06-19 ssteidte, added rtdDispLogicId as default parameter
  2015-11-30 sfrieske, added more parameters to empty_foup
  2016-01-26 sfrieske, added A3codes
  2016-12-09 sfrieske, moved A3codes to rtdtest.rb
  2019-01-28 sfrieske, improved logging for dispatch_list
  2019-04-01 sfrieske, fixed report, removed unused user exit calls
  2019-10-14 sfrieske, removed EmptyFoup call wrapper, minor cleanup
  2019-11-12 sfrieske, improved parameter handling
  2020-08-18 sfrieske, added @repository (for report)
  2021-02-05 sfrieske, use instance_of instead of kind_of
  2021-08-26 sfrieske, use read_config instead of _config_from_file
=end

require 'socket'
require 'util/readconfig'


module RTD
  DispatcherResponse = Struct.new(:headers, :rows, :rule, :process_duration)
  DispatcherStatus = Struct.new(:database, :host, :port, :status, :connected,
    :curtag, :schedfd, :respfd, :busy, :notifytag, :canceled, :why, :groups)


  # TCP Client for RTD Dispatcher
  class Client
    attr_accessor :instance, :host, :port, :socket, :timeout, :request, :response, :rthread, :repository

    def initialize(instance, overrides={})
      @instance = instance
      params = read_config(@instance, 'etc/rtdservers.conf', overrides)
      @host = params[:host] || Socket.gethostname    # local hostname, for Windows
      @port = params[:port] || 9090
      @timeout = params[:timeout] || 120
      @repository = params[:repository] || 'FastDB'
    end

    def inspect
      "#<#{self.class.name} #{@instance.inspect} (#{host}:#{@port})>"
    end

    def open
      ($log.warn "invalid host/port: #{@host}:#{@port}"; return) unless  @host && @port
      @socket.close if @socket
      @socket = TCPSocket.new(@host, @port)
    end

    def close
      @socket.close if @socket
      @socket = nil
    end

    # format the message and write it to the RTD server
    def write(msg, params={})
      msgtype = params[:msgtype] || 'CMD_Q'  # pass '' to resend an already complete msg (rtdtcpsrv.rb)
      msg_size = msg.size + msgtype.size
      mbuf = ''
      # if params[:header]
      #   # create message header and prepend it
      #   header = 'ID   rtdCorbaServer: iss_mt|true'
      #   h_size = header.size
      #   mbuf += [h_size, ~h_size].pack('NN') + header
      # end
      # add message size, checksum and the message
      mbuf += [msg_size, ~msg_size].pack('NN') + msgtype + msg
      @request = mbuf
      # return mbuf if params[:nosend]
      open
      @socket.write(mbuf)
    end

    def read(params={})
      @response = nil
      mlen = nil
      @rthread = Thread.new {
        # msgsize and checksum (not used)
        data = @socket.recvfrom(8)
        mlen = data[0].unpack('NN').first
        if mlen.nil?
          # internal uncaught server error
          $log.warn "  wrong response format"
        else
          # message, including leading CMD_A, may be sent in chunks
          @response = ''
          while @response.size < mlen
            $log.debug {"  response size: #{@response.size} of #{mlen}"}
            data = @socket.recvfrom(mlen - @response.size)
            @response += data[0]
          end
        end
      }
      @rthread.join(params[:timeout] || @timeout) || @rthread.kill
      close
      return (@response && @response.size == mlen) ? @response[5..-1] : nil # remove CMD_A header
    end

    # return dispatcher version (other status tool alike args: describe, diags, dlisqueue)
    def version
      write('status|version')
      read(timeout: 10)
    end

    # return dispatchers' status
    def admin_dispatchers(params={})
      write('admin|dispatchers')
      res = read(timeout: 10) || return
      ret = []
      res.split("\n")[1..-2].collect {|line|
        ff = line.split
        ff.each_index {|i|
          ff[i] = case ff[i]
          when '---'; nil
          when 'Yes'; true
          when 'No';  false
          else; ff[i]
          end
        }
        r = DispatcherStatus.new(*ff)
        ret << r if r.status == 'Enabled' || params[:all]
      }
      return ret.sort {|a, b| a.port <=> b.port}
    end

    # generic dlis command, return APF dispatch_list headers and rows or nil on error
    # station: dispatch station (entity), carrier, lot, or QT_LOTS_WITH_QT, ...)
    # report (rule, SiView: function): e.g. WhatNextLotList, WhereNextInterbay, WhereNextEquipment, ..
    # example: dispatch_list('x', report: "QA_EMPTY", category: "DISPATCHER/QA",
    #                             parameters: "FOUPCount|9|Protokoll|FEOL|SrtID|UTS001")
    def dispatch_list(station, params={})
      # optional report and category
      report = params[:report]
      category = params[:category]
      # $log.info "dispatch_list #{station.inspect}, #{params.select {|p| [:report, :category, :parameters].include?(p)}}"
      # build request message
      msg = "dlis|Station|#{station || report}"  # JCAP case, station same as report
      msg += "|Report|#{report}" if report
      msg += "|Category|#{category}" if category
      # additional parameters, either as array or as string like "requestingEntity|Pull"
      if parameters = params[:parameters]
        if parameters.instance_of?(String)
          msg += '|' unless parameters.start_with?('|')
          msg += parameters
        else  # must be an array
          msg += '|' + parameters.join('|')
        end
      end
      $log.info "msg: #{msg.inspect}"
      write(msg)
      res = read(timeout: params[:timeout]) || ($log.warn '  no response'; return)
      # parse response
      res = res.split("\n")
      ($log.warn '  empty response'; return) if res.empty?  # covers "" and "\n"
      return DispatcherResponse.new([res.first]) if res.first.start_with?('#error')
      # get column names
      colline = res.find {|l| l.start_with?('#columns')}
      ($log.warn "  malformed response, no #columns:\n#{res}"; return) unless colline
      columns = colline.strip.split(' ', 2)[1][1...-1].split("' '")
      # get result rows
      rule = nil
      pduration = nil
      rows = res[6..-1].collect {|t|
        pduration = t.split.last.delete("'").to_f if t.start_with?('#process_duration')
        rule = t.split.last.delete("'") if t.start_with?('#rule')
        next if t.start_with?('#') || t.empty?
        # split at ' ', remove leading and trailing "'"
        row = t.strip.split("' '")
        row.first.delete!("'")
        row.last.delete!("'")
        row
      }.compact
      return DispatcherResponse.new(columns, rows, rule, pduration)
    end

    # WhereNext dispatch_list call issued by Vamos AutoProc AutoPush, returns DispatcherResponse (UNUSED)
    def vap_wherenexta(carrier, params={})
      parameters = ['rtdDispLogicId', 'WhereNextEquipment', 'requestingEntity', 'AutoPush', 'Carrier', carrier]
      return dispatch_list('VAMOS_WN_A', parameters: parameters)
    end

    # WhereNext dispatch_list call issued by Vamos AutoProc ManualPush, returns DispatcherResponse (UNUSED)
    def vap_wherenext(carrier, params={})
      parameters = ['rtdDispLogicId', 'WhereNextEquipment', 'requestingEntity', 'Push', 'Carrier', carrier]
      return dispatch_list('VAMOS_WN', parameters: parameters)
    end

    # WhatNextLotList dispatch_list call, returns DispatcherResponse
    def what_next(station, params={})
      parameters = ['rtdDispLogicId', 'WhatNextLotList']
      parameters += params[:parameters] if params[:parameters]
      return dispatch_list(station, report: params[:report], category: params[:category], parameters: parameters)
    end

    # Query Executor report call, return raw response
    # ex: report('REPORTS/LINUX/MODULES/IE/AM_REPORTS', 'xxx.report')
    #     report('Station=@all|-url', '/apf/apf_itdc_v93/.../ie_TASKLIST_all.report')
    def report(category, report, params={})
      msg = "run|#{params[:model] || 'fab1'}|#{params[:repository] || @repository}"
      msg += "|#{category || 'REPORTS/LINUX/MODULES/IE/AM_REPORTS'}|#{report}"
      if parameters = params[:parameters]
        if parameters.instance_of?(String)
          msg += '|' unless parameters.start_with?('|')
          msg += parameters
        else  # must be an array
          msg += '|' + parameters.join('|')
        end
      end
      $log.info "msg: #{msg.inspect}"
      write(msg)
      res = read(timeout: params[:timeout]) || ($log.warn '  no response'; return)
      return res
    end

  end

end
