=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: Steffen Frieske, GLOBALFOUNDRIES, INC., 2019

Version:  1.0.1

History:
  2021-02-05 sfrieske, use instance_of instead of kind_of
  2021-08-26 sfrieske, use read_config instead of _config_from_file
=end

load './ruby/set_paths.rb'
require 'fileutils'
require 'json'
require 'optparse'
require 'socket'
require 'util/readconfig'
require_relative 'qetcpsvr'
require_relative 'rtdemuadmin'

Thread.abort_on_exception = false   # set to true for debugging only!


module RTD

  # APF QueryExecutor Proxy with TCP (remote status_tool) Interface
  class QueryExecutor
    attr_accessor :_params, :rmtimeout, :emureports, :emudir, :admin, :qeinterface

    def initialize(env, params={})
      @_params = read_config("#{env}_qe.emu", 'etc/rtdservers.conf', params)
      @rmtimeout = 24 * 3600  # remove remote methods after a day
      @emureports = {}  # reports to be served (emulated), see add_emureport
      @emudir = @_params[:emudir] || "#{File.dirname(Dir.pwd)}/qeanswers"
      FileUtils.mkdir_p(@emudir)
      #
      @admin = EmuAdmin.new(self, host: '', adminport: @_params[:adminport])
      # start the QE TCP interface
      @qeinterface = QEInterface.new(env, self, port: @_params[:port], maxconn: @_params[:maxconn], nclients: @_params[:nclients])
      $log.info self.inspect
    end

    def inspect
      "#<#{self.class.name}  #{@qeinterface.inspect}>"
    end

    def join
      @qeinterface.thsvr.join
    end

    # add an entry to @emureports for a specific report
    def add_emureport(report, method)
      $log.info "add_emureport #{report.inspect}, #{method.inspect}"
      if method.instance_of?(Array)  # remote emu
        method << Thread.new {sleep @rmtimeout; delete_emureport(report)}
      end
      @emureports[report] = method  # method is an array for remote, 'file' or 'once'
      return @emureports
    end

    # upload data for a 'file' or 'once' answer
    def upload_emureport(report, data)
      $log.info "upload_emureport #{report.inspect}, (data: #{data[0..32]} ..)"
      fpath = "#{@emudir}/#{report}.answer"
      File.binwrite(fpath, data)
    end

    # remove an entry from @emureports
    def delete_emureport(report, params={})
      $log.info "delete_emureport #{report.inspect}, #{params}" unless params[:silent]
      method = @emureports.delete(report)
      if method.instance_of?(Array)
        begin
          sock, remip, mid, th = method
          th.kill
          sock.close
        rescue
          $log.warn {"error removing remote method, rescued:\n#{$!}"}
        end
      else
        fpath = "#{@emudir}/#{report}.answer"
        if File.readable?(fpath)
          $log.info "removing answer file #{fpath}"
          File.delete(fpath)
        end
      end
      return @emureports
    end

    # dispatch the request to an emulated method or real Query Executor based on the report
    #
    # call the emu method if emulated and return its response, else nil
    def dispatch(report)
      method = @emureports[report] || return  # the caller of dispatch() will call the real Query Executor
      #
      # call the identified QE emulator's report or a remote server serving the report
      begin
        if method.instance_of?(Array)
          # remote method: [socket, method id]
          $log.info "calling emuremote #{method} (#{report})}"
          sock, remip, mid, th = method
          data = {station: station, report: report, mid: mid}.to_json
          sock.write([data.size].pack('N'))
          sock.write(data)
          rdata = sock.read(4)
          rlen = rdata.unpack('N').first
          res = sock.read(rlen)
          $log.info "completed emuremote #{method}"
          return JSON.parse(res)
        else
          # serving answer file
          fname = "#{report}.answer"
          fpath = File.join(@emudir, fname)
          if File.readable?(fpath)
            $log.info "reading emulator answer from file #{fname}"
            reply = File.binread(fpath)
            delete_emureport(report) if method == 'once'
            return reply
          else
            return "#error: 'QEEmu error reading answer file #{fname}'"
          end
        end
      rescue => e
        $log.error $!
        $log.warn e.backtrace.join("\n")
        return "#error: 'QEEmu executing #{report} failed'"
      end
    end
  end

end


def main(argv)
  # set defaults
  env = nil
  port = 22250
  verbose = false
  noconsole = true
  # parse cmdline options
  optparser = OptionParser.new {|o|
    o.banner = "Usage: #{File.basename(__FILE__)} -e <env> [options]"
    o.on('-e', '--env [ENV]', 'environment, e.g. "let"') {|s| env = s}
    o.on('-p', '--port [PORT]', Integer, "listen port, default is #{port}") {|s| port = s}
    o.on('-v', '--verbose', 'verbose logging') {verbose = true}
    o.on('--console', 'log to console in addition to log file') {noconsole = false}
    o.on_tail('-h', '--help', 'diplay this screen') {puts o; return}
  }
  optparser.parse(*argv)
  (STDOUT.puts "missing parameter -e <env>"; return) unless env
  logfile = "log/qeserver_#{env}.log"
  create_logger(file: logfile, file_verbose: verbose, verbose: verbose, noconsole: noconsole)
  STDOUT.puts "logging to #{logfile}, console log: #{!noconsole}"
  svr = RTD::QueryExecutor.new(env, port: port)
  $log.info "#{svr.inspect} started"
  svr.join
end

main(ARGV) if __FILE__ == $0
