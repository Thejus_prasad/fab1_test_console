=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: Steffen Steidten, 2014-03-03

History:
  2014-09-08 ssteidte, cleanup
  2015-02-02 dsteger,  merged with Fab1QA
  2016-05-19 sfrieske, fixed WhereNextVapTarget
  2016-06-10 sfrieske, allow what_next filter to filter by requesting entity; allow to pass MM instance instead of env
  2016-11-17 sfrieske, separated RTD::Dispatcher, RTD::TCPInterface and RTD::ServerManager
  2017-01-03 sfrieske, new RTD::EmuRemote without DRb, removed unused test_a_aws
  2017-01-09 sfrieske, remove unused StaticReply
  2020-08-26 sfrieske, use raw eqp_info
  2021-02-05 sfrieske, use instance_of instead of kind_of
  2021-07-12 sfrieske, added 2 s default delay (for AutoProc)
=end

require_relative 'rtdemu'


# Use the RTD Emulator for special tests, e.g. AutoProc with special conditions
module RTD
  # test methods must take station, rule, params as arguments and
  #   return a Hash with 'rows', 'headers', 'widths' and 'types':
  #   proc {|*args| {'headers'=>[..], 'widths'=>[..], 'types'=>[..], 'rows'=>data}}
  #

  class WhatNextFilter
    attr_accessor :lots, :procmon

    def initialize(env, params={})
      @sv = env.instance_of?(String) ? SiView::MM.new(env) : env
      @delay = params[:delay] || 2 # at least 1 s for AutoProc
      @lots = params[:lots]
      @lots = [@lots] if @lots.instance_of?(String)
      @procmon = params[:procmon]
      @e2echeck = params[:e2echeck] != false  # return EI carriers only if eqp has e2e enabled and port is UnloadReq
      @entity = params[:entity]
      $log.info self.inspect
    end

    def inspect
      "#<#{self.class.name} lots: #{@lots.inspect}#{', procmon: ' + @procmon.inspect if @procmon}>"
    end

    def call(station, rule, params)
      $log.info "#{self.class}.call #{station.inspect}, #{rule.inspect}, #{params}"
      sleep @delay
      res = @sv.what_next(station, criteria: 'SAVL', raw: true)
      return Emulator.empty_reply(station, rule, params) unless res
      colkeys = Emulator::ColumnKeys['AutoProc'].clone
      colkeys << 'monitor' if @procmon
      seq = 0
      lotlines = {}
      procmonline = nil
      res.strWhatNextAttributes.each {|attrs|
        lot = attrs.lotID.identifier
        if @lots.member?(lot) && (@entity.nil? || params[:requestingEntity] == @entity)
          if @e2echeck && attrs.transferStatus == 'EI'
            eqpi = @sv.eqp_info_raw(attrs.equipmentID)
            brinfo = eqpi.respond_to?(:equipmentInternalBufferInfo) ? eqpi.equipmentBRInfoForInternalBuffer : eqpi.equipmentBRInfo
            next unless brinfo.eqpToEqpTransferFlag
            next unless port = eqpi.equipmentPortInfo.strEqpPortStatus.find {|p|
              p.loadedCassetteID.identifier == attrs.cassetteID.identifier
            }
            next unless port.portState == 'UnloadReq'
          end
          seq += 1
          custom = {'seq'=>seq, 'runSize'=>res.processRunSizeMaximum, 'monitor'=>@procmon}
          if res.equipmentCategory == 'Wafer Sorter'
            custom['TargetPort'] = 'P1'
            colkeys << 'TargetPort'
          end
          lotlines[lot] = Emulator.what_next_row(colkeys, attrs, custom)
        elsif @procmon == lot
          seq += 1
          custom = {'seq'=>seq, 'runSize'=>res.processRunSizeMaximum, 'monitor'=>@procmon}
          procmonline = Emulator.what_next_row(colkeys, attrs, custom)
        end
      }
      # response is ordered by lots' order
      rows = []
      lots.each {|lot|
        lotline = lotlines[lot]
        rows << lotline if lotline
      }
      rows << procmonline if procmonline
      $log.info(">>\n#{rows.inspect}") unless rows.empty?
      return Emulator.get_headers(colkeys).merge('rows'=>rows)
    end

  end


  class WhatNextVapTarget
    attr_accessor :lot, :eqp, :stocker, :port
    # target port for Pull scenarios, target eqp or stocker for Push scenarios
    # special case: TargetEqp 'NoCandidate', TargetStocker given -> no cj, stock carrier in

    def initialize(env, lot, params={})
      @sv = env.instance_of?(String) ? SiView::MM.new(env) : env
      @delay = params[:delay] || 2 # at least 1 s for AutoProc
      @lot = lot
      @eqp = params[:eqp] || ''
      @stocker = params[:stocker] || ''
      @port = params[:port] || ''
      $log.info self.inspect
    end

    def inspect
      "#<#{self.class.name} lot: #{@lot.inspect}, eqp: #{@eqp.inspect}, stocker: #{@stocker.inspect}, port: #{@port.inspect}>"
    end

    def call(station, rule, params)
      $log.info "#{self.class}.call #{station.inspect}, #{rule.inspect}, #{params}"
      sleep @delay
      # ensure Pull scenarios don't interfere with the TargetStocker/TargetEQP Push scenario
      if (!@stocker.empty? || !@eqp.empty?) && (params['requestingEntity'] == 'Pull')
        return Emulator.get_headers(Emulator::ColumnKeys['AutoProc']).merge('config'=>self.class.name, 'rows'=>[])
      end
      # Pull with target port or Push with TargetEQP
      colkeys = Emulator::ColumnKeys['AutoProc'] + ['TargetEQP', 'TargetStocker', 'TargetPort']
      rows = []
      res = @sv.what_next(station, criteria: 'SAVL', raw: true)
      res.strWhatNextAttributes.each {|attrs|
        if attrs.lotID.identifier == @lot
          custom = {'seq'=>1, 'TargetEQP'=>@eqp, 'TargetStocker'=>@stocker, 'TargetPort'=>@port}
          rows << Emulator.what_next_row(colkeys, attrs, custom)
          break
        end
      }
      $log.info(">>\n#{rows.inspect}") unless rows.empty?
      return Emulator.get_headers(colkeys).merge('config'=>self.class.name, 'rows'=>rows)
    end

  end


  class WhereNextVapTarget
    attr_accessor :carrier, :eqp, :stocker

    def initialize(env, carrier, params={})
      @sv = env.instance_of?(String) ? SiView::MM.new(env) : env
      @delay = params[:delay] || 2 # at least 1 s for AutoProc
      @carrier = carrier
      @eqp = params[:eqp] || ''
      @stocker = params[:stocker] || ''
      $log.info self.inspect
    end

    def inspect
      "#<#{self.class.name} carrier: #{@carrier.inspect}, eqp: #{@eqp.inspect}, stocker: #{@stocker.inspect}>"
    end

    def call(station, rule, params)
      $log.info "#{self.class}.call #{station.inspect}, #{rule.inspect}, #{params}"
      sleep @delay
      colkeys = ['carrier', 'eqp', 'stationID', 'lot']
      # ensure Pull scenarios don't interfere with the TargetStocker/TargetEQP Push scenario
      if params['requestingEntity'].include?('Push') && params['Carrier'] == @carrier
        rows = @sv.lot_list_incassette(@carrier).collect {|lot| [@carrier, @eqp, @stocker, lot]}
      else
        rows = []
      end
      $log.info(">>\n#{rows.inspect}") unless rows.empty?
      return Emulator.get_headers(colkeys).merge('config'=>self.class.name, 'rows'=>rows)
    end

  end


  class WhatNextVapRank
    attr_accessor :lot, :lrank, :grank

    def initialize(env, lot, params={})
      @sv = env.instance_of?(String) ? SiView::MM.new(env) : env
      @delay = params[:delay] || 2 # at least 1 s for AutoProc
      @lot = lot
      @lrank = params[:lrank]
      @grank = params[:grank]
      $log.info self.inspect
    end

    def inspect
      "#<#{self.class.name} lot: #{@lot.inspect}, lrank: #{@lrank.inspect}, grank: #{@grank.inspect}>"
    end

    def call(station, rule, params)
      $log.info "#{self.class}.call #{station.inspect}, #{rule.inspect}, #{params}"
      sleep @delay
      ##colkeys = ['carrier', 'eqp', 'stationID', 'lot']
      colkeys = Emulator::ColumnKeys['AutoProc'] + ['lrank', 'grank']
      rows = []
      res = @sv.what_next(station, criteria: 'SAVL', raw: true)
      res.strWhatNextAttributes.each {|attrs|
        if attrs.lotID.identifier == @lot
          custom = {'seq'=>1, 'lrank'=>@lrank, 'grank'=>@grank}
          rows << Emulator.what_next_row(colkeys, attrs, custom)
          break
        end
      }
      $log.info(">>\n#{rows.inspect}") unless rows.empty?
      return Emulator.get_headers(colkeys).merge('config'=>self.class.name, 'rows'=>rows)
    end

  end

end
