=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author:  Steffen Steidten, AMD based on the code by Rick Hall, AMD

Version: 4.2.1

History:
  2008-10-16 ssteidte, created as Jython script
  2009-07-21 ssteidte, ported to JRuby
  2010-02-09 ssteidte, use EasySiViewR object for SiView connections
  2010-05-12 ssteidte, whereNextInterBay returns eqp stockers
  2010-11-11 ssteidte, added test_MMServerCrash_veryLongReply method
  2011-04-05 ssteidte, added test_a_aws method
  2013-09-11 dsteger,  fixed some rule errors
  2013-10-22 dsteger,  added reticle pod rules
  2013-12-18 ssteidte, use new SiView::MM class instead of EasySiViewR
  2014-02-25 ssteidte, cleaned up unused filters, wrapped class into module RTD
  2014-05-20 ssteidte, added EmptyFOUP rule, fixed WhereNextEquipment for E2E
  2017-01-06 sfrieske, return data in new Hash format, remove unused method SlmRetrievingCassette
  2017-11-03 sfrieske, fixed WhatNext Empty FOUP response headers, removed dependency on YAML
  2020-08-26 sfrieske, use raw eqp_info
  2021-07-12 sfrieske, fixed use of eqp_info_raw in WhereNextEquipment for E2E
=end

require 'siview'


module RTD

  # RTD Emulator, or a description of the functions see DIS.5 in the IBM SiView documentation.
  class Emulator
    # column definitions used in RTD replies (mainly WhatNext)
    Column = Struct.new(:header, :width, :datatype)

    ColumnDefs = {
      'seq' => Column.new('Seq', 3, 'INTEGER'),
      'externalPriority' => Column.new('Priority', 5, 'STRING'),
      'lot' => Column.new('Lot ID', 12, 'STRING'),
      'carrier' => Column.new('Carrier ID', 12, 'STRING'),
      'waferCount' => Column.new('Qty', 3, 'INTEGER'),
      'opname' => Column.new('Oper Name', 12, 'STRING'),
      'PDID' => Column.new('PD ID', 12, 'STRING'),
      'opno' => Column.new('Oper No', 6, 'STRING'),
      'lotStatus' => Column.new('Lot Processing State', 12, 'STRING'),
      'product' => Column.new('Product ID', 22, 'STRING'),
      'procmon' => Column.new('Proc Mon Prod', 22, 'STRING'),
      'monitor' => Column.new('Monitor', 22, 'STRING'),
      'lrecipe' => Column.new('Logical Recipe ID', 61, 'STRING'),
      'recipe' => Column.new('Machine Recipe ID', 61, 'STRING'),
      'mltype' => Column.new('Multi Lot Type', 3, 'STRING'),
      'stationID' => Column.new('Station ID', 12, 'STRING'),
      'hold' => Column.new('Hold State', 6, 'STRING'),
      'route' => Column.new('Route ID', 61, 'STRING'),
      'cassEqpID' => Column.new('Carrier Eqp ID', 12, 'STRING'),
      'stocker' => Column.new('Carrier Station ID', 12, 'STRING'),
      'xfer_status' => Column.new('Carrier Xfer Status', 12, 'STRING'),
      'xfer' => Column.new('Xfer', 12, 'STRING'),
      'precipe' => Column.new('Physical Recipe ID', 61, 'STRING'),
      'reticlegroup' => Column.new('Reticle Group ID', 8, 'STRING'),
      'eqp' => Column.new('Equipment ID', 12, 'STRING'),
      'runSize' => Column.new('Process RunSize Maximum', 1, 'INTEGER'),
      'age' => Column.new('Age', 8, 'STRING'),
      'type' => Column.new('Type', 6, 'STRING'),
      'slt' => Column.new('Sub Lot Type', 12, 'STRING'),
      'portID' => Column.new('Port ID', 12, 'STRING'),
      # A3
      'A3' => Column.new('A3', 1, 'STRING'),
      'A3Info' => Column.new('A3Info', 2, 'STRING'),
      'TargetEQP' => Column.new('TargetEQP', 12, 'STRING'),
      'TargetStocker' => Column.new('TargetStocker', 12, 'STRING'),
      'TargetPort' => Column.new('TargetPort', 12, 'STRING'),
      'planEqp' => Column.new('PlanEqp', 12, 'STRING'),
      'reservation' => Column.new('Reservation', 12, 'STRING'),
      'module' => Column.new('Module', 12, 'STRING'),
      'lrank' => Column.new('LRANK', 12, 'STRING'),
      'grank' => Column.new('GRANK', 12, 'STRING'),
      'emptyCarrier' => Column.new('EmptyCarrier', 12, 'STRING'),
      # Empty FOUP rule
      'ecarrier' => Column.new('Carrier ID', 12, 'STRING'),
      'ecategory' => Column.new('cast_category', 12, 'STRING'),
      'estocker' => Column.new('Stocker ID', 12, 'STRING'),
      # reticle
      'priority' => Column.new('Priority', 12, 'INTEGER'),
      'reticleID' => Column.new('Reticle ID', 12, 'STRING'),
      'podID' => Column.new('Pod ID', 12, 'STRING'),
      'reticlePodID' => Column.new('Reticle Pod ID', 12, 'STRING'),
      'toEqpID' => Column.new('To EquipmentID', 12, 'STRING'),
      'eqptype' => Column.new('Equipment Type', 12, 'STRING'),
    }

    # WhatNext column key selections
    ColumnKeys = {
      'Default'=>['seq', 'externalPriority', 'lot', 'carrier', 'waferCount', 'opname', 'opno', 'lotStatus',
        'product', 'procmon', 'lrecipe', 'recipe', 'stationID', 'mltype', 'hold', 'route',
        'cassEqpID', 'stocker', 'xfer_status', 'precipe', 'reticlegroup', 'eqp', 'runSize', 'age', 'type'
      ],
      'AutoProc'=>['seq', 'externalPriority', 'lot', 'carrier', 'A3', 'planEqp', 'reservation', 'module', 'xfer',
        'waferCount', 'PDID', 'opname', 'lrecipe', 'opno', 'lotStatus', 'product', 'recipe', 'mltype', 'stationID', 'hold',
        'route', 'cassEqpID', 'stocker', 'xfer_status', 'precipe', 'reticlegroup', 'eqp', 'runSize', 'age', 'slt', 'A3Info'
      ]
    }

    # get the return data {'headers'=>[..], 'widths'=>[..], 'types'=>[..]} for a list of keys from ColumnDefs
    def self.get_headers(colkeys, coldefs=nil)
      coldefs ||= ColumnDefs
      headers = []
      widths = []
      types = []
      colkeys.each {|k|
        column = coldefs[k]
        $log.warn {"undefined response column: #{k.inspect}"} unless column
        headers << column.header
        widths << column.width
        types << column.datatype
      }
      return {'headers'=>headers, 'widths'=>widths, 'types'=>types}
    end

    # return an empty reply with WhatNext headers (used in Emu and EmuExt), station and rule are not used
    def self.empty_reply(station, rule, params={})
      return Emulator.get_headers(ColumnKeys['Default']).merge('config'=>'RTDEmulator', 'rows'=>[])
    end

    # return row data from WhatNext raw result and params to override or fill in missing data
    def self.what_next_row(colkeys, attrs, params={})
      return colkeys.collect {|k|
        params[k] ||
        case k
        when 'externalPriority';  attrs.externalPriority;
        when 'lot';               attrs.lotID.identifier;
        when 'carrier';           attrs.cassetteID.identifier;
        when 'waferCount';        attrs.totalWaferCount;
        when 'opname';            attrs.operationID.identifier;
        when 'PDID';              attrs.operationID.identifier;
        when 'opno';              attrs.operationNumber;
        when 'product';           attrs.productID.identifier;
        when 'procmon';           attrs.processMonitorProductID.identifier;
        when 'lrecipe';           attrs.logicalRecipeID.identifier;
        when 'recipe';            attrs.machineRecipeID.identifier;
        when 'mltype';            attrs.multiLotType;
        when 'stationID';         attrs.stockerID.identifier;
        when 'hold';              attrs.processHoldFlag ? 'HOLD' : 'NOTONHOLD';
        when 'route';             attrs.routeID.identifier;
        when 'cassEqpID';         attrs.equipmentID.identifier;
        when 'stocker';           attrs.stockerID.identifier;
        when 'xfer_status';       attrs.transferStatus;
        when 'xfer';              attrs.transferStatus;
        when 'precipe';           attrs.physicalRecipeID;
        when 'reticlegroup';      attrs.reticleExistFlag ? attrs.reticleGroupIDs.first.identifier : '-';
        when 'eqp';               attrs.equipmentID.identifier;
        when 'type';              attrs.lotType;
        when 'runSize';           params['runSize'] || 1;
        when 'A3';                params['A3'] || 'Y';
        else
          ColumnDefs[k].datatype == 'STRING' ? '' : 0
        end
      }
    end


    attr_accessor :sv

    def initialize(env, params={})
      @sv = params[:sv] || SiView::MM.new(env)
      @stockout_port = params[:stockout_port] || 'P4'   # for WhereNextEquipment VAMOS_STK
      @fallback_brs = params[:brs] || 'UTBRS110'        # for WhereNextPod
      @fallback_pod_stocker = params[:rp_stocker] || 'UTRPSTO110'  # for WhereNextPod
      $log.info self.inspect
    end

    def inspect
      "#<#{self.class.name} #{@sv.inspect}>"
    end

    # return an empty reply, needs to appear as emu instance method for dispatch()
    def empty_reply(station, rule, params)
      return Emulator.empty_reply(station, rule, params)
    end

    # rule implementations, return Hash with the keys rows, headers, widths, types

    # rule: WhatNextLotList, station: equipment name or a special name like VAMOS_NOWIP
    # for AutoProc send params {'requestingEntity'=>'AutoPull'}
    def WhatNextLotList(station, rule, params)
      if station == 'VAMOS_NOWIP'
        return Emulator.empty_reply(station, rule, params)
      elsif station == 'EMPTY'
        # Empty FOUP rule, called e.g. by SJC
        rows = []
        sorter = params['EqpID'] || params['SrtID']
        category = params['Protokoll']
        n = (params['FOUPCount'] || 1).to_i
        i = 0
        # @sv.eqp_info(sorter).stockers.each {|stk|
        @sv.eqp_info_raw(eqp).equipmentStockerInfo.strEqpStockerStatus.each {|stk|
          carriers = @sv.carrier_list(category:  category, status: 'AVAILABLE',
            sjcheck: true, empty: true, stocker: stk.stockerID.identifier)
          carriers.each {|c|
            i += 1
            break if i > n
            rows << [c, category, stk]
          }
          break if i > n
        }
        return Emulator.get_headers(['ecarrier', 'ecategory', 'estocker']).merge('config'=>'EMPTY', 'rows'=>rows)
      else
        rows = []
        moveLots = 0
        moveWafers = 0
        holdLots = 0
        holdWafers = 0
        blockLots = 0
        blockWafers = 0
        rentity = params['requestingEntity']
        is_autoproc = rentity && (rentity.include?('Pull') || rentity.include?('Push'))  # push also calls WhatNext
        # column set to be used
        colcfg = is_autoproc ? 'AutoProc' : 'Default'
        colkeys = ColumnKeys[colcfg]
        $log.debug {"using column config #{colcfg}"}
        #
        seq = 0
        # call TxWhatNextLotListInq
        res = @sv.what_next(station, criteria: 'SAVL', raw: true) # SALL, SAVL, Auto3, SHLD
        ($log.error 'error in TxWhatNextLotListInq'; return Emulator.empty_reply(station, rule, params)) unless res
        res.strWhatNextAttributes.each {|attrs|
          # skip lots w/o carriers
          next if attrs.cassetteID.identifier.empty?
          # xfer status filter
          next if is_autoproc && ['AO', 'BO', 'EO', 'HO'].member?(attrs.transferStatus)
          # calculate counts of moveable and held lots
          if attrs.processHoldFlag
            holdLots += 1
            holdWafers += attrs.totalWaferCount
          else
            moveLots += 1
            moveWafers += attrs.totalWaferCount
          end
          # add a row
          seq += 1
          rows << Emulator.what_next_row(colkeys, attrs, 'seq'=>seq, 'runSize'=>res.processRunSizeMaximum)
          break if is_autoproc && seq == 20
        }
        headerdata = [station, moveLots, moveWafers, holdLots, holdWafers, blockLots, blockWafers]
        return Emulator.get_headers(ColumnKeys[colcfg]).merge('config'=>colcfg, 'rows'=>rows, 'headerdata'=>headerdata)
      end
    end

    # rule: WhereNextInterBay, station: carrier
    #
    # MRM: called when a carrier is placed into a Stocker Manual Input Port and the carrier ID in MTSC arrives at the host
    #
    # WhereNextInterBay returns the stockers associated with the equipment from which the carrier
    # is unloaded as configured in SM. If the carrier is in a stocker, nothing is returned.
    #
    # returns rows with stockers from Transport Equipment field in SM or nothing in case of errors
    def WhereNextInterBay(station, rule, params)
      colkeys = ['carrier', 'eqp', 'stationID', 'lot']
      rows = []
      if params.has_key?('empty')  # use case ??
        $log.warn 'empty reply requested'
      else
        cs = @sv.carrier_status(station)
        if cs
          eqpi = @sv.eqp_info_raw(cs.eqp)
          if eqpi
            # eqpi.stockers.each {|stk| rows << [station, '', stk, '']}
            eqpi.equipmentStockerInfo.strEqpStockerStatus.each {|stk|
              rows << [station, '', stk.stockerID.identifier, '']
            }
          else
            $log.warn "error in eqp_info for '#{cs.eqp}'"
          end
        else
          $log.warn "error in carrier_status for #{station.inspect}"
        end
      end
      return Emulator.get_headers(colkeys).merge('rows'=>rows)
    end

    # called by AutoProc in Push and Pull scenarios, returns destination equipment or stocker
    def WhereNextEquipment(station, rule, params)
      rows = []
      if params['requestingEntity'].include?('Push')   # station is VAMOS_WN  or VAMOS_WN_A
        colkeys = ['carrier', 'eqp', 'stationID', 'lot']
        carrier = params['Carrier']
        if carrier
          lis = @sv.lot_info(@sv.lot_list_incassette(carrier))
          li = lis.sort {|a, b| a.priority <=> b.priority}.first # take the lot with higest priority
          if li
            tgteqp = ''
            # set tgteqp to nil if E2E from current eqp is possible; to be overwritten below
            # if (li.xfer_status == 'EI') && !li.eqp.empty? && @sv.eqp_info_raw(li.eqp).equipmentBRInfo.eqpToEqpTransferFlag
            if (li.xfer_status == 'EI') && !li.eqp.empty?
              eqpi = @sv.eqp_info_raw(li.eqp)
              e2e = eqpi.respond_to?(:equipmentBRInfo) ?
                eqpi.equipmentBRInfo.eqpToEqpTransferFlag :
                eqpi.equipmentBRInfoForInternalBuffer.eqpToEqpTransferFlag
              tgteqp = nil if e2e
            end
            li.opEqps.each {|eqp|
              # eqpi = @sv.eqp_info(eqp)
              # stocker = (eqpi && eqpi.stockers.first) || ''
              eqpi = @sv.eqp_info_raw(eqp)
              stocker = eqpi && eqpi.equipmentStockerInfo.strEqpStockerStatus.first.stockerID.identifier || ''
              tgteqp ||= eqp  # only if e2e possible (else '')
              rows << [carrier, tgteqp, stocker, li.lot]
            }
          else
            $log.warn "  empty carrier #{carrier.inspect}"
          end
        else
          $log.warn '  error: no carrier given'
        end
      elsif station == 'VAMOS_STK'
        # nearest station rule in manual Pull scenarios (Auto-1 or Semi-Start-1 with empty port)
        colkeys = ['stationID', 'portID']
        if params['Carrier']
          carrier = params['Carrier']
          eqp = params['Equipment']
          if eqp
            # eqpi = @sv.eqp_info(eqp)
            eqpi = @sv.eqp_info_raw(eqp)
            rows << [eqpi.equipmentStockerInfo.strEqpStockerStatus.first.stockerID.identifier, @stockout_port]
          else
            $log.warn '  error message: equipment not found.'
          end
        else
          $log.warn '  error message: carrier not found.'
        end
      else
        $log.warn '  error message: only AutoPush supported.'
      end
      return Emulator.get_headers(colkeys).merge('rows'=>rows)
    end

    # Called by TxReticleActionListInq (ARDD)
    def WhatNextPod(station, rule, params)
      colkeys = ['priority', 'reticleID', 'reticlePodID', 'toEqpID']
      what, eqp = station.split('_')
      rows = []
      res = @sv.what_next(eqp, criteria: 'SAVL')
      if res.count > 0
        i = 1
        res.each do |r|
          lot = r.lot
          # take first lot on list and query for reticle / pod
          @sv.reticle_list(lot: lot, status: 'AVAILABLE').each {|rinfo|
            rows << [i, rinfo.reticleID.identifier, rinfo.reticleStatusInfo.reticlePodID.identifier, eqp]
            i += 1
          }
        end
      else
        $log.warn '  error: WhatNext failed.'
      end
      return Emulator.get_headers(colkeys).merge('rows'=>rows)
    end

    # Called by TxWhereNextRecipePod (ARDD), broken?
    def WhereNextPod(station, rule, params)
      colkeys = ['podID', 'eqp', 'portID', 'eqptype']
      rows = []
      if params['Pod']
        pod = params['Pod']
        pstatus = @sv.reticle_pod_status(pod).reticlePodStatusInfo
        if pstatus.stockerID.identifier != ''
          rows << [pod, pstatus.stockerID.identifier, '', 'ReticlePod']
        elsif pstatus.equipmentID.identifier != ''
          if pstatus.transferStatus == 'EI'
            eqp = pstatus.equipmentID.identifier
            # port = @sv.eqp_info(eqp).reticleports.find {|rp| rp.loaded_pod == pod}
            port = @@sv.eqp_info_raw(eqp).strEquipmentAdditionalReticleAttribute.strReticlePodPortIDs.find {|e|
              e.loadedReticlePodID.identifier == pod
            }
            if pstatus.empty?
              # Stay on equipment port, TODO: ??? maybe port.nil? ?? (sf, 2020-08-14)
              rows << [pod, eqp, port.reticlePodPortID.identifier, 'Equipment']
            else
              # Move to BRS
              rows << [pod, @fallback_brs, 'RP1', 'BareReticle']
            end
          else #pstatus.xfer_status == "SI"
            # Bare reticle stocker -> move to reticle pod stocker
            rows << [pod, @fallback_pod_stocker, '', 'ReticlePod']
          end
        else
          # Move to fallback reticle pod stocker
          rows << [pod, @fallback_pod_stocker, '', 'ReticlePod']
        end
      else
        $log.warn '  error message: pod not found.'
      end
      return Emulator.get_headers(colkeys).merge('rows'=>rows)
    end

  end

end
