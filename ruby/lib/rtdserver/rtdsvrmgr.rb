=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: sfrieske, 2016-11-09

Version:  1.0.4

History:
  2018-04-16 sfrieske, improved logging
  2019-06-20 sfrieske, handle odd number of parameters ('SCENARIO=')
  2021-02-05 sfrieske, use instance_of instead of kind_of
=end

require 'socket'    # for hostname
require 'corba'
require 'lib/rtdservermanager.jar'
require_relative 'rtdsvrmgrclient'


# RTD ServerManager
module RTD
  class ServerManager < Java::RTDServerManagerPOA
    java_import 'org.omg.PortableServer.POA'
    java_import 'org.omg.PortableServer.POAHelper'

    attr_accessor :dispatcher, :smclient, :orb, :thorb, :rootpoa, :ior


    def initialize(env, dispatcher, params={})
      super()  # no args
      @dispatcher = dispatcher
      @smclient = RTD::ServerManagerClient.new(env)  # connects to real RTD server's Corba interface
      # create and initialze the ORB
      props = Java::JavaUtil::Properties.new
      props.put('org.omg.CORBA.ORBClass', 'org.jacorb.orb.ORB')
      props.put('org.omg.CORBA.ORBSingletonClass', 'org.jacorb.orb.ORBSingleton')
      props.put('OAPort', (params[:svrmgrport] || 24356).to_s)
      #props.put('OAIAddr', '0.0.0.0')
      @orb = Java::OrgOmgCORBA::ORB.init([].to_java(:string), props)
      # get reference to rootpoa and activate the POAManager
      @rootpoa = @orb.resolve_initial_references('RootPOA')
      @rootpoa.the_POAManager().activate()
      # create servant and register it with the ORB
      #   not required, we are the servant (a.k.a. implementation) and already have the orb
      #   smimpl = ServerManagerImpl.new
      #   smimpl.setORB(@orb)
      # get object ref from the servant (self)
      ref = @rootpoa.servant_to_reference(self)
      @ior = @orb.object_to_string(ref)
      ## not needed   smref = Java::RTDServerManagerHelper.narrow(ref)
      start_orb
      fname = params[:iorfile] || "log/rtdservermanager_#{Socket.gethostname}_#{env}.ior"
      $log.info "writing IOR file #{fname.inspect}"
      File.binwrite(fname, @ior)
      $log.info self.inspect
    end

    def inspect
      "#<#{self.class.name} #{@smclient.inspect}>"
    end

    def start_orb
      $log.info 'start_orb'
      ($log.warn '  already running'; return) if @thorb && @thorb.alive?
      @thorb = Thread.new {@orb.run()}
    end

    def stop_orb
      @orb.shutdown(true)   # wait for completion
      @orb.destroy
      return @thorb.alive?
    end

    # RTDServerManagerPOA implementation

    # TxDispatchRequest, called by SiView MM
    # arguments are (station, rule, errcheck_col, errcheck_val, parameters)
    #
    # return Java::dispatchResult_struct
    def TxDispatchRequest(station, rule, errcheck_col, errcheck_val, parameters)
      astring = "#{station.inspect}, #{rule.inspect}, #{errcheck_col.inspect}, #{errcheck_val.inspect}, #{parameters.to_a}"
      $log.info "TxDispatchRequest #{astring}"
      # parameters come like 'requestingEntity=Pull' but also 'SCENARIO='
      begin
        params = parameters.collect {|e| e.split('=')}.flatten
        params << '' if params.size.odd?  # e.g. 'SCENARIO='
        res = @dispatcher.dispatch(station, rule, Hash[*params])
      rescue ArgumentError => e
        $log.error 'RTDEmu ServerManager error: ' + e.message
        res = 'RTDEmu ServerManager error: ' + e.message
      end
      if res
        # Emulator response, format
        if res.instance_of?(String)
          # error msg received
          dsperr_msg = res
          syserr = '20'
          syserr_msg = res
          usrerr = '20'  # '0'
          usrerr_msg = 'no error'
          rows = []
          res = {'headers'=>[], 'widths'=>[], 'types'=>[]}
        else
          dsperr_msg = ''
          syserr = '0'
          syserr_msg = 'no error'
          usrerr = '0'
          usrerr_msg = 'no error'
          # need to convert numbers to strings
          rows = res['rows'].collect {|row| row.collect {|e| e.to_s}}
        end
        ret = Java::dispatchResult_struct.new
        ret.numberRows = rows.size
        ret.numberCols = res['headers'].size
        ret.systemErrorCode = syserr
        ret.systemErrorMessage = syserr_msg
        ret.userErrorCode = usrerr
        ret.userErrorMessage = usrerr_msg
        ret.dispatchErrorMessage = dsperr_msg
        ret.dispatchRows = rows
        ret.columnHeaders = res['headers']
        ret.columnWidths = res['widths']
        ret.columnTypes = res['types']
        ret.siInfo = @orb.create_any
        return ret
      else
        # forward to the real RTD ServerManager
        $log.info "calling real Corba RTD server: #{astring}"
        ret = @smclient.manager.TxDispatchRequest(station, rule, errcheck_col, errcheck_val, parameters)
        $log.info "completed real Corba RTD server #{astring}"
        return ret
      end
    end

  end

end
