=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: Steffen Frieske, GLOBALFOUNDRIES, INC., 2019

Version:  1.0.0

History:
=end

require 'json'
require 'webrick'
require_relative 'rtdclient'


module RTD

  # TCP interface for QueryExecutor Emulator
  class QEInterface < WEBrick::GenericServer
    attr_accessor :dispatcher, :host, :port, :thsvr, :qeclients, :qeclients_avail

    def initialize(env, dispatcher, params={})
      @dispatcher = dispatcher
      # create QE server clients for forwarded msgs
      nclients = params[:nclients] || 2
      $log.info "QEInterface: creating #{nclients} qeclients"
      @qeclients = nclients.times.collect {RTD::Client.new("#{env}_qe")}
      @qeclients_avail = Queue.new
      @qeclients.size.times {|i| @qeclients_avail.push(i)}
      # start the TCP server
      @port = params[:port] || 22250
      super(Port: @port, BindAddress: '', MaxClients: params[:maxconn] || 8, DoNotReverseLookup: true)
      @thsvr = Thread.new {start}
      $log.info self.inspect
    end

    def inspect
      "#<#{self.class.name} on #{Socket.gethostname} listening at #{@host.inspect}:#{@port}>"
    end

    # method actually serving the request
    def run(sock)
      begin
        # read header length and header, the next 4 bytes (checksum) is not used
        data = sock.read(8)
        ($log.warn {"EOL read from #{sock.peeraddr(true)}"}; return) unless data
        rcvtime = Time.now
        $log.debug {"received data/header length: #{data.inspect}"}
        mlen = data.unpack('NN').first
        ($log.warn {'message size is 0'}; return) if mlen == 0
        # read data/header
        data = sock.read(mlen)
        $log.debug {"received data/header: #{data.inspect}"}
        # split data, removing CMD_Q
        dd = data[5..-1]
        ff = dd.split('|')
        $log.debug {ff.inspect}
        # extract command
        cmd = ff[0]
        reply = nil
        if cmd == 'run'
          # dispatch call
          report = File.basename(ff[-1])  # last field, currently no trailing parameters supported
          reply = @dispatcher.dispatch(report)
          if reply
            $log.info "completed QEEmu #{report}"
          elsif @qeclients.empty?
            reply = "#error: 'executing #{report} failed'"
            $log.warn $!
          else
            # forward to the real QE server
            $log.info "calling real QE server: #{report}\n  (#{dd})"
            idx = @qeclients_avail.pop
            begin
              qeclient = @qeclients[idx]
              qeclient.write(data, msgtype: '')
              $log.info "called real QE server: #{report}"
              reply = qeclient.read
              $log.warn 'no answer from real QE server' unless reply
            rescue
              reply = "#error: 'executing #{report} failed'"
              $log.warn $!
            end
            @qeclients_avail.push(idx)
            $log.info "completed real QE server #{report}"
          end
        elsif cmd == 'status'
          reply = "QEEmu on #{sock.addr[2]}:#{sock.addr[1]}"
        else
          reply = "Command #{cmd.inspect} unknown"
        end
        #
        # build and send the response msg
        if reply
          msg_size = reply.size + 5
          sock.write([msg_size, ~msg_size].pack('NN'))
          sock.write('CMD_A')
          sock.write(reply)
        else
          $log.warn "no reply!"
        end
      rescue => e
        $log.warn $!
        $log.warn e.backtrace.join("\n")
      end
    end

  end

end
