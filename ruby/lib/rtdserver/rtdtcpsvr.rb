=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: Steffen Steidten, GLOBALFOUNDRIES, INC., 2009

Version:  4.2.3

History:
  2010-02-09 ssteidte, created the pure JRuby version based on GServer
  2010-11-16 ssteidte, pass RTD query parameters as Hash to rtd_impl
  2012-07-21 ssteidte, improved startup
  2012-10-30 ssteidte, make msg headers optional
  2014-02-25 ssteidte, remote interface and rule based pass-thru to real RTD system
  2014-04-30 ssteidte, added support for dynamic rules (e.g. for AutoProc testing)
  2014-12-10 ssteidte, Thread.abort_on_exception must be false except for debugging
  2014-12-19 ssteidte, keep override rules in @emustations
  2015-01-07 ssteidte, don't include DRbUndumped in RTD::Server
  2015-02-25 dsteger,  fixed error replies
  2015-05-04 ssteidte, based on WEBRick::GenericServer instead of GServer (for Ruby 2.2)
  2015-11-03 sfrieske, better logging for nil methods, single emulator instance
  2015-11-30 sfrieske, correct handling of last parameter being an empty string
  2016-06-09 sfrieske, use Mutex instead of Thread.exclusive
  2016-11-17 sfrieske, split off of RTD::Server
  2017-01-03 sfrieske, control interface for new RTD::EmuRemote
  2017-09-06 sfrieske, added log line 'called real RTD server'
  2018-03-26 sfrieske, separated control interface for new RTD::EmuRemote to rtdemuadmin.rb
  2019-02-18 sfrieske, fixed rule (report) detection for JCAP jobs
  2020-14-07 sfrieske, removed obsolete "require 'json'"
  2021-02-05 sfrieske, use instance_of instead of kind_of
=end

require 'webrick'
require_relative 'rtdclient'


module RTD

  # TCP interface for RTD Emulator (remote status_tool), used by RTD::Dispatcher
  class TCPInterface < WEBrick::GenericServer
    attr_accessor :dispatcher, :host, :port, :thsvr, :rtdclients, :rtdclients_avail

    def initialize(env, dispatcher, params={})
      @dispatcher = dispatcher
      # create RTD server clients in case of forwarding, default is to use an emulator for all calls
      nclients = params[:nclients]
      $log.info "creating #{nclients} rtdclients"
      # create rtdclients
      @rtdclients = nclients.times.collect {RTD::Client.new(env)}
      @rtdclients_avail = Queue.new
      @rtdclients.size.times {|i| @rtdclients_avail.push(i)}
      # start the TCP server
      @port = params[:port] || 9090
      super(Port: @port, BindAddress: '', MaxClients: params[:maxconn] || 32, DoNotReverseLookup: true)
      @thsvr = Thread.new {start}
      $log.info self.inspect
    end

    def inspect
      "#<#{self.class.name} on #{Socket.gethostname} listening at #{@host.inspect}:#{@port}>"
    end

    # method actually serving the request
    def run(sock)
      begin
        # read header length and header, the next 4 bytes (checksum) is not used
        data = sock.read(8)
        ($log.warn {"EOL read from #{sock.peeraddr(true)}"}; return) unless data
        rcvtime = Time.now
        $log.debug {"received data/header length: #{data.inspect}"}
        mlen = data.unpack('NN').first
        ($log.warn {'message size is 0'}; return) if mlen == 0
        # read data/header
        data = sock.read(mlen)
        $log.debug {"received data/header: #{data.inspect}"}
        # header is optional, read data if data/header does not start with "CMD_Q"
        unless data.start_with?('CMD_')
          # read msg length + 4 dummy bytes
          data = sock.read(8)
          mlen = data.unpack('NN').first
          # read msg body
          data = sock.read(mlen)
          $log.debug {"received data: #{data.inspect}"}
        end
        # split data, removing CMD_Q
        dd = data[5..-1]
        ff = dd.split('|')
        # ff << '' if dd.end_with?('|')
        $log.debug {ff.inspect}
        # extract command, station, rule and optional params
        cmd = ff[0]
        if cmd == 'dlis'
          # dispatch call
          station = ff[2]
          params = ff[3..-1] || []  # just pass everything after Station
          # find rule name
          ridx = ff.index('rtdDispLogicId') || ff.index('Report')
          if ridx
            rule = ff[ridx + 1]
          elsif station == 'WHEREPOD'
            rule = 'WhereNextPod'
          elsif station.include?('WHATPOD')
            rule = 'WhatNextPod'
          else  # does this really happen?
            rule = 'WhatNextLotList'
          end
          params << nil if params.size.odd?
          params = Hash[*params]
          dsptime = Time.now
          res = @dispatcher.dispatch(station, rule, params)
          if res
            if res.instance_of?(Hash)
              headerdata = res['headerdata'] || [station, 0, 0, 0, 0, 0, 0]
              reply = '#headers' + "\n" +
                "'STATION: %s     MOVEABLE LOTS:   %d QTY:    %d     HELD LOTS:   %d QTY:    %d     BLOCKED LOTS:   %d QTY:    %d'" % headerdata +
                "\n" +
                "#columns '" + res['headers'].join("' '") + "'\n" +
                "#widths '" + res['widths'].join("' '") + "'\n" +
                "#types '" + res['types'].join("' '") + "'\n" +
                "#lotcol -1\n" +
                "#rows\n" + res['rows'].collect {|row| "'" + row.join("' '") + "'\n"}.join + "\n" +
                "#blocked" + " ''" * res['headers'].size + "\n" +           # empty string for each column
                "#rule '#{res['replyrule'] || res['config'] || rule}'\n" +  # returned rule name
                "#station '#{station}'\n" +
                "#station_type 'STATION'\n" +
                "#queue_duration '#{dsptime - rcvtime}'\n" +
                "#process_duration '#{Time.now - rcvtime}'\n"
              $log.debug {"reply:\n#{reply}\n\n"}
            elsif res.instance_of?(String) && res.start_with?('#error:')
              $log.debug {"reply:\n#{reply}\n\n"}
              reply = res
            else
              $log.warn "RTDEmu response is not a hash or error string:\n#{res.inspect}"
              reply = "#error: 'RTDEmu response is not a hash'"
            end
          else
            # forward to the real RTD server
            $log.info "calling real RTD server: #{station}, #{rule}, #{params}\n  (#{dd})"
            idx = @rtdclients_avail.pop
            begin
              rtdclient = @rtdclients[idx]
              rtdclient.write(data, msgtype: '')
              $log.info "called real RTD server: #{rule}, #{station}, #{params}"
              reply = rtdclient.read
              $log.warn 'no answer from real RTD server' unless reply
              #reply ||= "#error: 'no answer from real RTD server'"
            rescue
              reply = "#error: 'executing #{rule} failed'"
              $log.warn $!
            end
            @rtdclients_avail.push(idx)
            $log.info "completed real RTD server #{rule}, #{station}"
          end
        elsif cmd == 'status'
          # return an answer similar to a real RTD server
          reply = "RTDEmulator on #{sock.addr[2]}:#{sock.addr[1]}"
        else
          reply = "#error: 'command #{cmd.inspect} is not implemented'"
        end
        #
        # build and send the response msg
        if reply
          msg_size = reply.size + 5
          sock.write([msg_size, ~msg_size].pack('NN'))
          sock.write('CMD_A')
          sock.write(reply)
        end
      rescue => e
        $log.warn $!
        $log.warn e.backtrace.join("\n")
      end
    end

  end

end
