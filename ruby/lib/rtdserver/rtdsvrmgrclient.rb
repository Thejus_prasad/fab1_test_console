=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.


Author:   Steffen Frieske, 2016-11-09
Version:  1.0.4

History:
  2019-02-06 sfrieske, added missing java_import 'org.omg.CORBA.ORB'
  2021-02-05 sfrieske, use instance_of instead of kind_of
  2021-08-26 sfrieske, use read_config instead of _config_from_file
=end

require 'corba'
require 'lib/rtdservermanager.jar'
require 'util/readconfig'


module RTD

  # ServerManager Client for RTD Dispatcher
  class ServerManagerClient
    java_import 'org.omg.CORBA.ORB'
    ServerManagerResponse = Struct.new(:headers, :rows, :nrows, :ncols)
    attr_accessor :env, :_props, :iorparser, :orb, :manager

    def initialize(env, params={})
      @env = env
      @_props = read_config(params[:instance] || "#{@env}.svrmgr", 'etc/rtdservers.conf', params)
      @iorparser = IORParser.new(@_props[:ior] || @_props[:iorfile])
      @orb = Java::OrgOmgCORBA::ORB.init([].to_java(:string), nil)
      obj = @orb.string_to_object(@iorparser.ior)
      @manager = Java::RTDServerManagerHelper.narrow(obj)
    end

    def inspect
      "#<#{self.class.name} env: #{@env}>"
    end

    def dispatch_request(station, params={})
      rule = params[:rule] || 'WhatNextLotList'   # rtdDispLogicId
      errcheck_col = params[:errcheck_col] || ''
      errcheck_val = params[:errcheck_val] || ''
      parameters = params[:parameters] || []
      parameters = [parameters] if parameters.instance_of?(String)
      $log.info "dispatch_request #{station.inspect}, rule: #{rule.inspect}"
      begin
        res = @manager.TxDispatchRequest(station, rule, errcheck_col, errcheck_val, parameters)
        if res.systemErrorCode != '0' || res.userErrorCode != '0'
          $log.warn "#{station}: systemErrorCode: #{res.systemErrorCode}, userErrorCode: #{res.userErrorCode}"
          return
        end
        return res if params[:raw]
        return ServerManagerResponse.new(res.columnHeaders.to_a, res.dispatchRows.to_a, res.numberRows, res.numberCols)
      rescue
        $log.error "#{station}  #{$!}"
        return
      end
    end

  end

end
