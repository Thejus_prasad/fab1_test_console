=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: Steffen Steidten, GLOBALFOUNDRIES, INC., 2009

Version:  4.2.2

History:
  2010-02-09 ssteidte, created the pure JRuby version based on GServer
  2010-11-16 ssteidte, pass RTD query parameters as Hash to rtd_impl
  2012-07-21 ssteidte, improved startup
  2012-10-30 ssteidte, make msg headers optional
  2014-02-25 ssteidte, remote interface and rule based pass-thru to real RTD system
  2014-04-30 ssteidte, added support for dynamic rules (e.g. for AutoProc testing)
  2014-12-10 ssteidte, Thread.abort_on_exception must be false except for debugging
  2014-12-19 ssteidte, keep override rules in @emustations
  2015-01-07 ssteidte, don't include DRbUndumped in RTD::Server
  2015-02-25 dsteger,  fixed error replies
  2015-05-04 ssteidte, based on WEBRick::GenericServer instead of GServer (for Ruby 2.2)
  2015-11-03 sfrieske, better logging for nil methods, single emulator instance
  2015-11-30 sfrieske, correct handling of last parameter being an empty string
  2016-06-09 sfrieske, use Mutex instead of Thread.exclusive
  2016-11-17 sfrieske, separated RTD::Dispatcher, RTD::TCPInterface and RTD::ServerManager
  2017-01-03 sfrieske, new RTD::EmuRemote without DRb, removed delay - should go to RTDEmuExt
  2017-09-21 sfrieske, added back emudelay
  2017-12-12 sfrieske, renamed from abigous rtdserver.rb to dispatchersrv.rb
  2018-03-26 sfrieske, separated control interface for new RTD::EmuRemote to rtdemuadmin.rb
  2021-02-05 sfrieske, use instance_of instead of kind_of
  2021-03-11 sfrieske, wrap emuremote call in rmutex.synchronize
  2021-05-18 sfrieske, fixed cmdline parameter passing
  2021-08-26 sfrieske, use read_config instead of _config_from_file
=end

load './ruby/set_paths.rb'
require 'json'
require 'optparse'
require 'socket'
require 'util/readconfig'
require_relative 'rtdemu'
require_relative 'rtdemuadmin'
require_relative 'rtdsvrmgr'
require_relative 'rtdtcpsvr'

Thread.abort_on_exception = false   # set to true for debugging only!


module RTD

  # RTD Call Dispatcher with TCP (remote status_tool) and CORBA (ServerManager) Interfaces
  class Dispatcher
    attr_accessor :mutex, :emudelay, :emurandom, :rmtimeout, :emustations, :emu, :admin, :tcpinterface, :svrmgrinterface

    # env is a string like "itdc", matching an instance in etc/siview.conf
    # valid parameters are host to restrict the bind address, port, maxconn and stations
    # stations is a hash with optional RTD rules as keys (e.g. WhatNextLotList). If a rule does not appear
    # all requests are served by the emulator. If a rule appears only requests for stations starting with one of the
    # strings in the array for this key are serverd by the emulator, all other requests are forwarded to the real RTD server.
    def initialize(env, overrides={})
      params = read_config("#{env}.emu", 'etc/rtdservers.conf', overrides)
      @emudelay = 0           # delays emulator calls (not the forwarded ones, not the emuremote ones)
      @emurandom = 0.2        # 20 % randomness added
      @rmtimeout = 24 * 3600  # remove remote methods after a day
      # create RTD server clients in case of forwarding, default is to use an emulator for all calls
      @mutex = Mutex.new
      @rmutex = Mutex.new
      set_emustations(params[:stations])
      # create RTD engine
      @emu = Emulator.new(env, params)
      @admin = EmuAdmin.new(self, host: '', adminport: params[:adminport])
      # start the TCP and ServerManager interfaces
      nclients = params[:stations].nil? ? 0 : (params[:nclients] || 16)
      @tcpinterface = TCPInterface.new(env, self, port: params[:port], maxconn: params[:maxconn], nclients: nclients)
      @svrmgrinterface = ServerManager.new(env, self, svrmgrport: params[:svrmgrport])
      $log.info self.inspect
    end

    def inspect
      "#<#{self.class.name}  #{@emu.inspect}>"
    end

    def join
      @tcpinterface.thsvr.join
    end

    def set_emustations(stns, params={})
      rules = ['WhatNextLotList', 'WhereNextInterBay', 'WhereNextEquipment', 'WhatNextPod', 'WhereNextPod']
      if stns.nil?
        @emustations = Hash[rules.collect {|r| [r, [['', nil]]]}]  # all emulated because of the empty string
      elsif stns.instance_of?(String)
        # either none or like "UT" or "UT PDWH"
        @emustations = {}  ##Hash[rules.collect {|r| [r, []]}]  # nothing emulated
        stns.split.each {|stn| add_emustation(stn)} if stns != 'none'
      else
        @emustations = stns  # stations explicitly configured are emulated, others forwarded (per rule)
      end
      $log.info "emulated stations: #{@emustations}"
    end

    # add a single entry to @emustations for a specific rule, default is WhatNextLotList
    #
    # return emustations for the rule
    def add_emustation(stn, params={})
      $log.info "add_emustation #{stn}, #{params}"
      rule = params[:rule] || 'WhatNextLotList'
      mstns = delete_emustation(stn, rule: rule, silent: true)
      @mutex.synchronize {
        mstns = @emustations[rule] = [] if mstns.nil?
        method = params[:method]
        if method.instance_of?(Array)
          method << Thread.new {sleep @rmtimeout; delete_emustation(stn, rule: rule)}
        end
        mstns << [stn, method]  # use standard emulator method if nil
        # sort keys in descending order, such that the most specific rule comes first
        mstns.sort!
        mstns.reverse!
      }
    end

    # remove a single entry from @emustations for a specific rule, default is WhatNextLotList
    #
    # return remaining emustations for the rule
    def delete_emustation(stn, params={})
      $log.info "delete_emustation #{stn}, #{params}" unless params[:silent]
      rule = params[:rule] || 'WhatNextLotList'
      mstns = @emustations[rule] || return
      @mutex.synchronize {
        sname, method = mstns.delete(mstns.assoc(stn))
        if method.instance_of?(Array)
          begin
            sock, remip, mid, th = method
            th.kill
            sock.close
          rescue
            $log.warn {"error removing remote method, rescued: #{$!}"}
          end
        end
        mstns
      }
    end

    # dispatch the request to an emulated method or real RTD server based on station and rule
    #
    # call the emu method if emulated and return its response, else nil
    def dispatch(station, rule, params={})
      # find configuration for the station
      emulated = false
      method = rule
      @mutex.synchronize {
        mstns = @emustations[method]
        if mstns
          mstns.each {|e|
            stn, m = e
            if station.start_with?(stn)
              ($log.info "station #{station} specific method: #{m.inspect}"; method=m) if m
              emulated = true
              break
            end
          }
        end
      }
      # return nil if nothing found, the caller of dispatch() will call the real RTD server
      return unless emulated
      #
      # call the identified RTD emulator's rule or a remote server serving the rule
      begin
        if method.instance_of?(Array)
          # remote method: [socket, method id]
          $log.info "calling emuremote #{method} (#{rule}), station #{station}, #{params.inspect}"
          @mutex.synchronize {
            sock, remip, mid, th = method
            data = {station: station, rule: rule, params: params, mid: mid}.to_json
            sock.write([data.size].pack('N'))
            sock.write(data)
            rdata = sock.read(4)
            rlen = rdata.unpack('N').first
            res = sock.read(rlen)
            $log.info "completed emuremote #{method}, #{station}"
            return JSON.parse(res)
          }
        elsif @emu.respond_to?(method)
          $log.info "calling emu #{method}, #{station}, #{params.inspect}"
          if @emudelay > 0
            delay = @emudelay * (1.0 + @emurandom * rand)
            $log.info "delaying emu call by #{delay} s"
            sleep delay
            $log.info "calling delayed emu #{method}, #{station}"
          end
          reply = @emu.send(method, station, rule, params)
          $log.info "completed emu #{method}, #{station}"
          return reply
        else
          $log.warn "method #{method.inspect} is not implemented"
          return "#error: 'not implemented report #{rule}'"
        end
      rescue => e
        $log.error $!
        $log.warn e.backtrace.join("\n")
        return "#error: 'executing #{rule} failed'"
      end
    end
  end

end


def main(argv)
  # set defaults
  env = nil
  port = nil
  svrmgrport = nil
  verbose = false
  noconsole = true
  # parse cmdline options
  optparser = OptionParser.new {|o|
    o.banner = "Usage: #{File.basename(__FILE__)} -e <env> [options]"
    o.on('-e', '--env [ENV]', 'environment, e.g. "let"') {|s| env = s}
    o.on('-p', '--port [PORT]', Integer, 'listen port, default: 9090') {|s| port = s}
    o.on('-s', '--svrmgrport [PORT]', Integer, 'srvmgr port, default: 24356') {|s| svrmgrport = s}
    o.on('-v', '--verbose', 'verbose logging') {verbose = true}
    o.on('--console', 'log to console in addition to log file') {noconsole = false}
    o.on_tail('-h', '--help', 'diplay this screen') {puts o; return}
  }
  optparser.parse(*argv)
  (STDOUT.puts "missing parameter -e <env>"; return) unless env
  logfile = "log/rtdserver_#{env}.log"
  create_logger(file: logfile, file_verbose: verbose, verbose: verbose, noconsole: noconsole)
  STDOUT.puts "logging to #{logfile}, console log: #{!noconsole}"
  svr = RTD::Dispatcher.new(env, {port: port, svrmgrport: svrmgrport}.select {|k, v| !v.nil?})
  $log.info "#{svr.inspect} started"
  svr.join
end

main(ARGV) if __FILE__ == $0
