=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.


Author: Steffen Frieske, 2018-05-16

History:
  2021-08-30 sfrieske, moved xmlhash.rb to util
=end

require 'json'
require 'util/xmlhash'

module SIL

  # APC Communication mock
  class APC
    attr_accessor :mq, :timeout, :_resp

    def initialize(env, params={})
      @mq = MQ::JMS.new("silapc.#{env}")
      @timeout = params[:timeout] || 60
    end

    def receive_msg(params={})
      to = params[:timeout] || @timeout
      $log.info "receive_msg timeout: #{to}"
      msg = @mq.receive_msg(timeout: to) || return
      h = XMLHash.xml_to_hash(msg, '*//*Body')
      @_resp = h  # for dev only
      c = h[:executeRequest] || ($log.warn 'wrong message format, missing "executeRequest" tag'; return)
      c = c[:requestContent] || ($log.warn 'wrong message format, missing "requestContent" tag'; return)
      return JSON.parse(c.tr("\n", ''))
    end

    def send_response_fault(params={})
      reqtype = params[:type] || 'SIL-PCL-WfrSPCData'
      errcode = params[:errcode] || 'INVALID-REQUEST'
      errmsg = params[:errmsg] || 'QA: Request type not supported'
      h = {'env:Envelope': {'xmlns:env'=>'http://schemas.xmlsoap.org/soap/envelope/',
        'env:Header': nil, 'env:Body': {'rs:executeResponse': {
          'xmlns:rs'=>'urn:toolbox:/toolbox/services/baseline/requestBaselineService.xml',
          responseType: "#{reqtype}.Response", responseContent: nil,
          status: 'Fail', errorCode: errcode, errorMessage: errmsg
        }}
      }}
      return @mq.send_response(XMLHash.hash_to_xml(h))
    end

  end

end
