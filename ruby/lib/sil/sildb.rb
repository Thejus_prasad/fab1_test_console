=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.


Author: Steffen Steidten, 2011-11-04

History:
  2012-01-24 ssteidte, use Sequel instead of DBI for databases
  2012-08-23 ssteidte, remove to_char conversion from queries for Date and Time fields
  2013-02-12 ssteidte, pd_reference returns first set of PDs as default, parameter ACTIVE defaults to 'Y'
  2015-07-27 ssteidte, moved globals to constants
  2017-07-20 sfrieske, moved from Space to SIL module along with minor cleanup
  2018-09-11 sfrieske, removed unused methods
  2021-02-05 sfrieske, use instance_of instead of kind_of
=end

require 'dbsequel'


module SIL

  # SIL DB queries
  class DB
    SpcRun = Struct.new(:runid, :facility, :user, :eqp, :ts, :dcp, :sapdcp, :event, :storetime, :e10)
    SpcProcessingArea = Struct.new(:runid, :processingarea, :eqp, :storetime, :e10)
    SpcPDRef = Struct.new(:refid, :parameter, :eqp, :component, :pa, :mpd,
      :specid, :specversion, :active, :deactivated, :scenario, :product, :productgroup, :comment, :storetime, :nexttestpd, :pdrefs)
    SpcGenericKey = Struct.new(:keyid, :module, :name, :method, :fixed_property, :resppd, :parameter, :ldsvalue,
      :storetime, :comment, :specid, :specversion, :active, :deactivated, :scenario, :lds, :group)

    attr_accessor :env, :db


    def initialize(env, params={})
      @env = env
      @db = ::DB.new("sil.#{@env}", params)
    end

    # used in SIL_00#test14 and SIL_03
    def isAutocharting(parameter)
      res = @db.select('select count(*) from AUTO_CHARTING_INFO where PARAMETER_VALUE = ?', parameter) || return
      return res.first && res.first.first > 0
    end

    # return sorted array of sample ids belonging to the run id
    def runid_samples(runid, params={})
      q, qargs = @db._q_restriction('select distinct SAMPLE_ID from SPACE_CHART_LINK', [], 'RUN_ID', runid)
      res = @db.select(q, *qargs) || return
      return res.flatten.sort
    end

    # return referenced PDs as array of SpcPDRef structs for the first matching set, pass all: true to get an array of all matches
    def pd_reference(params={})
      qargs = []
      q = 'select ID, PARAMETER_NAME, EQUIPMENT_NAME, COMPONENT_NAME, PROCESSINGAREA_NAME, METROLOGY_PD, SPEC_ID,
        SPEC_VERSION, ACTIVE, DEACTIVATED_TIME, SPACE_SCENARIO, PRODUCT, PRODUCT_GROUP, COMMIT_COMMENT, STORETIME, NEXT_TEST_PD from PD_REFERENCE'
      q, qargs = @db._q_restriction(q, qargs, 'COMMIT_COMMENT', params[:comment])
      q, qargs = @db._q_restriction(q, qargs, 'METROLOGY_PD', params[:mpd])
      q, qargs = @db._q_restriction(q, qargs, 'SPEC_ID', params[:specid])
      q, qargs = @db._q_restriction(q, qargs, 'SPEC_VERSION', params[:specversion])
      q, qargs = @db._q_restriction(q, qargs, 'ACTIVE', (params[:active] || 'Y'))
      q += ' ORDER BY ID'
      res = @db.select(q, *qargs) || return
      ret = res.collect {|r|
        e = SpcPDRef.new(*r)
        e.refid = e.refid.to_i
        e.deactivated = e.deactivated.to_i
        e.storetime = e.storetime.to_i
        qargs = []
        q = 'select PROCESS_PD, PD_REFERENCE_GROUPING.GROUP_ORDER from PD_REFERENCE_VALUE
              inner join PD_REFERENCE_GROUPING
              on PD_REFERENCE_VALUE.PD_REFERENCE_GROUPING_ID = PD_REFERENCE_GROUPING.ID'
        q, qargs = @db._q_restriction(q, qargs, 'PD_REFERENCE_GROUPING.PD_REFERENCE_ID', e.refid)
        q += ' ORDER BY GROUP_ORDER'
        qargs.unshift(q)
        pdgroups = @db.select(*qargs)
        e.pdrefs = []
        pdgroups.each {|p, g|
          index = g.to_i
          e.pdrefs.push([]) if e.pdrefs.length <= index
          e.pdrefs[index].push(p)
        }
        e
      }
      return ret if params[:all]
      return [] if ret.first.nil?
      return ret.first.pdrefs.flatten
    end

    # used in SIL_85 (only)  TODO: move there !
    def run(runid, params={})
      q = 'select RUNID, FACILITY, OPERATOR, EQUIPMENT, TIMESTAMP, DCPNAME, SAPDCPID, EVENT, STORETIME, EQUIPMENT_E10_STATE from RUN where RUNID=?'
      res = @db.select(q, runid) || return
      return res.collect {|r| SpcRun.new(*r)}
    end

    # used in SIL_85 (only)  TODO: move there !
    def processingarealookup(runid, params={})
      q = 'select RUNID, PROCESSINGAREA, EQUIPMENT, STORETIME, PROCESSINGAREA_E10_STATE from PROCESSINGAREALOOKUP where RUNID = ? ORDER BY PROCESSINGAREA'
      res = @db.select(q, runid) || return
      return res.collect {|r| SpcProcessingArea.new(*r)}
    end

    # used in SIL_01 and SIL_02
    def nspeclimits(parameter, params={})
      q = "select count(*) from SPEC_LIMIT WHERE ACTIVE='Y'"
      q, qargs = @db._q_restriction(q, [], 'PARAMETER_NAME', parameter)
      q, qargs = @db._q_restriction(q, qargs, 'PD', params[:pd])
      q, qargs = @db._q_restriction(q, qargs, 'ROUTE', params[:route])
      q, qargs = @db._q_restriction(q, qargs, 'PRODUCT', params[:product])
      q, qargs = @db._q_restriction(q, qargs, 'PRODUCT_GROUP', params[:productgroup])
      q, qargs = @db._q_restriction(q, qargs, 'TECHNOLOGY', params[:technology])
      res = @db.select(q, *qargs) || return
      return res.first.first
    end

    # used in SIL_85, greenzonevalue itself only TODO: move there (?)
    def greenzonevalue(parameter, type, spec)
      q = 'select GREEN_ZONE_VALUE from MONITORING_SPEC_LIMITS where PARAMETER=? and GREEN_ZONE_TYPE=? and SPEC_ID=?'
      res = @db.select(q, parameter, type, spec) || return
      ($log.warn "no unique greenzonevalue: #{res}"; return) unless res.size == 1 && res.first.size == 1
      return res.first.first.to_f
    end

    # used in SIL::Test#gkeys_dc only, TODO: move there (?)
    def generickey(params={})
      qargs = []
      q = 'select ID, MODULE, KEY_NAME, METHOD, FIXED_PROPERTY, RESP_PD_INDEX, PARAMETER_NAME, LDS_VALUE, STORETIME,
        COMMIT_COMMENT, SPEC_ID, SPEC_VERSION, ACTIVE, DEACTIVATED_TIME, SPACE_SCENARIO, LDS_TYPE from GENERIC_KEY'
      q, qargs = @db._q_restriction(q, qargs, 'KEY_NAME', params[:name])
      q, qargs = @db._q_restriction(q, qargs, 'METHOD', params[:method])
      q, qargs = @db._q_restriction(q, qargs, 'COMMIT_COMMENT', params[:comment])
      q, qargs = @db._q_restriction(q, qargs, 'SPEC_ID', params[:specid])
      q, qargs = @db._q_restriction(q, qargs, 'PARAMETER_NAME', params[:parameter])
      q, qargs = @db._q_restriction(q, qargs, 'MODULE', params[:department])
      q, qargs = @db._q_restriction(q, qargs, 'ACTIVE', params[:active] || 'Y')
      q, qargs = @db._q_restriction(q, qargs, 'LDS_TYPE', params[:ldstype])
      q += ' ORDER BY ID'
      res = @db.select(q, *qargs) || return
      return res.collect {|r| SpcGenericKey.new(*r)}
    end


    def perfdata(method, params={})
      tend = params[:tend] || Time.now
      tend = Time.parse(tend) if tend.instance_of?(String)
      tstart = params[:tstart] || (tend - 86400 * 33)
      tstart = Time.parse(tstart) if tstart.instance_of?(String)
      q = 'select TIMESTAMP, MEAN from FBPERFDATA WHERE TIMESTAMP >= ? and TIMESTAMP <= ?'
      q, qargs = @db._q_restriction(q, [tstart, tend], 'METHOD', method)
      q += ' ORDER BY TIMESTAMP'
      res = @db.select(q, *qargs) || return
      res.collect {|ts, mean| [ts, mean.to_f]}
    end

  end
end
