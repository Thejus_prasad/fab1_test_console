=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.


Author: Steffen Steidten, 2011-11-23

History:
  2013-08-14 ssteidte, removed Ruby1.8 support
  2015-07-02 ssteidte, moved globals to constants
  2015-08-07 ssteidte, expand clients for parallel submission of  DCRs
  2016-05-10 sfrieske, use rubyXL instead of POI to read Excel files
  2016-12-07 sfrieske, removed unused webmrb, use sfcservice instead of setupfc
  2017-05-17 sfrieske, option to pass AQV msgs for verify_aqv_message
  2017-07-20 sfrieske, refactored from SpaceTest
  2018-05-16 sfrieske, moved to subdir sil, added SIL::APC
  2019-01-23 sfrieske, use new DerdackEvents
  2021-01-05 sfrieske, minot cleanup in SIL::Test#initialize()
  2021-02-05 sfrieske, use instance_of instead of kind_of
  2021-08-26 sfrieske, use read_config instead of _config_from_file
=end

require 'fileutils'
require 'pp'
require 'derdack'
require 'rqrsp_mq'
require 'siview'
require 'jcap/aqv'
require 'util/readconfig'
require 'util/verifyhash'
require 'util/waitfor'
require 'util/xmlhash'     # for AQV messages
require_relative 'sil'
require_relative 'silapi'
require_relative 'silapc'
require_relative 'sildb'
require_relative 'sildcr'


# SIL Testing Support
module SIL

  class Test
    attr_accessor :env, :sv, :db, :server, :client, :logdir, :sfc,
      :notifications, :notification_application, :notification_timeout,
      :mqaqv, :aqv_timeout, :apc, :lds_inline, :lds_setup, :cas, :ca_timeout,
      :defaults, :flags, :lot, :technology, :department, :route, :product, :productgroup,
      :parameter_template, :parameters, :mpd_lit, :mpd, :ppd, :mtool, :ptool, :chambers,
      :_dcr, :_msgs, :_channels  # for debugging only


    def initialize(env, params={})
      @sv = params[:sv] || SiView::MM.new(env)
      @db = SIL::DB.new(env, params)
      begin
        @server = SIL::Server.new(env)
      rescue
        $log.info 'no connection to SIL server, cannot validate advanced properties'
      end
      @client = RequestResponse::MQXML.new("space.#{env}", timeout: params[:sil_timeout] || 900)
      @logdir = 'log/sil'
      FileUtils.mkdir_p(@logdir)
      @notifications = params.has_key?(:notifications) ? params[:notifications] : DerdackEvents.new(env)
      @notification_application = env == 'f8stag' ? 'SILStaging' : 'SIL'
      @notification_timeout = params[:notification_timeout] || 240  # TODO: that long?
      @mqaqv = MQ::JMS.new("aqv.spc.qa.#{env}")
      @aqv_timeout = params[:aqv_timeout] || 120
      @apc = SIL::APC.new(env)
      # SIL objects
      SIL::SpaceApi.init(env, params)
      silprops = read_config(env, 'etc/space.conf', params)
      @lds_inline = SIL::SpaceApi.lds(silprops[:ldsname_inline])
      @lds_setup = SIL::SpaceApi.lds(silprops[:ldsname_setup])
      @ca_timeout = params[:ca_timeout] || 240  # TODO: shorter timeout?
      @parameter_template = params[:parameter_template] || '_Template_QA_PARAMS_900'
      # default SPC object names, change them for a series of tests and reset to default by set_defaults()
      @defaults = params[:defaults] || {
        lot: params[:lot] || SIL::DCR::DefaultLot, technology: SIL::DCR::DefaultTechnology,
        department: SIL::DCR::DefaultDepartment, route: SIL::DCR::DefaultRoute,
        product: SIL::DCR::DefaultProduct, productgroup: SIL::DCR::DefaultProductGroup,
        mpd_lit: params[:mpd_lit] || 'LIT-MGT-NMETDEPDR.LIT', mpd: params[:mpd] || SIL::DCR::DefaultOperation, ppd: nil,
        parameters: SIL::DCR::DefaultParameters,
        mtool: params[:mtool] || 'UTFSIL01', ptool: params[:ptool] || 'UTCSIL01', chambers: nil, cas: []
      }
      set_defaults
      # SIL server flags disabling e.g. AQV Inline checks
      @flags = {aqv_inline_active: false, aqv_setup_active: true}
    end

    def inspect
      "#<#{self.class.name} #{SIL::SpaceApi.inspect} #{@sv.env}>"
    end

    # set defaults for a series of tests, reset to 'default defaults' when nothing is passed, return true on success
    def set_defaults(params={})
      $log.info "SIL::Test.set_defaults #{params}"
      ret = true
      params = @defaults.merge(params)
      @lot = params[:lot]
      @mpd_lit = params[:mpd_lit]
      @mpd = params[:mpd]
      @ppd = params[:ppd]
      @ppd = SIL::DCR::DefaultOperation if @ppd == :default  # some tests require a PPD not matching the MPD
      @ppd ||= @db.pd_reference(mpd: @mpd).first
      ($log.warn "  no PPD for MPD #{@mpd.inspect}"; ret = false) unless @ppd
      @technology = params[:technology]
      @department = params[:department]
      @route = params[:route]
      @product = params[:product]
      @productgroup = params[:productgroup]
      @parameters = params[:parameters]
      @mtool = params[:mtool]
      @ptool = params[:ptool]
      @chambers = params[:chambers] || @sv.eqp_info(@ptool).chambers.take(3).collect {|c, i| c.chamber}
      @cas = params[:cas]
      ($log.warn 'configuration error: CAs is nil'; return false) if @cas.nil?
      @cas.each {|ca| SIL::SpaceApi.ca(ca) || ($log.warn "CA #{ca.inspect} not defined"; ret = false)}
      return ret
    end

    # returns logfilename that includes the test program and method name
    def tclogname
      caller_locations.each {|location|
        return 'console' if location.path == '(irb)'
        next unless location.path.start_with?(Dir.pwd) && location.label.start_with?('test')
        return File.basename(location.path, '.rb') + '_' + location.label
      }
      return 'unknown'
    end

    # create, send and verify process DCR, nocharting,  return the process DCR on success   TODO: unify meas and process DCRs
    # was: Test_Space_Setup.submit_process_dcr
    def submit_process_dcr(params={}, &blk)
      $log.info "submit_process_dcr #{params.select {|k, v| ![:export].member?(k)}}"
      dcr = SIL::DCR.new(eqp: params[:ptool] || @ptool, eqptype: :chamber, chambers: params[:chambers] || @chambers, cj: params[:cj])
      lots = params[:lots] || params[:lot] || @lot  # multiple lots with the same parameters and readings, _no exceptions_
      lots = [lots] if lots.instance_of?(String)
      lparms = {op: params[:ppd] || @ppd, opNo: params[:opNo], opName: params[:opName], route: params[:route] || @route,
        product: params[:product] || @product, productgroup: params[:productgroup] || @productgroup,
        technology: params[:technology] || @technology, department: params[:department] || @department,
        lrecipe: params[:lrecipe], mrecipe: params[:mrecipe]}
      readings = params[:readings] || params[:ooc] && :ooc || :default
      lots.each {|lot|
        dcr.add_lot(lot, lparms)
        dcr.add_parameters(params[:parameters] || @parameters, readings,
          processing_areas: params[:processing_areas], nocharting: params[:nocharting] != false)
      }
      blk.call(dcr) if blk  # for additional lots with special setup (???) TODO ???
      @_dcr = dcr  # for debugging
      export = (params[:export] || "#{@logdir}/#{[tclogname, 'p', params[:exporttag]].compact.join('_')}.xml").tr('<>() ', '')
      $log.info "send_receive export: #{export}"
      res = @client.send_receive(dcr.to_xml, export: export) || return
      nsamples = params[:nsamples] || dcr.nreadings  # normally 0 because of nocharting: true
      verify_dcr_accepted(res, nsamples) || return
      return dcr
    end

    # create, send and verify meas DCR, default parameters added unless a block is given, return DCR on success
    def submit_meas_dcr(params={}, &blk)
      $log.info "submit_meas_dcr #{params.select {|k, v| ![:export].member?(k)}}"
      dcr = SIL::DCR.new(eqp: params[:mtool] || @mtool, chambers: params[:chambers] || @chambers, params: params)
      lots = params[:lots] || params[:lot] || @lot  # multiple lots with the same parameters and readings, _no exceptions_
      lots = [lots] if lots.instance_of?(String)
      metainfos = params[:metainfos] || nil
      metainfos = [metainfos] if metainfos && metainfos.first.instance_of?(String)
      lparms = {op: params[:mpd] || @mpd, route: params[:route] || @route,
        product: params[:product] || @product, productgroup: params[:productgroup] || @productgroup,
        technology: params[:technology] || @technology, department: params[:department] || @department,
        lrecipe: params[:lrecipe], mrecipe: params[:mrecipe]}
      pparams = params.select {|k, v| [:readingtype, :sites, :processing_areas].member?(k)}
      readings = params[:readings] || params[:ooc] && :ooc || :default
      lots.each {|lot|
        dcr.add_lot(lot, lparms)
        dcr.add_parameters(params[:parameters] || @parameters, readings, pparams)
      }
      if metainfos
        metainfos.each {|mi|
          i = 0
          if params[:milotbased]
            mi.last.last['lotID'] = lots.first
          elsif params[:milotandwaferbased]
            mi.last.first['lotID'] = lots.first
            dcr.readings(mi.first, all: true).each_key {|waferid| mi.last.last['waferID'] = waferid if i == 0}
          else
            dcr.readings(mi.first, all: true).each_key {|waferid|
              if i == 0
                mi.last.each {|pair| pair['waferID'] = waferid}
              elsif i > 0 && params[:miforallreadings]
                mi.last << mi.last.first.clone
                mi.last.last['waferID'] = waferid
              end
              i+=1
            }
          end
          dcr.add_metainfo(mi.first, mi.last)
        }
      end
      blk.call(dcr) if blk  # for additional lot setup, e.g. SIL_05#test13 (no technology)
      @_dcr = dcr  # for debugging only
      export = (params[:export] || "#{@logdir}/#{[tclogname, 'm', params[:exporttag]].compact.join('_')}.xml").tr('<>() ', '')
      $log.info "send_receive export: #{export}"
      res = @client.send_receive(dcr.to_xml, export: export) || return
      nsamples = params[:nsamples] || dcr.nreadings  # nsamples is different from nreadings for LOT parameters or 2S channels (!?)
      nooc = params[:nooc]
      nooc = nsamples * 2 if nsamples != :any && nooc.nil? && params[:ooc]  # OOC samples in SPC and CKC channels for most tests, TODO: remove :any
      verify_dcr_accepted(res, nsamples, nooc: params[:nooc], spcerror: params[:spcerror]) || return
      return dcr
    end

    # submit a single DCR and verify the DCR data, return true on success
    def send_dcr_verify(dcr, params={})
      @_dcr = dcr  # for debugging
      export = (params[:export] || "#{@logdir}/#{[tclogname, params[:exporttag]].compact.join('_')}.xml").tr('<>() ', '')
      $log.info "send_receive export: #{export}"
      res = @client.send_receive(dcr.to_xml, export: export) || return
      nsamples = params[:nsamples] || dcr.nreadings  # nsamples is different from nreadings for LOT parameters
      verify_dcr_accepted(res, nsamples, nooc: params[:nooc], spcerror: params[:spcerror], wip: params[:wip])
    end

    # verify DCR data, return true on success
    def verify_dcr_accepted(res, nsamples, params={})
      $log.info "verify_dcr_accepted #{nsamples}, #{params.reject {|p| params[p].nil?}}"
      ($log.warn '  no SIL response'; return) unless res
      rids = runids(res, wip: params[:wip]) || ($log.warn '  no runids found'; return)
      $log.info "  runids: #{rids}"
      # get samples, may take some time
      nsids = 0
      wait_for(timeout: 40, sleeptime: 5) {
        nsids = @db.runid_samples(rids).count
        nsids == nsamples || ((nsamples == :any) && (nsids > 0))   # :any is for 2S channel creation in SIL_63#test10 TODO: remove
      } || ($log.warn "  #{nsids} samples instead of #{nsamples} (rids: #{rids})"; return)
      # number of ooc samples
      if nooc = params[:nooc]
        nooc = nsamples if nooc == 'all'
        nooc = nsamples * 2 if nooc == 'all*2'
        # verify number of OOC points
        rooc = res[:submitRunResponse][:submitRunReturn][:spcSummary][:oocPoints]
        $log.info "  oocPoints: #{rooc}, expected: #{nooc}"
        ($log.warn 'wrong number of oocPoints'; return) if rooc != nooc.to_s
        # verify error code
        srrs = res[:submitRunResponse][:submitRunReturn]
        srrs = [srrs] unless srrs.instance_of?(Array)
        srrs.each {|srr|
          errcode = srr[:nonOOCErrorCode]
          ($log.warn '  should return an spc error'; return) if (params[:spcerror] && errcode.nil?)
          ($log.warn "  should return no spc error, got #{errcode}"; return) unless (params[:spcerror] || errcode.nil?)
        }
      end
      return true
    end

    def runids(res, params={})
      ($log.warn '  no MQ response'; return) unless res
      rids = []
      srrs = res[:submitRunResponse][:submitRunReturn]
      srrs = [srrs] unless srrs.instance_of?(Array)
      srrs.each {|srr|
        runid = srr[:runID].to_i
        rids << runid
        rids << runid - 1 if params[:wip] == 'mixed'  # used in SIL_46 only, TODO: remove (?)
        ($log.warn "  invalid DCR:\n#{res}"; return) if rids.first <= 0
      }
      return rids
    end

    # return true if parameter channels have been created in autocreated_folder, description (a.k.a. template for autocreation) is correct,
    #   and readings match the DCR (or passed values): wafer entries incl ekeys and dkeys
    def verify_channels(dcr, params={}, &blk)
      # get LDS from lot context, folder defaults to LDS/../AutoCreated_<department>
      folder = params[:folder]
      unless folder
        lds = dcr.lot_context['technology'] == 'TEST' ? @lds_setup : @lds_inline
        dpt = dcr.lot_context['department']
        folder = lds.folder("AutoCreated_#{dpt}")
      end
      $log.info "verify_channels  folder: #{folder.inspect}, #{params.reject {|p| p == :folder}}"
      rtype = params[:readingtype] || 'Wafer'  # how to select DCR readings and filter samples
      (params[:parameters] || @parameters).each_with_index {|p, i|
        ch = folder.spc_channel(p) || ($log.warn "no channel for #{p}"; return)
        ($log.warn "wrong description: #{ch.desc}"; return) if ch.desc != "Auto-created using template: #{params[:template] || @parameter_template}"
        readings = params[:readings] || dcr.readings(p, readingtype: rtype)  # covers all lots
        ($log.warn "no readings for parameter #{p}"; return) if readings.empty? && params[:readings].nil?  # allows to pass readings: {} to skip
        readings.each_pair {|w, v|
          s = ch.sample(rtype=>w) || ($log.warn "no unique sample for wafer #{w.inspect} in channel #{ch.inspect}"; return)
          verify_hash(dcr.ekeys(rtype=>w), s.ekeys, nil, refonly: true) || ($log.warn "wrong ekeys for #{s.inspect}"; return)
          verify_hash(dcr.dkeys(rtype=>w, parameter: p), s.dkeys, nil, refonly: true, timediff: 1) || ($log.warn "wrong dkeys for #{s.inspect}"; return)
          (blk.call(ch, s) || ($log.warn "wrong sample check for #{s.inspect}"; return)) if blk  # for special tests like USL etc. SIL_47, SIL_62
        }
      }
      return true
    end

    # extract hash of DC generic key values from the submitted multiple dcrs (in the order of RespPDs), for SIL_51
    def gkeys_dc(dcrs, mapping, params={})
      department = dcrs[0].lot_context['department']
      ldstype = dcrs[0].lot_context['technology'] == 'TEST' ? 'SETUP' : 'INLINE'
      $log.info "dcr_generickeys_dc lookup  department: #{department}, LDS TYPE: #{ldstype}"
      # get generic keys per DCR
      dcr_gkeys = dcrs.collect {|dcr| dcr.gkeys(mapping: mapping, empty: params[:empty])}
      # fill return hash: find resonsible PD for the key name and get the value from the respective DCR
      ret = {}
      mapping.each {|e|
        k, pname = e  # like ['Reserve1', 'equipment']
        spckname = k.start_with?('Reserve') ? 'GenericExKey%' : 'GenericDataKey%'
        dbkeys = @db.generickey(ldstype: ldstype, department: department, method: 'DC', parameter: pname, name: spckname) || return
        # get responsible PD, in case of multiple entries: ignore if same resppd, else abort
        if dbkeys.size > 1
          $log.info "multiple generic keys for #{k.inspect} (#{spckname.inspect}) and #{pname.inspect}"
          resppds = dbkeys.collect {|e| e.resppd}.uniq
          ($log.warn 'multiple responsible PDs'; return) if resppds.size > 1
        end
        # spec is 1 based, if zero: -1, pointing to the last dcr which is correct (!), since SIL 2.0.x the index matches the RespPD index
        ret[k] = dcr_gkeys[dbkeys.first.resppd - 1][k]
      }
      return ret
    end

    def verify_ecomment(channels, comment, params={})
      channels = [channels] unless channels.instance_of?(Array)
      $log.info "verify_ecomment of #{channels.size} channels, #{comment.inspect}"
      channels.each {|ch|
        s = ch.ckc_samples(params).last || ($log.warn "  no samples in channel #{ch}"; return)
        scomment = s.ecomment
        unless scomment && scomment.include?(comment)
          ($log.warn "  wrong ecomment for #{s.parameter}: #{scomment.inspect}, expected: #{comment.inspect}"; return)
        end
      }
      return true
    end

    # read all aqv messages from the queue (unless passed), find one for the DCR lot and verify result (success or fail), return true on success
    def verify_aqv_message(dcr, result, params={})
      lotctx = dcr.lot_context(icarrier: params[:icarrier], ilot: params[:ilot])  #, 'Lot'=>params[:lot])
      lot = params[:lot] || lotctx['id']
      wafers = params[:wafers] || lotctx[:Wafer].collect {|e| e['id']}
      reportingTimeStamp = dcr.context['reportingTimeStamp']
      comment = params[:comment] || ''
      $log.info "verify_aqv_message #{result.inspect}, lot: #{lot.inspect}"
      if @flags[:aqv_inline_active] == false && !['TEST', nil].member?(lotctx['technology'])
        $log.info 'skipped because aqv_inline_active flag is false'
        return true
      end
      #
      @_msgs = []  # for debugging only
      tend = (params[:tstart] || Time.now) + @aqv_timeout
      msgs = params[:msgs]    # messages are provided and must not be read, e.g. SIL05#test26
      msgidx = 0
      while msgs || msgidx.zero? || Time.now < tend # useless but harmless if msgs are provided, ensure to run at least once
        if msgs
          msg = msgs[msgidx] || ($log.warn '  no matching message'; return)  # get one of the provided msgs until nothing left, no pop!!
          msgidx += 1
        else
          msg = @mqaqv.receive_msg(timeout: @aqv_timeout) || ($log.warn '  no SIL->AQV message received'; return)
        end
        @_msgs << msg
        export = (params[:export] || "#{@logdir}/#{[tclogname, 'aqv', params[:exporttag]].compact.join('_')}.xml").tr('<>() ', '')
        File.binwrite(export, msg)
        r = XMLHash.xml_to_hash(msg)
        # skip unrelated messages
        ($log.debug '  old timestamp, skipping'; next) if r[:event][:timestamp] < reportingTimeStamp
        ($log.info "  skipping msg for lot #{r[:event][:lotId]}" if msgs.nil?; next) if lot != r[:event][:lotId]
        # wrong wafers in message, return
        if r[:event][:wafers]
          if msgscribe = r[:event][:wafers][:scribe]
            msgscribe = [msgscribe] if msgscribe.instance_of?(String)
            ($log.warn "  wrong wafers: #{msgscribe.sort}"; return) if wafers.sort != msgscribe.sort
          end
        end
        # msg found, verify
        msgresult = r[:event][:result]
        msgcomment = r[:event][:comment]
        if msgresult == 'success'
          return true if result == msgresult && msgcomment.nil?
        else
          ($log.info "  wrong comment: #{msgcomment}"; return) unless msgcomment.include?(comment)
          return true if result == msgresult && msgcomment.include?(comment)
        end
        $log.warn " wrong result: #{msgresult}"
        return false
      end
    end

    # return matching messages or nil if not enough matching messages found
    def wait_notification(text, params={})
      $log.info "wait_notification #{text.inspect}, #{params}"
      ($log.warn '  skipped by configuration'; return true) if @notifications == false
      count = params[:n] || 1
      qparams = {tstart: params[:tstart] || (@client.txtime - 2), tend: params[:tend],
                 filter: {'Application'=>@notification_application}}
      msgs = nil
      res = wait_for(timeout: params[:timeout] || @notification_timeout, tstart: params[:tstart]) {
        msgs = @notifications.query(qparams) || return
        msgs.select! {|m| m['Message'] && m['Message'].include?(text)}
        msgs.size >= count
      }
      ($log.warn "  got #{msgs.size} msgs instead of #{count}"; return) if msgs.size != count
      @_msgs = msgs  # for debugging only
      return msgs
    end

    # wait for inhibits to be set by a CA, return inhibits or nil if no matching inhibit found
    def wait_inhibit(classname, objs, memo, params={})
      $log.info "wait_inhibit #{classname.inspect}, #{objs.inspect}, #{memo.inspect}, #{params}"
      count = params[:n] || 1
      inhs = nil
      wait_for(timeout: params[:timeout] || @ca_timeout, tstart: params[:tstart]) {
        inhs = @sv.inhibit_list(classname, objs)
        inhs.select! {|inh| inh.memo.include?(memo)} if memo != ''
        inhs.size == count
      }
      ($log.warn "  got #{inhs.size} inhibits instead of #{count}"; return) if inhs.size != count
      return inhs
    end


    # wait for future hold to be set by a CA, return future holds, filtered by claim memo or nil
    # if memo is an array any memo is considered valid
    def wait_futurehold(memo, params={})
      $log.info "wait_futurehold #{memo.inspect}, #{params}"
      count = params[:n] || 1
      holds = nil
      res = wait_for(timeout: params[:timeout] || @ca_timeout, tstart: params[:tstart]) {
        lot = params[:lot] || @lot
        if memo.instance_of?(Array)
          holds = @sv.lot_futurehold_list(params[:lot] || @lot).select {|h|
            memo.find {|m| h.memo.include?(m)}
          }
        else
          holds = @sv.lot_futurehold_list(params[:lot] || @lot, memo: memo)
        end
        holds.size == count
      }
      ($log.warn "  got #{holds.size} future holds instead of #{count}"; return) if holds.size != count
      return holds
    end

    # was: verify_lothold_released
    # wait for lothold release, filtered by claim mmemo, are released by a CA, return true on success
    def wait_lothold_release(memo, params={})
      $log.info "wait_lothold_release #{memo.inspect}"
      return wait_for(timeout: params[:timeout] || @ca_timeout, tstart: params[:tstart]) {
        @sv.lot_hold_list(params[:lot] || @lot, memo: memo).empty?
      }
    end

    # set up clean channels, send a process DCR and a NOOC or OOC meas DCR incl verification; return meas DCR
    def cleanup_send_dcrs(readings, params={})
      # setup channels incl. state and CAs
      cparms = params.select {|k, v| [:mpd, :ppd, :department, :technology, :parameters, :processing_areas, :cas, :state, :exceptState, :nsamples].member?(k)}
      channels = setup_channels(cparms) || return
      sleep 10  # separate DCRs for clean AQV messages
      # remove old AQV messages, clean up eqp and lot
      @mqaqv.delete_msgs(nil).nil? && return
      @sv.eqp_cleanup(@ptool).zero? || return
      lot = params[:lot] || @lot
      #####(@sv.lot_cleanup(lot).zero? || return) if lot && @sv.lot_exists?(lot)
      # send process DCR and verify
      cparms = params.select {|k, v| [:ptool, :lot, :lots, :ppd, :department, :technology, :parameters, :processing_areas, :lrecipe, :mrecipe, :nsamples].member?(k)}
      submit_process_dcr(cparms) || return
      # send meas DCR with NOOC or OOC values and verify
      cparms = params.select {|k, v| [:mtool, :lot, :lots, :mpd, :department, :technology, :parameters, :lrecipe, :mrecipe, :nsamples].member?(k)}
      cparms[:nsamples] = 0 if params[:state] == 'Archived'  # verify_dcr_accepted calculates nsamples, force 0 if Archived
      cparms[:readings] = readings
      return submit_meas_dcr(cparms)
    end

    # was: Test_Space_Setup#create_clean_channels
    ### delete parameter channels,
    # send a process and a meas DCR with default non-ooc readings to create clean channels with CKC,
    #   and assign CAs, return array of channels
    def setup_channels(params={})
      department = params[:department] || @department
      technology = params[:technology] || @technology
      parameters = params[:parameters] || @parameters
      paramfirstelement = params[:paramfirstelement] || 0
      paramlastelement = params[:paramlastelement] || -1
      $log.info "setup_channels #{{technology: technology, department: department}.merge(params)}"
      # # delete _all_ channels for the parameters
      # $log.info "deleting all parameter channels"
      # [@lds_inline, @lds_setup].each {|lds|
      #   parameters.each {|p| lds.spc_channels(name: "#{p}*").each {|ch| ch.delete(silent: true)}}
      # }
      if department != 'LIT'
        # send process DCR and verify
        cparms = params.select {|k, v| [:ptool, :lot, :ppd, :department, :technology, :parameters, :processing_areas].member?(k)}
        submit_process_dcr(cparms) || return
      end
      # send meas DCR with default NOOC readings and verify
      cparms = params.select {|k, v| [:mtool, :lot, :mpd, :department, :technology, :parameters, :nsamples].member?(k)}
      cparms[:mpd] = @mpd_lit if department == 'LIT'  # special LIT MPD
      submit_meas_dcr(cparms) || return
      # get parameter channels
      lds = technology == 'TEST' ? @lds_setup : @lds_inline
      folder = lds.folder("AutoCreated_#{department}") || ($log.warn "no folder #{lds} 'AutoCreated_#{department}'"; return)
      channels = parameters[paramfirstelement..paramlastelement].collect {|p| folder.spc_channel(p) || ($log.warn "  no channel for parameter #{p}"; return)}
      @_channels = channels  # for debugging only
      # assign CAs, pass cas: [] for no CAs
      assign_verify_cas(channels, params[:cas] || @cas) || return
      # set channels' state
      state = params[:state] || 'Offline'
      ok = channels[0..-2].inject(true) {|ok, ch| ok && ch.set_state(state)}
      ok &= channels[-1].set_state(params[:exceptState] || state)
      ($log.warn 'cannot set channel state'; return) unless ok
      return channels
    end

    # assign corrective actions to one or more channels, verify and set the channel stae Offline; return true on success
    def assign_verify_cas(channels, cas)
      channels = [channels] unless channels.instance_of?(Array)
      cas = [cas] unless cas.instance_of?(Array)
      $log.info "assign_verify_cas #{cas}"
      channels.each {|ch|
        ch.set_cas(cas)
        val = ch.valuations.first || ($log.warn 'no channel valuations defined, check channel and template'; return)
        aa = val.cas.collect {|a| a.name}
        ($log.warn '  error setting CAs'; return) if cas.sort != aa.sort
        ch.set_state('Offline')  # really required ??
      }
      return true
    end

  end
end
