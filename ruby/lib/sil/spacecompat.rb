
class Java::ComCamlineSpaceApi::SPCChannelConfig
  java_alias :controllimits_config, :getControlLimitsConfig, []
  java_alias :set_controllimits_config, :setControlLimitsConfig, [Java::ComCamlineSpaceApi::ControlLimitsConfig]
  java_alias :valuations, :getAttachedValuations, []
  java_alias :set_valuations, :setAttachedValuations, [[].to_java(Java::ComCamlineSpaceApi::Valuation).java_class]
end