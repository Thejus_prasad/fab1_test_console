=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.


Author: Steffen Steidten, 2011-05-13, 2017-07-18 (as sildcr.rb)

History:
  2012-01-06 ssteidte, refactored to use the new Space::DCR class for creation of DCRs
  2012-11-19 ssteidte, fixed errors in runId determination (due to previous changes in xml_to_hash)
  2016-12-07 sfrieske, use lib rqrsp_mq instead of rqrsp
  2017-07-11 sfrieske, separated Exerciser into dcr_exerciser.rb
  2017-07-12 sfrieske, moved txtime to rqrsp_mq, removed obsolete send_dcr_file
  2017-07-26 sfrieske, refactored from spacedcr.rb
  2020-06-17 sfrieske, added optional MetaInfo for Parameter tags
  2021-02-05 sfrieske, use instance_of instead of kind_of
  2021-08-30 sfrieske, moved xmlhash.rb to util
=end

require 'time'
require 'util/xmlhash'


module SIL

  # Create Space DCRs
  class DCR
    constants.each {|c| remove_const(c)}
    # METROLOGY and RETICLE don't store the wafer history for processing areas and sub processing areas, see MSR 529229
    EqpTypes = {nil=>'METROLOGY', chamber: 'PROCESS-CHAMBER'}  # unused: reticle: 'RETICLE', process: 'PROCESS', batch: 'PROCESS-BATCH', etc.
    DefaultLot = 'SPCDUMMY0.00'
    # the following defaults must match spec setup!
    DefaultDepartment = 'QA'
    DefaultTechnology = '32NM'
    DefaultProductGroup = 'QAPG1'
    DefaultProduct = 'SILPROD.01'
    DefaultRoute = 'SIL-0001.01'
    DefaultOperation = 'QA-MB-FTDEPM.01'  ## 'MGT-NMETDEPDR.QA'   # cf SpecLimits_ITDC.SpecID.xlsx
    DefaultWaferValues = [40, 45, 50]     # around spec TARGET 45 (LSL: 25, USL: 75)
    DefaultParameters = ['QA_PARAM_900', 'QA_PARAM_901', 'QA_PARAM_902', 'QA_PARAM_903', 'QA_PARAM_904']

    attr_accessor :eqp, :tlocations, :context, :parameters  #, :ts


    def initialize(params={})
      ##@ts = Time.now.utc.iso8601(3)  # TODO: use case?  use @context['reportingTimeStamp']
      dcp_armor_path = params[:dcp_armor_path] || '/Job DCP/DCP Repository/SEM-AMAT/C-ALL'
      dcp_plan_name = params[:dcp_plan_name] || 'SEM-YM'
      dcp_plan_version = params[:dcp_plan_version] || '11'
      @eqp = params.has_key?(:eqp) ? params[:eqp] : 'QAEqp1'
      @eqptype = EqpTypes[params[:eqptype]]
      @context = {
        'baselineVersion'=>'f36cei8.2. - patch 3',
        'dcpArmorPath'=>[dcp_armor_path, dcp_plan_name, dcp_plan_version].join('/'),
        'dcpPlanName'=>dcp_plan_name, 'dcpPlanVersion'=>dcp_plan_version,
        'description'=>'DCC DCP Version AutoOnly', 'equipmentModelName'=>'SEMVISIONG3', 'equipmentModelVersion'=>'1.0',
        'eventReportingName'=>params[:event] || 'C-DR-SEM', 'reportingTimeStamp'=>nil, 'schemaVersion'=>'DCR_2_2_0SPACE',
        Equipment: {
          'controlState'=>'ONLINE-REMOTE', 'description'=>'POLISHER', 'equipmentReportedE10State'=>'PRD',
          'id'=>@eqp, 'modelNumber'=>'POLPOL', 'name'=>'POLISHER', 'softwareRevision'=>'1.0.10', 'type'=>@eqptype,
          Port: {
            'accessMode'=>'AUTO', 'associationState'=>'NOT ASSOCIATED', 'equipmentPortID'=>'1',
            'id'=>'P1', 'loadPortTransferState'=>'READY TO LOAD', 'locationType'=>'PORT', 'occupiedState'=>'NOT OCCUPIED',
            'reservationState'=>'NOT RESERVED', 'siviewPortMode'=>'Semi-Start-3', 'siviewPortState'=>'LoadReq',
            'trackingLocation'=>'11', 'type'=>'INPUT OUTPUT'
          },
          ProcessingArea: [],
          Component: [
            {'id'=>'LLK_OUT', 'location'=>'4', 'trackingLocation'=>'4', 'type'=>'OTHER'},
            {'id'=>'LLK_OUT', 'location'=>'3', 'trackingLocation'=>'3', 'type'=>'OTHER'},
            {'id'=>'LLK_OUT', 'location'=>'2', 'trackingLocation'=>'2', 'type'=>'OTHER'},
          ]
        },
        JobSetup: nil
      }
      @tlocations = {'LLK_OUT_2'=>2, 'LLK_OUT_3'=>3, 'LLK_OUT_4'=>4}
      @parameters = []
      ##@context['reportingTimeStamp'] = nil  # or Time.now.utc.iso8601(3) ? set in to_xml anyway
      ##reporting_timestamp(params[:timestamp])
      chambers = params[:chambers] || ['PM1', 'PM2', 'PM3']
      chambers = [chambers] if chambers.instance_of?(String)
      chambers = chambers.keys if chambers.instance_of?(Hash)  # for backwards compat with old spacedcr setup, TODO: remove
      chambers.each {|name| add_eqp_processing_area(name)}
      ##unless params[:jobsetup] == false   # TODO: jobsetup: false in Test_Space16 only, use dcr.context.delete(:JobSetup) instead
      @context[:JobSetup] = {}
      if params[:sapjob]   # TODO: Test_Space15S and Test_Space16 only, really used?
        @context[:JobSetup][:SAPJob] = {
          inspectionCharacteristicID: params[:sapicid] || '0010', orderOperation: params[:sapoo] || '0095',
          SAPdataCollectionPlanID: params[:sapdcpid] || '60012916', SAPOrderNumber: params[:sapon] || '67853957'
        }
      end
      ## @tlocations[carrier] = 11  # useful ??
      @context[:JobSetup][:SiViewControlJob] = {'id'=>params[:cj] || "#{@eqp}-#{Time.now.to_i}", 'siviewUserID'=>params[:user] || '',
        :FOUP=>[{
          'carrierAccessingState'=>'NOT ACCESSED', 'idStatusState'=>'ID NOT READ', 'location'=>'P1', 'locationType'=>'PORT',
          'scheduledLoadPort'=>'P1', 'scheduledUnloadPort'=>'P1', 'slotMapState'=>'SLOT MAP NOT READ', 'id'=>params[:carrier] || 'DummyFoup',
          foupHistory: {'enterTime'=>(Time.now-100).utc.iso8601(3), 'location'=>'P1', 'locationType'=>'PORT', 'trackingLocation'=>'11'},
          Lot: []
        }]
      }
      $log.info self.inspect
    end

    def inspect
      "#<#{self.class.name} eqp: #{@eqp}, eqptype: #{@eqptype}>"
    end

    # set content parts, used internally and in Test_Space22  # TODO: use case?
    def add_eqp_processing_area(name)
      location = @tlocations.values.last + 1
      ispa = location * 10
      spas = ['_SIDE1', '_SIDE2'].collect {|side|
        ispa += 1
        spa = name + side
        @tlocations[spa] = ispa
        {'id'=>spa, 'locationType'=>'SUBPROCESSINGAREA', 'processState'=>'IDLE', 'trackingLocation'=>ispa, 'type'=>'ChamberSide'}
      }
      @tlocations[name] = location
      pa = {'id'=>name, 'locationType'=>'PROCESSINGAREA', 'processingAreaE10State'=>'PRD',
        'processState'=>'IDLE', 'trackingLocation'=>location, 'type'=>'ProcessingArea', SubProcessingArea: spas}
      @context[:Equipment][:ProcessingArea] << pa
    end

    def add_lot(lot, params={})
      lot = DefaultLot if lot == :default
      $log.info "add_lot #{lot.inspect}, #{params.reject {|k, v| v.nil?}}"
      # migration support:
      ($log.warn "!! DEPRECATED parameter name #{params.keys}"; return) unless ([:logical_recipe, :machine_recipe] & params.keys).empty?
      #
      armor_repository = params[:armor_path] || '/Job Recipes/Recipe Repository/SEM-AMAT/C-P-SEM'
      armor_recipe = params[:armor_recipe] || '4710B MGT-NMETDEPDR'
      recipe_version = params[:recipe_version] || '2'
      mrecipe = params[:mrecipe] || 'C-P-SEM-G4.4710B-MGT-NMETDEPDR.01'
      lots_context(icarrier: params[:icarrier]) << {
        'armorPath'=>[armor_repository, armor_recipe, recipe_version].join('/'),
        'armorRecipeID'=>armor_recipe, 'armorRecipeVersion'=>recipe_version,
        'department'=>params[:department] || DefaultDepartment,
        'customer'=>params[:customer] || 'Customer',
        'id'=>lot,
        'logicalRecipeID'=>params[:lrecipe] || 'C-P-4710B-MGT-NMETDEPDR.01',
        'engineeringRecipeSelect'=>params[:ers] || 'false',
        'lotType'=>params[:lottype] || 'Production',
        'machineRecipeID'=>mrecipe,
        'manufacturingLayer'=>params[:layer] || 'B',
        'monitorLotFlag'=>!!params[:monitorlotflag],
        'operationID'=>params[:op] || DefaultOperation,
        'operationName'=>params[:opName] || 'DEFECT REVIEW',
        'operationNumber'=>params[:opNo] || '2500.1000',
        'passCount'=>params[:passcount] || '1',
        'photoLayer'=>params[:photolayer] || 'DummyPL',
        'physicalRecipeID'=>mrecipe.split('.')[1],
        'productGroupID'=>params[:productgroup] || DefaultProductGroup,
        'productID'=>params[:product] || DefaultProduct,
        'productType'=>params[:producttype] || 'Wafer',
        'route'=>params[:route] || DefaultRoute,
        'subLotType'=>params[:sublottype] || 'PO',
        'technology'=>params[:technology] || DefaultTechnology,
        Wafer: [],
      }
      # return self
    end

    # add readings for each parameter
    ## no useful return value (was: return self)
    def add_parameters(names, readings, params={})
      names = DefaultParameters if names == :default || names.nil?
      return add_parameter(names, readings, params) if names.instance_of?(String)  # be tolerant :(
      names.each {|n| add_parameter(n, readings, params)}
      # return self
    end

    # add a new parameter with a hash of waferids and related values as reading
    def add_parameter(name, readings, params={})
      $log.info "add_parameter #{name.inspect}, #{readings.inspect}, #{params.reject {|p| params[p].nil?}}"
      # migration support:
      ($log.warn "!! DEPRECATED parameter name #{params.keys}"; return) unless ([:separate_lot, :reading_type, :value_type] & params.keys).empty?
      #
      ($log.warn "wrong name #{name.inspect}"; return) unless name.instance_of?(String)
      # find _last_ parameter entry (for multilot, SIL_05 ???,  shouldn't matter anyway)
      iparam = @parameters.rindex {|e| e['name'] == name}
      if iparam.nil? || params[:newinstance]  # add parameters for wafers of another lot as separate instance (SIL_05)
        iparam = @parameters.size
        @parameters << {'dcpRequestType'=>'Event', 'name'=>name, 'noCharting'=>!!params[:nocharting],
          'parameterValid'=>(params[:parameter_valid] != false), Reading: []}
      end
      add_readings(readings, params.merge(iparam: iparam))
      add_metainfo(name, params[:metainfo]) unless params[:metainfo].nil?
    end

    # add optional metainfo to a parameter specified by name
    # metainfo is an Array of Hashes, e.g. [{'key'=>'Error Code', 'value'=>'3', 'waferID'=>'XYZ'}]
    def add_metainfo(name, metainfo)
      parameter = @parameters.find {|e| e['name'] == name}
      parameter[:MetaInfo] = metainfo
    end

    # readings is a hash of waferids and related values
    ## no useful return value (was: return self)
    def add_readings(readings, params={})
      readingtype = params[:readingtype] || 'Wafer'
      cj = nil
      if params[:cj]
        cj = params[:cj]
      elsif @context[:JobSetup] && @context[:JobSetup][:SiViewControlJob]
        cj = @context[:JobSetup][:SiViewControlJob]['id']
      end
      iparam = params[:iparam] || -1
      eqp = params[:eqp] || @eqp  #@context[:Equipment][:id]
      rnumber = @parameters[iparam][:Reading].size + 1
      ppas = params[:processing_areas]  ## TODO: chambers?  pas !(?)
      ppas = [ppas] if ppas.instance_of?(String) && !ppas.start_with?('cycle_')
      pas = ppas
      pas = processing_areas if ppas.nil? || (ppas.instance_of?(String) && ppas.start_with?('cycle_'))
      pa = (ppas == 'cycle_parameter') ? pas[iparam % pas.size] : pas[0]
      vtype = params[:valuetype]
      #
      if readings.nil?
        # use DefaultWaferValues
        voffset = 0
        readings = nil
      elsif readings.kind_of?(Numeric)
        # offset to DefaultWaferValues passed
        voffset = readings
        readings = nil
      elsif readings.instance_of?(Symbol)
        # :default or :ooc passed
        voffset = readings == :default ? 0 : 100
        readings = nil
      end
      if readings.nil?
        # calculate waferid from actually used lotid and assign values like {'WAFERID1'=>44, 'WAFERID2'=>45, 'WAFERID3'=>46}
        # take the _last_ 8 digits to distinguish sublots
        lid = @context[:JobSetup][:SiViewControlJob][:FOUP][params[:icarrier] || -1][:Lot][params[:ilot] || -1]['id']
        prefix = lid.gsub('.', 'X').reverse[0..7]
        readings = Hash[DefaultWaferValues.each_with_index.collect {|v, i| ["#{prefix}%2.2dQA" % i, v + voffset]}]
      end
      # Hash either passed directly or calculated above
      readings.each_pair {|e, va|
        _params = params.clone
        if ppas == 'cycle_wafer'                # TODO: create readings explicitly in the test (?)
          pa = pas[(rnumber-1) % pas.size]
          _params[:processing_areas] = [pa]
        elsif ppas == 'cycle_parameter'         # TODO: create readings explicitly in the test (?)
          _params[:processing_areas] = pas
        end
        spa = params[:reading_spa] || subprocessing_areas(pa).first
        va = [va] unless va.instance_of?(Array)
        srand(1) #set the seed to have always the same sites
        va.each_with_index {|v, i|
          if ppas == 'cycle_reading'  ## TODO: pass processing_areas directly ! Test_Space16
            pa = pas[i % pas.size]
            spa = subprocessing_areas(pa).first
          end
          unless vtype
            vtype = 'unsigned integer' if v.class == Integer
            vtype = 'float' if v.class == Float
            vtype = 'string' if v.class == String
          end
          v = v.to_s if vtype == 'string'  # hack allowing to pass (default) numbers but make them strings in @parameters (for nreadings)
          reading = {'Equipment'=>eqp, 'ProcessingArea'=>pa, 'SubProcessingArea'=>spa,
            'readingNumber'=>rnumber, 'readingValid'=>(params[:reading_valid] != false),
            'receivedTimeStamp'=>Time.now.utc.iso8601(3), 'remeasuredFlag'=>!!params[:remeasured], 'value'=>v, 'valueType'=>vtype
          }
          reading.merge!('Site'=>i+1, 'SiteX'=>((rand-0.5) * 100000).round(2), 'SiteY'=>((rand-0.5) * 100000).round(2)) if params[:sites]
          reading['SiViewControlJob'] = cj if cj
          reading[readingtype] = e if e
          @parameters[iparam][:Reading] << reading
          rnumber += 1
          sleep 0.1 # to separate timestamps
        }
        ### sleep 1 # must be >=1 to have the new wafer sample time really different (??)
        add_wafer(e, _params) if e && (readingtype == 'Wafer')
      }
      # return self
    end

    def add_wafer(wafer, params={})
      icarrier = params[:icarrier] || -1
      ilot = params[:ilot] || -1
      pas = params[:processing_areas] || processing_areas
      pas = [pas] if pas.instance_of?(String)
      #
      wafers = @context[:JobSetup][:SiViewControlJob][:FOUP][icarrier][:Lot][ilot][:Wafer]
      waferids = wafers.collect {|w| w['id']}
      # return self if waferids.member?(wafer)
      return if waferids.member?(wafer)   # TODO: oes this case happen?
      npaevents = pas.inject(0) {|s, pa| s + subprocessing_areas(pa).size + 1}
      # XXX tend = params[:reading_timestamp] || (Time.now - 1) ## -1 ??
      # XXX tstart = tend - npaevents*2
      tend = params[:reading_timestamp] || Time.now
      tstart = tend - npaevents*2 - 1
      wloc = params[:location] || (waferids.size + 1)
      wh = [
        {'enterTime'=>tstart.utc.iso8601(3), 'location'=>wloc, 'locationType'=>'FOUP', 'state'=>'NEEDS PROCESSING', RecipeParameter: []},
        {'exitTime'=>(tstart+1).utc.iso8601(3), 'location'=>wloc, 'locationType'=>'FOUP', 'state'=>'NEEDS PROCESSING', RecipeParameter: []},
      ]
      tstart += 1
      pas.each {|pa|
        tstart += 1
        wh << {'enterTime'=>tstart.utc.iso8601(3), 'location'=>pa, 'locationType'=>'PROCESSINGAREA',
          'state'=>'IN PROCESS', 'trackingLocation'=>@tlocations[pa]
        }
        tstart += 1
        spas = params[:subprocessing_areas] || subprocessing_areas(pa)
        spas = [spas] if spas.instance_of?(String)
        spas.each {|spa|
          wh << {'enterTime'=>tstart.utc.iso8601(3), 'location'=>spa, 'locationType'=>'SUBPROCESSINGAREA',
            'state'=>'IN PROCESS', 'trackingLocation'=>@tlocations[spa]
          }
          tstart += 1
          wh << {'exitTime'=>tstart.utc.iso8601(3), 'location'=>spa, 'locationType'=>'SUBPROCESSINGAREA',
            'state'=>'IN PROCESS', 'trackingLocation'=>@tlocations[spa]
          }
        }
        tstart += 1
        wh << {'exitTime'=>tstart.utc.iso8601(3), 'location'=>pa, 'locationType'=>'PROCESSINGAREA',
          'state'=>'IN PROCESS', 'trackingLocation'=>@tlocations[pa]
        }
      }
      processed = params.has_key?(:processed) ? params[:processed] : true
      wh << {'enterTime'=>(tstart+1).utc.iso8601(3), 'location'=>wloc, 'locationType'=>'FOUP', 'state'=>'PROCESSED'} if processed
      wafers << {'id'=>wafer, WaferHistory: wh}
      # return self
    end

    def processing_areas
      @context[:Equipment][:ProcessingArea].collect {|pa| pa['id']}
    end

    def subprocessing_areas(paname)
      @context[:Equipment][:ProcessingArea].each {|pa|
        return pa[:SubProcessingArea].collect {|spa| spa['id']} if pa['id'] == paname
      }
      # nothing foud
      return []
    end

    # return array of lot contexts
    def lots_context(params={})
      return @context[:JobSetup][:SiViewControlJob][:FOUP][params[:icarrier] || -1][:Lot]
    end

    # return lot context, default for the last lot, optionally lot of wafer or lot by index
    def lot_context(params={})
      clots = lots_context(icarrier: params[:icarrier]) || return
      return clots[params[:ilot] || -1] if params[:ilot]
      return clots.find {|clot| clot['id'] == params['Lot']} if params['Lot']
      return clots.find {|clot| clot[:Wafer].find {|e| e['id'] == params['Wafer']}} if params['Wafer']
      return clots[-1]
    end

    # used in Test_Space07x  TODO: use directly!!
    def controljob
      @context[:JobSetup][:SiViewControlJob]['id']
    end

    # def ekeysOBS(params={})
    #   clot = lot_context(params) || return
    #   ret = {'Lot'=>clot['id'], 'Wafer'=>'-', 'Technology'=>clot['technology'], 'Route'=>clot['route'],
    #     'Product.EC'=>clot['productID'], 'ProductGroup'=>clot['productGroupID'],
    #     'Event'=>@context['eventReportingName'], 'Photolayer'=>clot['photoLayer']
    #   }
    #   ret.merge!('PLogRecipe'=>clot['logicalRecipeID'], 'PTool'=>@eqp,
    #     'PChamber'=>(processing_areas.join(':') || '-'), 'POperationID'=>clot['operationID']) if params[:responsiblepd]
    #   ret['Technology'] = '-' if [nil, ''].include?(ret['Technology'])
    #   if wafer = params['Wafer']  # TODO: seems to be redundant (?)
    #     wdata = clot[:Wafer].find {|e| e['id'] == wafer} || ($log.warn "no DCR ekey for #{params}"; return)
    #     ret['Wafer'] = wdata['id']
    #   end
    #   return ret
    # end
    #
    # # return an Hash as from Space::SpcApi::Sample.dkeys
    # def dkeysOBS(params={})
    #   ceqp = @context[:Equipment]
    #   cfoup = @context[:JobSetup][:SiViewControlJob][:FOUP][params[:icarrier] || -1]  # default: last
    #   clot = lot_context(params) || return
    #   ret = {'MTool'=>ceqp['id'], 'Foup'=>cfoup['id'], 'MOperationID'=>clot['operationID'], 'SubLotType'=>clot['subLotType']}
    #   wafer = params['Wafer']
    #   # get MSlot
    #   if wafer
    #     wdata = clot[:Wafer].find {|e| e['id'] == wafer} || ($log.warn "no DCR dkey for #{params}"; return)
    #     sdata = wdata[:WaferHistory].find {|e| e['locationType'] == 'FOUP' && !e['exitTime'].nil?}
    #     ret['MSlot'] = sdata['location'].to_s
    #   end
    #   # get MTime
    #   if p = params[:parameter]
    #     dcrparam = @parameters.find {|e| e['name'] == p}
    #     if dcrparam
    #       reading = dcrparam[:Reading].find {|r| r['Wafer'] == wafer}   ##|| (r[:Lot] == clot)}
    #       ret['MTime'] = Time.parse(reading['receivedTimeStamp']) if reading
    #     end
    #   end
    #   return ret
    # end

    # return a Hash as from SIL::SpcApi::Sample.ekeys
    def ekeys(params={})
      mapping = [['Lot', 'lotid'], ['Wafer', 'waferid'], ['Technology', 'technology'], ['Route', 'mainprocess'],
        ['Product.EC', 'product'], ['ProductGroup', 'productgroup'], ['Event', 'event'], ['Photolayer', 'photolayer']]
      mapping += [['Wafer', 'waferid']] if params['Wafer']  # pass-through wafer ID (else '-')  TODO seems to be redundant
      ret = gkeys(params.merge(mapping: mapping)) || return
    end

    # return a Hash as from SIL::SpcApi::Sample.dkeys
    def dkeys(params={})
      mapping = [['MTool', 'equipment'], ['Foup', 'foup'], ['MOperationID', 'process'], ['SubLotType', 'sublottype'], ['MSlot', 'slot']]
      ret = gkeys(params.merge(mapping: mapping)) || return
      if wafer = params['Wafer']
        if p = params[:parameter]
          dcrparam = @parameters.find {|e| e['name'] == p}
          if dcrparam
            reading = dcrparam[:Reading].find {|r| r['Wafer'] == wafer}
            ret['MTime'] = Time.parse(reading['receivedTimeStamp']) if reading
          end
        end
      else
        ret.delete('MSlot')  # don't return key with invalid value, TODO: return '-' ??
      end
      return ret
    end

    # return a Hash mapping DB parameter names to DCR elements, optionally filtered and remapped by the mapping hash {newkey: parametername}
    def gkeys(params={})
      cfoup = @context[:JobSetup][:SiViewControlJob][:FOUP][params[:icarrier] || -1]  # default: last
      clot = lot_context(params) || ($log.warn "no DCR ekey for #{params}"; return)
      # key names are from the generic key table (DB, spec)
      ret = {
        nil=>'-',  # for mappings to nil (no mapping in GenericKey spec) e.g. [['Reserve1', 'equipment'], ['Reserve2', nil], ..]
        'dcparmorpath'=>@context['dcpArmorPath'],
        'dcparmorpathwithoutversion'=>File.dirname(@context['dcpArmorPath']),
        'dcpname'=>@context['dcpPlanName'],
        'event'=>@context['eventReportingName'],
        'timestampstring'=>@context['reportingTimeStamp'],
        'equipment'=>@context[:Equipment]['id'],
        'port'=>@context[:Equipment][:Port]['equipmentPortID'],
        'PToolChamber'=>@context[:Equipment]['id'] + '-' + processing_areas.join(':'),
        'PE10State'=>'SBY.2WPR' + '|' + processing_areas.collect {|pa| 'SBY.2WPR'}.join(':'),
        'processingarea'=>processing_areas.join(':') || '-',
        'subprocessingarea'=>processing_areas.collect {|pa| subprocessing_areas(pa)}.flatten.join(':'),
        'PChamberSeq'=>processing_areas.collect {|pa| pa + ':' + subprocessing_areas(pa).join(':')}.join(':'),
        'facility'=>'-',
        'controljob'=>@context[:JobSetup][:SiViewControlJob]['id'],  # || '-' ?
        'foup'=>cfoup['id'],
        'customer'=>clot['customer'],
        'department'=>clot['department'] || '-',
        'logicalrecipe'=>clot['logicalRecipeID'],
        'lotid'=>clot['id'],
        'maskedlotid'=>clot['id'].split('.').first + '.',   # strange trailing dot
        'mainprocess'=>clot['route'],
        'machinerecipe'=>clot['machineRecipeID'],
        'manufacturinglayer'=>clot['manufacturingLayer'],
        'operationname'=>clot['operationName'],
        'operationnumber'=>clot['operationNumber'],
        'operationpasscount'=>clot['passCount'],
        'operator'=>'-',
        'photolayer'=>clot['photoLayer'],
        'process'=>clot['operationID'],
        'product'=>clot['productID'],
        'productgroup'=>clot['productGroupID'],
        'reticle'=> '-',   # src for reticle ?
        'rmsrecipe'=>clot['armorRecipeID'],
        'sublottype'=>clot['subLotType'],
        'technology'=>clot['technology'],
        'waferid'=>'-'  # default for ekeys
      }
      ret['technology'] = '-' if [nil, ''].include?(ret['technology'])  # 'fix' for intentionally sent corrupt DCRs
      # this is for SIL_15 tests only!! TODO: review FIXED tests in SIL_15 !!
      ret['Event'] = ret['event']
      ret['Foup'] = ret['foup']
      ret['MOperationID'] = ret['process']
      ret['MTool'] = ret['equipment']
      ret['PMachineRecipe'] = ret['machinerecipe']
      ret['POperation'] = ret['process']
      ret['Reticle'] = ret['reticle']
      ret['RouteProgramID'] = '-'
      ret['SubLotType'] = ret['sublottype']
      ret['Tool'] = ret['equipment']
      #
      if wafer = params['Wafer']  # attn: 'Wafer' parameter influences lot_context above!
        wdata = clot[:Wafer].find {|e| e['id'] == wafer} || ($log.warn "no DCR dkey for #{params}"; return)
        sdata = wdata[:WaferHistory].find {|e| e['locationType'] == 'FOUP' && !e['exitTime'].nil?}
        ret['slot'] = sdata['location'].to_s
        ret['waferid'] = wafer
        ret['WaferVendorID'] = wafer[8..9]  # for SIL_51, advanced props wafervendorid.scriberange=8-9
      end
      sapjob = @context[:JobSetup][:SAPJob]
      if sapjob
          ret['sapdcpid'] = sapjob['SAPdataCollectionPlanID']
          ret['sapordernumber'] = sapjob['SAPOrderNumber']
          ret['saporderoperation'] = sapjob['orderOperation']
          ret['sapinspectioncharacteristicid'] = sapjob['inspectionCharacteristicID']
      end
      empty = params[:empty] || []
      ##empty = [empty] if empty.instance_of?(String)
      empty.each {|k| ret[k] = '-' if ret.has_key?(k)}
      mapping = params[:mapping]
      return ret unless mapping
      # mapping is an array of arrays like [['Reserve1', 'customer'], ..], mapping sample e/dkeys to DB key names
      retmapped = {}
      mapping.each {|e| retmapped[e[0]] = ret[e[1]]}
      return retmapped
    end

    # return number of valid readings, all readings if all: true
    def nreadings(params={})
      n = 0
      @parameters.each {|p|
        if params[:all] || !p['noCharting'] && (p['parameterValid'] != false)
          p[:Reading].each {|r| n += 1 if params[:all] || r['readingValid'] && r['value'].kind_of?(Numeric)}
        end
      }
      return n
    end

    # return all valid numeric readings, all readings if all: true, for a parameter
    def readings(parameter, params={})
      rtype = params[:readingtype] || 'Wafer'
      ret = []
      @parameters.each {|p|
        next if p['name'] != parameter
        if params[:all] || !p['noCharting'] && (p['parameterValid'] != false)
          p[:Reading].each {|r|
            if params[:all] || r[rtype] && r['readingValid'] && r['value'].kind_of?(Numeric)
              ret << [r[rtype], r['value']]
            end
          }
        end
      }
      return Hash[ret]  # if keys are not unique, like for multiple parameters, the duplicates get dropped!
    end

    # return DCR as XML document
    def to_xml(params={})
      @context['reportingTimeStamp'] = Time.now.utc.iso8601(3) ### unless params[:keep_timestamp]
      h = {'SOAP-ENV:Envelope'=>{
        'xmlns'=>'http://www.amd.com/schemas/DataCollectionReport/DCR_2_2_0SPACE',
        'xmlns:SpaceSubmitRunPort'=>'http://www.globalfoundries.com/space',
        'xmlns:SOAP-ENV'=>'http://schemas.xmlsoap.org/soap/envelope/',
        :'SOAP-ENV:Body'=>{:'SpaceSubmitRunPort:submitRun'=>{
            'SOAP-ENV:encodingStyle'=>'http://schemas.xmlsoap.org/soap/encoding/',
            in0: {Context: @context, Parameter: @parameters}
        }}
      }}
      return XMLHash.hash_to_xml(h)
    end

  end

end
