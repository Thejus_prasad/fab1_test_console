=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.


Author: Steffen Steidten, 2011-05-13, 2017-07-18 (as sil.rb)

History:
  2012-01-06 ssteidte, refactored to use the new Space::DCR class for creation of DCRs
  2012-11-19 ssteidte, fixed errors in runId determination (due to previous changes in xml_to_hash)
  2016-12-07 sfrieske, use lib rqrsp_mq instead of rqrsp
  2017-07-11 sfrieske, separated Exerciser into dcr_exerciser.rb
  2017-07-12 sfrieske, moved txtime to rqrsp_mq, removed obsolete send_dcr_file
  2021-02-05 sfrieske, use instance_of instead of kind_of
  2021-08-27 sfrieske, split verify_hash off of misc.rb
=end

require 'remote'
require 'util/verifyhash'


module SIL
  # Access SIL server config
  class Server < RemoteHost
    attr_accessor :env, :advanced_props  #, :spisepp_props


    def initialize(env, params={})
      @env = env
      super("sil.#{env}", params)
      @propsdir = params[:propsdir] || '/amdapps/inspace/sil/sil/standalone/configuration/sil'
    end

    def read_jobprops
      @advanced_props = read_propsfile(File.join(@propsdir, 'advanced.properties'))
    end

    # def read_spiseppprops
    #   @spisepp_props = read_propsfile(File.join(@propsdir, 'spisepp.properties'))
    # end

    def verify_properties(params={})
      $log.info "verify_properties"
      ref = params[:ref] || File.join('./testparameters', @env, 'sil/advancedprops.yaml')
      ref = YAML.load_file(ref) if ref.instance_of?(String)
      return verify_hash(ref, read_jobprops || return)
    end

    def verify_property(name, value)
      $log.info "verify_property #{name.inspect}, #{value.inspect}"
      @advanced_props ||= read_jobprops
      prop = @advanced_props[name] || ($log.warn '  no such property'; return)
      ($log.warn "  wrong value, #{prop.inspect} instead of #{value.inspect}"; return) if prop != value.to_s
      return true
    end

  end

end
