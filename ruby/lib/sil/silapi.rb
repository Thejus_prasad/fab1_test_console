=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.


Author: Steffen Steidten, 2011-11-23

History:
  2012-08-31 ssteidte, added subfolder and organizational folder search, e.g. $lds.folder("/28BK/SHELBY")
  2012-09-03 dsteger,  added support for event folders
  2015-07-02 ssteidte, added support for server timezone (e.g. in Sample)
  2016-05-23 sfrieske, fixed get_limit_value for JRuby 9.1
  2016-09-02 sfrieske, Session.initialize honours all parameters from config file
  2017-07-20 sfrieske, refactored from spaceapi.rb
  2021-01-05 sfrieske, minor cleanup in SIL::SpaceApi::init
  2021-02-05 sfrieske, use instance_of instead of kind_of
  2021-04-08 aschmid3, fix permanent deletion in delete_channels, enhanced Folder#spc_channels to wildcard (*)
  2021-08-26 sfrieske, use read_config instead of _config_from_file
  2021-08-27 sfrieske, re-implemented silent: true for delete_channels
=end

require 'base64'
require 'time'
require 'corba'
require 'util/readconfig'

# get english language for ca messages, since Space 7.1
java.util.Locale.setDefault(java.util.Locale::ENGLISH)
ENV_JAVA['log4j.configurationFile'] = 'etc/log4j2.properties'
ENV_JAVA['java.awt.headless'] = 'true'


module SIL
  module SpaceApi
    constants.each {|c| remove_const(c)}
    CalculatedParam = Struct.new(:name, :param, :formula, :trigger, :refs)
    SpcMovingValue = Struct.new(:ma, :ms, :ewma_mean, :ewma_range, :ewma_sigma)
    SpcTriggerKey = Struct.new(:keyid, :name, :value)

    # access module (i.e. session wide) variables
    def self.srvtimezone; @srvtimezone; end

    def self.session; @session; end

    def self.apicode; @apicode; end

    def self.limit_ids; @limit_ids; end

    def self.speclimit_ids; @speclimit_ids; end


    # dynamically load the libraries, return true on success
    def self.init(env, params={})
      props = read_config(env, 'etc/space.conf', params)
      @srvtimezone = props[:srvtimezone] || '+00:00'
      # load JAVA libs
      Dir["lib/space/#{props[:flavour] || 'inline72'}/*jar"].sort.each {|f| require f}
      require_relative 'spacecompat'  # why is this necessary on some machines and not on others??
      @apicode = Java::ComCamlineSpaceApi
      # initialize session
      @session.terminate if @session  # safe to call on an already terminated session
      pw = props[:cleartext] ? props[:password] : Base64.decode64(props[:password])
      sprops= props[:props] || "etc/space/#{env}.space.navigator.properties"
      @session = @apicode::SpaceAPISessionFactory.createSpaceAPISession(props[:user], pw, sprops)
      @session.setViewDeletedObjects(true)  # show objects marked for deletion
      @source = @session.getAllSourcesNamed(props[:ds]).first
      #
      # often used constants, arrays of [id, name] with id as integer from 0... (NUM_LIMITS is not a valid limit type)
      @limit_ids = @apicode::SampleLimits.constants.reject {|c| c == :NUM_LIMITS}.collect {|c|
        [@apicode::SampleLimits.const_get(c), c.to_s]
      }.sort
      @speclimit_ids = @apicode::SampleSpecLimits.constants.collect {|c|
        [@apicode::SampleSpecLimits.const_get(c), c.to_s]
      }.sort
      #
      $log.info self.inspect
      return true
    end

    def self.inspect
      "#<SIL::SpaceApi #{@source ? @source.name : 'no source'}>"
    end

    def self.lds(name)
      res = @source.getAllLDSNamed(name).first
      return res ? LDS.new(res) : nil
    end

    def self.ca(name)
      res = @session.getAllCorrectiveActionsNamed(name)
      return nil if res.empty?
      return CA.new(res.first) #if res.size == 1
      # LET has AutoLotHold for multiple divisions, filter for DEFAULT_DIVISION
      $log.info "CA #{name} is not unique, selecting DEFAULT_DIVISION"
      res.each {|e|
        ca = CA.new(e)
        return e if ca.division == 'DEFAULT_DIVISION'
      }
      return nil
    end

    def self.valuations
      @apicode::SPCChannelConfig.getAllDefinedValuations.collect {|e| Valuation.new(e)}
    end

    module SubFolders
      attr_accessor :javaobject

      def initialize(obj)
        @javaobject = obj
      end

      def folders(name=nil)
        return [] unless @javaobject
        name = name.split('/') if name.instance_of?(String)
        fromroot = (name[0] == '') if name
        n = name.pop if name
        # parent folders ?
        return org_folder(name).folders(n) if name && !name.empty?
        @javaobject.forget_children #cleanup existing data
        res = name ? @javaobject.get_all_lds_folders_named(n) : @javaobject.get_all_lds_folders
        return res.collect {|e|
          Folder.new(e) unless fromroot && e.folder_parent != @javaobject
        }.compact
      end

      def folder(name)
        ff = folders(name) || return
        ($log.info "#{self.inspect} no such folder: #{name}"; return) if ff.empty?
        ($log.warn "#{self.inspect} multiple folders: #{name}"; return) unless ff.size == 1
        return ff.first
      end

      def org_folders(name=nil)
        return [] unless @javaobject
        name = name.split('/') if name.instance_of?(String)
        fromroot = (name[0] == '') if name
        name.shift if fromroot
        n = name.shift
         @javaobject.forget_children #cleanup existing data
        res = name ?  @javaobject.get_all_organizational_folders_named(n) : @javaobject.get_all_organizational_folders
        ret = res.collect {|e|
          Folder.new(e) unless fromroot && e.folder_parent != @javaobject
        }.compact
        return ret if name.empty?
        # go to subfolder
        return ret[0].org_folders(name)
      end

      def org_folder(name)
        ff = org_folders(name) || ff
        return unless ff.size == 1
        return ff.first
      end

    end


    class LDS
      include SubFolders

      def inspect
        "#<#{self.class.name} #{@javaobject.config.name}>"
      end

      # get extractor keys
      def ekeys(params={})
        ekeys = @javaobject.config.extractor_keys.collect {|e|
          {keyid: e.get_id, name: e.name, desc: e.description, refonly: e.reference_only}
        }
      end

      def create_folder(name, params={})
        $log.info "create_folder #{name.inspect}, #{params.inspect}"
        tmpl = params[:template]
        if tmpl
          ff = folders(tmpl)
          ($log.warn "  template folder missing"; return) unless ff && ff.size == 1
          newcfg = ff.first.folder.config
        else
          newcfg = @javaobject.lds_folder_default_config
        end
        newcfg.name = name
        f = @javaobject.create_lds_folder(newcfg)
        return Folder.new(f)
      end

      # get all spc channels in lds matching name: (wildcard is *)
      # unfortunately Folder and LDS Java objects use different method names for the similar task
      def spc_channels(params={})
        @javaobject.get_all_spc_channels_named(params[:name] || '*').collect {|c|
          SpcChannel.new(c) unless c.is_marked_for_deletion && !params[:deleted]
        }.compact
      end

    end


    class Folder
      include SubFolders

      def inspect
        "#<#{self.class.name} #{@javaobject.lds.config.name}/#{@javaobject.config.name}>"
      end

      # delete all channels identified by parameters in this folder, return true on success
      def delete_channels(params={})
        $log.info "#{inspect} delete_channels #{params}" unless params[:silent]
        ret = true
        channels = spc_channels(params)
        unless channels.empty?
          $log.info "#{inspect} delete_channels #{params}" if params[:silent]
          $log.info "  deleting #{channels.size} channels"
          channels.each_with_index {|ch, i|
            $log.info "    ch ##{i}" if (i % 10 == 0) && (i != 0)
            res = ch.delete(params)
            ($log.warn "  error deleting channel #{ch.name} #{ch.parameter}"; ret=false) if res != 0
          }
        end
        channels = spc_channels(params.merge(deleted: true))
        $log.info "  finally deleting #{channels.size} channels" unless params[:silent] && channels.empty?
        unless channels.empty?
          channels.each_with_index {|ch, i|
            $log.info "    ch ##{i}" if (i % 10 == 0) && (i != 0)
            res = ch.delete(params)
            ($log.warn "  error deleting channel #{ch.name} #{ch.parameter}"; ret=false) if res != 0
          }
        end
        return ret
      end

      # delete all channels in the folder, then the folder, return 0 on success
      def delete
        delete_channels
        $log.info "folder #{@javaobject.lds.config.name}/#{@javaobject.config.name}: delete"
        @javaobject.delete
      end

      def name=(name, comment="QA Test #{Time.now.utc.iso8601}")
        cfg = @folder.config
        cfg.name = name
        @folder.set_config(cfg, comment)
        @name = @folder.config.name
      end

      # get all SPC channels matching the (optional) parameters
      #  (unfortunately Folder and LDS Java objects use different method names for the similar task)
      def spc_channels(params={})
        # exclude = params[:exclude]
        pname = params[:name]
        pparameter = params[:parameter]   # TODO: not useful, remove!
        ret = []
        @javaobject.get_all_spc_channels.each {|e|
          next if e.is_marked_for_deletion && !params[:deleted]
          # next if exclude && e.parameter_name.start_with?(exclude)  # useless unless e.config.name is meant
          if pname
            cfg = e.config
            if pname.end_with?('*')
              pname2 = pname.delete_suffix('*')
              next if !cfg.name.start_with?(pname2)
            else
              next if cfg.name != pname
            end
          end
          if pparameter # TODO: remove!
            if pparameter.end_with?('*')
              pparameter2 = pparameter.delete_suffix('*')
              next if !e.parameter_name.start_with?(pparameter2)
            else
              next if e.parameter_name != pparameter
            end
          end
          ret << SpcChannel.new(e)
        }
        return ret
      end

      # params is either a channel name, channel id or hash {key: value}, return channel if exactly one exists
      def spc_channel(params)
        params = {parameter: params} if params.instance_of?(String)   # was name, but parameter is the most often used selector
        chs = spc_channels(params) || return
        ($log.warn "multiple channels found for #{params}"; return) if chs.size > 1
        return chs.first
      end

      def create_spc_channel(name, parameter, unit, ckeys, params={})
        newcfg = @javaobject.spc_channel_default_config
        newcfg.name = name
        newcfg.parameter_name = parameter
        newcfg.parameter_unit = unit
        newcfg.description = params[:description] || ''
        newcfg.spc_channel_keys = ckeys.collect {|e| e.ckey}
        ch = @javaobject.create_spc_channel(newcfg) || return
        SpcChannel.new(ch)
      end

      # move the channel here by creating a new one from the current config and deleting the old one
      # note that samples are not copies(!); return new channel
      def move_spc_channel(ch)
        cfg = ch.channel.config
        ch.delete
        ch = @javaobject.create_spc_channel(cfg) || return
        SpcChannel.new(ch)
      end

      def calculated_parameters(params={})
        @javaobject.getAllCalculatedParameter.collect {|calc|
          c = calc.getConfig
          c_refs = (c.getReferences || []).collect {|r| r.getParameterName}
          CalculatedParam.new(c.name, c.param_name, c.formula_text, c.trigger.parameter_name, c_refs)
        }
      end

    end

    class SpcChannel
      attr_accessor :channel, :chid, :parameter, :unit

      def initialize(c)
        @channel = c
        @chid = @channel.channel_id
        @parameter = @channel.parameter_name
        @unit = @channel.parameter_unit
      end

      # returns channel data as hash, avoids clumsy constructs like ch.inspect.to_hash
      def to_hash
        cfg = @channel.config
        {chid: @chid, name: cfg.name, desc: cfg.description, parameter: @parameter, unit: @unit, division: cfg.division}
      end

      def name
        @channel.config.name
      end

      def name=(name)
        cfg = @channel.config
        cfg.name = name
        @channel.set_config(cfg, "QA test #{Time.now.utc.iso8601}", false)
      end

      # often used alone in autocharting tests
      def desc
        return @channel.config.description
      end

      def ckcid
        @channel.ckc_id
      end

      def config
        @channel.config
      end

      # TODO: return no nils, :enabled and :all are the same, remove :enabled
      def limits(params={})
        clc = @channel.config.controllimits_config
        return {} unless clc.isGlobalLimitsEnable || params[:all]
        a = []
        if params[:enabled]
          SIL::SpaceApi.limit_ids.each {|i, c| a << [c, clc.get_limit_value(i)] if clc.limit_enabled?(i)}
        elsif params[:fixed]
          SIL::SpaceApi.limit_ids.each {|i, c| a << [c, clc.limit_enabled?(i) ? clc.get_limit_value(i) : nil] if clc.engineering_limit?(i)}
        else
          SIL::SpaceApi.limit_ids.each {|i, c| a << [c, (params[:all] || clc.limit_enabled?(i)) ? clc.get_limit_value(i) : nil]}
        end
        Hash[a]
      end

      def deleted
        @channel.is_marked_for_deletion
      end

      def customer_fields
        cfields = @channel.config.getAttachedCustomerFields
        h = {}
        cfields.each do |cf|
          _type = cf.get_type
          _value = cf.get_value
          case _type
          when 5
            _value = _value.to_i
          when 6
            _value = _value.to_f
          when 1
            _value = (_value == 'true')
          when 2,3
            _value
          else
            $log.warn "unsupported type: #{_type}"
          end
          h[cf.get_name] = _value
        end
        return h
      end

      def set_customer_fields(fields)
        cfg = @channel.config
        _cfields = SIL::SpaceApi.apicode::SPCChannelConfig.getAllCustomerFields
        _nfields = []
        _cfields.each {|c|
          name = c.get_name
          if fields.has_key?(name)
            c.setValue(fields[name])
            _nfields << c
          end
        }
        cfg.setAttachedCustomerFields(_nfields)
        @channel.set_config(cfg, "QA test #{Time.now.utc.iso8601}", false)
      end

      def state
        return @channel.config.getCurrentSPCChannelState.state
      end

      def set_state(stat)
        cfg = @channel.config
        return true if cfg.getCurrentSPCChannelState.state == stat
        $log.info "channel #{@parameter}: set_state #{stat.inspect}"
        avail_states = cfg.class.getAvailableSPCChannelStates2
        stateobj = avail_states.find {|s| s.to_s == stat} || ($log.warn "  #{stat.inspect} is not available"; return)
        cfg.setCurrentSPCChannelState(stateobj)
        @channel.set_config(cfg, "QA test #{Time.now.utc.iso8601}", false)
        return (stat == state)
      end

      def set_limits(limits, params={})
        cfg = @channel.config
        clc = cfg.controllimits_config  # getControlLimitsConfig
        clc.setGlobalLimitsEnabled(true) unless clc.isGlobalLimitsEnable
        limits.each_pair {|limit, value|
          ll = SIL::SpaceApi.limit_ids.rassoc(limit) || ($log.warn "limit #{limit} not found"; return)
          l = ll[0]
          if value
            clc.setLimitEnabled(l, true)
            clc.setLimitValue(l, value)
          else
            clc.setLimitEnabled(l, false)
          end
          clc.setEngineeringLimit(l, params[:fixed]) if params[:fixed]
        }
        cfg.set_controllimits_config(clc)
        @channel.set_config(cfg, "QA test #{Time.now.utc.iso8601}", false)
        true
      end

      def global_limits_enabled(enable=true)
        cfg = @channel.config
        clc = cfg.controllimits_config
        clc.setGlobalLimitsEnabled(enable)
        cfg.set_controllimits_config(clc)
        @channel.set_config(cfg, "QA test #{Time.now.utc.iso8601}", false)
      end

      def global_limits_enabled?
        cfg = @channel.config
        clc = cfg.controllimits_config
        clc.isGlobalLimitsEnable
      end

      def division
        @channel.config.division
      end

      def division=(division)
        cfg = @channel.config
        cfg.division = division
        @channel.set_config(cfg, "QA test #{Time.now.utc.iso8601}", false)
      end

      # return 0 on success
      def delete(params={})
        if @channel.is_marked_for_deletion
          @channel.permanently_delete
        else
          @channel.delete(params[:cascade] != false, params[:samples] != false)
        end
      end

      # get array of Sample, optionally filtered
      def samples(params={})
        ret = []
        @channel.get_samples(params[:tstart], params[:tend], params[:count] || 0, params[:days] || 0, params[:flagged] != false).each {|e|
          s = Sample.new(e)
          ekeys = s.ekeys
          selkeys = ['Wafer', 'Lot', 'PTool', 'PChamber'] & params.keys
          ret << s if selkeys.inject(true) {|ok, k| ok && (ekeys[k] == params[k])}  # filter by ekeys (Lot, Wafer, PTool)
          # legacy selectors, TODO: remove!!
          oldkeys = [:wafer, :lot, :pchamber] & params.keys
          $log.warn "OBSOLETE sample selector: #{oldkeys}" unless oldkeys.empty?
          ret << s if params[:wafer] && ekeys['Wafer'] == params[:wafer]
          ret << s if params[:lot] && ekeys['Lot'] == params[:lot]
          ret << s if params[:chamber] && ekeys['PChamber'] == params[:chamber]
        }
        return ret
      end

      def sample(params={})
        ss = samples(params) || return
        ($log.warn "multiple samples found for #{params}"; return) if ss.size > 1
        return ss.first
      end

      # replaces old ch.samples(ckc: true)
      def ckc_samples(params={})
        return @channel.allCKCs.collect {|e| SpcChannel.new(e).samples(params)}.flatten
      end

      def valuations
        @channel.config.valuations.collect {|e| Valuation.new(e)}
      end

      def set_valuations(vv)
        cfg = @channel.config
        cfg.set_valuations(vv.collect {|v| v.valuation})
        @channel.set_config(cfg, "QA test #{Time.now.utc.iso8601}", false)
      end

      # SpcChannel

      def keys(params={})
        res = params[:all] ? @channel.config.all_spc_channel_keys : @channel.config.spc_channel_keys
        return res.collect {|e| {keyid: e.get_id, fixed: e.fixed_value, exclude: e.excluded_values}}
      end

      # set the channel extractor keys, with the set of passed keys, return array of the new effective keys    TODO: Test_Space70
      def keys=(newkeys)
        cfg = @channel.config
        ckeys = cfg.all_spc_channel_keys
        objs = newkeys.collect {|key|
          ckey = ckeys.find {|ck| ck.getId == key[:keyid]} || ($log.warn "wrong key: #{key}"; return)
          ckey.fixed_value = key[:fixed]
          ckey.excluded_values = key[:exclude]
          ckey
        }
        cfg.set_spc_channel_keys(objs)
        @channel.set_config(cfg, "QA test #{Time.now.utc.iso8601}", false)
      end

      # set events like CAs for all valuations or list of valuation ids
      def set_events(evs, params={})
        vv = valuations
        vv.each {|v|
          if (params[:valuations] && !params[:valuations].member?(v.vid))
            v.set_events([])
          else
            $log.debug {"set event #{evs.inspect}"}
            v.set_events(evs)
          end
        }
        set_valuations(vv)
      end

      # set corrective actions for all valuations in channel ch     # TODO: use case for params??
      def set_cas(canames, params={})
        canames = [canames] if canames.instance_of?(String)
        department = params[:department]
        division = params[:division]
        cas = canames.collect {|name|
          ca = SIL::SpaceApi.ca(name)
          next if department && department != ca.department
          next if division && division != ca.division
          ca
        }.compact
        set_events(cas, params)
      end

      def set_email_addresses(s)
        vv = valuations
        vv.each {|v| v.set_email_addresses(s)}
        set_valuations(vv)
      end

    end


    class Valuation
      attr_accessor :valuation, :vid, :name, :comment_req

      def initialize(v)
        @valuation = v
        @vid = v.id
        @name = v.name
        @comment_req = v.is_comment_required
      end

      def inspect
        "#<#{self.class.name} #{@name.inspect} (id: #{vid})>"
      end

      def parameters
        Hash[@valuation.parameter_num.times.collect {|i| [@valuation.parameter_name(i), @valuation.parameter_value(i)]}]
      end

      def cas(valuation={})
        @valuation.get_attached_valuation_events.collect {|e|
          CA.new(SIL::SpaceApi.session.get_corrective_action(e.id)) if e.type == 1
        }.compact
      end

      def events
        @valuation.getAttachedValuationEvents.to_a
      end

      # set attached valuation events from a list of Events or CAs
      def set_events(data)
        data = [data] unless data.instance_of?(Array)
        evs = data.collect {|e|
          e.ca.getValuationEventIdentifier()
        }
        @valuation.setAttachedValuationEvents(evs)
      end

      def email_addresses
        @valuation.email_addresses
      end

      def set_email_addresses(s)
        @valuation.email_addresses = s
      end

    end


    class CA
      attr_accessor :ca, :caid, :name, :text, :department, :division, :attributes

      def initialize(ca)
        @ca = ca
        @caid = ca.id
        @name = ca.config.name
        @text = ca.config.text
        @department = ca.config.department
        @division = ca.config.division
        @attributes = ca.config.attributes.to_a
      end
    end


    class TSG
      attr_accessor :tsg, :tsgid, :name, :department, :division, :filename

      def initialize(tsg)
        @tsg = tsg
        @tsgid = tsg.id
        @name = tsg.config.name
        @department = tsg.config.department
        @division = tsg.config.division
        @filename = tsg.config.file_name
      end
    end


    class Sample
      attr_accessor :sample, :sid, :iflag, :eflag, :excluded, :time

      def initialize(sample)
        @sample = sample
        _init
      end

      def inspect
        "#<#{self.class.name} #{@sid.inspect}>"
      end

      def flag
        @sample.flag
        _init
      end

      def unflag
        @sample.unflag
        _init
      end

      def _init
        @sid = @sample.to_s.to_i       # not working because of 32 bit limitation: get_id
        @iflag = @sample.internally_flagged
        @eflag = @sample.externally_flagged
        @excluded = @sample.excluded
        @time = Time.at(@sample.date.time/1000)
        nil
      end

      # return sample chart
      def _get_chart(params={})
        chart = params[:chart]
        unless chart
          cc = @sample.charts
          ($log.warn "no unique chart for sample #{sid}"; return) unless cc && cc.size == 1
          chart = cc.first
        end
        return chart
      end

      def parameter
        @sample.parameter_name
      end

      def unit
        @sample.parameter_unit
      end

      def ecomment
        @sample.external_comment
      end

      def ecomment=(s, params={})
        chart = _get_chart(params) || return
        @sample.setExternalComment(chart, s)
      end

      def icomment
        @sample.internal_comment
      end

      def icomment=(s)
        @sample.set_internal_comment(s)
      end

      # def lds  # use case?
      #   @sample.get_lds_id
      # end
      #
      def dkeys
        dkeys = Hash[@sample.data_key_names.zip(@sample.data_keys)]
        if dkeys.has_key?('MTime')
          mtime = dkeys['MTime']
          dkeys['MTime'] = (mtime == '-') ? nil : Time.strptime("#{mtime} #{SIL::SpaceApi.srvtimezone}", '%Y/%m/%d %H:%M:%S:%N %Z')
        end
        return dkeys
      end

      def ekeys
        Hash[@sample.extractor_key_names.zip(@sample.extractor_keys)]
      end

      def raws
        @sample.raws.collect {|e| RawValue.new(e)}
      end

      def moving_values(params={})
        chart = _get_chart(params) || return
        mv = @sample.get_moving_values(chart)
        SpcMovingValue.new(mv.ma, mv.ms, mv.ewma_mean, mv.ewma_range, mv.ewma_sigma)
      end

      def statistics
        st = @sample.get_statistics
        {'MEAN'=>st.mean, 'MIN'=>st.min, 'MAX'=>st.max, 'STDDEV'=>st.sigma, 'RANGE'=>st.range, 'MEDIAN'=>st.median, 'COUNT'=>st.sample_size}
      end

      def limits(params={})
        chart = _get_chart(params) || return
        l = @sample.get_limits(chart)
        Hash[SIL::SpaceApi.limit_ids.collect {|i, c| [c, (params[:all] || l.limit_enabled(i)) ? l.get_limit_value(i) : nil]}]
      end

      def specs(params={})
        l = @sample.get_specifications
        Hash[SIL::SpaceApi.speclimit_ids.collect {|i, c| [c, (params[:all] || l.limit_enabled(i)) ? l.get_limit_value(i) : nil]}]
      end

      def assign_ca(ca, params={})
        $log.info "<Sample #{@sample.parameter_name}> assign_ca #{ca.inspect}"
        begin
          vv = violations(params)
          ($log.warn "no violations for sample #{self}"; return) unless vv && !vv.empty?
          chart = _get_chart(params) || return
          ca = SIL::SpaceApi.ca(ca) if ca.instance_of?(String)
          ca = ca.ca if ca.class == SIL::SpaceApi::CA
          @sample.assign_corrective_action(chart, ca, params[:comment] || "QA test #{Time.now.utc.iso8601}")
          return true
        rescue Exception=>e
          $log.warn "Sample#assign_ca exception"
          return !e.to_s.include?('The SPACE Listener generated following error') ## TODO still valid ?
        end
      end

      def violations(params={})
        chart = _get_chart(params) || return
        @sample.get_violations(chart).to_a
      end

    end


    class RawValue
      attr_accessor :raw, :value

      def initialize(raw)
        @raw = raw
        @value = @raw.value
      end

      def dkeys
        Hash[@raw.data_key_names.zip(@raw.data_keys)]
      end

      def ekeys
        Hash[@raw.extractor_key_names.zip(@raw.extractor_keys)]
      end
    end


    class MPC
      attr_accessor :mpc, :name

      def initialize(c)
        @mpc = c
        @name = @mpc.config.name
      end

      def inspect
        "#<#{self.class.name} #{@name.inspect}>"
      end

      def parameters
        @mpc.config.parameters.collect {|p| p.name}
      end

      def trigger_keys
        lds = LDS.new(@mpc.parent.getLDS)
        ekeys = lds.ekeys
        tks = @mpc.config.attached_trigger_keys
        ret = []
        tks.each {|tk|
          ekeys.each {|ek|   # TODO: use find
            (ret << SpcTriggerKey.new(ek.keyid, ek.name, tk.value); break) if ek.keyid == tk.id
          }
        }
        return ret
      end

      def groups
        @mpc.getAllMPCGroups
      end

    end

  end
end
