=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.


Author:  ssteidte, 2010-11-24

Version: 2.3.7

History:
  2012-01-24 ssteidte, use Sequel instead of DBI for databases
  2012-06-25 ssteidte, version 2 interfaces for B2B
  2014-07-30 ssteidte, cleaned up
  2016-08-24 sfrieske, use lib rqrsp_mq instead of rqrsp
  2017-02-17 sfrieske, allow part names with '*'
  2018-03-12 sfrieske, removed MQXML from ERPMES, now defined in the inheriting classes
  2018-10-22 sfrieske, removed dependency on XML builder
  2019-06-26 sfrieske, removed dependency on ERPMES
  2019-07-31 sfrieske, removed require 'xmlhash'
  2020-06-17 sfrieske, added ext_lotfamily_info
  2020-10-05 sfrieske, improved error logging for ext_lotfamily_info
  2021-01-20 sfrieske, added update_ext_lot_info
  2021-08-27 sfrieske, require waitfor instead of misc
=end

require 'time'  # iso8601
require 'rqrsp_mq'
require 'siview'
require 'util/waitfor'


# TurnKey Testing
module Turnkey

  # currently not used (was only used in Test_Tkey_Flow which is retired)
  class XXBranchStart
    attr_accessor :sv, :mq, :tktimeout

    def initialize(env, params={})
      @sv = params.has_key?(:sv) ? params[:sv] : SiView::MM.new(env)
      @mq = RequestResponse::MQXML.new("tkbranch.#{env}")
      @tktimeout = params[:tktimeout] || 360
    end

    def get_version(params={})
      $log.info "get_version #{self.inspect}"
      h = {'soapenv:Envelope': {'xmlns:soapenv'=>'http://www.w3.org/2003/05/soap-envelope',
        'xmlns:urn'=>'urn:TurnKeyERPBranchStartOperations', 'soapenv:Body': {'urn:getVersion': nil}
      }}
      ret = @mq.send_receive(h, {timeout: 20}.merge(params)) || return
      return ret[:getVersionResponse][:return]
    end

    # msg sent by FabGUI when the lot is at :tkbumpshipinit; the Turnkey job does a NonProBankIn.
    # return true on success
    def shipment_to_subcon(lot, params={})
      shiptype = params[:shiptype] || "SUBCON"  # one of SUBCON, FINAL, NONSTANDARD
      $log.info "shipment_to_subcon #{lot.inspect}, shiptype: #{shiptype.inspect}"
      li = @sv.lot_info(lot, wafers: true)
      h = {'soapenv:Envelope': {'xmlns:soapenv'=>'http://www.w3.org/2003/05/soap-envelope',
        'xmlns:ns2'=>'urn:TurnKeyERPBranchStartOperations', 'soapenv:Body': {
          'ns2:shipmentToSubCon': {
            'ns2:lotId': lot,
            'ns2:waferIds': li.wafers.collect {|w| w.wafer},
            'ns2:shipType': params[:shiptype] || 'SUBCON'  # one of SUBCON, FINAL, NONSTANDARD
          }
        }
      }}
      res = @mq.send_receive(h) || return
      ret = (res[:shipmentToSubConResponse][:return][:result][nil] == 'TURNKEY_SERVICE_OK')
      $log.warn "  error:\n#{res.inspect}" unless ret
      return ret
    end

    # wait until the lot has left the indicated operation, return true on success
    def wait_lot_move(lot, op, params={})
      $log.info "wait_lot_move #{lot.inspect}, #{op.inspect}, #{params.inspect}"
      res = wait_for({timeout: @tktimeout}.merge(params)) {@sv.lot_info(lot).op != @ops[op]}
      $log.info "  lot #{lot} has not moved from #{op} within #{@tktimeout}s" unless res
      return res
    end

    # wait until the lot has the specified hold, return true on success
    def wait_lot_hold(lot, reason, params={})
      $log.info "wait_lot_hold #{lot.inspect}, #{reason.inspect}"
      res = wait_for({timeout: @tktimeout}.merge(params)) {@sv.lot_hold_list(lot, reason: reason).first}
      $log.info "  lot #{lot} has no hold with reason #{reason}" unless res
      return res
    end

  end


  class B2B
    attr_accessor :sv, :mq

    def initialize(env, params={})
      @sv = params[:sv] || SiView::MM.new(env)
      @mq = RequestResponse::MQXML.new("tkb2b.#{env}")
    end

    def get_version(params={})
      $log.info "get_version #{self.inspect}"
      h = {'soapenv:Envelope': {'xmlns:soapenv'=>'http://www.w3.org/2003/05/soap-envelope',
        'xmlns:urn'=>'urn:TurnKeyB2BServiceOperations', 'soapenv:Body': {'urn:getVersion': nil}
      }}
      ret = @mq.send_receive(h, timeout: 20) || return
      return ret[:getVersionResponse][:return]
    end

    # returns array of info for all lots in the family, even if lot is a child lot
    def ext_lotfamily_info(lot)
      $log.info "ext_lotfamily_info #{lot.inspect}"
      h = {'soapenv:Envelope': {
        'xmlns:soapenv'=>'http://schemas.xmlsoap.org/soap/envelope/',
        'xmlns:urn'=>'urn:TurnKeyB2BServiceOperations',
        'soapenv:Body': {
          'urn:getExtLotFamilyInfo': {'urn:lotId': lot}
      }}}
      res = @mq.send_receive(h) || return
      # return unless res[:getExtLotFamilyInfoResponse][:return][:result][nil] == 'TURNKEY_B2B_SERVICE_OK'
      # return res[:getExtLotFamilyInfoResponse][:return][:lotInfo]
      if res[:getExtLotFamilyInfoResponse][:return][:result][nil] != 'TURNKEY_B2B_SERVICE_OK'
        $log.warn "TKEY_B2B_SERVICE error:\n#{res.inspect}"
        return
      end
      return res[:getExtLotFamilyInfoResponse][:return][:lotInfo]
    end

    # calls getExtLotInfo, returns true if TURNKEY_B2B_SERVICE_OK is returned
    def ext_lot_info(lot)
      $log.info "ext_lot_info #{lot.inspect}"
      h = {'soapenv:Envelope': {
        'xmlns:soapenv'=>'http://www.w3.org/2003/05/soap-envelope',
        'soapenv:Body': {
          'ns2:getExtLotInfo': {'xmlns:ns2'=>'urn:TurnKeyB2BServiceOperations', 'ns2:lotId': lot}
      }}}
      res = @mq.send_receive(h) || return
      return unless res[:getExtLotInfoResponse][:return][:result][nil] == 'TURNKEY_B2B_SERVICE_OK'
      return res[:getExtLotInfoResponse][:return][:lotInfo]
    end

    # change subcons, works but not used in Fab1
    def update_ext_lot_info(lot, params={})
      $log.info "update_ext_lot_info #{lot.inspect}, #{params}"
      li = @sv.lot_info(lot)
      uparams = Hash[@sv.user_parameter('Lot', lot).collect {|p| [p.name, p.value]}]
      lotInfo = {
        'xmlns:ns1'=>'urn:TurnKeyB2BServiceTypes',
        'xmlns:xsi'=>'http://www.w3.org/2001/XMLSchema-instance',
        'xsi:type'=>'ns1:TurnKeyB2BExtLotInfo',
        'ns1:carrierId': nil,
        'ns1:currentOperation': "1260.1200 - TKYSORT.1 - TKYSORT",
        'ns1:customerCode': li.customer,
        'ns1:lotDieQty': 0,
        'ns1:lotId': lot,
        'ns1:lotStatus': li.status,
        'ns1:lotType': li.lottype,
        'ns1:lotWaferQty': li.nwafers,
        'ns1:nextOperation': "1260.1300 - TKYOQAS.1 - TKYOQAS",
        'ns1:parentLotId': li.family,
        'ns1:productId': li.product,
        'ns1:routeId': li.route,
        'ns1:subconIdBump': params['TurnkeySubconIDBump'] || uparams['TurnkeySubconIDBump'],
        'ns1:subconIdSort': params['TurnkeySubconIDSort'] || uparams['TurnkeySubconIDSort']
      }
      # send current values (not required any more since 20.08)
      if params[:sendattribs]
        attribs = []
        if scbump = params['TurnkeySubconIDBump']
          attribs << {'ns1:scriptParameterId'=>'TurnkeySubconIDBump', 'ns1:scriptParameterValue'=>scbump}
        end
        if scsort = params['TurnkeySubconIDSort']
          attribs << {'ns1:scriptParameterId'=>'TurnkeySubconIDSort', 'ns1:scriptParameterValue'=>scsort}
        end
        lotInfo['ns1:subconScriptParamAttributes'] = attribs
      end
      lotInfo.merge!({
        # 'ns1:transferStatus': nil,
        'ns1:turnkeyType': uparams['TurnkeyType'],
        'ns1:bankId': nil,
        'ns1:familyLotId': li.family,
        'ns1:lotContent': li.content,
        'ns1:subLotType': li.sublottype,
        'ns1:subconIdAssembly': nil,
        'ns1:subconIdBump2': nil,
        'ns1:subconIdFinalTest': nil,
        'ns1:subconIdSort2': nil,
        'ns1:totalUnitQty': 0,
      })
      additionalAttribs = []
      lotAttrs = []
      if v = uparams['ERPItem']
        lotAttrs << 'ERP_ITEM'
        additionalAttribs << {'ns1:name': 'ERP_ITEM', 'ns1:strValue': v, 'ns1:type': 'STRING'}
      end
      if v = uparams['ERPSalesOrder']
        lotAttrs << 'ERP_SALES_ORDER'
        additionalAttribs << {'ns1:name': 'ERP_SALES_ORDER', 'ns1:strValue': v, 'ns1:type': 'STRING'}
      end
      lotInfo[:'ns1:additionalAttributes'] = additionalAttribs
      #
      h = {'soapenv:Envelope': {'xmlns:soapenv'=>'http://www.w3.org/2003/05/soap-envelope', 'soapenv:Body': {
        'ns2:updateExtLotInfo': {
          'xmlns:ns2'=>'urn:TurnKeyB2BServiceOperations', 'ns2:lotInfo': lotInfo, 'ns2:lotAttributes': lotAttrs
        }
      }}}
      res = @mq.send_receive(h) || return
      return unless res[:updateExtLotInfoResponse][:return][:result][nil] == 'TURNKEY_B2B_SERVICE_OK'
      return res[:updateExtLotInfoResponse][:return][:lotInfo]
    end


    def _action(lot, action, params={})
      # $log.info "B2B #{lot.inspect}, #{action.inspect}, #{params}"
      refId = params[:refid] || "QA_TEST_#{Time.now.iso8601}"
      uparams = Hash[@sv.user_parameter('Lot', lot).collect {|p| [p.name, p.value]}]
      partName = params[:partname] || uparams['ERPItem']
      subcons = params[:subcons] || [uparams['TurnkeySubconIDBump'], uparams['TurnkeySubconIDSort']].compact
      type = params[:type]
      subcon = params[:subcon]
      if type && subcon.nil?
        subcon = (type.to_s.upcase == 'BUMP') ? uparams['TurnkeySubconIDBump'] : uparams['TurnkeySubconIDSort']
      end
      op = params[:op] || 'DUMMY' # will be removed later
      eventTime = params[:eventtime] || Time.now
      eventTime = eventTime.utc.iso8601(3).tr('-:', '') if eventTime.kind_of?(Time)
      $log.info "#{action} #{lot.inspect}"
      ($log.warn 'no subcons'; return) if subcons.empty?
      li = @sv.lot_info(lot, wafers: true)
      #
      opinfo = {
        'xmlns:ns1'=>'urn:TurnKeyB2BServiceTypes',
        'xmlns:xsi'=>'http://www.w3.org/2001/XMLSchema-instance',
        'xsi:type'=>'ns1:TurnKeyB2BOperationInfo2',
        'ns1:currentStage': op,
        'ns1:eventTime': eventTime,
        'ns1:partName': partName,   ##.sub('*', '')
        'ns1:subconIdList': subcons,
      }
      opinfo[:'ns1:currentProcessInfo'] = {:'ns1:subconId'=>subcon, :'ns1:type'=>type} if type
      opinfo[:'ns1:lotContent'] = params[:lotcontent] || 'Wafer'
      data = {
        'xmlns:ns2'=>'urn:TurnKeyB2BServiceOperations',
        :'ns2:refId'=>refId,
        :'ns2:opInfo'=>opinfo,
        :'ns2:lotId'=>lot
      }
      data[:'ns2:lotState'] = params[:lotstate] || 'RUN' if ['start2', 'move2', 'complete2'].member?(action)
      data[:'ns2:childLotId'] = params[:child] if ['merge2'].member?(action)
      nwafers = li.wafers.size
      if action == 'merge2'
        cli = @sv.lot_info(params[:child])
        nwafers += cli.nwafers
      end
      if ['splitWaferLot2', 'scrapWaferLot2'].member?(action )
        data[:'ns2:waferQty'] = nwafers
      else
        data[:'ns2:unitQty'] = nwafers
      end
      case action
      when 'move2'
        # not necessary to accept the move request
        pgooddies = @sv.f7 ? 'B_GoodDieNo' : 'DieCountGood'
        ll = lot.split('.').first
        data[:'ns2:waferInfo'] = li.wafers.collect {|w| {'xmlns:ns1'=>'urn:TurnKeyB2BServiceTypes',
          'xmlns:xsi'=>'http://www.w3.org/2001/XMLSchema-instance', 'xsi:type'=>'ns1:TurnKeyB2BWaferInfo',
          :'ns1:waferId'=>w.alias.index('.') ? w.alias : "#{ll}.#{w.alias}",
          :'ns1:dieQty'=>params[:dcg] ||  @sv.user_parameter('Wafer', w.wafer, name: pgooddies).value
        }}
      when 'hold2'
        data[:'ns2:holdCode'] = params[:holdcode]
        holdreason = params[:holdreason]
        holdreason = 'QA Test' unless params.has_key?(:holdreason)
        data[:'ns2:holdReason'] = holdreason
      when 'release2'
        data[:'ns2:holdEventTime'] = params[:holdtime]
      when 'splitWaferLot2'
        data[:'ns2:waferIds'] = params[:wafers]
      when 'merge2'
        data[:'ns2:childUnitQty'] = cli.nwafers
      when 'scrapWaferLot2'
        ll = lot.split('.').first
        data[:'ns2:waferInfo'] = li.wafers.collect {|w| {'xmlns:ns1'=>'urn:TurnKeyB2BServiceTypes',
          'xmlns:xsi'=>'http://www.w3.org/2001/XMLSchema-instance', 'xsi:type'=>'ns1:TurnKeyB2BScrapWaferInfo',
          :'ns1:waferId'=>w.alias.index('.') ? w.alias : "#{ll}.#{w.alias}",
          :'ns1:scrapCode'=>params[:scrapcode]
        }}
        data[:'ns2:scrapReason'] = params[:scrapreason] || 'QA Test'
      end
      h = {'soapenv:Envelope': {
        'xmlns:soapenv'=>'http://www.w3.org/2003/05/soap-envelope', 'soapenv:Body': {"ns2:#{action}": data}
      }}
      res = @mq.send_receive(h) || return
      ($log.warn "  error: #{res.inspect}"; return) unless res[res.keys[0]][:return]
      r = res[res.keys[0]][:return][:result]
      r = r[nil] if r.kind_of?(Hash)
      ret = (r == 'TURNKEY_B2B_SERVICE_OK')
      $log.warn "TKEY_B2B_SERVICE error:\n#{res.inspect}" unless ret
      return ret
    end

    # start TKEY moves
    def start(lot, params={})
      _action(lot, 'start2', params)
    end

    # load lot and start processing
    def move(lot, params={})
      _action(lot, 'move2', params)
    end

    # load lot and start processing
    def complete(lot, params={})
      _action(lot, 'complete2', params)
    end

    # holdcodes are configured properties: PPD,PCD,PSORT,RSORT
    def hold(lot, holdcode, params={})
      _action(lot, 'hold2', params.merge(holdcode: holdcode))
    end

    def release(lot, params={})
      htime = params[:holdtime]
      if htime
        htime = htime.utc.iso8601(3).tr('-:', '') if htime.kind_of?(Time)
      else
        # get hold time from claim memo
        h = @sv.lot_hold_list(lot).first || ($log.warn "release: no hold to release"; return)
        htime = h.memo[8..26] + 'Z'
      end
      _action(lot, 'release2', params.merge(holdtime: htime))
    end

    # return child lot
    def split(lot, wafers, params={})
      ll = lot.split('.')[0]
      if wafers.kind_of?(String)
        waferids = wafers.split
      elsif wafers.kind_of?(Numeric)
        waferids = @sv.lot_info(lot, wafers: true).wafers[0...wafers].collect {|w|
          w.alias.index('.') ? w.alias : "#{ll}.#{w.alias}"
        }
      elsif wafers.kind_of?(Array)
        waferids = wafers.collect {|w|
          w.kind_of?(String) ? w : (w.alias.index('.') ? w.alias : "#{ll}.#{w.alias}")
        }
      end
      _action(lot, 'splitWaferLot2', params.merge(wafers: waferids)) || return
      child = @mq.service_response[:splitWaferLot2Response][:return][:splitLotId]
      $log.info "  child lot: #{child.inspect}"
      return child
    end

    def merge(lot, child, params={})
      _action(lot, 'merge2', params.merge(:child=>child))
    end

    # scrapcodes is any valid SiView WaferScrap code, e.g. TECH
    def scrap(lot, scrapcode, params={})
      _action(lot, 'scrapWaferLot2', params.merge(scrapcode: scrapcode))
    end

    # test support

    def verify_lot_hold(lot, onhold, params={})
      $log.info "verify_lot_hold: #{lot.inspect}, #{onhold}"
      lhs = @sv.lot_hold_list(lot, type: 'LotHold')
      if !onhold
        # check for no holds
        return true if lhs == []
        $log.warn "  unexpected lot holds: #{lhs.inspect}"
        return false
      end
      # check for correct holds
      if et = params[:eventtime]
        $log.info "  checking eventtime"
        et = et.utc.iso8601(3).tr('-:', '') if et.kind_of?(Time)
        ($log.warn "  wrong eventtime"; return) if lhs.select {|lh| lh.memo.start_with?("B2B_hold#{et}")}.size != 1
      end
      return true
    end

    def hold_verify(lot, holdcode, params={})
      hold(lot, holdcode, params) && verify_lot_hold(lot, true, params)
    end

    def release_verify(lot, params={})
      release(lot, params) && verify_lot_hold(lot, params[:onhold], params)  # support for multiple holds
    end

    # send start, move or complete request and verify new lot status
    def move_verify(action, lot, op, status, params={})
      send(action, lot, params) || return
      $log.info "verify_lot_state: #{lot.inspect}, #{op.inspect}, #{status.inspect}"
      ok = true
      li = @sv.lot_info(lot)
      ($log.warn "  wrong status: #{li.status.inspect}"; ok = false) if li.status != status
      ($log.warn "  wrong operation: #{li.op.inspect}"; ok = false) if li.op != op
      return ok
    end

  end

end
