=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author:     ssteidte, 2012-09-27

Version:    1.1.9

History:
  2012-09-27 ssteidte, created
  2012-10-24 ssteidte, added class TurnKeyBTF
  2013-07-05 ssteidte, added set_lot_wafer_bin
  2014-06-02 ssteidte, inherit from ERMES
  2015-10-05 sfrieske, added OQAS and tk type N
  2016-05-04 sfrieske, small cleanups in TestDB code
  2016-09-06 sfrieske, SOR2 is included in standard J (no impact)
  2017-04-21 sfrieske, update_sort_result uses lot's current PD as default
  2017-11-20 sfrieske, use struct_to_hash instead of Struct.to_hash
  2018-02-15 sfrieske, replaced select_all by select
  2018-03-12 sfrieske, removed dep on struct_to_hash, removed MQXML from ERPMES, now defined in the inheriting classes
  2018-05-23 sfrieske, removed direct dep on xmlhash.rb, conversion is handled by RequestResponse::MQXML
  2018-06-01 sfrieske, removed dependency on ERPMES
  2020-01-20 sfrieske, get_wafer_bins returns data in the same format as set_lot_wafer_bins (for dev/analysis) 
=end

require 'dbsequel'
require 'rqrsp_mq'
require 'siview'


module Turnkey

  # connection to Test DB for Binning Information
  class TestDB
    attr_accessor :sv, :db, :table

    def initialize(env, params={})
      @sv = params[:sv] || SiView::MM.new(env)
      @db = params[:db] || ::DB.new("testdb.#{env}")
      @table = params[:table] || 'WAFER_BIN_COUNT_TEST_TABLE'
    end

    # return lot wafer bins in the same format as returned by set_lot_wafer_bins, for dev and analysis
    def get_lot_wafer_bins(lot, params={})
      qargs = []
      q = 'SELECT WAFERID, BINNUMBER, DIEQTY, GOODBIN, OPERATIONID, LOTID from V_WAFER_BIN_COUNT'
      q, qargs = @db._q_restriction(q, qargs, 'LOTID', lot)
      q, qargs = @db._q_restriction(q, qargs, 'OPERATIONID', params[:op] || 'LOTSHIPDESIG.01')
      q, qargs = @db._q_restriction(q, qargs, 'WAFERID', params[:wafer])
      q, qargs = @db._q_restriction(q, qargs, 'BINNUMBER', params[:bin])
      q += ' ORDER BY WAFERID, BINNUMBER'
      res = @db.select(q, *qargs) || return
      ret = {}
      res.each {|r|
        wid = r[0]
        ret[wid] = [] if ret[wid].nil?
        ret[wid] << {
          waferid: wid, binnumber: r[1].to_i, dieqty: r[2].to_i, goodbin: r[3].to_i, operationid: r[4], lotid: r[5]
        }
      }
      return ret
    end

    # set binning data for lot wafers, preceded by the deletion of current lot's wafer data
    #
    # passing the optional parameters :binning_data and :goodbins allows to set data for selected wafers (only),
    # default is to set good dies in bin 1 for all wafers in a lot to 100
    #
    # pass set_dcg: false to prevent updating the SiView wafer script parameters correctly
    #
    # return hash of waferids mapped to an array of WaferBinCount hashes (for comparison with AsmView) or nil on error
    def set_lot_wafer_bins(lot, params={})
      # clean up lot data
      @db.delete_from(@table, lotid: lot) || return
      # get parameters with sensible defaults
      binning_data = params[:binning_data]  # hash {waferindex=>[dies_bin1, ...], }; waferindex 0 means 1st wafer, even if it is in slot 3
      goodbins = params[:goodbins]          # list of good bins in above array (unless all), e.g. [0, 1]; means bin1 and bin2 are flagged good
      set_dcg = params[:set_dcg] != false
      li = @sv.lot_info(lot, wafers: true)
      op = params[:op] || li.op
      $log.info "set_lot_wafer_bins #{lot.inspect}, op: #{op.inspect}"
      $log.debug {params.inspect}
      # set binning data per wafer and bin
      ok = true
      ret = {}
      li.wafers.each_with_index {|w, waferidx|
        ret[w.wafer] = []
        bdata = binning_data ? binning_data[waferidx] : [100]
        dcg = 0  # stays 0 if the wafer has no binning data
        if bdata
          bdata = [bdata] unless bdata.kind_of?(Array)
          bdata.each_with_index {|qty, bindidx|
            goodbinflag = 0
            if goodbins.nil? || goodbins.member?(bindidx)
              goodbinflag = 1
              dcg += qty
            end
            b = {waferid: w.wafer, binnumber: bindidx + 1, dieqty: qty, goodbin: goodbinflag, operationid: op, lotid: lot}
            ret[w.wafer] << b
            ok &= @db.insert_into(@table, b)
          }
        end
        ok &= (@sv.user_parameter_change('Wafer', w.wafer, 'DieCountGood', dcg, 1, datatype: 'INTEGER') == 0) if set_dcg
      }
      return ok ? ret : nil
    end

  end


  class BTF
    attr_accessor :sv, :mq, :response_msg

    def initialize(env, params={})
      @sv = params[:sv] || SiView::MM.new(env)
      @mq = RequestResponse::MQXML.new("tkbtf.#{env}")
    end

    def get_version(params={})
      $log.info "get_version #{self.inspect}"
      h = {'soapenv:Envelope': {'xmlns:soapenv'=>'http://www.w3.org/2003/05/soap-envelope',
        'xmlns:urn'=>'urn:QAQA', 'soapenv:Body': {'urn:getVersion': nil}
      }}
      ret = @mq.send_receive(h, timeout: 20) || return
      return ret[:getVersionResponse][:return]
    end

    # sort result message from FabGUI to the BTFTurnkey jCAP job
    #
    # note:
    # 1) lot is on LOTSHIPDESIG in sv, TKYSORT or TKYOQAB in av (discard everything after TKYSORT although lot goes on in sv)
    # 2) update_sort_result received (from FabGUI Shipping)
    # 3) send missing moves to AsmTurnkey until lot is on TKYOQAS/Processing, 
    #    get binning data from TestDB and send B2B Complete with binning data -> LOTSHIPD/Processing/AsmDieCount
    # 4) get response from AsmTurnkey, send response to FabGUI
    #
    # return true on TURNKEY_SORTRESULT_SERVICE_OK response
    def update_sort_result(lot, params={})
      $log.info "update_sort_result #{lot.inspect}, #{params}"
      #
      # stage = params[:stage]
      # data[:'urn:stageId'] = stage if stage  # not used
      h = {'soapenv:Envelope': {'xmlns:soapenv'=>'http://www.w3.org/2003/05/soap-envelope', 
        'xmlns:urn'=>'urn:TurnkeySortResultUpdateOperations', 'soapenv:Body': {'urn:updateSortResult': {
          'urn:lotId': lot, 
          'urn:operationId': params[:op] || @sv.lot_info(lot).op, 
          'urn:finalUpdate': params[:final] != false
        }}
      }}
      @response_msg = @mq.send_receive(h, params) || ($log.warn '  no response'; return)
      ($log.warn "  error: #{@response_msg}"; return) if @response_msg[:Fault]
      ret = (@response_msg[:updateSortResultResponse][:sortResultUpdateReturn][:code][nil] == 'TURNKEY_SORTRESULT_SERVICE_OK')
      $log.warn "  error: #{@response_msg.inspect}" unless ret
      return ret
    end

  end

end
