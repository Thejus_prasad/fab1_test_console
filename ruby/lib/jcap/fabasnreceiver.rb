=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.


Author:   Steffen Frieske, 2019-03-13

Version:  21.03

History:
  2019-03-13 sfrieske, use XMLHash via RequestResponse::MQXML instead of builder
  2021-04-09 sfrieske, removed subconScriptParamAttributes, use REXML::Document
  2021-07-20 aschmid3, adjusted tag3 - stageGroup to flexibility
  2021-08-27 sfrieske, require uniquestring instead of misc
  2021-11-26 sfrieske, set tktype at initialization

Notes:
  see https://docs.google.com/document/d/1YU_1vKPjMBkI50-VvphfDVAE-fj91xuh7DEG-dxk5ok/edit?ts=5c88e6ba#
=end

require 'time'
require 'rexml/document'
require 'rqrsp_mq'
require 'util/uniquestring'


class FabAsnReceiver
  attr_accessor :mq, :erpitem, :tktype, :srcfab, :_doc

  def initialize(env, params={})
    @erpitem = params[:erpitem] || '0000001VN343'
    @tktype = params[:tktype] || 'R'
    @srcfab = 'F8'
    @mq = RequestResponse::MQXML.new("fabasnreceiver.#{env}", {correlation: :msgid}.merge(params))
  end

  def get_version
    $log.info 'FabAsnReceiver#get_version'
    @_doc = doc = REXML::Document.new('<?xml version="1.0" encoding="UTF-8"?>')
    doc.add_element('soapenv:Envelope', {'xmlns:soapenv'=>'http://www.w3.org/2003/05/soap-envelope'})
    .add_element('soapenv:Body').add_element('getVersion')
    ret = @mq.send_receive(doc, timeout: 20) || return
    return ret[:getVersionResponse][:return]
  end

  # receiveAdvanceShipmentNotification2 message for 1 lot with 3 wafers to FabAsnReceiver, return true on success
  # partname is ignored, product is by MDM lookup, no fallback!
  def send_msg(lot, carrier, params={})
    $log.info "send_msg #{lot.inspect}, #{carrier.inspect}, #{params}"
    wafers = params[:wafers] || 3.times.collect {unique_string('WW')}
    slots = params[:slots] || []  # for special tests, typically empty
    @_doc = doc = REXML::Document.new('<?xml version="1.0" encoding="UTF-8"?>')
    tag1 = doc.add_element('soapenv:Envelope', {
      'xmlns:soapenv'=>'http://www.w3.org/2003/05/soap-envelope',
      'xmlns:urn'=>'urn:AsnServiceOperations',
      'xmlns:urn1'=>'urn:AsnServiceTypes',
    })
    .add_element('soapenv:Body')
    .add_element('urn:receiveAdvanceShipmentNotification2')
    .add_element('urn:advanceShipmentNotification')
    tag1.add_element('urn1:msgId').text = params[:msgid] || "QA00#{unique_string}"
    tag1.add_element('urn1:msgTime').text = Time.now.utc.iso8601
    tag1.add_element('urn1:receiver').text = 'F1-ITDC'
    tag1.add_element('urn1:sender').text = 'Manual'
    #
    tag2 = tag1.add_element('urn1:shipment')
    tag2.add_element('urn1:countryOfOrigin').text = 'US'
    tag2.add_element('urn1:customerCode').text = params[:customer] || 'ericab'
    tag2.add_element('urn1:customerShipmentNumber').text = params[:shipno] || "Ship-#{unique_string}"
    #
    tag3 = tag2.add_element('urn1:lot')
    # optional  tag3.add_element('urn1:customerLotId').text = lot.gsub('.', '')
    tag3.add_element('urn1:customerPoLine').text = 'Customer PO-LineQA37'
    tag3.add_element('urn1:customerPoNumber').text = '300132189'
    tag3.add_element('urn1:erpItemId').text = params.has_key?(:erpitem) ? params[:erpitem] : @erpitem
    tag3.add_element('urn1:formFactor').text = 'Wafer'
    tag3.add_element('urn1:fosbId').text = carrier
    tag3.add_element('urn1:lotCancelFlag').text = 'N'
    tag3.add_element('urn1:lotGrade').text = 'A1'
    tag3.add_element('urn1:lotId').text = params[:keep_lotid] ? lot : lot.gsub('.', '')
    tag3.add_element('urn1:partName').text = 'F10-ignored'
    tag3.add_element('urn1:qualityCode').text = '00'
    tag3.add_element('urn1:quantity').text = wafers.size
    tag3.add_element('urn1:sourceFabCode').text = @srcfab
    tag3.add_element('urn1:stageGroup').text = params[:stagegroup] || 'BUMP'  # unclear, not the mfg layer
    tag3.add_element('urn1:subLotType').text = 'PR' # does not matter
    tag3.add_element('urn1:turnkeyType').text = params[:tktype] || @tktype
    tag3.add_element('urn1:vendorLotId').text = unique_string('NN')
    wafers.each_with_index.collect {|w, i|
      walias = '%02d' % (i + 1)
      # optional customerWaferId  "#{lot.split('.').first}.CWID.#{walias}"
      slot = params[:noslots] ? '' : (slots[i] || (i + 1))   # normally like walias
      tagw = tag3.add_element('urn1:wafer')
      tagw.add_element('urn1:slotNo').text = slot
      tagw.add_element('urn1:waferAlias').text = "#{lot.split('.').first}.#{walias}"
      tagw.add_element('urn1:waferId').text = w
    }
    #
    tag2.add_element('urn1:shipFromPartnerId').text = @srcfab
    tag2.add_element('urn1:shipToPartnerId').text = 'GG'
    #
    res = @mq.send_receive(doc) || return
    ($log.warn res; return) if res[:Fault]
    ret = res[:receiveAdvanceShipmentNotification2Response][:return]
    return true if ret && ret[:shipment] && ret[:shipment][:code] == 'ADVANCE_SHIPPING_NOTICE_SERVICE_OK'
    $log.info "error response: #{res[:receiveAdvanceShipmentNotification2Response]}"
    return false
  end

end
