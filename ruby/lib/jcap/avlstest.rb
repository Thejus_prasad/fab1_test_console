=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author:     ssteidte, 2012-03-28
Version:    19.05.8

History:
  2012-08-23 ssteidte, remove to_char conversion from queries for Date and Time fields
  2013-03-18 ssteidte, separated avls and avlstest
  2013-09-30 ssteidte, new vendorlot convention in lot info
  2015-01-22 ssteidte, inherit from SJC::Test
  2015-02-16 ssteidte, remove VendorFile FTP tests
  2016-01-11 sfrieske, prepare for MOL and BEOL carriers, improved logging
  2017-11-01 sfrieske, better handling of missing lots in send_vmessage_verify_carrier_lot
  2018-12-18 sfrieske, remove dependency on obsolete SJC::QA test adapter
  2020-02-21 sfrieske, added SORT_TYPE check in avlstest
  2021-02-05 sfrieske, use instance_of instead of kind_of
  2021-08-27 sfrieske, require waitfor instead of misc
=end

require 'util/uniquestring'
require 'util/waitfor'
require_relative 'avls'
require_relative 'sjctest'


module AVLS

  # Testing Automated Vendor Lot Start
  class Test < SJC::Test
    attr_accessor :vmessage, :job_interval, :bank_incoming, :bank_raw, :location, :part, :vcarrier_category

    def initialize(env, params={})
      super(env, {mds: ::AVLS::MDS.new(env)}.merge(params))
      @vmessage = VendorMessage.new(env, params)
      @job_interval = params[:job_interval] || 330
      @bank_incoming = params[:bank_incoming] || 'O-PREFAB'
      @bank_raw = params[:bank_raw] || 'ON-RAW'
      @location = params[:location] || 'Fab 1'
      @part = params[:part] || '31006550'
      @vcarrier_category = params[:vcarrier_category] || 'FOSB'
    end

    # verify actions of AVLS on msg receive
    def send_vmessage_verify_carrier_lot(lots, carriers, params={})
      lots = [lots] if lots.instance_of?(String)
      carriers = [carriers] if carriers.instance_of?(String)
      $log.info "send_vmessage_verify_carrier_lot #{lots}, #{carriers}, #{params}"
      order = params[:order] || unique_string
      resp = @vmessage.create_fosb(lots, carriers, {location: @location, part: @part, order: order}.merge(params))
      ($log.warn '  error sending message'; return) unless resp
      resp = [resp] unless resp.instance_of?(Array)
      $log.debug {"  response: #{resp}"}
      #
      ### sleep 3
      ok = true
      carriers.each_with_index {|c, i|
        $log.info "  verify AVLS response for carrier #{c.inspect}"
        res = resp[i]
        ($log.info "    wrong carrier: #{res.inspect}"; ok=false; next) if res[:carrierId] != c
        $log.info "  verify AVLS DB entry for carrier #{c.inspect}"
        rec = @mds.avls_carriers(carrier: c.strip).first
        ($log.warn "    missing MDS entry"; ok=false; next) unless rec
        ($log.warn "    wrong correlation ID: #{rec.inspect}, #{res.inspect}"; ok=false) if res[:correlationId].to_i != rec.carrierpk
        ($log.warn "    wrong status: #{rec.inspect}"; ok=false) if rec.state != 'INITIAL'
      }
      return unless ok
      $log.info "  waiting up to #{@job_interval} s for the AVLS job to create carrier and lot"
      wait_for(timeout: @job_interval) {
        carriers.inject(true) {|ok, carrier|
          rec = @mds.avls_carriers(carrier: carrier).first
          ok && rec && rec.state == 'VENDORLOTPREPARATION'
        }
      }
      carriers.each {|c|
        $log.info "  verify AVLS DB entry for carrier #{c.inspect}"
        c = c.strip  # remove trailing spaces from special test cases
        rec = @mds.avls_carriers(carrier: c).first
        ($log.warn "    missing MDS entry"; ok=false; next) unless rec
        ($log.warn "    wrong status: #{rec.inspect}"; ok=false) unless rec.state == 'VENDORLOTPREPARATION'
        #
        csraw = @sv.carrier_status(c, raw: true)
        $log.info "  verify SiView carrier #{c.inspect}"
        ($log.info '    carrier has not been created'; ok=false; next) unless csraw
        ($log.info '    wrong category'; ok=false) if csraw.cassetteBRInfo.cassetteCategory != @vcarrier_category
        ($log.info '    wrong PM interval'; ok=false) if csraw.cassettePMInfo.intervalBetweenPM. != 0
        ($log.info '    wrong status'; ok=false) if csraw.cassetteStatusInfo.cassetteStatus != 'AVAILABLE'
        lot = rec.lot
        ($log.info "    no lot"; ok=false) unless lot
        next unless ok
        li = @sv.lot_info(lot)
        $log.info "  verify SiView vendor lot #{lot.inspect}"
        ($log.info "    wrong lot: #{li.inspect}"; ok=false) unless li && li.lottype == 'Vendor'
        ($log.info "    wrong bank: #{li.bank} instead of #{@bank_incoming}"; ok=false) unless li.bank == @bank_incoming
        vl = "#{rec.vendor} - #{rec.order}/#{rec.vendor_lot} - #{rec.part}"
        ($log.info "    wrong vendor lot: #{li.vendorlot} instead of #{vl.inspect}"; ok=false) if li.vendorlot != vl
        next unless ok
        # ok = wait_for(timeout: 60) {@sv.user_parameter('Lot', lot, name: 'AutoSTBAllowed').value == 'Y'}
        # $log.warn '    wrong lot script parameter AutoSTBAllowed' unless ok
        # next unless ok
        # li.wafers.each {|w|
        #   ok = wait_for(timeout: 60) {@sv.user_parameter('Wafer', w.wafer, name: 'OracleLotID').value == order}
        #   $log.warn '    wrong wafer script parameter OracleLotID'
        #   next unless ok
        # }
      }
      return ok
    end

    def carrier_stockin_verify_sr(carrier, params={})
      $log.info "carrier_stockin_verify_sr #{carrier.inspect}"
      res = @sv.carrier_xfer_status_change(carrier, '', @stocker, manualin: true)
      ($log.info '  error stocking in carrier'; return) if res != 0
      rec = nil
      state = params[:state] || 'SJCTRANSFERREQUEST'
      $log.info "waiting up to #{@job_interval} s for T_AVLS_CAST.PROCESSING_STATE #{state.inspect}"
      wait_for(timeout: @job_interval) {
        rec = @mds.avls_carriers(carrier: carrier).first
        rec && rec.state == state
      }
      ($log.warn '  missing MDS entry'; return) unless rec
      ($log.warn "  wrong processing state: #{rec}"; return) if rec.state != state
      # verify SORT_TYPE (RTD requires TRANSFER)
      if state == 'SJCTRANSFERREQUEST'
        $log.info "  found SR #{rec.srid.inspect}"
        sorttype = @mds.sjc_requests(srid: rec.srid).first.sorttype
        ($log.warn "  wrong sorttype: #{sorttype}"; return) if sorttype != 'TRANSFER'
      end
      return rec.srid
    end

    def verify_sr_slots(vcarrier, srid)
      $log.info "verify_sr_slots #{vcarrier.inspect}, #{srid} (reversed)"
      srwafers = @mds.sjc_wafers(srid: srid)
      msgwafers = @vmessage.fdata.find {|e| e[:'aut:carrierId'] == vcarrier}[:'aut:wafer']
      ret = true
      msgwafers.each {|e|
        mwafer = e[:'aut:waferId']
        mslot = e[:'aut:waferPosition']
        srslot = srwafers[mwafer]
        ($log.warn "  wrong SR slot for wafer #{mwafer}"; ret = false) if mslot + srslot != 26
      }
      return ret
    end

    def verify_carrier_slots(vcarrier, carrier)
      $log.info "verify_carrier_slots #{vcarrier.inspect}, #{carrier.inspect} (reversed)"
      cwafers = @sv.lot_info(@sv.lot_list_incassette(carrier).first, wafers: true).wafers
      msgwafers = @vmessage.fdata.find {|e| e[:'aut:carrierId'] == vcarrier}[:'aut:wafer']
      ret = true
      msgwafers.each {|e|
        mwafer = e[:'aut:waferId']
        mslot = e[:'aut:waferPosition']
        cslot = cwafers.find {|w| w.wafer == mwafer}.slot
        ($log.warn "  wrong SR slot for wafer #{mwafer}"; ret = false) if mslot + cslot != 26
      }
      return ret
    end

    def execute_sr_verify(vcarrier)
      $log.info "execute_sr_verify #{vcarrier.inspect}"
      rec = @mds.avls_carriers(carrier: vcarrier).first || ($log.warn 'missing MDS entry'; return)
      sjid = execute_verify(srid: rec.srid) || return
      @sv.claim_sj_execute(sjid: sjid) || ($log.warn 'error executing SiView SJ'; return)
      $log.info "waiting up to #{@job_interval} s for the AVLS job to move lot #{rec.lot.inspect} to bank #{@bank_raw}"
      li = nil
      wait_for(timeout: @job_interval) {
        li = @sv.lot_info(rec.lot)
        li.bank == @bank_raw
      } || ($log.warn "wrong bank: #{li.bank} instead of #{@bank_raw}"; return)
      return true
    end

    def send_wrong_vmessage_verify_notification(lots, carriers, message, params={})
      lots ||= unique_string('Q')
      carriers ||= unique_string('FQ')
      $log.info "send_wrong_vmessage_verify_notification #{lots.inspect}, #{carriers.inspect}"
      $log.info "  #{message.inspect}, #{params}"
      tstart = Time.now - 20
      # send and verify message
      res = @vmessage.create_fosb(lots, carriers, params)
      $log.info 'verifying response'
      ($log.info '  no MQ response'; return) unless res
      ($log.info "  request must be rejected: #{res.inspect}"; return) unless res[:correlationId] == '-1'
      return true if @notifications.nil?
      # timeout = 120
      # $log.info "waiting up to #{timeout} s for the AVLS job to send a notification"
      return @notifications.wait_for(tstart: tstart, timeout: 120,
        service: '%jCAP_AsyncNotification%', filter: {'Application'=>'AutoVenLotStart'}) {|msgs|
          msgs.find {|m| m['Message'].include?(message)}
      }
    end

  end

end
