=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author:  ssteidte, 2011-08-11
Version: 21.04

History:
  2012-04-30 ssteidte, use 'rqrsp' library
  2012-08-23 ssteidte, remove to_char conversion from queries for Date and Time fields
  2012-12-05 ssteidte, use new tables, separated MDS access code and test into wbtqdb.rb and wbtqtest.rb
  2013-12-10 ssteidte, added LEADINGORDER awareness
  2014-08-22 ssteidte, added spec parser
  2015-07-06 ssteidte, MQClient checks GUID in received messages
  2015-12-02 sfrieske, send FabGUI message as text insted of bytes message (FabGUI 11.10)
  2016-08-31 sfrieske, use the lightweight SetupFC::Client instead of EasySetupFC
  2016-12-07 sfrieske, use mqjms directly instead of rqrsp as response is obtained by polling anyway
  2017-03-14 sfrieske, separated SAPClient and WBTQClient
  2017-09-25 sfrieske, multiple MESSAGE items in WBTQ->SAP message, kit grouping
  2018-03-02 sfrieske, remove dependency on the now retired fabgui.rb
  2018-11-28 sfrieske, new FabGUI msg format for FabGUIResponse
  2019-08-07 sfrieske, added Cancel option for FabGUIResponse
  2020-07-09 sfrieske, added auto_waferselection_flag
  2021-02-05 sfrieske, use instance_of instead of kind_of
  2021-04-14 sfrieske, use REXML::Document
  2021-05-18 sfrieske, removed kit_slots method, use kit_info directly
  2021-08-30 sfrieske, moved xmlhash.rb to util
=end

require 'rexml/document'
require 'mqjms'
require 'util/xmlhash'
require 'misc/sfc'


module WBTQ

  # SetupFC Spec Interface
  class SFCClient
    attr_accessor :spec, :sfc, :kits

    SpecKit = Struct.new(:name, :eqp, :chamber, :carrier_category, :new_carrier, :match_slots, :match_order, :allow_other_wafers,
      :no_other_kits, :allow_other_chambers, :allow_chamber_down, :lot_definitions, :slot_definitions, :kit_slots)
    LotDefinition = Struct.new(:product, :productid, :src_pattern)
    SlotDefinition = Struct.new(:slot, :productid)

    def initialize(env, spec, params={})
      @spec = spec
      @sfc = SetupFC::Client.new(env)
    end

    def inspect
      "#<#{self.class.name}, #{@sfc.inspect}, spec: #{@spec}>"
    end

    # retrieve and parse kits from the spec on demand, return array of kits (@kits), used internally
    def get_kits(params={})
      return @kits if @kits
      res = @sfc.spec_content(params[:spec] || @spec, parsed: true) || return
      res = res[:myFields][:OperationPMSection][:Instructions_Section][:Instructions_Group]
      res = res[:Instruction_Section][:Instruction_Group]
      res = res[:Instruction_KitSpecifications][:KitSpecifications][:Section]
      return res if params[:hash]
      #
      return @kits = res.collect {|k|
        e = k[:Equipments]
        e = e[:Equipmentsx] if e
        $log.debug {"  #{k[:SectionID].inspect}, #{e.inspect}"}
        f = k[:FoupDefinitions][:FoupDefinition]
        _sdefs = f[:SortingDefinition][:SlotDefinition]
        _sdefs = [_sdefs] unless _sdefs.instance_of?(Array)
        sdefs = _sdefs.collect {|e|
          SlotDefinition.new(e[:SlotID].to_i, e[:SlotProductDefinition]) if e[:SlotProductDefinition]
        }.compact
        kslots = {} # collapsed lot and slot definitions, hash of product=>[slots]
        _ldefs = k[:LotDefinitions][:LotDefinition]
        _ldefs = [_ldefs] unless _ldefs.instance_of?(Array)
        ldefs = _ldefs.collect {|l|
          kslots[l[:ProductID]] = sdefs.collect {|s| s.slot if s.productid == l[:CombinationNumber]}.compact
          ps = l[:SourcePrioPatternList][:SourcePrioPattern]
          ps = [ps] unless ps.instance_of?(Array)
          LotDefinition.new(l[:ProductID], l[:CombinationNumber], ps.collect {|e| e[:SourcePattern]})
        }
        kit = SpecKit.new(k[:SectionID], e.class == Hash ? e[:Equipment_ID] : nil, e.class == Hash ? e[:Chamber] : nil,
          f[:RequiredCarrierCategory], f[:OptionAlwaysNewCarrier] == '1', f[:OptionFixedSlotDefinition] == '1',
          f[:OptionFixedWaferOrder] == '1', f[:OptionNotInKitDefinedWafer] == '1', f[:OptionNoOtherKit] == '1',
          f[:OptionAllowChamberCombination] == '1', f[:OptionAllowChamberDownCombination] == '1', ldefs, sdefs, kslots)
      }
    end

    # return kit names identified by SpecKit keys
    def select_kits(params)
      ret = []
      get_kits.each {|e|
        ret << e.name if params.keys.inject(true) {|ok, k| ok && e[k] == params[k]}
      }
      return ret.uniq
    end

    # return kit identified by by name and equipment
    def kit_info(name, eqp=nil)
      return get_kits.find {|e| e.name == name && (eqp.nil? || e.eqp == eqp)} || ($log.warn "no kit #{name.inspect}"; return)
    end

  end


  # WBTQ/FabGUI, sends messages as FabGUI does
  class FabGUIResponse
    attr_accessor :mq, :_doc

    def initialize(env)
      @mq = MQ::JMS.new("wbtq.fabgui.#{env}")
    end

    # send the FabGUI wafer select response to WBTQ, return nil on error
    def engineering_kit_response(guid, wselect, kslots, params={})
      $log.info "engineering_kit_response #{guid.inspect}, #{wselect}, #{params}"
      @_doc = doc = REXML::Document.new('<?xml version="1.0" encoding="UTF-8"?>')
      tag1 = doc.add_element('soapenv:Envelope', {
        'xmlns:soapenv'=>'http://schemas.xmlsoap.org/soap/envelope/',
        'xmlns:xsi'=>'http://www.w3.org/2001/XMLSchema-instance',
        'xmlns:urn'=>'urn:FabGUI',
      })
      .add_element('soapenv:Body')
      .add_element('urn:ExtendedQueryResult')
      tagg = tag1.add_element('GUIElements')
      tagg.add_element('ID').text = 1
      tagp = tagg.add_element('parameters')
      tagp.add_element('name').text = 'Class'
      tagp.add_element('value').text = 'Label'
      tagp = tagg.add_element('parameters')
      tagp.add_element('name').text = 'Label'
      tagp.add_element('value').text = "Products for Kit with GUID=#{guid}"
      kslots.values.flatten.each_with_index {|slot, idx|
        tagg.add_element('ID').text = idx + 2
        tagp = tagg.add_element('parameters')
        tagp.add_element('name').text = 'Class'
        tagp.add_element('value').text = 'CheckBox'
        tagp = tagg.add_element('parameters')
        tagp.add_element('name').text = 'Value'
        tagp.add_element('value').text = wselect.include?(slot)
      }
      tag1.add_element('response').text = params[:response] || 'OK'
      ret = String.new
      doc.write(ret)
      return @mq.send_msg(ret)
    end

  end


  # act as WBTQ client, sending and receiving messages as SAP/PM does
  class WBTQClient
    attr_accessor :mq_request, :mq_response, :response, :_doc

    def initialize(env, params={})
      @mq_request = MQ::JMS.new("wbtq.request.#{env}", params)
      @mq_response = MQ::JMS.new("wbtq.reply.#{env}", params)
    end

    def inspect
      "#<#{self.class.name} #{@mq_request.inspect}>"
    end

    # parse a msg string or raw object, return Hash
    def parse_msg(msg)
      if msg.instance_of?(String)
        timestamp = Time.now  # use with care, this is not the receive timestamp
      else
        msg ||= @mq_response.msg  # for dev
        timestamp = @mq_response.msg_properties(msg)[:timestamp]
        msg = @mq_response.process_msg(msg)
      end
      rsp = XMLHash.xml_to_hash(msg, '/facadeCallResponse/transaction')
      @response = rsp  # for dev
      ret = {guid: rsp[:transaction][:parameters][:guid], timestamp: timestamp}
      items = rsp[:transaction][:result][:items][:item]
      items = [items] unless items.instance_of?(Array)
      items.each {|item|
        k = item[:parameterName]
        if k == 'MESSAGE'
          ret[k] ||= []
          ret[k] << item[:fieldValue]
        else
          ret[k] = item[:fieldValue]
        end
      }
      return ret
    end

    # receive a message after a request, can be a 2nd message
    def receive_msg(guid, params={})
      tend = Time.now + (params[:timeout] || 180)
      loop {
        timeout = tend - Time.now
        break if timeout <= 0
        msg = @mq_response.receive_msg(raw: true, timeout: timeout)
        next if msg.nil?
        ret = parse_msg(msg)
        msgguid = ret[:guid]
        ($log.info "  discarding msg with GUID #{msgguid.inspect}"; next) if guid && msgguid != guid
        return ret
      }
      $log.warn 'no response from WBTQ'
      return
    end

    def create_request(name, name2, guid, items)
      items['MERKNR'] ||= '0000'
      $log.info "#{name}, #{name2}, #{guid.inspect}, #{items}"
      @_doc = doc = REXML::Document.new('<?xml version="1.0" encoding="UTF-8"?>')
      tag1 = doc.add_element('facadeCall', {'type'=>'generic-idoc'}).add_element('transaction')
      tag1.add_element('name').text = name
      tag1.add_element('name2').text = name2
      tag2 = tag1.add_element('parameters')
      tag2.add_element('guid').text = guid
      tag3 = tag2.add_element('items')
      items.each_pair {|k, v|
        tagi = tag3.add_element('item')
        tagi.add_element('parameterName').text = k
        tagi.add_element('hierarchyLevel').text = 0
        tagi.add_element('tableIndex').text = 0
        tagi.add_element('referenceHierarchyLevel').text = 0
        tagi.add_element('fieldValue').text = v unless v.nil?  # for 'CHAMBER'=>nil
      }
      ret = String.new
      doc.write(ret)
      return ret
    end

    def cancel_kit(guid, params={})
      rq = create_request('WBTQ_Cancel', 'WBTQ_CA', guid, {})
      @mq_request.send_msg(rq)
    end

    def display_started_lots(guid, params={})
      rq = create_request('WBTQ_DisplayStartedLots', 'WBTQ_SL', guid, {})
      @mq_request.send_msg(rq) || return
      receive_msg(guid, params) unless params[:timeout] == 0
    end

    def engineering_flag(guid, eqp, params={})
      # has no leading order
      ri = {'ORDERNUMBER'=>params[:order] || '000013370815', 'EQUIPMENT'=>eqp}
      rq = create_request('WBTQ_SetEngineeringFlag', 'WBTQ_EK', guid, ri)
      @mq_request.send_msg(rq) || return
      receive_msg(guid, params) unless params[:timeout] == 0
    end

    def auto_waferselection_flag(guid, eqp, params={})
      # has no leading order
      ri = {'ORDERNUMBER'=>params[:order] || '000013370815', 'EQUIPMENT'=>eqp}
      rq = create_request('WBTQ_SetAutoWaferSelectionFlag', 'WBTQ_AEK', guid, ri)
      @mq_request.send_msg(rq) || return
      receive_msg(guid, params) unless params[:timeout] == 0
    end

    def stb_only(guid, spec, kitno, eqp, chamber, params={})
      ri = {'ORDERNUMBER'=>params[:order] || '000013370815',
            'SPECID'=>spec, 'KITNUMBER'=>kitno, 'EQUIPMENT'=>eqp, 'CHAMBER'=>chamber
      }
      ri['LEADINGORDER'] = params[:leadingorder] unless params[:leadingorder].nil?
      rq = create_request('WBTQ_STBOnly', 'WBTQ_SO', guid, ri)
      @mq_request.send_msg(rq) || return
      receive_msg(guid, params) unless params[:timeout] == 0
    end

    def build_kit(guid, spec, kitno, eqp, chamber, params={})
      ri = {'ORDERNUMBER'=>params[:order] || '000013370815',
            'SPECID'=>spec, 'KITNUMBER'=>kitno, 'EQUIPMENT'=>eqp, 'CHAMBER'=>chamber
      }
      ri['LEADINGORDER'] = params[:leadingorder] unless params[:leadingorder].nil?
      rq = create_request('WBTQ_BuildKit', 'WBTQ_RL', guid, ri)
      @mq_request.send_msg(rq) || return
      receive_msg(guid, params) unless params[:timeout] == 0
    end

    def stb_trigger(guid, params={})
      rq = create_request('WBTQ_STBTrigger', 'WBTQ_TR', guid, {})
      @mq_request.send_msg(rq) || return
      receive_msg(guid, params) unless params[:timeout] == 0
    end

  end


  # act as SAP/PM client, sending messages as WBTQ does (for special tests)
  class SAPClient
    attr_accessor :mq, :_doc

    def initialize(env, params={})
      @mq = MQ::JMS.new("wbtq.sap.#{env}", params)
    end

    def create_response(name, name2, guid, items)
      items['MERKNR'] ||= '0010'
      $log.info "#{name}, #{name2}, #{guid.inspect}, #{items}"
      @_doc = doc = REXML::Document.new('<?xml version="1.0" encoding="UTF-8"?>')
      tag1 = doc.add_element('facadeCallResponse', {'type'=>'generic-idoc'}).add_element('transaction')
      tag1.add_element('name').text = name
      tag1.add_element('name2').text = name2
      tag2 = tag1.add_element('parameters')
      tag2.add_element('guid').text = guid
      tag3 = tag2.add_element('items')
      tagi = tag3.add_element('item')
      tagi.add_element('parameterName').text = 'MERKNR'
      tagi.add_element('hierarchyLevel').text = 0
      tagi.add_element('tableIndex').text = 0
      tagi.add_element('referenceHierarchyLevel').text = 0
      tagi.add_element('fieldValue').text = items['MERKNR']
      #
      tagr = tag1.add_element('result')
      tagr.add_element('guid').text = guid
      tagr3 = tagr.add_element('items')
      items.each_pair {|k, v|
        tagi = tagr3.add_element('item')
        tagi.add_element('parameterName').text = k
        tagi.add_element('hierarchyLevel').text = 0
        tagi.add_element('tableIndex').text = 0
        tagi.add_element('referenceHierarchyLevel').text = 0
        tagi.add_element('fieldValue').text = v unless v.nil?  # for 'CHAMBER'=>nil
      }
      ret = String.new
      doc.write(ret)
      return ret
    end

    def send_spec_response(guid, params={})
      ri = {'MERKNR'=>params[:merknr] || '0010',
            'RETURNCODE'=>params[:returncode] || '901',
            'MESSAGE'=>params[:msg] || 'Specification read.'
      }
      rq = create_response('WBTQ_STBOnly', 'WBTQ_SO', guid, ri)
      return params[:nosend] ? rq : @mq.send_msg(rq, params)
    end

    def send_foup_response(guid, params={})
      ri = {'MERKNR'=>params[:merknr] || '0010',
            'RETURNCODE'=>params[:returncode] || '208',
            'MESSAGE'=>params[:msg] || 'Not enough source wafer available. No Foup available'
      }
      rq = create_response('WBTQ_STBOnly', 'WBTQ_SO', guid, ri)
      return params[:nosend] ? rq : @mq.send_msg(rq, params)
    end

  end

end


# executed on request of SAP admins to test their interface
def generate_wbtq2sap_messages(n=60, params={})
  wbtqpm = WBTQ::SAPClient.new('itdc')
  guid = params[:guid] || 1400010779000010
  sleeptime = params[:sleeptime] || 60
  $log.info "sending #{n} messages with #{sleeptime} s pause"
  sleep 3
  n.times {|i|
    $log.info i
    wbtqpm.send_spec_response(guid)
    sleep 0.5
    wbtqpm.send_foup_response(guid)
    sleep sleeptime
  }
  return wbtqpm
end
