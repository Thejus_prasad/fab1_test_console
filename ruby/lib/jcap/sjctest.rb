=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author:  ssteidte, 2013-02-26

Version: 21.03

History:
  2014-05-13 ssteidte, pass sorter and stocker as parameters
  2014-09-08 ssteidte, use rtdemuext instead of rtdqarules
  2014-10-10 ssteidte, may be used without a sorter, allowing access to sjcexec et al
  2015-01-22 ssteidte, inherit from SiView::Test
  2017-11-03 sfrieske, use wait_for instead of sleep in execute_verify
  2018-02-20 sfrieske, added job_interval, speed up cleanup_sort_requests for multiple SRs
  2019-01-14 sfrieske, new Derdack notifications i/f (@notifications instead of @derdack)
  2020-01-07 sfrieske, removed MDS#sjc_requests_carrier, added verify_sr_removed
  2020-11-12 sfrieske, added create_slotmap_verify
  2021-03-16 sfrieske, removed Requestor from delete requests (since v20.07)
  2021-04-21 sfrieske, moved @sorter and @pg form SiView::Test to SJC::Test
  2021-08-27 sfrieske, require waitfor instead of misc
  2021-12-06 sfrieske, sjc_tasks can search for tgt and src carriers by passing :carrier
=end

require 'derdack'
require 'siviewtest'
require 'util/waitfor'
require_relative 'sjc'


module SJC

  class Test < SiView::Test
    attr_accessor :sorter, :pg, :svnox, :mds, :notifications, :sjcrequest, :sr, :job_interval,
      :_slotmap    # for dev only

    def initialize(env, params={})
      super
      @sorter = params[:sorter] || 'UTS001'
      @pg = params[:pg] || @sv.eqp_info(@sorter).ports.first.pg
      begin
        # for create_wildcard_verify
        @svnox = SiView::MM.new(env, user: 'NOX-UNITTEST', password: :default)
      rescue
        $log.warn 'no SiView::Test instance for user NOX-UNITTEST'
      end
      @mds = params[:mds] || MDS.new(env)
      @sjcrequest = SJC::SJCRequest.new(env)
      @notifications = DerdackEvents.new(env) unless params[:notifications] == false
      @job_interval = params[:job_interval] || 330  # 5 min + slack
    end

    # remove all sort requests for a carrier (source and target) or parameters for sjc_requests, return true on success
    def cleanup_sort_requests(params)
      return true if params.instance_of?(String) && params.start_with?('EMPTY')
      srs = nil
      15.times {|i|   # enough time for wildcard sorter jobs (? sf 2019-03-04)
        if params.instance_of?(String)
          srids = @mds.sjc_tasks(carrier: params).collect {|st| st.srid}
          return true if srids.empty?
          srs = @mds.sjc_requests(srid: srids)
        else
          srs = @mds.sjc_requests(params)   # currently only key is :creator
        end
        return true if srs.empty?
        $log.info {"cleanup_sort_requests #{params}"} if i == 0
        srs.each_with_index {|sr, idx|
          sleep 2 if idx > 0
          @sjcrequest.delete(sr.srid)
        }
        sleep 5
      }
      $log.warn "stale SRs: #{srs.inspect}"
      return false
    end

    # clean up and stock in carrier, optionally fill with lot and its children, leave empty if lot name is "empty"
    #
    # return true on success
    def prepare_carrier(carrier, lot)
      # clean up and stock in carrier, remove all sort requests
      $log.info "prepare_carrier #{carrier.inspect}, #{lot.inspect}"
      cleanup_carrier(carrier, make_empty: true) || return
      cleanup_sort_requests(carrier) || return
      ##@sv.npw_reserve_cancel('', carrier)
      #
      if lot == 'empty'
        # verify carrier is empty
        res = @sv.lot_list_incassette(carrier)
        ($log.warn "  carrier #{carrier.inspect} contains the lots #{res.inspect}"; return) unless res.empty?
      else
        # put the lot and its children in the carrier and remove all lot holds
        @sv.lot_family(lot).each {|lot|
          return if @sv.lot_hold_release(lot, nil) != 0
          return if @sv.wafer_sort(lot, carrier, slots: 'empty') != 0
        }
      end
      return true
    end

    def verify_lot_openote(sorttype, lot, srccarrier, tgtcarrier, params={})
      $log.info "verify_lot_openote #{sorttype.inspect}, #{lot.inspect}, #{srccarrier.inspect}, #{tgtcarrier.inspect}, #{params}"
      user = params[:user] || @sv.user
      sorter = params[:sorter] || @sorter  # sorter is 'null' for wildcard jobs
      pg = params[:pg] || @pg
      tstart = params[:tstart] || Time.now
      stype = {'COMBINE'=>'combined', 'SEPARATE'=>'separated', 'TRANSFER'=>'transferred', 'MOVE'=>'moved'}[sorttype]
      s0 = "Lots are automatically #{stype} upon request by #{user} on #{sorter} [#{pg}].  Lots moved=["
      s1 = "#{lot}(#{srccarrier} -> #{tgtcarrier})"
      return wait_for(timeout: params[:timeout] || 150) {
        note = @sv.lot_openotes(lot).first
        if note && @sv.siview_time(note.time) >= tstart
          ($log.warn "  wrong openote:\n#{note}"; return) unless note.note.start_with?(s0) && note.note.include?(s1)
          true
        else
          false
        end
      }
    end

    def verify_sjc_notification(tstart, lot, srccarrier, tgtcarrier, params={})
      $log.info "verify_sjc_notification #{tstart}, #{lot.inspect}, #{srccarrier.inspect}, #{tgtcarrier.inspect}"
      msgs = @notifications.query(tstart: tstart, service: '%jCAP_AsyncNotification%', filter: {'Category'=>'SortJobError'})
      ($log.warn "  wrong number of notifications: #{msgs.inspect}"; return) if msgs.size != 1
      p = msgs.first
      ($log.warn "  wrong carrier: #{p['CarrierID'].inspect}"; return) if p['CarrierID'] != "#{srccarrier}, #{tgtcarrier}"
      ($log.warn "  wrong event: #{p['Event'].inspect}"; return) if p['Event'] != 'SORTJOBERROR'
      msg = p['Message']
      s0 = params[:msg_start] || "\nSortRequest execution failed"
      s1 = "Requested lot movements: #{lot}(#{srccarrier} -> #{tgtcarrier})\n"
      ($log.warn "  wrong message: #{msg.inspect}"; return) unless msg.start_with?(s0) && msg.end_with?(s1)
      return true
    end

    # verify a COMBINE request is created correctly, return true on success
    def verify_sr_combine(lots, tgtcarriers, params={})
      lots = [lots] if lots.instance_of?(String)
      srccarriers = params[:srccarriers] || @sv.lots_info(lots).collect {|li| li.carrier}
      srccarriers = [srccarriers] if srccarriers.instance_of?(String)
      tgtcarriers = [tgtcarriers] if tgtcarriers.instance_of?(String)
      $log.info "verify_sr_combine #{lots}, #{tgtcarriers}, #{params.merge(srccarriers: srccarriers)}"
      #
      # get sorter tasks for the lots
      timeout = params[:timeout] || @job_interval
      ntasks = params[:ntasks] || lots.size
      sts = nil
      $log.info "  waiting up to #{timeout} s for SJC sort task creation"
      wait_for(timeout: timeout) {
        sts = @mds.sjc_tasks(srclot: lots)  # avoid tgtcarrier, 'EMPTY' might have DIRTYFOUP requests
        sts.size == ntasks
      } || ($log.warn "wrong number of SJC sort tasks: #{sts.inspect}"; return)
      #
      # verify sort tasks, tgtcarrier must be any of tgtcarriers, but the same for all tasks
      stsrids = []
      stsrccarriers = []
      sttgtcarriers = []
      sts.each {|st|
        stsrids << st.srid
        stsrccarriers << st.srccarrier
        sttgtcarriers << st.tgtcarrier
      }
      ($log.warn "  more than one SJC sort request: #{stsrids}"; return) if stsrids.uniq.size != 1
      ($log.warn "  more than one tgtcarrier: #{sttgtcarriers}"; return) if sttgtcarriers.uniq.size != 1
      tgtcarrier = sttgtcarriers.first
      ($log.warn "  wrong tgtcarrier: #{st.tgtcarrier}"; return) unless tgtcarriers.include?(tgtcarrier)
      if params[:ntasks]
        # in case ntasks is set there are more candidate lots than expected tasks
        ($log.warn "  wrong number of srccarriers: #{stsrccarriers}"; return) if stsrccarriers.size != params[:ntasks]
      else
        ($log.warn "  wrong srccarrier: #{stsrccarriers}"; return) if stsrccarriers.uniq.sort != srccarriers.uniq.sort
      end
      #
      # analyze the sort request
      @sr = @mds.sjc_requests(srid: sts.first.srid).first
      $log.info "  SR id #{@sr.srid} has been created"
      ($log.warn "  wrong SR status #{sr.status}"; return) if @sr.status != 'UNPROCESSED'
      #
      return true
    end

    # verify a SEPARATE request is created correctly, return true on success
    def verify_sr_separate(lots, srccarrier, tgtcarriers, params={})
      $log.info "verify_sr_separate #{lots}, #{srccarrier.inspect}, #{tgtcarriers}"
      # verify sort request has been created
      sleep 10
      sts = @mds.sjc_tasks(srccarrier: srccarrier)
      ($log.warn '  no SJC sort request'; return) if sts.empty?
      @sr = @mds.sjc_requests(srid: sts.first.srid).first   # a carrier can only be part of 1 SR
      $log.info "  SR id #{@sr.srid} has been created"
      ($log.warn "  wrong SR status #{sr.status}"; return) if @sr.status != 'UNPROCESSED'
      # verify sort tasks
      carriers = tgtcarriers.clone   # may be ['EMPTY.1', ...]
      lots.each {|lot|
        sts = @mds.sjc_tasks(srclot: lot)
        $log.info "  verfying sort task for lot #{lot.inspect}, tgtcarrier candidates: #{carriers}"
        ($log.warn "    wrong number of sort tasks: #{sts.size}"; return) if sts.size != 1
        st = sts.first
        ($log.warn "    wrong source carrier: #{st.srccarrier}"; return) if st.srccarrier != srccarrier
        ($log.warn "    wrong target carrier: #{st.tgtcarrier}"; return) unless carriers.member?(st.tgtcarrier)
        carriers.delete_at(carriers.index(st.tgtcarrier))
      }
      return true
    end

    # verify a TRANSFER request is created correctly, return true on success
    def verify_sr_transfer(lots, srccarrier, tgtcarrier, params={})
      $log.info "verify_sr_transfer #{lots}, #{srccarrier.inspect}, #{tgtcarrier.inspect}"
      # verify sort request has been created
      sleep 10
      sts = @mds.sjc_tasks(srccarrier: srccarrier)
      ($log.warn '  no SJC sort request'; return) if sts.empty?
      @sr = @mds.sjc_requests(srid: sts.first.srid).first   # a carrier can only be part of 1 SR
      $log.info "  SR id #{@sr.srid} has been created"
      ($log.warn "  wrong SR status #{sr.status}"; return) if @sr.status != 'UNPROCESSED'
      # verify sort tasks
      lots.each {|lot|
        sts = @mds.sjc_tasks(srclot: lot)
        $log.info "  verfying sort task for lot #{lot.inspect}"
        ($log.warn "    wrong number of sort tasks: #{sts.size}"; return) if sts.size != 1
        st = sts.first
        ($log.warn "    wrong source carrier: #{st.srccarrier}"; return) if st.srccarrier != srccarrier
        ($log.warn "    wrong target carrier: #{st.tgtcarrier}"; return) if st.tgtcarrier != tgtcarrier
      }
      return true
    end

    # verify the current SR gets removed from T_SJC_SORT_REQUEST, else MDS event config may be missing
    #
    # return true on success
    def verify_sr_removed(params={})
      $log.info "verify_sr_removed #{params}"
      res = wait_for(timeout: params[:timeout] || 125) {@mds.sjc_requests(srid: @sr.srid).empty?}
      $log.warn {"SR #{@sr.srid} not deleted"} unless res
      return res
    end

    # execute a sort request (incl SJC request update), return SiView sorter job on success
    def execute_verify(params={})
      sr = params[:sr] || @sr
      srid = params[:srid] || sr.srid
      sorter = params[:sorter] || @sorter
      pg = params[:pg] || @pg
      $log.info "execute_verify srid: #{srid}"
      @sjcrequest.execute(srid, sorter, pg) || ($log.warn 'error sending execute message'; return)
      $log.info 'verifying SiView SJ creation'
      wait_for(timeout: params[:timeout] || 60) {
        @sr = @mds.sjc_requests(srid: srid).first
        @sr && @sr.sjid
      } || ($log.warn "  no SiView sorter job: #{@sr.inspect}"; return)
      return @sr.sjid
    end

    # create and verify sort requests

    # return true on success
    def create_combine_verify(srccarriers, tgtcarrier, params={})
      srccarriers = [srccarriers] if srccarriers.instance_of?(String)
      $log.info "create_combine_verify #{srccarriers.inspect}, #{tgtcarrier.inspect}, #{params}"
      #
      # pick one lot from each src carrier unless specified
      lots = params[:lots] || srccarriers.collect {|c| @sv.lot_list_incassette(c)[0]}
      ($log.warn "  not all src carriers have a lot: #{lots.inspect}"; return) if lots.index(nil)
      @sr = nil
      # create new COMBINE request for all lots with the same target carrier
      @sjcrequest.create('COMBINE', params.merge(lots: lots, srccarriers: srccarriers, tgtcarriers: tgtcarrier)) || return
      # verify SR and set @sr on success
      return verify_sr_combine(lots, tgtcarrier, timeout: 30)
    end

    # return true on success
    def create_separate_verify(srccarrier, tgtcarriers, params={})
      tgtcarriers = [tgtcarriers] if tgtcarriers.instance_of?(String)
      $log.info "create_separate_verify #{srccarrier.inspect}, #{tgtcarriers.inspect}, #{params}"
      #
      # ensure the src carrier has enough lots, limit to number of tgtcarriers
      lots = @sv.lot_list_incassette(srccarrier).take(tgtcarriers.size)
      ($log.warn "  not enough lots in #{srccarrier}"; return) unless tgtcarriers.size == lots.size
      @sr = nil
      #
      # create new SEPARATE request for all lots with the same source carrier
      if wafers = params[:wafers]
        @sjcrequest.create_slots('SEPARATE', wafers, params[:tgtslots],
          lots: lots, srccarriers: srccarrier, tgtcarriers: tgtcarriers) || return
      else
        @sjcrequest.create('SEPARATE', params.merge(lots: lots, srccarriers: srccarrier, tgtcarriers: tgtcarriers)) || return
      end
      # verify SR and set @sr on success
      return verify_sr_separate(lots, srccarrier, params[:st_carriers] || tgtcarriers, timeout: 30)
    end

    # return true on success
    def create_transfer_verify(srccarrier, tgtcarrier, params={})
      $log.info "create_transfer_verify #{srccarrier.inspect}, #{tgtcarrier.inspect}, #{params}"
      lots = @sv.lot_list_incassette(srccarrier)
      ($log.warn '  src carrier is empty'; return) if lots.empty?
      # create new TRANSFER request (no lots passed), no wafers/tgtslots
      @sjcrequest.create('TRANSFER', srccarriers: srccarrier, tgtcarriers: tgtcarrier) || return
      # verify SR and set @sr on success
      return verify_sr_transfer(lots, srccarrier, tgtcarrier, timeout: 30)
    end

    # sorttype is COMBINE, SEPARATE or TRANSFER, return true on success
    def create_byslotmap_verify(sorttype, slotmap)
      @_slotmap = slotmap  # for dev only!
      lots, srccarriers, tgtcarriers = [], [], []
      slotmap.each {|e|
        unless lots.member?(e[:lot])
          lots << e[:lot]
          srccarriers << e[:srccarrier] ##unless srccarriers.member?(e[:srccarrier])
          tgtcarriers << e[:tgtcarrier] ##unless tgtcarriers.member?(e[:tgtcarrier])
        end
      }
      $log.info "create_byslotmap_verify #{sorttype.inspect}"
      @sjcrequest.create_byslotmap(sorttype, slotmap) || ($log.warn 'CREATE request error'; return)
      if sorttype == 'COMBINE'
        verify_sr_combine(lots, tgtcarriers, srccarriers: srccarriers, timeout: 30) || return
      elsif sorttype == 'SEPARATE'
        verify_sr_separate(lots, srccarriers.first, tgtcarriers, timeout: 30) || return
      elsif sorttype == 'TRANSFER'
        verify_sr_transfer(lots, srccarriers.first, tgtcarriers.first, timeout: 30) || return
      end
      # send Execute message
      sjid = execute_verify || return
      # execute the sorter job
      tstart = Time.now
      @sv.claim_sj_execute(sjid: sjid) || return
      # verify lots are in the new carrier(s) and have an operation note
      lis_new = @sv.lots_info(lots, wafers: true)
      lis_new.each {|li|
        srccarrier = slotmap.find {|e| e[:lot] == li.lot}[:srccarrier]
        tgtcarrier = slotmap.find {|e| e[:lot] == li.lot}[:tgtcarrier]
        ($log.warn "wrong carrier #{li.carrier.inspect} for lot #{li.lot}"; return) if tgtcarrier != li.carrier
        verify_lot_openote(sorttype, li.lot, srccarrier, tgtcarrier, tstart: tstart) || return
      }
      # verify new slots
      $log.info 'verifying slotmap'
      wafers = lis_new.collect {|li| li.wafers}.flatten
      slotmap.each {|e|
        tgtslot = e[:tgtslot]
        curslot = wafers.find {|w| w.wafer == e[:wafer]}.slot
        ($log.warn "wrong slot #{curslot}, expected #{e}"; return) if curslot != tgtslot
      }
      # verify SR entry is removed from T_SJC_SORT_REQUEST
      return verify_sr_removed
    end

    # create a SiView wildcard SJ, wait for SR creation by CaptureWildcardSorterJobs and verify SR
    #
    # return SJ id on success or nil
    def create_wildcard_verify(lot, tgtcarrier, params={})
      $log.info "create_wildcard_verify #{lot.inspect}, #{tgtcarrier.inspect}, #{params}"
      #
      # create wildcard sorter job, user name must not start with X- to prevent automatic deletion on status Error
      sjid = @svnox.sj_create('*', '*', '*', lot, tgtcarrier, slots: params[:slots])
      ($log.warn '  error creating sorter job'; return) unless sjid
      $log.info "  created sorter job #{sjid.identifier}"
      #
      # wait for CaptureWildcardSorterJobs
      sleeptime = params[:sleeptime] || 180
      $log.info "waiting up to #{sleeptime} s for the CaptureWildcardSorterJob to run"
      srs = []
      wait_for(timeout: sleeptime) {
        srids = @mds.sjc_tasks(tgtcarrier: tgtcarrier).collect {|st| st.srid}
        srs = @mds.sjc_requests(srids: srids, creator: 'SIVIEW') unless srids.empty?
        !srs.empty?
      }
      #
      # verify sort request has been created
      ($log.warn '  wrong number of SJC sort requests'; return) if srs.size != 1
      @sr = srs.first
      $log.info "  SR id #{@sr.srid} has been created"
      ($log.warn "  wrong SR status #{sr.status}"; return) if @sr.status != 'UNPROCESSED'
      #
      # verify sort task
      sts = @mds.sjc_tasks(srclot: lot)
      $log.info "  verfying sort task for lot #{lot}"
      ($log.warn "    wrong number of sort tasks: #{sts.size}"; return) if sts.size != 1
      st = sts.first
      ($log.warn "    wrong source carrier: #{st.srccarrier}"; return) if st.srccarrier != @sv.lot_info(lot).carrier
      ($log.warn "    wrong target carrier: #{st.tgtcarrier}"; return) if st.tgtcarrier != tgtcarrier
      #
      return sjid
    end

  end

end
