=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author:  ssteidte, 2011-07-27

Version: 21.03

History:
  2012-08-23 ssteidte, remove to_char conversion from queries for Date and Time fields
  2015-04-07 ssteidte, minor cleanup
  2015-09-18 ssteidte, convert BigNum to int
  2016-08-19 sfrieske, remove unused TemporalSpaceIncreaser queries
  2016-08-30 sfrieske, use lib rqrsp_http instead of rqrsp
  2016-11-15 sfrieske, added FAILURE_REASON and UPDATE_TIME to dirty_foup_report
  2017-04-26 sfrieske, use XMLHash instead of Builder
  2018-04-06 sfrieske, minor cleanup
  2018-11-07 sfrieske, removed QA Adapter interface, refactored
  2019-09-24 sfrieske, enhanced SJCRequest#create for tgtslots
  2020-01-07 sfrieske, removed MDS#sjc_requests_carrier
  2021-03-16 sfrieske, removed Requestor from delete requests (since v20.07)
  2021-12-06 sfrieske, sjc_tasks can search for tgt and src carriers by passing :carrier
=end

require 'rexml/document'
require 'dbsequel'
require 'rqrsp_http'


module SJC
  Request = Struct.new(:srid, :sjid, :timestamp, :creator, :sorter, :pg, :sorttype, :status,
    :version, :memo, :hint, :retrycount, :modified)
  Task = Struct.new(:stid, :srid, :srccarrier, :srccat, :srclot, :tgtcarrier, :tgtcat, :tgtlot)
  DirtyCarrier = Struct.new(:carrier, :category, :seqno, :empty, :status, :classification, :reason, :timestamp)


  # SJC SOAP interfaces used by RTD
  class SJCRequest
    attr_accessor :httpxml, :_doc, :_res

    def initialize(env, params={})
      @httpxml = RequestResponse::HttpXML.new("sjc.#{env}", params)
    end

    def new_doc
      @_doc = doc = REXML::Document.new('<?xml version="1.0" encoding="UTF-8"?>')
      return doc.add_element('soapenv:Envelope', {
        'xmlns:soapenv'=>'http://schemas.xmlsoap.org/soap/envelope/',
        'xmlns:ns2'=>'http://com.globalfoundries.sortrequest'
      }).add_element('soapenv:Body')
    end

    # create a SEPARATE, COMBINE or TRANSFER request, return true on success
    def create(sorttype, params={})
      $log.info "SJCRequest create #{sorttype.inspect}, #{params}"
      lots = params[:lots] || ['']  # '' makes only sense for TRANSFER to get 1 record with empty lot
      lots = [lots] unless lots.instance_of?(Array)
      srccarriers = params[:srccarriers] || ''
      srccarriers = [srccarriers] * lots.count unless srccarriers.instance_of?(Array)
      tgtcarriers = params[:tgtcarriers] || ''
      tgtcarriers = [tgtcarriers] * lots.count unless tgtcarriers.instance_of?(Array)
      tag = new_doc.add_element('ns2:create')
      lots.each_index {|i|
        tag.add_element('Seq').text = 123
        tag.add_element('SortType').text = sorttype
        tag.add_element('Hint').text = params[:hint] || 'QATest'
        tag.add_element('Requestor').text = params[:requestor] || 'X-UNITTEST'  # GENERATED_BY
        # not honoured ctag.add_element('Scenario').text = params[:scenario] || 'QATest'
        tag.add_element('SourceCarrier').text = srccarriers[i]
        tag.add_element('SourceCarrierCategory').text = params[:srccarrier_cat] || ''
        tag.add_element('Lot').text = lots[i]
        tag.add_element('TargetCarrier').text = tgtcarriers[i]
        tag.add_element('TargetCarrierCategory').text = params[:tgtcarrier_cat] || ''
      }
      @_res = res = @httpxml.post(tag.document, resource: 'RTDCreateSortRequestFacade') || return
      return res.has_key?(:createResponse)
    end

    # create a SEPARATE, COMBINE or TRANSFER request by slotmap, return true on success
    # slotmap is an array of {lot: .., wafer: .., srccarrier: .., tgtcarrier: .., tgtslot: ..}
    def create_byslotmap(sorttype, slotmap, params={})
      $log.info "SJCRequest create_byslotmap #{sorttype.inspect}"
      requestor = params[:requestor] || 'X-UNITTEST'
      tag = new_doc.add_element('ns2:create')
      slotmap.each {|e|
        tag.add_element('Seq').text = 123
        tag.add_element('SortType').text = sorttype
        tag.add_element('Hint').text = params[:hint] || 'QATest'
        tag.add_element('Requestor').text = requestor
        tag.add_element('SourceCarrier').text = e[:srccarrier]
        tag.add_element('SourceCarrierCategory').text = params[:srccarrier_cat] || ''
        tag.add_element('Lot').text = e[:lot]
        tag.add_element('TargetCarrier').text = e[:tgtcarrier]
        tag.add_element('TargetCarrierCategory').text = params[:tgtcarrier_cat] || ''
        tag.add_element('Wafer').text = e[:wafer]
        tag.add_element('WaferTargetPosition').text = e[:tgtslot]
      }
      @_res = res = @httpxml.post(tag.document, resource: 'RTDCreateSortRequestSlots') || return
      return res.has_key?(:createResponse)
    end

    def delete(srid)
      $log.info "SJCRequest delete #{srid}"
      tag = new_doc.add_element('ns2:delete')
      tag.add_element('SortRequestId').text = srid
      res = @httpxml.post(tag.document, resource: 'RTDDeleteSortRequestFacade') || return
      return res.has_key?(:deleteResponse)
    end

    def execute(srids, sorters, pgs='PG1')
      srids = [srids] unless srids.instance_of?(Array)
      sorters = [sorters] unless sorters.instance_of?(Array)
      pgs = [pgs] unless pgs.instance_of?(Array)
      $log.info "SJCRequest execute #{srids}, #{sorters}, #{pgs}"
      tag = new_doc.add_element('ns2:execute')
      srids.each_index {|i|
        tag.add_element('SortRequestId').text = srids[i]
        tag.add_element('SorterId').text = sorters[i]
        tag.add_element('PortGroup').text = pgs[i]
      }
      res = @httpxml.post(tag.document, resource: 'RTDExecuteSortRequestFacade') || return
      return res.has_key?(:executeResponse)
    end

  end


  # SJC DB interface
  class MDS
    attr_accessor :db

    def initialize(env, params={})
      @db = params[:db] || DB.new("mds.#{env}")
    end

    def inspect
      return "<#{self.class.name} #{@db.inspect}>"
    end

    def sjc_requests(params={})
      table = params[:history] ? 'T_SJC_SORT_REQUEST_HIST' : 'T_SJC_SORT_REQUEST'
      qargs = []
      q = "SELECT SORT_REQUEST_ID, SIVIEW_SJ_ID, REQUEST_DATE, GENERATED_BY, SORTER_ID, PORT_GROUP,
        SORT_TYPE, STATUS, VERSION, CLAIM_MEMO, HINT, RETRY_CNT, MODIFIED_DATE from #{table}"
      q, qargs = @db._q_restriction(q, qargs, 'SORT_REQUEST_ID', params[:srid])
      q, qargs = @db._q_restriction(q, qargs, 'GENERATED_BY', params[:creator])
      q, qargs = @db._q_restriction(q, qargs, 'SORT_TYPE', params[:sorttype])
      q, qargs = @db._q_restriction(q, qargs, 'STATUS', params[:status])
      q += ' ORDER BY SORT_REQUEST_ID'
      res = @db.select(q, *qargs) || return
      return res.collect {|r|
        [0, 8, 11].each {|i| r[i] = r[i].to_i if r[i]}
        Request.new(*r)
      }
    end

    def sjc_tasks(params={})
      table = params[:history] ? 'T_SJC_SORT_TASK_HIST' : 'T_SJC_SORT_TASK'
      qargs = []
      q = "SELECT SORT_TASK_ID, SORT_REQUEST_ID, SOURCE_CARRIER_ID, SOURCE_CARRIER_CATEGORY, SOURCE_LOT_ID,
        TARGET_CARRIER_ID, TARGET_CARRIER_CATEGORY, TARGET_LOT_ID from #{table}"
      q, qargs = @db._q_restriction(q, qargs, 'SORT_TASK_ID', params[:stid])
      q, qargs = @db._q_restriction(q, qargs, 'SORT_REQUEST_ID', params[:srid])
      q, qargs = @db._q_restriction(q, qargs, 'SOURCE_CARRIER_ID', params[:srccarrier])
      q, qargs = @db._q_restriction(q, qargs, 'SOURCE_CARRIER_CATEGORY', params[:srccat])
      q, qargs = @db._q_restriction(q, qargs, 'SOURCE_LOT_ID', params[:srclot])
      q, qargs = @db._q_restriction(q, qargs, 'TARGET_CARRIER_ID', params[:tgtcarrier])
      q, qargs = @db._q_restriction(q, qargs, 'TARGET_CARRIER_CATEGORY', params[:tgtcat])
      q, qargs = @db._q_restriction(q, qargs, 'TARGET_LOT_ID', params[:tgtlot])
      if carrier = params[:carrier]
        q, qargs = @db._q_restriction(q, qargs, '(SOURCE_CARRIER_ID', carrier)
        q += ' OR'
        q, qargs = @db._q_restriction(q, qargs, 'TARGET_CARRIER_ID', carrier)
        q += ')'
      end
      q += ' ORDER BY SORT_TASK_ID'
      res = @db.select(q, *qargs) || return
      return res.collect {|r|
        r[0] = r[0].to_i
        r[1] = r[1].to_i
        Task.new(*r)
      }
    end

    # filter by sjc_tasks parameters, for WBTQ and AVLS
    def sjc_wafers(params={})
      sts = sjc_tasks(params) || return
      table = params[:history] ? 'T_SJC_WFR_TRGT_POS_HIST' : 'T_SJC_WFR_TRGT_POS'
      qargs = []
      q = "SELECT WAFER_ID, TARGET_CARRIER_POS from #{table}"
      q, qargs = @db._q_restriction(q, qargs, 'SORT_TASK_ID', sts.collect {|st| st.stid})
      q += ' ORDER BY TARGET_POS_ID'
      res = @db.select(q, *qargs) || return
      ret = {}
      res.each {|r| ret[r[0]] = r[1].to_i}
      return ret
    end

    # for DirtyFoup
    def dirty_foup_report(params={})
      qargs = []
      q = 'SELECT CAST_ID, CAST_CATEGORY, SEQNO, EMPTY_FLAG, DRBL_STATE, CLASSIFICATION_ID,
        FAILURE_REASON, UPDATE_TIME FROM T_DIRTY_FOUP_REPORT'
      q, qargs = @db._q_restriction(q, qargs, 'CAST_ID', params[:carrier])
      q, qargs = @db._q_restriction(q, qargs, 'DRBL_STATE', params[:status])
      q, qargs = @db._q_restriction(q, qargs, 'CAST_CATEGORY', params[:carrier_category])
      q, qargs = @db._q_restriction(q, qargs, 'CLASSIFICATION_ID', params[:classification])
      q += ' ORDER BY SEQNO'
      res = @db.select(q, *qargs) || return
      return res.collect {|r|
        r[2] = r[2].to_i
        DirtyCarrier.new(*r)
      }
    end

  end

end
