=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author:     ssteidte, 2011-08-11
Version:    2.2.5

History:
  2012-04-30 ssteidte, use 'rqrsp' library
  2012-08-23 ssteidte, remove to_char conversion from queries for Date and Time fields
  2012-12-05 ssteidte, use new tables, separated MDS access code and test into wbtqdb.rb and wbtqtest.rb
  2013-12-10 ssteidte, added LEADINGORDER awareness and history lookup
  2014-05-13 ssteidte, pass default sorter, stocker, eqp etc. as initializer parameters
  2014-08-19 ssteidte, simplified setup by using Setup.FC
  2015-01-23 ssteidte, adjusted leadingorder checks with current SAP behaviour, added support for engineering select
  2015-07-22 ssteidte, added support for 1 kit with definitions for CH1 and CH11 (kits[:wrong_chamber])
  2015-09-29 sfrieske, prepare_src_carrier to build src carrier by passing the product-to-slot mapping
  2016-04-28 sfrieske, better handling of special kits to exclude, added kit with 25 wafers
  2017-02-10 sfrieske, quicker checks for cancel_kit
  2017-09-25 sfrieske, multiple MESSAGE items in WBTQ->SAP message
  2018-01-18 sfrieske, added build_kitgroup_verify, flattened KitCarrier struct and renamed to KitWafer
  2019-06-26 sfrieske, removed build_kitgroup_verify
  2020-04-23 sfrieske, added check for LeadingOrderCMMS
  2020-08-04 sfrieske, added auto_select_slots for Single Wafer Chamber Qual (SWC)
  2021-02-05 sfrieske, use instance_of instead of kind_of
  2021-05-17 sfrieske, added sublottype check (INC0971270)
  2021-08-27 sfrieske, require uniquestring instead of misc
=end

require 'util/uniquestring'
require 'util/waitfor'
require_relative 'sjctest'
require_relative 'wbtq'
require_relative 'wbtqdb'


module WBTQ

  # WBTQ Test interface
  class Test < SJC::Test
    KitWafer =  Struct.new(:carrier, :slot, :status, :product, :order, :lot, :wafer)

    attr_accessor :mds_sync, :client, :sfc, :fabgui, :order, :kits, :specialkits, :srcproduct, :eqp, :chamber,
      :rawproduct, :rawbank,
      # for timeout tests
      :stb_pre1, :stb_routeparameter


    def initialize(env, params={})
      super(env, {mds: ::WBTQ::MDS.new(env), carriers: 'TQ%', stocker: 'QASTOM1'}.merge(params))
      @mds_sync = params[:mds_sync] || 35
      @client = WBTQClient.new(env, params)
      @fabgui = FabGUIResponse.new(env)
      @srcproduct = params[:srcproduct] || 'B-100-CLUSTER.01'
      @rawproduct = params[:rawproduct] || 'UT-RAW.00'
      @rawbank = params[:rawbank] || 'WBTQ-DISPO'
      @eqp = params[:eqp] || 'QACLUST01'
      @chamber = params[:chamber] || 'CHA'
      # default SAP order number
      @order = params[:order] || '000013370815'
      # kit building Setup.FC spec
      @sfc = SFCClient.new(env, params[:spec] || 'C07-00000766')
      @sfc.get_kits
      # kits with special setup.FC or SiView setup
      @kits = {
        invalid_spec: ['QA-KIT00'], wrong_bank: ['QA-KIT15'], wrong_chamber: ['QA-KIT17'],
        srcprio: ['QA-KIT21', 'QA-KIT22', 'QA-KIT23', 'QA-KIT24'], many_lots: ['QA-KIT25']
      }
      @specialkits = @kits.values.flatten.uniq.sort
      # kits always requiring a new carrier (thus STBOnly will fail and BuildKit will alway create a SR)
      @kits[:new_carrier] = @sfc.select_kits(eqp: @eqp, new_carrier: true) - @specialkits
      # kits with successful stb_only when a carrier with matching src product slots and no additional wafers is available
      @kits[:stbonly_prebuilt] = @sfc.select_kits(eqp: @eqp, new_carrier: false) - @specialkits
      # kits with successful stb_only even with a carrier containing other lots
      @kits[:stbonly_other] = @sfc.select_kits(eqp: @eqp, new_carrier: false, allow_other_wafers: true) - @specialkits
      # kits with successful build_kit, with and without SR, provided a carrier containing other wafers is available
      @kits[:build_sr] = (@kits[:new_carrier] + @sfc.select_kits(eqp: @eqp, allow_other_wafers: false)).uniq - @specialkits
      @kits[:build_nosr] = @sfc.select_kits(eqp: @eqp, new_carrier: false, allow_other_wafers: true) - @specialkits
      # for the use product
      @stb_pre1 = 'UT-SLEEP-CONDRTE'
      @stb_routeparameter = 'UTSleep'
    end

    # return array of routes (not uniq!) used by the kit identified by name and eqp, for setup changes
    def kit_routes(name, eqp=nil)
      kinfo = @sfc.kit_info(name, eqp || @eqp) || return
      return kinfo.kit_slots.keys.collect {|product| @sv.CS_product_list(product: product).first.routeID}
    end

    # return array of KitWafer structs, sorted by slot
    def kitcarrier_info(carrier, params={})
      ret = []
      @sv.lot_list_incassette(carrier).each {|lot|
        porder = @sv.user_parameter('Lot', lot, name: 'SAPOrderNumber')
        order = porder.valueflag ? porder.value : nil
        li = @sv.lot_info(lot, wafers: true)
        li.wafers.each {|w|
          ret[w.slot] = KitWafer.new(li.carrier, w.slot, li.status, li.product, order, lot, w.wafer)
        }
      }
      return ret.compact  # remove all entries for empty slots
    end

    # pass an array of selected slots (as done in FabGUI by Engineer Select) and name: kit, eqp: eqp
    # return kit_slots hash with selected slots only, can be passed as kit_slots to verify_kit
    def engineering_kit_slots(selslots, params={})
      ekslots = {}
      kslots = params[:kit_slots] || @sfc.kit_info(params[:name], params[:eqp]).kit_slots
      kslots.each_pair {|product, slots|
        slots.each {|slot|
          next unless selslots.member?(slot)
          ekslots[product] ||= []
          ekslots[product] << slot
        }
      }
      return ekslots
    end

    # return kit_slots hash with selected slots only, can be passed as kit_slots to verify_kit
    def auto_select_slots(name, eqp, params={})
      $log.info "auto_select_slots #{name}, #{eqp}"
      eqpi = @sv.eqp_info(eqp)
      chambers = eqpi.chambers.collect {|ch| ch.chamber}
      ## is_eqpup = eqpi.status.end_with?('1PRD', '2WPR') # mainframe wafers are never deselected!
      ekslots = {}
      kslots = params[:kit_slots] || @sfc.kit_info(name, eqp).kit_slots
      kslots.each_pair {|product, slots|
        is_chproduct = false
        is_chup = false
        eqpi.chambers.each {|ch|
          if product.include?(ch.chamber)
            is_chproduct = true
            is_chup = ch.status.end_with?('1PRD', '2WPR')
            break
          end
        }
        ($log.info "  skipping #{product} (#{slots}) because chamber is down"; next) if is_chproduct && !is_chup
        # mainframe wafers are never deselected!
        ## ($log.info "  skipping #{product} (#{slots}) because mainframe is down"; next) if !is_chproduct && !is_eqpup
        ekslots[product] ||= []
        ekslots[product] += slots
      }
      return ekslots
    end

    # cancel existing kits identified by GUID, incl. wildcard ('QA%'), return true on success
    def cancel_kit_verify(guid)
      ret = true
      $log.info "cancel_kit_verify #{guid.inspect}"
      tstart = Time.now
	    @mds.contexts(guid: guid).each {|ctx|
        @client.cancel_kit(ctx.guid)
        wait_for(timeout: 120, sleeptime: 3, tstart: tstart) {
          @mds.contexts(guid: ctx.guid).empty?
        } || ($log.warn "  error canceling kit #{ctx.guid.inspect}"; ret=false)
  	  }
      return ret
    end

    # cancel pending test kits, ensure only configured carriers are available and in the right stocker
    # return true on success
    def prepare_resources(carriers=nil, params={})
      ret = true
      carriers = [] if carriers.nil?
      carriers = [carriers] if carriers.instance_of?(String)
      new_carrier = params[:new_carrier]
      carriers << new_carrier if new_carrier
      # set the test carriers and aux carriers AVAILABLE, all others NOTAVAILABLE
      available_carriers = params[:available_carriers] || []
      available_carriers = [available_carriers] if available_carriers.instance_of?(String)
      available_carriers << new_carrier if new_carrier
      available_carriers = (available_carriers + carriers).compact.sort.uniq
      $log.info "available carriers: #{available_carriers}"
      all_carriers = @sv.carrier_list(carrier: @carriers)
      all_carriers.uniq.sort.each {|c|
        status = available_carriers.member?(c) ? 'AVAILABLE' : 'NOTAVAILABLE'
        ret &= @sv.carrier_status_change(c, status, check: true).zero?
        ret &= cleanup_sort_requests(c)
        @mds.carrier_contexts(c).each {|guid| ret &= cancel_kit_verify(guid)}
      }
      # cancel all pending kit build requests
      ret &= cancel_kit_verify('QA%')
      # cleanup eqp
      ret &= @sv.eqp_cleanup(params[:eqp] || @eqp).zero?
      # stockin the carrier(s), remove left-over lot script parameters, put lots on the build route
      stocker = params[:stocker] || @stocker
      stockers = stocker.instance_of?(Array) ? stocker : Array.new(carriers.size, stocker)
      stockers << stocker if new_carrier
      product = params[:product] || @srcproduct
      products = product.instance_of?(Array) ? product : Array.new(carriers.size, product)
      products << product if new_carrier
      carriers.each_with_index {|c, i|
        cs = @sv.carrier_status(c)
        cs.lots.each {|lot|
          %w(QualEqpID SAPOrderNumber LeadingOrderCMMS).each {|p|
            # remove all WBTQ related lot script params
            if @sv.user_parameter('Lot', lot, name: p).valueflag
              ret &= @sv.user_parameter_change('Lot', lot, p, nil, 3, silent: true).zero?
            end
          }
        }
        ret &= cleanup_carrier(c, stocker: stockers[i], xjs: params[:xjs])  # speed up in case we know there are no xfer jobs
        ret &= rework_monitor_lots(c, product: products[i], lots: params[:lots] || cs.lots) unless cs.lots.empty?
      }
      # cleanup the conditional sleeps on all use routes
      products = @sfc.kits.collect {|k| k.lot_definitions.collect {|l| l.product}}.flatten.uniq
      products.collect {|p| @sv.CS_product_list(product: p).first.routeID}.uniq.each {|route|
        if @sv.user_parameter('Route', route, name: @stb_routeparameter).valueflag
          ret &= @sv.user_parameter_change('Route', route, @stb_routeparameter, nil, 3).zero?
        end
      }
      return ret
    end

    # prepare a src carrier with build products as specified, all current lots are removed
    #   pslots is an Array of hashes like {"B-100"=>[2], "B-200"=>[4,7], "B-300"=>2}
    #
    # return the collected lots in all carriers on success, else nil
    def prepare_src_carriers(carriers, pslots, params={})
      carriers = [carriers] unless carriers.instance_of?(Array)
      pslots = [pslots] unless pslots.instance_of?(Array)
      carriers.each_with_index.collect {|carrier, i|
        cleanup_carrier(carrier, make_empty: true, stocker: params[:stocker]) || return
        # create raw srclot with wafers from slot 1..last
        vlot1 = @sv.vendorlot_receive(@rawbank, @rawproduct, params[:nwafers] || 25) ||return
        vlot = @sv.vendorlot_prepare(vlot1, carrier) || return
        vlotempty = false
        # get product->slot map from kit definition if pslots is a string, thus building an ideal src carrier
        _psmap = pslots[i]
        # _psmap = @sfc.kit_slots(name: _psmap, eqp: params[:eqp] || @eqp, srcproduct: 0) if _psmap.instance_of?(String)
        if _psmap.instance_of?(String)
          # kit name passed
          kinfo = @sfc.kit_info(_psmap, params[:eqp] || @eqp)
          _psmap = {}
          _psmap[kinfo.lot_definitions.first.src_pattern.first] = kinfo.kit_slots.values.flatten.sort
        end
        # create the kit srcproduct lots
        lots = _psmap.each_pair.collect {|product, slots|
          if @sv.lot_info(vlot).nwafers > slots.size
            lc = @sv.lot_split_notonroute(vlot, slots) || return
          else
            lc = vlot
            vlotempty = true
          end
          @sv.stb_controllot('Equipment Monitor', product, lc)
        }
        # remove the other vlot wafers
        if params[:no_other_wafers] && !vlotempty
          @sv.delete_lot_family(vlot).zero? || return
        else
          lots.unshift(vlot)
        end
        lots
      }.flatten
    end

    # rework the lots on monitor routes ("move" from use product to build product), return true on success
    def rework_monitor_lots(carrier, params={})
      product = params[:product] || @srcproduct
      lots = params[:lots] || @sv.lot_list_incassette(carrier)
      $log.info "rework_monitor_lots #{carrier.inspect}, product: #{product.inspect}"
      ($log.info "  carrier is empty"; return true) if lots.empty?
      $log.info "  carrier contains #{lots.size} lots"
      ret = true
      srclots = []
      # _wafers = 0
      # lotfamilies = []
      lots.each {|lot|
        @sv.lot_cleanup(lot, move: false, silent: true)
        li = @sv.lot_info(lot)
        next if li.lottype != 'Equipment Monitor'  # to allow dummy wafers in the carrier
        if li.product == product
          next if li.status == 'COMPLETED'
          ($log.warn "  error cleaning up lot #{lot.inspect}"; ret=false)
        end
        srclots << lot
        # rework lot
        if li.states['Lot Inventory State'] != 'InBank'
          @sv.lot_opelocate(lot, :last)
          @sv.lot_bankin(lot) if (@sv.lot_info(lot).states['Lot Inventory State'] != 'InBank')
        end
        # _wafers += li.nwafers
      }
      # # prevent increasing fragmentation by not executed SRs, TODO: use lots directly
      # lotfamilies.uniq.sort.each {|lot|
      #   (@sv.lot_family(lot) - [lot]).each {|lc| @sv.lot_merge(lot, lc)}
      # }
      # convert use product wafers to build product wafers
      # ret &= !!@sv.stb_controllot('Equipment Monitor', product, srclots) if _wafers > 0
      ret &= !!@sv.stb_controllot('Equipment Monitor', product, srclots) unless srclots.empty?
      #
      return ret
    end

    # verify WBTQ response (as Hash or raw MQ message), return true on success
    def verify_response(response, rc, msg, params={})
      $log.info "verify_response #{rc.inspect}, #{msg.inspect}"
      ($log.warn "  no response from WBTQ"; return) unless response
      response = @client.parse_msg(response) unless response.instance_of?(Hash)
      ret = true
      rc_actual = response['RETURNCODE']
      ($log.warn "  wrong WBTQ RETURNCODE #{rc_actual}"; ret=false) if rc_actual != rc
      msg_actual = response['MESSAGE'].join('\n')  # multiple MESSAGE items since 17.5 allowed
      unless msg_actual.include?(msg)
        $log.warn "  wrong WBTQ MESSAGE\n#{response['MESSAGE'].pretty_inspect}"
        ret = false
      end
      timestamp = response[:timestamp]
      if mintime = params[:mintime]
        $log.info "  msg timestamp > #{mintime}"
        ($log.warn "  message sent early (#{timestamp})"; ret=false) if timestamp < mintime
      end
      if maxtime = params[:maxtime]
        $log.info "  msg timestamp < #{maxtime}"
        ($log.warn "  message sent late (#{timestamp})"; ret=false) if timestamp > maxtime
      end
      return ret
    end

    # return true on success
    def verify_kit(guid, state, errorstate, params={})
      history = !!params[:history]
      $log.info "verify_kit #{guid.inspect}, #{state.inspect}, #{errorstate.inspect}"
      pparams = params.select {|p| !p.nil?}
      $log.info params unless pparams.empty?
      ctx = @mds.contexts(guid: guid, history: history).last || ($log.warn '  WBTQ has not created the kit'; return)
      $log.info '  note: ENGINEERING_FLAG is set' if ctx.eng == 'Y'
      $log.info '  note: AUTO_WAFER_SELECTION_FLAG is set' if ctx.autosel == 'Y'
      ret = true
      # state
      $log.info "  state: #{state}"
      ($log.warn "    wrong state: #{ctx.state}"; ret=false) if ctx.state != state
      # errorstate
      $log.info "  errorstate: #{errorstate}"
      ($log.warn "    wrong errorstate: #{ctx.errorstate}"; ret=false) if ctx.errorstate != errorstate
      return ret if state == 'ENGINEERING_KIT'
      leadingorder = params[:leadingorder]
      if leadingorder
        $log.info "  leadingorder: #{leadingorder}"
        ($log.warn "    wrong leadingorder: #{ctx.leadingorder}"; ret=false) if ctx.leadingorder != leadingorder
      end
      return ret if state == 'ENGINEERING_SELECTION'
      return ret if errorstate == 'NO_KIT' || params[:carrier_check] == false
      #
      # used carrier
      ccarriers = @mds.context_carriers(guid) || ($log.warn '  MDS communication error'; return)
      carrier = params[:carrier]
      if carrier
        $log.info "  verifying carrier is #{carrier.inspect}"
        ($log.warn "    wrong source lot carrier: #{ccarriers}"; ret=false) if ccarriers != [carrier]
      else
        ($log.warn "  multiple carriers, cannot decide: #{ccarriers.inspect}"; return) if ccarriers.size > 1
        carrier = ccarriers.first
      end
      # kit info
      kinfo = params[:kit_info] || @sfc.kit_info(ctx.kit, ctx.eqp) || return
      #
      # sort request
      kslots = params[:kit_slots] || kinfo.kit_slots || return  # for engineering kits pass the selected kit slots
      sr_created = params.has_key?(:sr_created) ? params[:sr_created] : ['SORT_REQUEST_CREATED'].member?(state)
      if sr_created
        ($log.warn "  no SR for GUID #{guid.inspect}"; return) unless ctx.srid && ctx.srid > 0
        $log.info "  SR created: #{ctx.srid}"
        $log.info '  SR wafer slots'
        srwafers = @mds.sjc_wafers(srid: ctx.srid, history: history)
        ($log.warn '    no SR wafer info found'; return) if srwafers.empty?
        kslots.each_pair {|p, slots|
          slots.each {|slot|
            ($log.warn "    wrong slots for #{p}, slot #{slot}"; return) if srwafers.values.find {|v| v == slot}.nil?
          }
        }
        $log.info "  SR srccarrier: #{carrier}"
        sts = @mds.sjc_tasks(srid: ctx.srid, history: history) || ($log.warn "    no SJC tasks for SRID #{ctx.srid}"; return)
        sts.each {|st| ($log.warn "    wrong carrier: #{st.inspect}"; return) if st.srccarrier != carrier}
      else
        $log.info '  no SR created'
        ($log.warn "    wrong SR created: #{ctx.srid}"; ret=false) if ctx.srid && ctx.state != 'FINISHED'   # FINISHED when STBTrigger after SR
      end
      return ret if ['READY_FOR_STB', 'SORT_REQUEST_CREATED'].member?(state) || ['NOT_ENOUGH_WAFER'].member?(errorstate)
      #
      # products in the carrier
      $log.info "  verifying products in carrier #{carrier}"
      kci = kitcarrier_info(carrier)
      kslots.each_pair {|p, slots|
        pwafers = kci.select {|w| w.product == p && w.order.to_i == ctx.order.to_i}
        # compare wafer count
        ($log.warn "  wrong number of wafers for product #{p}: #{pwafers.size} instead of #{slots.size}"; ret=false) if pwafers.size != slots.size
        # compare slots
        if kinfo.match_slots
          $log.info "  matching wafer slots for product #{p}"
          slots.sort.each_with_index {|slot, i|
            pw = pwafers[i]
            $log.debug "    #{slot}: #{pw.inspect}"
            ($log.warn "    no wafer in slot #{slot}"; ret=false; next) if pw.nil?
            ($log.warn "    mismatch in slot #{slot}"; ret=false) if pw.slot != slot
          }
        end
      }
      return ret unless ret
	  	#
      if kinfo.match_order && (params[:match_order] != false)  # pass match_order: false for engineering kits
        $log.info "  matching product order, other wafers allowed: #{kinfo.allow_other_wafers}"
        # compare product order
        products = []
        kslots.each_pair {|p, slots| slots.each {|slot| products[slot] = p}}
        products.compact!
        i = 0
        kci.each {|w|
          next if w.product != products[i] && kinfo.allow_other_wafers
          ($log.warn "    wrong product order for #{products[i]}: #{w.inspect}"; ret=false) if w.product != products[i]
          i += 1
        }
      end
      return ret if !ret || state != 'FINISHED' || errorstate != 'ALL_OK'
      #
      $log.info '  lot parameters'
      sleep 10  # avoiding the first error when asking for STB in SiView lot history
      products = kslots.keys.uniq
      lots = kci.collect {|w| w.lot if products.member?(w.product)}.compact.uniq
      lots.each {|lot|
        uparams = @sv.user_parameter('Lot', lot)
        eqpid = uparams.find {|e| e.name == 'QualEqpID'}
        ($log.warn "  wrong QualEqpID for #{lot}: #{eqpid.inspect}"; ret=false) if eqpid.value != ctx.eqp
        orderno = uparams.find {|e| e.name == 'SAPOrderNumber'}
        ($log.warn "  wrong SAPOrderNumber for #{lot}: #{orderno.inspect}"; ret=false) if orderno.value.to_i != ctx.order.to_i
        lorder = uparams.find {|e| e.name == 'LeadingOrderCMMS'}
        if leadingorder
          ($log.warn "  wrong LeadingOrderCMMS for #{lot}: #{lorder.inspect}"; ret=false) if lorder.value != ctx.leadingorder
        else
          ($log.warn "  unexpected LeadingOrderCMMS for #{lot}: #{lorder.inspect}"; ret=false) if lorder
        end
        li = @sv.lot_info(lot)
        ($log.warn "  #{lot} wrong sublottype"; ret=false) if li.sublottype != li.lottype  # combined SiView & facade bug
        ret &= wait_for(timeout: 60, sleeptime: 5) {
          his = @sv.lot_operation_history(lot, opCategory: 'STB', route: li.route, opNo: li.opNo, pass: 1, raw: true)
          if his.nil? || his.empty?
            $log.debug {'  no lot history, retrying'}
            false
          else
            memo = his.first.claimMemo
            cond = memo.include?(guid) && memo.include?(ctx.order)
            $log.warn {"  wrong STB claim memo: #{memo.inspect}"} unless cond
            cond
          end
        }
      }
      return ret
    end

    # create a dummy qual lot within the carrier, WBTQ will see it as qual lot and not touch it
    # return lot on success
    def create_quallot(srclot, product)
      $log.info "create_quallot #{srclot.inspect}, #{product.inspect}"
      # ensure srclot is banked in
      li = @sv.lot_info(srclot)
      ($log.warn '  srclot must be banked in'; return) unless li.states['Lot Inventory State'] == 'InBank'
      # stb
      qlot = @sv.stb_controllot(li.lottype, product, srclot, nwafers: 1) || return
      # set lot script parameter to mark the lot as part of a qual kit
      @sv.user_parameter_change('Lot', qlot, 'QualEqpID', 'QADummy').zero? || return
      return qlot
    end

    # basic kit build scenarios with default parameters (also suitable for negative tests)

    # send stb_only msg and verify the response, return guid on success or nil
    def stb_only(kit, rc, msg='', params={})
      guid = params[:guid] || unique_string('QA_')
      eqp = params[:eqp] || @eqp
      chamber = params.has_key?(:chamber) ? params[:chamber] : @chamber
      lorder = params.has_key?(:leadingorder) ? params[:leadingorder] : unique_string  # pass nil to suppress
      timeout = params[:timeout]
      timeout = 0 unless rc && msg
      @order = '0' + Time.now.to_i.to_s  #(@mds.contexts.collect {|e| e.order}.sort.last || @order).next
      res = @client.stb_only(guid, @sfc.spec, kit, eqp, chamber, order: @order, leadingorder: lorder, timeout: timeout)
      return guid if timeout == 0
      verify_response(res, rc, msg) || return
      return guid
    end

    # send build_kit msg and verify the response, return guid on success or nil
    def build_kit(kit, rc, msg='', params={})
      guid = params[:guid] || unique_string('QA_')
      eqp = params[:eqp] || @eqp
      chamber = params.has_key?(:chamber) ? params[:chamber] : @chamber
      lorder = params.has_key?(:leadingorder) ? params[:leadingorder] : unique_string
      timeout = params[:timeout]
      timeout = 0 unless rc && msg
      @order = '0' + Time.now.to_i.to_s  #(@mds.contexts.collect {|e| e.order}.sort.last || @order).next
      res = @client.build_kit(guid, @sfc.spec, kit, eqp, chamber, order: @order, leadingorder: lorder, timeout: timeout)
      return guid if timeout == 0
      verify_response(res, rc, msg) || return
      return guid
    end

    # send auto_waferselection_flag msg and verify the response, return guid on success or nil
    def auto_waferselection_verify(eqp)
      guid = unique_string('QAAEK_')
      res = @client.auto_waferselection_flag(guid, eqp) || return
      verify_response(res, '100', 'OK') || return
      verify_kit(guid, 'ENGINEERING_KIT', 'ALL_OK') || return
      return guid
    end


    # complete kit build scenarios with carrier preparation and verification, for positive tests

    # build a kit via stb_only and verify the kit, return GUID on success or nil
    def stb_only_verify(kit, carrier, params={}, &blk)
      # prepare
      prepare_resources(carrier, params) || ($log.warn "stb_only_verify: error preparing resources"; return)
      if block_given?
        $log.info "-- begin specific test preparation"
        res = blk.call
        $log.info "-- end specific test preparation"
        ($log.warn "specific test preparation failed"; return) unless res
      end
      $log.info "waiting #{@mds_sync} s for the MDS to sync"; sleep @mds_sync
      #
      guid = params[:guid] || unique_string('QA_')
      eqp = params[:eqp] || @eqp
      # send stb_only and verify kit
      lorder = params.has_key?(:leadingorder)? params[:leadingorder] : unique_string
      stb_only(kit, '100', "Target carrier: #{carrier}",
        params.merge(leadingorder: lorder, eqp: eqp, guid: guid)) || return
      verify_kit(guid, 'FINISHED', 'ALL_OK',
        carrier_check: params[:carrier_check], carrier: carrier, leadingorder: lorder) || return
      return guid
    end

    # send auto_waferselection_flag, build a kit via stb_only and verify the kit, return GUID on success or nil
    def auto_waferselection_stbonly_verify(kit, eqp, carrier, params={}, &blk)
      # prepare
      prepare_resources(carrier, eqp: eqp) || ($log.warn "auto_waferselection: error preparing resources"; return)
      if block_given?
        $log.info "-- begin eqp/chamber preparation"
        res = blk.call
        $log.info "-- end eqp/chamber preparation"
        ($log.warn "eqp/chamber preparation failed"; return) unless res
      end
      $log.info "waiting #{@mds_sync} s for the MDS to sync"; sleep @mds_sync
      #
      # send auto_waferselection_flag
      guid = auto_waferselection_verify(eqp) || return
      # send stb_only and verify kit
      lorder = params.has_key?(:leadingorder)? params[:leadingorder] : unique_string
      stb_only(kit, '100', "Target carrier: #{carrier}",
        params.merge(leadingorder: lorder, eqp: eqp, guid: guid)) || return
      ekslots = auto_select_slots(kit, eqp)
      verify_kit(guid, 'FINISHED', 'ALL_OK', carrier: carrier, leadingorder: lorder, kit_slots: ekslots) || return
      return guid
    end

    # build a kit via build_kit and stb_trigger and verify the kit, return GUID on success or nil
    def build_kit_verify(kit, carrier, params={}, &blk)
      # prepare
      prepare_resources(carrier, params) || ($log.warn 'build_kit_verify: error preparing resources'; return)
      if block_given?
        $log.info "-- begin specific test preparation"
        res = blk.call
        $log.info "-- end specific test preparation"
        ($log.warn "specific test preparation failed"; return) unless res
      end
      $log.info "waiting #{@mds_sync} s for the MDS to sync"; sleep @mds_sync
      #
      guid = params[:guid] || unique_string('QA_')
      eqp = params[:eqp] || @eqp
      stbtrigger = params.delete(:stbtrigger)
      # send build_kit and verify kit
      lorder = params.has_key?(:leadingorder)? params[:leadingorder] : unique_string
      bkparams = params.merge(leadingorder: lorder, eqp: eqp, guid: guid)
      if @kits[:build_nosr].member?(kit) || params[:nosr]  # pass nosr: true when a prebuilt carrier is used
        build_kit(kit, '100', "Target carrier: #{carrier}", bkparams) || return
        verify_kit(guid, 'READY_FOR_STB', 'ALL_OK', leadingorder: lorder) || return
        # bank holds have been created
      else
        build_kit(kit, '903', 'SortJobController sort request was created.', bkparams) || return
        verify_kit(guid, 'SORT_REQUEST_CREATED', 'ALL_OK', leadingorder: lorder) || return
        # bank holds have been created here and stay until after the sort request, verify later
        if new_carrier = params[:new_carrier]
          # assign empty carrier and execute SR (note: EMPTY rule must be served by RTDEmu)
          ctx = @mds.contexts(guid: guid).first || ($log.warn '  WBTQ has not created the kit'; return)
          @sjcrequest.execute(ctx.srid, @sorter, @pg) || return
          # wait for SJC to create the SJ and claim to execute it (sorter port is UnloadReq to prevent automated actions)
          wait_for {!@sv.sj_list(eqp: @sorter, carrier: carrier).empty?} || ($log.warn '  no SJ created'; return)
          @sv.claim_sj_execute(eqp: @sorter, report: false) || return
          # ensure both carriers are stocked in after the sj
          [carrier, new_carrier].each {|c|
            (@sv.carrier_xfer_status_change(c, '', @stocker) == 0) || return
          }
          # wait for SJC to report SR execution and verify new kit status
          state = nil
          wait_for {(state = @mds.contexts(guid: guid).first.state) == 'READY_FOR_STB'} || ($log.warn " wrong kit state: #{state}"; return)
          carrier = new_carrier
          $log.info "context #{guid} new status: 'READY_FOR_STB', new carrier: #{carrier}"
        else
          $log.warn 'SR not executed because no empty carrier was passed. STBTrigger will fail' if stbtrigger != false
        end
      end
      # verify bank holds regardless if status is READY_FOR_STB or SORT_REQUEST_CREATED
      # as slot and order have been checked in verify_kit it is sufficient to check the number of lots on hold
      $log.info '  verifying bank holds'
      nlots = @sfc.kit_info(kit, params[:eqp] || @eqp).kit_slots.values.flatten.size
      nholds = kitcarrier_info(carrier).count {|kwi| kwi.status == 'ONHOLD'}
      ($log.warn "    #{nholds} instead of #{nlots} lots are ONHOLD"; return) if nholds != nlots
      #
      # send stb_trigger request
      return guid if stbtrigger == false
      resp = @client.stb_trigger(guid) || return
      verify_response(resp, '100', carrier) || return
      wait_for(timeout: 90) {@mds.contexts(guid: guid).first.state == 'FINISHED'}    # kit status change might take a minute
      verify_kit(guid, 'FINISHED', 'ALL_OK', leadingorder: lorder) || return
      return guid
    end

    # send auto_waferselection_flag, call build_kit and stb_trigger and verify the kit, return GUID on success or nil
    def auto_waferselection_buildkit_verify(kit, eqp, carrier, params={}, &blk)
      # prepare
      prepare_resources(carrier, eqp: eqp) || ($log.warn "auto_waferselection: error preparing resources"; return)
      if block_given?
        $log.info "-- begin eqp/chamber preparation"
        res = blk.call
        $log.info "-- end eqp/chamber preparation"
        ($log.warn "eqp/chamber preparation failed"; return) unless res
      end
      $log.info "waiting #{@mds_sync} s for the MDS to sync"; sleep @mds_sync
      #
      # send auto_waferselection_flag
      guid = auto_waferselection_verify(eqp) || return
      # send build_kit and verify kit
      stbtrigger = params.delete(:stbtrigger)
      lorder = params.has_key?(:leadingorder)? params[:leadingorder] : unique_string
      if @sfc.select_kits(eqp: eqp, new_carrier: false, allow_other_wafers: true) || params[:nosr]  # pass nosr: true when a prebuilt carrier is used
        build_kit(kit, '100', "Target carrier: #{carrier}",
          params.merge(leadingorder: lorder, eqp: eqp, guid: guid)) || return
        verify_kit(guid, 'READY_FOR_STB', 'ALL_OK', carrier: carrier, leadingorder: lorder) || return
        # bank holds have been created
      else
        build_kit(kit, '903', 'SortJobController sort request was created.',
          params.merge(leadingorder: lorder, eqp: eqp, guid: guid)) || return
        verify_kit(guid, 'SORT_REQUEST_CREATED', 'ALL_OK', carrier: carrier, leadingorder: lorder) || return
        # bank holds have been created here and stay until after the sort request, verify later
        #
        # note: stbtrigger not supported with new_carrier, pass stbtrigger: false!
      end
      # verify bank holds regardless if status is READY_FOR_STB or SORT_REQUEST_CREATED
      # as slot and order have been checked in verify_kit it is sufficient to check the number of lots on hold
      $log.info '  verifying bank holds'
      nlots = @sfc.kit_info(kit, eqp).kit_slots.values.flatten.size
      nholds = kitcarrier_info(carrier).count {|kwi| kwi.status == 'ONHOLD'}
      ($log.warn "    #{nholds} instead of #{nlots} lots are ONHOLD"; return) if nholds != nlots
      #
      # send stb_trigger request
      return guid if stbtrigger == false
      resp = @client.stb_trigger(guid) || return
      verify_response(resp, '100', carrier) || return
      wait_for(timeout: 90) {@mds.contexts(guid: guid).first.state == 'FINISHED'}    # kit status change might take a minute
      ekslots = auto_select_slots(kit, eqp)
      verify_kit(guid, 'FINISHED', 'ALL_OK', carrier: carrier, leadingorder: lorder, kit_slots: ekslots) || return
      return guid
    end

  end

end
