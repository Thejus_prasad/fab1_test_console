=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author:  ssteidte, 2010-11-24

Version: 1.4.2

History:
  2018-03-12 sfrieske, separated from erpmes
  2018-10-11 sfrieske, unified query
  2019-01-08 sfrieske, additionally return custrefitem, for CP TURNKEY_SUBCON_ALERT
  2019-05-27 sfrieske, select by CROSS_REF_TYPE instead of CROSS_REF_DESCRIPTION
=end

require 'dbsequel'


class ERPMDM
  MDMProduct = Struct.new(:product, :customer, :erpitem, :target, :tktype, :custrefitem, :xreftype) #, :xrefdesc)
  attr_accessor :db

  def initialize(env, params={})
    @db = DB.new("#{params[:use_hds] != false ? 'hds' : 'mds'}.#{env}", params)
  end

  # query for MDM entries by erpitem or product
  def mdm_entries(params={})
    ($log.warn 'mdm_entries: either erpitem or product must be passed'; return) unless params[:erpitem] || params[:product]
    qargs = []
    q = 'select UNIQUE r.CROSS_REF_VALUE, c.CUST_INITIALS, i.ITEM_NUMBER, i.ITEM_TARGET, i.ITEM_TURNKEY_TYPE, 
      ci.CUST_REF_ITEM, r.CROSS_REF_TYPE
      from MDM_ERP_ITEM i, MDM_ERP_ITEM_CROSS_REFERENCE r, MDM_ERP_CUSTOMER_ITEM ci, MDM_ERP_CUSTOMER c
      WHERE 1=1 and i.ITEM_NUMBER=r.ITEM_NUMBER and i.ITEM_NUMBER=ci.ITEM_NUMBER and ci.CUST_NUMBER=c.CUST_NUMBER'
    q, qargs = @db._q_restriction(q, qargs, 'c.CUST_INITIALS', params[:customer])
    q, qargs = @db._q_restriction(q, qargs, 'i.ITEM_NUMBER', params[:erpitem])
    q, qargs = @db._q_restriction(q, qargs, 'i.ITEM_TURNKEY_TYPE', params[:tktype])
    q, qargs = @db._q_restriction(q, qargs, 'r.CROSS_REF_VALUE', params[:product])
    q, qargs = @db._q_restriction(q, qargs, 'r.CROSS_REF_TYPE', params[:type] || 'MES Product')
    # q, qargs = @db._q_restriction(q, qargs, 'r.CROSS_REF_DESCRIPTION', params[:desc])
    q += ' ORDER BY r.CROSS_REF_VALUE'
    res = @db.select(q, *qargs) || return
    return res.collect {|r| MDMProduct.new(*r)}
  end

end
