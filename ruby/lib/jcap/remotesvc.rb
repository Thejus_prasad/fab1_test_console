=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author:  ssteidte, 2012-04-24

Version: 21.06

History:
  2012-12-12 ssteidte, added support for new methods in v12.11
  2014-10-20 ssteidte, use SiView::MM::LotOperation instead of $_lot_operation
  2016-05-18 sfrieske, msgid correlation per default
  2016-08-24 sfrieske, use lib rqrsp_mq instead of rqrsp
  2019-07-01 sfrieske, added new iCASE interface (v 19.06), get rid of XML Builder
  2020-04-01 sfrieske, added get_processed_lot_operation_history3
  2020-08-20 sfrieske, added get_lot_info4 and get_processed_lot_operation_history4
  2021-02-05 sfrieske, use instance_of instead of kind_of
  2021-07-05 sfrieske, added updateUserParameters, use rexml/document directly
  2021-08-31 sfrieske, requiring 'time' for #iso8601
=end

require 'time'
require 'rexml/document'
require 'siview'
require 'rqrsp_mq'


module JCAP

  # Testing of jCAP Remote Service for iCASE, WebMRB, ePRF, OJT and FabGUI/BtfTurnkey
  class RemoteService
    attr_accessor :sv, :mq, :_doc

    def initialize(env, params={})
      @sv = params.delete(:sv) || SiView::MM.new(env)
      @mq = RequestResponse::MQXML.new("jcapremote.#{env}", timeout: 120, correlation: :msgid)
    end

    # return a new boilerplate doc
    def new_doc(service)
      @_doc = doc = REXML::Document.new('<?xml version="1.0" encoding="UTF-8"?>')
      tag = doc.add_element('soapenv:Envelope', {
        'xmlns:soapenv'=>'http://www.w3.org/2003/05/soap-envelope',
        'xmlns:urn'=>'urn:JCapRemoteServiceOperations',
        'xmlns:urn1'=>'urn:JCapRemoteServiceTypes',
      }).add_element('soapenv:Body').add_element("urn:#{service}")
      taguser = tag.add_element('urn:user')
      taguser.add_element('urn1:password').text = @sv.password
      taguser.add_element('urn1:userName').text = @sv.user
      return tag
    end

    def get_version
      tag = new_doc('getVersion')
      res = @mq.send_receive(tag.document, timeout: 30) || return
      ret = res[:getVersionResponse][:return]
      $log.info "  #{ret}"
      return ret
    end

    def get_lot_info(lots, params={})
      version = params[:version] || ''
      $log.info "get_lot_info#{version} #{lots}"
      tag = new_doc("getLotInfo#{version}")
      if lots.instance_of?(String)
        tag.add_element('urn:lotIds').text = lots
      else
        lots.each {|lot| tag.add_element('urn:lotIds').text = lot}
      end
      res = @mq.send_receive(tag.document) || return
      ret = res[:"getLotInfo#{version}Response"][:return]
      ($log.warn ret.inspect; return) unless ret[:code] == 'SERVICE_OK'
      return ret[:lotResult][:"lotInfo#{version}"]
    end

    def get_lot_info2(lots, params={})
      return get_lot_info(lots, params.merge(version: 2))
    end

    def get_lot_info3(lots, params={})
      return get_lot_info(lots, params.merge(version: 3))
    end

    def get_lot_info4(lots, params={})
      return get_lot_info(lots, params.merge(version: 4))
    end

    def get_lot_operation_list(lot, params={})
      $log.info 'get_lot_operation_list'
      tag = new_doc('getLotOperationList')
      tag.add_element('urn:searchDirection').text = !params[:backward]
      tag.add_element('urn:posSearchFlag').text = !!params[:posflag]
      tag.add_element('urn:searchCount').text = 10000
      tag.add_element('urn:currentFlag').text = !(params[:current] == false)
      tag.add_element('urn:lotId').text = lot
      res = @mq.send_receive(tag.document) || return
      ret = res[:getLotOperationListResponse][:return]
      ($log.warn ret.inspect; return) unless ret[:code] == 'SERVICE_OK'
      return ret[:operationList]
    end

    def get_processed_lot_operation_history(lot, params={})
      version = params[:version] || ''
      $log.info "get_processed_lot_operation_history#{version} #{lot}"
      tag = new_doc("getProcessedLotOperationHistory#{version}")
      tag.add_element('urn:lotId').text = lot
      tag.add_element('urn:includeParents').text = !params[:noparents]
      res = @mq.send_receive(tag.document) || return
      ret = res[:"getProcessedLotOperationHistory#{version}Response"][:return]
      ($log.warn ret.inspect; return) unless ret[:code] == 'SERVICE_OK'
      # historyList might be empty, one element or an array
      hl = ret[:historyList] || []
      hl = [hl] unless hl.instance_of?(Array)
      return hl
    end

    def get_processed_lot_operation_history2(lot, params={})
      return get_processed_lot_operation_history(lot, params.merge(version: 2))
    end

    def get_processed_lot_operation_history3(lot, params={})
      return get_processed_lot_operation_history(lot, params.merge(version: 3))
    end

    def get_processed_lot_operation_history4(lot, params={})
      return get_processed_lot_operation_history(lot, params.merge(version: 4))
    end

    # currently UNUSED ??
    def get_user_parameters(lot)
      tag = new_doc('getUserParameters')
      tag.add_element('urn:paramClass').text = 'LOT'
      tag.add_element('urn:objectId').text = lot
      res = @mq.send_receive(tag.document) || return
      ret = res[:getUserParametersResponse][:return]
      ($log.warn ret.inspect; return) unless ret[:code] == 'SERVICE_OK'
      return ret[:userParam]
    end

    # uparams is an Array [{lot: lot, name: pname, type: 'STRING', strValue: 'QA'}]
    def update_user_parameters(uparams, params={})
      tag = new_doc('updateUserParametersList')
      tag.add_element('urn:paramClass').text = 'LOT'
      uparams.each {|e|
        tagparam = tag.add_element('urn:param')
        tagparam.add_element('urn1:name').text = e[:name]
        tagparam.add_element('urn1:objectId').text = e[:lot]
        tagparam.add_element('urn1:strValue').text = e[:strValue]
        tagparam.add_element('urn1:type').text = e[:type]
      }
      res = @mq.send_receive(tag.document, timeout: params[:timeout]) || return
$_res = res
      ret = res[:updateUserParametersListResponse][:return]
      ($log.warn res.inspect; return) unless ret[:code] == 'SERVICE_OK'
      return true
    end

    def lot_hold(lot, params={})
      tag = new_doc('lotHold')
      tag.add_element('urn:lotId').text = lot
      tag.add_element('urn:reasonCode').text = params[:reason] || 'ENG'
      tag.add_element('urn:claimMemo').text = params[:memo] || 'QA Test'
      res = @mq.send_receive(tag.document) || return
      ret = res[:lotHoldResponse][:return]
      ($log.warn ret.inspect; return) unless ret[:code] == 'SERVICE_OK'
      return true
    end

    def release_lot_hold(lot, holdreason, params={})
      tag = new_doc('releaseLotHold')
      tag.add_element('urn:lotId').text = lot
      tag.add_element('urn:holdReasonCode').text = holdreason
      tag.add_element('urn:releaseReasonCode').text = params[:reason] || 'ALL'
      tag.add_element('urn:claimMemo').text = params[:memo] || 'QA Test'
      res = @mq.send_receive(tag.document) || return
      ret = res[:releaseLotHoldResponse][:return]
      ($log.warn ret.inspect; return) unless ret[:code] == 'SERVICE_OK'
      return true
    end

    def future_hold(lot, opNo, params={})
      li = @sv.lot_info(lot)
      params[:post] = true if opNo.nil? && !params.has_key?(:post)
      params[:single] = params[:post] unless params.has_key?(:single)
      tag = new_doc('futureHold')
      tag.add_element('urn:lotId').text = lot
      tag.add_element('urn:routeId').text = params[:route] || li.route
      tag.add_element('urn:opNumber').text = opNo || li.opNo
      tag.add_element('urn:reasonCode').text = params[:reason] || 'ENG'
      tag.add_element('urn:postFlag').text = !!params[:post]
      tag.add_element('urn:singleTriggerFlag').text = !!params[:single]
      tag.add_element('urn:claimMemo').text = params[:memo] || 'QA Test'
      if params.has_key?(:exists_error)
        tag.add_element('urn:errorIfAlreadyExists').text = params[:exists_error]
      end
      res = @mq.send_receive(tag.document) || return
      ret = res[:futureHoldResponse][:return]
      ($log.warn ret.inspect; return) unless ret[:code] == 'SERVICE_OK'
      return true
    end

    def get_mes_user_report(since, params={})
      since = Time.parse(since) if since.instance_of?(String)
      tag = new_doc('getMesUserReport')
      tag.add_element('urn:lastLogOnSince').text = since.utc.iso8601(3)
      res = @mq.send_receive(tag.document) || return
      ret = res[:getMesUserReportResponse][:return]
      ($log.warn ret.inspect; return) unless ret[:code] == 'SERVICE_OK'
      return ret[:userInfo]
    end

    def set_mes_user_certification(user, disabled, params={})
      tag = new_doc('setMesUserCertification')
      tag.add_element('urn:userId').text = user
      tag.add_element('urn:disabled').text = disabled
      res = @mq.send_receive(tag.document) || return
      ret = res[:setMesUserCertificationResponse][:return]
      ($log.warn ret.inspect; return) unless ret[:code] == 'SERVICE_OK'
      return ret[:siViewReturnCode]['xsi:nil'] == '1'
    end

    # returns nil because there is no response
    def sync_backup_lot(lot, params={})
      tag = new_doc('syncBackupLotFamily')
      tagsync = tag.add_element('urn:syncRequest')
      tagsync.add_element('urn1:claimMemo').text = 'QA Test'
      tagsync.add_element('urn1:lotFamilyId')
      tagsync.add_element('urn1:lotId').text = lot
      if lc = params[:syncchild]
        tagchild = tagsync.add_element('urn1:splitLotIdent')
        tagchild.add_element('urn1:firstLotId').text = lot
        tagchild.add_element('urn1:secondLotId').text = lc
      end
      return @mq.send_receive(tag.document, timeout: 0)
    end

  end

end
