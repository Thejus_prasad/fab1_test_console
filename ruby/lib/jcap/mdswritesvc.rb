=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.


Author: Steffen Steidten, 2014-03-04

Version: 2.0.1

History:
  2016-08-24 sfrieske, use lib rqrsp_http instead of rqrsp
  2018-02-15 sfrieske, replaced select_all by select
  2019-02-22 sfrieske, separated MDSWriteService
  2021-08-30 sfrieske, moved xmlhash.rb to util
=end

ENV.delete('http_proxy')

require 'time'
require 'rqrsp_http'
require 'util/xmlhash'


# RTD calls JCAP to write eqp data into MDS V_PULL_A
class MdsWriteService
  attr_accessor :httpxml, :silent

  def initialize(env, params={})
    @httpxml = RequestResponse::HttpXML.new("mdswriteservice.#{env}")
    @silent = !!params[:silent]  # for perf tests
  end

  # data is either a Hash or an array of hashes, return true if the response is 'OK'
  def write_data(rqtype, data)
    # # wrong: receiver cannot parse XML with newlines and spaces (???)
    # s = XMLHash.hash_to_xml(data, 'input')  #.gsub("\n", '').gsub(/> *</, '><')
    h = {'soapenv:Envelope': {
      'xmlns:soapenv'=>'http://schemas.xmlsoap.org/soap/envelope/',
      'xmlns:com'=>'http://com.globalfoundries.jcap.rtdservices.mds.api',
      'soapenv:Body': {"com:#{rqtype}": {xmlDocument: XMLHash.hash_to_xml(data, 'input')}}
    }}
    res = @httpxml.post(XMLHash.hash_to_xml(h)) || return
    return res && res["#{rqtype}Response".to_sym][:result] == 'OK'
  end


  # writes T_TOOL: EQP_ID, WIP_LOT_AUTO3 (, LAST_TIME_DISP_RTD): write eqpWip 88, 77, checks
  def write_eqpwip(eqp, wip)
    $log.info {"write_eqpwip #{eqp.inspect}, #{wip}"} unless @silent
    return write_data('writeEqpWipAuto3', {row: {eqpId: eqp, eqpWip: wip}})
  end

  # Target-Table: T_TOOL, T_SUBTOOL
  # prerequisites: Eqp in 2WPR or 2SUP, WIP (lot as candidate), -> write 2INH, 2WMR, 2WCB (batch)
  # writeEqpStandbySubstates
  def write_eqpsubstates(eqp, substate, params={})
    $log.info {"write_eqpsubstates #{eqp.inspect}, #{substate.inspect}, #{params}"} unless @silent
    data = {
      # note: e10 and id must match SiView, they do not set the T_TOOL value
      # note: WIP_SIVIEW in T_TOOL limits wipXXX parameters
      # note: freqpTime must be formatted correctly but is not used
      eqpId: eqp, e10: params[:e10] || 'SBY', id: params[:state] || '2WPR', substate: substate,
      freqpTime: Time.now.utc.strftime('%m/%d/%y %H:%M:%S'),
      wipWfrAuto3: params[:wip_wfr_a3] || 0, wipWfrNonAuto3: params[:wip_wfr_noa3] || 0,
      wipLotAuto3: params[:wip_lot_a3] || 0, wipLotNonAuto3: params[:wip_lot_noa3] || 0,
    }
    if chamber = params[:chamber]
      data[:eqpSubId] = chamber
    end
    return write_data('writeEqpStandbySubstates', {row: data})
  end

  # writeEqpNextTimeRtd: set reftime and check NEXT_TIME_DISP_RTD = reftime+ waittime, send waittime -1 -> NEXT_TIME_DISP_RTD = null
  # Target-Table: T_TOOL
  # Target-Columns:
  # - EQP_ID -> reference
  # - NEXT_TIME_DISP_RTD -> RefTime+WaitTime
  # Hinweise:
  # - Negative WaitTime setzt den Wert T_TOOL.NEXT_TIME_DISP_RTD auf NULL
  def write_eqpnexttime(eqp, t, params={})
    $log.info {"write_eqpnexttime #{eqp.inspect}, #{t}, #{params}"} unless @silent
    reftime = params[:reftime] || Time.now.utc.iso8601(3).sub('Z', '+0000')
    return write_data('writeEqpNextTimeRtd', {refTime: reftime, row: {eqpId: eqp, waitTime: t}})
  end

  # Target-Table: T_TRANS_JOB alles echt, no xfer job, DISP_TARGET_LOC QAQA, check tgtloc and TRIGGER_ID RTD_QA
  # Target-Columns:
  # - CAST_ID -> reference
  # - STATE -> "REQUESTED"
  # - CURR_TYPE -> "STK/EQP" abhaengig ob FRCAST.EQP_ID in FREQP oder FRSTK steht
  # - CURR_LOC -> FRCAST.EQP_ID
  # - CURR_PORT -> FRCAST.MTRLLOC_ID
  # - TARGET_TYPE -> "STK/EQP"
  # - TRIGGER_ID -> "RTD_"+triggerId
  # Hinweise:
  #  frischer carrier oder STATE == REJECTED, STOCKED_IN
  def write_fouptransfer(carrier, tgtmachine, params={})
    disptgt = params[:disptgt] || ''
    tgttype = params[:tgttype] || 'STK'
    tgtport = params[:tgtport] || ''
    trigger = params[:trigger] || 'QATest'
    $log.info {"write_fouptransfer #{carrier.inspect}, #{tgtmachine.inspect}, #{params}"} unless @silent
    data = {
      castId: carrier, targetType: params[:tgttype] || 'STK', targetLoc: tgtmachine, targetPort: params[:tgtport] || '',
      triggerId: params[:trigger] || 'QATest', dispTargetLoc: params[:disptgt] || ''
    }
    return write_data('writeFoupTransferRequest', {row: data})
  end

  # writeLotTargetArea: lot exists, targetAreaId and claimMemo QA, check
  # Target-Table: T_LOT_TARGET_AREA
  # Hinweise:
  # - targetLocationId -> "Module1"/"Module2"
  # - targetAreaId musst du bei Dispatching anfragen (=> Wert wird wie uebergeben gespeichert)
  # - expireDuration und estProcStartDuration werden in Sekunden uebergeben
  # - lockFlag kann 0 oder 1 sein -> Boolean
  # - nach Ueberschreiten von "SYSDATE+expireDuration" wird der Datensatz automatisch geloescht
  def write_lottgtarea(lot, tgtarea, params={})
    $log.info {"write_lottgtarea #{lot.inspect}, #{tgtarea.inspect}, #{params}"} unless @silent
    data = {
      lotId: lot, targetLocationId: params[:tgtloc] || 'Module1', targetAreaId: tgtarea,
      targetEqpId: params[:tgteqp], sourceInstanceId: '', sourceEqpId: params[:srceqp], expireDuration: params[:expire] || 120,
      estProcStartDuration: 90, lockFlag: 0, dispRankNo: 100, claimMemo: params[:memo] || 'QATest'
    }
    return write_data('writeLotTargetArea', {row: data})
  end

end
