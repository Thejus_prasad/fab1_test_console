=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author:   Steffen Steidten, 2014-05-13

Version:  20.08

History:
  2015-08-25 ssteidte, minor cleanup
  2016-01-20 sfrieske, support for long running kit builds
  2016-04-08 sfrieske, select the most happy kit
  2016-04-12 sfrieske, removed ambiguity of result for spc msg and kit status
  2017-03-10 sfrieske, add support for EI VPDCompletionNotification
  2018-02-06 sfrieske, add support for WBTQ kit grouping
  2020-02-12 sfrieske, clean up lot_operation_list_fromhistory usage
  2020-09-14 sfrieske, force claim_process_lot to use the correct eqp
  2021-02-05 sfrieske, use instance_of instead of kind_of
  2021-08-27 sfrieske, require waitfor instead of misc
=end

require 'rqrsp_mq'
require 'sil/sildcr'
require 'util/uniquestring'
require 'util/waitfor'
require_relative 'aqv'
require_relative 'wbtqtest'


module AQV

  # AutoQualValidator testing
  class Test
    attr_accessor :sv, :wbtqtest, :nonprobank, :mds, :srccarrier, :kit, :spc, :sap, :ei,
      :sil_client, :sil_technology, :sil_department, :sil_parameters,
      :holdreason, :holdreason_noncritical, :evaldelay, :lot_failures, :kitresult, :_lines

    def initialize(env, params={})
      @srccarrier = params[:srccarrier] || 'TQ01'
      # all other parameters are meant for WBTQ::Test only (!)
      @wbtqtest = WBTQ::Test.new(env, sv: params[:sv])
      @sv = @wbtqtest.sv
      @nonprobank = params[:nonprobank] || 'UT-NONPROD'
      # a kit that can be built by STBOnly with enough wafers in the carrier
      @kit = params[:kit] || @wbtqtest.kits[:stbonly_other].first
      @holdreason = params[:holdreason] || 'C-ENG'
      @holdreason_noncritical = params[:holdreason_noncritical] || 'M-ENG'
      @sil_client = RequestResponse::MQXML.new("space.#{env}", timeout: params[:sil_timeout] || 900)
      @sil_technology = 'TEST'
      @sil_department = 'QAAQV'
      @sil_parameters = ['QA_AQV_PARAM_100', 'QA_AQV_PARAM_101']
      @spc = AQV::SPC.new(env)  # send simulated SIL messages to AQV
      @sap = AQV::SAP.new(env)
      @ei = AQV::EI.new(env)
      @mds = @wbtqtest.mds
      @mds_aqvlot_delay = 150                 # MDS builtin delay >~ 2min
      @mds_bankin_delay = 150                 # MDS builtin delay >~ 2min
      @evaldelay = params[:evaldelay] || 160  # from job properties + 10s
    end

    def inspect
      "#<#{self.class.name} #{@sv.env}>"
    end

    # trigger WBTQ kit creation (acting on behalf of SAP) and wait until it is seen in the AQV table
    #
    # return leadingorder on success or nil
    def create_aqv_kit(params={})
      unless params[:prepare_resources] == false
        @wbtqtest.prepare_resources(params[:srccarrier] || @srccarrier) || return
      end
      $log.info "waiting #{@wbtqtest.mds_sync} s for the MDS to sync"; sleep @wbtqtest.mds_sync
      # build kit
      # order = @mds.aqv_lot.collect {|e| e.order}.sort.last  # cannot leave this to WBTQ because there are different stale references
      # order &&= order.next
      # order = Time.now.to_i.to_s  # cannot leave this to WBTQ because there are different stale references
      lorder = unique_string
      kit = params[:kit] || @kit
      guid = @wbtqtest.stb_only(kit, '100', 'Target carrier:', leadingorder: lorder, timeout: params[:wbtq_timeout] || 60)
      ($log.warn "error building kit"; return) unless guid
      # wait for AQV table entry creation
      nlots = @wbtqtest.sfc.kit_info(kit).kit_slots.keys.size
      $log.info "waiting up to #{@mds_aqvlot_delay} s for the MDS to create the AQV table entries"
      wait_for(timeout: @mds_aqvlot_delay) {@mds.aqv_lot(leadingorder: lorder).size == nlots} || ($log.warn 'missing AQV lot entries'; return)
      return lorder
    end

    # send DCR for a lot at a specific operation and wait for the MDS entry, return true if result has been updated correctly
    ## TODO: verify (long time not executed)
    def send_dcr_wait(lot, routeop, dcrparams={})
      result = dcrparams[:result] != false
      $log.info "send_dcr_wait #{lot.inspect} (#{routeop.route}, #{routeop.op}, #{routeop.opNo}), result: #{result}"
      dcr = SIL::DCR.new(lot: lot, op: routeop.op, opNo: routeop.opNo, route: routeop.route,
        technology: @sil_technology, department: @sil_department)
      wafers = dcrparams[:wafers] || @sv.lot_info(lot, wafers: true).wafers.collect {|w| w.wafer}
      if result == false
        values = Hash[wafers.each_with_index.collect {|w, i| [w,  i + 149]}]
        dcr.add_parameter(@sil_parameters[0], values)
        values = Hash[wafers.each_with_index.collect {|w, i| [w,  i + 49]}] if dcrparams[:mixed]
        dcr.add_parameter(@sil_parameters[1], values)
      else
        values = Hash[wafers.each_with_index.collect {|w, i| [w,  i + 49]}]
        dcr.add_parameters(@sil_parameters, values)
      end
      # trigger Space/SIL to generate the aqv message
      tstart = Time.now
      @sil_client.send_receive(dcr.to_xml) || ($log.warn 'error sending DCR'; return)
      res = nil
      wait_for(timeout: 240) {
        res = @mds.aqv_lot_oper(lot: lot, opNo: routeop.opNo, tstart: tstart).first
      } || ($log.warn "no MDS update for lot #{lot}"; return)
      $log.info "  send_dcr_wait result: #{res.inspect}"
      res.spc_result == (result ? 'SUCCESS' : 'FAIL')
    end

    # send SPC message (on behalf of SIL) for a lot at a specific operation and wait for MDS entry, result: false for a fail message
    #
    # return true on success
    def spcevent_wait(lot, routeop, params={})
      @spc.send_msg(lot, routeop, params) || return
      result = params[:result] != false
      invalidres = params[:invalidres] != false
      # wait for the event to get processed by AQV, indicated by the SPC_MESSAGE field
      res = nil
      wait_for(sleeptime: params[:sleeptime] || 1, timeout: params[:timeout] || 60) {
        yield if block_given?
        res = @mds.aqv_lot_oper(lot: lot, opNo: routeop.opNo, spc_message: comment).first
      } || ($log.warn "no MDS update for lot #{lot}"; return)
      # verify SPC result
      res.spc_result == (result && !invalidres) ? 'SUCCESS' : 'FAIL'
    end

    # send many SPC events and mesure the time until the last one has been processed
    #
    # return process time or nil on error, currently UNUSED
    def spcevent_performance(lot, routeop, params={})
      result = (params.delete(:result) != false)
      count = params.delete(:count) || 100
      # send a msg with opposite result and verify it gets processed
      spcevent_wait(lot, routeop, result: !result) || return
      $log.info "sending #{count} messages with result #{result}"
      msg = @spc.send_msg(lot, routeop, comment: 'QA perf test', result: result, nosend: true)
      count.times {@spc.mq.send_msg(msg) || ($log.warn "MQ comm failure")}
      # wait until spc_update_time doesn't change anymore
      lotoper0 = @mds.aqv_lot_oper(lot: lot, opNo: routeop.opNo).first || return
      sleep(params[:sleeptime] || 5)
      wait_for(params) {
        @sap.mq.delete_msgs(nil)
        lotoper = @mds.aqv_lot_oper(lot: lot, opNo: routeop.opNo).first || return
        working = lotoper.spc_update_time > lotoper0.spc_update_time
        lotoper0 = lotoper
        working
      }
      duration = lotoper0.spc_update_time - lotoper0.spc_event_time
      # send a final msg with opposite result and verify it gets processed in time
      $log.info "sending a marker message and wait for it to get processed"
      spcevent_wait(lot, routeop, result: !result, timeout: params[:timeout]) || ($log.warn "error processing SPC events"; return)
      # clean up SAP queue
      @sap.mq.delete_msgs(nil)
      # return duration of test message processing
      duration
    end

    # returns nothing
    def trigger_spcevent(lot, routeop, params={})
      result = params[:spcresult] != false
      if params[:sil]
        @sv.lot_hold_release(lot, nil)
        @sv.lot_futurehold_cancel(lot, nil)
        send_dcr_wait(lot, routeop, result: result)
      elsif params[:wait_aqv_spc]  # for troubleshooting
        spcevent_wait(lot, routeop, result: result, invalidres: params[:invalidres], comment: params[:comment])
      else
        @spc.send_msg(lot, routeop, result: result, invalidres: params[:invalidres], comment: params[:comment])
      end
      @lot_failures[lot] << [routeop.opNo, 'SpcFail'] if !result && !params[:invalidres]
      @lot_failures[lot] << [routeop.opNo, 'SpcInvalid'] if params[:invalidres]
    end

    # process lot at current operation, release holds and optionally wait for update in AQV_LOT_OPER
    #
    # return true on success
    def process_lot(lot, params={})
      $log.info "process_lot #{lot.inspect}, #{params.inspect}"
      li = @sv.lot_info(lot)
      # SiView OpeComp
      pparams = {proctime: 0.1, running_hold: params[:running_hold]}
      if li.opEqps.member?(@wbtqtest.eqp)
        if eqpwrong = params[:eqpwrong]
          pparams[:eqp] = eqpwrong
          @lot_failures[lot] << [li.opNo, 'WrongEqp']
        else
          pparams[:eqp] = @wbtqtest.eqp
        end
      end
      @sv.claim_process_lot(lot, pparams) || return
      @lot_failures[lot] << [li.opNo, 'RunningHold'] if params[:running_hold]
      @sv.lot_hold_release(lot, nil).zero? || return  # from running hold or future hold
      return true unless params[:wait_aqv_lot]
      # for troubleshooting, it is normally sufficient to wait for the lot evaluation after completion
      $log.info "waiting for the MDS to update aqv_lot_oper"
      wait_for(timeout: 200) {
        res = @mds.aqv_lot_oper(lot: lot, opNo: li.opNo).first
        !res.nil? && !res.category.nil?
      } || ($log.warn "  no update in MDS AQV_LOT_OPER"; return)
    end

    # execute holds, future holds and bank holds
    def lot_actions(lot, opNo, params)
      holdreason = params[:holdreason] || @holdreason
      if params[:lothold]
        @sv.lot_hold(lot, holdreason)
        @sv.lot_hold_release(lot, nil)
        @lot_failures[lot] << [opNo, 'LotHold'] if holdreason != @holdreason_noncritical
      end
      if params[:futurehold]
        @sv.lot_futurehold(lot, opNo, holdreason, post: true)
        ## nono, the future hold must become effective  @sv.lot_futurehold_cancel(lot, nil)
        @lot_failures[lot] << [opNo, 'FutureHold'] if holdreason != @holdreason_noncritical
      end
      if params[:nonprobankin]
        @sv.lot_nonprobankin(lot, @nonprobank)
        @sv.lot_bankhold(lot)
        @sv.lot_bankhold_release(lot)
        @sv.lot_nonprobankout(lot)
      end
      if params[:lotsplit] && @sv.lot_info(lot).nwafers > 1
        lc = @sv.lot_split(lot, 1)
        @lot_failures[lot] << [opNo, 'LotSplit']
        @sv.lot_merge(lot, lc)
      end
    end

    # process lot until bankin and send the SPC events
    #
    # return the expected result (0 or 1) of the lot evaluation or nil on error
    def complete_lot_spc(lot, params={})
      $log.info "complete_lot_spc #{lot.inspect}, #{params}"
      rops = @sv.lot_operation_list(lot)
      skippableops = @mds.aqv_skippable(route: rops.first.route, skippable: 1).collect {|e| e.op}
      result = params[:result] != false  # expected result
      # process lot and send SPC events
      if params[:allspcfirst]
        # send SPC events for all ops
        rops[0..-2].each {|routeop| trigger_spcevent(lot, routeop, params)}
        # process lot at all ops
        rops[0..-2].each_with_index {|routeop, i|
          lot_actions(lot, routeop.opNo, params) if i == 0
          process_lot(lot, params) || return
        }
      else
        skip = false
        rops[0..-2].each_with_index {|routeop, i|
          if skip
            skip = false
            next
          end
          if params[:spcfirst]
            trigger_spcevent(lot, routeop, params)
            lot_actions(lot, routeop.opNo, params) if i == 0
            process_lot(lot, params) || return
          else
            if i == params[:gatepass]
              unless skippableops.include?(routeop.op)
                @lot_failures[lot] << [routeop.opNo, 'GatePass']
                result = false
              end
              @sv.lot_gatepass(lot)
            elsif i == params[:opelocate]
              unless skippableops.include?(routeop.op)
                @lot_failures[lot] << [routeop.opNo, 'OpeLocate']
                result = false
              end
              opelocatesteps = params[:opelocatesteps] || 1  # special TC with 2 skips
              skip = true if opelocatesteps > 1
              @sv.lot_opelocate(lot, rops[i + opelocatesteps].opNo)
            else
              _pp = {holdreason: params[:holdreason]}
              [:lothold, :futurehold, :nonprobankin, :lotsplit].each {|a| _pp[a] = params[a] == i}
              lot_actions(lot, routeop.opNo, _pp)
              process_lot(lot, params) || return
              if i == params[:nospcat]
                $log.info "no SPC for #{lot} at #{routeop.opNo}"
                unless skippableops.include?(routeop.op)
                  @lot_failures[lot] << [routeop.opNo, 'NoSpc']
                  result = false
                end
              elsif i == params[:invalidresat]
                trigger_spcevent(lot, routeop, {invalidres: true}.merge(params))
              elsif i == params[:spcfailat]
                trigger_spcevent(lot, routeop, {spcresult: false}.merge(params))
              else
                trigger_spcevent(lot, routeop, params)
              end
              if i == params[:opelocateback]
                @sv.lot_opelocate(lot, routeop.opNo).zero? || return # same operation again
                process_lot(lot, params) || return
                trigger_spcevent(lot, routeop, params) unless i == params[:nospcat]
              end
            end
          end
        }
        result = false if params[:invalidres] || params[:invalidresat] || params[:spcfailat] || (params[:spcresult] == false)
        result = false if params[:eqpwrong] || params[:running_hold]
        result = false if params[:lothold] && (params[:holdreason] != @holdreason_noncritical)
        result = false if params[:futurehold] && !params.has_key?(:result)
      end
      # bankin and optional bank hold
      sleep 1
      if params[:bankin] != false  # TODO: logic should eventually be reversed to speed up tests
        @sv.lot_bankin(lot) if @sv.lot_info(lot).states['Lot Inventory State'] != 'InBank'
        ($log.warn "  lot is not banked in"; return) if (@sv.lot_info(lot).states['Lot Inventory State'] != 'InBank')
        @sv.lot_bankhold(lot) if params[:bankhold]
        ($log.warn "  error setting bank hold"; return) if @sv.rc != 0
      end
      return (result ? 0 : 1)
    end

    # process all lots of a kit identified by the loading order, the SPC events and verify the AQV message sent to SAP
    #   all lots are treated equally, a new default kit is created if no leading order is passed
    #
    # return true on success
    def complete_kit_verify(aqvresult, params={})
      lorder = params.delete(:leadingorder) || create_aqv_kit(params) || return
      $log.info "complete_kit_verify #{aqvresult.inspect}, leadingorder: #{lorder.inspect}"
      aqvlots = @mds.aqv_lot(leadingorder: lorder) || return
      @lot_failures = Hash[aqvlots.collect {|aqvlot| [aqvlot.lot, []]}]
      @kitresult = nil
      # process the lots incl SPC messages lot by lot to avoid resource conflicts, TODO: logic should be reversed to speed up tests (?)
      if params[:process_lots_parallel]
        lots = aqvlots.collect {|aqvlot| aqvlot.lot}
        li = nil
        while (li = @sv.lot_info(lots.first)).states['Lot Inventory State'] != 'InBank'
          # process all lots in the carrier in 1 cj
          pparams = {proctime: 0.1}
          pparams[:eqp] = @wbtqtest.eqp if li.opEqps.member?(@wbtqtest.eqp)
          @sv.claim_process_lot(lots, pparams) || return
          # send SPC events
          lots.each {|lot| trigger_spcevent(lot, li)}
        end
        results = Array.new(lots.size, 0)  # all success
      else
        results = aqvlots.each_with_index.collect {|aqvlot, i|
          # invalidresidx is for special tests with invalid result string combinations
          pp = params.clone
          pp[:invalidres] = true if i == params[:invalidresidx]
          complete_lot_spc(aqvlot.lot, pp) || return
        }
      end
      # wait for MDS to propagate the lots' events
      $log.info "waiting up to #{@mds_bankin_delay} s for MDS bankin events"
      wait_for(timeout: @mds_bankin_delay) {
        aqvlots = @mds.aqv_lot(leadingorder: lorder)
        aqvlots.inject(true) {|ok, aqvlot| ok && aqvlot.bankin_time}  # bankin_time is not set for VPD scenario
      } || ($log.warn 'missing BankIn event'; return)
      # optionally send a backdated VPDCompletionNotification notification to speed things up by >~ 120 s
      aqvlots.each {|aqvlot| @ei.send_msg(aqvlot.lot, completiontime: Time.now - @evaldelay)} if params[:earlycompletion]
      #
      # wait for AQV kit evaluation
      evaldelay = params[:evaldelay] || @evaldelay  # longer delay for parallel_bankin
      $log.info "waiting up to #{evaldelay} s for AQV kit evaluation, results: #{results}"
      wait_for(timeout: evaldelay) {
        aqvlots = @mds.aqv_lot(leadingorder: lorder)
        aqvlots.inject(true) {|ok, aqvlot| ok && [0, 1].member?(aqvlot.status)}
      } || ($log.warn 'missing aqvlot status update'; return)
      res = true
      aqvlots.each_with_index {|aqvlot, i|
        ($log.warn "wrong aqvlot status: #{aqvlot}"; res=false) if aqvlot.status != results[i]
      }
      return unless res
      # verify final message
      @kitresult = @sap.receive_msg || ($log.warn 'no SAP message'; return)
      ($log.warn "wrong order number: #{kitresult.order} instead of #{aqvlots.first.order}"; return) if @kitresult.order != aqvlots.first.order
      ($log.warn "wrong result #{kitresult.result} instead of #{aqvresult}"; return false) if @kitresult.result != aqvresult
      return verify_summary
    end

    # verify failure summary in final message to SAP
    def verify_summary(params={})
      $log.info "verify_summary"
      aqvlots = @mds.aqv_lot(order: @kitresult.order)
      @_lines = (@kitresult.lines || []).clone
      ret = true
      @lot_failures.each {|lot, failures|
        hops = @sv.lot_operation_list_fromhistory(lot)
        noproc_found = false
        invalidspc_found = false
        failures.each {|opNo, reason|
          $log.info "- #{lot} #{opNo} #{reason}" if params[:verbose]
          op = hops.find {|hop| hop.operationNumber == opNo}.operationID.identifier
          if reason == 'LotHold'
            ret &= verify_kitresult_line(/#{lot}.*#{reason}/, params)
          elsif reason == 'FutureHold'
            ret &= verify_kitresult_line(/#{lot}.*#{reason}/, params)
          elsif reason == 'RunningHold'
            ret &= verify_kitresult_line(/#{lot}.*LotHold.*RNHL/, params)
            ret &= verify_kitresult_line(/#{lot}.*LotHold.*FCHL/, params)
          elsif reason == 'WrongEqp'
            noproc_found = verify_kitresult_line(/#{lot}.*not processed.*#{@wbtqtest.eqp}/, params) unless noproc_found
            ret &= noproc_found
          elsif reason == 'GatePass'
            ret &= verify_kitresult_line(/#{lot}.*#{op}.*Missing SPC/, params)
          elsif reason == 'OpeLocate'
            ret &= verify_kitresult_line(/#{lot}.*#{op}.*Missing SPC/, params)
          elsif reason == 'LotSplit'
            ret &= verify_kitresult_line(/#{lot}.*was split/, params)
          elsif reason == 'SpcFail'
            ret &= verify_kitresult_line(/#{lot}.*#{op}.*SPC.*fail/, params)
          elsif reason == 'SpcInvalid'
            invalidspc_found = verify_kitresult_line(/Invalid SPC.*#{lot}/, params) unless invalidspc_found
            ret &= invalidspc_found
          elsif reason == 'NoSpc'
            ret &= verify_kitresult_line(/#{lot}.*#{op}.*Missing SPC/, params)
          end
        }
        # reporting in case of invalid SPC messages is currently inconsistent, remove additional lines
        @_lines.delete_if {|line| line =~ /Lot '#{lot}' failed.*Missing SPC/} if invalidspc_found
      }
      # check for additional lines
      ($log.warn "#{@_lines.size} surplus lines:\n#{@_lines.pretty_inspect}"; ret = false) unless @_lines.empty?
      return ret
    end

    # internally used, returns true on success
    def verify_kitresult_line(regex, params={})
      idx = @_lines.find_index {|l| l =~ regex}
      if idx
        line = @_lines.delete_at(idx)
        $log.info line if params[:verbose]
        return true
      else
        $log.warn "  no entry for #{regex}"
        return false
      end
    end


    # wait for WBTQ to create kit(s) and send SAP message(s) to the real SAP system
    def wait_kits_sapmessage(kitresults, params={})
      kitresults = [kitresults] unless kitresults.instance_of?(Array)
      texts = params[:text]
      texts = [texts] unless texts.instance_of?(Array)
      tstart = params[:tstart] || Time.now
      ctxs = nil
      wait_for(timeout: 600) {
        ctxs = @wbtqtest.mds.contexts(kit: @kit, eqp: @wbtqtest.eqp, tstart: tstart, history: true)
        ctxs.size >= kitresults.size
      }
      ($log.warn "wrong kits: #{ctxs}"; return) unless ctxs.size == kitresults.size
      ctxs.each {|ctx|
        ($log.warn "wrong kit status: #{ctx}"; return) unless ctx.errorstate == 'ALL_OK' && ctx.state == 'FINISHED'
      }
      # send AQV msgs
      ctxs.each_with_index {|ctx, i|
        sleep 2 if i > 0  # allow SAP to process the previous msg to avoid long delays
        text = texts[i] || ["QA Test with #{kitresults[i]}"]
        @sap.send_msg(ctx.order, kitresults[i], text) || return
      }
      return true
    end

  end

end
