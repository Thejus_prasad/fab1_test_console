=begin
Automated Vendor Lot Start

(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author:     ssteidte, 2012-03-28

Version:    1.1.4

History:
  2012-08-23 ssteidte, remove to_char conversion from queries for Date and Time fields
  2015-02-16 ssteidte, removed VendorFile FTP
  2016-01-11 sfrieske, convert BigNum to int
  2016-08-30 sfrieske, use lib rqrsp_mq instead of rqrsp
  2018-02-15 sfrieske, replaced select_all by select
  2018-11-08 sfrieske, removed builder
  2020-01-15 sfrieske, store fdata for slotmap verification
  2021-02-05 sfrieske, use instance_of instead of kind_of
=end

require 'rqrsp_mq'
require_relative 'sjc'


# Automated Vendor Lot Start
module AVLS

  # AVLS MQ Message
  class VendorMessage
    attr_accessor :mq, :fdata

    def initialize(env, params={})
      @mq = RequestResponse::MQXML.new("avls.#{env}", {correlation: :msgid, timeout: 30}.merge(params))
    end

    def create_fosb(lots, carriers, params={})
      lots = [lots] if lots.instance_of?(String)
      carriers = [carriers] if carriers.instance_of?(String)
      $log.info {"create_fosb #{lots}, #{carriers}, #{params}"} unless params[:silent]
      nwafers = params[:nwafers] || 25
      trailing = params[:trailing] || 'QA'
      l = 12 - trailing.size - 2
      part = params[:part] || '31006550'  # mapped to FOSB carrier category (FOSB, FOSBM)
      supplier = params[:supplier] || 'SOITEC'
      supplier_site = params[:supplier_site] || 'Bernin'
      coa = params[:coa] || '2011-11-25'
      order = params[:order] || '999999'
      location = params[:location] || 'Fab 1'
      product = params[:product]
      #
      @fdata = lots.each_with_index.collect {|lot, i|
        lotdata = {'aut:carrierId': carriers[i], 'aut:deliveryNo': order, 'aut:location': location,
          'aut:partId': part, 'aut:vendorLotId': lot, 'aut:vendorName': supplier}
        lotdata[:'aut:productId'] = product if product
        unless params[:err_nowafers]
          _slot = params[:first_slot] || 1
          lotdata[:'aut:wafer'] = nwafers.times.collect {|i|
            wid = params[:err_waferid] || ("%#{l}.#{l}s%2.2d%s" % [lot, i + 1, trailing]).gsub(' ', 'X')
            slot = params[:err_noslot] ? '' : (params[:err_slot] || _slot)
            _slot += 1
            {'aut:waferId': wid, 'aut:waferPosition': slot}
          }
        end
        lotdata
      }
      h = {'soapenv:Envelope': {
        'xmlns:soapenv'=>'http://schemas.xmlsoap.org/soap/envelope/',
        'xmlns:aut'=>'http://jcap.globalfoundries.com/automatedvendorlotstart',
        'soapenv:Body': {'aut:createFosb': {'aut:fosb': @fdata}}
      }}
      res = @mq.send_receive(h) || return
      return res[:createFosbResponse][:correlation]
    end

  end


  # AVLS MDS
  class MDS < SJC::MDS
    AVLSCarrier = Struct.new(:carrierpk, :carrier, :created, :changed, :part, :order,
      :vendor, :vendor_lot, :location, :state, :retry, :lot, :srid, :xfer_time, :notfied)
    AVLSWafer = Struct.new(:waferpk, :carrierpk, :wafer, :slot)


    def avls_carriers(params={})
      qargs = []
      q = 'SELECT AVLS_CAST_PK, CAST_ID, CREATE_TIME, CHANGE_TIME, PART_ID, DELIVERY_NO, VENDOR_NAME,
        VENDOR_LOT_ID, LOCATION_NAME, PROCESSING_STATE, RETRY_COUNT, SIVIEW_LOT_ID, TRANSFER_REQUEST_ID,
        TRANSFER_REQUEST_TIME, LAST_NOTIFICATION_TIME from MDS_ADMIN.T_AVLS_CAST'
      q, qargs = @db._q_restriction(q, qargs, 'CAST_ID', params[:carrier])
      q, qargs = @db._q_restriction(q, qargs, 'DELIVERY_NO', params[:delivery])
      q, qargs = @db._q_restriction(q, qargs, 'VENDOR_LOT_ID', params[:vendor_lot]) # actually the vendor carrier id
      q += ' ORDER BY AVLS_CAST_PK'
      res = @db.select(q, *qargs) || return
      return res.collect {|r|
        r[0] = r[0].to_i
        r[10] = r[10].to_i
        r[12] = r[12].to_i
        w = AVLSCarrier.new(*r)
      }
    end

    def avls_carrier_sr(carrier, params={})
      rec = avls_carriers(carrier: carrier).first || return
      return sjc_requests({srid: rec.srid}.merge(params)).first
    end

    def avls_wafer(params={})
      qargs = []
      q = 'SELECT AVLS_WAFER_PK, AVLS_CAST_FK, WAFER_ID, WAFER_POS from MDS_ADMIN.T_AVLS_WAFER'
      q, qargs = @db._q_restriction(q, qargs, 'AVLS_CAST_FK', params[:carrierpk])
      q, qargs = @db._q_restriction(q, qargs, 'WAFER_ID', params[:wafer])
      q += ' ORDER BY AVLS_WAFER_PK'
      res = @db.select(q, *qargs) || return
      return res.collect {|r| w = AVLSWafer.new(*r)}
    end

  end

end
