=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author:  ssteidte, 2012-09-27

Version: 2.2.3

History:
  2016-08-04 sfrieske, split off of asmviewtest to remove dependency on AsmView::JCAP
  2016-11-08 sfrieske, in wait_move check lot script parameter ProcState directly
  2017-04-19 sfrieske, BSM support
  2018-03-13 sfrieske, removed use of lot_set_tkinfo, lot is already prepared before start_btf is called
  2019-03-29 sfrieske, added av_gid and get_avlot
  2019-05-06 sfrieske, separated Turnkey::BSMTest from Turnkey::BTFTest
  2019-10-18 aschiebe, added bin product check in verify_diecount
  2021-08-27 sfrieske, require waitfor instead of misc
=end

require 'asmview'
require 'dbsequel'
require 'util/waitfor'


module Turnkey

  # lot moves in Btf and verification in AsmView
  class BTFTest
    attr_accessor :sv, :av, :av_gid, :jobdelay, :stages, :mds

    def initialize(env, params={})
      @sv = params[:sv] || SiView::MM.new(env)
      @av = params[:av] || AsmView::MM.new(params[:av_env] || 'erpstage')
      @av_gid = params[:av_gid]  # nil, LOT_ID or LOT_LABEL
      @mds = ::DB.new("mds.#{$env}")  # for error analysis at start_btf
      @jobdelay = params[:jobdelay] || 480  # hard coded in MDS + BTFTurnkey job; 2020-05-06 - aschmid3: time changed from 410 to 480
      # mapping sv_stage => [av_op, opecomp], see property com.globalfoundries.jcap.btfturnkey.F1StageMappingRule
      @stages = params[:stages]
      unless stages
        @stages = {
          ### changed stage mapping to AsmView PDs per status 29.04.2020
          # old TkT J
          'old_J'=>{
            'TTB'=>['TKYTRANB.1', false],  # do not remove!
            'UBM'=>['TKYUBM.1', true],
            'SLD'=>['TKYSLDR.1', true],
            'RFL'=>['TKYRW1.1', true],
            'TTT'=>['TKYRW1.1', false],
            # 'BOO'=>['TKYOQAB.1', true], TODO: really no BOO for J ??
            # alternative to BOO in BTF: 'OQAB'=>['TKYOQAB.1', true],
            # 'TTS'=>['TKYTRANS.1', false], TODO: really no TTS for J ??
            'SOR'=>['TKYSORT.1', true],   # 1 opecomp in TKYSORT is sufficient, so the following ones are false
            ## not standard (N ?): 'SOR2'=>['TKYSORT.1', false],
            'TTR'=>['TKYSORT.1', false],
            'SPP'=>['TKYSORT.1', true],   # for binning data
            ### 'OQAS'=>['TKYSORT.1', false], # lot is kept here in AsmView by BTFTurnkey
            'DSS'=>['TKYSORT.1', false],  # transition to TKYOQAS.1 and LOTSHIPDESIG.1 happens on update_sort_result
          },
          # new TkT J - per Arthur Schiebelbein
          'J'=>{
            'TTB'=>['TKYTRANB.1', false],
            'UBM'=>['TKYUBM.1', true],
            'SLD'=>['TKYSLDR.1', true],
            'RFL'=>['TKYRW1.1', true],
            'TTS'=>['TKYTRANS.1', false],
            'SOR'=>['TKYSORT.1', true],
            'SOQ'=>['TKYSORT.1', false],
            'SPP'=>['TKYSORT.1', false],
            'DSS'=>['TKYOQAS.1', false],
          },
          # old TkT R
          'old_R'=>{  # not yet verified
            'TTB'=>['TKYTRANB.1', false],  # do not remove!
            'UBM'=>['TKYUBM.1', true],
            'SLD'=>['TKYSLDR.1', true],
            'RFL'=>['TKYRW1.1', true],
            'TTT'=>['TKYRW1.1', true], # changed from 'false' to 'true'
            'BOO'=>['TKYOQAB.1', false], # changed from 'true' to 'false'
            'TTS'=>['TKYTRANS.1', false],
            ## 2020-02-19: stages get skip per Pre1 script for using of jCAP BtfTurnkey v19.6
            # 'SOR'=>['TKYSORT.1', true],
            'SOR'=>['TKYOQAR.1', false],  # lot gets passed on to TKYOQAR.1
            # 'TTR'=>['TKYTRANR.1', false],
            # 'SRF'=>['TKYRW2.1', true],
            # 'ROQ'=>['TKYRW2.1', true],  # new according to file 'btfturnkey mapping analysis'
            ### 'BBM'=>['TKYRW2.1', true],  # not included in compare route from 'PROD' but new according to file 'btfturnkey mapping analysis'
            # 'RPP'=>['TKYRW2.1', true],    # for binning data
            # 'OQAR'=>['TKYOQAR.1', false],  # new according to file 'btfturnkey mapping analysis'
            'DSR'=>['TKYOQAR.1', false],  # transition to LOTSHIPDESIG.1 happens on update_sort_result
          },
          # new TkT R - per Arthur Schiebelbein
          'R'=>{
            'TTB'=>['TKYTRANB.1', false],
            'UBM'=>['TKYUBM.1', true],
            'SLD'=>['TKYSLDR.1', true],
            'RFL'=>['TKYRW1.1', true],
            'TTS'=>['TKYTRANS.1', false],
            'SOR'=>['TKYSORT.1', true],
            'TTR'=>['TKYTRANR.1', false],
            'SRF'=>['TKYRW2.1', true],
            'RPP'=>['TKYRW2.1', false],
            'OQAR'=>['TKYRW2.1', false],
            'DSR'=>['TKYOQAR.1', false],
          },
          # no change for TkT L
          'L'=>{
            'TTB'=>['TKYTRANB.1', false], # do not remove!
            'UBM'=>['TKYUBM.1', true],
            'SLD'=>['TKYSLDR.1', true],
            'RFL'=>['TKYRW1.1', true],
            'OQAB'=>['TKYOQAB.1', true],
            'DSB'=>['LOTSHIPD.1', false],
          },
          # old TkT S
          'old_S'=>{  # used for FabGUI_SH_Ship
            'TTS'=>['TKYTRANS.1', false],
            'SOR'=>['TKYSORT.1', true],
            'TTR'=>['TKYSORT.1', false],
            'SPP'=>['TKYSORT.1', true],   # for binning data
            'DSS'=>['TKYSORT.1', false]
          },
          # new TkT S - per Arthur Schiebelbein + adjustments
          'S'=>{  # used for FabGUI_SH_Ship
            'TTS'=>['TKYTRANS.1', false],
            'SOR'=>['TKYSORT.1', true],
            'SOQ'=>['TKYSORT.1', false],  # SOQ copied from TkT J; TTR is a reflow stage: 'TTR'=>['TKYSORT.1', false],
            'SPP'=>['TKYSORT.1', false],  # doesn't work: 'SPP'=>['TKYOQAS.1', true],
            'DSS'=>['TKYOQAS.1', false]
          },
          # 'N'=>{
          #   'TTB'=>['TKYTRANB.1', false],
          #   'UBM'=>['TKYUBM.1', true],
          #   'SLD'=>['TKYSLDR.1', true],
          #   'RFL'=>['TKYRW1.1', true],
          #   'TTT'=>['TKYRW1.1', false],
          #   'BOO'=>['TKYOQAB.1', true],
          #   'TTS'=>['TKYTRANS.1', false],
          #   'SOR'=>['TKYSORT.1', true],
          #   'SOR2'=>['TKYSORT.1', true],
          #   'TTR'=>['TKYSORT.1', false],
          #   ### 'OQAS'=>['TKYOQAS.1', true],  # lot is kept here in AsmView
          #   'DSS'=>['TKYOQAS.1', false],
          # },
        }
        @stages['N'] = @stages['J']  # for shipping preparations
      end
      # # for fast lot preparation, TODO: move to a separate file (?)
      # @routes = {
      #   'J'=>['BUMP-B.2', 'SORT-A.2'],  # 'TKYSORT.1'
      # }
    end

    # fast lot preparation for shipping tests, tktype controls the flow (must NOT match the true TurnkeyType)
    # return true on success
    def prepare_shipment(lot, tktype, params={})
      avlot = get_avlot(lot, av_gid: params[:av_gid]) || return
      #
      # av: put lot on the correct final (branch) route
      if tktype == 'J'                                    # adjusted + verified 15.05.2020
        (@av.lot_branch_cancel(avlot) == 0) || return
        sleep 5
        (@av.lot_opelocate(avlot, nil, op: 'SORTDISP.1') == 0) || return
        sleep 5
        (@av.lot_branch(avlot, 'SORT-A.2') == 0) || return
        sleep 5
        (@av.lot_opelocate(avlot, nil, op: 'TKYSORT.1') == 0) || return   # 2020-05-14: w/ TKYOQAS.1 lot is at LOTSHIPD.1 (route="TURNKEY.2")
        sleep 5
        # additional with FabGUI MoveEvent
        # lot put to ProcState=Processing for automatical lot moving per BtfTurnkey to TKYOQAS.1
        (@av.user_parameter_change('Lot', avlot, 'ProcState', 'Processing') == 0) || return
      elsif tktype == 'R'                                 # adjusted + verified 18.05.2020
        (@av.lot_branch_cancel(avlot) == 0) || return
        (@av.lot_opelocate(avlot, nil, op: 'BUMP2DISP.1') == 0) || return
        (@av.lot_branch(avlot, 'BUMP2-A.2') == 0) || return
        (@av.lot_opelocate(avlot, nil, op: 'TKYOQAR.1') == 0) || return
      elsif tktype == 'S'                                 # adjusted 15.05.2020 + verified 18.05.2020
        (@av.lot_opelocate(avlot, nil, op: 'TKYSORT.1') == 0) || return # adj
        # additional with FabGUI MoveEvent
        # lot put to ProcState=Processing for automatical lot moving per BtfTurnkey to TKYOQAS.1
        (@av.user_parameter_change('Lot', avlot, 'ProcState', 'Processing') == 0) || return
      elsif tktype == 'L'                                 # no change necessary 15.05.2020 + verified 18.05.2020
        (@av.lot_branch_cancel(avlot) == 0) || return
        (@av.lot_opelocate(avlot, nil, op: 'LOTSHIPD.1') == 0) || return
      else
        $log.warn "wrong tktype: #{tktype.inspect}"
        return false
      end
      #
      # sv: move lot to LOTSHIPDESIG.01
      (@sv.lot_opelocate(lot, nil, op: 'LOTSHIPDESIG.01') == 0) || return
      #
      sleep 240
      $log.info "processed lot #{lot}, current op (sv) #{@sv.lot_info(lot).op}, (av) #{@av.lot_info(avlot).op}"
      return true
    end

    # for error analysis at start_btf
    def lotevents(lot)
      return @mds.select('SELECT * FROM V_ASMV_LOT_HIST WHERE LOT_ID = ?', lot)
    end

    # return the lot ID used in AsmView, params[:av_gid] is used in special tests only
    def get_avlot(lot, params={})
      # TODO: auto ?
      return lot if (params[:av_gid] || @av_gid) != 'LOT_LABEL'
      ret = @sv.lot_label(lot)
      ($log.info "lot #{lot} has no LOT_LABEL"; return) if ret.nil? || ret.empty?
      return ret
    end

    # move lot to the first BTF stage (e.g. TTB), wait for STB and arrival at the first op in AsmView
    #
    # return true on success
    def start_btf(lot, params={})
      $log.info "start_btf #{lot.inspect}"
      # gatepass lot, else BtfTurnkey cannot handle it
      @sv.lot_gatepass(lot, force: :ondemand, mandatory: true).zero? || ($log.warn "error gatepassing lot #{lot}"; return)
      @sv.lot_hold_release(lot, nil)
      tstart = Time.now
      # locate lot to the first BTF stage
      tktype = @sv.user_parameter('Lot', lot, name: 'TurnkeyType').value
      stages = params[:stages] || @stages[tktype]
      @sv.lot_opelocate(lot, nil, stage: stages.keys.first).zero? || return
      $log.info "waiting 120 s for the lot to appear in the V_ASMV_LOT_HIST table"  # MDS hard coded delay
      sleep 120
      wait_for(timeout: 60) {lotevents(lot).first} || ($log.warn 'lot is not in V_ASMV_LOT_HIST'; return)
      avlot = get_avlot(lot, av_gid: params[:av_gid]) || return
      $log.info "waiting up to #{@jobdelay} s for lot #{avlot} to appear in AsmView"
      wait_for(timeout: @jobdelay, tstart: tstart) {
        @av.lot_exists?(avlot)
      } || ($log.warn 'lot is not in AsmView'; return)
      wait_for(timeout: 120) {
        @av.lot_info(avlot).op == stages.values.first[0]
      } || ($log.warn 'lot is not at the first stage in AsmView'; return)
      return true
    end

    # move lot(s) of the same TKType in SiView and wait for them to reach the mapped op in AsmView
    #
    # return true on success
    def move_wait(lots, svstage, params={})
      lots = [lots] if lots.kind_of?(String)
      $log.info "move_wait #{lots}, #{svstage.inspect}"
      tktype = @sv.user_parameter('Lot', lots.first, name: 'TurnkeyType').value
      stages = params[:stages] || @stages[tktype]
      avop, procflag = stages[svstage]
      # TODO: this should be skipped in case of a double move (stage with 1 PD only that is mandatory)
      # locate lots to the first op at the new stage in SiView
      ok = lots.inject(true) {|res, lot| res && (@sv.lot_opelocate(lot, nil, stage: svstage) == 0)}
      unless ok
        if procflag
          $log.warn "error locating to the mandatory stage #{svstage}"
          return
        else
          $log.info "ignoring not existing, not mandatory stage #{svstage}"
        end
      end
      # process lots if OpeStart is required or enforced
      # UNUSED: if params[:process_all] && !['DSB', 'DSS'].member?(svstage) || procflag
      if procflag
          lots.each {|lot| @sv.claim_process_lot(lot, running_hold: params[:running_hold]) || return}
      end
      # wait for lot move in AsmView
      avlots = lots.collect {|lot| get_avlot(lot, av_gid: params[:av_gid])} || return
      ($log.warn "error getting AsmView lot IDs for #{lots}: #{avlots}"; return) if avlots.include?(nil)
      $log.info "waiting up to #{@jobdelay} s for lots #{avlots} to move to #{avop} in AsmView"
      wait_for(timeout: @jobdelay) {
        avlots.inject(true) {|ok, lot| ok && @av.lot_exists?(lot) && @av.lot_info(lot).op == avop}
      } || ($log.warn "lot is not at op #{avop}"; return)
      # wait for lot status in AsmView if 'Processing'
      if procflag
        $log.info "waiting up to #{@jobdelay} s for lots #{avlots} to become Processing in AsmView"
        wait_for(timeout: @jobdelay) {
          avlots.inject(true) {|ok, lot|
            ok && @av.user_parameter('Lot', lot, name: 'ProcState').value == 'Processing'
          }
        } || ($log.warn 'lot is not Processing'; return)
      end
      return true
    end

    # process a lot in BTF and verify the AsmView moves, return true on success
    def process_btf(lot, params={})
      $log.info "process_btf #{lot.inspect}, #{params}"
      # get list of stages depending on TKTYPE
      stages = params[:stages] || @stages[@sv.user_parameter('Lot', lot, name: 'TurnkeyType').value]
      # optionally select start stage (normally next stage)
      if s0 = params[:start_at]
        stages = stages.clone
        found = false
        stages.keep_if {|stage, v|
          found = true if stage == s0
          found
        }
      end
      avlot = get_avlot(lot, av_gid: params[:av_gid])
      # move and verify av location
      stages.keys.each_with_index {|svstage, idx|
        avop, procflag = stages[svstage]
        break if svstage == params[:stop_before]
        ## UNUSED: next if params[:mandatory_only] && !procflag  # attn, the final move to LOTSHIPDESIG is also skipped
        $log.info "-- btfturnkey move to stage #{svstage}"
        res = move_wait(lot, svstage, stages: stages, av_gid: params[:av_gid])  # UNUSED: , process_all: params[:process_all])
        next if res
        # if the stage is only 1 op long and processing mandatory the lot is at the next stage
        if @sv.lot_info(lot).stage == stages.keys[idx + 1]
          $log.info '--> lot has been processed at the only PD in this stage and is now at the next stage'
        # else   # 2020-05-06 - aschmid3: changed after issues at test for FinalStdShipment TkT=L (FabGUI_SH_Ship24#test233)
        elsif @av.lot_info(avlot).op[0..-3] != avop.split('.').first  # replace row after named issue above (row from Arthur Schiebelbein)
          $log.warn "(av) lot #{avlot.inspect} is not at the expected PD #{avop}"
          return false
        end
      }
      #
      $log.info "processed lot #{lot}, current op (sv) #{@sv.lot_info(lot).op}, (av) #{@av.lot_info(avlot).op}"
      return true
    end

    # verify lot properties in AsmView are correct, return true on success
    def verify_asmview_lot(lot)
      avlot = get_avlot(lot)
      $log.info "verify_asmview_lot #{lot.inspect}"
      ($log.warn "  lot not created in #{@av.inspect}"; return) unless @av.lot_exists?(avlot)
      svwafers = @sv.lot_info(lot, wafers: true).wafers
      avwafers = @av.lot_info(avlot, wafers: true).wafers
      ($log.warn "  wafer count mismatch, #{avwafers.size} instead of #{svwafers.size}"; return) if avwafers.size != svwafers.size
      ret = true
      svwafers.each_with_index {|svw, i|
        walias = "#{lot.split('.').first}.#{svw.alias}"
        avw = avwafers.find {|w| w.wafer == svw.wafer}
        ($log.warn "    no av wafer matching #{svw}"; ret=false; next) unless avw
        ($log.warn "    wrong wafer alias, #{avw.alias.inspect} instead of #{walias.inspect}"; ret=false) if avw.alias != walias
      }
      $log.info '  turnkey type and subcons'
      svparams = @sv.user_parameter('Lot', lot)
      avparams = @av.user_parameter('Lot', avlot)
      %w(TurnkeyType TurnkeySubconIDBump TurnkeySubconIDSort
        TurnkeySubconIDReflow TurnkeySubconIDAssembly TurnkeySubconIDFinalTest).each {|name|
        svp = svparams.find {|e| e.name == name}
        avp = avparams.find {|e| e.name == name}
        ($log.warn "(av) wrong script parameter: #{avp} instead of #{svp}") if (avp && avp.value) != (svp && svp.value)
      }
      return ret
    end

    # compare asm die count with sort db data, return true on success
    def verify_diecount(avlot, sortdata)
      $log.info "verify_diecount for avlot #{avlot}"
      erpitem = @av.user_parameter('Lot', avlot, name: 'ERPItem').value.tr('*', '')
      erpitempart1 = erpitem.split('-').first
      erpitempart2 = erpitem.split('-')[1..-1].join('-')
      adc = @av.asm_diecount(avlot) || return
      ok = true
      sortdata.each_pair {|wafer, waferbincounts|
        # remove bins not flagged goodbin and empty bins
        wbcs = waferbincounts.select {|e| e[:goodbin] == 1 && e[:dieqty] > 0}
        wcount = wbcs.size
        asmcs = adc[wafer]
        acount = asmcs.size
        if acount != wcount
          $log.warn "  wrong number of bins for wafer #{wafer.inspect}: #{acount} instead of #{wcount}"
          ok = false
          next
        end
        wbcs.each_with_index {|wbc, i|
          asmc = asmcs[i]
          product = "#{erpitempart1}-#{asmc[:binnumber]}-#{erpitempart2}"
          ($log.warn "  wrong bin #{wbc}, #{asmc}"; ok=false; next) if wbc[:binnumber] != asmc[:binnumber] || wbc[:dieqty] != asmc[:dieqty]
          # TODO: Wait for BtfTurnkey 19.08 or later
          # ($log.warn "  wrong bin product #{wbc}, #{asmc}"; ok=false; next) if asmc[:product] != product
        }
      }
      return ok
    end

  end

end
