=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.


Author: Steffen Frieske, 2017-10-24

Version: 21.04

History:
  2019-01-30 sfrieske, minor cleanup
  2021-07-30 sfrieske, use rexml/document directly, moved mq_status_orig to pilotmgrtest.rb
  2021-08-30 sfrieske, mved xmlhash.rb to util
  2022-01-12 sfrieske, use RequestResponse::MQ instead of RequestResponse::MQXML
=end

require 'rexml/document'
require 'rqrsp_mq'
require 'util/xmlhash'


module APC

  # APC / MQSRAdapter communication, acting as APC/Catalyst
  class MQSRClient
    attr_accessor :sv, :mq, :mq_status, :_doc

    def initialize(env, params={})
      @sv = params[:sv] || SiView::MM.new(env)
      @mq = RequestResponse::MQ.new("sra.#{env}")
      @mq_status = MQ::JMS.new("srastatus.request.#{env}")
    end

    # pass an array of arrays of WaferInfo structs (wafer slots must be the _target_ positions!, return jCAP job response
    #   each destination is a carrier, multiple child lots can be moved into 1 carrier (!)
    def sjseparate(winfos, params={})
      srccarrier = params[:srccarrier]
      ccat = params[:srccarrier_cat]
      unless srccarrier && ccat
        li = @sv.lot_info(winfos.first.first.lot)
        srccarrier ||= li.carrier
        ccat ||= @sv.carrier_status(srccarrier).category
      end
      $log.info {"sjseparate  # srccarrier: #{srccarrier.inspect}"} unless params[:silent]
      @_doc = doc = REXML::Document.new('<?xml version="1.0" encoding="UTF-8"?>')
      tag = doc
      .add_element('SortRequestContainer')
      .add_element('SeparateRequestList')
      .add_element('SeparateRequest')
      tag.add_element('SourceCarrierId').text = srccarrier
      tag.add_element('SourceCarrierCategory').text = ccat
      tag.add_element('RequestId').text = params[:rqid] || 'QA123'
      tag.add_element('Scenario').text = params[:scenario] || 'GenericPilot'
      destinationsstag = tag.add_element('DestinationList')
      winfos.each {|wafers|
        waferstag = destinationsstag.add_element('Destination').add_element('WaferList')
        wafers.each {|w|
          wtag = waferstag.add_element('Wafer')
          wtag.add_element('LotId').text = w.lot
          wtag.add_element('WaferId').text = w.wafer
          wtag.add_element('DestinationPosition').text = w.slot
        }
      }
      s = String.new
      tag.document.write(s)
      res = @mq.send_receive(s) || return
      return XMLHash.xml_to_hash(res)[:SortRequestResponse]
    end

    # MQSRAdapter forwards SR status messages to APC on the generic CAT.REQUEST01 queue, we're listening like Catalyst
    def receive_statusmsg(params={})
      $log.info "receive_statusmsg"
      msg = params[:msg] || @mq_status.receive_msg(timeout: params[:timeout] || 120) || return
      res = XMLHash.xml_to_hash(msg, '*//*Body') || return
      res = res[:submitJob][:jobParametersXML] || return
      # the actual message is wrapped into SOAP, HTML entity encoding is handled automatically
      res2 = XMLHash.xml_to_hash(res) || return
      return Hash[res2[:controlData][:data][:dataEntry].collect {|e| e.values}]
    end

  end

end
