=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: Steffen Steidten, 2012-08-22

Version: 1.3.2

History:
  2013-01-03 dsteger,  added Tool parameters and switched to MDS namespace
  2013-11-14 ssteidte, use new unified rqrsp/mqjms
  2015-05-21 ssteidte, ensure msg are sent as bytes_msgs
  2015-06-29 ssteidte, ToolEvents::MDS does not inherit from obsolete AutoDB
  2016-08-24 sfrieske, use mqjms direcly instead of rqrsp
  2017-02-24 sfrieske, cleaned up MDS queries
  2018-02-15 sfrieske, replaced select_all by select
  2020-05-06 sfrieske, simplified WaferHistory DCR creation, remove xmls:ns in context (v 20.05)
  2021-02-05 sfrieske, use instance_of instead of kind_of
  2021-08-30 sfrieske, moved xmlhash.rb to util

Notes:
  Can be used in conjunction with claim_process_lot:
  wh = ToolEvents::WaferHistory.new("itdc")
  li = @@sv.lot_info(lot, wafers: true)
  $itdc.claim_process_lot(lot) {|cj, cjinfo, eqpiraw, pstatus, mode, lis|
    wh.wafer_history_report(cj, li, eqpiraw.equipmentID.identifier, port)
  }
=end

require 'time'
require 'mqjms'
require 'util/xmlhash'


# Wafer History, EI Parameters and Tool Alarm Testing Support
module ToolEvents

  # Send Tool Events to the jCAP ToolEventCollector Queue
  class Creator
    attr_accessor :mq, :ts, :eqp, :context

    def initialize(env, params={})
      # MQ, no response
      @mq = params[:mq] || MQ::JMS.new("abd.#{env}")
      @ts = params[:timestamp] || Time.now
      @eqp = params[:eqp] || 'QATEST001'
      # common DCR context
      dcp_armor_path = params[:dcp_armor_path] || '/Tool DCP/DCP Repository/Baseline'
      dcp_plan_name = params[:dcp_plan_name] || 'ToolViewWaferEvents'
      dcp_plan_version = params[:dcp_plan_version] || '2'
      @context = {
        'schemaVersion'=>'DCR_2_2_2',
        'baselineVersion'=>'f36cei8.2. - patch 11',
        'dcpArmorPath'=>"#{dcp_armor_path}/#{dcp_plan_name}/#{dcp_plan_version}",
        'dcpPlanName'=>dcp_plan_name, 'dcpPlanVersion'=>dcp_plan_version,
        'description'=>'QA Test', 'equipmentModelName'=>'*', 'equipmentModelVersion'=>'*',
        'eventReportingName'=>'', 'eventType'=>'', 'reportingTimeStamp'=>@ts.utc.iso8601(3),
        # 'xmlns:ns'=>'http://www.amd.com/schemas/DataCollectionReport/DCR_2_2_2',
        'ns:Equipment': {
          'id'=>@eqp, 'controlState'=>'ONLINE-REMOTE', 'description'=>'QA',
          'equipmentReportedE10State'=>'1PRD', 'modelNumber'=>'QA',
          'name'=>'QA', 'softwareRevision'=>'0.0.0', 'type'=>'PROCESS-CHAMBER'
        }
      }
    end

    def inspect
      "#<#{self.class.name} #{@mq}>"
    end

  end


  # Send EI WaferHistory DCRs
  class WaferHistory < Creator

    def initialize(env, params={})
      super
    end

    def create_dcr(cj, lot, carrier, wafer, slot, eqp, port, params={})
      ctx = @context.merge({
        'eventReportingName'=>'WaferCompleted', 'eventType'=>'eventOnly',
        :'ns:JobSetup'=>{
          :'ns:SiViewControlJob'=>{'id'=>'', 'siviewUserID'=>'',
            :'ns:FOUP'=>{
              'carrierAccessingState'=>'NOT ACCESSED', 'id'=>'', 'idStatusState'=>'ID VERIFICATION OK',
              'location'=>'P1', 'locationType'=>'PORT', 'scheduledLoadPort'=>'P1',
              'scheduledUnloadPort'=>'P1', 'slotMapState'=>'SLOT MAP VERIFICATION OK',
              :'ns:FOUPHistory'=>{
                'enterTime'=>'', 'location'=>'P1', 'locationType'=>'PORT', 'trackingLocation'=>'1'},
              :'ns:Lot'=>{'armorPath'=>'', 'armorRecipeID'=>'', 'armorRecipeVersion'=>'1', 'id'=>'',
                'logicalRecipeID'=>'', 'lotType'=>'Production', 'machineRecipeID'=>'',
                'manufacturingLayer'=>'B', 'monitorLotFlag'=>'false', 'operationID'=>'',
                'operationName'=>'', 'operationNumber'=>'', 'passCount'=>'1', 'photoLayer'=>'CA',
                'physicalRecipeID'=>'', 'productGroupID'=>'', 'productID'=>'', 'productType'=>'Wafer',
                'route'=>'', 'subLotType'=>'', 'technology'=>'', :'ns:Wafer'=>nil
              }
            }
          }
        }
      })
      ts = params[:timestamp] || Time.now
      ctx['reportingTimeStamp'] = ts.utc.iso8601(3)
      ctx[:'ns:Equipment']['id'] = eqp
      ctxcj = ctx[:'ns:JobSetup'][:'ns:SiViewControlJob']
      ctxcj['id'] = cj
      ctxcj['siviewUserID'] = "X-#{eqp}"
      ctxcj[:'ns:FOUP']['id'] = carrier
      ctxcj[:'ns:FOUP']['location'] = port
      ctxcj[:'ns:FOUP']['scheduledLoadPort'] = port
      ctxcj[:'ns:FOUP']['scheduledUnloadPort'] = port
      ctxcj[:'ns:FOUP'][:'ns:FOUPHistory']['port'] = port
      ctxcj[:'ns:FOUP'][:'ns:Lot']['id'] = lot
      slotin = slot  #|| '9'
      slotout = slot
      chambers = params[:chambers] || ['PM1']
      t = ts - chambers.size * 2 - 3
      ctxcj[:'ns:FOUP'][:'ns:FOUPHistory']['enterTime'] = t.utc.iso8601(3)
      wh = []
      t += 1
      wh << {'exitTime'=>t.utc.iso8601(3), 'location'=>slotin, 'locationType'=>'FOUP',
        'state'=>'NEEDS PROCESSING'}
      chambers.each {|ch|
        t += 1
        wh << {'enterTime'=>t.utc.iso8601(3), 'location'=>ch, 'locationType'=>'PROCESSINGAREA',
          'state'=>'NEEDS PROCESSING', 'trackingLocation'=>'400'}
        t += 1
        wh << {'exitTime'=>t.utc.iso8601(3), 'location'=>ch, 'locationType'=>'PROCESSINGAREA',
          'state'=>'IN PROCESS', 'trackingLocation'=>'400'}
      }
      t += 1
      wh << {'enterTime'=>t.utc.iso8601(3), 'location'=>slotout, 'locationType'=>'FOUP',
        'state'=>'IN PROCESS'}
      ctxcj[:'ns:FOUP'][:'ns:Lot'][:'ns:Wafer'] = {'id'=>wafer, :'ns:WaferHistory'=>wh}
      h = {'soap:Envelope'=>{
        'xmlns:soap'=>'http://schemas.xmlsoap.org/soap/envelope/',
        'xmlns:ns'=>'http://www.amd.com/schemas/DataCollectionReport/DCR_2_2_2',
        'xmlns:MDSSubmitRunPort'=>'http://com.amd.mds.adapter.EIRTUpdater',
        'soap:Body': {
          'MDSSubmitRunPort:onMessage': {
            # 'soap:encodingStyle'=>'http://schemas.xmlsoap.org/soap/encoding/',
            event: {'ns:Context': ctx}
          }
        }
      }}
      return XMLHash.hash_to_xml(h)
    end

    # send DCRs for all or selected wafers of a lot,
    #   e.g. from sv.lot_info(lot, wafers: true, select: [2, 3])
    def wafer_history_report(cj, li, eqp, port, params={})
      $log.info "wafer_history_report #{params}"
      li.wafers.each {|w|
        s = create_dcr(cj, li.lot, li.carrier, w.wafer, w.slot, eqp, port)
        export = params[:export]
        export = "log/wafer_history_#{lot}_#{w.wafer}.xml" if export == 'waferid'
        File.binwrite(export, s) if export
        @mq.send_msg(s, export: export, bytes_msg: true)
      }
    end
  end


  # Send EI Parameter
  class Parameter < Creator
    attr_accessor :parameters

    def initialize(env, params={})
      super
      @context.merge!({'eventReportingName'=>'DCEventToolStateChanged', 'eventType'=>'variable'})
      @context[:'ns:Equipment'][:'ns:ProcessingArea'] = [
        {'id'=>'PM2', 'locationType'=>'PROCESSINGAREA', 'processingAreaE10State'=>'2WPR',
          'processState'=>'IDLE', 'trackingLocation'=>5, 'type'=>'ProcessingArea'},
        {'id'=>'PM3', 'locationType'=>'PROCESSINGAREA', 'processingAreaE10State'=>'2WPR',
          'processState'=>'IDLE', 'trackingLocation'=>6, 'type'=>'ProcessingArea'}
      ]
      @parameters = []
    end

    def update(params)
      @ts = params[:timestamp] || Time.now
      @context['reportingTimeStamp'] = @ts.utc.iso8601(3)
      if params.has_key?(:eqp)
        @eqp = params[:eqp]
        @context[:'ns:Equipment']['id'] = @eqp
      end
      if params.has_key?(:parameter)
        @parameters = []
        params[:parameter].each_pair {|k, v| add_parameter(k, v, params)}
      end
    end

    # add equipment state parameters
    def add_parameter(name, reading_values, params={})
      ($log.warn "add_parameter: wrong name #{name.inspect}"; return) unless name.instance_of?(String)
      ptype = params[:dcp_type] || 'Event'
      pvalid = (params[:parameter_valid] != false)
      premeas = (params[:parameter_remeasured] == true)
      pnames = @parameters.collect {|p| p[:name]}
      unless pnames.member?(name)
        @parameters << {
          'dcpRequestType'=>ptype,
          'name'=>name,
          'parameterValid'=>pvalid,
          'parameterRemeasuredFlag'=>premeas,
          'ns:Reading': [],
        }
      end
      iparam = nil
      @parameters.each_with_index {|p, i| (iparam = i; break) if p[:name] == name}
      add_readings(reading_values, {iparam: iparam}.merge(params))
    end

    def add_readings(reading_values, params={})
      iparam = params[:iparam] || -1
      rnumber = params[:reading_number] || (@parameters[iparam][:'ns:Reading'].size + 1)
      rvalid = params[:reading_valid] != false
      ts = params[:reading_timestamp] || (@ts - 10)
      remeasured = params[:remeasured] == true
      #
      # ppas = params[:processing_area] || processing_areas
      ppas = @context[:'ns:Equipment'][:'ns:ProcessingArea'].collect {|pa| pa['id']}
      reading_values.each_with_index {|v, i|
        vtype = params[:value_type]
        if vtype.nil?
          if v.class == Integer
            vtype = 'integer'   # 'unsigned integer'
          elsif v.class == Float
            vtype = 'float'
          else
            vtype = 'string'
          end
        end
        reading = {
          'Equipment'=>@eqp,
          'ProcessingArea'=>ppas[i % ppas.count],
          'readingNumber'=>rnumber,
          'readingValid'=>rvalid,
          'receivedTimeStamp'=>ts.utc.iso8601(3),
          'remeasuredReading'=>remeasured,
          'remeasuredReadingNumber'=>0,
          'value'=>v,
          'valueType'=>vtype
        }
        @parameters[iparam][:'ns:Reading'] << reading
        rnumber += 1
      }
    end

    def submit(params={})
      update(params)
      h = {'soap:Envelope'=>{
        'xmlns:soap'=>'http://schemas.xmlsoap.org/soap/envelope/',
        'xmlns:ns'=>'http://www.amd.com/schemas/DataCollectionReport/DCR_2_2_2',
        'xmlns:MDSSubmitRunPort'=>'http://com.amd.mds.adapter.EIRTUpdater',
        'soap:Body': {
          :'MDSSubmitRunPort:onMessage'=>{
            'soap:encodingStyle'=>'http://schemas.xmlsoap.org/soap/encoding/',
            :event=>{:'ns:Context'=>@context, :'ns:Parameter'=>@parameters}
          }
        }
      }}
      s = XMLHash.hash_to_xml(h)
      @mq.send_msg(s, bytes_msg: true)
    end

  end


  # Send EI ToolAlarms
  class Alarm < Creator
    attr_accessor :alarm

    def initialize(env, params={})
      super
      @context.merge!({'eventReportingName'=>'UnknownAlarm', 'eventType'=>'alarm'})
      @alarm = {'errorCode'=>99876, 'errorText'=>'QA Test', 'state'=>true}
    end

    def update(params={})
      if params.has_key?(:timestamp)
        @ts = params[:timestamp]
        @context['reportingTimeStamp'] = @ts.utc.iso8601(3)
      end
      if params.has_key?(:eqp)
        @eqp = params[:eqp]
        @context[:'ns:Equipment']['id'] = @eqp
      end
      @alarm['errorCode'] = params[:code] if params.has_key?(:code)
      @alarm['errorText'] = params[:text] if params.has_key?(:text)
    end

    def submit(params={})
      update(params)
      h = {'soap:Envelope'=>{
        'xmlns:soap'=>'http://schemas.xmlsoap.org/soap/envelope/',
        'xmlns:ns'=>'http://www.amd.com/schemas/DataCollectionReport/DCR_2_2_2',
        'xmlns:MDSSubmitRunPort'=>'http://com.amd.mds.adapter.EIRTUpdater',
        'soap:Body': {
          'MDSSubmitRunPort:onMessage': {
            # 'soap:encodingStyle'=>'http://schemas.xmlsoap.org/soap/encoding/',
            event: {:'ns:Context'=>@context, :'ns:AlarmReporting'=>@alarm}
          }
        }
      }}
      s = XMLHash.hash_to_xml(h)
      return s if params[:nosend]
      @mq.send_msg(s, bytes_msg: true)
    end

  end


  # Find WaferHistory Data in the MDS
  class MDS
    require 'dbsequel'
    attr_accessor :db

    def initialize(env, params={})
      @db = DB.new("mds.#{env}", params)
    end

    def history_view_eqp(params={})
      ($log.warn "history_view_eqp: no selectors"; return) if params.empty?
      qargs = []
      q = 'select * from T_HISTORY_VIEW_EQP'
      q, qargs = @db._q_restriction(q, qargs, 'EQP_ID', params[:eqp])
      q, qargs = @db._q_restriction(q, qargs, 'TEXT', params[:text])
      q += ' ORDER BY SEQ'
      return @db.select(q, *qargs)
    end

    def history_view_wafer(params={})
      ($log.warn "history_view_wafer: no selectors"; return) if params.empty?
      qargs = []
      q = 'select * from T_HISTORY_VIEW_WAFER'
      q, qargs = @db._q_restriction(q, qargs, 'CONTROL_JOB', params[:cj])
      q, qargs = @db._q_restriction(q, qargs, 'EVENT', params[:event])
      q, qargs = @db._q_restriction(q, qargs, 'WAFER_ID', params[:wafer])
      return @db.select(q, *qargs)
    end

    def tool_para(params={})
      ($log.warn 'tool_para: no selectors'; return) if params.empty?
      qargs = []
      q = 'select * from T_TOOL_PARA'
      q, qargs = @db._q_restriction(q, qargs, 'EQP_ID', params[:eqp])
      q, qargs = @db._q_restriction(q, qargs, 'PARA_NAME', params[:name])
      q, qargs = @db._q_restriction(q, qargs, 'PARA_VALUE', params[:value].to_s) if params[:value]
      return @db.select(q, *qargs)
    end

  end

end
