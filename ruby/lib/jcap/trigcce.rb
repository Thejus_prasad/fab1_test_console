=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author:     ssteidte, 2012-01-24

Version:    1.2.2

History:
  2014-09-12 ssteidte, cleanup
  2015-04-27 ssteidte, added CCE test support
  2016-08-24 sfrieske, use lib rqrsp_mq instead of rqrsp
  2017-12-13 sfrieske, use mqjms, use hash_to_xml instead of builder, separate from CCE App testing lib
  2021-07-27 sfrieske, use rexml/document directly instead of hash_to_xml

Note:
  the CCE app must not be connected to the request queue
=end

require 'rexml/document'
require 'mqjms'
require 'siview'


module CCE

  # jCAP TrigCertCreateToCCE testing
  class TrigCCE
    attr_accessor :sv, :mq_request, :mq_response, :timeout, :_msg, :_doc

    def initialize(env, params={})
      @sv = params[:sv] || SiView::MM.new(env)
      @mq_request = MQ::JMS.new("trigcce.request.#{env}")
      @mq_response = MQ::JMS.new("trigcce.reply.#{env}")
      @timeout = params[:timeout] || 70
    end

    # receive a msg from TrigCertCreateToCCE to CCE on behalf of CCE unless passed, return true if correct
    def verify_trigger(lot, msg=nil)
      $log.info "verify_trigger #{lot.inspect}"
      msg ||= @mq_request.receive_msg(timeout: @timeout)
      @_msg = msg   # for dev only
      ($log.warn '  no message received'; return) unless msg
      msg.include?('sendTrigger') || ($log.warn "wrong msg:\n#{res.inspect}"; return)
      li = @sv.lot_info(lot)
      msg.include?("Lot_Id>#{lot}<") || ($log.warn "wrong lot:\n#{res.inspect}"; return)
      msg.include?("MainPd_Id>#{li.route}<") || ($log.warn "wrong route:\n#{res.inspect}"; return)
      msg.include?("Pd_Id>#{li.op}<") || ($log.warn "wrong PD:\n#{res.inspect}"; return)
      msg.include?("Op_No>#{li.opNo}<") || ($log.warn "wrong opNo:\n#{res.inspect}"; return)
      return true
    end

    # send a Success/Failed/Reject msg on behalf of CCE
    def send_cce_response(lot, returncode, params={})
      $log.info "send_cce_response #{lot.inspect}, #{returncode.inspect}, #{params}"
      comment = params[:comment] || case returncode
        when 'Success'; 'Approved & Customer default used'
        when 'Failed'; 'Invalid lot id in SiView request'
        when 'Reject'; 'Lot has been rejected (QA test)'
        when 'Rework'; 'Lot has been selected for rework (QA test)'
        else 'QA Test'
      end
      @_doc = doc = REXML::Document.new('<?xml version="1.0" encoding="UTF-8"?>')
      tag = doc.add_element('env:Envelope', {'xmlns:env'=>'http://schemas.xmlsoap.org/soap/envelope/'})
      .add_element('env:Body')
      .add_element('ns1:cceResponse', {'xmlns:ns1'=>'http://www.globalfoundries.com/jcap/cceResponse'})
      tag.add_element('ns1:Lot_Id').text = lot
      tag.add_element('ns1:Return_Code').text = returncode
      tag.add_element('ns1:Comment').text = comment
      if returncode == 'Success' && !params[:nowafers]
        waferstag = tag.add_element('ns1:WaferList')
        @sv.lot_info(lot, wafers: true).wafers.each {|w|
          wtag = waferstag.add_element('ns1:Wafer')
          wtag.add_element('ns1:Wafer_id').text = w.slot
          wtag.add_element('ns1:Wafer_Scribe').text = w.wafer
          wtag.add_element('ns1:OQAGoodDie').text = 800 + w.slot
          wtag.add_element('ns1:OQABadDie').text = 800 - w.slot
        }
      end
      #
      s = String.new
      tag.document.write(s)
      return @mq_response.send_msg(s)
    end

  end

end
