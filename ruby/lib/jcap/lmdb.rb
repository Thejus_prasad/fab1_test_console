=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author:   Steffen Steidten, 2013-11-18

Version:  1.2.0

History:
  2015-09-08 ssteidte, minor fixes, switch to API 1.1
  2016-01-15 sfrieske, adapted for version 16.01
  2018-02-15 sfrieske, replaced select_all by select
  2019-01-28 sfrieske, minor cleanup
  2020-05-04 sfrieske, added support for LotsWithCategory (for AutoPilot)
=end

require 'dbsequel'

 
# LotManager/RTD Integration Testing Support  
module LotManager
  #
  LmRequest = Struct.new(
    # LM_REQUEST
    :rqid, :maxlots, :maxwafers, :minlots, :remlots, :remwafers, :selrule, :selcount, 
    :selcount_min, :selwafers, :noabort_count, :expiration, :claim_time, :claim_memo, 
    :cascade_expiration, :stacksize, :jeoblock_expiration, :jeoblock_recoverycount, :init_rqid,
    # LM_REQUEST_LOT, space separated strings   TODO: make it Arrays
    :lots_jeopardy, :lots_pilot, :lots_candidate, :lots_withcategory,
    # LM_REQUEST_CONTEXT and T_LM_DYNAMIC_COUNT
    :rqcontext, :rqdynamic)
  LmRequestContext = Struct.new(:rqid, :name, :value)
  LmRequestDynamic = Struct.new(:rqid, :unit, :type, :count, :dyn_expiration)
  LmRequestLot = Struct.new(:rqid, :category, :lot)
  #
  LotSummary = Struct.new(:context, :pclbase, :pclqtskip, :pclsitesamplings, :pclchambers, 
    :recipes, :prios, :reservations, :limitations, :rqrequired)
  LmContext = Struct.new(:ctxid, :lot, :pd, :eqp)
  LmPclBase = Struct.new(:toolwish, :toolwish_time, :toolwish_memo, :toolscore, :toolscore_time, 
    :toolscore_memo, :selwafers, :selwafers_src, :selwafers_time, :selwafers_memo)
  LmPclQTSkip = Struct.new(:target_time, :claim_time, :claim_memo)
  LmPclSiteSampling = Struct.new(:ptype, :stype, :wafer)
  LmPclChamber = Struct.new(:chamber, :value, :claim_time, :claim_memo)
  LmRecipe = Struct.new(:type, :name, :claim_time, :claim_memo)
  LmPrio = Struct.new(:app, :targetpd, :prio, :expiration, :due, :claim_time, :claim_memo)
  LmReservation = Struct.new(:rqid, :app, :level, :value, :auto3, :init, :claim_time, :claim_memo)
  LmLimitation = Struct.new(:rqid, :app, :level, :value, :auto3, :init, :claim_time, :claim_memo)
  LmRqRequired = Struct.new(:app, :type)

  
  class DB
    attr_accessor :db
    
    def initialize(env, params={})
      @db = ::DB.new("mds.#{env}", params)
    end
    
    # LmRequest tables
    
    def lmrequest(rqid)
      qargs = []
      q = 'select LM_REQUEST_ID, MAX_COUNT_LOT, MAX_COUNT_WAFER, MIN_COUNT_LOT, REMAINING_COUNT_LOT, 
        REMAINING_COUNT_WAFER, WAFER_SELECTION_RULE, WAFER_SELECTION_COUNT, WAFER_SELECTION_COUNT_MIN, WAFER_SELECTION,
        NO_ABORT_WAFER_QUANTITY, EXPIRATION_DATE, CLAIM_TIME, CLAIM_MEMO , CASCADE_EXPIRATION_TIME, STACK_SIZE,
        JEO_BLOCK_EXPIRATION_DATE, JEO_BLOCK_RECOVERY_COUNT, INIT_LM_REQUEST_ID from T_LM_REQUEST'
      q, qargs = @db._q_restriction(q, qargs, 'LM_REQUEST_ID', rqid)
      res = @db.select(q, *qargs) || return
      return res.collect {|r|
        [0, 8, 10, 15, 17, 18].each {|i| r[i] = r[i].to_i}
        LmRequest.new(*r)
      }
    end

    def free_rqid(params={})
      min = params[:min] || 9000000000000
      max = params[:max] || (min + 100)
      res = @db.select('select LM_REQUEST_ID from T_LM_REQUEST WHERE LM_REQUEST_ID >= ? and LM_REQUEST_ID <= ?', min, max)
      rqids_used = res.collect {|e| e.first.to_i}.sort
      rqid = min
      rqid = rqid.next while rqids_used.member?(rqid)
      return rqid == max ? nil : rqid
    end

    # holds additional metadata for APC threads
    def lmrequest_context(rqid)
      qargs = []
      q = 'select LM_REQUEST_ID, CONTEXT_NAME, CONTEXT_VALUE from T_LM_REQUEST_CONTEXT'
      q, qargs = @db._q_restriction(q, qargs, 'LM_REQUEST_ID', rqid)
      res = @db.select(q, *qargs) || return
      return res.collect {|r| LmRequestContext.new(r[0].to_i, r[1], r[2])}
    end

    # holds data about time dependend jeopardy limits
    def lmrequest_dynamic(rqid)
      qargs = []
      q = 'select LM_REQUEST_ID, UNIT_ID, TYPE_ID, UNIT_COUNT, EXPIRATION_DATE from T_LM_DYNAMIC_COUNT'
      q, qargs = @db._q_restriction(q, qargs, 'LM_REQUEST_ID', rqid)
      res = @db.select(q, *qargs) || return
      return res.collect {|r|
        [0, 3].each {|i| r[i] = r[i].to_i}
        LmRequestDynamic.new(*r)
      }
    end

    def lmrequest_lot(params={})
      qargs = []
      q = 'select LM_REQUEST_ID, CATEGORY_ID, LOT_ID from T_LM_REQUEST_LOT'
      q, qargs = @db._q_restriction(q, qargs, 'LM_REQUEST_ID', params[:rqid])
      q, qargs = @db._q_restriction(q, qargs, 'CATEGORY_ID', params[:category])
      q, qargs = @db._q_restriction(q, qargs, 'LOT_ID', params[:lot])
      q += ' order by LM_REQUEST_ID, CATEGORY_ID, LOT_ID'
      res = @db.select(q, *qargs) || return
      return res.collect {|r| LmRequestLot.new(r[0].to_i, r[1], r[2])}
    end
    
    # LotSummary tables
    
    def lmcontext(params={})
      qargs = []
      q = 'select LM_CONTEXT_ID, LOT_ID, PD_ID, EQP_ID from T_LM_CONTEXT'
      q, qargs = @db._q_restriction(q, qargs, 'LM_CONTEXT_ID', params[:ctxid])
      q, qargs = @db._q_restriction(q, qargs, 'CLAIM_TIME', params[:claim_time])
      q, qargs = @db._q_restriction(q, qargs, 'LOT_ID', params[:lot])
      q, qargs = @db._q_restriction(q, qargs, 'PD_ID', params[:pd])
      q, qargs = @db._q_restriction(q, qargs, 'EQP_ID', params[:eqp])
      ctx = params[:context]
      if ctx
        q, qargs = @db._q_restriction(q, qargs, 'LOT_ID', ctx.lot)
        q, qargs = @db._q_restriction(q, qargs, 'PD_ID', ctx.pd)
        q, qargs = @db._q_restriction(q, qargs, 'EQP_ID', ctx.eqp)
      end
      res = @db.select(q, *qargs) || return
      return res.collect {|r|
        r[0] = r[0].to_i
        LmContext.new(*r)
      }
    end
    
    def lmpclbase(ctxid)
      q = 'select TOOL_WISH_STR, TOOL_WISH_CLAIM_TIME, TOOL_WISH_CLAIM_MEMO, TOOL_SCORE, 
        TOOL_SCORE_CLAIM_TIME, TOOL_SCORE_CLAIM_MEMO, SELECTED_WAFERS, SELECTED_WAFERS_SOURCE, 
        SELECTED_WAFERS_CLAIM_TIME, SELECTED_WAFERS_CLAIM_MEMO from T_LM_PCL_BASE where LM_CONTEXT_ID=?'
      res = @db.select(q, ctxid) || return
      return res.collect {|r| LmPclBase.new(*r)}
    end
    
    def lmpclqtskip(ctxid)
      q = 'select TARGET_TIME, CLAIM_TIME, CLAIM_MEMO from T_LM_PCL_QTSKIP where LM_CONTEXT_ID=?'
      res = @db.select(q, ctxid) || return
      return res.collect {|r| LmPclQTSkip.new(*r)}
    end
    
    def lmpclsitesampling(ctxid)
      q = 'select PARAMETER_TYPE, SITE_TYPE, WAFER_ID from T_LM_PCL_SITESAMPLING where LM_CONTEXT_ID=?'
      res = @db.select(q, ctxid) || return
      return res.collect {|r| LmPclSiteSampling.new(*r)}
    end
    
    def lmpclchamber(ctxid)
      q = 'select CHAMBER_ID, CHAMBER_WISH, CHAMBER_WISH_CLAIM_TIME, 
        CHAMBER_WISH_CLAIM_MEMO from T_LM_PCL_CHAMBER_WISH where LM_CONTEXT_ID=?'
      res = @db.select(q, ctxid) || return
      return res.collect {|r| LmPclChamber.new(*r)}
    end
    
    def lmrecipe(ctxid)
      q = 'select RECIPE_TYPE, RECIPE_NAME, CLAIM_TIME, CLAIM_MEMO 
        from T_LM_RECIPE_INFO where LM_CONTEXT_ID=?'
      res = @db.select(q, ctxid) || return
      return res.collect {|r| LmRecipe.new(*r)}
    end
    
    def lmprio(ctxid)
      q = 'select APPLICATION_ID, TARGET_PD_ID, PRIORITY, EXPIRATION_DATE, DUE_DATE, 
        CLAIM_TIME, CLAIM_MEMO from T_LM_PRIORITIZATION where LM_CONTEXT_ID=?'
      res = @db.select(q, ctxid) || return
      return res.collect {|r| LmPrio.new(*r)}
    end
    
    def lmreservation(ctxid)
      q = 'select LM_REQUEST_ID, APPLICATION_ID, REQ_LEVEL_NAME, REQ_LEVEL_VALUE,
        AUTO3_FLAG, INIT_FLAG, CLAIM_TIME, CLAIM_MEMO from T_LM_RESERVATION where LM_CONTEXT_ID=?'
      res = @db.select(q, ctxid) || return
      return res.collect {|r|
        r[0] = r[0].to_i
        LmReservation.new(*r)
      }
    end

    def lmlimitation(ctxid)
      q = 'select LM_REQUEST_ID, APPLICATION_ID, REQ_LEVEL_NAME, REQ_LEVEL_VALUE,
        AUTO3_FLAG, INIT_FLAG, CLAIM_TIME, CLAIM_MEMO from T_LM_LIMITATION where LM_CONTEXT_ID=?'
      res = @db.select(q, ctxid) || return
      return res.collect {|r|
        r[0] = r[0].to_i
        LmLimitation.new(*r)
      }
    end

    def lmrqrequired(ctxid)
      q = 'select APPLICATION_ID, REQUEST_TYPE from T_LM_REQUEST_REQUIRED where LM_CONTEXT_ID=?'
      res = @db.select(q, ctxid) || return
      return res.collect {|r| LmRqRequired.new(*r)}
    end
    
    # combined LotSummary data (by :ctxid or context)
    def lotsummary(params={})
      ctxs = lmcontext(params) || return
      return ctxs.collect {|ctx|
        ctxid = ctx.ctxid
        LotSummary.new(ctx, lmpclbase(ctxid).first, lmpclqtskip(ctxid).first,
          lmpclsitesampling(ctxid), lmpclchamber(ctxid), lmrecipe(ctxid), 
          lmprio(ctxid), lmreservation(ctxid), lmlimitation(ctxid), 
          lmrqrequired(ctxid))
      }
    end

  end
    
end  
