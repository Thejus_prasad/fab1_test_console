=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author:   Steffen Steidten, 2013-11-18

Version:  1.2.1

History:
  2015-09-07 ssteidte, switch to API 1.1
  2016-01-15 sfrieske, adapted for v16.01
  2016-08-24 sfrieske, use lib rqrsp_mq instead of rqrsp
  2016-11-02 sfrieske, added delete_lot_summary_lot etc to support test cleanup
  2019-01-24 sfrieske, removed dependency on Builder
  2020-05-04 sfrieske, added support for LotsWithCategory (for AutoPilot)
  2021-02-05 sfrieske, use instance_of instead of kind_of
  2021-08-31 sfrieske, requiring 'time' for #iso8601
=end

require 'time'
require 'rqrsp_mq'
require_relative 'lmdb'    # to get the structs


module LotManager

  # Interact with LotManager via the LMTransformer JCAP job
  class Transformer
    attr_accessor :mq, :lmrequest_data, :lotsummary_data
      # :lmreq_report_times, :lotsum_report_times, :lotsumdel_report_times,
      # :updwish_report_times, :updrec_report_times, :updselwaf_report_times

    def initialize(env, params={})
      @mq = RequestResponse::MQXML.new("lmtrans.#{env}")
      # @lmreq_report_times = []
      # @lotsum_report_times = []
      # @lotsumdel_report_times = []
      # @updwish_report_times = []
      # @updrec_report_times = []
      # @updselwaf_report_times = []
    end

    def inspect
      "#<#{self.class.name} #{@mq.inspect}>"
    end

    # set valid default data for requests
    def set_default_data
      $log.info "set_default_data"
      # ensure times have no fractions of a second for further comparisons
      now = Time.at(Time.now.to_i)
      @lmrequest_data = 3.times.collect {|i|
        rqid = 9966991 + i
        LmRequest.new(rqid, 2, 3, 1, 20, 21, 'WFRSELRULE', 4, 2, "QAWafer#{i+1}1 QAWafer#{i+1}2",
          2, now, now, "QA Test#{i+1}", now, i+1, now, 5, rqid,
          ["QALot#{i+1}1", "QALot#{i+1}2", "QALot#{i+1}3"],
          ["QALot#{i+1}4"],
          ["QALot#{i+1}5", "QALot#{i+1}6"],
          nil,
          [LmRequestContext.new(rqid, "QAName#{i+1}1", "QAValue#{i+1}1")],
          [LmRequestDynamic.new(rqid, 'WAFER', 'MIN', 3, now)]
        )
      }
      @lotsummary_data = 3.times.collect {|i|
        LotSummary.new(
          LmContext.new(nil, "QALot#{i+1}1", "QAPD#{i+1}1", "QAEQP#{i+1}1"),
          LmPclBase.new('NEVER', now, 'QA ToolWish', 31+i, now, 'QA ToolScore' , 4, 'MANUAL', now, 'QA Sel'),
          LmPclQTSkip.new(now + 600, now, 'QA QTSkip'),
          [LmPclSiteSampling.new("SmplParameter#{i+1}1", "SmplSite#{i+1}1", "QAWFR#{i+1}1")],
          [LmPclChamber.new("QACH#{i+1}", 'QAChamberWish', now, 'QA ChamberWish')],
          [LmRecipe.new("QA_TYPE#{i+1}", "QA-RECIPE-#{i+1}", now, 'QA CH')],
          [LmPrio.new("QAAPP#{i+1}", "QATARGETPD#{i+1}", 2, now, now, now, 'QA Prioritazation')],
          [LmReservation.new(9966991+i, "QAAPP#{i+1}", 'Mainframe', "QAEQP#{i+1}", 0, 0, now, 'QA Rsv')],
          [LmLimitation.new(9966991+i, "QAAPP#{i+1}", 'Mainframe', "QAEQP#{i+1}", 0, 0, now, 'QA Limit')],
          [LmRqRequired.new("QAAPP#{i+1}", 'Limitation')]
        )
      }
      true
    end

    def _change_data(data, excluded=[])
      if data.instance_of?(Array)
        data.each {|d| _change_data(d, excluded)}
      else
        now = Time.at(Time.now.to_i)
        data.members.each {|m|
          next if data[m].nil? || excluded.include?(m)
          if data[m].kind_of?(Time)
            data[m] = now
          elsif data[m].instance_of?(Array)
            data[m] = data[m].collect {|e| e.next}
          else
            data[m] = data[m].next
          end
        }
      end
    end

    def change_default_data
      $log.info "change_default_data"
      @lmrequest_data.each {|data|
        _change_data(data, [:rqid, :rqcontext, :rqdynamic])
        _change_data(data.rqcontext, [:rqid, :name])
        _change_data(data.rqdynamic, [:rqid])
      }
      @lotsummary_data.each {|data|
        _change_data(data.pclbase)
        _change_data(data.pclqtskip)
        _change_data(data.pclsitesamplings, [:ptype])
        _change_data(data.pclchambers, [:chamber])
        _change_data(data.recipes, [:name])
        _change_data(data.prios, [:app])
        _change_data(data.reservations, [:rqid, :level, :app])
        _change_data(data.limitations, [:rqid, :level, :app])
        _change_data(data.rqrequired, [:type])
      }
      true
    end

    def get_version
      $log.info 'LotManager::Transformer get_version'
      h = {'soap:Envelope': {
        'xmlns:soap'=>'http://www.w3.org/2003/05/soap-envelope',
        'xmlns:urn'=>'urn:com.globalfoundries.lotmanager.jcap.types:1.1',
        'soap:Body': {'urn:GetApiVersion': nil}
      }}
      res = @mq.send_receive(h, timeout: 20) || return
      return res[:GetApiVersionResponse][:ApiVersion]['version']
    end

    # sends data like @lmrequest_data, given as LmRequest struct(s)
    def update_lmrequests(params={})
      data = params[:data] || @lmrequest_data
      data = [data] unless data.instance_of?(Array)
      $log.info {'update_lmrequests'} unless params[:silent]
      lmrqs = data.collect {|d|
        expiration = d.expiration.instance_of?(String) ? d.expiration : d.expiration.utc.iso8601  # special TC
        rq = {
          'RequestID'=>d.rqid, 'MaxCountLot'=>d.maxlots, 'MaxCountWafer'=>d.maxwafers,
          'MinCountLot'=>d.minlots, 'RemainingCountLot'=>d.remlots, 'RemainingCountWafer'=>d.remwafers,
          'Expiration'=>expiration, 'WaferSelectionRule'=>d.selrule, 'WaferSelectionCount'=>d.selcount,
          'WaferSelectionCountMin'=>d.selcount_min, 'WaferSelection'=>d.selwafers,
          'ClaimMemo'=>d.claim_memo, 'ClaimTime'=>d.claim_time.utc.iso8601, 'StackSize'=>d.stacksize,
          'LotsJeopardy'=>d.lots_jeopardy.join(' '), 'LotsPilot'=>d.lots_pilot.join(' '),
          'LotsCandidate'=>d.lots_candidate.join(' '),
          'JeopardyBlockRecoveryCount'=>d.jeoblock_recoverycount, 'NoAbortWaferCount'=>d.noabort_count
        }
        rq['CascadeExpirationTime'] = d.cascade_expiration.utc.iso8601 unless d.cascade_expiration.nil?
        rq['JeopardyBlockExpiration'] = d.jeoblock_expiration.utc.iso8601 unless d.jeoblock_expiration.nil?
        rq['InitialRequestID'] = d.init_rqid unless d.init_rqid.nil?
        if d.lots_withcategory  # a Hash {cat1: [lots], ..}
          rq[:LotsWithCategory] = d.lots_withcategory.each_pair.collect {|cat, lots|
            {'CategoryId'=>cat, LotId: lots}
          }
        end
        rq[:RequestContext] = d.rqcontext.collect {|rc| {'ContextValue'=>rc.value, 'ContextName'=>rc.name}}
        rq[:DynamicCount] = d.rqdynamic.collect {|rd|
          {'ExpirationDate'=>rd.dyn_expiration.utc.iso8601,
            'UnitCount'=>rd.count, 'TypeId'=>rd.type, 'UnitId'=>rd.unit}
        }
        rq
      }
      h = {'soap:Envelope': {'xmlns:soap'=>'http://www.w3.org/2003/05/soap-envelope', 'soap:Body': {
        UpdateLmRequests11Request: {
          'xmlns:ns2'=>'urn:com.globalfoundries.PCL.LotManager:1.1',
          'xmlns'=>'urn:com.globalfoundries.lotmanager.jcap.types:1.1',
          LmRequest: lmrqs
        }
      }}}
      res = @mq.send_receive(h) || return
      ret = res[:GenericResponse11][:Result]['Message'] == 'success'
      $log.warn res unless ret
      return ret
    end

    # deletes LmRequests
    def delete_lmrequests(rqids, params={})
      rqids = [rqids] unless rqids.instance_of?(Array)
      $log.info "delete_lmrequests #{rqids.inspect}"
      h = {'soap:Envelope': {
        'xmlns:soap'=>'http://www.w3.org/2003/05/soap-envelope',
        'xmlns:urn'=>'urn:com.globalfoundries.lotmanager.jcap.types:1.1',
        'soap:Body': {'urn:DeleteLmRequests11Request': rqids.collect {|rqid|
          {'urn:DeleteRequest': {'RequestID'=>rqid, 'CascadeFlag'=>!!params[:cascade]}}
        }}
      }}
      res = @mq.send_receive(h) || return
      ret = res[:GenericResponse11][:Result]['Message'] == 'success'
      $log.warn res unless ret
      return ret
    end

    # Lot Summary

    # sends data like @lotsummary_data, given as LmRequest struct or Hash or Array of any of them
    def update_lotsummary(params={})
      data = params[:data] || @lotsummary_data
      data = [data] unless data.instance_of?(Array)
      $log.info {'update_lotsummary'} unless params[:silent]
      lsums = data.collect {|d|
        ret = {'LotID'=>d.context.lot, 'OperationID'=>d.context.pd, 'EquipmentID'=>d.context.eqp}
        if p = d.pclbase
          ctime = p.selwafers_time.instance_of?(String) ? p.selwafers_time : p.selwafers_time.utc.iso8601  # string: special TC
          ret[:'urn1:SelectedWafer'] = {
            'Value'=>p.selwafers, 'Source'=>p.selwafers_src, 'ClaimTime'=>ctime, 'ClaimMemo'=>p.selwafers_memo
          }
          if v = p.toolwish
            ret[:'urn1:ToolWish'] = {
              'Value'=>v, 'ClaimTime'=>p.toolwish_time.utc.iso8601, 'ClaimMemo'=>p.toolwish_memo
            }
          end
          if v = p.toolscore
            ret[:'urn1:ToolScore'] = {
              'Value'=>v, 'ClaimTime'=>p.toolscore_time.utc.iso8601, 'ClaimMemo'=>p.toolscore_memo
            }
          end
        end
        if p = d.pclqtskip
          ret[:'urn1:QTSkip'] = {'ClaimTime'=>p.claim_time.utc.iso8601, 'ClaimMemo'=>p.claim_memo}
          ret[:'urn1:QTSkip']['TargetTime'] = p.target_time.utc.iso8601 if p.target_time.kind_of?(Time)
        end
        if p = d.pclsitesamplings
          ret[:'urn1:SiteSamplings'] = {'urn1:SiteSampling': p.collect {|e|
            {'ParameterType'=>e.ptype, 'SiteType'=>e.stype, 'WaferID'=>e.wafer}
          }}
        end
        if p = d.pclchambers
          ret[:'urn1:ChamberWishes'] = {'Count'=>p.size, 'urn1:ChamberWish': p.collect {|e|
            {'Chamber'=>e.chamber, 'Value'=>e.value, 'ClaimTime'=>e.claim_time.utc.iso8601, 'ClaimMemo'=>e.claim_memo}
          }}
        end
        if p = d.recipes
          ret[:'urn1:Recipes'] = {'Count'=>p.size, 'urn1:Recipe': p.collect {|e|
            {'Type'=>e.type, 'Name'=>e.name, 'ClaimTime'=>e.claim_time.utc.iso8601, 'ClaimMemo'=>e.claim_memo}
          }}
        end
        if p = d.prios
          ret[:'urn1:Prioritizations'] = {'Count'=>p.size, 'urn1:Prioritization': p.collect {|e|
            {'ApplicationID'=>e.app, 'Priority'=>e.prio, 'Expiration'=>e.expiration.utc.iso8601, 'DueDate'=>e.due.utc.iso8601,
             'TargetPD'=>e.targetpd, 'ClaimTime'=>e.claim_time.utc.iso8601, 'ClaimMemo'=>e.claim_memo}
          }}
        end
        if p = d.reservations
          ret[:'urn1:Reservations'] = {'Count'=>p.size, 'urn1:Reservation': p.collect {|e|
            {'RequestLevelName'=>e.level, 'RequestLevelValue'=>e.value, 'ApplicationID'=>e.app, 'Initialization'=>e.init,
             'Auto3'=>e.auto3, 'RequestID'=>e.rqid, 'ClaimTime'=>e.claim_time.utc.iso8601, 'ClaimMemo'=>e.claim_memo}
          }}
        end
        if p = d.limitations
          ret[:'urn1:Limitations'] = {'Count'=>p.size, 'urn1:Limitation': p.collect {|e|
            {'RequestLevelName'=>e.level, 'RequestLevelValue'=>e.value, 'ApplicationID'=>e.app, 'Initialization'=>e.init,
             'Auto3'=>e.auto3, 'RequestID'=>e.rqid, 'ClaimTime'=>e.claim_time.utc.iso8601, 'ClaimMemo'=>e.claim_memo}
          }}
        end
        if p = d.rqrequired
          ret[:'urn1:RequestsRequired'] = {'Count'=>p.size, 'urn1:RequestRequired': p.collect {|e|
            {'RequestType'=>e.type, 'ApplicationID'=>e.app}
          }}
        end
        ret
      }
      h = {'soap:Envelope': {
        'xmlns:soap'=>'http://www.w3.org/2003/05/soap-envelope',
        'xmlns:urn'=>'urn:com.globalfoundries.lotmanager.jcap.types:1.1',
        'xmlns:urn1'=>'urn:com.globalfoundries.PCL.LotManager:1.1',
        'soap:Body': {'urn:UpdateLotSummary11Request': {'urn:LotSummary': lsums}}
      }}
      res = @mq.send_receive(h) || return
      ret = res[:GenericResponse11][:Result]['Message'] == 'success'
      $log.warn res unless ret
      return ret
    end

    # update recipe data only
    def update_recipes(params={})
      delete = params[:delete] || []  # array of data indexes, if true all info not in sent data will be deleted
      data = params[:data] || @lotsummary_data
      data = [data] unless data.instance_of?(Array)
      $log.info 'update_recipes'
      rictxs = data.each_with_index.collect {|d, i|
        ctx = {'LotID'=>d.context.lot, 'OperationID'=>d.context.pd, 'EquipmentID'=>d.context.eqp}
        ris = d.recipes.collect {|r|
          {'Type'=>r.type, 'Name'=>r.name, 'ClaimTime'=>r.claim_time.utc.iso8601, 'ClaimMemo'=>r.claim_memo}
        }
        ret = delete.include?(i) ? {'deleteFlag'=>true} : {}
        ret.merge({'urn:Context': ctx, 'urn:RecipeInfo': ris})
      }
      h = {'soap:Envelope': {
        'xmlns:soap'=>'http://www.w3.org/2003/05/soap-envelope',
        'xmlns:urn'=>'urn:com.globalfoundries.lotmanager.jcap.types:1.1',
        'soap:Body': {'urn:UpdateRecipeInfo11Request': {'urn:RecipeInfosWithContext': rictxs}}
      }}
      res = @mq.send_receive(h) || return
      ret = res[:GenericResponse11][:Result]['Message'] == 'success'
      $log.warn res unless ret
      return ret
    end

    # update wafer selection
    def update_wafer_selection(params={})
      data = params[:data] || @lotsummary_data
      data = [data] unless data.instance_of?(Array)
      $log.info 'update_wafer_selection'
      wsctxs = data.collect {|d|
        ctx = {'LotID'=>d.context.lot, 'OperationID'=>d.context.pd, 'EquipmentID'=>d.context.eqp}
        p = d.pclbase
        ws = {'Value'=>p.selwafers, 'Source'=>p.selwafers_src,
          'ClaimTime'=>p.selwafers_time.utc.iso8601, 'ClaimMemo'=>p.selwafers_memo}
        ret = {'urn:Context': ctx, 'urn:WaferSelection': ws}
        if d.pclsitesamplings
          ret[:'urn:SiteSamplings'] = d.pclsitesamplings.collect {|ss|
            {'urn1:SiteSampling': {'ParameterType'=>ss.ptype, 'SiteType'=>ss.stype, 'WaferID'=>ss.wafer}}
          }
        end
        ret
      }
      h = {'soap:Envelope': {
        'xmlns:soap'=>'http://www.w3.org/2003/05/soap-envelope',
        'xmlns:urn'=>'urn:com.globalfoundries.lotmanager.jcap.types:1.1',
        'xmlns:urn1'=>'urn:com.globalfoundries.PCL.LotManager:1.1',
        'soap:Body': {'urn:UpdateWaferSelection11Request': {'urn:WaferSelectionWithContext': wsctxs}}
      }}
      res = @mq.send_receive(h) || return
      ret = res[:GenericResponse11][:Result]['Message'] == 'success'
      $log.warn res unless ret
      return ret
    end

    # update wishes data only
    def update_wishes(params={})
      delete = params[:delete] || []  # array of data indexes, if true all info not in sent data will be deleted
      data = params[:data] || @lotsummary_data
      data = [data] unless data.instance_of?(Array)
      $log.info 'update_wishes'
      wictxs = data.each_with_index.collect {|d, i|
        ret = delete.include?(i) ? {'deleteFlag'=>true} : {}
        ret[:'urn:Context'] = {'LotID'=>d.context.lot, 'OperationID'=>d.context.pd, 'EquipmentID'=>d.context.eqp}
        # wishes to be kept or added
        p = d.pclbase
        if p.toolwish
          ret[:'urn:ToolWish'] = {'Value'=>p.toolwish, 'ClaimTime'=>p.toolwish_time.utc.iso8601, 'ClaimMemo'=>p.toolwish_memo}
        end
        if p.toolscore
          ret[:'urn:ToolScore'] = {'Value'=>p.toolscore, 'ClaimTime'=>p.toolscore_time.utc.iso8601, 'ClaimMemo'=>p.toolscore_memo}
        end
        # optional, 1 chamber only?
        chambers = d.pclchambers
        if chambers && !chambers.empty?
          ret[:'urn:ChamberWish'] = chambers.collect {|ch|
            {'Chamber'=>ch.chamber, 'Value'=>ch.value, 'ClaimTime'=>ch.claim_time.utc.iso8601, 'ClaimMemo'=>ch.claim_memo}
          }
        end
        ret
      }
      h = {'soap:Envelope': {
        'xmlns:soap'=>'http://www.w3.org/2003/05/soap-envelope',
        'xmlns:urn'=>'urn:com.globalfoundries.lotmanager.jcap.types:1.1',
        'soap:Body': {'urn:UpdateWishes11Request': {'urn:WishesWithContext': wictxs}}
      }}
      res = @mq.send_receive(h) || return
      ret = res[:GenericResponse11][:Result]['Message'] == 'success'
      $log.warn res unless ret
      return ret
    end

    # deletes LotSummaryRequests, pass either LmContext or LotSummary struct as params[:data] with @lmrequest_data as in update
    def delete_lotsummary(params={})
      data = params[:data] || @lotsummary_data
      data = [data] unless data.instance_of?(Array)
      $log.info 'delete_lotsummary'
      h = {'soap:Envelope': {
        'xmlns:soap'=>'http://www.w3.org/2003/05/soap-envelope',
        'xmlns:urn'=>'urn:com.globalfoundries.lotmanager.jcap.types:1.1',
        'soap:Body': {
          'urn:DeleteLotSummary11Request': {'urn:Context': data.collect {|d|
            d = d.context if d.context  # extract LmContext from LotSummary
            {'LotID'=>d.lot, 'OperationID'=>d.pd, 'EquipmentID'=>d.eqp}
          }}
        }
      }}
      res = @mq.send_receive(h) || return
      ret = res[:GenericResponse11][:Result]['Message'] == 'success'
      $log.warn res unless ret
      return ret
    end


    # shortcut methods to simplify test data cleanup

    def delete_lotsummary_lot(lot)
      return delete_lotsummary(data: LmContext.new(nil, lot, '*', '*'))
    end

    def delete_lotsummary_pd(op)
      return delete_lotsummary(data: LmContext.new(nil, '*', op.split('.').first, '*'))
    end

    def delete_lotsummary_eqp(eqp)
      return delete_lotsummary(data: LmContext.new(nil, '*', '*', eqp))
    end

  end

end
