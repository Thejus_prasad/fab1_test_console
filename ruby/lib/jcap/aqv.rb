=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: Steffen Steidten, 2014-05-13

History:
  2015-01-26 ssteidte, adapted SPC message to new requirements
  2016-12-07 sfrieske, use mqjms instead of rqrsp as send and receive are independently used
  2017-03-06 sfrieske, add EI MQ communication for LotCompletion
  2021-02-05 sfrieske, use instance_of instead of kind_of
  2021-08-30 sfrieske, moved xmlhash.rb to util
=end

require 'time'
require 'mqjms'
require 'util/xmlhash'
require 'siview'


module AQV

  # AQV - SAP messages
  class SAP
    attr_accessor :mq, :mqsap

    KitResult = Struct.new(:order, :result, :lines)

    def initialize(env, params={})
      @mq = MQ::JMS.new("aqv.sap.#{env}", params)
      # for SAP integration tests and special tests with msg forwarding, see aqvtest.rb
      @mqsap = MQ::JMS.new("aqv.sap.request.#{env}")
    end

    # receive all message on mq_request like SAPPM, assuming only 1 kits is handled at the same time
    #
    # return KitResult struct of the last message on success
    def receive_msg(params={})
      msg = @mq.receive_msgs({timeout: 1}.merge(params)).last || ($log.warn 'no SAP message received'; return)
      res = XMLHash.xml_to_hash(msg)
      header = res[:AQVqualificationMessage][:HEADER]
      ret = KitResult.new(header[:KitOrderNumber], header[:KitResult])
      if lines = res[:AQVqualificationMessage][:LINE]
        lines = [lines] unless lines.instance_of?(Array)
        ret.lines = lines.collect {|line| line[:Text]}
      end
      return ret
    end

    # send an AQV msg on the real queue to SAP, kitresult is one of STATUS (with multiple text lines), ACCEPT or REJECT
    def send_msg(order, kitresult, text, params={})
      $log.info "send_msg #{order.inspect}, #{kitresult.inspect}, #{text.inspect}"
      text = [text] if text.instance_of?(String)
      h = {AQVqualificationMessage: {
        HEADER: {KitOrderNumber: order, KitResult: kitresult},
        LINE: text.each_with_index.collect {|s, i| {Text: "[%4.4d] #{s}" % (i + 1)}}
      }}
      return params[:nosend] ? XMLHash.hash_to_xml(h) : @mqsap.send_msg(XMLHash.hash_to_xml(h))
    end

    # forward selected AQV messages from the QA test queue to the SAP AQV queue, return number of messages
    # TODO: currently not used, remove?
    def forward_sapmessages(orders=[])
      orders = [orders] unless orders.instance_of?(Array)
      orders = orders.collect {|o| o.to_i}
      re = /<KitOrderNumber>(.*)<\/KitOrderNumber>/
      count = 0
      @mq.read_msgs(raw: true).each {|msg|
        text = @mq.process_msg(msg)
        data = text.match(re)
        if data && (orders.empty? || orders.member?(data[1].to_i))
          $log.info "forwarding AQV message for order #{data[1]} to SAP"
          $log.info text
          count += 1
          @mqsap.send_msg(msg)
          @mq.receive_msg(nowait: true, raw: true, correlation: msg.jms_correlation_id)  # delete the msg
        end
      }
      return count
    end

  end


  # Send SPC event messages to AQV (like SIL)
  class SPC
    attr_accessor :mq

    def initialize(env, params={})
      @sv = params[:sv] || SiView::MM.new(env, params)
      @mq = MQ::JMS.new(params[:instance] || "aqv.spc.#{env}", params)   # for SIL tests a QA test instance is passed
    end

    def inspect
      "#<#{self.class.name}  #{@mq.inspect}>"
    end

    # send spc message for lot operation, routeop must have route, opNo and op data
    # either as SiView::MM:LotOperation struct or LotInfo or as returned from @mds.aqv_lot_oper
    #
    # return true on success
    def send_msg(lot, routeop, params={})
      ts = params.has_key?(:ts) ? params[:ts] : Time.now.utc.iso8601(3)
      wafers = params.has_key?(:wafers) ? params[:wafers] : @sv.lot_info(lot, wafers: true).wafers.collect {|w| w.wafer}
      result = params[:result] != false
      if params[:invalidres]
        resultstring = result ? 'suxxezz' : 'fehl'
      else
        resultstring = result ? 'success' : 'fail'
      end
      comment = params[:comment] || ''  # SIL sends at least an empty string
      $log.info "SPC aqv:event: #{lot.inspect} (#{routeop.route}, #{routeop.op}, #{routeop.opNo}), result: #{result}, comment: #{comment.inspect}"
      $log.info "  result string: #{resultstring.inspect}"
      h = {:'aqv:event'=>{
        'xmlns:aqv'=>'http://www.globalfoundries.com/jcap/spcEvent', 'xmlns:xsi'=>'http://www.w3.org/2001/XMLSchema-instance',
        :'aqv:lotId'=>lot, :'aqv:routeId'=>routeop.route, :'aqv:pdId'=>routeop.op, :'aqv:opeNo'=>routeop.opNo,
        :'aqv:result'=>resultstring, :'aqv:timestamp'=>ts
      }}
      h[:'aqv:event'].delete(:'aqv:opeNo') if routeop.opNo.nil?  # for VPD scenario
      h[:'aqv:event'][:'aqv:wafers'] = {:'aqv:scribe'=>wafers} if wafers
      h[:'aqv:event'][:'aqv:comment'] = comment
      return params[:nosend] ? XMLHash.hash_to_xml(h) : @mq.send_msg(XMLHash.hash_to_xml(h))
    end

  end


  # send Completion Notification, like the special EI
  class EI
    attr_accessor :mq

    def initialize(env, params={})
      @mq = MQ::JMS.new("aqv.cei.#{env}", params)
    end

    def send_msg(lot, params={})
      $log.info "EI VPDCompletionNotification: #{lot.inspect}"
      h = {:VPDCompletionNotification=>{'xmlns'=>'GLOBALFOUNDRIES/EI_to_jCAP/VPDCompletionNotification',
        LotID: lot, Timestamp: (params[:completiontime] || Time.now).utc.iso8601(3)
      }}
      return params[:nosend] ? XMLHash.hash_to_xml(h) : @mq.send_msg(XMLHash.hash_to_xml(h), params)  # send as text message
    end

  end

end
