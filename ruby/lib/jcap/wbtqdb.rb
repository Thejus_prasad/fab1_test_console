=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.
  
Author:     ssteidte, 2011-08-11

Version:    1.4.0

History:
  2012-04-30 ssteidte, use 'rqrsp' library
  2012-08-23 ssteidte, remove to_char conversion from queries for Date and Time fields
  2012-12-05 ssteidte, use new tables, separated MDS access code and test into wbtqdb.rb and wbtqtest.rb
  2013-12-10 ssteidte, added LEADINGORDER awareness and history lookup
  2015-02-24 ssteidte, more parameters to select contenxts for AQV
  2017-12-06 sfrieske, grouping support, merged with aqvdb.rb
  2020-07-29 sfrieske, removed grouping support
=end

require_relative 'sjc'


module WBTQ
  Context = Struct.new(:guid, :timestamp, :state, :eng, :autosel, :stbonly, :leadingorder, :order,
    :eqp, :chamber, :kit, :spec, :version, :srid, :errorstate)
  # for AQV
  AQVLot = Struct.new(:lot, :order, :leadingorder, :eqp, :product, :route, :create_time, 
    :update_time, :bankin_time, :split_flag, :status, :desc)
  AQVLotOper = Struct.new(:lot, :product, :route, :opNo, :op, :seq, :init_flag, :mandatory, :create_time, 
    :update_time, :category, :timestamp, :pass, :user, :spc_update_time, :spc_event_time, :spc_result, :spc_message)
  AQVPdSkippable = Struct.new(:spec, :version, :update_time, :route, :op, :skippable)
  
  # access WBTQ tables within the MDS  
  class MDS < SJC::MDS

    # return array of Context structs
    def contexts(params={})
      table = params[:history] ? 'T_WBTQ_CONTEXT_HIST' : 'T_WBTQ_CONTEXT'
      qargs = []
      q = "SELECT GUID, REQUEST_DATE, STATE, ENGINEERING_FLAG, AUTO_WAFER_SELECTION_FLAG,
        STB_ONLY_FLAG, SAP_LEADING_ORDER, SAP_ORDER_NO, EQUIPMENT_ID, CHAMBER_ID, KIT_ID, 
        SPEC_ID, VERSION_NO, SORT_REQUEST_ID, ERROR_STATE FROM #{table}"
      q, qargs = @db._q_restriction(q, qargs, 'GUID', params[:guid])
      q, qargs = @db._q_restriction(q, qargs, 'EQUIPMENT_ID', params[:eqp])
      # q, qargs = @db._q_restriction(q, qargs, 'SORT_REQUEST_ID', params[:srid])
      # q, qargs = @db._q_restriction(q, qargs, 'REQUEST_DATE >=', params[:tstart])
      # q, qargs = @db._q_restriction(q, qargs, 'REQUEST_DATE <=', params[:tend])
      q += ' ORDER BY PK_WBTQ_CONTEXT'
      res = @db.select(q, *qargs) || return
      return res.collect {|r|
        [12, 13].each {|i| r[i] = r[i].to_i if r[i]}
        Context.new(*r)
      }
    end

    # return array of carriers used within a given context
    def context_carriers(guid, params={})
      tcast, tctx = params[:history] ? ['T_WBTQ_CAST_HIST', 'T_WBTQ_CONTEXT_HIST'] : ['T_WBTQ_CAST', 'T_WBTQ_CONTEXT']
      q = "SELECT UNIQUE #{tcast}.CAST_ID FROM #{tcast}, #{tctx} 
        WHERE #{tcast}.FK_WBTQ_CONTEXT = #{tctx}.PK_WBTQ_CONTEXT AND #{tctx}.GUID = ?"
      res = @db.select(q, guid) || return
      return res.flatten.sort
    end

    # return array of context GUIDs that use a carrier, used in prepare_resources to clean up old kits
    def carrier_contexts(carrier, params={})
      tcast, tctx = params[:history] ? ['T_WBTQ_CAST_HIST', 'T_WBTQ_CONTEXT_HIST'] : ['T_WBTQ_CAST', 'T_WBTQ_CONTEXT']
      qargs = [carrier]
      q = "SELECT UNIQUE #{tctx}.GUID FROM #{tctx}, #{tcast} 
        WHERE #{tcast}.FK_WBTQ_CONTEXT = #{tctx}.PK_WBTQ_CONTEXT AND #{tcast}.CAST_ID = ?"
      q, qargs = @db._q_restriction(q, qargs, "#{tctx}.REQUEST_DATE >=", params[:tstart])
      res = @db.select(q, *qargs) || return
      return res.flatten.sort
    end
    
    # lots appear in AQV tables if they are in WBTQ tables, have a leading order and are STB'ed

    # return array of AQVLot structs
    def aqv_lot(params={})
      qargs = []
      q = 'SELECT LOT_ID, WBTQ_ORDER_NO, LEAD_ORDER_NO, QUAL_EQP_ID, PRODSPEC_ID, MAINPD_ID, CREATE_TIME, UPDATE_TIME,
        BANKIN_TIME, SPLIT_FLAG, STATUS_CODE, STATUS_DESC FROM T_AQV_LOT'
      q, qargs = @db._q_restriction(q, qargs, 'LOT_ID', params[:lot])
      q, qargs = @db._q_restriction(q, qargs, 'WBTQ_ORDER_NO', params[:order])
      q, qargs = @db._q_restriction(q, qargs, 'LEAD_ORDER_NO', params[:leadingorder])
      q, qargs = @db._q_restriction(q, qargs, 'QUAL_EQP_ID', params[:eqp])
      q, qargs = @db._q_restriction(q, qargs, 'UPDATE_TIME >=', params[:tstart])
      q, qargs = @db._q_restriction(q, qargs, 'STATUS_CODE', params[:status])
      q += ' ORDER BY CREATE_TIME, LOT_ID'
      res = @db.select(q, *qargs) || return
      return res.collect {|r| AQVLot.new(*r)}
    end
    
    # return array of AQVLotOper structs
    def aqv_lot_oper(params={})
      qargs = []
      q = 'SELECT LOT_ID, PRODSPEC_ID, MAINPD_ID, OPE_NO, PD_ID, SEQ_NO, INIT_FLAG, MANDATORY_FLAG, CREATE_TIME, UPDATE_TIME, 
        OPE_CATEGORY, OPE_FINISH_TIME, OPE_PASS_COUNT, OPE_USER_ID, SPC_UPDATE_TIME, SPC_EVENT_TIME, SPC_RESULT, SPC_MESSAGE
        FROM T_AQV_LOT_OPER'
      q, qargs = @db._q_restriction(q, qargs, 'LOT_ID', params[:lot])
      q, qargs = @db._q_restriction(q, qargs, 'OPE_NO', params[:opNo])
      q, qargs = @db._q_restriction(q, qargs, 'PD_ID', params[:op])
      q, qargs = @db._q_restriction(q, qargs, 'SPC_MESSAGE', params[:spc_message])
      q += ' ORDER BY LOT_ID, SEQ_NO'
      res = @db.select(q, *qargs) || return
      return res.collect {|r|
        r[5] = r[5].to_i
        r[12] = r[12].to_i
        AQVLotOper.new(*r)
      }
    end

    # data is filled by setup.FC at spec submit
    def aqv_skippable(params={})
      qargs = []
      q = 'select SPEC_ID, SPEC_VERSION, LAST_UPDATE, MAINPD_ID, PD_ID, SKIP_ALLOWED_FLAG from V_AQV_SPEC_PD_SKIP'
      q, qargs = @db._q_restriction(q, qargs, 'MAINPD_ID', params[:route])
      q, qargs = @db._q_restriction(q, qargs, 'PD_ID', params[:op])
      q, qargs = @db._q_restriction(q, qargs, 'SKIP_ALLOWED_FLAG', params[:skippable])
      q += ' ORDER BY MAINPD_ID, PD_ID'
      res = @db.select(q, *qargs) || return
      return res.collect {|r|
        r[1] = r[1].to_i
        AQVPdSkippable.new(*r)
      }
    end
  
  end

end
