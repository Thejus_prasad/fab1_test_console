=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author:   Steffen Steidten, 2013-11-18

Version:  1.2.1

History:
  2015-09-08 ssteidte, minor changes for API 1.1
  2016-01-15 sfrieske, adapted for v16.01
  2017-07-21 sfrieske, use verify_hash instead of verify_struct
  2017-11-20 sfrieske, use struct_to_hash instead of Struct.to_hash
  2019-01-24 sfrieske, minor cleanup
  2019-10-23 sfrieske, moved struct_to_hash to lmtest (only use case)
  2020-05-04 sfrieske, removed struct_to_hash, added support for LotsWithCategory (for AutoPilot)
  2021-02-05 sfrieske, use instance_of instead of kind_of
=end

# require 'fileutils'
# require 'time'
# require 'charts'
# require 'misc'
require_relative 'lmdb'
require_relative 'lmtrans'


module LotManager

# LotManager Transformer jCAP Job Tests
class Test
    attr_accessor :db, :transformer

    def initialize(env, params={})
      @db = params[:db] || ::LotManager::DB.new(env)
      @transformer = ::LotManager::Transformer.new(env)
    end

    def verify_lmrequest_data(params={})
      $log.info 'verify_lmrequest_data'
      data = params[:data] || @transformer.lmrequest_data
      data = [data] unless data.instance_of?(Array)   # must be an Array of LmRequest struct
      cols = ::LotManager::LmRequest.members.select {|e|
        ![:lots_jeopardy, :lots_pilot, :lots_candidate, :lots_withcategory, :rqcontext, :rqdynamic].member?(e)
      }
      ok = true
      data.each {|d|
        # compare LM_REQUEST columns
        rqrecord = @db.lmrequest(d.rqid).first || ($log.warn "no LM_REQUEST for #{d.rqid}"; return)
        cols.each {|col|
          v = rqrecord[col]
          ($log.warn "  wrong #{col.inspect}: #{v}, expected: #{d[col]}"; ok=false) if v != d[col]
        }
        # compare LM_REQUEST_LOT entries
        ref = {
          'CANDIDATE'=>d.lots_candidate, 'JEOPARDY'=>d.lots_jeopardy, 'PILOT'=>d.lots_pilot
        }.merge(d.lots_withcategory || {})
        lotcats = {}
        lotrecords = @db.lmrequest_lot(rqid: d.rqid)
        lotrecords.each {|lr|
          lotcats[lr.category] ||= []
          lotcats[lr.category] << lr.lot
        }
        ref.each_pair.each {|cat, reflots|
          v = lotcats[cat]
          ($log.warn "  wrong #{cat}: #{v}, expected: #{reflots}"; ok=false) if v != reflots
        }
        # compare LM_REQUEST_CONTEXT and LM_DYNAMIC_COUNT
        ctx = @db.lmrequest_context(d.rqid)
        ($log.warn "  wrong CONTEXT: #{ctx}, expected: #{d.rqcontext}"; ok=false) if ctx != d.rqcontext
        dyn = @db.lmrequest_dynamic(d.rqid)
        ($log.warn "  wrong DYNAMIC: #{dyn}, expected: #{d.rqdynamic}"; ok=false) if dyn != d.rqdynamic
      }
      return ok
    end

    def verify_lotsummary_data(params={})
      $log.info 'verify_lotsummary_data'
      data = params[:data] || @transformer.lotsummary_data
      data = [data] unless data.instance_of?(Array)   # must be an Array of LotSummary struct
      exclude = params[:exclude]
      ok = true
      data.each_with_index {|d, idx|
        r = @db.lotsummary(context: d.context).first || ($log.warn "no DB data for #{d.context}"; return)
        r.context.ctxid = nil   # LM_CONTEXT_ID does not matter
        r.pclbase[exclude] = d.pclbase[exclude] if exclude  # special tests
        ($log.warn "  wrong record ##{idx}"; ok=false) if r != d
      }
      return ok
    end

  end


  # # broken?
  # class Exerciser
  #   attr_accessor :env, :tstart, :tend, :lotsumthreads, :lmreqthreads

  #   def load_test(params={})
  #     lotsumthreads = (params[:lotsumthreads] or params[:n] or 1)
  #     lotreqthreads = (params[:lotreqthreads] or params[:m] or 10)
  #     @tstart = Time.now
  #     $log.info ""
  #     @lotsumthreads = @etcs.collect {|etc|
  #       Thread.new {
  #         Thread.current['name'] = "ETC_#{etc.eqp}"
  #         etc.report_loop(params)
  #       }
  #     }
  #     @lmreqthreads = (@hists +  @hists2).collect {|hist|
  #       Thread.new {
  #         Thread.current['name'] = "HWS_#{hist.name}"
  #         hist.cmp_etc_collections(params)
  #       }
  #     }
  #     @lotsumthreads.each {|t| t.join}
  #     $log.info "ETCs completed sending reports, waiting for HWSs to complete"
  #     sleep (params[:cmpwait] or 20) * 2
  #     @lmreqthreads.each {|t| ($log.warn "aborting #{t['name']}"; t.kill) if t.alive?}
  #     @tend = Time.now
  #     #
  #     # log results
  #     perflog = (params[:perflog] or "log/lotmanager_perf_#{Time.now.utc.iso8601.gsub(':', '-')}/delays.txt")
  #     ext = File.extname(perflog)
  #     dirname, fname = File.dirname(perflog), File.basename(perflog, ext)
  #     log_delays(:perflog=>"#{dirname}/#{fname}_1#{ext}")
  #     if @hists2 != []
  #       log_delays(:hists=>@hists2, :perflog=>"#{dirname}/#{fname}_2#{ext}")
  #       log_delays_diff(:difflog=>"#{dirname}/#{fname}_diff#{ext}")
  #     end
  #     # log summary
  #     s = summary
  #     $log.info("\n\n#{s}")
  #     File.write("#{dirname}/#{fname}_summary#{ext}", s)
  #     return s
  #   end

  #   def summary(params={})
  #     ret = "Retrieval Summary, #{tstart.utc.iso8601} .. #{@tend.utc.iso8601}\n"
  #     ret += "-" * 63 + "\n"
  #     ret += "test duration: #{duration_string(@tend - @tstart)}\n"
  #     ret += "\n\ntimeouts:\n"
  #     ok = true
  #     (@hists + @hists2).each_with_index {|hist, hi|
  #       ret += hist.name
  #       delays = hist.delays
  #       rtimes = @etcs[hi % @hists.size].report_times
  #       if delays.compact.size < rtimes.size
  #         ok = false
  #         (ret += ": failed (all)\n"; next) if delays.compact == []
  #         ret += ": failed\n"
  #         delays.each_with_index {|d, di| ret += "  #{rtimes[di].utc.iso8601}\n" unless d}
  #       else
  #         ret += ": passed\n"
  #       end
  #     }
  #     ret += ok ? "\ntimeout test passed\n" : "\ntimeout test failed\n"
  #     if @hists2 != []
  #       hist2diff = (params[:hist2diff] or 15)   # percent
  #       ret += "\n\ninstance response times differing > #{hist2diff}%:\n"
  #       @hists.each_with_index {|hist, hi|
  #         ret += hist.name + "\n"
  #         delays = hist.delays
  #         delays2 = @hists2[hi].delays
  #         rtimes = @etcs[hi].report_times
  #         delays.each_with_index {|d, di|
  #           d2 = delays2[di]
  #           next unless d and d2
  #           ret += "  #{rtimes[di].utc.iso8601}\n" if ((d2 - d)/(d2 + d)/2.0*100).abs > hist2diff
  #         }
  #       }
  #     end
  #     return ret
  #   end

  #   def log_delays(params={})
  #     fname = (params[:perflog] or "log/lotmanager_perf_#{Time.now.utc.iso8601.gsub(':', '-')}/delays.txt")
  #     FileUtils.mkdir_p(File.dirname(fname))
  #     tt = @etcs[0].report_times.collect {|t| t.utc.iso8601}
  #     open(fname, "wb") {|fh|
  #       fh.write "Time #{tt.join(' ')}\n"
  #       hists = (params[:hists] or @hists)
  #       hists.each {|hist|
  #         dd = hist.delays.collect {|d| d.nil? ? -1 : d.round(1)}
  #         fh.write "#{hist.name} #{dd.join(' ')}\n"
  #       }
  #     }
  #     LotManager.performance_charts(fname) unless params[:nocharts]
  #   end

  #   def log_delays_diff(params={})
  #     fname = (params[:difflog] or "log/lotmanager_perf_#{Time.now.utc.iso8601.gsub(':', '-')}/diff.txt")
  #     FileUtils.mkdir_p(File.dirname(fname))
  #     tt = @etcs[0].report_times.collect {|t| t.utc.iso8601}
  #     open(fname, "wb") {|fh|
  #       fh.write "Time #{tt.join(' ')}\n"
  #       @hists.each_with_index {|hist, hi|
  #         dd = []
  #         delays2 = @hists2[hi].delays
  #         hist.delays.each_with_index {|d, di|
  #           if d.nil?
  #             dd << -49
  #           elsif delays2[di].nil?
  #             dd << 49
  #           else
  #             dd << (delays2[di] - d).round(1)
  #           end
  #         }
  #         fh.write "#{hist.name} #{dd.join(' ')}\n"
  #       }
  #     }
  #     LotManager.performance_charts(fname, :vname=>"delta (s)") unless params[:nocharts]
  #   end
  # end

  # # create charts from a performance log file, broken?
  # def self.performance_charts(perflog, params={})
  #   $log.info "LotManager.performance_charts #{perflog.inspect}"
  #   ext = File.extname(perflog)
  #   dirname, fname = File.dirname(perflog), File.basename(perflog, ext)
  #   lines = File.readlines(perflog)
  #   return nil unless lines and lines.size > 1
  #   # first column: caption, other columns: values (times or durations per tool)
  #   times = lines[0].split[1..-1].collect {|t| Time.parse(t)}
  #   $tooldata = tooldata = {}
  #   lines[1..-1].each {|l|
  #     ff = l.split
  #     tool = ff.shift
  #     ff.collect! {|f| f.to_f}
  #     tooldata[tool] = ff
  #   }
  #   vname = (params[:vname] or "delay(s)")
  #   ch = Chart::Time.new(nil)
  #   tooldata.each_pair {|tool, tdata| ch.add_series(times.zip(tdata))}
  #   ch.create_chart(:title=>File.basename(fname), :y=>vname, :dots=>true, :export=>"#{dirname}/#{fname}_chart.png")
  #   ##ch.create_chart(:title=>File.basename(fname), :y=>vname, :ymin=>0.01, :ymax=>100, :dots=>true, :lg=>true, :export=>"#{dirname}/#{fname}_chartlg.png")
  #   chdata = []
  #   tooldata.each_pair {|tool, tdata| chdata += tdata}
  #   ch = Chart::Histogram.new(chdata)
  #   ch.create_chart(:title=>File.basename(fname), :bins=>50, :x=>vname, :export=>"#{dirname}/#{fname}_hist.png")
  # end

end
