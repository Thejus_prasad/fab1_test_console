=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author:  ssteidte, 2012-09-27

Version: 2.2.2

History:
  2016-08-04 sfrieske, split off of asmviewtest to remove dependency on AsmView::JCAP
  2016-11-08 sfrieske, in wait_move check lot script parameter ProcState directly
  2017-04-19 sfrieske, BSM support
  2018-03-13 sfrieske, removed use of lot_set_tkinfo, lot is already prepared before start_btf is called
  2019-03-29 sfrieske, added av_gid and get_avlot
  2019-05-06 sfrieske, separated Turnkey::BSMTest from Turnkey::BTFTest
  2021-08-27 sfrieske, require waitfor instead of misc
=end

require 'asmview'
require 'util/waitfor'


module Turnkey

  # lot start for BSM scenarios (re-STB on BUMP and SORT layers), currently UNUSED
  class BSMTest
    attr_accessor :sv, :sm, :av, :jobdelay, :bsm_routes

    def initialize(env, params={})
      @sv = params[:sv] || SiView::MM.new(env)
      @sm = params[:sm] || SiView::SM.new(env)
      @av = params[:av] || AsmView::MM.new(params[:av_env] || 'erpstage')
      @jobdelay = params[:jobdelay] || 410  # hard coded in MDS + BTFTurnkey job
      @bsm_routes = {'AH'=>'BSM-BUMP-A.1', 'AJ'=>'BSM-SORT-A.1'}
    end

      # start BSM processing (lot is typically just STB'ed on the previous layer), return true on success
    def start_bsm(lot, tkparams, params={})
      $log.info "start_bsm #{lot.inspect}, #{params}"
      # complete Fab lot if necessary, move it to the start bank and set TK info
      @sv.lot_opelocate(lot, :last) if @sv.lot_info(lot).status != 'COMPLETED'
      oinfo = @sm.object_info(:mainpd, params[:route]).first
      res = @sv.lot_bank_move(lot, oinfo.specific[:startbank])
      ($log.warn "error moving lot #{lot} to start bank"; return) if res != 0
      # set script parameters before STB (as in FabGUI), additional step to make BtfTurnkey happy
      @sv.user_parameter_change('Lot', lot, tkparams).zero? || ($log.warn 'error setting TK parameters'; return)
      @sv.user_parameter_change('Lot', lot, 'ERPItem', params[:erpitem]).zero? || ($log.warn 'error setting erpitem'; return)
      # STB used lot including parameters for correct comment (as usual, else 'wrong' TK params are set)
      comment = "ERPITEM=#{params[:erpitem]},DPML=1.5,PLANNED_FSD=#{Time.now.strftime('%F')}"
      relparams = params.merge(lot: lot, srclot: lot, lotGenerationType: 'By Source Lot', comment: comment)
      @sv.new_lot_release(relparams) || ($log.warn 'error scheduling lot'; return)
      @sv.stb(lot) || ($log.warn 'error STBing lot'; return)
      sleep 60 # else CustomerLotID is not set at all
      # process or gatepass lot, else BtfTurnkey cannot handle it
      res = @sv.lot_gatepass(lot, mandatory: true)
      ($log.warn "error gatepassing lot #{lot}"; return) if res != 0
      $log.info "waiting up to #{@jobdelay} s for the lot to appear in AsmView"
      avlot = get_avlot(lot)
      wait_for(timeout: @jobdelay) {@av.lot_exists?(avlot)} || ($log.warn "lot is not in AsmView"; return)
      # wait for branch on TK route, for AP (BUMP+SORT) the route is first the AH (BUMP), after re-STB to sort the AJ route
      tktype = tkparams['TurnkeyType']
      _t = tktype
      _t = oinfo.specific[:layer].start_with?('BU') ? 'AH' : 'AJ' if tktype == 'AP'
      avroute = @bsm_routes[_t] || ($log.warn "no AsmView route for TK type #{tktype.inspect}"; return)
      wait_for(timeout: 120) {
        @av.lot_info(avlot).route == avroute
      } || ($log.warn "lot is not on route #{avroute} in AsmView"; return)
      # verify AsmView lot, wait long enough to allow AsmTurnkey to set the script parameters
      $log.info 'verifying lot script parameters'
      svparams = @sv.user_parameter('Lot', lot)
      avparams = nil
      svlabel = @sv.lot_label(lot)
      avlabel = nil
      ok = wait_for(timeout: 120) {
        # svparams = @sv.user_parameter('Lot', lot)
        avparams = @av.user_parameter('Lot', avlot)
        avp = avparams.find {|e| e.name == 'CustomerLotID'}
        avlabel = avp ? avp.value : nil
        %w(ERPItem TurnkeyType TurnkeySubconIDBump TurnkeySubconIDSort
          TurnkeySubconIDReflow TurnkeySubconIDAssembly TurnkeySubconIDFinalTest).inject(true) {|name|
          svp = svparams.find {|e| e.name == name}
          avp = avparams.find {|e| e.name == name}
          (avp && avp.value) == (svp && svp.value) && (avlabel == svlabel)
        }
      }
      ($log.warn 'wrong lot script parameters'; return) unless ok
      $log.info 'verifying wafer script parameters'
      svwafers = @sv.lot_info(lot, wafers: true).wafers
      avwafers = @av.lot_info(avlot, wafers: true).wafers
      svwafers.each {|w|
        fwid = @sv.user_parameter('Wafer', w.wafer, name: 'CustomerWaferID').value
        awid = @av.user_parameter('Wafer', w.wafer, name: 'CustWaferId').value
        ok = false if awid != fwid
      }
      ($log.warn '(av) wrong CustWaferId'; return) unless ok
      return true
    end

    # BSM processing, lot is already on the branch route
    def process_bsm(lot, params={})
      avlot = get_avlot(lot)
      $log.info "process_bsm #{lot.inspect}#{', ' + params.inspect unless params.empty?}"
      # get SM route information with CustomerStepID UDATA
      svops = @sm.object_info(:mainpd, @sv.lot_info(lot).route, details: true).first.specific[:operations]
      # get list of stages on the AsmView branch route
      avops = @av.lot_operation_list(avlot)
      steps = avops.collect {|e| e.op if e.route == avops.first.route}.compact
      steps.each_with_index {|avop, idx|
        step = avop.split('.').first
        break if step == params[:stop_before]
        $log.info "-- btfturnkey move to customer step ##{idx + 1}/#{steps.size}: #{step}"
        svop = svops.find {|e| e.udata['CustomerStepID'].include?(";#{step}")}
        ($log.warn "(sv) no route operation with CustomerStepID #{step}"; return) unless svop
        # move lot in SiView
        if idx > 0
          res = @sv.lot_opelocate(lot, svop.opNo)
          ($log.warn '(sv) error locating lot'; return) if res != 0
        end
        # process lot in SiView if step (a.k.a. op) is mandatory in AsmView
        if avops[idx].mandatory && !svop.op.start_with?('LOTSHIP')
          @sv.claim_process_lot(lot) || ($log.warn '(sv) error processing lot'; return)
        end
        # wait for AsmView changes
        $log.info "waiting up to #{@jobdelay} s for lot #{lot} to move to #{avop} in AsmView"
        wait_for(timeout: @jobdelay) {
          @av.lot_info(avlot).op == avop
        } || ($log.warn "(av) lot is not at op #{avop}"; return)
        if avops[idx].mandatory && !svop.op.start_with?('LOTSHIP')
          $log.info "waiting up to #{@jobdelay} s for lot #{lot} to become Processing in AsmView"
          wait_for(timeout: @jobdelay) {
            @av.user_parameter('Lot', avlot, name: 'ProcState').value == 'Processing'
          } || ($log.warn '(av) lot status is not Processing'; return)
        end
      }
      #
      $log.info "processed lot #{lot}, current operation: (sv) #{@sv.lot_info(lot).op}, (av) #{@av.lot_info(avlot).op}"
      return true
    end

  end

end
