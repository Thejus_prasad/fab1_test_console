=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.


Author:   Steffen Frieske, 2016-10-27

Version:  1.0.2

History:
  2017-01-02 sfrieske, more detailed information on job run errors
  2021-08-27 sfrieske, require waitfor instead of misc
=end

ENV.delete('http_proxy')

require 'net/http/digest_auth'
require 'nokogiri'
require 'rqrsp_http'
require 'util/waitfor'


module APF

  # Web Client for APF Activity Manager Jobs
  class AM < RequestResponse::Http
    attr_accessor :env, :digest_auth

    def initialize(env, params={})
      super("apfam.#{env}", params)
      @env = env
      @authorization = nil
      @digest_auth = Net::HTTP::DigestAuth.new
    end

    def _send_receive
      res = super
      return res if res || @response.code != '401'
      @request.uri.user = @user
      @request.uri.password = @params[:cleartext] ? @params[:password] : Base64.decode64(@params[:password])
      auth = @digest_auth.auth_header(@request.uri, @response['www-authenticate'], @request.method)
      @request['Authorization'] = auth
      @request.uri.user = @request.uri.password = nil # do not send http://<user>:<password>@....
      return super
    end

    # return job details by handle (e.g. Job-927)
    def job_details(handle)
      res = get(resource: ['/job/details', handle]) || return
      ($log.warn "#{handle} is not a job handle"; return) if res.include?('is not a job')
      doc = Nokogiri::HTML(res)
      ee = doc.xpath('//td')
      return {name: ee[14].children[0].text, folder: ee[12].children[0].text,
        handle: ee[16].children[0].text, enabled: ee[18].children[0].text,
        current: ee[20].children[0].text, total: ee[22].children[0].text,
        errors: ee[24].children[0].text}
    end

    # return job details by handle (e.g. Job-927)
    def job_execution_details(handle)
      res = get(resource: ['/execution/job_details', handle]) || return
      doc = Nokogiri::HTML(res)
      row = doc.xpath('//tr').last
      error = row.children[4].children[0].text == 'No' ? nil : row.children[4].children[0].attributes['href'].value
      if error
        res2 = get(resource: error) || return
        doc2 = Nokogiri::HTML(res2)
        ee = doc2.xpath('//td')
        error = {code: ee[22].children[0].text, message: ee[24].children[0].text}
      end
      return {execid: row.children[0].children[0].text,
        # tstart: Time.strptime(row.children[1].children[0].text + ' UTC', '%m/%d/%Y %H:%M:%S %z'),
        # tend: Time.strptime(row.children[2].children[0].text + ' UTC', '%m/%d/%Y %H:%M:%S %z'),
        duration: row.children[3].children[0].text, error: error}
    end

    # start a job by name (e.g. '/QUEUE_TIME/ie_qt_gate_controller_gecko'), return true on success
    def manual_start(name, params={})
      $log.info "manual_start #{name.inspect}, #{params}"
      action = params[:action] || 'ManualStart'    # _ManualStart_Complete, ManualStart_RemoveLock, Remove_TouchFile
      res = get(resource: ['/manual_start', URI.encode_www_form_component(name), action]) || return
      doc = Nokogiri::HTML(res)
      text = doc.xpath('//pre').first.children.first.text
      ($log.warn "error starting job: #{text}"; return) unless text.include?('Job started')
      return true
    end

    # wait for job completion, start it and wait for completion, return true on success
    def wait_run_job(handle, params={})
      $log.info "wait_run_job #{handle.inspect}"
      stat0 = nil
      # wait if running, timeout is configured in AM
      wait_for(timeout: 600) {
        stat0 = job_details(handle)
        stat0[:current] == '0'
      } || ($log.warn "(old) job is still running, timeout"; return)
      # start job
      name = File.join(stat0[:folder], stat0[:name])
      manual_start(name, action: params[:action]) || return
      # wait
      stat1 = nil
      wait_for(timeout: 600) {
        stat1 = job_details(handle)
        stat1[:current] == '0'
      } || ($log.warn "(new) job is still running, timeout"; return)
      # verify successful completion
      ret = stat1[:errors] == stat0[:errors] && stat1[:total] == stat0[:total].next
      $log.warn "job error #{stat1}\n#{job_execution_details(handle)}" unless ret
      return ret
    end

    # start multiple jobs and wait for completion, return true on success
    def wait_run_jobs(handles, params={})
      $i = instances = {}
      $r = res = {}
      ths = handles.collect {|handle|
        Thread.new {
          Thread.current['name'] = handle
          instances[handle] = AM.new(@env)
          res[handle] = instances[handle].wait_run_job(handle, action: params[:action])
        }
      }
      ths.each {|t| t.join(600)}
      return res
    end

  end

end
