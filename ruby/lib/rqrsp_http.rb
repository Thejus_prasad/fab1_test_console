=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author:  sfrieske, 2016-08-19
Version: 1.0.4

History:
  2016-08-19 sfrieske, re-write of rqrsp.rb to reduce overhead
  2016-10-18 sfrieske, removed default query args
  2017-02-21 sfrieske, log cookie if it is automatically set
  2018-09-24 sfrieske, HttpXML#post calls hash_to_xml if body is not a string
  2021-02-05 sfrieske, use instance_of instead of kind_of
  2021-08-26 sfrieske, use read_config instead of _config_from_file
=end

require 'base64'
require 'net/http'
require 'rexml/document'
require 'util/readconfig'
require 'util/xmlhash'


module RequestResponse

  # Basic communication for XML, SOAP and JSON over HTTP
  class Http
    attr_accessor :instance, :params, :request, :response, :uri, :http, :proxy,
      :user, :authorization, :cookie, :content_type, :keepalive, :nosend, :duration

    def initialize(instance, params={})
      @instance = instance
      @params = read_config(instance, 'etc/urls.conf' || params[:config], params)
      url = @params[:url]
      url = File.join(url, @params[:resource]) if @params.has_key?(:resource)
      @uri = URI.parse(url)
      @uri.host = params[:host] if params[:host]  # override host part of url
      @uri.port = params[:port] if params[:port]  # override port part of url
      @content_type = @params[:content_type] || 'application/x-www-form-urlencoded'
      @user = @params[:user]
      password = @params[:password]
      if @user && password && (@params[:basic_auth] != false)
        pw = !!@params[:cleartext] ? password : Base64.decode64(password)
        @authorization = 'Basic ' + Base64.strict_encode64([@user, pw].join(':'))
      end
      @authorization = @params[:authorization] if @params.has_key?(:authorization) # overrides basic auth
      proxy_host = @params[:proxy_host]
      proxy_port = @params[:proxy_port]
      if proxy_host
        @proxy = Net::HTTP.Proxy(proxy_host, proxy_port)
        @http = @proxy.new(@uri.host, @uri.port)
      else
        @http = Net::HTTP.new(@uri.host, @uri.port)
      end
      @http.read_timeout = @params[:timeout] || 300
      @keepalive = params[:keepalive]
      @nosend = false
      $log.info self.inspect
    end

    def inspect
      "#<#{self.class.name} #{@instance} #{@uri.to_s}#{' user: ' + @user if @user}>"
    end

    # return new uri based on instance variables and params
    def _uri(params={})
      uri = @uri.clone
      uri.path = File.join(uri.path, params[:resource]) if params.has_key?(:resource)
      if args = params[:args]
        uri.query = URI.encode_www_form(args)
      end
      return uri
    end

    # send the finalized request, get the response and extract cookies, return true on success
    def _send_receive
      @response = @duration = nil
      @request['Cookie'] = @cookie if @cookie
      @request['Connection'] = 'keep-alive' if @keepalive
      @request['Authorization'] = @authorization if @authorization
      ##@request['Accept-Encoding'] = '' # no gzip to circumvent issues with setupfc in JRUBY > 1.7.15 ???
      File.binwrite(params[:export], @request.body) if params.has_key?(:export)
      begin
        @duration = nil
        tstart = Time.now
        ##@http = @proxy ? @proxy.new(@uri.host, @uri.port) : Net::HTTP.new(@uri.host, @uri.port)
        ##@http.read_timeout = 600
        @response = @http.request(@request) || ($log.warn "error sending request"; return)
        @duration = Time.now - tstart
      rescue
        $log.warn "HTTP error #{$!.inspect}"
        return
      end
      if ['200', '204', '302'].member?(@response.code)
        c = @response.header['set-cookie']
        if c
          $log.debug {"setting cookie #{c.inspect}"}
          @cookie = c.split(',').collect {|e| e.split(';').first.strip}.join('; ')
        end
        return true
      elsif ['401'].member?(@response.code)
        $log.debug {"response: #{@response.code} #{@response.message}: need to authenticate"}
        return
      else
        $log.warn "response: #{@response.code} #{@response.message}\n#{response.body}"
        return
      end
    end

    # GET request, return response body or nil
    def get(params={})
      @request = Net::HTTP::Get.new(_uri(params))
      return @request if @nosend
      _send_receive || return
      return @response.body
    end

    # POST request, return response body or nil
    def post(body, params={})
      @request = Net::HTTP::Post.new(_uri(params))
      @request['SOAPAction'] = params[:soapaction] if params.has_key?(:soapaction)
      @request['payload'] = params[:payload] if params.has_key?(:payload)  # for WARP
      @request.content_type = params[:content_type] || @content_type
      @request.body = body.instance_of?(String) ? body : URI.encode_www_form(body)
      return @request if @nosend
      _send_receive || return
      return @response.body
    end

    # PUT request, return true on success
    def put(body, params={})
      @request = Net::HTTP::Put.new(_uri(params))
      @request.content_type = @content_type
      @request.body = body.instance_of?(String) ? body : URI.encode_www_form(body)
      return @request if @nosend
      return _send_receive
    end

    # DELETE request, return true on success
    def delete(params={})
      @request = Net::HTTP::Delete.new(_uri(params))
      return @request if @nosend
      return _send_receive
    end
  end


  # Send and receive XML data
  class HttpXML < Http
    attr_accessor :bodytag, :service_response

    def initialize(instance, params={})
      super(instance, {content_type: 'text/xml;charset="utf-8"'}.merge(params))
      @bodytag = params.has_key?(:bodytag) ? params[:bodytag] : '*//*Body'
    end

    # GET request, return Hash of XML response
    def get(params={})
      res = super(params) || return
      return res if @nosend || res.empty? || params[:raw]
      return @service_response = XMLHash.xml_to_hash(res, params[:bodytag] || @bodytag)
    end

    # POST request, return Hash of XML response
    def post(data, params={})
      if data.instance_of?(String)
        s = data
      else
        s = String.new
        data.write(s)
      end
      res = super(s, params) || return
      return res if @nosend || res.empty? || params[:raw]
      return @service_response = XMLHash.xml_to_hash(res, params[:bodytag] || @bodytag)
    end
  end


  # Send and receive JSON data
  class HttpJSON < Http
    require 'json'

    def initialize(instance, params={})
      super(instance, {content_type: 'application/json'}.merge(params))
    end

    # GET request, return empty string, JSON object or nil on error
    def get(params={})
      res = super(params) || return
      return res if res.empty? || params[:raw]
      # convert JSON response to hash
      begin
        return JSON.parse(res)
      rescue
        $log.warn "JSON::ParserError: #{$!}"
        return
      end
    end

    # POST request, return empty string, JSON object or nil on error
    def post(body, params={})
      res = super(body.instance_of?(String) ? body : JSON.generate(body), params) || return
      return res if res.empty? || params[:raw]
      # convert JSON response to hash
      begin
        return JSON.parse(res)
      rescue
        $log.warn "JSON::ParserError: #{$!}"
        return
      end
    end

    # PUT request, return true on success
    def put(body, params={})
      return super(body.instance_of?(String) ? body : JSON.generate(body), params)
    end
  end

end
