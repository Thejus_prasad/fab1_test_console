=begin
(c) 2009 GLOBALFOUNDRIES Inc. All Rights Reserved.

Author:   Steffen Steidten, 2012-02-15
Version:  1.2.5

History:
  2014-11-17 ssteidte, cleanup, use dummytools.conf
  2015-10-15 sfrieske, don't log 'get' commands, threaded execution for multiple instances (LET)
  2019-07-30 sfrieske, use String.new to create new empty strings
  2021-02-05 sfrieske, use instance_of instead of kind_of
  2021-08-26 sfrieske, split off EINotification and moved it to attic
  2021-08-26 sfrieske, use read_config instead of _config_from_file
=end

require 'socket'
require 'thread'
require 'util/readconfig'


# Control DummyAMHS and JavaEIs
module Dummy

  # Generic TCP socket commander interface
  class Commander
    attr_accessor :instance, :host, :port, :_response


    def initialize(instance, overrides={})
      params = read_config(instance, 'etc/dummytools.conf', overrides)
      if params[:connections]   # for multiple DummyAmhs instances (e.g. LET)
        @host = []
        @port = []
        params[:connections].split.each {|e|
          h, p = e.split(':')
          @host << h
          @port << p.to_i
        }
      else
        @host = params[:host]
        @port = params[:port]
      end
    end

    def inspect
      "#<#{self.class.name} #{@env}, #{@host}:#{@port}>"
    end

    def _send_recv(host, port, cmd, timeout=60)
      # send command
      cmd += "\n" unless cmd.end_with?("\n")
      tcpconn = TCPSocket.new(host, port)
      tcpconn.readline  # read 'OK'
      tcpconn.puts(cmd)
      # receive response
      response = String.new
      Thread.new {
        loop {
          break if response.end_with?("OK\r\n")
          response << tcpconn.recv(64000)
        }
      }.join(timeout)
      tcpconn.close
      $log.debug {"  _recv: #{response.inspect}"}
      @_response = response  # for debugging only
      return response[-4..-1].index('OK') ? response.split("\r\n") : nil
    end

    def cmd(s)
      $log.info "DummyTool #{s.inspect}" unless s.start_with?('get', 'status', 'vars', 'globalvars')
      if @host.instance_of?(Array)
        # multiple instances
        ret = {}
        @host.each_with_index.collect {|h, i|
          Thread.new {
            res = _send_recv(h, @port[i], s)
            ($log.warn "  comm error"; ret[i] = []) unless res && res.last == 'OK'
            ret[i] = res
          }
        }.collect {|t| t.join(30); t}.each {|t| t.kill}
        return Hash[ret.to_a.sort]
      else
        # regular case
        res = _send_recv(@host, @port, s)
        ($log.warn "  comm error"; return []) unless res && res.last == 'OK'
        return res
      end
    end
  end


  # Control DummyAMHS, MultiInstances
  class AMHS < Commander
    attr_accessor :env, :dummyamhs

    def initialize(env, params={})
      @env = env
      super("dummyamhs.#{env}")
    end

    def register
      cmd('unregister')
      res = cmd('register')
      if res.instance_of?(Hash)
        ok = true
        res.each_pair {|i, ires|
          iok = ires[0].start_with?('dummyamhs registered as')
          $log.info "  #{i}: #{iok}"
          ok &= iok
        }
        return ok
      else
        $log.info res[0]
        return res[0].start_with?('dummyamhs registered as')
      end
    end

    # get the value for a specific tool variable
    def variable(name)
      res = cmd("get #{name}")
      if res.instance_of?(Hash)
        res.each_pair.collect {|i, ires| (ires.size < 2) ? nil : ires[-2].split('=', 2).last}
      else
        (res.size < 2) ? nil : res[-2].split('=', 2).last
      end
    end

    # set value of a variable for a specific tool or globally when eqp is nil, with verification
    def set_variable(name, value)
      res = cmd("set #{name} #{value}")
      if res.instance_of?(Hash)
        ok = true
        vv = variable(name)
        res.each_pair {|i, ires|
          iok = (ires.size == 2) && vv[i] && vv[i] == value.to_s
          $log.info "  #{i}: #{iok}"
          ok &= iok
        }
        return ok
      else
        (res.size < 2) ? nil : (variable(name) == value.to_s)
      end
    end

    def status
      cmd('status')
    end

    def queue
      res = status
      if res.instance_of?(Hash)
        ret = {}
        res.each {|k, v|
          ret[k] = nil
          v.each {|l| ret[k] = l.split[1].to_i if l.start_with?('jobs')}
        }
        return ret
      else
        res.find {|l| l.start_with?('jobs')}.split[1].to_i
      end
    end

  end


  # Control JavaEIs
  class EIs < Commander
    attr_accessor :env, :host, :port

    def initialize(env, params={})
      @env = env
      super("eis.#{env}")
    end

    def tools
      res = cmd("tools")
      return false if res.size < 2
      res[-2].gsub('[', '').gsub(']', '').gsub(',', ' ').split
    end

    def start_tool(eqp)
      res = cmd("starttool #{eqp}")
      return false if res.size < 2
      return res[-2].include?('successfully')
    end

    def stop_tool(eqp)
      res = cmd("stoptool #{eqp}")
      return false if res.size < 2
      return res[-2].include?('was stopped')
    end

    def restart_tool(eqp)
      stop_tool(eqp)
      start_tool(eqp)
    end

    def carrier_arrived(eqp, port, carrier)
      res = cmd("carrierarrived #{eqp} #{carrier} #{port}")
      return if res.size < 2
      return (res.first == 'carrier arrival done.')
    end

    def carrier_departed(eqp, port, carrier)
      res = cmd("carrierdeparted #{eqp} #{carrier} #{port}")
      return if res.size < 2
      return (res.first == 'carrier departure done.')
    end


    # return proctime in seconds
    def proctime(eqp)
      res = cmd("proctime #{eqp}")
      return if res.size < 2
      res[0].split('=')[1].tr('ms', '').to_i / 1000.0
    end

    # set proctime (in seconds)
    def set_proctime(eqp, t)
      res = cmd("proctime #{eqp} #{(t * 1000).to_i}")
      return (res.size == 2)
    end

    # get tool variables, global defaults if eqp is nil
    def variables(eqp=nil)
      res = eqp ? cmd("vars #{eqp}") : cmd('globalvars')
      return if res.size < 2
      return Hash[res[0..-2].collect {|e|
        k, v = e.split('=')
        [k, v.split.first]
      }.sort]
    end

    # get the value for a specific tool variable
    def variable(eqp, name)
      res = variables(eqp) || return
      return res[name]
    end

    # set value of a variable for a specific tool or globally when eqp is nil, with verification
    def set_variable(eqp, name, value)
      res = eqp ? cmd("set #{eqp} #{name} #{value}") : cmd("setglobal #{name} #{value}")
      return if res.size < 2
      # verify
      return (variable(eqp, name) == value.to_s)
    end

    def e10state(eqp, chamber=nil)
      res = chamber ? cmd("e10state #{eqp} #{chamber}") : cmd("e10state #{eqp}")
      return if res.size < 2
      return res.first
    end

    def set_e10state_verify(eqp, state, value, chamber=nil)
      res = chamber ? cmd("set_e10state #{eqp} #{chamber} #{value}") : cmd("set_e10state #{eqp} #{value}")
      return if res.size < 2
      ##return nil if res[-1] != 'OK'
      v = e10state(eqp, chamber)
      return (!state.nil? and (state = v))
    end

    def portstate(eqp, port)
      res = cmd("portstate #{eqp} #{port}")
      return if res.size < 2
      return res.first
    end

    def set_portstate_verify(eqp, state, value, port)
      res = cmd("set_portstate #{eqp} #{port} #{value}")
      return if res.size < 2
      #return nil if res.last != 'OK'
      v = portstate(eqp, port)
      return (!state.nil? && state == v)
    end

    # Set chambers used by an control job
    #  an empty list of chambers is not supported
    def set_cj_chambers(eqp, cj=nil, chambers=[])
      chambers = [chambers] unless chambers.instance_of?(Array)
      res = cj ? cmd("set_cj_chambers #{eqp} #{cj} #{chambers.join(' ')}") : cmd("set_cj_chambers #{eqp}")
      return !res.empty?
    end

    def reset_forcecomp_jobs(eqp)
      res = cmd("reset_forcecomp_jobs #{eqp}")
      return !res.empty?
    end

    def forcecomp_jobs(eqp)
      res = cmd("forcecomp_jobs #{eqp}")
      return false if res.size < 2
      res[-2].gsub('[', '').gsub(']', '').gsub(',', ' ').split
    end
  end

end
