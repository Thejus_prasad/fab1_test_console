=begin
  APC Testing
  
@author:     ssteidte, 2011-01-20
@version:    0.9.1
  
=end

require 'rqrsp'
require 'builder'
require 'misc'
require 'siview'

# APC Testing
module APC
  class Run2Run < RequestResponse::HTTP
  
    def initialize(env, params={})
      @env = env
      super("catalyst.#{env}", params)
    end
    
    def read_file(filename)
      open(filename, "rb") {|fh|
        if filename.end_with?('gz')
          Zlib::GzipReader.new(fh).read
        else
          fh.read
        end
      }
    end

    def submit_job(filename, params={})
      s = read_file(filename)
      #return URI::decode(s)
      #req = {"controlData"=>self.hash_to_xml(URI::decode(s))}
      req = "controlData=#{s}"
      $log.debug "#{req.inspect}"
      ret = _send_receive(req, params.merge(:content_type=>'application/x-www-form-urlencoded', :resource=>"submitjob", :rawxml=>true))
      return ret
    end
         
    def _process(data)
      if data.kind_of?(Hash)
        h = {}
        data.each_pair {|k,v|
          if [:data,:item, :dataEntry].member?(k)
            # open new Array
            v = v[:dataEntry] if k == :data
            # collect name / values
            if v.kind_of?(Array)
              return Hash[*[v.collect {|i|
                _val = i.has_key?(:data) ? _process(i[:data]) : i[:value]
                [i[:name], _val]
              }]]
            else
              $log.error " unexpected data #{v.inspect}"
              nil
            end
          else
            h[k] = _process(v)
          end
        }
        h
      else
        data
      end
    end
    
    def compare_result(hash, filename)
      s = read_file(filename)
      exp = xml_to_hash(s)
      # strip of hostname specifics
      return verify_hash(_process(exp), _process(hash), "result doesn't match #{filename}", ["ApcHostname",:serverInfo])
    end
    
    def send_compare(filename, params={})
      $log.info("#{__method__} #{filename} #{params.inspect}")
      rply_file = filename.sub(/req/, "rly") # expecting req / rly file names
      if File.exist?(rply_file)
        res = submit_job(filename, params)
        return compare_result(xml_to_hash(res), rply_file)
      else
        $log.error("reply file doesn't exist.")
        return false
      end
    end
    
    # Load APC request from file or all requests in directory
    # Compare APC result to "rly" file
    def load_test(dirname, params={})
      ok = {}
      if File.file?(dirname)        
        ok[dirname] = send_compare(dirname, params)
      else
        Dir["#{dirname}/**/*req*"].each { |filename|
          if File.file?(filename)            
            ok[filename] = send_compare(filename, params)
          end
        }
      end
      return ok
    end
    
  end

  # APC Testing
  class EI
    attr_accessor :env, :sv, :url, :response
    
    # SOAP default URL and message templates
    @@url_envs = {
      'itdc' => 'http://vf36catd01:8080/submitjob',
      'f8itdc' => 'http://adc051:8080/submitjob',
      'f8stag' => 'http://fc8catqv:80/submitjob'
    }
    
    def initialize(env='itdc', params={})
      @env = env
      @sv = (params[:sv] or SiView::MM.new(@env, params))
      @url = (params[:url] or @@url_envs[@env])
    end
    
    def _create_entry(xml, entry)
      # entry is a hash or array of hashes
      entry = [entry] unless entry.kind_of?(Array)
      entry.each {|e|
        e.each_pair {|k, v|
          xml.dataEntry {|xde|
            xde.name(k.to_s)
            if v.kind_of?(Hash) or v.kind_of?(Array)
              xde.data {|xdd| _create_entry(xdd, v)}
            elsif v == nil
              xde.value
            else
              xde.value(v.to_s)
            end
          }
        }
      }
    end
    
    def _create_control_data(context, data)
      # context and data are Hashes
      # return XML message string
      data = [data] unless data.kind_of?(Array)
      xml = Builder::XmlMarkup.new(:indent=>2)
      xml.instruct!
      xml.controlData(:version=>"1.0") {|x|
        # write context
        x.context {|ctx|
          context.each_pair {|k,v| ctx.item {|i| i.name(k.to_s); i.value(v)}}
        }
        # write data
        x.data {|xd| _create_entry(xd, data)}
      }
      xml.target!
    end
    
    def create_request(eqp, lot, params={})
      li = @sv.lot_info(lot, :wafers=>true)
      cj = (params[:cj] or li.cj)
      opNo = (params[:opNo] or li.opNo)
      op = (params[:op] or li.op)
      pd = op.split(".")[0]
      ei_url = ""
      facility = ""
      productgroup = ""
      technology = ""
      apcActionType = "Adjust"
      apcRecipeContext = (params[:recipe_context] or "")
      actual_recipe = (params[:recipe] or "")
      machine_recipe = (params[:machine_recipe] or "")
      ctx = {
        "processDefinition"=>pd, 
        "apcRecipeContext"=>apcRecipeContext, 
        "controlJobId"=>cj, 
        "equipmentType"=>"Process", 
        "operationNumber"=>opNo,
        "portOperationMode"=>"Auto-2",
        "userID"=>@sv.user,
        "apcActionType"=>apcActionType, 
        "equipmentId"=>eqp, 
        "experimentID"=>"NA", 
        "familyLotID"=>li.family, 
        "route"=>li.route,
        "EIReturnURL"=>ei_url, 
        "splitID"=>"NA", 
        "originatorID"=>"CEI", 
        "productGroupID"=>productgroup, 
        "productType"=>"Wafer", 
        "lotID"=>lot, 
        "technology"=>technology, 
        "layer"=>li.layer, 
        "facility"=>facility, 
        "originalProcessDefinition"=>"NA",
        "carrierID"=>li.carrier,
        "operationID"=>op,
        "productID"=>li.product, 
        "E10state"=>"2WPR", 
      }
      recipe = {"Recipe"=>{"Recipe0"=>{
        "Info"=>{"actualRecipeID"=>actual_recipe, "logicalRecipeID"=>"NA",
          "recipeVersion"=>"4", "machineRecipeID"=>machine_recipe,
        },
        "Parameters"=>{"CoatOnly"=>{"value"=>"true", "purpose"=>"Tuning"},
          "RandomizeWaferGroup1"=>{"value"=>nil, "purpose"=>"EI Control"}, 
          "RandomizeWaferGroup2"=>{"value"=>nil, "purpose"=>"EI Control"}, 
          "RandomizeTimestamp"=>{"value"=>nil, "purpose"=>"EI Control"}, 
        },
        "Body"=>nil}}
      }
      tstring = ""
      waferdata = {"WaferProperties"=>li.wafers.collect {|w|
        {w.wafer=>{"slot"=>w.slot, "history"=>"{#{w.alias}}", "locationEnter"=>tstring, "locationExit"=>"NA"}}}
      }
      _create_control_data(ctx, [recipe, waferdata])
    end
    
    def post_request(eqp, lot, params={})
      control_data = create_request(eqp, lot, params)      
      $log.info "#{control_data.inspect}"
      $log.info "#{URI::encode(control_data)}"
      @response = Net::HTTP.post_form(URI.parse(@url), {"controlData"=>control_data})
      return (@response.message == "OK")
    end

    def get_VRNDM_data(eqp, lot, params={})
      res = post_request(eqp, lot, params)
      ($log.warn "error posting APC request"; return nil) unless res
      doc = REXML::Document.new(@response.body)
      ($log.warn "error in APC response"; return nil) unless doc and doc.root
      res = REXML::XPath.match(doc, "//jobResults#{'/data/dataEntry'*6}/value")
      res.collect {|e| e.first.to_s}
    end
    
  end
end