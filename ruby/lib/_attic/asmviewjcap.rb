=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.


Author: Steffen Steidten, 2012-05-09

History:
  2012-12-17 ssteidte,  separate file for AsmErpShipment and AsmTurnkey jobs
  2014-03-11 ssteidte,  cleanup
=end

require 'builder'
require 'asmview'
require 'rqrsp'

# AsmView jCAP Testing Support
module AsmView

  # AsmView jCAP Testing Support
  class JCAP < RequestResponse::MQ
    attr_accessor :env, :av, :product, :route, :ops, :banks, :release_reason, :scrapcancel_code,
      :tkbumproutes, :tksortroutes, :tkroutes, :tkretops, :tktimeout

    def initialize(env, params={})
      super(params[:instance], {use_jms: true, timeout: 300}.merge(params))
      @env = env
      @av = params.has_key?(:av) ? params[:av] : AsmView::MM.new(@env, params)
      @product = params[:product] || 'TKEY01.A0'
      @route = params[:route] || 'TURNKEY.1'
      @tkbumproutes = {'B'=>:bumpb, 'D'=>:bumpa, 'G'=>:bumpa, 'J'=>:bumpb, 'K'=>:bumpa, 'L'=>:bumpb, 'M'=>:bumpa}
      @tksortroutes = {'B'=>:sortc, 'J'=>:sorta, 'K'=>:sorta, 'M'=>:sorta, 'P'=>:sorta, 'R'=>:sorta, 'S'=>:sorta, 'Z'=>:sortb}
      @tkroutes = {bumpa: 'BUMP-A.1', bumpb: 'BUMP-B.1', bumpc: 'BUMP-C.1',
        sorta: 'SORT-A.1', sortb: 'SORT-B.1', assy: 'ASSY-A.1', test: 'TEST-A.1'}
      @tkretops = {bump: '2000.0100', sort: '2300.0100', assy: '4000.0100', test: '9000.0100'}
      @tktimeout = params[:tktimeout] || 300
      @release_reason = params[:release_reason] || 'ALL'
      @scrapcancel_code = params[:scrapcancel_code] || 'EVAL'
      set_ops(params)
      set_banks(params)
    end

    def inspect
      "#<#{self.class.name} env: #{@env}>"
    end

    def set_ops(params={})
      @ops ||= {}
      ##@ops[:lotstart] = (params[:lotstart] or "LOTSTART.1")
      @ops[:tkbumpdispo] = (params[:tkbumpdispo] or "BUMPDISP.1")
      @ops[:tksortdispo] = (params[:tksortdispo] or "SORTDISP.1")
      @ops[:tkassydispo] = (params[:tkassydispo] or "ASSYDISP.1")
      @ops[:tktestdispo] = (params[:tktestdispo] or "TESTDISP.1")
      @ops[:lotshipdesig] = (params[:lotshipdesig] or "LOTSHIPD.1")
      # bump route stages
      @ops[:tranb]  = (params[:tranb] or "TKYTRANB.1")
      @ops[:trans] = (params[:tranb] or "TKYTRANS.1")
      @ops[:trans2] = (params[:tranb] or "TKYTRANR.1")
      @ops[:trana] = (params[:tranb] or "TKYTRANA.1")
      @ops[:ubm]    = (params[:ubm]   or "TKYUBM.1")
      @ops[:sldr]   = (params[:sldr]  or "TKYSLDR.1")
      @ops[:rw1]    = (params[:rw1]   or "TKYRW1.1")
      @ops[:oqab]   = (params[:oqab]  or "TKYOQAB.1")
      @ops[:trans]  = (params[:trans] or "TKYTRANS.1")
      @ops[:sort]   = (params[:sort]  or "TKYSORT.1")
      @ops[:oqas]   = (params[:oqas]  or "TKYOQAS.1")
    end

    def set_banks(params={})
      @banks ||= {}
      @banks[:end_std] = (params[:bank_end_std] or "WINV")
      @banks[:ship] = (params[:bank_ship] or "SHIP")
    end

    def lot_tkinfo(lot)  # DEPRECATED, use @av.lot_characteristics.tkinfo
      pp = @av.user_parameter("Lot", lot)
      h = Hash[pp.collect {|p| [p.name, p.value]}]
      # note: do not replace nil with ""
      SiView::MM::LotTkInfo.new(h["TurnkeyType"], h["TurnkeySubconIDBump"], h["TurnkeySubconIDSort"],
        h["TurnkeySubconIDReflow"], h["TurnkeySubconIDAssembly"], h["TurnkeySubconIDFinalTest"])
    end

    def lot_set_tkinfo(lot, tkinfo)
      $log.info "lot_set_tkinfo #{lot}, #{tkinfo.inspect}"
      ok = true
      ok &= @av.user_parameter_set_or_delete("Lot", lot, "TurnkeyType", tkinfo.tktype)
      ok &= @av.user_parameter_set_or_delete("Lot", lot, "TurnkeySubconIDBump", tkinfo.tkbump)
      ok &= @av.user_parameter_set_or_delete("Lot", lot, "TurnkeySubconIDSort", tkinfo.tksort)
      ok &= @av.user_parameter_set_or_delete("Lot", lot, "TurnkeySubconIDReflow", tkinfo.tkreflow)
      ok &= @av.user_parameter_set_or_delete("Lot", lot, "TurnkeySubconIDAssembly", tkinfo.tkassy)
      ok &= @av.user_parameter_set_or_delete("Lot", lot, "TurnkeySubconIDFinalTest", tkinfo.tktest)
      return ok
    end

    # return fabcode ("F1", "F7") or nil if lot does not exist
    def lot_fabcode(lot)
      return nil unless @av.lot_exists?(lot)
      p = @av.user_parameter("Lot", lot, name: "SourceFabCode")
      return p.value
    end

    # verify the TurnKey process has started, rte is either :sort, :bump, :assy or :test
    #
    # return true on success
    def lot_tkey_started?(lot, rte, params={})
      # verify the TurnKey process has started, rte is either :sort, :bump, :assy or :test
      # return true on success
      $log.info "lot_tkey_started? #{lot.inspect}, #{rte.inspect}"
      ($log.warn "no lot"; return nil) unless lot and @av.lot_exists?(lot)
      ok = true
      lc = lot_characteristics(lot)
      tkroute = @tkroutes[rte]
      unless tkroute
        # assuming rte is not :bumpa, but just :bump, get route from TurnkeyType
        if rte == :bump
          tkroute = @tkroutes[@tkbumproutes[lc.tkinfo.tktype]]
        elsif rte == :sort
          tkroute = @tkroutes[@tksortroutes[lc.tkinfo.tktype]]
        end
      end
      ($log.warn "lot is not on the TKEY route #{tkroute}"; return nil) if tkroute != lc.route
      tkop = @av.route_operations(tkroute)[0].op
      ($log.warn "  lot is not at the TKEY op #{tkop}"; ok=false) if tkop != lc.op
      proc = @av.user_parameter("Lot", lot, name: 'ProcState')
      ($log.warn "  lot status is not 'Processing'"; ok=false) unless proc and proc.value == 'Processing'
      return ok
    end

    # clean up the lot: remove from banks, tools, etc., ensure it has a carrier and move it to :lotstart
    def lot_cleanup(lot, params={})
      # clean up the lot: remove from banks, tools, etc., ensure it has a carrier and move it to :lotstart
      # return true on success
      $log.info "lot_cleanup #{lot}"
      ($log.warn "no lot"; return nil) unless lot
      ok = true
      li = @av.lot_info(lot)
      ok &= (@av.carrier_status_change(li.carrier, "AVAILABLE", :check=>true) == 0) unless li.carrier == ""
      bhs = @av.lot_hold_list lot, :type=>"BankHold"
      ok &= (@av.lot_bankhold_release(lot) == 0) if bhs.size > 0
      ok &= (@av.lot_nonprobankout(lot) == 0) if li.status == "NonProBank"
      ##  ok &= (@av.lot_hold_release(lot, nil, params.merge(:release_reason=>@release_reason)) == 0)
      li = @av.lot_info(lot)
      ok &= (@av.lot_ship_cancel(lot) == 0) if li.status == "SHIPPED"
      ok &= (@av.asm_scrap_wafers_cancel(lot, :reason=>@scrapcancel_code) == 0) if li.status == "SCRAPPED"
      if li.states["Lot Inventory State"] == "InBank"
        ok &= (@av.lot_bank_move(lot, @banks[:end_std]) == 0)
        ok &= (@av.lot_bankin_cancel(lot) == 0)
      end
      #
      ok &= (@av.lot_hold_release(lot, nil, params.merge(release_reason: @release_reason)) == 0)
      op, opNo = params.has_key?(:op) ? [params[:op], nil] : [nil, 'first']
      op = @ops[params[:op]] if op.kind_of?(Symbol)
      ok &= @av.lot_opelocate(lot, opNo, params.merge(op: op))
      ok &= @av.user_parameter_set_verify("Lot", lot, "ProcState", (params[:procstate] || "Waiting"))
      return ok
    end

    # locate lot to op, op is one of the symbols in @ops (e.g. :tkbranch) or :first
    #
    # return true on success
    def lot_locate(lot, op, params={})
      $log.info "lot_locate #{lot.inspect}, #{op.inspect}, #{params.inspect}"
      ($log.warn "no lot"; return nil) unless lot
      ($log.warn "no op"; return nil) unless op
      @av.lot_hold_release(lot, nil)
      if op.to_sym == :first
        res = @av.lot_opelocate(lot, :first)
      else
        _params = params.clone
        _params.delete(:op)
        res = @sv.lot_opelocate(lot, nil, {op: @ops[op], route: @route}.merge(_params))
        ##res = @av.lot_opelocate(lot, nil, {:op=>@ops[op], :route=>@route}.merge(_params))
      end
      return (res == 0)
    end

    # set ERPItem, ERPOrder and Turnkey parameters
    #
    # lotparams is a ERPMES::ErpAsmLot struct
    #
    # return true on success
    def lot_set_params(lot, lotparams, params={})
      # set sublottype
      # set lot script parameters
      lot ||= lotparams.lot
      $log.info "lot_set_params #{lot.inspect}"
      ok = true
      ok &= @av.user_parameter_set_verify("Lot", lot, "ERPItem", lotparams.erpitem)
      ok &= @av.user_parameter_set_verify("Lot", lot, "ERPSalesOrder", lotparams.order)
      ok &= lot_set_tkinfo(lot, lotparams.tkinfo)
      ##ok &= lot_set_grade(lot, (params[:grade] or "70"))
      return ok
    end

    # locate lot to correct dispo op,
    #
    # optionally on (start: true) bring lot on TK branch route and set status Processing (normally done by STB job)
    #
    # return true on success
    def lot_tkey_prepare(lot, params={})
      tktype = @av.user_parameter('Lot', lot, name: 'TurnkeyType').value
      if ["D", "J", "L", "M", "R"].member?(tktype[-1])
        op = :tkbumpdispo
        retop = @tkretops[:bump]
        tkrte = (lot_fabcode(lot) == "F7") ? @tkroutes[:bumpa] : @tkroutes[:bumpb]
      elsif ["AA"].member?(tktype[-2..-1])
        op = :tkbumpdispo
        retop = @tkretops[:bump]
        tkrte = @tkroutes[:bumpc]
      elsif ["S"].member?(tktype[-1])
        op = :tksortdispo
        retop = @tkretops[:sort]
        tkrte = @tkroutes[:sorta]
      elsif ["AB"].member?(tktype[-2..-1])
        op = :tkassydispo
        retop = @tkretops[:assy]
        tkrte = @tkroutes[:assy]
      else
        $log.warn "unexpected turnkey type #{tktype}"
        return
      end
      lot_cleanup(lot, {op: op}.merge(params)) || return
      return true unless params[:start]
      @av.lot_branch(lot, tkrte, retop: retop).zero? || return
      @av.user_parameter_set_verify('Lot', lot, 'ProcState', 'Processing')
    end

    # DEPRECATED, use @av.lot_characteristics
    def lot_characteristics(lot, params={})
      ($log.warn "no lot"; return nil) unless lot
      erpitem = @av.user_parameter("Lot", lot, name: "ERPItem").value
      order = @av.user_parameter("Lot", lot, name: "ERPSalesOrder").value
      fqc = @av.user_parameter("Lot", lot, name: "FabQualityCheck").value
      li = @av.lot_info(lot, :wafers=>params[:wafers])
      status = (li.status == "Waiting") ? @av.user_parameter("Lot", lot, name: "ProcState").value : li.status
      ERPMES::ErpAsmLot.new(lot, lot_tkinfo(lot), erpitem, order, li.product, li.route, li.customer,
        li.sublottype, li.op, li.opNo, status, li.carrier, li.bank, li.wafers, fqc, nil)
    end

   # service requests

    def get_version(params={})
      $log.info "get_version #{self.inspect}"
      xml = Builder::XmlMarkup.new(:indent=>2)
      xml.instruct!
      xml.soapenv :Envelope, "xmlns:soapenv"=>"http://www.w3.org/2003/05/soap-envelope",
        "xmlns:ns"=>"urn:#{(params[:urn] or 'Test')}"  do |enve|
          enve.soapenv(:Body) {|body| body.ns :getVersion}
        end
      ret = _send_receive(xml.target!, {:timeout=>30}.merge(params))
      return ret ? ret[:getVersionResponse][:return] : nil
    end
  end

end
