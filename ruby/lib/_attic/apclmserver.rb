=begin
Dummy APC LotManager service interfacing the ApcLotManager userexit of RTD.
It uses LotManagerEngine as (emulated) LotManager engine.
  
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.
    
Author:   Steffen Steidten, GLOBALFOUNDRIES, INC., 2012

Version:  1.0.4

History:
  2015-05-04 ssteidte,  based on WEBRick::GenericServer instead of GServer (for Ruby 2.2)
=end

require "#{Dir.getwd}/ruby/set_paths"; set_paths
require 'apclotmanager'
require 'rmiregistry'
#require 'gserver'
require 'webrick/server'
require 'optparse'
require 'drb'

# Dummy APC LotManager service interfacing the ApcLotManager userexit of RTD.
# It uses LotManagerEngine as (emulated) LotManager engine.
  
class LotManagerServer <  WEBrick::GenericServer  #GServer
  attr_accessor :env, :port, :addr, :nthreads, :tcpserver, :engine, :apclotmanager, :rmiregistry, :servicename
    
  # env is a string like "itdc" or "adc043"
  #
  # valid parameters are addr to restrict the bind address, port and nthreads      
  def initialize(env, params={})
    @env = env
    @addr = params[:addr] || ''
    @port = params[:port] || 22298   # management server port
    @nthreads = params[:nthreads] || 256
    @servicename = params[:servicename] || 'DummyApcLotManager'
    # create registry
    rport = params[:registryport] || 22299  # RMI registry port
    @rmiregistry = RMIRegistry.new(port: rport).create_registry
    # create LotManager engine
    @engine = APC::LotManagerEngine.new(env, params)
    # start and bind remote server process
    start_apclotmanager
    #
    # start the TCP management server
    #super(@port, @addr, @nthreads)
    super(Port: @port, BindAddress: @addr, MaxClients: @nthreads)
    $log.info inspect
  end
  
  def inspect
    "#<#{self.class.name} env: #{@env}, listening at #{@addr.inspect}:#{@port}>"
  end
  
  def start_apclotmanager
    stop_apclotmanager 
    @apclotmanager = DummyApcLotManager.new(@engine)
    @rmiregistry.rebind(@servicename, @apclotmanager) 
  end
  
  def stop_apclotmanager
    @rmiregistry.unbind(@servicename, @apclotmanager)
  end
  
  # method actually serving the request
  #def serve(io)
  def run(io)
    begin
      # identify the caller (3rd field in the socket, strip the domain name)
      host = io.peeraddr[2].split('.')[0]
      #
      loop do
        io.puts 'OK'
        ff = io.readline.strip.split
        $log.info "#{host.inspect} cmd: #{ff.inspect}"
        cmd = ff[0]
        case cmd
        when 'loglevel'
          $log.level = ff[1]
        when 'startlm'
          start_apclotmanager
        when 'stoplm'
          stop_apclotmanager
        when 'reload'
          load 'apclotmanager.rb'
        when 'exit'
          io.puts 'bye!'
          break
        when 'exitserver'
          $log.warn "stopping on request"
          stop_apclotmanager
          self.stop
          break
        else
          io.puts "unknown command: #{cmd.inspect}"
        end
      end
    rescue => e
      $log.warn $!
      $log.debug e.backtrace.join("\n")
    end
  end
  
end

# implementation of the ApcLotManagerRemote interface
#
# Dummy APC LotManager service interfacing the ApcLotManager userexit of RTD.
#
# It uses LotManagerEngine as (emulated) LotManager engine.

class DummyApcLotManager < java.rmi.server.UnicastRemoteObject
  java_import 'com.globalfoundries.jcap.rtdservices.apc.api.ApcLotManagerRemote'
  include ApcLotManagerRemote
  
  attr_accessor :lmengine
  
  def initialize(lmengine)
    super()
    @lmengine = lmengine
  end
  
  def getLotManager(keylist)
    res = @lmengine.handle_request(keylist)
  end
end

def main(argv)
  # set defaults
  logfile = nil
  env = "itdc"
  port = 22298
  drbport = port + 10000
  verbose = false
  noconsole = true
  # parse cmdline options
  optparse = OptionParser.new {|o|
    o.on('-h', '--help', 'diplay this screen') {puts o; exit}
    o.on('-e', '--env [ENV]', 'env, e.g. itd') {|e| env = e}
    o.on('-l', '--logfile [FILE]', 'log to FILE instead of STDOUT') {|f| logfile = f}
    o.on('-p', '--port [PORT]', "port number, default is #{port}") {|p| port = p}
    o.on('-d', '--drbport [PORT]', "drb port number, default is #{drbport}") {|p| drbport = p}
    o.on('-v', '--verbose', 'verbose logging') {verbose = true}
    o.on('--console', 'verbose logging') {noconsole = false}
  }
  optparse.parse(*argv)
  # create logger
  create_logger(file: logfile, file_verbose: verbose, verbose: verbose, noconsole: noconsole)
  STDOUT.puts "logging to #{logfile}, console log: #{!noconsole}"
  #
  srv = LotManagerServer.new(env, port: port)
  DRb.start_service("druby://#{Socket.gethostname}:#{drbport}", srv)
  srv.start  #.join
end

main(ARGV) if __FILE__ == $0
