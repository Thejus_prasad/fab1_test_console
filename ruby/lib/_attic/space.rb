=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.


Author: Steffen Steidten, 2011-05-13

History:
  2012-01-06 ssteidte, refactored to use the new Space::DCR class for creation of DCRs
  2012-11-19 ssteidte, fixed errors in runId determination (due to previous changes in xml_to_hash)
  2016-12-07 sfrieske, use lib rqrsp_mq instead of rqrsp
  2017-07-11 sfrieske, separated Exerciser into dcr_exerciser.rb
  2017-07-12 sfrieske, moved txtime to rqrsp_mq, removed obsolete send_dcr_file
=end

require 'zlib'
require 'remote'
require 'rqrsp_mq'


# SPACE Testing Support
module Space
  # Access SIL config
  class SIL < RemoteHost
    attr_accessor :advancedprops, :spiseppprops


    def initialize(env, params={})
      super("sil.#{env}", params)
      @propsdir = params[:propsdir] || '/amdapps/inspace/sil/sil/standalone/configuration/sil'
    end

    def read_jobprops
      @advancedprops = read_propsfile(File.join(@propsdir, 'advanced.properties'))
    end

    def read_spiseppprops
      @spiseppprops = read_propsfile(File.join(@propsdir, 'spisepp.properties'))
    end
  end


  # SIL Client sending DCRs like the EIs
  class Client < RequestResponse::MQXML
    attr_accessor :env

    def initialize(env, params={})
      @env = env
      super(params[:instance] || "space.#{@env}", params.merge(use_jms: false))
    end

    # sends DCRs provided either as string containg valid XML or as Space::DCR object
    #
    # return response hash on success
    def send_dcr(dcr, params={})
      $log.info "send_dcr" unless params[:silent]
      dcr = dcr.to_xml unless dcr.kind_of?(String)
      params[:export] = "log/#{caller_locations(1, 1)[0].label}.xml" if params[:export] == :caller
      send_receive(dcr, params)
    end
  end

end
