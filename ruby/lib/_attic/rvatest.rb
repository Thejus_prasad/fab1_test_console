=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author:     ssteidte, 2013-07-09

Version:    1.2.1

History:
  2016-08-24 sfrieske,  better logging
=end

require 'yaml'
require 'rvaresult'
require 'rvaserver'
require 'charts'


# RVA Testing Support
module RVA
  # RVA test execution support
  class Test
    attr_accessor :env, :server, :result, :tcdir, :refviolations

    def initialize(env, params={})
      @env = env
      @tcdir = params[:tcdir] || 'testcasedata/rva'
      @server = Server.new(env, params)
    end

    def inspect
      "#<#{self.class.name} env: #{@env}>"
    end

    def reload_files
      @server.reload_files
    end

    # have RVA validate the specs and parse the result file
    #
    # return true on successful parsing of the result file (even if errors are found)
    def validate(specs, params={})
      f = @server.validate(specs, params) || ($log.warn "error loading result file #{@server.locations}"; return)
      @result = ResultFile.new(f)
      @result.parse
    end

    # Store the found violations as YAML file in the testcase directory. Use with care!
    def save_testcase(tcname)
      tcname += '.yaml' unless tcname.end_with?('.yaml')
      fname = File.join(@tcdir, tcname)
      $log.info "writing file #{fname.inspect}"
      File.binwrite(fname, @result.violations.to_yaml)
    end

    # compare the found violations to the reference (either a hash or a file name)
    #
    # return true on success
    def verify(ref, params={})
      $log.info "verify #{ref.inspect}"
      if ref.kind_of?(String)
        # passed as testcase name
        ref += '.yaml' unless ref.end_with?('.yaml')
        ref = File.join(@tcdir, ref)
        ($log.warn "no such file: #{ref.inspect}"; return) unless File.exists?(ref)
        @refviolations = YAML.load_file(ref)
      else
        # passed as reference hash of violations
        @refviolations = ref
      end
      ok = true
      @refviolations.each_pair {|kref, vvref|
        $log.info "  #{kref.inspect}"
        vv = (@result.violations[kref] || [])
        ($log.warn "->    wrong count: #{vv.size} instead of #{vvref.size}"; ok=false) if vv.size != vvref.size
        vvref.each_with_index {|vref, i|
          details = vref[:details] || ''
          $log.info "    #{vref[:severity]}/#{vref[:ruleid]}/#{vref[:count]} \t#{details[0..58]}"
          v = vv[i]
          ($log.warn "->    violations not detected"; ok=false; next) if v.nil?
          [:severity, :ruleid, :count].each {|k|
            ($log.warn "      #{k} mismatch"; ok=false) if v[k] != vref[k]
          }
          ($log.warn "->      wrong postion count: #{v[:positions].size} instead of #{vref[:positions].size}"; ok=false) if v[:positions].size != vref[:positions].size
          vref[:positions].each_with_index {|posref, j|
            pos = v[:positions][j]
            ($log.warn "->    missing entry ##{j}"; ok=false; next) unless pos
            if posref[:rule]
              $log.info "      ##{j} (#{posref[:path]}): #{posref[:rule][0..24]}"
              ($log.warn "->      wrong rule: #{pos[:rule]}"; ok=false) unless pos[:rule].start_with?(posref[:rule])
            end
            if posref[:col]
              colrowref = "#{posref[:col]}#{posref[:row]}"
              colrow = "#{pos[:col]}#{pos[:row]}"
              ($log.warn "->      wrong cell: #{colrow} instead of #{colrowref}"; ok=false) if colrow != colrowref
            end
            if pos[:cellvalue]
              if cvref = posref[:cellvalue]
                ($log.warn "->      wrong cellvalue: #{pos[:cellvalue]}"; ok=false) unless pos[:cellvalue].start_with?(cvref)
              else
                ($log.warn "->      missing ref for cellvalue #{pos[:cellvalue]}"; ok=false)
              end
            end
          }
        }
      }
      return ok
    end

    # have RVA validate the specs, parse the result file and compare to the stored reference data
    #
    # return true if reference data is matched
    def validate_verify(spec, params={})
      validate(spec, params) || return
      verify(params[:tcname] || spec)
    end

    # run a series of validations with increasing spec number and create a chart
    #
    # return true on successful completion
    def validation_performance(specs, params={})
      wait_timeout = params[:wait_timeout] || 3600
      numbers = params[:numbers] || [1, 2, 5, 10, 30, 70, 100, 200, 300, 500, 1000]
      numbers.pop while numbers.last > specs.size
      vtimes = numbers.collect {|n|
        $log.info "-- validation of #{n} specs"
        res = @server.validate(specs.take(n), wait_timeout: wait_timeout, effective: params[:effective])
        [n, res ? @server.valduration : -1]
      }
      type = params[:effective] ? "effective" : "signoff"
      $log.info "before #{type} validation times:\n#{vtimes.inspect}"
      fname = "log/rva_#{Time.now.utc.iso8601.gsub(':', '-')}_#{type}.png"
      $log.info "creating chart #{fname}"
      ch = Chart::XY.new(vtimes)
      ch.create_chart(title: "Before #{type.capitalize} Verification", x: 'n specs', y: 't (s)', dotsize: 6, export: fname)
      return true
    end
  end

end
