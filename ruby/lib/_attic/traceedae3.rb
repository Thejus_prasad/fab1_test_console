=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.
    

Author:     ssteidte, 2012-01-30
            dsteger, 2013-04-20
Version:    1.3.1

History:
  2012-01-30 ssteidte,  adapted to new xml_to_hash
  2013-04-12 dsteger,   added error general error handling, trace event collection
  2013-08-23 ssteidte,  changed back to old error handling to avoid aborts on errors
  2013-10-14 ssteidte,  separated E3 classes
=end

require 'traceeda'

# Test TraceEDA (WonderWare)
module TraceEDA
  class InfA < ETC
    def initialize(eqp, eas, params={})
      super(eqp, eas, params.merge(:service=>"interfaceAExtended"))
    end
    
    # IFA: define new plan
    def define_dcp(params={})
      p = (params[:plan] or @plan)
      $log.info "define_dcp :plan=>#{p.inspect}"
      xmlmp = Builder::XmlMarkup.new(:indent=>0)
      xmlmp.instruct!
      xmlmp.PlanMapping "xmlns"=>"http://www.amd.com/schemas/interfaceAExtended" do |pm|
        (params[:events] or @events).each_pair do |k,v|
          v.each_pair do |t,contexts|
            pm.EventMapping "name"=>k, "tagName"=>"Eqp:[EQP]/#{t}"
          end
        end
        pm.TraceRequestMapping "id"=>"0", "dataChannel"=>"ProcessingChamberTrace" do |am|
          (params[:parameters] or @parameters).each_pair {|k,v|
            v = "string" if v.start_with?("string")
            am.TraceParameterMapping "name"=>k, "tagName"=>"Eqp:[EQP]/#{k}", "dataType"=>v, "units"=>@units
          }
        end
        (params[:alarms] or @alarms).each do |e|
          pm.ExceptionMapping "name"=>e, "tagName"=>"Eqp:[EQP]/#{e}"
        end
      end
      xmldcp = Builder::XmlMarkup.new(:indent=>0)
      xmldcp.instruct!
      xmldcp.DataCollectionPlanType "xmlns"=>"urn:semi-org.xsd.E134-1.V1105.DCM", "id"=>p, "name"=>"qa_test", "intervalInMinutes"=>"0", "isPersistent"=>"true" do |dm|
          dm.Description "/InterfaceA/DCP Adhoc/Test/AutoTest"
          dm.TraceRequest "id"=>"0", "intervalInSeconds"=>"10.0", "collectionCount"=>"0", "groupSize"=>"1", "isCyclical"=>"false"
          dm.ParameterRequest "parameterName"=>"test"
      end

      xml = Builder::XmlMarkup.new(:indent=>2)
      xml.instruct!
      xml.env :Envelope, "xmlns:env"=>"http://schemas.xmlsoap.org/soap/envelope/" do |enve|
        enve.env :Body do |body|
          body.DefineDCPRequest "xmlns"=>"http://www.amd.com/schemas/interfaceAExtended" do |sprq|
            sprq.DCPId p
            sprq.DCPXML xmldcp.target!
            sprq.MappingXML xmlmp.target!
          end
        end
      end
      action = 'DefineDCP'
      res = self._send_receive(xml.target!, {:soapaction=>action}.merge(params))
#      TraceEDA::parse_response(action, res, params)
      return res if params[:nosend]
      ($log.warn "error DefineDCP: #{res.inspect}"; return false) unless res and res.has_key?(:DefineDCPResponse)
      return true
    end
    
    def add_data_subscription
    end
  
  end
  
  
  # Test TraceEDA E3 (WonderWare)
  class E3Test
    attr_accessor :env, :tools, :eas, :etcs, :threads

    def initialize(env, params={})
      @env = env
      @eas = TraceEDA::EAS.new(env, params)
      @eas.connect
      @tools = params[:tools]
      unless @tools
        tool0 = (params[:tool0] or "QATEST_PM")
        n = (params[:ntools] or 5)
        s = nil
        @tools = n.times.collect {|i| s = s.nil? ? "#{tool0}#{i+1}" : s.next}
      end
      @etcs = @tools.collect {|t|
        toolid, chamber = t.split("_")
        plan = "QA-#{toolid}-#{chamber}-#{Time.now.to_i}"
        @eas.store_plan(:plan=>plan) # generate a new mapping each time
        TraceEDA::ETC.new(toolid, @eas, params.merge(:chambers=>[chamber], :plan=>plan))
      }
      @eas.close
    end

    def inspect
      "#<#{self.class.name}, env: #{@env}, tools: #{@tools.inspect}>"
    end

    def load_test(params={})
      tstart = Time.now
      $log.info ""
      @etcthreads = @etcs.collect {|etc|
        Thread.new {
          Thread.current['name'] = "ETC_#{etc.eqp}"
          etc.report_loop(params)
        }
      }
      @etcthreads.each {|t| t.join}
      # summary
      $log.info "\n" + "=" * 95
      $log.info "Retrieval Summary"
      $log.info "-" * 17
      $log.info "test duration: #{((Time.now - tstart)/60).round} min (#{tstart.utc.iso8601} .. #{Time.now.utc.iso8601})\n"
      $log.info "comparison results:"
      ret = true
      # log delays
      perflog = params[:perflog]
      fh = perflog ? open(perflog, "wb") : $stdout
      tt = @etcs[0].report_times.collect {|t| t.utc.iso8601}
      fh.write "Time #{tt.join(' ')}\n"      
      fh.close if perflog
      performance_charts(perflog) unless params[:nolog]
      #
      $log.info "success: #{ret}"
      return ret
    end
  
    # create charts from a performance log file
    def performance_charts(perflog, params={})
      $log.info "performance_charts #{perflog.inspect}"
      ext = File.extname(perflog)
      dirname, fname = File.dirname(perflog), File.basename(perflog, ext)
      lines = File.readlines(perflog)
      return nil unless lines and lines.size > 1
      # first column: caption, other columns: values (times or durations per tool)
      times = lines[0].split[1..-1].collect {|t| Time.parse(t)}
      $tooldata = tooldata = {}
      lines[1..-1].each {|l|
        ff = l.split
        tool = ff.shift
        ff.collect! {|f| f.to_f}
        tooldata[tool] = ff
      }
      vname = (params[:vname] or "delay(s)")
      ch = Chart::Time.new(nil)
      tooldata.each_pair {|tool, tdata| ch.add_series(times.zip(tdata))}
      ch.create_chart(:title=>File.basename(fname), y: vname, dots: true, export: "#{dirname}/#{fname}_chart.png")
      ##ch.create_chart(:title=>File.basename(fname), :y=>vname, :ymin=>0.01, :ymax=>100, :dots=>true, :lg=>true, :export=>"#{dirname}/#{fname}_chartlg.png")
      chdata = []
      tooldata.each_pair {|tool, tdata| chdata += tdata}
      ch = Chart::Histogram.new(chdata)
      ch.create_chart(title: File.basename(fname), bins: 50, x: vname, export: "#{dirname}/#{fname}_hist.png")
    end
    
  end
  
end
