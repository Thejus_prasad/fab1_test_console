=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.
=end
    
require "#{Dir.getwd}/ruby/set_paths"; set_paths
require 'fileutils'
require 'optparse'
require 'drb'

Dir['lib/commons-math*jar'].each {|f| require f}
require 'lib/junit_testing.jar'


module JavaEI
  
  # Ruby wrapper around JavaEI, allowing inspection via DRb
  class Farm
    include com.amd.siview.javaei.JavaEIVars
    attr_accessor:env, :setup, :farm
    
    def initialize(env, params={})
      # load JCS files
      flavour = (params[:flavour] or ENV['_flavour'] or "R10_custom")
      jcsfiles = "lib/#{flavour}/*jar"
      $log.debug "#{self.class.name} loading JCS files #{jcsfiles.inspect}"
      Dir[jcsfiles].each {|f| require f}
      #
      @env = env
      args = ["-env", @env]
      @setup = params[:setup]
      @setup = "#{env}_dummyei" unless params.has_key?(:setup)  # allows to disable setup with :setup=>nil
      args += ["-setup", @setup] if @setup
      args << "-notools" if params[:notools] != false
      args << "-noexerciser" if params[:noexerciser] != false
      args << "-noport" if params[:noport]
      iorfiles = (params[:iorfiles] or params[:fileserver])
      args += ["-fileserver", iorfiles] if iorfiles
      args += ["-port", params[:port].to_s] if params[:port]
      begin
        @farm = com.amd.siview.javaei.JavaEIFarm.new(args.to_java(:String))
      rescue java.net.BindException
        throw "port for JavaEIFarm is in use"
      end
      start_tool((params[:tools] or params[:eqp] or []))
    end

    def inspect
      "#<#{self.class.name} env: #{@env}, setup: #{@setup}>"
    end
    
    def method_missing(meth, *args, &block)
      @farm.send(meth, *args, &block) if @farm
    end
    
    def respond_to?(meth)
      @farm.instance_methods.member?(meth.intern)
    end

    # start ORB, usually not required
    def start
      return
      begin
        Thread.new {@farm.start}
      rescue => e
        $log.warn "errror starting JavaEIFarm\n#{$!}"
        $log.debug e.backtrace.join("\n")
      end
    end

    def shutdown
      begin
        @farm.shutdown
      rescue => e
        $log.warn "errror stopping JavaEIFarm\n#{$!}#{e.backtrace.join("\n")}"
      end
    end
    alias :stop :shutdown
    
    def get_ei(eqp)
      @farm.get_ei(eqp)
    end
    
    def start_tool(eqp)
      eqp = eqp.split if eqp.kind_of?(String)
      eqp.collect {|e|
        if tools.member?(e)
          $log.info "eqp #{e.inspect} has already been started"
        else
          @farm.startTool(e)
        end
        tools.member?(e)
      }
    end
    
    def stop_tool(eqp)
      eqp = eqp.split if eqp.kind_of?(String)
      eqp.collect {|e|
        if tools.member?(e)
          @farm.stopTool(e)
        else
          $log.info "eqp #{e.inspect} is not running"
        end
        !tools.member?(eqp)
      }
    end
    
    def get_ior(eqp)
      begin
        eiref = get_ei(eqp).get_ei_ref
        @farm.get_morb.object_to_string(eiref)
      rescue => e
        $log.warn $!
        nil
      end
    end
    
    def write_ior(eqp, path=nil)
      ior = get_ior(eqp) or return nil
      path ||= "log/ior/#{eqp}.IOR"
      FileUtils.mkdir_p(File.dirname(path))
      open(path, "wb") {|fh| fh.write(ior)}
      return path
    end
    
    def tools
      @farm.tools.collect {|t| t.to_s}
    end
    
    def proctime(eqp)
      get_ei(eqp).get_proctime
    end
    
    def set_proctime(eqp, t)
      get_ei(eqp).set_proctime(t)
    end
    
    # get tool variables, defaults if eqp is nil
    def variables(eqp=nil)
      begin
        vars = eqp ? get_ei(eqp).vars : @farm.vars  # Enum
        Hash[vars.var_list.collect {|v|
          [v.to_s, vars.get_string(v.class.get_variable_name(v.to_s))]
        }.sort]
      rescue
        $log.warn $!
        {}
      end  
    end
    
    # get the value for a specific variable
    def variable(eqp, name)
      res = variables(eqp)
      return nil unless res
      return res[name]
    end
    
    # set value of a variable for a specific tool or globally when eqp is nil, with verification
    def set_variable(eqp, name, value)
      begin
        if eqp
          get_ei(eqp).vars.set(Java::ComAmdSiviewJavaei::JavaEIVars::VARS_EI.getVariableName(name), value)
        else
          @farm.vars.set(Java::ComAmdSiviewJavaei::JavaEIVars::VARS_FARM.getVariableName(name), value)
        end
      rescue
        return false
      end
    end
    
    # return true on success
    def carrier_arrived(carrier, eqp, port, params={})
      begin
        return get_ei(eqp).handleCarrierArrival(carrier, port, params[:brs])
      rescue => e
        $log.warn $!
        $log.debug e.backtrace.join("\n")
        return nil
      end
    end
    
  end


  # remote control of a JavaEI::Farm
  class FarmRemote
    attr_accessor :env, :farm
    @@uri_envs = {"itdc"=>["f36asd27", 32221]}
    
    def initialize(env, params={})
      @env = env
      host, port = params[:host], params[:port]
      host, port = @@uri_envs[env] unless host and port
      DRb.start_service
      @farm = DRbObject.new_with_uri("druby://#{host}:#{port}")
      class << @farm
        undef :instance_eval  # force call to be passed to remote object
      end
    end
  end
  
end

def main(argv)
  # set defaults
  logfile = nil
  env = "itdc"
  port = 22221
  drbport = port + 10000
  verbose = false
  noport = false
  notools = false
  # parse cmdline options
  optparse = OptionParser.new {|o|
    o.on('-h', '--help', 'diplay this screen') {puts o; exit}
    o.on('-e', '--env [ENV]', 'env, e.g. itdc') {|e| env = e}
    o.on('-l', '--logfile [FILE]', 'log to FILE instead of STDOUT') {|f| logfile = f}
    o.on('-p', '--port [PORT]', "port number, default is #{port}") {|p| port = p}
    o.on('-d', '--drbport [PORT]', "drb port number, default is #{drbport}") {|p| drbport = p}
    o.on('-t', '--notools', "no tool startup") {|p| notools = true}
    o.on('-x', '--noport', "no telnet commander") {|p| noport = true}
    o.on('-v', '--verbose', 'verbose logging') {verbose = true}
  }
  optparse.parse(*argv)
  # create logger
  create_logger(:file=>logfile, :file_verbose=>verbose, :verbose=>verbose, :noconsole=>noconsole)
  STDOUT.puts "logging to #{logfile}, console log: #{!noconsole}"
  #
  farm = JavaEI::Farm.new(env, :port=>port, :noport=>noport, :notools=>notools)
  DRb.start_service("druby://#{Socket.gethostname}:#{drbport}", farm)
end

main(ARGV) if __FILE__ == $0
