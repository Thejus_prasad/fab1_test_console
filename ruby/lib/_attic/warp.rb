=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.


SVN: : https://gfsvnp01/svn/warp/warp-trunk/testing

Author:     ssteidte, 2014-06-12

Version:    0.2.1

History
  2016-08-24 sfrieske,  use lib rqrsp_http instead of rqrsp
=end


require 'misc'
require 'rqrsp_http'
require 'xmlhash'

ENV.delete('http_proxy')


# WARP (BRMS) access
module Warp
   ProcessDefinition = Struct.new(:definition, :version, :package, :suspended, :form)
   ProcessInstance = Struct.new(:instance, :definition, :tstart, :suspended, :signals, :signaling)
   InstanceTask = Struct.new(:task, :instance, :definition, :name, :assignee, :blocking, :signaling, :state, :priority, :url, :payload)

  # Workflow Engine REST I/F, not that responses are sometimes in JSON others in XML, post requests require form encoding
  class HttpClient < RequestResponse::Http
    include XMLHash
    attr_accessor :env, :authenticated

    def initialize(env, params={})
      @env = env
      super("warp.#{env}")
      # authentication needs to be done per request because timeouts may be short
      ##@authenticated = authenticate
      ##$log.info "authenticated: #{@authenticated}"
    end

    # no login required
    def server_status
      res = get(resource: 'server/status')
    end

    # return true on success (@user is unset - call it only once)
    def authenticate
      ## too verbose $log.debug "authenticate"
      @cookie = nil
      body = "j_username=#{@user}&j_password=#{Base64.decode64(@params[:password])}"
      post(body, resource: 'identity/secure/j_security_check') || return
      ($log.warn "auth error, no cookie"; return) unless @cookie && @cookie.include?('JSESSIONID')
      ##@user = nil   # no basic auth for subsequent requests required, but beware of short timeouts
      return true
    end

    # return array of ProcessDefinition structs
    def process_definitions
      authenticate || return
      res = get(resource: 'process/definitions') || return
      res = JSON.parse(res)
      res['definitions'].collect {|e|
        ProcessDefinition.new(e['id'], e['version'], e['packageName'], e['suspended'], e['formUrl'])
      }
    end

    # return array of ProcessInstance structs from definition string or struct
    def process_instances(definition)
      authenticate || return
      definition = definition.definition unless definition.kind_of?(String)
      res = get(resource: "process/definition/#{definition}/instances") || return
      res = JSON.parse(res)
      res['instances'].collect {|e|
        tok = e['rootToken']
        ProcessInstance.new(e['id'], e['definitionId'], e['startDate'], e['suspended'], tok['availableSignals'], tok['canBeSignaled'])
      }.sort {|a, b| a.instance <=> b.instance}
    end

    # Dont use, use start_process_instance instead    create a new instance, return ProcessInstance from definition string or struct
    def new_process_instance(definition, params={})
      definition = definition.definition unless definition.kind_of?(String)
      $log.info "new_process_instance #{definition.inspect}, #{params.inspect}"
      authenticate || return
      res = post('', resource: "process/definition/#{definition}/new_instance") || return
      ($log.debug "  empty JSON response"; return) if res.empty?
      e = JSON.parse(res)
      tok = e['rootToken']
      ProcessInstance.new(e['id'], e['definitionId'], e['startDate'], e['suspended'], tok['availableSignals'], tok['canBeSignaled'])
    end

    # create new instance, return ProcessInstanceId (number as String) from definition string or struct
    def start_process_instance(definition, params={})
      definition = definition.definition unless definition.kind_of?(String)
      data = params[:data] || {}
      $log.info "start_process_instance #{definition.inspect}, data: #{data.inspect}"
      authenticate || return
      res = post('', resource: "globalfoundries/workflows/#{definition}/start", payload: JSON(data)) || return
      ($log.warn "  error: #{res.inspect}"; return) if res.start_with?('{') || res.empty?
      return res
    end

    # delete an instance identified as instance id or struct, return true on success
    def delete_process_instance(instance)
      instance = instance.instance unless instance.kind_of?(String)
      $log.info "delete_process_instance '#{instance}'"
      authenticate || return
      res = post('', resource: "process/instance/#{instance}/delete") || return
      ($log.warn "  error: #{res}"; return) unless res.empty?
      return wait_for(timeout: 60) {process_instances(instance).empty?}
    end

    # get dataset defined for a process instance as Hash (same format as passed in start_process_instance :data)
    def process_instance_dataset(instance)
      instance = instance.instance unless instance.kind_of?(String)
      authenticate || return
      res = get(resource: "process/instance/#{instance}/dataset") || return
      ($log.info "  error: #{response.inspect}"; return) if res.empty? || res.start_with?('Could not')
      ret = {}
      res = xml_to_hash(res)
      res[:dataset][:data].each {|e| ret[e['key']] = e[:value][nil]}
      return ret
    end

    # send a signal
    def signal_process_instance(instance, params={})
      instance = instance.instance unless instance.kind_of?(String)
      data = params[:data] || {}
      $log.info "signal_task #{instance.inspect}, data: #{data.inspect}"
      authenticate || return
      res = post('', resource: "globalfoundries/workflows/#{instance}/signals/feedback/multiple", payload: JSON(data)) || return
      ($log.warnres; return false) unless res.empty?
      true
    end

    def finish_workitem(instance, task, wid, state) # ?????
      $log.info "finish_workitem #{task.inspect}"
      authenticate || return
      _submit_request('', resource: "globalfoundries/workflows/#{instance}/requests/#{task}/workItems/#{wid}/finish/#{state}") || return
      res
    end

    # tasks

    def release_task(task)
      task = task.task unless task.kind_of?(Numeric) || task.kind_of?(String)
      $log.info "release_task #{task.inspect}"
      authenticate || return
      post('', resource: "task/#{task}/release")
    end

    def close_task(task, params={})
      task = task.task unless task.kind_of?(Numeric) || task.kind_of?(String)
      $log.info "close_task #{task.inspect}"
      resource = "task/#{task}/close"
      resource += "/#{params[:outcome]}" if params.has_key?(:outcome)
      authenticate || return
      _submit_request('', resource: resource) || return
      res
    end

    # return array of InstanceTask structs from instance string or struct
    def instance_tasks(instance)
      instance = instance.instance unless instance.kind_of?(String)
      authenticate || return
      res = get(resource: "globalfoundries/tasks/#{instance}/process") || return
      res = JSON.parse(res)
      res['tasks'].collect {|e|
        tref = e['taskRef']
        InstanceTask.new(tref['id'], tref['processInstanceId'], tref['processId'], tref['name'], tref['assignee'],
          tref['isBlocking'], tref['isSignalling'], tref['currentState'], tref['priority'], tref['url'], e['payload'])
      }
    end

    # return array of task descriptions from definition string or struct
    def assignable_tasks(user)
      $log.info "assignable_tasks #{user.inspect}"
      authenticate || return
      res = get(resource: "globalfoundries/tasks/#{user}/assignable") || return
      res = JSON.parse(res)
      res['tasks']
    end

    def claim_user_task(user, task)
      task = task.task unless task.kind_of?(Numeric) || task.kind_of?(String)
      $log.info "claim_user_task #{user.inspect}, #{task.inspect}"
      authenticate || return
      res = post('', resource: "globalfoundries/tasks/#{user}/claim/#{task}") || return
      ($log.warn res; return false) unless res.empty?
      true
    end

    def release_user_task(user, task)
      task = task.task unless task.kind_of?(Numeric) || task.kind_of?(String)
      $log.info "release_user_task #{user.inspect}, #{task.inspect}"
      authenticate || return
      res = post('', resource: "globalfoundries/tasks/#{user}/release/#{task}") || return
      ($log.warn res; return false) unless res.empty?
      true
    end

    # return true on success
    def complete_user_task(user, task, params={})
      task = task.task unless task.kind_of?(Numeric) || task.kind_of?(String)
      data = params[:data] || {}
      $log.info "complete_user_task #{user.inspect}, #{task.inspect}, data: #{data.inspect}"
      authenticate || return
      res = post('', resource: "globalfoundries/tasks/#{user}/complete/#{task}", payload: JSON(data)) || return
      ($log.warn res; return false) unless res.empty?
      true
    end

    # users -- not used

    # return true if session is valid ?????
    def sessionid
      # response has the session id if valid
      _submit_request('GET', resource: 'identity/sid') || return
      return (res == @cookie.split('=').last)
    end

  end

end
