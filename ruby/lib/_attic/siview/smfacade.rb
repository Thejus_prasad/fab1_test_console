=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.
    
Author: Steffen Steidten, 2009-05-03

History:
  2013-12-18 ssteidte,  refactored EasySiViewR, SMFacade, XM, AsmViewR and Fab7 SiView
  2013-12-19 ssteidte,  added interface to TxGetObjectInfo
  2014-02-20 ssteidte,  separated SMFacade from SM
=end


module SiView

  # SM Facade for reticle handling
  class SMFacade
    attr_accessor :sm, :smfacade, :jcsfacade, :rc
    
    def initialize(env, params={})
      @sm = SM.new(env, params)
      Dir["lib/commons-logging*"].each {|f| require f}    
      @jcsfacade = com.amd.jcs.siview.facade
      @rc = @jcsfacade.ReturnCode
      @smfacade = @jcsfacade.SMFacade.new(@sm.manager, @sm.user, @sm.password)
      $log.info "connected to SM facade" if @smfacade
    end
    
    def inspect
      "#<#{self.class.name} #{@sm.inspect}>"
    end

    # return ReticleData structure
    def _set_reticle_data(reticle, params={})
      return @jcsfacade.reticle.ReticleData.new(
        reticle,                          # identifier
        "Reticle for Unit Testing",       # description
        "false",                          # usageCheckRequiredFlag
        "0",                              # maximumUsageCount
        "0",                              # maximumUsageDuration
        "0815",                           # partNumber
        "4711",                           # serialNumber
        "SHC",                            # supplierName
        "0",                              # intervalBetweenPM
        @sm.user,                         # owner
        "",                               # userGroup
        "Public",                         # permission
        "",                               # returnCode
        "Info"                            # infoText
      )
    end
    
    # return ReticleGroupData structure
    def _set_reticlegroup_data(reticlegroup, params={})
      return @jcsfacade.reticle.ReticleGroupData.new(
        reticlegroup,                     # identifier
        "Reticle Group for Unit Testing", # description
        @sm.user,                         # owner
        "",                               # userGroup
        "Public",                         # permission
        "",                               # returnCode
        "Info"                            # infoText
      )
    end
    
    # return SAPData structure with reticle and reticlegroup filled in
    def _set_SAP_data(reticle, reticlegroup, facility='Fab36')
      return @jcsfacade.reticle.SAPData.new(facility, _set_reticlegroup_data(reticlegroup), _set_reticle_data(reticle))
    end

    # reticle (and reticlegroup) can be strings or SAPData objects
    #
    # parameters are :facility and :raw
    #
    # call the facade method and return returnCodes for reticle and reticleGroup as array of Strings  
    def _execute(method, reticle, reticlegroup, params)
      facility = (params[:facility] or 'Fab36')
      data = reticle.kind_of?(String) ? _set_SAP_data(reticle, reticlegroup, facility) : reticle
      res = @smfacade.send(method, data)
      rcr, rcrg = res.reticleData.returnCode, res.reticleGroupData.returnCode
      $log.info "#{method}: #{rcr}, #{rcrg}"
      return res if params[:raw]
      return rcr, rcrg
    end
    
    # reticle (and reticlegroup) can be strings or SAPData objects
    #
    # parameters are :facility and :raw
    def check(reticle, reticlegroup, params={})    
      return _execute('check', reticle, reticlegroup, params)
    end
    
    # reticle (and reticlegroup) can be strings or SAPData objects
    #
    # return true on success
    def create(reticle, reticlegroup, params={})  
      res = _execute('create', reticle, reticlegroup, params)
      return res if params[:raw]
      rcr, rcrg = res
      return (rcr == @rc::RETICLE_RELEASED and rcrg == @rc::RETICLEGROUP_RELEASED)
    end

    # reticle (and reticlegroup) can be strings or SAPData objects
    #
    # return true on success
    def delete(reticle, reticlegroup='', params={})  
      res = _execute('delete', reticle, reticlegroup, params)
      return res if params[:raw]
      rcr, rcrg = res
      ret = (rcr == @rc::DELETION_SUCCESS)
      ret &= (rcrg == @rc::DELETION_SUCCESS) if rcrg != ""
      return ret
    end
    
    def reticle_exists?(reticle, reticlegroup='', params={})
      res = check(reticle, reticlegroup, params)
      return (res[0] != @rc::RETICLE_NOT_EXIST)
    end

    def reticlegroup_exists?(reticlegroup, params={})
      res = check('', reticlegroup, params)
      return (res[1] != @rc::RETICLEGROUP_NOT_EXIST)
    end
  end

end
