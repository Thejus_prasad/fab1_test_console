=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: Steffen Steidten, 2009-05-03; Daniel Steger, 2014-10-30

=end


class SiView::SM

  # special search for privilege groups (pass empty string to get all), return array of object ids
  def _privilegegroup_ids(privilegegroup, params={})
    ff = %w(BR_FIELD_PRIVILEGEGROUP_SUBSYSTEMNAME BR_FIELD_PRIVILEGEGROUP_CATEGORY BR_FIELD_PRIVILEGEGROUP_PRIVILEGEGROUPID)
    fields = privilegegroup.split('.', 3).collect.with_index {|name, idx|
      @jcscode.brFieldSearchCriterias_struct.new(false, ff[idx], [name].to_java(:string))
    }
    return object_ids(:privilegegroup, fields, params)
  end

  # add/update the users' privilege group, return true on success  # currently not used
  def user_update_privilegegroup(users, privilegegroup, params={})
    $log.info "user_update_privilegegroup #{users.inspect}, #{privilegegroup.inspect}"
    gid = _privilegegroup_ids(privilegegroup).first || ($log.error "  no such privilege group"; return)
    edit_objects(:user, users, params) {|uinfos|
      uinfos.each {|uinfo|
        ui = extract_si(uinfo.classInfo)
        privcat = privilegegroup.rpartition('.')[0]
        idx = ui.privilegeGroups.find_index {|u| u.identifier.start_with?(privcat)}
        if idx
          ui.privilegeGroups[idx] = gid
        else
          ui.privilegeGroups = oid(ui.privilegeGroups.to_a << gid)
        end
        uinfo.classInfo = insert_si(ui)
      }
    }
  end

  # remove the users' privilege group, return true on success  # currently not used
  def user_delete_privilegegroup(users, privilegegroup, params={})
    $log.info "user_delete_privilegegroup #{users.inspect}, #{privilegegroup.inspect}"
    gid = _privilegegroup_ids(privilegegroup).first || ($log.error "  no such privilege group"; return)
    edit_objects(:user, users, params) {|uinfos|
      uinfos.each {|uinfo|
        ui = extract_si(uinfo.classInfo)
        ui.privilegeGroups = oid(ui.privilegeGroups.to_a.delete_if {|u| u.identifier == privilegegroup})
        uinfo.classInfo = insert_si(ui)
      }
    }
  end

end
