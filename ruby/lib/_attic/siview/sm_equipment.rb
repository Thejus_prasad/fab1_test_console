=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: Steffen Steidten, 2009-05-03; Daniel Steger, 2014-10-30

=end


class SiView::SM

  # currently not used
  def eqp_add_carriercategory(eqp, categories, params={})
    categories = [categories] if categories.kind_of?(String)
    ports = params[:port]
    ports = [ports] if ports.kind_of?(String)
    $log.info "eqp_add_carriercategory #{eqp.inspect}, #{categories}, port: #{ports.inspect}"
    cats = object_info(:carriercategory, categories, raw: true) || ($log.warn "  carrier category does not exist"; return)
    edit_objects(:eqp, eqp) {|oinfos|
      oinfos.each {|e|
        ci = extract_si(e.classInfo)
        ci.portResources.each {|p|
          next if ports && !ports.member?(p.portResourceName)
          cc = p.carrierCategories.to_a
          ccids = cc.collect {|c| c.identifier}
          cats.each {|cat| cc << cat.objectId unless ccids.member?(cat.objectId.identifier)}
          p.carrierCategories = oid(cc)
        }
        e.classInfo = insert_si(ci)
      }
      true
    }
  end

  # currently not used
  def eqp_delete_carriercategory(eqp, categories, params={})
    categories = [categories] if categories.kind_of?(String)
    ports = params[:port]
    ports = [ports] if ports.kind_of?(String)
    $log.info "eqp_delete_carriercategory #{eqp.inspect}, #{categories}, port: #{ports.inspect}"
    edit_objects(:eqp, eqp) {|oinfos|
      oinfos.each {|e|
        ci = extract_si(e.classInfo)
        ci.portResources.each {|p|
          next if ports && !ports.member?(p.portResourceName)
          p.carrierCategories = oid(p.carrierCategories.to_a.delete_if {|c| categories.member?(c.identifier)})
        }
        e.classInfo = insert_si(ci)
      }
      true
    }
  end

  def workarea_add_eqp(area, eqp, params={})
    $log.info "workarea_add_eqp #{area.inspect} #{eqp.inspect}"
    eqpinfo = object_info(:eqp, eqp, raw: true).first || ($log.warn "no such eqp"; return)
    edit_objects(:workarea, area) {|oinfos|
      oinfos.each {|e|
        ci = extract_si(e.classInfo)
        ci.equipments = oid(ci.equipments.to_a << eqpinfo.objectId)
        e.classInfo = insert_si(ci)
      }
    }
  end

end
