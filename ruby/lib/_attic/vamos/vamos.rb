=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: Steffen Steidten, 2011-07-29

History:
  2015-01-09 ssteidte, added support for the new browser GUI
  2016-08-29 sfrieske, use MQ::JMS directly for ToolDisplay as there is no reply
  2018-05-03 sfrieske, disabled CORBA displays (browser only)
  2021-02-05 sfrieske, use instance_of instead of kind_of

Notes:
  StockerDisplay: http://f36asd3:9180/stockdsp/stockerdsp.jsf
  VamosDisplay:   http://f36asd3:9080/vamos/vamosdsp.jsf
=end

# require 'corba'
require 'misc'
require 'mqjms'
require 'xmlhash'


# VAMOS Stocker Display and Tool Display Test Clients.
module Vamos

  # # CORBA test client to initiate stocker display actions, normally done by AMHS EI, UNUSED
  # class StockerDisplay
  #   require Dir['lib/VAP-StockerDisplay*.jar'].first

  #   attr_accessor :_props, :stocker, :port, :url, # browser GUI
  #     :iorparser, :orb, :stockerdisplay, :client  # AMHS EI -> display server

  #   def initialize(env, stocker, port, params={})
  #     @stocker = stocker
  #     @port = port
  #     @url = params[:url] || _config_from_file("stockerdsp.#{env}", 'etc/urls.conf')[:url]
  #     @client = params[:client] || 'QATestClient'
  #     ior = params[:ior]
  #     unless ior
  #       @_props = _config_from_file("mm.#{env}", 'etc/siview.conf') # get the IOR web URL
  #       ior = File.join(File.dirname(@_props[:iorfile]), 'stockerdisplay.ior')
  #     end
  #     @iorparser = IORParser.new(ior)
  #     @orb = Java::OrgOmgCORBA::ORB.init([].to_java(:string), nil)
  #     obj = @orb.string_to_object(@iorparser.ior)
  #     @stockerdisplay = com.amd.stockerdisplay.corba.StockerDisplayHelper.narrow(obj)
  #   end

  #   def inspect
  #     "#<#{self.class.name} #{@iorparser}, #{@stocker}:#{@port}>"
  #   end

  #   def comm_test(params={})
  #     $log.info 'comm_test'
  #     res = @stockerdisplay.commTest(@client)
  #     return (res == "COMM_TEST successful for #{@client}!")
  #   end

  #   def carrier_info(carrier, removed, params={})
  #     # sent by AMHS EI on carrier stockout
  #     $log.info "carrier_info #{carrier.inspect}, #{removed}"
  #     ci = com.amd.stockerdisplay.corba.CarrierInfo.new(carrier, @stocker, @port, removed)
  #     res = @stockerdisplay.setCarrierInfo(ci, @client)
  #     return (res == "Received carrier: #{carrier},#{@stocker},#{@port},#{removed},#{@client}")
  #   end

  #   def remove_carrier_from_history(carrier, params={})
  #     # remove carrier info from displays
  #     $log.info "remove_carrier_from_history #{carrier.inspect}"
  #     res = @stockerdisplay.removeCarrierFromHistory(carrier, @client)
  #     return (res == "Removed carrier #{carrier},#{@client}")
  #   end

  #   def set_error(carrier, params={})
  #     $log.info "set_error #{carrier.inspect}"
  #     msg = params[:msg] || 'QA Test Message'
  #     timeout = params[:timeout] || 60
  #     se = com.amd.stockerdisplay.corba.StockerError.new(carrier, msg, @stocker, @port, timeout)
  #     res = @stockerdisplay.setError(se, @client)
  #     return (res == "Received error for: #{carrier},#{@stocker},#{@port},#{timeout},#{@client}")
  #   end

  #   def clear_error(carrier, params={})
  #     $log.info "clear_error #{carrier.inspect}"
  #     res = @stockerdisplay.clearError(carrier, @client)
  #     return (res == "Cleared error for #{carrier},#{@client}")
  #   end

  #   def set_user(carrier, user, params={})
  #     $log.info "set_user #{carrier.inspect}, #{user.inspect}"
  #     ui = com.amd.stockerdisplay.corba.UserInfo.new(carrier, user, @stocker, @port)
  #     res = @stockerdisplay.setUser(ui, @client)
  #     return (res == "Received setUser: #{carrier},#{user},#{@stocker},#{@port},#{@client}")
  #   end

  #   # # open a CORBA/GUI client
  #   # def start_gui(params={})
  #   #   hostname = params[:hostname] || Socket.gethostname  # any host configured on the server, will resolve the stocker for this host
  #   #   corbahost = params[:corbahost] || URI.parse(@url).host  #iorparser.profiles.first.host
  #   #   cp = (['etc'] + Dir['lib/vamos/VAP-StockerDisplay*.jar'] + Dir['lib/vamos/log4j*.jar']).join(File::PATH_SEPARATOR)
  #   #   cmd = "java -cp #{cp} com.globalfoundries.vap.stockerdisplay.StockerDisplayClient "
  #   #   cmd += "HostName=#{hostname} CorbaHost=#{corbahost} CorbaPort=1055"
  #   #   $log.info "starting stocker display in a separate process:\n#{cmd.inspect}"
  #   #   @guipid = Process.spawn(cmd)
  #   # end

  #   def start_browser(params={})
  #     cmd = "start #{@url}"
  #     $log.info "starting stocker display in a browser: #{cmd.inspect}"
  #     system(cmd)
  #   end

  # end


  # MQ/SOAP test client to initiate VAMOS tools display actions, normally sent by the EI
  class ToolDisplay
    attr_accessor :eqp, :port, :mq, :url

    def initialize(env, eqp, port, params={})
      @eqp = eqp
      @port = port
      @url = params[:url] || _config_from_file("vamosdsp#{'ib' if params[:ib]}.#{env}", 'etc/urls.conf')[:url]
      @mq = MQ::JMS.new("vamos.#{env}", use_jms: false)
    end

    def inspect
      "#<#{self.class.name} #{@mq.inspect}, #{@eqp}:#{@port}>"
    end

    # create a SOAP message normally sent by the EI, return true on success
    # msgtype is one of materialArrived, materialRemoved, waferStarted, waferCompleted
    def _ei_msg(msgtype, carrier, wafers, params={})
      eqp = params[:eqp] || @eqp
      port = params[:port] || @port
      waferstate = params[:waferstate] || 'NEEDS PROCESSING'
      wafers = [wafers] unless wafers.instance_of?(Array)
      recipes = params.has_key?(:recipes) ? params[:recipes] : 'QATestRecipe'
      selected = (params[:selected] == false) ? 'false' : 'true'
      cj = params[:cj] || 'qa-test-job'
      $log.info "#{msgtype} #{carrier} (#{wafers.size} wafers) #{params}"
      data = wafers.collect {|w| {lotID: w.lot,
        recipes: {'soapenc:arrayType'=>'tns1:Recipe[1]', 'xsi:type'=>'soapenc:Array', item: {recipeID: recipes}},
        selected: selected, waferID: w.wafer, waferSlot: w.slot, waferState: waferstate
      }}
      h = {'soapenv:Envelope': {
        'xmlns:soapenv'=>'http://schemas.xmlsoap.org/soap/envelope/',
        'xmlns:soapenc'=>'http://schemas.xmlsoap.org/soap/encoding/',
        'xmlns:xsi'=>'http://www.w3.org/2001/XMLSchema-instance',
        'xmlns:ns'=>'http://soap.vamos.amd.com',
        'xmlns:vamos'=>'http://f36asd6:8088/vamos1/services/VamosSoapService',
        'xmlns:tns1'=>'com.amd.vamos.soap', 'soapenv:Body': {
          "ns:#{msgtype}": {'soapenv:encodingStyle'=>'http://schemas.xmlsoap.org/soap/encoding/', portData: {
            carrier: {
              carrierID: carrier, wafers: {
                'soapenc:arrayType'=>"tns1:Wafer[#{wafers.size}]", 'xsi:type'=>'soapenc:Array', item: data
              }
            },
            entityID: eqp, portID: port, siviewJobID: cj
          }}
        }
      }}
      res = @mq.send_msg(XMLHash.hash_to_xml(h), params.merge(bytes_msg: true))
      return !res.nil?
    end

    def material_arrived(carrier, wafers, params={})
      _ei_msg(:materialArrived, carrier, wafers, {waferstate: 'NEEDS PROCESSING'}.merge(params))
    end

    def material_removed(carrier, wafers, params={})
      _ei_msg(:materialRemoved, carrier, wafers, {waferstate: 'PROCESSED'}.merge(params))
    end

    def wafer_started(carrier, wafers, params={})
      _ei_msg(:waferStarted, carrier, wafers, {waferstate: 'IN PROCESS'}.merge(params))
    end

    def wafer_completed(carrier, wafers, params={})
      _ei_msg(:waferCompleted, carrier, wafers, {waferstate: 'PROCESSED'}.merge(params))
    end

    # msgs sent by test tools not by the EI, useful for GUI endurance tests
    def hold
    end

    def port_loadreq
    end

    def port_unloadreq
    end

    def e10state
    end

    # # open a CORBA/GUI client
    # def start_gui(params={})
    #   # client hostname, any host configured on the server. The server resolves the eqp for this host.
    #   hostname = params[:hostname] || Socket.gethostname
    #   # CORBA server hostname, assuming it is the same as for WebBrowser
    #   uri = URI.parse(@url)  # for defaults
    #   corbahost = params[:corbahost] || uri.host
    #   corbaport = params[:corbaport] || uri.port - 4552
    #   cmd = "java -cp #{Dir['lib/vamos/VAP-VamosClient*.jar'].first}"
    #   cmd += "#{File::PATH_SEPARATOR}#{Dir['lib/vamos/swingmiglayout*.jar'].first}"
    #   cmd += "#{File::PATH_SEPARATOR}#{Dir['lib/vamos/log4j*.jar'].first}"
    #   cmd += " com.globalfoundries.vap.vamos.client.VamosClient "
    #   cmd += "HostName=#{hostname.downcase} CorbaHost=#{corbahost} CorbaPort=#{corbaport}"
    #   $log.info "starting vamos display in a separate process:\n#{cmd.inspect}"
    #   @guipid = Process.spawn(cmd)
    # end

    def start_browser(params={})
      cmd = "start #{@url}"
      $log.info "starting vamos display in a browser: #{cmd.inspect}"
      system(cmd)
    end
  end

end
