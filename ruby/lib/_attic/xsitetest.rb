=begin
SPACE Testing Support

(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

    
Author: Daniel Steger, 3/9/2012
=end

require 'derdack'
require 'siview'
require 'easyxsite'
require 'misc'

# Xsite testing support
class XsiteTest
  attr_accessor :env, :xs, :sv, :derdack
  
  def initialize(env, params={})
    @env = env
    @xs = (params[:xs] or Xsite::EasyXsite.new(env, params))
    @sv = (params[:sv] or SiView::MM.new(env, params))
    @derdack = Derdack::EventDB.new(env)

    @e10logging = {}
  end

  def cleanup_work_requests(eqp, params={})
    res = true
    include_chambers = (params[:chambers] or true)
    wrs = @xs.get_schedule(:eqp=>eqp, :work_requests=>true, :work_orders=>false, :children=>include_chambers)
    wrs = wrs.values.collect {|wr| wr.number} unless wrs == []
    $log.info " found work_requests #{wrs.inspect}"
    wrs.each do |wr|
      res &= @xs.cancel_work_request(wr) == {}
    end
    return res
  end
  
  def cleanup_work_orders(eqp, params={})
    res = true
    include_chambers = (params[:chambers] or true)
    wos = @xs.get_schedule(:eqp=>eqp, :work_requests=>false, :work_orders=>true, :children=>include_chambers)
    wos = wos.values.collect { |wo| wo.number} unless wos == []
    wos.each do |wo|
      res &= @xs.cancel_work_order(wo) == {}
    end
    return res
  end

  # Check for inhibt
  def verify_inhibit(eqp, params={})
    count = (params[:count] or 1)
    if params[:chamber]
      inhibits = $sv.inhibit_list("Chamber", eqp, :reason=>"RQPM", :chamber=>params[:chamber])
    else
      inhibits = $sv.inhibit_list("Equipment", eqp, :reason=>"RQPM")
    end
    inhibits = inhibits.find_all { |i| i.memo.include?(params[:msg])} if params[:msg]
    return verify_equal(count, inhibits.count, " unexpected number of PM inhibits")
  end
  
  # check no workrequest exists
  def verify_no_work_request(eqp, params={})
    $log.info "verify_no_work_request #{eqp}, #{params.inspect}"
    xeqp = eqp
    xeqp += params[:chamber] if params[:chamber]
    wrs = @xs.get_schedule(:eqp=>xeqp, :work_requests=>true, :work_orders=>false, :children=>false)    
    return true if wrs == []
    _wrs = wrs.values
    _wrs = _wrs.find_all { |wr| wr.title == params[:title]} if params[:title]
    _wrs = _wrs.find_all { |wr| wr.dateDueStart > params[:duestart] } if params[:duestart]
    verify_equal([], _wrs, "no work request expected")
  end
  
  
  def verify_work_request(eqp, title, params={})
    $log.info "verify_work_request #{eqp}, #{title.inspect}, #{params.inspect}"
    xeqp = eqp
    xeqp += params[:chamber] if params[:chamber]
    wrs = @xs.get_schedule(:eqp=>xeqp, :work_requests=>true, :work_orders=>false, :children=>false)
    ($log.warn " no work request found for #{xeqp}"; return false) if wrs == []
    _wr = wrs.values.find_all { |wr| wr.title==title}
    $log.warn " more than one work request found with title #{title}: #{_wr.count}" if _wr.count > 1
    wr = _wr[0]
    ($log.warn " no work request found with title #{title}"; return false) unless wr
    $log.info " found work request #{wr.number}"
    if params[:memo]
      comment = wr.lastComment.detailDescription
      ($log.warn " comment does not match: #{params[:memo]}"; return false) unless comment == params[:memo]
    end
    return verify_e10state(eqp, "5DLY", params)
  end

  def verify_work_order(eqp, title, activity, e10state, params={})
    $log.info "verify_work_order #{eqp}, #{title.inspect}, #{params.inspect}"
    xeqp = eqp
    xeqp += params[:chamber] if params[:chamber]
    wos = @xs.get_schedule(:eqp=>xeqp, :work_requests=>false, :work_orders=>true, :children=>false, :reason_codes=>{"ActivityType"=>activity})
    ($log.warn " no work order found for #{xeqp}"; return false) if wos == []
    _wo = wos.values.find_all {|wo| wo.title==title}
    res = verify_equal(1, _wo.count, " more than one work request found with title #{title}: #{_wo.count}")
    wo = _wo[0]
    ($log.warn " no work order found with title #{title}"; return false) unless wo
    $log.info " found work order #{wo.number}"
    if params[:memo]
      comment = wo.lastComment.detailDescription
      res &= verify_equal(params[:memo], comment, " comment does not match: #{params[:memo]}")
    end
    res &= verify_e10state(eqp, e10state, params)
    return res
  end

  # Check Xsite and SiView for correct E10 state (SiView sub state)
  def verify_e10state(eqp, state, params={})
    $log.info "verify_e10state '#{eqp}', '#{state}', #{params.inspect}"
    res = true
    xeqp = eqp
    xeqp += params[:chamber] if params[:chamber]
    svstatus = ""

    sleep 2 # Wait a short while for sync
    res = wait_for(:timeout=>60, :sleeptime=>5) {
      reply = @xs.eqp_check_status( xeqp )

      xstate = Xsite::E10StateMap[state]
      if verify_equal(xstate, reply.state, " wrong E10 state in Xsite")
        if params[:stocker]
          sinfo = @sv.stocker_info(eqp)
          svstatus = sinfo.status
        else
          einfo = @sv.eqp_info eqp
          if params[:chamber]
            cstatus = einfo.chambers.find { |c| c.chamber == params[:chamber] }
            svstatus = cstatus.status
          else
            svstatus = einfo.status
          end
        end
        verify_condition(" wrong E10 state in SiView  not #{state}, but #{svstatus}") { svstatus.end_with?(state) }        
      else
        false
      end
    }
    return false unless res
    log_e10state(eqp, state, params)
    return res
  end

  # Set and verify wait product state
  def set_verify_wait_product( eqp, params={} )
    $log.info "set_verify_wait_product '#{eqp}', #{params.inspect}"
    xeqp = eqp 
    if params[:chamber]
      xeqp = xeqp + params[:chamber]
      @sv.chamber_status_change_req eqp, params[:chamber], "2WPR"
    else
      @sv.eqp_status_change_req xeqp, "2WPR"
    end
    res = verify_e10state eqp, "2WPR", params
    unless res
      # force the change in Xsite
      @xs.eqp_force_state xeqp, "SBY-WT-PRODUCT"
    end
    return verify_e10state eqp, "2WPR", params
  end

  def report_verify_e10state(eqp, status, params={})
    if params[:chamber]
      $sv.chamber_status_change_rpt(eqp, params[:chamber], status, params)
    else
      $sv.eqp_status_change_rpt(eqp, status, params)
    end
    return verify_e10state(eqp, status, params)
  end

  def log_e10state(eqp, state, params={})
    key = "#{eqp}:#{params[:chamber]}"
    @e10logging[key] << state if @e10logging.has_key?(key)
  end

  def reset_e10state_logging(eqp, params={})
    key = "#{eqp}:#{params[:chamber]}"
    @e10logging[key] = []
    @e10logging_time = Time.now
  end

  def verify_e10state_history(eqp, states, starttime, params={})
    $log.info "verify_e10state_history \"#{eqp}\", #{states.inspect}, \"#{starttime.inspect}\", #{params.inspect}"
    res = true
    hist = @xs.eqp_query_history(eqp, {:begin=>starttime, :end=>Time.now}.merge(params))
    entries = hist.sort_by {|i| i.txnSequence}.sort_by {|i| i.time}
    entries = entries.select {|i| ["SIVIEW", "XNotifier"].member?(i.userName)}
    # order events with same timestamp by the previousState via insertion sort
    (1..entries.length-1).each { |i| 
      ee = entries[i]
      j=i
      while (j>0 && entries[j-1].time == ee.time && entries[j-1].state != ee.previousState)
        $log.debug "  exchange #{j}"
        entries[j] = entries[j-1]
        j -= 1
      end
      entries[j] = ee
    }
    return entries
    
    verify_equal(states.count, entries.count, " number of E10 state entries do not match. States in history: #{entries.map {|e| e.state}.inspect}")
    entries.each_with_index do |h,i| 
      res &= verify_equal(Xsite::E10StateMap[states[i]], h.state, " E10 states do not match")
    end
    return res
  end
  
  # Check that checklist is completed and all steps done
  def verify_checklist_completed(wo, cl_index, reason, params={})
    $log.info "verify_checklist_completed \"#{wo}\", #{cl_index}, \"#{reason}\", #{params.inspect}"
    wo_data = @xs.fetch_work_orders(wo)[-1]
    checklist = wo_data.checklists[cl_index]
    ($log.warn "No checklist found!"; return false) unless checklist
    res  = verify_equal(true, checklist.succeeded, "checklist should have succeeded")
    res &= verify_equal(true, checklist.completed, "checklist should be completed")
    checklist.steps.each_value do |step|
      if step.mandatory
        res &= verify_equal(true, step.completed, "step #{step} should be completed")
        res &= verify_equal(reason, step.reasonCode, "step #{step} should have reason code #{reason}")
      end
    end
    return res
  end

  def verify_checklist_step_not_complete(wo, cl_index, cl_step, reason, params={})
    $log.info "verify_checklist_step_not_complete \"#{wo}\", #{cl_index}, \"#{cl_step}\", \"#{reason}\", #{params.inspect}"
    wo_data = @xs.fetch_work_orders(wo)[-1]
    checklist = wo_data.checklists[cl_index]
    ($log.warn "No checklist found!"; return false) unless checklist
    checklist_step = checklist.steps[cl_step]
    ($log.warn "No checklist step found!"; return false) unless checklist_step
    res = verify_equal(reason, checklist_step.reasonCode, "Reason not expected")
    res &= verify_condition("Checklist step should not be completed") { (!checklist_step.completed and !checklist_step.succeeded) }
    if params[:comment]
      res &= verify_condition("comment \"#{params[:comment]}\" not found, but #{checklist_step.comments[-1].detailDescription}") { 
        (checklist_step.comments and checklist_step.comments[-1].detailDescription.index(params[:comment]))
      }
    end
    return res
  end

  # collects derdack events from five secs before startTime until now
  #
  # return false if not the expected count of events found
  def verify_notification(msg_start, msg_more=nil, params={})
    $log.info "#{__method__} #{msg_start.inspect}, #{msg_more.inspect}, #{params.inspect}"
    ($log.warn "  no connection to Derdack DB, skipping test"; return true) unless @derdack.db.connected?
    app = "Xsite"
    evs = @derdack.get_events((params[:tstart] or @ril.txtime), params[:tend], "Application"=>app)
    ($log.warn "  no connection to Derdack"; return nil) unless evs
    msgs = evs.collect {|e|
      $log.debug "  #{e.inspect}"
      m = e.params["Message"]
      next unless msg_start and m.start_with?(msg_start)
      next unless msg_more.nil? or !m.index(msg_more).nil?
      e
    }.compact
    $log.debug " msgs: #{msgs.inspect}"
    $log.warn "  more than one message found:\n #{msgs.inspect}" if msgs.size > 1
    $log.warn "  no message found:" if msgs.size == 0
    return params[:raw] ? msgs : msgs.size == 1
  end
end