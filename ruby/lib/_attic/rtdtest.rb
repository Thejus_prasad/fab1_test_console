=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author:     sfrieske, 2016-12-09
Version:    1.1.5

History:
  2017-01-02 sfrieske, added more A3 and TLgate codes
  2017-02-14 sfrieske, added update_lotsummary_lot
  2017-12-08 sfrieske, fixed filter_response rowidx handling
  2018-04-03 sfrieske, added header check
  2019-01-28 sfrieske, minor cleanup
  2021-02-05 sfrieske, use instance_of instead of kind_of
  2022-01-10 sfrieske, moved set_lmtoolwish to RTD_Rule_AWS and update_lotsummary_lot to RTD_Rule_Gecko
  2022-01-11 sfrieske, moved call_empty_rule to RTD_Rule_EMPTY
=end

# require 'pp'
require 'rtdserver/rtdclient'
require 'siviewtest'


module RTD
  # # reasons for A3='N', 150 means the 151st position in A3code is 1; all 0 in A3info => A3='Y'
  # # cf. http://vf1sfcp01:10080/setupfc-rest/v1/specs/C07-00003623/content?format=html
  # A3codes = {
  #   1=>'SM batch size max undefined', 2=>'carrier xfer state EI/EO', 3=>'carrier reserved', 4=>'carrier has cj', 5=>'no ML FOUPS allowed',
  #   10=>'wrong eqp E10 state', 11=>'TL Gecko QT violation', 12=>'other cascade', 13=>'wrong chamber E10 state', 17=>'not enough wafers for batch',
  #   21=>'load balance', 27=>'ENG recipe select', 29=>'TL Gecko (waiting for higher ranked lot with QT violation)',
  #   30=>'no multipass allowed', 31=>'no AMHS connect', 36=>'port status not LoadReq or disp state not Required',
  #   41=>'GateController file empty', 42=>'carrier has sj', 45=>'waiting for other lots', 49=>'TL Gecko data outdated',
  #   56=>'batch required', 61=>'TL Gecko (lot unscheduled)', 69=>'unerlaubte FOUP ID (4x)', 79=>'TL Gecko runpath',
  #   80=>'TL Gecko: not in schedule', 91=>'wrong carrier category for port', 92=>'no empty foup with matching category',
  #   100=>'carrier xfer state MO/SO but no xj', 104=>'TL related lot missing',
  #   120=>'carrier at other location', 129=>'Anderes Los im Sort-Request ist A3 No', 149=>'carrier has SR',
  #   150=>'not enough empty FOUPS', 151=>'NOFOUPOPEN PD', 162=>'no clean empty FOUP', 165=>'wrong carrier category'
  # }
  # # https://docs.google.com/spreadsheets/d/1clYtLvGgs0j1A4Ph4S2EQkd9g6aZX2g0v7DDXoCP04Y/edit?ts=59228e5d#gid=0
  # TLgatecodes = {
  #   'A'=>'Availability', 'a'=>'Long Down set', 'I'=>'Inhibited', 'D'=>'Dedication', 'C'=>'Missing Chamber for Module Dedication',
  #   'P'=>'PCL FMFG spec', 'L'=>'Missing LotManager data', 'T'=>'Missing ToolWish data',
  #   'G'=>'Polisher RecipeParameter ProcessGroup', 'Q'=>'EqpID != QualEqpID', 'W'=>'WIP Ratio',
  #   'r'=>'no ports available for required carrier category',
  #   'R'=>'blocked reticle', 'i'=>'reticle inhibited', 'l'=>'reticle at other location', 'n'=>'no reticle id found', 'u'=>'reticle not usable',
  #   't0'=>'no data in TEST DB',
  #   'M'=>'Merge Partner Missing'
  # }

  # Test RTD rules
  class Test < SiView::Test
    attr_accessor :client, :lmtrans, :apf_delay, :station, :rule, :category, :parameters, :columns, :colnames, :filter, :response

    def initialize(env, params={})
      super
      @client = RTD::Client.new(params[:rtdinstance] || env, params)
      @apf_delay = params[:apf_delay] || 15   # SiView -> APF sync duration
      @station = params[:station]  # often not used by APF unless station.cfg is used, default: use rule
      @rule = params[:rule]
      @category = params[:category]
      @parameters = params[:parameters] || []
      @filter = params[:filter]   # nil: no filter in call_rule
      @columns = params[:columns] # nil: no column checks in verify_rows
      # column names for A3 code etc. tests
      @colnames = {
        a3code: 'A3code', a3status: 'A3',
        tlgate_blocked: 'is_blocked', tlgate_reason: 'reason', tlgate_seq: 'lot_plan_step_id',
      }
    end

    def inspect
      "#<#{self.class.name} station: #{@station.inspect}, rule: #{@rule.inspect}, category: #{@category.inspect}>"
    end

    # # check equipment sanity from APF perspective, return true on success, UNUSED
    # def check_eqp(eqp, params={})
    #   $log.info "check_eqp #{eqp.inspect}, #{params}"
    #   nocheck = params[:nocheck] || []
    #   nocheck = [nocheck] unless nocheck.instance_of?(Array)
    #   oinfo = @sm.object_info(:eqp, eqp).first || ($log.warn 'eqp not found in SM'; return)
    #   ok = true
    #   # verify equipment type is set
    #   ($log.warn '  missing Eqpuipment Type ID'; ok=false) if oinfo.specific[:eqptype].empty?
    #   # verify ports have the carrier category set
    #   cat = params[:carrier_category] || @carrier_category
    #   port = params[:port] || oinfo.specific[:ports].first[:port]
    #   $log.info "  port: #{port.inspect}, carrier_category: #{cat.inspect}"
    #   pdata = oinfo.specific[:ports].find {|e| e[:port] == port} || ($log.warn "no such port: #{port}"; ok=false)
    #   ($log.warn "  missing port carrier category #{cat}"; ok=false) unless pdata[:carrier_categories].include?(cat)
    #   # # verify AmhsArea is set to the stocker ones  doesn't seem to matter
    #   # unless nocheck.include?('AmhsArea')
    #   #   udata = oinfo.udata['AmhsArea']
    #   #   ($log.warn "wrong UDATA AmhsArea: #{udata.inspect}"; ok=false) if @sv.user_data(:stocker, @stocker)['AmhsArea'] != udata
    #   # end
    #   unless nocheck.include?('Location')
    #     udata_sto = @sv.user_data(:stocker, @stocker)['Location']
    #     udata = oinfo.udata['Location']
    #     ($log.warn "wrong UDATA Location: #{udata.inspect} instead of #{udata_sto.inspect}"; ok=false) if udata_sto != udata
    #   end
    #   #
    #   return ok
    # end

    # # call the rule and return the result as DispatcherResponse struct
    # def call_rule(params={})
    #   ($log.info "waiting #{@apf_delay} s for APF replication"; sleep @apf_delay) if params[:wait] != false
    #   r = params[:rule] || @rule
    #   c = params[:category] || @category
    #   p = params[:parameters] || @parameters
    #   s = params[:station] || @station || r
    #   return @response = @client.dispatch_list(s, report: r, category: c, parameters: p)
    # end

    # # filter a response by passing e.g. filter: {'lot_id'=>lot, 'eqp_id'=>eqp}, returns a new DispatcherResponse
    # def filter_response(response, params={})
    #   filter = params[:filter] || @filter
    #   # if addfilter = params[:addfilter]
    #   #   filter = filter ? filter.merge(addfilter) : addfilter
    #   # end
    #   rowidx = params[:rowidx]
    #   return response unless filter || rowidx
    #   #
    #   response = @response if response == :last
    #   rows = response.rows
    #   if filter
    #     $log.info "  response filter: #{filter}"
    #     cols = filter.keys.map {|k| response.headers.index(k)}
    #     ($log.warn "  unknown column: #{cols} for #{filter.keys}"; return) if cols.index(nil)
    #     filter.values.each_with_index {|v, i|
    #       rows = rows.select {|row| row[cols[i]] == v.to_s}
    #     }
    #   end
    #   ($log.info "filter response: rowidx: #{rowidx}"; rows = [rows[rowidx]]) if rowidx
    #   return DispatcherResponse.new(response.headers, rows, response.rule, response.process_duration)
    # end

    # # verify the response (call the rule unless response is passed) has specific A3 codes; return true on success
    # def verify_a3codes(codes, params={})
    #   verify_rows([codes], params)
    # end

    # # verify multiple rows identified by row index, return true on success
    # def verify_rows(expecteds, params={}, &blk)
    #   response = params[:response] || call_rule(params) || return
    #   response = @response if response == :last
    #   response = filter_response(response, params)  # generic filter ## ?? excluding rowidx
    #   # expecteds are an Array of expectations (can be extended to Hash if rowidx is not 0...expecteds.length)
    #   expecteds = [[expecteds]] if expecteds.kind_of?(Numeric)       # old verify_a3codes style
    #   expecteds = [expecteds] unless expecteds.last.instance_of?(Array)  # a single line may be passed directly
    #   $log.info "verify_rows (#{expecteds.size})"
    #   if response.rows.size != expecteds.size
    #     $log.warn "  wrong number of rows: #{response.rows.size} instead of #{expecteds.size}"
    #     return
    #   end
    #   columns = params[:columns] || @columns
    #   $log.info "columns: #{columns}" if columns
    #   idxcode = response.headers.index(@colnames[:a3code]) || ($log.warn 'missing header "A3code"'; return)
    #   idxstatus = response.headers.index(@colnames[:a3status]) || ($log.warn 'missing header "A3"'; return)
    #   ret = true
    #   expecteds.each_with_index {|expected, idx|
    #     $log.info "row ##{idx} (#{idx + 1}/#{expecteds.size})" if expecteds.size > 1
    #     row = response.rows[idx]
    #     if blk
    #       blk.call(row, idx) || return
    #     end
    #     expcodes = expected[0] || []
    #     expcodes = [expcodes] if expcodes.kind_of?(Numeric)
    #     expstatus = expcodes.empty? ? 'Y' : 'N'
    #     $log.info "  verify A3 #{expstatus.inspect}, #{expcodes.inspect}"
    #     rtdstatus = row[idxstatus]
    #     ($log.warn "  wrong A3 status: #{rtdstatus} instead of #{expstatus}"; ret=false) if rtdstatus != expstatus
    #     # rtdcodes = RTD.a3codes(row[idxcode], silent: params[:silent])
    #     rtdcodes = []
    #     row[idxcode].chars.each_with_index {|c, i| rtdcodes << i + 1 if c != '0'}
    #     unless params[:silent] || rtdcodes.empty?
    #       $log.info "  reasons for A3='N':"
    #       rtdcodes.each {|r| $log.info '  %3.0d: ' % r + A3codes[r]}
    #     end
    #     expcodes.each {|c| rtdcodes.member?(c) || ($log.warn "  missing code #{c}"; ret=false)}
    #     next if columns.nil?  #expected.size < 2
    #     $log.info "  verify columns: #{expected[1..-1]}"
    #     columns.each_with_index {|column, colidx|
    #       v = row[response.headers.index(column)]
    #       vexp = expected[colidx + 1]
    #       ($log.warn "    #{column}: #{v.inspect} instead of #{vexp.inspect}"; ret=false) if v != vexp.to_s
    #     }
    #   }
    #   return ret
    # end

    # # call the rule unless passed as parameter res and verify it returns specific QT blocked reasons; return true on success
    # # extract the reasons for is_blocked='TRUE' from a QT GateController string (e.g. "R[nu]")
    # # used in RTD_Rule_Gecko only
    # def verify_tlgatecodes(codes, params={})
    #   response = params[:response] || call_rule(params) || return
    #   response = @response if response == :last
    #   response = filter_response(response, params)
    #   $log.info "verify_tlgatecodes #{codes.inspect}"
    #   ($log.warn "  result is empty"; return) if response.rows.empty?
    #   ok = true
    #   col_reason = response.headers.index(@colnames[:tlgate_reason]) || ($log.warn "no header '#{@colnames[:tlgate_reason]}'"; ok=false)
    #   col_stat = response.headers.index(@colnames[:tlgate_blocked]) || ($log.warn "no header '#{@colnames[:tlgate_blocked]}'"; ok=false)
    #   return unless ok
    #   blocked = codes.empty? ? 'FALSE' : 'TRUE'
    #   response.rows.each {|row|
    #     rtdcodes = row[col_reason]
    #     # RTD.tlgatecodes(rtdcodes)
    #     $log.info "  reasons for timelink gate blocking:"
    #     rtdcodes.chars.each {|r| $log.info  "  #{r}: #{TLgatecodes[r]}\n" unless ['[', ']'].include?(r)}
    #     # if params[:allow_others]  # use with care!!  currently not used
    #     #   codes.split('').each {|code|
    #     #     ($log.warn "missing reason code #{code.inspect}"; ok=false) unless rtdcodes.include?(code)
    #     #   }
    #     #   $log.warn "wrong reason code: #{row} instead of #{codes.inspect}" unless ok
    #     # else
    #     ($log.warn "wrong reason code: #{row} instead of #{codes.inspect}"; ok=false) if rtdcodes != codes
    #     # end
    #     rtdstat = row[col_stat]
    #     ($log.warn "  wrong 'blocked': #{row} instead of #{blocked}"; ok=false) if rtdstat != blocked
    #   }
    #   return ok
    # end

    # # verify at least one eqp at each op on the run path is not blocked, return true on success
    # # used in RTD_Rule_Gecko and RTD_Rule_GeckoA3 only
    # def check_tlrunpath(params={})
    #   response = params[:response] || call_rule(params) || return
    #   response = filter_response(response, params) || return
    #   $log.info 'check_tlrunpath'
    #   ($log.warn "  error calling rule: #{reponse}"; return) if response.rows.nil?
    #   ($log.warn '  result is empty'; return) if response.rows.empty?
    #   runpath = {}
    #   col_stat = response.headers.index(@colnames[:tlgate_blocked])
    #   col_seq = response.headers.index(@colnames[:tlgate_seq])
    #   response.rows.each {|row|
    #     rtdseq = row[col_seq]
    #     rtdstat = row[col_stat]
    #     runpath[rtdseq] ||= rtdstat
    #     runpath[rtdseq] = rtdstat if runpath[rtdseq] == 'TRUE'   # old status 'blocked', cannot get worse
    #   }
    #   res = runpath.values.inject(true) {|ok, v| ok && v == 'FALSE'}
    #   $log.warn "  some operations are blocked:\n##{runpath.pretty_inspect}" unless res
    #   return res
    # end

  end

end
