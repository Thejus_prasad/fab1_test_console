=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.


Author: Steffen Steidten, 2011-11-23

History:
  2013-08-14 ssteidte, removed Ruby1.8 support
  2015-07-02 ssteidte, moved globals to constants
  2015-08-07 ssteidte, expand clients for parallel submission of  DCRs
  2016-05-10 sfrieske, use rubyXL instead of POI to read Excel files
  2016-12-07 sfrieske, removed unused webmrb, use sfcservice instead of setupfc
  2017-05-17 sfrieske, option to pass AQV msgs for verify_aqv_message
=end

require 'aqv'
require 'derdack'
require 'siview'
require 'space'
require 'spaceapi'
require 'spacedb'
require 'spacedcr'
require 'spaceexcelupload'
require 'sfcservice'
require 'rubyXL'


# extends the module enumerable to have some statistical calculations available in class Array,
# conforming with SPACE conventions
module Enumerable
  def sum
    return self.inject(:+)
  end

  def average
    return self.sum / self.length.to_f
  end
  alias :mean :average
  alias :avg :average

  def median
    if self.length % 2 == 0
      return (self.sort[self.length/2].to_f + self.sort[(self.length/2)-1].to_f)/2
    else
      return self.sort[self.length/2]
    end
  end

  def sample_variance
    m = self.mean
    sum = self.inject(0) {|acc, i| acc + (i-m)**2}
    return sum/(self.length - 1).to_f
  end

  def standard_deviation
    return Math.sqrt(self.sample_variance)
  end

  def range
    return (self.max - self.min)
  end
end


# SPACE Testing Support
class SpaceTest
  include XMLHash

  # Mapping for specs: for a specific sub class:
  #    "column symbol" => [generic (true) or parameter table (false), name of column, split the column]
  SpecCpMapping = {
    'ToolControlPlan' => {
      module: [true, 'Module', false],
      lds: [true, 'LDS', false],
      parameter: [false, 'ParameterName', false],
      lsl: [false, 'LSL', false],
      usl: [false, 'USL', false],
      target: [false, 'Target', false],
      unit: [false, 'unit', false],
      mpd: [false, 'FinalMeasurementPD', false],
      pdrefs: [false, 'ResponsiblePD,ResponsiblePD2,ResponsiblePD3,ResponsiblePD4,ResponsiblePD5', true],
      productgroup: [false, 'ProductGroup', true],
      product: [false, 'ProductID', true],
      criticality: [false, 'Criticality', false],
      template: [false, 'AutochartTemplate', false]
    },
    'ProcessControlPlan' => {
      module: [false, 'EngineeringModule', false],
      lds: [true, 'LDS', false],
      parameter: [false, 'ParameterName', false],
      lsl: [false, 'LSL', false],
      usl: [false, 'USL', false],
      target: [false, 'Target', false],
      unit: [false, 'unit', false],
      mpd: [false, 'MeasurementPD', false],
      pdrefs: [false, 'ResponsiblePD', true],
      product: [true, 'ProductID', true],
      productgroup: [true, 'ProductGroup', true],
      route: [true, 'Route (s)', true],
      criticality: [false, 'Criticality', false],
      template: [false, 'AutochartTemplate', false],
      technology: [true, 'Technology', false],
      nsites: [false, 'SampleSize', false],
      specaggr: [false, 'SpecLimitAggregationLevel', false],
      opno: [false, 'OperationNumber', false],
      customer_criticality: [false, 'SpecialParameter', false]
    },
    'ContaminationControlPlan' => {
      lds: [true, 'LDS', false],
      parameter: [false, 'ParameterName', false],
      lsl: [false, 'LSL', false],
      usl: [false, 'USL', false],
      target: [false, 'Target', false],
      mpd: [false, 'MeasurementPD', false],
      pdrefs: [false, 'ResponsiblePD,ResponsiblePD2,ResponsiblePD3,ResponsiblePD4,ResponsiblePD5', true],
      product: [true, 'ProductID', true],
      productgroup: [true, 'ProductGroup', true],
      criticality: [false, 'Criticality', false],
      template: [false, 'AutoChartTemplate', false],
      technology: [true, 'Technology', false],
      module:  [true, 'Module', false]
    },
    'ElectricalControlPlan' => {
      module: [false, 'ProductModule', false],
      lds: [true, 'LDS', false],
      parameter: [false, 'ParameterName', false],
      lsl: [false, 'LSL', false],
      usl: [false, 'USL', false],
      target: [false, 'Target', false],
      unit: [false, 'unit', false],
      mpd: [false, 'MeasurementPD', false],
      pdrefs: [false, 'ResponsiblePD', true],
      product: [true, 'ProductID', true],
      productgroup: [true, 'ProductGroup', true],
      route: [true, 'Route (s)', true],
      criticality: [false, 'Criticality', false],
      template: [false, 'AutochartTemplate', false],
      technology: [true, 'Technology', false],
      specaggr: [false, 'SpecLimitAggregationLevel', false],
      customer_criticality: [false, 'SpecialParameter', false],
      nexttestpd: [false, 'NextTestPD', false]
    },
    # Fab1 monitoring specs
    'ZTABLESPCLIMITS02'=> {
      parameter: ['Parameter', false],
      lsl: ['LSL', false],
      usl: ['USL', false],
      target: ['Target', false],
      unit: ['Unit', false],
      mpd: ['MOperation', false],
      ppd: ['POperationID', false],
      plogrecipe: ['PLogRecipe', false],
      ptool: ['PTool', false],
      pchamber: ['PChamber', false],
      product: ['ProductID.EC', false],
      productgroup: ['ProductGroup', false],
      route: ['Route', false],
      criticality: ['Criticality', false],
      technology: ['Technology', false],
      mtool: ['MTool', false],
      mslot: ['MSlot', false],
      mlogrecipe: ['MLogRecipe', false],
      event: ['Event', false],
      sapinspectioncharacteristic: ['SAP Inspection Characteristic', false],
      reticle: ['Reticle', false],
      photolayer: ['Photolayer', false],
      reserve1: ['Reserve1', false],
      reserve2: ['Reserve2', false],
      reserve3: ['Reserve3', false],
      reserve4: ['Reserve4', false],
      reserve5: ['Reserve5', false],
      reserve6: ['Reserve6', false],
      reserve7: ['Reserve7', false],
      reserve8: ['Reserve8', false],
      datreserve1: ['DatReserve1', false],
      datreserve2: ['DatReserve2', false],
      datreserve3: ['DatReserve3', false],
      datreserve4: ['DatReserve4', false],
      datreserve5: ['DatReserve5', false],
      datreserve6: ['DatReserve6', false],
      datreserve7: ['DatReserve7', false],
      datreserve8: ['DatReserve8', false]
    }
  }

  ExcelMapping = {
    'PD' => :mpd,
    'Technology' => :technology,
    'Route' => :route,
    'Control Parameter' => :parameter,
    'Parameter Description' => :descr,
    'LSL' => :lsl,
    'Target' => :target,
    'USL' => :usl,
    'Unit' => :unit,
    'Spec Limit Aggregation Level' => :specaggr,
    'Criticality' => :criticality,
    'SPC ErrorAction' => :erraction,
    '# of sites' => :nsites,
    'Design Tag' => :designtag,
    'WithinReticleSelectionRule' => :within,
    'Generic' => 'Generic', #special treatment
    'Parameter' => :parameter,
    'LDS' => :lds,
    'Module' => :module,
    'Template' => :template,
    'Generic Key ID' => :name,
    'METHOD' => :method,
    'Content1' => 'Content',
    'Content2' => 'Content',
    'Content3' => 'Content',
    'Content4' => 'Content',
    'PTool' => :ptool,
    'Filter' => :filter,
    'POperationID' => :ppd,
    'PLogRecipe' => :plogrecipe,
    'PChamber' => :pchamber,
    'MOperation' => :mpd,
    'MTool' => :mtool,
    'MSlot' => :mslot,
    'MLogRecipe' => :mlogrecipe,
    'Event' => :event,
    'SAPInspectionCharacteristic' => :sapinspectioncharacteristic,
    'Reticle' => :reticle,
    'Photolayer' => :photolayer,
    'Reserve1' => :reserve1,
    'Reserve2' => :reserve2,
    'Reserve3' => :reserve3,
    'Reserve4' => :reserve4,
    'Reserve5' => :reserve5,
    'Reserve6' => :reserve6,
    'Reserve7' => :reserve7,
    'Reserve8' => :reserve8,
    'DatReserve1' => :datreserve1,
    'DatReserve2' => :datreserve2,
    'DatReserve3' => :datreserve3,
    'DatReserve4' => :datreserve4,
    'DatReserve5' => :datreserve5,
    'DatReserve6' => :datreserve6,
    'DatReserve7' => :datreserve7,
    'DatReserve8' => :datreserve8,
    'Qual Desc' => :qualdesc,
    'Parameter Desc' => :parameterdesc,
    'SpecLimitsValidFor' => :specLimitsvalidfor,
    'CustomerCriticality' => :customercriticality,
    'SPC OOS Actions' => :spcoosactions,
    'NumberOfSites' => :numberofsites,
    'DesignTag' => :designtag,
    'SPACEScenario' => :spacescenario,
    'Metrology Equipment Type' => :metrologyequipmenttype,
    'Frequency [every n days]' => :frequency,
    'Frequency Lot' => :frequencylot,
    'Frequency Wafer per Lot' => :frequencywaferperlot,
    'Link to PCP' => :linktopcp
  }

  Epsilon = 0.0002

  attr_accessor :env, :spc, :silserver, :sildb, :client, :clients, :exceluploader, :sv,
    :derdack, :sfc, :sapphire, :mqaqv, :ca_timeout, :notification_timeout


  def initialize(env, params={})
    @env = env
    @spc = Space::SpcApi::Session.new(env, params) unless params[:nospace]
    @ca_timeout = params[:ca_timeout] || 240
    @notification_timeout = params[:notification_timeout] || 240
    begin
      @silserver = Space::SIL.new(env)
    rescue
      $log.info "  no connection to SIL server, cannot validate advanced properties"
    end
    unless params[:space_only] # for upcoming AD tests
      @sildb = Space::SILDB.new(env, params)
      @client = Space::Client.new(env, {timeout: 900}.merge(params))
      @clients = 5.times.collect {Space::Client.new(env, {timeout: 900}.merge(params))}
      @exceluploader = Space::ExcelUpload.new(env, params)
      @sv = params[:sv] || SiView::MM.new(env)
      @derdack = Derdack::EventDB.new(env)
      @sfc = SetupFC::Client.new(env)
      @sapphire = MQ::JMS.new("sapph_space.#{env}")
      @mqaqv = MQ::JMS.new("aqv.spc.qa.#{env}")
      # SIL config files
      if @env == 'f8stag'
        @file_spec_limits = 'etc/testcasedata/space/SpecLimits_STAG.SpecID.xlsx'
      else
        @file_spec_limits = 'etc/testcasedata/space/SpecLimits_ITDC.QA-SpecLim-01.xlsx'
      end
      @file_pd_references = 'etc/testcasedata/space/PDSetup.QA-PDSetup-01.xlsx'
      @file_chamber_filter = 'etc/testcasedata/space/ChamberFilter.QA-ChamberFilter-01.xlsx'
    end
  end

  def inspect
    "#<#{self.class.name} env: #{@env}>"
  end

  # upload and verify config files, return true on success  - currently not used
  def sil_upload_config_files(params={})
    file_spec_limits = params[:file_spec_limits] || nil
    file_pd_references = params[:file_pd_references] || nil
    file_chamber_filter = params[:file_chamber_filter] || nil
    speclimit_type = params[:speclimit_type] || 'spec_limits'
    ret = true
    ret &= sil_upload_verify_speclimit(file_spec_limits, params) if !file_spec_limits.nil? && speclimit_type == 'spec_limits'
    @exceluploader.upload_file(file_pd_references, "QA #{Time.now.utc.iso8601}", 'PDReferer', params) if !file_pd_references.nil?
    ret &= sil_upload_verify_chamberfilter(file_chamber_filter, params) if !file_chamber_filter.nil?
    return ret
  end

  def sil_upload_verify_speclimit(file_speclimit, params={})
    comment = "QA-#{Time.now.utc.iso8601}"
    test_ok = @exceluploader.upload_file(file_speclimit, comment, 'Speclimit', params)
    test_ok &= verify_speclimit(filename: file_speclimit, comment: comment, all_active: true)
    return test_ok
  end

  def sil_upload_verify_generickey(file_generic_key, params={})
    comment = "QA-#{Time.now.utc.iso8601}"
    test_ok = @exceluploader.upload_file(file_generic_key, comment, 'Generickey', params)
    test_ok &= verify_generickey(filename: file_generic_key, comment: comment, all_active: true)
    return test_ok
  end

  def sil_upload_verify_chamberfilter(file_chamberfilter, params={})
    comment = "QA-#{Time.now.utc.iso8601}"
    test_ok = @exceluploader.upload_file(file_chamberfilter, comment, 'Chamberfilter', params)
    test_ok &= verify_chamberfilter(filename: file_chamberfilter, comment: comment, all_active: true)
    return test_ok
  end

  # Fab8 only
  def sil_upload_verify_autocharting(file_autocharting, params={})
    comment = "QA-#{Time.now.utc.iso8601}"
    test_ok = @exceluploader.upload_file(file_autocharting, comment, 'Autocharting', params)
    test_ok &= verify_autocharting(filename: file_autocharting, comment: comment, all_active: true)
    return test_ok
  end

  # ensure the templates (given as array of strings) exist, try to rename if not, return true on success
  def templates_exist(folder, templates)
    templates = [templates] if templates.kind_of?(String)
    $log.info "templates_exist in #{folder.inspect}: #{templates.inspect}"
    ret = true
    templates.each {|t|
      $log.debug t
      next if folder.spc_channel(t)
      ch = folder.spc_channel("#{t}_renamed")
      ($log.warn "  missing template #{t}_renamed"; ret = false) unless ch
      ch.name = t if ch
      ($log.warn "  missing template #{t}"; ret = false) unless folder.spc_channel(t)
    }
    return ret
  end


  # submit a single DCR and verify the DCR data, return true on success
  def send_dcr_verify(dcr, params={})
    exporttag = params[:exporttag] || caller_locations(1,1)[0].label.tr('<>', '')
    res = @client.send_dcr(dcr, silent: params[:silent], export: params[:export] || "log/#{exporttag}.xml")
    p = {}
    p[:nooc] = params[:nooc] if params.has_key?(:nooc)
    p[:spcerror] = params[:spcerror] if params.has_key?(:spcerror)
    verify_dcr_accepted(res, params[:nsamples] || 0, p)
  end

  # submit a collection of DCRs in parallel and verify the DCR data, nsamples and nooc must be the same for all DCRs
  # return true on success
  def send_dcrs_verify(dcrs, params={})
    dcrs = [dcrs] unless dcrs.kind_of?(Array)
    exporttag = params[:exporttag] || caller_locations(1,1)[0].label.tr('<>', '')
    ok = true
    if params[:serial]
      dcrs.each_with_index {|dcr, i| ok &= send_dcr_verify(dcr, exporttag: "#{exporttag}_#{i}")}
    else
      dcrs.each_with_index.collect {|dcr, i|
        sleep 0.1
        Thread.new(dcr, i) {|dcr, i|
          Thread.current['name'] = "dcr#{i}"
          ok &= send_dcr_verify(dcr, exporttag: "#{exporttag}_#{i}")
        }
      }.each {|t| t.join}
    end
    return ok
  end

  # verify DCR data, return true on success
  def verify_dcr_accepted(res, nsamples, params={})
    $log.info "verify_dcr_accepted #{nsamples}, #{params.inspect}"
    ($log.warn "  no MQ response"; return false) unless res
    rids = runids(res, params) || ($log.warn "  no runids found"; return)
    $log.info "rids found: #{rids}"
    # get samples, SIL may take a little time
    3.times {|loop|
      sids = @sildb.runid_samples(rids).count
      break if !nsamples || nsamples == sids
      $log.warn "  #{sids} samples instead of #{nsamples} (rids: #{rids})"
      return if loop == 2
      $log.warn "  retrying"
      sleep 10
    }
    nooc = params[:nooc]
    nooc = nsamples if nooc == 'all'
    nooc = nsamples * 2 if nooc == 'all*2'
    if nooc
      # check response to CEI
      res_ooc = res[:submitRunResponse][:submitRunReturn][:spcSummary][:oocPoints]
      if res_ooc != nooc.to_s
        $log.warn "  SIL Response: #{res.inspect}"
        $log.warn "  wrong number of oocPoints: #{res_ooc} instead of #{nooc}"
        return false
      end
      # check for error code
      srrs = res[:submitRunResponse][:submitRunReturn]
      srrs = [srrs] unless srrs.kind_of?(Array)
      srrs.each {|srr|
        errcode = srr[:nonOOCErrorCode]
        ($log.warn "  should return an spc error"; return false) if (params[:spcerror] && errcode.nil?)
        ($log.warn "  should return no spc error #{errcode}"; return false) unless (params[:spcerror] || errcode.nil?)
      }
    end
    return true
  end

  def runids(res, params={})
    ($log.warn "  no MQ response"; return) unless res
    wipstat = params[:wip] || 'wip'
    rids = []
    srrs = res[:submitRunResponse][:submitRunReturn]
    srrs = [srrs] unless srrs.kind_of?(Array)
    srrs.each {|srr|
      runid = srr[:runID].to_i
      rids << runid
      rids << runid - 1 if wipstat == 'mixed'
      ($log.warn "  invalid DCR, #{res.inspect}"; return) unless rids[0] > 0
    }
    return rids
  end

  # Speclimits are reported to EI via DCR response
  def verify_dcr_speclimits(res, parameter_speclimits)
    ret = true
    param_infos = res[:submitRunResponse][:submitRunReturn][:spaceParameterInfo]
    param_infos = [param_infos] unless param_infos.is_a?(Array)
    parameter_speclimits.each_pair {|parameter, speclimits|
      item = param_infos.find {|info| info['parameterName'] == parameter}
      next unless item
      lsl, target, usl = speclimits
      ($log.warn "Target not as expected: #{item["specTarget"].inspect} instead of #{target}"; ret = false) unless target == item["specTarget"].to_f
      ($log.warn "USL not as expected: #{item["specHighLimit"].inspect} instead of #{usl}"; ret = false) unless usl == item["specHighLimit"].to_f
      ($log.warn "LSL not as expected: #{item["specLowLimit"].inspect} instead of #{lsl}"; ret = false) unless lsl == item["specLowLimit"].to_f
    }
    return ret
  end

  # collects derdack events from five secs before startTime until now
  #
  # return false if not the expected count of events found
  def verify_notification(msg_start, msg_more=nil, params={})
    $log.info "verify_notification #{msg_start.inspect}, #{msg_more.inspect}, #{params.inspect}"
    ($log.warn "  no connection to Derdack DB, skipping test"; return true) unless @derdack.db.connected?
    msgcount = params[:msgcount] || 1
    app = (@env == 'f8stag') ? 'SILStaging' : 'SIL'
    msgs = nil
    res = wait_for(timeout: params[:timeout] || @notification_timeout) {
      evs = @derdack.get_events((params[:tstart] || (@client.txtime - 2)), params[:tend], 'Application'=>app)
      ($log.warn "  no connection to Derdack"; return) unless evs
      msgs = evs.collect {|e|
        m = e.params['Message']
        next unless m
        next if msg_start && !m.start_with?(msg_start)
        next if msg_more && !m.include?(msg_more)
        e
      }.compact
      msgs.size >= msgcount
    }
    $log.debug {" msgs: #{msgs.inspect}"}
    $log.warn "  more than #{msgcount} message(s) found (#{msgs.size})" if msgs.size > msgcount
    $log.warn "  less than #{msgcount} message(s) found (#{msgs.size})" if msgs.size < msgcount
    $log.warn "  no message found:" if msgs.size == 0
    return params[:raw] ? msgs : msgs.size == msgcount
  end

  # Verify presence of inhibit as CA
  #  _obj_ is the target equipment, route etc.
  #  _count_ number of inhibits expected
  #  :class is the inhibit class (or default Equipment)
  #  :msg is a substring to be expected in the inhibit memo
  # returns +true+ or +false+
  def verify_inhibit(obj, count, params={})
    cl = params[:class] || :eqp
    sid = params[:sample_id]
    msg = params[:msg]
    details = params[:details]
    $log.info "verify_inhibit #{obj.inspect}, #{count.inspect}, class: #{cl.inspect}"
    t0 = Time.now
    inhibits = nil
    res = wait_for(timeout: params[:timeout] || @ca_timeout) {
      inhibits = @sv.inhibit_list(cl, obj, params)
      inhibits.keep_if {|i| i.memo.include?(msg)} if msg
      if inhibits.size == count
        inhibits.inject(true) {|ok, inh|
          res = true
          if details
            runid = params[:spc_run] && runids(params[:spc_run]).first.to_s
            if inh.memo =~ /RUN_ID->([0-9]+), CH_ID->([0-9]+), CKC_ID->([0-9]+), SAMPLE_ID->([0-9]+)/
              # Auto CA
              res = verify_inhibit_details($2, $4, runid, inh, params[:dcr], params[:lot], params[:cj], params[:nonwip], params[:ldskey], params)
            elsif inh.memo =~ /CH_ID->([0-9]+), CKC_ID->([0-9]+), SAMPLE_ID->([0-9]+), USER->([a-zA-Z0-9]+)/
              # Manual CA
              res = verify_inhibit_details($1, $3, runid, inh, params[:dcr], params[:lot], params[:cj], params[:nonwip], params[:ldskey], params)
            else
              $log.warn "memo does not match"
              res = false
            end
          else
            res = true
          end
          ok && (@sv.siview_time(inh.end) > t0) &&   # is the inhibit still active?
            (inh.sublottypes == '*') &&
            res ## additional details
        }
      else
        false
      end
    }
    $log.warn "  expected #{count} inhibits, got #{inhibits.size}:\n#{inhibits.pretty_inspect}" unless res
    return res
  end

  # Check additional inhibit information
  def verify_inhibit_details(channel_id, sid, runid, inhibit, dcr, lot, controljob, nonwip=false, ldskey=false, params={})
    $log.info("verify_inhibit_details #{channel_id}, #{sid}, #{runid}, #{inhibit}, ...")
    nonwip = params[:nonwip] || false
    spc_data = $sv.user_data(:inhibit, inhibit.iid)
    channel = $spc.spc_channel(channel_id.to_i)
    sample = channel.sample(sid.to_i, ckc: true)
    e_keys = sample.extractor_keys
    d_keys = sample.data_keys
    violation = sample.violations.find { |v| v.is_top_priority } || ($log.error "no violation found"; return false)
    details = inhibit.reason_details.first || ($log.error "no inhibit details found"; return false)
    res = true
    unless nonwip
      dcr_lot = dcr.find_lot_context(lot)
      res &= verify_equal(dcr_lot['id'], details.lot, "  related lot missing")
      res &= verify_equal(controljob, details.cj, "  related control job missing")
      unless ldskey
        res &= verify_equal(dcr_lot['operationNumber'], details.opNo, "  related operation number missing")
        res &= verify_equal(dcr_lot['passCount'], details.pass, "  related passcount missing")
        res &= verify_equal(dcr_lot['route'], details.route, "  related route missing")
        res &= verify_equal(dcr_lot['operationID'], details.pd, "  related operation missing")
      end
    end
    spc_details = details.charts.first
    lds = sample.lds == $lds.lds.id ? 'inline' : 'setup'
    verify_equal(lds, spc_details.dc_type, "  related LDS information missing")
    res &= verify_equal(sample._get_chart.channel_id.to_s, spc_details.chart_group, "  related channel id missing")
    res &= verify_equal(sample._get_chart.ckc_id.to_s, spc_details.chart, "  related chart ckc id missing")
    res &= verify_hash({
      "SPC_RunID"=>runid,
      "SPC_WaferID"=>(nonwip || sample.parameter.end_with?('LOT')) ? '' : e_keys['Wafer'],
      "SPC_SampleID"=>sid,
      "SPC_ChannelName"=>channel.name,
      "SPC_MOperation"=>nonwip ? '' : d_keys['MOperationID'],
      "SPC_MTool"=>d_keys['MTool'],
      "SPC_ParameterName"=>sample.parameter,
      "SPC_Violation"=>violation.text}, spc_data, "user data not correct")
    return res
  end

  def verify_futurehold(lot, msg, msg_more=nil, params={})
    count = params[:expected_holds] || params[:count] || 1
    $log.info "verify_futurehold #{lot.inspect}, #{msg.inspect} count: #{count}"
    allholds = @sv.lot_futurehold_list(lot)
    if messages = params[:messages]
      $log.info "  verifying claim memos: #{messages.inspect}"
      holds = nil
      holds_size = 0
      specific_holds = Hash.new
      messages.each {|m|
        holds = @sv.lot_futurehold_list(lot, memo: m)
        specific_holds[m] = holds.size
        if holds.size == count
          holds_size = holds.size
          break
        end
        if specific_holds.values.sum == count
          holds_size = specific_holds.values.sum
          break
        end
      }
      ($log.warn "  future holds #{messages.pretty_inspect} (#{holds.size}): #{holds.pretty_inspect}"; return false) unless holds && holds_size == count
    else
      holds = @sv.lot_futurehold_list(lot, memo: msg)
      ($log.warn "  future holds #{msg.inspect} (#{holds.size}): #{holds.pretty_inspect}"; return false) unless holds && holds.size == count
    end
    if msg_more
      holds = @sv.lot_futurehold_list(lot, memo: msg_more)
      ($log.warn "  future holds #{msg_more.inspect} (#{holds.size}): #{holds.pretty_inspect}"; return false) unless holds && holds.size == count
    end
    return true
  end

  def verify_futurehold2(lot, msg, params={})
    $log.info "verify_futurehold2 #{lot.inspect}, #{msg.inspect}"
    $log.debug {"  #{params.inspect[0..50]}..."}
    holds = nil
    res = wait_for(timeout: params[:timeout] || @ca_timeout) {
      holds = @sv.lot_futurehold_list(lot, memo: msg)
      if holds.empty?
        false
      else
        if params[:dcr_lot] && @env == 'f8stag'
          verify_claim_memo_department(params[:parameter], params[:dcr_lot][:operationID], params[:dcr_lot][:productID], params[:dcr_lot][:productGroupID], holds)
        else
          true
        end
      end
    }
    $log.warn "  no or wrong future holds:\n#{holds.pretty_inspect}" unless res
    return res
  end

  def verify_futurehold_cancel(lot, msg, params={})
    $log.info "verify_futurehold_cancel #{lot.inspect}, #{msg.inspect}"
    $log.debug {"  #{params.inspect[0..50]}..."}
    holds = nil
    res = wait_for(timeout: params[:timeout] || @ca_timeout) {
      holds = @sv.lot_futurehold_list(lot, memo: msg)
      holds.empty?
    }
    $log.warn "  no or wrong future holds:\n#{holds.pretty_inspect}" unless res
    return res
  end

  def verify_lothold_released(lot, msg, params={})
    $log.info "verify_lothold_released #{lot.inspect}, #{msg.inspect}"
    holds = nil
    res = wait_for(timeout: params[:timeout] || @ca_timeout) {
      holds = @sv.lot_hold_list(lot, memo: msg)
      holds.empty?
    }
    $log.warn "  wrong pending holds:\n#{holds.pretty_inspect}" unless res
    return res
  end

  # used in Test_Space079 only (Fab8)
  def verify_lothold(lot, msg, params={})
    timeout = params[:timeout] || 0
    tend = Time.now + timeout
    holds = nil
    loop do
      holds = @sv.lot_hold_list(lot, memo: msg)
      if holds.size > 0
        if (params[:dcr_lot] and @env == 'f8stag')
          return verify_claim_memo_department(params[:parameter], params[:dcr_lot][:operationID], params[:dcr_lot][:productID], params[:dcr_lot][:productGroupID], holds )
        else
          return true
        end
      end
      break if Time.now > tend
      sleep 15
    end
    $log.warn "  no or wrong lot holds:\n#{holds.pretty_inspect}"
    return false
  end

  # currently used in Fab8 only
  def verify_claim_memo_department(parameter, operation, product, productgroup, hold_list)
    ret = true
    hold_list.each do |h|
      dept = h.memo.scan( /^DEPT->(\w+)[:|,]/ )[0]
      dept = dept[0] if dept

      # In case of Auto CA, the parameter is part of the claim memo
      if parameter
        _parameter = parameter
      else
        para = h.memo.scan( /(^DEPT->\w+: )?(\w+)\[/ )[0]
        _parameter = para[1] if para
      end

      # Get the module from speclimit
      limits = _determine_speclimits8(_parameter, operation, product, productgroup)
      if limits and limits.size == 1
        ret = verify_equal limits[0].module, dept, "  wrong department in hold claim memo"
      else
        $log.warn "  no unique active speclimit for #{_parameter}: #{limits.inspect}"
        ret = false
      end
    end
    return ret
  end

  # currently used in Fab8 only
  def _determine_speclimits8(parametername, pd, product, productgroup)
    limits = @sildb.speclimit(parameter: parametername, active: "Y", pd: pd)
    res = limits.find_all {|l| l.product == product}
    res = limits.find_all {|l| l.product == nil and l.productgroup == productgroup} if res == []
    #should not be found: res = limits.find_all {|l| l.product == nil and l.productgroup == nil} if res == []
    return res
  end

  # Check message sent to sapphire queue for sample - violation comment hash; Fab8 only
  def verify_sapphire_messages(sample_comments, params={})
    ($log.warn "Skipped!"; return true) unless @sapphire
    msgs = @sapphire.read_msgs(params)
    processed_samples = sample_comments.keys
    ret = true
    msgs.each do |msg|
      xml = _service_response(msg, bodytag: "//samples")
      s = xml[:samples][:sample]["sample-id"]
      _si = s.to_i
      if _si and sample_comments.has_key?(_si)
        if sample_comments[_si] == xml[:samples][:sample][:comment].strip
          processed_samples.delete(_si)
        else
          $log.warn "  violation comment does not match for #{s}"
          ret &= false
        end
      end
    end
    processed_samples.each do |s|
      $log.warn " no message found for #{s}"
      ret = false
    end
    return ret
  end

  # Verify DFS message after OOC idenified by channels (TODO: Test075 only, needs to be moved there)
  #   drops all other messages on the DFS queue
  # returns true on success
  def verify_dfs_message(channels, cas_by_channel, params={})
    $log.info "verify_dfs_message #{channels.inspect}, #{cas_by_channel.inspect}, #{params.inspect}"
    @dfs = MQ::JMS.new("dfs_space.#{$env}") unless @dfs
    msgs = @dfs.receive_msgs(params)
    $log.info "received #{msgs.count} messages"
    # first find relevant message with all channel ids in
    msg = msgs.find { |msg| channels.inject(true) { |res,ch| res & (msg =~ /ChannelId: #{ch.chid}/) } }
    ($log.error("DFS: no message found with channelids in message"); return false) unless msg
    # extract ooc information
    ooc_begin_tag = /\n=========================\nOOC Information\n-------------------------\n/
    ooc_end_tag = /\n-------------------------\nEnd of OOC Information\n=========================\n/
    oocs = msg.scan(/#{ooc_begin_tag}(.+?)#{ooc_end_tag}/m).collect { |ooc_str|
      ooc_str = ooc_str.first
      # convert into hash
      #Hash[ooc_str.split("\n").collect { |d| d.split(": ", 2) }.select { |d| d.count == 2}]
      ooc_str#.split("\n")
    }
    res = verify_equal(params[:nooc], oocs.count, "DFS: wrong number of OOCs") if params.has_key?(:nooc)
    cas_by_channel.each_pair do |channels, cas|
      channels.each do |ch|
        if params.has_key?(:ckcs)
          ch, ckcname = ch
          oocs_by_channel = oocs.select { |ooc| ooc =~ /ChannelId: #{ch.chid}\nCKC: #{ckcname}/ }
        else
          oocs_by_channel = oocs.select { |ooc| ooc =~ /ChannelId: #{ch.chid}/ }
        end
        ca_begin_tag = /Corrective Actions:\n-------------------------\n/
        ca_end_tag = /\n\nCharts:/
        oocs_by_channel = oocs_by_channel.map {|ooc| ooc.match(/#{ca_begin_tag}(.+?)#{ca_end_tag}/m) {|m| m[1]}}.compact
        oocs_by_channel.each do |ooc|
          cas.each do |ca|
            res &= ooc.include?(ca)
            ##res &= verify_contains(ca, ooc, "CA #{ca} not found for channel #{ch.chid})")
          end
        end
      end
    end
    # TODO: check control limits
    #res &= verify_equal(params[:ucl], )
    return res
  end

  def _compare(d, r)
    res = true
    if d.kind_of?(Array)
      if (d.size == r.size)
        d.each_with_index {|v, i| res &= _compare(v, r[i])}
      else
        res = false
      end
    elsif d.kind_of?(Hash)
      res = (d.keys | r.keys).inject(true) {|res, k| res && _compare(d[k], r[k])}
    elsif d.kind_of?(Time)
      res = (d - r).abs < 1
    elsif d.kind_of?(Float)
      res = (d - r.to_f).abs <= Epsilon * [1.0, d, r.to_f].max
    elsif d.respond_to?(:end_with?) && d.end_with?('*')
      res = r.start_with?(d[0..-2])
    else
      res = (d == r)
    end
    $log.debug {"  #{d.inspect} != #{r.inspect}"} unless res
    return res
  end

  # compare data with reference data, given as Hash or Structs
  #
  # return true if all data provided in ref exist and are equal to data (data_keys or extractor_keys)
  def verify_data(data, ref, params={})
    ($log.warn "  no data"; return) unless data
    ($log.warn "  wrong ref data: #{ref.inspect}"; return) if ref.kind_of?(Array)
    keys = ref.kind_of?(Hash) ? ref.keys : ref.members
    $log.debug {"  ref keys: #{keys.inspect}"}
    ret = true
    keys.each {|k|
      next if ref[k].nil?  # should be made a fail!, see Test_Space17#submit_dcr_verify_excluded   sfrieske 2016-02-25
      d = data[k]
      ##$log.debug {"  #{d.inspect}, ref: #{ref[k].inspect}"}
      _compare(d, ref[k]) || ($log.warn "  wrong data for #{k}: #{d.inspect}, expected: #{ref[k].inspect}"; ret = false)
    }
    return ret
  end

  # return true if sample data_keys, extractor_keys and moving values match the reference values
  def verify_sample(sample, ref_data, ref_ex, ref_values, params={})
    ref_values = [ref_values] unless ref_values.kind_of?(Array)
    ref_ex = ref_ex[0] if ref_ex.kind_of?(Array) and ref_ex.size == 1
    ret = true
    ret &= verify_data(sample.data_keys, ref_data, params)
    ret &= verify_data(sample.extractor_keys, ref_ex, params)
    _compare(sample.raw_values, ref_values) || ($log.warn "  wrong raw values: #{sample.raw_values.inspect}"; ret = false)
    return ret
  end

  # return true if sample statistics match the reference that MUST be like {'MEAN'=>41, 'STDDEV'=>1, 'MIN'=>31, 'MAX'=>55, 'COUNT'=>5}
  def verify_sample_statistics(sample, ref)
    ret = true
    stats = sample.statistics
    ($log.warn "  wrong MEAN value"; ret = false) if ref['MEAN'] != stats.mean
    ($log.warn "  wrong MIN value"; ret = false) if ref['MIN'] != stats.min
    ($log.warn "  wrong MAX value"; ret = false) if ref['MAX'] != stats.max
    ($log.warn "  wrong RANGE value"; ret = false) if (ref['MAX'] - ref['MIN']) != stats.range
    ($log.warn "  wrong STDDEV value"; ret = false) if ref['STDDEV'] != stats.sigma
    ($log.warn "  wrong COUNT value"; ret = false) if ref['COUNT'] != stats.size
    return ret
  end

  # Verify sample violations and chartlink data (requires CKC for channel)
  # currently not used
  def verify_sample_chartlink(channel, sample, channel_name=false, params={})
    ret = true
    expected_channel_state = params[:expected_channel_state]
    sid = sample.sid
    v = sample.violations.size
    chartlink = @sildb.space_chartlink(sid: sid)
    chartlink.each_with_index {|cl, i|
      if v > 0
        if cl.ooc != "Y"
          $log.warn " chartlink[#{i}]: no OOC indication in chartlink"
          ret = false
        end
      else
        if cl.ooc != "N"
          $log.warn " chartlink[#{i}]: wrong OOC indication in chartlink"
          ret = false
        end
      end

      if channel_name
        if cl.channel_name != s.parameter
          $log.warn " chartlink[#{i}]: channel name should be parameter name #{s.parameter}, but is #{cl.channel_name}"
          ret = false
        end
      else
        if cl.channel_name != channel.name
          $log.warn " chartlink[#{i}]: channel name should be channel name #{channel.name}, but is #{cl.channel_name}"
          ret = false
        end
      end

      if expected_channel_state
        if cl.channel_state != expected_channel_state
          $log.warn " chartlink[#{i}]/#{cl.inspect}: channel state has to be #{expected_channel_state}, but it is #{cl.channel_state.inspect}"
          ret = false
        else
          $log.info " chartlink[#{i}]/#{cl.inspect}: channel state is #{cl.channel_state.inspect}"
          ret = true
        end
      end
    }
    return ret
  end

  def verify_parameters(parameters, folder, nsamples, ekeys=nil, dkeys=nil, params={})
    parameters = [parameters] if parameters.kind_of?(String)
    $log.info "verify_parameters #{parameters} in #{folder.inspect} with #{nsamples} samples"
    ($log.warn "  no folder"; return) if folder.nil?
    ret = true
    parameters.each {|p|
      ch = folder.spc_channel(parameter: p)
      if ch
        samples = ch.samples
        if nsamples && nsamples != samples.size
          $log.warn "  wrong number of samples: #{samples.size} instead of #{nsamples} for parameter #{p} in #{ch.inspect}"
          ret = false
        end
        samples = samples.last(params[:samples_last]) if params.has_key?(:samples_last)
        samples.each {|s|
          ret &= verify_data(s.extractor_keys, ekeys) if ekeys
          ret &= verify_data(s.data_keys, dkeys) if dkeys
        }
        ($log.warn "  aborting after first parameter"; return false) unless ret
      else
        ret &= (nsamples == 0)
      end
    }
    return ret
  end

  # return true if channel information is as provided in ref (_spaceapi_channel struct)
  def verify_channel(ch, ref, dcr=nil, wafer_values=nil, params={})
    ($log.warn "  no channel"; return false) unless ch
    ret = verify_data(ch.inspect, ref, params)
    return ret unless ret && dcr && wafer_values
    # optionally check samples
    nsamples = params[:nsamples] || 1
    wafer_values.each_pair {|w, v|
      samples = ch.samples(wafer: w)
      ($log.warn "  wrong number of samples (#{samples.size}) for wafer #{w} in #{ch.inspect}, wafervalues: #{wafer_values.inspect}"; ret = false) if samples.size != nsamples
      # check only last sample
      samples = [samples.last] if params[:last_sample_only]
      samples.each {|s|
        dkeys = dcr.sample_dkeys({wafer: w, parameter: s.parameter}.merge(params))
        ekeys = dcr.sample_ekeys({wafer: w}.merge(params))
        ekeys.each {|ekey| ekey['Technology'] = '-' if ekey['Technology'] == ''}
        verify_sample(s, dkeys, ekeys, v, params) || ($log.warn "  wrong sample #{s.sid} in channel #{ch.chid} for wafer #{w}"; ret = false)
      }
    }
    return ret
  end

  def verify_sample_speclimits(samples, lsl, tgt, usl)
    ret = true
    samples = [samples] unless samples.kind_of?(Array)
    samples.each {|s|
      slsl = s.specs['LSL']
      ($log.warn "wrong LSL: #{slsl.inspect} instead of #{lsl.inspect}"; ret=false) unless lsl == slsl
      stgt = s.specs['TARGET']
      ($log.warn "wrong target: #{stgt.inspect} instead of #{tgt.inspect}"; ret=false) unless tgt == stgt
      susl = s.specs['USL']
      ($log.warn "wrong USL: #{susl.inspect} instead of #{usl.inspect}"; ret=false) unless usl == susl
    }
    return ret
  end

  # extract comments for violated samples, currently Fab8 only
  def _extract_mrbs(channels)
    mrbs = {}
    channels.each do |ch|
      ch.samples(ckc: true).each do |s|
        if (s.violations.length > 0)
          m = s.ecomment.match(/\[DocumentNumber->(.+), URL->(.+)\]/)
          if m && m[1]
            if mrbs.has_key?(m[1])
              url,samples = mrbs[m[1]]
              mrbs[m[1]] = url, samples << s.sid
            else
              mrbs[m[1]] = [m[2], [s.sid]]
            end
          end
        end
      end
    end
    return mrbs
  end

  # currently used in Fab8 only
  def verify_mrbs(channels, count)
    mrbs = {}
    res = wait_for {
      mrbs = _extract_mrbs(channels)
      mrbs.keys.count == count
    }
    $log.info " MRBs found:"
    mrbs.each_pair do |k,(url,s)|
      $log.info "  #{k}: #{url} #{s.inspect}"
    end
    return res
  end

  # verification of uniqueness of external comments at a sample, currently not used
  def are_ecomments_uniq(sample)
    test_ok = true
    ecomment_array_full = sample.ecomment.split(/\r\n|\n/).sort.select {|c| c != ''}
    ecomment_array_diff = ecomment_array_full - ecomment_array_full.uniq
    unless ecomment_array_diff.empty?
      $log.warn "  #{sample.parameter} #{sample.extractor_keys["Wafer"]}: external comment more than once: #{ecomment_array_diff.pretty_inspect}"
      test_ok = false
    end
    return test_ok
  end

  def verify_ecomment_sample(s, comment)
    if s.ecomment.nil?
      $log.warn "  wrong external comment for #{s.parameter}: expected #{comment.inspect}, got #{s.ecomment.inspect}" if !comment.nil?
      return comment.nil?
    else
      if comment.nil?
        $log.warn "  wrong external comment for #{s.parameter}: expected #{comment.inspect}, got #{s.ecomment.inspect}" if !s.ecomment.nil?
        return s.ecomment.nil?
      else
        res = s.ecomment.include?(comment)
        $log.warn "  wrong external comment for #{s.parameter}: expected #{comment.inspect}, got #{s.ecomment}" unless res
        # are_ecomments_uniq(s)  # was informational only, disabled for now
        return res
      end
    end
  end

  def verify_ecomment(channels, comment, params={})
    channels = [channels] unless channels.kind_of?(Array)
    test_ok = true
    channels.each {|ch|
      samples = ch.samples(params.merge(ckc: true))
      samples.each {|s|
        test_ok &= verify_ecomment_sample(s, comment) unless s.violations.empty?
      }
    }
    return test_ok
  end

  # verify ecomment per CKC, used in Test_Space70 only
  def verify_ecomment_ckc(ckc, comment)
    return ckc.samples.inject(true) do |res, s|
      if s.violations.count > 0
        ($log.warn " #{s.parameter} #{s.extractor_keys["Wafer"]}: no external comment"; res = false) if s.ecomment.nil?
        found_index = s.ecomment.index(comment)
        if found_index
          if s.ecomment[found_index+1..-1].index(comment)
            ($log.warn " #{s.parameter} #{s.extractor_keys["Wafer"]}: comment more than once (#{comment.inspect}): #{s.ecomment.inspect}"; res = false)
          end
        else
          ($log.warn " #{s.parameter} #{s.extractor_keys["Wafer"]}: comment not as expected (#{comment.inspect}): #{s.ecomment.inspect}"; res = false)
        end
      end
      res
    end
  end

  # extract the DC generic keys from the submitted dcrs, respecting the referred PD in the key setup (dcr[0] should be for RespPD1,
  #   dcr[1] should be for RespPD2, etc
  #
  # return hash of generic key values
  def get_sample_generickeys_dc(dcrs, mapping, params={})
    ret = {}
    department = dcrs[0].context[:JobSetup][:SiViewControlJob][:FOUP][0][:Lot][-1]['department']
    empty = params[:empty] || []
    sample_gkeys = dcrs.collect {|dcr| dcr.sample_generickeys(mapping: mapping, empty: empty)}
    mapping.each_pair {|k, v|
      # get extractor and data keys
      sil_gkeys = @sildb.generickey(active: 'Y', department: department, method: 'DC', parameter: v)
      ($log.warn "no DC generic key found for department #{department}, key #{v}: #{sil_gkeys.inspect}"; return) unless sil_gkeys
      # select extractor or data key
      sil_gkeys = sil_gkeys.select {|silk| silk.name.start_with?(k.start_with?('Reserve') ? 'GenericExKey' : 'GenericDataKey')}
      ($log.warn "no DC generic key found for department #{department}, key #{v}: #{sil_gkeys.inspect}"; return) if sil_gkeys.size < 1
      ($log.warn "multiple generic key entries found in DB for department #{department}, key #{v}: #{sil_gkeys.inspect}") if sil_gkeys.size > 1
      # since SIL 2.0.x the index matches the RespPD index
      idx = sil_gkeys[0].resppd
      if empty.member?(v)
        ret[k] = '-'
      elsif  idx < sample_gkeys.length
        ret[k] = sample_gkeys[idx][k]
      end
    }
    return ret
  end

  # Parse speclimits from Fab8 control plan, Fab8 only
  def speclimit_from_spec(params)
    return _data_from_spec(Space::SILDB::SpcSpecLimit.new, params)
  end

  # Parse speclimits from Fab1 equipment control plan, Fab1 only, currently not used
  def monitoringspeclimit_from_spec(params)
    return _data_from_monitoringspec(Space::SILDB::SpcMonitoringSpecLimit.new, params)
  end

  # Parse pdrefs from Fab8 control plan, Fab8 only
  def pdrefs_from_spec(params)
    return _data_from_spec(Space::SILDB::SpcPDRef.new, params)
  end

  # Parse autocharting from Fab8 control plan, Fab8 only
  def autocharting_from_spec(params)
    data = _data_from_spec(Space::SILDB::SpcAutoCharting.new, params)
    return data.find_all {|d| d.template != nil}
  end

  # Fab8 only
  def speclimit_from_file(params)
    return _data_from_file(Space::SILDB::SpcSpecLimit, params)
  end

  # Fab8 only
  def autocharting_from_file(params)
    return _data_from_file(Space::SILDB::SpcAutoCharting, params)
  end

  def chamberfilter_from_file(params)
    return _data_from_file(Space::SILDB::SpcChamberFilter, params)
  end

  def _data_from_file(templateobject, params)
    wb = RubyXL::Parser.parse(params[:filename])
    rows = wb[0].collect {|row| row ? row.cells.collect {|cell| cell && cell.value} : []}
    headers = rows[0].collect {|c| ExcelMapping[c]}
    res = []
    rows[1..-1].each do |cells|
      slimit = templateobject.new
      cells.each_with_index do |c, i|
        attrib = headers[i]
        if attrib
          if attrib == 'Generic'
            p = c.split('|')
            slimit.productgroup = p[0]
            slimit.product = p[1] if p[1]
          else
            slimit[attrib] = c if attrib
            slimit[attrib] = slimit[attrib].split.sort if attrib == :filter
          end
        end
        slimit.target = slimit.target.to_f if slimit.members.member?(:target)
      end
      sms = slimit.members
      if sms.member?(:product) && sms.member?(:productgroup) && sms.member?(:route) && sms.member?(:technology)
        res += _split_up_entries_techroute(slimit)
      else
        res << slimit
      end
    end
    return res
  end

  def _split_up_entries_techroute(e)
    entries = []
    _techs = e[:technology] == nil ? [nil] : e[:technology].gsub(",", " ").split
    _routes = e[:route] == nil ? [nil] : e[:route].gsub(",", " ").split
    _productgroup = e[:productgroup] == nil ? [nil] : e[:productgroup].gsub(",", " ").split
    _product = e[:product] == nil ? [nil] : e[:product].gsub(",", " ").split
    _techs.each do |t|
      t_tmp = e.clone
      t_tmp[:technology] = t
      _routes.each do |r|
        r_tmp = t_tmp.clone
        r_tmp[:route] = r
        _productgroup.each do |pg|
          pg_tmp = r_tmp.clone
          pg_tmp[:productgroup] = pg
          _product.each do |p|
            p_tmp = pg_tmp.clone
            p_tmp[:product] = p
            entries << p_tmp
          end
        end
      end
    end
    $log.debug {"entries: #{entries}"}
    return entries
  end

  # Read a generic key excel file
  def generickey_from_file(params)
    wb = RubyXL::Parser.parse(params[:filename])
    rows = wb[0].collect {|row| row ? row.cells.collect {|cell| cell && cell.value} : []}
    headers = rows[0].collect {|c| ExcelMapping[c]}
    res = []
    rows[1..-1].each_with_index do |cells, j|
      gkey = Space::SILDB::SpcGenericKey.new
      gkey.keyid = j
      content = []
      cells.each_with_index do |c, i|
        attrib = headers[i]
        if attrib
          if attrib == 'Content'
            content << c
          else
            gkey[attrib] = c
          end
        else
          $log.error "wrong entry #{c.inspect}"
        end
      end
      if gkey.method == 'GROUP'
        gkey.ldsvalue = content[1]
        gkey.group = _pattern_sub(content[2], content[3])
      elsif gkey.method == 'FIXED'
        gkey.fixed_property = content[0]
      elsif gkey.method == 'DC'
        gkey.resppd = content[1].to_i
        gkey.parameter = content[2]
      else
        if cells[0].nil?
          # merged cell, update latest saved key
          res[-1].group.merge!(_pattern_sub(content[2], content[3]))
          $log.error "wrong entry in row #{j}" if gkey.lds || gkey.module || gkey.method || gkey.name || gkey.ldsvalue
          next
        else
          $log.error "unknown method: #{gkey.method}"
        end
      end
      res << gkey
    end
    return res
  end

  # Replacement for regex
  def _pattern_sub(pattern, substitutions)
    pattern.gsub!('.', '\.')
    pattern.gsub!('?', '.')
    pattern.gsub!('*', '.*')
    p = pattern.split("\n")
    s = substitutions.split("\n")
    h = {}
    s.each_with_index do |svalue, i|
      next unless p[i]
      v = p[i].split(',')
      h[svalue] = v.collect {|e| "^#{e}$"}
    end
    return h
  end

  # Find the column by name in the spec and return its tag
  def _get_column(doc, name)
    $log.debug {"get column #{name}"}
    cols = name.split(',').collect do |n|
      _col = doc[3].elements["//my:Column[./my:Name='" + n + "']"] || ($log.error "#{n} not found!"; return)
      _col_i = _col.index_in_parent
      $log.debug("#{n}, #{_col}")
      "./my:Col_#{_col_i}"
    end
    return (cols.length == 1) ? cols.first : cols
  end

  # Get the element for an row based on the column query, multiple elements are comma separated
  #
  # return nil or list
  def _get_element(xml_root, col, can_split)
    return unless xml_root.kind_of?(REXML::Element)
    res = col ? xml_root.elements[col].text : nil
    return unless res
    can_split ? res.gsub(',', ' ').split : [res.strip]
  end

  # extract spec data for Fab 8 specs, Fab8 only
  # the spec has (at least) 2 tables:
  #   * generic table, valid for all entries
  #   * parameter tables, with entries per parameter
  def _data_from_spec(templateobject, params)
    #content,version = @sfc.get_specification(params[:specid], params[:specversion])
    content = @sfc.spec_content(params[:specid], version: params[:specversion])
    doc = REXML::Document.new(content)
    # Determine type
    subclass = doc[3].elements["//my:SubClass"].text
    #table1 = doc[3].elements["//my:Table[@my:TableUID='1']/my:Rows/my:Row"]
    #table2 = doc[3].elements["//my:Table[@my:TableUID='2']/my:Rows"]
    # Find generic table
    #tables = doc[3].elements["//my:Table"]
    generic = doc[3].elements["//my:Table[@my:TableUID='1']/my:Rows/my:Row"]
    # Create a template object first
    tmp = templateobject
    valid_attr = tmp.members
    # Preinitialize values
    cp_map = SpecCpMapping[subclass] || ($log.error "Could not determin plan #{subclass}"; return)
    row_cols = Hash.new
    cp_map.each_key do |k|
      t1,name,splitted = cp_map[k]
      if valid_attr.member?(k)
        if t1
          _xcol = _get_column(doc, name)
          $log.debug {"Get data for #{name}, #{k}, #{_xcol}"}
          tmp[k] = _get_element(generic, _xcol, splitted)
          tmp[k] = tmp[k][0] if tmp[k] and tmp[k].length == 1 and !splitted
          $log.debug {"#{k}, #{tmp[k]}"}
        else
          row_cols[k] = [_get_column(doc, name), splitted]
        end
      end
    end
    # Find row values and create new entries
    entries = []
    doc[3].each_element("//my:Table[@my:TableUID!='1']/my:Rows") do |table|
      table.each do |row|
        e = tmp.clone
        row_cols.each do |k,col_data|
          col,splitted = col_data
          if col.is_a?(Array)
            e[k] = []
            col.each { |c| elem = _get_element(row, c, splitted); e[k] << elem if elem }
          else
            e[k] = _get_element(row, col, splitted)
            e[k] = e[k][0] if (e[k] and e[k].length == 1 and !splitted)
            e[k] = [e[k]] if k == :pdrefs and e[k]
          end
          e.usl = e.usl.to_f if k == :usl and e.usl
          e.lsl = e.lsl.to_f if k == :lsl and e.lsl
          e.target = e.target.to_f if k == :target
          e.nsites = e.nsites.to_f if k == :nsites
        end
        # for PDreference ignore empty pd references as they are not stored in the DB
        _membs = e.members
        next if (_membs.member?(:pdrefs) and (not e[:pdrefs] or e[:pdrefs] == []))
        # If the control plan has an attribute :routes, split up the entries if necessary
        if _membs.member?(:route) and _membs.member?(:product) and _membs.member?(:productgroup)
          entries += _split_up_entries(e)
        elsif _membs.member?(:productgroup) and _membs.member?(:product)
          entries += _split_up_entries_noroute(e)
        else
          entries << e
        end
      end
    end
    return entries
  end

  # extract spec data for Fab 1 specs, Fab1 only, currently not used
  # the spec has only one table:
  #   * parameter tables, with entries per parameter
  def _data_from_monitoringspec(templateobject, params)
    ##content,version = @sfc.get_specification(params[:specid], params[:specversion])
    content = @sfc.spec_content(params[:specid], version: params[:specversion])
    doc = REXML::Document.new(content)
    # Determine type
    subclass = doc[3].elements["//my:SubClass"].text
    # Create a template object first
    tmp = templateobject
    valid_attr = tmp.members
    # Preinitialize values
    cp_map = SpecCpMapping[subclass]
    ($log.error "Could not determin plan #{subclass}"; return) unless cp_map
    row_cols = Hash.new
    cp_map.each_key do |k|
      name,splitted = cp_map[k]
      if valid_attr.member?(k)
        row_cols[k] = [_get_column(doc, name), splitted]
      end
    end
    # Find row values and create new entries
    entries = []
    doc[3].each_element("//my:Table[@my:TableUID='1']/my:Rows") do |table|
      table.each do |row|
        e = tmp.clone
        row_cols.each do |k,col_data|
          col,splitted = col_data
          if col.is_a?(Array)
            e[k] = []
            col.each { |c| elem = _get_element(row, c, splitted); e[k] << elem if elem }
          else
            e[k] = _get_element(row, col, splitted)
            e[k] = e[k][0] if (e[k] and e[k].length == 1 and !splitted)
            e[k] = [e[k]] if k == :pdrefs and e[k]
          end
          e.usl = e.usl.to_f if k == :usl and e.usl
          e.lsl = e.lsl.to_f if k == :lsl and e.lsl
          e.target = e.target.to_f if k == :target
          e.nsites = e.nsites.to_f if k == :nsites
        end
        # for PDreference ignore empty pd references as they are not stored in the DB
        entries << e
      end
    end
    return entries
  end

  # Split up multiple items
  def _split_up_entries(e)
    entries = []
    _routes = e[:route].nil? ? [nil] : e[:route]
    _products = e[:product].nil? ? [nil] : e[:product]
    _productgroups = e[:productgroup].nil? ? [nil] : e[:productgroup]
    _routes.each do |r|
      r_tmp = e.clone
      r_tmp[:route] = r
      _products.each do |p|
        p_tmp = r_tmp.clone
        p_tmp[:product] = p
        _productgroups.each do |pg|
          pg_tmp = p_tmp.clone
          pg_tmp[:productgroup] = pg
          entries << pg_tmp
        end
      end
    end
    $log.debug "list = #{entries.inspect}"
    return entries
  end

  def _split_up_entries_noroute(e)
    entries = []
    _products = e[:product].nil? ? [nil] : e[:product]
    _productgroups = e[:productgroup].nil? ? [nil] : e[:productgroup]
    _products.each do |p|
      p_tmp = e.clone
      p_tmp[:product] = p
      _productgroups.each do |pg|
        pg_tmp = p_tmp.clone
        pg_tmp[:productgroup] = pg
        entries << pg_tmp
      end
    end
    $log.debug "list = #{entries.inspect}"
    return entries
  end

  def key_from_generickey(gkeys, params={})
    compose_key_from_entries(gkeys, [:lds, :module, :name, :method], [:ldsvalue, :resppd, :parameter, :fixed_property, :group])
  end

  def key_from_chamberfilter(ckeys, params={})
    compose_key_from_entries(ckeys, [:lds, :ptool], [:filter])
  end

  def key_from_speclimit(limits, params={})
    compose_key_from_entries(limits, [:parametername, :pd, :product, :productgroup], [:usl,:lsl,:target,:unit,:criticality,:module,:operationnumber,:customercriticality])
  end

  def key_from_monitoringspeclimit(limits, params={})
    compose_key_from_entries(limits, [:parameter, :mpd, :technology, :route, :productgroup, :product, :mtool, :mslot, :mlogrecipe,
        :ppd, :plogrecipe, :ptool, :pchamber, :event, :sapinspectioncharacteristic, :reticle, :photolayer,
        :reserve1, :reserve2, :reserve3, :reserve4, :reserve5, :reserve6, :reserve7, :reserve8,
        :datreserve1, :datreserve2, :datreserve3, :datreserve4, :datreserve5, :datreserve6, :datreserve7, :datreserve8], [:usl,:lsl,:target,:unit,:criticality])
    end

  def key_from_speclimit_ext(limits, params={})
    compose_key_from_entries(limits, [:parametername, :pd, :product, :productgroup, :route, :technology], [:usl,:lsl,:target,:unit,:criticality,:module,:operationnumber,:customercriticality])
  end

  def key_from_pdrefs(pdrefs, params={})
    add_valkeys = params[:add_valkeys] || []
    compose_key_from_entries(pdrefs, [:parameter, :mpd, :product, :productgroup], [:pdrefs] + add_valkeys)
  end

  def key_from_autocharting(autocharting, params={})
    compose_key_from_entries(autocharting, [:parameter, :module, :lds], [:template])
  end

  # Generate a fingerprint of keys from a hash and a fingerprint of values into a comparison hash
  def compose_key_from_entries(entries, keys, values)
    hash = {}
    entries.each do |l|
      key= keys.reduce(":") { |key, k| key +="#{l[k]}:" }
      value = values.inject([]) { |val, v|
        _v = l[v]
        if _v.is_a? Array
          val = _v.map {|pdrefs| pdrefs.is_a?(Array) ? pdrefs.sort : pdrefs}
        elsif _v.is_a? Hash
          val << _v
        else
          _v.capitalize! if (_v && [:criticality].member?(v)) # Special columns are autoreplaced
          val << _v.inspect
        end
      }
      if key =~ /^:::+/
        $log.warn "Drop key: #{key} not well defined."
      elsif hash.has_key? key
        $log.debug "Duplicate key found: #{key}"
      else
        # Insert the value in the hash table unless all entries are empty
        hash[key] = value if key.delete(":") != "" and value.delete(":") != ""
      end
    end
    return hash
  end

  # currently only :filename and :comment are supported (generic key is read from file)
  def verify_generickey(params)
    return compare_content(:key_from_generickey, :generickey, :generickey_from_file, params) if params[:filename] and params[:comment]
  end

  # currently only :filename and :comment are supported (chamberfilter key is read from file)
  def verify_chamberfilter(params)
    return compare_content(:key_from_chamberfilter, :chamberfilter, :chamberfilter_from_file, params) if params[:filename] and params[:comment]
  end

  # currently only :specid and :specversion are supported
  def verify_speclimit(params)
    return compare_content(:key_from_speclimit, :speclimit, :speclimit_from_spec, params) if params[:specid] and params[:specversion]
    return compare_content(:key_from_speclimit_ext, :speclimit, :speclimit_from_file, params) if params[:filename] and params[:comment]
  end

  # currently only :specid and :specversion are supported, Fab1 only, currently not used
  def verify_monitoringspeclimit(params)
    return compare_content(:key_from_monitoringspeclimit, :monitoringspeclimit, :monitoringspeclimit_from_spec, params) if params[:specid] and params[:specversion]
  end

  # currently only :specid and :specversion are supported, Fab8 only
  def verify_autocharting(params)
    return compare_content(:key_from_autocharting, :autocharting, :autocharting_from_spec, params) if params[:specid] and params[:specversion]
    return compare_content(:key_from_autocharting, :autocharting, :autocharting_from_file, params) if params[:filename] and params[:comment]
  end

  # currently only :specid and :specversion are supported, Fab8 only
  def verify_pdrefs(params)
    compare_content(:key_from_pdrefs, :pd_reference, :pdrefs_from_spec, params.merge(all: true))
  end

  # Fab8 only
  def verify_spec(params)
    res = verify_speclimit(params)
    res &= verify_autocharting(params)
    res &= verify_pdrefs(params)
    return res
  end

  # Compares the values according to specific keys, internally used only
  def compare_content(key_func, db_key_func, from_spec_func, params)
    db_results = @sildb.send(db_key_func, params)
    ret = true
    if active = params[:all_active] != nil  # parameter is set to true or false
      # check if all values are 'Y' or 'N'
      val = active ? 'Y' : 'N'
      res = db_results.inject(true) {|res, row| res && (row.active == val)}
      $log.error "Not all values are #{active ? 'activated' : 'deactivated'}!" unless res
      ret &= res
    end
    db_hash = send(key_func, db_results, params)
    sp_hash = send(key_func, send(from_spec_func, params), params)
    ret &= verify_data(db_hash, sp_hash)
    return ret
  end

  # Check that no data exists for this spec + version, Test_Space08 (Fab8) only
  def verify_no_spec_data(params)
    res = true
    ($log.warn " speclimit table not empty!"; res = false) unless @sildb.speclimit(params) == []
    ($log.warn " autocharting table not empty!"; res = false) unless @sildb.autocharting(params) == []
    ($log.warn " pd references not empty!"; res = false) unless @sildb.pd_reference(params) == []
    return res
  end

  # assign corrective actions to one or more channels and verify, return true on success
  def assign_verify_cas(channels, actions)
    channels = [channels] unless channels.kind_of?(Array)
    actions = [actions] unless actions.kind_of?(Array)
    channels.each {|ch|
      @spc.set_corrective_actions(ch, actions)
      val = ch.valuations.first
      ($log.warn "no channel valuations defined, check channel and template"; return false) unless val
      aa = @spc.corrective_actions(val).collect {|a| a.name}
      ($log.warn "error setting corrective actions"; return false) if actions.sort != aa.sort
      ch.set_state('Offline')
    }
    return true
  end

  # read all aqv messages from the queue, find one that matches the test and verify, return true on success
  def verify_aqv_message(params={})
    exporttag = params[:exporttag] || caller_locations(1, 1)[0].label.tr('<>', '')
    export = "log/#{exporttag}_aqv.xml"
    lot = params[:lot]
    wafer = params[:wafer]
    result = params[:result] || 'success'
    comment = params[:comment] || ''
    reportingTimeStamp = params[:dcr].context['reportingTimeStamp']
    $log.info "verify_aqv_message #{params}"
    msgs = params[:msgs]  # for multiple lot checks
    unless msgs
      sleep 120
      msgs = @mqaqv.read_msgs  #(params)
    end
    ($log.warn "  no SIL->AQV message found"; return) unless msgs && !msgs.empty?
    msgs.reverse.each {|m|
      r = xml_to_hash(m)
      next if r[:event][:timestamp] < reportingTimeStamp
      ($log.info "  wrong lot: #{r[:event][:lotId]}"; next) if lot != r[:event][:lotId]
      if r[:event][:wafers] && r[:event][:wafers][:scribe]
        ($log.info "  wrong wafers: #{r[:event][:wafers][:scribe].sort}"; next) if wafer != r[:event][:wafers][:scribe].sort
        #($log.info "  wrong wafers: #{r[:event][:wafers][:scribe]}, expected: #{wafer}"; next) if wafer != r[:event][:wafers][:scribe]
      end
      if r[:event][:result] == 'success'
        if result == r[:event][:result] && r[:event][:comment] == nil
          $log.debug "msg found:\n#{r.pretty_inspect}"
          File.binwrite(export, m)
          return true
        end
      else
        ($log.info "  wrong comment: #{r[:event][:comment]}"; next) if !r[:event][:comment].include?(comment)
        if result == r[:event][:result] && r[:event][:comment].include?(comment)
          $log.debug "msg found:\n#{r.pretty_inspect}"
          File.binwrite(export, m)
          return true
        end
      end
      $log.warn " wrong result: #{r[:event][:result]}"
      File.binwrite(export, m)
      return false
    }
    $log.warn "  no suitable aqv message found. SIL->AQV messages:\n#{msgs.pretty_inspect}"
    return false
  end

  # compare current MM server env vars with a reference list (array of hashes or file name or nil for default file)
  #
  # return true on match
  def verify_sil_properties(ref=nil, excluded_keys=[])
    $log.info "verify_sil_properties"
    ($log.warn "  no connection to SIL server"; return) unless @silserver
    ref ||= File.join('./testparameters', @env, 'space/advancedprops.yaml')
    ref = YAML.load_file(ref) if ref.kind_of?(String)
    verify_hash(ref, @silserver.read_jobprops, nil, exclude: excluded_keys)
  end

  def readspiseppprops
    @silserver.read_spiseppprops
  end
end
