=begin
Test WebMRB

(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.
    

Author:     dsteger, 2012-10-08
Version:    1.0.0
=end

require 'time'
require 'builder'
require 'rqrsp'

# Test WebMRB
module WebMRB

  class Web < RequestResponse::HTTP
    def initialize(env, url)
      super("webmrb.#{env}", :url=>url)
      @env = env
    end
    
    def get_mrb()
      _submit_request("GET")
    end
    
    
  end
  
  # Test WebMRB
  class Client < RequestResponse::MQ
    
    attr_accessor :env, :request, :response, :mq_request, :mq_response
    
    def initialize(env, params={})
      super("webmrb.#{env}", params)
      @env = env      
    end
    
    def autoraise(params={})
      xml = '<?xml version="1.0" encoding="UTF-8" standalone="yes"?><autoRaiseInfo xmlns:ns2="http://tempuri.org/"><UserName>X-WEBMRB</UserName><Password>CSNCFX.Y</Password><Fab>FAB 1</Fab><OriginatorID>SPACE</OriginatorID><OwnerID>cstenzel</OwnerID><OwnerDept>CFM</OwnerDept><NCDetectedDate>2014-02-12T14:27:37.579+01:00</NCDetectedDate><Equipment>UTC001</Equipment><ProblemDescription>DEPT-&gt;CFM, [(78669:2) RUN_ID-&gt;Raw below specification, CH_ID-&gt;75850, CH_NAME-&gt;QA_eCAP_900/3260C/QA_MB-FTDEP.01, CKC_ID-&gt;1, CKC_NAME-&gt;UTC001, SAMPLE_ID-&gt;7001820099, SAMPLE_VALUES-&gt;{10.0}]</ProblemDescription><ProblemSpec>2:Raw below specification</ProblemSpec><ActualData>[SAMPLE_ID-&gt;7001820099, SAMPLE_VALUES-&gt;{10.0}]</ActualData><AssessImpact>inline</AssessImpact><LotDetails><LotInfo><LotID>UFE-SOOC_14.00</LotID><Status>affected</Status><AffectedLotWaferInfos><WaferInfo><ns2:SlotNumber>1</ns2:SlotNumber><SlotNumberSpecified>true</SlotNumberSpecified><WaferAlias>01</WaferAlias><WaferId>WWHR902FL601</WaferId><CriteriaInfoInfos><CriteriaInfo><ViolationID>2</ViolationID><Parameter>QA_eCAP_900</Parameter><SubLotType>PO</SubLotType><Technology>45NM</Technology><Criticality>Monitoring</Criticality><Route>SIL-0001.01</Route><ProductGroup>3260C</ProductGroup><ChannelID>75850</ChannelID></CriteriaInfo></CriteriaInfoInfos></WaferInfo></AffectedLotWaferInfos><AffectedProcessInfos><LotOperationInfo><OpId>QA_MB-FTDEP.01</OpId><OpName>DEFECT REVIEW</OpName><OpNumber>2500.6000</OpNumber><RouteId>SIL-0001.01</RouteId></LotOperationInfo></AffectedProcessInfos></LotInfo><LotInfo><LotID>UFE-SOOC_14.00</LotID><Status>suspect</Status><AffectedLotWaferInfos><WaferInfo><SlotNumberSpecified>false</SlotNumberSpecified><WaferAlias>01</WaferAlias><WaferId>WWHR902FL601</WaferId></WaferInfo><WaferInfo><SlotNumberSpecified>false</SlotNumberSpecified><WaferAlias>02</WaferAlias><WaferId>WWHR902FL602</WaferId></WaferInfo><WaferInfo><SlotNumberSpecified>false</SlotNumberSpecified><WaferAlias>03</WaferAlias><WaferId>WWHR902FL603</WaferId></WaferInfo><WaferInfo><SlotNumberSpecified>false</SlotNumberSpecified><WaferAlias>04</WaferAlias><WaferId>WWHR902FL604</WaferId></WaferInfo><WaferInfo><SlotNumberSpecified>false</SlotNumberSpecified><WaferAlias>05</WaferAlias><WaferId>WWHR902FL605</WaferId></WaferInfo><WaferInfo><SlotNumberSpecified>false</SlotNumberSpecified><WaferAlias>06</WaferAlias><WaferId>WWHR902FL606</WaferId></WaferInfo><WaferInfo><SlotNumberSpecified>false</SlotNumberSpecified><WaferAlias>07</WaferAlias><WaferId>WWHR902FL607</WaferId></WaferInfo><WaferInfo><SlotNumberSpecified>false</SlotNumberSpecified><WaferAlias>08</WaferAlias><WaferId>WWHR902FL608</WaferId></WaferInfo><WaferInfo><SlotNumberSpecified>false</SlotNumberSpecified><WaferAlias>09</WaferAlias><WaferId>WWHR902FL609</WaferId></WaferInfo><WaferInfo><SlotNumberSpecified>false</SlotNumberSpecified><WaferAlias>10</WaferAlias><WaferId>WWHR902FL610</WaferId></WaferInfo><WaferInfo><SlotNumberSpecified>false</SlotNumberSpecified><WaferAlias>11</WaferAlias><WaferId>WWHR902FL611</WaferId></WaferInfo><WaferInfo><SlotNumberSpecified>false</SlotNumberSpecified><WaferAlias>12</WaferAlias><WaferId>WWHR902FL612</WaferId></WaferInfo><WaferInfo><SlotNumberSpecified>false</SlotNumberSpecified><WaferAlias>13</WaferAlias><WaferId>WWHR902FL613</WaferId></WaferInfo><WaferInfo><SlotNumberSpecified>false</SlotNumberSpecified><WaferAlias>14</WaferAlias><WaferId>WWHR902FL614</WaferId></WaferInfo><WaferInfo><SlotNumberSpecified>false</SlotNumberSpecified><WaferAlias>15</WaferAlias><WaferId>WWHR902FL615</WaferId></WaferInfo><WaferInfo><SlotNumberSpecified>false</SlotNumberSpecified><WaferAlias>16</WaferAlias><WaferId>WWHR902FL616</WaferId></WaferInfo><WaferInfo><SlotNumberSpecified>false</SlotNumberSpecified><WaferAlias>17</WaferAlias><WaferId>WWHR902FL617</WaferId></WaferInfo><WaferInfo><SlotNumberSpecified>false</SlotNumberSpecified><WaferAlias>18</WaferAlias><WaferId>WWHR902FL618</WaferId></WaferInfo><WaferInfo><SlotNumberSpecified>false</SlotNumberSpecified><WaferAlias>19</WaferAlias><WaferId>WWHR902FL619</WaferId></WaferInfo><WaferInfo><SlotNumberSpecified>false</SlotNumberSpecified><WaferAlias>20</WaferAlias><WaferId>WWHR902FL620</WaferId></WaferInfo><WaferInfo><SlotNumberSpecified>false</SlotNumberSpecified><WaferAlias>21</WaferAlias><WaferId>WWHR902FL621</WaferId></WaferInfo><WaferInfo><SlotNumberSpecified>false</SlotNumberSpecified><WaferAlias>22</WaferAlias><WaferId>WWHR902FL622</WaferId></WaferInfo><WaferInfo><SlotNumberSpecified>false</SlotNumberSpecified><WaferAlias>23</WaferAlias><WaferId>WWHR902FL623</WaferId></WaferInfo><WaferInfo><SlotNumberSpecified>false</SlotNumberSpecified><WaferAlias>24</WaferAlias><WaferId>WWHR902FL624</WaferId></WaferInfo><WaferInfo><SlotNumberSpecified>false</SlotNumberSpecified><WaferAlias>25</WaferAlias><WaferId>WWHR902FL625</WaferId></WaferInfo></AffectedLotWaferInfos><AffectedProcessInfos><LotOperationInfo><OpId>QA_MB-FTDEP.01</OpId><OpName>DEFECT REVIEW</OpName><OpNumber>2500.6000</OpNumber><RouteId>SIL-0001.01</RouteId></LotOperationInfo></AffectedProcessInfos></LotInfo></LotDetails></autoRaiseInfo>'
      res = _send_receive(xml, params)
      return res
    end
        
    def get_lot_shipping_availability(lots, params={})
      $log.info "get_lot_shipping_availability #{lots.inspect}"

      xml = Builder::XmlMarkup.new
      xml.instruct!
      xml.tag! "SOAP-ENV:Envelope", 
        "xmlns:SOAP-ENV"=>"http://schemas.xmlsoap.org/soap/envelope/",
        "xmlns:tns"=>"http://tempuri.org/",
        "xmlns:ns1"=>"http://schemas.microsoft.com/2003/10/Serialization/Arrays" do |envelope|
        envelope.tag! "SOAP-ENV:Body" do |body|
          body.tns :GetLotShippingAvailability do |a|
            a.tns :lots do |lot|
              lots.each do |l|
                lot.ns1 :string, l
              end
            end
          end
        end
      end
      res = _send_receive(xml.target!, params)
      kv = res[:GetLotShippingAvailabilityResponse][:GetLotShippingAvailabilityResult][:ArrayOfKeyValueOfstringArrayOfMRBLotReportSyc7bmur][:KeyValueOfstringArrayOfMRBLotReportSyc7bmur]
      kv = [kv] unless kv.kind_of?(Array)
      resp_hash = {}
      kv.each do |e|
        resp_hash[e[:Key]] = e[:Value]
      end
      return resp_hash
    end     


#    def autoraise(lots, params={})
#      $log.info "autoraise #{lots.inspect}"
#
#
#
#      xml = Builder::XmlMarkup.new
#      xml.instruct!
#      xml.tag! "SOAP-ENV:Envelope",
#        "xmlns:SOAP-ENV"=>"http://schemas.xmlsoap.org/soap/envelope/",
#        "xmlns:tns"=>"http://tempuri.org/",
#        "xmlns:ns1"=>"http://schemas.microsoft.com/2003/10/Serialization/Arrays" do |envelope|
#        envelope.tag! "SOAP-ENV:Body" do |body|
#          body.tns :autoRaiseInfo do |a|
#            a.tns :UserName, (params[:user] or "X-SIL")
#            a.tns :Password, passwd
#            a.tns :Fab, fab
#            a.tns :OriginatorID, "QA Test"
#            a.tns :OwnerID, owner
#            a.tns :OwnerDept, dept
#            a.tns :NCDetectedDate, date
#            a.tns :Equipment, eqp
#            a.tns :ProblemDescription, desc
#            a.tns :ProblemSpec
#            ActualData
#            AssessImpact
#            a.tns :LotDetails do |l|
#              lots.each do |lot|
#                lot.ns1 :LotInfo do |nl|
#                  nl.tns :LotID, lot
#                  nl.tns :AffectedLotWaferInfos
#                    <WaferInfo>
#                    <ns2:SlotNumber>1</ns2:SlotNumber>
#                    <SlotNumberSpecified>true</SlotNumberSpecified>
#                    <WaferAlias>01</WaferAlias>
#                    <WaferId>SIL030201000</WaferId>
#                    <CriteriaInfoInfos>
#                      <CriteriaInfo>
#                        <ViolationID>1</ViolationID>
#                        <Parameter>QA_MEMO_904</Parameter>
#                        <SubLotType>MW</SubLotType>
#                        <Technology>32NM</Technology>
#                        <Criticality>Monitoring</Criticality>
#                        <Route>SIL-0001.01</Route>
#                        <ProductGroup>QAPG1</ProductGroup>
#                        <ChannelID>567225</ChannelID>
#                      </CriteriaInfo>
#                    </CriteriaInfoInfos>
#                  </WaferInfo>
#                  <AffectedProcessInfos>
#                  <LotOperationInfo>
#                    <OpId>QA_CLAIM-PROCESS.01</OpId>
#                    <OpName>DEFECT REVIEW</OpName>
#                    <OpNumber>3200.1860</OpNumber>
#                    <RouteId>SIL-0001.01</RouteId>
#                  </LotOperationInfo>
#                </AffectedProcessInfos>
#                end
#                lot.ns1 :LotInfo, l
#                lot.ns1 :LotInfo, l
#                lot.ns1 :LotInfo, l
#                lot.ns1 :LotInfo, l
#
#              end
#            end
#          end
#        end
#      end
#      res = _send_receive(xml.target!, params)
#      kv = res[:GetLotShippingAvailabilityResponse][:GetLotShippingAvailabilityResult][:ArrayOfKeyValueOfstringArrayOfMRBLotReportSyc7bmur][:KeyValueOfstringArrayOfMRBLotReportSyc7bmur]
#      kv = [kv] unless kv.kind_of?(Array)
#      resp_hash = {}
#      kv.each do |e|
#        resp_hash[e[:Key]] = e[:Value]
#      end
#      return resp_hash
#    end
  end

end

