=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author:     ssteidte, 2013-03-05

Version:    0.1.0

History:
  2016-09-05 sfrieske,  removed smfacade
=end

require 'siview'
require 'mqjms'
require 'xmlhash'


# ICADA RSM change message handling
#
# The messages are sent by the MDS2MQBridge to the RSM MQ Listener
module RSM
  ChangeMsg = Struct.new(:change_type, :object_type, :object_id, :claim_time, :user)
  ChangeSpMsg = Struct.new(:change_type, :object_type, :object_id, :sp_name, :sp_value, :claim_time, :user)

  class Test
    include XMLHash
    attr_accessor :env, :sv, :smfacade, :q, :read_delay, :msgs_raw, :msgs

    def initialize(env, params={})
      @env = env
      ##@smfacade = SiView::SMFacade.new(env, params)
      @sv = params[:sv] || SiView::MM.new(env, params)
      @q = MQ::JMS.new("rsm.#{env}", params)
      @read_delay = params[:read_delay] || 60
    end

    # clean up old data
    def cleanup(pod)
      $log.info("#{__method__} #{pod}")
      rps = @sv.reticle_pod_status(pod)
      rps.reticles.each {|r|
        res = @sv.reticle_justinout(r.reticle, pod, r.slot, 'out')
        return nil if res != 0
      }
      @sv.reticle_pod_multistatus_change(pod, 'AVAILABLE') if rps.status != 'AVAILABLE'
      eqi = @sv.eqp_info(rps.eqp)
      @sv.reticle_pod_offline_unload(rps.eqp, eqi.reticleports[0].port, pod) if rps.eqp != '' and rps.xfer_status == 'EI'
      if rps.stocker != '' and rps.xfer_status == 'SI'
        brs = @sv.brs_info(rps.stocker)
        if brs
          _rp = brs.ports.find {|rp| rp.loaded_pod == pod}
          @sv.reticle_pod_unload(pod, brs: rps.stocker, rsc: _rp.port) if _rp
        end
      end
    end

    # clean up old data
    def cleanup_reticle(reticle)
      $log.info("cleanup_reticle #{reticle.inspect}")
      _rcl = @sv.reticle_status(reticle)
      if _rcl.xfer_status == "EI"
        return @sv.reticle_xfer_status_change(reticle, 'EO', eqp: _rcl.eqp)
      elsif _rcl.pod != ""
        _rps = @sv.reticle_pod_status(_rcl.pod)
        _rinfo = _rps.reticles.find {|rp| rp.reticle == reticle}
        if _rinfo
          return @sv.reticle_justinout(reticle, _rcl.pod, _rinfo.slot, 'out')
        else
          return true
        end
      else
        return true
      end
    end

    # ensure reticle is in a pod, not stored in an eqp
    def prepare_reticle(reticle, pod, slot, eqp)
      $log.info("prepare_reticle #{reticle.inspect}, #{pod.insepct}, #{slot.inspect}, #{eqp.inspect}")
      @sv.reticle_justinout(reticle, pod, slot, "in")
      rs = @sv.reticle_status(reticle)
      rps = @sv.reticle_pod_status(pod)
      eqi = @sv.eqp_info(eqp)
      @sv.reticle_pod_xfer_status_change(pod, "EI", eqp: rs.eqp) # gives no error if already EI
      @sv.reticle_pod_xfer_status_change(pod, "EO", eqp: rs.eqp)
      reticleport = eqi.reticleports[0]
      @sv.reticle_pod_offline_load(eqp, reticleport.port, pod) unless reticleport.loaded_pod == pod
      @sv.reticle_offline_retrieve(eqp, reticleport.port, pod, reticle, :slot=>slot) if rs.pod.empty?
#      @sv.reticle_pod_xfer_status_change(pod, "EO", eqp: rs.eqp)
      @sv.reticle_pod_offline_unload(eqp, reticleport.port, pod)
      return true
    end

    def read_msgs(params={})
      $log.info "read_msgs"
      ($log.info " waiting #{@read_delay}s for MDS"; sleep @read_delay) unless params[:delay] == false
      @msgs_raw = q.read_msgs
      mm = @msgs_raw.collect {|m| xml_to_hash(m)}
      @msgs = mm.collect {|m|
        if m.keys[0] == :SIVIEW_OBJECT_CHANGED
          ChangeMsg.new(*m[:SIVIEW_OBJECT_CHANGED].values)
        elsif m.keys[0] == :SIVIEW_SP_CHANGED
          ChangeSpMsg.new(*m[:SIVIEW_SP_CHANGED].values)
        end
      }.compact
      q.delete_msgs(nil) unless params[:delete] == false
      return @msgs
    end

    # compare msgs with reference message
    #
    # return true on match
    def verify_msg(*args)
      params = args[-1]
      if params.kind_of?(Hash)
        args.pop
      else
        params = {}
      end
      ref = params[:sp] ? ChangeSpMsg.new(*args) : ChangeMsg.new(*args)
      $log.info "verify_msg #{ref.inspect}"
      ($log.warn "  no messages"; return nil) unless @msgs
      # find ref within stored msgs
      ret = false
      @msgs.each_with_index {|msg, i|
        next if msg.class.name != ref.class.name
        ok = true
        ref.members.each {|m| (ok = false; break) if ref[m] and ref[m] != msg[m]}
        (ret = true; @msgs.delete_at(i); break) if ok
      }
      $log.warn "  no matching msg" unless ret
      return ret
    end

    def read_verify(*args)
      params = args[-1]
      params = {} unless params.kind_of?(Hash)
      read_msgs(params)
      verify_msg(*args)
    end

    def test(*args)
      params = args[-1]
      if params.kind_of?(Hash)
        args.pop
      else
        params = {}
      end
      puts "args: #{args.inspect}, params: #{params.inspect}"
    end
  end
end
