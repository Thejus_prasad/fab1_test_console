=begin 
Java RMI registry

Allows to create a rmiregistry and to connect to existing registries.

Note that web server EJBs use RMI-IIOP which is not compatible with 
plain RMI registries.

(c) Copyright 2011 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: Steffen Steidten, 2012-02-07
=end

require 'java'

# Java RMI registry
class RMIRegistry
  java_import 'java.rmi.registry.LocateRegistry' 
  
  attr_accessor :host, :port
    
  def initialize(params={})
    @host = params[:host] || 'localhost'
    @port = params[:port] || 1099
  end
  
  # create a registry on the local server
  def create_registry
##    java.lang.System.set_security_manager(java.rmi.RMISecurityManager.new)
    LocateRegistry.create_registry(@port)
    self
  end

  # return the remote registry
  def registry
    LocateRegistry.get_registry(@host, @port)
  end
  
  # bind or re-bind object
  def rebind(name, obj)
    registry.rebind(name, obj)
  end
  
  # unbind object and stop export
  def unbind(name, obj=nil)
    begin
      registry.unbind(name)
      java.rmi.server.UnicastRemoteObject.unexport_object(obj, true) if obj
      return true
    rescue
      return false
    end
  end
  
  # return an array of names
  def list
    registry.list.to_a
  end
  
  # return the class reference
  def lookup(name)
    registry.lookup(name)
  end
  
end

# sample implementation class, PingInterface.class must exist!
#unless Object.const_defined? 'PingImpl'
#  class PingImpl < java.rmi.server.UnicastRemoteObject
#    java_import 'PingInterface'
#    include PingInterface
#    
#    def ping
#      return Time.now.utc.iso8601
#    end
#  end
#end

def testrmi
  $rs = RMIRegistry.new(:port=>1088).create_registry
  $srv = PingImpl.new
  $rs.rebind("Ping", $srv)
  puts "\nPing Server ready."
  #
  $rc = RMIRegistry.new(:port=>1088)
  puts $rc.lookup("Ping").ping
  #
#  $rs.unbind("Ping", $srv)
end
