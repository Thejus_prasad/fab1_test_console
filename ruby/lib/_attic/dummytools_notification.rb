=begin
(c) 2009 GLOBALFOUNDRIES Inc. All Rights Reserved.

Author:   Steffen Steidten, 2012-02-15
Version:  1.2.5

History:
  2014-11-17 ssteidte, cleanup, use dummytools.conf
  2015-10-15 sfrieske, don't log 'get' commands, threaded execution for multiple instances (LET)
  2019-07-30 sfrieske, use String.new to create new empty strings
  2021-02-05 sfrieske, use instance_of instead of kind_of
  2021-08-26 sfrieske, split off EINotification and moved it to attic
=end

require 'socket'
require 'thread'


# Control DummyAMHS and JavaEIs
module Dummy

  # send xferJobComp reports to LETCL and Java EIs; currently only used in attic Test_LRNDM
  class EINotification
    require 'siview'

    attr_accessor :env, :jcscode, :eqp, :user, :password, :iorparser, :orb, :manager


    def initialize(env, eqp, params={})
      @env = env
      @eqp = eqp
      #
      flavour = params[:flavour] || ENV['_flavour'] || 'R15_custom'
      jcsfiles = "lib/#{flavour}/*jar"
      $log.debug "#{self.class.name} loading JCS files #{jcsfiles.inspect}"
      Dir[jcsfiles].each {|f| require f}
      @jcscode = com.amd.jcs.siview.code
      #
      pfile = params[:properties] || "etc/connections/#{@env}.properties"
      $log.info "properties #{pfile}"
      @_propsfile = File.join(Dir.pwd, pfile)
      @_props = read_properties(@_propsfile)
      #@_propsfile, @_props = get_properties(params[:properties] || "#{@env}.properties")
      @user = @_props[:username]
      @password = @_props[:password]
      # create default iorfile from SiView connection parameter
      iorfile = @_props[:iorfilename] || @_props[:iorfile]
      iorfile = "#{File.dirname(iorfile)}/#{eqp}.IOR" if iorfile
      iorfile = params[:iorfile] if params.has_key?(:iorfile)
      $log.info "IOR file #{iorfile.inspect}"
      #
      @iorparser = IORParser.new(params[:ior] || @_props[:ior] || iorfile)
      @orb = ORB.init([].to_java(:string), nil)
      obj = @orb.string_to_object(@iorparser.ior)
      @manager = @jcscode.TCSServiceManagerHelper.narrow(obj)
      @_user = user_struct(@user, @password, params)
    end

    def inspect
      "#<#{self.class.name} env: #{@env}, eqp: #{@eqp}>"
    end

    def user_struct(user, password, params={})
      newpw = params[:newpw] || ''
      function = params[:function] || ''
      clientNode = params[:client] || ''
      @jcscode.pptUser_struct.new(oid(user), password, newpw, function, clientNode, any)
    end

    # return SiView id or list of ids for a string or array of strings, resp.
    def oid(s)
      begin
        if s.instance_of?(String)
          return @jcscode.objectIdentifier_struct.new(s, '')
        else
          return s.collect {|e| @jcscode.objectIdentifier_struct.new(e, '')}.to_java(@jcscode.objectIdentifier_struct)
        end
      rescue
        $log.warn "cannot create objectIdentifier_struct for #{s.inspect}"
        return
      end
    end

    def any
      @orb.create_any
    end

    def xfer_complete(carrier, port, status='EI')
      xfers = [@jcscode.pptXferJobComp_struct.new(oid(carrier), oid(@eqp), oid(port), status, any)]
      res = @manager.TxLotCassetteXferJobCompRpt(@_user, xfers, 'QA Test')
      return res.strResult.returnCode.to_i
    end

  end

end
