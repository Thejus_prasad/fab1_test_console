=begin
Access QA test rules on an RTD server

(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.
    
Author: Steffen Steidten, 2012-03-19

Version: 1.0.4

History:
  2013-06-28 ssteidte,  added Java User Exit test rule support
  2014-03-25 ssteidte,  removed QaRuleAutoProc, superseeded by MdsWriteService#write_eqpwip
  2015-11-30 sfrieske,  removed QARuleFiles for combine_separate and dekitting
=end

require 'remote'


module RTD
  # QA rule input files

  QARuleFiles = {
    # obsolete :autoproc=>"qa_eqp_lot.apf",
    # obsolete :combine_separate=>"qa_cs_test.csv",
    # obsolete :skip_op=>"QA_JCAP_SKIP.csv",
    # obsolete :testwafer_grading=>"QA_JCAP_TW_STB.csv",
    # obsolete :dekitting=>"QA_SJM_DEKIT_FILE.csv",
    # juetest rules
    :juetest_getLotManager=>"qa_juetest_getLotManager.apf",
    :juetest_writeEqpNextTimeRtd=>"qa_juetest_writeEqpNextTimeRtd.apf",
    :juetest_writeEqpStandbySubstates=>"qa_juetest_writeEqpStandbySubstates.apf",
    :juetest_writeEqpWipAuto3=>"qa_juetest_writeEqpWipAuto3.apf",
    :juetest_writeFoupTransferRequest=>"qa_juetest_writeFoupTransferRequest.apf",
    :lot_manager_to_apc=>"send_lotManager_to_APC",
    :lot_manager_preview=>"lot_manager_preview"
  }
  
  # manipulate QA rule content
  #
  # sufficient for most use cases
  class QARule
    attr_accessor :env, :rtdserver, :rule, :file
    
    def initialize(env, params={})
      @env = env
      @rtdserver = RemoteHost.new("rtdserver.#{@env}")      
      @rule = params[:rule]
      path = (env == "f8stag") ? "/apf/apfhome/models/fab8/class" : "/apf/apf_itdc/models/fab1/class"
      @file = (params[:file] or File.join(path, QARuleFiles[@rule]))
      $log.warn "no QA rule file" unless @file
    end
    
    # return content of rule file or nil
    def read_file
      @rtdserver.reconnect
      @rtdserver.read_file(@file)
    end
    
    # write string or array of strings or array of arrays to file
    #
    # return true on success
    def write_file(data, params={})
      @rtdserver.reconnect
      if data.kind_of?(Array)
        lines = data.collect {|line|
          line = line.join("|") if line.kind_of?(Array)
          line.chomp
        }.join("\n")
      else
        lines = data
      end
      lines += "\n" unless lines.end_with?("\n")
      @rtdserver.write_file(lines, @file, params)
    end
    
    # get file and write it back with all lines commented out
    #
    # return true on success
    def deactivate_rule
      lines = read_file
      return nil unless lines
      write_file(lines.split("\n").collect {|l| l.start_with?('#') ? l : '#'+l})
    end
  end
  
  
  # MDS connections for testing RTD Java User Exits etc.
  class MDS
    require 'dbsequel'
    attr_accessor :env, :db
    
    def initialize(env, params={})
      @env = env
      @db = params[:db]
      @db = DB.new("mds.#{env}", :schema=>"MDS_ADMIN") unless @db
    end
    
    # for JUE
    def t_tool(params={})
      retstruct = Struct.new(:eqp, :location, :e10, :e10_id, :e10_substate, :user, :time, :wip_siview, :wip_wfr_auto3,
        :wip_wafer_non_auto3, :wip_lot_auto3, :wip_lot_non_auto3, :last_time_disp, :is_auto3_wip, :is_long_down, :next_time_disp, :is_inhibit)
      qargs = [(params[:tstart] or Time.now - 3600)]
      q = "SELECT EQP_ID, LOCATION, E10, E10_ID, E10_SUBSTATE, USER_ID, TIME, WIP_SIVIEW, WIP_WFR_AUTO3, WIP_WFR_NON_AUTO3,
        WIP_LOT_AUTO3, WIP_LOT_NON_AUTO3, LAST_TIME_DISP_RTD, IS_AUTO3_WIP, IS_LONG_DOWN, NEXT_TIME_DISP_RTD, IS_INHIBIT FROM MDS_ADMIN.T_TOOL "
      q += " WHERE TIME > ? "
      q, qargs = @db._q_restriction(q, qargs, "EQP_ID", params[:eqp])
      q, qargs = @db._q_restriction(q, qargs, "LOCATION", (params[:location] or params[:workarea]))
      q, qargs = @db._q_restriction(q, qargs, "USER_ID", params[:user])
      q += " ORDER BY TIME DESC"
      qargs.unshift(q)
      res = @db.select_all(*qargs)
      return nil unless res
      return res if params[:raw]
      return res.collect {|r| retstruct.new(*r)}
    end
    
    def t_trans_job(params={})
      retstruct = Struct.new(:carrier, :pid, :last_update, :state, :curr_type, :curr_eqp, :curr_port, :tgt_type, :tgt_eqp, :tgt_port, :cj, :info)
      qargs = [(params[:tstart] or Time.now - 3600)]
      q = "SELECT CAST_ID, PROCESS_ID, LAST_UPDATE, STATE, CURR_TYPE, CURR_LOC, CURR_PORT
        TARGET_TYPE, TARGET_LOC, TARGET_PORT, CTRLJOB_ID, TRANS_JOB_INFO FROM MDS_ADMIN.T_TRANS_JOB "
      q += " WHERE LAST_UPDATE > ? "
      q, qargs = @db._q_restriction(q, qargs, "CAST_ID", params[:carrier])
      q, qargs = @db._q_restriction(q, qargs, "CURR_LOC", params[:src])
      q, qargs = @db._q_restriction(q, qargs, "TARGET_LOC", params[:tgt])
      q += " ORDER BY LAST_UPDATE DESC"
      qargs.unshift(q)
      res = @db.select_all(*qargs)
      return nil unless res
      return res if params[:raw]
      return res.collect {|r| retstruct.new(*r)}
    end
  
    # for XOAK 
    def reticles_at_pd(pd, params={})
      retstruct = Struct.new(:technology, :product, :route, :pd, :opNo, :layer, :reticleset, :reticlegroup, :reticle)
      q = "SELECT PS.TECH_ID, PS.PRODSPEC_ID, FL.MAINPD_ID, FL.PD_ID, FL.OPE_NO, FL.PHOTO_LAYER, PS.RTCLSET_ID, RS.RTCLGRP_ID, RG.RTCL_ID
        FROM V_MAINPD_FLOW_EXT FL, V_PRODSPEC PS,
         (SELECT T1.RTCLSET_ID, T2.PHOTO_LAYER, T3.IDENTIFIER AS RTCLGRP_ID 
          FROM FRRTCLSET T1, FRRTCLSET_DF T2, FRRTCLSET_DF_RGRP T3
          WHERE T1.D_THESYSTEMKEY = T2.D_THESYSTEMKEY
            AND T2.D_THESYSTEMKEY = T3.D_THESYSTEMKEY
           AND TO_CHAR(T2.D_SEQNO) = T3.D_THETABLEMARKER) RS,
         (SELECT T1.DRBLGRP_ID AS RTCLGRP_ID, T2.DRBL_ID AS RTCL_ID 
          FROM FRDRBLGRP T1, FRDRBLGRP_DRBL T2 WHERE T1.D_THESYSTEMKEY = T2.D_THESYSTEMKEY) RG
        WHERE FL.MAINPD_ID = PS.MAINPD_ID
          AND PS.RTCLSET_ID = RS.RTCLSET_ID
          AND FL.PHOTO_LAYER = RS.PHOTO_LAYER
          AND RS.RTCLGRP_ID = RG.RTCLGRP_ID
          AND FL.PD_ID = ?
        ORDER BY PS.TECH_ID, PS.PRODSPEC_ID, FL.PD_ID, RG.RTCL_ID"
      qargs = [pd]
      qargs.unshift(q)
      res = @db.select_all(*qargs)
      return nil unless res
      return res if params[:raw]
      return res.collect {|r| retstruct.new(*r)}
    end
    
  end
  
end
