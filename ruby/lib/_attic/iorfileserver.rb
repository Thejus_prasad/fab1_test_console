=begin
CORBA IOR file service

(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: Steffen Steidten, GLOBALFOUNDRIES INC., 2011

History:
  2015-05-04 ssteidte,  based on WEBRick::GenericServer instead of GServer (for Ruby 2.2)
=end

require './ruby/set_paths'
set_paths

#require 'gserver'
require 'webrick/server'
require 'fileutils'
require 'optparse'
require 'drb'


# CORBA IOR file service
class IORFileServer <  WEBrick::GenericServer  #GServer

  def initialize(params={})
    @addr = params[:addr] || ''
    @port = params[:port] || 22220
    @nthreads = params[:nthreads] || 256
    @iorpath = params[:iorpath] || './log/ior'
    FileUtils.mkdir_p(@iorpath)
    # start the TCP server
    ##super(@port, @addr, @nthreads)
    super(Port: @port, BindAddress: @addr, MaxClients: @nthreads)
    $log.warn "IOR file server listening on #{@addr.inspect}:#{@port}"
  end
  
  # method actually serving the request
  #def serve(io)
  def run(io)
    begin
      $log.info io.peeraddr.inspect
      loop do
        io.puts 'OK'
        ff = io.readline.strip.split
        $log.info "cmd: #{ff.inspect}"
        cmd = ff[0]
        if ff[1]
          fname = File.expand_path(File.join(@iorpath, ff[1]))
          fname += '.IOR' unless fname.end_with?('.IOR')
        end
        case cmd
        when 'delete', 'delior'
          if File.exist?(fname)
            File.delete(fname)
            io.puts "error: file #{fname} could not be deleted" if File.exist?(fname)
          else
            io.puts "error: file #{fname} does not exist"
          end
        when 'ior'
          open(fname, 'wb') {|fh| fh.write ff[2]}
          $log.info "wrote file #{fname}"
        when 'getior'
          if File.exist?(fname)
            s = File.read(fname)
            io.puts s
            $log.info "read file #{fname}"
          else
            io.puts "error: file #{fname} does not exist"
          end
        #
        when 'exit'
          io.puts 'bye!'
          break
        when 'exitserver'
          $log.warn "stopping on request"
          self.stop
          break
        else
          io.puts "unknown command: #{cmd.inspect}"
        end
      end
    rescue EOFError
      # closed socked, log for DEBUG only
      $log.debug $!
      $log.debug e.backtrace.join("\n")
    rescue => e
      $log.warn $!
      $log.debug e.backtrace.join("\n")
    end
  end
end


def main(argv)
  # set defaults
  logfile = 'log/iorfileserver.log'
  port = 22220
  drbport = port + 10000
  verbose = false
  # parse cmdline options
  optparse = OptionParser.new {|o|
    o.on('-h', '--help', 'diplay this screen') {puts o; exit}
    o.on('-l', '--logfile [FILE]', 'log to FILE instead of STDOUT') {|f| logfile = f}
    o.on('-p', '--port [PORT]', Integer, "port number, default is #{port}") {|p| port = p}
    o.on('-d', '--drbport [PORT]', Integer, "drb port number, default is #{drbport}") {|p| drbport = p}
    o.on('-v', '--verbose', 'verbose logging') {verbose = true}
  }
  optparse.parse(*argv)
  create_logger(file: logfile, file_verbose: verbose, verbose: verbose)
  #
  $log.info "Starting IOR file server, listening on port #{port}"
  srv = IORFileServer.new(iorpath: '/corbaior', port: port)
  DRb.start_service("druby://#{Socket.gethostname}:#{drbport}", srv)
  srv.start  #.join
end

main(ARGV) if __FILE__ == $0
