=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author:     ssteidte, 2011-04-13
Version:    1.2.4

History:
  2015-05-27 ssteidte, added RMS/Armor load tests
  2015-05-28 ssteidte, moved Exerciser from armor.rb to armor_exerciser.rb
  2017-04-25 sfrieske, replaced rqrsp by rqrsp_mq
  2021-02-05 sfrieske, use instance_of instead of kind_of
=end

require 'base64'
require 'builder'
require 'rqrsp_mq'
require 'xmlhash'


# Armor Testing Support
module Armor
  Recipe = Struct.new(:oid, :path, :dcppaths, :eqprecipename, :recipetype,
      :downloadflag, :parameters, :linkedrecipes, :dcps, :version, :bodyholder, :toolrecipename)
  Parameter = Struct.new(:pid, :eiparametername, :lowlimit, :highlimit, :value, :purpose, :options)

  # EI to ARMOR MQ interface
  class EI < RequestResponse::MQ
    include XMLHash
    attr_accessor :env, :eqp

    def initialize(env, eqp, params={})
      @env = env
      @eqp = eqp
      super("armor.#{env}", {bytes_msg: true, use_jms: false}.merge(params))
    end

    def inspect
      "#<#{self.class.name} env: #{@env}, eqp: #{@eqp}>"
    end

    # works via HTTP as part of the PCMS interface, but not MQ
    #
    # useful for testing the communication
    def get_fabs(installationId='Default', params={})
      xml = Builder::XmlMarkup.new(indent: 2)
      xml.instruct!
      xml.env :Envelope, "xmlns:xsi"=>"http://www.w3.org/2001/XMLSchema-instance",
        "xmlns:xsd"=>"http://www.w3.org/2001/XMLSchema", "xmlns:env"=>"http://schemas.xmlsoap.org/soap/envelope/" do |enve|
        enve.env :Body do |body|
          body.n1 :getFabs, "xmlns:n1"=>"http://ejb.setup.rm.maui.amd.com/jaws" do |t|
            t.n1 :installationId, installationId
          end
        end
      end
      return send_receive(xml.target!, params)
    end

    def _create_context_data(context_params)
      cxml = Builder::XmlMarkup.new
      cxml.instruct!
      cxml.ns :fab36_context, 'xmlns:ns'=>'http://www.amd.com/schemas/armor-payload.xsd' do |t|
        context_params.each_pair {|k, values|
          values = [values] if values.instance_of?(String)
          values.each {|v|
            t.ns :nvpair do |nv|
              nv.ns :name, k
              nv.ns :value, v
            end
          }
        }
      end
      cxml.target!
    end

    # generic method to get payload, rtype must be either :bycontext or :byoid
    #
    # return response as Hash (with key :Fault on logical error) or nil
    def get_payload(rtype, params={})
      ctx = params[:context]
      if ctx.nil?
        # create context from params
        ctx = {}
        ctx['Equipment'] = params[:eqp] || @eqp
        ctx['RecipeName'] = params[:recipe] if params[:recipe]
        ctx['RecipeNameSpaceID'] = params[:recipe_namespace] if params[:recipe_namespace]
        $log.debug {"  ctx: #{ctx.inspect}"}
        context = _create_context_data(ctx)
      elsif ctx.instance_of?(Hash)
        # create context from Hash
        context = _create_context_data(ctx)
      else
        # context already in XML format, e.g. from payload log files
        context = ctx
      end
      # context is a string containing XML
      context = Base64.encode64(context)
      installationid = params[:installationid] || 'Default'
      startpath = params[:startpath] || '/Job Setup'
      renderer = params[:renderer] || 'DCP_2_2_9-TDM_2_2_9-DCC_2_2_9'
      rendererpath = params[:rendererpath] || "/system/plugins/renderers/idxmlrenderer-#{renderer}"
      ns = case rtype
      when :bycontext; :getRenderedPayloadByContext
      when :byoid; :getRenderedPayloadByOid
      else :unknown
      end
      # build request
      xml = Builder::XmlMarkup.new(indent: 2)
      xml.instruct!
      xml.env :Envelope, 'xmlns:ns'=>'http://interfaces.runtime.armor.amd.com',
        'xmlns:env'=>'http://schemas.xmlsoap.org/soap/envelope/' do |enve|
        enve.env :Body do |body|
          body.ns ns do |t|
            t.installationId installationid
            if rtype == :bycontext
              t.startingPath startpath
            elsif rtype == :byoid
              t.oid params[:oid]
            end
            t.rendererPath rendererpath
            t.context context
          end
        end
      end
      # send message
      return send_receive(xml.target!, params)
    end

    # submit getRenderedPayloadByContext request as EIs do at SLR
    #
    # return payload as XML string on success, Hash with key :Fault on logical error or nil
    def rendered_payload_context(recipe_namespace, recipe, params={})
      $log.info "rendered_payload_context #{recipe_namespace.inspect}, #{recipe.inspect}"
      $log.debug {params.inspect}
      res = get_payload(:bycontext, params.merge(recipe_namespace: recipe_namespace, recipe: recipe)) || return
      ret = xml_to_hash(res)[:getRenderedPayloadByContextResponse] || return
      return ret[:getRenderedPayloadByContextReturn].unpack('m').first
    end

    # submit getRenderedPayloadByOid request as EIs do for Interface/A
    #
    # return payload as XML string on success, Hash with key :Fault on logical error or nil
    def rendered_payload_oid(oid, params={})
      $log.info "rendered_payload_oid #{oid.inspect}, #{params.inspect}"
      $res = res = get_payload(:byoid, params.merge(oid: oid)) || return
      ret = xml_to_hash(res)[:getRenderedPayloadByOidResponse] || return
      return ret[:getRenderedPayloadByOidReturn].unpack('m').first
    end

    # interpretes the payload as ARMOR recipe
    def to_recipe(payload)
      # dive elements until we find recipe version
      xml = REXML::Document.new(payload) if payload.instance_of?(String)
      recipepath = xml.elements['//recipeVersion']
      return _xml_to_recipe(recipepath)
    end

    def _xml_to_recipe(recipepath)
      oid = recipepath.attributes['oid']
      path = recipepath.attributes['fullElementPath']
      version = recipepath.attributes['version']
      recipe_type = recipepath.elements['recipeType'].text
      download_flag = recipepath.elements['downloadFlag'].text
      eqp_recipe_name = recipepath.elements['equipmentRecipeName'].text
      parameters = _parse_parameters(recipepath.get_elements('parameter'))
      rawbody = recipepath.elements['bodyHolder/body']
      if rawbody
        body = recipepath.elements['bodyHolder/body'].text.gsub("\n", '').unpack('m').first
        tool_recipe_name = _extract_toolrecipename(body)
      else
        body = tool_recipe_name = nil
      end
      linked_recipes = []
      linked_recipes = recipepath.get_elements('recipeVersion').collect do |rcp|
        $log.info "  found #{rcp.inspect}"
        _xml_to_recipe(rcp)
      end
      #parse dcps
      dcppaths = []
      dcps = []
      return Recipe.new(oid, path, dcppaths, eqp_recipe_name, recipe_type,
        download_flag, parameters, linked_recipes, dcps, version, body, tool_recipe_name)
    end

    def _parse_parameters(xml_path)
      xml_path.collect do |rcp|
        $log.debug {"  found #{rcp.inspect}"}
        low = nil
        high = nil
        if rcp.elements['parameterValueString']
          value = rcp.elements['parameterValueString'].text
        elsif rcp.elements['parameterValueInteger']
          value = rcp.elements['parameterValueInteger'].text.to_i
        elsif rcp.elements['parameterValueFloat']
          value = rcp.elements['parameterValueFloat'].text.to_f
        else
          value = nil
        end
        options = nil
        pname = rcp.elements['eiParameterName'] ? rcp.elements['eiParameterName'].text : nil
        ppurpose = rcp.elements['parameterPurpose'] ? rcp.elements['parameterPurpose'].text : nil
        Parameter.new(rcp.attributes["nameInPath"], pname, low, high, value, ppurpose, options)
      end
    end

    # Extract the tool recipe name, works for LAM
    def _extract_toolrecipename(recipebody)
      _iend = recipebody.index("A\x06")   # some magic bytes in the LAM recipe
      ($log.debug "tool recipe name not found in body"; return) unless _iend
      return recipebody[4.._iend-1]
    end

    # for EIs requesting EC-SV and DCPs separately (recipe comes from RMS)

    # return EC-SV data as Hash with key :Fault on logical error or nil
    def ecsv(params={})
      eqp = params[:eqp] || @eqp
      startpath = params[:startpath] || '/Job Recipes/EC-SV Check BLOB'
      $log.info "ecsv startpath: #{startpath.inspect}, eqp: #{eqp.inspect}"
      get_payload(:bycontext, startpath: startpath, eqp: eqp)
    end

    # return DCP payload as XML string on success, Hash with key :Fault on logical error or nil
    def dcp(startpath, params={})
      eqp = params[:eqp] || @eqp
      $log.info "dcp #{startpath.inspect} eqp: #{eqp.inspect}"
      get_payload(:bycontext, startpath: startpath, eqp: eqp)
    end

    # retrieve DCPs and EC-SV data in parallel
    #
    # return array of responses or nil if check: true is not passed
    # return nil on comm error, false on logical error or true on success if check: true
    def dcps_ecsv(startpaths, params={})
      eqp = params[:eqp] || @eqp
      startpaths = [startpaths] unless startpaths.instance_of?(Array)
      res = Array.new(startpaths.size + 1)
      tt = startpaths.each_with_index.collect {|startpath, i|
        Thread.new(startpath, i) {|startpath, i| res[i] = dcp(startpath, eqp: eqp)}
      }
      tt << Thread.new {res[-1] = ecsv(eqp: eqp)}
      tt.each {|t| t.join}
      return res unless params[:check]
      return nil if res.include?(nil)
      res.each {|e| return false if e.has_key?(:Fault)}
      return true
    end

  end


  # SetupFC MQ interface, currently not used
  class XXSetupFC < RequestResponse::MQ
    attr_accessor :env, :mq_request_write

    def initialize(env, params={})
      @env = env
      super("armor_setupfc.#{env}", params)
    end

    # send message on behalf of Setup.FC to ARMOR
    # msg is PREPARE or RELEASE, spec e.g. C05-00013452
    # return true on success
    def setupfc_message(msg, spec, params={})
      $log.info "setupfc_message #{msg.inspect}, #{spec.inspect}, #{params.inspect}"
      cid = (params[:cid] or "34456")
      spec_version = (params[:spec_version] or 1)
      recipient = (params[:recipient] or "FCS-SYSTEM")
      reply_required = (params[:reply_required] or "true")
      sender = (params[:sender] or "SETUPFC_WF_ENGINE")
      comment = (params[:comment] or "QA Test")
      #
      xml = Builder::XmlMarkup.new(indent: 2)
      xml.instruct!
      xml.tns :setupfcMessage, "xmlns:tns"=>"http://setupfc.globalfoundries.com/xml/message",
        "xmlns:xsi"=>"http://www.w3.org/2001/XMLSchema-instance",
        "xsi:schemaLocation"=>"http://setupfc.globalfoundries.com/xml/message setupfc-messages.xsd",
        "correlationId"=>cid,
        "specificationId"=>spec,
        "specificationVersion"=>spec_version,
        "message"=>msg,
        "recipient"=>recipient,
        "replyRequired"=>reply_required,
        "sender"=>sender,
        "comment"=>comment
      # send message
      ret = send_receive(xml.target!, params) || return
      return ret if params[:nosend]
      return xml_to_hash(ret)
    end
  end

end
