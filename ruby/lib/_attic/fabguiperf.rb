=begin
FabGUI / ERP Performance Statistics
  
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

    
Author: Steffen Steidten, 2011-06-30
=end

require 'time'
require 'yaml'
require 'misc'
require 'remote'
require 'charts'

# FabGUI / ERP Performance Statistics
module FabGUI

  # FabGUI / ERP Performance Statistics
  class ERPPerformance
    attr_accessor :env, :rhg, :_perflines, :stats
    
    def initialize(env)
      @env = env
      @rhg = RemoteHostGroup.new "fabgui.#{env}"
      @rhg = RemoteHostGroup.new("fabgui.itdc", :host=>"f36asd6") if @env == "dev"
    end
    
    def lot_transfer_stats(params={})
      $log.info "lot_transfer_stats"
      get_log_lines(params)
      extract_stats(params)
      show_lot_transfer_stats(params) if params[:show]
    end
    
    def get_log_lines(params={})
      @_perflines = []
      res = @rhg.exec!("grep -hA 3 '^sendERPRequest' /local/logs/httpd/java/processctrl/processctrl*")
      res.each_with_index {|r, i|
        ($log.warn "no data from #{rhg.hosts[i].host}"; next) unless r
        # optionally save logfile entries
        fname = params[:savelogfile]
        open("#{fname}_#{rhg.hosts[i].host}", "wb") {|fh| fh.write(r)} if fname
        @_perflines.concat r.split("\n")
      }
    end
    
    def extract_stats(params={})
      lots = params[:lots]
      tstart = params[:tstart]
      tstart = Time.parse(tstart) if tstart.kind_of?(String)
      tend = params[:tend]
      tend = Time.parse(tend) if tend.kind_of?(String)
      @stats = {:lot=>[], :time=>[], :checks=>[], :msg=>[], :total=>[]}
      i = -1
      valid = false
      @_perflines.each {|line|
        ff = line.split
        if line.start_with?("sendERPRequest(")
          lot = ff[-1][0..-2]  # remove trailing ':'
          valid = ((lots == nil) or lots.member?(lot))
          $log.debug "  lot #{lot} consider: #{valid}"
          next unless valid
          t = Time.parse(ff[0].split('(')[1].split(')')[0])
          if (tstart and t < tstart) or (tend and t > tend)
            valid = false
            next
          end
          $log.info "  lot #{lot} at #{t.utc.iso8601}"
          i += 1
          @stats[:lot][i] = lot
          @stats[:time][i] = t
        else
          next unless valid
          case ff[0]
          when "parameter"
            @stats[:checks][i] = ff[-2].to_f/1000.0
          when "message"
            @stats[:msg][i] = ff[-2].to_f/1000.0
          when "overall"
            @stats[:total][i] = ff[-2].to_f/1000.0
          when ("--" or "##")
          else
            $log.warn "  invalid line: #{line.inspect}"
          end
        end
      }
      return @stats
    end
    
    # display the statistics collected by lot_transfer_stats
    def show_lot_transfer_stats(params={})
      stats = (params[:stats] or @stats)
      ($log.warn "no stats data"; return nil) if stats[:time].size == 0
      fname = params[:export]
      open(fname, "wb") {|fh| fh.write(stats.to_yaml)} if fname
      bname = (params[:file] or "log/lot_transfer_#{@env}_#{stats[:time][0].iso8601.gsub(':', '_')}")
      #
      n = @stats[:msg].size
      duration = @stats[:time].max - @stats[:time].min
      frequency = (n * 36000 / duration).to_i / 10.0
      over30 = over120 = 0
      @stats[:msg].each {|d|
        over30 += 1 if d > 30
        over120 += 1 if d > 120
      }
      #over30 = over30 * 100.0 / @stats[:msg].size
      #over120 = over120 * 100.0 / @stats[:msg].size
      #
      open("#{bname}_summary.txt", "wb") {|fh|
        fh.puts "#LotTransfer statistics from #{stats[:time].min.iso8601} .. #{stats[:time].max.iso8601}"
        fh.puts "#" + "=" * 72
        fh.puts "#requests sent: #{stats[:lot].size} within #{duration}s (#{frequency} per hour)"
        fh.puts "#  requests with msg roundtrip time > 30s: #{over30} (#{(over30*100.0/n).round}%)"
        fh.puts "#  timed out after 120s: #{over120} (#{(over120*100.0/n).round}%)"
        fh.puts
        fh.puts "#                   average (s)  stddev (s)        min (s)         max(s)"
        fh.puts "#------------------------------------------------------------------------"
        fh.puts "#param checks       %8.3f       %8.3f       %8.3f       %8.3f" % 
          [stats[:checks].average, stats[:checks].standard_deviation, stats[:checks].min, stats[:checks].max]
        fh.puts "#msg roundtrip      %8.3f       %8.3f       %8.3f       %8.3f" % 
          [stats[:msg].average, stats[:msg].standard_deviation, stats[:msg].min, stats[:msg].max]
        fh.puts "#overall duration   %8.3f       %8.3f       %8.3f       %8.3f" % 
          [stats[:total].average, stats[:total].standard_deviation, stats[:total].min, stats[:total].max]
        fh.puts "\n\n"
        fh.puts "#timestamp               lot                  checks       msg     total"
        fh.puts "#-----------------------------------------------------------------------"
        stats[:time].zip(stats[:lot]).zip(stats[:checks]).zip(stats[:msg]).zip(stats[:total]).sort.each {|line|
          line.flatten!
          fh.puts "#{line[0].iso8601}    %-16.16s   %8.3f  %8.3f  %8.3f" % line[1..-1]
        }
        performance_charts(params)
      }
    end
  
    def performance_charts(params={})
      stats = (params[:stats] or @stats)
      bname = (params[:file] or "log/lot_transfer_#{@env}_#{stats[:time][0].iso8601.gsub(':', '_')}")
      ch = Chart::Time.new(nil)
#      [:total, :msg, :checks].each {|s| ch.add_series(stats[:time].zip(stats[s]))}
      [:msg].each {|s| ch.add_series(stats[:time].zip(stats[s]))}
      ch.create_chart(:title=>File.basename(bname), :y=>"duration (s)", :dots=>true, :dotsize=>5, :export=>"#{bname}_chart1.png")
      #ch.create_chart(:title=>File.basename(bname), :y=>"duration", :dots=>false, :lg=>true, :export=>"#{bname}_chart2.png")
    end

  end
  
end
