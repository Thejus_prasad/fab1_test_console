=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.
    

Author: Steffen Steidten, 2014-03-25

History:
  2017-12-07 sfrieske, use rqrsp_http
=end

ENV.delete('http_proxy')
require 'rqrsp_http'


# FabView testing
module FabView

  class Client
    attr_accessor :env, :http, :authenticated
    
    def initialize(env, params={})
      @http = RequestResponse::Http.new("fabview.#{env}", {basic_auth: false, keepalive: true}.merge(params))
      user = params.delete(:user)
      password = params.delete(:password)
      @authenticated = authenticate
      $log.info "authenticated: #{@authenticated.inspect}"
    end
    
    def authenticate
      @http.get || return
      body = {'loginForm'=>'loginForm', 'loginForm:username'=>@http.user, 
        'loginForm:password'=>@http.params[:password].unpack('m').first, 
        'loginForm:submit'=>'Login', 'javax.faces.ViewState'=>''}
      res = @http.post(body) || return
      return res.include?('Welcome')
    end
    
    # call e.g. rest('lot', 'U14KA.00'), returns web page, only useful for load tests
    def rest(classname, entity, params={})
      since = params.delete(:since) || '365'
      pagesize = params.delete(:pagesize) || '300'
      return @http.get(resource: "#{classname}/#{entity}", args: params.merge({'since'=>since, 'pageSize'=>pagesize}))
    end
  end 
end