=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author:     dsteger, 2014-07-15
Version:    0.0.1

History
=end

require 'siview'
require 'misc'
require 'rqrsp'

# MUX SiView Access
module SiView

  # MM REST I/F
  class HttpClient < MM  
    attr_accessor :env, :manager

    def initialize(env, params={})      
      @env = env      
      @jcscode ||= com.amd.jcs.siview.code # may be set already for MMF7 or AsmView MM, defaulting to regular MM client package                  
      @orb = ORB.init([].to_java(:string), nil)
      @manager = ManagerProxy.new(env)
      @_user = user_struct(@manager.user, @manager.password, params)
      @release = 10
    end    
  end
  
  class ManagerProxy < RequestResponse::HTTP
    def initialize(env)
      super("mux_mm.#{env}")
    end
    
    def method_missing(method, *args, &blk)
      $log.info "called #{method} with args #{args.join(',')}"      
      resource = method[0].downcase + method[1..-1]
      body = {}
      args.each_with_index {|arg,i| body["arg#{i}"] = to_json(arg)}
      $log.info "#{body.pretty_inspect}"
      res = _post_json_request(body, resource: resource) 
      return res
    end
    
    # parse a ruby object into json and sends it via put request
    def _post_json_request(body, params={})      
      url, header, user = _prepare_request(params)
      header['Content-Type'] = 'application/json'
      @request = Net::HTTP::Post.new(url, header)
      jbody = JSON(body) 
      $log.info "#{jbody}"
      @request.body = jbody     
      _process_request(nil ) # no basic authentification
    end
    
    def respond_to_missing?(symbol, include_private)
      true
    end
        
    # Generate a JSON hash structure from a ppt structure
    def to_json(ppt_struct)
      json = {}
      o = ppt_struct
      if o.kind_of?(String) or o.kind_of?(java.lang.String)
        return o
      elsif o.kind_of?(Any)
        return {}
      elsif o.respond_to?('java_class')
        if o.java_class.array?
          return o.collect {|i| to_json(i)}
        else
          ff = o.java_class.fields
          return Hash[ff.collect {|f| [f.name, to_json(o.send(f.name))]}]
        end        
      end
      return json
    end
      
  end  
end
