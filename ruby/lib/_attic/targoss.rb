=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author:     ssteidte, 2014-01-17

Version:    0.0.1

History:
  2016-08-24 sfrieske,  use lib rqrsp_mq instead of rqrsp
=end

require 'time'
require 'builder'
require 'rqrsp_mq'
require 'siview'


# Test Targoss
module Targoss

  # create a Targoss DCR
  class DCR
    attr_accessor :lot, :route, :product, :facility, :op, :eqp, :recipe, :operator, :pg, :stepper, :wafers

    def initialize(params={})
      @lot = params[:lot] || "QALOT001.000"
      @route = params[:route] || "QAROUTE01.01"
      @facility = params[:facility] || "MODULE2"
      @op = params[:op] || "QAPD01.01"
      @eqp = params[:eqp] || "OVL200"             # OVL100
      @operator = params[:operator] || "HOST"
      @pg = params[:pg] || "2970G"                #
      @recipe = params[:recipe] || "#{pg}QA"
      @product = params[:product] || "2970GH.K1"
      @stepper = params[:stepper] || ""
      @wafers = params[:wafers] || [SiView::MM::Wafer.new('QALOT00101', 1, '01', @lot), SiView::MM::Wafer.new('QALOT00102', 2, '02', @lot)]
    end

    def inspect
      "#<#{self.class.name}, lot: #{@lot}, op: #{@op}, route: #{@route}>"
    end

    def to_xml(params={})
      @timestamp = params[:timestamp] || Time.now
      xml = Builder::XmlMarkup.new(:indent=>2)
      xml.ns :OVLMeasurement do |ovlmeas|
        ovlmeas.ns :LotInfo do |lotinfo|
          lotinfo.ns :LotID, @lot
          lotinfo.ns :Route, @route
          lotinfo.ns :Product, @product
          lotinfo.ns :Facility, @facility
        end
        ovlmeas.ns :RunInfo do |runinfo|
          runinfo.ns :Operation, @op
          runinfo.ns :TimeStamp, @timestamp.utc.iso8601(3)
          runinfo.ns :Tool, @eqp
          runinfo.ns :Recipe, @recipe
          runinfo.ns :Operator, @operator
        end
        ovlmeas.ns :MeasInfo do |measinfo|
          measinfo.ns :ProdClass, @pg
          measinfo.ns :LastStepper, @stepper
          measinfo.ns :WaferList do |wlist|
            @wafers.each {|w|
              wlist.ns :Wafer do |wtag|
                wtag.ns :Port, 1
                wtag.ns :Slot, w.slot
                wtag.ns :WaferId, w.wafer
                wtag.ns :Size, 300.0
                wtag.ns :Orientation, 0, 'Unit'=>'degrees'
              end
            }
          end
        end
        ovlmeas.ns :MetaInfo do |metainfo|
        end
      end
      return xml.target!
    end
  end


  # send DCRs to Targoss (on behalf of EIs)
  class Client < RequestResponse::MQ
    attr_accessor :env, :dcr

    def initialize(env, params={})
      @env = env
      super("targoss.#{env}", {use_jms: false}.merge(params))
    end

    # return true on sucess
    def send_dcr(dcr, params={})
      dcr = dcr.to_xml(params) unless dcr.kind_of?(String)
      res = send_receive(dcr, params)
      return res
    end

  end

end

