=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.


Author: Steffen Steidten, Ulrich Koerner, 2012-01-06
Version: 2.1.5

History:
  2014-01-31 ssteidte,  use XMLHash
  2015-07-24 ssteidte,  minor cleanup
  2016-02-24 sfrieske,  added extrator keys to sample_generickeys
  2016-05-25 sfrieske,  add_parameters now works always serially to prevent rare ArrayOutOfBound exceptions
=end

require 'time'
require 'misc'
require 'xmlhash'

# SPACE/SIL Testing Support
module Space

  # Create and modify a DCR
  class DCR
    include XMLHash

    attr_accessor :eqp, :tlocations, :context, :parameters, :ts


    def initialize(params={})
      @eqp = params.has_key?(:eqp) ? params[:eqp] : 'QAEqp1'
      @tlocations = {}
      @ts = Time.now.utc.iso8601(3)
      dcp_armor_path = params[:dcp_armor_path] || '/Job DCP/DCP Repository/SEM-AMAT/C-ALL'
      dcp_plan_name = params[:dcp_plan_name] || 'SEM-YM'
      dcp_plan_version = params[:dcp_plan_version] || '11'
      evname = params[:eventReportingName] || 'C-DR-SEM'
      @context = {
        'baselineVersion'=>'f36cei8.2. - patch 3',
        'dcpArmorPath'=>[dcp_armor_path, dcp_plan_name, dcp_plan_version].join('/'),
        'dcpPlanName'=>dcp_plan_name, 'dcpPlanVersion'=>dcp_plan_version,
        'description'=>'DCC DCP Version AutoOnly', 'equipmentModelName'=>'SEMVISIONG3', 'equipmentModelVersion'=>'1.0',
        'eventReportingName'=>evname, 'reportingTimeStamp'=>nil, 'schemaVersion'=>'DCR_2_2_0SPACE',
        Equipment: {
          'controlState'=>'ONLINE-REMOTE', 'description'=>'POLISHER', 'equipmentReportedE10State'=>params[:equipmentReportedE10State] || 'PRD',
          'id'=>@eqp, 'modelNumber'=>'POLPOL', 'name'=>'POLISHER', 'softwareRevision'=>'1.0.10', 'type'=>params[:eqptype] || 'METROLOGY',
          Port: {
            'accessMode'=>'AUTO', 'associationState'=>'NOT ASSOCIATED', 'equipmentPortID'=>'1',
            'id'=>'P1', 'loadPortTransferState'=>'READY TO LOAD', 'locationType'=>'PORT', 'occupiedState'=>'NOT OCCUPIED',
            'reservationState'=>'NOT RESERVED', 'siviewPortMode'=>'Semi-Start-3', 'siviewPortState'=>'LoadReq',
            'trackingLocation'=>'11', 'type'=>'INPUT OUTPUT'
          },
          ProcessingArea: [],
          Component: [
            {'id'=>'LLK_OUT', 'location'=>'4', 'trackingLocation'=>'4', 'type'=>'OTHER'},
            {'id'=>'LLK_OUT', 'location'=>'3', 'trackingLocation'=>'3', 'type'=>'OTHER'},
            {'id'=>'LLK_OUT', 'location'=>'2', 'trackingLocation'=>'2', 'type'=>'OTHER'},
          ]
        },
        JobSetup: nil
      }
      @parameters = []
      #
      reporting_timestamp(params[:timestamp])
      chambers = params[:chambers] || {'CHA'=>5, 'CHB'=>6, 'CHC'=>7}
      chambers.each {|name, location| add_eqp_processing_area(name, location, params)}
      unless params[:jobsetup] == false
        @context[:JobSetup] = {SAPJob: nil, SiViewControlJob: nil}
        set_sapjob(params) if params[:sapjob]
        unless params.has_key?(:cj) && params[:cj].nil?
          set_controljob(params)
          add_lot(params[:lot] || 'SPCDUMMY0.00', params) unless params.has_key?(:lot) && params[:lot].nil?
        end
      end
    end

    def inspect
      "#<#{self.class.name}, eqp: #{@eqp}}>"
    end

    def reporting_timestamp(ts=nil)
      ts ||= Time.now
      ts = Time.parse(ts) if ts.kind_of?(String)
      @context['reportingTimeStamp'] = @ts = ts.utc.iso8601(3)
    end

    # set content parts
    def add_eqp_processing_area(name, location, params={})
      ispa = location*10
      spas = ['_SIDE1', '_SIDE2'].collect {|e|
        ispa += 1
        spa = name + e
        @tlocations[spa] = ispa
        {'id'=>spa, 'locationType'=>'SUBPROCESSINGAREA', 'processState'=>'IDLE', 'trackingLocation'=>ispa, 'type'=>'ChamberSide'}
      }
      @tlocations[name] = location
      pa = {'id'=>name, 'locationType'=>'PROCESSINGAREA', 'processingAreaE10State'=>params[:processingAreaE10State] || 'PRD',
        'processState'=>'IDLE', 'trackingLocation'=>location, 'type'=>'ProcessingArea', SubProcessingArea: spas}
      @context[:Equipment][:ProcessingArea] << pa
    end

    def set_controljob(params={})
      carrier = params.has_key?(:carrier) ? params[:carrier] : 'DummyFoup'
      @context[:JobSetup][:SiViewControlJob] = {'id'=>params[:cj] || "#{@eqp}-#{Time.now.to_i}", 'siviewUserID'=>params[:user] || '',
        :FOUP=>[{
          'carrierAccessingState'=>'NOT ACCESSED', 'idStatusState'=>'ID NOT READ', 'location'=>'P1', 'locationType'=>'PORT',
          'scheduledLoadPort'=>'P1', 'scheduledUnloadPort'=>'P1', 'slotMapState'=>'SLOT MAP NOT READ', 'id'=>carrier,
          foupHistory: {'enterTime'=>(Time.now-100).utc.iso8601(3), 'location'=>'P1', 'locationType'=>'PORT', 'trackingLocation'=>'11'},
          Lot: []
        }]
      }
    end

    def set_sapjob(params={})
      @context[:JobSetup][:SAPJob] = {'inspectionCharacteristicID'=>params[:sapicid] || '0010',
        'orderOperation'=>params[:sapoo] || '0095', 'SAPdataCollectionPlanID'=>params[:sapdcpid] || '60012916',
        'SAPOrderNumber'=>params[:sapon] || '67853957'
      }
    end

    def add_lot(lot, params={})
      icarrier = params[:icarrier] || -1
      armor_repository = params[:armor_path] || '/Job Recipes/Recipe Repository/SEM-AMAT/C-P-SEM'
      armor_recipe = params[:armor_recipe] || '4710B MGT-NMETDEPDR'
      recipe_version = params[:recipe_version] || '2'
      mrecipe = params[:machine_recipe] || 'C-P-SEM-G4.4710B-MGT-NMETDEPDR.01'
      @context[:JobSetup][:SiViewControlJob][:FOUP][icarrier][:Lot] << {
        'armorPath'=>[armor_repository, armor_recipe, recipe_version].join('/'),
        'armorRecipeID'=>armor_recipe, 'armorRecipeVersion'=>recipe_version,
        'department'=>params.has_key?(:department) ? params[:department] : 'QA',
        'customer'=>params[:customer] || 'Customer',
        'id'=>lot,
        'logicalRecipeID'=>params[:logical_recipe] || 'C-P-4710B-MGT-NMETDEPDR.01',
        'engineeringRecipeSelect'=>params[:ers] || 'false',
        'lotType'=>params[:lottype] || 'Production',
        'machineRecipeID'=>mrecipe,
        'manufacturingLayer'=>params[:layer] || 'B',
        'monitorLotFlag'=>!!params[:monitorlotflag],
        'operationID'=>params[:op] || 'MGT-NMETDEPDR.QA',    # cf SpecLimits_ITDC.SpecID.xlsx
        'operationName'=>params[:opName] || 'DEFECT REVIEW',
        'operationNumber'=>params[:opNo] || '2500.1000',
        'passCount'=>params[:passcount] || '1',
        'photoLayer'=>params[:photolayer] || 'DummyPL',
        'physicalRecipeID'=>mrecipe.split('.')[1],
        'productGroupID'=>params.has_key?(:productgroup) ? params[:productgroup] : 'QAPG1',
        'productID'=>params.has_key?(:product) ? params[:product] : 'SILPROD.01',
        'productType'=>params.has_key?(:producttype) ? params[:producttype] : 'Wafer',
        'route'=>params.has_key?(:route) ? params[:route] : 'SIL-0001.01',
        'subLotType'=>params.has_key?(:sublottype) ? params[:sublottype] : 'PO',
        'technology'=>params.has_key?(:technology) ? params[:technology] : '32NM',
        Wafer: [],
      }
      return true
    end

    # names is an array of parameter names, wafer_values a wafer/value hash witch is added as readings for each parameter
    #
    # return self
    def add_parameters(names, wafer_values, params={})
      names.each {|n| add_parameter(n, wafer_values, params)}
      return self
    end

    # Add a new parameter
    # name is the paramter name, wafer_values is a hash of waferids and related reading values
    #   Parameter names are checked for duplicates unless parameter _:separate_lot_ is set
    #   Readings always go to the last instance of the parameter with _name_
    # return self
    def add_parameter(name, wafer_values, params={})
      ($log.warn "add_parameter: wrong name #{name.inspect}"; return) unless name.kind_of?(String)
      space_listening = params[:space_listening]
      nocharting = !!params[:nocharting]
      pvalid = params[:parameter_valid] != false
      pnames = @parameters.collect {|p| p['name']}
      unless pnames.member?(name) && !params.has_key?(:separate_lot)
        @parameters << {'dcpRequestType'=>'Event', 'name'=>name, 'spaceListeningOnly'=>space_listening,
          'noCharting'=>nocharting, 'parameterValid'=>pvalid, Reading: []
        }
        @parameters.last.delete('spaceListeningOnly') if space_listening.nil?  # avoid an empty XML tag attribute
      end
      if params.has_key?(:separate_lot) # find index of parameter, use last one in case of second instance was given
        iparam = @parameters.rindex { |p| p['name'] == name }
      else
        iparam = @parameters.index { |p| p['name'] == name }
      end
      add_readings(wafer_values, {iparam: iparam}.merge(params))
    end

    # reading_values is a hash of waferids and related values, return self
    def add_readings(reading_values, params={})
      reading_type = params[:reading_type] || 'Wafer'
      cj = nil
      if params[:cj]
        cj = params[:cj]
      elsif @context[:JobSetup] && @context[:JobSetup][:SiViewControlJob]
        cj = @context[:JobSetup][:SiViewControlJob]['id']
      end
      iparam = params[:iparam] || -1
      eqp = params[:eqp] || @eqp  #@context[:Equipment][:id]
      rnumber = params[:reading_number] || (@parameters[iparam][:Reading].size + 1)
      rvalid = (params[:reading_valid] != false)
      ppas = params[:processing_areas]
      ppas = [ppas] if ppas.kind_of?(String) && !ppas.start_with?('cycle_')
      pas = ppas
      pas = processing_areas if ppas.nil? || (ppas.kind_of?(String) && ppas.start_with?('cycle_'))
      pa = (ppas == 'cycle_parameter') ? pas[iparam % pas.size] : pas[0]
      remeasured = !!params[:remeasured]
      icarrier = params[:icarrier] || -1
      ilot = params[:ilot] || -1
      iwafer = params[:iwafer] || (rnumber - 1)
      vtype = params[:value_type]
      #
      reading_values.each_pair {|e, va|
        _params = params.clone
        if ppas == 'cycle_wafer'
          pa = pas[(rnumber-1) % pas.size]
          _params[:processing_areas] = [pa]
        elsif ppas == 'cycle_parameter'
          _params[:processing_areas] = pas
        end
        spa = params[:reading_spa] || subprocessing_areas(pa)[0]
        va = [va] unless va.kind_of?(Array)
        srand(1) #set the seed to have always the same sites
        va.each_with_index {|v, i|
          if ppas == 'cycle_reading'
            pa = pas[i % pas.size]
            spa = subprocessing_areas(pa)[0]
          end
          unless vtype
            vtype = 'unsigned integer' if v.class == Fixnum
            vtype = 'float' if v.class == Float
            vtype = 'string' if v.class == String
          end
          reading = {'Equipment'=>eqp, 'ProcessingArea'=>pa, 'SubProcessingArea'=>spa, 'readingNumber'=>rnumber, 'readingValid'=>rvalid,
            'receivedTimeStamp'=>Time.now.utc.iso8601(3), 'remeasuredFlag'=>remeasured, 'value'=>v, 'valueType'=>vtype
          }
          reading.merge!('Site'=>i+1, 'SiteX'=>((rand-0.5) * 100000).round(2), 'SiteY'=>((rand-0.5) * 100000).round(2)) if params[:sites]
          reading['SiViewControlJob'] = cj if cj
          reading[reading_type] = e if e
          @parameters[iparam][:Reading] << reading
          rnumber += 1
          sleep 0.1 # to separate timestamps (was 1, really necessary?)
        }
        # XXX sleep 1 # must be >=1 to have the new wafer sample time really different (??)
        add_wafer(e, _params) if e && (reading_type == 'Wafer') && (params[:add_wafer] != false)
      }
      return self
    end

    def add_wafer(wafer, params={})
      icarrier = params[:icarrier] || -1
      ilot = params[:ilot] || -1
      pas = params[:processing_areas] || processing_areas
      pas = [pas] if pas.kind_of?(String)
      #
      wafers = @context[:JobSetup][:SiViewControlJob][:FOUP][icarrier][:Lot][ilot][:Wafer]
      waferids = wafers.collect {|w| w['id']}
      return self if waferids.member?(wafer)
      npaevents = pas.inject(0) {|s, pa| s + subprocessing_areas(pa).size + 1}
      # XXX tend = params[:reading_timestamp] || (Time.now - 1) ## -1 ??
      # XXX tstart = tend - npaevents*2
      tend = params[:reading_timestamp] || Time.now
      tstart = tend - npaevents*2 - 1
      wloc = params[:location] || (waferids.size + 1)
      wh = [
        {'enterTime'=>tstart.utc.iso8601(3), 'location'=>wloc, 'locationType'=>'FOUP', 'state'=>'NEEDS PROCESSING', RecipeParameter: []},
        {'exitTime'=>(tstart+1).utc.iso8601(3), 'location'=>wloc, 'locationType'=>'FOUP', 'state'=>'NEEDS PROCESSING', RecipeParameter: []},
      ]
      tstart += 1
      pas.each {|pa|
        tstart += 1
        wh << {'enterTime'=>tstart.utc.iso8601(3), 'location'=>pa, 'locationType'=>'PROCESSINGAREA',
          'state'=>'IN PROCESS', 'trackingLocation'=>@tlocations[pa]
        }
        tstart += 1
        spas = params[:subprocessing_areas] || subprocessing_areas(pa)
        spas = [spas] if spas.kind_of?(String)
        spas.each {|spa|
          wh << {'enterTime'=>tstart.utc.iso8601(3), 'location'=>spa, 'locationType'=>'SUBPROCESSINGAREA',
            'state'=>'IN PROCESS', 'trackingLocation'=>@tlocations[spa]
          }
          tstart += 1
          wh << {'exitTime'=>tstart.utc.iso8601(3), 'location'=>spa, 'locationType'=>'SUBPROCESSINGAREA',
            'state'=>'IN PROCESS', 'trackingLocation'=>@tlocations[spa]
          }
        }
        tstart += 1
        wh << {'exitTime'=>tstart.utc.iso8601(3), 'location'=>pa, 'locationType'=>'PROCESSINGAREA',
          'state'=>'IN PROCESS', 'trackingLocation'=>@tlocations[pa]
        }
      }
      processed = params.has_key?(:processed) ? params[:processed] : true
      wh << {'enterTime'=>(tstart+1).utc.iso8601(3), 'location'=>wloc, 'locationType'=>'FOUP', 'state'=>'PROCESSED'} if processed
      wafers << {'id'=>wafer, WaferHistory: wh}
      return self
    end

    def processing_areas
      @context[:Equipment][:ProcessingArea].collect {|pa| pa['id']}
    end

    def subprocessing_areas(paname)
      @context[:Equipment][:ProcessingArea].each {|pa|
        return pa[:SubProcessingArea].collect {|spa| spa['id']} if pa['id'] == paname
      }
      # nothing foud
      return []
    end

    # set the spaceListeningOnly value for each parameter, obsolete
    #
    # return self
    def space_listening(v=true)
      @parameters.each {|p| p[:spaceListeningOnly] = v}
      return self
    end

    def find_lot_context(lot)
      @context[:JobSetup][:SiViewControlJob][:FOUP].each {|cc|
        res = cc[:Lot].find {|l| l['id'] == lot}
        return res if res
      }
    end

    def controljob()
      @context[:JobSetup][:SiViewControlJob]['id']
    end

    def _history_chambers(wh, params={})
      return unless wh.kind_of?(Array)
      wh.collect {|he|
        next unless he.has_key?('enterTime') && he['locationType'] == 'PROCESSINGAREA'
        he['location']
      }.compact
    end

    def _history_sequence(wh, params={})
      return unless wh.kind_of?(Array)
      wh.collect {|he|
        next unless he.has_key?('enterTime') && he['locationType'].include?('PROCESSINGAREA')
        he['location']
      }.compact
    end

    def sample_ekeys(params={})
      # return an array of Hashes per wafer from @context in the same format as from Space::SpcApi::Sample
      icarrier = params[:icarrier] || -1
      ilot = params[:ilot] || -1
      clots = @context[:JobSetup][:SiViewControlJob][:FOUP][icarrier][:Lot]
      ##clots = context_lots(params)
      if clots
        clot = clots[ilot]
        wafer = params[:wafer]
        return clot[:Wafer].collect {|w|
          next if wafer && wafer != w['id']
          ekey = {'Lot'=>clot['id'], 'Wafer'=>w['id'], 'Technology'=>clot['technology'], 'Route'=>clot['route'], 'Product.EC'=>clot['productID'],
            'ProductGroup'=>clot['productGroupID'], 'Event'=>@context['eventReportingName'], 'Photolayer'=>clot['photoLayer']}
          ekey.merge!('PLogRecipe'=>clot['logicalRecipeID'], 'PTool'=>@eqp,   #@context[:Equipment][:id],
            'PChamber'=>(processing_areas.join(':') || '-'), 'POperationID'=>clot['operationID']) if params[:responsible_pd]
          #ekey['Technology'] = '-' if ekey['Technology'].empty?
          ekey['Technology'] = '-' if ekey['Technology'].nil?
          # PChamber and PSequence from wafer history instead of tool model
          wh = w[:WaferHistory]
          ekey.merge!('PChamber'=>(_history_chambers(wh).join(':') || '-')) if params[:pchamber]
          ekey.merge!('PSequence'=>(_history_sequence(wh).join(':') || '-')) if params[:psequence]
          ekey
        }.compact
      else
        return {}
      end
    end

    # return an Hash as from Space::SpcApi::Sample from @context
    def sample_dkeys(params={})
      icarrier = params[:icarrier] || -1
      ilot = params[:ilot] || -1
      ceqp = @context[:Equipment]
      cfoup = @context[:JobSetup][:SiViewControlJob][:FOUP][icarrier]
      clot = cfoup[:Lot][ilot]
      dkeys = {'MTool'=>ceqp['id'], 'Foup'=>cfoup['id'], 'MOperationID'=>clot['operationID'], 'SubLotType'=>clot['subLotType']}
      # collect MTime and MSlot
      if params[:wafer]
        wi = clot[:Wafer].find_index {|w| w['id'] == params[:wafer]}
        wafer = clot[:Wafer][wi]
        sloti = wafer[:WaferHistory].find_index {|wh| wh['locationType'] == 'FOUP' && !wh['exitTime'].nil?}
        dkeys['MSlot'] = wafer[:WaferHistory][sloti]['location'].to_s
      end
      # get MTime
      pi = @parameters.find_index {|p| p['name'] == params[:parameter]}
      if pi
        wi = @parameters[pi][:Reading].find_index {|r| r[:Wafer] == params[:wafer] or r[:Lot] == clot}
        dkeys['MTime'] = Time.parse(@parameters[pi][:Reading][wi]['receivedTimeStamp']) if wi
      end
      return dkeys
    end

    # return an Hash as from Space::SpcApi::Sample from @context, optionally filtered and remapped by the mapping hash {newkey: stdkey}
    def sample_generickeys(params={})
      icarrier = params[:icarrier] || -1
      ilot = params[:ilot] || -1
      clot = @context[:JobSetup][:SiViewControlJob][:FOUP][icarrier][:Lot][ilot]
      # key names are as in the generic key table (DB, spec)
      ret = {
        'dcparmorpath'=>@context['dcpArmorPath'],
        'dcparmorpathwithoutversion'=>File.dirname(@context['dcpArmorPath']),
        'dcpname'=>@context['dcpPlanName'],
        'event'=>@context['eventReportingName'],
        'timestampstring'=>@context['reportingTimeStamp'],
        'equipment'=>@eqp,
        'port'=>@context[:Equipment][:Port]['equipmentPortID'],
        'PToolChamber'=>@eqp + '-' + processing_areas.join(':'),
        'facility'=>'-',
        'PMachineRecipe'=>clot['machineRecipeID'],
        'customer'=>clot['customer'],
        'department'=>clot['department'] || '-',
        'logicalrecipe'=>clot['logicalRecipeID'],
        'lotid'=>clot['id'],
        'mainprocess'=>clot['route'],
        'machinerecipe'=>clot['machineRecipeID'],
        'manufacturinglayer'=>clot['manufacturingLayer'],
        'operationname'=>clot['operationName'],
        'operationnumber'=>clot['operationNumber'],
        'operationpasscount'=>clot['passCount'],
        'operator'=>'-',
        'process'=>clot['operationID'],
        'product'=>clot['productID'],
        'productgroup'=>clot['productGroupID'],
        'SubLotType'=>clot['subLotType'],
        'technology'=>clot['technology'],
        'SpecID'=>'Please check manually!',
        'SpecIDandRev'=>'Please check manually!',
      }
      cj = @context[:JobSetup][:SiViewControlJob]
      if cj
        ret.merge!('controljob'=>cj['id'])
      end
      sapjob = @context[:JobSetup][:SAPJob]
      if sapjob
        ret.merge!(
          'sapdcpid'=>sapjob[:SAPdataCollectionPlanID],
          'sapordernumber'=>sapjob[:SAPOrderNumber],
          'saporderoperation'=>sapjob[:orderOperation],
          'sapinspectioncharacteristicid'=>sapjob[:inspectionCharacteristicID]
        )
      end
      empty = params[:empty] || []
      empty = [empty] if empty.kind_of?(String)
      empty.each {|k| ret[k] = '-' if ret.has_key?(k)}
      mapping = params[:mapping]
      return ret unless mapping
      refhash = mapping.clone
      refhash.each_pair {|k, v| refhash[k] = ret[v]}
      return refhash
    end

    # build XML representation

    def OLD_hash2xml(t0, tagname, h)
      return h.collect {|e| _hash2xml(t0, tagname, e)} if h.kind_of?(Array)
      attrs = {}
      tt = []
      h.each_pair {|k, v|
        next if v.nil?
        if v.kind_of?(String) or v.kind_of?(Numeric) or v == true or v == false
          attrs[k] = v
        elsif v.kind_of?(Array) or v.kind_of?(Hash)
          tt << k
        else
          $log.warn "_hash2xml: unsupported data: #{k.inspect}, #{v.inspect}"
        end
      }
      if tt.size > 0
        t0.tag!(tagname, attrs) {|subtag|
          tt.each {|t| _hash2xml(subtag, t, h[t])}
        }
      else
        return t0.tag!(tagname, attrs)
      end
    end

    def OLDto_xml(params={})
      xml = Builder::XmlMarkup.new(indent: 2)
      xml.instruct!
      @context['reportingTimeStamp'] = Time.now.utc.iso8601(3) unless params[:keep_timestamp]
      xml.tag!('SOAP-ENV:Envelope', 'xmlns'=>'http://www.amd.com/schemas/DataCollectionReport/DCR_2_2_0SPACE',
        'xmlns:SpaceSubmitRunPort'=>'http://www.globalfoundries.com/space',
        'xmlns:SOAP-ENV'=>'http://schemas.xmlsoap.org/soap/envelope/') {|enve|
        enve.tag!('SOAP-ENV:Body') {|body|
          body.SpaceSubmitRunPort :submitRun,
            'SOAP-ENV:encodingStyle'=>'http://schemas.xmlsoap.org/soap/encoding/' do |run|
            _hash2xml(run, 'in0', {Context: @context, Parameter: @parameters})
          end
        }
      }
      return xml.target!
    end

    def to_xml(params={})
      @context['reportingTimeStamp'] = Time.now.utc.iso8601(3) unless params[:keep_timestamp]
      h = {'SOAP-ENV:Envelope'=>{
        'xmlns'=>'http://www.amd.com/schemas/DataCollectionReport/DCR_2_2_0SPACE',
        'xmlns:SpaceSubmitRunPort'=>'http://www.globalfoundries.com/space',
        'xmlns:SOAP-ENV'=>'http://schemas.xmlsoap.org/soap/envelope/',
        :'SOAP-ENV:Body'=>{
          :'SpaceSubmitRunPort:submitRun'=>{
            'SOAP-ENV:encodingStyle'=>'http://schemas.xmlsoap.org/soap/encoding/',
            in0: {Context: @context, Parameter: @parameters}
          }
        }
      }}
      return hash_to_xml(h)
    end

  end
end
