=begin
Wrapper around the Xsite commands.
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.
    
Author: Daniel Steger, 2012-06-22

History:
  2012-06-22 dsteger, Initial version recreated
=end

#unless $log
#  puts "Creating logger"
#  require 'logger'
#  $stdout.sync = true
#  $log = Logger.new($stdout)
#  $log.datetime_format = "%Y-%m-%d %H:%M:%S "
#  $log.level = Logger::INFO
#end

require 'wireformat'
require 'mbx'


$e10map = Hash["3OTH", "ENG-OTHER",
        "3DEV", "ENG-PROD-DEV",
        "IDLE", "IDLE",
        "6ESE", "NST-EQ-START-END",
        "6DEV", "NST-PROD-DEV-UPG",
        "6UWS", "NST-UNWORKED-SHIFT",
        "6UPG", "NST-UPGRADE",
        "1PRD", "PRD-PRODUCT",
        "1SUP", "PRD-SETUP",
        "2NDP", "SBY-NO-DISPATCH",
        "2SUP", "SBY-SETUP",
        "2WPR", "SBY-WT-PRODUCT",
        "4CNS", "SDT-CONS-CHG",
        "4CON", "SDT-CONT",
        "4DLY", "SDT-DELAY",
        "4FAC", "SDT-FACILITY",
        "4MTN", "SDT-MAINT",
        "4PMQ", "SDT-PM-QUAL",
        "4QUL", "SDT-QUAL",
        "4SUP", "SDT-SETUP",
        "4WCO", "SDT-WT-CONT",
        "4WFC", "SDT-WT-FACILITY",
        "4WMT", "SDT-WT-MAINT",
        "5CMQ", "UDT-CM-QUAL",
        "5CNS", "UDT-CONS-CHG",
        "5CON", "UDT-CONT",
        "5DLY", "UDT-DELAY",
        "5MTN", "UDT-MAINT",
        "5NTL", "UDT-NON-TOOL",
        "5WCO", "UDT-WT-CONT",
        "5WMT", "UDT-WT-MAINT",
        "5WNP", "UDT-WT-NO-PARTS",
        "5WNT", "UDT-WT-NON-TOOL"]

# Wrapper around the Xsite commands.
class XsiteError < StandardError
  def initialize(error_msg)
    super(error_msg.inspect)
  end
end

# Wrapper around the Xsite commands.
class EasyXsite
  
  attr_accessor :env, :xsite
  
  # env is either a string like "xsite.adc043"
  def initialize(env, params={})
    @env = env
    @xsite = Xsite::MBX.new(env)    
    @user = "dsteger"#"FwAdmin"
  end
  
  # get equipment schedule, returns a hash of entries
  def get_schedule(params={})
    include_wr = (params[:work_requests] or false)
    include_wo = (params[:work_orders] or false)
    children = (params[:children] or false)
    work_templates = (params[:work_templates] or false)
    future_wo = (params[:future_workorders] or false)

    eqps = (params[:eqp] or [])
    eqps = [eqps] if eqps.is_a?(String)
    
    hash = {}
    hash["includeWorkRequests"] = include_wr
    hash["includeWorkOrders"] = include_wo
    hash["filterEquipment"] = eqps     
    hash["includeChildrenEquipment"] = children
    hash["includeWorkTemplates"] = work_templates
    hash["includeFutureWorkOrders"] = future_wo
    hash["filterReasonCodes"] = params[:reason_codes] if params[:reason_codes]

    res = _sendreceive("XsiteGetScheduleTxn", hash)
    return res["value"] if res.has_key?("value")
  end
  
  # returns the current equipment status in Xsite
  def eqp_check_status(eqp, params={})    
    req = { "name"=>eqp }
    res = _sendreceive("FwCheckStatusTxn", req, params)
    return res if params[:raw]
    return res["value"]
  end
  
  def eqp_change_state(eqp, new_state)
    # set the equipment's E10 status in Xsite if the equipment has no state model
    reply = @xsite.send_msg "FwChangeStateTxn", ">>L FwChangeStateTxn name=#{eqp} state=#{state}"
    return @xsite.parse_msg(reply)
  end
  
  # for recovery only
  def eqp_force_attr(eqp, attributes)
    # set the equipment's E10 status in Xsite if the equipment has no state model
    req = { "userName"=>@user,
      "name"=>eqp,
      "attributes"=>attributes }
    return _sendreceive("FwEqpForceAttributeTxn", req)
  end
  
  def eqp_force_state(eqp, new_state)
    $log.info "eqp_force_state '#{eqp}', #{new_state.inspect}"
    eqp_force_attr(eqp, {"state"=>new_state})  
  end
  
  # changes attributes of equipment
  # specify only attributes which need to change
  def eqp_modify_attr(eqp, attributes, params={})
    req = { "userName"=>@user,
      "name"=>eqp,
      "attributes"=>attributes }
    return _sendreceive("FwEquipmentModifyAttributeTxn", req)
  end

  # set the equipment's E10 status in Xsite based on the state model
  def eqp_event(eqp, event)    
    req = { "userName"=>@user,
      "name"=>eqp,
      "eventName"=>event }
    return _sendreceive("FwEqpEventTxn", req)
  end

  # query equipment history
  def eqp_query_history(eqp, params={})
    starttime = (params[:begin] or Time.now - 3600)
    endtime = (params[:end] or Time.now)

    req = {"name"=>eqp}
    req["event"] = params[:event] if params[:event]
    req["startDateTime"] = starttime.strftime "%Y%m%d %H%M%S000"
    req["endDateTime"] = endtime.strftime "%Y%m%d %H%M%S000"
    req["previousTxns"] = (params[:num_txn] or 100)

    begin
      res = _sendreceive("FwQueryHistoryTxn", req)
      return res["value"]
    rescue XsiteError
      return []
    end
  end

  # create a new workorder for the specified equipment
  def create_work_order(eqp, params={})
    title = (params[:title] or "Automated workorder")
    activity = (params[:activity] or "PM")
    subactivity = (params[:subactivity] or "MAINT")
        
    req = { "method"=>"fromScratch",
      "userName"=>@user,
      "comments"=>"Automated test",
      "equipmentName"=>eqp,
      "title"=>title,
      "reasonCodes"=>{"ActivityType"=>activity, "SubActivityType"=>subactivity}
    }
    if params[:earlydue]
      req["dateTargetEarly"]=params[:earlydue]
      req["dateTargetStart"]=params[:earlydue] + 60
      req["dateTargetEnd"]=params[:earlydue] + 120
      req["dateTargetLate"]=params[:earlydue] + 120
      req["performWindowEarlyDueEvent"]=true
    end
    if params[:due]
      req["dateTargetStart"]=params[:due]
      req["dateTargetEnd"]=params[:due] + 60
      req["dateTargetLate"]=params[:due] + 60
      req["performWindowDueEvent"]=true
    end
    if params[:overdue]
      req["dateTargetLate"]=params[:overdue]
      req["performWindowOverdueEvent"]=true
      req["windowOverdueEventComment"]="Automated overdue"
    end
    
    res = _sendreceive("XsiteCreateWorkOrderTxn", req)
    return res["value"].value if res.has_key?("value")
  end
  
  def cancel_work_order(workorder)
    req = {
      "workOrderNumber"=>workorder,
      "userName"=>@user,
      "rescheduleNext"=>false,
      "reasonCodes"=>{"CancellationReason"=>"NO_PROBLEM_FOUND"},
      "comments"=>"Automated cancelation",
      "addCommentsToWorkOrder"=>true
    }
    _sendreceive("XsiteCancelWorkOrderTxn", req)
  end
  
  def activate_work_order(workorder)
    req = {
      "workOrderNumber"=>workorder,
      "userName"=>@user,
      "comments"=>"Automated activation",
      "addCommentsToWorkOrder"=>true
    }
    res = _sendreceive("XsiteActivateWorkOrderTxn", req)
    return (res.has_key?("value") and res["value"].value == 1)
  end
  
  def clockon_work_order(workorder)
    req = {
      "workOrderNumber"=>workorder,
      "userName"=>@user,
      "comments"=>"Automated clock on",
      "addCommentsToWorkOrder"=>true
    }
    res = _sendreceive("XsiteClockOnWorkOrderTxn", req)
    return (res.has_key?("value"))
  end
  
  def clockoff_work_order(workorder, params={})
    all = (params[:all] or false)
    req = {
      "workOrderNumber"=>workorder,
      "clockOffAll"=>all,
      "userName"=>@user,
      "comments"=>"Automated clock off",
      "addCommentsToWorkOrder"=>true
    }
    res = _sendreceive("XsiteClockOffWorkOrderTxn", req)
    return (res == {})
  end
  
  # Completes the workorder with Pass/Fail
  def complete_work_order(workorder, params={})
    solution = (params[:solution] or "Pass")
    req = {
      "workOrderNumber"=>workorder,
      "userName"=>@user,
      "reasonCodes"=>{"FailureCode1"=>"WOCompletedRCC", "SolutionCode"=>solution},
      "comments"=>"Automated completion",
      "addCommentsToWorkOrder"=>true
    }
    _sendreceive("XsiteCompleteWorkOrderTxn", req)
  end
  
  def copy_checklist_to_work_order(workorder, checklist_template)
    req = {
      "workOrderNumber"=>workorder,
      "checklistTemplateNumber"=>checklist_template,
      "replace"=>false,
      "position"=>-1,
      "userName"=>@user      
    }
    _sendreceive("XsiteCopyChecklistFromTemplateTxn", req)   
  end
  
  # start qualification
  def activate_checklist(workorder, checklist_index=1)
    req = {
      "workOrderNumber"=>workorder,
      "checklistNumber"=>checklist_index,
      "userName"=>@user,
      "comments"=>"Automated checklist",
      "addCommentsToWorkOrder"=>true
    }
    _sendreceive("XsiteActivateChecklistTxn", req)
  end
  
  def begin_checklist(workorder, checklist_index=1)
    req = {
      "workOrderNumber"=>workorder,
      "checklistNumber"=>checklist_index,
      "userName"=>@user,
      "comments"=>"Automated checklist started",
      "addCommentsToWorkOrder"=>true
    }
    _sendreceive("XsiteBeginChecklistTxn", req)
  end
  
  
  # complete each checklist step
  def complete_checkliststep(workorder, checklist_index=1, step=1)
    req = {
      "workOrderNumber"=>workorder,
      "checklistNumber"=>checklist_index,
      "step"=>step,
      "userName"=>@user,
      "completedDate"=>Time.now,
      "succeeded"=>true,
      "comments"=>"Automated step"
    }
    _sendreceive("XsiteCompleteChecklistStepTxn", req)
  end
  
  def complete_checklist(workorder, checklist_index=1)
    req = {
      "workOrderNumber"=>workorder,
      "checklistNumber"=>checklist_index,
      "userName"=>@user,
      "completedDate"=>Time.now,
      "succeeded"=>true,
      "comments"=>"Automated checklist completed",
      "addCommentsToWorkOrder"=>true
    }
    _sendreceive("XsiteCompleteChecklistTxn", req)
  end

  def fetch_work_orders(workorders, params={})
    workorders = [workorders] unless workorders.is_a?(Array)
    req = {
      "workOrderNumbers"=>workorders,
      "includeActuals"=>false,
      "includeComments"=>true,
      "includeWorkRequests"=>(params[:workorders] or false),
      "includeChecklists"=>true,
      "includeChecklistSteps"=>true,
      "includeEstimates"=>false,
      "includeHistory"=>(params[:history] or false)
    }
    res = _sendreceive("XsiteFetchWorkOrderTxn", req)
    return res["value"] if res.has_key?("value")
  end

  # Creates a new work request and returns the ID or nil
  def create_work_request(eqp, params={})
    title = (params[:title] or "Automated workrequest")
    activity = (params[:activity] or "UNSCHED")
    subactivity = (params[:subactivity] or "MAINT")

    req = {
      "equipmentName"=>eqp,
      "reasonCodes"=>{"ActivityType"=>activity, "SubActivityType"=>subactivity},
      "title"=>title,
      "comments"=>"Automated test",
      "userName"=>@user
    }
    res = _sendreceive("XsiteCreateWorkRequestTxn", req)
    return res["value"].value if res.has_key?("value")
  end
  
  # Add a work request to a work order
  def add_work_request_to_work_order(work_request, work_order, params={})
    req = {
      "workRequestNumber"=>work_request,
      "workOrderNumber"=>work_order,
      "userName"=>@user,
      "comments"=>"Automated Workrequest to Workorder",
      "addCommentsToWorkRequest"=>true,
      "addCommentsToWorkOrder"=>true
    }
    _sendreceive("XsiteAddWorkRequestToWorkOrderTxn", req)    
  end

  def cancel_work_request(workrequest, params={})
    req = {
      "workRequestNumber"=>workrequest,
      "userName"=>@user,
      "reasonCodes"=>{"CancellationReason"=>"NO_PROBLEM_FOUND"},
      "comments"=>"Automated cancelation",
      "addCommentsToWorkRequest"=>true
    }
    _sendreceive("XsiteCancelWorkRequestTxn", req)
  end

  def execute_server_side_rule(rule, params={})
    
    req = { "name" => rule,
      "attributes" => { :eventName => "WBTQ_DisplayStartedLots", 
      :guid => "000000000012",
      :items => { :checkListStep => "3", :returnCode => "100" },
      :messages => [ "OK", "TEST", "TEST2" ]    
    }}

    _sendreceive("FwExecuteServerSideRuleTxn", req)
  end
  
  def execute_server_side_rule_invalid()
    request = ">>L XsiteCreateWorkOrderTxn
      method=\"fromScratch\"
      userName=\"#{@user}\"
      comments=\"Automated Test\"
      equipmentName=\"#{eqp}\"
      title=\"Automated work order\"
      reasonCodes={ { class=ASSOC { class=A1 \"ActivityType\" } { class=A1 \"PM\" } } { \"SubActivityType\" \"MAINT\" } }"
    reply = @xsite.send_msg "XsiteCreateWorkOrderTxn", request
    $log.info "#{reply}"
    return @xsite.parse_msg(reply)
  end
  
  def guess_type(v)
    if v.is_a?(Integer)
      v_str = "S4"
    elsif v.is_a?(Float)
      v_str = "F4"
    elsif v.is_a?(Time)
      v_str = "DTM"
    elsif v.is_a?(Hash)
      v_str = "ASSOC"
    elsif v.is_a?(Array)
      v_str = "ORDERED"
    else  
      v_str = "A1"
    end
  end
  
  def _value2str(value,  print_type=false)
    ret = ""
    ret += "{ class=#{guess_type(value)} " if print_type
    if value == true 
      v_str = "T" 
    elsif value == false
      v_str = "F"
    elsif value.is_a?(String)
      v_str = "\"#{value}\""
    elsif value.is_a?(Time)
      v_str = value.strftime "{ date=%Y%m%d time=%H%M%S }"    
    else
      v_str = value.to_s
    end
    ret += v_str
    ret +=" }" if print_type
    return ret
  end
  
  def _assoc2wiredX(hash, start=true)
    req = "" 
    req += "{ class=ORDERED " if start
    prev_k_type = nil; prev_v_type = nil
    hash.each_pair do |p,v|
      k_type = guess_type(p)
      v_type = guess_type(v)
      if k_type != prev_k_type or v_type != prev_v_type
        req += " { class=ASSOC { #{_value2str(p, true)} } { #{_value2str(v, true)} } }"
        prev_k_type = k_type
        prev_v_type = v_type
      else
        req += " { #{_value2str(p)} #{_value2str(v)} }"
      end
    end
    req += " }" if start
    return req
  end
  
  def _ordered2wiredX(a)
    req = "{ "
    prev_v_type = nil
    a.each do |p|      
      v_type =  guess_type(p)
      if v_type != prev_v_type
        req += "class=ORDERED { #{_value2str(p, true)} }"
        prev_v_type = v_type
      else
        req += " { #{_value2str(p)} }"
      end
    end
    req += " }"
    return req
  end

  def _props2wiredX(hash)
    req = ""
    hash.each_pair do |p,v|
      if v.is_a?(Hash)
        v_str = _assoc2wired(v)
      elsif v.is_a?(Array)
        v_str = _ordered2wired(v)
      else
        v_str = _value2str(v)
      end
      req +=" #{p}=#{v_str}"
    end
    return req
  end
  
  def _ordered2wired(a, start=true)
    req = "{ class=ORDERED" if start
    prev_v_type = nil
    a.each do |p|      
      v_type =  guess_type(p)
      if v_type != prev_v_type
        req += " #{_prop2wired(p, true)}"
        prev_v_type = v_type
      else
        req += " #{_value2str(p)} "
      end
    end
    req += " }" if start
    return req
  end
  
  def _assoc2wired(hash, start=true)
    req = "" 
    req += "{ class=ORDERED { class=ASSOC" if start
    prev_k_type = nil; prev_v_type = nil
    hash.each_pair do |p,v|
      k_type = guess_type(p)
      v_type = guess_type(v)
      if k_type != prev_k_type or v_type != prev_v_type
        req += " #{_prop2wired(p, true)} #{_prop2wired(v, true)} }"
        prev_k_type = k_type
        prev_v_type = v_type
      else
        req += " { #{_value2str(p)} #{_value2str(v)} }"
      end
    end
    req += " }" if start
    return req
  end
  
  def _prop2wired(v, show_type)
    v_str = ""
    if v.is_a?(Hash)
      v_str = _assoc2wired(v, true)
    elsif v.is_a?(Array)
      v_str = _ordered2wired(v, true)
    else
      v_str = _value2str(v, show_type)
    end    
    return v_str
  end
  
  def _props2wired(properties)
    req = ""
    properties.each_pair {|k,v| req += " #{k}=#{_prop2wired(v, false)}"}
    return req
  end
  
  def FwCheckStatusTxn(eqp)
    _sendreceive("FwCheckStatusTxn", {"name"=>eqp})
  end
  
  def _sendreceive(function, properties, params={})
    msg = ">>L #{function}" + _props2wired(properties)
    $log.debug "Send #{function}: \"#{msg}\""  
    #return
    reply = @xsite.send_msg function, msg
    #return @xsite.parse_msg(reply)    
    return reply if params[:raw]
    res = Xsite::Parser.new.parse(reply)
    if res.has_key?("error")
      raise XsiteError.new res["error"]
    end
    return res
  end
  
  def disconnect
    @xsite.disconnect
  end
  
end


