=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

    
Author: Steffen Steidten, 2012-05-09

History:
  2015-07-02 ssteidte,  added lot op and state sync with AsmView to btfturnkey_move_wait
  2016-08-05 sfrieske,  moved btfturnkey test methods to turnkeybtftest.rb
=end

require 'asmview'
require 'siview'
require 'asmviewjcap'
require 'asmerpshipment'
require 'asmturnkey'
require 'turnkey'
require 'turnkeybtf'
require 'misc'


# AsmView Testing Support
module AsmView
  
  # AsmView jCAP Testing Support
  class Test
    attr_accessor :env, :av, :avsm, :product, :avship, :avlotcomplete, :avb2b, :avstb, 
      :f1sv, :f1b2b, :f1branch, :f8sv, :f8b2b, :f8branch, :f7sv, :f7b2b, :f7branch
    
    def initialize(env, params={})
      @env = env
      ##@product = params[:product] || "TKEY01.A0"
      @av = AsmView::MM.new(env, params)
      ## not yet used @avsm = AsmView::SM.new(env)
      @avb2b = AsmView::TurnKeyB2B.new(env, {av: @av}.merge(params))
      @avstb = AsmView::TurnKeySTB.new(env, {av: @av}.merge(params))
      if params[:erpshipment]
        @avship = AsmView::Shipment.new(env, {av: @av}.merge(params))
        @avlotcomplete = AsmView::LotComplete.new(env, {av: @av}.merge(params))
      end
      if params[:f1] || params[:btf]
        @f1sv = SiView::MM.new("f1.#{env}")
        fabparams = {sv: @f1sv}.merge(params[:f1params] || {})
        @f1b2b = Turnkey::B2B.new("f1.#{env}", fabparams)
        @f1branch = Turnkey::BranchStart.new("f1.#{env}", fabparams)
      end
      if params[:f7]
        @f7sv = SiView::MMF7.new("f7.#{env}")
        fabparams = {sv: @f7sv, release_reason: 'GCIM', scrapcancel_code: 'EVAL'}.merge(params[:f7params] || {})
        @f7b2b = Turnkey::B2B.new("f7.#{env}", fabparams)
        @f7b2b.banks[:ship] = 'SHIP'
      end
      if params[:f8]
        @f8sv = SiView::MM.new("f8.#{env}")
        fabparams = {sv: @f8sv}.merge(params[:f8params] || {})
        @f8b2b = Turnkey::B2B.new("f8.#{env}", fabparams)
        @f8branch = Turnkey::BranchStart.new("f8.#{env}", fabparams)
        @f8b2b.banks[:end_std] = 'O-ENDFG'
        @f8b2b.banks[:ship] = 'O-SHIP'
      end
    end
    
    def inspect
      "#<#{self.class.name} env: #{@env}>"
    end
    
    def check_connectivity(params={})
      $log.info "check_connectivity"
      ok = true
      #
      [@av, @f1sv, @f7sv, @f8sv].each {|s|
        next unless s
        res = s.logon_check
        ($log.warn "  not connected or wrong user"; ok = false) if res != 0
      }
      [@avstb, @avb2b, @avship, @avlotcomplete, @f1b2b, @f1branch, @f7b2b, @f8b2b, @f8branch].each {|s|
        next unless s
        res = s.get_version
        $log.info "  #{res.inspect}"
        ($log.warn "  not connected"; ok = false) unless res =~ /\d\.\d/
      }
      #
      return ok
    end
    
    # get env specific connections based on :fabcode parameter or lot script parameter
    def get_env_connections(lot, params={})
      fabcode = (params[:fabcode] or @avb2b.lot_fabcode(lot)).downcase
      sv = send("#{fabcode}sv")
      b2b = send("#{fabcode}b2b")
      branch = send("#{fabcode}branch")
      return sv, b2b, branch
    end

    def lot_set_tkinfo(lot, tkinfo, params={})
      sv, b2b, branch = get_env_connections(lot, params)
      @avb2b.lot_set_tkinfo(lot, tkinfo) && b2b.lot_set_tkinfo(lot, tkinfo)
    end
    
    def lot_tkey_prepare(lot, params={})
      sv, b2b, branch = get_env_connections(lot, params)
      @avb2b.lot_tkey_prepare(lot, params) && b2b.lot_tkey_prepare(lot, params)
    end
    
    # verify lot has been created correctly
    def verify_asmview_lot(lot, params={})
      $log.info "verify_asmview_lot #{lot.inspect}, #{params.inspect}"
      ($log.warn "  lot not created in #{@av.inspect}"; return nil) unless @av.lot_exists?(lot)
      sv, b2b, branch = get_env_connections(lot, params)
      flc = b2b.lot_characteristics(lot, wafers: true)
      alc = @avb2b.lot_characteristics(lot, wafers: true)
      ($log.warn "  wafer count mismatch, #{alc.wafers.size} instead of #{flc.wafers.size}"; return false) if alc.wafers.size != flc.wafers.size
      ret = true
      flc.wafers.each_with_index {|svw, i|
        walias = (@avb2b.lot_fabcode(lot) == "F7") ? svw.alias : "#{lot.split('.')[0]}.#{svw.alias}"
        avw = params[:checkposition] ? alc.wafers[i] : alc.wafers.find {|w| w.wafer == svw.wafer}
        ($log.warn "    no av wafer matching #{svw}"; ret=false; next) unless avw
        ($log.warn "    wrong wafer alias, #{avw.alias.inspect} instead of #{walias.inspect}"; ret=false) if avw.alias != walias
      }
      $log.info "  turnkey type and subcons"
      ($log.warn "    mismatch: #{alc.tkinfo} instead of #{flc.tkinfo}"; ret=false) if alc.tkinfo != flc.tkinfo
      return ret if params[:noproctime]
      $log.info "  proctime"
      lp = @avstb.lot_proctime(lot)
      ($log.warn "    not set"; return false) unless lp
      proctime = params[:proctime] || Time.now
      proctime = Time.parse(proctime) if proctime.kind_of?(String)
      spread = params[:spread] || 65
      ($log.warn "    wrong, #{lp.utc.iso8601} instead of #{proctime.utc.iso8601}"; ret=false) if (lp - proctime).abs > spread
      #
      return ret
    end
    
    def stb_verify(lot, route, params={})
      @avstb.stb(lot, params) || return 
      verify_asmview_lot(lot, params) || return
      @avstb.lot_tkey_started?(lot, route) || return
      return true
    end

    def hold_verify(lot, holdcode, params={})
      sv, b2b, branch = get_env_connections(lot, params)
      @avb2b.hold_verify(lot, holdcode, params) || return
      return true if @av.lot_info(lot).content == 'Chip'
      b2b.verify_lot_hold(lot, true, params)
    end

    def release_verify(lot, params={})
      sv, b2b, branch = get_env_connections(lot, params)
      @avb2b.release_verify(lot, params) || return
      b2b.verify_lot_hold(lot, false)
    end
    
    def split_waferlot_verify(lot, nwafers, params={})
      sv, b2b, branch = get_env_connections(lot, params)
      child = @avb2b.split_waferlot(lot, nwafers, params)
      b2b.verify_lot_split(child, nwafers) || return
      verify_asmview_lot(child, {proctime: @avstb.lot_proctime(lot)}.merge(params)) || return
      return child
    end
    
    def merge_verify(lot, child, params={})
      sv, b2b, branch = get_env_connections(lot, params)
      @avb2b.merge_verify(lot, child, params) || return
      b2b.verify_lot_merge(child)
    end

    def move_verify(action, lot, exp_op, exp_status, params={})
      sv, b2b, branch = get_env_connections(lot, params)
      @avb2b.move_verify(action, lot, exp_op, exp_status, params) || return
      return true if @av.lot_info(lot).content != 'Wafer'
      # in F7, lots get automatically moved on from PTKYQACHECK.1
      li = sv.lot_info(lot)
      if li.op == b2b.ops[:qacheck] # as of 13.9 quality check only exists in fab 7
        $log.info "waiting for lot to leave op #{b2b.ops[:qacheck].inspect} in Fab7 SiView"
        res = wait_for(timeout: 840) {sv.lot_info(lot).op != b2b.ops[:qacheck]}
        ($log.warn("#{lot} is still at operation #{b2b.ops[:qacheck].inspect}"); return false) unless res
        exp_status = 'Waiting' if params[:fabcode] == 'F7'
        exp_op = b2b.ops[:tkbankassy] unless exp_status == 'COMPLETED' 
        params[:fabop] = exp_op
        params[:fabstatus] = exp_status
      end
      b2b.verify_lot_state(lot, params[:fabop], params[:fabstatus], params)
    end
  
    def scrap_waferlot_verify_cancel(lot, scrapcode, params={})
      sv, b2b, branch = get_env_connections(lot, params)
      fabli0 = sv.lot_info(lot)
      ret = @avb2b.scrap_waferlot_verify_cancel(lot, scrapcode, params)
      ret &= b2b.verify_lot_scrapped(lot)
      sv.wafer_sort_req(lot, fabli0.carrier)
      sv.scrap_wafers_cancel(lot, :reason=>b2b.scrapcancel_code)
      return ret
    end
    
    # test rejection of an attempt to send a shipToSubcon message to AsmErpShipment for a wrong lot
    #
    # now: sent by ERP to AsmErpShipment, sends STS msg to AsmTurnkey
    #
    # return true on successful rejection
    def ship_complete_wrong_lot(params={})
      $log.info "ship_complete_wrong_lot #{params.inspect}"
      lot = (params[:lot] or "XXNOTEX.00")
      fabcode = (params[:fabcode] or "F1")
      sv, b2b, branch = get_env_connections(lot, {:fabcode=>fabcode}.merge(params))
      ($log.warn "  (av) lot #{lot} exists"; return nil) if @av.lot_exists?(lot)
      ($log.warn "  (sv) lot #{lot} exists"; return nil) if sv.lot_exists?(lot)
      # ship complete message
      res = @avship.ship_complete(lot, :transstatus=>"Trans-Running", :fabcode=>fabcode)
      ($log.warn "  shipment of the not existing lot #{lot} must fail"; return false) if res
      msg = @avship._service_response[:shipCompleteResponse][:return][:details][:msg]
      ($log.warn "  wrong reply"; return false) unless msg.index("ab #{fabcode} failed")  # Fab and fab are in use
      ($log.warn "  wrong reply from ERPShip: #{msg.inspect}"; return false) unless msg.index(lot)
      # verify lot has not been created in ASM
      ($log.warn "  lot #{lot} unexpectedly created in AsmView"; return false) if $av.lot_exists?(lot)
      return true
    end
    
    # test rejection of an attempt to send a shipmentToSubCon message to AsmTurnkey for a wrong lot
    #
    # sent by AsmErpShipment to AsmTurnkey (STB) upon receipt of ship_complete from ERP
    #
    # return true on successful rejection
    def stb_wrong_lot(params={})
      $log.info "stb_wrong_lot #{params.inspect}"
      lot = (params[:lot] or "XXNOTEX.00")
      fabcode = (params[:fabcode] or "F1")
      sv, b2b, branch = get_env_connections(lot, {:fabcode=>fabcode}.merge(params))
      ($log.warn "  (av) lot #{lot} exists"; return nil) if @av.lot_exists?(lot)
      ($log.warn "  (sv) lot #{lot} exists"; return nil) if sv.lot_exists?(lot)
      # stb message
      res = @avstb.stb(lot, {:fabcode=>fabcode}.merge(params))
      ($log.warn "  shipmentToSubcon of the not existing lot #{lot} must fail"; return false) if res
      # verify response
      msg = @avstb._service_response[:shipmentToSubConResponse][:return][:reason][nil]
      ($log.warn "  wrong reply"; return false) unless msg.index("ab #{fabcode} failed")  # Fab and fab are in use
      ($log.warn "  wrong reply from AsmTurnkey: #{msg.inspect}"; return false) unless msg.index(lot)
      # verify lot has not been created in ASM
      ($log.warn "  lot #{lot} unexpectedly created in AsmView"; return false) if $av.lot_exists?(lot)
      return true
    end
    
    def lot_complete_verify(lot, params={})
      sv, b2b, branch = get_env_connections(lot, params)
      # send LotComplete msg
      @avlotcomplete.lot_complete(lot, params) || return
      # verify lot status in AsmView and SiView
      ret = @avlotcomplete.verify_lot_complete(lot, params) || return
      return true if @av.lot_info(lot).content == 'Chip'
      b2b.verify_lot_complete(lot, params)
    end
      
    def pick_release_verify(lot, params={})
      sv, b2b, branch = get_env_connections(lot, params)
      ret = @avship.pick_release(lot, params) || return
      li = @av.lot_info(lot)
      $log.info "(av) verify lot status is COMPLETED and still on the end bank"
      ($log.info "  wrong lot state #{li.states["Lot Finished State"]}"; ret=false) if li.states["Lot Finished State"] != "COMPLETED"
      ($log.info "  wrong bank #{li.bank}"; ret=false) if li.bank != @avship.banks[:end_std]
      return ret if li.content == "Chip"
      li = sv.lot_info(lot)
      $log.info "(sv) verify lot status is COMPLETED and on the ship bank"
      ($log.info "  wrong lot state #{li.states["Lot Finished State"]}"; ret=false) if li.states["Lot Finished State"] != "COMPLETED"
      ($log.info "  wrong bank #{li.bank}"; ret=false) if li.bank != b2b.banks[:ship]
      return ret
    end
    
    def pick_confirm_verify(lot, params={})
      sv, b2b, branch = get_env_connections(lot, params)
      ret = @avship.pick_confirm(lot, params) || return
      li = @av.lot_info(lot)
      $log.info "(av) verify lot status is COMPLETED and on the ship bank"
      ($log.info "  wrong lot state #{li.states["Lot Finished State"]}"; ret=false) if li.states["Lot Finished State"] != "COMPLETED"
      ($log.info "  wrong bank #{li.bank}"; ret=false) if li.bank != @avship.banks[:ship]
      return ret if li.content == "Chip"
      li = sv.lot_info(lot)
      $log.info "(sv) verify lot status is COMPLETED and on the ship bank"
      ($log.info "  wrong lot state #{li.states["Lot Finished State"]}"; ret=false) if li.states["Lot Finished State"] != "COMPLETED"
      ($log.info "  wrong bank #{li.bank}"; ret=false) if li.bank != b2b.banks[:ship]
      return ret
    end
    
    # 1st leg shipment to subcon,
    # verify that the lot is STBed and started in ASMView
    def ship_subcon_verify(lot, route, params={})
      @avship.ship_complete(lot, {transstatus: 'Trans-Running'}.merge(params)) || return
      verify_asmview_lot(lot, params) || return
      @avstb.lot_tkey_started?(lot, route)
    end
    
    def ship_confirm_verify(lot, params={})
      sv, b2b, branch = get_env_connections(lot, params)
      ret = @avship.ship_complete(lot,  {transstatus: 'Shipped'}.merge(params)) || return
      li = @av.lot_info(lot)
      $log.info "(av) verify lot status is SHIPPED" 
      ($log.warn "  wrong, status is #{li.status}"; ret=false) if li.status != "SHIPPED"
      return true if li.content == 'Chip'
      li = sv.lot_info(lot)
      $log.info "(sv) verify lot status is SHIPPED" 
      ($log.warn "  wrong, status is #{li.status}"; ret=false) if li.status != "SHIPPED"
      return ret
    end
    
    # move along a Turnkey branch route
    #
    # return true on success
    def subroute_moves_verify(lot, tktype, stage, params={})
      $log.info "subroute_moves_verify #{lot.inspect}, #{tktype.inspect}, #{stage.inspect}, #{params.inspect}"
      sv, b2b, branch = get_env_connections(lot, params)
      fabop = case stage
      when :tkbump; b2b.ops[:tkbankbump]
      when :tksort; (tktype == 'S') ? b2b.ops[:tkbanksort_S] : b2b.ops[:tkbanksort]
      when :tksrt2; b2b.ops[:tkbanksrt2]
      when :tkassy; b2b.ops[:tkbankassy]
      end
      #
      if [['J', :tkbump], ['L', :tkbump], ['M', :tkbump], ['R', :tkbump], ['S', :tksort], ['AA', :tkbump], ['AB', :tkassy]].member?([tktype, stage])
        # start at the first subroute's start
        ##ops = @av.lot_route_operations(lot).collect {|o| o.op}
        ops = @av.lot_operation_list(lot).collect {|o| o.op}
        move_verify('start', lot, ops[1], 'Processing', params.merge(fabop: fabop, fabstatus: 'NonProBank')) || return
        ops = ops[2..-1]
      else
        # move on to next subroute in AsmView
        exp_op = case stage
        when :tkbump; @avb2b.ops[:tranb]
        when :tksort; @avb2b.ops[:trans]
        when :tksrt2; @avb2b.ops[:trans2]
        when :tkassy; @avb2b.ops[:trana]
        end
        move_verify('move', lot, exp_op, 'Processing', params.merge(fabop: fabop, fabstatus: params[:fabcode]=='F7' ? 'Waiting' : 'NonProBank')) || return
        ##ops = @av.lot_route_operations(lot).collect {|o| o.op}[1..-1]
        ops = @av.lot_operation_list(lot).collect {|o| o.op}[1..-1]
      end
      #
      # subsequent moves to the route end
      ops.each {|o| 
        move_verify('move', lot, o, 'Processing', params.merge(fabop: fabop, fabstatus: 'NonProBank')) || return
      }
      #
      # complete at the last subroute's end
      if [['J', :tksort], ['L', :tkbump], ['M', :tkassy], ['R', :tksrt2], ['S', :tksort], ['AA', :tkbump], ['AB', :tkassy]].member?([tktype, stage])
        p1 = params[:fabcode] == 'F7' ? {fabop: 'DUMMY1.01', fabstatus: 'COMPLETED'} : {fabop: b2b.ops[:lotshipdesig], fabstatus: 'Waiting'}
        move_verify('complete', lot, 'LOTSHIPD.1', 'ONHOLD', params.merge(p1)) || return
      end
      return true
    end

    # send B2B start, move and complete events to AsmTurnkey and verify lot moves in the Fab
    #
    # return true on success, false on error, nil if setup is wrong
    def b2b_moves_verify(lot, params={})
      sv, b2b, branch = get_env_connections(lot, params)
      tktype = params[:tktype] || b2b.lot_tkinfo(lot).tktype.split('-')[-1]
      ok = true
      ok &= subroute_moves_verify(lot, tktype, :tkbump, params) if ['L', 'J', 'M', 'R', 'AA'].member?(tktype)
      ok &= subroute_moves_verify(lot, tktype, :tksort, params) if ['S', 'J', 'M', 'R'].member?(tktype)
      ok &= subroute_moves_verify(lot, tktype, :tksrt2, params) if ['R'].member?(tktype)
      ok &= subroute_moves_verify(lot, tktype, :tkassy, params) if ['M', 'AB'].member?(tktype)
    end
    
  end
  
end
