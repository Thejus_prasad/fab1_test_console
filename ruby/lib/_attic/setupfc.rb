=begin
 automated test of setup.FC API 

(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author:  Ulrich Koerner, 2011-08-01

History:
  2012-01-27 ssteidte,  copied BaseSoap from separate file here
  2014-07-11 dsteger, converted to xmlhash function
=end

require 'base64'
require 'rqrsp'
require 'builder'
require 'misc'

# automated test of setup.FC API
class SetupFcError < StandardError
end

class RequestDifferenceError < StandardError
end

# automated test of setup.FC API
class EasySetupFC
  attr_accessor :service, :support, :rest, :env, :user

  @@department     = "ALL"

  TaskNames = {
    :approve => "gfwf:approve",
    :acknowledge => "gfwf:acknowledge",
    :edit_content => "gfwf:edit-spec-content",
    :edit_metadata => "gfwf:edit-spec-metadata",
    :review => "gfwf:review-author",
    :terminate => "gfwf:terminate-spec"
  }
  
  def initialize(env, params={})
    # if params[:user]
      # @user = params[:user]
    # else
      # @user = @@defaultUser
      # params[:user] = @user
    # end
    # params[:password] = @@defaultPass unless params[:password] 
    @service = ServiceSoap.new(env, params)
	  @user = @service.user
    begin
      @support = SupportSoap.new(env, params)
      $rest = @rest = RestAPI.new(env, params)
    rescue
      #all
    end
      
    @env = env 
  end
  
  def _isok?(res)
    #$log.debug "Reply: \n#{'%s' % res.pretty_inspect}"
    raise SetupFcError, res[:return][:errorDetail] if res && res.has_key?(:return) && res[:return][:success] != 'true'
    raise SetupFcError, res[:Fault][:faultstring] if res and res.has_key?(:Fault)
    return res
  end
  
  # create a new spec and returns the Id   
  def create_new_specification(sclass, subclass, workflow_config, template, params={})
    params[:title] = "Automated spec test" unless params[:title]
    params[:fabLocation] = "M" unless params[:fabLocation]
    params[:department] = "ALL" unless params[:department]
    params[:owner] = @service.user unless params[:owner]
    params[:author] = @service.user unless params[:author]
    params[:classId] = sclass
    params[:subClassId] = subclass
    params[:workflowConfig] = workflow_config
    params[:template] = template
             
    res = _isok?(@support.createSpecification({ :arg0 => params }))
    spId = res[:createSpecificationResponse][:return][:id]
    $log.info " - #{spId} specification created"
    return spId 
  end
  
  # Returns the tasks assigned to the user
  def get_user_tasks(params={})
    $log.info ("#{__method__} #{params.inspect}")
    spec_id = params[:specid]
    name = params[:name]
    
    tasks = self._isok?(@support.getUserTaskList(params))[:getUserTaskListResponse][:return]
    return self._filter_tasks(tasks, name, spec_id)
  end

  # Return all tasks for specific spec or with name
  def get_all_tasks(params={})
    spec_id = params[:specid]
    name = params[:name]

    tasks = self._isok?(@support.getAllActiveTasks(false))[:getAllActiveTasksResponse][:return]
    return self._filter_tasks(tasks, name, spec_id)
  end

  def wait_for_task(spec_id, params)
    $log.info("#{__method__} #{spec_id} #{params.inspect}")
    wp = {}
    key = :timeout; wp[key] = params.delete(key) if params.key?(key)
    key = :sleeptime; wp[key] = params.delete(key) if params.key?(key)
    key = :tstart; wp[key] = params.delete(key) if params.key?(key)
    wait_for(wp) { self.get_user_tasks(params.merge({:specid=>spec_id})).count > 0 }
  end

  # Generate specific wait methods
  TaskNames.each_key do |tn|
    define_method("wait_for_#{tn}".to_sym) { |spec_id, *arg| 
      p = arg[0]
      p = {} if p.nil?
      self.wait_for_task(spec_id, p.merge(:name=>TaskNames[tn]))
    }
  end

  
  # filter array of tasks for name and speci_id
  def _filter_tasks(tasks, name, spec_id)
    $log.debug "tasks - #{tasks.inspect}"
    tasks = [] unless tasks
    tasks = [tasks] unless tasks.kind_of? Array

    # Filter for a spec id
    tasks = tasks.find_all { |task| task[:workflowProperties][:specId] == spec_id } if spec_id

    # Filter for type
    if name
      name = TaskNames[name] if TaskNames.has_key?(name)
      tasks = tasks.find_all { |task| task[:name] == name }
    end
    # or return all
    return tasks
  end

  # Wait for a certain time actively polling and a timeout (s)
  def wait_get_user_task(params={})
    timeout = (params[:timeout] or 300)
    polling = (params[:polling] or false)
    task = nil
    remaining_time = timeout
    until task or remaining_time <= 0
      $log.debug "Going to sleep for 5s before getting the task"
      sleep 5
      remaining_time -= 5
      task = get_user_tasks(params)[0]
      break unless polling      
    end
    return task
  end
  
  # Request a spec content change
  def content_change(spec_id)
    res = _isok?(@support.workOnSpecification({ :arg0 => spec_id, :arg1 => "content_change" }))
    return res
  end
  
  # Request a spec properties change
  def properties_change(spec_id)
    res = _isok?(@support.workOnSpecification({ :arg0 => spec_id, :arg1 => "properties_change" }))
    return res
  end
  
  # Request a spec deletion
  def delete_spec(spec_id, params={})
    res = _isok?($sfc.support.deleteSpecification({ :arg0 => spec_id}, params))
    return res.has_key? :deleteSpecificationResponse
  end
  
  # Create spec reference
  def create_reference(spec_id, target_spec_id, params={})
    res = _isok?($sfc.service.createSpecificationReference(params.merge({
         :"ws:sourceSpecId" => spec_id,
         :"ws:targetSpecId" => target_spec_id # or any other existing spec
    })))[:createSpecificationReferenceResponse][:return]
    return res[:success] == "true"
  end
   
  # Delete spec reference
  def delete_reference(spec_id, target_spec_id, params={})
    res = _isok?($sfc.service.deleteSpecificationReference(params.merge({
         :"ws:sourceSpecId" => spec_id,
         :"ws:targetSpecId" => target_spec_id # or any other existing spec
    })))[:deleteSpecificationReferenceResponse][:return]
    return res[:success] == "true"
  end
  
  # Reassign the task
  def assign_task(spec_id, user, params={})
    requestor = (params[:requestor] or @user)
    polling = (params[:polling] or false)
    
    task = wait_get_user_task(:specid=>spec_id, :polling=>polling, :user=>requestor)
    #$log.info "task: #{task.inspect}"
    if not task
      $log.error "No task found for #{spec_id}"
      return
    end    
    res = _isok?(@support.transferTaskToUser(:arg0 => task[:id], :arg1 => user, :arg2 => requestor, :arg3=> "Automated test", :user=>requestor))
    
    return res
  end
  
  def get_active_workflows_for_spec(spec_id, params={})
    wfs = _isok?(@support.getActiveWorkflowsForSpec(spec_id))[:getActiveWorkflowsForSpecResponse][:return]
    wfs = [wfs] unless wfs.kind_of?(Array)
    wfs = wfs.select {|w| w[:workflowDefinition][:name] == params[:name]} if params[:name]
    return wfs
  end
  
    
  # Process user tasks
  def process_user_tasklist
    tasks = get_user_tasks
    
    tasks.each do |task|
      task_id = task[:id]
      props = task[:workflowProperties]      
      spec_id = props[:specId]
      task_name = task[:name]
      
      $log.info "Process #{spec_id}"
      case task_name
      when "gfwf:approve"
        human_workflow_action(spec_id)
      when "gfwf:edit-spec-content", "gfwf:review-author"
        human_workflow_action(spec_id, action: "CANCEL")
      when "gfwf:acknowledge"
        acknowledge(spec_id)
      else
        $log.error "Cannot process task #{task_name}"
      end      
    end
  end
  
  def do_spec_approval(spec_id)
    tasks = get_all_tasks(specid: spec_id)
    tasks.select {|t| t[:name] == "gfwf:approve"}.each do |t| 
      $log.info "process task #{t[:id]}"
      group = _isok?(@support.getPooledActorsForTask(t[:id]))[:getPooledActorsForTaskResponse][:return]
      $log.info " group is #{group}"
      user = users_for_group(group).sample
      $log.info " user is #{user}"
      human_workflow_action(spec_id, user: user, name: TaskNames[:approve])
    end
  end
  
  # Cancel an active workflow. Returns false if workflow is not cancelable or nil on error.
  #
  # internal states:
  #
  # WORKFLOW_CANCELED, // signoff successful
  #
  # NO_WORKFLOW, // spec not in signoff state
  #
  # NO_TASK, // internal
  #
  # NO_OP, // internal
  #
  # CANCEL_SUPPORTED, // cancel of signoff is possible
  #
  # CANCEL_REQUESTED,  // asynchron cancel request generated
  #
  # CANCEL_NOT_SUPPORTED,  // cancel of signoff impossible
  #
  # CANCEL_ALREADY_REQUESTED // cancel already in progress 
  def request_cancel_signoff(spec_id, params={})
    requestor = (params[:user] or @user)
    name = (params[:name] or "jbpm$gfwf:edit-spec")
    workflow = get_active_workflows_for_spec(spec_id, :name=>name)
    unless workflow.size > 0
      log.error "No active workflow found"
      return nil 
    end
    can_cancel = _isok?(@support.canCancelSignoff(spec_id, params))
    can_cancel = (can_cancel[:canCancelSignoffResponse][:return][:status]=="CANCEL_SUPPORTED")
    ($log.warn("#{__method__}: cancel impossible"); return nil) unless can_cancel    
    return _isok?(@support.cancelSignoff(spec_id, requestor))[:cancelSignoffResponse][:return][:status]
  end
  
  # Submit a new version of the spec
  def submit_properties_change(spec_id, params={})
    spec = _isok?(@service.getSpecification(spec_id))[:getSpecificationResponse][:return][:result]

    new_version = spec[:version].to_i + 1
    spec[:version] = new_version.to_s    
    spec[:changeReason] =  "Properties change"
    spec[:changeDescription] = "Automated Test"
          
    $log.debug "Content of spec:\n#{spec.inspect}" 
    res2 = _isok?(@support.submitSpecification(spec, "EDIT_SPEC_METADATA", params))
    $log.info " - #{spec_id} submitted."
    return res2, new_version
  end
    
  # Submit a new version of the spec
  def submit_termination(spec_id, params={})
    spec = _isok?(@service.getSpecification(spec_id))[:getSpecificationResponse][:return][:result]   
    new_version = spec[:version].to_i + 1
    
    # No content update  
    #content = Base64.decode64(spec["content"])
    #doc = REXML::Document.new(content)
    
    #doc[3].elements['//my:Version'].text = new_version
    #doc[3].elements['//my:ChangeReason'].text = "Terminate spec"
    #doc[3].elements['//my:ChangeDescription'].text = "Automated Test"
    
    #content2 = ""
    #doc.write content2
    #spec["content"] = Base64.encode64(content2).tr("\n","")
      
    spec[:version] = new_version.to_s
    spec[:changeReason] =  "Terminate spec"
    spec[:changeDescription] = "Automated Test"
        
    $log.debug "Content of spec:\n#{spec.inspect}" 
    res2 = _isok?(@support.submitSpecification(spec_id, spec, "TERMINATE_SPEC", params))
    $log.info " - #{spec_id} submitted."
    return res2, new_version
  end
  
  # Submit a new version of the spec (type is Base, ARMOR or SPACE)
  #
  # use :shortapproval=>true for short workflow
  def submit_specification(spec_id, params={})
    $log.info "#{__method__} #{spec_id}" #, #{params.inspect}"
    content, version = get_specification(spec_id)    
    doc = REXML::Document.new(content)
    new_version = version + 1
    type = (params[:type] or "Base")

    root = doc.elements['/my:myFields']
    ($log.error "invalid spec content, root not found."; return) unless root
    root.elements['//my:Version'].text = new_version    
    root.elements['//my:ChangeReason'].text = "Automated Test"
    root.elements['//my:ChangeDescription'].text = "Automated Test"
      
    root.elements['//my:Purpose'].text = "Automated Test"
    root.elements['//my:Scope'].text = "Automated Test"  

    if type == "ARMOR"
      _add_armor_content(doc, spec_id, new_version, params)
    elsif type == "EOPM"
      _add_eopm_content(doc, params)  #, spec_id, new_version, params)
    elsif type == "SP"
      _add_sp_content(doc, spec_id, new_version, params)
    elsif type =~ /SPACE|REPLACE/ and params[:filename]
      begin
        f = File.open(params[:filename])
        spec_content = REXML::Document.new f
        _replace_xml_element(root, spec_content.root, '//my:Content')
        _replace_xml_element(root, spec_content.root, '//my:Chapters')
        _replace_xml_element(root, spec_content.root, '//my:AuxiliaryFields')
        _replace_xml_element(root, spec_content.root, '//my:AdditionalEquipments')
        _replace_xml_element(root, spec_content.root, '//my:OperationPMSection')
        _replace_xml_element(root, spec_content.root, '//my:WBTQGroup')

        if params[:table110_mig]
          REXML::XPath.each(doc, "//my:Columns") {|e|
            sortmode = REXML::Element.new("my:TableTypeSortMode")
            sortmode.text = "Null"
            e.parent.insert_before(e, sortmode)
          }
        end
      rescue => e
        $log.error "Could not replace content of spec with #{params[:filename]}!"
        $log.error e.message
        return nil
      end
    end
    
    content2 = ""
    doc.write content2
    $log.debug content2
        
    res2 = _isok?(@service.submitSpecification(spec_id, Base64.encode64(content2).tr("\n",""), params))
    $log.info " - #{spec_id} submitted."
    return res2, new_version
  end
  
  def _replace_xml_element(doc1, doc2, xpath)
    e1 = doc1.elements[xpath]
    e2 = doc2.elements[xpath]
    unless e1 and e2
      $log.warn("#{xpath} not found. Will ignore it.")
      return
    end
    doc1.elements[xpath] = e2
  end
  
   # changes content of eopm specs, for now only additional equipment is implemented
  def _add_eopm_content(doc, params={}) #, spec_id, new_version, params)
    type = params.delete(:type)
    $log.info "changing #{type} content (#{params.inspect})"
    add_eqp = params[:additional_equipment] or ""
	# preparation for more complex content changes
	# hash = {
	  # "MetaInformation"=> {
	    # "Id" => spec_id,
        # "Title" => "qa_1.6.0_jira1421_keyword_while_nodes_are_empty",
        # "Version" => "#{version}",
        # "Author" => owner,
        # "Template" => "EOPM",
        # "FabLocation" => "C",
        # "Class" => "07",
        # "SubClass" => "EOPM",
        # "PCRB" => "",
        # "Owner" => owner,
        # "Department" => "FAT",
        # "GroupAccessLevel" => "",
        # "EffectiveDate" => "",
        # "ContentUpdateDate" => "",
        # "ChangeReason" => "",
        # "ChangeDescription" => "",
        # "WorkflowConfiguration" => "C07-FAT-18"
	  # },
	  # "Attachments" => "",
	  # "ReferredSpecs" => "",
	  # "Chapters" => {
	    # "Purpose" => "", 
		# "Scope" => "", 
		# "Definitions" => "", 
		# "AddChapters" => ""
	  # },
	  # "AuxiliaryFields" => {
	    # "SpecVersionBackup" => "",
		# "TableOfContents" => {
		  # "CommonContent" => "", 
		  # "TechnicalContent" => ""
		# },
		# "PageSAP" => { 
		  # "NumberOfRows" => "10", 
		  # "StartingRow" => "1", 
		  # "HelperField" => ""
		# }
	  # },
	  # "AdditionalEquipments" => {
	    # "AdditionalEquipment" => {
		  # "Equipment_ID" => equipment
		# }
	  # },
	  # "OperationPMSection" => "",
	  # "WBTQGroup" => {
	    # "Tools" => {
		  # "ValText" => ""
		# }
	  # }
	# }
 
    el = doc.root.elements["my:AdditionalEquipments/my:AdditionalEquipment/my:Equipment_ID"]
    el.text = add_eqp
    return doc
  end
  
  def _add_armor_content(doc, spec_id, new_version, params={})
    $log.info "Add ARMOR content"
    doc[3].elements['//my:MAUISetupBlock'].add_attribute("my:SpecID", spec_id)
    doc[3].elements['//my:MAUISetupBlock'].add_attribute("my:SpecVersion", new_version)
    doc[3].elements['//my:Armor_Recipe_Section_Number'].text = "5."

    tooltype = (params[:tooltype] or "QAA-TOOLTYPE")
    category = (params[:recipeset] or "QAA-SET")
    recipename = (params[:recipename] or "AUTO-RECIPE-#{Time.now.to_i}")
    eqprecipename = (params[:eqprecipename] or "/Path/To/Auto/Recipe/#{recipename}")
    recipetype = (params[:recipetype] or "Main")
    download = (params[:download] or false)
    upload = (params[:upload] or false)
    ctxnamespace = (params[:ctxnamespace] or tooltype)
    ctxrecipename = (params[:ctxrecipename] or recipename)
    
    hash = {:"my:RecipeSet"=>{:"my:Organization"=>
      {:"my:ToolType"=>tooltype, :"my:RecipeCategory"=>category, 
       :"my:uniqueOrganization"=>tooltype+category}, 
       :"my:Recipe"=>{:"my:DisplayName"=>recipename, 
        :"my:UploadSpecVersion"=>nil, 
        :"my:UploadSpecVersionBackup"=>nil, 
        :"my:UploadSpecIdWoVers"=>nil, 
        :"my:RecipeVersion"=>
        { :"my:VersionNumber"=>new_version, 
          :"my:EIRecipeName"=>recipename, 
          :"my:EquipmentRecipeName"=>eqprecipename, 
          :"my:RecipeType"=>recipetype, 
          :"my:DownloadRequired"=>download, 
          :"my:UploadFrom"=>nil, 
          :"my:UploadedFrom"=>nil, 
          :"my:UploadRecipe"=>upload, 
          :"my:UploadRecipeName"=>nil, 
          :"my:CopyRecipeBodyFromOID"=>nil, 
          :"my:ApprovalState"=>nil, 
          :"my:ContextSection"=>{:"my:Context"=>
          { :"my:ContextLevel"=>
            { :"my:ContextEC"=>nil, 
              :"my:ContextEIVersion"=>nil, 
              :"my:ContextEquipmentGroup"=>nil, 
              :"my:ContextEquipmentID"=>nil, 
              :"my:ContextPhotoLayer"=>nil, 
              :"my:ContextProcessDefinitionID"=>nil, 
              :"my:ContextProductID"=>nil, 
              :"my:ContextProductIDEC"=>nil, 
              :"my:ContextRecipeName"=>ctxrecipename, :"my:ContextRecipeNameSpace"=>ctxnamespace, 
              :"my:ContextSAPChamberID"=>nil, :"my:ContextSAPDCPIdentifier"=>nil, 
              :"my:unique_context"=>ctxnamespace+ctxrecipename}}}, 
         :"my:UploadChangeUncritical"=>"false", 
         }}}}
          
    hash[:"my:RecipeSet"][:"my:Recipe"][:"my:RecipeVersion"][:"my:StepperSection"] = { :"my:LithoStepperLink"=>params[:stepperlink] } if params[:stepperlink]
         
    xml = _build_recipeblock(hash)
    r = REXML::Document.new xml
    doc[3].elements['//my:RecipeBlock'] = r.elements['//my:RecipeBlock']
    $log.debug "ARMOR content\n#{xml.inspect}"
    return xml
  end
  
  def _add_sp_content(doc, spec_id, new_version, params={})
    $log.info "Add Shared Parameter content"
    doc[3].elements['//my:MAUISetupBlock'].add_attribute("my:SpecID", spec_id)
    doc[3].elements['//my:MAUISetupBlock'].add_attribute("my:SpecVersion", new_version)
    doc[3].elements['//my:Armor_Recipe_Section_Number'].text = "5."
  
    tooltype = (params[:tooltype] or "QAA-TOOLTYPE")
    category = (params[:recipeset] or "QAA-SET")
    parametername = (params[:parametername] or "AUTO-SP-#{Time.now.to_i}")
    eiparametername = (params[:parametername] or "EI-#{parametername}")
    
    hash = {:"my:SharedParameterSet"=>{
       :"my:SharedParameterSetDescription"=>"Automated test",
       :"my:Organization"=>
         {:"my:ToolType"=>tooltype, :"my:RecipeCategory"=>category, 
          :"my:uniqueOrganization"=>tooltype+category}, 
       :"my:RecipeParameter"=>{
        :"my:Name"=>parametername,
        :"my:EIName"=>eiparametername, 
        :"my:ParameterValue"=>{
          :"my:IntegerParameterValueWithLimits"=>{
            :"my:DefaultValueInteger"=>1,
            :"my:LowerLimitInteger"=>0,
            :"my:UpperLimitInteger"=>2}},
        :"my:ParameterPurpose"=>"APC Only"  
       }}}
         
    xml = _build_recipeblock(hash, "SharedParameterBlock")
    r = REXML::Document.new xml
    doc[3].elements['//my:SharedParameterBlock'] = r.elements['//my:SharedParameterBlock']
    $log.debug "ARMOR content\n#{xml.inspect}"
    return xml
  end

  # Creates a new recipe block
  def _build_recipeblock(hash, blocktype="RecipeBlock")
    xml = Builder::XmlMarkup.new
    xml.tag!("myFields",  "xmlns:my"=>"http://schemas.xmlsoap.org/soap/envelope/") do |run|
      ##service._hash2xml(run, blocktype, hash, "my")
      @service.hash_to_xml(hash, :"my:#{blocktype}", run)
    end
    return xml.target!
  end
  
  # Service to inquire a specification and retrieve the content
  def get_specification(spec_id, version = nil)
    res = _isok?(@service.getSpecification(spec_id, version)[:getSpecificationResponse])[:return]
    content = Base64.decode64(res[:result][:content])
    version = res[:result][:version].to_i
    $log.debug(content)
    return content, version   
  end

  def get_spec_data(spec_id, version = nil)
    res = _isok?(@service.getSpecification(spec_id, version))[:getSpecificationResponse][:return]
    return res
  end

  # Service to inquire a specification and retrieve the content
  def get_spec_metadata(spec_id, version = nil)
    res = _isok?(@service.getSpecificationMetadata(spec_id, version))[:getSpecificationMetadataResponse][:return]
    return res[:result]
  end
  
  # Add spec attachment with given filename
  def create_attachment(filename, content, spec_id, temporary = false)
    res = _isok?(@service.createAttachment({
         :"ws:specificationId" => spec_id,
         :"ws:temporary"       => temporary.to_s,
         :"ws:fileName"        => filename,
         :"ws:content"         => content
    }))[:createAttachmentResponse][:return]
    return res[:result]
  end
  
  # Get spec attachment with given filename
  def get_attachment(filename, spec_id, version = nil, params={})
    hparams = { 
      :"ws:specificationId"      => spec_id,
      :"ws:fileName"             => filename
    }
    hparams[:"ws:specificationVersion"] = version unless version.nil?
    res = _isok?(@service.getAttachment(params.merge(hparams)))[:getAttachmentResponse][:return]
    if res[:success] == "true"
      Base64.decode64(res[:result][:content])
    else
      nil
    end
  end
  
  # Delete spec attachment with given filename
  def delete_attachment(filename, spec_id, version = nil, params={})
    hparams = { 
      :"ws:specificationId"      => spec_id,
      :"ws:fileName"             => filename
    }
    hparams[:"ws:specificationVersion"] = version unless version.nil?
    res = _isok?(@service.deleteAttachment(params.merge(hparams)))[:deleteAttachmentResponse][:return]
    return res[:success] == "true"
  end
  
  # Performs a user action on the spec
  #
  # actions: APPROVE, CANCEL, RETRY, REJECT
  def human_workflow_action(spec_id, params={})    
    action = (params[:action] or "APPROVE")
    $log.info(" - #{spec_id} human workflow action (#{action})")
    task = wait_get_user_task(params.merge(:specid=>spec_id))
    $log.debug "task: #{task.inspect}"
    if not task
      $log.error "No task found for #{spec_id}"
      return
    end
    task_id = task[:id]
    transition = action
    
    props = task[:workflowProperties]
    # props[:requireRework] = params[:requireRework] or true if action == "CANCEL" 
    props[:requireRework] = params[:requireRework] if (action == "CANCEL") and params[:requireRework]
    res = _isok?(@support.completeTask(task_id, transition, "automated test approval", props, params))
    sleep params[:sleep] if params[:sleep]
    return res
  end
  
  # Terminate a :spec_id
  def terminate(spec_id)
    res = _isok?(@support.workOnSpecification({ :arg0 => spec_id, :arg1 => "termination" }))
    return res
  end
  
  # Acknowledge a :spec_id
  def acknowledge(spec_id, params={})
    if params[:all]
      res = true
      ack_tasks=$sfc.get_all_tasks(:specid=>spec_id, :name=>:acknowledge)
      if ack_tasks
        ack_tasks.each do |t|
          $log.info("human workflow action #{t[:title]} for #{spec_id}")
          res &= _isok?($sfc.support.completeTask(t[:id], nil, "automated test acknowledge", t[:workflowProperties], :user=>t[:owner]))
        end
      end
      return res
    end
    task = wait_get_user_task(params.merge(:specid=>spec_id, :name=>"gfwf:acknowledge"))
    $log.info " - #{spec_id} acknowledge #{params[:user]}"
    if not task
      $log.error "No task found for #{spec_id}"
      return
    end
    
    task_id = task[:id]
    props = task[:workflowProperties]
    
    res = _isok?(@support.completeTask(task_id, nil, "automated test acknowledge", props, params))
    return res
  end
  
  # Downloads spec documents from Setup.FC using metadata search
  def specs_to_file(file_path, params = {})
    res = _isok?(@service.findSpecificationsByMetadata(params))
    specs = res[:findSpecificationsByMetadataResponse][:return][:result][:specList]
    $log.info "Returned specs: #{specs.inspect}"
    specs = [specs] if !specs.is_a? Array
    specs.each do |spec|
      spec_id = spec[:id]
      spec_version = spec[:version]
      fname = "#{file_path}/#{spec[:name]}"
      begin
        content,version = get_specification spec_id, spec_version
        $log.debug "  writing #{fname}"
        File.open(fname, 'w') {|fh| fh.write(content)}
        $log.info " #{fname} written (#{version})"
      rescue => e
        $log.warn "error writing #{fname}\n#{$!}"
      end
    end
  end
  
  def files_to_specs(filenames, params={})
    filenames.each do |filename|
      #begin
        f = File.open(filename)
        spec_content = REXML::Document.new f
        sclass = spec_content.root.elements['//my:Class'].text
        subclass = spec_content.root.elements['//my:SubClass'].text
        template = spec_content.root.elements['//my:Template'].text
        workflow = (params[:workflow] or spec_content.root.elements['//my:WorkflowConfiguration'].text)
        title = spec_content.root.elements['//my:Title'].text
        spec_id = create_new_specification(sclass, subclass, workflow, template, :title=>title, :id=>nil)
        content_change(spec_id)
        spec, version = submit_specification(spec_id, :type=>"SPACE", :filename=>filename)
        human_workflow_action(spec_id, :timeout=>30, :polling=>true)
        acknowledge(spec_id, :timeout=>30, :polling=>true)
      #rescue
      #  $log.error "Could not replace content of spec with #{filename}!"
      #end
    end
  end

  # Get the userids of all users in a group
  def users_for_group(group, params={})
    res = _isok?(@support.getUsersForGroup(group, params))
    users = res[:getUsersForGroupResponse][:return]
    users = [users] unless users.kind_of?(Array)    
    return users.collect {|u| u[:userName]}
  end
  
  # find existing workflow configs
  #  if specified als by :wf_cfg_id
  def find_workflow_configs(wf_class, wf_subclass, params={})    
    $log.info "finding workflow configs for class #{wf_class} / subclass #{wf_subclass}"
    res = _isok?(@support.findWorkflowConfigs(wf_class, wf_subclass, params))[:findWorkflowConfigsResponse][:return]
    if res
      res = [res] unless res.kind_of?(Array)
      res = res.find {|wf| wf[:metaInformation][:workflowConfigId] == params[:wf_cfg_id]} if params[:wf_cfg_id]
    end
    return res
  end
  
  # Create a new workflow configuration
  # add new normal approval layers like this 
  #   { 1 => [QA, OQA, ...], 2=> ...,  }
  def create_workflow_config(title, params={})
    # check parameters
    fablocation = (params[:fablocation] || "M")
    wf_set = (params[:workflowSetId] || "M_STANDARD")
    wf_class = (params[:classId] || "05")
    wf_subclass = (params[:subClassId] || "Base")
    wf_department = (params[:departmentId] || "ALL")
    approval = (params[:approve] || { 1 => "QA" })
    wf_id = (params[:workflowConfigId] || "#{fablocation}#{wf_class}-#{wf_department}-#{Time.now.to_i}")
    $log.info "create workflow config #{wf_id} - #{title}"
    
    # Standard approval config with one tier
    normal_approval = (params[:normal_approval] || 
      {"type"=>"normal_approval", :"wor:approvalCycle"=>{"id"=>"approval1", 
        :"wor:approvalTier"=>{"order"=>"1", :"wor:groupConfig"=>{"group"=>"QA"}}}})
    
    # Short approval is disabled per standard
    short_approval = (params[:short_approval] ||
      {"type"=>"short_approval", :"wor:approvalCycle"=>{"id"=>"approval1"}})
    
    acknowledge = (params[:acknowledge] || {}) # One group example {:"wor:groupConfig"=>{"group"=>"QA"}}
    
    config = {
      :"wor:metaInformation" => {
        :"wor:workflowConfigId" => wf_id,
        :"wor:workflowConfigTitle" => title,
        :"wor:workflowSetId" => wf_set,
        :"wor:classId" => wf_class,
        :"wor:subClassId" => wf_subclass,
        :"wor:departmentId" => wf_department
      },
      :"wor:approvalConfig" => [normal_approval, short_approval],
      :"wor:acknowledgementConfig" => acknowledge,
      :"wor:notificationConfig"=>{:"wor:notificationEvent"=>[{"type"=>"spec_submitted"}, {"type"=>"spec_rejected"}, {"type"=>"spec_effective"}]}
    }    
    return wf_id if _isok?(@support.createWorkflowConfig(config))[:createWorkflowConfigResponse]        
  end
  
  def update_workflow_config(wf_cfg_id, class_id, subclass_id, params={})
    # find existing config
    config = find_workflow_configs(class_id, subclass_id, wf_cfg_id: wf_cfg_id)

    ($log.error("workflow config #{wf_cfg_id} not found");return) unless config
  
    $log.info "update workflow config #{wf_cfg_id}, #{params.inspect}"
    
    # update namespaces
    config = @support.add_namespace_to_hash(config, "wor")    
    
    # update workflow configs
    normal_approval, short_approval = config[:"wor:approvalConfig"]    
    normal_approval = params[:normal_approval] if params[:normal_approval]
    short_approval = params[:short_approval] if params[:short_approval]      
    config[:"wor:approvalConfig"] = [normal_approval, short_approval]
    config[:"wor:acknowledgementConfig"] = params[:acknowledge] if params[:acknowledge]
 
     $log.info "#{config.inspect}"
    return wf_cfg_id if _isok?(@support.updateWorkflowConfig(config))[:updateWorkflowConfigResponse]        
  end
  
  # Delete an existing workflow config
  def delete_workflow_config(wf_cfg_id, params={})
    $log.info "delete workflow config #{wf_cfg_id} (#{params.inspect})"
    wf_set = (params[:wf_set] or "M_STANDARD")    
    return _isok?(@support.deleteWorkflowConfig(wf_cfg_id, wf_set, params))[:deleteWorkflowConfigResponse]
  end
  
  def getStackVersion(params = {})
    # getting sstack version - used to test communication
    
    res = _isok?(@service.getStackVersion(params))[:getStackVersionResponse][:return]
    $log.info " - stack version fetched (#{res[:result].split(",")[0]})" 
    return res
  end
  
  def hasOpenAcknowledgements(user, params = {})
    res = _isok?(@service.hasOpenAcknowledgements(user, params))[:hasOpenAcknowledgementsResponse][:return]
    return res
  end
  
  def getDepartments(params={})
    res = _isok?(@service.getDepartments())[:getDepartmentsResponse][:return]
    return res
  end
  
  def getSpecificationClasses(params={})
    res = _isok?(@service.getSpecificationClasses())[:getSpecificationClassesResponse][:return]
    return res
  end
  
  def getSpecificationSubClasses(key, params={})
    res = _isok?(@service.getSpecificationSubClasses(key))[:getSpecificationSubClassesResponse][:return]
    return res
  end
  
  def getSpecificationWithContentTransformation(spec_id, version=nil, style_sheet=nil)
    res = _isok?(@service.getSpecificationWithContentTransformation(spec_id, version, style_sheet))[:getSpecificationWithContentTransformationResponse][:return]
    return res
  end
  
  def getKeywordTypes(params={})
    res = _isok?(@service.getKeywordTypes())[:getKeywordTypesResponse][:return]
    return res
  end
  
  def getKeywordValues(keyword_type, params={})
    res = _isok?(@service.getKeywordValues(keyword_type))[:getKeywordValuesResponse][:return]
    return res
  end
 
  def findSpecificationsByKeywords(keyword_type, params={})
    res = _isok?(@service.findSpecificationsByKeywords(keyword_type))
    res = res[:findSpecificationsByKeywordsResponse][:return]
    return res
  end
  
  def findSpecificationsByMetadata(soap_params, params = {})
    res = _isok?(@service.findSpecificationsByMetadata(soap_params, params))
    res = res[:findSpecificationsByMetadataResponse][:return]
    return res
  end
  
  def getSpecificationHistory(sp_id, params={})
    version = (params[:version] or nil)
    res = _isok?(@service.getSpecificationHistory(sp_id, version))
    res = res[:getSpecificationHistoryResponse][:return]
    return res
  end
end

# automated test of setup.FC API
class BaseSoap < RequestResponse::HTTP
  @@namespaces = {
    "soapenv" => "http://schemas.xmlsoap.org/soap/envelope/",
    "service" => "http://ws.setupfc.globalfoundries.com/",
    "support" => "http://service.gearbox.setupfc.globalfoundries.com/",
    "workflow"=> "http://setupfc.globalfoundries.com/workflowconfig"    
  }

  # get url, user and pw from etc/urls.conf or params
  def initialize(instance, params)
    super(instance, params)
  end
  
  # build XML representation (values are text elements, not attributes)    
  def _hash2xml(t0, tagname, h, ns="")      
    tagname = ns+":"+tagname unless ns==""
    return t0.tag!(tagname) do |subtag|
      h.each_pair {|k,v|
        next if v.nil?          
        if v.kind_of?(String) or v.kind_of?(Numeric) or v == true or v == false or v.kind_of?(Array)          
          k = ns+":"+k unless ns==""
          v = [v] unless v.is_a?(Array)
          v.each {|v1|
            if v1.is_a?(Hash)
              _hash2xml(subtag, k, v1, ns)
            else
              subtag.tag!(k) { |b| b.text! v1.to_s }
            end
          }
        elsif v.kind_of?(Hash)
          _hash2xml(subtag, k, v, ns)
        else
          $log.warn "_hash2xml: unsupported data: #{k.inspect}, #{v.inspect}"
        end
      }
    end     
  end



  def _send_receive(xml, params={})
    res = super(xml, params)
    res = res["return"] if ((!res.nil?) && (res.has_key?("return")))
    return res
  end
   	
  def getActions
    # provides all actions of the service
    xml_data = Net::HTTP.get_response(@uri).body
    doc = REXML::Document.new(xml_data)
    a = []
    REXML::XPath.each(doc,"//operation[@name]"){|e|
      a << e.attributes["name"] if ("binding" == e.parent.name)
    }
    return a
  end
end

# automated test of setup.FC API
class ServiceSoap < BaseSoap  
  #   env - environment (e.g. dev or let)
  #   params, valid values :user and :password
  def initialize(env,params={})
    @env = env
    instance = "sfcservice.#{env}"
    super(instance, params)
  end

  def build_request(fname, request_params={})
    xml = Builder::XmlMarkup.new
    xml.instruct!
    xml.tag!("SOAP-ENV:Envelope",  "xmlns:SOAP-ENV"=>@@namespaces["soapenv"], "xmlns:ws"=>@@namespaces["service"]) do |enve|
      enve.tag!("SOAP-ENV:Body") do |body|       
        hash_to_xml(request_params, :"ws:#{fname}".to_sym, body)
      end       
    end
    return xml.target!
  end
  
  def verySpecialBehaviorOfFind
    # to store the results
    s={}
    
    # do some times (getAll)
    for i in 1..5
      r = fSBK
      r[:result][:spec_list].each do |spec|
        # if in list incerment
        key = spec[:id]
        if !s.has_key?(key.to_sym)
          s[key.to_sym]=0
        end
        s[key.to_sym]=s[key.to_sym]+1
      end  
    end
    
    # give a summary
    sum=0
    s.keys.sort{ |a,b| a.to_s <=> b.to_s }.each do |key|
      # shhows wich spec how often
      puts "#{key}  #{s[key]}"
      # summarize
      sum=sum+s[key]
    end
    puts "count #{s.count}   sum #{sum}"
  end

  def createAttachment(params = {})
    xml = build_request("createAttachment", params)
    return _send_receive(xml, params)
  end
  
  def createSpecificationReference(params = {})
    xml = build_request("createSpecificationReference", params)
    return _send_receive(xml, params)
  end

  def deleteAttachment(params = {})
    xml = build_request("deleteAttachment", params)
    return _send_receive(xml, params)
  end

  def deleteSpecificationReference(params = {})
    xml = build_request("deleteSpecificationReference", params)
    return _send_receive(xml, params)
  end

  def findSpecificationsByKeywords(params = {})
    soap_params = {:key=>"", :value=>""}
    key = params.delete(:key)
    value = params.delete(:value)
    soap_params[:key] = key unless key.nil?
    soap_params[:value] = value unless value.nil? 
    soapBody = {
      :"ws:specificationMetadata" => {}, 
      :"ws:keywords" => soap_params
    }
    xml = build_request("findSpecificationsByKeywords", soapBody)
    return _send_receive(xml, params)
  end
  
  def fSBK # not really find... but getall
    xml = build_request("findSpecificationsByKeywords", {:specificationMetadata=>{}})
    return _send_receive(xml, params)
  end

  def findSpecificationsByMetadata(soap_params, params = {})
    xml = build_request("findSpecificationsByMetadata",  {:"ws:specificationMetadata"=>soap_params})
    return _send_receive(xml, params)
  end

  def getAttachment(params = {}) #"ws:`specificationId" => "", "ws:specificationVersion" => "", "ws:fileName" => ""})
    xml = build_request("getAttachment", params)
    return _send_receive(xml, params)
  end

  def getDepartments
    xml = build_request("getDepartments")
    return _send_receive(xml, params)
  end

  def getKeywordTypes
    xml = build_request("getKeywordTypes")
    return _send_receive(xml, params)
  end

  def getKeywordValues(key)
	  xml = build_request("getKeywordValues", {:"ws:keywordType"=>key})
    return _send_receive(xml, params)
  end
  
  def getSpecification(spec_id, version = nil)
    params = {:"ws:specificationId" => spec_id}
    params[:"ws:specificationVersion"] = version.to_s if version
    xml = build_request("getSpecification", params)
    return _send_receive(xml, params)
  end

  def getSpecificationClasses
    xml = build_request("getSpecificationClasses")
    return _send_receive(xml, params)
  end

  def getSpecificationHistory( spec_id, version = nil )
    version ||= "0"
    xml = build_request("getSpecificationHistory", {:"ws:specificationId" => spec_id, :"ws:specificationVersion" => version})
    return _send_receive(xml, params)
  end
  
  def getSpecificationSubClasses( class_id )
    xml = build_request("getSpecificationSubClasses", {:"ws:specificationClassId" => class_id})
    return _send_receive(xml, params)
  end
  
  def getSpecificationMetadata(spec_id, version = nil)
    params = {:"ws:specificationId" => spec_id}
    params[:"ws:specificationVersion"] = version.to_s if version
    xml = build_request("getSpecificationMetadata", params)
    return _send_receive(xml, params)
  end
  
  def getStackVersion(params = {})
    xml = build_request("getStackVersion")
    return _send_receive(xml, params)
  end
  
  # don't be worried about results - only class 07 specs 
  def hasOpenAcknowledgements(user, params={})
    xml = build_request("hasOpenAcknowledgements", {:"ws:userName" => user})
    return _send_receive(xml, params)
  end

  def submitSpecification(spec_id, content, params={})
    shortapproval = (params[:shortapproval] or false)
    xml = build_request("submitSpecification", {:"ws:specificationId" => spec_id, :"ws:specificationContent" => content, :"ws:submitParameters" => { :"ws:useShortWorkflow" => shortapproval }})
    return _send_receive(xml, params)
  end
  
  def updateSpecification(spec_id, content, params={})
    xml = build_request("updateSpecification", {:"ws:specificationId" => spec_id, :"ws:specificationContent" => content})
    return _send_receive(xml, params)
  end
  
  def getSpecificationWithContentTransformation(spec_id, version, style_sheet, params={})
    s_params = {:"ws:specificationId" => spec_id}
    s_params[:"ws:specificationVersion"] = version unless version.nil?
    s_params[:"ws:stylesheetName"] = style_sheet unless style_sheet.nil?
    xml = build_request("getSpecificationWithContentTransformation", s_params)
    return _send_receive(xml, params)
  end
end

# automated test of setup.FC Test Support API
class SupportSoap < BaseSoap #SetupSoap
  #   env - environment (e.g. dev or let)
  #
  #   params, valid values :user and :password
  #
  #   params[url] is set for support-wsdl
  def initialize(env,params={})
    @env = env
    instance = "sfcsupport.#{env}"
    super(instance, params)
  end

  def build_request(fname, params={})
    xml = Builder::XmlMarkup.new
    xml.instruct!
    xml.tag!("SOAP-ENV:Envelope",  "xmlns:SOAP-ENV"=>@@namespaces["soapenv"], "xmlns:ws"=>@@namespaces["support"], "xmlns:wor"=>@@namespaces["workflow"]) do |enve|
      enve.tag!("SOAP-ENV:Body") do |body|
        #_hash2xml(body, "ws:#{fname}", params)          
        hash_to_xml(params, :"ws:#{fname}".to_sym, body)
      end       
    end
    return xml.target!
  end
    
  def submitSpecification(spec, task, params={})
    shortapproval=(params[:shortapproval] or false)
    args = {:arg0 => spec, :arg1 => task, :arg2 => { :useShortWorkflow => shortapproval } }
    xml = build_request("submitSpecification", args)
    pp xml
    return _send_receive(xml, args)
  end

  def cancelSignoff(spec_id, requestor, params={})
    comment = (params[:comment] or "Test")
    xml = build_request("cancelSignoff", { :arg0 => spec_id, :arg1 => requestor, :arg2 => comment })
    return _send_receive(xml, params)
  end
  
  def canCancelSignoff(workflow_id, params)
    xml = build_request("canCancelSignoff", { :arg0 => workflow_id })
    return _send_receive(xml, params)
  end
  
  def completeTask(task_id, transition, comment, props, params = {})
    s_params = {:arg0 => task_id}
    s_params.merge!({:arg1 => transition}) if transition
    s_params.merge!({:arg2 => comment, :arg3 => props})
    xml = build_request("completeTask", s_params)
    return _send_receive(xml, params)
  end

  def getApprovalStatus(task_id, params={})
    xml = build_request("getApprovalStatus", { :arg0 => task_id})
    return _send_receive(xml, params)
  end

  # retrieve all active tasks, If spec_content is true the spec content will be retrieved
  def getAllActiveTasks(with_content, params={})
    xml = build_request("getAllActiveTasks", { :arg0 => with_content})
    return _send_receive(xml, params)
  end
  
  # retrieve all active workflows, If spec_content is true the spec content will be retrieved
  def getAllActiveWorkflows(with_content, params={})
    xml = build_request("getAllActiveWorkflows", { :arg0 => with_content})
    return _send_receive(xml, params)
  end

  def getCurrentWorkflowNode(workflow_id, params={})
    xml = build_request("getCurrentWorkflowNode", { :arg0 => workflow_id})
    return _send_receive(xml, params)
  end

  def createSpecification(params = { :arg0 => "" })
    xml = build_request("createSpecification", params)
    return _send_receive(xml, params)
  end

  def createWorkingCopy(params = {})
    xml = build_request("createWorkingCopy", params)
    return _send_receive(xml, params)
  end

  def deleteSpecification(args, params = {})
    xml = build_request("deleteSpecification", args)
    return _send_receive(xml, params)
  end

  def deleteWorkingCopy(params = {})
    xml = build_request("deleteWorkingCopy", params)
    return _send_receive(xml, params)
  end

  def getActiveWorkflowsForSpec(spec_id)
    xml = build_request("getActiveWorkflowsForSpec", {:arg0=>spec_id})
    return _send_receive(xml, params)
  end
  
  def getUserTaskList(params={})
    xml = build_request("getUserTaskList")
    return _send_receive(xml, params)
  end
  
  def getPooledActorsForTask(task_id, params={})
    xml = build_request("getPooledActorsForTask", {:arg0=>task_id})
    return _send_receive(xml, params)
  end

  def workOnSpecification(params={})
    xml = build_request("workOnSpecification", params)
    return _send_receive(xml, params)
  end
  
  def transferTaskToUser(params)
    xml = build_request("transferTaskToUser", params)
    return _send_receive(xml, params)
  end
  
  def createWorkflowConfig(wf_cfg, params={})
    xml = build_request("createWorkflowConfig", {:arg0 => wf_cfg})    
    return _send_receive(xml, params)
  end
  
  def updateWorkflowConfig(wf_cfg, params={})
    xml = build_request("updateWorkflowConfig", {:arg0 => wf_cfg})    
    return _send_receive(xml, params)
  end
  
  # delete workflow config workflowConfigId
  def deleteWorkflowConfig(wf_cfg_id, wf_set, params={})
    args = {:arg0=>{:"wor:metaInformation" => {:"wor:workflowConfigId"=>wf_cfg_id, :"wor:workflowSetId"=>wf_set}}}
    xml = build_request("deleteWorkflowConfig", args)
    return _send_receive(xml, params)
  end
  
  # returns an array of workflow configurations for class/subclass
  def findWorkflowConfigs(wf_class, wf_subclass, params={})
    xml = build_request("findWorkflowConfigs", params.merge({:arg0=>wf_class,:arg1=>wf_subclass}))
    return _send_receive(xml, params)
  end
  
  def deleteKeywords(sp_id)
    # delete keywords for a given spec
    xml = build_request("deleteKeywords", :arg0 => sp_id)
    return _send_receive(xml, params)
  end
  
  def getApprovalStatusFromSpecification(spec_id, params={})
      xml = build_request("getApprovalStatusFromSpecification", {:arg0 => spec_id})
      return _send_receive(xml, params)      
  end
  
  def getUsersForGroup(group, params={})
    xml = build_request("getUsersForGroup", {:arg0 => group})
    return _send_receive(xml, params)
  end
  
  # # # # #

  ##TODO
  def createSpecFromData(params = {}); not_implemented(); end
  def deleteKeywordsBySpecIdAndVerion(params = {}); not_implemented(); end
  def extractAndPersistKeywordsV1(params = {}); not_implemented(); end
  def extractAndPersistKeywordsV2(params = {}); not_implemented(); end
  def getKeywordValuesBySpecIdAndVersion(params = {}); not_implemented(); end
  def getKeywordValuesByType(params = {}); not_implemented(); end
  def getSpecsByKeywordValue(params = {}); not_implemented(); end
  def getSpecsByKeywordValueAndType(params = {}); not_implemented(); end
  def getSpecsByMultipleValuesAndTypes(params = {}); not_implemented(); end
  def publishOutgoingMessage(params={}); not_implemented(); end
  def publishReplyMessage(params={}); not_implemented(); end
  def commitKeywords; not_implemented(); end
  def completeTaskWithChecks; not_implemented(); end
  def createUser; not_implemented(); end
  def deleteKeyword; not_implemented(); end
  def deleteUser; not_implemented(); end
  def extractAndPersitKeywords; not_implemented(); end
  def getKeywordsBySpecId; not_implemented(); end
  def getKeywordsBySpecIdAndVersion; not_implemented(); end
  def revertKeywords; not_implemented(); end
  def runScript; not_implemented(); end
  def setKeywordValue; not_implemented(); end
  def getApprovalStatusFromTask; not_implemented(); end
  def sendSetupfcMessage; not_implemented(); end

  # # # # #
  
  def not_implemented()
    $log.warn "NOT IMPLEMENTED"
    return nil
  end
end

# automated test of setup.FC API
class RestAPI < RequestResponse::HTTP
  
  def initialize(env, params={})
    @env = env
    super("sfcrest.#{env}", params)
    _auth_token()
  end

  def _auth_token()
    ret = _send_receive(nil, params.merge(:resource=>"auth/token", :request_type=>"GET", :rawxml=>true))
    if ret
      @auth_token = { "Connection"=>"keep-alive", "Cookie" => "SFC-AUTH-TOKEN=#{response}" }
    else
      $log.error "cannot authentificate connection"
    end
  end

  # Retrieves spec metadata, :content or :attachments
  def get_specs(params={})
    resource = "specs"
    resource += "/#{params[:spec_id]}" if params[:spec_id]
    resource += "/v#{params[:version]}" if params[:version]
    if params[:filename]
      resource += "/attachments/#{URI::encode(params[:filename])}"
      resource += "/content" if params[:content]
    elsif params[:attachments]
      resource += "/attachments"
    elsif params[:content]
      resource += "/content"
    end
    $log.info "#{__method__}, #{resource.inspect}"
    res = _send_receive(nil, params.merge(:resource=>resource, :header=>@auth_token, :request_type=>"GET", :all=>true))
    return res if params[:raw]
    return res[:specificationList][:specifications] if res.has_key?(:specificationList)
    return res
  end

  def get_specxxx(params={})
    resource = "specs"
    if params[:spec_id] 
      resource += "/"+params[:spec_id]+"/"
    else 
      resource += "?"
      params.each{|pk,pv| resource += "#{pk.to_s}=#{pv.to_s}&"}
    end
    $log.info "#{__method__}, #{resource.inspect}"
    res = _send_receive(nil, params.merge(:resource=>resource, :header=>@auth_token, :request_type=>"GET", :all=>true))
    return nil if res.nil?
$r = res
    if params[:spec_id] 
      return [res]
    else
      r = nil
      if res.has_key?(:specificationList) and res[:specificationList]
        r = res[:specificationList][:specifications]        
      elsif res.has_key?(:collection) and res[:collection]
        r = res[:collection][:specification]
      end
      return r.nil? ? [] : r      
    end
  end
  
  def get_tasks(params={})
    resource = "tasks"
    res = _send_receive(nil, params.merge(:resource=>resource, :header=>@auth_token, :request_type=>"GET", :all=>true))
    return [] if res.nil?
    return (r=res[:taskList]).nil? ? [] : r
  end

  def get_xml_schemas(params={})
    resource = "xmlschemas"
    resource += "/#{params[:xsd]}" if params[:xsd]
    return _send_receive(nil, params.merge(:resource=>resource, :header=>@auth_token, :request_type=>"GET", :all=>true))
  end

  def get_xml_configs(params={})
    return _send_receive(nil, params.merge(:resource=>"xmlconfigs", :header=>@auth_token, :request_type=>"GET", :all=>true))
  end

  def get_master_config
    # http://vf1sfcq01:10080/setupfc-rest/v1/masterconfig/
    return _send_receive(nil, params.merge(:resource=>"masterconfig", :header=>@auth_token, :request_type=>"GET", :all=>true))
  end
end

