=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.


Author:     dsteger, 2011-09-29
Version:    2.0.0
History:
  2017-02-20 sfrieske, detect server side errors (e.g. exceptions) on excel file upload
=end

ENV.delete('http_proxy')
require 'rqrsp_http'


module Space

  # Upload excel files via sil-pcp-processor
  class ExcelUpload
    attr_accessor :env, :http

    def initialize(env, params={})
      @env = env
      @http = RequestResponse::Http.new(params[:pcp_processor_instance] || "silpcpprocessor.#{@env}", {timeout: 600}.merge(params))
      @resources = {
        Autocharting: 'autoCharting-doupload.action',
        Speclimit: 'pcp-doupload.action',
        MonitoringSpeclimit: 'monitoringSpec-doupload.action',
        PDReferer: 'pd-doupload.action',
        Chamberfilter: 'chamberFilter-doupload.action',
        Futurehold: 'futureHold-doupload.action',
        Generickey: 'genericKey-doupload.action',
        Terminate: 'doTerminateSpec.action'
      }
    end

    def inspect
      "#<#{self.class.name} #{@http}>"
    end

    # return true on successful upload
    def upload_file(fname, caption, type, params={})
      $log.info "upload_file #{fname.inspect}, #{caption.inspect}, #{type.inspect}"
      ($log.warn "invalid upload type: #{@resources.keys.inspect}"; return) unless @resources.keys.member?(type.to_sym)
      # call upload page to get a session (cookie is obtained and stored by @http internally)
      @http.get(resource: 'pcp-upload.action') || return
      # prepare upload request
      s = File.binread(fname) || return
      boundary = params[:boundary] || '--SpcXlsUplBnd4455XX'
      body = "--#{boundary}\r\n" +
        "Content-Disposition: form-data; name=\"upload\"; filename=\"#{File.basename(fname)}\"\r\n" +
        "Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet\r\n\r\n" + s +
        "\r\n--#{boundary}\r\n" +
        "Content-Disposition: form-data; name=\"caption\"\r\n\r\n" + caption +
        "\r\n--#{boundary}--\r\n"
      # upload, check response body for processing errors
      res = @http.post(body, content_type: "multipart/form-data; boundary=#{boundary}", resource: @resources[type.to_sym]) || return
      ($log.warn res[0..999]; return false) if res.include?('Error')
      return true
    end
  end

end
