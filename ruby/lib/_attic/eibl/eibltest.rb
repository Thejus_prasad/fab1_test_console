=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.


Author: Conny Stenzel, 2020-04-01

History:
=end

require 'fileutils'
# require 'pp'
require 'derdack'
# require 'misc'        # verify_hash
require 'rqrsp_mq'
require 'siview'
require_relative 'eibl'

# EIBL Testing Support
module EIBL

  class Test
    attr_accessor :sv, :eiserver, :logdir,
      :notifications, :notification_application, :notification_timeout
      # :defaults, :lot, :eqp


    def initialize(env, eqp, eiversion, params={})
      @sv = params[:sv] || SiView::MM.new(env)
      # begin
      @eiserver = EIBL::Server.new(env, eqp, eiversion)
      # rescue
      #   $log.info 'no connection to EI server, cannot parse any EI log files'
      # end
      @logdir = 'log/eibl'
      FileUtils.mkdir_p(@logdir)
      @notifications = params.has_key?(:notifications) ? params[:notifications] : DerdackEvents.new(env)
      #@notification_application = '' # to be determined!!!
      @notification_timeout = params[:notification_timeout] || 120
      # @defaults = params[:defaults] || {
      #   lot: params[:lot] || 'U70D1.01', 
      #   eqp: params[:eqp] || 'HYB171'
      # }
      # set_defaults
    end

    def inspect
      "#<#{self.class.name}>"
    end

    # # set defaults for a series of tests, reset to 'default defaults' when nothing is passed, return true on success
    # def set_defaults(params={})
    #   $log.info "EIBL::Test.set_defaults #{params}"
    #   ret = true
    #   params = @defaults.merge(params)
    #   @lot = params[:lot]
    #   @eqp = params[:eqp]
    #   return ret
    # end

    # returns logfilename that includes the test program and method name
    def tclogname
      caller_locations.each {|location|
        return 'console' if location.path == '(irb)'
        next unless location.path.start_with?(Dir.pwd) && location.label.start_with?('test')
        return File.basename(location.path, '.rb') + '_' + location.label
      }
      return 'unknown'
    end

    # return matching messages or nil if not enough matching messages found
    def wait_notification(text, params={})
      $log.info "wait_notification #{text.inspect}, #{params}"
      ($log.warn '  skipped by configuration'; return true) if @notifications == false
      count = params[:n] || 1
      qparams = {tstart: params[:tstart] || (@client.txtime - 2), tend: params[:tend], 
                 filter: {'Application'=>@notification_application}}
      msgs = nil
      res = wait_for(timeout: params[:timeout] || @notification_timeout, tstart: params[:tstart]) {
        msgs = @notifications.query(qparams) || return
        msgs.select! {|m| m['Message'] && m['Message'].include?(text)}
        msgs.size >= count
      }
      ($log.warn "  got #{msgs.size} msgs instead of #{count}"; return) if msgs.size != count
      @_msgs = msgs  # for debugging only
      return msgs
    end

  end
  
end
