=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.


Author: Conny Stenzel, 2020-04-01

History:

Notes:
  When logging in interactively as user cei '. /users/service/eiadmin/bin/ceienv' sets env and $PATH
=end

require 'socket'
require 'remote'
require 'misc'


module EIBL

  EIStatusCols = %w(HOST ENTITY EQ_MODEL LOCATION REMOTE SUS BASELINE LINE FAB APP_TYPE VAMOS 
    SUB_MODEL VERSION USER1 TOOL_VERSION PARCEL MODULE MAIN_TOOL PLATFORM_OWNER TOOL_TYPE MSR 
    AMMO CONTACT SETUPVERSION)

  # Access EI log files, EI config file
  class Server
    attr_accessor :server, :ceienv, :eqp, :eiversion, :eilogsdir, :eiconfig, :_cmd_env

    # e.g. 'itdc', 'HYB171', '10.1.0'
    def initialize(env, eqp, eiversion, params={})
      @server = RemoteHost.new("cei.#{env}", {host: 'f36eid4'}.merge(params))
      @ceienv = _config_from_file(env, 'etc/ceienv.conf').merge(params)
      @_cmd_env = "CEI_ENV=#{@ceienv[:cei_env]} CEI_ROOT=#{@ceienv[:cei_root]}"
      @eqp = eqp
      @eiversion = eiversion
      @eilogsdir = params[:eilogsdir] || "/local/logs/eilogs/#{@eqp}/"
    end

    def inspect
      "#<#{self.class} #{@server.host}, #{@eqp} v#{eiversion}>"
    end

    def read_log(log='log')
      @server.close
      log = @server.read_file(@eilogsdir + log) || return
      return log.lines
    end

    def read_eiconfig
      fname = "#{@ceienv[:cei_root]}/repository/#{@eqp}/#{@eiversion}/#{@eqp}.cfg"
      $log.info "read_eiconfig (#{fname})"
      @server.close
      res = @server.read_file(fname) || return
      ret = {}
      res.lines.each {|line|
        line.chomp!
        next if line.empty? || line.start_with?('#')
        ff = line.split
        ret[ff[0]] = ff[2].tr('"', '')  # ff[1] is the type like string, boolean
      }
      return ret
    end

    # read status line for this eqp, like eistatus @eqp
    def read_eistatus
      fname = "#{@ceienv[:cei_root]}/EIstatus/EIstatus"  # link to /amdapps/fabgui/database/EIstatus
      # $log.info "read_eistatus (#{fname})"
      @server.close
      res = @server.read_file(fname) || return
      ret = []
      res.lines.each {|line|
        line.chomp!
        next if line.empty? || line.start_with?('#')
        ff = line.split
        next if ff[1] != @eqp
        ret << Hash[EIStatusCols.zip(ff)]
      }
      return ret
    end

    # based on pseis, checks if @eqp is running on @host
    def ei_running?
      cmd = "#{@_cmd_env} /users/service/eiadmin/bin/pseis #{@eqp}"
      $log.info "ei_running? (#{@eqp})"
      @server.close
      res = @server.exec!(cmd) || return
      return !res.include?('not running')
    end

    # based on kill_ei
    def kill_ei
      cmd = "#{@_cmd_env} /users/service/eiadmin/bin/kill_ei #{@eqp}"
      $log.info "kill_ei (#{@eqp})"
      @server.close
      res = @server.exec!(cmd) || return
      return res.include?("done\nno child process found\n") || res.include?('process not found')
    end

    # based on start_ei, TODO: -i <QA_IMG>.im
    def start_ei
      cmd = "#{@_cmd_env} PATH=$PATH:/users/service/eiadmin/bin /users/service/eiadmin/bin/start_ei #{@eqp}"
      $log.info "start_ei (#{@eqp})"
      @server.close
      res = @server.exec!(cmd) || return
      $log.debug res
      return !res.include?('ERROR')
    end

    # like /users/service/cei/bin/hybSim, X server must be running!
    def start_toolsim
      cmd = "#{@_cmd_env} DISPLAY=#{Socket.gethostname}:0 VISUALWORKS=/3rd_party/visualworks/vw7.5"
      cmd += " nohup /ei/develop/vm/vw75_Linux /ei/develop/image/BaselineTestToolSimulator.im &; exit"
      $log.info "start_toolsim (#{cmd})"
      @server.close
      @server.open
      res = @server.ssh.exec(cmd) || return
      @server.close
      return res #.include?('VisualWorks(R)')
    end

    def kill_toolsims
      # kill $(ps aux | grep '[p]ython csp_build.py' | awk '{print $2}')
      cmd = "kill $(ps -ef|grep ToolSimulator|grep -v grep|awk '{print $2}')"
      $log.info "kill_toolsims (#{cmd})"
      @server.close
      res = @server.exec!(cmd) || return
      return res.empty? ||res.start_with?('Usage')
    end


    # move to eibltest?
    # def verify_eiconfig(params={})
    #   $log.info "verify_eiconfig"
    #   #ref = params[:ref] || File.join('./testparameters', @env, 'sil/advancedprops.yaml')
    #   #ref = YAML.load_file(ref) if ref.kind_of?(String)
    #   #return verify_hash(ref, read_jobprops || return)
    # end

  end

end
