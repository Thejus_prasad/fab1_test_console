=begin 
APC LotManager client and server emulator

(c) Copyright 2011 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: Steffen Steidten, 2012-02-07
=end

require 'builder'
require 'drb'
require 'java'
Dir["lib/lotmanager/*jar"].each {|f| require f}

$_apc_lmkey = Struct.new(:eqp, :lot, :pd)

# APC LotManager client and server emulator
module APC
  java_import 'com.globalfoundries.jcap.rtdservices.apc.model.ApcLotManagerKey'
  
  # APC LotManager client and server emulator
  #
  # connects to the ApcLotManager jCAP service 
  class LotManager
    attr_accessor :env, :servicename, :props, :ctx

    # emulator
    @@euri_envs = {
      "itdc"=>"rmi://f36asd7:22299",
      "dev"=>"rmi://localhost:22299"
    }
    # real service, for passthru or special queries
    @@uri_envs = {
      "itdc"=>"jnp://vf36capd01:2099",
      "dev"=>"jnp://vf36capd01:2099"
    }  
    
    def initialize(env, params={})
      @env = env
      @servicename = params[:servicename]
      @props = java.util.Properties.new
      params[:context].each_pair{|k,v| @props[k] = c} if params[:context]
      if params[:emulator]
        @props["java.naming.factory.initial"] ||= "com.sun.jndi.rmi.registry.RegistryContextFactory"
        @props["java.naming.provider.url"] ||= (params[:uri] or @@euri_envs[env])
        @servicename ||= "DummyApcLotManager"
      else
        @props["java.naming.factory.initial"] ||= "org.jboss.security.jndi.JndiLoginInitialContextFactory"
        @props["java.naming.factory.url.pkgs"] ||= "org.jboss.naming:org.jnp.interfaces"
        @props["java.naming.provider.url"] ||= @@uri_envs[env]
        @props["java.naming.security.principal"] ||= "apcr"
        @props["java.naming.security.credentials"] ||= "rtd"
        @servicename ||= "rtd-services-apc/ApcLotManager/remote"
      end
      @ctx = javax.naming.InitialContext.new(@props)
    end
    
    def lookup
      @ctx.lookup(@servicename)
    end
    
    # keys is an array of $_apc_lmkey structs or [eqp, lot, pd] arrays
    #
    # return java array of ApcLotManagerKeys
    def lmkeys(keys)
      keys.collect {|key|
        key = $_apc_lmkey.new(*key) if key.kind_of?(Array)
        pd = key.pd
        pd = pd.split('.')[0]   # remove trailing version
        ApcLotManagerKey.new(key.eqp, key.lot, pd)
      }.to_java(ApcLotManagerKey)
    end
    
    # call the remote ApcLotManager#getLotManager method
    #
    # keylist is an array of ApcLotManagerKeys or $_apc_lmkeys or [eqp, lot, pd] arrays
    #
    # returns array of strings, suitable as answer to RTD
    def lot_manager(keylist)
      keylist = lmkeys(keylist) if keylist.kind_of?(Array)
      lookup.getLotManager(keylist).collect
    end
  end
  
  # access ApcLotManagerKeys as $_apc_lmkey struct
  #
  # APC LotManager client and server emulator
  class ApcLotManagerKey
    def to_apc_lmkey
      $_apc_lmkey.new(self.eqp_id, self.lot_id, self.pd_id)
    end
  end

  # APC LotManager Implementation
  class LotManagerEngine
    attr_accessor :env, :passthru, :lmclient

    def initialize(env, params={})
      @env = env
      @lmclient = LotManager.new(env, params) # for lookups to the real LotManager
      @passthru = false
      $log.info inspect
    end
    
    def inspect
      "#<#{self.class.name} env: #{@env}>"
    end
    
    def reload
      load __FILE__
    end
    
    # key is a $_apc_lmkey struct
    #
    # returns an XML string
    def lm_entry(key, params={})
      pd = key.pd
      pd = pd.split('.')[0]   # remove trailing version
      lastupdate = (params[:lastupdate] or Time.now)
      nlastupdate = lastupdate.strftime("%Y%m%d.%H%M%S")
      wafers = (params[:wafers] or "all")
      #
      xml = Builder::XmlMarkup.new(:indent=>2)
      xml.instruct!
      xml.LotSummary "lotID"=>key.lot, "operationID"=>pd, "equipmentID"=>key.eqp, "nLastUpdate"=>nlastupdate do |ls|
        ls.SelectedWafer "Value"=>"all"
      end
      xml.target!
    end
    
    # called by DummyApcLotManager#getLotManager
    #
    # keylist is an array of ApcLotManagerKeys or, for testing only, array of [eqp, lot, pd] arrays
    # 
    # return array of strings
    def handle_request(keylist)

      if @passthru
        res = @lmclient.lot_manager(keylist)
        $log.debug "lmclient.handle_request (passthru) result:\n#{res.inspect}"
        return res
      else
        res = keylist.collect {|key|
          key = $_apc_lmkey.new(*key) if key.kind_of?(Array)  # for testing purposes only
          key = key.to_apc_lmkey if key.instance_of?(ApcLotManagerKey)
          # simple answer
          lm_entry(key)
        }
        $log.debug "lmclient.handle_request result:\n#{res.inspect}"
        return res
      end
    end
  end
  
  # remote control of a LotManagerEngine
  #
  # APC LotManager client and server emulator
  class LotManagerRemote
    attr_accessor :env, :srv, :engine, :lmclient
    
    @@uri_envs = {"itdc"=>["f36asd7", 32298]}
    
    def initialize(env, params={})
      @env = env
      host, port = params[:host], params[:port]
      host, port = @@uri_envs[env] unless host and port
      DRb.start_service
      @srv = DRbObject.new_with_uri("druby://#{host}:#{port}")
      class << @srv
        undef :instance_eval  # force call to be passed to remote object
      end
      @engine = @srv.engine
      class << @engine
        undef :instance_eval  # force call to be passed to remote object
      end
      @lmclient = @engine.lmclient
      class << @lmclient
        undef :instance_eval  # force call to be passed to remote object
      end
    end
    
    def reval(*a, &p)
      @rsrv.instance_eval(*a, &p)
    end
  end
end
