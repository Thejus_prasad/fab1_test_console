=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.


Author: Steffen Steidten, 2011-11-23

History:
  2012-08-31 ssteidte,  added subfolder and organizational folder search, e.g. $lds.folder("/28BK/SHELBY")
  2012-09-03 dsteger,   added support for event folders
  2015-07-02 ssteidte,  added support for server timezone (e.g. in Sample)
  2016-05-23 sfrieske,  fixed get_limit_value for JRuby 9.1
  2016-09-02 sfrieske,  Session.initialize honours all parameters from config file
=end

require 'base64'
require 'corba'
require 'misc'

#get english language for ca messages since Space 7.1
java.util.Locale.setDefault(java.util.Locale::ENGLISH)

# SPACE Testing Support
module Space
  module SpcApi
    constants.each {|c| remove_const(c)}
    #
    CalculatedParam = Struct.new(:name, :param, :formula, :trigger, :refs)
    ChannelData = Struct.new(:chid, :name, :desc, :parameter, :unit, :recipe, :tool, :chamber, :division)
    SpcCKey = Struct.new(:keyid, :fixed, :exclude)
    SpcEKey = Struct.new(:keyid, :name, :desc, :refonly)
    SpcMovingValue = Struct.new(:ma, :ms, :ewma_mean, :ewma_range, :ewma_sigma)
    SpcSampleStats = Struct.new(:mean, :median, :min, :max, :range, :sigma, :size)
    SpcTriggerKey = Struct.new(:keyid, :name, :value)
    @@limit_ids = nil
    @@speclimit_ids = nil
    @@javaSpaceApi = nil

    # dynamically load the libraries, return true on success
    def self.init(params={})
      # loading JAVA libs
      flavour = params[:flavour] || 'inline'
      libfiles = "lib/space/#{flavour}/*jar"
      $log.info "loading lib files #{libfiles.inspect}"
      Dir[libfiles].sort.each {|f| require f}
      require 'spacecompat'  # why is this necessary on some machines and not on others??
      java_import 'com.camline.space.navigator.SpaceCAPSFactory'
      @@javaSpaceApi = Java::ComCamlineSpaceApi
      # often used constants, arrays of [id, name] with id as integer from 0...
      # constant NUM_LIMITS is rejected because its not a value in the API
      @@limit_ids = @@javaSpaceApi::SampleLimits.constants.reject { |c| c == :NUM_LIMITS }.collect {|c|
        [@@javaSpaceApi::SampleLimits.const_get(c), c.to_s]
      }.sort
      @@speclimit_ids = @@javaSpaceApi::SampleSpecLimits.constants.collect {|c|
        [@@javaSpaceApi::SampleSpecLimits.const_get(c), c.to_s]
      }.sort
      #
      return true
    end


    class Session
      include Space::SpcApi
      attr_accessor :env, :params, :srvtimezone, :ds, :user, :props, :src, :session

      def initialize(env, params={})
        @env = env
        @params = (_config_from_file(env, 'etc/space.conf') || {}).merge(params)
        @srvtimezone = @params[:srvtimezone] || (@env.start_with?('f8', 'mtqa') ? '-05:00' : '+00:00')
        @props= @params[:props] || "etc/space/#{@env}.space.navigator.properties"
        @ds = @params[:ds]
        @user = @params[:user]
        pw = @params[:cleartext] ? @params[:password] : Base64.decode64(@params[:password])
        Space::SpcApi.init(flavour: @params[:flavour]) if @@javaSpaceApi.nil?
        @session = @@javaSpaceApi::SpaceAPISessionFactory.createSpaceAPISession(@user, pw, @props)
        @session.setViewDeletedObjects(true)  # show objects marked for deletion
        @src = source(ds)
      end

      def inspect
        "#<#{self.class.name} env: #{@env}, #{@src.inspect}>"
      end

      def close
        @session.terminate if @session and !@session.terminated?
      end

      def sources(name=nil)
        res = name ? @session.getAllSourcesNamed(name) : @session.getAllSources
        res.collect {|e| Source.new(e, @srvtimezone)} if res
      end

      def source(name)
        res = sources(name) || return
        return if res.size != 1
        return res[0]
      end

      def source=(name)
        @src = source(name)
      end

      def ldses(name=nil)
        @src.ldses(name)
      end

      def lds(name)
        @src.lds(name)
      end

      def event_folder(name)
        @session.get_all_event_folders_named(name).collect {|f| EventFolder.new(f)}
      end

      def spc_channel(chid)
        spcch = @session.getSPCChannel(chid) || ($log.warn "no channel with id #{chid}"; return)
        SpcChannel.new(spcch, @srvtimezone)
      end

      # move Space::SpcApi::Channel to new Space::SpcApi::Folder
      #
      # return new Space::SpcApi::Channel object on success
      def move_spc_channel(ch, targetfolder)
        cfg = ch.channel.config
        ch.delete
        res = targetfolder.folder.create_spc_channel(cfg) || return
        return SpcChannel.new(res, @srvtimezone)
      end

      def tsgs(valuation={})
        if valuation.class == Space::SpcApi::Valuation
          # get actions for this valuation
          valuation.events.collect {|e|
            TroubleShootingGuide.new(@session.tsg(e.id)) if e.type == 2
          }.compact
        else
          # get defined actions, treat valuation as parameter hash
          department = valuation[:department]
          division = valuation[:division]
          @session.getAllTSGsNamed(valuation[:name] || '*').collect {|e|
            next if department && department != e.config.department
            next if division && division != e.config.division
            TroubleShootingGuide.new(e)
          }.compact
        end
      end

      def tsg(name, params={})
        res = tsgs({name: name}.merge(params))
        return res[0]
      end

      def corrective_actions(valuation={})
        if valuation.class == Space::SpcApi::Valuation
          # get actions for this valuation
          valuation.events.collect {|e|
            CorrectiveAction.new(@session.get_corrective_action(e.id)) if e.type == 1
          }.compact
        else
          # get defined actions, treat valuation as parameter hash
          department = valuation[:department]
          division = valuation[:division]
          @session.getAllCorrectiveActionsNamed(valuation[:name] || '*').collect {|e|
            next if department && department != e.config.department
            next if division && division != e.config.division
            CorrectiveAction.new(e)
          }.compact
        end
      end

      def corrective_action(name, params={})
        res = corrective_actions({name: name}.merge(params))
        # ($log.warn "no unique result for corrective_action #{name.inspect}: #{res.inspect}"; return) unless res.size == 1
        return res[0]
      end

      def events(valuation={})
        if valuation.class == Space::SpcApi::Valuation
          # get actions for this valuation
          valuation.events.collect {|e|
            case e.type
            when 1
              CorrectiveAction.new(@session.corrective_action(e.id))
            when 2
              TroubleShootingGuide.new(@session.tsg(e.id))
            end
          }.compact
        else
          # get defined actions, treat valuation as parameter hash
          department = valuation[:department]
          division = valuation[:division]
          res = @session.getAllCorrectiveActionsNamed(valuation[:name] || '*').collect {|e|
            next if department && department != e.config.department
            next if division && division != e.config.division
            CorrectiveAction.new(e)
          }.compact
          res += @session.getAllTSGsNamed(valuation[:name] || '*').collect {|e|
            next if department && department != e.config.department
            next if division && division != e.config.division
            TroubleShootingGuide.new(e)
          }.compact
          res
        end
      end

      # def event(name, params)
      #   events({name: name}.merge(params)).first
      # end

      # in channel ch, set events
      def set_events(ch, canames, params={})
        canames = [canames] if canames.kind_of?(String)
        cas = canames.collect {|n| event(n, params)}
        ch.set_events(cas, params)
      end

      # in channel ch, set corrective actions for all valuations
      def set_corrective_actions(ch, canames, params={})
        canames = [canames] if canames.kind_of?(String)
        cas = canames.collect {|n| corrective_action(n, params)}
        ch.set_events(cas, params)
      end

      # in channel ch, set tsg for all valuations
      def set_tsgs(ch, canames, params={})
        canames = [canames] if canames.kind_of?(String)
        cas = canames.collect {|n| tsg(n, params)}
        ch.set_events(cas, params)
      end
    end


    class Source
      attr_accessor :source, :name, :srvtimezone

      def initialize(src, srvtimezone='UTC')
        @source = src
        @name = @source.name
        @srvtimezone = srvtimezone
      end

      def inspect
        "#<#{self.class.name} #{@name.inspect}>"
      end

      def ldses(name=nil)
        res = name ? @source.getAllLDSNamed(name) : @source.getAllLDSes
        res.collect {|e| LDS.new(e, @srvtimezone)} if res
      end

      def lds(name)
        res = ldses(name) || return
        return unless res.size == 1
        return res.first
      end
    end


    class SubFolders
      attr_accessor :srvtimezone

      def folders(name=nil)
        name = name.split('/') if name.kind_of?(String)
        fromroot = (name[0] == '') if name
        n = name.pop if name
        # parent folders ?
        return org_folder(name).folders(n) if name && !name.empty?
        # no path (any more)
        l = self.class == Space::SpcApi::LDS ? @lds : @folder
        return [] unless l
        l.forget_children #cleanup existing data
        res = name ? l.get_all_lds_folders_named(n) : l.get_all_lds_folders
        return res.collect {|e|
          Folder.new(e, @srvtimezone) unless fromroot && e.folder_parent != lds
        }.compact
      end

      def folder(name)
        ff = folders(name) || return
        ($log.info "no such folder: #{self.name}/#{name}"; return) if ff.empty?
        ($log.warn "multiple folders: #{self.name}/#{name}"; return) unless ff.size == 1
        return ff.first
      end

      def org_folders(name=nil)
        name = name.split('/') if name.kind_of?(String)
        fromroot = (name[0] == '') if name
        name.shift if fromroot
        n = name.shift
        l = self.class == Space::SpcApi::LDS ? @lds : @folder
        return [] unless l
        l.forget_children #cleanup existing data
        res = name ? l.get_all_organizational_folders_named(n) : l.get_all_organizational_folders
        ret = res.collect {|e|
          Folder.new(e, @srvtimezone) unless fromroot && e.folder_parent != lds
        }.compact
        return ret if name.empty?
        # go to subfolder
        return ret[0].org_folders(name)
      end

      def org_folder(name)
        ff = org_folders(name) || ff
        return unless ff.size == 1
        return ff.first
      end
    end


    class LDS < SubFolders
      attr_accessor :lds, :name

      def initialize(lds, srvtimezone='UTC')
        @lds = lds
        @name = @lds.get_name
        @srvtimezone = srvtimezone
      end

      def inspect
        "#<#{self.class.name} #{@name.inspect}>"
      end

      def extractor_keys(params={})
        ekeys = @lds.config.extractor_keys.collect {|e| ExtractorKey.new(e)}
        return params[:raw] ? ekeys : ekeys.collect {|e| e.inspect}
      end

      def create_folder(name, params={})
        $log.info "create_folder #{name.inspect}, #{params.inspect}"
        tmpl = params[:template]
        if tmpl
          ff = folders(tmpl)
          ($log.warn "  template folder missing"; return) unless ff && ff.size == 1
          newcfg = ff.first.folder.config
        else
          newcfg = @lds.lds_folder_default_config
        end
        # set name
        newcfg.name = name
        # create folder
        f = @lds.create_lds_folder(newcfg)
        return Folder.new(f, @srvtimezone)
      end

      # get all spc channels in lds that match name (wildcard is *)
      def all_spc_channels(name, params={})
        @lds.get_all_spc_channels_named(name).collect {|c|
          SpcChannel.new(c, @srvtimezone) unless c.is_marked_for_deletion && !params[:deleted]
        }.compact
      end
    end


    class Folder < SubFolders
      attr_accessor :folder, :name

      def initialize(f, srvtimezone='UTC')
        @folder = f
        @name = @folder.config.name
        @srvtimezone = srvtimezone
      end

      def inspect
        "#<#{self.class.name} #{@folder.lds.get_name}/#{@name}>"
      end

      def lds
        LDS.new(@folder.lds, @srvtimezone)
      end

      # delete all channels identified by parameters in this folder
      #
      # return true on success
      def delete_channels(params={})
        ret = true
        silent = params.delete(:silent)
        channels = spc_channels(params)
        unless silent && channels.empty?
          $log.info "#{inspect}: delete_channels #{params.inspect}"
          $log.info "  deleting #{channels.size} channels"
          channels.each_with_index {|ch, i|
            $log.info "    ch ##{i}" if (i % 10 == 0) && (i != 0)
            res = ch.delete(params)
            ($log.warn "  error deleting channel #{ch.name} #{ch.parameter}"; ret=false) if res != 0
          }
        end
        # 2times doesn't work since we need the deleted tag!
        channels = spc_channels(params.merge(deleted: true))
        unless silent && channels.empty?
          $log.info "  finally deleting #{channels.size} channels"
          channels.each_with_index {|ch, i|
            $log.info "    ch ##{i}" if (i % 10 == 0) && (i != 0)
            res = ch.delete(params)
            ($log.warn "  error deleting channel #{ch.name} #{ch.parameter}"; ret=false) if res != 0
          }
        end
        return ret
      end

      # delete all channels in the folder, then the folder
      #
      # return 0 on success
      def delete
        delete_channels
        $log.info "folder #{lds.name}/#{@name}: delete"
        @folder.delete
      end

      def name=(name, comment="QA Test #{Time.now.utc.iso8601}")
        cfg = @folder.config
        cfg.name = name
        @folder.set_config(cfg, comment)
        @name = @folder.config.name
      end

      def parent
        Folder.new(@folder.folder_parent, @srvtimezone)
      end

      def spc_channels(params={})
        @folder.get_all_spc_channels.collect {|e|
          next if e.is_marked_for_deletion && !params[:deleted]
          ch = SpcChannel.new(e, @srvtimezone)
          ok = true
          params.each_pair {|k,v| ok &= (ch.send(k) == v)}  # filter
          ok ? ch : nil
        }.compact
      end

      # params is either a channel name, channel id or hash like :parameter=>p
      #
      # return channel if exactly one exists or nil
      def spc_channel(params)
        params = {chid: params} if params.kind_of?(Numeric)
        params = {name: params} if params.kind_of?(String)
        chs = spc_channels(params) || return
        return unless chs.size == 1
        return chs[0]
      end

      def create_spc_channel(name, parameter, unit, ckeys, params={})
        newcfg = @folder.spc_channel_default_config
        newcfg.name = name
        newcfg.parameter_name = parameter
        newcfg.parameter_unit = unit
        newcfg.description = params[:description] || ''
        newcfg.spc_channel_keys = ckeys.collect {|e| e.ckey}
        ch = @folder.create_spc_channel(newcfg)
        SpcChannel.new(ch, @srvtimezone)
      end

      def calculated_parameters(params={})
        @folder.getAllCalculatedParameter.collect {|calc|
          c = calc.getConfig
          c_refs = (c.getReferences || []).collect {|r| r.getParameterName}
          CalculatedParam.new(c.name, c.param_name, c.formula_text, c.trigger.parameter_name, c_refs)
        }
      end

      def mpc_channels(params={})
        @folder.getAllMPCs.collect {|e|
          ch = MPC.new(e)
          ok = true
          params.each_pair {|k,v| ok &= (ch.send(k) == v)}  # filter
          ok ? ch : nil
        }.compact
      end
    end


    class ExtractorKey
      attr_accessor :ekey, :name

      def initialize(k)
        @ekey = k
        @name = @ekey.name
      end

      def inspect
        SpcEKey.new(@ekey.get_id, @ekey.name, @ekey.description, @ekey.reference_only)
      end
    end


    class EventFolder
      attr_accessor :folder, :name

      def initialize(f)
        @folder = f
        @name = @folder.config.name
      end

      def inspect
        "#<#{self.class.name} #{@name.inspect}>"
      end

      def all_corrective_actions
        @folder.all_corrective_actions.collect {|ca| CorrectiveAction.new(ca)}
      end

      def all_tsgs
        @folder.all_ts_gs.collect {|tsg| TroubleShootingGuide.new(tsg)}
      end

      def create_corrective_action(name, text, attributes)
        cas = all_corrective_actions || return
        config = cas[0].ca.get_config
        config.name = name
        config.text = text
        config.attributes = attributes
        config.department = 'QA'
        config.author = 'QA'
        CorrectiveAction.new(@folder.create_corrective_action(config))
      end
    end


    class Channel
      include Space::SpcApi
      attr_accessor :channel, :srvtimezone, :chid, :parameter, :unit

      def initialize(c, srvtimezone='UTC')
        @channel = c
        @srvtimezone = srvtimezone
        @chid = @channel.channel_id
        @parameter = @channel.parameter_name
        @unit = @channel.parameter_unit
      end

      def inspect
        cfg = @channel.config
        ChannelData.new(@chid, cfg.name, cfg.description, @parameter, @unit, nil, nil, nil, cfg.division)
      end

      # returns channel data as hash, avoids clumsy constructs like ch.inspect.to_hash
      def to_hash
        cfg = @channel.config
        {chid: @chid, name: cfg.name, desc: cfg.description, parameter: @parameter, unit: @unit, division: cfg.division}
      end

      def ckcid
        @channel.ckc_id
      end

      def config
        @channel.config
      end

      def limits(params={})
        clc = @channel.config.getControlLimitsConfig
        return {} unless clc.isGlobalLimitsEnable || params[:all]
        a = []
        if params[:enabled]
          @@limit_ids.each {|i, c| a << [c, clc.get_limit_value(i)] if clc.limit_enabled?(i)}
        elsif params[:fixed]
          @@limit_ids.each {|i, c| a << [c, clc.limit_enabled?(i) ? clc.get_limit_value(i) : nil] if clc.engineering_limit?(i)}
        else
          @@limit_ids.each {|i, c| a << [c, (params[:all] || clc.limit_enabled?(i)) ? clc.get_limit_value(i) : nil]}
        end
        Hash[a]
      end

      def deleted
        @channel.is_marked_for_deletion
      end

      def customer_fields
        cfields = @channel.config.getAttachedCustomerFields
        h = {}
        cfields.each do |cf|
          _type = cf.get_type
          _value = cf.get_value
          case _type
          when 5
            _value = _value.to_i
          when 6
            _value = _value.to_f
          when 1
            _value = (_value == 'true')
          when 2,3
            _value
          else
            $log.warn "unsupported type: #{_type}"
          end
          h[cf.get_name] = _value
        end
        return h
      end

      # Override the channel's extractor keys, expects array with channel extractor key struct
      def set_extractor_keys(keys)
        cfg = @channel.config
        ckeys = cfg.getAllSPCChannelKeys
        newkeys = []
        keys.each {|key|
          ckey = ckeys.find {|ck| ck.getId == key.keyid}
          if ckey
            ckey.fixed_value = key.fixed
            ckey.excluded_values = key.exclude
            newkeys << ckey
          else
            $log.warn "id #{key} not found!"
          end
        }
        cfg.setSPCChannelKeys(newkeys)
        @channel.set_config(cfg, "QA test #{Time.now.utc.iso8601}", false)
      end

      def get_extractor_keys
        @channel.config.getSPCChannelKeys.collect {|k| SpcCKey.new(k.get_id, k.fixed_value, k.excluded_values)}
      end

      def set_customer_fields(fields)
        cfg = @channel.config
        _cfields = @@javaSpaceApi::SPCChannelConfig.getAllCustomerFields
        _nfields = []
        _cfields.each do |c|
          name = c.get_name
          if fields.has_key?(name)
            c.setValue(fields[name])
            _nfields << c
          end
        end
        cfg.setAttachedCustomerFields(_nfields)
        @channel.set_config(cfg, "QA test #{Time.now.utc.iso8601}", false)
      end

      def state
        return @channel.config.getCurrentSPCChannelState.state
      end

      def set_state(stat)
        $log.info "channel #{@parameter}: set_state #{stat.inspect}"
        cfg = @channel.config
        avail_states = cfg.class.getAvailableSPCChannelStates2
        _state_obj = avail_states.find {|s| s.to_s == stat} || ($log.warn "  #{stat.inspect} is not available"; return false)
        cfg.setCurrentSPCChannelState(_state_obj)
        @channel.set_config(cfg, "QA test #{Time.now.utc.iso8601}", false)
        return (stat == state)
      end

      def set_limits(limits, params={})
        cfg = @channel.config
        clc = cfg.getControlLimitsConfig
        clc.setGlobalLimitsEnabled(true) unless clc.isGlobalLimitsEnable
        #($log.warn "limits are disabled"; return false) unless clc.isGlobalLimitsEnable
        limits.each_pair {|limit, value|
          ll = @@limit_ids.rassoc(limit) || ($log.warn "limit #{limit} not found"; return false)
          l = ll[0]
          #l = JavaSpaceApi.SampleLimits.const_get(limit) || ($log.warn "limit #{limit} not found"; return false)
          if value
            clc.setLimitEnabled(l, true)
            clc.setLimitValue(l, value)
          else
            clc.setLimitEnabled(l, false)
          end
          clc.setEngineeringLimit(l, params[:fixed]) if params[:fixed]
        }
        cfg.setControlLimitsConfig(clc)
        @channel.set_config(cfg, "QA test #{Time.now.utc.iso8601}", false)
        true
      end

      def global_limits_enabled(enable=true)
        cfg = @channel.config
        clc = cfg.getControlLimitsConfig
        clc.setGlobalLimitsEnabled(enable)
        cfg.setControlLimitsConfig(clc)
        @channel.set_config(cfg, "QA test #{Time.now.utc.iso8601}", false)
      end

      def global_limits_enabled?
        cfg = @channel.config
        clc = cfg.getControlLimitsConfig
        clc.isGlobalLimitsEnable
      end

      def name
        @channel.config.name
      end

      def name=(name)
        cfg = @channel.config
        cfg.name = name
        @channel.set_config(cfg, "QA test #{Time.now.utc.iso8601}", false)
      end

      def division
        @channel.config.division
      end

      def division=(division)
        cfg = @channel.config
        cfg.division = division
        @channel.set_config(cfg, "QA test #{Time.now.utc.iso8601}", false)
      end

      # return 0 on success
      def delete(params={})
        if @channel.is_marked_for_deletion
          @channel.permanently_delete
        else
          @channel.delete(params[:cascade] != false, params[:samples] != false)
        end
      end

      # get array of Sample, optionally filtered
      def samples(params={})
        from = params[:tstart]
        to = params[:tend]
        count = params[:count] || 0
        days = params[:days] || 0
        flagged = (params[:flagged] != false)
        res = @channel.get_samples(from, to, count, days, flagged)
        return res if params[:raw]
        res.collect {|e|
          s = Sample.new(e, @srvtimezone)
          ekeys = s.extractor_keys
          wafer = params[:wafer]
          next if wafer and ekeys['Wafer'] != wafer
          lot = params[:lot]
          next if lot and ekeys['Lot'] != lot
          cha = params[:chamber]
          next if cha and ekeys['PChamber'] != cha
          s
        }.compact
      end

      def sample(sid, params={})
        samples(params).find {|s| s.sid == sid}
      end

      def valuations
        @channel.config.getAttachedValuations.collect {|e| Valuation.new(e)}
      end

      # vv is an array of Space::Valuation objects
      def set_valuations(vv)
        cfg = @channel.config
        $log.debug {"set_valuations #{vv.inspect}"}
        cfg.attached_valuations = vv.collect {|v| v.valuation}
        @channel.set_config(cfg, "QA test #{Time.now.utc.iso8601}", false)
      end

      def self.all_valuations
        @@javaSpaceApi::SPCChannelConfig.getAllDefinedValuations.collect {|e| Valuation.new(e)}
      end
    end


    class SpcChannel < Channel
      def keys(params={})
        res = params[:all] ? @channel.config.all_spc_channel_keys : @channel.config.spc_channel_keys
        res.collect {|e| SpcExtractorKey.new(e)}
      end

      def ckcs
        @channel.allCKCs.collect {|e| CKC.new(e, @srvtimezone)}
      end

      def samples(params={})
        return super unless params[:ckc]
        cc = ckcs
        ($log.warn "no CKCs for SPC channel #{self}"; return) unless cc && !cc.empty?
        cc.collect {|c| c.samples(params)}.flatten
      end

      # set events like corrective actions for all valuations or list of valuation ids
      def set_events(evs, params={})
        vv = valuations
        vv.each do |v|
          if (params[:valuations] && !params[:valuations].member?(v.vid))
            v.set_events([])
          else
            $log.debug {"set event #{evs.inspect}"}
            v.set_events(evs)
          end
        end
        set_valuations(vv)
      end

      def set_email_addresses(s)
        vv = valuations
        vv.each {|v| v.set_email_addresses(s)}
        set_valuations(vv)
      end

      # WORK IN PROGRESS, from v7.1 on usable
      def capture_image(fname, params={})
        tend = params[:tend] || Time.now
        tstart = params[:tstart] || tend - 86400 * 7  # 1 week
        siv = @@javaSpaceApi::SamplesSelectionInterval.new(tstart, tend)
        cfg = @channel.config
        cdm = cfg.getSPCChannelSettingsConfig.getChartDMConfig
        vc = cfg.getVisualChartConfig
        vc.setShowPluginsArea(false)
        cfg.setVisualChartConfig(vc)
        cic = @@javaSpaceApi.ChartImageConfig.new  # (java.awt.Dimension.new(params[:width] || 850, params[:height] || 900))
        cic.setChartDisplayModeConfig(cdm)
        img = @channel.captureImage(cic, siv, nil, nil) # java.util.ArrayList.new([charttype])
        return img if fname.nil?
        javax.imageio.ImageIO.write(img, 'PNG', java.io.File.new(fname))
      end

    end


    class CKC < Channel
      attr_accessor :ckcid

      def initialize(ckc, srvtimezone)
        super
        @ckcid = ckc.id
      end

      # WORK IN PROGRESS, from v7.1 on usable
      def capture_image(fname, params={})
        tend = params[:tend] || Time.now
        tstart = params[:tstart] || tend - 86400 * 7  # 1 week
        siv = @@javaSpaceApi::SamplesSelectionInterval.new(tstart, tend)
        cfg = @channel.parent.config
        cdm = cfg.getSPCChannelSettingsConfig.getChartDMConfig
        vc = cfg.getVisualChartConfig
        vc.setShowPluginsArea(false)
        cfg.setVisualChartConfig(vc)
        cic = @@javaSpaceApi.ChartImageConfig.new  # (java.awt.Dimension.new(params[:width] || 850, params[:height] || 900))
        cic.setChartDisplayModeConfig(cdm)
        img = @channel.captureImage(cic, siv, nil, nil) # java.util.ArrayList.new([charttype])
        return img if fname.nil?
        javax.imageio.ImageIO.write(img, 'PNG', java.io.File.new(fname))
      end

    end


    class Valuation
      include Space::SpcApi
      attr_accessor :valuation, :vid, :name, :comment_req

      def initialize(v)
        @valuation = v
        @vid = v.id
        @name = v.name
        @comment_req = v.is_comment_required
      end

      def inspect
        "#<#{self.class.name} #{@name.inspect} (id: #{vid})>"
      end

      def parameters
        Hash[@valuation.parameter_num.times.collect {|i| [@valuation.parameter_name(i), @valuation.parameter_value(i)]}]
      end

      def events
        @valuation.getAttachedValuationEvents.to_a  #collect {|e| Event.new(e)}
      end

      # set attached valuation events from a list of Events or CorrectiveActions
      def set_events(data)
        data = [data] unless data.kind_of?(Array)
        evs = data.collect {|e|
          # if e.class == Space::SpcApi::Event
          #   e.event
          if e.kind_of? Space::SpcApi::CorrectiveAction
            @@javaSpaceApi::ValuationEventIdentifier.new(e.ca)
          elsif e.kind_of? Space::SpcApi::TroubleShootingGuide
            @@javaSpaceApi::ValuationEventIdentifier.new(e.tsg)
          else
            e
          end
        }
        @valuation.setAttachedValuationEvents(evs)
      end

      def email_addresses
        @valuation.email_addresses
      end

      def set_email_addresses(s)
        @valuation.email_addresses = s
      end
    end


    # class Event
    #   attr_accessor :event, :eid, :etype

    #   def initialize(e)
    #     @event = e
    #     @eid = e.id
    #     @etype = e.type
    #   end
    # end


    class CorrectiveAction
      attr_accessor :ca, :caid, :name, :text, :department, :division, :attributes

      def initialize(ca)
        @ca = ca
        @caid = ca.id
        @name = ca.config.name
        @text = ca.config.text
        @department = ca.config.department
        @division = ca.config.division
        @attributes = ca.config.attributes.to_a
      end

      def inspect
        "#<#{self.class.name} #{@name.inspect}>"
      end

      #used for export to file
      def export
        "#<#{self.class.name} @caid=#{@caid.inspect}, @name=#{@name.inspect}, @text=#{@text.inspect}, @division=#{@division.inspect}, @department=#{@department.inspect}, @attributes=#{@attributes.inspect}>"
      end
    end


    class TroubleShootingGuide
      attr_accessor :tsg, :tsgid, :name, :department, :division, :filename

      def initialize(tsg)
        @tsg = tsg
        @tsgid = tsg.id
        @name = tsg.config.name
        @department = tsg.config.department
        @division = tsg.config.division
        @filename = tsg.config.file_name
      end

      def inspect
        "#<#{self.class.name} #{@name.inspect}>"
      end

      #used for export to file
      def export
        "#<#{self.class.name} @tsgid=#{@tsgid.inspect}, @name=#{@name.inspect}, @department=#{@department.inspect}, @division=#{@division.inspect}, @filename=#{@filename.inspect}>"
      end
    end


    class Sample
      include Space::SpcApi
      attr_accessor :sample, :srvtimezone, :sid, :iflag, :eflag, :excluded, :time

      def initialize(sample, srvtimezone='UTC')
        @sample = sample
        @srvtimezone = srvtimezone
        _init
      end

      def inspect
        "#<#{self.class.name} #{@sid.inspect}>"
      end

      def flag
        @sample.flag
        _init
      end

      def unflag
        @sample.unflag
        _init
      end

      def _init
        @sid = @sample.to_s.to_i       # not working because of 32 bit limitation: get_id
        @iflag = @sample.internally_flagged
        @eflag = @sample.externally_flagged
        @excluded = @sample.excluded
        @time = Time.at(@sample.date.time/1000)
        nil
      end

      # return sample chart
      def _get_chart(params={})
        chart = params[:chart]
        unless chart
          cc = @sample.charts
          ($log.warn "no unique chart for sample #{sid}"; return) unless cc && cc.size == 1
          chart = cc.first
        end
        return chart
      end

      def parameter
        @sample.parameter_name
      end

      def unit
        @sample.parameter_unit
      end

      def ecomment
        @sample.external_comment
      end

      # return true if s is part of the comment
      def ecomment?(s)
        return (ecomment && ecomment.include?(s))
      end

      def ecomment=(s, params={})
        chart = _get_chart(params) || return
        @sample.setExternalComment(chart, s)
      end

      def icomment
        @sample.internal_comment
      end

      # return true if s is part of the comment
      def icomment?(s)
        return (icomment && icomment.include?(s))
      end

      def icomment=(s)
        @sample.set_internal_comment(s)
      end

      #def charts
      #  @sample.charts
      #end

      def lds
        @sample.get_lds_id
      end

      def data_keys
        dkeys = Hash[@sample.data_key_names.zip( @sample.data_keys)]
        if dkeys.has_key?('MTime')
          dkeys['MTime'] = (dkeys['MTime'] == '-') ? nil : Time.strptime("#{dkeys['MTime']} #{@srvtimezone}", '%Y/%m/%d %H:%M:%S:%N %Z')
        end
        return dkeys
      end

      def extractor_keys
        Hash[@sample.extractor_key_names.zip(@sample.extractor_keys)]
      end

      def raws
        @sample.raws.collect {|e| RawValue.new(e)}
      end

      def raw_values
        raws.collect {|e| e.value}
      end

      def raw_value_data_keys
        raws.collect {|e| e.data_keys}
      end

      def moving_values(params={})
        chart = _get_chart(params) || return
        mv = @sample.get_moving_values(chart)
        SpcMovingValue.new(mv.ma, mv.ms, mv.ewma_mean, mv.ewma_range, mv.ewma_sigma)
      end

      def statistics
        st = @sample.get_statistics
        SpcSampleStats.new(st.mean, st.median, st.min, st.max, st.range, st.sigma, st.sample_size)
      end

      def limits(params={})
        chart = _get_chart(params) || return
        l = @sample.get_limits(chart)
        Hash[@@limit_ids.collect {|i, c| [c, (params[:all] || l.limit_enabled(i)) ? l.get_limit_value(i) : nil]}]
      end

      def specs(params={})
        l = @sample.get_specifications
        Hash[@@speclimit_ids.collect {|i, c| [c, (params[:all] || l.limit_enabled(i)) ? l.get_limit_value(i) : nil]}]
      end

      def assign_ca(ca, params={})
        begin
          vv = violations(params)
          ($log.warn "no violations for sample #{self}"; return) unless vv && !vv.empty?
          comment = params[:comment] || "QA test #{Time.now.utc.iso8601}"
          chart = _get_chart(params) || return
          ca = ca.ca if ca.class == Space::SpcApi::CorrectiveAction
          @sample.assign_corrective_action(chart, ca, comment)
          return true
        rescue Exception=>e
          $log.warn "An exception occured during execution of method 'assign_ca'. Exception: #{e.pretty_inspect}"
          return !e.to_s.include?('The SPACE Listener generated following error')
        end
      end

      def violations(params={})
        chart = _get_chart(params) || return
        @sample.getViolations(chart).to_a  #collect {|e| Violation.new(e)}
      end

      # def violation_comment(params={})
      #   vv = violations(params) || return
      #   return if vv.empty?
      #   return vv.first.comment
      # end

      # def violation_comment=(s, params={})
      #   chart = _get_chart(params) || return
      #   @sample.setViolationComment(chart, s)
      # end
    end


    # class XXViolation
    #   attr_accessor :violation, :vid, :caid

    #   def initialize(v)
    #     @violation = v
    #     @vid = v.id
    #     @caid = v.ca_id
    #   end

    #   def priority
    #     @violation.priority
    #   end

    #   def is_top_priority
    #     @violation.isTopPriority
    #   end

    #   def text
    #     @violation.text
    #   end

    #   def comment
    #     @violation.comment
    #   end
    # end


    class RawValue
      attr_accessor :raw, :value

      def initialize(raw)
        @raw = raw
        @value = @raw.value
      end

      def data_keys
        Hash[@raw.data_key_names.zip(@raw.data_keys)]
      end

      def extractor_keys
        Hash[@raw.extractor_key_names.zip(@raw.extractor_keys)]
      end
    end


    class SpcExtractorKey
      attr_accessor :ckey

      def initialize(ckey)
        @ckey = ckey
      end

      def inspect
        SpcCKey.new(@ckey.get_id, @ckey.fixed_value, @ckey.excluded_values)
      end
    end


    class MPC
      attr_accessor :mpc, :name

      def initialize(c)
        @mpc = c
        @name = @mpc.config.name
      end

      def inspect
        "#<#{self.class.name} #{@name.inspect}>"
      end

      def parameters
        @mpc.config.parameters.collect {|p| p.name}
      end

      def trigger_keys
        tks = @mpc.config.attached_trigger_keys
        lds = LDS.new(@mpc.parent.getLDS)
        exkeys = lds.extractor_keys
        ##@mpc.config.attached_trigger_keys
        ret = []
        tks.each {|tk|
          exkeys.each {|ek|
            (ret << SpcTriggerKey.new(ek.keyid, ek.name, tk.value); break) if ek.keyid == tk.id
          }
        }
        return ret
      end

      def groups
        @mpc.getAllMPCGroups
      end
    end
  end

end
