=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

    
Authors: Steffen Steidten, 2012-07-12
         Adel Darwaish, 2012-07-29

Version: 1.0.3

History:
  2014-12-12 ssteidte,  code cleanup
=end

require 'fileutils'
require 'misc'
require 'remote'
require 'xmlhash'

# ETest SPACE Testing Support
module ETSpace
  Dir['lib/space/etest/*DIS*jar'].each {|f| require f}
  
  Code = Java::WetDis2
  CodeData = Java::WetDis2Data
  CodeLimit = Java::WetDis2Limit

  DisHeader = ['###############################################################', '#',
    '#  JDisPP - Java DIS post-processor', '#    version: 2.3.21', '#    built  : 20120704_160425',
    '#', '###############################################################', ''].join("\n")

  DefaultSites = [[2,-4], [-4,-2], [-6,0], [-3,2], [-1,0], [1,-2], [6,0], [2,1], 
    [1,4], [4,2], [-2,3], [0,-1], [5,-2], [3,-1], [3,3], [-5,2], [-2,-4]]
    
  # ETest SPACE Testing Support
  class DIS2
    attr_accessor :env, :host, :tmpdir, :basedir, :indir, :data, :limit, :nc, :nctype, :ncname
    
    def initialize(env, params={})
      @env = env
      @host = RemoteHost.new("etest.#{env}")
      @indir = {}
      @tmpdir = params[:tmpdir] || File.expand_path('tmp/etspace')
      FileUtils.mkdir_p(@tmpdir)
      @basedir = params[:basedir]	  
      if @env.end_with?('f8stag')
        @basedir ||= '/customdata/espace'
        @indir[:filter] = File.join(@basedir, 'export')
        @basedir = File.join(@basedir, 'filter')
        @indir[:parser] = File.join(@basedir, 'etparser_data_1')
        @indir[:parser2] = File.join(@basedir, 'etparser_data_2')
        @indir[:parser_qa1] = File.join(@basedir, 'etparser_data_1')
        @indir[:parser_qa2] = File.join(@basedir, 'etparser_data_2')
      elsif params[:standalone]
        @basedir ||= '/T36_WET_YM/space_dev/disfilter_standalone'
        @indir[:parser] = File.join(@basedir, 'etparser_dummy')
        @indir[:filter] = File.join(@basedir, 'input')
	    else
        @basedir ||= '/T36_WET_YM/space_dev/disfilter'
        @indir[:filter] = File.join(@basedir, 'input')
        @indir[:parser] = File.join(File.dirname(@basedir), 'etparser')
        @indir[:parser2] = File.join(File.dirname(@basedir), 'etparser2')
        @indir[:parser_qa1] = File.join(File.dirname(@basedir), 'qa1')
        @indir[:parser_qa2] = File.join(File.dirname(@basedir), 'qa2')
      end
      @indir[:rejected] = File.join(@basedir, 'rejected')
      @indir[:processing] = File.join(@basedir, 'processing')
      @indir[:staging] = File.join(@basedir, 'staging')
      @indir[:archived] = File.join(@basedir, 'archived')      
      @indir[:accepted] = File.join(@basedir, 'accepted')
      @indir[:ignored] = File.join(@basedir, 'ignored')
      @indir[:published] = File.join(@basedir, 'published')
      # data and limit files
      @data = []
      @limit = []
      lot = params[:lot] || 'QA001.00'
      wafers = params[:wafers] || params[:wafer] || "#{lot}.01"
      wafers = wafers.split if wafers.kind_of?(String)
      wafers.flatten.each {|w|
        slot = w.split('.')[-1]
        @data << Data.new(params.merge(lot: lot, wafer: w, slot: slot))
        @limit << Limit.new(params)
      }
      @nctype = params[:nctype] || :nc
      @nc = (@nctype == :mnc) ? MNC.new(params) : NC.new(params)
      @ncname = params[:ncname]
      dis_file_rename(params[:ncname])
    end
    
    def inspect
      "#<#{self.class.name}, env: #{env}>"
    end
    
    # set default values for all default limits and update the reporting timestamp
    def add_defaults(params={})
      @limit.each {|l| l.add_default_limits(params)}
      @data.each {|d| d.add_default_sites(params)}
      reporting_timestamp({timestamp: (params[:timestamp] || Time.now)}.merge(params))
    end

    # set values for all default limits and update the reporting timestamp
    #
    # site_values is an array of parameters values
    def add_values(site_values, params={})
      add_fixed_values(site_values, params)
    end
    
    # all sites values are the same
    def add_fixed_values(site_values, params={})
      iwafer = params[:iwafer] || -1
      ts = params[:timestamp] || Time.now
      @limit.each {|l| l.add_default_limits(params)}
      @data.each {|d| d.add_default_sites({site_values: site_values}.merge(params))}
      # @limit[iwafer].add_default_limits(params)
      # @data[iwafer].add_default_sites({:site_values=>site_values}.merge(params))
      reporting_timestamp({timestamp: ts}.merge(params))
    end
    
    # different site values for each site
    def add_variable_values(values, params={})
      iwafer = params[:iwafer] || -1
      ts = params[:timestamp] || Time.now
      @limit.each {|l| l.add_default_limits(params)}
      @data.each {|d| d.add_variable_sites({fixed_site_values: values}.merge(params))}
      # @limit[iwafer].add_default_limits(params)     
      # @data[iwafer].add_variable_sites({:fixed_site_values=>fixed_values}.merge(params))
      reporting_timestamp({timestamp: ts}.merge(params))
    end
    
    def reporting_timestamp(params={})
      ts = params[:timestamp] || Time.now
      ts = Time.parse(ts) if ts.kind_of?(String)
      @nc.reporting_timestamp({timestamp: ts}.merge(params))
      @data.each {|d| d.reporting_timestamp(timestamp: ts)}
      nil
    end
    
    def dis_file_rename(f)
      if f.nil?
        l = @nc.nc[:Lot][0]
        f = [@nc.nc['flow'], l[:CustomerCode], l[:ProductID], l[:FabLotID], l[:ProcessDef]].join('_')
      end
      @nc.ncname = f  
      @nc.nc[:Lot].each_with_index {|nclot, i|
        nclot[:Wafer].each_with_index {|lotwafer, j|
          s = lotwafer[:WaferNC]
          @data[i+j].dis_file_name = "#{s}.data.dis"
          @limit[i+j].dis_file_name = "#{s}.limit.dis"
        }
      }
    end
    
    # report file names of nc, data and limit files
    #
    # for reporting/debugging only
    def dis_file_names
      return ([@nc] + @data + @limit).collect {|f| f.dis_file_name}
    end

    # report file names of nc, data and limit files
    #
    # for reporting/debugging only
    def nc_package_name
      return @nc.ncname if @nc != nil
      return nil if @nc == nil
    end
    
    # read nc, limit and data files from the local file system
    #
    # f is the dis file name, dir the local path
    def read(f, dir=nil)
      dir ||= @tmpdir
      f = "#{f}.#{@nctype}" unless f.end_with?('nc', 'mnc')
      fname = File.join(dir, f)
      $log.info "read data parameters from #{fname.inspect}"
      # read nc file
      @nc = NC.new
      @nc.read(fname)
      #
      @data = []
      @limit = []
      @nc.nc[:Lot].each_with_index {|nclot, i|
        nclot[:Wafer].each_with_index {|lotwafer, j|
          s = lotwafer[:WaferNC]
          d = Data.new
          d.read("#{dir}/#{s}.data.dis")
          @data << d
          l = Limit.new
          l.read("#{dir}/#{s}.limit.dis")
          @limit << l
        }
      }
      $log.info "returned limits #{pp limit.inspect}"
      return true
    end
    
    # write data and limit file to a local file system
    def write(dir=nil)
      dir ||= @tmpdir
      @data.each {|d| d.write(dir)}
      @limit.each {|l| l.write(dir)}
      @nc.write(dir)
    end    
    
    # read data and limit file from the server
    #
    # what is either :filter or :parser
    def remote_write(what, params={})
      return nil unless what
      # clean up tmp dir
      ['data.dis', 'limit.dis', 'nc', 'mnc'].each {|ext|
        fname = "f.#{ext}"
        File.delete(fname) if File.exist?(fname)
      }
      # write to temp dir (for upload)
      write(@tmpdir)
      #      
      # limit & data files for all parsers instances are sent to the default parser folder
      what_ext = [:parser, :parser2, :parser3].member?(what) ? :parser : what
      ##$log.info "etspace lib, method remote_write, used what = #{what}, what_ext = #{what_ext}"
      #
      # upload
      @data.each {|d| @host.upload!(d.filename, File.join(@indir[what_ext], 'data'))} unless params[:nodata]
      @limit.each {|l| @host.upload!(l.filename, File.join(@indir[what_ext], 'limit'))} unless params[:nolimit]
      @host.upload!(@nc.filename, @indir[what_ext]) unless params[:nonc]
      return true
    end
    
    # methods for testing DISFilter only
    #
    # Attention! A special file naming convention is used!
    def remote_cleanup(f, what)
      # remove the disfiles f on the remote server      
      if what == :all
        [:filter, :staging, :processing, :rejected, :archived, :parser, :accepted, :ignored, :published].each {|w| 
          remote_cleanup(f, w)
        }
        return
      end
      # limit & data files for all parsers instances are sent to the default parser folder
      what_ext = [:parser, :parser2, :parser3].member?(what) ? :parser : what
      
      $log.info "remote_cleanup #{f.inspect}, #{what.inspect}"
      ncdir = [:filter, :parser, :parser2, :parser3].member?(what)? '' : @nctype.to_s
      @host.delete_file(File.join(@indir[what], ncdir, "#{f}.#{@nctype}"))
      @host.delete_file(File.join(@indir[what_ext], "data", "#{f}_0.data.dis"))
      @host.delete_file(File.join(@indir[what_ext], "slim_data", "#{f}_0.data.dis"))
      @host.delete_file(File.join(@indir[what_ext], "limit", "#{f}_0.limit.dis"))
      @host.delete_file(File.join(@indir[what_ext], "slim_limit", "#{f}_0.limit.dis"))
    end

    def remote_cleanup_def(f, what)
      # remove the disfiles f on the remote server     
      $log.info "remote_cleanup_def #{f.inspect}, #{what.inspect}"
      if what == :all
        [:filter, :staging, :processing, :rejected, :archived, :parser, :accepted, :ignored, :published].each {|w| 
          remote_cleanup_def(f, w)
        }
        return
      end
      # limit & data files for all parsers instances are sent to the default parser folder
      what_ext = [:parser, :parser2, :parser3].member?(what) ? :parser : what
            
      ncdir = [:filter, :parser, :parser2, :parser3, :accepted, :ignored, :published].member?(what)? '' : @nctype.to_s
      @host.delete_file(File.join(@indir[what], ncdir, "#{f}.#{@nctype}"))
      @host.delete_file(File.join(@indir[what], ncdir, "#{f}.mnc"))
      @host.delete_file(File.join(@indir[what_ext], "data", "#{f}.data.dis"))
      @host.delete_file(File.join(@indir[what_ext], "slim_data", "#{f}.data.dis"))
      @host.delete_file(File.join(@indir[what_ext], "limit", "#{f}.limit.dis"))
      @host.delete_file(File.join(@indir[what_ext], "slim_limit", "#{f}.limit.dis"))
    end
    
    def remote_read(f, subdir, what)
      # f is the dis file name, "what" is either :filter or :parser
      return unless f and what
      #
      # limit & data files for all parsers instances are sent to the default parser folder
      what_ext = [:parser, :parser2, :parser3].member?(what) ? :parser : what
      #
      # clean up tmp dir
      ['data.dis', 'limit.dis', 'nc', 'mnc'].each {|ext|
        fname = "f.#{ext}"
        File.delete(fname) if File.exist?(fname)
      }
      # download
      $log.info "remote_read from subdir #{subdir.inspect}"
      # ncdir = [:filter, :parser, :parser2, :parser3, :accepted, :ignored, :published].member?(subdir)? '' : "nc"      
	    ncdir = ['filter', 'parser', 'parser2', 'parser3', 'accepted', 'ignored', 'published'].member?(subdir)? '' : 'nc' 
	    $log.info "ncdir = #{ncdir}. Try to download from #{subdir}"
      @host.download_file(File.join(File.dirname(@indir[what]), subdir, ncdir, "#{f}.#{@nctype}"), @tmpdir)
      @host.download_file(File.join(File.dirname(@indir[what_ext]), subdir, "data", "#{f}.data.dis"), @tmpdir)
      @host.download_file(File.join(File.dirname(@indir[what_ext]), subdir, "limit", "#{f}.limit.dis"), @tmpdir)
      #
      # check if files are in slim folders and download them if exist
      @host.download_file(File.join(File.dirname(@indir[what_ext]), subdir, "slim_data", "#{f}.data.dis"), @tmpdir)
      @host.download_file(File.join(File.dirname(@indir[what_ext]), subdir, "slim_limit", "#{f}.limit.dis"), @tmpdir)
      #
      # read
      @data[0].read("#{@tmpdir}/#{f}.data.dis")
      @limit[0].read("#{@tmpdir}/#{f}.limit.dis")
      @nc.read("#{@tmpdir}/#{f}.#{@nctype}")
      return true
    end
    
    # upload etest parameters files to the space server
    def upload_files(what, params={})
      $log.info "upload_files #{what.inspect} "
      return nil unless what
      #
      what_ext = [:parser, :parser2, :parser3].member?(what) ? :parser : what
      #
      # upload
      @host.upload!(@data.filename, File.join(@indir[what_ext], "data")) unless params[:nodata]
      @host.upload!(@limit.filename, File.join(@indir[what_ext], "limit")) unless params[:nolimit]
      @host.upload!(@nc.filename, @indir[what]) unless params[:nonc]      
    end
    
    # filter limits (optionally, if no limits are given) and filter data sites according to the limits
    #
    # this changes the data and optionally limit file!
    def filter_parameters(c=0, count=nil)
      $log.info "filter_parameters #{c.inspect}, #{count.inspect}"
      limits = @limit[0].filter_limits(c, count)
      @data.filter_results(limits)
    end
    
    def verify_parameter_limits(params={})
      # ensure all sites have values for all limits and no more information
      c = params[:criticality] || 0
      s = params[:site]
      $log.info "verify_parameter_limits"
      ret = true
      @data.each_with_index {|d, idx|
        limits = params[:limits] || @limit[idx].wet_limit_filtered(c)
        sites = d.sites
        sites = [sites[s]] if s
        sites.each_with_index {|site, i|
          $log.info "  wafer #{idx}, site #{i}"
    		  if site.wet_result.size != limits.size
      			$log.info "    size error, found #{site.wet_result.size} for #{limits.size} limits"
      			ret = false
      			next
    		  end
          limits.each_with_index {|limit, j|
            _s = site.wet_result[j].test_ident
            _l = limit.test_number.to_s
            ($log.info "    wrong ident for limit #{j}: #{_s} instead of #{_l}"; ret=false) unless _s == _l
          }        
        }
      }
      return ret
    end
    
    # return load time in seconds
    def logged_loadtime(match, params={})
      log = params[:log] || '/local/logs/etspace/igatesp/spisepp.log'
      log += '*' if params[:all_logs]
      res = @host.exec!("grep Loaded: #{log} | grep #{match} | awk '{print $1, $2, $6, $10}'")
      return res if params[:raw]
      return (res.split[-1].to_i/1000.0).round
    end
  end
end


# load the etspcae extensions
Dir["#{File.dirname(__FILE__)}/etspace/*rb"].each {|f| load(f)}
