=begin
Etest SPACE Testing Support

(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

    
Authors:   Steffen Steidten, 2012-07-12
           Adel Darwaish, 2012-07-29
Version:  1.0.2

History:
=end

require 'misc'
require 'xmlhash'

# Etest SPACE Testing Support
module ETSpace

  # Etest SPACE Testing Support
  class NC
    include XMLHash
    attr_accessor :filename, :dis_file_name, :data, :nc, :lot, :eqp, :cj

    def initialize(params={})
      @lot = (params[:lot] or "QA001.00")
      @eqp = (params[:eqp] or "QAEQP")
      @cj = (params[:cj] or "#{@eqp}-#{unique_string}")    #"#{@eqp}-#{Time.now.strftime('%Y%m%d')}-0001"
      flow = (params[:flow] or "FAB1")
      @nc = {"dept"=>"WET", "flow"=>flow, "type"=>"WAFER", :Lot=>[]}
      add_wafers(params)
      reporting_timestamp(params)
    end
    
    def inspect
      "#<#{self.class.name}, #{@dis_file_name.inspect}>"
    end
    
    def reporting_timestamp(params={})
      ts = (params[:timestamp] or Time.now)
      ts = Time.parse(ts) if ts.kind_of?(String)      
      tstart = (params[:tstart] or ts - 500)
      @nc[:Lot].each {|nclot|
        nclot['LotSTTI'] = tstart.strftime('%Y%m%d-%H%M%S')
        nclot['LotENTI'] = ts.strftime('%Y%m%d-%H%M%S')
        nclot[:Wafer].each {|lotwafer|
          lotwafer['WaferSTTI'] = tstart.strftime('%Y%m%d-%H%M%S')
          lotwafer['WaferENTI'] = ts.strftime('%Y%m%d-%H%M%S')
        }
      }
    end
    
    def add_wafer(wafer, params={})      
      ilot = (params[:ilot] or -1)
      slot = (params[:slot] or 1)
      @nc[:Lot][ilot][:Wafer] << {'WaferID'=>wafer, 'WaferSTTI'=>'', 'WaferENTI'=>'', 'WaferSTAT'=>'SUCCEED', 
        :WaferDD=>'/export/web/public/qa',
        :WaferNC=>nil,
        :AdptDecision=>'NoAdptDecision',  # STD:F
        :PolishCount=>'8',
        :Slot=>('%2.2d' % params[:slot] or '%2.2d' % wafer.split('.')[-1]),
        :TDCount=>'816',
        :VWID=>('%2.2d' % params[:slot] or '%2.2d' % wafer.split('.')[-1]),
        :Yield=>'Pass',
      }
    end
    
    def add_wafers(params={})
      # create lot section for the wafers
      nclot = {"LotID"=>@lot, "LotSTTI"=>"", "LotENTI"=>"", "LotSTAT"=>"SUCCEED", 
        :LotDD=>"/export/web/public/qa",
        :LotNC=>nil,
        :AdptDefinition=>"STD:782",
        :ConversionFlow=>"NoConversionFlow",
        :CustomerName=>"QCT",
        :CustomerCode=>"qgt",
        :CustomerProd=>"CD90-N9209-12G",
        :DeviceID=>"SHELBY2",
        :FabLotID=>@lot,
        :FoupID=>"QAQA",
        :InsertionName=>(params[:ins_name] or "FWET"),
        :LimitSetName=>(params[:set_name] or "QA_QA_FWET_142"), #"SHELBY2_LB_FWET_142",
        :LimitSetRev=>(params[:set_rev] or "142_008"),  #"142",
        :Notch=>"270",
        :Operator=>"qatester",
        :ParentLotID=>@lot,
        :ProbeCard=>"#{@eqp}_1",
        :ProberSetup=>"SHELBY21F--FILA--JAK",
        :ProcessDef=>(params[:op] or "FINA-FWET.01"),
        :ProductGroup=>(params[:pg] or "PGQA1"),
        :ProductID=>(params[:product] or "QAPRODUCT.A0"), #"SHELBY21AD.B2",
        :ProductIDV=>"NA",
        :SitePattern=>(params[:sp] or "S17A"),
        :SiViewJobID=>@cj,
        :SpecID=>"SHELBY2_LB_FWET",
        :SubLotType=>(params[:slt] or "PR"),
        :SubProduct=>"NoSubProduct",
        :TechCode=>(params[:technology] or "QA"),
        :Temperature=>"25",
        :TestCode=>"NoTestCode",
        :TestProgramName=>"SHELBY2_LB_FWET_142",
        :TestProgramRev=>"142",
        :TestSetup=>"6542",
        :TesterID=>@eqp,
        :TesterPlatform=>"Agilent",
        :TesterPoolID=>"NoTesterPoolID",
        :TesterPoolName=>"NoTesterPoolName",
        :Wafer=>[],
      }  
      nclot[:NextTestPD] = params[:next_pd] if params.has_key?(:next_pd)
      @nc[:Lot] << nclot
      wafers = (params[:wafers] or params[:wafer] or "#{@lot}.01")
      wafers = wafers.split if wafers.kind_of?(String)
      wafers.each_with_index {|w, i| add_wafer(w, {:slot=>i+1}.merge(params))}
    end
    
    def ncname=(s)
      f = "#{s}_#{@cj}"
      @dis_file_name = "#{f}.nc"
      @nc[:Lot].each_with_index {|nclot, i|
        nclot[:LotNC] = f
        nclot[:Wafer].each_with_index {|lotwafer, j|
          lotwafer[:WaferNC] = "#{f}_#{i+j}"
        }
      }
    end
    
    def to_xml
      hash_to_xml(:NC=>@nc)
    end
    
    def read(f)
      return nil unless @filename = f
      @data = nil
      open(f, 'rb') {|fh| @data = fh.read}
      @nc = xml_to_hash(data)[:NC]
      # ensure correct structure
      @nc[:Lot] = [@nc[:Lot]] unless @nc[:Lot].kind_of?(Array)
      @nc[:Lot].each {|nclot|
        nclot[:Wafer] = [nclot[:Wafer]] unless nclot[:Wafer].kind_of?(Array)
      }
      #
      @cj = @nc[:Lot][0][:SiViewJobID]
      @dis_file_name = File.split(f)[-1]
    end
    
    def write(path)
      FileUtils.mkdir_p(path)
      @filename = File.join(path, @dis_file_name)
      open(@filename, 'wb') {|fh| fh.write(to_xml)}
      return @filename
    end
    
  end

  # Etest SPACE Testing Support
  class MNC < NC

    def initialize(params={})
      @lot = (params[:lot] or "QA001.00")
      @eqp = (params[:eqp] or "QAEQP")
      @cj = (params[:cj] or "#{@eqp}-#{unique_string}")    #"#{@eqp}-#{Time.now.strftime('%Y%m%d')}-0001"
      flow = (params[:flow] or "FAB1")
      @nc = {"dept"=>"WET", "flow"=>flow, "type"=>"MASTER", :Lot=>[]}
      wafergroups = (params[:wafers] or params[:wafer] or "#{@lot}.01")
      wafergroups = wafergroups.split if wafergroups.kind_of?(String)
      wafergroups = [wafergroups] unless wafergroups[0].kind_of?(Array)
      wafergroups.each {|wafers|
        add_wafers(params.merge(:wafers=>wafers))
      }
      reporting_timestamp(params)
    end
    
    def add_wafer(wafer, params={})
      ilot = (params[:ilot] or -1)
      slot = (params[:slot] or 1)
      @nc[:Lot][ilot][:Wafer] << {"WaferID"=>wafer, "WaferDATA"=>"DISTRIBUTED", "WaferSTTI"=>"", "WaferENTI"=>"", "WaferSTAT"=>"SUCCEED", 
        :WaferDD=>"/export/web/public/qa",
        :WaferNC=>nil,
        :AdptDecision=>"NoAdptDecision",  # STD:F
        :PolishCount=>"8",
        :Slot=>("%2.2d" % params[:slot] or "%2.2d" % wafer.split('.')[-1]),
        :TDCount=>"816",
        :VWID=>("%2.2d" % params[:slot] or "%2.2d" % wafer.split('.')[-1]),
        :Yield=>"Pass",
      }
    end
    
    def add_wafers(params={})
      nclot = {"LotID"=>@lot, "LotDATA"=>"UNKNOWN", "LotSTTI"=>"", "LotENTI"=>"", "LotSTAT"=>"SUCCEED", 
        :LotDD=>"/export/web/public/qa",
        :LotNC=>nil,
        :AdptDefinition=>"STD:782",
        :ConversionFlow=>"NoConversionFlow",
        :CustomerName=>"QCT",
        :CustomerCode=>"qgt",
        :CustomerProd=>"CD90-N9209-12G",
        :DeviceID=>"SHELBY2",
        :FabLotID=>@lot,
        :FoupID=>"QAQA",
        :InsertionName=>(params[:ins_name] or "FWET"),
        :LimitSetName=>(params[:set_name] or "QA_QA_FWET_142"), #"SHELBY2_LB_FWET_142",
        :LimitSetRev=>(params[:set_rev] or "142_008"),  #"142",
        :Notch=>"270",
        :Operator=>"qatester",
        :ParentLotID=>@lot,
        :ProbeCard=>"#{@eqp}_1",
        :ProberSetup=>"SHELBY21F--FILA--JAK",
        :ProcessDef=>(params[:op] or "FINA-FWET.01"),
        :ProductGroup=>(params[:pg] or "PGQA1"),
        :ProductID=>(params[:product] or "QAPRODUCT.A0"), #"SHELBY21AD.B2",
        :ProductIDV=>"NA",
        :SitePattern=>(params[:sp] or "S17A"),
        :SiViewJobID=>@cj,
        :SpecID=>"SHELBY2_LB_FWET",
        :SubLotType=>(params[:slt] or "PR"),
        :SubProduct=>"NoSubProduct",
        :TechCode=>(params[:technology] or "QA"),
        :Temperature=>"25",
        :TestCode=>"NoTestCode",
        :TestProgramName=>"SHELBY2_LB_FWET_142",
        :TestProgramRev=>"142",
        :TestSetup=>"6542",
        :TesterID=>@eqp,
        :TesterPlatform=>"Agilent",
        :TesterPoolID=>"NoTesterPoolID",
        :TesterPoolName=>"NoTesterPoolName",
        :Wafer=>[],
      }
      nclot[:NextTestPD] = params[:next_pd] if params.has_key?(:next_pd)
      @nc[:Lot] << nclot
      wafers = (params[:wafers] or params[:wafer] or "#{@lot}.01")
      wafers = wafers.split if wafers.kind_of?(String)
      wafers.each_with_index {|w, i| add_wafer(w, {:slot=>i+1}.merge(params))}
    end
    
    def ncname=(s)
      super
      @dis_file_name = "#{s}_#{@cj}.mnc"
    end
    
  end  
end  
