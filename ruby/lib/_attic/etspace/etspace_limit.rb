=begin
Etest SPACE Testing Support

(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

    
Authors:   Steffen Steidten, 2012-07-12
           Adel Darwaish, 2012-07-29
Version:  1.0.2

History:
=end

# Etest SPACE Testing Support
module ETSpace

  # Etest SPACE Testing Support
  class CodeLimit::LimitFile
    # re-open java class
    field_accessor :disFileName
    
    def dis_file_name=(s)
      self.disFileName = s
    end
  end
  
  # Etest SPACE Testing Support
  class Limit
    attr_accessor :filename, :limitfile, :data
    
    def initialize(params={})
      @limitfile = CodeLimit::LimitFile.new
      @limitfile.dis_file_name = "#{(params[:ncname] or "qa")}.limit.dis"
      @data = {
        :dis_header=>DisHeader,
        :wet_limit_header=>CodeLimit::WetLimitHeader.new,
        :loader_option=>CodeLimit::LoaderOption.new,
        :wet_limit=>CodeLimit::WetLimit.new,
        :test_limit_properties=>CodeLimit::TestLimitProperties.new
      }
      # wet limit header
      @data[:wet_limit_header].mfg_area_name = (params[:mfg_area] or "FAB36")
      @data[:wet_limit_header].test_lim_set_name = (params[:set_name] or "QA_QA_FWET_142")
      @data[:wet_limit_header].test_lim_set_rev = (params[:set_rev] or "142_008")
      @data[:wet_limit_header].test_lim_rev_date = (params[:rev_date] or "01-01-1970 01:00:00")
    end
    
    def inspect
      "#<#{self.class.name}, #{limitfile.inspect}>"
    end
    
    def add_limit(n, name, params={})
      desc = (params[:desc] or "QA_#{name}")
      llow = (params[:llow] or params[:limit] or -1e20)
      lhigh = (params[:lhigh] or -llow)
      slow = (params[:slow] or llow)
      shigh = (params[:shigh] or lhigh)
      clow = (params[:clow] or llow)
      chigh = (params[:chigh] or lhigh)
      unit = (params[:unit] or "mV")
      scale = (params[:scale] or 0)
      prio = (params[:prio] or 0)
      trgt = (params[:trgt])
      cols = [n, name, desc, llow, lhigh, slow, shigh, clow, chigh, unit, scale, prio, trgt]
      cols.each_with_index {|c, i| cols[i] = c.to_s}
      @data[:wet_limit].add(CodeLimit::WetLimitItem.new(cols.to_java(:string)))
    end
    
    def add_default_limits(params={})
      count = (params[:count] or 10) if params[:prios] == nil
      count = (params[:count] or params[:prios].size) if params[:prios] != nil      
      #$log.info "params[:prios] = #{params[:prios].inspect}"
      (1..count).each {|i| 
        if params[:prios] != nil && params[:prios] != []
          params[:prio] = (params[:prios][i-1])
        end        
        add_limit(i, "Param_%4.4d" % i, params)
      }
    end
    
    # extract limitfile data
    def limitfile_to_data
      @data = {:dis_header=>dis_header, :wet_limit_header=>wet_limit_header, :loader_option=>loader_option,
        :wet_limit=>wet_limit, :test_limit_properties=>test_limit_properties}
    end
    
    # set limitfile data
    def data_to_limitfile
      @data.each_pair {|k,v| @limitfile.send("#{k}=", v)}
    end
    
    def read(f)
      @limitfile = Code.limit.LimitFile.new
      return nil unless @filename = f
      @limitfile.read(f)
      limitfile_to_data
      return true
    end
    
    def write(path)
      data_to_limitfile
      FileUtils.mkdir_p(path)
      @filename = File.join(path, @limitfile.dis_file_name)
      @limitfile.write(path)
      return @filename
    end

    def wet_limit_size
      @limitfile.wet_limit.size
    end
    
    # return new WetLimit instance with filtered items
    def wet_limit_filtered(c=0, count=nil)
      ret = CodeLimit::WetLimit.new
      ret.parent = @limitfile
      if c >= 0
        # filtered by min criticality, default is 0 (all items) 
        @limitfile.wet_limit.each {|e| 
          next unless e.test_disp_pct >= c
          e.parent = ret
          ret.add(e)
          break if count and ret.size >= count
        }
      else
        # filter by max criticality, -1 means all non-critical
        c = c.abs
        @limitfile.wet_limit.each {|e| 
          next unless e.test_disp_pct < c
          e.parent = ret
          ret.add(e) 
          break if count and ret.size >= count
        }
      end
      return ret
    end
    
    # filter wet limit and write it back to @limitfile
    def filter_limits(c=0, count=nil)
      $log.info "filter_limits"
      @limitfile.wet_limit = wet_limit_filtered(c, count)
    end

    def method_missing(meth, *args, &block)
      @limitfile.send(meth, *args, &block) if @limitfile
    end
    
    def respond_to?(meth)
      @limitfile.instance_methods.member?(meth.intern)
    end
  end
end
