=begin
Etest SPACE Testing Support

(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

    
Authors:  Steffen Steidten, 2012-10-08
Version:  0.1.0

History:
=end

require 'rqrsp'

# Etest SPACE Testing Support
module ETSpace
  class EI < RequestResponse::MQ
    
    def initialize(env, params={})
      super("sil_etest.#{env}", {:use_jms=>false, :correlation=>:corrid}.merge(params))
    end
    
    def status_query(params={})
      lot = (params[:lot] or "QA001.00")
      eqp = (params[:eqp] or "QAEQP")
      cj = (params[:cj] or "#{eqp}-#{unique_string}")    #"#{@eqp}-#{Time.now.strftime('%Y%m%d')}-0001"
      op = (params[:op] or "FINA-FWET.01")
      #
      xml = Builder::XmlMarkup.new(:indent=>2)
      xml.instruct!
      xml.soapenv :Envelope, "xmlns:soapenv"=>"http://schemas.xmlsoap.org/soap/envelope/",
        "xmlns:ns"=>"http://com.globalfoundries.sil.cei" do |enve|
        enve.soapenv :Body do |body|
          body.ns :statusQuery, "soapenv:encodingStyle"=>"http://schemas.xmlsoap.org/soap/encoding/" do |q|
            q.statusRequest {|rq|
              rq.controlJobID cj
              rq.equipmentID eqp
              rq.lotID lot
              rq.processDefinition op
            }
          end
        end
      end
      res = _send_receive(xml.target!, params)
      return res if params[:nosend]
      return nil unless res
      return res
    end
  end
end  
