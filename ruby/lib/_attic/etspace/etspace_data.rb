=begin
Etest SPACE Testing Support

(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

    
Authors:   Steffen Steidten, 2012-07-12
           Adel Darwaish, 2012-07-29
Version:  1.0.2

History:
=end

# Etest SPACE Testing Support
module ETSpace
  
  # Etest SPACE Testing Support
  class CodeData::DataFile
    # re-open java class
    field_accessor :disFileName
    
    def dis_file_name=(s)
      self.disFileName = s
    end
  end
  
  # Etest SPACE Testing Support
  class Data
    attr_accessor :filename, :datafile, :data, :lot, :wafer
    
    def initialize(params={})
      @datafile = CodeData::DataFile.new
      @datafile.dis_file_name = "#{(params[:ncname] or "qa")}.data.dis"
      @data = {
        :dis_header=>DisHeader,
        :wet_lot=>CodeData::WetLot.new,
        :loader_option=>CodeData::LoaderOption.new,
        :wet_equipment=>CodeData::WetEquipment.new,
        :foundry=>nil,  # optional
        :wet_wafer=>CodeData::WetWafer.new,
        :wet_lot_calc=>nil, # optional
        :coordinate_definition=>nil,  #optional
      }
      #
      @lot = params[:lot]  # or "QA001.00")
      @data[:wet_lot].lot_id = @lot
      @data[:wet_lot].mfg_area_name = (params[:mfg_area] or "FAB36")
      @data[:wet_lot].mfg_step_name = (params[:op] or "FINA-FWET.01")
      @data[:wet_lot].intl_device_version = "NA"
      @data[:wet_lot].insertion_name = (params[:wet] or "FWET")
      @data[:wet_lot].test_lim_set_name = (params[:set_name] or "QA_QA_FWET_142")
      @data[:wet_lot].test_lim_set_rev = (params[:set_rev] or "142_008")
      @data[:wet_lot].program_name = (params[:set_name] or "QA_QA_FWET_142")
      @data[:wet_lot].program_author = "QA"
      @data[:wet_lot].program_descp = "QA Test Program"
      @data[:wet_lot].program_rev = "1.0.0"
      @data[:wet_lot].program_rev_date = (params[:rev_date] or "01-01-1970 01:00:00")
      @data[:wet_lot].program_source_file = (params[:set_name] or "QA_QA_FWET_142")
      @data[:wet_lot].test_plan_name = (params[:set_name] or "QA_QA_FWET_142")
      @data[:wet_lot].test_plan_rev = (params[:set_rev] or "142_008")
      @data[:wet_lot].theta = (params[:theta] or 180)
      @data[:wet_lot].unit_type = (params[:unit_type] or "X")
      @data[:wet_lot].num_possible_tests = (params[:ntests] or 10000)
      @data[:wet_lot].num_possible_sites = (params[:sites] or 17)
      @data[:wet_lot].num_wafers_in_testrun = 1
      @data[:wet_lot].num_tiles_in_testrun = (params[:sites] or 17)
      @data[:wet_lot].operator_id = "QATester"
      @data[:wet_lot].mes_product_id = (params[:product] or "QAPRODUCT.A0")
      #
      @wafer = params[:wafer]
      @data[:wet_wafer].vendor_scribe = @wafer
      @data[:wet_wafer].sequence_num = (params[:seq] or 1)
      @data[:wet_wafer].wafer_number = (params[:slot] or 1)
      @data[:wet_wafer].inhouse_scribe = "NONE"
      @data[:wet_wafer].waf_comment = "QA Test"
      #
      eqp = (params[:eqp] or "QAEQP")
      @data[:wet_equipment].equipment_id = eqp
      @data[:wet_equipment].equipment_type = (params[:eqp_type] or "STATION")
      @data[:wet_equipment].wet_component = CodeData.WetComponent.new
      @data[:wet_equipment].wet_component.add(CodeData.WetComponentItem.new(["#{eqp}_1", "PROBECARD"].to_java(:string)))
      #
      reporting_timestamp(params)
    end
    
    def inspect
      "#<#{self.class.name}, #{datafile.inspect}>"
    end
    
    def reporting_timestamp(params={})
      ts = (params[:timestamp] or Time.now)
      ts = Time.parse(ts) if ts.kind_of?(String)
      tstart = (params[:tstart] or ts - 500)
      @data[:wet_lot].date_test_started = tstart
      @data[:wet_lot].date_test_finished = ts
      @data[:wet_wafer].test_time = ts.to_s
    end
    
    def add_site(x, y, desc, result)
      # result is an array of [test_ident, value] pairs
      ws = CodeData::WetSite.new
      ws.setXCoordinate(x)
      ws.setYCoordinate(y)
      ws.site_descp = desc
      result.collect {|e| e.join('|')}.to_java(:string) 
      ws.wet_result = CodeData::WetResult.new
      result.each {|p,v|
        ws.wet_result.add(CodeData::WetResultItem.new([p.to_s, v.to_s].to_java(:string)))
      }
      @data[:wet_wafer].add_wet_site(ws)
    end
    
    def add_default_sites(params={})
      count = (params[:count] or 10) if params[:site_values] == nil
      count = (params[:count] or params[:site_values].size) if params[:site_values] != nil
      $log.info "lib etspace_data, method: add_default_sites,  count: #{count} "
      values = (params[:site_values] or (1..count).collect {|i| i*2})
      DefaultSites.each_with_index {|c,i|      
        add_site(c[0], c[1], i+1, (1..count).collect {|j| [j, values[j-1]]} )
      }
    end        

    def add_variable_sites(params={})
      count = (params[:count] or 10)
      values = (params[:fixed_site_values] or (1..count).collect {|i| i*2})            
      DefaultSites.each_with_index {|c,i|
        if (i < params[:site_values].size)
          curr_values = params[:site_values][i]
        else
          curr_values = values
        end
        vals = (1..count).collect {|j| [j, curr_values[j-1]] }
        add_site(c[0], c[1], i+1, vals )
      }
    end        
       
    # extract datafile data
    def datafile_to_data
      @data = {:dis_header=>dis_header, :wet_lot=>wet_lot, :loader_option=>loader_option,
        :wet_equipment=>wet_equipment, :foundry=>foundry, :wet_wafer=>wet_wafer}
    end
    
    # set datafile data
    def data_to_datafile
      @data.each_pair {|k,v| @datafile.send("#{k}=", v) if v}
    end
    
    def read(f)
      @datafile = Code.data.DataFile.new
      return nil unless @filename = f
      @datafile.read(f)
      datafile_to_data
      return true
    end
    
    def write(path)
      data_to_datafile
      FileUtils.mkdir_p(path)
      @filename = File.join(path, @datafile.dis_file_name)
      @datafile.write(path)
      return @filename
    end
    
    def sites
      @datafile.wet_wafer.wet_site
    end
    
    # return new WetResult instance with site's wet result items filtered by limits
    def wet_result_filtered(site, limits)
      testnumbers = limits.collect {|l| l.test_number.to_s}
      ret = CodeData::WetResult.new
      ret.parent = site
      site.wet_result.each {|e| 
        next unless testnumbers.member?(e.test_ident)
        e.parent = ret
        ret.add(e)
      }
      return ret
    end
    
    # filter wet results of all sites and write them back to @data
    def filter_results(limits)
      $log.info "filter_results"
      sites.each_with_index {|site, i| 
        $log.info "  site #{i}"
        site.wet_result = wet_result_filtered(site, limits)
      }
    end
    
    def method_missing(meth, *args, &block)
      @datafile.send(meth, *args, &block) if @datafile
    end
    
    def respond_to?(meth)
      @datafile.instance_methods.member?(meth.intern)
    end
  end
end  
  
