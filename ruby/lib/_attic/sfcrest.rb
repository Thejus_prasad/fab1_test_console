=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author:  Ulrich Koerner, 2011-08-01

History:
  2014-07-11 dsteger,   converted to xmlhash function
  2016-08-30 sfrieske,  split setupfc.rb in separate files, use lib rqrsp_http instead of rqrsp
=end

require 'rqrsp_http'

ENV.delete('http_proxy')


module SetupFC
  class Rest < RequestResponse::HttpXML

    def initialize(env, params={})
      @env = env
      super("sfcrest.#{env}", {bodytag: ''}.merge(params))
      authorize  # times out pretty quickly
    end

    def authorize
      res = get(resource: 'auth/token', raw: true)
      ($log.warn "cannot authorize"; return) unless res
      @cookie = "SFC-AUTH-TOKEN=#{res}"
      return true
    end

    # Retrieves spec metadata, :content or :attachments
    def get_specs(params={})
      authorize || return
      resource = ['specs']
      resource << params[:spec_id] if params[:spec_id]
      resource << 'v' + params[:version] if params[:version]
      if params[:filename]
        resource += ['attachments', URI::encode(params[:filename])]
        resource << 'content' if params[:content]
      elsif params[:attachments]
        resource << 'attachments'
      elsif params[:content]
        resource << 'content'
      end
      $log.info "get_specs: #{resource.inspect}"
      res = get(resource: resource)
      return res if params[:raw]
      return res[:specificationList][:specifications] if res.has_key?(:specificationList)
      return res
    end

    def get_tasks(params={})
      authorize || return
      resource = "tasks"
      res = get(resource: 'tasks')
      return [] if res.nil? || res[:taskList].nil?
      return res[:taskList]
    end

    def get_xml_schemas(params={})
      # no need to authorize
      resource = ['xmlschemas']
      resource << params[:xsd] if params[:xsd]
      return get(resource: resource)
    end

    def get_xml_configs(params={})
      # no need to authorize
      return get(resource: 'xmlconfigs')
    end

    def get_master_config
      authorize || return
      return get(resource: 'masterconfig')
    end
  end

end
