=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.
    
Author: Steffen Steidten, 2015-03-25

History:
=end

require Dir['lib/jcifs*jar'].last

# Wrapper for JCIFS
module CIFS
  class Share
    attr_accessor :auth, :smbfile

    # url: "smb://vdrsnobak1/NoBackup/FactoryAutomation/RVA_Test_ITDC/Flows/QA/C02-QA-Load/"
    def initialize(url, params={})
      domain = params[:domain] || 'gfoundries.com'
      user = params[:user] || 'gmstest01'
      password = params[:password] || 'UGFzc3dvcmQwNQ=='.unpack('m').first
      @auth = Java::JcifsSmb::NtlmPasswordAuthentication.new(domain, user, password)
      @smbfile = Java::JcifsSmb::SmbFile.new(url, @auth)
    end

    def inspect
      "#<#{self.class.name} user: #{@auth.username} path: #{@smbfile.path}>"
    end

    # return list of files (and subdirs) or SmbFile objects if raw
    def files(filter='*', params={})
      begin
        res = @smbfile.listFiles(filter)
      rescue
        res = []
      end
      return res if params[:raw]
      res.collect {|f| f.path.split(@smbfile.path).last}.sort
    end

    # file is either a file name relative to @smbfile.path or a SmbFile object
    #
    # return file content as String
    def read(file)
      file = Java::JcifsSmb::SmbFile.new(@smbfile.path + file, @auth) if file.kind_of?(String)
      fis = file.input_stream
      barr = Java::byte[file.length].new
      fis.read(barr)
      String.from_java_bytes(barr)
    end
  end
end
