=begin
SPACE Testing Support

(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

    
Author: Daniel Steger

#backwards compatibility (Space 6)
=end


class Java::ComCamlineSpaceApi::CorrectiveActionConfig
  java_alias :name, :getName, []
  java_alias :name=, :setName, [java.lang.String]
  java_alias :department, :getDepartment, []
  java_alias :department=, :setDepartment, [java.lang.String]
  java_alias :author=, :setAuthor, [java.lang.String]
  java_alias :division, :getDivision, []
end

class Java::ComCamlineSpaceApi::TSGConfig
  java_alias :name, :getName, []
  java_alias :department, :getDepartment, []
  java_alias :division, :getDivision, []
  java_alias :file_name, :getFileName, []
end

class Java::ComCamlineSpaceApi::EventFolderConfig
  java_alias :name, :getName, []
end

class Java::ComCamlineSpaceApi::LDSFolderConfig
  java_alias :name, :getName, []
end

class Java::ComCamlineSpaceApi::SPCChannelConfig
  java_alias :name, :getName, []
  java_alias :name=, :setName, [java.lang.String]
  java_alias :division, :getDivision, []
  java_alias :division=, :setDivision, [java.lang.String]
  java_alias :owner, :getOwner, []
  java_alias :description, :getDescription, []
end

class Java::ComCamlineSpaceApi::CKCConfig
  java_alias :name, :getName, []
  java_alias :name=, :setName, [java.lang.String]  
end

class Java::ComCamlineSpaceApi::CalcParamConfig
  java_alias :name, :getName, []
  java_alias :param_name, :getParamName, []
  java_alias :formula_text, :getFormulaText, []
  java_alias :trigger, :getTrigger, []
end

