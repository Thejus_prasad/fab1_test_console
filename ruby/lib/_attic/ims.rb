=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.


Inhibit Management System
 
Author:     dsteger, 2013-10-17
Version:    0.0.1
=end

require 'rqrsp'
require 'siview'
##require 'easysiview/easysiview_inhibit'

# IMS Testing Support
module IMS
  class Client < RequestResponse::HTTP
    attr_accessor :env, :session, :user
    
    TimePattern = "%B %e, %Y %l:%M:%S %p"
    
    def initialize(env, params={})
      @env = env
      super("ims.#{env}", params)
      authenticate()
    end
    
    def authenticate()
      sv_password = (@cleartext ? @password : @password.unpack("m")[0]).nt_pw
      res = self._get_json_request(:resource=>"ims/rs/user/authenticate/#{@user}", :args=>{"pw"=>"#{sv_password}"})
      @session = "#{@user}%2C#{res["sessionID"]}" if res
    end
    
    def logout()
      self._get_json_request(:resource=>"ims/rs/user/logout/#{@user}")
    end
    
    def submit(classname, entities, params={})
      $log.info "#{__method__} #{classname.inspect}, #{entities.inspect}, #{params.inspect}"
      owner = (params[:owner] or @user)
      description = (params[:description] or "Automated QA test")
      comment = description
      priority = (params[:priority] or "Normal") # or Critical
      sublottypes = (params[sublottypes] or ["*"])
      start_date = (params[:start_date] or Time.now)
      end_date = (params[:end_date] or start_date + 360*24*3600)
      eta_date = (params[:end_date] or end_date)
      if params[:reasoncode]
        rc = params[:reasoncode]
        rc_desc = ""
      else
        rc, rc_desc = self.reasoncodes.first
      end
      #
      contexts = []
      SiView::MM::InhibitClasses[classname].each_with_index {|c,i|        
        if c == "Chamber" # multiple chambers are not supported
          contexts << { "className"=>"Equipment", "entityId"=>entities[i] }
          contexts << { "className"=>"Chamber", "entityId"=>params[:attrib] }
        else
          contexts << { "className"=>c, "entityId"=>entities[i] }
        end
      }
      request = {
        "requestID"=>"new", "ownerID"=>owner, 
        "requestDate"=>start_date.strftime(TimePattern), 
        "endDate"=>end_date.strftime(TimePattern), "etaDate"=>eta_date.strftime(TimePattern), 
        "category"=>"Equipment", 
        "priority"=>priority, "status"=>"Pending", "description"=>description, "comment"=>comment, "reasonCode"=>rc, 
        "reasonCodeDescription"=>rc_desc, 
        "inhibitContextList"=>contexts, "inhibitSubLotList"=>sublottypes}
      ##pp request
      res = self._post_json_request(request, :resource=>"ims/rs/request/submit", :cookie=>@session)
      return nil unless res
      return JSON(@response)["requestID"]
    end
    
    def get_request(rid)
      res = self._get_json_request(:resource=>"ims/rs/request/retrieve/id/#{rid}", :cookie=>@session)  
      return res[0] if res.kind_of?(Array) and res.count == 1
      $log.error " invalid reply: #{res.inspect}"
      return nil
    end
    
    def approve(rid, params={})
      $log.info "approve #{rid}, #{params.inspect}"
      _do_action(rid, "approve", "approve", params)
    end
    
    def addcomment(rid, params={})
      $log.info "addcomment #{rid}, #{params.inspect}"
      _do_action(rid, "comment", "addcomment", params)
    end
    
    def reject(rid, params={})
      $log.info "reject #{rid}, #{params.inspect}"
      _do_action(rid, "reject", "reject", params)
    end
    
    
    def _do_action(rid, action, resource, params={})
      approver = (params[:approver] or @user)
      comment = (params[:comment] or "QA #{Time.now.iso8601}")
      request = {"requestID"=>rid, "userID"=>approver, "action"=>action, 
        "comment"=>comment, 
        "date"=>Time.now.strftime(TimePattern)
      }
      return self._post_json_request(request, :resource=>"ims/rs/request/#{resource}", :cookie=>@session)
    end
    
    # context element lists
    
    def equipments()
      _get_list("equipment", "equpimentID")
    end
    
    def recipes()
      _get_list("recipe", "recipe")
    end
    
    def pds()
      _get_list("pd", "processDefinition")
    end
    
    def modulepds()
      _get_list("modulepd", "moduleProcessDefinition")
    end
    
    def products()
      _get_list("productid", "productID")
    end
    
    def routes()
      _get_list("routeid", "routeID")
    end
    
    def reticlegroups()
      _get_list("reticlegroup", "reticleGroup")
    end
    
    def reticles()
      _get_list("reticle", "reticle")
    end
    
    def sublottypes()
      _get_list("sublots", "sublot")
    end
    
    def reasoncodes()
      _get_list("reasoncodes", "code", "description")
    end
    
    def _get_list(resource, *tags)
      res = self._get_json_request(:resource=>"ims/rs/entities/#{resource}/list")    
      return res.map {|m| 
        if tags.length == 1
          m[tags[0]]
        else
          tags.collect {|tag| m[tag]}
        end
      }
    end
    
    def equipmentchamber_list(eqp)
      res = self._get_json_request(:resource=>"ims/rs/entities/equipmentchamber/equipment/#{eqp}")
      return res["chambers"].map {|m| m["chamber"]}
    end

    def about
      self._get_json_request(:resource=>"ims/rs/server/versioninfo")
    end
  end
  
  
  class Test
    attr_accessor :env, :sv, :ims
  
    def initialize(env, params={})
      @env = env
      @sv = params.fetch(:sv, SiView::MM.new(@env))
      @ims = params.fetch(:ims, Client.new(@env))
    end
    
    def cleanup(entity)
      @sv.inhibit_cancel(entity, nil, :owner=>@ims.user)
    end
        
    def stress_scenario
      cleanup(:recipe)
      recipes = @ims.recipes
      threads = 5.times.collect { |tid|
        Thread.new {
          ims = Client.new(@env)
          part = recipes.count / 5
          _recipes = recipes[tid*part, part]
          resp = {"submit"=>[], "comment"=>[], "approve"=>[], "reject"=>[]}
          data = 100.times.collect { |i|
            ok = true
            recipe = recipes.sample
            rid = ims.submit(:recipe, [recipe])
            resp["submit"] << ims.responsetime
            if rid
              ok &= ims.addcomment(rid, :comment=>"Stress test")
              resp["comment"] << ims.responsetime
              if rand >= 0.5
                ok &= ims.approve(rid)
                resp["approve"] << ims.responsetime
                ok &= verify_equal 1, @sv.inhibit_list(:recipe, recipe).count
                ok &= verify_equal "ACTIVE", ims.get_request(rid)["status"]
              else
                ok &= ims.reject(rid)
                resp["reject"] << ims.responsetime
                ok &= verify_equal "REJECTED", ims.get_request(rid)["status"]
              end
            else
              ok = false
            end
            [rid, ok]
          }
          [data, resp]
        }
      }
      all = {"submit"=>[], "comment"=>[], "approve"=>[], "reject"=>[]}
      all_ok = true
      err_ids = []
      threads.collect {|t| 
        d,resp = t.value
        all_ok &= d.inject(true) {|res,(rid,ok)| 
          err_ids << rid unless ok
          res = ok
        }
        all["submit"] += resp["submit"]
        all["comment"] += resp["comment"]
        all["approve"] += resp["approve"]
        all["reject"] += resp["reject"]
      }
      $log.info " failed requests: #{err_ids}" unless all_ok
      all.each_pair do |k,v|
        $log.info "#{k}; #{v.min}; #{v.avg}; #{v.max}"
      end
    end  
  end
end