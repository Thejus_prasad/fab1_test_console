=begin
Test E3 (Data Queries)

(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.
    

Author:     dsteger, 2013-04-25
Version:    0.1

History:
=end

require 'misc'
require 'rqrsp'

# Test E3
module E3
  class E3Error < StandardError
    def initialize(method, msg)
      super("#{method} - #{msg}")
    end
  end

  # General error handler
  def self.parse_response(method, res, params={})
    return res if res.nil? or params[:nosend] or params[:raw]    
    if res.has_key?(:Fault)
      _res = res[:Fault]
      if _res.has_key?(:faultstring)
        raise E3Error.new(method, _res[:faultstring].inspect)
      else
        raise E3Error.new(method, "error #{_res.inspect}")
      end
    end
    return res["#{method}Response".to_sym][:return]
  end
  
  DataItem = Struct.new(:runid, :name, :value, :timestamp)
  
  # Communication with EDA Application Server
  class Adapter < RequestResponse::HTTP
    attr_accessor :env, :user

    @@schema = "http://schemas.microsoft.com/clr/nsassem/EES.Core.WebServices.Adapter.Adapter/EES.Core.WebServices.Adapter"

    def initialize(env, params={})
      super("e3.#{env}", params)
      @env = env      
    end

    def _start_end_time(params)
      start_time = (params[:start] or Time.now - 30*60)
      start_time = start_time.strftime("%Y/%m/%d %H:%M:%S.%L") unless start_time.kind_of?(String)
      end_time = (params[:end] or Time.now)
      end_time = end_time.strftime("%Y/%m/%d %H:%M:%S.%L") unless end_time.kind_of?(String)
      return [start_time, end_time]
    end

    def _tool_id(tool)
      tool = self._data_query "EES_TOOL", ["NAME", "ID", "PARTITION_ID"], [["NAME", "=", tool,""]]
      raise E3Error.new(__method__, "tool not found") unless tool.has_key?(:ID) and tool.has_key?(:PARTITION_ID)
      return [tool[:ID], tool[:PARTITION_ID]]
    end
    
    def _tool_template(tool)
      tool = self._data_query "EES_TOOL", ["NAME", "ID", "PARTITION_ID", "EQUIP_TEMPL_CHMBR_ID"], [["NAME", "=", tool,""]]
      raise E3Error.new(__method__, "tool not found") unless tool.has_key?(:ID) and tool.has_key?(:EQUIP_TEMPL_CHMBR_ID)
      return [tool[:ID], tool[:PARTITION_ID], tool[:EQUIP_TEMPL_CHMBR_ID]]
    end

    def _process_type_id(process_type)
      proc_type = self._data_query "EES_PROCESS_TYPE", ["NAME", "ID"], [["NAME", "=", process_type,""]]
      raise E3Error.new(__method__, "process_type not found") unless proc_type.has_key?(:ID)
      return proc_type[:ID]
    end

    def run_context(tool, params={})
      $log.info "#{__method__} #{tool.inspect}, #{params.inspect}"
      start_time, end_time = self._start_end_time(params)

      tool_id, part_id = self._tool_id(tool)

      context = self._data_query "EES_RUN_CONTEXT", ["RUN_ID", "START_TIME", "CONTEXT_1", "CONTEXT_2", "CONTEXT_3", "CONTEXT_4", "CONTEXT_5", "CONTEXT_6", "CONTEXT_7"],
        [["TOOL_ID", "=", tool_id, "AND"],["PARTITION_ID", "=", part_id, "AND"],
         ["START_TIME", ">=", start_time, "AND"],["START_TIME", "<=", end_time, ""]
        ]
      return context
    end

    def material_context(tool, params={})
      $log.info "#{__method__} #{tool.inspect}, #{params.inspect}"
      start_time, end_time = self._start_end_time(params)

      tool_id, part_id = self._tool_id(tool)

      context = self._data_query "EES_MATERIAL_CONTEXT", ["RUN_ID", "START_TIME", "END_TIME", "CONTEXT_9", "CONTEXT_10", "CONTEXT_11", "CONTEXT_12", "CONTEXT_13", "CONTEXT_6", "CONTEXT_7", "CONTEXT_8"],
        [["TOOL_ID", "=", tool_id, "AND"],["PARTITION_ID", "=", part_id, "AND"],
         ["START_TIME", ">=", start_time, ""]
        ]
      return context
    end

    def events(tool, params={})
      $log.info "#{__method__} #{tool.inspect}, #{params.inspect}"
      start_time, end_time = self._start_end_time(params)

      tool_id, part_id = self._tool_id(tool)

      where_clause = [["TOOL_ID", "=", tool_id, "AND"],["PARTITION_ID", "=", part_id, "AND"]]
      where_clause += [["RUN_ID", "=", params[:run_id], "AND"]] if params[:run_id]
      where_clause += [["START_TIME", ">=", start_time, "AND"],["START_TIME", "<=", end_time, ""]]
      
      events = self._data_query "EES_ALARM_EVENT_DATA", ["RUN_ID", "START_TIME", "ID", "PROC_EVENT_NAME", "PROC_EVENT_TYPE", "TIME_STAMP", "ALARM_CODE", "ALARM_STATUS"],
        where_clause
      return events
    end

    def _process_type_id(eqp_chmber_id, params={})
      proc_type = self._data_query "EES_EQUIP_TEMPL_CHAMBER_DEF", ["ID", "PROC_TYPE_ID"], [["ID", "=", eqp_chmber_id]]
      raise E3Error.new(__method__, "process_type not found") unless proc_type.has_key?(:PROC_TYPE_ID)
      return proc_type[:PROC_TYPE_ID]
    end
    
    def _proc_type_var(process_type, params={})
      proc_type_id = self._process_type_id(process_type)

      vars = self._data_query "EES_PROC_TYPE_VAR", ["NAME", "DATA_TYPE", "TABLE_NAME", "COLUMN_NAME"],
        [["PROC_TYPE_ID", "=", proc_type_id]]
      return vars
    end
    
    def _tool_vars(tool, params={})
    
      tool_id, part_id, eqp_chmber_id = _tool_template(tool)      
      proc_type_id = self._process_type_id(eqp_chmber_id)

      vars = self._data_query "EES_PROC_TYPE_VAR", ["NAME", "DATA_TYPE", "TABLE_NAME", "COLUMN_NAME"],
        [["PROC_TYPE_ID", "=", proc_type_id]]
      return [tool_id, part_id, vars]
    end

    def query_vars(tool, vars, params={})
      $log.info "#{__method__} #{tool.inspect}, #{vars.inspect}, #{params.inspect}"
      start_time, end_time = self._start_end_time(params)
      tool_id, part_id, _vars = self._tool_vars(tool)

      _qvars = {}
      vars.each do |var|
        _dat = _vars.find {|v| var == v[:NAME] }
        if _dat
          _qvars[_dat[:COLUMN_NAME]] = [var,_dat[:DATA_TYPE]]
        end
      end
      data = self._data_query "EES_RUN_DATA", ["RUN_ID", "TIME_STAMP"] + _qvars.keys,
        [["TOOL_ID", "=", tool_id, "AND"],["PARTITION_ID", "=", part_id, "AND"],
         ["START_TIME", ">=", start_time, "AND"],["START_TIME", "<=", end_time, ""]
        ]
       $log.info "#{data.inspect}"
      _res = []
      data.each do |d|
        _qvars.each_pair do |col,(v,dtype)|
          scol = col.to_sym
          if d.has_key?(scol)
            value = d[scol]
            value = value.to_f if dtype == "float"
            value = value.to_i if dtype == "long"
            value = value.to_i if dtype == "int"
            _res << DataItem.new(d[:RUN_ID], v, value, Time.parse(d[:TIME_STAMP]))
          end
        end
      end
      return _res.compact
    end
    
    def _data_query(table, columns, conditions, params={})
      user = (params[:user] or @user)
      pass = (params[:password] or (@cleartext ? @password : @password.unpack("m")[0]))

      xmlq = Builder::XmlMarkup.new(:indent=>0)
      xmlq.instruct!
      xmlq.Select do |select|
        select.TableName table
        select.Columns do |cols|
          columns.each {|c| cols.Column {|col| col.Name c}}
        end
        select.WhereClause do |clauses|
          conditions.each do |(l_operand, op, r_operand, c_op)|
            clauses.Clause do |clause|
              clause.LeftBrackets {|lb| lb.Bracket "(" }
              clause.Condition do |cond|
                cond.LeftOperand l_operand
                cond.Operator op
                cond.RightOperand r_operand
              end
              clause.RightBrackets {|rb| rb.Bracket ")" }
              clause.ClauseOperator c_op
            end
          end
        end
      end

      xml = Builder::XmlMarkup.new(:indent=>2)
      xml.instruct!
      xml.env :Envelope, "xmlns:env"=>"http://schemas.xmlsoap.org/soap/envelope/" do |enve|
        enve.env :Body, "xmlns:xsi"=>"http://www.w3.org/2001/XMLSchema-instance", "xmlns:xsd"=>"http://www.w3.org/2001/XMLSchema" do |body|
          body.SendDataQuery "xmlns"=>@@schema do |dq|
            dq.domainName "GFOUNDRIES"
            dq.userName user
            dq.password pass
            dq.queryXml xmlq.target!
          end
        end
      end
      action = "SendDataQuery"
      res = _send_receive(xml.target!, {:soapaction=>"#{@@schema}\##{action}"}.merge(params))
      return xml_to_hash(E3::parse_response(action, res, params)[nil])[:NewDataSet][:Table1]
    end
    
  end
  
  class Test
    attr_accessor :env, :adapter
    
    ContextKeyMap = {
      :CONTEXT_1 => :cj,
      :CONTEXT_2 => :lot,
      :CONTEXT_3 => :recipename,
      :CONTEXT_4 => :slot,
      :CONTEXT_5 => :wafer,
      :CONTEXT_6 => :recipestep,
      :CONTEXT_7 => :recipestepno,
    }
    
    def initialize(env, params={})
      @env = env      
      @adapter = Adapter.new(env, params)
    end
    
    def verify_run(tool, run_count, events, context, start_time, params={})
      $log.info "#{__method__} #{tool.inspect}, #{run_count}, #{events.inspect}, #{context.inspect}, #{start_time.inspect}, #{params.inspect}"
      e3events = @adapter.events(tool, :start=>start_time)
      return false unless verify_condition("no events found") { e3events }
      # get runs
      e3events = [e3events] unless e3events.kind_of?(Array)
      runs = e3events.map {|event| event[:RUN_ID]}.uniq
      $log.info "found runs #{runs.inspect}"
      res = verify_equal run_count, runs.count, "no of runs does not match"
      # check events for each run
      runs.each do |runid|
        _e3 = e3events.select {|e| e[:RUN_ID] == runid}.sort{|e1,e2| e1[:TIME_STAMP] <=> e2[:TIME_STAMP] }.map {|e| e[:ID]}
        res &= verify_equal events, _e3, "events not correct for #{runid}"
      end
      # get contexts
      contexts = @adapter.run_context(tool, :start=>start_time)
      contexts = [contexts] unless contexts.kind_of?(Array)
      runs.each do |runid|
        _e3 = contexts.select {|c| c[:RUN_ID] == runid}
        res &= verify_equal 1, _e3.count, "expected one context for #{runid}"
        if res
          _e3data = _e3.first
          _e3context = {}
          ContextKeyMap.each_pair do |k,v|
            _e3context[v] = _e3data[k] if _e3data.has_key?(k)
          end
          res &= verify_hash context, _e3context, "wrong context for #{runid}"
        end
      end
      return res
    end

    def verify_data(tool, vars, data, start_time, params={})
      $log.info "#{__method__} #{tool}, #{vars.inspect}, #{data.inspect}, #{start_time.inspect}, #{params.inspect}"
      # get data
      e3data = @adapter.query_vars(tool, vars, :start=>start_time)
      runs = e3data.map {|d| d.runid}.uniq
      res = true#verify_equal data.count, runs.count, "number of runs do not match, #{runs.inspect}"
      data.each do |run_data|
        run_data.each do |time, vals|
          _e3data = e3data.select {|d| d.timestamp == time}.map {|d| d.value}
          res &= verify_equal vals.count, _e3data.count, "number of data values not equal"
          if _e3data
            rounded_vals = vals.map {|v| (v * 100).round/100.0}
            res &= verify_equal rounded_vals, _e3data, "values do not match for #{vars}"
          end
        end
      end
      return res
    end
  
  end  
end

