=begin
Etest SPACE Load Test

(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

    
Authors:  Conny Stenzel, 2013-05-02
Version:  1.0.0

History:
=end

require 'fileutils'
require 'misc'
require 'remote'
require 'etspacetest'
require 'charts'

# Etest SPACE Testing Support
module ETSpace
	
  # Create load by sending previously collected requestest  
  class Exerciser
    attr_accessor :env, :testparser, :available, :clientthreads, :perflog_filter, :perflog_parser, :rcps_received, :rcps_timeout, :payload_entries

		@@technology = "QA"
		@@pg = "PGQA1"
		@@op = "FINA-FWET.01"
		@@product1 = "SHELBY21AD.B1"  
		@@product2 = "SHELBY21AD.B2"
		@@product3 = "SHELBY21AD.B3"  
		@@lot = "U282V.00"		
		@@dummy_lot = "LOT"
		
		@@lcl = -3
		@@center = 0.0
		@@ucl = 3
		@@lsl = -5
		@@usl = 5 
  
		@@corrective_actions = ["AutoLotHold", "LotHold", "ReleaseLotHold","CancelLotHold"]
    @@refresh_time = 60
    @@wait_filter = 300
    @@wait_parser = 4800
    @@wait_router = 4800
  
    def initialize(env, params={})
      @env = env      
			nctype = (params[:nctype] or :mnc)
      if env == "f8stag"
        @@technology = "QA"
        @@pg = "QAPG1"
        @@product1 = "SILPROD.01"  
        @@product2 = "SILPROD.02"
        @@product3 = "SILPROD.03" 
        @@op = "TD-WET-OBANPK.01"
        @@lot = "8XYK16003.000"
        params = {:lds=>"Fab8_ETest",
          :folder=>"/QA/QAPG1/TD-WET-OBANPK.01/ATTR"}
      else
        params = {}
      end
      $ptest = ETSpace::TestParser.new(@env, params.merge({:nctype=>nctype}))
      @available = []
    end
    
    def inspect
      "#<#{self.class.name}, env: #{env}>"
    end
    
    def load_test(params={})
      $log.info "load_test, parameters: #{params.inspect}"
      # initialize performance log
      @perflog_filter = params[:perflog_filter]
      @perflog_parser = params[:perflog_parser]
      # @perflog_filter = "log/etspace_perf_#{Time.now.utc.iso8601.gsub(':', '-')}.csv" unless @perflog or params[:nolog]
      # @perflog_parser = "log/etspace_perf_#{Time.now.utc.iso8601.gsub(':', '-')}.csv" unless @perflog or params[:nolog]
      if @perflog_filter
        open(File.expand_path(@perflog_filter), "wb") {|fh| fh.write("Sendtime;Requestnumber;Duration(s)\n")}
        $log.info " DISFilter performance log: #{@perflog_filter}"
      end
      if @perflog_parser
        open(File.expand_path(@perflog_parser), "wb") {|fh| fh.write("Sendtime;Requestnumber;Duration(s)\n")}
        $log.info " ETParsers performance log: #{@perflog_parser}"
      end      
			#
			iterations = (params[:iterations] or 1).to_i
			speed = (params[:speed] or 1).to_f
			tmax_multiplicator = 1
			tmax_multiplicator = speed if speed and speed != 0
			$log.info "  starting test with #{iterations} iterations at #{speed}-times speed [where 0 means as fast as possible]"
			duration = params[:duration]    # max time to run
			@rcps_received = @rcps_timeout = 0
			sleep 3
			tmax = duration ? Time.now + duration : nil
			iteration = 0
			packages = (params[:packages] or 1)
      wafers = (params[:wafers] or 25)
			parameters = (params[:parameters] or 1)
			violations = (params[:violations] or 0)
			violations = 100 if violations > 100
			violations = 0 if violations < 0
			violations = violations.to_f
			recipefine = (params[:recipefine] or false)
			nctype = (params[:nctype] or :mnc)
			# starting an infinite loop if iterations = 0
			starttime = Time.now
			loop do
				iteration = iteration + 1
				timestep_noclient = 1
				# start scheduler
				$log.info "------------------------------------------------------------------------------------"
				$log.info "  starting #{iteration}. run of #{iterations} runs"
				$log.info "    max duration #{duration}s until #{tmax}" if duration
				$log.info "    going to send #{packages} packages with #{wafers} wafers each, "
				$log.info "    #{violations} violations, recipefine(#{recipefine}) packages and nctype #{nctype}"
				$log.info "------------------------------------------------------------------------------------"
				#
				# n% of packages have aql violation
				perc_aql = ((violations/100) * packages).to_i
				#
				aql_pckgs = []
				@@all_data_names = {}
				local_write = false
				#
				# delete channels to clean up
				# $ptest.delete_channels()
				#
				(perc_aql.to_i).times {|i| aql_pckgs << i*violations }
				#
				# create basic package for channels auto charting on space
				# $ptest.create_verify_data(1,false, :pg=>@@pg, :lot=>@@dummy_lot + 0.to_s, :nctype=>nctype, :product=>@@product1, :op=>@@op, 
					# :local_write=>local_write, :check_channels=>true, :lcl=>-@@lcl, :ucl=>@@ucl, :what=>:filter)
				
				# set_limit_and_customer_fields if !local_write
				# $ptest.set_corrective_actions(@@corrective_actions)
				
				load_test_run(packages, iteration, aql_pckgs, params)
				# packages.times {|package_number|
					# _load_test_run(package_number, package, iteration, params)
				# }
				break if iteration >= iterations and iterations != 0
				break if tmax and Time.now > tmax
			end
			endtime = Time.now
			$log.info "-------------------------------------------------"
			$log.info "Test completed in #{endtime - starttime} seconds."
			$log.info "-------------------------------------------------"
      ETSpace.performance_charts(@perflog_filter) unless params[:nolog]
      ETSpace.performance_charts(@perflog_parser) unless params[:nolog]
      return [@rcps_received = @rcps_timeout]
    end
      
    def load_test_run(packages, iteration, aql_pckgs, params={})
			nctype = (params[:nctype] or :mnc)
      @@wafers_per_lot = (params[:wafers] or 25)
			#
			@@all_data_names = {}
			local_write = false
			# write_report_info("test case: test13_create_n_packages_x_AQL_violations_same_product_10_params")
			#
			# create several packages
			$log.info "created aql_packgs index array: #{aql_pckgs.inspect}"
			packages.times {|j|
				site_values = []
				wafers = []
				lot = @@dummy_lot + j.to_s      
				if aql_pckgs[0] == j        
					site_values = create_aql_violation_values()
					aql_pckgs.delete_at(0)
				end      
				@@wafers_per_lot.times{|i| wafers << "#{lot}.#{i.to_s}"}
        
        _params = (@env == "f8stag") ? {:flow=>"FAB8", :mfg_area=>"FAB8"} : {}
				if site_values.size > 0
					$ptest.create_verify_data(1,false, _params.merge({:pg=>@@pg, :lot=>lot, :wafers=>wafers, :nctype=>nctype, :product=>@@product1, :op=>@@op, :site_values=>site_values, :local_write=>local_write , :what=>:filter}))
					$log.info "created package no. #{j} for lot #{lot} with AQL violation"
				else      
					$ptest.create_verify_data(1,false, _params.merge({:pg=>@@pg, :lot=>lot, :wafers=>wafers, :nctype=>nctype, :product=>@@product1, :op=>@@op, 
						:local_write=>local_write, :what=>:filter})) 
				end
        #
				nc_t = (nctype ==:nc)? "nc" : "mnc"
				$ptest.dis.dis_file_names[0].slice!(".#{nc_t}")
				@@all_data_names[lot] = [$ptest.dis.dis_file_names[0], Time.now]
			}
      # save for debugging on ruby console
      $all_data_names = @@all_data_names
			#
			$log.info "******************************************************************************"
			$log.info "*************** following data package names are created: "
			$log.info "******************************************************************************"
			@@all_data_names.each {|data| $log.info "#{data.inspect}"}     
			return if local_write
			#      
      wait_for_package_movements_all
      $log.warn "not all packages have been processed during time of #{@@wait_parser}" if @@finished.size < @@all_data_names.size
      # return
      # DISFilter processing charts time per package
      $all_data_names.each {|p|
        start_time = p[1][1]
        end_time = $pckg_time_info[$ptest.dis.indir[:published]][p[0]][1]
        used_time = (end_time - start_time)
        if @perflog_filter
          s = "#{start_time.utc.iso8601(3)};0;#{used_time}\n"
          open(File.expand_path(@perflog_filter), "a") {|fh| fh.write(s)}
        end		        
      }
      
      # ETParser processing charts time per package
      all_start_times = {}
      $pckg_time_info[$ptest.dis.indir[:parser_qa1]].each {|p|  all_start_times[p[0]] = p[1][1]}
      $pckg_time_info[$ptest.dis.indir[:parser_qa2]].each {|p|  all_start_times[p[0]] = p[1][1]}
      #
      all_end_times = {}
      $pckg_time_info["#{$ptest.dis.indir[:parser_qa1]}/out"].each {|p|   all_end_times[p[0]] = p[1][1]}
      $pckg_time_info["#{$ptest.dis.indir[:parser_qa2]}/out"].each {|p|   all_end_times[p[0]] = p[1][1]}
      
      all_start_times.each {|l|
        start_time = l[1]
        end_time = all_end_times[l[0]]
        used_time = (end_time - start_time)
        # $log.info "time in minutes used to process #{packages+1} packages is #{used_time.to_s} mins"
        if @perflog_parser
          s = "#{start_time.utc.iso8601(3)};0;#{used_time}\n"
          open(File.expand_path(@perflog_parser), "a") {|fh| fh.write(s)}
        end		                
      }            
    end
		
	# Aux help methods
    # example to call:
      # folders = [ "#{$ptest.dis.indir[:parser_qa1]}/out",
                  # "#{$ptest.dis.indir[:parser_qa2]}/out"]                        
      # wait_for_package_movements_all (:folders=>folders)  
    def wait_for_package_movements_all(params={})    
      @@finished = []
      cur_folder_info = {}
      @@pckg_time_info = {}    
      def_folders = [ $ptest.dis.indir[:accepted], 
                      $ptest.dis.indir[:published],
                      $ptest.dis.indir[:parser_qa1],
                      $ptest.dis.indir[:parser_qa2],
                      "#{$ptest.dis.indir[:parser_qa1]}/out",
                      "#{$ptest.dis.indir[:parser_qa2]}/out"]                    
      
      all_folders = (params[:folders] or def_folders)
      stop_f = (params[:stop_f] or "")
      start_time1 = Time.now      
      start_time = Time.now
      $log.info "*************************************************************************************************************"
      $log.info "**************************** wait_for_package_movements_all ..."
      # prepare general hash list
      all_folders.each {|f| @@pckg_time_info[f] = {} }
      while @@finished.size < @@all_data_names.size && Time.now < start_time1 + @@wait_parser
        #
        @@all_data_names.keys.each {|lot|     
          next if @@finished.member?(lot)        
          data = @@all_data_names[lot][0]       
          #
          all_folders.each {|f|
            data_folder = "#{f}/data"
            limit_folder = "#{f}/limit"                
            next if @@pckg_time_info[f].size == @@all_data_names.size or @@pckg_time_info[f].keys.member?(lot)          
            # ext_log = true if f.end_with?("published")
            if !!$ptest.check_files_in_folder(data, f, data_folder, limit_folder, :nctype=>"mnc")
              if (!stop_f.empty? && f == stop_f) or f.end_with?("out") or f == all_folders[-1]
                @@finished << lot  
                $log.info ".... finished lots: #{@@finished.inspect}"       
              end
              #            
              file_name = "#{f}/#{data}.mnc"
              cmd = "stat -c '%Y' /#{file_name}"
              timestamp = $ptest.dis.host.exec!(cmd)          
              @@pckg_time_info[f][lot] = [@@all_data_names[lot][1], Time.now, f, timestamp]  
              # reset start time for the next package
              start_time = Time.now
              $log.info "found pckg of lot: #{lot} in folder: #{f}"        
            end
          }
        }
      end
      $pckg_time_info = @@pckg_time_info
      $log.info "used DISRouter time per data:"
      $log.info "Lot|start time|end time|nc_folder|timestamp"
      return @@finished    
    end  
  
		def set_limit_and_customer_fields  
			$log.info "enter method set_limit_and_customer_fields. wait..."
			param_names = $ptest.get_param_names
			cs = $ptest.folder.spc_channels
			cs.each{|ch|
				next if !param_names.member?(ch.parameter)
				$log.warn "failed to set limit for channel #{ch.name}" if !ch.set_limits({"MEAN_VALUE_UCL"=>@@ucl, "MEAN_VALUE_CENTER"=>0.0, "MEAN_VALUE_LCL"=>@@lcl, "SIGMA_UCL"=>1.0, "SIGMA_CENTER"=>nil, "SIGMA_LCL"=>nil}) 
				# set customer fields
				fields = ch.customer_fields.merge("Maverick check"=>"1;1;15", "Maverick-Action"=>"email+hold", "Criticality"=> "Critical")
				$log.warn "failed to set customer field 'Maverick check' for channel #{ch.name}" if ch.set_customer_fields(fields) != nil
				$log.warn "customer field 'Maverick check' for channel #{ch.name} was not set!" if ch.customer_fields.member?("Maverick check")
			}           
			$log.info "leave method set_limit_and_customer_fields"
		end
		
		def create_aql_violation_values
			site_values = []
			values = []
			(0..16).each {|i|
				(0..9).each {|j|      
					if(j == 0)        
						(values << @@ucl + i* 0.2 ) if i.even?
						(values << @@lcl - i* 0.2 ) if i.odd?                
					else
						(values << @@ucl - i* 0.2 - j*0.2) if i.even?
						(values << @@lcl + i* 0.2 + j*0.2) if i.odd?
					end
				}
				site_values << values
				values = []      
			}
			return site_values
		end
		
		def create_new_file(text)
			file = File.open(@@filename,"w")do |f|     
					f.write(text)   
			end
			return file
		end
  end
	
  # create performance charts from a performance log file
  def ETSpace.performance_charts(fname)
    bname = fname[0...-File.extname(fname).size]
    lines = File.readlines(fname)
    return nil unless lines and lines.size > 1
    # first column: time, 3rd column: duration
    $data=data = lines[1..-1].collect {|l|
      ff = l.split(';')
      [Time.parse(ff[0]), ff[2].to_f]
    }
    # DCR response time charts
    ch = Chart::Time.new(data)
    ch.create_chart(:y=>"duration", :x=>"start time", :dots=>true, :dotsize=>6, :export=>"#{bname}_chart1.png")
    ch.create_chart(:y=>"duration", :x=>"start time", :dots=>true, :dotsize=>6, :lg=>true, :export=>"#{bname}_chart2.png")
    ch = Chart::Histogram.new(data.map {|d| d[1]}, :x=>"scala time diff", :y=>"duration")
    ch.create_chart(:title=>File.basename(bname), :export=>"#{bname}_hist.png")
  end   
end