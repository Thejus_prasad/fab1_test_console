=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.
    

Author: Steffen Steidten, 2015-04-20

History:
  2018-02-15 sfrieske, replaced select_all by select
=end

require 'dbsequel'


# Lot Grading database
module Lotgrading

  class DB 
    LotGrade = Struct.new(:lot, :CustomerLotGrade, :IntegrationLotGrade)

    attr_accessor :db

    def initialize(env, params={})
      @db = ::DB.new("lotgrading.#{env}")
    end
    
    def customer(params={})
      qargs = []
      q = 'SELECT Lot, CustomerLotGrade from CustomerLotGrade2SiView'
      q, qargs = @db._q_restriction(q, qargs, 'Lot', params[:lot])
      q += ' ORDER BY Lot'
      res = @db.select(q, *qargs)
    end

    def integration(params={})
      qargs = []
      q = 'SELECT LOT_ID, IntegrationLotGrade from IntegrationLotGrade2SiView'
      q, qargs = @db._q_restriction(q, qargs, 'LOT_ID', params[:lot])
      q += ' ORDER BY LOT_ID'
      res = @db.select(q, *qargs)
    end

    # get lots with customer and integration lot grade set
    def lot_data(lots, params={})
      qargs = [lots]
      q = "SELECT c.Lot, c.CustomerLotGrade, i.IntegrationLotGrade from CustomerLotGrade2SiView c, IntegrationLotGrade2SiView i 
        WHERE c.LOT LIKE ? AND c.Lot = i.LOT_ID ORDER BY c.Lot"
      res = @db.select(q, *qargs) || return
      return res.collect {|r| LotGrade.new(*r)}
    end
  
  end

end
