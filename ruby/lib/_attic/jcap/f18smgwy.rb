=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author:     ssteidte, 2015-01-14

Version:    1.1.1

History:
  2016-08-29 sfrieske,  use lib rqrsp_mq instead of rqrsp
  2016-09-07 sfrieske,  pass request as hash, avoiding the use of builder
=end

require 'rqrsp_mq'


module JCAP
	# Gateway to Fab SM (Specification Manager)
	class Fab18SpecGateway < RequestResponse::MQXML
		attr_accessor :env, :tags

		def initialize(env, params={})
			super("f18smgwy.#{env}", {timeout: 30}.merge(params))
			@env = env
			@tags = {
				product: ['getProductInfo', 'productId'],
				productgroup: ['getProductGroupInfo', 'productGroupId'],
				technology: ['getTechnologyInfo', 'technologyId']
			}
		end

    def object_info(classname, obj, params={})
      $log.info "object_info #{classname.inspect}, #{obj.inspect}"
      itag, otag = @tags[classname]
      $log.info "  #{itag}  #{otag}"
      h = {'soapenv:Envelope': {'xmlns:soapenv'=>'http://schemas.xmlsoap.org/soap/envelope/',
        'xmlns:urn'=>'urn:FabSpecServiceOperations',
        'soapenv:Header': nil,
        'soapenv:Body': {"urn:#{itag}": {"urn:#{otag}": obj}}
      }}
      res = send_receive(hash_to_xml(h), params) || return
      return res if @nosend
      rtag = :"#{itag}Response"
      ($log.warn res.inspect; return) unless res[rtag] && res[rtag][:return][:resultCode][nil] == 'SUCCESS'
      ret = res[rtag][:return]
      ret.delete(:resultCode)
      ret = ret[ret.keys.first]
      ret.delete('xmlns')
      ret
		end

	end

end
