=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.


Author:   Steffen Frieske, 2016-04-25

Version:  16.08

ASN ERP Message mapping: https://docs.google.com/spreadsheets/d/1j5iiDde5bgxYZmi81qPISD6TUl-S-UH4bM2iiH4Urmk/

History:
  2019-03-13 sfrieske, use XMLHash via RequestResponse::MQXML instead of builder
=end

require 'rqrsp_mq'
require 'misc'  # for unique_string
require 'siview'


module ERPAsnReceiver

  class AsnNotification
    attr_accessor :mq, :erpitem

    def initialize(env, params={})
      @mq = RequestResponse::MQXML.new("erpasnreceiver.#{env}", {timeout: 180, correlation: :msgid}.merge(params))
      @erpitem = params[:erpitem] || 'BAFFINS1MA-M01-JA0'
    end

    def get_version
      $log.info "get_version #{self.inspect}"
      h = {'soapenv:Envelope': {'xmlns:soapenv'=>'http://www.w3.org/2003/05/soap-envelope',
        'soapenv:Body': {getVersion: nil}
      }}
      ret = @mq.send_receive(h, timeout: 20) || return
      return ret[:getVersionResponse][:return]
    end

    # (old) receiveAdvanceShipmentNotification message from ERP to ERPAsnReceiver, return true on success
    def notification(lots, product, params={})
      lots = [lots] if lots.kind_of?(String)
      carriers = params[:carriers] || Array.new(lots.size, '')
      #
      $log.info "notification #{lots}, #{product.inspect}, #{params}"
      ldata = lots.each_with_index.collect {|lot, idx|
        wafers = params[:wafers] || 3.times.collect {unique_string('WW')}
        {
          'urn1:customerPoLine': params[:poline] || 37, 'urn1:customerPoNumber': '300132189',
          'urn1:erpItemId': params.has_key?(:erpitem) ? params[:erpitem] : @erpitem,
          'urn1:fosbId': carriers[idx], 'urn1:lotCancelFlag': params[:cancel] ? 'Y' : 'N',
          'urn1:lotGrade': 'A1', 'urn1:lotId': lot, 'urn1:moistureBarrierBagSealDate': Time.now.to_s.split.first + 'Z',
          'urn1:partName': product, 'urn1:qualityCode': '00', 'urn1:shipWaferQty': wafers.size,
          'urn1:subLotType': 'PR', 'urn1:supplierPartName': product.split('.').first, 'urn1:ull': unique_string,
          'urn1:vendorLotId': unique_string('NN'), 'urn1:wafer': wafers.each_with_index.collect {|w, i|
            walias = '%2.2d' % (i + 1)
            {'urn1:slotNo': walias, 'urn1:waferAlias': "#{lot.split('.').first}.#{walias}", 'urn1:waferId': w}
          }
        }
      }
      data = {
        'urn1:msgId': params[:msgid] || "QA00#{unique_string}", 'urn1:msgTime': Time.now.utc.iso8601, 
        'urn1:receiver': 'Fab8 SiView', 'urn1:sender': 'Oracle ERP', 'urn1:shipment': {
          'urn1:countryOfOrigin': 'KR', 'urn1:customerCode': params[:customer] || 'amd',
          'urn1:customerShipmentNumber': params[:shipno] || "QA00#{unique_string}", 'urn1:lot': ldata,
          'urn1:shipFromPartnerId': params[:tkfab] || 'S1', 'urn1:shipToPartnerId': 'GG'
        }
      }
      h = {'soapenv:Envelope': {'xmlns:soapenv'=>'http://www.w3.org/2003/05/soap-envelope',
        'xmlns:urn'=>'urn:AsnServiceOperations', 'xmlns:urn1'=>'urn:AsnServiceTypes',
        'soapenv:Body': {
          'urn:receiveAdvanceShipmentNotification': {'urn:advanceShipmentNotification': data}
        }
      }}
      res = @mq.send_receive(h) || return
      ret = (res[:receiveAdvanceShipmentNotificationResponse][:return][:code][nil] == 'ADVANCE_SHIPPING_NOTICE_SERVICE_OK')
      $log.warn "  error:\n#{res.inspect}" unless ret
      return ret
    end

  end

end
