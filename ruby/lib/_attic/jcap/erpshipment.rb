=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.


Author:   Steffen Steidten, 2011-04-20

Version:  21.03

History:
  2015-11-26 sfrieske, unified prepare_std and _nonstd
  2016-04-28 sfrieske, use local temporary queues instead of the remote queues, remove MDS conn
  2016-12-12 sfrieske, support BSM shipments (shipto: 'F1-BSM')
  2018-03-06 sfrieske, replaced user_parameter_delete_verify by user_paramer_delete, minor cleanup
  2018-03-12 sfrieske, removed MQXML from ERPMES, now defined in the inheriting classes
  2018-06-01 sfrieske, removed dependency on builder and xmlhash (hash_to_xml handled by rqrsp_mq)
  2019-06-20 sfrieske, minor cleanup
  2020-04-27 sfrieske, adjusted for configurable ShipToFirm deletion
  2021-02-05 sfrieske, use instance_of instead of kind_of
  2021-04-09 sfrieske, use REXML::Document
=end

require 'rexml/document'
require 'rqrsp_mq'


# Sales Order Interface from ERP to jCAP OneErpShipment, Picking and Shipping
module ERPShipment

  class PickShip
    attr_accessor :sv, :mq, :fabcode, :banks, :_docs

    def initialize(env, params={})
      @sv = params[:sv] || SiView::MM.new(env)
      @mq = RequestResponse::MQXML.new("erp_jcp_lotplcs.#{env}")
      @fabcode = env.start_with?('f8') ? 'FS8' : 'FS1'
      @banks = {
        end_std: 'OX-ENDFG',            # finished goods, std
        end_nonstd: 'OT-SHIP-NS',       # finished goods, non-std
        end_nonstd_return: 'O-OUTF36',  # NonPro bank for finished goods, non-std return
        ship: 'OY-SHIP'                 # via UDATA from @@bank_ship as "Destination Bank ID"
      }
    end

    def get_version
      $log.info 'PickShip#get_version'
      @_doc = doc = REXML::Document.new('<?xml version="1.0" encoding="UTF-8"?>')
      doc.add_element('soapenv:Envelope', {
        'xmlns:soapenv'=>'http://www.w3.org/2003/05/soap-envelope',
        'xmlns:urn'=>'urn:OneErpMesShipmentServiceOperations'
      })
      .add_element('soapenv:Body').add_element('urn:getVersion')
      ret = @mq.send_receive(doc, timeout: 20) || return
      return ret[:getVersionResponse][:return]
    end

    def prepare_picking(lot, params={})
      $log.info "prepare_picking #{lot.inspect}, #{params}"
      res = @sv.lot_cleanup(lot, {opNo: :last, endbank: @banks[:end_std], carrier_category: nil}.merge(params))
      return if res != 0
      ['ERPSalesOrder', 'ShipToFirm'].each {|p|
        @sv.user_parameter_delete('Lot', lot, p, check: true)
      }
      @sv.lot_bankin(lot) if @sv.lot_info(lot).states['Lot Inventory State'] != 'InBank'
      bank = params[:nonstd] ? @banks[:end_nonstd] : @banks[:end_std]
      return @sv.lot_bank_move(lot, bank).zero?
    end

    def prepare_picking_nonstd_return(lot, params={})
      $log.info "prepare_picking_nonstd_return #{lot.inspect}"
      res = @sv.lot_cleanup(lot, {endbank: @banks[:end_std], carrier_category: nil}.merge(params))
      return if res != 0
      return @sv.lot_nonprobankin(lot, banks[:end_nonstd_return]).zero?
    end

    # pick confirm, useful to test with different carriers
    def pick_confirm_verify(lot, params={})
      prepare_picking(lot, params) || return
      liold = @sv.lot_info(lot)
      # order and shipto are not set by this job, except for 9% carriers std shipment
      order = unique_string
      shipto = unique_string
      pick_confirm(lot, {order: order, shipto: shipto}.merge(params)) || return
      # verify
      if liold.carrier.start_with?('F', 'Z')
        shipto = params[:shipto] || 'QALocation'
        if shipto.include?('STUB')
          # special case, handled separately since v16.04, same behaviour for std and nonstd
          $log.info 'verifying bank and carrier'
          li = @sv.lot_info(lot)
          ($log.warn "wrong bank: #{li.bank.inspect}"; return) if li.bank != liold.bank
          ($log.warn "wrong carrier: #{li.carrier.inspect}"; return) unless li.carrier.start_with?('1')
          return true
        end
        expcarrier = ''
        expbank = liold.bank
      elsif liold.carrier.start_with?('9')
        expcarrier = liold.carrier
        expbank = params[:nonstd] ? liold.bank : @banks[:ship]
        unless params[:nonstd]
          ($log.warn 'wrong ShipToFirm'; return) if @sv.user_parameter('Lot', lot, name: 'ShipToFirm').value != shipto
        end
      else
        # regular case, do nothing
        expcarrier = liold.carrier
        expbank = liold.bank
      end
      $log.info 'verifying bank and carrier'
      li = @sv.lot_info(lot)
      ($log.warn "wrong bank: #{li.bank.inspect}"; return) if li.bank != expbank
      ($log.warn "wrong carrier: #{li.carrier.inspect}"; return) if li.carrier != expcarrier
      return true
    end

    # pick release and ship, useful to test with different carriers
    def pick_ship_verify(lot, params={})
      prepare_picking(lot, params) || return
      carrier = @sv.lot_info(lot).carrier
      # pick and pick update
      bank = params[:nonstd] ? @banks[:end_nonstd] : @banks[:ship]
      2.times {|i|
        $log.info "-- pick #{'update' if i > 0}"
        order = unique_string
        shipto = params[:shipto] || unique_string
        pick_release(lot, {order: order, shipto: shipto}.merge(params)) || return
        ($log.warn 'wrong bank'; return) if @sv.lot_info(lot).bank != bank
        unless params[:nonstd]  # nonstd pick message has no order
          ($log.warn "wrong ERPSalesOrder #{order}"; return) if @sv.user_parameter('Lot', lot, name: 'ERPSalesOrder').value != order
          ($log.warn 'wrong ShipToFirm'; return) if @sv.user_parameter('Lot', lot, name: 'ShipToFirm').value != shipto
        end
        ($log.warn 'lot is ONHOLD'; return) if @sv.lot_info(lot).states['Lot Hold State'] != 'NOTONHOLD'
      }
      # ship
      $log.info '-- ship'
      shipto = params[:shipto] || unique_string
      ship_complete(lot, {shipto: shipto}.merge(params)) || return
      unless params[:nonstd]
        # nonstd pick message had no order to delete
        delparams = params[:ship_params_delete]
        if delparams && delparams.include?('ShipToFirm')
          # deletion of ShipToFirm in std is subject to props configuration
          ($log.warn 'wrong ShipToFirm'; return) unless @sv.user_parameter('Lot', lot, name: 'ShipToFirm').value.empty?
        end
      end
      li = @sv.lot_info(lot)
      ($log.warn 'wrong status'; return) if li.status != 'SHIPPED'
      ($log.warn 'wrong bank'; return) if li.bank != bank
      expcarrier = carrier.start_with?(*(9..19).collect {|c| c.to_s}) ? '' : carrier
      $log.info "verifying lot carrier #{carrier.inspect} -> #{expcarrier.inspect}"
      ($log.warn 'wrong carrier'; return) if li.carrier != expcarrier
      ($log.warn 'lot is ONHOLD'; return) if li.states['Lot Hold State'] != 'NOTONHOLD'
      return true
    end

    # SOAP messages from ERP to OneERPShipment

    # must do nothing in F1 except lots in 9% carriers, return true on success
    def pick_confirm(lots, params={})
      return pick_msg(lots, 'pickLotsForShipment', params)
    end

    # return true on success
    def pick_release(lots, params={})
      return pick_msg(lots, 'pickRelease', params)
    end

    def pick_msg(lots, msgtype, params={})
      lots = [lots] if lots.instance_of?(String)
      nonstd = !!params[:nonstd]
      order = params[:order] || 'QAOrder'
      $log.info "pick_msg #{lots}, #{msgtype.inspect}, #{params}"
      @_doc = doc = REXML::Document.new('<?xml version="1.0" encoding="UTF-8"?>')
      tag1 = doc.add_element('soapenv:Envelope', {'xmlns:soapenv'=>'http://www.w3.org/2003/05/soap-envelope'})
      .add_element('soapenv:Body')
      .add_element("ns2:#{msgtype}", {'xmlns:ns2'=>'urn:OneErpMesShipmentServiceOperations'})
      .add_element('ns2:pickList', {
        'xmlns:ns1'=>'urn:OneErpMesShipmentServiceTypes',
        'xmlns:xsi'=>'http://www.w3.org/2001/XMLSchema-instance',
        'xsi:type'=>'ns1:OneErpMesPickInfo'
      })
      lots.each {|lot|
        tag1.add_element('ns1:customerNumber').text = params[:customer] || 'QACustomer'
        tag1.add_element('ns1:lotId').text = lot
        tag1.add_element('ns1:nonStandardOrder').text = nonstd
        tag1.add_element('ns1:shipToLoc').text = params[:shipto] || 'QALocation'
        tag1.add_element('ns1:transactionTime').text = Time.now.utc.iso8601
        tag1.add_element('ns1:salesOrder').text = order unless nonstd
      }
      res = @mq.send_receive(doc) || return
      ret = (res[:"#{msgtype}Response"][:return][:code][nil] == 'ONEERP_MES_SHIPMENT_SERVICE_OK')
      $log.warn "error:\n#{res.inspect}" unless ret
      return ret
    end

    # std, non-std shipment (returning/not returning) and turnkey-to-subcon, return true on success
    def ship_complete(lots, params={})
      lots = [lots] if lots.instance_of?(String)
      transstatus = params[:transstatus] || 'Shipped'  # 'Trans-Running' for TurnKey shipment to subcon
      $log.info "ship_complete #{lots}, transstatus: #{transstatus.inspect}"
      @_doc = doc = REXML::Document.new('<?xml version="1.0" encoding="UTF-8"?>')
      tag1 = doc.add_element('soapenv:Envelope', {'xmlns:soapenv'=>'http://www.w3.org/2003/05/soap-envelope'})
      .add_element('soapenv:Body')
      .add_element('ns2:shipComplete', {'xmlns:ns2'=>'urn:OneErpMesShipmentServiceOperations'})
      .add_element('ns2:shipList', {
        'xmlns:ns1'=>'urn:OneErpMesShipmentServiceTypes',
        'xmlns:xsi'=>'http://www.w3.org/2001/XMLSchema-instance',
        'xsi:type'=>'ns1:OneErpMesShipInfo'
      })
      lots.each {|lot|
        tag1.add_element('ns1:customerNumber').text = params[:customer] || 'QACustomer'
        tag1.add_element('ns1:lotId').text = lot
        tag1.add_element('ns1:nonStandardOrder').text = !!params[:nonstd]
        tag1.add_element('ns1:shipToLoc').text = params[:shipto] || 'QALocation'
        tag1.add_element('ns1:transactionTime').text = Time.now.utc.iso8601
        tag1.add_element('ns1:fab').text = @fabcode
        tag1.add_element('ns1:transStatus').text = transstatus
      }
      res = @mq.send_receive(doc) || return
      ret = (res[:shipCompleteResponse][:return][:code][nil] == 'ONEERP_MES_SHIPMENT_SERVICE_OK')
      $log.warn "error:\n#{res.inspect}" unless ret
      return ret
    end

  end


  class PrePackage
    attr_accessor :mq, :_doc

    def initialize(env)
      @mq = RequestResponse::MQXML.new("prepackage.#{env}")
    end

    def get_version
      $log.info 'PrePackage#get_version'
      @_doc = doc = REXML::Document.new('<?xml version="1.0" encoding="UTF-8"?>')
      doc.add_element('soapenv:Envelope', {
        'xmlns:soapenv'=>'http://www.w3.org/2003/05/soap-envelope',
        'xmlns:urn'=>'urn:OneErpPrepackageServiceOperations'
      })
      .add_element('soapenv:Body').add_element('urn:getVersion')
      ret = @mq.send_receive(doc, timeout: 20) || return
      return ret[:getVersionResponse][:return]
    end

    # msg sent by special EI to OneErpShipment to avoid waiting for the poll
    #   applies to std shipment only (bank UDATA), return true on success
    def trigger(lots, params={})
      lots = [lots] if lots.instance_of?(String)  # lots in the (one) carrier, crrierid unused
      $log.info "trigger #{lots}"
      @_doc = doc = REXML::Document.new('<?xml version="1.0" encoding="UTF-8"?>')
      tag1 = doc.add_element('soapenv:Envelope', {
        'xmlns:soapenv'=>'http://www.w3.org/2003/05/soap-envelope',
        'xmlns:urn'=>'urn:OneErpPrepackageServiceOperations',
        'xmlns:urn1'=>'urn:OneErpPrepackageServiceTypes',
      })
      .add_element('soapenv:Body')
      .add_element('urn:triggerSendLotDataToERP')
      tag2 = tag1.add_element('urn:lotData')
      lots.each {|lot|
        tag2.add_element('urn1:lotId').text = lot
      }
      tag1.add_element('urn:carrierId').text = params[:carrier] || 'QATestCarrier'
      res = @mq.send_receive(doc) || return
      ($log.warn res; return) if res[:Fault]
      ret = (res[:triggerSendLotDataToERPResponse][:return][:code][nil] == 'ONEERP_PREPACKAGE_SERVICE_OK')
      $log.warn "  error:\n#{res.inspect}" unless ret
      return ret
    end

  end

end
