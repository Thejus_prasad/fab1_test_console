=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

    
Authors: Steffen Steidten, 2012-07-12
         Adel Darwaish, 2012-07-26        

Version: 1.0.3

History:
  2014-12-12 ssteidte,  code cleanup
=end

require 'etspace'
require 'spaceapi'
require 'siview'

# Etest SPACE Testing Support
module ETSpace   

  # Etest SPACE Filter
  class TestFilter
    attr_accessor :env, :dis, :testdatadir, :filter_timeout

    @@file_notfound = "No such file or directory\n"
    
    def initialize(env, params={})
      @env = env
      @testdatadir = params[:dir] || 'etc/testcasedata/etspace'
      @filter_timeout = params[:filter_timeout] || 60
      @dis = DIS2.new(env, params)
    end
    
    def submit_data_verify(disfile, params={})
      res = submit_data(disfile, params) || ($log.warn "  error writing test files"; return)
      # extract (critical) parameters
      filter = params[:expect_filter] ? 1 : 0
      subdir = params[:subdir] || 'published'
      folder = @dis.indir[:filter]
      nc_folder = "#{File.dirname(folder)}/#{subdir}/nc"
      data_folder = "#{File.dirname(folder)}/#{subdir}/data"
      limit_folder = "#{File.dirname(folder)}/#{subdir}/limit"
      timeout = params[:timeout] || @filter_timeout
      $log.info "waiting #{timeout}s for the filter to process"
      wait_for(timeout: timeout) {
        check_files_in_folder(disfile, nc_folder, data_folder, limit_folder, {nctype: @dis.nctype}.merge(params))
      }
      sleep 5
      #
      # read filtered files, moved to input if correctly published
      @dis.remote_read(disfile, subdir, :filter) || ($log.warn "  error reading filtered test files"; return)
	    sleep 5
      # verify parameters have been filtered
      @dis.verify_parameter_limits(limits: @dis.limit[0].wet_limit_filtered(filter)) || ($log.warn "  verify critical parameters failed"; return)
      return true
    end
    
    # read test data from etc/testcasedata and send it to the filer input dir
    #
    # check if files exist in rejected folder
    def submit_data(disfile, params={})
      $log.info "submit_data #{disfile.inspect}, #{params.inspect}"
      if params[:cleanup]
        $log.info "remote clean up def"
        @dis.remote_cleanup_def(disfile, :all) 
        $log.info "  waiting 5s for the deletion to complete"
        sleep 5
      end
      # send data to the filter app
      res = @dis.read(disfile, @testdatadir) || ($log.warn "  error reading test files"; return false)
      res = @dis.remote_write(:filter, params) || ($log.warn "  error writing test files"; return false)
      return true
    end
    
    def file_found_in_folder?(full_filename)
      $log.info "enter method file_found_in_folder?, full_filename: #{full_filename}"      
      cmd = "ls #{full_filename}"
      result = @dis.host.exec!(cmd)
      ($log.warn "file #{full_filename} not found"; return) if result.end_with?(@@file_notfound)     
      return result
    end         
    
    def check_files_in_folder(data, nc_folder, data_folder, limit_folder, params={})
      # $log.info "check_files_in_folder"
	  # $log.info "enter method  #{this_method_name} with inputs: data = #{data}, nc_folder = #{nc_folder}, data_folder = #{data_folder}, params = #{params.inspect}"  
      nctype = params[:nctype] || 'nc'
      extlog = params[:extlog]
      #
      # nc file      
      cmd = "ls #{nc_folder}/#{data}.#{nctype}" 
      result = @dis.host.exec!(cmd)      
      ($log.warn "file #{data}.#{nctype} not found in #{nc_folder} folder" if extlog; return false) if result.end_with?(@@file_notfound)      
      #      
      if [@dis.indir[:parser], @dis.indir[:parser2], @dis.indir[:parser_qa1], @dis.indir[:parser_qa2]].member?(nc_folder)
        data_folder = "#{nc_folder}/data"
        limit_folder = "#{nc_folder}/limit"
      end      
      #
      if ["#{@dis.indir[:parser]}/out", "#{@dis.indir[:parser2]}/out", "#{@dis.indir[:parser_qa1]}/out", "#{@dis.indir[:parser_qa2]}/out"].member?(nc_folder)
        data_folder = File.join(File.dirname(nc_folder), 'data')
        limit_folder = File.join(File.dirname(nc_folder), 'limit')      
      end
      #
      # data file
      cmd = "ls #{data_folder}/#{data}*.data.dis" 
      result = @dis.host.exec!(cmd)
      ($log.warn "file #{data}*.data.dis not found in #{data_folder}/ folder" if extlog; return false) if result.end_with?(@@file_notfound)
      #
      # limit file
      cmd = "ls #{limit_folder}/#{data}*.limit.dis" 
      result = @dis.host.exec!(cmd)
      ($log.warn "file #{data}*.limit.dis not found in #{limit_folder}/ folder" if extlog; return false) if result.end_with?(@@file_notfound)      
      #
      return true
    end    
    
    def check_files_not_in_folder_old(data, folder, params={})
      $log.info "check_files_not_in_folder, data: #{data}, folder: #{folder}"
      nctype = params[:nctype] || 'nc'
      extlog = params[:extlog]
      #
      cmd = "ls #{folder}/nc/#{data}.#{nctype}"
      result = @dis.host.exec!(cmd)
      ($log.warn "file #{data}.#{nctype} found in #{folder}/nc/ folder" if extlog; return result) unless result.end_with?(@@file_notfound)
      #
      # limit file
      cmd = "ls #{folder}/limit/#{data}.limit.dis"
      result = @dis.host.exec!(cmd)
      ($log.warn "file #{data}.limit.dis found in #{folder}/limit/ folder" if extlog; return result) unless result.end_with?(@@file_notfound)
      #
      cmd = "ls #{folder}/slim_limit/#{data}.limit.dis"
      result = @dis.host.exec!(cmd)
      ($log.warn "file #{data}.limit.dis found in #{folder}/slim_limit/ folder" if extlog; return result) unless result.end_with?(@@file_notfound)
      #
      # data file
      cmd = "ls #{folder}/data/#{data}.data.dis"
      result = @dis.host.exec!(cmd)
      ($log.warn "file #{data}.data.dis found in #{folder}/data/ folder" if extlog; return result) unless result.end_with?(@@file_notfound)
      #
      cmd = "ls #{folder}/slim_data/#{data}.data.dis"
      result = @dis.host.exec!(cmd)
      ($log.warn "file #{data}.data.dis found in #{folder}/slim_data/ folder" if extlog; return result) unless result.end_with?(@@file_notfound)
      
      return nil
    end    

    def check_files_not_in_folder(data, folder, params={})
      $log.info "check_files_not_in_folder, data: #{data}, folder: #{folder}"
      nctype = params[:nctype] || 'nc'
      extlog = params[:extlog]
      #
      cmd = "ls #{folder}/#{data}.#{nctype}"
      result = @dis.host.exec!(cmd)
      ($log.warn "file #{data}.#{nctype} found in #{folder}/ folder" if extlog; return result) unless result.end_with?(@@file_notfound)
      #
      # limit file
      cmd = "ls #{folder}/limit/#{data}.limit.dis"
      result = @dis.host.exec!(cmd)
      ($log.warn "file #{data}.limit.dis found in #{folder}/limit/ folder" if extlog; return result) unless result.end_with?(@@file_notfound)
      #
      # data file
      cmd = "ls #{folder}/data/#{data}.data.dis"
      result = @dis.host.exec!(cmd)
      ($log.warn "file #{data}.data.dis found in #{folder}/data/ folder" if extlog; return result) unless result.end_with?(@@file_notfound)
      
      return nil
    end    
    
    def get_files_timestamp(data, folder, params={})
      $log.info "get_files_time, data: #{data}, folder: #{folder}"
      time_stamps = {}
      nctype = params[:nctype] || 'nc'
      extlog = params[:extlog]
      #
      # nc file
      file_name = "#{data}.#{nctype}"
      if folder == @dis.indir[:parser] || folder == @dis.indir[:input] || folder == @dis.indir[:published] || folder == @dis.indir[:ignored]
        cmd = "stat -c '%Y' #{folder}/#{file_name}"
      else
        cmd = "stat -c '%Y' #{folder}/nc/#{file_name}"
      end      
      result = @dis.host.exec!(cmd)
      ($log.warn "file #{data}.nc not found in #{folder} folder" if extlog; return nil) if result.end_with?(@@file_notfound)
      time_stamps.store(file_name, result)      
      #
      # limit file
      file_name = "#{data}.limit.dis"
      cmd = "stat -c '%Y' #{folder}/limit/#{file_name}"
      result = @dis.host.exec!(cmd)
      ($log.warn "file #{data}.limit.dis not found in #{folder} folder" if extlog; return nil) if result.end_with?(@@file_notfound)
      time_stamps.store(file_name, result)
      #
      # data file
      file_name = "#{data}.data.dis"
      cmd = "stat -c '%Y' #{folder}/data/#{file_name}"
      result = @dis.host.exec!(cmd)
      ($log.warn "file #{data}.data.dis not found in #{folder} folder" if extlog; return nil) if result.end_with?(@@file_notfound)
      time_stamps.store(file_name, result)
      #
      return nil if time_stamps.size < 3 || time_stamps.size > 3
      return time_stamps    
    end
    
  end
  
  class TestParser
    attr_accessor :env, :dis, :spc, :sv, :folder, :lds, :filter
    @@folder = "/QA/PGQA1/FINA-FWET.01"
    
    def initialize(env, params={})
      @env = env      
      @spc = SpaceAPI::Session.new("etest_#{env}", params) unless params[:nospace]
      @lds = @spc.lds((params[:lds] or "ET_Fab1"))      
      @@folder = (params[:folder] or @@folder)
      begin
        @folder =  @lds.folder(@@folder)
      rescue
        $log.error "#{@@folder} not found"
      end        
      @sv = (params[:sv] or SiView::MM.new(env, params))
      nctype = (params[:nctype] or :nc)
      @filter = ETSpace::TestFilter.new(@env, params.merge({nctype: nctype}))
      @dis = @filter.dis
      $sv = @sv
    end
    
    def check_lothold(lot, params={})
      holdcode = (params[:reason] or nil)
      return @sv.lot_hold_list(lot, :reason=>holdcode)
    end

    def check_futurehold(lot, params={})
      holdcode = (params[:reason] or nil)
      return @sv.lot_futurehold_list(lot, :reason=>holdcode)
    end
    
    def cancel_futrueholds(lot)
      return @sv.lot_futurehold_cancel(lot, nil)
    end

    def verify_lot_state(lot, opno, params={})
      $log.info "#{__method__} #{lot}, #{opno}"
      _linfo = @sv.lot_info(lot)
      res = true
      res &= verify_equal(opno, _linfo.opNo, "wrong operation")
      if params[:hold]        
        res &= verify_equal(1, @sv.lot_hold_list(lot, params).count, "hold expected")
      elsif params[:futurehold]  
        res &= verify_equal(1, @sv.lot_futurehold_list(lot, params).count, "future hold expected")
      else
        res &= verify_equal(0, check_lothold(lot).count, "no hold expected")
        res &= verify_equal(0, check_futurehold(lot).count, "no future hold expected")
      end
      return res
    end

    def verify_no_lot_note(lot, pd, txtime)
      $log.info "#{__method__} #{lot}, #{pd}, #{txtime.inspect}"
      notes = @sv.lot_notes(lot).select {|note| siview_time(note.timestamp) > txtime }
      verify_equal(0, notes.count, "expected no note, got #{notes.count}")
    end

    def verify_lot_note(lot, pd, txtime)
      $log.info "#{__method__} #{lot}, #{pd}, #{txtime.inspect}"
      notes = @sv.lot_notes(lot).select {|note| siview_time(note.timestamp) > txtime }
      res = verify_equal(1, notes.count, "Expected only one note, got #{notes.count}", true )
      res &= verify_match /WET SPC Passed/, notes[0].title
      res &= verify_match /SPC results.*#{pd}and they were successful/, notes[0].desc
      return res
    end
    
    def verify_futurehold_cancel(lot, msg, params={})
      $log.info "verify_futurehold_cancel #{lot.inspect}, #{msg.inspect}, #{params.inspect[0..50]}..."
      msg_more = params[:msg_more]
      timeout = (params[:timeout] or 0)
      tend = Time.now + timeout
      holds = nil
      loop do
        holds = @sv.lot_futurehold_list(lot, :memo=>msg)
        return true if holds.size == 0
        if msg_more
          return true if holds.collect {|h| h if h.memo.index(msg_more)}.compact.size == 0
        end
        break if Time.now > tend
        sleep 15
      end
      $log.warn "  no or wrong future holds: #{holds.pretty_inspect}"
      return false
    end
     
    def verify_inhibit(obj, count, params={})
      to = (params[:timeout] or 0)
      cl = (params[:class] or "Equipment")
      cl = [cl] unless cl.is_a?(Array)
      sid = (params[:sample_id] or nil)    
      $log.info "verify_inhibit #{obj.inspect}, #{count}, #{params.inspect}"
      t0 = Time.now
      tend = t0 + to
      inhibits=nil
      
      test_ok = false
      loop do
        inhibits = @sv.inhibit_list(cl, obj, params)
        inhibits = inhibits.find_all{|i| i.memo.include?(params[:msg])} if params[:msg]
        test_ok = (inhibits.count == count)
        inhibits.each{|i|
          test_ok &= Time.parse(i.end)>t0 # really future hold
          test_ok &= i.sublotTypes=="*"
          test_ok &= (sid.to_s == /SAMPLE_ID->([0-9]*)/.match(i.memo)[1]) if !sid.nil?        
        }
        break if test_ok
        break if Time.now > tend
        sleep 15
      end
      
      $log.warn "  wrong count (#{inhibits.size}) of inhibits: #{inhibits.pretty_inspect}" if !test_ok
      return test_ok
    end
    
    # general method to create multiple data parameter samples
    # input param with_spec_violation = true if required to provoke spec violation
    def create_verify_data(sample_count, with_spec_violation, params={})      
      what = (params[:what] or :filter)
      check_channels = (params[:check_channels] or false)
      timeout = (params[:timeout] or 300)
      local_write = (params[:local_write] or false)
      lcl = (params[:lcl] or -3)
      ucl  = (params[:ucl] or 3)
      lsl = (params[:lsl] or -5)
      usl = (params[:usl] or 5)      
      #
      (1..sample_count).each {|i|    
        values = []
        #
        @dis = ETSpace::DIS2.new($env, params)  
        # generate 10 parameter values randomly between clow & chigh
        (1..10).each {|i|
          a = [-rand(lcl), rand(ucl)]
          v = a[rand(a.size)]        
          values << v
        }
        #
        # provoke spec limit violation for the first parameter if desired.
        values[0] = params[:chigh] if with_spec_violation     
        $log.info "values = #{values.inspect}"        
        #
        # use the variable site_values if provided by the invoker instead of the default local values
        if params[:site_values]
          $log.info "call add_variable_values #{params[:site_values].inspect}"
          @dis.add_variable_values(params[:site_values], params)
        else
          $log.info "call add_values"
          @dis.add_fixed_values(values, params)
        end        
        # wheather it is required to write the data package on the server or locally.
        if local_write
          @dis.write()          
          return true
        else        
          @dis.remote_write(what)
        end
        return true if !check_channels
        #
        # verify channel creation
        $log.info "check spc channels count. this may take a while...."
        if @folder.spc_channels.size == 0
          $log.info "waiting for ETParser to create channels on space...."
          wait_for(:timeout=>timeout) {@folder.spc_channels.size > 0}
        end
        #
        # read limits data out of limit file for comparison
        param_names = get_param_names()
        return false  if param_names == nil
        #
        # check parameters names in space
        param_names.each {|p|
          # get the name of the parameter
          if !@folder.spc_channel(:parameter=>p)
            $log.warn"limit parameter #{p} not populated to space"
            return false
          end
        }    
      }  
      return true
    end     

    # delete channels and verify deletion
    def delete_channels
      $log.info "start deleting channels. wait for a while......"      
      while @folder.spc_channels.size > 0 do      
        if !@folder.delete_channels
          $log.error"error deleting channnels in folder #{@@folder}"
          return false
        end
      end              
      return true
    end

    # returns an array of the limits data records of the specified file
    def get_param_names
      data = []
      param_names = @dis.limit[0].limitfile_to_data
      if (param_names == nil || param_names.size == 0)
        $log.error "returns nil param_names"
        return nil
      end      
      limits = param_names[:wet_limit].to_a
      limits.each{|l|
        data << l.to_string.split('|')[1]
      }                
      return data
    end
    
    def set_corrective_actions(corrective_actions)
      cs = @folder.spc_channels
      cs.each{|c|              
        if c.name.start_with?("Param_") && !c.name["_Loss"]
          $log.info "handle channel: #{c.name}"
          if c.valuations.size == 0
            $log.warn "no valuations defined for channel #{c.name}"   
            return false
          end
          #
          # cleanup corrective actions -> this is done by overrite them with empty array
          if @spc.set_corrective_actions(c, []) != nil
            $log.warn"could not remove corrective action for channel #{c.name}"
            return false
          end
          #
          # verify that no corrective actions still exist
          c.valuations.each{|v|
            ca = @spc.corrective_actions(v)
            if ca.size > 0 then
              $log.warn "corrective action exist for the valuation = #{v.name} with vid = #{v.vid} of channel #{c.name}"; 
              next
            end
          }    
          #
          # set new corrective action
          if @spc.set_corrective_actions(c, corrective_actions) != nil 
            $log.warn "could not set corrective action for channel #{c.name}"
            return false
          end
          # verify the assignement of the CAs for all valuations 
          c.valuations.each{|v|
            ca = @spc.corrective_actions(v)
            if ca.size == 0
              $log.error "no corrective action was set for the valuation = #{v.name} with vid = #{v.vid} of channel #{c.name}"
              return false
            end
          }
        end
      }  
      return true
    end  
    
    def check_channel_samples (new_samples_count, data, params={})
      $log.info "check_channel_samples. expected samples count per parameter #{new_samples_count.inspect}...."
      waiting_time = params[:waiting_time] || 0     
      days = params[:days] || 1
      parameters = []
      parameters = data
      tstart = Time.now
      while parameters.size > 0 do
        parameters.each_with_index {|p,i|    
          $log.info "checking sample #{p}"
          ch = @folder.spc_channel(:parameter=>p)
          ($log.info "nil channel for parameter #{p}"; return false) if ch == nil
          parameters = parameters - [p] if ch.samples(:days=>days).size == new_samples_count[i]
        }      
        ($log.info "check_channel_samples timeout"; return false) if (waiting_time != 0 && Time.now > tstart + waiting_time) 
      end
      return true
    end    
    
    def all_params_exist?(param_names, params={})
      $log.info "wait untill all channels are created in Space...."
      waiting_time = params[:waiting_time] || 0     
      parameters = []
      parameters = param_names
      tstart = Time.now
      while parameters.size > 0 do
        param_names.each {|p|          
          ch = @folder.spc_channel(:parameter=>p)
          parameters = parameters - [p] if ch != nil                    
        }      
        ($log.info "all_param_exist timeout"; return false) if (waiting_time != 0 && Time.now > tstart + waiting_time) 
      end
      return true    
    end
    
    def extract_sample_chart_values(param_name, sample_no)
      result = {}
      ch = @folder.spc_channel(:parameter=>param_name)
      s = ch.samples[sample_no]
      # get mean value
      result[:mean] = s.statistics.mean
      #
      # get mean ucl. Other values are similar
      result[:ucl] = s.limits["MEAN_VALUE_UCL"]
      result[:lcl] = s.limits["MEAN_VALUE_LCL"]
      #
      # get USL (TARGET & LSL are similare to get)
      specs = s.specs
      {:usl=>s.specs["USL"]}.merge(result)
      result[:usl] = s.specs["USL"]
      result[:lsl] = s.specs["LSL"]       
      
      result[:data_keys] = s.data_keys
      result[:extractor_keys] = s.extractor_keys
      
      return result
    end
    
    # input parameters:
    #
    # n: total number of units
    #
    # c: maximum number of allowed defective units
    #
    # p: fraction defective units
    #
    # formel: aql = (d = 0 to c)sum( (n!/(d!.(n-d)!) )P^d.(1-p)^(n-d) )
    def calc_AQL_limit(n,c,p)
      aql = 0
      (0..c).each {|d|
        aql += (( (fakultaet(n)/(fakultaet(d) * fakultaet(n-d) ) )* p**d) * (1-p)**(n-d))
        $log.info "aql by #{d} = #{aql}"
      }
      return aql
    end
    
    def fakultaet(number)
      product = 1
      (1..number).each {|n|
        product *= n
      }
      return product
    end
    # wrapper methods for TestFilter class
    def check_files_in_folder(data, nc_folder, data_folder, limit_folder, params={})
      return @filter.check_files_in_folder(data,nc_folder, data_folder, limit_folder, params)
    end    
    
    def check_files_not_in_folder(data, folder, params={})
      return  @filter.check_files_not_in_folder(data,folder, params)
    end    
    def get_files_timestamp(data, folder, params={})    
      return @filter.get_files_timestamp(data, folder, params)
    end
    
    def verify_result_summary(folder)
      res = folder.spc_channel(:name=>"Result_Summary")
      ($log.warn("Result channel not found"); return false) unless res
      res
    end
    
    def verify_ecomment_sample(s, comment)
      test_ok = true
      if s.ecomment == nil
        $log.warn "  #{s.parameter} #{s.extractor_keys["Wafer"]}: external comment not as expected (#{comment.inspect}): #{s.ecomment.inspect}" if !comment.nil?
        test_ok &= comment.nil?
      else
        if comment == nil
          $log.warn "  #{s.parameter} #{s.extractor_keys["Wafer"]}: external comment not as expected (#{comment.inspect}): #{s.ecomment.inspect}" if !s.ecomment.nil?
          test_ok &= s.ecomment.nil?
        else
          res = s.ecomment.index(comment)
          $log.warn "  #{s.parameter} #{s.extractor_keys["Wafer"]}: external comment not as expected (#{comment.inspect}): #{s.ecomment}" if res.nil?
          test_ok &= !res.nil?
        end      
      end
      return test_ok  
    end
    
    def verify_new_samples(channels, scount, txtime, no_data_channels=nil)
      _res = channels.inject(true) {|res,ch|
        res &= verify_equal scount, ch.samples.select {|s| s.time > txtime}.count, "wrong count for #{ch.name}"
      }
      _res = no_data_channels.inject(_res) {|res,ch|
        res &= verify_equal 0, ch.samples.select {|s| s.time > txtime}.count, "wrong count for #{ch.name}"
      }
      return _res     
    end

    
    # Verify loss parameter value as expected
    def verify_parameter_loss(folder, parameters, value, nsites)
      res = true
      parameters.each do |name|
        s = folder.spc_channel(:parameter=>"#{name}_Loss").samples[-1]
        res &= verify_equal value, s.statistics.mean, "wrong loss data for #{name}"
        res &= verify_equal nsites, s.statistics.size, "wrong loss data for #{name}"
      end
      return res
    end
  end
end
