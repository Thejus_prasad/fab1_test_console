=begin

(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

    
Author: Steffen Steidten, 2012-05-09

Version: 13.10

History:
  2012-12-17 ssteidte,  separate file for AsmERPShipment job testing
  2013-11-15 ssteidte,  fixed message correlation type (msgid instead of corrid)
=end

require 'asmviewjcap'

module AsmView

  # Send LotComplete events (dedicated queues), normally sent by FabGUI or F7Turnkey to the OneErpMesLotComplete service
  class LotComplete < JCAP
    attr_accessor :erp_sleeptime
    
    def initialize(env, params={})
      super(env, {:instance=>"asmview.lotcmpl.#{env}", :use_jms=>false, :timeout=>30}.merge(params))
      @erp_sleeptime = params[:erp_sleeptime] || 1200 # job interval is 10 minutes, + ERP + lot processing
    end
    
    # send (FabGUI) lotComplete message
    def lot_complete(lots, params={})
      msgtype = params[:msgtype] || :std  # :std, :nonstd or :other
      $log.info "lot_complete #{lots.inspect}, #{params.merge(msgtype: msgtype).inspect}"
      lots = lots.split if lots.kind_of?(String)
      customers = (params[:customer] or [nil])
      customers = customers.split if customers.kind_of?(String)
      customers *= lots.count if customers.count == 1
      lottypes = (params[:lottype] or [nil])
      lottypes = lottypes.split if lottypes.kind_of?(String)
      lottypes *= lots.count if lottypes.count == 1
      partnames = (params[:partname] or [nil])
      partnames = partnames.split if partnames.kind_of?(String)
      partnames *= lots.count if partnames.count == 1
      srcfabcodes = (params[:srcfabcode] or params[:fabcode] or [nil])
      srcfabcodes = srcfabcodes.split if srcfabcodes.kind_of?(String)
      srcfabcodes *= lots.count if srcfabcodes.count == 1
      currentfabcode = params[:currentfabcode] 
      ship_label_note = ""
      dieflag = "*"
      lotgrade = ""
      qualitycode = ""
      waferdata = nil
      ship_location = params[:ship_location] || 'QASL'
      #
      xml = Builder::XmlMarkup.new(:indent=>2)
      xml.instruct!
      xml.soapenv :Envelope, "xmlns:soapenv"=>"http://www.w3.org/2003/05/soap-envelope",
        "xmlns:urn"=>"urn:OneErpMesLotCompleteServiceOperations", "xmlns:urn1"=>"urn:OneErpMesLotCompleteServiceTypes" do |enve|
        enve.soapenv :Body do |body|
          msgname = {std: 'lotCompleteStd', nonstd: 'lotCompleteNonStd', other: 'lotCompleteOther'}[msgtype]
          body.tag! "urn:#{msgname}" do |msg|
            lots.each_with_index {|lot, i|
              eli = lot_characteristics(lot, :wafers=>true)
              stageid = (eli.tkinfo.tktype == "U") ? 'UWINV' : 'PWINV'
              subconid = (params[:subconid] or SiView.tkinfo_to_erp(eli.tkinfo))
              srcfabcode = (srcfabcodes[i] or lot_fabcode(lot))
              country = params[:country] || @av.user_parameter('Lot', lot, name: 'CountryOfOrigin').value
              partname = (partnames[i] or eli.erpitem)
              customer = (customers[i] or eli.customer)
              lottype = (lottypes[i] or eli.sublottype)
              #
              msg.urn :lotData do |data|
                data.urn1 :countryOfOrigin, country if country
                data.urn1 :currentFabCode, currentfabcode if currentfabcode
                data.urn1 :custName, customer
                data.urn1 :location, ship_location
                data.urn1 :lotNumber, eli.lot
                data.urn1 :mesCarrierId, eli.carrier
                ##data.urn1 :organizationID, params[:organization] if params[:organization]   ## removed as of v13.12, ERP pending
                data.urn1 :partName, partname  #.sub('*', '')     ## * must not be removed!!
                data.urn1 :shipLabelNote, ship_label_note
                data.urn1 :srcFabCode, srcfabcode
                data.urn1 :stageId, stageid
                data.urn1 :transactionDate, Time.now.gmtime.iso8601
                # optional, not required for std message
                if waferdata
                  data.urn1 :waferData do |wtag|
                    wtag.urn1 :custWaferId, w.wafer
                    wtag.urn1 :dieQuantity, (params[:dcg] or 334)
                    wtag.urn1 :subLotId, w.alias
                    wtag.urn1 :waferVendorScribe, w.alias           
                  end
                end
                data.urn1 :wfrQuantity, eli.wafers.size
                if msgtype == :std
                  data.urn1 :customerNCSLInformation, ""
                  data.urn1 :customerNCSLPrintFlag, ""
                  data.urn1 :dieFlag, dieflag
                  data.urn1 :dieQuantity, (params[:dcg] or 334)
                  data.urn1 :lotGrade, lotgrade
                  data.urn1 :lotType, lottype
                  data.urn1 :qualityCode, qualitycode
                  data.urn1 :salesOrderTagging, eli.order
                  data.urn1 :subconId, subconid
                elsif msgtype == :nonstd
                  data.urn1 :productDescription, descr
                  data.urn1 :shipFromContact, shipfrom_contact
                  data.urn1 :shipPaidBy, shippaid
                  data.urn1 :shipToContact, shipto_contact
                  data.urn1 :shipToLocationId, shipto_location
                elsif msgtype == :other
                  data.urn1 :dieFlag, dieflag
                  data.urn1 :dieQuantity, (params[:dcg] or 334)
                  data.urn1 :lotType, eli.sublottype
                  data.urn1 :salesOrderTagging, eli.order
                  data.urn1 :subconId, subconid
                end
              end
            }
          end
        end
      end
      res = _send_receive(xml.target!, params)
      return res if params[:nosend]
      return nil unless res and res.kind_of?(Hash)
      ret = res[res.keys[0]][:return][:code][nil] == 'SUCCESS'
      $log.warn res unless ret
      return ret
    end

    def verify_lot_complete(lot, params={})
      ret = true
      # verify FabQualityCheck
      $log.info "(av) verify lot script parameter FabQualityCheck is 'Complete' (timeout 400s)"
      ok = wait_for(timeout: 400) {@av.user_parameter("Lot", lot, :name=>"FabQualityCheck").value == "Complete"}
      ($log.warn "  wrong FabQualityCheck value"; ret=false) unless ok
      # wait for ERP message to the LotComplete interface
      $log.info "  waiting up to #{@erp_sleeptime}s for the ERP system to answer"
      wait_for(timeout: @erp_sleeptime) {@av.lot_info(lot).states["Lot Finished State"] == "COMPLETED"}
      # verify lot status in AsmView
      li = @av.lot_info(lot)
      $log.info "(av) verify lot #{lot} is COMPLETED and on the end bank"
      ($log.info "  wrong lot status ONHOLD"; ret=false) if li.status == "ONHOLD"
      ($log.info "  wrong lot state #{li.states["Lot Finished State"]}"; ret=false) if li.states["Lot Finished State"] != "COMPLETED"
      ($log.info "  wrong bank #{li.bank}"; ret=false) if li.bank != @banks[:end_std]
      return ret
    end
  end
  
  
  # Picking and Shipping events (dedicated queues), normally sent by ERP or subcon
  #
  # msg format see http://myteamsgf/sites/ERPTeam/MES/Shared%20Documents/TO-BE%20Documents%20200mm_300mm/Development/Interfaces/AsmView/AsmErpShipment_SoapItf.docx
  class Shipment < JCAP
    
    def initialize(env, params={})
      super(env, {:instance=>"asmview.erpship.#{env}", :use_jms=>false}.merge(params))
    end
    
    # used in Fab1 only, causes the bank move otherwise seen after pick_confirm
    #
    # return true on success
    def pick_release(lots, params={})
      $log.info "pick_release #{lots.inspect}"
      _pick_msg(lots, "pickRelease", params)
    end
    
    # causes the bank move in all Fabs except Fab1
    #
    # return true on success
    def pick_confirm(lots, params={})
      $log.info "pick_confirm #{lots.inspect}"
      _pick_msg(lots, "pickConfirm", params)
    end
    
    # return true on success
    def _pick_msg(lots, msgtype, params={})
      lots = lots.split if lots.kind_of?(String)
      customer = (params[:customer] or "gf")
      nonstd = !!params[:nonstd]
      shipto = (params[:shipto] or "SAN DIEGO - CA - USA - ZR")
      partname = (params[:partname] or "IRISV20-M01")
      salesorder = (params[:salesorder] or "8070000369:1:2")
      ts = (params[:timestamp] or "2012-07-06 17:27:16") #Time.now - 86400)
      ts = Time.parse(ts) if ts.kind_of?(String)
      xml = Builder::XmlMarkup.new(:indent=>2)
      xml.instruct!
      xml.Envelope "xmlns:tns"=>"http://schemas.xmlsoap.org/soap/envelope/", "xmlns"=>"http://schemas.xmlsoap.org/soap/envelope/" do |enve|
        enve.tns :Body do |body|
          body.tag! msgtype, "xmlns:ns"=>"urn:AsmErpShipmentServiceOperations", "xmlns"=>"urn:AsmErpShipmentServiceOperations" do |pick|
            lots.each {|lot|
              fabcode = (params[:fabcode] or lot_fabcode(lot))
              organization = (params[:organization] or params[:fab])
              organization ||= (fabcode == "F1") ? "FS1" : "FS7"
              #
              pick.ns :pickList, "xmlns:urn1"=>"urn:AsmErpShipmentServiceTypes" do |pl|
                pl.urn1 :customerNumber, customer
                pl.urn1 :fabCode, fabcode
                pl.urn1 :fabOrgId, organization
                pl.urn1 :lotId, lot
                pl.urn1 :nonStandardOrder, nonstd
                pl.urn1 :partName, partname
                pl.urn1 :salesOrder, salesorder
                pl.urn1 :shipToLoc, shipto
                pl.urn1 :transactionTime, ts.gmtime.iso8601
              end
            }
          end
        end
      end
      res = _send_receive(xml.target!, params)
      return res if params[:nosend]
      return nil unless res and res.kind_of?(Hash)
      ret = res["#{msgtype}Response".to_sym][:return][:code][nil] == "SHIPMENT_SERVICE_OK"
      $log.warn res unless ret
      return ret
    end

    # std shipment, non-std shipment (returning/not returning), turnkey-to-subcon and final shipment
    #
    # return true on success    
    def ship_complete(lots, params={})
      lots = lots.split if lots.kind_of?(String)
      customer = (params[:customer] or "QACustomer")
      nonstd = !!params[:nonstd]
      shipto = (params[:shipto] or "QALocation")
      transstatus = (params[:transstatus] or "Trans-Running") # "Trans-Running": to subcon, "Shipped": from ERP
      transstatus = "Trans-Running" if transstatus.downcase.start_with?("trans")
      ts = (params[:timestamp] or Time.now)
      ts = Time.parse(ts) if ts.kind_of?(String)
      $log.info "ship_complete #{lots.inspect}, :transstatus=>'#{transstatus}'"
      xml = Builder::XmlMarkup.new(:indent=>2)
      xml.instruct!
      xml.soapenv :Envelope, "xmlns:soapenv"=>"http://www.w3.org/2003/05/soap-envelope" do |enve|
        enve.soapenv :Body do |body|
          body.ns2 :shipComplete, "xmlns:ns2"=>"urn:AsmErpShipmentServiceOperations" do |ship|
            lots.each {|lot|
              fabcode = (params[:fabcode] or lot_fabcode(lot))
              organization = (params[:organization] or params[:fab]) # is depreciated
              if fabcode == "F1"
                organization = "FS1"
              elsif fabcode == "F8"
                organization = "FS8"
              else
                organization = "FS7"
              end
              #
              ship.ns2 :shipList, "xmlns:ns1"=>"urn:AsmErpShipmentServiceTypes" do |sl|
                sl.ns1 :customerNumber, customer
                sl.ns1 :fabCode, fabcode
                sl.ns1 :fabOrgId, organization
                sl.ns1 :lotId, lot
                sl.ns1 :nonStandardOrder, nonstd
                # partName
                # salesOrder
                sl.ns1 :shipToLoc, shipto
                sl.ns1 :transStatus, transstatus
                sl.ns1 :transactionTime, ts.gmtime.iso8601
              end
            }
          end
        end
      end
      res = _send_receive(xml.target!, params)
      return res if params[:nosend]
      return nil unless res and res.kind_of?(Hash)
      ret = (res[:shipCompleteResponse] and (res[:shipCompleteResponse][:return][:code][nil] == "SHIPMENT_SERVICE_OK"))
      $log.warn res unless ret
      return ret
    end
  end
  
end
