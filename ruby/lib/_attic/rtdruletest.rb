=begin
Suppport RTD Rule testing

(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.
    
Author: Thomas Klose

Version: 0.1

=end

require 'siview'

# Suppport RTD Rule testing
module RTD
  
  # Suppport RTD Rule testing
  class RuleTest
    # RRT = Rtd Rule Testing
    attr_accessor :env, :sv, :lot
    
    def initialize(env, params={})
        @env = env
        @sv = params.has_key?(:sv) ? params[:sv] : SiView::MM.new(@env)
    end
    
    def get_headers(eqp)
        headers = @sv.rtd_dispatch_list(eqp).headers
        return headers
    end
    
    # dummy method
    def get_items(eqp)
        items = eqp
        return items
    end
    
    # get lots from RTD (via SiView), return array of lots sorted by rtd
    def get_lots_from_rtd(eqp, params={})
        column = (params[:column] or 2)  # coloumn for lot entries
        lotsrtd = @sv.rtd_dispatch_list(eqp)
        column = lotsrtd.headers.index(column) if column.class == String
        return false if column == nil
        return lotsrtd.rows.collect {|rows| rows[column]}
    end
    
    def get_lots_from_wn(eqp)
        # get lots from what next, return array of lots sorted by what next (usually priority)
        return @sv.what_next(eqp).collect {|wn| wn.lot}
        #get lots from whats next, sort them by available priority, delete the line above
        lots = @sv.what_next(eqp).collect {|wn| wn.lot}

        # get available prio classes
        priority_classes = lots.collect {|lot| 
            @sv.lot_info(lot).priority
        }
        
        lots_sorted = {}
        priority_classes.uniq.each {|pc|
            lots_sorted[pc] = lots.collect {|l|
                li = @sv.lot_info(l)
                li.lot if li.priority == pc
            }.compact
        }
        return lots_sorted
    end
    
    #Change Dispatch List to hash with only Line from own lot
    def change_list_to_hash(list, lot)
      row = {}
      myrow = []
      list.rows.each {|r| myrow = r if r.include?(lot)}
      #$log.info myrow
      return row if myrow.empty?
      list.headers.each {|header| row[header] = myrow[list.headers.index(header)]}
      return row
    end
    
    # rtdlist is the result from get_lots_from_rtd
    #
    # expected is a heash with expected keys and values
    #
    # identifier is the key from expected to identify the row
    def compare_a(rtdlist, expected, identifier, rule="") 
      test_ok = true
      cmp_field(rtdlist.rows.size != 0, true, "rtdlist was empty")
      result = rtdlist.rows.collect {|r| r if r[rtdlist.headers.index(identifier)] == expected[identifier]}.compact.flatten
      expected.each_pair {|k, v|
        test_ok &= cmp_field(rtdlist.headers.member?(k), true, "column #{k} doesnt exists in rule #{rule}")
        test_ok &= cmp_field(result[rtdlist.headers.index(k)], v, "column #{k} is not #{v} in rule #{rule}") if rtdlist.headers.member?(k)
      }          
      return test_ok
    end
    
    # rtdlist is the result from get_lots_from_rtd
    #
    # expected is a heash with expected keys and values
    #
    # identifier is the key from expected to identify the row
    #
    # params can be res_exists, verbose
    def eval_rtdlist(rtdlist, expected, identifier, params={}) 
      res_exists = !(params[:res_exists] == false)
      verbose = (params[:verbose] or false)
      test_ok = true
      $log.info("rtdlist is empty") if rtdlist.rows.size == 0 && verbose
      result = rtdlist.rows.collect {|r| r if r[rtdlist.headers.index(identifier)] == expected[identifier]}.compact.flatten
      expected.each_pair {|k, v|            
        test_ok &= cmp_field(rtdlist.headers.member?(k), true, "column #{k} doesnt exists", :verbose=>verbose)
        test_ok &= cmp_field(result[rtdlist.headers.index(k)], v, "column #{k} is not #{v}", :verbose=>verbose) if rtdlist.headers.member?(k)
      }        
      return test_ok == res_exists
    end
    
    def cmp_field(assertion, expected, failuretext, params={})
      verbose = (params[:verbose] or false)
      return true if assertion == expected
      $log.info("... " + failuretext) if verbose
      return false
    end

    def wait_for_qt(rule, time2wait)
      t_start = Time.now
      ok = true
      while ok
        list = @sv.rtd_dispatch_list(rule)  
        ok &= list.rows.size == 0
        ok &= Time.now - t_start < time2wait
        sleep 1
      end 
      return list
    end
    
    # ####################################################################### Setup for RTD Test ########################################################################################
    
    # Lot count to create 
    def setup_for_test(lot_count)
      # Return Value
      $result_rtd = []
      ok = true
      
      # Expected Values for Lot
      user = "X-UNITTEST"
      
      # All Equipments
      opEqps = "RTDT01 RTDT02 RTDT03 RTDT04 RTDT05 RTDT06 RTDIB01 RTDIB02 RTDDUMMY RTDA01 RTDA02 RTDA03 RTDA04 RTDA05 RTDA06 RTDA07 RTDA08".split

      # Create Lot
      cl = @sv.carrier_list(:category=>"BEOL", :status=>"AVAILABLE", :carrier =>"G%", :empty=>true)
      comment = "ERPITEM=3006AA-UA0,DPML=2.0,PLANNED_FSD=#{Time.now.strftime('%Y-%m-%d')}"
      @lot = @sv.new_lot_release(:product=>"RTDT-PRODUCT.01", :customer=>"gf", :route=>"P-RTDT-MAINPD01.01", :sublottype=>"PO", :carrier => cl.pop, :comment=>comment, :stbdelay=>1)
      if lot_count > 1
        @lot2 = @sv.new_lot_release(:product=>"RTDT-PRODUCT.02", :customer=>"gf", :route=>"P-RTDT-MAINPD02.01", :sublottype=>"PO", :carrier => cl.pop, :comment=>comment, :stbdelay=>1)
        lots = [@lot]+[@lot2]
      else
        lots = [@lot]
      end
      ok &= lot_carrier_cleanup(lots)
      ok &= eqp_cleanup(opEqps)
      ok &= stocker_cleanup()
      
      # Cancel setup if user is wrong
      setup_correct = false unless @sv.user == user and ok == true
      
      # Check Setup is correct
      if setup_correct == false
          $log.info("Setup has wrong User or wrong Setup")
      end
      return lots
    end
    
    # ####################################################################### Helpfunctions for Setup for RTD ###########################################################################
    
    def prepare_carrier(lots)
      ok = true
      [lots].each {|l|
        li = @sv.lot_info(l)
        # Delete Reservation of Carrier
        ok &= (@sv.carrier_reserve_cancel(li.carrier) == 0) unless @sv.carrier_status(li.carrier).xfer_user == ""
        
        #Set carrier back to default (Loaction M1 and Xferstat M0)
        ok &= (@sv.carrier_xfer_status_change(li.carrier, "SO", "INTER1") == 0)
        ok &= (@sv.carrier_xfer_status_change(li.carrier, "MI", "UTSTO11") == 0)
      }
      return ok
    end
    
    def delete_lots(lots)
      puts "\ndelete all lots: #{lots.inspect} (y/n)?"
      answer = STDIN.gets
      if "jy".index(answer.chomp.downcase)
        lots.each {|l| 
          lot_carrier_cleanup(l)
          @sv.delete_lot_family(l, :delete => false)
        }
      end
    end
    
    def create_lots_rtd(product, params={})
      route = (params[:route] or "P-RTDC-MAINPD01.01")
      count = (params[:count] or 1)
      prio = (params[:prio] or "4")
      nwafers = (params[:nwafers] or 25)
      category = (params[:category] or"FEOL")
      sublottype = (params[:sublottype] or"PO")
      sleeptime = 2
      comment = "ERPITEM=3006AA-UA0,DPML=2.0,PLANNED_FSD=#{Time.now.strftime('%Y-%m-%d')}"
      cl = @sv.carrier_list(:category=>category, :status=>"AVAILABLE", :carrier =>"G%", :empty=>true)
      lots = []
      count.times do
        sleep sleeptime
        lots << @sv.new_lot_release(:product=>product, :customer=>"gf", :route=>route, :sublottype=>sublottype, :carrier=>cl.pop, :priority=>prio, :nwafers=>nwafers, :comment=>comment, :stbdelay=>1)
      end
      return lots
    end
    
    # ####################################################################### Lot Cleanup for RTD Test ##################################################################################
    
    # clean up the lot: remove from banks, tools, etc., ensure it has a carrier and move it to :lotstart
    #
    # return true on success
    def lot_carrier_cleanup(lot, params={})
      $log.info "lot_cleanup #{lot.inspect}"
      ($log.warn "no lot"; return nil) unless lot
      ok = true

      # Important Values
      cstatus = "AVAILABLE"
      opNo = "1000.1000"
     
      # cleanup
      if lot.class == String
        lots = lot.split 
      else
        lots = lot
      end

      # Go trough each Lot
      lots.each {|l|
        li = @sv.lot_info(l)
        if li.status != "Waiting"
          ok &= (@sv.lot_hold_release(l, nil) == 0) if li.status == "ONHOLD"
          ok &= (@sv.lot_ship_cancel(l) == 0) if li.status == "FINISHED"
        end
        
        # Delete Lot Experminent
        ok &= @sv.lot_experiment_delete(l)
        
        # Carrier PP Delete
        ok &= (@sv.pp_force_delete(li.carrier, '') == 0)
        
        # Lot Future Hold Cancel
        ok &= (@sv.lot_futurehold_cancel(l, nil) == 0) if @sv.lot_futurehold_list(l).size > 0
        
        # Delete all Script Parameter on Lot
        @sv.user_parameter_delete("Lot", l, "QualEqpID", "STRING") if @sv.user_parameter_set_verify("Lot", l, "QualEqpID", "STRING") == true
        ok &= (@sv.user_parameter_delete_verify("Lot", l, "QualEqpID") == true)
        
        # Branch Cancel
        ok &= (@sv.lot_branch_cancel(l) == 0) unless li.route == "P-RTDT-MAINPD01.01" or li.route == "P-RTDT-MAINPD02.01" or li.route == "P-RTDF-MAINPD01.01" or li.route == "P-RTDP-MAINPD01.01"
        
        # MFG Order Change
        ok &= (@sv.lot_mfgorder_change(l, :sublottype => "PO") == 0) unless li.sublottype == "PO"

        # Change Lot priority
        ok &= (@sv.schdl_change(l, :priority =>"4") == 0) unless li.priority == "4"
        
        # Lot locate to start for Tests
        ok &= (@sv.lot_opelocate(l, opNo) == 0) unless li.opNo == opNo
        
        # Lot Merge 
        lfam = (@sv.lot_family(l) - [l])
        if lfam.size > 0
          lfam.each {|c|
            lic = @sv.lot_info(c)
            ok &= (@sv.lot_opelocate(c, opNo) == 0) unless lic.opNo == opNo
            ok &= (@sv.lot_merge(l, c) == 0)
          }
        end
        
        # Wafer Sort request if Lot is not in normal Carrier
        unless li.carrier.start_with?("G") == true
          cl = @sv.carrier_list(:category=>"BEOL", :status=>"AVAILABLE", :carrier =>"G%", :empty=>true)
          @sv.wafer_sort_req(l, cl.pop)
        end
        
        li1 = @sv.lot_info(l)
        # Prepare carrier
        ok &= (@sv.carrier_status_change(li1.carrier,cstatus) == 0) unless @sv.carrier_status(li1.carrier).status == cstatus
        ok &= (prepare_carrier(l) == true)
        
        # Cleanup EQP
        ok &= (eqp_cleanup(li1.opEqps) == true) if li1.opEqps.size > 0
      }
      return ok
    end
    
    # ###################################################################### Equipment Cleanup for RTD Test #############################################################################
    
    # clean up the Equipment
    #      
    # return true on success
    def eqp_cleanup(opEqps)
      $log.info "eqp_cleanup #{opEqps.inspect}"
      ($log.warn "no eqp_cleanup"; return nil) unless opEqps
      ok = true
      
      # Expected Values for Equipment
      estatus = "2WPR"
      pstat = "LoadReq"
      pmode = "Auto-2"
      opEqps = opEqps.split if opEqps.class == String
      
      # cleanup
      # Go trough each Equipment
      opEqps.each {|eqp|
        # Get Equipment Info
        eq = @sv.eqp_info(eqp)
        
        # Delete all Inhibits
        inhibits = @sv.inhibit_list("Equipment")
        inhibits.each {|x| x.entities.each {|ent| ok &= (@sv.inhibit_cancel("Equipment",eqp) == 0) if ent.objectid == eqp}}
         
        # Processing n Tool cancel
        ok &= ($sv.eqp_opestart_cancel(eqp, eq.cjs[0]) == 0) unless eq.cjs == []
        ok &= ($sv.eqp_opestart_cancel(eqp, eq.cjs[1]) == 0) unless eq.cjs == []
        
        # SLR Cancel
        eq.ports.each {|port|
          unless port.rsv_carrier == ""
            li = @sv.lot_info(@sv.carrier_status(port.rsv_carrier).lots)
            ok &= (@sv.slr_cancel(eqp, li[0].cj) == 0) unless li[0].cj == ""
          end
        }
        
        # Unload Tool
        if eq.ib
          eq.ib.loaded_carriers.each {|c| ok &= (@sv.eqp_unload(eqp, eq.ports[0].port, c) == 0)}
        else
          eq.ports.each {|port| ok &= (@sv.eqp_unload(eqp,port.port,port.loaded_carrier) == 0) unless port.loaded_carrier == ""}
        end
        
        # Set Equipment Mode for Tool
        ok &= (@sv.eqp_mode_change(eqp, pmode, :notifyTCS=>false) == 0) unless eq.ports[0].mode == pmode
        
        # Set Port to the right Configuration (NPW Reserve Cance, Portstatus Change)
        eq.ports.each {|port|
          ok &= (@sv.npw_reserve_cancel(eqp, :carrier => port.rsv_carrier) == 0) unless port.rsv_carrier == ""
          ok &= (@sv.eqp_port_status_change(eqp, port.port, pstat)==0) unless port.state == pstat
        }
        
        # Set Equipment Status for all chamber
        eq.chambers.each {|ch| ok &= @sv.chamber_status_change_req(eq.eqp, ch.chamber, estatus) unless ch.status.split(".")[-1] == estatus}

        # Set Equipment Status for Tool
        ok &= (@sv.eqp_status_change_req(eq.eqp, estatus))== 0 unless eq.status.split(".")[-1] == estatus
      }
      return ok
    end
    # ####################################################################### Stocker Cleanup for RTD Test ##############################################################################
    
    # clean up the Stocker
    #
    # return true on success
    def stocker_cleanup()
      $log.info "stocker_cleanup"
      
      # Expected Values for Stocker
      stocker = "UTSTO11 UTSTO12 A-VETHAND A-QTSTOP".split
      status = "2WPR"
      statusdown = "2NDP"
      ok = true
      
      # cleanup
      # Go trough each Equipment
      stocker.each {|s|
        if s != "A-VETHAND"
          ok &= @sv.eqp_status_change_req(s, status)
        else
          ok &= @sv.eqp_status_change_req(s, statusdown)
        end
      }
      return ok
    end
  end
end

=begin
    lots.sort_by {|c| c["carrier"]}  # sort array of structs 
    lots.sort_by {|c| c[:carrier]}   # sort array of hashes
    a.to_hash   # struct to hash
=end