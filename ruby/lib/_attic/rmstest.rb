=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.
  
Author:  dsteger, 2012-08-27

Version: 1.2.1

History:
  2015-11-04 sfrieske, added RMS::Server
  2019-01-23 sfrieske, use new DerdackEvents
=end

require 'armor'
require 'derdack'
require 'ril'
require 'rmsapi'


class RmsTest
  attr_accessor :env, :ril, :rildb, :rms, :armor, :department, :notifications

  @@rtd_configured_params = %w(Species Energy Dose ProbeCardType CoatOnly ThicknessValue
    MaxProdWfrCnt MonitorLotWfrCnt UsedBoatSize ProcessGroup ModuleCombination
    TimeTuningAfterDryClean TubeThkAfterDryClean
	  TestPlanName ProberRecipeFileName
	  ResidueType ClearResidue
	  MinAvailableChambers
	  BreakInType CarrierExchangeRequired
	  WeeklyMonitorLotWfrCnt
	  APCRecipeContext
    QAParamInteger QAParamBool QAParamTime QASubParam1 QASubParam2 QASubParam3)
  #@@rtd_configured_groups = ['ModuleID ConditioningActive ConditioningType ConditioningMaxIdleTime', 'ModuleID ConditioningActive ConditioningType ConditioningMinWafersPerJob']
  @@rtd_configured_groups = ['ConditioningType', 'ConditioningType ConditioningMinWafersPerJob']

  def initialize(env, eqp, department, params={})
    @env = env
    begin
      @ril = RMS::RIL.new(env, eqp, params)
      @rms = RmsAPI::SessionMQ.new(env)
      @rildb = RMS::RtdParameterDB.new(env)
      @armor = Armor::EI.new(env, eqp, params)
      # @derdack = Derdack::EventDB.new(env)
      @notifications = DerdackEvents.new(env)
      @department = department
    rescue => e
      $log.warn "not all components initialized, #{e.inspect}"
      puts e.backtrace
    end
  end

  @@test_recipe = { 
    'Pump' => RmsAPI::RobObject.new( nil, {}, { 'DCPPath' => '/path/to/pump/dcp' }, nil),
    'Spinner' => RmsAPI::RobObject.new( nil, {}, { 'DCPPath' => '/path/to/spinner/dcp' }, [{'RecipeType'=>'Pump'}]),
    #'Module' => RmsAPI::RobObject.new( nil, {}, { '__purpose__OxygenFlow_StepA_PM1' => 'Tuning', 'OxygenFlow_StepA_PM1' => '27', 'DCPPath' => '/path/to/module1/dcp' }, nil),
    'System' => RmsAPI::RobObject.new( nil, {}, { 'DCPPath' => '/path/to/system/dcp' }, nil),
    'WaferFlow' => RmsAPI::RobObject.new( nil, { 'RecipeNameSpaceID'=>'QA-TEST', 'RecipeName'=>'LITHO RECIPE' },
      {}, [{'RecipeType'=>'Spinner'}, {'RecipeType'=>'System'}]),
  }

  # Create a new RMS recipe from a given recipetype => ROBDEF hash
  def recipe_create(rcp_hash, params={})
    $log.info "#{__method__} #{rcp_hash.inspect}, #{params.inspect}"
    depart = (params[:department] || @department)
    eqp_prefix = (params['ToolPrefix'] || params[:tool_prefix] || 'BST')
    softrev = (params['SoftRev'] || params[:softrev] || 'QA')
    vendor = (params['ToolVendor'] || params[:tool_vendor] || 'LAM')
    if params[:target_name] == 'EITest'
      registration = 'RCP_DEF_EI_TEST'
      params[:target_type] = 'EITest'
    else # default
      registration = 'RCP_DEF_PROD'
    end
        
    robs = []
    time = Time.now.to_i
    
    rcp_hash.each_pair do |recipe_name, rob_tmpl|
      recipe_type = rob_tmpl.context['RecipeType']
      ($log.warn 'no recipe type given found'; return nil) unless recipe_type

      robdef = @rms.find_object_list( {'Department'=>depart, 'ToolPrefix'=>eqp_prefix, 'ToolVendor'=>vendor,
          'RecipeType'=>recipe_type, 'SoftRev'=>softrev}, :registration=>registration, :with_keys=>true)
      ($log.warn 'no ROBDEF found'; return nil) unless robdef.count > 0
      $log.info "Found #{robdef[0].rmshandle}"

      # modify recipe context
      rob_tmpl.context['RecipeName'] = (rob_tmpl.context['RecipeName'] || '-')
      rob_tmpl.context['RecipeNameSpaceID'] = (rob_tmpl.context['RecipeNameSpaceID'] || '-')
      rob_tmpl.context['RecipePurpose'] = (rob_tmpl.context['RecipePurpose'] || 'Production')
      rob_tmpl.context['SoftRev'] = softrev
      
      if rob_tmpl.linked_contexts
        rob_tmpl.linked_contexts.collect do |linked_context|
          linked_context['SoftRev'] = softrev
          linked_context['ToolPrefix'] = eqp_prefix unless linked_context.key?('ToolPrefix') # per convention always set
          linked_context['ToolVendor'] = vendor unless linked_context.key?('ToolVendor') # per convention always set
        end
      end
      if rob_tmpl.shared_params
        rob_tmpl.shared_params.collect do |shared_param|
          shared_param['SoftRev'] = softrev
          shared_param['ToolPrefix'] = eqp_prefix unless shared_param.has_key?('ToolPrefix') # per convention always set
          shared_param['ToolVendor'] = vendor unless shared_param.has_key?('ToolVendor') # per convention always set
        end
      end      
      recipe_file = "testcasedata/recipes/#{recipe_name}" if recipe_name

      rob = @rms.create_object(robdef[0], rob_tmpl.context, rob_tmpl.dataitems,
            params.merge(recipe_file: recipe_file, 
            linked_contexts: rob_tmpl.linked_contexts,
            sp_contexts: rob_tmpl.shared_params
        ))
      
      # check for variants to the rob and add them
      if params[:variants] && params[:variants].key?(recipe_name)
        params[:variants][recipe_name].each do |vrob_tmpl|
          rob_v = $rms.put_variant_keys(rob, vrob_tmpl.context)
          $rms.put_data(rob_v, vrob_tmpl.dataitems)
        end
      end
      robs << rob
    end
    robs
  end

  # Create a new ECSV object by importing a CSV file
  def ec_sv_create(eqp, params={})
    $log.info "#{__method__} #{eqp.inspect}, #{params.inspect}"
    depart = (params[:department] || @department)
    eqp_prefix = (params[:tool_prefix] || 'BST')
    vendor = (params[:tool_vendor] || 'LAM')
    ec_type = (params[:ec_type] || 'Main')
    registration = 'ECSV_DEF_PROD'
        
    time = Time.now.to_i
    
    ecsvdef = @rms.find_object_list( {'Department'=>depart, 'ToolPrefix'=>eqp_prefix, 'ToolVendor'=>vendor}, :registration=>registration, :with_keys=>true)
    ($log.warn 'no ECSV_DEF found'; return nil) unless ecsvdef.count > 0
    $log.info "Found #{ecsvdef[0].rmshandle}"

    context = {'Department'=>depart, 'ToolPrefix'=>eqp_prefix, 'ToolVendor'=>vendor, 'ECType'=>ec_type,
        'ToolRecipeName'=>'ecsv'}
      
    ecsv = EI::ECSVData.new(eqp)
    ecsv_data = ecsv.ecsv_as_xml()
    rob = @rms.create_object(ecsvdef[0], context, {'ECSVData'=>ecsv_data}, name: "QA Test ECSV #{Time.now.to_i}")
    rob
  end

  # Create a new RMS shared parameter set from a given recipetype => ROBDEF hash
  def shared_parameter_set_create(default_params, rcp_hash, params={})
    depart = (params[:department] || @department)
    eqp_prefix = (params[:tool_prefix] || 'BST')
    softrev = (params[:softrev] || 'QA')
    vendor = (params[:tool_vendor] || 'LAM')

    recipe_type = 'SharedParameterSet'
    ($log.warn 'no recipe type given found'; return nil) unless recipe_type

    robdef = @rms.find_object_list( {'Department'=>depart, 'ToolPrefix'=>eqp_prefix, 'ToolVendor'=>vendor,
        'RecipeType'=>recipe_type, 'SoftRev'=>softrev}, registration: 'RCP_DEF_PROD', :with_keys=>true)
    ($log.warn 'no SP ROBDEF found'; return nil) unless robdef.count > 0
    $log.info "Found #{robdef[0].rmshandle}"

    # modify recipe context
    context = {}
    context['RecipeName'] = (params[:recipe_name] || '-')
    context['RecipeNameSpaceID'] = (params[:recipename_space] || '-')
    context['RecipePurpose'] = (params[:purpose] || 'Production')
    context['ToolRecipeName'] = (params[:tool_recipe_name] || Time.now.to_i)
    context['OperationMode'] = 'RecipeSelect' # per convention
    context['SoftRev'] = softrev
    context['Equipment'] = params[:eqp] if params[:eqp]

    # SP does not allow links
    rob = @rms.create_object(robdef[0], context, default_params)

    rcp_hash.each_pair do |tool_name, variant_params|
      rob_v = $rms.put_variant_keys(rob, {'Equipment' =>tool_name})      
      $rms.put_data(rob_v, variant_params)
    end
    rob
  end
  
  # Approval loop of recipes
  def recipe_approval(robs)
    begin
      ($log.error ' start approval failed'; return) unless @rms.start_approval(robs)
      ($log.error ' approval failed'; return) unless @rms.approve(robs)
    rescue RmsAPI::RmsError => r
      $log.error r.message.to_s
      return unless r.message.index('code=-215')
    end
    ($log.error ' release failed'; return) unless @rms.release(robs)
    robs
  end

  # Updates a given recipe in RMS
  def recipe_update(rob, params={})
    $log.info "update recipe ## rob: #{rob.rmshandle}}, #{params}"
    new_context = (params[:context] || {})    
    new_dataitems = (params[:dataitems] || {})

    rob_new = @rms.create_work_copy(rob, new_context, new_dataitems, params)
    # remove data items if necessary
    rob_new = @rms.del_data(rob_new, params[:rm_dataitems]) if params[:rm_dataitems]
        
    ($log.error ' new workcopy creation failed'; return) unless rob_new
    #
    rob = recipe_approval(rob_new)
    @rms.get_data(rob)
  end

  def update_recipes
    depart = 'LIT'
    eqp_type = 'ALC-TEL'
    omode = 'RecipeCompare'
    recipe_type = 'WaferFlow'
    (6..32).each do |i|
      recipe_name = format("/test/recipe/#{recipe_type}/%03d", i)
      rob = rms.find_object_with_data('Department' => depart, 'EquipmentType' => eqp_type, 'ToolRecipeName' => recipe_name)
      $log.info "found #{rob.inspect}"
      robs = rms.create_work_copy(rob, { 'RecipeName' => format('RECIPE%03d', i) }, {})
      robs = rms.start_approval(robs)
      ($log.error ' start approval failed'; return) unless robs
      robs = rms.approve(robs)
      ($log.error ' approval failed'; return) unless robs
      robs = rms.release(robs)
      ($log.error ' release failed'; return) unless robs
      $log.info "#{i} ... recipes updated"
    end
  end

  def setup_litho_recipes
    depart = @department
    eqp_prefix = 'ALC'
    vendor = 'TEL'
    omode = 'RecipeCompare'
    purpose = 'Production'

    (11..15).each do |i|
      robs = []
      @@test_recipe.each_key do |recipe_type|
        rob_tmpl = @@test_recipe[recipe_type]
        recipe_name = format("/test/recipe/big/#{recipe_type}/%03d", i)

        robdef = @rms.find_object_list({ 'Department' => depart, 'ToolPrefix' => eqp_prefix, 'ToolVendor' => vendor,
          'RecipeType' => recipe_type, 'SoftRev' => 'QA' }, registration: 'RCP_DEF_PROD', with_keys: true)
        next unless robdef.count > 0
        $log.info "Found #{robdef[0].rmshandle}"

        # modify recipename
        rob_tmpl.context['RecipeName'] = rob_tmpl.context.key?('RecipeName') ? format('RECIPE%03d', i) : '-'
        rob_tmpl.context['RecipeNameSpaceID'] = (rob_tmpl.context['RecipeNameSpaceID'] || '-')
        rob_tmpl.context['OperationMode'] = omode
        rob_tmpl.context['ToolRecipeName'] = recipe_name
        rob_tmpl.context['RecipePurpose'] = purpose

        # linked recipes are linked by tool recipe name
        rob_tmpl.linked_contexts&.map do |linked_context|
          linked_rcp_type = linked_context['RecipeType']
          linked_context['ToolRecipeName'] = format("/test/recipe/big/#{linked_rcp_type}/%03d", i)
        end
        robs << @rms.create_object(robdef[0], rob_tmpl.context, rob_tmpl.dataitems,
                                   recipe_file: "testcasedata/recipes/#{recipe_type}.rcp", linked_contexts: rob_tmpl.linked_contexts)
      end
      robs = @rms.start_approval(robs)
      ($log.error ' start approval failed'; return) unless robs
      robs = @rms.approve(robs)
      ($log.error ' approval failed'; return) unless robs
      robs = @rms.release(robs)
      ($log.error ' release failed'; return) unless robs
      $log.info "#{i} ... recipes created"
    end
  end

  def test
    (11..15).each {|i|
      robs = @rms.find_object_list({ 'Department'=>@department, 'ToolPrefix'=>'ALC', 'ToolVendor'=>'TEL', 'SoftRev'=>'QA', 'ToolRecipeName'=>"/test/recipe/big/System/0#{i}"},  :registration=>'RCP_ALL', :with_keys=>true)
      robs.each{|rob|
        begin
          @rms.disapprove(rob)
        rescue
        end
      }

      robs.each{|rob|
        begin
          @rms.delete_work(rob)
        rescue
        end
      }
      
      robs = @rms.find_object_list({ 'Department'=>@department, 'ToolPrefix'=>'ALC', 'ToolVendor'=>'TEL', 'SoftRev'=>'QA', 'ToolRecipeName'=>"/test/recipe/big/Spinner/0#{i}"},  :registration=>'RCP_ALL', :with_keys=>true)
      robs.each{|rob|
        begin
          @rms.disapprove(rob)
        rescue
        end
      }

      robs.each{|rob|
        begin
          @rms.delete_work(rob)
        rescue
        end
      }

      robs = @rms.find_object_list({ 'Department'=>@department, 'ToolPrefix'=>'ALC', 'ToolVendor'=>'TEL', 'SoftRev'=>'QA', 'ToolRecipeName'=>"/test/recipe/big/Pump/0#{i}"},  :registration=>'RCP_ALL', :with_keys=>true)
      robs.each{|rob|
        begin
          @rms.disapprove(rob)
        rescue
        end
      }

      robs.each{|rob|
        begin
          @rms.delete_work(rob)
        rescue
        end
      }

      robs = @rms.find_object_list({ 'Department'=>@department, 'ToolPrefix'=>'ALC', 'ToolVendor'=>'TEL', 'SoftRev'=>'QA', 'ToolRecipeName'=>"/test/recipe/big/WaferFlow/0#{i}"},  :registration=>'RCP_ALL', :with_keys=>true)
      robs.each{|rob|
        begin
          @rms.disapprove(rob)
        rescue
        end
      }
      
      robs.each{|rob|
        begin
          @rms.delete_work(rob)
        rescue
        end
      }
    }
  end
  
  def armor_recipe_to_rob(recipenamespace, recipename)
    payload = @armor.rendered_payload_context recipenamespace, recipename #'E-Q-BST-LAM-CORONUS-210X', 'Q-PM2-COND'    
    recipe = @armor.to_recipe(payload)
    robs = []
    _armor_recipe_to_rob(robs, recipe, recipename: recipename, recipenamespace: recipenamespace)
    robs
  end

  def _armor_recipe_to_rob(robs, recipe, params = {})
    depart = (params[:department] || @department)
    eqp_prefix = 'ETX'
    vendor = 'LAM'
    omode = 'RecipeCompare'
    purpose = 'Production'

    # find the robdef
    robdefs = @rms.find_object_list({ 'Department' => depart, 'ToolPrefix' => eqp_prefix, 'ToolVendor' => vendor,
          'CEIRecipeType' => recipe.recipetype, 'SoftRev' => 'ARMOR' }, registration: 'RCP_DEF_PROD', with_keys: true)
    return unless robdefs.count > 0
    robdef = robdefs[0]
    $log.info "Found #{robdef.rmshandle}"

    context = {}
    context['RecipeName'] = (params[:recipename] || '-')
    context['RecipeNameSpaceID'] = (params[:recipenamespace] || '-')
    context['RecipePurpose'] = purpose
    context['ToolRecipeName'] = recipe.toolrecipename
    # context["SoftRev"] = params[:softrev] if params[:softrev]

    dataitems = {}
    recipe.parameters&.each do |p|
      dataitems[p.pid] = RmsAPI::DataItem.new(p.value, p.purpose)
    end

    # linked recipes are linked by tool recipe name
    linked_contexts = []
    softrev = 'ARMOR'
    recipe.linkedrecipes.each do |rcp|
      rob = _armor_recipe_to_rob(robs, rcp, softrev: softrev)
      linked_contexts << { 'ToolRecipeName' => rob.context['ToolRecipeName'], 'SoftRev' => softrev }
    end
    rob = @rms.create_object(robdef, context, dataitems,
                             recipe_body: recipe.bodyholder, linked_contexts: linked_contexts,
                             operation_mode: omode)
    robs << rob
    rob
  end

  # sanity check for rms recipe
  def verify_rms_recipe(recipe)
    ret = true
    ($log.warn ' no valid recipe'; return false) unless recipe

    $log.info "checking #{recipe.rid}:"
    ($log.warn ' no valid dcppaths found') unless recipe.dcppaths
    ($log.warn ' no valid operation mode', ret = false) unless ['RecipeDownload', 'RecipeCompare', 'RecipeSelect'].member?(recipe.operationmode)
    ($log.warn ' no valid eqp recipe name', ret = false) unless recipe.toolrecipename
    ($log.warn ' no valid eqp recipe type', ret = false) unless recipe.recipetype
    recipe.linkedrecipes.inject(ret) {|ret, rcp| ret &= verify_rms_recipe(rcp)}
  end
  
  # check tool recipe names according to given array of names (depth search)
  def verify_tool_recipe_names(recipe, tool_rcp_names)
    _trcp_name = tool_rcp_names[0]
    unless _trcp_name
      $log.warn("no entry for #{recipe.toolrecipename} found")
      return false
    end
    res = verify_equal(_trcp_name, recipe.toolrecipename)
    recipe.linkedrecipes.inject(res) { |_res, rcp|  
      tool_rcp_names = tool_rcp_names.drop(1)
      _res &= verify_tool_recipe_names(rcp, tool_rcp_names)
    }
  end

  def verify_many_links(rob, item_name, ctx_name, count = 1)
    values = rob.dataitems[item_name].value
    values = [values] unless values.is_a?(Array)
    values.inject(true) do |res, v|
      if v == '' # skip empty values
        res
      else
        links = rob.linked_contexts.select { |c| c[ctx_name] && c[ctx_name].index(v) }
        res & verify_equal(count, links.count, "expected one link for '#{v}', but found #{links.inspect}")
      end
    end
  end
  
  # find links which are no template
  def verify_existing_links(robdef, rob, params={})    
    links = robdef.linked_contexts
    links.reject! {|l| l.values.find {|v| v =~ /(\$.+\$)|(#.+#)/}} unless params[:tmpl_links]
    links.inject(true) {|res, link|
      l = rob.linked_contexts.find {|rlink| rlink == link}
      $log.warn "  link does not match: #{link.inspect}" unless l
      res && !!l
      # res &= verify_condition("link does not match, #{link}") { rob.linked_contexts.find {|rlink| rlink == link} }
    }
  end

  # sanitiy check for parameters
  def check_recipe_parameters(recipe, einame: true)
    parameters = recipe[:parameters]
    ret = true
    ($log.warn ' no valid parameters'; return false) unless parameters
    parameters.each do |parameter|
      $log.info "checking #{parameter.pid}:"
      ($log.warn ' no valid parameter name found'; ret = false) unless parameter.pid
      if einame
        ($log.warn ' no valid ei parameter name found (fallback active)'; ret = false) unless parameter.eiparametername && parameter.eiparametername != ''
      end
      value = parameter.value
      ($log.warn ' no valid value found') unless value
      if value.is_a?(Numeric)
        ($log.warn ' no lower limit found') unless parameter.lowlimit
        ($log.warn ' no upper limit found') unless parameter.highlimit
      end
      ($log.warn ' no valid purpose found'; ret = false) unless parameter.purpose
    end
    recipe.linkedrecipes.inject(ret) {|ret,rcp| ret &= check_recipe_parameters(rcp)}
  end
  
  # checks the parameters' exisitence in given recipe. No subrecipe considered.
  def verify_recipe_parameters(parameters, recipe)
    _parameters = recipe[:parameters]
    ret = true
    ($log.warn ' no valid parameters'; return false) unless _parameters
    parameters.each do |parameter|
      $log.info "  checking #{parameter.pid}:"
      _p = recipe[:parameters].find {|p| p.pid == parameter.pid}
      ($log.error "#{parameter.pid} not found"; ret=false; next) unless _p
      ret &= verify_equal(parameter, _p)
    end
    ret    
  end

  # new parameter verification
  def verify_rtd_recipe_parameters(mainrob, subrobs, params = {})
    $log.info "#{__method__} #{mainrob.rmshandle.inspect}, #{subrobs.map(&:rmshandle).inspect}, #{params.inspect}"
    #
    ctx = Hash[mainrob.context.each_pair.select { |c, _v| RMS::ContextKeys.values.member?(c) }]
    # map relevant keys to context
    subrobs.each do |rob|
      RMS::ContextKeys.values.each do |key|
        ctx[key] = rob.context[key] if rob.context.key?(key) && !ctx.key?(key)
      end
    end
    res, mds_rcp = verify_context(mainrob, ctx, params)
    return res if mds_rcp == []
    # check variants
    $rms.get_variants(mainrob).each do |vrob|
      vctx = ctx.merge(vrob.context)
      vres, mds_vrcp = verify_context(vrob, vctx, params)
      $log.info "#{vrob.rmshandle}: #{vres}"
      res &= vres
    end
    # Sub recipes
    res &= subrobs.inject(true) do |r, srob|
      r &= verify_parameters(srob, mds_rcp, params.merge(srob: srob))
      $rms.get_variants(srob).each do |vrob|
        vctx = ctx.merge(vrob.context)
        vres, mds_vrcp = verify_context(mainrob, vctx, params.merge(srob: vrob))
        $log.info "#{vrob.rmshandle}: #{vres}"
        r &= vres
      end
      r
    end
    res
  end

  # Generic method to verify context of rtd data and rob context
  def verify_context(mainrob, ctx, params)
    res = true
    # retrieve data for main rob
    rmshandle = mainrob.rmshandle.split('2c')[0]
    $log.info "checking mrob #{rmshandle}"

    # filter for recipe handle and additional keys
    rparam = @rildb.recipe_parameter(ctx.merge(rmshandle: rmshandle))
    if params[:deactivated]
      return verify_equal(0, rparam.size, 'recipe should have been removed'), []
    end

    return false, [] unless verify_equal(1, rparam.size, "need one matching recipe, #{rparam.size} found")
    mds_rcp = rparam[0]

    # Compare contexts
    res &= verify_equal(ctx['RecipeNameSpaceID'], mds_rcp.recipenamespace, 'wrong recipenamespace')
    res &= verify_equal(ctx['RecipeName'], mds_rcp.recipename, 'wrong recipename')
    res &= verify_equal(ctx['Equipment'], mds_rcp.equipmentid, 'wrong equipment')
    res &= verify_equal(ctx['PhotoLayer'], mds_rcp.photolayer, 'wrong photolayer')
    res &= verify_equal(ctx['ProcessDefinition'], mds_rcp.pd, 'wrong photolayer')
    res &= verify_equal(ctx['MainProcessDefinition'], mds_rcp.mainpd, 'wrong route')
    res &= verify_equal(ctx['ProductID_EC'], mds_rcp.productidec, 'wrong product')
    res &= verify_equal(ctx['ProductGroup'], mds_rcp.productgroup, 'wrong product group')
    res &= verify_equal(ctx['Technology'], mds_rcp.technology, 'wrong technology')
    res &= verify_equal(ctx['ReticleID'], mds_rcp.reticleid, 'wrong reticle')

    # check for onhold flag
    res &= if params[:onhold_flag]
             verify_equal('TRUE', mds_rcp.onhold, 'wrong onhold state in MDS replication')
           else
             verify_equal('FALSE', mds_rcp.onhold, 'wrong onhold state in MDS replication')
           end

    # Checking main recipe
    _rob = params[:srob] ? params[:srob] : mainrob
    res &= verify_parameters(_rob, mds_rcp)

    [res, mds_rcp]
  end

  def verify_parameters(rob, mds_rcp, params = {})
    $log.info "#{__method__} #{rob.rmshandle} #{mds_rcp.inspect} #{params.inspect}"
    rtd_params = @@rtd_configured_params  #.split
    rmshandle = rob.rmshandle
    rmshandle = rmshandle.split('2c')[0] unless rob.is_variant
    res = true
    # Parameters
    unless rob.dataitems.keys.select { |k| rtd_params.member?(k) }.count > 0 || params[:eval_subrobs]
      res &= verify_equal(0, mds_rcp.parameters.count, 'No recipe parameter should be replicated')
    end

    # group parameters
    if params[:eval_subrobs]
      groups = @@rtd_configured_groups.map(&:split)
      groups.each do |group|
        # find complete group in list
        if Set.new(rob.dataitems.keys) & Set.new(group) == Set.new(group)
          # all parameters of group should exist
          $log.info "  check group #{group}"
          res &= group.inject(true) do |_res, key|
            _res &= verify_param(mds_rcp, rmshandle, key, rob.dataitems[key].value, rob.is_variant)
          end
          break # no other group to check
        else
          # no parameter of list should be found
          res &= group.inject(true) do |_res, key|
            _db_par = if rob.is_variant
                        mds_rcp.parameters.select { |p| p.rmshandleext == rmshandle && p.pname == key }
                      else
                        mds_rcp.parameters.select { |p| p.rmshandle == rmshandle && p.pname == key }
              end
            _res &= verify_equal(0, _db_par.count, "  #{key} not expected in table")
          end
        end
      end
    end

    # other parameters
    rob.dataitems.each_pair do |k, v|
      next unless rtd_params.member?(k) # skip non-rtd parameters
      res &= verify_param(mds_rcp, rmshandle, k, v.value, rob.is_variant)
    end
    res
  end

  def verify_param(mds_rcp, rmshandle, key, v, is_variant)
    $log.info "#{__method__} #{rmshandle}, #{is_variant.inspect}"
    _db_par = if is_variant
                mds_rcp.parameters.select { |p| p.rmshandleext == rmshandle && p.pname == key }
              else
                mds_rcp.parameters.select { |p| p.rmshandle == rmshandle && p.pname == key }
              end
    return false unless verify_equal(1, _db_par.count, "  #{key} not found in table")
    _val = _db_par[0].pvalue
    _val = _val.to_f if v.is_a?(Float)
    _val = _val.to_i if v.is_a?(Integer)
    _val = _val == 'true' if v.is_a?(TrueClass)
    _val = _val != 'false' if v.is_a?(FalseClass)
    # TODO: Improve verification
    v = (v.to_i * 1000).to_s if v.is_a?(Time)
    verify_equal(v, _val, "  invalid parameter #{key}")
  end

  # return false if not the expected count of events found
  def verify_notification(msg_start, msg_more = nil, params = {})
    $log.info "#{__method__} #{msg_start.inspect}, #{msg_more.inspect}, #{params.inspect}"
    # ($log.warn '  no connection to Derdack DB, skipping test'; return true) unless @derdack.db.connected?
    # app = 'RIL'
    # evs = @derdack.get_events((params[:tstart] || @ril.txtime), params[:tend], 'Application' => app)
    # ($log.warn '  no connection to Derdack'; return nil) unless evs
    # msgs = evs.map do |e|
    #   $log.debug "  #{e.inspect}"
    #   m = e.params['Message']
    #   next unless msg_start && m.start_with?(msg_start)
    #   next unless msg_more.nil? || !m.index(msg_more).nil?
    #   e
    # end.compact
    msgs = @notifications.query(tstart: params[:tstart] || @ril.txtime, tend: params[:tend], filter: {'Application'=>'RIL'}) || return
    msgs.select! {|e|
      m = e['Message']
      msg_start && m.start_with?(msg_start) && (msg_more.nil? || m.include?(msg_more))
    }
    $log.debug " msgs: #{msgs.inspect}"
    $log.warn "  more than one message found:\n #{msgs.inspect}" if msgs.size > 1
    $log.warn '  no message found:' if msgs.empty?
    params[:raw] ? msgs : msgs.size == 1
  end

  # find valid EI contexts based on context keys
  def find_contexts(count, params = {})
    # find recipes in SiView
    sv_rcps = @rms.get_reg_entries(filters: params.merge('KnownBySiView' => 'TRUE'), columns: %w[RecipeNameSpaceID RecipeName Equipment])

    contexts = []
    sv_rcps.shuffle.take(count).each do |(rcp_space, rcp_name, eqp, handle)|
      # save context temporarly
      ctx = Hash[RMS::ContextKeys.values.map { |c| [c, nil] }]
      ctx['RecipeNameSpaceID'] = rcp_space
      ctx['RecipeName'] = rcp_name
      ctx['Equipment'] = eqp if eqp != ''
      _extract_ctx_from_links(handle, ctx)
      puts ctx.inspect
      # build contexts
      contexts << ctx.values
    end
    contexts
  end

  def _extract_ctx_from_links(handle, ctx)
    rob = @rms.get_data(RmsAPI::RobObject.new(handle))
    rob.linked_contexts.each do |l|
      # find links with wildcard
      next unless l.value?('$')
      _cols = []
      _filters = ctx.reject { |c, v| v.nil? || c == 'RecipeName' || c == 'RecipeNameSpaceID' || c == 'Equipment' }
      # extract relevant contexts
      l.each_pair do |k, v|
        if v == '$'
          _cols << k unless _cols.member?(k)
        else
          if _filters.key?(k)
            raise "cannot match link key #{k}=#{v} to context: #{ctx.inspect}"
          end
          _filters[k] = v
        end
      end
      # add empty context key to the columns
      ctx.each_pair { |k, v| _cols << k unless v || _cols.member?(k) }
      # extract all available contexts from links
      _ctx = {}
      @rms.get_reg_entries(filters: _filters, columns: _cols).map do |subrcp|
        _cols.each_with_index do |c, i|
          next if subrcp[i] == ''
          _ctx[c] = Set.new unless _ctx.key?(c)
          _ctx[c] << subrcp[i]
        end
      end
      _ctx.each_pair do |c, v|
        $log.info "found context values for #{c}: #{v.inspect}"
        ctx[c] = v.first # merge the context values, take first one
      end
    end
    ctx
  end

  def _generate_contexts(items, ctx)
    if index < ctx.count
      _generate_contexts(items, ctx, index+1)
    else
      ctx[index].each {|c| items}
    end
  end

  def find_or_create_recipe(context, recipe_hash, main_tool_recipe)
    # check if recipes with context exist
    _context = context.merge('Department' => @department)
    robs = @rms.find_object_list(_context, with_keys: true)

    if robs.count == recipe_hash.keys.count
      return robs.find { |r| r.context['KnownBySiView'] == 'TRUE' }.context['RecipeName']
    elsif robs.count > 0
      @rms.deactivate(robs)
    end
    robs = recipe_create(recipe_hash, _context.merge(ignorelinks: true, checklinks: false))
    robs = recipe_approval(robs)
    robs.find { |r| r.context['ToolRecipeName'] == main_tool_recipe }.context['RecipeName']
  end

  # Search departments for recipes and retrieve all context keys required
  #  the contexts are saved in a csv file
  #  ncount is the number of randomly taken recipes from the department
  def contexts_to_csv(departments, filename, ncount = 10)
    File.open(filename, 'w') do |fh|
      fh.write('#')
      fh.write(RMS::ContextKeys.values.map(&:to_s).join(','))
      fh.write("\n")
      departments.each do |dpt|
        find_contexts(ncount, 'Department' => dpt, 'state' => 'ACTIVE').each do |ctx|
          fh.write(ctx.join(','))
          fh.write("\n")
        end
      end
    end
  end
  
  # Call function to save all active robs and robdefs to a file
  def backup()
    robdefs = @rms.find_object_list({'Department'=>@department, 'state'=>'ACTIVE'}, :registration=>'RCP_DEF_ALL', :with_keys=>true)
    robdefs.each {|robdef| @rms.export(robdef, 'testcasedata/robdefs')}
    robs = @rms.find_object_list({'Department'=>@department, 'state'=>'ACTIVE'}, :registration=>'RCP_ALL', :with_keys=>true)
    robs.each {|rob| @rms.export(rob, 'testcasedata/robs')}
  end
  
  # Restore the ROBDEFs from the folder
  def restore_robdefs()
    robdefs = []
    Dir['testcasedata/robdefs/*'].each do |f|
      if File.file?(f)
        $log.info "importing #{f}..."
        data = File.open(f, 'r') {|fh| fh.read}
        robdefs << @rms.import(data)
      end
    end
    robdefs
  end
  
  # Restore the ROBS from the folder
  def restore_robs()
    robs = []
    Dir['testcasedata/robs/*'].each do |f|
      if File.file?(f)
        $log.info "importing #{f}..."
        data = File.open(f, 'r') {|fh| fh.read}
        begin
          robs << @rms.import(data)
        rescue
          $log.error "failed to import #{f}!"
        end
      end
    end
    robs
  end
  
  def stress_recipe_create(rcount, rsize=1)
    robs = []
    #rcount.times do |i|
    (0..99).each do |i|
      recipe = "QA-RECIPE-%02dMB-%04d" % [rsize,i]
      cds_recipe = {
        nil => RmsAPI::RobObject.new( nil,
          { 'ToolRecipeName'=>recipe, 'OperationMode'=>'RecipeDownload', 'RecipeType'=>'Main', 'RecipeNameSpaceID'=>'QA-TEST', 'RecipeName'=>recipe},
          {},
          nil),
      }
      robs += self.recipe_create(cds_recipe, :tool_prefix=>'CDS-QA', :tool_vendor=>'AMAT', :recipe_body=>nil)
    end
    #self.recipe_approval(robs)
  end
  
  def calculate_intervalls(interval, intervalbase) 
    #$log.info "#{__method__}"
    intervals = Array.new
    if interval.include?('/')
      time = interval.split('/').last.to_i
      intervalbase.times {|i| 
        time_candidate = time * (i+1)
        intervals << time_candidate if time_candidate <= intervalbase
      }
    elsif interval.include?('*')
      intervalbase.times {|i| intervals << i}
    else
      intervals << interval.to_i
    end
    return intervals
  end
  
  def next_purging()
    purger_wait = '60' #'rlm.purger.min.time.inactive'
    purger_hour = '*' #'rlm.purger.timer.hour'
    purger_minute = '/12' #'rlm.purger.timer.minute'
    purger_second = '30' #'rlm.purger.timer.second'
    $log.info "purger_wait: #{purger_wait}, purger_hour: #{purger_hour}, purger_minute: #{purger_minute}, purger_second: #{purger_second}"
    
    purger_hours = calculate_intervalls(purger_hour, 24) 
    purger_minutes = calculate_intervalls(purger_minute, 60) 
    purger_seconds = calculate_intervalls(purger_second, 60)
    $log.info "Hours: #{purger_hours}, Minutes: #{purger_minutes}, Seconds: #{purger_seconds}"
    
    purging_times = Array.new
    purger_hours.each {|ph| 
      purger_minutes.each {|pm|
        purger_seconds.each {|ps|
          pm = 00 if pm == 60 
          purging_times << '%02i' % ph + ':' + '%02i' % pm + ':' + '%02i' % ps
        }
      }
    }
    $log.info "Purging times: #{purging_times.inspect}"
    
    purging_timers = Array.new
    time = Time.now
    wait_until = time + purger_wait.to_i * 60
    $log.info "wait_until: #{wait_until.inspect}"
    purging_times.each {|pt|
      $log.info "Time.parse(#{pt.inspect}): #{Time.parse(pt).inspect}"
      if Time.parse(pt) < wait_until
        purging_timers << Time.parse(pt) if Time.parse(pt) > Time.now
        next
      else
        wait_until = Time.parse(pt)
        break
      end
    }
    $log.info "Next Purge at: #{wait_until}"
    return wait_until, purging_timers
  end
end
