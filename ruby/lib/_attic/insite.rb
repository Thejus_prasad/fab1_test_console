=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author:     ssteidte, 2014-03-07
Version:    0.0.1
History:
=end

require 'builder'
require 'rqrsp'

# Fab3E Insite WebService Interface
module Insite

  class Service < RequestResponse::HTTP
    attr_accessor :env, :insiteuser, :insiteclient
    
    def initialize(params={})
      @env = params[:env] || 'fab3e'
      @insiteuser = params[:insiteuser] || 'ASMVIEW'
      @insiteclient = params[:insiteclient] || '050001'
      super("insite.#{@env}", {timeout: 60}.merge(params))
    end
    
    def lot_info(lot, params={})
      $log.info "lot_info #{lot.inspect}"
      xml = Builder::XmlMarkup.new(:indent=>2)
      xml.instruct!
      xml.soapenv :Envelope,  "xmlns:soapenv"=>"http://schemas.xmlsoap.org/soap/envelope/" do |enve|
        enve.soapenv :Body do |body|
          body.ns1 :ViewLotService_GetStdDetail, "xmlns:ns1"=>"http://localhost/InSiteService/" do |svc|
            svc.ns1 :inParam do |inparam|
              inparam.ns1 :Header do |htag|
                htag.ns1 :User, @insiteuser
                htag.ns1 :Client, @insiteclient
              end
              inparam.ns1 :Name, lot
            end
          end
        end
      end
      ret = _send_receive(xml.target!, {soapaction: "http://localhost/InSiteService/ViewLotService_GetStdDetail"}.merge(params))
      ok = ret[:ViewLotService_GetStdDetailResponse][:ViewLotService_GetStdDetailResult][:Header][:Status] == "SUCCESS"
      ($log.warn "error: #{$ret}"; return nil) unless ok
      return ret[:ViewLotService_GetStdDetailResponse][:ViewLotService_GetStdDetailResult][:LotInf]
    end
    
  end
end
