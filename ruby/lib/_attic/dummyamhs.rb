=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: Steffen Steidten, 2010-04-16
=end
    
load './ruby/set_paths.rb'
set_paths

require 'siview'
require Dir['lib/log4j*jar'].first
Dir['lib/slf4j*jar'].each {|f| require f}
Dir['lib/commons*jar'].each {|f| require f}


class DummyAMHSR
  attr_accessor :env, :xm, :setup, :zone, :entity, :_launcher, :_launcherthread
  
  # env is a string line "itdc"  
  def initialize(env, params={})
    @env = env
    # ensure XM JCSs are loaded
    @xm = SiView::XM.new(env, flavour: params[:flavour])
    require 'lib/junit_testing.jar'
    java_import 'com.amd.siview.dummyamhs.DummyAmhsLaunch'
    #
    @setup = params[:setup] || "#{env.sub('HAS', '')}_dummyamhs"
    args = ["-env", @env, "-setup", @setup]
    @zone = params[:zone] || 'DEFAULT'
    args << "-zone" << @zone
    @entity = params[:entity] || 'AMHS88'
    args << "-forceentity" << @entity
    args << "-noport" if params[:noport]
    args << "-port" << params[:port].to_s if params[:port]
    args << "-register" unless params[:noregister]
    $args = args
    begin
      @_launcher = com.amd.siview.dummyamhs.DummyAmhsLaunch.new
      @_launcherthread = Thread.new {@_launcher.start(args.to_java(:String))}
      @amhs = DummyAmhsLaunch.amhs
    rescue java.net.BindException
      throw "port for DummyAMHS is in use"
    end
  end


  def mtIOR; DummyAmhsLaunch.mtIOR; end

  def foIOR; DummyAmhsLaunch.foIOR; end
  
  
  def register
    res = DummyAmhsLaunch.register_equipment_interface(amhs.xm)
    @xm.rc(res)
  end

  def unregister
    res = DummyAmhsLaunch.unregister_equipment_interface(amhs.xm)
    @xm.rc(res)
  end


  def write_iors(dir=nil)
    dir ||= "log/ior"
    FileUtils.mkdir_p(dir)
    open("#{dir}/#{@env}_#{@entity}_mt.IOR", "wb") {|fh| fh.write(mtIOR)}
    open("#{dir}/#{@env}_#{@entity}_fo.IOR", "wb") {|fh| fh.write(foIOR)}
  end
  
end
