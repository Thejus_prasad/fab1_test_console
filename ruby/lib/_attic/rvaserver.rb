=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.


Fetch Spec List: http://f36asd25:8380/RVA2Server/request?limit=1000&userID=mdatzman&offset=0&type=LoadSpecsCacheRequest&useFinal=true
Reload from P-Drive: http://f36asd25:8380/RVA2Server/request?limit=1000&userID=treissma&offset=0&type=CacheSpecsRequest&useFinal=true&isRequestFromUI=true
Validation Results: http://f36asd25:8380/RVA2Server/request?limit=1000&userID=mdatzman&consume_exceptions=true&offset=0&useFinal=true&type=ValidationResultsRequest

For the validation itself, you will of-course have to select the specs/routes from the list available when you hit the fetch spec list or Reload from p-drive [modify request parameter: specsList]
Before Spec Sign-off Validation: http://f36asd25:8380/RVA2Server/request?specsList=C02-0024-AR%2CC02-0026-K%2C&userID=treissma&email=Thomas.Reissmann%40globalfoundries.com&jobID=BeforeSignOff2013-07-09-242857&isEmailRequired=true&c07udataspec=C07-00000428KG&type=CheckSpecsBeforeSignoffRequest

Before Effective validation: http://f36asd25:8380/RVA2Server/request?specsList=C02-1535-A&userID=mwillkom&email=Maik.Willkommen%40globalfoundries.com&jobID=BeforeEffective2013-07-09-130844&isEmailRequired=true&c07udataspec=C07-00000428KG&type=CheckSpecsBeforeEffectiveRequest


Author:     ssteidte, 2013-07-09

Version:    1.2.1

History:
  2015-02-26 ssteidte,  use _get_json_request where appropriate
  2016-08-24 sfrieske,  use rqrsp_http with get etc., change spec names from C02-QE01-v1 to C02-QE01v1
=end

require 'rqrsp_http'

# RVA Testing Support
module RVA
  class Server < RequestResponse::HttpJSON
    attr_accessor :env, :spread, :rvauser, :udataspec, :speclist, :locations, :results, :resultfile, :valduration

    def initialize(env, params={})
      @env = env
      @spread = params[:spread] || 60
      @rvauser = params[:rvauser] || 'qatest'   # denotes the work directory, e.g. /local/logs/rval/workarea/f36asd25/qatest
      @udataspec = params[:udataspec] || 'C07-00000428KH'
      @speclist = []
      @locations = []
      super("rva.#{@env}", params)
    end

    def fetch_speclist
      $log.debug "fetch_speclist"
      @speclist = get(resource: 'request', args: {'type'=>'LoadSpecsCacheRequest'})
    end

    # reload spec files from the external file system (P:)
    def reload_files
      $log.info "reload_files"
      @speclist = get(resource: 'request', args: {'type'=>'CacheSpecsRequest'})
    end

    def specfiles(spec)
      fetch_speclist
      s = spec.split('v').first # split off version
      @speclist.select {|e| (e['specificationID'] == spec) && (version.nil? || (e['Version'] == version))}
    end


    def check_specs(specs, params={})
      email = params[:email]
      specs = [specs] if specs.kind_of?(String)
      type = params[:effective] ? 'CheckSpecsBeforeEffectiveRequest' : 'CheckSpecsBeforeSignoffRequest'
      args = {'specsList'=>specs.join(','), 'userID'=>@rvauser, 'isEmailRequired'=>(!!email).to_s,
        'enableUdata'=>(!!params[:udata]).to_s, 'jobID'=>"QA#{unique_string}", 'type'=>type}
      args['c07udataspec'] = @udataspec if @udataspec
      args['email'] = email if email
      $log.info "check_specs #{specs}, udata: #{!!params[:udata]}, effective: #{!!params[:effective]}"
      @locations = []
      res = get(resource: 'request', args: args) || return
      @locations = res['results']
    end

    # get validation result of the check_specs request, pass unfinished: true to get results for unfinished requests,
    #
    # pass location: :all to get results for all (not just the last) requests
    #
    # return hash of job data
    def validation_results(params={})
      tstart = params[:tstart]
      tend = params[:tend]
      wait_timeout = params[:wait_timeout] || 300   # time to wait for the request to become visible
      location = params[:location] || @locations.first
      $log.info "validation_results location: #{location}"
      return unless location
      @results = []
      wait_for(timeout: wait_timeout) {
        # res = _send_receive("GET", raw: true, resource: 'request', args: {'type'=>'ValidationResultsRequest', 'userID'=>@rvauser}) || return
        res = get(resource: 'request', args: {'type'=>'ValidationResultsRequest', 'userID'=>@rvauser}) || return        # the result is an array of hashes, with the jobs grouped by day; convert timestamps and sort by timeStart; optionally filter
        @results = res.collect {|e| JSON.parse(e)}.flatten.collect {|e|
          e['timeStart'] = Time.parse(e['timeStart'])
          next if tstart && ((e["timeStart"] - tstart).abs > @spread)
          if e['timeEnd'].nil?
            next unless params[:unfinished]
          elsif e['timeEnd'].start_with?('[validation')
            next unless params[:unfinished]
          elsif e['timeEnd'].start_with?('Job cancel')
            next unless params[:unfinished]
          else
            e['timeEnd'] = Time.parse(e['timeEnd'])
            next if tend && ((e['timeEnd'] - tend).abs > @spread)
          end
          next if (location != :all) && (e['location'] != location)
          e
        }.compact.sort {|a, b| a['timeStart'] <=> b['timeStart']}
        # return for wait_for
        !@results.empty?
      } || ($log.warn "  timeout after #{wait_timeout}s"; return)
      return @results
    end

    # cancel a validation job, identified by its location or jobID
    def cancel_validation(params={})
      jobid = params[:jobid] || validation_results({unfinished: true}.merge(params)).first['jobID']
      $log.info "cancel_validation #{jobid.inspect}"
      res = get(resource: 'request', args: {'type'=>'CancelValidationRequest', 'jobID'=>jobid})
      return res == ''
    end

    # return xlsx file on success or nil
    def get_resultfile(params={})
      # location is either a path as returned by check_specs or "location" in #validation_results' response
      location = params[:location] || @locations.first
      $log.info "get_resultfile location: #{location.inspect}"
      ($log.warn "  no location"; return) unless location
      @resultfile = nil
      args = {'filename'=>File.basename(location), 'userID'=>(params[:rvauser] || @rvauser)}
      res = get(raw: true, resource: 'download', args: args)
      ($log.warn "  error retrieving file: #{res.inspect}"; return) unless res && res.start_with?('PK')
      @resultfile = res
    end

    def save_resultfile(fname=nil)
      fname ||= "log/#{File.basename(@locations.first)}"
      $log.info "writing file #{fname}"
      File.binwrite(fname, @resultfile)
    end

    # higher level method calling check_specs, validation_results (waiting for the job to complete) and get_resultfile
    #
    # return xlsx file or nil
    def validate(specs, params={})
      @valduration = nil
      specs = [specs] if specs.kind_of?(String)
      check_specs(specs, params) || return
      res = validation_results(params)
      return if res.nil? || res.empty?
      # consistency check
      slist = res.first['specsList']
      ($log.warn "  error in spec format: #{slist}"; return) unless slist.include?(specs.join(' '))
      @valduration = (res.first['timeEnd'] - res.first['timeStart']).to_i  # server reports whole seconds anyway
      $log.info "  validation time: #{@valduration}s"
      sleep 20  # to allow the server to complete writing
      get_resultfile(params)
    end
  end
end
