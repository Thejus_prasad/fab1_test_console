=begin
ARMOR MQ interface
  
Author:     dsteger, 2012-08-27
Version:    1.1.0
  
=end

require 'rubygems'
require 'builder'
require 'rqrsp_mq'
require 'misc'
require 'base64'
require 'dbsequel'

module RMS
  RtdRecipe = Struct.new(:id, :toolprefix, :toolvendor, :eqp, :operationmode, :photolayer, :pd, :product,
    :productec, :rcp_name, :rcp_namespace, :version, :changed, :created, :rmshandle, :reticle, :route, :parameters)

  RtdContext = Struct.new(:cid, :created, :equipmentid, :mainpd, :mainrobhandle, :operationmode, :photolayer, 
    :pd, :productgroup, :productidec, :recipename, :recipenamespace, :recipeversionid, :reticleid, :technology, 
    :toolprefix, :toolvendor, :lot, :opno, :onhold, :parameters)
  
  RtdParameter = Struct.new(:pid, :rmshandle, :rmshandleext, :treeindex, :cid, :pname, :pvalue, :gid)
    
  class RilError < StandardError
  end

  ContextKeys = {
    'RecipeName' => 'RecipeName',
    'RecipeNameSpaceID' => 'RecipeNameSpaceID',
    'Equipment' => 'Equipment',
    'MainPD' => 'MainProcessDefinition',
    'PhotoLayer' => 'PhotoLayer',
    'ProcessDefinition' => 'ProcessDefinition',
    'ProductID.EC' => 'ProductID_EC',
    'ReticleID' => 'ReticleID',
    'ProductGroup' => 'ProductGroup',
    'Technology' => 'Technology'
  }.freeze # Produt group, Technology

  # new tables for RTD parameters
  class RtdParameterDB 
    attr_accessor :env, :db

    DBKeys = {
      :rmshandle => :mainrobhandle,
      'RecipeName' => :recipename,
      'RecipeNameSpaceID' => :recipenamespace,
      'Equipment' => :equipmentid,
      'MainProcessDefinition' => :mainpd,
      'PhotoLayer' => :photolayer,
      'ProcessDefinition' => :processdefinitionid,
      'ProductID_EC' => :productidec,
      'ReticleID' => :reticleid,
      'ProductGroup' => :productgroup,
      'Technology' => :technology,
      'Lot' => :lotid,
      'OperationNumber' => :operationnumber
    }.freeze
    
    def initialize(env)
      @env = env
      @db = ::DB.new("mds.#{@env}", schema: ['let', 'itdc'].member?(@env) ? 'MDS_ADMIN' : 'RMS_MISC')
    end
    
    def recipe_parameter(params={})
      query = @db.dbh[:T_RTD_RECIPE_CONTEXT]
      #query = params.inject(query) {|q,(k,v)|         
      #  q = q.filter(DBKeys[k]=>v)
      #}
      # all other keys are NULL
      query = DBKeys.inject(query) {|q,(k,v)|
        q = q.filter(DBKeys[k]=>params[k])
      }
      # need to define which columns are returned
      query = query.select_map([:id, :created, :equipmentid, 
        :mainpd, :mainrobhandle, :operationmode, :photolayer, 
        :processdefinitionid, :productgroup, :productidec, :recipename, :recipenamespace, 
        :recipeversionid, :reticleid, :technology, 
        :toolprefix, :toolvendor, :lotid, :operationnumber, :onholdindicator])
      
      query.map {|context|        
        rtd_param = RtdContext.new(*context)        
        pg = @db.dbh[:T_RTD_PARAMETER_GROUP].filter(contextid: rtd_param.cid).inner_join(:T_RTD_PARAMETER, groupid: :id)        
        rtd_param.parameters = pg.all.collect {|p| RtdParameter.new(*p.values)}
        rtd_param
      }      
    end
    
    def parameters(params={})
      query = @db.dbh[:T_RTD_PARAMETER_GROUP]
      query = params.inject(query) {|q,(k,v)| q = q.filter(k=>v)}
      query.inner_join(:T_RTD_PARAMETER, groupid: :id).all.collect {|p| RtdParameter.new(*p.values)}
    end
  end
  
  class RIL < RequestResponse::MQXML
    attr_accessor :env, :eqp
    include XMLHash

    # TODO: uploadEquipmentId and uploadUser
    RmsRecipe = Struct.new(:rid, :contextkey, :dcppaths, :operationmode, :rmshandle, :toolrecipename, :recipetype,
      :downloadflag, :parameters, :linkedrecipes, :dcps, :version, :bodyholder)

    Parameter = Struct.new(:pid, :eiparametername, :lowlimit, :highlimit, :value, :purpose, :options)

    # Context keys for CEI (mapping to RMS context names possible):
    #
    #"RecipeNameSpaceID"
    #
    #"RecipeName"
    #
    #"Equipment"
    #
    #"PD"
    #
    #"PhotoLayer"
    #
    #"ReticleID"
    #
    #"MainPD"
    #
    #"ProductID.EC"
    
    def initialize(env, eqp, params={})
      @env = env
      @eqp = eqp
      super("ril.#{env}", params.merge({reply_queue: "CEI.#{@eqp}_REPLY01", correlation: :msgid, use_jms: false, mquser: ''}))
    end
    
    def inspect
      "#<#{self.class.name} env: #{@env}, eqp: #{@eqp}>"
    end
  
    def _create_context_data(context_params, params={})
      mode = (params[:mode] or 'Prod')
      smartbody = (params[:smartbody] or nil)
      cxml = Builder::XmlMarkup.new
      cxml.instruct!
      cxml.getRecipePayloadRequest "xmlns"=>"http://www.amd.com/schemas/cei-ril.xsd", :mode=>mode, :smartBodyFetch=>smartbody do |t|
        context_params.each_pair {|k,values|
          values = [values] if values.kind_of?(String)
          values.each {|v|
            t.context do |nv|
              nv.name k
              nv.value v
            end
          }
        }
      end
      cxml.target!      
    end

    # Extract recipe handle and bodies from a recipe
    def _retrieve_recipe_bodies(rhash, recipe)
      rhash[recipe.rmshandle] = recipe.bodyholder if recipe.bodyholder #recipe.operationmode == "RecipeCompare"
      recipe.linkedrecipes.each{|rcp| _retrieve_recipe_bodies(rhash, rcp)}
      rhash
    end
    
    def encode_name(toolrecipename)
      _str = toolrecipename
      repl_map = { '"' => '%22', 
        '*' => '%2A', 
        '/' => '%2F', 
        ':' => '%3A', 
        '<' => '%3C', 
        '>' => '%3E',
        '?' => '%3F',
        '\\' => '%5C',
        '|' => '%7C' }
      repl_map.each {|k,v| _str = _str.gsub(k,v)}
      _str
    end
        
    def bodies_to_file(toolname, recipe)
      path = "testcasedata/recipes/#{toolname}"
      Dir.mkdir(path) unless File.directory?(path)
      rcpname = encode_name(recipe.toolrecipename)      
      filename = "#{path}/#{rcpname}"
      if recipe.bodyholder
        File.open(filename, 'wb') do |fh|
          fh.write(recipe.bodyholder.unpack('m')[0])
        end
        $log.info "#{filename} saved."
      end
      recipe.linkedrecipes.each{|rcp| bodies_to_file(toolname, rcp)}
    end
    
    def file_to_body(filename)
      body = open(filename, 'rb') {|io| io.read }
      encode64(body).tr('\n','')
    end

    def _create_compare_data(recipe_hash, params={})
      mode = (params[:mode] or "Prod")
      smartbody = (params[:smartbody] or nil)
      index = 0
      count = (params[:count] or recipe_hash.keys.count)
      cxml = Builder::XmlMarkup.new
      cxml.instruct!
      cxml.compareRecipesRequest "xmlns"=>"http://www.amd.com/schemas/cei-ril.xsd", :mode=>mode, :smartBodyFetch=>smartbody do |t|
        recipe_hash.each_pair do |handle, body|
          break if index > count
          t.compareRecipe do |nv|
            nv.rmsHandle handle
            nv.recipeBody body
          end
          index += 1
        end
      end
      cxml.target!   
    end

    # Compares a recipe (and all linked recipe parts)
    def compare_recipes(recipe, params={})
      $log.info "#{__method__} #{recipe.inspect[1..100]}..., #{params.inspect}"
      bodies = recipe.is_a?(RmsRecipe) ? _retrieve_recipe_bodies({}, recipe) : recipe
      context = Base64.encode64(_create_compare_data(bodies, params))
      # build request
      xml = Builder::XmlMarkup.new()
      xml.instruct!
      xml.env :Envelope, "xmlns:env"=>"http://schemas.xmlsoap.org/soap/envelope/", "xmlns:ns"=>"http://cei.ril.globalfoundries.com/" do |enve|
        enve.env :Body do |body|
          body.ns :compareRecipes do |t|
            t.recipes context
          end
        end
      end
      # send message
      ret = send_receive(xml.target!, params)
      return nil unless ret
      return ret if params[:rawxml]
      raise RilError.new("#{ret[:Fault].inspect}") if ret.has_key?(:Fault)
      compare = ret[:compareRecipesResponse][:result].unpack("m")[0]
      return compare if params[:rawunpacked]
      return xml_to_hash(compare)
    end


    def _hash_to_params(params_hash)
      return [] unless params_hash
      params_hash = [params_hash] unless params_hash.kind_of?(Array)
      params_hash.collect do |param|
        low = param[:lowLimit]
        high = param[:highLimit]
        value = param[:parameterValueString] if param.has_key?(:parameterValueString)
        low = low.to_f if low
        high = high.to_f if high
        value = param[:parameterValueFloat].to_f if param.has_key?(:parameterValueFloat)
        value = param[:parameterValueInteger].to_i if param.has_key?(:parameterValueInteger)
        value = param[:parameterValueBoolean] == "true" if param.has_key?(:parameterValueBoolean)
        options = {}
        _p_options = param[:parameterOption]
        if _p_options
          _p_options = [_p_options] unless _p_options.kind_of?(Array)
          _p_options.each do |opt| 
            _val = opt[nil] or ""
            options[opt["name"]] = _val
        end
        end
        Parameter.new(param["name"], param[:eiParameterName], low, high, value, param[:parameterPurpose], options)
      end
    end
    
    def _hash_to_body(body_hash)
      return body_hash[:body] if body_hash
    end

    def _hash_to_recipe(rob_hash)
      ##$log.info "found robhash: #{rob_hash.inspect}"
      subrobs = (rob_hash[:subROB] or [])
      subrobs = [subrobs] unless subrobs.is_a?(Array)
      subrobs.collect!{ |subrob| _hash_to_recipe(subrob) }
      rname = rob_hash[:contextKey].find {|r| r[:name] == "object_name"}
      rname = rname[:value] if rname
      RmsRecipe.new(rname, rob_hash[:contextKey], rob_hash[:dcpPath], rob_hash[:operationMode],
        rob_hash[:rmsHandle], rob_hash[:toolRecipeName], rob_hash[:recipeType], 
        rob_hash[:operationMode] == "RecipeDownload", _hash_to_params(rob_hash[:recipeParameter]),
        subrobs, [], nil,  _hash_to_body(rob_hash[:bodyHolder]))
    end

    # parse an rms recipe payload
    def payload_to_recipe(payload)
      rob = xml_to_hash(payload)[:getRecipePayloadResponse][:mainROB]
      _hash_to_recipe(rob)
    end

    # Retrieve a recipe
    def get_recipe_payload_request(context, params={})
      $log.info "#{__method__} #{context.inspect}, #{params.inspect}"
      context["RecipeNameSpaceID"] = "QA-TEST" unless context.has_key?("RecipeNameSpaceID")
      _context = Base64.encode64(_create_context_data(context, params))
      # build request
      xml = Builder::XmlMarkup.new()
      xml.instruct!
      xml.env :Envelope, "xmlns:env"=>"http://schemas.xmlsoap.org/soap/envelope/", "xmlns:ns"=>"http://cei.ril.globalfoundries.com/" do |enve|
        enve.env :Body do |body|
          body.ns :getRecipePayload do |t|
            t.context _context
          end
        end
      end
      # send message
      ret = send_receive(xml.target!, params)
      return nil unless ret
      return ret if params[:rawxml] or params[:raw]
      raise RilError.new("#{context.inspect}: #{ret[:Fault].inspect}") if ret.has_key?(:Fault)
      payload = ret[:getRecipePayloadResponse][:payload].unpack("m")[0]
      return payload if params[:rawunpacked]
      return payload_to_recipe(payload) if payload
    end
  end
end
