=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.


Author:  sfrieske, 2015-12-07

Version: 2.1.0

History:
  2019-05-09 sfrieske, cleanup
=end

require_relative 'session'


module ECM

  # EC handling 
  class EC
    attr_accessor :session, :ecid, :ecoid

    # session is an ecisting session object or an env (ITDC), optionally pass an EC by id, objectId or JSON object
    def initialize(session, ecxx=nil)
      @session = session.kind_of?(String) ? ECM::Session.new(session) : session
      if ecxx
        mdata = @session.ec_metadata(ecxx)
        @ecid = mdata['ecId']
        @ecoid = mdata['objectId']
      end
    end

    def inspect
      "#<ECM::EC #{@session.inspect} ecid: #{@ecid.inspect}>"
    end

    # create EC from scope and store it in this object, return ecid
    def create_ec(scopeid)
      ($log.info 'create_ec: no scope'; return) unless scopeid
      scopeid = scopeid['objectId'] if scopeid.kind_of?(Hash)
      $log.info "create_ec #{scopeid.inspect}"
      ret = @session.rqrsp.post({ecType: 'ROUTE', scopeId: scopeid}, resource: 'engineeringchanges') || return
      $log.info "  ecid: #{ret['ecId'].inspect}"
      @ecoid = ret['objectId']
      @ecid = ret['ecId']
      return @ecid
    end

    # delete EC
    def delete
      $log.info "delete #{@ecid}"
      @session.rqrsp.delete(resource: "engineeringchanges/#{@ecoid}")
    end

    # admin resolve EC
    def resolve(params={})
      data = {comment: params[:comment] || 'QA'}
      $log.info "resolve #{@ecid}, #{data}"
      res = @session.rqrsp.post(data, resource: "engineeringchanges/#{@ecoid}/resolve")
      return res == ''
    end

    # change EC metadata, valid parameters are :comment, :description, :owner; return true on success
    def actions(args)
      $log.info "actions #{args}"
      # {actions: [{operation: op, data: [strings]}]},  op is addComment, editDescription, changeOwner (new oid)
      ops = {comment: 'addComment', description: 'editDescription', owner: 'changeOwner'}
      actiondata = args.each_pair.collect {|action, data|
        data = [data] if data.kind_of?(String)
        {operation: ops[action], data: data}
      }
      res = @session.rqrsp.post({actions: actiondata}, resource: "engineeringchanges/#{@ecoid}")
      return res == ''
    end

    # submit EC, optional parameters are neededBy: Time (defaults to tomorrow),
    #   type: 1, mains: [{copyFrom: route, copyTo: newroute}]   # route copy
    #   type: 2        # new version (CMFG)
    #   type: 3        # new version (POR)
    # return true on success
    def submit(params={})
      body = params.clone
      body[:neededBy] = (params[:neededBy] || Time.now + 86400).utc.iso8601
      $log.info "submit #{body}"
      res = @session.rqrsp.post(body, resource: "engineeringchanges/#{@ecoid}/submit") || return
      $log.info "  #{res}"
      return res['succeeded']
    end

    # recall a submitted EC, verify state as the request returns nothing and return true on success
    def recall(params={})
      body = {comment: params[:comment] || 'QA Test'}
      $log.info "recall #{body}"
      res = @session.rqrsp.post(body, resource: "engineeringchanges/#{@ecoid}/recall") || return
      return res == ''
      # ecdata = metadata(ecoid)
      # if ecdata['state'] != 'EDIT'
      #   $log.warn "  wrong state: #{ecdata['state']}"
      #   return false
      # end
      # return true
    end

    # return EC approvals object
    def approvals
      return @session.rqrsp.get(resource: "engineeringchanges/#{@ecoid}/approvals")
    end

    # approve EC, return true on state change (the request itself returns nothing)
    def approve(params={})
      body = {comment: params[:comment] || 'QA Test'}
      session = params[:session] || @session
      $log.info "approve #{body}"
      $log.info session.inspect if params.has_key?(:session)
      res = session.rqrsp.post(body, resource: "engineeringchanges/#{@ecoid}/approve") || return
      ecdata = metadata
      ($log.warn "  wrong state: #{ecdata['state']}"; return) if ecdata['state'] != 'PRE_SETUP'
      return true
    end

    # return response object containing the changes
    def changes
      return @session.rqrsp.get(resource: "engineeringchanges/#{@ecoid}/changesummary")
    end

    # return consistency object, result 3: no changes/OK
    def consistency
      return @session.rqrsp.get(resource: "engineeringchanges/#{@ecoid}/consistency")
    end

    # return impact object
    def impact
      return @session.rqrsp.get(resource: "engineeringchanges/#{@ecoid}/impact")
    end

    # return impact object with slightly terser data of MNPD, TECH, etc.
    def impactsummary
      return @session.rqrsp.get(resource: "engineeringchanges/#{@ecoid}/impact/summary")
    end

    # return array of errors and warnings or empty array on success, pass all: true to get all validations
    def validation(params={})
      $log.info "validation #{params}"
      vals = @session.rqrsp.get(resource: "engineeringchanges/#{@ecoid}/validation") || return
      return vals if params[:raw]
      ret = vals['results']
      # optionally filter, levels are 1: error, 2: success, 3: warning
      levels = params[:accept_warning] ? [1] : [1, 3]
      ret.select! {|val| levels.include?(val['result'])} unless params[:all]
      ($log.warn "validation error:\n#{ret.pretty_inspect}") unless ret.empty?
      return ret
    end

    # return array of dependency objects (one is created with the EC as of 1.4.4)
    def dependencies
      res = @session.rqrsp.get(resource: "engineeringchanges/#{@ecoid}/dependencies") || return
      ($log.warn "  wrong number of dependencies: #{res['total']}") if res['total'] != 1
      return res['data']
    end

    # create a new dependency, return dependency object
    def add_dependency(body)
      $log.info "add_dependency #{body}"
      @session.rqrsp.post(body, resource: "engineeringchanges/#{@ecoid}/dependencies")
    end

    # update the last dependency (e.g. {name: 'QA', description: 'Test'}), return true although the request returns nil
    def update_dependency(body)
      $log.info "update_dependency #{body}"
      ($log.warn "  missing name"; return) if body[:name].nil?
      depid = dependencies.last || return
      depid = depid['objectId']
      res = @session.rqrsp.post(body, resource: "engineeringchanges/#{@ecoid}/dependencies/#{depid}")
      return res == ''
    end

    # delete an dependency, return true on success
    def delete_dependency(depid)
      depid = depid['objectId'] if depid.kind_of?(Hash)
      $log.info "delete_dependency #{depid.inspect}"
      @session.delete(resource: "engineeringchanges/#{@ecoid}/dependencies/#{depid}")
    end

    # return EC task details object in data
    def tasks(ecoid=@ecoid)
      @session.rqrsp.get(resource: "engineeringchanges/#{ecoid}/tasks")
    end

    # return required (unless all: true), executable and enabled tasks as array of task objects
    def tasks_pending(params={})
      res = tasks || return
      tasks = res['data'] || ($log.warn "no tasks for EC #{@ecid}"; return)
      tasks.select! {|task| task['isRequired']} unless params[:all]
      tasks.select! {|task| task['enabled']}
      tasks.select! {|task| task['status'] == 'EXECUTABLE'}
      return tasks
    end

    # # claim EC's current (group/unassigned) task for the current user
    # def ec_task_claim(ecoid)
    #   get(['engineeringchanges', ec_oid(ecoid), 'claim'])
    # end

    # complete a task, check task status because the request returns nil; return true on success
    def complete_task(taskid, params={})
      taskid = taskid['objectId'] if taskid.kind_of?(Hash)
      body = {comment: params[:comment] || 'QA'}
      $log.info "complete_task #{taskid.inspect}, #{body}"
      res = @session.rqrsp.post(body, resource: "engineeringchanges/#{@ecoid}/task/#{taskid}/complete")
      return res == ''
      # # get task status
      # tasks = ec_tasks(ecoid) || return
      # task = tasks['data'].find {|e| e['objectId'] == taskid} || return
      # return task['status'] == 'COMPLETED'
    end


    # high level test support, from ec_test

    # clean up old EC
    def cleanup
      ecdata = @session.ec_metadata(@ecoid) || return
      state = ecdata['state']
      if ['POST_SETUP', 'POST_RELEASE'].include?(state)
        complete_tasks(first_state: state)
      elsif state != 'RESOLUTION'
        recall
        delete
      end
    end

    # complete all tasks step by step until RESOLUTION, return true on success
    def complete_tasks(params={})
      if params[:pre_approvals]
        firststate = 'PRE_APPROVALS'
        laststate = 'APPROVALS'
      else
        firststate = params[:first_state] || 'PRE_SETUP'
        laststate = params[:last_state] || 'RESOLUTION'
      end
      $log.info "complete_tasks first_state: #{firststate}, last_state: #{laststate}"
      ecdata = nil
      unless firststate == :current
        wait_for(timeout: 60) {
          metadata['state'] == firststate
        } || ($log.warn "#{@ecid} is not in state #{firststate}"; return)
      end
      ecdata = metadata
      while (state = ecdata['state']) != laststate
        tasks_pending.each {|task|
          $log.info "state: #{state}, task: #{task['data']['name']}"
          complete_task(task) || ($log.warn "task status is not COMPLETED"; return)
          sleep 1  # internal race condition?
          ecdata = metadata
          ['RESOLUTION'].each {|st|
            ($log.warn "unexpected state #{st}"; return) if ecdata['state'] == st && laststate != st
          }
          ($log.warn "state #{ecdata['state']} stateStatus ERROR"; return) if ecdata['stateStatus'] == 'ERROR'
        }
      end
      ecdata = metadata
      $log.info "state #{ecdata['state']}"
      return ecdata['state'] == laststate
    end

    # complete an EC, return true on success
    def complete_ec(params={})
      if metadata['state'] == 'EDIT'
        # add a dependency and description, call impact and validation (required in OPI only)
        update_dependency(name: 'PCRBQA', description: 'Test') || return
        actions(description: 'QA automated test') || return
        impact || return
        validation(accept_warning: true).empty? || return
        submit || return
      end
      if metadata['state'] == 'PRE_APPROVALS'
        complete_tasks(first_state: 'PRE_APPROVALS', last_state: 'APPROVALS') || return
      end
      if metadata['state'] == 'APPROVALS'
        approve(session: params[:approver_session] || @session2 || @session) || return
      end
      complete_tasks(first_state: params[:first_state], last_state: params[:last_state]) || return
    end

    # verify a validation is found, optionally filtered by opNo or a check that there are no others
    #
    # return true on success
    def verify_validation(name, params={})
      $log.info "verify_validation #{name.inspect}, #{params}"
      vals = validation(params)
      ($log.warn "error running validation"; return) unless vals
      ret = vals.select {|v| v['name'] == name}
      ret = ret.select {|v| v['data'].find {|d| d['trigger'] == params[:opNo]}} if params[:opNo]
      ($log.warn "other validations found"; return) if params[:only] && vals.size > ret.size
      ($log.warn "validation not found"; return) if ret.empty?
      return true
    end

  end

end
