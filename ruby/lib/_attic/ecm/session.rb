=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.


Author:  sfrieske, 2015-12-07

Version: 2.1.0

History:
  2016-08-22 sfrieske, refactored to streamline usage
  2016-10-18 sfrieske, removed default args, send them for GET requests only
  2019-05-09 sfrieske, cleanup
=end

ENV.delete('http_proxy')
require 'rqrsp_http'


module ECM

  # ECM HTTP Session with scopes, tags, users
  class Session
    # constants.each {|c| remove_const(c)}
    # Dir["#{File.dirname(__FILE__)}/ecm/ecm_*rb"].each {|f| load(f)}

    attr_accessor :rqrsp, :scope, :_user_oids

    def initialize(env, params={})
      @_user_oids = {}  # caches uer ObjectIds
      @rqrsp = RequestResponse::HttpJSON.new(params.delete(:instance) || "ecm.#{env}", params)
      authorize unless @rqrsp.authorization && @rqrsp.authorization.start_with?('Bearer')
    end

    def inspect
      "#<ECM::Session #{@rqrsp.instance}#{' user: ' + @rqrsp.user if @rqrsp.user}>"
    end

    # return user object on success
    def authorize
      $log.info "authorize  # user: #{@rqrsp.user}"
      ret = @rqrsp.post({}, resource: 'auth/login') || return
      auth = @rqrsp.response.header['authorization']
      ($log.warn 'not authorized'; return) unless auth
      @rqrsp.authorization = "Bearer #{auth}"
      return ret
    end

    # get SiView entries for a category. e.g. category: 'mnpd', criteria: 'C02-4*'  TODO: fails
    def categories(args={})
      @rqrsp.get(resource: 'categories', args: {limit: 0}.merge(args))
    end

    # get tags, pass label: xx to select labels by start_with
    def tags(args={})
      @rqrsp.get(resource: 'tags', args: {limit: 0}.merge(args))
    end

    # get ECs
    # optional arguments:
    #   details: true returns status, owner, description etc
    #   ecId for a single EC
    #   ##state: 'EDIT', 'APPROVALS', 'RESOLUTION', ..
    #   ecType: 1 ERA, 2 (ROUTE), nothing for all
    #   ecState: 3  # Draft (1) or Open (2); ecState: 4  # Closed
    #   limit: 0 for all, skip: xx to get entries from number xx on
    #   createdOnAfter: (Time.now - 86400 * 14).utc.iso8601  # 14 days back
    #   owner: user's oid
    #   originator: user's oid
    #
    # return response objects with ECs in data
    def ecs(args={})
      args['ecId'] = args[:ecname] if args.has_key?(:ecname)
      args['createdOnAfter'] = args[:tstart].utc.iso8601 if args.has_key?(:tstart)
      @rqrsp.get(resource: 'engineeringchanges', args: {ecType: 2, details: true, limit: 0}.merge(args))
    end

    # return oid from EC, EC name or directly if an oid was passed
    def ec_oid(ecname)
      ($log.warn 'ec_oid: no ec ID passed'; return) if ecname.nil?
      return ecname['objectId'] if ecname.kind_of?(Hash)
      return ecname unless ecname.start_with?('EC')
      res = ecs(ecname: ecname) || return
      ($log.warn "no such EC: #{ecname.inspect}"; return) if res['total'] == 0
      return res['data'].first['objectId']
    end

    # return all metadata for a particular EC as single JSON object
    def ec_metadata(ecid)
      return @rqrsp.get(resource: "engineeringchanges/#{ec_oid(ecid)}")
    end

    # user's ECs (My Engineering Changes)
    def ecs_user(user=@rqrsp.user)
      return ecs(originator: user_oid(user))
    end

    # return response objects with users in data
    def users(args={})
      return @rqrsp.get(resource: 'users', args: {details: true, limit: 0}.merge(args))
    end

    # return oid from login name or directly if an oid was passed
    def user_oid(username=@rqrsp.user)
      return username['objectId'] if username.kind_of?(Hash)
      return username if username.length >= 24  # oid passed
      # user selection not working, filter and cache
      if @_user_oids['username']
        return @_user_oids['username']
      else
        res = @rqrsp.get(resource: 'users', args: {details: true, limit: 0}) || return
        udat = res['data'].find {|e| e['username'] == username} || ($log.warn "no such user: #{username}")
        return @_user_oids['username'] = udat['objectId']
      end
    end

    # return all data for a particular user as single JSON object, UNUSED
    # def user_data(oid)
    #   @rqrsp.get(resource: "users/#{user_oid(oid)}")
    # end

    # user's EC tasks (My Tasks) as array, optionally filtered
    def user_tasks(params={})
      res = @rqrsp.get(resource: 'engineeringchanges/tasks', limit: 0) || return
      ret = res['data']
      # if ec = params[:ec]
      #   ec = ec_oid(ec)
      #   ret.keep_if {|e| e['objectId'] == ec}
      # end
      return ret
    end

    # update user data, namely tags
    def update_user(oid, action)
      $log.info "user_update #{oid.inspect}, #{action}"
      @rqrsp.post(['users', oid], action)
    end

    # add tags to self (needs approval) or a managed user (becomes effective)
    def user_tags_add(username, addtags)
      tagdata = tags
      addtags = [addtags] if addtags.kind_of?(String)
      data = addtags.collect {|tag| tagdata['data'].find {|t| t['label'] == tag}['objectId']}
      user_update(user_oid(username), {actions: [{operation: 'addTags', data: data}]})
    end

    ## map ECM scopes to SM classes
    #Scopes = {eqp: 'eqp', mainpd: 'mnpd'}

    # get scope by oid
    def get_scope(oid)
      oid = oid['objectId'] if oid.kind_of?(Hash)
      @rqrsp.get(resource: ['scopes', oid], limit: 0)
    end

    # get objects in scope belonging to a specific category (partial data from scope() above)
    def scope_categories(oid, category)
      oid = oid['objectId'] if oid.kind_of?(Hash)
      @rqrsp.get(resource: ['scopes', oid, 'categories', category], limit: 0)
    end

    # create a new scope, returns the new scope object
    def new_scope(category, data)
      category = [category] if category.kind_of?(String)
      @scope = @rqrsp.post({actions: [{operation: 'add', target: category, data: data}]}, resource: 'scopes')
    end

    # generic call to update scope data, operations are add, addAll, remove, removeAll, edit (description)
    # returns the new scope object as from #scope
    def update_scope(body)
      @scope = @rqrsp.post(body, resource: ['scopes', @scope['objectId']])
    end

    # add specific elements to a scope, returns the scope object
    def update_scope_add(data)
      category = @scope['categories'][0]['category']
      update_scope({actions: [{operation: 'add', target: category, data: data}]})
    end

    # remove specific elements from a scope, returns the scope object
    def update_scope_remove(oid, data)
      category = get_scope(oid)['categories'][0]['category']
      update_scope({actions: [{operation: 'remove', target: category, data: data}]})
    end

    # remove all elements, returns the scope object
    def update_scope_remove_all(oid)
      category = get_scope(oid)['categories'][0]['category']
      update_scope({actions: [{operation: 'removeAll', target: category}]})
    end

  end

end
