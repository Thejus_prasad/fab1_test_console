=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.


Author:  sfrieske, 2015-12-07

Version: 2.1.0

History:
  2019-05-09 sfrieske, cleanup
=end

require_relative('ec')


module ECM

  # RouteEdit EC
  class ECRoute < ECM::EC
    # RteTimelink = Struct.new(:id, :type, :opNo, :target_opNo, :actions)
    # RteTimelinkAction = Struct.new(:id, :action, :duration)
    # RteSubroute = Struct.new(:id, :type, :opNo, :mainpd, :joining_opNo)

    attr_accessor :routes

    # initialize with existing EC or create a new EC with the routes passed
    def initialize(session, params={})
      super(session, params[:ecid])
      create_ecroute(params[:routes]) if !params[:ecid] && params.has_key?(:routes)
    end

    def inspect
      "#<ECM::ECRoute #{@session.inspect} ecid: #{@ecid.inspect} routes: #{@routes}>"
    end

    # return the new ecname for route change
    def create_ecroute(routes)
      routes = [routes] if routes.kind_of?(String)
      @routes = routes
      $log.info "create_ecroute #{routes}"
      return create_ec(@session.new_scope('MNPD', routes))
    end

    # return routes in this EC as single JSON object
    def ec_routes
      return @session.rqrsp.get(resource: "routeEc/#{@ecoid}/routes", limit: 0)
    end

    # return the route details as seen by this EC as single JSON object
    def ec_routedata(route=@routes.first, params={})
      return @session.rqrsp.get(resource: "routes/#{route}/#{@ecoid}", args: {limit: params[:limit] || 0})
    end

    # return Hash of lagtimes by opNo
    def lagtimes(route=@routes.first, params={})
      rdata = params[:rdata] || ec_routedata(route) || return
      ret = {}
      rdata['operations']['data'].each {|e|
        lts = e['mainPdData']['lagTimes']
        next unless lts
        ret[e['id']] = lts.first
      }
      return ret
    end

    # delete a lagtime, return true on success
    def lagtime_delete(route, opNo, ltoid)
      rdata = ec_routedata(route, limit: 10)
      rev = rdata['mainPd']['baseObject']['revision']
      $log.info "lagtime_delete #{route.inspect}, #{opNo.inspect} (rev: #{rev})"
      oid = ltoid.kind_of?(Hash) ? ltoid['id'] : ltoid
      @session.rqrsp.delete(resource: "mainpds/#{route}/#{@ecoid}/#{rev}/operations/#{opNo}/lagTimes/#{oid}")
    end

    # return array of qtime objects
    def qtimes(route=@routes.first, params={})
      rdata = params[:rdata] || ec_routedata(route) || return
      return rdata['mainPd']['queueTimes']
    end

    # delete a QT, return true on success
    def qtime_delete(route, qtoid)
      rdata = ec_routedata(route, limit: 10)
      rev = rdata['mainPd']['baseObject']['revision']
      $log.info "qtime_delete #{route.inspect} (rev: #{rev})"
      oid = qtoid.kind_of?(Hash) ? qtoid['queueTimeId'] : qtoid
      @session.rqrsp.delete(resource: "mainpds/#{route}/#{@ecoid}/#{rev}/queueTimes/#{oid}")
    end

    # delete an opNo, return true on success
    def opNo_delete(route, opNo)
      rdata = ec_routedata(route)
      opdata = rdata['operations']['data'].find {|e| e['id'] == opNo}
      mod = opdata['modulePdId']
      moddata = rdata['modulePds'].find {|e| e['baseObject']['id'] == mod}
      modrev = moddata['baseObject']['revision'] 
      $log.info "opNo_delete #{route.inspect}, #{opNo.inspect} (module: #{mod}, rev: #{modrev})"
      resource = "modulePds/#{mod}/#{@ecoid}/#{modrev}/operations/#{opNo.split('.').last}"
      @session.rqrsp.delete(resource: resource)
    end

    # reset an opNo, return true on success
    def opNo_reset(route, opNo)
      rdata = ec_routedata(route)
      opdata = rdata['operations']['data'].find {|e| e['id'] == opNo}
      mod = opdata['modulePdId']
      moddata = rdata['modulePds'].find {|e| e['baseObject']['id'] == mod}
      modrev = moddata['baseObject']['revision'] 
      $log.info "opNo_reset #{route.inspect}, #{opNo.inspect} (module: #{mod}, rev: #{modrev})"
      resource = "modulePds/#{mod}/#{@ecoid}/#{modrev}/operations/#{opNo.split('.').last}/reset"
      @session.rqrsp.post({}, resource: resource)
    end

    # # return EC route udata as hash
    # def ecroute_udata(route, params={})
    #   rdata = params[:rdata] || ec_routedata(route) || return
    #   Hash[rdata['mainPdData']['udata'].collect {|e| [e['key'], e['value']['value']]}]
    # end

    # # return EC route modules as array or module object if modulePd is passed
    # def ecroute_modules(route, params={})
    #   rdata = params[:rdata] || ec_routedata(route) || return
    #   moddata = rdata['modulePds']['data']
    #   return moddata.find {|e| e['name']['value'] == params[:modulePd]} if params[:modulePd]
    #   return moddata
    # end

    # # return EC route operations as (filtered) array or the operation object if opNo is passed
    # def ecroute_operations(route, params={})
    #   rdata = params[:rdata] || ec_routedata(route) || return
    #   opdata = rdata['operations']['data']
    #   if params.has_key?(:type)
    #     value = {other: 0, process: 1, measurement: 2}
    #     opdata = opdata.select {|e| e['type'] == value}
    #   end
    #   if params.has_key?(:mandatory)
    #     value = params[:mandatory] ? 1 : 0
    #     opdata = opdata.select {|e| e['state']['value'] == value}
    #   end
    #   if params.has_key?(:safeop)
    #     safeop = params[:safeop]
    #     check = (safeop && safeop != '') ? proc {|s| s.start_with?(safeop)} : proc {|s| s == ''}
    #     opdata = opdata.select {|e|
    #       e['mainpdUData'].find {|u| u['key'] == 'RouteOpSafeOp' && check.call(u['value']['value'])}
    #     }
    #   end
    #   if dpt = params[:department]
    #     check = dpt.start_with?('!') ? proc {|s| s != dpt[1..-1]} : proc {|s| s == dpt}
    #     opdata = opdata.select {|e| check.call(e['department']['value'])}
    #   end
    #   return opdata.find {|e| e['number'] == params[:opNo]} if params[:opNo]
    #   return opdata
    # end

    # # return EC route subroutes (branch, rework) as array
    # def ecroute_subroutes(route, params={})
    #   opdata = ecroute_operations(route, params) || return
    #   rtype = params[:rework] ? 'reworks' : 'branches'
    #   ret = []
    #   opdata.each {|o|
    #     next unless o[rtype]
    #     o[rtype].each {|e|
    #       if params[:raw]
    #         ret << e
    #       else
    #         ret << RteSubroute.new(e['id'], rtype, o['number'], e['subFlowMainPd']['value'], e['joiningOp']['opNumber'])
    #       end
    #     }
    #   }
    #   return ret
    # end

  end

end
