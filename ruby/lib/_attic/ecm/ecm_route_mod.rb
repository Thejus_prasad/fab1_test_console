=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.


Author:     sfrieske, 2015-12-07

History:
=end


# to be called from ec.rb

module ECM
  class SessionXXX

    # resolve module id from module number, or the value passed as moduleID if it contains no '.'
    def ecroute_modid(ecid, route, mod, params={})
      return mod['id'] if mod.kind_of?(Hash)
      return mod unless mod.include?('.')  # assuming ID
      res = ecroute_modules(ecid, route, modulePd: mod) || ($log.warn "no such module: #{mod.inspect}"; return)
      return res['id']
    end

    # add a module, params after: true (default) or before: true must be passed
    # return new modid on success
    def ecroute_mod_add(ecid, route, modid, params={})
      ecid = ec_oid(ecid)
      modid = ecroute_modid(ecid, route, modid)
      $log.info "ecroute_mod_add #{ecid.inspect}, #{route.inspect}, #{modid.inspect}, #{params}"
      body = params.has_key?(:before) ? {addModulePdBeforeModulePdId: modid} : {addModulePdAfterModulePdId: modid}
      post(['engineeringchanges', ecid, 'routes', route, 'modulepds'], body)
    end

    # delete a module, return true on success
    def ecroute_mod_delete(ecid, route, modid, params={})
      ecid = ec_oid(ecid)
      modid = ecroute_modid(ecid, route, modid)
      $log.info "ecroute_mod_delete #{ecid.inspect}, #{route.inspect}, #{modid.inspect}"
      delete(['engineeringchanges', ecid, 'routes', route, 'modulepds', modid])
    end

    # restore a changed or deleted module, return true on success
    def ecroute_mod_restore(ecid, route, modid, params={})
      ecid = ec_oid(ecid)
      modid = ecroute_modid(ecid, route, modid)
      $log.info "ecroute_mod_restore #{ecid.inspect}, #{route.inspect}, #{modid.inspect}"
      post(['engineeringchanges', ecid, 'routes', route, 'modulepds', modid, 'restore'], {})
      return @response == ''
    end

    # edit module, e.g. modulePd: <name>, stageId: 'STI'
    def ecroute_moddata(ecid, route, modid, body)
      ecid = ec_oid(ecid)
      modid = ecroute_modid(ecid, route, modid)
      $log.info "ecroute_moddata #{ecid.inspect}, #{route.inspect}, #{modid.inspect}, #{body}"
      post(['engineeringchanges', ecid, 'routes', route, 'modulepds', modid], body)
      return @response == ''
    end

  end

end
