=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author:  sfrieske, 2016-04-14

Version: 2.1.0

History:
  2016-08-22 sfrieske, refactored to streamline usage
  2016-11-29 sfrieske, new_testobject calls wait_ecm_sync for mainpds
  2019-05-09 sfrieske, cleanup

Notes:
  https://sites.google.com/a/globalfoundries.com/ecm/dev/api-2-0
=end

require 'siview'
require_relative 'session'
require_relative 'ecroute'


module ECM

  # ECM test execution support
  class Test
    attr_accessor :sm, :session, :session2, :sync_duration, :object_prefix, :object_templates  #, :ec

    def initialize(env, params={})
      @sm = params[:sm] || SiView::SM.new(env)
      @session = ECM::Session.new(env, params)
      @session2 = ECM::Session.new(env, instance: "ecm_approver.#{env}")
      @sync_duration = params[:sync_duration] || 1800
      @object_prefix = params[:object_prefix] || 'F1QA-ECM-A-'
      @object_templates = params[:templates] || {
        productgroup: "#{@object_prefix}PG0000",
        reticleset: "#{@object_prefix}RS0000",
        product: "#{@object_prefix}PRD0000.0000",
        mainpd: "#{@object_prefix}RTE0000.0000",
        modulepd: "#{@object_prefix}MOD0000.0000"
      }
    end

    def inspect
      "#<ECM::Test #{@session.inspect}>"
    end

    # SiView test objects

    # remove all test objects matching the object prefix except the templates; return true on success
    def delete_all_testobjects(params={})
      $log.info "delete_all_testobjects"
      unless @object_prefix.include?('QA-ECM-A')
        $log.warn "object prefix #{@object_prefix.inspect} is not specific enough"
        return
      end
      ret = true
      @object_templates.each_pair {|oclass, otemplate|
        onames = @sm.object_info(oclass, otemplate.sub(/0+/, '%'), exclude: otemplate).collect {|o| o.name}
        ret &= @sm.delete_objects(oclass, onames) unless onames.empty?
      }
      return ret
    end

    # create new test object in SiView, return object id on success
    def new_testobject(oclass, params={})
      n = params[:n] || 1
      $log.info "new_testobject #{oclass.inspect}, n: #{n}"
      otemplate = @object_templates[oclass]
      names = @sm.object_info(oclass, "#{otemplate.tr('0.', '')}%").collect {|oinfo|
        oinfo.name[0...oinfo.name.rindex('.')]
      }
      # create new objects from template
      name = names.sort.last
      (params[:name_offset] || 0).times {name = name.next}
      newnames = n.times.collect {name = name.next}
      newnames.map! {|e| e + '.0000'} if @object_templates[oclass].end_with?('.0000')
      #
      @sm.copy_object(oclass, otemplate, newnames, release: false) || return
      if params[:obsolete]
        @sm.obsolete_mainobject(oclass, newnames) || return
      end
      @sm.release_objects(oclass, newnames) || return
      # until immediate update has been well established
      $log.info "waiting #{@sync_duration} s for ECM synchronization"; sleep @sync_duration
      # if oclass == :mainpd
      #   wait_ecm_sync(newnames) || return
      # end
      return n == 1 ? newnames.first : newnames
    end

    def create_ecroute(routes, params={})
      return ECM::ECRoute.new(params[:session] || @session, routes: routes)
    end

    # wait for SiView route changes to sync, return true on success
    def wait_ecm_sync(routes)
      routes = [routes] if routes.kind_of?(String)
      ec = create_ecroute(routes)
      $log.info "wait_ecm_sync #{routes}  (up to #{@sync_duration} s)"
      ret = wait_for(timeout: @sync_duration, sleep: 60) {
        # if blk
        #   newec = new_ecroute(routes, session: @session2)
        #   res = blk.call(newec)
        #   newec.delete_ec
        #   res
        # else
          routes.inject(true) {|ok, route| ok && ec.ec_routedata(route)}
        # end
      }
      ec.delete
      return ret
    end


    # wait for EC status, return true on success    currently not used
    def XXwait_ec_state(ec, state, params={})
      session = params[:session] || @session
      wait_for({timeout: 120}.merge(params)) {
        ecdata = session.metadata(ec)
        $log.info "state: " + ecdata['state']
        ecdata && ecdata['state'] == state
      }
    end

  end

end
