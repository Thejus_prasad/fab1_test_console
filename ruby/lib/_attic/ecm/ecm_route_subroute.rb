=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.


Author:     sfrieske, 2016-05-30

History:
=end


# to be called from ec.rb
require_relative('ecm_ec')

module ECM
  # RouteEdit EC
  class ECRoute < ECM::EC

    # create a branch route, return object with id on success
    def add_subroute(route, opid, params={})
      body = {level: params[:level] || 0}  # attached at main pd
      opid = _opid(route, opid, rdata: params[:rdata])
      rtype = params[:rework] ? 'reworks' : 'branches'
      $log.info "add_subroute #{route.inspect}, #{opid.inspect}, #{params}"
      @session.post(body, resource: ['engineeringchanges', @ecid, 'routes', route, 'operations', opid, rtype])
    end

    # delete subroute, return true on success
    def delete_subroute(route, opid, srid, params={})
      opid = _opid(route, opid, rdata: params[:rdata])
      srid = srid['id'] if srid.kind_of?(Hash)  # pass as id or subroute object
      rtype = params[:rework] ? 'reworks' : 'branches'
      $log.info "delete_subroute #{route.inspect}, #{opid.inspect}, #{srid.inspect}, #{params}"
      @session.delete(
        resource: ['engineeringchanges', @ecid, 'routes', route, 'operations', opid, rtype, srid])
    end

    # add subroute data, e.g. {subFlowMainPd: xxx} or {joiningOpNumber: opNo}, return true on success
    def update_subroute(route, opid, srid, params={})
      opid = _opid(route, opid, rdata: params[:rdata])
      srid = srid['id'] if srid.kind_of?(Hash)  # pass as id or subroute object
      rtype = params[:rework] ? 'reworks' : 'branches'  # 1.4.3.. reworks instead of branches
      $log.info "update_subroute_data #{route.inspect}, #{opid.inspect}, #{params}"
      # must be sent one by one to avoid long compute times (??)
      body = params.clone.keep_if {|k| [:subFlowMainPd, :joiningOpNumber].member?(k)}
      res = @session.post(body,
        resource: ['engineeringchanges', @ecid, 'routes', route, 'operations', opid, rtype, srid])
      return res == ''
    end

    # note: undo with restore_operation()

  end

end
