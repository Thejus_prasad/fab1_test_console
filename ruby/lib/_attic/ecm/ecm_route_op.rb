=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.


Author:     sfrieske, 2015-12-07

History:
=end


# normally called from ec.rb
require_relative('ecm_ec')

# RouteEdit EC
module ECM
  class ECRoute < ECM::EC

    # resolve operation id from operation number, or the value passed as opNo if it contains no '.'
    def _opid(route, opNo, params={})
      return opNo['id'] if opNo.kind_of?(Hash)
      return opNo unless opNo.include?('.')  # assuming ID
      res = ecroute_operations(route, rdata: params[:rdata], opNo: opNo) || ($log.warn "no such opNo: #{opNo.inspect}"; return)
      return res['id']
    end

    # add a route operation, params after: true (default) or before: true must be passed
    # return new opid on success
    def add_operation(ecid, route, opid, params={})
      opid = _opid(route, opid, rdata: params[:rdata])
      $log.info "add_operation #{route.inspect}, #{opid.inspect}, before: #{!!params[:before]}"
      body = params.has_key?(:before) ? {addOperationBeforeOpId: opid} : {addOperationAfterOpId: opid}
      return @session.post(body, resource: ['engineeringchanges', @ecid, 'routes', route, 'operations'])
    end

    # delete an operation, return true on success
    def delete_operation(route, opid, params={})
      opid = _opid(route, opid, rdata: params[:rdata])
      $log.info "delete_operation #{route.inspect}, #{opid.inspect}"
      return @session.delete(resource: ['engineeringchanges', @ecid, 'routes', route, 'operations', opid])
    end

    # restore a changed or deleted operation, return true on success
    def restore_operation(route, opid, params={})
      opid = _opid(route, opid, rdata: params[:rdata])
      $log.info "restore_operation #{route.inspect}, #{opid.inspect}"
      res = @session.post({}, resource: ['engineeringchanges', @ecid, 'routes', route, 'operations', opid, 'restore'])
      return res == ''
    end

    # edit route operation, e.g. mandatory: true (change on module PD),
    #   mainPdScripts: {preOne: 'PCL-PRE1'}, protocolZone: 'FEOL', photolayer: 'XD' (main PD)
    #
    # return true on success
    def update_operation(route, opid, body, params={})
      opid = _opid(route, opid, rdata: params[:rdata])
      $log.info "update_operation #{route.inspect}, #{opid.inspect}, #{body}"
      res = @session.post(body, resource: ['engineeringchanges', @ecid, 'routes', route, 'operations', opid])
      return res == ''
    end

    # edit route operation UDATA, e.g. {"RouteOpSafeOp": "PRElong"} or {"RouteOpSafeOp": ''},
    #   {processDefinition: pdname}
    #
    # return true on success
    def update_operation_udata(route, opid, key, value, params={})
      opid = _opid(route, opid, rdata: params[:rdata])
      $log.info "update_operation_udata #{route.inspect}, #{opid.inspect}, #{key.inspect}, #{value.inspect}"
      body = {key: key, value: value}
      res = @session.post(body, resource: ['engineeringchanges', @ecid, 'routes', route, 'operations', opid, 'udata'])
      return res == ''
    end
  end

end
