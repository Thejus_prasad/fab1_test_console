=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.


Author:     sfrieske, 2015-12-07

History:
=end


# to be called from ec.rb
require_relative('ecm_ec')

module ECM
  # RouteEdit EC
  class ECRoute < ECM::EC
    # Route Q-time (type: 0) and lagtime (type: 1) for route (level: 0) or module (level: 1)

    # resolve qt id from RteTimelink struct or timelink object
    def _qtid(qtid)
      return qtid['id'] if qtid.kind_of?(Hash)
      return qtid.id if qtid.class.name == 'ECM::ECRoute::RteTimelink'
      return qtid  # assuming ID
    end

    # create a Q-time without actions, return timelink object on success
    def add_qtime(route, trigger, target, params={})
      body = {type: params[:type] || 0, level: params[:level] || 0,
        triggerOpNumber: trigger, targetOpNumber: target || trigger}
      tl = body[:type] == 0 ? 'qtime' : 'lagtime'
      $log.info "add_#{tl} #{route.inspect}, #{trigger.inspect}, #{target.inspect}"
      @session.post(body, resource: ['engineeringchanges', @ecid, 'routes', route, 'timelinks'])
    end

    # create a lagtime (degenerated case of Q-time)
    def add_lagtime(route, trigger, params={})
      add_qtime(route, trigger, trigger, params.merge({type: 1}))
    end

    # delete a Q-time or lagtime, return true on success
    def delete_timelink(route, qtid)
      $log.info "delete_timelink #{route.inspect}"
      @session.delete(resource: ['engineeringchanges', @ecid, 'routes', route, 'timelinks', _qtid(qtid)])
    end


    # add an action, e.g. {action: 'lagtime', contexType: 3, duration: 15}
    #
    # return action object on success
    def add_timelink_action(route, qtid, action)
      qtid = _qtid(qtid)
      action[:contextType] ||= 3  # default, 0: product, 1: productgroup, 2: technology
      $log.info "add_timelink_action #{route.inspect}"
      $log.info "  action: #{action}"
      ($log.warn "  missing action"; return) if action[:action].nil?
      ($log.warn "  missing duration"; return) if action[:duration].nil?
      @session.post(action, resource: ['engineeringchanges', @ecid, 'routes', route, 'timelinks', qtid, 'actions'])
    end

    # change action data, return true on success
    def update_timelink_action(route, qtid, aid, action)
      qtid = _qtid(qtid)
      aid = aid['id'] if aid.kind_of?(Hash)
      $log.info "update_timelink_action #{route.inspect}"
      res = @session.post(action,
        resource: ['engineeringchanges', @ecid, 'routes', route, 'timelinks', qtid, 'actions', aid])
      return res == ''
    end

    # delete an action
    def delete_timelink_action(route, qtid, aid)
      qtid = _qtid(qtid)
      aid = aid['id'] if aid.kind_of?(Hash)
      $log.info "delete_timelink_action #{route.inspect}"
      @session.delete(resource: ['engineeringchanges', @ecid, 'routes', route, 'timelinks', qtid, 'actions', aid])
    end

  end

end
