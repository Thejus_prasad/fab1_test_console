#
# ssteidte, 2015-03-03
#

require 'dbsequel'


# Access the Fast/Request database
module GF
  
  class FastRequest
    attr_accessor :db, :app_blacklist, :cr_blacklist

    CR = Struct.new(:id, :title, :requestor, :status, :longstatus, :type, :submit_time, :approve_time, :areas, :apps)
    MonthlyCRReport = Struct.new(:id, :title, :apps)
    MonthlyCRRbReport = Struct.new(:id, :title, :apps)
    Area = Struct.new(:areaid, :area, :status, :longstatus, :status_time, :contact)

    OwnCR = Struct.new(:id, :title, :requestor, :status, :longstatus, :app, :type, :approve_time, :impl_time, :contact)
    OwnMSR = Struct.new(:id, :title, :requestor, :status, :longstatus, :system, :submit_time, :approve_time)

    Organizations = {f1: 199, f8: 844, mtqa: 1125, dummy: 9999}
    StatusCodes = {open: 22, released: 23, staged: 24, closed: 35}  # applies to request status and area status
    TypeCodes = {'Standard'=>7, 'FYI'=>8, 'Emergency'=>9}

    def initialize(params={})
      @db = DB.new('fast')
      @app_blacklist = params[:app_blacklist] || [
        'APF:AM-A Jobs', 'CamLine RMS ', 'Catalyst', 'EICC', 'eCentre', 
        'iCADA RSM- Reticle Management System', 'MACS', 'OneWiki', 
        'Plan-Throughput', 'QlikView', 'Request System (RS)', 'Request System (RS) Configuration',
        'SAP PM', 'SAPPHiRE', 'SAPPHiRE Database', 'SPACE ETEST', 'Synopsys YieldManager  ', 
        'ToolView', 'TOSCA', 'traceEFP', 'UCS', 'WARP-CONF', 'XoaK'
        ]

      @cr_blacklist = params[:cr_blacklist] || [73220, 77499, 77733, 79757, 81141, 83000, 83208]
    end

    # CR query, defaults to open CRs for the specified Fab
    # pass title: '%BTF%' to search for CRs with BTF
    def crs(params={})
      # get the requests, area data is filled in from sog_id later
      qargs = []
      q = 'SELECT distinct cr.request_id, cr.change_title, cr.requestor, cr.status_id, cr.status_name, cr.request_type_name,
        cr.submit_date, cr.final_approval_date FROM v_cr_sob cr, v_selected_organization org
        WHERE cr.app_id=2 AND cr.request_id=org.request_id AND cr.request_type_id != 8'
      q, qargs = @db._q_restriction(q, qargs, 'cr.status_id', [StatusCodes[params[:rqstatus] || :open]])
      q, qargs = @db._q_restriction(q, qargs, 'cr.request_id', params[:id])
      q, qargs = @db._q_restriction(q, qargs, 'cr.change_title', "%#{params[:title]}%") if params[:title]
      q, qargs = @db._q_restriction(q, qargs, 'org.org_id', Organizations[params[:area] || :f1])
      q, qargs = @db._q_restriction(q, qargs, 'org.status_id', StatusCodes[params[:status] || :staged])
      res = @db.select(q, *qargs) || return
      return res if params[:raw]
      ret = res.collect {|r| CR.new(*r)}
      # get data for all affected areas of the selected requests, regardless of the area or status filter
      ret.each {|e| e.areas = areas(e.id)}
      # get affected applications
      ret.each {|e| e.apps = apps(e.id).flatten}
      # app filter
      ## app = params[:app]
      ## ret.delete_if {|e| e.apps.select {|a| a.include?(app)}.empty?}
      # blacklist filter
      ret.delete_if {|e| (e.apps - @app_blacklist).empty?}
      ret.delete_if {|e| @cr_blacklist.member?(e.id)}
      ret.sort {|a, b| a.id <=> b.id}
    end

    def monthly_cr_report(params={})
      # get CRs for given month
      qargs = []
      applications = []    # collecting apps
      q = "SELECT cr.request_id, cr.change_title
          FROM v_cr cr, request_history his WHERE his.request_id=cr.request_id AND cr.app_id=2
          AND his.sog_name='Fab 1'
          AND his.status_name='Production Release'
          AND cr.request_type_name!='FYI'"
      q, qargs = @db._q_restriction(q, qargs, 'his.update_date >=', (Time.new-60*60*24*31).strftime('%Y-%m-01'))
      q, qargs = @db._q_restriction(q, qargs, 'his.update_date <=',  Time.new.strftime('%Y-%m-01'))
      res = @db.select(q, *qargs) || return
      return res if params[:raw]
      ret = res.collect {|r| MonthlyCRReport.new(*r)}
      ret.each {|e| e.apps = apps(e.id).flatten; applications += e.apps} # currently not used, for creating/checking a blacklist
      ret.delete_if {|e| (e.apps - @app_blacklist).empty?}
      return ret
    end

    def monthly_cr_rollback_report(params={})
      # get rolled back CRs for given month
      qargs = []
      q = "SELECT DISTINCT cr.request_id , cr.change_title
          FROM v_cr cr, v_request_history his, v_selected_organization org
          WHERE his.request_id=cr.request_id
          AND cr.request_id = org.request_id
          AND org.status_name in('Rollback', 'Rollback in Staging')"
      q, qargs = @db._q_restriction(q, qargs, 'his.update_date >=', (Time.new-60*60*24*31).strftime('%Y-%m-01'))
      q, qargs = @db._q_restriction(q, qargs, 'his.update_date <=',  Time.new.strftime('%Y-%m-01'))
      res = @db.select(q, *qargs) || return
      return res if params[:raw]
      ret = res.collect {|r| MonthlyCRRbReport.new(*r)}
      ret.each {|e| e.apps = apps(e.id).flatten}
      ret.delete_if {|e| (e.apps - @app_blacklist).empty?}
      return ret
    end


    # area data per request
    def areas(request_id)
      q = 'SELECT org_id, org_name, status_id, status_name, status_date, contact
        FROM v_selected_organization WHERE request_id=? ORDER by org_name'
      res = @db.select(q, request_id) || return
      res.collect {|r| Area.new(*r)}
    end

    # affected applications
    def apps(request_id)
      q = 'SELECT system_name FROM v_sog WHERE org_id=0 AND sog_id IN
        (SELECT sog_id FROM v_cr_sob WHERE request_id=?)'
      res = @db.select(q, request_id) || return
    end


    # Dirk's queries

    # own CRs
    def own_crs(name, params={})
      qargs = [name]  # e.g. "%Steidten%"
      q = "select m_req_id, m_title, m_req, m_statusid, m_stat, m_app, m_type, m_fin_appr, m_end_impl, m_cont from
        (select distinct cr.request_id as m_req_id,
          cr.change_title as m_title,
          cr.requestor as m_req,
          cr.status_id as m_statusid,
          cont.status_name as m_stat,
          (select app_name from application a where a.app_id = cr.app_id) as m_app,
          cr.request_type_name as m_type,
          cr.final_approval_date as m_fin_appr,
          cont.end_imp_date as m_end_impl,
          cont.contact as m_cont
          from v_cr_sob cr, v_selected_organization cont where cont.request_id=cr.request_id
            and (contact like ?) and cont.status_name ='Production Release') A order by m_req_id desc"
      #q, qargs = @db._q_restriction(q, qargs, "LOT_ID", params[:lot])
      qargs.unshift(q)
      res = @db.select(*qargs) || return
      return res if params[:raw]
      return res.collect {|r| OwnCR.new(*r)}
    end

    # own MSRs (name is the requestor)
    def own_msrs(name, params={})
      qargs = [name] # requestor, e.g. "%Steidten%"
      q = "select n_req, n_title, n_requestor, n_stat_id, n_stat, n_sys, n_sub, n_appr from
        (select distinct msr.request_id as 'n_req',
          msr.description as n_title,
          msr.requestor as n_requestor,
          msr.status_id as n_stat_id,
          msr.status_name as n_stat,
          msr.system_name as n_sys,
          msr.submit_date as n_sub,
          msr.final_approval_date as n_appr
          from v_msr_sob msr where msr.app_id=1 and (msr.org_id > 800) and ((msr.status_id < 20)
            or (DATEDIFF(day, msr.submit_date, sysdatetime()) < 700)) and (msr.requestor like ?)
        ) A  order by n_stat_id, n_req desc"
      #q, qargs = @db._q_restriction(q, qargs, "LOT_ID", params[:lot])
      qargs.unshift(q)
      res = @db.select(*qargs) || return
      return res if params[:raw]
      return res.collect {|r| OwnMSR.new(*r)}
    end

    # ??
    def msrs2(params={})
      qargs = [params[:requestor] || '%']  # e.g. "%Steidten%"
      q = "select n_req, n_title, n_requestor, n_stat, n_sys, n_sub, n_appr, n_stat_id from
        (select distinct cast(msr.request_id as char(6)) as \"n_req\",
          substring(msr.description,1,60) as n_title,
          substring(msr.requestor,1,25) as n_requestor,
          substring(msr.status_name,1,25) as n_stat,
          substring(msr.system_name,1,20) as n_sys,
          convert(VARCHAR(19),msr.submit_date,20) as n_sub,
          convert(VARCHAR(19),msr.final_approval_date,20) as n_appr,
          msr.status_id as n_stat_id
          from v_msr_sob msr where msr.app_id=1 and (msr.org_id > 800)
            and ((msr.status_id < 20 ) or (DATEDIFF(day,msr.submit_date,sysdatetime()) < 700 ))
            and (msr.requestor like ?)) A  order by n_stat_id, n_req desc"
      #q, qargs = @db._q_restriction(q, qargs, "LOT_ID", params[:lot])
      qargs.unshift(q)
      res = @db.select(*qargs) || return
    end

    # assigned MSRs
    def assigned_msrs(name, params={})
      qargs = [name]  # e.g. "%Steidten%"
      q = "select m_color, n_req, n_title, n_requestor, n_stat, n_sys, n_sub, n_appr, n_stat_id from
        (select distinct cast(msr.request_id as char(6)) as \"n_req\",
          REPLACE(replace(substring(msr.description,1,60),char(10),' '),Char(13),' ') as n_title,
          substring(msr.requestor,1,25) as n_requestor,
          substring(msr.status_name,1,25) as n_stat,
          substring(msr.system_name,1,20) as n_sys,
          convert(VARCHAR(19),msr.submit_date,20) as n_sub,
          convert(VARCHAR(19),msr.final_approval_date,20) as n_appr,
          msr.status_id as n_stat_id,
          m_color = case
          when msr.status_id=4 and (DATEDIFF(hour,msr.final_approval_date,SYSDATETIME()) < 24) then 'yellow'
          when msr.status_id=4 and DATEDIFF(hour,msr.final_approval_date,SYSDATETIME()) > 24 then 'red'
          else 'cornsilk'
          end
          from v_msr_sob msr where msr.app_id=1 and (msr.org_id > 800)
            and ((msr.status_id < 20 ) or (DATEDIFF(day,msr.submit_date,sysdatetime()) < 700 ))
            and (msr.assigned_to like ?)) A  order by n_stat_id, n_req desc"
      #q, qargs = @db._q_restriction(q, qargs, "LOT_ID", params[:lot])
      qargs.unshift(q)
      res = @db.select(*qargs) || return
    end

  end

end
