=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.
    
Author:     ssteidte, 2014-09-09
Version:    1.1.1

History:
  2014-12-10 ssteidte,  added method to measure assigned_tasks individually
  2015-12-08 sfrieske,  minor fixes
=end

require 'warp'


Thread.abort_on_exception = false   # set to true for debugging only

# Warp Testing Support
module Warp
  # Warp test execution support
  class Test
    attr_accessor :env, :client, :pdname, :user, :tasks
    
    def initialize(env, params={})
      @env = env
      @client = HttpClient.new(env, params)
      @pdname = params[:pdname] || 'FICSQA.Flow.Test1.v1_0_0'
      @user = params[:user] || 'sfrieske@gfoundries.com' # must be member of LDAP group (FC1.APP.WARP.D.QA.GROUP1 for the default pd)
      # tasks that trigger special behavior and the data to send
      @tasks = params[:tasks] || {
        'FICSQA.Flow.Test1.Request'=>{'priority'=>'LOW'},
        'FICSQA.Flow.Test1.Approve'=>{'state'=>'DISAPPROVED'},
        'FICSQA.Flow.Test1.Specia1'=>{},
        'FICSQA.Flow.Test1.Special2'=>{},
        'FICSQA.Flow.Test1.Special3'=>{},
        'FICSQA.Flow.Test1.Special4'=>{},
      }
    end
    
    def inspect
      "#<#{self.class.name} env: #{@env}>"
    end
    
    # instantiate a process definition and complete the tasks
    #
    # return performance counters on success else nil
    def create_execute_instance(params={})
      tasks = params[:tasks] || @tasks
      timeout = params[:timeout] || 120
      idletime = params[:idletime] || 60
      $log.info "create_execute_instance tasks: #{tasks.keys.inspect}"
      tstart = Time.now
      pid = @client.start_process_instance(@pdname) || ($log.warn "error creating instance for #{@pdname}"; return)
      perf = {tstart: tstart, pid: pid, idle: 0, pstart: Time.now - tstart, tcreate: [], tlist: [], tclaim: [], tcomplete: []}
      $log.info "  pid: #{pid.inspect}"
      tasks.each_pair {|name, data|
        # wait for task to become visible
        task = nil
        wait_for(timeout: timeout, sleeptime: 2) {task = @client.instance_tasks(pid).first}
        perf[:tcreate] << @client.responsetime
        ($log.warn "  missing task #{name.inspect}"; return) unless task
        ($log.warn "  wrong task #{task.name.inspect} instead of #{name.inspect}"; return) if task.name != name
        # get list of assignable tasks
        ##@client.assignable_tasks(@user) # dummy to fill the cache TODO
        @client.assignable_tasks(@user)
        perf[:tlist] << @client.responsetime
        # claim task
        @client.claim_user_task(user, task) || ($log.warn "  error claiming task #{task.inspect}"; return)
        perf[:tclaim] << @client.responsetime
        # sleep to simulate user actions outside of WARP
        sleep idletime
        perf[:idle] += idletime
        # complete
        @client.complete_user_task(user, task, data: @tasks[task.name]) || ($log.warn "  error completing task #{task.inspect}"; return)
        perf[:tcomplete] << @client.responsetime
      }
      # verify instance is completed
      ok = wait_for(timeout: timeout, sleeptime: 2) {@client.process_instances(@pdname).find {|e| e.instance == pid}.nil?}
      perf[:pcomplete] = @client.responsetime
      perf[:pduration] = Time.now - perf[:tstart]
      ($log.warn "error completing instance #{pid}"; return) unless ok
      return perf
    end
  end


  class PerfTest < Test
    require 'fileutils'
    require 'pp'
    require 'yaml'
    require 'charts'

    attr_accessor :perfdata, :chartdata


    # delete all instances of the pd
    def pid_cleanup(pdname=@pdname)
      @client.process_instances(pdname).each {|pid| @client.delete_process_instance(pid)}
      pids = @client.process_instances(pdname)
      ($log.warn "unable to delete pids #{pids.inspect}"; return) unless pids.empty?
      return true
    end
    
    # run a series of tests with increasing number of sessions and create perf charts
    #
    # return true on successful completion
    def load_sessions(params={})
      loops = params[:loops] || 1
      numbers = params[:numbers] || [1, 2, 5, 10, 30, 50, 70, 100]
      wts = numbers.max.times.collect {Warp::Test.new(@env, pdname: @pdname, tasks: @tasks, user: (params[:user] || @user))}
      @perfdata = {}
      loops.times {|loop|
        $log.info "-- loop #{loop+1}/#{loops}" if loops > 1
        numbers.each {|n|
          $log.info "deleting old instances"
          pid_cleanup
          sleep 10
          $log.info "-- testing with #{n} sessions"
          threads = []
          @perfdata[n] = Array.new(n, nil)
          n.times {|i|
            sleep params[:shift] || 1
            threads << Thread.new(i, n) {|i, n|
              Thread.current['name'] = "wc_#{n}_%3.3d" % (i+1)
              @perfdata[n][i] = wts[i].create_execute_instance(params)
            }
          }
          $log.info "waiting for the threads to complete.."
          threads.each {|t| t.join}
        }
      }
      pid_cleanup
      perflog(params)
    end

    # run a series of tests with increasing number of tasks but only few queries and creeate perf chart
    #
    # return true on successful completion
    def load_tasklist(params={})
      timeout = params[:timeout] || 120
      numbers = params[:numbers] || [1, 2, 5, 10, 20, 50, 100, 200, 500, 1000]
      nsessions = params[:nsessions] || 20  # number of parallel queries
      otheruser = params[:otheruser] || @user
      wts = (nsessions - 1).times.collect {Warp::Test.new(@env, pdname: @pdname, tasks: @tasks, user: otheruser)}
      @perfdata = {}
      $log.info "deleting old instances"
      pid_cleanup
      pids = []
      numbers.each {|n|
        $log.info "-- testing with #{n} tasks"
        pids << @client.start_process_instance(@pdname) while pids.size < n
        $log.info "waiting for task creation"
        wait_for(timeout: timeout, sleeptime: 2) {
          pids.inject(true) {|ok, pid| ok && !@client.instance_tasks(pid).empty?}
        }
        # start parallel session threads, each asking 10 times for assignable tasks, to generate load
        threads = wts.collect {|wt|
          sleep 0.2
          Thread.new {
            10.times {wt.client.assignable_tasks(wt.user)}
          }
        }
        # call assignable_tasks from the use client and store durations
        tstart = Time.now
        durations = 10.times.collect {
          @client.assignable_tasks(@user)
          @client.responsetime
        }
        @perfdata[n] = [{tstart: tstart, tlist: durations}]
        threads.each {|t| t.join}
      }
      pid_cleanup
      tag = "#{params.delete(:tag) || ''}_#{nsessions}sessions"
      perflog({keys: [:tlist], tag: tag, xlegend: 'number of tasks'}.merge(params))
    end


    def perflog(params={})
      $log.info "creating performance log"
      ok = true
      @chartdata = {}
      keys = params[:keys] || [:pduration, :pstart, :pcomplete, :tcreate, :tlist, :tclaim, :tcomplete]
      keys.each {|key|
        @chartdata[key] = {average: [], max: []}
        $log.info "collecting data for #{key}"
        @perfdata.each_pair {|n, perfs|
          if perfs.include?(nil)
            ok = false
            if params[:tolerant]
              perfs = perfs.compact
              $log.warn "  perfdata for #{n} sessions have only #{perfs.size} entries"
            else
              perfs = []
            end
          end
          if perfs.empty?
            ok = false
            durations = [-1]
            $log.warn "  no perfdata for #{n} sessions"
          else
            durations = perfs.collect {|perf| perf[key]}.flatten
          end
          @chartdata[key][:average] << [n, durations.average.round(1)]
          @chartdata[key][:max] << [n, durations.max.round(1)]
        }
      }
      return ok if params[:charts] == false
      #
      tag = params[:tag] || ''
      tag += '_' unless tag.empty?
      logdir = "log/warp_perf_#{tag}#{Time.now.utc.iso8601.gsub(':', '-')}"
      FileUtils.mkdir_p(logdir)
      File.write "#{logdir}/perfdata_raw.yaml", @perfdata.to_yaml
      File.write "#{logdir}/perfdata.yaml", @chartdata.to_yaml
      # 
      @chartdata.each_pair {|key, data|
        $log.info "creating chart for #{key}"
        ch = Chart::XY.new(data[:average], name: 'average')
        ch.add_series(data[:max], name: 'maximum')
        if key == :pduration
          title = "Warp Performance (#{tag}CompleteWorkflow)"
          fname =  "#{logdir}/chart_overall.png"
        else
          title = "Warp Performance (#{tag}#{key.to_s.capitalize})"
          fname =  "#{logdir}/chart_#{tag}#{key.to_s}.png"
        end
        title += ' ' + params[:comment] if params[:comment]
        xlegend = params[:xlegend] || 'number of sessions'
        ch.create_chart(title: title, x: xlegend, y: 'duration (s)', legend: true, dotsize: 6, export: fname)
      }
      return ok
    end
  end

end
