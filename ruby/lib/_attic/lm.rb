=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.


Author:   Steffen Steidten, 2013-11-18
Version:  1.0.0
History:
=end

require 'builder'
require 'fileutils'
require 'time'
require 'misc'
require 'charts'
require 'rqrsp'
require 'lmtrans'
require 'lmdb'
require 'lmtest'
  
# LotManager/RTD Integration Tests 
module LotManager
  
  class Exerciser
    attr_accessor :env, :lotsumclients, :lmreqclients, :tstart, :tend, :nlotsumthreads, :nlmreqthreads, :lotsumthreads, :lmreqthreads
    
    def initialize(env, params={})
      @env = env
      nlotsumthreads = (params[:nlotsumthreads] or params[:n] or 6)
      nlmreqthreads = (params[:nlmreqthreads] or params[:m] or 1)
      @lotsumclients = nlotsumthreads.times.collect {LotManager::Transformer.new(@env, params)}
      @lmreqclients = nlmreqthreads.times.collect {LotManager::Transformer.new(@env, params)}
      @lotsumthreads = []
      @lmreqthreads = []
      @requesttypes = ["update_lotsummary", "delete_lotsummary", "update_lmrequest", "update_recipes", "update_selected_wafer", "update_wishes"]
    end
    
    def load_test(params={})
      lotsum_report_times = []
      lmreq_report_times = []
      lotsumdel_report_times = []
      updwish_report_times = []
      updrec_report_times = []
      updselwaf_report_times = []
      @tstart = Time.now
      $log.info ""
      @lotsumclients.each_with_index {|c,i|
        @lotsumthreads << Thread.new {
          Thread.current['name'] = "LotSummary_#{i}"
          c.lotsummary_loop(params.merge({:threadnum=>i}))
          lotsum_report_times << c.lotsum_report_times
          lotsumdel_report_times << c.lotsumdel_report_times
        }
      }
      @lmreqclients.each_with_index {|c,i|
        @lmreqthreads << Thread.new {
          Thread.current['name'] = "LmRequest_#{i}"
          c.lmrequest_loop(params.merge({:threadnum=>i}))
          lmreq_report_times << c.lmreq_report_times
          updwish_report_times << c.updwish_report_times
          updrec_report_times << c.updrec_report_times
          updselwaf_report_times << c.updselwaf_report_times
        }
      }
      @lotsumthreads.each {|t| t.join}
      lotsum_report_times.flatten!
      lotsumdel_report_times.flatten!
      $log.info "LotSummary updates completed sending reports (Update: #{lotsum_report_times.size}, Delete: #{lotsumdel_report_times.size}), waiting for Update-Threads to complete"
      @lmreqthreads.each {|t| t.join}
      lmreq_report_times.flatten!
      updselwaf_report_times.flatten!
      updrec_report_times.flatten!
      updwish_report_times.flatten!
      $log.info "Updates completed sending reports (LMRequests: #{lmreq_report_times.size}, UpdateSelectedWafers: #{updselwaf_report_times.size}, UpdateRecipes: #{updrec_report_times.size}, UpdateWishes: #{updwish_report_times.size})"
      @tend = Time.now
      #
      # log results
      perflog = (params[:perflog] or "log/lmtrans_perf_#{Time.now.utc.iso8601.gsub(':', '-')}/run_times.log")
      ext = File.extname(perflog)
      dirname, fname = File.dirname(perflog), File.basename(perflog, ext)
      fname = "#{dirname}/#{fname}_1#{ext}"
      FileUtils.mkdir_p(File.dirname(fname))
      lsum = lotsum_report_times.collect {|s| s.nil? ? -1 : s}
      lsumdel = lotsumdel_report_times.collect {|s| s.nil? ? -1 : s}
      lmreq = lmreq_report_times.collect {|r| r.nil? ? -1 : r}
      updwish = updwish_report_times.collect {|r| r.nil? ? -1 : r}
      updrec = updrec_report_times.collect {|r| r.nil? ? -1 : r}
      updselwaf = updselwaf_report_times.collect {|r| r.nil? ? -1 : r}
      open(fname, "wb") {|fh|
        fh.write "update_lotsummary #{lsum.join(' ')}\n"
        fh.write "delete_lotsummary #{lsumdel.join(' ')}\n"
        fh.write "update_lmrequest #{lmreq.join(' ')}\n"
        fh.write "update_recipes #{updwish.join(' ')}\n"
        fh.write "update_selected_wafer #{updrec.join(' ')}\n"
        fh.write "update_wishes #{updselwaf.join(' ')}\n"
      }
      LotManager.performance_charts(fname, requesttypes: @requesttypes) unless params[:nocharts]
    end
  end
  
  # create charts from a performance log file
  def LotManager.performance_charts(perflog, params={})
    $log.info "LotManager.performance_charts #{perflog.inspect}"
    ext = File.extname(perflog)
    dirname, fname = File.dirname(perflog), File.basename(perflog, ext)
    lines = File.readlines(perflog)
    return unless lines and lines.size > 1
    requesttypes = params[:requesttypes]
    # columns: values (durations per request)
    rqdata = {}
    lines.each {|l|
      ff = l.split
      requesttype = ff.shift
      ff.collect! {|f| f.to_f}
      rqdata[requesttype] = ff
    }
    requesttypes.each {|type|
      ch = Chart::Histogram.new(rqdata[type])
      notes = ["count: #{rqdata[type].size} (#{rqdata[type].count(-1)} timeouts)", 
        "avg: #{rqdata[type].average.round(1)}  +- #{rqdata[type].standard_deviation.round(1)}"]
      ch.create_chart(title: type, bins: 50, x: "runtime(s)", export: "#{dirname}/#{fname}_#{type}_hist.png", notes: notes)
    }
  end
    
end  


# attic methods from lmtrans moved here for conservation (sfrieske, 2019-01-24)
#

def lotsummary_loop(params={})
  $log.info "lotsummary_loop #{params.inspect}"
  duration = duration_in_seconds((params[:duration] or 3600)).to_i
  interval = (params[:lotsuminterval] or 600).to_f # * (params[:nlotsumthreads] or 1).to_f
  lotsummessages = ((params[:lotsummessages].to_f/params[:nlotsumthreads].to_f).ceil or (12.to_f/(params[:nlotsumthreads].to_f).ceil or 1))
  loops = (duration.to_f/interval).ceil
  tnext = Time.now + interval
  $log.info "#{__method__}: duration=#{duration}, interval=#{interval}, loops=#{loops}"
  loops.times {|i|
    $log.info "#{__method__}: Loop #{i}"
    if i > 0
      count = (lotsummessages.to_f/2.to_f).ceil
      count.times {|j|
        run = nil
        data = create_lotsumdata(params.merge(pdnum: j))
        #s = Time.now
        #res =
        delete_lotsummary(data: data)
        #run = (Time.now - s) if res
        Thread.exclusive {@lotsumdel_report_times << @mq_response.responsetime}    #run
      }
      count.times {|j|
        data = create_lotsumdata(params.merge(:pdnum=>j))
        update_lotsummary(data: data)
        Thread.exclusive {@lotsum_report_times << @mq_response.responsetime}
      }
      count.times {|j|
        data = create_lotsumdata(params.merge(:pdnum=>j+count, :chgdata=>true))
        update_lotsummary(data: data)
        Thread.exclusive {@lotsum_report_times << @mq_response.responsetime}
      }
    else
      lotsummessages.times {|j|
        data = create_lotsumdata(params.merge(:pdnum=>j))
        update_lotsummary(data: data)
        Thread.exclusive {@lotsum_report_times << @mq_response.responsetime}
      }
    end
    # sync loops with sample time
    tsleep = tnext - Time.now
    if (i + 1) < loops
      sleep tsleep if tsleep > 0
    else
      break
    end
    tnext += interval
  }
  return
end

def lmrequest_loop(params={})
  $log.info "#{__method__} #{params.inspect}"
  duration = duration_in_seconds((params[:duration] or 60)).to_i
  # * 4 because of 4 messages in each loop
  interval = (params[:lmreqinterval] or 0.5).to_f * (params[:nlmreqthreads] or 10).to_f * 4.to_f
  loops = (duration/interval).ceil
  tnext = Time.now + interval
  $log.info "lmrequest_loop: duration=#{duration}, interval=#{interval}, loops=#{loops}"
  loops.times {|i|
    $log.info "-- Loop #{i+1}/#{loops}"
    # sync loops with sample time
    # update_wishes, update_lmrequests
    tsleep = tnext - Time.now
    sleep tsleep if tsleep > 0
    tnext += interval
    update_lmrequests
    Thread.exclusive {@lmreq_report_times << @mq_response.responsetime}
    # update_recipes
    update_recipes
    Thread.exclusive {@updrec_report_times << @mq_response.responsetime}
    # update_selected_wafer
    update_selected_wafer
    Thread.exclusive {@updselwaf_report_times << @mq_response.responsetime}
    # update_wishes
    update_wishes
    Thread.exclusive {@updwish_report_times << @mq_response.responsetime}
  }
  return
end

# creates x context data datasets in the database
def push_lotsummary_data_base_into_database(params={})
  threadnum = params[:threadnum] || 2
  equipments = params[:eqpnum] || 10
  pds = params[:pdnum] || 100
  lotsummessagesize = params[:lotsummessagesize] || 1000
  sum = threadnum * equipments * pds * lotsummessagesize
  threads = []
  $log.info "#{__method__}: Threads #{threadnum}, EQPs #{equipments}, PDs #{pds}, message size #{lotsummessagesize} = #{sum} records (to be inserted/updated)"

  log = false
  if log
    filllog = "log/lm_databasefill.log"
    ext = File.extname(filllog)
    dirname, fname = File.dirname(filllog), File.basename(filllog, ext)
    fname = "#{dirname}/#{fname}_1#{ext}"
    FileUtils.mkdir_p(File.dirname(fname))
    open(fname, 'wb') {|fh|
      threadnum.times {|thread|
        threads << Thread.new(thread) {|t|
          Thread.current['name'] = "th%02d" % t
          equipments.times {|eqpnum|
            pds.times {|pdnum|
              Thread.exclusive {
                 lotsummessagesize.times {|i|
                   fh.write "QALot#{i} QAPD%04d QAEQP1%02d%04d\n" % [pdnum, thread, eqpnum]
                   @entries << "QALot#{i} QAPD%04d QAEQP1%02d%04d\n" % [pdnum, thread, eqpnum]
                 }
              }
            }
          }
        }
      }
      threads.each {|t| t.join}
    }
  else
    threadnum.times {|thread|
      threads << Thread.new(thread) {|t|
        Thread.current['name'] = "th%02d" % t
        equipments.times {|eqpnum|
          pds.times {|pdnum|
            s = Time.now
            update_lotsummary(:data=>create_lotsumdata(params.merge({:lotsummessagesize=>lotsummessagesize, :pdnum=>"%04d" % pdnum, :threadnum=>"%02d%04d" % [thread, eqpnum]})))
            $log.info "Thread: #{thread}; EQP: #{eqpnum}; pdnum: #{pdnum} ... took: #{(Time.now - s)}"
            sleep 10
          }
        }
      }
    }
    threads.each {|t| t.join}
  end
end

def create_lotsumdata(params={})
  size = (params[:lotsummessagesize] or 1)
  pdnum = (params[:pdnum] or 1)
  threadnum = (params[:threadnum] or 1)
  data = []
  now = Time.at(Time.now.to_i)
  size.times {|i|
    if params[:chgdata] == true
      data << LotSummary.new(
        LmContext.new(nil, "QALot#{i}", "QAPD#{pdnum}", "QAEQP1#{threadnum}", now),
        LmPclBase.new(nil, nil, "QAEQP11", now, "QA ToolWish memo X", 33, now, "QA ToolScore memo X" , 4, "MANUAL", now),
        [LmPclChamber.new(nil, nil, "QACH1", "QAChamberWish", now, "QA ChamberWish memo X")],
        [LmRecipe.new(nil, nil, "QA_TYPEX", "QA-RECIPE-X")],
        [LmPrio.new(nil, nil, "QAAPP1", "QATARGETPD1", 2, now, now, now, "QA Prioritazation memo X")],
        [LmReservation.new(nil, nil, 9966991, "QAAPP1", "Mainframe", "QAEQP1", 0, 1, now, "QA Rsv memo X")],
        [LmLimitation.new(nil, nil, 9966991, "QAAPP1", "Mainframe", "QAEQP1", 0, 1, now, "QA Limit memo X")],
        [LmRqRequired.new(nil, nil, "QAAPPX", "Limitation")])
    else
      data << LotSummary.new(
        LmContext.new(nil, "QALot#{i}", "QAPD#{pdnum}", "QAEQP1#{threadnum}", now),
        LmPclBase.new(nil, nil, "QAEQP11", now, "QA ToolWish memo", 33, now, "QA ToolScore memo" , 4, "MANUAL", now),
        [LmPclChamber.new(nil, nil, "QACH1", "QAChamberWish", now, "QA ChamberWish memo")],
        [LmRecipe.new(nil, nil, "QA_TYPE1", "QA-RECIPE-1")],
        [LmPrio.new(nil, nil, "QAAPP1", "QATARGETPD1", 2, now, now, now, "QA Prioritazation memo")],
        [LmReservation.new(nil, nil, 9966991, "QAAPP1", "Mainframe", "QAEQP1", 0, 1, now, "QA Rsv memo")],
        [LmLimitation.new(nil, nil, 9966991, "QAAPP1", "Mainframe", "QAEQP1", 0, 1, now, "QA Limit memo")],
        [LmRqRequired.new(nil, nil, "QAAPP1", "Limitation")])
    end
  }
  return data
end

