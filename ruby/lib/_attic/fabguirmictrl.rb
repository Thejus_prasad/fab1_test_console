=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

@author:     ssteidte, 2012-03-21
@version:    1.2.0

History
  2012-12-07 ssteidte,  work on all instances unless explicitly told
  2013-02-04 ssteidte,  support new service ports
=end

Dir["lib/FabGUI*jar"].each {|f| require f}

require 'misc'
require 'rmiregistry'
require 'rqrsp'

# FabGUI control via RMI
module FabGUIRMI
  
  AppPorts = {
    "EI3DBAHandlerAudit"=>15010,
    "SchedulerNewPluginAudit"=>15002,
    "Shipping1ERPPluginAudit"=>15003,
    "XFactorAudit"=>15010,
    "ErfSaServer"=>15011,
  }
  

  class FabGuiRmiError < StandardError
    def initialize(method, msg)
      super("#{method} - #{msg}")
    end
  end


  # FabGUI instance control via RMI
  class InstanceControl
    attr_accessor :host, :reg, :ctrl, :app_port, :svc_name

    # establish connection to RMI controller indicated by host
    def initialize(host, params={})
      @host = host
      port = params[:port] || 15000
      @reg = RMIRegistry.new(host: host, port: port)
      @ctrl = @reg.lookup('RMIController')
      @app_port = params[:app_port] || port
      @svc_name = params[:svc_name]
    end
    
    def inspect
      "#<#{self.class.name}, host: #{@host}, app port: #{@app_port}, svc name: #{@svc_name}>"
    end

    def services
      apps = @ctrl.srv_list_services2
      apps.get_no_a.times.collect {|i| apps.get_app(i)}.sort
    end

    def app_methods(app)
      mm = @ctrl.srv_list_methods2("//#{@reg.host}:#{@app_port}/#{app}")
      mm.get_no_m.times.collect {|i| mm.get_method_name(i)}.sort
    end

    def execute(app, m)
      m += '()' unless m.end_with?')'
      @result = @ctrl.srv_execute_method2("TestUser", "//#{@reg.host}:#{@app_port}/#{app}", m)
      raise FabGuiRmiError.new(m, @result.comment) if @result.fail?
      return @result.get_result
    end
    
    def dump
      execute(@svc_name, "dump")
    end
    
    def runtime_values(pname=nil)
      res = execute(@svc_name, "listRuntimeValues")
      return nil if res.nil?
      ret = Hash[res.split("\n")[2..-1].collect {|l| l[3..-1].split(" = ")}.sort]
      return pname ? ret[pname] : ret
    end

    # set a parameter <pname> to <pvalue> and verify
    #
    # return true on success, nil if :verify=>false
    def set_runtime_value(pname, pvalue, params={})
      execute(@svc_name, "setRuntimeValue(L/java/lang/String; #{pname},L/java/lang/String; #{pvalue})V")
      return nil if params[:verify] == false
      return runtime_values(pname) == pvalue
    end
    
    # set parameters, provided in a hash, and verify
    #
    # return true on success
    def set_runtime_values(values, params={})
      values.each_pair {|k,v| set_runtime_value(k, v, :verify=>false)}
      return nil if params[:verify] == false
      return verify_runtime_values(values)
    end
    
    # verify runtime values, provided as property=>expexted_value hash
    #
    # return true on success
    def verify_runtime_values(values)
      $log.info "verify_runtime_values #{values.inspect} # #{@host}"
      res = runtime_values
      ok = true
      values.each {|k,v|
        ($log.warn "  wrong property #{k.inspect}=>#{res[k].inspect}, expected #{v.inspect}"; ok=false) if res[k] != v 
      }
      return ok
    end
  end

  # control all instances in an environment
  class Control
    attr_accessor :env, :hosts, :settings, :instances
    
    def initialize(env, params={})
      @env = env
      _params = _config_from_file("fabgui.#{env}", 'etc/remotehosts.conf') || {}
      params = _params.merge(params)
      @hosts = params[:host]
      @hosts = @hosts.split if @hosts.kind_of?(String)
      @settings = params
      raise "no hosts defined for env #{env.inspect}" unless @hosts
      # set app_port from service
      svc = params[:svc_name]
      @settings[:app_port] = AppPorts[svc] if svc && !params.has_key?(:app_port)
      # create control instances
      @instances = @hosts.collect {|host| InstanceControl.new(host, @settings)}
    end
    
    def inspect
      "#<#{self.class.name}, env: #{@env} #{@hosts.inspect}>"
    end

    def method_missing(meth, *args, &block)
      @instances.collect {|i| i.send(meth, *args, &block)}
    end
    
    def respond_to?(meth)
      @instances.inject(true) {|s,i| s and i.methods.member?(meth)}
    end
    
    # set a parameter <pname> to <pvalue> in all instances and verify
    #
    # return true on success
    def set_runtime_value(pname, pvalue, params={})
      @instances.inject(true) {|s,i| s and i.set_runtime_value(pname, value, params)}
    end
    
    # set parameters, provided in a hash, in all instances and verify
    #
    # return true on success
    def set_runtime_values(values, params={})
      @instances.inject(true) {|s,i| s and i.set_runtime_values(values, params)}
    end
    
    # verify runtime values, provided as property=>expexted_value hash, in all instances
    #
    # return true on success
    def verify_runtime_values(values)
      @instances.inject(true) {|s,i| s and i.verify_runtime_values(values)}
    end
  end
  
  
  # Shipping settings
  class EMS < Control
    def initialize(env, params={})
      super(env, {svc_name: "ErfSaServer"}.merge(params))
    end
  end
end
