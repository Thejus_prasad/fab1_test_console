=begin
SPACE Testing Support

(c) Copyright 2012 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

    
Author: Daniel Steger, 2012-07-05

History:
  2017-01-23 cstenzel, switch to rqrsp_mq
  2017-01-24 dsteger,  added support for RMS 6.1 API
=end

require 'xmlhash'
require 'misc'
require 'rqrsp_mq'
require 'base64'

# RMS API (access by MQ)
module RmsAPI
  class RmsError < StandardError
  end

  RobObject = Struct.new(:rmshandle, :context, :dataitems, :linked_contexts, :shared_params, :compare_info, :is_variant) do
    def initialize(*args)
      super(*args)
      set_handle(rmshandle) if rmshandle
    end

    def set_handle(new_handle)
      # handle consists of <robid> ; <version> [; <variant>[; ...]]
      items = new_handle.split('2c')
      self.rmshandle = "#{items[0]}2c2d31"
      if items.size > 2
        self.rmshandle += "2c#{items[2]}"
        self.is_variant = true
      end
      $log.info "#{__method__}: new handle #{self.rmshandle}"
    end
  end

  Service = com.camline.rm.services
  RilService = com.globalfoundries.ril.jaxb.customdata
  DataItem = Struct.new(:value, :tags, :options)

  #Perform the RMS login
  module RmsMethods
    def login(params={})
      pw = params[:cleartext] ? @password : @password.unpack('m')[0]
      login_req = RmsAPI::Service.RmsLoginRequest.new
      login_req.set_user(@user)
      login_req.set_password(pw)
      @csid = _api_call(login_req, RmsAPI::Service.RmsLoginResponse.java_class).get_sid()
    end

    # xml to class
    def _xml_to_class(java_class, text)
      org.exolab.castor.xml.Unmarshaller.unmarshal(
        java_class,
        java.io.StringReader.new(text)
      )
    end

    # class to xml
    def _class_to_xml(obj)
      swr = java.io.StringWriter.new
      org.exolab.castor.xml.Marshaller.marshal(obj, swr)
      swr.to_string
    end

    # Logout
    def logout()
      logout_req = RmsAPI::Service.RmsLogoutRequest.new
      logout_req.set_sid(@csid)
      logout_req.set_tid(__method__)
      _api_call(logout_req, RmsAPI::Service.RmsLogoutResponse.java_class)
    end

    # Get objects in a certain registration (filter hash, select columns as array)
    def get_reg_entries(params={})
      $log.info "#{__method__} #{params.inspect}"
      req = RmsAPI::Service.RmsGetRegistrationEntriesRequest.new
      req.set_sid(@csid)
      req.set_tid(__method__)
      req.set_registration_name((params[:registration] || 'RCP_PROD'))            
      req.set_include_rms_handle((params[:no_rmshandle] || true))
      req.set_filter_key(_hash_to_filterkeys((params[:filters] || {})).to_java(RmsAPI::Service.FilterKey))
      req.set_selection_key((params[:columns] || []).to_java(java.lang.String))
      res = _api_call(req, RmsAPI::Service.RmsGetRegistrationEntriesResponse.java_class)
      res.get_registration_entry.map {|e| e.get_entry_value.map {|x| x} }
    end

    def _hash_to_filterkeys(hash)
      hash.map do |k,v|
        f = RmsAPI::Service.FilterKey.new
        f.name = k
        f.value = v
        f
      end
    end

    # Find an object by certain context keys in the rms repository
    def find_object_with_data(keys, params={})
      req = RmsAPI::Service.RmsFindObjectWithDataRequest.new
      req.set_sid(@csid)
      req.set_tid(__method__)
      req.set_registration_name((params[:registration] || 'RCP_PROD'))
      req.set_with_keys(!!params[:with_keys])
      req.add_rms_key_value_set(_hash_to_key_value_set(keys))

      res = _api_call(req, RmsAPI::Service.RmsFindObjectWithDataResponse.java_class)
      return nil unless res.get_rms_find_object_with_data_response_sequence_count == 1
      item = res.get_rms_find_object_with_data_response_sequence[0].get_rms_find_object_with_data_response_sequence_item()
      #return RobObject.new(
      #  item.get_rms_handle.get_content,
      #  item.get_rms_key_value.collect, #Hash[*[item.get_rms_key_value.collect {|kv| [kv.get_name, kv.get_value]}]],
      #  item.get_rms_data_item.collect #{|d| [d.get_name, d.get_rms_string.collect {|s| s.get_content}]}
      #)
      RobObject.new(
        item.get_rms_handle.get_content,
        Hash[item.get_rms_key_value.map { |kv| [kv.get_name, kv.get_value] }],
        _dataitems_to_hash(item.get_rms_data_item),
        _links_to_hash(item.get_rms_data_item),
        _sp_to_hash(item.get_rms_data_item)
      )
    end

    # Find an object by certain context keys in the rms repository
    def find_object_with_data_exp(keys, params={})
      rob_objs = find_object_list(keys, params)
      return nil unless rob_objs.count == 1
      get_data(rob_objs.first)
    end

    # Find an object by certain context keys in the rms repository
    def find_object_list(keys, params={})
      $log.info "#{__method__} #{keys.inspect}, #{params.inspect}"
      req = RmsAPI::Service.RmsFindObjectListRequest.new
      req.set_sid(@csid)
      req.set_tid(__method__)
      req.set_registration_name((params[:registration] || 'RCP_PROD'))
      req.set_with_keys(!!params[:with_keys])
      req.set_exact_match(!!params[:exact_match])
      req.set_rms_key_values_set(_hash_to_keyvaluessets(keys).to_java(RmsAPI::Service.RmsKeyValuesSet))

      res = _api_call(req, RmsAPI::Service.RmsFindObjectListResponse.java_class)
      return if res.get_rms_handle_set.empty?
      res.get_rms_handle_set[0].get_rms_handle_set_item.map do |i|
        ctx = Hash[i.get_rms_key_value.map { |kv| [kv.get_name, kv.get_value] }] if params[:with_keys]
        RobObject.new(i.get_rms_handle, ctx)
      end
    end

    # Get object details by rmshandle
    def get_object_details(rmshandle, params={})
      $log.info "#{__method__} #{rmshandle.inspect}, #{params.inspect}"
      req = RmsAPI::Service.RmsGetObjectDetailsRequest.new
      req.set_sid(@csid)
      req.set_tid(__method__)
      req.set_rms_identifier(_rmshandle_to_identifier(RobObject.new(rmshandle)))

      res = _api_call(req, RmsAPI::Service.RmsGetObjectDetailsResponse.java_class)
      item = res.get_rms_get_object_details_response_sequence
      RobObject.new(
        item.get_rms_handle,
        _get_context_with_kind_keys(item.get_context_with_kind_keys),
        _dataitems_to_hash(item.get_rms_data_item),
        _links_to_hash(item.get_rms_data_item),
        _sp_to_hash(item.get_rms_data_item)
      )
    end

    def _hash_to_keyvaluessets(keys)
      keys = [keys] unless keys.is_a?(Array)
      keys.map do |key|
        keyset = RmsAPI::Service.RmsKeyValuesSet.new
        key.each_pair.map do |k, v|
          values = RmsAPI::Service.RmsKeyValues.new
          values.set_name(k)
          values.add_value(v.to_s)
          keyset.add_rms_key_values(values)
        end
        keyset
      end
    end

    def _hash_to_keysets(hash)
      hash.each_pair.map do |k, v|
        keyset = RmsAPI::Service.RmsKeySet.new
        keyset.set_name(k)
        values = RmsAPI::Service.RmsKeyValues.new
        if v.is_a?(Hash)
          v.each_pair do |k2, v2|
            values = RmsAPI::Service.RmsKeyValues.new
            values.set_name(k2)
            values.add_value(v2.to_s)
            keyset.add_rms_key_values(values)
          end
        else
          values.set_name(k)
          values.add_value(v.to_s)
          keyset.add_rms_key_values(values)
        end
        keyset
      end
    end

    def _hash_to_key_value_set(keys)
      rms_set = RmsAPI::Service.RmsKeyValueSet.new
      keys.each_pair do |k,v|
        rms_kv = RmsAPI::Service.RmsKeyValue.new
        rms_kv.set_name(k)
        rms_kv.set_value(v)
        rms_set.add_rms_key_value(rms_kv)
      end
      rms_set
    end

    def _hash_to_keyvalues(hash)
      hash.each_pair.map do |k, v|
        value = RmsAPI::Service.RmsKeyValue.new
        value.set_name(k)
        value.set_value(v)
        value
      end
    end

    # Get definied key sets
    def get_keyset(params={})
      req = RmsAPI::Service.RmsGetKeySetRequest.new
      req.set_sid(@csid)
      req.set_tid(__method__)
      req.set_key_set_name(params[:name]) if params[:name]

      res = _api_call(req, RmsAPI::Service.RmsGetKeySetResponse.java_class)   
      res.get_key_set.map(&:get_name)
    end

    # Get the context keys of a ROB
    def get_keys(rob)
      req = RmsAPI::Service.RmsGetKeysRequest.new
      req.set_sid(@csid)
      req.set_tid(__method__)
      req.set_rms_identifier(_rmshandle_to_identifier(rob))
      #req.set_ctx_name(name)

      res = _api_call(req, RmsAPI::Service.RmsGetKeysResponse.java_class)
      rob.context = _get_context_with_kind_keys(res.get_context_with_kind_keys)
      rob
    end

    def _get_context_with_kind_keys(context_with_kind_keys)
      context = {}
      context_with_kind_keys.each do |k|
        val = {}
        k.get_rms_key_with_kind_values.each { |v| val[v.get_name] = v.get_value[0] }
        # special handling for context variables with same name as value key
        context[k.get_name] = if val.count == 1 && val.keys.first == k.get_name
                                val[k.get_name]
                              else
                                val
                              end
      end
      context
    end

    # Change the context keys of a ROB
    def put_keys(rob, context)
      req = RmsAPI::Service.RmsPutKeysRequest.new
      req.set_sid(@csid)
      req.set_tid(__method__)
      req.set_rms_identifier(_rmshandle_to_identifier(rob))
      req.set_rms_key_set(_hash_to_keysets(context).to_java(RmsAPI::Service.RmsKeySet))
      #return String.from_java_bytes(_object_to_xml(req))
      res = _api_call(req, RmsAPI::Service.RmsPutKeysResponse.java_class)
      res.get_rms_handle
    end

    # Get the data items of a ROB
    def get_data(rob, params={})
      req = RmsAPI::Service.RmsGetDataRequest.new
      req.set_sid(@csid)
      req.set_tid(__method__)
      req.set_rms_identifier(_rmshandle_to_identifier(rob))
      req.set_only_variant_components(!!params[:variant_only])
      req.set_patch_bodies(!!params[:patch_bodies])
      #req.set_ctx_name(name)

      res = _api_call(req, RmsAPI::Service.RmsGetDataResponse.java_class)
      dataitem = res.get_rms_get_data_response_sequence.get_rms_data_item
      rob.dataitems = _dataitems_to_hash(dataitem)
      rob.linked_contexts = _links_to_hash(dataitem)
      rob.shared_params = _sp_to_hash(dataitem)
      rob
    end

    # Put the data items of a ROB
    def put_data(rob, dataitems, params={})
      registration = (params[:registration] || 'RCP_PROD')
      #
      req = RmsAPI::Service.RmsPutDataRequest.new
      req.set_sid(@csid)
      req.set_tid(__method__)
      req.set_rms_identifier(_rmshandle_to_identifier(rob))
      req.set_rms_data_item(_hash_to_dataitems(dataitems).to_java(RmsAPI::Service.RmsDataItem))

      _add_links(req, registration, params)
      _add_sp_links(req, registration, params)

      res = _api_call(req, RmsAPI::Service.RmsPutDataResponse.java_class)
      get_data(RobObject.new(res.get_rms_handle))
    end

    # get existing variants of a rob
    def get_variants(rob, params = {})
      req = RmsAPI::Service.RmsGetVariantsRequest.new
      req.set_sid(@csid)
      req.set_tid(__method__)
      req.set_rms_identifier(_rmshandle_to_identifier(rob))

      res = _api_call(req, RmsAPI::Service.RmsGetVariantsResponse.java_class)
      res.get_rms_key_values_type_variant.map do |v|
        rob = RobObject.new(v.get_rms_handle, _get_context_with_kind_keys(v.get_context_with_kind_keys))
        get_data(rob, params)
      end
    end

    # define new variant by context keys
    def put_variant_keys(rob, context)
      req = RmsAPI::Service.RmsPutVariantKeysRequest.new
      req.set_sid(@csid)
      req.set_tid(__method__)
      req.set_rms_identifier(_rmshandle_to_identifier(rob))
      req.set_rms_key_value(_hash_to_keyvalues(context).to_java(RmsAPI::Service.RmsKeyValue))

      res = _api_call(req, RmsAPI::Service.RmsPutVariantKeysResponse.java_class)
      RobObject.new(res.get_rms_handle, context)
    end

    # removes a variant from the rob
    def del_variant(rob)
      req = RmsAPI::Service.RmsDeleteVariantRequest.new
      req.set_sid(@csid)
      req.set_tid(__method__)
      req.set_rms_identifier(_rmshandle_to_identifier(rob))

      res = _api_call(req, RmsAPI::Service.RmsDeleteVariantResponse.java_class)
      RobObject.new(res.get_rms_handle)
    end

    def _links_to_hash(dataitems)
      lobj = dataitems.find {|item| item.get_name == 'RecipeLink'}
      _extract_links(lobj)
    end

    def _sp_to_hash(dataitems)
      lobj = dataitems.find {|item| item.get_name == 'SharedParameterLink'}
      _extract_links(lobj)
    end

    def _extract_links(lobj)
      if lobj
        # sort by index to preserve order
        links = lobj.get_data_item_type_choice.get_rms_base64_binary.sort_by(&:get_idx)
        links.map do |b|
          link = _xml_to_class(RmsAPI::Service.RmsLinkInfo.java_class, String.from_java_bytes(b.get_content))
          h = {}
          link.get_rms_key_value.each { |kv| h[kv.get_name] = kv.get_value }
          h
        end
      else
        []
      end
    end

    # get all robs linking to the specified rob, propagation is enabled by default
    def get_linked_object(rob, params = {})
      req = RmsAPI::Service.RmsGetLinkedObjectRequest.new
      req.set_sid(@csid)
      req.set_tid(__method__)
      req.set_rms_identifier(_rmshandle_to_identifier(rob))
      req.set_ignore_propagation_marker(!!params[:propagated])
      req.setMaxNumberOfSelectedROBs(10) # unlimited
      res = _api_call(req, RmsAPI::Service.RmsGetLinkedObjectResponse.java_class)
      res.get_rms_get_linked_object_response_sequence.map do |link|
        l = link.get_rms_get_linked_object_response_sequence_item
        rob = RobObject.new(l.get_rms_handle)
        rob.context = _get_context_with_kind_keys(l.get_context_with_kind_keys)
        rob.dataitems = _dataitems_to_hash(l.get_rms_data_item)
        rob.linked_contexts = _links_to_hash(l.get_rms_data_item)
        rob
      end
    end

    def validate(robs)
      req = RmsAPI::Service.RmsValidateRequest.new
      req.set_sid(@csid)
      req.set_tid(__method__)
      robs = [robs] unless robs.is_a?(Array)
      req.set_rms_identifier(robs.map { |rob| _rmshandle_to_identifier(rob) }.to_java(RmsAPI::Service.RmsIdentifier))
      res = _api_call(req, RmsAPI::Service.RmsValidateResponse.java_class)
      res.get_rms_validate_response_sequence.map do |val_item|
        v = val_item.get_rms_validate_response_sequence_item
        [v.get_rms_handle, v.get_validation]
      end
    end

    def _dataitems_to_hash(dataitems)
      res = {}
      dataitems.each do |item|
        data = item.get_data_item_type_choice
        $res = item
        val = nil
        if data.get_rms_boolean_count.positive?
          val = data.get_rms_boolean.sort_by(&:get_idx).map(&:get_content)
        elsif data.get_rms_integer_count.positive?
          val = data.get_rms_integer.sort_by(&:get_idx).map(&:get_content)
        elsif data.get_rms_string_count.positive?
          val = data.get_rms_string.sort_by(&:get_idx).map(&:get_content)
        elsif data.get_rms_double_count.positive?
          val = data.get_rms_double.sort_by(&:get_idx).map(&:get_content)
        elsif data.get_rms_date_time_count.positive?
          val = data.get_rms_date_time.sort_by(&:get_idx).map { |i| Time.at(i.get_content / 1000.0) }
        elsif data.get_rms_base64_binary_count.positive?
          val = data.get_rms_base64_binary.map { |b| String.from_java_bytes(b.get_content) }
        elsif data.get_xml_content_count.positive?
          val = data.get_xml_content.map { |x| Base64.decode64(x.get_any_object) }
        else
          $log.warn "Not supported type: #{item.get_name.inspect}"
        end
        next unless val # skip assigment if no value is available
        val = val[0] if val.count == 1
        tags = item.get_tag.to_a if item.get_tag_count.positive?
        # the same option can have multiple values
        options = {}
        item.get_option.each do |i|
          if options.key?(i.get_name)
            options[i.get_name] << i.get_value
          else
            options[i.get_name] = [i.get_value]
          end
        end
        res[item.get_name] = DataItem.new(val, tags, options)
      end
      res
    end

    # Convert hash to data items
    #  - special handling for limits
    #  - if value is equal type class, empty value is set
    def _hash_to_dataitems(item_hash)
      item_hash.each_pair.map do |name, itemdata|
        value = itemdata.is_a?(RmsAPI::DataItem) ? itemdata.value : itemdata
        item = RmsAPI::Service.RmsDataItem.new
        item.set_name(name)
        data = RmsAPI::Service.DataItemTypeChoice.new
        item.set_data_item_type_choice(data)
        # Special handling for comparison
        if name == 'ProductionCompareInfo'
          value.each_with_index do |(ref_name, include), i|
            binary = RmsAPI::Service.RmsBase64Binary.new
            binary.set_idx(i.to_s)
            compare_info = RmsAPI::Service.RmsCompareInfo.new
            compare_info.set_include(include)
            compare_info.set_ref_name(ref_name)
            binary.set_content(_object_to_xml(compare_info).to_java_bytes)
            data.add_rms_base64_binary(binary)
          end
          # override name since this is the standard phrase
          item.set_name('ProductionCompareInfo')
        # ECSV data
        elsif name == 'ECSVData'
          to_xml = RmsAPI::Service.XmlContent.new
          to_xml.set_any_object(Base64.encode64(value).to_s)
          to_xml.set_idx('0')
          data.add_xml_content(to_xml)
        # Special handling for control info items
        elsif name.end_with?('_ci')
          binary = RmsAPI::Service.RmsBase64Binary.new
          binary.set_idx('0')
          c_info = RmsAPI::Service.RmsControlInfo.new
          if value.is_a?(Array)
            min, max = value
            range = RmsAPI::Service.Range.new
            range.set_min(min)
            range.set_max(max)
            c_info.set_range(range)
          elsif value.is_a?(Set)
            set = RmsAPI::Service.RmsSet.new
            set.set_value(value.to_a.to_java(:String))
            c_info.set_rms_set(set)
          else
            $log.error "unkonwn value type for #{name}"
          end
          binary.set_content(_object_to_xml(c_info).to_java_bytes)
          data.add_rms_base64_binary(binary)
        elsif value.is_a?(Array)
          value.each_with_index { |v, i| _add_value(data, i, v) }
        else
          _add_value(data, 0, value)
        end
        if itemdata.is_a?(RmsAPI::DataItem)
          $log.info "#{name}: #{itemdata.inspect}"
          itemdata.tags.each { |tag| item.add_tag(tag) } if itemdata&.tags
          itemdata.options.each_pair { |k, v| v.each { |i| item.add_option(_add_option(k, i)) } } if itemdata&.options
        end
        item
      end
    end

    def _add_value(datatype, index, value)
      sindex = index.to_s
      if value.is_a?(Integer)
        s = RmsAPI::Service.RmsInteger.new
        s.set_idx(sindex)
        s.set_content(value)
        datatype.add_rms_integer(s)
      elsif value.is_a?(Float)
        s = RmsAPI::Service.RmsDouble.new
        s.set_idx(sindex)
        s.set_content(value)
        datatype.add_rms_double(s)
      elsif value.is_a?(Time)
        s = RmsAPI::Service.RmsDateTime.new
        s.set_idx(sindex)
        s.set_content(value.to_i * 1000) # milliseconds since epoche
        datatype.add_rms_date_time(s)
      elsif value.is_a?(TrueClass) || value.is_a?(FalseClass)
        s = RmsAPI::Service.RmsBoolean.new
        s.set_idx(sindex)
        s.set_content(value)
        datatype.add_rms_boolean(s)
      elsif value == Integer # init with empty value
        s = RmsAPI::Service.RmsInteger.new
        s.set_idx(sindex)
        datatype.add_rms_integer(s)
      elsif value == String # init with empty value
        s = RmsAPI::Service.RmsString.new
        s.set_idx(sindex)
        datatype.add_rms_string(s)
      else
        s = RmsAPI::Service.RmsString.new
        s.set_idx(sindex)
        s.set_content(value)
        datatype.add_rms_string(s)
      end
    end

    def _add_option(key, value)
      o = RmsAPI::Service.Option.new
      o.set_name(key)
      o.set_value(value)
      o
    end

    def _xml_to_object(java_class, bytes)
      org.exolab.castor.xml.Unmarshaller.unmarshal(
        java_class,
        java.io.InputStreamReader.new(java.io.ByteArrayInputStream.new(bytes), 'UTF-8')
      )
    end

    def _object_to_xml(obj)
      bos = java.io.ByteArrayOutputStream.new
      org.exolab.castor.xml.Marshaller.marshal(
        obj,
        java.io.OutputStreamWriter.new(bos)
      )
      bos.close
      bos.to_s
    end

    # Create a new recipe based on a ROBDEF
    def create_object(robdef, context, dataitems, params={})
      $log.info "#{__method__} #{robdef.rmshandle}" #, #{context.inspect}, #{dataitems.inspect}, #{params.inspect}"
      context['OperationMode'] = (context['OperationMode'] || 'RecipeCompare') # or "RecipeCompare"
      context['ToolRecipeName'] = (context['ToolRecipeName'] || '/test/qa/recipe')
      obj_name = (params[:name] || "qa test recipe #{Time.now.to_i}")
      context['builtin'] = { 'object_name' => obj_name }

      req = RmsAPI::Service.RmsCreateObjectRequest.new
      req.set_sid(@csid)
      req.set_tid(__method__)
      req.set_object_type(params[:object_type] || 'Recipe') # "Recipe", "Recipe_definition", "Metadata"
      req.set_target_name(params[:target_name] || 'Production')
      req.set_target_type(params[:target_type] || 'Production')
      req.set_work_branch_name("#{@user}_#{Time.now.iso8601}")

      registration = (params[:registration] || 'RCP_PROD') #

      req.set_rms_identifier(_rmshandle_to_identifier(robdef))

      req.set_rms_key_set(_hash_to_keysets(context).to_java(RmsAPI::Service.RmsKeySet))

      req.set_rms_data_item(_hash_to_dataitems(dataitems).to_java(RmsAPI::Service.RmsDataItem))

      if _add_body(req, params)
        # Add compare info for Body
        item = RmsAPI::Service.RmsDataItem.new
        binary = RmsAPI::Service.RmsBase64Binary.new
        binary.set_idx('0')
        compare_info = RmsAPI::Service.RmsCompareInfo.new
        compare_info.set_include(true)
        compare_info.set_ref_name('Body')
        binary.set_content(_object_to_xml(compare_info).to_java_bytes)
        item.set_name('ProductionCompareInfo')
        data = RmsAPI::Service.DataItemTypeChoice.new
        data.add_rms_base64_binary(binary)
        item.set_data_item_type_choice(data)
        req.add_rms_data_item(item)
      end

      # Add links
      _add_links(req, registration, params)

      # Add SPs
      _add_sp_links(req, registration, params)

      res = _api_call(req, RmsAPI::Service.RmsCreateObjectResponse.java_class)
      RobObject.new(res.get_rms_handle, context, dataitems, params[:linked_contexts], params[:sp_contexts])
    end

    # Add the recipe body if available
    def _add_body(req, params)
      body = nil
      if params.key?(:recipe_body)
        body = params[:recipe_body]
      elsif params.key?(:recipe_file)
        body = File.open(params[:recipe_file], 'rb', &:read)
      end
      if body
        item = RmsAPI::Service.RmsDataItem.new
        binary = RmsAPI::Service.RmsBase64Binary.new
        binary.set_idx('0')
        binary.set_content(body.to_java_bytes)
        item.set_name('Body')
        data = RmsAPI::Service.DataItemTypeChoice.new
        data.add_rms_base64_binary(binary)
        item.set_data_item_type_choice(data)
        req.add_rms_data_item(item)
        return true
      else
        return false
      end
    end

    # Add link dataitem if available
    def _add_links(req, registration, params={})
      linked_contexts = params[:linked_contexts]
      if linked_contexts && linked_contexts.count > 0
        _set_links(req,registration, linked_contexts, 'RecipeLink', params)     
      end
    end

    # Add sp link dataitem if available
    def _add_sp_links(req, registration, params={})
      sp_contexts = params[:sp_contexts]
      if sp_contexts && sp_contexts.count > 0
        _set_links(req, registration, sp_contexts, 'SharedParameterLink', params)
      end
    end

    def _set_links(req, registration, linked_contexts, link_name, params)
      item = RmsAPI::Service.RmsDataItem.new
      item.set_name(link_name)
      data = RmsAPI::Service.DataItemTypeChoice.new
      linked_contexts = [linked_contexts] unless linked_contexts.is_a?(Array)
      linked_contexts.each_with_index do |linked_context,i|
        binary = RmsAPI::Service.RmsBase64Binary.new
        binary.set_idx(i.to_s)
        link = RmsAPI::Service.RmsLinkInfo.new
        link.set_registration_name(registration)
        link.set_check(params[:checklinks] || true)
        link.set_ignore(params[:ignorelinks] || false)
        link.set_rms_key_value(_hash_to_keyvalues(linked_context).to_java(RmsAPI::Service.RmsKeyValue))
        binary.set_content(_object_to_xml(link).to_java_bytes)
        data.add_rms_base64_binary(binary)
      end
      item.set_data_item_type_choice(data)
      req.add_rms_data_item(item)
    end

    # Create a new workable version of a ROB
    def create_work_copy(rob, new_context, new_dataitems, params={})
      req = RmsAPI::Service.RmsCreateWorkCopyRequest.new
      req.set_sid(@csid)
      req.set_tid(__method__)
      req.set_target_name((params[:target_name] || 'Production'))
      req.set_target_type((params[:target_type] || 'Production'))
      registration = (params[:registration] || 'RCP_PROD')
      req.set_work_name("#{@user}_#{Time.now.iso8601}")
      req.set_rms_identifier(_rmshandle_to_identifier(rob))
      req.set_rms_key_set(_hash_to_keysets(new_context).to_java(RmsAPI::Service.RmsKeySet))
      req.set_rms_data_item(_hash_to_dataitems(new_dataitems).to_java(RmsAPI::Service.RmsDataItem))
      _add_body(req, params)      
      _add_links(req, registration, params)
      _add_sp_links(req, registration, params)
      res = _api_call(req, RmsAPI::Service.RmsCreateObjectResponse.java_class)
      new_rob = RobObject.new(res.get_rms_handle)
      get_data(new_rob)
      get_keys(new_rob)
      new_rob
    end

    def del_data(rob, dataitems)
      req = RmsAPI::Service.RmsDelDataRequest.new
      req.set_sid(@csid)
      req.set_tid(__method__)
      req.set_rms_identifier(_rmshandle_to_identifier(rob))
      req.set_data_item_name(dataitems.to_java(:String))
      
      res = _api_call(req, RmsAPI::Service.RmsDelDataResponse.java_class)
      rob.rmshandle = res.get_rms_handle
      get_data(rob)
    end

    def _rmshandle_to_identifier(rob)
      identifier = RmsAPI::Service.RmsIdentifier.new
      identifier.set_rms_handle(rob.rmshandle)
      identifier
    end

    def _update_robhandles(robs, res)
      unless robs.count == res.get_rms_handle_count
        $log.error " rob count does not match #{robs.count} expected, but #{res.get_rms_handle_count} found"
        return []
      end
      robs.each_with_index{|rob,i| rob.set_handle(res.get_rms_handle[i])}
      robs
    end

    # Delete an inwork rob
    def delete_work(robs)
      robs = [robs] unless robs.kind_of?(Array)
      $log.info "#{__method__}: #{robs.collect {|rob| rob.rmshandle}}"
      req = RmsAPI::Service.RmsDeleteWorkRequest.new
      req.set_sid(@csid)
      req.set_tid(__method__)
      req.set_rms_identifier(robs.map{|rob| _rmshandle_to_identifier(rob)}.to_java(RmsAPI::Service.RmsIdentifier))
      _api_call(req, RmsAPI::Service.RmsDeleteWorkResponse.java_class)
    end

    def start_approval(robs, params={})
      robs = [robs] unless robs.kind_of?(Array)
      $log.info "#{__method__}: #{robs.collect {|rob| rob.rmshandle}}, #{params}"
      req = RmsAPI::Service.RmsStartApprovalRequest.new
      req.set_sid(@csid)
      req.set_tid(__method__)
      req.set_comment('QA TEST start approval')
      req.set_override(true) unless params[:no_override]
      req.set_rms_identifier(robs.map{|rob| _rmshandle_to_identifier(rob)}.to_java(RmsAPI::Service.RmsIdentifier))

      res = _api_call(req, RmsAPI::Service.RmsStartApprovalResponse.java_class)
      _update_robhandles(robs, res)
    end

    def approve(robs, params={})
      robs = [robs] unless robs.kind_of?(Array)
      $log.info "#{__method__}: #{robs.collect {|rob| rob.rmshandle}}, #{params}"

      state = (params[:state] || 'yes') #no or abort
      state = RmsAPI::Service.types.RmsApproveRequestResultType.const_get(state.upcase)

      req = RmsAPI::Service.RmsApproveRequest.new
      req.set_sid(@csid)
      req.set_tid(__method__)
      req.set_comment('QA TEST approve')
      req.set_rms_identifier(robs.map{|rob| _rmshandle_to_identifier(rob)}.to_java(RmsAPI::Service.RmsIdentifier))
      req.set_result(state)

      res = _api_call(req, RmsAPI::Service.RmsApproveResponse.java_class)
      _update_robhandles(robs, res)
    end

    def disapprove(robs, params={})
      robs = [robs] unless robs.kind_of?(Array)
      $log.info "#{__method__}: #{robs.collect {|rob| rob.rmshandle}}, #{params}"

      req = RmsAPI::Service.RmsDisapproveRequest.new
      req.set_sid(@csid)
      req.set_tid(__method__)
      req.set_comment('QA TEST approve')
      req.set_rms_identifier(robs.map{|rob| _rmshandle_to_identifier(rob)}.to_java(RmsAPI::Service.RmsIdentifier))

      res = _api_call(req, RmsAPI::Service.RmsDisapproveResponse.java_class)
      _update_robhandles(robs, res)
    end

    def release(robs, params={})
      robs = [robs] unless robs.kind_of?(Array)
      $log.info "#{__method__}: #{robs.collect {|rob| rob.rmshandle}}, #{params}"
    
      req = RmsAPI::Service.RmsReleaseRequest.new
      req.set_sid(@csid)
      req.set_tid(__method__)
      req.set_comment('QA TEST release')
      req.set_rms_identifier(robs.map{|rob| _rmshandle_to_identifier(rob)}.to_java(RmsAPI::Service.RmsIdentifier))
      
      res = _api_call(req, RmsAPI::Service.RmsReleaseResponse.java_class)      
      _update_robhandles(robs, res)
    end

    def deactivate(robs, params={})
      robs = [robs] unless robs.kind_of?(Array)
      $log.info "deactivate ## robs: #{robs.collect {|rob| rob.rmshandle}}, #{params}"
    
      req = RmsAPI::Service.RmsDeactivateRequest.new
      req.set_sid(@csid)
      req.set_tid(__method__)
      req.set_comment('QA TEST deactivation')
      req.set_rms_identifier(robs.map{|rob| _rmshandle_to_identifier(rob)}.to_java(RmsAPI::Service.RmsIdentifier))
      
      res = _api_call(req, RmsAPI::Service.RmsDeactivateResponse.java_class)      
      _update_robhandles(robs, res)
    end

    # Retrieves the available onhold reasons
    def onhold_reasons()
      req = RmsAPI::Service.RmsGetOnHoldReasonsRequest.new
      req.set_sid(@csid)
      req.set_tid(__method__)
      #
      res = _api_call(req, RmsAPI::Service.RmsGetOnHoldReasonsResponse.java_class)      
      res.get_on_hold_reason.map {|r| r}
    end
    
    # Set the ROB onhold
    # available reasons are DEFAULT, SPC_PROBLEM, TOOL_PROBLEM
    def set_onhold(robs, hold_reasons, params={})
      robs = [robs] unless robs.kind_of?(Array)
      $log.info "set_onhold ## robs: #{robs.collect {|rob| rob.rmshandle}}, #{hold_reasons.inspect}, #{params}"
      reasons = onhold_reasons().map {|r|
        if hold_reasons.member?(r.get_name)
          r.set_on_hold_flag(true)
        else
          r.set_on_hold_flag(false)
        end
        r
      }.to_java(RmsAPI::Service.OnHoldReason)
      
      req = RmsAPI::Service.RmsOnHoldRequest.new
      req.set_sid(@csid)
      req.set_tid(__method__)
      req.set_comment('QA TEST hold')
      req.set_on_hold_reason_item(robs.map {|rob| 
          ri = RmsAPI::Service.OnHoldReasonItem.new
          ri.set_rms_identifier(_rmshandle_to_identifier(rob))
          ri.set_on_hold_reason(reasons)
          ri
        }.to_java(RmsAPI::Service.OnHoldReasonItem)
      )
      
      res = _api_call(req, RmsAPI::Service.RmsOnHoldResponse.java_class)
      _update_robhandles(robs, res)
    end

    # Set flags: 'OnHold', 'MarkForArchive', 'MarkForDelete'
    def set_flags(rob, flagname, enabled, params={})
      #robs = [robs] unless robs.kind_of?(Array)
      $log.info "set_flags ## robs: #{rob.collect {|rob| rob.rmshandle}}, #{flagname.inspect}, #{enabled.inspect}, #{params}"
      flag = RmsAPI::Service.types.Flags.const_get(flagname.upcase)
      req = RmsAPI::Service.RmsSetFlagsRequest.new
      req.set_sid(@csid)
      req.set_tid(__method__)
      req.set_comment('QA TEST set flags')
      req.set_flag_name(flag)
      req.set_flag_value(enabled)
      req.set_rms_identifier(_rmshandle_to_identifier(rob))
      
      res = _api_call(req, RmsAPI::Service.RmsSetFlagsResponse.java_class)      
      rob.set_handle(res.rms_handle)
      rob
    end

    # Marks the robs for deletion
    def mark_for_deletion(robs)
      robs = [robs] unless robs.kind_of?(Array)
      robs.collect {|rob| set_flags(rob, 'MarkForDelete', true)}
    end

    def import(xml_data, params={})
      $log.info "#{__method__} <files>, #{params.inspect}"
      xml_data = [xml_data] unless xml_data.kind_of?(Array)
      
      req = RmsAPI::Service.RmsImportRequest.new
      req.set_sid(@csid)
      req.set_tid(__method__)
      req.set_stick_version((params[:stick_version] or false))
      xml_data.each do |d|
        req.add_import_data(d.to_java_bytes)
      end
      
      res = _api_call(req, RmsAPI::Service.RmsImportResponse.java_class)
      res.get_rms_handle.collect {|h| RobObject.new(h)}
    end

    # Export a given rob/robdef 
    def export(robs, path, params={})
      robs = [robs] unless robs.kind_of?(Array)
      $log.info "export ## robs: #{robs.collect {|rob| rob.rmshandle}}, #{path.inspect}, #{params}"
      links = (params[:follow_links] || false)
      structure = (params[:structure] || true)

      req = RmsAPI::Service.RmsExportRequest.new
      req.set_sid(@csid)
      req.set_tid(__method__)
      req.set_follow_links(links)
      req.set_with_structure(structure)
      req.set_rms_identifier(robs.map{|rob| _rmshandle_to_identifier(rob)}.to_java(RmsAPI::Service.RmsIdentifier))

      res = _api_call(req, RmsAPI::Service.RmsExportResponse.java_class)
      res.get_export_data.each_with_index do |d,i|
        _rob = robs[i]
        get_keys(_rob) unless _rob.context
        obj_name = _rob.context.has_key?('object_name') ? _rob.context['object_name'] : _rob.context['builtin']['object_name']
        name = "#{_rob.context['SoftRev']}-#{obj_name}.xml"
        filename = "#{path}/#{name}"
        File.open(filename, 'w') { |fh| fh.write(d) }
        $log.info " #{filename} written."
      end
    end

    def _create_recipe_trigger_command(eqp, command, params = {})
      cha = (params[:chamber] || '')
      com_tag = (params[:com_tag] || "#{command}Request")
      com_tag = com_tag[0, 1].downcase + com_tag[1..-1]
      cxml = Builder::XmlMarkup.new
      cxml.instruct!
      cxml.tag! com_tag, 'xmlns' => 'http://www.amd.com/schemas/rms-customdata.xsd' do |t|
        t.userID @user if %w[RecipeUpload RecipeDownload].member?(command)
        t.equipmentID eqp
        t.moduleIdentifier cha
        t.toolRecipeName params[:rcp_name] if params[:rcp_name]
      end
      cxml.target!
    end

    # available commands: RecipeList, RecipeDownload, RecipeUpload, RecipeDelete
    def trigger_transfer(eqp, rob, params = {})
      command = (params[:command] || 'RecipeList')

      req = RmsAPI::Service.RmsRecipeTransferTriggerRequest.new
      req.set_sid(@csid)
      req.set_tid(__method__)
      req.set_command(command)
      req.set_rms_identifier(_rmshandle_to_identifier(rob))

      cdata = RmsAPI::Service.CustomData.new
      cdata.set_any_object(Base64.encode64(_create_recipe_trigger_command(eqp, command, params)))
      req.set_custom_data(cdata)

      res = _api_call(req, RmsAPI::Service.RmsRecipeTransferTriggerResponse.java_class)
      res
      # return res.get_rms_recipe_transfer_trigger_response_sequence
    end

    # Get the status of a trigger transfer command
    def trigger_transfer_status(callback, params = {})
      wait_time = (params[:wait_time] || 60)

      req = RmsAPI::Service.RmsGetRecipeTransferStatusRequest.new
      req.set_sid(@csid)
      req.set_tid(__method__)
      req.set_call_back_id(callback)
      req.set_waiting_time(wait_time)

      res = nil
      done = wait_for(timeout: 60) do
        res = _api_call(req, RmsAPI::Service.RmsGetRecipeTransferStatusResponse.java_class)
        res.get_status != 'Pending'
      end
      raise RmsError, 'timeout in trigger transfer' unless done
      res
    end

    # Trigger transfer recipe list
    def trigger_recipe_list(eqp, rob, _params = {})
      resp = trigger_transfer(eqp, rob, command: 'RecipeList')
      resp = resp.get_rms_recipe_transfer_trigger_response_sequence.get_custom_data.get_any_object.unpack('m')[0]
      return xml_to_hash(resp)[:recipeListResponse][:recipeName] if resp
      raise RmsError, 'no data found'
    end

    # Trigger transfer upload command
    def trigger_recipe_upload(eqp, rob, rcp_name, params = {})
      resp1 = trigger_transfer(eqp, rob, params.merge(command: 'RecipeUpload', rcp_name: rcp_name))
      _callback(resp1)
    end

    def _callback(resp1, params = {})
      resp2 = trigger_transfer_status(resp1.get_call_back_id)
      if resp2.get_status == 'Done'
        if params[:custom_data]
          cust_data = resp1.get_rms_recipe_transfer_trigger_response_sequence.get_custom_data.get_any_object.unpack('m')[0]
          return xml_to_hash(cust_data) if cust_data
          raise RmsError, 'no data found'
        else
          return RmsAPI::RobObject.new(resp2.get_rms_handle)
        end
      else
        raise RmsError, resp2.get_status_comment
      end
    end

    # Trigger transfer download command
    def trigger_recipe_download(eqp, rob, rcp_name, params = {})
      resp1 = trigger_transfer(eqp, rob, params.merge(command: 'RecipeDownload', rcp_name: rcp_name))
      _callback(resp1)
    end

    # Trigger transfer delete command
    def trigger_recipe_delete(eqp, rob, rcp_name, params = {})
      resp1 = trigger_transfer(eqp, rob, params.merge(command: 'RecipeDelete', rcp_name: rcp_name))
      _callback(resp1)
    end

    # Uploads and compares one recipe
    def upload_compare(rmshandle, rcp_body, params = {})
      $log.info "#{__method__} #{rmshandle.inspect}, #{params.inspect}"

      req = RmsAPI::Service.RmsUploadCompareRequest.new
      req.set_sid(@csid)
      req.set_tid(__method__)
      req.set_compare(true)
      req.set_validate(true)
      rid = RmsAPI::Service.RmsIdentifier.new
      rid.set_rms_handle(rmshandle)
      req.set_rms_identifier(rid)
      _add_body(req, recipe_body: rcp_body)
      req.set_compare_info_name(['ProductionCompareInfo'].to_java(java.lang.String))
      res = _api_call(req, RmsAPI::Service.RmsUploadCompareResponse.java_class)
      s = res.get_rms_upload_compare_response_sequence
      val_res = (s.get_rms_upload_compare_validation_result.count == 0)
      unless val_res
        $log.warn(' validation errors:')
        s.get_rms_upload_compare_validation_result.each do |v|
          $log.warn "#{v.get_data_item_name}: #{v.get_message}"
        end
      end
      diff_res = (s.get_rms_upload_compare_diff_det_result.count == 0)
      unless diff_res
        $log.warn(' differences:')
        s.get_rms_upload_compare_diff_det_result.each do |diffs|
          $log.warn "    #{diffs.get_data_item}:"
          diffs.get_diff_item_descr.each do |d|
            $log.warn "      #{d.get_val_left} | #{d.get_val_right}"
          end
        end
      end
      val_res & diff_res
    end

    # Trigger transfer verify EC/SV data
    def trigger_ecsv_verify(eqp, ecsv, params = {})
      $log.info "#{__method__} #{eqp.inspect}, #{ecsv.rmshandle}, #{params.inspect}"
      resp1 = trigger_transfer(eqp, ecsv, params.merge(command: 'ECSVVerify', com_tag: 'verifyECSVDataAsXMLRequest'))
      _callback(resp1, custom_data: true)
    end

    # Trigger transfer upload EC/SV data from tool
    def trigger_ecsv_upload(eqp, ecsv, params = {})
      $log.info "#{__method__} #{eqp.inspect}, #{ecsv.rmshandle}, #{params.inspect}"
      resp1 = trigger_transfer(eqp, ecsv, params.merge(command: 'ECSVUpload', com_tag: 'GetECSVDataAsXMLRequest'))
      _callback(resp1)
    end

    def terminate
      logout
      @session&.close
    end
    alias close terminate
    alias disconnect terminate
  end

  # RMS Interface using CORBA
  class Session
    attr_accessor :env, :ds, :user, :password, :cleartext, :props, :src, :session, :csid, :responsetime
    include XMLHash
    include RmsMethods

    def initialize(env, params={})
      @env = env
      configfile = (params[:config] || 'etc/rms.conf')
      _params = _config_from_file(env, configfile)
      if _params
        params = _params.merge(params)
      else
        $log.warn "entry #{env.inspect} not found in config file #{configfile}"
      end
      libfiles = 'lib/rms/*jar'
      $log.info "loading lib files #{libfiles.inspect}"
      Dir[libfiles].each {|f| require f}
      include_class 'com.camline.asf.frame.ServerConnectionWrapper'

      @props = (params[:props] || "etc/rms/#{@env}.server.properties")
      @session = ServerConnectionWrapper.new(@props)
      @timeout = (params[:timeout] || 120)
      @user = params[:user]
      @password = params[:password]
      login(params)
    end

    # do the rms api call and handle errors
    def _api_call(obj, response_class)
      start = Time.now
      res = @session.xml_call(obj, response_class, @timeout)
      @responsetime = Time.now - start
      exp = res.get_rms_exception
      if exp && exp.has_error_code
        if exp.error_code == -172 && exp.application_error_code == 2
          login
        else
          text = "completed=#{exp.completion_status} code=#{exp.error_code} appcode=#{exp.application_error_code} text=#{exp.standard_error_text}"
          text += " #{exp.error_arguments.map { |arg| arg }.join(', ')}"
          raise RmsError, text
        end
      end
      res
    end
  end

  # RMS interface using MQ
  class SessionMQ < RequestResponse::MQ
    attr_accessor :env, :ds, :user, :password, :cleartext, :src, :csid
    include XMLHash
    include RmsMethods

    def initialize(env, params={})
      @env = env
      @retry = 0
      super("rms.#{env}", params.merge({:correlation=>:msgid, :use_jms=>true, :mquser=>''}))


      configfile = (params[:config] or 'etc/rms.conf')
      _params = _config_from_file(env, configfile)
      if _params
        params = _params.merge(params)
      else
        $log.warn "entry #{env.inspect} not found in config file #{configfile}"
      end
      libfiles = 'lib/rms/*jar'
      $log.info "loading lib files #{libfiles.inspect}"
      Dir[libfiles].each {|f| require f}
      
      @user = params[:user]
      @password = params[:password]
      login(params)
    end

    # do the rms api call and handle errors
    def _api_call(obj, response_class)
      xml = send_receive(_class_to_xml(obj), timeout: 120)
      ($log.error 'timeout occured'; return nil) unless xml
      res = _xml_to_class(response_class, xml)
      exp = res.get_rms_exception()
      if exp && exp.has_error_code
        if exp.error_code == -172 && exp.application_error_code == 2
          $log.error "login failed with error #{exp.error_code}"
          @retry += 1
          if @retry > 3
            raise RmsError, 'Retries exceeded'
          else
            login()
          end
        else
          text = "completed=#{exp.completion_status.to_s} code=#{exp.error_code} appcode=#{exp.application_error_code} text=#{exp.standard_error_text}"
          text += " #{exp.error_arguments.collect {|arg| arg}.join(', ')}"
          raise RmsError, text
        end
      else
        @retry = 0
      end            
      res
    end
  end

end

#rob = rms.find_object_with_data( {'Department'=>'LIT', 'EquipmentType'=>'ALC-TEL', 'RecipeType'=>'WaferFlow', 'state'=>'ACTIVE'}, :registration=>'ROBDEF_ALL', :with_keys=>true)


#>> rms.create_object rob, 'testbla3', { 'RecipeNameSpaceID'=>'bla', 'RecipeName'=>'test' }, {'CoatOnly'=>'no', 'ModuleCombination'=>'PM1', 'APCRecipeContext'=>'none', '__purpose__APCRecipeContext'=>'Tuning', '__purpose__CoatOnly'=>'Tuning'}, 'testcasedata/recipes/body.dat', :linked_context=>{'ToolRecipeName'=>'Pump/017/POR/N9-TPI6-2_8-2_8'}

#=> "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<rmsPutKeysRequest sid=\"asds\" xmlns=\"http://www.camline.com/rm4/services\"><rmsIdentifier><rmsHandle>xxxxx</rmsHandle></rmsIdentifier><context name=\"builtin\"><key name=\"builtin\"><value>test</value><value>object_typetest</value></key></context></rmsPutKeysRequest>"
