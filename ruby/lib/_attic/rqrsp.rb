=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author:     ssteidte, 2012-01-30
Version:    1.3.5

History:
  2012-07-19 ssteidte,  use new xml_to_hash
  2013-05-27 dsteger,   added json support
  2013-11-14 ssteidte,  use the unified mqjms only for MQ
  2014-06-12 ssteidte,  unified HTTP#_submit_request with _x_json_request, still returning nil on timeouts etc.
  2014-06-12 ssteidte,  HTTP: read and store cookie if provided
  2015-02-24 ssteidte,  _send_receive returns nil if read timeout <= 0
  2015-12-07 sfrieske,  HTTP: added @basic_auth, allows user and password to be used otherwise if basic_auth: false
  2015-12-08 sfrieske,  HTTP: added handling of Authorization header
  2016-04-15 sfrieske,  added DELETE request handling
  2016-08-24 sfrieske,  moved _service_response from xmlhash.rb here
=end

require 'xmlhash'


# Basic communication for SOAP over HTTP, MQ and MQJMS
module RequestResponse

  # Basic communication for SOAP over HTTP, MQ and MQJMS
  class Common
    include XMLHash
    attr_accessor :instance, :params, :timeout, :request, :response

    def initialize(instance, params)
      if instance
        @instance = instance
        _params = _config_from_file(instance, params[:config]) || {}
        params = _params.merge(params)
      end
      @params = params
      @timeout = @params[:timeout] || 300
    end

    def _submit_request(s, params)
      $log.warn "_submit_request not implemented"
      return nil
    end

    # send string s, receive the reply message
    #
    # return the parsed response as Hash or nil on error
    def _send_receive(s, params={})
      export = params[:export]
      File.binwrite(export, s) if export
      return s if params[:nosend]
      ok = _submit_request(s, params)
      ($log.debug {"no response"}; return) unless ok # something went wrong or timeout: 0
      return _service_response(@response, params)
    end

    def _service_response(s=nil, params={})
      s ||= @response ##?? if @response
      return s if params[:rawxml] || params[:raw]
      ($log.warn 'response is nil'; return) unless s
      begin
        root = REXML::Document.new(s).root
        r = params[:all] ? root : root.elements[params[:bodytag] || '*//*Body']
        return {r.name.to_sym=>xml_to_hash(r)}
      rescue => e
        $log.warn "invalid XML response: #{s[0..127].inspect}\n#{$!}"
        $log.debug e.backtrace.join("\n")
        return nil
      end
    end
  end


  # Basic communication for SOAP over HTTP, MQ and MQJMS
  class HTTP < Common
    require 'cgi'
    require 'json'
    require 'net/http'

    attr_accessor :uri, :http, :proxy_host, :proxy_port, :user, :password, :basic_auth, :content_type,
    :http, :http_response, :cookie, :authorization, :responsetime


    def initialize(instance, params={})
      super(instance, {config: 'etc/urls.conf'}.merge(params))
      url = @params[:url]
      url.sub!('HOST', @params[:host]) if @params.has_key?(:host)   # substitue HOST when URL is http://HOST:8080/
      url.sub!('PORT', @params[:port].to_s) if @params.has_key?(:port)   # substitue PORT when URL is http://xxxx:PORT/
      url = File.join(url, @params[:resource]) if @params.has_key?(:resource)
      $log.info "#{self.class.name} using URL #{url.inspect}"
      @uri = URI.parse(url)
      @content_type = @params[:content_type] || 'text/xml;charset="utf-8"'
      @user = @params[:user]
      @password = @params[:password]
      @basic_auth = @params[:basic_auth] || @user && @password
      @cleartext = !!@params[:cleartext]
      @authorization = @params[:authorization]
      @proxy_host = @params[:proxy_host]
      @proxy_port = @params[:proxy_port]
      if @proxy_host
        @proxy = Net::HTTP.Proxy(@proxy_host, @proxy_port.to_i)
        @http = @proxy.new(@uri.host, @uri.port)
      else
        @http = Net::HTTP.new(@uri.host, @uri.port)
      end
    end

    def inspect
      "#<#{self.class.name} instance: #{@instance.inspect}, uri: #{@uri.inspect}>"
    end

    # submit body data and read response, GET is used if body is "GET", default POST
    #
    # return true on success
    def _submit_request(body, params={})
      # build URL and headers
      url, header = _prepare_request(params)
      # create request
      type = params[:request_type] || ((body == 'GET') && 'GET') || 'POST'  # default is POST
      if type == 'GET'
        @request = Net::HTTP::Get.new(url, header)
      elsif type == 'POST'
        @request = Net::HTTP::Post.new(url, header)
        @request.content_type = params[:content_type] || @content_type
        @request['SOAPAction'] = params[:soapaction] if params.has_key?(:soapaction)
        @request.body = body.kind_of?(String) ? body : body.collect {|k,v| CGI.escape(k.to_s) + '=' + CGI.escape(v.to_s)}.join('&')
      else
        $log.error "invalid type: #{type}"
        return nil
      end
      # send and receive
      begin
        return _process_request
      rescue
        $log.warn "HTTP error #{$!.inspect}"
        return nil
      end
    end

    # get response in json format and converts it to ruby objects
    def _get_json_request(params={})
      url, header = _prepare_request(params)
      #header['Accept'] = 'application/json'
      @request = Net::HTTP::Get.new(url, header)
      _process_request || return
      ($log.debug "  empty JSON response"; return) if @response.empty?
      return JSON.parse(@response)
    end

    # parse a ruby object into json and sends it via post request
    def _post_json_request(body, params={})
      url, header = _prepare_request(params)
      @request = Net::HTTP::Post.new(url, header)
      @request.content_type = 'application/json'
      @request.body = body.to_json
      _process_request || return
      ($log.debug "  empty JSON response"; return) if @response.empty?
      return JSON.parse(@response)
    end

    # parse a ruby object into json and sends it via put request
    def _put_json_request(body, params={})
      url, header = _prepare_request(params)
      @request = Net::HTTP::Put.new(url, header)
      @request.content_type = 'application/json'
      @request.body = body.to_json
      _process_request
    end

    # send a DELETE request, return true on success
    def _delete_request(params={})
      url, header = _prepare_request(params)
      @request = Net::HTTP::Delete.new(url, header)
      _process_request
    end

    def _prepare_request(params)
      @http.read_timeout = params[:timeout] || @timeout
      #
      url = params.has_key?(:resource) ? File.join(@uri.request_uri, params[:resource]) : @uri.request_uri
      if args = params[:args]
        url += '?' + args.collect {|k, v| CGI.escape(k.to_s) + '=' + CGI.escape(v.to_s) unless v.nil?}.compact.join('&')
      end
      #
      header = params[:header] || {}
      _c = params.has_key?(:cookie) ? params[:cookie] : @cookie
      header['Cookie'] = _c if _c
      if @authorization
        header['Authorization'] = @authorization
      elsif @basic_auth
        token = [[params[:user] || @user, @cleartext ? @password : @password.unpack('m').first].join(':')].pack('m')
        header['Authorization'] = 'Basic ' + token
      end
      header['Accept-Encoding'] = ''  # no gzip to circumvent issues with setupfc in JRUBY > 1.7.15
      return url, header
    end

    # return true on success
    def _process_request
      @response = @responsetime = nil
      tstart = Time.now
      # send request and receive response
      @http_response = @http.request(@request) || ($log.warn "HTTP error"; return)
      @responsetime = Time.now - tstart
      ret = ['200', '302', '500'].member?(@http_response.code)
      $log.warn "error submitting message: #{@http_response.code} #{@http_response.message}" unless ret
      @response = @http_response.body
      _c = @http_response.header['set-cookie']
      @cookie = _c.split(',').collect {|e| e.split(';').first.strip}.join('; ') if _c
      _a = @http_response.header['authorization']
      @authorization = _a if _a
      return ret
    end
  end


  # Basic communication via MQ
  class XXMQ < Common
    require 'mqjms'

    attr_accessor :use_jms, :send_remote, :receive_remote, :rinstance, :correlation, :bytes_msg, :mq_request, :mq_response, :rmq_request, :rmq_response

    def initialize(instance, params={})
      super(instance, {config: 'etc/mq.conf'}.merge(params))
      @rinstance = params[:rinstance]
      @send_remote = params[:send_remote]
      @receive_remote = params[:receive_remote]
      @use_jms = (params[:use_jms] != false)
      @correlation = params[:correlation] || (@use_jms ? :corrid : :msgid)
      @bytes_msg = params[:bytes_msg]
      if @instance
        $log.info "#{self.class.name} MQ instance: #{@instance.inspect}"
        @mq_request = ::MQ::JMS.new(@instance, params)
        if @mq_request.rqueue
          @mq_response = ::MQ::JMS.new(@instance, {reply_queue: @mq_request.rqueue.name, use_reply_queue: true}.merge(params))
        end
      end
      if @rinstance
        begin
          $log.info "#{self.class.name} MQ instance: #{@instance.inspect}"
          @rmq_request = ::MQ::JMS.new(@rinstance, params)
          if @mq_request.rqueue
            @rmq_response = ::MQ::JMS.new(@rinstance, {reply_queue: @rmq_request.rqueue.name, use_reply_queue: true}.merge(params))
          end
        rescue
          $log.warn "remote queues are not connected"
          $log.warn $!
        end
      end
    end

    def inspect
      "#<#{self.class.name} instance: #{@instance.inspect}, use_jms: #{!!@use_jms}, correlation: #{@correlation}>"
    end

    # return the time the message was sent
    def txtime
      @mq_request.txtime
    end

    # return the time between sending and receiving the response
    def responsetime
      @mq_response.responsetime
    end

    # send string s, receive the reply message
    #
    # return s if params[:nosend], else the response
    def _submit_request(s, params={})
      @request = s
      sq = @send_remote ? @rmq_request : @mq_request
      $log.debug {"send queue: #{sq.inspect}"}
      ($log.warn "no send queue"; return) unless sq
      # generate corrid unless one is provided or nil explicitly passed to force automatic generation
      if @correlation == :corrid && !params.has_key?(:correlation)
        corrid = "C#{unique_string}#{Thread.current.object_id}"
      else
        corrid = nil
      end
      $log.debug {"send_msg correlation: #{corrid.inspect}"}
      res = sq.send_msg(s, {bytes_msg: @bytes_msg, correlation: corrid}.merge(params))
      ($log.warn "error sending MQ message"; return) unless res
      $log.debug {"send_msg returned: #{res.inspect}"}
      params[:timeout] ||= @timeout
      return @response = nil if params[:timeout] <= 0
      msgid, corrid = res
      rq = @receive_remote ? @rmq_response : @mq_response
      $log.debug {"response queue: #{rq.inspect}"}
      ($log.warn "no response queue"; return) unless rq
      if params.has_key?(:correlation)
        corr = params[:correlation]
      else
        if @correlation == :none
          corr = nil
        elsif @correlation == :corrid
          corr = corrid
        elsif @correlation == :msgid
          corr = msgid
        end
      end
      $log.debug {"receive_msg correlation: #{corr.inspect}"}
      @response = rq.receive_msg(params.merge(correlation: corr, first: true))
    end
  end
end
