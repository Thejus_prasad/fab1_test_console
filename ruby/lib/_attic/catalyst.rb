=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.
    

Author: Steffen Steidten, 2014-03-25

=end

require 'builder'
require 'rqrsp'
require 'siview'

# Catalyst testing
module Catalyst

  class Client < RequestResponse::HTTP
    attr_accessor :env, :sv, :result
    
    def initialize(env, params={})
      @env = env
      @sv = params[:sv] || SiView::MM.new(@env, params)
      super("catalyst.#{env}", {content_type: 'application/x-www-form-urlencoded', resource: 'submitjob'}.merge(params))
    end
    
    def ping
      submitjob({}, {})
      @response and @response.include?('statusCode')
    end
    
    # return hash with jobResults (name value pairs consolidated) or nil on errors
    def submitjob(context, data, params={})
      $log.debug "submitjob\n#{context.pretty_inspect}\n#{data.pretty_inspect}\n"
      xml = Builder::XmlMarkup.new(indent: 2)
      xml.instruct!
      xml.controlData(version: '1.0') {|x|
        x.context {|ctx| context.each_pair {|k, v| ctx.item {|i| i.name(k); i.value(v)}} }
        x.data {|xd| data.each_pair {|k, v| xd.dataEntry {|i| i.name(k); i.value(v)}} }
      }
      res = _send_receive("controlData=#{xml.target!}", params.merge(all: true))  # all: no SOAP elements
      return res if params[:nosend] or res.nil?
      ($log.warn _service_response(nil, all: true); return nil) unless res[:controlResults][:status][:statusCode] == '0'
      return {} unless res[:controlResults][:jobResults]
      return Hash[res[:controlResults][:jobResults][:data][:dataEntry].collect {|e| e.values}]
    end
    
    # call QASimulator
    #
    # return true on success
    def qasimulator(params={})
      $log.info "qasimulator #{params.inspect}"
      context = {'originatorID'=>'SiView', 'apcRecipeContext'=>'QASimulator', 'apcActionType'=>'QATest', 'equipmentId'=>'QASimulator'}
      data = {'StartTime'=>Time.now.utc.iso8601}  # for Catalyst logging only
      lot = nil
      li = nil
      #
      # iterate over parameter keys because order matters (and they are ordered)
      params.keys.each_with_index {|k, i|
        k1 = k.to_s.split('_').first.to_sym  # allow e.g. multiple sleeps by passing longsleep_1; numbers don't matter, order is important
        case k1
        when :setwaferselection  # wafers don't need to exist
          data.merge!({"step.#{i}.SetWaferSelection"=>1, 'SetWaferSelection.operationType'=>'THK', 'SetWaferSelection.photoLayer'=>'OJ'})
          lot ||= params[k]
          li ||= @sv.lot_info(lot, wafers: true)
          li.wafers.each_with_index {|w, j| data["SetWaferSelection.wafer#{j}"] = w.wafer}
        when :getwaferselection
          data.merge!({"step.#{i}.GetWaferSelection"=>1, 'GetWaferSelection.operationType'=>'THK', 'GetWaferSelection.photoLayer'=>'OJ'})
          lot ||= params[k]
          li ||= @sv.lot_info(lot, wafers: true)
          li.wafers.each_with_index {|w, j| data["GetWaferSelection.wafer#{j}"] = w.wafer}
        when :lotinfo  # LotInfo, GetOperationList, GetOperationHistory
          data["step.#{i}.LotInfo"] = params[k]
        when :futurehold
          lot ||= params[k]
          li ||= @sv.lot_info(lot, wafers: true)
          data.merge!({"step.#{i}.FutureHold"=>lot, 'FutureHold.Route'=>li.route, 'FutureHold.OpNo'=>li.opNo, 
            'FutureHold.Op'=>li.op, 'FutureHold.ReasonCode'=>(params[k] || 'C-HD'), 'FutureHold.ReleaseReasonCode'=>'ALL'})
        when :svmodule  # TxModuleOperationList
          data.merge!({"step.#{i}.SiViewModule"=>1, 'SiViewModule.PDId'=>'PADOXPRECLN-0295', 'SiViewModule.version'=>'01', 
            'SiViewModule.PDType'=>'Process', 'SiViewModule.PDLevel'=>'Operation'})
        when :eqpinfo
          data["step.#{i}.EqpInfo"] = params[k]
        when :setupfc      # FindCurrentVersion
          data["step.#{i}.SETUPFC"] = params[k] || 'C07-00000428'
        ##data.merge!({'JavaEmail'=>1, 'Email.recipients'=>params[:email]}) if params[:email]   # no TCLMail any more
        when :baselineaddon
          data["step.#{i}.BaselineAddOn"] = params[k] * 1000
        when :mqsend
          data.merge!({"step.#{i}.MQSend"=>params[k], 'MQSend.Reply'=>1})
        when :longsleep
          data["step.#{i}.LongSleep"] = params[k] * 1000
        when :restartworker
          data["step.#{i}.RestartWorkerFlag"] = 1
        when :wrongtestcase
          data["step.#{i}.WrongTestCase"] = 1
        end
      }
      #
      @result = submitjob(context, data, params)
      return nil unless @result
      @result = Hash[@result.to_a.sort]
      $log.debug "result:\n#{@result.pretty_inspect}"
      return (@result.has_key?('testResult') and @result['testResult'] == '1')
    end
    
  end
end
