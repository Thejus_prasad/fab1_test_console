=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

    
Author:   Steffen Steidten, 2014-04-02
Version:  1.0.0

History:
=end

require 'lotexerciser'
require 'rtdclient'
require 'siview'

Thread.abort_on_exception = true   # set to true for debugging

# Test RTD Dispatcher
module RTD

  class DispatcherTest
    attr_accessor :env, :exerciser, :dclient, :dispatcherstatus, :rtdclients, :rtdthreads, :tools, 
      :smclient, :smthread, :mmclients, :mmthreads, :running
    
    def initialize(env, params={})
      @env = env
      @smclient = SiView::SM.new(@env)
      @dclient = RTD::Client.new(@env)  # for status queries
      @rtdclients = (params[:rtdclients] || 24).times.collect {RTD::Client.new(@env, timeout: 120)}
      @rtdthreads = []
      @mmclients = (params[:mmclients] || 64).times.collect {SiView::MM.new(@env)}
      @mmthreads = []
    end
    
    def inspect
      "#<#{self.class.name} env: #{@env}, #{@rtdclients.size} clients>"
    end
    
    def show_pt
      @dipatcherstatus.each_pair {|t, vv| puts t if vv.find {|v| v.why == 'PT'}}
    end
    
    def stop_loadtest
      @running = false
      $log.info "stopping lot exerciser"
      @exerciser.stop if @exerciser
      $log.info "stopping MM clients"
      @mmthreads.each {|t| t.kill if t.alive?}
      $log.info "stopping RTD clients"
      sleep 3
      @rtdthreads.each {|t| t.kill if t.alive?}
      @rtdthreads = []
      $log.info "stopping SM client"
      @smthread.join if @smthread
      true
    end
    
    def start_loadtest(params={})
      stop_loadtest
      # lot erciser
      @exerciser = LotExerciser.load1(@env, duration: -1, proctime: 1, async: true)
      @running = true
      #
      # script parameter changes
      lsps = 20.times.collect {|i| "LET_S#{i}"}
      lsvs = 20.times.collect {|i| "XXX#{i}"}
      lots = @exerciser.lotagent.lots.keys
      @mmthreads = []
      @mmclients.each_with_index {|c, i|
         @mmthreads << Thread.new(c, i) {|c, i|
          Thread.current['name'] = "MM#{i}"
          while @running
            $log.info c.user_parameter_change('Lot', lots[i], lsps, lsvs, 1, datatype: 'STRING')
          end
        }
      }
      # dispatcher clients, calling WhatNext for different tools
      @tools = params[:tools]
      unless @tools
        tool0 = params[:tool0] || 'LETCL1050'
        n = params[:ntools] || @rtdclients.size
        s = nil
        @tools = n.times.collect {s = s.nil? ? tool0 : s.next}
      end
      @running = true
      @rtdthreads = []
      @rtdclients.each_with_index {|c, i|
        @rtdthreads << Thread.new(c) {|c|
          Thread.current['name'] = "dlis_#{@tools[i]}"
          while @running
            c._send_receive("dlis|Station|#{@tools[i]}")
          end
        }
      }
      # SM clients doing route check-out and check-ins
      smreleasing = nil
      @smthread = Thread.new {
        Thread.current['name'] = 'SM'
        while @running
          # check-out and check-in
          smreleasing = false
          objs = @smclient.object_ids(:pd, "UT*")
          @smclient.edit_objects(:mainpd, objs, release: false) {true}
          smreleasing = true
          # release
          @smclient.release_objects(:pd, objs)
        end
      }
      # collect dispatcher status
      duration = duration_in_seconds(params[:duration] || 900)
      tstart = Time.now
      tend = Time.now + duration
      @dispatcherstatus = {}
      while ((t = Time.now) < tend) and @running
        if smreleasing
          @dispatcherstatus[t] = @dclient.admin_dispatchers
          $log.warn @dclient.response
          sleep 5
        else
          sleep 1
        end
      end
      #
      stop_loadtest
    end
    
  end
  
end
