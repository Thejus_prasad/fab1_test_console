=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: Steffen Steidten, 2009-10-10
=end

require "lib/mq7/connector.jar"
Dir["lib/mq7/com.ibm.mq*jar"].each {|f| require f}
Dir["lib/mq7/com.ibm.jmq*jar"].each {|f| require f}

%w(constants.CMQC MQEnvironment MQMessage MQQueue MQQueueManager MQGetMessageOptions
   MQPutMessageOptions MQException).each {|c| java_import "com.ibm.mq.#{c}"}

require 'misc'

module MQ
  # For special cases only, use MQ::JMS.new("mqinstance", :use_jms=>false) instead
  class QueueManager

    attr_accessor :mqinstance, :host, :port, :channel, :qmgrname, :qmgr, :queue, :reply_queue, :msg, :txtime, :responsetime

    def initialize(mqinstance, params={})
      @mqinstance = mqinstance
      _params = _config_from_file(mqinstance, 'etc/mq.conf') || {}
      params = _params.merge(params)
      @host = params[:host]
      @port = params[:port].to_i
      @qmgrname = params[:qmgr]
      @channel = params[:channel]
      @reply_queue = (params[:reply_queue] or "")
      if @host and @port and @qmgrname and @channel
        connect
        qname = params[:qname]
        use_reply_queue = params[:use_reply_queue]
        qname = @reply_queue if use_reply_queue
        qopen(qname, params[:write]) if qname
      end
    end

    def inspect
      q = @queue ? @queue.name.strip : nil
      "#<#{self.class.name} instance: #{@mqinstance}, qmgr: #{qmgrname}, queue: #{q}>"
    end

    #MQEnvironment.disableTracing
    def connect
      $log.info "connecting to #{@host}:#{@port}, qmgr #{@qmgrname}"
      MQEnvironment.hostname = @host
      MQEnvironment.port = @port
      MQEnvironment.channel = @channel
      MQEnvironment.properties.put(CMQC.TRANSPORT_PROPERTY, CMQC.TRANSPORT_MQSERIES)
      @qmgr = MQQueueManager.new(@qmgrname)
    end

    def disconnect
      @qmgr.disconnect
    end

    def connected?
      return nil unless @qmgr
      return @qmgr.connected?
    end

    # operations on queues
    def qopen(qname, write=false)
      # close the current queue
      qclose
      # open a queue for read or write
      return nil unless @qmgr
      if write
        oopts = CMQC.MQOO_INQUIRE + CMQC.MQOO_OUTPUT
      else
        oopts = CMQC.MQOO_INQUIRE + CMQC.MQOO_BROWSE + CMQC.MQOO_FAIL_IF_QUIESCING + CMQC.MQOO_INPUT_SHARED
      end
      begin
        @queue = nil
        $log.info "opening queue #{qname}, write: #{!!write}"
        @queue = @qmgr.accessQueue(qname, oopts)
      rescue => e
        $log.warn "error opening queue #{qname.inspect}:\n#{$!}"
        nil
      end
    end

    def qclose
      @queue.close if @queue
    end

    def qopen?
      @queue.is_open? if @queue
    end

    def qname
      @queue.name.strip if @queue
    end

    def qdepth
      return nil unless qopen?
      ($log.warn "cannot check depth on a write queue"; return nil) if (@queue.openOptions & CMQC.MQOO_OUTPUT != 0)
      @queue.getCurrentDepth
    end

    # convert a Java byte array to string
    def _barr_to_s(barr)
      res = "\000" * barr.size
      barr.each_with_index {|b,i| res[i] = (b & 0xff).chr}
      #res = java.lang.String.new(barr, "UTF-8").to_s
      return res
    end

    # valid params are :first and :timeout
    #
    # return array of messages or nil on error
    def read_msgs(params={})
      ret = []
      while true do
        if ret.size == 0
          _params = params.clone
        else
          _params[:first] = false
        end
        msg = read_msg(_params)
        break unless msg
        ret << msg
      end
      return ret
    end

    # valid params are :first, :timeout and :correlation
    #
    # return message text or nil on error
    def read_msg(params={})
      @responsetime = nil
      tstart = Time.now
      return nil unless qopen?
      ($log.warn "cannot read from a write queue"; return nil) if (@queue.openOptions & CMQC.MQOO_OUTPUT != 0)
      corrid = params[:correlation]
      # set options
      gmo = MQGetMessageOptions.new
      gmo.matchOptions = CMQC.MQMO_MATCH_CORREL_ID if corrid
      gmo.waitInterval = (params[:timeout] or 5) * 1000
      gmo.options = CMQC.MQGMO_WAIT  | CMQC.MQGMO_FAIL_IF_QUIESCING
      gmo.options |= params[:first] ? CMQC.MQGMO_BROWSE_FIRST : CMQC.MQGMO_BROWSE_NEXT
      begin
        # read next msg
        @msg = MQMessage.new
        if corrid
          @msg.correlationId = corrid.kind_of?(String) ? corrid.to_java_bytes : corrid
        end
        @queue.get(@msg, gmo)
        @responsetime = Time.now - tstart
        return @msg if params[:raw]
        # get content
        l = @msg.getMessageLength
        barr = Java::byte[l].new
        @msg.readFully(barr)
        return _barr_to_s(barr)
      rescue com.ibm.mq.MQException => mqerror
        unless [2033, 2034].member?(mqerror.reasonCode)
          # MQRC_NO_MSG_AVAILABLE, MQRC_NO_MSG_UNDER_CURSOR
          $log.info "error reading from queue\n#{$!}"
          $log.info e.backtrace.join("\n")
        end
        return nil
      end
    end

    # receive a message, removing it from the queue
    #
    # return array of messages or nil on error
    def receive_msgs(params={})
      nmax = params[:nmax]
      ret = []
      while true do
        msg = receive_msg(params)
        break unless msg
        ret << msg
        break if nmax and ret.size >= nmax
      end
      return ret
    end

    # read and delete one message from a queue
    #
    # return msg on success or nil
    def receive_msg(params={})
      @responsetime = nil
      tstart = Time.now
      return nil unless qopen?
      ($log.warn "cannot read from a write queue"; return nil) if (@queue.openOptions & CMQC.MQOO_OUTPUT != 0)
      corrid = params[:correlation]
      gmo = MQGetMessageOptions.new
      if params[:nowait]
        gmo.options = CMQC.MQGMO_NO_WAIT | CMQC.MQGMO_FAIL_IF_QUIESCING
      else
        gmo.options = CMQC.MQGMO_WAIT | CMQC.MQGMO_FAIL_IF_QUIESCING
        gmo.waitInterval = (params[:timeout] or 10) * 1000
      end
      @msg = MQMessage.new
      if corrid
        gmo.matchOptions = CMQC.MQMO_MATCH_CORREL_ID
        @msg.correlationId = corrid.kind_of?(String) ? corrid.to_java_bytes : corrid
      else
        gmo.matchOptions = CMQC.MQMO_NONE
##        @msg.correlationId = "".to_java_bytes
      end
      begin
        @queue.get(@msg, gmo) # gives sometimes error 2195, but continues
        @responsetime = Time.now - tstart
        return @msg if params[:raw]
        # get content
        if params[:check_msgtype]
          $log.warn "invalid mq message type" unless @msg.messageType == CMQC.MQMT_REPLY
        end
        l = @msg.getMessageLength
        barr = Java::byte[l].new
        @msg.readFully(barr)
        return _barr_to_s(barr)
      rescue com.ibm.mq.MQException => mqerror
        unless [2033, 2034].member?(mqerror.reasonCode)
          # MQRC_NO_MSG_AVAILABLE, MQRC_NO_MSG_UNDER_CURSOR
          $log.info "error reading from queue\n#{$!}"
          $log.info e.backtrace.join("\n")
        end
        return nil
      end
    end

    # delete messages from a queue
    #
    # return true on success
    def delete_msg(n=1)
      return nil unless @queue
      ($log.info "#{qname}: no messages to delete"; return true) unless qdepth > 0
      begin
        count = 0
        # delete all messages one by one
        while true
          @msg = receive_msg(:raw=>true, :nowait=>true)
          if @msg
            count += 1
          else
            break
          end
          break if n and count >= n
        end
        $log.info "#{qname}: deleted #{count} messages"
        return true
      rescue => e
        $log.info "error deleting msg\n#{$!}"
        $log.debug e.backtrace.join("\n")
        return false
      end
    end
    alias :delete_msgs :delete_msg

    # put a string message on the queue
    #
    # return array of message id and correlation id on success or nil
    def send_msg(s, params={})
      return nil unless qopen?
      ($log.warn "cannot write to a read queue"; return nil) if (@queue.openOptions & CMQC.MQOO_OUTPUT == 0)
      @txtime = nil
      begin
        pmo = MQPutMessageOptions.new
        pmo.options = CMQC.MQPMO_FAIL_IF_QUIESCING
        # create message
        @msg = MQMessage.new
        @msg.format = "CMQC.MQFMT_STRING"
        @msg.characterSet = (params[:charset] or 1208)   # UTF-8, default is 819 (Latin-1), see http://middleware.its.state.nc.us/middleware/Documentation/en_US/htm/csqzaw09/csqzaw0924.htm
        #
        # generate msgid unless one is provided or nil explicitly passed to force automatic generation
        msgid = params.has_key?(:msgid) ? params[:msgid] : "M#{Time.now.to_f}#{Thread.current.object_id}"
        if msgid
          @msg.messageId = msgid.kind_of?(String) ? msgid.to_java_bytes : msgid
        else
          pmo.options |= CMQC.MQPMO_NEW_MSG_ID
        end
        #
        # generate corrid unless one is provided or nil explicitly passed to force automatic generation
        corrid = params.has_key?(:correlation) ? params[:correlation] : "C#{Time.now.to_f}#{Thread.current.object_id}"
        if corrid
          @msg.correlationId = corrid.kind_of?(String) ? corrid.to_java_bytes : corrid
        else
          pmo.options |= CMQC.MQPMO_NEW_CORREL_ID
        end
        pmo.options |= CMQC.MQGMO_SYNCPOINT if params[:transacted]

        @msg.replyToQueueName = (params[:reply_queue] or @reply_queue)
        @msg.replyToQueueManagerName = (params[:reply_queuemanager] or qmgr.name)
        @msg.writeString(s)
        # send it
        if params[:do_not_send_mq_msg]
          $log.warn "not sending MQ message, because :do_not_send_mq_msg=>true"
        else
          @queue.put(@msg, pmo)
          @txtime = Time.now
        end
        return [_barr_to_s(@msg.messageId).gsub("\000", ""), _barr_to_s(@msg.correlationId).gsub("\000", "")]
      rescue => e
        $log.warn "error writing msg to queue #{qname.inspect}\n#{$!}"
        $log.warn e.backtrace.join("\n")
        return nil
      end
    end

    # if using transacted messages do an explicit commit
    def commit(params={})
      @qmgr.commit()
    end

    # put the messages in the string array msgs on the queue
    #
    # return array of correlation ids
    def send_msgs(msgs, params={})
      msgs = [msgs] unless msgs.kind_of?(Array)
      return msgs.collect {|msg| send_msg(msg, params)}
    end
  end

  # Collect queue statistics
  class Statistics
    attr_accessor :qmgr, :agent

    def initialize(qmgr, params={})
      require "lib/mq/com.ibm.mq.pcf.jar"
      java_import "com.ibm.mq.pcf.PCFMessageAgent"
      java_import "com.ibm.mq.pcf.PCFMessage"
      java_import "com.ibm.mq.constants.CMQCFC"

      @qmgr = qmgr
      @agent = PCFMessageAgent.new(qmgr.qmgr)
    end

    # get status of current open queue
    def queue_status()
      msg = PCFMessage.new(CMQCFC.MQCMD_INQUIRE_Q_STATUS)
      msg.add_parameter(CMQC.MQCA_Q_NAME, @qmgr.qname)
      msg.add_parameter(CMQCFC.MQIACF_Q_STATUS_TYPE, CMQCFC.MQIACF_Q_STATUS)
      msg.add_parameter(CMQCFC.MQIACF_Q_STATUS_ATTRS,
        [ CMQC.MQCA_Q_NAME, CMQC.MQIA_CURRENT_Q_DEPTH,
          CMQCFC.MQCACF_LAST_GET_DATE, CMQCFC.MQCACF_LAST_GET_TIME,
          CMQCFC.MQCACF_LAST_PUT_DATE, CMQCFC.MQCACF_LAST_PUT_TIME,
          CMQCFC.MQIACF_OLDEST_MSG_AGE, CMQC.MQIA_OPEN_INPUT_COUNT,
          CMQC.MQIA_OPEN_OUTPUT_COUNT, CMQCFC.MQIACF_UNCOMMITTED_MSGS ].to_java(Java::int))
      pcf_rply = @agent.send(msg)
      return nil unless pcf_rply
      return Hash[*[pcf_rply[0].get_parameters.collect {|p|
        [p.get_parameter_name, p.get_value]
      }]]
    end
  end
end
