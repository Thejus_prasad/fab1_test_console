=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.
  
Author:     sfrieske, 2015-11-04
Version:    1.0.0

History:
  2018-02-15 sfrieske, replaced select_all by select
=end

require 'dbsequel'

  
module RMS
  RLMRecipe = Struct.new(:rid, :robhandle, :create_time, :update_time, :head, :instance, :state, :type, :version)
  RLMWorkQ = Struct.new(:qid, :robhandle, :create_time, :update_time, :entered, :processed, :instance, 
    :mark_archive, :mark_delete, :retry, :fromstate, :state, :tostate)

  class DB
    attr_accessor :db
    
    def initialize(env, params={})
      @db = params[:db] || ::DB.new("rms.#{env}", schema: 'RMS_MISC')
    end
    
    # return array of RMS::RLMRecipe structs
    def rlm_recipe(params={})
      qargs = []
      q = 'SELECT ID, ROB_HANDLE, CREATION_TIME, LAST_UPDATE_TIME, HEAD_ID, RMS_INSTANCE, STATE, TYPE, VERSION 
        FROM T_RLM_RECIPE'
      q, qargs = @db._q_restriction(q, qargs, 'ROB_HANDLE', params[:robhandle])
      q += ' ORDER BY ROB_HANDLE'
      res = @db.select(q, *qargs) || return
      return res.collect {|r| RLMRecipe.new(*r)}
    end

    # return array of RMS::RLMWorkQ structs
    def rlm_workqueue(params={})
      qargs = []
      q = 'SELECT ID, ROBHANDLE, CREATION_TIME, LAST_UPDATE_TIME, ENTERED, PROCESSED, RMS_INSTANCE, 
        MARKED_FOR_ARCHIVING, MARKED_FOR_DELETION, RETRY_COUNTER, FROM_STATE, STATE, TO_STATE FROM T_RLM_WORKQUEUE'
      q, qargs = @db._q_restriction(q, qargs, 'ROBHANDLE', params[:robhandle])
      q += ' ORDER BY PROCESSED'
      res = @db.select(q, *qargs) || return
      return res.collect {|r| RLMWorkQ.new(*r)}
    end

  end
    
end
