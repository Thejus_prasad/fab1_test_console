=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.
    
Author:   Steffen Steidten, GLOBALFOUNDRIES, INC., 2012
Version:  1.0.3 
=end

require "#{Dir.getwd}/ruby/set_paths"; set_paths
require 'rmiregistry'
require 'optparse'

# EMS+ server interface
module EMS
  class ConsoleClient
    attr_accessor :env, :rmiregistry, :ems

    def initialize(env, params={})
      @env = env
      # create registry
      rport = params[:registryport] || 15011  # RMI regitry port
      host = 'f38asq10'
      @rmiregistry = RMIRegistry.new(host: host, port: rport).create_registry
      @ems = @rmiregistry.registry
    end

  end
end
