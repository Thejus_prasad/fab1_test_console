=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author:  Ulrich Koerner, 2011-08-01

History:
  2012-01-27 ssteidte, copied BaseSoap from separate file here
  2014-07-11 dsteger,  converted to xmlhash function
  2016-08-30 sfrieske, split setupfc.rb in separate files, use lib rqrsp_http instead of rqrsp
  2017-07-03 sfrieske, fixed assignment of defaultuser
  2018-01-02 sfrieske, moved from xmlhash.rb here
=end

require_relative 'sfcservice'
require_relative 'sfcrest'


module SetupFC

  class SetupFcError < StandardError
  end

  class RequestDifferenceError < StandardError
  end

  # Wraps SetupFC interfaces, methods returning directly usable values
  class Interaction
    @@tasknames = {approve: 'gfwf:approve', acknowledge: 'gfwf:acknowledge', edit_content: 'gfwf:edit-spec-content',
      edit_metadata: 'gfwf:edit-spec-metadata',review: 'gfwf:review-author', terminate: 'gfwf:terminate-spec'}

    @@department = 'ALL'

    attr_accessor :service, :support, :rest, :env, :defaultuser


    def initialize(env, params={})
      @env = env
      @service = Client.new(env, params)
      @support = QAClient.new(env, params)
      @rest = Rest.new(env, params)
      @defaultuser = @service.http.params[:user] # only used as default requestor etc
    end

    def inspect
      "#<EasySetupFC #{@env}>"
    end

    def _isok?(res)
      #$log.debug "Reply: \n#{'%s' % res.pretty_inspect}"
      raise SetupFcError, res[:return][:errorDetail] if res && res.has_key?(:return) && res[:return][:success] != 'true'
      raise SetupFcError, res[:Fault][:faultstring] if res && res.has_key?(:Fault)
      return res
    end

    # create a new spec and returns the Id
    def create_new_specification(sclass, subclass, workflow_config, template, params={})
      params[:title] ||= 'Automated spec test'
      params[:fabLocation] ||= 'M'
      params[:department] ||= 'ALL'
      params[:owner] ||= @defaultuser
      params[:author] ||= @defaultuser
      params[:classId] = sclass
      params[:subClassId] = subclass
      params[:workflowConfig] = workflow_config
      params[:template] = template
      res = _isok?(@support.createSpecification({arg0: params}))
      spId = res[:createSpecificationResponse][:return][:id]
      $log.info " - #{spId} specification created"
      return spId
    end

    # Returns the tasks assigned to the user
    def get_user_tasks(params={})
      tasks = _isok?(@support.getUserTaskList(params))[:getUserTaskListResponse][:return]
      return _filter_tasks(tasks, params[:name], params[:specid])
    end

    # Return all tasks for specific spec or with name
    def get_all_tasks(params={})
      tasks = _isok?(@support.getAllActiveTasks(false))[:getAllActiveTasksResponse][:return]
      return _filter_tasks(tasks, params[:name], params[:specid])
    end

    def wait_for_task(spec_id, params)
      $log.info "wait_for_task #{spec_id.inspect} #{params}"
      wp = {}
      [:sleeptime, :timeout, :tstart].each {|k| wp[k] = params.delete(k) if params.has_key?(k)}
      wait_for(wp) { !get_user_tasks(params.merge(specid: spec_id)).empty? }
    end

    # generate specific wait methods, DEPRECATED
    @@tasknames.each_pair do |tn, tname|
      define_method("wait_for_#{tn}".to_sym) {|spec_id, *arg|
        p = arg[0] || {}
        wait_for_task(spec_id, p.merge(name: tname))
      }
    end

    # filter array of tasks for name and speci_id
    def _filter_tasks(tasks, name, spec_id)
      $log.debug {"tasks - #{tasks.inspect}"}
      tasks ||= []
      tasks = [tasks] unless tasks.kind_of?(Array)
      # Filter for a spec id
      tasks = tasks.find_all {|task| task[:workflowProperties][:specId] == spec_id} if spec_id
      # Filter for type
      if name
        name = @@tasknames[name] if @@tasknames.has_key?(name)
        tasks = tasks.find_all {|task| task[:name] == name}
      end
      return tasks
    end

    # Wait for a certain time activity polling and a timeout (s), DUPLICATE of wait_for_task??
    def wait_get_user_task(params={})
      task = nil
      remaining_time = params[:timeout] || 300
      until task || remaining_time <= 0
        $log.debug "waiting for 5s before getting the task"
        sleep 5
        remaining_time -= 5
        task = get_user_tasks(params)[0]
        break if params[:polling] != true
      end
      return task
    end

    # Request a spec content change
    def content_change(spec_id)
      return _isok?(@support.workOnSpecification({arg0: spec_id, arg1: 'content_change'}))
    end

    # Request a spec properties change
    def properties_change(spec_id)
      return _isok?(@support.workOnSpecification({arg0: spec_id, arg1: 'properties_change'}))
    end

    # Request a spec deletion
    def delete_spec(spec_id, params={})
      res = _isok?(@support.deleteSpecification({arg0: spec_id}, params))
      return res.has_key?(:deleteSpecificationResponse)
    end

    # Create spec reference
    def create_reference(spec_id, target_spec_id, params={})
      res = _isok?(@service.createSpecificationReference(params.merge({
           :"ws:sourceSpecId" => spec_id,
           :"ws:targetSpecId" => target_spec_id # or any other existing spec
      })))[:createSpecificationReferenceResponse][:return]
      return res[:success] == 'true'
    end

    # Delete spec reference
    def delete_reference(spec_id, target_spec_id, params={})
      res = _isok?(@service.deleteSpecificationReference(params.merge({
           :"ws:sourceSpecId" => spec_id,
           :"ws:targetSpecId" => target_spec_id # or any other existing spec
      })))[:deleteSpecificationReferenceResponse][:return]
      return res[:success] == 'true'
    end

    # Reassign the task
    def assign_task(spec_id, user, params={})
      requestor = params[:requestor] || @defaultuser
      task = wait_get_user_task(:specid=>spec_id, polling: !!params[:polling] , user: requestor)
      ($log.error "No task found for #{spec_id}"; return) unless task
      return _isok?(@support.transferTaskToUser(:arg0 => task[:id], :arg1 => user, :arg2 => requestor, :arg3=> "Automated test", :user=>requestor))
    end

    def get_active_workflows_for_spec(spec_id, params={})
      wfs = _isok?(@support.getActiveWorkflowsForSpec(spec_id))[:getActiveWorkflowsForSpecResponse][:return]
      wfs = [wfs] unless wfs.kind_of?(Array)
      wfs = wfs.select {|w| w[:workflowDefinition][:name] == params[:name]} if params[:name]
      return wfs
    end


    # Process user tasks
    def process_user_tasklist
      tasks = get_user_tasks
      tasks.each do |task|
        task_id = task[:id]
        props = task[:workflowProperties]
        spec_id = props[:specId]
        task_name = task[:name]

        $log.info "Process #{spec_id}"
        case task_name
        when "gfwf:approve"
          human_workflow_action(spec_id)
        when "gfwf:edit-spec-content", "gfwf:review-author"
          human_workflow_action(spec_id, action: "CANCEL")
        when "gfwf:acknowledge"
          acknowledge(spec_id)
        else
          $log.error "Cannot process task #{task_name}"
        end
      end
    end

    def do_spec_approval(spec_id)
      tasks = get_all_tasks(specid: spec_id)
      tasks.select {|t| t[:name] == "gfwf:approve"}.each do |t|
        $log.info "process task #{t[:id]}"
        group = _isok?(@support.getPooledActorsForTask(t[:id]))[:getPooledActorsForTaskResponse][:return]
        $log.info " group is #{group}"
        user = users_for_group(group).sample
        $log.info " user is #{user}"
        human_workflow_action(spec_id, user: user, name: @@tasknames[:approve])
      end
    end

    # Cancel an active workflow. Returns false if workflow is not cancelable or nil on error.
    #
    # internal states:
    # WORKFLOW_CANCELED, // signoff successful
    # NO_WORKFLOW, // spec not in signoff state
    # NO_TASK, // internal
    # NO_OP, // internal
    # CANCEL_SUPPORTED, // cancel of signoff is possible
    # CANCEL_REQUESTED,  // asynchron cancel request generated
    # CANCEL_NOT_SUPPORTED,  // cancel of signoff impossible
    # CANCEL_ALREADY_REQUESTED // cancel already in progress
    def request_cancel_signoff(spec_id, params={})
      requestor = params[:user] || @defaultuser
      name = params[:name] || 'jbpm$gfwf:edit-spec'
      workflow = get_active_workflows_for_spec(spec_id, name: name)
      ($log.error "No active workflow found"; return) if workflow.empty? || worflow[0].nil?
      can_cancel = _isok?(@support.canCancelSignoff(spec_id, params))
      can_cancel = (can_cancel[:canCancelSignoffResponse][:return][:status] == 'CANCEL_SUPPORTED')
      ($log.warn("#{__method__}: cancel impossible"); return) unless can_cancel
      return _isok?(@support.cancelSignoff(spec_id, requestor))[:cancelSignoffResponse][:return][:status]
    end

    # Submit a new version of the spec
    def submit_properties_change(spec_id, params={})
      spec = _isok?(@service.getSpecification(spec_id))[:getSpecificationResponse][:return][:result]
      new_version = spec[:version].to_i + 1
      spec[:version] = new_version.to_s
      spec[:changeReason] =  "Properties change"
      spec[:changeDescription] = "Automated Test"
      $log.debug "Content of spec:\n#{spec.inspect}"
      res2 = _isok?(@support.submitSpecification(spec, "EDIT_SPEC_METADATA", params))
      $log.info " - #{spec_id} submitted."
      return res2, new_version
    end

    # Submit a new version of the spec
    def submit_termination(spec_id, params={})
      spec = _isok?(@service.getSpecification(spec_id))[:getSpecificationResponse][:return][:result]
      new_version = spec[:version].to_i + 1
      # No content update
      #content = Base64.decode64(spec["content"])
      #doc = REXML::Document.new(content)
      #doc[3].elements['//my:Version'].text = new_version
      #doc[3].elements['//my:ChangeReason'].text = "Terminate spec"
      #doc[3].elements['//my:ChangeDescription'].text = "Automated Test"
      #content2 = ""
      #doc.write content2
      #spec["content"] = Base64.encode64(content2).tr("\n","")
      spec[:version] = new_version.to_s
      spec[:changeReason] =  'Terminate spec'
      spec[:changeDescription] = 'Automated Test'
      $log.debug "Content of spec:\n#{spec.inspect}"
      res2 = _isok?(@support.submitSpecification(spec_id, spec, "TERMINATE_SPEC", params))
      $log.info " - #{spec_id} submitted."
      return res2, new_version
    end

    # Submit a new version of the spec (type is Base, ARMOR or SPACE)
    #
    # use :shortapproval=>true for short workflow
    def submit_specification(spec_id, params={})
      $log.info "submit_specification #{spec_id}" #, #{params.inspect}"
      content, version = get_specification(spec_id)
      doc = REXML::Document.new(content)
      new_version = version + 1
      type = params[:type] || 'Base'
      root = doc.elements['/my:myFields']
      ($log.error "invalid spec content, root not found."; return) unless root
      root.elements['//my:Version'].text = new_version
      root.elements['//my:ChangeReason'].text = "Automated Test"
      root.elements['//my:ChangeDescription'].text = "Automated Test"
      root.elements['//my:Purpose'].text = "Automated Test"
      root.elements['//my:Scope'].text = "Automated Test"
      if type == 'ARMOR'
        _add_armor_content(doc, spec_id, new_version, params)
      elsif type == 'EOPM'
        _add_eopm_content(doc, params)  #, spec_id, new_version, params)
      elsif type == 'SP'
        _add_sp_content(doc, spec_id, new_version, params)
      elsif (type =~ /SPACE|REPLACE/) && params[:filename]
        begin
          f = File.open(params[:filename])
          spec_content = REXML::Document.new(f)
          _replace_xml_element(root, spec_content.root, '//my:Content')
          _replace_xml_element(root, spec_content.root, '//my:Chapters')
          _replace_xml_element(root, spec_content.root, '//my:AuxiliaryFields')
          _replace_xml_element(root, spec_content.root, '//my:AdditionalEquipments')
          _replace_xml_element(root, spec_content.root, '//my:OperationPMSection')
          _replace_xml_element(root, spec_content.root, '//my:WBTQGroup')
          if params[:table110_mig]
            REXML::XPath.each(doc, "//my:Columns") {|e|
              sortmode = REXML::Element.new("my:TableTypeSortMode")
              sortmode.text = "Null"
              e.parent.insert_before(e, sortmode)
            }
          end
        rescue => e
          $log.error "Could not replace content of spec with #{params[:filename]}!"
          $log.error e.message
          return nil
        end
      end

      content2 = ""
      doc.write content2
      $log.debug content2

      res2 = _isok?(@service.submitSpecification(spec_id, Base64.encode64(content2).tr("\n",""), params))
      $log.info " - #{spec_id} submitted."
      return res2, new_version
    end

    def _replace_xml_element(doc1, doc2, xpath)
      e1 = doc1.elements[xpath]
      e2 = doc2.elements[xpath]
      ($log.warn "#{xpath} not found. Will ignore it."; return) unless e1 && e2
      doc1.elements[xpath] = e2
    end

     # changes content of eopm specs, for now only additional equipment is implemented
    def _add_eopm_content(doc, params={}) #, spec_id, new_version, params)
      type = params.delete(:type)
      $log.info "changing #{type} content (#{params.inspect})"
      add_eqp = params[:additional_equipment] || ''
      el = doc.root.elements['my:AdditionalEquipments/my:AdditionalEquipment/my:Equipment_ID']
      el.text = add_eqp
      return doc
    end

    def _add_armor_content(doc, spec_id, new_version, params={})
      $log.info "Add ARMOR content"
      doc[3].elements['//my:MAUISetupBlock'].add_attribute("my:SpecID", spec_id)
      doc[3].elements['//my:MAUISetupBlock'].add_attribute("my:SpecVersion", new_version)
      doc[3].elements['//my:Armor_Recipe_Section_Number'].text = "5."

      tooltype = (params[:tooltype] or "QAA-TOOLTYPE")
      category = (params[:recipeset] or "QAA-SET")
      recipename = (params[:recipename] or "AUTO-RECIPE-#{Time.now.to_i}")
      eqprecipename = (params[:eqprecipename] or "/Path/To/Auto/Recipe/#{recipename}")
      recipetype = (params[:recipetype] or "Main")
      download = (params[:download] or false)
      upload = (params[:upload] or false)
      ctxnamespace = (params[:ctxnamespace] or tooltype)
      ctxrecipename = (params[:ctxrecipename] or recipename)

      hash = {:"my:RecipeSet"=>{:"my:Organization"=>
        {:"my:ToolType"=>tooltype, :"my:RecipeCategory"=>category,
         :"my:uniqueOrganization"=>tooltype+category},
         :"my:Recipe"=>{:"my:DisplayName"=>recipename,
          :"my:UploadSpecVersion"=>nil,
          :"my:UploadSpecVersionBackup"=>nil,
          :"my:UploadSpecIdWoVers"=>nil,
          :"my:RecipeVersion"=>
          { :"my:VersionNumber"=>new_version,
            :"my:EIRecipeName"=>recipename,
            :"my:EquipmentRecipeName"=>eqprecipename,
            :"my:RecipeType"=>recipetype,
            :"my:DownloadRequired"=>download,
            :"my:UploadFrom"=>nil,
            :"my:UploadedFrom"=>nil,
            :"my:UploadRecipe"=>upload,
            :"my:UploadRecipeName"=>nil,
            :"my:CopyRecipeBodyFromOID"=>nil,
            :"my:ApprovalState"=>nil,
            :"my:ContextSection"=>{:"my:Context"=>
            { :"my:ContextLevel"=>
              { :"my:ContextEC"=>nil,
                :"my:ContextEIVersion"=>nil,
                :"my:ContextEquipmentGroup"=>nil,
                :"my:ContextEquipmentID"=>nil,
                :"my:ContextPhotoLayer"=>nil,
                :"my:ContextProcessDefinitionID"=>nil,
                :"my:ContextProductID"=>nil,
                :"my:ContextProductIDEC"=>nil,
                :"my:ContextRecipeName"=>ctxrecipename, :"my:ContextRecipeNameSpace"=>ctxnamespace,
                :"my:ContextSAPChamberID"=>nil, :"my:ContextSAPDCPIdentifier"=>nil,
                :"my:unique_context"=>ctxnamespace+ctxrecipename}}},
           :"my:UploadChangeUncritical"=>"false",
           }}}}

      hash[:"my:RecipeSet"][:"my:Recipe"][:"my:RecipeVersion"][:"my:StepperSection"] = { :"my:LithoStepperLink"=>params[:stepperlink] } if params[:stepperlink]
      xml = _build_recipeblock(hash)
      r = REXML::Document.new xml
      doc[3].elements['//my:RecipeBlock'] = r.elements['//my:RecipeBlock']
      $log.debug {"ARMOR content\n#{xml.inspect}"}
      return xml
    end

    def _add_sp_content(doc, spec_id, new_version, params={})
      $log.info "Add Shared Parameter content"
      doc[3].elements['//my:MAUISetupBlock'].add_attribute("my:SpecID", spec_id)
      doc[3].elements['//my:MAUISetupBlock'].add_attribute("my:SpecVersion", new_version)
      doc[3].elements['//my:Armor_Recipe_Section_Number'].text = "5."

      tooltype = (params[:tooltype] or "QAA-TOOLTYPE")
      category = (params[:recipeset] or "QAA-SET")
      parametername = (params[:parametername] or "AUTO-SP-#{Time.now.to_i}")
      eiparametername = (params[:parametername] or "EI-#{parametername}")

      hash = {:"my:SharedParameterSet"=>{
         :"my:SharedParameterSetDescription"=>"Automated test",
         :"my:Organization"=>
           {:"my:ToolType"=>tooltype, :"my:RecipeCategory"=>category,
            :"my:uniqueOrganization"=>tooltype+category},
         :"my:RecipeParameter"=>{
          :"my:Name"=>parametername,
          :"my:EIName"=>eiparametername,
          :"my:ParameterValue"=>{
            :"my:IntegerParameterValueWithLimits"=>{
              :"my:DefaultValueInteger"=>1,
              :"my:LowerLimitInteger"=>0,
              :"my:UpperLimitInteger"=>2}},
          :"my:ParameterPurpose"=>"APC Only"
         }}}

      xml = _build_recipeblock(hash, "SharedParameterBlock")
      r = REXML::Document.new(xml)
      doc[3].elements['//my:SharedParameterBlock'] = r.elements['//my:SharedParameterBlock']
      $log.debug {"ARMOR content\n#{xml.inspect}"}
      return xml
    end

    # Creates a new recipe block
    def _build_recipeblock(hash, blocktype="RecipeBlock")
      xml = Builder::XmlMarkup.new
      xml.tag!("myFields", "xmlns:my"=>"http://schemas.xmlsoap.org/soap/envelope/") do |run|
        @service.hash_to_xml(hash, :"my:#{blocktype}", run)
      end
      return xml.target!
    end

    # Service to inquire a specification and retrieve the content
    def get_specification(spec_id, version=nil)
      res = @service.getSpecification(spec_id, version)[:getSpecificationResponse][:return]
      return [Base64.decode64(res[:result][:content]), res[:result][:version].to_i]
    end

    def get_spec_data(spec_id, version=nil)
      return _isok?(@service.getSpecification(spec_id, version))[:getSpecificationResponse][:return]
    end

    # Service to inquire a specification and retrieve the content
    def get_spec_metadata(spec_id, version=nil)
      return _isok?(@service.getSpecificationMetadata(spec_id, version))[:getSpecificationMetadataResponse][:return][:result]
    end

    # Add spec attachment with given filename
    def create_attachment(filename, content, spec_id, temporary=false)
      return _isok?(@service.createAttachment({
           :"ws:specificationId" => spec_id,
           :"ws:temporary"       => temporary.to_s,
           :"ws:fileName"        => filename,
           :"ws:content"         => content
      }))[:createAttachmentResponse][:return][:result]
    end

    # Get spec attachment with given filename
    def get_attachment(filename, spec_id, version = nil, params={})
      hparams = {:'ws:specificationId'=>spec_id, :'ws:fileName'=>filename}
      hparams[:'ws:specificationVersion'] = version if version
      res = _isok?(@service.getAttachment(params.merge(hparams)))[:getAttachmentResponse][:return]
      return res[:success] == 'true' ? Base64.decode64(res[:result][:content]) : nil
    end

    # Delete spec attachment with given filename
    def delete_attachment(filename, spec_id, version=nil, params={})
      hparams = {:'ws:specificationId'=>spec_id, :'ws:fileName'=>filename}
      hparams[:'ws:specificationVersion'] = version if version
      res = _isok?(@service.deleteAttachment(params.merge(hparams)))[:deleteAttachmentResponse][:return]
      return res[:success] == 'true'
    end

    # Performs a user action on the spec
    #
    # actions: APPROVE, CANCEL, RETRY, REJECT
    def human_workflow_action(spec_id, params={})
      action = params[:action] || 'APPROVE'
      $log.info "human_workflow_action #{specid.inspect}, action: #{action}"
      $log.info(" - #{spec_id} human workflow action (#{action})")
      task = wait_get_user_task(params.merge(:specid=>spec_id))
      $log.debug {"task: #{task.inspect}"}
      ($log.error "No task found for #{spec_id}"; return) unless task
      props = task[:workflowProperties]
      # props[:requireRework] = params[:requireRework] or true if action == "CANCEL"
      props[:requireRework] = params[:requireRework] if (action == 'CANCEL') && params[:requireRework]
      res = _isok?(@support.completeTask(task[:id], action, "automated test approval", props, params))
      ($log.info "waiting #{params[:sleep]}s"; sleep params[:sleep]) if params[:sleep]
      return res
    end

    # Terminate a :spec_id
    def terminate(spec_id)
      res = _isok?(@support.workOnSpecification({ :arg0 => spec_id, :arg1 => "termination" }))
      return res
    end

    # Acknowledge a :spec_id
    def acknowledge(spec_id, params={})
      if params[:all]
        res = true
        ack_tasks=$sfc.get_all_tasks(:specid=>spec_id, :name=>:acknowledge)
        if ack_tasks
          ack_tasks.each do |t|
            $log.info("human workflow action #{t[:title]} for #{spec_id}")
            res &= _isok?($sfc.support.completeTask(t[:id], nil, "automated test acknowledge", t[:workflowProperties], :user=>t[:owner]))
          end
        end
        return res
      end
      task = wait_get_user_task(params.merge(:specid=>spec_id, :name=>"gfwf:acknowledge"))
      $log.info " - #{spec_id} acknowledge #{params[:user]}"
      ($log.error "No task found for #{spec_id}"; return) unless task
      task_id = task[:id]
      props = task[:workflowProperties]
      res = _isok?(@support.completeTask(task_id, nil, "automated test acknowledge", props, params))
      return res
    end

    # Downloads spec documents from Setup.FC using metadata search
    def specs_to_file(file_path, params = {})
      res = _isok?(@service.findSpecificationsByMetadata(params))
      specs = res[:findSpecificationsByMetadataResponse][:return][:result][:specList]
      $log.info "Returned specs: #{specs.inspect}"
      specs = [specs] unless specs.kind_of?(Array)
      specs.each do |spec|
        spec_id = spec[:id]
        spec_version = spec[:version]
        fname = "#{file_path}/#{spec[:name]}"
        begin
          content,version = get_specification(spec_id, spec_version)
          File.binwrite(fname, content)
          $log.info " #{fname} written (#{version})"
        rescue => e
          $log.warn "error writing #{fname}\n#{$!}"
        end
      end
    end

    def files_to_specs(filenames, params={})
      filenames.each {|filename|
        #begin
          f = File.open(filename)
          spec_content = REXML::Document.new(f)
          sclass = spec_content.root.elements['//my:Class'].text
          subclass = spec_content.root.elements['//my:SubClass'].text
          template = spec_content.root.elements['//my:Template'].text
          workflow = (params[:workflow] or spec_content.root.elements['//my:WorkflowConfiguration'].text)
          title = spec_content.root.elements['//my:Title'].text
          spec_id = create_new_specification(sclass, subclass, workflow, template, :title=>title, :id=>nil)
          content_change(spec_id)
          spec, version = submit_specification(spec_id, :type=>"SPACE", :filename=>filename)
          human_workflow_action(spec_id, :timeout=>30, :polling=>true)
          acknowledge(spec_id, :timeout=>30, :polling=>true)
        #rescue
        #  $log.error "Could not replace content of spec with #{filename}!"
        #end
      }
    end

    # Get the userids of all users in a group
    def users_for_group(group, params={})
      res = _isok?(@support.getUsersForGroup(group, params))
      users = res[:getUsersForGroupResponse][:return]
      users = [users] unless users.kind_of?(Array)
      return users.collect {|u| u[:userName]}
    end

    # find existing workflow configs
    #  if specified als by :wf_cfg_id
    def find_workflow_configs(wf_class, wf_subclass, params={})
      $log.info "finding workflow configs for class #{wf_class} / subclass #{wf_subclass}"
      res = _isok?(@support.findWorkflowConfigs(wf_class, wf_subclass, params))[:findWorkflowConfigsResponse][:return]
      if res
        res = [res] unless res.kind_of?(Array)
        res = res.find {|wf| wf[:metaInformation][:workflowConfigId] == params[:wf_cfg_id]} if params[:wf_cfg_id]
      end
      return res
    end

    # Create a new workflow configuration
    # add new normal approval layers like this
    #   { 1 => [QA, OQA, ...], 2=> ...,  }
    def create_workflow_config(title, params={})
      # check parameters
      fablocation = (params[:fablocation] || "M")
      wf_set = (params[:workflowSetId] || "M_STANDARD")
      wf_class = (params[:classId] || "05")
      wf_subclass = (params[:subClassId] || "Base")
      wf_department = (params[:departmentId] || "ALL")
      approval = (params[:approve] || { 1 => "QA" })
      wf_id = (params[:workflowConfigId] || "#{fablocation}#{wf_class}-#{wf_department}-#{Time.now.to_i}")
      $log.info "create workflow config #{wf_id} - #{title}"

      # Standard approval config with one tier
      normal_approval = (params[:normal_approval] ||
        {"type"=>"normal_approval", :"wor:approvalCycle"=>{"id"=>"approval1",
          :"wor:approvalTier"=>{"order"=>"1", :"wor:groupConfig"=>{"group"=>"QA"}}}})

      # Short approval is disabled per standard
      short_approval = (params[:short_approval] ||
        {"type"=>"short_approval", :"wor:approvalCycle"=>{"id"=>"approval1"}})

      acknowledge = params[:acknowledge] || {} # One group example {:"wor:groupConfig"=>{"group"=>"QA"}}

      config = {
        :"wor:metaInformation" => {
          :"wor:workflowConfigId" => wf_id,
          :"wor:workflowConfigTitle" => title,
          :"wor:workflowSetId" => wf_set,
          :"wor:classId" => wf_class,
          :"wor:subClassId" => wf_subclass,
          :"wor:departmentId" => wf_department
        },
        :"wor:approvalConfig" => [normal_approval, short_approval],
        :"wor:acknowledgementConfig" => acknowledge,
        :"wor:notificationConfig"=>{:"wor:notificationEvent"=>[{"type"=>"spec_submitted"}, {"type"=>"spec_rejected"}, {"type"=>"spec_effective"}]}
      }
      return wf_id if _isok?(@support.createWorkflowConfig(config))[:createWorkflowConfigResponse]
    end

    def update_workflow_config(wf_cfg_id, class_id, subclass_id, params={})
      # find existing config
      config = find_workflow_configs(class_id, subclass_id, wf_cfg_id: wf_cfg_id)
      ($log.error("workflow config #{wf_cfg_id} not found"); return) unless config
      $log.info "update workflow config #{wf_cfg_id}, #{params.inspect}"
      # update namespaces,  TODO: get config with namespace in find_workflow_configs
      ##config = @support.add_namespace_to_hash(config, 'wor')
      config = add_namespace_to_hash(config, 'wor')
      # update workflow configs
      normal_approval, short_approval = config[:"wor:approvalConfig"]
      normal_approval = params[:normal_approval] if params[:normal_approval]
      short_approval = params[:short_approval] if params[:short_approval]
      config[:"wor:approvalConfig"] = [normal_approval, short_approval]
      config[:"wor:acknowledgementConfig"] = params[:acknowledge] if params[:acknowledge]
       $log.info "#{config.inspect}"
      return wf_cfg_id if _isok?(@support.updateWorkflowConfig(config))[:updateWorkflowConfigResponse]
    end

    # helper method to add a namespace
    def add_namespace_to_hash(hash, namespace)
      result = {}
      hash.each {|k,v|
        mapped_key = k.kind_of?(Symbol) ? :"#{namespace}:#{k}" : k # only for tags, not attributes
        result[mapped_key] = v.kind_of?(Hash) ? add_namespace_to_hash(v, namespace) : v
        result[mapped_key] = v.collect {|obj| add_namespace_to_hash(obj, namespace) if obj.kind_of?(Hash)} if v.kind_of?(Array)
      }
      result
    end

    
    # Delete an existing workflow config
    def delete_workflow_config(wf_cfg_id, params={})
      $log.info "delete workflow config #{wf_cfg_id} (#{params.inspect})"
      wf_set = params[:wf_set] || 'M_STANDARD'
      return _isok?(@support.deleteWorkflowConfig(wf_cfg_id, wf_set, params))[:deleteWorkflowConfigResponse]
    end

    def getStackVersion(params={})
      # getting stack version - used to test communication
      res = _isok?(@service.getStackVersion(params))[:getStackVersionResponse][:return]
      $log.info " - stack version fetched (#{res[:result].split(",")[0]})"
      return res
    end

    def hasOpenAcknowledgements(user, params={})
      return _isok?(@service.hasOpenAcknowledgements(user, params))[:hasOpenAcknowledgementsResponse][:return]
    end

    def getDepartments(params={})
      return _isok?(@service.getDepartments())[:getDepartmentsResponse][:return]
    end

    def getSpecificationClasses(params={})
      return _isok?(@service.getSpecificationClasses())[:getSpecificationClassesResponse][:return]
    end

    def getSpecificationSubClasses(key, params={})
      return _isok?(@service.getSpecificationSubClasses(key))[:getSpecificationSubClassesResponse][:return]
    end

    def getSpecificationWithContentTransformation(spec_id, version=nil, style_sheet=nil)
      return _isok?(@service.getSpecificationWithContentTransformation(spec_id, version, style_sheet))[:getSpecificationWithContentTransformationResponse][:return]
    end

    def getKeywordTypes(params={})
      res = _isok?(@service.getKeywordTypes())[:getKeywordTypesResponse][:return]
      return res
    end

    def getKeywordValues(keyword_type, params={})
      return _isok?(@service.getKeywordValues(keyword_type))[:getKeywordValuesResponse][:return]
    end

    def findSpecificationsByKeywords(keyword_type, params={})
      res = _isok?(@service.findSpecificationsByKeywords(keyword_type))
      return res[:findSpecificationsByKeywordsResponse][:return]
    end

    def findSpecificationsByMetadata(soap_params, params = {})
      res = _isok?(@service.findSpecificationsByMetadata(soap_params, params))
      return res[:findSpecificationsByMetadataResponse][:return]
    end

    def getSpecificationHistory(sp_id, params={})
      version = (params[:version] or nil)
      res = _isok?(@service.getSpecificationHistory(sp_id, version))
      return res[:getSpecificationHistoryResponse][:return]
    end
  end
end
