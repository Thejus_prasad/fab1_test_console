=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author:     ssteidte, 2009-02-26

Version:    1.4.4

History:
  2013-11-13 ssteidte,  fixed ei_ping after transition to XMLHash
  2016-08-24 sfrieske,  use lib rqrsp_mq and _http instead of rqrsp
=end

require 'rqrsp_http'
require 'rqrsp_mq'


# FabGUI Testing
module FabGUI

  # FabGUI Testing
  module Message
    def _send_receive(s, params={})
      # send string s, receive the reply message and return the response
      @request = s
      return s if params[:nosend]
      @response = nil
      res = @mq_request.send_msg(s, params)
      ($log.warn "error sending message"; return nil) unless res
      msgid, corrid = res
      params[:timeout] ||= 60
      @response = @mq_response.receive_msg(params.merge(:correlation=>msgid, :first=>true))
      ($log.warn "no response"; return nil) unless @response
      ret = _service_response(@response, params)
      return ret
    end

    def _multiref_intvalue(xml, eid, value)
      xml.multiRef value, 'id'=>"id#{eid}", 'soapenc:root'=>'0',
        'soapenv:encodingStyle'=>'http://schemas.xmlsoap.org/soap/encoding/',
        'xsi:type'=>'xsd:int',
        ## 'xmlns:ns#{ns}'=>'com.globalfoundries.fabgui.soap.data',
        'xmlns:soapenc'=>'http://schemas.xmlsoap.org/soap/encoding/'
    end

    def _gui_multiref_element(xml, ns, eid, refid, paramid)
      xml.multiRef 'id'=>"id#{eid}", 'soapenc:root'=>'0',
        'soapenv:encodingStyle'=>'http://schemas.xmlsoap.org/soap/encoding/',
        'xsi:type'=>"ns#{ns}:GUIElement",
        "xmlns:ns#{ns}"=>'com.globalfoundries.fabgui.soap.data',
        'xmlns:soapenc'=>'http://schemas.xmlsoap.org/soap/encoding/' do |ref|
        ## ref.ID "href"=>"#id#{refid}"
        ref.ID refid, 'xsi:type'=>'xsd:int'
        ref.parameters 'href'=>"#id#{paramid}"
      end
    end

    # create a button GUI element
    def _gui_multiref_button(xml, ns, eid, label, default)
      xml.multiRef "id"=>"id#{eid}", "soapenc:root"=>"0",
        "soapenv:encodingStyle"=>"http://schemas.xmlsoap.org/soap/encoding/",
        "xsi:type"=>"ns#{ns}:Button",
        "xmlns:ns#{ns}"=>"com.globalfoundries.fabgui.soap.data",
        "xmlns:soapenc"=>"http://schemas.xmlsoap.org/soap/encoding/" do |ref|
        ref.default default.to_s
        ref.label label
      end
    end

    def _gui_multiref_map(xml, ns, eid, valuehash)
      # create a map GUI element
      xml.multiRef "id"=>"id#{eid}", "soapenc:root"=>"0",
        "soapenv:encodingStyle"=>"http://schemas.xmlsoap.org/soap/encoding/",
        "xsi:type"=>"ns#{ns}:Map",
        "xmlns:ns#{ns}"=>"http://xml.apache.org/xml-soap",
        "xmlns:soapenc"=>"http://schemas.xmlsoap.org/soap/encoding/" do |ref|
        valuehash.each_pair {
          |k,v| ref.item {|i|
            i.key k.to_s, "xsi:type"=>"soapenc:string"
            i.value v.to_s, "xsi:type"=>"soapenc:string"
          }
        }
      end
    end
  end

  # FabGUI Testing
  class Client < RequestResponse::MQXML
    include FabGUI::Message
    attr_accessor :env

    def initialize(env, params={})
      @env = env
      super("fabgui.#{@env}", params.merge(use_jms: false))
    end

    def inspect
      "#<#{self.class.name}, env: #{@env}>"
    end

    # need to make sure we use byte messages
    def _send_receive(xml, params={})
      return super(xml, params.merge(bytes_msg: true))
    end

    # SOAP actions
    #
    # send EI ping
    #
    # return true on sucess
    def ei_ping(eqp, params={})
      runID = params[:runID] || "#{eqp}@#{Time.now.iso8601}"
      $log.info "ei_ping #{eqp.inspect}, runID: #{runID.inspect}"
      xml = Builder::XmlMarkup.new(indent: 2)
      xml.instruct!
      xml.soapenv :Envelope, 'xmlns:soapenv'=>'http://schemas.xmlsoap.org/soap/envelope/',
        'xmlns:ns'=>'http://services.soap.amd.com'  do |enve|
        enve.soapenv :Body, 'soapenv:encodingStyle'=>'http://schemas.xmlsoap.org/soap/encoding/' do |body|
          body.ns :eiPing do |a|
            a.theEntity eqp
            a.theRunID runID
          end
        end
      end
      res = send_receive(xml.target!, params) || return
      ($log.warn res.inspect; return) unless res[:eiPingResponse] && res[:eiPingResponse][:eiPingReturn][nil] == 'OK'
      return true
    end

    # used for manual DC
    def handle_message(eqp, params={})
      msgtimeout = (params[:msgtimeout] or 50)
      uniq_str = (Time.now.to_f * 1000).to_i.to_s
      taid = (params[:taid] or uniq_str)
      input = (params[:input] or uniq_str)
      source = (params[:source] or "QA Test") # EI sends EI/MQ
      user = (params[:user] or "")
      sessionID = (params[:session] or "All")
      popup = params[:popup]
      text = (params[:text] or "Entry screen for manual data collection")
      $log.info "handle_message #{eqp}, session #{sessionID}"
      xml = Builder::XmlMarkup.new(:indent=>2)
      xml.instruct!
      xml.soapenv :Envelope,
        "xmlns:soapenv"=>"http://schemas.xmlsoap.org/soap/envelope/",
        "xmlns:ns0"=>"http://www.w3.org/2001/XMLSchema",
        "xmlns:ns1"=>"http://xml.apache.org/xml-soap",
        "xmlns:xsi"=>"http://www.w3.org/2001/XMLSchema-instance" do |enve|
        enve.soapenv :Body do |body|
          body.handleMessage "soapenv:encodingStyle"=>"http://schemas.xmlsoap.org/soap/encoding/" do |h|
            h.theTarget eqp
            h.theTAID taid
            h.theData do |d|
              d.properties "xsi:type"=>"SOAP-ENC:Array" do |p|
                p.item {|i| i.key "Source"; i.value(source, "xsi:type"=>"ns0:string")}
                p.item {|i| i.key "TAID"; i.value(taid.to_s, "xsi:type"=>"ns0:long")}
                p.item {|i| i.key "Timestamp"; i.value(Time.now.iso8601, "xsi:type"=>"ns0:string")}
                p.item {|i| i.key "Handler"; i.value("Query", "xsi:type"=>"ns0:string")}
                p.item {|i| i.key "Timeout"; i.value((msgtimeout * 1000).to_s, "xsi:type"=>"ns0:string")}
                p.item {|i| i.key "Display"; i.value(sessionID, "xsi:type"=>"ns0:string")}
                p.item {|i| i.key "Text"; i.value(text, "xsi:type"=>"ns0:string")}
                p.item {|i| i.key "Layout"; i.value("S", "xsi:type"=>"ns0:string")}
                if popup != nil
                  _v = popup ? "Y" : "N"
                  p.item {|i| i.key "Popup"; i.value(_v, "xsi:type"=>"ns0:string")}
                end
                p.item {|i|
                  i.key "Parameter"
                  i.value("xsi:type"=>"ns1:Vector") {|v|
                    v.item("xsi:type"=>"ns1:Map") {|ii|
                      ii.item {|iii| iii.key "Value"; iii.value ""}
                      ii.item {|iii| iii.key "Description"; iii.value ""}
                      ii.item {|iii| iii.key "Label"; iii.value "Input"}
                      ii.item {|iii| iii.key "Range"; iii.value ""}
                      ii.item {|iii| iii.key "Class"; iii.value "TextField"}
                      ii.item {|iii| iii.key "Size"; iii.value "22"}
                      ii.item {|iii| iii.key "Type"; iii.value ""}
                      ii.item {|iii| iii.key "ReadingID"; iii.value "3"}
                      ii.item {|iii| iii.key "Unit"; iii.value "string"}
                      ii.item {|iii| iii.key "Default"; iii.value input}
                      ii.item {|iii| iii.key "LabelAlign"; iii.value "L2"}
                    }
                  }
                }
              end
            end
          end
        end
      end
      res = send_receive(xml.target!, {timeout: 120}.merge(params)) || return
      ret = (res[:handleMessageResponse][:handleMessageReturn][nil] == 'NOWAIT')
      $log.warn res.inspect unless ret
      return ret
    end

    def process_log_tool_log(eqp, sessionID, msg, params={})
      displaygroup = (params[:displaygroup] or "All")
      originator = (params[:originator] or "QA Test")
      msgtimeout = (params[:msgtimeout] or 50)
      user = (params[:user] or "QATest")
      $log.info "process_log_tool_log #{eqp}, session #{sessionID}, msg #{msg.inspect}"
      xml = Builder::XmlMarkup.new(:indent=>2)
      xml.instruct!
      xml.soapenv :Envelope, "xmlns:soapenv"=>"http://schemas.xmlsoap.org/soap/envelope/" do |enve|
        enve.soapenv :Body do |body|
          body.processLogToolLog do |log|
            log.conDesc do |con|
              con.displayGroup displaygroup
              con.originatorID originator
              con.sessionID sessionID
              con.timeout(msgtimeout * 1000)
              con.toolID eqp
              con.userID user
            end
            log.message(msg)
          end
        end
      end
      res = send_receive(xml.target!, {timeout: 120}.merge(params)) || return
      ret = (res[:processLogToolLogResponse][:processLogToolLogReturn][nil] == 'OK')
      $log.warn res.inspect unless ret
      return ret
    end

    def set_facility_move0(lot, target, params={})
      blocking = (params[:blocking] or 0)
      duration = (params[:duration] or 24)
      reset = !(params[:reset] == false)
      memo = (params[:memo] or "QA Test")
      $log.info "set_facility_move0 #{lot}, #{target}, #{params.inspect}"
      xml = Builder::XmlMarkup.new(:indent=>2)
      xml.instruct!
      xml.soapenv :Envelope, "xmlns:soapenv"=>"http://schemas.xmlsoap.org/soap/envelope/" do |enve|
        enve.soapenv :Body do |body|
          body.setFacilityMove do |a|
            a.listData do |d|
              d.blocking blocking
              d.duration duration
              d.lotID lot
              d.reset reset
              d.targetFacility target
            end
            a.claimMemo memo
          end
        end
      end
      return send_receive(xml.target!, {timeout: 120}.merge(params))
      #ret = (res["viewSimpleQueryReturn"] == "OK")
      #$log.warn res.inspect unless ret
      #return ret
    end

    def set_facility_move3(carrier, target, params={})
      blocking = (params[:blocking] or 0)
      duration = (params[:duration] or 24)
      reset = !(params[:reset] == false)
      memo = (params[:memo] or "QA Test")
      $log.info "set_facility_move3 #{carrier}, #{target}, #{params.inspect}"
      xml = Builder::XmlMarkup.new(:indent=>2)
      xml.instruct!
      xml.soapenv :Envelope, "xmlns:soapenv"=>"http://schemas.xmlsoap.org/soap/envelope/" do |enve|
        enve.soapenv :Body do |body|
          body.setFacilityMove do |a|
            a.setFacilityMoveRequest1 do |d|
              d.theCarrierID carrier
              d.theTargetFacility target
              d.theBlocking blocking
              d.theDuration duration
              d.theClaimMemo memo
              d.doReset reset
            end
          end
        end
      end
      return send_receive(xml.target!, {timeout: 120}.merge(params))
      #ret = (res["viewSimpleQueryReturn"] == "OK")
      #$log.warn res.inspect unless ret
      #return ret
    end

    def view_simple_query(eqp, sessionID, msg, params={})
      displaygroup = (params[:displaygroup] or "All")
      originator = (params[:originator] or "QA Test")
      msgtimeout = (params[:msgtimeout] or 50)
      user = (params[:user] or "QATest")
      $log.info "view_simple_query #{eqp}, session #{sessionID}, msg #{msg.inspect}"
      xml = Builder::XmlMarkup.new(:indent=>2)
      xml.instruct!
      xml.soapenv :Envelope, "xmlns:soapenv"=>"http://schemas.xmlsoap.org/soap/envelope/" do |enve|
        enve.soapenv :Body do |body|
          body.viewSimpleQuery do |log|
            log.conDesc do |con|
              con.displayGroup displaygroup
              con.originatorID originator
              con.sessionID sessionID
              con.timeout(msgtimeout * 1000).to_s
              con.toolID eqp
              con.userID user
            end
            log.message(msg)
          end
        end
      end
      res = send_receive(xml.target!, {timeout: 120}.merge(params)) || return
      ret = res[:viewSimpleQueryResponse][:viewSimpleQueryReturn]
      $log.warn res.inspect unless ret
      return ret
    end

    def view_extended_query(eqp, msg, params={})
      timeout = (params[:msgtimeout] or 50)
      user = (params[:user] or "All")
      $log.info "view_extended_query #{eqp}, msg #{msg.inspect}"
      xml = Builder::XmlMarkup.new(:indent=>2)
      xml.instruct!
      xml.soapenv :Envelope,
        "xmlns:soapenv"=>"http://schemas.xmlsoap.org/soap/envelope/",
        "xmlns:xsd"=>"http://www.w3.org/2001/XMLSchema",
        "xmlns:xsi"=>"http://www.w33.org/2001/XMLSchema-instance" do |enve|
        enve.soapenv :Body, "soapenv:encodingStyle"=>"http://schemas.xmlsoap.org/soap/encoding/" do |body|
          body.viewExtendedQuery {|q|
            q.conDesc {|con|
              con.displayGroup "xsi:nil"=>"true" #"All"
              con.originatorID "QA Test"
              con.sessionID "xsi:nil"=>"true"  #sessionID
              con.timeout(timeout * 1000).to_s
              con.toolID eqp
              con.userID user
            }
            q.message msg
            q.guiElements {|gui|
              gui.item {|i| i.ID 0; i.parameters("href"=>"#id1")}
              gui.item {|i| i.ID 1; i.parameters("href"=>"#id2")}
              gui.item {|i| i.ID 2; i.parameters("href"=>"#id3")}
            }
            q.buttons {|b|
              b.item {|i| i.default "false"; i.label "OK"}
              b.item {|i| i.default "true"; i.label "Cancel"}
            }
          }
          _gui_multiref_map(body, 12, 1, {"Value"=>msg, "Color"=>"red", "Class"=>"TextField", "Label"=>"Input"})
          _gui_multiref_map(body, 12, 2, {"Value"=>"true", "Class"=>"CheckBox", "Label"=>"Check"})
          _gui_multiref_map(body, 12, 3, {"Value"=>"Debug", "List"=>"Trace|Debug|Verbose", "Class"=>"ComboBox", "Label"=>"Select"})
        end
      end
      #
      res = send_receive(xml.target!, {timeout: 120}.merge(params)) || return
      $log.info "#{res.inspect}"
      ret = res[:viewExtendedQueryResponse][:viewExtendedQueryReturn]
      $log.warn res.inspect unless ret
      return ret
    end

    def engineering_kit_select(eqp, kit, order, guid, params={})
      msg = "<html><title>SAP Kit Number: #{kit}</title><body>
      <p><font face='arial,sans-serif' size='+0'><b>SAP Kit Number: #{kit}</b></font></p>
      <p><font face='arial,sans-serif' size='-1'>SAP Order Number: #{order}</font></p>
      </body></html>"
      timeout = (params[:msgtimeout] or 50)
      $log.info "engineering_kit_select #{eqp}, kit #{kit}, order #{order.inspect}"
      xml = Builder::XmlMarkup.new(:indent=>2)
      xml.instruct!
      xml.soapenv :Envelope,
        "xmlns:soapenv"=>"http://schemas.xmlsoap.org/soap/envelope/",
        "xmlns:xsd"=>"http://www.w3.org/2001/XMLSchema",
        "xmlns:xsi"=>"http://www.w33.org/2001/XMLSchema-instance" do |enve|
        enve.soapenv :Body do |body|
          body.ns1 :viewExtendedQuery, "soapenv:encodingStyle"=>"http://schemas.xmlsoap.org/soap/encoding/",
            "xmlns:ns1"=>"http://services.soap.fabgui.globalfoundries.com" do |q|
            q.conDesc("href"=>"#id0")
            q.message msg
            q.guiElements "soapenc:arrayType"=>"ns2:GUIElement[10]",
              "xmlns:ns2"=>"com.globalfoundries.fabgui.soap.data",
              "xmlns:soapenc"=>"http://schemas.xmlsoap.org/soap/encoding/", "xsi:type"=>"soapenc:Array" do |gui|
              (1..10).each {|i| gui.guiElements("href"=>"#id#{i}")}
            end
            q.buttons "soapenc:arrayType"=>"ns3:Button[10]",
              "xmlns:ns2"=>"com.globalfoundries.fabgui.soap.data",
              "xmlns:soapenc"=>"http://schemas.xmlsoap.org/soap/encoding/", "xsi:type"=>"soapenc:Array" do |b|
              (11..12).each {|i| b.buttons("href"=>"#id#{i}")}
            end
          end
          # connection
          body.multiRef "id"=>"id0", "soapenc:root"=>"0",
            "soapenv:encodingStyle"=>"http://schemas.xmlsoap.org/soap/encoding/",
            "xmlns:ns3"=>"com.globalfoundries.fabgui.soap.data",
            "xmlns:soapenc"=>"http://schemas.xmlsoap.org/soap/encoding/", "xsi:type"=>"ns3:ConnectionDescriptor" do |ref|
            ref.displayGroup "xsi:nil"=>"true"
            ref.originatorID "SAP"
            ref.sessionID "xsi:nil"=>"true"
            ref.timeout(timeout * 1000).to_s
            ref.toolID eqp
            ref.userID "All"
          end
          # buttons
          _gui_multiref_button(body, 4, 12, "OK", true)
          _gui_multiref_button(body, 4, 11, "Cancel", false)
          # GUIElements
          _gui_multiref_element(body, 5, 1, 0, 22)
          _gui_multiref_element(body, 5, 2, 1, 15)
          _gui_multiref_element(body, 5, 3, 2, 22)
          _gui_multiref_element(body, 5, 4, 3, 28)
          _gui_multiref_element(body, 5, 5, 4, 22)
          _gui_multiref_element(body, 5, 6, 5, 20)
          _gui_multiref_element(body, 5, 7, 6, 26)
          _gui_multiref_element(body, 5, 8, 7, 17)
          _gui_multiref_element(body, 5, 9, 8, 24)
          _gui_multiref_element(body, 5, 10, 9, 33)
          # other
          _gui_multiref_map(body, 6, 28, {"Class"=>"Label", "Label"=>"SlotID ProductID Use"})
          _gui_multiref_map(body, 6, 22, {"Class"=>"LineSeparator"})
          # wafer map
          _p = {"Value"=>"true", "Class"=>"CheckBox", "LabelAlign"=>"L5", "Label"=>""}
          _gui_multiref_map(body, 6, 20, _p.merge("Label"=>"3 U-100-CLUSTER-QASAPCLUSTER.01"))
          _gui_multiref_map(body, 6, 26, _p.merge("Label"=>"5 U-100-CLUSTER-QASAPCLUSTERCHAL.01"))
          _gui_multiref_map(body, 6, 17, _p.merge("Label"=>"6 U-100-CLUSTER-QASAPCLUSTERCHAR.01"))
          _gui_multiref_map(body, 6, 24, _p.merge("Label"=>"9 U-100-CLUSTER-QASAPCLUSTERCHAL.01"))
          _gui_multiref_map(body, 6, 33, _p.merge("Label"=>"10 U-100-CLUSTER-QASAPCLUSTERCHAR.01"))
          _gui_multiref_map(body, 6, 15, _p.merge("LabelAlign"=>"L0", "Label"=>"Products for Kit with GUID=#{guid}"))
        end
      end
      #
      res = send_receive(xml.target!, {timeout: 120}.merge(params)) || return
      ret = (res[:handleMessageResponse][:viewExtendedQueryReturn] == nil)
      $log.warn res.inspect unless ret
      return ret
    end

    # Sent by jCAP TurnKey job. FabGUI sends a lotCompleteStdMessage to ERP.
    #
    # return true on success
    def lot_transfer(lot, params={})
      ttype = (params[:type] or "FAB_TO_BTFBUMP")
      $log.info "lot_transfer #{lot.inspect}"
      xml = Builder::XmlMarkup.new(:indent=>2)
      xml.instruct!
      xml.soapenv :Envelope, "xmlns:soapenv"=>"http://schemas.xmlsoap.org/soap/envelope/" do |enve|
        enve.soapenv :Body do |body|
          body.ns2 :lotTransfer, "xmlns:ns2"=>"urn:TurnKeyERPLotTransferOperations" do |a|
            a.ns2 :type, ttype
            a.ns2 :lotIdList, lot
          end
        end
      end
      res = send_receive(xml.target!, {timeout: 180}.merge(params)) || return
      ($log.warn "FabGUI comm error?"; return) unless res["lotTransferReturn"]
      details = res["lotTransferReturn"]["details"]
      ($log.warn "malformed FabGUI response: #{res.inspect}"; return) unless details
      if details["code"] == "ONEERP_MES_TKLOTTRANSFER_SERVICE_OK"
        return true
      else
        $log.warn "error: #{details.inspect}"
        return false
      end
    end
  end


  class FabGUIError < StandardError
    def initialize(method, msg)
      super("#{method} - #{msg}")
    end
  end


  class TMRSClient < RequestResponse::HttpXML
    attr_accessor :env, :suser, :token

    def initialize(env, params={})
      @env = env
      super("fabgui.#{@env}")
      @suser = @params[:suser] # user, do not mix with http user
      @token = params[:token] || @params[:token] # take token from FabGUI config
    end

    # General error handler
    def parse_response(method, res, params={})
      return res if res.nil? || params[:nosend] || params[:raw]
      _res = res["#{method}Response".to_sym]
      raise FabGUIError.new(method, "unexpected response #{res.inspect}") unless _res
      raise FabGUIError.new(method, _res[:Fault].inspect) if _res.has_key?(:Fault)
      return _res
    end

    # Get wafer sampling information
    # Values are 1-MUST, 2-EXCLUDE, 3-DO_NOT_CARE and 4-NO_DATA
    def get_wafer_sampling(lot_wafers, process_type, process_layer, params={})
      user = params[:suser] || @suser
      $log.info "get_wafer_sampling #{lot_wafers.inspect}, #{process_type}, #{process_layer}, #{params}"
      xml = Builder::XmlMarkup.new(indent: 2)
      xml.instruct!
      xml.soapenv :Envelope, 'xmlns:soapenv'=>'http://schemas.xmlsoap.org/soap/envelope/' do |enve|
        enve.soapenv :Body do |body|
          body.tmrs :getWaferSampling, 'xmlns:tmrs'=>'http://fabgui.globalfoundries.com/sampling/tmrs' do |a|
            a.getWaferSamplingParameter do |b|
              b.token @token
              b.user user
              b.lotInfos do |l|
                lot_wafers.each_pair do |lot, wafers|
                  l.lotInfo do |li|
                    li.lotId lot
                    li.processType process_type
                    li.processLayer process_layer
                    li.waferIds do |w|
                      wafers.each {|wafer, sel| w.waferId wafer}
                    end
                  end
                end
              end
            end
          end
        end
      end
      res = post(xml.target!, params)
      return parse_response('getWaferSampling', res, params)
    end

    # Set wafer sampling for lot
    # Allowed values are 1-MUST and 3-DO_NOT_CARE
    def set_wafer_sampling(lot_wafers, process_type, process_layer, params={})
      user = params[:suser] || @suser
      comment = params[:comment] || 'QA Test'
      $log.info "set_wafer_sampling #{lot_wafers.inspect}, #{process_type}, #{process_layer}, #{params}"
      xml = Builder::XmlMarkup.new(indent: 2)
      xml.instruct!
      xml.soapenv :Envelope, 'xmlns:soapenv'=>'http://schemas.xmlsoap.org/soap/envelope/' do |enve|
        enve.soapenv :Body do |body|
          body.tmrs :setWaferSampling, 'xmlns:tmrs'=>'http://fabgui.globalfoundries.com/sampling/tmrs' do |a|
            a.setWaferSamplingParameter do |b|
              b.token @token
              b.user user
              b.lotInfos do |l|
                lot_wafers.each_pair do |lot,wafers|
                  l.lotInfo do |li|
                    li.lotId lot
                    li.processType process_type
                    li.processLayer process_layer
                    li.claimMemo comment
                    li.waferInfos do |wi|
                      wafers.each_pair do |wafer, selection|
                        wi.waferInfo do |w|
                          w.waferId wafer
                          w.selection selection # 1 or 3
                          #w.reason comment #is not supported
                        end
                      end
                    end
                  end
                end
              end
            end
          end
        end
      end
      res = post(xml.target!, params)
      return parse_response('setWaferSampling', res, params)[:return][:resultCode] == '0'
    end
  end
end
