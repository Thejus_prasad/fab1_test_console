=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.


Author:     dsteger, 2012-04-04
Version:    1.1.0

History
  2017-02-20 sfrieske, use mqjms instad of rqrsp
=end

require 'builder'
require 'mqjms'


# Test OCAP Alarmreporting
module OCAP

  # Client sending MQ messages to OCAP, like EIs do
  class Client
    include XMLHash
    attr_accessor :env, :mq

    def initialize(env, params={})
      @env = env
      @mq = MQ::JMS.new("ocap.#{env}", params)
    end

    # Alarm Codes
    # code: 258, code_ignored: 100   # codes < 128 are ignored
    #
    # Alarm ids (configured actions in OCAP)
    # default: 456, downtool: 457, runninghold: 458, down_hold: 459, futurehold: 460,
    # disabled: 461, chamberdown2: 500, chamberdown3: 501
    #
    # send msg, return true on sucess
    def alarmrpt(tool, alcode=258, alid=456, params={})
      $log.info "alarmrpt #{tool}, #{alcode}, #{alid}"
      xml = Builder::XmlMarkup.new  # no indent otherwise OCAP will not accept the message
      xml.instruct!
      xml.tag! 'SOAP-ENV:Envelope',
        'xmlns:SOAP-ENV'=>'http://schemas.xmlsoap.org/soap/envelope/',
        'xmlns:OCAPServices'=>'http://www.globalfoundries.com/OCAPServices' do |enve|
        enve.tag! 'SOAP-ENV:Body' do |body|
          body.OCAPServices :SendAlarmInfo, 'SOAP-ENV:encodingStyle'=>'http://schemas.xmlsoap.org/soap/encoding/' do |a|
            a.AlarmInfo do |ai|
              ai.EquipID(tool)
              ai.ALCode(alcode.to_s)
              ai.ALID(alid.to_s)
              ai.ALText(params[:text] || 'QA Test Alarm')
              ai.ALTime(params[:time] || Time.now.utc.iso8601(3))
            end
          end
        end
      end
      ##params.merge!({:reply_queue=>"CEI.#{tool}_REPLY01", :bytes_msg=>true}) # Need to send reply queue to determine tool (??)
      return @mq.send_msg(xml.target!, {bytes_msg: true})
    end
  end


  class History
    require 'dbsequel'
    Entry = Struct.new(:seq, :equip_id, :chamber_id, :alarm_id, :alarm_code, :enabled, :alarm_messege_text, :alarm_config_name, :alarm_config_text, :ocap_config_id, :corrective_action, :siview_result, :claim_time)

    attr_accessor :env, :db

    def initialize(env, params={})
      @env = env
      @db = params[:db]
      @db = DB.new("ocap.#{env}", :schema=>"CES") unless @db
    end

    def ocap_alarm_hist(eqp, params={})
      from = (params[:from] or (Time.now - 3600))
      from = Time.parse(from) if from.kind_of?(String)
      to = (params[:to] or Time.now)
      to = Time.parse(to) if to.kind_of?(String)

      q = @db.dbh[:T_OCAP_ALARM_HIST].filter(:equip_id=>eqp).filter('claim_time >= ?',from).filter('claim_time < ?', to)
        #filter("claim_time >= to_timestamp('#{from.strftime("%Y-%m-%d %H:%M:%S")}', 'yyyy-mm-dd hh24:mi:ss')").
        #filter("claim_time <= to_timestamp('#{to.strftime("%Y-%m-%d %H:%M:%S")}', 'yyyy-mm-dd hh24:mi:ss')")
      return q.all.collect {|s| Entry.new(*s.values) }
    end
  end

  class Test
    require 'siview'

    attr_accessor :env, :client, :oh, :sv, :derdack

    def initialize(env, params={})
      @env = env
      @oh = (params[:oh] or History.new(@env))
      @sv = (params[:sv] or SiView::MM.new(@env))
      @derdack = (params[:derdack] or Derdack::EventDB.new(@env))
    end

    def verify_alarm(eqp, params={})
      $log.info "verify_alarm #{eqp.inspect}, #{params.inspect}"
      res = true
      # Expected one entry in the history
      exp_alarms = @oh.ocap_alarm_hist(eqp, params)
      count = params[:ignored] ? 0 : 1
      return false unless verify_equal(count, exp_alarms.count, "One alarm expected, but found #{exp_alarms.count}!")

      if params[:rh]
        res &= verify_match(/RunningHold/, exp_alarms[0].corrective_action)
        if params[:cj]
          res &= verify_match(/HoldAct:RunningHold.*#{params[:cj]} \[#{params[:lot]}\]/, exp_alarms[0].siview_result)
        else
          res &= verify_match(/HoldAct:No ControlJob to RunningHold/, exp_alarms[0].siview_result)
        end
      end

      if params[:fh]
        res &= verify_match(/^FutureHold/, exp_alarms[0].corrective_action)
        if params[:cj]
          res &= verify_match(/HoldAct:EnhancedFutureHold.*#{params[:cj]} \[#{params[:lot]}\]/, exp_alarms[0].siview_result)
        else
          res &= verify_match(/HoldAct:No ControlJob to EnhancedFutureHold/, exp_alarms[0].siview_result)
        end
      end

      if params[:downtool]
        res &= verify_match(/DownTool/, exp_alarms[0].corrective_action)
        res &= verify_match(/InhibitAct:Equipment Downed/, exp_alarms[0].siview_result)
      end

      if params[:downchamber]
        res &= verify_match(/DownTool/, exp_alarms[0].corrective_action)
        res &= verify_match(/InhibitAct:Inhibited Chamber #{params[:chamber_id]}/, exp_alarms[0].siview_result)
      end

      # No entry in SiView
      res &= verify_equal(0, @sv.eqp_alarm_history(eqp, params).count, "No SiView alarm expected")
      return res
    end

    def verify_alarm_count(eqp, acount, params={})
      $log.info "verify_alarm_count #{eqp.inspect}, #{acount}, #{params.inspect}"
      ca = (params[:ca] or "OCAPTEST")
      exp_alarms = @oh.ocap_alarm_hist(eqp, params)
      active = exp_alarms.select {|a| a.corrective_action =~ /#{ca}/}
      ##Not anymore: disabled = exp_alarms.select {|a| a.corrective_action =~ /Alarm is disabled/}
      res = verify_equal(acount, active.count,"Not correct number of alarms!")

      # No entry in SiView
      res &= verify_equal(0, @sv.eqp_alarm_history(eqp, params).count, "No SiView alarm expected")
      return res
    end

    # collects derdack events
    #
    # return false if not the expected count of events found
    def verify_notification(eqp, alid, alcode, params={})
      $log.info "#{__method__} #{eqp.inspect}, #{alid.inspect}, #{alcode.inspect}, #{params.inspect}"
      ($log.warn "  no connection to Derdack DB, skipping test"; return true) unless @derdack.db.connected?
      app = "AlarmOCAP"
      evs = @derdack.get_events(params[:from], params[:tend], "Application"=>app)
      ($log.warn "  no connection to Derdack"; return nil) unless evs
      msgs = evs.collect {|e|
        $log.debug "  #{e.inspect}"
        m = e.params["Message"]
        next unless m =~ /Tool alarm from OCAP staging - OCAPTEST/
        next unless m =~ /#{eqp}, #{alid}, #{alcode}/
        e
      }.compact
      $log.debug " msgs: #{msgs.inspect}"
      $log.warn "  more than one message found:\n #{msgs.inspect}" if msgs.size > 1
      $log.warn "  no message found:" if msgs.size == 0
      return params[:raw] ? msgs : msgs.size == 1
    end

  end

end
