=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.


Author: Steffen Steidten, 2011-11-23

History:
  2013-08-14 ssteidte, removed Ruby1.8 support
  2015-07-02 ssteidte, moved globals to constants
  2015-08-07 ssteidte, expand clients for parallel submission of  DCRs
  2016-05-10 sfrieske, use rubyXL instead of POI to read Excel files
  2016-12-07 sfrieske, removed unused webmrb, use sfcservice instead of setupfc
  2017-05-17 sfrieske, option to pass AQV msgs for verify_aqv_message
  2017-07-20 sfrieske, refactored from SpaceTest
=end

ENV.delete('http_proxy')
require 'rqrsp_http'


module SIL
  # Excel spec upload via sil-pcp-processor
  class Excel
    attr_accessor :env, :http

    def initialize(env, params={})
      @env = env
      @http = RequestResponse::Http.new(params[:pcp_processor_instance] || "silpcpprocessor.#{@env}", {timeout: 600}.merge(params))
      @resources = {
        Autocharting: 'autoCharting-doupload.action',
        Speclimit: 'pcp-doupload.action',
        MonitoringSpeclimit: 'monitoringSpec-doupload.action',
        PDReferer: 'pd-doupload.action',
        Chamberfilter: 'chamberFilter-doupload.action',
        Futurehold: 'futureHold-doupload.action',
        Generickey: 'genericKey-doupload.action',
        Terminate: 'doTerminateSpec.action'
      }
    end

    def inspect
      "#<#{self.class.name} #{@http}>"
    end

    # return (auto-created) caption on successful upload
    def upload_file(fname, type, params={})
      $log.info "upload_file #{fname.inspect}, #{type.inspect}, #{params}"
      ($log.warn "invalid upload type: #{@resources.keys.inspect}"; return) unless @resources.keys.member?(type.to_sym)
      # call upload page to get a session (cookie is obtained and stored by @http internally)
      @http.get(resource: 'pcp-upload.action') || return
      # prepare upload request
      caption = params[:caption] || "QA-#{Time.now.utc.iso8601(3)}"
      s = File.binread(fname) || return
      boundary = params[:boundary] || '--SpcXlsUplBnd4455XX'
      body = "--#{boundary}\r\n" +
        "Content-Disposition: form-data; name=\"upload\"; filename=\"#{File.basename(fname)}\"\r\n" +
        "Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet\r\n\r\n" + s +
        "\r\n--#{boundary}\r\n" +
        "Content-Disposition: form-data; name=\"caption\"\r\n\r\n" + caption +
        "\r\n--#{boundary}--\r\n"
      # upload and check response body for processing errors
      res = @http.post(body, content_type: "multipart/form-data; boundary=#{boundary}", resource: @resources[type.to_sym]) || return
      ($log.warn res[0..999]; return) if res.include?('Error')
      return caption
    end

  end
end
