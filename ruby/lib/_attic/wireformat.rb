=begin
Wrapper around the Xsite commands.
(c) Copyright 2010-12 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.
    
Author: Daniel Steger, 2012-05-01

History:
  2012-06-27 dsteger,  Some fixes
=end

require 'time'
require 'logger'
require 'rbtree'

# Wrapper around the Xsite commands.
module Xsite

  ErrorMessage = Struct.new(:code, :section, :severity, :message)

  # Wrapper around the Xsite commands.
  class Parser
    def extract_string_literals( string )
      string_literal_pattern = /"([^"\\]|\\.)*"/
      string_replacement_token = "___+++STRING_LITERAL+++___"
      # Find and extract all the string literals
      string_literals = []
      string.gsub(string_literal_pattern) {|x| string_literals << x}
      # Replace all the string literals with our special placeholder token
      string = string.gsub(string_literal_pattern, string_replacement_token)
      # Return the modified string and the array of string literals
      return [string, string_literals]
    end

    def tokenize_string( string )
      string = string.gsub("{", " { ")
      string = string.gsub("}", " } ")
      string = string.gsub("=", " = ")
      token_array = string.split " "
      return token_array
    end

    def restore_string_literals( token_array, string_literals )
      return token_array.map do |x|
        if(x == '___+++STRING_LITERAL+++___')
          # Since we've detected that a string literal needs to be replaced we
          # will grab the first available string from the string_literals array
          string_literals.shift
        else
          # This is not a string literal so we need to just return the token as it is
          x
        end
      end
    end

    # A helper method to take care of the repetitive stuff for us
    def is_match?(string, pattern)
      match = string.match(pattern)
      return false unless match
      # Make sure that the matched pattern consumes the entire token
      match[0].length == string.length
    end

    # Detect a symbol
    #
    # Anything other than parentheses, single or double quote and commas
    def is_symbol?( string )  
      return is_match?( string, /[^\"\'\,\(\)]+/ )
    end

    # Detect a string
    #
    # Anything other than parentheses, single or double quote and commas
    def is_string_literal?( string )
      return is_match?( string, /"([^"\\]|\\.)*"/ )
    end


    def process_literal(t)
      $log.debug "process #{t}"
      _t = t
      _t = nil if t == "NIL"
      _t = t[1..-2] if is_string_literal?(t)
      _t = true if t == "T"
      _t = false if t == "F"
      return _t
    end

    def process_symbol(symbol, t, optional=false)
      res = (t == symbol)
      raise " expecting '#{symbol}', got #{t}" unless (res or optional)
      return res
    end

    # parse a new hash
    def parse_new_hash(tarray)
      $log.debug "start hash"
      h = MultiRBTree.new
      tarray, _val1 = parse_val(tarray, nil)
      tarray, _val2 = parse_val(tarray, nil)
      $log.debug "end ASSOC, #{_val1} #{_val2}"
      process_symbol( "}", tarray.shift)
      h[_val1.to_s] = _val2
      $log.debug "first value #{_val1},#{_val2}"
      t = tarray.shift
      while t and t != "}"
        tarray, h = parse_literal(t, h, tarray)
        t = tarray.shift
      end
      $log.debug "Return hash, #{tarray.inspect}"
      return tarray, h
    end

    # parse a class
    def parse_class(tarray)
      t = tarray.shift
      $log.error "Invalid token #{t}" unless t == "="
      t1 = tarray.shift
      if t1 == "A1"
        _value = tarray.shift
        process_symbol( "}", tarray.shift)
        return tarray, _value[1..-2]
      elsif ["S4", "U4", "S8", "U8"].member?(t1)
        _value = tarray.shift
        process_symbol( "}", tarray.shift)
        return tarray, _value.to_i
      elsif ["F4", "F8"].member?(t1)
        _value = tarray.shift
        process_symbol( "}", tarray.shift)
        return tarray, _value.to_f
      elsif t1 == "B1"
        _value = tarray.shift
        process_symbol( "}", tarray.shift)
        return tarray, _value == "T"
      elsif t1 == "ASSOC"
        return parse_new_hash(tarray)
      elsif ["SORTED", "ORDERED"].member?(t1)
        # check for ASSOC and start a hash
        if tarray.size > 4 and (tarray[0..3].join == "{class=ASSOC")
          tarray.shift(2)
          return parse_class(tarray)
        end
        #Array
        $log.debug "Start array"
        tarray, value = parse_val(tarray, [])
        value = [value] unless value.is_a?(Array)
        $log.debug "first value #{value.inspect}"
        #next value
        t = tarray.shift
        while t and t != "}"
          lv = value.last
          tarray, nv = parse_literal(t, lv, tarray)
          #tarray.shift # skip following braket "}"
          $log.debug "adding, #{nv.inspect}"
          value << nv
          t = tarray.shift
        end
        $log.debug "Return array"
        #parse_val( tarray, value )
        return tarray, value
      else
        #Class
        classname = t1.to_sym
        $log.debug "Start Class: #{classname}"
        h = {"k"=>[], "v"=>[]}
        tarray, _val = parse_val(tarray, h)
        # closing bracket already processed
        
        Xsite.const_set(classname, Struct.new(*h["k"])) unless Xsite.const_defined?(classname)
        $log.debug "Return Class: #{classname}"
        return tarray, Xsite.const_get(classname).new(*h["v"])
      end
    end

    # Special handling of error message structure
    def parse_error_struct(tarray)
      _val = Xsite::ErrorMessage.new
     
      _val.members.each do |m|
        process_symbol(m.to_s, tarray.shift)
        process_symbol("=", tarray.shift)
        _val[m] = process_literal(tarray.shift)
      end
     
      return tarray, _val
    end
   
    # We have already a value as reference and try to parse a new one of the same type
    def parse_literal(t, value, tarray)
      $log.debug "Process second tag #{t}, #{value.inspect}"
      if value.is_a?(Hash) or value.is_a?(MultiRBTree)
        $log.debug "Enter - hash pair"
        set_class = false
        process_symbol( "{", t)
        t = tarray.shift
        if t == "class"
          process_symbol( "=", tarray.shift)
          process_symbol( "ASSOC", tarray.shift)
          tarray, k = parse_val(tarray, nil)
          set_class = true
        else
          k = process_literal(t)
        end
        if set_class
          tarray, v = parse_val(tarray, nil)
        else
          t = tarray.shift
          tarray, v = parse_literal(t, value.values.last, tarray)
        end
        $log.debug "adding pair (#{k},#{v}) to hash"
        process_symbol( "}", tarray.shift)
        value[k.to_s] = v
        return tarray, value
        
      # elsif value.is_a?(Array)
        # $log.debug "Enter - array"
        # lv = value.last
        # tarray, nv = parse_literal(t, lv, tarray)
        # tarray.shift # skip following braket "}"
        # value << nv
        # $log.debug "Return - array"
        # return tarray, value
      
      elsif value.is_a?(Struct)            
        $log.debug "Enter - struct"
        process_symbol( "{", t)        
        #raise " expecting '{', got #{t}, #{value.inspect}, #{tarray.inspect}" unless t == '{'
        nv = value.class.new
        full_parse = false
        # need to peek for class symbol
        if process_symbol( "class", tarray[0], true)
          tarray.shift
          process_symbol( "=", tarray.shift)
          process_symbol( value.class.name.split('::').last, tarray.shift)
          full_parse = true
        end
        
        value.members.each do |attr|  
          $log.debug " member #{attr}"
          t = tarray.shift
          if full_parse            
            process_symbol(attr.to_s, t) # TODO: Ruby1.9 uses symbols instead of string. Removed in future.
            process_symbol( "=", tarray.shift)
            t = tarray.shift
          end           
          _lvalue = value[attr.to_sym] # TODO: no need to convert in Ruby1.9
          if t == "NIL"
            k = nil
          elsif t == "{" and tarray[0] == "}"
            k = nil
            process_symbol("}", tarray.shift)
          elsif _lvalue.is_a?(Hash) or _lvalue.is_a?(MultiRBTree)
            #$log.debug "Parse new hash, #{_lvalue.inspect}"
            tarray, k = parse_val(tarray)
          elsif _lvalue.is_a?(Array)
            #$log.debug "Parse new array, #{_lvalue.inspect}"
            tarray, k = parse_val([t] + tarray)
          elsif _lvalue == nil        
            tarray, k = parse_val([t] + tarray)
          else
            tarray, k = parse_literal(t, _lvalue, tarray)
          end
          nv[attr.to_sym] = k  # TODO: no need to convert in Ruby1.9
        end
        process_symbol( "}", tarray.shift)
        # no skip following braket "}"
        $log.debug "Return - struct"
        return tarray, nv
      elsif value.is_a?(Time)
        # I need to give it back to parse_val for timestamps  
        process_symbol("{", t)
        t = tarray.shift        
        if process_symbol("date", t, true)
          process_symbol("=", tarray.shift)
          t = tarray.shift
        end        
        date_time = t
        t = tarray.shift
        if process_symbol("time", t, true)
          process_symbol("=", tarray.shift)
          t = tarray.shift 
        end
        date_time += t
        process_symbol("}", tarray.shift)
        $log.debug date_time
        return tarray, Time.parse(date_time)
      elsif value.is_a?(Integer)
        return tarray, t.to_i
      elsif value.is_a?(Float)
        return tarray, t.to_f
      elsif value.is_a?(String)
        if t.start_with?('"')
          t = t[1..-2]
        end
        return tarray, t.to_s
      elsif value == true or value == false
        return tarray, t == "T"
      else
        return tarray, nil
      end   
    end
     
   
    def parse_val(tarray, value=nil)
      unless tarray.length > 0
        $log.debug "Returned value: #{value.inspect}"
        return tarray, value
      end
      t = tarray.shift
      $log.debug "Process tag: #{t}"
      if t=="{"
        #begin
        $log.debug "Start parsing: #{tarray.inspect} (#{value.inspect})"        
        t = tarray.shift
        if value and value != []
          if value.is_a?(Hash) and value["k"] and value["v"]            
            value["k"] << t.to_sym
            process_symbol("=", tarray.shift)
            tarray, _val = parse_val(tarray, nil)
            value["v"] << _val
            parse_val(tarray, value)
          else
            $log.error "next value pairs"
            _tarray, _value = parse_literal( t, value, tarray )
            parse_val(_tarray, _value)
          end        
        elsif t == "class"          
          # Determine the class
          tarray, _value = parse_class(tarray)
          return tarray, _value
        elsif t == "{"
          # maybe a dictionary
          process_symbol("class", tarray.shift)
          $log.debug " -> start DICT?"
          tarray, _value = parse_class(tarray)
          if _value.is_a?(Hash) or _value.is_a?(MultiRBTree)
            $log.debug " -> end DICT , #{_value.inspect}"
          else
            _nvalue = _value
            _value = [_value]
            t = tarray.shift
            while t and t != "}"
              tarray, _nvalue = parse_literal(t, _nvalue, tarray)
              _value << _nvalue
              t = tarray.shift
            end
          end
          return tarray, _value
        elsif t == "date"          
          # Datetime object          
          process_symbol("=", tarray.shift)
          t = tarray.shift          
          _date = process_literal(t)          
          if tarray[0] == "time"
            tarray.shift
            process_symbol("=", tarray.shift)
            t = tarray.shift          
            _date += process_literal(t)
          end
          process_symbol("}", tarray.shift)
          return tarray, Time.parse(_date)
        elsif t == "}"
          return tarray, nil
        elsif t == "code"
          tarray, _value = parse_error_struct([t]+tarray)
          parse_val( tarray, _value )
        else
          raise "Something happened: #{t}, #{tarray}"
        end
      elsif t=="}"
        #end
        $log.debug "End parsing:  #{tarray.inspect} (#{value.inspect})"
        return tarray, value
      elsif t == "class"
        $log.error "class ...Should not occur..."
        tarray, _value = parse_class(tarray)
        $log.debug "class -> #{_value}"
        parse_val( tarray, _value )
      else
        if value.is_a?(Hash) and value["k"] and value["v"]
          value["k"] << t.to_sym
          process_symbol("=", tarray.shift)
          tarray, _val = parse_val(tarray, nil)
          value["v"] << _val
          parse_val(tarray, value)
        elsif value
          $log.error "second value pairs"
          _tarray, _value = parse_literal( t, value, tarray )
          parse_val(_tarray, _value)
        else  
          _t =  process_literal t
          $log.debug "Return value:  #{tarray.inspect} (#{_t.inspect})"
          return tarray, _t
        end
        # add
        # if value.is_a?(Array)
        # value << _t
        # elsif value.is_a?(Hash)
        # tarray = process_equal(tarray)
        # tarray, _val = parse_val(tarray, nil)
        # value["k"] << _t.to_sym
        # value["v"] << _val
        # $log.debug "Next item"
        # parse_val(tarray, value)
        # else
        # value = _t
        # end
        #tarray, nvalue = parse_literal(t, value, tarray)
        
       
      end
    end
   
    def parse_properties(token_array)
      hash = {}
      while( token_array.length > 0 )
        t1, t2 = token_array
        break unless t2 == "="
        token_array, value = parse_val(token_array.drop(2))
        hash[t1] = value if t1 and value
      end
      return hash
    end



    def re_structure( token_array, offset = 0 )
      struct = []
      while( offset < token_array.length )
        if(token_array[offset] == "{")
          # Multiple assignment from the array that re_structure() returns
          offset, tmp_array = re_structure(token_array, offset + 1)
          struct << tmp_array
        elsif(token_array[offset] == "}")
          break
        else
          struct << token_array[offset]
        end
        offset += 1
      end
      return [offset, struct]
    end

    def parse( string )
      istart = string.index("//l") + 3
      string = string[istart..-1]      
      string, string_literals = extract_string_literals string 
      token_array = tokenize_string string      
      token_array = restore_string_literals token_array, string_literals
      # Drop return code, tx and colon
      hash = parse_properties(token_array.drop(3))      
      return hash
    end
  end
end

def wired_test
#sample = '(testreply{:448074)::(:0):://l 0 XsiteGetScheduleTxn : value={ class=SORTED { class=ASSOC { class=A1 "20120404 120000000" } { class=XsiteSchedule action=1 timeStamp=6 postCommitTime={ date=20120306 time=062516000 } txnName="XsiteGetScheduleTxn" scheduleType="WorkOrder" number="P0003147" userName="tgallagh" isIterated=F state="Scheduled" equipmentName="AMI2100" serialNumber="" title="Online Qual" dateDueStart={ date=20120404 time=120000000 } dateDueEnd={ date=20120404 time=130000000 } dateDueEarly={ date=20120404 time=110000000 } dateDueLate={ date=20120404 time=130000000 } dateActualStart=NIL earliestEstDateCriticalAct={ date=20120304 time=120000000 } currentShopName="CFM" responsibleShopName="CFM" priority=2 estimatedShiftScheduleName="" estimatedShiftScheduleVer=0 estimatedShiftName="" estimatedElapsedHours=1 performWindowEarlyDueEvent=F performWindowDueEvent=F performWindowOverdueEvent=F PerformWindowCompleteEvent=F lastComment=NIL hasAttachments=F hasUnassignedResourceEstimates=F hasUnissuedMaterialsEstimates=F recurringActivity=F insufficientMaterials=F workRequestId="" workOrderId=00002f46.0a0a2e0c.4f5533ad.0000206b.28033 workTemplateId=00002f64.162f0a0a.4f28130c.000017b4.27 activeChecklistType="" meterRemainingEarly=0 meterRemainingDue=0 meterRemainingLate=0 meterType="" readingUnits="" meterValueEarly=0 meterValueDue=0 meterValueLate=0 reasonCodes={ { class=ASSOC { class=A1 "ActivityType" } { class=A1 "ONLINE" } } { "SubActivityType" "OTHER" } } clockedOnUsers={ } requestedByUsers={ } estimatedCosts={ { class=ASSOC { class=A1 "Contractor" } { class=F8 0 } } { "Cost Category" 0 } { "Labor" 0 } { "Parts" 0 } } actualCosts={ { class=ASSOC { class=A1 "Contractor" } { class=F8 0 } } { "Cost Category" 0 } { "Labor" 0 } { "Parts" 0 } }
#
#  assignedToUsers={ } assignedToEquipment={ } MeterLimits={ } CurrentMeterValue=0 LastReadingValue=0 ReadingType="" } } { "20120406 044047000" { 1 6 { date=20120306 time=062516000 } "XsiteGetScheduleTxn" "WorkOrder" "P0003148" "tgallagh" F "Scheduled" "AMI2100" "" "Daily PM" { date=20120406 time=044047000 } { date=20120406 time=045547000 } { date=20120406 time=014047000 } { date=20120406 time=074047000 } NIL { date=20120306 time=044047000 } "CFM" "CFM" 2 "" 0 "" 0.25 F F F F NIL F F F F F "" 00002f46.0a0a2e0c.4f5533e9.00002040.49847 00002f64.23240b0a.4e95e9eb.00004778.36 "" 0 0 0 "" "" 0 0 0 { { class=ASSOC { class=A1 "ActivityType" } { class=A1 "PM" } } { "FailureCode1" "OTHER" } { "SolutionCode" "DONE" } { "SubActivityType" "MAINT" } { "SymptomCode" "OTHER" } } { } { } { { class=ASSOC { class=A1 "Contractor" } { class=F8 0 } } { "Cost Category" 0 } { "Labor" 0 } { "Parts" 0 } } { { class=ASSOC { class=A1 "Contractor" } { class=F8 0 } } { "Cost Category" 0 } { "Labor" 0 } { "Parts" 0 } } { } { } { } 0 0 "" } } } '
#  
#sample = '(testreply{:448074)::(:0):://l 0 XsiteGetScheduleTxn : value={ class=SORTED { class=ASSOC { class=A1 "20120404 120000000" }       { class=XsiteSchedule         action=1         timeStamp=6         postCommitTime={ date=20120306 time=062516000 }         txnName="XsiteGetScheduleTxn"   scheduleType="WorkOrder"   number="P0003147"   userName="tgallagh"   isIterated=F  clockonUser={ }}    }  }'
#
#sample = '(testreply{:448074)::(:0):://l 0 XsiteGetScheduleTxn : value={ class=SORTED { class=A1 "Test1" } "Test2" "Test3" }'
#
#sample = '(testreply{:448074)::(:0):://l 0 XsiteGetScheduleTxn : equipmentName="Etcher" reasonCodes={  { class=ASSOC { class=A1  "ActivityType" }  { class=A1 "ASDASD" } } { "Second" "Third" } { "Four" "Third" }} title="Missaligned"'
#
#sample = '{ class=ASSOC { class=A1  "ActivityType" } { class=A1 "Unsched" } } { "asdasd" "afsdfdsf" }'
#
#sample = '(testreply{:448074)::(:0):://l 0 XsiteGetScheduleTxn : value={ class=SORTED { class=XsiteSchedule action=1 timeStamp=6 postCommitTime={ date=20120306 time=062516000 } } { 2 7 { date=20110306 time=062516000 } } }'
#
#sample = '(testreply{:448074)::(:0):://l 0 XsiteGetScheduleTxn : value={ class=SORTED { class=XsiteSchedule action=1 timeStamp=6 } { 2 7 } }'
#
#sample = '(testreply{:448074)::(:0):://l 0 XsiteGetScheduleTxn : value={ class=SORTED { class=ASSOC { class=A1  "ActivityType" }  { class=XsiteSchedule action=1 timeStamp=6 } } { "test" { 2 7 } } }'
#
#sample = '(testreply{:448074)::(:0):://l 0 XsiteGetScheduleTxn : value={ class=SORTED { class=ASSOC { class=A1  "ActivityType" }  { class=XsiteSchedule action=1 timeStamp=6 } } { "test" { 2 7 } } { class=ASSOC { class=A1  "ActivityType2" }  { class=A1 "ASDASD" } } { "Second" "Third" } }'
#
#sample = '(testreply{:448074)::(:0):://l 0 XsiteGetScheduleTxn : value={ class=SORTED { class=SORTED { class=A1 "Test1" } "Test2" } "Test3" "Test4" }'
#
#sample = '(testreply:58991)::(:0):://l 1 XsiteCreateWorkOrderTxn : error={ code=1 section=38 severity=3 message="Nothing to Fetch. Object:Equipment  Value:test" }'
$log = Logger.new(STDOUT)

#sample = "(testreply{:448074)::(:0):://l 0 XsiteFetchWorkOrderTxn : value={ class=ORDERED { class=XsiteWorkOrderInt workOrderNumber=\"P0010047\" equipmentName=\"POL2700\" serialNumber=\"\" workTemplateId=00002f64.19260b0a.4f16ded0.00001840.277 workTemplateNumber=\"P0000628\" workTemplateVersion=2 equipmentWorkTemplateId=00002f6d.31240b0a.4f171c8f.00002908.32 workOrderGroupKey=\"\" title=\"AMAT REFLEXION CUPOL MONTHLY PM 08\" responsibleShopName=\"CMP\" currentShopName=\"CMP\" state=\"Canceled\" previousState=\"Planned\" dateProjectedStart={ date=20120809 time=100000000 } dateProjectedEnd={ date=20120809 time=140000000 } dateProjectedEarly={ date=20120802 time=100000000 } dateProjectedLate={ date=20120816 time=100000000 } dateTargetStart=NIL dateTargetEnd=NIL dateTargetEarly=NIL dateTargetLate=NIL dateDueStart={ date=20120809 time=100000000 } dateDueEnd={ date=20120809 time=140000000 } dateDueEarly={ date=20120802 time=100000000 } dateDueLate={ date=20120816 time=100000000 } earliestEstDateCriticalAct={ date=20120809 time=000000000 } performWindowEarlyDueEvent=F performWindowDueEvent=F performWindowOverdueEvent=T performWindowCompleteEvent=T windowOverdueEventComment=NIL estimatedElapsedHours=4 estimatedShiftScheduleName=\"\" estimatedShiftScheduleVer=0 estimatedShiftName=\"\" dateActualStart=NIL dateActualEnd=NIL actualElapsedHours=0 actualShiftScheduleName=\"\" actualShiftScheduleVersion=0 actualShiftName=\"\" priority=2 defaultCostCodeName=\"8018055\" meterRemainingEarly=0 meterRemainingDue=0 meterRemainingLate=0 meterType=\"\" readingUnits=\"\" meterValueEarly=0 meterValueDue=0 meterValueLate=0 defaultWarehouseName=\"\" createdByUserName=\"bclayton\" createdTimestamp={ date=20120118 time=142502000 } completedByUserName=\"\" markedDirty=F sysId=00002f46.0a0b20fe.4f171c8e.00002a8c.87269 fwTimeStamp=27 extendedPropertiesSysId=\"\" activeChecklistType=\"\" comments={ } reasonCodes={ { class=ASSOC { class=A1 \"ActivityType\" } { class=A1 \"PM\" } } { \"CancellationReason\" \"NO_PROBLEM_FOUND\" } { \"SubActivityType\" \"MAINT\" } } attachments={ { class=ASSOC { class=A1 \"M07-00000494: AMAT Reflexion PM Spec\" } { class=XsiteAttachmentInt name=\"M07-00000494: AMAT Reflexion PM Spec\" description=\"\" attachmentTypeName=\"Windows Document\" param1=\"http://fc8sfcpv:8080/setupfc/pages/restricted/viewSpec.seam?SpecId=M07-00000494\" param2=\"\" param3=\"\" param4=\"\" param5=\"\" param6=\"\" param7=\"\" param8=\"\" param9=\"\" printWithTicket=F markedDirty=F sysId=00002f28.0a0b20fe.4f5503f5.00003369.226 fwTimeStamp=0 extendedPropertiesSysId=\"\" alternatePropertiesSysIds={ } } } } workRequests={ } checklists={ { class=XsiteChecklistInt name=\"AMAT REFLEXION CUPOL MONTHLY PM 08\" checklistType=\"PM\" checklistTemplateNumber=\"C0000947\" checklistTemplateVersion=2 shopName=\"CMP\" state=\"New\" completed=F completedBy=\"\" completedDate=NIL succeeded=F reasonCode=\"\" markedDirty=F sysId=00002f4c.0a0b20fe.4f5503f5.00003369.230 fwTimeStamp=0 extendedPropertiesSysId=\"\" alternatePropertiesSysIds={ } steps={ { class=ASSOC { class=S4 1 } { class=XsiteChecklistStepInt stepNumber=1 instructions=\"Open PM Spec for AMAT Reflexion Polishers\" estimatedElapsedSeconds=0 mandatory=F equipmentName=\"POL2700\" lastCompletedBy=\"\" lastCompletedDate=NIL lastCompletedOnWorkOrderNum=\"\" completed=F completedBy=\"\" completedDate=NIL succeeded=F reasonCode=\"\" actualElapsedSeconds=0 checklistTmplStepId=00002f61.59250b0a.4f512e6d.00001b30.80 workOrderNumber=\"P0010047\" edcPlanName=\"\" edcPlanVersion=0 edcPlanMustPass=F edcPlanPassed=F edcPlanInstanceId=\"\" ocapPlanName=\"\" ocapPlanVersion=0 ocapMustPass=F ocapPlanPassed=F ocapIncidentId=\"\" markedDirty=F sysId=00002f4f.0a0b20fe.4f5503f5.00003369.231 fwTimeStamp=0 extendedPropertiesSysId=\"\" attachments={ } meterTypesMandatory={ } meterTypesOptional={ } comments={ } compAttachments={ } readings={ } meterTypObjMandatory={ } meterTypObjOptional={ } alternatePropertiesSysIds={ } mandatorycomments=F stepRules={ } } } { 2 { 2 \"Open attached JBI for each step and follow instructions exactly\" 0 F \"POL2700\" \"\" NIL \"\" F \"\" NIL F \"\" 0 00002f61.59250b0a.4f512e6d.00001b30.81 \"P0010047\" \"\" 0 F F \"\" \"\" 0 F F \"\" F 00002f4f.0a0b20fe.4f5503f5.00003369.232 0 \"\" { } { } { } { } { } { } { } { } { } F { } } } { 3 { 3 \"Open Spec verification form and fill in necessary values during PM\" 0 F \"POL2700\" \"\" NIL \"\" F \"\" NIL F \"\" 0 00002f61.59250b0a.4f512e6e.00001b30.82 \"P0010047\" \"\" 0 F F \"\" \"\" 0 F F \"\" F 00002f4f.0a0b20fe.4f5503f5.00003369.233 0 \"\" { } { } { } { } { } { } { } { } { } F { } } } { 4 { 4 \"UPA Verification with Calibration; Wafer Slip Sensor Verification\" 0 F \"POL2700\" \"\" NIL \"\" F \"\" NIL F \"\" 0 00002f61.59250b0a.4f512e6e.00001b30.83 \"P0010047\" \"\" 0 F F \"\" \"\" 0 F F \"\" F 00002f4f.0a0b20fe.4f5503f5.00003369.234 0 \"\" { } { } { } { } { } { } { } { } { } F { } } } { 5 { 5 \"Pad Conditioner Down-force and Home Position Verification with Calibration\" 0 F \"POL2700\" \"\" NIL \"\" F \"\" NIL F \"\" 0 00002f61.59250b0a.4f512e6e.00001b30.84 \"P0010047\" \"\" 0 F F \"\" \"\" 0 F F \"\" F 00002f4f.0a0b20fe.4f5503f5.00003369.235 0 \"\" { } { } { } { } { } { } { } { } { } F { } } } { 6 { 6 \"Slurry and Chemical Flow Rate Verification with Calibration\" 0 F \"POL2700\" \"\" NIL \"\" F \"\" NIL F \"\" 0 00002f61.59250b0a.4f512e6f.00001b30.85 \"P0010047\" \"\" 0 F F \"\" \"\" 0 F F \"\" F 00002f4f.0a0b20fe.4f5503f5.00003369.236 0 \"\" { } { } { } { } { } { } { } { } { } F { } } } { 7 { 7 \"Megasonic Tank Roller Inspection\" 0 F \"POL2700\" \"\" NIL \"\" F \"\" NIL F \"\" 0 00002f61.59250b0a.4f512e6f.00001b30.86 \"P0010047\" \"\" 0 F F \"\" \"\" 0 F F \"\" F 00002f4f.0a0b20fe.4f5503f5.00003369.237 0 \"\" { } { } { } { } { } { } { } { } { } F { } } } { 8 { 8 \"Verify Cleaner Crescent Tilt and Vertical Speed\" 0 F \"POL2700\" \"\" NIL \"\" F \"\" NIL F \"\" 0 00002f61.59250b0a.4f512e72.00001b30.87 \"P0010047\" \"\" 0 F F \"\" \"\" 0 F F \"\" F 00002f4f.0a0b20fe.4f5503f5.00003369.238 0 \"\" { } { } { } { } { } { } { } { } { } F { } } } { 9 { 9 \"Factory Interface Robot Handling Verification\" 0 F \"POL2700\" \"\" NIL \"\" F \"\" NIL F \"\" 0 00002f61.59250b0a.4f512e72.00001b30.88 \"P0010047\" \"\" 0 F F \"\" \"\" 0 F F \"\" F 00002f4f.0a0b20fe.4f5503f5.00003369.239 0 \"\" { } { } { } { } { } { } { } { } { } F { } } } } } { \"AMAT REFLEXION CUPOL MONTHLY PM QUAL\" \"Qualification\" \"C0000766\" 4 \"CMP\" \"New\" F \"\" NIL F \"\" F 00002f4c.0a0b20fe.4f5503f5.00003369.240 0 \"\" { } { { class=ASSOC { class=S4 1 } { class=XsiteChecklistStepInt stepNumber=1 instructions=\"Open Op Spec for AMAT Reflexion Polishers and locate Section 9: Qualifications\" estimatedElapsedSeconds=0 mandatory=F equipmentName=\"POL2700\" lastCompletedBy=\"\" lastCompletedDate=NIL lastCompletedOnWorkOrderNum=\"\" completed=F completedBy=\"\" completedDate=NIL succeeded=F reasonCode=\"\" actualElapsedSeconds=0 checklistTmplStepId=00002f61.59250b0a.4f512e8b.00001b30.147 workOrderNumber=\"P0010047\" edcPlanName=\"\" edcPlanVersion=0 edcPlanMustPass=F edcPlanPassed=F edcPlanInstanceId=\"\" ocapPlanName=\"\" ocapPlanVersion=0 ocapMustPass=F ocapPlanPassed=F ocapIncidentId=\"\" markedDirty=F sysId=00002f4f.0a0b20fe.4f5503f5.00003369.241 fwTimeStamp=0 extendedPropertiesSysId=\"\" attachments={ } meterTypesMandatory={ } meterTypesOptional={ } comments={ } compAttachments={ } readings={ } meterTypObjMandatory={ } meterTypObjOptional={ } alternatePropertiesSysIds={ } mandatorycomments=F stepRules={ } } } { 2 { 2 \"Select correct Qualification Plan\" 0 F \"POL2700\" \"\" NIL \"\" F \"\" NIL F \"\" 0 00002f61.59250b0a.4f512e8c.00001b30.148 \"P0010047\" \"\" 0 F F \"\" \"\" 0 F F \"\" F 00002f4f.0a0b20fe.4f5503f5.00003369.242 0 \"\" { } { } { } { } { } { } { } { } { } F { } } } { 3 { 3 \"Use TW Route IDs, TW Product IDs, and Tool Recipes to run the quantity of wafers shown on the table\" 0 F \"POL2700\" \"\" NIL \"\" F \"\" NIL F \"\" 0 00002f61.59250b0a.4f512e8c.00001b30.149 \"P0010047\" \"\" 0 F F \"\" \"\" 0 F F \"\" F 00002f4f.0a0b20fe.4f5503f5.00003369.243 0 \"\" { } { } { } { } { } { } { } { } { } F { } } } } } } estimates={ { class=XsiteWorkOrderEstimateInt txnSequence=1326502 txnType=\"Resource\" resourceCategoryName=\"GF Technician\" assignedToUserName=\"\" assignedToEquipmentName=\"\" catalogName=\"GF Technician\" warehouseName=\"\" partNumber=\"\" dateRequired={ date=20120809 time=100000000 } dateCriticalAction={ date=20120809 time=100000000 } leadTimeDays=0 description=\"Globalfoundries Technician\" vendorName=\"\" quantityRequired=4 unitOfMeasureName=\"Hours\" unitCost=0 costModifierName=\"\" costCodeName=\"123.456\" costCategoryName=\"Labor\" storesTxnId=\"\" txnComment=NIL enteredByUser=\"bclayton\" enteredTimeStamp={ date=20120305 time=132037000 } state=\"New\" markedDirty=F sysId=00002f55.0a0b20fe.4f5503f5.00003369.244 fwTimeStamp=0 extendedPropertiesSysId=\"\" requirementNumber=\"\" reservationNumber=\"\" orderNumber=\"\" location=\"\" serialNumber=\"\" actuals={ } alternatePropertiesSysIds={ } } } actuals={ } txnHistory={ } estimatedCosts={ { class=ASSOC { class=A1 \"Labor\" } { class=F8 0 } } } actualCosts={ } compAttachments={ } alternatePropertiesSysIds={ } } }"

sample = "(testreply{:448074)::(:0):://l 0 XsiteGetScheduleTxn : value={ class=SORTED { class=XsiteSchedule action=1 timeStamp={ { class=TestClass name=\"abcd\" } { \"efg\" } } } }"

begin
  res = Xsite::Parser.new.parse(sample)
  puts res.inspect
rescue Exception => e
  $log.error "#{e}\n" + e.backtrace.join("\n")
end
#  return res
end
