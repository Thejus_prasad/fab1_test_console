=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

    
Author: Steffen Steidten, 2011-11-04

History:
  2012-01-24 ssteidte,  use Sequel instead of DBI for databases
  2012-08-23 ssteidte,  remove to_char conversion from queries for Date and Time fields
  2013-02-12 ssteidte,  pd_reference returns first set of PDs as default, parameter ACTIVE defaults to 'Y'
  2015-07-27 ssteidte,  moved globals to constants
=end

require 'dbsequel'


module Space

  # SIL DB queries
  class SILDB
    SpcRunResult = Struct.new(:runid, :errcode, :errmsg, :storetime)

    SpcRunUnitReading = Struct.new(:readingid, :parametername, :measurementvalue, 
          :string_value, :isempty, :flashid, :flashx, :flashy, :siteid, :sitex, :sitey, 
          :dieid, :diex, :diey, :equipment_timestamp, :booleanvalue, :readingvalid, 
          :parametervalid, :parameter_invalid_reason, :readingnumber, :processing_area, 
          :subprocessing_area, :nocharting, :spacelisteningonly, :contextid, :storetime)
          
    SpcRunUnit = Struct.new(:contextid, :mainprocess, :moduleprocess, :process, :operationnumber, 
          :operationname, :technology, :productgroup, :product, :foup, :batch, :lotid, 
          :waferid, :slot, :port, :buffer, :processingarea, :subprocessingarea, 
          :controljob, :processjob, :reticle, :rmsrecipe, :machinerecipe, :operationpasscount, 
          :logicalrecipe, :manufacturinglayer, :photolayer, :component, :sap_order_number, 
          :sap_order_operation, :sap_insp_characteristic_id, :maskedlotid, :customer, 
          :department, :runid, :storetime, :sublottype)

    SpcRun = Struct.new(:runid, :facility, :user, :eqp, :ts, :dcp, :sapdcp, :event, :storetime, :e10)

    SpcProcessingArea = Struct.new(:runid, :processingarea, :eqp, :storetime, :e10)

    SpcChartLink = Struct.new(:runid, :channelid, :ckcid, :sid, :storetime, :channel_name, :ooc, :parameter_name, :limit_id, :ckc_name, :channel_state)

    SpcAutoCharting = Struct.new(:refid, :parameter, :module, :lds, :template, 
      :specid, :specversion, :active, :deactivated, :scenario, :comment, :storetime)

    SpcPDRef = Struct.new(:refid, :parameter, :eqp, :component, :pa, :mpd, 
      :specid, :specversion, :active, :deactivated, :scenario, :product, :productgroup, :comment, :storetime, :nexttestpd, :pdrefs)   
          
    SpcSpecLimit = Struct.new(:refid, :parametername, :pd, :technology, :route, :product, :productgroup, :unit, :usl, :lsl, :target, :storetime, 
      :commitcomment, :specaggr, :criticalitylevel, :erroraction, :designtag, :withinreticalselection, :parameterdescription, :numberofsites, 
      :specid, :specversion, :active, :deactivatedtime, :spacescenario, :module, :operationnumber, :customercriticality, :jcalpara, :productmodule, 
      :yield, :rel, :storetime2, :deactivatedtime2, :testcategory, :priority, :macroout, :b2b, :customerparamname)   
          
    SpcMonitoringSpecLimit = Struct.new(:refid, :parameter, :technology, :route, :productgroup, :product, 
      :ppd, :plogrecipe, :ptool, :pchamber, :mpd, :mtool, :mslot, :mlogrecipe, :event, :sapinspectioncharacteristic,
      :reticle, :photolayer, :reserve1, :reserve2, :reserve3, :reserve4, :reserve5, :reserve6, :reserve7, :reserve8,
      :datreserve1, :datreserve2, :datreserve3, :datreserve4, :datreserve5, :datreserve6, :datreserve7, :datreserve8,
      :qualdesc, :parameterdesc, :linktopcp, :lsl, :target, :usl, :unit, :speclimitsvalidfor, :criticality,
      :customercriticality, :spcoosaction, :numberofsites, :designtag, :withinreticleselectionrule, :spacescenario,
      :metrologyequipmenttype, :frequency, :frequencylot, :frequencywaferperlot, :specid, :specversion,
      :active, :deactivatedtime, :storetime, :commitcomment, :entrynumber, :greenzonetype, :greenzonevalue)
         
    SpcGenericKey = Struct.new(:keyid, :module, :name, :method, :fixed_property, :resppd, :parameter, :ldsvalue,
      :storetime, :comment, :specid, :specversion, :active, :deactivated, :scenario, :lds, :group)

    SpcChamberFilter = Struct.new(:refid, :lds, :ptool, :specid, :specversion, :active, :deactivated, :scenario, :comment, :storetime, :filter)


    attr_accessor :env, :db
    
    def initialize(env, params={})
      @env = env
      @dbschema = "SIL_ADMIN"
      @db = DB.new("sil.#{@env}", {schema: @dbschema}.merge(params))
    end
    
    def autocharting(params={})
      qargs = []
      q = 'select ID, PARAMETER_VALUE, MODULE_VALUE, LDS_VALUE, TEMPLATE_VALUE, SPEC_ID, SPEC_VERSION, ACTIVE, 
        DEACTIVATED_TIME, SPACE_SCENARIO, COMMIT_COMMENT, STORETIME from AUTO_CHARTING_INFO'
      q, qargs = @db._q_restriction(q, qargs, "PARAMETER_VALUE", params[:parameter])
      q, qargs = @db._q_restriction(q, qargs, "COMMIT_COMMENT", params[:comment])
      q, qargs = @db._q_restriction(q, qargs, "SPEC_ID", params[:specid])
      q, qargs = @db._q_restriction(q, qargs, "SPEC_VERSION", params[:specversion])
      q, qargs = @db._q_restriction(q, qargs, "MODULE_VALUE", params[:module])
      q += " ORDER BY ID"
      qargs.unshift(q)
      res = @db.select_all(*qargs) || return
      return res if params[:raw]
      return res.collect {|r|
        e = SpcAutoCharting.new(*r)
        e.refid = e.refid.to_i
        e.deactivated = e.deactivated.to_i
        e.storetime = e.storetime.to_i
        e
      }
    end
    
    def chamberfilter(params={})
      qargs = []
      q = 'select ID, LDS_VALUE, PTOOL_VALUE, SPEC_ID, SPEC_VERSION, ACTIVE, DEACTIVATED_TIME, 
        SPACE_SCENARIO, COMMIT_COMMENT, STORETIME from CHAMBER_FILTER_INFO'
      q, qargs = @db._q_restriction(q, qargs, "PTOOL_VALUE", params[:ptool])
      q, qargs = @db._q_restriction(q, qargs, "COMMIT_COMMENT", params[:comment])
      q, qargs = @db._q_restriction(q, qargs, "SPEC_ID", params[:specid])
      q, qargs = @db._q_restriction(q, qargs, "SPEC_VERSION", params[:specversion])
      q += " ORDER BY ID"
      qargs.unshift(q)
      res = @db.select_all(*qargs) || return
      return res if params[:raw]
      return res.collect {|r|
        c = SpcChamberFilter.new(*r)
        c.refid = c.refid.to_i
        c.deactivated = c.deactivated.to_i
        c.storetime = c.storetime.to_i
        qargs = []
        q = 'select FILTER from CHAMBER_FILTER_VALUES inner join CHAMBER_FILTER_INFO
              on CHAMBER_FILTER_VALUES.CHAMBER_FILTER_GROUPING_ID = CHAMBER_FILTER_INFO.ID'
        q, qargs = @db._q_restriction(q, qargs, "CHAMBER_FILTER_INFO.ID", c.refid)      
        q += " ORDER BY CHAMBER_FILTER_VALUES.FILTER"
        qargs.unshift(q)
        filter = @db.select_all(*qargs)
        c.filter = filter.flatten
        c
      }
    end
    
    def space_chartlink(params={})
      qargs = []
      q = 'select RUN_ID, CHANNEL_ID, CKC_ID, SAMPLE_ID, STORETIME, CHANNEL_NAME, OOC, PARAMETER_NAME, 
        SPEC_LIMIT_ID, CKC_NAME, CHANNEL_STATE from SPACE_CHART_LINK'
      q, qargs = @db._q_restriction(q, qargs, "RUN_ID", params[:runid])
      q, qargs = @db._q_restriction(q, qargs, "CHANNEL_ID", params[:chid])
      q, qargs = @db._q_restriction(q, qargs, "SAMPLE_ID", params[:sid])
      q, qargs = @db._q_restriction(q, qargs, "STORETIME > ", params[:tstart])
      q, qargs = @db._q_restriction(q, qargs, "STORETIME < ", params[:tend])
      qargs.unshift(q)
      res = @db.select_all(*qargs) || return
      return res if params[:raw]
      return res.collect {|r|
        e = SpcChartLink.new(*r)
        e.runid = e.runid.to_i
        e.channelid = e.channelid.to_i
        e.ckcid = e.ckcid.to_i
        e.sid = e.sid.to_i        
        e
      }
    end

    def readings_in_rununitreadings(runid)
      q = "SELECT count(readingid) FROM rununitreading where contextid in (SELECT CONTEXTID FROM rununit WHERE RUNID = ?)"
      res = @db.select_all(q, runid) || return
      return res[0][0]
    end

    # return sorted array of sample ids belonging to the run id
    def runid_samples(runid, params={})      
      res = space_chartlink(runid: runid) || return
      res.collect {|e| e.sid}.uniq.sort
    end
    
    # return referenced PDs as array of the first matching set of PDs, 
    # 
    # pass :all=>true to get an array of all matches as _spc_pdref structs
    def pd_reference(params={})
      qargs = []
      q = 'select ID, PARAMETER_NAME, EQUIPMENT_NAME, COMPONENT_NAME, PROCESSINGAREA_NAME, METROLOGY_PD, SPEC_ID, 
        SPEC_VERSION, ACTIVE, DEACTIVATED_TIME, SPACE_SCENARIO, PRODUCT, PRODUCT_GROUP, COMMIT_COMMENT, STORETIME, NEXT_TEST_PD from PD_REFERENCE'
      q, qargs = @db._q_restriction(q, qargs, "COMMIT_COMMENT", params[:comment])
      q, qargs = @db._q_restriction(q, qargs, "METROLOGY_PD", params[:mpd])
      q, qargs = @db._q_restriction(q, qargs, "SPEC_ID", params[:specid])
      q, qargs = @db._q_restriction(q, qargs, "SPEC_VERSION", params[:specversion])
      q, qargs = @db._q_restriction(q, qargs, "ACTIVE", (params[:active] || 'Y'))
      q += " ORDER BY ID"
      qargs.unshift(q)
      res = @db.select_all(*qargs) || return
      return res if params[:raw]
      ret = res.collect {|r|
        e = SpcPDRef.new(*r)
        e.refid = e.refid.to_i
        e.deactivated = e.deactivated.to_i
        e.storetime = e.storetime.to_i
        qargs = []
        q = 'select PROCESS_PD, PD_REFERENCE_GROUPING.GROUP_ORDER from PD_REFERENCE_VALUE
              inner join PD_REFERENCE_GROUPING
              on PD_REFERENCE_VALUE.PD_REFERENCE_GROUPING_ID = PD_REFERENCE_GROUPING.ID'
        q, qargs = @db._q_restriction(q, qargs, "PD_REFERENCE_GROUPING.PD_REFERENCE_ID", e.refid)    
        q += " ORDER BY GROUP_ORDER"
        qargs.unshift(q)
        pdgroups = @db.select_all(*qargs)
        e.pdrefs = []
        pdgroups.each {|p, g|
          index = g.to_i
          e.pdrefs.push([]) if e.pdrefs.length <= index        
          e.pdrefs[index].push(p)
        }
        e
      }
      return ret if params[:all]
      return nil if ret.first.nil?
      return ret.first.pdrefs.flatten
    end
  
    def run(runid, params={})
      q = 'select RUNID, FACILITY, OPERATOR, EQUIPMENT, TIMESTAMP, DCPNAME, SAPDCPID, EVENT, STORETIME, EQUIPMENT_E10_STATE from RUN where RUNID = ?'
      res = @db.select_all(q, runid) || return
      return res if params[:raw]
      return res.collect {|r|
        e = SpcRun.new(*r)
        e.runid = e.runid.to_i
        e
      }
    end
  
    def processingarealookup(runid, params={})
      q = 'select RUNID, PROCESSINGAREA, EQUIPMENT, STORETIME, PROCESSINGAREA_E10_STATE from PROCESSINGAREALOOKUP where RUNID = ?'
      res = @db.select_all(q, runid) || return
      return res if params[:raw]
      return res.collect {|r|
        e = SpcProcessingArea.new(*r)
        e.runid = e.runid.to_i
        e
      }
    end
    
    def spcrunresult(runid, params={})
      q = 'select RUNID, NONOOCERRORCODE, NONOOCERRORMESSAGE, STORETIME from SPCRUNRESULT where RUNID = ?'
      res = @db.select_all(q, runid) || return
      return res if params[:raw]
      return res.collect {|r|
        e = SpcRunResult.new(*r)
        e.runid = e.runid.to_i
        e
      }
    end
    
    def rununit(runid, params={})
      q = 'select CONTEXTID, MAINPROCESS, MODULEPROCESS, PROCESS, OPERATIONNUMBER, OPERATIONNAME, TECHNOLOGY, 
        PRODUCTGROUP, PRODUCT, FOUP, BATCH, LOTID, WAFERID, SLOT, PORT, BUFFER, PROCESSINGAREA, SUBPROCESSINGAREA, 
        CONTROLJOB, PROCESSJOB, RETICLE, RMSRECIPE, MACHINERECIPE, OPERATIONPASSCOUNT, LOGICALRECIPE, 
        MANUFACTURINGLAYER, PHOTOLAYER, COMPONENT, SAP_ORDER_NUMBER, SAP_ORDER_OPERATION, 
        SAP_INSP_CHARACTERISTIC_ID, MASKEDLOTID, CUSTOMER, DEPARTMENT, RUNID, STORETIME, SUBLOTTYPE 
        from RUNUNIT where RUNID = ? and WAFERID is not null ORDER BY WAFERID'
      res = @db.select_all(q, runid) || return
      return res if params[:raw]
      return res.collect {|r|
        e = SpcRunUnit.new(*r)
        e.runid = e.runid.to_i
        e
      }
    end
    
    def runrunitreading(contextid, params={})
      q = 'SELECT readingid, parametername, measurementvalue, string_value, isempty, flashid, flashx, flashy, siteid, sitex, sitey, 
        dieid, diex, diey, equipment_timestamp, booleanvalue, readingvalid, parametervalid, parameter_invalid_reason, readingnumber, 
        processing_area, subprocessing_area, nocharting, spacelisteningonly, contextid, storetime 
        FROM RUNUNITREADING where CONTEXTID = ?'
      res = @db.select_all(q, contextid) || return
      return res if params[:raw]
      return res.collect {|r|
        e = SpcRunUnitReading.new(*r)
        e.contextid = e.contextid.to_i
        e
      }
    end 
      
    def speclimit(params={})
      qargs = []
      q = 'select ID, PARAMETER_NAME, PD, TECHNOLOGY, ROUTE, PRODUCT, PRODUCT_GROUP, UNIT, USL, LSL, TARGET, STORETIME, COMMIT_COMMENT, 
        SPEC_AGGREGATE, CRITICALITY_LEVEL, ERROR_ACTION, DESIGN_TAG, WITHIN_RETICAL_SELECTION, PARAMETER_DESCRIPTION, NUMBER_OF_SITES, SPEC_ID, 
        SPEC_VERSION, ACTIVE, DEACTIVATED_TIME, SPACE_SCENARIO, MODULE, OPERATIONNUMBER, CUSTOMER_CRITICALITY, JCALPARA, PRODUCT_MODULE,
        YIELD, REL, STORETIME2, DEACTIVATED_TIME2, TEST_CATEGORY, PRIORITY, MACRO_DUT, B2B, CUSTOMER_PARAM_NAME from SPEC_LIMIT'
      q, qargs = @db._q_restriction(q, qargs, "COMMIT_COMMENT", params[:commitcomment])
      q, qargs = @db._q_restriction(q, qargs, "SPEC_ID", params[:specid])
      q, qargs = @db._q_restriction(q, qargs, "PARAMETER_NAME", params[:parametername])
      q, qargs = @db._q_restriction(q, qargs, "PD", params[:pd])
      q, qargs = @db._q_restriction(q, qargs, "TECHNOLOGY", params[:technology])
      q, qargs = @db._q_restriction(q, qargs, "ROUTE", params[:route])
      q, qargs = @db._q_restriction(q, qargs, "PRODUCT", params[:product])
      q, qargs = @db._q_restriction(q, qargs, "PRODUCT_GROUP", params[:productgroup])
      q, qargs = @db._q_restriction(q, qargs, "SPEC_VERSION", params[:specversion])
      q, qargs = @db._q_restriction(q, qargs, "MODULE", params[:module])
      q, qargs = @db._q_restriction(q, qargs, "ACTIVE", params[:active])
      q += " ORDER BY ID"
      qargs.unshift(q)
      res = @db.select_all(*qargs) || return
      return res if params[:raw]
      return res.collect {|r|
        e = SpcSpecLimit.new(*r)
        e.usl = e.usl.to_f if e.usl
        e.lsl = e.lsl.to_f if e.lsl
        e.target = e.target.to_f # a nil value will be casted to 0.0!
        e.numberofsites = e.numberofsites.to_f
        e.refid = e.refid.to_i
        e.deactivatedtime = e.deactivatedtime.to_i
        e.storetime = e.storetime.to_i
        e
      }
    end
    
    def monitoringspeclimit(params={})
      qargs = []
      q = 'select ID, PARAMETER, TECHNOLOGY, ROUTE, PRODUCT_GROUP, PRODUCTID_EC, POPERATION_ID, PLOG_RECIPE, 
        PTOOL, PCHAMBER, MOPERATION, MTOOL, MSLOT, MLOG_RECIPE, EVENT, SAP_INSPECTION_CHARACTERISTIC, RETICLE,
				PHOTOLAYER, RESERVE1, RESERVE2, RESERVE3, RESERVE4, RESERVE5, RESERVE6, RESERVE7, RESERVE8, DAT_RESERVE1, 
        DAT_RESERVE2, DAT_RESERVE3, DAT_RESERVE4, DAT_RESERVE5, DAT_RESERVE6, DAT_RESERVE7, DAT_RESERVE8,
				QUAL_DESC, PARAMETER_DESC, LINK_TO_PCP, LSL, TARGET, USL, UNIT, SPEC_LIMITS_VALID_FOR, CRITICALITY,
				CUSTOMER_CRITICALITY, SPC_OOS_ACTION, NUMBER_OF_SITES, DESIGN_TAG, WITHIN_RETICLE_SELECTION_RULE, 
        SPACE_SCENARIO, METROLOGY_EQUIPMENT_TYPE, FREQUENCY, FREQUENCY_LOT, FREQUENCY_WAFER_PER_LOT, SPEC_ID, 
        SPEC_VERSION, ACTIVE, DEACTIVATED_TIME, STORETIME, COMMIT_COMMENT, ENTRY_NUMBER,
        GREEN_ZONE_TYPE, GREEN_ZONE_VALUE from MONITORING_SPEC_LIMITS'
      q, qargs = @db._q_restriction(q, qargs, "COMMIT_COMMENT", params[:comment])
      q, qargs = @db._q_restriction(q, qargs, "SPEC_ID", params[:specid])
      q, qargs = @db._q_restriction(q, qargs, "PARAMETER", params[:parameter])
      q, qargs = @db._q_restriction(q, qargs, "MOPERATION", params[:pd])
      q, qargs = @db._q_restriction(q, qargs, "TECHNOLOGY", params[:technology])
      q, qargs = @db._q_restriction(q, qargs, "ROUTE", params[:route])
      q, qargs = @db._q_restriction(q, qargs, "PRODUCTID_EC", params[:product])
      q, qargs = @db._q_restriction(q, qargs, "PRODUCT_GROUP", params[:productgroup])
      q, qargs = @db._q_restriction(q, qargs, "SPEC_VERSION", params[:specversion])
      q, qargs = @db._q_restriction(q, qargs, "ACTIVE", params[:active])
      q += " ORDER BY ID"
      qargs.unshift(q)
      res = @db.select_all(*qargs) || return
      return res if params[:raw]
      return res.collect {|r|
        e = SpcMonitoringSpecLimit.new(*r)
        e.usl = e.usl.to_f if e.usl
        e.lsl = e.lsl.to_f if e.lsl
        e.target = e.target.to_f # a nil value will be casted to 0.0!
        e.numberofsites = e.numberofsites.to_f
        e.refid = e.refid.to_i
        e.deactivatedtime = e.deactivatedtime.to_i
        e.storetime = e.storetime.to_i
        e
      }
    end
    
    def greenzonevalue(params={})
      qargs = []
      q = 'select ID, PARAMETER, TECHNOLOGY, ROUTE, PRODUCT_GROUP, PRODUCTID_EC, POPERATION_ID, PLOG_RECIPE, 
        PTOOL, PCHAMBER, MOPERATION, MTOOL, MSLOT, MLOG_RECIPE, EVENT, SAP_INSPECTION_CHARACTERISTIC, RETICLE,
				PHOTOLAYER, RESERVE1, RESERVE2, RESERVE3, RESERVE4, RESERVE5, RESERVE6, RESERVE7, RESERVE8, DAT_RESERVE1, 
        DAT_RESERVE2, DAT_RESERVE3, DAT_RESERVE4, DAT_RESERVE5, DAT_RESERVE6, DAT_RESERVE7, DAT_RESERVE8,
				QUAL_DESC, PARAMETER_DESC, LINK_TO_PCP, LSL, TARGET, USL, UNIT, SPEC_LIMITS_VALID_FOR, CRITICALITY,
				CUSTOMER_CRITICALITY, SPC_OOS_ACTION, NUMBER_OF_SITES, DESIGN_TAG, WITHIN_RETICLE_SELECTION_RULE, 
        SPACE_SCENARIO, METROLOGY_EQUIPMENT_TYPE, FREQUENCY, FREQUENCY_LOT, FREQUENCY_WAFER_PER_LOT, SPEC_ID, 
        SPEC_VERSION, ACTIVE, DEACTIVATED_TIME, STORETIME, COMMIT_COMMENT, ENTRY_NUMBER,
        GREEN_ZONE_TYPE, GREEN_ZONE_VALUE from MONITORING_SPEC_LIMITS'
      q, qargs = @db._q_restriction(q, qargs, "PARAMETER", params[:parameter])
      q, qargs = @db._q_restriction(q, qargs, "GREEN_ZONE_TYPE", params[:gzt])
      q, qargs = @db._q_restriction(q, qargs, "SPEC_ID", params[:specid])
      q, qargs = @db._q_restriction(q, qargs, "SPEC_VERSION", params[:specversion])
      q, qargs = @db._q_restriction(q, qargs, "ACTIVE", params[:active])
      q += " ORDER BY ID"
      qargs.unshift(q)
      res = @db.select_all(*qargs) || return
      return res if params[:raw]
      return res.collect {|r|
        e = SpcMonitoringSpecLimit.new(*r)
        e.usl = e.usl.to_f if e.usl
        e.lsl = e.lsl.to_f if e.lsl
        e.target = e.target.to_f # a nil value will be casted to 0.0!
        e.greenzonevalue = e.greenzonevalue.to_f
        e.refid = e.refid.to_i
        e
      }
    end

    def generickey(params={})
      qargs = []
      q = 'select ID, MODULE, KEY_NAME, METHOD, FIXED_PROPERTY, RESP_PD_INDEX, PARAMETER_NAME, LDS_VALUE, STORETIME, 
        COMMIT_COMMENT, SPEC_ID, SPEC_VERSION, ACTIVE, DEACTIVATED_TIME, SPACE_SCENARIO, LDS_TYPE FROM GENERIC_KEY'
      q, qargs = @db._q_restriction(q, qargs, "KEY_NAME", params[:name])
      q, qargs = @db._q_restriction(q, qargs, "METHOD", params[:method])
      q, qargs = @db._q_restriction(q, qargs, "COMMIT_COMMENT", params[:comment])
      q, qargs = @db._q_restriction(q, qargs, "SPEC_ID", params[:specid])
      q, qargs = @db._q_restriction(q, qargs, "PARAMETER_NAME", params[:parameter])
      q, qargs = @db._q_restriction(q, qargs, "MODULE", params[:department])
      q, qargs = @db._q_restriction(q, qargs, "ACTIVE", params[:active])
      q, qargs = @db._q_restriction(q, qargs, "LDS_TYPE", params[:lds])
      q += " ORDER BY ID"
      qargs.unshift(q)
      res = @db.select_all(*qargs) || return
      return res if params[:raw]
      return res.collect {|r|
        e = SpcGenericKey.new(*r) # group attribute is not filled and should stay nil
        e.keyid = e.keyid.to_i
        e.resppd = e.resppd.to_i if e.resppd
        e.deactivated = e.deactivated.to_i
        e.storetime = e.storetime.to_i
        e.group = generickey_group(e.keyid) if e.method == 'GROUP'
        e
      }
    end
    
    def generickey_group(keyid, params={})
      qargs = []
      q = "SELECT PATTERN, SUBSTITUTION from GENERIC_KEY_GROUP"
      q, qargs = @db._q_restriction(q, qargs, "KEY_ID", keyid)
      q += " ORDER BY PRIO"
      qargs.unshift(q)
      res = @db.select_all(*qargs) || return
      return res if params[:raw]
      h = {}
      res.each do |p,s|
        if h.has_key?(s)
          h[s] << p
        else
          h[s] = [p]
        end
      end
      return h
    end
    
    def perfdata(method, params={})
      tend = params[:tend] || Time.now
      tend = Time.parse(tend) if tend.kind_of?(String)
      tstart = params[:tstart] || (tend - 86400 * 33)
      tstart = Time.parse(tstart) if tstart.kind_of?(String)
      qargs = [tstart, tend]
      q = "SELECT TIMESTAMP, MEAN FROM FBPERFDATA WHERE TIMESTAMP >= ? and TIMESTAMP <= ?"
      q, qargs = @db._q_restriction(q, qargs, "METHOD", method)
      q += " ORDER BY TIMESTAMP"
      qargs.unshift(q)
      res = @db.select_all(*qargs) || return
      return res if params[:raw]
      res.collect {|ts, mean| [ts, mean.to_f]}
    end

  end

end
