=begin

(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

    
Author: Steffen Steidten, 2012-05-09

History:
  2012-12-17 ssteidte,  separate file for AsmERPShipment job testing
=end

require 'asmviewjcap'

module AsmView
  # jCAP AsmTurnkey job
  
  # AsmView TurnKey B2B interface
  #
  # see http://myteamsmst/sites/MST/APM/APMI/SG/Shared%20Documents/MSR527514%20Unitlevel%20Turnkey%20Tracking/Development/interfaces/AsmTurnkeyB2B/AsmTurnkeyB2B_SoapItf.docx
  class TurnKeyB2B < JCAP
    
    def initialize(env, params={})
      super(env, {instance: "asmview.b2b.#{env}"}.merge(params))
    end

    def _add_binning_info(xml, urn1, urn2, tag, bins)
      # bins is a $_asm_diecount struct, see asmview.rb
      return unless bins
      bins.each {|b|
        xml.tag! urn1, tag do |bi|
          bi.tag! urn2, :binNo, b.bin
          bi.tag! urn2, :binProductId, b.product
          bi.tag! urn2, :qty, b.qty
        end
      }
    end
    
    def _action(lot, action, params={})
      refId = params[:refid] || "QA_TEST_#{Time.now.iso8601}"
      li = params[:lotinfo] || @av.lot_info(lot, wafers: true)
      partName = params[:erpitem] || @av.user_parameter('Lot', lot, name: 'ERPItem').value
      partName = partName[1..-1] if partName.start_with?('*')
      subcons = params[:subcons] || (params[:tkinfo] || lot_tkinfo(lot)).to_a[1..-1].compact
      subcons = subcons.split if subcons.kind_of?(String)
      fabcode = params[:fabcode] || lot_fabcode(lot)
      lotcontent = params[:lotcontent] || li.content
      op = params[:op] 
      op = li.op if op == :current
      op ||=  ["start", "move", "complete"].member?(action) ? @av._guess_op_route(lot, 1).first.op : li.op
      lotState = params[:lotstate] || params[:state] || 'RUN'
      eventTime = params[:eventtime] || Time.now
      eventTime = eventTime.utc.iso8601(3).tr('-:', '') if eventTime.kind_of?(Time)
      $log.info "#{action} #{lot.inspect} #{op.inspect}"
      #
      xml = Builder::XmlMarkup.new(indent: 2)
      xml.instruct!
      xml.soapenv :Envelope, "xmlns:soapenv"=>"http://www.w3.org/2003/05/soap-envelope",
        "xmlns:urn1"=>"urn:AsmTurnkeyB2BOperations", "xmlns:urn2"=>"urn:AsmTurnkeyB2BTypes" do |enve|
        enve.soapenv :Body do |body|
          body.urn1 action.intern do |a|
            a.urn1 :refId, refId
            a.urn1 :opInfo do |opinfo|
              opinfo.urn2 :currentStage, op
              opinfo.urn2 :eventTime, eventTime
              opinfo.urn2 :fabCode, fabcode
              opinfo.urn2 :lotContent, lotcontent
              opinfo.urn2 :partName, partName
              subcons.each {|s| a.urn2 :subconIdList, s if s and s != ""}
            end
            a.urn1 :lotId, lot
            a.urn1 :lotState, lotState if ["start", "move", "complete"].member?(action)
            a.urn1 :childLotId, params[:child] if ["splitWaferLotWithId", "merge"].member?(action)
            #
            nwafers = params[:nwafers] || li.nwafers
            if action == "merge"
              cli = @av.lot_info(params[:child])
              nwafers +=  cli.nwafers
            end
            bins = params[:bins]
            wbins = params[:wbins]
            dcg = params[:dcg]
            #
            if ["splitWaferLot", "splitWaferLotWithId", "scrapWaferLot"].member?(action)
              a.urn1 :waferQty, nwafers
            else
              units = params[:units]
              units ||= bins ? bins.inject(0) {|s, b| s + b.qty} : nwafers
              a.urn1 :unitQty, units
            end
            #
            if ["move", "complete"].member?(action)
              if params[:waferinfo] or params[:dcg] or params[:wbins]
                # optional
                ll = lot.split('.')[0]
                li.wafers.each {|w|
                  a.urn1 :waferInfo do |wi|
                    wi.urn2 :waferId, w.alias
                    _add_binning_info(a, "urn2", "urn2", :binningInfo, wbins)
                    wi.urn2 :dieQty, dcg if dcg
                  end
                }
              end
              # optional array of $_asm_diecount (may be emptyto delete), for chip lots
              _add_binning_info(a, "urn1", "urn2", :binningInfo, bins)
            elsif ["splitWaferLot", "splitWaferLotWithId"].member?(action)
              params[:wafers].each {|w| 
                a.urn1 :splitWaferInfo do |wi|
                  wi.urn2 :waferId, w
                  _add_binning_info(a, "urn2", "urn2", :splitBinningInfo, wbins)
                  # not accepted: wi.urn2 :dieQty, dcg if dcg
                end
              }
            elsif action == "splitChipLot"
              a.urn1 :childUnitQty, params[:chips]
              _add_binning_info(a, "urn1", "urn2", :splitBinningInfo, bins)
            elsif action == "hold"
              a.urn1 :holdCode, params[:holdcode]
              holdreason = params[:holdreason]
              holdreason = "QA Test" unless params.has_key?(:holdreason)
              a.urn1 :holdReason, holdreason
            elsif action == "release"
              a.urn1 :holdEventTime, params[:holdtime]
            elsif action ==  "merge"
              a.urn1 :childUnitQty, cli.nwafers
            elsif action == "scrapWaferLot"
              scrapreason = params[:scrapreason]
              scrapreason = "QA Test" unless params.has_key?(:scrapreason)
              a.urn1 :scrapReason, scrapreason
              li.wafers.each {|w|
                a.urn1 :waferInfo do |winfo|
                  winfo.urn2 :waferId, w.alias
                  winfo.urn2 :scrapCode, params[:scrapcode]
                end
              }
            elsif action == "scrapChipLot"
              a.urn1 :scrapUnitInfo do |uinfo|
                uinfo.urn2 :scrapCode, params[:scrapcode]
                uinfo.urn2 :unitCount, li.nwafers
              end
              scrapreason = params[:scrapreason]
              a.urn1 :scrapReason, scrapreason if scrapreason
            end
          end
        end
      end
      raw = params.delete(:raw)
      res = _send_receive(xml.target!, params)
      return nil unless res
      return res if params[:nosend] or raw
      ($log.warn "  error: #{res.inspect}"; return nil) unless res[res.keys[0]][:return]
      r = res[res.keys[0]][:return][:result]
      r = r[nil] if r.kind_of?(Hash)
      ret = (r == "TURNKEY_B2B_SERVICE_OK")
      $log.warn res unless ret
      return ret
    end
    
    # start TKEY moves
    def start(lot, params={})
      _action(lot, "start", params)
    end
    
    # load lot and start processing
    def move(lot, params={})  
      _action(lot, "move", params)
    end
    
    # end processing
    def complete(lot, params={})      
      _action(lot, "complete", params)
    end
    
    # holdcodes are configured properties: PPD,PCD,PSORT,RSORT
    def hold(lot, holdcode, params={})
      _action(lot, "hold", params.merge(:holdcode=>holdcode))
    end
    
    def release(lot, params={})
      htime = params[:holdtime]
      if htime
        htime = htime.utc.iso8601(3).tr('-:', '') if htime.kind_of?(Time)
      else
        # get hold time from claim memo
        h = @av.lot_hold_list(lot)[0]
        ($log.warn "release: no hold to release"; return nil) unless h
        s = h.memo
        htime = s[8..26] + "Z"
      end
      _action(lot, "release", params.merge(:holdtime=>htime))
    end
    
    # return child lot
    def split_waferlot(lot, wafers, params={})
      # return child lot
      if wafers.kind_of?(String)
        waferids = wafers.split
      elsif wafers.kind_of?(Numeric)
        waferids = @av.lot_info(lot, :wafers=>true).wafers[0...wafers].collect {|w| w.alias}
      elsif wafers.kind_of?(Array)
        waferids = wafers.collect {|w| w.kind_of?(String) ? w : w.alias}
      end
      res = _action(lot, "splitWaferLot", params.merge(:wafers=>waferids))
      return nil unless res
      child = _service_response[:splitWaferLotResponse][:return][:splitLotId]
      $log.info "  child lot: #{child.inspect}"
      return child
    end
    
    # return child lot
    def split_waferlot_withid(lot, child, wafers, params={})
      # return child lot
      if wafers.kind_of?(String)
        waferids = wafers.split
      elsif wafers.kind_of?(Numeric)
        waferids = @av.lot_info(lot, :wafers=>true).wafers[0...wafers].collect {|w| w.alias}
      elsif wafers.kind_of?(Array)
        waferids = wafers.collect {|w| w.kind_of?(String) ? w : w.alias}
      end
      _action(lot, "splitWaferLotWithId", params.merge(:wafers=>waferids, :child=>child))
    end
    
    def split_chiplot(lot, chips, params={})
      _action(lot, "splitChipLot", params.merge(:chips=>chips))
    end
    
    def merge(lot, child, params={})
      _action(lot, "merge", params.merge(:child=>child))
    end
    
    # scrapcode is any valid SiView WaferScrap code, e.g. CCC
    def scrap_waferlot(lot, scrapcode, params={})
      # scrapcode is any valid SiView WaferScrap code, e.g. CCC
      _action(lot, "scrapWaferLot", params.merge(:scrapcode=>scrapcode))
    end

    # scrapcode is any valid SiView WaferScrap code, e.g. CCC
    def scrap_chiplot(lot, scrapcode, params={})
      # scrapcode is any valid SiView WaferScrap code, e.g. CCC
      _action(lot, "scrapChipLot", params.merge(:scrapcode=>scrapcode))
    end

    # test support
    
    def hold_verify(lot, holdcode, params={})
      hold(lot, holdcode, params) and verify_lot_hold(lot, true)
    end

    def release_verify(lot, params={})
      release(lot, params) and verify_lot_hold(lot, false)
    end

    def split_waferlot_withid_verify(lot, child, nwafers, params={})
      split_waferlot_withid(lot, child, nwafers, params) or return false
      li = @av.lot_info(child)
      ($log.warn "  error, wrong nwafers"; return false) unless li.nwafers == nwafers
      return true
    end

    def merge_verify(lot, child, params={})
      merge(lot, child, params) or return false
      $log.info "verify_lot_merge (av): #{child}"
      li = @av.lot_info(child)
      ($log.warn "  errror, lot status: #{li.status}"; return false) unless li.status == 'EMPTIED'
      return true
    end

    # send start, move or cancel request and verify new lot status
    def move_verify(action, lot, exp_op, exp_status, params={})
      # send start, move or cancel request and verify new lot status
      res = send(action, lot, params)
      stat = (action == 'complete') ? "Processing" : exp_status
      res &= verify_lot_state(lot, exp_op, stat)
    end
    
    def scrap_waferlot_verify_cancel(lot, scrapcode, params={})
      scrap_waferlot(lot, scrapcode, params) || return
      li = @av.lot_info(lot)
      res = (li.status == 'SCRAPPED') and (li.carrier == '')
      $log.info "verify_lot_scrapped (av): #{li.lot}"
      $log.warn "  error, status: #{li.status}, carrier: #{li.carrier.inspect}" unless res
      @av.asm_scrap_wafers_cancel(lot, :reason=>@scrapcancel_code)
      return res
    end
  
    def verify_lot_hold(lot, onhold, params={})
      li = @av.lot_info(lot)
      res = ((li.status == 'ONHOLD') == onhold)
      $log.info "verify_lot_hold (av): #{li.lot}: #{onhold}"
      ($log.warn "  wrong status: #{li.status.inspect}"; return false) unless res
      return res
    end

    def verify_lot_state(lot, op, status, params={})
      $log.info "verify_lot_state (av): #{lot.inspect}, #{op.inspect}, #{status.inspect}"
      ok = true
      li = @av.lot_info(lot)
      lstatus = li.status
      lstatus = @av.user_parameter('Lot', lot, name: 'ProcState').value if lstatus == 'Waiting'
      ($log.warn "  wrong status: #{lstatus.inspect}"; ok = false) if lstatus != status
      if op
        op = @ops[op] if op.kind_of?(Symbol)
        ($log.warn "  wrong operation: #{li.op.inspect}"; ok = false) if li.op != op
      end
      return ok
    end

  end

  # AsmView Turnkey Shipment To Subcon interface
  # see ...
  class TurnKeySTB < JCAP
    
    def initialize(env, params={})
      super(env, {:instance=>"asmview.stb.#{env}"}.merge(params))
    end
    
    def stb(lot, params={})
      $log.info "shipment_to_subcon #{lot.inspect}"
      fabcode = (params[:fabcode] or "F1")
      shiptype = (params[:shiptype] or "SUBCON")
      partName = (params[:partname] or "") 
      salesOrder = params[:salesorder]
      xml = Builder::XmlMarkup.new(:indent=>2)
      xml.instruct!
      xml.soapenv :Envelope, "xmlns:soapenv"=>"http://www.w3.org/2003/05/soap-envelope" do |enve|
        enve.soapenv :Body do |body|
          body.ns2 :shipmentToSubCon, "xmlns:ns2"=>"urn:AsmTurnkeySTBOperations" do |stb|
            stb.ns2 :fabCode, fabcode
            stb.ns2 :lotId, lot
            stb.ns2 :shipType, shiptype
            stb.ns2 :partName, partName
            stb.ns2 :salesOrder, salesOrder
          end
        end
      end
      res = _send_receive(xml.target!, params)
      return nil unless res
      ret = res[:shipmentToSubConResponse][:return][:reason][nil] == "OK"
      $log.warn res unless ret
      return ret
    end
    
    def lot_proctime(lot)
      _p = @av.user_parameter("Lot", lot, :name=>"ProcTime")
      return nil if !_p.valueflag or _p.value == ""
      return Time.parse(_p.value)
    end
  end
end
