=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author:     ssteidte, 2010-11-24

Version:    1.3.1

History:
  2012-10-16 ssteidte, use $_erpasmlot structure instead of $_erpmeslot for better TurnKey subcon support
  2014-04-10 ssteidte, delete lot experiments in lot_cleanup
  2014-07-30 ssteidte, cleanup because Turnkey uses banks now, default route uses active versioning
  2014-12-01 ssteidte, use @sv.f7 to switch fab specific behaviour
  2016-05-04 sfrieske, require builder, remove obsolete ULL query in ERPMDS
  2016-08-04 sfrieske, moved lot_characteristics to SiView::MM
  2016-08-24 sfrieske, use lib rqrsp_mq instead of rqrsp
  2016-10-11 sfrieske, pass nil for route and product to skip route parts, still benefiting from new_lot
  2017-07-18 sfrieske, removed unused lot_set_erpitem and lot_set_order, parameters are set in lot_set_params
  2018-03-12 sfrieske, removed lot_tkey_prepare (was used in Test_Tkey_B2B, replaced there)
  2018-03-12 sfrieske, separated ERPMDM as it is used in STBMfgAttributes tests only
  2018-03-12 sfrieske, removed MQXML (rqrsp_mq and get_version), now defined in the inheriting classes
  2018-03-13 sfrieske, removed new_lot, lot_set_params, lot_set_tkinfo, lot_locate, verify_lot_state
  2019-05-02 sfrieske, deleted the code that was disabled a year ago 
=end

require 'siview'


# Testing of ERP-MES, ERP-AsmView and TurnKey interfaces, holds operation and bank information, TODO: convert into a Module
class ERPMES
  attr_accessor :sv, :route, :product, :ops, :banks


  def initialize(env, params={})
    @sv = params.has_key?(:sv) ? params[:sv] : SiView::MM.new(env)
    @product = params.has_key?(:product) ? params[:product] : 'TKEY03.A0'
    # pass route: nil to let the system find the currently active route
    @route = params.has_key?(:route) ? params[:route] : 'C02-TKEY-ACTIVE.01'
    if (!@route && @product)
      p = @sv.product_list(product: @product).first
      @route = p.route if p
      $log.warn "no route for product #{@product.inspect}" unless p
    end
    if @route
      @route_ops = @sv.route_operations(@route)
      set_ops(params)
      set_banks(params)
    end
  end

  def inspect
    "#<#{self.class.name} #{@sv.inspect}>"
  end

  def set_ops(params={})
    @ops ||= {}
    # for active versioning: find the operations on the route
    if @sv.f7
      # TK BUMP
      @ops[:tkbumpshipinit] = params[:tkbumpshipinit] || 'PTKYBUMPDISPO.1'
      # TK SORT
      @ops[:tksortshipinit] = params[:tksortshipinit] || 'PTKYSORTDISPO.1'
      # bank
      @ops[:tkbankbump] = @ops[:tkbumpshipinit]
      @ops[:tkbanksort] = @ops[:tksortshipinit]
      @ops[:tkbanksort_S] = @ops[:tksortshipinit]
      @ops[:tkbanksrt2] = 'PTKYBUMP2DISPO.1'
      @ops[:tkbankassy] = 'PTKYASSYDISPO.1'
      @ops[:qacheck] = 'PTKYQACHECK.1'
      # SHIP
      @ops[:lotshipdesig] = 'PTKYLOTSHIPDISPO.1'
    else
      # TK BUMP
      @ops[:tkbumpdispo] = _find_op('TKEYBUMPDISPO')
      # @ops[:trc4b] = _find_op('TR-TO-C4B')
      @ops[:tktypeufilter] = _find_op('TKEYTYPEUFILTER')
      @ops[:tkbumpshipdispo] = _find_op('TKEYBUMPSHIPDISPO')
      @ops[:tkbumpshipinit] = _find_op('TKEYBUMPSHIPINIT')
      @ops[:erpstatechgbump] = _find_op('ERPSTATECHGBUMP')
      # TK SORT
      @ops[:tksortdispo] = _find_op('TKEYSORTDISPO')
      # @ops[:trc4s] = _find_op('TR-TO-C4S')
      @ops[:tktypelfilter] = _find_op('TKEYTYPELFILTER')
      @ops[:tksortshipdispo] = _find_op('TKEYSORTSHIPDISPO')
      @ops[:tksortshipinit] = _find_op('TKEYSORTSHIPINIT')
      @ops[:erpstatechgsort] = _find_op('ERPSTATECHGSORT')
      # TK Reflow
      @ops[:trc4r] = _find_op('TR-TO-C4RFL')
      # bank based moves
      @ops[:tkbankbump] = @ops[:tkbumpshipinit]
      @ops[:tkbanksort] = @ops[:tkbumpshipinit]
      @ops[:tkbanksort_S] = @ops[:tksortshipinit]
      @ops[:tkbanksrt2] = @ops[:tkbumpshipinit]
      @ops[:tkbankassy] = 'TKEYXXXXSHIPINIT.01' #TODO check if setup is complete
      # SHIP
      @ops[:chkplnhld] = _find_op('CHKPLNHLD')
      @ops[:lotshipdesig] = _find_op('LOTSHIPDESIG')
      # END
      @ops[:bankin] = _find_op('BANKIN')
    end
    @ops
  end

  def _find_op(name)
    o = @route_ops.find {|e| e.op.start_with?(name)}
    o.nil? ? nil : o.op
  end

  def set_banks(params={})
    @banks ||= {}
    if @sv.f7
      @banks[:end_std] = params[:bank_end_std] || 'WINV'                          # finished goods, std
    else
      @banks[:tkbump] = params[:bank_tkbump] || 'O-TKYWBANK'
      @banks[:tksort] = params[:bank_tksort] || 'O-TKYWBANK'
      @banks[:end_std] = params[:bank_end_std] || 'OX-ENDFG'                      # finished goods, std
      @banks[:end_nonstd] = params[:bank_end_nonstd] || 'OT-SHIP-NS'              # finished goods, non-std
      @banks[:end_nonstd_return] = params[:bank_end_nonstd_return] || 'O-OUTF36'  # NonPro bank for finished goods, non-std return
      @banks[:ship] = params[:bank_ship] || 'OY-SHIP'                             # via SM from @@bank_ship as "Destination Bank ID"
    end
    @banks
  end

end
