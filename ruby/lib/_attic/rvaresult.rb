=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author:     ssteidte, 2013-07-09

Version:    1.2.1

History:
  2015-03-05 ssteidte,  added create_loadtest_files
  2016-05-10 sfrieske,  use rubyXL instead of POI for reading Excel files
  2016-08-24 sfrieske,  clone resultfile, it gets changed by the parser
=end

require 'time'
require 'rubyXL'
require 'rvaserver'
require 'siview'  # for siview_next


# RVA Testing Support
module RVA

  def self.create_loadtest_files
    s = s0 = 'C02-QL00'
    wb = RubyXL::Parser.parse("./log/#{s0}v1.xlsx")
    sheet = wb[0]
    300.times {
      s = SiView.siview_next(s)
      $log.info s
      sheet.sheet_name = "#{s}.01"
      wb.save("./log/#{s}v1.xlsx")  # currently not working, write error
      #wb = POI::Workbook.open("./log/#{s0}v1.xlsx")
      #wb.set_sheet_name(0, "#{s}.01")
      #wb.save_as("./log/#{s}v1.xlsx")
    }
  end

  class ResultFile
    attr_accessor :filename, :workbook, :sheet, :headline, :runtime, :runid, :rvauser, :violations, :compref

    def initialize(f, params={})
      if f.start_with?('PK')
        @workbook = RubyXL::Parser.parse_buffer(@filename = StringIO.new(f.clone))
      else
        @workbook = RubyXL::Parser.parse(@filename = f)
      end
      @sheet = @workbook[params[:sheet] || 0]
      @violations = {}
    end

    def inspect
      "#<#{self.class.name} #{@filename.inspect}, violations: #{violation_count.inspect}>"
    end

    def violation_count(violations=nil)
      violations ||= @violations
      Hash[violations.collect {|k, v|
        [k, v.inject(0) {|s, e| s + e[:positions].size}]
      }]
    end

    # return true if no errors found, false on errors, nil on xls file errors
    def parse
      @violations = {}
      rows = @sheet.collect {|row| row ? row.cells.collect {|cell| cell && cell.value} : []}
      ($log.warn "  no valid results file"; return) if rows.size < 2
      @headline = rows[0][0]
      @headline =~ /^Validation result dated (.*) for user (.*) for: (.*)$/
      @runtime = Time.parse($1)
      @runid = $3
      @rvauser = $2
      $log.info "parse (result file #{@filename.inspect} from #{@runtime.iso8601})"
      ($log.info "  no violations"; return true) if rows.size < 4
      vtype = nil
      rows[3..-1].each {|cells|
        next if cells.size == 0
        if cells[1] =~ /^(Structure|Format|Consistency) Violation/
          vtype = $1.downcase.to_sym
          @violations[vtype] ||= []
        else
          if cells[1]
            @violations[vtype] << {severity: cells[1], ruleid: cells[3], count: cells[5].to_i, details: cells[4], positions: []}
          else
            row = cells[10]
            row = row.to_i if row.kind_of?(Numeric)
            @violations[vtype].last[:positions] << {rule: cells[4], filename: cells[7], sheetname: cells[8],
              col: cells[9], row: row, cellvalue: cells[12], path: cells[14]}
          end
        end
      }
      $log.info "  violations: #{violation_count.inspect}"
      true
    end

    def show_result
      res = "\nresult file summary: #{violation_count.pretty_inspect}\n"
      res += "structure violations:\n"
      @violations[:structure].each {|v|
        v[:positions].each {|p|
          res += "  %-6.6s  %-12.12s  %-32.32s  %-48.48s\n" % [v[:severity], v[:ruleid], p[:rule], p[:cellvalue]]
        }
      }
      res += "format violations:\n"
      @violations[:format].each {|v|
        v[:positions].each {|p|
          res += "  %-6.6s  %-12.12s  %-32.32s  %s%s  %-32s\n" % [v[:severity], v[:ruleid], p[:rule], p[:col], p[:row], p[:cellvalue]]
        }
      }
      res
    end
  end

end
