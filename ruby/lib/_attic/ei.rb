=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author:     ssteidte, 2014-04-01

Version:    1.0.2

History:
  2015-04-17 ssteidte,  minor cleanup
  2016-08-24 sfrieske,  use lib rqrsp_mq instead of rqrsp
=end

require 'rqrsp_mq'

# MQ Communication with SmallTalk EIs
module EI

  # MQ messages
  class MQClient < RequestResponse::MQXML
    attr_accessor :env, :eqp

    def initialize(env, eqp, params={})
      @env = env
      @eqp = eqp
      super("ei.#{@env}", {use_jms: false, timeout: 30,
        qname: "CEI.#{eqp}_REQUEST01", reply_queue: "CEI.#{eqp}_REPLY01"}.merge(params))
    end

    def inspect
      "#<#{self.class.name}, env: #{@env} eqp: #{@eqp}>"
    end

    # send EI ping, return response msg as hash or nil
    def ei_info(params={})
      $log.info "ei_info (#{@eqp.inspect})"
      xml = Builder::XmlMarkup.new(indent: 2)
      xml.instruct!
      xml.soapenv :Envelope, 'xmlns:soapenv'=>'http://schemas.xmlsoap.org/soap/envelope/',
        'xmlns:xsd'=>'http://www.w3.org/2001/XMLSchema', 'xmlns:xsi'=>'http://www.w3.org/2001/XMLSchema-instance' do |enve|
        enve.soapenv :Body do |body|
          body.ns1 :EIInformation, 'soapenv:encodingStyle'=>'http://schemas.xmlsoap.org/soap/encoding/', 'xmlns:ns1'=>'http://www.amd.com/schemas/baselineservices'
        end
      end
      res = send_receive(xml.target!, params) || return
      ($log.warn res.inspect; return) unless res[:EIInformationResponse]
      res[:EIInformationResponse][:EIInformation]
    end
  end

end
