=begin
Connect to IBM WebSphere MQ through JMS

(c) Copyright 2011 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.
    
  Author: Daniel Steger, 2011-05-10
=end
    
require 'java'
require 'yaml'
require 'timeout'
require 'misc'
java_import java.util.concurrent.ExecutorService
java_import java.util.concurrent.Executors
java_import java.util.concurrent.TimeUnit


# Connect to IBM WebSphere MQ through JMS
module Xsite
  
  # Connect to IBM WebSphere MQ through JMS
  class MBX
    attr_accessor :env, :host, :mbx, :pcmbx, :rbx, :msg
    attr_reader :responsetime
      
    def initialize(env, params={})
      @env = env
      name = "xsite.#{env}"
      configfile = (params[:config] or "etc/mbx.conf")
      _params = _config_from_file(name, configfile)
      if _params
        params = _params.merge(params)
      else
        $log.warn "entry #{name.inspect} not found in config file #{configfile}"
      end
      @host = params[:host]
      @mbx = params[:mbx]
      @pcmbx = params[:pcmbx]    
      if @host and @mbx and @pcmbx
        connect(params)      
        reply_box = params[:rbox]
      end
    end
      
    def connect(params)
      user = params[:mbxuser]
      password = params[:mbxpassword]
      $mbx = com.brooks.mbx.tcp.MbxConnection.new(@mbx, @host, @pcmbx)
      $mbx.connect
      
      $provider = Class.new(com.brooks.mbx.transport.BigMbxProvider) do
        define_method :getMbxConnection do
          return $mbx
        end
      end.new
    end
    
    def disconnect    
      $mbx.mbxDisconnect if $mbx and connected?
    end
    
    def connected?    
      return $mbx.mbxIsConnected
    end
    
    # operations on queues
    #
    # open a queue for read or write
    def mopen(mbox, options="")      
      begin
        return $mbx.mbxOpen(mbox, options)
      rescue
        $log.warn "error opening mailbox #{@dest_box.inspect}:\n#{$!}"
        nil
      end
    end
   
    # put a string message on the queue
    #
    # return true on success
    def send_msg(dest_box, s, params={})      
      reply_box = (params[:reply_box] or "qa-#{Time.now.to_i}")
      timeout =  (params[:timeout] or 120)      
      ret = []
      begin
        $log.debug s
        @msg = s
        # open destination box only if it already exists
        dbox = mopen(dest_box, 'q')      
        # open reply box
        rbox = mopen(reply_box)
                  
        # send the message
        tstart = Time.now
        $mbx.mbxPut(dbox, rbox, @msg)
        
        # retrieve data (the old way)
        #reply = $mbx.mbxGetNoWaitRemove(rbox)
        
        # this might not work until Ruby 1.9
        # t = Thread.new {          
        #  Timeout.timeout(timeout) {
        #    reply = $provider.mbxWait($provider.getMbxConnection, rbox, com.brooks.mbx.MbxBaseWaitItem.java_class)
        #    @reply = reply.get_message
        #   }
        #}
        #t.join
        
        task = lambda do
          reply = $provider.mbxWait($provider.getMbxConnection, rbox, com.brooks.mbx.MbxBaseWaitItem.java_class)
          return reply.get_message        
        end
      
        executor = Executors.newCachedThreadPool
        submit = executor.java_method(:submit, [Java::JavaUtilConcurrent::Callable.java_class])
        future = submit.call(task)
        @reply = future.get(timeout, TimeUnit.const_get("SECONDS"))
        @responsetime = Time.now - tstart
        
        $log.debug "Reply message: #{@reply.inspect}"
      rescue => e
        $log.info "error sending msg\n#{$!}"
        $log.debug e.backtrace.join("\n")
        @reply = nil
      ensure
        executor.shutdown
        # close boxes
        $mbx.mbxClose(rbox)        
      end
      return @reply
    end
    alias :put_msg :send_msg
    
    # put the messages in the string array msgs on the queue
    #
    # return true on success
    def send_msgs(msgs, params={})
      ret = true
      msgs = [msgs] unless msgs.kind_of?(Array)
      msgs.each {|msg| ret &= send_msg(msg, params)}
      return ret
    end
    alias :put_msgs :send_msgs
    
   # simple parsing of Xsite messages
   #
   # TODO: Correctly implement message format
    def parse_msg(msg_string) 
      key_values = msg_string.scan(/(\w+)=(\"?([\w-]+)\"?)/)
      keys = key_values.map { |x| x[0].intern }
      values = key_values.map { |x| x[2] }
        
      reply = nil
      if keys.length > 0
        reply_struct = Struct.new(*keys)
        reply = reply_struct.new(*values)
      end
      return reply
    end    
  end
end


def mbx_test  
  mbx = MBX.new("xsite.itdc")
  mbx.send_msg "FwCheckStatusTxn", ">>L FwCheckStatusTxn name=UTS001"
  puts @reply
end
