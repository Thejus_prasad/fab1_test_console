=begin 
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.
    
Author: Daniel Steger

Version: FOD-MS libary
=end

require 'dbsequel'

module FODMS
  LotStatus = Struct.new(:lot, :lot_family, :parent_lot, :created_time, :insert_time, :archive_time, :last_calc_time, :fsd, :fod_init, :fod_commit, :fod_min, :fod_calc, :lot_type, :sub_lot_type, :sub_lot_type_group, :tech_id, :prodspec_id, :prod_mainpd_id, :priority_class, :customer, :lot_owner, :qty, :lot_state, :lot_process_state, :lot_hold_state, :lot_inv_state, :lot_finished_state, :status, :current_loc_flag, :ope_enter_time, :route, :opeNo, :pd_id, :ope_pass_count, :bank_id)
  LotSchedule = Struct.new(:lot, :schedule)
  Operation = Struct.new(:route, :opeNo, :pd_id, :active, :ope_end_time_init, :ope_end_time_plan, :ope_end_time_act, :mask_layer, :smpl_value, :dept)
  
  # Find FOD data in MDS
  class MDS
    attr_accessor :env, :db
    
    def initialize(env, params={})
      @env = env
      @db = DB.new("mds.#{@env}", params)
    end
    
    def lot_calc_history(lot, params={})
      q = @db.dbh[:MDS_ADMIN__T_XF_LOT_CALC_HIST].filter(:lot_id=>lot)
      q = q.filter(:calc_reason_code=>params[:reason]) if params.has_key?(:reason)
      q = q.where { claim_time > params[:tstart] } if params.has_key?(:tstart)
      res = q.all      
      if res.count > 0
        s = Struct.new(*q.columns)
        return res.collect {|r| s.new(*r.values)}
      else
        return []
      end
    end

    # Return the lot status information
    # nil if lot was not found
    def lot_status(params={})
      q = @db.dbh[:MDS_ADMIN__V_XF_LOT_STATUS]
      q = q.filter(:lot_id=>params[:lot]) if params.has_key?(:lot)
      q = q.filter(:fod_calc=>params[:fod_calc]) if params.has_key?(:fod_calc)
      q = q.filter(:fod_commit=>params[:fod_commit]) if params.has_key?(:fod_commit)
      res = q.first
      return nil unless res
      return LotStatus.new(*res.values)
    end
    
    # Return the lot schedule of the given lot
    def lot_track(lot, params={})
      q = @db.dbh[:MDS_ADMIN__V_XF_LOT_TRACK].filter(:lot_id=>lot).order(:seq_no)
      _schedule = q.all
      _seq = _schedule.collect {|s| 
        Operation.new(s[:mainpd_id], s[:ope_no], s[:pd_id], s[:active_flag], s[:ope_end_time_plan_init], s[:ope_end_time_plan], 
          s[:ope_end_time_actual], s[:mask_layer], s[:smpl_value], s[:department])
      }
      return LotSchedule.new(lot, _seq)
    end
  end
  
  class Test
    attr_accessor :env, :mds
    
    
    def initialize(env, params={})
      @mds = MDS.new(env, params)
    end
  
  
    def verify_recalculated(lot, event, params={})
      $log.info("verify_recalculated #{lot}, #{event}, #{params.inspect}")
            
      tstart = (params[:tstart] or Time.now)
      tstart = Time.parse(tstart) unless tstart.kind_of?(Time)
      
      # wait for recalculation
      lot_status = nil
      res = wait_for(timeout: 800) {
        lot_status = @mds.lot_status(lot: lot)
        (lot_status && lot_status.last_calc_time && lot_status.last_calc_time > tstart)
      }
      ($log.warn " #{lot} was not recalculated since #{tstart.inspect}"; return false) unless res

      # verify history
      hist = @mds.lot_calc_history(lot, reason: event, tstart: tstart)
      ($log.warn " no history for #{lot} and #{event}"; return false) unless hist.first
      res &= verify_equal("OK", hist.first.claim_memo, " error in calculation of #{lot}")
      
      sod_calc = lot_status[:fod_calc]      
      res &= verify_plan(lot, sod_calc, params)
      return res
    end
    
    # some sanity checks on the calculated plan
    def verify_plan(lot, sod_calc, params={})
      schedule = @mds.lot_track(lot)
      res  = verify_equal sod_calc, schedule.schedule.last.ope_end_time_plan, " SOD is expected at same time as planned end of last operation"
      # get sod_calc from parent
      if params[:parent]
        psod_calc = @mds.lot_status(lot: params[:parent]).fod_calc
        res &= verify_condition { psod_calc <= sod_calc }
      end
      return res
    end
  
  end
  
end