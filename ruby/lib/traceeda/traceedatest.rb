# encoding: UTF-8
=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.


Author:     ssteidte, 2012-01-30

Version:    1.4.2

History:
  2012-01-30 ssteidte, adapted to new xml_to_hash
  2013-04-12 dsteger,  added error general error handling, trace event collection
  2013-08-23 ssteidte, changed back to old error handling to avoid aborts on errors
  2013-10-14 ssteidte, separated E3 classes
  2013-10-23 ssteidte, pass test duration and calculate loops
  2013-12-02 ssteidte, new report_loop with clearer events, lot and wafer creation
  2014-01-29 ssteidte, use queues for sync
  2014-02-07 ssteidte, group ETCs in batches sending with a time shift, new charting
  2016-08-19 sfrieske, separated Test class into traceedatest.rb
  2020-01-03 sfrieske, moved TraceEDA files into subdirectory traceeda
  2021-08-26 sfrieske, removed dependency on misc.rb (duration_string)
=end

require 'fileutils'
require 'time'
require 'thread'
require 'util/charts'
require 'util/durationstring'

Thread.abort_on_exception = true   # set to true for debugging


# Test TraceEDA (WonderWare)
module TraceEDA
  Dir["#{File.dirname(__FILE__)}/traceeda_*rb"].each {|f| load(f)}

  class Test
    attr_accessor :env, :tools, :eas, :hists, :hists2, :tstart, :tend, :etcthreads, :histthreads

    def initialize(env, params={})
      @env = env
      @tools = params[:tools]
      unless @tools
        tool0 = params[:tool0] || 'LETCL1050'
        s = nil
        @tools = (params[:ntools] || 3).times.collect {s = s.nil? ? tool0 : s.next}
      end
      @hists = []
      @hists2 = []
      @eas = TraceEDA::EAS.new(env, params)
      @eas.connect
      @eas.store_plan
      @tools.each {|t|
        etc = TraceEDA::ETC.new(t, @eas, params)
        @hists << TraceEDA::WWHWS.new(env, etc: etc)
        @hists2 << TraceEDA::WWHWS.new(env, etc: etc, hist2: true) if params[:hist2]
      }
      @eas.close
    end

    def inspect
      "#<#{self.class.name} env: #{@eas.env}, tools: #{@hists.collect {|h| h.name}}>"
    end

    def comparison_test(params={})
      hist = @hists.first
      colls = hist.etc.create_collections(params.merge(teststring: '§^'))
      hist.etc.report(colls) || return
      sleep 60
      res = hist.get_data(colls.first.first, colls.last.first) || return
      return true if colls.first.last == res.first
      $log.warn "data mismatch:\n#{colls.first.last.inspect}\n#{res.first.inspect}"
      ##$c1, $c2 = colls.first.last, res.first
      return false
    end

    def stop_loadtest
      @etcthreads.each {|t| t.kill if t.alive?}
      @histthreads.each {|t| t.kill if t.alive?}
    end

    def load_test(params={})
      $log.info "load_test\n#{params}"
      # ensure parameter integrity
      sinterval = params[:sinterval] || 0.5
      ncollections = params[:ncollections] || 25
      looptime = ncollections * sinterval
      cmpratio = params[:cmpratio] || 4  # e.g. 4 to store every 4th collection for comparison, 0 for none
      if !cmpratio.zero? && (looptime * cmpratio < @hists.first.cmpmaxwait)
        $log.warn "invalid parameter set: sinterval * ncollections * cmpratio must be greater than cmpmaxwait"
        $log.warn "parameter values: #{sinterval} * #{ncollections} * #{cmpratio} < #{@hists.first.cmpmaxwait}"
        return false
      end
      #
      bsize = params[:bsize] || 5
      bshift = params[:bshift] || 2.0
      $log.info "grouping ETCs in batches of #{bsize} with #{bshift}s shift beween reporting times"
      @tstart = Time.now
      @etcthreads = []
      @histthreads = []
      @hists.each_with_index {|hist, i|
        # group ETCs in batches
        sleep bshift if i % bsize == 0
        # clear ETC's queues and start reporting
        hist.etc.collections.clear
        hist.etc.collections2.clear if hist.etc.collections2
        @etcthreads << Thread.new {
          Thread.current['name'] = "ETC_#{hist.etc.eqp}"
          hist.etc.report_loop(params)
        }
        # start comparing
        @histthreads << Thread.new {
          Thread.current['name'] = "HWS_#{hist.name}"
          hist.cmp_etc_collections(params)
        }
        hist2 = @hists2[i]
        if hist2
          @histthreads << Thread.new {
            Thread.current['name'] = "HWS_#{hist2.name}"
            hist2.cmp_etc_collections(params)
          }
        end
      }
      @etcthreads.each {|t| t.join}
      tsleep = @hists.first.cmpmaxwait + 40
      $log.info "ETCs completed sending reports, waiting for HWSes to complete within #{tsleep} s"
      sleep tsleep
      @histthreads.each {|t| ($log.warn "aborting #{t['name']}"; t.kill) if t.alive?}
      @tend = Time.now
      #
      # log results
      logdir = "log/traceeda_perf_#{Time.now.utc.iso8601.gsub(':', '-')}"
      FileUtils.mkdir_p(logdir)
      File.binwrite("#{logdir}/comparetries.yaml",
        Hash[(@hists + @hists2).collect {|h| [h.name, h.comparetries]}].to_yaml)
      perflog("#{logdir}/delays_1", nocharts: params[:nocharts])
      if @hists2 != []
        perflog("#{logdir}/delays_2", nocharts: params[:nocharts], hists: @hists2)
        difflog("#{logdir}/diff", nocharts: params[:nocharts])
      end
      summary("#{logdir}/summary")
    end

    def summary(fname, params={})
      ret = "Retrieval Summary, #{tstart.utc.iso8601} .. #{@tend.utc.iso8601}\n"
      ret += "-" * 63 + "\n"
      ret += "test duration: #{duration_string(@tend - @tstart)}\n"
      ret += "\n\ntimeouts:\n"
      ok = true
      (@hists + @hists2).each_with_index {|hist, hi|
        ret += hist.name
        delays = hist.delays
        rtimes = hist.etc.report_times
        if delays.find {|d| d < 0}
          ok = false
          (ret += ": failed (all)\n"; next) unless delays.find {|d| d >= 0}
          ret += ": failed\n"
          delays.each_with_index {|d, di| ret += "  #{rtimes[di].utc.iso8601}\n" if d < 0}
        else
          ret += ": passed\n"
        end
      }
      ret += ok ? "\ntimeout test passed\n" : "\ntimeout test failed\n"
      if @hists2 != []
        hist2diff = (params[:hist2diff] or 15)   # percent
        ret += "\n\ninstance response times differing > #{hist2diff}%:\n"
        @hists.each_with_index {|hist, hi|
          ret += hist.name + "\n"
          delays = hist.delays
          delays2 = @hists2[hi].delays
          rtimes = hist.etc.report_times
          delays.each_with_index {|d, di|
            d2 = delays2[di]
            next unless d and d2
            ret += "  #{rtimes[di].utc.iso8601}\n" if ((d2 - d)/(d2 + d)/2.0*100).abs > hist2diff
          }
        }
      end
      $log.info "\n\n#{ret}\n"
      File.binwrite("#{fname}.txt", ret) if fname
      return
    end

    def perflog(fname, params={})
      $log.info "perflog #{fname.inspect}"
      hists = params[:hists] || @hists
      open("#{fname}.txt", 'wb') {|fh|
        hists.each {|hist|
          fh.write('Time ' + hist.etc.report_times.collect {|t| t.utc.iso8601}.join(' ') + "\n")
          fh.write("#{hist.name} " + hist.delays.collect {|d| d.round(1)}.join(' ') + "\n\n")
        }
      }
      return if params[:nocharts]
      vname = params[:vname] || 'delay(s)'
      ch = Chart::Time.new(nil)
      hists.each {|hist| ch.add_series(hist.etc.report_times.zip(hist.delays))}
      ch.create_chart(title: File.basename(fname), y: vname, dots: true, export: "#{fname}_chart.png")
      ##ch.create_chart(:title=>File.basename(fname), :y=>vname, :ymin=>0.01, :ymax=>100, :dots=>true, :lg=>true, :export=>"#{fname}_chartlg.png")
      ch = Chart::Histogram.new(hists.collect {|hist| hist.delays}.flatten)
      ch.create_chart(title: File.basename(fname), bins: 50, x: vname, export: "#{fname}_hist.png")
    end

    def difflog(fname, params={})
      $log.info "difflog #{fname.inspect}"
      open("#{fname}.txt", 'wb') {|fh|
        @hists.each_with_index {|hist, hi|
          dd = []
          delays2 = @hists2[hi].delays
          hist.delays.each_with_index {|d, di|
            if d < 0
              dd << -59
            elsif delays2[di] < 0
              dd << 59
            else
              dd << (delays2[di] - d).round(1)
            end
          }
          fh.write "Time " + hist.etc.report_times.collect {|t| t.utc.iso8601}.join(' ') + "\n"
          fh.write "#{hist.name} #{dd.join(' ')}\n\n"
        }
      }
      return if params[:nocharts]
      vname = params[:vname] || 'delta (s)'
      alldiffs = []
      ch = Chart::Time.new(nil)
      hists.each_with_index {|hist, hi|
        dd = []
        delays2 = @hists2[hi].delays
        hist.delays.each_with_index {|d, di|
          if d < 0
            dd << -59
          elsif delays2[di] < 0
            dd << 59
          else
            dd << delays2[di] - d
          end
        }
        alldiffs += dd
        ch.add_series(hist.etc.report_times.zip(dd))
      }
      ch.create_chart(title: File.basename(fname), y: vname, dots: true, export: "#{fname}_chart.png")
      ch = Chart::Histogram.new(alldiffs)
      ch.create_chart(title: File.basename(fname), bins: 50, x: vname, export: "#{fname}_hist.png")
    end

  end

end
