=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.


Author:     ssteidte, 2012-01-30
Version:    1.4.1

History:
  2012-01-30 ssteidte, adapted to new xml_to_hash
  2013-04-12 dsteger,  added error general error handling, trace event collection
  2013-08-23 ssteidte, changed back to old error handling to avoid aborts on errors
  2013-10-14 ssteidte, separated E3 classes
  2013-10-23 ssteidte, pass test duration and calculate loops
  2013-12-02 ssteidte, new report_loop with clearer events, lot and wafer creation
  2014-01-29 ssteidte, use queues for sync
  2014-02-07 ssteidte, group ETCs in batches sending with a time shift, new charting
  2016-08-22 sfrieske, inherit from new RequestResponse::HttpXML
  2016-09-09 sfrieske, separated traceeda into traceeda_eas, traceeda_etc and traceeda_wwhist
  2021-02-05 sfrieske, use instance_of instead of kind_of
  2021-08-31 sfrieske, requiring 'time' for #iso8601
=end

require 'time'
require 'rqrsp_http'

ENV.delete('http_proxy')


# Communication with WW Historian
class TraceEDA::WWHWS < RequestResponse::HttpXML
  attr_accessor :env, :name, :hist2, :etc, :eqp, :parameters, :datatypes, :delimiter,
    :cmpmaxwait, :cmpsleeptime, :delays, :comparetries

  def initialize(env, params={})
    @hist2 = params[:hist2]
    @env = env
    @etc = params[:etc]
    if @etc
      @eqp = @etc.eqp
      @parameters = @etc.parameters
      @etc.collections2 = Queue.new if @hist2  # create a queue in the ETC so it knows it has to fill it
    end
    @eqp ||= params[:eqp]
    @parameters ||= params[:parameters]
    @datatypes = @parameters.values
    @delimiter = params[:delimiter] || '|'
    @cmpmaxwait = params[:cmpmaxwait] || 40
    @cmpsleeptime = params[:cmpsleeptime] || 0.5    # time to wait between compare retries
    @name = "#{@eqp}#{'-2' if @hist2}"
    super("traceeda_wwhws#{'2' if @hist2}.#{env}", {timeout: 10}.merge(params))
  end

  def inspect
    "#<#{self.class.name} env: #{@env}, name: #{@name}>"
  end

  # retrieve eqp parameter data between tstart and tend (Time.now if nil)
  def get_data(tstart, tend, params={})
    tend ||= Time.now
    $log.debug {"get_data #{@name} #{tstart.iso8601(3)}, #{tend.iso8601(3)}"}
    h = {'env:Envelope': {'xmlns:env'=>'http://schemas.xmlsoap.org/soap/envelope/',
      'env:Body':
       {GetDataRequest: {'xmlns'=>'urn:amd-com:HistorianQueryService.xsd',
         ToolId: @eqp,
         TagNames: {TagName: @parameters.keys.collect {|p| "Eqp:#{@eqp}/#{p}"} },
         TimeFrame: {StartTime: tstart.utc.iso8601(3), StopTime: tend.utc.iso8601(3)},
         EventFrame: nil,
         RetrievalMode: {'Delta-Wide': {MaxRowLimit: 999999}},
         DataFormat: {'delimiter'=>'|'}
      }}
    }}
    res = post(hash_to_xml(h), {soapaction: 'urn:amd-com:HistorianQueryService:GetData'})
    return res if res.nil? or @nosend
    res = res[:GetDataResponse]
    ($log.warn "error retrieving data: #{res.inspect}"; return) unless res and res.has_key?(:HistoricalData)
    # get parameter values (data arriving in the request order)
    vv = res[:HistoricalData][:TagDataSet][:TagData]
    ($log.warn 'empty TagData'; return []) unless vv
    vv = [vv] if vv.instance_of?(Hash)   # if only one set is returned
    # extract values and convert them to correct data types
    return vv.collect {|coll|
      line = coll[nil]
      data = []
      line.split(@delimiter).each_with_index {|v, i|
        if v == 'NULL'
          data << nil
        else
          dtype = @datatypes[i]
          if dtype == 'float'
            data << v.to_f.round(3)
          elsif dtype == 'unsigned integer'
            data << v.to_i
          else  #if dtype.start_with?('string')
            data << v
          end
        end
      }
      data
    }
  end

  # get the data from the reference collections. additional first or last rows are ignored
  #
  # return number of missing collection data sets (always integer!)
  def retrieve_compare(colls, params={})
    $log.debug {"retrieve_compare #{@name} (#{colls.first.first})"}
    res = get_data(colls.first.first, colls.last.first, params)
    return -colls.size if res.nil?
    # find first line of colls in the response (res may have preceding surplus data)
    istart = nil
    res.each_with_index {|r, i| (istart = i; break) if r == colls[0][1]}
    return -colls.size unless istart
    # take as many lines as in colls (res may have surplus trailing data)
    res = res[istart, colls.size]
    ret = colls.size - res.size
    ($log.debug "  data not yet complete, #{res.size}"; return ret) if ret > 0
    # compare result and reference colls data, first line already done above
    colls[1..-1].each_index {|i|
      ($log.debug "  data not yet correct, #{i}"; return colls.size - i) if res[i] != colls[i][1]
    }
    return 0
  end

  # read etc.collections until [] is reached and compare them
  #
  # return true if all collections have been retrieved
  def cmp_etc_collections(params={})
    $log.info "cmp_etc_collections"
    ret = true
    @delays = []
    @comparetries = []
    etccollections = @hist2 ? @etc.collections2 : @etc.collections
    n = 0
    loop {
      colls = etccollections.shift  # waits for data
      ($log.info "done comparing"; break) if colls.nil?   # end marker encountered
      $log.info "comparing collection# #{n}"
      sleep 0.3 # to avoid empty 1st answers because it's too quick
      treport = @etc.report_times[n]
      tcmpend = treport + @cmpmaxwait
      retries = []
      loop {
        nmissing = retrieve_compare(colls, params)
        $log.debug  "retrieve_compare collection# #{n}: #{nmissing}"
        delay = Time.now - treport
        retries << [delay, nmissing]
        if nmissing == 0
          @delays << delay
          break
        end
        sleep @cmpsleeptime
        if Time.now > tcmpend
          $log.warn "cmp_etc_collections timeout"
          @delays << nmissing # error flag
          ret = false
          break
        end
      }
      @comparetries << retries
      n += 1
    }
    return ret
  end
end
