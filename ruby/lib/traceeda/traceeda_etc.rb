=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.


Author:     ssteidte, 2012-01-30
Version:    1.4.2

History:
  2012-01-30 ssteidte, adapted to new xml_to_hash
  2013-04-12 dsteger,  added error general error handling, trace event collection
  2013-08-23 ssteidte, changed back to old error handling to avoid aborts on errors
  2013-10-14 ssteidte, separated E3 classes
  2013-10-23 ssteidte, pass test duration and calculate loops
  2013-12-02 ssteidte, new report_loop with clearer events, lot and wafer creation
  2014-01-29 ssteidte, use queues for sync
  2014-02-07 ssteidte, group ETCs in batches sending with a time shift, new charting
  2016-08-22 sfrieske, inherit from new RequestResponse::HttpXML
  2016-09-09 sfrieske, separated traceeda into traceeda_eas, traceeda_etc and traceeda_wwhist
  2021-02-05 sfrieske, use instance_of instead of kind_of
  2021-08-26 sfrieske, removed dependency on misc.rb (duration_in_seconds)
=end

require 'time'
require 'builder'
require 'rqrsp_http'
require 'util/durationstring'
require 'util/uniquestring'

ENV.delete('http_proxy')


# Communication with ETCs (like EIs/tools do)
class TraceEDA::ETC < RequestResponse::HttpXML
  attr_accessor :eas, :eqp, :substitutions, :parameters, :chambers, :subprocs, :recipename, :plan, :events,
    :alarms, :doreport_recipesteps, :report_times, :collections, :collections2, :cjs, :lots, :wafers

  def initialize(eqp, eas, params={})
    @eqp = eqp
    @chambers = params[:chambers] || ['PM1', 'PM2']
    @subprocs = params[:subprocs] || ['Side1']
    @recipename = params[:recipename] || "#{eqp}-RCP1"
    @eas = eas
    @plan = params[:plan] || @eas.plan
    @substitutions = params[:substitutions] || {'[PA=ProcessingChamber]'=>@chambers, '[SPA=Side]'=>@subprocs}
    @parameters = @eas.mappings
    @events = @eas.evt_map
    @substitutions.each_pair do |subst, subvalues|
      @parameters = _subst(@parameters, subst, subvalues)
      @events = _subst(@events, subst, subvalues)
    end
    @alarms = @eas.alarms # alarms are concrete
    @collections = Queue.new
    @collections2 = nil   # set by historian if desired
    @cjs = []
    @lots = []
    @wafers = []
    super(nil, {url: eas.tool_url(eqp, params)}.merge(params))
  end

  # substiution of events and trace variables, internally used only
  def _subst(text, subst, subvalues)
    if text.instance_of?(Hash)
      ret = {}
      text.each_pair do |k, v|
        if k.index(subst)
          subvalues.each {|sv| ret[k.gsub(subst,sv)] = _subst(v, subst, [sv])}
        else
          ret[k] = v
        end
      end
      return ret
    elsif text.instance_of?(Array)
      ret = []
      text.each do |k|
        if k.index(subst)
          subvalues.each {|sv| ret << k.gsub(subst, sv)}
        else
          ret << k
        end
      end
      return ret
    else
      return text # no substituion
    end
  end

  def inspect
    "#<#{self.class.name} uri: #{@uri.to_s}>"
  end

  def clear_context(params={})
    $log.info "clear_context (#{@eqp})"
    xml = Builder::XmlMarkup.new(indent: 2)
    xml.instruct!
    xml.env :Envelope, 'xmlns:ns'=>'http://www.amd.com/schemas/interfaceAExtended',
      'xmlns:env'=>'http://schemas.xmlsoap.org/soap/envelope/' do |enve|
      enve.env :Body do |body|
        body.ns :ClearContextDataRequest do |drq|
          drq.ns :equipmentId, @eqp
          drq.ns :planName, @plan
          drq.ns :timeStamp, Time.now.utc.iso8601(3)
          drq.ns :reason do |rs|
            rs.ns :errorCode, '302'
            rs.ns :errorMessage, 'Clear context'
          end
        end
      end
    end
    action = 'ClearContextDataRequest'
    res = post(xml.target!, {soapaction: action}.merge(params))
    return res if res.nil? || params[:nosend]
    ($log.warn "#{action} error: #{res.inspect}"; return) unless res.has_key?(:ClearContextDataRequestResponse)
    return true
  end

  # concrete mapping, sent by EI on startup
  def plan_mapping(params={})
    $log.info "plan_mapping (#{@eqp})"
    xml = Builder::XmlMarkup.new(indent: 2)
    xml.instruct!
    xml.env :Envelope, 'xmlns:ns'=>'http://www.amd.com/schemas/interfaceAExtended',
      'xmlns:env'=>'http://schemas.xmlsoap.org/soap/envelope/' do |enve|
      enve.env :Body do |body|
        body.ns :DefineExternalPlanMappingRequest do |pmrq|
          pmrq.ns :equipmentId, @eqp
          pmrq.ns :planName, @plan
          pmrq.ns :placeholderResolution do |ph|
            @substitutions.each_pair {|k,values|
              ph.ns :placeholderSubstitution, 'placeholder'=>k do |phs|
                values.each {|v| phs.ns :substitution, v}
              end
            }
          end
          if params[:event_parameters] != false
            @events.each_pair do |e, context|
              pmrq.ns :eventParameterDefinitions do |pd|
                pd.ns :eventName, e
                #Frameevents: Only name is reported
                #context.each do |c|
                #  pd.ns :parameterDefinition, "", "parameterName"=>c, "dataType"=>"string"
                #end
              end
            end
          else
            $log.info "  without event parameters"
          end
          if params[:trace_parameters] != false
            pmrq.ns :traceParameterDefinitions do |pd|
              pd.ns :traceRequestName, 'ProcessingChamberTrace'
              @parameters.each_pair {|p, dtype|
                if dtype.start_with?('string')
                  pd.ns :parameterDefinition, '', 'parameterName'=>p, 'dataType'=>'string', 'length'=>dtype.split[-1]
                else
                  pd.ns :parameterDefinition, '', 'parameterName'=>p, 'dataType'=>dtype, 'units'=>@eas.units
                end
              }
            end
          else
            $log.info "  without trace parameters"
          end
        end
      end
    end
    action = 'DefineExternalPlanMapping'
    res = post(xml.target!, {soapaction: action}.merge(params))
    return res if res.nil? || params[:nosend]
    ($log.warn "#{action} error: #{res.inspect}"; return) unless res.has_key?(:DefineExternalPlanMappingResponse)
    return true
  end

  # remove mapping, sent by EI on startup
  def unresolve_plan(params={})
    $log.info "unresolve_plan #{@eqp}, #{@plan}"
    xml = Builder::XmlMarkup.new(indent: 2)
    xml.instruct!
    xml.env :Envelope, 'xmlns:ns'=>'http://www.amd.com/schemas/interfaceAExtended',
      'xmlns:env'=>'http://schemas.xmlsoap.org/soap/envelope/' do |enve|
      enve.env :Body do |body|
        body.ns :UnresolvePlanRequest do |pmrq|
          pmrq.ns :equipmentId, @eqp
          pmrq.ns :planName, @plan
        end
      end
    end
    action = 'UnresolvePlan'
    res = post(xml.target!, {soapaction: action}.merge(params))
    return res if res.nil? || params[:nosend]
    ($log.warn "#{action} error: #{res.inspect}"; return) unless res.has_key?(:UnresolvePlanResponse)
    return true
  end

  # send NewDataNotification, return true on success  currently not used
  def new_data(params={})
    $log.info "new_data (#{@eqp})"
    report_time = Time.now.utc.iso8601(3)
    xml = Builder::XmlMarkup.new(indent: 2)
    xml.instruct!
    xml.env :Envelope, 'xmlns:ns'=>'http://www.amd.com/schemas/interfaceAExtended',
      'xmlns:env'=>'http://schemas.xmlsoap.org/soap/envelope/' do |enve|
      enve.env :Body do |body|
        body.ns :NewDataNotification do |drq|
          drq.ns :DCR, 'planId'=>@plan, 'reportTime'=>report_time
          drq.ns :planName, @plan
          drq.ns :factoryAutomationSessionId, 'xxx'
          drq.ns :traceReport, 'traceId'=>'0', 'reportTime'=>report_time do |rpt|
            #@parameters.each_pair {|p, dtype| rpt.ns :parameterName, p}
            (params[:collections] || []).each {|c|
              rpt.ns :TR, 'collectionTime'=>c[0].utc.iso8601(3) do |coll|
                c[1].each {|v| coll.ns :PV do |val|
                  if v
                    puts 'todo'
                  else
                    val.ns :NoValue, 'reasonCode'=>'ValueNotAvailable' do |noval|
                      noval.ns :description, 'Parameter Name not found in DataCompilation:Equipment:CtMcwFlow'
                    end
                  end
                end
                }
              end
            }
          end
        end
      end
    end
    action = 'NewDataNotification'
    res = post(xml.target!, {soapaction: action}.merge(params))
    return res if res.nil? || params[:nosend]
    ($log.warn "#{action} error: #{res.inspect}"; return) unless res.has_key?(:NewDataNotificationResponse)
    return true
  end

  # send NotifyExternalDataRequest, return true on success
  def report(colls, params={})
    $log.info "report (#{colls.size} colls)"
    xml = Builder::XmlMarkup.new(indent: 2)
    xml.instruct!
    xml.env :Envelope, 'xmlns:ns'=>'http://www.amd.com/schemas/interfaceAExtended',
      'xmlns:env'=>'http://schemas.xmlsoap.org/soap/envelope/' do |enve|
      enve.env :Body do |body|
        body.ns :NotifyExternalDataRequest do |drq|
          drq.ns :equipmentId, @eqp
          drq.ns :planName, @plan
          drq.ns :traceReport do |rpt|
            rpt.ns :traceRequestName, 'ProcessingChamberTrace'
            @parameters.each_pair {|p, dtype| rpt.ns :parameterName, p}
            colls.each {|c|
              rtime, data = c
              body.ns :TR, collectionTime: rtime.utc.iso8601(3) do |tr|
                data.each {|v| tr.ns :PV, v}
              end
            }
          end
        end
      end
    end
    action = 'NotifyExternalData'
    res = post(xml.target!, {soapaction: action}.merge(params))
    return res if res.nil? || params[:nosend]
    ($log.warn "#{action} error: #{res.inspect}"; return) unless res.has_key?(:NotifyExternalDataResponse)
    return true
  end

  # send NotifyExternalEventRequest, return true on success
  def report_event(event, params={})
    $log.info "report_event #{event.inspect}"
    $log.debug {params}
    ($log.error "No concrete mapping for event"; return) unless @events.has_key?(event)
    tstamp = Time.now
    contexts = {}
    elem = @events[event].values.flatten
    elem.each {|e|
      ename = e[e.rindex('/')+1 .. -1]  # "PA:PM1/WaferID" -> "WaferID"
      contexts[e] = case ename
      when 'ChamberID';         params[:chamber] || event[event.index(':')+1 .. event.index('/')-1]
      when 'WaferID';           params[:wafer] || 'QAWAFER01001'
      when 'RecipeName';        params[:recipename] || @recipename
      when 'LotID';             params[:lot] || 'QALOT01.00'
      when 'ControlJobID';      params[:cj] || "#{@eqp}-20130416-0001"
      when 'SlotID';            params[:slot] || 1
      when 'RecipeStepName';    params[:recipestepname] || 'Q-PM-PC-MECH-STEP1'
      when 'RecipeStepNumber';  params[:recipestepno] || 1
      else
        $log.warn "cannot match context: #{c}"
        nil
      end
    }
    xml = Builder::XmlMarkup.new(indent: 2)
    xml.instruct!
    xml.env :Envelope, 'xmlns:ns'=>'http://www.amd.com/schemas/interfaceAExtended',
      'xmlns:env'=>'http://schemas.xmlsoap.org/soap/envelope/' do |enve|
      enve.env :Body do |body|
        body.ns :NotifyExternalEventRequest do |drq|
          drq.ns :equipmentId, @eqp
          drq.ns :planName, @plan
          drq.ns :eventName, event
          drq.ns :timeStamp, tstamp.utc.iso8601(3)
          drq.ns :contextData do |rpt|
            contexts.each {|c|
              key, value = c
              rpt.ns :KeyValueType do |kv|
                kv.ns :key, key
                kv.ns :value, value
              end
            }
          end
        end
      end
    end
    action = 'NotifyExternalEvent'
    res = post(xml.target!, {soapaction: action, nosend: params[:nosend]})
    return res if res.nil? || params[:nosend]
    ($log.warn "#{action} error: #{res.inspect}"; return) unless res.has_key?(:NotifyExternalEventResponse)
    return true
  end

  # send reports and events for lots, used in load tests
  #
  # lot IDs are created on the fly based on the test time
  #   each wafer of a lot (10) is processed sequentially in all chambers
  #
  # return true on success
  def report_loop(params={})
    $log.info "report_loop #{params}"
    unresolve_plan
    plan_mapping
    @collections.clear
    @collections2.clear unless @collections2.nil?  # optional support for 2nd historian
    @report_times = []
    @cjs = []
    @lots = []
    @wafers = []
    duration = params[:duration] || 60
    sinterval = params[:sinterval] || 0.5
    ncollections = params[:ncollections] || 3
    looptime = ncollections * sinterval
    cmpratio = params[:cmpratio] || 4        # store every nth collection for comparison, 0 for none
    recipesteps = params[:recipesteps] || 4  # recipesteps per wafer and chamber
    nwafers = params[:nwafers] || 10
    nlots = (duration.to_f / looptime / nwafers / recipesteps / chambers.size).ceil
    $log.info "report_loop duration: #{duration_string(duration)}, cmpratio: #{cmpratio}, recipesteps: #{recipesteps}"
    loop = 0
    tnext = Time.now + looptime
    tend = Time.now + duration
    nlots.times {|nlot|
      cj = "#{@eqp}-#{tnext.to_i}"
      @cjs << cj
      lot = "QA#{unique_string}.00"
      waferbase = lot.split('.').first
      @lots << lot
      $log.info "lot #{nlot+1}/#{nlots}: #{lot.inspect}"
      nwafers.times {|nwafer|
        now = Time.now
        break if now > tend
        wafer = "WW#{waferbase}%2.2d" % nwafer
        @wafers << wafer
        @chambers.each {|ch|
          break if now > tend
          report_event("PA:#{ch}/ProcessingStarted_Coronus",
            chamber: ch, cj: cj, lot: lot, wafer: wafer, slot: nwafer+1)
          recipesteps.times {|rstep|
            break if now > tend
            if @doreport_recipesteps
              report_event("PA:#{ch}/RecipeStepStarted_Coronus",
                chamber: ch, recipestepname: "#{@recipename}-#{rstep+1}", recipestepno: rstep+1)
            end
            #
            # sync loops with sample time
            tsleep = tnext - Time.now
            $log.info "collection# #{loop}, sleeping #{tsleep.round(1)}s"
            sleep tsleep if tsleep > 0
            tnext += looptime
            #
            # send a data collection
            colls = create_collections(params)
            report(colls, params)
            # optionally put collections in the queue for further evaluation
            if !cmpratio.zero? && ((loop % cmpratio) == 0)
              $log.info "storing collection# #{loop}"
              @report_times << Time.now
              @collections << colls
              @collections2 << colls unless @collections2.nil?  # optional support for 2nd historian
            end
            loop += 1
            #
            if @doreport_recipesteps
              report_event("PA:#{ch}/RecipeStepCompleted_Coronus",
                chamber: ch, recipestepname: "#{@recipename}-#{rstep+1}", recipestepno: rstep+1)
            end
          }
          report_event("PA:#{ch}/ProcessingCompleted_Coronus",
            chamber: ch, cj: cj, lot: lot, wafer: wafer, slot: nwafer+1)
        }
      }
    }
    #
    @collections << nil  # end marker
    @collections2 << nil unless @collections2.nil?  # end marker for optional 2nd historian
    return true
  end

  # create collections of samples with timestamps in the the past (max 30 seconds allowed!)
  def create_collections(params={})
    seed = params[:seed] || 0
    func = params[:func] || lambda {|x, y, seed| seed + x + y}
    timeshift = params[:timeshift] || 10
    sinterval = params[:sinterval] || 0.5
    ncollections = params[:ncollections] || 3
    teststring = params[:teststring] || ' '
    tstamp = Time.now - timeshift - ncollections * sinterval
    ncollections.times.collect {|i|
      j = 0
      tstring = tstamp.utc.iso8601(3)
      values = @parameters.collect {|p, dtype|
        if dtype == 'float'
          v = i + j + seed + 0.1
        elsif dtype == 'unsigned integer'
          v = (seed + i + j).to_i
        elsif dtype == 'signed integer'
          v = -(seed + i + j).to_i
        elsif dtype == 'double'
          v = (seed + i + j) * Float::MIN
        elsif dtype == 'binary'
          v = (seed + i + j).to_s(2)
        elsif dtype == 'boolean'
          v = j.odd?
        elsif dtype.start_with?('string')
          ssize = [1, dtype.split[-1].to_i - tstring.size - teststring.size].max
          v = tstring + teststring + 'Q' * ssize
        else
          $log.warn "unknown datatype #{dtype}"
        end
        j += 1
        v
      }
      tstamp += sinterval
      [tstamp, values]
    }
  end

end
