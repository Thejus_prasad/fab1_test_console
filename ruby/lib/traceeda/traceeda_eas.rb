=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.


Author:     ssteidte, 2012-01-30
            dsteger, 2013-04-20
Version:    1.4.0

History:
  2012-01-30 ssteidte,  adapted to new xml_to_hash
  2013-04-12 dsteger,   added error general error handling, trace event collection
  2013-08-23 ssteidte,  changed back to old error handling to avoid aborts on errors
  2013-10-14 ssteidte,  separated E3 classes
  2013-10-23 ssteidte,  pass test duration and calculate loops
  2013-12-02 ssteidte,  new report_loop with clearer events, lot and wafer creation
  2014-01-29 ssteidte,  use queues for sync
  2014-02-07 ssteidte,  group ETCs in batches sending with a time shift, new charting
  2016-08-22 sfrieske,  inherit from new RequestResponse::HttpXML
  2016-09-09 sfrieske,  separated traceeda into traceeda_eas, traceeda_etc and traceeda_wwhist
  2021-08-27 sfrieske, require uniquestring instead of misc
=end

require 'builder'
require 'rqrsp_http'

ENV.delete('http_proxy')


# Communication with EDA Application Server
class TraceEDA::EAS < RequestResponse::HttpXML
  attr_accessor :env, :sid, :plan, :mappings, :evt_map, :alarms, :units

  def initialize(env, params={})
    super("traceeda_eas.#{env}", params)
    @env = env
    @plan = params[:plan] || 'QA_LETCL'
    @units = params[:units] || 'W'
    @mappings = params[:mappings]
    unless @mappings
      @mappings = {}
      25.times {|i| @mappings["PA:[PA=ProcessingChamber]/QAFloatTraceVar#{i+1}"] = 'float'}
      25.times {|i| @mappings["PA:[PA=ProcessingChamber]/QAIntegerTraceVar#{i+1}"] = 'unsigned integer'}
      @mappings['PA:[PA=ProcessingChamber]/QAStringTraceVar1'] = 'string 32'
      @mappings['PA:[PA=ProcessingChamber]/QAStringTraceVar2'] = 'string 128'
      @mappings['PA:[PA=ProcessingChamber]/QAStringTraceVar3'] = 'string 512'
    end
    @alarms = params[:alarms] || ['Alarm1']
    @evt_map = params[:event_map] || {
      'LotStarted_Coronus'=>{'LotStarted'=>['LotID', 'ControlJobID']},
      'LotFailed_Coronus'=>{'LotFailed'=>['LotID', 'ControlJobID']},
      'LotCompleted_Coronus'=>{'LotCompleted'=>['LotID', 'ControlJobID']},
      'PA:[PA=ProcessingChamber]/ProcessingStarted_Coronus'=>{'PA:[PA=ProcessingChamber]/ProcessStarted'=>
         [ 'PA:[PA=ProcessingChamber]/LotID', 'PA:[PA=ProcessingChamber]/WaferID',
           'PA:[PA=ProcessingChamber]/SlotID', 'PA:[PA=ProcessingChamber]/RecipeName',
           'PA:[PA=ProcessingChamber]/ControlJobID']
      },
      'PA:[PA=ProcessingChamber]/ProcessingCompleted_Coronus'=>{'PA:[PA=ProcessingChamber]/ProcessCompleted'=>
         ['PA:[PA=ProcessingChamber]/LotID', 'PA:[PA=ProcessingChamber]/WaferID',
           'PA:[PA=ProcessingChamber]/SlotID', 'PA:[PA=ProcessingChamber]/RecipeName',
           'PA:[PA=ProcessingChamber]/ControlJobID']
      },
      'PA:[PA=ProcessingChamber]/ProcessingFailed_Coronus'=>{'PA:[PA=ProcessingChamber]/ProcessCompleted'=>
         ['PA:[PA=ProcessingChamber]/LotID', 'PA:[PA=ProcessingChamber]/WaferID',
           'PA:[PA=ProcessingChamber]/SlotID', 'PA:[PA=ProcessingChamber]/RecipeName',
           'PA:[PA=ProcessingChamber]/ControlJobID']
      },
      'PA:[PA=ProcessingChamber]/RecipeStepStarted_Coronus'=>{'PA:[PA=ProcessingChamber]/ProcessStepStarted'=>
         ['PA:[PA=ProcessingChamber]/RecipeStepName', 'PA:[PA=ProcessingChamber]/RecipeStepNumber']
      },
      'PA:[PA=ProcessingChamber]/RecipeStepCompleted_Coronus'=>{'PA:[PA=ProcessingChamber]/ProcessStepCompleted'=>
         ['PA:[PA=ProcessingChamber]/RecipeStepName', 'PA:[PA=ProcessingChamber]/RecipeStepNumber']
      },
      'PA:[PA=ProcessingChamber]/RecipeStepFailed_Coronus'=>{'PA:[PA=ProcessingChamber]/ProcessStepFailed'=>
         ['PA:[PA=ProcessingChamber]/RecipeStepName', 'PA:[PA=ProcessingChamber]/RecipeStepNumber']
      },
      'PA:[PA=ProcessingChamber]/MaterialStarted_Coronus'=>{
        'PA:[PA=ProcessingChamber]/MaterialStarted'=>['PA:[PA=ProcessingChamber]/WaferID']
      },
      'PA:[PA=ProcessingChamber]/MaterialCompleted_Coronus'=>{
        'PA:[PA=ProcessingChamber]/MaterialCompleted'=>['PA:[PA=ProcessingChamber]/WaferID']
      }
    }
  end

  def connect(params={})
    user = params[:user] || 'stressTestScript'
    xml = Builder::XmlMarkup.new(indent: 2)
    xml.instruct!
    xml.env :Envelope, 'xmlns:env'=>'http://schemas.xmlsoap.org/soap/envelope/' do |enve|
      enve.env :Body, 'xmlns:xsi'=>'http://www.w3.org/2001/XMLSchema-instance',
        'xmlns:xsd'=>'http://www.w3.org/2001/XMLSchema' do |body|
        body.EstablishSessionRequest 'xmlns'=>'http://www.amd.com/schemas/interfaceAExtended' do |esrq|
          esrq.userName user
          esrq.httpClientSinkEndpoint 'noEndpoint'
        end
      end
    end
    action = 'EstablishSession'
    res = post(xml.target!, {soapaction: action}.merge(params))
    return res if res.nil? || params[:nosend]
    res = res[:EstablishSessionResponse]
    ($log.warn "#{action} error: #{res.inspect}"; return) unless res && res.has_key?(:sessionId)
    @sid = res[:sessionId]
  end

  def close(params={})
    sid = params[:sid] || @sid
    xml = Builder::XmlMarkup.new(indent: 2)
    xml.instruct!
    xml.env :Envelope, 'xmlns:env'=>'http://schemas.xmlsoap.org/soap/envelope/' do |enve|
      enve.env :Body, 'xmlns:xsi'=>'http://www.w3.org/2001/XMLSchema-instance',
        'xmlns:xsd'=>'http://www.w3.org/2001/XMLSchema' do |body|
        body.CloseSessionRequest 'xmlns'=>'http://www.amd.com/schemas/interfaceAExtended' do |sess|
          sess.sessionId sid
        end
      end
    end
    action = 'CloseSession'
    res = post(xml.target!, {soapaction: action}.merge(params))
    return res if params[:nosend]
    ($log.warn "#{action} error: #{res.inspect}"; return) unless res && res.has_key?(:CloseSessionResponse)
    @sid = nil if sid == @sid
    return true
  end

  # abstract mapping, sent by ARMOR on plan update
  def store_plan(params={})
    was_connected = @sid
    connect unless was_connected
    p = (params[:plan] or @plan)
    $log.info "store_plan plan: #{p.inspect}"
    xmlmp = Builder::XmlMarkup.new(indent: 0)
    xmlmp.instruct!
    xmlmp.PlanMapping 'xmlns'=>'http://www.amd.com/schemas/interfaceAExtended' do |pm|
      (params[:evt_map] || @evt_map).each_pair do |k,v|
        v.each_pair do |t,contexts|
          pm.AbstractExternalEventMapping 'name'=>k, 'tagName'=>"Eqp:[EQP]/#{t}" do |am|
            contexts.each do |c|
              am.EventDataMapping 'name'=>c, 'tagName'=>"Eqp:[EQP]/#{c}", 'dataType'=>'string'
            end
          end
        end
      end
      pm.AbstractExternalTraceDataMapping 'traceRequestName'=>'ProcessingChamberTrace' do |am|
        (params[:mappings] || @mappings).each_pair {|k,v|
          v = 'string' if v.start_with?('string')
          am.TraceParameterMapping 'name'=>k, 'tagName'=>"Eqp:[EQP]/#{k}", 'dataType'=>v, 'units'=>@units
        }
      end
      (params[:alarms] || @alarms).each do |e|
        pm.ExceptionMapping 'name'=>e, 'tagName'=>"Eqp:[EQP]/#{e}"
      end
    end
    xml = Builder::XmlMarkup.new(indent: 2)
    xml.instruct!
    xml.env :Envelope, 'xmlns:env'=>'http://schemas.xmlsoap.org/soap/envelope/' do |enve|
      enve.env :Body do |body|
        body.StorePlanRequest 'xmlns'=>'http://www.amd.com/schemas/interfaceAExtended' do |sprq|
          sprq.sessionId @sid
          sprq.planName p
          sprq.planXML ''
          sprq.mappingXML xmlmp.target!
        end
      end
    end
    action = 'StorePlan'
    res = post(xml.target!, {soapaction: action}.merge(params))
    close unless was_connected
    return res if res.nil? or params[:nosend]
    ($log.warn "#{action} error: #{res.inspect}"; return) unless res && res.has_key?(:StorePlanResponse)
    return true
  end

  # active plans, sent by EI on startup
  def active_plans(eqp, params={})
    $log.info "active_plans (#{eqp})"
    xml = Builder::XmlMarkup.new(indent: 2)
    xml.instruct!
    xml.env :Envelope, 'xmlns:ns'=>'http://www.amd.com/schemas/interfaceAExtended',
      'xmlns:env'=>'http://schemas.xmlsoap.org/soap/envelope/' do |enve|
      enve.env :Body do |body|
        body.ns :GetActivePlansRequest do |pmrq|
          pmrq.ns :sessionId, @sid
          pmrq.ns :equipmentId, eqp
        end
      end
    end
    action = 'GetActivePlans'
    res = post(xml.target!, {soapaction: action}.merge(params))
    ## return TraceEDA::parse_response(action, res, params.merge({:tagname=>:activePlans}))
    return res if res.nil? or params[:nosend]
    res = res[:GetActivePlansResponse]
    ($log.warn "#{action} error: #{res.inspect}"; return) unless res && res.has_key?(:activePlans)
    return res[:activePlans]
  end

  # get the tool URL, sent by EI on startup
  def tool_url(eqp, params={})
    service = params[:service] || 'externalDataSink'
    $log.info "tool_url #{eqp.inspect}"
    was_connected = @sid
    connect unless was_connected
    xml = Builder::XmlMarkup.new(indent: 2)
    xml.instruct!
    xml.env :Envelope, 'xmlns:env'=>'http://schemas.xmlsoap.org/soap/envelope/',
      'xmlns:ns'=>'http://www.amd.com/schemas/interfaceAExtended' do |enve|
      enve.env :Body do |body|
        body.ns :GetToolClientURLRequest do |tcrq|
          tcrq.ns :sessionId, @sid
          tcrq.ns :equipmentId, eqp
          tcrq.ns :serviceName, service
        end
      end
    end
    action = 'GetToolClientURL'
    res = post(xml.target!, {soapaction: action}.merge(params))
    close unless was_connected
    return res if res.nil? or params[:nosend]
    res = res[:GetToolClientURLResponse]
    ($log.warn "#{action} error: #{res.inspect}"; return) unless res && res.has_key?(:url)
    return res[:url]
  end

  # get the tool's freeze level
  def tool_freeze(eqp, params={})
    $log.info "tool_freeze #{eqp.inspect}"
    was_connected = @sid
    connect unless was_connected
    xml = Builder::XmlMarkup.new(indent: 2)
    xml.instruct!
    xml.env :Envelope, 'xmlns:env'=>'http://schemas.xmlsoap.org/soap/envelope/',
      'xmlns:ns'=>'http://www.amd.com/schemas/interfaceAExtended' do |enve|
      enve.env :Body do |body|
        body.ns :GetFreezeRequest do |tcrq|
          tcrq.ns :sessionId, @sid
          tcrq.ns :equipmentId, eqp
        end
      end
    end
    action = 'GetFreeze'
    res = post(xml.target!, {soapaction: action}.merge(params))
    close unless was_connected
    ## return TraceEDA::parse_response(action, res, params.merge({:tagname=>:freezeLevel}))
    return res if res.nil? or params[:nosend]
    res = res[:GetFreezeResponse]
    ($log.warn "#{action} error: #{res.inspect}"; return) unless res && res.has_key?(:freezeLevel)
    return res[:freezeLevel]
  end
end
