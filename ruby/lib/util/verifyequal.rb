=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.


Author: Steffen Steidten, 2011-06-30

History:
  ...
  2021-08-26 sfrieske, split off read_properties, _config_from_file, duration_string, verify_hash, verify_equal
    removed get_password, duration_in_seconds
=end


# compare two objects using "==", return true or false
def verify_equal(expected, actual, msg=nil)
  return true if expected == actual
  errmsg = "expected: #{expected.inspect}, got: #{actual.inspect}"
  errmsg = "#{msg}: #{errmsg}" if msg
  $log.warn errmsg
  return false
end
