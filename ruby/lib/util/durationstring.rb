=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.


Author: Steffen Steidten, 2011-06-30

History:
  ...
  2021-08-26 sfrieske, split duration_string off of misc
=end


# return duration in seconds as nice string, like "1.5 h"
def duration_string(d)
  if d > 86400 * 1.5
    return "#{(d/86400.0).round(1)} d"
  elsif d > 3600 * 1.5
    return "#{(d/3600.0).round(1)} h"
  elsif d > 60 * 1.5
    return "#{(d/60.0).round(1)} m"
  else
    return "#{d.round(1)} s"
  end
end
