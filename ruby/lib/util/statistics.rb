=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.


Author: Steffen Frieske, 2017-08-04

History:
  2017-08-04 sfrieske, moved statisctics extension of Enumarable into a separate module
=end

# Statistical Calculations
module Statistics
  def self.sum(a)
    return a.inject(:+)
  end

  def self.average(a)
    return nil if a.empty?
    return a.inject(:+) / a.size.to_f
  end

  def self.median(a)
    return nil if a.empty?
    if a.size % 2 == 0
      return (a.sort[a.size/2].to_f + a.sort[(a.size/2) - 1].to_f)/2
    else
      return a.sort[a.size/2]
    end
  end

  def self.variance(a)
    return nil if a.empty?
    m = average(a)
    sum = a.inject(0) {|acc, i| acc + (i - m)**2}
    return sum/(a.size - 1).to_f
  end

  def self.stddev(a)
    return nil if a.empty?
    return Math.sqrt(variance(a))
  end

  def self.calc(a)
    a = a.values.flatten if a.kind_of?(Hash)  # take all the values
    return nil if a.empty?
    avg = average(a)
    dev = stddev(a)
    {average: avg, stddev: dev, stddev_pct: 100.0 * dev / avg, median: median(a), range: a.max - a.min, min: a.min, max: a.max, count: a.count}
  end

  # same as calculate, with SPACE key names
  def self.space(a)
    return nil if a.empty?
    s = calc(a)
    {'MEAN'=>s[:average], 'MIN'=>s[:min], 'MAX'=>s[:max], 'STDDEV'=>s[:stddev], 'RANGE'=>s[:range], 'MEDIAN'=>s[:median], 'COUNT'=>s[:count]}
  end

end


# # extends the module enumerable to have some statistical calculations available in class Array,
# # conforming with SPACE conventions
# module Enumerable
#   def sum
#     return self.inject(:+)
#   end
#
#   def average
#     return self.sum / self.length.to_f
#   end
#   alias :mean :average
#   alias :avg :average
#
#   def median
#     if self.length % 2 == 0
#       return (self.sort[self.length/2].to_f + self.sort[(self.length/2)-1].to_f)/2
#     else
#       return self.sort[self.length/2]
#     end
#   end
#
#   def sample_variance
#     m = self.mean
#     sum = self.inject(0) {|acc, i| acc + (i-m)**2}
#     return sum/(self.length - 1).to_f
#   end
#
#   def standard_deviation
#     return Math.sqrt(self.sample_variance)
#   end
#
#   def range
#     return (self.max - self.min)
#   end
# end
#
#
