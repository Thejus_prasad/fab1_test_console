=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.


Author:   Steffen Steidten, 2012-07-19

Version:  1.2.2

History:
  2013-04-20 ssteidte, removed DEBUG logging because it is too much for perf tests
  2016-04-07 sfrieske, hash_to_xml can convert JSON style hashes (no symbols)
  2016-08-24 sfrieske, moved _service_response to legacy rqrsp.rb
  2016-09-07 sfrieske, xml_to_hash can create valid SOAP hashes if the option withprefix is set
  2018-01-02 sfrieske, moved add_namespace_to_hash to sfctest.rb (only used there)
  2019-01-17 sfrieske, added functions as module_function, allowing them to be called without include
  2019-07-26 sfrieske, remove dependency on Builder, removed namespace, _singletag_ and JSON options
  2021-02-05 sfrieske, use instance_of instead of kind_of
=end

require 'rexml/document'


# Parse XML data to Hash and create XML documents from a Hash. Mapping:
# XML         Hash
# -----------------------------------------------------
# Tag         Symbol: value
# Attribute   String=>value
# Value       String
# Value       nil=>value  if the tag has attributes and a value
module XMLHash

  # return XML String from hash, tagname is the anchor element, xml is used internally
  def hash_to_xml(h, tagname=nil, xml=nil, pretty=false)
    curele = xml.nil? ? REXML::Document.new('<?xml version="1.0" encoding="UTF-8"?>') : xml
    curele.context[:attribute_quote] = :quote
    if tagname.nil?
      # assuming h has one key
      tagname = h.keys.first
      h = h[tagname]
    end
    # $log.debug {"  hash_to_xml, tagname: #{tagname.inspect}"}
    # $log.debug {"  hash: #{h.inspect}"}
    attrs = {}
    tt = []
    h.each_pair {|k, v|
      next if k.nil?  # valid case, handled later
      if k.instance_of?(String)
        if v.nil?
          # Space::DCRs did it, should be fixed at source (else an unwanted attribute like attrib="" is sent)
          $log.warn "hash_to_xml: passed nil value for attribute #{k.inspect}"
        else
          attrs[k] = v
        end
      elsif k.instance_of?(Symbol)
        tt << k
      else
        $log.warn "hash_to_xml: unsupported data: #{k.inspect}, #{v.inspect}"
      end
    }
    # $log.debug {"  attrs: #{attrs.inspect}"}
    # $log.debug {"  tt: #{tt.inspect}"}
    if tt.size > 0
      subtag = curele.add_element(tagname.to_s, attrs)
      tt.each {|t|
        v = h[t]
        if v.instance_of?(Hash)
          hash_to_xml(v, t, subtag)
        elsif v.instance_of?(Array)
          if v.first.instance_of?(String)  # multiple strings
            v.each {|ve|
              txtele = subtag.add_element(t.to_s)
              txtele.text = ve
            }
          else
            v.each {|e| hash_to_xml(e, t, subtag)}
          end
        else
          txtele = subtag.add_element(t.to_s)
          txtele.text = v  # v can be nil -> empty element
        end
      }
    else
      subtag = curele.add_element(tagname.to_s, attrs)
      subtag.text = h[nil]  # nil key means there is a single value for a tag (with attributes)
    end
    return if xml  # called recursively
    ret = String.new
    if pretty  # for dev
      fmtr = REXML::Formatters::Pretty.new
      fmtr.compact = true
      fmtr.write(curele, ret)
    else
      curele.write(ret)
    end
    return ret
  end
  module_function :hash_to_xml

  # return a well readable string, for dev
  def pretty_format(xml)  #, doublequotes=false)
    doc = xml.instance_of?(String) ? REXML::Document.new(xml) : xml
    doc.context[:attribute_quote] = :quote
    ret = String.new
    fmtr = REXML::Formatters::Pretty.new
    fmtr.compact = true
    fmtr.write(doc, ret)
    return ret  #doublequotes ? ret.gsub("'", '"') + "\n" : ret
  end
  module_function :pretty_format

  # convert an XML document or string to a hash, starting at an body tag like '*//*Body' (default: root)
  def xml_to_hash(e, bodytag=nil, withprefix=false)
    return nil if e.nil?
    if e.instance_of?(String)
      r = REXML::Document.new(e).root
      r = r.elements[bodytag] if bodytag
      name = r.name
      name = "#{r.prefix}:#{name}" if withprefix
      return {name.to_sym=>xml_to_hash(r, nil, withprefix)}
    end
    #
    #$log.debug "xml_to_hash #{e.name} # #{e.elements.size} elements"   # may be too much in perf tests
    if e.elements.size == 0
      #$log.debug "  leaf node"   # may be too much in perf tests
      # leaf node
      if e.has_attributes?
        #$log.debug "    has attributes"   # may be too much in perf tests
        ret = {}
        e.attributes.each {|attr, value| ret[attr] = value}
        ret[nil] = e.text if e.text
        return ret
      elsif e.text
        return e.text
      else
        return nil
      end
    else
      # collect child nodes
      ret = {}
      e.attributes.each {|attr, value| ret[attr] = value}
      e.elements.each {|ee|
        res = xml_to_hash(ee, nil, withprefix)
        name = ee.name
        name = "#{ee.prefix}:#{name}" if withprefix
        name = name.to_sym
        if ret[name]
          # multiple occurances
          ret[name] = [ret[name]] unless ret[name].instance_of?(Array)
          ret[name] << res
        else
          # one (or first) occurance
          ret[name] = res
        end
      }
      return ret
    end
  end
  module_function :xml_to_hash

end
