=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.


Author: Steffen Steidten, 2011-06-30

History:
  ...
  2021-08-26 sfrieske, copy from misc.rb to migrate away from misc.rb
=end

if $__unique_id_mutex.nil?
  $__unique_id_mutex = Mutex.new
  # $__unique_id = 0
end


# return a unique integer based on the current time with 1 ms precision, thread and loop safe
def unique_id
  return $__unique_id_mutex.synchronize {
    sleep 0.002
    (Time.now.to_f * 1000).to_i
  }
end

# return a unique readable uppercase string based on a unique id
def unique_string(prefix=nil)
  id = unique_id.to_s(36).upcase
  return prefix.nil? ? id : prefix + id
end
