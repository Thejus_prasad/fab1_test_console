=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.


Author: Steffen Steidten, 2011-06-30

History:
  ...
  2021-08-26 sfrieske, split read_properties and _config_from_file off of misc
    removed get_password, duration_in_seconds, duration_string
=end

require 'yaml'


# get the entry described by key from a parameter hash, provided in a YAML configfile
def read_config(key, configfile, params={})
  begin
    entries = YAML.load_file(configfile)
    return entries[key.to_s].merge(params)
  rescue
    puts "error reading instance #{key.inspect} from configfile #{configfile.inspect}: #{$!}"
    return params
  end
end
