=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.


Author: Steffen Steidten, 2011-06-30

History:
  ...
  2021-08-26 sfrieske, split off read_properties, _config_from_file, duration_string, verify_hash
    removed get_password, duration_in_seconds
=end


# verify a hash, printing the difference
def verify_hash(expected, actual, msg=nil, params={})
  # Recursively diff two hashes, see Henrik Nyh <http://henrik.nyh.se> 2009-07-14 (MIT license.)
  def _deep_diff_hashes(a, b, params)
    timediff = params[:timediff] || 0   # allowed difference between times
    numdiff = params[:numdiff] || 0     # allowed difference between (float) numbers
    exclude = params[:exclude] || []    # keys to exclude from comparison
    refonly = !!params[:refonly]        # flag, compare only the keys in the reference hash (skips additional keys actual)
    keys = a.keys
    keys |= b.keys unless refonly
    keys -= exclude
    keys.inject({}) {|diff, k|
      if a[k] != b[k]
        if a[k].instance_of?(Array) && b[k].instance_of?(Array)
          if a[k].size == b[k].size
            a[k].each_index {|i|
              res = _deep_diff_hashes(a[k][i], b[k][i], params)
              if res != {}
                diff[k] ||= []
                diff[k] << res
              end
            }
          else
            diff[k] = ["[array of #{a[k].size}]", "[array of #{b[k].size}]"]
          end
        elsif a[k].instance_of?(Hash) && b[k].instance_of?(Hash)
          res = _deep_diff_hashes(a[k], b[k], params)
          diff[k] = res unless res == {}
        elsif a[k].instance_of?(Time) && b[k].instance_of?(Time) && (a[k] - b[k]).abs <= timediff
          # nothing, this is good (times match)
        elsif a[k].kind_of?(Numeric) && b[k].kind_of?(Numeric) && (a[k] - b[k]).abs <= numdiff
          # nothing, this is good (numbers match)
        else
          diff[k] = [a[k], b[k]]
        end
      end
      diff
    }
  end
  #
  hdiff = _deep_diff_hashes(expected, actual, params)
  return hdiff if params[:return_diff]
  return true if hdiff.empty?
  $log.warn "wrong hash data:\n#{hdiff.pretty_inspect}"
  $log.warn msg if msg
  return false
end
