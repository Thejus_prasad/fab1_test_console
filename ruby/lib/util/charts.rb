=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author:  Steffen Steidten, GLOBALFOUNDRIES INC., 2012

Version: 2.0.1

History:
  2013-10-14 ssteidte, minor changes
  2014-09-08 ssteidte, code clean-up
  2016-08-22 sfrieske, handle empty datasets gracefully
  2019-08-08 sfrieske, add default colors, code cleanup
  2021-02-05 sfrieske, use instance_of instead of kind_of
=end

ENV_JAVA['java.awt.headless'] = 'true' if RbConfig::CONFIG['host_os'] == 'linux'
Dir['lib/charts/*jar'].each {|f| require f}

# Leverage JFreeChart to create charts, see http://www.jfree.org/jfreechart/api/javadoc/
module Chart

  class Base
    attr_accessor :colors, :series, :collection, :chart

    def initialize(data=nil, params={})
      @colors = params[:colors] || [
        java.awt.Color.new(16, 16, 192),
        java.awt.Color.new(192, 16, 16),
        java.awt.Color.new(16, 192, 16),
        java.awt.Color.new(192, 192, 16),
      ]
      @series = []
      add_series(data, params) if data
    end

    def render(params={})
      plot = @chart.plot
      plot.background_paint = java.awt.Color.white
      renderer = plot.renderer
      if params[:dots] or params[:dotsize]
        # show dots w/o line
        renderer = plot.renderer = org.jfree.chart.renderer.xy.XYDotRenderer.new
        dotsize = params[:dotsize] || 6
        renderer.dot_height = dotsize
        renderer.dot_width = dotsize
      elsif params[:datapoints] != false
        # show line with data points
        @series.each_index {|i| renderer.set_series_shapes_visible(i, true)}
      end
      if renderer.kind_of?(Java::OrgJfreeChartRendererXy::XYBarRenderer)
        renderer.bar_painter = org.jfree.chart.renderer.xy.StandardXYBarPainter.new
        renderer.margin = 0.8
        renderer.bar_alignment_factor = -0.5
      end
      @series.each_index {|i|
        renderer.set_series_paint(i, @colors[i])
      }
      # axes
      plot.domain_axis = org.jfree.chart.axis.LogAxis.new(plot.domain_axis.label) if params[:xlg]
      daxis = plot.domain_axis
      daxis.lower_bound = params.delete(:xmin) if params[:xmin]
      daxis.upper_bound = params.delete(:xmax) if params[:xmax]
      plot.range_axis = org.jfree.chart.axis.LogAxis.new(plot.range_axis.label) if params[:ylg]
      raxis = plot.range_axis
      raxis.lower_bound = params.delete(:ymin) if params[:ymin]
      raxis.upper_bound = params.delete(:ymax) if params[:ymax]
      xmin = daxis.lower_bound
      xmax = daxis.upper_bound
      ymin = raxis.lower_bound
      ymax = raxis.upper_bound
      # annotations
      if annotations = params[:notes]
        annotations = [annotations] if annotations.instance_of?(String)
        font = java.awt.Font.new('Sans', 0, 12)
        x = xmin + (xmax-xmin)/10
        y = ymax - (ymax-ymin)/10
        ystep = (ymax-ymin)/30
        annotations.each {|annotation|
          a = org.jfree.chart.annotations.XYTextAnnotation.new(annotation, x, y)
          a.font = font
          a.text_anchor = org.jfree.ui.TextAnchor::BASELINE_LEFT
          plot.add_annotation(a)
          y -= ystep
        }
      end
      show if params[:show]
      write(params) if params[:export]
      nil
    end

    def show
      cp = org.jfree.chart.ChartPanel.new(@chart)
      cp.fill_zoom_rectangle = true
      cp.preferred_size = java.awt.Dimension.new(600, 400)
      f = javax.swing.JFrame.new('Chart')
      f.content_pane = cp
      f.pack
      f.visible = true
    end

    def write(params={})
      width = params[:width] || 800
      height = params[:height] || 600
      rinfo = nil
      encode_alpha = false
      compression = params[:compression] || 0
      org.jfree.chart.ChartUtilities.save_chart_as_png(
        java.io.File.new(params[:export]), @chart, width, height, rinfo, encode_alpha, compression)
    end

    def encode_png(params={})
      width = params[:width] || 800
      height = params[:height] || 600
      encode_alpha = false
      compression = params[:compression] || 0
      buf = java.io.ByteArrayOutputStream.new
      org.jfree.chart.ChartUtilities.write_chart_as_png(buf, @chart, width, height, encode_alpha, compression)
      return String.from_java_bytes(buf.toByteArray)
    end

  end


  class XY < Base

    def add_series(data, params={})
      ($log.warn 'Chart::Time: no data'; return) if data.nil? || data.empty?
      s = org.jfree.data.xy.XYSeries.new(params[:name] || "#{series.size + 1}")
      m = s.java_method(:add, [Java::double, Java::double])
      data.each {|x, y| m.call(x, y)}
      @series << s
    end

    def create_chart(params={})
      @collection = org.jfree.data.xy.XYSeriesCollection.new
      @series.each {|s| @collection.add_series(s)}
      legend = params.has_key?(:legend) ? params[:legend] : @series.size > 1
      @chart = org.jfree.chart.ChartFactory.createXYLineChart(params[:title], params[:x], params[:y],
        @collection, org.jfree.chart.plot.PlotOrientation::VERTICAL, legend, false, false)
      render(params)
    end

  end


  class Time < Base

    def add_series(data, params={})
      ($log.warn 'Chart::Time: no data'; return) if data.nil? || data.empty?
      s = org.jfree.data.time.TimeSeries.new(params[:name] || "#{series.size + 1}")
      m = s.java_method(:addOrUpdate, [org.jfree.data.time.RegularTimePeriod, Java::double])
      data.each {|x, y| m.call(org.jfree.data.time.Millisecond.new(x), y)}
      @series << s
    end

    def create_chart(params={})
      @collection = org.jfree.data.time.TimeSeriesCollection.new
      @series.each {|s| @collection.add_series(s)}
      legend = params.has_key?(:legend) ? params[:legend] : @series.size > 1
      @chart = org.jfree.chart.ChartFactory.createTimeSeriesChart(params[:title], params[:x], params[:y],
        @collection, legend, false, false)
      render(params)
    end

  end


  class Histogram < Base

    def add_series(data, params={})
      ($log.warn 'Chart::Time: no data'; return) if data.nil? || data.empty?
      @series << data.to_java(Java::double)
    end

    def create_chart(params={})
      @collection = org.jfree.data.statistics.HistogramDataset.new
      @collection.type = org.jfree.data.statistics.HistogramType::RELATIVE_FREQUENCY # SCALE_AREA_TO_1 or RELATIVE_FREQUENCY
      @series.each_with_index {|s, i| @collection.add_series('', s, params[:bins] || 10)}
      legend = params.has_key?(:legend) ? params[:legend] : @series.size > 1
      @chart = org.jfree.chart.ChartFactory.createHistogram(params[:title], params[:x], params[:y] || 'relative frequency',
        @collection, org.jfree.chart.plot.PlotOrientation::VERTICAL, legend, false, false)
      render(params)
    end

  end

end
