=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.


Author: Steffen Steidten, 2011-06-30

History:
  ...
  2021-08-26 sfrieske, split off of misc.rb
=end


# wait for a condition, provided in a block, to become true, return true or false
def wait_for(params={})
  sleeptime = params[:sleeptime] || 10
  tstart = params[:tstart] || Time.now
  tend = tstart + (params[:timeout] || 300)
  loop {
    return true if yield
    return false if Time.now >= tend
    sleep sleeptime
  }
end
