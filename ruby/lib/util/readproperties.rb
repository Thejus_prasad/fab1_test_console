=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.


Author: Steffen Steidten, 2011-06-30

History:
  ...
  2021-08-26 sfrieske, split read_properties off of misc
=end


# read Java properties from a file or array of lines and return the extracted properties as Hash with keys converted to symbols
# currently used in remote.rb only
def read_properties(lines)
  if lines.instance_of?(String)  # assuming file name, else array of lines
    fname = File.expand_path(lines)
    return unless File.exist?(fname)
    lines = File.readlines(fname)
  end
  return unless lines
  ret = {}
  lines.each {|line|
    line.strip!
    next if line.empty? || line.start_with?('#') || !line.include?('=')
    ff = line.split('=', 2)
    ret[ff[0].strip.to_sym] = ff[1].strip
  }
  return ret
end
