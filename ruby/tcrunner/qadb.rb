=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: sfrieske, 2019-03-20

Version: 1.0.0

History:
=end

require 'yaml'
require 'dbsequel'


class QADB
  attr_accessor :db

  def initialize(params={})
    @db = ::DB.new(params[:instance] || 'fas1.f1qa')
    @db = nil if @db.dsn.nil?
  end

  def inspect
    "#<#{self.class.name} #{@db.inspect}>"
  end

  # return the envid for an environment like 'itdc'; create a new entry for envname on demand
  def envid(envname)
    ($log.warn "QADB#envid: no envname"; return) if envname.nil? || envname.empty?
    envname = envname.downcase
    res = @db.select('SELECT envid FROM env WHERE envname = ?', envname).first
    if res.nil?
      $log.info "QADB#envid: creating entry for ENVNAME: #{envname.inspect}"
      @db.insert('INSERT INTO env (ENVID, ENVNAME) VALUES(SEQUUID.nextval, ?)', envname) || return
      res = @db.select('SELECT envid FROM env WHERE envname = ?', envname).first || return
    end
    return res.first.to_i
  end

  # return the seqid for a sequence like 'siviewstress'; create a new entry for seqname on demand
  def seqid(seqname)
    ($log.warn "QADB#seqid: no seqname"; return) if seqname.nil? || seqname.empty?
    seqname.downcase!
    res = @db.select('SELECT seqid FROM seq WHERE seqname = ?', seqname).first
    if res.nil?
      $log.info "QADB#seqid: creating entry for SEQNAME: #{seqname.inspect}"
      @db.insert('INSERT INTO seq (SEQID, SEQNAME) VALUES(SEQUUID.nextval, ?)', seqname) || return
      res = @db.select('SELECT seqid FROM seq WHERE seqname = ?', seqname).first || return
    end
    return res.first.to_i
  end


  # get the TCRUN entry; create a new entry on demand
  def tcrun(envid, seqid, tc)
    cols = 'TID, ENVID, SEQID, NAME, PSECTION, STATUS, TSTART, DURATION, NOTES, PARAMETERS, REPORT, MTIME'
    name = tc[:name]
    ($log.warn "QADB#tcrun: no tc name"; return) if name.nil? || name.empty?
    psection = YAML.dump(tc[:psection])  # part of the unique key, YAML ensures it is not empty (null)
    q = "SELECT #{cols} FROM tcrun"
    q, qargs = @db._q_restriction(q, [], 'ENVID', envid)
    q, qargs = @db._q_restriction(q, qargs, 'SEQID', seqid)
    q, qargs = @db._q_restriction(q, qargs, 'NAME', name)
    q, qargs = @db._q_restriction(q, qargs, 'PSECTION', psection)
    res = @db.select(q, *qargs).first
    if res.nil?
      $log.info "QADB#tcrun: creating entry for #{envid}, #{seqid}, #{tc}"
      data = [envid, seqid, tc[:name], psection, YAML.dump(tc[:status]), tc[:tstart], tc[:duration], tc[:notes], 
              YAML.dump(tc[:parameters]), YAML.dump(tc[:report]), Time.now]
      @db.insert("INSERT INTO tcrun (#{cols}) VALUES(SEQUUID.nextval, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)", *data) || return
      res = @db.select(q, *qargs).first || return
    end
    return {name: res[3], psection: YAML.load(res[4]), status: YAML.load(res[5] || ''), tstart: res[6], duration: res[7].to_f, 
      notes: res[8] || '', parameters: YAML.load(res[9]), report: YAML.load(res[10]), mtime: res[11], tid: res[0].to_i}
  end

  # update a TCRUN entry, identified by TID that is part of the tc Hash
  def update_status(tc)
    q = 'UPDATE tcrun set STATUS = ?, TSTART = ?, DURATION = ?, REPORT = ?, MTIME = ? WHERE TID = ?'
    data = [YAML.dump(tc[:status]), tc[:tstart], tc[:duration], YAML.dump(tc[:report]), Time.now, tc[:tid]]
    @db.update(q, *data)
  end

end
