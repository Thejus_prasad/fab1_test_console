=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: Steffen Steidten, 2014-06-13

Version: 1.2.4

History:
  2016-07-05 sfrieske, added reloadlibs
  2018-04-13 sfrieske, use a Hash instead of struct for testcase objects
  2019-03-26 sfrieske, added loadlastrun
  2019-05-07 sfrieske, updated Sinatra and Rack
=end

require 'json'
require 'rack'
require 'tilt/erb'
require 'sinatra/base'
require_relative 'tcrunner'


class TCRunnerApp < Sinatra::Base
  set :protection, false

  attr_accessor :tcrunner, :oldstatus

  def initialize(env, **params, &block)
    super(nil, &block)
    self.class.set(:public_folder, params[:public_folder]) if params.has_key?(:public_folder)
    @tcrunner = TCRunner.new(env, params)
    @oldstatus = ''
  end
  
  # before do
  #   $request = @request
  #   puts "\n#{@request.inspect}"
  # end

  get '/' do
    erb :index
  end

  # list the test cases
  get '/list' do
    erb :list
  end

  # show a tc file
  get '/show/*' do |f|
    @tcfile = f
    @tccode = File.read(@tcfile)
    erb :show 
  end
  
  # testcase runner page
  get '/tcrunner' do
    # puts "settings: #{settings}"
    erb :tcrunner
  end

  # change environment, use with care
  get '/changeenv' do
    @tcrunner.env = params[:envname]
    ''
  end

  # reload siview.rb 
  get '/reloadlibs' do
    load 'siview.rb'
    ''
  end
  
  # store current sequence's result
  get '/storeresult' do
    @tcrunner.store_result
    ''
  end

  # get lst run's data from DB
  get '/loadlastrun' do
    @tcrunner.load_last_run
    ''
  end

  # start test sequence, mandatory parameter is seqname
  get '/loadsequence' do
    @tcrunner.load_sequence(params[:seqname])
    ''
  end
  
  # start the test run
  get '/startsequence' do
    @tcrunner.start_sequence(idx: params[:idxs].split(',').collect {|i| i.to_i})
    ''
  end

  # abort a running test, called via Ajax
  get '/stopsequence' do
    @tcrunner.stop_sequence
    ''
  end
  
  # start an individual test from the suite
  get '/starttestcase/*' do |idx|
    @tcrunner.start_sequence(idx: idx.to_i)
    ''
  end
  
  # stop the currently running test
  get '/stoptestcase/*' do |idx|
    @tcrunner.stop_testcase(idx.to_i)
    ''
  end

  # status update
  get '/status' do
    statusj = {status: @tcrunner.status, sequence: @tcrunner.sequence}.to_json
    if @oldstatus == statusj
      'null'
    else
      @oldstatus = statusj
    end
  end

  # show testcase data, e.g. result on a separate page
  get '/tcdata/*' do |idx|
    erb :tcdata, locals: {tc: @tcrunner.sequence[idx.to_i]}
  end

  # fallback
  get '/*' do |p|
    puts "\n\nGET not implemented for #{p.inspect}\n\n"
  end

  post '/*' do |p|
    puts "\n\nPOST not implemented for #{p.inspect}\n\n"
  end

end
