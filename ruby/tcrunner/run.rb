=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: Steffen Steidten, 2014-06-13

Version: 1.2.4

History:
  2017-02-08 sfrieske, support for interactive console
  2019-04-10 sfrieske, choose a free port by default (allows multiple instances)
  2019-05-07 sfrieske, updated Sinatra and Rack
=end

require 'socket'
require 'optparse'
require 'rack'
require_relative 'tcsinatra'


# defaults
env = ENV['TCRUNNER_ENV'] || 'itdc'
host = 'localhost'
port = 5579  # 0 not working in 9.2.8
tcdir = nil
loglevel = 0
# options
OptionParser.new do |opts|
  opts.banner = "Usage: #{__FILE__} [options]"
  opts.on('-e', '--env [ENV]', "environment, default is #{env}") {|v| env = v}
  opts.on('-a', '--addr HOST', String, "server address, default: #{host}") {|v| host = v}
  opts.on('-p', '--port PORT', Integer, "set port, default: #{port}") {|v| port = v}
  opts.on('-t', '--tcdir DIR', String, 'set testcase dir') {|v| tcdir = v}
  opts.on('-v', '--verbose', 'more verbose logging') {|v| loglevel -= 1}
  opts.separator ''
  opts.on('-h', '--help', 'Show this help message.') {puts opts; exit}
end.parse!

# pick a free port unless explicitly set
port = TCPServer.new(host, 0).addr[1] if port == 0
url = "http://#{host}:#{port}/qa/tcrunner"

# # regular test framework lib
# require_relative '../set_paths' unless Object.const_defined?(:IRB)
create_logger(file: "log/tcrunner_#{env}_#{port}.log")

$app = Rack::Builder.new {
  map('/env') {run proc {|env| [200, {}, env.keys.sort.collect {|k| "#{k}   #{env[k].inspect}\r\n"}]} }
  map('/logs') {run Rack::Directory.new('./log')}
  map('/assets') {run Rack::File.new(Dir.pwd + '/ruby/tcrunner/public/assets')}
  map('/qa') {run $tcrunnerapp = TCRunnerApp.new(env, tcdir: tcdir, loglevel: loglevel)}
}.to_app

# start the server, in a separate thread if called from IRB
$tcrunner = $tcrunnerapp.instance_variable_get(:@instance).tcrunner
$srv = Rack::Server.new(Host: host, Port: port, server: 'webrick', environment: 'none', quiet: true, AccessLog: [], app: $app)
if Object.const_defined?(:IRB)
  Thread.new {$srv.start}
  puts "\n\n-> The browser will open #{url} in 3 s.\n\n"
  sleep 3
  system("start #{url}")
else
  $srv.start
end
