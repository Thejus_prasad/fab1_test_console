=begin
(c) Copyright 2010 GLOBALFOUNDRIES Inc. All rights reserved.
    GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
    combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

Author: Steffen Steidten, 2014-06-13

Version: 1.1.0

History:
  2015-03-13 ssteidte, removed code for sequence conversion
  2015-10-14 sfrieske, switched to Ruby 2.2 (JRuby9)
  2017-06-06 sfrieske, TC status is :failed on TC load errors
  2017-11-10 sfrieske, set external_encoding BINARY
  2018-04-13 sfrieske, use a Hash instead of struct for testcase objects
  2019-03-20 sfrieske, started to optionally use a DB
=end

require 'fileutils'
require 'yaml'
require_relative 'qadb'
require 'RubyTestCase'

# external files can always be read, the reader decides on data encoding
Encoding.default_external = Encoding::BINARY


class TCRunner
  attr_accessor :env, :seqname, :seqthread, :reporter, :sequence, :status, :instance, :qadb, :envid, :seqid

  def initialize(env, params={})
    @env = env
    @qadb = QADB.new(instance: params[:instance])  # not to be confused with the TC instance!
    @qadb = nil if qadb.db.nil?
    @seqname = ''
    @sequence = []
    @status = :idle  # not yet loaded
  end

  def inspect
    "#<#{self.class.name} env: #{@env}>"
  end

  def environments
    Dir.entries(TestCases::ParameterDir).select {|e|
      !e.start_with?('.') && File.directory?(File.join(TestCases::ParameterDir, e))
    }
  end

  # return hash of directories (relative to tcdir) with the testcase files (absolute file names or tc names)
  def testcases(params={})
    dirs = Dir.glob("#{TestCases::TcDir}/**/*").select {|e| File.directory?(e)}
    tcs = {}
    dirs.sort.each {|d|
      mfiles = Dir["#{d}/*.rb"].sort
      mfiles.collect! {|f| File.basename(f, '.rb')} if params[:tcnames]
      dirname = File.basename(d.split(TestCases::TcDir).last)
      tcs[dirname] = mfiles
    }
    tcs
  end

  # return array of defined sequence names
  def sequence_names
    Dir.entries(TestCases::ParameterDir).collect {|e| e[0..-5] if e.end_with?('.seq')}.compact.sort
  end


  # test execution

  # loads sequence and properties into @oldtestsequence
  # testcase parameters can be edited afterwards, e.g. when debugging
  #
  # return :loaded or nil
  def load_sequence(seqname)
    $log.info "load_sequence #{seqname.inspect}"
    ($log.warn "test is already running"; return) if [:loading, :running].member?(@status)
    @status = :loading
    @seqname = seqname
    f = "#{TestCases::ParameterDir}/#{seqname}.seq"
    ($log.warn "  no such file: #{f.inspect}"; @status = :idle; return) unless File.readable?(f)
    tc = nil
    @sequence = File.readlines(f).collect {|line|
      line.chomp!
      next if line.start_with?('#', "\t") || line.strip.empty?
      if line.start_with?('  ') # 2 spaces: instance parameters for the previous tc
        begin
          tc[:parameters] = YAML.load(line)
        rescue
          $log.warn $!
          $log.warn "syntax error in line #{line.inspect}"
          break
        end
        nil
      else
        name, notes = line.split(',', 2)
        name, psection = name.split('-')
        name ||= ''
        psection ||= ''
        notes ||= ''
        tc = {name: name, psection: psection, status: '', tstart: nil, duration: nil, notes: notes, parameters: {}, report: []}
      end
    }
    if @sequence
      @sequence.compact!
      if @qadb
        @envid = @qadb.envid(@env)
        @seqid = @qadb.seqid(@seqname)
        @sequence.each {|tc|
          next if tc[:name].empty?
          res = @qadb.tcrun(@envid, @seqid, tc) || next
          tc[:tid] = res[:tid]
        }
      end
      @status = :loaded
    else
      @sequence = []
      @status = :error
    end
  end

  # load the last run's data from tcrun, for display
  def load_last_run
    ($log.warn 'no sequence'; return) unless @env && @sequence
    ($log.warn 'no QADB'; return) unless @qadb
    @sequence.each_with_index {|tc, idx|
      next if tc[:name].empty?
      @sequence[idx] = @qadb.tcrun(@envid, @seqid, tc) || next
    }
  end

  # start the loaded sequence with optional override parameters, return thread
  def start_sequence(params={})
    $log.info "start_sequence (#{@seqname.inspect})  #{params}"
    ($log.warn '  test is already running'; return) if [:running, :aborting].member?(@status)
    ($log.warn '  no test seqence loaded'; return) if @status == :idle
    # optionally run selected tests only
    idxs = params.delete(:idx)
    idxs = [idxs] if idxs.kind_of?(Numeric)
    # clean up results (in case of a re-run)
    @sequence.each_with_index {|tc, idx|
      next if idxs && !idxs.member?(idx)
      tc[:status] = nil
      tc[:tstart] = tc[:duration] = nil
      @qadb.update_status(tc) if @qadb
    }
    # run the (selected) tests
    @status = :running
    @seqthread = Thread.new {
      @sequence.each_with_index {|tc, idx|
        next if idxs && !idxs.member?(idx)
        next if tc[:name].nil? || tc[:name].empty?
        tc[:status] = :running
        tc[:tstart] = Time.now
        @qadb.update_status(tc) if @qadb
        begin
          @reporter = TestCases.runtest(tc[:name], @env, params.merge(parameters: tc[:parameters])) {|inst|
            @instance = inst  # allows to abort the current testcase selectively
            tc[:duration] = inst.name[0..12]
          }
          tc[:status] = :error unless @reporter
        rescue SyntaxError, StandardError, LoadError, Exception => ex
          $log.warn "#{$!}\n#{ex.backtrace.join("\n")}"
          tc[:status] = :error
        end
        tc[:duration] = (Time.now - tc[:tstart]).to_f.round(1)
        if @reporter
          sio = StringIO.new
          @reporter.reporters[0].io = sio
          @reporter.reporters[0].report
          sio.rewind
          tc[:report] = sio.readlines
          tc[:status] = @reporter.passed? ? :passed : :failed
        else
          tc[:status] = :failed
        end
        @qadb.update_status(tc) if @qadb
        $log.info "TC status: #{tc[:status]}"
        # call cleanup method also when failed to avoid leftover lots
        if [:failed, :error, :aborted].member?(tc[:status])
          begin
            Object.const_get(tc[:name]).after_all_passed
          rescue
          end
        end
      }
      # update overall status
      if @status == :running # may have been aborted
        @sequence.each {|tc|
          next if tc[:name].nil? || tc[:name].empty?
          @status = :failed if tc[:status] == :failed
          @status = :partial if tc[:status].nil?
          @status = :aborted if tc[:status] == :aborted
          @status = :error if tc[:status] == :error
          break if [:partial, :error, :aborted].member?(@status)
        }
        @status = :passed if @status == :running
      end
    }
  end

  # abort test sequence
  def stop_sequence
    $log.info 'stop_sequence'
    return unless @seqthread && @seqthread.alive?
    @seqthread.kill
    @status = :aborting
    @seqthread.join
    @status = :aborted
    @sequence.each {|tc|
      if tc[:status] == :running
        tc[:status] = :aborted
        @qadb.update_status(tc) if @qadb
      end
    }
  end

  # abort the currently running testcase (current method will be completed), continue with the next one
  def stop_testcase(idx)
    $log.info 'stop_testcase'
    return unless @seqthread && @seqthread.alive?
    # prevent further tests
    $setup_ok = false
    # inject an assertion for better reporting
    ass = Minitest::Assertion.new("\n--> Manually aborted!")
    ass.set_backtrace('')
    @instance.failures << ass
    # show status while the current test is still running
    tc = @sequence[idx]
    tc[:status] = :aborted
    @qadb.update_status(tc) if @qadb
  end

  # start the loaded sequence and wait for completion (for interactive use), return status
  def run_sequence(params={})
    t = start_sequence(params) || return
    t.join
    @status
  end

  # store the current sequence's result in a file, return true on success
  def store_result(params={})
    tag = params[:tag] || ''
    tag += '_' unless tag.empty?
    fname = "log/tcrunner_#{@env}_#{@seqname}_#{tag}#{Time.now.utc.strftime('%F_%T').gsub(':', '-')}.txt"
    $log.info "store_result (#{fname})"
    File.binwrite(fname, @sequence.to_yaml)
  end

  def compare_results(resultfiles=nil, params={})
    alldata = []
    resultfiles ||= "log/tcrunner_#{@env}_*.txt"
    allfiles = Dir[resultfiles]
    $log.info "compare_results:\n#{allfiles.pretty_inspect}"
    runs = ['TestCase']
    durations = {}
    allfiles.each_with_index {|f, i|
      runs << File.basename(f, '.txt').sub('tcrunner_', '')
      tcs = YAML.load_file(f)
      tcs.each {|tc|
        next if tc[:name] == ''
        durations[tc[:name]] ||= []
        durations[tc[:name]][i] = tc[:duration]
      }
    }
    $log.info "\ntest runs:\n#{runs.join('  ')}\n#{durations.pretty_inspect}"
    separator = params[:separator] || ','
    res = "TCRunner Results\n" + runs.join(separator) +"\n" + "\n"
    durations.each_pair {|tcname, tt|
      res += tcname + separator + '="' + tt.join("\"#{separator}=\"") + "\"\n"
    }
    tag = params[:tag] || Time.now.utc.strftime('%F_%T').gsub(':', '-')
    File.binwrite("log/tcrunner_comp_#{env.upcase}_#{tag}.csv", res)
  end

end
