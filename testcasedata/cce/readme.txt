Dataset folder containing subdirectories named as the PD Ids used in KLA files.
The KLA files and images are taken as-is from original production lots.
They are used as templates to generate lot specific data that is sent to the YMS server.
If a PD folder is empty the CCE::YMSLoader instance takes the KLA file and the images in
the folder named 'default', thus avoiding duplicate data.
--
sfrieske, 2016-10-18
