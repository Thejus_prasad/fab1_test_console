# Create start and stop scripts for test environments
#
# Author: ssteidte, 2014-10-10
#
# History:
#   2015-03-03 ssteidte, use jdk1.7xx_x64 for Linux
#   2019-04-05 sfrieske, added QE Emulator starter

require 'time'
require 'zip'

TargetDir = 'dist'

# create zip file in dist, ready for server distribution
def create_zipfile(site='Fab1')
  zname = "#{TargetDir}/testconsole_#{site.downcase}_#{Time.now.iso8601.split('T').first}.zip"
  puts "Creating #{zname}"
  rm zname if File.exists?(zname)
  Zip::File.open(zname, true) {|zipfile|
    #zipfile.mkdir('log')
    dir = File.join(TargetDir, 'bin')
    Dir.entries(dir).each {|f|
      next if f.start_with?('.')
      zipfile.add(File.join('bin', f), File.join(dir, f))
    }
    Dir['etc/**/*'].each {|f|
      next if f.start_with?('.') || f.include?('testcasedata') && !f.include?('recipe')
      zipfile.add(f, f)
    }
    Dir['lbin/**/*'].each {|f|
      next if f.start_with?('.')
      zipfile.add(f, f)
    }
    Dir['lib/**/*'].each {|f|
      next if f.start_with?('.', 'XX', 'lib/mq9', 'lib/asmview', 'lib/rms', 'lib/space')
      next if f.include?('javadoc') || f.include?('source') || f.include?('-src-')
      zipfile.add(f, f)
    }
    Dir['ruby/**/*'].each {|f|
      # next if f.start_with?('ruby/tcrunner', 'ruby/testcases') || f.include?('attic')
      next if f.include?('attic')  # keep testcases and tcrunner code for reference
      zipfile.add(f, f)
    }
    # Dir['jruby/bin/**/*'].each {|f| zipfile.add(f, f)}
    # Dir['jruby/lib/**/*'].each {|f| zipfile.add(f, f)}
    # Dir['testparameters/**/*'].each {|f|
    #   zipfile.add(f, f)
    # }
  }
  puts 'Done.'
end

# create start and stop scripts <fname>.bat and <fname>.sh in bin, command is anything org.jruby.Main takes
#
# pass bg: false to start the command in foreground (e.g. console),
# else a background launcher is created along with a stop script
def create_starter(fname, command, params={})
  dir = params[:dir] || "#{TargetDir}/bin"
  javaargs = String.new('')
  javaargs += "-cp lib/jacorb/*:lib/simulators/*:lib/#{params[:flavour] || 'R15_custom'}/* " if params[:java]
  javaargs += params[:javaparams] + ' ' if params[:javaparams]  # e.g. -Duser.timezone=
  if params[:java]
    javaargs += '-Dorg.omg.CORBA.ORBClass=org.jacorb.orb.ORB -Dorg.omg.CORBA.ORBSingletonClass=org.jacorb.orb.ORBSingleton '
  else
    javaargs += '-XX:+UseConcMarkSweepGC -jar lib/jruby-complete.jar '
  end
  javaargs += command
  #
  # Unix .sh
  javacmd = "$java #{javaargs} $*"
  lines = [
    '#!/bin/bash', '# automatically generated script, make changes in Rakefile',
    'pushd $(dirname $BASH_SOURCE)/..',
    'java=$(ls -dtra /3rd_party/java/Linux/jdk1.8*_x64|tail -n 1)/bin/java',
    'export RUBYOPT=--enable=frozen-string-literal'
  ]
  if env = params[:env]
    env.each_pair {|k, v| lines << "export #{k}=#{v}"}
  end
  if params[:bg] != false
    # write stop script
    create_stopscript("stop_#{fname.sub('start_', '')}", params[:grepfor] || command, dir: dir)
    # start in background
    log = params[:log] || "log/#{fname}_start.log"
    lines << "nohup #{javacmd} >#{log} 2>&1 &"
    # show if background process is running
    lines += ['sleep 5', "echo process running: $(ps -ef|grep -e '#{command}'|grep -v grep)", '']
  else
    lines << javacmd
  end
  lines += ['popd', '']
  # write start script
  puts "write #{dir}/#{fname}.sh"
  File.binwrite("#{dir}/#{fname}.sh", lines.join("\n"))
  #
  # Windows .bat, foreground starters only
  return unless params[:bat]
  javacmd = "java #{javaargs} %*"
  title = params[:title] || 'QA Console'
  lines = [
    '@ECHO OFF', ':: automatically generated script, make changes in Rakefile',
    'pushd %~dp0', 'cd %~dp0..',
    'echo Starting %~nx0', "title #{title}",
    'set JAVA_HOME=',
    'set RUBYOPT=--enable=frozen-string-literal',
  ]
  if env = params[:env]
    env.each_pair {|k, v| lines << "set #{k}=#{v}"}
  end
  lines += ["java #{javaargs} %*", 'popd', '']
  puts "writing #{dir}/#{fname}.bat"
  File.binwrite("#{dir}/#{fname}.bat", lines.join("\r\n"))
end

def create_stopscript(fname, grepfor, params={})
  dir = params[:dir] || "#{TargetDir}/bin"
  lines = ['#!/bin/bash', '# automatically generated script, make changes in Rakefile',
    "ps -ef|grep '#{grepfor}'|grep -v grep|awk '{print $2}'|xargs kill >/dev/null 2>&1",
    'sleep 5', "echo process running: $(ps -ef|grep '#{grepfor}'|grep -v grep)"
  ]
  File.binwrite("#{dir}/#{fname}.sh", lines.join("\n"))
end

desc 'Create TestConsole and jrake starters'
task :console, :site, :dir do |t, params|
  flavour = 'R15_custom'
  create_starter("console_#{flavour}", './ruby/console.rb',
    {title: "QA Console #{flavour}", flavour: flavour, bat: true, bg: false}.merge(params))
  create_starter("tcrunner_#{flavour}", './ruby/console.rb ruby/tcrunner/run.rb',
    {title: "TCRunner #{flavour}", flavour: flavour, bat: true, bg: false}.merge(params))
  # create_starter('jrake', '-S rake', {title: 'JRake', bat: true, bg: false, env: {GEM_PATH: 'ruby/gems'}}.merge(params))
  create_starter('send_mq', 'ruby/misc/send_mq.rb', title: 'SendMQ', bat: true, bg: false)
end

desc 'Create server starters'
task :servers, :site do |t, params|
  envs = (params[:site] || '').downcase.start_with?('f8', 'mt') ? ['f8stag', 'mtqa'] : ['itdc', 'let', 'training']
  envs.each {|env|
    # DummyAMHS
    if env == 'let'
      # multiple instances
      dir = params[:dir] || "#{TargetDir}/bin"
      lines = ['#!/bin/bash', 'pushd $(dirname $0)', 'hostname=$(uname -n)', 'if [ $hostname == "f38asq6" ]; then']
      port, ename, instance = 7777, 'AMHS01', 1
      5.times {
        lines << "  ./_dummyamhs.sh -e let -port #{port} -entity #{ename} -i #{instance}"
        port, ename, instance = port.next, ename.next, instance.next
      }
      lines << 'elif [ $hostname == "f38asq12" ]; then'
      5.times {
        lines << "  ./_dummyamhs.sh -e let -port #{port} -entity #{ename} -i #{instance}"
        port, ename, instance = port.next, ename.next, instance.next
      }
      lines += ['else', '  echo "Error. The DummyAMHS for LET must be started on f38asq6 and f38asq12."', 'fi',
        'sleep 5', 'echo "process running: $(ps -f|grep dummyamhs.DummyAmhsLaunch|grep -v grep)"', 'popd'
      ]
      File.binwrite("#{dir}/start_dummyamhs_let.sh", lines.join("\n"))
      create_starter('_dummyamhs', 'com.globalfoundries.qa.dummyamhs.DummyAmhsLaunch', java: true, log: 'log/start_dummyamhs${2}.log')
      create_stopscript('stop_dummyamhs_let', 'dummyamhs.DummyAmhsLaunch -e let')
    else
      javaparams = env.start_with?('f8', 'mt') ? '-Duser.timezone=Etc/GMT+5' : ''
      javaargs = "com.globalfoundries.qa.dummyamhs.DummyAmhsLaunch -e #{env}"
      javaargs += " -zone UTZ1 -entity AMHSUT1" if env == 'itdc'
      create_starter("start_dummyamhs_#{env}", javaargs, java: true, javaparams: javaparams)
    end
    # JavaEI farm
    command = "com.globalfoundries.qa.javaei.JavaEIFarm -e #{env} -noexerciser"
    javaparams = env.start_with?('f8', 'mt') ? '-Duser.timezone=Etc/GMT+5' : ''
    create_starter("start_javaeifarm_#{env}", command, java: true, javaparams: javaparams)
    # EI recipe request server
    create_starter("start_eirqserver_#{env}", "ruby/lib/misc/eirqserver.rb --env #{env}")
    # RTD servers
    create_starter("start_rtdserver_#{env}", "ruby/lib/rtdserver/dispatchersrv.rb --env #{env}")
    create_starter("start_qeserver_#{env}", "ruby/lib/rtdserver/qesrv.rb --env #{env}")
  }
  # exercisers
  # create_starter('armor_exerciser', 'ruby/misc/armor_exerciser.rb')
  create_starter('space_exerciser', 'ruby/misc/dcr_exerciser.rb')
  create_starter('space_extractor', 'ruby/misc/dcr_extractor.rb')
  create_starter('space_dcrlist_creator', 'ruby/misc/dcrlist_creator.rb', bg: false)
  # create_starter('traceeda_exerciser', 'ruby/misc/traceeda_exerciser.rb')
  #create_starter('lmtrans_exerciser', 'ruby/misc/lmtrans_exerciser.rb')
  #create_starter('pdwh_exerciser_itdc', 'ruby/misc/lotexerciser.rb -e itdc -t pdwh -d -1')
  #create_starter('Stress80kUserParam', 'ruby/loadtests/Stress80kUserParam.rb')
end

task :clean do
  Dir["#{TargetDir}/*"].each {|f| rm_r f unless f.end_with?('zip')}
  mkdir_p File.join(TargetDir, 'bin')
end

desc 'Create site specific start and stop scripts'
task :scripts, [:site] => :clean do |t, params|
  site = params[:site] || ''
  task(:console).invoke(site)
  task(:servers).invoke(site)
end

task :zipfile do
  create_zipfile
end

# task :rdoc do
#   RDoc::RDoc.new.document ['-O', '-o', File.expand_path('rdoc/lib'), File.expand_path('ruby/lib')]
# end

task :default=>[:scripts, :zipfile]
