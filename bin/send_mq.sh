#!/bin/bash
# automatically generated script, make changes in Rakefile
pushd $(dirname $BASH_SOURCE)/..
java=$(ls -dtra /3rd_party/java/Linux/jdk1.8*_x64|tail -n 1)/bin/java
export RUBYOPT=--enable=frozen-string-literal
$java -XX:+UseConcMarkSweepGC -jar lib/jruby-complete.jar ruby/misc/send_mq.rb $*
popd
