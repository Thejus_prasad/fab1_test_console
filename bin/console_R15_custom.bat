@ECHO OFF
:: automatically generated script, make changes in Rakefile
pushd %~dp0
cd %~dp0..
echo Starting %~nx0
title QA Console R15_custom
set JAVA_HOME=
set RUBYOPT=--enable=frozen-string-literal
java -XX:+UseConcMarkSweepGC -jar lib/jruby-complete.jar ./ruby/console.rb %*
popd
